BEGIN
    BEGIN
         EXECUTE IMMEDIATE 'DROP TABLE CONNECTORS_MASTER';
    EXCEPTION
         WHEN OTHERS THEN
                IF SQLCODE != -942 THEN
                     RAISE;
                END IF;
    END;

    EXECUTE IMMEDIATE 'CREATE TABLE CONNECTORS_MASTER(
            ConnectorID int NOT NULL,
            ConnectorName varchar(255) NOT NULL,
            ConnectorAPIVersion varchar(50) NULL,
            ConnectionString varchar(1000) NULL,
			CONSTRAINT PK_Connectors_Master PRIMARY KEY (ConnectorID) 
			)';
END;

BEGIN
    BEGIN
         EXECUTE IMMEDIATE 'DROP TABLE CONNECTED_CONNECTORS';
    EXCEPTION
         WHEN OTHERS THEN
                IF SQLCODE != -942 THEN
                     RAISE;
                END IF;
    END;

    EXECUTE IMMEDIATE 'CREATE TABLE CONNECTED_CONNECTORS(
            ID int NOT NULL,
            ConnectorID int NOT NULL,
            ConnectorServerURL varchar(1000) NOT NULL,
            ConnectorApplication varchar(1000) NOT NULL,
            IsActive number(1) NULL,
			CONSTRAINT cons_isactive CHECK (IsActive IN (1,0)),
			CONSTRAINT PK_Registered_Connectors PRIMARY KEY (ID)
			)';
END;