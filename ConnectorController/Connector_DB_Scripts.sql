USE [Connectors]
GO

/****** Object:  Table [dbo].[ConnectorsMaster]    Script Date: 5/28/2013 2:52:26 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ConnectorsMaster](
	[ID] [int] NOT NULL,
	[ControllerID] [varchar](50) NOT NULL,
	[ConnectorID] [int] NOT NULL,
	[ConnectorName] [varchar](255) NOT NULL,
	[ConnectorAPIVersion] [varchar](50) NOT NULL,
	[ConnectionString] [varchar](1000) NOT NULL,
	[IsDefaultVersion] [bit] NULL,
 CONSTRAINT [PK_Connectors_Master] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


USE [Connectors]
GO

/****** Object:  Table [dbo].[ConnectorInstances]    Script Date: 5/28/2013 2:52:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ConnectorInstances](
	[InstanceID] [int] NOT NULL,
	[ControllerID] [varchar](50) NOT NULL,
	[ConnectorID] [int] NOT NULL,
	[HostName] [varchar](100) NOT NULL,
	[ConnectorServerURL] [varchar](1000) NOT NULL,
	[ConnectorApplication] [varchar](1000) NOT NULL,
	[IsActive] [bit] NULL,
	[Disabled] [bit] NULL,
 CONSTRAINT [PK_ConnectorInstances] PRIMARY KEY CLUSTERED 
(
	[InstanceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


USE [Connectors]
GO

/****** Object:  Table [dbo].[ConnectorParameters]    Script Date: 5/28/2013 2:59:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ConnectorParameters](
	[ParameterID] [int] NOT NULL,
	[ControllerID] [varchar](50) NOT NULL,
	[ConnectorID] [int] NOT NULL,
	[ParameterName] [nvarchar](255) NOT NULL,
	[ParameterValue] [nvarchar](1000) NULL,
 CONSTRAINT [PK_ConnectorParameters] PRIMARY KEY CLUSTERED 
(
	[ParameterID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


USE [Connectors]
GO

/****** Object:  Table [dbo].[ConnectorActivity]    Script Date: 5/28/2013 2:57:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ConnectorActivity](
	[ID] [int] NOT NULL,
	[InstanceID] [int] NOT NULL,
	[Client] [nvarchar](50) NULL,
	[SpaceID] [uniqueidentifier] NULL,
	[UniqueID] [uniqueidentifier] NULL,
	[RequestType] [nvarchar](50) NULL,
	[Status] [int] NULL,
	[StartTime] [datetime] NULL,
	[FinishTime] [datetime] NULL,
 CONSTRAINT [PK_ConnectorActivity] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


