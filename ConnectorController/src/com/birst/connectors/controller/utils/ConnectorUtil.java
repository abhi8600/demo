package com.birst.connectors.controller.utils;

public class ConnectorUtil {
	public enum Status 
	{
	    none ("None"),
	    inProgress ("InProgress"),
	    completed ("Completed"),
	    failed ("Failed");

	    private final String name;       

	    private Status(String s) 
	    {
	        name = s;
	    }

	    public boolean equalsName(String otherName)
	    {
	        return (otherName == null)? false:name.equals(otherName);
	    }

	    public String toString()
	    {
	       return name;
	    }
	}
	
	public enum Operation 
	{
	    none ("None"),
	    extract ("Extract");

	    private final String name;       

	    private Operation(String s)
	    {
	        name = s;
	    }

	    public boolean equalsName(String otherName)
	    {
	        return (otherName == null)? false:name.equals(otherName);
	    }

	    public String toString()
	    {
	       return name;
	    }
	}
}
