package com.birst.connectors.controller.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

/**
 * 
 * @author mpandit
 *
 */
public final class EncryptionService {

	private static EncryptionService instance;

	private EncryptionService() {
	}

	private static final Logger logger = Logger.getLogger(EncryptionService.class);

	// character set to choose the salt from
	private static final String SALTCHARS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

	// random number generator for generating random salts
	private SecureRandom generator = null;
	
	public static synchronized EncryptionService getInstance()
	{
		if (instance == null) {
			instance = new EncryptionService();
            try
            {
            	instance.generator = SecureRandom.getInstance("SHA1PRNG");
            }
            catch (NoSuchAlgorithmException nex)
            {
                logger.error("No SHA1PRNG provider: " + nex.getMessage());
            }
		}
		return instance;
	}

	// generate a new random salt for encrypting new passwords
	public synchronized String genSalt() {
		StringBuilder salt = new StringBuilder();
		salt.append(SALTCHARS.charAt(generator.nextInt(52)));
		salt.append(SALTCHARS.charAt(generator.nextInt(52)));
		return salt.toString();
	}

	public synchronized String createDigest(String salt, String cleartext) {

		if ((cleartext == null) || "".equals(cleartext)) {
			return null;
		}

		MessageDigest sha = null;

		try {
			sha = MessageDigest.getInstance("SHA-1");
		} catch (java.security.NoSuchAlgorithmException e) {
			return null;
		}
		byte[] saltbytes = salt.getBytes();

		sha.reset();
		sha.update(cleartext.getBytes());
		sha.update(saltbytes);
		byte[] pwhash = sha.digest();

		return "$sha-1$" + new String(Base64.encodeBase64(concatenate(pwhash, saltbytes)));
	}
	
	public synchronized String createDigest(String cleartext) {

		if ((cleartext == null) || "".equals(cleartext)) {
			return null;
		}

		MessageDigest sha = null;

		try {
			sha = MessageDigest.getInstance("SHA-1");
		} catch (java.security.NoSuchAlgorithmException e) {
			return null;
		}
		sha.reset();
		sha.update(cleartext.getBytes());
		byte[] pwhash = sha.digest();

		return new String(Base64.encodeBase64(pwhash));
	}


	public synchronized boolean equals(String digest, String cleartext) {
		
		// if both are null, then okay - eventually we should disallow null passwords
		if ((digest == null) && (cleartext == null)) {
			return true;
		}

		// if only one of them is null, they dont match.
		if (((digest == null) && (cleartext != null)) ||
			((digest != null) && (cleartext == null)))
		{
			return false;
		}
		
		// if the cleartext is already hashed, compare it directly with the digest
		// - special case of password stored in repository and sent from the Admin tool
		if ((cleartext != null) && cleartext.startsWith("$sha-1$"))
		{
			return cleartext.equals(digest);
		}
		
		if (cleartext == null)
			return false;

		// otherwise, extract the salt from the digest, hash the cleartext and compare
		
		MessageDigest sha = null;

		try {
			sha = MessageDigest.getInstance("SHA-1");
		} catch (java.security.NoSuchAlgorithmException e) {
			return false;
		}
		
		digest = digest.substring(7);
		
		byte[][] hs = split(Base64.decodeBase64(digest.getBytes()), 20);
		byte[] hash = hs[0];
		byte[] salt = hs[1];

		sha.reset();
		sha.update(cleartext.getBytes());
		sha.update(salt);
		byte[] pwhash = sha.digest();

		return MessageDigest.isEqual(hash, pwhash);
	}

	private byte[] concatenate(byte[] l, byte[] r) {
		byte[] b = new byte[l.length + r.length];
		System.arraycopy(l, 0, b, 0, l.length);
		System.arraycopy(r, 0, b, l.length, r.length);
		return b;
	}

	private byte[][] split(byte[] src, int n) {
		byte[] l, r;
		if (src == null || src.length <= n) {
			l = src;
			r = new byte[0];
		} else {
			l = new byte[n];
			r = new byte[src.length - n];
			System.arraycopy(src, 0, l, 0, n);
			System.arraycopy(src, n, r, 0, r.length);
		}
		byte[][] lr = { l, r };
		return lr;
	}
	
	/**
	 * decrypt the DB password
	 */
	public String decrypt(String text)
	{
		// build up the encryption password - never have the full password as a literal that can be easily extracted
		// keep these literals small enough that they look like noise
		String SecretPassword = ("KA" + "Sh" + "DfOI" + "wue" + "hr" + "QwE" + "iNau" + "vnA" + "Weu").toLowerCase();
		return decrypt(text, SecretPassword);
	}

	/**
	 * Decrypt a string using a password
	 * 
	 * @param text
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public synchronized String decrypt(String text, String password)
	{
		if (text == null)
			return (null);
		try
		{
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			byte[] keyBytes = new byte[16];
			byte[] b = password.getBytes("UTF-8");
			int len = b.length;
			if (len > keyBytes.length)
				len = keyBytes.length;
			System.arraycopy(b, 0, keyBytes, 0, len);
			SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
			IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);
			cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
			byte[] results = cipher.doFinal(Base64.decodeBase64(text.getBytes("UTF-8")));
			return new String(results, "UTF-8");
		} catch (Exception ex)
		{
			logger.warn("Decryption failed - can not show text because it might be cleartext, returning original text");
			return text;
		}
	}

	/**
	 * Encrypt the db password
	 */
	public String encrypt(String text)
	{
		// build up the encryption password - never have the full password as a literal that can be easily extracted
		// keep these literals small enough that they look like noise
		String SecretPassword = ("KA" + "Sh" + "DfOI" + "wue" + "hr" + "QwE" + "iNau" + "vnA" + "Weu").toLowerCase();
		return encrypt(text, SecretPassword);
	}

	/**
	 * Encrypt a string using a password
	 */
	public synchronized String encrypt(String text, String password)
	{
		if (text == null)
			return (null);
		try
		{
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			byte[] keyBytes = new byte[16];
			byte[] b = password.getBytes("UTF-8");
			int len = b.length;
			if (len > keyBytes.length)
				len = keyBytes.length;
			System.arraycopy(b, 0, keyBytes, 0, len);
			SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
			IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);
			cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);
			byte[] inputBytes = text.getBytes("UTF8");
			byte[] outputBytes = cipher.doFinal(inputBytes);
			return new String(Base64.encodeBase64(outputBytes), "UTF-8");
		} catch (Exception ex)
		{
			logger.warn("Encryption failed - not showing the cleartext or password for obvious reasons");
		}
		return (null);
	}

}
