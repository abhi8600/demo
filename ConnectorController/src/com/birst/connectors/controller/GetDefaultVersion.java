package com.birst.connectors.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.birst.connectors.controller.utils.ConnectionPool;

/**
 * Servlet implementation class GetDefaultVersion
 */
public class GetDefaultVersion extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(GetDefaultVersion.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetDefaultVersion() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/xml;charset=UTF-8");
	    response.setCharacterEncoding("UTF-8");
		PrintWriter pw = response.getWriter();
	    String output = null;
	    String errorMessage = null;
	    ConnectionPool cp = null;
	    Connection conn = null;
	    PreparedStatement pstmt = null;
	    try {
	    	String connectorString = request.getParameter("ConnectorString");
	    	cp = ConnectionPool.getInstance(ConnectorListener.connectorDBDriver, ConnectorListener.connectorDBConnectString, ConnectorListener.connectorDBAdminUser, ConnectorListener.connectorDBAdminPwd, ConnectorListener.maxConnections, ConnectorListener.connectorDBType);
	    	conn = cp.getConnection();
	    	pstmt = conn.prepareStatement("SELECT ConnectorName, IsDefaultVersion FROM " + ConnectorListener.CONNECTORS_SCHEMA + "." + ConnectorListener.CONNECTORS_MASTER +
	    			" WHERE ConnectionString=? AND ControllerID=?");
	    	pstmt.setString(1, connectorString);
	    	pstmt.setString(2, ConnectorListener.controllerID);
	    	boolean foundConnector = false;
	    	boolean isDefaultConnector = false;
	    	ResultSet rs = pstmt.executeQuery();
	    	String connectorName = null;
	    	if (rs.next())
	    	{
	    		foundConnector = true;
	    		connectorName = rs.getString(1);
	    		isDefaultConnector = rs.getBoolean(2);
	    	}
	    	if (isDefaultConnector)
	    	{
	    		output = connectorString;
	    	}
	    	else 
	    	{
	    		if (foundConnector)
	    		{
		    		//find default connector for ConnectionString
		    		pstmt = conn.prepareStatement("SELECT ConnectionString FROM " + ConnectorListener.CONNECTORS_SCHEMA + "." + ConnectorListener.CONNECTORS_MASTER +
		    				" WHERE ConnectorName=? AND IsDefaultVersion=?");
		    		pstmt.setString(1, connectorName);
		    		pstmt.setBoolean(2, true);
		    		rs = pstmt.executeQuery();
		    		if (rs.next())
		    		{
		    			output = rs.getString(1);
		    		}
		    		else
			    	{
		    			throw new Exception("No default connector found for connectionstring:" + connectorString);
			    	}
	    		}
	    		else
		    	{
	    			throw new Exception("No connector found for connectionstring:" + connectorString);
		    	}
	    	}	    	
	    }
	    catch (Exception e) {
	    	logger.error(e.getMessage(), e);
	    	errorMessage = e.getMessage();
	    }
	    finally {
	    	ConnectorListener.closePreparedStatement(pstmt);
	    }
	    StringBuilder sb = new StringBuilder();
	    sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
	    if (output == null)
	    {
	    	sb.append("<Root>");
	    	sb.append("<Success>false</Success>");
	    	sb.append("<Reason>").append(errorMessage).append("</Reason>");
	    	sb.append("</Root>");
	    }
	   else
	    {
		   sb.append("<Root>");
		   sb.append("<Success>true</Success>");
		   sb.append("<Result>").append(output).append("</Result>");
		   sb.append("</Root>");
	    }
	    pw.write(sb.toString());
	    pw.flush();
	}

}
