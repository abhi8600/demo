package com.birst.connectors.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;

import com.birst.connectors.controller.utils.ConnectionPool;

/**
 * Servlet implementation class GetConnectorParameters
 */
public class GetConnectorParameters extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(GetConnectorParameters.class);
	public static final String NODE_CONNECTOR_PARAMS_ROOT = "ConnectorParameters";
	public static final String NODE_CONNECTOR_PARAMS_NODE = "ConnectorParameter";
	public static final String NODE_CONNECTOR_PARAM_NAME_NODE = "ParameterName";
	public static final String NODE_CONNECTOR_PARAM_VALUE_NODE = "ParameterValue";
	public static final String BIRST_PARAMETERS = "BirstParameters";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetConnectorParameters() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ConnectionPool cp = null;
	    Connection conn = null;
	    PreparedStatement pstmt = null;
	    try {
	    	String connectorString = request.getParameter("ConnectorString");
	    	cp = ConnectionPool.getInstance(ConnectorListener.connectorDBDriver, ConnectorListener.connectorDBConnectString, ConnectorListener.connectorDBAdminUser, ConnectorListener.connectorDBAdminPwd, ConnectorListener.maxConnections, ConnectorListener.connectorDBType);
	    	conn = cp.getConnection();
	    	pstmt = conn.prepareStatement("SELECT ConnectorID FROM " + ConnectorListener.CONNECTORS_SCHEMA + "." + ConnectorListener.CONNECTORS_MASTER +
	    			" WHERE ConnectionString=? AND ControllerID=?");
	    	pstmt.setString(1, connectorString);
	    	pstmt.setString(2, ConnectorListener.controllerID);
	    	ResultSet rs = pstmt.executeQuery();
	    	int connectorID = -9999;
	    	if (rs.next())
	    	{
	    		connectorID = rs.getInt(1);
	    	}
	    	if (connectorID == -9999)
	    		throw new Exception("No connector found for connectionstring:" + connectorString);
	    	OMNamespace ns = null;
    		OMFactory factory = OMAbstractFactory.getOMFactory();
    		OMElement root = factory.createOMElement(NODE_CONNECTOR_PARAMS_ROOT, ns);
	    	
    		//read connector parameters
    		pstmt = conn.prepareStatement("SELECT ParameterID, ParameterName, ParameterValue FROM " + ConnectorListener.CONNECTORS_SCHEMA + "." + ConnectorListener.CONNECTOR_PARAMETERS +
	    			" WHERE ConnectorID=? AND ControllerID=?");
	    	pstmt.setInt(1, connectorID);
	    	pstmt.setString(2, ConnectorListener.controllerID);
	    	rs = pstmt.executeQuery();
	    	while (rs.next())
	    	{
	    		String paramName = rs.getString(2);
	    		String paramValue = rs.getString(3);
	    		addToResult(ns, factory, root, paramName, paramValue);
	    	}
	    	
	    	
	    	//read birst global parameters
    		pstmt = conn.prepareStatement("SELECT PropertyName, PropertyValue FROM " + ConnectorListener.ACORN_SCHEMA + "." + ConnectorListener.GLOBAL_PROPERTIES +
	    			" WHERE ENABLE=? AND PropertyName like 'Connector_%' AND ACCOUNT_ID is null");
    		pstmt.setInt(1, 1);
	    	rs = pstmt.executeQuery();
	    	boolean isFirstBirstParam = true;
	    	String birstParamsKey = BIRST_PARAMETERS;
	    	StringBuffer birstParamsValues = new StringBuffer();
	    	while (rs.next())
	    	{
	    		if (isFirstBirstParam)
	    		{
	    			isFirstBirstParam = false;
	    		}
	    		else
	    		{
	    			birstParamsValues.append(",");
	    		}
	    		String propertyName = rs.getString(1);
	    		propertyName = propertyName.substring("Connector_".length());
	    		birstParamsValues.append(propertyName + "=" + rs.getString(2));
	    	}
	    	if (birstParamsValues.length() > 0)
	    	{
	    		addToResult(ns, factory, root, birstParamsKey, birstParamsValues.toString());
	    	}
	    	
	    	OutputStream pw = response.getOutputStream();
			try {
				response.setContentType("text/xml;charset=UTF-8");
				pw.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>".getBytes("UTF-8"));
				XMLStreamWriter writer = XMLOutputFactory.newInstance().createXMLStreamWriter(pw, "UTF-8");
				root.serialize(writer);
				writer.flush();
			} catch (XMLStreamException e) {
				logger.error(e, e);
			}
	    }
	    catch (Exception e) {
	    	logger.error(e.getMessage(), e);	    	
	    }
	    finally {
	    	ConnectorListener.closePreparedStatement(pstmt);
	    }	    
	}
	
	private void addToResult(OMNamespace ns, OMFactory factory, OMElement root, String name, String value)
	{
		if (value == null)
			value = "";
		OMElement paramChild = factory.createOMElement(NODE_CONNECTOR_PARAMS_NODE, ns);
		OMElement paramNameNode = factory.createOMElement(NODE_CONNECTOR_PARAM_NAME_NODE, ns);
		paramNameNode.setText(name);
		paramChild.addChild(paramNameNode);
		OMElement paramValueNode = factory.createOMElement(NODE_CONNECTOR_PARAM_VALUE_NODE, ns);
		paramValueNode.setText(value);
		paramChild.addChild(paramValueNode);
		root.addChild(paramChild);
	}

}
