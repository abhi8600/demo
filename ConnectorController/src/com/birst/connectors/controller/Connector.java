package com.birst.connectors.controller;

public class Connector {
	private int connectorID;
	private String connectorName;
	private String connectorAPIVersion;
	private String connectionString;
	private boolean defaultVersion;
	
	public int getConnectorID() {
		return connectorID;
	}
	public void setConnectorID(int connectorID) {
		this.connectorID = connectorID;
	}
	public String getConnectorName() {
		return connectorName;
	}
	public void setConnectorName(String connectorName) {
		this.connectorName = connectorName;
	}
	public String getConnectorAPIVersion() {
		return connectorAPIVersion;
	}
	public void setConnectorAPIVersion(String connectorAPIVersion) {
		this.connectorAPIVersion = connectorAPIVersion;
	}
	public String getConnectionString() {
		return connectionString;
	}
	public void setConnectionString(String connectionString) {
		this.connectionString = connectionString;
	}
	public boolean isDefaultVersion() {
		return defaultVersion;
	}
	public void setIsDefaultVersion(boolean defaultVersion) {
		this.defaultVersion = defaultVersion;
	}
}
