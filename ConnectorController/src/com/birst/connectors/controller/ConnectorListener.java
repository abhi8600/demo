package com.birst.connectors.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Map.Entry;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.birst.connectors.controller.utils.ConnectionPool;

/**
 * Servlet implementation class ConnectorListener
 * @author mpandit
 */
public class ConnectorListener extends HttpServlet implements ServletContextListener{
	private static final long serialVersionUID = 1L;
	
	private static final Logger logger = Logger.getLogger(ConnectorListener.class);
	public static int maxConnections;
	public static String controllerID = "1";
	public static final String ControllerHostName = setHostName();
	public static String connectorDBDriver;
	public static String connectorDBConnectString;
	public static String connectorDBAdminUser;
	public static String connectorDBAdminPwd;
	public static String connectorDBType;

	public static final String CONNECTORS_SCHEMA = "dbo";
	public static final String ACORN_SCHEMA = "dbo";
	public static final String CONNECTORS_MASTER = "ConnectorsMaster";
	public static final String CONNECTOR_INSTANCES = "ConnectorInstances";
	public static final String CONNECTOR_PARAMETERS = "ConnectorParameters";
	public static final String CONNECTOR_ACTIVITY = "ConnectorActivity";
	public static final String GLOBAL_PROPERTIES = "GLOBAL_PROPERTIES";
	
	private static final String setHostName()
	{
		try
		{
			return InetAddress.getLocalHost().getHostName();			
		}
		catch(Exception uhe)
		{
		}
		logger.error("Problem resolving hostname, using loopback hostname");
		return InetAddress.getLoopbackAddress().getHostName();
	}
	
	/**
	 * dump the properties to the log file
	 * @param props
	 */
	private static void printProperties(Properties props)
	{
		logger.debug("== Properties files ==");
		for (Entry<Object, Object> item : props.entrySet())
		{
			logger.debug(item.getKey().toString() + "=" + item.getValue().toString());
		}
		logger.debug("====");
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		// TODO Auto-generated method stub		
	}

	@SuppressWarnings("resource")
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		String connectorHome = System.getProperty("connector.home");
		logger.debug("Context Initializing - connector.home=" + connectorHome);
		
		ServletContext context = sce.getServletContext();
		
		InputStream in = null;
		Properties buildProperties = new Properties();
		try
		{
			in = context.getResourceAsStream("build.number");
			if (in != null)
			{
				buildProperties.load(in);
				logger.debug("Using properties file: build.number (found via the classpath)");
			}
		} catch (IOException e)
		{
			logger.debug("classpath properties file (build.number) not found.");
		} finally
		{
			if (in != null)
			{
				try
				{
					in.close();
				} catch (Exception e)
				{
				}
			}
		}
		// Report results of search
		if (buildProperties.isEmpty())
		{
			logger.debug("Properties file (build.number) not found or empty after full search was performed.");
		} else
		{
			if (logger.isDebugEnabled())
				printProperties(buildProperties);
		}
		
		File connectorsProperties = new File(connectorHome + File.separator + "conf" + File.separator + "connectors.properties");
		Properties configProperties = new Properties();
		if (connectorsProperties.exists())
		{
			logger.debug("Overriding connectors configuration properties from " + connectorsProperties);
			try
			{
				configProperties.load(new FileReader(connectorsProperties));
			}catch (IOException ioe)
			{
				logger.error("problem loading connectors.properties - " + ioe.getMessage(), ioe);
			}
		}
		
		String connectorControllerID = getParameter("ConnectorControllerID", configProperties, context);
		if (connectorControllerID != null && !connectorControllerID.isEmpty())
		{
			controllerID = connectorControllerID;
			logger.debug("Initializing ConnectorController: " + controllerID);			
		}
		else
		{
			logger.warn("ConnectorControllerID not found, using default value: " + controllerID);
		}
		
		connectorDBDriver = getParameter("ConnectorDBDriver", configProperties, context);
		connectorDBConnectString = getParameter("ConnectorDBConnectString", configProperties, context);
		connectorDBAdminUser = getParameter("ConnectorDBAdminUser", configProperties, context);
		connectorDBAdminPwd = getParameter("ConnectorDBAdminPwd", configProperties, context);
		connectorDBType = getParameter("ConnnectorDBType", configProperties, context);
		ConnectionPool cp = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		Statement stmt = null;
		ResultSet rs = null;
		try
		{
			logger.debug("Initializing connector database for contoller: " + controllerID + " for Host:" + ControllerHostName);
			cp = ConnectionPool.getInstance(connectorDBDriver, connectorDBConnectString, connectorDBAdminUser, connectorDBAdminPwd, maxConnections, connectorDBType);
			conn = cp.getConnection();
			
			//cleanup tables for current controller id and host 
			cleanupTables(conn);
			
			String selectSQL = "SELECT ConnectorID FROM " + CONNECTORS_SCHEMA + "." + CONNECTORS_MASTER + " WHERE ControllerID=?";
			logger.debug(selectSQL);
			List<Integer> availableConnectors = new ArrayList<Integer>();
			pstmt = conn.prepareStatement(selectSQL);
			pstmt.setString(1, controllerID);			
			rs = pstmt.executeQuery();
			while (rs.next())
			{
				availableConnectors.add(rs.getInt(1));				
			}
			
			
			String connectorsConfigFile = context.getInitParameter("ConnectorsConfig");
			if (connectorsConfigFile != null && !connectorsConfigFile.isEmpty())
			{
				connectorsConfigFile = connectorsConfigFile.replace("V{CONNECTOR_HOME}", connectorHome);
				File configFile = new File(connectorsConfigFile);
				if (configFile.exists())
				{
					BufferedReader configFileReader = null;
					try
					{
						selectSQL = "SELECT Max(ID) FROM " + CONNECTORS_SCHEMA + "." + CONNECTORS_MASTER;
						logger.debug(selectSQL);
						pstmt = conn.prepareStatement(selectSQL);
						rs = pstmt.executeQuery();
						int maxID = 0;
						if (rs.next())
						{
							maxID = rs.getInt(1);
						}
						configFileReader = new BufferedReader(new FileReader(configFile));
						String insertSQL = "INSERT INTO " + CONNECTORS_SCHEMA + "." + CONNECTORS_MASTER + " (ID, ControllerID, ConnectorID, ConnectorName, ConnectorAPIVersion, ConnectionString, IsDefaultVersion) " +
											"VALUES(?,?,?,?,?,?,?)";
						logger.debug(insertSQL);
						pstmt = conn.prepareStatement(insertSQL);
						String line = null;
						int addedConnectors = 0;
						while ((line = configFileReader.readLine()) != null)
						{
							if (line.startsWith("//"))
								continue;
							String[] connectorProperties = line.split("\t");
							if (connectorProperties != null && connectorProperties.length == 5)
							{
								if(!availableConnectors.contains(Integer.parseInt(connectorProperties[0])))
								{
									pstmt.setInt(1, ++maxID);
									pstmt.setString(2, controllerID);
									pstmt.setInt(3, Integer.parseInt(connectorProperties[0]));
									pstmt.setString(4, connectorProperties[1]);
									pstmt.setString(5, connectorProperties[2]);
									pstmt.setString(6, connectorProperties[3]);
									pstmt.setBoolean(7, Boolean.parseBoolean(connectorProperties[4]));
									pstmt.execute();
									addedConnectors++;
								}
							}
							else
							{
								logger.error("Failed to parse connectors.config: " + line);
							}
						}
						if (addedConnectors > 0)
						{
							logger.debug(addedConnectors + " connectors added to " + CONNECTORS_SCHEMA + "." + CONNECTORS_MASTER);
						}
						logger.debug("successfully loaded connectors.config");
					}
					catch (IOException ioe)
					{
						logger.error("problem loading connectors.config - " + ioe.getMessage(), ioe);
					}
					finally 
					{
						if (configFileReader != null)
						{
							try
							{
								configFileReader.close();
							}
							catch (IOException ex)
							{							
							}
						}
					}
				}
			}
			String connectorInstancesConfig = context.getInitParameter("ConnectorInstancesConfig");
			if (connectorInstancesConfig != null && !connectorInstancesConfig.isEmpty())
			{
				availableConnectors = new ArrayList<Integer>();
				List<String> availableConnectorServerURLs = new ArrayList<String>();
				selectSQL = "SELECT ConnectorID, ConnectorServerURL FROM " + CONNECTORS_SCHEMA + "." + CONNECTOR_INSTANCES + " WHERE ControllerID=? AND HostName=?";
				logger.debug(selectSQL);
				pstmt = conn.prepareStatement(selectSQL);
				pstmt.setString(1, controllerID);
				pstmt.setString(2, ControllerHostName);
				rs = pstmt.executeQuery();
				while (rs.next())
				{
					availableConnectors.add(rs.getInt(1));
					availableConnectorServerURLs.add(rs.getString(2));
				}
				
				connectorInstancesConfig = connectorInstancesConfig.replace("V{CONNECTOR_HOME}", connectorHome);
				File configFile = new File(connectorInstancesConfig);
				if (configFile.exists())
				{
					BufferedReader configFileReader = null;
					try
					{
						selectSQL = "SELECT Max(InstanceID) FROM " + CONNECTORS_SCHEMA + "." + CONNECTOR_INSTANCES;
						logger.debug(selectSQL);
						pstmt = conn.prepareStatement(selectSQL);
						rs = pstmt.executeQuery();
						int maxInstanceID = 0;
						if (rs.next())
						{
							maxInstanceID = rs.getInt(1);
						}
						configFileReader = new BufferedReader(new FileReader(configFile));
						String insertSQL = "INSERT INTO " + CONNECTORS_SCHEMA + "." + CONNECTOR_INSTANCES + " (InstanceID, ControllerID, ConnectorID, HostName, ConnectorServerURL, ConnectorApplication) VALUES(?,?,?,?,?,?)";
						logger.debug(insertSQL);
						pstmt = conn.prepareStatement(insertSQL);
						String line = null;
						int addedInstances = 0;
						while ((line = configFileReader.readLine()) != null)
						{
							if (line.startsWith("//"))
								continue;
							String[] connectorProperties = line.split("\t");
							if (connectorProperties != null && connectorProperties.length == 3)
							{
								if (!(availableConnectors.contains(Integer.parseInt(connectorProperties[0])) && availableConnectorServerURLs.contains(connectorProperties[1])))
								{
									pstmt.setInt(1, ++maxInstanceID);
									pstmt.setString(2, controllerID);
									pstmt.setInt(3, Integer.parseInt(connectorProperties[0]));
									pstmt.setString(4, ControllerHostName);
									pstmt.setString(5, connectorProperties[1]);
									pstmt.setString(6, connectorProperties[2]);
									pstmt.execute();
									addedInstances++;
								}
							}
							else
							{
								logger.error("Failed to parse connectorinstances.config : " + line);
							}
						}
						if (addedInstances > 0)
						{
							logger.debug(addedInstances + " connector instances added to " + CONNECTORS_SCHEMA + "." + CONNECTOR_INSTANCES);
						}
						logger.debug("successfully loaded connectorinstances.config");
					}
					catch (IOException ioe)
					{
						logger.error("problem loading connectedconnectors.config - " + ioe.getMessage(), ioe);
					}
					finally 
					{
						if (configFileReader != null)
						{
							try
							{
								configFileReader.close();
							}
							catch (IOException ex)
							{							
							}
						}
					}
				}
			}
			
			String connectorParametersConfig = context.getInitParameter("ConnectorParametersConfig");
			if (connectorParametersConfig != null && !connectorParametersConfig.isEmpty())
			{
				availableConnectors = new ArrayList<Integer>();
				List<String> availableConnectorParamName = new ArrayList<String>();
				selectSQL = "SELECT ConnectorID, ParameterName FROM " + CONNECTORS_SCHEMA + "." + CONNECTOR_PARAMETERS + " WHERE ControllerID=?";
				logger.debug(selectSQL);
				pstmt = conn.prepareStatement(selectSQL);
				pstmt.setString(1, controllerID);
				rs = pstmt.executeQuery();
				while (rs.next())
				{
					availableConnectors.add(rs.getInt(1));
					availableConnectorParamName.add(rs.getString(2));
				}
				
				connectorParametersConfig = connectorParametersConfig.replace("V{CONNECTOR_HOME}", connectorHome);
				File configFile = new File(connectorParametersConfig);
				if (configFile.exists())
				{
					BufferedReader configFileReader = null;
					try
					{
						selectSQL = "SELECT Max(ParameterID) FROM " + CONNECTORS_SCHEMA + "." + CONNECTOR_PARAMETERS;
						logger.debug(selectSQL);
						pstmt = conn.prepareStatement(selectSQL);
						rs = pstmt.executeQuery();
						int maxParamID = 0;
						if (rs.next())
						{
							maxParamID = rs.getInt(1);
						}
						configFileReader = new BufferedReader(new FileReader(configFile));
						String insertSQL = "INSERT INTO " + CONNECTORS_SCHEMA + "." + CONNECTOR_PARAMETERS + " (ParameterID, ControllerID, ConnectorID, ParameterName, ParameterValue) VALUES(?,?,?,?,?)";
						logger.debug(insertSQL);
						String line = null;
						int addedParameters = 0;
						while ((line = configFileReader.readLine()) != null)
						{
							if (line.startsWith("//"))
								continue;
							String[] connectorProperties = line.split("\t");
							if (connectorProperties != null && connectorProperties.length >= 2)
							{
								if (!(availableConnectors.contains(Integer.parseInt(connectorProperties[0])) && availableConnectorParamName.contains(connectorProperties[1])))
								{
									pstmt = conn.prepareStatement(insertSQL);
									pstmt.setInt(1, ++maxParamID);
									pstmt.setString(2, controllerID);
									pstmt.setInt(3, Integer.parseInt(connectorProperties[0]));
									pstmt.setString(4, connectorProperties[1]);
									pstmt.setString(5, (connectorProperties.length == 2 ? null : connectorProperties[2]));
									pstmt.execute();
									addedParameters++;
								}
							}
							else
							{
								logger.error("Failed to parse connectorparameters.config : " + line);
							}
						}
						if (addedParameters > 0)
						{
							logger.debug(addedParameters + " connector parameters added to " + CONNECTORS_SCHEMA + "." + CONNECTOR_PARAMETERS);
						}
						logger.debug("successfully loaded connectorparameters.config");
					}
					catch (IOException ioe)
					{
						logger.error("problem loading connectorparameters.config - " + ioe.getMessage(), ioe);
					}
					finally 
					{
						if (configFileReader != null)
						{
							try
							{
								configFileReader.close();
							}
							catch (IOException ex)
							{							
							}
						}
					}
				}
			}
			
			//reset IsActive field for all connectors
			String updateSQL = "UPDATE " + CONNECTORS_SCHEMA + "." + CONNECTOR_INSTANCES + " SET IsActive=0";
			logger.debug(updateSQL);
			stmt = conn.createStatement();
			stmt.executeUpdate(updateSQL);
			
			logger.debug("Connector database initialized successfully");		
		}
		catch (Exception ex)
		{
			logger.error("Problem initializing Connector database - " + ex.getMessage(), ex);
		}
		finally
		{
			if (rs != null)
			{
				try
				{
					rs.close();
				}
				catch (SQLException sqle)
				{
					logger.error("problem closing resultset - " + sqle.getMessage(), sqle);
				}
			}
			if (stmt != null)
			{
				try
				{
					stmt.close();
				}
				catch (SQLException sqle)
				{
					logger.error("problem closing statement - " + sqle.getMessage(), sqle);
				}
			}
			closePreparedStatement(pstmt);			
		}		
	}
	
	private static void cleanupTables(Connection conn) throws SQLException
	{
		PreparedStatement pstmt = null;
		try
		{
			int cnt = 0;
			String cleanupParamTableSQL = "DELETE FROM " + CONNECTORS_SCHEMA + "." + CONNECTOR_PARAMETERS + " WHERE ControllerID=?";
			logger.debug("Delete query for connector parameter table : " + cleanupParamTableSQL);
			pstmt = conn.prepareStatement(cleanupParamTableSQL);
			pstmt.setString(1, controllerID);
			cnt = pstmt.executeUpdate();
			logger.debug(cnt + " entries deleted from " + CONNECTORS_SCHEMA + "." + CONNECTOR_PARAMETERS);
			
			String cleanupInstancesTableSQL = "DELETE FROM " + CONNECTORS_SCHEMA + "." + CONNECTOR_INSTANCES + " WHERE ControllerID=? AND HostName=?";
			logger.debug("Delete query for connector instances table : " + cleanupInstancesTableSQL);
			pstmt = conn.prepareStatement(cleanupInstancesTableSQL);
			pstmt.setString(1, controllerID);
			pstmt.setString(2, ControllerHostName);
			cnt = pstmt.executeUpdate();
			logger.debug(cnt + " connector instances deleted from " + CONNECTORS_SCHEMA + "." + CONNECTOR_INSTANCES);
			
			String cleanupMasterTableSQL = "DELETE FROM " + CONNECTORS_SCHEMA + "." + CONNECTORS_MASTER + " WHERE ControllerID=?";
			logger.debug("Delete query for connector master table : " + cleanupMasterTableSQL);
			pstmt = conn.prepareStatement(cleanupMasterTableSQL);
			pstmt.setString(1, controllerID);
			cnt = pstmt.executeUpdate();
			logger.debug(cnt + " records deleted from " + CONNECTORS_SCHEMA + "." + CONNECTORS_MASTER);
		}
		finally
		{
			if (pstmt != null)
			{
				try
				{
					pstmt.close();
				}
				catch(SQLException ex)
				{
					logger.warn("Error while closing statement " + ex);
				}
			}
		}
	}
	
	private synchronized void updateConnectors()
	{
		//XXX ping each connector instance and update connector status in db (IsActive field)
	}
	
	public static void closePreparedStatement(PreparedStatement pstmt)
	{
		if (pstmt != null)
		{
			try
			{
				pstmt.close();
			} catch (SQLException e)
			{
				logger.warn("Exception during pstmt.close()", e);
			}
		}
	}
	
	private static Connection getConnection()
	{
		ConnectionPool cp = null;
		try
		{
			cp = ConnectionPool.getInstance(connectorDBDriver, connectorDBConnectString, connectorDBAdminUser, connectorDBAdminPwd, maxConnections, connectorDBType);
			return cp.getConnection();
		}
		catch (Exception ex)
		{
			logger.error(ex.getMessage(), ex);
		}
		return null;
	}
	
	public static List<Connector> getAllConnectors()
	{
		try
		{
			return getAllConnectors(getConnection());
		}
		catch (Exception ex)
		{
			logger.error(ex.getMessage(), ex);
		}
		return null;
	}
	
	private static List<Connector> getAllConnectors(Connection conn) throws SQLException
	{
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = conn.prepareStatement("SELECT ConnectorID, ConnectorName, ConnectorAPIVersion, ConnectionString, IsDefaultVersion FROM " + CONNECTORS_SCHEMA + "." + CONNECTORS_MASTER + 
				" WHERE ControllerID=? ORDER BY ConnectorName, IsDefaultVersion DESC, ConnectorAPIVersion");
			pstmt.setString(1, controllerID);
			rs = pstmt.executeQuery();
			List<Connector> connectors = new ArrayList<Connector>();
			while (rs.next())
			{
				Connector connector = new Connector();
				connector.setConnectorID(rs.getInt(1));
				connector.setConnectorName(rs.getString(2));
				connector.setConnectorAPIVersion(rs.getString(3));
				connector.setConnectionString(rs.getString(4));
				connector.setIsDefaultVersion(rs.getBoolean(5));
				connectors.add(connector);
			}
			return connectors;
		}
		finally
		{
			closePreparedStatement(pstmt);
		}
	}
	
	private static int getConnectorIDForConnectionString(String connectionString, Connection conn) throws SQLException
	{
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = conn.prepareStatement("SELECT ConnectorID FROM " + CONNECTORS_SCHEMA + "." + CONNECTORS_MASTER + 
				" WHERE ConnectionString=? AND ControllerID=?");
			pstmt.setString(1, connectionString);
			pstmt.setString(2, controllerID);
			rs = pstmt.executeQuery();
			if (rs.next())
			{
				return rs.getInt(1);
			}
			else
			{
				logger.error("Cannot find connector for connectionstring - " + connectionString);
			}
			return -1;
		}
		finally
		{
			closePreparedStatement(pstmt);
		}
	}
	
	public static List<ConnectorInstance> getInstancesForConnectorID(int connectorID, boolean getDisabledOrInActiveInstances)
	{
		try
		{
			return getInstancesForConnectorID(connectorID, getConnection(), getDisabledOrInActiveInstances);
		}
		catch (Exception ex)
		{
			logger.error(ex.getMessage(), ex);
		}
		return null;
	}
	
	private static List<ConnectorInstance> getInstancesForConnectorID(int connectorID, Connection conn, boolean getDisabledOrInActiveInstances) throws SQLException
	{
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = conn.prepareStatement("SELECT InstanceID, ConnectorServerURL, ConnectorApplication FROM " + CONNECTORS_SCHEMA + "." + CONNECTOR_INSTANCES + 
					" WHERE ConnectorID=? AND ControllerID=? AND HostName=?" + (getDisabledOrInActiveInstances ? "" : " AND (Disabled=? OR Disabled IS NULL)"));//XXX AND isActive=1
			pstmt.setInt(1, connectorID);
			pstmt.setString(2, controllerID);
			pstmt.setString(3, ControllerHostName);
			if (!getDisabledOrInActiveInstances)
			{
				pstmt.setBoolean(4, false);
				//XXX pstmt.setBoolean(5, false);//isActive
			}
			rs = pstmt.executeQuery();
			List<ConnectorInstance> connectorInstances = new ArrayList<ConnectorInstance>();
			while (rs.next())
			{
				ConnectorInstance instance = new ConnectorInstance();
				instance.setInstanceID(rs.getInt(1));
				instance.setConnectorServerURL(rs.getString(2));
				instance.setConnectorApplication(rs.getString(3));
				connectorInstances.add(instance);
			}
			return connectorInstances;
		}
		finally
		{
			closePreparedStatement(pstmt);
		}
	}
	
	public static ConnectorInstance getConnectorInstance(String connectionString)
	{
		ResultSet rs = null;
		try
		{
			int connectorID = getConnectorIDForConnectionString(connectionString, getConnection());
			if (connectorID == -1)
			{
				return null;
			}
			List<ConnectorInstance> instances = getInstancesForConnectorID(connectorID, getConnection(), false);
			if (instances.isEmpty())
			{
				return null;
			}
			return instances.get(0);//XXX use load balancing logic to figure out which connector to use, use ConnectorActivity table to figure out connector with least load at given time
		}
		catch (Exception ex)
		{
			logger.error(ex.getMessage(), ex);
		}
		finally
		{
			if (rs != null)
			{
				try
				{
					rs.close();
				}
				catch (SQLException sqle)
				{
					logger.error("problem closing resultset - " + sqle.getMessage(), sqle);
				}
			}						
		}
		return null;
	}
	
	private String getParameter(String parameterName, Properties configProperties, ServletContext context)
	{
		if (parameterName == null)
			return null;
		if (configProperties.containsKey(parameterName))
			return configProperties.getProperty(parameterName);
		return context.getInitParameter(parameterName);
	}

}
