package com.birst.connectors.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.birst.connectors.controller.utils.ConnectionPool;
import com.birst.connectors.controller.utils.ConnectorUtil;

/**
 * Servlet implementation class LogConnectorActivity
 */
public class LogConnectorActivity extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(LogConnectorActivity.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LogConnectorActivity() 
    {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		response.setContentType("text/xml;charset=UTF-8");
	    response.setCharacterEncoding("UTF-8");
		PrintWriter pw = response.getWriter();
	    String output = null;
	    String errorMessage = null;
	    ConnectionPool cp = null;
	    Connection conn = null;
	    PreparedStatement pstmt = null;
	    try {
	    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	    	String connectorString = request.getParameter("ConnectorString");
	    	cp = ConnectionPool.getInstance(ConnectorListener.connectorDBDriver, ConnectorListener.connectorDBConnectString, ConnectorListener.connectorDBAdminUser, ConnectorListener.connectorDBAdminPwd, ConnectorListener.maxConnections, ConnectorListener.connectorDBType);
	    	conn = cp.getConnection();
	    	String getInstanceSQL = "SELECT InstanceID FROM " + ConnectorListener.CONNECTORS_SCHEMA + "." + ConnectorListener.CONNECTOR_INSTANCES  + 
	    			" WHERE ConnectorID = " + 
	    			" (SELECT ConnectorID FROM " + ConnectorListener.CONNECTORS_SCHEMA + "." + ConnectorListener.CONNECTORS_MASTER +
	    			" WHERE ConnectionString=? AND ControllerID=?)" + 
	    			" AND HostName=? AND ControllerID=?";
	    	pstmt = conn.prepareStatement(getInstanceSQL);
	    	pstmt.setString(1, connectorString);
	    	pstmt.setString(2, ConnectorListener.controllerID);
	    	pstmt.setString(3, ConnectorListener.ControllerHostName);
	    	pstmt.setString(4, ConnectorListener.controllerID);
	    	logger.debug(getInstanceSQL);
	    	ResultSet rs = pstmt.executeQuery();
	    	int instanceID = -1;
	    	if (rs.next())
	    	{
	    		instanceID = rs.getInt(1);
	    		if (rs.next())
	    		{
	    			logger.warn("Multiple InstanceIDs found running on same Host with same ControllerID for Connector, using first InstanceID found to log Activity into Database:" + instanceID);
	    		}
	    		String statusStr = request.getParameter("Status");
	    		int status = ConnectorUtil.Status.inProgress.toString().equals(statusStr) ? 1 : ConnectorUtil.Status.failed.toString().equals(statusStr) ? 3 : 2;
	    		String requestType = request.getParameter("RequestType");
	    		String spaceID = request.getParameter("SpaceID");
	    		String client = request.getParameter("Client");
	    		String uniqueID = request.getParameter("UniqueID");
	    		String activityIDStr = request.getParameter("ActivityID");
	    		int activityID = -1;
	    		if (activityIDStr != null && activityIDStr.length() > 0)
	    		{
	    			activityID = Integer.parseInt(activityIDStr);
	    		}
	    		Timestamp startTime = new java.sql.Timestamp(sdf.parse(request.getParameter("StartTime")).getTime());
	    		String finishTimeStr = request.getParameter("FinishTime");
	    		Timestamp finishTime = null;
	    		if (finishTimeStr != null && finishTimeStr.length() > 0)
	    		{
	    			finishTime = new java.sql.Timestamp(sdf.parse(finishTimeStr).getTime());
	    		}
	    		if (ConnectorUtil.Status.inProgress.toString().equals(statusStr))//insert
	    		{
	    			
	    			String selectSQL = "SELECT Max(ID) FROM " + ConnectorListener.CONNECTORS_SCHEMA + "." + ConnectorListener.CONNECTOR_ACTIVITY;
					logger.debug(selectSQL);
					pstmt = conn.prepareStatement(selectSQL);
					rs = pstmt.executeQuery();
					int maxID = 0;
					if (rs.next())
					{
						maxID = rs.getInt(1);
					}
					//Insert data
	    			String insertSQL = "INSERT INTO " + ConnectorListener.CONNECTORS_SCHEMA + "." + ConnectorListener.CONNECTOR_ACTIVITY +
	    					  " (ID, InstanceID, Client, SpaceID, UniqueID, RequestType, Status, StartTime) VALUES(?,?,?,?,?,?,?,?)";
		    		logger.debug(insertSQL);
		    		pstmt = conn.prepareStatement(insertSQL);
		    		pstmt.setInt(1, ++maxID);
		    		pstmt.setInt(2, instanceID);
					pstmt.setString(3, client);
					pstmt.setString(4, spaceID);
					pstmt.setString(5, uniqueID);
					pstmt.setString(6, requestType);
					pstmt.setInt(7, status);
					pstmt.setTimestamp(8, startTime);
					int rows = pstmt.executeUpdate();
					if (rows > 0)
					{
						output = String.valueOf(maxID);
						logger.debug("Inserted connector activity log successfully. InstanceID " + instanceID + ", ConnectorString " + connectorString + ", ControllerID " + ConnectorListener.controllerID + ", SpaceID " + spaceID + ", UniqueID" + uniqueID);
					}
					else
					{
						output = String.valueOf(-1);
						logger.debug("Insert connector activity log is unsuccessful. InstanceID " + instanceID + ", ConnectorString " + connectorString + ", ControllerID " + ConnectorListener.controllerID + ", SpaceID " + spaceID + ", UniqueID" + uniqueID);
					}
	    		}
	    		else if (activityID != -1)//update
	    		{
	    			String updateSQL = "UPDATE " + ConnectorListener.CONNECTORS_SCHEMA + "." + ConnectorListener.CONNECTOR_ACTIVITY +
	    					  " SET Status=?, FinishTime=? WHERE ID=?";
	    					  
		    		logger.debug(updateSQL);
		    		pstmt = conn.prepareStatement(updateSQL);
		    		pstmt.setInt(1, status);
		    		pstmt.setTimestamp(2, finishTime);
		    		pstmt.setInt(3, activityID);
					int rows = pstmt.executeUpdate();
					if (rows > 0)
					{
						output = String.valueOf(rows);
						logger.debug("Updated connector activity log successfully. InstanceID " + instanceID + ", ConnectorString " + connectorString + ", ControllerID " + ConnectorListener.controllerID + ", SpaceID " + spaceID + ", UniqueID" + uniqueID);
					}
					else
					{
						output = String.valueOf(-1);
						logger.debug("Update connector activity log is unsuccessful. InstanceID " + instanceID + ", ConnectorString " + connectorString + ", ControllerID " + ConnectorListener.controllerID + ", SpaceID " + spaceID + ", UniqueID" + uniqueID);
					}
	    		}
	    		else
	    		{
	    			output = String.valueOf(-1);
					logger.debug("Can not insert or update activity for current status: " + statusStr + " and activityID " + activityID);
	    		}
	    	}
	    	else
	    	{
	    		output = String.valueOf(-1);
	    		logger.warn("Not able to log connector activity because connector instance not found for connectString:" + connectorString + " and controllerID: " + ConnectorListener.controllerID);
	    	}
	    }
	    catch (Exception e)
	    {
	    	logger.error(e.getMessage(), e);
	    	errorMessage = e.getMessage();
	    }
	    finally 
	    {
	    	ConnectorListener.closePreparedStatement(pstmt);
	    }
	    StringBuilder sb = new StringBuilder();
	    sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
	    if (output == null)
	    {
	    	sb.append("<Root>");
	    	sb.append("<Success>false</Success>");
	    	sb.append("<Reason>").append(errorMessage).append("</Reason>");
	    	sb.append("</Root>");
	    }
	   else
	    {
		   sb.append("<Root>");
		   sb.append("<Success>true</Success>");
		   sb.append("<Result>").append(output).append("</Result>");
		   sb.append("</Root>");
	    }
	    pw.write(sb.toString());
	    pw.flush();
	}
}
