package com.birst.connectors.controller;


/**
 * 
 * @author mpandit
 *
 */
public class ConnectorInstance {
	private int instanceID;
	private String connectorServerURL;
	private String connectorApplication;
	
	public int getInstanceID() {
		return instanceID;
	}
	public void setInstanceID(int instanceID) {
		this.instanceID = instanceID;
	}
	public String getConnectorServerURL() {
		return connectorServerURL;
	}
	public void setConnectorServerURL(String connectorServerURL) {
		this.connectorServerURL = connectorServerURL;
	}
	public String getConnectorApplication() {
		return connectorApplication;
	}
	public void setConnectorApplication(String connectorApplication) {
		this.connectorApplication = connectorApplication;
	}
}
