package com.birst.connectors.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.net.URLConnection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;

public class ConnectorHealth extends HttpServlet {
	// Member vars.
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(ConnectorHealth.class);
	private static final String NODE_CONNECTORS_ROOT = "Connectors";
	private static final String NODE_CONNECTOR_ROOT = "Connector";
	private static final String NODE_CONNECTED_CONTROLLER_ID = "ConnectedConnectorId";
	private static final String NODE_CONNECTOR_SERVER_URL = "ConnectorServerURL";
	private static final String NODE_CONNECTOR_APPLICATION = "ConnectorApplication";
	private static final String NODE_AVAILABLE = "Available";
	private static final String NODE_STATUS = "Status";
	private static final String EMPTY_STRING = "";
	private static final String SPACE = " ";
	private static final String EQUALS = "=";
	private static final String QUESTION_MARK = "?";
	private static final String SLASH = "/";
	private static final String QUOTE = "\"";
	private static final String COLON = ":";
	private static final String ENCODING_VALUE = "UTF-8";
	private static final String VERSION_VALUE = "1.0";
	private static final String INPUT_ID = "id";
	private static final String HOST = "host";
	private static final String PORT = "port";
	private static final String HTTP_SPLIT_TOKEN = "://";
	private static final String RESPONSE_TYPE = "xml";
	private static final String VERSION_NAME = "version";
	private static final String ENCODING_NAME = "encoding";
	private static final String RESPONSE_OPEN = "<?";
	private static final String RESPONSE_CLOSE = "?>";
	private static final String SUCCESS = "SUCCESS";
	private static final String FAILURE = "FAILURE";
	private static final String ICONNECTOR_SERVLET = "IConnectorServlet";
	private static final String METHOD_PARAMETER = "Method";
	private static final String METHOD = "ping";
	private static final String TEST_PORT = "testPort";
	private static final String SUCCESS_CHECK = "<Success>true</Success>";
	private static final String PING_EXCEPTION = "Ping failed for : %s";
	
	/*** Post method. ***/
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	/*** Get method. ***/
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		// Get input params.
		String inputIdStr = request.getParameter(INPUT_ID);
		String testPortStr = request.getParameter(TEST_PORT);
		int inputId = ((inputIdStr != null && !inputIdStr.trim().equals(EMPTY_STRING)) ? Integer.valueOf(inputIdStr) : 1);
		
		// Build output.
		OMNamespace namespace = null;
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement root = factory.createOMElement(NODE_CONNECTORS_ROOT, namespace);
		
		// Loop over connector properties.
		for (Connector conn : ConnectorListener.getAllConnectors()) {
			// Build output nodes.
			List<ConnectorInstance> connectedConnectors = ConnectorListener.getInstancesForConnectorID(conn.getConnectorID(), true);
			OMElement connNode = factory.createOMElement(NODE_CONNECTOR_ROOT, namespace);
			OMElement connApp = factory.createOMElement(NODE_CONNECTOR_APPLICATION, namespace);
			OMElement connId = factory.createOMElement(NODE_CONNECTED_CONTROLLER_ID, namespace);
			OMElement connHost = factory.createOMElement(NODE_CONNECTOR_SERVER_URL, namespace);
			OMElement connAvailable = factory.createOMElement(NODE_AVAILABLE, namespace);
			OMElement connStatus = factory.createOMElement(NODE_STATUS, namespace);
			
			// Perform health check.
			Socket socket = null;
			SocketAddress socketAddress;
			Map<String, String> parsedUrlMap;
			int id;
			String url;
			String pingUrl;
			String connectorApplication;
			URL pingServletUrl;
			URLConnection connection;
			InputStreamReader inputReader = null;
			BufferedReader bufferedReader = null;
			boolean isSuccess = false;
			String line;
			for (ConnectorInstance connector : connectedConnectors) {
				id = connector.getInstanceID();
				if (inputId == id || inputId == 0) {
					// Get connector meta-data.
					url = connector.getConnectorServerURL();
					connectorApplication = connector.getConnectorApplication();
					connId.setText(String.valueOf(id));
					connApp.setText(connectorApplication);
					connHost.setText(url);

					try {
						parsedUrlMap = parseConnectorUrl(url);
						if (testPortStr != null && testPortStr.trim().length() > 0 && Boolean.getBoolean(testPortStr)) {
							// Build and test socket connection.
							socketAddress = new InetSocketAddress(InetAddress.getByName(parsedUrlMap.get(HOST)), 
									Integer.parseInt(parsedUrlMap.get(PORT)));
							socket = new Socket(); 
							socket.connect(socketAddress);  
							connAvailable.setText(Boolean.TRUE.toString());
							connStatus.setText(SUCCESS);
						} else {
							// Build ping servlet.
							pingUrl = buildPingUrl(url, connectorApplication);
							pingServletUrl = new URL(pingUrl);
							connection = pingServletUrl.openConnection();
					        connection.setDoOutput(true);
					        connection.connect();
					        inputReader = new InputStreamReader((InputStream) connection.getContent());
					        bufferedReader = new BufferedReader(inputReader);

					        // Check for success status.
					        while ((line = bufferedReader.readLine()) != null) {
					        	if (line.contains(SUCCESS_CHECK)) {
					        		connAvailable.setText(Boolean.TRUE.toString());
									connStatus.setText(SUCCESS);
									isSuccess = true;
									break;
					        	}
					        }
					        if (!isSuccess) {
					        	throw new Exception(String.format(PING_EXCEPTION, pingUrl));
					        }
						}
					} catch (Exception ex) {
						// Set health test result to false.
						connAvailable.setText(Boolean.FALSE.toString());
						connStatus.setText(FAILURE);
						logger.error(ex, ex);
					} finally {
						// Clean-up.
						if (socket != null) {
							socket.close();
						}
						if (inputReader != null) {
							inputReader.close();
						}
						if (bufferedReader != null) {
							bufferedReader.close();
						}
					}
					
					// Add to parent element.
					connNode.addChild(connId);
					connNode.addChild(connApp);
					connNode.addChild(connHost);
					connNode.addChild(connAvailable);
					connNode.addChild(connStatus);
					
					// Exit conditions.
					if (inputId == id) {
						break;
					}
				}

				// Exit conditions.
				if (inputId == id) {
					break;
				}
			}
			
			// Add to root node, if node is not empty.
			if (connNode.getChildren().hasNext()) {
				root.addChild(connNode);
			}
		}
		
		// Write output.
		response.setContentType("text/xml;charset=UTF-8");
	    response.setCharacterEncoding("UTF-8");
		PrintWriter writer = response.getWriter();
		try {
			
			writer.write(getResponseStart());
			XMLStreamWriter streamWriter = XMLOutputFactory.newInstance().createXMLStreamWriter(writer);
			root.serialize(streamWriter);
			writer.flush();
		} catch (XMLStreamException ex) {
			logger.error(ex, ex);
		} finally {
			if (writer != null) {
				writer.close();
			}
		}
	}
	
	/*** Helper methods. ***/
	private String buildPingUrl(String url, String connectorApplication) {
		StringBuilder sb = new StringBuilder();
		sb.append(url).append(SLASH).append(connectorApplication).append(SLASH).append(ICONNECTOR_SERVLET).
			append(QUESTION_MARK).append(METHOD_PARAMETER).append(EQUALS).append(METHOD);
		return sb.toString();
	}
	
	private String getResponseStart() {
		// Build response start.
		StringBuilder sb = new StringBuilder();
		sb.append(RESPONSE_OPEN).append(RESPONSE_TYPE).append(SPACE).append(VERSION_NAME).append(EQUALS).append(QUOTE).append(VERSION_VALUE).
			append(QUOTE).append(SPACE).append(ENCODING_NAME).append(EQUALS).append(QUOTE).append(ENCODING_VALUE).append(QUOTE).append(RESPONSE_CLOSE);
		return sb.toString();
	}
	
	private Map<String, String> parseConnectorUrl(String url) {
		// Parse url to retrieve host and port.
		Map<String, String> parsedUrlMap = new HashMap<String, String>();
		String[] urlTokensNoHttp = url.split(HTTP_SPLIT_TOKEN);
		String[] urlTokens = urlTokensNoHttp[1].split(COLON);
		parsedUrlMap.put(HOST, urlTokens[0]);
		parsedUrlMap.put(PORT, urlTokens[1]);
		return parsedUrlMap;
	}
}