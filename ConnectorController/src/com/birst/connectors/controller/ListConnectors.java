package com.birst.connectors.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;


/**
 * Servlet implementation class ConnectorController
 * @author mpandit
 */
public class ListConnectors extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(ListConnectors.class);
	public static final String NODE_CONNECTORS_ROOT = "Connectors";
	public static final String NODE_CONNECTOR_CHILD = "Connector";
	public static final String NODE_CONNECTOR_NAME = "ConnectorName";
	public static final String NODE_CONNECTOR_VERSION = "ConnectorVersion";
	public static final String NODE_CONNECTOR_CONNECTIONSTRING = "ConnectorConnectionString";
	public static final String NODE_CONNECTOR_IS_DEFAULT_VERSION = "IsDefaultAPIVersion";
	public static final String NODE_CONNECTOR_SERVER_URL = "ConnectorServerURL";
	public static final String NODE_CONNECTOR_APPLICATION = "ConnectorApplication";
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OMNamespace ns = null;
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement root = factory.createOMElement(NODE_CONNECTORS_ROOT, ns);
		String connectionString = request.getParameter("ConnectionString");
		if (connectionString != null && !connectionString.isEmpty())
		{
			//validate connectionstring and provide url for connector
			ConnectorInstance ccn = ConnectorListener.getConnectorInstance(connectionString);
			if (ccn != null)
			{
				OMElement connChild = factory.createOMElement(NODE_CONNECTOR_CHILD, ns);
				OMElement connURLEl = factory.createOMElement(NODE_CONNECTOR_SERVER_URL, ns);
				connURLEl.setText(ccn.getConnectorServerURL());
				connChild.addChild(connURLEl);
				OMElement connServletEl = factory.createOMElement(NODE_CONNECTOR_APPLICATION, ns);
				connServletEl.setText(ccn.getConnectorApplication());
				connChild.addChild(connServletEl);
				root.addChild(connChild);
			}
			else
			{
				//no active connector found response
			}
		}
		else
		{
			for (Connector conn : ConnectorListener.getAllConnectors())
			{
				OMElement connChild = factory.createOMElement(NODE_CONNECTOR_CHILD, ns);
				OMElement connNameEl = factory.createOMElement(NODE_CONNECTOR_NAME, ns);
				connNameEl.setText(conn.getConnectorName());
				connChild.addChild(connNameEl);
				OMElement connVersionEl = factory.createOMElement(NODE_CONNECTOR_VERSION, ns);
				connVersionEl.setText(conn.getConnectorAPIVersion());
				connChild.addChild(connVersionEl);
				OMElement connConnStringEl = factory.createOMElement(NODE_CONNECTOR_CONNECTIONSTRING, ns);
				connConnStringEl.setText(conn.getConnectionString());
				connChild.addChild(connConnStringEl);
				OMElement connIsDefaultEl = factory.createOMElement(NODE_CONNECTOR_IS_DEFAULT_VERSION, ns);
				connIsDefaultEl.setText(String.valueOf(conn.isDefaultVersion()));
				connChild.addChild(connIsDefaultEl);
				root.addChild(connChild);
			}			
		}
		OutputStream pw = response.getOutputStream();
		try {
			response.setContentType("text/xml;charset=UTF-8");
			pw.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>".getBytes("UTF-8"));
			XMLStreamWriter writer = XMLOutputFactory.newInstance().createXMLStreamWriter(pw, "UTF-8");
			root.serialize(writer);
			writer.flush();
		} catch (XMLStreamException e) {
			logger.error(e, e);
		}
	}

}
