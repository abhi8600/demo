define( function( require, exports, module ) {
	'use strict';
	var _ = require( 'lodash' );
	var $ = require( 'jquery' );
	var app = require( 'app' );

	//Keep track of all instantiated views for cleanup
	window.activeViews = app.activeViews = app.activeViews || {};

	var View = function( options ) {
		var that = this;
		this.viewRendered = new $.Deferred();
		this.cid = _.uniqueId( 'view' );
		this._namespace = '.' + this.cid;
		options || ( options = {} );
		_.extend( this, _.pick( options, viewOptions ) );
		this._ensureElement();
		this._bindElement();
		this.initialize.apply( this, arguments );
		if( this.autoRender ) {
			this.render();
		}
		this.viewRendered.done( function() {
			app.activeViews[ that.cid ] = that;
		} );
	};

	//Options that are extendable
	var viewOptions = [ 'template', 'el', 'tagName', 'id', 'attributes', 'className', 'model', 'collection', 'autoRender', 'renderFunction' ];

	_.extend( View.prototype, {

		_type : 'view',
		// Sometimes one will need to control when to render
		autoRender : true,

		template : null,

		el : null,

		// el must be enclosed
		useTemplateEl : true,

		events : {},

		tagName : 'div',

		model : {},

		collection : [],

		_globalEvents : [],

		//For custom render functionality
		renderFunction : null,

		initialize : function() {},

		setElement : function( element ) {
			if( !_.isElement( element ) && !_.isString( element ) ) {
				this.$el = element;
			} else {
				this.$el = $( element );
			}
			this.el = this.$el[ 0 ];
			return this;
		},

		_ensureElement : function() {
			if ( !this.el ) {
				var attrs = _.extend( {}, _.result( this, 'attributes' ) );
				var $el;
				if ( this.id ) attrs.id = _.result( this, 'id' );
				if ( this.className ) attrs[ 'class' ] = _.result( this, 'className' );
				if( this.useTemplateEl && this.template ) {
					$el = this.template( this.model );
				} else {
					$el = $( '<' + _.result(this, 'tagName' ) + '>').attr( attrs );
				}
				this.setElement( $el, false );
			} else {
				this.setElement( _.result( this, 'el' ) );
			}
		},

		_getEventParams : function( evString ) {
			var params = evString.split( ' ' );
			var delegate = null;
			if( params.length > 2 ) {
				delegate = params.splice( 1, params.length ).join( ' ' );
			} else if( params.length === 2 ) {
				delegate = params[1];
			}
			return {
				eventType : params[0],
				eventDelegate : delegate
			};
		},

		_bindElement : function() {
			_.each( this.events, function( value, key ) {
				var func = _.result( this.events, key );
				if( _.isFunction( this[ func ] ) ) {
					var params = this._getEventParams( key );
					var eventType = params.eventType + this._namespace;
					if( params.eventDelegate ) {
						// allow window bindings
						if( params.eventDelegate === 'window' ) {
							$( window ).on( eventType, $.proxy( this[ func ], this ) );
						} else {
							this.$el.on( eventType, params.eventDelegate, $.proxy( this[ func ], this ) );
						}
					} else {
						this.$el.on( eventType, $.proxy( this[ func ], this ) );
					}
				}
			}, this );
		},

		_unbindElement : function() {
			this.$el.off( this._namespace );
			$( window ).off( this._namespace );
		},

		beforeRender : function(){},

		afterRender : function(){},

		remove : function() {
			this._unbindElement();
			if( this.rv ) this.rv.unbind();
			this.$el.remove();
		},

		supportsDragImage : function() {
			var testVar = window.DataTransfer || window.Clipboard;
			return 'setDragImage' in testVar.prototype;
		},

		render : function(){
			this.beforeRender();
			this.$el.trigger( 'beforeRender' );
			if( this.renderFunction !== null && _.isFunction( this.renderFunction ) ){
				this.renderFunction.apply( this, arguments );
			} else if( !this.useTemplateEl ) {
				if( _.isFunction( this.template ) ) this.$el.html( this.template( this.model ) );
			}
			this.afterRender();
			this.$el.trigger( 'afterRender' + this._namespace );
			this.viewRendered.resolve();
		}

	} );

	module.exports = View;

} );
