define( function( require, exports, module ) {
	'use strict';
	var crossroads = require( 'crossroads' );
	var hasher = require( 'hasher' );
	var _ = require( 'lodash' );
	var app = require( 'app' );

	var Router = function( options ) {
		options || ( options = {} );
		if (options.routes) { this.routes = options.routes; }
		crossroads.ignoreState = true;
		this.initialize.apply( this, arguments );
		this.addRouteActions();
		hasher.changed.add( this.parseHash ); //add hash change listener
		hasher.initialized.add( this.parseHash ); //add initialized listener (to grab initial value in case it is already set)
		hasher.init();
	};

	_.extend( Router.prototype, {

		initialize : function() {},

		addRouteActions : function() {
			_.each( this.routes, function( route, key ) {
				var route1 = crossroads.addRoute( key, _.bind( this[ route ], this ) );
			}, this );
		},

		parseHash : function( newHash, oldHash ) {
			crossroads.parse( newHash );
		}

	} );

	module.exports = Router;

} );
