(function(){function e(a) {
  throw a;
}
var h = void 0, j = !0, p = null, q = !1;
function aa() {
  return function(a) {
    return a
  }
}
function s() {
  return function() {
  }
}
function t(a) {
  return function(b) {
    this[a] = b
  }
}
function u(a) {
  return function() {
    return this[a]
  }
}
function x(a) {
  return function() {
    return a
  }
}
var z, A = A || {};
A.global = this;
A.dB = q;
A.dP = "en";
A.RR = function(a) {
  A.dI(a)
};
A.cS = function(a) {
  A.dB || (a = a || "", e(Error("Importing test-only code into non-debug environment" + a ? ": " + a : ".")))
};
A.dI = function(a, b, c) {
  a = a.split(".");
  c = c || A.global;
  !(a[0] in c) && c.execScript && c.execScript("var " + a[0]);
  for(var d;a.length && (d = a.shift());) {
    !a.length && A.To(b) ? c[d] = b : c = c[d] ? c[d] : c[d] = {}
  }
};
A.EM = function() {
  for(var a = ["window", "event"], b = A.global, c;c = a.shift();) {
    if(A.gN(b[c])) {
      b = b[c]
    }else {
      return p
    }
  }
  return b
};
A.UQ = function(a, b) {
  var c = b || A.global, d;
  for(d in a) {
    c[d] = a[d]
  }
};
A.GP = s();
A.OO = j;
A.WR = s();
A.UP = "";
A.XJ = s();
A.ZQ = aa();
A.xa = function() {
  e(Error("unimplemented abstract method"))
};
A.HP = function(a) {
  a.IQ = function() {
    return a.YM || (a.YM = new a)
  }
};
A.Gj = function(a) {
  var b = typeof a;
  if("object" == b) {
    if(a) {
      if(a instanceof Array) {
        return"array"
      }
      if(a instanceof Object) {
        return b
      }
      var c = Object.prototype.toString.call(a);
      if("[object Window]" == c) {
        return"object"
      }
      if("[object Array]" == c || "number" == typeof a.length && "undefined" != typeof a.splice && "undefined" != typeof a.propertyIsEnumerable && !a.propertyIsEnumerable("splice")) {
        return"array"
      }
      if("[object Function]" == c || "undefined" != typeof a.call && "undefined" != typeof a.propertyIsEnumerable && !a.propertyIsEnumerable("call")) {
        return"function"
      }
    }else {
      return"null"
    }
  }else {
    if("function" == b && "undefined" == typeof a.call) {
      return"object"
    }
  }
  return b
};
A.To = function(a) {
  return a !== h
};
A.wR = function(a) {
  return a === p
};
A.gN = function(a) {
  return a != p
};
A.isArray = function(a) {
  return"array" == A.Gj(a)
};
A.$e = function(a) {
  var b = A.Gj(a);
  return"array" == b || "object" == b && "number" == typeof a.length
};
A.uR = function(a) {
  return A.hn(a) && "function" == typeof a.getFullYear
};
A.Qc = function(a) {
  return"string" == typeof a
};
A.fN = function(a) {
  return"boolean" == typeof a
};
A.mJ = function(a) {
  return"number" == typeof a
};
A.Dy = function(a) {
  return"function" == A.Gj(a)
};
A.hn = function(a) {
  var b = typeof a;
  return"object" == b && a != p || "function" == b
};
A.Um = function(a) {
  return a[A.qw] || (a[A.qw] = ++A.gO)
};
A.ON = function(a) {
  "removeAttribute" in a && a.removeAttribute(A.qw);
  try {
    delete a[A.qw]
  }catch(b) {
  }
};
A.qw = "closure_uid_" + Math.floor(2147483648 * Math.random()).toString(36);
A.gO = 0;
A.GQ = A.Um;
A.UR = A.ON;
A.EL = function(a) {
  var b = A.Gj(a);
  if("object" == b || "array" == b) {
    if(a.Ca) {
      return a.Ca()
    }
    var b = "array" == b ? [] : {}, c;
    for(c in a) {
      b[c] = A.EL(a[c])
    }
    return b
  }
  return a
};
A.AL = function(a, b, c) {
  return a.call.apply(a.bind, arguments)
};
A.zL = function(a, b, c) {
  a || e(Error());
  if(2 < arguments.length) {
    var d = Array.prototype.slice.call(arguments, 2);
    return function() {
      var c = Array.prototype.slice.call(arguments);
      Array.prototype.unshift.apply(c, d);
      return a.apply(b, c)
    }
  }
  return function() {
    return a.apply(b, arguments)
  }
};
A.bind = function(a, b, c) {
  A.bind = Function.prototype.bind && -1 != Function.prototype.bind.toString().indexOf("native code") ? A.AL : A.zL;
  return A.bind.apply(p, arguments)
};
A.fK = function(a, b) {
  var c = Array.prototype.slice.call(arguments, 1);
  return function() {
    var b = Array.prototype.slice.call(arguments);
    b.unshift.apply(b, c);
    return a.apply(this, b)
  }
};
A.FR = function(a, b) {
  for(var c in b) {
    a[c] = b[c]
  }
};
A.now = Date.now || function() {
  return+new Date
};
A.TQ = function(a) {
  if(A.global.execScript) {
    A.global.execScript(a, "JavaScript")
  }else {
    if(A.global.eval) {
      if(A.Dx == p && (A.global.eval("var _et_ = 1;"), "undefined" != typeof A.global._et_ ? (delete A.global._et_, A.Dx = j) : A.Dx = q), A.Dx) {
        A.global.eval(a)
      }else {
        var b = A.global.document, c = b.createElement("script");
        c.type = "text/javascript";
        c.defer = q;
        c.appendChild(b.createTextNode(a));
        b.body.appendChild(c);
        b.body.removeChild(c)
      }
    }else {
      e(Error("goog.globalEval not available"))
    }
  }
};
A.Dx = p;
A.FQ = function(a, b) {
  function c(a) {
    return A.zH[a] || a
  }
  var d;
  d = A.zH ? "BY_WHOLE" == A.NL ? c : function(a) {
    for(var a = a.split("-"), b = [], d = 0;d < a.length;d++) {
      b.push(c(a[d]))
    }
    return b.join("-")
  } : aa();
  return b ? a + "-" + d(b) : d(a)
};
A.ZR = function(a, b) {
  A.zH = a;
  A.NL = b
};
A.LQ = function(a, b) {
  var c = b || {}, d;
  for(d in c) {
    var f = ("" + c[d]).replace(/\$/g, "$$$$"), a = a.replace(RegExp("\\{\\$" + d + "\\}", "gi"), f)
  }
  return a
};
A.Gg = function(a, b) {
  A.dI(a, b, h)
};
A.W = function(a, b, c) {
  a[b] = c
};
A.e = function(a, b) {
  function c() {
  }
  c.prototype = b.prototype;
  a.f = b.prototype;
  a.prototype = new c;
  a.prototype.constructor = a
};
A.TP = function(a, b, c) {
  var d = arguments.callee.caller;
  if(d.f) {
    return d.f.constructor.apply(a, Array.prototype.slice.call(arguments, 1))
  }
  for(var f = Array.prototype.slice.call(arguments, 2), g = q, k = a.constructor;k;k = k.f && k.f.constructor) {
    if(k.prototype[b] === d) {
      g = j
    }else {
      if(g) {
        return k.prototype[b].apply(a, f)
      }
    }
  }
  if(a[b] === d) {
    return a.constructor.prototype[b].apply(a, f)
  }
  e(Error("goog.base called from a method of one name to a method of a different name"))
};
A.scope = function(a) {
  a.call(A.global)
};
A.debug = {};
function ba(a) {
  this.stack = Error().stack || "";
  a && (this.message = "" + a)
}
A.e(ba, Error);
ba.prototype.name = "CustomError";
A.U = {};
A.U.aO = function(a) {
  return 0 == a.lastIndexOf("/", 0)
};
A.U.gM = function(a) {
  var b = a.length - 1;
  return 0 <= b && a.indexOf("/", b) == b
};
A.U.dQ = function(a, b) {
  return 0 == A.U.$G(b, a.substr(0, b.length))
};
A.U.cQ = function(a, b) {
  return 0 == A.U.$G(b, a.substr(a.length - b.length, b.length))
};
A.U.bO = function(a, b) {
  for(var c = 1;c < arguments.length;c++) {
    var d = ("" + arguments[c]).replace(/\$/g, "$$$$"), a = a.replace(/\%s/, d)
  }
  return a
};
A.U.hQ = function(a) {
  return a.replace(/[\s\xa0]+/g, " ").replace(/^\s+|\s+$/g, "")
};
A.U.ri = function(a) {
  return/^[\s\xa0]*$/.test(a)
};
A.U.vR = function(a) {
  return A.U.ri(A.U.qN(a))
};
A.U.tR = function(a) {
  return!/[^\t\n\r ]/.test(a)
};
A.U.rR = function(a) {
  return!/[^a-zA-Z]/.test(a)
};
A.U.xR = function(a) {
  return!/[^0-9]/.test(a)
};
A.U.sR = function(a) {
  return!/[^a-zA-Z0-9]/.test(a)
};
A.U.zR = function(a) {
  return" " == a
};
A.U.AR = function(a) {
  return 1 == a.length && " " <= a && "~" >= a || "\u0080" <= a && "\ufffd" >= a
};
A.U.hS = function(a) {
  return a.replace(/(\r\n|\r|\n)+/g, " ")
};
A.U.bQ = function(a) {
  return a.replace(/(\r\n|\r|\n)/g, "\n")
};
A.U.JR = function(a) {
  return a.replace(/\xa0|\s/g, " ")
};
A.U.IR = function(a) {
  return a.replace(/\xa0|[ \t]+/g, " ")
};
A.U.gQ = function(a) {
  return a.replace(/[\t\r\n ]+/g, " ").replace(/^[\t\r\n ]+|[\t\r\n ]+$/g, "")
};
A.U.trim = function(a) {
  return a.replace(/^[\s\xa0]+|[\s\xa0]+$/g, "")
};
A.U.trimLeft = function(a) {
  return a.replace(/^[\s\xa0]+/, "")
};
A.U.trimRight = function(a) {
  return a.replace(/[\s\xa0]+$/, "")
};
A.U.$G = function(a, b) {
  var c = ("" + a).toLowerCase(), d = ("" + b).toLowerCase();
  return c < d ? -1 : c == d ? 0 : 1
};
A.U.ZJ = /(\.\d+)|(\d+)|(\D+)/g;
A.U.KR = function(a, b) {
  if(a == b) {
    return 0
  }
  if(!a) {
    return-1
  }
  if(!b) {
    return 1
  }
  for(var c = a.toLowerCase().match(A.U.ZJ), d = b.toLowerCase().match(A.U.ZJ), f = Math.min(c.length, d.length), g = 0;g < f;g++) {
    var k = c[g], l = d[g];
    if(k != l) {
      return c = parseInt(k, 10), !isNaN(c) && (d = parseInt(l, 10), !isNaN(d) && c - d) ? c - d : k < l ? -1 : 1
    }
  }
  return c.length != d.length ? c.length - d.length : a < b ? -1 : 1
};
A.U.fM = /^[a-zA-Z0-9\-_.!~*'()]*$/;
A.U.us = function(a) {
  a = "" + a;
  return!A.U.fM.test(a) ? encodeURIComponent(a) : a
};
A.U.OA = function(a) {
  return decodeURIComponent(a.replace(/\+/g, " "))
};
A.U.vN = function(a, b) {
  return a.replace(/(\r\n|\r|\n)/g, b ? "<br />" : "<br>")
};
A.U.OI = function(a) {
  if(!A.U.uL.test(a)) {
    return a
  }
  -1 != a.indexOf("&") && (a = a.replace(A.U.vL, "&amp;"));
  -1 != a.indexOf("<") && (a = a.replace(A.U.pN, "&lt;"));
  -1 != a.indexOf(">") && (a = a.replace(A.U.NM, "&gt;"));
  -1 != a.indexOf('"') && (a = a.replace(A.U.HN, "&quot;"));
  return a
};
A.U.vL = /&/g;
A.U.pN = /</g;
A.U.NM = />/g;
A.U.HN = /\"/g;
A.U.uL = /[&<>\"]/;
A.U.VK = function(a) {
  return A.U.contains(a, "&") ? "document" in A.global ? A.U.hO(a) : A.U.iO(a) : a
};
A.U.hO = function(a) {
  var b = {"&amp;":"&", "&lt;":"<", "&gt;":">", "&quot;":'"'}, c = document.createElement("div");
  return a.replace(A.U.oL, function(a, f) {
    var g = b[a];
    if(g) {
      return g
    }
    if("#" == f.charAt(0)) {
      var k = Number("0" + f.substr(1));
      isNaN(k) || (g = String.fromCharCode(k))
    }
    g || (c.innerHTML = a + " ", g = c.firstChild.nodeValue.slice(0, -1));
    return b[a] = g
  })
};
A.U.iO = function(a) {
  return a.replace(/&([^;]+);/g, function(a, c) {
    switch(c) {
      case "amp":
        return"&";
      case "lt":
        return"<";
      case "gt":
        return">";
      case "quot":
        return'"';
      default:
        if("#" == c.charAt(0)) {
          var d = Number("0" + c.substr(1));
          if(!isNaN(d)) {
            return String.fromCharCode(d)
          }
        }
        return a
    }
  })
};
A.U.oL = /&([^;\s<&]+);?/g;
A.U.uS = function(a, b) {
  return A.U.vN(a.replace(/  /g, " &#160;"), b)
};
A.U.iS = function(a, b) {
  for(var c = b.length, d = 0;d < c;d++) {
    var f = 1 == c ? b : b.charAt(d);
    if(a.charAt(0) == f && a.charAt(a.length - 1) == f) {
      return a.substring(1, a.length - 1)
    }
  }
  return a
};
A.U.truncate = function(a, b, c) {
  c && (a = A.U.VK(a));
  a.length > b && (a = a.substring(0, b - 3) + "...");
  c && (a = A.U.OI(a));
  return a
};
A.U.oS = function(a, b, c, d) {
  c && (a = A.U.VK(a));
  if(d && a.length > b) {
    d > b && (d = b), a = a.substring(0, b - d) + "..." + a.substring(a.length - d)
  }else {
    if(a.length > b) {
      var d = Math.floor(b / 2), f = a.length - d, a = a.substring(0, d + b % 2) + "..." + a.substring(f)
    }
  }
  c && (a = A.U.OI(a));
  return a
};
A.U.TF = {"\x00":"\\0", "\u0008":"\\b", "\u000c":"\\f", "\n":"\\n", "\r":"\\r", "\t":"\\t", "\x0B":"\\x0B", '"':'\\"', "\\":"\\\\"};
A.U.Oy = {"'":"\\'"};
A.U.quote = function(a) {
  a = "" + a;
  if(a.quote) {
    return a.quote()
  }
  for(var b = ['"'], c = 0;c < a.length;c++) {
    var d = a.charAt(c), f = d.charCodeAt(0);
    b[c + 1] = A.U.TF[d] || (31 < f && 127 > f ? d : A.U.$H(d))
  }
  b.push('"');
  return b.join("")
};
A.U.vQ = function(a) {
  for(var b = [], c = 0;c < a.length;c++) {
    b[c] = A.U.$H(a.charAt(c))
  }
  return b.join("")
};
A.U.$H = function(a) {
  if(a in A.U.Oy) {
    return A.U.Oy[a]
  }
  if(a in A.U.TF) {
    return A.U.Oy[a] = A.U.TF[a]
  }
  var b = a, c = a.charCodeAt(0);
  if(31 < c && 127 > c) {
    b = a
  }else {
    if(256 > c) {
      if(b = "\\x", 16 > c || 256 < c) {
        b += "0"
      }
    }else {
      b = "\\u", 4096 > c && (b += "0")
    }
    b += c.toString(16).toUpperCase()
  }
  return A.U.Oy[a] = b
};
A.U.lS = function(a) {
  for(var b = {}, c = 0;c < a.length;c++) {
    b[a.charAt(c)] = j
  }
  return b
};
A.U.contains = function(a, b) {
  return-1 != a.indexOf(b)
};
A.U.jQ = function(a, b) {
  return a && b ? a.split(b).length - 1 : 0
};
A.U.Iz = function(a, b, c) {
  var d = a;
  0 <= b && b < a.length && 0 < c && (d = a.substr(0, b) + a.substr(b + c, a.length - b - c));
  return d
};
A.U.remove = function(a, b) {
  var c = RegExp(A.U.pK(b), "");
  return a.replace(c, "")
};
A.U.rK = function(a, b) {
  var c = RegExp(A.U.pK(b), "g");
  return a.replace(c, "")
};
A.U.pK = function(a) {
  return("" + a).replace(/([-()\[\]{}+?*.$\^|,:#<!\\])/g, "\\$1").replace(/\x08/g, "\\x08")
};
A.U.repeat = function(a, b) {
  return Array(b + 1).join(a)
};
A.U.MR = function(a, b, c) {
  a = A.To(c) ? a.toFixed(c) : "" + a;
  c = a.indexOf(".");
  -1 == c && (c = a.length);
  return A.U.repeat("0", Math.max(0, b - c)) + a
};
A.U.qN = function(a) {
  return a == p ? "" : "" + a
};
A.U.BL = function(a) {
  return Array.prototype.join.call(arguments, "")
};
A.U.BI = function() {
  return Math.floor(2147483648 * Math.random()).toString(36) + Math.abs(Math.floor(2147483648 * Math.random()) ^ A.now()).toString(36)
};
A.U.hH = function(a, b) {
  for(var c = 0, d = A.U.trim("" + a).split("."), f = A.U.trim("" + b).split("."), g = Math.max(d.length, f.length), k = 0;0 == c && k < g;k++) {
    var l = d[k] || "", n = f[k] || "", m = RegExp("(\\d*)(\\D*)", "g"), o = RegExp("(\\d*)(\\D*)", "g");
    do {
      var v = m.exec(l) || ["", "", ""], w = o.exec(n) || ["", "", ""];
      if(0 == v[0].length && 0 == w[0].length) {
        break
      }
      c = A.U.SB(0 == v[1].length ? 0 : parseInt(v[1], 10), 0 == w[1].length ? 0 : parseInt(w[1], 10)) || A.U.SB(0 == v[2].length, 0 == w[2].length) || A.U.SB(v[2], w[2])
    }while(0 == c)
  }
  return c
};
A.U.SB = function(a, b) {
  return a < b ? -1 : a > b ? 1 : 0
};
A.U.nL = 4294967296;
A.U.XQ = function(a) {
  for(var b = 0, c = 0;c < a.length;++c) {
    b = 31 * b + a.charCodeAt(c), b %= A.U.nL
  }
  return b
};
A.U.kO = 2147483648 * Math.random() | 0;
A.U.lQ = function() {
  return"goog_" + A.U.kO++
};
A.U.mS = function(a) {
  var b = Number(a);
  return 0 == b && A.U.ri(a) ? NaN : b
};
A.U.RK = {};
A.U.kS = function(a) {
  return A.U.RK[a] || (A.U.RK[a] = ("" + a).replace(/\-([a-z])/g, function(a, c) {
    return c.toUpperCase()
  }))
};
A.U.SK = {};
A.U.nS = function(a) {
  return A.U.SK[a] || (A.U.SK[a] = ("" + a).replace(/([A-Z])/g, "-$1").toLowerCase())
};
A.Xc = {};
A.Xc.Rk = A.dB;
function da(a, b) {
  b.unshift(a);
  ba.call(this, A.U.bO.apply(p, b));
  b.shift()
}
A.e(da, ba);
da.prototype.name = "AssertionError";
A.Xc.Fm = function(a, b, c, d) {
  var f = "Assertion failed";
  if(c) {
    var f = f + (": " + c), g = d
  }else {
    a && (f += ": " + a, g = b)
  }
  e(new da("" + f, g || []))
};
A.Xc.assert = function(a, b, c) {
  A.Xc.Rk && !a && A.Xc.Fm("", p, b, Array.prototype.slice.call(arguments, 2));
  return a
};
A.Xc.yQ = function(a, b) {
  A.Xc.Rk && e(new da("Failure" + (a ? ": " + a : ""), Array.prototype.slice.call(arguments, 1)))
};
A.Xc.PP = function(a, b, c) {
  A.Xc.Rk && !A.mJ(a) && A.Xc.Fm("Expected number but got %s: %s.", [A.Gj(a), a], b, Array.prototype.slice.call(arguments, 2));
  return a
};
A.Xc.RP = function(a, b, c) {
  A.Xc.Rk && !A.Qc(a) && A.Xc.Fm("Expected string but got %s: %s.", [A.Gj(a), a], b, Array.prototype.slice.call(arguments, 2));
  return a
};
A.Xc.NP = function(a, b, c) {
  A.Xc.Rk && !A.Dy(a) && A.Xc.Fm("Expected function but got %s: %s.", [A.Gj(a), a], b, Array.prototype.slice.call(arguments, 2));
  return a
};
A.Xc.QP = function(a, b, c) {
  A.Xc.Rk && !A.hn(a) && A.Xc.Fm("Expected object but got %s: %s.", [A.Gj(a), a], b, Array.prototype.slice.call(arguments, 2));
  return a
};
A.Xc.LP = function(a, b, c) {
  A.Xc.Rk && !A.isArray(a) && A.Xc.Fm("Expected array but got %s: %s.", [A.Gj(a), a], b, Array.prototype.slice.call(arguments, 2));
  return a
};
A.Xc.MP = function(a, b, c) {
  A.Xc.Rk && !A.fN(a) && A.Xc.Fm("Expected boolean but got %s: %s.", [A.Gj(a), a], b, Array.prototype.slice.call(arguments, 2));
  return a
};
A.Xc.OP = function(a, b, c, d) {
  A.Xc.Rk && !(a instanceof b) && A.Xc.Fm("instanceof check failed.", p, c, Array.prototype.slice.call(arguments, 3))
};
A.$ = {};
A.Zn = j;
A.$.NR = function(a) {
  return a[a.length - 1]
};
A.$.Od = Array.prototype;
A.$.indexOf = A.Zn && A.$.Od.indexOf ? function(a, b, c) {
  return A.$.Od.indexOf.call(a, b, c)
} : function(a, b, c) {
  c = c == p ? 0 : 0 > c ? Math.max(0, a.length + c) : c;
  if(A.Qc(a)) {
    return!A.Qc(b) || 1 != b.length ? -1 : a.indexOf(b, c)
  }
  for(;c < a.length;c++) {
    if(c in a && a[c] === b) {
      return c
    }
  }
  return-1
};
A.$.lastIndexOf = A.Zn && A.$.Od.lastIndexOf ? function(a, b, c) {
  return A.$.Od.lastIndexOf.call(a, b, c == p ? a.length - 1 : c)
} : function(a, b, c) {
  c = c == p ? a.length - 1 : c;
  0 > c && (c = Math.max(0, a.length + c));
  if(A.Qc(a)) {
    return!A.Qc(b) || 1 != b.length ? -1 : a.lastIndexOf(b, c)
  }
  for(;0 <= c;c--) {
    if(c in a && a[c] === b) {
      return c
    }
  }
  return-1
};
A.$.forEach = A.Zn && A.$.Od.forEach ? function(a, b, c) {
  A.$.Od.forEach.call(a, b, c)
} : function(a, b, c) {
  for(var d = a.length, f = A.Qc(a) ? a.split("") : a, g = 0;g < d;g++) {
    g in f && b.call(c, f[g], g, a)
  }
};
A.$.kM = function(a, b) {
  for(var c = a.length, d = A.Qc(a) ? a.split("") : a, c = c - 1;0 <= c;--c) {
    c in d && b.call(h, d[c], c, a)
  }
};
A.$.filter = A.Zn && A.$.Od.filter ? function(a, b, c) {
  return A.$.Od.filter.call(a, b, c)
} : function(a, b, c) {
  for(var d = a.length, f = [], g = 0, k = A.Qc(a) ? a.split("") : a, l = 0;l < d;l++) {
    if(l in k) {
      var n = k[l];
      b.call(c, n, l, a) && (f[g++] = n)
    }
  }
  return f
};
A.$.map = A.Zn && A.$.Od.map ? function(a, b, c) {
  return A.$.Od.map.call(a, b, c)
} : function(a, b, c) {
  for(var d = a.length, f = Array(d), g = A.Qc(a) ? a.split("") : a, k = 0;k < d;k++) {
    k in g && (f[k] = b.call(c, g[k], k, a))
  }
  return f
};
A.$.reduce = function(a, b, c, d) {
  if(a.reduce) {
    return d ? a.reduce(A.bind(b, d), c) : a.reduce(b, c)
  }
  var f = c;
  A.$.forEach(a, function(c, k) {
    f = b.call(d, f, c, k, a)
  });
  return f
};
A.$.reduceRight = function(a, b, c, d) {
  if(a.reduceRight) {
    return d ? a.reduceRight(A.bind(b, d), c) : a.reduceRight(b, c)
  }
  var f = c;
  A.$.kM(a, function(c, k) {
    f = b.call(d, f, c, k, a)
  });
  return f
};
A.$.some = A.Zn && A.$.Od.some ? function(a, b, c) {
  return A.$.Od.some.call(a, b, c)
} : function(a, b, c) {
  for(var d = a.length, f = A.Qc(a) ? a.split("") : a, g = 0;g < d;g++) {
    if(g in f && b.call(c, f[g], g, a)) {
      return j
    }
  }
  return q
};
A.$.every = A.Zn && A.$.Od.every ? function(a, b, c) {
  return A.$.Od.every.call(a, b, c)
} : function(a, b, c) {
  for(var d = a.length, f = A.Qc(a) ? a.split("") : a, g = 0;g < d;g++) {
    if(g in f && !b.call(c, f[g], g, a)) {
      return q
    }
  }
  return j
};
A.$.find = function(a, b, c) {
  b = A.$.hI(a, b, c);
  return 0 > b ? p : A.Qc(a) ? a.charAt(b) : a[b]
};
A.$.hI = function(a, b, c) {
  for(var d = a.length, f = A.Qc(a) ? a.split("") : a, g = 0;g < d;g++) {
    if(g in f && b.call(c, f[g], g, a)) {
      return g
    }
  }
  return-1
};
A.$.zQ = function(a, b, c) {
  b = A.$.hM(a, b, c);
  return 0 > b ? p : A.Qc(a) ? a.charAt(b) : a[b]
};
A.$.hM = function(a, b, c) {
  for(var d = a.length, f = A.Qc(a) ? a.split("") : a, d = d - 1;0 <= d;d--) {
    if(d in f && b.call(c, f[d], d, a)) {
      return d
    }
  }
  return-1
};
A.$.contains = function(a, b) {
  return 0 <= A.$.indexOf(a, b)
};
A.$.ri = function(a) {
  return 0 == a.length
};
A.$.clear = function(a) {
  if(!A.isArray(a)) {
    for(var b = a.length - 1;0 <= b;b--) {
      delete a[b]
    }
  }
  a.length = 0
};
A.$.$Q = function(a, b) {
  A.$.contains(a, b) || a.push(b)
};
A.$.VI = function(a, b, c) {
  A.$.splice(a, c, 0, b)
};
A.$.aR = function(a, b, c) {
  A.fK(A.$.splice, a, c, 0).apply(p, b)
};
A.$.insertBefore = function(a, b, c) {
  var d;
  2 == arguments.length || 0 > (d = A.$.indexOf(a, c)) ? a.push(b) : A.$.VI(a, b, d)
};
A.$.remove = function(a, b) {
  var c = A.$.indexOf(a, b), d;
  (d = 0 <= c) && A.$.Iz(a, c);
  return d
};
A.$.Iz = function(a, b) {
  return 1 == A.$.Od.splice.call(a, b, 1).length
};
A.$.VR = function(a, b, c) {
  b = A.$.hI(a, b, c);
  return 0 <= b ? (A.$.Iz(a, b), j) : q
};
A.$.concat = function(a) {
  return A.$.Od.concat.apply(A.$.Od, arguments)
};
A.$.Ca = function(a) {
  if(A.isArray(a)) {
    return A.$.concat(a)
  }
  for(var b = [], c = 0, d = a.length;c < d;c++) {
    b[c] = a[c]
  }
  return b
};
A.$.kG = function(a) {
  return A.isArray(a) ? A.$.concat(a) : A.$.Ca(a)
};
A.$.extend = function(a, b) {
  for(var c = 1;c < arguments.length;c++) {
    var d = arguments[c], f;
    if(A.isArray(d) || (f = A.$e(d)) && d.hasOwnProperty("callee")) {
      a.push.apply(a, d)
    }else {
      if(f) {
        for(var g = a.length, k = d.length, l = 0;l < k;l++) {
          a[g + l] = d[l]
        }
      }else {
        a.push(d)
      }
    }
  }
};
A.$.splice = function(a, b, c, d) {
  return A.$.Od.splice.apply(a, A.$.slice(arguments, 1))
};
A.$.slice = function(a, b, c) {
  return 2 >= arguments.length ? A.$.Od.slice.call(a, b) : A.$.Od.slice.call(a, b, c)
};
A.$.TR = function(a, b) {
  for(var c = b || a, d = {}, f = 0, g = 0;g < a.length;) {
    var k = a[g++], l = A.hn(k) ? "o" + A.Um(k) : (typeof k).charAt(0) + k;
    Object.prototype.hasOwnProperty.call(d, l) || (d[l] = j, c[f++] = k)
  }
  c.length = f
};
A.$.SG = function(a, b, c) {
  return A.$.TG(a, c || A.$.uo, q, b)
};
A.$.XP = function(a, b, c) {
  return A.$.TG(a, b, j, h, c)
};
A.$.TG = function(a, b, c, d, f) {
  for(var g = 0, k = a.length, l;g < k;) {
    var n = g + k >> 1, m;
    m = c ? b.call(f, a[n], n, a) : b(d, a[n]);
    0 < m ? g = n + 1 : (k = n, l = !m)
  }
  return l ? g : ~g
};
A.$.sort = function(a, b) {
  A.$.Od.sort.call(a, b || A.$.uo)
};
A.$.gS = function(a, b) {
  for(var c = 0;c < a.length;c++) {
    a[c] = {index:c, value:a[c]}
  }
  var d = b || A.$.uo;
  A.$.sort(a, function(a, b) {
    return d(a.value, b.value) || a.index - b.index
  });
  for(c = 0;c < a.length;c++) {
    a[c] = a[c].value
  }
};
A.$.fS = function(a, b, c) {
  var d = c || A.$.uo;
  A.$.sort(a, function(a, c) {
    return d(a[b], c[b])
  })
};
A.$.yR = function(a, b, c) {
  for(var b = b || A.$.uo, d = 1;d < a.length;d++) {
    var f = b(a[d - 1], a[d]);
    if(0 < f || 0 == f && c) {
      return q
    }
  }
  return j
};
A.$.CC = function(a, b, c) {
  if(!A.$e(a) || !A.$e(b) || a.length != b.length) {
    return q
  }
  for(var d = a.length, c = c || A.$.RL, f = 0;f < d;f++) {
    if(!c(a[f], b[f])) {
      return q
    }
  }
  return j
};
A.$.gH = function(a, b, c) {
  return A.$.CC(a, b, c)
};
A.$.iQ = function(a, b, c) {
  for(var c = c || A.$.uo, d = Math.min(a.length, b.length), f = 0;f < d;f++) {
    var g = c(a[f], b[f]);
    if(0 != g) {
      return g
    }
  }
  return A.$.uo(a.length, b.length)
};
A.$.uo = function(a, b) {
  return a > b ? 1 : a < b ? -1 : 0
};
A.$.RL = function(a, b) {
  return a === b
};
A.$.VP = function(a, b, c) {
  c = A.$.SG(a, b, c);
  return 0 > c ? (A.$.VI(a, b, -(c + 1)), j) : q
};
A.$.WP = function(a, b, c) {
  b = A.$.SG(a, b, c);
  return 0 <= b ? A.$.Iz(a, b) : q
};
A.$.YP = function(a, b) {
  for(var c = {}, d = 0;d < a.length;d++) {
    var f = a[d], g = b(f, d, a);
    A.To(g) && (c[g] || (c[g] = [])).push(f)
  }
  return c
};
A.$.repeat = function(a, b) {
  for(var c = [], d = 0;d < b;d++) {
    c[d] = a
  }
  return c
};
A.$.jM = function(a) {
  for(var b = [], c = 0;c < arguments.length;c++) {
    var d = arguments[c];
    A.isArray(d) ? b.push.apply(b, A.$.jM.apply(p, d)) : b.push(d)
  }
  return b
};
A.$.rotate = function(a, b) {
  a.length && (b %= a.length, 0 < b ? A.$.Od.unshift.apply(a, a.splice(-b, b)) : 0 > b && A.$.Od.push.apply(a, a.splice(0, -b)));
  return a
};
A.$.vS = function(a) {
  if(!arguments.length) {
    return[]
  }
  for(var b = [], c = 0;;c++) {
    for(var d = [], f = 0;f < arguments.length;f++) {
      var g = arguments[f];
      if(c >= g.length) {
        return b
      }
      d.push(g[c])
    }
    b.push(d)
  }
};
A.$.eS = function(a, b) {
  for(var c = b || Math.random, d = a.length - 1;0 < d;d--) {
    var f = Math.floor(c() * (d + 1)), g = a[d];
    a[d] = a[f];
    a[f] = g
  }
};
A.debug.Xe = {};
A.debug.QO = s();
A.debug.Xe.Rr = [];
A.debug.Xe.GE = [];
A.debug.Xe.LJ = q;
A.debug.Xe.kF = function(a) {
  A.debug.Xe.Rr[A.debug.Xe.Rr.length] = a;
  if(A.debug.Xe.LJ) {
    for(var b = A.debug.Xe.GE, c = 0;c < b.length;c++) {
      a(A.bind(b[c].sO, b[c]))
    }
  }
};
A.debug.Xe.GR = function(a) {
  A.debug.Xe.LJ = j;
  for(var b = A.bind(a.sO, a), c = 0;c < A.debug.Xe.Rr.length;c++) {
    A.debug.Xe.Rr[c](b)
  }
  A.debug.Xe.GE.push(a)
};
A.debug.Xe.qS = function(a) {
  for(var b = A.debug.Xe.GE, a = A.bind(a.sS, a), c = 0;c < A.debug.Xe.Rr.length;c++) {
    A.debug.Xe.Rr[c](a)
  }
  b.length--
};
A.debug.uQ = {kK:aa()};
A.userAgent = {};
A.userAgent.zG = q;
A.userAgent.yG = q;
A.userAgent.DG = q;
A.userAgent.bB = q;
A.userAgent.CG = q;
A.userAgent.eL = q;
A.userAgent.$p = A.userAgent.zG || A.userAgent.yG || A.userAgent.bB || A.userAgent.DG || A.userAgent.CG;
A.userAgent.II = function() {
  return A.global.navigator ? A.global.navigator.userAgent : p
};
A.userAgent.Xx = function() {
  return A.global.navigator
};
A.userAgent.XM = function() {
  A.userAgent.Ht = q;
  A.userAgent.JH = q;
  A.userAgent.wx = q;
  A.userAgent.KH = q;
  A.userAgent.IH = q;
  var a;
  if(!A.userAgent.$p && (a = A.userAgent.II())) {
    var b = A.userAgent.Xx();
    A.userAgent.Ht = 0 == a.indexOf("Opera");
    A.userAgent.JH = !A.userAgent.Ht && -1 != a.indexOf("MSIE");
    A.userAgent.wx = !A.userAgent.Ht && -1 != a.indexOf("WebKit");
    A.userAgent.KH = A.userAgent.wx && -1 != a.indexOf("Mobile");
    A.userAgent.IH = !A.userAgent.Ht && !A.userAgent.wx && "Gecko" == b.product
  }
};
A.userAgent.$p || A.userAgent.XM();
A.userAgent.fB = A.userAgent.$p ? A.userAgent.CG : A.userAgent.Ht;
A.userAgent.Ij = A.userAgent.$p ? A.userAgent.zG : A.userAgent.JH;
A.userAgent.ow = A.userAgent.$p ? A.userAgent.yG : A.userAgent.IH;
A.userAgent.hm = A.userAgent.$p ? A.userAgent.DG || A.userAgent.bB : A.userAgent.wx;
A.userAgent.iP = A.userAgent.bB || A.userAgent.KH;
A.userAgent.rP = A.userAgent.hm;
A.userAgent.YL = function() {
  var a = A.userAgent.Xx();
  return a && a.platform || ""
};
A.userAgent.hB = A.userAgent.YL();
A.userAgent.BG = q;
A.userAgent.EG = q;
A.userAgent.AG = q;
A.userAgent.FG = q;
A.userAgent.Qs = A.userAgent.BG || A.userAgent.EG || A.userAgent.AG || A.userAgent.FG;
A.userAgent.WM = function() {
  A.userAgent.VL = A.U.contains(A.userAgent.hB, "Mac");
  A.userAgent.WL = A.U.contains(A.userAgent.hB, "Win");
  A.userAgent.UL = A.U.contains(A.userAgent.hB, "Linux");
  A.userAgent.XL = !!A.userAgent.Xx() && A.U.contains(A.userAgent.Xx().appVersion || "", "X11")
};
A.userAgent.Qs || A.userAgent.WM();
A.userAgent.fP = A.userAgent.Qs ? A.userAgent.BG : A.userAgent.VL;
A.userAgent.DP = A.userAgent.Qs ? A.userAgent.EG : A.userAgent.WL;
A.userAgent.bP = A.userAgent.Qs ? A.userAgent.AG : A.userAgent.UL;
A.userAgent.EP = A.userAgent.Qs ? A.userAgent.FG : A.userAgent.XL;
A.userAgent.ZL = function() {
  var a = "", b;
  A.userAgent.fB && A.global.opera ? (a = A.global.opera.version, a = "function" == typeof a ? a() : a) : (A.userAgent.ow ? b = /rv\:([^\);]+)(\)|;)/ : A.userAgent.Ij ? b = /MSIE\s+([^\);]+)(\)|;)/ : A.userAgent.hm && (b = /WebKit\/(\S+)/), b && (a = (a = b.exec(A.userAgent.II())) ? a[1] : ""));
  return A.userAgent.Ij && (b = A.userAgent.wM(), b > parseFloat(a)) ? "" + b : a
};
A.userAgent.wM = function() {
  var a = A.global.document;
  return a ? a.documentMode : h
};
A.userAgent.VERSION = A.userAgent.ZL();
A.userAgent.gH = function(a, b) {
  return A.U.hH(a, b)
};
A.userAgent.xJ = {};
A.userAgent.Yo = function(a) {
  return A.userAgent.eL || A.userAgent.xJ[a] || (A.userAgent.xJ[a] = 0 <= A.U.hH(A.userAgent.VERSION, a))
};
A.userAgent.dJ = {};
A.userAgent.cJ = function() {
  return A.userAgent.dJ[9] || (A.userAgent.dJ[9] = A.userAgent.Ij && !!document.documentMode && 9 <= document.documentMode)
};
A.G = {};
!A.userAgent.Ij || A.userAgent.cJ();
var ea = !A.userAgent.Ij || A.userAgent.cJ(), fa = A.userAgent.Ij && !A.userAgent.Yo("8");
!A.userAgent.hm || A.userAgent.Yo("528");
A.userAgent.ow && A.userAgent.Yo("1.9b") || A.userAgent.Ij && A.userAgent.Yo("8") || A.userAgent.fB && A.userAgent.Yo("9.5") || A.userAgent.hm && A.userAgent.Yo("528");
!A.userAgent.ow || A.userAgent.Yo("8");
A.$L = {};
A.$L.XO = s();
function ga() {
}
ga.prototype.PH = q;
ga.prototype.aj = function() {
  this.PH || (this.PH = j, this.Em())
};
ga.prototype.Em = function() {
  this.SL && A.OH.apply(p, this.SL)
};
A.aj = function(a) {
  a && "function" == typeof a.aj && a.aj()
};
A.OH = function(a) {
  for(var b = 0, c = arguments.length;b < c;++b) {
    var d = arguments[b];
    A.$e(d) ? A.OH.apply(p, d) : A.aj(d)
  }
};
function ha(a, b) {
  this.type = a;
  this.currentTarget = this.target = b
}
A.e(ha, ga);
z = ha.prototype;
z.Em = function() {
  delete this.type;
  delete this.target;
  delete this.currentTarget
};
z.Ap = q;
z.uv = j;
z.stopPropagation = function() {
  this.Ap = j
};
z.preventDefault = function() {
  this.uv = q
};
ha.stopPropagation = function(a) {
  a.stopPropagation()
};
ha.preventDefault = function(a) {
  a.preventDefault()
};
var ka = {cB:"click", HO:"dblclick", gm:"mousedown", Ps:"mouseup", Os:"mouseover", Ns:"mouseout", Yn:"mousemove", uP:"selectstart", $O:"keypress", ZO:"keydown", aP:"keyup", AO:"blur", SO:"focus", IO:"deactivate", TO:A.userAgent.Ij ? "focusin" : "DOMFocusIn", UO:A.userAgent.Ij ? "focusout" : "DOMFocusOut", CO:"change", tP:"select", vP:"submit", YO:"input", pP:"propertychange", MO:"dragstart", JO:"dragenter", LO:"dragover", KO:"dragleave", NO:"drop", zP:"touchstart", yP:"touchmove", xP:"touchend", 
wP:"touchcancel", EO:"contextmenu", PO:"error", WO:"help", cP:"load", eP:"losecapture", qP:"readystatechange", pw:"resize", sP:"scroll", BP:"unload", VO:"hashchange", lP:"pagehide", mP:"pageshow", oP:"popstate", FO:"copy", nP:"paste", GO:"cut", xO:"beforecopy", yO:"beforecut", zO:"beforepaste", gP:"message", DO:"connect", AP:A.userAgent.hm ? "webkitTransitionEnd" : A.userAgent.fB ? "oTransitionEnd" : "transitionend"};
A.Bp = {};
A.Bp.object = function(a, b) {
  return b
};
A.Bp.QF = function(a) {
  A.Bp.QF[" "](a);
  return a
};
A.Bp.QF[" "] = A.XJ;
A.Bp.CL = function(a) {
  try {
    return A.Bp.QF(a.nodeName), j
  }catch(b) {
  }
  return q
};
function la(a, b) {
  a && this.Ha(a, b)
}
A.e(la, ha);
z = la.prototype;
z.target = p;
z.relatedTarget = p;
z.offsetX = 0;
z.offsetY = 0;
z.clientX = 0;
z.clientY = 0;
z.screenX = 0;
z.screenY = 0;
z.button = 0;
z.keyCode = 0;
z.charCode = 0;
z.ctrlKey = q;
z.altKey = q;
z.shiftKey = q;
z.metaKey = q;
z.Jm = p;
z.Ha = function(a, b) {
  var c = this.type = a.type;
  ha.call(this, c);
  this.target = a.target || a.srcElement;
  this.currentTarget = b;
  var d = a.relatedTarget;
  d ? A.userAgent.ow && (A.Bp.CL(d) || (d = p)) : c == ka.Os ? d = a.fromElement : c == ka.Ns && (d = a.toElement);
  this.relatedTarget = d;
  this.offsetX = A.userAgent.hm || a.offsetX !== h ? a.offsetX : a.layerX;
  this.offsetY = A.userAgent.hm || a.offsetY !== h ? a.offsetY : a.layerY;
  this.clientX = a.clientX !== h ? a.clientX : a.pageX;
  this.clientY = a.clientY !== h ? a.clientY : a.pageY;
  this.screenX = a.screenX || 0;
  this.screenY = a.screenY || 0;
  this.button = a.button;
  this.keyCode = a.keyCode || 0;
  this.charCode = a.charCode || ("keypress" == c ? a.keyCode : 0);
  this.ctrlKey = a.ctrlKey;
  this.altKey = a.altKey;
  this.shiftKey = a.shiftKey;
  this.metaKey = a.metaKey;
  this.state = a.state;
  this.Jm = a;
  delete this.uv;
  delete this.Ap
};
z.stopPropagation = function() {
  la.f.stopPropagation.call(this);
  this.Jm.stopPropagation ? this.Jm.stopPropagation() : this.Jm.cancelBubble = j
};
z.preventDefault = function() {
  la.f.preventDefault.call(this);
  var a = this.Jm;
  if(a.preventDefault) {
    a.preventDefault()
  }else {
    if(a.returnValue = q, fa) {
      try {
        if(a.ctrlKey || 112 <= a.keyCode && 123 >= a.keyCode) {
          a.keyCode = -1
        }
      }catch(b) {
      }
    }
  }
};
z.Em = function() {
  la.f.Em.call(this);
  this.relatedTarget = this.currentTarget = this.target = this.Jm = p
};
A.G.IG = s();
A.G.IG.prototype.nb = s();
A.G.IG.prototype.ed = s();
function na() {
}
var oa = 0;
z = na.prototype;
z.key = 0;
z.Cp = q;
z.MB = q;
z.Ha = function(a, b, c, d, f, g) {
  A.Dy(a) ? this.hJ = j : a && a.handleEvent && A.Dy(a.handleEvent) ? this.hJ = q : e(Error("Invalid listener argument"));
  this.Br = a;
  this.lK = b;
  this.src = c;
  this.type = d;
  this.capture = !!f;
  this.ky = g;
  this.MB = q;
  this.key = ++oa;
  this.Cp = q
};
z.handleEvent = function(a) {
  return this.hJ ? this.Br.call(this.ky || this.src, a) : this.Br.handleEvent.call(this.Br, a)
};
A.object = {};
A.object.forEach = function(a, b, c) {
  for(var d in a) {
    b.call(c, a[d], d, a)
  }
};
A.object.filter = function(a, b, c) {
  var d = {}, f;
  for(f in a) {
    b.call(c, a[f], f, a) && (d[f] = a[f])
  }
  return d
};
A.object.map = function(a, b, c) {
  var d = {}, f;
  for(f in a) {
    d[f] = b.call(c, a[f], f, a)
  }
  return d
};
A.object.some = function(a, b, c) {
  for(var d in a) {
    if(b.call(c, a[d], d, a)) {
      return j
    }
  }
  return q
};
A.object.every = function(a, b, c) {
  for(var d in a) {
    if(!b.call(c, a[d], d, a)) {
      return q
    }
  }
  return j
};
A.object.Tm = function(a) {
  var b = 0, c;
  for(c in a) {
    b++
  }
  return b
};
A.object.CQ = function(a) {
  for(var b in a) {
    return b
  }
};
A.object.DQ = function(a) {
  for(var b in a) {
    return a[b]
  }
};
A.object.contains = function(a, b) {
  return A.object.yq(a, b)
};
A.object.Ff = function(a) {
  var b = [], c = 0, d;
  for(d in a) {
    b[c++] = a[d]
  }
  return b
};
A.object.lg = function(a) {
  var b = [], c = 0, d;
  for(d in a) {
    b[c++] = d
  }
  return b
};
A.object.SQ = function(a, b) {
  for(var c = A.$e(b), d = c ? b : arguments, c = c ? 0 : 1;c < d.length && !(a = a[d[c]], !A.To(a));c++) {
  }
  return a
};
A.object.Xj = function(a, b) {
  return b in a
};
A.object.yq = function(a, b) {
  for(var c in a) {
    if(a[c] == b) {
      return j
    }
  }
  return q
};
A.object.iM = function(a, b, c) {
  for(var d in a) {
    if(b.call(c, a[d], d, a)) {
      return d
    }
  }
};
A.object.AQ = function(a, b, c) {
  return(b = A.object.iM(a, b, c)) && a[b]
};
A.object.ri = function(a) {
  for(var b in a) {
    return q
  }
  return j
};
A.object.clear = function(a) {
  for(var b in a) {
    delete a[b]
  }
};
A.object.remove = function(a, b) {
  var c;
  (c = b in a) && delete a[b];
  return c
};
A.object.add = function(a, b, c) {
  b in a && e(Error('The object already contains the key "' + b + '"'));
  A.object.set(a, b, c)
};
A.object.get = function(a, b, c) {
  return b in a ? a[b] : c
};
A.object.set = function(a, b, c) {
  a[b] = c
};
A.object.bS = function(a, b, c) {
  return b in a ? a[b] : a[b] = c
};
A.object.Ca = function(a) {
  var b = {}, c;
  for(c in a) {
    b[c] = a[c]
  }
  return b
};
A.object.lO = function(a) {
  var b = A.Gj(a);
  if("object" == b || "array" == b) {
    if(a.Ca) {
      return a.Ca()
    }
    var b = "array" == b ? [] : {}, c;
    for(c in a) {
      b[c] = A.object.lO(a[c])
    }
    return b
  }
  return a
};
A.object.fO = function(a) {
  var b = {}, c;
  for(c in a) {
    b[a[c]] = c
  }
  return b
};
A.object.JG = "constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf".split(",");
A.object.extend = function(a, b) {
  for(var c, d, f = 1;f < arguments.length;f++) {
    d = arguments[f];
    for(c in d) {
      a[c] = d[c]
    }
    for(var g = 0;g < A.object.JG.length;g++) {
      c = A.object.JG[g], Object.prototype.hasOwnProperty.call(d, c) && (a[c] = d[c])
    }
  }
};
A.object.create = function(a) {
  var b = arguments.length;
  if(1 == b && A.isArray(arguments[0])) {
    return A.object.create.apply(p, arguments[0])
  }
  b % 2 && e(Error("Uneven number of arguments"));
  for(var c = {}, d = 0;d < b;d += 2) {
    c[arguments[d]] = arguments[d + 1]
  }
  return c
};
A.object.KL = function(a) {
  var b = arguments.length;
  if(1 == b && A.isArray(arguments[0])) {
    return A.object.KL.apply(p, arguments[0])
  }
  for(var c = {}, d = 0;d < b;d++) {
    c[arguments[d]] = j
  }
  return c
};
A.G.wO = q;
A.G.fe = {};
A.G.Pg = {};
A.G.Jk = {};
A.G.BN = "on";
A.G.SE = {};
A.G.BR = "_";
A.G.nb = function(a, b, c, d, f) {
  if(b) {
    if(A.isArray(b)) {
      for(var g = 0;g < b.length;g++) {
        A.G.nb(a, b[g], c, d, f)
      }
      return p
    }
    var d = !!d, k = A.G.Pg;
    b in k || (k[b] = {zc:0, Bi:0});
    k = k[b];
    d in k || (k[d] = {zc:0, Bi:0}, k.zc++);
    var k = k[d], l = A.Um(a), n;
    k.Bi++;
    if(k[l]) {
      n = k[l];
      for(g = 0;g < n.length;g++) {
        if(k = n[g], k.Br == c && k.ky == f) {
          if(k.Cp) {
            break
          }
          return n[g].key
        }
      }
    }else {
      n = k[l] = [], k.zc++
    }
    g = A.G.HM();
    g.src = a;
    k = new na;
    k.Ha(c, g, a, b, d, f);
    c = k.key;
    g.key = c;
    n.push(k);
    A.G.fe[c] = k;
    A.G.Jk[l] || (A.G.Jk[l] = []);
    A.G.Jk[l].push(k);
    a.addEventListener ? (a == A.global || !a.AH) && a.addEventListener(b, g, d) : a.attachEvent(A.G.yI(b), g);
    return c
  }
  e(Error("Invalid event type"))
};
A.G.HM = function() {
  var a = A.G.ju, b = ea ? function(c) {
    return a.call(b.src, b.key, c)
  } : function(c) {
    c = a.call(b.src, b.key, c);
    if(!c) {
      return c
    }
  };
  return b
};
A.G.nN = function(a, b, c, d, f) {
  if(A.isArray(b)) {
    for(var g = 0;g < b.length;g++) {
      A.G.nN(a, b[g], c, d, f)
    }
  }else {
    a = A.G.nb(a, b, c, d, f), A.G.fe[a].MB = j
  }
};
A.G.CR = function(a, b, c, d, f) {
  b.nb(a, c, d, f)
};
A.G.ed = function(a, b, c, d, f) {
  if(A.isArray(b)) {
    for(var g = 0;g < b.length;g++) {
      A.G.ed(a, b[g], c, d, f)
    }
    return p
  }
  d = !!d;
  a = A.G.dD(a, b, d);
  if(!a) {
    return q
  }
  for(g = 0;g < a.length;g++) {
    if(a[g].Br == c && a[g].capture == d && a[g].ky == f) {
      return A.G.LA(a[g].key)
    }
  }
  return q
};
A.G.LA = function(a) {
  if(!A.G.fe[a]) {
    return q
  }
  var b = A.G.fe[a];
  if(b.Cp) {
    return q
  }
  var c = b.src, d = b.type, f = b.lK, g = b.capture;
  c.removeEventListener ? (c == A.global || !c.AH) && c.removeEventListener(d, f, g) : c.detachEvent && c.detachEvent(A.G.yI(d), f);
  c = A.Um(c);
  f = A.G.Pg[d][g][c];
  if(A.G.Jk[c]) {
    var k = A.G.Jk[c];
    A.$.remove(k, b);
    0 == k.length && delete A.G.Jk[c]
  }
  b.Cp = j;
  f.VJ = j;
  A.G.dH(d, g, c, f);
  delete A.G.fe[a];
  return j
};
A.G.pS = function(a, b, c, d, f) {
  b.ed(a, c, d, f)
};
A.G.dH = function(a, b, c, d) {
  if(!d.Vy && d.VJ) {
    for(var f = 0, g = 0;f < d.length;f++) {
      d[f].Cp ? d[f].lK.src = p : (f != g && (d[g] = d[f]), g++)
    }
    d.length = g;
    d.VJ = q;
    0 == g && (delete A.G.Pg[a][b][c], A.G.Pg[a][b].zc--, 0 == A.G.Pg[a][b].zc && (delete A.G.Pg[a][b], A.G.Pg[a].zc--), 0 == A.G.Pg[a].zc && delete A.G.Pg[a])
  }
};
A.G.rK = function(a, b, c) {
  var d = 0, f = b == p, g = c == p, c = !!c;
  if(a == p) {
    A.object.forEach(A.G.Jk, function(a) {
      for(var k = a.length - 1;0 <= k;k--) {
        var l = a[k];
        if((f || b == l.type) && (g || c == l.capture)) {
          A.G.LA(l.key), d++
        }
      }
    })
  }else {
    if(a = A.Um(a), A.G.Jk[a]) {
      for(var a = A.G.Jk[a], k = a.length - 1;0 <= k;k--) {
        var l = a[k];
        if((f || b == l.type) && (g || c == l.capture)) {
          A.G.LA(l.key), d++
        }
      }
    }
  }
  return d
};
A.G.KQ = function(a, b, c) {
  return A.G.dD(a, b, c) || []
};
A.G.dD = function(a, b, c) {
  var d = A.G.Pg;
  return b in d && (d = d[b], c in d && (d = d[c], a = A.Um(a), d[a])) ? d[a] : p
};
A.G.JQ = function(a, b, c, d, f) {
  d = !!d;
  if(a = A.G.dD(a, b, d)) {
    for(b = 0;b < a.length;b++) {
      if(!a[b].Cp && a[b].Br == c && a[b].capture == d && a[b].ky == f) {
        return a[b]
      }
    }
  }
  return p
};
A.G.VQ = function(a, b, c) {
  var a = A.Um(a), d = A.G.Jk[a];
  if(d) {
    var f = A.To(b), g = A.To(c);
    return f && g ? (d = A.G.Pg[b], !!d && !!d[c] && a in d[c]) : !f && !g ? j : A.$.some(d, function(a) {
      return f && a.type == b || g && a.capture == c
    })
  }
  return q
};
A.G.wQ = function(a) {
  var b = [], c;
  for(c in a) {
    a[c] && a[c].id ? b.push(c + " = " + a[c] + " (" + a[c].id + ")") : b.push(c + " = " + a[c])
  }
  return b.join("\n")
};
A.G.yI = function(a) {
  return a in A.G.SE ? A.G.SE[a] : A.G.SE[a] = A.G.BN + a
};
A.G.BQ = function(a, b, c, d) {
  var f = A.G.Pg;
  return b in f && (f = f[b], c in f) ? A.G.Mq(f[c], a, b, c, d) : j
};
A.G.Mq = function(a, b, c, d, f) {
  var g = 1, b = A.Um(b);
  if(a[b]) {
    a.Bi--;
    a = a[b];
    a.Vy ? a.Vy++ : a.Vy = 1;
    try {
      for(var k = a.length, l = 0;l < k;l++) {
        var n = a[l];
        n && !n.Cp && (g &= A.G.KC(n, f) !== q)
      }
    }finally {
      a.Vy--, A.G.dH(c, d, b, a)
    }
  }
  return Boolean(g)
};
A.G.KC = function(a, b) {
  var c = a.handleEvent(b);
  a.MB && A.G.LA(a.key);
  return c
};
A.G.QQ = function() {
  return A.object.Tm(A.G.fe)
};
A.G.dispatchEvent = function(a, b) {
  var c = b.type || b, d = A.G.Pg;
  if(!(c in d)) {
    return j
  }
  if(A.Qc(b)) {
    b = new ha(b, a)
  }else {
    if(b instanceof ha) {
      b.target = b.target || a
    }else {
      var f = b, b = new ha(c, a);
      A.object.extend(b, f)
    }
  }
  var f = 1, g, d = d[c], c = j in d, k;
  if(c) {
    g = [];
    for(k = a;k;k = k.XE) {
      g.push(k)
    }
    k = d[j];
    k.Bi = k.zc;
    for(var l = g.length - 1;!b.Ap && 0 <= l && k.Bi;l--) {
      b.currentTarget = g[l], f &= A.G.Mq(k, g[l], b.type, j, b) && b.uv != q
    }
  }
  if(q in d) {
    if(k = d[q], k.Bi = k.zc, c) {
      for(l = 0;!b.Ap && l < g.length && k.Bi;l++) {
        b.currentTarget = g[l], f &= A.G.Mq(k, g[l], b.type, q, b) && b.uv != q
      }
    }else {
      for(d = a;!b.Ap && d && k.Bi;d = d.XE) {
        b.currentTarget = d, f &= A.G.Mq(k, d, b.type, q, b) && b.uv != q
      }
    }
  }
  return Boolean(f)
};
A.G.PR = function(a) {
  A.G.ju = a.kK(A.G.ju)
};
A.G.ju = function(a, b) {
  if(!A.G.fe[a]) {
    return j
  }
  var c = A.G.fe[a], d = c.type, f = A.G.Pg;
  if(!(d in f)) {
    return j
  }
  var f = f[d], g, k;
  if(!ea) {
    g = b || A.EM();
    var l = j in f, n = q in f;
    if(l) {
      if(A.G.iN(g)) {
        return j
      }
      A.G.sN(g)
    }
    var m = new la;
    m.Ha(g, this);
    g = j;
    try {
      if(l) {
        for(var o = [], v = m.currentTarget;v;v = v.parentNode) {
          o.push(v)
        }
        k = f[j];
        k.Bi = k.zc;
        for(var w = o.length - 1;!m.Ap && 0 <= w && k.Bi;w--) {
          m.currentTarget = o[w], g &= A.G.Mq(k, o[w], d, j, m)
        }
        if(n) {
          k = f[q];
          k.Bi = k.zc;
          for(w = 0;!m.Ap && w < o.length && k.Bi;w++) {
            m.currentTarget = o[w], g &= A.G.Mq(k, o[w], d, q, m)
          }
        }
      }else {
        g = A.G.KC(c, m)
      }
    }finally {
      o && (o.length = 0), m.aj()
    }
    return g
  }
  d = new la(b, this);
  try {
    g = A.G.KC(c, d)
  }finally {
    d.aj()
  }
  return g
};
A.G.sN = function(a) {
  var b = q;
  if(0 == a.keyCode) {
    try {
      a.keyCode = -1;
      return
    }catch(c) {
      b = j
    }
  }
  if(b || a.returnValue == h) {
    a.returnValue = j
  }
};
A.G.iN = function(a) {
  return 0 > a.keyCode || a.returnValue != h
};
A.G.jO = 0;
A.G.RQ = function(a) {
  return a + "_" + A.G.jO++
};
A.debug.Xe.kF(function(a) {
  A.G.ju = a(A.G.ju)
});
function pa() {
}
A.e(pa, ga);
z = pa.prototype;
z.AH = j;
z.XE = p;
z.addEventListener = function(a, b, c, d) {
  A.G.nb(this, a, b, c, d)
};
z.removeEventListener = function(a, b, c, d) {
  A.G.ed(this, a, b, c, d)
};
z.dispatchEvent = function(a) {
  return A.G.dispatchEvent(this, a)
};
z.Em = function() {
  pa.f.Em.call(this);
  A.G.rK(this);
  this.XE = p
};
var qa = A.global.window;
A.Eu = {};
A.Eu.kN = function(a) {
  return/^\s*$/.test(a) ? q : /^[\],:{}\s\u2028\u2029]*$/.test(a.replace(/\\["\\\/bfnrtu]/g, "@").replace(/"[^"\\\n\r\u2028\u2029\x00-\x08\x10-\x1f\x80-\x9f]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, "]").replace(/(?:^|:|,)(?:[\s\u2028\u2029]*\[)+/g, ""))
};
A.Eu.parse = function(a) {
  a = "" + a;
  if(A.Eu.kN(a)) {
    try {
      return eval("(" + a + ")")
    }catch(b) {
    }
  }
  e(Error("Invalid JSON string: " + a))
};
A.Eu.rS = function(a) {
  return eval("(" + a + ")")
};
A.Eu.mb = function(a, b) {
  return(new sa(b)).mb(a)
};
function sa(a) {
  this.Jz = a
}
sa.prototype.mb = function(a) {
  var b = [];
  ta(this, a, b);
  return b.join("")
};
function ta(a, b, c) {
  switch(typeof b) {
    case "string":
      ua(b, c);
      break;
    case "number":
      c.push(isFinite(b) && !isNaN(b) ? b : "null");
      break;
    case "boolean":
      c.push(b);
      break;
    case "undefined":
      c.push("null");
      break;
    case "object":
      if(b == p) {
        c.push("null");
        break
      }
      if(A.isArray(b)) {
        var d = b.length;
        c.push("[");
        for(var f = "", g = 0;g < d;g++) {
          c.push(f), f = b[g], ta(a, a.Jz ? a.Jz.call(b, "" + g, f) : f, c), f = ","
        }
        c.push("]");
        break
      }
      c.push("{");
      d = "";
      for(g in b) {
        Object.prototype.hasOwnProperty.call(b, g) && (f = b[g], "function" != typeof f && (c.push(d), ua(g, c), c.push(":"), ta(a, a.Jz ? a.Jz.call(b, g, f) : f, c), d = ","))
      }
      c.push("}");
      break;
    case "function":
      break;
    default:
      e(Error("Unknown type: " + typeof b))
  }
}
var va = {'"':'\\"', "\\":"\\\\", "/":"\\/", "\u0008":"\\b", "\u000c":"\\f", "\n":"\\n", "\r":"\\r", "\t":"\\t", "\x0B":"\\u000b"}, wa = /\uffff/.test("\uffff") ? /[\\\"\x00-\x1f\x7f-\uffff]/g : /[\\\"\x00-\x1f\x7f-\xff]/g;
function ua(a, b) {
  b.push('"', a.replace(wa, function(a) {
    if(a in va) {
      return va[a]
    }
    var b = a.charCodeAt(0), f = "\\u";
    16 > b ? f += "000" : 256 > b ? f += "00" : 4096 > b && (f += "0");
    return va[a] = f + b.toString(16)
  }), '"')
}
;A.kj = {};
function xa() {
}
xa.prototype.XG = p;
xa.prototype.du = function() {
  var a;
  if(!(a = this.XG)) {
    a = {}, ya(this) && (a[0] = j, a[1] = j), a = this.XG = a
  }
  return a
};
function za(a, b) {
  this.tO = a;
  this.EN = b
}
A.e(za, xa);
za.prototype.WB = function() {
  return this.tO()
};
za.prototype.du = function() {
  return this.EN()
};
A.kj.Jj = function() {
  return A.kj.Jj.fI.WB()
};
A.kj.Jj.du = function() {
  return A.kj.Jj.fI.du()
};
A.kj.Jj.$R = function(a, b) {
  A.kj.Jj.AK(new za(a, b))
};
A.kj.Jj.AK = function(a) {
  A.kj.Jj.fI = a
};
function Aa() {
}
A.e(Aa, xa);
Aa.prototype.WB = function() {
  var a = ya(this);
  return a ? new ActiveXObject(a) : new XMLHttpRequest
};
Aa.prototype.FD = p;
function ya(a) {
  if(!a.FD && "undefined" == typeof XMLHttpRequest && "undefined" != typeof ActiveXObject) {
    for(var b = ["MSXML2.XMLHTTP.6.0", "MSXML2.XMLHTTP.3.0", "MSXML2.XMLHTTP", "Microsoft.XMLHTTP"], c = 0;c < b.length;c++) {
      var d = b[c];
      try {
        return new ActiveXObject(d), a.FD = d
      }catch(f) {
      }
    }
    e(Error("Could not create ActiveXObject. ActiveX might be disabled, or MSXML might not be installed"))
  }
  return a.FD
}
A.kj.Jj.AK(new Aa);
A.zd = {};
A.zd.Tm = function(a) {
  return"function" == typeof a.Tm ? a.Tm() : A.$e(a) || A.Qc(a) ? a.length : A.object.Tm(a)
};
A.zd.Ff = function(a) {
  if("function" == typeof a.Ff) {
    return a.Ff()
  }
  if(A.Qc(a)) {
    return a.split("")
  }
  if(A.$e(a)) {
    for(var b = [], c = a.length, d = 0;d < c;d++) {
      b.push(a[d])
    }
    return b
  }
  return A.object.Ff(a)
};
A.zd.lg = function(a) {
  if("function" == typeof a.lg) {
    return a.lg()
  }
  if("function" != typeof a.Ff) {
    if(A.$e(a) || A.Qc(a)) {
      for(var b = [], a = a.length, c = 0;c < a;c++) {
        b.push(c)
      }
      return b
    }
    return A.object.lg(a)
  }
};
A.zd.contains = function(a, b) {
  return"function" == typeof a.contains ? a.contains(b) : "function" == typeof a.yq ? a.yq(b) : A.$e(a) || A.Qc(a) ? A.$.contains(a, b) : A.object.yq(a, b)
};
A.zd.ri = function(a) {
  return"function" == typeof a.ri ? a.ri() : A.$e(a) || A.Qc(a) ? A.$.ri(a) : A.object.ri(a)
};
A.zd.clear = function(a) {
  "function" == typeof a.clear ? a.clear() : A.$e(a) ? A.$.clear(a) : A.object.clear(a)
};
A.zd.forEach = function(a, b, c) {
  if("function" == typeof a.forEach) {
    a.forEach(b, c)
  }else {
    if(A.$e(a) || A.Qc(a)) {
      A.$.forEach(a, b, c)
    }else {
      for(var d = A.zd.lg(a), f = A.zd.Ff(a), g = f.length, k = 0;k < g;k++) {
        b.call(c, f[k], d && d[k], a)
      }
    }
  }
};
A.zd.filter = function(a, b, c) {
  if("function" == typeof a.filter) {
    return a.filter(b, c)
  }
  if(A.$e(a) || A.Qc(a)) {
    return A.$.filter(a, b, c)
  }
  var d, f = A.zd.lg(a), g = A.zd.Ff(a), k = g.length;
  if(f) {
    d = {};
    for(var l = 0;l < k;l++) {
      b.call(c, g[l], f[l], a) && (d[f[l]] = g[l])
    }
  }else {
    d = [];
    for(l = 0;l < k;l++) {
      b.call(c, g[l], h, a) && d.push(g[l])
    }
  }
  return d
};
A.zd.map = function(a, b, c) {
  if("function" == typeof a.map) {
    return a.map(b, c)
  }
  if(A.$e(a) || A.Qc(a)) {
    return A.$.map(a, b, c)
  }
  var d, f = A.zd.lg(a), g = A.zd.Ff(a), k = g.length;
  if(f) {
    d = {};
    for(var l = 0;l < k;l++) {
      d[f[l]] = b.call(c, g[l], f[l], a)
    }
  }else {
    d = [];
    for(l = 0;l < k;l++) {
      d[l] = b.call(c, g[l], h, a)
    }
  }
  return d
};
A.zd.some = function(a, b, c) {
  if("function" == typeof a.some) {
    return a.some(b, c)
  }
  if(A.$e(a) || A.Qc(a)) {
    return A.$.some(a, b, c)
  }
  for(var d = A.zd.lg(a), f = A.zd.Ff(a), g = f.length, k = 0;k < g;k++) {
    if(b.call(c, f[k], d && d[k], a)) {
      return j
    }
  }
  return q
};
A.zd.every = function(a, b, c) {
  if("function" == typeof a.every) {
    return a.every(b, c)
  }
  if(A.$e(a) || A.Qc(a)) {
    return A.$.every(a, b, c)
  }
  for(var d = A.zd.lg(a), f = A.zd.Ff(a), g = f.length, k = 0;k < g;k++) {
    if(!b.call(c, f[k], d && d[k], a)) {
      return q
    }
  }
  return j
};
A.sb = {};
A.sb.Of = "StopIteration" in A.global ? A.global.StopIteration : Error("StopIteration");
function Ba() {
}
Ba.prototype.next = function() {
  e(A.sb.Of)
};
Ba.prototype.iB = function() {
  return this
};
A.sb.Sh = function(a) {
  if(a instanceof Ba) {
    return a
  }
  if("function" == typeof a.iB) {
    return a.iB(q)
  }
  if(A.$e(a)) {
    var b = 0, c = new Ba;
    c.next = function() {
      for(;;) {
        b >= a.length && e(A.sb.Of);
        if(b in a) {
          return a[b++]
        }
        b++
      }
    };
    return c
  }
  e(Error("Not implemented"))
};
A.sb.forEach = function(a, b, c) {
  if(A.$e(a)) {
    try {
      A.$.forEach(a, b, c)
    }catch(d) {
      d !== A.sb.Of && e(d)
    }
  }else {
    a = A.sb.Sh(a);
    try {
      for(;;) {
        b.call(c, a.next(), h, a)
      }
    }catch(f) {
      f !== A.sb.Of && e(f)
    }
  }
};
A.sb.filter = function(a, b, c) {
  var a = A.sb.Sh(a), d = new Ba;
  d.next = function() {
    for(;;) {
      var d = a.next();
      if(b.call(c, d, h, a)) {
        return d
      }
    }
  };
  return d
};
A.sb.SR = function(a, b, c) {
  var d = 0, f = a, g = c || 1;
  1 < arguments.length && (d = a, f = b);
  0 == g && e(Error("Range step argument must not be zero"));
  var k = new Ba;
  k.next = function() {
    (g > 0 && d >= f || g < 0 && d <= f) && e(A.sb.Of);
    var a = d;
    d = d + g;
    return a
  };
  return k
};
A.sb.join = function(a, b) {
  return A.sb.kG(a).join(b)
};
A.sb.map = function(a, b, c) {
  var a = A.sb.Sh(a), d = new Ba;
  d.next = function() {
    for(;;) {
      var d = a.next();
      return b.call(c, d, h, a)
    }
  };
  return d
};
A.sb.reduce = function(a, b, c, d) {
  var f = c;
  A.sb.forEach(a, function(a) {
    f = b.call(d, f, a)
  });
  return f
};
A.sb.some = function(a, b, c) {
  a = A.sb.Sh(a);
  try {
    for(;;) {
      if(b.call(c, a.next(), h, a)) {
        return j
      }
    }
  }catch(d) {
    d !== A.sb.Of && e(d)
  }
  return q
};
A.sb.every = function(a, b, c) {
  a = A.sb.Sh(a);
  try {
    for(;;) {
      if(!b.call(c, a.next(), h, a)) {
        return q
      }
    }
  }catch(d) {
    d !== A.sb.Of && e(d)
  }
  return j
};
A.sb.eQ = function(a) {
  var b = arguments, c = b.length, d = 0, f = new Ba;
  f.next = function() {
    try {
      return d >= c && e(A.sb.Of), A.sb.Sh(b[d]).next()
    }catch(a) {
      return(a !== A.sb.Of || d >= c) && e(a), d++, this.next()
    }
  };
  return f
};
A.sb.rQ = function(a, b, c) {
  var a = A.sb.Sh(a), d = new Ba, f = j;
  d.next = function() {
    for(;;) {
      var d = a.next();
      if(!f || !b.call(c, d, h, a)) {
        return f = q, d
      }
    }
  };
  return d
};
A.sb.jS = function(a, b, c) {
  var a = A.sb.Sh(a), d = new Ba, f = j;
  d.next = function() {
    for(;;) {
      if(f) {
        var d = a.next();
        if(b.call(c, d, h, a)) {
          return d
        }
        f = q
      }else {
        e(A.sb.Of)
      }
    }
  };
  return d
};
A.sb.kG = function(a) {
  if(A.$e(a)) {
    return A.$.kG(a)
  }
  var a = A.sb.Sh(a), b = [];
  A.sb.forEach(a, function(a) {
    b.push(a)
  });
  return b
};
A.sb.CC = function(a, b) {
  var a = A.sb.Sh(a), b = A.sb.Sh(b), c, d;
  try {
    for(;;) {
      c = d = q;
      var f = a.next();
      c = j;
      var g = b.next();
      d = j;
      if(f != g) {
        break
      }
    }
  }catch(k) {
    k !== A.sb.Of && e(k);
    if(c && !d) {
      return q
    }
    if(!d) {
      try {
        b.next()
      }catch(l) {
        return l !== A.sb.Of && e(l), j
      }
    }
  }
  return q
};
A.sb.HR = function(a, b) {
  try {
    return A.sb.Sh(a).next()
  }catch(c) {
    return c != A.sb.Of && e(c), b
  }
};
A.sb.product = function(a) {
  if(A.$.some(arguments, function(a) {
    return!a.length
  }) || !arguments.length) {
    return new Ba
  }
  var b = new Ba, c = arguments, d = A.$.repeat(0, c.length);
  b.next = function() {
    if(d) {
      for(var a = A.$.map(d, function(a, b) {
        return c[b][a]
      }), b = d.length - 1;0 <= b;b--) {
        if(d[b] < c[b].length - 1) {
          d[b]++;
          break
        }
        if(0 == b) {
          d = p;
          break
        }
        d[b] = 0
      }
      return a
    }
    e(A.sb.Of)
  };
  return b
};
A.sb.oQ = function(a) {
  var b = A.sb.Sh(a), c = [], d = 0, a = new Ba, f = q;
  a.next = function() {
    var a = p;
    if(!f) {
      try {
        return a = b.next(), c.push(a), a
      }catch(k) {
        (k != A.sb.Of || A.$.ri(c)) && e(k), f = j
      }
    }
    a = c[d];
    d = (d + 1) % c.length;
    return a
  };
  return a
};
function Ca(a, b) {
  this.Rg = {};
  this.pb = [];
  var c = arguments.length;
  if(1 < c) {
    c % 2 && e(Error("Uneven number of arguments"));
    for(var d = 0;d < c;d += 2) {
      this.set(arguments[d], arguments[d + 1])
    }
  }else {
    if(a) {
      a instanceof Ca ? (c = a.lg(), d = a.Ff()) : (c = A.object.lg(a), d = A.object.Ff(a));
      for(var f = 0;f < c.length;f++) {
        this.set(c[f], d[f])
      }
    }
  }
}
z = Ca.prototype;
z.zc = 0;
z.iw = 0;
z.Tm = u("zc");
z.Ff = function() {
  Da(this);
  for(var a = [], b = 0;b < this.pb.length;b++) {
    a.push(this.Rg[this.pb[b]])
  }
  return a
};
z.lg = function() {
  Da(this);
  return this.pb.concat()
};
z.Xj = function(a) {
  return Ea(this.Rg, a)
};
z.yq = function(a) {
  for(var b = 0;b < this.pb.length;b++) {
    var c = this.pb[b];
    if(Ea(this.Rg, c) && this.Rg[c] == a) {
      return j
    }
  }
  return q
};
z.CC = function(a, b) {
  if(this === a) {
    return j
  }
  if(this.zc != a.Tm()) {
    return q
  }
  var c = b || Fa;
  Da(this);
  for(var d, f = 0;d = this.pb[f];f++) {
    if(!c(this.get(d), a.get(d))) {
      return q
    }
  }
  return j
};
function Fa(a, b) {
  return a === b
}
z.ri = function() {
  return 0 == this.zc
};
z.clear = function() {
  this.Rg = {};
  this.iw = this.zc = this.pb.length = 0
};
z.remove = function(a) {
  return Ea(this.Rg, a) ? (delete this.Rg[a], this.zc--, this.iw++, this.pb.length > 2 * this.zc && Da(this), j) : q
};
function Da(a) {
  if(a.zc != a.pb.length) {
    for(var b = 0, c = 0;b < a.pb.length;) {
      var d = a.pb[b];
      Ea(a.Rg, d) && (a.pb[c++] = d);
      b++
    }
    a.pb.length = c
  }
  if(a.zc != a.pb.length) {
    for(var f = {}, c = b = 0;b < a.pb.length;) {
      d = a.pb[b], Ea(f, d) || (a.pb[c++] = d, f[d] = 1), b++
    }
    a.pb.length = c
  }
}
z.get = function(a, b) {
  return Ea(this.Rg, a) ? this.Rg[a] : b
};
z.set = function(a, b) {
  Ea(this.Rg, a) || (this.zc++, this.pb.push(a), this.iw++);
  this.Rg[a] = b
};
z.Ca = function() {
  return new Ca(this)
};
z.fO = function() {
  for(var a = new Ca, b = 0;b < this.pb.length;b++) {
    var c = this.pb[b];
    a.set(this.Rg[c], c)
  }
  return a
};
z.iB = function(a) {
  Da(this);
  var b = 0, c = this.pb, d = this.Rg, f = this.iw, g = this, k = new Ba;
  k.next = function() {
    for(;;) {
      f != g.iw && e(Error("The map has changed since the iterator was created"));
      b >= c.length && e(A.sb.Of);
      var k = c[b++];
      return a ? k : d[k]
    }
  };
  return k
};
function Ea(a, b) {
  return Object.prototype.hasOwnProperty.call(a, b)
}
;A.uri = {};
A.uri.R = {};
A.uri.R.UG = function(a, b, c, d, f, g, k) {
  var l = [];
  a && l.push(a, ":");
  c && (l.push("//"), b && l.push(b, "@"), l.push(c), d && l.push(":", d));
  f && l.push(f);
  g && l.push("?", g);
  k && l.push("#", k);
  return l.join("")
};
A.uri.R.$N = RegExp("^(?:([^:/?#.]+):)?(?://(?:([^/?#]*)@)?([\\w\\d\\-\\u0100-\\uffff.%]*)(?::([0-9]+))?)?([^?#]+)?(?:\\?([^#]*))?(?:#(.*))?$");
A.uri.R.split = function(a) {
  return a.match(A.uri.R.$N)
};
A.uri.R.sx = function(a) {
  return a && decodeURIComponent(a)
};
A.uri.R.Uq = function(a, b) {
  return A.uri.R.split(b)[a] || p
};
A.uri.R.mD = function(a) {
  return A.uri.R.Uq(1, a)
};
A.uri.R.yM = function(a) {
  a = A.uri.R.mD(a);
  !a && self.location && (a = self.location.protocol, a = a.substr(0, a.length - 1));
  return a ? a.toLowerCase() : ""
};
A.uri.R.LM = function(a) {
  return A.uri.R.Uq(2, a)
};
A.uri.R.JI = function(a) {
  return A.uri.R.sx(A.uri.R.LM(a))
};
A.uri.R.xM = function(a) {
  return A.uri.R.Uq(3, a)
};
A.uri.R.rI = function(a) {
  return A.uri.R.sx(A.uri.R.xM(a))
};
A.uri.R.iD = function(a) {
  return Number(A.uri.R.Uq(4, a)) || p
};
A.uri.R.GM = function(a) {
  return A.uri.R.Uq(5, a)
};
A.uri.R.eu = function(a) {
  return A.uri.R.sx(A.uri.R.GM(a))
};
A.uri.R.AI = function(a) {
  return A.uri.R.Uq(6, a)
};
A.uri.R.AM = function(a) {
  var b = a.indexOf("#");
  return 0 > b ? p : a.substr(b + 1)
};
A.uri.R.aS = function(a, b) {
  return A.uri.R.MN(a) + (b ? "#" + b : "")
};
A.uri.R.tI = function(a) {
  return A.uri.R.sx(A.uri.R.AM(a))
};
A.uri.R.HQ = function(a) {
  a = A.uri.R.split(a);
  return A.uri.R.UG(a[1], a[2], a[3], a[4])
};
A.uri.R.PQ = function(a) {
  a = A.uri.R.split(a);
  return A.uri.R.UG(p, p, p, p, a[5], a[6], a[7])
};
A.uri.R.MN = function(a) {
  var b = a.indexOf("#");
  return 0 > b ? a : a.substr(0, b)
};
A.uri.R.YQ = function(a, b) {
  var c = A.uri.R.split(a), d = A.uri.R.split(b);
  return c[3] == d[3] && c[1] == d[1] && c[4] == d[4]
};
A.uri.R.xL = function(a) {
  A.dB && (0 <= a.indexOf("#") || 0 <= a.indexOf("?")) && e(Error("goog.uri.utils: Fragment or query identifiers are not supported: [" + a + "]"))
};
A.uri.R.lB = function(a) {
  if(a[1]) {
    var b = a[0], c = b.indexOf("#");
    0 <= c && (a.push(b.substr(c)), a[0] = b = b.substr(0, c));
    c = b.indexOf("?");
    0 > c ? a[1] = "?" : c == b.length - 1 && (a[1] = h)
  }
  return a.join("")
};
A.uri.R.PG = function(a, b, c) {
  if(A.isArray(b)) {
    for(var d = 0;d < b.length;d++) {
      c.push("&", a), "" !== b[d] && c.push("=", A.U.us(b[d]))
    }
  }else {
    b != p && (c.push("&", a), "" !== b && c.push("=", A.U.us(b)))
  }
};
A.uri.R.CB = function(a, b, c) {
  for(c = c || 0;c < b.length;c += 2) {
    A.uri.R.PG(b[c], b[c + 1], a)
  }
  return a
};
A.uri.R.ZP = function(a, b) {
  var c = A.uri.R.CB([], a, b);
  c[0] = "";
  return c.join("")
};
A.uri.R.VG = function(a, b) {
  for(var c in b) {
    A.uri.R.PG(c, b[c], a)
  }
  return a
};
A.uri.R.$P = function(a) {
  a = A.uri.R.VG([], a);
  a[0] = "";
  return a.join("")
};
A.uri.R.IP = function(a, b) {
  return A.uri.R.lB(2 == arguments.length ? A.uri.R.CB([a], arguments[1], 0) : A.uri.R.CB([a], arguments, 1))
};
A.uri.R.JP = function(a, b) {
  return A.uri.R.lB(A.uri.R.VG([a], b))
};
A.uri.R.wL = function(a, b) {
  return A.uri.R.lB([a, "&", "zx", "=", A.U.us(b)])
};
A.uri.R.Ix = function(a, b, c, d) {
  for(var f = c.length;0 <= (b = a.indexOf(c, b)) && b < d;) {
    var g = a.charCodeAt(b - 1);
    if(38 == g || 63 == g) {
      if(g = a.charCodeAt(b + f), !g || 61 == g || 38 == g || 35 == g) {
        return b
      }
    }
    b += f + 1
  }
  return-1
};
A.uri.R.ly = /#|$/;
A.uri.R.WQ = function(a, b) {
  return 0 <= A.uri.R.Ix(a, 0, b, a.search(A.uri.R.ly))
};
A.uri.R.NQ = function(a, b) {
  var c = a.search(A.uri.R.ly), d = A.uri.R.Ix(a, 0, b, c);
  if(0 > d) {
    return p
  }
  var f = a.indexOf("&", d);
  if(0 > f || f > c) {
    f = c
  }
  d += b.length + 1;
  return A.U.OA(a.substr(d, f - d))
};
A.uri.R.OQ = function(a, b) {
  for(var c = a.search(A.uri.R.ly), d = 0, f, g = [];0 <= (f = A.uri.R.Ix(a, d, b, c));) {
    d = a.indexOf("&", f);
    if(0 > d || d > c) {
      d = c
    }
    f += b.length + 1;
    g.push(A.U.OA(a.substr(f, d - f)))
  }
  return g
};
A.uri.R.eO = /[?&]($|#)/;
A.uri.R.NN = function(a) {
  for(var b = a.search(A.uri.R.ly), c = 0, d, f = [];0 <= (d = A.uri.R.Ix(a, c, "zx", b));) {
    f.push(a.substring(c, d)), c = Math.min(a.indexOf("&", d) + 1 || b, b)
  }
  f.push(a.substr(c));
  return f.join("").replace(A.uri.R.eO, "$1")
};
A.uri.R.WN = function(a) {
  var b = A.U.BI();
  return A.uri.R.wL(A.uri.R.NN(a), b)
};
A.uri.R.KP = function(a, b) {
  A.uri.R.xL(a);
  A.U.gM(a) && (a = a.substr(0, a.length - 1));
  A.U.aO(b) && (b = b.substr(1));
  return A.U.BL(a, "/", b)
};
A.uri.R.rN = function(a) {
  return A.uri.R.WN(a)
};
function Ga(a) {
  pa.call(this);
  this.headers = new Ca;
  this.ZA = a || p
}
A.e(Ga, pa);
Ga.GG = "Content-Type";
Ga.pL = /^https?$/i;
Ga.mL = "application/x-www-form-urlencoded;charset=utf-8";
Ga.tF = [];
Ga.send = function(a, b, c, d, f, g) {
  var k = new Ga;
  Ga.tF.push(k);
  b && A.G.nb(k, "complete", b);
  A.G.nb(k, "ready", A.fK(Ga.DL, k));
  g && (k.jG = Math.max(0, g));
  k.send(a, c, d, f)
};
Ga.fQ = function() {
  for(var a = Ga.tF;a.length;) {
    a.pop().aj()
  }
};
Ga.QR = function(a) {
  Ga.prototype.dv = a.kK(Ga.prototype.dv)
};
Ga.DL = function(a) {
  a.aj();
  A.$.remove(Ga.tF, a)
};
z = Ga.prototype;
z.im = q;
z.Bc = p;
z.YA = p;
z.BJ = "";
z.DC = q;
z.qy = q;
z.HD = q;
z.No = q;
z.jG = 0;
z.Up = p;
z.uK = "";
z.vG = q;
z.send = function(a, b, c, d) {
  this.Bc && e(Error("[goog.net.XhrIo] Object is active with another request"));
  b = b ? b.toUpperCase() : "GET";
  this.BJ = a;
  this.DC = q;
  this.im = j;
  this.Bc = this.ZA ? this.ZA.WB() : A.kj.Jj();
  this.YA = this.ZA ? this.ZA.du() : A.kj.Jj.du();
  this.Bc.onreadystatechange = A.bind(this.eK, this);
  try {
    this.HD = j, this.Bc.open(b, a, j), this.HD = q
  }catch(f) {
    Ha(this);
    return
  }
  var a = c || "", g = this.headers.Ca();
  d && A.zd.forEach(d, function(a, b) {
    g.set(b, a)
  });
  "POST" == b && !g.Xj(Ga.GG) && g.set(Ga.GG, Ga.mL);
  A.zd.forEach(g, function(a, b) {
    this.Bc.setRequestHeader(b, a)
  }, this);
  this.uK && (this.Bc.responseType = this.uK);
  A.object.Xj(this.Bc, "withCredentials") && (this.Bc.withCredentials = this.vG);
  try {
    this.Up && (qa.clearTimeout(this.Up), this.Up = p), 0 < this.jG && (this.Up = qa.setTimeout(A.bind(this.cO, this), this.jG)), this.qy = j, this.Bc.send(a), this.qy = q
  }catch(k) {
    Ha(this)
  }
};
z.cO = function() {
  "undefined" != typeof A && this.Bc && (this.dispatchEvent("timeout"), this.abort(8))
};
function Ha(a) {
  a.im = q;
  a.Bc && (a.No = j, a.Bc.abort(), a.No = q);
  Ia(a);
  Ja(a)
}
function Ia(a) {
  a.DC || (a.DC = j, a.dispatchEvent("complete"), a.dispatchEvent("error"))
}
z.abort = function() {
  this.Bc && this.im && (this.im = q, this.No = j, this.Bc.abort(), this.No = q, this.dispatchEvent("complete"), this.dispatchEvent("abort"), Ja(this))
};
z.Em = function() {
  this.Bc && (this.im && (this.im = q, this.No = j, this.Bc.abort(), this.No = q), Ja(this, j));
  Ga.f.Em.call(this)
};
z.eK = function() {
  !this.HD && !this.qy && !this.No ? this.dv() : Ka(this)
};
z.dv = function() {
  Ka(this)
};
function Ka(a) {
  if(a.im && "undefined" != typeof A && (!a.YA[1] || !(4 == (a.Bc ? a.Bc.readyState : 0) && 2 == La(a)))) {
    if(a.qy && 4 == (a.Bc ? a.Bc.readyState : 0)) {
      qa.setTimeout(A.bind(a.eK, a), 0)
    }else {
      if(a.dispatchEvent("readystatechange"), 4 == (a.Bc ? a.Bc.readyState : 0)) {
        a.im = q;
        var b = La(a), c;
        a: {
          switch(b) {
            case 200:
            ;
            case 201:
            ;
            case 202:
            ;
            case 204:
            ;
            case 304:
            ;
            case 1223:
              c = j;
              break a;
            default:
              c = q
          }
        }
        if(!c) {
          if(b = 0 === b) {
            b = A.uri.R.yM("" + a.BJ), b = !Ga.pL.test(b)
          }
          c = b
        }
        c ? (a.dispatchEvent("complete"), a.dispatchEvent("success")) : Ia(a);
        Ja(a)
      }
    }
  }
}
function Ja(a, b) {
  if(a.Bc) {
    var c = a.Bc, d = a.YA[0] ? A.XJ : p;
    a.Bc = p;
    a.YA = p;
    a.Up && (qa.clearTimeout(a.Up), a.Up = p);
    b || a.dispatchEvent("ready");
    try {
      c.onreadystatechange = d
    }catch(f) {
    }
  }
}
function La(a) {
  try {
    return 2 < (a.Bc ? a.Bc.readyState : 0) ? a.Bc.status : -1
  }catch(b) {
    return-1
  }
}
A.debug.Xe.kF(function(a) {
  Ga.prototype.dv = a(Ga.prototype.dv)
});
function Ma(a) {
  a = typeof a;
  return"string" == a || "number" == a || "boolean" == a
}
function Na(a) {
  return"string" != typeof a && "undefined" != typeof a.length
}
function Oa(a, b) {
  var c = "<" + a;
  "undefined" != typeof b.functionName && (b["function"] = b.functionName, delete b.functionName);
  for(var d in b) {
    "format" != d && "text" != d && "custom_attribute_value" != d && "attr" != d && Ma(b[d]) && (c += " " + d + '="' + b[d] + '"')
  }
  c += ">";
  for(d in b) {
    if("arg" == d && Na(b[d])) {
      for(var f = b[d], g = 0;g < f.length;g++) {
        c += "<arg><![CDATA[" + f[g] + "]]\></arg>"
      }
    }else {
      if("custom_attribute_value" == d || "attr" == d) {
        c += "<![CDATA[" + b[d] + "]]\>"
      }else {
        if("format" == d || "text" == d) {
          c += "<" + d + "><![CDATA[" + b[d] + "]]\></" + d + ">"
        }else {
          if(Na(b[d])) {
            f = b[d];
            for(g = 0;g < f.length;g++) {
              c += Oa(d, f[g])
            }
          }else {
            Ma(b[d]) || (c += Oa(d, b[d]))
          }
        }
      }
    }
  }
  return c + ("</" + a + ">")
}
function Pa() {
}
z = Pa.prototype;
z.oa = p;
z.B = p;
z.Om = p;
z.nz = p;
z.mz = p;
z.UJ = p;
z.LE = p;
z.Hx = p;
z.Ha = function(a) {
  this.oa = a;
  this.LE = this.UJ = this.mz = this.nz = this.vl = q;
  this.ai = this.oa.enableFirefoxPrintPreviewFix
};
z.vl = q;
z.$J = function() {
  this.vl = j;
  this.nz && this.Hi(this.oa.xmlFile);
  this.mz && this.Oh(this.oa.xmlData);
  this.UJ && this.Kn(this.oa.jsonFile);
  this.LE && this.wj(this.oa.jsonData)
};
z.aK = function() {
  this.ai && Qa(this)
};
z.Nc = x(p);
z.mu = function() {
  this.oa.visible = q;
  this.B && (this.B.style.visibility = "hidden", this.B.setAttribute("width", "1px"), this.B.setAttribute("height", "1px"))
};
z.show = function() {
  this.oa.visible = j;
  this.B && (this.B.style.visibility = "visible", this.B.setAttribute("width", this.ab + ""), this.B.setAttribute("height", this.ob + ""))
};
z.Fq = function(a, b) {
  a && "OBJECT" == a.nodeName && (B.platform.sr && B.platform.Zo ? (a.style.display = "none", function() {
    4 == a.readyState ? (B.platform.wk && b != p && B.Jt(window[b]), B.Jt(a)) : setTimeout(arguments.callee, 10)
  }()) : a.parentNode.removeChild(a))
};
z.Jt = function(a) {
  for(var b in a) {
    "function" == typeof a[b] && (a[b] = p)
  }
  a.parentNode && a.parentNode.removeChild(a)
};
z.lo = function() {
  return B.platform.BD && this.oa.swfFile != p
};
z.write = function(a) {
  var b = this.oa.width + "", c = this.oa.height + "", d = this.oa.preloaderSWFFile ? this.oa.preloaderSWFFile : this.oa.swfFile;
  if(this.ai = this.ai && B.platform.fJ) {
    this.Hx = new Ra
  }
  if(B.platform.sr && B.platform.Zo) {
    var f;
    f = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"' + (' id="' + this.oa.id + '"');
    f = f + (' width="' + b + '"') + (' height="' + c + '"') + (' style="visibility:' + (this.oa.visible ? "visible" : "hidden") + '"');
    f += ' codebase="' + B.platform.protocol + '//fpdownload.macromedia.com/get/flashplayer/current/swflash.cab">';
    f += Sa("movie", d);
    f += Sa("bgcolor", this.oa.bgColor);
    f += Sa("allowScriptAccess", "always");
    f += Sa("flashvars", Ta(this));
    this.oa.wMode != p && (f += Sa("wmode", this.oa.wMode));
    f += "</object>";
    if(B.platform.wk) {
      b = p;
      for(c = a;c;) {
        if(c.nodeName != p && "form" == c.nodeName.toLowerCase()) {
          b = c;
          break
        }
        c = c.parentNode
      }
      if(b != p) {
        window[this.oa.id] = {};
        window[this.oa.id].rL = s();
        a.innerHTML = f;
        window[this.oa.id].rL = p;
        var a = {}, g;
        for(g in window[this.oa.id]) {
          "function" == typeof window[this.oa.id][g] && (a[g] = window[this.oa.id][g])
        }
        this.B = window[this.oa.id] = b[this.oa.id];
        this.oa.flashObject = this.B;
        for(g in a) {
          Ua(g)
        }
      }else {
        a.innerHTML = f, this.B = document.getElementById(this.oa.id), this.oa.flashObject = this.B
      }
    }else {
      a.innerHTML = f, this.B = document.getElementById(this.oa.id), this.oa.flashObject = this.B
    }
  }else {
    g = document.createElement("object");
    g.setAttribute("type", "application/x-shockwave-flash");
    g.setAttribute("id", this.oa.id);
    g.setAttribute("width", b);
    g.setAttribute("height", c);
    g.setAttribute("data", d);
    g.setAttribute("style", "visibility: " + (this.oa.visible ? "visible" : "hidden"));
    Va(g, "movie", d);
    Va(g, "bgcolor", this.oa.bgColor);
    Va(g, "allowScriptAccess", "always");
    Va(g, "flashvars", Ta(this));
    this.oa.wMode != p && Va(g, "wmode", this.oa.wMode);
    if(a.hasChildNodes()) {
      for(;0 < a.childNodes.length;) {
        a.removeChild(a.firstChild)
      }
    }
    this.B = a.appendChild(g);
    this.oa.flashObject = this.B;
    this.Om = a
  }
  return this.B != p
};
function Sa(a, b) {
  return'<param name="' + a + '" value="' + b + '" />'
}
function Va(a, b, c) {
  var d = document.createElement("param");
  d.setAttribute("name", b);
  d.setAttribute("value", c);
  a.appendChild(d)
}
function Ua(a) {
  eval('obj[functionName] = function(){return eval(this.CallFunction("<invoke name=\\"' + a + '\\" returntype=\\"javascript\\">" + __flash__argumentsToXML(arguments,0) + "</invoke>"));}')
}
function Ta(a) {
  var b = "__externalobjid=" + a.oa.id;
  a.oa.preloaderSWFFile && (b += "&swffile=" + a.oa.swfFile);
  a.oa.xmlFile && (b += "&xmlfile=" + a.oa.xmlFile);
  a.oa.enabledChartEvents && (b += "&__enableevents=1");
  a.oa.enabledChartMouseEvents && (b += "&__enablechartmouseevents=1");
  b += "&preloaderInit=" + a.oa.messages.preloaderInit;
  b += "&preloaderLoading=" + a.oa.messages.preloaderLoading;
  b += "&init=" + a.oa.messages.init;
  b += "&loadingConfig=" + a.oa.messages.loadingConfig;
  b += "&loadingTemplates=" + a.oa.messages.loadingTemplates;
  b += "&loadingResources=" + a.oa.messages.loadingResources;
  b += "&noData=" + a.oa.messages.noData;
  return b += "&waitingForData=" + a.oa.messages.waitingForData
}
z.at = x(j);
z.NA = function() {
  (this.ai || this.Om) && this.Hx.aj();
  Qa(this)
};
z.Mp = function(a, b) {
  this.B && (this.B.setAttribute("width", a + ""), this.B.setAttribute("height", b + ""))
};
z.remove = function() {
  this.B && (this.B.Dispose && this.B.Dispose(), Wa(this.B, this.$m), this.ai && (this.ai || this.Om) && this.Hx.aj());
  this.B = p;
  this.oa.flashObject = this.B;
  if(B && B.TM()) {
    for(var a = B.xI(), b = 0;b < a;b++) {
      if(B.oI()[b] == this) {
        B.oI().splice(b, 1);
        break
      }
    }
  }
  B && B.pI() && this.$m != p && (B.pI()[this.$m] = p)
};
z.clear = function() {
  this.B && this.B.Clear && this.B.Clear()
};
z.refresh = function() {
  this.B != p && this.B.Refresh != p && this.B.Refresh()
};
z.qs = function(a, b) {
  this.B != p && this.B.UpdateData != p && this.B.UpdateData(a, b)
};
z.bA = function(a) {
  this.B && this.B.SetLoading && this.B.SetLoading(a)
};
z.Nx = function() {
  return this.B.GetInformation()
};
z.Hi = function(a) {
  this.B && this.vl ? this.B.SetXMLDataFromURL(a) : this.nz = j
};
z.Oh = function(a) {
  this.B && this.vl ? this.B.SetXMLDataFromString(a) : this.mz = j
};
z.Kn = s();
z.wj = function(a) {
  this.B && this.vl ? this.Oh(Oa("anychart", a)) : this.LE = j
};
z.Gv = function(a) {
  this.B && this.vl ? this.B.SetXMLDataFromString(a) : this.mz = j
};
z.jA = function(a) {
  this.B && this.vl ? this.B.SetXMLDataFromURL(a) : this.nz = j
};
z.Np = function(a, b) {
  this.B && this.vl && this.B.UpdateViewFromURL(a, b)
};
z.fA = function(a, b) {
  this.B && this.vl && this.B.UpdateViewFromString(a, b)
};
z.Pd = function(a, b) {
  this.B != p && this.B.AddPoint != p && this.B.AddPoint(a, b)
};
z.Kj = function(a, b, c) {
  this.B != p && this.B.AddPointAt != p && this.B.AddPointAt(a, b, c)
};
z.Bk = function(a, b) {
  this.B != p && this.B.RemovePoint != p && this.B.RemovePoint(a, b)
};
z.Nk = function(a, b, c) {
  this.B != p && this.B.UpdatePoint != p && this.B.UpdatePoint(a, b, c)
};
z.Ok = function(a, b, c) {
  this.B != p && this.B.UpdatePointData != p && this.B.UpdatePointData(a, b, c)
};
z.mk = function(a, b, c) {
  this.B != p && this.B.HighlightPoint != p && this.B.HighlightPoint(a, b, c)
};
z.tj = function(a, b, c) {
  this.B != p && this.B.SelectPoint != p && this.B.SelectPoint(a, b, c)
};
z.MA = function(a, b, c, d) {
  this.B && this.B.updatePointWithAnimation != p && this.B.updatePointWithAnimation(a, b, c, d)
};
z.uA = function() {
  this.B && this.B.startPointsUpdate != p && this.B.startPointsUpdate()
};
z.Lj = function(a) {
  this.B != p && this.B.AddSeries != p && this.B.AddSeries(a)
};
z.Mj = function(a, b) {
  this.B != p && this.B.AddSeriesAt != p && this.B.AddSeriesAt(a, b)
};
z.Ck = function(a) {
  this.B != p && this.B.RemoveSeries != p && this.B.RemoveSeries(a)
};
z.Pk = function(a, b) {
  this.B != p && this.B.UpdateSeries != p && this.B.UpdateSeries(a, b)
};
z.Ik = function(a, b) {
  this.B != p && this.B.ShowSeries != p && this.B.ShowSeries(a, b)
};
z.nk = function(a, b) {
  this.B != p && this.B.HighlightSeries != p && this.B.HighlightSeries(a, b)
};
z.lk = function(a, b) {
  this.B != p && this.B.HighlightCategory != p && this.B.HighlightCategory(a, b)
};
z.qA = function(a, b) {
  this.B != p && this.B.ShowAxisMarker != p && this.B.ShowAxisMarker(a, b)
};
z.Hk = function(a, b) {
  this.B != p && this.B.SetPlotCustomAttribute != p && this.B.SetPlotCustomAttribute(a, b)
};
z.Is = function(a, b, c) {
  this.B != p && this.B.View_SetPlotCustomAttribute != p && this.B.View_SetPlotCustomAttribute(a, b, c)
};
z.zs = function(a, b) {
  this.B != p && this.B.View_AddSeries != p && this.B.View_AddSeries(a, b)
};
z.Gs = function(a, b) {
  this.B != p && this.B.View_RemoveSeries != p && this.B.View_RemoveSeries(a, b)
};
z.As = function(a, b, c) {
  this.B != p && this.B.View_AddSeriesAt != p && this.B.View_AddSeriesAt(a, b, c)
};
z.Ls = function(a, b, c) {
  this.B != p && this.B.View_UpdateSeries != p && this.B.View_UpdateSeries(a, b, c)
};
z.Js = function(a, b, c) {
  this.B != p && this.B.View_ShowSeries != p && this.B.View_ShowSeries(a, b, c)
};
z.xs = function(a, b, c) {
  this.B != p && this.B.View_AddPoint != p && this.B.View_AddPoint(a, b, c)
};
z.ys = function(a, b, c, d) {
  this.B != p && this.B.View_AddPointAt != p && this.B.View_AddPointAt(a, b, c, d)
};
z.Fs = function(a, b, c) {
  this.B != p && this.B.View_RemovePoint != p && this.B.View_RemovePoint(a, b, c)
};
z.Ks = function(a, b, c, d) {
  this.B != p && this.B.View_UpdatePoint != p && this.B.View_UpdatePoint(a, b, c, d)
};
z.UA = function(a) {
  this.B != p && this.B.View_Clear != p && this.B.View_Clear(a)
};
z.Es = function(a) {
  this.B != p && this.B.View_Refresh != p && this.B.View_Refresh(a)
};
z.Ds = function(a, b, c) {
  this.B != p && this.B.View_HighlightSeries != p && this.B.View_HighlightSeries(a, b, c)
};
z.Cs = function(a, b, c, d) {
  this.B != p && this.B.View_HighlightPoint != p && this.B.View_HighlightPoint(a, b, c, d)
};
z.Bs = function(a, b, c) {
  this.B != p && this.B.View_HighlightCategory != p && this.B.View_HighlightCategory(a, b, c)
};
z.Hs = function(a, b, c, d) {
  this.B != p && this.B.View_SelectPoint != p && this.B.View_SelectPoint(a, b, c, d)
};
z.rs = function(a, b, c, d) {
  this.B != p && this.B.UpdateViewPointData != p && this.B.UpdateViewPointData(b, c, d)
};
z.Zq = function(a, b) {
  return this.B.GetPngScreen(a, b)
};
z.$x = function(a, b) {
  return this.B.GetPdfScreen(a, b)
};
z.Gz = function() {
  this.B.PrintChart()
};
z.Tz = function(a, b) {
  this.B.SaveAsImage(a, b)
};
z.Uz = function(a, b) {
  this.B.SaveAsPDF(a, b)
};
z.Vz = function(a) {
  this.B != p && this.B.ScrollXTo != p && this.B.ScrollXTo(a)
};
z.Wz = function(a) {
  this.B != p && this.B.ScrollYTo != p && this.B.ScrollYTo(a)
};
z.scrollTo = function(a, b) {
  this.B != p && this.B.ScrollTo != p && this.B.ScrollTo(a, b)
};
z.RA = function(a, b) {
  this.B != p && this.B.ViewScrollXTo != p && this.B.ViewScrollXTo(a, b)
};
z.SA = function(a, b) {
  this.B != p && this.B.ViewScrollYTo != p && this.B.ViewScrollYTo(a, b)
};
z.gy = function() {
  return this.B != p && this.B.GetXScrollInfo != p ? this.B.GetXScrollInfo() : p
};
z.hy = function() {
  return this.B != p && this.B.GetYScrollInfo != p ? this.B.GetYScrollInfo() : p
};
z.ey = function(a) {
  if(this.B != p && this.B.GetViewXScrollInfo != p) {
    return this.B.GetViewXScrollInfo(a)
  }
};
z.fy = function(a) {
  if(this.B != p && this.B.GetViewYScrollInfo != p) {
    return this.B.GetViewYScrollInfo(a)
  }
};
z.kA = function(a) {
  this.B != p && this.B.SetXZoom != p && this.B.SetXZoom(a)
};
z.lA = function(a) {
  this.B != p && this.B.SetYZoom != p && this.B.SetYZoom(a)
};
z.mA = function(a, b) {
  this.B != p && this.B.SetZoom != p && this.B.SetZoom(a, b)
};
z.gA = function(a, b) {
  this.B != p && this.B.SetViewXZoom != p && this.B.SetViewXZoom(a, b)
};
z.hA = function(a, b) {
  this.B != p && this.B.SetViewYZoom != p && this.B.SetViewYZoom(a, b)
};
z.iA = function(a, b, c) {
  this.B != p && this.B.SetViewZoom != p && this.B.SetViewZoom(a, b, c)
};
z.zw = function() {
  this.B != p && this.B.RepeatAnimation != p && this.B.RepeatAnimation()
};
function Wa(a, b) {
  a && "OBJECT" == a.nodeName && (B.platform.sr && B.platform.Zo ? (a.style.display = "none", function() {
    4 == a.readyState ? (B.platform.wk && b && this.Jt(window[b]), B.Jt(a)) : setTimeout(arguments.callee, 10)
  }()) : a.parentNode.removeChild(a))
}
z.Jt = function(a) {
  for(var b in a) {
    "function" == typeof a[b] && (a[b] = p)
  }
  a.parentNode && a.parentNode.removeChild(a)
};
function Qa(a) {
  if(a.ai && a.Om) {
    var b = a.Zq();
    if(b && 0 != b.length) {
      var c = a.Om.getAttribute("id");
      c || (c = "__chart_generated_container__" + a.$m, a.Om.setAttribute("id", c));
      var d = a.Hx, f = a.Om, g = a.ab + "", k = a.ob + "", a = a.Om.nodeName, l = document.getElementsByTagName("head");
      if(l = 0 < l.length ? l[0] : p) {
        var n;
        n = l;
        var m = document.createElement("style");
        m.setAttribute("type", "text/css");
        m.setAttribute("media", "screen");
        var o = a + "#" + c, v = o + " img";
        m.appendChild(document.createTextNode(o + (" object { width:" + g + "; height:" + k + ";padding:0; margin:0; }\n")));
        m.appendChild(document.createTextNode(v + " { display: none; }"));
        n = n.appendChild(m);
        d.Pt = n;
        n = document.createElement("style");
        n.setAttribute("type", "text/css");
        n.setAttribute("media", "print");
        a = a + "#" + c;
        c = a + " img";
        g = " { display: block; width: " + g + "; height: " + k + "; }";
        n.appendChild(document.createTextNode(a + " object { display: none; }\n"));
        n.appendChild(document.createTextNode(c + g));
        g = l.appendChild(n);
        d.Qt = g;
        g = document.createElement("img");
        g = f.appendChild(g);
        g.src = "data:image/png;base64," + b;
        d.Ot = g
      }
    }
  }
}
function Ra() {
}
z.Pt = p;
z.Qt = p;
z.Ot = p;
Ra.prototype.aj = function() {
  this.Ot && this.Ot.parentNode && this.Ot.parentNode.removeChild(this.Ot);
  this.Pt && (this.Pt.parentNode.removeChild(this.Pt), this.Pt = p);
  this.Qt && (this.Qt.parentNode.removeChild(this.Qt), this.Qt = p)
};
A.Gg("anychart.scope.goog", A);
A.Gg("anychart.scope.Event", ha);
A.Gg("anychart.scope.EventTarget", pa);
A.Gg("anychart.scope.EventType", ka);
A.Gg("anychart.scope.XhrIo", Ga);
var Xa = {};
A.Gg("anychart.WMode", Xa);
A.W(Xa, "WINDOW", "window");
A.W(Xa, "TRANSPARENT", "transparent");
A.W(Xa, "OPAQUE", "opaque");
var Ya = {};
A.Gg("anychart.RenderingType", Ya);
A.W(Ya, "FLASH_ONLY", "flash");
A.W(Ya, "SVG_ONLY", "svg");
A.W(Ya, "FLASH_PREFERRED", "flashPreferred");
A.W(Ya, "SVG_PREFERRED", "svgPreferred");
function B(a, b, c) {
  c && (this.id = c);
  this.swfFile = B.swfFile;
  this.preloaderSWFFile = B.preloaderSWFFile;
  this.width = B.width;
  this.height = B.height;
  this.enableFirefoxPrintPreviewFix = B.enableFirefoxPrintPreviewFix;
  this.enabledChartEvents = B.enabledChartEvents;
  this.enabledChartMouseEvents = B.enabledChartMouseEvents;
  this.renderingType = B.renderingType;
  this.watermark = B.watermark;
  this.messages.preloaderInit = B.messages.preloaderInit;
  this.messages.preloaderLoading = B.messages.preloaderLoading;
  this.messages.init = B.messages.init;
  this.messages.loadingConfig = B.messages.loadingConfig;
  this.messages.loadingResources = B.messages.loadingResources;
  this.messages.loadingTemplates = B.messages.loadingTemplates;
  this.messages.noData = B.messages.noData;
  this.messages.waitingForData = B.messages.waitingForData;
  a && (this.swfFile = a);
  b && (this.preloaderSWFFile = b);
  this.fe = {};
  B.LN(this)
}
A.Gg("AnyChart", B);
B.VERSION = "6.0.10 (build #36916)";
A.W(B, "VERSION", B.VERSION);
B.width = 550;
A.W(B, "width", B.width);
B.height = 400;
A.W(B, "height", B.height);
B.mF = "flash";
A.W(B, "renderingType", B.mF);
B.xC = j;
A.W(B, "enabledChartEvents", B.xC);
B.yC = q;
A.W(B, "enabledChartMouseEvents", B.yC);
B.hG = p;
A.W(B, "swfFile", B.hG);
B.gF = p;
A.W(B, "preloaderSWFFile", B.gF);
B.ai = j;
A.W(B, "enableFirefoxPrintPreviewFix", B.ai);
B.prototype.id = p;
A.W(B.prototype, "id", B.prototype.id);
B.it = [];
B.TM = function() {
  return B.it && 0 < B.xI()
};
B.xI = function() {
  return B.it.length
};
B.oI = function() {
  return B.it
};
B.tq = {};
B.pI = function() {
  return B.tq
};
B.prototype.mF = p;
A.W(B.prototype, "renderingType", B.prototype.mF);
B.prototype.va = p;
B.prototype.rk = q;
B.prototype.vO = p;
A.W(B.prototype, "xmlFile", B.prototype.vO);
B.prototype.mN = p;
A.W(B.prototype, "jsonFile", B.prototype.mN);
B.prototype.uO = p;
A.W(B.prototype, "xmlData", B.prototype.uO);
B.prototype.lN = p;
A.W(B.prototype, "jsonData", B.prototype.lN);
B.prototype.width = p;
A.W(B.prototype, "width", B.prototype.width);
B.prototype.height = p;
A.W(B.prototype, "height", B.prototype.height);
B.prototype.rO = p;
A.W(B.prototype, "watermark", B.prototype.rO);
B.prototype.hG = p;
A.W(B.prototype, "swfFile", B.prototype.hG);
B.prototype.gF = p;
A.W(B.prototype, "preloaderSWFFile", B.prototype.gF);
B.prototype.ai = j;
A.W(B.prototype, "enableFirefoxPrintPreviewFix", B.prototype.ai);
B.prototype.qO = p;
A.W(B.prototype, "wMode", B.prototype.qO);
B.prototype.xC = j;
A.W(B.prototype, "enabledChartEvents", B.prototype.xC);
B.prototype.yC = q;
A.W(B.prototype, "enabledChartMouseEvents", B.prototype.yC);
B.prototype.fe = p;
B.prototype.bgColor = "#FFFFFF";
A.W(B.prototype, "bgColor", B.prototype.bgColor);
B.prototype.Ms = j;
A.W(B.prototype, "visible", B.prototype.Ms);
B.prototype.show = function() {
  this.visible = j;
  this.va.show()
};
A.W(B.prototype, "show", B.prototype.show);
B.prototype.mu = function() {
  this.visible = q;
  this.va.mu()
};
A.W(B.prototype, "hide", B.prototype.mu);
B.R = {};
B.R.AD = function(a) {
  return"undefined" != typeof a
};
B.R.push = function(a, b) {
  a[a.length] = b
};
var Za = navigator && navigator.userAgent ? navigator.userAgent.toLowerCase() : p, $a = navigator && navigator.platform ? navigator.platform.toLowerCase() : p;
B.platform = {};
A.W(B, "platform", B.platform);
B.platform.Zo = $a ? /win/.test($a) : /win/.test(Za);
A.W(B.platform, "isWin", B.platform.Zo);
B.platform.hN = !B.platform.Zo && ($a ? /mac/.test($a) : /mac/.test(Za));
A.W(B.platform, "isMac", B.platform.hN);
B.platform.MI = "undefined" != typeof document.getElementById && "undefined" != typeof document.getElementsByTagName && "undefiend" != typeof document.createElement;
A.W(B.platform, "hasDom", B.platform.MI);
B.platform.$K = /webkit/.test(Za) ? parseFloat(Za.replace(/^.*webkit\/(\d+(\.\d+)?).*$/, "$1")) : q;
A.W(B.platform, "webKit", B.platform.$K);
B.platform.sr = /msie/.test(Za);
A.W(B.platform, "isIE", B.platform.sr);
B.platform.fJ = /firefox/.test(Za);
A.W(B.platform, "isFirefox", B.platform.fJ);
B.platform.protocol = "https:" == location.protocol ? "https:" : "http:";
A.W(B.platform, "protocol", B.platform.protocol);
B.gJ = function() {
  B.platform.ii = [0, 0, 0];
  var a;
  if(navigator.plugins && "object" == typeof navigator.plugins["Shockwave Flash"]) {
    if((a = navigator.plugins["Shockwave Flash"].description) && (!navigator.mimeTypes || !navigator.mimeTypes[["application/x-shockwave-flash"]] || navigator.mimeTypes["application/x-shockwave-flash"].enabledPlugin)) {
      a = a.replace(/^.*\s+(\S+\s+\S+$)/, "$1"), B.platform.ii[0] = parseInt(a.replace(/^(.*)\..*$/, "$1"), 10), B.platform.ii[1] = parseInt(a.replace(/^.*\.(.*)\s.*$/, "$1"), 10), B.platform.ii[2] = /[a-zA-Z]/.test(a) ? parseInt(a.replace(/^.*[a-zA-Z]+(.*)$/, "$1"), 10) : 0
    }
  }else {
    if("undefined" != typeof window.ActiveXObject) {
      try {
        var b = new window.ActiveXObject("ShockwaveFlash.ShockwaveFlash");
        if(b && (a = b.GetVariable("$version"))) {
          a = a.split(" ")[1].split(","), B.platform.ii = [parseInt(a[0], 10), parseInt(a[1], 10), parseInt(a[2], 10)]
        }
      }catch(c) {
      }
    }
  }
  return B.platform.ii != p && 9 <= Number(B.platform.ii[0])
};
A.W(B, "isFlashPlayerValidVersion", B.gJ);
B.platform.BD = B.gJ();
A.W(B.platform, "hasRequiredVersion", B.platform.BD);
B.platform.ii = B.platform.ii;
A.W(B.platform, "flashPlayerVersion", B.platform.ii);
B.platform.wk = B.platform.BD && B.platform.sr && B.platform.Zo;
A.W(B.platform, "needFormFix", B.platform.wk);
B.platform.wk && (B.platform.wk = 9 == Number(B.platform.ii[0]), B.platform.wk = B.platform.wk && 0 == Number(B.platform.ii[1]), B.platform.wk = B.platform.wk && 115 > Number(B.platform.ii[2]));
B.BC = [];
B.IN = function(a) {
  A.Dy(a) && B.BC.push(a)
};
A.W(B, "ready", B.IN);
B.oN = function(a) {
  if(a == p || a == h) {
    a = B.qM()
  }
  var a = a + "AnyChartHTML5.js", b = new Ga;
  b.vG = j;
  A.G.nb(b, "complete", B.dK, p, B);
  A.G.nb(b, ["error", "abort"], B.cK, p, B);
  b.send(a, "GET", p, "anychart.com")
};
A.W(B, "loadHTML5Engine", B.oN);
B.dK = function(a) {
  var a = a.target, b;
  try {
    b = a.Bc ? a.Bc.responseText : ""
  }catch(c) {
    b = ""
  }
  eval(b.toString());
  b = B.BC.length;
  for(var d = 0;d < b;d++) {
    B.BC[d]()
  }
  B.TL(a)
};
B.cK = function() {
  e(Error("Can not find AnyChart html5 engine in JS folder."))
};
B.TL = function(a) {
  A.G.ed(a, "complete", B.dK, p, B);
  A.G.ed(a, ["error", "abort"], B.cK, p, B)
};
B.qM = function() {
  for(var a = A.global.document.getElementsByTagName("script"), b = a.length - 1;b >= 0;--b) {
    var c = a[b].src, d = c.lastIndexOf("?"), d = d == -1 ? c.length : d, c = c.substr(0, d), f = c.split("/"), f = f[f.length - 1].toLowerCase(), g = f.length;
    if(f.indexOf("anychart") != -1 && f.substr(g - 3, 3) == ".js") {
      return c.substr(0, d - g)
    }
  }
  return p
};
B.R.attachEvent = function(a, b, c) {
  a.attachEvent(b, c);
  B.R.push(B.R.DR, [a, b, c])
};
B.R.tL = function(a, b) {
  if(B.R.AD(window.addEventListener)) {
    window.addEventListener(a, b, q)
  }else {
    if(B.R.AD(document.addEventListener)) {
      document.addEventListener(a, b, q)
    }else {
      if(B.R.AD(window.attachEvent)) {
        B.R.attachEvent(window, "on" + a, b)
      }else {
        if(typeof window["on" + a] == "function") {
          var c = window["on" + a];
          window["on" + a] = function() {
            c();
            b()
          }
        }else {
          window["on" + a] = b
        }
      }
    }
  }
};
A.Gg("AnyChart.utils.addGlobalEventListener", B.R.tL);
B.R.qr = q;
B.R.nC = [];
B.R.sL = function(a) {
  B.R.qr ? a() : B.R.nC.push(a)
};
B.R.Iq = function() {
  if(!B.R.qr) {
    try {
      var a = document.getElementsByTagName("body")[0].appendChild(document.createElement("span"));
      a.parentNode.removeChild(a)
    }catch(b) {
      return
    }
    B.R.qr = j;
    for(var a = B.R.nC.length, c = 0;c < a;c++) {
      B.R.nC[c]()
    }
  }
};
B.R.KN = function() {
  if(B.platform.MI) {
    typeof document.readyState != "undefined" && (document.readyState == "complete" || document.getElementsByTagName("body")[0] || document.body) && B.R.Iq();
    if(!B.R.qr) {
      document.addEventListener && document.addEventListener("DOMContentLoaded", B.R.Iq, q);
      if(B.platform.sr && B.platform.Zo) {
        document.attachEvent("onreadystatechange", function() {
          if(document.readyState == "complete") {
            document.detachEvent("onreadystatechange", arguments.callee);
            B.R.Iq()
          }
        });
        window == top && function() {
          if(!B.R.qr) {
            try {
              document.documentElement.doScroll("left")
            }catch(a) {
              setTimeout(arguments.callee, 0);
              return
            }
            B.R.Iq()
          }
        }()
      }
      B.platform.$K && function() {
        B.R.qr || (/loaded|complete/.test(document.readyState) ? B.R.Iq() : setTimeout(arguments.callee, 0))
      }();
      A.G.nb(window, "load", B.R.Iq)
    }
  }
};
B.R.KN();
B.dispatchEvent = function(a, b) {
  B.bH(a) && b != p && B.tq[a].dispatchEvent(b)
};
A.W(B, "dispatchEvent", B.dispatchEvent);
function ab(a) {
  switch(a.renderingType) {
    case "svg":
      return new window.anychart.render.svg.SVGRenderer;
    case "flash":
      return new Pa;
    default:
    ;
    case "flashPreferred":
      return B.platform.hasRequiredVersion ? new Pa : new window.anychart.render.svg.SVGRenderer;
    case "svgPreferred":
      return window.SVGAngle != h ? new window.anychart.render.svg.SVGRenderer : new Pa
  }
}
B.LN = function(a) {
  a.id = a.id == h ? "__AnyChart___" + B.it.length : a.id;
  B.tq[a.id] = a;
  B.it.push(a)
};
B.bH = function(a) {
  return a && B.tq && B.tq[a]
};
B.tM = function(a) {
  return B.bH(a) ? B.tq[a] : p
};
A.W(B, "getChartById", B.tM);
B.aM = function(a, b) {
  B.Fq(a, b)
};
A.W(B, "disposeFlashObject", B.aM);
B.Fq = function(a, b) {
  a.Fq(a, b)
};
A.W(B, "disposeVisual", B.Fq);
B.prototype.Fq = function(a, b) {
  this.va.Fq(a, b)
};
B.R.lf = {};
A.Gg("AnyChart.utils.JSConverter", B.R.lf);
B.R.lf.ZD = function(a) {
  a = typeof a;
  return a == "string" || a == "number" || a == "boolean"
};
A.W(B.R.lf, "isAttribute", B.R.lf.ZD);
B.R.lf.isArray = function(a) {
  return typeof a != "string" && typeof a.length != "undefined"
};
A.W(B.R.lf, "isArray", B.R.lf.isArray);
B.R.lf.GL = function(a) {
  return B.R.lf.XB("anychart", a)
};
A.W(B.R.lf, "convert", B.R.lf.GL);
B.R.lf.XB = function(a, b) {
  var c, d, f = "<" + a;
  if(typeof b.functionName != "undefined") {
    b["function"] = b.functionName;
    delete b.functionName
  }
  for(c in b) {
    c != "format" && c != "text" && c != "custom_attribute_value" && c != "attr" && B.R.lf.ZD(b[c]) && (f = f + (" " + c + '="' + b[c] + '"'))
  }
  f = f + ">";
  for(c in b) {
    if(c == "arg" && B.R.lf.isArray(b[c])) {
      var g = b[c];
      for(d = 0;d < g.length;d++) {
        f = f + ("<arg><![CDATA[" + g[d] + "]]\></arg>")
      }
    }else {
      if(c == "custom_attribute_value" || c == "attr") {
        f = f + ("<![CDATA[" + b[c] + "]]\>")
      }else {
        if(c == "format" || c == "text") {
          f = f + ("<" + c + "><![CDATA[" + b[c] + "]]\></" + c + ">")
        }else {
          if(B.R.lf.isArray(b[c])) {
            g = b[c];
            for(d = 0;d < g.length;d++) {
              f = f + B.R.lf.XB(c, g[d])
            }
          }else {
            B.R.lf.ZD(b[c]) || (f = f + B.R.lf.XB(c, b[c]))
          }
        }
      }
    }
  }
  return f + ("</" + a + ">")
};
B.messages = {};
B.messages.preloaderInit = " ";
B.messages.preloaderLoading = "Loading... ";
B.messages.init = "Initializing...";
B.messages.loadingConfig = "Loading config...";
B.messages.loadingResources = "Loading resources...";
B.messages.loadingTemplates = "Loading templates...";
B.messages.noData = "No Data";
B.messages.waitingForData = "Waiting for data...";
B.watermark = p;
B.prototype.tN = {};
A.W(B.prototype, "messages", B.prototype.tN);
B.prototype.messages.preloaderInit = p;
B.prototype.messages.preloaderLoading = p;
B.prototype.messages.init = p;
B.prototype.messages.loadingConfig = p;
B.prototype.messages.loadingResources = p;
B.prototype.messages.loadingTemplates = p;
B.prototype.messages.noData = p;
B.prototype.messages.waitingForData = p;
B.prototype.addEventListener = function(a, b) {
  this.fe[a] == p && (this.fe[a] = []);
  this.fe[a].push(b)
};
A.W(B.prototype, "addEventListener", B.prototype.addEventListener);
B.prototype.removeEventListener = function(a, b) {
  if(!(this.fe == p || this.fe[a] == p)) {
    for(var c = -1, d = 0;d < this.fe[a].length;d++) {
      if(this.fe[a][d] == b) {
        c = d;
        break
      }
    }
    c != -1 && this.fe[a].splice(c, 1)
  }
};
A.W(B.prototype, "removeEventListener", B.prototype.removeEventListener);
B.prototype.dispatchEvent = function(a) {
  if(!(a == p || a.type == p)) {
    a.type == "create" ? this.va.$J() : a.type == "draw" && this.va.aK();
    a.target = this;
    if(!(this.fe == p || this.fe[a.type] == p)) {
      for(var b = this.fe[a.type].length, c = 0;c < b;c++) {
        this.fe[a.type][c](a)
      }
    }
  }
};
A.W(B.prototype, "dispatchEvent", B.prototype.dispatchEvent);
B.prototype.lo = function() {
  return this.va.lo()
};
A.W(B.prototype, "canWrite", B.prototype.lo);
B.prototype.write = function(a) {
  this.va = ab(this);
  this.va.Ha(this);
  if(this.lo()) {
    if(a == h) {
      a = "__chart_generated_container__" + this.id;
      document.write('<div id="' + a + '"></div>')
    }
    var b = this;
    B.R.sL(function() {
      typeof a == "string" ? db(b, document.getElementById(a)) : db(b, a)
    })
  }
};
A.W(B.prototype, "write", B.prototype.write);
function db(a, b) {
  a.rk = a.va.write(b);
  a.rk && a.at() && (a.xmlFile != p ? a.Hi(a.xmlFile) : a.jsonFile != p ? a.Kn(a.jsonFile) : a.xmlData != p ? a.Gv(a.xmlData) : a.jsonData != p && a.wj(a.jsonData))
}
B.prototype.at = function() {
  return this.va.at()
};
A.W(B.prototype, "canSetConfigAfterWrite", B.prototype.at);
B.prototype.NA = function() {
  this.va.NA()
};
A.W(B.prototype, "updatePrintForFirefox", B.prototype.NA);
B.prototype.Mp = function(a, b) {
  this.width = a;
  this.height = b;
  this.va.Mp(a, b)
};
A.W(B.prototype, "setSize", B.prototype.Mp);
A.Gg("AnyChart.prototype.resize", B.prototype.Mp);
B.prototype.remove = function() {
  this.va.remove()
};
A.W(B.prototype, "remove", B.prototype.remove);
B.prototype.clear = function() {
  this.va.clear()
};
A.W(B.prototype, "clear", B.prototype.clear);
B.prototype.refresh = function() {
  this.va.refresh()
};
A.W(B.prototype, "refresh", B.prototype.refresh);
B.prototype.qs = function(a, b) {
  this.va.qs(a, b)
};
A.W(B.prototype, "updateData", B.prototype.qs);
B.prototype.bA = function(a) {
  this.va.bA(a)
};
A.W(B.prototype, "setLoading", B.prototype.bA);
B.prototype.Nx = function() {
  return this.va.Nx()
};
A.W(B.prototype, "getInformation", B.prototype.Nx);
B.prototype.Hi = function(a) {
  this.xmlFile = a;
  this.rk && this.va.Hi(a)
};
A.W(B.prototype, "setXMLFile", B.prototype.Hi);
B.prototype.Oh = function(a) {
  this.xmlData = "" + a;
  this.rk && this.va.Oh(this.xmlData)
};
A.W(B.prototype, "setXMLData", B.prototype.Oh);
B.prototype.Gv = function(a) {
  this.rk && this.va.Gv(a)
};
A.W(B.prototype, "setXMLDataFromString", B.prototype.Gv);
B.prototype.jA = function(a) {
  this.rk && this.va.jA(a)
};
A.W(B.prototype, "setXMLDataFromURL", B.prototype.jA);
B.prototype.Np = function(a, b) {
  this.va.Np(a, b)
};
A.W(B.prototype, "setViewXMLFile", B.prototype.Np);
B.prototype.fA = function(a, b) {
  this.va.fA(a, b)
};
A.W(B.prototype, "setViewData", B.prototype.fA);
B.prototype.Kn = function(a) {
  this.jsonFile = a;
  this.rk && this.va.Kn(a)
};
A.W(B.prototype, "setJSONFile", B.prototype.Kn);
B.prototype.wj = function(a) {
  this.jsonData = a;
  this.rk && this.va.wj(this.jsonData)
};
A.W(B.prototype, "setJSONData", B.prototype.wj);
A.Gg("AnyChart.prototype.setJSData", B.prototype.wj);
B.prototype.setData = function(a) {
  if(a != p) {
    for(var b = "" + a;b.charAt(0) == " " && b.length > 0;) {
      b = b.substr(1)
    }
    if(b.charAt(0) == "<") {
      this.xmlData = "" + a;
      this.rk && this.va.Oh(this.xmlData)
    }else {
      this.jsonData = a;
      this.rk && this.va.wj(this.jsonData)
    }
  }
};
A.W(B.prototype, "setData", B.prototype.setData);
B.prototype.Pd = function(a, b) {
  this.va.Pd(a, b)
};
A.W(B.prototype, "addPoint", B.prototype.Pd);
B.prototype.Kj = function(a, b, c) {
  this.va.Kj(a, b, c)
};
A.W(B.prototype, "addPointAt", B.prototype.Kj);
B.prototype.Bk = function(a, b) {
  this.va.Bk(a, b)
};
A.W(B.prototype, "removePoint", B.prototype.Bk);
B.prototype.Nk = function(a, b, c) {
  this.va.Nk(a, b, c)
};
A.W(B.prototype, "updatePoint", B.prototype.Nk);
B.prototype.Ok = function(a, b, c) {
  this.va.Ok(a, b, c)
};
A.W(B.prototype, "updatePointData", B.prototype.Ok);
B.prototype.mk = function(a, b, c) {
  this.va.mk(a, b, c)
};
A.W(B.prototype, "highlightPoint", B.prototype.mk);
B.prototype.tj = function(a, b, c) {
  this.va.tj(a, b, c)
};
A.W(B.prototype, "selectPoint", B.prototype.tj);
B.prototype.MA = function(a, b, c, d) {
  this.va.MA(a, b, c, d)
};
A.W(B.prototype, "updatePointWithAnimation", B.prototype.MA);
B.prototype.uA = function() {
  this.va.uA()
};
A.W(B.prototype, "startPointsAnimation", B.prototype.uA);
B.prototype.Lj = function(a) {
  this.va.Lj(a)
};
A.W(B.prototype, "addSeries", B.prototype.Lj);
B.prototype.Mj = function(a, b) {
  this.va.Mj(a, b)
};
A.W(B.prototype, "addSeriesAt", B.prototype.Mj);
B.prototype.Ck = function(a) {
  this.va.Ck(a)
};
A.W(B.prototype, "removeSeries", B.prototype.Ck);
B.prototype.Pk = function(a, b) {
  this.va.Pk(a, b)
};
A.W(B.prototype, "updateSeries", B.prototype.Pk);
B.prototype.Ik = function(a, b) {
  this.va.Ik(a, b)
};
A.W(B.prototype, "showSeries", B.prototype.Ik);
B.prototype.nk = function(a, b) {
  this.va.nk(a, b)
};
A.W(B.prototype, "highlightSeries", B.prototype.nk);
B.prototype.lk = function(a, b) {
  this.va.lk(a, b)
};
A.W(B.prototype, "highlightCategory", B.prototype.lk);
B.prototype.qA = function(a, b) {
  this.va.qA(a, b)
};
A.W(B.prototype, "showAxisMarker", B.prototype.qA);
B.prototype.Hk = function(a, b) {
  this.va.Hk(a, b)
};
A.W(B.prototype, "setPlotCustomAttribute", B.prototype.Hk);
B.prototype.Is = function(a, b, c) {
  this.va.Is(a, b, c)
};
A.W(B.prototype, "view_setPlotCustomAttribute", B.prototype.Is);
B.prototype.zs = function(a, b) {
  this.va.zs(a, b)
};
A.W(B.prototype, "view_addSeries", B.prototype.zs);
B.prototype.Gs = function(a, b) {
  this.va.Gs(a, b)
};
A.W(B.prototype, "view_removeSeries", B.prototype.Gs);
B.prototype.As = function(a, b, c) {
  this.va.As(a, b, c)
};
A.W(B.prototype, "view_addSeriesAt", B.prototype.As);
B.prototype.Ls = function(a, b, c) {
  this.va.Ls(a, b, c)
};
A.W(B.prototype, "view_updateSeries", B.prototype.Ls);
B.prototype.Js = function(a, b, c) {
  this.va.Js(a, b, c)
};
A.W(B.prototype, "view_showSeries", B.prototype.Js);
B.prototype.xs = function(a, b, c) {
  this.va.xs(a, b, c)
};
A.W(B.prototype, "view_addPoint", B.prototype.xs);
B.prototype.ys = function(a, b, c, d) {
  this.va.ys(a, b, c, d)
};
A.W(B.prototype, "view_addPointAt", B.prototype.ys);
B.prototype.Fs = function(a, b, c) {
  this.va.Fs(a, b, c)
};
A.W(B.prototype, "view_removePoint", B.prototype.Fs);
B.prototype.Ks = function(a, b, c, d) {
  this.va.Ks(a, b, c, d)
};
A.W(B.prototype, "view_updatePoint", B.prototype.Ks);
B.prototype.UA = function(a) {
  this.va.UA(a)
};
A.W(B.prototype, "view_clear", B.prototype.UA);
B.prototype.Es = function(a) {
  this.va.Es(a)
};
A.W(B.prototype, "view_refresh", B.prototype.Es);
B.prototype.Ds = function(a, b, c) {
  this.va.Ds(a, b, c)
};
A.W(B.prototype, "view_highlightSeries", B.prototype.Ds);
B.prototype.Cs = function(a, b, c, d) {
  this.va.Cs(a, b, c, d)
};
A.W(B.prototype, "view_highlightPoint", B.prototype.Cs);
B.prototype.Bs = function(a, b, c) {
  this.va.Bs(a, b, c)
};
A.W(B.prototype, "view_highlightCategory", B.prototype.Bs);
B.prototype.Hs = function(a, b, c, d) {
  this.va.Hs(a, b, c, d)
};
A.W(B.prototype, "view_selectPoint", B.prototype.Hs);
B.prototype.rs = function(a, b, c, d) {
  this.va.rs(a, b, c, d)
};
A.W(B.prototype, "updateViewPointData", B.prototype.rs);
B.prototype.Zq = function(a, b) {
  if(a == p || a == h) {
    a = NaN
  }
  if(b == p || b == h) {
    b = NaN
  }
  return this.va.Zq(a, b)
};
A.W(B.prototype, "getPNG", B.prototype.Zq);
A.Gg("AnyChart.prototype.getPNGImage", B.prototype.Zq);
B.prototype.$x = function(a, b) {
  if(a == p || a == h) {
    a = NaN
  }
  if(b == p || b == h) {
    b = NaN
  }
  return this.va.$x(a, b)
};
A.W(B.prototype, "getPDF", B.prototype.$x);
B.prototype.Gz = function() {
  this.va.Gz()
};
A.W(B.prototype, "printChart", B.prototype.Gz);
B.prototype.Tz = function(a, b) {
  if(a == p || a == h) {
    a = NaN
  }
  if(b == p || b == h) {
    b = NaN
  }
  this.va.Tz(a, b)
};
A.W(B.prototype, "saveAsImage", B.prototype.Tz);
B.prototype.Uz = function(a, b) {
  if(a == p || a == h) {
    a = NaN
  }
  if(b == p || b == h) {
    b = NaN
  }
  this.va.Uz(a, b)
};
A.W(B.prototype, "saveAsPDF", B.prototype.Uz);
B.prototype.Vz = function(a) {
  this.va.Vz(a)
};
A.W(B.prototype, "scrollXTo", B.prototype.Vz);
B.prototype.Wz = function(a) {
  this.va.Wz(a)
};
A.W(B.prototype, "scrollYTo", B.prototype.Wz);
B.prototype.scrollTo = function(a, b) {
  this.va.scrollTo(a, b)
};
A.W(B.prototype, "scrollTo", B.prototype.scrollTo);
B.prototype.RA = function(a, b) {
  this.va.RA(a, b)
};
A.W(B.prototype, "viewScrollXTo", B.prototype.RA);
B.prototype.SA = function(a, b) {
  this.va.SA(a, b)
};
A.W(B.prototype, "viewScrollYTo", B.prototype.SA);
B.prototype.gy = function() {
  return this.va.gy()
};
A.W(B.prototype, "getXScrollInfo", B.prototype.gy);
B.prototype.hy = function() {
  return this.va.hy()
};
A.W(B.prototype, "getYScrollInfo", B.prototype.hy);
B.prototype.ey = function(a) {
  return this.va.ey(a)
};
A.W(B.prototype, "getViewXScrollInfo", B.prototype.ey);
B.prototype.fy = function(a) {
  return this.va.fy(a)
};
A.W(B.prototype, "getViewYScrollInfo", B.prototype.fy);
B.prototype.kA = function(a) {
  this.va.kA(a)
};
A.W(B.prototype, "setXZoom", B.prototype.kA);
B.prototype.lA = function(a) {
  this.va.lA(a)
};
A.W(B.prototype, "setYZoom", B.prototype.lA);
B.prototype.mA = function(a, b) {
  this.va.mA(a, b)
};
A.W(B.prototype, "setZoom", B.prototype.mA);
B.prototype.gA = function(a, b) {
  this.va.gA(a, b)
};
A.W(B.prototype, "setViewXZoom", B.prototype.gA);
B.prototype.hA = function(a, b) {
  this.va.hA(a, b)
};
A.W(B.prototype, "setViewYZoom", B.prototype.hA);
B.prototype.iA = function(a, b, c) {
  this.va.iA(a, b, c)
};
A.W(B.prototype, "setViewZoom", B.prototype.iA);
B.prototype.zw = function() {
  this.va.zw()
};
A.W(B.prototype, "animate", B.prototype.zw);
B.prototype.Nc = function(a) {
  return this.va.Nc(a)
};
A.W(B.prototype, "getSVG", B.prototype.Nc);
})()
