define( function( require, exports, module ) {
	'use strict';
	require( 'extjs' );
	require( 'anychart' );
	require( 'anychartHTML5' );
	var app = require( 'app' );
	Ext.define('Birst.core.Renderer', {
		requires: ['Ext.dd.DragTracker', 'Ext.util.Region'],
		xmlDoc: null,
		panel: null,
		tracker: null,
		dashletCtrl: null,
		maxX: null,
		maxY: null,
		panelId: null,

		constructor: function(panel, dashletCtrl){
			this.maxX = 0;
			this.maxY = 0;
			this.panel = panel;
			this.panelId = panel.id;
			this.dashletCtrl = dashletCtrl;
		},

		renderChartInFrame: function(frameXml, xOrigin, yOrigin, surface) {
			var frameX = frameXml.getAttribute('x');
			var frameY = frameXml.getAttribute('y');
			if (frameX) {
				frameX = parseInt(frameX, 10);
			} else {
				frameX = 0;
			}

			if (frameY) {
				frameY = parseInt(frameY, 10);
			} else {
				frameY = 0;
			}

			var child = frameXml.firstChild;

			while(child){
				if(child.tagName === 'chart' || child.tagName === 'gauge'){
					this.renderChart(child, frameX + xOrigin, frameY + yOrigin, surface);
				}
				else if (child.tagName === 'frame') {
					this.renderChartInFrame(child, frameX + xOrigin, frameY + yOrigin, surface);
				}
				child = child.nextSibling;
			}
		},

		renderChart: function(chartXml, xOrigin, yOrigin, surface, scale){
			var sprite;
			var anychartXml = chartXml.children[0];
			if(anychartXml && anychartXml.tagName === 'anychart'){
				var chartWidth = chartXml.getAttribute('width');
				var chartHeight = chartXml.getAttribute('height');
				var x = parseInt(chartXml.getAttribute('x'), 10) + xOrigin;
				var y = parseInt(chartXml.getAttribute('y'), 10) + yOrigin;
				if (scale) {
					var r = surface.el.getRegion();
					if (r.right != r.left && r.top != r.bottom) {
					var w = surface.el.dom.offsetWidth - surface.el.dom.clientWidth;
					var h = surface.el.dom.offsetHeight - surface.el.dom.clientHeight;
					chartWidth = r.right - r.left - w - x - 15;
					chartHeight = r.bottom - r.top - h - y - 15;
					surface.setAutoScroll(false);
					}
				}

				//Fixup for DASH-368. Client side html5 only for now. Could be server side if Flex is ok
				//with the additional parameter
				var series = chartXml.getElementsByTagName('series');
				for(var i = 0; series && i < series.length; i++){
					var se = series[i];
					se.setAttribute('missing_points', 'no_paint');
				}

				var chartType = 'gauge';
				if (chartXml.nodeName !== 'gauge') {
					var chartTypeXml = chartXml.getElementsByTagName('chart');
					chartType = chartTypeXml[0].getAttribute('plot_type');
				}
				if (chartType === 'Scatter') {
					// do more to determine if its a bubble chart
					var seriesList = chartXml.getElementsByTagName('series');
					if (seriesList && seriesList.length > 0) {
						var series = seriesList[0];
						var isBubble = false;
						while(series && !isBubble) {
							isBubble = (series.getAttribute('type') === 'Bubble');
							series = series.nextSibling;
						}

						if (isBubble) {
							chartType = 'Bubble';
						}
					}
				}
				if (chartType === 'Radar' || chartType === 'Map') {
					sprite = this.createChartContainer(surface, x, y, chartWidth, chartHeight);
					var label = Ext.create('Ext.form.Label', {
						text: Ext.String.format(Birst.LM.Locale.get('CHART_NOTSUPPORTED'),chartType),
						cls: 'chartError',
						y: '50%',
						anchor: '100%'
					});
					sprite.add(label);
				}
				else if(chartType === 'UMap'){
					sprite = this.createChartContainer(surface, x, y, chartWidth, chartHeight);
					var bingMaps = Ext.create('Birst.dashboards.BingMaps', sprite.body.dom.id, chartTypeXml[0], this);
					bingMaps.render();
				}else{
					var chartData = app.xml2Str(anychartXml);
					AnyChart.renderingType = anychart.RenderingType.SVG_ONLY;
					AnyChart.enabledChartMouseEvents = true;

					var chart = new AnyChart();
					chart.width =  chartWidth;
					chart.height = chartHeight;

					this.maxX = Math.max(this.maxX, chart.width + x);
					this.maxY = Math.max(this.maxY, chart.height + y);
					chart.setData(chartData);

					chart.addEventListener('pointClick', this.handleChartClick);
					//chart.addEventListener('pointSelect', this.handleChartClick);
					var obj = this;
					if (this.isVisualFilter()) {
						chart.addEventListener('multiplePointsSelect', function(e) {
												obj.handleSelectionChange(e, chart);
						});
					}
					sprite = this.createChartContainer(surface, x, y, chartWidth, chartHeight);
					chart.write(sprite.body.dom.id);

					//We add the chart's location for each renderer so we can check if we are clicking
					//on a chart object and disable our dragtracker visual filtering which interferes with AnyChart
					//visual filtering.
					//TODO: Is this needed? Birst.core.AnyChartTracker.add(chart.id, this,  { x: x, y: y, width: chartWidth, height: chartHeight} );

				}

			}
		},

		createChartContainer: function(surface, x, y, chartWidth, chartHeight){

			var sprite = Ext.create('Ext.panel.Panel', {
						x: x,
						y: y,
						width: parseInt(chartWidth, 10),
						height: parseInt(chartHeight, 10),
						layout: 'absolute',
						border: false
			});
			surface.add(sprite);
			return sprite;
		},

		handleMultipleChartSelect: function(event) {

			// multiple items selected
			var e = event;
		},

		handleChartClick: function(event) {

			if (event && event.data && event.data.Attributes && event.data.Attributes.DrillType) {
				if (event.data.Attributes.DrillType === 'Drill To Dashboard') {
					var ctrlr = Dashboard.getApplication().getController('DashboardTabLists');
					if (ctrlr) {
						var dashParts = event.data.Attributes.targetURL.split(',');
						if (dashParts.length > 1) {
							ctrlr.navigateToDash(dashParts[0], dashParts[1], event.data.Attributes.filters);
						}
					}

				}
				else if (event.data.Attributes.DrillType === 'Drill Down') {
					var me = Birst.core.AnyChartTracker.getRenderer(event.target.id);
					Birst.core.Webservice.dashboardRequest('drillDown',
					   [{key: 'xmlReport', value: app.xml2Str(me.dashletCtrl.adhocReportXml)},
						{key: 'drillCols', value: event.data.Attributes.drillBy},
						{key: 'filters', value: event.data.Attributes.filters}],
					   event,
					   function(response, options) {
							var me = Birst.core.AnyChartTracker.getRenderer(this.target.id);
							var xml = response.responseXML;
							var xmlReportsArray = xml.getElementsByTagName('com.successmetricsinc.adhoc.AdhocReport');
							me.dashletCtrl.adhocReportXml = xmlReportsArray[0];
							var newPrompts = xml.getElementsByTagName('Prompts');
							if (newPrompts.length > 0) {
								me.dashletCtrl.promptsController.addPrompts(newPrompts[0]);
							}
							else {
								if (me.dashletCtrl.promptsController) {
									me.dashletCtrl.renderByReportXml(this.dashletCtrl.promptsController.getPromptsData());
								}
								else {
									me.dashletCtrl.renderByReportXml('<Prompts/>');
								}
							}
					  });
				}

			}
		},

		requestFilterPrompt: function(newPrompts){
			if (newPrompts && newPrompts.getCount() > 0) {
				var keys = newPrompts.getKeys();
				var name = '';
				for (i = 0; i < keys.length; i++) {
					if (i > 0) {
						name = name + ',';
					}
					name = name + keys[i];
				}

				Birst.core.Webservice.adhocRequest('getFilterPrompts',
											[ {key: 'column', value: name}
											],
											this,
											function(response, options) {
												var newPrompts = response.responseXML.getElementsByTagName('prompts');
												var numExistingPrompts = 0;
												var numNewPrompts = 0;

												if (newPrompts.length > 0) {
													var values = options.additionalParam;
													var prompt = newPrompts[0].firstChild;
													while (prompt) {
														var column = getXmlChildValue(prompt, 'ParameterName');
														var promptControl = this.dashletCtrl.promptsController.promptControls.get(column);
														var value = values.get(column);

														var promptToDelete = null;
														if (value) {
															if(promptControl){
																promptControl.setSelectedValuesByArray(value);
																numExistingPrompts++;
																promptToDelete = prompt;
															}else{
																var selectedValues = prompt.getElementsByTagName('selectedValues');
																if (! selectedValues || selectedValues.length === 0) {
																	selectedValues = prompt.ownerDocument.createElement('selectedValues');
																	prompt.appendChild(selectedValues);
																}
																else {
																	selectedValues = selectedValues[0];
																}

																var child = selectedValues.firstChild;
																while (child) {
																	selectedValues.removeChild(child);
																	child = selectedValues.firstChild;
																}

																var test = {};

																for (var i = 0; i < value.length; i++) {
																	if (test[value[i]])
																		continue;

																	test[value[i]] = 1;
																	var data = selectedValues.ownerDocument.createElement('selectedValue');
																	data.appendChild(selectedValues.ownerDocument.createTextNode(value[i]));
																	selectedValues.appendChild(data);
																}
																numNewPrompts++;
															}
														}

														prompt = prompt.nextSibling;
														if(promptToDelete){
															newPrompts[0].removeChild(promptToDelete);
														}
													}

													if(numNewPrompts > 0){
														this.dashletCtrl.promptsController.addPrompts(newPrompts[0]);
													}else{
														if(numExistingPrompts > 0){
															this.dashletCtrl.promptsController.applyPrompts();
														}
													}
												}
											},
											null,
											newPrompts);


			}
		},

		handleSelectionChange: function(e, chart){
			if (!e || !e.data || !e.data.points || !e.data.points.length) {
				return;
			}

			var pts = e.data.points;

			if (!pts.length) {
				return;
			}

			var newPrompts = new Ext.util.HashMap();

			for(var i = 0; i < pts.length; i++){
				var point = pts[i];
				if(point.hasOwnProperty('Attributes')){
					var attr = point.Attributes;
					for(var y = 0; y < 10; y++){
						if(!attr.hasOwnProperty('column' + y)){
							break;
						}

						var dim = attr['dimension' + y];
						if(dim == 'EXPR'){
							continue;
						}

						var col = attr["column" + y];
						var name = dim + "." + col;
						var value = attr["value" + y];

						var valArray = newPrompts.get(name);
						if (! valArray) {
							valArray = [];
							newPrompts.add(name, valArray);
						}
						valArray.push(value);
					}
				}
			}

			this.requestFilterPrompt(newPrompts);
		},
		/*
		 * 	private function handleMultipleChartSelect(event:AnyChartMultiplePointsEvent):void {
				Tracer.debug("multiple chart select " + event);
				var pts:Array = event.points;
				var columns:Array = [];
				for each (var pt:AnyChartPointInfo in pts) {
					for (var i:uint = 0; i < MAX_CATEGORIES; i++) {
						if (pt.pointCustomAttributes.hasOwnProperty("column" + i) == false)
							break;

						var dim:String = pt.pointCustomAttributes["dimension" + i];

						//We don't do filtering on expressions.
						if(dim == 'EXPR'){
							continue;
						}

						var col:String = pt.pointCustomAttributes["column" + i];
						var name:String = dim + "." + col;
						var columnData:Object = columns[name];
						if (columnData == null) {
							columnData = [];
							columnData["columnType"] = "dimension";
							columnData["dimension"] = dim;
							columnData["column"] = col;
							columnData["datetimeFormat"] = pt.pointCustomAttributes["datetimeFormat" + i];
							columnData["values"] = [];
							columns[name] = columnData;
						}
						columnData["values"].push(pt.pointCustomAttributes["value" + i]);
					}
				}
				sendSelectionEvent(columns);
			}

			private function sendSelectionEvent(columns:Array):void {
				var data:XML = XML("<Filters/>");
				for each (var c:Array in columns) {
					var filter:XML = XML("<Filter/>");
					if (c["dimension"] == "EXPR")
						continue;

					var cType:XML = XML("<ColumnType>" + c["columnType"] + "</ColumnType>");
					var dim:XML   = XML("<Dimension>" + c["dimension"] + "</Dimension>");
					var col:XML   = XML("<Column>" + c["column"] + "</Column>");

					filter.appendChild(cType);
					filter.appendChild(dim);
					filter.appendChild(col);

					//If it is a datetime object, it will have a format
					var fmt:String = c["datetimeFormat"];
					if (fmt){
						filter.appendChild(new XML("<datetimeFormat>" + fmt + "</datetimeFormat>"));
					}

					var val:XML = XML("<selectedValues/>");
					for (var j:int = 0; j < c["values"].length; j++) {
						var v:XML = XML("<selectedValue>" + c["values"][j] + "</selectedValue>");
						val.appendChild(v);
					}
					filter.appendChild(val);
					data.appendChild(filter);
				}

				if (parent){
					parent.dispatchEvent(new ReportRendererEvent(ReportRendererEvent.RECTANGLE_SELECT, true, false, data));
				}
			}

		 */
		renderText: function(textXml, surface, xOrigin, yOrigin) {

			var xPos, yPos;
			var textFld = Ext.create('Birst.core.text', textXml, xOrigin, yOrigin);

			this.maxX = Math.max(this.maxX, textFld.width + textFld.x);
			this.maxY = Math.max(this.maxY, textFld.height + textFld.y);
			var item = {
				type: 'rect',
				width: textFld.width,
				height: textFld.height,
				x: textFld.x,
				y: textFld.y,
				text: textFld.text
			};
			if (textXml.getAttribute('transparent') !== 'true') {
				item.fill = textFld.backColor;

			}
			surface.add(item);
			var bottomBorderColor = textXml.getAttribute('bottomBorderColor');
			var bottomBorderWidth = textXml.getAttribute('bottomBorderWidth');
			var bottomBorderStyle = textXml.getAttribute('bottomBorderStyle');
			if (bottomBorderWidth) {
				bottomBorderWidth = parseInt(bottomBorderWidth, 10);
				if (bottomBorderWidth > 0) {
					yPos = textFld.y + parseInt(textFld.height, 10);
					surface.add( {
						type: 'path',
						path: "M" + textFld.x + " " + yPos + "l" + parseInt(textFld.width, 10) + " 0",
						"stroke-width": bottomBorderWidth,
						stroke: bottomBorderColor
						}).show(true);
				}
			}
			bottomBorderColor = textXml.getAttribute('leftBorderColor');
			bottomBorderWidth = textXml.getAttribute('leftBorderWidth');
			bottomBorderStyle = textXml.getAttribute('leftBorderStyle');
			if (bottomBorderWidth) {
				bottomBorderWidth = parseInt(bottomBorderWidth, 10);
				if (bottomBorderWidth > 0) {
					yPos = textFld.y;
					surface.add( {
						type: 'path',
						path: "M" +textFld. x + " " + textFld.y + "l 0 " + parseInt(textFld.height, 10),
						"stroke-width": bottomBorderWidth,
						stroke: bottomBorderColor
						}).show(true);
				}
			}
			bottomBorderColor = textXml.getAttribute('rightBorderColor');
			bottomBorderWidth = textXml.getAttribute('rightBorderWidth');
			bottomBorderStyle = textXml.getAttribute('rightBorderStyle');
			if (bottomBorderWidth) {
				bottomBorderWidth = parseInt(bottomBorderWidth, 10);
				if (bottomBorderWidth > 0) {
					xPos = parseInt(textFld.x, 10) + parseInt(textFld.width, 10);
					surface.add( {
						type: 'path',
						path: "M" + xPos + " " + textFld.y + "l 0 " + parseInt(textFld.height, 10),
						"stroke-width": bottomBorderWidth,
						stroke: bottomBorderColor
						}).show(true);
				}
			}
			bottomBorderColor = textXml.getAttribute('topBorderColor');
			bottomBorderWidth = textXml.getAttribute('topBorderWidth');
			bottomBorderStyle = textXml.getAttribute('topBorderStyle');
			if (bottomBorderWidth) {
				bottomBorderWidth = parseInt(bottomBorderWidth, 10);
				if (bottomBorderWidth > 0) {
					yPos = textFld.y + parseInt(textFld.height, 10);
					surface.add( {
						type: 'path',
						path: "M" + textFld.x + " " + textFld.y + "l " + parseInt(textFld.width, 10) + " 0",
						"stroke-width": bottomBorderWidth,
						stroke: bottomBorderColor
						}).show(true);
				}
			}
			textFld.y = parseInt(textFld.y, 10) + (parseInt(textFld.height, 10)/2);
			var sprite = surface.add( {
				type: 'text',
				text: textFld.text,
				fill: textFld.textColor,
				width: textFld.width,
				height: textFld.height,
				x: textFld.textX,
				y: textFld.y,
				"font-family": textFld.fontName,
				"font-size": textFld.fontSize,
				"font-style": 'normal',
				"font-weight": textFld.fontWeight,
				"text-anchor": textFld.anchor
				}
				).show(true);
			var link = textXml.getAttribute('link');
			if (link) {
				sprite.on("click", this.onClick, this, textXml);
				sprite.addCls('clickable');
				}

		},

		renderTextAsHTML: function(textXml, panel, xOrigin, yOrigin) {

			var width = parseInt(textXml.getAttribute('width'), 10);
			var height = parseInt(textXml.getAttribute('height'), 10);
			var x = parseInt(textXml.getAttribute('x'), 10) + xOrigin;
			var y = parseInt(textXml.getAttribute('y'), 10) + yOrigin;
			var item = {
				html: Birst.core.Webservice.unescapeXml(textXml.getAttribute('value')),
				width: width,
				height: height,
				x: x,
				y: y
			};
			var htmlFld = Ext.create('Ext.Component', item);
			panel.add(htmlFld);
			return htmlFld;
		},

		renderTextAsDiv: function(textXml, panel, xOrigin, yOrigin) {

			var xPos, yPos;
			var textFld = Ext.create('Birst.core.text', textXml, xOrigin, yOrigin);

			this.maxX = Math.max(this.maxX, textFld.width + textFld.x);
			this.maxY = Math.max(this.maxY, textFld.height + textFld.y);
			var item = {
						width: textFld.width,
						height: textFld.height,
						x: textFld.x,
						y: textFld.y,
						text: textFld.text
			};
			var style = {
				"font-family": textFld.fontName,
				"font-size": textFld.fontSize + 'px',
				"font-style": 'normal',
				"font-weight": textFld.fontWeight,
				"text-align": textFld.alignment,
				"color": textFld.textColor
				};

			if (textXml.getAttribute('transparent') !== 'true') {
				style["background-color"] = textFld.backColor;
			}
			else {
				style["background-color"] = "transparent";
			}
			var bottomBorderColor = textXml.getAttribute('bottomBorderColor');
			var bottomBorderWidth = textXml.getAttribute('bottomBorderWidth');
			var bottomBorderStyle = textXml.getAttribute('bottomBorderStyle');
			style["border-bottom"] = "0px none";
			if (bottomBorderWidth) {
				bottomBorderWidth = parseInt(bottomBorderWidth, 10);
				if (bottomBorderWidth > 0) {
					style["border-bottom"] = bottomBorderWidth + 'px solid ' + bottomBorderColor;
				}
			}
			bottomBorderColor = textXml.getAttribute('leftBorderColor');
			bottomBorderWidth = textXml.getAttribute('leftBorderWidth');
			bottomBorderStyle = textXml.getAttribute('leftBorderStyle');
			style["border-left"] = "0px none";
			if (bottomBorderWidth) {
				bottomBorderWidth = parseInt(bottomBorderWidth, 10);
				if (bottomBorderWidth > 0) {
					style["border-left"] = bottomBorderWidth + 'px solid ' + bottomBorderColor;
				}
			}
			bottomBorderColor = textXml.getAttribute('rightBorderColor');
			bottomBorderWidth = textXml.getAttribute('rightBorderWidth');
			bottomBorderStyle = textXml.getAttribute('rightBorderStyle');
			style["border-right"] = "0px none";
			if (bottomBorderWidth) {
				bottomBorderWidth = parseInt(bottomBorderWidth, 10);
				if (bottomBorderWidth > 0) {
					style["border-right"] = bottomBorderWidth + 'px solid ' + bottomBorderColor;
				}
			}
			bottomBorderColor = textXml.getAttribute('topBorderColor');
			bottomBorderWidth = textXml.getAttribute('topBorderWidth');
			bottomBorderStyle = textXml.getAttribute('topBorderStyle');
			style["border-top"] = "0px none";
			if (bottomBorderWidth) {
				bottomBorderWidth = parseInt(bottomBorderWidth, 10);
				if (bottomBorderWidth > 0) {
					style["border-top"] = bottomBorderWidth + 'px solid ' + bottomBorderColor;
				}
			}

			if (textFld.isUnderline) {
				style["text-decoration"] = 'underline';
			}

			item.style = style;
			var link = textXml.getAttribute('link');
			var label = Ext.create('Ext.form.Label', item);
			panel.add(label);
			if (link) {
				label.on('afterrender', function(me, eOpts) {
					me.getEl().on('click', this.onClick, this, eOpts);
				}, this, textXml);
				label.addCls('clickable');
			}
			return label;
		},

		renderButton: function(textXml, panel, xOrigin, yOrigin) {

			var textFld = Ext.create('Birst.core.text', textXml, xOrigin, yOrigin);

			this.maxX = Math.max(this.maxX, textFld.width + textFld.x);
			this.maxY = Math.max(this.maxY, textFld.height + textFld.y);

			var btn = Ext.create('Ext.button.Button', {
				text: textFld.text,
				width: textFld.width,
				height: textFld.height,
				x: textFld.x,
				y: textFld.y,
				style: {
				"font-family": textFld.fontName,
				"font-size": textFld.fontSize,
				"font-style": 'normal',
				"font-weight": textFld.fontWeight,
				"text-anchor": textFld.anchor
				}
			});
			btn.on("click", this.onClick, this, textXml);
			btn.on("afterrender", function(me, eOpts) {
				var id = me.getId();
				var spanId = id + '-btnInnerEl';
				var el = me.getEl();
				if (el) {
					var span = el.getById(spanId);
					if (span) {
						span.setStyle('font-family', eOpts.fontName);
						span.setStyle('font-size', eOpts.fontSize);
						span.setStyle('font-weight', eOpts.fontWeight);
						span.setStyle('color', eOpts.textColor);
					}
				}
			}, this, textFld);
			panel.add(btn);
		},

		renderImage:  function(textXml, panel, xOrigin, yOrigin) {

			var x = textXml.getAttribute('x');
			var y = textXml.getAttribute('y');
			x = parseInt(x, 10) + xOrigin;
			y = parseInt(y, 10) + yOrigin;
			var width  = textXml.getAttribute('width');
			var height = textXml.getAttribute('height');
			width = parseInt(width, 10);
			height = parseInt(height, 10);
			var ibackcolor = textXml.getAttribute('backcolor');
			var link = textXml.getAttribute('link');

			var properties = {
				x: x,
				y: y,
				width: width,
				height: height,
				src: '/SMIWeb/' + getXmlValue(textXml),
				style: {
					'background-color': ibackcolor
				}
			};

			if (link && link !== '') {
				properties.cls = 'clickable';
			}
			var img = Ext.create('Ext.Img', properties);

			if (link && link !== '') {
				img.on("afterrender", function(me, eOpts) {
					var el = me.getEl();
					if (el) {
						el.on('click', this.onClick, this, textXml);
					}

				}, this, textXml);
			}

			panel.add(img);
		},

		onClick: function(e, t, eOpts) {

			var link = new String(eOpts.getAttribute('link'));
			if (link.indexOf('javascript:AdhocContent.DrillThru(') === 0) {
				// drill or navigate
				var parms = link.substring(link.indexOf('('), link.length);
				var paramArray = parms.split(',');
				for (var i = 0; i < paramArray.length; i++) {
					paramArray[i] = Ext.String.trim(paramArray[i]);
				}
				if (paramArray[1] === "'Drill Down'") {
					// drill down
				// send this.xmlDoc, paramArray[2], paramArray[3]
					Birst.core.Webservice.dashboardRequest('drillDown',
						[
							{key: 'xmlReport', value: app.xml2Str(this.dashletCtrl.adhocReportXml)},
							{key: 'drillCols', value: paramArray[3].slice(1, -1)},
							{key: 'filters', value: paramArray[2].slice(1, -1)}
						],
						this,
						function(response, options) {
							var xml = response.responseXML;
							var xmlReportsArray = xml.getElementsByTagName('com.successmetricsinc.adhoc.AdhocReport');
							this.dashletCtrl.adhocReportXml = xmlReportsArray[0];
							var newPrompts = xml.getElementsByTagName('Prompts');
							if (newPrompts.length > 0) {
								this.dashletCtrl.promptsController.addPrompts(newPrompts[0]);
							}
							else {
								if (this.dashletCtrl.promptsController) {
									this.dashletCtrl.renderByReportXml(this.dashletCtrl.promptsController.getPromptsData());
								}
								else {
									this.dashletCtrl.renderByReportXml('<Prompts/>');
								}
							}
					});
				}
				else if (paramArray[2] === "'Drill To Dashboard'") {
					var ctrlr = Dashboard.getApplication().getController('DashboardTabLists');
					if (ctrlr) {
						ctrlr.navigateToDash(paramArray[0].slice(2, -1), paramArray[1].slice(1, -1), paramArray[3].slice(1, -1));
					}
				}

			}
			else if (link) {
				var dest = eOpts.getAttribute('target');
				if (!dest) {
					dest = '_blank';
				}
				window.open(link, dest);
			}
		},

		clearPanel: function(){
			console.log( this.panel );
			var cell = this.panel;
			if ( cell.hasChildNodes() ) {
				while ( cell.childNodes.length >= 1 ) {
					cell.removeChild( cell.firstChild );
				}
			}

			this.panel.removeAll();
		},

		renderInMaximizedPanel: function(maxPanel){

			var refPanel = this.panel;
			this.panel = maxPanel;
			this.render(this.xmlDoc, true);
			this.panel = refPanel;
		},


		parse: function(doc){

			//this.clearPanel();
			this.xmlDoc = doc;
			this.render(this.xmlDoc);
		},

		calcMaxDims: function(item, xOffset, yOffset) {

			if (item.tagName === 'frame') {
				var frameX = item.getAttribute('x');
				var frameY = item.getAttribute('y');
				if (frameX) {
					frameX = parseInt(frameX, 10);
				} else {
					frameX = 0;
				}

				if (frameY) {
					frameY = parseInt(frameY, 10);
				} else {
					frameY = 0;
				}

				var child = item.firstChild;

				while(child){
					this.calcMaxDims(child, frameX + xOffset, frameY + yOffset);
					child = child.nextSibling;
				}
			}
			else {
				var x = item.getAttribute('x');
				var y = item.getAttribute('y');
				x = parseInt(x, 10) + xOffset;
				y = parseInt(y, 10) + yOffset;
				var width  = parseInt(item.getAttribute('width'), 10);
				var height = parseInt(item.getAttribute('height'), 10);
				if (isFinite(width) && isFinite(x)) {
					this.width = Math.max(this.width, width + x);
				}
				if (isFinite(height) && isFinite(y)) {
					this.height = Math.max(this.height, height + y);
				}
			}
		},

		render: function(doc, dontResize){

			var parent = this.panel;
			this.panel = Ext.create('Ext.panel.Panel', {
				autoScroll: true,
				layout: 'absolute',
				width: parent.getWidth(),
				height: parent.getHeight()
			});

			if (this.isVisualFilter()) {
				this.panel.on('afterrender', this.createDragSelectTracker, this);
			}

			try {
			this.width = 0; //doc.getAttribute('width');
			this.height = 0; //doc.getAttribute('height');
			if (dontResize) {
				this.width = doc.getAttribute('width');
				this.height = doc.getAttribute('height');
			}
			var numItems = 0;
			var numCharts = 0;

			var child = doc.firstChild;
			while(child){
				this.calcMaxDims(child, 0, 0);
				if (child.tagName ==='chart' || child.tagName ==='gauge') {
					numItems += 1;
					numCharts += 1;
				}
				else if (child.tagName ==='frame' ||
						child.tagName === 'text' ||
						child.tagName === 'html' ||
						child.tagName === 'adhocButton' ||
						child.tagName === 'image') {
					numItems += 1;
				}
				child = child.nextSibling;
			}

			var scaleChart = false;
			if (numItems <= 3 && numCharts === 1) {
				scaleChart = true;
			}


			child = doc.firstChild;
			while(child){
				if(child.tagName === 'text'){
					this.renderTextAsDiv(child, this.panel, 0, 0);
				}
				else if (child.tagName === 'html') {
					this.renderTextAsHTML(child, this.panel, 0, 0);
				}
				else if (child.tagName === 'frame') {
					var grandChild = child.firstChild;
					var xOrigin = child.getAttribute('x');
					var yOrigin = child.getAttribute('y');
					if (xOrigin) {
						xOrigin = parseInt(xOrigin, 10);
					} else {
						xOrigin = 0;
					}

					if (yOrigin) {
						yOrigin = parseInt(yOrigin, 10);
					} else {
						yOrigin = 0;
					}
					var label = null;
					var cellType = null;
					var childCounts = 0;
					while (grandChild) {
						if(grandChild.tagName === 'text'){
							label = this.renderTextAsDiv(grandChild, this.panel, xOrigin, yOrigin);
							childCounts++;
						}
						else if (grandChild.tagName === 'html') {
							label = this.renderTextAsHTML(grandChild, this.panel, xOrigin, yOrigin);
							childCounts++;
						}
						else if (grandChild.tagName === 'adhocButton') {
							this.renderButton(grandChild, this.panel, xOrigin, yOrigin);
						}
						else if (grandChild.tagName === 'net.sf.jasperreports.crosstab.cell.type') {
							cellType = getXmlValue(grandChild);
						}
						else if (grandChild.tagName === 'image') {
							this.renderImage(grandChild, this.panel, xOrigin, yOrigin);
						}
						grandChild = grandChild.nextSibling;
					}
					if (label && (cellType === 'Data' || cellType === 'RowHeader' || cellType === 'CrosstabHeader' || cellType === 'ColumnHeader')) {
						var textWidth = parseInt(child.getAttribute('width'), 10);
						var textHeight = Math.max(parseInt(child.getAttribute('height'), 10), label.height);
						if (childCounts > 1) {
							textWidth = label.width + (textWidth - ((label.x - xOrigin) + label.width));
						}

						if (cellType === 'CrosstabHeader') {
							textHeight = label.height;
						}
						else if (childCounts > 1) {
							textHeight = label.height + parseInt(child.getAttribute('height'), 10) - ((label.y - yOrigin) + label.height);
						}

						label.setWidth(Math.min(label.width, textWidth));
						label.setHeight(textHeight);
					}
				}
				else if (child.tagName === 'adhocButton') {
					this.renderButton(child, this.panel, 0, 0);
				}
				else if (child.tagName === 'image') {
					this.renderImage(child, this.panel, 0, 0);
				}
				child = child.nextSibling;
			}

			}
			catch (err) {

			}

			parent.add(this.panel);
			this.panel.show();
			child = doc.firstChild;
			while(child){
				if(child.tagName === 'chart' || child.tagName === 'gauge'){
					this.renderChart(child, 0, 0, this.panel, scaleChart);
				}
				else if (child.tagName === 'frame') {
					this.renderChartInFrame(child, 0, 0, this.panel);
				}
				child = child.nextSibling;
			}
			this.panel = parent;
			if (! dontResize) {
				this.panel.setSize(this.maxX, this.maxY);
			}
		},

		cancelClick: function() {
			return !this.tracker.dragging;
		},

		createDragSelectTracker: function() {
			var thisRenderer = this;
			this.tracker = Ext.create('Ext.dd.DragTracker', {
				autoStart: false,
	//			constrainTo: this.panel,
				delegate: this.panel.body,
				preventDefault: true,
				trackerPanel: null,
				renderer: this,
				panel: this.panel,

				listeners: {
					beforedragstart: function(that, e, eOpts) {
						var p = e.getXY();

						//This drag tracker interferes with chart visual filtering
						//if we point at any chart area, we do not run this drag tracker
						if(Birst.core.AnyChartTracker.pointIsInCharts(p[0], p[1], thisRenderer)){
							return false;
						}

						//console.log('onBeforeStart: currentTarget is ' + e.currentTarget.id + ' panel is ' + this.panel.body.id);
						if (that.dragTarget && that.dragTarget.id === that.panel.body.id)
						{

							var r = that.el.getRegion();
							var w = that.dragTarget.offsetWidth - that.dragTarget.clientWidth;
							var h = that.dragTarget.offsetHeight - that.dragTarget.clientHeight;
							if (p[0] < (r.right-w) && p[1] < (r.bottom-h)) {
								return true;
							}
						}
						return false;
					}
				},

				onStart: function(e) {
						// Flag which controls whether the cancelClick method vetoes the processing of the DataView's containerclick event.
						// On IE (where else), this needs to remain set for a millisecond after mouseup because even though the mouse has
						// moved, the mouseup will still trigger a click event.
						this.dragging = true;

						this.trackerPanel = Ext.create('Ext.window.Window', {
							header: false,
							style: {
								 background: '#ffcc',
								 opacity: 0.6
								},
							//bodyCls: 'visual-selector',
							x: this.startXY[0],
							y: this.startXY[1],
							width: 1,
							height: 1,
							border: false,
							layout: 'absolute',
							resizable: false,
							minWidth: 1,
							minHeight: 1
							}).show();
				},

				onDrag: function(e) {
					var minX = Math.min(e.xy[0], this.startXY[0]);
					var minY = Math.min(e.xy[1], this.startXY[1]);
					var maxX = Math.max(e.xy[0], this.startXY[0]);
					var maxY = Math.max(e.xy[1], this.startXY[1]);
					Ext.apply(this.dragRegion, {
						top: minY,
						left: minX,
						right: maxX,
						bottom: maxY
					});

					this.trackerPanel.setSize(maxX - minX, maxY - minY);
					this.trackerPanel.setPosition(minX, minY);

	//				this.dragRegion.constrainTo(bodyRegion);
				},

				onEnd: Ext.Function.createDelayed(function(e) {
					this.dragging = false;
					this.trackerPanel.close();
					this.renderer.handleDragSelect(this.dragRegion, this.dragTarget.scrollTop, this.dragTarget.scrollLeft);
				}, 1)
			});
			this.tracker.initEl(this.panel.body);
		},

		handleDragSelect: function(region, scrollTopOffset, scrollLeftOffset) {

			var i;
			// offset region by this.panel.body
			var offsetX = this.panel.body.getLeft(false) - scrollLeftOffset;
			var offsetY = this.panel.body.getTop(false) - scrollTopOffset;
			var minX = region.left - offsetX;
			var minY = region.top - offsetY;
			var maxX = region.right - offsetX;
			var maxY = region.bottom - offsetY;

			var elementArray = this.findIntersectingTextFields(minX, minY, maxX, maxY, this.xmlDoc);

			// find columns and getFilterPrompt
			var newPrompts = new Ext.util.HashMap();
			for (i = 0; i < elementArray.length; i++) {
				if (getXmlChildValue(elementArray[i], 'columnType') === 'Dimension') {
					var column = getXmlChildValue(elementArray[i], 'dimension') + '.' + getXmlChildValue(elementArray[i], 'column');
					var value = getXmlChildValue(elementArray[i], 'valueToUse');
					if (!value) {
						value = elementArray[i].getAttribute('value');
					}
					var valArray = newPrompts.get(column);
					if (! valArray) {
						valArray = [];
						newPrompts.add(column, valArray);
					}
					valArray.push(value);
				}
			}

			this.requestFilterPrompt(newPrompts);
		},

		findIntersectingTextFields: function(minX, minY, maxX, maxY, parentDoc) {

			var elementArray = [];

			var child = parentDoc.firstChild;
			while(child){
				if(child.tagName === 'text'){
					var x = parseInt(child.getAttribute('x'), 10);
					var y = parseInt(child.getAttribute('y'), 10);
					var width  = parseInt(child.getAttribute('width'), 10);
					var height = parseInt(child.getAttribute('height'), 10);
					if (x <= maxX && y <= maxY && x + width > minX && y + height > minY) {
						elementArray.push(child);
					}
				}
				else if (child.tagName === 'frame') {
					var grandChild = child.firstChild;
					var xOrigin = child.getAttribute('x');
					var yOrigin = child.getAttribute('y');
					if (xOrigin) {
						xOrigin = parseInt(xOrigin, 10);
					} else {
						xOrigin = 0;
					}

					if (yOrigin) {
						yOrigin = parseInt(yOrigin, 10);
					} else {
						yOrigin = 0;
					}
					var childElements = this.findIntersectingTextFields(minX - xOrigin, minY - yOrigin, maxX - xOrigin, maxY - yOrigin, child);
					elementArray = elementArray.concat(childElements);
				}
				child = child.nextSibling;
			}

			return elementArray;
		},
		isVisualFilter: function(){
			var rectangleSelect = this.dashletCtrl.model.xmlDoc.getElementsByTagName('RectangleSelect');
			if(rectangleSelect && rectangleSelect.length > 0){
				return getXmlValue(rectangleSelect[0]) === 'false' ? false : true;
			}
			return true;
	   }
	});
} );