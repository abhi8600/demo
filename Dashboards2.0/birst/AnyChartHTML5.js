(function(){function e(a) {
  throw a;
}
var h = void 0, j = !0, p = null, q = !1;
function aa() {
  return function(a) {
    return a
  }
}
function s() {
  return function() {
  }
}
function t(a) {
  return function(b) {
    this[a] = b
  }
}
function u(a) {
  return function() {
    return this[a]
  }
}
function x(a) {
  return function() {
    return a
  }
}
var eb = window.anychart.scope, A = A || eb.goog, ha = ha || eb.Event, pa = pa || eb.EventTarget, ka = ka || eb.EventType, Ga = Ga || eb.XhrIo;
function fb() {
  this.arguments = []
}
fb.prototype.arguments = p;
fb.prototype.pq = function() {
  return this.arguments[0].Pe != h && this.arguments[1].Pe != h
};
fb.prototype.execute = function(a) {
  var b = this.arguments[0];
  b.execute != h ? b = b.execute(a) : "%color" == b && (b = new gb(a.Ai, a.ni, a.Vh, a.bq, q));
  var c = this.arguments[1];
  c.execute != h ? c = c.execute(a) : "%color" == c && (c = new gb(a.Ai, a.ni, a.Vh, a.bq, q));
  if(b.Pe == h || c.Pe == h) {
    return a
  }
  var a = this.arguments[2], d = 1 - a;
  return new gb(Math.floor((b.Ai * a + c.Ai * d) % 256), Math.floor((b.ni * a + c.ni * d) % 256), Math.floor((b.Vh * a + c.Vh * d) % 256))
};
fb.prototype.uq = function() {
  if(3 != this.arguments.length) {
    return"Blend function expects 3 arguments, " + this.arguments.length + " passed"
  }
  if(this.arguments[0].execute == h && this.arguments[0].Pe == h && "%color" != this.arguments[0]) {
    return"Blend function requires a color, a function returning a color or a %color variable as it's first argument"
  }
  if(this.arguments[1].execute == h && this.arguments[1].Pe == h && "%color" != this.arguments[1]) {
    return"Blend function requires a color, a function returning a color or a %color variable as it's second argument"
  }
  if(this.arguments[2].execute != h || this.arguments[2].Pe != h || "%color" == this.arguments[2]) {
    return"Blend function requires a number as it's third argument"
  }
  1 < this.arguments[2] && (this.arguments[2] = 1);
  0 > this.arguments[2] && (this.arguments[2] = 0);
  return p
};
function hb() {
  this.arguments = []
}
hb.prototype.arguments = p;
hb.prototype.pq = function() {
  return this.arguments[0].Pe != h
};
hb.prototype.execute = function(a) {
  var b = this.arguments[0];
  b.execute != h ? b = b.execute(a) : "%color" == b && (b = new gb(a.Ai, a.ni, a.Vh));
  if(b.Pe == h) {
    return a
  }
  a = ib(b);
  b = 100 * a.Eh;
  a.Eh -= 0.007 * b * b / 100;
  0 > a.Eh && (a.Eh = 0);
  return jb(a.vD, a.Eh, a.rF)
};
hb.prototype.uq = function() {
  return 1 != this.arguments.length ? "DarkColor function expects 1 argument, " + this.arguments.length + " passed" : this.arguments[0].execute == h && this.arguments[0].Pe == h && "%color" != this.arguments[0] ? "DarkColor function requires a color, a function returning a color or a %color variable as it's argument" : p
};
function kb() {
  this.arguments = []
}
kb.prototype.arguments = p;
kb.prototype.pq = x(j);
kb.prototype.execute = function() {
  return lb(this.arguments[0], this.arguments[1], this.arguments[2])
};
kb.prototype.uq = function() {
  if(3 != this.arguments.length) {
    return"HSB function expects 3 arguments, " + this.arguments.length + " passed"
  }
  if(this.arguments[0].execute != h || this.arguments[0].Pe != h || "%color" == this.arguments[0]) {
    return"HSB function requires a number as it's first argument"
  }
  if(this.arguments[1].execute != h || this.arguments[1].Pe != h || "%color" == this.arguments[1]) {
    return"HSB function requires a number as it's second argument"
  }
  if(this.arguments[2].execute != h || this.arguments[2].Pe != h || "%color" == this.arguments[2]) {
    return"HSB function requires a number as it's third argument"
  }
  this.arguments[0] = Math.round(this.arguments[0]);
  360 < this.arguments[0] ? this.arguments[0] = 360 : 0 > this.arguments[0] && (this.arguments[0] = 0);
  for(var a = 1;3 > a;a++) {
    this.arguments[a] = Math.round(this.arguments[a]), 100 < this.arguments[a] ? this.arguments[a] = 100 : 0 > this.arguments[a] && (this.arguments[a] = 0)
  }
  return p
};
function mb() {
  this.arguments = []
}
mb.prototype.arguments = p;
mb.prototype.pq = function() {
  return this.arguments[0].Pe != h
};
mb.prototype.execute = function(a) {
  var b = this.arguments[0];
  b.execute != h ? b = b.execute(a) : "%color" == b && (b = new gb(a.Ai, a.ni, a.Vh));
  if(b.Pe == h) {
    return a
  }
  a = ib(b);
  b = 100 * a.Eh - 100;
  a.Eh += 0.007 * b * b / 100;
  1 < a.Eh && (a.Eh = 1);
  return jb(a.vD, a.Eh, a.rF)
};
mb.prototype.uq = function() {
  return 1 != this.arguments.length ? "LightColor function expects 1 argument, " + this.arguments.length + " passed" : this.arguments[0].execute == h && this.arguments[0].Pe == h && "%color" != this.arguments[0] ? "LightColor function requires a color, a function returning a color or a %color variable as it's argument" : p
};
function nb() {
  this.arguments = []
}
nb.prototype.arguments = p;
nb.prototype.pq = x(j);
nb.prototype.execute = function() {
  return new gb(this.arguments[0], this.arguments[1], this.arguments[2])
};
nb.prototype.uq = function() {
  if(3 != this.arguments.length) {
    return"RGB function expects 3 arguments, " + this.arguments.length + " passed"
  }
  if(this.arguments[0].execute != h || this.arguments[0].Pe != h || "%color" == this.arguments[0]) {
    return"RGB function requires a number as it's first argument"
  }
  if(this.arguments[1].execute != h || this.arguments[1].Pe != h || "%color" == this.arguments[1]) {
    return"RGB function requires a number as it's second argument"
  }
  if(this.arguments[2].execute != h || this.arguments[2].Pe != h || "%color" == this.arguments[2]) {
    return"RGB function requires a number as it's third argument"
  }
  for(var a = 0;3 > a;a++) {
    this.arguments[a] = Math.round(this.arguments[a]), 255 < this.arguments[a] ? this.arguments[a] = 255 : 0 > this.arguments[a] && (this.arguments[a] = 0)
  }
  return p
};
function ob(a) {
  switch(a) {
    case tb.blend:
      return new fb;
    case tb.lightcolor:
      return new mb;
    case tb.darkcolor:
      return new hb;
    case tb.rgb:
      return new nb;
    case tb.hsb:
      return new kb;
    default:
      e(Error("Unknown color operation identifier passed"))
  }
}
;function gb(a, b, c, d, f) {
  a != h && (this.Ai = a);
  b != h && (this.ni = b);
  c != h && (this.Vh = c);
  d != h && (this.bq = d);
  if(d != h && (f == h || f)) {
    this.bq /= 255
  }
}
function lb(a, b, c) {
  b = Number(b / 100);
  c = Number(c / 100);
  if(0 == b) {
    return new gb(Math.floor(255 * c), Math.floor(255 * c), Math.floor(255 * c))
  }
  var d = 6 * Number(a / 360), a = Math.floor(d), f = d - a, d = c * (1 - b), b = a & 1 ? c * (1 - b * f) : c * (1 - b * (1 - f));
  switch(a) {
    case 0:
      return new gb(Math.floor(255 * c), Math.floor(255 * b), Math.floor(255 * d));
    case 1:
      return new gb(Math.floor(255 * b), Math.floor(255 * c), Math.floor(255 * d));
    case 2:
      return new gb(Math.floor(255 * d), Math.floor(255 * c), Math.floor(255 * b));
    case 3:
      return new gb(Math.floor(255 * d), Math.floor(255 * b), Math.floor(255 * c));
    case 4:
      return new gb(Math.floor(255 * b), Math.floor(255 * d), Math.floor(255 * c));
    default:
      return new gb(Math.floor(255 * c), Math.floor(255 * d), Math.floor(255 * b))
  }
}
function jb(a, b, c) {
  var d = 0, f = 0, g = 0;
  if(0 == b) {
    d = f = g = 0
  }else {
    if(0 == c) {
      d = f = g = b
    }else {
      d = 0.5 >= b ? b * (1 + c) : b + c - b * c;
      b = 2 * b - d;
      f = [a + 0.3333333333333333, a, a - 0.3333333333333333];
      a = [];
      for(c = 0;3 > c;c++) {
        0 > f[c] ? f[c] += 1 : 1 < f[c] && (f[c] -= 1), a[c] = 1 > 6 * f[c] ? b + 6 * (d - b) * f[c] : 1 > 2 * f[c] ? d : 2 > 3 * f[c] ? b + 6 * (d - b) * (0.6666666666666666 - f[c]) : b
      }
      d = Math.max(0, Math.min(1, a[0]));
      f = Math.max(0, Math.min(1, a[1]));
      g = Math.max(0, Math.min(1, a[2]))
    }
  }
  return new gb(Math.floor(255 * d), Math.floor(255 * f), Math.floor(255 * g))
}
z = gb.prototype;
z.Ai = 0;
z.ni = 0;
z.Vh = 0;
z.bq = 1;
z.Pe = function() {
  return"rgba(" + this.Ai.toString() + ", " + this.ni.toString() + ", " + this.Vh.toString() + ", " + Math.round(255 * this.alpha).toString() + ")"
};
function ub(a) {
  return"rgb(" + a.Ai.toString() + ", " + a.ni.toString() + ", " + a.Vh.toString() + ")"
}
function vb(a) {
  return"0x" + (a.Ai << 24 | a.ni << 16 | a.Vh << 8 | a.bq).toString(16)
}
z.toString = function() {
  return"rgb(" + this.Ai.toString() + "," + this.ni.toString() + "," + this.Vh.toString() + ")"
};
function ib(a) {
  var b = a.Ai / 255, c = a.ni / 255, a = a.Vh / 255, d = Math.min(b, Math.min(c, a)), f = Math.max(b, Math.max(c, a));
  if(d == f) {
    return new wb(0, (f + d) / 2, 0)
  }
  var g = f - d, d = f + d, k = 0, k = 0.1666 * (b == f ? 6 + (c - a) / g : c == f ? 2 + (a - b) / g : 4 + (b - c) / g);
  1 <= k && (k -= 1);
  return new wb(k, d / 2, 1 >= d ? g / d : g / (2 - d))
}
function wb(a, b, c) {
  this.vD = a;
  this.Eh = b;
  this.rF = c
}
wb.prototype.vD = 0;
wb.prototype.Eh = 0;
wb.prototype.rF = 0;
function xb(a, b) {
  this.Xa = a;
  a.execute != h ? (this.ad = b, this.zy = j) : "%color" == a ? this.zy = j : (this.Xa.alpha = b, this.zy = q)
}
xb.prototype.Xa = p;
xb.prototype.ad = 1;
xb.prototype.zy = q;
xb.prototype.Ba = function(a) {
  return this.zy ? (a || (a = new gb), this.Xa.execute != h && (a = this.Xa.execute(a), a.alpha = this.ad), a) : this.Xa
};
function zb(a, b) {
  if(!a) {
    return new xb(new gb(0, 0, 0), 1)
  }
  var c = new Ab(a);
  try {
    return new xb(c.parse(), b == h ? 1 : b)
  }catch(d) {
    return new xb(new gb(0, 0, 0), 1)
  }
}
function Bb(a) {
  this.input = Cb(a);
  this.TD = a.length
}
Bb.prototype.input = "";
Bb.prototype.TD = 0;
Bb.prototype.Zj = 0;
var Db = {steelblue:4620980, royalblue:267920, cornflowerblue:6591981, lightsteelblue:11584734, mediumslateblue:8087790, slateblue:6970061, darkslateblue:4734347, midnightblue:1644912, navy:128, darkblue:139, mediumblue:205, blue:255, dodgerblue:2003199, deepskyblue:49151, lightskyblue:8900346, skyblue:8900331, lightblue:11393254, powderblue:11591910, azure:15794175, lightcyan:14745599, paleturquoise:11529966, mediumturquoise:4772300, lightseagreen:2142890, darkcyan:35723, teal:32896, cadetblue:6266528, 
darkturquoise:52945, aqua:65535, cyan:65535, turquoise:4251856, aquamarine:8388564, mediumaquamarine:6737322, darkseagreen:9419919, mediumseagreen:3978097, seagreen:3050327, darkgreen:25600, green:32768, forestgreen:2263842, limegreen:3329330, lime:65280, chartreuse:8388352, lawngreen:8190976, greenyellow:11403055, yellowgreen:10145074, palegreen:10025880, lightgreen:9498256, springgreen:65407, mediumspringgreen:64154, darkolivegreen:5597999, olivedrab:7048739, olive:8421376, darkkhaki:12433259, 
darkgoldenrod:12092939, goldenrod:14329120, gold:16766720, yellow:16776960, khaki:15787660, palegoldenrod:15657130, blanchedalmond:16772045, moccasin:16770229, wheat:16113331, navajowhite:16768685, burlywood:14596231, tan:13808780, rosybrown:12357519, sienna:10506797, saddlebrown:9127187, chocolate:13789470, peru:13468991, sandybrown:16032864, darkred:9109504, maroon:8388608, brown:10824234, firebrick:11674146, indianred:13458524, lightcoral:15761536, salmon:16416882, darksalmon:15308410, lightsalmon:16752762, 
coral:16744272, tomato:16737095, darkorange:16747520, orange:16753920, orangered:16729344, crimson:14423100, red:16711680, deeppink:16716947, fuchsia:16711935, magenta:16711935, hotpink:16738740, lightpink:16758465, pink:16761035, palevioletred:14381203, mediumvioletred:13047173, purple:8388736, darkmagenta:9109643, mediumpurple:9662683, blueviolet:9055202, indigo:4915330, darkviolet:9699539, darkorchid:10040012, mediumorchid:12211667, orchid:14315734, violet:15631086, plum:14524637, thistle:14204888, 
lavender:15132410, ghostwhite:16316671, aliceblue:15792383, mintcream:16121850, honeydew:15794160, lightgoldenrodyellow:16448210, lemonchiffon:16775885, ornsilk:16775388, lightyellow:16777184, ivory:16777200, floralwhite:16775920, linen:16445670, oldlace:16643558, antiquewhite:16444375, bisque:16770244, peachpuff:16767673, papayawhip:16773077, beige:16119260, seashell:16774638, lavenderblush:16773365, istyrose:16770273, snow:16775930, white:16777215, whitesmoke:16119285, gainsboro:14474460, lightgrey:13882323, 
silver:12632256, darkgray:11119017, gray:8421504, lightslategray:7833753, slategray:7372944, imgray:6908265, darkslategray:3100495, black:0}, tb = {blend:1, lightcolor:2, darkcolor:3, rgb:4, hsb:5};
Bb.prototype.cf = 0;
Bb.prototype.Ud = p;
Bb.prototype.mn = 0;
function Eb(a) {
  var b = q, c = 0, d = "", f = q;
  a.cf = 0;
  for(a.mn = a.Zj;!b && a.Zj < a.TD;) {
    var g = a.input.charAt(a.Zj), k = "a" <= g && "z" >= g, l = "0" <= g && "9" >= g, n = l || "a" <= g && "f" >= g, m = " " == g || "\t" == g;
    switch(c) {
      case 0:
        m ? a.mn = a.Zj + 1 : (d = g, k || "%" == g ? c = 1 : "0" == g ? c = 4 : l ? c = 2 : "#" == g ? (d = "0x", c = 3) : "." == g ? (f = j, c = 2) : "(" == g ? (a.cf = 6, a.Ud = g, a.Zj++, b = j) : ")" == g ? (a.cf = 7, a.Ud = g, a.Zj++, b = j) : "," == g ? (a.cf = 5, a.Ud = g, a.Zj++, b = j) : a.mn = a.Zj + 1);
        break;
      case 1:
        k ? d += g : (Fb(a, d), b = j);
        break;
      case 2:
        l ? d += g : "." == g ? f ? (a.cf = 4, a.Ud = Number(d), isNaN(a.Ud) && (a.Ud = 0), b = j) : (d += g, f = j) : (a.cf = 4, a.Ud = Number(d), isNaN(a.Ud) && (a.Ud = 0), b = j);
        break;
      case 3:
        n ? d += g : (a.cf = 3, a.Ud = Gb(Number(d)), b = j);
        break;
      case 4:
        "x" == g ? (d += g, c = 3) : l ? (d += g, c = 2) : "." == g ? (d += g, f = j, c = 2) : (a.cf = 4, a.Ud = 0, b = j)
    }
    b || a.Zj++
  }
  if(a.Zj < a.TD) {
    return b
  }
  switch(c) {
    case 0:
      return 0 != a.cf;
    case 1:
      return Fb(a, d), j;
    case 2:
      return a.cf = 4, a.Ud = Number(d), isNaN(a.Ud) && (a.Ud = 0), j;
    case 3:
      return a.cf = 3, a.Ud = Gb(Number(d)), j;
    case 4:
      return a.cf = 4, a.Ud = 0, j
  }
  return q
}
function Fb(a, b) {
  "%color" == b ? (a.cf = 2, a.Ud = b) : tb[b] != h ? (a.cf = 1, a.Ud = tb[b]) : (a.cf = 3, a.Ud = Gb(Db[b] != h ? Db[b] : 13421772))
}
function Gb(a) {
  var b = a >> 8;
  return new gb((b >> 8) % 256, b % 256, a % 256)
}
function Ab(a) {
  this.ue = new Bb(a)
}
Ab.prototype.ue = p;
Ab.prototype.parse = function() {
  for(var a = 0, b = [], c;Eb(this.ue);) {
    switch(a) {
      case 0:
        switch(this.ue.cf) {
          case 1:
            a = 1;
            b.push(ob(this.ue.Ud));
            break;
          case 3:
          ;
          case 2:
            a = 4;
            b.push(this.ue.Ud);
            break;
          default:
            e(Error("Color parser error: unexpected lexem at " + this.ue.mn + ' in "' + this.ue.input + '". Function name, color or %Color expected.'))
        }
        break;
      case 1:
        6 != this.ue.cf && e(Error("Color parser error: unexpected left parenthesis at " + this.ue.mn + ' in "' + this.ue.input + '". Left paren expected.'));
        a = 2;
        break;
      case 2:
        switch(this.ue.cf) {
          case 1:
            a = 1;
            b.push(ob(this.ue.Ud));
            break;
          case 3:
          ;
          case 2:
          ;
          case 4:
            a = 3;
            b[b.length - 1].arguments.push(this.ue.Ud);
            break;
          case 7:
            c = b.pop();
            a = c.uq();
            a != p && e(Error(a));
            c.pq() && (c = c.execute());
            0 == b.length ? (a = 4, b.push(c)) : (a = 3, b[b.length - 1].arguments.push(c));
            break;
          default:
            e(Error("Color parser error: unexpected lexem at " + this.ue.mn + ' in "' + this.ue.input + '". Function name, color, %Color, number or right paren expected.'))
        }
        break;
      case 3:
        switch(this.ue.cf) {
          case 7:
            c = b.pop();
            a = c.uq();
            a != p && e(Error(a));
            c.pq() && (c = c.execute());
            0 == b.length ? (a = 4, b.push(c)) : (a = 3, b[b.length - 1].arguments.push(c));
            break;
          case 5:
            a = 2;
            break;
          default:
            e(Error("Color parser error: unexpected lexem at " + this.ue.mn + ' in "' + this.ue.input + '". Expected comma or right paren.'))
        }
        break;
      case 4:
        e(Error("Color parser error: unexpected lexem at " + this.ue.mn + ' in "' + this.ue.input + '". End of string expected.'))
    }
  }
  4 != a && e(Error('Color parser error: unexpected end of string in "' + this.ue.input + '"'));
  return b.pop()
};
function Hb(a) {
  for(var a = a.replace(/^\s*/, ""), b = /\s/, c = a.length;b.test(a.charAt(--c));) {
  }
  return a.slice(0, c + 1)
}
var Ib = {};
function Jb(a) {
  return Kb((new DOMParser).parseFromString(a, "text/xml"))
}
function Kb(a) {
  if(!a) {
    return p
  }
  switch(a.nodeType) {
    case 1:
      var b = {}, c, d = a.childNodes.length;
      for(c = 0;c < d;c++) {
        var f = Kb(a.childNodes[c]);
        if(f != p) {
          var g = a.childNodes[c].nodeName;
          if("#" == g.charAt(0)) {
            b.value = b.value == h || "string" != typeof b.value ? f.value : b.value + f.value
          }else {
            if(b[g] == h) {
              b[g] = f
            }else {
              if(b[g] instanceof Array) {
                b[g].push(f)
              }else {
                if("value" != g || "string" != typeof b.value) {
                  b[g] = [b[g], f]
                }
              }
            }
          }
        }
      }
      d = a.attributes == p ? 0 : a.attributes.length;
      for(c = 0;c < d;c++) {
        f = a.attributes[c], b[f.nodeName] == h && (b[f.nodeName] = f.nodeValue)
      }
      return b;
    case 3:
      return a = Hb(a.nodeValue), "" == a ? p : {value:a};
    case 4:
      return{value:a.nodeValue};
    case 9:
      return Kb(a.documentElement);
    default:
      return p
  }
}
function C(a, b) {
  return a && a[b] != h
}
function F(a, b) {
  if(!a) {
    return p
  }
  var c = a[b];
  return c == p || c == h ? p : c instanceof Array ? c[0] : c
}
function H(a, b, c) {
  a && (a[b] = c)
}
function Lb(a, b) {
  a && a[b] && delete a[b]
}
function I(a, b) {
  if(!a || !b) {
    return[]
  }
  A.To(a[b]) ? a[b] instanceof Array || (a[b] = [a[b]]) : a[b] = [];
  return a[b]
}
function J(a, b) {
  if(!a) {
    return p
  }
  var c = a[b];
  if(!c) {
    return p
  }
  c = c instanceof Array ? c[0] : c;
  return"string" == typeof c ? c : c.value == h ? p : c.value
}
function Mb(a) {
  if(!a) {
    return NaN
  }
  a = Hb(a.toString());
  return"%" == a.charAt(a.length - 1)
}
function Nb(a) {
  return Mb(a) ? (a = Hb(a.toString()), a = a.substr(0, a.length - 1), Ob(a) / 100) : NaN
}
function Pb(a) {
  return Ob(a) / 100
}
function Qb(a, b) {
  return Pb(F(a, b))
}
function Rb(a) {
  return a == h ? q : "1" == a || "true" == a.toString().toLowerCase()
}
function K(a, b) {
  return Rb(F(a, b))
}
function Ob(a) {
  if(A.Qc(a)) {
    if(!Hb(a)) {
      return NaN
    }
  }else {
    if(A.mJ(a)) {
      return a
    }
    if(a == p) {
      return NaN
    }
  }
  return Number(a)
}
function M(a, b) {
  return Ob(F(a, b))
}
function Sb(a) {
  a = Number(a);
  1 < a ? a = 1 : 0 > a && (a = 0);
  return a
}
function Tb(a, b) {
  if(!a) {
    return q
  }
  var c = F(a, "enabled");
  return c == p || c == h ? b == h || b == p ? j : b : Rb(c)
}
function Ub(a, b) {
  return Tb(F(a, b))
}
function Cb(a) {
  return a ? a.toString().toLowerCase() : ""
}
function N(a, b) {
  return Cb(F(a, b))
}
function Vb(a) {
  return a ? a.toString().toLowerCase() : ""
}
function Wb(a, b) {
  return Vb(F(a, b))
}
function Xb(a) {
  return a != h && "auto" == Cb(a)
}
function Yb(a) {
  return zb(F(a, "color"), h)
}
function Zb(a) {
  var b;
  if(a instanceof Function) {
    b = a
  }else {
    if(a instanceof Array) {
      b = [];
      for(var c = 0, d = a.length;c < d;c++) {
        b[c] = Zb(a[c])
      }
    }else {
      if(a instanceof Object) {
        for(c in b = {}, a) {
          b[c] = Zb(a[c])
        }
      }else {
        b = a
      }
    }
  }
  return b
}
function $b(a, b, c, d) {
  if(a == p && b == p) {
    return p
  }
  if(a == p) {
    return b
  }
  if(b == p) {
    return a instanceof Array ? a[a.length - 1] : a
  }
  var a = Zb(a), b = Zb(b), f = {}, f = ac(a, b);
  if(c) {
    if("gradient" == c) {
      f.key = 0 != I(b, "key").length ? b.key : I(a, "key")
    }else {
      if("format" == c || "text" == c || "save_as_image_item_text" == c || "save_as_pdf_item_text" == c || "print_chart_item_text" == c) {
        return b
      }
      if("action" == c) {
        return 0 == I(b, "arg").length ? ac(a, b, f) : f = bc(a, b, d), f
      }
      f = bc(a, b, d)
    }
  }else {
    f = bc(a, b, d)
  }
  return f
}
function cc(a, b) {
  if(a == p && b == p) {
    return p
  }
  if(a == p) {
    return b
  }
  if(b == p) {
    return a
  }
  if(!dc(a)) {
    var c = Zb(a), d;
    for(d in b) {
      if(0 == I(c, d).length) {
        c[d] = b[d]
      }else {
        if(A.isArray(b[d])) {
          for(var f = 0;f < b[d].length;f++) {
            ec(c[d], b[d][f])
          }
        }else {
          ec(c[d], b[d])
        }
      }
    }
  }
  return c
}
function ec(a, b) {
  var c = F(b, "name");
  if(fc(a, c)) {
    var d;
    a: {
      c = Cb(c);
      if(!dc(a)) {
        for(d in a) {
          if(C(a[d], "name") && N(a[d], "name") == c) {
            break a
          }
        }
      }
      d = p
    }
    a[d] = b
  }else {
    a.push(b)
  }
}
function gc(a, b, c) {
  if(a == p && b == p) {
    return p
  }
  if(a == p) {
    return b
  }
  if(b == p) {
    return a
  }
  var a = Zb(a), d;
  for(d in b) {
    a[d] ? d == c ? (C(a[d], "name") && C(b[d], "name") && fc(a[d], F(a[d], "name")) == F(b[d], "name") && delete a[d], a[d] = b[d]) : a[d] = $b(b[d], a[d], d, h) : a[d] = b[d]
  }
  return a
}
function bc(a, b, c) {
  if(b instanceof Function) {
    return b
  }
  if(a instanceof Array && b instanceof Object && !(b instanceof Array)) {
    return $b(a[a.length - 1], b)
  }
  if(a instanceof Object && b instanceof Object) {
    var d, f;
    f = {};
    b instanceof Array && a instanceof Array && (f = []);
    for(d in a) {
      d != c ? (f[d] = $b(a[d], b[d]), delete b[d]) : f[d] = b[d]
    }
    for(d in b) {
      f[d] = b[d]
    }
  }else {
    f = b
  }
  return f
}
function ac(a, b, c) {
  c || (c = {});
  for(var d in a) {
    dc(a[d]) && (c[d] = a[d])
  }
  for(d in b) {
    dc(b[d]) && (c[d] = b[d])
  }
  return c
}
function dc(a) {
  return Boolean(a instanceof Function || !(a instanceof Array || a instanceof Object))
}
function fc(a, b) {
  return hc(a, "name", b)
}
function hc(a, b, c) {
  c = Cb(c);
  if(!dc(a)) {
    for(var d in a) {
      if(A.isArray(a[d])) {
        var f = fc(a[d], c);
        if(f) {
          return f
        }
      }
      if(C(a[d], b) && N(a[d], b) == c) {
        return a[d]
      }
    }
  }
  return p
}
function ic(a, b, c) {
  var d = a.length;
  if(b > d) {
    return a.push(c), a
  }
  var f = [], g;
  for(g = 0;g < b;g++) {
    f[g] = a[g]
  }
  f[b] = c;
  for(g = b + 1;g <= d;g++) {
    f[g] = a[g - 1]
  }
  return f
}
function jc(a, b) {
  var c = a.length, d = [], f = [], g;
  for(g = 0;g < c;g++) {
    if(C(a[g], "id")) {
      var k = F(a[g], "id");
      k == b && (d.push(g), f.push(k))
    }
  }
  c = d.length;
  for(g = 0;g < c;g++) {
    a.splice(d[g], 1)
  }
  return f
}
function kc(a, b) {
  Lb(b, "id");
  ac(a, b, a);
  for(var c in b) {
    c == p || c == h || (a[c] = "actions" == c || "extra_labels" == c || "extra_markers" == c || "extra_tooltips" == c ? b[c] : $b(a[c], b[c], c))
  }
}
function lc(a, b) {
  if(!a) {
    return p
  }
  var c = b.length;
  return!b || 0 == c ? a : mc(a, b, c)
}
function mc(a, b, c) {
  var d;
  if(a instanceof Function) {
    d = a
  }else {
    if(a instanceof Array) {
      d = [];
      for(var f = 0, g = a.length;f < g;f++) {
        d[f] = mc(a[f], b, c)
      }
    }else {
      if(a instanceof Object) {
        for(f in d = {}, a) {
          d[f] = mc(a[f], b, c)
        }
      }else {
        d = a;
        for(f = 0;f < c;f++) {
          -1 != a.indexOf(b[f][0].toString()) && (d = a.split(b[f][0]).join(b[f][1]))
        }
      }
    }
  }
  return d
}
function nc() {
}
var oc = p;
function pc(a, b) {
  var c = qc(a, b), d, f, g;
  if(0 == c.length % 2) {
    if(f = c[Math.floor(c.length / 2) - 1], g = c[Math.floor(c.length / 2) + 1], f == p || g == p) {
      return NaN
    }
  }else {
    if(d = c[Math.ceil(c.length / 2)], d == p) {
      return NaN
    }
  }
  return d ? d.Na(oc) : (f.Na(oc) + g.Na(oc)) / 2
}
nc.prototype.ml = function(a, b) {
  var c = {}, d, f = 0, g = 0;
  for(d = 0;d < a.length;d++) {
    var k = Number(a[d].Na(b));
    k && (c[k] != p ? c[k]++ : c[k] = 1, c[k] > g && (g = c[k], f = k))
  }
  return f
};
function qc(a, b) {
  oc = b;
  for(var c = [], d = 0, d = 0;d < a.length;d++) {
    c.push(a[d])
  }
  return c.sort(function(a, b) {
    return Number(a.Na(oc)) - Number(b.Na(oc))
  })
}
function sc(a, b) {
  var c = Math.floor(b);
  a.setUTCMonth(a.getUTCMonth() + c);
  b != c && tc(a, 30 * (b - c))
}
function tc(a, b) {
  var c = Math.floor(b);
  a.setUTCDate(a.getUTCDate() + c);
  b != c && uc(a, 24 * (b - c))
}
function uc(a, b) {
  var c = Math.floor(b);
  a.setUTCHours(a.getUTCHours() + c);
  b != c && vc(a, 60 * (b - c))
}
function vc(a, b) {
  var c = Math.floor(b);
  a.setUTCMinutes(a.getUTCMinutes() + c);
  b != c && a.setUTCSeconds(a.getUTCSeconds() + 60 * (b - c))
}
function wc() {
  return"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="
}
;function xc(a, b, c) {
  isNaN(a) || (this.ad = a);
  b && (this.Xa = b);
  c && (this.Aa = c)
}
z = xc.prototype;
z.Aa = NaN;
z.Ib = u("Aa");
z.qb = t("Aa");
z.Xa = zb(p, 1);
z.Ba = u("Xa");
z.ad = 1;
z.g = function(a) {
  C(a, "position") && (this.Aa = Sb(F(a, "position")));
  C(a, "opacity") && (this.ad *= Sb(F(a, "opacity")));
  C(a, "color") && (this.Xa = zb(F(a, "color"), this.ad))
};
z.LB = 0;
z.Fz = function(a) {
  this.LB = ub(this.Xa.Ba(a))
};
z.toString = function() {
  return"{ position: " + this.Aa + ", opacity: " + this.ad + ", calculatedColor: " + this.LB + "}"
};
function yc(a) {
  this.ad = a;
  this.pb = []
}
z = yc.prototype;
z.ad = 1;
z.pb = p;
z.ac = 0;
z.Wd = t("ac");
z.Tt = 0;
z.Ra = 0;
z.Jb = u("Ra");
z.gh = t("Ra");
z.k = p;
z.n = u("k");
z.g = function(a) {
  C(a, "type") && (this.Ra = "radial" == Vb(F(a, "type")) ? 1 : 0);
  C(a, "angle") && (this.ac = Ob(F(a, "angle")));
  if(C(a, "focal_point")) {
    var b = Number(F(a, "focal_point"));
    1 < b ? b = 1 : -1 > b && (b = -1);
    this.Tt = b
  }
  for(var a = I(a, "key"), b = a.length, c = 0;c < b;c++) {
    var d = new xc(this.ad);
    d.g(a[c]);
    this.pb[c] = d
  }
  a = this.pb.length;
  isNaN(this.pb[0].Ib()) && this.pb[0].qb(0);
  isNaN(this.pb[a - 1].Ib()) && this.pb[a - 1].qb(1);
  for(b = 1;;) {
    for(;b < a && !isNaN(this.pb[b].Ib());) {
      b++
    }
    if(b == a) {
      break
    }
    for(var c = b - 1, f = a - 1;b < a && isNaN(this.pb[b].Ib());) {
      b++
    }
    for(var d = this.pb[c].Ib(), f = this.pb[f].Ib(), g = 1;g < b - c;g++) {
      this.pb[g].qb(d + g * (f - d) / (b - c))
    }
  }
};
z.toString = function() {
  for(var a = "{ type: " + this.Ra + ", angle: " + this.ac + ", focal_point: " + this.Tt + ", keys: [", b = [], c = 0;c < this.pb.length;c++) {
    b.push(this.pb[c].toString())
  }
  a += b.join(", ");
  a = a + "]," + this.k.toString();
  return a + "}"
};
z.Fz = function(a, b) {
  this.k = a;
  for(var c = this.pb.length, d = 0;d < c;d++) {
    this.pb[d].Fz(b)
  }
};
function zc(a) {
  this.F = a;
  this.Vm = {}
}
var Ac = 0;
zc.prototype.F = p;
zc.prototype.Vm = p;
function Bc(a, b, c) {
  c || (c = q);
  var d = b.toString();
  if(a.Vm[d]) {
    return a.Vm[d]
  }
  if(0 == b.Jb()) {
    var f = c, c = Cc(a.F, "linearGradient");
    if(f) {
      var f = b.ac, g = b.n(), k = g.y + g.height / 2, l = g.x + g.width, n = g.y + g.height / 2, f = "rotate(" + f + ")";
      c.setAttribute("x1", g.x);
      c.setAttribute("y1", k);
      c.setAttribute("x2", l);
      c.setAttribute("y2", n);
      c.setAttribute("gradientTransform", f);
      c.setAttribute("spreadMethod", "pad");
      c.setAttribute("gradientUnits", "userSpaceOnUse")
    }else {
      f = b.ac * Math.PI / 180, f -= Math.PI / 2, l = Math.tan(f), k = g = 0, Math.abs(l) != Number.POSITIVE_INFINITY && (g = l / 2, k = 1 / (2 * l)), l = q, 0.5 >= Math.abs(g) ? (k = -0.5, l = 0 > Math.cos(f)) : (g = -0.5, l = 0 < Math.sin(f)), f = {x:0.5 + g, y:0.5 + k}, g = {x:0.5 - g, y:0.5 - k}, f = l ? {start:g, end:f} : {start:f, end:g}, c.setAttribute("x1", f.start.x), c.setAttribute("y1", f.start.y), c.setAttribute("x2", f.end.x), c.setAttribute("y2", f.end.y), c.setAttribute("gradientUnits", 
      "objectBoundingBox")
    }
    Dc(a, c, b);
    b = c
  }else {
    c || (c = q), c ? (c = Cc(a.F, "radialGradient"), f = b.ac * Math.PI / 180, k = Math.cos(f), l = Math.sin(f), n = b.n(), f = n.x + n.width / 2, g = n.y + n.height / 2, r = Math.min(n.width, n.height) / 2, n = b.Tt, k = f + n * r * k, l = g + n * r * l, c.setAttribute("cx", f), c.setAttribute("cy", g), c.setAttribute("r", r), c.setAttribute("fx", k), c.setAttribute("fy", l), c.setAttribute("gradientUnits", "userSpaceOnUse")) : (c = Cc(a.F, "radialGradient"), f = b.ac * Math.PI / 180, k = Math.cos(f), 
    l = Math.sin(f), f = new O(0.5, 0.5), g = new O(b.Tt * k, b.Tt * l), c.setAttribute("cx", f.x), c.setAttribute("cy", f.y), c.setAttribute("fx", (g.x + 1) / 2), c.setAttribute("fy", (g.y + 1) / 2), c.setAttribute("r", 1), c.setAttribute("gradientUnits", "objectBoundingBox")), c.setAttribute("spreadMethod", "pad"), Dc(a, c, b), b = c
  }
  c = "gradient_" + (Ac++).toString();
  b.setAttribute("id", c);
  c = "url(#" + c + ")";
  a.F.gl.appendChild(b);
  return a.Vm[d] = c
}
function Ec(a, b) {
  var c = b.substr(b.indexOf("fill:url(#") + 10, b.length - 10 - 2), d = "url(#" + c + ")", f;
  for(f in a.Vm) {
    if(a.Vm[f] == d) {
      delete a.Vm[f];
      var g = c, k = a.F.gl, l = k.childNodes, n = h;
      for(n in l) {
        l[n] != h && l[n].id == g && k.removeChild(l[n])
      }
    }
  }
}
function Dc(a, b, c) {
  for(var d = c.pb.length, f = 0;f < d;f++) {
    var g = c.pb[f], k = Cc(a.F, "stop");
    k.setAttribute("offset", "" + 100 * g.Aa + "%");
    k.setAttribute("style", "stop-color:" + g.LB + ";stop-opacity:" + g.ad);
    b.appendChild(k)
  }
}
zc.prototype.clear = function() {
  Ac = 0;
  this.Vm = {}
};
function Fc() {
}
function Gc(a) {
  switch(a) {
    case "backwarddiagonal":
      return 0;
    case "forwarddiagonal":
      return 1;
    case "horizontal":
      return 2;
    case "vertical":
      return 3;
    case "dashedbackwarddiagonal":
      return 4;
    case "dashedforwarddiagonal":
      return 6;
    case "dashedhorizontal":
      return 7;
    case "dashedvertical":
      return 8;
    case "diagonalcross":
      return 9;
    case "diagonalbrick":
      return 10;
    case "divot":
      return 11;
    case "horizontalbrick":
      return 12;
    case "verticalbrick":
      return 13;
    case "checkerboard":
      return 14;
    case "confetti":
      return 15;
    case "plaid":
      return 16;
    case "soliddiamond":
      return 17;
    case "zigzag":
      return 18;
    case "weave":
      return 19;
    case "percent05":
      return 20;
    case "percent10":
      return 21;
    case "percent20":
      return 22;
    case "percent25":
      return 23;
    case "percent30":
      return 24;
    case "percent40":
      return 25;
    case "percent50":
      return 26;
    case "percent60":
      return 27;
    case "percent70":
      return 28;
    case "percent75":
      return 29;
    case "percent80":
      return 30;
    case "percent90":
      return 31;
    case "grid":
      return 5;
    default:
      return 0
  }
}
z = Fc.prototype;
z.Ma = j;
z.isEnabled = u("Ma");
z.Ra = 0;
z.bE = q;
z.Jb = u("Ra");
z.Xa = zb(p, 0.5);
z.Ba = u("Xa");
z.ad = 0.5;
z.Rh = 1;
z.Va = u("Rh");
z.yz = 10;
z.g = function(a) {
  if(this.Ma = Tb(a)) {
    if(C(a, "type")) {
      var b = N(a, "type");
      this.bE = "%hatchtype" == b;
      this.bE || (this.Ra = Gc(Vb(b)))
    }
    C(a, "opacity") && (this.ad = Sb(F(a, "opacity")));
    C(a, "color") && (this.Xa = zb(N(a, "color"), this.ad));
    C(a, "thickness") && (this.Rh = Ob(F(a, "thickness")));
    C(a, "pattern_size") && (this.yz = Math.floor(Number(F(a, "pattern_size"))))
  }
};
z.toString = function() {
  return"T" + this.Ra + "C" + vb(this.Xa.Ba()) + "TH" + this.Rh + "PS" + this.yz
};
function Hc(a, b, c, d) {
  if(!a.Ma) {
    return""
  }
  a.Ra = a.bE ? c : a.Ra;
  b = b.ED;
  c = a.toString();
  if(b.jv[c]) {
    a = b.jv[c]
  }else {
    var f = "hatchfill_" + Ic++, g = "" + a.yz, k = Cc(b.F, "pattern");
    k.setAttribute("id", f);
    k.setAttribute("x", 0);
    k.setAttribute("y", 0);
    k.setAttribute("width", g);
    k.setAttribute("height", g);
    k.setAttribute("patternUnits", "userSpaceOnUse");
    var g = a.yz, l, n, m;
    switch(a.Jb()) {
      case 0:
        l = b.F.ja();
        m = Jc(-1, 0, g + 1, 0, a.Va());
        l.setAttribute("d", m);
        k.setAttribute("patternTransform", "rotate(-45)");
        k.appendChild(l);
        Kc(a, l, d);
        break;
      case 1:
        l = b.F.ja();
        m = Jc(-1, 0, g + 1, 0, a.Va());
        l.setAttribute("d", m);
        k.setAttribute("patternTransform", "rotate(45)");
        k.appendChild(l);
        Kc(a, l, d);
        break;
      case 2:
        l = b.F.ja();
        m = Jc(-1, g / 2, g + 1, g / 2, a.Va());
        l.setAttribute("d", m);
        k.appendChild(l);
        Kc(a, l, d);
        break;
      case 3:
        l = b.F.ja();
        m = Jc(g / 2, -1, g / 2, g + 1, a.Va());
        l.setAttribute("d", m);
        k.appendChild(l);
        Kc(a, l, d);
        break;
      case 9:
        l = b.F.ja();
        m = Jc(0, g / 2, g, g / 2, a.Va());
        m += Jc(g / 2, 0, g / 2, g, a.Va());
        l.setAttribute("d", m);
        k.setAttribute("patternTransform", "rotate(45)");
        k.appendChild(l);
        Kc(a, l, d);
        break;
      case 5:
        l = b.F.ja();
        m = Jc(-1, g / 2, g + 1, g / 2, a.Va());
        m += Jc(g / 2, -1, g / 2, g + 1, a.Va());
        l.setAttribute("d", m);
        k.appendChild(l);
        Kc(a, l, d);
        break;
      case 12:
        Lc(b, k, a, g, d);
        break;
      case 13:
        Lc(b, k, a, g, d);
        k.setAttribute("patternTransform", "rotate(90)");
        break;
      case 10:
        b.F.ja();
        Lc(b, k, a, g, d);
        k.setAttribute("patternTransform", "rotate(45)");
        break;
      case 14:
        l = Mc(b.F, 0, 0, g / 2, g / 2);
        g = Mc(b.F, g / 2, g / 2, g, g);
        k.appendChild(l);
        k.appendChild(g);
        Nc(a, l, d);
        Nc(a, g, d);
        break;
      case 15:
        l = g / 8;
        g = new P(0, 0, g / 4, g / 4);
        Q(b.F, 0, 2 * l, g.width, g.height, a, k, d);
        Q(b.F, l, 5 * l, g.width, g.height, a, k, d);
        Q(b.F, 2 * l, 0, g.width, g.height, a, k, d);
        Q(b.F, 4 * l, 4 * l, g.width, g.height, a, k, d);
        Q(b.F, 5 * l, l, g.width, g.height, a, k, d);
        Q(b.F, 6 * l, 6 * l, g.width, g.height, a, k, d);
        break;
      case 16:
        Q(b.F, 0, 0, g / 2, g / 2, a, k, d);
        l = g / 8;
        n = q;
        for(m = 0;2 > m;m++) {
          n = q;
          for(var o = 0;4 > o;o++) {
            n = !n;
            for(var v = 0;4 > v;v++) {
              n && Q(b.F, o * l + m * g / 2, v * l + g / 2, l, l, a, k, d), n = !n
            }
          }
        }
        break;
      case 17:
        l = b.F.ja();
        m = S(g / 2, 0);
        m += U(0, g / 2);
        m += U(g / 2, g);
        m += U(g, g / 2);
        m += U(g / 2, 0);
        l.setAttribute("d", m + " Z");
        k.appendChild(l);
        Nc(a, l, d);
        break;
      case 6:
        l = b.F.ja();
        m = Jc(0, 0, g / 2, g / 2, a.Va());
        l.setAttribute("d", m);
        k.appendChild(l);
        Kc(a, l, d);
        break;
      case 4:
        l = b.F.ja();
        m = Jc(g / 2, 0, 0, g / 2, a.Va());
        l.setAttribute("d", m);
        k.appendChild(l);
        Kc(a, l, d);
        break;
      case 7:
        l = b.F.ja();
        m = Jc(0, 0, g / 2, 0, a.Va());
        m += Jc(g / 2, g / 2, g, g / 2, a.Va());
        l.setAttribute("d", m);
        k.appendChild(l);
        Kc(a, l, d);
        break;
      case 8:
        l = b.F.ja();
        m = Jc(0, 0, 0, g / 2, a.Va());
        m += Jc(g / 2, g / 2, g / 2, g, a.Va());
        l.setAttribute("d", m);
        k.appendChild(l);
        Kc(a, l, d);
        break;
      case 11:
        o = 0.1 * g;
        n = g * (0.8 - 0.2) / 2;
        l = b.F.ja();
        m = S(o + n, o);
        m += U(o, o + n / 2);
        m += U(o + n, o + n);
        m += S(g - o - n, g - o - n);
        m += U(g - o, g - o - n / 2);
        m += U(g - o - n, g - o);
        l.setAttribute("d", m);
        k.appendChild(l);
        Kc(a, l, d);
        break;
      case 18:
        l = b.F.ja();
        m = S(0, 0);
        m += U(g / 2, g / 2);
        m += U(g, 0);
        m += S(0, g / 2);
        m += U(g / 2, g);
        m += U(g, g / 2);
        l.setAttribute("d", m);
        k.appendChild(l);
        Kc(a, l, d);
        break;
      case 19:
        l = b.F.ja();
        m = S(0, 0);
        m += U(g / 2, g / 2);
        m += U(g, 0);
        m += S(0, g / 2);
        m += U(g / 2, g);
        m += U(g, g / 2);
        m += S(g / 2, g / 2);
        m += U(3 * g / 4, 3 * g / 4);
        m += S(g, g / 2);
        m += U(3 * g / 4, g / 4);
        l.setAttribute("d", m);
        k.appendChild(l);
        Kc(a, l, d);
        break;
      case 20:
        k.setAttribute("width", 8);
        k.setAttribute("height", 8);
        Q(b.F, 0, 0, 1, 1, a, k, d);
        Q(b.F, 4, 4, 1, 1, a, k, d);
        break;
      case 21:
        k.setAttribute("width", 8);
        k.setAttribute("height", 4);
        Q(b.F, 0, 0, 1, 1, a, k, d);
        Q(b.F, 4, 2, 1, 1, a, k, d);
        break;
      case 22:
        k.setAttribute("width", 4);
        k.setAttribute("height", 4);
        Q(b.F, 0, 0, 1, 1, a, k, d);
        Q(b.F, 2, 2, 1, 1, a, k, d);
        break;
      case 23:
        k.setAttribute("width", 4);
        k.setAttribute("height", 2);
        Q(b.F, 0, 0, 1, 1, a, k, d);
        Q(b.F, 2, 1, 1, 1, a, k, d);
        break;
      case 24:
        k.setAttribute("width", 4);
        k.setAttribute("height", 4);
        Q(b.F, 0, 0, 1, 1, a, k, d);
        Q(b.F, 2, 0, 1, 1, a, k, d);
        Q(b.F, 3, 1, 1, 1, a, k, d);
        Q(b.F, 0, 2, 1, 1, a, k, d);
        Q(b.F, 2, 2, 1, 1, a, k, d);
        Q(b.F, 1, 3, 1, 1, a, k, d);
        break;
      case 25:
        k.setAttribute("width", 4);
        k.setAttribute("height", 8);
        Q(b.F, 0, 0, 1, 1, a, k, d);
        Q(b.F, 2, 0, 1, 1, a, k, d);
        Q(b.F, 3, 1, 1, 1, a, k, d);
        Q(b.F, 0, 2, 1, 1, a, k, d);
        Q(b.F, 2, 2, 1, 1, a, k, d);
        Q(b.F, 1, 3, 1, 1, a, k, d);
        Q(b.F, 3, 3, 1, 1, a, k, d);
        Q(b.F, 0, 4, 1, 1, a, k, d);
        Q(b.F, 2, 4, 1, 1, a, k, d);
        Q(b.F, 1, 5, 1, 1, a, k, d);
        Q(b.F, 3, 5, 1, 1, a, k, d);
        Q(b.F, 0, 6, 1, 1, a, k, d);
        Q(b.F, 2, 6, 1, 1, a, k, d);
        Q(b.F, 1, 7, 1, 1, a, k, d);
        Q(b.F, 3, 7, 1, 1, a, k, d);
        break;
      case 26:
        k.setAttribute("width", 2);
        k.setAttribute("height", 2);
        Q(b.F, 0, 0, 1, 1, a, k, d);
        Q(b.F, 1, 1, 1, 1, a, k, d);
        break;
      case 27:
        k.setAttribute("width", 4);
        k.setAttribute("height", 4);
        Q(b.F, 0, 0, 1, 1, a, k, d);
        Q(b.F, 2, 0, 1, 1, a, k, d);
        Q(b.F, 0, 1, 1, 1, a, k, d);
        Q(b.F, 1, 1, 1, 1, a, k, d);
        Q(b.F, 3, 1, 1, 1, a, k, d);
        Q(b.F, 0, 2, 1, 1, a, k, d);
        Q(b.F, 2, 2, 1, 1, a, k, d);
        Q(b.F, 1, 3, 1, 1, a, k, d);
        Q(b.F, 2, 3, 1, 1, a, k, d);
        Q(b.F, 3, 3, 1, 1, a, k, d);
        break;
      case 28:
        k.setAttribute("width", 4);
        k.setAttribute("height", 4);
        Q(b.F, 0, 0, 1, 1, a, k, d);
        Q(b.F, 2, 0, 1, 1, a, k, d);
        Q(b.F, 3, 0, 1, 1, a, k, d);
        Q(b.F, 0, 1, 1, 1, a, k, d);
        Q(b.F, 1, 1, 1, 1, a, k, d);
        Q(b.F, 2, 1, 1, 1, a, k, d);
        Q(b.F, 0, 2, 1, 1, a, k, d);
        Q(b.F, 2, 2, 1, 1, a, k, d);
        Q(b.F, 3, 2, 1, 1, a, k, d);
        Q(b.F, 0, 3, 1, 1, a, k, d);
        Q(b.F, 1, 3, 1, 1, a, k, d);
        Q(b.F, 2, 3, 1, 1, a, k, d);
        break;
      case 29:
        k.setAttribute("width", 4);
        k.setAttribute("height", 4);
        Q(b.F, 0, 0, 4, 4, a, k, d);
        Oc(b.F, 0, 0, k);
        Oc(b.F, 2, 2, k);
        break;
      case 30:
        k.setAttribute("width", 8);
        k.setAttribute("height", 4);
        for(g = 0;8 > g;g++) {
          for(l = 0;4 > l;l++) {
            Q(b.F, g, l, 1, 1, a, k, d)
          }
        }
        Oc(b.F, 0, 0, k);
        Oc(b.F, 4, 2, k);
        break;
      case 31:
        k.setAttribute("width", 8);
        k.setAttribute("height", 8);
        for(g = 0;8 > g;g++) {
          for(l = 0;8 > l;l++) {
            Q(b.F, g, l, 1, 1, a, k, d)
          }
        }
        Oc(b.F, 7, 7, k);
        Oc(b.F, 4, 3, k)
    }
    b.F.gl.appendChild(k);
    f = "url(#" + f + ")";
    a = b.jv[c] = f
  }
  return"fill:" + a + ";"
}
function Qc(a) {
  this.F = a;
  this.jv = {}
}
var Ic = 0;
Qc.prototype.F = p;
Qc.prototype.jv = p;
function Lc(a, b, c, d, f) {
  var a = a.F.ja(), g = Jc(0, 0, 0, d / 2 - 1, c.Va()), g = g + Jc(0, d / 2 - 1, d, d / 2 - 1, c.Va()), g = g + Jc(d / 2, d / 2 - 1, d / 2, d - 1, c.Va()), g = g + Jc(0, d - 1, d, d - 1, c.Va());
  a.setAttribute("d", g);
  b.appendChild(a);
  Kc(c, a, f)
}
function Q(a, b, c, d, f, g, k, l) {
  a = Mc(a, b, c, d, f);
  Nc(g, a, l);
  k.appendChild(a)
}
function Oc(a, b, c, d) {
  a = Mc(a, b, c, 1, 1);
  a.setAttribute("fill", "white");
  a.setAttribute("stroke", "none");
  d.appendChild(a)
}
function Kc(a, b, c) {
  b.setAttribute("fill", "none");
  b.setAttribute("stroke-width", a.Va());
  b.setAttribute("stroke-opacity", a.ad);
  b.setAttribute("stroke", ub(a.Ba().Ba(c)))
}
function Nc(a, b, c) {
  b.setAttribute("fill", ub(a.Ba().Ba(c)));
  b.setAttribute("fill-opacity", a.ad);
  b.setAttribute("stroke", "none")
}
Qc.prototype.clear = function() {
  this.vE = 0;
  this.jv = {}
};
function Rc() {
  this.Ma = j
}
z = Rc.prototype;
z.Ma = p;
z.isEnabled = function() {
  return Boolean(this.Ma && this.Hq && 0 < this.Hq.length)
};
z.Hq = p;
z.g = s();
z.toString = function() {
  for(var a = this.Hq.length, b = "", c = 0;c < a;c++) {
    var d = this.Hq[c];
    d && d.isEnabled() && (b += d.toString())
  }
  return b
};
function Sc(a) {
  this.F = a;
  this.clear()
}
Sc.prototype.vE = p;
Sc.prototype.zx = p;
function Tc(a, b) {
  var c = b.toString();
  if(a.zx[c]) {
    return a.zx[c]
  }
  var d;
  d = a.F;
  if(b.Ma) {
    var f = Cc(d, "filter"), g = b.Hq.length;
    if(0 == g) {
      d = p
    }else {
      for(var k = 0;k < g;k++) {
        var l = b.Hq[k];
        l && l.isEnabled() && l.kQ(f, d)
      }
      d = f
    }
  }else {
    d = p
  }
  if(!d) {
    return"filter:none"
  }
  f = "effect_" + a.vE++;
  d.setAttribute("id", f);
  d.setAttribute("filterUnits", "userSpaceOnUse");
  a.F.gl.appendChild(d);
  f = "url(#" + f + ")";
  return a.zx[c] = f
}
Sc.prototype.clear = function() {
  this.vE = 0;
  this.zx = {}
};
function Uc(a, b) {
  return 0 >= a ? 0 : a * Math.round(1E15 * Math.cos(b * Math.PI / 180)) / 1E15
}
function Vc(a, b) {
  return 0 >= a ? 0 : a * Math.round(1E15 * Math.sin(b * Math.PI / 180)) / 1E15
}
function O(a, b) {
  a != h && (this.x = Number(a));
  b != h && (this.y = Number(b))
}
O.prototype.x = 0;
O.prototype.y = 0;
O.prototype.Ca = function() {
  return new O(this.x, this.y)
};
O.prototype.translate = function(a, b) {
  this.x += a;
  this.y += b
};
function P(a, b, c, d) {
  a != h && (this.x = Number(a));
  b != h && (this.y = Number(b));
  c != h && (this.width = Number(c));
  d != h && (this.height = Number(d))
}
z = P.prototype;
z.x = 0;
z.y = 0;
z.width = 0;
z.height = 0;
z.tb = u("x");
z.rb = u("y");
z.Ja = function() {
  return this.x + this.width
};
z.ra = function() {
  return this.y + this.height
};
z.bh = function(a) {
  a -= this.tb();
  this.x += a;
  this.width -= a
};
z.eh = function(a) {
  this.width -= this.Ja() - a
};
z.fh = function(a) {
  a -= this.rb();
  this.y += a;
  this.height -= a
};
z.$g = function(a) {
  this.height -= this.ra() - a
};
z.ae = function() {
  return new O(this.x + this.width / 2, this.y + this.height / 2)
};
function Wc(a, b) {
  return!(a.Ja() < b.tb() || a.ra() < b.rb() || a.tb() > b.Ja() || a.rb() > b.ra())
}
function Xc(a, b) {
  return a.x <= b.x && a.Ja() >= b.Ja() && a.y <= b.y && a.ra() >= b.ra()
}
z.translate = function(a, b) {
  this.x += a;
  this.y += b
};
function Yc(a, b, c) {
  a.x -= b;
  a.width += 2 * b;
  a.y -= c;
  a.height += 2 * c
}
z.Ca = function() {
  return new P(this.x, this.y, this.width, this.height)
};
z.toString = function() {
  return this.x.toFixed(1) + " " + this.y.toFixed(1) + " " + this.width.toFixed(1) + " " + this.height.toFixed(1)
};
function Zc() {
}
z = Zc.prototype;
z.bc = 0;
z.Pl = 0;
z.sin = 0;
z.cos = 1;
z.xf = 0;
z.wf = 1;
z.Ae = function() {
  360 <= Math.abs(this.bc) && (this.bc %= 360);
  0 > this.bc && (this.bc += 360);
  this.Pl = this.bc * Math.PI / 180;
  this.sin = Math.sin(this.Pl);
  this.cos = Math.cos(this.Pl);
  this.xf = 0 > this.sin ? -this.sin : this.sin;
  this.wf = 0 > this.cos ? -this.cos : this.cos
};
function $c() {
  this.Lk = document.createElementNS(this.gG, "svg");
  this.Lk.setAttribute("xmlns", this.gG);
  this.Lk.setAttribute("xmlns:xlink", "http://www.w3.org/1999/xlink");
  this.is = new P;
  bd(this);
  this.Jr = this.Ca(this.Lk);
  this.Jr.setAttribute("id", "AnyChart SVG element for text measure");
  this.Jr.setAttribute("width", "0");
  this.Jr.setAttribute("height", "0");
  this.gl = Cc(this, "defs");
  this.Lk.appendChild(this.gl);
  this.cr = new zc(this);
  this.GD = new cd(this);
  this.PK = new dd(this);
  this.ED = new Qc(this);
  this.gk = new Sc(this)
}
z = $c.prototype;
z.gG = "http://www.w3.org/2000/svg";
z.Lk = p;
z.Nc = u("Lk");
z.is = p;
z.kD = function() {
  bd(this);
  return this.is
};
function bd(a) {
  try {
    var b = a.Lk.getBBox();
    a.is.x = b.x;
    a.is.y = b.y;
    a.is.width = b.width;
    a.is.height = b.height
  }catch(c) {
  }
}
z.gl = p;
z.Jr = p;
z.gk = p;
z.cr = p;
z.GD = p;
z.PK = p;
z.ED = p;
z.ja = function(a) {
  var b = Cc(this, "path");
  a && b.setAttribute("shape-rendering", "crispEdges");
  return b
};
function S(a, b) {
  return"M" + a.toString() + "," + b.toString()
}
function U(a, b) {
  return" L" + a.toString() + "," + b.toString()
}
function Jc(a, b, c, d, f) {
  a === c && (a = c = Math.round(a) + f % 2 / 2);
  b === d && (b = d = Math.round(b) + f % 2 / 2);
  return"M" + a.toString() + "," + b.toString() + " L" + c.toString() + "," + d.toString()
}
z.ub = function(a, b, c, d, f, g) {
  5 == arguments.length && (g = a);
  return 0 == a || 0 == g ? "" : " A" + a.toString() + "," + g.toString() + " 0 " + (b ? "1" : "0") + " " + (c ? "1" : "0") + " " + d.toString() + "," + f.toString()
};
function ed(a, b, c, d) {
  var f = b + d, b = b + d * Math.cos(359.999 * Math.PI / 180), g = c + d * Math.sin(359.999 * Math.PI / 180), c = "" + S(f, c);
  return c += a.ub(d, 1, 1, b, g, d)
}
z.WE = function(a, b, c, d, f, g, k) {
  return f && 7 == arguments.length ? " Q" + a.toString() + "," + b.toString() + " " + c.toString() + "," + d.toString() + " T" + g.toString() + "," + k.toString() : " Q" + a.toString() + "," + b.toString() + " " + c.toString() + "," + d.toString()
};
function fd(a, b, c, d) {
  var c = a + c, d = b + d, f = S(a, b), f = f + U(c, b), f = f + U(c, d), f = f + U(a, d);
  return f += U(a, b)
}
function gd(a) {
  return fd(a.x, a.y, a.width, a.height)
}
function hd(a) {
  return Cc(a, "polyline")
}
function id(a, b, c) {
  c && (a = Math.round(10 * a) / 10, b = Math.round(10 * b) / 10);
  return a.toString() + "," + b.toString() + " "
}
var jd = 0;
function Cc(a, b) {
  return a.Lk.ownerDocument.createElementNS(a.gG, b)
}
function kd(a) {
  for(;0 < a.childNodes.length;) {
    a.removeChild(a.childNodes[0])
  }
}
function Mc(a, b, c, d, f) {
  0 > d && (d = Math.abs(d), b -= d);
  0 > f && (f = Math.abs(f), c -= f);
  a = Cc(a, "rect");
  a.setAttribute("x", b);
  a.setAttribute("y", c);
  a.setAttribute("width", d);
  a.setAttribute("height", f);
  return a
}
function ld(a) {
  a.setAttribute("cursor", "pointer")
}
function md() {
  return"fill:none;"
}
function nd() {
  return"stroke:none;"
}
function od() {
  return md() + nd()
}
$c.prototype.Ca = function(a) {
  return a.cloneNode(q)
};
function W(a) {
  pa.call(this);
  this.I = Cc(a, "g");
  this.F = a;
  this.wq = []
}
A.e(W, pa);
z = W.prototype;
z.au = function() {
  return this.I.getAttribute("id")
};
z.F = p;
z.r = u("F");
z.I = p;
z.Vb = 0;
z.ma = 0;
z.Bb = 0;
z.vK = 0;
z.wK = 0;
z.wq = p;
z.Yb = p;
z.getParent = u("Yb");
z.em = p;
z.appendChild = function(a) {
  a && this.I.appendChild(a)
};
z.removeChild = function(a) {
  if(a && a.I) {
    var b;
    b = this.wq.length;
    for(var c = 0;c < b;c++) {
      this.wq[c] == a && this.wq.splice(c, 1)
    }
    b = this.OE();
    for(c = 0;c < b;c++) {
      this.I.childNodes[c] == a.I && this.I.removeChild(this.I.childNodes[c])
    }
  }
};
z.OE = function() {
  return this.I.childElementCount
};
z.Xd = function(a) {
  this.Vb = a;
  pd(this)
};
z.Hd = u("Vb");
function qd(a, b) {
  a.Vb += b;
  pd(a)
}
z.gd = function(a) {
  this.ma = a;
  pd(this)
};
z.Sb = u("ma");
z.Wl = function(a, b, c) {
  this.Bb = a;
  b != h && (this.vK = b);
  c != h && (this.wK = c);
  pd(this)
};
z.qb = function(a, b) {
  this.Vb = a;
  this.ma = b;
  pd(this)
};
function pd(a) {
  a.I.setAttribute("transform", "translate(" + Number(a.Vb).toString() + "," + Number(a.ma).toString() + ") rotate(" + Number(a.Bb) + " " + Number(a.vK).toString() + " " + Number(a.wK).toString() + ")")
}
z.Aj = function(a) {
  this.em = a;
  a === j ? this.I.setAttribute("visibility", "visible") : this.I.setAttribute("visibility", "hidden")
};
z.clear = function() {
  kd(this.I)
};
z.ia = function(a) {
  this.wq.push(a);
  this.I.appendChild(a.I);
  a.Yb = this
};
function rd(a) {
  if(a.Yb) {
    var b = rd(a.Yb);
    return new O(a.Yb.Hd() + b.x, a.Yb.Sb() + b.y)
  }
  return new O(0, 0)
}
z.ax = p;
function sd(a, b) {
  if(b) {
    var c = Cc(a.F, "clipPath");
    c.setAttribute("id", "anychart_clippath_" + (jd++).toString());
    c.setAttribute("clip-rule", "nonzero");
    a.ax = a.F.ja();
    c.appendChild(a.ax);
    a.appendChild(c);
    a.I.setAttribute("clip-path", "url(#" + c.getAttribute("id") + ")");
    a.I.setAttribute("clipPathUnits", "userSpaceOnUse")
  }
}
function td(a, b) {
  b && (a.ax || sd(a, b), a.ax.setAttribute("d", gd(b)))
}
function ud(a) {
  W.call(this, a);
  this.Gp = new W(a);
  this.wq.push(this.Gp);
  this.I.appendChild(this.Gp.I);
  this.Gp.Yb = this
}
A.e(ud, W);
ud.prototype.Gp = p;
ud.prototype.appendChild = function(a) {
  a && this.Gp.appendChild(a)
};
ud.prototype.ia = function(a) {
  a && a.I && this.Gp.ia(a)
};
ud.prototype.scrollTo = function(a, b) {
  a && !isNaN(a) && b && !isNaN(b) && this.Gp.qb(a, b)
};
function vd(a, b, c, d) {
  switch(b) {
    case 0:
      a.x -= c.width + d;
      break;
    case 1:
      a.x += d;
      break;
    case 2:
      a.x -= c.width / 2
  }
}
function wd(a) {
  a = Cb(a);
  switch(a) {
    case "left":
      return 0;
    case "right":
      return 1;
    case "center":
      return 2;
    default:
      return 2
  }
}
var xd = {};
function yd(a, b, c, d) {
  switch(b) {
    case 0:
      a.y -= c.height + d;
      break;
    case 2:
      a.y += d;
      break;
    case 1:
      a.y -= c.height / 2
  }
}
function zd(a) {
  a = Cb(a);
  switch(a) {
    case "top":
      return 0;
    default:
    ;
    case "center":
      return 1;
    case "bottom":
      return 2
  }
}
function Ad(a) {
  a = Cb(a);
  switch(a) {
    default:
    ;
    case "center":
      return 0;
    case "centerleft":
    ;
    case "leftcenter":
    ;
    case "left":
      return 1;
    case "lefttop":
    ;
    case "topleft":
      return 2;
    case "centertop":
    ;
    case "topcenter":
    ;
    case "top":
      return 3;
    case "righttop":
    ;
    case "topright":
      return 4;
    case "centerright":
    ;
    case "rightcenter":
    ;
    case "right":
      return 5;
    case "rightbottom":
    ;
    case "bottomright":
      return 6;
    case "centerbottom":
    ;
    case "bottomcenter":
    ;
    case "bottom":
      return 7;
    case "leftbottom":
    ;
    case "bottomleft":
      return 8;
    case "float":
      return 9;
    case "xaxis":
      return 10
  }
}
function Bd(a) {
  a == p || a == h ? this.Ql(10) : this.Ql(a)
}
z = Bd.prototype;
z.wc = 0;
z.tb = u("wc");
z.bh = t("wc");
z.Pz = 0;
z.Ja = u("Pz");
z.eh = t("Pz");
z.le = 0;
z.rb = u("le");
z.fh = t("le");
z.Lw = 0;
z.ra = u("Lw");
z.$g = t("Lw");
z.Ql = function(a) {
  this.wc = this.Pz = this.le = this.Lw = a
};
z.g = function(a) {
  C(a, "all") && this.Ql(isNaN(M(a, "all")) ? 10 : M(a, "all"));
  C(a, "left") && this.bh(isNaN(M(a, "left")) ? 10 : M(a, "left"));
  C(a, "right") && this.eh(isNaN(M(a, "right")) ? 10 : M(a, "right"));
  C(a, "top") && this.fh(isNaN(M(a, "top")) ? 10 : M(a, "top"));
  C(a, "bottom") && this.$g(isNaN(M(a, "bottom")) ? 10 : M(a, "bottom"))
};
function Cd(a, b) {
  b.x += a.wc;
  b.y += a.le;
  b.width -= a.wc + a.Pz;
  b.height -= a.le + a.Lw
}
;function Dd() {
}
z = Dd.prototype;
z.Ra = 0;
z.Jb = u("Ra");
z.gh = t("Ra");
z.P = 0;
z.ha = 0;
z.jD = u("ha");
z.sa = 0;
z.X = 0;
z.g = function(a) {
  if(C(a, "type")) {
    switch(Cb(F(a, "type"))) {
      case "square":
        this.Ra = 0;
        break;
      case "rounded":
        this.Ra = 1;
        break;
      case "roundedinner":
        this.Ra = 2;
        break;
      case "cut":
        this.Ra = 3
    }
  }
  if(0 != this.Ra && (C(a, "all") && this.Ql(Ob(F(a, "all"))), C(a, "left_top") && (this.P = Ob(F(a, "left_top"))), C(a, "right_top") && (this.ha = Ob(F(a, "right_top"))), C(a, "right_bottom") && (this.X = Ob(F(a, "right_bottom"))), C(a, "left_bottom"))) {
    this.sa = Ob(F(a, "left_bottom"))
  }
};
function Ed(a, b, c) {
  var d;
  switch(a.Ra) {
    default:
    ;
    case 0:
      d = S(c.x, c.y);
      d += U(c.x + c.width, c.y);
      d += U(c.x + c.width, c.y + c.height);
      d += U(c.x, c.y + c.height);
      d += U(c.x, c.y);
      d += " Z";
      break;
    case 1:
      d = S(c.x + a.P, c.y);
      d += U(c.x + c.width - a.ha, c.y);
      d += b.ub(a.ha, q, j, c.x + c.width, c.y + a.ha);
      d += U(c.x + c.width, c.y + c.height - a.X);
      d += b.ub(a.X, q, j, c.x + c.width - a.X, c.y + c.height);
      d += U(c.x + a.sa, c.y + c.height);
      d += b.ub(a.sa, q, j, c.x, c.y + c.height - a.sa);
      0 != a.P && (d += U(c.x, c.y + a.P), d += b.ub(a.P, q, j, c.x + a.P, c.y));
      d += " Z";
      break;
    case 2:
      d = S(c.x + a.P, c.y);
      d += U(c.x + c.width - a.ha, c.y);
      d += b.ub(a.ha, q, q, c.x + c.width, c.y + a.ha);
      d += U(c.x + c.width, c.y + c.height - a.X);
      d += b.ub(a.X, q, q, c.x + c.width - a.X, c.y + c.height);
      d += U(c.x + a.sa, c.y + c.height);
      d += b.ub(a.sa, q, q, c.x, c.y + c.height - a.sa);
      0 != a.P && (d += U(c.x, c.y + a.P), d += b.ub(a.P, q, q, c.x + a.P, c.y));
      d += " Z";
      break;
    case 3:
      d = S(c.x + a.P, c.y), d += U(c.x + c.width - a.ha, c.y), d += U(c.x + c.width, c.y + a.ha), d += U(c.x + c.width, c.y + c.height - a.X), d += U(c.x + c.width - a.X, c.y + c.height), d += U(c.x + a.sa, c.y + c.height), d += U(c.x, c.y + c.height - a.sa), d += U(c.x, c.y + a.P), d += " Z"
  }
  return d
}
z.Ql = function(a) {
  this.P = this.ha = this.X = this.sa = a
};
z.Ca = function(a) {
  a || (a = new Dd);
  a.gh(this.Ra);
  a.sa = this.sa;
  a.X = this.X;
  a.P = this.P;
  a.ha = this.ha;
  return a
};
function Fd(a, b) {
  this.an = a;
  this.gr = b || Gd
}
var Gd = 0;
Fd.prototype.gr = Gd;
Fd.prototype.an = p;
function cd(a) {
  this.F = a;
  this.ou = {}
}
var Hd = 0;
cd.prototype.F = p;
cd.prototype.ou = p;
function Id(a, b, c) {
  var d = b.an + "-" + b.gr;
  if(a.ou[d]) {
    return a.ou[d]
  }
  var f = Cc(a.F, "pattern"), g = Cc(a.F, "image"), k = "img" + Hd++ + "-" + b.an + "-" + b.gr;
  f.setAttribute("id", k);
  g.setAttribute("xlink:href", b.an);
  g.setAttribute("x", 0);
  g.setAttribute("y", 0);
  g.href.baseVal = b.an;
  g.href.animVal = b.an;
  f.setAttribute("x", c.x);
  f.setAttribute("y", c.y);
  f.setAttribute("patternUnits", "userSpaceOnUse");
  1 == b.gr ? (f.setAttribute("width", c.width), f.setAttribute("height", c.height), g.setAttribute("width", c.width), g.setAttribute("height", c.height), g.setAttribute("preserveAspectRatio", "none"), g.setAttribute("image-rendering", "optimize-speed")) : 2 == b.gr ? (g.setAttribute("preserveAspectRatio", "xMinYMin meet"), f.setAttribute("width", c.width), f.setAttribute("height", c.height), g.setAttribute("width", c.width), g.setAttribute("height", c.height)) : (f.setAttribute("width", 1), f.setAttribute("height", 
  1), g.setAttribute("width", 1), g.setAttribute("height", 1));
  f.appendChild(g);
  k = "url(#" + k + ")";
  a.F.gl.appendChild(f);
  a.ou[d] = k;
  var l = new Image;
  l.src = b.an;
  b.gr == Gd && (l.onload = function() {
    var a = l.width, b = l.height;
    f.setAttribute("width", a);
    f.setAttribute("height", b);
    g.setAttribute("width", a);
    g.setAttribute("height", b)
  });
  return k
}
cd.prototype.clear = function() {
  Hd = 0;
  this.ou = {}
};
function Jd() {
}
z = Jd.prototype;
z.Ma = j;
z.isEnabled = u("Ma");
z.yg = t("Ma");
z.Ra = 0;
z.se = function() {
  return 1 == this.Ra
};
z.Xa = zb("#EEEEEE", 1);
z.Ba = u("Xa");
z.ad = 1;
z.py = p;
z.yb = p;
z.g = function(a) {
  if(this.Ma = Tb(a)) {
    if(C(a, "type")) {
      switch(Vb(F(a, "type"))) {
        case "solid":
          this.Ra = 0;
          break;
        case "gradient":
          this.Ra = 1;
          break;
        case "image":
          this.Ra = 2
      }
    }
    C(a, "opacity") && (this.ad = Sb(F(a, "opacity")));
    C(a, "color") && (this.Xa = zb(F(a, "color"), this.ad));
    if(2 == this.Ra) {
      var b, c;
      C(a, "image_url") && (b = F(a, "image_url"));
      if(C(a, "image_mode")) {
        switch(Vb(F(a, "image_mode"))) {
          case "stretch":
            c = 1;
            break;
          case "tile":
            c = Gd;
            break;
          case "fitbyproportions":
            c = 2
        }
      }
      this.py = new Fd(b, c)
    }
    1 == this.Ra && C(a, "gradient") && (this.yb = new yc(this.ad), this.yb.g(F(a, "gradient")));
    if(2 == this.Ra && (!this.py || !this.py.an)) {
      this.Ra = 0
    }
    if(1 == this.Ra && (this.yb == p || this.yb.pb == p)) {
      this.Ra = 0
    }
  }
};
function Kd(a, b, c, d, f) {
  if(!a.Ma) {
    return""
  }
  switch(a.Ra) {
    case 0:
      return"fill:" + ub(a.Xa.Ba(d)) + ";fill-opacity:" + a.ad + ";";
    case 1:
      return a.yb.Fz(c, d), "fill:" + Bc(b.cr, a.yb, f) + ";";
    case 2:
      return"fill:" + Id(b.GD, a.py, c) + ";"
  }
}
;function Ld() {
}
z = Ld.prototype;
z.Ma = j;
z.isEnabled = u("Ma");
z.Ra = 0;
z.se = function() {
  return 1 == this.Ra
};
z.Jb = u("Ra");
z.Xa = zb(p, 1);
z.ad = 1;
z.Rh = 1;
z.Va = u("Rh");
z.ct = 1;
z.Ny = 1;
z.yb = p;
z.CH = q;
z.BH = 2;
z.IK = 2;
z.g = function(a) {
  if(this.Ma = Tb(a)) {
    if(C(a, "type")) {
      switch(Vb(F(a, "type"))) {
        case "solid":
          this.Ra = 0;
          break;
        case "gradient":
          this.Ra = 1
      }
    }
    C(a, "opacity") && (this.ad = Sb(F(a, "opacity")));
    C(a, "color") && (this.Xa = zb(F(a, "color"), this.ad));
    1 == this.Ra && C(a, "gradient") && (this.yb = new yc(this.ad), this.yb.g(F(a, "gradient")));
    if(1 == this.Ra && (this.yb == p || this.yb.pb == p)) {
      this.Ra = 0
    }
    C(a, "thickness") && (this.Rh = Ob(F(a, "thickness")));
    if(C(a, "caps")) {
      switch(Vb(F(a, "caps"))) {
        case "none":
          this.ct = 0;
          break;
        case "square":
          this.ct = 2;
          break;
        default:
          this.ct = 1
      }
    }
    if(C(a, "joints")) {
      switch(Vb(F(a, "joints"))) {
        default:
        ;
        case "round":
          this.Ny = 1;
          break;
        case "miter":
          this.Ny = 0;
          break;
        case "bevel":
          this.Ny = 2
      }
    }
    C(a, "dashed") && (this.CH = Rb(F(a, "dashed")));
    C(a, "dash_length") && (this.BH = Ob(F(a, "dash_length")));
    C(a, "space_length") && (this.IK = Ob(F(a, "space_length")))
  }
};
function Md(a, b, c, d, f) {
  if(!a.Ma) {
    return""
  }
  var g = "stroke-width:" + a.Rh + ";", k;
  a: {
    switch(a.ct) {
      case 0:
        k = "butt";
        break a;
      case 1:
        k = "round";
        break a;
      case 2:
        k = "square"
    }
  }
  var l;
  a: {
    switch(a.Ny) {
      case 0:
        l = "miter";
        break a;
      case 1:
        l = "round";
        break a;
      case 2:
        l = "bevel"
    }
  }
  g = g + ("stroke-linecap:" + k + ";") + ("stroke-linejoin:" + l + ";") + ("stroke-opacity:" + a.ad + ";");
  switch(a.Ra) {
    case 0:
      g += "stroke:" + ub(a.Xa.Ba(d)) + ";";
      break;
    case 1:
      a.yb.Fz(c, d), g += "stroke:" + Bc(b.cr, a.yb, f) + ";"
  }
  a.CH && (g += "stroke-dasharray: " + a.BH + ", " + a.IK + ";");
  return g
}
;function Nd(a, b, c) {
  W.call(this, a);
  this.Hg = b;
  this.Mg = c;
  this.appendChild(b);
  this.appendChild(c)
}
A.e(Nd, W);
Nd.prototype.Hg = p;
Nd.prototype.Mg = p;
function Od() {
}
z = Od.prototype;
z.sc = p;
z.Lc = u("sc");
function Sd(a) {
  return a.sc && a.sc.isEnabled()
}
z.Hb = p;
z.mc = u("Hb");
function Td(a) {
  return a.Hb && a.Hb.isEnabled()
}
z.Pc = p;
z.De = u("Pc");
z.Ma = j;
z.isEnabled = u("Ma");
z.yg = t("Ma");
z.Ve = p;
z.kl = u("Ve");
z.g = function(a) {
  if(this.Ma = Tb(a)) {
    if(Ub(a, "fill") && (this.Hb = new Jd, this.Hb.g(F(a, "fill")), this.Hb.isEnabled() || (this.Hb = q)), Ub(a, "border") && (this.sc = new Ld, this.sc.g(F(a, "border")), this.sc.isEnabled() || (this.sc = q)), Ub(a, "hatch_fill") && (this.Pc = new Fc, this.Pc.g(F(a, "hatch_fill")), this.Pc.isEnabled() || (this.Pc = q)), Ub(a, "effects")) {
      this.Ve = new Rc, this.Ve.g(F(a, "effects")), this.Ve.isEnabled() || (this.Ve = q)
    }
  }
};
function Ud() {
}
A.e(Ud, Od);
z = Ud.prototype;
z.D = p;
z.fb = u("D");
z.p = function(a) {
  if(!this.Ma) {
    return p
  }
  var b = this.createElement(a), c = this.createElement(a);
  return this.D = new Nd(a, b, c)
};
z.qa = function(a, b, c) {
  var d = a.r(), f;
  this.Ve && this.Ve.isEnabled() && (f = Tc(d.gk, this.Ve));
  d = this.yh(d, b);
  this.qC(a.Hg, d, a, b, c, f);
  Vd(this, a.Mg, d, a, f)
};
z.qC = function(a, b, c, d, f, g) {
  var c = c.r(), k = "", k = this.Hb && this.Hb.isEnabled() ? k + Kd(this.Hb, c, d, f) : k + md(), k = this.sc && this.sc.isEnabled() ? k + Md(this.sc, c, d, f) : k + nd();
  Wd(a, b, k, g)
};
function Vd(a, b, c, d, f) {
  var g = "", g = a.Pc && a.Pc.isEnabled() ? Hc(a.Pc, d.r()) : od();
  Wd(b, c, g, f)
}
function Wd(a, b, c, d) {
  d ? a.setAttribute("filter", d) : a.removeAttribute("filter");
  a.setAttribute("d", b);
  a.setAttribute("style", c)
}
z.Wa = function(a, b) {
  var c = this.yh(a.r(), b);
  a.Hg.setAttribute("d", c);
  a.Mg.setAttribute("d", c)
};
function Xd() {
}
A.e(Xd, Ud);
z = Xd.prototype;
z.so = p;
z.Oa = p;
function Yd(a, b) {
  a.Oa && Cd(a.Oa, b)
}
z.g = function(a) {
  Xd.f.g.call(this, a);
  if(this.isEnabled() && (C(a, "corners") && (this.so = new Dd, this.so.g(F(a, "corners"))), C(a, "inside_margin"))) {
    this.Oa = new Bd, this.Oa.g(F(a, "inside_margin"))
  }
};
z.createElement = function(a) {
  return a.ja()
};
z.yh = function(a, b) {
  if(this.so) {
    return Ed(this.so, a, b)
  }
  var c = S(b.x, b.y), c = c + U(b.x + b.width, b.y), c = c + U(b.x + b.width, b.y + b.height), c = c + U(b.x, b.y + b.height), c = c + U(b.x, b.y);
  return c + " Z"
};
var Zd = {u:function(a) {
  return a.getTime().toString()
}, d:function(a) {
  return a.getUTCDate().toString()
}, dd:function(a) {
  a = a.getUTCDate().toString();
  return 1 == a.length ? "0" + a : a
}, ddd:function(a, b) {
  return b.pA[a.getUTCDay()]
}, dddd:function(a, b) {
  return b.XA[a.getUTCDay()]
}, M:function(a) {
  return(a.getUTCMonth() + 1).toString()
}, MM:function(a) {
  a = (a.getUTCMonth() + 1).toString();
  return 1 == a.length ? "0" + a : a
}, MMM:function(a, b) {
  return b.oD(a.getUTCMonth())
}, MMMM:function(a, b) {
  return b.eD(a.getUTCMonth())
}, y:function(a) {
  return a.getUTCFullYear().toString().substr(3, 1)
}, yy:function(a) {
  return a.getUTCFullYear().toString().substr(2, 2)
}, yyyy:function(a) {
  return a.getUTCFullYear().toString()
}, h:function(a) {
  a = a.getUTCHours();
  return 0 == a ? "12" : 12 < a ? (a - 12).toString() : a.toString()
}, hh:function(a) {
  a = a.getUTCHours();
  if(0 == a) {
    return"12"
  }
  if(12 < a) {
    return a = (a - 12).toString(), 1 == a.length ? "0" + a : a
  }
  a = a.toString();
  return 1 == a.length ? "0" + a : a
}, H:function(a) {
  return a.getUTCHours().toString()
}, HH:function(a) {
  a = a.getUTCHours().toString();
  return 1 == a.length ? "0" + a : a
}, m:function(a) {
  return a.getUTCMinutes().toString()
}, mm:function(a) {
  a = a.getUTCMinutes().toString();
  return 1 == a.length ? "0" + a : a
}, s:function(a) {
  return a.getUTCSeconds().toString()
}, ss:function(a) {
  a = a.getUTCSeconds().toString();
  return 1 == a.length ? "0" + a : a
}, t:function(a, b) {
  return 12 <= a.getUTCHours() ? b.oA : b.nA
}, tt:function(a, b) {
  return 12 <= a.getUTCHours() ? b.Ez : b.yw
}, z:function(a) {
  a = -a.getTimezoneOffset() / 60;
  return 0 <= a ? "+" + a.toString() : a.toString()
}, zz:function(a) {
  var a = -a.getTimezoneOffset() / 60, b = Math.abs(a).toString(), b = 1 == b.length ? "0" + b : b;
  return 0 > a ? "-" + b : "+" + b
}, zzz:function(a) {
  var b = (-a.getTimezoneOffset() % 60).toString(), a = -a.getTimezoneOffset() / 60, b = Math.abs(a).toString() + ":" + (1 == b.length ? "0" + b : b), b = 4 == b.length ? "0" + b : b;
  return 0 > a ? "-" + b : "+" + b
}};
function $d(a, b, c) {
  this.Pn = a;
  this.$y = b == h ? 0 : b;
  this.JC = c === h ? "" : c
}
$d.prototype.Pn = p;
$d.prototype.$y = p;
$d.prototype.JC = p;
function ae(a, b) {
  0 < a.$y && b.length > a.$y && (b = b.substr(0, a.$y), a.JC != p && (b += a.JC));
  return b
}
function be(a, b, c, d, f) {
  $d.call(this, a, b, c);
  this.QC = d ? d : [];
  this.RC = f ? f : [""]
}
A.e(be, $d);
be.prototype.ta = function(a, b) {
  for(var c = Number(a.Na(this.Pn)), c = new Date(isNaN(c) ? 0 : c), d = "", f = this.QC.length, g = 0;g < f;g++) {
    d += this.RC[g] + this.QC[g](c, b)
  }
  d += this.RC[f];
  return ae(this, d)
};
be.prototype.QC = p;
be.prototype.RC = p;
function ce(a, b, c) {
  $d.call(this, a, b, c)
}
A.e(ce, $d);
z = ce.prototype;
z.YK = j;
z.UK = j;
z.wE = 1;
z.Ih = 2;
z.On = ",";
z.to = ".";
function de(a, b, c) {
  var d = Number(Math.round(b * Math.pow(10, a.Ih)) / Math.pow(10, a.Ih)).toString(), f = d.indexOf(".");
  -1 == f ? (b = d, d = "") : (b = d.substr(0, f), d = d.substr(f + 1, d.length - 1));
  if(a.UK && d.length < a.Ih) {
    for(;d.length < a.Ih;) {
      d += "0"
    }
  }
  if(1 < a.wE) {
    for(;b.length < a.wE;) {
      b = "0" + b
    }
  }
  if(a.On && 3 < b.length) {
    for(f = b.length;3 < f;f -= 3) {
      b = b.substr(0, f - 3) + a.On + b.substr(f - 3, b.length - 1)
    }
  }
  d = "" != d ? b + a.to + d : b;
  return ae(a, c ? a.YK ? "-" + d : "(" + d + ")" : d)
}
function ee(a, b, c, d) {
  $d.call(this, a, b, c);
  this.La = d
}
A.e(ee, ce);
ee.prototype.La = p;
ee.prototype.ta = function(a) {
  var b = a.Na(this.Pn), a = Number(b);
  if(isNaN(b)) {
    return b
  }
  (b = 0 > a) && (a = -a);
  for(var c = this.La.length - 1, d = Math.floor(a / Number(this.La[c].aq));0 == d && 0 < c;) {
    d = Math.floor(a / Number(this.La[--c].aq))
  }
  return de(this, a / Number(this.La[c].aq), b) + this.La[c].dG
};
function fe(a, b, c) {
  $d.call(this, a, b, c)
}
A.e(fe, ce);
fe.prototype.ta = function(a) {
  var b = a.Na(this.Pn), a = Number(b);
  if(isNaN(b)) {
    return b
  }
  b = 0 > a;
  return de(this, b ? -a : a, b)
};
function ge(a, b, c, d) {
  $d.call(this, a, b, c);
  this.La = d
}
A.e(ge, ce);
ge.prototype.La = p;
ge.prototype.ta = function(a) {
  var b = a.Na(this.Pn), a = Number(b);
  if(isNaN(b)) {
    return b
  }
  b = 0 > a;
  return de(this, (b ? -a : a) / Number(this.La.scale), b) + this.La.dG
};
function he(a, b, c) {
  $d.call(this, a, b, c)
}
A.e(he, $d);
he.prototype.ta = function(a) {
  a = a.Na(this.Pn);
  a = a == h ? "" : a.toString();
  return ae(this, a)
};
he.prototype.Pn = p;
function ie(a) {
  this.LK = a
}
ie.prototype.ta = u("LK");
ie.prototype.LK = p;
function je(a) {
  this.Ut = a;
  this.Mf = []
}
z = je.prototype;
z.ta = function(a, b) {
  this.lG = a;
  this.bC = b;
  if(0 == this.Mf.length) {
    for(var c = this.Ut, d = c.length, f = 0, g = "", k, l, n, m, o = q, v = 0;v < d;v++) {
      var w = c.charAt(v);
      switch(f) {
        case 0:
          "{" == w ? f = 1 : g += w;
          break;
        case 1:
          "%" == w ? (0 < g.length && this.Mf.push(new ie(g)), g = "%", f = 2) : (g += "{" + w, f = 0);
          break;
        case 2:
          "}" == w ? 1 < g.length ? (l = g, g = "", f = 3) : (g = "{%}", f = 0) : g += w;
          break;
        case 3:
          "{" == w ? f = 4 : (ke(this, l, {}), g = w, f = 0);
          break;
        case 4:
          "%" == w ? (ke(this, l, {}), g = "%", f = 2) : (k = "{" + w, g = w, n = "", m = {}, o = "\\" == w, f = 5);
          break;
        case 5:
          k += w;
          o ? (g += w, o = q) : "\\" == w ? o = j : ":" == w ? (n = g, g = "", f = 6) : "}" == w ? (n = g, m[n] = "", ke(this, l, m), k = g = n = "", f = 0) : g += w;
          break;
        case 6:
          k += w, o ? (g += w, o = q) : "\\" == w ? o = j : "," == w ? (f = g, m[n] = f, n = g = "", f = 5) : "}" == w ? (f = g, m[n] = f, ke(this, l, m), k = g = n = "", f = 0) : g += w
      }
    }
    switch(f) {
      case 0:
        0 < g.length && this.Mf.push(new ie(g));
        break;
      case 1:
        this.Mf.push(new ie("{"));
        break;
      case 2:
        this.Mf.push(new ie("{" + g));
        break;
      case 3:
        ke(this, l, {});
        break;
      case 4:
        ke(this, l, {});
        this.Mf.push(new ie("{"));
        break;
      case 5:
      ;
      case 6:
        this.Mf.push(new ie(k))
    }
  }
  c = "";
  d = this.Mf.length;
  for(k = 0;k < d;k++) {
    c += this.Mf[k].ta(this.lG, this.bC)
  }
  return c
};
z.Ut = p;
z.lG = p;
z.bC = p;
z.Mf = p;
function ke(a, b, c) {
  var d = a.lG.Pa(b), f = [];
  if(c.enabled != h && "true" != c.enabled.toLowerCase()) {
    a.Mf.push(new he(b))
  }else {
    switch(d) {
      case 1:
        a.Mf.push(new he(b, c.maxChar, c.maxCharFinalChars));
        break;
      case 2:
        c.scale != h && (f = le(c.scale));
        me(a, b, c, f);
        break;
      case 3:
        if(c.scale != h && (f = le(c.scale)), 0 < f.length) {
          me(a, b, c, f)
        }else {
          for(var d = c.dateTimeFormat == h ? a.bC.Zy : c.dateTimeFormat, f = d.length, g = [], k = [], l = 0, n = "", m = "", o = 0;o < f;o++) {
            var v = d.charAt(o);
            switch(l) {
              case 0:
                "%" == v ? (k.push(n), n = "", l = 1) : n += v;
                break;
              case 1:
                "%" == v ? k[k.length - 1] += "%" : (n = m = v, l = 2);
                break;
              case 2:
                v == m ? n += v : (l = Zd[n], l == h ? k[k.length - 1] += "%" + n : g.push(l), n = v, l = 0)
            }
          }
          switch(l) {
            case 0:
              k.push(n);
              break;
            case 1:
              k.push("%");
              break;
            case 2:
              l = Zd[n], l == h ? k[k.length - 1] += "%" + n : (g.push(l), k.push(""))
          }
          a.Mf.push(new be(b, c.maxChar, c.maxCharFinalChars, g, k))
        }
    }
  }
}
function me(a, b, c, d) {
  var f = p, b = f = 0 == d.length ? new fe(b, c.maxChar, c.maxCharFinalChars) : 1 == d.length ? new ge(b, c.maxChar, c.maxCharFinalChars, d[0]) : new ee(b, c.maxChar, c.maxCharFinalChars, d), d = c.useNegativeSign, g = c.trailingZeros, k = c.leadingZeros, l = c.numDecimals, n = c.thousandsSeparator, c = c.decimalSeparator;
  d !== h && (b.YK = "true" == d.toLowerCase());
  g !== h && (b.UK = "true" == g.toLowerCase());
  k !== h && (b.wE = Number(k));
  l !== h && (b.Ih = Number(l));
  n !== h && (b.On = n);
  c !== h && (b.to = c);
  a.Mf.push(f)
}
function le(a) {
  for(var b = a.length, c = j, d = 0, f = [], g = "", k = 0;k < b;k++) {
    var l = a.charAt(k);
    "(" != l && (")" == l ? (c ? (g = Number(g), f[d] = {scale:g, dG:"", aq:g}, 0 < d && (f[d].aq *= f[d - 1].aq)) : f[d].dG = g, g = "", d++) : "|" == l ? (c = q, d = 0, g = "") : g += l)
  }
  return f
}
function ne() {
  this.os = {}
}
ne.prototype.os = p;
ne.prototype.get = function(a) {
  return oe(this, a) ? this.os[a] : ""
};
function oe(a, b) {
  return a.os[b] != h
}
ne.prototype.add = function(a, b) {
  this.os[a] = b
};
function pe(a, b, c) {
  a.os[b] = oe(a, b) ? a.os[b] + c : c
}
function qe(a) {
  return vb(a)
}
function re(a) {
  return a && (1 == a.indexOf("%") || 0 == a.indexOf("%"))
}
function se(a, b) {
  return 0 == a.indexOf("%DataPlot") || b.ql(a)
}
function te(a, b) {
  return b && (0 == a.indexOf("%Threshold") || b.pr(a))
}
function ue(a) {
  return 0 == a.indexOf("%Category")
}
function ve(a) {
  return a.slice(0, 1) + a.slice(2, a.length)
}
;function dd(a) {
  this.F = a;
  this.Sc = Cc(this.F, "text");
  this.Sc.setAttribute("visibility", "hidden");
  this.Sc.setAttribute("x", "0");
  this.Sc.setAttribute("y", "0");
  this.GA = this.F.Lk.ownerDocument.createTextNode("");
  this.Sc.appendChild(this.GA);
  a.Jr.appendChild(this.Sc)
}
dd.prototype.F = p;
dd.prototype.Sc = p;
dd.prototype.GA = p;
dd.prototype.measureText = function(a, b) {
  we(b, this.Sc);
  this.GA.nodeValue = a;
  var c = this.Sc.getBBox();
  this.GA.nodeValue = "";
  var d = this.Sc;
  d.removeAttribute("font-family");
  d.removeAttribute("font-size");
  d.removeAttribute("font-weight");
  d.removeAttribute("font-style");
  d.removeAttribute("font-decoration");
  d.removeAttribute("fill");
  d.removeAttribute("stroke");
  return new P(c.x, c.y, c.width, c.height)
};
function xe(a, b, c) {
  W.call(this, a);
  c && (this.Cc = c, this.ia(c));
  b && (this.QK = b, this.I.appendChild(b))
}
A.e(xe, W);
xe.prototype.QK = p;
xe.prototype.gu = u("QK");
xe.prototype.Cc = p;
xe.prototype.Lx = u("Cc");
function ye(a, b) {
  W.call(this, a);
  this.kf = b;
  this.ia(b)
}
A.e(ye, W);
ye.prototype.gu = function() {
  return this.kf.gu()
};
ye.prototype.Lx = function() {
  return this.kf.Lx()
};
function ze() {
  Ae(this)
}
z = ze.prototype;
z.uH = function() {
  return new Zc
};
z.kC = j;
z.HC = p;
z.ke = p;
z.Mp = t("ke");
z.AB = p;
z.sE = p;
z.rG = p;
z.Xa = p;
z.Tp = p;
z.lF = p;
function Ae(a) {
  a.lF = q;
  a.Tp = 0;
  a.Xa = zb(p, 1);
  a.rG = q;
  a.sE = q;
  a.AB = q;
  a.ke = 10;
  a.HC = "Arial";
  a.Bb = a.uH();
  Be(a, 0);
  a.Y = p
}
z.Qh = p;
z.k = p;
z.n = u("k");
z.nf = p;
z.Ig = function() {
  var a = this.nf.Ca();
  a.x = 1;
  a.y = 1;
  a.width -= 2;
  a.height -= 2;
  return a
};
z.Bb = p;
function Be(a, b) {
  a.Bb.bc = -b;
  a.Bb.Ae()
}
z.Ma = j;
z.isEnabled = u("Ma");
z.yg = t("Ma");
z.FA = p;
z.Y = p;
z.md = u("Y");
z.g = function(a) {
  if(Tb(a)) {
    C(a, "background") && (this.Y = new Xd, this.Y.g(F(a, "background")));
    Be(this, C(a, "rotation") ? M(a, "rotation") : 0);
    var b = C(a, "text_align") ? "text_align" : "multi_line_align";
    if(C(a, b)) {
      switch(Cb(F(a, b))) {
        case "left":
          this.Tp = 0;
          break;
        case "center":
          this.Tp = 2;
          break;
        case "right":
          this.Tp = 1
      }
    }
    if(C(a, "font") && (a = F(a, "font"), C(a, "family") && (this.HC = Cb(F(a, "family"))), C(a, "size") && (this.ke = Ob(F(a, "size"))), C(a, "bold") && (this.AB = Rb(F(a, "bold"))), C(a, "italic") && (this.sE = Rb(F(a, "italic"))), C(a, "underline") && (this.rG = Rb(F(a, "underline"))), C(a, "color") && (this.Xa = zb(F(a, "color"), h)), C(a, "render_as_html"))) {
      this.lF = K(a, "render_as_html")
    }
  }else {
    this.Ma = q
  }
};
z.uc = function(a, b) {
  this.lF && (b = b.replace(/<.*?>/gi, ""));
  for(var c = b, d = [], f = c.length, g = "", k = 0;k < f;k++) {
    var l = c.charCodeAt(k);
    13 == l || 10 == l ? (d.push(Hb(g)), g = "") : k == f - 1 ? (g += c.charAt(k), d.push(Hb(g)), g = "") : g += c.charAt(k)
  }
  c = this.FA = d;
  d = new P(0, 0, 0, 0);
  for(f = 0;f < c.length;f++) {
    g = a.PK.measureText(c[f], this), d.width = Math.max(d.width, g.width), d.height += g.height
  }
  this.Qh = d;
  this.Qh.x += 1;
  this.Qh.y += 1;
  c = this.Qh.Ca();
  this.Y && this.Y.isEnabled() && this.Y.Oa && (c = this.Qh, d = this.Qh.Ca(), c.x += this.Y.Oa.tb() + 1, c.y += this.Y.Oa.rb() + 1, d.x = c.x, d.y = c.y, d.width += this.Y.Oa.tb() + this.Y.Oa.Ja() + 4, d.height += this.Y.Oa.rb() + this.Y.Oa.ra() + 4, c = d);
  this.nf = c;
  c = this.Bb;
  d = this.nf.Ca();
  !isNaN(c.bc) && 0 != c.bc && (f = d.width, g = d.height, d.width = f * c.wf + g * c.xf, d.height = f * c.xf + g * c.wf);
  this.k = d
};
function we(a, b, c) {
  b.setAttribute("font-family", a.HC);
  b.setAttribute("font-size", a.ke);
  a.AB && b.setAttribute("font-weight", "bold");
  a.sE && b.setAttribute("font-style", "italic");
  a.rG && b.setAttribute("font-decoration", "underline");
  b.setAttribute("fill", ub(a.Xa.Ba(c)));
  b.setAttribute("stroke", "none")
}
z.jc = function(a, b, c) {
  var d = Cc(a, "text"), f;
  this.Y && this.Y.isEnabled() && (f = this.Y.p(a), this.Y.qa(f, this.Ig(), b));
  f || (f = new Nd(a, a.ja(), a.ja()));
  f = new xe(a, d, f);
  this.kC && d.setAttribute("pointer-events", "none");
  (c || c == p || c == h) && Ce(this, a, d, b);
  this.aI(f);
  return new ye(a, f)
};
z.aI = function(a) {
  a.Wl(this.Bb.bc);
  var b = new O, c = this.Bb, d = this.nf, f = 0, g = 0, k = 0 <= c.sin, l = 0 <= c.cos;
  switch(k && l ? 0 : k && !l ? 1 : !l ? 2 : 3) {
    case 0:
      f = d.height * c.sin;
      break;
    case 1:
      f = d.width * c.wf + d.height * c.xf;
      g = d.height * c.wf;
      break;
    case 2:
      f = d.width * c.wf;
      g = d.width * c.xf + d.height * c.wf;
      break;
    case 3:
      f = 0, g = d.width * c.xf
  }
  b.x += f;
  b.y += g;
  a.qb(b.x, b.y)
};
z.update = function(a, b, c, d, f) {
  Ce(this, a, b.gu(), c, d, f);
  this.bw(b.Lx(), this.Ig(), c)
};
function Ce(a, b, c, d, f, g) {
  we(a, c, d);
  c.setAttribute("x", a.Qh.x);
  c.setAttribute("y", a.Qh.y);
  if(g == h || g == p) {
    g = -1
  }
  if(g) {
    switch(g) {
      case 2:
        c.setAttribute("text-anchor", "middle");
        break;
      case 1:
        c.setAttribute("text-anchor", "end")
    }
  }
  if(!f) {
    for(;0 < c.childNodes.length;) {
      c.removeChild(c.childNodes[0])
    }
    for(d = 0;d < a.FA.length;d++) {
      f = Cc(b, "tspan");
      f.appendChild(b.Lk.ownerDocument.createTextNode(a.FA[d]));
      switch(g) {
        case 2:
          f.setAttribute("x", a.Qh.width / 2);
          break;
        case 1:
          f.setAttribute("x", a.Qh.width);
          break;
        default:
          f.setAttribute("x", a.Qh.x)
      }
      f.setAttribute("dy", "1em");
      c.appendChild(f)
    }
  }
}
z.bw = function(a, b, c) {
  this.Y && this.Y.isEnabled() && this.Y.qa(a, b, c)
};
function De() {
  ze.apply(this)
}
A.e(De, ze);
z = De.prototype;
z.D = p;
z.fb = u("D");
z.yc = p;
z.Bh = u("yc");
z.Ia = 5;
z.ga = u("Ia");
z.za = 1;
z.g = function(a) {
  De.f.g.call(this, a);
  var b = C(a, "text") ? "text" : "format";
  C(a, b) && (this.yc = new je(J(a, b)));
  if(C(a, "align")) {
    switch(N(a, "align")) {
      case "near":
      ;
      case "left":
      ;
      case "top":
        this.za = 0;
        break;
      case "far":
      ;
      case "right":
      ;
      case "bottom":
        this.za = 2;
        break;
      case "center":
        this.za = 1
    }
  }
  C(a, "padding") && (this.Ia = M(a, "padding"))
};
z.jc = function(a) {
  return this.D = De.f.jc.call(this, a)
};
z.Wa = s();
function Ee(a, b) {
  ze.apply(this);
  this.IJ = a;
  this.iI = b
}
A.e(Ee, De);
z = Ee.prototype;
z.Pf = p;
z.IJ = p;
z.iI = p;
z.g = function(a) {
  Ee.f.g.call(this, a);
  this.Ma && C(a, "actions") && (this.Pf = new Fe(this.IJ, this.iI), this.Pf.g(F(a, "actions")))
};
z.jc = function(a) {
  this.Pf && (this.kC = q);
  Ee.f.jc.call(this, a);
  this.Pf && (this.D.I.setAttribute("cursor", "pointer"), this.pu(this.D));
  return this.D
};
z.pu = function(a) {
  A.G.nb(a.I, ka.cB, this.Jl, q, this)
};
z.Jl = function() {
  this.Pf && this.Pf.execute()
};
function Ge(a) {
  ha.call(this, a)
}
A.e(Ge, ha);
function He(a, b) {
  ha.call(this, a);
  this.PA = b
}
A.e(He, ha);
He.prototype.PA = p;
function Ie(a, b, c, d) {
  ha.call(this, a);
  this.j = b;
  this.QJ = c;
  this.RJ = d
}
A.e(Ie, ha);
Ie.prototype.Jb = u("type");
Ie.prototype.j = p;
Ie.prototype.QJ = p;
Ie.prototype.RJ = p;
function Je(a, b) {
  ha.call(this, a);
  this.ba = b
}
A.e(Je, ha);
Je.prototype.Jb = u("type");
Je.prototype.ba = p;
Je.prototype.Fe = function() {
  return this.ba.length
};
Je.prototype.Ga = function(a) {
  return this.ba[a]
};
function Ke(a, b, c) {
  ha.call(this, a);
  this.errorCode = b;
  this.errorMessage = c
}
A.e(Ke, ha);
Ke.prototype.errorCode = p;
Ke.prototype.errorMessage = p;
Ke.prototype.toString = function() {
  return"Error code: " + this.errorCode + " error message: " + this.errorMessage
};
Ke.prototype.toString = function() {
  return"Error message: " + this.errorMessage
};
function Le(a) {
  e(new Me("anychartError", 22, "Feature is not supported.", a))
}
function Me(a, b, c, d) {
  Ke.call(this, a, b, c);
  this.featureName = d
}
A.e(Me, Ke);
Me.prototype.featureName = p;
Me.prototype.toString = function() {
  return Me.f.toString.call(this) + " Feature: " + this.featureName + "."
};
function Ne() {
}
z = Ne.prototype;
z.j = p;
z.I = p;
z.$a = p;
z.p = function(a, b, c) {
  this.j = a;
  this.$a = b;
  this.I = c ? c.call(b) : this.createElement()
};
z.update = function(a, b) {
  a && (this.vf(a), this.ud(a));
  b === j && this.Ta()
};
z.vf = function() {
  A.xa()
};
z.Ta = function() {
  A.xa()
};
z.ud = s();
function Oe() {
}
A.e(Oe, Ne);
Oe.prototype.vf = function(a) {
  var b = this.j.r(), c = "", d = this.gD(), f = this.n(), c = c + Md(this.qD(a), b, f, d), c = c + md();
  this.I.setAttribute("style", c)
};
Oe.prototype.n = function() {
  return this.j.n()
};
Oe.prototype.gD = function() {
  return this.j.Ba().Ba()
};
function Pe() {
}
A.e(Pe, Ne);
Pe.prototype.vf = function(a) {
  var b = this.j.r(), c = "", d = this.gD(), f = this.n();
  this.Ie(a) ? (c += Kd(this.Xq(a), b, f, d), c += nd()) : c += od();
  this.I.setAttribute("style", c)
};
Pe.prototype.n = function() {
  return this.j.n()
};
Pe.prototype.gD = function() {
  return this.j.Ba().Ba()
};
Pe.prototype.Ie = x(j);
function Qe() {
}
A.e(Qe, Ne);
z = Qe.prototype;
z.Yh = p;
z.createElement = function() {
  return this.$a.createElement()
};
z.Ta = function() {
  this.$a.Ta(this.I)
};
z.oh = function(a) {
  var b = this.j.r();
  this.Yh && (Ec(b.cr, this.Yh), this.Yh = "");
  this.vf(a)
};
z.vf = function(a) {
  var b = this.j.r(), c = "", d = this.j.Ba().Ba(), f = this.j.ca().n();
  this.Ie(a) ? (c += Kd(this.mc(a), b, f, d, j), this.Yh = c += nd()) : c = od();
  this.I.setAttribute("style", c)
};
z.ud = s();
function Re() {
}
Re.prototype.j = p;
Re.prototype.p = function(a) {
  this.j = a;
  this.Yc();
  this.ff()
};
function Se(a, b) {
  this.K = a;
  b != h && (this.WF = b)
}
z = Se.prototype;
z.K = p;
z.ka = u("K");
z.WF = 0;
z.g = function(a) {
  if(!a) {
    return p
  }
  a = Zb(a);
  if(C(a, "color")) {
    var b = F(a, "color"), c;
    for(c in a) {
      C(a[c], "color") && H(a[c], "color", N(a[c], "color").split("%color").join(b))
    }
  }
  return a
};
z.copy = function(a) {
  a || e(Error("Unknown target"));
  a.K = this.K;
  a.WF = this.WF;
  return a
};
function Te(a, b) {
  Se.call(this, a, b)
}
A.e(Te, Se);
Te.prototype.Ve = p;
Te.prototype.kl = u("Ve");
Te.prototype.pp = function() {
  return Boolean(this.Ve && this.Ve.isEnabled())
};
Te.prototype.g = function(a) {
  a = Te.f.g.call(this, a);
  if(!a) {
    return p
  }
  Ub(a, "effects") && (this.Ve = new Rc, this.Ve.g(F(a, "effects")));
  return a
};
function $() {
  var a = this.cc();
  this.wa = new a(this, 0);
  this.dc = new a(this, 1);
  this.kc = new a(this, 2);
  this.Zb = new a(this, 3);
  this.gc = new a(this, 4);
  this.nc = new a(this, 5)
}
z = $.prototype;
z.wa = p;
z.dh = t("wa");
z.dc = p;
z.ah = t("dc");
z.kc = p;
z.Zb = p;
z.gc = p;
z.nc = p;
z.g = function(a) {
  if(a) {
    if(a = Zb(a), C(a, "states")) {
      var b = F(a, "states");
      this.wa.g(this.Fh(a, F(b, "normal")));
      this.dc.g(this.Fh(a, F(b, "hover")));
      this.kc.g(this.Fh(a, F(b, "pushed")));
      this.Zb.g(this.Fh(a, F(b, "selected_normal")));
      this.gc.g(this.Fh(a, F(b, "selected_hover")));
      this.nc.g(this.Fh(a, F(b, "missing")))
    }else {
      this.wa.g(a), this.dc.g(a), this.kc.g(a), this.Zb.g(a), this.gc.g(a), this.nc.g(a)
    }
  }
};
z.Fh = function(a, b) {
  return!b ? a : $b(a, b, p, "states")
};
z.p = function(a) {
  var b = this.lc(a);
  b.p(a);
  return b
};
z.copy = function(a) {
  a || (a = new $);
  a.dh(this.wa.copy());
  a.ah(this.dc.copy());
  var b = this.kc.copy();
  a.kc = b;
  b = this.Zb.copy();
  a.Zb = b;
  b = this.gc.copy();
  a.gc = b;
  b = this.nc.copy();
  a.nc = b;
  return a
};
function Ue(a) {
  this.Ki = a;
  this.Ti = {}
}
Ue.prototype.Ki = p;
Ue.prototype.Ti = p;
Ue.prototype.ka = function(a, b) {
  return!this.Ti[a] || !this.Ti[a][b] ? p : this.Ti[a][b]
};
function Ve(a, b, c, d, f) {
  var g = I(a.Ki, b), k = [], l;
  c && k.push(c);
  d && (d = Cb(d), l = fc(g, d));
  We(k, l, g);
  return Xe(a, k, b, f)
}
function ef(a, b, c) {
  if(!b || a.ka(b, c)) {
    return p
  }
  var b = Cb(b), c = Cb(c), d = I(a.Ki, b), c = fc(d, c), f = [];
  We(f, c, d);
  return Xe(a, f, b)
}
function We(a, b, c) {
  if(b) {
    for(a.push(Zb(b));"anychart_default" != J(b, "name");) {
      b = !C(b, "parent") || !fc(c, J(b, "parent")) ? fc(c, "anychart_default") : fc(c, F(b, "parent")), a.push(Zb(b))
    }
  }else {
    a.push(fc(c, "anychart_default"))
  }
}
function Xe(a, b, c, d) {
  var f = b.length;
  if(d == h || d == p) {
    d = j
  }
  if(d) {
    for(d = 0;d < b.length;d++) {
      var g = b, k = d, l;
      l = a;
      var n = b[d], m = c ? c : J(n, "#name#");
      if(m && -1 != ff.indexOf(m)) {
        l = n
      }else {
        var o = {};
        m && (o["#name#"] = m);
        o.states = {"#name#":"states", "#children#":[]};
        gf(o.states, "normal");
        gf(o.states, "hover");
        gf(o.states, "pushed");
        gf(o.states, "selected_normal");
        gf(o.states, "selected_hover");
        gf(o.states, "missing");
        ac(n, o, o);
        o.states.normal = l.Fh(n, "normal");
        o.states.hover = l.Fh(n, "hover");
        o.states.pushed = l.Fh(n, "pushed");
        o.states.selected_normal = l.Fh(n, "selected_normal");
        o.states.selected_hover = l.Fh(n, "selected_hover");
        o.states.missing = l.Fh(n, "missing");
        l = o
      }
      g[k] = l
    }
  }
  a = b[f - 1];
  a.inherits = C(a, "name") ? Cb(F(a, "name")) : "";
  for(d = f - 1;0 < d;d--) {
    a = $b(a, b[d - 1]), C(b[d - 1], "name") && (a.inherits += "," + Cb(F(b[d - 1], "name")))
  }
  return a
}
Ue.prototype.Fh = function(a, b) {
  a.states || (a.states = {});
  a.states[b] || (a.states[b] = {});
  return $b(a, a.states[b], b, "states")
};
function gf(a, b) {
  a[b] = {"#name#":b, "#children#":[]}
}
var ff = "animation_style,animation,line_axis_marker_style,range_axis_marker_style,custom_label_style,trendline_style,color_range_style,tooltip_style".split(",");
Ue.prototype.clear = function() {
  this.Ki = {};
  this.Ti = {}
};
function hf() {
}
z = hf.prototype;
z.ah = s();
z.dh = s();
z.Pd = s();
z.Kj = s();
z.Bk = s();
z.Nk = s();
z.Ok = s();
z.mk = s();
z.xK = s();
z.Lj = s();
z.Mj = s();
z.Ck = s();
z.Pk = s();
z.Ik = s();
z.nk = s();
z.lk = function() {
  e(new Ke("anychartError", 1, "This method can be used in axes plot only."))
};
function jf() {
  this.Ge();
  this.fF = this.PD = q
}
A.e(jf, hf);
z = jf.prototype;
z.N = p;
z.cA = t("N");
z.rm = p;
z.yK = t("rm");
z.wp = p;
z.Zr = t("wp");
z.Sj = p;
z.fG = x(j);
z.uF = function(a) {
  this.Sj = Zb(a)
};
z.Y = p;
z.Cc = p;
z.uB = p;
z.Ig = u("k");
z.k = p;
z.n = u("k");
z.Rc = t("k");
z.Xt = u("k");
z.Sf = p;
z.zK = t("Sf");
z.Mk = p;
z.dy = u("Mk");
z.uk = p;
z.Vx = u("uk");
z.Yy = p;
z.yB = p;
z.xt = p;
z.vw = p;
z.g = function(a, b) {
  if(a == p) {
    return p
  }
  C(a, "chart_settings") && this.fC(F(a, "chart_settings"), b);
  C(a, "data_plot_settings") && this.Dq(F(a, "data_plot_settings"));
  this.ux(a, b);
  this.yk(a);
  return a
};
z.fC = function(a) {
  C(a, "data_plot_background") && (this.Y = new Xd, this.Y.g(F(a, "data_plot_background")))
};
z.Dq = s();
function kf(a, b) {
  if(C(b, "interactivity")) {
    var c = F(b, "interactivity");
    C(c, "allow_multiple_select") && (a.jm = K(c, "allow_multiple_select"));
    C(c, "select_rectangle") && (c = F(c, "select_rectangle"), C(c, "enabled") && (a.xw = K(c, "enabled")))
  }
}
z.ux = function(a) {
  if(C(a, "data") && (a = F(a, "data"), C(a, "attributes"))) {
    this.jd = {};
    for(var a = I(F(a, "attributes"), "attribute"), b = a.length, c = 0;c < b;c++) {
      var d = a[c];
      if(C(d, "name")) {
        var f;
        C(d, "custom_attribute_value") ? f = F(d, "custom_attribute_value") : C(d, "value") && (f = F(d, "value"));
        f && H(this.jd, "%" + F(d, "name"), f)
      }
    }
  }
};
z.PD = p;
z.F = p;
z.r = u("F");
z.D = p;
z.fb = u("D");
z.tl = function(a) {
  this.F = a;
  this.uB = new W(this.F);
  this.yB = new W(this.F);
  this.xt = new W(this.F);
  this.vw = new W(this.F);
  this.uk = new W(this.F);
  this.Yy = new W(this.F)
};
z.p = function(a, b, c) {
  this.D = new W(this.F);
  this.Mk = b;
  this.k = a;
  this.D.Xd(this.k.x);
  this.D.gd(this.k.y);
  c && this.Y && this.Y.isEnabled() && (this.Cc = this.Y.p(this.F), this.Cc.qb(0.5, 0.5), this.uB.ia(this.Cc));
  this.D.ia(this.uB);
  this.ry();
  this.D.ia(this.yB);
  this.D.ia(this.xt);
  this.D.ia(this.vw);
  this.D.ia(this.uk);
  this.D.ia(this.Yy);
  this.Mk && (this.Mk.Xd(this.k.x), this.Mk.gd(this.k.y));
  this.PD = j;
  return this.D
};
z.fF = p;
z.zp = s();
z.qa = function() {
  this.Cc && this.Y.qa(this.Cc, this.Ig())
};
z.om = function(a) {
  this.k = a.Ca()
};
z.Jq = function() {
  this.Cc && this.Y.Wa(this.Cc, this.Ig())
};
z.b = p;
z.Ge = function() {
  this.b = new ne;
  this.Cb()
};
z.Cb = function() {
  this.b.add("%DataPlotPointCount", 0);
  this.b.add("%DataPlotYSum", 0);
  this.b.add("%DataPlotYMax", -Number.MAX_VALUE);
  this.b.add("%DataPlotYMin", Number.MIN_VALUE);
  this.b.add("%DataPlotYAverage", 0);
  this.b.add("%DataPlotBubbleSizeSum", 0);
  this.b.add("%DataPlotBubbleMaxSize", -Number.MAX_VALUE);
  this.b.add("%DataPlotBubbleMinSize", Number.MAX_VALUE);
  this.b.add("%DataPlotBubbleSizeAverage", 0);
  this.b.add("%DataPlotMaxYValuePointName", "");
  this.b.add("%DataPlotMinYValuePointName", "");
  this.b.add("%DataPlotMaxYValuePointSeriesName", "");
  this.b.add("%DataPlotMinYValuePointSeriesName", "");
  this.b.add("%DataPlotMaxYSumSeriesName", "");
  this.b.add("%DataPlotMinYSumSeriesName", "")
};
z.Na = function(a) {
  if(this.ql(a)) {
    return this.jd[a]
  }
  oe(this.b, "%DataPlotBubbleSizeAverage") || this.b.add("%DataPlotBubbleSizeAverage", this.b.get("%DataPlotBubbleSizeSum") / this.b.get("%DataPlotBubblePointCount"));
  return this.b.get(a)
};
z.Pa = function(a) {
  if(this.ql(a)) {
    return 1
  }
  switch(a) {
    case "%DataPlotPointCount":
    ;
    case "%DataPlotBubblePointCount":
    ;
    case "%DataPlotYSum":
    ;
    case "%DataPlotYMax":
    ;
    case "%DataPlotYMin":
    ;
    case "%DataPlotYAverage":
    ;
    case "%DataPlotBubbleSizeSum":
    ;
    case "%DataPlotBubbleMaxSize":
    ;
    case "%DataPlotBubbleMinSize":
    ;
    case "%DataPlotBubbleSizeAverage":
      return 2;
    case "%DataPlotMaxYValuePointName":
    ;
    case "%DataPlotMinYValuePointName":
    ;
    case "%DataPlotMaxYValuePointSeriesName":
    ;
    case "%DataPlotMinYValuePointSeriesName":
    ;
    case "%DataPlotMaxYSumSeriesName":
    ;
    case "%DataPlotMinYSumSeriesName":
      return 1
  }
};
z.jd = p;
z.ql = function(a) {
  return Boolean(this.jd && this.jd[a])
};
z.Hk = function(a, b) {
  this.jd || (this.jd = {});
  this.jd[a] = b;
  if(this.Sj && C(this.Sj, "data")) {
    var c = F(this.Sj, "data"), d;
    C(c, "attributes") ? d = F(c, "attributes") : (d = {}, H(c, "attributes", d));
    C(d, "attribute") ? c = I(d, "attribute") : (c = [], H(d, "attribute", c));
    (d = fc(c, a)) || (d = {});
    A.isArray(d) ? d.push(lf(a, b)) : d = lf(a, b);
    c.push(d)
  }
};
function lf(a, b) {
  return{"#name#":"attribute", "#children#":[b], name:a, value:b}
}
z.mb = function(a) {
  if(a == p || a == h) {
    a = {}
  }
  if(this.jd) {
    for(var b in this.jd) {
      a[b] = this.jd[b]
    }
  }
  a.YSum = this.b.get("%DataPlotYSum");
  a.YMax = this.b.get("%DataPlotYMax");
  a.YMin = this.b.get("%DataPlotYMin");
  a.YAverage = this.b.get("%DataPlotYSum") / this.b.get("%DataPlotYBasedPointsCount");
  a.BubbleSizeSum = this.b.get("%DataPlotBubbleSizeSum");
  a.BubbleMaxSize = this.b.get("%DataPlotBubbleSizeMax");
  a.BubbleMinSize = this.b.get("%DataPlotBubbleSizeMin");
  a.BubbleSizeAverage = this.b.get("%DataPlotBubbleSizeSum") / this.b.get("%DataPlotBubblePointCount");
  a.MaxYValuePointName = this.b.get("%DataPlotMaxYValuePointName");
  a.MaxYValuePointSeriesName = this.b.get("%DataPlotMaxYValuePointSeriesName");
  a.MinYValuePointName = this.b.get("%DataPlotMinYValuePointName");
  a.MinYValuePointSeriesName = this.b.get("%DataPlotMinYValuePointSeriesName");
  a.MaxYSumSeriesName = this.b.get("%DataPlotMaxYSumSeriesName");
  a.MinYSumSeriesName = this.b.get("%DataPlotMinYSumSeriesName");
  a.PointCount = this.b.get("%DataPlotPointCount");
  return a
};
function mf(a, b, c) {
  var d, f;
  C(c, "charts") ? (c = F(c, "charts"), C(c, "chart") && (d = F(c, "chart"), f = q)) : C(c, "gauges") && (c = F(c, "gauges"), C(c, "gauge") && (d = F(c, "gauge"), f = j));
  if(d) {
    var g, k, l;
    if(C(d, "template")) {
      var c = a.Rp, n = N(d, "template");
      k = [];
      l = {};
      for(var m = c.Sp[n] ? Zb(c.Sp[n]) : p;m != p;) {
        k.push(m);
        l[n] = j;
        n = Cb(F(m, "parent"));
        if(l[n] != h) {
          break
        }
        m = c.Sp[n] ? Zb(c.Sp[n]) : p
      }
      c = k
    }else {
      c = p
    }
    if(!f && c) {
      k = 0;
      for(l = c.length;k < l;k++) {
        if(C(c[k], "chart")) {
          k = F(c[k], "chart");
          C(k, "plot_type") && (g = N(k, "plot_type"));
          break
        }
      }
    }
    C(d, "plot_type") && (g = N(d, "plot_type"));
    var o;
    if(f) {
      o = nf
    }else {
      switch(g) {
        case "map":
          Le("Plot type Map");
          break;
        case "table":
          Le("Plot type Table");
          break;
        case "polar":
          Le("Plot type Polar");
          break;
        case "radar":
          Le("Plot type Radar");
          break;
        case "treemap":
          o = of;
          break;
        case "pie":
        ;
        case "doughnut":
          o = pf;
          break;
        case "funnel":
          o = qf;
          break;
        default:
          o = rf
      }
    }
    g || (g = "categorizedvertical");
    if(o) {
      o = new o;
      o.Zr(g);
      o.yK(a);
      o.cA(a.dj());
      a = o.br();
      g = Zb(sf);
      f = a.zh();
      g[a.Sq()] = F(g, "chart");
      g = a.$f(g, f);
      if(c) {
        for(f = c.length - 1;0 <= f;f--) {
          g = a.$f(g, c[f])
        }
      }
      d = a.DE(g, d);
      o.fG() && o.uF(d);
      b.Ul(o);
      b.g(d)
    }
  }
}
;var sf = {chart:{chart_settings:{chart_background:{border:{thickness:"2", color:"#2466B1"}, fill:{gradient:{key:[{position:"0", color:"#FFFFFF"}, {position:"0.5", color:"#F3F3F3"}, {position:"1", color:"#FFFFFF"}], angle:"90"}, type:"Gradient"}, inside_margin:{all:"15"}, corners:{type:"Rounded", all:"10"}, effects:{drop_shadow:{enabled:"True", distance:"2", opacity:"0.3"}, enabled:"False"}}, title:{text:{value:"Chart Title"}, background:{border:{gradient:{key:[{position:"0", color:"#DDDDDD"}, {position:"1", 
color:"#D0D0D0"}], angle:"90"}, enabled:"True", type:"Gradient"}, fill:{gradient:{key:[{position:"0", color:"#FFFFFF"}, {position:"0.5", color:"#F3F3F3"}, {position:"1", color:"#FFFFFF"}], angle:"90"}, type:"Gradient"}, inside_margin:{all:"5"}, effects:{drop_shadow:{enabled:"True", distance:"1", opacity:"0.1"}, enabled:"True"}, enabled:"false"}, font:{family:"Tahoma", size:"11", color:"#232323", bold:"True"}, align_by:"DataPlot"}, subtitle:{text:{value:"Chart Sub-Title"}, background:{border:{gradient:{key:[{position:"0", 
color:"#DDDDDD"}, {position:"1", color:"#D0D0D0"}], angle:"90"}, type:"Gradient"}, fill:{gradient:{key:[{position:"0", color:"#FFFFFF"}, {position:"0.5", color:"#F3F3F3"}, {position:"1", color:"#FFFFFF"}], angle:"90"}, type:"Gradient"}, inside_margin:{all:"10"}, effects:{drop_shadow:{enabled:"True", opacity:"0.1", angle:"45", blur_x:"1", blur_y:"1", color:"Black", distance:"1", strength:"1"}, enabled:"True"}, enabled:"True"}, font:{family:"Tahoma", size:"11", color:"#232323", bold:"True"}, enabled:"False", 
align_by:"dataplot"}, footer:{text:{value:"\u00a9 2008 AnyChart.Com Flash Charting Solutions"}, background:{border:{gradient:{key:[{position:"0", color:"#DDDDDD"}, {position:"1", color:"#D0D0D0"}], angle:"90"}, type:"Gradient"}, fill:{gradient:{key:[{position:"0", color:"#FFFFFF"}, {position:"0.5", color:"#F3F3F3"}, {position:"1", color:"#FFFFFF"}], angle:"90"}, type:"Gradient"}, inside_margin:{all:"10"}, effects:{drop_shadow:{enabled:"True", opacity:"0.1", angle:"45", blur_x:"1", blur_y:"1", color:"#000000", 
distance:"1", strength:"1"}, enabled:"True"}, enabled:"True"}, font:{family:"Tahoma", size:"11", color:"#232323"}, enabled:"false", position:"bottom", align:"far"}}}, defaults:{legend:{font:{family:"Verdana", size:"10", bold:"false", color:"rgb(35,35,35)"}, format:{value:"{%Icon} {%Name}"}, title:{text:{value:"Legend Title"}, font:{family:"Verdana", size:"10", bold:"true", color:"rgb(35,35,35)"}, background:{inside_margin:{all:"6"}, enabled:"false"}, padding:"2"}, title_separator:{gradient:{key:[{position:"0", 
color:"#333333", opacity:"0"}, {position:"0.5", color:"#333333", opacity:"1"}, {position:"1", color:"#333333", opacity:"0"}]}, enabled:"true", type:"Gradient", padding:"7"}, background:{border:{gradient:{key:[{position:"0", color:"rgb(221,221,221)"}, {position:"1", color:"rgb(208,208,208)"}], angle:"90"}, type:"Gradient"}, fill:{gradient:{key:[{position:"0", color:"rgb(255,255,255)"}, {position:"0.5", color:"rgb(243,243,243)"}, {position:"1", color:"rgb(255,255,255)"}], angle:"90"}, type:"Gradient"}, 
inside_margin:{all:"5"}, effects:{drop_shadow:{enabled:"True", distance:"1", opacity:"0.1"}, enabled:"True"}, enabled:"true"}, rows_separator:{enabled:"true", padding:"3", type:"Solid", color:"#333333", opacity:"0.05"}, columns_separator:{enabled:"true", padding:"3", type:"Solid", color:"#333333", opacity:"0.05"}, scroll_bar:{fill:{gradient:{key:[{color:"#94999B", position:"0"}, {color:"White", position:"1"}], angle:"0"}, enabled:"true", type:"Gradient"}, border:{enabled:"true", color:"#707173", 
opacity:"1"}, buttons:{background:{corners:{type:"Square", all:"2"}, fill:{gradient:{key:[{color:"#FDFDFD"}, {color:"#EBEBEB"}], angle:"90"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#898B8D"}, {color:"#5D5F60"}], angle:"90"}, enabled:"true", type:"Gradient", opacity:"1"}, effects:{drop_shadow:{enabled:"true", distance:"5", opacity:"0.2"}, enabled:"false"}}, arrow:{fill:{color:"#111111"}, border:{enabled:"false"}}, states:{hover:{background:{fill:{gradient:{key:[{color:"#FDFDFD"}, 
{color:"#F4F4F4"}], angle:"90"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#009DFF"}, {color:"#0056B8"}], angle:"90"}, type:"Gradient"}}}, pushed:{background:{fill:{gradient:{key:[{color:"#DFF2FF"}, {color:"#99D7FF"}], angle:"90"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#009DFF"}, {color:"#0056B8"}], angle:"90"}, type:"Gradient"}}}}}, thumb:{background:{corners:{type:"Rounded", all:"0", right_top:"3", right_bottom:"3"}, fill:{gradient:{key:[{color:"#FFFFFF"}, 
{color:"#EDEDED"}], angle:"0"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#ABAEB0"}, {color:"#6B6E6F"}], angle:"0"}, enabled:"true", type:"Gradient", color:"#494949", opacity:"1"}, effects:{drop_shadow:{enabled:"false", distance:"5", opacity:".2"}, enabled:"true"}}, states:{hover:{background:{fill:{gradient:{key:[{color:"#FFFFFF"}, {color:"#F7F7F7"}], angle:"0"}, enabled:"true", type:"gradient", color:"Cyan"}, border:{gradient:{key:[{color:"#009DFF"}, {color:"#0064C6"}], angle:"0"}, 
type:"Gradient"}}}, pushed:{background:{fill:{gradient:{key:[{color:"#D8F0FF", position:"0"}, {color:"#ACDEFF", position:"1"}], angle:"0"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#009DFF"}, {color:"#008AEC"}], angle:"0"}, type:"Gradient"}}}}}, size:"16", enabled:"true"}, position:"right", align:"near", elements_align:"Left", columns_padding:"10", rows_padding:"10"}, panel:{margin:{all:"5"}, title:{font:{family:"Tahoma", size:"11", color:"#232323", bold:"True"}, align:"center"}, 
background:{border:{thickness:"2", color:"#2466B1"}, fill:{gradient:{key:[{position:"0", color:"#FFFFFF"}, {position:"0.5", color:"#F3F3F3"}, {position:"1", color:"#FFFFFF"}], angle:"90"}, type:"Gradient"}, inside_margin:{all:"5"}, corners:{type:"Rounded", all:"10"}}}, dashboard:{margin:{all:"5"}, title:{font:{family:"Tahoma", size:"11", color:"#232323", bold:"True"}, text:{value:"Dashboard Title"}, align:"center"}}}};
function tf() {
}
z = tf.prototype;
z.zh = x(p);
z.$f = function(a, b) {
  var c = {"#name#":"template"};
  ac(a, b, c);
  var d = this.Sq(), f = F(a, d), g = F(b, d);
  c[d] = {"#name#":d, "#children#":[]};
  this.rn(f, g, c[d]);
  d = $b(F(a, "defaults"), F(b, "defaults"));
  d != p && (c.defaults = d);
  return c
};
z.DE = function(a, b) {
  var c = this.Sq(), d = {"#name#":c};
  this.rn(F(a, c), b, d);
  this.Ts(d, F(a, "defaults"));
  return d
};
z.rn = function(a, b, c) {
  ac(a, b, c);
  var d = F(a, "chart_settings"), f = F(b, "chart_settings"), g = $b(d, f);
  g && (c.chart_settings = g);
  a = cc(F(a, "styles"), F(b, "styles"));
  if(a != p) {
    c.styles = a;
    for(var k in c.styles) {
      C(c.styles[k], "name") && (c.styles[k].name = Cb(F(c.styles[k], "name"))), C(c.styles[k], "parent") && (c.styles[k].name = Cb(F(c.styles[k], "parent")))
    }
  }
  if(d && C(d, "controls")) {
    var l = F(d, "controls")
  }
  if(f && C(d, "controls")) {
    var n = F(f, "controls")
  }
  (c = cc(l, n)) && g && H(g, "controls", c)
};
z.Ts = function(a, b) {
  if(a && b) {
    var c = F(b, "legend"), d = F(a, "chart_settings");
    if(c && d && (C(d, "legend") && H(d, "legend", $b(c, F(d, "legend"), "legend")), C(d, "controls"))) {
      for(var d = F(d, "controls"), f = I(d, "legend"), g = f.length, k = 0;k < g;k++) {
        f[k] = $b(c, f[k], "legend")
      }
      H(d, "legend", f)
    }
  }
};
function uf() {
  this.Sp = {}
}
uf.prototype.Sp = p;
function vf(a, b) {
  for(var c = 0, d = b.length;c < d;c++) {
    var f = b[c], g = Cb(F(f, "name"));
    "" != g && (a.Sp[g] = f)
  }
}
uf.prototype.clear = function() {
  this.Sp = {}
};
var wf = {gauge:{styles:{label_style:{format:{value:"{%Value}"}, position:{anchor:"Top", halign:"Center", valign:"Top", padding:"5"}, font:{family:"Verdana", size:"10", color:"rgb(35,35,35)", bold:"True"}, background:{border:{gradient:{key:[{position:"0", color:"rgb(221,221,221)"}, {position:"1", color:"rgb(208,208,208)"}], angle:"90"}, enabled:"True", type:"Gradient"}, fill:{gradient:{key:[{position:"0", color:"rgb(255,255,255)"}, {position:"0.5", color:"rgb(243,243,243)"}, {position:"1", color:"rgb(255,255,255)"}], 
angle:"90"}, type:"Gradient"}, inside_margin:{all:"3"}, effects:{drop_shadow:{enabled:"True", distance:"1", opacity:"0.1"}, enabled:"True"}, enabled:"false"}, name:"anychart_default"}, tooltip_style:{format:{value:"{%Value}{numDecimals:2}"}, position:{anchor:"Float", halign:"Center", valign:"Top", padding:"5"}, font:{family:"Verdana", size:"10", color:"rgb(35,35,35)", bold:"True", italic:"False", underline:"False"}, background:{border:{gradient:{key:[{position:"0", color:"rgb(221,221,221)", opacity:"1"}, 
{position:"1", color:"rgb(208,208,208)", opacity:"1"}], angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", thickness:"1", type:"Gradient", opacity:"1", color:"rgb(0,0,0)", dashed:"False", dash_length:"5", space_length:"3", caps:"Round", joints:"Round"}, fill:{gradient:{key:[{position:"0", color:"rgb(255,255,255)", opacity:"1"}, {position:"0.5", color:"rgb(243,243,243)", opacity:"1"}, {position:"1", color:"rgb(255,255,255)", opacity:"1"}], angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", 
type:"Gradient", color:"rgb(255,255,255)", opacity:"1", image_mode:"Stretch", image_url:""}, hatch_fill:{enabled:"False", type:"BackwardDiagonal", color:"rgb(0,0,0)", opacity:"1", thickness:"1", pattern_size:"10"}, inside_margin:{left:"10", top:"5", right:"10", bottom:"5"}, corners:{type:"Square", all:"10"}, effects:{drop_shadow:{enabled:"True", opacity:"0.3", angle:"45", color:"Black", distance:"2", blur_x:"2", blur_y:"2"}, enabled:"True"}, enabled:"True"}, name:"anychart_default", enabled:"true"}}, 
circular_template:{styles:{custom_label_style:{label:{format:{value:"Custom label"}, align:"Outside", enabled:"true", padding:"10"}, tickmark:{fill:{color:"%Color"}, border:{color:"Red"}, enabled:"true", shape:"Star5", width:"%AxisSize*.10", length:"%AxisSize*.10", align:"Outside"}, name:"anychart_default"}, trendline_style:{line:{color:"%Color"}, name:"anychart_default", size:"%AxisSize*.1"}, color_range_style:{fill:{color:"%Color", opacity:"0.8"}, name:"anychart_default", start_size:"5", end_size:"5", 
align:"Outside", padding:"0"}, knob_pointer_style:{knob_background:{border:{enabled:"true", color:"#494949", opacity:"0.5"}, fill:{gradient:{key:[{color:"White"}, {color:"Rgb(220,220,220)"}], angle:"45"}, enabled:"true", type:"Gradient"}, effects:{drop_shadow:{enabled:"true", distance:"1", opacity:"0.3"}, enabled:"true"}, radius:"80", gear_height:"5"}, cap:{inner_stroke:{enabled:"false"}, outer_stroke:{enabled:"false"}, background:{fill:{gradient:{key:[{color:"White"}, {color:"Rgb(220,220,220)"}], 
angle:"45"}, enabled:"true", type:"Gradient"}, border:{enabled:"true", color:"#494949", opacity:"0.5"}, enabled:"true"}, enabled:"true", radius:"40"}, needle:{fill:{color:"#F0673B", type:"Solid"}, border:{enabled:"true", type:"Solid", color:"#494949", opacity:"0.3"}, radius:"90", base_radius:"0", thickness:"15", point_thickness:"10", point_radius:"4"}, name:"anychart_default"}, needle_pointer_style:{fill:{color:"%Color", type:"Solid"}, border:{enabled:"true", type:"Solid", color:"#494949", opacity:"0.3"}, 
states:{hover:{border:{color:"#999999", opacity:"0.7"}, fill:{color:"LightColor(%Color)"}}, pushed:{border:{color:"#999999", opacity:"0.7"}, fill:{color:"LightColor(%Color)"}, effects:{inner_shadow:{enabled:"true", distance:"2", blur_x:"2", blur_y:"2", opacity:"0.4"}, enabled:"true"}}}, cap:{background:{fill:{gradient:{key:[{color:"#FFFFFF"}, {color:"#777777"}], type:"Radial", focal_point:"-0.5", angle:"45"}, type:"Gradient"}, border:{enabled:"true", color:"#575757"}, enabled:"true"}, enabled:"true", 
radius:"15"}, name:"anychart_default", radius:"100", base_radius:"0", point_thickness:"0", thickness:"12", point_radius:"0"}, bar_pointer_style:{fill:{type:"Solid", color:"%Color"}, border:{type:"Solid", color:"Blend(DarkColor(%Color),%Color,0.7)"}, effects:{enabled:"false"}, states:{hover:{fill:{type:"Solid", color:"LightColor(%Color)"}, hatch_fill:{enabled:"true", type:"Percent50", color:"%Color"}, border:{color:"%Color"}}, pushed:{fill:{type:"Solid", color:"%Color"}, border:{type:"Solid", color:"Blend(DarkColor(%Color),%Color,0.7)"}, 
hatch_fill:{enabled:"true", type:"Percent50", color:"%Color"}, effects:{bevel:{enabled:"false"}, drop_shadow:{enabled:"false"}, inner_shadow:{enabled:"true", opacity:"0.5", distance:"2"}, enabled:"true"}}}, name:"anychart_default", width:"%AxisSize", align:"Center", padding:"0"}, range_bar_pointer_style:{fill:{type:"Solid", color:"%Color"}, border:{type:"Solid", color:"Blend(DarkColor(%Color),%Color,0.7)"}, effects:{enabled:"false"}, states:{hover:{fill:{type:"Solid", color:"LightColor(%Color)"}, 
hatch_fill:{enabled:"true", type:"Percent50", color:"%Color"}, border:{color:"%Color"}}, pushed:{fill:{type:"Solid", color:"%Color"}, border:{type:"Solid", color:"Blend(DarkColor(%Color),%Color,0.7)"}, hatch_fill:{enabled:"true", type:"Percent50", color:"%Color"}, effects:{bevel:{enabled:"false"}, drop_shadow:{enabled:"false"}, inner_shadow:{enabled:"true", opacity:"0.5", distance:"2"}, enabled:"true"}}}, name:"anychart_default", width:"%AxisSize"}, marker_pointer_style:{fill:{enabled:"true", color:"%Color"}, 
border:{color:"DarkColor(%Color)"}, effects:{bevel:{enabled:"true", distance:"1", highlight_opacity:"0.5", shadow_opacity:"0.5"}, enabled:"true"}, states:{hover:{fill:{color:"Blend(LightColor(%Color),%Color,0.5)"}, hatch_fill:{enabled:"true", type:"Percent50", color:"LightColor(%Color)"}, border:{color:"%Color"}}, pushed:{fill:{color:"Blend(LightColor(%Color),%Color,0.5)"}, hatch_fill:{enabled:"true", type:"Percent50", color:"LightColor(%Color)"}, border:{color:"%Color"}, effects:{bevel:{enabled:"false"}, 
inner_shadow:{enabled:"true", distance:"2", color:"Black", blur_x:"3", blur_y:"3", opacity:"0.5"}}}}, name:"anychart_default", shape:"Triangle", width:"8", height:"8", align:"Inside", padding:"0"}, tooltip_style:{format:{value:"{%Value}{numDecimals:2}"}, position:{anchor:"Float", halign:"Center", valign:"Top", padding:"5"}, font:{family:"Verdana", size:"10", color:"rgb(35,35,35)"}, background:{border:{gradient:{key:[{position:"0", color:"rgb(221,221,221)", opacity:"1"}, {position:"1", color:"rgb(208,208,208)", 
opacity:"1"}], angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", thickness:"1", type:"Gradient", opacity:"1", color:"rgb(0,0,0)", dashed:"False", dash_length:"5", space_length:"3", caps:"Round", joints:"Round"}, fill:{gradient:{key:[{position:"0", color:"rgb(255,255,255)", opacity:"1"}, {position:"0.5", color:"rgb(243,243,243)", opacity:"1"}, {position:"1", color:"rgb(255,255,255)", opacity:"1"}], angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", type:"Gradient", color:"rgb(255,255,255)", 
opacity:"1", image_mode:"Stretch", image_url:""}, hatch_fill:{enabled:"False", type:"BackwardDiagonal", color:"rgb(0,0,0)", opacity:"1", thickness:"1", pattern_size:"10"}, inside_margin:{left:"10", top:"5", right:"10", bottom:"5"}, corners:{type:"Square", all:"10"}, effects:{drop_shadow:{enabled:"True", opacity:"0.3", angle:"45", color:"Black", distance:"2", blur_x:"2", blur_y:"2"}, enabled:"True"}, enabled:"True"}, name:"anychart_default"}, label_style:{format:{value:"{%Value}{numDecimals:2}"}, 
position:{placement_mode:"ByAnchor", anchor:"Center", halign:"Center", valign:"Top", padding:"5"}, font:{family:"Verdana", size:"10", color:"rgb(35,35,35)", bold:"true"}, background:{border:{gradient:{key:[{color:"rgb(221,221,221)"}, {color:"rgb(208,208,208)"}], angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", type:"Gradient"}, fill:{gradient:{key:[{color:"rgb(255,255,255)"}, {color:"rgb(243,243,243)"}, {color:"rgb(255,255,255)"}], angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", 
type:"Gradient", color:"rgb(255,255,255)", opacity:"1", image_mode:"Stretch", image_url:""}, inside_margin:{left:"10", top:"5", right:"10", bottom:"5"}, effects:{drop_shadow:{enabled:"True", opacity:"0.3", angle:"45", color:"Black", distance:"2", blur_x:"2", blur_y:"2"}, enabled:"True"}}, name:"anychart_default"}}, frame:{inner_stroke:{fill:{gradient:{key:[{color:"White"}, {color:"#B0B0B0"}], angle:"-135"}, enabled:"true", type:"Gradient"}, border:{enabled:"true", type:"solid", color:"Rgb(190,190,190)"}, 
enabled:"true", thickness:"5"}, outer_stroke:{fill:{gradient:{key:[{color:"White"}, {color:"#B0B0B0"}], angle:"45"}, enabled:"true", type:"Gradient"}, border:{enabled:"true", type:"solid", color:"Rgb(190,190,190)"}, enabled:"true", thickness:"5"}, background:{fill:{gradient:{key:[{color:"White"}, {color:"Rgb(220,220,220)"}], angle:"45"}, enabled:"true", type:"Gradient"}, enabled:"true"}, effects:{drop_shadow:{enabled:"true", distance:"3", opacity:"0.4"}, enabled:"true"}, enabled:"true", type:"Auto", 
padding:"10"}, axis:{}, pointers:{label:{enabled:"false"}, tooltip:{enabled:"false"}, color:"#F0673B"}, defaults:{axis:{scale:{minimum:"0", maximum:"100", major_interval:"10"}, scale_bar:{fill:{color:"#75B7E1"}, enabled:"true", size:"%AxisSize"}, major_tickmark:{fill:{enabled:"true", color:"#494949"}, enabled:"true", shape:"Rectangle", width:"2", length:"%AxisSize"}, minor_tickmark:{fill:{enabled:"true", color:"#494949"}, enabled:"true", shape:"Rectangle", width:"1", length:"%AxisSize*0.6"}, labels:{format:{value:"{%Value}{numDecimals:0}"}, 
enabled:"true", rotate_circular:"False"}, scale_line:{enabled:"false", color:"#494949"}, radius:"50", size:"4", start_angle:"20", sweep_angle:"320"}}, name:"default"}, linear_template:{styles:{custom_label_style:{label:{format:{value:"Custom label"}, align:"Outside", enabled:"true", padding:"10"}, tickmark:{fill:{color:"%Color"}, border:{color:"Red"}, enabled:"true", shape:"Star5", width:"%AxisSize*.10", length:"%AxisSize*.10", align:"Outside"}, name:"anychart_default"}, trendline_style:{line:{color:"%Color"}, 
name:"anychart_default", size:"%AxisSize*.1"}, color_range_style:{fill:{color:"%Color", opacity:"0.8"}, name:"anychart_default", start_size:"%AxisSize", end_size:"%AxisSize", align:"Center", padding:"0"}, tooltip_style:{format:{value:"{%Value}{numDecimals:2}"}, position:{anchor:"Float", halign:"Center", valign:"Top", padding:"5"}, font:{family:"Verdana", size:"10", color:"rgb(35,35,35)"}, background:{border:{gradient:{key:[{position:"0", color:"rgb(221,221,221)", opacity:"1"}, {position:"1", color:"rgb(208,208,208)", 
opacity:"1"}], angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", thickness:"1", type:"Gradient", opacity:"1", color:"rgb(0,0,0)", dashed:"False", dash_length:"5", space_length:"3", caps:"Round", joints:"Round"}, fill:{gradient:{key:[{position:"0", color:"rgb(255,255,255)", opacity:"1"}, {position:"0.5", color:"rgb(243,243,243)", opacity:"1"}, {position:"1", color:"rgb(255,255,255)", opacity:"1"}], angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", type:"Gradient", color:"rgb(255,255,255)", 
opacity:"1", image_mode:"Stretch", image_url:""}, hatch_fill:{enabled:"False", type:"BackwardDiagonal", color:"rgb(0,0,0)", opacity:"1", thickness:"1", pattern_size:"10"}, inside_margin:{left:"10", top:"5", right:"10", bottom:"5"}, corners:{type:"Square", all:"10"}, effects:{drop_shadow:{enabled:"True", opacity:"0.3", angle:"45", color:"Black", distance:"2", blur_x:"2", blur_y:"2"}, enabled:"True"}, enabled:"True"}, name:"anychart_default"}, label_style:{format:{value:"{%Value}{numDecimals:2}"}, 
position:{placement_mode:"ByAnchor", anchor:"Center", halign:"Center", valign:"Top", padding:"5"}, font:{family:"Verdana", size:"10", color:"rgb(35,35,35)", bold:"true"}, background:{border:{gradient:{key:[{color:"rgb(221,221,221)"}, {color:"rgb(208,208,208)"}], angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", type:"Gradient"}, fill:{gradient:{key:[{color:"rgb(255,255,255)"}, {color:"rgb(243,243,243)"}, {color:"rgb(255,255,255)"}], angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", 
type:"Gradient", color:"rgb(255,255,255)", opacity:"1", image_mode:"Stretch", image_url:""}, inside_margin:{left:"10", top:"5", right:"10", bottom:"5"}, effects:{drop_shadow:{enabled:"True", opacity:"0.3", angle:"45", color:"Black", distance:"2", blur_x:"2", blur_y:"2"}, enabled:"True"}}, name:"anychart_default"}, bar_pointer_style:{fill:{gradient:{key:[{color:"Blend(DarkColor(%Color),%Color,0.5)"}, {color:"%Color"}, {color:"Blend(DarkColor(%Color),%Color,0.5)"}], angle:"0"}, type:"Gradient"}, effects:{enabled:"false"}, 
states:{hover:{fill:{gradient:{key:[{color:"Blend(DarkColor(%Color),LightColor(%Color),0.5)"}, {color:"LightColor(%Color)"}, {color:"Blend(DarkColor(%Color),LightColor(%Color),0.5)"}], angle:"0"}, type:"Gradient"}, hatch_fill:{enabled:"true", type:"Percent50", color:"%Color"}, border:{color:"%Color"}}, pushed:{fill:{gradient:{key:[{color:"Blend(DarkColor(%Color),LightColor(%Color),0.5)"}, {color:"LightColor(%Color)"}, {color:"Blend(DarkColor(%Color),LightColor(%Color),0.5)"}], angle:"0"}, type:"Gradient"}, 
hatch_fill:{enabled:"true", type:"Percent50", color:"%Color"}, border:{color:"%Color"}, effects:{bevel:{enabled:"false"}, drop_shadow:{enabled:"false"}, inner_shadow:{enabled:"true", opacity:"0.5", distance:"2"}, enabled:"true"}}}, name:"anychart_default", width:"%AxisSize"}, tank_pointer_style:{states:{hover:{color:"LightColor(%Color)"}, pushed:{color:"DarkColor(%Color)"}}, name:"anychart_default", width:"%AxisSize*0.9"}, thermometer_pointer_style:{fill:{gradient:{key:[{color:"Blend(DarkColor(%Color),%Color,0.7)"}, 
{color:"%Color"}, {color:"Blend(DarkColor(%Color),%Color,0.7)"}], angle:"0"}, type:"Gradient"}, border:{color:"DarkColor(%Color)"}, effects:{enabled:"false"}, states:{hover:{fill:{gradient:{key:[{color:"Blend(DarkColor(%Color),LightColor(%Color),0.5)"}, {color:"LightColor(%Color)"}, {color:"Blend(DarkColor(%Color),LightColor(%Color),0.5)"}], angle:"0"}, type:"Gradient"}, hatch_fill:{enabled:"true", type:"Percent50", color:"%Color"}, border:{color:"%Color"}}, pushed:{fill:{gradient:{key:[{color:"Blend(DarkColor(%Color),LightColor(%Color),0.5)"}, 
{color:"LightColor(%Color)"}, {color:"Blend(DarkColor(%Color),LightColor(%Color),0.5)"}], angle:"0"}, type:"Gradient"}, hatch_fill:{enabled:"true", type:"Percent50", color:"%Color"}, border:{color:"%Color"}, effects:{bevel:{enabled:"false"}, drop_shadow:{enabled:"false"}, inner_shadow:{enabled:"true", opacity:"0.5", distance:"2"}, enabled:"true"}}}, name:"anychart_default", width:"%AxisSize", bulb_radius:"%AxisSize", bulb_padding:"9"}, range_bar_pointer_style:{fill:{gradient:{key:[{color:"Blend(DarkColor(%Color),%Color,0.5)"}, 
{color:"%Color"}, {color:"Blend(DarkColor(%Color),%Color,0.5)"}], angle:"0"}, type:"Gradient"}, effects:{enabled:"false"}, states:{hover:{fill:{gradient:{key:[{color:"Blend(DarkColor(%Color),LightColor(%Color),0.5)"}, {color:"LightColor(%Color)"}, {color:"Blend(DarkColor(%Color),LightColor(%Color),0.5)"}], angle:"0"}, type:"Gradient"}, hatch_fill:{enabled:"true", type:"Percent50", color:"%Color"}, border:{color:"%Color"}}, pushed:{fill:{gradient:{key:[{color:"Blend(DarkColor(%Color),LightColor(%Color),0.5)"}, 
{color:"LightColor(%Color)"}, {color:"Blend(DarkColor(%Color),LightColor(%Color),0.5)"}], angle:"0"}, type:"Gradient"}, hatch_fill:{enabled:"true", type:"Percent50", color:"%Color"}, border:{color:"%Color"}, effects:{bevel:{enabled:"false"}, drop_shadow:{enabled:"false"}, inner_shadow:{enabled:"true", opacity:"0.5", distance:"2"}, enabled:"true"}}}, name:"anychart_default", width:"%AxisSize"}, marker_pointer_style:{fill:{enabled:"true", color:"%Color"}, border:{color:"DarkColor(%Color)"}, effects:{bevel:{enabled:"true", 
distance:"1", highlight_opacity:"0.5", shadow_opacity:"0.5"}, enabled:"true"}, states:{hover:{fill:{color:"Blend(LightColor(%Color),%Color,0.5)"}, hatch_fill:{enabled:"true", type:"Percent50", color:"LightColor(%Color)"}, border:{color:"%Color"}}, pushed:{fill:{color:"Blend(LightColor(%Color),%Color,0.5)"}, hatch_fill:{enabled:"true", type:"Percent50", color:"LightColor(%Color)"}, border:{color:"%Color"}, effects:{bevel:{enabled:"false"}, inner_shadow:{enabled:"true", distance:"2", color:"Black", 
blur_x:"3", blur_y:"3", opacity:"0.5"}}}}, name:"anychart_default", shape:"Triangle", width:"8", height:"8", align:"Inside", padding:"0"}}, axis:{}, frame:{enabled:"false"}, pointers:{label:{enabled:"false"}, tooltip:{enabled:"false"}}, defaults:{axis:{scale:{minimum:"0", maximum:"100", major_interval:"20"}, scale_bar:{fill:{gradient:{key:[{color:"#E9E9E9"}, {color:"#FFFFFF"}, {color:"#E9E9E9"}]}, enabled:"true", type:"Gradient"}, effects:{drop_shadow:{enabled:"true", opacity:"0.4", distance:"1"}, 
enabled:"true"}, border:{enabled:"true", color:"#B9B9B9"}, enabled:"true", size:"%AxisSize"}, scale_line:{enabled:"true", color:"#494949", align:"Inside"}, major_tickmark:{border:{enabled:"true", color:"#494949"}, enabled:"true", align:"Inside", width:"1.5", length:"5", shape:"Line"}, minor_tickmark:{border:{enabled:"true", color:"#494949"}, enabled:"true", align:"Inside", width:"1", length:"2", shape:"Line"}, labels:{format:{value:"{%Value}{numDecimals:0}"}, enabled:"true", align:"Inside", padding:"5"}, 
position:"50", orientation:"Vertical", size:"10", start_margin:"5", end_margin:"5"}}, name:"default"}, label_template:{styles:{label_style:{format:{value:"{%Value}{numDecimals:2}"}, position:{placement_mode:"ByAnchor", anchor:"Center", halign:"Center", valign:"Top", padding:"5"}, font:{family:"Verdana", size:"10", color:"rgb(35,35,35)", bold:"true"}, background:{border:{gradient:{key:[{color:"rgb(221,221,221)"}, {color:"rgb(208,208,208)"}], angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", 
type:"Gradient"}, fill:{gradient:{key:[{color:"rgb(255,255,255)"}, {color:"rgb(243,243,243)"}, {color:"rgb(255,255,255)"}], angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", type:"Gradient", color:"rgb(255,255,255)", opacity:"1", image_mode:"Stretch", image_url:""}, inside_margin:{left:"10", top:"5", right:"10", bottom:"5"}, effects:{drop_shadow:{enabled:"True", opacity:"0.3", angle:"45", color:"Black", distance:"2", blur_x:"2", blur_y:"2"}, enabled:"True"}}, name:"anychart_default"}}, 
name:"default"}, image_template:{styles:{label_style:{format:{value:"{%Value}{numDecimals:2}"}, position:{placement_mode:"ByAnchor", anchor:"Center", halign:"Center", valign:"Top", padding:"5"}, font:{family:"Verdana", size:"10", color:"rgb(35,35,35)", bold:"true"}, background:{border:{gradient:{key:[{color:"rgb(221,221,221)"}, {color:"rgb(208,208,208)"}], angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", type:"Gradient"}, fill:{gradient:{key:[{color:"rgb(255,255,255)"}, {color:"rgb(243,243,243)"}, 
{color:"rgb(255,255,255)"}], angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", type:"Gradient", color:"rgb(255,255,255)", opacity:"1", image_mode:"Stretch", image_url:""}, inside_margin:{left:"10", top:"5", right:"10", bottom:"5"}, effects:{drop_shadow:{enabled:"True", opacity:"0.3", angle:"45", color:"Black", distance:"2", blur_x:"2", blur_y:"2"}, enabled:"True"}}, name:"anychart_default"}}, name:"default"}, indicator_template:{styles:{tooltip_style:{format:{value:"{%Value}{numDecimals:2}"}, 
position:{anchor:"Float", halign:"Center", valign:"Top", padding:"5"}, font:{family:"Verdana", size:"10", color:"rgb(35,35,35)"}, background:{border:{gradient:{key:[{position:"0", color:"rgb(221,221,221)", opacity:"1"}, {position:"1", color:"rgb(208,208,208)", opacity:"1"}], angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", thickness:"1", type:"Gradient", opacity:"1", color:"rgb(0,0,0)", dashed:"False", dash_length:"5", space_length:"3", caps:"Round", joints:"Round"}, fill:{gradient:{key:[{position:"0", 
color:"rgb(255,255,255)", opacity:"1"}, {position:"0.5", color:"rgb(243,243,243)", opacity:"1"}, {position:"1", color:"rgb(255,255,255)", opacity:"1"}], angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", type:"Gradient", color:"rgb(255,255,255)", opacity:"1", image_mode:"Stretch", image_url:""}, hatch_fill:{enabled:"False", type:"BackwardDiagonal", color:"rgb(0,0,0)", opacity:"1", thickness:"1", pattern_size:"10"}, inside_margin:{left:"10", top:"5", right:"10", bottom:"5"}, corners:{type:"Square", 
all:"10"}, effects:{drop_shadow:{enabled:"True", opacity:"0.3", angle:"45", color:"Black", distance:"2", blur_x:"2", blur_y:"2"}, enabled:"True"}, enabled:"True"}, name:"anychart_default"}, label_style:{format:{value:"{%Value}{numDecimals:2}"}, position:{placement_mode:"ByAnchor", anchor:"Center", halign:"Center", valign:"Top", padding:"5"}, font:{family:"Verdana", size:"10", color:"rgb(35,35,35)", bold:"true"}, background:{border:{gradient:{key:[{color:"rgb(221,221,221)"}, {color:"rgb(208,208,208)"}], 
angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", type:"Gradient"}, fill:{gradient:{key:[{color:"rgb(255,255,255)"}, {color:"rgb(243,243,243)"}, {color:"rgb(255,255,255)"}], angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", type:"Gradient", color:"rgb(255,255,255)", opacity:"1", image_mode:"Stretch", image_url:""}, inside_margin:{left:"10", top:"5", right:"10", bottom:"5"}, effects:{drop_shadow:{enabled:"True", opacity:"0.3", angle:"45", color:"Black", distance:"2", blur_x:"2", 
blur_y:"2"}, enabled:"True"}}, name:"anychart_default"}, indicator_pointer_style:{fill:{color:"%Color"}, border:{color:"Black"}, name:"anychart_default", shape:"Rectangle", width:"10", height:"10"}}, marker:{color:"Orange", value:"10"}, name:"default"}}};
function xf() {
}
A.e(xf, tf);
xf.prototype.Sq = x("gauge");
xf.prototype.zh = function() {
  return this.$f(xf.f.zh.call(this), wf)
};
xf.prototype.rn = function(a, b, c) {
  xf.f.rn.call(this, a, b, c);
  yf(a, b, c, "circular_template");
  yf(a, b, c, "linear_template");
  yf(a, b, c, "label_template");
  yf(a, b, c, "image_template");
  yf(a, b, c, "indicator_template");
  C(b, "circular") && 0 < I(b, "circular").length && H(c, "circular", I(b, "circular"));
  C(b, "linear") && 0 < I(b, "linear").length && H(c, "linear", I(b, "linear"));
  C(b, "label") && 0 < I(b, "label").length && H(c, "label", I(b, "label"));
  C(b, "image") && 0 < I(b, "image").length && H(c, "image", I(b, "image"));
  C(b, "indicator") && 0 < I(b, "indicator").length && H(c, "indicator", I(b, "indicator"))
};
function yf(a, b, c, d) {
  var f = I(a, d), b = I(b, d);
  c[d] = [];
  for(var d = c[d], g = f.length, a = 0;a < g;a++) {
    C(f[a], "name") && (f[a].name = N(f[a], "name")), d.push(f[a])
  }
  f = b.length;
  for(a = 0;a < f;a++) {
    if(C(b[a], "name")) {
      g = N(b[a], "name");
      H(b[a], "name", g);
      for(var k = q, l = 0;l < d.length;l++) {
        if(C(d[a], "name") && J(d[a], "name") == g) {
          k = j;
          c[l] = b[a];
          break
        }
      }
      k || d.push(b[a])
    }else {
      d.push(b[a])
    }
  }
}
;function zf() {
  this.Xa = 0;
  this.qG = q
}
z = zf.prototype;
z.Xa = p;
z.qG = p;
z.D = p;
z.hk = p;
z.K = p;
z.vc = p;
z.g = function(a, b) {
  C(a, "color") && (this.Xa = Yb(a));
  C(a, "under_pointers") && (this.qG = K(a, "under_pointers"));
  Lb(a, "color");
  var c = Ve(b, "label_style", a, C(a, "style") ? N(a, "style") : "anychart_default");
  this.K = new Af;
  this.K.g(c)
};
z.p = function(a, b) {
  this.D = new W(a);
  this.hk = b;
  var c = this.K.wa.Mb();
  c.uc(a, c.eg);
  this.vc = c.jc(a);
  this.D.ia(this.vc);
  return this.D
};
z.qa = function() {
  this.vc && this.K.wa.Mb().update(this.vc.r(), this.vc, this.hk.x, this.hk.y, this.hk.width, this.hk.height)
};
z.Wa = function(a) {
  if(this.vc) {
    var b = this.K.wa.Mb();
    this.hk = a;
    b.update(this.vc.r(), this.vc, this.hk.x, this.hk.y, this.hk.width, this.hk.height)
  }
};
function Bf() {
  Se.call(this)
}
A.e(Bf, Se);
Bf.prototype.Ea = p;
Bf.prototype.Mb = u("Ea");
Bf.prototype.g = function(a) {
  a = Bf.f.g.call(this, a);
  this.Ea = new Cf;
  this.Ea.g(a);
  return a
};
function Af() {
  $.call(this)
}
A.e(Af, $);
Af.prototype.cc = function() {
  return Bf
};
function Df(a) {
  W.call(this, a)
}
A.e(Df, W);
Df.prototype.$a = p;
Df.prototype.update = function(a) {
  this.$a.update(a)
};
Df.prototype.Gi = function(a) {
  this.$a.Gi(a)
};
function Ef() {
}
z = Ef.prototype;
z.Ma = q;
z.isEnabled = u("Ma");
z.yg = t("Ma");
z.K = p;
z.ka = u("K");
z.rf = t("K");
z.Vq = function() {
  A.xa()
};
z.Ib = function(a, b, c) {
  return a.Wq(this, b, this.n(a, b, c))
};
z.n = x(p);
z.qb = function(a, b) {
  b && a && b.qb(a.x, a.y)
};
z.xH = function(a) {
  return new Df(a)
};
z.sh = function(a) {
  return!a ? q : C(a, "style") ? j : C(a, "enabled") ? K(a, "enabled") != this.Ma : q
};
z.pm = function(a) {
  return a && C(a, "style")
};
z.Ue = function(a, b) {
  C(a, "enabled") && (this.Ma = K(a, "enabled"), delete a.enabled);
  Ff(this, a, b)
};
z.Zi = function(a, b) {
  Gf(this, a, b)
};
z.Yi = function(a, b) {
  Gf(this, a, b)
};
function Gf(a, b, c) {
  C(b, "enabled") && (a.Ma = K(b, "enabled"));
  C(b, "style") && Hf(a, c, N(b, "style"))
}
function Ff(a, b, c) {
  var d = N(b, "style"), b = Ve(c, a.Rb(), b, d);
  a.K = a.Qb();
  a.K.g(b)
}
function Hf(a, b, c) {
  var d = a.Rb(), f = b.ka(d, c);
  f ? a.K = f : (a.K = a.Qb(), a.K.g(ef(b, d, c)))
}
z.Wa = function(a, b, c) {
  b.isEnabled() && this.qb(this.Ib(a, b, c), c)
};
z.copy = function(a) {
  a.Ma = this.Ma;
  a.K = this.K;
  return a
};
function If(a, b) {
  Se.call(this, a, b)
}
A.e(If, Se);
z = If.prototype;
z.enabled = j;
z.isEnabled = u("enabled");
z.padding = 5;
z.ga = u("padding");
z.anchor = 0;
z.Df = u("anchor");
z.jy = 2;
z.ll = u("jy");
z.vAlign = 1;
z.pl = u("vAlign");
z.g = function(a) {
  a = If.f.g.call(this, a);
  this.enabled = Tb(a);
  if(C(a, "hatch_type")) {
    var b = J(a, "hatch_type");
    if(C(a, "hatch_fill")) {
      var c = F(a, "hatch_fill");
      C(c, "type") && H(c, "type", N(c, "type").split("%hatchtype").join(b))
    }
  }
  return a
};
function Jf() {
}
z = Jf.prototype;
z.j = p;
z.I = p;
z.D = p;
z.p = function(a, b, c) {
  this.j = a;
  this.I = b;
  this.D = c;
  this.Yc(a, b);
  this.ff(c)
};
z.update = function(a) {
  this.I.qb(this.I.Ib(this.j, a, this.D), this.D)
};
z.Gi = function(a) {
  a ? this.D.I.setAttribute("visibility", "visible") : this.D.I.setAttribute("visibility", "hidden")
};
function Kf() {
  $.call(this)
}
A.e(Kf, $);
Kf.prototype.p = function(a, b) {
  if(!b.isEnabled() || !this.wa.isEnabled() && !this.dc.isEnabled() && !this.kc.isEnabled() && !this.gc.isEnabled() && !this.Zb.isEnabled() && !this.nc.isEnabled()) {
    return p
  }
  var c = b.xH(a.r()), d = this.lc();
  d.p(a, b, c);
  c.$a = d;
  b.Vq(a).ia(c);
  return c
};
function Lf() {
}
A.e(Lf, Ne);
Lf.prototype.createElement = function() {
  return this.$a.createElement()
};
Lf.prototype.Ta = function() {
  this.$a.Ta(this.I)
};
Lf.prototype.vf = function(a) {
  var b = this.j.r(), c = "", d = this.j.Ba().Ba(), f = this.j.n();
  a.Md() ? (c += Md(a.Lc(), b, f, d), c += md()) : c += od();
  this.I.setAttribute("style", c)
};
Lf.prototype.ud = function(a) {
  this.$a.ud(this.I, a)
};
function Mf() {
}
A.e(Mf, Qe);
Mf.prototype.mc = function(a) {
  return a.mc()
};
Mf.prototype.Ie = function(a) {
  return a.Ie()
};
Mf.prototype.ud = function(a) {
  this.$a.ud(this.I, a)
};
function Nf() {
}
A.e(Nf, Ne);
Nf.prototype.createElement = function() {
  return this.$a.createElement()
};
Nf.prototype.Ta = function() {
  this.$a.Ta(this.I)
};
Nf.prototype.vf = function(a) {
  var b = this.j.r(), c = "", d = this.j.Ba().Ba(), f = this.j.n();
  a.xc() ? (c = a.Ie() ? c + Kd(a.mc(), b, f, d) : c + md(), c = a.Md() ? c + Md(a.Lc(), b, f, d) : c + nd()) : c += od();
  this.I.setAttribute("style", c)
};
Nf.prototype.ud = function(a) {
  this.$a.ud(this.I, a)
};
function Of() {
}
A.e(Of, Ne);
Of.prototype.createElement = function() {
  return this.$a.createElement()
};
Of.prototype.Ta = function() {
  this.$a.Ta(this.I)
};
Of.prototype.vf = function(a) {
  this.I.setAttribute("style", a.df() ? Hc(a.De(), this.j.r(), this.j.Ze, this.j.Ba().Ba()) : od())
};
Of.prototype.ud = function(a) {
  this.$a.ud(this.I, a)
};
function Pf() {
}
A.e(Pf, Re);
z = Pf.prototype;
z.Db = p;
z.Fb = p;
z.Yc = function() {
  var a = this.j.ka();
  a.wn() && (this.Db = new Nf, this.Db.p(this.j, this));
  a.wi() && (this.Fb = new Of, this.Fb.p(this.j, this))
};
z.ff = function() {
  var a = this.fb();
  this.Db && a.appendChild(this.Db.I);
  this.Fb && a.appendChild(this.Fb.I)
};
z.fb = function() {
  return this.j.fb()
};
z.update = function(a, b) {
  this.Db && this.Db.update(a, b);
  this.Fb && this.Fb.update(a, b)
};
z.ud = function(a, b) {
  b.pp() ? a.setAttribute("filter", Tc(this.j.r().gk, b.kl())) : a.removeAttribute("filter")
};
function Qf(a, b) {
  Se.call(this, a, b)
}
A.e(Qf, Te);
z = Qf.prototype;
z.Hb = p;
z.mc = u("Hb");
z.sc = p;
z.Lc = u("sc");
z.Pc = p;
z.De = u("Pc");
z.g = function(a) {
  a = Qf.f.g.call(this, a);
  C(a, "fill") && (this.Hb = new Jd, this.Hb.g(F(a, "fill")));
  this.eC(a);
  C(a, "hatch_fill") && (this.Pc = new Fc, this.Pc.g(F(a, "hatch_fill")));
  return a
};
z.eC = function(a) {
  C(a, "border") && (this.sc = new Ld, this.sc.g(F(a, "border")))
};
z.Ie = function() {
  return this.Hb && this.Hb.isEnabled()
};
z.Md = function() {
  return this.sc && this.sc.isEnabled()
};
z.df = function() {
  return this.Pc && this.Pc.isEnabled()
};
z.xc = function() {
  return this.Ie() || this.Md()
};
z.Gf = function() {
  return this.Ie() && this.Hb.se() || this.Md() && this.sc.se()
};
function Rf() {
  $.call(this)
}
A.e(Rf, $);
z = Rf.prototype;
z.cc = function() {
  return Qf
};
z.lc = function() {
  return new Pf
};
z.av = function() {
  return this.wa.xc() || this.dc.xc() || this.kc.xc() || this.Zb.xc() || this.gc.xc() || this.nc.xc()
};
z.lz = function() {
  return this.wa.Md() || this.dc.Md() || this.kc.Md() || this.Zb.Md() || this.gc.Md() || this.nc.Md()
};
z.wn = function() {
  return this.wa.xc() || this.dc.xc() || this.kc.xc() || this.Zb.xc() || this.gc.xc() || this.nc.xc()
};
z.wi = function() {
  return this.wa.df() || this.dc.df() || this.kc.df() || this.Zb.df() || this.gc.df() || this.nc.df()
};
z.Gf = function() {
  return this.wa.Gf() || this.dc.Gf() || this.kc.Gf() || this.Zb.Gf() || this.gc.Gf() || this.nc.Gf()
};
var Sf = {};
function Tf() {
}
A.e(Tf, Nf);
Tf.prototype.update = function(a) {
  Tf.f.update.call(this, a);
  Uf(this.$a, this.I, a)
};
function Vf() {
}
A.e(Vf, Nf);
Vf.prototype.update = function(a) {
  Vf.f.update.call(this, a);
  Uf(this.$a, this.I, a)
};
function Wf() {
}
A.e(Wf, Jf);
z = Wf.prototype;
z.Db = p;
z.Fb = p;
z.Yc = function(a) {
  var b = this.j.gb.ka();
  b.av() && (this.Db = new Tf, this.Db.p(a, this, this.Bm));
  b.wi() && (this.Fb = new Vf, this.Fb.p(a, this, this.Bm))
};
z.ff = function(a) {
  this.Db && a.appendChild(this.Db.I);
  this.Fb && a.appendChild(this.Fb.I)
};
z.update = function(a) {
  Wf.f.update.call(this, a);
  this.Db && this.Db.update(a);
  this.Fb && this.Fb.update(a)
};
z.Bm = function() {
  return this.j.r().ja()
};
function Uf(a, b, c) {
  var d = c.mg(a.j);
  d === Xf ? b.setAttribute("style", od()) : b.setAttribute("d", a.I.JJ.Io(d, a.j.r(), c.ec(), 0, 0, c.Ka(), c.vb()))
}
z.ud = function(a, b) {
  b.pp() ? a.setAttribute("filter", Tc(this.j.r().gk, b.kl())) : a.removeAttribute("filter")
};
function Yf(a, b) {
  Se.call(this, a, b)
}
A.e(Yf, If);
z = Yf.prototype;
z.Ra = -1;
z.Jb = u("Ra");
z.Lt = q;
z.ke = 10;
z.ec = u("ke");
z.tA = 0;
z.Ka = u("ab");
z.sA = 0;
z.vb = u("ob");
z.Y = p;
z.md = u("Y");
z.kl = function() {
  return this.Y.kl()
};
z.k = p;
z.n = u("k");
z.re = function() {
  return this.Y && this.Y.isEnabled()
};
z.pp = function() {
  return this.re() && Boolean(this.Y.Ve && this.Y.Ve.isEnabled())
};
z.Ie = function() {
  return this.re() && this.Y.mc() && this.Y.mc().isEnabled()
};
z.Md = function() {
  return this.re() && this.Y.Lc() && this.Y.Lc().isEnabled()
};
z.xc = function() {
  return this.Ie() || this.Md()
};
z.df = function() {
  return this.re() && this.Y.De() && this.Y.mc().isEnabled()
};
z.mc = function() {
  return this.Y.mc()
};
z.De = function() {
  return this.Y.De()
};
z.Lc = function() {
  return this.Y.Lc()
};
z.mg = function(a) {
  return this.Lt ? a.mg() : this.Ra
};
z.g = function(a) {
  var a = Yf.f.g.call(this, a), b = F(a, "marker");
  C(a, "marker_type") && b && C(b, "type") && H(b, "type", N(b, "type").split("%markertype").join(J(a, "marker_type")));
  if(b && (C(b, "anchor") && (this.anchor = Ad(F(b, "anchor"))), C(b, "size") && (this.ke = M(b, "size")), C(b, "size_width") && (this.tA = M(b, "size_width")), C(b, "size_height") && (this.sA = M(b, "size_height")), C(b, "padding") && (this.padding = M(b, "padding")), C(b, "h_align") && (this.jy = wd(F(b, "h_align"))), C(b, "v_align") && (this.vAlign = zd(F(b, "v_align"))), C(b, "type"))) {
    b = N(b, "type"), this.Lt = "%markertype" == b, this.Lt || (this.Ra = Zf(b))
  }
  0 == this.sA && 0 == this.tA && (this.sA = this.tA = this.ke);
  10 == this.anchor && (this.vAlign = this.jy = 2);
  this.Y = new Od;
  this.Y.g(a);
  this.k = new P(0, 0, this.tA, this.sA);
  return a
};
function $f() {
  $.call(this)
}
A.e($f, Kf);
z = $f.prototype;
z.cc = function() {
  return Yf
};
z.lc = function() {
  return new Wf
};
z.av = function() {
  return this.wa.xc() || this.dc.xc() || this.kc.xc() || this.Zb.xc() || this.gc.xc() || this.nc.xc()
};
z.lz = function() {
  return this.wa.Md() || this.dc.Md() || this.kc.Md() || this.Zb.Md() || this.gc.Md() || this.nc.Md()
};
z.wn = function() {
  return this.wa.xc() || this.dc.xc() || this.kc.xc() || this.Zb.xc() || this.gc.xc() || this.nc.xc()
};
z.wi = function() {
  return this.wa.df() || this.dc.df() || this.kc.df() || this.Zb.df() || this.gc.df() || this.nc.df()
};
z.Gf = function() {
  return this.wa.Gf() || this.dc.Gf() || this.kc.Gf() || this.Zb.Gf() || this.gc.Gf() || this.nc.Gf()
};
var Xf = -1;
function Zf(a) {
  switch(a) {
    default:
    ;
    case "none":
      return Xf;
    case "circle":
      return 0;
    case "square":
      return 1;
    case "diamond":
      return 2;
    case "cross":
      return 3;
    case "diagonalcross":
      return 4;
    case "hline":
      return 5;
    case "vline":
      return 6;
    case "star4":
      return 7;
    case "star5":
      return 8;
    case "star6":
      return 9;
    case "star7":
      return 10;
    case "star10":
      return 11;
    case "triangleup":
      return 12;
    case "triangledown":
      return 13;
    case "image":
      return 14
  }
}
function ag() {
  this.Ra = Xf;
  this.JJ = new bg
}
A.e(ag, Ef);
z = ag.prototype;
z.Ra = 0;
z.Jb = u("Ra");
z.JJ = p;
z.Rb = x("marker_style");
z.Qb = function() {
  return new $f
};
z.Vq = function(a) {
  return a.ca().Yy
};
z.Zi = function(a, b) {
  ag.f.Zi.call(this, a, b);
  cg(this, a)
};
z.Yi = function(a, b) {
  ag.f.Yi.call(this, a, b);
  cg(this, a)
};
function cg(a, b) {
  C(b, "type") && (a.Ra = Zf(N(b, "type")))
}
z.n = function(a, b) {
  return b.n()
};
z.copy = function(a) {
  a || (a = new ag);
  a.Ra = this.Ra;
  return ag.f.copy.call(this, a)
};
function bg() {
}
bg.prototype.Io = function(a, b, c, d, f, g, k) {
  switch(a) {
    default:
    ;
    case Xf:
      return p;
    case 0:
      return k = S(d, f + c / 2), k += b.ub(c / 2, q, j, d + c / 2, f), k += b.ub(c / 2, q, j, d + c, f + c / 2), k += b.ub(c / 2, q, j, d + c / 2, f + c), k += b.ub(c / 2, q, j, d, f + c / 2);
    case 1:
      return k || (k = c), g || (g = c), c = d + 1, f += 1, d = g - 2, k -= 2, g = S(c, f), g += U(c + d, f), g += U(c + d, f + k), g += U(c, f + k), g += U(c, f), g + " Z";
    case 2:
      return k = S(d + c / 2, f), k += U(d + c, f + c / 2), k += U(d + c / 2, f + c), k += U(d, f + c / 2), k += U(d + c / 2, f);
    case 3:
      return k = c / 4, g = S(d + c / 2 - k / 2, f), g += U(d + c / 2 - k / 2, f + c / 2 - k / 2), g += U(d, f + c / 2 - k / 2), g += U(d, f + c / 2 + k / 2), g += U(d + c / 2 - k / 2, f + c / 2 + k / 2), g += U(d + c / 2 - k / 2, f + c), g += U(d + c / 2 + k / 2, f + c), g += U(d + c / 2 + k / 2, f + c / 2 + k / 2), g += U(d + c, f + c / 2 + k / 2), g += U(d + c, f + c / 2 - k / 2), g += U(d + c / 2 + k / 2, f + c / 2 - k / 2), g += U(d + c / 2 + k / 2, f), g += U(d + c / 2 - k / 2, f);
    case 4:
      return k = c / 4 * Math.SQRT2 / 2, g = S(d + k, f), g += U(d + c / 2, f + c / 2 - k), g += U(d + c - k, f), g += U(d + c, f + k), g += U(d + c / 2 + k, f + c / 2), g += U(d + c, f + c - k), g += U(d + c - k, f + c), g += U(d + c / 2, f + c / 2 + k), g += U(d + k, f + c), g += U(d, f + c - k), g += U(d + c / 2 - k, f + c / 2), g += U(d, f + k), g += U(d + k, f);
    case 5:
      return this.rC(b, c, d, f);
    case 6:
      return k = S(d + (c - c / 4) / 2, f), k += U(d + (c - c / 4) / 2 + c / 4, f), k += U(d + (c - c / 4) / 2 + c / 4, f + c), k += U(d + (c - c / 4) / 2, f + c), k += U(d + (c - c / 4) / 2, f);
    case 13:
      return k = S(d + c / 2, f + c), k += U(d + c, f), k += U(d, f), k += U(d + c / 2, f + c);
    case 12:
      return k = S(d + c / 2, f), k += U(d + c, f + c), k += U(d, f + c), k += U(d + c / 2, f);
    case 7:
      return dg(c, d, f, 4);
    case 8:
      return dg(c, d, f, 5);
    case 9:
      return eg(c, d, f, 6);
    case 10:
      return dg(c, d, f, 7);
    case 11:
      return eg(c, d, f, 10)
  }
};
bg.prototype.rC = function(a, b, c, d) {
  a = S(c, d + (b - b / 4) / 2);
  a += U(c + b, d + (b - b / 4) / 2);
  a += U(c + b, d + (b - b / 4) / 2 + b / 4);
  a += U(c, d + (b - b / 4) / 2 + b / 4);
  return a += U(c, d + (b - b / 4) / 2)
};
function dg(a, b, c, d) {
  for(var f = "", a = a / 2, g = a / 2, k = 2 * Math.PI / d, l = -Math.PI / 2, n = l + k / 2, m = 0;m <= d;m++) {
    var o = a * Math.cos(l), v = a * Math.sin(l), w = g * Math.cos(n), y = g * Math.sin(n), o = o + a, v = v + a, w = w + a, y = y + a, f = 0 == m ? f + S(b + o, c + v) : f + U(b + o, c + v), f = f + U(b + w, c + y), l = l + k, n = n + k
  }
  return f
}
function eg(a, b, c, d) {
  var a = a / 2, f = 2 * Math.PI / d, g = [], k, l = "";
  for(k = 0;k <= d;k++) {
    g.push(new O(-a * Math.sin(k * f), -a * Math.cos(k * f)))
  }
  l = g[0];
  k = g[1];
  var n = g[2], m = new O;
  m.x = (l.x - n.x) * (k.y - n.y) / (l.y - n.y) + n.x;
  m.y = k.y;
  m = Math.sqrt(Math.pow(m.x, 2) + Math.pow(m.y, 2));
  n = [];
  for(k = 0;k <= d;k++) {
    n.push(new O(-m * Math.sin(k * f + f / 2), -m * Math.cos(k * f + f / 2)))
  }
  var l = S(b + l.x + a, c + l.y + a), o = 0;
  for(k = 0;k <= 2 * d;k += 2) {
    f = g[o], m = n[o], l += U(b + f.x + a, c + f.y + a), l += U(b + m.x + a, c + m.y + a), o++
  }
  return l
}
;function fg() {
}
A.e(fg, Ud);
z = fg.prototype;
z.Rh = 0.05;
z.Va = u("Rh");
z.re = function() {
  return this.isEnabled()
};
z.g = function(a) {
  fg.f.g.call(this, a);
  this.isEnabled() && C(a, "thickness") && (this.Rh = Qb(a, "thickness"))
};
z.p = function(a) {
  if(!this.isEnabled()) {
    return p
  }
  var b = new gg(a);
  b.pi = a.ja();
  b.xi = a.ja();
  b.Jw = a.ja();
  b.Kw = a.ja();
  b.pi && b.appendChild(b.pi);
  b.xi && b.appendChild(b.xi);
  b.Jw && b.appendChild(b.Jw);
  b.Kw && b.appendChild(b.Kw);
  return b
};
z.qa = function(a, b) {
  var c = a.r(), d = a.pi, f = p, f = this.Lc() && this.Lc().isEnabled() ? Md(this.Lc(), c, b) : nd(), f = f + md();
  d.setAttribute("d", hg(this, c, b));
  d.setAttribute("style", f);
  c = a.r();
  d = a.xi;
  f = p;
  f = this.Lc() && this.Lc().isEnabled() ? Md(this.Lc(), c, b) : nd();
  f += md();
  d.setAttribute("d", ig(this, c, b));
  d.setAttribute("style", f);
  this.oC(a.r(), a.Jw, a.Kw, b)
};
z.qC = function(a, b, c, d, f, g) {
  var c = c.r(), k = "", k = this.Hb && this.Hb.isEnabled() ? k + Kd(this.Hb, c, d, f, j) : k + md(), k = this.sc && this.sc.isEnabled() ? k + Md(this.sc, c, d, f, j) : k + nd();
  Wd(a, b, k, g)
};
z.createElement = function(a) {
  return a.ja()
};
function gg(a) {
  W.call(this, a)
}
A.e(gg, W);
gg.prototype.pi = p;
gg.prototype.xi = p;
gg.prototype.Jw = p;
gg.prototype.Kw = p;
function jg(a) {
  switch(a) {
    default:
    ;
    case "none":
      return-1;
    case "circle":
      return 0;
    case "square":
      return 1;
    case "diamond":
      return 2;
    case "cross":
      return 3;
    case "diagonalcross":
      return 4;
    case "hline":
      return 5;
    case "vline":
      return 6;
    case "star4":
      return 7;
    case "star5":
      return 8;
    case "star6":
      return 9;
    case "star7":
      return 10;
    case "star10":
      return 11;
    case "triangleup":
      return 12;
    case "triangledown":
      return 13;
    case "image":
      return 14;
    case "rectangle":
      return 105;
    case "triangle":
      return 106;
    case "trapezoid":
      return 107;
    case "pentagon":
      return 109;
    case "line":
      return 108
  }
}
function kg() {
}
A.e(kg, bg);
kg.prototype.Io = function(a, b, c, d, f, g, k, l) {
  switch(a) {
    case 109:
      g = Math.min(g, k) / 2;
      k = S(d + g * this.aF[0], f + g * this.bF[0]);
      for(a = 1;5 > a;a++) {
        k += U(d + g * this.aF[a], f + g * this.bF[a])
      }
      k += U(d + g * this.aF[0], f + g * this.bF[0]);
      return k + " Z";
    case 105:
      return fd(d, f, g, k);
    case 107:
      return a = g / 3, b = S(d + a, f), b += U(d + g - a, f), b += U(d + g, f + k), b += U(d, f + k), b += U(d + a, f), b + " Z";
    case 106:
      return a = S(d + g / 2, f), a += U(d, f + k), a += U(d + g, f + k), a += U(d + g / 2, f), a + " Z";
    case 108:
      return Jc(d + g / 2, f, d + g / 2, f + k, l);
    case 5:
      return this.rC(b, c, d, f, g, k, l);
    default:
      return kg.f.Io.call(this, a, b, c, d, f, g, k)
  }
};
kg.prototype.aF = [1 + Math.cos((0.4 - 0.5) * Math.PI), 1 + Math.cos((0.8 - 0.5) * Math.PI), 1 + Math.cos(0.7 * Math.PI), 1 + Math.cos(1.1 * Math.PI), 1 + Math.cos(1.5 * Math.PI)];
kg.prototype.bF = [1 + Math.sin((0.4 - 0.5) * Math.PI), 1 + Math.sin((0.8 - 0.5) * Math.PI), 1 + Math.sin(0.7 * Math.PI), 1 + Math.sin(1.1 * Math.PI), 1 + Math.sin(1.5 * Math.PI)];
kg.prototype.rC = function(a, b, c, d, f, g, k) {
  return Jc(c, d + g / 2, c + f, d + g / 2, k)
};
function lg(a) {
  this.Xb = a;
  this.padding = 0;
  this.type = 2
}
z = lg.prototype;
z.Uc = p;
z.Vd = p;
z.background = p;
z.zm = p;
z.type = p;
z.zb = p;
z.Xb = p;
z.padding = p;
z.pB = j;
z.g = function(a) {
  if(!C(a, "enabled") || K(a, "enabled")) {
    if(C(a, "type")) {
      switch(N(a, "type")) {
        case "circular":
          this.type = 0;
          break;
        case "auto":
          this.type = 1;
          break;
        case "rectangular":
          this.type = 2;
          break;
        case "roundedrectangular":
          this.type = 3
      }
    }
    C(a, "auto_fit") && (this.pB = K(a, "auto_fit"));
    C(a, "padding") && (this.padding = Qb(a, "padding"));
    Tb(a, "inner_stroke") && (this.Uc = this.rH(), this.Uc.g(F(a, "inner_stroke")));
    Tb(a, "outer_stroke") && (this.Vd = this.sH(), this.Vd.g(F(a, "outer_stroke")));
    C(a, "background") && (this.background = this.nH(), this.background.g(F(a, "background")));
    C(a, "corners") && (this.zm = new Dd, this.zm.g(F(a, "corners")))
  }
};
z.cd = p;
z.vB = p;
z.RD = p;
z.VE = p;
z.Bg = p;
z.p = function(a, b) {
  this.Bg = a;
  this.zb = b;
  this.cd = new W(this.Bg);
  this.Vd && this.Vd.isEnabled() && (this.VE = this.Vd.p(this.Bg), this.cd.ia(this.VE));
  this.Uc && this.Uc.isEnabled() && (this.RD = this.Uc.p(this.Bg), this.cd.ia(this.RD));
  this.background && this.background.isEnabled() && (this.vB = this.background.p(this.Bg), this.cd.ia(this.vB));
  return this.cd
};
z.qa = function() {
  this.Vd && this.Vd.isEnabled() && this.Vd.qa(this.VE, this.zb);
  this.Uc && this.Uc.isEnabled() && this.Uc.qa(this.RD, this.zb);
  this.background && this.background.isEnabled() && this.background.qa(this.vB, this.zb)
};
z.Wa = function(a) {
  this.zb = a;
  this.qa()
};
function mg(a, b) {
  Se.call(this, a, b)
}
A.e(mg, If);
z = mg.prototype;
z.Yv = p;
z.ej = u("Yv");
z.OK = p;
z.Bh = u("OK");
z.g = function(a) {
  a = mg.f.g.call(this, a);
  if(!this.enabled) {
    return a
  }
  this.Yv = this.yH();
  this.Yv.g(a);
  if(!this.Yv.isEnabled()) {
    return this.enabled = q, a
  }
  if(C(a, "position")) {
    var b = F(a, "position");
    C(b, "anchor") && (this.anchor = Ad(F(b, "anchor")));
    C(b, "halign") && (this.jy = wd(F(b, "halign")));
    C(b, "valign") && (this.vAlign = zd(F(b, "valign")));
    C(b, "padding") && (this.padding = M(b, "padding"))
  }
  C(a, "format") && (this.OK = new je(J(a, "format")));
  return a
};
z.yH = function() {
  return new ze
};
function ng() {
}
A.e(ng, Jf);
z = ng.prototype;
z.kf = p;
z.Yc = function(a, b) {
  var c = a.be(b.ka()), d = c.ej();
  d.uc(a.r(), (b.yc ? b.yc : c.Bh()).ta(a, this.Gd(a)));
  this.jc(a, b, d)
};
z.Gd = function(a) {
  return a.ca ? a.ca().Gd() : p
};
z.jc = function(a, b, c) {
  this.kf = c.jc(a.r(), a.Ba().Ba(), q)
};
z.ff = function(a) {
  a.ia(this.kf)
};
z.update = function(a) {
  if(!a.isEnabled() || !a.ej()) {
    this.Gi(q)
  }else {
    ng.f.update.call(this, a);
    var b = a.ej(), c = this.j.r();
    b.uc(c, (this.I.yc ? this.I.yc : a.Bh()).ta(this.j, this.Gd(this.j)));
    this.GC(c, b, this.kf, this.j.Ba().Ba());
    this.Gi(j)
  }
};
z.GC = function(a, b, c, d) {
  b.update(a, c, d)
};
z.Gi = function(a) {
  a ? this.kf.I.setAttribute("visibility", "visible") : this.kf.I.setAttribute("visibility", "hidden")
};
function og() {
  $.call(this)
}
A.e(og, Kf);
og.prototype.lc = function() {
  return new ng
};
og.prototype.cc = function() {
  return mg
};
function pg() {
}
A.e(pg, Ef);
z = pg.prototype;
z.yc = p;
z.Rb = x("label_style");
z.Qb = function() {
  return new og
};
z.Vq = function(a) {
  return a.ca().Vx(this)
};
z.pm = function(a) {
  return a && (C(a, "format") || C(a, "style"))
};
z.Ue = function(a, b) {
  pg.f.Ue.call(this, a, b);
  qg(this, a)
};
z.Zi = function(a, b) {
  pg.f.Zi.call(this, a, b);
  qg(this, a)
};
z.Yi = function(a, b) {
  pg.f.Yi.call(this, a, b);
  qg(this, a)
};
function qg(a, b) {
  C(b, "format") && (a.yc = new je(J(b, "format")))
}
z.n = function(a, b, c) {
  b.ej().uc(c.r(), (this.yc ? this.yc : b.Bh()).ta(a, a.Gd ? a.Gd() : p));
  return b.ej().n().Ca()
};
z.copy = function(a) {
  a || (a = new pg);
  a.yc = this.yc;
  return pg.f.copy.call(this, a)
};
function rg(a, b) {
  Se.call(this, a, b)
}
A.e(rg, mg);
rg.prototype.g = function(a) {
  a = rg.f.g.call(this, a);
  9 == this.anchor && 2 == this.vAlign && (this.padding += 20);
  return a
};
function sg() {
  $.call(this)
}
A.e(sg, og);
sg.prototype.p = function(a, b) {
  var c = sg.f.p.call(this, a, b);
  c && c.Gi(q);
  return c
};
sg.prototype.Qb = function() {
  return new rg
};
function tg() {
  this.lI = A.userAgent.hm ? this.pM : this.oM
}
A.e(tg, pg);
z = tg.prototype;
z.Rb = x("tooltip_style");
z.Qb = function() {
  return new sg
};
z.Vq = function(a) {
  return a.ca().dy()
};
function ug(a, b, c, d, f) {
  if(9 == d.Df()) {
    var g = a.n(c, d, f), k = new O;
    a.lI(b, f, k);
    c.Ur(k, d.Df(), d.ll(), d.pl(), g, d.ga());
    c.Vr(k, d.Df(), d.ll(), d.pl(), g, d.ga());
    a.qb(k, f)
  }
}
z.lI = p;
z.oM = function(a, b, c) {
  for(var d = b.r().Nc().parentNode, b = rd(b), f = 0, g = 0;d != p;) {
    f += d.offsetLeft, g += d.offsetTop, d = d.offsetParent
  }
  c.x = a.clientX - (b.x + f);
  c.y = a.clientY - (b.y + g);
  return c
};
z.pM = function(a, b, c) {
  b = rd(b);
  c.x = a.offsetX - b.x;
  c.y = a.offsetY - b.y;
  return c
};
z.copy = function(a) {
  a || (a = new tg);
  return tg.f.copy.call(this, a)
};
function vg(a, b) {
  Se.call(this, a, b)
}
A.e(vg, mg);
vg.prototype.yH = function() {
  return new Cf
};
vg.prototype.hD = function() {
  return this.Yv.hD()
};
function wg() {
}
A.e(wg, ng);
wg.prototype.Gd = x(p);
wg.prototype.GC = function(a, b, c, d) {
  b.update(this.j.r(), c, 0, 0, this.j.w.Zc().n().width, this.j.w.Zc().n().height, d)
};
function xg() {
  $.call(this)
}
A.e(xg, og);
xg.prototype.lc = function() {
  return new wg
};
xg.prototype.cc = function() {
  return vg
};
function yg() {
  this.Ma = j;
  this.aw = q
}
A.e(yg, pg);
z = yg.prototype;
z.Oc = p;
z.Zc = u("Oc");
z.Zz = t("Oc");
z.aw = p;
z.Qb = function() {
  return new xg
};
z.Vq = function(a) {
  return this.aw ? a.Zc().Qy : a.Zc().Py
};
z.Ue = function(a, b) {
  C(a, "enabled") && (this.Ma = K(a, "enabled"));
  C(a, "under_pointers") && (this.aw = K(a, "under_pointers"));
  Ff(this, a, b)
};
z.Ib = function(a, b) {
  var c = new O;
  switch(b.hD()) {
    case zg:
      a.Qq(b.Df(), c);
      break;
    case Ag:
    ;
    case Bg:
      c.x = this.Oc.n().x, c.y = this.Oc.n().y
  }
  return c
};
z.copy = function(a) {
  a || (a = new yg);
  a.Zz(this.Oc);
  a.aw = this.aw;
  return yg.f.copy.call(this, a)
};
function Cg() {
  tg.call(this)
}
A.e(Cg, tg);
Cg.prototype.Ue = function(a, b) {
  C(a, "enabled") && (this.Ma = K(a, "enabled"));
  Ff(this, a, b)
};
Cg.prototype.Vq = function(a) {
  return a.w.Zc().uf
};
Cg.prototype.copy = function(a) {
  a || (a = new Cg);
  return Cg.f.copy.call(this, a)
};
function Dg() {
  this.y = this.x = 0;
  this.height = this.width = 1;
  this.zb = new P;
  this.Oa = new Bd(0);
  this.children = [];
  this.O = []
}
z = Dg.prototype;
z.$C = x(0);
z.Sa = p;
z.getName = u("Sa");
z.YE = p;
z.Ul = s();
z.x = 0;
z.y = 0;
z.width = 1;
z.height = 1;
z.children = p;
z.tf = p;
z.OE = function() {
  return this.children.length
};
z.zb = p;
z.n = u("zb");
z.Oa = p;
z.O = p;
z.sF = p;
z.mb = function(a) {
  a = a || {};
  a.children = [];
  for(var b = 0;b < this.children.length;b++) {
    a.children.push(this.children[b].mb())
  }
  a.name = this.Sa;
  return a
};
z.g = function(a) {
  this.tf = new Ue(F(a, "styles"));
  C(a, "name") && (this.Sa = J(a, "name"));
  C(a, "parent") && (this.YE = J(a, "parent"));
  C(a, "x") && (this.x = Qb(a, "x"));
  C(a, "y") && (this.y = Qb(a, "y"));
  C(a, "width") && (this.width = Qb(a, "width"));
  C(a, "height") && (this.height = Qb(a, "height"));
  C(a, "margin") && this.Oa.g(F(a, "margin"));
  (this.frame = this.qH(F(a, "frame"))) && C(a, "frame") && this.frame.g(F(a, "frame"));
  if(C(a, "labels")) {
    for(var a = I(F(a, "labels"), "label"), b = a.length, c = 0;c < b;c++) {
      var d = new zf;
      d.g(a[c], this.tf);
      this.O.push(d)
    }
  }
};
z.frame = p;
z.Bg = p;
z.cd = p;
z.uf = p;
z.hx = p;
z.xB = p;
z.Qy = p;
z.Py = p;
z.p = function(a, b, c) {
  this.Bg = a;
  this.Rc(b);
  this.cd = new W(this.Bg);
  this.uf = c;
  this.frame && this.cd.ia(this.frame.p(this.Bg, this.zb));
  this.hx = new W(this.Bg);
  this.xB = new W(this.Bg);
  this.Qy = new W(this.Bg);
  this.Py = new W(this.Bg);
  this.hx.ia(this.xB);
  this.cd.ia(this.Qy);
  this.cd.ia(this.hx);
  this.cd.ia(this.Py);
  var d, b = 0;
  for(d = this.O.length;b < d;b++) {
    var f = this.O[b];
    f.qG ? this.Qy.ia(f.p(a, this.zb)) : this.Py.ia(f.p(a, this.zb))
  }
  b = 0;
  for(d = this.children.length;b < d;b++) {
    this.cd.ia(this.children[b].p(this.Bg, this.zb, c))
  }
  return this.cd
};
z.qa = function() {
  var a, b;
  this.frame && this.frame.qa();
  a = 0;
  for(b = this.O.length;a < b;a++) {
    this.O[a].qa()
  }
  a = 0;
  for(b = this.children.length;a < b;a++) {
    this.children[a].qa()
  }
};
z.Wa = function(a) {
  var b;
  this.Rc(a);
  this.frame && this.frame.Wa(this.zb);
  a = 0;
  for(b = this.O.length;a < b;a++) {
    this.O[a].Wa(this.zb)
  }
  a = 0;
  for(b = this.children.length;a < b;a++) {
    this.children[a].Wa(this.zb)
  }
};
z.Rc = function(a) {
  this.zb.x = a.x + this.x * a.width;
  this.zb.y = a.y + this.y * a.height;
  this.zb.width = a.width * this.width;
  this.zb.height = a.height * this.height;
  var a = this.zb, b = a.width, c = a.height;
  a.bh(a.tb() + this.Oa.tb() * b / 100);
  a.eh(a.Ja() - this.Oa.Ja() * b / 100);
  a.fh(a.rb() + this.Oa.rb() * c / 100);
  a.$g(a.ra() - this.Oa.ra() * c / 100)
};
function Eg() {
}
Eg.prototype.apply = function(a, b, c) {
  var d = Fg(b, "default"), f = C(a, "template") ? N(a, "template") : c;
  if(f) {
    c = [];
    for(f = Fg(b, f);f;) {
      c.push(f), f = C(f, "parent") ? Fg(b, N(f, "parent")) : p
    }
    for(b = c.length - 1;0 <= b;b--) {
      d = Gg(d, c[b])
    }
  }
  return Gg(d, a)
};
function Gg(a, b) {
  var c = $b(a, b);
  a && Hg(a);
  b && Hg(b);
  var d = cc(F(a, "styles"), F(b, "styles"));
  d && H(c, "styles", d);
  if(C(c, "defaults") && (d = F(c, "defaults"), C(d, "axis"))) {
    d = F(d, "axis");
    if(C(c, "axis")) {
      var f = Ig(d, F(a, "axis")), f = Ig(f, F(c, "axis"));
      H(c, "axis", f)
    }
    if(C(c, "extra_axes")) {
      for(var f = [], g = F(c, "extra_axes"), k = I(g, "axis"), l = k.length, n = 0;n < l;n++) {
        f.push(Ig(d, k[n]))
      }
      H(g, "axis", f)
    }
  }
  return c
}
function Ig(a, b) {
  if(b == p && a != p) {
    return a
  }
  if(b != p && a == p) {
    return b
  }
  var c = $b(a, b);
  H(c, "color_ranges", gc(F(a, "color_ranges"), F(b, "color_ranges"), "color_range"));
  H(c, "custom_labels", gc(F(a, "custom_labels"), F(b, "custom_labels"), "custom_label"));
  H(c, "trendlines", gc(F(a, "trendlines"), F(b, "trendlines"), "trendline"));
  return c
}
function Hg(a) {
  if(C(a, "styles")) {
    for(var a = I(a, "styles"), b = 0;b < a.length;b++) {
      C(a[b], "name") && (a[b].name = N(a[b], "name"))
    }
  }
}
function Fg(a, b) {
  for(var b = b.toLowerCase(), c = 0;c < a.length;c++) {
    var d = a[c];
    if(C(d, "name") && N(d, "name") == b) {
      return d
    }
  }
  return p
}
;function Jg(a, b) {
  Se.call(this, a, b);
  this.za = Kg;
  this.Ia = 0
}
A.e(Jg, Qf);
z = Jg.prototype;
z.za = p;
z.Ia = p;
z.ga = u("Ia");
z.g = function(a) {
  var b = a = Jg.f.g.call(this, a);
  C(b, "align") && (this.za = Lg(b));
  C(b, "padding") && (this.Ia = Qb(b, "padding"));
  return a
};
z.Po = function(a) {
  this.Hb && this.Hb.Gf() && a.Me(this.Hb.yb);
  this.sc && this.sc.Gf() && a.Me(this.sc.yb)
};
z.Po = function(a) {
  this.Hb && this.Hb.se() && a.Me(this.Hb.yb);
  this.Kb && this.Kb.se() && a.Me(this.Kb.yb)
};
function Mg() {
  this.k = new P;
  this.xv = this.hE = this.uj = q;
  this.Xa = zb("#F1683C", h)
}
z = Mg.prototype;
z.Oc = p;
z.Zc = u("Oc");
z.Zz = t("Oc");
z.uj = q;
z.Ra = -1;
z.Sa = p;
z.getName = u("Sa");
z.zD = function() {
  return this.Sa && "" != this.Sa
};
z.xv = q;
z.Un = j;
z.w = p;
z.vj = t("w");
z.D = p;
z.fb = u("D");
z.r = function() {
  return this.D.r()
};
z.jd = p;
z.Pf = p;
z.K = p;
z.ka = u("K");
z.ya = p;
z.Ce = p;
z.Ea = p;
z.Fc = p;
z.vc = p;
z.uf = p;
z.bb = NaN;
z.ta = u("bb");
z.Ln = t("bb");
z.hs = p;
function Ng(a, b) {
  var c = a.w.fa();
  return!c.pd && b < c.la ? c.la : !c.od && b > c.pa ? c.pa : b
}
z.k = p;
z.n = u("k");
z.Xa = p;
z.Ba = u("Xa");
z.Di = t("Xa");
z.Ze = p;
z.Ip = t("Ze");
z.g = function(a, b, c) {
  a: {
    var d = C(a, "style") ? N(a, "style") : "anychart_default", f = this.Rb(), g = this.Rb() ? F(a, this.Rb()) : a, k;
    if(g) {
      k = Ve(b, f, g, d)
    }else {
      k = b.ka(f, d);
      if(k != p) {
        this.K = k;
        break a
      }
      k = ef(b, f, d)
    }
    k ? (this.K = this.Qb(), this.K.g(k), g && (g = this.K, b.Ti[f] || (b.Ti[f] = {}), b.Ti[f][d] = g)) : this.K = b.ka(f, "anychart_default")
  }
  d = this.K;
  d.wa.Po && (d.wa.Po(this.Oc), d.dc.Po(this.Oc), d.kc.Po(this.Oc), d.Zb.Po(this.Oc), d.gc.Po(this.Oc));
  C(a, "name") && (this.Sa = F(a, "name"));
  C(a, "value") && (this.bb = M(a, "value"));
  C(a, "selected") && (this.uj = K(a, "selected"));
  C(a, "use_hand_cursor") && (this.Un = K(a, "use_hand_cursor"));
  C(a, "allow_select") && (this.xv = K(a, "allow_select"));
  if(C(a, "attributes")) {
    this.jd = {};
    d = I(F(a, "attributes"), "attribute");
    f = d.length;
    for(g = 0;g < f;g++) {
      if(k = d[g], C(k, "name")) {
        var l;
        C(k, "custom_attribute_value") ? l = F(k, "custom_attribute_value") : C(k, "value") && (l = F(k, "value"));
        l && (this.jd["%" + J(k, "name")] = l)
      }
    }
  }
  C(a, "actions") && (this.Pf = new Fe(c, this), this.Pf.g(F(a, "actions")));
  C(a, "color") && (this.Xa = Yb(a));
  C(a, "hath_type") && (this.Ze = Gc(N(a, "hatch_type")));
  C(a, "label") && this.Ea.Ue(F(a, "label"), b);
  C(a, "tooltip") && this.Fc.Ue(F(a, "tooltip"), b);
  this.bb && !isNaN(this.bb) && (this.hs = this.bb.toString())
};
z.pu = function(a) {
  this.Un && ld(a.I);
  A.G.nb(a.I, ka.Os, this.mj, q, this);
  A.G.nb(a.I, ka.Ns, this.Kl, q, this);
  A.G.nb(a.I, ka.gm, this.AN, q, this);
  A.G.nb(a.I, ka.cB, this.Jl, q, this);
  A.G.nb(a.I, ka.Yn, this.QE, q, this)
};
z.mj = function() {
  this.hE = j;
  this.Ce = this.uj ? "SelectedHover" : "Hover";
  this.ya = this.be(this.K);
  this.$a.update(this.ya, q);
  this.uf && (this.uf.update(this.be(this.Fc.ka())), this.uf.Aj(j))
};
z.Kl = function() {
  this.hE = q;
  this.Ce = this.uj ? "SelectedNormal" : "Normal";
  this.ya = this.be(this.K);
  this.$a.update(this.ya, q);
  this.uf && this.uf.Gi(q)
};
z.AN = function() {
  this.Pf && this.Pf.execute(this);
  this.uj || (this.Ce = "Pushed", this.ya = this.be(this.K), this.$a.update(this.ya, q))
};
z.Jl = function(a) {
  this.xv && !this.uj && (this.Oc.sF && this.Oc.sF.tx(), this.Oc.sF = this, this.uj = j, this.mj(a))
};
z.QE = function(a) {
  this.hE && this.uf && ug(this.Fc, a, this, this.be(this.Fc.ka()), this.uf)
};
z.tx = function() {
  this.uj = q;
  this.Kl(p)
};
z.p = function(a) {
  this.D = new W(a);
  this.pu(this.D);
  this.Ce = (this.uj = this.xv && this.uj) ? "SelectedNormal" : "Normal";
  this.ya = this.be(this.K);
  this.$a = this.K.p(this);
  this.Ea && (this.vc = this.Ea.ka().p(this, this.Ea)) && this.pu(this.vc);
  this.Fc && (this.uf = this.Fc.ka().p(this, this.Fc));
  return this.D
};
z.qa = function() {
  this.$a.update(this.ya, j);
  if(this.Ea) {
    var a = this.Ea, b = this.vc;
    b && a && b.update(this.be(a.ka()))
  }
};
z.be = function(a) {
  return this.pD(a, this.Ce)
};
z.pD = function(a, b) {
  switch(b) {
    default:
    ;
    case "Normal":
      return a.wa;
    case "Hover":
      return a.dc;
    case "Pushed":
      return a.kc;
    case "SelectedNormal":
      return a.Zb;
    case "SelectedHover":
      return a.gc;
    case "Missing":
      return a.nc
  }
};
z.Wa = function() {
  this.$a.update(p, j);
  this.Dp(this.Ea, this.vc)
};
z.Dp = function(a, b) {
  a && b && b.update(this.be(a.ka()))
};
z.Na = function(a) {
  return"%Value" == a ? this.hs : "%Name" == a ? this.Sa : this.jd[a] ? this.jd[a] : ""
};
z.Pa = function(a) {
  return"%Value" == a ? 2 : 1
};
z.Wq = function(a, b, c) {
  if(!a || !b || !c || this.Gb && !this.aa.pk) {
    return p
  }
  a = new O;
  this.Qq(a, b.Df());
  10 != b.Df() && (this.Ur(a, b.Df(), b.ll(), b.pl(), c, b.ga()), this.Vr(a, b.Df(), b.ll(), b.pl(), c, b.ga()));
  return a
};
z.Ur = function(a, b, c, d, f, g) {
  switch(c) {
    case 0:
      a.x -= f.width + g;
      break;
    case 1:
      a.x += g;
      break;
    case 2:
      a.x -= f.width / 2
  }
};
z.Vr = function(a, b, c, d, f, g) {
  switch(d) {
    case 0:
      a.y -= f.height + g;
      break;
    case 2:
      a.y += g;
      break;
    case 1:
      a.y -= f.height / 2
  }
};
z.Qq = function(a, b) {
  switch(a) {
    case 0:
      b.x = this.k.x + this.k.width / 2;
      b.y = this.k.y + this.k.height / 2;
      break;
    case 7:
      b.x = this.k.x + this.k.width / 2;
      b.y = this.k.y + this.k.height;
      break;
    case 1:
      b.x = this.k.x;
      b.y = this.k.y + this.k.height / 2;
      break;
    case 5:
      b.x = this.k.x + this.k.width;
      b.y = this.k.y + this.k.height / 2;
      break;
    case 3:
      b.x = this.k.x + this.k.width / 2;
      b.y = this.k.y;
      break;
    case 8:
      b.x = this.k.x;
      b.y = this.k.y + this.k.height;
      break;
    case 2:
      b.x = this.k.x;
      b.y = this.k.y;
      break;
    case 6:
      b.x = this.k.x + this.k.width;
      b.y = this.k.y + this.k.height;
      break;
    case 4:
      b.x = this.k.x + this.k.width, b.y = this.k.y
  }
};
var Kg = 2;
function Lg(a) {
  switch(N(a, "align")) {
    case "inside":
      return 0;
    case "outside":
      return 1;
    default:
      return Kg
  }
}
function Og(a, b, c) {
  var d = J(a, b);
  return 0 == d.indexOf("%") ? (a = c, -1 != d.indexOf("*") && (a *= Number(d.substr(d.indexOf("*") + 1))), a) : Qb(a, b)
}
function Pg(a) {
  return 0 == ("" + a).indexOf("%")
}
function Qg(a) {
  a = "" + a;
  return-1 == a.indexOf("*") ? 1 : Number(a.substr(a.indexOf("*") + 1))
}
;function Rg() {
  Tg(this)
}
z = Rg.prototype;
z.XH = s();
z.uJ = x(q);
z.ld = p;
z.kd = p;
z.Td = q;
z.Nb = u("Td");
z.la = p;
z.pa = p;
z.pd = p;
z.od = p;
z.Wc = NaN;
z.yl = p;
z.je = NaN;
z.Wf = p;
z.jz = p;
z.fz = p;
z.Rf = p;
z.en = p;
z.Td = p;
z.aD = u("Td");
z.ug = p;
z.ml = u("ug");
z.ef = p;
z.we = p;
z.fc = p;
z.Jf = p;
z.g = function(a) {
  if(C(a, "mode")) {
    switch(Wb(a, "mode")) {
      default:
      ;
      case "normal":
        this.ug = 0;
        break;
      case "stacked":
        this.ug = 1;
        break;
      case "percentstacked":
        this.ug = 2;
        break;
      case "overlay":
        this.ug = 3;
        break;
      case "sortedoverlay":
        this.ug = 4
    }
  }
  if(C(a, "minimum")) {
    var b = F(a, "minimum");
    Xb(b) ? this.pd = j : (this.pd = q, this.la = this.$i(b))
  }else {
    2 == this.ug && (this.pd = q, this.la = 0)
  }
  C(a, "maximum") ? (b = F(a, "maximum"), Xb(b) ? this.od = j : (this.od = q, this.pa = this.$i(b))) : 2 == this.ug && (this.od = q, this.pa = 100);
  C(a, "major_interval") && (b = F(a, "major_interval"), Xb(b) ? this.yl = j : (this.yl = q, this.Wc = Ob(b)));
  C(a, "minor_interval") && (b = F(a, "minor_interval"), Xb(b) ? this.Wf = j : (this.Wf = q, this.je = Ob(b)));
  C(a, "minimum_offset") && (this.jz = Sb(F(a, "minimum_offset")));
  C(a, "maximum_offset") && (this.fz = Sb(F(a, "maximum_offset")));
  C(a, "inverted") && (this.Td = K(a, "inverted"));
  C(a, "base_value") && (a = F(a, "base_value"), Xb(a) ? this.en = j : (this.en = q, this.Rf = this.$i(a)))
};
z.$i = function(a) {
  return Ob(a)
};
z.Ae = function() {
  isNaN(this.ld) && isNaN(this.kd) && this.pd && this.od && (this.pd = q, this.la = 0, this.od = q, this.pa = 1);
  if(this.od || this.pd) {
    var a = this.kd - this.ld;
    this.pd && (this.la = 0 > this.ld || 0 <= this.ld - this.jz * a ? this.ld - this.jz * a : this.ld);
    this.od && (this.pa = 0 < this.kd || 0 >= this.kd + this.fz * a ? this.kd + this.fz * a : this.kd);
    this.pa <= this.la && (this.od ? this.pa = this.la + 1 : this.pd && (this.la = this.pa - 1))
  }
};
function Tg(a) {
  a.la = 0;
  a.pa = 1;
  a.Rf = 0;
  a.jz = 0.1;
  a.fz = 0.1;
  a.Td = q;
  a.kd = NaN;
  a.ld = NaN;
  a.pd = j;
  a.od = j;
  a.yl = j;
  a.Wf = j;
  a.en = j;
  a.ug = 0
}
z.contains = function(a) {
  var b = this.la, c = this.pa, d = a;
  0 == d && (d = b);
  0 == d && (d = c);
  var f = Math.pow(10, Math.max(6, Math.floor(Math.log(Math.abs(d)) / Math.log(10)) + 3));
  return(a > b || Math.abs(a - b) <= Math.abs(d) / f) && (a < c || Math.abs(a - c) <= Math.abs(d) / f)
};
z.Uy = function(a) {
  return this.contains(a)
};
z.rd = function(a, b) {
  var c = b ? this.we - this.ef : this.we, d = b ? 0 : this.ef, f = (this.we - this.ef) * ((a - this.la) / (this.pa - this.la));
  return this.Td ? c - f : d + f
};
z.pc = function(a) {
  a = (this.we - this.ef) * ((a - this.la) / (this.pa - this.la));
  return this.Td ? this.we - a : this.ef + a
};
z.Mc = function(a) {
  return(10 * this.Rf + 10 * a * this.Wc) / 10
};
z.Rq = function(a) {
  return this.Mc(a)
};
z.jk = function(a, b) {
  return(10 * a + 10 * b * this.je) / 10
};
function Ug(a) {
  a.fc = parseInt((a.pa - a.Rf) / a.Wc + 0.01);
  0 > a.fc && (a.fc = 0)
}
z.kq = function() {
  Ug(this);
  this.Jf = parseInt(this.Wc / this.je + 0.01);
  0 > this.Jf && (this.Jf = 0)
};
z.Uj = function() {
  this.en || (this.Rf = 1)
};
function Vg(a, b) {
  if(0 == a) {
    return 0
  }
  var c = a / b;
  return b * (c - Math.floor(c))
}
function Wg(a, b, c) {
  a /= b;
  b = Math.floor(Math.log(a) / Math.LN10);
  b = Math.pow(10, b);
  c = c ? Math.ceil(a / b) : Math.floor(Number(a / b + 0.5));
  5 < c ? c = 10 : 2 < c ? c = 5 : 1 < c && (c = 2);
  return c * b
}
function Xg(a, b) {
  var c = Math.max(a / 7, Math.pow(10, -b)), d = Math.floor(Math.log(c) / Math.LN10), d = Math.pow(10, d), c = Math.floor(Number(c / d + 0.5));
  5 < c ? c = 10 : 2 < c ? c = 5 : 1 < c && (c = 2);
  return c * d
}
z.Wa = s();
function Yg() {
}
function Zg(a, b, c) {
  a = new Date(a);
  switch(b) {
    case 0:
      b = Math.floor(c);
      a.setUTCFullYear(a.getUTCFullYear() + b);
      c != b && sc(a, 12 * (c - b));
      break;
    case 1:
      sc(a, c);
      break;
    case 2:
      tc(a, c);
      break;
    case 3:
      uc(a, c);
      break;
    case Yg.hP:
      vc(a, c);
      break;
    case 5:
      a.setUTCSeconds(a.getUTCSeconds() + c)
  }
  return a.getTime()
}
function $g(a, b, c) {
  var d = new Date(b), f = c ? 1 : 0, g = d.getUTCFullYear(), k = d.getUTCMonth(), l = d.getUTCDate(), n = d.getUTCHours(), m = d.getUTCMinutes(), d = d.getUTCSeconds(), o = 1 == l, v = 0 == n, w = 0 == m, y = 0 == d;
  switch(a) {
    default:
    ;
    case 0:
      return c && 0 == k && o && v && w && y ? b : (new Date(Date.UTC(g + f, 0))).getTime();
    case 1:
      return c && o && v && w && y ? b : (new Date(Date.UTC(g, k + f))).getTime();
    case 2:
      return c && v && w && y ? b : (new Date(Date.UTC(g, k, l + f))).getTime();
    case 3:
      return c && w && y ? b : (new Date(Date.UTC(g, k, l, n + f))).getTime();
    case 4:
      return c && y ? b : (new Date(Date.UTC(g, k, l, n, m + f))).getTime();
    case 5:
      return(new Date(Date.UTC(g, k, l, n, m, d + f))).getTime()
  }
}
function ah(a, b) {
  var c = a.pa - a.la, d;
  15768E7 < c ? (c = bh(a, c, b), a.Wf && (a.Gh = 0, 1 == c ? a.je = 0.25 : (Xg(c), a.je = h)), d = c) : 31536E6 < c ? (d = bh(a, c, b), a.Wf && (a.Gh = 1, a.je = Math.ceil(c / (3 * b) / 2592E6), 6 < a.je ? a.je = 12 : 3 < a.je && (a.je = 6))) : 7776E6 < c ? (a.Qg = 1, c = Math.ceil(c / b / 2592E6), a.Wf && (a.Gh = 2, a.je = 7.5 * c), d = c) : 864E6 < c ? (c = ch(a, c, b), a.Wf && (a.Gh = 2, d = c / 4, a.je = 12 < d ? 12 : 6 < d ? 6 : 3 < d ? 3 : 2 < d ? 2 : 1), d = c) : 2592E5 < c ? (d = ch(a, c, 
  b), a.Wf && (a.Gh = 3, c = Math.ceil(24 * (c / (3 * b))), a.je = 6 < c ? 12 : 3 < c ? 6 : 1)) : 36002880 < c ? (c = dh(a, c, b), c = 12 < c ? 24 : 6 < c ? 12 : 2 < c ? 6 : 1 < c ? 2 : 1, a.Wf && (a.Gh = 3, a.je = 1 >= c ? 0.25 : 6 >= c ? 1 : 12 >= c ? 2 : 4), d = c) : 108E5 < c ? (d = dh(a, c, b), a.Wf && (a.Gh = 4, a.je = eh(Math.ceil(60 * (c / (3 * b)))))) : 599616 < c ? (c = fh(a, c, b), c = eh(c), a.Wf && (a.Gh = 4, a.je = 1 >= c ? 0.25 : 5 >= c ? 1 : 5), d = c) : 179971.2 < c ? (d = fh(a, 
  c, b), a.Wf && (a.Gh = 5, c = Math.ceil(86400 * (c / (3 * b))), c = eh(c), a.je = c)) : 2592E5 < c && (a.Qg = 5, c = Math.ceil(86400 * (c / b) / 864E5), c = eh(c), a.Wf && (a.Gh = 5, a.je = 1 >= c ? 0.25 : 5 >= c ? 1 : 5), d = c);
  return d
}
function bh(a, b, c) {
  a.Qg = 0;
  return a = Math.ceil(b / c / 31536E6)
}
function ch(a, b, c) {
  a.Qg = 2;
  return a = Math.ceil(b / c / 864E5)
}
function dh(a, b, c) {
  a.Qg = 3;
  return a = Math.ceil(b / c / 864E5)
}
function fh(a, b, c) {
  a.Qg = 4;
  return a = Math.ceil(1440 * (b / c) / 864E5)
}
function eh(a) {
  return 15 < a ? 30 : 5 < a ? 15 : 1 < a ? 5 : 1
}
;function gh(a) {
  this.Dl = a
}
z = gh.prototype;
z.Dl = p;
z.Aa = p;
z.qb = t("Aa");
z.Ib = u("Aa");
z.aB = p;
z.setYear = t("aB");
z.kz = p;
z.setMonth = t("kz");
z.Cq = p;
z.Mo = p;
z.Xu = p;
z.wv = p;
z.KA = p;
z.yt = p;
z.Zm = p;
z.Ji = p;
z.vy = p;
function hh(a, b) {
  for(var c = b.length, d = 0;d < c;d++) {
    var f = Cb(b[d]);
    if(a.Aa + f.length <= a.Ji.length && a.Ji.substr(Cb(a.Aa)) == f) {
      return d
    }
  }
  return-1
}
function ih(a) {
  var b = a.Ji.charAt(a.Aa);
  for(a.Aa++;2 > b.length && a.Aa < a.Ji.length && !isNaN(Number(a.Ji.charAt(a.Aa)));) {
    b += a.Ji.charAt(a.Aa), a.Aa++
  }
  return Math.floor(Number(b))
}
function jh(a, b) {
  var c = Math.floor(Number(a.Ji.substr(a.Aa, b)));
  a.Aa += b;
  return c
}
function kh(a, b, c) {
  b = Cb(b);
  c = Cb(c);
  a.Ji.substr(Cb(a.Aa)) == b ? (a.vy = j, a.Aa += b.length) : a.Ji.substr(Cb(a.Aa)) == c && (a.vy = q, a.Aa += c.length)
}
z.clear = function() {
  this.Xu = this.Zm = this.Mo = this.yt = this.Cq = this.kz = this.aB = -1;
  this.KA = this.wv = NaN;
  this.vy = j
};
var lh = {u:function(a) {
  for(var b = "", c = a.Ib(), d = a.Ji;c < d.length && !isNaN(Number(d.charAt(c)));) {
    b += d.charAt(c), c++
  }
  a.qb(c);
  a.KA = Number(b)
}, d:function(a) {
  var b = ih(a);
  a.Cq = b
}, dd:function(a) {
  var b = jh(a, 2);
  a.Cq = b
}, ddd:function(a) {
  var b = hh(a, a.BE.pA[h]);
  a.yt = b;
  a.Aa += a.BE.dS[b].length
}, dddd:function(a) {
  var b = hh(a, a.BE.XA[h]);
  a.yt = b;
  a.Aa += a.BE.tS[b].length
}, M:function(a) {
  a.setMonth(ih(a) - 1)
}, MM:function(a) {
  a.setMonth(jh(a, 2) - 1)
}, MMM:function(a) {
  var b = hh(a, a.Dl.oD());
  a.setMonth(b);
  a.Aa += a.Dl.oD[b].length
}, MMMM:function(a) {
  var b = hh(a, a.Dl.eD());
  a.setMonth(b);
  a.Aa += a.Dl.eD[b].length
}, y:function(a) {
  a.setYear(2E3 + ih(a))
}, yy:function(a) {
  a.setYear(2E3 + jh(a, 2))
}, yyyy:function(a) {
  a.setYear(jh(a, 4))
}, h:function(a) {
  var b = ih(a);
  a.Zm = b
}, hh:function(a) {
  var b = jh(a, 2);
  a.Zm = b
}, H:function(a) {
  var b = ih(a);
  a.Mo = b
}, HH:function(a) {
  var b = jh(a, 2);
  a.Mo = b
}, m:function(a) {
  var b = ih(a);
  a.Xu = b
}, mm:function(a) {
  var b = jh(a, 2);
  a.Xu = b
}, s:function(a) {
  var b = ih(a);
  a.wv = b
}, ss:function(a) {
  var b = jh(a, 2);
  a.wv = b
}, t:function(a) {
  kh(a, a.Dl.nA, a.Dl.oA)
}, tt:function(a) {
  kh(a, a.Dl.yw, a.Dl.Ez)
}};
function mh() {
}
z = mh.prototype;
z.oB = p;
z.dl = p;
z.iv = p;
z.p = function(a) {
  this.oB = [];
  this.dl = [];
  this.iv = new gh(a);
  a = a.Zy.split("%");
  this.dl = [];
  this.dl.push(a[0].length);
  for(var b = a.length, c = 1;c < b;c++) {
    for(var d = ("" + a[c]).charAt(0), f = d, g = ("" + a[c]).length, k = 1;k < g && ("" + a[c]).charAt(k) == d;k++) {
      f += d
    }
    (d = lh[f]) ? (this.oB.push(d), this.dl.push(("" + a[c]).substring(k).length)) : this.dl[this.dl.length - 1] += ("" + a[c]).length
  }
};
z.getDate = function(a) {
  this.iv.clear();
  this.iv.Ji = a;
  var a = this.iv, b = this.oB, c = this.dl, d = a.Ji.length, f = b.length;
  a.Aa = c[0];
  for(var g = 0;g < f && !(b[g](a), a.Aa += c[g + 1], a.Aa >= d);g++) {
  }
  b = this.iv;
  if(isNaN(b.KA)) {
    if(-1 == b.Mo && -1 != b.Zm && (b.Zm = Math.floor(b.Zm % 12), b.Mo = b.vy ? b.Zm : 12 + b.Zm), a = new Date(Date.UTC(-1 == b.aB ? 1970 : b.aB, -1 == b.kz ? 0 : b.kz, -1 == b.Cq ? 1 : b.Cq, -1 == b.Mo ? 0 : b.Mo, -1 == b.Xu ? 0 : b.Xu, isNaN(b.wv) ? 0 : b.wv)), -1 != b.yt && -1 == b.Cq) {
      b = b.yt - a.getDay(), a.setUTCDate(a.getDate() + b + (0 > b ? 7 : 0))
    }
  }else {
    a = new Date(1E3 * b.KA)
  }
  return a
};
function nh() {
  this.HE = "January,February,March,April,May,June,July,August,September,October,November,December".split(",");
  this.OF = "Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec".split(",");
  this.yw = "AM";
  this.Ez = "PM";
  this.nA = "A";
  this.oA = "P";
  this.XA = "Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday".split(",");
  this.pA = "Sun,Mon,Tue,Wed,Thu,Fri,Sat".split(",");
  this.Zy = "%yyyy/%M/%d %h:%m:%s";
  this.IA = {};
  this.$E = new mh
}
z = nh.prototype;
z.HE = p;
z.eD = function(a) {
  return this.HE[a]
};
z.OF = p;
z.oD = function(a) {
  return this.OF[a]
};
z.yw = p;
z.nA = p;
z.Ez = p;
z.oA = p;
z.XA = p;
z.pA = p;
z.Zy = p;
z.$E = p;
z.g = function(a) {
  if(C(a, "months")) {
    var b = F(a, "months");
    C(b, "names") && (this.HE = J(b, "names").split(","));
    C(b, "short_names") && (this.OF = J(b, "short_names").split(","))
  }
  if(C(a, "week_days") && (b = F(a, "week_days"), C(b, "names") && (this.XA = J(b, "names").split(",")), C(b, "short_names"))) {
    this.pA = J(b, "short_names").split(",")
  }
  if(C(a, "time") && (b = F(a, "time"), C(b, "am_string") && (this.yw = F(b, "am_string")), C(b, "short_am_string") && (this.nA = F(b, "short_am_string")), C(b, "pm_string") && (this.Ez = F(b, "pm_string")), C(b, "short_pm_string"))) {
    this.oA = F(b, "short_pm_string")
  }
  C(a, "format") && (this.Zy = J(a, "format"))
};
z.IA = p;
z.p = function() {
  this.$E.p(this)
};
function oh(a, b) {
  if(a.IA[b]) {
    return a.IA[b]
  }
  var c = a.$E.getDate(b).getTime();
  return a.IA[b] = c
}
function ph() {
}
z = ph.prototype;
z.to = ".";
z.On = "";
z.aE = j;
z.g = function(a) {
  C(a, "decimal_separator") && (this.to = F(a, "decimal_separator").toString());
  C(a, "thousands_separator") && (this.On = F(a, "thousands_separator").toString());
  this.aE = "." == this.to && "" == this.On
};
z.clear = function() {
  this.to = ".";
  this.On = "";
  this.aE = j
};
z.ta = function(a) {
  this.aE || (a = ("" + a).split(this.On).join(""), a = a.split(this.to).join("."));
  return a = Number(a)
};
function qh(a) {
  Rg.apply(this);
  this.w = a;
  this.Ev()
}
A.e(qh, Rg);
z = qh.prototype;
z.Ra = p;
z.Jb = u("Ra");
z.w = p;
z.g = function(a) {
  qh.f.g.call(this, a)
};
z.nI = function(a) {
  return a.getName()
};
function rh(a) {
  qh.call(this, a);
  this.rA = q
}
A.e(rh, qh);
z = rh.prototype;
z.Ev = function() {
  this.Ra = 4
};
z.yJ = q;
z.TJ = j;
z.g = function(a) {
  rh.f.g.call(this, a);
  C(a, "auto_calculation_mode") && ("smart" == N(a, "auto_calculation_mode") ? this.Bu = j : this.Bu = q);
  C(a, "always_show_zero") && (K(a, "always_show_zero") ? this.rA = j : this.rA = q)
};
z.Ae = function() {
  if(this.pd || this.od || this.yl || this.Wf) {
    rh.f.Ae.call(this);
    if(this.pd || this.od) {
      0 == Number(this.kd - this.ld) && this.pd && this.od && (this.la = this.ld - 0.5, this.pa = this.kd + 0.5);
      if(this.pa - this.la < Number.MIN_VALUE && (this.od && (this.pa += 0.2 * (0 == this.pa ? 1 : Math.abs(this.pa))), this.pd)) {
        this.la -= 0.2 * (0 == this.la ? 1 : Math.abs(this.la))
      }
      this.pd && 0 < this.la && 0.25 > this.la / (this.pa - this.la) && (this.la = 0);
      this.od && 0 > this.pa && 0.25 > Math.abs(this.pa / (this.pa - this.la)) && (this.pa = 0);
      if(this.rA && (this.pd && 0 < this.la && (this.la = 0), this.od && 0 > this.pa)) {
        this.pa = 0
      }
    }
    if(this.yl && (this.Wc = this.Bu ? Xg(this.pa - this.la, this.Ih) : Wg(this.pa - this.la, 7, q), sh(this.w) && !this.w.Ss())) {
      this.Uj();
      Ug(this);
      var a = this.w.O.$s(this.w.aa.r());
      a <= (this.pa - this.la) / this.Wc && (this.Wc = Wg(this.pa - this.la, a, j));
      this.yJ = j
    }
    this.Wf && (this.je = Wg(this.Wc, 5, j));
    this.pd && (this.la -= Vg(this.la, this.Wc));
    this.od && (a = Vg(this.pa, this.Wc), 0 != a && (this.pa += this.Wc - a))
  }
  this.TJ && !this.yJ && sh(this.w) && th(this.w);
  this.Uj();
  this.kq()
};
z.Uj = function() {
  this.en && (this.Rf = Math.ceil(Number(this.la) / Number(this.Wc) - 1.0E-8) * Number(this.Wc))
};
z.Ih = p;
z.Bu = q;
z.uJ = u("Bu");
z.XH = function(a) {
  a != p && (this.Bu = j, this.Ih = a)
};
z.rA = q;
function uh(a) {
  qh.call(this, a)
}
A.e(uh, qh);
uh.prototype.Ev = function() {
  this.Ra = 1
};
uh.prototype.Ae = function() {
  this.la = this.ld - 0.5;
  this.pa = this.kd + 0.5;
  this.je = 1;
  this.Rf = this.la;
  var a = q;
  this.yl ? sh(this.w) && !this.w.Ss() ? (this.Wc = 1, Ug(this), a = this.w.O.$s(this.w.aa.r()), this.Wc = Math.ceil((this.pa - this.la) / a), a = j) : this.Wc = Math.floor(Number((this.pa - this.la - 1) / 12)) + 1 : (this.Wc = Math.floor(Number(this.Wc)), 0 >= this.Wc && (this.Wc = 1));
  this.Wf && (this.je = this.Wc);
  !a && sh(this.w) && th(this.w);
  this.kq()
};
function vh(a) {
  qh.call(this, a)
}
A.e(vh, uh);
vh.prototype.Ev = function() {
  this.Ra = 3
};
vh.prototype.$i = function(a) {
  return"" == "" + a ? NaN : oh(this.w.ca().Gd(), "" + a)
};
vh.prototype.nI = function(a) {
  a = a.getName();
  return oh(this.w.ca().Gd(), "" + a)
};
vh.prototype.Nw = function() {
  vh.f.Nw.call(this);
  if(0 != (this.pa - this.Rf) % this.Wc) {
    var a = this.Mc(this.fc);
    a >= this.la && a <= this.pa && this.fc++
  }
};
function wh(a) {
  qh.call(this, a)
}
A.e(wh, qh);
wh.prototype.Ev = function() {
  this.Ra = 0
};
var xh = [0, 0.301029995663981, 0.477121254719662, 0.602059991327962, 0.698970004336019, 0.778151250383644, 0.845098040014257, 0.903089986991944, 0.954242509439325, 1];
z = wh.prototype;
z.Mi = p;
z.Sn = p;
z.Kd = 10;
z.g = function(a) {
  if(a != p) {
    wh.f.g.call(this, a);
    var b = C(a, "logarithmic_base") ? "logarithmic_base" : "log_base";
    C(a, b) && (a = Cb(F(a, b)), this.Kd = "e" == a ? Math.E : Ob(a))
  }
};
z.Ae = function() {
  isNaN(this.ld) && isNaN(this.kd) && this.pd && this.od && (this.pd = q, this.la = 1, this.od = q, this.pa = 10);
  wh.f.Ae.call(this);
  this.yl && (this.Wc = 1);
  0 >= this.la && 0 >= this.pa ? (this.la = 1, this.pa = this.Kd) : 0 > this.EE ? this.EE = this.ER / this.Kd : 0 == this.EE ? this.EE = 1 : 0 >= this.pa && (this.pa = this.la * this.Kd);
  if(1.0E-20 > this.pa - this.la && (this.od && (this.pa *= 2), this.pd)) {
    this.la /= 2
  }
  this.pd && (this.la = Math.pow(this.Kd, Math.floor(Math.log(this.la) / Math.log(this.Kd))));
  this.od && (this.pa = Math.pow(this.Kd, Math.ceil(Math.log(this.pa) / Math.log(this.Kd))));
  this.Mi = Math.round(1E3 * this.Zg(this.la)) / 1E3;
  this.Sn = Math.round(1E3 * this.Zg(this.pa)) / 1E3;
  sh(this.w) && th(this.w);
  this.Uj();
  this.kq()
};
z.Mc = function(a) {
  return this.Rf + a
};
z.Rq = function(a) {
  return Math.pow(this.Kd, this.Mc(a))
};
z.jk = function(a, b) {
  return a + Math.floor(Number(b) / 9) + xh[(b + 9) % 9]
};
z.rd = function(a, b) {
  var c = (this.we - this.ef) * ((a - this.Mi) / (this.Sn - this.Mi)), d = b ? this.we - this.ef : this.we, f = b ? 0 : this.ef;
  return this.Td ? d - c : f + c
};
z.pc = function(a) {
  a = (this.we - this.ef) * ((this.Zg(a) - this.Mi) / (this.Sn - this.Mi));
  return this.Td ? this.we - a : this.ef + a
};
z.Uj = function() {
  this.en && (this.Rf = Math.ceil(this.Zg(this.la) - 1.0E-8))
};
z.kq = function() {
  this.fc = parseInt(Math.floor(this.Zg(this.pa) + 1.0E-12)) - parseInt(this.Rf);
  this.Mc(this.fc) > this.Zg(this.pa) && this.fc--;
  1 > this.fc && (this.fc = 1);
  this.Jf = 9
};
z.Zg = function(a) {
  return 1.0E-20 < a ? Math.log(a) / Math.log(this.Kd) : 0
};
z.Uy = function(a) {
  return a >= this.Mi && a <= this.Sn
};
z.dC = function(a) {
  return Math.pow(this.Kd, a)
};
function yh(a) {
  qh.call(this, a);
  this.Gh = this.Qg = 0
}
A.e(yh, qh);
z = yh.prototype;
z.Ev = function() {
  this.Ra = 2
};
z.Qg = p;
z.Gh = p;
z.g = function(a) {
  yh.f.g.call(this, a);
  if(a) {
    var b;
    C(a, "major_interval_unit") && (b = zh(F(a, "major_interval_unit")), -1 != b && (this.Qg = b));
    C(a, "minor_interval_unit") && (b = zh(F(a, "minor_interval_unit")), -1 != b && (this.Gh = b))
  }
};
function zh(a) {
  switch(Vb(a)) {
    case "year":
      return 0;
    case "month":
      return 1;
    case "day":
      return 2;
    case "hour":
      return 3;
    case "minute":
      return 4;
    case "second":
      return 5
  }
  return-1
}
z.$i = function(a) {
  return"" === "" + a ? NaN : oh(this.w.ca().Gd(), "" + a)
};
z.Ae = function() {
  yh.f.Ae.call(this);
  var a = q;
  this.yl && (this.Wc = ah(this, 7), this.w.O && !this.w.O.ao && (this.Uj(), this.Nw(), a = this.w.O.$s(this.w.r()), a <= (this.pa - this.la) / this.Wc && (this.Wc = ah(this, a)), a = j));
  this.od && (this.pa = $g(this.Qg, this.pa, j));
  this.pd && (this.la = $g(this.Qg, this.la, q));
  this.Uj();
  this.Nw();
  this.Jf = 0;
  for(var b = this.Mc(0), c = this.Mc(1), d = this.jk(b, 0);d < c;) {
    this.Jf++, d = this.jk(b, this.Jf)
  }
  this.Jf = Math.max(0, this.Jf);
  !a && this.w.O && this.w.O.bn(this.w.r())
};
z.Mc = function(a) {
  return Zg(this.Rf, this.Qg, a * this.Wc)
};
z.jk = function(a, b) {
  return Zg(a, this.Gh, b * this.je)
};
z.Uj = function() {
  if(this.en) {
    var a = new Date(this.la), b;
    switch(this.Qg) {
      default:
      ;
      case 0:
        b = new Date(Date.UTC(a.getUTCFullYear(), 0));
        break;
      case 1:
        b = new Date(Date.UTC(a.getUTCFullYear(), a.getUTCMonth()));
        break;
      case 2:
        b = new Date(Date.UTC(a.getUTCFullYear(), a.getUTCMonth(), a.getUTCDate()));
        break;
      case 3:
        b = new Date(Date.UTC(a.getUTCFullYear(), a.getUTCMonth(), a.getUTCDate(), a.getUTCHours()));
        break;
      case 4:
        b = new Date(Date.UTC(a.getUTCFullYear(), a.getUTCMonth(), a.getUTCDate(), a.getUTCHours(), a.getUTCMinutes()));
        break;
      case 5:
        b = a
    }
    this.Rf = b.getTime() < a.getTime() ? Zg(b.getTime(), this.Qg, 1) : b.getTime()
  }
};
z.Nw = function() {
  var a = new Date(this.la), b = new Date(this.pa), c = this.Wc, d = 1, d = b.getUTCFullYear() - a.getUTCFullYear();
  switch(this.Qg) {
    default:
    ;
    case 0:
      d /= c;
      break;
    case 1:
      d = (b.getUTCMonth() - a.getUTCMonth() + 12 * d) / c;
      break;
    case 2:
      d = (this.pa - this.la) / (864E5 * c);
      break;
    case 3:
      d = (this.pa - this.la) / (36E5 * c);
      break;
    case 4:
      d = (this.pa - this.la) / (6E4 * c);
      break;
    case 5:
      d = (this.pa - this.la) / (1E3 * c)
  }
  1 > d && (d = 1);
  this.fc = Math.floor(d)
};
function Ah() {
  rh.call(this);
  this.TJ = q
}
A.e(Ah, rh);
Ah.prototype.AE = aa();
Ah.prototype.dC = aa();
function Bh() {
  Tg(this);
  this.Kd = 10
}
A.e(Bh, Rg);
z = Bh.prototype;
z.Mi = p;
z.Sn = p;
z.Kd = 10;
z.g = function(a) {
  a && (Bh.f.g.call(this, a), C(a, "log_base") && (a = N(a, "log_base"), this.Kd = "e" == a ? Math.E : Ob(a)))
};
z.Ae = function() {
  isNaN(this.ld) && isNaN(this.kd) && this.pd && this.od && (this.pd = q, this.la = 1, this.od = q, this.pa = 10);
  Bh.f.Ae.call(this);
  this.yl && (this.Wc = 1);
  0 >= this.la && 0 >= this.pa ? (this.la = 1, this.pa = this.Kd) : 0 >= this.la ? this.la = this.pa / this.Kd : 0 >= this.pa && (this.pa = this.la * this.Kd);
  if(1.0E-20 > this.pa - this.la && (this.od && (this.pa *= 2), this.pd)) {
    this.la /= 2
  }
  this.pd && (this.la = Math.pow(this.Kd, Math.floor(Math.log(this.la) / Math.log(this.Kd))));
  this.od && (this.pa = Math.pow(this.Kd, Math.ceil(Math.log(this.pa) / Math.log(this.Kd))));
  this.Mi = Math.round(1E3 * this.Zg(this.la)) / 1E3;
  this.Sn = Math.round(1E3 * this.Zg(this.pa)) / 1E3;
  this.Uj();
  this.kq()
};
z.kq = function() {
  this.fc = parseInt(Math.floor(this.Zg(this.pa) + 1.0E-12)) - parseInt(this.Rf);
  this.Mc(this.fc) > this.Zg(this.pa) && this.fc--;
  1 > this.fc && (this.fc = 1);
  this.Jf = 9
};
z.Uj = function() {
  this.en && (this.Rf = Math.ceil(this.Zg(this.la) - 1.0E-8))
};
z.Mc = function(a) {
  return this.Rf + a
};
z.jk = function(a, b) {
  return a + Math.floor(Number(b) / 9) + xh[(b + 9) % 9]
};
z.pc = function(a) {
  a = (this.we - this.ef) * ((this.Zg(a) - this.Mi) / (this.Sn - this.Mi));
  return this.Td ? this.we - a : this.ef + a
};
z.rd = function(a, b) {
  var c = (this.we - this.ef) * ((a - this.Mi) / (this.Sn - this.Mi)), d = b ? this.we - this.ef : this.we, f = b ? 0 : this.ef;
  return this.Td ? d - c : f + c
};
z.Kd = p;
z.AE = function(a) {
  return this.Zg(a)
};
z.Zg = function(a) {
  return 1.0E-20 < a ? Math.log(a) / Math.log(this.Kd) : 0
};
z.dC = function(a) {
  return Math.pow(this.Kd, a)
};
function Ch() {
  this.Y = new Od;
  this.Mn = new kg
}
z = Ch.prototype;
z.Y = p;
z.md = u("Y");
z.Ia = 0;
z.ga = u("Ia");
z.za = Kg;
z.Ar = 0.05;
z.li = u("Ar");
z.ab = 0.05;
z.Ka = u("ab");
z.Bj = t("ab");
z.ob = 0.05;
z.vb = u("ob");
z.Rl = t("ob");
z.Bb = 0;
z.Wl = t("Bb");
z.co = j;
z.Yd = 0;
z.ol = u("Yd");
z.Lp = t("Yd");
z.Mn = p;
z.zo = function(a, b, c, d, f, g, k) {
  var l = new Nd(a, a.ja(), a.ja()), n = new P(b, c, f, g), b = this.Mn.Io(this.Yd, a, d, b, c, f, g, Sd(this.Y) ? this.Y.Lc().Va() : 1), c = "", c = Td(this.Y) ? c + Kd(this.Y.mc(), a, n, k) : c + md(), c = Sd(this.Y) ? c + Md(this.Y.Lc(), a, n, k) : c + nd();
  l.Hg.setAttribute("style", c);
  c = "";
  c = this.Y.Pc && this.Y.Pc.isEnabled() ? c + Hc(this.background.De(), a, p, k) : c + od();
  l.Mg.setAttribute("style", c);
  l.Hg.setAttribute("d", b);
  l.Mg.setAttribute("d", b);
  return l
};
z.g = function(a, b) {
  this.Y.g(a);
  if(this.Y.isEnabled() && (C(a, "padding") && (this.Ia = Qb(a, "padding")), C(a, "length") && (this.Ar = Og(a, "length", b)), C(a, "width") && (this.ab = Og(a, "width", b)), C(a, "shape") && (this.Yd = jg(N(a, "shape"))), C(a, "align") && (this.za = Lg(a)), C(a, "rotation") && (this.Bb = M(a, "rotation")), C(a, "auto_rotate") && (this.co = K(a, "auto_rotate")), 105 == this.Yd || 106 == this.Yd || 107 == this.Yd || 108 == this.Yd || (this.Ar = this.ab = Math.min(this.ab, this.Ar)), 108 == this.Yd)) {
    this.Y.Hb = p, this.Y.Pc = p
  }
};
function Dh(a, b) {
  Jg.call(a, b)
}
A.e(Dh, Jg);
function Eh() {
}
A.e(Eh, Pf);
Eh.prototype.update = function(a, b) {
  b && this.hd();
  Fh(this);
  Eh.f.update.call(this, a, b)
};
function Fh(a) {
  var b = a.j, c = b.ka().wa.Mb(), d = new O(0, 0);
  if(c) {
    d = c.ej();
    c.PC && d.Mp((c.Cy ? b.w.ec() : 1) * c.PC);
    d.uc(b.r(), d.eg);
    var f = b.w.fa().pc(isNaN(c.ta()) ? a.YC() : c.ta()), g = new O;
    a.Jg(g, f, c.za, c.ga());
    a.vc || (a.vc = c.ej().jc(b.r()), b.fb().ia(a.vc));
    d = d.update(b.r(), a.vc, 0, 0, b.w.Zc().n().width, b.w.Zc().n().height, b.Ba())
  }
  a.vc && a.vc.qb(g.x + d.x, g.y + d.y)
}
Eh.prototype.Jg = function(a, b, c, d) {
  var f = this.j.w;
  f instanceof Gh ? Hh(f, f.ng(c, d, 0), b, a) : f.Zc().Vf() ? (a.x = b, a.y = f.Ib(c, d, 0)) : (a.y = b, a.x = f.Ib(c, d, 0))
};
function Ih(a, b) {
  Dh.call(a, b);
  this.vA = 0.5;
  this.Cx = 0.2;
  this.Cu = this.yu = q
}
A.e(Ih, Dh);
z = Ih.prototype;
z.vA = p;
z.Cx = p;
z.Cu = p;
z.yu = p;
z.Ea = p;
z.Mb = u("Ea");
z.g = function(a) {
  var a = Ih.f.g.call(this, a), b;
  C(a, "start_size") && (b = J(a, "start_size"), this.vA = (this.Cu = Pg(b)) ? Qg(b) : Ob(b) / 100);
  C(a, "end_size") && (b = J(a, "end_size"), this.Cx = (this.yu = Pg(b)) ? Qg(b) : Ob(b) / 100);
  Ub(a, "label") && (this.Ea = new Jh, this.Ea.g(F(a, "label")));
  return a
};
function Kh() {
}
A.e(Kh, Eh);
Kh.prototype.hd = function() {
  var a = this.j, b = a.w, c = b.fa(), d = a.be(), f = a.n(), g = c.pc(a.Ub), a = c.pc(a.Lb);
  this.$l = new O;
  this.Ao = new O;
  this.Op = new O;
  this.Bo = new O;
  var c = (d.Cu ? b.ec() : 1) * d.vA, k = (d.yu ? b.ec() : 1) * d.Cx;
  b.Vl(Lh(b, d.za, d.ga(), c, j), g, this.$l);
  b.Vl(Lh(b, d.za, d.ga(), k, j), a, this.Ao);
  b.Vl(Lh(b, d.za, d.ga(), c, q), g, this.Op);
  b.Vl(Lh(b, d.za, d.ga(), k, q), a, this.Bo);
  f.x = Math.min(this.$l.x, this.Ao.x, this.Op.x, this.Bo.x);
  f.y = Math.min(this.$l.y, this.Ao.y, this.Op.y, this.Bo.y);
  f.width = Math.max(this.$l.x, this.Ao.x, this.Op.x, this.Bo.x) - f.x;
  f.height = Math.max(this.$l.y, this.Ao.y, this.Op.y, this.Bo.y) - f.y
};
Kh.prototype.YC = function() {
  var a = this.j;
  return(a.Ub + a.Lb) / 2
};
Kh.prototype.createElement = function() {
  return this.j.r().ja()
};
Kh.prototype.Ta = function(a) {
  var b = S(this.$l.x, this.$l.y), b = b + U(this.Ao.x, this.Ao.y), b = b + U(this.Bo.x, this.Bo.y), b = b + U(this.Op.x, this.Op.y), b = b + U(this.$l.x, this.$l.y);
  a.setAttribute("d", b)
};
function Mh() {
  $.call(this)
}
A.e(Mh, Rf);
Mh.prototype.cc = function() {
  return Ih
};
Mh.prototype.lc = function() {
  return new Kh
};
function Nh() {
}
A.e(Nh, Eh);
Nh.prototype.createElement = function() {
  return this.j.r().ja()
};
Nh.prototype.hd = s();
Nh.prototype.Ta = function(a) {
  var b = this.j, c = b.w, d = c.fa(), f = b.be();
  b.n();
  var g = (f.Cu ? c.size : 1) * f.vA, k = (f.yu ? c.size : 1) * f.Cx, l = Oh(c, f.za, f.ga(), g, j), n = Oh(c, f.za, f.ga(), k, j), g = Oh(c, f.za, f.ga(), g, q), f = Oh(c, f.za, f.ga(), k, q), k = d.pc(b.Ub), d = d.pc(b.Lb), b = b.r(), m = c.Zc().oc, o = "", v = 1, w = c.fa().Nb() ? 0 : 1;
  180 > Math.abs(k - d) && (v = 0);
  if(l == n) {
    var n = m.x + l * Math.cos(k * Math.PI / 180), y = m.y + l * Math.sin(k * Math.PI / 180), E = m.x + l * Math.cos(d * Math.PI / 180), V = m.y + l * Math.sin(d * Math.PI / 180), o = o + S(n, y), o = o + b.ub(l, v, w, E, V, l)
  }else {
    y = new O, Hh(c, l, k, y), o += S(y.x, y.y), o += Ph(c, k, d, l, n)
  }
  f == g ? (n = m.x + f * Math.cos(k * Math.PI / 180), y = m.y + f * Math.sin(k * Math.PI / 180), E = m.x + f * Math.cos(d * Math.PI / 180), V = m.y + f * Math.sin(d * Math.PI / 180), o += U(E, V), o += b.ub(f, v, !w, n, y, f)) : o += Ph(c, d, k, f, g);
  a.setAttribute("d", o)
};
function Ph(a, b, c, d, f) {
  var g = "", k, l, n = (f - d) / (c - b), m = new O;
  if(c > b) {
    for(k = b;k <= c;k++) {
      l = d + n * (k - b), Hh(a, l, k, m), g += U(m.x, m.y)
    }
  }else {
    for(k = b;k >= c;k--) {
      l = d + n * (k - b), Hh(a, l, k, m), g += U(m.x, m.y)
    }
  }
  Hh(a, f, c, m);
  return g += U(m.x, m.y)
}
function Qh() {
  $.call(this)
}
A.e(Qh, Rf);
Qh.prototype.cc = function() {
  return Ih
};
Qh.prototype.lc = function() {
  return new Nh
};
function Rh() {
  Dh.call(this)
}
A.e(Rh, Dh);
z = Rh.prototype;
z.Ea = p;
z.Mb = u("Ea");
z.ke = p;
z.ec = u("ke");
z.qE = p;
z.g = function(a) {
  a = Rh.f.g.call(this, a);
  if(C(a, "size")) {
    var b = J(a, "size");
    this.ke = (this.qE = Pg(b)) ? Qg(b) : Pb(b)
  }
  Ub(a, "line") && (this.sc = new Ld, this.sc.g(F(a, "line")));
  C(a, "label") && (this.Ea = new Jh, this.Ea.g(F(a, "label")));
  this.Pc = this.Hb = p
};
function Sh() {
}
A.e(Sh, Eh);
Sh.prototype.Ub = p;
Sh.prototype.Lb = p;
Sh.prototype.hd = function() {
  var a = this.j, b = a.w, c = b.fa();
  this.Ub = new O;
  this.Lb = new O;
  var c = c.pc(a.ta()), d = a.be(), f = (d.qE ? b.ec() : 1) * d.ec();
  if(b instanceof Gh) {
    var g = Oh(b, d.za, d.ga(), f, q);
    Hh(b, Oh(b, d.za, d.ga(), f, j), c, this.Ub);
    Hh(b, g, c, this.Lb)
  }else {
    b.Vl(Lh(b, d.za, d.ga(), f, j), c, this.Ub), b.Vl(Lh(b, d.za, d.ga(), f, q), c, this.Lb)
  }
  a = a.n();
  a.x = Math.min(this.Ub.x, this.Lb.x);
  a.y = Math.min(this.Ub.y, this.Lb.y);
  a.width = Math.max(this.Ub.x, this.Lb.x) - a.x;
  a.height = Math.max(this.Ub.y, this.Lb.y) - a.y
};
Eh.prototype.YC = function() {
  return this.j.ta()
};
Sh.prototype.update = function(a, b) {
  b && this.hd();
  Sh.f.update.call(this, a, b)
};
Sh.prototype.createElement = function() {
  return this.j.r().ja()
};
Sh.prototype.Ta = function(a) {
  var b = S(this.Ub.x, this.Ub.y), b = b + U(this.Lb.x, this.Lb.y);
  a.setAttribute("d", b)
};
function Th() {
  $.call(this)
}
A.e(Th, Rf);
Th.prototype.cc = function() {
  return Rh
};
Th.prototype.lc = function() {
  return new Sh
};
function Uh() {
  Dh.call(this)
}
A.e(Uh, Dh);
Uh.prototype.Ea = p;
Uh.prototype.Mb = u("Ea");
Uh.prototype.lh = p;
Uh.prototype.g = function(a) {
  a = Uh.f.g.call(this, a);
  C(a, "label") && (this.Ea = new Jh, this.Ea.g(F(a, "label")));
  C(a, "tickmark") && (this.lh = new Vh, this.lh.g(F(a, "tickmark")));
  return a
};
function Wh() {
}
A.e(Wh, Eh);
z = Wh.prototype;
z.js = p;
z.Yc = function() {
  var a = this.j;
  a.ka().wa.lh && (this.js = new W(a.r()))
};
z.ff = function() {
  this.js && this.j.fb().ia(this.js)
};
z.update = function() {
  Fh(this);
  this.js && (this.js.clear(), this.js.ia(this.j.w.zo(this.j.ka().wa.lh, this.j.w.fa().pc(this.j.ta()), this.j.Ba().Ba())))
};
z.YC = function() {
  return this.j.ta()
};
function Xh() {
  $.call(this)
}
A.e(Xh, Rf);
Xh.prototype.cc = function() {
  return Uh
};
Xh.prototype.lc = function() {
  return new Wh
};
var Ag = 0, Bg = 1, zg = 2;
function Cf() {
  Ae(this);
  this.Ko = 2;
  this.Xp = 1;
  this.Dz = Ag;
  this.Ia = 0;
  this.nE = q
}
A.e(Cf, ze);
z = Cf.prototype;
z.Ko = p;
z.Xp = p;
z.Dz = p;
z.hD = u("Dz");
z.Vb = p;
z.Xd = t("Vb");
z.ma = p;
z.gd = t("ma");
z.nE = p;
z.gK = NaN;
z.wg = NaN;
z.es = p;
z.Ia = p;
z.eg = p;
z.g = function(a) {
  Lb(a, "width");
  Cf.f.g.call(this, a);
  if(C(a, "position")) {
    var b = F(a, "position");
    C(b, "halign") && (this.Ko = wd(N(b, "halign")));
    C(b, "valign") && (this.Xp = zd(N(b, "valign")));
    if(C(b, "placement_mode")) {
      var c;
      a: {
        switch(N(b, "placement_mode")) {
          default:
          ;
          case "bypoint":
            c = Ag;
            break a;
          case "byrectangle":
            c = Bg;
            break a;
          case "byanchor":
            c = zg
        }
      }
      this.Dz = c
    }
    C(b, "x") && (this.Vb = Qb(b, "x"));
    C(b, "y") && (this.ma = Qb(b, "y"));
    C(b, "width") && (this.gK = Qb(b, "width"));
    C(b, "height") && (this.wg = Qb(b, "height"));
    C(b, "spread") && (this.es = K(b, "spread"));
    C(b, "padding") && (this.Ia = M(b, "padding"))
  }
  C(a, "format") && (this.eg = J(a, "format"))
};
z.update = function(a, b, c, d, f, g, k, l) {
  var c = isNaN(this.Vb) ? c : (this.nE ? this.Vb : this.Vb * f) + c, d = isNaN(this.ma) ? d : (this.nE ? this.ma : this.ma * g) + d, n = c, m = d, o = this.k.width, v = this.k.height, f = this.gK * f, g = this.wg * g;
  isNaN(f) && (f = this.nf.width);
  isNaN(g) && (g = this.nf.height);
  switch(this.Dz) {
    case Ag:
    ;
    case zg:
      switch(this.Ko) {
        case 2:
          n -= f / 2;
          c -= o / 2;
          break;
        case 0:
          this.es ? (n -= f + this.Ia, c += -f - this.Ia + (f - o) / 2) : c -= o + this.Ia;
          break;
        case 1:
          this.es ? (n += this.Ia, c += this.Ia + (f - o) / 2) : c += this.Ia
      }
      switch(this.Xp) {
        case 0:
          this.es ? (m -= g + this.Ia, d += -g - this.Ia + (g - v) / 2) : d -= v + this.Ia;
          break;
        case 1:
          m -= g / 2;
          d -= v / 2;
          break;
        case 2:
          this.es ? (m += this.Ia, d += this.Ia + (g - v) / 2) : d += this.Ia
      }
      break;
    case Bg:
      switch(this.Ko) {
        case 0:
          c += this.Ia;
          break;
        case 2:
          c += (f - o) / 2;
          break;
        case 1:
          c += f - o - this.Ia
      }
      switch(this.Xp) {
        case 0:
          d += this.Ia;
          break;
        case 1:
          d += (g - v) / 2;
          break;
        case 2:
          d += g - v - this.Ia
      }
  }
  this.es && (this.Y && this.Y.Oa && (o = this.Y.Oa, f += o.tb() + o.Ja(), g += o.rb() + o.ra(), this.Y.fb().qb(n - c, m - d)), this.nf.width = f, this.nf.height = g);
  Cf.f.update.call(this, a, b, k, l);
  b.qb(c, d);
  return new O(c, d)
};
z.aI = function(a) {
  a.Wl(this.Bb.bc, this.nf.width / 2, this.nf.height / 2)
};
function Yh() {
}
z = Yh.prototype;
z.Xa = p;
z.Ba = u("Xa");
z.K = p;
z.ka = u("K");
z.w = p;
z.vj = t("w");
z.Zc = function() {
  return this.w.Zc()
};
z.$a = p;
z.D = p;
z.fb = u("D");
z.r = function() {
  return this.D.r()
};
z.k = p;
z.n = u("k");
z.g = function(a, b) {
  this.Xa = C(a, "color") ? Yb(a) : zb("0xCCCCCC", h);
  this.K = this.jC(a, b)
};
z.jC = function(a, b) {
  var c = Ve(b, this.Rb(), a, J(a, "style")), d = this.Qb(a);
  d.g(c);
  return d
};
z.p = function(a) {
  this.k = new P;
  this.D = new W(a);
  this.$a = this.K.p(this);
  return this.D
};
z.be = function() {
  return this.K.wa
};
z.qa = function() {
  this.$a.update(this.be(), j)
};
z.Wa = function() {
  this.$a.update(p, j)
};
z.Gc = function(a, b) {
  if(isNaN(b.ld) || a < b.ld) {
    b.ld = a
  }
  if(isNaN(b.kd) || a > b.kd) {
    b.kd = a
  }
};
function Jh() {
  this.za = Kg;
  this.Ia = 0;
  this.Sc = new ze;
  this.Cy = q
}
z = Jh.prototype;
z.za = p;
z.Ia = p;
z.ga = u("Ia");
z.Sc = p;
z.ej = u("Sc");
z.PC = p;
z.Cy = p;
z.bb = NaN;
z.ta = u("bb");
z.g = function(a) {
  C(a, "align") && (this.za = Lg(a));
  C(a, "padding") && (this.Ia = Qb(a, "padding"));
  if(C(a, "font")) {
    var b = F(a, "font");
    C(b, "size") && (b = J(b, "size"), Lb(a, "size"), this.PC = (this.Cy = Pg(b)) ? Qg(b) : 100 * Pb(b))
  }
  C(a, "value") && (this.bb = M(a, "value"));
  this.Sc = new Cf;
  this.Sc.g(a)
};
function Zh() {
}
A.e(Zh, Yh);
z = Zh.prototype;
z.Ub = p;
z.Lb = p;
z.Qb = function() {
  switch(this.Zc().$C()) {
    case 2:
      return new Mh;
    case 1:
      return new Qh
  }
};
z.Rb = x("color_range_style");
z.g = function(a, b, c) {
  Zh.f.g.call(this, a, b, c);
  C(a, "start") && (this.Ub = M(a, "start"));
  C(a, "end") && (this.Lb = M(a, "end"))
};
z.vq = function(a) {
  this.Gc(this.Ub, a);
  this.Ub = this.Ub > a.pa ? a.pa : this.Ub < a.la ? a.la : this.Ub;
  this.Gc(this.Lb, a);
  this.Lb = this.Lb > a.pa ? a.pa : this.Lb < a.la ? a.la : this.Lb
};
z.ta = function() {
  return(this.Ub + this.Lb) / 2
};
function $h() {
}
A.e($h, Yh);
z = $h.prototype;
z.bb = p;
z.ta = u("bb");
z.Rb = x("trendline_style");
z.Qb = function() {
  return new Th
};
z.g = function(a, b, c) {
  $h.f.g.call(this, a, b, c);
  C(a, "value") && (this.bb = M(a, "value"));
  (a = this.be().Lc()) && a.se() && this.Zc().Me(a.yb)
};
z.vq = function(a) {
  this.Gc(this.bb, a)
};
function Vh() {
  Ch.call(this)
}
A.e(Vh, Ch);
Vh.prototype.qd = p;
Vh.prototype.wl = p;
Vh.prototype.g = function(a) {
  Vh.f.g.call(this, a);
  var b;
  C(a, "length") && (b = J(a, "length"), this.Ar = (this.qd = Pg(b)) ? Qg(b) : Pb(b));
  C(a, "width") && (b = J(a, "width"), this.ab = (this.wl = Pg(b)) ? Qg(b) : Pb(b))
};
function ai() {
}
A.e(ai, Yh);
z = ai.prototype;
z.bb = p;
z.ta = u("bb");
z.Rb = x("custom_label_style");
z.Qb = function() {
  return new Xh
};
z.g = function(a, b) {
  ai.f.g.call(this, a, b);
  var c = this.K.wa.lh;
  c && bi(this.Zc(), c.md());
  C(a, "value") && (this.bb = M(a, "value"))
};
z.p = function(a) {
  ai.f.p.call(this, a);
  return this.D
};
z.vq = function(a) {
  this.Gc(this.bb, a)
};
function ci() {
  this.on = []
}
z = ci.prototype;
z.w = p;
z.vj = t("w");
z.La = p;
z.on = p;
z.D = p;
z.g = function(a, b, c) {
  var d, f, g;
  if(C(a, "color_ranges")) {
    f = I(F(a, "color_ranges"), "color_range");
    d = 0;
    for(g = f.length;d < g;d++) {
      Tb(f[d]) && di(this, new Zh, f[d], b, c)
    }
  }
  if(C(a, "trendlines")) {
    f = I(F(a, "trendlines"), "trendline");
    d = 0;
    for(g = f.length;d < g;d++) {
      Tb(f[d]) && di(this, new $h, f[d], b, c)
    }
  }
  if(C(a, "custom_labels")) {
    f = I(F(a, "custom_labels"), "custom_label");
    d = 0;
    for(g = f.length;d < g;d++) {
      Tb(f[d]) && di(this, new ai, f[d], b, c)
    }
  }
};
function di(a, b, c, d, f) {
  b.vj(a.w);
  b.g(c, d, f);
  b.vq(a.La);
  a.on.push(b)
}
z.p = function(a) {
  this.D = new W(a);
  for(var b = this.on.length, c = 0;c < b;c++) {
    this.D.ia(this.on[c].p(a))
  }
  return this.D
};
z.qa = function() {
  for(var a = this.on.length, b = 0;b < a;b++) {
    this.on[b].qa()
  }
};
z.Wa = function() {
  for(var a = this.on.length, b = 0;b < a;b++) {
    this.on[b].Wa()
  }
};
function ei(a) {
  this.Xb = a
}
z = ei.prototype;
z.Xb = p;
z.Zc = u("Xb");
z.Sa = p;
z.Sl = t("Sa");
z.zD = function() {
  return this.Sa != p && 0 < this.Sa.length
};
z.getName = u("Sa");
z.Fp = p;
z.Co = p;
z.enabled = j;
z.size = 0;
z.ec = u("size");
z.nv = p;
z.scale = p;
z.fa = u("scale");
z.fp = p;
z.op = p;
z.bf = p;
z.cd = p;
z.Wu = p;
z.Nu = p;
z.uk = p;
z.Sg = p;
z.p = function(a) {
  this.cd = new W(a);
  this.Fp && this.cd.ia(this.Fp.p(a));
  this.Co && this.cd.ia(this.Co.p(a));
  this.op && (this.Wu = new W(a), this.cd.ia(this.Wu));
  this.fp && (this.Nu = new W(a), this.cd.ia(this.Nu));
  this.bf && this.bf.isEnabled() && (this.uk = new W(a), this.cd.ia(this.uk));
  this.Sg && this.cd.ia(this.Sg.p(a));
  return this.cd
};
z.jr = s();
z.qa = function() {
  this.Fp && this.Fp.qa();
  this.Co && this.Co.qa();
  this.op && this.tC();
  this.fp && this.sC();
  if(this.bf && this.bf.isEnabled()) {
    for(var a = this.scale.fc, a = this.bf.Jv ? a : a - 1, b = this.cd.r(), c = this.bf.Iv ? 0 : 1;c <= a;c++) {
      this.bm = this.scale.Mc(c);
      var d = this.bf;
      d.Sc.uc(b, d.yc.ta(d.w, p));
      d = this.Jg(this.scale.rd(this.bm));
      this.uk.ia(this.bf.qa(b, d))
    }
  }
  this.Sg && this.Sg.qa()
};
z.tC = function() {
  for(var a = this.scale.AE(this.scale.la), b = this.scale.AE(this.scale.pa), c = 0;c <= this.scale.fc;c++) {
    for(var d = this.scale.Mc(c), f = 1;f < this.scale.Jf;f++) {
      var g = this.scale.jk(d, f);
      g < a || g > b || this.Wu.ia(this.zo(this.op, this.scale.rd(g)))
    }
  }
};
z.sC = function() {
  for(var a = 0;a <= this.scale.fc;a++) {
    this.Nu.ia(this.zo(this.fp, this.scale.rd(this.scale.Mc(a))))
  }
};
z.Na = function(a) {
  if("%Value" == a) {
    return this.scale.dC(this.bm)
  }
};
z.Pa = function(a) {
  return"%Value" == a ? 2 : 4
};
z.Wa = function() {
  this.jr();
  this.Nu && this.Nu.clear();
  this.Wu && this.Wu.clear();
  this.uk && this.uk.clear();
  this.qa();
  this.Sg && this.Sg.Wa()
};
z.g = function(a, b) {
  if(a) {
    if(C(a, "enabled") && (this.enabled = K(a, "enabled")), C(a, "size") && (this.size = Qb(a, "size")), C(a, "scale") ? (this.scale = C(F(a, "scale"), "type") && "logarithmic" == N(F(a, "scale"), "type") ? new Bh : new Ah, this.scale.g(F(a, "scale"))) : this.scale = new Ah, this.enabled) {
      C(a, "scale_bar") && (this.Fp = new fi(this, q), this.Fp.g(F(a, "scale_bar"), this.size), bi(this.Xb, this.Fp.md())), C(a, "scale_line") && (this.Co = new fi(this, j), this.Co.g(F(a, "scale_line"), this.size), bi(this.Xb, this.Co.md())), C(a, "major_tickmark") && (this.fp = new Ch, this.fp.g(F(a, "major_tickmark"), this.size), bi(this.Xb, this.fp.md())), C(a, "minor_tickmark") && (this.op = new Ch, this.op.g(F(a, "minor_tickmark"), this.size), bi(this.Xb, this.op.md())), this.bf = new gi(this), 
      C(a, "labels") && this.bf.g(F(a, "labels"), this.size), this.Sg = new ci, this.Sg.vj(this), this.Sg.La = this.scale, this.Sg.g(a, b, this.size)
    }
  }else {
    this.scale = new Ah
  }
};
z.Zw = function(a) {
  hi(this, a.ta());
  a.zM && hi(this, a.zM())
};
function hi(a, b) {
  if(isNaN(a.scale.ld) || b < a.scale.ld) {
    a.scale.ld = b
  }
  if(isNaN(a.scale.kd) || b > a.scale.kd) {
    a.scale.kd = b
  }
}
function fi(a, b) {
  this.w = a;
  this.eI = b;
  this.Y = new Od
}
z = fi.prototype;
z.w = p;
z.eI = q;
z.Yd = 1;
z.ol = u("Yd");
z.ke = 0.05;
z.ec = u("ke");
z.Ia = 0;
z.ga = u("Ia");
z.za = Kg;
z.Y = p;
z.md = u("Y");
z.D = p;
z.p = function(a) {
  this.D = new Nd(a, a.ja(), a.ja());
  this.D.I.setAttribute("id", "scaleBar");
  return this.D
};
z.qa = function() {
  var a = this.w.lD(this), b = this.D.r(), c = "", c = Td(this.Y) ? c + Kd(this.Y.mc(), b, a) : c + md(), c = Sd(this.Y) ? c + Md(this.Y.Lc(), b, a) : c + nd();
  this.D.Hg.setAttribute("style", c);
  c = "";
  c = this.Y.Pc && this.Y.Pc.isEnabled() ? c + Hc(this.Y.De(), b, a) : c + od();
  this.D.Mg.setAttribute("style", c);
  a = this.w.DI(b, this);
  this.D.Hg.setAttribute("d", a);
  this.D.Mg.setAttribute("d", a)
};
z.g = function(a, b) {
  if(this.eI) {
    this.Yd = 0, Td(this.Y) && this.Y.mc().yg(q), this.Y.g({border:a})
  }else {
    if(C(a, "shape")) {
      switch(N(a, "shape")) {
        case "line":
          this.Yd = 0;
          break;
        case "bar":
          this.Yd = 1
      }
    }
    this.Y.g(a)
  }
  C(a, "size") && (this.ke = Og(a, "size", b));
  C(a, "padding") && (this.Ia = Qb(a, "padding"));
  C(a, "align") && (this.za = Lg(a))
};
function gi(a) {
  this.Sc = new ze;
  this.Sc.yg(q);
  this.w = a
}
z = gi.prototype;
z.za = 0;
z.Ia = 0.02;
z.ga = u("Ia");
z.Iv = j;
z.Jv = j;
z.w = p;
z.Sc = p;
z.ej = u("Sc");
z.isEnabled = function() {
  return this.Sc && this.Sc.isEnabled()
};
z.yc = p;
z.g = function(a, b) {
  if(C(a, "font")) {
    var c = F(a, "font");
    C(c, "size") && H(c, "size", "" + 100 * Og(c, "size", b))
  }
  this.Sc.g(a);
  C(a, "enabled") && this.Sc.yg(K(a, "enabled"));
  C(a, "format") && (this.yc = new je(J(a, "format")));
  C(a, "align") && (this.za = Lg(a));
  C(a, "padding") && (this.Ia = Qb(a, "padding"));
  C(a, "show_first") && (this.Iv = K(a, "show_first"));
  C(a, "show_last") && (this.Jv = K(a, "show_last"))
};
z.qa = function(a, b) {
  var c = this.Sc.jc(a);
  c.qb(b.x, b.y);
  return c
};
function ii() {
  Dg.call(this);
  this.Oj = [];
  this.rB = {};
  this.zi = [];
  this.jK = {};
  this.Ea = new yg;
  this.Ea.Zz(this);
  this.Fc = new Cg
}
A.e(ii, Dg);
z = ii.prototype;
z.Oj = p;
z.rB = p;
z.zi = p;
z.jK = p;
z.Ea = p;
z.Fc = p;
z.mb = function(a) {
  a = ii.f.mb.call(this, a);
  a.zi = [];
  for(var b = 0;b < this.zi.length;b++) {
    a.zi.push(this.zi[b].mb())
  }
  return a
};
function ji(a, b) {
  a.Oj.push(b);
  b.zD() && (a.rB[b.getName()] = b)
}
z.g = function(a) {
  ii.f.g.call(this, a);
  var b, c;
  b = this.TB(F(a, "axis"), this.tf);
  b.Sl("default");
  b.g(F(a, "axis"), this.tf);
  ji(this, b);
  var d = b;
  if(C(a, "extra_axes")) {
    var f = I(F(a, "extra_axes"), "axis");
    for(c = 0;c < f.length;c++) {
      b = this.TB(f[c], this.tf), b.g(f[c], this.tf), C(f[c], "name") && b.Sl(N(f[c], "name")), ji(this, b)
    }
  }
  if(C(a, "pointers")) {
    c = F(a, "pointers");
    this.Ea.Ue(F(c, "label"), this.tf);
    this.Fc.Ue(F(c, "tooltip"), this.tf);
    a = p;
    b = -1;
    var f = q, g = j;
    C(c, "color") && (a = Yb(c));
    C(c, "hatch_type") && (b = Gc(N(c, "hatch_type")));
    C(c, "allow_select") && (f = K(c, "allow_select"));
    C(c, "use_hand_cursor") && (g = K(c, "use_hand_cursor"));
    var k = I(c, "pointer");
    for(c = 0;c < k.length;c++) {
      var l = k[c];
      if(C(l, "value") || C(l, "start") && C(l, "end")) {
        var n = this.tH(Wb(l, "type"));
        if(n) {
          n.Zz(this);
          n.xv = f;
          n.Un = g;
          var m = this.Ea.copy();
          n.Ea = m;
          m = this.Fc.copy();
          n.Fc = m;
          a && n.Di(a);
          n.Ip(b);
          n.g(l, this.tf);
          C(l, "axis") ? (l = this.rB[N(l, "axis")]) ? n.vj(l) : n.vj(d) : n.vj(d);
          this.zi.push(n);
          n.zD() && (this.jK[n.getName()] = n);
          this.Zw(n);
          n.w.Zw(n)
        }
      }
    }
  }
};
z.Zw = s();
z.p = function(a, b, c) {
  ii.f.p.call(this, a, b, c);
  b = 0;
  for(c = this.Oj.length;b < c;b++) {
    this.xB.ia(this.Oj[b].p(this.Bg))
  }
  b = 0;
  for(c = this.zi.length;b < c;b++) {
    this.hx.ia(this.zi[b].p(a))
  }
  b = 0;
  for(c = this.Oj.length;b < c;b++) {
    this.Oj[b].jr()
  }
  return this.cd
};
z.qa = function() {
  ii.f.qa.call(this);
  var a, b;
  a = 0;
  for(b = this.Oj.length;a < b;a++) {
    this.Oj[a].qa()
  }
  a = 0;
  for(b = this.zi.length;a < b;a++) {
    this.zi[a].qa()
  }
};
z.Wa = function(a) {
  ii.f.Wa.call(this, a);
  var b, a = 0;
  for(b = this.Oj.length;a < b;a++) {
    this.Oj[a].Wa()
  }
  a = 0;
  for(b = this.zi.length;a < b;a++) {
    this.zi[a].Wa()
  }
};
function bi(a, b) {
  b && (Td(b) && b.mc().yb && a.Me(b.mc().yb), Sd(b) && b.Lc().yb && a.Me(b.Lc().yb))
}
z.Me = s();
function ki(a) {
  lg.call(this, a)
}
A.e(ki, lg);
z = ki.prototype;
z.lp = NaN;
z.Rh = NaN;
z.SD = NaN;
z.Cw = function(a) {
  var b = 0;
  this.Uc && this.Uc.re() && (b = this.Uc.Va());
  var c = 0;
  this.Vd && this.Vd.re() && (c = this.Vd.Va());
  this.lp = Math.min(a.width, a.height);
  this.SD = b;
  this.Rh = b + c;
  b = (b + c + this.padding) * this.lp;
  a.Ca();
  a.bh(a.tb() + b);
  a.eh(a.Ja() - b);
  a.fh(a.rb() + b);
  a.$g(a.ra() - b)
};
z.rH = function() {
  return new li(this)
};
z.sH = function() {
  return new mi(this)
};
z.nH = function() {
  return new ni(this)
};
z.ja = function(a, b) {
  var c;
  c = this.zm ? this.zm.Ca() : new Dd;
  var d = this.lp / 100;
  c.P *= d;
  c.sa *= d;
  c.ha *= d;
  c.X *= d;
  return Ed(c, a, b)
};
function oi(a) {
  this.frame = a
}
A.e(oi, fg);
oi.prototype.frame = p;
oi.prototype.oC = function(a, b, c, d) {
  var f = p, f = p, g = this.Wt(a, d);
  this.mc() && this.mc().isEnabled() ? (f = Kd(this.mc(), a, d), f += "fill-rule: evenodd") : f = md();
  b.setAttribute("d", g);
  b.setAttribute("style", f);
  b.setAttribute("fill-rule", "evenodd");
  this.De() && this.De().isEnabled() ? (f = Hc(this.De(), a, d), f += "fill-rule: evenodd") : f = od();
  c.setAttribute("d", g);
  c.setAttribute("style", f);
  c.setAttribute("fill-rule", "evenodd")
};
function pi(a, b) {
  var c = b.Ca(), d = a.uI();
  c.x += d;
  c.y += d;
  c.width -= 2 * d;
  c.height -= 2 * d;
  return c
}
function qi(a, b) {
  var c = b.Ca(), d = a.zI();
  c.x += d;
  c.y += d;
  c.width -= 2 * d;
  c.height -= 2 * d;
  return c
}
function hg(a, b, c) {
  c = pi(a, c);
  return a.frame.ja(b, c, j, j)
}
function ig(a, b, c) {
  c = qi(a, c);
  return a.frame.ja(b, c, j, j)
}
oi.prototype.Wt = function(a, b) {
  var c = pi(this, b), d = qi(this, b);
  return this.frame.ja(a, d, j, q) + " " + this.frame.ja(a, c, q, j)
};
function li(a) {
  this.frame = a
}
A.e(li, oi);
li.prototype.uI = function() {
  return-this.frame.padding * this.frame.lp
};
li.prototype.zI = function() {
  return-(this.frame.SD + this.frame.padding) * this.frame.lp
};
function mi(a) {
  this.frame = a
}
A.e(mi, oi);
mi.prototype.uI = function() {
  return-(this.frame.SD + this.frame.padding) * this.frame.lp
};
mi.prototype.zI = function() {
  return-(this.frame.Rh + this.frame.padding) * this.frame.lp
};
function ni(a) {
  this.Oc = a
}
A.e(ni, Ud);
ni.prototype.Oc = p;
ni.prototype.createElement = function(a) {
  return a.ja()
};
ni.prototype.yh = function(a, b) {
  return this.Oc.ja(a, b)
};
function ri() {
  Jg.call(this)
}
A.e(ri, Jg);
z = ri.prototype;
z.ab = p;
z.Ka = u("ab");
z.qd = p;
z.fs = p;
z.g = function(a) {
  a = ri.f.g.call(this, a);
  if(C(a, "width")) {
    var b = J(a, "width");
    this.ab = (this.qd = Pg(b)) ? Qg(b) : Pb(b)
  }
  C(a, "start_from_zero") && (this.fs = K(a, "start_from_zero"));
  return a
};
function si() {
}
A.e(si, Pf);
si.prototype.update = function(a, b) {
  b && this.j.hd();
  si.f.update.call(this, a, b)
};
si.prototype.createElement = function() {
  return this.j.r().ja()
};
si.prototype.Ta = function(a) {
  a.setAttribute("d", gd(this.j.n()))
};
function ti() {
  $.call(this)
}
A.e(ti, Rf);
ti.prototype.cc = function() {
  return ri
};
ti.prototype.lc = function() {
  return new si
};
function ui(a) {
  this.ob = this.ab = 0.1;
  this.Yd = 108;
  this.za = Kg;
  this.Ia = 0;
  a && (this.Y = new Od);
  this.wl = this.qd = q;
  this.Bb = 0;
  this.co = j
}
z = ui.prototype;
z.za = p;
z.Ia = p;
z.ga = u("Ia");
z.ab = p;
z.Ka = u("ab");
z.qd = p;
z.ob = p;
z.vb = u("ob");
z.wl = p;
z.Yd = p;
z.Bb = p;
z.co = p;
z.Y = p;
z.md = u("Y");
z.g = function(a) {
  C(a, "shape") && (this.Yd = jg(Wb(a, "shape")));
  C(a, "align") && (this.za = Lg(a));
  C(a, "padding") && (this.Ia = Qb(a, "padding"));
  C(a, "rotation") && (this.Bb = M(a, "rotation"));
  C(a, "auto_rotate") && (this.co = K(a, "auto_rotate"));
  var b;
  C(a, "width") && (b = J(a, "width"), this.ab = (this.qd = Pg(b)) ? Qg(b) : Pb(b));
  C(a, "height") && (b = J(a, "height"), this.ob = (this.wl = Pg(b)) ? Qg(b) : Pb(b));
  105 == this.Yd || 106 == this.Yd || 107 == this.Yd || 108 == this.Yd || (this.ab = this.ob = Math.min(this.ab, this.ob));
  this.Y && this.Y.g(a)
};
function vi() {
  Jg.call(this);
  this.gb = new ui(q)
}
A.e(vi, Jg);
vi.prototype.gb = p;
vi.prototype.g = function(a) {
  a = vi.f.g.call(this, a);
  this.gb.g(a);
  return a
};
function wi() {
  this.Mn = new kg
}
A.e(wi, Pf);
z = wi.prototype;
z.Mn = p;
z.fb = function() {
  return this.j.sd
};
z.update = function(a, b) {
  this.j.hd();
  wi.f.update.call(this, a, b)
};
z.createElement = function() {
  return this.j.r().ja()
};
z.Ta = function(a) {
  a.setAttribute("d", this.Mn.Io(this.j.ya.gb.Yd, this.j.r(), Math.min(this.j.n().width, this.j.n().height), this.j.gp.x, this.j.gp.y, this.j.n().width, this.j.n().height, this.j.ya.Md() ? this.j.ya.Lc().Va() : 1))
};
function xi() {
  $.call(this)
}
A.e(xi, Rf);
xi.prototype.cc = function() {
  return vi
};
xi.prototype.lc = function() {
  return new wi
};
function yi() {
  Jg.call(this);
  this.mo = 0.15;
  this.Tw = 0;
  this.Ro = this.qd = j;
  this.mo = this.ab = 1
}
A.e(yi, Jg);
z = yi.prototype;
z.ab = p;
z.Ka = u("ab");
z.qd = p;
z.mo = p;
z.Ro = p;
z.Tw = p;
z.WG = p;
z.g = function(a) {
  var a = yi.f.g.call(this, a), b;
  C(a, "width") && (b = J(a, "width"), this.ab = (this.qd = Pg(b)) ? Qg(b) : Ob(b) / 100);
  C(a, "bulb_radius") && (b = J(a, "bulb_radius"), this.mo = (this.Ro = Pg(b)) ? Qg(b) : Ob(b) / 100);
  C(a, "bulb_padding") && (this.Tw = Qb(a, "bulb_padding"));
  Ub(a, "blub") && (this.WG = new Od, this.WG.g(F(a, "blub")));
  return a
};
function zi() {
}
A.e(zi, Pf);
zi.prototype.update = function(a, b) {
  if(b) {
    var c = this.j, d = c.ya, f = (d.qd ? c.w.ec() : 1) * d.Ka(), g = (d.Ro ? c.w.ec() : 1) * d.mo, k = Lh(c.w, d.za, d.ga(), f, j), l = Lh(c.w, d.za, d.ga(), f, q), f = c.w.fa().pc(c.w.fa().la), n = c.w.fa().pc(Ng(c, c.bb)), m = c.w.qj, g = m * g, d = m * d.Tw, m = f, o = new P;
    c.w.Zc().Vf() ? (o.y = (k + l) / 2 - g, o.height = 2 * g, c.w.fa().Nb() ? (o.x = n, o.width = m + 2 * g + d - o.x) : (o.x = m - 2 * g - d, o.width = n - o.x)) : (o.x = (k + l) / 2 - g, o.width = 2 * g, c.w.fa().Nb() ? (o.y = n, o.height = m + 2 * g - o.y) : (o.y = m - 2 * g, o.height = n - o.y));
    f = c.w.fa().Nb() ? f + d : f - d;
    c.Oc.Vf() ? (d = new O(n, k), n = new O(n, l), l = new O(f, l), k = new O(f, k)) : (d = new O(k, n), n = new O(l, n), l = new O(l, f), k = new O(k, f));
    f = S(d.x, d.y);
    f += U(n.x, n.y);
    f += U(l.x, l.y);
    f += c.r().ub(g, 1, 1, k.x, k.y, g);
    f += U(k.x, k.y);
    f += U(d.x, d.y);
    c.k.x = Math.min(d.x, n.x, l.x, k.x);
    c.k.y = Math.min(d.y, n.y, l.y, k.y);
    c.k.width = Math.max(d.x, n.x, l.x, k.x) - c.k.x;
    c.k.height = Math.max(d.y, n.y, l.y, k.y) - c.k.y;
    g = f;
    c.NK = g
  }
  zi.f.update.call(this, a, b)
};
zi.prototype.createElement = function() {
  return this.j.r().ja()
};
zi.prototype.Ta = function(a) {
  a.setAttribute("d", this.j.NK)
};
function Ai() {
  $.call(this)
}
A.e(Ai, Rf);
Ai.prototype.cc = function() {
  return yi
};
Ai.prototype.lc = function() {
  return new zi
};
function Bi() {
  Jg.call(this);
  this.ab = 0.1;
  this.qd = q
}
A.e(Bi, Jg);
z = Bi.prototype;
z.ab = p;
z.Ka = u("ab");
z.qd = p;
z.Xa = "%color";
z.DB = p;
z.Rj = p;
z.Si = p;
z.zB = p;
z.Qj = p;
z.Ri = p;
z.Hv = p;
z.g = function(a) {
  a = Bi.f.g.call(this, a);
  if(C(a, "width")) {
    var b = J(a, "width");
    this.ab = (this.qd = Pg(b)) ? Qg(b) : Pb(b)
  }
  C(a, "color") && (this.Xa = N(a, "color"));
  this.DB = Ci("0xFFFFFF", 0.3);
  this.zB = Ci(this.Xa, 1);
  this.Hv = new Ld;
  this.Hv.g({thickness:"3", color:"0xFFFFFF", opacity:"0.3"});
  this.Rj = Di(0.3);
  this.Qj = Di(1);
  this.Si = Ei("0xFFFFFF", 0.3);
  this.Ri = Ei(this.Xa, 1);
  return a
};
function Ci(a, b) {
  var c = "DarkColor(" + a + ")", d = "LightColor(" + a + ")", f = new Jd;
  f.g({type:"gradient", gradient:{key:[{color:c, position:"0", opacity:b}, {color:c, position:"0.05", opacity:b}, {color:d, position:"0.85", opacity:b}, {color:d, position:"0.85", opacity:b}, {color:c, position:"1", opacity:b}]}});
  return f
}
function Di(a) {
  var b = new Jd;
  b.g({type:"gradient", gradient:{key:[{color:"0xFFFFFF", position:"0", opacity:0}, {color:"0xFFFFFF", position:"0.2", opacity:Number(160 / 255) * a}, {color:"0xFFFFFF", position:"0.25", opacity:Number(140 / 255) * a}, {color:"0xFFFFFF", position:"0.3", opacity:Number(30 / 255) * a}, {color:"0xFFFFFF", position:"0.35", opacity:0}, {color:"0xFFFFFF", position:"1", opacity:0}]}});
  return b
}
function Ei(a, b) {
  var c = new Jd;
  c.g({type:"gradient", gradient:{key:[{color:"0xFFFFFF", position:"0", opacity:0.1 * b}, {color:"DarkColor(" + a + ")", position:"1", opacity:b}]}});
  return c
}
function Fi() {
  Bi.call(this)
}
A.e(Fi, Bi);
Fi.prototype.g = function(a) {
  a = Fi.f.g.call(this, a);
  this.zB.yb.Wd(-90);
  this.DB.yb.Wd(-90);
  this.Qj.yb.Wd(-90);
  this.Rj.yb.Wd(-90);
  this.Ri.yb.Wd(140);
  this.Si.yb.Wd(140);
  return a
};
function Gi() {
  Bi.call(this)
}
A.e(Gi, Bi);
Gi.prototype.g = function(a) {
  a = Gi.f.g.call(this, a);
  this.Ri.yb.Wd(50);
  this.Si.yb.Wd(50);
  return a
};
function Hi() {
}
A.e(Hi, Re);
z = Hi.prototype;
z.gq = p;
z.Si = p;
z.Rj = p;
z.hq = p;
z.dq = p;
z.Ri = p;
z.Qj = p;
z.eq = p;
z.fq = p;
z.Yc = function() {
  this.gq = new Pe;
  this.gq.p(this.j, this, this.el);
  this.gq.Xq = function(a) {
    return a.DB
  };
  this.gq.Ta = function() {
    this.I.setAttribute("d", this.j.cu(this.j.nm))
  };
  this.hq = new Oe;
  this.hq.p(this.j, this, this.el);
  this.hq.qD = function(a) {
    return a.Hv
  };
  this.hq.Ta = function() {
    this.I.setAttribute("d", this.j.sD(this.j.nm))
  };
  this.Rj = new Pe;
  this.Rj.p(this.j, this, this.el);
  this.Rj.Xq = function(a) {
    return a.Rj
  };
  this.Rj.Ta = function() {
    this.I.setAttribute("d", this.j.cu(this.j.nm))
  };
  this.Si = new Pe;
  this.Si.p(this.j, this, this.el);
  this.Si.Xq = function(a) {
    return a.Si
  };
  this.Si.Ta = function() {
    this.I.setAttribute("d", this.j.rD(this.j.nm))
  };
  this.dq = new Pe;
  this.dq.p(this.j, this, this.el);
  this.dq.Xq = function(a) {
    return a.zB
  };
  this.dq.Ta = function() {
    this.I.setAttribute("d", this.j.cu(this.j.Tk))
  };
  this.eq = new Oe;
  this.eq.p(this.j, this, this.el);
  this.eq.qD = function(a) {
    return a.Hv
  };
  this.eq.Ta = function() {
    this.I.setAttribute("d", this.j.sD(this.j.Tk))
  };
  this.Qj = new Pe;
  this.Qj.p(this.j, this, this.el);
  this.Qj.Xq = function(a) {
    return a.Qj
  };
  this.Qj.Ta = function() {
    this.I.setAttribute("d", this.j.cu(this.j.Tk))
  };
  this.fq = new Oe;
  this.fq.p(this.j, this, this.el);
  this.fq.qD = function(a) {
    return a.Hv
  };
  this.fq.Ta = function() {
    this.I.setAttribute("d", this.j.mI(this.j.Tk))
  };
  this.Ri = new Pe;
  this.Ri.p(this.j, this, this.el);
  this.Ri.Xq = function(a) {
    return a.Ri
  };
  this.Ri.Ta = function() {
    this.I.setAttribute("d", this.j.rD(this.j.Tk))
  }
};
z.el = function() {
  return this.j.r().ja()
};
z.ff = function() {
  this.j.w.fa().Nb() ? (Ii(this), Ji(this)) : (Ji(this), Ii(this))
};
function Ji(a) {
  var b = a.j.fb();
  b.appendChild(a.dq.I);
  b.appendChild(a.eq.I);
  b.appendChild(a.fq.I);
  b.appendChild(a.Qj.I);
  b.appendChild(a.Ri.I)
}
function Ii(a) {
  var b = a.j.fb();
  b.appendChild(a.gq.I);
  b.appendChild(a.hq.I);
  b.appendChild(a.Rj.I);
  b.appendChild(a.Si.I)
}
z.update = function(a, b) {
  b && this.j.hd();
  this.gq.update(a, b);
  this.hq.update(a, b);
  this.Rj.update(a, b);
  this.Si.update(a, b);
  this.dq.update(a, b);
  this.eq.update(a, b);
  this.Qj.update(a, b);
  this.Ri.update(a, b);
  this.fq.update(a, b)
};
function Ki() {
}
A.e(Ki, Hi);
Ki.prototype.ff = function() {
  this.j.w.fa().Nb() ? (Ii(this), Ji(this)) : (Ji(this), Ii(this))
};
function Li() {
}
A.e(Li, Hi);
Li.prototype.ff = function() {
  this.j.w.fa().Nb() ? (Ji(this), Ii(this)) : (Ii(this), Ji(this))
};
function Mi() {
  $.call(this)
}
A.e(Mi, $);
Mi.prototype.cc = function() {
  return Fi
};
Mi.prototype.lc = function() {
  return new Ki
};
function Ni() {
  $.call(this)
}
A.e(Ni, $);
Ni.prototype.cc = function() {
  return Gi
};
Ni.prototype.lc = function() {
  return new Li
};
function Oi() {
  Mg.call(this)
}
A.e(Oi, Mg);
Oi.prototype.p = function(a) {
  Oi.f.p.call(this, a);
  this.OB();
  return this.D
};
Oi.prototype.OB = s();
function Pi() {
  Mg.call(this)
}
A.e(Pi, Oi);
z = Pi.prototype;
z.Rb = x("bar_pointer_style");
z.Qb = function() {
  return new ti
};
z.p = function(a) {
  Pi.f.p.call(this, a);
  return this.D
};
z.hd = function() {
  var a = this.sI(), b = (this.ya.qd ? this.w.ec() : 1) * this.ya.Ka(), c = Lh(this.w, this.ya.za, this.ya.ga(), b, j), b = Lh(this.w, this.ya.za, this.ya.ga(), b, q), d = this.w.scale.pc(this.FI()), a = this.w.scale.pc(a);
  this.w.Zc().Vf() ? (this.k.y = Math.min(c, b), this.k.height = Math.max(c, b) - this.k.y, this.k.x = Math.min(d, a), this.k.width = Math.max(d, a) - this.k.x) : (this.k.x = Math.min(c, b), this.k.width = Math.max(c, b) - this.k.x, this.k.y = Math.min(d, a), this.k.height = Math.max(d, a) - this.k.y)
};
z.FI = function() {
  return this.ya.fs ? 0 : this.w.fa().la
};
z.sI = function() {
  return Ng(this, this.bb)
};
function Qi() {
  Mg.call(this)
}
A.e(Qi, Pi);
z = Qi.prototype;
z.Rd = p;
z.Rb = x("range_bar_pointer_style");
z.g = function(a, b) {
  Qi.f.g.call(this, a, b);
  C(a, "start") && (this.bb = M(a, "start"));
  C(a, "end") && (this.Rd = M(a, "end"))
};
z.FI = function() {
  return Ng(this, this.bb)
};
z.sI = function() {
  return Ng(this, this.Rd)
};
z.Na = function(a) {
  if("%Value" == a) {
    return this.Rd - this.bb
  }
  if("%StartValue" == a) {
    return this.bb
  }
  if("%EndValue" == a) {
    return this.Rd
  }
};
z.Pa = x(2);
function Ri() {
  Mg.call(this)
}
A.e(Ri, Oi);
z = Ri.prototype;
z.gp = p;
z.sd = p;
z.Rb = x("marker_pointer_style");
z.Qb = function() {
  return new xi
};
z.p = function(a) {
  this.sd = new W(a);
  Ri.f.p.call(this, a);
  this.D.ia(this.sd);
  return this.D
};
z.hd = function() {
  var a = this.ya.gb, b = this.w.qj, c = (a.wl ? this.w.ec() : 1) * a.vb() * b, d = (a.qd ? this.w.ec() : 1) * a.Ka() * b, b = this.Oc.Vf(), f = this.w.Ib(this.ya.za, this.ya.ga(), 0, q), g = new O;
  this.w.Vl(f, this.w.fa().pc(Ng(this, this.bb)), g);
  g.x -= c / 2;
  g.y -= d / 2;
  this.k.x = -c / 2;
  this.k.y = -d / 2;
  this.k.width = c;
  this.k.height = d;
  this.gp = new O(-c / 2, -d / 2);
  f = g.Ca();
  if(b) {
    this.sd.gd(g.y + d / 2);
    switch(this.ya.za) {
      case 0:
        var k = this.sd;
        k.ma -= d / 2;
        pd(k);
        break;
      case 1:
        k = this.sd, k.ma += d / 2, pd(k)
    }
    this.sd.Xd(g.x + c / 2)
  }else {
    this.sd.Xd(g.x + c / 2);
    switch(this.ya.za) {
      case 0:
        k = this.sd;
        k.Vb -= c / 2;
        pd(k);
        break;
      case 1:
        qd(this.sd, c / 2)
    }
    this.sd.gd(g.y + d / 2)
  }
  c = 0;
  if(a.co) {
    switch(this.ya.za) {
      case 0:
      ;
      case Kg:
        c = b ? -180 : 90;
        break;
      case 1:
        c = b ? 0 : -90
    }
  }
  this.sd.Wl(c + a.Bb);
  this.k.x = f.x;
  this.k.y = f.y
};
function Si() {
  Mg.call(this)
}
A.e(Si, Oi);
Si.prototype.Rb = x("thermometer_pointer_style");
Si.prototype.Qb = function() {
  return new Ai
};
Si.prototype.NK = p;
Si.prototype.OB = function() {
  var a = this.K.wa;
  Ti(this, this.K.wa);
  Ti(this, this.K.dc);
  Ti(this, this.K.kc);
  Ti(this, this.K.Zb);
  Ti(this, this.K.gc);
  a = 2 * (a.Ro ? this.w.ec() : 1) * a.mo + a.Tw;
  this.w.fa().Nb() ? this.Oc.bj = Math.max(this.Oc.bj, a) : this.Oc.Dj = Math.max(this.Oc.Dj, a)
};
function Ti(a, b) {
  var c = (b.qd ? a.w.ec() : 1) * b.Ka();
  if((b.Ro ? a.w.ec() : 1) * b.mo < c / 2) {
    b.mo = c / 2 / (b.Ro ? a.w.ec() : 1)
  }
}
function Ui() {
  Mg.call(this)
}
A.e(Ui, Oi);
Ui.prototype.Rb = x("tank_pointer_style");
Ui.prototype.nm = p;
Ui.prototype.Tk = p;
Ui.prototype.OB = function() {
  var a = 0.1488095238 * (this.ya.qd ? this.w.ec() : 1) * this.ya.Ka();
  this.Oc.bj = Math.max(this.Oc.bj, a);
  this.Oc.Dj = Math.max(this.Oc.Dj, a)
};
function Vi() {
  Mg.call(this)
}
A.e(Vi, Ui);
z = Vi.prototype;
z.Qb = function() {
  return new Mi
};
z.hd = function() {
  var a = (this.ya.qd ? this.w.ec() : 1) * this.ya.Ka(), b = Lh(this.w, this.ya.za, this.ya.ga(), a, j), c = this.w.fa().pc(this.w.fa().la), d = this.w.fa().pc(this.w.fa().pa), f = this.w.fa().pc(this.bb), a = a * this.w.qj;
  this.w.fa().Nb() ? (this.k = new P(f, b, c - f, a), this.Tk = new P(f, b, c - f, a), this.nm = new P(d, b, f - d, a)) : (this.k = new P(c, b, f - c, a), this.Tk = new P(c, b, f - c, a), this.nm = new P(f, b, d - f, a))
};
z.cu = function(a) {
  var b = this.r(), c = 0.1488095238 * a.height, d = a.height / 2, f = a.y + a.height / 2 + Vc(d, 90), g = S(a.x + Uc(c, 90), f), g = g + b.ub(c, 1, 1, a.x, a.y, d), g = g + U(a.x + a.width, a.y);
  return g += b.ub(c, 0, 1, a.x + a.width, f, d)
};
z.sD = function(a) {
  var b = 0.1488095238 * a.height, c = a.height / 2, d = a.y + a.height / 2 + Vc(c, 90), f = S(a.x + a.width + 1.5, a.y + 1);
  return f += this.r().ub(b, 1, 0, a.x + a.width + 1.5, d, c)
};
z.mI = function(a) {
  var b = 0.1488095238 * a.height, c = a.height / 2, d = a.y + a.height / 2 + Vc(c, 90), f = S(a.x + 1, a.y);
  return f += this.r().ub(b, 0, 0, a.x, d, c)
};
z.rD = function(a) {
  var b = S(a.x + a.width - 1, a.y);
  return b += this.r().ub(0.1488095238 * a.height, 1, 0, a.x + a.width - 1 + 0.1, a.y, a.height / 2)
};
function Wi() {
  Mg.call(this)
}
A.e(Wi, Ui);
z = Wi.prototype;
z.Qb = function() {
  return new Ni
};
z.hd = function() {
  var a = (this.ya.qd ? this.w.ec() : 1) * this.ya.Ka(), b = Lh(this.w, this.ya.za, this.ya.ga(), a, j), c = this.w.fa().pc(this.w.fa().la), d = this.w.fa().pc(this.w.fa().pa), f = this.w.fa().pc(this.bb), a = a * this.w.qj;
  this.w.fa().Nb() ? (this.k = new P(b, f, a, c - f), this.Tk = new P(b, f, a, c - f), this.nm = new P(b, d, a, f - d)) : (this.k = new P(b, c, a, f - c), this.Tk = new P(b, c, a, f - c), this.nm = new P(b, f, a, d - f))
};
z.cu = function(a) {
  var b = this.r(), c = S(a.x, a.y), c = c + b.ub(a.width / 2, 0, 1, a.x + a.width, a.y, 0.1488095238 * a.width), c = c + U(a.x + a.width, a.y + a.height);
  return c += b.ub(a.width / 2, 0, 1, a.x, a.y + a.height, 0.1488095238 * a.width)
};
z.sD = function(a) {
  var b = S(a.x, a.y);
  return b += this.r().ub(a.width / 2, 1, 0, a.x, a.y - 0.1, 0.1488095238 * a.width)
};
z.mI = function(a) {
  var b = S(a.x, a.y + a.height - 1);
  return b += this.r().ub(a.width / 2, 0, 0, a.x + a.width, a.y + a.height, 0.1488095238 * a.width)
};
z.rD = function(a) {
  var b = S(a.x, a.y);
  return b += this.r().ub(a.width / 2, 1, 0, a.x, a.y - 0.1, 0.1488095238 * a.width)
};
function Xi() {
  ii.call(this);
  this.ny = q;
  this.bj = this.Dj = 0
}
A.e(Xi, ii);
z = Xi.prototype;
z.ny = q;
z.Vf = u("ny");
z.Dj = 0;
z.bj = 0;
z.p = function(a, b, c) {
  Xi.f.p.call(this, a, b, c);
  return this.cd
};
z.g = function(a) {
  C(a, "orientation") && (this.ny = "horizontal" == N(a, "orientation"));
  Xi.f.g.call(this, a)
};
z.$C = x(2);
z.qH = function() {
  return new ki(this)
};
z.TB = function() {
  return new Yi(this)
};
z.tH = function(a) {
  switch(a) {
    case "bar":
      return new Pi;
    case "rangebar":
      return new Qi;
    case "marker":
      return new Ri;
    case "tank":
      return this.Vf() ? new Vi : new Wi;
    case "thermometer":
      return new Si;
    default:
      return new Pi
  }
};
z.Me = function(a) {
  this.ny && a && a.Wd(a.ac + 90)
};
z.Rc = function(a) {
  Xi.f.Rc.call(this, a);
  this.frame.Cw(this.zb)
};
function Yi(a) {
  this.Xb = a
}
A.e(Yi, ei);
z = Yi.prototype;
z.vp = NaN;
z.Aa = 0.5;
z.Dj = 0.05;
z.bj = 0.05;
z.g = function(a, b) {
  Yi.f.g.call(this, a, b);
  this.Xb.Vf() || (this.scale.Td = !this.scale.aD());
  Zi(this, this.fp);
  Zi(this, this.op);
  C(a, "position") && (this.Aa = Qb(a, "position"));
  C(a, "start_margin") && (this.Dj = Qb(a, "start_margin"));
  C(a, "end_margin") && (this.bj = Qb(a, "end_margin"))
};
function Zi(a, b) {
  if(b) {
    if(a.Xb.Vf()) {
      var c = b.li();
      b.Ar = b.Ka();
      b.Bj(c)
    }else {
      108 == b.ol() && b.Lp(5)
    }
  }
}
z.jr = function() {
  Yi.f.jr.call(this);
  var a = this.Xb.Vf(), b = this.Xb.n();
  this.Wy = a ? b.width : b.height;
  this.GJ = a ? b.height : b.width;
  this.qj = Math.min(b.width, b.height);
  this.vp = (a ? b.y : b.x) + this.GJ * this.Aa;
  var c = this.Wy, d = this.Dj * c, c = this.bj * c, f = this.qj, d = Math.max(d, this.Xb.Dj * f), c = Math.max(c, this.Xb.bj * f);
  this.ov = this.size * f;
  a ? (d += b.x, c = b.Ja() - c) : (d += b.y, c = b.ra() - c);
  a = this.scale;
  a.ef = d;
  a.we = c;
  this.scale.Ae()
};
z.ov = NaN;
z.qj = NaN;
z.Wy = NaN;
z.GJ = NaN;
z.Ib = function(a, b, c, d) {
  d == h && (d = q);
  var f = this.qj;
  switch(a) {
    case 0:
      return this.vp - f * (d ? b : b + c) - (d ? c : 0) - this.ov / 2;
    case 1:
      return this.vp + f * b + this.ov / 2;
    default:
      return this.vp - (d ? 1 : f) * c / 2 + f * b
  }
};
z.lD = function(a) {
  var b = new P, c = this.Xb.n();
  b.x = c.x;
  b.y = c.y;
  var d = this.qj, f;
  1 == a.ol() ? (f = this.Ib(a.za, a.ga(), a.ec()), a = d * a.ec()) : (f = this.Ib(a.za, a.ga(), 0), a = 0);
  var d = this.Dj * this.Wy, g = this.bj * this.Wy, k = this.qj, d = Math.max(d, this.Xb.Dj * k), g = Math.max(g, this.Xb.bj * k);
  this.Xb.Vf() ? (b.y = f, b.height = a, b.x += d, b.width = c.width - d - g) : (b.x = f, b.width = a, b.y += d, b.height = c.height - d - g);
  return b
};
z.DI = function(a, b) {
  var c = this.lD(b), d = "";
  1 == b.ol() ? (d += S(c.x, c.y), d += U(c.Ja(), c.y), d += U(c.Ja(), c.ra()), d += U(c.x, c.ra())) : (d += S(c.x, c.y), d += U(c.Ja(), c.ra()));
  return d + " Z"
};
z.zo = function(a, b, c) {
  var d = this.cd.r(), f = this.qj, g = a.li() * f, f = a.Ka() * f, k = Math.min(g, f), l = 0, n = 0;
  this.Xb.Vf() ? (n = this.Ib(a.za, a.ga(), f, j), l = b - g / 2) : (l = this.Ib(a.za, a.ga(), g, j), n = b - f / 2);
  b = a.zo(d, l, n, k, g, f, c);
  b.Wl(b.Bb + a.Bb);
  return b
};
z.Jg = function(a) {
  var b = new O, c = this.bf.ej().n();
  this.Xb.Vf() ? (b.x = a - c.width / 2, b.y = this.Ib(this.bf.za, this.bf.ga(), c.height, j)) : (b.y = a - c.height / 2, b.x = this.Ib(this.bf.za, this.bf.ga(), c.width, j));
  return b
};
z.Vl = function(a, b, c) {
  this.Xb.Vf() ? (c.x = b, c.y = a) : (c.x = a, c.y = b)
};
function Lh(a, b, c, d, f) {
  var g = a.qj;
  switch(b) {
    case 0:
      return a.vp - g * (c + (f ? 0 : d)) - a.ov / 2;
    case 1:
      return a.vp + g * (c + (f ? 0 : d)) + a.ov / 2;
    default:
    ;
    case Kg:
      return a.vp + g * d * (f ? -0.5 : 0.5)
  }
}
;function $i(a) {
  lg.call(this, a)
}
A.e($i, lg);
z = $i.prototype;
z.rH = function() {
  return new aj(this)
};
z.sH = function() {
  return new bj(this)
};
z.nH = function() {
  return new cj(this)
};
z.Va = function() {
  var a = 0;
  this.Uc && this.Uc.isEnabled() && (a += this.Uc.Va());
  this.Vd && this.Vd.isEnabled() && (a += this.Vd.Va());
  return a
};
z.Cw = function(a) {
  var b = 1 + this.Va() + this.padding, c = this.Xb, d = 2 * b, f = 2 * b, b = Math.min(a.width / d, a.height / f), d = d * b, f = f * b;
  a.x += (a.width - d) / 2;
  a.y += (a.height - f) / 2;
  a.width = d;
  a.height = f;
  c.az = 2 * b;
  c.Qm = b;
  c.Ml.x = 0.5;
  c.Ml.y = 0.5
};
z.ci = function(a, b) {
  var c;
  c = this.Xb.Qm;
  var d = this.Xb.oc;
  c = "" + ed(a, d.x, d.y, c + this.padding * c + b * c);
  return c + " Z"
};
z.Fx = function(a, b, c) {
  var d = "", f = this.Xb.Qm, g = this.padding * f, b = f + g + b * f, c = f + g + c * f, k = this.Xb.oc.x, f = this.Xb.oc.y, g = k + b, l = k + b * Math.cos(359.999 * Math.PI / 180), n = f + b * Math.sin(359.999 * Math.PI / 180), d = d + S(g, f), d = d + a.ub(b, 1, 1, l, n, b), b = k + c, k = k + c * Math.cos(359.999 * Math.PI / 180), l = f + c * Math.sin(359.999 * Math.PI / 180), d = d + S(k, l), d = d + a.ub(c, 1, 0, b, f, c);
  return d += S(g, f)
};
function dj(a) {
  this.frame = a
}
A.e(dj, fg);
dj.prototype.p = function(a) {
  if(!this.Ma) {
    return p
  }
  var b = this.createElement(a), c = this.createElement(a);
  return new Nd(a, b, c)
};
dj.prototype.qa = function(a, b, c) {
  var d = a.r(), f;
  this.Ve && this.Ve.isEnabled() && (f = Tc(d.gk, this.Ve));
  d = this.yh(d);
  this.qC(a.Hg, d, a, b, c, f);
  Vd(this, a.Mg, d, a, f)
};
dj.prototype.yh = function(a) {
  var b = "", c, d;
  this instanceof aj ? (c = 0, d = this.Va()) : (c = 0, this.frame.Uc.isEnabled() && (c = this.frame.Uc.Va()), d = c + this.Va());
  return b += this.frame.Fx(a, c, d) + " Z"
};
dj.prototype.frame = p;
function aj(a) {
  this.frame = a
}
A.e(aj, dj);
function bj(a) {
  this.frame = a
}
A.e(bj, dj);
function cj(a) {
  this.jI = a
}
A.e(cj, Ud);
cj.prototype.jI = p;
cj.prototype.createElement = function(a) {
  return a.ja()
};
cj.prototype.yh = function(a) {
  return this.jI.ci(a, 0) + " Z"
};
function ej(a) {
  lg.call(this, a)
}
A.e(ej, $i);
ej.prototype.Fx = function(a, b, c) {
  b = "" + this.ci(a, b);
  return b += this.ci(a, c)
};
ej.prototype.ci = function(a, b) {
  var c = "", d = this.Xb, f = d.Qm;
  if(this.pB) {
    f += this.padding * f + b * f, d = new P(d.oc.x - f, d.oc.y - f, 2 * f, 2 * f)
  }else {
    var g = new Bd, k = this.Va();
    g.Ql(k - b);
    k = new P;
    k.bh(d.zb.x + g.tb() * f);
    k.fh(d.zb.y + g.rb() * f);
    k.eh(d.zb.Ja() - g.Ja() * f);
    k.$g(d.zb.ra() - g.ra() * f);
    d = k
  }
  this.zm == p ? c += gd(d) : (g = this.zm.Ca(), g.sa = g.sa * f / 100, g.P = g.P * f / 100, g.X = g.X * f / 100, g.ha = g.ha * f / 100, c += Ed(g, a, d));
  return c
};
function fj(a) {
  lg.call(this, a)
}
A.e(fj, $i);
fj.prototype.cj = function(a) {
  var b = Math.cos(a), a = Math.sin(a);
  return 0 <= b && 0 <= a ? 0 : 0 >= b && 0 <= a ? 1 : 0 >= b && 0 > a ? 2 : 3
};
fj.prototype.Cw = function() {
  var a = this.Xb, b = this.Va(), c = this.padding, d = a.Uu, f = a.Ru, g = f - d, k = new Bd;
  k.Ql(0);
  if(320 <= g) {
    k.Ql(1 + c)
  }else {
    var d = d * Math.PI / 180, l = f * Math.PI / 180, g = [];
    g.push(d);
    g.push(l);
    var f = [], n = this.cj(d), m = this.cj(l);
    if(n != m) {
      switch(n) {
        case 0:
          1 == m ? f.push(Math.PI / 2) : 2 == m ? (f.push(Math.PI / 2), f.push(Math.PI)) : 3 == m && (f.push(Math.PI / 2), f.push(Math.PI), f.push(3 * Math.PI / 2));
          break;
        case 1:
          0 == m ? (f.push(0), f.push(Math.PI), f.push(3 * Math.PI / 2)) : 2 == m ? f.push(Math.PI) : 3 == m && (f.push(Math.PI), f.push(3 * Math.PI / 2));
          break;
        case 2:
          1 == m ? (f.push(Math.PI / 2), f.push(0), f.push(3 * Math.PI / 2)) : 3 == m ? f.push(3 * Math.PI / 2) : 0 == m && (f.push(0), f.push(3 * Math.PI / 2));
          break;
        case 3:
          0 == m ? f.push(0) : 1 == m ? (f.push(0), f.push(Math.PI / 2)) : 2 == m && (f.push(0), f.push(Math.PI / 2), f.push(Math.PI))
      }
    }else {
      l - d > Math.PI && (f.push(0), f.push(Math.PI / 2), f.push(Math.PI), f.push(3 * Math.PI / 2))
    }
    for(d = 0;d < g.length;d++) {
      var o = g[d], l = c, n = k, m = Math.cos(o), o = Math.sin(o), m = new P(m, o, 0, 0);
      Yc(m, l, l);
      0 > m.tb() ? n.bh(Math.min(n.tb(), m.tb())) : n.eh(Math.max(n.Ja(), m.tb()));
      0 > m.Ja() ? n.bh(Math.min(n.tb(), m.Ja())) : n.eh(Math.max(n.Ja(), m.Ja()));
      0 > m.rb() ? n.fh(Math.min(n.rb(), m.rb())) : n.$g(Math.max(n.ra(), m.rb()));
      0 > m.ra() ? n.fh(Math.min(n.rb(), m.rb())) : n.$g(Math.max(n.ra(), m.ra()))
    }
    for(d = 0;d < f.length;d++) {
      n = f[d], m = c, g = k, l = (1 + m) * Math.cos(n), n = (1 + m) * Math.sin(n), 0 > l ? g.bh(Math.min(g.tb(), l)) : g.eh(Math.max(g.Ja(), l)), 0 > n ? g.fh(Math.min(g.rb(), n)) : g.$g(Math.max(g.ra(), n))
    }
    c += this.Xb.El;
    k.bh(Math.max(Math.abs(k.tb()), c));
    k.eh(Math.max(k.Ja(), c));
    k.fh(Math.max(Math.abs(k.rb()), c));
    k.$g(Math.max(k.ra(), c))
  }
  k.bh(k.tb() + b);
  k.eh(k.Ja() + b);
  k.fh(k.rb() + b);
  k.$g(k.ra() + b);
  b = k.tb() + k.Ja();
  c = k.rb() + k.ra();
  f = Math.min(a.zb.width / b, a.zb.height / c);
  d = b * f;
  g = c * f;
  a.zb.x += (a.zb.width - d) / 2;
  a.zb.y += (a.zb.height - g) / 2;
  a.zb.width = d;
  a.zb.height = g;
  a.az = 2 * f;
  a.Qm = f;
  a.Ml.x = k.tb() / b;
  a.Ml.y = k.rb() / c
};
function gj(a) {
  lg.call(this, a)
}
A.e(gj, fj);
gj.prototype.ci = function(a, b) {
  var c = "", d = this.Xb, f = d.Qm, g = new Bd, k = this.Va();
  g.Ql(k - b);
  k = new P;
  k.bh(d.zb.x + g.tb() * f);
  k.fh(d.zb.y + g.rb() * f);
  k.eh(d.zb.Ja() - g.Ja() * f);
  k.$g(d.zb.ra() - g.ra() * f);
  this.zm == p ? c += gd(k) : (d = this.zm.Ca(), d.sa = d.sa * f / 100, d.P = d.P * f / 100, d.X = d.X * f / 100, d.ha = d.ha * f / 100, c += Ed(d, a, k));
  return c
};
gj.prototype.Fx = function(a, b, c) {
  b = "" + this.ci(a, b);
  return b += this.ci(a, c)
};
function hj(a) {
  lg.call(this, a)
}
A.e(hj, fj);
hj.prototype.ci = function(a, b) {
  var c = "", d = this.Xb, f = d.Qm, g = d.El * f, k = this.padding * f + b * f, l = d.Uu, n = d.Ru, m = l * Math.PI / 180, o = n * Math.PI / 180, v = new O;
  v.x = d.oc.x + f * Math.cos(m);
  v.y = d.oc.y + f * Math.sin(m);
  m = new O;
  m.x = d.oc.x + f * Math.cos(o);
  m.y = d.oc.y + f * Math.sin(o);
  o = new P(v.x, v.y, 0, 0);
  Yc(o, k, k);
  v = new P(m.x, m.y, 0, 0);
  Yc(v, k, k);
  m = new P(d.oc.x, d.oc.y, 0, 0);
  Yc(m, k + g, k + g);
  g = new P(d.oc.x, d.oc.y, 0, 0);
  Yc(g, f + k, f + k);
  var w = n - l;
  320 <= w ? c += ed(a, d.oc.x, d.oc.y, f + k) : 270 < w ? (d = 90 - (360 - w) / 2, c += ij(o, l + 270 + d, 90 - d, j), c += ij(g, l, w, q), c += ij(v, n, 90 - d, q)) : (c += ij(o, l + 270, 90, j), c += ij(g, l, w, q), c += ij(v, n, 90, q), c += ij(m, n + 45, 360 - w - 90, q));
  return c
};
function ij(a, b, c, d) {
  var f = "", g = a.x + a.width / 2, k = a.y + a.height / 2, c = b + c, l = a.height / 2, a = a.width / 2, n = d;
  n == h && (n = j);
  var d = "", m, o, v = b;
  m = g + Uc(a, v);
  o = k + Vc(l, v);
  d = n ? d + S(m, o) : d + U(m, o);
  n = 1;
  c < b && (n = -n);
  for(b = 0 < n ? v < c : v > c;b;) {
    m = g + Uc(a, v), o = k + Vc(l, v), d += U(m, o), v += n, b = 0 < n ? v < c : v > c
  }
  v = c;
  m = g + Uc(a, v);
  o = k + Vc(l, v);
  d += U(m, o);
  return f + d
}
hj.prototype.Fx = function(a, b, c) {
  b = "" + this.ci(a, b);
  return b += this.ci(a, c)
};
function jj() {
}
z = jj.prototype;
z.pi = p;
z.xi = p;
z.ho = p;
z.K = p;
z.p = function(a, b, c) {
  this.K = c;
  this.j = a;
  c.Uc && c.Uc.isEnabled() && (this.pi = a.r().ja());
  c.Vd && c.Vd.isEnabled() && (this.xi = a.r().ja());
  c.md() && c.md().isEnabled() && (this.ho = a.r().ja())
};
z.mB = function(a) {
  this.ho && a.appendChild(this.ho);
  this.pi && a.appendChild(this.pi);
  this.xi && a.appendChild(this.xi)
};
z.update = function() {
  this.ho && this.bw(this.HB());
  if(this.pi) {
    var a = kj(this, this.K.Uc);
    this.pi && (this.pi.setAttribute("d", lj(this, this.j.r())), this.pi.setAttribute("style", this.Zt(this.K.Uc, a)))
  }
  this.xi && (a = kj(this, this.K.Vd), this.xi && (this.xi.setAttribute("d", mj(this, this.j.r())), this.xi.setAttribute("style", this.Zt(this.K.Vd, a))))
};
z.bw = function(a) {
  this.ho && (this.ho.setAttribute("d", this.Wt(this.j.r())), this.ho.setAttribute("style", this.Zt(this.K.md(), a)))
};
z.Zt = function(a, b) {
  var c;
  c = "" + (a.Lc() ? Md(a.Lc(), this.j.r(), b, p, j) : "");
  c += a.mc() ? Kd(a.mc(), this.j.r(), b, p, j) : "";
  return c += a.De() ? Hc(a.De(), this.j.r()) : ""
};
z.HB = function() {
  var a = this.j.w, b = a.Zc().oc, a = a.vd((this.K.te ? a.size : 1) * this.K.td), c = new P;
  c.x = b.x - a;
  c.y = b.y - a;
  c.width = 2 * a;
  c.height = 2 * a;
  return c
};
function kj(a, b) {
  var c = new P, d = a.j.w, f = (a.K.te ? d.size : 1) * a.K.td, g = d.Zc().oc, d = b == a.K.Uc ? d.vd(f + a.K.Uc.Va()) : d.vd(f + a.K.Uc.Va() + a.K.Vd.Va());
  c.x = g.x - d;
  c.y = g.y - d;
  c.width = c.height = 2 * d;
  return c
}
z.Wt = function(a) {
  var b = this.j.w, c = b.Zc().oc, a = "" + ed(a, c.x, c.y, b.vd((this.K.te ? b.size : 1) * this.K.td));
  return a + " Z"
};
function lj(a, b) {
  var c = "", d = a.j.w, f = (a.K.te ? d.size : 1) * a.K.td, g = d.Zc().oc, k = d.vd(f), d = d.vd(f + a.K.Uc.Va()), l = g.x, g = g.y, f = l + k, n = l + k * Math.cos(359.999 * Math.PI / 180), m = g + k * Math.sin(359.999 * Math.PI / 180), c = c + S(f, g), c = c + b.ub(k, 1, 1, n, m, k), k = l + d, l = l + d * Math.cos(359.999 * Math.PI / 180), n = g + d * Math.sin(359.999 * Math.PI / 180), c = c + S(l, n), c = c + b.ub(d, 1, 0, k, g, d), c = c + S(f, g);
  return c + " Z"
}
function mj(a, b) {
  var c = "", d = a.j.w, f = (a.K.te ? d.size : 1) * a.K.td, g = d.Zc().oc, k = d.vd(f + a.K.Uc.Va()), d = d.vd(f + a.K.Uc.Va() + a.K.Vd.Va()), l = g.x, g = g.y, f = l + k, n = l + k * Math.cos(359.999 * Math.PI / 180), m = g + k * Math.sin(359.999 * Math.PI / 180), c = c + S(f, g), c = c + b.ub(k, 1, 1, n, m, k), k = l + d, l = l + d * Math.cos(359.999 * Math.PI / 180), n = g + d * Math.sin(359.999 * Math.PI / 180), c = c + S(l, n), c = c + b.ub(d, 1, 0, k, g, d), c = c + S(f, g);
  return c + " Z"
}
function nj() {
}
A.e(nj, Ud);
z = nj.prototype;
z.io = p;
z.K = p;
z.j = p;
z.p = function(a, b) {
  this.K = b;
  this.j = a;
  b.md() && b.md().isEnabled() && (this.io = a.r().ja())
};
z.mB = function(a) {
  this.io && a.appendChild(this.io)
};
z.update = function() {
  this.io && this.bw(this.HB())
};
z.bw = function(a) {
  this.io && (this.io.setAttribute("d", this.Wt(this.j.r())), this.io.setAttribute("style", this.Zt(this.K.md(), a)))
};
z.Zt = function(a, b) {
  var c;
  c = "" + (a.Lc() ? Md(a.Lc(), this.j.r(), b, p, j) : "");
  c += a.mc() ? Kd(a.mc(), this.j.r(), b, p, j) : "";
  return c += a.De() ? Hc(a.De(), this.j.r()) : ""
};
z.HB = function() {
  var a = this.j.w, b = this.K, a = a.vd((b.te ? a.size : 1) * b.td + (b.Ey ? a.size : 1) * b.$B);
  return new P(-a, -a, 2 * a, 2 * a)
};
z.Wt = function(a) {
  var b = "", c = this.j, d = c.w, f = c.ya.Hu, g = (f.te ? d.size : 1) * f.td, k = f.eM, l = d.vd(g), f = d.vd(g + (f.Ey ? d.size : 1) * f.$B), d = d.Zc().oc, g = 360 / k;
  c.Gu.Xd(d.x);
  c.Gu.gd(d.y);
  var c = 90 - g / 2, n = q, d = new O(0, 0), m = new O;
  m.x = d.x + f * Math.cos(c * Math.PI / 180);
  m.y = d.y + f * Math.sin(c * Math.PI / 180);
  var o, v, w;
  w = m.x;
  v = m.y;
  for(var y, E, b = b + S(w, v), V = 0;V < k;V++) {
    o = n ? l : f, v = c + g * V, w = v + g, y = d.x + o * Math.cos(w * Math.PI / 180), E = d.x + o * Math.sin(w * Math.PI / 180), w = d.x + o * Math.cos(v * Math.PI / 180), v = d.y + o * Math.sin(v * Math.PI / 180), b += U(w, v), b += a.ub(o, 0, 1, y, E, o), n = !n
  }
  b += U(m.x, m.y);
  return b + " Z"
};
function oj() {
  this.background = new Od;
  this.td = 0.5;
  this.Ey = this.te = q
}
z = oj.prototype;
z.eM = 12;
z.$B = 0.03;
z.td = p;
z.te = p;
z.Ey = p;
z.background = p;
z.md = u("background");
z.g = function(a) {
  var b;
  this.background.g(a);
  C(a, "radius") && (b = J(a, "radius"), this.td = (this.te = Pg(b)) ? Qg(b) : Pb(b));
  C(a, "gear_height") && (b = J(a, "gear_height"), this.Ey = Pg(b), this.$B = this.te ? Qg(b) : Pb(b))
};
function pj() {
  this.td = 0.1;
  this.te = q
}
z = pj.prototype;
z.td = p;
z.te = p;
z.Uc = p;
z.Vd = p;
z.background = p;
z.md = u("background");
z.g = function(a) {
  var b;
  C(a, "radius") && (b = J(a, "radius"), this.td = (this.te = Pg(b)) ? Qg(b) : Pb(b));
  Ub(a, "inner_stroke") && (this.Uc = new fg, this.Uc.g(F(a, "inner_stroke")));
  Ub(a, "outer_stroke") && (this.Vd = new fg, this.Vd.g(F(a, "outer_stroke")));
  Ub(a, "background") && (this.background = new Od, this.background.g(F(a, "background")))
};
function qj(a) {
  Jg.call(this, a)
}
A.e(qj, Jg);
qj.prototype.no = p;
qj.prototype.g = function(a) {
  a = qj.f.g.call(this, a);
  Ub(a, "cap") ? (this.no = new pj, this.no.g(F(a, "cap"))) : this.no = p;
  return a
};
function rj() {
}
A.e(rj, Pf);
z = rj.prototype;
z.qq = p;
z.Yc = function() {
  rj.f.Yc.call(this);
  var a = this.j.ya.no;
  a != p && (this.qq = new jj, this.qq.p(this.j, this, a))
};
z.ff = function() {
  rj.f.ff.call(this);
  this.qq != p && this.qq.mB(this.j.Uw)
};
z.createElement = function() {
  return this.j.r().ja()
};
z.Ta = function() {
  var a = this.j;
  this.Db && this.Db.I.setAttribute("d", a.vh.qa(a.fb(), a));
  this.Fb && this.Fb.I.setAttribute("d", a.vh.qa(a.fb(), a));
  this.qq != p && this.qq.update()
};
z.ud = s();
function sj(a) {
  a && (this.background = new Od);
  this.Du = this.te = this.Iy = this.Hy = this.xu = q
}
z = sj.prototype;
z.td = p;
z.te = p;
z.wB = p;
z.xu = p;
z.iK = p;
z.Iy = p;
z.hK = p;
z.Hy = p;
z.HA = p;
z.Du = p;
z.g = function(a, b) {
  var c;
  C(a, "radius") && (c = J(a, "radius"), this.td = (this.te = Pg(c)) ? Qg(c) : Pb(c));
  C(a, "thickness") && (c = J(a, "thickness"), this.HA = (this.Du = Pg(c)) ? Qg(c) : Pb(c));
  C(a, "base_radius") && (c = J(a, "base_radius"), this.wB = (this.xu = Pg(c)) ? Qg(c) : Pb(c));
  C(a, "point_thickness") && (c = J(a, "point_thickness"), this.iK = (this.Iy = Pg(c)) ? Qg(c) : Pb(c));
  C(a, "point_radius") && (c = J(a, "point_radius"), this.hK = (this.Hy = Pg(c)) ? Qg(c) : Pb(c));
  this.background != p && this.background.g(a, b)
};
function tj(a) {
  Jg.call(this, a);
  this.Hh = new sj(q)
}
A.e(tj, qj);
tj.prototype.Hh = p;
tj.prototype.wI = u("Hh");
tj.prototype.g = function(a) {
  a = tj.f.g.call(this, a);
  this.Hh.g(a, this.ka());
  return a
};
function uj() {
}
A.e(uj, rj);
function vj() {
  $.call(this)
}
A.e(vj, Rf);
vj.prototype.cc = function() {
  return tj
};
vj.prototype.lc = function() {
  return new uj
};
function wj(a) {
  Jg.call(this, a);
  this.fs = q;
  this.ab = 0.1;
  this.wy = q
}
A.e(wj, qj);
z = wj.prototype;
z.fs = p;
z.ab = p;
z.Ka = u("ab");
z.wy = p;
z.g = function(a) {
  var a = wj.f.g.call(this, a), b;
  C(a, "width") && (b = J(a, "width"), this.ab = (this.wy = Pg(b)) ? Qg(b) : Pb(b));
  C(a, "start_from_zero") && (this.fs = K(a, "start_from_zero"));
  return a
};
function xj() {
}
A.e(xj, Pf);
xj.prototype.update = function(a, b) {
  xj.f.update.call(this, a, b)
};
xj.prototype.createElement = function() {
  return this.j.r().ja()
};
xj.prototype.Ta = function(a) {
  a.setAttribute("d", yj(this.j))
};
function zj() {
  $.call(this)
}
A.e(zj, Rf);
zj.prototype.cc = function() {
  return wj
};
zj.prototype.lc = function() {
  return new xj
};
function Aj() {
  Jg.call(this, h);
  this.gb = new ui(q)
}
A.e(Aj, qj);
Aj.prototype.gb = p;
Aj.prototype.g = function(a) {
  a = Aj.f.g.call(this, a);
  this.gb.g(a);
  return a
};
function Bj() {
  this.Mn = new kg
}
A.e(Bj, Pf);
z = Bj.prototype;
z.Mn = p;
z.fb = function() {
  return this.j.sd
};
z.update = function(a, b) {
  this.j.hd();
  Bj.f.update.call(this, a, b)
};
z.createElement = function() {
  return this.j.r().ja()
};
z.Ta = function(a) {
  a.setAttribute("d", this.Mn.Io(this.j.ya.gb.Yd, this.j.r(), Math.min(this.j.n().width, this.j.n().height), this.j.gp.x, this.j.gp.y, this.j.n().width, this.j.n().height, this.j.ya.Md() ? this.j.ya.Lc().Va() : 1))
};
function Cj() {
  $.call(this)
}
A.e(Cj, Rf);
Cj.prototype.cc = function() {
  return Aj
};
Cj.prototype.lc = function() {
  return new Bj
};
function Dj(a) {
  Jg.call(this, a)
}
A.e(Dj, qj);
Dj.prototype.Hu = p;
Dj.prototype.Hh = p;
Dj.prototype.wI = u("Hh");
Dj.prototype.g = function(a) {
  Ub(a, "knob_background") ? (this.Hu = new oj, this.Hu.g(F(a, "knob_background"))) : this.Hu = p;
  Ub(a, "needle") ? (Dj.f.g.call(this, F(a, "needle")), this.Hh = new sj(j), this.Hh.g(F(a, "needle"), this.ka())) : this.Hh = p;
  return a = Dj.f.g.call(this, a)
};
function Ej() {
}
A.e(Ej, rj);
Ej.prototype.zr = p;
Ej.prototype.Yc = function() {
  Ej.f.Yc.call(this);
  var a = this.j.ya.Hu;
  a != p && (this.zr = new nj, this.zr.p(this.j, a))
};
Ej.prototype.ff = function() {
  Ej.f.ff.call(this);
  this.zr != p && this.zr.mB(this.j.Gu)
};
Ej.prototype.Ta = function() {
  Ej.f.Ta.call(this);
  this.zr != p && this.zr.update()
};
function Fj() {
  $.call(this)
}
A.e(Fj, Rf);
Fj.prototype.cc = function() {
  return Dj
};
Fj.prototype.lc = function() {
  return new Ej
};
function Gj() {
}
z = Gj.prototype;
z.bd = p;
z.vd = u("bd");
z.lv = p;
z.Cz = p;
z.Bz = p;
z.mv = p;
z.qa = function(a, b) {
  var c = b.be(b.ka()), d = c.Hh, f = c.mc(), c = b.w, g = 0, k = new O(0, 0), l = new O, n = new O, m = new O, o = new O, v = new O, w = new O;
  f != p && 1 == f.Ra && 0 == f.yb.Ra && (f.yb.ac += g);
  g *= Math.PI / 180;
  this.bd = c.vd((d.te ? c.size : 1) * d.td);
  this.lv = c.vd((d.xu ? c.size : 1) * d.wB);
  this.Cz = c.vd((d.Du ? c.size : 1) * d.HA);
  this.Bz = c.vd((d.Iy ? c.size : 1) * d.iK);
  this.mv = c.vd((d.Hy ? c.size : 1) * d.hK);
  var d = Math.sin(g), g = Math.cos(g), f = this.Cz / 2 * d, y = this.Cz / 2 * g;
  w.x = k.x + this.lv * g;
  w.y = k.y + this.lv * d;
  l.x = w.x - f;
  l.y = w.y + y;
  n.x = w.x + f;
  n.y = w.y - y;
  f = this.Bz / 2 * d;
  y = this.Bz / 2 * g;
  w.x = k.x + (this.bd - this.mv) * g;
  w.y = k.y + (this.bd - this.mv) * d;
  m.x = w.x - f;
  m.y = w.y + y;
  o.x = w.x + f;
  o.y = w.y - y;
  v.x = k.x + this.bd * g;
  v.y = k.y + this.bd * d;
  k = [l, n, o, v, m];
  w = new P;
  w.x = Math.min(l.x, n.x, m.x, o.x, v.x);
  w.y = Math.min(l.y, n.y, m.y, o.y, v.y);
  w.width = Math.max(l.x, n.x, m.x, o.x, v.x) - w.x;
  w.height = Math.max(l.y, n.y, m.y, o.y, v.y) - w.y;
  b.Ln(b.ta() > c.fa().pa ? c.fa().pa : b.ta() < c.fa().la ? c.fa().la : b.ta());
  a.Xd(c.Xb.oc.x);
  a.gd(c.Xb.oc.y);
  a.Wl(c.fa().pc(b.ta()));
  c = "" + S(k[0].x, k[0].y);
  c += U(k[1].x, k[1].y);
  c += U(k[2].x, k[2].y);
  c += U(k[3].x, k[3].y);
  c += U(k[4].x, k[4].y);
  c += U(k[0].x, k[0].y);
  return c + " Z"
};
function Hj() {
  Mg.call(this)
}
A.e(Hj, Mg);
function Ij(a, b, c, d, f, g, k) {
  var l = new O, n = new O, m = new O, o = new O, v = Math.sin(k * Math.PI / 180), k = Math.cos(k * Math.PI / 180), w = a.Oc.oc, a = w.x + d / 2 * v, y = w.y - d / 2 * k, E = w.x - d / 2 * v, d = w.y + d / 2 * k;
  m.x = a + (g - f / 2) * k;
  m.y = y + (g - f / 2) * v;
  n.x = E + (g + f / 2) * k;
  n.y = d + (g + f / 2) * v;
  l.x = a + (g + f / 2) * k;
  l.y = y + (g + f / 2) * v;
  o.x = E + (g - f / 2) * k;
  o.y = d + (g - f / 2) * v;
  switch(b) {
    case 0:
      c.x = (l.x + o.x) / 2;
      c.y = (l.y + o.y) / 2;
      break;
    case 7:
      c.x = (m.x + o.x) / 2;
      c.y = (m.y + o.y) / 2;
      break;
    case 1:
      c.x = (l.x + m.x) / 2;
      c.y = (l.y + m.y) / 2;
      break;
    case 5:
      c.x = (o.x + n.x) / 2;
      c.y = (o.y + n.y) / 2;
      break;
    case 3:
      c.x = (l.x + n.x) / 2;
      c.y = (l.y + n.y) / 2;
      break;
    case 8:
      c.x = m.x;
      c.y = m.y;
      break;
    case 2:
      c.x = l.x;
      c.y = l.y;
      break;
    case 6:
      c.x = o.x;
      c.y = o.y;
      break;
    case 4:
      c.x = n.x, c.y = n.y
  }
}
function Jj() {
  Mg.call(this)
}
A.e(Jj, Hj);
Jj.prototype.Rm = p;
Jj.prototype.Uw = p;
Jj.prototype.p = function(a) {
  this.Rm = new W(a);
  this.Uw = new W(a);
  Jj.f.p.call(this, a)
};
function Kj() {
  Mg.call(this);
  this.vh = new Gj
}
A.e(Kj, Jj);
z = Kj.prototype;
z.vh = p;
z.Rb = x("needle_pointer_style");
z.Qb = function() {
  return new vj
};
z.p = function(a) {
  Kj.f.p.call(this, a);
  this.Rm.ia(this.D);
  this.Rm.ia(this.Uw);
  return this.Rm
};
z.Qq = function(a, b) {
  var c = this.w.fa().pc(Ng(this, this.bb)), d = Math.max(this.vh.lv, this.vh.mv, this.vh.vd()), f = Math.min(this.vh.lv, this.vh.mv, this.vh.vd()), g = Math.max(this.vh.Bz, this.vh.Cz);
  Ij(this, a, b, g, d - f, (f + d) / 2, c)
};
function Lj() {
  Mg.call(this)
}
A.e(Lj, Hj);
z = Lj.prototype;
z.jb = p;
z.tc = p;
z.Tb = p;
z.Qa = p;
z.Rb = x("bar_pointer_style");
z.Qb = function() {
  return new zj
};
function yj(a) {
  var b = a.w, c = b.Zc().oc, d = a.be(a.ka()), f = (d.wy ? b.size : 1) * d.Ka(), g, k;
  a instanceof Mj ? (g = Ng(a, a.bb), k = Ng(a, a.Rd)) : (g = d.fs ? 0 : b.fa().la, k = Ng(a, a.bb));
  a.jb = b.fa().pc(g);
  a.tc = b.fa().pc(k);
  g = Math.abs(a.jb - a.tc);
  a.Tb = Oh(b, d.za, d.ga(), f, j);
  a.Qa = Oh(b, d.za, d.ga(), f, q);
  var l, n;
  a.tc % 360 != a.jb % 360 ? (n = (a.tc + 0.5) * Math.PI / 180, l = a.jb * Math.PI / 180) : (b = Math.min(a.jb, a.tc), n = (a.tc - b - 0.001) * Math.PI / 180, l = (a.jb - b) * Math.PI / 180);
  b = c.x + a.Tb * Math.cos(l);
  d = c.y + a.Tb * Math.sin(l);
  f = c.x + a.Tb * Math.cos(n);
  k = c.y + a.Tb * Math.sin(n);
  var m = c.x + a.Qa * Math.cos(l);
  l = c.y + a.Qa * Math.sin(l);
  var o = c.x + a.Qa * Math.cos(n), c = c.y + a.Qa * Math.sin(n);
  n = 1;
  180 > g && (n = 0);
  g = a.r();
  var v;
  v = "" + S(b, d);
  v += g.ub(a.Tb, n, 1, f, k, a.Tb);
  v += U(o, c);
  v += g.ub(a.Qa, n, q, m, l, a.Qa);
  v += U(b, d);
  return v + " Z"
}
z.Qq = function(a, b) {
  var c = this.w.Zc().oc, d, f;
  b.x = c.x;
  b.y = c.y;
  switch(a) {
    case 0:
      d = (this.jb + this.tc) / 2;
      f = (this.Tb + this.Qa) / 2;
      break;
    case 7:
      d = (this.jb + this.tc) / 2;
      f = this.Tb;
      break;
    case 1:
      d = this.jb;
      f = (this.Tb + this.Qa) / 2;
      break;
    case 5:
      d = this.Hm;
      f = (this.Tb + this.Qa) / 2;
      break;
    case 3:
      d = (this.jb + this.Hm) / 2;
      f = this.Qa;
      break;
    case 8:
      d = this.jb;
      f = this.Tb;
      break;
    case 2:
      d = this.jb;
      f = this.Qa;
      break;
    case 6:
      d = this.tc;
      f = this.Tb;
      break;
    case 4:
      d = this.tc, f = this.Qa
  }
  b.x += f * Math.cos(d * Math.PI / 180);
  b.y += f * Math.sin(d * Math.PI / 180)
};
function Mj() {
  Mg.call(this)
}
A.e(Mj, Lj);
Mj.prototype.Rd = p;
Mj.prototype.Rb = x("range_bar_pointer_style");
Mj.prototype.g = function(a, b, c) {
  a.value = a.start;
  Mj.f.g.call(this, a, b, c);
  this.Rd = M(a, "end")
};
function Nj() {
  Mg.call(this)
}
A.e(Nj, Hj);
z = Nj.prototype;
z.gp = p;
z.sd = p;
z.p = function(a) {
  this.sd = new W(a);
  Nj.f.p.call(this, a);
  this.D.ia(this.sd);
  return this.D
};
z.Qq = function(a, b) {
  var c = this.ya, d = c.gb, f = this.w, g = f.vd((d.qd ? f.size : 1) * d.Ka()), d = f.vd((d.wl ? f.size : 1) * d.vb()), k = f.ng(c.za, c.ga(), 0, q);
  switch(c.za) {
    case 0:
      k -= g / 2;
      break;
    case 1:
      k += g / 2
  }
  var c = new O, l = f.fa().pc(Ng(this, this.bb));
  Hh(f, k, l, c);
  Ij(this, a, b, g, d, k, l)
};
z.hd = function() {
  var a = this.ya, b = a.gb, c = this.w, d = c.vd((b.qd ? c.size : 1) * b.Ka()), f = c.vd((b.wl ? c.size : 1) * b.vb()), g = c.ng(a.za, a.ga(), 0, q);
  switch(a.za) {
    case 0:
      g -= d / 2;
      break;
    case 1:
      g += d / 2
  }
  var k = new O, l = c.fa().pc(this.bb);
  Hh(c, g, l, k);
  this.gp = new O(-d / 2, -f / 2);
  this.k.x = k.x;
  this.k.y = k.y;
  this.k.width = d;
  this.k.height = f;
  c = 0;
  if(b.co) {
    switch(a.za) {
      case 0:
      ;
      case Kg:
        c = l + 90;
        break;
      case 1:
        c = l - 90
    }
  }
  this.sd.Wl(c + b.Bb);
  this.sd.Xd(k.x);
  this.sd.gd(k.y)
};
z.Rb = x("marker_pointer_style");
z.cc = function() {
  return Aj
};
z.Qb = function() {
  return new Cj
};
function Oj() {
  Mg.call(this);
  this.vh = new Gj
}
A.e(Oj, Jj);
z = Oj.prototype;
z.vh = p;
z.Gu = p;
z.Rb = x("knob_pointer_style");
z.cc = function() {
  return Dj
};
z.Qb = function() {
  return new Fj
};
z.p = function(a) {
  this.Gu = new W(a);
  Oj.f.p.call(this, a);
  this.Rm.ia(this.Gu);
  this.Rm.ia(this.D);
  this.Rm.ia(this.Uw);
  return this.Rm
};
function Pj() {
  ii.call(this);
  this.Ml = new O(0.5, 0.5);
  this.oc = new O;
  this.bz = 0;
  this.Uu = 72E3;
  this.Ru = -72E3;
  this.El = 0
}
A.e(Pj, ii);
z = Pj.prototype;
z.az = p;
z.Qm = p;
z.Ml = p;
z.oc = p;
z.bz = p;
z.Uu = p;
z.Ru = p;
z.El = p;
z.p = function(a, b, c) {
  Pj.f.p.call(this, a, b, c);
  return this.cd
};
z.g = function(a) {
  Pj.f.g.call(this, a);
  F(a, "pivot_point") && (a = F(a, "pivot_point"), this.Ml.x = Pb(a.x), this.Ml.y = Pb(a.y))
};
z.tH = function(a) {
  switch(a) {
    case "bar":
      return new Lj;
    case "rangebar":
      return new Mj;
    case "marker":
      return new Nj;
    case "knob":
      return new Oj;
    default:
      return new Kj
  }
};
z.Zw = function(a) {
  Qj(this, a, a.ka().wa);
  Qj(this, a, a.ka().dc);
  Qj(this, a, a.ka().kc);
  Qj(this, a, a.ka().Zb);
  Qj(this, a, a.ka().gc)
};
function Qj(a, b, c) {
  b = b.w;
  c.no != p && (a.El = Math.max(a.El, (c.no.te ? b.size : 1) * c.no.td));
  c.wI && (a.El = Math.max(a.El, (c.Hh.Du ? b.size : 1) * c.Hh.HA / 2), c = (c.Hh.xu ? b.size : 1) * c.Hh.wB, 0 > c && (a.El = Math.max(-c, a.El)))
}
z.qH = function(a) {
  if(a != p && C(a, "type")) {
    switch(Wb(a, "type")) {
      case "auto":
        return new hj(this);
      case "rectangular":
        return new ej(this);
      case "autorectangular":
        return new gj(this)
    }
  }
  return new $i(this)
};
z.TB = function(a, b) {
  var c = new Gh(this);
  c.g(a, b);
  this.bz = Math.max(c.td, this.bz);
  this.Uu = Math.min(c.Cj, this.Uu);
  this.Ru = Math.max(c.Cj + c.Qp, this.Ru);
  return c
};
z.$C = x(1);
z.Rc = function(a) {
  Pj.f.Rc.call(this, a);
  this.frame.pB ? this.frame.Cw(this.zb) : (this.az = a = Math.min(this.zb.width, this.zb.height), this.Qm = this.bz * a);
  this.oc.x = this.zb.x + this.Ml.x * this.zb.width;
  this.oc.y = this.zb.y + this.Ml.y * this.zb.height
};
function Gh(a) {
  this.Xb = a;
  this.td = 0.6;
  this.Qp = this.Cj = 90;
  this.Hm = this.Cj + this.Qp
}
A.e(Gh, ei);
z = Gh.prototype;
z.Jg = function(a) {
  var b = new O, c = this.ng(this.bf.za, this.bf.ga(), 0, q), d = a * Math.PI / 180, a = Math.round(1E3 * Math.cos(d)) / 1E3, d = Math.round(1E3 * Math.sin(d)) / 1E3, f = this.bf.ej().n(), g = f.width / 2, f = f.height / 2;
  switch(this.bf.za) {
    case 1:
      c += g * Math.abs(a) + f * Math.abs(d);
      break;
    case 0:
      c -= g * Math.abs(a) + f * Math.abs(d)
  }
  b.x = this.Zc().oc.x + c * a - g;
  b.y = this.Zc().oc.y + c * d - f;
  return b
};
z.g = function(a, b) {
  Gh.f.g.call(this, a, b);
  C(a, "radius") && (this.td = Qb(a, "radius"));
  C(a, "start_angle") && (this.Cj = M(a, "start_angle") + 90);
  C(a, "sweep_angle") && (this.Qp = M(a, "sweep_angle"));
  this.Hm = this.Cj + this.Qp
};
z.jr = function() {
  Gh.f.jr.call(this);
  this.bd = this.Xb.az * this.td;
  this.nv = this.bd * this.size;
  var a = this.scale, b = this.Hm;
  a.ef = this.Cj;
  a.we = b;
  this.scale.LR = this.Qp / 10;
  this.scale.Ae()
};
function Hh(a, b, c, d) {
  a = a.Xb.oc;
  c = c * Math.PI / 180;
  d.x = a.x + b * Math.cos(c);
  d.y = a.y + b * Math.sin(c)
}
function Oh(a, b, c, d, f) {
  switch(b) {
    case 0:
      return a.bd * (1 - c - (f ? 0 : d)) - a.nv / 2;
    case 1:
      return a.bd * (1 + c + (f ? 0 : d)) + a.nv / 2;
    default:
      return a.bd * (1 + (f ? -0.5 : 0.5) * d - c)
  }
}
z.ng = function(a, b, c, d) {
  switch(a) {
    case 0:
      return this.bd * (1 - (d ? 0 : c) - b) - (d ? c : 0) - this.nv / 2;
    case 1:
      return this.bd * (1 + b) + this.nv / 2;
    default:
      return this.bd * (1 - (d ? 0 : c / 2)) - (d ? c / 2 : 0)
  }
};
z.zo = function(a, b, c) {
  var d = this.cd.r(), f = a.Ka() * this.bd, g = a.li() * this.bd, k = Math.min(f, g), l = this.ng(a.za, a.ga(), 0, q);
  switch(a.za) {
    case 0:
      l -= g / 2;
      break;
    case 1:
      l += g / 2
  }
  var n = this.Xb.oc, m = n.x + Uc(l, b) - f / 2, l = n.y + Vc(l, b) - g / 2, c = a.zo(d, m, l, k, f, g, c);
  a.co ? c.Wl(b - 90 + a.Bb, m + f / 2, l + g / 2) : c.Wl(a.Bb);
  return c
};
z.lD = function(a) {
  var b = new P, c = this.Xb.oc, d = 0, d = 0;
  1 == a.ol() && (d = this.ng(a.za, a.ga(), a.ec(), q), d += this.vd(1) * a.ec());
  b.x = c.x - d;
  b.y = c.y - d;
  b.width = 2 * d;
  b.height = 2 * d;
  return b
};
z.DI = function(a, b) {
  var c = this.Xb.oc, d = 1 == b.ol(), f;
  if(d) {
    f = this.ng(b.za, b.ga(), b.ec(), q);
    var g = f + this.bd * b.ec()
  }else {
    f = this.ng(b.za, b.ga(), 0, q)
  }
  var k, l, n;
  this.Hm % 360 != this.Cj % 360 ? (l = this.Hm * Math.PI / 180, k = this.Cj * Math.PI / 180) : (n = Math.min(this.Cj, this.Hm), l = (this.Hm - n - 0.001) * Math.PI / 180, k = (this.Cj - n) * Math.PI / 180);
  n = c.x + f * Math.cos(k);
  var m = c.y + f * Math.sin(k), o = c.x + f * Math.cos(l), v = c.y + f * Math.sin(l), w = c.x + g * Math.cos(k);
  k = c.y + g * Math.sin(k);
  var y = c.x + g * Math.cos(l), c = c.y + g * Math.sin(l);
  l = 1;
  180 > this.Qp && (l = 0);
  var E;
  E = "" + S(n, m);
  E += a.ub(f, l, 1, o, v, f);
  d && (E += U(y, c), E += a.ub(g, l, q, w, k, g), E += U(n, m));
  E += a.ub(f, l, q, n, m, f);
  return E + " Z"
};
z.td = p;
z.bd = p;
z.vd = function(a) {
  return this.bd * a
};
z.Cj = p;
z.Qp = p;
z.tc = p;
function nf() {
}
A.e(nf, hf);
z = nf.prototype;
z.Ok = s();
z.zp = s();
z.Zr = s();
z.yK = s();
z.Hk = s();
z.yk = s();
z.zK = s();
z.SH = s();
z.Ge = s();
z.Pa = x(4);
z.Na = x(p);
z.clear = s();
z.refresh = s();
z.N = p;
z.cA = t("N");
z.br = function() {
  return new xf
};
z.jo = p;
z.Kx = p;
z.Sm = p;
z.UI = p;
z.g = function(a) {
  this.UI = new Eg;
  this.jo = new Dg;
  this.Sm = [];
  this.Kx = {};
  var b = [], c, d, f, g, k = p, l, n, m, o, v = p;
  C(a, "circular_default_template") && (l = N(a, "circular_default_template"));
  C(a, "linear_default_template") && (n = N(a, "linear_default_template"));
  C(a, "label_default_template") && (m = N(a, "label_default_template"));
  C(a, "image_default_template") && (o = N(a, "image_default_template"));
  C(a, "indicator_default_template") && (v = N(a, "indicator_default_template"));
  C(a, "circular") && (c = I(a, "circular"), b.push(c));
  C(a, "linear") && (d = I(a, "linear"), b.push(d));
  C(a, "label") && (f = I(a, "label"), b.push(f));
  C(a, "image") && (g = I(a, "image"), b.push(g));
  C(a, "indicator") && (k = I(a, "indicator"), b.push(k));
  var w;
  if(c) {
    w = I(a, "circular_template");
    for(b = 0;b < c.length;b++) {
      Rj(this, "circular", c[b], w, Pj, l)
    }
  }
  if(d) {
    w = I(a, "linear_template");
    for(b = 0;b < d.length;b++) {
      Rj(this, "linear", d[b], w, Xi, n)
    }
  }
  if(f) {
    w = I(a, "label_template");
    for(b = 0;b < f.length;b++) {
      Rj(this, "label", f[b], w, p, m)
    }
  }
  if(g) {
    w = I(a, "image_template");
    for(b = 0;b < g.length;b++) {
      Rj(this, "image", g[b], w, p, o)
    }
  }
  if(k) {
    w = I(a, "indicator_template");
    for(b = 0;b < k.length;b++) {
      Rj(this, "indicator", k[b], w, p, v)
    }
  }
  for(b = 0;b < this.Sm.length;b++) {
    a = p, this.Sm[b].YE != p && (a = this.Sm[b].YE, a = this.Kx[a]), a ? a.children.push(this.Sm[b]) : this.jo.children.push(this.Sm[b])
  }
};
function Rj(a, b, c, d, f, g) {
  f == p && Le(b + " gauge");
  b = p;
  if(C(c, "name")) {
    if(b = J(c, "name"), a.Kx[b] != p) {
      return
    }
  }else {
    b = "___gauge__" + a.Sm.length
  }
  f = new f;
  f.Ul(a);
  var k = p;
  C(c, "pointers") && C(F(c, "pointers"), "pointer") && (k = I(F(c, "pointers"), "pointer"));
  c = a.UI.apply(c, d, g);
  k && (c.pointers.pointer = k);
  f.g(c);
  a.Sm.push(f);
  a.Kx[b] = f
}
z.F = p;
z.tl = t("F");
z.k = p;
z.n = u("k");
z.Rc = t("k");
z.Xt = u("k");
z.D = p;
z.fb = u("D");
z.p = function(a, b) {
  this.D = new W(this.F);
  this.k = a;
  this.D.ia(this.jo.p(this.F, a, b));
  return this.D
};
z.qa = function() {
  this.jo.qa()
};
z.om = t("k");
z.Jq = function() {
  this.jo.Wa(this.k)
};
z.fG = x(q);
z.uF = s();
z.dy = x(p);
z.mb = function(a) {
  a == p && (a = {});
  a.nM = [];
  for(var b = 0;b < this.jo.OE();b++) {
    var c = this.jo.children[b];
    c && a.nM.push(c.mb())
  }
  return a
};
z.Xw = x(q);
function Sj() {
}
Sj.prototype.ti = p;
function Tj(a, b) {
  b >= a.ti.length && (b %= a.ti.length);
  return a.ti[b]
}
Sj.prototype.g = function() {
  this.ti = []
};
Sj.prototype.Ca = function(a) {
  a == h && (a = new Sj);
  a.ti = this.ti;
  return a
};
function Uj() {
}
function Vj(a, b, c) {
  if(!a.Hc) {
    return p
  }
  for(var c = Cb(c), a = I(a.Hc, b), b = a.length, d = 0;d < b;d++) {
    if(C(a[d], "name") && N(a[d], "name") == c) {
      return a[d]
    }
  }
  return p
}
Uj.prototype.Hc = p;
Uj.prototype.g = t("Hc");
function Wj() {
}
A.e(Wj, Sj);
var Xj = {lL:0, hL:1};
z = Wj.prototype;
z.yb = p;
z.wu = q;
z.Sa = "";
z.Ra = 0;
z.vm = "auto";
z.aJ = q;
z.g = function(a) {
  Wj.f.g.call(this, a);
  C(a, "name") && (this.Sa = N(a, "name"));
  C(a, "color_count") && (this.vm = F(a, "color_count"), Xb(this.vm) ? this.aJ = j : this.vm = Ob(this.vm));
  if(C(a, "type")) {
    var b;
    a: {
      switch(N(a, "type")) {
        case "colorrange":
          b = Xj.hL;
          break a;
        default:
          b = Xj.lL
      }
    }
    this.Ra = b
  }
  switch(this.Ra) {
    case 1:
      this.wu = j;
      this.vm && !this.aJ && (b = new yc(1), b.g(F(a, "gradient")), Yj(this, b, this.vm), this.wu = q);
      this.yb = new yc(1);
      this.yb.g(F(a, "gradient"));
      break;
    default:
      a = I(a, "item");
      b = a.length;
      for(var c = 0;c < b;c++) {
        this.ti.push(Yb(a[c]))
      }
  }
};
function Zj(a, b, c) {
  var d = a;
  if(a.wu && !c || a.yb && c) {
    d = new Wj, d.ti = [], Yj(d, a.yb, b)
  }
  return d
}
function Yj(a, b, c) {
  var d;
  if(2 > c) {
    for(d = 0;d < c;d++) {
      b.pb[d] && a.ti.push(b.pb[d].Ba())
    }
  }
  for(d = 0;d < c;d++) {
    a.ti.push($j(b, d / (c - 1)))
  }
}
function $j(a, b) {
  for(var c = -1, d = -1, f = 1E7, g = -1, k = a.pb.length, l = 0;l < k;l++) {
    var n = a.pb[l], m = 255 * n.Aa / 255;
    if(m == b) {
      return n.Ba()
    }
    m < b && m > g && (c = l, g = m);
    m > b && m < f && (d = l, f = m)
  }
  if(-1 == c) {
    return-1 == d ? zb(p, 1) : a.pb[d].Ba()
  }
  if(-1 == d || g == f) {
    return a.pb[c].Ba()
  }
  g = 255 * a.pb[c].Aa / 255;
  f = a.pb[d];
  d = 1 - (b - g) / (255 * a.pb[d].Aa / 255 - g);
  g = new fb;
  c = a.pb[c].Ba().Ba();
  f = f.Ba().Ba();
  g.arguments[0] = c;
  g.arguments[1] = f;
  g.arguments[2] = d;
  return new xb(g.execute())
}
z.Ca = function(a) {
  a == h && (a = new Wj);
  Wj.f.Ca.call(this, a);
  a.Sa = this.Sa;
  a.vm = this.vm;
  a.yb = this.yb;
  a.Ra = this.Ra;
  a.wu = this.wu;
  return a
};
function ak() {
}
A.e(ak, Sj);
ak.prototype.g = function(a) {
  ak.f.g.call(this, a);
  for(var a = I(a, "item"), b = a.length, c = 0;c < b;c++) {
    this.ti.push(Gc(N(a[c], "type")))
  }
};
ak.prototype.Ca = function(a) {
  a == h && (a = new ak);
  ak.f.Ca.call(this, a);
  return a
};
function bk() {
}
A.e(bk, Sj);
bk.prototype.g = function(a) {
  bk.f.g.call(this, a);
  for(var a = I(a, "item"), b = a.length, c = 0;c < b;c++) {
    this.ti.push(Zf(N(a[c], "type")))
  }
};
bk.prototype.Ca = function(a) {
  a == h && (a = new bk);
  bk.f.Ca.call(this, a);
  return a
};
function ck() {
  this.b = new ne
}
z = ck.prototype;
z.Sa = p;
z.getName = u("Sa");
z.b = p;
z.g = function(a) {
  C(a, "name") && (this.Sa = J(a, "name"), this.b.add("%ThresholdName", this.Sa))
};
z.lt = function() {
  A.xa()
};
z.jt = function() {
  A.xa()
};
z.$I = x(q);
z.getData = function() {
  A.xa()
};
z.Ge = function() {
  this.b = new ne;
  this.Cb()
};
z.Cb = function() {
  A.xa()
};
z.HF = function() {
  A.xa()
};
z.Na = function(a) {
  return this.b.get(a)
};
z.pr = x(q);
function dk(a) {
  a = a.split("{").join("");
  return a.split("}").join("")
}
function ek() {
  this.Xa = zb("0#FF0000", 1);
  this.Sa = "condition";
  this.ba = [];
  this.Ge()
}
z = ek.prototype;
z.ew = p;
z.cE = q;
z.fw = p;
z.dE = q;
z.gw = p;
z.eE = q;
z.Sa = p;
z.Xa = p;
z.Ba = u("Xa");
z.ib = p;
z.as = t("ib");
z.ba = p;
z.km = p;
z.g = function(a) {
  C(a, "color") && (this.Xa = Yb(a));
  C(a, "name") && (this.Sa = J(a, "name"));
  var b;
  C(a, "value_1") && (b = J(a, "value_1"), this.cE = re(b), this.ew = b);
  C(a, "value_2") && (b = J(a, "value_2"), this.dE = re(b), this.fw = b);
  C(a, "value_3") && (b = J(a, "value_3"), this.eE = re(b), this.gw = b);
  if(C(a, "attributes")) {
    a = I(F(a, "attributes"), "attribute");
    this.km = {};
    for(b = 0;b < a.length;b++) {
      var c = a[b];
      C(c, "name") && (this.km["%Threshold" + J(c, "name")] = J(c, "value"), this.km["%Condition" + J(c, "name")] = J(c, "value"))
    }
  }
  this.HF()
};
z.Ed = function(a) {
  var b = this.cE ? a.Na(fk(this.ew)) : this.ew, c = this.dE ? a.Na(fk(this.fw)) : this.fw, d = this.eE ? a.Na(fk(this.gw)) : this.gw;
  if(this.al(isNaN(Number(b)) ? b : Number(b), isNaN(Number(c)) ? c : Number(c), isNaN(Number(d)) ? d : Number(d))) {
    a.Di(this.Xa), a.Cg = this, this.ba.push(a)
  }
};
function fk(a) {
  a = a.split("{").join("");
  return a.split("}").join("")
}
z.al = function() {
  A.xa()
};
z.Ge = function() {
  this.b = new ne;
  this.Cb()
};
z.Cb = function() {
  this.b.add("%ThresholdConditionName", "");
  this.b.add("%ThresholdName", "")
};
z.Pa = x(1);
z.HF = function() {
  this.b.add("%Name", this.Sa);
  this.b.add("%ThresholdConditionName", this.Sa);
  this.b.add("%ThresholdName", this.Sa)
};
z.Na = function(a) {
  return this.km && this.km[a] ? this.km[a] : this.b.get(a)
};
z.pr = function(a) {
  return Boolean(this.km && this.km[a])
};
z.ah = s();
z.dh = s();
function gk() {
  ek.apply(this)
}
A.e(gk, ek);
gk.prototype.al = function(a, b, c) {
  return a >= b && a <= c
};
function hk() {
  ek.apply(this)
}
A.e(hk, ek);
hk.prototype.al = function(a, b, c) {
  return a < b || a > c
};
function ik() {
  ek.apply(this)
}
A.e(ik, ek);
ik.prototype.al = function(a, b) {
  return a == b
};
function jk() {
  ek.apply(this)
}
A.e(jk, ek);
jk.prototype.al = function(a, b) {
  return a != b
};
function kk() {
  ek.apply(this)
}
A.e(kk, ek);
kk.prototype.al = function(a, b) {
  return a > b
};
function lk() {
  ek.apply(this)
}
A.e(lk, ek);
lk.prototype.al = function(a, b) {
  return a >= b
};
function mk() {
  ek.apply(this)
}
A.e(mk, ek);
mk.prototype.al = function(a, b) {
  return a < b
};
function nk() {
  ek.apply(this)
}
A.e(nk, ek);
nk.prototype.al = function(a, b) {
  return a <= b
};
function ok() {
  ck.apply(this)
}
A.e(ok, ck);
z = ok.prototype;
z.wm = p;
z.g = function(a) {
  ok.f.g.call(this, a);
  this.wm = [];
  for(var a = I(a, "condition"), b = 0;b < a.length;b++) {
    var c;
    c = a[b];
    if(C(c, "type")) {
      var d = h;
      switch(N(c, "type")) {
        default:
        ;
        case "lessthan":
          d = new mk;
          break;
        case "between":
          d = new gk;
          break;
        case "notbetween":
          d = new hk;
          break;
        case "equalto":
          d = new ik;
          break;
        case "notequalto":
          d = new jk;
          break;
        case "greaterthan":
          d = new kk;
          break;
        case "greaterthanorequalto":
          d = new lk;
          break;
        case "lessthanorequalto":
          d = new nk
      }
      d.as(this);
      d.g(c);
      c = d
    }else {
      c = p
    }
    c && this.wm.push(c)
  }
};
z.lt = function(a) {
  for(var b = 0;b < this.wm.length && !this.wm[b].Ed(a);b++) {
  }
};
z.jt = s();
z.getData = function(a) {
  for(var b = this.wm.length, c = 0;c < b;c++) {
    var d = this.wm[c];
    pk(a, new qk(d.Ba(), 0, -1, j, d))
  }
};
z.Pa = x(1);
z.pr = function(a) {
  for(var b = this.wm.length, c = 0;c < b;c++) {
    if(this.wm[c].pr(a)) {
      return j
    }
  }
  return q
};
function rk() {
  this.ba = [];
  this.Ge()
}
z = rk.prototype;
z.Ub = p;
z.setStart = t("Ub");
z.Lb = p;
z.setEnd = t("Lb");
z.ba = p;
z.ze = p;
z.contains = function(a) {
  return a >= this.Ub && a < this.Lb
};
z.b = p;
z.Ge = function() {
  this.b = new ne;
  this.Cb()
};
z.Cb = function() {
  this.b.add("%ThresholdRangeMin", 0);
  this.b.add("%ThresholdRangeMax", 0);
  this.b.add("%ThresholdValue", 0)
};
z.HF = function() {
  this.b.add("%ThresholdRangeMin", this.Ub);
  this.b.add("%ThresholdRangeMax", this.Lb)
};
z.Na = function(a) {
  return"%RangeMin" == a ? this.Ub : "%RangeMax" == a ? this.Lb : this.b.get(a)
};
z.Pa = x(2);
z.pr = x(q);
z.ah = s();
z.dh = s();
function sk() {
  ck.apply(this)
}
A.e(sk, ck);
z = sk.prototype;
z.ze = p;
z.ie = p;
z.he = p;
z.hf = p;
z.Vc = p;
z.qt = p;
z.wb = p;
z.g = function(a) {
  sk.f.g.call(this, a);
  this.ie = Number.MAX_VALUE;
  this.he = -Number.MAX_VALUE;
  this.wb = C(a, "range_count") ? M(a, "range_count") : 5;
  this.ze = C(a, "auto_value") ? J(a, "auto_value") : "%Value"
};
z.p = function() {
  this.ie == Number.MAX_VALUE && (this.ie = 0);
  this.he == -Number.MAX_VALUE && (this.he = 10);
  this.Vc = []
};
z.jt = function(a) {
  a = tk(a, this.ze);
  a < this.ie && (this.ie = a);
  a > this.he && (this.he = a)
};
function uk(a) {
  for(var b = (a.he - a.ie) / a.wb, c = 0;c < a.wb;c++) {
    var d = new rk;
    d.setStart(0 == c ? a.ie + b * c : a.Vc[c - 1].Lb);
    d.setEnd(d.Ub + b);
    d.ze = a.ze;
    a.Vc[c] = d
  }
}
function vk(a, b) {
  var c = tk(b, a.ze);
  if(c < a.ie || c > a.he) {
    return-1
  }
  for(var d = 0;d < a.wb;d++) {
    if(a.Vc[d] && a.Vc[d].contains(c)) {
      return d
    }
  }
  return c == a.Vc[a.wb - 1].Lb ? a.wb - 1 : -1
}
function tk(a, b) {
  var c = a.Na(dk(b));
  if("" == c) {
    return p
  }
  c = Ob(c);
  return isNaN(c) ? p : c
}
z.$I = x(j);
z.getData = function(a) {
  for(var b = this.Vc.length, c = 0;c < b;c++) {
    pk(a, new qk(Tj(this.qt, c), 0, -1, j, this.Vc[c]))
  }
};
function wk(a, b) {
  a && (this.j = a);
  b && (this.Be = b)
}
wk.prototype.j = p;
wk.prototype.Vl = t("j");
wk.prototype.Be = p;
function xk() {
  sk.apply(this);
  this.Jc = []
}
A.e(xk, sk);
xk.prototype.Jc = p;
xk.prototype.lt = function(a) {
  var b = vk(this, a);
  -1 != b && (a.Cg = this.Vc[b], this.Vc[b].ba.push(a), a.Di(Tj(this.qt, b)))
};
xk.prototype.jt = function(a) {
  var b = tk(a, this.ze);
  b < this.ie && (this.ie = b);
  b > this.he && (this.he = b);
  isNaN(b) || this.Jc.push(new wk(a, b))
};
function yk(a, b, c) {
  a += 0.5 * (b - a);
  if(0 == a) {
    return 0
  }
  b = Math.floor(Math.log(Math.abs(a)) / Math.log(10));
  b = Math.pow(10, Number(b - 1));
  a /= b;
  a = c ? Math.floor(a) : Math.ceil(a);
  return a * b
}
function zk() {
  sk.apply(this)
}
A.e(zk, sk);
zk.prototype.p = function() {
  zk.f.p.call(this);
  if(this.ie != this.he) {
    var a = Math.pow(10, Math.log(Math.max(Math.abs(this.ie), Math.abs(this.he))) / Math.log(10) - 1);
    this.ie = Math.floor(this.ie / a);
    var b = 10 * this.ie;
    this.ie *= a;
    this.he = Math.ceil(this.he / a);
    this.he += Number(this.wb - (10 * this.he - b) % this.wb) / 10;
    this.he *= a
  }
  this.hf = (this.he - this.ie) / this.wb;
  uk(this)
};
zk.prototype.lt = function(a) {
  var b = vk(this, a);
  -1 != b && (this.Vc[b].ba.push(a), a.Cg = this.Vc[b], b = dk(this.ze), b = a.b.get(b), a.Di(Tj(this.qt, Math.floor((b - this.ie) / this.hf))))
};
function Ak() {
  xk.apply(this)
}
A.e(Ak, xk);
Ak.prototype.p = function() {
  Ak.f.p.call(this);
  this.Jc.sort(function(a, b) {
    return a.Be - b.Be
  });
  var a = this.wb;
  if(0 == this.Jc.length) {
    uk(this)
  }else {
    if(a > this.Jc.length && (this.wb = a = this.Jc.length), 2 > a) {
      this.wb = a, this.ie = yk(this.ie, this.ie, j), this.he = yk(this.he, this.he, q), uk(this)
    }else {
      var b = this.Jc.length / a, c = b, d = this.Jc[0].Be, f = this.Jc[Math.round(c - 1)].Be, g = this.Jc[Math.round(c)].Be, k = new rk;
      k.setStart(yk(d, d, j));
      d = yk(f, g, j);
      k.setEnd(d);
      k.ze = this.ze;
      this.Vc[0] = k;
      for(k = 1;k < a - 1;k++) {
        c += b, this.Vc[k] = new rk, this.Vc[k].ze = this.ze, this.Vc[k].setStart(d), d = yk(this.Jc[Math.round(c - 1)].Be, this.Jc[Math.round(c)].Be, j), this.Vc[k].setEnd(d)
      }
      this.Vc[this.wb - 1] = new rk;
      this.Vc[this.wb - 1].setStart(d);
      this.Vc[this.wb - 1].ze = this.ze;
      this.Vc[this.wb - 1].setEnd(yk(this.Jc[this.Jc.length - 1].Be, this.Jc[this.Jc.length - 1].Be, q))
    }
  }
};
function Bk() {
  xk.apply(this)
}
A.e(Bk, xk);
Bk.prototype.p = function() {
  Bk.f.p.call(this);
  this.Jc.sort(function(a, b) {
    return a.Be - b.Be
  });
  if(0 == this.Jc.length) {
    uk(this)
  }else {
    var a = Math.min(this.wb, this.Jc.length);
    if(4 >= a) {
      this.wb = Math.max(this.wb, 1), uk(this)
    }else {
      if(a > this.Jc.length - 3 && (this.wb = this.Jc.length), 4 > a) {
        this.wb = Math.max(this.wb, 1), uk(this)
      }else {
        for(var a = [], b = 0;b < this.Jc.length;b++) {
          a.push(this.Jc[b].Be)
        }
        a = Ck(a, this.wb);
        b = new rk;
        b.ze = this.ze;
        var c = this.Jc[0].Be;
        b.setStart(yk(c, c, j));
        var c = this.Jc[a[0] - 1].Be, d = this.Jc[a[0]].Be, c = yk(c, d, j);
        b.setEnd(c);
        this.Vc[0] = b;
        for(var f = 1;f < Math.min(this.wb - 1, a.length);f++) {
          b = new rk, b.ze = this.ze, b.setStart(c), c = this.Jc[a[f] - 1].Be, d = this.Jc[a[f]].Be, c = yk(c, d, j), b.setEnd(c), this.Vc[f] = b
        }
        b = new rk;
        b.ze = this.ze;
        b.setStart(c);
        b.setEnd(yk(this.Jc[this.Jc.length - 1].Be, this.Jc[this.Jc.length - 1].Be, q));
        this.Vc[this.wb - 1] = b
      }
    }
  }
};
function Ck(a, b) {
  for(var c = [], d = [], f = 0;f < a.length + 1;f++) {
    c.push(Array(b + 1)), d.push(Array(b + 1))
  }
  for(f = 1;f <= b;f++) {
    c[1][f] = 1;
    d[1][f] = 0;
    for(var g = 2;g <= a.length;g++) {
      d[g][f] = Number.POSITIVE_INFINITY
    }
  }
  for(var k = 0, l = 2;l <= a.length;l++) {
    for(var n = 0, m = 0, o = 0, v = 1;v <= l;v++) {
      if(f = l - v, g = Number(a[f]), m += g * g, n += g, o++, k = m - n * n / o, g = f, 0 != g) {
        for(var w = 2;w <= b;w++) {
          d[l][w] >= k + d[g][w - 1] && (c[l][w] = f + 1, d[l][w] = k + d[g][w - 1])
        }
      }
    }
    c[l][1] = 1;
    d[l][1] = k
  }
  d = [];
  g = a.length;
  for(l = b;2 <= l;l--) {
    f = c[g][l] - 2, 0 < f && d.splice(0, 0, f), g = Math.max(c[g][l] - 1, 0)
  }
  d.push(a.length - 1);
  return d
}
function Dk(a) {
  this.iG = a;
  this.wz = {};
  this.ZE = [];
  this.qB = {};
  this.Dw = []
}
z = Dk.prototype;
z.iG = p;
z.wz = p;
z.ZE = p;
z.Dw = p;
z.qB = p;
function Ek(a) {
  for(var b = 0;b < a.Dw.length;b++) {
    a.Dw[b].p()
  }
}
z.Kg = function(a, b) {
  if(!this.iG) {
    return p
  }
  a = Cb(a);
  if(this.wz[a]) {
    return this.wz[a]
  }
  var c = fc(this.iG, a);
  if(!c) {
    return p
  }
  var d;
  switch(N(c, "type")) {
    default:
    ;
    case "custom":
      d = new ok;
      break;
    case "equalinterval":
    ;
    case "equalsteps":
      d = new zk;
      break;
    case "quantiles":
    ;
    case "equaldistribution":
      d = new Ak;
      break;
    case "optimal":
    ;
    case "absolutedeviation":
      d = new Bk
  }
  if(!d) {
    return p
  }
  var f = a;
  d.g(c);
  if(d.$I()) {
    var g = new Wj, k;
    C(c, "palette") && (c = F(c, "palette"), k = b ? Vj(b, "palette", c) : p);
    k || (k = Vj(b, "palette", "default"));
    g.g(k);
    c = Zj(g, d.wb, j);
    d.qt = c;
    this.Dw.push(d);
    this.qB[f] = d
  }
  this.ZE.push(d);
  return this.wz[a] = d
};
function Fk() {
  jf.apply(this);
  this.xw = this.jm = q;
  this.pk = j;
  this.Ua = [];
  this.Kh = {};
  this.Hp = {};
  this.iy = {};
  this.o = [];
  this.rp = [];
  this.CJ = new Gk(this)
}
A.e(Fk, jf);
z = Fk.prototype;
z.bk = 0;
z.Jo = u("bk");
z.o = p;
z.rp = p;
function Hk(a) {
  return a.o ? a.o.length : 0
}
function Ik(a, b) {
  return a.o ? a.o[b] : p
}
z.Gd = x(p);
z.iy = p;
z.cy = aa();
z.Mx = function(a, b, c, d) {
  var f = Jk(this, c);
  if(f) {
    return f
  }
  f = this.jx(c);
  if(f == p) {
    return p
  }
  f.Ul(this);
  f.cA(this.N);
  Kk(f, b);
  f.hl(b);
  a = F(a, f.Ah());
  f.hl(a);
  Kk(f, a);
  f.g(a, d);
  f.Eq(a, d);
  f.Dm(a, d);
  this.iy[this.cy(this.cy(c))] = f;
  this.pk || f.MD();
  return f
};
function Jk(a, b) {
  var c = a.cy(b);
  return a.iy[c] ? a.iy[c] : p
}
function Lk(a, b, c, d) {
  var f = p;
  C(c, b) && (f = Vj(d, b, F(c, b)));
  f == p && (f = Vj(d, b, "default"));
  a = new a;
  a.g(f);
  return a
}
function Mk(a, b, c, d, f, g) {
  if(C(f, b)) {
    if(b = Vj(g, b, F(f, b)), b != p) {
      return a = new a, a.g(b), a
    }
  }else {
    return d.Aw ? c : p
  }
  return p
}
z.Li = p;
z.CJ = p;
jf.prototype.SH = function(a, b, c, d, f) {
  b.Ba() || b.Di(a.color);
  a = Mc(c.r(), d, f, a.Ka(), a.vb());
  d = "stroke:none;";
  d = b.Ba() ? d + ("fill:" + ub(b.Ba().Ba()) + ";") : d + "fill:none;";
  a.setAttribute("style", d);
  c.appendChild(a)
};
z = Fk.prototype;
z.Xz = p;
z.Dq = function(a) {
  Fk.f.Dq.call(this, a);
  C(a, "default_series_type") && (this.bk = this.Jo(Cb(F(a, "default_series_type"))));
  C(a, "ignore_missing") && (this.pk = K(a, "ignore_missing"));
  kf(this, a)
};
z.vx = s();
z.RG = function(a, b) {
  return b
};
z.RE = function(a) {
  this.pk || a.Ab().XI(a);
  if(a.Ab().Xo && a.Ab().bt()) {
    Nk(a.Ab(), a);
    for(var b = 0;b < a.Fe();b++) {
      a.Ga(b).Xr(b)
    }
  }
  a.th(this)
};
z.Xw = function(a) {
  if(!C(a, "data")) {
    return j
  }
  a = F(a, "data");
  return!C(a, "series") ? j : 0 == I(a, "series").length
};
z.ux = function(a, b) {
  Fk.f.ux.call(this, a);
  this.Li = new Dk(F(a, "thresholds"));
  var c = new Uj;
  C(a, "palettes") && c.g(F(a, "palettes"));
  var d = F(a, "data"), f = F(a, "data_plot_settings"), g = I(d, "series"), k = g.length, l = Lk(Wj, "palette", d, c), n = Lk(bk, "marker_palette", d, c), m = Lk(ak, "hatch_palette", d, c), l = l ? Zj(l, k) : p, o = p;
  C(d, "threshold") && (o = this.Li.Kg(N(d, "threshold"), c));
  for(var v = 0;v < k;v++) {
    var w = g[v];
    if(!C(w, "visible") || K(w, "visible")) {
      var y = C(w, "type") ? this.Jo(N(w, "type")) : this.bk, E = this.Mx(f, d, y, b);
      if(E != p) {
        y = E.uh(y);
        y.Xr(v);
        y.Sl("Series " + v.toString());
        Ok(y, w);
        y.cb = E;
        Pk(y, E.Qe);
        Kk(y, w);
        if(C(w, "missing_points")) {
          var V = J(w, "missing_points");
          V && (y.FE = V)
        }
        y.rf(E.ka());
        y.Eq(w, b);
        Qk(y, E);
        y.hl(w);
        C(w, "color") ? y.Di(Yb(w)) : y.Di(Tj(l, v));
        C(w, "hatch_type") ? y.Ip(Gc(N(w, "hatch_type"))) : y.Ip(Tj(m, v));
        y.ge = C(w, "marker_type") ? Zf(N(w, "marker_type")) : Tj(n, v);
        C(w, "threshold") ? y.as(this.Li.Kg(N(w, "threshold"), c)) : y.as(o);
        E.Mh(y);
        y.Dm(w, b);
        y.g(w);
        this.vx(y, w);
        var V = Mk(Wj, "palette", l, E, w, c), R = Mk(bk, "marker_palette", n, E, w, c), T = Mk(ak, "hatch_palette", n, E, w, c);
        H(w, "point", this.RG(y, I(w, "point")));
        for(var ca = y.jB(), w = I(w, "point"), ra = w.length, V = V ? Zj(V, ra) : p, Pc = y.FE ? y.FE : p, yb = 0;yb < ra;yb++) {
          var Y = w[yb];
          "no_paint" == Pc && !Y.y && !Y.start && !Y.end && (Y.y = 0, Y.$o = j);
          var L = y.qe();
          L.cb = E;
          L.rf(y.ka());
          L.Eq(Y, b);
          Pk(L, y.Qe);
          Kk(L, Y);
          Qk(L, y);
          L.hl(Y);
          L.Xr(yb);
          L.o = y;
          L.Sl(yb.toString());
          Ok(L, Y);
          L.g(Y, this.F);
          if(!L.Gb || E.ZG()) {
            this.pk || E.cH(L);
            y.Mh(L);
            L.Dm(Y, b);
            L.Di(y.Ba());
            L.Ip(y.Ze);
            L.ge = y.mg();
            C(Y, "color") ? L.Di(Yb(Y)) : L.Di(V ? Tj(V, L.Nr) : y.Ba());
            C(Y, "hatch_type") ? L.Ip(Gc(N(Y, "hatch_type"))) : L.Ip(T ? Tj(T, L.Nr) : y.Ze);
            L.ge = C(Y, "marker_type") ? Zf(N(Y, "marker_type")) : R ? Tj(R, L.Nr) : y.mg();
            Y.$o && (L.ge = -1);
            C(Y, "threshold") ? L.as(this.Li.Kg(N(Y, "threshold"), c)) : L.as(y.Kg());
            y.Pd(L, L.na);
            L.Gb || y.Ed(L);
            if(ca && (!L.Gb || !this.pk)) {
              L.ib != p && L.Kg().jt(L), this.Ua.push(L)
            }
            Y.$o && (L.$o = j)
          }
        }
        this.RE(y);
        y.un() && this.rp.push(y);
        this.o.push(y)
      }
    }
  }
  this.Li && Ek(this.Li);
  this.b.add("%DataPlotSeriesCount", this.o.length);
  if(this.jm && this.xw && (c = this.F.Nc(), this.jm && this.xw && this.N.YH && (d = new Rk(this)))) {
    A.G.nb(c, ka.Yn, d.RM, q, this), A.G.nb(c, ka.gm, d.QM, q, this), A.G.nb(c, ka.Ps, d.SM, q, this), A.G.nb(this.N, "anychartRefresh", d.OM, q, this)
  }
};
z.Pa = function(a) {
  return"%DataPlotSeriesCount" == a ? 2 : Fk.f.Pa.call(this, a)
};
z.br = x(p);
z.pk = j;
z.xw = q;
z.jm = q;
z.Lf = p;
z.Ek = p;
z.EI = function(a, b) {
  for(var c = [], d = 0;d < b.length;d++) {
    var f = b[d];
    f.kn(a) && c.push(f)
  }
  return c
};
function Rk(a) {
  function b(b) {
    window._anychart_svg_tmp = a.F.Nc();
    b.Jm.preventDefault();
    var c = eval("_anychart_svg_tmp.createSVGPoint()");
    window._anychart_svg_pt_tmp = c;
    b = b.Jm;
    c.x = b.clientX;
    c.y = b.clientY;
    window._anychart_svg_tt_tmp = eval("_anychart_svg_tmp.getScreenCTM().inverse()");
    c = eval("_anychart_svg_pt_tmp.matrixTransform(_anychart_svg_tt_tmp)");
    window._anychart_svg_pt_tmp = h;
    window._anychart_svg_tt_tmp = h;
    window._anychart_svg_tmp = h;
    return c
  }
  function c() {
    return 0 < k.width && 0 < k.height ? k : {x:0 > k.width ? k.x + k.width : k.x, y:0 > k.height ? k.y + k.height : k.y, width:0 > k.width ? -k.width : k.width, height:0 > k.height ? -k.height : k.height}
  }
  function d(a) {
    a = b(a);
    k.width = a.x - k.x;
    k.height = a.y - k.y
  }
  var f, g = q, k = {x:0, y:0, width:0, height:0}, l = p;
  this.RM = function(b) {
    f && (d(b), g ? l ? (b = c(), l.setAttribute("x", b.x), l.setAttribute("y", b.y), l.setAttribute("width", b.width), l.setAttribute("height", b.height)) : g = f = q : (l || (b = c(), l = Mc(a.F, b.x, b.y, b.width, b.height), l.setAttribute("style", "fill:rgb(180, 216, 247);stroke-width=1;stroke:rgb(0,0,0);fill-opacity:0.5;stroke-opacity:0.5;"), a.N.ft.appendChild(l)), l.setAttribute("visibility", "visible"), g = j))
  };
  this.QM = function(a) {
    f = j;
    a = b(a);
    k = {x:a.x, y:a.y, width:0, height:0}
  };
  this.SM = function(b) {
    if(l) {
      if(f = q, g) {
        d(b);
        g = q;
        var b = a.N.hb.aa.Ua, m = c(), o = a.N.hb.aa.k;
        m.x -= o.x;
        m.y -= o.y;
        m = a.EI(new P(m.x, m.y, m.width, m.height), b, l);
        l.setAttribute("visibility", "hidden");
        l.setAttribute("x", 0);
        l.setAttribute("y", 0);
        l.setAttribute("width", 0);
        l.setAttribute("height", 0);
        k = {x:0, y:0, width:0, height:0};
        if(m.length && (b = a.N.hb.aa, b.jm && m && m.length)) {
          b.Lf == p && (b.Lf = []);
          for(o = 0;o < m.length;o++) {
            var v = m[o];
            v.$o || (v.select(j, j), b.Kh[h] = Sk(v.YR, v.OR), b.Lf.push(v))
          }
          if(b.Lf && b.Lf.length) {
            b.N.dispatchEvent(new Je("multiplePointsSelect", b.Lf));
            for(m = 0;m < b.Lf.length;m++) {
              b.Lf[m].update()
            }
          }
        }
      }
    }else {
      g = f = q
    }
  };
  this.OM = function() {
    l = p
  }
}
z.tj = function(a, b) {
  this.jm ? (this.Lf == p && (this.Lf = []), -1 == this.Lf.indexOf(a) && this.Lf.push(a)) : this.Ek != a && (this.Ek != p && this.Ek.tx(b), this.Ek = a, b && this.Ek.update())
};
z.p = function(a, b, c) {
  Fk.f.p.call(this, a, b, c);
  a = 0;
  for(b = this.Ua.length;a < b;a++) {
    this.xt.appendChild(this.Ua[a].p(this.F).I)
  }
  return this.D
};
z.zp = function() {
  if(this.PD && !this.fF && (this.fF = j, this.Ek && this.Ek.Qe && this.Ek.Qe.execute(this.Ek), this.Lf)) {
    for(var a = this.Lf.length, b = 0;b < a;b++) {
      this.Lf[b].Qe && this.Lf[b].Qe.execute(this.Lf[b])
    }
  }
};
z.Ua = p;
z.qa = function() {
  Fk.f.qa.call(this);
  a: {
    var a, b;
    if(this.o && this.o.length) {
      for(a = 0;a < this.o.length;a++) {
        if(b = this.o[a], b instanceof Tk && b.ba && 1 == b.ba.length) {
          break a
        }
      }
    }
    if(!(1 == this.Ua.length && this.Ua[0].o instanceof Tk)) {
      a = 0;
      for(b = this.rp.length;a < b;a++) {
        this.rp[a].Ug()
      }
      a = 0;
      for(b = this.Ua.length;a < b;a++) {
        var c = this.Ua[a];
        c.ib && c.ib.lt(c);
        this.Ua[a].update()
      }
    }
  }
};
z.Jq = function() {
  Fk.f.Jq.call(this);
  this.Nz()
};
z.Nz = function() {
  var a, b;
  a = 0;
  for(b = this.rp.length;a < b;a++) {
    this.rp[a].An()
  }
  a = 0;
  for(b = this.Ua.length;a < b;a++) {
    this.Ua[a].Wa()
  }
};
z.mb = function(a) {
  a = Fk.f.mb.call(this, a);
  a.Series = [];
  for(var b = 0, c = Hk(this), d = 0;d < c;d++) {
    a.Series.push(this.o[d].mb()), b += this.o[d].Fe()
  }
  a.SeriesCount = c;
  a.PointCount = b;
  return a
};
z.Pd = function(a, b) {
  if(a && b) {
    var c = Uk(this, a);
    if(c) {
      if(b = Vk(b), c = Wk(c), A.isArray(b)) {
        for(var d = b.length, f = 0;f < d;f++) {
          c.push(b[f]), this.Kh[Sk(a, b)] = b
        }
      }else {
        A.hn(b) && (c.push(b), this.Kh[Sk(a, b)] = b)
      }
    }
  }
};
z.Kj = function(a, b, c) {
  if(a && !(b == p || b == h || !c)) {
    var d = Uk(this, a);
    if(d) {
      var c = Vk(c), f = Wk(d), f = ic(f, b, c);
      this.Kh[Sk(a, c)] = c;
      H(d, "point", f)
    }
  }
};
z.Bk = function(a, b) {
  var c = Uk(this, a);
  if(c) {
    for(var c = jc(Wk(c), b), d = c.length, f = 0;f < d;f++) {
      var g = Sk(a, c[f]);
      this.Kh[g] && delete this.Kh[g]
    }
  }
};
z.Nk = function(a, b, c) {
  var d;
  if(!a || !b) {
    d = p
  }else {
    if(d = Sk(a, b), this.Kh[d]) {
      d = this.Kh[d]
    }else {
      var f = Uk(this, a), f = I(f, "point"), f = hc(f, "id", b);
      d = this.Kh[d] = f
    }
  }
  d && (c = Vk(c), kc(d, c), this.Kh[h] = Sk(a, b))
};
z.Ok = s();
z.mk = function(a, b, c) {
  var d = Xk(this, a, b);
  d && (c ? d.ah() : d.dh(), this.Kh[h] = Sk(a, b))
};
z.xK = function(a, b, c) {
  var d = Xk(this, a, b);
  if(!d) {
    return p
  }
  c ? d.select(j) : d.tx(j);
  this.Kh[h] = Sk(a, b);
  return d
};
function Xk(a, b, c) {
  a = Yk(a, b);
  if(!a) {
    return p
  }
  for(var b = a.Fe(), d = 0;d < b;d++) {
    var f = a.Ga(d);
    if(f.au() && f.au() == c) {
      return f
    }
  }
  return p
}
z.Lj = function(a) {
  if(a) {
    var a = Vk(a), b = Zk(this);
    if(A.isArray(a)) {
      for(var c = a.length, d = 0;d < c;d++) {
        b.push(a[d]), $k(this, a[d])
      }
    }else {
      A.hn(a) && (b.push(a), $k(this, a))
    }
    al(this, b)
  }
};
z.Mj = function(a, b) {
  if(!(a == p || a == h || !b)) {
    var b = Vk(b), c = Zk(this);
    ic(c, a, b);
    al(this, c);
    $k(this, b)
  }
};
z.Ck = function(a) {
  if(!(a == p || a == h)) {
    var b = Zk(this);
    jc(b, a);
    al(this, b);
    this.Hp[a] && delete this.Hp[a]
  }
};
z.Pk = function(a, b) {
  if(a && b) {
    var b = Vk(b), c = Uk(this, a);
    c && (kc(c, b), $k(this, b, a))
  }
};
z.Ik = function(a, b) {
  if(a && !(b == p || b == h)) {
    var c = Uk(this, a);
    c && (H(c, "visible", Cb(b)), $k(this, c, a))
  }
};
z.nk = function(a, b) {
  if(a && !(b == p || b == h)) {
    var c = Yk(this, a);
    c && (b ? c.ah(j) : c.dh(j))
  }
};
function Yk(a, b) {
  for(var c = Hk(a), d = 0;d < c;d++) {
    var f = Ik(a, d);
    if(f.au() && f.au() == b) {
      return f
    }
  }
  return p
}
z.clear = function() {
  H(F(this.Sj, "data"), "series", [])
};
z.refresh = function() {
  this.rm.Fk(this.Sj)
};
z.Hp = p;
function Uk(a, b) {
  if(!b) {
    return p
  }
  if(a.Hp[b]) {
    return a.Hp[b]
  }
  var c = Zk(a), d = hc(c, "id", b);
  al(a, c);
  $k(a, d, b);
  return d
}
function $k(a, b, c) {
  c ? a.Hp[c] = b : C(b, "id") && (a.Hp[F(b, "id")] = b)
}
function Zk(a) {
  var b, c;
  C(a.Sj, "data") ? b = F(a.Sj, "data") : (b = {}, H(a.Sj, "data", b));
  C(b, "series") ? c = I(b, "series") : H(b, "series", []);
  return c
}
function Wk(a) {
  if(C(a, "point")) {
    return I(a, "point")
  }
  var b = [];
  H(a, "point", b);
  return b
}
function al(a, b) {
  H(F(a.Sj, "data"), "series", b)
}
z.Kh = p;
function Sk(a, b) {
  var c, d;
  c = A.hn(a) ? J(a, "id") : a;
  A.hn(b) ? c = J(b, "id") : d = b;
  return!a || !b ? p : "series:" + c + ":point:" + d
}
function Vk(a) {
  if(A.Qc(a)) {
    return Jb(a)
  }
  if(A.hn(a)) {
    return a
  }
}
;var bl = {chart:{palettes:{hatch_palette:{item:[{type:"backwardDiagonal"}, {type:"forwardDiagonal"}, {type:"horizontal"}, {type:"vertical"}, {type:"dashedBackwardDiagonal"}, {type:"grid"}, {type:"dashedForwardDiagonal"}, {type:"dashedHorizontal"}, {type:"dashedVertical"}, {type:"diagonalCross"}, {type:"diagonalBrick"}, {type:"divot"}, {type:"horizontalBrick"}, {type:"verticalBrick"}, {type:"checkerboard"}, {type:"confetti"}, {type:"plaid"}, {type:"solidDiamond"}, {type:"zigzag"}, {type:"weave"}, 
{type:"percent05"}, {type:"percent10"}, {type:"percent20"}, {type:"percent25"}, {type:"percent30"}, {type:"percent40"}, {type:"percent50"}, {type:"percent60"}, {type:"percent70"}, {type:"percent75"}, {type:"percent80"}, {type:"percent90"}], name:"default"}, marker_palette:{item:[{type:"Circle"}, {type:"Square"}, {type:"Diamond"}, {type:"Star4"}, {type:"Star5"}, {type:"Star7"}, {type:"TriangleUp"}, {type:"TriangleDown"}, {type:"Cross"}, {type:"DiagonalCross"}, {type:"HLine"}, {type:"VLine"}, {type:"Star6"}, 
{type:"Star10"}], name:"default"}, palette:{item:[{color:"#1D8BD1"}, {color:"#F1683C"}, {color:"#2AD62A"}, {color:"#DBDC25"}, {color:"#8FBC8B"}, {color:"#D2B48C"}, {color:"#FAF0E6"}, {color:"#20B2AA"}, {color:"#B0C4DE"}, {color:"#DDA0DD"}, {color:"#9C9AFF"}, {color:"#9C3063"}, {color:"#FFFFCE"}, {color:"#CEFFFF"}, {color:"#630063"}, {color:"#FF8284"}, {color:"#0065CE"}, {color:"#CECFFF"}, {color:"#000084"}, {color:"#FF00FF"}, {color:"#FFFF00"}, {color:"#00FFFF"}, {color:"#840084"}, {color:"#840000"}, 
{color:"#008284"}, {color:"#0000FF"}, {color:"#00CFFF"}, {color:"#CEFFFF"}, {color:"#CEFFCE"}, {color:"#FFFF9C"}, {color:"#9CCFFF"}, {color:"#FF9ACE"}, {color:"#CE9AFF"}, {color:"#FFCF9C"}, {color:"#3165FF"}, {color:"#31CFCE"}, {color:"#9CCF00"}, {color:"#FFCF00"}, {color:"#FF9A00"}, {color:"#FF6500"}], name:"default"}}, styles:{animation_style:[{name:"anychart_default", enabled:"true", interpolation_type:"Cubic"}, {name:"defaultLabel", enabled:"True", start_time:"1.2", duration:"0.5", interpolation_type:"Cubic"}, 
{name:"defaultMarker", enabled:"True", start_time:"1.2", duration:"0.5", type:"Appear", interpolation_type:"Cubic"}, {name:"defaultScaleYBottom", enabled:"True", start_time:"0.2", type:"ScaleYBottom", duration:"1", interpolation_type:"Cubic"}, {name:"defaultSideYBottom", enabled:"True", start_time:"0.2", type:"SideFromBottom", duration:"1", interpolation_type:"Cubic"}, {name:"defaultScaleYCenter", enabled:"True", start_time:"0.2", type:"ScaleYCenter", duration:"1", interpolation_type:"Cubic"}, {name:"defaultScaleXYCenter", 
enabled:"True", start_time:"0.2", type:"ScaleXYCenter", duration:"1", interpolation_type:"Cubic"}, {name:"defaultPie", enabled:"True", start_time:"0.2", type:"OutSide", duration:"2", show_mode:"Smoothed", smoothed_delay:"0.5", interpolation_type:"Cubic"}, {name:"defaultChart", enabled:"False", type:"Appear", interpolation_type:"Cubic", start_time:"0", duration:"0.5"}], marker_style:{marker:{type:"%MarkerType", size:"6", anchor:"top"}, fill:{color:"LightColor(%Color)"}, border:{color:"DarkColor(%Color)", 
thickness:"1"}, effects:{drop_shadow:{enabled:"false", distance:"1.5", opacity:"0.15"}}, states:{hover:{marker:{size:"10"}, fill:{color:"LightColor(%Color)"}, effects:{glow:{enabled:"true", blur_x:"2", blur_y:"2", opacity:"0.8"}}}, missing:{fill:{type:"Solid", color:"White", opacity:"0"}, border:{thickness:"1", color:"White", opacity:"1"}}}, name:"anychart_default"}, label_style:[{format:{value:"{%Value}"}, position:{anchor:"Top", halign:"Center", valign:"Top", padding:"5"}, font:{family:"Verdana", 
size:"10", color:"rgb(35,35,35)", bold:"True"}, background:{border:{gradient:{key:[{position:"0", color:"rgb(221,221,221)"}, {position:"1", color:"rgb(208,208,208)"}], angle:"90"}, enabled:"True", type:"Gradient"}, fill:{gradient:{key:[{position:"0", color:"rgb(255,255,255)"}, {position:"0.5", color:"rgb(243,243,243)"}, {position:"1", color:"rgb(255,255,255)"}], angle:"90"}, type:"Gradient"}, inside_margin:{all:"3"}, effects:{drop_shadow:{enabled:"True", distance:"1", opacity:"0.1"}, enabled:"True"}, 
enabled:"false"}, name:"anychart_default"}, {format:{value:"{%Value}"}, position:{anchor:"Top", halign:"Center", valign:"Top", padding:"5"}, font:{family:"Verdana", size:"10", color:"DarkColor(%Color)", bold:"True"}, background:{border:{gradient:{key:[{position:"0", color:"DarkColor(%Color)"}, {position:"1", color:"DarkColor(%Color)"}], angle:"90"}, enabled:"True", type:"Gradient"}, fill:{gradient:{key:[{position:"0", color:"rgb(255,255,255)"}, {position:"0.5", color:"rgb(243,243,243)"}, {position:"1", 
color:"rgb(255,255,255)"}], angle:"90"}, enabled:"true", type:"Gradient"}, inside_margin:{all:"3"}, effects:{drop_shadow:{enabled:"True", distance:"1", opacity:"0.1"}, enabled:"True"}, enabled:"true"}, name:"Default01"}, {font:{bold:"false"}, name:"Default02", parent:"Default01"}, {font:{color:"rgb(35,35,35)"}, name:"Default03", parent:"Default01"}, {font:{color:"rgb(35,35,35)", bold:"false"}, name:"Default04", parent:"Default01"}, {font:{color:"White", bold:"false"}, background:{border:{enabled:"false"}, 
fill:{enabled:"true", type:"Solid", color:"DarkColor(%Color)", opacity:"0.7"}, effects:{enabled:"false"}, inside_margin:{top:"1", bottom:"2"}, corners:{type:"Rounded", all:"2"}, enabled:"true"}, name:"Default05"}], tooltip_style:[{format:{value:"{%Value}{numDecimals:2}"}, position:{anchor:"Float", halign:"Center", valign:"Top", padding:"5"}, font:{family:"Verdana", size:"10", color:"rgb(35,35,35)", bold:"True", italic:"False", underline:"False"}, background:{border:{gradient:{key:[{position:"0", 
color:"rgb(221,221,221)", opacity:"1"}, {position:"1", color:"rgb(208,208,208)", opacity:"1"}], angle:"90", type:"Linear", focal_point:"0"}, enabled:"True", thickness:"1", type:"Gradient", opacity:"1", color:"rgb(0,0,0)", dashed:"False", dash_length:"5", space_length:"3", caps:"Round", joints:"Round"}, fill:{gradient:{key:[{position:"0", color:"rgb(255,255,255)", opacity:"1"}, {position:"0.5", color:"rgb(243,243,243)", opacity:"1"}, {position:"1", color:"rgb(255,255,255)", opacity:"1"}], angle:"90", 
type:"Linear", focal_point:"0"}, enabled:"True", type:"Gradient", color:"rgb(255,255,255)", opacity:"1", image_mode:"Stretch", image_url:""}, hatch_fill:{enabled:"False", type:"BackwardDiagonal", color:"rgb(0,0,0)", opacity:"1", thickness:"1", pattern_size:"10"}, inside_margin:{left:"10", top:"5", right:"10", bottom:"5"}, corners:{type:"Square", all:"10"}, effects:{drop_shadow:{enabled:"True", opacity:"0.3", angle:"45", color:"Black", distance:"2", blur_x:"2", blur_y:"2"}, enabled:"True"}, enabled:"True"}, 
name:"anychart_default", enabled:"true"}, {background:{enabled:"false"}, name:"Default01", enabled:"true"}, {position:{halign:"Center", valign:"Top", padding:"5"}, font:{color:"#575757"}, background:{fill:{gradient:{key:[{position:"0", color:"#FFFFFF"}, {position:"0.5", color:"#E4E5F0"}], angle:"90"}, type:"gradient"}, inside_margin:{left:"4", right:"4", top:"3", bottom:"3"}, border:{color:"#767676"}, effects:{drop_shadow:{enabled:"true", distance:"2"}}}, name:"Default02", enabled:"true"}]}, data_plot_settings:{}, 
chart_settings:{data_plot_background:{border:{color:"#B7B7B7"}, fill:{color:"#FFFFFF"}, effects:{drop_shadow:{enabled:"True", distance:"2", opacity:"0.2"}}}}}, defaults:{color_swatch:{labels:{format:{value:"{%Value}{numDecimals:2}"}, font:{family:"Verdana", size:"10", bold:"false", color:"#232323"}, padding:"0"}, background:{border:{gradient:{key:[{position:"0", color:"rgb(221,221,221)"}, {position:"1", color:"rgb(208,208,208)"}], angle:"90"}, type:"Gradient"}, fill:{gradient:{key:[{position:"0", 
color:"#FFFFFF"}, {position:"0.5", color:"rgb(243,243,243)"}, {position:"1", color:"#FFFFFF"}], angle:"90"}, type:"Gradient"}, inside_margin:{all:"5"}, effects:{drop_shadow:{enabled:"True", distance:"1", opacity:"0.1"}, enabled:"True"}, enabled:"true"}, tickmark:{thickness:"1", size:"4", color:"#232323"}, range_item:{background:{fill:{color:"%Color"}, states:{hover:{fill:{color:"LightColor(%Color)"}}}}}, title:{text:{value:"Color Swatch"}, enabled:"false"}, title_separator:{gradient:{key:[{position:"0", 
color:"#333333", opacity:"0"}, {position:"0.5", color:"#333333", opacity:"1"}, {position:"1", color:"#333333", opacity:"0"}]}, enabled:"true", type:"Gradient"}, orientation:"Horizontal", position:"bottom", align:"near", inside_dataplot:"false", align_by:"dataplot", padding:"5"}}};
function cl() {
}
A.e(cl, tf);
z = cl.prototype;
z.Sq = x("chart");
z.zh = function() {
  return this.$f(cl.f.zh.call(this), bl)
};
z.$f = function(a, b) {
  var c = cl.f.$f.call(this, a, b), d = this.Sq(), f = F(F(a, d), "data"), g = F(F(b, d), "data");
  if(f != p || g != p) {
    (f = gc(f, g, "series")) && H(F(c, d), "data", f)
  }
  return c
};
z.DE = function(a, b) {
  var c = cl.f.DE.call(this, a, b), d = F(F(a, this.Sq()), "data"), f = F(b, "data");
  if(f) {
    if(d) {
      var g = $b(d, f, "data", "series");
      H(c, "data", g);
      var f = I(f, "series"), k = f.length, l = [];
      H(g, "series", l);
      for(g = 0;g < k;g++) {
        var n = f[g];
        if(C(n, "name")) {
          var m = fc(I(d, "series"), F(n, "name"));
          m && (l[g] = $b(m, n, "series", "point"))
        }else {
          l[g] = n
        }
      }
    }else {
      c.data = f
    }
  }
  return c
};
z.rn = function(a, b, c) {
  cl.f.rn.call(this, a, b, c);
  var d = $b(F(a, "data_plot_settings"), F(b, "data_plot_settings"));
  d != p && (c.data_plot_settings = d);
  var f, d = cc(F(a, "palettes"), F(b, "palettes"));
  if(d != p) {
    for(f in c.palettes = d, c.palettes) {
      C(c.palettes[f], "name") && (c.palettes[f].name = Cb(F(c.palettes[f], "name")))
    }
  }
  a = cc(F(a, "thresholds"), F(b, "thresholds"));
  if(a != p) {
    for(f in c.thresholds = a, c.thresholds) {
      C(c.thresholds[f], "name") && (c.thresholds[f].name = Cb(F(c.thresholds[f], "name")))
    }
  }
};
z.Ts = function(a, b) {
  cl.f.Ts.call(this, a, b);
  if(a && b && C(b, "color_swatch")) {
    var c = F(b, "color_swatch");
    if(C(a, "chart_settings")) {
      var d = F(a, "chart_settings");
      C(d, "controls") && (d = F(d, "controls"), C(d, "color_swatch") && H(d, "color_swatch", $b(c, F(d, "color_swatch"), "color_swatch")))
    }
  }
};
var dl = {chart:{styles:{line_axis_marker_style:{label:{font:{family:"Tahoma", size:"11", bold:"True"}, format:{value:"{%Value}"}, background:{border:{gradient:{key:[{position:"0", color:"rgb(221,221,221)"}, {position:"1", color:"rgb(208,208,208)"}], angle:"90"}, enabled:"True", type:"Gradient"}, fill:{gradient:{key:[{position:"0", color:"rgb(255,255,255)"}, {position:"0.5", color:"rgb(243,243,243)"}, {position:"1", color:"rgb(255,255,255)"}], angle:"90"}, type:"Gradient"}, inside_margin:{all:"5"}, 
effects:{drop_shadow:{enabled:"True", distance:"1", opacity:"0.1"}, enabled:"True"}, enabled:"false"}, enabled:"False", position:"BeforeAxisLabels", align:"Near", text_align:"Left"}, name:"anychart_default", thickness:"1", color:"#DC0A0A", caps:"Square", joints:"miter", dash_length:"10", space_length:"10"}, range_axis_marker_style:{label:{format:{value:"{%Minimum} - {%Maximum}"}, font:{family:"Tahoma", size:"11", color:"rgb(34,34,34)"}, background:{border:{gradient:{key:[{position:"0", color:"rgb(221,221,221)"}, 
{position:"1", color:"rgb(208,208,208)"}], angle:"90"}, enabled:"True", type:"Gradient"}, fill:{gradient:{key:[{position:"0", color:"rgb(255,255,255)"}, {position:"0.5", color:"rgb(243,243,243)"}, {position:"1", color:"rgb(255,255,255)"}], angle:"90"}, type:"Gradient"}, inside_margin:{all:"5"}, effects:{drop_shadow:{enabled:"True", distance:"1", opacity:"0.1"}, enabled:"True"}, enabled:"false"}, enabled:"False", position:"BeforeAxisLabels", text_align:"Left"}, fill:{color:"%Color", opacity:".4"}, 
minimum_line:{color:"DarkColor(%Color)"}, maximum_line:{color:"DarkColor(%Color)"}, name:"anychart_default", color:"Green"}}, chart_settings:{axes:{scroll_bar_settings:{vert_scroll_bar:{fill:{gradient:{key:[{color:"#94999B", position:"1"}, {color:"#E7E7E7", position:"0.7"}, {color:"#E7E7E7", position:"0"}], angle:"0"}, enabled:"true", type:"Solid", color:"#FEFEFE"}, border:{enabled:"true", color:"#707173", opacity:"1"}, effects:{inner_shadow:{enabled:"true", angle:"180", blur_x:"15", blur_y:"0", 
distance:"7", color:"#777777"}, enabled:"true"}, buttons:{background:{corners:{type:"Square", all:"2"}, fill:{gradient:{key:[{color:"#FDFDFD"}, {color:"#EBEBEB"}], angle:"90"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#898B8D"}, {color:"#5D5F60"}], angle:"90"}, enabled:"true", type:"Gradient", opacity:"1"}, effects:{drop_shadow:{enabled:"true", distance:"5", opacity:"0.2"}, enabled:"false"}}, arrow:{fill:{color:"#111111"}, border:{enabled:"false"}}, states:{hover:{background:{fill:{gradient:{key:[{color:"#FDFDFD"}, 
{color:"#F4F4F4"}], angle:"90"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#009DFF"}, {color:"#0056B8"}], angle:"90"}, type:"Gradient"}}}, pushed:{background:{fill:{gradient:{key:[{color:"#DFF2FF"}, {color:"#99D7FF"}], angle:"90"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#009DFF"}, {color:"#0056B8"}], angle:"90"}, type:"Gradient"}}}}}, thumb:{background:{corners:{type:"Rounded", all:"0", left_top:"3", left_bottom:"3"}, fill:{gradient:{key:[{color:"#FFFFFF"}, 
{color:"#EDEDED"}], angle:"0"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#ABAEB0"}, {color:"#6B6E6F"}], angle:"0"}, enabled:"true", type:"Gradient", color:"#494949", opacity:"1"}, effects:{drop_shadow:{enabled:"false", distance:"5", opacity:".2"}, enabled:"true"}}, states:{hover:{background:{fill:{gradient:{key:[{color:"#FFFFFF"}, {color:"#F7F7F7"}], angle:"0"}, enabled:"true", type:"gradient", color:"Cyan"}, border:{gradient:{key:[{color:"#009DFF"}, {color:"#0064C6"}], angle:"0"}, 
type:"Gradient"}}}, pushed:{background:{fill:{gradient:{key:[{color:"#D8F0FF", position:"0"}, {color:"#ACDEFF", position:"1"}], angle:"0"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#009DFF"}, {color:"#008AEC"}], angle:"0"}, type:"Gradient"}}}}}, size:"16"}, horz_scroll_bar:{fill:{gradient:{key:[{color:"#94999B", position:"0"}, {color:"#E7E7E7", position:"0.7"}, {color:"#E7E7E7", position:"1"}], angle:"90"}, enabled:"true", type:"Solid", color:"#FEFEFE"}, border:{enabled:"true", 
color:"#707173", opacity:"1"}, effects:{inner_shadow:{enabled:"true", blur_x:"0", blur_y:"15", distance:"7", color:"#555555"}, enabled:"true"}, buttons:{background:{corners:{type:"Square", all:"2"}, fill:{gradient:{key:[{color:"#FDFDFD"}, {color:"#EBEBEB"}], angle:"90"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#898B8D"}, {color:"#5D5F60"}], angle:"90"}, enabled:"true", type:"Gradient", opacity:"1"}, effects:{drop_shadow:{enabled:"true", distance:"5", opacity:"0.2"}, enabled:"false"}}, 
arrow:{fill:{color:"#111111"}, border:{enabled:"false"}}, states:{hover:{background:{fill:{gradient:{key:[{color:"#FDFDFD"}, {color:"#F4F4F4"}], angle:"90"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#009DFF"}, {color:"#0056B8"}], angle:"90"}, type:"Gradient"}}}, pushed:{background:{fill:{gradient:{key:[{color:"#DFF2FF"}, {color:"#99D7FF"}], angle:"90"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#009DFF"}, {color:"#0056B8"}], angle:"90"}, type:"Gradient"}}}}}, 
thumb:{background:{corners:{type:"Rounded", all:"0", left_bottom:"3", right_bottom:"3"}, fill:{gradient:{key:[{color:"#FFFFFF"}, {color:"#EDEDED"}], angle:"90"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#ABAEB0"}, {color:"#6B6E6F"}], angle:"90"}, enabled:"true", type:"Gradient", color:"#494949", opacity:"1"}, effects:{drop_shadow:{enabled:"false", distance:"5", opacity:".2"}, enabled:"true"}}, states:{hover:{background:{fill:{gradient:{key:[{color:"#FFFFFF"}, {color:"#F7F7F7"}], 
angle:"90"}, enabled:"true", type:"gradient", color:"Cyan"}, border:{gradient:{key:[{color:"#009DFF"}, {color:"#0064C6"}], angle:"90"}, type:"Gradient"}}}, pushed:{background:{fill:{gradient:{key:[{color:"#D8F0FF", position:"0"}, {color:"#ACDEFF", position:"1"}], angle:"90"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#009DFF"}, {color:"#008AEC"}], angle:"90"}, type:"Gradient"}}}}}, size:"16"}}, x_axis:{major_grid:{enabled:"true", interlaced:"false"}, minor_grid:{enabled:"true", 
interlaced:"false"}, title:{text:{value:" "}}}, y_axis:{major_grid:{enabled:"true"}, minor_grid:{enabled:"true"}, title:{text:{value:" "}}}, z_axis:{enabled:"false"}}, data_plot_background:{effects:{enabled:"False"}, x_axis_plane:{fill:{}, border:{}}, y_axis_plane:{fill:{}, border:{}}}}}, defaults:{axis:{line:{enabled:"True", color:"#474747"}, zero_line:{enabled:"True", color:"#FF0000", opacity:"0.3"}, major_grid:{line:{enabled:"true", color:"#C1C1C1", dash_length:"1", space_length:"5"}, interlaced_fills:{even:{fill:{enabled:"true", 
color:"#F5F5F5", opacity:"0.5"}}, odd:{fill:{enabled:"true", color:"#FFFFFF", opacity:"0.5"}}}, enabled:"false", interlaced:"true"}, minor_grid:{line:{enabled:"true", color:"#EEEEEE", dash_length:"1", space_length:"10"}, interlaced_fills:{even:{fill:{enabled:"True", color:"#F5F5F5", opacity:"0.5"}}, odd:{fill:{enabled:"True", color:"#FFFFFF", opacity:"0.5"}}}, enabled:"false", interlaced:"false"}, major_tickmark:{enabled:"true", size:"5", color:"#313131"}, minor_tickmark:{enabled:"true", size:"2", 
color:"#3C3C3C"}, title:{text:{value:"Axis title"}, font:{family:"Tahoma", size:"11", color:"rgb(34,34,34)", bold:"True"}, background:{border:{gradient:{key:[{position:"0", color:"#DDDDDD"}, {position:"1", color:"#D0D0D0"}], angle:"90"}, enabled:"True", type:"Gradient"}, fill:{gradient:{key:[{position:"0", color:"#FFFFFF"}, {position:"0.5", color:"#F3F3F3"}, {position:"1", color:"#FFFFFF"}], angle:"90"}, type:"Gradient"}, inside_margin:{all:"5"}, effects:{drop_shadow:{enabled:"True", distance:"1", 
opacity:"0.1"}, enabled:"True"}, enabled:"false"}, enabled:"True", text_align:"Center", align:"Center"}, labels:{format:{value:"{%Value}{numDecimals:2}"}, background:{border:{gradient:{key:[{position:"0", color:"#DDDDDD"}, {position:"1", color:"#D0D0D0"}], angle:"90"}, enabled:"True", type:"Gradient"}, fill:{gradient:{key:[{position:"0", color:"#FFFFFF"}, {position:"0.5", color:"#F3F3F3"}, {position:"1", color:"#FFFFFF"}], angle:"90"}, type:"Gradient"}, inside_margin:{all:"5"}, effects:{drop_shadow:{enabled:"True", 
distance:"1", opacity:"0.1"}, enabled:"True"}, enabled:"false"}, font:{family:"Tahoma", size:"11", color:"rgb(34,34,34)"}, enabled:"true", align:"inside", position:"outside", display_mode:"normal", multi_line_align:"center", rotation:"0", allow_overlap:"false", show_first_label:"true", show_last_label:"true", show_cross_label:"true"}, zoom:{enabled:"false"}, enabled:"true"}}};
var el = {chart:{styles:{bar_style:[{border:{type:"Solid", color:"DarkColor(%Color)"}, fill:{gradient:{key:[{position:"0", color:"%Color", opacity:"1"}, {position:"1", color:"Blend(DarkColor(%Color),%Color,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", color:"%Color", opacity:"0.9"}, effects:{bevel:{enabled:"true", distance:"1"}, enabled:"True"}, states:{hover:{border:{type:"Solid", color:"DarkColor(White)"}, fill:{gradient:{key:[{position:"0", color:"White", opacity:"1"}, {position:"1", color:"Blend(DarkColor(White),%White,0.7)", 
opacity:"1"}], angle:"90"}, type:"Gradient", color:"White", opacity:"0.9"}}, pushed:{border:{type:"Solid", color:"DarkColor(White)"}, fill:{gradient:{key:[{position:"0", color:"White", opacity:"1"}, {position:"1", color:"Blend(DarkColor(White),White,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", color:"White", opacity:"0.9"}, effects:{bevel:{enabled:"true", distance:"2", angle:"225"}}}, selected_normal:{border:{thickness:"2", color:"DarkColor(%Color)"}, hatch_fill:{enabled:"true", thickness:"3", 
type:"ForwardDiagonal", opacity:"0.3"}}, selected_hover:{fill:{gradient:{key:[{position:"0", color:"White", opacity:"1"}, {position:"1", color:"Blend(DarkColor(White),White,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", color:"White", opacity:"0.9"}, border:{thickness:"2", color:"DarkColor(White)"}, hatch_fill:{enabled:"true", thickness:"3", type:"ForwardDiagonal", color:"DarkColor(White)", opacity:"0.3"}}, missing:{border:{type:"Solid", color:"DarkColor(White)", thickness:"1", opacity:"0.4"}, 
fill:{gradient:{key:[{position:"0", color:"White", opacity:"1"}, {position:"1", color:"Blend(DarkColor(White),White,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", opacity:"0.3"}, hatch_fill:{type:"Checkerboard", pattern_size:"20", enabled:"true", opacity:"0.1"}, effects:{bevel:{enabled:"false", distance:"1", angle:"225"}}}}, name:"anychart_default"}, {fill:{color:"%Color"}, name:"AquaLight"}, {fill:{color:"%Color"}, name:"AquaDark"}, {effects:{bevel:{enabled:"false"}, drop_shadow:{enabled:"false"}}, 
fill:{color:"%Color"}, border:{color:"DarkColor(%Color)", type:"solid", thickness:"1"}, states:{normal:{fill:{color:"%Color", opacity:"1"}}, hover:{fill:{color:"White", opacity:"0.8"}}, pushed:{fill:{color:"Blend(White,Black,0.9)", opacity:"1"}, effects:{bevel:{enabled:"true", distance:"2", angle:"225", shadow_opacity:"0.1"}}}, selected_normal:{hatch_fill:{enabled:"true", thickness:"3", type:"ForwardDiagonal", opacity:"0.3"}, fill:{color:"%Color", opacity:"1"}}, selected_hover:{fill:{color:"White", 
opacity:".8"}, hatch_fill:{enabled:"true", thickness:"3", type:"ForwardDiagonal", color:"Black", opacity:"0.3"}}, missing:{fill:{color:"White", opacity:"0.3"}, hatch_fill:{type:"Checkerboard", pattern_size:"20", enabled:"true", opacity:"0.1"}, effects:{bevel:{enabled:"false", distance:"1", angle:"225"}}}}, name:"cylinder"}, {fill:{type:"solid", color:"%color"}, name:"plastic"}, {name:"silver", parent:"cylinder"}]}, data_plot_settings:{bar_series:{animation:{enabled:"True", style:"defaultScaleYBottom"}, 
label_settings:{animation:{enabled:"true", style:"defaultLabel"}, enabled:"false"}, marker_settings:{marker:{anchor:"CenterTop", valign:"Center", halign:"Center"}, animation:{enabled:"true", style:"defaultMarker"}, enabled:"false"}, tooltip_settings:{}, group_padding:"0.5", point_padding:"0.2"}, range_bar_series:{bar_style:{}, animation:{style:"defaultScaleYCenter"}, start_point:{tooltip_settings:{format:{value:"{%YRangeStart}"}, position:{anchor:"centerBottom", valign:"bottom"}, enabled:"False"}, 
marker_settings:{animation:{style:"defaultMarker"}}, label_settings:{animation:{style:"defaultLabel"}}}, end_point:{tooltip_settings:{format:{value:"{%YRangeEnd}"}, position:{anchor:"centerTop", valign:"top"}, enabled:"False"}, marker_settings:{animation:{style:"defaultMarker"}}, label_settings:{animation:{style:"defaultLabel"}}}, group_padding:"0.5", point_padding:"0.2"}}}};
var fl = {chart:{styles:{bubble_style:[{border:{type:"Solid", thickness:"1", color:"DarkColor(%Color)"}, fill:{gradient:{key:[{position:"0", color:"%Color", opacity:"0.9"}, {position:"1", color:"Blend(DarkColor(%Color),%Color,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", color:"%Color", opacity:"1"}, effects:{bevel:{enabled:"false", distance:"1"}}, states:{hover:{border:{type:"Solid", color:"DarkColor(White)"}, fill:{gradient:{key:[{position:"0", color:"White", opacity:"1"}, {position:"1", 
color:"Blend(DarkColor(White),White,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", opacity:"0.9"}}, pushed:{border:{type:"Solid", color:"DarkColor(White)"}, fill:{gradient:{key:[{position:"0", color:"White", opacity:"1"}, {position:"1", color:"Blend(DarkColor(White),White,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", opacity:"0.9"}, effects:{bevel:{enabled:"true", distance:"2", angle:"225"}}}, selected_normal:{border:{thickness:"2", color:"DarkColor(%Color)"}, hatch_fill:{enabled:"true", 
thickness:"2", type:"ForwardDiagonal", opacity:"0.3", pattern_size:"6", color:"Black"}}, selected_hover:{fill:{gradient:{key:[{position:"0", color:"White", opacity:"1"}, {position:"1", color:"Blend(DarkColor(White),White,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", opacity:"0.9"}, border:{thickness:"2", color:"DarkColor(White)"}, hatch_fill:{enabled:"true", thickness:"2", type:"ForwardDiagonal", color:"DarkColor(White)", opacity:"0.3", pattern_size:"6"}}, missing:{border:{type:"Solid", color:"DarkColor(White)", 
thickness:"1", opacity:"0.4"}, fill:{gradient:{key:[{position:"0", color:"White", opacity:"1"}, {position:"1", color:"Blend(DarkColor(White),White,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", opacity:"0.3"}, hatch_fill:{type:"Checkerboard", pattern_size:"20", enabled:"true", opacity:"0.1"}, effects:{bevel:{enabled:"false", distance:"1", angle:"225"}}}}, name:"anychart_default"}, {border:{type:"Solid", thickness:"1", color:"DarkColor(%Color)"}, fill:{type:"Solid", color:"%Color", opacity:"0.9"}, 
effects:{bevel:{enabled:"false", distance:"1"}}, states:{hover:{border:{type:"Solid", color:"%Color", thickness:"2"}, fill:{type:"Solid", color:"LightColor(%Color)", opacity:"0.9"}}, pushed:{border:{type:"Solid", color:"DarkColor(%Color)", thickness:"2"}, fill:{type:"Solid", color:"Blend(%Color,Dark,0.9)", opacity:"0.9"}}, selected_normal:{border:{thickness:"2", color:"DarkColor(%Color)"}, hatch_fill:{enabled:"true", thickness:"2", type:"ForwardDiagonal", opacity:"0.3", pattern_size:"6"}}, selected_hover:{border:{type:"Solid", 
color:"DarkColor(%Color)", thickness:"2"}, hatch_fill:{enabled:"true", thickness:"2", type:"ForwardDiagonal", color:"DarkColor(%Color)", opacity:"0.7", pattern_size:"6"}, fill:{type:"Solid", color:"LightColor(%Color)", opacity:"0.9"}}, missing:{border:{type:"Solid", color:"DarkColor(White)", thickness:"1", opacity:"0.4"}, fill:{gradient:{key:[{position:"0", color:"White", opacity:"1"}, {position:"1", color:"Blend(DarkColor(White),White,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", opacity:"0.3"}, 
hatch_fill:{type:"Checkerboard", pattern_size:"20", enabled:"true", opacity:"0.1"}, effects:{bevel:{enabled:"false", distance:"1", angle:"225"}}}}, name:"Flat"}, {border:{type:"Solid", thickness:"1", color:"DarkColor(%Color)"}, fill:{type:"Solid", color:"%Color", opacity:"0.5"}, effects:{bevel:{enabled:"false", distance:"1"}}, states:{hover:{border:{type:"Solid", color:"%Color", thickness:"2"}, fill:{type:"Solid", color:"LightColor(%Color)", opacity:"0.5"}}, pushed:{border:{type:"Solid", color:"DarkColor(%Color)", 
thickness:"2"}, fill:{type:"Solid", color:"Blend(%Color,Dark,0.9)", opacity:"0.5"}}, selected_normal:{border:{thickness:"2", color:"DarkColor(%Color)"}, hatch_fill:{enabled:"true", thickness:"2", type:"ForwardDiagonal", opacity:"0.3", pattern_size:"6"}}, selected_hover:{border:{type:"Solid", color:"DarkColor(%Color)", thickness:"2"}, hatch_fill:{enabled:"true", thickness:"2", type:"ForwardDiagonal", color:"DarkColor(%Color)", opacity:"0.7", pattern_size:"6"}, fill:{type:"Solid", color:"LightColor(%Color)", 
opacity:"0.5"}}, missing:{border:{type:"Solid", color:"DarkColor(White)", thickness:"1", opacity:"0.4"}, fill:{gradient:{key:[{position:"0", color:"White", opacity:"1"}, {position:"1", color:"Blend(DarkColor(White),White,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", opacity:"0.3"}, hatch_fill:{type:"Checkerboard", pattern_size:"20", enabled:"true", opacity:"0.1"}, effects:{bevel:{enabled:"false", distance:"1", angle:"225"}}}}, name:"Transparent"}, {effects:{bevel:{enabled:"false"}, drop_shadow:{enabled:"false"}}, 
states:{normal:{fill:{color:"%Color", opacity:"1"}}, hover:{fill:{color:"White", opacity:"0.8"}}, pushed:{fill:{color:"Blend(White,Black,0.7)", opacity:"1"}, effects:{bevel:{enabled:"true", distance:"2", angle:"225"}}}, selected_normal:{hatch_fill:{enabled:"true", thickness:"3", type:"ForwardDiagonal", opacity:"0.2"}}, selected_hover:{fill:{color:"White", opacity:"0.8"}, hatch_fill:{enabled:"true", thickness:"3", type:"ForwardDiagonal", color:"DarkColor(White)", opacity:"0.2"}}, missing:{fill:{color:"White", 
opacity:"0.3"}, hatch_fill:{type:"Checkerboard", pattern_size:"20", enabled:"true", opacity:"0.1"}, effects:{bevel:{enabled:"false", distance:"1", angle:"225"}}}}, name:"Aqua", "final":"true"}]}, data_plot_settings:{bubble_series:{animation:{style:"defaultScaleXYCenter"}, label_settings:{animation:{style:"defaultLabel"}, enabled:"False"}, marker_settings:{animation:{style:"defaultMarker"}, marker:{anchor:"Center", v_align:"Center", h_align:"Center"}, enabled:"false"}, tooltip_settings:{}}}}};
var gl = {chart:{styles:{line_style:{line:{thickness:"2", color:"%Color"}, states:{hover:{line:{thickness:"3", color:"LightColor(%Color)"}, effects:{glow:{enabled:"true"}}}, missing:{line:{thickness:"3", color:"%Color", opacity:"0.2"}}}, name:"anychart_default"}, area_style:[{line:{enabled:"True", color:"DarkColor(%Color)"}, fill:{color:"%Color", opacity:"1"}, states:{hover:{fill:{type:"solid", color:"LightColor(%Color)", opacity:"1"}}, pushed:{fill:{type:"solid", color:"Blend(LightColor(%Color),Black,0.7)", 
opacity:"1"}}, selected_normal:{fill:{type:"solid", color:"Blend(LightColor(%Color),Black,0.9)", opacity:"1"}, hatch_fill:{type:"ForwardDiagonal", enabled:"True", thickness:"3", opacity:"0.3", pattern_size:"6"}}, selected_hover:{fill:{type:"solid", color:"Blend(LightColor(%Color),Black,0.95)", opacity:"1"}, hatch_fill:{type:"ForwardDiagonal", enabled:"True", thickness:"3", opacity:"0.2", pattern_size:"6"}}}, name:"anychart_default"}, {line:{enabled:"True", type:"Solid", color:"DarkColor(%Color)"}, 
fill:{gradient:{key:[{position:"0", color:"%Color", opacity:"1"}, {position:"1", color:"Blend(DarkColor(%Color),%Color,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", color:"%Color", opacity:"0.9"}, effects:{bevel:{enabled:"true", distance:"1"}}, states:{hover:{line:{enabled:"True", type:"Solid", color:"DarkColor(White)"}, fill:{gradient:{key:[{position:"0", color:"White", opacity:"1"}, {position:"1", color:"Blend(DarkColor(White),%White,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", opacity:"0.9"}}, 
pushed:{line:{enabled:"True", type:"Solid", color:"DarkColor(White)"}, fill:{gradient:{key:[{position:"0", color:"White", opacity:"1"}, {position:"1", color:"Blend(DarkColor(White),White,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", opacity:"0.9"}, effects:{bevel:{enabled:"true", distance:"2", angle:"225"}}}, selected_normal:{line:{enabled:"True", thickness:"2", color:"DarkColor(%Color)"}, hatch_fill:{enabled:"true", thickness:"3", type:"ForwardDiagonal", opacity:"0.3"}}, selected_hover:{fill:{gradient:{key:[{position:"0", 
color:"White", opacity:"1"}, {position:"1", color:"Blend(DarkColor(White),White,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", opacity:"0.9"}, line:{enabled:"True", thickness:"2", color:"DarkColor(White)"}, hatch_fill:{enabled:"true", thickness:"3", type:"ForwardDiagonal", color:"DarkColor(White)", opacity:"0.3"}}, missing:{line:{enabled:"True", type:"Solid", color:"DarkColor(White)", thickness:"1", opacity:"0.4"}, fill:{gradient:{key:[{position:"0", color:"White", opacity:"1"}, {position:"1", 
color:"Blend(DarkColor(White),White,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", opacity:"0.3"}, hatch_fill:{type:"Checkerboard", pattern_size:"20", enabled:"true", opacity:"0.1"}, effects:{bevel:{enabled:"false", distance:"1", angle:"225"}}}}, name:"Dark"}, {line:{enabled:"True", color:"DarkColor(%Color)"}, fill:{color:"%Color", opacity:"0.5"}, states:{hover:{fill:{type:"solid", color:"LightColor(%Color)", opacity:"0.5"}}, pushed:{fill:{type:"solid", color:"Blend(LightColor(%Color),Black,0.7)", 
opacity:"0.5"}}, selected_normal:{fill:{type:"solid", color:"Blend(LightColor(%Color),Black,0.9)", opacity:"0.5"}, hatch_fill:{type:"ForwardDiagonal", enabled:"True", thickness:"3", opacity:"0.3", pattern_size:"6"}}, selected_hover:{fill:{type:"solid", color:"Blend(LightColor(%Color),Black,0.95)", opacity:"0.5"}, hatch_fill:{type:"ForwardDiagonal", enabled:"True", thickness:"3", opacity:"0.2", pattern_size:"6"}}}, name:"Transparent"}], range_area_style:{start_line:{color:"DarkColor(%Color)", thickness:"2"}, 
end_line:{color:"DarkColor(%Color)", thickness:"2"}, fill:{type:"Solid", color:"%Color"}, states:{hover:{start_line:{color:"%Color", thickness:"2"}, end_line:{color:"%Color", thickness:"2"}, fill:{type:"Solid", color:"LightColor(%Color)"}}}, name:"anychart_default"}}, data_plot_settings:{line_series:{animation:{style:"defaultScaleYCenter"}, marker_settings:{animation:{style:"defaultMarker"}, fill:{gradient:{key:[{position:"0", color:"LightColor(%Color)", opacity:"1"}, {position:"0.7", color:"%Color", 
opacity:"1"}, {position:"1", color:"LightColor(%Color)", opacity:"1"}], angle:"45", type:"radial", focal_point:"-0.7"}, type:"gradient", opacity:"1"}, states:{missing:{marker:{type:"None"}, fill:{opacity:"0"}, border:{thickness:"1", color:"%Color", opacity:"0.3"}}}, enabled:"true"}, effects:{drop_shadow:{enabled:"true", distance:"1.5", opacity:"0.25"}}, tooltip_settings:{enabled:"False"}, label_settings:{animation:{style:"defaultLabel"}, enabled:"False"}}, area_series:{animation:{style:"defaultScaleYBottom"}, 
tooltip_settings:{enabled:"False"}, label_settings:{animation:{style:"defaultLabel"}, enabled:"False"}, marker_settings:{animation:{style:"defaultMarker"}, enabled:"False"}}, range_area_series:{animation:{style:"defaultScaleYCenter"}, start_point:{tooltip_settings:{format:{value:"{%YRangeStart}"}, position:{anchor:"centerBottom", valign:"bottom"}, enabled:"False"}, marker_settings:{animation:{style:"defaultMarker"}, marker:{anchor:"centerBottom"}, enabled:"False"}, label_settings:{animation:{style:"defaultLabel"}, 
format:{value:"{%YRangeStart}"}, position:{anchor:"centerBottom", valign:"bottom"}, enabled:"False"}}, end_point:{tooltip_settings:{format:{value:"{%YRangeEnd}"}, position:{anchor:"centerTop", valign:"top"}, enabled:"False"}, marker_settings:{animation:{style:"defaultMarker"}, marker:{anchor:"centerTop"}, enabled:"False"}, label_settings:{animation:{style:"defaultLabel"}, format:{value:"{%YRangeEnd}"}, position:{anchor:"centerTop", valign:"top"}, enabled:"False"}}}}}};
var hl = {chart:{styles:{ohlc_style:{line:{thickness:"6"}, up:{line:{color:"LightColor(%Color)"}}, down:{line:{color:"DarkColor(%Color)"}}, name:"anychart_default"}, candlestick_style:{fill:{type:"solid", color:"%Color"}, border:{}, line:{color:"Black", thickness:"2"}, up:{fill:{color:"LightColor(%Color)"}}, down:{fill:{color:"DarkColor(%Color)"}}, name:"anychart_default"}}, data_plot_settings:{ohlc_series:{label_settings:{}, tooltip_settings:{}, marker_settings:{}, group_padding:"0.5", point_padding:"0.2"}, 
candlestick_series:{label_settings:{}, tooltip_settings:{}, marker_settings:{}, group_padding:"0.5", point_padding:"0.2"}}}};
var il = {chart:{styles:{}, data_plot_settings:{marker_series:{animation:{style:"defaultScaleXYCenter"}, label_settings:{animation:{style:"defaultLabel"}, enabled:"False"}, tooltip_settings:{}}}}};
var jl = {chart:{styles:{heat_map_style:{border:{type:"Solid", thickness:"1", color:"DarkColor(%Color)"}, fill:{type:"Solid", color:"%Color"}, name:"anychart_default"}}, data_plot_settings:{heat_map:{label_settings:{}, tooltip_settings:{}, marker_settings:{}}}}};
function kl() {
}
A.e(kl, cl);
kl.prototype.zh = function() {
  var a = this.$f(kl.f.zh.call(this), dl), a = this.$f(a, hl), a = this.$f(a, gl), a = this.$f(a, fl), a = this.$f(a, il), a = this.$f(a, jl);
  return a = this.$f(a, el)
};
kl.prototype.rn = function(a, b, c) {
  kl.f.rn.call(this, a, b, c);
  var d = p, f = p, g;
  if(C(a, "chart_settings") && C(g = F(a, "chart_settings"), "axes") && C(g = F(g, "axes"), "extra")) {
    d = F(g, "extra")
  }
  if(C(b, "chart_settings") && C(g = F(b, "chart_settings"), "axes") && C(g = F(g, "axes"), "extra")) {
    f = F(g, "extra")
  }
  a = cc(d, f);
  a != p && (F(F(c, "chart_settings"), "axes").extra = a)
};
kl.prototype.Ts = function(a, b) {
  kl.f.Ts.call(this, a, b);
  if(!(a == p || b == p)) {
    var c = F(b, "axis"), d = p;
    if(c != p && C(a, "chart_settings") && C(d = F(a, "chart_settings"), "axes")) {
      if(d = F(d, "axes"), C(d, "x_axis") && (d.x_axis = $b(c, F(d, "x_axis"), "x_axis")), C(d, "y_axis") && (d.y_axis = $b(c, F(d, "y_axis"), "y_axis")), C(d, "extra")) {
        d = F(d, "extra");
        d.x_axis instanceof Array || (d.x_axis = d.x_axis ? [d.x_axis] : []);
        var f = d.x_axis, g = f.length, k;
        for(k = 0;k < g;k++) {
          f[k] = $b(c, f[k], "x_axis"), C(f[k], "name") && (f[k].name = Cb(F(f[k], "name")))
        }
        d.y_axis instanceof Array || (d.y_axis = d.y_axis ? [d.y_axis] : []);
        f = d.y_axis;
        g = f.length;
        for(k = 0;k < g;k++) {
          f[k] = $b(c, f[k], "y_axis"), C(f[k], "name") && (f[k].name = Cb(F(f[k], "name")))
        }
      }
    }
  }
};
function ll(a) {
  this.ea = a
}
z = ll.prototype;
z.ea = p;
z.Tv = function() {
  A.xa()
};
z.Vv = function() {
  A.xa()
};
z.Uv = function() {
  A.xa()
};
z.yv = function() {
  A.xa()
};
z.Bv = function() {
  A.xa()
};
function ml(a) {
  this.ea = a
}
A.e(ml, ll);
ml.prototype.Xl = function(a, b, c, d) {
  d.x = this.ea.n().x + b - a / 2;
  d.width = a;
  d.height = c
};
function nl(a) {
  this.ea = a
}
A.e(nl, ml);
z = nl.prototype;
z.yv = function(a, b, c, d) {
  this.Xl(a, b, c, d);
  d.y = this.ea.n().ra() - c
};
z.Bv = function(a, b, c, d) {
  this.Xl(a, b, c, d);
  d.y = this.ea.n().ra()
};
z.Tv = function(a, b, c, d, f) {
  a = a.ja();
  b += this.ea.n().x;
  a.setAttribute("d", Jc(b, this.ea.n().ra() + c - d, b, this.ea.n().ra() + c, f));
  return a
};
z.Vv = function(a, b, c, d, f) {
  a = a.ja();
  b += this.ea.n().x;
  a.setAttribute("d", Jc(b, this.ea.n().ra() + c, b, this.ea.n().ra() + c + d, f));
  return a
};
z.Uv = function(a, b, c, d, f) {
  a = a.ja();
  b += this.ea.n().x;
  a.setAttribute("d", Jc(b, this.ea.n().y - c, b, this.ea.n().y - c + d, f));
  return a
};
function ol(a) {
  this.ea = a
}
A.e(ol, ml);
z = ol.prototype;
z.yv = function(a, b, c, d) {
  this.Xl(a, b, c, d);
  d.y = this.ea.n().y
};
z.Bv = function(a, b, c, d) {
  this.Xl(a, b, c, d);
  d.y = this.ea.n().y - c
};
z.Tv = function(a, b, c, d, f) {
  a = a.ja();
  b += this.ea.n().x;
  a.setAttribute("d", Jc(b, this.ea.n().y, b, this.ea.n().y + d, f));
  return a
};
z.Vv = function(a, b, c, d, f) {
  a = a.ja();
  b += this.ea.n().x;
  a.setAttribute("d", Jc(b, this.ea.n().y - d - c, b, this.ea.n().y - c, f));
  return a
};
z.Uv = function(a, b, c, d, f) {
  a = a.ja();
  b += this.ea.n().x;
  a.setAttribute("d", Jc(b, this.ea.n().ra() + c - d, b, this.ea.n().ra() + c, f));
  return a
};
function pl(a) {
  this.ea = a
}
A.e(pl, ll);
pl.prototype.Xl = function(a, b, c, d) {
  b = this.ea.n().ra() - b;
  d.y = b - a / 2;
  d.height = a;
  d.width = c
};
function ql(a) {
  this.ea = a
}
A.e(ql, pl);
z = ql.prototype;
z.yv = function(a, b, c, d) {
  this.Xl(a, b, c, d);
  d.x = this.ea.n().x
};
z.Bv = function(a, b, c, d) {
  this.Xl(a, b, c, d);
  d.x = this.ea.n().x - c
};
z.Tv = function(a, b, c, d, f) {
  b = this.ea.n().ra() - b;
  a = a.ja();
  a.setAttribute("d", Jc(this.ea.n().x, b, this.ea.n().x + d, b, f));
  return a
};
z.Vv = function(a, b, c, d, f) {
  b = this.ea.n().ra() - b;
  a = a.ja();
  a.setAttribute("d", Jc(this.ea.n().x - d - c, b, this.ea.n().x - c, b, f));
  return a
};
z.Uv = function(a, b, c, d, f) {
  b = this.ea.n().ra() - b;
  a = a.ja();
  a.setAttribute("d", Jc(this.ea.n().Ja() - d, b, this.ea.n().Ja(), b, f));
  return a
};
function rl(a) {
  this.ea = a
}
A.e(rl, pl);
z = rl.prototype;
z.yv = function(a, b, c, d) {
  this.Xl(a, b, c, d);
  d.x = this.ea.n().Ja() - c
};
z.Bv = function(a, b, c, d) {
  this.Xl(a, b, c, d);
  d.x = this.ea.n().Ja()
};
z.Tv = function(a, b, c, d, f) {
  b = this.ea.n().ra() - b;
  a = a.ja();
  a.setAttribute("d", Jc(this.ea.n().Ja() - d, b, this.ea.n().Ja(), b, f));
  return a
};
z.Vv = function(a, b, c, d, f) {
  b = this.ea.n().ra() - b;
  a = a.ja();
  a.setAttribute("d", Jc(this.ea.n().Ja() + c, b, this.ea.n().Ja() + d + c, b, f));
  return a
};
z.Uv = function(a, b, c, d, f) {
  b = this.ea.n().ra() - b;
  a = a.ja();
  a.setAttribute("d", Jc(this.ea.n().x, b, this.ea.n().x + d, b, f));
  return a
};
function sl(a) {
  this.ea = a
}
z = sl.prototype;
z.ea = p;
z.Wv = function() {
  A.xa()
};
z.Xv = function() {
  A.xa()
};
z.BA = function() {
  A.xa()
};
z.Me = function() {
  A.xa()
};
z.yj = function() {
  A.xa()
};
z.Cv = function() {
  A.xa()
};
z.DF = function() {
  A.xa()
};
function tl(a) {
  this.ea = a
}
A.e(tl, sl);
z = tl.prototype;
z.Me = s();
z.Cv = function(a, b, c) {
  c.x = this.ea.n().x + b - a / 2;
  c.width = a;
  c.y = this.ea.n().y;
  c.height = this.ea.n().height
};
z.DF = function(a, b, c) {
  c.x = this.ea.n().x + a;
  c.width = b - a;
  c.y = this.ea.n().y;
  c.height = this.ea.n().height
};
z.yj = function(a, b, c, d, f) {
  f.y = this.ea.n().y;
  f.x = this.ea.n().x + b;
  f.width = c - b;
  f.height = a
};
z.Xv = function(a, b, c) {
  a = a.ja();
  b += this.ea.n().x;
  a.setAttribute("d", Jc(b, this.ea.n().y + 0.5, b, this.ea.n().ra(), c));
  return a
};
z.BA = function(a, b) {
  return Mc(a, b.x, b.y, b.width, b.height)
};
function ul(a) {
  this.ea = a
}
A.e(ul, tl);
ul.prototype.yj = function(a, b, c, d, f) {
  ul.f.yj.call(this, a, b, c, d, f);
  f.y += this.ea.n().ra() + d - a / 2
};
ul.prototype.Wv = function(a, b, c, d, f) {
  a = a.ja();
  d = this.ea.n().ra() + d;
  a.setAttribute("d", Jc(b + 0.5, d, c, d, f));
  return a
};
function vl(a) {
  this.ea = a
}
A.e(vl, tl);
vl.prototype.yj = function(a, b, c, d, f) {
  vl.f.yj.call(this, a, b, c, d, f);
  f.y += this.ea.n().y - d - a / 2
};
vl.prototype.Wv = function(a, b, c, d, f) {
  a = a.ja();
  d = this.ea.n().y - d;
  a.setAttribute("d", Jc(b, d, c + 1, d, f));
  return a
};
vl.prototype.Me = function(a) {
  a.Wd(-a.ac)
};
function wl(a) {
  this.ea = a
}
A.e(wl, sl);
z = wl.prototype;
z.Cv = function(a, b, c) {
  c.y = this.ea.n().ra() - b - a / 2;
  c.height = a;
  c.x = this.ea.n().x;
  c.width = this.ea.n().width
};
z.DF = function(a, b, c) {
  a = this.ea.n().ra() - a;
  b = this.ea.n().ra() - b;
  c.y = Math.min(a, b);
  c.height = Math.max(a, b) - c.y;
  c.x = this.ea.n().x;
  c.width = this.ea.n().width
};
z.yj = function(a, b, c, d, f) {
  f.x = this.ea.n().x;
  f.y = b;
  f.height = c - b;
  f.width = a
};
z.Xv = function(a, b, c) {
  b = this.ea.n().ra() - b;
  a = a.ja();
  a.setAttribute("d", Jc(this.ea.n().x + 0.5, b, this.ea.n().Ja(), b, c));
  return a
};
z.BA = function(a, b) {
  return Mc(a, b.x, b.y, b.width, b.height)
};
function xl(a) {
  this.ea = a
}
A.e(xl, wl);
xl.prototype.yj = function(a, b, c, d, f) {
  xl.f.yj.call(this, a, b, c, d, f);
  f.x += this.ea.n().x - d - a / 2
};
xl.prototype.Wv = function(a, b, c, d, f) {
  a = a.ja();
  d = this.ea.n().x - d;
  a.setAttribute("d", Jc(d, b + 1, d, c, f));
  return a
};
xl.prototype.Me = function(a) {
  a.Wd(180 - (a.ac - 90))
};
function yl(a) {
  this.ea = a
}
A.e(yl, wl);
yl.prototype.yj = function(a, b, c, d, f) {
  yl.f.yj.call(this, a, b, c, d, f);
  f.x += this.ea.n().Ja() + d - a / 2
};
yl.prototype.Wv = function(a, b, c, d, f) {
  a = a.ja();
  d = this.ea.n().Ja() + d;
  a.setAttribute("d", Jc(d, b + 0.5, d, c, f));
  return a
};
yl.prototype.Me = function(a) {
  a.Wd(-(a.ac + 90))
};
function zl() {
}
z = zl.prototype;
z.Yx = function() {
  A.xa()
};
z.fD = function() {
  A.xa()
};
z.Zx = function() {
  A.xa()
};
z.Ox = function() {
  A.xa()
};
z.bD = function() {
  A.xa()
};
z.cD = function() {
  A.xa()
};
function Al() {
}
A.e(Al, zl);
z = Al.prototype;
z.Yx = aa();
z.Ox = function(a) {
  switch(a) {
    case 7:
      return 3;
    case 3:
      return 7;
    case 8:
      return 2;
    case 2:
      return 8;
    case 6:
      return 4;
    case 4:
      return 6;
    default:
      return a
  }
};
z.fD = aa();
z.Zx = function(a, b) {
  return b
};
z.bD = aa();
z.cD = function(a, b) {
  switch(b) {
    case 0:
      return 2;
    case 2:
      return 0;
    default:
      return b
  }
};
function Bl() {
}
A.e(Bl, Al);
function Cl() {
}
A.e(Cl, Al);
function Dl() {
}
A.e(Dl, zl);
z = Dl.prototype;
z.Yx = function(a) {
  switch(a) {
    case 7:
      return 1;
    case 1:
      return 3;
    case 5:
      return 7;
    case 3:
      return 5;
    case 8:
      return 2;
    case 2:
      return 4;
    case 6:
      return 8;
    case 4:
      return 6;
    default:
      return a
  }
};
z.Ox = function(a) {
  switch(a) {
    case 7:
      return 5;
    case 1:
      return 3;
    case 5:
      return 7;
    case 3:
      return 1;
    case 8:
      return 4;
    case 4:
      return 8;
    default:
      return a
  }
};
z.fD = function(a, b) {
  switch(b) {
    case 0:
      return 1;
    case 2:
      return 0;
    default:
      return 2
  }
};
z.Zx = function(a) {
  switch(a) {
    case 0:
      return 0;
    case 1:
      return 2;
    default:
      return 1
  }
};
z.bD = function(a, b) {
  switch(b) {
    case 0:
      return 0;
    case 2:
      return 1;
    default:
      return 2
  }
};
z.cD = function(a, b) {
  return this.Zx(a, b)
};
function El() {
}
A.e(El, Dl);
function Fl() {
}
A.e(Fl, Dl);
function Gl(a) {
  this.ea = a
}
Gl.prototype.ea = p;
Gl.prototype.zj = function() {
  A.xa()
};
Gl.prototype.GF = function() {
  A.xa()
};
function Hl(a) {
  this.ea = a
}
A.e(Hl, Gl);
Hl.prototype.GF = s();
Hl.prototype.zj = function(a, b) {
  switch(a.za) {
    case 0:
      b.x = this.ea.n().x;
      break;
    case 1:
      b.x = this.ea.n().x + (this.ea.n().width - a.nf.width) / 2;
      break;
    case 2:
      b.x = this.ea.n().Ja() - a.nf.width
  }
};
function Il(a) {
  this.ea = a
}
A.e(Il, Hl);
Il.prototype.zj = function(a, b) {
  Il.f.zj.call(this, a, b);
  b.y = this.ea.n().ra() + a.Tf() + a.ga()
};
function Jl(a) {
  this.ea = a
}
A.e(Jl, Hl);
Jl.prototype.zj = function(a, b) {
  Jl.f.zj.call(this, a, b);
  b.y = this.ea.n().rb() - a.Tf() - a.ga() - a.nf.height
};
function Kl(a) {
  this.ea = a
}
A.e(Kl, Gl);
Kl.prototype.GF = function(a) {
  Be(a, a.Bb.bc + 90)
};
Kl.prototype.zj = function(a, b) {
  switch(a.za) {
    case 0:
      b.y = this.ea.n().ra() - a.n().height;
      break;
    case 1:
      b.y = this.ea.n().rb() + (this.ea.n().height - a.n().height) / 2;
      break;
    case 2:
      b.y = this.ea.n().rb()
  }
};
function Ll(a) {
  this.ea = a
}
A.e(Ll, Kl);
Ll.prototype.zj = function(a, b) {
  Ll.f.zj.call(this, a, b);
  b.x = this.ea.n().x - a.Tf() - a.ga() - a.n().width
};
function Ml(a) {
  this.ea = a
}
A.e(Ml, Kl);
Ml.prototype.zj = function(a, b) {
  Ml.f.zj.call(this, a, b);
  b.x = this.ea.n().Ja() + a.Tf() + a.ga()
};
function Nl(a, b) {
  this.w = a;
  this.J = b;
  ze.apply(this)
}
A.e(Nl, ze);
z = Nl.prototype;
z.uH = function() {
  return new Ol(this.J)
};
z.w = p;
z.Ia = 5;
z.ga = u("Ia");
z.za = 0;
z.ul = q;
z.J = p;
z.Iv = j;
z.Jv = j;
z.yc = p;
z.ao = q;
z.bm = NaN;
z.KI = u("bm");
z.Ih = p;
z.g = function(a) {
  Nl.f.g.call(this, a);
  if(this.Ma) {
    if(C(a, "format") && (this.yc = new je(J(a, "format")), this.w.fa().uJ())) {
      var b = this.yc;
      b.ta(this.w);
      var c = p, d;
      for(d in b.Mf) {
        if("%Value" == b.Mf[d].Pn) {
          c = b.Mf[d];
          break
        }
      }
      this.Ih = c != p && c.Ih != h ? c.Ih : 2;
      this.w.fa().XH(this.Ih)
    }
    C(a, "padding") && (this.Ia = M(a, "padding"));
    if(C(a, "align")) {
      switch(Wb(a, "align")) {
        case "inside":
          this.za = 0;
          break;
        case "outside":
          this.za = 1;
          break;
        case "center":
          this.za = 2
      }
    }
    C(a, "position") && (this.ul = "inside" == N(a, "position"));
    C(a, "show_first_label") && (this.Iv = K(a, "show_first_label"));
    C(a, "show_last_label") && (this.Jv = K(a, "show_last_label"));
    C(a, "allow_overlap") && (this.ao = K(a, "allow_overlap"))
  }
};
z.Ld = p;
z.Fl = p;
z.uc = function(a, b) {
  this.Bb.Ae();
  Nl.f.uc.call(this, a, b)
};
z.bn = function(a) {
  this.Fl = new P;
  this.Ld = new P;
  for(var b = this.J.J.w.fa(), c = b.fc, d = 0;d <= c;d++) {
    this.bm = b.Rq(d);
    var f = this.yc.ta(this.w, this.w.Gd());
    this.uc(a, f);
    var f = this.k, g = this.nf;
    f.width > this.Ld.width && (this.Ld.width = f.width, this.Fl.width = g.width);
    f.height > this.Ld.height && (this.Ld.height = f.height, this.Fl.height = g.height)
  }
};
z.$s = function(a) {
  this.bn(a);
  a = this.J.Ho(this.Fl, this.Ld, this.Bb);
  a = Math.ceil(a);
  return 1 > a ? 1 : a
};
z.yd = 0;
z.nd = u("yd");
z.Zv = 0;
z.FB = function() {
  this.yd = this.J.og(this.Ld);
  this.Zv = this.yd + this.Ia
};
z.Px = function(a, b) {
  var c = new O, d = this.J.J.w.fa(), f = d.Mc(b);
  this.bm = d.Rq(b);
  this.uc(a, this.yc.ta(this.w, this.w.Gd()));
  this.J.qf(this, c, this.k, this.nf, d.rd(f));
  Pl(this, c, this.k, this.nf, this.Bb);
  d = this.k.Ca();
  d.x = c.x;
  d.y = c.y;
  return d
};
z.Je = 0;
z.Tf = u("Je");
z.Ei = t("Je");
z.yo = function(a) {
  var b = new O, c = this.J.J.w.fa(), d = !this.ao, f = d && this.J.tr(c.Nb()), g = f ? Number.POSITIVE_INFINITY : Number.NEGATIVE_INFINITY, k;
  this.Bb.or || (k = this.J.By(this.Fl, this.Bb));
  var l = this.Iv ? 0 : 1, n = c.fc;
  for(this.Jv || n--;l <= n;l++) {
    var m = c.Mc(l);
    this.bm = c.Rq(l);
    if(!c.Uy(m)) {
      break
    }
    var o = this.yc.ta(this.w, this.w.Gd());
    this.uc(a.r(), o);
    var o = this.k, v = this.nf;
    this.J.qf(this, b, o, v, c.rd(m));
    if(d) {
      if(this.J.Fy(f, this.Bb, b, g, k, o, v)) {
        continue
      }
      g = this.J.Qx(f, this.Bb, b, k, o, v)
    }
    Pl(this, b, o, v, this.Bb);
    m = this.jc(a.r());
    m.qb(b.x, b.y);
    a.ia(m)
  }
};
function Pl(a, b, c, d, f) {
  a.Bb.eN ? (b.x += c.width * f.VH, b.y += c.height * f.WH) : 360 >= f.bc && 270 <= f.bc && a.J && a.J instanceof Ql ? b.x -= c.width - d.height / 2 * f.xf : (b.x += d.width * f.VH + d.height * f.cM, b.y += d.width * f.dM + d.height * f.WH)
}
function Rl(a, b) {
  Nl.call(this, a, b)
}
A.e(Rl, Nl);
z = Rl.prototype;
z.fi = p;
z.bn = function(a) {
  this.Ld = new P;
  this.fi = new P;
  for(var b = this.w.fa(), c = b.fc, d = 0;d <= c;d++) {
    b.Mc(d);
    this.bm = b.Rq(d);
    var f = this.yc.ta(this.w, this.w.Gd());
    this.uc(a, f);
    f = 0 == d % 2 ? this.Ld : this.fi;
    this.k.width > f.width && (f.width = this.k.width);
    this.k.height > f.height && (f.height = this.k.height)
  }
};
z.$s = function(a) {
  this.bn(a);
  var a = this.J.Ho(this.Ld, this.Ld, this.Bb), b = this.J.Ho(this.fi, this.fi, this.Bb);
  1 > a && (a = 1);
  1 > b && (b = 1);
  return a + b
};
z.FB = function() {
  this.yd = this.J.og(this.Ld) + this.J.og(this.fi) + this.Ia;
  this.Zv = this.yd + this.Ia
};
z.yo = function(a) {
  for(var b = new O, c = this.J.J.w.fa(), d = !this.ao, f = d && this.J.tr(c.Nb()), g = f ? Number.POSITIVE_INFINITY : Number.NEGATIVE_INFINITY, k = g, l = this.J.og(this.Ld), n = this.J.og(this.fi), m, o, v = 0;v <= c.fc;v++) {
    var w = 0 == v % 2, y = c.Mc(v);
    this.bm = c.Rq(v);
    m = this.yc.ta(this.w, this.w.Gd());
    this.uc(a.r(), m);
    w ? (o = l, m = 0) : (o = n, m = l);
    var E = this.k;
    this.J.Fi(this, b, E, c.rd(y), m, o);
    if(d) {
      if(w) {
        if(this.J.xr(f, b, E, g)) {
          continue
        }
        g = this.J.$q(f, b, E)
      }else {
        if(this.J.xr(f, b, E, k)) {
          continue
        }
        k = this.J.$q(f, b, E)
      }
    }
    w = this.jc(a.r());
    w.qb(b.x, b.y);
    a.ia(w)
  }
};
z.g = function(a) {
  Rl.f.g.call(this, a);
  Be(this, 0)
};
function Ol(a) {
  this.QA = a
}
A.e(Ol, Zc);
Ol.prototype.Ae = function() {
  Ol.f.Ae.call(this);
  this.$n = this.tan = Math.tan(this.Pl);
  0 > this.$n && (this.$n = -this.$n);
  this.eN = 0 == this.Pl % 180;
  this.or = 0 == this.Pl % 90;
  var a = 0, a = 90 > this.Pl ? 0 : 180 > this.Pl ? 1 : 270 > this.Pl ? 2 : 3;
  this.VH = this.QA.Sx(a, this);
  this.cM = this.QA.Rx(a, this);
  this.dM = this.QA.Ux(a, this);
  this.WH = this.QA.Tx(a, this)
};
function Sl(a) {
  this.J = a
}
z = Sl.prototype;
z.J = p;
z.Ho = function() {
  A.xa()
};
z.og = function() {
  A.xa()
};
z.qf = function() {
  A.xa()
};
z.Qx = function() {
  A.xa()
};
z.By = function() {
  A.xa()
};
z.Fy = function() {
  A.xa()
};
z.Sx = function() {
  A.xa()
};
z.Rx = function() {
  A.xa()
};
z.Ux = function() {
  A.xa()
};
z.Tx = function() {
  A.xa()
};
z.tr = function() {
  A.xa()
};
z.nB = function() {
  A.xa()
};
z.Fi = function() {
  A.xa()
};
z.xr = function() {
  A.xa()
};
z.$q = function() {
  A.xa()
};
function Tl(a) {
  this.J = a
}
A.e(Tl, Sl);
z = Tl.prototype;
z.og = function(a) {
  return a.height
};
z.Ho = function(a, b, c) {
  var d = this.J.n().width, f = 0;
  return f = 0 != c.bc % 90 ? a.width * c.$n < a.height ? Number(d * c.wf / a.width) : Number(d * c.xf / a.height) : 0 != c.bc % 180 ? d / a.height : d / b.width
};
z.tr = aa();
z.By = function(a, b) {
  return a.width * b.$n < a.height
};
z.Ux = x(0);
z.Tx = x(0);
z.qf = function(a, b, c, d, f) {
  b.x = this.J.n().x + f;
  b.y = 0
};
z.Fy = function(a, b, c, d, f, g, k) {
  return a ? b.or ? c.x + g.width > d : c.x + (f ? k.width / b.wf : k.height / b.xf) > d : Math.round(c.x) < d
};
z.Qx = function(a, b, c, d, f, g) {
  return a ? c.x : c.x + (b.or ? f.width : d ? g.width / b.wf : g.height / b.xf)
};
z.nB = function(a, b, c) {
  var d = b, f = c;
  this.J.w.fa().Nb() && (d = c, f = b);
  0 > d.x && (a.x += -d.x, a.width += d.x);
  f.x + f.width > this.J.n().x + this.J.n().width && (a.width -= f.x + f.width - this.J.n().width - this.J.n().x)
};
z.Fi = function(a, b, c, d) {
  b.x = d - c.x - c.width / 2 + this.J.n().x;
  b.y = 0
};
z.xr = function(a, b, c, d) {
  return a ? b.x + c.width > d : b.x < d
};
z.$q = function(a, b, c) {
  return a ? b.x : b.x + c.width
};
function Ql(a) {
  this.J = a
}
A.e(Ql, Tl);
Ql.prototype.Sx = function(a, b) {
  return 0 == b.bc % 180 ? -0.5 : 0 == b.bc % 90 ? 0 : 1 == a || 3 == a ? -b.wf : 0
};
Ql.prototype.Rx = function(a, b) {
  return 0 == b.bc % 180 ? 0 : 0 == b.bc % 90 ? -0.5 : 0 == a || 2 == a ? -b.xf : 0
};
Ql.prototype.qf = function(a, b, c, d, f) {
  Ql.f.qf.call(this, a, b, c, d, f);
  b.y += this.J.n().ra() + a.Tf();
  switch(a.za) {
    case 2:
      b.y += (a.nd() - c.height) / 2;
      break;
    case 1:
      b.y += a.nd() - c.height
  }
};
Ql.prototype.Fi = function(a, b, c, d, f, g) {
  Ql.f.Fi.call(this, a, b, c, d, f, g);
  b.y += this.J.n().ra() + a.Tf() + f;
  switch(a.za) {
    case 2:
      b.y += (g - c.height) / 2;
      break;
    case 1:
      b.y += g - c.height
  }
};
function Ul(a) {
  this.J = a
}
A.e(Ul, Tl);
Ul.prototype.Sx = function(a, b) {
  return 0 == b.bc % 180 ? -0.5 : 0 == b.bc % 90 ? 0 : 0 == a || 2 == a ? -b.wf : 0
};
Ul.prototype.Rx = function(a, b) {
  return 0 == b.bc % 180 ? 0 : 0 == b.bc % 90 ? -0.5 : 1 == a || 3 == a ? -b.xf : 0
};
Ul.prototype.qf = function(a, b, c, d, f) {
  Ul.f.qf.call(this, a, b, c, d, f);
  b.y += this.J.n().y - a.Tf() - c.height;
  switch(a.za) {
    case 2:
      b.y -= (a.nd() - c.height) / 2;
      break;
    case 1:
      b.y -= a.nd() - c.height
  }
};
Ul.prototype.Fi = function(a, b, c, d, f, g) {
  Ul.f.Fi.call(this, a, b, c, d, f, g);
  b.y += this.J.n().y - a.Tf() - c.height - f;
  switch(a.za) {
    case 2:
      b.y -= (g - c.height) / 2;
      break;
    case 1:
      b.y -= g - c.height
  }
};
function Vl(a) {
  this.J = a
}
A.e(Vl, Sl);
z = Vl.prototype;
z.og = function(a) {
  return a.width
};
z.Ho = function(a, b, c) {
  var d = this.J.n().height, f = 0;
  return f = c && 0 != c.bc % 90 ? a.width / c.$n < a.height ? Number(d * c.xf / a.width) : Number(d * c.wf / a.height) : c && 0 != c.bc % 180 ? d / a.width : d / b.height
};
z.tr = function(a) {
  return!a
};
z.By = function(a, b) {
  return a.width / b.$n < a.height
};
z.Sx = x(0);
z.Rx = x(0);
z.qf = function(a, b, c, d, f) {
  b.y = this.J.n().ra() - f;
  b.x = 0
};
z.Fy = function(a, b, c, d, f, g, k) {
  return a ? b.or ? c.y + g.height > d : c.y + (f ? k.width / b.xf : k.height / b.wf) > d : Math.round(c.y) < d
};
z.Qx = function(a, b, c, d, f, g) {
  return a ? c.y : c.y + (b.or ? f.height : d ? g.width / b.xf : g.height / b.wf)
};
z.nB = function(a, b, c) {
  var d = c, f = b;
  this.J.w.fa().Nb() && (d = b, f = c);
  0 > d.y && (a.y += -d.y, a.height += d.y);
  f.y + f.height > this.J.n().y + this.J.n().height && (a.height -= f.y + f.height - this.J.n().height - this.J.n().y)
};
z.Fi = function(a, b, c, d) {
  b.y = this.J.n().ra() - d - c.height / 2;
  b.x = 0
};
z.xr = function(a, b, c, d) {
  return a ? b.y + c.height > d : b.y < d
};
z.$q = function(a, b, c) {
  return a ? b.y : b.y + c.height
};
function Wl(a) {
  this.J = a
}
A.e(Wl, Vl);
Wl.prototype.Ux = function(a, b) {
  return 0 == b.bc % 180 ? 0 : 0 == b.bc % 90 ? -0.5 : 0 == a || 2 == a ? -b.xf : 0
};
Wl.prototype.Tx = function(a, b) {
  return 0 == b.bc % 180 ? -0.5 : 0 == b.bc % 90 ? 0 : 1 == a || 3 == a ? -b.wf : 0
};
Wl.prototype.qf = function(a, b, c, d, f) {
  Wl.f.qf.call(this, a, b, c, d, f);
  b.x += this.J.n().x - a.Tf() - c.width;
  switch(a.align) {
    case 2:
      b.x -= (a.nd() - c.width) / 2;
      break;
    case 1:
      b.x -= a.nd() - c.width
  }
};
Wl.prototype.Fi = function(a, b, c, d, f, g) {
  Wl.f.Fi.call(this, a, b, c, d, f, g);
  b.x += this.J.n().x - a.Tf() - c.width - f;
  switch(a.align) {
    case 2:
      b.x -= (g - c.width) / 2;
      break;
    case 1:
      b.x -= g - c.width
  }
};
function Xl(a) {
  this.J = a
}
A.e(Xl, Vl);
Xl.prototype.Ux = function(a, b) {
  return 0 == b.bc % 180 ? 0 : 0 == b.bc % 90 ? -0.5 : 1 == a || 3 == a ? -b.xf : 0
};
Xl.prototype.Tx = function(a, b) {
  return 0 == b.bc % 180 ? -0.5 : 0 == b.bc % 90 ? 0 : 0 == a || 2 == a ? -b.wf : 0
};
Xl.prototype.qf = function(a, b, c, d, f) {
  Xl.f.qf.call(this, a, b, c, d, f);
  b.x += this.J.n().Ja() + a.Tf();
  switch(a.align) {
    case 2:
      b.x += (a.nd() - c.width) / 2;
      break;
    case 1:
      b.x += a.nd() - c.width
  }
};
Xl.prototype.Fi = function(a, b, c, d, f, g) {
  Xl.f.Fi.call(this, a, b, c, d, f, g);
  b.x += this.J.n().Ja() + a.Tf() + f;
  switch(a.align) {
    case 2:
      b.x += (g - c.width) / 2;
      break;
    case 1:
      b.x += g - c.width
  }
};
function Yl(a) {
  this.J = a
}
z = Yl.prototype;
z.J = p;
z.dg = function() {
  A.xa()
};
z.cg = function() {
  A.xa()
};
z.xj = function() {
  A.xa()
};
z.zv = function() {
  A.xa()
};
function Zl(a, b, c, d, f) {
  if(d == p || d == h) {
    d = j
  }
  if(f == p || f == h) {
    f = q
  }
  var g = new O, k = new O;
  a.dg(b, g);
  a.cg(c, k);
  f && (a = g, g = k, k = a);
  d = d ? S(g.x, g.y) : U(g.x, g.y);
  return d += U(k.x, k.y)
}
function $l(a) {
  this.J = a
}
A.e($l, Yl);
$l.prototype.dg = function(a, b) {
  b.x = a + this.J.n().x;
  b.y = this.J.n().y
};
$l.prototype.cg = function(a, b) {
  b.x = a + this.J.n().x;
  b.y = this.J.n().ra()
};
$l.prototype.xj = function(a, b, c, d, f) {
  f.x = this.J.n().x + a - d.width / 2;
  f.y = 0
};
function am(a) {
  this.J = a
}
A.e(am, Yl);
am.prototype.dg = function(a, b) {
  b.x = this.J.n().x;
  b.y = this.J.w.fa().we - a + this.J.n().y
};
am.prototype.cg = function(a, b) {
  b.x = this.J.n().Ja();
  b.y = this.J.w.fa().we - a + this.J.n().y
};
am.prototype.xj = function(a, b, c, d, f) {
  f.y = this.J.n().ra() - a - d.height / 2;
  f.x = 0
};
function bm(a) {
  this.J = a
}
A.e(bm, am);
bm.prototype.xj = function(a, b, c, d, f) {
  bm.f.xj.call(this, a, b, c, d, f);
  f.x += this.J.n().tb() - c - d.width - b
};
bm.prototype.zv = function(a, b, c, d, f, g) {
  var k = this.J.n(), a = new O(k.x, k.ra() - a), b = new O(k.Ja(), k.ra() - b), l = Math.atan2(b.y - a.y, b.x - a.x), k = d * Math.cos(l), d = d * Math.sin(l);
  switch(c) {
    case 0:
      g.x = a.x + k;
      g.y = a.y + d - f.height / 2;
      break;
    case 1:
      g.y = (a.y + b.y) / 2 - f.height / 2;
      g.x = (a.x + b.x) / 2 - f.width / 2;
      break;
    case 2:
      g.x = b.x - k - f.width, g.y = b.y - f.height / 2 - d
  }
};
function cm(a) {
  this.J = a
}
A.e(cm, am);
cm.prototype.xj = function(a, b, c, d, f) {
  cm.f.xj.call(this, a, b, c, d, f);
  f.x += this.J.n().Ja() + c + b
};
cm.prototype.zv = function(a, b, c, d, f, g) {
  var a = new O(this.J.n().Ja(), this.J.n().ra() - a), b = new O(this.J.n().x, this.J.n().ra() - b), k = Math.atan2(b.y - a.y, b.x - a.x), l = d * Math.cos(k), d = d * Math.sin(k);
  switch(c) {
    case 0:
      g.x = a.x - l - f.width;
      g.y = a.y + d - f.height / 2;
      break;
    case 1:
      g.y = (a.y + b.y) / 2 - f.height / 2;
      g.x = (a.x + b.x) / 2 - f.width / 2;
      break;
    case 2:
      g.x = b.x + l, g.y = b.y - f.height / 2 - d
  }
  g.x += this.J.n().x
};
cm.prototype.dg = function(a, b) {
  cm.f.cg.call(this, a, b)
};
cm.prototype.cg = function(a, b) {
  cm.f.dg.call(this, a, b)
};
function dm(a) {
  this.J = a
}
A.e(dm, $l);
dm.prototype.xj = function(a, b, c, d, f) {
  dm.f.xj.call(this, a, b, c, d, f);
  f.y += this.J.n().rb() - c - d.height - b
};
dm.prototype.zv = function(a, b, c, d, f, g) {
  var k = new O(a, this.J.n().y), b = new O(b, this.J.n().ra()), l = Math.atan2(b.y - k.y, b.x - k.x), n = d * Math.cos(l), d = d * Math.sin(l);
  switch(c) {
    case 0:
      g.y = k.y + d;
      g.x = a + n - f.width / 2;
      break;
    case 1:
      g.y = (k.y + b.y) / 2 - f.height / 2;
      g.x = (k.x + b.x) / 2 - f.width / 2;
      break;
    case 2:
      g.y = b.y - f.height - d, g.x = b.x - f.width / 2 - n
  }
};
function em(a) {
  this.J = a
}
A.e(em, $l);
em.prototype.xj = function(a, b, c, d, f) {
  em.f.xj.call(this, a, b, c, d, f);
  f.y += this.J.n().ra() + c + b
};
em.prototype.zv = function(a, b, c, d, f, g) {
  var k = new O(a, this.J.n().ra()), b = new O(b, this.J.n().y), l = Math.atan2(b.y - k.y, b.x - k.x), n = d * Math.cos(l), d = d * Math.sin(l);
  switch(c) {
    case 0:
      g.y = k.y - f.height + d;
      g.x = a + n - f.width / 2;
      break;
    case 1:
      g.y = (k.y + b.y) / 2 - f.height / 2;
      g.x = (k.x + b.x) / 2 - f.width / 2;
      break;
    case 2:
      g.y = b.y - d, g.x = b.x - f.width / 2 - n
  }
  g.x += this.J.n().x
};
em.prototype.dg = function(a, b) {
  em.f.cg.call(this, a, b)
};
em.prototype.cg = function(a, b) {
  em.f.dg.call(this, a, b)
};
function fm() {
}
fm.ZB = function(a) {
  switch(a.Ib()) {
    case 0:
      return new gm(a);
    case 1:
      return new hm(a);
    case 2:
      return new im(a);
    case 3:
      return new jm(a)
  }
  return p
};
function km(a) {
  this.w = a
}
z = km.prototype;
z.k = p;
z.n = u("k");
z.w = p;
z.ks = p;
z.$d = p;
z.vi = p;
z.ls = p;
z.ij = p;
z.pn = p;
z.Jp = t("k");
z.bo = function() {
  A.xa()
};
z.uD = function() {
  A.xa()
};
z.tD = function() {
  A.xa()
};
z.Fv = function() {
  A.xa()
};
z.Ei = function() {
  A.xa()
};
z.dg = function() {
  A.xa()
};
z.cg = function() {
  A.xa()
};
z.nd = function() {
  A.xa()
};
function lm(a) {
  this.w = a
}
A.e(lm, km);
z = lm.prototype;
z.Fv = function(a, b) {
  b.x = this.k.x + a
};
z.Jp = function(a) {
  lm.f.Jp.call(this, a);
  var b = this.w.fa(), a = a.width;
  b.ef = 0;
  b.we = a
};
z.dg = function(a, b) {
  b.x = a;
  b.y = this.k.y
};
z.cg = function(a, b) {
  b.x = a;
  b.y = this.k.ra()
};
z.uD = function() {
  return this.k.tb()
};
z.tD = function() {
  return this.k.Ja()
};
z.nd = function(a) {
  return a.height
};
function mm(a) {
  this.w = a
}
A.e(mm, km);
z = mm.prototype;
z.Jp = function(a) {
  mm.f.Jp.call(this, a);
  var b = this.w.fa(), a = a.height;
  b.ef = 0;
  b.we = a
};
z.Fv = function(a, b) {
  b.y = this.k.ra() - a
};
z.dg = function(a, b) {
  b.y = a;
  b.x = this.k.x
};
z.cg = function(a, b) {
  b.y = a;
  b.x = this.k.Ja()
};
z.uD = function() {
  return this.k.y
};
z.tD = function() {
  return this.k.ra()
};
z.nd = function(a) {
  return a.width
};
function im(a) {
  this.w = a;
  this.ks = new ol(this);
  this.$d = new vl(this);
  this.vi = new Cl;
  this.ls = new Jl(this);
  this.ij = new Ul(this);
  this.pn = new dm(this)
}
A.e(im, lm);
im.prototype.bo = function(a, b) {
  a.height -= b;
  a.y += b
};
im.prototype.Ei = function(a, b, c) {
  c.y = this.k.y - a - b.height;
  c.x -= b.width / 2
};
function jm(a) {
  this.w = a;
  this.ks = new nl(this);
  this.$d = new ul(this);
  this.vi = new Bl;
  this.ls = new Il(this);
  this.ij = new Ql(this);
  this.pn = new em(this)
}
A.e(jm, lm);
jm.prototype.bo = function(a, b) {
  a.height -= b
};
jm.prototype.dg = function(a, b) {
  lm.prototype.cg.call(this, a, b)
};
jm.prototype.cg = function(a, b) {
  lm.prototype.dg.call(this, a, b)
};
jm.prototype.Ei = function(a, b, c) {
  c.y = this.k.ra() + a;
  c.x -= b.width / 2
};
function gm(a) {
  this.w = a;
  this.ks = new ql(this);
  this.$d = new xl(this);
  this.vi = new El;
  this.ls = new Ll(this);
  this.ij = new Wl(this);
  this.pn = new bm(this)
}
A.e(gm, mm);
gm.prototype.bo = function(a, b) {
  a.width -= b;
  a.x += b
};
gm.prototype.Ei = function(a, b, c) {
  c.x = this.k.x - a - b.width;
  c.y -= b.height / 2
};
function hm(a) {
  this.w = a;
  this.ks = new rl(this);
  this.$d = new yl(this);
  this.vi = new Fl;
  this.ls = new Ml(this);
  this.ij = new Xl(this);
  this.pn = new cm(this)
}
A.e(hm, mm);
hm.prototype.bo = function(a, b) {
  a.width -= b
};
hm.prototype.Ei = function(a, b, c) {
  c.x = this.k.Ja() + a;
  c.y -= b.height / 2
};
hm.prototype.dg = function(a, b) {
  mm.prototype.cg.call(this, a, b)
};
hm.prototype.cg = function(a, b) {
  mm.prototype.dg.call(this, a, b)
};
function nm(a, b) {
  Se.call(this, a, b)
}
A.e(nm, Se);
nm.prototype.Ea = p;
nm.prototype.Mb = u("Ea");
nm.prototype.g = function(a) {
  Ub(a, "label") && (this.Ea = new om, this.Ea.g(F(a, "label")));
  return a
};
function pm(a, b) {
  Se.call(this, a, b)
}
A.e(pm, nm);
pm.prototype.Kb = p;
pm.prototype.g = function(a) {
  a = pm.f.g.call(this, a);
  this.Kb = new Ld;
  this.Kb.g(a);
  return a
};
function qm() {
  $.call(this)
}
A.e(qm, $);
qm.prototype.cc = function() {
  return pm
};
function rm(a, b) {
  Se.call(this, b, h)
}
A.e(rm, nm);
z = rm.prototype;
z.iz = p;
z.ez = p;
z.Hb = p;
z.mc = u("Hb");
z.Pc = p;
z.De = u("Pc");
z.g = function(a) {
  a = rm.f.g.call(this, a);
  Ub(a, "minimum_line") && (this.iz = new Ld, this.iz.g(F(a, "minimum_line")));
  Ub(a, "maximum_line") && (this.ez = new Ld, this.ez.g(F(a, "maximum_line")));
  Ub(a, "fill") && (this.Hb = new Jd, this.Hb.g(F(a, "fill")));
  Ub(a, "hatch_fill") && (this.Pc = new Fc, this.Pc.g(F(a, "hatch_fill")));
  return a
};
function sm() {
  $.call(this)
}
A.e(sm, $);
sm.prototype.cc = function() {
  return rm
};
function tm(a) {
  this.w = a;
  this.em = this.kB = this.It = j;
  this.b = new ne
}
z = tm.prototype;
z.w = p;
z.Sg = p;
z.K = p;
z.It = p;
z.kB = p;
z.em = p;
z.D = p;
z.g = function(a, b) {
  C(a, "visible") && (this.em = K(a, "visible"));
  C(a, "display_under_data") && (this.It = K(a, "display_under_data"));
  C(a, "affects_scale_range") && (this.kB = K(a, "affects_scale_range"));
  this.jC(a, b)
};
z.jC = function(a, b) {
  var c = Ve(b, this.Rb(), a, J(a, "style"));
  this.K = this.Qb();
  this.K.g(c)
};
z.Qb = function() {
  A.xa()
};
z.Rb = function() {
  A.xa()
};
z.ta = function(a) {
  "{" == a.charAt(0) && (a = a.substr(1, a.length - 2));
  var b;
  if(this.w.$D()) {
    b = Number(a);
    if(!isNaN(b)) {
      return b - 1
    }
    if(this.w.Vk[a] != h) {
      return this.w.Ef(a).na
    }
  }
  b = this.w.fa().$i(a);
  return!isNaN(b) ? b : this.w.Na(a)
};
z.vq = function(a) {
  var b = this.w.fa();
  if(isNaN(b.ld) || a < b.ld) {
    b.ld = a
  }
  if(isNaN(b.kd) || a > b.kd) {
    b.kd = a
  }
};
z.p = function(a) {
  this.D = new W(a);
  this.It ? this.w.ca().yB.ia(this.D) : this.w.ca().vw.ia(this.D);
  var b = this.Mb();
  b && b.p(a, this)
};
z.Mb = function() {
  return this.K.wa.Mb()
};
z.Na = function(a) {
  return se(a, this.w.ca()) ? this.w.ca().Na(a) : 0 == a.indexOf("%Axis") ? this.w.Na(a) : this.b.get(a)
};
z.Pa = function(a) {
  return se(a, this.w.ca()) ? this.w.ca().Pa(a) : 0 == a.indexOf("%Axis") ? this.w.Pa(a) : 4
};
z.qa = function() {
  this.D.clear()
};
z.Kt = function(a, b, c, d) {
  if(b.Au()) {
    var f = this.w.fa().rd(c), d = 0;
    switch(b.Ib()) {
      case um:
        d = this.w;
        c = d.Ng ? d.Je + d.Zd : d.Je + d.Ll;
        d = c += d.Nl;
        break;
      case vm:
        d = this.w;
        c = d.Ng ? d.Je + d.Zd : d.Je + d.Ll;
        c += d.Nl;
        d = c += d.Qu;
        break;
      case wm:
        d = this.w, c = d.Ng ? d.Je + d.Zd : d.Je + d.Ll, c = d.O != p ? c + Math.max(d.vk, xm(d)) : c + d.vk, c += d.Qu, d = c += d.Nl
    }
    this.w.J.pn.xj(f, b.ga(), d, b.n(), b.kv);
    b.qa(a, this.Sg.fb())
  }else {
    c = this.w.fa().rd(c);
    d = this.w.fa().rd(d);
    switch(b.Ib()) {
      case ym:
        f = 0;
        break;
      case zm:
        f = 1;
        break;
      case Am:
        f = 2
    }
    this.w.J.pn.zv(c, d, f, b.ga(), b.n(), b.kv);
    b.qa(a, this.D)
  }
};
var wm = 0, um = 1, vm = 2, ym = 3, zm = 4, Am = 5, Bm = {gL:wm, dL:um, fL:vm, kP:ym, BO:zm, RO:Am};
function om() {
  tm.call(this);
  this.Sc = new ze;
  this.kv = new O;
  this.jn = j;
  this.Ia = 5;
  this.Aa = wm
}
A.e(om, tm);
z = om.prototype;
z.Aa = p;
z.Ib = u("Aa");
z.Ia = p;
z.ga = u("Ia");
z.kv = p;
z.jn = p;
z.Au = u("jn");
z.Sc = p;
z.yc = p;
z.n = function() {
  return this.Sc.n()
};
z.g = function(a) {
  this.Sc.g(a);
  C(a, "padding") && (this.Ia = M(a, "padding"));
  C(a, "format") && (this.yc = new je(J(a, "format")));
  if(C(a, "position")) {
    switch(Wb(a, "position")) {
      case "beforeaxislabels":
        this.Aa = wm;
        this.jn = j;
        break;
      case "afteraxislabels":
        this.Aa = um;
        this.jn = j;
        break;
      case "axis":
        this.Aa = vm;
        this.jn = j;
        break;
      case "near":
        this.Aa = ym;
        this.jn = q;
        break;
      case "center":
        this.Aa = zm;
        this.jn = q;
        break;
      case "far":
        this.Aa = Am, this.jn = q
    }
  }
};
z.p = function(a, b) {
  this.Sc.uc(a, this.yc.ta(b))
};
z.qa = function(a, b) {
  var c = this.Sc.jc(a);
  c.qb(this.kv.x, this.kv.y);
  b.ia(c)
};
function Cm(a) {
  tm.call(this, a)
}
A.e(Cm, tm);
z = Cm.prototype;
z.bb = p;
z.sf = p;
z.Rd = p;
z.xA = p;
z.wA = p;
z.hs = p;
z.g = function(a, b) {
  Cm.f.g.call(this, a, b);
  C(a, "value") && (this.xA = this.wA = this.hs = J(a, "value"));
  C(a, "start_value") && (this.xA = J(a, "start_value"));
  C(a, "end_value") && (this.wA = J(a, "end_value"))
};
z.Qb = function() {
  return new qm
};
z.Rb = x("line_axis_marker_style");
z.p = function(a) {
  this.xA && (this.sf = this.ta(this.xA));
  this.wA && (this.Rd = this.ta(this.wA));
  this.hs && (this.bb = this.ta(this.hs));
  this.kB && !this.w.$D() && (isNaN(this.sf) || this.vq(this.sf), isNaN(this.Rd) || this.vq(this.Rd));
  Cm.f.p.call(this, a)
};
z.qa = function(a) {
  if(this.em) {
    Cm.f.qa.call(this, a);
    var b = this.K.wa, c = this.w.fa(), d = Md(b.Kb, a), d = d + md(), c = Zl(this.w.J.pn, c.rd(this.sf), c.rd(this.Rd)), f = a.ja();
    f.setAttribute("d", c);
    f.setAttribute("style", d);
    this.D.appendChild(f);
    b.Mb() && this.Kt(a, b.Mb(), this.sf, this.Rd)
  }
};
z.Na = function(a) {
  "%Value" == a && !oe(this.b, a) && this.b.add(a, (this.sf + this.Rd) / 2);
  ("%StartValue" == a || "%Start" == a) && !oe(this.b, a) && this.b.add(this.sf);
  ("%EndValue" == a || "%End" == a) && !oe(this.b, a) && this.b.add(this.sf);
  return Cm.f.Na.call(this, a)
};
z.Pa = function(a) {
  switch(a) {
    case "%Value":
    ;
    case "%Start":
    ;
    case "%StartValue":
    ;
    case "%End":
    ;
    case "%EndValue":
      return 2
  }
};
function Dm(a) {
  tm.call(this, a)
}
A.e(Dm, tm);
z = Dm.prototype;
z.la = p;
z.pa = p;
z.np = p;
z.mp = p;
z.ip = p;
z.hp = p;
z.bG = p;
z.ZF = p;
z.aG = p;
z.$F = p;
z.YF = p;
z.XF = p;
z.g = function(a, b) {
  Dm.f.g.call(this, a, b);
  C(a, "minimum") && (this.bG = J(a, "minimum"));
  C(a, "minimum_start") && (this.aG = J(a, "minimum_start"));
  C(a, "minimum_end") && (this.$F = J(a, "minimum_end"));
  C(a, "maximum") && (this.ZF = J(a, "maximum"));
  C(a, "maximum_start") && (this.YF = J(a, "maximum_start"));
  C(a, "maximum_end") && (this.XF = J(a, "maximum_end"))
};
z.Qb = function() {
  return new sm
};
z.Rb = x("range_axis_marker_style");
z.p = function(a) {
  this.bG && (this.np = this.mp = this.la = this.ta(this.bG));
  this.aG && (this.np = this.ta(this.aG));
  this.$F && (this.mp = this.ta(this.$F));
  this.ZF && (this.ip = this.hp = this.pa = this.ta(this.ZF));
  this.YF && (this.ip = this.ta(this.YF));
  this.XF && (this.hp = this.ta(this.XF));
  Dm.f.p.call(this, a)
};
z.qa = function(a) {
  if(this.em) {
    Dm.f.qa.call(this, a);
    var b = this.K.wa, c = this.w.fa(), d = this.w.J.pn, f, g = c.rd(this.np), k = c.rd(this.mp), l = c.rd(this.ip), c = c.rd(this.hp);
    f = new P;
    var n = new O, m = new O, o = new O, v = new O;
    d.dg(g, n);
    d.dg(l, o);
    d.cg(k, m);
    d.cg(c, v);
    f.x = Math.min(n.x, o.x, m.x, v.x);
    f.y = Math.min(n.y, o.y, m.y, v.y);
    f.width = Math.max(n.x, o.x, m.x, v.x) - f.x;
    f.height = Math.max(n.y, o.y, m.y, v.y) - f.y;
    g = Zl(d, g, k, j);
    k = Zl(d, l, c, q, j);
    b.mc() && (f = Kd(b.mc(), a, f), f += nd(), n = a.ja(), n.setAttribute("d", g + k), n.setAttribute("style", f), this.D.appendChild(n));
    b.iz && (f = Md(b.iz, a), f += md(), n = a.ja(), n.setAttribute("d", g), n.setAttribute("style", f), this.D.appendChild(n));
    b.ez && (f = Md(b.ez, a), f += md(), n = a.ja(), n.setAttribute("d", Zl(d, l, c, j)), n.setAttribute("style", f), this.D.appendChild(n));
    b.De() && (f = Hc(b.De(), a), d = a.ja(), d.setAttribute("d", g + k), d.setAttribute("style", f), this.D.appendChild(d));
    b.Mb() && this.Kt(a, b.Mb(), (this.np + this.ip) / 2, (this.mp + this.hp) / 2)
  }
};
z.Na = function(a) {
  oe(this.b, a) || ("%Value" == a && this.b.add(a, (this.np + this.mp + this.ip + this.hp) / 4), ("%MinValue" == a || "%Min" == a) && this.b.add(a, (this.np + this.mp) / 2), ("%MaxValue" == a || "%Max" == a) && this.b.add(a, (this.ip + this.hp) / 2), ("%MinStartValue" == a || "%MinStart" == a) && this.b.add(a, this.np), ("%MaxStartValue" == a || "%MaxStart" == a) && this.b.add(a, this.ip), ("%MinEndValue" == a || "%MinEnd" == a) && this.b.add(a, this.mp), ("%MaxEndValue" == a || "%MaxEnd" == a) && 
  this.b.add(a, this.hp));
  return Dm.f.Na.call(this, a)
};
z.Pa = function(a) {
  switch(a) {
    case "%Value":
    ;
    case "%MinValue":
    ;
    case "%Min":
    ;
    case "%MaxValue":
    ;
    case "%Max":
    ;
    case "%MinStartValue":
    ;
    case "%MinStart":
    ;
    case "%MaxStartValue":
    ;
    case "%MaxStart":
    ;
    case "%MinEndValue":
    ;
    case "%MinEnd":
    ;
    case "%MaxEndValue":
    ;
    case "%MaxEnd":
      return 2
  }
  return Dm.f.Pa.call(this, a)
};
function Em() {
  this.uw = this.Iw = this.Ws = 0
}
z = Em.prototype;
z.w = p;
z.vj = t("w");
z.D = p;
z.fb = u("D");
z.EK = t("D");
z.He = p;
z.Ws = p;
z.Iw = p;
z.uw = p;
z.g = function(a, b) {
  this.He = [];
  var c, d, f, g, k;
  if(C(a, "lines")) {
    c = F(a, "lines");
    if(!C(c, "line")) {
      return
    }
    k = Fm(c);
    g = I(c, "line");
    c = 0;
    for(d = g.length;c < d;c++) {
      f = new Cm(this.w), f.It = k, f.g(g[c], b), f.Sg = this, this.He.push(f)
    }
  }
  if(C(a, "ranges") && (c = F(a, "ranges"), C(c, "range"))) {
    k = Fm(c);
    g = I(c, "range");
    c = 0;
    for(d = g.length;c < d;c++) {
      f = new Dm(this.w), f.It = k, f.g(g[c], b), f.Sg = this, this.He.push(f)
    }
  }
};
function Fm(a) {
  return C(a, "display_under_data") ? K(a, "display_under_data") : j
}
z.p = function(a) {
  for(var b = this.He.length, c = 0;c < b;c++) {
    var d = this.He[c];
    d.p(a);
    if((d = d.Mb()) && d.Au()) {
      var f = Bm, g = this.w.J.ij;
      switch(d.Ib()) {
        case f.dL:
          d = g.og(d.n()) + d.ga();
          this.Ws = Math.max(d, this.Ws);
          break;
        case f.gL:
          d = g.og(d.n()) + d.ga();
          this.uw = Math.max(d, this.uw);
          break;
        case f.fL:
          d = g.og(d.n()) + d.ga(), this.Iw = Math.max(d, this.Iw)
      }
    }
  }
};
z.qa = function(a) {
  for(var b = this.He.length, c = 0;c < b;c++) {
    this.He[c].qa(a)
  }
};
function Gm() {
}
z = Gm.prototype;
z.Ma = j;
z.isEnabled = u("Ma");
z.WI = j;
z.ee = p;
z.Ye = u("ee");
z.Im = p;
z.Mt = p;
z.zn = p;
z.bv = p;
z.g = function(a) {
  C(a, "enabled") && (this.Ma = K(a, "enabled"));
  C(a, "interlaced") && (this.WI = K(a, "interlaced"));
  Tb(F(a, "line")) && (this.ee = new Ld, this.ee.g(F(a, "line")));
  if(this.WI && C(a, "interlaced_fills")) {
    a = F(a, "interlaced_fills");
    if(C(a, "even")) {
      var b = F(a, "even");
      C(b, "fill") && Tb(F(b, "fill")) && (this.Im = new Jd, this.Im.g(F(b, "fill")));
      C(b, "hatch_fill") && Tb(F(b, "hatch_fill")) && (this.Mt = new Fc, this.Mt.g(F(b, "hatch_fill")))
    }
    if(C(a, "odd") && (a = F(a, "odd"), C(a, "fill") && Tb(F(a, "fill")) && (this.zn = new Jd, this.zn.g(F(a, "fill"))), C(a, "hatch_fill") && Tb(F(a, "hatch_fill")))) {
      this.bv = new Fc, this.bv.g(F(a, "hatch_fill"))
    }
  }
};
function Hm() {
  this.Kb = new Ld;
  this.Kb.ct = 0
}
z = Hm.prototype;
z.Kb = p;
z.ke = 10;
z.ec = u("ke");
z.ul = q;
z.tz = j;
z.Au = u("tz");
z.TE = q;
z.k = p;
z.g = function(a) {
  this.Kb.g(a);
  C(a, "size") && (this.ke = M(a, "size"));
  C(a, "inside") && (this.ul = K(a, "inside"));
  C(a, "outside") && (this.tz = K(a, "outside"));
  C(a, "opposite") && (this.TE = K(a, "opposite"));
  !this.ul && !this.tz && !this.TE ? this.Kb = p : this.k = new P
};
z.qa = function(a, b, c, d, f, g) {
  var k = this.Kb.Va(), l, n;
  this.ul && (b && f.yv(k, c, d, this.ke, this.k), l = g || Md(this.Kb, a.r(), this.k), l != p && 0 < l.length && (n = f.Tv(a.r(), c, d, this.ke, this.Kb.Va()), n.setAttribute("style", l), a.appendChild(n)));
  this.tz && (b && f.Bv(k, c, d, this.ke, this.k), l = g || Md(this.Kb, a.r(), this.k), l != p && 0 < l.length && (n = f.Vv(a.r(), c, d, this.ke, this.Kb.Va()), n.setAttribute("style", l), a.appendChild(n)));
  this.TE && (b && A.xa(), l = g || Md(this.Kb, a.r(), this.k), l != p && 0 < l.length && (n = f.Uv(a.r(), c, d, this.ke, this.Kb.Va()), n.setAttribute("style", l), a.appendChild(n)))
};
function Im(a, b) {
  De.apply(this);
  this.w = a;
  this.J = b;
  this.Aa = new O;
  this.J.ls.GF(this)
}
A.e(Im, De);
z = Im.prototype;
z.Je = 0;
z.Tf = u("Je");
z.Ei = t("Je");
z.yd = 0;
z.nd = u("yd");
z.Aa = p;
z.J = p;
z.w = p;
z.Bh = u("yc");
z.g = function(a) {
  var b = this.Bb.bc;
  Im.f.g.call(this, a);
  Be(this, -b - this.Bb.bc)
};
z.p = function(a) {
  this.uc(a, this.yc.ta(this.w, this.w.Gd()));
  return this.yd = this.J.ij.og(this.n()) + this.Ia
};
function Jm() {
  this.ba = [];
  this.b = new ne;
  this.Cb();
  this.Wm = new P;
  this.JK = new nc
}
z = Jm.prototype;
z.Ma = j;
z.isEnabled = u("Ma");
z.yg = t("Ma");
z.Jb = x(0);
z.r = function() {
  return this.aa.r()
};
z.Aa = NaN;
z.Ib = u("Aa");
z.qb = t("Aa");
z.Sa = p;
z.getName = function() {
  return this.Ya ? this.Ya.Bh().ta(this) : this.Sa
};
z.Sl = t("Sa");
z.aa = p;
z.ca = u("aa");
z.Ul = t("aa");
z.dn = p;
z.ba = p;
z.Nm = 0;
z.JA = 0;
z.yd = 0;
z.nd = u("yd");
z.Zd = 0;
z.kr = 0;
z.Ll = 0;
z.Cr = 0;
z.vk = 0;
z.Qu = 0;
z.He = p;
z.J = p;
z.La = p;
z.fa = u("La");
z.ZB = function(a) {
  this.J = a.ZB(this)
};
z.Xf = p;
z.nJ = u("Xf");
z.$D = x(q);
z.Ng = p;
z.Ia = 0;
z.Je = 0;
z.Ei = t("Je");
z.Gx = 0;
z.Yf = p;
z.ag = p;
z.Zf = p;
z.mf = p;
z.ee = p;
z.fm = p;
z.Ya = p;
z.O = p;
z.So = q;
z.JK = p;
z.Nl = 0;
z.xp = p;
z.Gd = function() {
  return this.aa.Gd()
};
z.$i = function(a) {
  return this.La.$i(a)
};
z.Gc = function(a) {
  var b = this.La;
  if(isNaN(b.ld) || a < b.ld) {
    b.ld = a
  }
  if(isNaN(b.kd) || a > b.kd) {
    b.kd = a
  }
};
z.transform = function(a, b, c, d) {
  isNaN(d) && (d = 0);
  this.J.Fv(this.La.pc(b) + d, c)
};
z.yk = s();
z.PB = s();
z.wJ = x(q);
z.g = function(a, b) {
  this.Je = this.Nm = 0;
  this.Gx = this.Ia = 10;
  C(a, "enabled") && (this.Ma = K(a, "enabled"));
  if(this.Ma) {
    Ub(a, "zoom") && Le("Zoom");
    var c = F(a, "major_grid");
    Tb(c) && (this.Yf = new Gm, this.Yf.g(c), Km(this, this.Yf));
    c = F(a, "minor_grid");
    Tb(c) && (this.ag = new Gm, this.ag.g(c), Km(this, this.ag));
    this.Ll = this.kr = this.Zd = 0;
    c = F(a, "major_tickmark");
    if(Tb(c) && (this.Zf = new Hm, this.Zf.g(c), this.Zf.Kb != p && (this.Zf.Kb.se() && this.J.$d.Me(this.Zf.Kb.yb), this.Zf.Au() && (this.Zd += this.Zf.ec(), this.Ll = this.Zf.ec()), this.Zf.ul && !this.Ng))) {
      this.Zd += this.Zf.ec(), this.kr = this.Zf.ec()
    }
    c = F(a, "minor_tickmark");
    if(Tb(c) && (this.mf = new Hm, this.mf.g(c), this.mf.Kb != p)) {
      if(this.mf.Kb.se() && this.J.$d.Me(this.mf.Kb.yb), c = 0, this.mf.Au() && (c += this.mf.ec(), this.mf.ec() > this.Ll && (this.Ll = this.mf.ec())), this.mf.ul && !this.Ng && (c += this.mf.ec(), this.mf.ec() > this.kr && (this.kr = this.mf.ec())), c > this.Zd) {
        this.Zd = c
      }
    }
    c = F(a, "line");
    Tb(c) && (this.ee = new Ld, this.ee.g(c), this.ee.se() && (this.zE = new P, this.J.$d.Me(this.ee.yb)));
    c = F(a, "zero_line");
    Tb(c) && (this.fm = new Ld, this.fm.g(c), this.fm.se() && (this.wG = new P, this.J.$d.Me(this.fm.yb)));
    this.Nm = this.Zd;
    this.Gx += this.kr;
    this.Ng || (this.Nm += this.Ia);
    c = F(a, "title");
    Tb(c) && (this.Ya = new Im(this, this.J), this.Ya.g(c));
    c = F(a, "labels");
    Tb(c) && (this.O = this.kx(c)) && this.O.g(c);
    Ub(a, "axis_markers") && (this.He = new Em, this.He.vj(this), this.He.g(F(a, "axis_markers"), b), this.He.EK(this.tp));
    this.yd = this.Nm
  }
};
function Lm(a) {
  return 3 == a.La.Jb() || 2 == a.La.Jb()
}
z.kx = function() {
  A.xa()
};
function Km(a, b) {
  b.Im != p && b.Im.se() && a.J.$d.Me(b.Im.yb);
  b.zn != p && b.zn.se() && a.J.$d.Me(b.zn.yb);
  b.ee != p && b.Ye().se() && a.J.$d.Me(b.Ye().yb)
}
z.b = p;
z.Cb = function() {
  this.b.add("%AxisName", "");
  this.b.add("%AxisValue", 0);
  this.b.add("%AxisMax", -Number.MAX_VALUE);
  this.b.add("%AxisMin", Number.MAX_VALUE);
  this.b.add("%AxisBubbleSizeMin", Number.MAX_VALUE);
  this.b.add("%AxisBubbleSizeMax", -Number.MAX_VALUE)
};
z.Na = function(a) {
  if(("%AxisValue" == a || "%Value" == a) && this.O) {
    return this.O.KI()
  }
  if(se(a, this.ca())) {
    return this.ca().Na(a)
  }
  "%AxisScaleMax" == a && this.b.add("%AxisScaleMax", this.La.pa);
  "%AxisScaleMin" == a && this.b.add("%AxisScaleMin", this.La.la);
  oe(this.b, "%AxisRange") || this.b.add("%AxisRange", this.b.get("%AxisMax") - this.b.get("%AxisMin"));
  oe(this.b, "%AxisAverage") || this.b.add("%AxisAverage", this.b.get("%AxisSum") / this.ba.length);
  oe(this.b, "%AxisMedian") || this.b.add("%AxisMedian", pc(this.ba, this.dn ? "%XValue" : "%YValue"));
  oe(this.b, "%AxisMode") || this.b.add("%AxisMode", this.JK.ml(this.ba, this.dn ? "%XValue" : "%YValue"));
  return this.b.get(a)
};
z.Pa = function(a) {
  if(Lm(this) && "%Value" == a) {
    return 3
  }
  switch(a) {
    default:
      return 1;
    case "%Value":
    ;
    case "%AxisValue":
    ;
    case "%AxisSum":
    ;
    case "%AxisMax":
    ;
    case "%AxisMin":
    ;
    case "%AxisRange":
    ;
    case "%AxisScaleMax":
    ;
    case "%AxisScaleMin":
    ;
    case "%AxisAverage":
    ;
    case "%AxisBubbleSizeSum":
    ;
    case "%AxisBubbleSizeMin":
    ;
    case "%AxisBubbleSizeMax":
    ;
    case "%AxisMode":
    ;
    case "%AxisMedian":
      return 2
  }
};
function Mm(a) {
  a.Cr = 0;
  a.vk = 0;
  a.Qu = 0;
  a.He != p && (a.He.p(a.aa.r()), a.Qu = a.He.Ws, a.Cr = a.He.uw + a.He.Ws, a.vk = a.He.Iw)
}
function xm(a) {
  return a.O && !a.O.ul ? a.O.Zv : 0
}
function Nm(a, b) {
  a.Ma && (a.J.Jp(b), a.La.Ae(), a.O && a.O.FB())
}
function Om(a, b) {
  a.Ma && (a.O ? a.J.bo(b, Math.max(xm(a), a.vk)) : a.J.bo(b, a.vk))
}
z.Rc = function(a) {
  this.J.Jp(a);
  this.La.Ae();
  Pm(this)
};
z.BK = function(a, b) {
  Qm(this, a, b, this.La.fc)
};
function Qm(a, b, c, d) {
  if(a.Ma && a.O != p) {
    var f = a.O.Px(a.aa.r(), 0), d = a.O.Px(a.aa.r(), d);
    a.J.Jp(c);
    f && d && (f.x > b.width ? f = d : d.x > b.width && (d = f), f.y > b.height ? f = d : d.y > b.height && (d = f), a.J.ij.nB(b, f, d))
  }
}
function sh(a) {
  return a.O && a.O.isEnabled()
}
z.Ss = function() {
  return this.O.ao
};
function th(a) {
  a.O.bn(a.aa.r())
}
z.Jx = p;
z.Xg = p;
z.tp = p;
z.zE = p;
z.wG = p;
function Rm(a) {
  if(a.Ma && a.ee != p) {
    var b = a.Je, c = a.J.uD(), d = a.J.tD();
    a.ee.se() && a.J.$d.yj(a.ee.Va(), c, d, b, a.zE);
    var f = Md(a.ee, a.aa.r(), a.zE);
    f && (b = a.J.$d.Wv(a.Jx.r(), c, d, b, a.ee.Va()), b.setAttribute("style", f), a.Jx.appendChild(b))
  }
}
z.uC = function() {
  if(this.Ma && !(this.fm == p || !this.La.contains(0) || 0 == this.La.la)) {
    var a = this.La.pc(0);
    this.fm.se() && this.J.$d.Cv(this.fm.Va(), a, this.wG);
    var b = Md(this.fm, this.Xg.r(), this.wG);
    b && (a = this.J.$d.Xv(this.Xg.r(), a, this.fm.Va()), a.setAttribute("style", b), this.Xg.appendChild(a))
  }
};
function Pm(a) {
  a.Ma && a.Ya && (a.Ya.p(a.Xg.r()), a.JA = a.Ya.nd())
}
z.yx = function() {
  if(this.Ma && this.Ya) {
    var a = this.Ng ? this.Zd : this.Ll, b = this.Je;
    this.O && this.O.isEnabled() ? a += Math.max(this.vk, xm(this)) : b += this.vk;
    b += this.Nl + this.Cr;
    this.Ya.Ei(b + a);
    a = this.Ya;
    b = this.Jx;
    a.J.ls.zj(a, a.Aa);
    var c = a.jc(b.r());
    c.qb(a.Aa.x, a.Aa.y);
    b.ia(c)
  }
};
z.yo = function() {
  if(this.Ma && this.O != p) {
    if(this.O.ul) {
      this.O.Ei(this.Je - this.kr - 2 * this.O.ga() - this.O.Zv)
    }else {
      var a = this.Ng ? this.Zd : this.Ll, b = this.Je, a = a + this.O.ga(), b = b + this.Nl, b = b + this.Qu;
      this.O.Ei(b + a)
    }
    this.O.yo(this.tp)
  }
};
function Sm(a) {
  a.Ma && (a.mf != p && a.mf.Kb != p && a.tC(), a.Zf != p && a.Zf.Kb != p && a.sC())
}
z.sC = function() {
  var a = this.Zf.Kb.se(), b = p;
  a || (b = Md(this.Zf.Kb, this.tp.r()));
  var c = this.Je, d = this.La.fc, f = 0;
  this.So && (d--, f = 0.5);
  for(var g = 0;g <= d;g++) {
    var k = this.La.Mc(g) + f;
    k >= this.La.la && k <= this.La.pa && this.Zf.qa(this.tp, a, this.La.rd(k), c, this.J.ks, b)
  }
};
z.tC = function() {
  var a = this.mf.Kb.se(), b = p;
  a || (b = Md(this.mf.Kb, this.tp.r()));
  var c = this.Je, d = 0;
  this.So && (d = 0.5);
  var f = this.fa().fc - 1, g = q;
  this.fa().Mc(f) < this.fa().pa && (f++, g = j);
  for(var k = this.fa().Jf, l = 0;l <= f;l++) {
    for(var n = this.fa().Mc(l), m = 1;m < k;m++) {
      if(!(0 != d && l + 1 == this.La.fc && m == this.La.Jf)) {
        var o = this.fa().jk(n, m) + d;
        if(g && !this.La.Uy(o)) {
          break
        }
        this.mf.qa(this.tp, a, this.La.rd(o), c, this.J.ks, b)
      }
    }
  }
};
function Tm(a) {
  if(a.Ma && a.Yf && a.Yf.ee != p) {
    var b = a.Yf.Ye().se(), c = p;
    b || (c = Md(a.Yf.Ye(), a.Xg.r()));
    var d = a.La.fc, f = 0;
    a.So && (d--, f = 0.5);
    for(var g = 0;g <= d;g++) {
      var k = a.La.rd(a.La.Mc(g) + f);
      b && (a.J.$d.Cv(a.Yf.Ye().Va(), k, a.Wm), c = Md(a.Yf.Ye(), a.Xg.r(), a.Wm));
      c && (k = a.J.$d.Xv(a.Xg.r(), k, a.Yf.Ye().Va()), k.setAttribute("style", c), a.Xg.appendChild(k))
    }
  }
}
function Um(a) {
  if(a.Ma && a.ag && a.ag.ee != p) {
    var b = a.ag.Ye().se(), c = p;
    b || (c = Md(a.ag.Ye(), a.Xg.r()));
    var d = a.La.fc - 1, f = q;
    a.La.Mc(d) < a.fa().pa && (d++, f = j);
    var g = 0;
    a.So && (g = 0.5);
    for(var k = 0;k <= d;k++) {
      for(var l = a.La.Mc(k), n = 1;n < a.La.Jf;n++) {
        if(!(0 != g && k + 1 == a.La.fc && n == a.La.Jf)) {
          var m = a.La.jk(l, n) + g;
          if(f && !a.La.Uy(m)) {
            break
          }
          m = a.La.rd(m);
          b && (a.J.$d.Cv(a.ag.Ye().Va(), m, a.Wm), c = c = Md(a.ag.Ye(), a.Xg.r(), a.Wm));
          c && (m = a.J.$d.Xv(a.Xg.r(), m, a.ag.Ye().Va()), m.setAttribute("style", c), a.Xg.appendChild(m))
        }
      }
    }
  }
}
function Vm(a) {
  if(a.Ma) {
    var b, c;
    if(a.ag != p && (b = a.ag.Im != p != p || a.ag.Mt != p != p, c = a.ag.zn != p != p || a.ag.bv != p != p, b || c)) {
      var d = q, f = 0;
      a.So && (f = 0.5);
      for(var g = 0;g < a.La.fc;g++) {
        for(var k = a.La.Mc(g), l = 0;l < a.La.Jf;l++) {
          if(!(0 != f && g + 1 == a.La.fc && l == a.La.Jf)) {
            if(d && c || !d && b) {
              var n = a.La.jk(k, l) + f, m = a.La.jk(k, l + 1) + f;
              Wm(a, n, m, a.ag, d)
            }
            d = !d
          }
        }
      }
    }
    if(a.Yf != p && (b = a.Yf.Im != p != p || a.Yf.Mt != p != p, c = a.Yf.zn != p != p || a.Yf.bv != p != p, b || c)) {
      d = q;
      f = a.La.fc;
      g = 0;
      a.So && (f--, g = 0.5);
      for(k = 0;k < f;k++) {
        if(d && c || !d && b) {
          l = a.La.Mc(k) + g, n = a.La.Mc(k + 1) + g, Wm(a, l, n, a.Yf, d)
        }
        d = !d
      }
    }
  }
}
function Wm(a, b, c, d, f) {
  b = a.La.rd(b);
  c = a.La.rd(c);
  a.J.$d.DF(b, c, a.Wm);
  f ? (b = d.zn, d = d.bv) : (b = d.Im, d = d.Mt);
  c = a.Xg;
  f = p;
  if(b != p && (f = Kd(b, c.r(), a.Wm))) {
    b = a.J.$d.BA(c.r(), a.Wm), b.setAttribute("style", f), c.appendChild(b)
  }
  if(d != p && (f = Hc(d, c.r()))) {
    b = a.J.$d.BA(c.r(), a.Wm), b.setAttribute("style", f), c.appendChild(b)
  }
}
z.mb = function(a) {
  if(a == h || a == p) {
    a = {}
  }
  a.Key = this.Sa;
  a.Name = this.b.get("%AxisName");
  a.Sum = this.b.get("%AxisSum");
  a.Max = this.b.get("%AxisMax");
  a.Min = this.b.get("%AxisMin");
  a.BubbleSizeMax = this.b.get("%AxisBubbleMax");
  a.BubbleSizeMin = this.b.get("%AxisBubbleMin");
  a.BubbleSizeSum = this.b.get("%AxisBubbleSum");
  a.Average = this.b.get("%AxisAverage");
  a.Median = this.b.get("%AxisMedian");
  a.Mode = this.b.get("%AxisMode");
  return a
};
function Xm() {
  Jm.apply(this)
}
A.e(Xm, Jm);
Xm.prototype.Jb = x(1);
Xm.prototype.wJ = x(j);
Xm.prototype.wH = function(a) {
  switch(Vb(F(a, "type"))) {
    default:
    ;
    case "linear":
      a = new rh(this);
      break;
    case "logarithmic":
      a = new wh(this);
      break;
    case "datetime":
      a = new yh(this)
  }
  return a
};
Xm.prototype.kx = function(a) {
  return C(a, "display_mode") && "stager" == N(a, "display_mode") ? new Rl(this, this.J.ij) : new Nl(this, this.J.ij)
};
function Ym(a) {
  this.aa = a;
  this.ba = [];
  this.Ge()
}
z = Ym.prototype;
z.Sa = p;
z.Sl = t("Sa");
z.getName = u("Sa");
z.na = NaN;
z.Xr = t("na");
z.Xa = 0;
z.aa = p;
z.ca = u("aa");
z.ba = p;
z.Ed = function(a) {
  a.mt(this);
  this.b.add("%CategoryName", this.Sa);
  this.b.add("%Name", this.Sa);
  this.b.add("%CategoryIndex", this.na);
  this.ba.push(a)
};
z.b = p;
z.Ge = function() {
  this.b = new ne;
  this.Cb()
};
z.Cb = function() {
  this.b.add("%CategoryName", "");
  this.b.add("%Name", "");
  this.b.add("%CategoryIndex", "");
  this.b.add("%CategoryPointCount", 0);
  this.b.add("%CategoryYSum", 0);
  this.b.add("%CategoryYMax", 0);
  this.b.add("%CategoryYMin", 0);
  this.b.add("%CategoryYRangeMax", -Number.MAX_VALUE);
  this.b.add("%CategoryYRangeMin", Number.MAX_VALUE);
  this.b.add("%CategoryYRangeSum", 0);
  this.b.add("%CategoryYPercentOfTotal", 0);
  this.b.add("%CategoryYAverage", 0)
};
z.Pa = function(a) {
  if(se(a, this.ca())) {
    return this.ca().Pa(a)
  }
  switch(a) {
    case "%CategoryPointCount":
    ;
    case "%CategoryYSum":
    ;
    case "%CategoryYMax":
    ;
    case "%CategoryYMin":
    ;
    case "%CategoryYRangeMax":
    ;
    case "%CategoryYRangeMin":
    ;
    case "%CategoryYRangeSum":
    ;
    case "%CategoryYPercentOfTotal":
    ;
    case "%CategoryYAverage":
      return 2;
    default:
    ;
    case "%CategoryName":
    ;
    case "%Name":
      return 1
  }
};
z.Na = function(a) {
  if(se(a, this.ca())) {
    return this.ca().b.get(a)
  }
  oe(this.b, "%CategoryYPercentOfTotal") || this.b.add("%CategoryYPercentOfTotal", 100 * (this.b.get("%CategoryYSum") / this.ca().Na("%DataPlotYSum")));
  oe(this.b, "%CategoryYAverage") || this.b.add("%CategoryYAverage", this.b.get("%CategoryYSum") / this.b.get("%CategoryPointCount"));
  return this.b.get(a)
};
z.mb = function(a) {
  if(a == p || a == h) {
    a = {}
  }
  a.Name = this.Sa;
  a.XSum = this.b.get("%CategoryXSum");
  a.YSum = this.b.get("%CategoryYSum");
  a.BubbleSizeSum = this.b.get("%CategoryBubbleSizeSum");
  a.YRangeMax = this.b.get("%CategoryRangeMax");
  a.YRangeMin = this.b.get("%CategoryRangeMin");
  a.YRangeSum = this.b.get("%CategoryRangeSum");
  a.YPercentOfTotal = this.b.get("%CategoryYPercentOfTotal");
  a.YMedian = this.b.get("%CategoryYMedian");
  a.YMode = this.b.get("%CategoryYMode");
  a.YAverage = this.b.get("%CategoryYAverage");
  return a
};
z.ah = function() {
  for(var a = this.ba.length, b = 0;b < a;b++) {
    this.ba[b].ah()
  }
};
z.dh = function() {
  for(var a = this.ba.length, b = 0;b < a;b++) {
    this.ba[b].dh()
  }
};
function Zm(a) {
  Ym.call(this, a)
}
A.e(Zm, Ym);
Zm.prototype.w = p;
Zm.prototype.vj = t("w");
function $m() {
  this.Gw = {}
}
z = $m.prototype;
z.Xm = NaN;
z.Dn = NaN;
z.Il = 0;
z.Gw = p;
z.pt = NaN;
z.aH = NaN;
z.Ae = function(a) {
  this.aH = a;
  this.pt = a / (this.Xm + this.Il + this.Dn * (this.Il - 1))
};
z.Tf = function(a) {
  return this.pt * (a * (1 + this.Dn) + 0.5 + this.Xm / 2) - this.aH / 2
};
function an(a) {
  this.rJ = a;
  this.yp = [];
  this.qp = [];
  this.ba = []
}
z = an.prototype;
z.rJ = q;
z.Ol = 0;
z.Hl = 0;
z.yp = p;
z.qp = p;
z.ba = p;
z.Pd = function(a, b) {
  var c = j;
  if(0 == b && 0 < this.ba.length) {
    for(var c = this.ba.length - 1, d = this.ba[c];0 == d.hc && 0 < c;) {
      c--, d = this.ba[c]
    }
    c = 0 <= d.hc
  }
  this.ba.push(a);
  d = 0 < b;
  0 == b && (d = c);
  if(d) {
    return 0 < this.yp.length && (a.gf = this.yp[this.yp.length - 1]), this.yp.push(a), this.Ol += b
  }
  0 < this.qp.length && (a.gf = this.qp[this.qp.length - 1]);
  this.qp.push(a);
  return this.Hl += b
};
function bn(a, b) {
  return a.rJ ? 100 : 0 < b ? a.Ol : a.Hl
}
function cn(a, b, c, d, f) {
  Ym.call(this, a);
  this.na = b;
  this.Sa = c;
  this.dn = d;
  this.w = f;
  this.Kk = {};
  this.Az = []
}
A.e(cn, Zm);
z = cn.prototype;
z.dn = j;
z.zf = p;
z.wF = t("zf");
z.Kk = p;
z.Az = p;
z.Hw = p;
z.eo = p;
z.Vs = p;
function dn(a, b, c, d) {
  a.Kk[b] == p && (a.Kk[b] = {});
  a.Kk[b][c] == p && (a.Kk[b][c] = new an(d), d && a.Az.push(a.Kk[b][c]));
  return a.Kk[b][c]
}
z.yk = function() {
  for(var a = 0;a < this.Az.length;a++) {
    for(var b = this.Az[a], c = h, c = 0;c < b.yp.length;c++) {
      b.yp[c].FF(b.Ol)
    }
    for(c = 0;c < b.qp.length;c++) {
      b.qp[c].FF(b.Hl)
    }
  }
};
function en() {
  Jm.call(this);
  this.Wh = [];
  this.Vk = {};
  this.ot = []
}
A.e(en, Jm);
z = en.prototype;
z.Vk = p;
z.Wh = p;
z.ot = p;
z.$D = x(j);
z.uC = s();
z.yk = function() {
  for(var a = 0;a < this.Wh.length;a++) {
    this.Wh[a].yk()
  }
};
z.Rc = function(a) {
  en.f.Rc.call(this, a);
  for(var a = this.La.pc(this.La.la + 1) - this.La.pc(this.La.la), b = 0;b < this.ot.length;b++) {
    this.ot[b].Ae(a)
  }
};
z.kx = x(p);
z.BK = function(a, b) {
  Qm(this, a, b, this.La.fc - 1)
};
z.Ef = function(a, b) {
  return this.Vk[a] == h ? fn(this, a, b) : this.Vk[a]
};
z.qK = function(a) {
  this.aa.Bq.push(a)
};
z.g = function(a, b) {
  en.f.g.call(this, a, b);
  C(a, "tickmarks_placement") && (this.So = "center" == Wb(a, "tickmarks_placement"))
};
function fn(a, b, c) {
  var d = new cn(a.aa, a.Wh.length, b, a.dn, a);
  a.dn ? (a.aa.aL.push(d), a.qK(d)) : a.aa.bL.push(d);
  if(a.O != p) {
    var f = a.O;
    f.Vp = d;
    var g = f.yc.ta(f.w, f.w.Gd());
    f.Vp.Hw = g;
    f.uc(c, f.Vp.Hw);
    c = f.k.Ca();
    f.Vp.eo = c;
    c = f.nf.Ca();
    f.Vp.Vs = c;
    f.Vp = p
  }
  a.Gc(d.na);
  a.Wh.push(d);
  return a.Vk[b] = d
}
z.kx = function(a) {
  return C(a, "display_mode") && "stager" == N(a, "display_mode") ? new gn(this, this.J.ij) : new hn(this, this.J.ij)
};
z.wH = function(a) {
  return!a || !C(a, "type") || "datetime" != Wb(a, "type") ? new uh(this) : new vh(this)
};
function jn() {
  en.call(this);
  this.RB = {}
}
A.e(jn, en);
jn.prototype.RB = {};
jn.prototype.Jb = x(2);
jn.prototype.PB = function(a) {
  if(a.fn()) {
    var b = this.RB[a.Wj];
    b == p && (b = new $m, b.Xm = a.Ab().Xm, b.Dn = a.Ab().Dn, this.RB[a.Wj] = b, this.ot.push(b));
    a.wF(b);
    if(a.eH()) {
      a.qo = 0, b.Il = 1
    }else {
      var c = a.eb, d = b.Il;
      1 != c.Jb() || 0 == c.fa().ml() ? (b.Il++, b = d) : b.Gw[c.getName()] != p ? b = b.Gw[c.getName()] : (b.Gw[c.getName()] = d, b.Il++, b = d);
      a.qo = b
    }
  }
};
jn.prototype.transform = function(a, b, c, d) {
  a.o.fn() ? (isNaN(d) && (d = 0), b = this.La.pc(b) + d, a = a.o, b += a.zf.Tf(a.qo), this.J.Fv(b, c)) : jn.f.transform.call(this, a, b, c, d)
};
function kn() {
  en.call(this)
}
A.e(kn, en);
kn.prototype.Jb = x(3);
kn.prototype.PB = function(a, b) {
  var c = fn(this, a.getName(), b);
  a.Dc = c;
  var d = new $m;
  d.Xm = a.Ab().Xm;
  d.Dn = a.Ab().Dn;
  c.wF(d);
  c = a.eb;
  1 == c.Jb() && 0 != c.fa().ml() && (a.tJ = j, d.Il = 1);
  this.ot.push(d)
};
kn.prototype.transform = function(a, b, c, d) {
  isNaN(d) && (d = 0);
  b = this.La.pc(b) + d;
  b += a.Ef().zf.Tf(a.qo);
  this.J.Fv(b, c)
};
kn.prototype.qK = s();
function hn(a, b) {
  Nl.call(this, a, b)
}
A.e(hn, Nl);
z = hn.prototype;
z.Vp = p;
z.KI = function() {
  return this.w.fa().nI(this.Vp)
};
z.bn = function() {
  this.Fl = new P;
  this.Ld = new P;
  for(var a = this.w.fa(), b = a.fc, c = 0;c <= b;c++) {
    var d;
    if(d = this.w.Wh[a.Mc(c) + 0.5]) {
      var f = d.eo;
      d = d.Vs;
      f.width > this.Ld.width && (this.Ld.width = f.width, this.Fl.width = d.width);
      f.height > this.Ld.height && (this.Ld.height = f.height, this.Fl.height = d.height)
    }
  }
};
z.yo = function(a) {
  var b = new O, c = this.J.J.w.fa(), d = !this.ao, f = d && this.J.tr(c.Nb()), g = f ? Number.POSITIVE_INFINITY : Number.NEGATIVE_INFINITY, k;
  this.Bb.or || (k = this.J.By(this.Fl, this.Bb));
  for(var l = 0;l < c.fc;l++) {
    var n = c.Mc(l), m = this.w.Wh[n + 0.5];
    if(m) {
      var o = m.eo, v = m.Vs;
      this.J.qf(this, b, o, v, c.rd(n + 0.5));
      if(d) {
        if(this.J.Fy(f, this.Bb, b, g, k, o, v)) {
          continue
        }
        g = this.J.Qx(f, this.Bb, b, k, o, v)
      }
      Pl(this, b, o, v, this.Bb);
      this.uc(a.r(), m.Hw);
      n = this.jc(a.r());
      n.qb(b.x, b.y);
      a.ia(n)
    }
  }
};
z.Px = function(a, b) {
  var c = new O, d = this.w.fa().Mc(b) + 0.5, f = this.w.Wh[d];
  if(!f) {
    return p
  }
  this.J.qf(this, c, f.eo, f.Vs, this.w.fa().rd(d));
  Pl(this, c, f.eo, f.Vs, this.Bb);
  d = f.eo.Ca();
  d.x = c.x;
  d.y = c.y;
  return d
};
function gn(a, b) {
  Nl.call(this, a, b)
}
A.e(gn, hn);
z = gn.prototype;
z.bn = function() {
  this.Ld = new P;
  this.fi = new P;
  for(var a = this.w.fa(), b = a.fc, c = 0;c <= b;c++) {
    var d;
    if(d = this.w.Wh[a.Mc(c) + 0.5]) {
      d = d.eo;
      var f = 0 == c % 2 ? this.Ld : this.fi;
      d.width > f.width && (f.width = d.width);
      d.height > f.height && (f.height = d.height)
    }
  }
};
z.$s = function(a) {
  this.bn(a);
  var a = this.J.Ho(this.Ld, this.Ld, this.Bb), b = this.J.Ho(this.fi, this.fi, this.Bb), a = Math.ceil(a + b);
  return 1 > a ? 1 : a
};
z.FB = function() {
  this.yd = this.J.og(this.Ld) + this.J.og(this.fi) + this.Ia;
  this.Zv = this.yd + this.Ia
};
z.g = function(a) {
  gn.f.g.call(this, a);
  Be(this, 0)
};
z.yo = function(a) {
  for(var b = new O, c = !this.ao, d = this.w.fa(), f = c && this.J.tr(d.Nb()), g = f ? Number.POSITIVE_INFINITY : Number.NEGATIVE_INFINITY, k = g, l = this.J.og(this.Ld), n = this.J.og(this.fi), m, o, v, w = 0;w < d.fc;w++) {
    var y = d.Mc(w), E = this.w.Wh[y + 0.5];
    if(E) {
      (v = 0 == w % 2) ? (o = l, m = 0) : (o = n, m = l);
      var V = this.k;
      this.J.Fi(this, b, V, d.rd(y + 0.5), m, o);
      if(c) {
        if(v) {
          if(this.J.xr(f, b, V, g)) {
            continue
          }
          g = this.J.$q(f, b, V)
        }else {
          if(this.J.xr(f, b, V, k)) {
            continue
          }
          k = this.J.$q(f, b, V)
        }
      }
      this.uc(a.r(), E.Hw);
      m = this.jc(a.r());
      m.qb(b.x, b.y);
      a.ia(m)
    }
  }
};
function ln(a) {
  switch(a) {
    case "cylinder":
      return 1;
    case "pyramid":
      return 2;
    case "cone":
      return 3;
    default:
      return 0
  }
}
function mn(a, b) {
  Se.call(this, a, b)
}
A.e(mn, Qf);
mn.prototype.so = p;
mn.prototype.g = function(a) {
  a = mn.f.g.call(this, a);
  C(a, "corners") && (this.so = new Dd, this.so.g(F(a, "corners")))
};
function nn(a, b) {
  Se.call(this, a, b)
}
A.e(nn, Qf);
nn.prototype.g = function(a) {
  var a = nn.f.g.call(this, a), b = F(a, "fill");
  this.Gt(F(F(a, "fill"), "color"), C(b, "opacity") ? F(b, "opacity") : 1)
};
nn.prototype.Gt = function(a, b) {
  var c = {}, d = {}, f = [], g = {}, k = {}, l = {};
  g.position = "0";
  g.opacity = b;
  g.color = "DarkColor(" + a + ")";
  f.push(g);
  k.position = 0.41;
  k.opacity = b;
  k.color = "LightColor(" + a + ")";
  f.push(k);
  l.position = 1;
  l.opacity = b;
  l.color = "DarkColor(" + a + ")";
  f.push(l);
  d.type = "linear";
  d.key = f;
  d.angle = 90;
  c.enabled = j;
  c.type = "gradient";
  c.gradient = d;
  this.Hb = new Jd;
  this.Hb.g(c);
  this.Kb = new Ld;
  this.Kb.g(c)
};
function on(a, b) {
  Se.call(this, a, b)
}
A.e(on, Qf);
on.prototype.g = function(a) {
  var a = on.f.g.call(this, a), b = F(a, "fill");
  this.Gt(F(F(a, "fill"), "color"), C(b, "opacity") ? F(b, "opacity") : 1)
};
on.prototype.Gt = function(a, b) {
  var c = {}, d = {}, f = [], g = {}, k = {}, l = {};
  g.position = "0";
  g.opacity = b;
  g.color = "DarkColor(" + a + ")";
  f.push(g);
  k.position = 0.41;
  k.opacity = b;
  k.color = "LightColor(" + a + ")";
  f.push(k);
  l.position = 1;
  l.opacity = b;
  l.color = "DarkColor(" + a + ")";
  f.push(l);
  d.type = "linear";
  d.key = f;
  d.angle = "0";
  c.enabled = j;
  c.type = "gradient";
  c.gradient = d;
  this.Hb = new Jd;
  this.Hb.g(c);
  this.Kb = new Ld;
  this.Kb.g(c)
};
function pn() {
  this.P = new O;
  this.X = new O
}
A.e(pn, Pf);
z = pn.prototype;
z.P = p;
z.X = p;
z.vJ = p;
z.sf = p;
z.Rd = p;
z.hd = function() {
  var a = this.j.o;
  this.sf = isNaN(this.j.Ag) ? a.vs : this.j.Ag;
  this.Rd = this.j.hc;
  var b = this.j.Ab().VC(this.j) / 2, c;
  if(c = this.j.Ef() != p) {
    c = this.j.Ef();
    var d = a.eb.getName();
    c = c.Kk && c.Kk[d] && c.Kk[d][a.Jb()]
  }
  this.vJ = c;
  a.Wb.transform(this.j, this.j.Hd(), this.P, -b);
  a.eb.transform(this.j, this.sf, this.P);
  a.Wb.transform(this.j, this.j.Hd(), this.X, b);
  a.eb.transform(this.j, this.Rd, this.X);
  this.j.n().x = Math.min(this.P.x, this.X.x);
  this.j.n().y = Math.min(this.P.y, this.X.y);
  this.j.n().width = Math.max(this.P.x, this.X.x) - this.j.n().x;
  this.j.n().height = Math.max(this.P.y, this.X.y) - this.j.n().y
};
z.Qq = function() {
  A.xa()
};
function qn() {
}
A.e(qn, Nf);
qn.prototype.Ta = function() {
  var a = this.j.n();
  this.I.setAttribute("d", gd(a))
};
function rn() {
}
A.e(rn, Of);
rn.prototype.Ta = function() {
  var a = this.j.n();
  this.I.setAttribute("d", gd(a))
};
function sn() {
  pn.call(this)
}
A.e(sn, pn);
sn.prototype.Yc = function() {
  var a = this.j.ka();
  a.wn() && (this.Db = new qn, this.Db.p(this.j, this));
  a.wi() && (this.Fb = new rn, this.Fb.p(this.j, this))
};
sn.prototype.update = function(a, b) {
  b === j && this.hd();
  sn.f.update.call(this, a, b)
};
sn.prototype.createElement = function() {
  return this.j.r().ja()
};
sn.prototype.bg = function(a, b) {
  if(this.P.x > this.X.x) {
    var c = this.P.x;
    this.P.x = this.X.x;
    this.X.x = c
  }
  0 > this.P.y && (this.P.y = 0);
  this.P.y > this.X.y && (c = this.P.y, this.P.y = this.X.y, this.X.y = c);
  switch(b) {
    case 0:
      a.x = (this.P.x + this.X.x) / 2;
      a.y = (this.P.y + this.X.y) / 2;
      break;
    case 1:
      a.x = this.P.x;
      a.y = (this.P.y + this.X.y) / 2;
      break;
    case 5:
      a.x = this.X.x;
      a.y = (this.P.y + this.X.y) / 2;
      break;
    case 7:
      a.x = (this.P.x + this.X.x) / 2;
      a.y = this.X.y;
      break;
    case 3:
      a.x = (this.P.x + this.X.x) / 2;
      a.y = this.P.y;
      break;
    case 2:
      a.x = this.P.x;
      a.y = this.P.y;
      break;
    case 4:
      a.x = this.X.x;
      a.y = this.P.y;
      break;
    case 8:
      a.x = this.P.x;
      a.y = this.X.y;
      break;
    case 6:
      a.x = this.X.x, a.y = this.X.y
  }
};
function tn() {
}
A.e(tn, Nf);
tn.prototype.Ta = function() {
  this.I.setAttribute("d", this.$a.yh(this.j.r()))
};
function un() {
}
A.e(un, Of);
un.prototype.Ta = function() {
  this.I.setAttribute("d", this.$a.yh(this.j.r()))
};
function vn() {
  pn.call(this);
  this.ha = new O;
  this.sa = new O
}
A.e(vn, pn);
z = vn.prototype;
z.ha = p;
z.sa = p;
z.Yc = function() {
  var a = this.j.ka();
  a.wn() && (this.Db = new tn, this.Db.p(this.j, this));
  a.wi() && (this.Fb = new un, this.Fb.p(this.j, this))
};
z.hd = function() {
  vn.f.hd.call(this);
  this.vJ ? this.OD(this.Nb(), this.j.n(), this.sf, this.Rd) : this.ND(this.Nb(), this.j.n())
};
z.update = function(a, b) {
  b === j && this.hd();
  vn.f.update.call(this, a, b)
};
z.createElement = function() {
  return this.j.r().ja()
};
z.yh = function() {
  var a = S(this.sa.x, this.sa.y), a = a + U(this.P.x, this.P.y), a = a + U(this.ha.x, this.ha.y), a = a + U(this.X.x, this.X.y);
  return a + " Z"
};
z.ND = function() {
  A.xa()
};
z.OD = function() {
  A.xa()
};
z.Nb = x(q);
z.bg = function(a, b) {
  switch(b) {
    case 0:
      a.x = (this.P.x + this.ha.x + this.X.x + this.sa.x) / 4;
      a.y = (this.P.y + this.ha.y + this.X.y + this.sa.y) / 4;
      break;
    case 1:
      a.x = (this.P.x + this.sa.x) / 2;
      a.y = (this.P.y + this.ha.y + this.X.y + this.sa.y) / 4;
      break;
    case 5:
      a.x = (this.ha.x + this.X.x) / 2;
      a.y = (this.P.y + this.ha.y + this.X.y + this.sa.y) / 4;
      break;
    case 7:
      a.x = (this.P.x + this.ha.x + this.X.x + this.sa.x) / 4;
      a.y = (this.X.y + this.sa.y) / 2;
      break;
    case 3:
      a.x = (this.P.x + this.ha.x + this.X.x + this.sa.x) / 4;
      a.y = (this.P.y + this.ha.y) / 2;
      break;
    case 2:
      a.x = this.P.x;
      a.y = this.P.y;
      break;
    case 4:
      a.x = this.ha.x;
      a.y = this.ha.y;
      break;
    case 8:
      a.x = this.sa.x;
      a.y = this.sa.y;
      break;
    case 6:
      a.x = this.X.x, a.y = this.X.y
  }
};
function wn() {
  vn.call(this)
}
A.e(wn, vn);
wn.prototype.aD = function() {
  return this.P.x > this.X.x
};
wn.prototype.ND = function(a, b) {
  this.ha.x = this.X.x = b.Ja();
  this.X.y = a ? b.ra() : b.y + b.height / 2;
  this.ha.y = a ? b.rb() : b.y + b.height / 2;
  this.P.x = this.sa.x = b.tb();
  this.sa.y = !a ? b.ra() : b.y + b.height / 2;
  this.P.y = !a ? b.rb() : b.y + b.height / 2
};
wn.prototype.OD = function(a, b, c, d) {
  var f = this.j.o, f = bn(dn(this.j.Ef(), f.eb.getName(), f.Jb(), 2 == f.eb.fa().ug), this.j.Sb()), c = b.height * (f - c) / f, d = b.height * (f - d) / f;
  this.X.x = this.ha.x = b.Ja();
  this.sa.x = this.P.x = b.tb();
  this.ha.y = b.y + (!a ? (b.height - d) / 2 : (b.height - c) / 2);
  this.X.y = b.y + (!a ? (b.height + d) / 2 : (b.height + c) / 2);
  this.P.y = b.y + (a ? (b.height - d) / 2 : (b.height - c) / 2);
  this.sa.y = b.y + (a ? (b.height + d) / 2 : (b.height + c) / 2)
};
function xn() {
  vn.call(this)
}
A.e(xn, vn);
xn.prototype.aD = function() {
  return this.P.y < this.X.y
};
xn.prototype.ND = function(a, b) {
  this.ha.y = this.P.y = b.rb();
  this.X.y = this.sa.y = b.ra();
  this.ha.x = a ? b.Ja() : b.x + b.width / 2;
  this.P.x = a ? b.tb() : b.x + b.width / 2;
  this.sa.x = a ? b.x + b.width / 2 : b.tb();
  this.X.x = a ? b.x + b.width / 2 : b.Ja()
};
xn.prototype.OD = function(a, b, c, d) {
  var f = this.j.o, f = bn(dn(this.j.Ef(), f.eb.getName(), f.Jb(), 2 == f.eb.fa().ug), this.j.Sb()), c = b.width * (f - c) / f, d = b.width * (f - d) / f;
  this.sa.y = this.X.y = b.ra();
  this.P.y = this.ha.y = b.rb();
  this.X.x = b.x + (!a ? (b.width + c) / 2 : (b.width + d) / 2);
  this.sa.x = b.x + (!a ? (b.width - c) / 2 : (b.width - d) / 2);
  this.ha.x = b.x + (a ? (b.width + c) / 2 : (b.width + d) / 2);
  this.P.x = b.x + (a ? (b.width - c) / 2 : (b.width - d) / 2)
};
function yn() {
  $.call(this)
}
A.e(yn, Rf);
yn.prototype.cc = function() {
  return mn
};
function zn() {
  $.call(this)
}
A.e(zn, yn);
zn.prototype.cc = function() {
  return nn
};
function An() {
  $.call(this)
}
A.e(An, yn);
An.prototype.cc = function() {
  return on
};
yn.prototype.lc = function(a) {
  switch(a.ol()) {
    default:
    ;
    case 0:
      return new sn;
    case 2:
    ;
    case 3:
      return a.o.ca().Vf() ? new wn : new xn;
    case 1:
      return new sn
  }
};
function Bn() {
  pn.call(this)
}
A.e(Bn, sn);
Bn.prototype.createElement = function() {
  return this.j.r().ja()
};
Bn.prototype.hd = function() {
  this.j.hd()
};
Bn.prototype.update = function(a, b) {
  b === j && this.hd();
  Bn.f.update.call(this, a, b)
};
function Cn() {
  $.call(this)
}
A.e(Cn, Rf);
Cn.prototype.cc = function() {
  return mn
};
Cn.prototype.lc = function() {
  return new Bn
};
function Dn(a) {
  this.N = a;
  this.gj = q
}
Dn.prototype.N = p;
Dn.prototype.gj = p;
function En(a) {
  Dn.call(this, a)
}
A.e(En, Dn);
En.prototype.tG = p;
En.prototype.XK = p;
En.prototype.g = function(a) {
  if(this.gj = C(a, "url")) {
    this.tG = new je(J(a, "url"));
    var b = "_blank";
    C(a, "target") && (b = J(a, "target"));
    C(a, "url_target") && (b = J(a, "url_target"));
    this.XK = new je(b)
  }
};
En.prototype.execute = function(a) {
  switch(this.XK.ta(a)) {
    default:
    ;
    case "_blank":
      window.open(this.tG.ta(a));
      break;
    case "_self":
      window.location.href = this.tG.ta(a)
  }
  return q
};
function Fn(a) {
  Dn.call(this, a)
}
A.e(Fn, Dn);
Fn.prototype.kI = p;
Fn.prototype.Bw = p;
Fn.prototype.g = function(a) {
  C(a, "function") && (this.gj = j, this.kI = new je(F(a, "function")));
  if(this.gj && C(a, "arg")) {
    this.Bw = [];
    for(var a = I(a, "arg"), b = a.length, c = 0;c < b;c++) {
      this.Bw.push(new je(dc(a[c]) ? a[c] : F(a[c], "value")))
    }
  }
};
Fn.prototype.execute = function(a) {
  var b = this.kI.ta(a), c, d = this.Bw.length, f = "";
  for(c = 0;c < d;c++) {
    f += '"' + this.Bw[c].ta(a) + '"', 1 < d && c != d - 1 && (f += ", ")
  }
  eval(b + ".call(null," + f + ");");
  return q
};
function Gn(a) {
  Dn.call(this, a)
}
A.e(Gn, Dn);
Gn.prototype.Lv = p;
Gn.prototype.sj = p;
Gn.prototype.SF = p;
Gn.prototype.g = function(a) {
  if(this.gj = C(a, "source") && C(a, "source_mode")) {
    var b;
    a: {
      switch(Wb(a, "source_mode")) {
        case "externaldata":
          b = 0;
          break a;
        case "internaldata":
          b = 1;
          break a;
        default:
          b = p
      }
    }
    this.SF = b;
    this.Lv = new je(J(a, "source"));
    if(C(a, "replace")) {
      a = I(a, "replace");
      b = a.length;
      this.sj = [];
      for(var c = 0;c < b;c++) {
        var d = a[c];
        this.sj.push([J(d, "token"), new je(J(d, "value"))])
      }
    }
  }
};
function Hn(a, b) {
  if(!a.sj) {
    return p
  }
  for(var c = [], d = a.sj.length, f = 0;f < d;f++) {
    c.push([a.sj[f][0], a.sj[f][1].ta(b)])
  }
  return c
}
function In(a) {
  Dn.call(this, a)
}
A.e(In, Gn);
In.prototype.execute = function(a) {
  if(0 == this.SF) {
    this.N.Hi(this.Lv.ta(a), Hn(this, a), q)
  }else {
    var b = this.N, c = this.Lv.ta(a), a = Hn(this, a);
    b.sj = a ? a : p;
    b.$u = q;
    b.Fk(b.sq[c])
  }
  return q
};
function Jn(a) {
  Dn.call(this, a)
}
A.e(Jn, Gn);
Jn.prototype.ZK = p;
Jn.prototype.g = function(a) {
  Jn.f.g.call(this, a);
  if(this.gj = this.gj && (C(a, "view") || C(a, "view_id"))) {
    this.ZK = new je(C(a, "view_id") ? J(a, "view_id") : J(a, "view"))
  }
};
Jn.prototype.execute = function(a) {
  var b = this.ZK.ta(a);
  if(0 == this.SF) {
    this.N.Np(b, this.Lv.ta(a), Hn(this, a))
  }else {
    var c = this.N, d = this.Lv.ta(a), a = Hn(this, a), d = c.sq[d], b = Kn(c, b);
    d && b && (a && (d = lc(d, a)), b.hb.D.clear(), b.Fk(d))
  }
  return q
};
function Fe(a, b) {
  this.N = a;
  this.SC = b
}
z = Fe.prototype;
z.N = p;
z.SC = p;
z.Pf = p;
z.g = function(a) {
  this.Pf = [];
  for(var a = I(a, "action"), b = a.length, c = 0;c < b;c++) {
    var d;
    a: {
      d = a[c];
      var f = p;
      if(C(d, "type")) {
        switch(Wb(d, "type")) {
          case "navigatetourl":
            f = new En(this.N);
            break;
          case "call":
            f = new Fn(this.N);
            break;
          case "updatechart":
            f = new In(this.N);
            break;
          case "updateview":
            f = new Jn(this.N);
            break;
          case "scroll":
            Le("Action scroll");
            break;
          case "zoom":
            Le("Action zoom");
            break;
          case "scrollview":
            Le("Action scrollView");
            break;
          case "zoomView":
            Le("Action zoomView");
            break;
          default:
            d = p;
            break a
        }
        f.g(d);
        f.gj || (f = p)
      }
      d = f
    }
    d && this.Pf.push(d)
  }
};
z.execute = function(a) {
  a && (this.SC = a);
  for(var a = this.Pf.length, b = q, c = 0;c < a;c++) {
    b = b || this.Pf[c].execute(this.SC)
  }
  return b
};
function Ln() {
}
z = Ln.prototype;
z.sz = p;
z.xk = p;
z.jl = p;
z.gI = p;
z.ty = p;
z.p = function(a, b) {
  this.clear();
  this.jl = a;
  this.gI = b
};
function Mn(a, b, c) {
  a.Gb(b) || (a.sz++, a.xk.push(c))
}
z.lr = function(a) {
  if(a.length == this.xk.length) {
    return[]
  }
  this.ty = [];
  if(0 == this.sz) {
    for(var b = a.length, c = 0;c < b;c++) {
      Nn(this, a[c], 1)
    }
  }else {
    if(1 == this.sz) {
      for(var b = a.length, c = Ob(this.jl.call(a[this.xk[0]])), d = 0;d < b;d++) {
        Nn(this, a[d], c)
      }
    }else {
      On(this, a)
    }
  }
  return this.ty
};
z.clear = function() {
  this.sz = 0;
  this.xk = [];
  this.ty = []
};
function On(a, b) {
  a.xk.sort(function(a, b) {
    return a - b
  });
  var c, d = a.xk.length, f = a.xk[0], g = a.xk[d - 1];
  for(c = 0;c < d - 1;c++) {
    var k = a.xk[c], l = a.xk[c + 1];
    1 < l - k && Pn(a, b, k, l)
  }
  0 < f && Qn(a, b, f);
  g < b.length - 1 && Rn(a, b, g)
}
function Pn(a, b, c, d) {
  for(var f = Ob(a.jl.call(b[c])), g = (Ob(a.jl.call(b[d])) - f) / (d - c), k = c + 1;k < d;k++) {
    Nn(a, b[k], f + (k - c) * g)
  }
}
function Qn(a, b, c) {
  for(var d = Ob(a.jl.call(b[c])), f = Ob(a.jl.call(b[c + 1])) - d, g = c - 1;0 <= g;g--) {
    Nn(a, b[g], d - f * (c - g))
  }
}
function Rn(a, b, c) {
  for(var d = Ob(a.jl.call(b[c])), f = Ob(a.jl.call(b[c - 1])), d = f - d, g = c + 1;g < b.length;g++) {
    Nn(a, b[g], f - d * (g - c + 1))
  }
}
function Nn(a, b, c) {
  a.gI.call(b, c);
  a.ty.push(b)
}
z.Gb = function(a) {
  return isNaN(Ob(this.jl.call(a)))
};
function Sn() {
}
A.e(Sn, pg);
Sn.prototype.zu = p;
Sn.prototype.$z = t("zu");
Sn.prototype.sh = function(a) {
  return Sn.f.sh.call(this, a) || C(a, "format")
};
Sn.prototype.copy = function(a) {
  a || (a = new Sn);
  return Sn.f.copy.call(this, a)
};
function Tn() {
  ag.apply(this)
}
A.e(Tn, ag);
Tn.prototype.sh = function(a) {
  return Tn.f.sh.call(this, a) || C(a, "type")
};
Tn.prototype.copy = function(a) {
  a || (a = new Tn);
  return Tn.f.copy.call(this, a)
};
function Un() {
  tg.apply(this)
}
A.e(Un, tg);
Un.prototype.sh = function(a) {
  return Un.f.sh.call(this, a) || C(a, "format")
};
Un.prototype.copy = function(a) {
  a || (a = new Un);
  return Un.f.copy.call(this, a)
};
function Vn() {
}
z = Vn.prototype;
z.FE = p;
z.wr = j;
z.Uo = j;
z.Un = j;
function Qk(a, b) {
  a.wr = b.wr;
  a.Uo = b.Uo;
  a.Un = b.Un
}
z.hl = function(a) {
  C(a, "allow_select") && (this.wr = K(a, "allow_select"));
  C(a, "hoverable") && (this.Uo = K(a, "hoverable"));
  C(a, "use_hand_cursor") && (this.Un = K(a, "use_hand_cursor"))
};
z.K = p;
z.ka = u("K");
z.rf = t("K");
z.Ea = p;
z.Mb = u("Ea");
z.gb = p;
z.Fc = p;
z.NB = j;
z.di = p;
z.ei = p;
z.wh = p;
z.Dm = function(a, b) {
  this.vo(a, b);
  this.ek(a, b);
  this.wo(a, b);
  this.gC(a, b);
  this.hC(a, b);
  this.iC(a, b)
};
z.vo = function() {
  A.xa()
};
z.ek = function() {
  A.xa()
};
z.wo = function() {
  A.xa()
};
z.gC = function(a, b) {
  if(C(a, "extra_labels")) {
    this.di = [];
    for(var c = I(F(a, "extra_labels"), "label"), d = c.length, f = 0;f < d;f++) {
      var g = this.cb.Aq();
      g.Ue(c[f], b);
      g.$z(j);
      this.di.push(g)
    }
  }
};
z.hC = function(a, b) {
  if(C(a, "extra_markers")) {
    this.ei = [];
    for(var c = I(F(a, "extra_markers"), "marker"), d = c.length, f = 0;f < d;f++) {
      var g = this.cb.Bm();
      g.Ue(c[f], b);
      this.ei.push(g)
    }
  }
};
z.iC = function(a, b) {
  if(C(a, "extra_tooltips")) {
    this.wh = [];
    for(var c = I(F(a, "extra_tooltips"), "tooltip"), d = c.length, f = 0;f < d;f++) {
      var g = new Un;
      g.Ue(c[f], b);
      this.wh.push(g)
    }
  }
};
z.Mh = function(a) {
  a.Ea = this.Ea;
  a.gb = this.gb;
  a.Fc = this.Fc;
  a.di = this.di;
  a.ei = this.ei;
  a.wh = this.wh
};
z.g = s();
z.Qe = p;
function Pk(a, b) {
  b && (a.Qe = b)
}
function Kk(a, b) {
  b && C(b, "actions") && (a.Qe = new Fe(a.dj()), a.Qe.g(F(b, "actions")))
}
function Wn() {
  this.Ge()
}
A.e(Wn, Vn);
z = Wn.prototype;
z.$m = p;
z.au = u("$m");
z.na = p;
z.Xr = t("na");
z.Sa = p;
z.Sl = t("Sa");
z.getName = u("Sa");
z.jd = p;
z.Xa = p;
z.Di = t("Xa");
z.Ba = u("Xa");
z.Ze = p;
z.Ip = t("Ze");
z.ge = p;
z.mg = u("ge");
z.cb = p;
z.Ab = u("cb");
z.dj = function() {
  return this.cb.dj()
};
z.ca = function() {
  return this.cb.ca()
};
z.Gd = function() {
  return this.ca().Gd()
};
z.ib = p;
z.as = t("ib");
z.Kg = u("ib");
z.g = function(a, b) {
  Wn.f.g.call(this, a, b);
  C(a, "id") && (this.$m = J(a, "id"));
  if(C(a, "attributes")) {
    this.jd = {};
    for(var c = I(F(a, "attributes"), "attribute"), d = c.length, f = 0;f < d;f++) {
      var g = c[f];
      if(C(g, "name")) {
        var k;
        C(g, "custom_attribute_value") ? k = F(g, "custom_attribute_value") : C(g, "value") && (k = F(g, "value"));
        k && H(this.jd, "%" + F(g, "name"), k)
      }
    }
  }
};
function Ok(a, b) {
  C(b, "name") && (a.Sa = J(b, "name"))
}
z.Eq = function(a, b) {
  if(C(a, "style")) {
    var c = J(a, "style"), d = this.Ab().Rb();
    if(c = ef(b, d, c)) {
      this.K = this.Ab().Qb(), this.K.g(c)
    }
  }
};
z.b = p;
z.ql = function(a) {
  return Boolean(this.jd && this.jd[a])
};
z.Ge = function() {
  this.b = new ne;
  this.Cb()
};
z.Cb = function() {
  A.xa()
};
z.mb = function(a) {
  if(a == p || a == h) {
    a = {}
  }
  a.Name = this.Sa;
  var b = a, c;
  var d = this.jd;
  if(d) {
    c = {};
    for(var f in d) {
      d.hasOwnProperty(f) && (c[/^%(.*)$/.exec(f)[1]] = d[f])
    }
  }
  b.Attributes = c;
  a.Color = this.Xa.Ba().Pe();
  a.HatchType = this.Ze;
  a.MarkerType = this.ge;
  this.$m && (a.ID = this.$m);
  return a
};
function Xn() {
  this.Ge()
}
A.e(Xn, Wn);
z = Xn.prototype;
z.g = function(a, b) {
  Xn.f.g.call(this, a, b);
  this.Nr = this.na;
  C(a, "selected") && (this.Og = K(a, "selected"))
};
z.Gb = q;
z.o = p;
z.Nr = 0;
z.k = p;
z.n = u("k");
z.Cg = p;
z.kn = x(q);
z.D = p;
z.fb = u("D");
z.r = function() {
  return this.D.r()
};
z.$a = p;
z.p = function(a) {
  this.k || (this.k = new P);
  this.D = new W(a);
  if(this.nc) {
    this.ya = this.K.nc, this.Ce = "Missing"
  }else {
    if(this.wr && this.Og) {
      if(!this.ca().jm && (a = this.ca().Ek)) {
        a.Og = q, a.ya = a.ka().wa
      }
      this.ya = this.K.Zb;
      this.Ce = "SelectedNormal";
      this.ca().tj(this, q)
    }else {
      this.ya = this.K.wa, this.Ce = "Normal"
    }
  }
  this.SI();
  this.KD();
  Yn(this, this.D);
  return this.D
};
z.SI = function() {
  this.$a = this.K.p(this)
};
z.qk = s();
z.update = function() {
  this.$a.update(this.ya, j);
  this.Tn()
};
z.Wa = function() {
  this.$a.update(p, j);
  this.Kz()
};
z.ya = p;
z.Ce = p;
z.be = function(a) {
  return this.pD(a, this.Ce)
};
z.pD = function(a, b) {
  switch(b) {
    default:
    ;
    case "Normal":
      return a.wa;
    case "Hover":
      return a.dc;
    case "Pushed":
      return a.kc;
    case "SelectedNormal":
      return a.Zb;
    case "SelectedHover":
      return a.gc;
    case "Missing":
      return a.nc
  }
};
z.gE = q;
function Yn(a, b) {
  b != p && (A.G.nb(b.I, ka.Os, a.mj, q, a), A.G.nb(b.I, ka.Ns, a.Kl, q, a), A.G.nb(b.I, ka.gm, a.FN, q, a), A.G.nb(b.I, ka.cB, a.Jl, q, a), A.G.nb(b.I, ka.Yn, a.QE, q, a), a.Un && ld(b.I))
}
z.mj = function(a, b) {
  this.Uo && (this.Og ? (this.ya = this.K.gc, this.Ce = "SelectedHover") : (this.ya = this.K.dc, this.Ce = "Hover"), this.update());
  this.sG(a);
  this.gE = j;
  b != j && Zn(this, a, "pointMouseOver")
};
z.Kl = function(a, b) {
  this.gE = q;
  this.Uo && (this.Og ? (this.ya = this.K.Zb, this.Ce = "SelectedNormal") : (this.ya = this.K.wa, this.Ce = "Normal"), this.update());
  this.my();
  b != j && Zn(this, a, "pointMouseOut")
};
z.QE = function(a) {
  this.gE && this.IE(a)
};
z.FN = function(a) {
  var b = q;
  this.Qe && (b = this.Qe.execute(this));
  !b && this.wr && !this.Og && (this.ya = this.K.kc, this.Ce = "Pushed", this.update(), this.Tn());
  Zn(this, a, "pointMouseDown")
};
z.Jl = function(a, b) {
  this.wr && (this.ca().jm ? (this.Og = !this.Og, this.Uo ? this.mj(a, j) : (this.Og ? (this.ya = this.K.Zb, this.Ce = "SelectedNormal") : (this.ya = this.K.gc, this.Ce = "SelectedHover"), this.update(), this.Tn()), this.Og ? Zn(this, a, "pointSelect") : Zn(this, a, "pointDeselect"), this.ca().tj(this, j)) : this.Og || (this.Og = j, this.ca().tj(this, j), Zn(this, a, "pointSelect"), this.Uo ? this.mj(a, j) : (this.ya = this.K.Zb, this.Ce = "SelectedNormal")));
  this.update();
  this.Tn();
  b || (Zn(this, a, "pointMouseUp"), Zn(this, a, "pointClick"))
};
function Zn(a, b, c) {
  var d = NaN, f = NaN;
  b && (d = b.clientX, f = b.clientY);
  a.Ab().dj().dispatchEvent(new Ie(c, a, d, f))
}
z.Og = q;
z.tx = function(a) {
  this.Og = q;
  this.ya = this.K.wa;
  this.Ce = "Normal";
  a != h && a && this.update();
  Zn(this, p, "pointDeselect")
};
z.select = function(a, b) {
  this.Og = j;
  this.ya = this.K.Zb;
  this.Ce = "SelectedNormal";
  a != h && a && this.update();
  b || Zn(this, p, "pointSelect")
};
z.ah = function() {
  this.NB = q;
  this.mj(p, j);
  this.NB = j
};
z.dh = function() {
  this.Kl(p, j)
};
z.vo = function(a, b) {
  this.Ea && (C(a, "label") && this.Ea.pm(F(a, "label")) && (this.Ea = this.Ea.copy(), this.Ea.Yi(F(a, "label"), b)), this.Vj(this.Ea))
};
z.ek = function(a, b) {
  this.gb && (C(a, "marker") && this.gb.pm(F(a, "marker")) && (this.gb = this.gb.copy(), this.gb.Yi(F(a, "marker"), b)), this.Vj(this.gb))
};
z.wo = function(a, b) {
  this.Fc && this.Fc.pm(F(a, "tooltip")) && (this.Fc = this.Fc.copy(), this.Fc.Yi(F(a, "tooltip"), b));
  this.Vj(this.Fc)
};
z.gC = function(a, b) {
  Xn.f.gC.call(this, a, b);
  if(this.di) {
    for(var c = 0;c < this.di.length;c++) {
      this.Vj(this.di[c])
    }
  }
};
z.hC = function(a, b) {
  Xn.f.hC.call(this, a, b);
  if(this.ei) {
    for(var c = 0;c < this.ei.length;c++) {
      this.Vj(this.ei[c])
    }
  }
};
z.iC = function(a, b) {
  Xn.f.iC.call(this, a, b);
  if(this.wh) {
    for(var c = 0;c < this.wh.length;c++) {
      this.Vj(this.wh[c])
    }
  }
};
z.vc = p;
z.sd = p;
z.uf = p;
z.Km = p;
z.Lm = p;
z.gi = p;
z.KD = function() {
  this.vc = $n(this, this.Ea, j);
  if(this.di) {
    this.Km = [];
    for(var a = this.di.length, b = 0;b < a;b++) {
      this.Km[b] = $n(this, this.di[b], j)
    }
  }
  this.sd = $n(this, this.gb, j);
  if(this.ei) {
    this.Lm = [];
    a = this.ei.length;
    for(b = 0;b < a;b++) {
      this.Lm[b] = $n(this, this.ei[b], j)
    }
  }
  this.uf = $n(this, this.Fc, q);
  if(this.wh) {
    this.gi = [];
    a = this.wh.length;
    for(b = 0;b < a;b++) {
      this.gi[b] = $n(this, this.wh[b], q)
    }
  }
};
function $n(a, b, c) {
  return b ? ((b = b.ka().p(a, b)) && c && !a.nc && Yn(a, b), b) : p
}
z.Tn = function() {
  this.Ni(this.Ea, this.vc);
  if(this.Km) {
    for(var a = this.Km.length, b = 0;b < a;b++) {
      this.Ni(this.di[b], this.Km[b])
    }
  }
  this.Ni(this.gb, this.sd);
  if(this.Lm) {
    a = this.Lm.length;
    for(b = 0;b < a;b++) {
      this.Ni(this.ei[b], this.Lm[b])
    }
  }
};
z.sG = function(a) {
  if(this.NB) {
    var b = this.Fc, c = this.uf;
    this.Ni(b, c);
    ao(this, a, b, c);
    if(this.gi) {
      b = this.gi.length;
      for(c = 0;c < b;c++) {
        this.Ni(this.wh[c], this.gi[c]), ao(this, a, this.wh[c], this.gi[c])
      }
    }
  }
};
z.my = function() {
  bo(this.uf);
  if(this.gi) {
    for(var a = this.gi.length, b = 0;b < a;b++) {
      bo(this.gi[b])
    }
  }
};
z.IE = function(a) {
  ao(this, a, this.Fc, this.uf);
  if(this.gi) {
    for(var b = this.gi.length, c = 0;c < b;c++) {
      ao(this, a, this.wh[c], this.gi[c])
    }
  }
};
z.Ni = function(a, b) {
  b && b.update(this.be(a.ka()))
};
function ao(a, b, c, d) {
  d && ug(c, b, a, a.be(c.ka()), d)
}
function bo(a) {
  a && (a.qb(0, 0), a.Gi(q))
}
z.Kz = function() {
  this.Mz(this.Ea, this.vc);
  if(this.Km) {
    for(var a = this.Km.length, b = 0;b < a;b++) {
      this.Mz(this.di[b], this.Km[b])
    }
  }
  this.sd && this.Dp(this.gb, this.sd);
  if(this.Lm) {
    a = this.Lm.length;
    for(b = 0;b < a;b++) {
      this.Dp(this.ei[b], this.Lm[b])
    }
  }
  this.my()
};
z.Mz = function(a, b) {
  b && this.Dp(a, b)
};
z.Dp = function(a, b) {
  a && b && a.Wa(this, this.be(a.ka()), b)
};
z.Vj = s();
z.Wq = function(a, b, c) {
  if(!a || !b || !c || this.Gb && !this.ca().pk) {
    return p
  }
  a = new O;
  this.bg(a, b.Df(), c, b.ga());
  10 != b.Df() && (this.Ur(a, b.Df(), b.ll(), b.pl(), c, b.ga()), this.Vr(a, b.Df(), b.ll(), b.pl(), c, b.ga()));
  return a
};
z.bg = function(a, b) {
  if(this.k != p) {
    switch(b) {
      default:
      ;
      case 0:
        a.x = this.k.x + this.k.width / 2;
        a.y = this.k.y + this.k.height / 2;
        break;
      case 7:
        a.x = this.k.x + this.k.width / 2;
        a.y = this.k.y + this.k.height;
        break;
      case 1:
        a.x = this.k.x;
        a.y = this.k.y + this.k.height / 2;
        break;
      case 5:
        a.x = this.k.x + this.k.width;
        a.y = this.k.y + this.k.height / 2;
        break;
      case 3:
        a.x = this.k.x + this.k.width / 2;
        a.y = this.k.y;
        break;
      case 8:
        a.x = this.k.x;
        a.y = this.k.y + this.k.height;
        break;
      case 2:
        a.x = this.k.x;
        a.y = this.k.y;
        break;
      case 6:
        a.x = this.k.x + this.k.width;
        a.y = this.k.y + this.k.height;
        break;
      case 4:
        a.x = this.k.x + this.k.width, a.y = this.k.y
    }
  }
};
z.Ur = function(a, b, c, d, f, g) {
  switch(c) {
    case 0:
      a.x -= f.width + g;
      break;
    case 1:
      a.x += g;
      break;
    default:
    ;
    case 2:
      a.x -= f.width / 2
  }
};
z.Vr = function(a, b, c, d, f, g) {
  switch(d) {
    case 0:
      a.y -= f.height + g;
      break;
    case 2:
      a.y += g;
      break;
    default:
    ;
    case 1:
      a.y -= f.height / 2
  }
};
z.Cb = function() {
  this.b.add("%Index", 0);
  this.b.add("%Name", "");
  this.b.add("%Color", "0xFFFFFF")
};
z.Se = function() {
  this.b.add("%Index", this.na);
  this.b.add("%Name", this.Sa);
  this.Xa && this.b.add("%Color", qe(this.Xa.Ba()))
};
z.Na = function(a) {
  return this.ql(a) ? this.jd[a] : 0 == a.indexOf("%Series") || this.o.ql(a) ? this.o.Uf(a) : se(a, this.ca()) ? this.ca().Na(a) : this.Cg && (0 == a.indexOf("%Threshold") || this.Cg.pr(a)) ? "%ConditionValue1" == a ? this.Cg.cE ? this.Na(fk(this.Cg.ew)) : this.Cg.ew : "%ConditionValue2" == a ? this.Cg.dE ? this.Na(fk(this.Cg.fw)) : this.Cg.fw : "%ConditionValue3" == a ? this.Cg.eE ? this.Na(fk(this.Cg.gw)) : this.Cg.gw : this.Cg.Na(a) : te(a, this.ib) ? this.ib.Na(a) : this.b.get(a)
};
z.Pa = function(a) {
  if(this.ql(a)) {
    return 1
  }
  if(0 == a.indexOf("%Series") || this.o.ql(a)) {
    return this.o.Pa(a)
  }
  if(se(a, this.ca())) {
    return this.ca().Pa(a)
  }
  if(te(a, this.ib)) {
    return this.ib.Pa(a)
  }
  switch(a) {
    case "%Index":
      return 2;
    case "%Name":
    ;
    case "%Color":
    ;
    case "%text":
      return 1;
    default:
      return 4
  }
};
z.mb = function(a) {
  a = Xn.f.mb.call(this, a);
  a.SeriesPointCount = this.o.Fe();
  a.Index = this.na;
  return a
};
function co(a) {
  var b = a.mb();
  b.Series = a.o.mb();
  return b
}
function eo() {
  this.Ge();
  this.ba = []
}
A.e(eo, Wn);
z = eo.prototype;
z.ba = p;
z.Fe = function() {
  return this.ba ? this.ba.length : 0
};
z.Ga = function(a) {
  return this.ba[a]
};
z.qe = function() {
  A.xa()
};
z.Pd = function(a, b) {
  this.ba[b] = a
};
z.Ra = NaN;
z.Jb = u("Ra");
z.gh = t("Ra");
z.vo = function(a, b) {
  this.Ea && this.Ea.sh(F(a, "label")) && (this.Ea = this.Ea.copy(), this.Ea.Zi(F(a, "label"), b))
};
z.ek = function(a, b) {
  this.gb && this.gb.sh(F(a, "marker")) && (this.gb = this.gb.copy(), this.gb.Zi(F(a, "marker"), b))
};
z.wo = function(a, b) {
  this.Fc && this.Fc.sh(F(a, "tooltip")) && (this.Fc = this.Fc.copy(), this.Fc.Zi(F(a, "tooltip"), b))
};
z.g = function(a, b) {
  eo.f.g.call(this, a, b)
};
z.ah = function() {
  for(var a = this.Fe(), b = 0;b < a;b++) {
    this.Ga(b).ah()
  }
};
z.dh = function() {
  for(var a = this.Fe(), b = 0;b < a;b++) {
    this.Ga(b).dh()
  }
};
z.Cb = function() {
  this.b.add("%SeriesPointCount", 0);
  this.b.add("%Name", "");
  this.b.add("%SeriesName", "")
};
z.Pa = function(a) {
  if(this.ql(a)) {
    return 1
  }
  if(se(a, this.ca())) {
    return this.ca().Pa(a)
  }
  switch(a) {
    default:
      return 1;
    case "%SeriesPointCount":
      return 2
  }
};
z.Ed = function(a) {
  a.Se()
};
z.th = function(a) {
  pe(a.b, "%DataPlotPointCount", this.ba.length);
  this.rh()
};
z.rh = function() {
  this.b.add("%SeriesName", this.Sa);
  this.b.add("%Name", this.Sa);
  this.b.add("%SeriesPointCount", this.ba.length)
};
z.Uf = function(a) {
  return this.jd && this.jd[a] ? this.jd[a] : this.b.get(a)
};
z.Na = function(a) {
  return se(a, this.ca()) ? this.ca().Na(a) : this.Uf(a)
};
z.un = x(q);
z.Ug = s();
z.An = s();
z.jB = x(j);
z.mb = function(a) {
  a = eo.f.mb.call(this, a);
  a.Points = [];
  for(var b = this.ba.length, c = 0;c < b;c++) {
    this.ba[c] && a.Points.push(this.ba[c].mb())
  }
  return a
};
function fo() {
  this.cb = this
}
A.e(fo, Vn);
fo.prototype.aa = p;
fo.prototype.Ul = t("aa");
fo.prototype.ca = u("aa");
Vn.prototype.N = p;
Vn.prototype.cA = t("N");
Vn.prototype.dj = u("N");
z = fo.prototype;
z.oy = x(q);
z.Aw = q;
z.Xm = 0.1;
z.Dn = 0.1;
z.Rb = function() {
  A.xa()
};
z.Qb = function() {
  A.xa()
};
z.g = function(a, b) {
  fo.f.g.call(this, a, b);
  C(a, "apply_palettes_to") && (this.Aw = "points" == Cb(F(a, "apply_palettes_to")));
  C(a, "group_padding") && (this.Xm = M(a, "group_padding"));
  C(a, "point_padding") && (this.Dn = M(a, "point_padding"));
  if((this.bt() || this.Sw()) && C(a, "sort")) {
    switch(Wb(a, "sort")) {
      case "asc":
        this.Xo = j;
        this.Kv = q;
        break;
      case "desc":
        this.Kv = this.Xo = j;
        break;
      default:
      ;
      case "none":
        this.Xo = q
    }
  }
};
z.Eq = function(a, b) {
  var c = this.Rb();
  if(c) {
    var d, f;
    C(a, "style") && (d = N(a, "style"));
    C(a, c) && (f = F(a, c), C(f, "parent") && (d = F(f, "parent")));
    c = Ve(b, c, f, d);
    c.name = d;
    this.K = this.Qb(c);
    this.K.g(c)
  }
};
z.hl = function(a) {
  fo.f.hl.call(this, a);
  C(a, "interactivity") && fo.f.hl.call(this, F(a, "interactivity"))
};
z.Kv = q;
z.Xo = q;
z.bt = x(q);
z.Sw = x(q);
function Nk(a, b) {
  !a.Sw() && a.Xo && a.bt() && a.FC(b, a.Kv)
}
z.FC = s();
z.bI = x(p);
z.Aq = function() {
  return new Sn
};
z.Bm = function() {
  return new Tn
};
z.vo = function(a, b) {
  (this.Ea = this.Aq()) && (a && C(a, "label_settings") ? this.Ea.Ue(F(a, "label_settings"), b) : this.Ea.rf(new $))
};
z.ek = function(a, b) {
  (this.gb = this.Bm()) && (a && C(a, "marker_settings") ? this.gb.Ue(F(a, "marker_settings"), b) : this.gb.rf(new $))
};
z.wo = function(a, b) {
  (this.Fc = new Un) && (a && C(a, "tooltip_settings") ? this.Fc.Ue(F(a, "tooltip_settings"), b) : this.Fc.rf(new $))
};
z.ZG = x(j);
function go() {
  fo.apply(this)
}
A.e(go, fo);
go.prototype.nw = p;
go.prototype.MD = function() {
  this.nw = new Ln;
  this.nw.p(ho.prototype.Sb, ho.prototype.gd)
};
go.prototype.cH = function(a) {
  Mn(this.nw, a, a.na)
};
fo.prototype.XI = function(a) {
  for(var a = this.nw.lr(a.ba), b = a.length, c = 0;c < b;c++) {
    a[c].qk()
  }
  this.nw.clear()
};
function io() {
  go.apply(this)
}
A.e(io, go);
io.prototype.Pr = 0.05;
io.prototype.mE = j;
io.prototype.g = function(a, b) {
  io.f.g.call(this, a, b);
  C(a, "scatter_point_width") && (this.Pr = F(a, "scatter_point_width"), this.Pr = (this.mE = Mb(this.Pr)) ? Nb(this.Pr) : Ib.MQ(this.Pr))
};
function jo() {
  Xn.apply(this)
}
A.e(jo, Xn);
z = jo.prototype;
z.Vb = NaN;
z.Hd = u("Vb");
z.Dc = p;
z.Ef = u("Dc");
z.Fd = p;
z.qo = 0;
z.zf = p;
z.Ic = q;
z.xl = q;
z.g = function(a, b) {
  this.cl(a);
  jo.f.g.call(this, a);
  this.GH(a, b);
  this.Gb || this.$h(a);
  !this.Gb && this.Fd && (this.Fd.Ed(this), this.Fd != this.Dc && this.Dc.Ed(this));
  this.o.Wb.ba.push(this);
  this.o.eb.ba.push(this)
};
z.GH = function(a, b) {
  var c = this.o.Wb;
  C(a, "x") && (this.Vb = c.$i(F(a, "x")));
  this.Ic = 0 == c.Ib() || 1 == c.Ib();
  var d = c.Jb();
  if(1 == d) {
    C(a, "name") && isNaN(this.Vb) && (this.Vb = c.$i(F(a, "name"))), this.o.Wb.Gc(this.Vb)
  }else {
    if(2 == d) {
      this.Fd = this.Dc = c.Ef(this.Sa, b), this.Vb = this.Dc.na, this.zf = this.o.zf, this.na = this.Nr = this.Dc.na
    }else {
      if(3 == d) {
        this.Dc = this.o.Ef();
        var d = this.cb.ca(), f = this.Sa;
        if(d.qx[f]) {
          c = d.qx[f]
        }else {
          var g = new Zm(d);
          g.vj(c);
          g.Xr(d.Bq.length);
          g.Sl(f);
          d.Bq.push(g);
          c = d.qx[f] = g
        }
        this.Fd = c;
        this.Vb = this.o.Ef().na;
        this.o.tJ ? this.qo = 0 : (c = this.Ef().zf, d = this.Ef().zf, c.Il = d.Il + 1, this.qo = this.na);
        this.zf = this.Ef().zf
      }else {
        e(Error("Unknown argument axis type"))
      }
    }
  }
};
z.$h = function() {
  A.xa()
};
function ko(a, b) {
  return a.o.eb.$i(b)
}
z.cl = function() {
  A.xa()
};
z.qk = function() {
  jo.f.qk.call(this);
  this.Fd && this.Fd.Ed(this);
  this.Fd != p && this.Dc != p && this.Fd != this.Dc && this.Dc.Ed(this)
};
z.bg = function(a, b, c, d) {
  var f = this.o.Wb;
  10 == b ? (f.transform(this, this.Hd(), a), f.J.Ei(d, c, a)) : (b = this.xl ? f.J.vi.Ox(b) : f.J.vi.Yx(b), jo.f.bg.call(this, a, b, c, d))
};
z.Ur = function(a, b, c, d, f, g) {
  if(9 != b) {
    var k = this.o.Wb, c = this.xl ? k.J.vi.bD(c, d) : k.J.vi.fD(c, d)
  }
  jo.f.Ur.call(this, a, b, c, d, f, g)
};
z.Vr = function(a, b, c, d, f, g) {
  if(9 != b) {
    var k = this.o.Wb, d = this.xl ? k.J.vi.cD(c, d) : k.J.vi.Zx(c, d)
  }
  jo.f.Vr.call(this, a, b, c, d, f, g)
};
z.Vj = function(a) {
  if(10 == a.ka().wa.Df()) {
    var b = this.o.Wb;
    b.xp || (b.xp = []);
    b.xp.push([a, this])
  }
};
z.Mz = function(a, b) {
  b && a && a.ka() && a.ka().wa && 10 == a.ka().wa.Df() && this.ca().Vx(a).ia(b);
  jo.f.Mz.call(this, a, b)
};
z.mt = s();
z.Cb = function() {
  jo.f.Cb.call(this);
  this.b.add("%XValue", 0);
  this.b.add("%XPercentOfSeries", 0);
  this.b.add("%XPercentOfTotal", 0)
};
z.Pa = function(a) {
  if(ue(a) && this.Fd) {
    return this.Fd.Pa(a)
  }
  if(0 == a.indexOf("%YAxis")) {
    return this.o.eb.Pa(ve(a))
  }
  if(0 == a.indexOf("%XAxis")) {
    return this.o.Wb.Pa(ve(a))
  }
  if(Lm(this.o.Wb) && ("%XValue" == a || "%Name" == a)) {
    return 3
  }
  switch(a) {
    case "%XValue":
    ;
    case "%XPercentOfSeries":
    ;
    case "%XPercentOfTotal":
      return 2
  }
  return jo.f.Pa.call(this, a)
};
z.Se = function() {
  jo.f.Se.call(this);
  this.Vb && this.b.add("%XValue", this.Vb)
};
z.Na = function(a) {
  oe(this.b, "%XPercentOfSeries") || this.b.add("%XPercentOfSeries", 100 * (this.Vb / this.o.Uf("%SeriesXSum")));
  oe(this.b, "%XPercentOfTotal") || this.b.add("%XPercentOfTotal", 100 * (this.Vb / this.ca().Na("%DataPlotXSum")));
  return ue(a) && this.Fd ? this.Fd.Na(a) : 0 == a.indexOf("%YAxis") ? this.o.eb.Na(ve(a)) : 0 == a.indexOf("%XAxis") ? this.o.Wb.Na(ve(a)) : jo.f.Na.call(this, a)
};
z.mb = function(a) {
  a = jo.f.mb.call(this, a);
  a.XValue = this.Vb;
  a.XPercentOfSeries = this.b.get("%XPercentOfSeries");
  a.XPercentOfTotal = this.b.get("%XPercentOfTotal");
  a.XPercentOfCategory = this.b.get("%XPercentOfCategory");
  a.Category = this.Fd ? this.Fd.getName() : p;
  return a
};
function lo() {
  eo.call(this)
}
A.e(lo, eo);
z = lo.prototype;
z.Wb = p;
z.eb = p;
z.Dc = p;
z.Ef = u("Dc");
z.qo = p;
z.tJ = q;
z.Wj = NaN;
z.fn = x(j);
z.eH = x(q);
z.zf = p;
z.wF = t("zf");
z.Cb = function() {
  lo.f.Cb.call(this);
  this.b.add("%SeriesFirstXValue", 0);
  this.b.add("%SeriesLastXValue", 0);
  this.b.add("%SeriesXSum", 0);
  this.b.add("%SeriesXMax", -Number.MAX_VALUE);
  this.b.add("%SeriesXMin", Number.MAX_VALUE);
  this.b.add("%SeriesXAverage", 0);
  this.b.add("%SeriesYAxisName", "");
  this.b.add("%SeriesXAxisName", "")
};
z.Pa = function(a) {
  switch(a) {
    case "%SeriesFirstXValue":
    ;
    case "%SeriesLastXValue":
    ;
    case "%SeriesXSum":
    ;
    case "%SeriesXMin":
    ;
    case "%SeriesXMax":
    ;
    case "%SeriesXAverage":
      return Lm(this.Wb) ? 3 : 2;
    case "%SeriesYAxisName":
    ;
    case "%SeriesXAxisName":
      return 1
  }
  return lo.f.Pa.call(this, a)
};
z.th = function(a) {
  lo.f.th.call(this, a);
  a = a.b;
  pe(a, "%DataPlotXSum", this.b.get("%SeriesXSum"));
  this.b.get("%SeriesXMax") > a.get("%DataPlotXMax") && a.add("%DataPlotXMax", this.b.get("%SeriesXMax"));
  this.b.get("%SeriesXMin") < a.get("%DataPlotXMin") && a.add("%DataPlotXMin", this.b.get("%SeriesXMin"));
  this.kt()
};
z.Ed = function(a) {
  lo.f.Ed.call(this, a);
  var a = a.Hd(), b = Number(this.b.get("%SeriesXMax")), c = Number(this.b.get("%SeriesXMin"));
  pe(this.b, "%SeriesXSum", a);
  a > b && this.b.add("%SeriesXMax", a);
  a < c && this.b.add("%SeriesXMin", a)
};
z.kt = function() {
  var a = this.Wb.b;
  pe(a, "%AxisPointCount", this.ba.length);
  pe(a, "%AxisSum", this.b.get("%SeriesXSum"));
  this.b.get("%SeriesXMax") > a.get("%AxisMax") && a.add("%AxisMax", this.b.get("%SeriesXMax"));
  this.b.get("%SeriesXMin") < a.get("%AxisMin") && a.add("%AxisMin", this.b.get("%SeriesXMin"));
  a = this.Wb;
  a.b.add("%AxisName", a.getName())
};
z.rh = function() {
  lo.f.rh.call(this);
  this.b.add("%SeriesYAxisName", this.eb.getName());
  this.b.add("%SeriesXAxisName", this.Wb.getName());
  0 < this.ba.length && this.Ga(0) && this.b.add("%SeriesFirstXValue", this.Ga(0).Hd());
  0 < this.ba.length && this.Ga(this.ba.length - 1) && this.b.add("%SeriesLastXValue", this.Ga(this.ba.length - 1).Hd());
  this.b.add("%SeriesXAverage", this.b.get("%SeriesXSum") / this.ba.length)
};
z.mb = function(a) {
  a = lo.f.mb.call(this, a);
  a.StartMarkerType = a.EndMarkerType = this.ge;
  a.FirstXValue = this.b.get("%SeriesFirstXValue");
  a.LastXValue = this.b.get("%SeriesLastXValue");
  a.XSum = this.b.get("%SeriesXSum");
  a.XMax = this.b.get("%SeriesXMax");
  a.XMin = this.b.get("%SeriesXMin");
  a.XAverage = this.b.get("%SeriesXAverage");
  a.XMedian = this.b.get("%SeriesXMedian");
  a.XMode = this.b.get("%SeriesXMode");
  this.eb && (a.YAxis = this.eb.getName());
  this.Wb && (a.XAxis = this.Wb.getName());
  return a
};
function mo() {
  lo.apply(this)
}
A.e(mo, lo);
z = mo.prototype;
z.Cb = function() {
  mo.f.Cb.call(this);
  this.b.add("%SeriesLastYValue", 0);
  this.b.add("%SeriesYSum", 0);
  this.b.add("%Value", 0);
  this.b.add("%SeriesValue", 0);
  this.b.add("%SeriesYMax", -Number.MAX_VALUE);
  this.b.add("%SeriesYMin", Number.MAX_VALUE);
  this.b.add("%SeriesYAverage", 0);
  this.b.add("%MaxYValuePointName", "");
  this.b.add("%MinYValuePointName", "")
};
z.Pa = function(a) {
  if(se(a, this.ca())) {
    return this.ca().Pa(a)
  }
  switch(a) {
    case "%SeriesLastYValue":
    ;
    case "%SeriesFirstYValue":
    ;
    case "%SeriesYSum":
    ;
    case "%Value":
    ;
    case "%SeriesValue":
    ;
    case "%SeriesYMax":
    ;
    case "%SeriesYMin":
    ;
    case "%SeriesYAverage":
      return Lm(this.eb) ? 3 : 2;
    case "%MaxYValuePointName":
    ;
    case "%MinYValuePointName":
      return 1
  }
  return mo.f.Pa.call(this, a)
};
z.th = function(a) {
  mo.f.th.call(this, a);
  a = a.b;
  pe(a, "%DataPlotYSum", this.b.get("%SeriesYSum"));
  pe(a, "%YBasedPointsCount", this.ba.length);
  this.b.get("%SeriesYMax") > a.get("%DataPlotYMax") && (a.add("%DataPlotYMax", this.b.get("%SeriesYMax")), a.add("%DataPlotMaxYValuePointName", this.b.get("%SeriesMaxYValuePointName")), a.add("%DataPlotMaxYValuePointSeriesName", this.Sa));
  this.b.get("%SeriesYMin") < a.get("%DataPlotYMin") && (a.add("%DataPlotYMin", this.b.get("%SeriesYMin")), a.add("%DataPlotMinYValuePointName", this.b.get("%SeriesMinYValuePointName")), a.add("%DataPlotMinYValuePointSeriesName", this.Sa));
  this.b.get("%SeriesYSum") > a.get("%DataPlotMaxYSumSeries") && (a.add("%DataPlotMaxYSumSeries", this.b.get("%SeriesYSum")), a.add("%DataPlotMaxYSumSeriesName", this.Sa));
  this.b.get("%SeriesYSum") > a.get("%DataPlotMinYSumSeries") && (a.add("%DataPlotMinYSumSeries", this.b.get("%SeriesYSum")), a.add("%DataPlotMinYSumSeriesName", this.Sa))
};
z.Ed = function(a) {
  mo.f.Ed.call(this, a);
  pe(this.b, "%SeriesYSum", a.Sb());
  pe(this.b, "%Value", a.Sb());
  pe(this.b, "%SeriesValue", a.Sb());
  a.Sb() > this.b.get("%SeriesYMax") && (this.b.add("%SeriesYMax", a.Sb()), this.b.add("%SeriesMaxYValuePointName", a.getName()));
  a.Sb() < this.b.get("%SeriesYMin") && (this.b.add("%SeriesYMin", a.Sb()), this.b.add("%SeriesMinYValuePointName", a.getName()))
};
z.kt = function() {
  mo.f.kt.call(this);
  var a = this.eb.b;
  pe(a, "%AxisPointCount", this.ba.length);
  pe(a, "%AxisSum", this.b.get("%SeriesYSum"));
  this.b.get("%SeriesYMax") > a.get("%AxisMax") && a.add("%AxisMax", this.b.get("%SeriesYMax"));
  this.b.get("%SeriesYMin") < a.get("%AxisMin") && a.add("%AxisMin", this.b.get("%SeriesYMin"));
  a = this.eb;
  a.b.add("%AxisName", a.getName())
};
z.rh = function() {
  mo.f.rh.call(this);
  this.b.add("%SeriesYAverage", this.b.get("%SeriesYSum") / this.ba.length);
  this.ba && this.Ga(0) && this.b.add("%SeriesFirstYValue", this.Ga(0).Sb());
  this.ba && this.Ga(this.ba.length - 1) && this.b.add("%SeriesLastYValue", this.Ga(this.ba.length - 1).Sb())
};
z.mb = function(a) {
  a = mo.f.mb.call(this, a);
  a.FirstYValue = this.b.get("%SeriesFirstYValue");
  a.LastYValue = this.b.get("%SeriesLastYValue");
  a.YSum = this.b.get("%SeriesYSum");
  a.YMax = this.b.get("%SeriesYMax");
  a.YMin = this.b.get("%SeriesYMin");
  a.YAverage = this.b.get("%SeriesYAverage");
  a.YMedian = this.b.get("%SeriesYMedian");
  a.YMode = this.b.get("%SeriesYMode");
  return a
};
function no() {
  jo.apply(this)
}
A.e(no, jo);
z = no.prototype;
z.ma = p;
z.Sb = u("ma");
z.cl = function(a) {
  C(a, "y") ? 1 == this.o.eb.Jb() && (this.Gb = isNaN(ko(this, F(a, "y")))) : this.Gb = j
};
z.$h = function(a) {
  1 == this.o.eb.Jb() ? (this.ma = ko(this, F(a, "y")), (this.Gb = isNaN(this.ma)) || this.o.eb.Gc(this.ma)) : (this.mw = this.eb.Ef(J(a, "y")), this.y = this.mw.index, this.Gb || this.mw.Ed(this));
  no.f.$h.call(this, a)
};
z.qk = function() {
  this.o.eb.Gc(this.ma);
  no.f.qk.call(this)
};
z.Cb = function() {
  no.f.Cb.call(this);
  this.b.add("%Value", 0);
  this.b.add("%YValue", 0)
};
z.Pa = function(a) {
  switch(a) {
    case "%Value":
    ;
    case "%YValue":
      return Lm(this.o.eb) ? 3 : 2;
    case "%YPercentOfSeries":
    ;
    case "%YPercentOfTotal":
    ;
    case "%YPercentOfCategory":
      return 2
  }
  return no.f.Pa.call(this, a)
};
z.mt = function(a) {
  a = a.b;
  pe(a, "%CategoryYSum", this.ma);
  pe(a, "%CategoryPointCount", 1);
  this.ma > a.get("%CategoryYMax") && a.add("%CategoryYMax", this.ma);
  this.ma < a.get("%CategoryYMin") && a.add("%CategoryYMin", this.ma)
};
z.Se = function() {
  no.f.Se.call(this);
  this.ma && this.b.add("%YValue", this.ma);
  this.ma && this.b.add("%Value", this.ma)
};
z.Na = function(a) {
  oe(this.b, "%YPercentOfSeries") || this.b.add("%YPercentOfSeries", 100 * (this.Sb() / this.o.Uf("%SeriesYSum")));
  oe(this.b, "%YPercentOfTotal") || this.b.add("%YPercentOfTotal", 100 * (this.Sb() / this.ca().Na("%DataPlotYSum")));
  !oe(this.b, "%YPercentOfCategory") && this.Fd && this.b.add("%YPercentOfCategory", 100 * (this.Sb() / this.Fd.Na("%CategoryYSum")));
  return no.f.Na.call(this, a)
};
z.mb = function(a) {
  a = no.f.mb.call(this, a);
  a.YValue = this.ma;
  a.YPercentOfSeries = this.b.get("%YPercentOfSeries");
  a.YPercentOfTotal = this.b.get("%YPercentOfTotal");
  a.YPercentOfCategory = this.b.get("%YPercentOfCategory");
  return a
};
function oo() {
  jo.apply(this)
}
A.e(oo, no);
z = oo.prototype;
z.gf = p;
z.hc = NaN;
z.$h = function(a) {
  var b = this.o.eb;
  if(1 == b.Jb()) {
    this.ma = ko(this, F(a, "y"));
    if(this.Dc != p) {
      switch(b.fa().ml()) {
        case 1:
          this.hc = dn(this.Dc, b.getName(), this.o.Jb(), q).Pd(this, this.ma);
          b.Gc(this.hc);
          break;
        case 2:
          this.hc = dn(this.Dc, b.getName(), this.o.Jb(), j).Pd(this, this.ma);
          break;
        default:
          this.hc = this.ma
      }
    }else {
      this.hc = this.ma, b.Gc(this.hc)
    }
    this.Gb = isNaN(this.ma);
    this.xl = b.fa().Nb() && 0 <= this.ma || !b.fa().Nb() && 0 > this.ma;
    this.Gb || b.Gc(this.hc)
  }else {
    oo.f.$h.call(this, a)
  }
};
z.qk = function() {
  var a = this.o.eb;
  if(this.Dc != p) {
    switch(a.fa().ml()) {
      case 1:
        this.hc = dn(this.Dc, a.getName(), this.o.Jb(), q).Pd(this, this.ma);
        a.Gc(this.hc);
        break;
      case 2:
        this.hc = dn(this.Dc, a.getName(), this.o.Jb(), j).Pd(this, this.ma);
        break;
      default:
        this.hc = this.ma
    }
  }else {
    this.hc = this.ma, a.Gc(this.hc)
  }
  this.xl = a.fa().Nb() && 0 <= this.ma || !a.fa().Nb() && 0 > this.ma;
  oo.f.qk.call(this, this)
};
z.FF = function(a) {
  this.hc *= 100 / a
};
function po() {
  jo.apply(this)
}
A.e(po, oo);
po.prototype.Ag = NaN;
po.prototype.$h = function(a) {
  var b = this.o.eb;
  if(1 == b.Jb()) {
    this.ma = ko(this, F(a, "y"));
    if(this.Dc != p) {
      switch(b.fa().ml()) {
        case 1:
          a = dn(this.Dc, b.getName(), this.o.Jb(), q);
          this.Ag = 0 > this.ma ? a.Hl : a.Ol;
          this.hc = a.Pd(this, this.ma);
          b.Gc(this.Ag);
          b.Gc(this.hc);
          break;
        case 2:
          a = dn(this.Dc, b.getName(), this.o.Jb(), j);
          this.Ag = 0 > this.ma ? a.Hl : a.Ol;
          this.hc = a.Pd(this, this.ma);
          break;
        default:
          this.Ag = NaN, this.hc = this.ma, b.Gc(this.hc)
      }
    }else {
      this.Ag = NaN, this.hc = this.ma, b.Gc(this.hc)
    }
    this.Gb = isNaN(this.ma);
    this.xl = b.fa().Nb() && 0 <= this.ma || !b.fa().Nb() && 0 > this.ma
  }else {
    po.f.$h.call(this, a), this.Ag = NaN, this.hc = this.ma
  }
};
po.prototype.qk = function() {
  var a = this.o.eb;
  if(this.Dc != p) {
    var b;
    switch(a.fa().ml()) {
      case 1:
        b = dn(this.Dc, a.getName(), this.o.Jb(), q);
        this.Ag = 0 > this.ma ? b.Hl : b.Ol;
        this.hc = b.Pd(this, this.ma);
        a.Gc(this.Ag);
        a.Gc(this.hc);
        break;
      case 2:
        b = dn(this.Dc, a.getName(), this.o.Jb(), j);
        this.Ag = 0 > this.ma ? b.Hl : b.Ol;
        this.hc = b.Pd(this, this.ma);
        break;
      default:
        this.Ag = NaN, this.hc = this.ma, a.Gc(this.hc)
    }
  }else {
    this.Ag = NaN, this.hc = this.ma, a.Gc(this.hc)
  }
  this.xl = a.fa().Nb() && 0 <= this.ma || !a.fa().Nb() && 0 > this.ma
};
po.prototype.FF = function(a) {
  this.Ag *= 0 == a ? 0 : 100 / a;
  this.hc *= 0 == a ? 0 : 100 / a
};
function qo() {
  jo.apply(this)
}
A.e(qo, jo);
z = qo.prototype;
z.Ub = p;
z.Lb = p;
z.hf = p;
z.cl = function(a) {
  !C(a, "start") || !C(a, "end") ? this.Gb = j : this.o.eb.wJ() && (this.Gb = (this.Gb = isNaN(ko(this, F(a, "start")))) || isNaN(ko(this, F(a, "end"))))
};
z.$h = function(a) {
  C(a, "start") && (this.Ub = ko(this, F(a, "start")));
  C(a, "end") && (this.Lb = ko(this, F(a, "end")));
  a = this.o.eb;
  this.xl = a.fa().Nb() && this.Ub < this.Lb || !a.fa().Nb() && this.Ub > this.Lb;
  this.Gb || (this.hf = this.Lb - this.Ub, a.Gc(this.Ub), a.Gc(this.Lb))
};
z.qk = function() {
  this.hf = this.Lb - this.Ub;
  var a = this.o.eb;
  a.Gc(this.Ub);
  a.Gc(this.Lb);
  qo.f.qk.call(this)
};
z.rw = p;
z.sw = p;
z.tw = p;
z.KD = function() {
  qo.f.KD.call(this);
  this.rw = $n(this, this.Ad, j);
  this.sw = $n(this, this.Bd, j);
  this.tw = $n(this, this.Cd, q)
};
z.Tn = function() {
  qo.f.Tn.call(this);
  this.Ni(this.Ad, this.rw);
  this.Ni(this.Bd, this.sw)
};
z.sG = function(a) {
  qo.f.sG.call(this, a);
  var b = this.Cd, c = this.tw;
  this.Ni(b, c);
  ao(this, a, b, c)
};
z.my = function() {
  qo.f.my.call(this);
  bo(this.tw)
};
z.IE = function(a) {
  qo.f.IE.call(this, a);
  ao(this, a, this.Cd, this.tw)
};
z.Kz = function() {
  qo.f.Kz.call(this);
  this.rw && this.Dp(this.Ad, this.rw);
  this.sw && this.Dp(this.Bd, this.sw)
};
z.Ad = p;
z.Bd = p;
z.Cd = p;
z.Dm = function(a, b) {
  C(a, "start_point") && (this.vo(F(a, "start_point"), b), this.ek(F(a, "start_point"), b), this.wo(F(a, "start_point"), b));
  C(a, "end_point") && (this.Ct(F(a, "end_point"), b), this.Dt(F(a, "end_point"), b), this.Et(F(a, "end_point"), b))
};
z.Ct = function(a, b) {
  this.Ad && (C(a, "label") && this.Ad.pm(F(a, "label")) && (this.Ad = this.Ad.copy(), this.Ad.Yi(F(a, "label"), b)), this.Vj(this.Ad))
};
z.Dt = function(a, b) {
  this.Bd && (C(a, "marker") && this.Bd.pm(F(a, "marker")) && (this.Bd = this.Bd.copy(), this.Bd.Yi(F(a, "marker"), b)), this.Vj(this.Bd))
};
z.Et = function(a, b) {
  this.Cd && C(a, "tooltip") && this.Cd.pm(F(a, "tooltip")) && (this.Cd = this.Cd.copy(), this.Cd.Yi(F(a, "tooltip"), b));
  this.Vj(this.Cd)
};
z.Cb = function() {
  qo.f.Cb.call(this);
  this.b.add("%RangeStart", 0);
  this.b.add("%YRangeStart", 0);
  this.b.add("%RangeEnd", 0);
  this.b.add("%YRangeEnd", 0);
  this.b.add("%Range", 0);
  this.b.add("%YRange", 0)
};
z.Pa = function(a) {
  switch(a) {
    case "%RangeStart":
    ;
    case "%YRangeStart":
    ;
    case "%RangeEnd":
    ;
    case "%YRangeEnd":
      return Lm(this.o.eb) ? 3 : 2;
    case "%Range":
    ;
    case "%YRange":
    ;
    case "%Value":
      return 2
  }
  return qo.f.Pa.call(this, a)
};
z.mt = function(a) {
  qo.f.mt.call(this, a);
  a = a.b;
  pe(a, "%CategoryYRangeSum", this.hf);
  this.hf > a.get("%CategoryYRangeMax") && a.add("%CategoryYRangeMax", this.hf);
  this.hf < a.get("%CategoryYRangeMin") && a.add("%CategoryYRangeMin", this.hf)
};
z.Se = function() {
  qo.f.Se.call(this);
  this.b.add("%RangeStart", this.Ub);
  this.b.add("%YRangeStart", this.Ub);
  this.b.add("%RangeEnd", this.Lb);
  this.b.add("%YRangeEnd", this.Lb);
  this.b.add("%Range", this.hf);
  this.b.add("%YRange", this.hf);
  this.b.add("%Value", this.hf)
};
z.mb = function(a) {
  a = qo.f.mb.call(this, a);
  a.YRangeStart = this.Ub;
  a.YRangeEnd = this.Lb;
  a.YRange = this.hf;
  a.StartValue = this.Ub;
  a.EndValue = this.Lb;
  a.StartMarkerType = this.ge;
  a.EndMarkerType = this.ge;
  return a
};
function ro() {
  lo.apply(this)
}
A.e(ro, lo);
z = ro.prototype;
z.Ad = p;
z.Bd = p;
z.Cd = p;
z.Dm = function(a, b) {
  this.vo(F(a, "start_point"), b);
  this.ek(F(a, "start_point"), b);
  this.wo(F(a, "start_point"), b);
  this.Ct(F(a, "end_point"), b);
  this.Dt(F(a, "end_point"), b);
  this.Et(F(a, "end_point"), b)
};
z.Ct = function(a, b) {
  this.Ad && this.Ad.sh(F(a, "label")) && (this.Ad = this.Ad.copy(), this.Ad.Zi(F(a, "label"), b))
};
z.Dt = function(a, b) {
  this.Bd && this.Bd.sh(F(a, "marker")) && (this.Bd = this.Bd.copy(), this.Bd.Zi(F(a, "marker"), b))
};
z.Et = function(a, b) {
  this.Cd && this.Cd.sh(F(a, "tooltip")) && (this.Cd = this.Cd.copy(), this.Cd.Zi(F(a, "tooltip"), b))
};
z.Mh = function(a) {
  ro.f.Mh.call(this, a);
  a.Ad = this.Ad;
  a.Bd = this.Bd;
  a.Cd = this.Cd
};
z.Cb = function() {
  ro.f.Cb.call(this);
  this.b.add("%SeriesYRangeMax", -Number.MAX_VALUE);
  this.b.add("%SeriesYRangeMin", Number.MAX_VALUE);
  this.b.add("%SeriesYRangeSum", 0)
};
z.Pa = function(a) {
  switch(a) {
    case "%SeriesYRangeMax":
    ;
    case "%SeriesYRangeMin":
    ;
    case "%SeriesYRangeSum":
      return Lm(this.eb) ? 3 : 2
  }
  return ro.f.Pa.call(this, a)
};
z.th = function(a) {
  var b = a.b;
  pe(b, "%DataPlotYRangeSum", this.b.get("%SeriesYRangeSum"));
  this.b.get("%SeriesYRangeMax") > b.get("%DataPlotYRangeMax") && b.add("%DataPlotYRangeMax", this.b.get("%SeriesYRangeMax"));
  this.b.get("%SeriesYRangeMin") < b.get("%DataPlotYRangeMin") && b.add("%DataPlotYRangeMin", this.b.get("%SeriesYRangeMin"));
  ro.f.th.call(this, a)
};
z.Ed = function(a) {
  ro.f.Ed.call(this, a);
  pe(this.b, "%SeriesYRangeSum", a.hf);
  a.hf > this.b.get("%SeriesYRangeMax") && this.b.add("%SeriesYRangeMax", a.hf);
  a.hf < this.b.get("%SeriesYRangeMin") && this.b.add("%SeriesYRangeMin", a.hf)
};
z.rh = function() {
  ro.f.rh.call(this)
};
z.mb = function(a) {
  a = ro.f.mb.call(this, a);
  a.YRangeSum = this.b.get("%SeriesRangeSum");
  a.YRangeMax = this.b.get("%SeriesRangeMax");
  a.YRangeMin = this.b.get("%SeriesRangeMin");
  return a
};
function so() {
  io.apply(this)
}
A.e(so, io);
z = so.prototype;
z.Ad = p;
z.Bd = p;
z.Cd = p;
z.Dm = function(a, b) {
  this.vo(F(a, "start_point"), b);
  this.ek(F(a, "start_point"), b);
  this.wo(F(a, "start_point"), b);
  this.Ct(F(a, "end_point"), b);
  this.Dt(F(a, "end_point"), b);
  this.Et(F(a, "end_point"), b)
};
z.Ct = function(a, b) {
  (this.Ad = this.Aq()) && (a && C(a, "label_settings") ? this.Ad.Ue(F(a, "label_settings"), b) : this.Ad.rf(new $))
};
z.Dt = function(a, b) {
  (this.Bd = this.Bm()) && (a && C(a, "marker_settings") ? this.Bd.Ue(F(a, "marker_settings"), b) : this.Bd.rf(new $))
};
fo.prototype.Et = function(a, b) {
  (this.Cd = new Un) && (a && C(a, "tooltip_settings") ? this.Cd.Ue(F(a, "tooltip_settings"), b) : this.Cd.rf(new $))
};
fo.prototype.Mh = function(a) {
  fo.f.Mh.call(this, a);
  a.Ad = this.Ad;
  a.Bd = this.Bd;
  a.Cd = this.Cd
};
so.prototype.MD = s();
function to() {
  jo.apply(this)
}
A.e(to, po);
to.prototype.Yl = 0;
to.prototype.ol = u("Yl");
to.prototype.Lp = t("Yl");
to.prototype.kn = function(a) {
  return Wc(this.n(), a)
};
function uo() {
  lo.apply(this)
}
A.e(uo, mo);
z = uo.prototype;
z.vs = NaN;
z.Yl = 0;
z.Lp = t("Yl");
z.un = x(j);
z.g = function(a, b) {
  uo.f.g.call(this, a, b);
  vo(this, a)
};
z.Ug = function() {
  this.vs = 0 < this.eb.fa().la ? this.eb.fa().la : 0
};
z.qe = function() {
  var a = new to;
  a.Lp(this.Yl);
  return a
};
function wo() {
  go.apply(this)
}
A.e(wo, io);
z = wo.prototype;
z.Yl = 0;
z.Lp = t("Yl");
z.rr = q;
z.St = NaN;
z.g = function(a, b) {
  wo.f.g.call(this, a, b);
  C(a, "point_width") && (this.rr = j, this.St = M(a, "point_width"));
  vo(this, a)
};
function vo(a, b) {
  C(b, "shape_type") && a.Lp(ln(N(b, "shape_type")))
}
z.VC = function(a) {
  return this.aa.Ly ? (this.mE ? this.aa.n().width : 1) * this.Pr : this.rr ? this.St : a.zf.pt
};
z.uh = function() {
  var a = new uo;
  a.gh(0);
  a.Wj = 0;
  a.Lp(this.Yl);
  return a
};
z.Ah = x("bar_series");
z.Rb = x("bar_style");
z.Qb = function() {
  switch(this.Yl) {
    case 3:
      return this.aa.Ic ? new zn : new An;
    default:
      return new yn
  }
};
to.prototype.bg = function(a, b, c, d) {
  b = this.xl ? this.o.Wb.J.vi.Ox(b) : this.o.Wb.J.vi.Yx(b);
  10 == b ? to.f.bg.call(this, a, b, c, d) : this.$a.bg(a, b, c, d)
};
function xo() {
  qo.apply(this)
}
A.e(xo, qo);
xo.prototype.ol = x(0);
xo.prototype.p = function(a) {
  xo.f.p.call(this, a);
  return this.D
};
xo.prototype.hd = function() {
  var a = new O, b = new O, c = this.o, d = this.Ab().VC(this) / 2;
  c.Wb.transform(this, this.Hd(), a, -d);
  c.eb.transform(this, this.Ub, a);
  c.Wb.transform(this, this.Hd(), b, d);
  c.eb.transform(this, this.Lb, b);
  c = Math.min(a.x, b.x);
  d = Math.min(a.y, b.y);
  this.k = new P(c, d, Math.max(a.x, b.x) - c, Math.max(a.y, b.y) - d)
};
xo.prototype.kn = function(a) {
  return Wc(this.n(), a)
};
function yo() {
  ro.apply(this);
  this.rr = q
}
A.e(yo, ro);
yo.prototype.qe = function() {
  return new xo
};
function zo() {
  so.apply(this)
}
A.e(zo, so);
z = zo.prototype;
z.rr = p;
z.St = p;
z.g = function(a) {
  zo.f.g.call(this, a);
  C(a, "point_width") && (this.rr = j);
  this.St = M(a, "point_width")
};
z.VC = function(a) {
  return this.rr ? this.St : a.zf.pt
};
z.Qb = function() {
  return new Cn
};
z.uh = function() {
  var a = new yo;
  a.gh(1);
  a.Wj = 1;
  return a
};
z.Ah = x("range_bar_series");
z.Rb = x("bar_style");
function Ao() {
}
A.e(Ao, Nf);
Ao.prototype.Ta = function() {
  Bo(this.j, this.I)
};
function Co() {
}
A.e(Co, Of);
Co.prototype.Ta = function() {
  Bo(this.j, this.I)
};
function Do() {
}
A.e(Do, Pf);
Do.prototype.Yc = function() {
  var a = this.j.ka();
  a.wn() && (this.Db = new Ao, this.Db.p(this.j, this));
  a.wi() && (this.Fb = new Co, this.Fb.p(this.j, this))
};
Do.prototype.createElement = function() {
  var a = this.j.dt || 0, b = this.j.et || 0, c = Math.abs(this.j.bd - this.j.Zh) || 0, d = Cc(this.j.r(), "circle");
  d.setAttribute("cx", a);
  d.setAttribute("cy", b);
  d.setAttribute("r", c);
  return d
};
Do.prototype.Ta = function() {
  Bo(this.j, this.I)
};
function Eo() {
  $.call(this)
}
A.e(Eo, Rf);
Eo.prototype.lc = function() {
  return new Do
};
function Fo() {
  oo.apply(this);
  this.Qd = new O
}
A.e(Fo, oo);
z = Fo.prototype;
z.qh = p;
z.Do = u("qh");
z.bd = p;
z.vd = u("bd");
z.dt = p;
z.et = p;
z.Zh = p;
z.Qd = p;
z.ae = u("Qd");
z.hd = function() {
  this.o.Wb.transform(this, this.Vb, this.Qd);
  this.o.eb.transform(this, this.hc, this.Qd);
  this.dt = this.Qd.x;
  this.et = this.Qd.y;
  this.bd = this.Ab().Do(this.qh) / 2;
  this.Zh = 0;
  this.k = new P(this.dt - this.bd, this.et - this.bd, 2 * this.bd, 2 * this.bd);
  this.k.x += this.Zh;
  this.k.y += this.Zh;
  this.k.width -= 2 * this.Zh;
  this.k.height -= 2 * this.Zh;
  this.ya.Lc() && this.ya.Lc().isEnabled() && (this.Zh = this.ya.Lc().Va() / 2)
};
function Bo(a, b) {
  a.hd();
  b.setAttribute("cx", a.dt);
  b.setAttribute("cy", a.et);
  b.setAttribute("r", Math.abs(a.bd - a.Zh));
  a.k.x -= a.Zh;
  a.k.y -= a.Zh;
  a.k.width += 2 * a.Zh;
  a.k.height += 2 * a.Zh
}
z.$h = function(a) {
  Fo.f.$h.call(this, a);
  this.qh = M(a, "size");
  if(isNaN(this.qh)) {
    this.Gb = j
  }else {
    if(a = this.Ab(), this.qh > a.gz && (a.gz = this.qh), this.qh < a.Vu) {
      a.Vu = this.qh
    }
  }
};
z.cl = function(a) {
  Fo.f.cl.call(this, a);
  this.Gb = this.Gb || !C(a, "size") || isNaN(M(a, "size"));
  this.Ab().NH && 0 > M(a, "size") && (this.Gb = j)
};
z.kn = function(a) {
  var b = new Go(a.x, a.y), c = new Go(a.x + a.width, a.y + a.height), d = new Go(this.dt, this.et), f = this.n();
  return(b = Ho.aN(d, f.width / 2, [b, c])) && b.Fa.length ? j : Xc(a, f) || Xc(f, a)
};
z.Cb = function() {
  Fo.f.Cb.call(this);
  this.b.add("%BubbleSize", 0)
};
z.Pa = function(a) {
  switch(a) {
    case "%BubbleSize":
    ;
    case "%BubbleSizePercentOfSeries":
    ;
    case "%BubbleSizePercentOfTotal":
    ;
    case "%BubbleSizePercentOfCategory":
      return 2
  }
  return Fo.f.Pa.call(this, a)
};
z.Se = function() {
  Fo.f.Se.call(this);
  this.b.add("%BubbleSize", this.qh)
};
z.Na = function(a) {
  oe(this.b, "%BubbleSizePercentOfSeries") || this.b.add("%BubbleSizePercentOfSeries", 100 * (this.qh / this.o.Uf("%SeriesBubbleSizeSum")));
  oe(this.b, "%BubbleSizePercentOfTotal") || this.b.add("%BubbleSizePercentOfTotal", 100 * (this.qh / this.ca().Na("%DataPlotBubbleSizeSum")));
  !oe(this.b, "%BubbleSizePercentOfCategory") && this.Fd && this.b.add("%BubbleSizePercentOfCategory", 100 * (this.qh / 0));
  return Fo.f.Na.call(this, a)
};
z.mb = function(a) {
  a = Fo.f.mb.call(this, a);
  a.BubbleSize = this.qh;
  a.BubbleSizePercentOfSeries = this.b.get("%BubbleSizePercentOfSeries");
  a.BubbleSizePercentOfTotal = this.b.get("%BubbleSizePercentOfTotal");
  a.BubbleSizePercentOfCategory = this.b.get("%BubbleSizePercentOfCategory");
  return a
};
function Io() {
  mo.apply(this)
}
A.e(Io, mo);
z = Io.prototype;
z.fn = x(q);
z.qe = function() {
  return new Fo
};
z.Cb = function() {
  Io.f.Cb.call(this);
  this.b.add("%SeriesBubbleSizeSum", 0);
  this.b.add("%SeriesBubbleMaxSize", -Number.MAX_VALUE);
  this.b.add("%SeriesBubbleMinSize", Number.MAX_VALUE)
};
z.Pa = function(a) {
  switch(a) {
    case "%SeriesBubbleSizeSum":
    ;
    case "%SeriesBubbleMaxSize":
    ;
    case "%SeriesBubbleMinSize":
    ;
    case "%SeriesBubbleSizeAverage":
    ;
    case "%SeriesBubbleSizeMedian":
    ;
    case "%SeriesBubbleSizeMode":
      return 2
  }
  return Io.f.Pa.call(this, a)
};
z.th = function(a) {
  Io.f.th.call(this, a);
  a = a.b;
  pe(a, "%DataPlotBubbleSizeSum", this.b.get("%SeriesBubbleSizeSum"));
  pe(a, "%DataPlotBubblePointCount", this.ba.length);
  this.b.get("%SeriesBubbleMaxSize") > a.get("%DataPlotBubbleMaxSize") && a.add("%DataPlotBubbleMaxSize", this.b.get("%SeriesBubbleMaxSize"));
  this.b.get("%SeriesBubbleMinSize") < a.get("%DataPlotBubbleMinSize") && a.add("%DataPlotBubbleMinSize", this.b.get("%SeriesBubbleMinSize"))
};
z.Ed = function(a) {
  Io.f.Ed.call(this, a);
  pe(this.b, "%SeriesBubbleSizeSum", a.Do());
  a.Do() > this.b.get("%SeriesBubbleMaxSize") && this.b.add("%SeriesBubbleMaxSize", a.Do());
  a.Do() < this.b.get("%SeriesBubbleMinSize") && this.b.add("%SeriesBubbleMinSize", a.Do())
};
z.kt = function() {
  Jo(this, this.Wb.b);
  Jo(this, this.eb.b);
  Io.f.kt.call(this)
};
function Jo(a, b) {
  pe(b, "%AxisBubbleSizeSum", a.b.get("%SeriesBubbleSizeSum"));
  a.b.get("%SeriesBubbleMaxSize") > b.get("%AxisBubbleMax") && b.add("%AxisBubbleSizeMax", a.b.get("%SeriesBubbleMaxSize"));
  a.b.get("%SeriesBubbleMinSize") < b.get("%AxisBubbleSizeMin") && b.add("%AxisBubbleSizeMin", a.b.get("%SeriesBubbleMinSize"))
}
z.rh = function() {
  Io.f.rh.call(this);
  this.b.add("%SeriesBubbleSizeAverage", this.Uf("%SeriesBubbleSizeSum") / this.ba.length)
};
z.mb = function(a) {
  a = Io.f.mb.call(this, a);
  a.BubbleSizeSum = this.b.get("%SeriesBubbleSizeSum");
  a.BubbleMaxSize = this.b.get("%SeriesBubbleSizeMax");
  a.BubbleMinSize = this.b.get("%SeriesBubbleSizeMin");
  a.BubbleSizeAverage = this.b.get("%SeriesBubbleSizeAverage");
  a.BubbleSizeMedian = this.b.get("%SeriesBubbleSizeMedian");
  a.BubbleSizeMode = this.b.get("%SeriesBubbleSizeMode");
  return a
};
function Ko() {
  io.apply(this)
}
A.e(Ko, io);
z = Ko.prototype;
z.lE = q;
z.Uk = 0;
z.Vu = Number.MAX_VALUE;
z.gz = -Number.MAX_VALUE;
z.jE = j;
z.qn = 0.2;
z.kE = j;
z.sn = 0.01;
z.NH = j;
z.Do = function(a) {
  var b = Math.min(this.ca().n().width, this.ca().n().height);
  if(0 < this.Uk) {
    return this.lE ? a * this.Uk * b : a * this.Uk
  }
  var c = this.kE ? b * this.sn : this.sn, b = this.jE ? b * this.qn : this.qn;
  return isNaN(a) || this.Vu == this.gz ? b : c + (a - this.Vu) / (this.gz - this.Vu) * (b - c)
};
z.g = function(a) {
  if(a && (Ko.f.g.call(this, a), C(a, "maximum_bubble_size") && (this.qn = F(a, "maximum_bubble_size"), Mb(this.qn) ? (this.jE = j, this.qn = Nb(this.qn)) : (this.jE = q, this.qn = Ob(this.qn))), C(a, "minimum_bubble_size") && (this.sn = F(a, "minimum_bubble_size"), Mb(this.sn) ? (this.kE = j, this.sn = Nb(this.sn)) : (this.kE = q, this.sn = Ob(this.sn))), C(a, "bubble_size_multiplier") && (this.Uk = F(a, "bubble_size_multiplier"), Mb(this.Uk) ? (this.lE = j, this.Uk = Nb(this.Uk)) : (this.lE = q, 
  this.Uk = Ob(this.Uk))), C(a, "display_negative_bubbles"))) {
    this.NH = K(a, "display_negative_bubbles")
  }
};
z.Qb = function() {
  return new Eo
};
z.uh = function() {
  var a = new Io;
  a.type = 12;
  a.FL = 12;
  return a
};
z.Ah = x("bubble_series");
z.Rb = x("bubble_style");
function Lo() {
}
A.e(Lo, Ne);
Lo.prototype.createElement = function() {
  return this.$a.createElement()
};
Lo.prototype.vf = function(a) {
  var b = this.j.r();
  Mo(a) ? (a = Md(a.Kb, b, this.j.n(), this.j.Ba().Ba()), a += md()) : a = od();
  this.I.setAttribute("style", a)
};
Lo.prototype.ud = function(a) {
  this.$a.ud(this.I, a)
};
function No() {
}
A.e(No, Re);
z = No.prototype;
z.If = p;
z.Yc = function() {
  this.j.ka().lz() && (this.If = new Lo, this.If.p(this.j, this))
};
z.ff = function() {
  this.If && this.j.fb().appendChild(this.If.I)
};
z.update = function(a, b) {
  this.If && this.If.update(a, b)
};
z.ud = function(a, b) {
  b.pp() ? a.setAttribute("filter", Tc(this.j.r().gk, b.kl())) : a.removeAttribute("filter")
};
function Oo(a, b) {
  Se.call(this, a, b)
}
A.e(Oo, Te);
Oo.prototype.Kb = p;
function Mo(a) {
  return a.Kb && a.Kb.isEnabled()
}
Oo.prototype.g = function(a) {
  if(!a) {
    return a
  }
  a = Oo.f.g.call(this, a);
  C(a, "line") && Tb(F(a, "line")) && (this.Kb = new Ld, this.Kb.g(F(a, "line")));
  return a
};
function Po() {
  $.call(this)
}
A.e(Po, $);
Po.prototype.cc = function() {
  return Oo
};
Po.prototype.lc = s();
Po.prototype.lz = function() {
  return Mo(this.wa) || Mo(this.dc) || Mo(this.kc) || Mo(this.Zb) || Mo(this.gc) || Mo(this.nc)
};
function Qo() {
}
A.e(Qo, Lo);
Qo.prototype.Ta = function() {
  this.j.Tj();
  this.j.Gy && this.I.setAttribute("points", this.$a.yh())
};
function Ro() {
}
A.e(Ro, No);
Ro.prototype.Yc = function() {
  this.If = new Qo;
  this.If.p(this.j, this)
};
Ro.prototype.createElement = function() {
  return hd(this.j.r())
};
Ro.prototype.yh = function() {
  var a = this.j.Ua;
  if(a) {
    var b = a.length;
    if(!(1 >= b)) {
      for(var c = id(a[0].x, a[0].y), d = 1;d < b;d++) {
        c += id(a[d].x, a[d].y)
      }
      return c
    }
  }
};
function So() {
  $.call(this)
}
A.e(So, Po);
So.prototype.lc = function() {
  return new Ro
};
function To() {
  jo.apply(this);
  this.Da = new O
}
A.e(To, oo);
z = To.prototype;
z.sJ = q;
z.Gl = j;
z.Lg = q;
z.Ch = q;
z.Gy = q;
z.Ua = p;
z.kn = function(a) {
  var b = [], c = this.Ua;
  if(2 > c.length) {
    return q
  }
  for(var d = Uo.ay({x:a.x, y:a.y}), f = Uo.ay({x:a.x + a.width, y:a.y + a.height}), g = 0;g < c.length - 1;g++) {
    var k = c[g + 1];
    b.push({Vg:Uo.ay(c[g]), Wg:Uo.ay(k)})
  }
  for(c = 0;c < b.length;c++) {
    g = b[c];
    if((k = Ho.nr(g.Vg, g.Wg, d, f)) && k.Fa && k.Fa.length) {
      return j
    }
    g = Uo.WC([g.Vg, g.Wg]);
    g = new P(g.x, g.y, g.width, g.height);
    if(Xc(a, g) || Xc(g, a)) {
      return j
    }
  }
  return q
};
z.Da = p;
z.p = function(a) {
  To.f.p.call(this, a);
  return this.D
};
z.Tj = function() {
  this.Wo || this.Oo();
  this.k = new P(this.Da.x, this.Da.y, 0, 0);
  this.Gy && this.Am()
};
z.Oo = function() {
  this.sJ || (this.Lg = 0 < this.na && this.o.Ga(this.na - 1) && (!this.o.Ga(this.na - 1).Gb || !this.ca().pk), this.Ch = this.na < this.o.Fe() - 1 && this.o.Ga(this.na + 1) && (!this.o.Ga(this.na + 1).Gb || !this.ca().pk), this.Gy = this.Lg || this.Ch, this.sJ = j);
  this.$v();
  this.o.eb.transform(this, this.hc, this.Da)
};
z.$v = function() {
  this.o.Wb.transform(this, this.Vb, this.Da)
};
z.Am = function() {
  this.Ua = [];
  var a, b;
  this.Lg && (a = this.o.Ga(this.na - 1), a = a.Da, b = new O, b.x = (a.x + this.Da.x) / 2, b.y = (a.y + this.Da.y) / 2, this.Ua.push(b));
  this.Ua.push(this.Da);
  this.Ch && (a = this.o.Ga(this.na + 1), a.Oo(), a.Gl = q, a = a.Da, b = new O, b.x = (a.x + this.Da.x) / 2, b.y = (a.y + this.Da.y) / 2, this.Ua.push(b))
};
function Vo() {
  lo.apply(this)
}
A.e(Vo, mo);
Vo.prototype.qe = function() {
  return new To
};
Vo.prototype.fn = x(q);
Vo.prototype.un = x(j);
function Wo() {
  go.apply(this)
}
A.e(Wo, io);
z = Wo.prototype;
z.NF = q;
z.g = function(a) {
  Wo.f.g.call(this, a);
  C(a, "shift_step_line") && (this.NF = K(a, "shift_step_line"))
};
z.uh = function(a) {
  var b;
  switch(a) {
    case 5:
      b = new Xo;
      break;
    case 4:
      b = new Yo;
      break;
    default:
    ;
    case 2:
      b = new Vo
  }
  b.gh(a);
  b.Wj = 2;
  return b
};
z.Qb = function() {
  return new So
};
z.Ah = x("line_series");
z.Rb = x("line_style");
function Xo() {
  lo.apply(this)
}
A.e(Xo, Vo);
Xo.prototype.qe = function() {
  return new Zo
};
function Zo() {
  To.call(this)
}
A.e(Zo, To);
Zo.prototype.Am = function() {
  this.Ua = [];
  var a, b;
  this.Lg && (a = this.o.Ga(this.na - 1).Da, b = this.Ic ? new O(this.Da.x, (a.y + this.Da.y) / 2) : new O((a.x + this.Da.x) / 2, this.Da.y), this.Ua.push(b));
  this.Ua.push(this.Da);
  this.Ch && (this.o.Ga(this.na + 1).Oo(), this.o.Ga(this.na + 1).Gl = q, a = this.o.Ga(this.na + 1).Da, b = this.Ic ? new O(a.x, this.Da.y) : new O(this.Da.x, a.y), this.Ua.push(b), b = this.Ic ? new O(a.x, (a.y + this.Da.y) / 2) : new O((a.x + this.Da.x) / 2, a.y), this.Ua.push(b))
};
Zo.prototype.$v = function() {
  Zo.f.$v.call(this);
  if(this.Dc && this.Ab().NF) {
    var a = this.o.Wb, b = a.fa().pc(1) - a.fa().pc(0.5);
    a.transform(this, this.Vb, this.Da, -b)
  }
};
function Yo() {
  lo.apply(this)
}
A.e(Yo, Vo);
Yo.prototype.qe = function() {
  return new $o
};
function $o() {
  To.call(this)
}
A.e($o, To);
$o.prototype.Am = function() {
  this.Ua = [];
  var a, b;
  this.Lg && (a = this.o.Ga(this.na - 1).Da, b = this.Ic ? new O(a.x, (a.y + this.Da.y) / 2) : new O((a.x + this.Da.x) / 2, a.y), this.Ua.push(b), b = this.Ic ? new O(a.x, this.Da.y) : new O(this.Da.x, a.y), this.Ua.push(b));
  this.Ua.push(this.Da);
  this.Ch && (this.o.Ga(this.na + 1).Oo(), this.o.Ga(this.na + 1).JE = q, a = this.o.Ga(this.na + 1).Da, b = this.Ic ? new O(this.Da.x, (a.y + this.Da.y) / 2) : new O((a.x + this.Da.x) / 2, this.Da.y), this.Ua.push(b))
};
$o.prototype.$v = function() {
  $o.f.$v.call(this);
  if(this.Dc && this.Ab().NF) {
    var a = this.o.Wb, b = a.fa().pc(1) - a.fa().pc(0.5);
    a.transform(this, this.Vb, this.Da, -b)
  }
};
function ap() {
}
A.e(ap, Lf);
ap.prototype.createElement = function() {
  return this.$a.mx()
};
ap.prototype.Ta = function() {
  var a = this.$a;
  this.I.setAttribute("points", a.j.HI(a.j.r()))
};
function bp() {
}
A.e(bp, Mf);
bp.prototype.createElement = function() {
  return this.$a.lx()
};
bp.prototype.Ta = function() {
  this.I.setAttribute("d", this.$a.j.CI())
};
function cp() {
}
A.e(cp, Mf);
cp.prototype.createElement = function() {
  return this.$a.vt()
};
cp.prototype.Ta = function() {
  var a = this.$a;
  this.I.setAttribute("d", a.j.nD(a.j.r()))
};
function dp() {
}
A.e(dp, Of);
dp.prototype.createElement = function() {
  return this.$a.vt()
};
dp.prototype.Ta = function() {
  var a = this.$a;
  this.I.setAttribute("d", a.j.nD(a.j.r()))
};
function ep() {
}
A.e(ep, Re);
z = ep.prototype;
z.Qn = p;
z.vv = p;
z.Db = p;
z.Fb = p;
z.Yc = function() {
  var a = this.j.ka();
  a.lz() && (this.Qn = new ap, this.Qn.p(this.j, this));
  a.av() && (this.Db = new cp, this.Db.p(this.j, this), this.vv = new bp, this.vv.p(this.j, this));
  a.wi() && (this.Fb = new dp, this.Fb.p(this.j, this))
};
z.ff = function() {
  this.Db && (this.j.fb().appendChild(this.Db.I), this.j.fb().appendChild(this.vv.I));
  this.Qn && this.j.fb().appendChild(this.Qn.I);
  this.Fb && this.j.fb().appendChild(this.Fb.I)
};
z.mx = function() {
  return this.j.mx(this.j.r())
};
z.lx = function() {
  return this.j.lx(this.j.r())
};
z.vt = function() {
  return this.j.vt(this.j.r())
};
z.update = function(a, b) {
  this.Db && (this.Db.update(a, b), this.vv.update(a, b));
  this.Qn && this.Qn.update(a, b);
  this.Fb && this.Fb.update(a, b)
};
z.ud = function(a, b) {
  b.pp() ? a.setAttribute("filter", Tc(this.j.r().gk, b.kl())) : a.removeAttribute("filter")
};
function fp(a, b) {
  Se.call(this, a, b)
}
A.e(fp, Qf);
fp.prototype.eC = function(a) {
  C(a, "line") && (this.sc = new Ld, this.sc.g(F(a, "line")))
};
function gp() {
  $.call(this)
}
A.e(gp, Rf);
gp.prototype.cc = function() {
  return fp
};
gp.prototype.lc = function() {
  return new ep
};
function hp() {
}
A.e(hp, Lf);
hp.prototype.update = function(a, b) {
  hp.f.update.call(this, a, b)
};
hp.prototype.vf = function(a) {
  ip(a) && hp.f.vf.call(this, a)
};
hp.prototype.Ta = function() {
  for(var a = this.$a.j, b = "", c = a.Ne.length, d = 0;d < c;d++) {
    b += id(a.Ne[d].x, a.Ne[d].y)
  }
  this.I.setAttribute("points", b)
};
function jp() {
}
A.e(jp, ep);
z = jp.prototype;
z.Ys = p;
z.Yc = function() {
  jp.f.Yc.call(this);
  if(ip(this.j.ka().wa) || ip(this.j.ka().dc) || ip(this.j.ka().kc) || ip(this.j.ka().Zb) || ip(this.j.ka().gc) || ip(this.j.ka().nc)) {
    this.Ys = new hp, this.Ys.p(this.j, this, this.UB)
  }
};
z.ff = function() {
  this.Db && (this.j.fb().appendChild(this.Db.I), this.j.fb().appendChild(this.vv.I));
  this.Qn && this.j.fb().appendChild(this.Qn.I);
  this.Ys && this.j.fb().appendChild(this.Ys.I);
  this.Fb && this.j.fb().appendChild(this.Fb.I)
};
z.UB = function() {
  return this.j.UB(this.j.r())
};
z.update = function(a, b) {
  jp.f.update.call(this, a, b);
  a && ip(a) && this.Ys.update(a, b)
};
function kp(a, b) {
  Se.call(this, a, b)
}
A.e(kp, fp);
kp.prototype.Bx = p;
function ip(a) {
  return a.Bx && a.Bx.isEnabled()
}
kp.prototype.eC = function(a) {
  Tb(F(a, "start_line")) && (this.sc = new Ld, this.sc.g(F(a, "start_line")));
  Tb(F(a, "end_line")) && (this.Bx = new Ld, this.Bx.g(F(a, "end_line")))
};
function lp() {
  $.call(this)
}
A.e(lp, Rf);
lp.prototype.cc = function() {
  return kp
};
lp.prototype.lc = function() {
  return new jp
};
function mp() {
  go.apply(this)
}
A.e(mp, io);
mp.prototype.Qb = function() {
  return new gp
};
mp.prototype.Ah = x("area_series");
mp.prototype.Rb = x("area_style");
mp.prototype.uh = function(a) {
  var b;
  switch(a) {
    case 9:
      b = new np;
      break;
    case 8:
      b = new op;
      break;
    case 6:
      b = new Tk;
      break;
    default:
      b = new Tk
  }
  b.gh(a);
  b.Wj = 2;
  return b
};
function Tk() {
  lo.apply(this)
}
A.e(Tk, Vo);
z = Tk.prototype;
z.CK = s();
z.un = x(j);
z.vs = NaN;
z.P = p;
z.X = p;
z.Zs = function() {
  this.P = new O(Number.MAX_VALUE, Number.MAX_VALUE);
  this.X = new O(-Number.MAX_VALUE, -Number.MAX_VALUE);
  for(var a = this.Fe(), b = 0;b < a;b++) {
    var c = this.Ga(b);
    c && !c.Gb && (c.AF(j), c.BF(j), c.Tj(), pp(this, c), c.gf == p ? (this.P.x = Math.min(c.Kc.x, c.ne.x, this.P.x), this.P.y = Math.min(c.Kc.y, c.ne.y, this.P.y), this.X.x = Math.max(c.Kc.x, c.ne.x, this.X.x), this.X.y = Math.max(c.Kc.y, c.ne.y, this.X.y)) : pp(this, c.gf))
  }
};
function pp(a, b) {
  for(var c = b.Ua, d = 0;d < c.length;d++) {
    a.P.x = Math.min(c[d].x, a.P.x), a.P.y = Math.min(c[d].y, a.P.y), a.X.x = Math.max(c[d].x, a.X.x), a.X.y = Math.max(c[d].y, a.X.y)
  }
}
z.Ug = function() {
  Tk.f.Ug.call(this);
  this.vs = 0 < this.eb.fa().la ? this.eb.fa().la : 0;
  this.Zs()
};
z.An = function() {
  this.Zs()
};
z.qe = function() {
  return new qp
};
function qp() {
  To.call(this)
}
A.e(qp, To);
z = qp.prototype;
z.ne = p;
z.Kc = p;
z.vn = j;
z.AF = t("vn");
z.xn = j;
z.BF = t("xn");
z.Gl = p;
z.JE = u("Gl");
z.Av = t("Gl");
z.p = function(a) {
  if(!this.Gb) {
    return this.o.CK(this.K.Gf()), qp.f.p.call(this, a), this.D
  }
};
z.ir = function() {
  this.Oo();
  this.xn = q
};
z.Oo = function() {
  this.xn && (qp.f.Oo.call(this), this.Gy && !this.gf && (this.ne = new O, this.Kc = new O, this.o.eb.transform(this, this.o.vs, this.ne), this.o.eb.transform(this, this.o.vs, this.Kc)))
};
z.Am = function() {
  if(this.vn) {
    this.Ua = [];
    var a, b;
    this.Lg ? (a = this.o.Ga(this.na - 1).Da, b = new O, b.x = (a.x + this.Da.x) / 2, b.y = (a.y + this.Da.y) / 2, this.gf == p && (this.Ic ? this.ne.y = b.y : this.ne.x = b.x), this.Ua.push(b)) : this.gf == p && (this.Ic ? this.ne.y = this.Da.y : this.ne.x = this.Da.x);
    this.Ua.push(this.Da);
    this.Ch ? (this.o.Ga(this.na + 1).ir(), this.o.Ga(this.na + 1).Av(q), a = this.o.Ga(this.na + 1).Da, b = new O, b.x = (a.x + this.Da.x) / 2, b.y = (a.y + this.Da.y) / 2, this.Ua.push(b), this.gf == p && (this.Ic ? this.Kc.y = b.y : this.Kc.x = b.x)) : this.gf == p && (this.Ic ? this.Kc.y = this.Da.y : this.Kc.x = this.Da.x)
  }
  this.vn = q
};
z.kn = function(a) {
  var b = [], c = this.ne, d = this.Kc;
  b.push(new Go(c.x, c.y));
  for(c = 0;c < this.Ua.length;c++) {
    var f = this.Ua[c];
    b.push(new Go(f.x, f.y))
  }
  b.push(new Go(d.x, d.y));
  d = new Go(a.x, a.y);
  c = new Go(a.x + a.width, a.y + a.height);
  if((d = Ho.WD(b, d, c)) && d.Fa.length) {
    return j
  }
  b = Uo.WC(b);
  b = new P(b.x, b.y, b.width, b.height);
  return Xc(a, b) || Xc(b, a)
};
z.mx = function(a) {
  return hd(a)
};
z.HI = function() {
  for(var a = this.Ua.length, b = id(this.Ua[0].x, this.Ua[0].y), c = 0;c < a;c++) {
    b += id(this.Ua[c].x, this.Ua[c].y)
  }
  return b
};
z.lx = function(a) {
  return a.ja()
};
z.CI = function() {
  if(!this.Kc) {
    return"M 0 0"
  }
  var a = this.Ua[this.Ua.length - 1], b;
  this.Ic ? (b = S(this.Kc.x, a.y - 1), b += U(this.Kc.x + (a.x - this.Kc.x), a.y - 1), b += U(this.Kc.x + (a.x - this.Kc.x), a.y + 1), b += U(this.Kc.x, a.y + 1), b += U(this.Kc.x, a.y)) : (b = S(a.x - 1, a.y), b += U(a.x + 1, a.y), b += U(a.x + 1, a.y + (this.Kc.y - a.y)), b += U(a.x - 1, a.y + (this.Kc.y - a.y)), b += U(a.x - 1, a.y));
  return b
};
z.vt = function(a) {
  return a.ja()
};
z.nD = function() {
  for(var a = this.Ua.length, b = S(this.Ua[0].x, this.Ua[0].y), c = 1;c < a;c++) {
    b += U(this.Ua[c].x, this.Ua[c].y)
  }
  if(this.gf) {
    for(var a = this.gf, c = "", d = a.Ua.length - 1;0 <= d;d--) {
      c += U(a.Ua[d].x, a.Ua[d].y)
    }
    a = c
  }else {
    a = "" + U(this.Kc.x, this.Kc.y), a += U(this.ne.x, this.ne.y)
  }
  return b + a + " Z"
};
function np() {
  lo.apply(this)
}
A.e(np, Tk);
np.prototype.qe = function() {
  return new rp
};
function rp() {
  To.call(this)
}
A.e(rp, qp);
rp.prototype.Am = function() {
  this.Ua = [];
  var a, b;
  this.Lg ? (a = this.o.Ga(this.na - 1).Da, b = this.Ic ? new O(this.Da.x, (a.y + this.Da.y) / 2) : new O((a.x + this.Da.x) / 2, this.Da.y), this.Ua.push(b), this.gf == p && (this.Ic ? this.ne.y = b.y : this.ne.x = b.x)) : this.gf == p && (this.Ic ? this.ne.y = this.Da.y : this.ne.x = this.Da.x);
  this.Ua.push(this.Da);
  this.Ch ? (this.o.Ga(this.na + 1).ir(), this.o.Ga(this.na + 1).Av(q), a = this.o.Ga(this.na + 1).Da, b = this.Ic ? new O(a.x, this.Da.y) : new O(this.Da.x, a.y), this.Ua.push(b), b = this.Ic ? new O(a.x, (a.y + this.Da.y) / 2) : new O((a.x + this.Da.x) / 2, a.y), this.Ua.push(b), this.gf == p && (this.Ic ? this.Kc.y = b.y : this.Kc.x = b.x)) : this.gf == p && (this.Ic ? this.Kc.y = this.Da.y : this.Kc.x = this.Da.x)
};
function op() {
  lo.apply(this)
}
A.e(op, Tk);
op.prototype.qe = function() {
  return new sp
};
function sp() {
  To.call(this)
}
A.e(sp, qp);
sp.prototype.Am = function() {
  this.Ua = [];
  var a, b;
  this.Lg ? (a = this.o.Ga(this.na - 1).Da, b = this.Ic ? new O(a.x, (a.y + this.Da.y) / 2) : new O((a.x + this.Da.x) / 2, a.y), this.Ua.push(b), this.gf == p && (this.Ic ? this.ne.y = b.y : this.ne.x = b.x), b = this.Ic ? new O(a.x, this.Da.y) : new O(this.Da.x, a.y), this.Ua.push(b)) : this.gf == p && (this.Ic ? this.ne.y = this.Da.y : this.ne.x = this.Da.x);
  this.Ua.push(this.Da);
  this.Ch ? (this.o.Ga(this.na + 1).ir(), this.o.Ga(this.na + 1).Av(q), a = this.o.Ga(this.na + 1).Da, b = this.Ic ? new O(this.Da.x, (a.y + this.Da.y) / 2) : new O((a.x + this.Da.x) / 2, this.Da.y), this.Ua.push(b), this.gf == p && (this.Ic ? this.Kc.y = b.y : this.Kc.x = b.x)) : this.gf == p && (this.Ic ? this.Kc.y = this.Da.y : this.Kc.x = this.Da.x)
};
function tp() {
  qo.apply(this);
  this.Gl = j;
  this.Ch = this.Lg = this.rE = q;
  this.jh = new O;
  this.Fg = new O;
  this.xn = this.vn = j
}
A.e(tp, qo);
z = tp.prototype;
z.Gl = p;
z.JE = u("Gl");
z.Av = t("Gl");
z.rE = p;
z.jh = p;
z.kn = function(a) {
  for(var b = [], c = this.Ne.concat(this.We), d = 0;d < c.length;d++) {
    var f = c[d];
    b.push(new Go(f.x, f.y))
  }
  c = new Go(a.x, a.y);
  d = new Go(a.x + a.width, a.y + a.height);
  if((c = Ho.WD(b, c, d)) && c.Fa.length) {
    return j
  }
  b = Uo.WC(b);
  b = new P(b.x, b.y, b.width, b.height);
  return Xc(a, b) || Xc(b, a)
};
z.Fg = p;
z.vn = p;
z.AF = t("vn");
z.xn = p;
z.BF = t("xn");
z.eJ = p;
z.Ne = p;
z.We = p;
z.Lg = p;
z.Ch = p;
z.ir = function() {
  if(this.xn) {
    var a = this.o;
    this.rE || (this.Lg = 0 < this.na && this.o.Ga(this.na - 1) && !this.o.Ga(this.na - 1).Gb, this.Ch = this.na < this.o.Fe() - 1 && this.o.Ga(this.na + 1) && !this.o.Ga(this.na + 1).Gb, this.eJ = this.Lg || this.Ch, this.rE = j);
    a.Wb.transform(this, this.Hd(), this.jh);
    a.eb.transform(this, this.Ub, this.jh);
    a.Wb.transform(this, this.Hd(), this.Fg);
    a.eb.transform(this, this.Lb, this.Fg);
    var a = Math.min(this.jh.x, this.Fg.x), b = Math.min(this.jh.y, this.Fg.y);
    this.k = new P(a, b, Math.max(this.jh.x, this.Fg.x) - a, Math.max(this.jh.y, this.Fg.y) - b)
  }
  this.xn = q
};
z.Am = function() {
  if(this.vn) {
    this.Ne = [];
    this.We = [];
    var a = this.o, b, c;
    this.Lg && (b = a.Ga(this.na - 1).jh, c = new O, c.x = (b.x + this.jh.x) / 2, c.y = (b.y + this.jh.y) / 2, this.Ne.push(c));
    this.Ne.push(this.jh);
    this.Ch && (a.Ga(this.na + 1).ir(), a.Ga(this.na + 1).Av(q), b = a.Ga(this.na + 1).jh, c = new O, c.x = (b.x + this.jh.x) / 2, c.y = (b.y + this.jh.y) / 2, this.Ne.push(c), b = a.Ga(this.na + 1).Fg, c = new O, c.x = (b.x + this.Fg.x) / 2, c.y = (b.y + this.Fg.y) / 2, this.We.push(c));
    this.We.push(this.Fg);
    this.Lg && (b = a.Ga(this.na - 1).Fg, c = new O, c.x = (b.x + this.Fg.x) / 2, c.y = (b.y + this.Fg.y) / 2, this.We.push(c))
  }
  this.vn = q
};
z.vt = function(a) {
  return a.ja()
};
z.nD = function() {
  var a, b = S(this.Ne[0].x, this.Ne[0].y);
  for(a = 1;a < this.Ne.length;a++) {
    b += U(this.Ne[a].x, this.Ne[a].y)
  }
  for(a = 0;a < this.We.length;a++) {
    b += U(this.We[a].x, this.We[a].y)
  }
  return b
};
z.mx = function(a) {
  return hd(a)
};
z.HI = function() {
  for(var a = id(this.We[0].x, this.We[0].y), b = 1;b < this.We.length;b++) {
    a += id(this.We[b].x, this.We[b].y)
  }
  return a
};
z.UB = function(a) {
  return hd(a)
};
z.lx = function(a) {
  return a.ja()
};
z.CI = function() {
  var a = this.Ne[this.Ne.length - 1], b = this.We[0], c;
  this.Ic ? (c = S(a.x, a.y), c += U(a.x, a.y + 0.1), c += U(b.x, a.y + 0.1), c += U(b.x, b.y - 0.1)) : (c = S(a.x, a.y), c += U(a.x + 0.1, a.y), c += U(a.x + 0.1, b.y), c += U(b.x - 0.1, b.y));
  return c
};
function up() {
  ro.apply(this);
  this.KE = j
}
A.e(up, ro);
z = up.prototype;
z.KE = p;
z.CK = t("KE");
z.D = p;
z.fb = u("D");
z.un = x(j);
z.fn = x(q);
z.qe = function() {
  return new tp
};
z.Zs = function() {
  if(this.KE) {
    for(var a = new O(Number.MAX_VALUE, Number.MAX_VALUE), b = new O(-Number.MAX_VALUE, -Number.MAX_VALUE), c = 0;c < this.ba.length;c++) {
      var d = this.ba[c];
      if(!d.Gb) {
        d.AF(j);
        d.BF(j);
        d.JE() && d.ir();
        d.eJ && d.Am();
        for(var f = 0;f < d.Ne.length;f++) {
          a.x = Math.min(d.Ne[f].x, a.x), a.y = Math.min(d.Ne[f].y, a.y), b.x = Math.max(d.Ne[f].x, b.x), b.y = Math.max(d.Ne[f].y, b.y)
        }
        for(f = 0;f < d.We.length;f++) {
          a.x = Math.min(d.We[f].x, a.x), a.y = Math.min(d.We[f].y, a.y), b.x = Math.max(d.We[f].x, b.x), b.y = Math.max(d.We[f].y, b.y)
        }
      }
    }
  }
};
z.Ug = function() {
  up.f.Ug.call(this);
  this.Zs()
};
z.An = function() {
  this.Zs()
};
function vp() {
  so.apply(this)
}
A.e(vp, so);
vp.prototype.uh = function(a) {
  var b;
  switch(a) {
    default:
    ;
    case 10:
      b = new up;
      break;
    case 11:
      b = new up
  }
  b.type = a;
  b.FL = 10;
  return b
};
vp.prototype.Qb = function() {
  return new lp
};
vp.prototype.Ah = x("range_area_series");
vp.prototype.Rb = x("range_area_style");
function wp() {
}
A.e(wp, Ne);
z = wp.prototype;
z.FH = p;
z.vI = p;
z.WK = p;
z.p = function(a, b, c, d, f, g) {
  wp.f.p.call(this, a, b, c);
  this.FH = d;
  this.vI = f;
  this.WK = g
};
z.vf = function(a) {
  var b = "", c = this.j.r();
  this.FH.call(this.$a, a) ? (a = this.vI.call(this.$a, a), b += Md(a, c, this.j.n(), this.j.Ba().Ba()), b += md()) : b += od();
  this.I.setAttribute("style", b)
};
z.Ta = function() {
  this.WK.call(this.$a, this.I)
};
z.ud = function(a) {
  this.$a.ud(this.I, a)
};
function xp() {
}
A.e(xp, Ne);
xp.prototype.vf = function(a) {
  var b = this.j.Dh;
  if(a.xc(b)) {
    var c = this.j.r(), d = "", f = this.j.Ba().Ba(), g = this.j.n(), a = a.XC(b), d = a.Ie() ? d + Kd(a.mc(), c, g, f) : d + md(), d = a.Md() ? d + Md(a.Lc(), c, g, f) : d + nd()
  }else {
    d += od()
  }
  this.I.setAttribute("style", d)
};
xp.prototype.Ta = function() {
  yp(this.$a, this.I)
};
xp.prototype.ud = function(a) {
  this.$a.ud(this.I, a)
};
function zp() {
}
A.e(zp, Ne);
zp.prototype.vf = function(a) {
  var b = this.j.Dh, c;
  Ap(a, b) && (a = a.XC(b), c = a.df() ? Hc(a.De(), this.j.r(), this.j.Ze, this.j.Ba().Ba()) : od());
  this.I.setAttribute("style", c)
};
zp.prototype.Ta = function() {
  yp(this.$a, this.I)
};
zp.prototype.ud = function(a) {
  this.$a.ud(this.I, a)
};
function Bp(a, b) {
  Se.call(this, a, b)
}
A.e(Bp, Se);
z = Bp.prototype;
z.Mv = p;
z.ar = u("Mv");
z.rz = function() {
  return this.Mv && this.Mv.isEnabled()
};
z.Bn = p;
z.Yq = u("Bn");
z.qz = function() {
  return this.Bn && this.Bn.isEnabled()
};
z.um = p;
z.Tq = u("um");
z.oz = function() {
  return this.um && this.um.isEnabled()
};
z.g = function(a) {
  a = Bp.f.g.call(this, a);
  if(Tb(F(a, "line"))) {
    var b = F(a, "line");
    this.Mv = new Ld;
    this.Bn = new Ld;
    this.um = new Ld;
    this.Mv.g(b);
    this.Bn.g(b);
    this.um.g(b)
  }
  Tb(F(a, "open_line")) && (this.Bn || (this.Bn = new Ld), this.Bn.g(F(a, "open_line")));
  Tb(F(a, "close_line")) && (this.um || (this.um = new Ld), this.um.g(F(a, "close_line")))
};
function Cp(a, b) {
  Se.call(this, a, b);
  this.Nf = new Bp;
  this.Cf = new Bp
}
A.e(Cp, Te);
z = Cp.prototype;
z.Nf = p;
z.Cf = p;
function Dp(a, b) {
  return b ? a.Nf.rz() : a.Cf.rz()
}
function Ep(a, b) {
  return b ? a.Nf.qz() : a.Cf.qz()
}
function Fp(a, b) {
  return b ? a.Nf.oz() : a.Cf.oz()
}
z.ar = function(a) {
  return a ? this.Nf.ar() : this.Cf.ar()
};
z.Yq = function(a) {
  return a ? this.Nf.Yq() : this.Cf.Yq()
};
z.Tq = function(a) {
  return a ? this.Nf.Tq() : this.Cf.Tq()
};
z.g = function(a) {
  if(!a) {
    return a
  }
  var b = $b(a, F(a, "up"), "up"), c = $b(a, F(a, "down"), "down");
  this.Nf.g(b);
  this.Cf.g(c);
  return a
};
function Gp() {
}
A.e(Gp, Re);
z = Gp.prototype;
z.gs = p;
z.Lr = p;
z.xq = p;
z.Yc = function() {
  var a = this.j.Dh, b = this.j.ka();
  if(Dp(b.wa, a) || Dp(b.dc, a) || Dp(b.kc, a) || Dp(b.Zb, a) || Dp(b.gc, a) || Dp(b.nc, a)) {
    this.gs = new wp, this.gs.p(this.j, this, this.LL, this.rz, this.ar, this.pO)
  }
  if(Ep(b.wa, a) || Ep(b.dc, a) || Ep(b.kc, a) || Ep(b.Zb, a) || Ep(b.gc, a) || Ep(b.nc, a)) {
    this.Lr = new wp, this.Lr.p(this.j, this, this.JL, this.qz, this.Yq, this.oO)
  }
  if(Fp(b.wa, a) || Fp(b.dc, a) || Fp(b.kc, a) || Fp(b.Zb, a) || Fp(b.gc, a) || Fp(b.nc, a)) {
    this.xq = new wp, this.xq.p(this.j, this, this.HL, this.oz, this.Tq, this.mO)
  }
};
z.ff = function() {
  var a = this.j.fb();
  this.gs && a.appendChild(this.gs.I);
  this.Lr && a.appendChild(this.Lr.I);
  this.xq && a.appendChild(this.xq.I)
};
z.update = function(a, b) {
  b && this.j.lq();
  this.gs && this.gs.update(a, b);
  this.Lr && this.Lr.update(a, b);
  this.xq && this.xq.update(a, b)
};
z.ud = function(a, b) {
  b.pp() ? a.setAttribute("filter", Tc(this.j.r().gk, b.kl())) : a.removeAttribute("filter")
};
z.LL = function() {
  return hd(this.j.r())
};
z.pO = function(a) {
  var b = this.j, c = id(b.pg.x, b.pg.y, b.Hj), c = c + id(b.rg.x, b.rg.y, b.Hj);
  a.setAttribute("points", c)
};
z.rz = function(a) {
  return Dp(a, this.j.Dh)
};
z.ar = function(a) {
  return a.ar(this.j.Dh)
};
z.JL = function() {
  return hd(this.j.r())
};
z.oO = function(a) {
  var b = this.j, c = id(b.ve.x, b.ve.y, b.Hj), c = c + id(b.Zk.x, b.Zk.y, b.Hj);
  a.setAttribute("points", c)
};
z.qz = function(a) {
  return Ep(a, this.j.Dh)
};
z.Yq = function(a) {
  return a.Yq(this.j.Dh)
};
z.HL = function() {
  return hd(this.j.r())
};
z.mO = function(a) {
  var b = this.j, c = id(b.pe.x, b.pe.y, b.Hj), c = c + id(b.Xk.x, b.Xk.y, b.Hj);
  a.setAttribute("points", c)
};
z.oz = function(a) {
  return Fp(a, this.j.Dh)
};
z.Tq = function(a) {
  return a.Tq(this.j.Dh)
};
z.XC = function(a) {
  return a ? this.Nf : this.Cf
};
function Hp() {
  $.call(this)
}
A.e(Hp, $);
Hp.prototype.lc = function() {
  return new Gp
};
Hp.prototype.cc = function() {
  return Cp
};
function Ip() {
  Se.call(this)
}
A.e(Ip, Se);
z = Ip.prototype;
z.Hb = p;
z.mc = u("Hb");
z.Ie = function() {
  return this.Hb && this.Hb.isEnabled()
};
z.sc = p;
z.Lc = u("sc");
z.Md = function() {
  return this.sc && this.sc.isEnabled()
};
z.Pc = p;
z.De = u("Pc");
z.df = function() {
  return this.Pc && this.Pc.isEnabled()
};
z.ee = p;
z.Ye = u("ee");
z.pz = function() {
  return this.ee && this.ee.isEnabled()
};
z.g = function(a) {
  if(!a) {
    return a
  }
  a = Ip.f.g.call(this, a);
  if(C(a, "hatch_type")) {
    var b = F(a, "hatch_type");
    if(C(a, "hatch_fill")) {
      var c = F(a, "hatch_fill");
      C(c, "type") && (c.type = J(c, "type").replace("%hatchtype", b))
    }
  }
  Tb(F(a, "fill")) && (this.Hb = new Jd, this.Hb.g(F(a, "fill")));
  Tb(F(a, "border")) && (this.sc = new Ld, this.sc.g(F(a, "border")));
  Tb(F(a, "hatch_fill")) && (this.Pc = new Fc, this.Pc.g(F(a, "hatch_fill")));
  Tb(F(a, "line")) && (this.ee = new Ld, this.ee.g(F(a, "line")));
  return a
};
function Jp(a, b) {
  Se.call(this, a, b);
  this.Nf = new Ip;
  this.Cf = new Ip
}
A.e(Jp, Te);
z = Jp.prototype;
z.Nf = p;
z.Cf = p;
z.xc = function(a) {
  a = a ? this.Nf : this.Cf;
  return a.Ie() || a.Md()
};
function Ap(a, b) {
  return b ? a.Nf.df() : a.Cf.df()
}
function Kp(a, b) {
  return b ? a.Nf.pz() : a.Cf.pz()
}
z.Ye = function(a) {
  return a ? this.Nf.Ye() : this.Cf.Ye()
};
z.XC = function(a) {
  return a ? this.Nf : this.Cf
};
z.g = function(a) {
  if(!a) {
    return a
  }
  var b = $b(a, F(a, "up"), "up"), c = $b(a, F(a, "down"), "down");
  this.Nf.g(b);
  this.Cf.g(c);
  return a
};
function Lp() {
}
A.e(Lp, Re);
z = Lp.prototype;
z.Db = p;
z.Fb = p;
z.If = p;
z.Yc = function() {
  var a = this.j.ka(), b = this.j.Dh;
  a.av(b) && (this.Db = new xp, this.Db.p(this.j, this, this.pH));
  a.wi(b) && (this.Fb = new zp, this.Fb.p(this.j, this, this.pH));
  if(Kp(a.wa, b) || Kp(a.dc, b) || Kp(a.kc, b) || Kp(a.Zb, b) || Kp(a.gc, b) || Kp(a.nc, b)) {
    this.If = new wp, this.If.p(this.j, this, this.IL, this.pz, this.Ye, this.nO)
  }
};
z.ff = function() {
  var a = this.j.fb();
  this.If && a.appendChild(this.If.I);
  this.Db && a.appendChild(this.Db.I);
  this.Fb && a.appendChild(this.Fb.I)
};
z.update = function(a, b) {
  b && this.j.lq();
  this.Db && this.Db.update(a, b);
  this.Fb && this.Fb.update(a, b);
  this.If && this.If.update(a, b)
};
z.ud = function(a, b) {
  b.pp() ? a.setAttribute("filter", Tc(this.j.r().gk, b.kl())) : a.removeAttribute("filter")
};
z.pH = function() {
  return Mc(this.j.r())
};
function yp(a, b) {
  var c = new P(a.j.Jh.x, a.j.Jh.y, a.j.Jh.width, a.j.Jh.height);
  b.setAttribute("x", c.x);
  b.setAttribute("y", c.y);
  b.setAttribute("width", c.width);
  b.setAttribute("height", c.height)
}
z.IL = function() {
  return hd(this.j.r())
};
z.nO = function(a) {
  var b = this.j, c = id(b.pg.x, b.pg.y, b.Hj), c = c + id(b.rg.x, b.rg.y, b.Hj);
  a.setAttribute("points", c)
};
z.pz = function(a) {
  return Kp(a, this.j.Dh)
};
z.Ye = function(a) {
  return a.Ye(this.j.Dh)
};
function Mp() {
  $.call(this)
}
A.e(Mp, $);
Mp.prototype.cc = function() {
  return Jp
};
Mp.prototype.lc = function() {
  return new Lp
};
Mp.prototype.av = function(a) {
  return this.wa.xc(a) || this.dc.xc(a) || this.kc.xc(a) || this.Zb.xc(a) || this.gc.xc(a) || this.nc.xc(a)
};
Mp.prototype.wi = function(a) {
  return Ap(this.wa, a) || Ap(this.dc, a) || Ap(this.kc, a) || Ap(this.Zb, a) || Ap(this.gc, a) || Ap(this.nc, a)
};
function Np() {
  jo.apply(this)
}
A.e(Np, jo);
z = Np.prototype;
z.nj = p;
z.FM = u("nj");
z.UN = t("nj");
z.Vi = p;
z.uM = u("Vi");
z.RN = t("Vi");
z.sl = p;
z.BM = u("sl");
z.SN = t("sl");
z.nn = p;
z.DM = u("nn");
z.TN = t("nn");
z.Dh = p;
z.pg = p;
z.rg = p;
z.cl = function(a) {
  this.sl = Op(this, a, "high", "h");
  this.nn = Op(this, a, "low", "l");
  this.nj = Op(this, a, "open", "o");
  this.Vi = Op(this, a, "close", "c");
  this.Gb = isNaN(this.sl) || isNaN(this.nn) || isNaN(this.nj) || isNaN(this.Vi)
};
z.$h = function() {
  if(!this.Gb) {
    this.Dh = this.Vi > this.nj;
    var a = this.o.eb;
    a.Gc(this.sl);
    a.Gc(this.nn);
    a.Gc(this.nj);
    a.Gc(this.Vi)
  }
};
function Op(a, b, c, d) {
  return C(b, c) ? ko(a, F(b, c)) : C(b, d) ? ko(a, F(b, d)) : NaN
}
z.lq = function() {
  this.pg = new O;
  this.rg = new O;
  var a = this.o;
  a.eb.transform(this, this.sl, this.pg);
  a.eb.transform(this, this.nn, this.rg);
  a.Wb.transform(this, this.Hd(), this.pg);
  a.Wb.transform(this, this.Hd(), this.rg)
};
z.Ka = function() {
  return this.zf.pt / 2
};
z.p = function(a) {
  Np.f.p.call(this, a);
  return this.D
};
z.ci = function() {
  Np.f.ci.call(this);
  A.xa()
};
z.Cb = function() {
  Np.f.Cb.call(this);
  this.b.add("%High", 0);
  this.b.add("%Hight", 0);
  this.b.add("%Low", 0);
  this.b.add("%Open", 0);
  this.b.add("%Close", 0)
};
z.Pa = function(a) {
  switch(a) {
    case "%High":
    ;
    case "%Hight":
    ;
    case "%Low":
    ;
    case "%Open":
    ;
    case "%Close":
      return 2
  }
  return Np.f.Pa.call(this, a)
};
z.Se = function() {
  Np.f.Se.call(this);
  this.b.add("%High", this.sl);
  this.b.add("%Hight", this.sl);
  this.b.add("%Low", this.nn);
  this.b.add("%Open", this.nj);
  this.b.add("%Close", this.Vi)
};
z.mb = function(a) {
  a = Np.f.mb.call(this, a);
  a.High = this.sl;
  a.Low = this.nn;
  a.Open = this.nj;
  a.Close = this.Vi;
  return a
};
function Pp() {
  lo.apply(this)
}
A.e(Pp, lo);
Pp.prototype.fn = x(j);
Pp.prototype.eH = x(j);
Pp.prototype.un = x(j);
function Qp() {
  io.apply(this)
}
A.e(Qp, io);
z = Qp.prototype;
z.nu = p;
z.Mu = p;
z.ev = p;
z.nt = p;
z.MD = function() {
  this.nu = new Ln;
  this.Mu = new Ln;
  this.ev = new Ln;
  this.nt = new Ln;
  this.nu.p(Np.prototype.BM, Np.prototype.SN);
  this.Mu.p(Np.prototype.DM, Np.prototype.TN);
  this.ev.p(Np.prototype.FM, Np.prototype.UN);
  this.nt.p(Np.prototype.uM, Np.prototype.RN)
};
z.cH = function(a) {
  Mn(this.nu, a, a.na);
  Mn(this.Mu, a, a.na);
  Mn(this.ev, a, a.na);
  Mn(this.nt, a, a.na)
};
z.XI = function(a) {
  this.nu.lr(a.ba);
  this.Mu.lr(a.ba);
  this.ev.lr(a.ba);
  this.nt.lr(a.ba);
  this.nu.clear();
  this.Mu.clear();
  this.ev.clear();
  this.nt.clear()
};
function Rp() {
  Np.apply(this);
  this.Hj = j
}
A.e(Rp, Np);
z = Rp.prototype;
z.ve = p;
z.pe = p;
z.Zk = p;
z.Xk = p;
z.Hj = p;
z.p = function(a) {
  Rp.f.p.call(this, a);
  return this.D
};
z.lq = function() {
  Rp.f.lq.call(this);
  this.ve = new O;
  this.pe = new O;
  this.Zk = new O;
  this.Xk = new O;
  var a = this.Ka(), b = this.o;
  b.eb.transform(this, this.nj, this.ve);
  b.eb.transform(this, this.nj, this.Zk);
  b.eb.transform(this, this.Vi, this.pe);
  b.eb.transform(this, this.Vi, this.Xk);
  b.Wb.transform(this, this.Vb, this.ve, -a);
  b.Wb.transform(this, this.Vb, this.Zk);
  b.Wb.transform(this, this.Vb, this.pe, a);
  b.Wb.transform(this, this.Vb, this.Xk);
  a = Math.min(this.ve.x, this.pe.x, this.pg.x, this.rg.x, this.Zk.x, this.Xk.x);
  b = Math.min(this.ve.y, this.pe.y, this.pg.y, this.rg.y, this.Zk.y, this.Xk.y);
  this.k = new P(a, b, Math.max(this.ve.x, this.pe.x, this.pg.x, this.rg.x, this.Zk.x, this.Xk.x) - a, Math.max(this.ve.y, this.pe.y, this.pg.y, this.rg.y, this.Zk.y, this.Xk.y) - b)
};
function Sp() {
  Pp.apply(this)
}
A.e(Sp, Pp);
Sp.prototype.qe = function() {
  return new Rp
};
function Tp() {
  Qp.apply(this)
}
A.e(Tp, Qp);
Tp.prototype.uh = function(a) {
  var b = new Sp;
  b.gh(a);
  b.Wj = a;
  return b
};
Tp.prototype.Ah = x("ohlc_series");
Tp.prototype.Rb = x("ohlc_style");
Tp.prototype.Qb = function() {
  return new Hp
};
function Up() {
  Np.apply(this);
  this.Hj = j
}
A.e(Up, Np);
z = Up.prototype;
z.ve = p;
z.pe = p;
z.Jh = p;
z.Hj = p;
z.Hg = p;
z.Mg = p;
z.lq = function() {
  Up.f.lq.call(this);
  this.Jh = new P;
  this.ve = new O;
  this.pe = new O;
  var a = this.o, b = this.Ka();
  a.eb.transform(this, this.nj, this.ve);
  a.eb.transform(this, this.Vi, this.pe);
  a.Wb.transform(this, this.Hd(), this.ve, -b);
  a.Wb.transform(this, this.Hd(), this.pe, b);
  this.Jh.x = Math.min(this.ve.x, this.pe.x);
  this.Jh.y = Math.min(this.ve.y, this.pe.y);
  this.Jh.width = Math.max(this.ve.x, this.pe.x) - this.Jh.x;
  this.Jh.height = Math.max(this.ve.y, this.pe.y) - this.Jh.y;
  0 == this.Jh.height && (this.Jh.height = 1);
  a = Math.min(this.ve.x, this.pe.x, this.pg.x, this.rg.x);
  b = Math.min(this.ve.y, this.pe.y, this.pg.y, this.rg.y);
  this.k = new P(a, b, Math.max(this.ve.x, this.pe.x, this.pg.x, this.rg.x) - a, Math.max(this.ve.y, this.pe.y, this.pg.y, this.rg.y) - b)
};
function Vp() {
  Pp.apply(this)
}
A.e(Vp, Pp);
Vp.prototype.qe = function() {
  return new Up
};
function Wp() {
  Qp.apply(this)
}
A.e(Wp, Qp);
Wp.prototype.uh = function(a) {
  var b = new Vp;
  b.gh(a);
  b.Wj = a;
  return b
};
Wp.prototype.Ah = x("candlestick_series");
Wp.prototype.Rb = x("candlestick_style");
Wp.prototype.Qb = function() {
  return new Mp
};
function Xp() {
  Tn.apply(this)
}
A.e(Xp, Tn);
Xp.prototype.Ue = function(a, b) {
  var c = this.Rb();
  if(C(a, "style") || C(a, c)) {
    if(C(a, "style") && !C(a, c)) {
      Hf(this, b, F(a, "style"))
    }else {
      var d, f;
      C(a, "style") && (d = F(a, "style"));
      C(a, c) && (f = F(a, c), C(f, "parent") && (d = F(f, "parent")));
      c = Ve(b, c, f, d);
      H(c, "name", d);
      this.K = new $f;
      this.K.g(c)
    }
  }else {
    this.K = new $f, d = I(b.Ki, c), this.K.g(fc(d, "anychart_default")), d = this.K, b.Ti[c] || (b.Ti[c] = {}), b.Ti[c].anychart_default = d
  }
};
function Yp() {
  oo.apply(this)
}
A.e(Yp, oo);
z = Yp.prototype;
z.Or = p;
z.ek = function(a, b) {
  if(C(a, "style")) {
    if(C(a, "marker")) {
      H(a, "marker", F(a, "style"))
    }else {
      var c = {};
      H(c, "style", F(a, "style"));
      H(c, "#name#", "marker");
      H(a, "marker", c)
    }
  }
  this.gb.pm(F(a, "marker")) && (this.gb = this.gb.copy(), this.gb.Yi(F(a, "marker"), b), this.K = this.gb.ka());
  this.gb.yg(j)
};
z.p = function(a) {
  Yp.f.p.call(this, a);
  return this.D
};
z.SI = s();
function Zp(a) {
  a.Or = new O;
  a.o.Wb.transform(a, a.Vb, a.Or);
  a.o.eb.transform(a, a.hc, a.Or)
}
z.update = function() {
  Zp(this);
  this.Tn()
};
z.Wa = function() {
  Zp(this);
  this.Kz()
};
z.bg = function(a) {
  a.x = this.Or.x;
  a.y = this.Or.y
};
z.kn = function(a) {
  var b = this.Or;
  return Xc(a, new P(b.x, b.y, 1, 1))
};
function $p() {
  mo.apply(this)
}
A.e($p, mo);
$p.prototype.fn = x(q);
$p.prototype.qe = function() {
  return new Yp
};
$p.prototype.ek = function(a, b) {
  if(C(a, "style")) {
    if(C(a, "marker")) {
      H(a, "marker", F(a, "style"))
    }else {
      var c = {};
      H(c, "style", F(a, "style"));
      H(c, "#name#", "marker");
      H(a, "marker", c)
    }
  }
  this.gb.sh(F(a, "marker")) && (this.gb = this.gb.copy(), this.gb.Zi(F(a, "marker"), b), this.K = this.gb.ka());
  this.gb.yg(j)
};
function aq() {
  io.apply(this)
}
A.e(aq, io);
z = aq.prototype;
z.ek = function(a, b) {
  this.gb || (this.gb = this.Bm());
  a ? this.gb.Ue(a, b) : this.gb.rf(new $);
  this.gb.yg(j);
  this.K = this.gb.ka()
};
z.uh = function() {
  var a = new $p;
  a.gh(18);
  return a
};
z.Ah = x("marker_series");
z.Rb = x(p);
z.cc = x(p);
z.Bm = function() {
  return new Xp
};
function bq() {
}
A.e(bq, Nf);
bq.prototype.Ta = function() {
  this.$a.Ni(this.I)
};
function cq() {
}
A.e(cq, Of);
cq.prototype.Ta = function() {
  this.$a.Ni(this.I)
};
function dq() {
}
A.e(dq, Pf);
dq.prototype.Yc = function() {
  var a = this.j.ka();
  a.wn() && (this.Db = new bq, this.Db.p(this.j, this));
  a.wi() && (this.Fb = new cq, this.Fb.p(this.j, this))
};
dq.prototype.update = function(a, b) {
  b && this.j.hd();
  dq.f.update.call(this, a, b)
};
dq.prototype.createElement = function() {
  return this.j.r().ja()
};
dq.prototype.Ni = function(a) {
  a.setAttribute("d", gd(this.j.n()))
};
function eq() {
  $.call(this)
}
A.e(eq, Rf);
eq.prototype.lc = function() {
  return new dq
};
function fq() {
  jo.apply(this);
  this.P = new O;
  this.X = new O
}
A.e(fq, no);
z = fq.prototype;
z.P = p;
z.X = p;
z.oF = p;
z.GH = function(a, b) {
  var c = this.o.Wb, d = this.o.eb, f = J(a, "column"), g = J(a, "row");
  this.Ic = 0 === c.Ib() || 1 === c.Ib();
  this.Dc = c.Ef(f, b);
  this.mw = d.Ef(g, b);
  this.fH = this.Dc.na;
  this.oF = this.mw.na;
  this.zf = this.o.zf;
  this.Nr = this.Dc.na;
  this.Gb || this.Dc.Ed(this)
};
z.cl = function(a) {
  C(a, "y") && (this.Gb = isNaN(ko(this, F(a, "y"))));
  this.Gb = this.Gb || !C(a, "column") || !C(a, "row")
};
z.$h = function(a) {
  C(a, "y") && (this.ma = M(a, "y"))
};
z.p = function(a) {
  return fq.f.p.call(this, a)
};
z.hd = function() {
  var a = this.o.Wb, b = this.o.eb;
  a.transform(this, this.fH - 0.5, this.P);
  a.transform(this, this.fH + 0.5, this.X);
  b.transform(this, this.oF - 0.5, this.P);
  b.transform(this, this.oF + 0.5, this.X);
  a = Math.min(this.P.x, this.X.x);
  b = Math.min(this.P.y, this.X.y);
  this.k = new P(a, b, Math.max(this.P.x, this.X.x) - a, Math.max(this.P.y, this.X.y) - b)
};
z.Cb = function() {
  this.b.add("%Column", 0);
  this.b.add("%Row", 0)
};
z.Pa = function(a) {
  switch(a) {
    case "%Column":
    ;
    case "%Row":
      return 2
  }
  return fq.f.Pa.call(this, a)
};
z.Se = function() {
  this.b.add("%Column", this.Dc.getName());
  this.b.add("%Row", this.mw.getName());
  fq.f.Se.call(this)
};
function gq() {
  lo.apply(this)
}
A.e(gq, mo);
gq.prototype.qe = function() {
  return new fq
};
gq.prototype.fn = x(q);
function hq() {
  go.apply(this)
}
A.e(hq, io);
hq.prototype.uh = function() {
  var a = new gq;
  a.gh(23);
  a.Wj = 23;
  return a
};
hq.prototype.Ah = x("heat_map");
hq.prototype.Rb = x("heat_map_style");
hq.prototype.Qb = function() {
  return new eq
};
function iq() {
}
A.e(iq, Qo);
iq.prototype.Ta = function() {
  this.j.Tj();
  var a = this.j.Ua;
  if(a && !(2 > a.length)) {
    for(var b = this.j.o.UF, c = 0;c < a.length;c++) {
      var d = b, f = a[c].x, g = a[c].y;
      switch(d.Nv) {
        case 3:
          if(d.Wn == f && d.Xn == g) {
            break
          }
          var k = d, l = d.Zp, n = d.Wn, m = d.Xn, o = f, v = new O;
          v.x = l + (n - l) * k.CA;
          v.y = m;
          l = new O;
          l.x = o - (o - n) * k.CA;
          l.y = m;
          o = new O;
          o.x = n;
          o.y = m;
          k.o.da.push([v, l, o]);
          k.na++;
          d.Zp = d.Wn;
          d.$A = d.Xn;
          d.Wn = f;
          d.Xn = g;
          break;
        case 2:
          if(d.Wn == f && d.Xn == g) {
            break
          }
          d.Zp = d.Wn;
          d.$A = d.Xn;
          d.Wn = f;
          d.Xn = g;
          d.Nv = 3;
          break;
        case 1:
          if(d.Zp == f && d.$A == g) {
            break
          }
          k = d;
          n = d.$A;
          m = new O;
          m.x = f - (f - d.Zp) * k.CA;
          m.y = n;
          k.o.da.push(m);
          k.na++;
          d.Wn = f;
          d.Xn = g;
          d.Nv = 2;
          break;
        case 0:
          d.Zp = f, d.$A = g, d.Nv = 1
      }
    }
    this.j.o.Fe() - 1 == this.j.na && (a = new O, a.x = b.Zp + (b.Wn - b.Zp) * b.CA, a.y = b.Xn, b.o.da.push(a));
    this.I.setAttribute("d", this.$a.yh())
  }
};
function jq() {
}
A.e(jq, Ro);
jq.prototype.Yc = function() {
  this.If = new iq;
  this.If.p(this.j, this)
};
jq.prototype.createElement = function() {
  return this.j.r().ja()
};
jq.prototype.yh = function() {
  var a = this.j.r(), b = this.j.Ua;
  if(b) {
    var c = b.length;
    if(!(2 > c)) {
      var d = this.j.o.da, f, g, k;
      k = this.j.na;
      var l = function(a) {
        if(a.$o) {
          return q
        }
        var b = a.o.ba[k + 1];
        return b && b.$o || 0 < k && (a = a.o.ba[k - 1]) && a.$o ? q : j
      };
      2 == c ? (k == this.j.o.Fe() - 1 ? (f = new O, f.x = d[2 * k - 1].x, f.y = d[2 * k - 1].y, c = S(b[0].x, b[0].y)) : (c = S(b[0].x, b[0].y), f = new O, f.x = d[k].x, f.y = d[k].y), l(this.j) && (c += a.WE(f.x, f.y, b[1].x, b[1].y))) : (f = d[2 * k - 1][0], g = d[2 * k - 1][1], d = d[2 * k - 1][2], c = S(b[0].x, b[0].y), l(this.j) && (c += a.WE(f.x, f.y, d.x, d.y), c += a.WE(g.x, g.y, b[2].x, b[2].y)));
      return c
    }
  }
};
function kq() {
  $.call(this)
}
A.e(kq, Po);
kq.prototype.lc = function() {
  return new jq
};
function lq() {
  lo.apply(this);
  this.da = [];
  this.UF = new mq(this)
}
A.e(lq, Vo);
lq.prototype.da = p;
lq.prototype.UF = p;
lq.prototype.Ug = s();
lq.prototype.An = function() {
  this.da = [];
  var a = this.UF;
  a.Nv = 0;
  a.na = 0
};
function mq(a) {
  this.Nv = 0;
  this.CA = 0.5;
  this.na = 0;
  this.o = a
}
function nq() {
  go.apply(this)
}
A.e(nq, Wo);
nq.prototype.uh = function(a) {
  var b;
  switch(a) {
    case 3:
      b = new lq
  }
  b.gh(a);
  b.Wj = 3;
  return b
};
nq.prototype.Qb = function() {
  return new kq
};
function rf() {
  Fk.call(this);
  this.bk = 0;
  this.me = [];
  this.Qf = [];
  this.Us = {};
  this.fg = [];
  this.hw = {};
  this.aL = [];
  this.bL = [];
  this.Bq = [];
  this.qx = {}
}
A.e(rf, Fk);
z = rf.prototype;
z.Zr = function(a) {
  rf.f.Zr.call(this, a);
  "heatmap" == a && (this.bk = 23);
  this.Ic = -1 != a.indexOf("horizontal");
  this.Ly = "scatter" == a
};
z.Ly = p;
z.fo = p;
z.BB = p;
z.Xt = u("BB");
z.Ic = p;
z.Vf = u("Ic");
z.yD = q;
z.CD = q;
z.DD = q;
z.wD = q;
z.me = p;
z.Qf = p;
z.fg = p;
z.Us = p;
z.hw = p;
z.fv = p;
z.gv = p;
z.Fw = p;
z.Ew = p;
z.Gd = function() {
  return this.N.Gd()
};
z.aL = p;
z.bL = p;
z.Bq = p;
z.qx = p;
z.br = function() {
  return new kl
};
z.cy = function(a) {
  switch(a) {
    case 2:
    ;
    case 5:
    ;
    case 4:
      return 2;
    case 6:
    ;
    case 7:
    ;
    case 9:
    ;
    case 8:
      return 6;
    default:
      return a
  }
};
z.jx = function(a) {
  switch(a) {
    case 0:
      return new wo;
    case 1:
      return new zo;
    case 12:
      return new Ko;
    case 3:
      return new nq;
    case 2:
    ;
    case 5:
    ;
    case 4:
      return new Wo;
    case 6:
    ;
    case 7:
    ;
    case 9:
    ;
    case 8:
      return new mp;
    case 10:
    ;
    case 11:
      return new vp;
    case 13:
      return new Tp;
    case 14:
      return new Wp;
    case 18:
      return new aq;
    case 23:
      return new hq
  }
  return p
};
z.Jo = function(a) {
  if("heatmap" == this.wp) {
    return 23
  }
  if(a == p) {
    return this.bk
  }
  switch(a) {
    case "bubble":
      return 12;
    case "bar":
      return 0;
    case "rangebar":
      return 1;
    case "rangearea":
      return 10;
    case "candlestick":
      return 14;
    case "ohlc":
      return 13;
    case "line":
      return 2;
    case "steplineforward":
      return 4;
    case "steplinebackward":
      return 5;
    case "spline":
      return 3;
    case "area":
      return 6;
    case "splinearea":
      return 7;
    case "stepareaforward":
    ;
    case "steplineforwardarea":
      return 8;
    case "stepareabackward":
    ;
    case "steplinebackwardarea":
      return 9;
    case "rangesplinearea":
      return 11;
    case "marker":
      return 18;
    default:
      return this.bk
  }
};
z.fC = function(a, b) {
  rf.f.fC.call(this, a, b);
  this.wD = this.DD = this.CD = this.yD = q;
  var c = F(a, "axes");
  oq(this, F(c, "x_axis"), j, b, "x_axis");
  oq(this, F(c, "y_axis"), j, b, "y_axis");
  if(C(c, "extra")) {
    var d = F(c, "extra");
    if(C(d, "x_axis")) {
      for(var f = I(d, "x_axis"), c = 0;c < f.length;c++) {
        oq(this, f[c], q, b, "x_axis")
      }
    }
    if(C(d, "y_axis")) {
      d = I(d, "y_axis");
      for(c = 0;c < d.length;c++) {
        oq(this, d[c], q, b, "y_axis")
      }
    }
  }
};
function oq(a, b, c, d, f) {
  if(!(!c && !C(b, "name") || "x_axis" != f && "y_axis" != f)) {
    var g = C(b, "scale") && "linear" == N(F(b, "scale"), "type"), k = "heatmap" == a.wp, l = "categorizedbyserieshorizontal" == a.wp || "categorizedbyseriesvertical" == a.wp, n = "categorizedhorizontal" == a.wp || "categorizedbyserieshorizontal" == a.wp, f = "x_axis" == f, g = !k && (a.Ly || !f || g) ? new Xm : !k && l ? new kn : new jn;
    g.Ul(a);
    k = p;
    if(C(b, "position") && (k = Wb(b, "position"), "bottom" == k || "right" == k)) {
      H(b, "position", "opposite"), k = "opposite"
    }
    k = "opposite" == k || k == p && !c;
    l = f && (a.Ly || !n) || !f && n;
    n && l && (k = !k);
    g.dn = f;
    g.La = g.wH(F(b, "scale"));
    C(b, "scale") && g.La.g(F(b, "scale"));
    g.Ng = q;
    l ? k ? (g.qb(2), a.DD || (g.Ng = j, a.DD = j)) : (g.qb(3), a.wD || (g.Ng = j, a.wD = j)) : k ? (g.qb(1), a.CD || (g.Ng = j, a.CD = j)) : (g.qb(0), a.yD || (g.Ng = j, a.yD = j));
    var k = a.Us.primary, m = a.hw.primary, m = !c && (f ? k : m).Ib() != g.Ib();
    g.Xf = m;
    l = l ? a.fv : a.gv;
    m = a.Ew;
    g.Jx = a.Fw;
    g.tp = l;
    g.Xg = m;
    g.ZB(fm);
    g.g(b, d);
    b = c ? "primary" : N(b, "name");
    g.Sl(b);
    n && f && (g.fa().Td = !g.fa().Nb());
    n && !f && 1 == k.Ib() && (g.fa().Td = !g.fa().Nb());
    f ? (a.Us[b] = g, a.Qf.push(g)) : (a.hw[b] = g, a.fg.push(g));
    a.me.push(g)
  }
}
function pq(a, b) {
  return b != p && a.Us[b] ? a.Us[b] : pq(a, "primary")
}
function qq(a, b) {
  return b != p && a.hw[b] ? a.hw[b] : qq(a, "primary")
}
z.vx = function(a, b) {
  var c = pq(this, C(b, "x_axis") ? N(b, "x_axis") : p), d = qq(this, C(b, "y_axis") ? N(b, "y_axis") : p);
  a.Wb = c;
  a.eb = d;
  c.PB(a, this.F)
};
z.yk = function() {
  for(var a = 0;a < this.me.length;a++) {
    this.me[a].yk(this.Ua)
  }
};
z.Ig = u("fo");
z.Xt = u("BB");
z.Vx = function(a) {
  return a.ka() && a.ka().wa && 10 == a.ka().wa.Df() ? this.Vf() ? this.gv : this.fv : rf.f.Vx.call(this, a)
};
z.ry = function() {
  this.D.ia(this.Ew)
};
z.tl = function(a) {
  rf.f.tl.call(this, a);
  this.fv = new W(a);
  this.gv = new W(a);
  this.Fw = new W(a);
  this.Ew = new W(a)
};
z.p = function(a, b, c) {
  a.Ca();
  var d, f, g = this.me.length;
  for(d = 0;d < g;d++) {
    if(f = this.me[d], (isNaN(f.La.ld) || isNaN(f.La.kd)) && !f.dn && "primary" != f.getName()) {
      var k = qq(this);
      f.Gc(k.fa().ld);
      f.Gc(k.fa().kd)
    }
  }
  d = this.me.length;
  b = rf.f.p.call(this, a, b, c);
  a = a.Ca();
  b.ia(this.fv);
  b.ia(this.gv);
  b.ia(this.Fw);
  for(c = 0;c < d;c++) {
    if(g = this.me[c], Mm(g), g.Nl = 0, g.xp) {
      for(f = 0;f < g.xp.length;f++) {
        k = g.xp[f][0], g.Nl = Math.max(g.Nl, g.J.nd(k.n(g.xp[f][1], k.ka().wa, g.aa.fb())) + k.ka().wa.ga())
      }
    }
  }
  rq(this, a);
  sq(this);
  for(c = 0;c < d;c++) {
    g = this.me[c], Mm(g)
  }
  return b
};
function sq(a) {
  td(a.xt, a.fo);
  td(a.Yy, a.fo);
  td(a.uk, a.fo)
}
z.qa = function() {
  tq(this);
  rf.f.qa.call(this)
};
function tq(a) {
  var b, c;
  b = 0;
  for(c = a.fg.length;b < c;b++) {
    Vm(a.fg[b])
  }
  b = 0;
  for(c = a.Qf.length;b < c;b++) {
    Vm(a.Qf[b])
  }
  b = 0;
  for(c = a.fg.length;b < c;b++) {
    Um(a.fg[b])
  }
  b = 0;
  for(c = a.Qf.length;b < c;b++) {
    Um(a.Qf[b])
  }
  for(b = 0;b < a.fg.length;b++) {
    Tm(a.fg[b])
  }
  b = 0;
  for(c = a.Qf.length;b < c;b++) {
    Tm(a.Qf[b])
  }
  b = 0;
  for(c = a.fg.length;b < c;b++) {
    Sm(a.fg[b]), a.fg[b].yo()
  }
  b = 0;
  for(c = a.Qf.length;b < c;b++) {
    Sm(a.Qf[b]), a.Qf[b].yo()
  }
  for(b = 0;b < a.fg.length;b++) {
    Rm(a.fg[b]), a.fg[b].uC()
  }
  b = 0;
  for(c = a.Qf.length;b < c;b++) {
    Rm(a.Qf[b]), a.Qf[b].uC()
  }
  b = 0;
  for(c = a.me.length;b < c;b++) {
    a.me[b].yx();
    var d = a.me[b];
    d.Ma && d.He && d.He.qa(a.r())
  }
}
function rq(a, b) {
  var c, d = 0, f = a.me.length, b = b.Ca(), g = b.Ca();
  g.x = 0;
  g.y = 0;
  g.width *= 0.75;
  g.height *= 0.75;
  for(d = 0;d < f;d++) {
    c = a.me[d], Nm(c, g), Pm(c)
  }
  g.width /= 0.75;
  g.height /= 0.75;
  for(var k = g.Ca(), d = 0;d < f;d++) {
    c = a.me[d], c.Ma && c.J.bo(g, c.Nm + c.JA + c.Nl + c.Cr)
  }
  c = g.Ca();
  for(d = 0;d < f;d++) {
    Om(a.me[d], g)
  }
  for(d = 0;d < f;d++) {
    var l = a.me[d];
    Nm(l, g);
    l.yd = l.Ma && l.O ? l.Cr + l.Nm + l.JA + Math.max(xm(l), l.vk) : l.Cr + l.Nm + l.JA + l.vk
  }
  g = c;
  for(d = 0;d < f;d++) {
    Om(a.me[d], g)
  }
  for(var l = g.x - k.x, n = g.y - k.y, m = k.width - g.width - l, o = k.height - g.height - n, d = 0;d < f;d++) {
    c = a.me[d];
    var v = k.Ca();
    c.BK(v, g);
    c = v.x - k.x;
    var w = v.y - k.y, y = k.width - v.width - c, v = k.height - v.height - w;
    c > l && (l = c);
    y > m && (m = y);
    w > n && (n = w);
    v > o && (o = v)
  }
  g.x = k.x + l;
  g.y = k.y + n;
  g.width = k.width - l - m;
  g.height = k.height - n - o;
  for(d = 0;d < f;d++) {
    a.me[d].Rc(g)
  }
  for(d = m = n = l = k = 0;d < f;d++) {
    c = a.me[d];
    o = 0;
    switch(c.Ib()) {
      case 0:
        o = k;
        k += c.nd();
        break;
      case 1:
        o = l;
        l += c.nd();
        break;
      case 2:
        o = n;
        n += c.nd();
        break;
      case 3:
        o = m, m += c.nd()
    }
    c.Ng || (o += c.Gx);
    c.Ei(o);
    c.Rc(g)
  }
  a.fo = g;
  b.x += g.x;
  b.y += g.y;
  b.width = g.width;
  b.height = g.height;
  a.BB = b
}
z.om = function(a) {
  a.Ca();
  a = a.Ca();
  rq(this, a);
  rf.f.om.call(this, a)
};
z.Jq = function() {
  this.Fw.clear();
  this.fv.clear();
  this.gv.clear();
  this.Ew.clear();
  rf.f.Jq.call(this);
  tq(this);
  sq(this)
};
z.Cb = function() {
  rf.f.Cb.call(this);
  this.b.add("%DataPlotXMax", -Number.MAX_VALUE);
  this.b.add("%DataPlotXMin", Number.MAX_VALUE);
  this.b.add("%DataPlotXSum", 0);
  this.b.add("%DataPlotXAverage", 0);
  this.b.add("%DataPlotYRangeMin", Number.MAX_VALUE);
  this.b.add("%DataPlotYRangeMax", -Number.MAX_VALUE);
  this.b.add("%DataPlotYRangeSum", 0)
};
z.Pa = function(a) {
  switch(a) {
    case "%DataPlotXMax":
    ;
    case "%DataPlotXMin":
    ;
    case "%DataPlotXSum":
    ;
    case "%DataPlotXAverage":
    ;
    case "%DataPlotYAverage":
    ;
    case "%DataPlotYRangeMin":
    ;
    case "%DataPlotYRangeMax":
    ;
    case "%DataPlotYRangeSum":
      return 2
  }
  return rf.f.Pa.call(this, a)
};
z.Na = function(a) {
  oe(this.b, "%DataPlotXAverage") || this.b.add("%DataPlotXAverage", this.b.get("%DataPlotXSum") / this.b.get("%DataPlotPointCount"));
  oe(this.b, "%DataPlotYAverage") || this.b.add("%DataPlotYAverage", this.b.get("%DataPlotYSum") / this.b.get("%DataPlotPointCount"));
  return rf.f.Na.call(this, a)
};
z.mb = function(a) {
  a = rf.f.mb.call(this, a);
  a.YRangeMax = this.b.get("%DataPlotRangeMax");
  a.YRangeMin = this.b.get("%DataPlotRangeMin");
  a.YRangeSum = this.b.get("%DataPlotRangeSum");
  a.XSum = this.b.get("%DataPlotXSum");
  a.XMax = this.b.get("%DataPlotXMax");
  a.XMin = this.b.get("%DataPlotXMin");
  a.XAverage = this.b.get("%DataPlotXSum") / this.b.get("%DataPlotPointCount");
  a.Categories = [];
  var b, c = this.Bq.length;
  for(b = 0;b < c;b++) {
    var d = this.Bq[b];
    d && a.Categories.push(d.mb())
  }
  a.XAxes = {};
  a.YAxes = {};
  var d = this.Qf.length, f = this.fg.length;
  for(b = 0;b < d;b++) {
    c = this.Qf[b], a.XAxes[c.getName()] = c.mb()
  }
  for(b = 0;b < f;b++) {
    c = this.fg[b], a.YAxes[c.getName()] = c.mb()
  }
  return a
};
z.lk = function(a, b) {
  var c, d;
  for(d in this.Vk) {
    if(c = this.Vk[d], c.getName == a) {
      break
    }
  }
  c && (b ? c.ah() : c.dh())
};
function uq(a, b) {
  Se.call(this, a, b)
}
A.e(uq, Qf);
z = uq.prototype;
z.Kb = p;
z.UE = p;
z.QD = p;
z.Gt = function(a, b) {
  var c = {}, d = {}, f = [], g = {}, k = {}, l = {};
  g.position = "0";
  g.opacity = b;
  g.color = "LightColor(" + a + ")";
  f.push(g);
  k.position = 0.95;
  k.opacity = b;
  k.color = "DarkColor(" + a + ")";
  f.push(k);
  l.position = 1;
  l.opacity = b;
  l.color = "DarkColor(" + a + ")";
  f.push(l);
  d.type = "radial";
  d.angle = -145;
  d.focal_point = "0.5";
  d.key = f;
  c.enabled = j;
  c.type = "gradient";
  c.gradient = d;
  this.Hb = new Jd;
  this.Hb.g(c);
  this.Kb = new Ld;
  this.Kb.g(c);
  this.Kb.ad = 0
};
z.g = function(a) {
  var a = uq.f.g.call(this, a), b = F(a, "fill"), b = C(b, "opacity") ? F(b, "opacity") : 1;
  this.Gt(F(F(a, "fill"), "color"), b);
  var c = {}, d = {}, f = [], g = {}, k = {}, l = {};
  g.position = "0";
  g.opacity = "0";
  g.color = "Black";
  f.push(g);
  k.position = 0.95;
  k.opacity = "0";
  k.color = "Black";
  f.push(k);
  l.position = 1;
  l.opacity = 100 / 255 * b;
  l.color = "Black";
  f.push(l);
  d.type = "radial";
  d.angle = "0";
  d.focal_point = "0";
  d.key = f;
  c.enabled = j;
  c.type = "gradient";
  c.gradient = d;
  this.UE = new Jd;
  this.UE.g(c);
  this.QD = new Jd;
  return a
};
function vq() {
}
A.e(vq, Ne);
z = vq.prototype;
z.Yh = p;
z.Ta = function() {
  this.I.setAttribute("d", this.j.nl())
};
z.createElement = function() {
  return this.$a.createElement()
};
z.oh = function(a) {
  var b = this.j.r();
  this.Yh && -1 == this.Yh.indexOf("fill:none") && Ec(b.cr, this.Yh);
  this.Yh = "";
  this.vf(a)
};
z.vf = function(a) {
  var b = this.j.r(), c = "", d = this.j.Ba().Ba(), f = this.j.n();
  this.Yh = c += Kd(a.mc(), b, f, d, j);
  this.I.setAttribute("style", c)
};
z.ud = s();
function wq() {
}
A.e(wq, Nf);
wq.prototype.Ta = function() {
  this.I.setAttribute("d", this.j.nl())
};
function xq() {
}
A.e(xq, Of);
xq.prototype.Ta = function() {
  this.I.setAttribute("d", this.j.nl())
};
function yq() {
}
A.e(yq, Pf);
yq.prototype.Yc = function() {
  var a = this.j.ka();
  a.wn() && (this.Db = new wq, this.Db.p(this.j, this));
  a.wi() && (this.Fb = new xq, this.Fb.p(this.j, this))
};
yq.prototype.createElement = function() {
  return this.j.r().ja()
};
yq.prototype.update = function(a, b) {
  b && this.j.Tj();
  this.Db && this.Db.update(a, b);
  this.Fb && this.Fb.update(a, b)
};
yq.prototype.oh = s();
function zq() {
}
A.e(zq, vq);
function Aq() {
}
A.e(Aq, vq);
Aq.prototype.vf = function(a) {
  var b = this.j.r(), c = "", d = this.j.Ba().Ba(), f = this.j.n();
  this.Yh = c += Kd(a.UE, b, f, d, j);
  this.I.setAttribute("style", c)
};
function Bq() {
}
A.e(Bq, vq);
Bq.prototype.vf = function(a) {
  var b = this.j.r(), c = "", d = this.j.Ba().Ba(), f = this.j.n(), g = this.j.o, k = g.Tb, l = g.Qa, g = g.na;
  if(0 < k) {
    var k = k / l, l = {}, n = {}, m = [], o = {}, v = {}, w = {};
    o.position = k;
    o.opacity = 100 / 255;
    o.color = "Black";
    m.push(o);
    v.position = 0 == g ? k + 0.1 : k + 0.05;
    v.opacity = "0";
    v.color = "Black";
    m.push(v);
    w.position = 1;
    w.opacity = "0";
    w.color = "Black";
    m.push(w);
    n.type = "radial";
    n.angle = "0";
    n.focal_point = "0";
    n.key = m;
    l.enabled = j;
    l.type = "gradient";
    l.gradient = n;
    a.QD.g(l);
    c += Kd(a.QD, b, f, d, j)
  }else {
    c += "fill:none;"
  }
  this.Yh = c;
  this.I.setAttribute("style", c)
};
function Cq() {
}
A.e(Cq, Re);
z = Cq.prototype;
z.fb = function() {
  return this.j.fb()
};
z.tm = p;
z.Cn = p;
z.cn = p;
z.Yc = function() {
  this.tm = new zq;
  this.tm.p(this.j, this);
  this.Cn = new Aq;
  this.Cn.p(this.j, this);
  this.cn = new Bq;
  this.cn.p(this.j, this)
};
z.ff = function() {
  var a = this.fb();
  this.tm && a.appendChild(this.tm.I);
  this.Cn && a.appendChild(this.Cn.I);
  this.cn && a.appendChild(this.cn.I)
};
z.createElement = function() {
  return this.j.r().ja()
};
z.update = function(a, b) {
  b && this.j.Tj();
  this.tm && this.tm.update(a, b);
  this.Cn && this.Cn.update(a, b);
  this.cn && this.cn.update(a, b)
};
z.oh = function(a) {
  this.tm && this.tm.oh(a);
  this.Cn && this.Cn.oh(a);
  this.cn && this.cn.oh(a)
};
function Dq() {
  Se.call(this, h, h)
}
A.e(Dq, uq);
function Eq() {
  $.call(this)
}
A.e(Eq, Rf);
Eq.prototype.p = function(a) {
  return Eq.f.p.call(this, a)
};
Eq.prototype.lc = function() {
  return new Cq
};
Eq.prototype.cc = function() {
  return Dq
};
function Fq() {
  $.call(this)
}
A.e(Fq, Rf);
Fq.prototype.lc = function() {
  return new yq
};
Fq.prototype.cc = function() {
  return Qf
};
function Gq() {
}
A.e(Gq, Qe);
Gq.prototype.mc = function(a) {
  return a.UC
};
Gq.prototype.Ie = x(j);
Gq.prototype.Ta = function() {
  var a;
  a = this.j;
  var b = "";
  if(a.xh) {
    for(var c = 0;c < a.xh.li();c++) {
      b += a.xh.qa(c)
    }
    a = b + " Z"
  }else {
    a = p
  }
  a != p && " Z" != a && "" != a ? this.I.setAttribute("d", a) : this.I.removeAttribute("d")
};
function Hq() {
}
A.e(Hq, Qe);
Hq.prototype.mc = function(a) {
  return a.tB
};
Hq.prototype.Ie = x(j);
Hq.prototype.Ta = function() {
  if(this.j.Pj) {
    var a;
    a = this.j;
    var b = "";
    if(a.Pj) {
      for(var c = 0;c < a.Pj.li();c++) {
        b += a.Pj.qa(c)
      }
      a = b + " Z"
    }else {
      a = p
    }
    a != p && " Z" != a ? this.I.setAttribute("d", a) : this.I.removeAttribute("d")
  }
};
function Iq() {
}
A.e(Iq, Qe);
Iq.prototype.mc = function(a) {
  return a.nG
};
Iq.prototype.Ie = x(j);
Iq.prototype.Ta = function() {
  var a;
  a = this.j;
  var b = a.o.Qa, c = a.o.oj, d = a.o.Tb, f = a.o.Qo, g = a.jb % 360 * Math.PI / 180, k = a.tc % 360 * Math.PI / 180;
  a.jb % 360 == a.tc % 360 && (k = g = 0);
  var l = a.ae(), n = l.x + d * Math.cos(g), m = l.y + f * Math.sin(g), o = l.x + d * Math.cos(k), v = l.y + f * Math.sin(k), w = l.x + b * Math.cos(g), g = l.y + c * Math.sin(g), y = l.x + b * Math.cos(k), k = l.y + c * Math.sin(k), E = l = q;
  180 <= a.kh && (l = !l, E = !E);
  if(a.nh != p) {
    var V = a.nh.r(), R;
    R = "" + S(n, m);
    R += V.ub(d, l, j, o, v, f);
    R += U(y, k);
    R += V.ub(b, E, q, w, g, c);
    R += U(n, m);
    w == y && g == k && a.jb % 360 == a.tc % 360 && (R = "" + S(n, m), R += V.ub(d, 1, 1, o, v - 0.001, f), R += S(w, g), R += V.ub(b, 1, 0, y, k + 0.001, c), R += U(n, m));
    a = R + " Z"
  }else {
    a = p
  }
  a != p && " Z" != a && "" != a ? this.I.setAttribute("d", a) : this.I.removeAttribute("d")
};
function Jq() {
}
A.e(Jq, Qe);
Jq.prototype.mc = function(a) {
  return a.RF
};
Jq.prototype.Ie = x(j);
Jq.prototype.Ta = function() {
  if(this.j.xh != p) {
    var a;
    a = this.j;
    for(var b = a.ae(), c = "", d = a.o.Qa, f = a.o.oj, g = 0.03 * a.ca().Qa, k, l, n, m = 0;m < a.xh.li();m++) {
      var o = a.xh.Pb[m] - 0.275, v = a.xh.Go(m) + 0.275;
      for(k = o;k <= v;k++) {
        l = g / 2 * Math.abs(Math.sin(k * Math.PI / 180)), n = g / 2 * Math.abs(Math.sin(k * Math.PI / 180)), c = k == o ? c + S(b.x + Uc(d - l, k), b.y + Vc(f - n, k)) : c + U(b.x + Uc(d - l, k), b.y + Vc(f - n, k))
      }
      l = g / 2 * Math.abs(Math.sin(v * Math.PI / 180));
      n = g / 2 * Math.abs(Math.sin(v * Math.PI / 180));
      c += U(b.x + Uc(d - l, v), b.y + Vc(f - n, v));
      for(k = v;k >= o;k--) {
        l = g / 2 * Math.abs(Math.sin(k * Math.PI / 180)), n = g / 2 * Math.abs(Math.sin(k * Math.PI / 180)), c += U(b.x + Uc(d + l, k), b.y + Vc(f + n, k))
      }
      l = g / 2 * Math.abs(Math.sin(o * Math.PI / 180));
      n = g / 2 * Math.abs(Math.sin(o * Math.PI / 180));
      c += U(b.x + Uc(d - l, o), b.y + Vc(f - n, o));
      c += " Z"
    }
    a = c;
    a != p && " Z" != a && "" != a ? this.I.setAttribute("d", a) : this.I.removeAttribute("d")
  }
};
function Kq() {
}
A.e(Kq, Qe);
Kq.prototype.mc = function(a) {
  return a.VF
};
Kq.prototype.Ie = x(j);
Kq.prototype.Ta = function() {
  var a = this.j.Ii != p ? Lq(this.j, this.j.jb) : p;
  a != p && " Z" != a && "" != a ? this.I.setAttribute("d", a) : this.I.removeAttribute("d")
};
function Mq() {
}
A.e(Mq, Qe);
Mq.prototype.mc = function(a) {
  return a.AC
};
Mq.prototype.Ie = x(j);
Mq.prototype.Ta = function() {
  var a = this.j.bi != p ? Lq(this.j, this.j.tc) : p;
  a != p && " Z" != a && "" != a ? this.I.setAttribute("d", a) : this.I.removeAttribute("d")
};
function Nq() {
}
A.e(Nq, Re);
z = Nq.prototype;
z.ji = p;
z.Uh = p;
z.Ii = p;
z.bi = p;
z.nh = p;
z.ds = p;
z.Yc = function() {
  this.ji = new Gq;
  this.ji.p(this.j, this);
  this.Uh = new Hq;
  this.Uh.p(this.j, this);
  this.Ii = new Kq;
  this.Ii.p(this.j, this);
  this.bi = new Mq;
  this.bi.p(this.j, this);
  this.nh = new Iq;
  this.nh.p(this.j, this);
  this.ds = new Jq;
  this.ds.p(this.j, this);
  this.ds.I.setAttribute("filter", "url(#smileBlur)")
};
z.ff = function() {
  var a = this.j;
  if(a.nh) {
    var b = new W(a.r());
    b.appendChild(this.nh.I);
    var c = new W(a.r());
    c.appendChild(this.ds.I);
    a.nh.appendChild(b.I);
    a.nh.appendChild(c.I)
  }
  a.ji && a.ji.appendChild(this.ji.I);
  a.Uh && a.Uh.appendChild(this.Uh.I);
  a.Ii && a.Ii.appendChild(this.Ii.I);
  a.bi && a.bi.appendChild(this.bi.I)
};
z.update = function(a, b) {
  b && this.j.Tj();
  this.nh.update(a, b);
  this.ds.update(a, b);
  this.ji && this.ji.update(a, b);
  this.Uh && this.Uh.update(a, b);
  this.Ii.update(a, b);
  this.bi.update(a, b)
};
z.oh = function(a) {
  this.nh.oh(a);
  this.ds.oh(a);
  this.ji && this.ji.oh(a);
  this.Uh && this.Uh.oh(a);
  this.Ii.oh(a);
  this.bi.oh(a)
};
z.Ta = s();
z.createElement = function() {
  return this.j.r().ja()
};
function Oq(a, b) {
  Se.call(this, a, b)
}
A.e(Oq, Qf);
z = Oq.prototype;
z.UC = p;
z.tB = p;
z.VF = p;
z.AC = p;
z.nG = p;
z.RF = p;
z.g = function(a) {
  var a = Oq.f.g.call(this, a), b = F(F(a, "fill"), "color"), c = [], d = {}, f = {}, g = {}, k = {};
  d.position = "0";
  d.opacity = "1";
  d.color = b;
  c.push(d);
  f.position = 1;
  f.opacity = "1";
  f.color = "Blend(" + b + ", DarkColor(" + b + "),0.7)";
  c.push(f);
  g.type = "linear";
  g.angle = "-50";
  g.key = c;
  k.enabled = j;
  k.type = "gradient";
  k.gradient = g;
  this.nG = new Jd;
  this.nG.g(k);
  var c = [], d = {}, f = {}, g = {}, k = {}, l = {}, c = [];
  d.position = "0";
  d.opacity = 1;
  d.color = b;
  c.push(d);
  f.position = 0.19;
  f.opacity = 1;
  f.color = "Blend(DarkColor(" + b + "),LightColor(" + b + "),0.3)";
  c.push(f);
  g.position = 1;
  g.opacity = 1;
  g.color = "Blend(" + b + ",DarkColor(" + b + "),0.3)";
  c.push(g);
  k.type = "linear";
  k.angle = "45";
  k.key = c;
  l.enabled = j;
  l.type = "gradient";
  l.gradient = k;
  this.UC = new Jd;
  this.UC.g(l);
  var c = [], d = {}, f = {}, g = {}, k = {}, l = {}, n = {};
  d.position = 0;
  d.opacity = 0.29;
  d.color = "white";
  c.push(d);
  f.position = 0.28;
  f.opacity = 0.8;
  f.color = "white";
  c.push(f);
  g.position = 0.72;
  g.opacity = 0.16;
  g.color = "white";
  c.push(g);
  k.position = 1;
  k.opacity = "0";
  k.color = "white";
  c.push(k);
  l.type = "linear";
  l.angle = "0";
  l.key = c;
  n.enabled = j;
  n.type = "gradient";
  n.gradient = l;
  this.RF = new Jd;
  this.RF.g(n);
  c = {enabled:j, type:"solid"};
  c.color = "Blend(" + b + ", DarkColor(" + b + "), 0.35)";
  this.tB = new Jd;
  this.tB.g(c);
  this.VF = new Jd;
  this.VF.g(c);
  this.AC = new Jd;
  this.AC.g(c);
  return a
};
function Pq() {
  $.call(this)
}
A.e(Pq, Rf);
Pq.prototype.p = function(a) {
  return Pq.f.p.call(this, a)
};
Pq.prototype.lc = function() {
  return new Nq
};
Pq.prototype.cc = function() {
  return Oq
};
function Qq(a) {
  this.Pb = [];
  this.$b = [];
  this.bs = [];
  this.F = a
}
z = Qq.prototype;
z.Pb = p;
z.$b = p;
z.Go = function(a) {
  return this.$b[a]
};
z.j = p;
z.bs = p;
z.li = function() {
  return this.bs.length
};
z.af = p;
z.F = p;
z.p = function() {
  A.xa()
};
z.qa = function() {
  A.xa()
};
z.li = function() {
  return this.bs.length
};
z.fb = function(a) {
  return this.bs[a]
};
function Rq(a, b) {
  var c = a.Pb[b], d = a.$b[b];
  d < c && (d += 360);
  return(c + d) / 2
}
z.isEnabled = function() {
  A.xa()
};
z.cj = function(a, b) {
  return 0 <= a && 0 <= b ? 1 : 0 >= a && 0 <= b ? 2 : 0 >= a && 0 > b ? 3 : 4
};
function Sq(a) {
  Qq.call(this, a)
}
A.e(Sq, Qq);
Sq.prototype.p = function(a) {
  this.j = a;
  var b = a.jb, a = a.Go(), c = Math.sin(b * Math.PI / 180), d = Math.sin(a * Math.PI / 180), f = Math.cos(b * Math.PI / 180), g = Math.cos(a * Math.PI / 180), c = this.cj(f, c), d = this.cj(g, d);
  if(1 == c) {
    switch(d) {
      case 1:
        f <= g && (this.Pb.push(180), this.$b.push(360));
        break;
      case 3:
        this.Pb.push(180);
        this.$b.push(a);
        break;
      case 4:
        this.Pb.push(180), this.$b.push(a), this.af = j
    }
  }else {
    if(2 == c) {
      switch(d) {
        case 1:
          this.Pb.push(180);
          this.$b.push(360);
          this.af = j;
          break;
        case 2:
          f <= g && (this.Pb.push(180), this.$b.push(360), this.af = j);
          break;
        case 3:
          this.Pb.push(180);
          this.$b.push(a);
          break;
        case 4:
          this.Pb.push(180), this.$b.push(a), this.af = j
      }
    }else {
      if(3 == c) {
        switch(d) {
          case 1:
          ;
          case 2:
            this.Pb.push(b);
            this.$b.push(360);
            this.af = j;
            break;
          case 3:
            f >= g ? (this.Pb.push(b), this.$b.push(360), this.af = j, this.Pb.push(180)) : this.Pb.push(b);
            this.$b.push(a);
            break;
          case 4:
            this.Pb.push(b), this.$b.push(a), this.af = j
        }
      }else {
        if(4 == c) {
          switch(d) {
            case 1:
            ;
            case 2:
              this.Pb.push(b);
              this.$b.push(360);
              break;
            case 3:
              this.Pb.push(b);
              this.$b.push(360);
              this.Pb.push(180);
              this.$b.push(a);
              break;
            case 4:
              f >= g ? (this.Pb.push(b), this.$b.push(360), this.Pb.push(180), this.$b.push(a), this.af = j) : (this.Pb.push(b), this.$b.push(a))
          }
        }
      }
    }
  }
  b = this.Pb.length;
  for(f = 0;f < b;f++) {
    a = this.j.Uh, this.bs.push(a)
  }
};
Sq.prototype.qa = function(a) {
  var b = this.fb(a), c = this.j.o.Tb, d = this.j.o.Qo, f = this.j.ae(), g = this.j.ca().Qa * this.j.ca().Pi, k = this.Pb[a] * Math.PI / 180, l = this.$b[a] * Math.PI / 180, a = f.x + c * Math.cos(k), k = f.y + d * Math.sin(k), n = f.x + c * Math.cos(l), f = f.y + d * Math.sin(l), l = q, m = j;
  180 < this.kh && (l = !l, m = !m);
  var b = b.r(), o;
  o = "" + S(a, k);
  o += b.ub(c, l, m, n, f, d);
  o += U(n, f + g);
  o += b.ub(c, 0, 0, a, k + g, d);
  return o += U(a, k)
};
Sq.prototype.isEnabled = function(a, b) {
  var a = a * (Math.PI / 180), b = b * (Math.PI / 180), c = Math.cos(a), d = Math.cos(b), f = this.cj(c, Math.sin(a)), g = this.cj(d, Math.sin(b));
  return 3 == f || 4 == f ? j : 1 == f ? 3 == g || 4 == g ? j : 1 == g ? c <= d : q : 2 == f ? 2 == g ? c <= d : j : q
};
function Tq(a) {
  Qq.call(this, a)
}
A.e(Tq, Qq);
Tq.prototype.p = function(a) {
  this.j = a;
  var b = a.jb, a = a.Go(), c = Math.sin(b * Math.PI / 180), d = Math.sin(a * Math.PI / 180), f = Math.cos(b * Math.PI / 180), g = Math.cos(a * Math.PI / 180), c = this.cj(f, c), d = this.cj(g, d);
  if(1 == c) {
    switch(d) {
      case 1:
        f >= g ? (this.Pb.push(b), this.$b.push(a)) : (this.Pb.push(b), this.$b.push(180), this.Pb.push(360), this.$b.push(a), this.af = j);
        break;
      case 2:
        this.Pb.push(b);
        this.$b.push(a);
        this.af = j;
        break;
      case 3:
      ;
      case 4:
        this.Pb.push(b), this.$b.push(180), this.af = j
    }
  }else {
    if(2 == c) {
      switch(d) {
        case 1:
          this.Pb.push(b);
          this.$b.push(180);
          this.Pb.push(360);
          this.$b.push(a);
          break;
        case 2:
          f >= g ? (this.Pb.push(b), this.$b.push(a)) : (this.Pb.push(b), this.$b.push(180), this.Pb.push(360), this.$b.push(a), this.af = j);
          break;
        case 3:
        ;
        case 4:
          this.Pb.push(b), this.$b.push(180)
      }
    }else {
      if(3 == c) {
        switch(d) {
          case 1:
            this.Pb.push(360);
            this.$b.push(a);
            break;
          case 2:
            this.Pb.push(360);
            this.$b.push(a);
            this.af = j;
            break;
          case 3:
            f >= g && (this.Pb.push(0), this.$b.push(180), this.af = j)
        }
      }else {
        if(4 == c) {
          switch(d) {
            case 1:
              this.Pb.push(360);
              this.$b.push(a);
              break;
            case 2:
              this.Pb.push(360);
              this.$b.push(a);
              this.af = j;
              break;
            case 3:
              this.Pb.push(360);
              this.$b.push(180);
              this.af = j;
              break;
            case 4:
              f >= g && (this.Pb.push(0), this.$b.push(180), this.af = j)
          }
        }
      }
    }
  }
  b = this.Pb.length;
  for(f = 0;f < b;f++) {
    a = this.j.ji, this.bs.push(a)
  }
};
Tq.prototype.qa = function(a) {
  var b = this.fb(a), c = this.j.o.Qa, d = this.j.o.oj, f = this.j.ae(), g = this.j.ca().Qa * this.j.ca().Pi, k = this.Pb[a] * Math.PI / 180, l = this.$b[a] * Math.PI / 180, a = f.x + c * Math.cos(k), k = f.y + d * Math.sin(k), n = f.x + c * Math.cos(l), f = f.y + d * Math.sin(l), l = q, m = j;
  180 < this.kh && (l = !l, m = !m);
  var b = b.r(), o;
  o = "" + S(a, k);
  o += b.ub(c, l, m, n, f, d);
  o += U(n, f + g);
  o += b.ub(c, 0, 0, a, k + g, d);
  return o += U(a, k)
};
Tq.prototype.isEnabled = function(a, b) {
  if(a == b) {
    return q
  }
  var a = a * (Math.PI / 180), b = b * (Math.PI / 180), c = Math.cos(a), d = Math.cos(b), f = this.cj(c, Math.sin(a)), g = this.cj(d, Math.sin(b));
  return 1 == f || 2 == f ? j : 3 == f ? 1 == g || 2 == g ? j : 3 == g ? c >= d : q : 4 == f ? 4 == g ? c >= d : j : q
};
function ho() {
  this.Ge()
}
A.e(ho, Xn);
z = ho.prototype;
z.ma = NaN;
z.Sb = u("ma");
z.gd = t("ma");
z.ta = u("ma");
z.g = function(a, b) {
  ho.f.g.call(this, a, b);
  C(a, "value") ? this.ma = F(a, "value") : C(a, "y") && (this.ma = F(a, "y"));
  this.ma = Ob(this.ma)
};
z.Cb = function() {
  ho.f.Cb.call(this);
  this.b.add("%Value", 0);
  this.b.add("%YValue", 0)
};
z.Pa = function(a) {
  switch(a) {
    case "%Value":
    ;
    case "%YValue":
    ;
    case "%YPercentOfSeries":
    ;
    case "%YPercentOfTotal":
      return 2
  }
  return ho.f.Pa.call(this, a)
};
z.Se = function() {
  ho.f.Se.call(this);
  this.ma && (this.b.add("%Value", this.ma), this.b.add("%YValue", this.ma))
};
z.Na = function(a) {
  oe(this.b, "%YPercentOfSeries") || this.b.add("%YPercentOfSeries", 100 * (this.ma / this.o.Uf("%SeriesYSum")));
  oe(this.b, "%YPercentOfTotal") || this.b.add("%YPercentOfTotal", 100 * (this.ma / this.ca().Na("%DataPlotYSum")));
  return ho.f.Na.call(this, a)
};
z.mb = function(a) {
  a = ho.f.mb.call(this, a);
  a.YValue = this.b.get("%YValue");
  a.YPercentOfSeries = this.b.get("%YPercentOfSeries");
  a.YPercentOfTotal = this.b.get("%YPercentOfTotal");
  return a
};
function Uq() {
  eo.call(this)
}
A.e(Uq, eo);
z = Uq.prototype;
z.Cb = function() {
  Uq.f.Cb.call(this);
  this.b.add("%SeriesYSum", 0);
  this.b.add("%SeriesYMax", 0);
  this.b.add("%SeriesYMin", 0);
  this.b.add("%SeriesYBasedPointsCount", 0);
  this.b.add("%SeriesFirstYValue", 0);
  this.b.add("%SeriesLastYValue", 0);
  this.b.add("%SeriesYAverage", 0);
  this.b.add("%SeriesMaxYValuePointName", "");
  this.b.add("%SeriesMinYValuePointName", "")
};
z.Pa = function(a) {
  switch(a) {
    case "%SeriesYSum":
    ;
    case "%SeriesYMax":
    ;
    case "%SeriesYMin":
    ;
    case "%SeriesYBasedPointsCount":
    ;
    case "%SeriesFirstYValue":
    ;
    case "%SeriesLastYValue":
    ;
    case "%SeriesYAverage":
      return 2;
    case "%SeriesMaxYValuePointName":
    ;
    case "%SeriesMinYValuePointName":
      return 1
  }
  return Uq.f.Pa.call(this, a)
};
z.rh = function() {
  Uq.f.rh.call(this);
  var a = this.b.get("%SeriesYSum");
  this.b.add("%SeriesValue", a);
  this.b.add("%SeriesYValue", a);
  var b = this.Fe();
  this.b.add("%SeriesYAverage", a / b);
  0 >= b || (this.Ga(0) && this.b.add("%SeriesFirstYValue", this.Ga(0).Sb()), this.Ga(b - 1) && this.b.add("%SeriesLastYValue", this.Ga(b - 1).Sb()))
};
z.th = function(a) {
  Uq.f.th.call(this, a);
  a = a.b;
  pe(a, "%DataPlotYSum", this.b.get("%SeriesYSum"));
  pe(a, "%DataPlotYBasedPointsCount", this.Fe());
  this.b.get("%SeriesYMax") > a.get("%DataPlotYMax") && (a.add("%DataPlotYMax", this.b.get("%SeriesYMax")), a.add("%DataPlotMaxYValuePointName", this.b.get("%SeriesMaxYValuePointName")), a.add("%DataPlotMaxYValuePointSeriesName", this.Sa));
  this.b.get("%SeriesYMin") < a.get("%DataPlotYMin") && (a.add("%DataPlotYMin", this.b.get("%SeriesYMin")), a.add("%DataPlotMinYValuePointName", this.b.get("%SeriesMinYValuePointName")), a.add("%DataPlotMinYValuePointSeriesName", this.Sa));
  this.b.get("%SeriesYSum") > a.get("%DataPlotMaxYSumSeries") && (a.add("%DataPlotMaxYSumSeries", this.b.get("%SeriesYSum")), a.add("%DataPlotMaxYSumSeriesName", this.Sa));
  this.b.get("%SeriesYSum") < a.get("%DataPlotMinYSumSeries") && (a.add("%DataPlotMinYSumSeries", this.b.get("%SeriesYSum")), a.add("%DataPlotMinYSumSeriesName", this.Sa))
};
z.Ed = function(a) {
  Uq.f.Ed.call(this, a);
  var b = a.Sb(), c = Number(this.b.get("%SeriesYMax")), d = Number(this.b.get("%SeriesYMin"));
  pe(this.b, "%SeriesYSum", b);
  b > c && (this.b.add("%SeriesYMax", b), this.b.add("%SeriesMaxYValuePointName", a.getName()));
  b < d && (this.b.add("%SeriesYMin", b), this.b.add("%SeriesMinYValuePointName", a.getName()))
};
z.mb = function(a) {
  a = Uq.f.mb.call(this, a);
  a.FirstYValue = this.Uf("%SeriesFirstYValue");
  a.LastYValue = this.Uf("%SeriesLastYValue");
  a.YSum = this.Uf["%SeriesYSum"];
  a.YMax = this.Uf["%SeriesYMax"];
  a.YMin = this.Uf["%SeriesYMin"];
  a.YAverage = this.Uf("%SeriesYAverage");
  a.YMedian = this.Uf("%SeriesYMedian");
  a.YMode = this.Uf("%SeriesYMode");
  return a
};
function Vq() {
}
A.e(Vq, ng);
Vq.prototype.update = function(a) {
  this.j.o.fj && !this.j.ca().rc[this.j.ap].isEnabled() ? this.Gi(q) : Vq.f.update.call(this, a)
};
Vq.prototype.GC = function(a, b, c, d) {
  b.update(a, c, d, q, b.Tp)
};
function Wq() {
  $.call(this)
}
A.e(Wq, og);
Wq.prototype.lc = function() {
  return new Vq
};
function Xq() {
}
A.e(Xq, Sn);
z = Xq.prototype;
z.zu = p;
z.$z = t("zu");
z.Qb = function() {
  return new Wq
};
z.Ib = function(a, b, c) {
  return this.zu ? Xq.f.Ib.call(this, a, b, c) : a.Jg(this, b, this.n(a, b, c))
};
z.copy = function(a) {
  a = Xq.f.copy.call(this, a);
  a.$z(this.zu);
  return a
};
function Yq(a, b, c, d) {
  this.ac = a;
  this.ob = b.height;
  this.ab = b.width;
  this.j = c;
  this.Ma = j;
  this.na = d
}
z = Yq.prototype;
z.ac = NaN;
z.Wd = t("ac");
z.ob = NaN;
z.vb = u("ob");
z.Rl = t("ob");
z.ab = NaN;
z.Ka = u("ab");
z.Bj = t("ab");
z.j = p;
z.Vl = t("j");
z.Ma = q;
z.isEnabled = u("Ma");
z.yg = t("Ma");
z.na = NaN;
z.Xr = t("na");
function Zq(a, b, c) {
  this.aa = c;
  this.hr = $q(a.ac, -90, 270, 90);
  a.ac = this.hr;
  this.wc = 90 <= this.hr && 270 > this.hr;
  this.O = [];
  this.O.push(a);
  this.tk = [];
  this.tk.push(a.ac);
  this.sk = [];
  this.sk.push(a.ob);
  this.wt = this.Th = a.ob;
  this.wc ? (this.ig = this.fl = ar(c, a, q).y + this.Th / 2, this.kg = this.ig - this.Th) : (this.kg = this.fl = ar(c, a, q).y - this.Th / 2, this.ig = this.kg + this.Th);
  this.Yj = b;
  this.sg = Number.MAX_VALUE;
  this.tg = -Number.MAX_VALUE;
  br(this, 0)
}
z = Zq.prototype;
z.wc = q;
z.Yj = NaN;
z.O = p;
z.tk = p;
z.sk = p;
z.kg = NaN;
z.ig = NaN;
z.sg = NaN;
z.tg = NaN;
z.wC = NaN;
z.fl = NaN;
z.wt = NaN;
z.Th = NaN;
z.aa = p;
z.hr = NaN;
function br(a, b) {
  var c = a.O[b], d = c.ac, f, g = 0;
  a.wc ? (c.Wd(Math.max(90.01, $q(a.tk[b] - a.Yj, -90, 270, 90))), f = ar(a.aa, c, q).y - c.vb() / 2 + a.sk[b], g = a.sg, a.sg = Math.min(g, f), c.Wd(Math.min(270, $q(a.tk[b] + a.Yj, 90, 450, 270))), f = ar(a.aa, c, q).y - c.vb() / 2 + a.sk[b], a.tg < f && (a.tg = f, a.wC = b)) : (c.Wd(Math.max(-90, $q(a.tk[b] - a.Yj, -270, 90, -90))), f = ar(a.aa, c, q).y + c.vb() / 2 - a.sk[b], g = a.tg, a.tg = Math.max(g, f), c.Wd(Math.min(90, $q(a.tk[b] + a.Yj, -90, 270, 90))), f = ar(a.aa, c, q).y + c.vb() / 
  2 - a.sk[b], a.sg > f && (a.sg = f, a.wC = b));
  c.Wd(d)
}
z.Yw = function(a) {
  return a.wc != this.wc ? q : this.wc ? this.kg <= a.ig : this.ig >= a.kg
};
function cr(a, b) {
  for(var c = b.O.length, d = 0;d < c;d++) {
    var f = b.O[d];
    f.Wd(b.tk[d]);
    a.O.push(f);
    a.tk.push(f.ac);
    a.Th += f.vb();
    a.sk.push(a.Th);
    a.fl = a.wc ? a.fl + (ar(a.aa, f, q).y + f.ob / 2 + a.wt) : a.fl + (ar(a.aa, f, q).y - f.ob / 2 - a.wt);
    a.wt += f.vb();
    br(a, a.O.length - 1)
  }
  a.wc ? (a.ig = a.fl / a.O.length, a.tg <= a.sg) ? (a.ig > a.sg && (a.ig = a.sg), a.ig < a.tg && (a.ig = a.tg), a.kg = a.ig - a.Th) : (a.ig = a.sg, a.kg = a.tg - a.Th) : (a.kg = a.fl / a.O.length, a.tg <= a.sg) ? (a.kg > a.sg && (a.kg = a.sg), a.kg < a.tg && (a.kg = a.tg), a.ig = a.kg + a.Th) : (a.kg = a.tg, a.ig = a.sg + a.Th)
}
function dr(a) {
  var b, c = a.O.length, d;
  d = a.tg <= a.sg ? 1 : 1 - (a.tg - a.sg) / a.sk[a.wC];
  if(a.wc) {
    for(b = 0;b < c;b++) {
      a.O[b].ac = er(a, 180 - fr(a.aa, a.ig - a.sk[b] * d + a.O[b].ob / 2), a.tk[b])
    }
  }else {
    for(b = 0;b < c;b++) {
      a.O[b].ac = er(a, fr(a.aa, a.kg + a.sk[b] * d - a.O[b].ob / 2), a.tk[b])
    }
  }
}
z.ML = 88 * Math.PI / 180;
function er(a, b, c) {
  var d = b, b = b * Math.PI / 180, c = c * Math.PI / 180;
  return 0 > Math.cos(b) * Math.cos(c) || Math.abs(Math.cos(b)) < Math.cos(a.ML) ? 0 < Math.sin(c) ? 90 - 2 * (0 < Math.cos(c) ? 1 : -1) : 270 + 2 * (0 < Math.cos(c) ? 1 : -1) : d
}
function $q(a, b, c, d) {
  for(;a < b;) {
    a += 360
  }
  for(;a >= c;) {
    a -= 360
  }
  if(a == b || a == d) {
    a += 0.01
  }
  return a
}
;function gr() {
  this.Ge();
  this.xz = {}
}
A.e(gr, ho);
z = gr.prototype;
z.jb = NaN;
z.tc = NaN;
z.Go = u("tc");
z.kh = NaN;
z.Qd = p;
z.gn = q;
z.cl = function() {
  isNaN(this.ma) && (this.Gb = j, this.ma = 0)
};
z.g = function(a, b) {
  gr.f.g.call(this, a, b);
  this.cl();
  C(a, "exploded") && (this.gn = K(a, "exploded"));
  this.gn && (this.cb.xD = j, this.ca().Kq = this)
};
z.p = function(a) {
  gr.f.p.call(this, a);
  this.kk() && (this.Bf = a.ja(), this.D.appendChild(this.Bf));
  return this.D
};
z.update = function() {
  this.Tj();
  gr.f.update.call(this);
  this.pC(this.fb().r())
};
z.Tj = function() {
  var a = this.ae();
  this.k.x = a.x - this.o.Qa;
  this.k.y = a.y - this.o.Qa;
  this.k.width = this.k.height = 2 * this.o.Qa
};
z.ae = function() {
  this.Qd == p && (this.Qd = new O);
  this.Qd.x = this.ca().ae().x;
  this.Qd.y = this.ca().ae().y;
  if(this.gn) {
    var a = this.ca().ik(), b = (this.jb + this.tc) / 2, b = b * (Math.PI / 180);
    this.Qd.x += a * Math.cos(b);
    this.Qd.y += a * Math.sin(b)
  }
  return this.Qd
};
z.GB = function() {
  this.jb = 0 == this.na ? this.o.jb : this.o.Ga(this.na - 1).Go();
  var a = this.o.eG;
  this.kh = 0 == a ? 360 / this.o.ba.length : 360 * this.ta() / a;
  this.tc = this.jb + this.kh
};
z.Jl = function(a) {
  var b = this.ca();
  b.Eb.Nt && (b.Eb.lJ || (b.Kq && b.Kq != this && (b.Kq.gn = q, b.Kq.Tj()), b.Kq = this), this.gn = !this.gn);
  gr.f.Jl.call(this, a)
};
z.xz = p;
z.nl = function() {
  var a = this.k.toString() + " " + this.gn.toString();
  if(this.xz[a]) {
    return this.xz[a]
  }
  var b = this.YB();
  return this.xz[a] = b
};
z.YB = function() {
  var a;
  a = 1 <= this.o.Tb ? this.o.Tb - 1 : this.o.Tb;
  var b = this.o.Qa, c, d, f;
  this.tc % 360 != this.jb % 360 ? (d = (this.tc + 0.5) * Math.PI / 180, c = this.jb * Math.PI / 180) : (f = Math.min(this.jb, this.tc), d = (this.tc - f) * Math.PI / 180, c = (this.jb - f) * Math.PI / 180);
  var g = this.ae();
  f = g.x + a * Math.cos(c);
  var k = g.y + a * Math.sin(c), l = g.x + b * Math.cos(c);
  c = g.y + b * Math.sin(c);
  var n = g.x + a * Math.cos(d), m = g.y + a * Math.sin(d), o = g.x + b * Math.cos(d);
  d = g.y + b * Math.sin(d);
  var v = g = q, w = j;
  180 < this.kh && (v = g = j);
  var y = this.r();
  l == o && (d -= 1.0E-4, w = q);
  f == n && (k += 1.0E-4);
  var E = S(f, k), E = E + S(l, c), E = E + y.ub(b, g, j, o, d), E = E + (w ? U(n, m) : S(n, m)), E = E + y.ub(a, v, q, f, k);
  return E + " Z"
};
z.Bf = p;
z.st = p;
z.kk = function() {
  return this.vc && this.Ab().kk()
};
z.nF = function() {
  if(this.kk()) {
    var a = this.Yt(this.r());
    a ? (this.Bf.setAttribute("d", a), this.Bf.setAttribute("visibility", "visible")) : this.Bf.setAttribute("visibility", "hidden")
  }
};
z.pC = function(a) {
  if(this.kk()) {
    var b = this.Yt(a);
    b != p ? this.Bf.setAttribute("d", b) : this.Bf.removeAttribute("d");
    this.Yz(a, this.Bf)
  }
};
z.Yz = function(a, b) {
  var c = Md(this.Ab().ut, a, this.st), c = c + md();
  b.setAttribute("style", c)
};
z.Yt = function(a) {
  if(this.o.fj && this.vc && this.jJ && this.Ab().fj && this.ca().rc[this.ap].isEnabled()) {
    var b = this.ca(), c = new O, d = new O, f = new O, c = this.ae().Ca(), g = b.rc[this.ap].ac, k = this.Ab().oJ ? b.Qa * this.Ab().Xi : this.Ab().Xi, d = ar(b, b.rc[this.ap], q).Ca(), f = ar(b, b.rc[this.ap], q).Ca();
    d.x = 90 < g % 360 && 270 > g % 360 ? d.x + k : d.x - k;
    return this.RH(a, c, d, f)
  }
};
z.RH = function(a, b, c, d) {
  var a = this.o.Qa, f = (this.jb + this.tc) / 2;
  b.x += Uc(a, f);
  b.y += Vc(a, f);
  return hr(this, b, c, d)
};
function hr(a, b, c, d) {
  var f = "";
  a.Ab().ut != p && (a.st = new P(Math.min(b.x, c.x, d.x), Math.min(b.y, c.y, d.y), Math.max(b.x, c.x, d.x) - Math.min(b.x, c.x, d.x), Math.max(b.y, c.y, d.y) - Math.min(b.y, c.y, d.y)), f += S(b.x, b.y), f += U(c.x, c.y), f += U(d.x, d.y));
  return f
}
z.ap = NaN;
z.jJ = j;
function ir(a, b) {
  b == h && (b = j);
  a.Mb().Ma = b;
  a.jJ = b
}
z.Jg = function(a, b, c) {
  return this.o.fj ? ar(this.ca(), this.ca().rc[this.ap], j) : this.Wq(a, b, c)
};
z.bg = function(a, b) {
  var c, d, f = this.o.Tb, g = this.o.Qa;
  switch(b) {
    case 0:
      c = this.jb + this.kh / 2;
      d = (f + g) / 2;
      break;
    case 7:
      c = this.jb + this.kh / 2;
      d = f;
      break;
    case 1:
      c = this.jb;
      d = (f + g) / 2;
      break;
    case 5:
      c = this.tc;
      d = (f + g) / 2;
      break;
    case 3:
      c = this.jb + this.kh / 2;
      d = g;
      break;
    case 8:
      c = this.jb;
      d = f;
      break;
    case 2:
      c = this.jb;
      d = g;
      break;
    case 6:
      c = this.tc;
      d = f;
      break;
    case 4:
      c = this.tc, d = g
  }
  f = this.ae();
  c *= Math.PI / 180;
  a.x = f.x + d * Math.cos(c);
  a.y = f.y + d * Math.sin(c)
};
z.Wa = function() {
  gr.f.Wa.call(this);
  this.Tn();
  this.nF();
  this.$a.oh(this.ya)
};
z.mt = s();
z.Cb = function() {
  gr.f.Cb.call(this);
  this.b.add("%YValue", 0)
};
z.Pa = function(a) {
  if(ue(a) && this.Fd) {
    return this.Fd.Pa(a)
  }
  switch(a) {
    case "%YValue":
    ;
    case "%Value":
    ;
    case "%YPercentOfSeries":
    ;
    case "%YPercentOfTotal":
    ;
    case "%YPercentOfCategory":
      return 2
  }
  return gr.f.Pa.call(this, a)
};
z.Se = function() {
  gr.f.Se.call(this);
  this.ma && (this.b.add("%YValue", this.ma), this.b.add("%Value", this.ma))
};
z.Na = function(a) {
  if(ue(a) && this.Fd) {
    return this.Fd.Na(a)
  }
  oe(this.b, "%YPercentOfSeries") || this.b.add("%YPercentOfSeries", 100 * (this.ma / this.o.Uf("%SeriesYSum")));
  oe(this.b, "%YPercentOfTotal") || this.b.add("%YPercentOfTotal", 100 * (this.ma / this.ca().Na("%DataPlotYSum")));
  !oe(this.b, "%YPercentOfCategory") && this.Fd && this.b.add("%YPercentOfCategory", 100 * (this.ma / this.Fd().Na("%CategoryYSum")));
  return gr.f.Na.call(this, a)
};
function jr() {
  eo.call(this);
  this.fj = q
}
A.e(jr, eo);
z = jr.prototype;
z.fj = p;
z.jb = 0;
z.Tb = 0;
z.Qa = 0;
z.un = x(j);
z.eG = 0;
z.qe = function() {
  return new gr
};
z.dA = function() {
  this.Tb = kr(this.cb.ca(), this.na, j);
  this.Qa = kr(this.cb.ca(), this.na, q)
};
z.Pd = function(a, b) {
  jr.f.Pd.call(this, a, b);
  this.eG += a.ta()
};
z.g = function(a, b) {
  jr.f.g.call(this, a, b);
  C(a, "start_angle") && (this.jb = M(a, "start_angle"));
  this.jb += this.cb.jb
};
z.Ug = function() {
  this.dA()
};
z.An = function() {
  this.dA()
};
z.Cb = function() {
  jr.f.Cb.call(this);
  this.b.add("%SeriesLastYValue", 0);
  this.b.add("%SeriesYSum", 0);
  this.b.add("%SeriesYMax", -Number.MAX_VALUE);
  this.b.add("%SeriesYMin", Number.MAX_VALUE);
  this.b.add("%SeriesYAverage", 0);
  this.b.add("%MaxYValuePointName", "");
  this.b.add("%MinYValuePointName", "")
};
z.Pa = function(a) {
  if(se(a, this.ca())) {
    return this.ca().Pa(a)
  }
  switch(a) {
    case "%SeriesLastYValue":
    ;
    case "%SeriesFirstYValue":
    ;
    case "%SeriesYSum":
    ;
    case "%SeriesYMax":
    ;
    case "%SeriesYMin":
    ;
    case "%SeriesYAverage":
      return 2;
    case "%MaxYValuePointName":
    ;
    case "%MinYValuePointName":
      return 1
  }
  return jr.f.Pa.call(this, a)
};
z.th = function(a) {
  jr.f.th.call(this, a);
  a = a.b;
  pe(a, "%DataPlotYSum", this.b.get("%SeriesYSum"));
  pe(a, "%YBasedPointsCount", this.ba.length);
  this.b.get("%SeriesYMax") > a.get("%DataPlotYMax") && (a.add("%DataPlotYMax", this.b.get("%SeriesYMax")), a.add("%DataPlotMaxYValuePointName", this.b.get("%SeriesMaxYValuePointName")), a.add("%DataPlotMaxYValuePointSeriesName", this.Sa));
  this.b.get("%SeriesYMin") < a.get("%DataPlotYMin") && (a.add("%DataPlotYMin", this.b.get("%SeriesYMin")), a.add("%DataPlotMinYValuePointName", this.b.get("%SeriesMinYValuePointName")), a.add("%DataPlotMinYValuePointSeriesName", this.Sa));
  this.b.get("%SeriesYSum") > a.get("%DataPlotMaxYSumSeries") && (a.add("%DataPlotMaxYSumSeries", this.b.get("%SeriesYSum")), a.add("%DataPlotMaxYSumSeriesName", this.Sa));
  this.b.get("%SeriesYSum") > a.get("%DataPlotMinYSumSeries") && (a.add("%DataPlotMinYSumSeries", this.b.get("%SeriesYSum")), a.add("%DataPlotMinYSumSeriesName", this.Sa))
};
z.Ed = function(a) {
  jr.f.Ed.call(this, a);
  pe(this.b, "%SeriesYSum", a.Sb());
  a.Sb() > this.b.get("%SeriesYMax") && (this.b.add("%SeriesYMax", a.Sb()), this.b.add("%SeriesMaxYValuePointName", a.getName()));
  a.Sb() < this.b.get("%SeriesYMin") && (this.b.add("%SeriesYMin", a.Sb()), this.b.add("%SeriesMinYValuePointName", a.getName()))
};
z.rh = function() {
  jr.f.rh.call(this);
  this.b.add("%SeriesYAverage", this.b.get("%SeriesYSum") / this.ba.length);
  this.ba && this.Ga(0) && this.b.add("%SeriesFirstYValue", this.Ga(0).Sb());
  this.ba && this.Ga(this.ba.length - 1) && this.b.add("%SeriesLastYValue", this.Ga(this.ba.length - 1).Sb())
};
function lr() {
  gr.call(this)
}
A.e(lr, gr);
z = lr.prototype;
z.Ii = p;
z.bi = p;
z.nh = p;
z.ji = p;
z.Uh = p;
z.xh = p;
z.Pj = p;
z.xG = 0.001;
z.iH = q;
z.p = function(a) {
  this.Ii = new W(a);
  this.bi = new W(a);
  this.nh = new W(a);
  this.ji = new W(a);
  this.Uh = new W(a);
  lr.f.p.call(this, a);
  this.xh = new Tq(a);
  this.Pj = new Sq(a);
  return this.D
};
z.ae = function(a) {
  a || (a = new O);
  a.x = this.ca().ae().x;
  a.y = this.ca().ae().y;
  if(this.gn) {
    var b = this.ca().ik() * this.ca().ph, c = (this.jb + this.tc) / 2;
    a.x += Uc(this.ca().ik(), c);
    a.y += Vc(b, c)
  }
  return a
};
z.Tj = function() {
  var a = this.ae(), b = this.ca().Qa * this.ca().Pi;
  this.TK = new P(a.x - this.o.Qa, a.y - this.o.oj, 2 * this.o.Qa, 2 * this.o.oj);
  this.k = this.TK.Ca();
  this.k.height += b
};
z.GB = function() {
  lr.f.GB.call(this);
  if(!this.Gb) {
    this.ca().ck.oG.ia(this.nh);
    var a = this.Ii, b = this.o.na;
    this.ca().ck.ih.push({s:Math.sin((this.jb + this.xG) * Math.PI / 180), c:a, i:b});
    a = this.bi;
    b = this.o.na;
    this.ca().ck.ih.push({s:Math.sin((this.tc - this.xG) * Math.PI / 180), c:a, i:b});
    this.Gb || (Yn(this, this.Ii), Yn(this, this.bi), Yn(this, this.nh));
    if(this.xh.isEnabled(this.jb, this.tc)) {
      this.xh.p(this);
      this.ca().ck.Pq.push({d:this.xh, i:this.o.na});
      for(a = 0;a < this.xh.li();a++) {
        Yn(this, this.xh.fb(a))
      }
    }
    if(this.Pj.isEnabled(this.jb, this.tc)) {
      this.Pj.p(this);
      this.ca().ck.cq.push({d:this.Pj, i:this.o.na});
      for(a = 0;a < this.Pj.li();a++) {
        Yn(this, this.Pj.fb(a))
      }
    }
  }
};
function Lq(a, b) {
  if(NaN == b) {
    return""
  }
  var c = a.ae(), d = "", f = a.o.Qa, g = a.o.oj, k = a.o.Qo, l = a.ca().Qa * a.ca().Pi, k = new O(c.x + Uc(a.o.Tb, b), c.y + Vc(k, b)), c = new O(c.x + Uc(f, b), c.y + Vc(g, b)), f = new O(c.x, c.y + l), l = new O(k.x, k.y + l), d = d + S(k.x, k.y), d = d + U(c.x, c.y), d = d + U(f.x, f.y), d = d + U(l.x, l.y), d = d + U(k.x, k.y);
  return d + " Z"
}
z.RH = function(a, b, c, d) {
  var a = this.o.Qa, f = this.o.oj, g = (this.jb + this.tc) / 2;
  b.y += this.ca().Qa * this.ca().Pi / 2;
  b.x += Uc(a, g);
  b.y += Vc(f, g);
  this.iH || (0 > Math.sin(g * Math.PI / 180) ? this.ca().ck.sB.appendChild(this.Bf) : this.ca().ck.TC.appendChild(this.Bf), this.iH = j);
  return hr(this, b, c, d)
};
z.bg = function(a, b) {
  this.ae(a);
  var c = this.o.Qa, d = this.o.oj, f = this.o.Tb, g = this.o.Qo, k, l, n;
  switch(b) {
    case 0:
      k = this.jb + this.kh / 2;
      l = (f + c) / 2;
      n = (g + d) / 2;
      break;
    case 7:
      k = this.jb + this.kh / 2;
      l = f;
      n = g;
      break;
    case 1:
      k = this.jb;
      l = (f + c) / 2;
      n = (g + d) / 2;
      break;
    case 5:
      k = this.tc;
      l = (f + c) / 2;
      n = (g + d) / 2;
      break;
    case 3:
      k = this.jb + this.kh / 2;
      l = c;
      n = d;
      break;
    case 8:
      k = this.jb;
      l = f;
      n = g;
      break;
    case 2:
      k = this.jb;
      l = c;
      n = d;
      break;
    case 6:
      k = this.tc;
      l = f;
      n = g;
      break;
    case 4:
      k = this.tc, l = c, n = d
  }
  a.x += Uc(l, k);
  a.y += Vc(n, k)
};
function mr() {
  jr.call(this)
}
A.e(mr, jr);
mr.prototype.oj = p;
mr.prototype.Qo = p;
mr.prototype.qe = function() {
  return new lr
};
mr.prototype.dA = function() {
  mr.f.dA.call(this);
  this.oj = this.Qa * this.ca().ph;
  this.Qo = this.Tb * this.ca().ph
};
function nr() {
  go.apply(this);
  this.Aw = j
}
A.e(nr, go);
z = nr.prototype;
z.cI = 0.1;
z.ik = u("cI");
z.xD = q;
z.lJ = j;
z.Nt = j;
z.mK = 1;
z.ng = u("mK");
z.Tb = 0.3;
z.jb = 0;
z.MG = q;
z.Ss = u("MG");
z.fj = q;
z.iE = q;
z.tE = 20;
z.zJ = q;
z.HK = q;
z.ut = p;
z.ex = q;
z.oJ = q;
z.Xi = 5;
z.kk = function() {
  return this.ut && this.ex
};
z.uh = function() {
  var a = this.aa.Jd ? new mr : new jr;
  a.gh(16);
  return a
};
z.Ah = x("pie_series");
z.Rb = x("pie_style");
z.Qb = function(a) {
  return this.aa.Jd ? new Pq : C(a, "name") && "aqua" == N(a, "name") ? new Eq : new Fq
};
z.Aq = function(a) {
  if(a == p || a == h) {
    a = q
  }
  var b = new Xq;
  b.$z(a);
  return b
};
z.g = function(a, b) {
  nr.f.g.call(this, a, b);
  C(a, "radius") && (this.mK = M(a, "radius") / 100);
  C(a, "inner_radius") && (this.Tb = M(a, "inner_radius") / 100);
  C(a, "explode_on_click") && (this.Nt = K(a, "explode_on_click"));
  C(a, "explode") && (this.cI = M(a, "explode") / 100);
  C(a, "allow_multiple_expand") && (this.lJ = K(a, "allow_multiple_expand"));
  C(a, "start_angle") && (this.jb = M(a, "start_angle"));
  if(C(a, "label_settings")) {
    var c = F(a, "label_settings");
    C(c, "mode") && (this.zJ = j, this.fj = "outside" == N(c, "mode"));
    C(c, "smart_labels") && (this.HK = K(c, "smart_labels"));
    if(this.fj && (C(c, "allow_overlap") && (this.MG = K(c, "allow_overlap")), C(c, "position") && (c = F(c, "position"), C(c, "padding") && (c = F(c, "padding"), this.tE = (this.iE = Mb(c)) ? Nb(c) : Ob(c))), c = F(a, "connector"), Tb(c))) {
      this.ex = j, this.ut = new Ld, this.ut.g(c), C(c, "padding") && (c = F(c, "padding"), Mb(c) ? (this.oJ = j, this.Xi = Nb(c)) : this.Xi = Ob(c))
    }
  }
};
z.Sw = x(j);
z.bt = x(q);
z.FC = function(a, b) {
  b ? a.sort(function(a, b) {
    return b.Sb() - a.Sb()
  }) : a.sort(function(a, b) {
    return a.Sb() - b.Sb()
  })
};
z.bI = function(a, b) {
  a = a.slice();
  b ? a.sort(function(a, b) {
    return M(b, "y") - M(a, "y")
  }) : a.sort(function(a, b) {
    return M(a, "y") - M(b, "y")
  });
  return a
};
var or = {chart:{styles:{pie_style:[{fill:{type:"solid", color:"%color", opacity:"1"}, border:{type:"solid", color:"DarkColor(%color)"}, states:{hover:{fill:{color:"Blend(White,%Color,0.3)"}}}, name:"anychart_default"}, {fill:{color:"%Color"}, states:{hover:{fill:{color:"Blend(White,%Color,0.9)"}}, pushed:{color:"rgb(200,200,200)"}, selected_hover:{fill:{color:"Blend(White,%Color,0.3)"}}, selected_normal:{fill:{color:"Blend(White,%Color,0.3)"}}}, name:"aqua"}]}, data_plot_settings:{pie_series:{animation:{style:"defaultPie"}, 
label_settings:{position:{padding:"20"}, animation:{style:"defaultLabel"}}, marker_settings:{animation:{style:"defaultMarker"}}, connector:{enabled:"true"}, tooltip_settings:{}, style:"Aqua"}}, chart_settings:{data_plot_background:{enabled:"false"}}}};
function pr() {
}
A.e(pr, cl);
pr.prototype.zh = function() {
  return this.$f(pr.f.zh.call(this), or)
};
/*

 Copyright (c) 2000-2004, Kevin Lindsey
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:

 - Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.

 - Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.

 - Neither the name of this software nor the names of its contributors
 may be used to endorse or promote products derived from this software
 without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
var Go, Ho, Uo, qr, rr;
(function() {
  function a() {
    this.Ha()
  }
  function b() {
    this.Ha()
  }
  function c() {
    this.Ha()
  }
  function d(a) {
    0 < arguments.length && this.Ha(a)
  }
  function f(a) {
    0 < arguments.length && this.Ha(a)
  }
  function g(a, b) {
    0 < arguments.length && this.Ha(a, b)
  }
  function k(a, b) {
    0 < arguments.length && (this.x = a, this.y = b)
  }
  function l() {
    this.Ha(arguments)
  }
  function n(a, b) {
    0 < arguments.length && (this.x = a, this.y = b)
  }
  function m(a) {
    0 < arguments.length && this.Ha(a)
  }
  function o(a) {
    0 < arguments.length && this.Ha(a)
  }
  function v(a) {
    0 < arguments.length && this.Ha(a)
  }
  function w(a, b, c) {
    0 < arguments.length && this.Ha(a, b, c)
  }
  function y(a, b, c, d, f) {
    0 < arguments.length && this.Ha(a, b, c, d, f)
  }
  function E(a, b, c) {
    0 < arguments.length && this.Ha(a, b, c)
  }
  function V(a) {
    0 < arguments.length && this.Ha(a)
  }
  function R(a, b) {
    0 < arguments.length && this.Ha(a, b)
  }
  function T(a) {
    0 < arguments.length && this.Ha(a)
  }
  function ca(a, b, c, d) {
    0 < arguments.length && this.Ha(a, b, c, d)
  }
  function ra(a, b, c) {
    0 < arguments.length && this.Ha("A", a, b, c)
  }
  function Pc(a, b, c) {
    0 < arguments.length && this.Ha("Q", a, b, c)
  }
  function yb(a, b, c) {
    0 < arguments.length && this.Ha("C", a, b, c)
  }
  function Y(a, b, c) {
    0 < arguments.length && this.Ha("H", a, b, c)
  }
  function L(a, b, c) {
    0 < arguments.length && this.Ha("L", a, b, c)
  }
  function Ye(a, b, c) {
    0 < arguments.length && this.Ha("M", a, b, c)
  }
  function Ze(a, b, c) {
    0 < arguments.length && this.Ha("T", a, b, c)
  }
  function Pd(a, b, c) {
    0 < arguments.length && this.Ha("S", a, b, c)
  }
  function ia(a, b, c, d) {
    0 < arguments.length && this.Ha(a, b, c, d)
  }
  function Qd(a, b, c) {
    0 < arguments.length && this.Ha("z", a, b, c)
  }
  function $e(a, b, c) {
    0 < arguments.length && this.Ha("q", a, b, c)
  }
  function af(a, b, c) {
    0 < arguments.length && this.Ha("c", a, b, c)
  }
  function bf(a, b, c) {
    0 < arguments.length && this.Ha("l", a, b, c)
  }
  function cf(a, b, c) {
    0 < arguments.length && this.Ha("m", a, b, c)
  }
  function df(a, b, c) {
    0 < arguments.length && this.Ha("t", a, b, c)
  }
  function Rd(a, b, c) {
    0 < arguments.length && this.Ha("s", a, b, c)
  }
  function ad(a) {
    0 < arguments.length && this.Ha(a)
  }
  function rc(a) {
    0 < arguments.length && this.Ha(a)
  }
  Array.prototype.lM = function(a) {
    for(var b = this.length, c = 0;c < b;c++) {
      a(this[c])
    }
  };
  Array.prototype.map = function(a) {
    for(var b = this.length, c = [], d = 0;d < b;d++) {
      c.push(a(this[d]))
    }
    return c
  };
  Array.prototype.min = function() {
    for(var a = this.length, b = this[0], c = 0;c < a;c++) {
      var d = this[c];
      d < b && (b = d)
    }
    return b
  };
  Array.prototype.max = function() {
    for(var a = this.length, b = this[0], c = 0;c < a;c++) {
      var d = this[c]
    }
    d > b && (b = d);
    return b
  };
  a.VERSION = "1.2";
  a.prototype.Ha = function() {
    var a = svgDocument.documentElement;
    this.AA = [];
    this.scale = 1;
    this.AJ = a.vH();
    a.addEventListener("SVGZoom", this, q);
    a.addEventListener("SVGScroll", this, q);
    a.addEventListener("SVGResize", this, q)
  };
  a.prototype.handleEvent = function(a) {
    var b = a.type;
    this[b] == p && e(Error("Unsupported event type: " + b));
    this[b](a)
  };
  a.prototype.update = function() {
    if(0 < this.AA.length) {
      for(var a = svgDocument.documentElement, b = window.CP != p ? new d(a) : p, b = b != p ? b.KM() : a.vH(), c = a.nQ, b = b.scale(1 / a.mQ), b = b.translate(-c.x, -c.y), a = 0;a < this.AA.length;a++) {
        c = b.multiply(this.AJ.multiply(this.AA[a].EQ())), c = "matrix(" + [c.FP, c.SP, c.aQ, c.pQ, c.tQ, c.xQ].join() + ")", this.AA[a].setAttributeNS(p, "transform", c)
      }
      this.AJ = b.inverse()
    }
  };
  b.VERSION = 1;
  b.prototype.Ha = s();
  b.prototype.handleEvent = function(a) {
    this[a.type] == p && e(Error("Unsupported event type: " + a.type));
    this[a.type](a)
  };
  c.prototype = new b;
  c.prototype.constructor = c;
  c.ic = b.prototype;
  c.prototype.Ha = function() {
    this.kb = p;
    this.lb = [];
    this.MF = [];
    this.xd()
  };
  c.prototype.xd = function() {
    if(this.kb == p) {
      var a = svgDocument.createElementNS("http://www.w3.org/2000/svg", "rect");
      this.kb = a;
      a.setAttributeNS(p, "x", "-16384");
      a.setAttributeNS(p, "y", "-16384");
      a.setAttributeNS(p, "width", "32767");
      a.setAttributeNS(p, "height", "32767");
      a.setAttributeNS(p, "fill", "none");
      a.setAttributeNS(p, "pointer-events", "all");
      a.setAttributeNS(p, "display", "none");
      svgDocument.documentElement.appendChild(a)
    }
  };
  c.prototype.kF = function(a) {
    if(-1 == this.PM(a)) {
      var b = a.vg;
      a.select(j);
      this.lb.push(a);
      b != p && -1 == this.XN(b) && this.MF.push(b)
    }
  };
  c.prototype.PM = function(a) {
    for(var b = -1, c = 0;c < this.lb.length;c++) {
      if(this.lb[c] === a) {
        b = c;
        break
      }
    }
    return b
  };
  c.prototype.XN = function(a) {
    for(var b = -1, c = 0;c < this.MF.length;c++) {
      if(this.MF[c] === a) {
        b = c;
        break
      }
    }
    return b
  };
  d.VERSION = "1.0";
  d.prototype.Ha = function(a) {
    var b = a.getAttributeNS(p, "viewBox"), a = a.getAttributeNS(p, "preserveAspectRatio");
    b != p && "" != b ? (b = b.split(/\s*,\s*|\s+/), this.x = parseFloat(b[0]), this.y = parseFloat(b[1]), this.width = parseFloat(b[2]), this.height = parseFloat(b[3])) : (this.y = this.x = 0, this.width = innerWidth, this.height = innerHeight);
    this.VN(a)
  };
  d.prototype.KM = function() {
    var a = svgDocument.documentElement, b = svgDocument.documentElement.vH(), c = a.getAttributeNS(p, "width"), d = a.getAttributeNS(p, "height"), c = c != p && "" != c ? parseFloat(c) : window.innerWidth ? innerWidth : document.documentElement.viewport.width, d = d != p && "" != d ? parseFloat(d) : window.innerHeight ? innerHeight : document.documentElement.viewport.height, a = this.width / c, f = this.height / d, b = b.translate(this.x, this.y);
    if("none" == this.Rs) {
      b = b.XR(a, f)
    }else {
      if(a < f && "meet" == this.Tu || a > f && "slice" == this.Tu) {
        a = 0, c = c * f - this.width, "Mid" == this.Rs ? a = -c / 2 : "Max" == this.Rs && (a = -c), b = b.translate(a, 0), b = b.scale(f)
      }else {
        if(a > f && "meet" == this.Tu || a < f && "slice" == this.Tu) {
          c = 0, f = d * a - this.height, "Mid" == this.ww ? c = -f / 2 : "Max" == this.ww && (c = -f), b = b.translate(0, c)
        }
        b = b.scale(a)
      }
    }
    return b
  };
  d.prototype.VN = function(a) {
    if(a) {
      var a = a.split(/\s+/), b = a[0];
      "none" == b ? this.ww = this.Rs = "none" : (this.Rs = b.substring(1, 4), this.ww = b.substring(5, 9));
      this.Tu = 2 == a.length ? a[1] : "meet"
    }else {
      this.align = "xMidYMid", this.ww = this.Rs = "Mid", this.Tu = "meet"
    }
  };
  f.prototype.Ha = function(a) {
    this.status = a;
    this.Fa = []
  };
  f.prototype.Nj = function(a) {
    this.Fa.push(a)
  };
  f.prototype.qc = function(a) {
    this.Fa = this.Fa.concat(a)
  };
  var Tu = {intersectEllipseRectangle:function(a, b) {
    return f.cN.apply(a, b)
  }, intersectLineRectangle:function(a, b) {
    return f.nr.apply(a, b)
  }, intersectPolygonRectangle:function(a, b) {
    return f.WD.apply(a, b)
  }, intersectLineLine:function(a, b) {
    return f.mr.apply(a, b)
  }};
  f.ZI = function(a, b) {
    var c = a.Ee(), d = b.Ee(), g;
    c != p && d != p ? "Path" == c.name ? g = f.YI(a, b) : "Path" == d.name ? g = f.YI(b, a) : (c.name < d.name ? (g = "intersect" + c.name + d.name, c = c.vz.concat(d.vz)) : (g = "intersect" + d.name + c.name, c = d.vz.concat(c.vz)), g = Tu[g](p, c)) : g = new f("No Intersection");
    return g
  };
  f.YI = function(a, b) {
    return a.dN(b)
  };
  f.bR = function(a, b, c, d, g, n) {
    var m, o, v = new f("No Intersection");
    m = b.multiply(-2);
    c = a.add(m.add(c));
    m = a.multiply(-2);
    o = b.multiply(2);
    b = m.add(o);
    a = new k(a.x, a.y);
    m = g.multiply(-2);
    n = d.add(m.add(n));
    m = d.multiply(-2);
    o = g.multiply(2);
    g = m.add(o);
    d = new k(d.x, d.y);
    m = c.x * b.y - b.x * c.y;
    o = n.x * b.y - b.x * n.y;
    var w = n.x * c.y - c.x * n.y, y = g.x * c.y - c.x * g.y, E = c.x * (a.y - d.y) + c.y * (-a.x + d.x);
    m = (new l(-w * w, -2 * w * y, m * o - y * y - 2 * w * E, m * (g.x * b.y - b.x * g.y) - 2 * y * E, m * (b.x * (a.y - d.y) + b.y * (-a.x + d.x)) - E * E)).mi();
    for(o = 0;o < m.length;o++) {
      if(w = m[o], 0 <= w && 1 >= w && (y = (new l(-c.x, -b.x, -a.x + d.x + w * g.x + w * w * n.x)).mi(), E = (new l(-c.y, -b.y, -a.y + d.y + w * g.y + w * w * n.y)).mi(), 0 < y.length && 0 < E.length)) {
        var T = 0;
        a:for(;T < y.length;T++) {
          var L = y[T];
          if(0 <= L && 1 >= L) {
            for(var R = 0;R < E.length;R++) {
              if(1.0E-4 > Math.abs(L - E[R])) {
                v.Fa.push(n.multiply(w * w).add(g.multiply(w).add(d)));
                break a
              }
            }
          }
        }
      }
    }
    return v
  };
  f.cR = function(a, b, c, d, g, ma, m) {
    var o, w, v, y = new f("No Intersection");
    o = b.multiply(-2);
    c = a.add(o.add(c));
    o = a.multiply(-2);
    w = b.multiply(2);
    b = o.add(w);
    a = new k(a.x, a.y);
    o = d.multiply(-1);
    w = g.multiply(3);
    v = ma.multiply(-3);
    o = o.add(w.add(v.add(m)));
    m = new n(o.x, o.y);
    o = d.multiply(3);
    w = g.multiply(-6);
    v = ma.multiply(3);
    o = o.add(w.add(v));
    ma = new n(o.x, o.y);
    o = d.multiply(-3);
    w = g.multiply(3);
    v = o.add(w);
    g = new n(v.x, v.y);
    d = new n(d.x, d.y);
    o = b.x * b.x;
    w = b.y * b.y;
    v = c.x * c.x;
    var E = c.y * c.y;
    o = (new l(-2 * c.x * c.y * m.x * m.y + v * m.y * m.y + E * m.x * m.x, -2 * c.x * c.y * ma.x * m.y - 2 * c.x * c.y * ma.y * m.x + 2 * E * ma.x * m.x + 2 * v * ma.y * m.y, -2 * c.x * g.x * c.y * m.y - 2 * c.x * c.y * g.y * m.x - 2 * c.x * c.y * ma.x * ma.y + 2 * g.x * E * m.x + E * ma.x * ma.x + v * (2 * g.y * m.y + ma.y * ma.y), 2 * a.x * c.x * c.y * m.y + 2 * a.y * c.x * c.y * m.x + b.x * b.y * c.x * m.y + b.x * b.y * c.y * m.x - 2 * d.x * c.x * c.y * m.y - 2 * c.x * d.y * c.y * m.x - 2 * c.x * 
    g.x * c.y * ma.y - 2 * c.x * c.y * g.y * ma.x - 2 * a.x * E * m.x - 2 * a.y * v * m.y + 2 * d.x * E * m.x + 2 * g.x * E * ma.x - w * c.x * m.x - o * c.y * m.y + v * (2 * d.y * m.y + 2 * g.y * ma.y), 2 * a.x * c.x * c.y * ma.y + 2 * a.y * c.x * c.y * ma.x + b.x * b.y * c.x * ma.y + b.x * b.y * c.y * ma.x - 2 * d.x * c.x * c.y * ma.y - 2 * c.x * d.y * c.y * ma.x - 2 * c.x * g.x * c.y * g.y - 2 * a.x * E * ma.x - 2 * a.y * v * ma.y + 2 * d.x * E * ma.x - w * c.x * ma.x - o * c.y * ma.y + g.x * g.x * 
    E + v * (2 * d.y * ma.y + g.y * g.y), 2 * a.x * c.x * c.y * g.y + 2 * a.y * c.x * g.x * c.y + b.x * b.y * c.x * g.y + b.x * b.y * g.x * c.y - 2 * d.x * c.x * c.y * g.y - 2 * c.x * d.y * g.x * c.y - 2 * a.x * g.x * E - 2 * a.y * v * g.y + 2 * d.x * g.x * E - w * c.x * g.x - o * c.y * g.y + 2 * v * d.y * g.y, -2 * a.x * a.y * c.x * c.y - a.x * b.x * b.y * c.y - a.y * b.x * b.y * c.x + 2 * a.x * c.x * d.y * c.y + 2 * a.y * d.x * c.x * c.y + b.x * d.x * b.y * c.y + b.x * b.y * c.x * d.y - 2 * d.x * 
    c.x * d.y * c.y - 2 * a.x * d.x * E + a.x * w * c.x + a.y * o * c.y - 2 * a.y * v * d.y - d.x * w * c.x - o * d.y * c.y + a.x * a.x * E + a.y * a.y * v + d.x * d.x * E + v * d.y * d.y)).by(0, 1);
    for(w = 0;w < o.length;w++) {
      v = o[w];
      var E = (new l(c.x, b.x, a.x - d.x - v * g.x - v * v * ma.x - v * v * v * m.x)).mi(), T = (new l(c.y, b.y, a.y - d.y - v * g.y - v * v * ma.y - v * v * v * m.y)).mi();
      if(0 < E.length && 0 < T.length) {
        var L = 0;
        a:for(;L < E.length;L++) {
          var R = E[L];
          if(0 <= R && 1 >= R) {
            for(var X = 0;X < T.length;X++) {
              if(1.0E-4 > Math.abs(R - T[X])) {
                y.Fa.push(m.multiply(v * v * v).add(ma.multiply(v * v).add(g.multiply(v).add(d))));
                break a
              }
            }
          }
        }
      }
    }
    0 < y.Fa.length && (y.status = "Intersection");
    return y
  };
  f.dR = function(a, b, c, d, g) {
    return f.ZM(a, b, c, d, g, g)
  };
  f.ZM = function(a, b, c, d, g, n) {
    var m, o = new f("No Intersection");
    m = b.multiply(-2);
    c = a.add(m.add(c));
    m = a.multiply(-2);
    b = b.multiply(2);
    m = m.add(b);
    a = new k(a.x, a.y);
    g *= g;
    n *= n;
    d = (new l(n * c.x * c.x + g * c.y * c.y, 2 * (n * c.x * m.x + g * c.y * m.y), n * (2 * c.x * a.x + m.x * m.x) + g * (2 * c.y * a.y + m.y * m.y) - 2 * (n * d.x * c.x + g * d.y * c.y), 2 * (n * m.x * (a.x - d.x) + g * m.y * (a.y - d.y)), n * (a.x * a.x + d.x * d.x) + g * (a.y * a.y + d.y * d.y) - 2 * (n * d.x * a.x + g * d.y * a.y) - g * n)).mi();
    for(n = 0;n < d.length;n++) {
      g = d[n], 0 <= g && 1 >= g && o.Fa.push(c.multiply(g * g).add(m.multiply(g).add(a)))
    }
    0 < o.Fa.length && (o.status = "Intersection");
    return o
  };
  f.ru = function(a, b, c, d, g) {
    var m, o, Sg, v, w = d.min(g), y = d.max(g), E = new f("No Intersection");
    m = b.multiply(-2);
    Sg = a.add(m.add(c));
    m = a.multiply(-2);
    o = b.multiply(2);
    m = m.add(o);
    o = new k(a.x, a.y);
    v = new n(d.y - g.y, g.x - d.x);
    roots = (new l(v.fk(Sg), v.fk(m), v.fk(o) + (d.x * g.y - g.x * d.y))).mi();
    for(Sg = 0;Sg < roots.length;Sg++) {
      m = roots[Sg], 0 <= m && 1 >= m && (m = a.qg(b, m).qg(b.qg(c, m), m), d.x == g.x ? w.y <= m.y && m.y <= y.y && (E.status = "Intersection", E.Nj(m)) : d.y == g.y ? w.x <= m.x && m.x <= y.x && (E.status = "Intersection", E.Nj(m)) : m.LI(w) && m.HJ(y) && (E.status = "Intersection", E.Nj(m)))
    }
    return E
  };
  f.eR = function(a, b, c, d) {
    for(var g = new f("No Intersection"), k = d.length, l = 0;l < k;l++) {
      var n = f.ru(a, b, c, d[l], d[(l + 1) % k]);
      g.qc(n.Fa)
    }
    0 < g.Fa.length && (g.status = "Intersection");
    return g
  };
  f.fR = function(a, b, c, d, g) {
    var l = d.min(g), n = d.max(g), g = new k(n.x, l.y), m = new k(l.x, n.y), d = f.ru(a, b, c, l, g), g = f.ru(a, b, c, g, n), n = f.ru(a, b, c, n, m), a = f.ru(a, b, c, m, l), b = new f("No Intersection");
    b.qc(d.Fa);
    b.qc(g.Fa);
    b.qc(n.Fa);
    b.qc(a.Fa);
    0 < b.Fa.length && (b.status = "Intersection");
    return b
  };
  f.gR = function(a, b, c, d, g, k, m, o) {
    var v, w, y, E = new f("No Intersection");
    v = a.multiply(-1);
    w = b.multiply(3);
    y = c.multiply(-3);
    v = v.add(w.add(y.add(d)));
    d = new n(v.x, v.y);
    v = a.multiply(3);
    w = b.multiply(-6);
    y = c.multiply(3);
    v = v.add(w.add(y));
    c = new n(v.x, v.y);
    v = a.multiply(-3);
    w = b.multiply(3);
    y = v.add(w);
    b = new n(y.x, y.y);
    a = new n(a.x, a.y);
    v = g.multiply(-1);
    w = k.multiply(3);
    y = m.multiply(-3);
    v = v.add(w.add(y.add(o)));
    o = new n(v.x, v.y);
    v = g.multiply(3);
    w = k.multiply(-6);
    y = m.multiply(3);
    v = v.add(w.add(y));
    m = new n(v.x, v.y);
    v = g.multiply(-3);
    w = k.multiply(3);
    y = v.add(w);
    k = new n(y.x, y.y);
    g = new n(g.x, g.y);
    v = a.x * a.x;
    w = a.y * a.y;
    y = b.x * b.x;
    var T = b.x * b.x * b.x, L = b.y * b.y, R = b.y * b.y * b.y, X = c.x * c.x, V = c.x * c.x * c.x, Z = c.y * c.y, ca = c.y * c.y * c.y, G = d.x * d.x, Y = d.x * d.x * d.x, D = d.y * d.y, ja = d.y * d.y * d.y, ra = g.x * g.x, ia = g.y * g.y, pb = k.x * k.x, bb = k.y * k.y, qb = m.x * m.x, cb = m.y * m.y, rb = o.x * o.x, sb = o.y * o.y;
    v = (new l(-Y * o.y * o.y * o.y + ja * o.x * o.x * o.x - 3 * d.x * D * rb * o.y + 3 * G * d.y * o.x * sb, -6 * d.x * m.x * D * o.x * o.y + 6 * G * d.y * m.y * o.x * o.y + 3 * m.x * ja * rb - 3 * Y * m.y * sb - 3 * d.x * D * m.y * rb + 3 * G * m.x * d.y * sb, -6 * k.x * d.x * D * o.x * o.y - 6 * d.x * m.x * D * m.y * o.x + 6 * G * m.x * d.y * m.y * o.y + 3 * k.x * ja * rb + 3 * qb * ja * o.x + 3 * k.x * G * d.y * sb - 3 * d.x * k.y * D * rb - 3 * d.x * qb * D * o.y + G * d.y * o.x * (6 * k.y * 
    o.y + 3 * cb) + Y * (-k.y * sb - 2 * cb * o.y - o.y * (2 * k.y * o.y + cb)), b.x * c.y * d.x * d.y * o.x * o.y - b.y * c.x * d.x * d.y * o.x * o.y + 6 * k.x * m.x * ja * o.x + 3 * b.x * c.x * d.x * d.y * sb + 6 * a.x * d.x * D * o.x * o.y - 3 * b.x * c.x * D * o.x * o.y - 3 * b.y * c.y * d.x * d.y * rb - 6 * a.y * G * d.y * o.x * o.y - 6 * g.x * d.x * D * o.x * o.y + 3 * b.y * c.y * G * o.x * o.y - 2 * c.x * Z * d.x * o.x * o.y - 6 * k.x * d.x * m.x * D * o.y - 6 * k.x * d.x * D * m.y * o.x - 
    6 * d.x * k.y * m.x * D * o.x + 6 * k.x * G * d.y * m.y * o.y + 2 * X * c.y * d.y * o.x * o.y + m.x * m.x * m.x * ja - 3 * a.x * ja * rb + 3 * a.y * Y * sb + 3 * g.x * ja * rb + ca * d.x * rb - V * d.y * sb - 3 * a.x * G * d.y * sb + 3 * a.y * d.x * D * rb - 2 * b.x * c.y * G * sb + b.x * c.y * D * rb - b.y * c.x * G * sb + 2 * b.y * c.x * D * rb + 3 * g.x * G * d.y * sb - c.x * Z * d.y * rb - 3 * g.y * d.x * D * rb + X * c.y * d.x * sb - 3 * d.x * qb * D * m.y + G * d.y * o.x * (6 * g.y * o.y + 
    6 * k.y * m.y) + G * m.x * d.y * (6 * k.y * o.y + 3 * cb) + Y * (-2 * k.y * m.y * o.y - g.y * sb - m.y * (2 * k.y * o.y + cb) - o.y * (2 * g.y * o.y + 2 * k.y * m.y)), 6 * b.x * c.x * d.x * d.y * m.y * o.y + b.x * c.y * d.x * m.x * d.y * o.y + b.x * c.y * d.x * d.y * m.y * o.x - b.y * c.x * d.x * m.x * d.y * o.y - b.y * c.x * d.x * d.y * m.y * o.x - 6 * b.y * c.y * d.x * m.x * d.y * o.x - 6 * a.x * m.x * ja * o.x + 6 * g.x * m.x * ja * o.x + 6 * a.y * Y * m.y * o.y + 2 * ca * d.x * m.x * o.x - 
    2 * V * d.y * m.y * o.y + 6 * a.x * d.x * m.x * D * o.y + 6 * a.x * d.x * D * m.y * o.x + 6 * a.y * d.x * m.x * D * o.x - 3 * b.x * c.x * m.x * D * o.y - 3 * b.x * c.x * D * m.y * o.x + 2 * b.x * c.y * m.x * D * o.x + 4 * b.y * c.x * m.x * D * o.x - 6 * a.x * G * d.y * m.y * o.y - 6 * a.y * G * m.x * d.y * o.y - 6 * a.y * G * d.y * m.y * o.x - 4 * b.x * c.y * G * m.y * o.y - 6 * g.x * d.x * m.x * D * o.y - 6 * g.x * d.x * D * m.y * o.x - 2 * b.y * c.x * G * m.y * o.y + 3 * b.y * c.y * G * m.x * 
    o.y + 3 * b.y * c.y * G * m.y * o.x - 2 * c.x * Z * d.x * m.x * o.y - 2 * c.x * Z * d.x * m.y * o.x - 2 * c.x * Z * m.x * d.y * o.x - 6 * g.y * d.x * m.x * D * o.x - 6 * k.x * d.x * k.y * D * o.x - 6 * k.x * d.x * m.x * D * m.y + 6 * g.x * G * d.y * m.y * o.y + 2 * X * c.y * d.x * m.y * o.y + 2 * X * c.y * m.x * d.y * o.y + 2 * X * c.y * d.y * m.y * o.x + 3 * k.x * qb * ja + 3 * pb * ja * o.x - 3 * d.x * k.y * qb * D - 3 * pb * d.x * D * o.y + G * m.x * d.y * (6 * g.y * o.y + 6 * k.y * m.y) + 
    G * d.y * o.x * (6 * g.y * m.y + 3 * bb) + k.x * G * d.y * (6 * k.y * o.y + 3 * cb) + Y * (-2 * g.y * m.y * o.y - o.y * (2 * g.y * m.y + bb) - k.y * (2 * k.y * o.y + cb) - m.y * (2 * g.y * o.y + 2 * k.y * m.y)), b.x * k.x * c.y * d.x * d.y * o.y + b.x * c.y * d.x * k.y * d.y * o.x + b.x * c.y * d.x * m.x * d.y * m.y - b.y * c.x * k.x * d.x * d.y * o.y - b.y * c.x * d.x * k.y * d.y * o.x - b.y * c.x * d.x * m.x * d.y * m.y - 6 * b.y * k.x * c.y * d.x * d.y * o.x - 6 * a.x * k.x * ja * o.x + 6 * 
    g.x * k.x * ja * o.x + 2 * k.x * ca * d.x * o.x + 6 * a.x * k.x * d.x * D * o.y + 6 * a.x * d.x * k.y * D * o.x + 6 * a.x * d.x * m.x * D * m.y + 6 * a.y * k.x * d.x * D * o.x - 3 * b.x * c.x * k.x * D * o.y - 3 * b.x * c.x * k.y * D * o.x - 3 * b.x * c.x * m.x * D * m.y + 2 * b.x * k.x * c.y * D * o.x + 4 * b.y * c.x * k.x * D * o.x - 6 * a.y * k.x * G * d.y * o.y - 6 * a.y * G * k.y * d.y * o.x - 6 * a.y * G * m.x * d.y * m.y - 6 * g.x * k.x * d.x * D * o.y - 6 * g.x * d.x * k.y * D * o.x - 
    6 * g.x * d.x * m.x * D * m.y + 3 * b.y * k.x * c.y * G * o.y - 3 * b.y * c.y * d.x * qb * d.y + 3 * b.y * c.y * G * k.y * o.x + 3 * b.y * c.y * G * m.x * m.y - 2 * c.x * k.x * Z * d.x * o.y - 2 * c.x * k.x * Z * d.y * o.x - 2 * c.x * Z * d.x * k.y * o.x - 2 * c.x * Z * d.x * m.x * m.y - 6 * g.y * k.x * d.x * D * o.x - 6 * k.x * d.x * k.y * m.x * D + 6 * g.y * G * k.y * d.y * o.x + 2 * X * k.x * c.y * d.y * o.y + 2 * X * c.y * k.y * d.y * o.x + 2 * X * c.y * m.x * d.y * m.y - 3 * a.x * qb * ja + 
    3 * g.x * qb * ja + 3 * pb * m.x * ja + ca * d.x * qb + 3 * a.y * d.x * qb * D + b.x * c.y * qb * D + 2 * b.y * c.x * qb * D - c.x * Z * qb * d.y - 3 * g.y * d.x * qb * D - 3 * pb * d.x * D * m.y + X * c.y * d.x * (2 * k.y * o.y + cb) + b.x * c.x * d.x * d.y * (6 * k.y * o.y + 3 * cb) + k.x * G * d.y * (6 * g.y * o.y + 6 * k.y * m.y) + V * d.y * (-2 * k.y * o.y - cb) + a.y * Y * (6 * k.y * o.y + 3 * cb) + b.y * c.x * G * (-2 * k.y * o.y - cb) + b.x * c.y * G * (-4 * k.y * o.y - 2 * cb) + a.x * 
    G * d.y * (-6 * k.y * o.y - 3 * cb) + G * m.x * d.y * (6 * g.y * m.y + 3 * bb) + g.x * G * d.y * (6 * k.y * o.y + 3 * cb) + Y * (-2 * g.y * k.y * o.y - m.y * (2 * g.y * m.y + bb) - g.y * (2 * k.y * o.y + cb) - k.y * (2 * g.y * o.y + 2 * k.y * m.y)), -a.x * b.x * c.y * d.x * d.y * o.y + a.x * b.y * c.x * d.x * d.y * o.y + 6 * a.x * b.y * c.y * d.x * d.y * o.x - 6 * a.y * b.x * c.x * d.x * d.y * o.y - a.y * b.x * c.y * d.x * d.y * o.x + a.y * b.y * c.x * d.x * d.y * o.x + b.x * b.y * c.x * c.y * 
    d.x * o.y - b.x * b.y * c.x * c.y * d.y * o.x + b.x * g.x * c.y * d.x * d.y * o.y + b.x * g.y * c.y * d.x * d.y * o.x + b.x * k.x * c.y * d.x * d.y * m.y + b.x * c.y * d.x * k.y * m.x * d.y - g.x * b.y * c.x * d.x * d.y * o.y - 6 * g.x * b.y * c.y * d.x * d.y * o.x - b.y * c.x * g.y * d.x * d.y * o.x - b.y * c.x * k.x * d.x * d.y * m.y - b.y * c.x * d.x * k.y * m.x * d.y - 6 * b.y * k.x * c.y * d.x * m.x * d.y - 6 * a.x * g.x * ja * o.x - 6 * a.x * k.x * m.x * ja - 2 * a.x * ca * d.x * o.x + 
    6 * g.x * k.x * m.x * ja + 2 * g.x * ca * d.x * o.x + 2 * k.x * ca * d.x * m.x + 2 * a.y * V * d.y * o.y - 6 * a.x * a.y * d.x * D * o.x + 3 * a.x * b.x * c.x * D * o.y - 2 * a.x * b.x * c.y * D * o.x - 4 * a.x * b.y * c.x * D * o.x + 3 * a.y * b.x * c.x * D * o.x + 6 * a.x * a.y * G * d.y * o.y + 6 * a.x * g.x * d.x * D * o.y - 3 * a.x * b.y * c.y * G * o.y + 2 * a.x * c.x * Z * d.x * o.y + 2 * a.x * c.x * Z * d.y * o.x + 6 * a.x * g.y * d.x * D * o.x + 6 * a.x * k.x * d.x * D * m.y + 6 * a.x * 
    d.x * k.y * m.x * D + 4 * a.y * b.x * c.y * G * o.y + 6 * a.y * g.x * d.x * D * o.x + 2 * a.y * b.y * c.x * G * o.y - 3 * a.y * b.y * c.y * G * o.x + 2 * a.y * c.x * Z * d.x * o.x + 6 * a.y * k.x * d.x * m.x * D - 3 * b.x * g.x * c.x * D * o.y + 2 * b.x * g.x * c.y * D * o.x + b.x * b.y * Z * d.x * o.x - 3 * b.x * c.x * g.y * D * o.x - 3 * b.x * c.x * k.x * D * m.y - 3 * b.x * c.x * k.y * m.x * D + 2 * b.x * k.x * c.y * m.x * D + 4 * g.x * b.y * c.x * D * o.x + 4 * b.y * c.x * k.x * m.x * D - 
    2 * a.x * X * c.y * d.y * o.y - 6 * a.y * g.x * G * d.y * o.y - 6 * a.y * g.y * G * d.y * o.x - 6 * a.y * k.x * G * d.y * m.y - 2 * a.y * X * c.y * d.x * o.y - 2 * a.y * X * c.y * d.y * o.x - 6 * a.y * G * k.y * m.x * d.y - b.x * b.y * X * d.y * o.y - 2 * b.x * L * d.x * d.y * o.x + 3 * g.x * b.y * c.y * G * o.y - 2 * g.x * c.x * Z * d.x * o.y - 2 * g.x * c.x * Z * d.y * o.x - 6 * g.x * g.y * d.x * D * o.x - 6 * g.x * k.x * d.x * D * m.y - 6 * g.x * d.x * k.y * m.x * D + 3 * b.y * g.y * c.y * 
    G * o.x + 3 * b.y * k.x * c.y * G * m.y + 3 * b.y * c.y * G * k.y * m.x - 2 * c.x * g.y * Z * d.x * o.x - 2 * c.x * k.x * Z * d.x * m.y - 2 * c.x * k.x * Z * m.x * d.y - 2 * c.x * Z * d.x * k.y * m.x - 6 * g.y * k.x * d.x * m.x * D - L * c.x * c.y * d.x * o.x + 2 * g.x * X * c.y * d.y * o.y + 6 * g.y * G * k.y * m.x * d.y + 2 * y * b.y * d.x * d.y * o.y + y * c.x * c.y * d.y * o.y + 2 * X * g.y * c.y * d.y * o.x + 2 * X * k.x * c.y * d.y * m.y + 2 * X * c.y * k.y * m.x * d.y + k.x * k.x * k.x * 
    ja + 3 * v * ja * o.x - 3 * w * Y * o.y + 3 * ra * ja * o.x + R * G * o.x - T * D * o.y - b.x * L * G * o.y + y * b.y * D * o.x - 3 * v * d.x * D * o.y + 3 * w * G * d.y * o.x - y * Z * d.x * o.y + L * X * d.y * o.x - 3 * pb * d.x * k.y * D - 3 * ra * d.x * D * o.y + 3 * ia * G * d.y * o.x + b.x * c.x * d.x * d.y * (6 * g.y * o.y + 6 * k.y * m.y) + V * d.y * (-2 * g.y * o.y - 2 * k.y * m.y) + a.y * Y * (6 * g.y * o.y + 6 * k.y * m.y) + b.y * c.x * G * (-2 * g.y * o.y - 2 * k.y * m.y) + X * c.y * 
    d.x * (2 * g.y * o.y + 2 * k.y * m.y) + b.x * c.y * G * (-4 * g.y * o.y - 4 * k.y * m.y) + a.x * G * d.y * (-6 * g.y * o.y - 6 * k.y * m.y) + g.x * G * d.y * (6 * g.y * o.y + 6 * k.y * m.y) + k.x * G * d.y * (6 * g.y * m.y + 3 * bb) + Y * (-2 * g.y * k.y * m.y - ia * o.y - k.y * (2 * g.y * m.y + bb) - g.y * (2 * g.y * o.y + 2 * k.y * m.y)), -a.x * b.x * c.y * d.x * d.y * m.y + a.x * b.y * c.x * d.x * d.y * m.y + 6 * a.x * b.y * c.y * d.x * m.x * d.y - 6 * a.y * b.x * c.x * d.x * d.y * m.y - a.y * 
    b.x * c.y * d.x * m.x * d.y + a.y * b.y * c.x * d.x * m.x * d.y + b.x * b.y * c.x * c.y * d.x * m.y - b.x * b.y * c.x * c.y * m.x * d.y + b.x * g.x * c.y * d.x * d.y * m.y + b.x * g.y * c.y * d.x * m.x * d.y + b.x * k.x * c.y * d.x * k.y * d.y - g.x * b.y * c.x * d.x * d.y * m.y - 6 * g.x * b.y * c.y * d.x * m.x * d.y - b.y * c.x * g.y * d.x * m.x * d.y - b.y * c.x * k.x * d.x * k.y * d.y - 6 * a.x * g.x * m.x * ja - 2 * a.x * ca * d.x * m.x + 2 * g.x * ca * d.x * m.x + 2 * a.y * V * d.y * m.y - 
    6 * a.x * a.y * d.x * m.x * D + 3 * a.x * b.x * c.x * D * m.y - 2 * a.x * b.x * c.y * m.x * D - 4 * a.x * b.y * c.x * m.x * D + 3 * a.y * b.x * c.x * m.x * D + 6 * a.x * a.y * G * d.y * m.y + 6 * a.x * g.x * d.x * D * m.y - 3 * a.x * b.y * c.y * G * m.y + 2 * a.x * c.x * Z * d.x * m.y + 2 * a.x * c.x * Z * m.x * d.y + 6 * a.x * g.y * d.x * m.x * D + 6 * a.x * k.x * d.x * k.y * D + 4 * a.y * b.x * c.y * G * m.y + 6 * a.y * g.x * d.x * m.x * D + 2 * a.y * b.y * c.x * G * m.y - 3 * a.y * b.y * c.y * 
    G * m.x + 2 * a.y * c.x * Z * d.x * m.x - 3 * b.x * g.x * c.x * D * m.y + 2 * b.x * g.x * c.y * m.x * D + b.x * b.y * Z * d.x * m.x - 3 * b.x * c.x * g.y * m.x * D - 3 * b.x * c.x * k.x * k.y * D + 4 * g.x * b.y * c.x * m.x * D - 2 * a.x * X * c.y * d.y * m.y - 6 * a.y * g.x * G * d.y * m.y - 6 * a.y * g.y * G * m.x * d.y - 6 * a.y * k.x * G * k.y * d.y - 2 * a.y * X * c.y * d.x * m.y - 2 * a.y * X * c.y * m.x * d.y - b.x * b.y * X * d.y * m.y - 2 * b.x * L * d.x * m.x * d.y + 3 * g.x * b.y * 
    c.y * G * m.y - 2 * g.x * c.x * Z * d.x * m.y - 2 * g.x * c.x * Z * m.x * d.y - 6 * g.x * g.y * d.x * m.x * D - 6 * g.x * k.x * d.x * k.y * D + 3 * b.y * g.y * c.y * G * m.x + 3 * b.y * k.x * c.y * G * k.y - 2 * c.x * g.y * Z * d.x * m.x - 2 * c.x * k.x * Z * d.x * k.y - L * c.x * c.y * d.x * m.x + 2 * g.x * X * c.y * d.y * m.y - 3 * b.y * pb * c.y * d.x * d.y + 6 * g.y * k.x * G * k.y * d.y + 2 * y * b.y * d.x * d.y * m.y + y * c.x * c.y * d.y * m.y + 2 * X * g.y * c.y * m.x * d.y + 2 * X * 
    k.x * c.y * k.y * d.y - 3 * a.x * pb * ja + 3 * g.x * pb * ja + 3 * v * m.x * ja - 3 * w * Y * m.y + 3 * ra * m.x * ja + pb * ca * d.x + R * G * m.x - T * D * m.y + 3 * a.y * pb * d.x * D - b.x * L * G * m.y + b.x * pb * c.y * D + 2 * b.y * c.x * pb * D + y * b.y * m.x * D - c.x * pb * Z * d.y - 3 * g.y * pb * d.x * D - 3 * v * d.x * D * m.y + 3 * w * G * m.x * d.y - y * Z * d.x * m.y + L * X * m.x * d.y - 3 * ra * d.x * D * m.y + 3 * ia * G * m.x * d.y + X * c.y * d.x * (2 * g.y * m.y + bb) + 
    b.x * c.x * d.x * d.y * (6 * g.y * m.y + 3 * bb) + V * d.y * (-2 * g.y * m.y - bb) + a.y * Y * (6 * g.y * m.y + 3 * bb) + b.y * c.x * G * (-2 * g.y * m.y - bb) + b.x * c.y * G * (-4 * g.y * m.y - 2 * bb) + a.x * G * d.y * (-6 * g.y * m.y - 3 * bb) + g.x * G * d.y * (6 * g.y * m.y + 3 * bb) + Y * (-2 * g.y * bb - ia * m.y - g.y * (2 * g.y * m.y + bb)), -a.x * b.x * c.y * d.x * k.y * d.y + a.x * b.y * c.x * d.x * k.y * d.y + 6 * a.x * b.y * k.x * c.y * d.x * d.y - 6 * a.y * b.x * c.x * d.x * k.y * 
    d.y - a.y * b.x * k.x * c.y * d.x * d.y + a.y * b.y * c.x * k.x * d.x * d.y - b.x * b.y * c.x * k.x * c.y * d.y + b.x * b.y * c.x * c.y * d.x * k.y + b.x * g.x * c.y * d.x * k.y * d.y + 6 * b.x * c.x * g.y * d.x * k.y * d.y + b.x * g.y * k.x * c.y * d.x * d.y - g.x * b.y * c.x * d.x * k.y * d.y - 6 * g.x * b.y * k.x * c.y * d.x * d.y - b.y * c.x * g.y * k.x * d.x * d.y - 6 * a.x * g.x * k.x * ja - 2 * a.x * k.x * ca * d.x + 6 * a.y * g.y * Y * k.y + 2 * g.x * k.x * ca * d.x + 2 * a.y * V * k.y * 
    d.y - 2 * V * g.y * k.y * d.y - 6 * a.x * a.y * k.x * d.x * D + 3 * a.x * b.x * c.x * k.y * D - 2 * a.x * b.x * k.x * c.y * D - 4 * a.x * b.y * c.x * k.x * D + 3 * a.y * b.x * c.x * k.x * D + 6 * a.x * a.y * G * k.y * d.y + 6 * a.x * g.x * d.x * k.y * D - 3 * a.x * b.y * c.y * G * k.y + 2 * a.x * c.x * k.x * Z * d.y + 2 * a.x * c.x * Z * d.x * k.y + 6 * a.x * g.y * k.x * d.x * D + 4 * a.y * b.x * c.y * G * k.y + 6 * a.y * g.x * k.x * d.x * D + 2 * a.y * b.y * c.x * G * k.y - 3 * a.y * b.y * k.x * 
    c.y * G + 2 * a.y * c.x * k.x * Z * d.x - 3 * b.x * g.x * c.x * k.y * D + 2 * b.x * g.x * k.x * c.y * D + b.x * b.y * k.x * Z * d.x - 3 * b.x * c.x * g.y * k.x * D + 4 * g.x * b.y * c.x * k.x * D - 6 * a.x * g.y * G * k.y * d.y - 2 * a.x * X * c.y * k.y * d.y - 6 * a.y * g.x * G * k.y * d.y - 6 * a.y * g.y * k.x * G * d.y - 2 * a.y * X * k.x * c.y * d.y - 2 * a.y * X * c.y * d.x * k.y - b.x * b.y * X * k.y * d.y - 4 * b.x * g.y * c.y * G * k.y - 2 * b.x * L * k.x * d.x * d.y + 3 * g.x * b.y * 
    c.y * G * k.y - 2 * g.x * c.x * k.x * Z * d.y - 2 * g.x * c.x * Z * d.x * k.y - 6 * g.x * g.y * k.x * d.x * D - 2 * b.y * c.x * g.y * G * k.y + 3 * b.y * g.y * k.x * c.y * G - 2 * c.x * g.y * k.x * Z * d.x - L * c.x * k.x * c.y * d.x + 6 * g.x * g.y * G * k.y * d.y + 2 * g.x * X * c.y * k.y * d.y + 2 * y * b.y * d.x * k.y * d.y + y * c.x * c.y * k.y * d.y + 2 * X * g.y * k.x * c.y * d.y + 2 * X * g.y * c.y * d.x * k.y + 3 * v * k.x * ja - 3 * w * Y * k.y + 3 * ra * k.x * ja + R * k.x * G - T * 
    k.y * D - 3 * ia * Y * k.y - b.x * L * G * k.y + y * b.y * k.x * D - 3 * v * d.x * k.y * D + 3 * w * k.x * G * d.y - y * Z * d.x * k.y + L * X * k.x * d.y - 3 * ra * d.x * k.y * D + 3 * ia * k.x * G * d.y, a.x * a.y * b.x * c.y * d.x * d.y - a.x * a.y * b.y * c.x * d.x * d.y + a.x * b.x * b.y * c.x * c.y * d.y - a.y * b.x * b.y * c.x * c.y * d.x - a.x * b.x * g.y * c.y * d.x * d.y + 6 * a.x * g.x * b.y * c.y * d.x * d.y + a.x * b.y * c.x * g.y * d.x * d.y - a.y * b.x * g.x * c.y * d.x * d.y - 
    6 * a.y * b.x * c.x * g.y * d.x * d.y + a.y * g.x * b.y * c.x * d.x * d.y - b.x * g.x * b.y * c.x * c.y * d.y + b.x * b.y * c.x * g.y * c.y * d.x + b.x * g.x * g.y * c.y * d.x * d.y - g.x * b.y * c.x * g.y * d.x * d.y - 2 * a.x * g.x * ca * d.x + 2 * a.y * V * g.y * d.y - 3 * a.x * a.y * b.x * c.x * D - 6 * a.x * a.y * g.x * d.x * D + 3 * a.x * a.y * b.y * c.y * G - 2 * a.x * a.y * c.x * Z * d.x - 2 * a.x * b.x * g.x * c.y * D - a.x * b.x * b.y * Z * d.x + 3 * a.x * b.x * c.x * g.y * D - 4 * 
    a.x * g.x * b.y * c.x * D + 3 * a.y * b.x * g.x * c.x * D + 6 * a.x * a.y * g.y * G * d.y + 2 * a.x * a.y * X * c.y * d.y + 2 * a.x * b.x * L * d.x * d.y + 2 * a.x * g.x * c.x * Z * d.y + 6 * a.x * g.x * g.y * d.x * D - 3 * a.x * b.y * g.y * c.y * G + 2 * a.x * c.x * g.y * Z * d.x + a.x * L * c.x * c.y * d.x + a.y * b.x * b.y * X * d.y + 4 * a.y * b.x * g.y * c.y * G - 3 * a.y * g.x * b.y * c.y * G + 2 * a.y * g.x * c.x * Z * d.x + 2 * a.y * b.y * c.x * g.y * G + b.x * g.x * b.y * Z * d.x - 3 * 
    b.x * g.x * c.x * g.y * D - 2 * a.x * X * g.y * c.y * d.y - 6 * a.y * g.x * g.y * G * d.y - 2 * a.y * g.x * X * c.y * d.y - 2 * a.y * y * b.y * d.x * d.y - a.y * y * c.x * c.y * d.y - 2 * a.y * X * g.y * c.y * d.x - 2 * b.x * g.x * L * d.x * d.y - b.x * b.y * X * g.y * d.y + 3 * g.x * b.y * g.y * c.y * G - 2 * g.x * c.x * g.y * Z * d.x - g.x * L * c.x * c.y * d.x + 3 * w * b.x * c.x * d.x * d.y + 3 * b.x * c.x * ia * d.x * d.y + 2 * g.x * X * g.y * c.y * d.y - 3 * v * b.y * c.y * d.x * d.y + 
    2 * y * b.y * g.y * d.x * d.y + y * c.x * g.y * c.y * d.y - 3 * ra * b.y * c.y * d.x * d.y - a.x * a.x * a.x * ja + a.y * a.y * a.y * Y + g.x * g.x * g.x * ja - g.y * g.y * g.y * Y - 3 * a.x * ra * ja - a.x * R * G + 3 * v * g.x * ja + a.y * T * D + 3 * a.y * ia * Y + g.x * R * G + v * ca * d.x - 3 * w * g.y * Y - w * V * d.y + ra * ca * d.x - T * g.y * D - V * ia * d.y - a.x * y * b.y * D + a.y * b.x * L * G - 3 * a.x * w * G * d.y - a.x * L * X * d.y + a.y * y * Z * d.x - b.x * L * g.y * G + 
    3 * v * a.y * d.x * D + v * b.x * c.y * D + 2 * v * b.y * c.x * D - 2 * w * b.x * c.y * G - w * b.y * c.x * G + y * g.x * b.y * D - 3 * a.x * ia * G * d.y + 3 * a.y * ra * d.x * D + b.x * ra * c.y * D - 2 * b.x * ia * c.y * G + g.x * L * X * d.y - b.y * c.x * ia * G - v * c.x * Z * d.y - 3 * v * g.y * d.x * D + 3 * w * g.x * G * d.y + w * X * c.y * d.x - y * g.y * Z * d.x + 2 * ra * b.y * c.x * D + 3 * g.x * ia * G * d.y - ra * c.x * Z * d.y - 3 * ra * g.y * d.x * D + X * ia * c.y * d.x)).by(0, 
    1);
    for(w = 0;w < v.length;w++) {
      if(y = v[w], T = (new l(d.x, c.x, b.x, a.x - g.x - y * k.x - y * y * m.x - y * y * y * o.x)).mi(), L = (new l(d.y, c.y, b.y, a.y - g.y - y * k.y - y * y * m.y - y * y * y * o.y)).mi(), 0 < T.length && 0 < L.length) {
        R = 0;
        a:for(;R < T.length;R++) {
          if(X = T[R], 0 <= X && 1 >= X) {
            for(V = 0;V < L.length;V++) {
              if(1.0E-4 > Math.abs(X - L[V])) {
                E.Fa.push(o.multiply(y * y * y).add(m.multiply(y * y).add(k.multiply(y).add(g))));
                break a
              }
            }
          }
        }
      }
    }
    0 < E.Fa.length && (E.status = "Intersection");
    return E
  };
  f.hR = function(a, b, c, d, g, k) {
    return f.$M(a, b, c, d, g, k, k)
  };
  f.$M = function(a, b, c, d, g, k, m) {
    var o, v, w, y = new f("No Intersection");
    o = a.multiply(-1);
    v = b.multiply(3);
    w = c.multiply(-3);
    o = o.add(v.add(w.add(d)));
    d = new n(o.x, o.y);
    o = a.multiply(3);
    v = b.multiply(-6);
    w = c.multiply(3);
    o = o.add(v.add(w));
    c = new n(o.x, o.y);
    o = a.multiply(-3);
    v = b.multiply(3);
    w = o.add(v);
    b = new n(w.x, w.y);
    a = new n(a.x, a.y);
    k *= k;
    m *= m;
    g = (new l(d.x * d.x * m + d.y * d.y * k, 2 * (d.x * c.x * m + d.y * c.y * k), 2 * (d.x * b.x * m + d.y * b.y * k) + c.x * c.x * m + c.y * c.y * k, 2 * d.x * m * (a.x - g.x) + 2 * d.y * k * (a.y - g.y) + 2 * (c.x * b.x * m + c.y * b.y * k), 2 * c.x * m * (a.x - g.x) + 2 * c.y * k * (a.y - g.y) + b.x * b.x * m + b.y * b.y * k, 2 * b.x * m * (a.x - g.x) + 2 * b.y * k * (a.y - g.y), a.x * a.x * m - 2 * a.y * g.y * k - 2 * a.x * g.x * m + a.y * a.y * k + g.x * g.x * m + g.y * g.y * k - k * m)).by(0, 
    1);
    for(m = 0;m < g.length;m++) {
      k = g[m], y.Fa.push(d.multiply(k * k * k).add(c.multiply(k * k).add(b.multiply(k).add(a))))
    }
    0 < y.Fa.length && (y.status = "Intersection");
    return y
  };
  f.su = function(a, b, c, d, g, k) {
    var m, o, v, w, y, E = g.min(k), L = g.max(k), T = new f("No Intersection");
    m = a.multiply(-1);
    o = b.multiply(3);
    v = c.multiply(-3);
    w = m.add(o.add(v.add(d)));
    y = new n(w.x, w.y);
    m = a.multiply(3);
    o = b.multiply(-6);
    v = c.multiply(3);
    w = m.add(o.add(v));
    w = new n(w.x, w.y);
    m = a.multiply(-3);
    o = b.multiply(3);
    v = m.add(o);
    m = new n(v.x, v.y);
    o = new n(a.x, a.y);
    v = new n(g.y - k.y, k.x - g.x);
    roots = (new l(v.fk(y), v.fk(w), v.fk(m), v.fk(o) + (g.x * k.y - k.x * g.y))).mi();
    for(y = 0;y < roots.length;y++) {
      w = roots[y], 0 <= w && 1 >= w && (m = b.qg(c, w), w = a.qg(b, w).qg(m, w).qg(m.qg(c.qg(d, w), w), w), g.x == k.x ? E.y <= w.y && w.y <= L.y && (T.status = "Intersection", T.Nj(w)) : g.y == k.y ? E.x <= w.x && w.x <= L.x && (T.status = "Intersection", T.Nj(w)) : w.LI(E) && w.HJ(L) && (T.status = "Intersection", T.Nj(w)))
    }
    return T
  };
  f.iR = function(a, b, c, d, g) {
    for(var k = new f("No Intersection"), l = g.length, m = 0;m < l;m++) {
      var n = f.su(a, b, c, d, g[m], g[(m + 1) % l]);
      k.qc(n.Fa)
    }
    0 < k.Fa.length && (k.status = "Intersection");
    return k
  };
  f.jR = function(a, b, c, d, g, l) {
    var m = g.min(l), n = g.max(l), l = new k(n.x, m.y), o = new k(m.x, n.y), g = f.su(a, b, c, d, m, l), l = f.su(a, b, c, d, l, n), n = f.su(a, b, c, d, n, o), a = f.su(a, b, c, d, o, m), b = new f("No Intersection");
    b.qc(g.Fa);
    b.qc(l.Fa);
    b.qc(n.Fa);
    b.qc(a.Fa);
    0 < b.Fa.length && (b.status = "Intersection");
    return b
  };
  f.kR = function(a, b, c, d) {
    var g;
    g = Math.abs(b - d);
    var l = a.QH(c);
    if(l > b + d) {
      g = new f("Outside")
    }else {
      if(l < g) {
        g = new f("Inside")
      }else {
        g = new f("Intersection");
        var m = (b * b - d * d + l * l) / (2 * l), d = a.qg(c, m / l), b = Math.sqrt(b * b - m * m) / l;
        g.Fa.push(new k(d.x - b * (c.y - a.y), d.y + b * (c.x - a.x)));
        g.Fa.push(new k(d.x + b * (c.y - a.y), d.y - b * (c.x - a.x)))
      }
    }
    return g
  };
  f.lR = function(a, b, c, d, g) {
    return f.bN(a, b, b, c, d, g)
  };
  f.tu = function(a, b, c, d) {
    var g;
    g = (d.x - c.x) * (d.x - c.x) + (d.y - c.y) * (d.y - c.y);
    var k = 2 * ((d.x - c.x) * (c.x - a.x) + (d.y - c.y) * (c.y - a.y)), a = k * k - 4 * g * (a.x * a.x + a.y * a.y + c.x * c.x + c.y * c.y - 2 * (a.x * c.x + a.y * c.y) - b * b);
    0 > a ? g = new f("Outside") : 0 == a ? g = new f("Tangent") : (b = Math.sqrt(a), a = (-k + b) / (2 * g), k = (-k - b) / (2 * g), (0 > a || 1 < a) && (0 > k || 1 < k) ? g = 0 > a && 0 > k || 1 < a && 1 < k ? new f("Outside") : new f("Inside") : (g = new f("Intersection"), 0 <= a && 1 >= a && g.Fa.push(c.qg(d, a)), 0 <= k && 1 >= k && g.Fa.push(c.qg(d, k))));
    return g
  };
  f.mR = function(a, b, c) {
    for(var d = new f("No Intersection"), g = c.length, k, l = 0;l < g;l++) {
      k = f.tu(a, b, c[l], c[(l + 1) % g]), d.qc(k.Fa)
    }
    d.status = 0 < d.Fa.length ? "Intersection" : k.status;
    return d
  };
  f.aN = function(a, b, c) {
    var d = c.min(h), g = c.max(h), l = new k(g.x, d.y), m = new k(d.x, g.y), c = f.tu(a, b, d, l), l = f.tu(a, b, l, g), g = f.tu(a, b, g, m), a = f.tu(a, b, m, d), b = new f("No Intersection");
    b.qc(c.Fa);
    b.qc(l.Fa);
    b.qc(g.Fa);
    b.qc(a.Fa);
    b.status = 0 < b.Fa.length ? "Intersection" : c.status;
    return b
  };
  f.bN = function(a, b, c, d, g, m) {
    for(var a = [c * c, 0, b * b, -2 * c * c * a.x, -2 * b * b * a.y, c * c * a.x * a.x + b * b * a.y * a.y - b * b * c * c], d = [m * m, 0, g * g, -2 * m * m * d.x, -2 * g * g * d.y, m * m * d.x * d.x + g * g * d.y * d.y - g * g * m * m], g = f.yL(a, d).mi(), m = 0.001 * (a[0] * a[0] + 2 * a[1] * a[1] + a[2] * a[2]), b = 0.001 * (d[0] * d[0] + 2 * d[1] * d[1] + d[2] * d[2]), c = new f("No Intersection"), n = 0;n < g.length;n++) {
      for(var o = (new l(a[0], a[3] + g[n] * a[1], a[5] + g[n] * (a[4] + g[n] * a[2]))).mi(), v = 0;v < o.length;v++) {
        var w = (a[0] * o[v] + a[1] * g[n] + a[3]) * o[v] + (a[2] * g[n] + a[4]) * g[n] + a[5];
        Math.abs(w) < m && (w = (d[0] * o[v] + d[1] * g[n] + d[3]) * o[v] + (d[2] * g[n] + d[4]) * g[n] + d[5], Math.abs(w) < b && c.Nj(new k(o[v], g[n])))
      }
    }
    0 < c.Fa.length && (c.status = "Intersection");
    return c
  };
  f.uu = function(a, b, c, d, g) {
    var k = n.mM(d, g), a = (new n(d.x, d.y)).Ph(new n(a.x, a.y)), l = new n(a.x / (b * b), a.y / (c * c)), b = k.fk(new n(k.x / (b * b), k.y / (c * c))), k = k.fk(l), a = a.fk(l) - 1, a = k * k - b * a;
    0 > a ? b = new f("Outside") : 0 < a ? (l = Math.sqrt(a), a = (-k - l) / b, k = (-k + l) / b, (0 > a || 1 < a) && (0 > k || 1 < k) ? b = 0 > a && 0 > k || 1 < a && 1 < k ? new f("Outside") : new f("Inside") : (b = new f("Intersection"), 0 <= a && 1 >= a && b.Nj(d.qg(g, a)), 0 <= k && 1 >= k && b.Nj(d.qg(g, k)))) : (k = -k / b, 0 <= k && 1 >= k ? (b = new f("Intersection"), b.Nj(d.qg(g, k))) : b = new f("Outside"));
    return b
  };
  f.nR = function(a, b, c, d) {
    for(var g = new f("No Intersection"), k = d.length, l = 0;l < k;l++) {
      var m = f.uu(a, b, c, d[l], d[(l + 1) % k]);
      g.qc(m.Fa)
    }
    0 < g.Fa.length && (g.status = "Intersection");
    return g
  };
  f.cN = function(a, b, c, d, g) {
    var l = d.min(g), m = d.max(g), g = new k(m.x, l.y), n = new k(l.x, m.y), d = f.uu(a, b, c, l, g), g = f.uu(a, b, c, g, m), m = f.uu(a, b, c, m, n), a = f.uu(a, b, c, n, l), b = new f("No Intersection");
    b.qc(d.Fa);
    b.qc(g.Fa);
    b.qc(m.Fa);
    b.qc(a.Fa);
    0 < b.Fa.length && (b.status = "Intersection");
    return b
  };
  f.mr = function(a, b, c, d) {
    var g, l = (d.x - c.x) * (a.y - c.y) - (d.y - c.y) * (a.x - c.x);
    g = (b.x - a.x) * (a.y - c.y) - (b.y - a.y) * (a.x - c.x);
    c = (d.y - c.y) * (b.x - a.x) - (d.x - c.x) * (b.y - a.y);
    0 != c ? (l /= c, g /= c, 0 <= l && 1 >= l && 0 <= g && 1 >= g ? (g = new f("Intersection"), g.Fa.push(new k(a.x + l * (b.x - a.x), a.y + l * (b.y - a.y)))) : g = new f("No Intersection")) : g = 0 == l || 0 == g ? new f("Coincident") : new f("Parallel");
    return g
  };
  f.vu = function(a, b, c) {
    for(var d = new f("No Intersection"), g = c.length, k = 0;k < g;k++) {
      var l = f.mr(a, b, c[k], c[(k + 1) % g]);
      d.qc(l.Fa)
    }
    0 < d.Fa.length && (d.status = "Intersection");
    return d
  };
  f.nr = function(a, b, c, d) {
    var g = c.min(d), l = c.max(d), d = new k(l.x, g.y), m = new k(g.x, l.y), c = f.mr(g, d, a, b), d = f.mr(d, l, a, b), l = f.mr(l, m, a, b), a = f.mr(m, g, a, b), b = new f("No Intersection");
    b.qc(c.Fa);
    b.qc(d.Fa);
    b.qc(l.Fa);
    b.qc(a.Fa);
    0 < b.Fa.length && (b.status = "Intersection");
    return b
  };
  f.oR = function(a, b) {
    for(var c = new f("No Intersection"), d = a.length, g = 0;g < d;g++) {
      var k = f.vu(a[g], a[(g + 1) % d], b);
      c.qc(k.Fa)
    }
    0 < c.Fa.length && (c.status = "Intersection");
    return c
  };
  f.WD = function(a, b, c) {
    var d = b.min(c), g = b.max(c), c = new k(g.x, d.y), l = new k(d.x, g.y), b = f.vu(d, c, a), c = f.vu(c, g, a), g = f.vu(g, l, a), a = f.vu(l, d, a), d = new f("No Intersection");
    d.qc(b.Fa);
    d.qc(c.Fa);
    d.qc(g.Fa);
    d.qc(a.Fa);
    0 < d.Fa.length && (d.status = "Intersection");
    return d
  };
  f.pR = function(a, b, c, d) {
    var g;
    g = (d.x - c.x) * (a.y - c.y) - (d.y - c.y) * (a.x - c.x);
    var l = (b.x - a.x) * (a.y - c.y) - (b.y - a.y) * (a.x - c.x), c = (d.y - c.y) * (b.x - a.x) - (d.x - c.x) * (b.y - a.y);
    0 != c ? (l = g / c, g = new f("Intersection"), g.Fa.push(new k(a.x + l * (b.x - a.x), a.y + l * (b.y - a.y)))) : g = 0 == g || 0 == l ? new f("Coincident") : new f("Parallel");
    return g
  };
  f.qR = function(a, b, c, d) {
    var g = a.min(b), l = a.max(b), b = new k(l.x, g.y), m = new k(g.x, l.y), a = f.nr(g, b, c, d), b = f.nr(b, l, c, d), l = f.nr(l, m, c, d), c = f.nr(m, g, c, d), d = new f("No Intersection");
    d.qc(a.Fa);
    d.qc(b.Fa);
    d.qc(l.Fa);
    d.qc(c.Fa);
    0 < d.Fa.length && (d.status = "Intersection");
    return d
  };
  f.yL = function(a, b) {
    var c = a[0] * b[1] - b[0] * a[1], d = a[0] * b[2] - b[0] * a[2], f = a[0] * b[3] - b[0] * a[3], g = a[0] * b[4] - b[0] * a[4], k = a[0] * b[5] - b[0] * a[5], m = a[1] * b[2] - b[1] * a[2], n = a[3] * b[5] - b[3] * a[5], o = a[1] * b[5] - b[1] * a[5] + (a[3] * b[4] - b[3] * a[4]), v = a[1] * b[4] - b[1] * a[4] - (a[2] * b[3] - b[2] * a[3]);
    return new l(c * m - d * d, c * v + f * m - 2 * d * g, c * o + f * v - g * g - 2 * d * k, c * n + f * o - 2 * g * k, f * n - k * k)
  };
  g.prototype.Ha = function(a, b) {
    this.name = a;
    this.vz = b
  };
  k.prototype.Ca = function() {
    return new k(this.x, this.y)
  };
  k.prototype.add = function(a) {
    return new k(this.x + a.x, this.y + a.y)
  };
  k.prototype.LG = function(a) {
    this.x += a.x;
    this.y += a.y;
    return this
  };
  k.prototype.Ph = function(a) {
    return new k(this.x - a.x, this.y - a.y)
  };
  k.prototype.multiply = function(a) {
    return new k(this.x * a, this.y * a)
  };
  k.prototype.mC = function(a) {
    return new k(this.x / a, this.y / a)
  };
  k.prototype.gH = function(a) {
    return this.x - a.x || this.y - a.y
  };
  k.prototype.HJ = function(a) {
    return this.x <= a.x && this.y <= a.y
  };
  k.prototype.LI = function(a) {
    return this.x >= a.x && this.y >= a.y
  };
  k.prototype.qg = function(a, b) {
    return new k(this.x + (a.x - this.x) * b, this.y + (a.y - this.y) * b)
  };
  k.prototype.QH = function(a) {
    var b = this.x - a.x, a = this.y - a.y;
    return Math.sqrt(b * b + a * a)
  };
  k.prototype.min = function(a) {
    return new k(Math.min(this.x, a.x), Math.min(this.y, a.y))
  };
  k.prototype.max = function(a) {
    return new k(Math.max(this.x, a.x), Math.max(this.y, a.y))
  };
  k.prototype.toString = function() {
    return this.x + "," + this.y
  };
  l.Qi = 1.0E-6;
  l.cL = 6;
  l.lr = function(a, b, c, d, f) {
    (a.constructor !== Array || b.constructor !== Array) && e(Error("Polynomial.interpolate: xs and ys must be arrays"));
    (isNaN(c) || isNaN(d) || isNaN(f)) && e(Error("Polynomial.interpolate: n, offset, and x must be numbers"));
    for(var g = 0, k = 0, l = Array(c), m = Array(c), n = 0, g = Math.abs(f - a[d]), o = 0;o < c;o++) {
      var v = Math.abs(f - a[d + o]);
      v < g && (n = o, g = v);
      l[o] = m[o] = b[d + o]
    }
    g = b[d + n];
    n--;
    for(b = 1;b < c;b++) {
      for(o = 0;o < c - b;o++) {
        var k = a[d + o] - f, v = a[d + o + b] - f, w = k - v;
        if(0 == w) {
          break
        }
        w = (l[o + 1] - m[o]) / w;
        m[o] = v * w;
        l[o] = k * w
      }
      k = 2 * (n + 1) < c - b ? l[n + 1] : m[n--];
      g += k
    }
    return{y:g, sQ:k}
  };
  l.prototype.Ha = function(a) {
    this.Ec = [];
    for(var b = a.length - 1;0 <= b;b--) {
      this.Ec.push(a[b])
    }
    this.KG = "t"
  };
  l.prototype.eval = function(a) {
    isNaN(a) && e(Error("Polynomial.eval: parameter must be a number"));
    for(var b = 0, c = this.Ec.length - 1;0 <= c;c--) {
      b = b * a + this.Ec[c]
    }
    return b
  };
  l.prototype.add = function(a) {
    for(var b = new l, c = this.ki(), d = a.ki(), f = Math.max(c, d), g = 0;g <= f;g++) {
      b.Ec[g] = (g <= c ? this.Ec[g] : 0) + (g <= d ? a.Ec[g] : 0)
    }
    return b
  };
  l.prototype.multiply = function(a) {
    for(var b = new l, c = 0;c <= this.ki() + a.ki();c++) {
      b.Ec.push(0)
    }
    for(c = 0;c <= this.ki();c++) {
      for(var d = 0;d <= a.ki();d++) {
        b.Ec[c + d] += this.Ec[c] * a.Ec[d]
      }
    }
    return b
  };
  l.prototype.ZN = function() {
    for(var a = this.ki();0 <= a;a--) {
      if(Math.abs(this.Ec[a]) <= l.Qi) {
        this.Ec.pop()
      }else {
        break
      }
    }
  };
  l.prototype.Xs = function(a, b) {
    var c = this.eval(a), d = this.eval(b), f;
    if(Math.abs(c) <= l.Qi) {
      f = a
    }else {
      if(Math.abs(d) <= l.Qi) {
        f = b
      }else {
        if(0 >= c * d) {
          for(var d = Math.log(b - a), d = Math.ceil((d + Math.LN10 * l.cL) / Math.LN2), g = 0;g < d;g++) {
            f = 0.5 * (a + b);
            var k = this.eval(f);
            if(Math.abs(k) <= l.Qi) {
              break
            }
            0 > k * c ? b = f : (a = f, c = k)
          }
        }
      }
    }
    return f
  };
  l.prototype.toString = function() {
    for(var a = [], b = [], c = this.Ec.length - 1;0 <= c;c--) {
      var d = Math.round(1E3 * this.Ec[c]) / 1E3;
      if(0 != d) {
        var f = 0 > d ? " - " : " + ", d = Math.abs(d);
        0 < c && (d = 1 == d ? this.KG : d + this.KG);
        1 < c && (d += "^" + c);
        b.push(f);
        a.push(d)
      }
    }
    b[0] = " + " == b[0] ? "" : "-";
    d = "";
    for(c = 0;c < a.length;c++) {
      d += b[c] + a[c]
    }
    return d
  };
  l.prototype.ki = function() {
    return this.Ec.length - 1
  };
  l.prototype.vM = function() {
    for(var a = new l, b = 1;b < this.Ec.length;b++) {
      a.Ec.push(b * this.Ec[b])
    }
    return a
  };
  l.prototype.mi = function() {
    var a;
    this.ZN();
    switch(this.ki()) {
      case 0:
        a = [];
        break;
      case 1:
        a = this.CM();
        break;
      case 2:
        a = this.IM();
        break;
      case 3:
        a = this.qI();
        break;
      case 4:
        a = this.JM();
        break;
      default:
        a = []
    }
    return a
  };
  l.prototype.by = function(a, b) {
    var c = [], d;
    if(1 == this.ki()) {
      d = this.Xs(a, b)
    }else {
      var f = this.vM().by(a, b);
      if(0 < f.length) {
        d = this.Xs(a, f[0]);
        d != p && c.push(d);
        for(i = 0;i <= f.length - 2;i++) {
          d = this.Xs(f[i], f[i + 1]), d != p && c.push(d)
        }
        d = this.Xs(f[f.length - 1], b)
      }else {
        d = this.Xs(a, b)
      }
    }
    d != p && c.push(d);
    return c
  };
  l.prototype.CM = function() {
    var a = [], b = this.Ec[1];
    0 != b && a.push(-this.Ec[0] / b);
    return a
  };
  l.prototype.IM = function() {
    var a = [];
    if(2 == this.ki()) {
      var b = this.Ec[2], c = this.Ec[1] / b, b = c * c - 4 * (this.Ec[0] / b);
      0 < b ? (b = Math.sqrt(b), a.push(0.5 * (-c + b)), a.push(0.5 * (-c - b))) : 0 == b && a.push(0.5 * -c)
    }
    return a
  };
  l.prototype.qI = function() {
    var a = [];
    if(3 == this.ki()) {
      var b = this.Ec[3], c = this.Ec[2] / b, d = this.Ec[1] / b, f = (3 * d - c * c) / 3, b = (2 * c * c * c - 9 * d * c + 27 * (this.Ec[0] / b)) / 27, c = c / 3, d = b * b / 4 + f * f * f / 27, b = b / 2;
      Math.abs(d) <= l.Qi && (d = 0);
      if(0 < d) {
        var f = Math.sqrt(d), g, d = -b + f;
        g = 0 <= d ? Math.pow(d, 1 / 3) : -Math.pow(-d, 1 / 3);
        d = -b - f;
        g = 0 <= d ? g + Math.pow(d, 1 / 3) : g - Math.pow(-d, 1 / 3);
        a.push(g - c)
      }else {
        0 > d ? (f = Math.sqrt(-f / 3), d = Math.atan2(Math.sqrt(-d), -b) / 3, b = Math.cos(d), d = Math.sin(d), g = Math.sqrt(3), a.push(2 * f * b - c), a.push(-f * (b + g * d) - c), a.push(-f * (b - g * d) - c)) : (d = 0 <= b ? -Math.pow(b, 1 / 3) : Math.pow(-b, 1 / 3), a.push(2 * d - c), a.push(-d - c))
      }
    }
    return a
  };
  l.prototype.JM = function() {
    var a = [];
    if(4 == this.ki()) {
      var b = this.Ec[4], c = this.Ec[3] / b, d = this.Ec[2] / b, f = this.Ec[1] / b, b = this.Ec[0] / b, g = (new l(1, -d, c * f - 4 * b, -c * c * b + 4 * d * b - f * f)).qI()[0], k = c * c / 4 - d + g;
      Math.abs(k) <= l.Qi && (k = 0);
      if(0 < k) {
        if(b = Math.sqrt(k), g = 3 * c * c / 4 - b * b - 2 * d, f = (4 * c * d - 8 * f - c * c * c) / (4 * b), d = g + f, f = g - f, Math.abs(d) <= l.Qi && (d = 0), Math.abs(f) <= l.Qi && (f = 0), 0 <= d && (d = Math.sqrt(d), a.push(-c / 4 + (b + d) / 2), a.push(-c / 4 + (b - d) / 2)), 0 <= f) {
          d = Math.sqrt(f), a.push(-c / 4 + (d - b) / 2), a.push(-c / 4 - (d + b) / 2)
        }
      }else {
        if(!(0 > k) && (f = g * g - 4 * b, f >= -l.Qi && (0 > f && (f = 0), f = 2 * Math.sqrt(f), g = 3 * c * c / 4 - 2 * d, g + f >= l.Qi && (d = Math.sqrt(g + f), a.push(-c / 4 + d / 2), a.push(-c / 4 - d / 2)), g - f >= l.Qi))) {
          d = Math.sqrt(g - f), a.push(-c / 4 + d / 2), a.push(-c / 4 - d / 2)
        }
      }
    }
    return a
  };
  n.prototype.length = function() {
    return Math.sqrt(this.x * this.x + this.y * this.y)
  };
  n.prototype.fk = function(a) {
    return this.x * a.x + this.y * a.y
  };
  n.prototype.add = function(a) {
    return new n(this.x + a.x, this.y + a.y)
  };
  n.prototype.LG = function(a) {
    this.x += a.x;
    this.y += a.y;
    return this
  };
  n.prototype.Ph = function(a) {
    return new n(this.x - a.x, this.y - a.y)
  };
  n.prototype.multiply = function(a) {
    return new n(this.x * a, this.y * a)
  };
  n.prototype.mC = function(a) {
    return new n(this.x / a, this.y / a)
  };
  n.prototype.toString = function() {
    return this.x + "," + this.y
  };
  n.mM = function(a, b) {
    return new n(b.x - a.x, b.y - a.y)
  };
  m.prototype = new b;
  m.prototype.constructor = m;
  m.ic = b.prototype;
  m.prototype.Ha = function(a) {
    this.kb = a;
    this.Ms = j;
    this.selected = q;
    this.YG = p
  };
  m.prototype.show = function(a) {
    this.Ms = a;
    this.kb.setAttributeNS(p, "display", a ? "inline" : "none")
  };
  m.prototype.refresh = s();
  m.prototype.update = function() {
    this.refresh();
    this.vg && this.vg.update(this);
    this.YG != p && this.YG(this)
  };
  m.prototype.translate = s();
  m.prototype.select = t("selected");
  o.prototype = new m;
  o.prototype.constructor = o;
  o.ic = m.prototype;
  o.prototype.Ha = function(a) {
    if("circle" == a.localName) {
      o.ic.Ha.call(this, a);
      var b = parseFloat(a.getAttributeNS(p, "cx")), c = parseFloat(a.getAttributeNS(p, "cy")), a = parseFloat(a.getAttributeNS(p, "r"));
      this.gg = new w(b, c, this);
      this.td = new w(b + a, c, this)
    }else {
      e(Error("Circle.init: Invalid SVG Node: " + a.localName))
    }
  };
  o.prototype.xd = function() {
    this.kb != p && (this.gg.xd(), this.td.xd(), this.gg.show(q), this.td.show(q), this.kb.addEventListener("mousedown", this, q))
  };
  o.prototype.translate = function(a) {
    this.gg.translate(a);
    this.td.translate(a);
    this.refresh()
  };
  o.prototype.refresh = function() {
    var a = this.td.ua.QH(this.gg.ua);
    this.kb.setAttributeNS(p, "cx", this.gg.ua.x);
    this.kb.setAttributeNS(p, "cy", this.gg.ua.y);
    this.kb.setAttributeNS(p, "r", a)
  };
  o.prototype.Ee = function() {
    return new g("Circle", [this.gg.ua, parseFloat(this.kb.getAttributeNS(p, "r"))])
  };
  v.prototype = new m;
  v.prototype.constructor = v;
  v.ic = m.prototype;
  v.prototype.Ha = function(a) {
    (a == p || "ellipse" != a.localName) && e(Error("Ellipse.init: Invalid localName: " + a.localName));
    v.ic.Ha.call(this, a);
    var b = parseFloat(a.getAttributeNS(p, "cx")), c = parseFloat(a.getAttributeNS(p, "cy")), d = parseFloat(a.getAttributeNS(p, "rx")), a = parseFloat(a.getAttributeNS(p, "ry"));
    this.gg = new w(b, c, this);
    this.hF = new w(b + d, c, this);
    this.iF = new w(b, c + a, this)
  };
  v.prototype.xd = function() {
    this.gg.xd();
    this.hF.xd();
    this.iF.xd();
    this.gg.show(q);
    this.hF.show(q);
    this.iF.show(q);
    this.kb.addEventListener("mousedown", this, q)
  };
  v.prototype.refresh = function() {
    var a = Math.abs(this.gg.ua.x - this.hF.ua.x), b = Math.abs(this.gg.ua.y - this.iF.ua.y);
    this.kb.setAttributeNS(p, "cx", this.gg.ua.x);
    this.kb.setAttributeNS(p, "cy", this.gg.ua.y);
    this.kb.setAttributeNS(p, "rx", a);
    this.kb.setAttributeNS(p, "ry", b)
  };
  v.prototype.Ee = function() {
    return new g("Ellipse", [this.gg.ua, parseFloat(this.kb.getAttributeNS(p, "rx")), parseFloat(this.kb.getAttributeNS(p, "ry"))])
  };
  w.prototype = new m;
  w.prototype.constructor = w;
  w.ic = m.prototype;
  w.qL = 0;
  w.jL = 1;
  w.kL = 2;
  w.prototype.Ha = function(a, b, c) {
    w.ic.Ha.call(this, p);
    this.ua = new k(a, b);
    this.vg = c;
    this.kH = w.qL
  };
  w.prototype.xd = function() {
    if(this.kb == p) {
      var a = svgDocument.createElementNS("http://www.w3.org/2000/svg", "rect"), b;
      b = this.vg != p && this.vg.kb != p ? this.vg.kb.parentNode : svgDocument.documentElement;
      a.setAttributeNS(p, "x", this.ua.x - 2);
      a.setAttributeNS(p, "y", this.ua.y - 2);
      a.setAttributeNS(p, "width", 4);
      a.setAttributeNS(p, "height", 4);
      a.setAttributeNS(p, "stroke", "black");
      a.setAttributeNS(p, "fill", "white");
      a.addEventListener("mousedown", this, q);
      b.appendChild(a);
      this.kb = a;
      this.show(this.Ms)
    }
  };
  w.prototype.translate = function(a) {
    this.kH == w.jL ? this.ua.x += a.x : this.kH == w.kL ? this.ua.y += a.y : this.ua.LG(a);
    this.refresh()
  };
  w.prototype.refresh = function() {
    this.kb.setAttributeNS(p, "x", this.ua.x - 2);
    this.kb.setAttributeNS(p, "y", this.ua.y - 2)
  };
  w.prototype.select = function(a) {
    w.ic.select.call(this, a);
    a ? this.kb.setAttributeNS(p, "fill", "black") : this.kb.setAttributeNS(p, "fill", "white")
  };
  y.prototype = new m;
  y.prototype.constructor = y;
  y.ic = m.prototype;
  y.prototype.Ha = function(a, b, c, d, f) {
    y.ic.Ha.call(this, p);
    this.ua = new w(a, b, this);
    this.Lu = new E(c, d, this);
    this.vg = f
  };
  y.prototype.xd = function() {
    if(this.kb == p) {
      var a = svgDocument.createElementNS("http://www.w3.org/2000/svg", "line"), b;
      b = this.vg != p && this.vg.kb != p ? this.vg.kb.parentNode : svgDocument.documentElement;
      a.setAttributeNS(p, "x1", this.ua.ua.x);
      a.setAttributeNS(p, "y1", this.ua.ua.y);
      a.setAttributeNS(p, "x2", this.Lu.ua.x);
      a.setAttributeNS(p, "y2", this.Lu.ua.y);
      a.setAttributeNS(p, "stroke", "black");
      b.appendChild(a);
      this.kb = a;
      this.ua.xd();
      this.Lu.xd();
      this.show(this.Ms)
    }
  };
  y.prototype.refresh = function() {
    this.kb.setAttributeNS(p, "x1", this.ua.ua.x);
    this.kb.setAttributeNS(p, "y1", this.ua.ua.y);
    this.kb.setAttributeNS(p, "x2", this.Lu.ua.x);
    this.kb.setAttributeNS(p, "y2", this.Lu.ua.y)
  };
  E.prototype = new w;
  E.prototype.constructor = E;
  E.ic = w.prototype;
  E.prototype.xd = function() {
    if(this.kb == p) {
      var a = svgDocument.createElementNS("http://www.w3.org/2000/svg", "circle"), b;
      b = this.vg != p && this.vg.kb != p ? this.vg.kb.parentNode : svgDocument.documentElement;
      a.setAttributeNS(p, "cx", this.ua.x);
      a.setAttributeNS(p, "cy", this.ua.y);
      a.setAttributeNS(p, "r", 2.5);
      a.setAttributeNS(p, "fill", "black");
      a.addEventListener("mousedown", this, q);
      b.appendChild(a);
      this.kb = a;
      this.show(this.Ms)
    }
  };
  E.prototype.refresh = function() {
    this.kb.setAttributeNS(p, "cx", this.ua.x);
    this.kb.setAttributeNS(p, "cy", this.ua.y)
  };
  E.prototype.select = function(a) {
    E.ic.select.call(this, a);
    this.kb.setAttributeNS(p, "fill", "black")
  };
  V.prototype = new m;
  V.prototype.constructor = V;
  V.ic = m.prototype;
  V.prototype.Ha = function(a) {
    (a == p || "line" != a.localName) && e(Error("Line.init: Invalid localName: " + a.localName));
    V.ic.Ha.call(this, a);
    var b = parseFloat(a.getAttributeNS(p, "x2")), c = parseFloat(a.getAttributeNS(p, "y2"));
    this.Vg = new w(parseFloat(a.getAttributeNS(p, "x1")), parseFloat(a.getAttributeNS(p, "y1")), this);
    this.Wg = new w(b, c, this)
  };
  V.prototype.xd = function() {
    this.Vg.xd();
    this.Wg.xd();
    this.Vg.show(q);
    this.Wg.show(q);
    this.kb.addEventListener("mousedown", this, q)
  };
  V.prototype.refresh = function() {
    this.kb.setAttributeNS(p, "x1", this.Vg.ua.x);
    this.kb.setAttributeNS(p, "y1", this.Vg.ua.y);
    this.kb.setAttributeNS(p, "x2", this.Wg.ua.x);
    this.kb.setAttributeNS(p, "y2", this.Wg.ua.y)
  };
  V.prototype.Ee = function() {
    return new g("Line", [this.Vg.ua, this.Wg.ua])
  };
  R.prototype.Ha = function(a, b) {
    this.type = a;
    this.text = b
  };
  R.prototype.pG = function(a) {
    return this.type == a
  };
  T.prototype = new m;
  T.prototype.constructor = T;
  T.ic = m.prototype;
  T.iL = 0;
  T.eB = 1;
  T.HG = 2;
  T.gB = {A:"rx,ry,x-axis-rotation,large-arc-flag,sweep-flag,x,y".split(","), a:"rx,ry,x-axis-rotation,large-arc-flag,sweep-flag,x,y".split(","), C:"x1,y1,x2,y2,x,y".split(","), c:"x1,y1,x2,y2,x,y".split(","), H:["x"], h:["x"], L:["x", "y"], l:["x", "y"], M:["x", "y"], m:["x", "y"], Q:["x1", "y1", "x", "y"], q:["x1", "y1", "x", "y"], S:["x2", "y2", "x", "y"], s:["x2", "y2", "x", "y"], T:["x", "y"], t:["x", "y"], V:["y"], v:["y"], Z:[], z:[]};
  T.prototype.Ha = function(a) {
    (a == p || "path" != a.localName) && e(Error("Path.init: Invalid localName: " + a.localName));
    T.ic.Ha.call(this, a);
    this.Dk = p;
    this.GN(a.getAttributeNS(p, "d"))
  };
  T.prototype.xd = function() {
    for(var a = 0;a < this.Dk.length;a++) {
      this.Dk[a].xd()
    }
    this.kb.addEventListener("mousedown", this, q)
  };
  T.prototype.refresh = function() {
    for(var a = [], b = 0;b < this.Dk.length;b++) {
      a.push(this.Dk[b].toString())
    }
    this.kb.setAttributeNS(p, "d", a.join(" "))
  };
  T.prototype.GN = function(a) {
    var a = this.dO(a), b = 0, c = a[b], d = "BOD";
    for(this.Dk = [];!c.pG(T.HG);) {
      var f, g = [];
      "BOD" == d ? "M" == c.text || "m" == c.text ? (b++, f = T.gB[c.text].length, d = c.text) : e(Error("Path data must begin with a moveto command")) : c.pG(T.eB) ? f = T.gB[d].length : (b++, f = T.gB[c.text].length, d = c.text);
      if(b + f < a.length) {
        for(c = b;c < b + f;c++) {
          var k = a[c];
          k.pG(T.eB) ? g[g.length] = k.text : e(Error("Parameter type is not a number: " + d + "," + k.text))
        }
        var l, c = this.Dk.length, c = 0 == c ? p : this.Dk[c - 1];
        switch(d) {
          case "A":
            l = new ra(g, this, c);
            break;
          case "C":
            l = new yb(g, this, c);
            break;
          case "c":
            l = new af(g, this, c);
            break;
          case "H":
            l = new Y(g, this, c);
            break;
          case "L":
            l = new L(g, this, c);
            break;
          case "l":
            l = new bf(g, this, c);
            break;
          case "M":
            l = new Ye(g, this, c);
            break;
          case "m":
            l = new cf(g, this, c);
            break;
          case "Q":
            l = new Pc(g, this, c);
            break;
          case "q":
            l = new $e(g, this, c);
            break;
          case "S":
            l = new Pd(g, this, c);
            break;
          case "s":
            l = new Rd(g, this, c);
            break;
          case "T":
            l = new Ze(g, this, c);
            break;
          case "t":
            l = new df(g, this, c);
            break;
          case "Z":
            l = new Qd(g, this, c);
            break;
          case "z":
            l = new Qd(g, this, c);
            break;
          default:
            e(Error("Unsupported segment type: " + d))
        }
        this.Dk.push(l);
        b += f;
        c = a[b];
        "M" == d && (d = "L");
        "m" == d && (d = "l")
      }else {
        e(Error("Path data ended before all parameters were found"))
      }
    }
  };
  T.prototype.dO = function(a) {
    for(var b = [];"" != a;) {
      a.match(/^([ \t\r\n,]+)/) ? a = a.substr(RegExp.$1.length) : a.match(/^([aAcChHlLmMqQsStTvVzZ])/) ? (b[b.length] = new R(T.iL, RegExp.$1), a = a.substr(RegExp.$1.length)) : a.match(/^(([-+]?[0-9]+(\.[0-9]*)?|[-+]?\.[0-9]+)([eE][-+]?[0-9]+)?)/) ? (b[b.length] = new R(T.eB, parseFloat(RegExp.$1)), a = a.substr(RegExp.$1.length)) : e(Error("Unrecognized segment command: " + a))
    }
    b[b.length] = new R(T.HG, p);
    return b
  };
  T.prototype.dN = function(a) {
    for(var b = new f("No Intersection"), c = 0;c < this.Dk.length;c++) {
      var d = f.ZI(this.Dk[c], a);
      b.qc(d.Fa)
    }
    0 < b.Fa.length && (b.status = "Intersection");
    return b
  };
  T.prototype.Ee = function() {
    return new g("Path", [])
  };
  ca.prototype.Ha = function(a, b, c, d) {
    this.Xh = a;
    this.vg = c;
    this.Ob = d;
    this.lb = [];
    for(a = 0;a < b.length;) {
      this.lb.push(new w(b[a], b[a + 1], c)), a += 2
    }
  };
  ca.prototype.xd = function() {
    for(var a = 0;a < this.lb.length;a++) {
      var b = this.lb[a];
      b.xd();
      b.show(q)
    }
  };
  ca.prototype.toString = function() {
    var a = [], b = "";
    if(this.Ob == p || this.Ob.constructor != this.fx) {
      b = this.Xh
    }
    for(var c = 0;c < this.lb.length;c++) {
      a.push(this.lb[c].ua.toString())
    }
    return b + a.join(" ")
  };
  ca.prototype.Sd = function() {
    return this.lb[this.lb.length - 1].ua
  };
  ca.prototype.Ee = x(p);
  ra.prototype = new ca;
  ra.prototype.constructor = ra;
  ra.ic = ca.prototype;
  ra.prototype.Ha = function(a, b, c, d) {
    var f = [], g = b.pop(), k = b.pop();
    f.push(k, g);
    ra.ic.Ha.call(this, a, f, c, d);
    this.pF = parseFloat(b.shift());
    this.qF = parseFloat(b.shift());
    this.OG = parseFloat(b.shift());
    this.QG = parseFloat(b.shift());
    this.MK = parseFloat(b.shift())
  };
  ra.prototype.toString = function() {
    var a = "";
    this.Ob.constructor != this.fx && (a = this.Xh);
    return a + [this.pF, this.qF, this.OG, this.QG, this.MK, this.lb[0].ua.toString()].join()
  };
  ra.prototype.Ee = function() {
    return new g("Ellipse", [this.sM(), this.pF, this.qF])
  };
  ra.prototype.sM = function() {
    var a = this.Ob.Sd(), b = this.lb[0].ua, c = this.pF, d = this.qF, f = this.OG * Math.PI / 180, g = Math.cos(f), f = Math.sin(f), l = a.Ph(b).mC(2), m = l.x * g + l.y * f, l = l.x * -f + l.y * g, n = m * m, o = l * l, v = n / (c * c) + o / (d * d);
    1 < v && (v = Math.sqrt(v), c *= v, d *= v);
    var v = c * c, w = d * d, o = v * o, n = w * n, v = (v * w - o - n) / (o + n);
    1.0E-6 > Math.abs(v) && (v = 0);
    n = Math.sqrt(v);
    this.QG == this.MK && (n = -n);
    a = a.add(b).mC(2);
    b = n * c * l / d;
    c = n * -d * m / c;
    return new k(b * g - c * f + a.x, b * f + c * g + a.y)
  };
  Pc.prototype = new ca;
  Pc.prototype.constructor = Pc;
  Pc.ic = ca.prototype;
  Pc.prototype.Fo = function() {
    return this.lb[0].ua
  };
  Pc.prototype.Ee = function() {
    return new g("Bezier2", [this.Ob.Sd(), this.lb[0].ua, this.lb[1].ua])
  };
  yb.prototype = new ca;
  yb.prototype.constructor = yb;
  yb.ic = ca.prototype;
  yb.prototype.bu = function() {
    return this.lb[1].ua
  };
  yb.prototype.Ee = function() {
    return new g("Bezier3", [this.Ob.Sd(), this.lb[0].ua, this.lb[1].ua, this.lb[2].ua])
  };
  Y.prototype = new ca;
  Y.prototype.constructor = Y;
  Y.ic = ca.prototype;
  Y.prototype.Ha = function(a, b, c, d) {
    var f = d.Sd(), g = [];
    g.push(b.pop(), f.y);
    Y.ic.Ha.call(this, a, g, c, d)
  };
  Y.prototype.toString = function() {
    var a = "";
    this.Ob.constructor != this.fx && (a = this.Xh);
    return a + this.lb[0].ua.x
  };
  L.prototype = new ca;
  L.prototype.constructor = L;
  L.ic = ca.prototype;
  L.prototype.toString = function() {
    var a = "";
    this.Ob.constructor != this.fx && this.Ob.constructor != Ye && (a = this.Xh);
    return a + this.lb[0].ua.toString()
  };
  L.prototype.Ee = function() {
    return new g("Line", [this.Ob.Sd(), this.lb[0].ua])
  };
  Ye.prototype = new ca;
  Ye.prototype.constructor = Ye;
  Ye.ic = ca.prototype;
  Ye.prototype.toString = function() {
    return"M" + this.lb[0].ua.toString()
  };
  Ze.prototype = new ca;
  Ze.prototype.constructor = Ze;
  Ze.ic = ca.prototype;
  Ze.prototype.Fo = function() {
    var a = this.Ob.Sd();
    if(this.Ob.Xh.match(/^[QqTt]$/)) {
      var b = this.Ob.Fo().Ph(a), a = a.Ph(b)
    }
    return a
  };
  Ze.prototype.Ee = function() {
    return new g("Bezier2", [this.Ob.Sd(), this.Fo(), this.lb[0].ua])
  };
  Pd.prototype = new ca;
  Pd.prototype.constructor = Pd;
  Pd.ic = ca.prototype;
  Pd.prototype.ZC = function() {
    var a = this.Ob.Sd();
    return this.Ob.Xh.match(/^[SsCc]$/) ? a.Ph(this.Ob.bu().Ph(a)) : a
  };
  Pd.prototype.bu = function() {
    return this.lb[0].ua
  };
  Pd.prototype.Ee = function() {
    return new g("Bezier3", [this.Ob.Sd(), this.ZC(), this.lb[0].ua, this.lb[1].ua])
  };
  ia.prototype = new ca;
  ia.prototype.constructor = ia;
  ia.ic = ca.prototype;
  ia.prototype.Ha = function(a, b, c, d) {
    this.Xh = a;
    this.vg = c;
    this.Ob = d;
    this.lb = [];
    a = this.Ob ? this.Ob.Sd() : new k(0, 0);
    for(d = 0;d < b.length;) {
      this.lb.push(new w(a.x + b[d], a.y + b[d + 1], c)), d += 2
    }
  };
  ia.prototype.toString = function() {
    var a = [], b = "", c;
    c = this.Ob ? this.Ob.Sd() : new k(0, 0);
    if(this.Ob == p || this.Ob.constructor != this.constructor) {
      b = this.Xh
    }
    for(var d = 0;d < this.lb.length;d++) {
      a.push(this.lb[d].ua.Ph(c).toString())
    }
    return b + a.join(" ")
  };
  Qd.prototype = new ia;
  Qd.prototype.constructor = Qd;
  Qd.ic = ia.prototype;
  Qd.prototype.Sd = function() {
    for(var a = this.Ob, b;a;) {
      if(a.Xh.match(/^[mMzZ]$/)) {
        b = a.Sd();
        break
      }
      a = a.Ob
    }
    return b
  };
  Qd.prototype.Ee = function() {
    return new g("Line", [this.Ob.Sd(), this.Sd()])
  };
  $e.prototype = new ia;
  $e.prototype.constructor = $e;
  $e.ic = ia.prototype;
  $e.prototype.Fo = function() {
    return this.lb[0].ua
  };
  $e.prototype.Ee = function() {
    return new g("Bezier2", [this.Ob.Sd(), this.lb[0].ua, this.lb[1].ua])
  };
  af.prototype = new ia;
  af.prototype.constructor = af;
  af.ic = ia.prototype;
  af.prototype.bu = function() {
    return this.lb[1].ua
  };
  af.prototype.Ee = function() {
    return new g("Bezier3", [this.Ob.Sd(), this.lb[0].ua, this.lb[1].ua, this.lb[2].ua])
  };
  bf.prototype = new ia;
  bf.prototype.constructor = bf;
  bf.ic = ia.prototype;
  bf.prototype.toString = function() {
    var a;
    a = this.lb[0].ua.Ph(this.Ob ? this.Ob.Sd() : new Point(0, 0));
    this.Ob.constructor != this.fx && this.Ob.constructor != cf && (cmd = this.Xh);
    return cmd + a.toString()
  };
  bf.prototype.Ee = function() {
    return new g("Line", [this.Ob.Sd(), this.lb[0].ua])
  };
  cf.prototype = new ia;
  cf.prototype.constructor = cf;
  cf.ic = ia.prototype;
  cf.prototype.toString = function() {
    return"m" + this.lb[0].ua.toString()
  };
  df.prototype = new ia;
  df.prototype.constructor = df;
  df.ic = ia.prototype;
  df.prototype.Fo = function() {
    var a = this.Ob.Sd();
    if(this.Ob.Xh.match(/^[QqTt]$/)) {
      var b = this.Ob.Fo().Ph(a), a = a.Ph(b)
    }
    return a
  };
  df.prototype.Ee = function() {
    return new g("Bezier2", [this.Ob.Sd(), this.Fo(), this.lb[0].ua])
  };
  Rd.prototype = new ia;
  Rd.prototype.constructor = Rd;
  Rd.ic = ia.prototype;
  Rd.prototype.ZC = function() {
    var a = this.Ob.Sd();
    return this.Ob.Xh.match(/^[SsCc]$/) ? a.Ph(this.Ob.bu().Ph(a)) : a
  };
  Rd.prototype.bu = function() {
    return this.lb[0].ua
  };
  Rd.prototype.Ee = function() {
    return new g("Bezier3", [this.Ob.Sd(), this.ZC(), this.lb[0].ua, this.lb[1].ua])
  };
  ad.prototype = new m;
  ad.prototype.constructor = ad;
  ad.ic = m.prototype;
  ad.prototype.Ha = function(a) {
    if("polygon" == a.localName) {
      ad.ic.Ha.call(this, a);
      a = a.getAttributeNS(p, "points").split(/[\s,]+/);
      this.lb = [];
      for(var b = 0;b < a.length;b += 2) {
        this.lb.push(new w(parseFloat(a[b]), parseFloat(a[b + 1]), this))
      }
    }else {
      e(Error("Polygon.init: Invalid SVG Node: " + a.localName))
    }
  };
  ad.prototype.xd = function() {
    if(this.kb != p) {
      for(var a = 0;a < this.lb.length;a++) {
        this.lb[a].xd(), this.lb[a].show(q)
      }
      this.kb.addEventListener("mousedown", this, q)
    }
  };
  ad.prototype.refresh = function() {
    for(var a = [], b = 0;b < this.lb.length;b++) {
      a.push(this.lb[b].ua.toString())
    }
    this.kb.setAttributeNS(p, "points", a.join(" "))
  };
  ad.prototype.Ee = function() {
    for(var a = [], b = 0;b < this.lb.length;b++) {
      a.push(this.lb[b].ua)
    }
    return new g("Polygon", [a])
  };
  rc.prototype = new m;
  rc.prototype.constructor = rc;
  rc.ic = m.prototype;
  rc.prototype.Ha = function(a) {
    if("rect" == a.localName) {
      rc.ic.Ha.call(this, a);
      var b = parseFloat(a.getAttributeNS(p, "x")), c = parseFloat(a.getAttributeNS(p, "y")), d = parseFloat(a.getAttributeNS(p, "width")), a = parseFloat(a.getAttributeNS(p, "height"));
      this.Vg = new w(b, c, this);
      this.Wg = new w(b + d, c + a, this)
    }else {
      e(Error("Rectangle.init: Invalid SVG Node: " + a.localName))
    }
  };
  rc.prototype.xd = function() {
    this.kb != p && (this.Vg.xd(), this.Wg.xd(), this.Vg.show(q), this.Wg.show(q), this.kb.addEventListener("mousedown", this, q))
  };
  rc.prototype.refresh = function() {
    var a = this.Vg.ua.min(this.Wg.ua), b = this.Vg.ua.max(this.Wg.ua);
    this.kb.setAttributeNS(p, "x", a.x);
    this.kb.setAttributeNS(p, "y", a.y);
    this.kb.setAttributeNS(p, "width", b.x - a.x);
    this.kb.setAttributeNS(p, "height", b.y - a.y)
  };
  rc.prototype.Ee = function() {
    return new g("Rectangle", [this.Vg.ua, this.Wg.ua])
  };
  Uo = {WC:function(a) {
    var b = 999999999, c = -999999999, d = 999999999, f = -999999999;
    a.lM(function(a) {
      a.x < b && (b = a.x);
      a.x > c && (c = a.x);
      a.y < d && (d = a.y);
      a.y > f && (f = a.y)
    });
    return{x:b, y:d, width:c - b, height:f - d}
  }, ay:function(a) {
    return new k(a.x, a.y)
  }};
  Go = k;
  qr = T;
  rr = rc;
  Ho = f
})();
function sr(a) {
  this.F = a.r();
  this.Pq = [];
  this.cq = [];
  this.ih = [];
  this.oG = new W(this.F);
  this.PF = new W(this.F);
  this.TC = new W(this.F);
  this.sB = new W(this.F);
  a.appendChild(this.sB.I);
  a.appendChild(this.PF.I);
  a.appendChild(this.oG.I);
  a.appendChild(this.TC.I)
}
z = sr.prototype;
z.oG = p;
z.PF = p;
z.TC = p;
z.sB = p;
z.Pq = p;
z.cq = p;
z.ih = p;
z.F = p;
z.GK = q;
function tr(a) {
  if(!a.GK) {
    var b, c, d, f, g = a.Pq.length;
    for(b = 0;b < g;b++) {
      if(a.Pq[b]) {
        var k = a.Pq[b].d;
        c = a.Pq[b].i;
        f = k.li();
        for(d = 0;d < f;d++) {
          a.ih.push({c:k.fb(d), s:k.af ? 1 : Math.sin(Rq(k, d) * Math.PI / 180), i:c})
        }
      }
    }
    g = a.cq.length;
    for(b = 0;b < g;b++) {
      if(a.cq[b]) {
        k = a.cq[b].d;
        c = a.cq[b].i;
        f = k.li();
        for(d = 0;d < f;d++) {
          a.ih.push({c:k.fb(d), s:k.af ? -1 : Math.sin(Rq(k, d) * Math.PI / 180), i:c})
        }
      }
    }
    g = a.ih.length;
    for(b = 0;b < g;b++) {
      a.ih[b].s = 0 <= a.ih[b].s ? a.ih[b].s + 2 * a.ih[b].i : a.ih[b].s - 2 * a.ih[b].i
    }
    a.ih.sort(function(a, b) {
      return a.s - b.s
    });
    for(b = 0;b < g;b++) {
      a.PF.appendChild(a.ih[b].c.I)
    }
    a.GK = j
  }
}
function pf() {
  Fk.call(this);
  this.Qd = new O;
  this.Jd = this.xy = q;
  this.Vk = {};
  this.Wh = []
}
A.e(pf, Fk);
z = pf.prototype;
z.Eb = p;
z.pE = q;
z.xy = q;
z.jH = p;
z.Jd = q;
z.Pi = 0.2;
z.ph = 0.45;
z.ck = p;
z.Vk = p;
z.Wh = p;
z.Qa = NaN;
z.Tb = NaN;
z.Qd = p;
z.ae = u("Qd");
z.oj = NaN;
z.Qo = NaN;
z.Kq = p;
z.ik = function() {
  return this.Qa * this.$t()
};
z.$t = function() {
  return this.Eb.ik()
};
z.Yj = -10;
z.bp = NaN;
z.rc = p;
z.Zr = function(a) {
  pf.f.Zr.call(this, a);
  "doughnut" == a && (this.xy = j)
};
z.br = function() {
  return new pr
};
z.jx = function() {
  return new nr
};
z.Mx = function(a, b, c, d) {
  return this.Eb ? this.Eb : this.Eb = pf.f.Mx.call(this, a, b, c, d)
};
z.vx = s();
z.yk = s();
z.RG = function(a, b) {
  return this.Eb.Sw() && this.Eb.Xo ? this.Eb.bI(b, this.Eb.Kv) : b
};
z.Dq = function(a) {
  C(a, "enable_3d_mode") && (this.Jd = K(a, "enable_3d_mode"));
  this.Jd !== j && kf(this, a)
};
z.EI = function(a, b, c) {
  function d(a, b, c, d) {
    c = Math.atan2(d.y - c.y, d.x - c.x);
    0 > c && (c += 2 * Math.PI);
    c = 180 * c / Math.PI;
    b = (a + b) % 360;
    c = (360 + c % 360) % 360;
    a = (36E5 + a) % 360;
    b = (36E5 + b) % 360;
    return a < b ? a - 5 <= c && c <= b + 5 : a - 5 <= c || c <= b + 5
  }
  function f(a, b) {
    return Math.sqrt((b.x - a.x) * (b.x - a.x) + (b.y - a.y) * (b.y - a.y))
  }
  var g = this.N.hb.aa;
  a.x = c.getAttribute("x") - g.D.Vb;
  a.y = c.getAttribute("y") - g.D.ma;
  c.setAttribute("x", a.x);
  c.setAttribute("y", a.y);
  for(var g = [], k = [{x:a.x, y:a.y}, {x:a.x + a.width, y:a.y}, {x:a.x, y:a.y + a.height}, {x:a.x + a.width, y:a.y + a.height}], l = 0;l < b.length;l++) {
    for(var n = b[l], m = new qr(n.D.I.firstChild), o = new rr(c), v = Ho.ZI(m, o), m = n.o.Tb, o = n.o.Qa, w = n.ae(), y = q, E = 0;E < v.Fa.length;E++) {
      var V = v.Fa[E], R = f(w, V);
      if(R > m - 5 && R < o + 5 && d(n.jb, n.kh, w, V)) {
        y = j;
        break
      }
    }
    if(y) {
      g.push(n)
    }else {
      y = j;
      for(v = 0;4 > v;v++) {
        if(E = k[v], R = f(w, E), R > m - 5 && R < o + 5) {
          if(!d(n.jb, n.kh, w, E)) {
            y = q;
            break
          }
        }else {
          y = q;
          break
        }
      }
      y ? g.push(n) : Xc(a, n.n()) && g.push(n)
    }
  }
  return g
};
z.Jo = x(16);
z.bk = 16;
function kr(a, b, c) {
  b = (a.xy ? a.Qa - a.Tb : a.Qa) * (c ? b : b + 1) / Hk(a);
  return a.xy ? a.Tb + b : b
}
z.ik = function() {
  return this.Qa * this.Eb.ik()
};
z.$t = function() {
  return this.Eb.ik()
};
z.qa = function() {
  pf.f.qa.call(this);
  this.ck && tr(this.ck)
};
z.om = function(a) {
  this.k = this.Jd ? ur(this, a) : vr(this, a);
  pf.f.om.call(this, a);
  this.Eb.fj && wr(this, a)
};
z.Ig = function() {
  return new P(0, 0, this.k.width, this.k.height)
};
z.ry = s();
z.p = function(a, b, c) {
  this.jH = new W(this.r());
  this.vw.ia(this.jH);
  var d = Hk(this);
  if(this.Jd) {
    this.ck = new sr(this.xt);
    var f = this.r(), g = f.gl, k = Cc(f, "filter");
    k.setAttribute("id", "smileBlur");
    f = Cc(f, "feGaussianBlur");
    f.setAttribute("in", "SourceGraphic");
    f.setAttribute("stdDeviation", "1.1");
    k.appendChild(f);
    g.appendChild(k)
  }
  pf.f.p.call(this, a, b, c);
  for(b = 0;b < d;b++) {
    g = this.o[b].Fe();
    for(c = 0;c < g;c++) {
      Ik(this, b).Ga(c).GB()
    }
  }
  this.pE = 1 == d;
  this.k = this.Jd ? ur(this, a) : vr(this, a);
  this.pE || (this.Eb.Nt = q);
  this.Eb.fj && (this.o[d - 1].fj = j, wr(this, a));
  return this.D
};
function vr(a, b) {
  var c = b, b = b.Ca(), d = Math.min(b.width, b.height);
  b.x += (b.width - d) / 2;
  b.y += (b.height - d) / 2;
  b.width = b.height = d;
  d /= 2;
  a.Qd.x = c.width / 2;
  a.Qd.y = c.height / 2;
  if(!(0 >= Hk(a))) {
    return a.pE && (a.Eb.xD || a.Eb.Nt) ? (c = d / (a.Eb.ng() + a.Eb.ik()), a.Qa = c * a.Eb.ng(), a.Tb = c * a.Eb.Tb) : (a.Qa = a.Eb.ng() * d, a.Tb = a.Eb.Tb * d), b
  }
}
function ur(a, b) {
  var c = b, b = b.Ca(), d = Math.min(b.width / 2, b.height / (2 * a.ph + a.Pi));
  b.x = b.x + b.width / 2 - d;
  b.y = b.y + b.height / 2 - d * (a.ph + a.Pi / 2);
  b.width = 2 * d;
  b.height = 2 * d * a.ph + d * a.Pi;
  a.Qd.x = c.width / 2;
  a.Qd.y = c.height / 2 - d * a.Pi / 2;
  c = Hk(a);
  0 < c && (1 == c && (a.Eb.xD || a.Eb.Nt) ? (d /= a.Eb.ng() + a.Eb.ik(), a.Qa = d * a.Eb.ng(), a.Tb = d * a.Eb.Tb) : (a.Qa = a.Eb.ng() * d, a.Tb = a.Eb.Tb * d), a.oj = a.Qa * a.ph, a.Qo = a.Tb * a.ph);
  return b
}
function wr(a, b) {
  var c = a.Eb.iE ? a.Eb.tE * a.Qa : a.Eb.tE, d = a.Eb.ex ? a.Eb.Xi : 0;
  a.rc = [];
  for(var f = a.o.length - 1, g = 0;g < Ik(a, f).Fe();g++) {
    var k = Ik(a, f).Ga(g);
    if(k.vc != p) {
      var l = (k.jb + k.Go()) * Math.PI / 360;
      k.Mb().ka().wa.padding = c;
      k.Mb().ka().dc.padding = c;
      k.Mb().ka().kc.padding = c;
      k.Mb().ka().Zb.padding = c;
      k.Mb().ka().gc.padding = c;
      k.Mb().ka().nc.padding = c;
      var n = k.Mb().n(k, k.Mb().ka().wa, k.fb());
      a.Qa = Math.min(a.Qa, Math.abs((b.width - 2 * (n.width + d)) / (2 * (1 + a.$t() + c / a.Qa) * Math.cos(l))));
      a.Qa = a.Jd ? Math.min(a.Qa, Math.abs((b.height * a.ph - 2 * n.height) / (2 * (1 + a.$t() + c / a.Qa) * a.ph * Math.sin(l))) / a.ph) : Math.min(a.Qa, Math.abs((b.height - 2 * n.height) / (2 * (1 + a.$t() + c / a.Qa) * Math.sin(l))));
      a.rc.push(new Yq((k.jb + k.Go()) / 2, n, k, a.rc.length));
      k.ap = a.rc.length - 1;
      -10 == a.Yj && (a.Yj = Math.acos(1 / (1 + c / a.Qa)))
    }
  }
  a.bp = c + a.Qa;
  a.Tb = a.Qa * a.Eb.Tb;
  if(a.Eb.zJ) {
    if(a.Eb.HK) {
      xr(a);
      var m;
      if(!a.Eb.Ss() && 0 < a.rc.length) {
        for(var o in a.rc) {
          m = a.rc[o], yr(m, q)
        }
        c = yr(a.rc[a.rc.length - 1]);
        for(o in a.rc) {
          m = a.rc[o];
          if(d = m != c) {
            f = a, g = m, d = c, k = ar(f, g, j), g = new P(k.x, k.y, g.ab, g.ob), f = ar(f, d, j), d = !Wc(g, new P(f.x, f.y, d.ab, d.ob))
          }
          d && (c = yr(m))
        }
      }else {
        for(o in a.rc) {
          m = a.rc[o], yr(m)
        }
      }
    }else {
      if(a.sm()) {
        o = a.rc.length - 1;
        for(d = 0;d <= o;d++) {
          if(c = a.rc[d], k = d == o ? a.rc[0] : a.rc[d + 1], f = ar(a, c, j), g = ar(a, k, j), f = new P(f.x, f.y, c.Ka(), c.vb()), g = new P(g.x, g.y, k.Ka(), k.Ka()), Wc(f, g) || !(c.ac > k.ac)) {
            k = Math.ceil(c.ac - 180 * a.Yj / Math.PI);
            for(l = c.ac;l > k;l--) {
              if(c.Wd(l), n = ar(a, c, j), f.x = n.x, f.y = n.y, !Wc(f, g)) {
                c.Wd(l);
                break
              }
            }
          }
        }
        a.Yw ? m = a.Yw() : a.sm && (m = a.sm());
        if(m) {
          m = a.rc.length - 1;
          for(c = 0;c <= m;c++) {
            if(o = a.rc[c], g = 0 == c ? a.rc[m] : a.rc[c - 1], d = ar(a, o, j), f = ar(a, g, j), d = new P(d.x, d.y, o.Ka(), o.vb()), f = new P(f.x, f.y, g.Ka(), g.vb()), Wc(d, f) || !(o.ac > g.ac)) {
              g = Math.ceil(o.ac + 180 * a.Yj / Math.PI);
              for(k = o.ac;k < g;k++) {
                if(o.Wd(k), l = ar(a, o, j), d.x = l.x, d.y = l.y, !Wc(d, f)) {
                  o.Wd(k);
                  break
                }
              }
            }
          }
          a.Yw || a.sm && a.sm()
        }
      }
      for(m = 0;m < a.rc.length;m++) {
        o = a.rc[m], o.isEnabled() && !zr(a, o) ? a.Eb.Ss() ? (o.yg(j), ir(o.j, j)) : (o.yg(q), ir(o.j, q)) : (o.yg(j), ir(o.j, j))
      }
    }
  }
}
function ar(a, b, c) {
  var d = b.j.ae();
  if(a.Jd) {
    var f = a.Qa * a.ph, g = a.bp - a.Qa;
    d.x += (a.Qa + g) * Math.cos(b.ac * Math.PI / 180);
    d.y += (f + g) * Math.sin(b.ac * Math.PI / 180);
    d.y += a.Qa * a.Pi / 2
  }else {
    d.x += a.bp * Math.cos(b.ac * Math.PI / 180), d.y += a.bp * Math.sin(b.ac * Math.PI / 180)
  }
  f = new P(0, 0, b.Ka(), b.vb());
  a.Eb.ex && (d.x = 90 < b.ac && 270 > b.ac ? d.x - a.Eb.Xi : d.x + a.Eb.Xi);
  c && (0 > Math.cos(b.ac * Math.PI / 180) && vd(d, 0, f, 0), yd(d, 1, f, 0));
  return d
}
function fr(a, b) {
  var b = b - a.ae().y, c;
  if(a.Jd) {
    c = a.Qa * a.ph;
    var d = a.bp - a.Qa, b = b - a.Qa * a.Pi / 2;
    c = 180 * Math.asin(b / (c + d)) / Math.PI
  }else {
    c = 180 * Math.asin(b / a.bp) / Math.PI
  }
  return isNaN(c) ? 0 < b ? 90.01 : -89.99 : c
}
function xr(a) {
  var b = [], c, d = a.rc.length;
  for(c = 0;c < d;c++) {
    b.push(new Zq(a.rc[c], 180 * Math.acos(a.Qa / a.bp) / Math.PI, a))
  }
  b.sort(function(a, b) {
    return a.hr - b.hr
  });
  a = [];
  d = b.length;
  for(c = 0;c < d;c++) {
    for(var f = b[c], g = a.pop();g != p;) {
      if(g.Yw(f)) {
        cr(g, f), f = g, g = a.pop()
      }else {
        a.push(g);
        break
      }
    }
    a.push(f)
  }
  d = a.length;
  for(c = 0;c < d;c++) {
    dr(a[c])
  }
}
function yr(a, b) {
  if(b == p || b == h) {
    b = j
  }
  a.yg(b);
  ir(a.j, b);
  return b ? a : p
}
z.sm = function() {
  for(var a = this.rc.length, b = 0;b < a;b++) {
    if(!zr(this, this.rc[b])) {
      return j
    }
  }
  return q
};
function zr(a, b) {
  for(var c = ar(a, b, j), c = new P(c.x, c.y, b.width, b.height), d = 0;d < a.rc.length;d++) {
    if(d != b.na) {
      var f = a.rc[d];
      if(f.isEnabled()) {
        var g = ar(a, f, j);
        if(Wc(c, new P(g.x, g.y, f.width, f.height))) {
          return q
        }
      }
    }
  }
  return j
}
;function Ar() {
  this.xg = this.Te = -1;
  this.xx = 1;
  this.Oi = this.oi = 5
}
z = Ar.prototype;
z.Te = p;
z.xg = p;
z.xx = p;
z.oi = p;
z.ur = q;
z.Oi = p;
z.vr = q;
z.g = function(a) {
  C(a, "columns") && (this.Te = M(a, "columns"));
  C(a, "rows") && (this.xg = M(a, "rows"));
  if(C(a, "direction")) {
    var b;
    a: {
      switch(N(a, "direction")) {
        default:
        ;
        case "vertical":
          b = 0;
          break a;
        case "horizontal":
          b = 1
      }
    }
    this.xx = b
  }
  C(a, "vertical_padding") && (b = F(a, "vertical_padding"), Mb(b) ? (this.Oi = Nb(b), this.vr = j) : this.Oi = Ob(b));
  C(a, "horizontal_padding") && (b = F(a, "horizontal_padding"), Mb(b) ? (this.oi = Nb(b), this.ur = j) : this.oi = Ob(b))
};
z.p = function(a) {
  -1 == this.Te && -1 != this.xg && (this.Te = Math.ceil(a.length / this.xg));
  -1 == this.Te && -1 == this.xg && (this.Te = 4, 4 >= a.length && (this.Te = a.length));
  this.xg = Math.ceil(a.length / this.Te)
};
z.eA = function(a, b) {
  var c = 0, d = 0, f = 0, g = 0;
  this.ur && (this.oi *= b.width);
  this.vr && (this.Oi *= b.height);
  for(var k = b.width - this.oi * (this.Te - 1), l = b.height - this.Oi * (this.xg - 1), n, m = 0;m < a.length;m++) {
    if(n = a[m], n.eA(new P(b.x + f, b.y + g, k / this.Te, l / this.xg)), 1 == this.xx && (d++, f += n.Ac.width + this.oi, d > this.Te - 1 && (f = d = 0, g += n.Ac.height + this.Oi, c++)), 0 == this.xx) {
      c++, g += n.Ac.height + this.Oi, c > this.xg - 1 && (g = c = 0, f += n.Ac.width + this.oi, d++)
    }
  }
};
function Br() {
  Fk.call(this);
  this.Jd = q;
  this.Iu = new Ar
}
A.e(Br, Fk);
z = Br.prototype;
z.Jd = p;
z.Iu = p;
z.Dq = function(a) {
  Br.f.Dq.call(this, a);
  C(a, "enable_3d_mode") && (this.Jd = q);
  F(a, "multiple_series_layout") && this.Iu.g(F(a, "multiple_series_layout"))
};
z.p = function(a, b, c) {
  a.Ca();
  b = Br.f.p.call(this, a, b, c);
  a = a.Ca();
  a.x = 0;
  a.y = 0;
  this.Y && Yd(this.Y, a);
  this.Iu.p(this.o);
  this.Iu.eA(this.o, a);
  return b
};
z.om = function(a) {
  a.Ca();
  var b = a.Ca();
  b.x = 0;
  b.y = 0;
  this.Y && Yd(this.Y, b);
  this.Iu.eA(this.o, b);
  Br.f.om.call(this, a)
};
z.Cb = function() {
  Br.f.Cb.call(this);
  this.b.add("%DataPlotMaxYValuePointName", "");
  this.b.add("%DataMaxYValuePointSeriesName", "");
  this.b.add("%DataPlotMinYValuePointName", "");
  this.b.add("%DataPlotMinYValuePointSeriesName", "");
  this.b.add("%DataPlotMaxYSumSeriesName", "");
  this.b.add("%DataPlotMinYSumSeriesName", "");
  this.b.add("%DataPlotMaxYSumSeries", 0);
  this.b.add("%DataPlotMinYSumSeries", 0)
};
z.Pa = function(a) {
  switch(a) {
    case "%DataPlotMaxYValuePointName":
    ;
    case "%DataMaxYValuePointSeriesName":
    ;
    case "%DataPlotMinYValuePointName":
    ;
    case "%DataPlotMinYValuePointSeriesName":
    ;
    case "%DataPlotMaxYSumSeriesName":
    ;
    case "%DataPlotMinYSumSeriesName":
      return 1;
    case "%DataPlotMaxYSumSeries":
    ;
    case "%DataPlotMinYSumSeries":
      return 2
  }
  return Br.f.Pa.call(this, a)
};
z.RE = function(a) {
  Br.f.RE.call(this, a);
  for(var b = a.Fe(), c = q, d = 0, f, g, k = 0;k < b;k++) {
    f = a.Ga(k), g = f.ta(), f.Dg = g, 0 == g && d++
  }
  d == b && (c = j);
  if(c) {
    for(k = 0;k < b;k++) {
      a.Ga(k).Dg = 1
    }
  }
};
var Cr = {chart:{styles:{funnel_style:{border:{type:"Solid", color:"DarkColor(%Color)"}, fill:{gradient:{key:[{position:"0", color:"%Color", opacity:"1"}, {position:"1", color:"Blend(DarkColor(%Color),%Color,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", color:"%Color", opacity:"0.9"}, effects:{bevel:{enabled:"true", distance:"1"}, enabled:"True"}, states:{hover:{border:{type:"Solid", color:"DarkColor(White)"}, fill:{gradient:{key:[{position:"0", color:"White", opacity:"1"}, {position:"1", color:"Blend(DarkColor(White),%White,0.7)", 
opacity:"1"}], angle:"90"}, type:"Gradient", color:"White", opacity:"0.9"}}, pushed:{border:{type:"Solid", color:"DarkColor(White)"}, fill:{gradient:{key:[{position:"0", color:"White", opacity:"1"}, {position:"1", color:"Blend(DarkColor(White),White,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", color:"White", opacity:"0.9"}, effects:{bevel:{enabled:"true", distance:"2", angle:"225"}}}, selected_normal:{border:{thickness:"2", color:"DarkColor(%Color)"}, hatch_fill:{enabled:"true", thickness:"3", 
type:"ForwardDiagonal", opacity:"0.3"}}, selected_hover:{fill:{gradient:{key:[{position:"0", color:"White", opacity:"1"}, {position:"1", color:"Blend(DarkColor(White),White,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", color:"White", opacity:"0.9"}, border:{thickness:"2", color:"DarkColor(White)"}, hatch_fill:{enabled:"true", thickness:"3", type:"ForwardDiagonal", color:"DarkColor(White)", opacity:"0.3"}}, missing:{border:{type:"Solid", color:"DarkColor(White)", thickness:"1", opacity:"0.4"}, 
fill:{gradient:{key:[{position:"0", color:"White", opacity:"1"}, {position:"1", color:"Blend(DarkColor(White),White,0.7)", opacity:"1"}], angle:"90"}, type:"Gradient", opacity:"0.3"}, hatch_fill:{type:"Checkerboard", pattern_size:"20", enabled:"true", opacity:"0.1"}, effects:{bevel:{enabled:"false", distance:"1", angle:"225"}}}}, name:"anychart_default"}}, chart_settings:{data_plot_background:{inside_margin:{all:"0"}}}, data_plot_settings:{funnel_series:{label_settings:{}, marker_settings:{}, tooltip_settings:{}, 
apply_palettes_to:"points", neck_height:"0.2", min_width:"0.2", mode:"circular", padding:"0.02", min_point_size:"0.02"}}}};
function Dr() {
}
A.e(Dr, cl);
Dr.prototype.zh = function() {
  return this.$f(Dr.f.zh.call(this), Cr)
};
function Er() {
  this.sa = new O;
  this.P = new O;
  this.X = new O;
  this.ha = new O;
  this.k = new P
}
z = Er.prototype;
z.k = p;
z.sa = p;
z.P = p;
z.X = p;
z.ha = p;
z.jD = u("ha");
z.Rn = p;
z.lm = p;
z.n = u("k");
z.jD = u("ha");
z.Kp = function(a, b, c) {
  var d = a.o.vb(), f = a.o.Ac.x, g = a.o.Ac.y;
  this.P.x = b ? f + c / 2 - a.oe / 2 * c : f + c / 2 - a.xe / 2 * c;
  this.ha.x = b ? f + c / 2 + a.oe / 2 * c : f + c / 2 + a.xe / 2 * c;
  this.sa.x = b ? f + c / 2 - a.xe / 2 * c : f + c / 2 - a.oe / 2 * c;
  this.X.x = b ? f + c / 2 + a.xe / 2 * c : f + c / 2 + a.oe / 2 * c;
  this.P.y = this.ha.y = b ? g + d - a.Re * d : g + a.Nd * d;
  this.sa.y = this.X.y = b ? g + d - a.Nd * d : g + a.Re * d
};
z.cw = function(a) {
  this.sa.x -= a;
  this.P.x -= a;
  this.X.x -= a;
  this.ha.x -= a
};
z.bg = function(a, b) {
  switch(a) {
    case 0:
      b.x = (this.ha.x + this.P.x) / 2;
      b.y = (this.sa.y + this.P.y) / 2;
      break;
    case 1:
      b.x = (this.P.x + this.sa.x) / 2;
      b.y = (this.P.y + this.sa.y) / 2;
      break;
    case 5:
      b.x = (this.ha.x + this.X.x) / 2;
      b.y = (this.ha.y + this.X.y) / 2;
      break;
    case 7:
      b.x = (this.ha.x + this.P.x) / 2;
      b.y = this.X.y;
      break;
    case 3:
      b.x = (this.ha.x + this.P.x) / 2;
      b.y = this.ha.y;
      break;
    case 2:
      b.x = this.P.x;
      b.y = this.P.y;
      break;
    case 4:
      b.x = this.ha.x;
      b.y = this.ha.y;
      break;
    case 8:
      b.x = this.sa.x;
      b.y = this.sa.y;
      break;
    case 6:
      b.x = this.X.x, b.y = this.X.y
  }
};
function Fr() {
  Er.call(this);
  this.de = new O;
  this.Ke = new O;
  this.Eg = new O;
  this.hg = new O;
  this.$k = new O;
  this.Wk = new O;
  this.qm = new O;
  this.oo = new O;
  this.zg = 0
}
A.e(Fr, Er);
z = Fr.prototype;
z.de = p;
z.Ke = p;
z.Eg = p;
z.hg = p;
z.$k = p;
z.Wk = p;
z.qm = p;
z.oo = p;
z.Vw = p;
z.zg = p;
z.TK = p;
z.bg = function(a, b, c) {
  switch(a) {
    case 0:
      b.x = (this.ha.x + this.P.x) / 2;
      b.y = (this.Eg.y + this.hg.y) / 2;
      break;
    case 1:
      b.x = (this.P.x + this.sa.x) / 2;
      b.y = (this.P.y + this.sa.y) / 2;
      this.P.y != this.de.y && (b.x = b.y <= this.de.y ? c ? this.P.x : this.P.x + (this.de.x - this.P.x) * (b.y - this.P.y) / (this.de.y - this.P.y) : c ? this.de.x - (this.de.x - this.sa.x) / (this.sa.y - this.de.y) * (b.y - this.de.y) : this.sa.x);
      break;
    case 5:
      b.x = (this.ha.x + this.X.x) / 2;
      b.y = (this.ha.y + this.X.y) / 2;
      this.ha.y != this.Ke.y && (b.x = b.y <= this.de.y ? c ? this.ha.x : this.ha.x - (this.ha.x - this.Ke.x) / (this.Ke.y - this.ha.y) * (b.y - this.ha.y) : c ? this.Ke.x + (this.X.x - this.Ke.x) / (this.X.y - this.Ke.y) * (b.y - this.Ke.y) : this.X.x);
      break;
    case 7:
      b.x = (this.ha.x + this.P.x) / 2;
      b.y = this.hg.y;
      break;
    case 3:
      b.x = (this.ha.x + this.P.x) / 2;
      b.y = this.Eg.y;
      break;
    case 2:
      b.x = this.P.x;
      b.y = this.P.y;
      break;
    case 4:
      b.x = this.ha.x;
      b.y = this.ha.y;
      break;
    case 8:
      b.x = this.sa.x;
      b.y = this.hg.y;
      break;
    case 6:
      b.x = this.X.x, b.y = this.X.y
  }
};
z.Kp = function(a, b, c) {
  var d = a.o.vb(), f = a.o.Ac.x, g = a.o.Ac.y, k = a.o.zg, l = a.o.Ac.width, n = a.o.Ac.height, m = a.cb.Zu, o = a.cb.rM * c, v = a.cb.IC, c = d * v;
  0 == k ? c > l && (c = l, d = l / v, g = (n - d) / 2) : c > k && (c = k, d = k / v, g = (n - d) / 2);
  this.zg = c - l;
  k = a.cb.Zl * d;
  a.Jd || (k = 0);
  this.lm = a.oe * k;
  this.Rn = a.xe * k;
  this.Vw = a.rq * k;
  this.Wk.x = this.hg.x = this.oo.x = this.qm.x = this.$k.x = this.Eg.x = f + l / 2;
  !b && 0 == m && 0 == o ? (this.Wk.y = g + d - a.Nd * d, this.oo.y = g + d - a.po * d, this.$k.y = g + d - a.Re * d, this.hg.y = this.Wk.y + 2 * this.lm, this.qm.y = this.oo.y + 2 * this.Vw, this.Eg.y = this.$k.y + 2 * this.Rn) : (this.hg.y = b ? g + a.Re * d : g + d - a.Nd * d, this.qm.y = b ? g + a.po * d : g + d - a.po * d, this.Eg.y = b ? g + a.Nd * d : g + d - a.Re * d, this.Wk.y = this.hg.y - 2 * this.lm, this.oo.y = this.qm.y - 2 * this.Vw, this.$k.y = this.Eg.y - 2 * this.Rn);
  this.sa.y = this.X.y = this.hg.y - this.lm;
  this.de.y = this.Ke.y = this.qm.y - this.Vw;
  this.P.y = this.ha.y = this.Eg.y - this.Rn;
  this.sa.x = f + l / 2 - a.oe * c / 2;
  this.de.x = f + l / 2 - a.rq * c / 2;
  this.P.x = f + l / 2 - a.xe * c / 2;
  this.X.x = f + l / 2 + a.oe * c / 2;
  this.Ke.x = f + l / 2 + a.rq * c / 2;
  this.ha.x = f + l / 2 + a.xe * c / 2
};
z.cw = function(a) {
  this.sa.x -= a;
  this.de.x -= a;
  this.P.x -= a;
  this.X.x -= a;
  this.Ke.x -= a;
  this.ha.x -= a;
  this.Eg.x -= a;
  this.hg.x -= a;
  this.$k.x -= a;
  this.Wk.x -= a;
  this.oo.x -= a;
  this.qm.x -= a
};
function Gr() {
  Fr.call(this)
}
A.e(Gr, Fr);
Gr.prototype.Kp = function(a, b, c) {
  var d = a.o.vb(), f = a.o.Ac.x, g = a.o.Ac.y;
  this.lm = a.oe * a.cb.Zl * d;
  this.Rn = a.xe * a.cb.Zl * d;
  a.Jd || (this.Rn = this.lm = 0);
  this.hg.x = this.Wk.x = this.$k.x = this.Eg.x = f + c / 2;
  this.hg.y = b ? g + d - a.Re * d : g + a.Re * d;
  this.Eg.y = b ? g + d - a.Nd * d : g + a.Nd * d;
  this.Wk.y = this.hg.y - 2 * this.lm;
  this.$k.y = this.Eg.y - 2 * this.Rn;
  this.sa.x = f + c / 2 - a.oe * c / 2;
  this.X.x = f + c / 2 + a.oe * c / 2;
  this.P.x = f + c / 2 - a.xe * c / 2;
  this.ha.x = f + c / 2 + a.xe * c / 2;
  this.sa.y = this.X.y = this.hg.y - this.lm;
  this.P.y = this.ha.y = this.Eg.y - this.Rn
};
Gr.prototype.bg = function(a, b) {
  switch(a) {
    case 0:
      b.x = (this.ha.x + this.P.x) / 2;
      b.y = (this.sa.y + this.P.y) / 2;
      break;
    case 1:
      b.x = (this.P.x + this.sa.x) / 2;
      b.y = (this.P.y + this.sa.y) / 2;
      break;
    case 5:
      b.x = (this.ha.x + this.X.x) / 2;
      b.y = (this.ha.y + this.X.y) / 2;
      break;
    case 7:
      b.x = (this.ha.x + this.P.x) / 2;
      b.y = this.X.y;
      break;
    case 3:
      b.x = (this.ha.x + this.P.x) / 2;
      b.y = this.ha.y;
      break;
    case 2:
      b.x = this.P.x;
      b.y = this.P.y;
      break;
    case 4:
      b.x = this.ha.x;
      b.y = this.ha.y;
      break;
    case 8:
      b.x = this.sa.x;
      b.y = this.sa.y;
      break;
    case 6:
      b.x = this.X.x, b.y = this.X.y
  }
};
function Hr() {
}
A.e(Hr, Nf);
Hr.prototype.Ta = function() {
  this.$a.Ta(this.I)
};
function Ir() {
}
A.e(Ir, Of);
Ir.prototype.Ta = function() {
  this.$a.Ta(this.I)
};
function Jr() {
}
A.e(Jr, Pf);
z = Jr.prototype;
z.Yc = function() {
  var a = this.j.ka();
  a.wn() && (this.Db = new Hr, this.Db.p(this.j, this));
  a.wi() && (this.Fb = new Ir, this.Fb.p(this.j, this))
};
z.createElement = function() {
  var a = this.j.r().ja();
  a.setAttribute("d", this.nl());
  return a
};
z.nl = function() {
  return this.j.nl()
};
z.update = function(a, b) {
  b && this.j.Rc();
  Jr.f.update.call(this, a, b)
};
z.Ta = function(a) {
  a.setAttribute("d", this.nl())
};
function Kr() {
  $.call(this)
}
A.e(Kr, Rf);
Kr.prototype.lc = function() {
  return new Jr
};
Kr.prototype.cc = function() {
  return Qf
};
function Lr(a) {
  switch(a) {
    default:
    ;
    case "outsideleft":
      return 1;
    case "outsideleftincolumn":
      return 0;
    case "outsiderightincolumn":
      return 2;
    case "outsideright":
      return 3;
    case "inside":
      return 4
  }
}
function Mr() {
}
A.e(Mr, Sn);
Mr.prototype.Ib = function(a, b, c) {
  return a.Jg(b, this, this.n(a, b, c))
};
function Nr() {
  this.Ge()
}
A.e(Nr, ho);
z = Nr.prototype;
z.oe = p;
z.xe = p;
z.Re = p;
z.Nd = p;
z.da = p;
z.wg = p;
z.fu = function(a) {
  return 1 - a * (this.o.ba.length - 1)
};
z.Dg = p;
z.p = function(a) {
  Nr.f.p.call(this, a)
};
z.update = function() {
  Nr.f.update.call(this);
  this.pC(this.r())
};
z.Wa = function() {
  Nr.f.Wa.call(this);
  this.nF()
};
z.nF = function() {
  if(this.kk()) {
    var a = this.r();
    this.Bf.setAttribute("d", this.Yt(a));
    this.Ab().xm.se() && this.Yz(a, this.Bf)
  }
};
z.Bf = p;
z.st = p;
z.pC = function(a) {
  this.kk() && (this.Bf = a.ja(), this.Bf.setAttribute("d", this.Yt(a)), this.Yz(a, this.Bf), this.D.appendChild(this.Bf))
};
z.Yz = function(a, b) {
  b.setAttribute("style", Md(this.Ab().xm, a, this.st))
};
z.Yt = function() {
  var a = this.Ab(), b = a.cp, c = a.xm, d = a.Xi;
  if(4 == b) {
    return""
  }
  var f = new O, a = new O, g = new O, k = new P, l = new O, l = this.be(this.Ea.ka()), k = this.Ea.n(this, l, this.vc), l = this.Jg(l, this.Ea, k);
  switch(b) {
    case 1:
    ;
    case 0:
      f.x = (this.da.P.x + this.da.sa.x) / 2;
      f.y = (this.da.P.y + this.da.sa.y) / 2;
      a.x = l.x + k.width;
      a.y = l.y + k.height / 2;
      g.y = a.y;
      g.x = a.x + d;
      break;
    case 3:
    ;
    case 2:
      f.x = (this.da.ha.x + this.da.X.x) / 2, f.y = (this.da.ha.y + this.da.X.y) / 2, a.x = l.x, a.y = l.y + k.height / 2, g.y = a.y, g.x = a.x - d
  }
  c.se() && (this.st = new P(Math.min(f.x, a.x), Math.min(f.y, a.y), Math.max(f.x, a.x) - Math.min(f.x, a.x), Math.max(f.y, a.y) - Math.min(f.y, a.y)));
  b = S(f.x, f.y);
  b += U(g.x, g.y);
  return b += U(a.x, a.y)
};
z.kk = function() {
  return this.vc && this.Ab().kk()
};
z.Rc = function() {
  var a = this.o.vb();
  this.k.x = Math.min(this.da.P.x, this.da.sa.x);
  this.k.y = Math.min(this.da.P.y, this.da.sa.y);
  this.k.width = Math.max(this.da.ha.x - this.da.P.x, this.da.X.x - this.da.sa.x);
  this.k.height = this.wg * a
};
z.EF = function() {
  this.oe = this.Re;
  this.xe = this.Nd
};
z.DK = function(a, b, c, d, f) {
  this.wg = a ? 1 / (this.o.ba.length - 1) : this.Dg < b ? b / d : this.Dg / d;
  this.wg *= this.fu(c, f);
  this.Re = this.o.Kf;
  this.o.Kf -= this.wg;
  this.Nd = this.o.Kf;
  this.o.Kf -= c
};
z.Jg = function(a, b, c) {
  var d = this.cb.cp, f = new O;
  if(4 == d) {
    return this.Wq(b, a, c)
  }
  1 == d ? (f.x = (this.da.P.x + this.da.sa.x) / 2, f.y = (this.da.P.y + this.da.sa.y) / 2, yd(f, 0, c, a.ga()), yd(f, 1, c, a.ga())) : 3 == d ? (f.x = (this.da.ha.x + this.da.X.x) / 2, f.y = (this.da.ha.y + this.da.X.y) / 2, yd(f, 1, c, a.ga()), yd(f, 1, c, a.ga())) : 2 == d && this.Ea != p ? (f.x = this.o.Ac.x + this.o.Ka() - this.Ea.n(this, this.Ea.ka().wa, b).width, f.y = (this.da.ha.y + this.da.X.y) / 2, yd(f, 1, c, a.ga())) : 0 == d && this.Ea != p && (f.x = this.o.Ac.x, f.y = (this.da.ha.y + 
  this.da.X.y) / 2, yd(f, 1, c, a.ga()));
  return f
};
function Or() {
  eo.call(this)
}
A.e(Or, Uq);
z = Or.prototype;
z.Ac = p;
z.eA = t("Ac");
z.Kf = 1;
z.tv = p;
z.Yp = p;
z.zg = 0;
z.vb = function() {
  return this.Ac.height
};
z.Ka = function() {
  return this.Ac.width
};
z.aA = function() {
  for(var a = new O, b = new P, c = this.Ka(), d = this.vb(), f = c / (2 * d), g, k = this.ba.length, l, n, m = 0;m < k;m++) {
    if(g = this.ba[m], g.Ea.isEnabled() && !g.Gb && (l = (g.da.X.x + g.da.jD.x) / 2, b = g.Mb().n(g, g.Mb().ka().wa, this.Mb().fb()), a = g.Jg(g.Mb().ka().wa, g.Mb(), b), n = g.Mb().ka().wa.ga(), g = (g.Re + g.Nd) * d / 2, a.x - b.width < this.Ac.x || a.x + b.width > this.Ka() || l + b.width > this.Ka())) {
      f = Math.min(f, (c - b.width - n) / (d + g))
    }
  }
  this.zg = 2 * d * f
};
function Pr(a) {
  for(var b = a.cb.cp, c = a.cb.Nb(), d, f = 0;f < a.ba.length;f++) {
    switch(d = a.ba[f], b) {
      case 1:
        d.da.Kp(d, c, a.zg);
        d.da.cw((a.zg - a.Ka()) / 2);
        break;
      case 3:
        d.da.Kp(d, c, a.zg);
        d.da.cw((a.Ka() - a.zg) / 2);
        break;
      case 0:
        d.da.Kp(d, c, a.zg);
        d.da.cw((a.zg - a.Ka()) / 2);
        break;
      case 2:
        d.da.Kp(d, c, a.zg), d.da.cw((a.Ka() - a.zg) / 2)
    }
  }
}
z.un = x(j);
z.Ug = function() {
  this.aA();
  Pr(this)
};
z.An = function() {
  this.aA();
  Pr(this)
};
function Qr() {
  fo.apply(this);
  this.Zl = 0.09;
  this.Ia = 0.02;
  this.hz = 0.01;
  this.cp = 1;
  this.Cm = this.Td = q;
  this.Xi = 5;
  this.IC = 0.6
}
A.e(Qr, go);
z = Qr.prototype;
z.Zl = p;
z.Ia = p;
z.ga = u("Ia");
z.hz = p;
z.xm = p;
z.kk = function() {
  return this.xm && this.xm.isEnabled()
};
z.xm = p;
z.Xi = p;
z.cp = p;
z.Td = p;
z.Nb = u("Td");
z.Cm = p;
z.IC = p;
z.ZG = x(q);
z.g = function(a, b) {
  Qr.f.g.call(this, a, b);
  C(a, "depth_3d") && (this.Zl = Sb(F(a, "depth_3d")));
  C(a, "padding") && (this.Ia = Sb(F(a, "padding")));
  C(a, "min_point_size") && (this.hz = Sb(F(a, "min_point_size")));
  C(a, "fit_aspect") && (this.IC = M(a, "fit_aspect"));
  C(a, "inverted") && (this.Td = K(a, "inverted"));
  C(a, "data_is_width") && (this.Cm = K(a, "data_is_width"));
  if(C(a, "label_settings")) {
    var c = F(a, "label_settings");
    C(c, "placement_mode") && (this.cp = Lr(Wb(c, "placement_mode")))
  }
  Ub(a, "connector") && (c = F(a, "connector"), this.xm = new Ld, this.xm.g(c), C(c, "padding") && (this.Xi = M(c, "padding")))
};
z.Aq = function() {
  return new Mr
};
function Rr() {
  this.da = new Fr;
  this.Ge()
}
A.e(Rr, Nr);
z = Rr.prototype;
z.po = p;
z.rq = p;
z.Jd = p;
z.Rc = function() {
  var a = this.cb.Zu, b = this.o.vb();
  this.k.x = Math.min(this.da.de.x, this.da.P.x, this.da.sa.x);
  this.k.width = this.Re < a && this.Nd < a ? this.da.ha.x - this.da.sa.x : Math.max(this.da.ha.x - this.da.P.x, this.da.X.x - this.da.sa.x);
  this.k.y = Math.min(this.da.sa.y, this.da.de.y, this.da.P.y);
  this.k.height = b * this.wg
};
z.EF = function() {
  if(this.cb.Cm) {
    if(this.na != this.o.ba.length - 1) {
      var a = this.na, b = this.cb.Sk;
      this.oe = (this.Dg - this.o.b.get("%SeriesYMin")) / (this.QN.b.get("%SeriesYMax") - this.QN.b.get("%SeriesYMin")) * (1 - b) + b;
      this.oe < b && (this.oe = b);
      this.o.Ga(a + 1) != p ? (this.xe = this.o.Ga(a + 1).Dg - this.o.b.get("%SeriesYMin") / (this.o.b.get("%SeriesYMax") - this.o.b.get("%SeriesYMin")) * (1 - b) + b, this.xe < b && (this.xe = b)) : this.xe = (this.Dg - this.o.b.get("%SeriesYMin")) / (this.o.b.get("%SeriesYMax") - this.o.b.get("%SeriesYMin")) * (1 - b) + b
    }
  }else {
    var a = this.cb.Zu, b = this.cb.Sk, c = b * (1 - a) / (1 - b), d = this.cb.Nb();
    this.oe = d ? (this.Re - a + c) / (1 - a + c) : (this.Nd - a + c) / (1 - a + c);
    this.xe = d ? (this.Nd - a + c) / (1 - a + c) : (this.Re - a + c) / (1 - a + c);
    this.po = this.Nd;
    this.rq = d ? this.xe : this.oe;
    this.Nd < a && this.Re < a ? this.rq = this.xe = this.oe = b : this.Nd < a && (this.po = a, this.rq = b, d ? this.xe = b : this.oe = b)
  }
};
z.fu = function(a, b) {
  var c = this.cb.Cm, d = this.cb.Sk, f = this.cb.Nb();
  this.o.b.get("%SeriesYSum");
  return 0 == this.cb.Sk && !c ? 1 - a * (this.o.Yp - 1) : f ? 1 - a * (this.o.Yp - 1) - 2 * d * b : 1 - a * (this.o.Yp - 1) - 2 * b
};
z.Jg = function(a, b, c) {
  var d = this.cb.cp, f = new O, g = this.cb.Nb();
  if(4 == d) {
    return this.Wq(b, a, c)
  }
  1 == d && (f.x = (this.da.P.x + this.da.sa.x) / 2, f.y = (this.da.P.y + this.da.sa.y) / 2, this.po != this.Nd && (f.x = f.y >= this.da.Ke.y ? g ? (this.da.de.x + this.da.sa.x) / 2 : this.da.de.x : g ? this.da.de.x : (this.da.de.x + this.da.P.x) / 2), vd(f, 0, c, a.ga()), yd(f, 1, c, a.ga()));
  3 == d && (f.x = (this.da.ha.x + this.da.X.x) / 2, f.y = (this.da.ha.y + this.da.X.y) / 2, this.po != this.Nd && (f.x = f.y >= this.da.Ke.y ? g ? (this.da.Ke.x + this.da.X.x) / 2 : this.da.Ke.x : g ? this.da.Ke.x : (this.da.Ke.x + this.da.ha.x) / 2), vd(f, 1, c, a.ga()), yd(f, 1, c, a.ga()));
  2 == d && this.Ea != p && (f.x = this.o.Ac.x + this.o.Ka() - this.Ea.n(this, this.Ea.ka().wa, b).width, f.y = (this.da.ha.y + this.bM.X.y) / 2, yd(f, 1, c, a.ga()));
  0 == d && this.Ea != p && (f.x = this.o.Ac.x, f.y = (this.da.ha.y + this.da.X.y) / 2, yd(f, 1, c, a.ga()));
  return f
};
z.bg = function(a, b) {
  this.da.bg(b, a, this.cb.Nb())
};
function Sr() {
  Rr.call(this);
  this.Jd = q
}
A.e(Sr, Rr);
Sr.prototype.p = function(a) {
  Sr.f.p.call(this, a);
  return this.D
};
Sr.prototype.nl = function() {
  return this.YB()
};
Sr.prototype.YB = function() {
  var a = S(this.da.P.x, this.da.P.y), a = a + U(this.da.de.x, this.da.de.y), a = a + U(this.da.sa.x, this.da.sa.y), a = a + U(this.da.X.x, this.da.X.y), a = a + U(this.da.Ke.x, this.da.Ke.y), a = a + U(this.da.ha.x, this.da.ha.y), a = a + U(this.da.P.x, this.da.P.y);
  return a + " Z"
};
Sr.prototype.fu = function(a) {
  return 1 - a * (this.o.Yp - 1)
};
function Tr() {
  Rr.call(this);
  this.Jd = j
}
A.e(Tr, Rr);
function Ur() {
  Rr.call(this);
  this.Jd = q;
  this.bM = new Gr
}
A.e(Ur, Rr);
z = Ur.prototype;
z.Sk = p;
z.DK = function(a, b, c, d, f) {
  this.Sk = this.o.b.get("%SeriesYMin") / this.o.b.get("%SeriesYMax");
  f = this.o.Ga(0);
  a = this.o.Ga(this.na - 1);
  f = this.cb.Nb() ? f.Dg / this.o.b.get("%SeriesYMax") * this.cb.Zl : 0;
  this.Jd || (f = 0);
  this.wg = 1 / (this.o.ba.length - 1);
  this.wg *= this.fu(c, f);
  this.Re = this.o.Kf;
  this.o.Kf -= this.wg;
  this.Nd = this.o.Kf;
  this.o.Kf -= c;
  this.Nd -= 2 * f;
  this.Re -= 2 * f;
  this.na == this.o.ba.length - 1 && (this.Nd = this.Re = a.Nd, this.wg = 0)
};
z.fu = function(a, b) {
  var c = this.o.Ga(0), d = this.o.Ga(this.o.ba.length - 1), b = this.cb.Nb() ? c.Dg / this.o.b.get("%SeriesYMax") * this.cb.Zl : d.Dg / this.o.b.get("%SeriesYMax") * this.cb.Zl;
  this.Jd || (b = 0);
  return 1 - a * (this.o.ba.length - 2) - 2 * b
};
z.EF = function() {
  var a = this.o.Ga(this.na + 1);
  this.oe = this.Dg / this.o.b.get("%SeriesYMax");
  this.xe = a != p ? a.Dg / this.o.b.get("%SeriesYMax") : this.oe
};
z.Rc = function() {
  var a = this.o.vb();
  this.k.x = Math.min(this.da.P.x, this.da.sa.x);
  this.k.width = Math.max(this.da.ha.x - this.da.P.x, this.da.X.x - this.da.sa.x);
  this.k.y = Math.min(this.da.sa.y, this.da.P.y);
  this.k.height = a * this.wg
};
z.Jg = function(a, b, c) {
  var d = this.cb.cp, f = new O, g = this.cb.Nb();
  if(4 == d) {
    return this.Wq(b, a, c)
  }
  1 == d && (f.x = (this.da.P.x + this.da.sa.x) / 2, f.y = (this.da.P.y + this.da.sa.y) / 2, vd(f, 0, c, a.ga()), yd(f, 1, c, a.ga()), f.x < this.o.Ac.x && (f.x = this.o.Ac.x));
  3 == d && (f.x = (this.da.ha.x + this.da.X.x) / 2, f.y = (this.da.ha.y + this.da.X.y) / 2, vd(f, 1, c, a.ga()), yd(f, 1, c, a.ga()), f.x + this.Ea.n(this, this.Ea.ka().wa, b).width > this.o.Ac.x + this.o.Ka() && (f.x = this.o.Ac.x + this.o.Ka() - this.Ea.n(this, this.Ea.ka().wa, b).width));
  2 == d && this.Ea != p && (f.x = this.o.Ac.x + this.o.Ka() - this.Ea.n(this, this.Ea.ka().wa, b).width, f.y = (this.da.ha.y + this.da.X.y) / 2, xd(f, 1, c, a.ga()));
  0 == d && this.Ea != p && (f.x = this.o.Ac.x, f.y = (this.da.ha.y + this.da.X.y) / 2, yd(f, 1, c, a.ga()));
  this.na == this.o.ba.length - 1 && (f.y = g ? f.y - c.height / 2 : f.y + c.height / 2);
  return f
};
function Vr() {
  Ur.call(this);
  this.Jd = j
}
A.e(Vr, Ur);
function Wr() {
  eo.call(this)
}
A.e(Wr, Or);
Wr.prototype.aA = function() {
  for(var a = new W(this.ca().r()), b = new O, c = new P, d = this.Ka(), f = this.vb(), b = this.cb.Zu * f, c = this.cb.Sk * d, g = b - (f - b) * c / (d - c), k = d / (f - g) / 2, l, n, m, o = 0;o < this.ba.length;o++) {
    if(l = this.ba[o], l.Mb().isEnabled() && !l.Gb && (n = (l.da.X.x + l.da.ha.x) / 2, c = l.Mb().n(l, l.Mb().ka().wa, a), b = l.Jg(l.Mb().ka().wa, p, c), m = l.Mb().ka().wa.ga(), l = (l.Re + l.Nd) * f / 2 - g, b.x - c.width < this.Ac.x || b.x + c.width > this.Ka() || n + c.width > this.Ka())) {
      k = Math.min(k, (d - c.width - m) / (f - g + l))
    }
  }
  this.zg = 2 * (f - g) * k
};
Wr.prototype.g = function(a, b) {
  Wr.f.g.call(this, a, b)
};
function Xr() {
  Qr.call(this)
}
A.e(Xr, Qr);
z = Xr.prototype;
z.Sk = p;
z.rM = u("Sk");
z.Zu = p;
z.ug = p;
z.ml = u("ug");
z.uh = function() {
  var a;
  a = this.aa.Jd ? this.Cm ? new Yr : new Zr : this.Cm ? new $r : new as;
  a.gh(19);
  return a
};
z.Ah = x("funnel_series");
z.Rb = x("funnel_style");
z.g = function(a, b) {
  Xr.f.g.call(this, a, b);
  C(a, "min_width") && (this.Sk = Sb(F(a, "min_width")));
  C(a, "neck_height") && (this.Zu = Sb(F(a, "neck_height")));
  if(C(a, "mode")) {
    switch(Wb(a, "mode")) {
      case "square":
        this.ug = bs;
        break;
      case "circular":
        this.ug = cs
    }
  }
};
z.Qb = function() {
  return new Kr
};
function as() {
  eo.call(this)
}
A.e(as, Wr);
as.prototype.qe = function() {
  return new Sr
};
as.prototype.Ug = function() {
  this.Kf = 1;
  for(var a, b = this.cb.Cm, c = this.cb.hz * this.b.get("%SeriesYSum"), d = this.Yp = this.tv = 0;d < this.ba.length;d++) {
    a = this.ba[d], this.Yp++, d == this.ba.length - 1 && b ? this.Yp-- : this.tv = a.Dg < c ? this.tv + c : this.tv + a.Dg
  }
  a = this.cb.ga();
  for(var b = this.cb.Cm, c = this.cb.Nb(), d = this.cb.hz * this.b.get("%SeriesYSum"), f = this.cb.Zl, g = 0;g < this.ba.length;g++) {
    var k = this.ba[g];
    k.DK(b, d, a, this.tv, f);
    k.EF();
    k.da.Kp(k, c, this.Ka())
  }
  as.f.Ug.call(this)
};
function Zr() {
  eo.call(this)
}
A.e(Zr, Wr);
Zr.prototype.qe = function() {
  return new Tr
};
Zr.prototype.jB = x(q);
function $r() {
  eo.call(this)
}
A.e($r, Wr);
$r.prototype.DA = p;
$r.prototype.qe = function() {
  return new Ur
};
$r.prototype.Ug = s();
$r.prototype.aA = function() {
  var a = new P, b, c;
  this.DA = this.Ac.Ka();
  for(var d = 0;d < this.ba.length;d++) {
    c = this.ba[d], c.Mb().isEnabled() && !c.Gb && (a = c.Mb().n(c, c.Mb().ka().wa, c.fb()), c.Jg(c.Mb().ka().wa, p, a), b = c.Mb().ka().wa.ga(), c = (c.oe + c.xe) / 2 * this.Ac.Ka(), b + a.width + c / 2 + this.Ac.Ka() / 2 > this.Ac.Ka() && (this.DA = Math.max(this.DA, b + a.width + c / 2 + this.Ac.Ka() / 2)))
  }
  this.zg = Math.ceil(this.Ac.Ka() - (this.DA - this.Ac.Ka()))
};
function Yr() {
  eo.call(this)
}
A.e(Yr, $r);
Yr.prototype.qe = function() {
  return new Vr
};
Yr.prototype.Ug = s();
Yr.prototype.An = s();
Yr.prototype.jB = x(q);
function qf() {
  Br.call(this);
  this.bk = 19
}
A.e(qf, Br);
z = qf.prototype;
z.br = function() {
  return new Dr
};
z.Xw = function(a) {
  return!C(a, "data") ? j : !C(F(a, "data"), "series")
};
z.jx = function(a) {
  switch(a) {
    case 19:
      return new Xr
  }
  return p
};
z.Jo = function(a) {
  switch(Vb(a)) {
    case "funnel":
      return 19;
    case "pyramid":
      return 22;
    case "cone":
      return 21;
    default:
      return this.bk
  }
};
z.vx = s();
z.yk = s();
z.ry = s();
z.Ig = function() {
  return new P(0, 0, this.k.width, this.k.height)
};
var bs = 0, cs = 1;
function ds(a, b, c, d, f) {
  a != p && a != h && (this.index = a);
  b && (this.data = b);
  c && (this.tf = c);
  d && (this.hv = d);
  f && (this.Ej = f)
}
z = ds.prototype;
z.data = p;
z.tf = p;
z.hv = p;
z.index = p;
z.Ej = p;
z.copy = function(a) {
  if(a == p || a == h) {
    a = new ds
  }
  a.index = this.index;
  a.data = this.data;
  a.tf = this.tf;
  a.hv = this.hv;
  a.Ej = this.Ej;
  return a
};
function es() {
  this.Ge()
}
A.e(es, Xn);
z = es.prototype;
z.Yb = p;
z.getParent = u("Yb");
z.Tl = t("Yb");
z.bb = p;
z.ta = u("bb");
z.Ln = t("bb");
z.ws = p;
z.FK = t("ws");
z.Vn = p;
z.JF = t("Vn");
z.Ym = p;
z.yF = t("Ym");
z.Ka = function() {
  return this.k.width
};
z.Bj = function(a) {
  this.k.width = a
};
z.vb = function() {
  return this.k.height
};
z.Rl = function(a) {
  this.k.height = a
};
z.Hd = function() {
  return this.k.x
};
z.Xd = function(a) {
  this.k.x = a
};
z.Sb = function() {
  return this.k.y
};
z.gd = function(a) {
  this.k.y = a
};
z.em = p;
z.Aj = function(a) {
  this.em = a;
  this.D && this.D.Aj(a);
  this.vc && this.vc.Gi(a);
  this.sd && this.sd.Gi(a);
  this.uf && this.uf.Gi(a)
};
z.Se = function() {
  es.f.Se.call(this);
  this.b.add("%Value", this.bb);
  this.b.add("%YValue", this.bb)
};
z.Pa = function(a) {
  return"%Value" == a || "%YValue" == a ? 2 : es.f.Pa.call(this, a)
};
function fs() {
  this.Ge()
}
A.e(fs, es);
fs.prototype.g = function(a, b) {
  fs.f.g.call(this, a, b);
  this.bb = C(a, "y") ? M(a, "y") : C(a, "value") ? M(a, "value") : 0;
  if(0 > this.bb || isNaN(this.bb)) {
    this.bb = 0
  }
};
fs.prototype.dk = function(a) {
  this.o = this.Yb;
  this.na = a.index;
  this.Sa = this.na.toString();
  Ok(this, a.data);
  this.cb = this.Yb.Ab();
  Pk(this, this.Yb.Qe);
  Kk(this, a.data);
  Qk(this, this.Yb);
  this.hl(a.data);
  this.rf(this.Yb.ka());
  this.Eq(a.data, a.tf);
  this.g(a.data);
  this.Yb.Mh(this);
  this.Dm(a.data, a.tf);
  this.Xa = C(a.data, "color") ? Yb(a.data) : this.Yb.Ba();
  this.Ze = C(a.data, "hatch_type") ? M(a.data, "hatch_type") : this.Yb.Ze;
  this.ge = this.Yb.mg();
  this.ib = this.Yb.Kg();
  C(a.data, "threshold") && (this.ib = this.ca().Li.Kg(N(a.data, "threshold"), a.hv));
  this.Yb.Ed(this);
  this.ib && this.ib.jt(this);
  0 <= this.bb && this.ca().Ua.push(this)
};
fs.prototype.createElement = function(a) {
  return a.ja()
};
fs.prototype.nl = function() {
  return gd(this.k)
};
function gs() {
  this.Ge()
}
A.e(gs, es);
z = gs.prototype;
z.dk = function() {
  this.o = this.Yb;
  this.na = this.Yb.index;
  this.Sa = this.Yb.getName();
  this.Xa = this.Yb.Ba();
  this.Ze = this.Yb.Ze;
  this.ge = this.Yb.mg();
  this.ib = this.Yb.Kg();
  this.rf(this.Yb.ka());
  this.cb = this.Yb.Ab();
  Qk(this, this.Yb);
  Pk(this, this.Yb.Qe);
  this.Yb.Ed(this)
};
z.p = function(a) {
  gs.f.p.call(this, a);
  if(this.K.wa.re() || this.K.kc.re() || this.K.dc.re() || this.K.Zb.re() || this.K.gc.re() || this.K.nc.re()) {
    this.Cc = (this.K.wa.re() ? this.K.wa.md() : this.K.kc.re() ? this.K.kc.md() : this.K.dc.re() ? this.K.dc.md() : this.K.Zb.re() ? this.K.Zb.md() : this.K.gc.re() ? this.K.gc.md() : this.K.nc.re() ? this.K.nc.md() : p).p(this.r()), this.D.ia(this.Cc)
  }
  this.ku = hs(this).jc(a, this.Xa, this.Ze);
  this.D.ia(this.ku);
  return this.D
};
z.update = function() {
  gs.f.update.call(this);
  is(this);
  js(this)
};
z.Wa = function() {
  gs.f.Wa.call(this);
  is(this);
  js(this)
};
function hs(a) {
  if(ks(a.K.wa) || ks(a.K.kc) || ks(a.K.dc) || ks(a.K.Zb) || ks(a.K.gc) || ks(a.K.nc)) {
    var b = ks(a.K.wa) ? a.K.wa.getHeader() : ks(a.K.kc) ? a.K.kc.getHeader() : ks(a.K.dc) ? a.K.dc.getHeader() : ks(a.K.Zb) ? a.K.Zb.getHeader() : ks(a.K.gc) ? a.K.gc.getHeader() : ks(a.K.nc) ? a.K.nc.getHeader() : p;
    b.uc(a.r(), b.Bh().ta(a));
    a.NI = b.n();
    return b
  }
}
function is(a) {
  if(a.ku) {
    if(ks(a.ya)) {
      var b = a.ya.getHeader();
      b.uc(a.r(), b.Bh().ta(a));
      b.update(a.ku, a.k);
      if((b = b.md()) && b.isEnabled()) {
        var c = new P(0, 0, a.k.width, Math.min(ls(a), a.k.height));
        b.qa(a.ku.Lx(), c)
      }
    }else {
      a.ku.Aj(q)
    }
  }
}
function ls(a) {
  if(!ks(a.ya)) {
    return 0
  }
  var b = a.ya.getHeader();
  if(b.vb()) {
    return b.vb()
  }
  hs(a);
  return a.NI ? a.NI.height : 0
}
function js(a) {
  a.Cc && a.ya.re() && a.ya.md().qa(a.Cc, a.k, a.Xa)
}
z.Jl = function(a) {
  gs.f.Jl.call(this, a);
  if(this.Ab().zC) {
    var a = this.ca(), b = this.Yb;
    b == a.$j ? b != a.Qz && ms(a, b.getParent()) : ms(a, b);
    a.Nz()
  }
};
function ns() {
  eo.call(this);
  this.k = new P;
  this.Gq = j
}
A.e(ns, eo);
z = ns.prototype;
z.Yb = p;
z.getParent = u("Yb");
z.Tl = t("Yb");
z.bb = NaN;
z.ta = u("bb");
z.Ln = t("bb");
z.ws = p;
z.FK = t("ws");
z.Vn = p;
z.JF = t("Vn");
z.Ym = p;
z.yF = t("Ym");
z.k = p;
z.ns = p;
z.ms = p;
z.Ka = function() {
  return this.k.width
};
z.Bj = function(a) {
  this.k.width = a
};
z.vb = function() {
  return this.k.height
};
z.Rl = function(a) {
  this.k.height = a
};
z.Hd = function() {
  return this.k.x
};
z.Xd = function(a) {
  this.k.x = a
};
z.Sb = function() {
  return this.k.y
};
z.gd = function(a) {
  this.k.y = a
};
z.em = p;
z.Aj = function(a) {
  var b = this.Fe();
  this.Lh && this.Lh.Aj(a);
  for(var c = 0;c < b;c++) {
    this.Ga(c).Aj(a)
  }
};
z.Lh = p;
function os(a) {
  a.Lh && a.Lh.Ln(a.bb)
}
z.Gq = p;
z.$r = t("ba");
z.qi = p;
z.yi = p;
z.dk = function(a) {
  this.na = a.index;
  this.Sa = this.na.toString();
  var b = a.data;
  Ok(this, b);
  this.Xa = C(b, "color") ? Yb(b) : zb("0xCCCCCC", h);
  this.Ze = C(b, "hatch_type") ? Gc(Wb(b, "hatch_type")) : 0;
  this.ge = 0;
  this.Yb && (this.cb = this.Yb.Ab(), this.Qe = this.Yb.Qe, Kk(this, b), Qk(this, this.cb), this.hl(b), this.K = this.Yb.ka(), this.Eq(b, a.tf));
  C(b, "threshold") && (this.ib = this.ca().Li.Kg(N(b, "threshold"), a.hv));
  this.ib ? a.Ej = this.ib : this.ib = a.Ej;
  this.Dm(b, a.tf);
  this.g(b, a.tf);
  this.qi = this.K.wa.qi;
  this.yi = this.K.wa.yi;
  var c = this.ca();
  c.o && this && c.o.push(this);
  this.Gq && (this.Lh = new gs, this.Lh.Tl(this), b = Zb(b), a.data = {"#name#":"point"}, this.Lh.dk(a), a.data = b, this.ca().Ua.push(this.Lh))
};
z.Ug = function() {
  this.Mw()
};
z.An = function() {
  this.Mw()
};
z.Mw = function() {
  if(0 != this.bb) {
    var a = this.k.width, b = this.k.height, c = this.k.x, d = this.k.y;
    if(this.Gq) {
      this.yi && (a -= this.yi.tb() + this.yi.Ja(), b -= this.yi.ra() + this.yi.rb(), c += this.yi.tb(), d += this.yi.rb());
      this.Lh.n().height = b;
      this.Lh.n().width = a;
      this.Lh.n().x = c;
      this.Lh.n().y = d;
      var f = ls(this.Lh), b = b - f, d = d + f;
      this.qi && (a -= this.qi.tb() + this.qi.tb(), b -= this.qi.ra() + this.qi.rb(), c += this.qi.tb(), d += this.qi.rb())
    }
    var g, f = this.ba.length, k = q;
    if(1 >= a) {
      k = j;
      for(g = 0;g < f;g++) {
        this.ba[g].n().width = 0
      }
    }
    if(1 >= b) {
      k = j;
      for(g = 0;g < f;g++) {
        this.ba[g].n().height = 0
      }
    }
    if(!k) {
      this.ns = a;
      this.ms = b;
      var k = 0, l, k = a * b / this.bb;
      for(g = 0;g < f;g++) {
        this.ba[g].FK(this.ba[g].ta() * k)
      }
      var n = k = 0, m = this.ns > this.ms;
      g = Number.MAX_VALUE;
      for(var o, v = 0, w = 0;n < f;) {
        l = n;
        o = m;
        for(var y = 0, E = 0, V = E = h, R = h, T = h, T = k;T <= l;T++) {
          y += this.ba[T].ws
        }
        o ? (V = this.ms / y, E = y / this.ms) : (E = this.ns / y, V = y / this.ns);
        for(T = k;T <= l;T++) {
          R = this.ba[T], o ? (R.yF(V * R.ws), R.JF(E)) : (R.JF(E * R.ws), R.yF(V))
        }
        R = this.ba[l];
        o = E = Math.max(R.Ym / R.Vn, R.Vn / R.Ym);
        if(o > g) {
          y = o = 0;
          for(g = k;g < n;g++) {
            l = this.ba[g], l.Xd(c + v + o), l.gd(d + w + y), m ? y += l.vb() : o += l.Ka()
          }
          m ? v += this.ba[k].Ka() : w += this.ba[k].vb();
          this.ns = a - v;
          this.ms = b - w;
          m = this.ns > this.ms;
          n = k = n;
          g = Number.MAX_VALUE
        }else {
          for(g = k;g <= n;g++) {
            l = this.ba[g], l.Bj(1 > l.Vn || isNaN(l.Vn) ? 1 : l.Vn), l.Rl(1 > l.Ym || isNaN(l.Ym) ? 1 : l.Ym)
          }
          g = o;
          n++
        }
      }
      b = a = 0;
      for(g = k;g < n;g++) {
        l = this.ba[g], l.Xd(c + v + a), l.gd(d + w + b), m ? b += l.vb() : a += l.Ka()
      }
      for(g = 0;g < f;g++) {
        l = this.ba[g], l.Mw && l.Mw()
      }
    }
  }
};
function ps() {
  this.cb = this;
  this.Aw = j;
  this.Ax = qs;
  this.Xo = this.zC = j
}
A.e(ps, fo);
z = ps.prototype;
z.Ax = p;
z.zC = p;
z.Kv = j;
z.bt = x(j);
z.FC = function(a, b) {
  b ? a.sort(function(a, b) {
    return b.ta() - a.ta()
  }) : a.sort(function(a, b) {
    return a.ta() - b.ta()
  })
};
z.Ah = x("tree_map");
z.Rb = x("tree_map_style");
z.Qb = function() {
  switch(this.Ax) {
    case rs:
      return new ss;
    case ts:
      return new us;
    default:
    ;
    case qs:
      return new vs
  }
};
z.Aq = function() {
  return new ws
};
z.g = function(a, b) {
  ps.f.g.call(this, a, b);
  C(a, "enable_drilldown") && (this.zC = F(a, "enable_drilldown"));
  if(C(a, "label_settings")) {
    var c = F(a, "label_settings");
    if(C(c, "drawing_mode")) {
      a: {
        switch(Wb(c, "drawing_mode")) {
          case "displayall":
            c = rs;
            break a;
          case "hide":
            c = ts;
            break a;
          default:
          ;
          case "crop":
            c = qs
        }
      }
      this.Ax = c
    }
  }
};
function xs() {
  this.yf = []
}
xs.prototype.DH = p;
xs.prototype.getData = u("DH");
xs.prototype.setData = t("DH");
xs.prototype.yf = p;
function ys() {
}
z = ys.prototype;
z.UD = p;
z.qv = p;
z.eF = p;
z.fr = p;
z.Rz = p;
z.Yu = p;
z.zA = p;
z.g = function(a) {
  var b;
  a: {
    switch(Wb(a, "input_mode")) {
      case "byparent":
        b = 1;
        break a;
      case "bylevel":
        b = 2;
        break a;
      default:
      ;
      case "bytree":
        b = 0
    }
  }
  this.UD = b;
  switch(this.UD) {
    case 2:
      this.fr = 0;
      this.qv = I(a, "point");
      this.eF = this.qv.length;
      break;
    case 1:
      var c = I(a, "point");
      b = c.length;
      this.Yu = {};
      this.zA = [];
      var d, f;
      for(f = 0;f < b;f++) {
        var g = c[f];
        d = new xs;
        d.setData(g);
        this.zA.push(d);
        C(g, "id") && (this.Yu[J(g, "id")] = d)
      }
      this.Rz = new xs;
      this.Rz.setData(a);
      for(f = 0;f < b;f++) {
        d = this.zA[f];
        a = d.getData();
        if(C(a, "parent") && (a = J(a, "parent"), this.Yu[a])) {
          this.Yu[a].yf.push(d);
          continue
        }
        this.Rz.yf.push(d)
      }
      b = this.zA;
      if(b != p) {
        d = b.length;
        for(f = 0;f < d;f++) {
          b[f] = p, delete b[f]
        }
      }
      b = this.Yu;
      if(b != p) {
        for(var k in b) {
          b[k] = p, delete b[k]
        }
      }
  }
};
function zs(a, b, c, d, f, g) {
  if(!b) {
    return p
  }
  if(g == p || g == h) {
    g = j
  }
  var k = b.yf, l = k.length;
  c.data = b.getData();
  if(0 < l || f) {
    b = new ns;
    b.Tl(d);
    var n = b;
    n.Gq = g;
    f ? (n.cb = f, Pk(n, f.Qe), Qk(n, f), n.rf(f.ka()), f.Mh(n)) : d && d.Mh(n);
    d = c.Ej;
    n.dk(c);
    f = [];
    n.$r(f);
    n.Ln(0);
    for(g = 0;g < l;g++) {
      var m = k[g];
      c.index = g;
      c.data = m.getData();
      m = zs(a, m, c, n);
      if(0 != m.ta()) {
        var o = m.ta();
        n.bb += o;
        f.push(m)
      }
    }
    c.Ej = d;
    Nk(n.Ab(), n.ba);
    os(n)
  }else {
    b = new fs, b.Tl(d), b.dk(c)
  }
  return b
}
function As(a, b) {
  return a.fr < a.eF ? Bs(a.qv[a.fr]) <= b ? p : a.qv[a.fr++] : p
}
function Bs(a) {
  return!C(a, "level") ? 0 : M(a, "level")
}
function Cs(a, b, c) {
  var d, f = Bs(b.data);
  if(a.fr >= a.eF ? 0 : Bs(a.qv[a.fr]) > f) {
    d = new ns;
    d.Tl(c);
    var g = d;
    g.cb = c.Ab();
    Pk(g, c.Ab().Qe);
    Qk(g, c.Ab());
    g.rf(c.Ab().ka());
    c.Ab().Mh(g);
    g.dk(b);
    c = [];
    g.$r(c);
    g.Ln(0);
    var k = 0;
    do {
      b.index = k++;
      b.data = As(a, f);
      if(!b.data) {
        break
      }
      var l = Cs(a, b, g), n = l.ta();
      g.bb += n;
      c.push(l)
    }while(1);
    Nk(g.Ab(), g.ba);
    os(g)
  }else {
    d = new fs, d.Tl(c), d.dk(b)
  }
  return d
}
function Ds(a, b, c, d, f) {
  if(!b) {
    return p
  }
  if(f == p || f == h) {
    f = j
  }
  var g, k = I(b.data, "point"), l = k.length;
  if(0 < l || d) {
    g = new ns;
    g.Tl(c);
    var n = g;
    n.Gq = f;
    d ? (n.cb = d, Pk(n, d.Qe), Qk(n, d), n.rf(d.ka()), d.Mh(n)) : c && c.Mh(n);
    c = b.Ej;
    n.dk(b);
    d = [];
    n.$r(d);
    n.Ln(0);
    for(f = 0;f < l;f++) {
      b.index = f;
      b.data = k[f];
      var m = Ds(a, b, n), o = m.ta();
      n.bb += o;
      d.push(m)
    }
    b.Ej = c;
    Nk(n.Ab(), n.ba);
    os(n)
  }else {
    g = new fs, g.Tl(c), g.dk(b)
  }
  return g
}
;function Es() {
}
A.e(Es, ng);
Es.prototype.Yc = function(a, b) {
  Es.f.Yc.call(this, a, b);
  this.kf && sd(this.kf, a.n())
};
Es.prototype.update = function(a) {
  Es.f.update.call(this, a);
  this.ps(a)
};
Es.prototype.ps = function() {
  this.kf && td(this.kf, new P(this.j.n().x - this.kf.getParent().Hd(), this.j.n().y - this.kf.getParent().Sb(), this.j.n().width, this.j.n().height))
};
function Fs() {
}
A.e(Fs, ng);
Fs.prototype.update = function(a) {
  Fs.f.update.call(this, a);
  this.ps(a)
};
Fs.prototype.ps = function(a) {
  var b = new P(this.j.n().x - this.kf.getParent().Hd(), this.j.n().y - this.kf.getParent().Sb(), this.j.n().width, this.j.n().height), a = this.j.Mb().n(this.j, a, this.kf);
  Xc(b, a) ? this.kf.Aj(j) : this.kf.Aj(q)
};
function Gs() {
  $.call(this)
}
A.e(Gs, og);
Gs.prototype.yA = p;
Gs.prototype.p = function(a, b) {
  switch(a.Ab().Ax) {
    case rs:
      this.yA = ng;
      break;
    case ts:
      this.yA = Fs;
      break;
    default:
    ;
    case qs:
      this.yA = Es
  }
  return Gs.f.p.call(this, a, b)
};
Gs.prototype.lc = function() {
  return new this.yA
};
var rs = 0, qs = 1, ts = 2;
function Hs() {
  ze.apply(this)
}
A.e(Hs, De);
Hs.prototype.ob = p;
Hs.prototype.vb = u("ob");
Hs.prototype.g = function(a) {
  Hs.f.g.call(this, a);
  C(a, "height") && (this.ob = M(a, "height"))
};
Hs.prototype.update = function(a, b, c, d) {
  Hs.f.update.call(this, a.r(), a, c, d);
  var f;
  switch(this.za) {
    case 0:
      f = b.x;
      break;
    case 1:
      f = b.x + (b.width - this.k.width) / 2;
      break;
    case 2:
      f = b.x + (b.width - this.k.width)
  }
  a.qb(f, b.y)
};
function Is() {
  ze.apply(this)
}
A.e(Is, Hs);
Is.prototype.jc = function(a, b, c) {
  a = Is.f.jc.call(this, a, b, c);
  sd(a, new P);
  return a
};
Is.prototype.update = function(a, b, c, d) {
  Is.f.update.call(this, a, b, c, d);
  td(a, new P(0, 0, b.width, b.height))
};
function Js() {
  ze.apply(this)
}
A.e(Js, Hs);
Js.prototype.update = function(a, b, c, d) {
  Js.f.update.call(this, a, b, c, d);
  b.width >= this.k.width && b.height >= this.k.height ? a.gu().setAttribute("visibility", "visible") : a.gu().setAttribute("visibility", "hidden")
};
function Ks(a) {
  W.call(this, a)
}
A.e(Ks, Df);
Ks.prototype.ps = function(a) {
  this.$a.ps(a)
};
function ws() {
}
A.e(ws, Sn);
ws.prototype.Qb = function() {
  return new Gs
};
ws.prototype.Wa = function(a, b, c) {
  ws.f.Wa.call(this, a, b, c);
  c.ps(b)
};
ws.prototype.xH = function(a) {
  return new Ks(a)
};
function Ls(a, b) {
  Se.call(this, a, b)
}
A.e(Ls, Qf);
z = Ls.prototype;
z.Y = p;
z.md = u("Y");
z.re = function() {
  return this.Y && this.Y.isEnabled()
};
z.qi = p;
z.yi = p;
z.lu = p;
z.getHeader = u("lu");
z.g = function(a) {
  a = Ls.f.g.call(this, a);
  if(!C(a, "branch")) {
    return a
  }
  var b = F(a, "branch");
  Ub(b, "background") && (this.Y = new Xd, this.Y.g(F(b, "background")));
  Ub(b, "inside_margin") && (this.qi = new Bd, this.qi.g(F(b, "inside_margin")));
  Ub(b, "outside_margin") && (this.yi = new Bd, this.yi.g(F(b, "outside_margin")));
  Ub(b, "header") && (this.lu = this.VB(), this.lu.g(F(b, "header")));
  return a
};
z.VB = function() {
  return new Hs
};
function ks(a) {
  return a.lu && a.lu.isEnabled()
}
function Ms(a, b) {
  Se.call(this, a, b)
}
A.e(Ms, Ls);
Ms.prototype.VB = function() {
  return new Is
};
function Ns(a, b) {
  Se.call(this, a, b)
}
A.e(Ns, Ls);
Ns.prototype.VB = function() {
  return new Js
};
function Os() {
}
A.e(Os, Pf);
Os.prototype.createElement = function() {
  return this.j.r().ja()
};
Os.prototype.Ta = function(a) {
  a.setAttribute("d", gd(this.j.n()))
};
function ss() {
  $.call(this)
}
A.e(ss, Rf);
ss.prototype.cc = function() {
  return Ls
};
ss.prototype.lc = function() {
  return new Os
};
function vs() {
  $.call(this)
}
A.e(vs, ss);
vs.prototype.cc = function() {
  return Ms
};
function us() {
  $.call(this)
}
A.e(us, ss);
us.prototype.cc = function() {
  return Ns
};
var Ps = {chart:{styles:{tree_map_style:{branch:{background:{border:{enabled:"true"}, fill:{enabled:"true"}, enabled:"false"}, inside_margin:{all:"0", top:"-1"}, outside_margin:{all:"0"}, header:{text:{value:"{%Name}"}, background:{inside_margin:{all:"0", left:"6", top:"2"}, enabled:"true"}, enabled:"true", align:"Near"}}, states:{normal:{branch:{header:{background:{fill:{gradient:{key:[{color:"#FDFDFD"}, {color:"#EBEBEB"}], angle:"90"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#898B8D"}, 
{color:"#5D5F60"}], angle:"90"}, enabled:"true", type:"Gradient", opacity:"1"}}, enabled:"true", align:"Near"}}, fill:{enabled:"true", type:"Solid", color:"%Color", opacity:"1"}, border:{enabled:"true", type:"Solid", color:"#898B8D", opacity:"1"}}, hover:{branch:{header:{font:{underline:"true"}, background:{fill:{gradient:{key:[{color:"#DFF2FF"}, {color:"#99D7FF"}], angle:"90"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#009DFF"}, {color:"#0056B8"}], angle:"90"}, type:"Gradient"}}}}, 
fill:{enabled:"true", type:"Solid", color:"%Color", opacity:"1"}, border:{enabled:"true", type:"Solid", color:"#898B8D", opacity:"1"}, hatch_fill:{enabled:"true", color:"White", type:"Percent50", opacity:"0.6"}}, pushed:{branch:{header:{font:{underline:"true"}, background:{fill:{gradient:{key:[{color:"#DFF2FF"}, {color:"#99D7FF"}], angle:"90"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#009DFF"}, {color:"#0056B8"}], angle:"90"}, type:"Gradient"}}}}, fill:{enabled:"true", type:"Solid", 
color:"%Color", opacity:"1"}, border:{enabled:"true", type:"Solid", color:"#898B8D", opacity:"1"}, hatch_fill:{enabled:"true", color:"Gray", type:"Percent50", opacity:"0.6"}}, selected_normal:{branch:{header:{background:{fill:{gradient:{key:[{color:"#FDFDFD"}, {color:"#EBEBEB"}], angle:"90"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#898B8D"}, {color:"#5D5F60"}], angle:"90"}, enabled:"true", type:"Gradient", opacity:"1"}}, enabled:"true", align:"Near"}}, fill:{enabled:"true", 
type:"Solid", color:"%Color", opacity:"1"}, border:{enabled:"true", type:"Solid", color:"#898B8D", opacity:"1"}, hatch_fill:{enabled:"true", color:"Gray", type:"Percent50", opacity:"0.7"}}, selected_hover:{branch:{header:{font:{underline:"true"}, background:{fill:{gradient:{key:[{color:"#DFF2FF"}, {color:"#99D7FF"}], angle:"90"}, enabled:"true", type:"Gradient"}, border:{gradient:{key:[{color:"#009DFF"}, {color:"#0056B8"}], angle:"90"}, type:"Gradient"}}}}, fill:{enabled:"true", type:"Solid", color:"%Color", 
opacity:"1"}, border:{enabled:"true", type:"Solid", color:"#898B8D", opacity:"1"}, hatch_fill:{enabled:"true", color:"Gray", type:"Percent50", opacity:"0.5"}, color:"%Color"}}, name:"anychart_default"}}, data_plot_settings:{tree_map:{label_settings:{position:{anchor:"Center", valign:"Center", halign:"Center"}, font:{bold:"false"}, format:{value:"{%Name}"}, enabled:"false"}, tooltip_settings:{format:{value:"{%Name} - {%YValue}{numDecimals:0}"}, enabled:"false"}, interactivity:{use_hand_cursor:"false", 
allow_select:"true"}, marker_settings:{}, enable_drilldown:"true"}}}};
function Qs() {
}
A.e(Qs, cl);
Qs.prototype.zh = function() {
  return this.$f(Qs.f.zh.call(this), Ps)
};
function of() {
  Fk.call(this)
}
A.e(of, Fk);
z = of.prototype;
z.Qz = p;
z.$j = p;
z.br = function() {
  return new Qs
};
z.Ab = function() {
  return Jk(this, 23)
};
z.jx = function() {
  return new ps
};
z.ux = function(a, b) {
  var c = F(a, "data"), d = new Uj;
  C(a, "palettes") && d.g(F(a, "palettes"));
  this.Li = new Dk(F(a, "thresholds"));
  var f = this.Mx(F(a, "data_plot_settings"), c, 23, b), d = new ds(0, c, b, d, p), g = new ys;
  g.g(c);
  var k;
  a: {
    switch(g.UD) {
      case 2:
        if(0 < I(d.data, "point").length && f) {
          c = new ns;
          c.Gq = q;
          c.Tl(p);
          c.cb = f;
          Pk(c, f.Qe);
          Qk(c, f);
          c.rf(f.ka());
          f.Mh(c);
          f = d.Ej;
          c.dk(d);
          c.$r([]);
          c.Ln(0);
          k = 0;
          do {
            d.index = k++;
            d.data = As(g, -Number.MAX_VALUE);
            if(!d.data) {
              break
            }
            var l = Cs(g, d, c), n = l.ta();
            c.bb += n;
            c.ba.push(l)
          }while(1);
          d.Ej = f;
          Nk(c.Ab(), c.ba);
          os(c)
        }else {
          c = p
        }
        k = c;
        break a;
      case 1:
        k = zs(g, g.Rz, d, p, f, q);
        break a;
      case 0:
        k = Ds(g, d, p, f, q)
    }
  }
  this.Qz = k;
  ms(this, this.Qz);
  Ek(this.Li)
};
z.yk = s();
function ms(a, b) {
  a.$j && a.$j.Aj(q);
  a.$j = b;
  a.rp[0] = a.$j;
  a.$j.Aj(j)
}
z.p = function(a, b, c) {
  of.f.p.call(this, a, b, c);
  Rs(this);
  td(this.D, this.Ig());
  return this.D
};
z.ry = s();
z.Ig = function() {
  return new P(0, 0, this.k.width, this.k.height)
};
z.Nz = function() {
  Rs(this);
  of.f.Nz.call(this);
  td(this.D, this.Ig())
};
function Rs(a) {
  a.Qz && (a.$j.Xd(0), a.$j.gd(0), a.$j.Bj(1 > a.k.width ? 1 : a.k.width - 1), a.$j.Rl(1 > a.k.height ? 1 : a.k.height - 1))
}
z.Xw = function(a) {
  return!a || !C(a, "data") || !C(F(a, "data"), "point")
};
function Ss(a) {
  switch(a) {
    default:
    ;
    case "near":
      return 0;
    case "center":
      return 1;
    case "far":
      return 2;
    case "spread":
      return 3
  }
}
function Ts(a) {
  switch(a) {
    default:
    ;
    case "right":
      return 1;
    case "left":
      return 0;
    case "top":
      return 2;
    case "bottom":
      return 3;
    case "float":
      return 4;
    case "fixed":
      return 5
  }
}
function Us() {
  this.Oa = new Bd(0)
}
z = Us.prototype;
z.Oa = p;
z.Aa = 1;
z.Ib = u("Aa");
z.za = 0;
z.qu = q;
z.NG = 0;
z.Df = u("NG");
z.ab = p;
z.Bj = t("ab");
z.Al = q;
z.hj = q;
z.ob = p;
z.Rl = t("ob");
z.zl = q;
z.si = q;
z.oi = 0;
z.ur = q;
z.cF = p;
z.Oi = 0;
z.vr = q;
z.dF = p;
z.Ir = p;
z.Fr = p;
z.Ko = -1;
z.ll = u("Ko");
z.Xp = -1;
z.pl = u("Xp");
z.NC = NaN;
z.OC = NaN;
z.Tc = j;
z.XD = q;
z.g = function(a) {
  C(a, "position") && (this.Aa = Ts(Wb(a, "position")));
  C(a, "align") && (this.za = Ss(Wb(a, "align")));
  C(a, "inside_dataplot") && (this.qu = K(a, "inside_dataplot"));
  C(a, "anchor") && (this.NG = Ad(Wb(a, "anchor")));
  var b;
  C(a, "width") && (b = F(a, "width"), this.ab = (this.Al = Mb(b)) ? Nb(b) : Ob(b), this.hj = j);
  C(a, "height") && (b = F(a, "height"), this.ob = (this.zl = Mb(b)) ? Nb(b) : Ob(b), this.si = j);
  C(a, "horizontal_padding") && (b = F(a, "horizontal_padding"), this.oi = (this.ur = Mb(b)) ? Nb(b) : Ob(b));
  C(a, "vertical_padding") && (b = F(a, "vertical_padding"), this.Oi = (this.vr = Mb(b)) ? Nb(b) : Ob(b));
  C(a, "halign") && (this.Ko = wd(Wb(a, "halign")));
  C(a, "valign") && (this.Xp = zd(Wb(a, "valign")));
  if(this.qu) {
    this.XD = this.Tc = j
  }else {
    if(C(a, "align_by")) {
      this.Tc = "chart" != Wb(a, "align_by"), this.XD = j
    }else {
      if(4 == this.Aa || 5 == this.Aa) {
        this.Tc = q
      }
    }
  }
  if(4 != this.Aa && 5 != this.Aa) {
    C(a, "padding") || H(a, "padding", 10);
    b = M(a, "padding");
    if(this.qu) {
      switch(this.Aa) {
        case 0:
          this.Oa.bh(b);
          break;
        case 1:
          this.Oa.eh(b);
          break;
        case 2:
          this.Oa.fh(b);
          break;
        case 3:
          this.margin.$g(b)
      }
    }else {
      switch(this.Aa) {
        case 0:
          this.Oa.eh(b);
          break;
        case 1:
          this.Oa.bh(b);
          break;
        case 2:
          this.Oa.$g(b);
          break;
        case 3:
          this.Oa.fh(b)
      }
    }
    C(a, "margin") && this.Oa.g(C(a, "margin"))
  }
};
z.Rc = function(a, b, c) {
  a.width = p;
  a.height = p;
  c = this.Tc ? c : b;
  if(4 != this.Aa && 5 != this.Aa && !this.qu) {
    switch(this.Aa) {
      case 0:
      ;
      case 1:
        this.ab && (a.width = this.Al ? b.width * this.ab : this.ab);
        this.ob && (a.height = this.zl ? c.height * this.ob : this.ob);
        break;
      case 2:
      ;
      case 3:
        if(this.ab && (a.width = this.Al ? c.width * this.ab : this.ab), this.ob) {
          a.height = this.zl ? b.height * this.ob : this.ob
        }
    }
  }else {
    if(this.ab && (a.width = this.Al ? c.width * this.ab : this.ab), this.ob) {
      a.height = this.zl ? c.height * this.ob : this.ob
    }
  }
  this.cF = this.ur ? c.width * this.oi : this.oi;
  this.dF = this.vr ? c.height * this.Oi : this.Oi
};
function Vs() {
  this.Za = new Us;
  this.k = new P;
  this.xb = new P;
  this.EC = new pa
}
z = Vs.prototype;
z.k = p;
z.n = u("k");
z.xb = p;
z.Y = p;
z.Cc = p;
z.Za = p;
z.D = p;
z.fb = u("D");
z.r = function() {
  return this.aa.r()
};
z.N = p;
z.aa = p;
z.pv = p;
z.p = function(a) {
  this.D = new W(a);
  this.Y && this.Y.isEnabled() && (this.Cc = this.Y.p(a), this.D.ia(this.Cc));
  A.G.nb(this.D.I, ka.gm, this.JN, q, this);
  return this.D
};
z.JD = function(a, b, c) {
  this.N = a;
  this.aa = b;
  this.pv = c;
  this.Bl = new ud(this.aa.r())
};
z.g = function(a) {
  this.Za.g(a);
  C(a, "background") && (a = F(a, "background"), Tb(a) && (this.Y = new Xd, this.Y.g(a)))
};
z.Rc = function(a, b) {
  this.Za.Rc(this.k, a, b);
  this.bl();
  this.Jn()
};
function Ws(a) {
  var b = a.k.width;
  Xs(a) && (b -= Ys(a));
  return b
}
z.Eo = function() {
  var a = this.k.height;
  Xs(this) && (a -= Zs(this));
  return a
};
z.Wx = function() {
  var a = this.Za.Fr;
  Xs(this) && (a -= Zs(this));
  return a
};
z.bl = function() {
  this.k.width += $s(this);
  this.k.height += at(this);
  if(Xs(this) && (this.Za.hj || (this.k.width += Ys(this)), !this.Za.si)) {
    this.k.height += Zs(this)
  }
};
z.Jn = function() {
  this.xb.x = 0;
  this.xb.y = 0;
  this.xb.width = this.k.width - $s(this);
  this.xb.height = this.k.height - at(this);
  Xs(this) && Cd(this.Y.Oa, this.xb)
};
z.qa = function() {
  bt(this);
  this.oC()
};
function bt(a) {
  a.D.Xd(a.k.x + a.Za.Oa.tb());
  a.D.gd(a.k.y + a.Za.Oa.rb())
}
z.oC = function() {
  this.Cc && this.Y.qa(this.Cc, this.Ig())
};
z.EC = p;
z.addEventListener = function(a, b, c, d) {
  this.EC.addEventListener(ka.gm, b, c, d)
};
z.JN = function(a) {
  a.target = this;
  this.EC.dispatchEvent(a)
};
function Xs(a) {
  return a.Y && a.Y.Oa
}
function Ys(a) {
  return a.Y.Oa.tb() + a.Y.Oa.Ja()
}
function Zs(a) {
  return a.Y.Oa.rb() + a.Y.Oa.ra()
}
function $s(a) {
  return a.Za.Oa.tb() + a.Za.Oa.Ja()
}
function at(a) {
  return a.Za.Oa.rb() + a.Za.Oa.ra()
}
z.Ig = function() {
  return new P(0, 0, this.k.width - $s(this), this.k.height - at(this))
};
z.Wa = function() {
  bt(this);
  this.Y && this.Y.Wa(this.Cc, this.Ig())
};
function ct() {
  De.apply(this)
}
A.e(ct, De);
z = ct.prototype;
z.yd = p;
z.nd = u("yd");
z.KJ = p;
z.Gn = p;
z.Tr = p;
z.g = function(a) {
  a && (ct.f.g.call(this, F(a, "title")), C(a, "title_separator") && (this.Gn = new Ld, this.Gn.g(F(a, "title_separator"))))
};
z.uc = function(a, b) {
  ct.f.uc.call(this, a, this.yc.ta(b));
  this.yd = this.k.height + this.Ia + 4;
  this.KJ = this.k.width;
  this.Gn && (this.yd += this.Gn.Va())
};
z.jc = function(a, b) {
  ct.f.jc.call(this, a);
  var c = new W(a), d = this.Ia, f;
  switch(this.za) {
    case 0:
      f = b.x;
      break;
    case 1:
      f = b.x + (b.width - this.k.width) / 2;
      break;
    case 2:
      f = b.Ja() - this.k.width
  }
  this.D.Xd(f);
  this.D.gd(d);
  c.ia(this.D);
  if(this.Gn) {
    d += this.k.height + this.Gn.Va() / 2 + 2;
    this.Tr = new P(b.x, d, b.width, this.Gn.Va() / 2);
    d = Md(this.Gn, a, this.Tr);
    f = a.ja(j);
    var g = S(this.Tr.tb(), this.Tr.y), g = g + U(this.Tr.Ja(), this.Tr.y);
    f.setAttribute("d", g);
    f.setAttribute("style", d);
    c.appendChild(f)
  }
  return c
};
function dt() {
  Vs.apply(this)
}
A.e(dt, Vs);
z = dt.prototype;
z.Ya = p;
z.mh = p;
z.NE = p;
z.g = function(a, b) {
  dt.f.g.call(this, a, b);
  Ub(a, "title") && (this.Ya = new ct, this.Ya.g(a), this.Ya.uc(this.aa.r(), this.aa))
};
z.Wx = function() {
  var a = dt.f.Wx.call(this);
  this.Ya && (a -= this.Ya.nd());
  return a
};
z.Eo = function() {
  var a = dt.f.Eo.call(this);
  this.Ya && (a -= this.Ya.nd());
  return a
};
z.bl = function() {
  if(this.Ya && (this.Za.hj || (this.k.width = Math.max(this.k.width, this.Ya.KJ)), !this.Za.si)) {
    this.k.height += this.Ya.nd()
  }
  dt.f.bl.call(this)
};
z.Jn = function() {
  dt.f.Jn.call(this);
  this.NE = this.xb.Ca();
  if(this.Ya) {
    var a = this.Ya, b = this.xb;
    b.y += a.yd;
    b.height -= a.yd
  }
};
z.qa = function() {
  dt.f.qa.call(this);
  this.Ya && (this.mh = this.Ya.jc(this.D.r(), this.NE), this.D.ia(this.mh))
};
z.Wa = function() {
  dt.f.Wa.call(this);
  this.Ya && (this.D.removeChild(this.mh), this.mh = this.Ya.jc(this.D.r(), this.NE), this.D.ia(this.mh))
};
function et() {
}
et.prototype.ye = function() {
  A.xa()
};
et.prototype.hi = function() {
  A.xa()
};
function ft() {
  this.controls = []
}
A.e(ft, et);
ft.prototype.controls = p;
ft.prototype.ye = function(a) {
  this.controls.push(a)
};
ft.prototype.hi = function(a, b) {
  for(var c = this.controls.length, d = 0;d < c;d++) {
    this.controls[d].Za.Ir = b.width, this.controls[d].Za.Fr = b.height, this.controls[d].Rc(b, a), gt(this.controls[d], this.controls[d].Za.Tc ? a : b)
  }
  return j
};
function gt(a, b) {
  var c = a.Za, d, f, g = new O(b.x, b.y);
  switch(c.Df()) {
    case 0:
      d = 2;
      f = 1;
      g.x += b.width / 2;
      g.y += b.height / 2;
      break;
    case 7:
      d = 2;
      f = 0;
      g.x += b.width / 2;
      g.y += b.height;
      break;
    case 1:
      f = d = 1;
      g.y += b.height / 2;
      break;
    case 5:
      d = 0;
      f = 1;
      g.x += b.width;
      g.y += b.height / 2;
      break;
    case 3:
      f = d = 2;
      g.x += b.width / 2;
      break;
    case 8:
      d = 1;
      f = 0;
      g.y += b.height;
      break;
    case 2:
      d = 1;
      f = 2;
      break;
    case 6:
      f = d = 0;
      g.x += b.width;
      g.y += b.height;
      break;
    case 4:
      d = 0, f = 2, g.x += b.width
  }
  d = -1 == c.ll() ? d : c.ll();
  f = -1 == c.pl() ? f : c.pl();
  var k = a.n().width, l = a.n().height;
  switch(d) {
    case 0:
      g.x -= k + c.cF;
      break;
    case 2:
      g.x -= k / 2;
      break;
    case 1:
      g.x += c.cF
  }
  switch(f) {
    case 0:
      g.y -= l + c.dF;
      break;
    case 1:
      g.y -= l / 2;
      break;
    case 2:
      g.y += c.dF
  }
  a.n().x = g.x;
  a.n().y = g.y
}
function ht() {
  ft.apply(this)
}
A.e(ht, ft);
function it() {
  ft.apply(this)
}
A.e(it, ft);
z = it.prototype;
z.kJ = q;
z.N = p;
z.jg = p;
z.PE = p;
z.Kf = p;
z.ye = function(a) {
  this.N = a.pv;
  a.addEventListener(ka.gm, this.uN, q, this);
  it.f.ye.call(this, a)
};
z.hi = function(a, b) {
  if(!this.N) {
    return j
  }
  if(this.kJ) {
    for(var c = this.N.n(), d = this.controls.length, f = 0;f < d;f++) {
      this.controls[f].Za.Ir = b.width, this.controls[f].Za.Fr = b.height, this.controls[f].Rc(b, a), isNaN(this.controls[f].Za.NC && !isNaN(this.controls[f].Za.OC)) ? gt(this.controls[f], this.controls[f].Za.Tc ? a : b) : (this.controls[f].n().x = this.controls[f].Za.NC * c.width, this.controls[f].n().y = this.controls[f].Za.OC * c.height)
    }
  }else {
    it.f.hi.call(this, a, b), this.kJ = j
  }
  return j
};
z.uN = function(a) {
  this.jg = a.target;
  this.PE = a.clientX;
  this.Kf = a.clientY;
  A.G.nb(this.jg.r().Nc(), ka.Yn, this.OJ, q, this);
  A.G.nb(this.jg.r().Nc(), ka.Ps, this.KK, q, this)
};
z.OJ = function(a) {
  if(this.jg) {
    var b = this.N.n(), c = this.jg.n(), a = new O(a.clientX - this.PE + c.x, a.clientY - this.Kf + c.y);
    a.x < b.tb() ? a.x = b.tb() : a.x + c.width > b.Ja() && (a.x = b.Ja() - c.width);
    a.y < b.rb() ? a.y = b.rb() : a.y + c.height > b.ra() && (a.y = b.ra() - c.height);
    this.jg.fb().qb(a.x, a.y)
  }
};
z.KK = function(a) {
  A.G.ed(this.jg.r().Nc(), ka.Yn, this.OJ, q, this);
  A.G.ed(this.jg.r().Nc(), ka.Ps, this.KK, q, this);
  this.PE = a.clientX;
  this.Kf = a.clientY;
  var a = this.jg.fb().Hd(), b = this.jg.fb().Sb();
  this.jg.n().x = a;
  this.jg.n().y = b;
  this.jg.Za.NC = a / this.N.n().width;
  this.jg.Za.OC = b / this.N.n().height;
  this.jg = p
};
function jt() {
  this.controls = [];
  this.Tc = j;
  this.YD = q
}
jt.prototype.controls = p;
jt.prototype.Tc = p;
jt.prototype.YD = p;
jt.prototype.ye = function(a) {
  a.Za.Tc || (this.Tc = q);
  this.controls.push(a)
};
function kt(a) {
  if(!a.YD && !a.Tc) {
    a.YD = j;
    for(var b = a.controls.length, c = 0;c < b;c++) {
      a.controls[c].Za.Tc = q
    }
  }
}
function lt(a) {
  this.Xf = a;
  this.tn = new jt;
  this.Yk = new jt;
  this.Mm = new jt
}
z = lt.prototype;
z.tn = p;
z.Yk = p;
z.Mm = p;
z.Xf = p;
z.nJ = u("Xf");
z.Dd = p;
z.ye = function(a) {
  switch(a.Za.za) {
    case 0:
      this.tn.ye(a);
      break;
    case 1:
      this.Yk.ye(a);
      break;
    case 2:
      this.Mm.ye(a);
      break;
    case 3:
      this.cG(a.Za), this.Yk.ye(a)
  }
};
z.cG = function() {
  A.xa()
};
z.Lq = function() {
  A.xa()
};
z.Rt = function() {
  A.xa()
};
function mt(a, b, c) {
  kt(a.tn);
  kt(a.Yk);
  kt(a.Mm);
  var d = a.zF(b, c), f = a.vF(b, c), b = a.xF(b, c);
  a.Lq(a.tn.controls, d);
  a.Lq(a.Mm.controls, b);
  a.Rt(a.Yk.controls, f);
  return Math.max(d, f, b)
}
z.zF = x(0);
z.vF = x(0);
z.xF = x(0);
z.Gk = function() {
  A.xa()
};
function nt(a) {
  lt.call(this, a)
}
A.e(nt, lt);
z = nt.prototype;
z.Lq = function(a, b) {
  for(var c = 0;c < a.length;c++) {
    var d = a[c];
    this.Xf ? d.n().y = this.Dd.ra() - b : d.n().y = this.Dd.rb() + b - d.n().height
  }
};
z.Rt = function(a, b) {
  for(var c = this.Xf ? this.Dd.ra() - b : this.Dd.rb() + b, d = 0;d < a.length;d++) {
    var f = a[d];
    this.Xf ? (f.n().y = c, c += f.n().height) : (f.n().y = c - f.n().height, c -= f.n().height)
  }
};
z.cG = function(a) {
  a.hj = j;
  a.Al = j;
  a.Bj(1)
};
z.zF = function(a, b) {
  for(var c = 0, d = (this.tn.Tc ? b : a).x, f = this.tn.controls, g = 0;g < f.length;g++) {
    var k = f[g];
    k.n().x = d;
    this.Gk(k.Za, a, b);
    k.Rc(a, b);
    d += k.n().width;
    c = Math.max(c, k.n().height)
  }
  return c
};
z.vF = function(a, b) {
  for(var c = 0, d = this.Yk.Tc ? b : a, d = d.x + d.width / 2, f = this.Yk.controls, g = 0;g < f.length;g++) {
    var k = f[g];
    this.Gk(k.Za, a, b);
    k.Rc(a, b);
    k.n().x = d - k.n().width / 2;
    c += k.n().height
  }
  return c
};
z.xF = function(a, b) {
  for(var c = 0, d = (this.Mm.Tc ? b : a).Ja(), f = this.Mm.controls, g = 0;g < f.length;g++) {
    var k = f[g];
    this.Gk(k.Za, a, b);
    k.Rc(a, b);
    k.n().x = d - k.n().width;
    d -= k.n().width;
    c = Math.max(c, k.n().height)
  }
  return c
};
z.Gk = function(a, b, c) {
  a.Ir = a.Tc ? c.width : b.width;
  a.Fr = 0.5 * b.height
};
function ot(a) {
  lt.call(this, a)
}
A.e(ot, nt);
ot.prototype.Lq = function(a) {
  for(var b = 0;b < a.length;b++) {
    var c = a[b];
    this.Xf ? c.n().y = this.Dd.ra() - c.n().height : c.n().y = this.Dd.rb()
  }
};
ot.prototype.Rt = function(a) {
  for(var b = this.Xf ? this.Dd.ra() : this.Dd.rb(), c = 0;c < a.length;c++) {
    var d = a[c];
    this.Xf ? (d.n().y = b - d.n().height, b -= d.n().height) : (d.n().y = b, b += d.n().height)
  }
};
ot.prototype.Gk = function(a, b, c) {
  a.Ir = a.Tc ? c.width : b.width;
  a.Fr = b.height
};
function pt(a) {
  lt.call(this, a)
}
A.e(pt, lt);
z = pt.prototype;
z.Lq = function(a, b) {
  for(var c = 0;c < a.length;c++) {
    var d = a[c];
    this.Xf ? d.n().x = this.Dd.Ja() - b : d.n().x = this.Dd.tb() + b - d.n().width
  }
};
z.Rt = function(a, b) {
  for(var c = this.Xf ? this.Dd.Ja() - b : this.Dd.tb() + b, d = 0;d < a.length;d++) {
    var f = a[d];
    this.Xf ? (f.n().x = c, c += f.n().width) : (f.n().x = c - f.n().width, c -= f.n().width)
  }
};
z.cG = function(a) {
  a.si = j;
  a.zl = j;
  a.Rl(1)
};
z.zF = function(a, b) {
  var c = 0, d = (this.tn.Tc ? b : a).rb(), f = this.tn.controls, g, k;
  for(g = 0;g < f.length;g++) {
    k = f[g], k.n().y = d, this.Gk(k.Za, a, b), k.Rc(a, b), d += k.n().height, c = Math.max(c, k.n().width)
  }
  return c
};
z.vF = function(a, b) {
  for(var c = 0, d = this.Yk.Tc ? b : a, d = d.y + d.height / 2, f = this.Yk.controls, g = 0;g < f.length;g++) {
    var k = f[g];
    this.Gk(k.Za, a, b);
    k.Rc(a, b);
    k.n().y = d - k.n().height / 2;
    c += k.n().width
  }
  return c
};
z.xF = function(a, b) {
  var c = 0, d = (this.Mm.Tc ? b : a).ra(), f = this.Mm.controls, g, k;
  for(g = 0;g < f.length;g++) {
    k = f[g], this.Gk(k.Za, a, b), k.Rc(a, b), k.n().y = d - k.n().height, d -= k.n().height, c = Math.max(c, k.n().width)
  }
  return c
};
z.Gk = function(a, b, c) {
  a.Fr = a.Tc ? c.height : b.height;
  a.Ir = 0.5 * b.width
};
function qt(a) {
  lt.call(this, a)
}
A.e(qt, pt);
qt.prototype.Lq = function(a) {
  for(var b = 0;b < a.length;b++) {
    var c = a[b];
    this.Xf ? c.n().x = this.Dd.Ja() - c.n().width : c.n().x = this.Dd.tb()
  }
};
qt.prototype.Rt = function(a) {
  for(var b = this.nJ ? this.Dd.Ja() : this.Dd.tb(), c = 0;c < a.length;c++) {
    var d = a[c];
    this.Xf ? (d.n().x = b - d.n().width, b -= d.n().width) : (d.n().x = b, b += d.n().width)
  }
};
qt.prototype.Gk = function(a, b, c) {
  a.maxHeight = a.Tc ? c.height : b.height;
  a.maxWidth = b.width
};
function rt() {
}
A.e(rt, et);
z = rt.prototype;
z.dp = p;
z.Ep = p;
z.Wp = p;
z.ko = p;
z.ye = function(a) {
  switch(a.Za.Ib()) {
    case 0:
      this.dp.ye(a);
      break;
    case 1:
      this.Ep.ye(a);
      break;
    case 2:
      this.Wp.ye(a);
      break;
    case 3:
      this.ko.ye(a)
  }
};
function st() {
  this.dp = new qt(q);
  this.Ep = new qt(j);
  this.Wp = new ot(q);
  this.ko = new ot(j)
}
A.e(st, rt);
st.prototype.hi = function(a, b) {
  this.dp.Dd = a;
  this.Ep.Dd = a;
  this.Wp.Dd = a;
  this.ko.Dd = a;
  mt(this.dp, b, a);
  mt(this.Ep, b, a);
  mt(this.Wp, b, a);
  mt(this.ko, b, a);
  return j
};
function tt() {
  this.OL = this.QL = this.PL = j;
  this.dp = new pt(q);
  this.Ep = new pt(j);
  this.Wp = new nt(q);
  this.ko = new nt(j)
}
A.e(tt, rt);
z = tt.prototype;
z.EH = p;
z.Oa = p;
z.ye = function(a) {
  if(!a.Za.XD) {
    switch(a.Za.Ib()) {
      case 0:
        a.Za.Tc = this.EH;
        break;
      case 1:
        a.Za.Tc = this.PL;
        break;
      case 2:
        a.Za.Tc = this.QL;
        break;
      case 3:
        a.Za.Tc = this.OL
    }
  }
  tt.f.ye.call(this, a)
};
z.hi = function(a, b) {
  var c = new Bd;
  this.Yr(c, a, b);
  var d = c.tb() == this.Oa.tb() && c.Ja() == this.Oa.Ja() && c.rb() == this.Oa.rb() && c.ra() == this.Oa.ra();
  this.Oa = c;
  return d
};
z.Nh = function(a, b) {
  this.Oa = new Bd;
  this.Yr(this.Oa, a, b);
  this.nx(a)
};
z.nx = function(a) {
  Cd(this.Oa, a)
};
z.Yr = function(a, b, c) {
  this.dp.Dd = c;
  this.Ep.Dd = c;
  this.Wp.Dd = c;
  this.ko.Dd = c;
  a.fh(mt(this.Wp, c, b));
  a.$g(mt(this.ko, c, b));
  a.bh(mt(this.dp, c, b));
  a.eh(mt(this.Ep, c, b))
};
function qk(a, b, c, d, f) {
  a && (this.Xa = a);
  b && (this.Ze = b);
  c != p && c != h && (this.ge = c);
  d && (this.QI = d);
  f && (this.am = f)
}
z = qk.prototype;
z.Xa = p;
z.Ba = u("Xa");
z.Di = t("Xa");
z.Ze = p;
z.ge = p;
z.mg = u("ge");
z.QI = p;
z.oy = u("QI");
z.am = p;
z.In = 0;
z.Jo = u("In");
function Gk(a) {
  this.aa = a
}
Gk.prototype.aa = p;
function ut(a, b, c) {
  for(var b = C(b, "series") ? J(b, "series") : p, d = Hk(a.aa), f = 0;f < d;f++) {
    var g = Ik(a.aa, f);
    if(!b || g.getName() == b) {
      var k;
      k = g.gb.ka().wa.Lt ? -1 == g.gb.Jb() ? g.mg() : g.gb.Jb() : g.gb.ka().wa.Jb();
      k = new qk(g.Ba(), g.Ze, k, g.Ab().oy(), g);
      k.In = g.Jb();
      pk(c, k)
    }
  }
}
function vt(a, b, c) {
  for(var b = C(b, "threshold") ? N(b, "threshold") : p, a = a.aa.Li.ZE, d = a.length, f = 0;f < d;f++) {
    var g = a[f];
    (!b || b == g.getName().toLowerCase()) && g.getData(c)
  }
}
function wt() {
  this.ge = Xf
}
z = wt.prototype;
z.ab = 20;
z.Ka = u("ab");
z.ob = 10;
z.vb = u("ob");
z.Xa = p;
z.Ba = u("Xa");
z.Xy = q;
z.ge = p;
z.mg = u("ge");
z.Ou = p;
z.Pu = p;
z.g = function(a) {
  if(a && (C(a, "width") && (this.ab = F(a, "width")), C(a, "height") && (this.ob = F(a, "height")), C(a, "color") && "%color" != J(a, "color") && (this.color = Yb(a)), C(a, "marker"))) {
    if(a = F(a, "marker"), C(a, "enabled") && (this.Xy = K(a, "enabled")), C(a, "type") && "%markertype" != N(a, "type") && (this.ge = Zf(Wb(a, "type"))), C(a, "color") && (this.Ou = Yb(a)), C(a, "size")) {
      this.Pu = M(a, "size")
    }
  }
};
z.copy = function(a) {
  a && (a = new wt);
  a.ab = this.ab;
  a.ob = this.ob;
  a.Xy = this.Xy;
  a.ge = this.ge;
  a.Pu = this.Pu;
  a.Ou = this.Ou;
  a.Xa = this.Xa;
  return a
};
function xt(a) {
  this.ge = Xf;
  this.aa = a;
  this.My = q;
  this.Ay = j
}
A.e(xt, wt);
z = xt.prototype;
z.My = p;
z.aa = p;
z.Ay = p;
z.In = p;
z.Jo = u("In");
z.g = function(a) {
  xt.f.g.call(this, a);
  C(a, "type") && (this.My = "box" == Wb(a, "type"));
  C(a, "series_type") && (a = Wb(a, "series_type"), "%SeriesType" != a && (this.Ay = q), this.In = a)
};
z.copy = function(a) {
  a || (a = new xt(this.aa));
  xt.f.copy.call(this, a);
  a.My = this.My;
  a.Ay = this.Ay;
  a.In = this.In;
  return a
};
function yt() {
}
A.e(yt, Ld);
yt.prototype.Ia = 1;
yt.prototype.ga = u("Ia");
yt.prototype.g = function(a) {
  yt.f.g.call(this, a);
  C(a, "padding") && (this.Ia = M(a, "padding"))
};
function zt(a) {
  W.call(this, a);
  this.xb = new P;
  A.G.nb(this.I, ka.Os, this.mj, q, this);
  A.G.nb(this.I, ka.Ns, this.Kl, q, this)
}
A.e(zt, W);
zt.prototype.xb = p;
zt.prototype.am = p;
zt.prototype.mj = function() {
  At(this, j)
};
zt.prototype.Kl = function() {
  At(this, q)
};
function At(a, b) {
  var c = a.am;
  if(c && c.K) {
    c.ya = b ? c.K.dc : c.K.wa;
    var d = b ? function(a) {
      a.ah()
    } : function(a) {
      a.dh()
    };
    if(c.ba) {
      for(var f = c.ba.length, g = 0;g < f;g++) {
        d(c.ba[g])
      }
    }else {
      c instanceof gr && d(c)
    }
  }
}
function Bt(a) {
  this.aa = a;
  this.Nq = this.Oq = NaN
}
z = Bt.prototype;
z.eg = p;
z.Lo = p;
z.ep = p;
z.Ju = p;
z.er = p;
z.Oz = p;
z.Sr = p;
z.dr = p;
z.Id = p;
z.aa = p;
z.Ul = t("aa");
z.Oq = p;
z.Nq = p;
z.g = function(a) {
  C(a, "format") ? Ct(this, J(a, "format")) : C(a, "text") && Ct(this, J(a, "text"));
  this.Id && (this.Id = this.Id.copy(), C(a, "icon") && this.Id.g(F(a, "icon")))
};
function Dt(a) {
  var b = "fill:" + ub(a.Ba()) + ";fill-opacity:1; stroke-thickness:1; stroke-opacity:1;stroke:", a = "darkcolor(" + ub(a.Ba()) + ")", a = zb(a, h);
  return b + a
}
function Ct(a, b) {
  a.Lo = q;
  a.er = q;
  var c = b.indexOf("{%Icon}");
  a.dr = -1 != c;
  if(a.dr) {
    if(0 < c && (a.Lo = j, a.yE = b.substring(0, c), a.Ju = new je(a.yE)), c < b.length - 7) {
      a.er = j, a.PN = b.substring(c + 7), a.Oz = new je(a.PN)
    }
  }else {
    a.Lo = j, a.yE = b, a.Ju = new je(a.yE)
  }
}
z.copy = function(a) {
  a || (a = new Bt);
  a.Id = this.Id;
  a.dr = this.dr;
  a.Lo = this.Lo;
  a.Ju = this.Ju;
  a.ep = this.ep;
  a.er = this.er;
  a.Sr = this.Sr;
  a.Oz = this.Oz;
  a.eg = this.eg;
  a.aa = this.aa;
  a.Oq = this.Oq;
  a.Nq = this.Nq;
  return a
};
function Et() {
  Vs.apply(this);
  this.Sz = this.dx = 2;
  this.Gm = this.Fn = 0;
  this.Ut = "{%Icon} {%Name}";
  this.Wi = this.dx;
  this.Ci = this.Sz
}
A.e(Et, dt);
z = Et.prototype;
z.dx = p;
z.Sz = p;
z.Gm = p;
z.Hz = p;
z.Ut = p;
z.Af = p;
z.Le = p;
z.Wi = p;
z.Ci = p;
z.Qr = p;
z.Bl = p;
z.Hn = p;
z.Ku = p;
z.RI = p;
z.Qk = p;
z.iJ = q;
z.ox = p;
z.iu = p;
z.zt = p;
z.Fn = p;
z.g = function(a, b) {
  Et.f.g.call(this, a, b);
  a = Zb(a);
  C(a, "width") && delete a.width;
  C(a, "height") && delete a.height;
  C(a, "columns_padding") && (this.dx = M(a, "columns_padding"));
  C(a, "rows_padding") && (this.Sz = M(a, "rows_padding"));
  C(a, "elements_align") && (this.Gm = wd(Wb(a, "elements_align")));
  C(a, "format") && (this.Ut = J(a, "format"));
  Ub(a, "columns_separator") && (this.Af = new yt, this.Af.g(F(a, "columns_separator")));
  Ub(a, "rows_separator") && (this.Le = new yt, this.Le.g(F(a, "rows_separator")));
  C(a, "ignore_auto_item") && (this.RI = K(a, "ignore_auto_item"));
  var c = a;
  this.iu = [];
  this.Ku = [];
  var d = this.zt = new Bt(this.aa);
  d.eg = new ze;
  d.eg.g(c);
  d.eg.Y = p;
  d.eg.kC = q;
  d.g(c);
  if(d.aa && (d.Id = new xt(d.aa), C(c, "icon") && d.Id.g(F(c, "icon")), C(c, "fixed_item_width") && (d.Oq = M(c, "fixed_item_width")), C(c, "fixed_item_height"))) {
    d.Nq = M(c, "fixed_item_height")
  }
  this.RI || (this.Ku.push({"#name#":"item", "#children#":[], source:"auto"}), this.iu.push(this.zt));
  if(C(c, "items")) {
    for(var c = I(F(c, "items"), "item"), d = c.length, f = 0;f < d;f++) {
      var g = c[f], k = this.zt.copy();
      k.g(g);
      this.iu.push(k);
      this.Ku.push(g)
    }
  }
  this.Wi = this.dx;
  this.Ci = this.Sz;
  this.Af && (this.Wi += this.Af.ga());
  this.Le && (this.Ci += this.Le.ga());
  this.Hz = this.Gm
};
z.p = function(a) {
  Et.f.p.call(this, a);
  this.Hn = new W(this.Bl.r());
  this.Bl.ia(this.Hn);
  return this.D
};
z.sy = function() {
  if(!this.iJ) {
    this.Qk = [];
    for(var a = this.Ku.length, b = 0;b < a;b++) {
      this.ox = this.iu[b];
      this.ox.Ul(this.aa);
      var c = this.Ku[b];
      if(C(c, "source")) {
        var d = this.aa.CJ, f = c;
        switch(Wb(c, "source")) {
          case "series":
            ut(d, f, this);
            break;
          case "points":
            c = C(f, "name") ? J(f, "name") : p;
            f = C(f, "series") ? J(f, "series") : p;
            for(d.aa.Xz = 0;d.aa.Xz < d.aa.o.length;) {
              var g;
              g = d.aa;
              var k = g.o[g.Xz];
              g.Xz++;
              g = k;
              if(!(f && g.getName() != f)) {
                for(var k = g.ba.length, l = 0;l < k;l++) {
                  var n = g.Ga(l);
                  if(n && (!c || c == n.getName())) {
                    n.Kg() && n.Kg().lt(n);
                    var m;
                    m = n.gb.ka().wa.Lt ? -1 == n.gb.Jb ? n.mg() : n.gb.Jb() : g.gb.ka().wa.mg();
                    n = new qk(n.Ba(), n.Ze, m, n.Ab().oy(), n);
                    n.In = g.Jb();
                    pk(this, n)
                  }
                }
              }
            }
            break;
          case "thresholds":
            vt(d, f, this);
            break;
          case "auto":
            ut(d, f, this), vt(d, f, this)
        }
      }else {
        pk(this, new qk(p, 0, -1, j, p))
      }
    }
    this.iu = this.ox = this.zt = p;
    this.iJ = j
  }
};
z.Jn = function() {
  Et.f.Jn.call(this)
};
z.qa = function() {
  Et.f.qa.call(this);
  this.Bl.qb(this.xb.x, this.xb.y);
  this.lC();
  this.D.ia(this.Bl)
};
z.Wa = function() {
  Et.f.Wa.call(this);
  this.Bl.qb(this.xb.x, this.xb.y);
  this.Hn && this.Hn.clear();
  this.lC()
};
function pk(a, b) {
  b.am || (b.am = a.aa);
  var c = a.ox, d = new zt(c.aa.r());
  c.Lo && (c.eg.uc(c.aa.r(), c.Ju.ta(b.am)), c.ep = c.eg.n(), d.xb.width += c.ep.width + 4, d.xb.height = Math.max(c.ep.height, d.xb.height));
  c.er && (c.eg.uc(c.aa.r(), c.Oz.ta(b.am)), c.Sr = c.eg.n(), d.xb.width += c.Sr.width, d.xb.height = Math.max(c.Sr.height, d.xb.height));
  c.dr && (d.xb.width += c.Id.Ka() + 4, d.xb.height = Math.max(c.Id.vb(), d.xb.height));
  var f = 0, g, k;
  c.Lo && (g = (d.xb.height - c.ep.height) / 2, k = c.eg.jc(c.aa.r(), b.Ba()), k.qb(f, g), d.ia(k), f += c.ep.width + 4);
  if(c.dr) {
    k = new W(c.aa.r());
    g = (d.xb.height - c.Id.vb()) / 2;
    c.aa.SH(c.Id, b, k, 0, 0);
    d.ia(k);
    if(b.oy() && c.Id.Xy) {
      var l = c.Id.mg() == Xf ? b.mg() : c.Id.mg();
      if(l != Xf) {
        var n = c.Id.Pu ? c.Id.Pu : 0.5 * Math.min(c.Id.ab, c.Id.ob), m = new Sf.jP(c.aa.r()), o = c.Id.Ou ? c.Id.Ou : b.Ba(), v = c.aa.r().ja(), l = bg.Io(l, c.aa.r(), n, 0, 0);
        v.setAttribute("style", Dt(o));
        v.setAttribute("d", l);
        m.appendChild(v);
        m.Xd((c.Id.Ka() - n) / 2);
        m.gd((c.Id.vb() - n) / 2);
        d.ia(m)
      }
    }
    k.Xd(f);
    k.gd(g);
    f += c.Id.Ka()
  }
  c.er && (g = (d.xb.height - c.Sr.height) / 2, k = c.eg.jc(c.aa.r(), b.Ba()), k.qb(f + 4, g), d.ia(k));
  d.am = b.am;
  isNaN(c.Oq) || (d.xb.width = c.Oq);
  isNaN(c.Nq) || (d.xb.height = c.Nq);
  a.Qk.push(d);
  a.Bl.ia(d)
}
function Ft() {
  Et.call(this);
  this.Te = 1
}
A.e(Ft, Et);
z = Ft.prototype;
z.Te = p;
z.yr = p;
z.ro = p;
z.Pp = p;
z.rt = p;
z.cx = p;
z.wc = p;
z.le = p;
z.g = function(a, b) {
  Ft.f.g.call(this, a, b);
  C(a, "columns") && (this.Te = M(a, "columns"))
};
z.sy = function() {
  Ft.f.sy.call(this);
  this.LD()
};
z.LD = function() {
  var a = this.Qk.length;
  this.yr = Math.ceil(a / this.Te);
  this.Te = Math.ceil(a / this.yr);
  this.rt = [];
  this.cx = [];
  for(var b = this.ro = this.Pp = 0, c = 0, d = 0;d < a;d++) {
    var f = this.Qk[d], b = Math.max(b, f.xb.width), c = c + f.xb.height;
    d == this.yr - 1 || d == this.Qk.length - 1 || 0 == (d + 1) % this.yr ? (this.cx.push(b), this.rt.length + 1 == this.Te || (b += this.Wi), this.rt.push(b), this.Pp += b, this.ro = Math.max(c, this.ro), c = b = 0) : c += this.Ci
  }
};
z.bl = function() {
  this.sy();
  this.le = this.wc = 0;
  this.Gm = this.Hz;
  this.Fn = 0;
  this.Za.si || (this.k.height = this.ro);
  var a = this.Wx();
  this.Za.si ? this.Eo() : this.k.height > a && (this.k.height = a);
  this.Qr = this.ro;
  if(this.Za.hj) {
    if(this.wc = 0, a = Ws(this) - this.Fn, this.Pp < a) {
      switch(this.Hz) {
        case 2:
          this.wc = (a - this.Pp) / 2;
          break;
        case 1:
          this.wc = a - this.Pp
      }
    }else {
      this.Gm = 0
    }
  }else {
    this.k.width = this.Pp + this.Fn
  }
  Ft.f.bl.call(this)
};
z.lC = function() {
  if(this.Af) {
    var a = new P(0, 0, this.Af.Va(), this.ro)
  }
  if(this.Le) {
    var b = new P(0, 0, 0, this.Le.Va())
  }
  var c = this.Bl.r(), d, f, g, k, l = this.wc, n = this.le, m = this.Qk.length, o = 0, v = this.Wi / 2, w = v;
  this.Af && (v += this.Af.ga() / 2, w -= this.Af.ga() / 2);
  for(var y = 0;y < m;y++) {
    d = this.Qk[y];
    d.Xd(l);
    d.gd(n);
    switch(this.Gm) {
      case 2:
        qd(d, (this.cx[o] - d.xb.width) / 2);
        break;
      case 1:
        qd(d, this.cx[o] - d.xb.width)
    }
    if(y == this.yr - 1 || y == this.Qk.length - 1 || 0 == (y + 1) % this.yr) {
      l += this.rt[o], n = 0, this.Af && o + 1 != this.Te && (g = l - this.Wi / 2, a && (a.x = g - this.Af.Va() / 2), d = c.ja(), f = S(g, 0), f += U(g, this.ro), g = Md(this.Af, c, a), d.setAttribute("d", f), d.setAttribute("style", g), this.Hn.appendChild(d)), o++
    }else {
      if(n += d.xb.height + this.Ci, this.Le) {
        k = n - this.Ci / 2;
        g = l;
        var E = this.rt[o] - v;
        0 < o && (g -= w, E += w);
        o + 1 == this.Te && (E += v);
        b && (b.width = E, b.x = g, b.y = k - this.Le.Va() / 2);
        d = c.ja();
        f = S(g, k);
        f += U(g + E, k);
        g = Md(this.Le, c, b);
        d.setAttribute("d", f);
        d.setAttribute("style", g);
        this.Hn.appendChild(d)
      }
    }
  }
};
function Gt() {
  this.ln = [];
  this.ma = this.ob = this.ab = 0
}
z = Gt.prototype;
z.ln = p;
z.ab = p;
z.Ka = u("ab");
z.Bj = t("ab");
z.ob = p;
z.vb = u("ob");
z.Rl = t("ob");
z.ma = p;
z.gd = t("ma");
z.Sb = u("ma");
function Ht(a, b) {
  for(var c = a.ln.length, d = 0;d < c;d++) {
    qd(a.ln[d], b)
  }
}
function It() {
  Et.call(this)
}
A.e(It, Et);
z = It.prototype;
z.jj = p;
z.xg = p;
z.bl = function() {
  this.sy();
  this.Gm = this.Hz;
  this.LD();
  It.f.bl.call(this)
};
z.LD = function() {
  var a;
  this.Za.hj ? a = Ws(this) : (a = this.Za.Ir, Xs(this) && (a -= Ys(this)));
  var b = this.Za.si ? this.Eo() : this.Wx();
  Jt(this, a);
  this.Za.hj || (this.k.width = this.jj + this.Fn);
  this.Za.si || (this.k.height = Math.min(this.Qr, b));
  if(this.Qr > b && (Jt(this, a - this.Fn), this.Za.hj || (this.k.width = this.jj + this.Fn), !this.Za.si)) {
    this.k.height = Math.min(this.Qr, b)
  }
};
function Jt(a, b) {
  a.jj = 0;
  a.xg = [];
  var c = a.Qk.length, d = a.Qr = 0, f = 0, g = q, k = new Gt;
  k.gd(f);
  for(var l = 0;l < c;l++) {
    var n = a.Qk[l];
    if(g = d + n.xb.width > b) {
      k.Bj(d - a.Wi), a.jj = Math.max(k.Ka(), a.jj), a.xg.push(k), f += k.vb() + a.Ci, d = 0, k = new Gt, k.gd(f)
    }
    n.Xd(d);
    n.gd(f);
    k.Rl(Math.max(k.vb(), n.xb.height));
    d += n.xb.width + a.Wi;
    k.ln.push(n)
  }
  k.Bj(d - a.Wi);
  a.xg.push(k);
  a.jj = Math.max(k.Ka(), a.jj);
  f += k.vb();
  a.Qr = f
}
z.lC = function() {
  if(this.Af) {
    var a = new P(0, 0, this.Af.Va(), 0)
  }
  if(this.Le) {
    var b = new P(0, 0, this.jj, this.Le.Va())
  }
  for(var c, d, f, g = this.Bl.r(), k = this.xg.length, l = 0;l < k;l++) {
    var n = this.xg[l], m = n.ln.length, o = 0;
    if(0 == this.Fn) {
      if(3 == this.Za.za) {
        var v = n;
        c = Ws(this);
        if(!(v.ab >= c)) {
          var w = v.ln.length;
          c /= w;
          for(d = 0;d < w;d++) {
            var y = v.ln[d];
            y.Xd(c * d + (c - y.xb.width) / 2)
          }
        }
      }else {
        switch(this.Gm) {
          case 2:
            o = (Ws(this) - this.jj) / 2;
            Ht(n, (Ws(this) - n.Ka()) / 2);
            break;
          case 1:
            o = Ws(this) - this.jj, Ht(n, Ws(this) - n.Ka())
        }
      }
    }
    if(this.Af) {
      v = n.Sb();
      w = n.vb();
      0 == l && 0 < k ? (w += this.Ci / 2, this.Le && (w -= this.Le.ga() / 2)) : 0 < l && l == k - 1 ? (c = this.Ci / 2, this.Le && (c -= this.Le.ga() / 2), w += c, v -= c) : (c = this.Ci, this.Le && (c -= this.Le.ga()), w += c, v -= c / 2);
      for(y = 0;y < m;y++) {
        y < m - 1 && (c = n.ln[y], f = c.Hd() + c.xb.width + this.Wi / 2, a.x = f - this.Af.HA / 2, a.y = v, a.height = w, c = g.ja(), d = S(f, v), d += U(f, v + w), f = Md(this.Af, g, a), c.setAttribute("d", d), c.setAttribute("style", f), this.Hn.appendChild(c))
      }
    }
    this.Le && l < k - 1 && (n = n.Sb() + n.vb() + this.Ci / 2, b.y = n - this.Le.Va() / 2, c = g.ja(), d = S(o, n), d += U(o + this.jj, n), f = Md(this.Le, g, b), c.setAttribute("d", d), c.setAttribute("style", f), this.Hn.appendChild(c))
  }
};
function Kt() {
  Ae(this);
  this.Ia = 5;
  this.b = new ne
}
A.e(Kt, ze);
z = Kt.prototype;
z.ix = p;
z.Ia = p;
z.ga = u("Ia");
z.yc = p;
z.ib = p;
z.vc = p;
z.F = p;
z.$c = p;
function Lt(a) {
  return a.$c[a.$c.length - 1]
}
z.uE = p;
z.Er = p;
z.Dr = p;
z.Qv = p;
z.Pv = p;
z.Hr = p;
z.Gr = p;
z.Sv = p;
z.Rv = p;
z.cz = p;
z.dz = p;
z.CE = p;
z.Su = p;
z.sf = p;
z.Rd = p;
z.bb = p;
z.g = function(a) {
  Kt.f.g.call(this, a);
  C(a, "format") && (this.yc = new je(J(a, "format")));
  C(a, "padding") && (this.Ia = M(a, "padding"))
};
z.p = function(a) {
  this.$c = [];
  this.uE = [];
  this.F = a;
  this.Su = this.CE = this.dz = this.cz = this.Sv = this.Rv = this.Hr = this.Gr = this.Qv = this.Pv = this.Er = this.Dr = 0;
  this.ib = this.ix.Kg();
  this.ix.Oe ? Mt(this, this.VM) : Mt(this, this.UM)
};
function Mt(a, b) {
  var c = a.ib.wb, d;
  if(a.ix.Nb()) {
    for(d = c - 1;0 <= d;d--) {
      b.call(a, d)
    }
  }else {
    for(d = 0;d <= c;d++) {
      b.call(a, d)
    }
  }
  c = a.ib.wb;
  for(d = 0;d < c;d++) {
    a.cz = Math.max(a.cz, a.$c[d].width + a.$c[d + 1].width), a.CE = Math.max(a.CE, a.$c[d].height + a.$c[d + 1].height)
  }
  if(2 < c) {
    for(d = 0;d <= c - 2;d++) {
      a.dz = Math.max(a.dz, a.$c[d].width + a.$c[d + 1].width), a.Su = Math.max(a.Su, a.$c[d].height + a.$c[d + 1].height)
    }
  }
}
z.VM = function(a) {
  this.sf = this.ib.Vc[a].Ub;
  this.Rd = this.ib.Vc[a].Lb;
  this.bb = this.sf + (this.Rd - this.sf) / 2;
  Nt(this);
  Ot(this, 0 == a % 2)
};
z.UM = function(a) {
  this.sf = this.Rd = this.bb = a != this.ib.wb ? this.ib.Vc[a].Ub : this.ib.Vc[a - 1].Lb;
  Nt(this);
  Ot(this, 0 == a % 2)
};
function Ot(a, b) {
  var c = a.yc.ta(a);
  a.uc(a.F, c);
  a.$c.push(a.n());
  a.uE.push(c);
  b ? (a.Qv += a.n().width, a.Pv += a.n().height, a.Er = Math.max(a.Er, a.n().width), a.Dr = Math.max(a.Dr, a.n().height)) : (a.Sv += a.n().width, a.Rv += a.n().height, a.Hr = Math.max(a.Hr, a.n().width), a.Gr = Math.max(a.Gr, a.n().height))
}
z.Kt = function(a, b, c) {
  this.uc(this.F, this.uE[c]);
  this.vc = this.jc(this.F);
  this.vc.qb(b.x, b.y);
  a.ia(this.vc)
};
z.b = p;
function Nt(a) {
  a.b.add("%RangeMin", a.sf);
  a.b.add("%RangeMax", a.Rd);
  a.b.add("%Value", a.bb)
}
z.Na = function(a) {
  return te(a) ? this.ib.Na(a) : this.b.get(a)
};
z.Pa = function(a) {
  if(te(a)) {
    return this.ib.Pa(a)
  }
  switch(a) {
    case "%RangeMin":
    ;
    case "%RangeMax":
    ;
    case "%Value":
      return 2
  }
  return 4
};
function Pt(a) {
  W.call(this, a);
  ld(this.I);
  this.k = new P;
  A.G.nb(this.I, ka.Os, this.mj, q, this);
  A.G.nb(this.I, ka.Ns, this.Kl, q, this)
}
A.e(Pt, W);
z = Pt.prototype;
z.K = p;
z.rf = t("K");
z.k = p;
z.n = u("k");
z.bx = p;
z.ba = p;
z.$r = t("ba");
z.ya = p;
z.Hg = p;
z.Mg = p;
z.mj = function() {
  this.ya = this.K.dc;
  for(var a = this.ba.length, b = 0;b < a;b++) {
    this.ba[b].ah()
  }
  this.update()
};
z.Kl = function() {
  this.ya = this.K.wa;
  for(var a = this.ba.length, b = 0;b < a;b++) {
    this.ba[b].dh()
  }
  this.update()
};
z.qa = function() {
  this.ya = this.K.wa;
  this.Hg = Mc(this.F);
  this.Mg = Mc(this.F);
  this.I.appendChild(this.Hg);
  this.I.appendChild(this.Mg);
  this.update()
};
z.update = function() {
  var a;
  this.ya.xc() ? (a = "", a = this.ya.Ie() ? a + Kd(this.ya.mc(), this.F, this.k, this.bx.Ba()) : a + md(), a = this.ya.Md() ? a + Md(this.ya.Lc(), this.F, this.k, this.bx.Ba()) : a + nd()) : a = od();
  this.Hg.setAttribute("style", a);
  this.Ta(this.Hg);
  a = this.ya.df() ? Hc(this.ya.De(), this.F, p, this.bx) : a + nd();
  this.Mg.setAttribute("style", a);
  this.Ta(this.Mg)
};
z.Ta = function(a) {
  a.setAttribute("x", this.k.x);
  a.setAttribute("y", this.k.y);
  a.setAttribute("width", this.k.width);
  a.setAttribute("height", this.k.height)
};
function Qt() {
  Vs.apply(this);
  this.Oe = q;
  this.Fj = 5;
  this.lj = this.Hf = this.yn = this.Zd = 0;
  this.ui = 2;
  this.Td = this.Jy = this.Ky = q;
  this.uy = 1;
  this.oE = q
}
A.e(Qt, dt);
z = Qt.prototype;
z.pf = p;
z.of = p;
z.LF = p;
z.KF = p;
z.xE = p;
z.mG = p;
z.jF = p;
z.zq = p;
z.O = p;
z.ui = p;
z.lj = p;
z.Hf = p;
z.lh = p;
z.Oe = p;
z.Fj = p;
z.yn = p;
z.Zd = p;
z.Td = p;
z.Nb = u("Td");
z.uy = p;
z.ib = p;
z.Kg = u("ib");
z.Ky = p;
z.Jy = p;
z.Bt = p;
z.At = p;
z.rj = p;
z.wc = p;
z.le = p;
z.Sy = p;
z.Ry = p;
z.Ty = p;
z.g = function(a, b) {
  Qt.f.g.call(this, a, b);
  if(C(a, "range_item")) {
    var c = F(a, "range_item");
    C(c, "width") && (this.LF = M(c, "width"));
    C(c, "height") && (this.KF = M(c, "height"));
    Ub(c, "background") && (this.jF = new Rf, this.jF.g(F(c, "background")))
  }
  if(C(a, "labels") && (c = F(a, "labels"), Tb(c))) {
    var d;
    a: {
      switch(Wb(c, "position")) {
        case "normal":
          d = 0;
          break a;
        default:
        ;
        case "combined":
          d = 2;
          break a;
        case "opposite":
          d = 1
      }
    }
    this.ui = d;
    this.O = new Kt;
    this.O.g(c);
    this.O.ix = this
  }
  Ub(a, "tickmark") && (c = F(a, "tickmark"), this.lh = new Ld, this.lh.g(c), C(c, "size") && (this.Fj = M(c, "size")));
  C(a, "tickmarks_placement") && (this.Oe = "center" == Wb(a, "tickmarks_placement"));
  C(a, "inverted") && (this.Td = K(a, "inverted"));
  F(a, "interval") && (this.uy = M(a, "interval"));
  C(a, "threshold") && (this.ib = this.aa.Li.qB[N(a, "threshold")])
};
z.p = function(a) {
  Qt.f.p.call(this, a);
  this.zq = new W(a);
  return this.D
};
z.Jn = function() {
  if(this.ib) {
    var a = this.aa.r();
    if(!this.oE) {
      this.O && this.O.p(a);
      this.rj = [];
      var b, c = this.ib.wb - 1;
      if(this.Td) {
        for(b = c;0 <= b;b--) {
          Rt(this, a, b)
        }
      }else {
        for(b = 0;b <= c;b++) {
          Rt(this, a, b)
        }
      }
      this.yn = this.Zd = 0;
      if(this.lh && this.lh.isEnabled()) {
        switch(this.ui) {
          case 2:
            this.Zd = 2 * this.Fj;
            this.yn = this.Fj;
            break;
          case 0:
            this.yn = this.Zd = this.Fj;
            break;
          case 1:
            this.Zd = this.Fj
        }
      }
      this.IB();
      this.oE = j
    }
    this.mG = this.xE = 0;
    !this.Za.hj && !this.Ky ? (this.pf = this.KB(), this.k.width = this.mq()) : this.Za.hj && !this.Ky ? (this.pf = this.Pw(), 0 > this.pf && (this.pf = 0, this.k.width = this.mq())) : !this.Za.hj && this.Ky ? (this.pf = this.LF, this.k.width = this.mq()) : (a = this.Pw(), this.pf = Math.min(a, this.LF), a = this.mq(), b = Ws(this), a < b && (this.xE = (b - a) / 2));
    !this.Za.si && !this.Jy ? (this.of = this.JB(), this.k.height = this.jq()) : this.Za.si && !this.Jy ? (this.of = this.Ow(), 0 > this.of && (this.of = 0, this.k.height = this.jq())) : !this.Za.si && this.Jy ? (this.of = this.KF, this.k.height = this.jq()) : (a = this.Ow(), this.of = Math.min(a, this.KF), a = this.jq(), b = this.Eo(), a < b && (this.mG = (b - a) / 2));
    Qt.f.Jn.call(this)
  }else {
    this.k.width = 0, this.k.height = 0, Qt.f.Jn.call(this, this)
  }
};
z.oE = p;
function Rt(a, b, c) {
  b = new Pt(b);
  b.rf(a.jF);
  b.bx = Tj(a.ib.qt, c);
  b.$r(a.ib.Vc[c].ba);
  a.rj.push(b)
}
z.qa = function() {
  Qt.f.qa.call(this);
  St(this, j);
  this.D.ia(this.zq)
};
function Tt(a, b, c, d) {
  var f = a.D.r().ja();
  f.setAttribute("style", Md(a.lh, a.D.r(), b));
  f.setAttribute("d", a.GI(b, c, d));
  a.zq.appendChild(f)
}
z.Wa = function() {
  Qt.f.Wa.call(this);
  St(this)
};
function St(a, b) {
  if(a.ib) {
    a.zq.clear();
    var c = new O, d = 0;
    a.cv();
    for(var f = a.rj.length, g = 0;g < f;g++) {
      a.Dv(a.rj[g].n(), g);
      b ? a.rj[g].qa() : a.rj[g].update();
      if(0 == g % a.uy) {
        a.lh && Tt(a, a.rj[g].n(), g, d);
        if(a.O && a.O.isEnabled()) {
          var k = a, l = c, n = a.rj[g].n(), m = g, o = d;
          k.qf(l, n, m, o);
          k.sm(m, l, o) || k.O.Kt(k.zq, l, m)
        }
        a.D.ia(a.rj[g])
      }
      d++
    }
    !a.Oe && 0 == f % a.uy && (a.lh && a.lh.isEnabled() && Tt(a, a.rj[f - 1].n(), f, d), a.O && a.O.isEnabled() && (g = a.rj[f - 1].n(), a.qf(c, g, f, d), a.sm(f, c, d) || a.O.Kt(a.zq, c, f)))
  }
}
z.sm = function(a, b, c) {
  a = this.O.$c[a].Ca();
  a.x = b.x;
  a.y = b.y;
  if(2 == this.ui) {
    if(0 == c % 2) {
      if(this.Ry) {
        if(Wc(this.Ry, a)) {
          return j
        }
      }else {
        this.Ry = a
      }
      this.Ry = a
    }else {
      if(this.Ty) {
        if(Wc(this.Ty, a)) {
          return j
        }
      }else {
        this.Ty = a
      }
      this.Ty = a
    }
  }else {
    if(this.Sy) {
      if(Wc(this.Sy, a)) {
        return j
      }
    }else {
      this.Sy = a
    }
    this.Sy = a
  }
  return q
};
z.cv = function() {
  this.wc = this.xb.tb() + this.xE;
  this.le = this.xb.rb() + this.mG
};
z.Dv = function(a) {
  a.width = this.pf;
  a.height = this.of
};
z.IB = s();
z.KB = x(NaN);
z.mq = x(NaN);
z.Pw = x(NaN);
z.JB = x(NaN);
z.jq = x(NaN);
z.Ow = x(NaN);
function Ut(a, b) {
  return 0 == a.ui || 2 == a.ui && 0 == b % 2
}
function Vt() {
  Qt.call(this);
  this.Bt = 20;
  this.At = 10
}
A.e(Vt, Qt);
z = Vt.prototype;
z.cv = function() {
  Vt.f.cv.call(this);
  this.le += this.lj + this.yn;
  if(this.O) {
    if(this.Oe) {
      var a = this.O.$c[0].width;
      a > this.pf && (this.wc += (a - this.pf) / 2)
    }else {
      this.wc += this.O.$c[0].width / 2
    }
  }
};
z.Dv = function(a, b) {
  Vt.f.Dv.call(this, a, b);
  a.x = this.wc + this.pf * b;
  a.y = this.le
};
z.JB = u("At");
z.Ow = function() {
  return this.Eo() - this.Zd - this.Hf
};
z.jq = function() {
  return this.of + this.Zd + this.Hf
};
z.KB = function() {
  if(!this.O || !this.O.isEnabled()) {
    return this.Bt
  }
  switch(this.ui) {
    case 2:
      return!this.Oe && 1 < this.ib.wb || this.Oe && 2 < this.ib.wb ? 0.25 * this.O.dz : this.Bt;
    case 0:
    ;
    case 1:
      return this.O.cz / 2
  }
  return NaN
};
z.mq = function() {
  var a = this.pf * this.ib.wb;
  this.O && this.O.isEnabled() && (this.Oe ? (this.O.$c[0].width > this.pf && (a += (this.O.$c[0].width - this.pf) / 2), Lt(this.O).width > this.pf && (a += (Lt(this.O).width - this.pf) / 2)) : a += this.O.$c[0].width / 2 + Lt(this.O).width / 2);
  return a
};
z.Pw = function() {
  var a = Ws(this);
  if(this.O && this.O.isEnabled()) {
    if(this.Oe) {
      var b = q, b = 2 == this.ui ? this.O.Qv <= a && this.O.Sv <= a : this.O.Qv + this.O.Sv <= a, c = this.O.$c[0].width, d = Lt(this.O).width, f = this.ib.wb;
      if(b) {
        b = a / this.ib.wb;
        if(b >= c && b >= d) {
          return b
        }
        b = (2 * a - c) / (2 * f - 1);
        if(b <= c && b >= d) {
          return b
        }
        b = (2 * a - d) / (2 * f - 1);
        if(b >= c && b <= d) {
          return b
        }
        b = (2 * a - c - d) / (2 * (f - 1));
        if(b <= c && b <= d) {
          return b
        }
      }
      return(2 * a - c - d) / (2 * (f - 1))
    }
    a -= this.O.$c[0].width / 2 + Lt(this.O).width / 2
  }
  return a / this.ib.wb
};
z.qf = function(a, b, c, d) {
  var f = this.O.$c[c];
  a.x = this.Oe ? b.x + (b.width - f.width) / 2 : (c == this.ib.wb ? b.Ja() : b.tb()) - f.width / 2;
  b = this.O.ga() + this.yn;
  a.y = Ut(this, d) ? this.le - b - f.height : this.le + this.of + b
};
z.IB = function() {
  this.lj = this.Hf = 0;
  if(this.O && this.O.isEnabled()) {
    switch(this.ui) {
      case 2:
        this.Hf = this.lj = this.O.Dr + this.O.ga();
        if(1 < this.ib.wb && this.Oe || 0 < this.ib.wb && !this.Oe) {
          this.Hf += this.O.Gr + this.O.ga()
        }
        break;
      case 0:
        this.lj = this.Hf = Math.max(this.O.Dr, this.O.Gr) + this.O.ga();
        break;
      case 1:
        this.Hf = Math.max(this.O.Dr, this.O.Gr)
    }
  }
};
z.GI = function(a, b, c) {
  a = this.Oe ? a.x + (a.width - 0) / 2 : (b == this.ib.wb ? a.Ja() : a.tb()) - 0;
  c = Ut(this, c) ? this.le - this.Fj : this.le + this.of;
  b = S(a, c);
  return b += U(a, c + this.Fj)
};
function Wt() {
  Qt.call(this);
  this.Bt = 10;
  this.At = 20
}
A.e(Wt, Qt);
z = Wt.prototype;
z.cv = function() {
  Wt.f.cv.call(this);
  this.wc += this.lj + this.yn;
  if(this.O) {
    if(this.Oe) {
      var a = this.O.$c[0].height;
      a > this.of && (this.le += (a - this.of) / 2)
    }else {
      this.le += this.O.$c[0].height / 2
    }
  }
};
z.Dv = function(a, b) {
  Wt.f.Dv.call(this, a, b);
  a.x = this.wc;
  a.y = this.le + this.of * b
};
z.JB = function() {
  if(!this.O || !this.O.isEnabled()) {
    return this.At
  }
  switch(this.ui) {
    case 2:
      return!this.Oe && 1 < this.ib.wb || this.Oe && 2 < this.ib.wb ? 0.25 * this.O.Su : this.At;
    case 0:
    ;
    case 1:
      return this.O.Su / 2
  }
  return NaN
};
z.Ow = function() {
  var a = this.of * this.ib.wb;
  this.O && this.O.isEnabled() && (this.Oe ? (this.O.$c[0].height > this.of && (a += (this.O.$c[0].height - this.of) / 2), Lt(this.O).height > this.of && (a += (Lt(this.O).height - this.of) / 2)) : a += this.O.$c[0].height / 2 + Lt(this.O).height / 2);
  return a
};
z.jq = function() {
  var a = this.Eo();
  if(this.O && this.O.isEnabled()) {
    if(this.Oe) {
      var b = q, b = 2 == this.ui ? this.O.Pv <= a && this.O.Rv <= a : this.O.Pv + this.O.Rv <= a, c = this.O.$c[0].height, d = Lt(this.O).height, f = this.ib.wb;
      if(b) {
        b = a / this.ib.wb;
        if(b >= c && b >= d) {
          return b
        }
        b = (2 * a - c) / (2 * f - 1);
        if(b <= c && b >= d) {
          return b
        }
        b = (2 * a - d) / (2 * f - 1);
        if(b >= c && b <= d) {
          return b
        }
        b = (2 * a - c - d) / (2 * (f - 1));
        if(b <= c && b <= d) {
          return b
        }
      }
      return(2 * a - c - d) / (2 * (f - 1))
    }
    a -= this.O.$c[0].height / 2 + Lt(this.O).height / 2
  }
  return a / this.ib.wb
};
z.KB = u("Bt");
z.mq = function() {
  return Ws(this) - this.Hf - this.Zd
};
z.Pw = function() {
  return this.pf + this.Hf + this.Zd
};
z.qf = function(a, b, c, d) {
  var f = this.O.Px(c);
  a.y = this.Oe ? b.y + (b.height - f.height) / 2 : (c == this.ib.wb ? b.ra() : b.rb()) - f.height / 2;
  b = this.O.ga() + this.yn;
  a.x = Ut(this, d) ? this.wc - b - f.width : this.wc + this.pf + b
};
z.IB = function() {
  this.lj = this.Hf = 0;
  if(this.O && this.O.isEnabled()) {
    switch(this.ui) {
      case 2:
        this.lj = this.O.Er + this.O.ga();
        this.Hf = this.lj + this.O.Hr + this.O.ga();
        break;
      case 0:
        this.lj = this.Hf = Math.max(this.O.Er, this.O.Hr) + this.O.ga();
        break;
      case 1:
        this.Hf = Math.max(this.O.Er, this.O.Hr) + this.O.ga()
    }
  }
};
z.GI = function(a, b, c) {
  a = this.Oe ? a.y + (a.height - 0) / 2 : (b == this.ib.wb ? a.ra() : a.rb()) - 0;
  c = Ut(this, c) ? this.wc - this.Fj : this.wc + this.pf;
  b = S(c, a);
  return b += U(c + this.Fj, a)
};
function Xt() {
  Vs.call(this)
}
A.e(Xt, Vs);
z = Xt.prototype;
z.Ya = p;
z.mh = p;
z.JD = function(a, b, c) {
  Xt.f.JD.call(this, a, b, c);
  this.Ya = new Ee(a, b)
};
z.g = function(a, b) {
  var c = Ve(b, "label_style", a, C(a, "style") ? F(a, "style") : p, q);
  Xt.f.g.call(this, c, b);
  this.Ya.g(c);
  this.Ya.Y = p
};
z.p = function(a) {
  Xt.f.p.call(this, a);
  this.Ya.uc(a, this.Ya.Bh().ta(this.aa));
  this.mh = this.Ya.jc(a);
  this.Ya.pu(this.D);
  return this.D
};
z.bl = function() {
  this.Ya.uc(this.aa.r(), this.Ya.Bh().ta(this.aa));
  var a = this.Ya.n();
  this.k.width || (this.k.width = a.width, 2 == this.Ya.Tp && (this.k.width += this.Ya.ga()));
  this.k.height || (this.k.height = a.height);
  Xt.f.bl.call(this)
};
z.qa = function() {
  Xt.f.qa.call(this);
  this.D.ia(this.mh);
  this.yx()
};
z.yx = function() {
  var a = this.xb.x, b = this.xb.y;
  switch(this.Ya.Tp) {
    case 0:
      a += this.Ya.ga();
      break;
    case 2:
      a += (this.xb.width - this.Ya.n().width) / 2;
      break;
    case 1:
      a += this.xb.width - this.Ya.n().width - this.Ya.ga()
  }
  this.mh.qb(a, b)
};
z.Wa = function() {
  Xt.f.Wa.call(this);
  this.yx()
};
function Yt() {
}
Yt.prototype.create = function(a, b) {
  if(!b) {
    return p
  }
  switch(b) {
    case "legend":
      var c;
      a: {
        if(C(a, "elements_layout") && "horizontal" != N(a, "elements_layout")) {
          c = new Ft
        }else {
          if(C(a, "position") && (c = N(a, "position"), "float" == c || "fixed" == c || "left" == c || "right" == c)) {
            c = new Ft;
            break a
          }
          c = new It
        }
      }
      return c;
    case "color_swatch":
      a: {
        switch(N(a, "orientation")) {
          default:
          ;
          case "vertical":
            c = new Wt;
            break a;
          case "horizontal":
            c = new Vt
        }
      }
      return c;
    case "image":
      return p;
    case "label":
      return new Xt
  }
  return new Ft
};
function Zt(a, b, c) {
  this.ym = [];
  this.N = a;
  this.aa = b;
  this.pv = c;
  this.mH = new Yt;
  this.MC = new it;
  this.LC = new ht;
  this.VD = new st;
  this.Mr = new tt
}
z = Zt.prototype;
z.D = p;
z.fb = u("D");
z.ym = p;
z.aa = p;
z.N = p;
z.pv = p;
z.mH = p;
z.VD = p;
z.Mr = p;
z.MC = p;
z.LC = p;
z.g = function(a) {
  C(a, "align_left_by") && (this.Mr.EH = "chart" == Wb(a, "align_left_by"))
};
z.Ft = function(a, b, c) {
  c = this.mH.create(a, c);
  c != p && (c.JD(this.N, this.aa, this.pv), c.g(a, b), 4 == c.Za.Ib() ? this.MC.ye(c) : 5 == c.Za.Ib() ? this.LC.ye(c) : c.Za.qu ? this.VD.ye(c) : this.Mr.ye(c), this.ym.push(c))
};
z.p = function(a) {
  this.D = new W(a);
  for(var b = this.ym.length, c = 0;c < b;c++) {
    this.D.ia(this.ym[c].p(a))
  }
};
z.qa = function() {
  for(var a = this.ym.length, b = 0;b < a;b++) {
    this.ym[b].qa()
  }
};
z.Wa = function() {
  for(var a = this.ym.length, b = 0;b < a;b++) {
    this.ym[b].Wa()
  }
};
z.Nh = function(a, b) {
  this.Mr.Nh(a, b)
};
z.nx = function(a) {
  this.Mr.nx(a)
};
z.hi = function(a, b) {
  return this.Mr.hi(a, b) ? (this.VD.hi(a, b), this.MC.hi(a, b), this.LC.hi(a, b), j) : q
};
function $t(a, b) {
  Ee.call(this, a, b)
}
A.e($t, Ee);
z = $t.prototype;
z.Aa = 2;
z.Tc = q;
function au(a, b) {
  var c = a.n(), d = a.ga(), f = c.width + d, d = c.height + d;
  switch(a.Aa) {
    case 0:
      a.D.Xd(b.x);
      b.x += f;
      b.width -= f;
      break;
    case 1:
      a.D.Xd(b.Ja() - c.width);
      b.width -= f;
      break;
    case 2:
      a.D.gd(b.y);
      b.y += d;
      b.height -= d;
      break;
    case 3:
      a.D.gd(b.ra() - c.height), b.height -= d
  }
}
z.qb = function(a, b) {
  var c = this.Tc ? b : a;
  switch(this.Aa) {
    case 2:
    ;
    case 3:
      var d = 0;
      switch(this.za) {
        case 0:
          d = c.x;
          break;
        case 1:
          d = c.x + (c.width - this.n().width) / 2;
          break;
        case 2:
          d = c.Ja() - this.n().width
      }
      d < c.x && (d = c.x);
      this.D.Xd(d);
      break;
    case 0:
    ;
    case 1:
      d = 0;
      switch(this.za) {
        case 0:
          d = c.y;
          break;
        case 1:
          d = c.y + (c.height - this.n().height) / 2;
          break;
        case 2:
          d = c.ra() - this.n().height
      }
      d < c.y && (d = c.y);
      this.D.gd(d)
  }
};
z.g = function(a) {
  $t.f.g.call(this, a);
  if(C(a, "position")) {
    switch(Cb(F(a, "position"))) {
      case "left":
        this.Aa = 0;
        Be(this, this.Bb.bc + 90);
        break;
      case "right":
        this.Aa = 1;
        Be(this, this.Bb.bc - 90);
        break;
      case "top":
        this.Aa = 2;
        break;
      case "bottom":
        this.Aa = 3
    }
  }
  C(a, "align_by") && (this.Tc = "dataplot" == N(a, "align_by"))
};
z.jc = function(a, b) {
  this.uc(a, this.yc.ta(b));
  return $t.f.jc.call(this, a)
};
z.Na = function(a) {
  return this.aa ? this.aa.Na(a) : ""
};
z.Pa = function(a) {
  return this.aa ? this.aa.Pa(a) : 1
};
function bu(a, b) {
  this.N = a;
  this.rm = b;
  this.Wo = q
}
z = bu.prototype;
z.N = p;
z.rm = p;
z.F = p;
z.tl = t("F");
z.Ya = p;
z.Nn = p;
z.Pm = p;
z.Y = p;
z.Cc = p;
z.aa = p;
z.Ul = t("aa");
z.ca = u("aa");
z.D = p;
z.k = p;
z.n = u("k");
z.Oa = p;
z.Yr = t("Oa");
z.Sf = p;
z.Ki = p;
z.$w = function() {
  this.Ki && this.Ki.clear()
};
z.bJ = x(q);
z.rl = q;
z.Wo = p;
z.fE = u("Wo");
function cu(a, b, c) {
  return Tb(F(b, c)) ? (a = new $t(a.N, a.aa), a.g(F(b, c)), a) : p
}
z.g = function(a) {
  this.rl = q;
  if(a) {
    if(!this.aa || this.aa.Xw(a)) {
      this.rm.cs(), du(this, a)
    }else {
      if(this.rl = j, this.Ki = new Ue(F(a, "styles")), this.Sf = new Zt(this.N, this.aa, this), this.aa && (this.aa.tl(this.F), this.aa.zK(this.Sf), this.aa.g(a, this.Ki), this.aa.fG() && this.aa.uF(a)), C(a, "chart_settings")) {
        var b = F(a, "chart_settings");
        du(this, a);
        this.Ya = cu(this, b, "title");
        this.Nn = cu(this, b, "subtitle");
        this.Pm = cu(this, b, "footer");
        C(b, "controls") && this.Sf.g(F(b, "controls"));
        Ub(b, "legend") && this.Sf.Ft(F(b, "legend"), this.Ki, "legend");
        if(C(b, "controls")) {
          var a = F(b, "controls"), c;
          for(c in a) {
            if(b = a[c], b instanceof Array) {
              for(var d = b.length, f = 0;f < d;f++) {
                this.Ft(b[f], c)
              }
            }else {
              b instanceof Object && this.Ft(b, c)
            }
          }
        }
      }
    }
  }else {
    this.rm.cs()
  }
};
z.Ft = function(a, b) {
  Tb(a) && this.Sf.Ft(a, this.Ki, b)
};
function du(a, b) {
  if(C(b, "chart_settings")) {
    var c = F(b, "chart_settings");
    Tb(F(c, "chart_background")) && (a.Y = new Xd, a.Y.g(F(c, "chart_background")))
  }
}
z.ce = function(a, b) {
  this.Wo = j;
  this.k = b;
  this.D = a;
  this.Oa && Cd(this.Oa, b);
  this.Y && (this.Cc = this.Y.p(this.F), this.D.ia(this.Cc), this.fo = b.Ca(), Yd(this.Y, b));
  if(!this.rl) {
    return this.D
  }
  var c = this.Ya;
  c && this.D.ia(c.jc(this.F, this.aa));
  (c = this.Nn) && this.D.ia(c.jc(this.F, this.aa));
  (c = this.Pm) && this.D.ia(c.jc(this.F, this.aa));
  c = eu(this, b);
  this.aa.Rc(c);
  var d = c.Ca();
  this.Sf.p(this.F, this.D);
  this.aa && (this.Sf.Nh(c, d), this.aa.p(c, this.rm.dy(), j), fu(this, c, d), this.D.ia(this.aa.fb()));
  gu(this, b, this.aa.Xt());
  this.D.ia(this.Sf.fb());
  return this.D
};
function fu(a, b, c) {
  for(var d = a.Sf.hi(b, c), f = 0;!d;) {
    f++, 10 == f && e(Error("Finalize layout error")), a.Sf.nx(b), d = a.Sf.hi(b, c)
  }
}
function gu(a, b, c) {
  a.Pm && a.Pm.qb(b, c);
  a.Ya && a.Ya.qb(b, c);
  a.Nn && a.Nn.qb(b, c)
}
z.qa = function() {
  this.Cc && this.Y.qa(this.Cc, this.fo);
  this.aa && this.rl ? (this.aa.qa(), this.Sf.qa(), this.N.WA && (this.VA = new W(this.D.r()), this.kw = new ze, this.D.ia(this.VA), hu(this))) : this.rl || this.rm.cs()
};
function eu(a, b) {
  var c = b.Ca();
  a.Pm && au(a.Pm, c);
  a.Ya && au(a.Ya, c);
  a.Nn && au(a.Nn, c);
  return c
}
z.Wa = function(a) {
  a = a.Ca();
  this.Oa && Cd(this.Oa, a);
  this.Y && this.Y.Wa(this.Cc, a);
  if(this.rl) {
    if(this.aa) {
      this.Y && Yd(this.Y, a);
      var b = eu(this, a), c = b.Ca();
      this.aa.Rc(b);
      this.Sf.Nh(b, c);
      this.aa.om(b);
      fu(this, b, c);
      this.aa.Jq(b);
      this.Sf.Wa();
      gu(this, a, this.aa.Xt())
    }
    this.k = a.Ca();
    this.N.WA && (this.VA.clear(), hu(this))
  }
};
z.CF = function() {
  this.rl = q
};
z.zp = function() {
  this.rl && this.aa && this.aa.zp()
};
z.VA = p;
z.kw = p;
function hu(a) {
  a.kw.g({font:{family:"Verdana", size:a.k.width / 12.4}});
  a.kw.uc(a.D.r(), a.N.WA);
  var b = a.kw.jc(a.D.r(), zb("red", h), j), c = a.kw.n();
  b.qb(a.k.width / 2 - c.width / 2 + a.k.x, a.k.height / 2 - c.height / 2 + a.k.y);
  b.I.setAttribute("opacity", 0.15);
  a.VA.ia(b)
}
z.mb = function(a) {
  if(a == p || a == h) {
    a = {}
  }
  this.Ya && (a.Title = this.Ya.Bh().ta(this.aa));
  this.Nn && (a.SubTitle = this.Nn.Bh().ta(this.aa));
  this.Pm && (a.Footer = this.Pm.Bh().ta(this.aa));
  this.rl && this.aa && this.aa.mb(a);
  return a
};
function iu(a) {
  this.ak = a;
  this.Vb = 0;
  this.Qw = NaN;
  this.pJ = j;
  this.ma = 0;
  this.Rw = NaN;
  this.qJ = j;
  this.ab = 1;
  this.Al = j;
  this.oq = NaN;
  this.ob = 1;
  this.zl = j;
  this.nq = NaN;
  this.Oa = new Bd(5)
}
z = iu.prototype;
z.Oa = p;
z.k = p;
z.D = p;
z.F = p;
z.tl = t("F");
z.Vb = p;
z.Hd = function(a) {
  return isNaN(this.Qw) ? a.x + (this.pJ ? a.width * this.Vb : this.Vb) : this.Qw
};
z.pJ = p;
z.Qw = p;
z.ma = p;
z.Sb = function(a) {
  return isNaN(this.Rw) ? a.y + (this.qJ ? a.height * this.ma : this.ma) : this.Rw
};
z.qJ = p;
z.Rw = p;
z.ab = p;
z.Ka = function(a) {
  return isNaN(this.oq) ? this.Al ? a.width * this.ab : this.ab : this.oq
};
z.Al = p;
z.oq = p;
z.ob = p;
z.vb = function(a) {
  return isNaN(this.nq) ? this.zl ? a.height * this.ob : this.ob : this.nq
};
z.zl = p;
z.nq = p;
z.ak = p;
z.g = function(a) {
  var b;
  C(a, "margin") && (this.Oa = new Bd, this.Oa.g(F(a, "margin")));
  C(a, "x") && (b = F(a, "x"), this.Vb = Mb(b) ? Nb(b) : Ob(b));
  C(a, "y") && (b = F(a, "y"), this.ma = Mb(b) ? Nb(b) : Ob(b));
  C(a, "width") && (b = F(a, "width"), this.ab = Mb(b) ? Nb(b) : Ob(b));
  C(a, "height") && (b = F(a, "height"), this.ob = Mb(b) ? Nb(b) : Ob(b))
};
z.ce = function(a, b) {
  this.D || (this.D = new W(a));
  this.Nh(b);
  return this.D
};
z.qa = s();
z.Wa = function(a) {
  this.Nh(a)
};
z.Nh = function(a) {
  this.qb(a)
};
z.qb = function(a) {
  this.k = a.Ca();
  this.k.x = this.Hd(a);
  this.k.y = this.Sb(a);
  this.k.width = this.Ka(a);
  this.k.height = this.vb(a);
  this.D.qb(this.k.x, this.k.y);
  this.k.x = 0;
  this.k.y = 0;
  this.Oa && Cd(this.Oa, this.k)
};
function ju(a) {
  iu.call(this, a);
  this.dm = new ku(a.dj(), this)
}
A.e(ju, iu);
z = ju.prototype;
z.dm = p;
z.Sa = p;
z.getName = u("Sa");
z.gt = p;
z.g = function(a) {
  ju.f.g.call(this, a);
  this.ak.cm.push(this.dm);
  if(C(a, "name")) {
    var b = this.Sa = J(a, "name"), c = this.dm;
    b && c && (this.ak.jw[b] = c)
  }
  C(a, "source") && (b = J(a, "source"), c = q, C(a, "source_mode") && (a = Wb(a, "source_mode"), c = "external" === a || "externaldata" === a), c ? this.dm.lw = b : this.gt = b)
};
z.ce = function(a, b) {
  ju.f.ce.call(this, a, b);
  this.gt && (this.dm.Hc = this.ak.dj().sq[this.gt], this.dm.ht = this.ak.dj().Ww[this.gt]);
  this.gt = p;
  this.dm.ce(a, this.D, new P(0, 0, this.k.width, this.k.height));
  if(this.Oa) {
    qd(this.D, this.Oa.tb());
    var c = this.D;
    c.ma += this.Oa.rb();
    pd(c)
  }
  c = this.dm;
  c.Hc ? c.Fk(c.Hc) : c.lw ? c.Hi(c.lw) : c.Fu ? c.Fu = c.Fu : lu(c, c.N.uG);
  return this.D
};
z.Wa = function(a) {
  ju.f.Wa.call(this, a);
  this.dm.Wa(new P(0, 0, this.k.width, this.k.height))
};
function mu(a) {
  iu.call(this, a);
  this.yf = []
}
A.e(mu, iu);
z = mu.prototype;
z.yf = p;
z.QB = p;
z.gx = p;
z.g = function(a) {
  mu.f.g.call(this, a);
  for(var b in a) {
    var c = a[b];
    if(!dc(c) && (Boolean(c instanceof Object && !(c instanceof Function || c instanceof Array)) && nu(this, c, b), A.isArray(c))) {
      for(var d = c.length, f = 0;f < d;f++) {
        nu(this, c[f], b)
      }
    }
  }
};
function nu(a, b, c) {
  if(c = "hbox" === c ? new ou(a.ak) : "vbox" === c ? new pu(a.ak) : "view" === c && C(b, "type") ? "panel" == N(b, "type") ? new qu(a.ak) : new ju(a.ak) : p) {
    c.g(b), a.yf.push(c)
  }
}
z.ce = function(a, b) {
  this.QB = new W(a);
  this.gx = new W(a);
  mu.f.ce.call(this, a, b);
  this.D.ia(this.gx);
  this.D.ia(this.QB);
  for(var c = this.yf.length, d = 0;d < c;d++) {
    this.QB.ia(this.yf[d].ce(a, this.k))
  }
  return this.D
};
z.qa = function() {
  for(var a = this.yf.length, b = 0;b < a;b++) {
    this.yf[b].qa()
  }
};
z.Wa = function(a) {
  mu.f.Wa.call(this, a);
  for(var a = this.yf.length, b = 0;b < a;b++) {
    this.yf[b].Wa(this.k)
  }
};
function ru(a) {
  Ee.call(this, a, this)
}
A.e(ru, Ee);
z = ru.prototype;
z.yd = p;
z.nd = u("yd");
z.uc = function(a) {
  ru.f.uc.call(this, a, this.yc.ta());
  this.yd = this.Ia + this.k.height
};
z.jc = function(a, b) {
  var c = ru.f.jc.call(this, a);
  this.qb(c, b);
  return c
};
z.qb = function(a, b) {
  var c;
  switch(this.za) {
    case 0:
      c = b.x;
      break;
    case 2:
      c = b.x + b.width - this.k.width;
      break;
    case 1:
      c = b.x + (b.width - this.k.width) / 2
  }
  a.qb(c, b.y)
};
z.Na = x("");
z.Pa = x(1);
function qu(a) {
  mu.call(this, a)
}
A.e(qu, mu);
z = qu.prototype;
z.Ya = p;
z.mh = p;
z.Y = p;
z.Cc = p;
z.uz = p;
z.lH = p;
z.g = function(a) {
  a = su(Zb(sf.defaults.panel), a);
  tu(this, a)
};
function tu(a, b) {
  qu.f.g.call(a, b);
  if(Ub(b, "title")) {
    var c = F(b, "title");
    C(c, "text") && (a.Ya = new ru(a.ak.dj()), a.Ya.g(c))
  }
  C(b, "background") && (a.Y = new Xd, a.Y.g(F(b, "background")))
}
function su(a, b) {
  var c = Zb(b);
  ac(a, b, c);
  for(var d in a) {
    var f = a[d];
    dc(f) || (c[d] = c[d] ? $b(f, c[d], d) : f)
  }
  return c
}
z.ce = function(a, b) {
  this.Ya && this.Ya.uc(a);
  qu.f.ce.call(this, a, b);
  this.Y && this.Y.isEnabled() && (this.Cc = this.Y.p(a), this.gx.ia(this.Cc));
  this.Ya && (this.mh = this.Ya.jc(a, this.lH), this.gx.ia(this.mh));
  return this.D
};
z.qa = function() {
  qu.f.qa.call(this);
  this.Cc && this.Y.qa(this.Cc, this.uz)
};
z.Wa = function(a) {
  qu.f.Wa.call(this, a);
  this.mh && this.Ya.qb(this.mh, this.uz);
  this.Cc && this.Y.Wa(this.Cc, this.uz)
};
z.Nh = function(a) {
  qu.f.Nh.call(this, a);
  this.uz = this.k.Ca();
  this.Y && this.Y.isEnabled() && Yd(this.Y, this.k);
  this.lH = this.k.Ca();
  this.Ya && (this.k.y += this.Ya.nd(), this.k.height -= this.Ya.nd())
};
function ou(a) {
  mu.call(this, a)
}
A.e(ou, mu);
ou.prototype.ce = function(a, b) {
  ou.f.ce.call(this, a, b);
  return this.D
};
ou.prototype.Nh = function(a) {
  ou.f.Nh.call(this, a);
  for(var b = this.k.x, c = this.yf.length, d = 0;d < c;d++) {
    var f = this.yf[d];
    f.Qw = b;
    f.oq = NaN;
    f.oq = f.Ka(a);
    b += f.oq
  }
};
function pu(a) {
  mu.call(this, a)
}
A.e(pu, mu);
pu.prototype.ce = function(a, b) {
  pu.f.ce.call(this, a, b);
  return this.D
};
pu.prototype.Nh = function(a) {
  pu.f.Nh.call(this, a);
  for(var b = this.k.y, c = this.yf.length, d = 0;d < c;d++) {
    var f = this.yf[d];
    f.Rw = b;
    f.nq = NaN;
    f.nq = f.vb(a);
    b += f.nq
  }
};
function uu(a) {
  mu.call(this, this);
  this.N = a;
  this.jw = {};
  this.cm = []
}
A.e(uu, qu);
z = uu.prototype;
z.N = p;
z.dj = u("N");
z.jw = p;
z.cm = p;
z.Wo = q;
z.fE = x(j);
z.bJ = x(j);
z.hu = p;
z.Yr = t("hu");
z.fb = u("D");
z.n = u("k");
z.g = function(a) {
  var b = Zb(F(F(sf, "defaults"), "dashboard")), c = Zb(F(F(sf, "defaults"), "panel")), a = su($b(c, b), a);
  tu(this, a)
};
z.ce = function(a, b) {
  this.D = a;
  var c = b.Ca();
  this.hu && Cd(this.hu, c);
  uu.f.ce.call(this, this.F, c);
  this.Wo = j;
  return this.D
};
z.zp = function() {
  vu(this)
};
function vu(a) {
  if(a.Wo) {
    var b, c = a.cm.length;
    for(b = 0;b < c;b++) {
      var d = a.cm[b];
      if(d.hb && !d.hb.fE()) {
        return
      }
    }
    for(b = 0;b < c;b++) {
      a.cm[b].hb && a.cm[b].hb.fE() && a.cm[b].hb.zp()
    }
  }
}
z.Wa = function(a) {
  a = a.Ca();
  this.hu && Cd(this.hu, a);
  uu.f.Wa.call(this, a)
};
function wu(a, b) {
  var c;
  a instanceof wu ? (this.Wr(b == p ? a.ok : b), xu(this, a.mD()), yu(this, a.JI()), zu(this, a.rI()), Au(this, a.iD()), Bu(this, a.eu()), Cu(this, a.AI().Ca()), Du(this, a.tI())) : a && (c = A.uri.R.split("" + a)) ? (this.Wr(!!b), xu(this, c[1] || "", j), yu(this, c[2] || "", j), zu(this, c[3] || "", j), Au(this, c[4]), Bu(this, c[5] || "", j), Cu(this, c[6] || "", j), Du(this, c[7] || "", j)) : (this.Wr(!!b), this.Ak = new Eu(p, this, this.ok))
}
z = wu.prototype;
z.En = "";
z.dw = "";
z.xo = "";
z.rv = p;
z.pj = "";
z.Vt = "";
z.jN = q;
z.ok = q;
z.toString = function() {
  if(this.Ui) {
    return this.Ui
  }
  var a = [];
  this.En && a.push(Fu(this.En, Gu), ":");
  this.xo && (a.push("//"), this.dw && a.push(Fu(this.dw, Gu), "@"), a.push(A.Qc(this.xo) ? encodeURIComponent(this.xo) : p), this.rv != p && a.push(":", "" + this.iD()));
  this.pj && (this.xo && "/" != this.pj.charAt(0) && a.push("/"), a.push(Fu(this.pj, "/" == this.pj.charAt(0) ? Hu : Iu)));
  var b = "" + this.Ak;
  b && a.push("?", b);
  this.Vt && a.push("#", Fu(this.Vt, Ju));
  return this.Ui = a.join("")
};
z.Ca = function() {
  var a = this.En, b = this.dw, c = this.xo, d = this.rv, f = this.pj, g = this.Ak.Ca(), k = this.Vt, l = new wu(p, this.ok);
  a && xu(l, a);
  b && yu(l, b);
  c && zu(l, c);
  d && Au(l, d);
  f && Bu(l, f);
  g && Cu(l, g);
  k && Du(l, k);
  return l
};
z.mD = u("En");
function xu(a, b, c) {
  Ku(a);
  delete a.Ui;
  a.En = c ? b ? decodeURIComponent(b) : "" : b;
  a.En && (a.En = a.En.replace(/:$/, ""))
}
z.JI = u("dw");
function yu(a, b, c) {
  Ku(a);
  delete a.Ui;
  a.dw = c ? b ? decodeURIComponent(b) : "" : b
}
z.rI = u("xo");
function zu(a, b, c) {
  Ku(a);
  delete a.Ui;
  a.xo = c ? b ? decodeURIComponent(b) : "" : b
}
z.iD = u("rv");
function Au(a, b) {
  Ku(a);
  delete a.Ui;
  b ? (b = Number(b), (isNaN(b) || 0 > b) && e(Error("Bad port number " + b)), a.rv = b) : a.rv = p
}
z.eu = u("pj");
function Bu(a, b, c) {
  Ku(a);
  delete a.Ui;
  a.pj = c ? b ? decodeURIComponent(b) : "" : b
}
function Cu(a, b, c) {
  Ku(a);
  delete a.Ui;
  b instanceof Eu ? (a.Ak = b, a.Ak.ts = a, a.Ak.Wr(a.ok)) : (c || (b = Fu(b, Lu)), a.Ak = new Eu(b, a, a.ok))
}
z.AI = u("Ak");
z.tI = u("Vt");
function Du(a, b, c) {
  Ku(a);
  delete a.Ui;
  a.Vt = c ? b ? decodeURIComponent(b) : "" : b
}
z.rN = function() {
  Ku(this);
  var a = A.U.BI();
  Ku(this);
  delete this.Ui;
  this.Ak.set("zx", a);
  return this
};
function Ku(a) {
  a.jN && e(Error("Tried to modify a read-only Uri"))
}
z.Wr = function(a) {
  this.ok = a;
  this.Ak && this.Ak.Wr(a);
  return this
};
var Mu = /^[a-zA-Z0-9\-_.!~*'():\/;?]*$/;
function Fu(a, b) {
  var c = p;
  A.Qc(a) && (c = a, Mu.test(c) || (c = encodeURI(a)), 0 <= c.search(b) && (c = c.replace(b, Nu)));
  return c
}
function Nu(a) {
  a = a.charCodeAt(0);
  return"%" + (a >> 4 & 15).toString(16) + (a & 15).toString(16)
}
var Gu = /[#\/\?@]/g, Iu = /[\#\?:]/g, Hu = /[\#\?]/g, Lu = /[\#\?@]/g, Ju = /#/g;
function Eu(a, b, c) {
  this.il = a || p;
  this.ts = b || p;
  this.ok = !!c
}
function Ou(a) {
  if(!a.wd && (a.wd = new Ca, a.zc = 0, a.il)) {
    for(var b = a.il.split("&"), c = 0;c < b.length;c++) {
      var d = b[c].indexOf("="), f = p, g = p;
      0 <= d ? (f = b[c].substring(0, d), g = b[c].substring(d + 1)) : f = b[c];
      f = A.U.OA(f);
      f = Pu(a, f);
      a.add(f, g ? A.U.OA(g) : "")
    }
  }
}
z = Eu.prototype;
z.wd = p;
z.zc = p;
z.Tm = function() {
  Ou(this);
  return this.zc
};
z.add = function(a, b) {
  Ou(this);
  Qu(this);
  a = Pu(this, a);
  if(this.Xj(a)) {
    var c = this.wd.get(a);
    A.isArray(c) ? c.push(b) : this.wd.set(a, [c, b])
  }else {
    this.wd.set(a, b)
  }
  this.zc++;
  return this
};
z.remove = function(a) {
  Ou(this);
  a = Pu(this, a);
  if(this.wd.Xj(a)) {
    Qu(this);
    var b = this.wd.get(a);
    A.isArray(b) ? this.zc -= b.length : this.zc--;
    return this.wd.remove(a)
  }
  return q
};
z.clear = function() {
  Qu(this);
  this.wd && this.wd.clear();
  this.zc = 0
};
z.ri = function() {
  Ou(this);
  return 0 == this.zc
};
z.Xj = function(a) {
  Ou(this);
  a = Pu(this, a);
  return this.wd.Xj(a)
};
z.yq = function(a) {
  var b = this.Ff();
  return A.$.contains(b, a)
};
z.lg = function() {
  Ou(this);
  for(var a = this.wd.Ff(), b = this.wd.lg(), c = [], d = 0;d < b.length;d++) {
    var f = a[d];
    if(A.isArray(f)) {
      for(var g = 0;g < f.length;g++) {
        c.push(b[d])
      }
    }else {
      c.push(b[d])
    }
  }
  return c
};
z.Ff = function(a) {
  Ou(this);
  if(a) {
    if(a = Pu(this, a), this.Xj(a)) {
      var b = this.wd.get(a);
      if(A.isArray(b)) {
        return b
      }
      a = [];
      a.push(b)
    }else {
      a = []
    }
  }else {
    for(var b = this.wd.Ff(), a = [], c = 0;c < b.length;c++) {
      var d = b[c];
      A.isArray(d) ? A.$.extend(a, d) : a.push(d)
    }
  }
  return a
};
z.set = function(a, b) {
  Ou(this);
  Qu(this);
  a = Pu(this, a);
  if(this.Xj(a)) {
    var c = this.wd.get(a);
    A.isArray(c) ? this.zc -= c.length : this.zc--
  }
  this.wd.set(a, b);
  this.zc++;
  return this
};
z.get = function(a, b) {
  Ou(this);
  a = Pu(this, a);
  if(this.Xj(a)) {
    var c = this.wd.get(a);
    return A.isArray(c) ? c[0] : c
  }
  return b
};
z.toString = function() {
  if(this.il) {
    return this.il
  }
  if(!this.wd) {
    return""
  }
  for(var a = [], b = 0, c = this.wd.lg(), d = 0;d < c.length;d++) {
    var f = c[d], g = A.U.us(f), f = this.wd.get(f);
    if(A.isArray(f)) {
      for(var k = 0;k < f.length;k++) {
        0 < b && a.push("&"), a.push(g), "" !== f[k] && a.push("=", A.U.us(f[k])), b++
      }
    }else {
      0 < b && a.push("&"), a.push(g), "" !== f && a.push("=", A.U.us(f)), b++
    }
  }
  return this.il = a.join("")
};
function Qu(a) {
  delete a.cC;
  delete a.il;
  a.ts && delete a.ts.Ui
}
z.Ca = function() {
  var a = new Eu;
  this.cC && (a.cC = this.cC);
  this.il && (a.il = this.il);
  this.wd && (a.wd = this.wd.Ca());
  return a
};
function Pu(a, b) {
  var c = "" + b;
  a.ok && (c = c.toLowerCase());
  return c
}
z.Wr = function(a) {
  a && !this.ok && (Ou(this), Qu(this), A.zd.forEach(this.wd, function(a, c) {
    var d = c.toLowerCase();
    c != d && (this.remove(c), this.add(d, a))
  }, this));
  this.ok = a
};
z.extend = function(a) {
  for(var b = 0;b < arguments.length;b++) {
    A.zd.forEach(arguments[b], function(a, b) {
      this.add(b, a)
    }, this)
  }
};
function Ru() {
  this.Tg = [];
  this.Kr = {}
}
z = Ru.prototype;
z.Tg = p;
z.Kr = p;
z.zk = p;
z.sp = p;
function Su(a, b) {
  if(Uu[b] != h ? 0 : a.Kr[b] == h) {
    var c = new Vu;
    c.p(b, q);
    Wu(a, c)
  }
}
function Xu(a, b) {
  if(Uu[b] != h ? 0 : a.Kr[b] == h) {
    var c = new Yu;
    c.p(b, j);
    Wu(a, c)
  }
}
function Wu(a, b) {
  a.Tg.push(b);
  a.Kr[b.eu()] = b;
  b.zk = function() {
    a.xN.call(a, this)
  };
  b.sp = function(b) {
    a.wN.call(a, this, b)
  }
}
z.xN = function(a) {
  delete this.Kr[a.eu()];
  a = this.Tg.indexOf(a);
  -1 != a && this.Tg.splice(a, 1);
  this.zk && 0 == this.Tg.length && this.zk()
};
z.wN = function(a, b) {
  this.Ov();
  this.sp && this.sp(a, b)
};
z.load = function() {
  for(var a = 0;a < this.Tg.length;a++) {
    var b = this.Tg[a];
    b.Cl.send(b.ts, "GET", p, "anychart.com")
  }
};
z.Ov = function() {
  for(var a = 0;a < this.Tg.length;a++) {
    this.Tg[a].Ov()
  }
  this.Tg = [];
  this.Kr = {}
};
var Uu = {};
function Zu() {
  this.EB = j;
  this.Cl = new Ga;
  this.Cl.vG = j;
  A.G.nb(this.Cl, "complete", this.zN, p, this);
  this.sp && A.G.nb(this.Cl, ["error", "abort"], this.sp, p, this)
}
z = Zu.prototype;
z.Cl = p;
z.EB = j;
z.pj = p;
z.ts = p;
z.eu = u("pj");
z.p = function(a, b) {
  this.pj = a;
  this.EB = b;
  this.EB || (a = -1 != a.indexOf("?") ? a + "&" : a + "?", a += "XMLCallDate=" + (new Date).getTime().toString());
  this.ts = new wu(a)
};
z.Ov = function() {
  this.Cl.abort()
};
z.getData = function() {
  A.xa()
};
z.zN = function() {
  var a = this.getData();
  Uu[this.pj] = a;
  this.zk && this.zk.call(this, a)
};
z.zk = p;
z.sp = p;
function Vu() {
  Zu.apply(this)
}
A.e(Vu, Zu);
Vu.prototype.getData = function() {
  return this.Cl.Bc ? this.Cl.Bc.responseText : ""
};
function Yu() {
  Zu.apply(this)
}
A.e(Yu, Zu);
Yu.prototype.getData = function() {
  var a;
  var b = this.Cl;
  try {
    a = b.Bc ? b.Bc.responseXML : p
  }catch(c) {
    a = p
  }
  return a
};
function $u(a) {
  pa.call(this);
  this.N = a;
  this.IF()
}
A.e($u, pa);
$u.prototype.N = p;
$u.prototype.dj = u("N");
$u.prototype.TA = p;
function av(a) {
  $u.call(this, a);
  bv(this)
}
A.e(av, $u);
z = av.prototype;
z.IF = function() {
  this.TA = 2
};
z.F = p;
z.D = p;
z.fb = u("D");
z.ft = p;
z.k = p;
z.n = u("k");
z.Mk = p;
z.dy = u("Mk");
z.ce = function(a, b, c) {
  this.F = a;
  this.k = c.Ca();
  this.D = new W(a);
  this.ft = new W(a);
  this.Mk = new W(a);
  this.D.ia(this.ft);
  this.D.ia(this.Mk);
  this.jp || (this.jp = new W(a), this.kp = new ze, this.D.ia(this.jp));
  b.ia ? b.ia(this.D) : b.appendChild(this.D.I);
  lu(this, this.N.uG)
};
z.jf = p;
function bv(a) {
  a.jf = new Ru;
  a.jf.sp = function(b, c) {
    a.YN.call(a, c.message)
  }
}
z.Vo = q;
z.refresh = function() {
  this.fd || (this.Vo = j, this.hb.ca().refresh())
};
z.$w = function() {
  !this.fd && this.hb && this.hb.$w()
};
z.Pd = function(a, b) {
  this.fd || this.hb.ca().Pd(a, b)
};
z.Kj = function(a, b, c) {
  this.fd || this.hb.ca().Kj(a, b, c)
};
z.Bk = function(a, b) {
  this.fd || this.hb.ca().Bk(a, b)
};
z.Nk = function(a, b, c) {
  this.fd || this.hb.ca().Nk(a, b, c)
};
z.Ok = function(a, b, c) {
  this.fd || this.hb.ca().Ok(a, b, c)
};
z.mk = function(a, b, c) {
  this.fd || this.hb.ca().mk(a, b, c)
};
z.tj = function(a, b, c) {
  this.fd || this.hb.ca().xK(a, b, c)
};
z.Lj = function(a) {
  this.fd || this.hb.ca().Lj(a)
};
z.Mj = function(a, b) {
  this.fd || this.hb.ca().Mj(a, b)
};
z.Ck = function(a) {
  this.fd || this.hb.ca().Ck(a)
};
z.Pk = function(a, b) {
  this.fd || this.hb.ca().Pk(a, b)
};
z.Ik = function(a, b) {
  this.fd || this.hb.ca().Ik(a, b)
};
z.nk = function(a, b) {
  this.fd || this.hb.ca().nk(a, b)
};
z.lk = function(a, b) {
  this.fd || this.hb.ca().lk(a, b)
};
z.Hk = function(a, b) {
  this.fd || this.hb.ca().Hk(a, b)
};
z.sj = p;
z.$u = j;
z.lw = p;
z.Hi = function(a, b, c) {
  this.sj = b;
  this.$u = c != p && c != h ? c : j;
  this.lw = a;
  this.jf.Ov();
  Su(this.jf, a);
  if(0 == this.jf.Tg.length) {
    this.Oh(Uu[a])
  }else {
    var d = this;
    cv(this);
    this.jf.zk = function() {
      d.DN.call(d)
    };
    this.jf.load()
  }
};
z.DN = function() {
  this.Oh(Uu[this.lw])
};
z.Fu = p;
z.Kn = function(a) {
  this.Fu = a;
  this.jf.Ov();
  Su(this.jf, a);
  if(0 == this.jf.Tg.length) {
    this.wj(Uu[a])
  }else {
    var b = this;
    cv(this);
    this.jf.zk = function() {
      b.yN.call(b)
    };
    this.jf.load()
  }
};
z.yN = function() {
  this.wj(Uu[this.Fu])
};
z.Hc = p;
z.Oh = function(a) {
  this.Fk(Jb(a))
};
z.wj = function(a) {
  this.Fk(eval("(" + a + ")"))
};
z.Fk = function(a) {
  this.sj && (a = lc(a, this.sj));
  this.Hc = a;
  lu(this, this.N.TI);
  try {
    this.sv()
  }catch(b) {
    dv(this.N, b), lu(this, b.toString())
  }
};
z.ht = p;
z.sv = function() {
  if(!this.Hc) {
    return this.cs(), q
  }
  if(this.ht) {
    var a = 1 == this.ht ? "chart" : "gauge";
    if(!C(this.Hc, a + "s")) {
      var b = this.Hc, c = {}, d = {};
      H(c, a + "s", d);
      H(d, a, b);
      this.Hc = c
    }
  }
  return j
};
function ev(a) {
  a.oH(a.Hc);
  0 == a.jf.Tg.length ? a.bK() : (lu(a, a.N.EJ), a.jf.zk = function() {
    a.bK.call(a)
  }, a.jf.load())
}
z.bK = function() {
  fv(this);
  var a = this.k.Ca();
  this.D.qb(a.x, a.y);
  this.hb.ce(this.ft, a);
  this.hb.bJ() ? this.hb.zp() : 1 == this.TA && vu(this.N.hb);
  this.MH();
  this.hb.qa();
  this.LH()
};
z.Rp = p;
z.jp = p;
z.kp = p;
z.px = p;
function lu(a, b, c, d) {
  c !== j && Ae(a.kp);
  fv(a);
  a.px = b;
  a.kp.g({format:b, rotation:d});
  a.kp.uc(a.F, b);
  a.jp.ia(a.kp.jc(a.F));
  b = a.kp.n();
  a.jp.qb(a.k.x + (a.k.width - b.width) / 2, a.k.y + (a.k.height - b.height) / 2)
}
function fv(a) {
  a.jp && a.jp.clear();
  a.px = p
}
z.cs = function() {
  var a = this.N.ME, b = a && C(a, "text") ? J(a, "text") : this.N.WJ;
  this.kp.g(this.N.ME);
  lu(this, b, j, F(a, "rotation"))
};
function cv(a, b) {
  b || (b = a.N.DJ);
  lu(a, b)
}
z.YN = function(a) {
  lu(this, "Error: " + a)
};
z.hb = p;
z.clear = function() {
  this.hb = p;
  this.D != p && this.D.clear()
};
z.Wa = function(a) {
  this.k = a;
  this.px && lu(this, this.px, j);
  this.hb && this.hb.Wa(a)
};
function ku(a, b) {
  this.aC = b;
  this.Rp = a.Rp;
  av.call(this, a)
}
A.e(ku, av);
z = ku.prototype;
z.IF = function() {
  this.TA = 1
};
z.aC = p;
z.EK = t("D");
z.fb = u("D");
z.ce = function(a, b, c) {
  ku.f.ce.call(this, a, b, c)
};
z.sv = function() {
  ku.f.sv.call(this) && ev(this)
};
z.oH = function() {
  this.fd = q;
  this.hb = new bu(this.N, this);
  this.hb.tl(this.F);
  mf(this, this.hb, this.Hc)
};
z.cs = function() {
  this.hb.CF();
  ku.f.cs.call(this)
};
z.MH = function() {
  this.Vo || dv(this.N, new He("dashboardViewRefresh", this.aC.getName()))
};
z.LH = function() {
  this.Ex()
};
z.Ex = function() {
  var a = this.N, b = this.aC.getName();
  this.Vo ? dv(a, new He("dashboardViewRefresh", b)) : dv(a, new He("dashboardViewDraw", b));
  this.Vo = q;
  a.vC++
};
function gv() {
  av.call(this, this);
  this.Rp = new uf;
  this.Oa = new Bd(20);
  this.sq = {};
  this.Ww = {};
  this.$u = q
}
A.e(gv, av);
z = gv.prototype;
z.IF = function() {
  this.TA = 0
};
z.ce = function(a, b, c) {
  gv.f.ce.call(this, a, b, c)
};
z.EA = p;
function hv(a) {
  a.EA = p;
  if(C(a.Hc, "templates")) {
    var b = F(a.Hc, "templates");
    C(b, "path") && (a.EA = J(b, "path"), Xu(a.jf, a.EA));
    vf(a.Rp, I(b, "template"))
  }
  0 == a.jf.Tg.length ? ev(a) : (lu(a, a.N.FJ), a.jf.zk = function() {
    a.CN.call(a)
  }, a.jf.load())
}
z.CN = function() {
  var a = Kb(Uu[this.EA]);
  vf(this.Rp, I(a, "template"));
  ev(this)
};
z.Fk = function(a) {
  C(a, "anychart") && (a = F(a, "anychart"));
  if(this.$u) {
    var b = this.F;
    kd(b.gl);
    b.GD.clear();
    b.cr.clear();
    b.ED.clear();
    b.gk.clear();
    this.sq = {};
    this.Ww = {};
    this.Rp.clear();
    this.ft.clear();
    this.Mk.clear();
    fv(this);
    if(this.fd) {
      for(var c in this.jw) {
        this.jw[c].$w()
      }
    }else {
      this.$w()
    }
  }
  gv.f.Fk.call(this, a);
  this.$u = j
};
z.Oa = p;
z.iq = p;
z.ME = p;
z.sv = function() {
  if(gv.f.sv.call(this)) {
    if(this.iq = C(this.Hc, "margin") && !this.iq ? F(this.Hc, "margin") : $b(this.iq, F(this.Hc, "margin"), "margin")) {
      H(this.Hc, "margin", this.iq), this.Oa.g(this.iq)
    }
    this.rx = new nh;
    this.YJ = new ph;
    if(C(this.Hc, "settings")) {
      var a = F(this.Hc, "settings");
      if(C(a, "locale")) {
        var b = F(a, "locale");
        C(b, "date_time_format") && this.rx.g(F(b, "date_time_format"));
        C(b, "number_format") && this.YJ.g(F(b, "number_format"))
      }
      C(a, "no_data") && (a = F(a, "no_data"), C(a, "label") && (this.ME = F(a, "label")))
    }
    this.rx.p();
    C(this.Hc, "gauges") && (iv(this, I(F(this.Hc, "gauges"), "gauge"), 2), this.ht = 2);
    C(this.Hc, "charts") && (iv(this, I(F(this.Hc, "charts"), "chart"), 1), this.ht = 1);
    hv(this)
  }
};
z.fd = p;
z.oH = function() {
  C(this.Hc, "dashboard") && C(F(this.Hc, "dashboard"), "view") ? (this.fd = j, this.vC = 0, this.hb = new uu(this), this.hb.Yr(this.Oa), this.hb.tl(this.F), this.hb.g(F(F(this.Hc, "dashboard"), "view"))) : (this.fd = q, this.hb = new bu(this, this), this.hb.Yr(this.Oa), this.hb.tl(this.F), mf(this, this.hb, this.Hc))
};
z.sq = p;
z.Ww = p;
function iv(a, b, c) {
  for(var d = b.length, f = 0;f < d;f++) {
    var g = b[f];
    if(g && C(g, "name")) {
      var k = a, l = F(g, "name"), n = c;
      k.sq[l] = g;
      k.Ww[l] = n
    }
  }
}
z.MH = function() {
  this.Vo || dv(this, new Ge("anychartRender"))
};
z.LH = function() {
  this.hb && (this.fd ? (0 == this.hb.cm.length || this.hb.cm.length <= this.vC) && this.Ex() : this.Ex())
};
z.Ex = function() {
  this.Vo ? dv(this, new Ge("anychartRefresh")) : dv(this, new Ge("anychartDraw"));
  this.Vo = q
};
z.vC = p;
function dv(a, b) {
  A.G.dispatchEvent(a, b)
}
z.rx = p;
z.Gd = u("rx");
z.YJ = p;
z.mb = function() {
  return this.hb.mb()
};
z.Np = function(a, b) {
  if(this.fd) {
    var c = Kn(this, a);
    c && c.Hi(b)
  }
};
function Kn(a, b) {
  if(!a.fd) {
    return p
  }
  var c = a.hb.jw[b];
  return!c || !c.hb || !c.hb.ca() ? p : c
}
z.Is = function(a, b, c) {
  (a = Kn(this, a)) && a.Hk(b, c)
};
z.zs = function(a, b) {
  var c = Kn(this, a);
  c && c.Lj(b)
};
z.Gs = function(a, b) {
  var c = Kn(this, a);
  c && c.Ck(b)
};
z.As = function(a, b, c) {
  (a = Kn(this, a)) && a.Mj(b, c)
};
z.Ls = function(a, b, c) {
  (a = Kn(this, a)) && a.Pk(b, c)
};
z.Js = function(a, b, c) {
  (a = Kn(this, a)) && a.Ik(b, c)
};
z.xs = function(a, b, c) {
  (a = Kn(this, a)) && a.Pd(b, c)
};
z.ys = function(a, b, c, d) {
  (a = Kn(this, a)) && a.Kj(b, c, d)
};
z.Fs = function(a, b, c) {
  (a = Kn(this, a)) && a.Bk(b, c)
};
z.Ks = function(a, b, c, d) {
  (a = Kn(this, a)) && a.Nk(b, c, d)
};
z.Es = function(a) {
  (a = Kn(this, a)) && a.refresh()
};
z.Ds = function(a, b, c) {
  (a = Kn(this, a)) && a.nk(b, c)
};
z.Cs = function(a, b, c, d) {
  var f = Kn(this, a);
  f && f.mk(a, b, c, d)
};
z.Bs = function(a, b, c) {
  (a = Kn(this, a)) && a.lk(b, c)
};
z.Hs = function(a, b, c, d) {
  (a = Kn(this, a)) && a.tj(b, c, d)
};
z.rs = function(a, b, c, d) {
  (a = Kn(this, a)) && a.Ok(b, c, d)
};
z.qs = function(a, b) {
  if(b instanceof Document) {
    b = Kb(b)
  }else {
    if("string" == typeof b && "<" == b.charAt(0)) {
      b = Jb(b)
    }else {
      if(!(b instanceof Object)) {
        return
      }
    }
  }
  for(var c = b, d = 0, f = this.Hc, g = a.length, k;d < g;) {
    k = a.charAt(d);
    var l, n;
    switch(k) {
      case "/":
        d++;
        break;
      case "[":
        k = a.substring(d, a.indexOf("]", d));
        d += k.length + 1;
        if(!A.isArray(f)) {
          continue
        }
        k = k.substring(1);
        f = f[k];
        break;
      case "(":
        k = a.substring(d, a.indexOf(")", d));
        d += k.length + 1;
        k = k.substring(1);
        l = k.split("=");
        k = l[0];
        var m = l[1];
        l = p;
        for(n in f) {
          f[n][k] != h && f[n][k] == m && (l == p ? l = f[n] : (A.isArray(l) || (l = [l]), l.push(f[n])))
        }
        f = l;
        break;
      default:
        if("." == k && d < a.length - 1 && "." == a.charAt(d + 1)) {
          l = jv(a, d);
          d = l[1];
          f = l = f.qQ(l[0].substr(2));
          break
        }
        l = jv(a, d);
        d = l[1];
        k = l[0];
        f = A.isArray(f) ? f[0][k] : f[k]
    }
  }
  f != p && (c = Vk(c), ac(f, c, f), c.text && (f.text = c.text), c.format && (f.text = c.format));
  this.Fk(this.Hc)
};
function jv(a, b) {
  for(var c = a.charAt(b), d = "", f = j, g = a.length;b < g && f;) {
    d += c, b++, c = a.charAt(b), f = a.charAt(b), f = "[" == f || "]" == f || "(" == f || ")" == f || "/" == f ? q : !("." == f && b < a.length - 1 && "." == a.charAt(b + 1))
  }
  return[d, b]
}
z.WA = p;
z.uG = p;
z.TI = p;
z.YH = q;
z.DJ = p;
z.EJ = p;
z.FJ = p;
z.WJ = p;
z.CF = t("WJ");
function kv() {
}
A.Gg("anychart.render.svg.SVGRenderer", kv);
z = kv.prototype;
z.F = p;
z.N = p;
z.oa = p;
z.at = x(j);
z.Ha = function(a) {
  if(A.userAgent.Ij) {
    var b = document.getElementsByTagName("head")[0], c = document.createElement("meta");
    c.name = "anychart.com";
    c.content = "IE=Edge";
    c["http-equiv"] = "X-UA-Compatible";
    b.appendChild(c)
  }
  this.lo() && (this.oa = a, this.F = new $c, this.N = new gv, this.N.uG = this.oa.messages.waitingForData, this.N.CF(this.oa.messages.noData), this.N.FJ = this.oa.messages.loadingTemplates, this.N.EJ = this.oa.messages.loadingResources, this.N.DJ = this.oa.messages.loadingConfig, this.N.TI = this.oa.messages.init, this.oa.watermark && (this.N.WA = this.oa.watermark), A.G.nb(this.N, "anychartDraw", this.TH, q, this), A.G.nb(this.N, "anychartRender", this.sK, q, this), A.G.nb(this.N, "anychartError", 
  this.ZH, q, this), A.G.nb(this.N, "anychartRefresh", this.nK, q, this), A.G.nb(this.N, "dashboardViewDraw", this.UH, q, this), A.G.nb(this.N, "dashboardViewRender", this.tK, q, this), A.G.nb(this.N, "dashboardViewRefresh", this.oK, q, this), A.G.nb(this.F.Nc(), ka.pw, this.Lz, q, this), A.G.nb(window, ka.pw, this.Lz, q, this), A.G.nb(this.F.Nc(), ka.Yn, this.NJ, q, this), A.G.nb(this.F.Nc(), ka.gm, this.MJ, q, this), A.G.nb(this.F.Nc(), ka.Ps, this.PJ, q, this), A.G.nb(this.N, "pointClick", this.Yg, 
  q, this), A.G.nb(this.N, "pointMouseOver", this.Yg, q, this), A.G.nb(this.N, "pointMouseOut", this.Yg, q, this), A.G.nb(this.N, "pointMouseDown", this.Yg, q, this), A.G.nb(this.N, "pointMouseUp", this.Yg, q, this), A.G.nb(this.N, "pointSelect", this.Yg, q, this), A.G.nb(this.N, "pointDeselect", this.Yg, q, this), A.G.nb(this.N, "multiplePointsSelect", this.SJ, q, this), this.oa.visible || this.mu())
};
z.$J = s();
z.aK = s();
z.Nc = function(a) {
  var b = new XMLSerializer, c = this.F.Nc(), c = b.serializeToString(c);
  if(a) {
    var a = "", d, f, g, k, l, n, b = 0, c = c.replace(/\r\n/g, "\n");
    f = "";
    for(g = 0;g < c.length;g++) {
      k = c.charCodeAt(g), 128 > k ? f += String.fromCharCode(k) : (127 < k && 2048 > k ? f += String.fromCharCode(k >> 6 | 192) : (f += String.fromCharCode(k >> 12 | 224), f += String.fromCharCode(k >> 6 & 63 | 128)), f += String.fromCharCode(k & 63 | 128))
    }
    for(c = f;b < c.length;) {
      d = c.charCodeAt(b++), f = c.charCodeAt(b++), g = c.charCodeAt(b++), k = d >> 2, d = (d & 3) << 4 | f >> 4, l = (f & 15) << 2 | g >> 6, n = g & 63, isNaN(f) ? l = n = 64 : isNaN(g) && (n = 64), a = a + wc().charAt(k) + wc().charAt(d) + wc().charAt(l) + wc().charAt(n)
    }
  }else {
    a = c
  }
  return a
};
z.show = function() {
  this.F.Nc().setAttribute("visibility", "visible")
};
z.mu = function() {
  this.F.Nc().setAttribute("visibility", "hidden")
};
function lv() {
  window.console && window.console.log("Feature not supported yet")
}
z.lo = function() {
  return window.SVGAngle != h
};
z.write = function(a) {
  if(this.lo()) {
    var b = this.F.Nc();
    this.N.YH = this.oa.enabledChartMouseEvents;
    b.setAttribute("width", this.oa.width);
    b.setAttribute("height", this.oa.height);
    a.appendChild(b);
    document.body.appendChild(this.F.Jr);
    this.k = this.kD();
    this.N.ce(this.F, this.F.Nc(), this.k);
    return j
  }
};
z.kD = function() {
  var a, b, c = this.F.Nc();
  a = c.clientWidth;
  b = c.clientHeight;
  a || (a = /^([0-9]+\%?)/.exec(this.oa.width)[1], a = Mb(a) ? c.parentNode.clientWidth * Nb(a) : a);
  b || (b = /^([0-9]+\%?)/.exec(this.oa.height)[1], b = Mb(b) ? c.parentNode.clientHeight * Nb(b) : b);
  return new P(0, 0, a, b)
};
z.NA = s();
z.Mp = function(a, b) {
  this.F.Nc().setAttribute("width", a);
  this.F.Nc().setAttribute("height", b);
  this.Wa(a, b)
};
z.remove = function() {
  this.F.Nc().parentNode && (this.F.Nc().parentNode.removeChild(this.F.Nc()), A.G.ed(this.N, "anychartDraw", this.TH, q, this), A.G.ed(this.N, "anychartRender", this.sK, q, this), A.G.ed(this.N, "anychartError", this.ZH, q, this), A.G.ed(this.N, "anychartRefresh", this.nK, q, this), A.G.ed(this.N, "dashboardViewDraw", this.UH, q, this), A.G.ed(this.N, "dashboardViewRender", this.tK, q, this), A.G.ed(this.N, "dashboardViewRefresh", this.oK, q, this), A.G.ed(this.F.Nc(), ka.pw, this.Lz, q, this), A.G.ed(window, 
  ka.pw, this.Lz, q, this), A.G.ed(this.F.Nc(), ka.Yn, this.NJ, q, this), A.G.ed(this.F.Nc(), ka.gm, this.MJ, q, this), A.G.ed(this.F.Nc(), ka.Ps, this.PJ, q, this), A.G.ed(this.N, "pointClick", this.Yg, q, this), A.G.ed(this.N, "pointMouseOver", this.Yg, q, this), A.G.ed(this.N, "pointMouseOut", this.Yg, q, this), A.G.ed(this.N, "pointMouseDown", this.Yg, q, this), A.G.ed(this.N, "pointMouseUp", this.Yg, q, this), A.G.ed(this.N, "pointSelect", this.Yg, q, this), A.G.ed(this.N, "pointDeselect", this.Yg, 
  q, this), A.G.ed(this.N, "multiplePointsSelect", this.SJ, q, this))
};
z.clear = function() {
  var a = this.N;
  a.fd || a.hb.ca().clear()
};
z.refresh = function() {
  this.N.refresh()
};
z.qs = function(a, b) {
  this.N.qs(a, b)
};
z.bA = function(a) {
  cv(this.N, a)
};
z.Nx = function() {
  return this.N.mb()
};
z.TH = function() {
  this.oa.enabledChartEvents && this.oa.dispatchEvent({type:"draw"})
};
z.sK = function() {
  this.oa.enabledChartEvents && this.oa.dispatchEvent({type:"render"})
};
z.nK = function() {
  this.oa.enabledChartEvents && this.oa.dispatchEvent({type:"refresh"})
};
z.ZH = function(a) {
  this.oa.enabledChartEvents && this.oa.dispatchEvent({type:"error", errorCode:a.errorCode, errorMessage:a.errorMessage})
};
z.UH = function(a) {
  this.oa.enabledChartEvents && this.oa.dispatchEvent({type:"drawView", view:a.PA})
};
z.tK = function(a) {
  this.oa.enabledChartEvents && this.oa.dispatchEvent({type:"renderView", view:a.PA})
};
z.oK = function(a) {
  this.oa.enabledChartEvents && this.oa.dispatchEvent({type:"refreshView", view:a.PA})
};
z.Lz = function() {
  var a = this.kD();
  if(this.k.width != a.width || this.k.height != a.height) {
    this.k = a.Ca(), this.Wa(this.k.width, this.k.height), this.oa.enabledChartEvents && this.oa.dispatchEvent({type:"resize"})
  }
};
z.NJ = function(a) {
  this.oa.enabledChartMouseEvents && this.oa.dispatchEvent({type:"chartMouseMove", mouseX:a.clientX, mouseY:a.clientY})
};
z.MJ = function(a) {
  this.oa.enabledChartMouseEvents && this.oa.dispatchEvent({type:"chartMouseDown", mouseX:a.clientX, mouseY:a.clientY})
};
z.PJ = function(a) {
  this.oa.enabledChartMouseEvents && this.oa.dispatchEvent({type:"chartMouseUp", mouseX:a.clientX, mouseY:a.clientY})
};
z.Yg = function(a) {
  if(a.j) {
    var b = co(a.j)
  }
  this.oa.dispatchEvent({type:a.Jb(), data:b, mouseX:a.QJ, mouseY:a.RJ})
};
z.SJ = function(a) {
  if(a && 0 != a.Fe()) {
    for(var b = {points:[]}, c = a.Fe(), d = 0;d < c;d++) {
      b.points.push(co(a.Ga(d)))
    }
    this.oa.dispatchEvent({type:a.Jb(), data:b})
  }
};
z.Wa = function(a, b) {
  if(this.N && a && !(0 > a || !b || 0 > b || !this.N)) {
    this.k.width = a, this.k.height = b, this.N.Wa(this.k)
  }
};
z.Hi = function(a) {
  this.N.Hi(a)
};
z.Oh = function(a) {
  this.N.Oh(a)
};
z.wj = function(a) {
  this.N.Fk(a)
};
z.Kn = function(a) {
  this.N.Kn(a)
};
z.Gv = function(a) {
  this.N.Oh(a)
};
z.jA = function(a) {
  this.N.Hi(a)
};
z.Np = function(a, b) {
  this.N.Np(a, b, p)
};
z.fA = function(a, b) {
  var c = this.N;
  c.fd && (c = Kn(c, a)) && c.Oh(b)
};
z.Pd = function(a, b) {
  this.N.Pd(a, b)
};
z.Kj = function(a, b, c) {
  this.N.Kj(a, b, c)
};
z.Bk = function(a, b) {
  this.N.Bk(a, b)
};
z.Nk = function(a, b, c) {
  this.N.Nk(a, b, c)
};
z.Ok = function() {
  lv()
};
z.mk = function(a, b, c) {
  this.N.mk(a, b, c)
};
z.tj = function(a, b, c) {
  this.N.tj(a, b, c)
};
z.MA = function() {
  lv()
};
z.uA = function() {
  lv()
};
z.Lj = function(a) {
  this.N.Lj(a)
};
z.Mj = function(a, b) {
  this.N.Mj(a, b)
};
z.Ck = function(a) {
  this.N.Ck(a)
};
z.Pk = function(a, b) {
  this.N.Pk(a, b)
};
z.Ik = function(a, b) {
  this.N.Ik(a, b)
};
z.nk = function(a, b) {
  this.N.nk(a, b)
};
z.lk = function(a, b) {
  this.N.lk(a, b)
};
z.qA = function() {
  lv()
};
z.Hk = function(a, b) {
  this.N.Hk(a, b)
};
z.Is = function(a, b, c) {
  this.N.Is(a, b, c)
};
z.zs = function(a, b) {
  this.N.zs(a, b)
};
z.Gs = function(a, b) {
  this.N.Gs(a, b)
};
z.As = function(a, b, c) {
  this.N.As(a, b, c)
};
z.Ls = function(a, b, c) {
  this.N.Ls(a, b, c)
};
z.Js = function(a, b, c) {
  this.N.Js(a, b, c)
};
z.xs = function(a, b, c) {
  this.N.xs(a, b, c)
};
z.ys = function(a, b, c, d) {
  this.N.ys(a, b, c, d)
};
z.Fs = function(a, b, c) {
  this.N.Fs(a, b, c)
};
z.Ks = function(a, b, c, d) {
  this.N.Ks(a, b, c, d)
};
z.UA = function(a) {
  (a = Kn(this.N, a)) && (a.fd || a.hb.ca().clear())
};
z.Es = function(a) {
  this.N.Es(a)
};
z.Ds = function(a, b, c) {
  this.N.Ds(a, b, c)
};
z.Cs = function(a, b, c, d) {
  this.N.Cs(a, b, c, d)
};
z.Bs = function(a, b, c) {
  this.N.Bs(a, b, c)
};
z.Hs = function(a, b, c, d) {
  this.N.Hs(a, b, c, d)
};
z.rs = function(a, b, c, d) {
  this.N.rs(a, b, c, d)
};
z.Zq = function() {
  lv()
};
z.$x = function() {
  lv()
};
z.Gz = function() {
  lv()
};
z.Tz = function() {
  lv()
};
z.Uz = function() {
  lv()
};
z.Vz = function() {
  lv()
};
z.Wz = function() {
  lv()
};
z.scrollTo = function() {
  lv()
};
z.RA = function() {
  lv()
};
z.SA = function() {
  lv()
};
z.gy = function() {
  lv()
};
z.hy = function() {
  lv()
};
z.ey = function() {
  lv()
};
z.fy = function() {
  lv()
};
z.kA = function() {
  lv()
};
z.lA = function() {
  lv()
};
z.mA = function() {
  lv()
};
z.gA = function() {
  lv()
};
z.hA = function() {
  lv()
};
z.iA = function() {
  lv()
};
z.zw = function() {
  lv()
};
})()
