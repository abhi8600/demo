'use strict';

module.exports = function(grunt) {
	require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

	grunt.initConfig({
		// Project settings
		yeoman: {
			// Configurable paths
			app: '.',
			dist: 'dist'
		},
		// load all grunt tasks
		watch: {
			options: {
				livereload: true
			},
			css : {
				files: ['app/css/**/*.css']
			},
			js : {
				files: ['app/**/*.js','birst/**/*.js'],
				tasks: ['jshint']
			},
			html : {
				files: ['index.html','app/**/*.html']
			},
			img : {
				files : ['app/img/**/*.{gif,jpeg,jpg,png,svg,webp}']
			}
		},

		connect: {
			options : {
				port: 9000,
				hostname: 'localhost',
				livereload: 35729
			},
			livereload : {
				options: {
					open: true,
					base: [
						'.tmp',
						'.'
					]
				}
			},
			dist : {
				options : {
					open : true,
					base : 'dist',
					livereload : false
				}
			}
		},

		// Wipe out previous builds and test reporting.
		clean: ['dist/', 'test/reports'],

		// Run your source code through JSHint's defaults.
		jshint: ['app/**/*.js'],

		// This task uses James Burke's excellent r.js AMD builder to take all
		// modules and concatenate them into a single file.
		requirejs: {
			js: {
				options: {
					mainConfigFile: 'app/config.js',
					generateSourceMaps: true,
					include: ['main'],
					insertRequire: ['main'],
					out: 'dist/app/main.min.js',
					optimize: 'uglify2',

					// Since we bootstrap with nested `require` calls this option allows
					// R.js to find them.
					findNestedDependencies: true,

					// Include a minimal AMD implementation shim.
					name: 'almond',

					// Setting the base url to the distribution directory allows the
					// Uglify minification process to correctly map paths for Source
					// Maps.
					baseUrl: 'dist/app',

					// Wrap everything in an IIFE.
					wrap: true,

					// Do not preserve any license comments when working with source
					// maps.  These options are incompatible.
					preserveLicenseComments: false
				}
			},
			css : {
				options: {
					optimizeCss: 'standard',
					cssIn: 'app/css/main.css',
					out: 'dist/app/css/main.min.css'
				}
			}
		},

		processhtml: {
			release: {
				files: {
					'dist/index.html': ['index.html']
				}
			}
		},

		// Move vendor and app logic during a build.
		copy: {
			release: {
				files: [
					{ src: ['app/**'], dest: 'dist/' },
					{ src: ['birst/**'], dest: 'dist/' },
					{ src: 'bower/**', dest: 'dist/' },
					{ src: 'mocks/**', dest: 'dist/' }
				]
			}
		}
	});

	grunt.registerTask('build',[
		'clean',
		'copy',
		'jshint',
		'requirejs:js',
		'requirejs:css',
		'processhtml'
	]);

	grunt.registerTask('server', function(target) {
		if( target === 'dist') {
			return grunt.task.run(['build', 'connect:dist:keepalive']);
		}
		grunt.task.run([
			'connect:livereload',
			'watch'
		]);
	} );
};
