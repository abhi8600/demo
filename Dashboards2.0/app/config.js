// Require.js Configurations
// -------------------------
require.config( {

	// Sets the js folder as the base directory for all future relative paths
	baseUrl : 'app',

	// 3rd party script alias names (Easier to type 'jquery' than 'libs/jquery, etc')
	// probably a good idea to keep version numbers in the file names for updates checking
	paths : {

		// Core Libraries
		// --------------

		'jquery': '../bower/jquery/jquery.min',

		'almond' : '../bower/almond/almond',

		'signals' : '../bower/js-signals/dist/signals.min',

		'crossroads': '../bower/crossroads/dist/crossroads.min',

		'hasher' : '../bower/hasher/dist/js/hasher.min',

		'rivets' : '../bower/rivets/dist/rivets.min',

		'watch' : '../bower/watch/src/watch.min',

		'lodash' : '../bower/lodash/dist/lodash.min',

		'ldsh' : '../bower/lodash-template-loader/loader',

		'birst' : '../birst/birst',

		//'extjs' : '../birst/ext-all',

		'Ext.Birst.core.Renderer' : '../birst/Ext.Birst.core.Renderer',

		'Birst.core.Renderer' : '../birst/Birst.core.Renderer',

		'anychart' : '../birst/AnyChart',

		'anychartHTML5' : '../birst/AnyChartHTML5',

		'webservice' : 'services/webservice'

	}/*,

	shim : {
		'anychart' : {
			'exports' : 'AnyChart'
		}
	}*/

} );
