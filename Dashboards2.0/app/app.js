define( function( require, exports, module ) {
	'use strict';

	var Birst = require( 'birst' );
	var $ = require( 'jquery' );
	var webservice = require( 'webservice' );
	var locale = require( 'services/locale' );
	var app = module.exports;
	var signals = require( 'signals' );
	app.locale = locale.load();
	app.ws = webservice;
	app.root = '/';

	app.mainContainer = $( '#main' );

	app.liveviews = {};

	app.editMode = new signals.Signal();
	app.editMode.memorize = true;
	app.editMode.dispatch( true );

	// Utility functions
	app.elementInDom = function( el ) {
		/* jshint ignore:start */
		while ( el = el.parentNode ) {
			if (el === document) {
				return true;
			}
		}
		/* jshint ignore:end */
		return false;
	};

	app.cleanChildViews = function() {
		var cid, view;
		for ( cid in app.liveviews ){
			view = app.liveviews[ cid ];
			if ( !app.elementInDOM( view.el ) ) {
				app.liveviews[ cid ] = null;
				delete app.liveviews[ cid ];
				view.remove();
			}
		}
	};

} );
