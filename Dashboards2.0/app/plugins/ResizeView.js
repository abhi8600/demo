define( function( require, exports, module ) {
	'use strict';
	var Birst = require( 'birst' );

	var ResizeView = Birst.View.extend( {

		events : {
			'mousedown .resize-handle' : '_startResize',
			'mouseup.resize .resize-box' : '_endResize',
			'afterRender' : 'initResizeable',
			'mouseup.resize window' : '_endResize'
		},

		initResizeable : function( e ) {
			this.$el.addClass( 'resizeable' );
			this.$el.append( '<span class="resize-box"></span><span class="resize-handle"></span>' );
			this.$resizeBox = this.$el.find( '.resize-box' );
		},

		_startResize : function( e ) {
			if( e.which === 1 ) {
				this.resizing = true;
				$( 'body' ).addClass( 'noselect' );
				this.$resizeBox.show();
				this.$el.parent().on( 'mousemove.resizing', null, this, this._handleResize );
			}
		},

		_handleResize : function( e ) {
			e.stopPropagation();
			var that = e.data;
			var off = that.$el.offset();
			var startRightPos = off.left + that.$el.width();
			var changeRight = startRightPos - e.clientX;
			var startBottomPos = off.top + that.$el.height();
			var changeBottom = startBottomPos -e.clientY;
			that.$resizeBox.css( { right : changeRight, bottom : changeBottom } );
		},

		_endResize : function( e ) {
			if( this.resizing ) {
				var width = this.$resizeBox.outerWidth();
				var height = this.$resizeBox.outerHeight();
				this.afterResize( { width : width, height : height } );
				this.$resizeBox.css( { right : 0, bottom : 0 } ).hide();
				$( 'body' ).removeClass( 'noselect' );
				this.$el.parent().off( '.resizing' );
				this.resizing = false;
			}
		},
		//What to do?
		afterResize : function( dimensions ) {
			console.log( dimensions );
		}

	} );

	module.exports = ResizeView;

} );