define( function( require, exports, module ) {
	'use strict';
	var app = require( 'app' );
	var Birst = require( 'birst' );
	var Report = require( 'modules/report/index' );
	var Page = require( 'modules/page/index' );
	var $ = require( 'jquery' );

	var Router = Birst.Router.extend( {
		// Define route patterns.
		routes : {
			'' : 'homeRoute',
			'home' : 'homeRoute'
		},

		// Route Handlers
		homeRoute : function() {
			var ReportList = new Report.Views.List( { el : this.reportsPanel } );
			var PageCanvas = new Page.Views.List( { el : this.pageCanvas } );
		},

		initialize : function() {
			this.reportsPanel = $( '.db-reports' );
			this.pageCanvas = $( '.db-canvas' );
		}
	} );

	module.exports = Router;

} );
