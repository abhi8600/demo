define( function( require, exports, module ) {
	'use strict';
	var $ = require( 'jquery' );
	var _ = require( 'lodash' );
	var api = 'mocks/';

	module.exports = {

		get : function( path ) {
			path = api + path;
			if( api.match( /mocks/ ) !== null ) {
				path += '/index.json';
			}
			var dataType = path.indexOf( 'json' ) > 0 ? 'json' : 'xml';
			return $.ajax( { url : path, dataType : dataType } );
		},

		escapeXml : function( str ) {
			return str ? str.replace( /&/g, '&amp;' ).replace( /</g, '&lt;' ).replace( />/g, '&gt;' ).replace( /"/g, '&quot;' ) : '';
		},

		createEnvelope : function( service, fnName, args, ns ) {
			var soap = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><SOAP-ENV:Body><ns:';
			soap += fnName + ' xmlns:ns="' + ns + '">';
			for ( var i = 0; i < args.length > 0; i++ ) {
				var key = args[i].key;
				var value = args[i].value;
				if ( key ) {
					soap += '<ns:' + key + '>' + this.escapeXml( value ) + '</ns:' + key + '>';
				}
			}
			soap += '</ns:' + fnName + '></SOAP-ENV:Body></SOAP-ENV:Envelope>';
			return soap;
		},

		getCookie : function( cookieName ) {
			return document.cookie.split( ';' ).reduce( function( prev, c ) {
				var arr = c.split( '=' );
				return ( arr[0].trim() === cookieName ) ? arr[1] : prev;
			}, undefined );
		},

		getAntiCSRFToken : function() {
			if ( !this.token ) {
				this.token = this.getCookie( "XSRF-TOKEN" );
			}
			return this.token;
		},

		request: function( url, ns, soapAction, service, fnName, args, scope, successFn, failFn, additionalParam, defErrMsg, maskself, cookie ) {
			var that = this,
				envelope = this.createEnvelope( service, fnName, args, ns );
			// TEMPOMIT:
			// this.createCookieProvider();
			var headers = {
				'Content-Type' : 'text/xml; charset=utf-8',
				'SOAPAction' : soapAction,
				'tns' : 'urn:',
				'X-XSRF-TOKEN': this.getAntiCSRFToken()
			};
			if ( cookie ) {
				headers.Cookie = cookie;
			}
			var options = {
				url : url,
				type : 'POST',
				headers : headers,
				context : scope,
				data : envelope,
				timeout : 300000,
				// TEMPOMIT:
				//additionalParam: additionalParam,
				success : function( data, textStatus, response ) {
					// if (!maskself) {Ext.getBody().unmask();}

					/*TEMPOMIT:
					if (response.status === 302) {
						window.open("/Login.aspx", "_self");
						return;
					}
					try {
						var error = response.responseXML.getElementsByTagName('Error');
						if (error && error.length > 0) {
							var errorMessage = getXmlChildValue(error[0], 'ErrorMessage');
							if (errorMessage === 'Unable to continue. Please login again') {
								window.open("/Login.aspx", "_self");
							}

							if ((defErrMsg === undefined || defErrMsg) && errorMessage) {
								Ext.create('Birst.Util,ErrorDialog', {
										msg: errorMessage,
										btnText: 'OK'
								}).show();
							}
						}
					} catch (err) {
					}*/
					var bwsr = response.responseXML.getElementsByTagName( 'com.successmetricsinc.WebServices.BirstWebServiceResult' );
					if ( bwsr && bwsr.length > 0 ) {
						var errorCode = that.getXmlChildValue( bwsr[0], 'ErrorCode' );
						if ( errorCode !== '0' ) {
							if ( failFn ) {
								failFn.call( scope, response, options );
							}
							/*TEMPOMIT:
							else {
								var errorMessage = getXmlChildValue(bwsr[0], 'ErrorMessage');
								if (errorMessage) {
									Ext.create('Birst.Util,ErrorDialog', {
										msg: errorMessage,
										btnText: 'OK'
									}).show();
								}
								else {
									Ext.create('Birst.Util,ErrorDialog', {
										msg: 'General failure. Error code:' +  errorCode,
										btnText: 'OK'
									}).show();
								}
							}*/
							return;
						}
					}
					if ( successFn ) {
						successFn.call( scope, response, options );
					}
				},
				failure: failFn ?
					function( data, textStatus, response ) {
						/*TEMPOMIT:
						if (!maskself) {
							Ext.getBody.unmask();
						}
						if (response.status === 401) {
							window.open("/Login.aspx", "_self");
						}*/
						failFn.call(scope, response, options);
					}
						:
					function(data, textStatus, response) {
						/*TEMPOMIT:
						Ext.getBody().unmask();
						if (response.status === 401) {
							window.open("/Login.aspx", "_self");
						}

						var errtextmsg;
						if (response.status === 408 || response.status === 500) {
							errtextmsg = 'Server did not respond in a timely fashion or internal error has occured. Please try again later.';
						}
						else {
							errtextmsg = 'General failure. Request could not be completed. Please try again later.';
						}

						Ext.create('Birst.Util,ErrorDialog', {
							msg: errtextmsg,
							btnText: 'OK',
							handler: function () {
								window.open("/html/home.aspx", '_self');
							}
						}).show();*/
				}
			};

			var response = $.ajax( options );
			/*TEMPOMIT:
			if (!maskself) {
				Ext.getBody().mask('');
			}*/
			return response; //	TODO: remove this response variable declaration and simply return expression result if possible.
		},

		xml2Str : function( xmlNode ) {
			try {
				// Gecko- and Webkit-based browsers (Firefox, Chrome), Opera.
				return ( new XMLSerializer() ).serializeToString( xmlNode );
			} catch (e) {
				try {
					// Internet Explorer.
					return xmlNode.xml;
				} catch (e1) {
					//Other browsers without XML Serializer
					console.log( 'Xmlserializer not supported' );
				}
			}
			return false;
		},

		getXmlValue : function( node ) {
			var child = node.firstChild;
			if (child && child.nodeName === '#text') {
				if( child.nodeValue ) {
					return child.nodeValue;
				}
				return child.wholeText;
			}
			return null;
		},

		getXmlChildValue : function( node, childName) {
			var child = node.getElementsByTagName( childName );
			if ( child && child.length > 0 ) {
				return this.getXmlValue( child[0] );
			}
			return null;
		},

		getRootPath : function() {
			if( !this.rootPath ) {
				var path = window.location.href;
				if( path.indexOf( 'dashboards2.0' ) > 0 ) {
					this.rootPath = '/dashboards2.0/';
				} else {
					this.rootPath = '';
				}
			}
			return this.rootPath;
		},

		adhocRequest : function( opts ) {
			return $.ajax( this.getRootPath() + 'mocks/getDashboardRenderedReportResponse/index.xml', { dataType : 'xml' } );
		},

		getFilterPrompts : function( opts ) {
			return $.ajax( this.getRootPath() + 'mocks/getFilterPrompts/index.xml', _.extend( { dataType : 'xml' }, opts ) );
		},

		getFileMethod : function( method, params ) {
			/*available methods:
			getDirectory
			getVisibleFilesystem
			[ { key: 'dir', value: null }, { key: 'writeable', value: 'false'} ],*/
			var url = '/SMIWeb/services/File.FileHttpSoap11Endpoint',
			ns = 'http://file.WebServices.successmetricsinc.com',
			soapAction = 'urn:' + method;
			return this.request( url, ns, soapAction, method, method, params, this );
		},

		getAdhocMethod : function( method, params ) {
			var url = '/SMIWeb/services/Adhoc.AdhocHttpSoap11Endpoint',
			ns = 'http://adhoc.WebServices.successmetricsinc.com',
			soapAction = 'urn:' + method;
			return this.request( url, ns, soapAction, method, method, params, this );
		}

	};

} );
