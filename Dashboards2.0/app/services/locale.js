define( function( require, exports, module ) {
	'use strict';
	var $ = require( 'jquery' );
	module.exports = {

		Resources : {},

		TouchGlobalResource : function( TextRes ) {
			var txtress = TextRes.split('\n');
			var _res = {};
			try {
				txtress.map( function( ln ){
					if ( ln.length === 0 ) { return; }
					var res = ln.match( /(\w*)\=(.*)/ );
					if ( res ) {
						_res[res[1]] = res[2];
					}
				} );
			} catch( e ) {
				console.log( 'An exception occurred while processing localization file: ' + e );
			}

			// todo: validation?
			this.Resources = _res;
			return;
		},

		load : function( scope, callback, file ){
			var el = document.getElementById( 'birst-locale-plchldr' );
			var locale;
			var fileName;
			var resRoot; //varies if served from flash or html5 page
			var url;

			if ( el ) {
				locale = el.value;
			} else {
				locale = 'en_US';
			}

			// property file
			if ( !file ) {
				this.resPrefix = 'dashboards';
				if ( file ) {
					fileName = file;
				} else {
					fileName = this.resPrefix + 'Properties.txt';
				}
				resRoot = 'app/locale';
				url = resRoot +'/' + locale + '/' + fileName;
			}

			this.request( url,
				function( response ){
					// success
					this.TouchGlobalResource( response );
					if( callback ){
						callback.call( scope );
					}

				},
				function( response ){
					// fallback to the default locale
					var url =  resRoot + '/' + 'en_US/' + fileName;
					this.request( url,
						function( response ){
							// success
							this.TouchGlobalResource( response );
							if(callback){
								callback.call(scope);
							}
						},
						function(response){
							console.log( response.status === 404 ? 'Localization file for a selected language was not found' : 'Failed loading a localization file: ' + url );
						} );
				});
			return this;
		},

		request : function( url, succFunc, failFunc ){

			$.ajax({
				url: url ,
				method: 'GET',
				dataType : 'text',
				context: this,
				contentType : 'text/plain; charset=utf-8',
				timeout: 50000,
				success: function (response, status, xhr) {
					if (succFunc) {
						succFunc.call( this, response );
					}
				},
				failure: function (response, options) {
					if (failFunc) {
						failFunc.call( this, response );
					}
				}
			});
		},
		//TODO - this getXX functions need to fall back to en_US values if the locale we're looking for
		//doesn't exist
		get : function( prop ){
			var value = this.Resources[ prop ];
			return  (value) ? value : '';
		},
		getNBS : function( prop ){
			var value = this.Resources[prop];
			return (value) ? value.replace(/ /g,'&nbsp;') : '';
		}

	};
} );
