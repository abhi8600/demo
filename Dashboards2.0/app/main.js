// Break out the application running from the configuration definition to
// assist with testing.
require( [ 'config' ], function( conf ) {
	'use strict';
	// Kick off the application.
	require( [ 'app', 'jquery', 'router', 'rivets', 'watch' ], function( app, $, Router, rivets, WatchJS ) {

		var watch = WatchJS.watch;
		var unwatch = WatchJS.unwatch;
		var callWatchers = WatchJS.callWatchers;

		rivets.configure({
			prefix : 'data-rv',
		});
		rivets.adapters['.'] ={
			subscribe: function(obj, keypath, callback) {
				watch(obj, keypath, callback);
			},
			unsubscribe: function(obj, keypath, callback) {
				unwatch(obj, keypath, callback);
			},
			read: function(obj, keypath) {
				return obj[keypath];
			},
			publish: function(obj, keypath, value) {
				obj[keypath] = value;
			}
		};

		app.router = new Router();
	});
});
