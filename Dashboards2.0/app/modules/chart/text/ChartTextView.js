define( function( require, exports, module ) {
	'use strict';
	var birst = require( 'birst' );
	var Txt = require( 'modules/chart/text/ChartTextModel' );
	var TxtDivView = module.exports = birst.View.extend( {

		tagName : 'label',

		autoRender : true,

		renderFunction : function() {
			var xPos, yPos;

			this.$el.text( this.model.text ).css( this.model.style );

			if ( this.link ) {
				this.$el.addClass( 'clickable' ).on( this._namespace + '.click', $.proxy( this.handleClick, this ) );
			}

			/*TEMPOMIT: if (link) {
				label.on('afterrender', function(me, eOpts) {
					me.getEl().on('click', this.onClick, this, eOpts);
				}, this, textXml);
				label.addCls('clickable');
			}*/

		},

		handleClick : function( e ) {
			console.log( 'clicked' );
		}

	} );
} );
