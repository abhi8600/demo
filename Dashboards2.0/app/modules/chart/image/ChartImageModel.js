define( function( require, exports, module ) {
	'use strict';
	var _ = require( 'lodash' );
	var app = require( 'app' );

	var ChartImageModel = module.exports = function( opts ) {
		var defaults = {
			xOrigin : 0,
			yOrigin : 0,
			xml : null
		};
		_.extend( this, defaults, opts );
		this.parse();
	};

	ChartImageModel.prototype = {
		parse : function(){
			this.x = parseInt( this.xml.getAttribute( 'x' ), 10 );
			this.y = parseInt( this.xml.getAttribute( 'y' ), 10 );
			this.width  = parseInt( this.xml.getAttribute('width'), 10 );
			this.height = parseInt( this.xml.getAttribute('height'), 10 );
			this.backgroundColor = this.xml.getAttribute('backcolor') || 'transparent';
			this.link = this.xml.getAttribute('link');
			this.src = '/SMIWeb/' + app.ws.getXmlValue( this.xml );
			this.setStyle();
			this.setAttributes();
			this.setLink();
		},
		setStyle : function() {
			this.style = {
				backgroundColor : this.backgroundColor,
				width : this.width,
				height : this.height,
				left : this.x,
				top : this.y
			};
		},
		setAttributes : function() {
			this.attrs = {
				src : this.src,
			};
		},
		setLink : function() {

			if ( this.link && this.link !== '' ) {
				this.attrs.className = 'clickable';
			}
			/*OMIT MOVE TO VIEW: var img = Ext.create('Ext.Img', properties);

			if (link && link !== '') {
				img.on("afterrender", function(me, eOpts) {
					var el = me.getEl();
					if (el) {
						el.on('click', this.onClick, this, this.xml);
					}

				}, this, this.xml);
			}*/
		},
		_dataType : 'ChartImage'
	};

} );
