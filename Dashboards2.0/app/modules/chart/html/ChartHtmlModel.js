define( function( require, exports, module ) {
	'use strict';
	var _ = require( 'lodash' );
	var HTMLModule = module.exports = function( opts ) {
		//OMIT: this.html = this.unescapeXml( xml.getAttribute( 'value' ) );
		var defaults = {
			xOrigin : 0,
			yOrigin : 0,
			xml : null
		};
		_.extend( this, defaults, opts );
		this.parse();
	};

	HTMLModule.prototype = {
		parse : function() {
			this.html = this.xml.getAttribute( 'value' );
			this.x = parseInt( this.xml.getAttribute( 'x' ), 10 ) + this.xOrigin;
			this.y = parseInt( this.xml.getAttribute( 'y' ), 10 ) + this.yOrigin;
		},
		_dataType : 'ChartHtml'
	};

} );
