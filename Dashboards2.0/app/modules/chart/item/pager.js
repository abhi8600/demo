define( function( require, module, exports ) {
	'use strict';
//	require( 'extjs' );
//	var Ext = window.Ext;
	Ext.define('Birst.dashlet.Pager', {
		extend: 'Ext.toolbar.Paging',
		count: null,
		currentPage: null,
		dashletController: null,

		constructor: function(dc) {
			this.dashletController = dc;
			this.dock = 'bottom';
			this.currentPage = 0;
			this.count = 1;
			//TEMPOMIT: this.callParent([{buttonAlign: 'middle', cls: 'dashlet-pager'}]);
		},

		getPagingItems: function() {
			var ret = this.callParent();
			ret.pop();
			ret.pop();
			ret.unshift({xtype: 'component', flex: 0.5});
			ret.push({xtype: 'component', flex: 0.5});
			return ret;
		},

		setCurrentPage: function(c) {
			this.currentPage = c;
		},

		getCurrentPage: function() {
			return this.currentPage;
		},

		setCount: function(c) {
			this.count = c;
		},

		getCount: function() {
			return this.count;
		},

		updateInfo : function(){
		},

		onLoad : function(){
			var me = this,
				currPage,
				pageCount,
				afterText,
				count,
				isEmpty;
			console.log( me );
			count = me.getCount();
			isEmpty = count === 0;
			if (!isEmpty && count > 1) {
				me.show();
				currPage = me.getCurrentPage() + 1;
				pageCount = me.getCount();
				afterText = Ext.String.format(me.afterPageText, isNaN(pageCount) ? 1 : pageCount);
			} else {
				me.hide();
				currPage = 1;
				pageCount = 1;
				afterText = Ext.String.format(me.afterPageText, 0);
			}

			Ext.suspendLayouts();
			// me.child('#afterTextItem').setText(afterText);
			// me.child('#inputItem').setDisabled(isEmpty).setValue(currPage);
			// me.child('#first').setDisabled(currPage === 1 || isEmpty);
			// me.child('#prev').setDisabled(currPage === 1  || isEmpty);
			// me.child('#next').setDisabled(currPage === pageCount  || isEmpty);
			// me.child('#last').setDisabled(currPage === pageCount  || isEmpty);
			me.updateInfo();
			Ext.resumeLayouts(true);

		},

		onPagingBlur : function(e){
			this.child('#inputItem').setValue(this.getCurrentPage());
		},

		readPageFromInput : function(){
			var v = this.child('#inputItem').getValue(),
				pageNum = parseInt(v, 10);

			if (!v || isNaN(pageNum)) {
				this.child('#inputItem').setValue(this.getCurrentPage() + 1);
				return false;
			}
			return pageNum;
		},

		onPagingKeyDown : function(field, e){
			var me = this,
				k = e.getKey(),
				increment = e.shiftKey ? 10 : 1,
				pageNum;

			if (k === e.RETURN) {
				e.stopEvent();
				pageNum = me.readPageFromInput();
				if (pageNum !== false) {
					pageNum -= 1;
					pageNum = Math.min(Math.max(1, pageNum), me.getCount() - 1);
					if(me.fireEvent('beforechange', me, pageNum) !== false){
						me.loadPage(pageNum);
					}
				}
			} else if (k === e.HOME || k === e.END) {
				e.stopEvent();
				pageNum = k === e.HOME ? 1 : me.getCount() - 1;
				field.setValue(pageNum);
			} else if (k === e.UP || k === e.PAGE_UP || k === e.DOWN || k === e.PAGE_DOWN) {
				e.stopEvent();
				pageNum = me.readPageFromInput();
				if (pageNum) {
					if (k === e.DOWN || k === e.PAGE_DOWN) {
						increment *= -1;
					}
					pageNum += increment;
					if (pageNum >= 1 && pageNum <= me.getCount()) {
						field.setValue(pageNum);
					}
				}
			}
		},

		// @private
		beforeLoad : function(){
			if(this.rendered && this.refresh){
				this.refresh.disable();
			}
		},

		/**
		 * Move to the first page, has the same effect as clicking the 'first' button.
		 */
		moveFirst : function(){
			if (this.fireEvent('beforechange', this, 1) !== false){
				this.loadPage(0);
			}
		},

		/**
		 * Move to the previous page, has the same effect as clicking the 'previous' button.
		 */
		movePrevious : function(){
			var me = this,
				prev = me.getCurrentPage() - 1;

			if (prev >= 0) {
				if (me.fireEvent('beforechange', me, prev) !== false) {
					me.loadPage(prev);
				}
			}
		},

		/**
		 * Move to the next page, has the same effect as clicking the 'next' button.
		 */
		moveNext : function(){
			var me = this,
				total = me.getCount(),
				next = me.getCurrentPage() + 1;

			if (next < total) {
				if (me.fireEvent('beforechange', me, next) !== false) {
					me.loadPage(next);
				}
			}
		},

		/**
		 * Move to the last page, has the same effect as clicking the 'last' button.
		 */
		moveLast : function(){
			var me = this,
				last = me.getCount() - 1;

			if (me.fireEvent('beforechange', me, last) !== false) {
				me.loadPage(last);
			}
		},

		loadPage: function(page) {
			this.currentPage = page;
			this.dashletController.renderByReportXml(this.dashletController.promptsController.getPromptsData(), true);
		}
	});

} );