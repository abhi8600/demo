define( function( require, exports, module ) {
	'use strict';
	var app = require( 'app' );
	var TextModel = require( 'modules/chart/text/ChartTextModel' );
	var HtmlModel = require( 'modules/chart/html/ChartHtmlModel' );
	var ButtonModel = require( 'modules/chart/button/ChartButtonModel' );
	var ImageModel = require( 'modules/chart/image/ChartImageModel' );

	var ChartCollection = module.exports = function( xmlDoc ) {
		this.xmlDoc = xmlDoc;
		this.width = 0;
		this.height = 0;
		this.itemCollection = [];
		this.chartCollection = [];
		this.parse();
	};

	ChartCollection.prototype = {
		calcMaxDims : function( item, xOffset, yOffset ) {
			if ( item.tagName === 'frame' ) {
				var frameX = item.getAttribute( 'x' );
				var frameY = item.getAttribute( 'y' );
				if ( frameX ) {
					frameX = parseInt( frameX, 10 );
				} else {
					frameX = 0;
				}

				if ( frameY ) {
					frameY = parseInt( frameY, 10 );
				} else {
					frameY = 0;
				}

				var child = item.firstChild;

				while( child ){
					this.calcMaxDims( child, frameX + xOffset, frameY + yOffset );
					child = child.nextSibling;
				}
			} else {
				var x = item.getAttribute( 'x' );
				var y = item.getAttribute( 'y' );
				x = parseInt( x, 10 ) + xOffset;
				y = parseInt( y, 10 ) + yOffset;
				var width  = parseInt( item.getAttribute( 'width' ), 10 );
				var height = parseInt( item.getAttribute( 'height' ), 10 );
				if ( isFinite( width ) && isFinite( x ) ) {
					this.width = Math.max( this.width, width + x );
				}
				if ( isFinite( height ) && isFinite( y ) ) {
					this.height = Math.max( this.height, height + y );
				}
			}
		},

		createTextObject : function( opts ) {
			var textObj = new TextModel( _.extend( { maxY : this.maxY, maxX : this.maxX }, opts ) );
			this.itemCollection.push( textObj );
			return textObj;
		},

		createHtmlObject : function( opts ) {
			var htmlObj = new HtmlModel( opts );
			this.itemCollection.push( htmlObj );
			return htmlObj;
		},

		parse : function() {
			var child;
			try {
				child = this.xmlDoc.firstChild;
				while( child ){
					var cTag = child.tagName;
					// Is text element?
					if( cTag === 'text' ){
						this.createTextObject( { xml : child } );
					// Is HTML element
					} else if ( cTag === 'html' ) {
						this.itemCollection.push( child );
					//Is Frame element
					} else if ( cTag === 'frame' ) {
						var grandChild = child.firstChild,
							xOrigin = child.getAttribute( 'x' ),
							yOrigin = child.getAttribute( 'y' ),
							label = null,
							cellType = null,
							childCounts = 0;

						xOrigin = xOrigin ? parseInt( xOrigin, 10 ) : 0;
						yOrigin = yOrigin ? parseInt( yOrigin, 10 ) : 0;

						while ( grandChild ) {
							var tag = grandChild.tagName;
							if( tag === 'text' ) {
								label = this.createTextObject( { xml : grandChild, xOrigin : xOrigin, yOrigin : yOrigin } );
								childCounts++;
							}
							if( tag === 'html' ) {
								label = this.createHtmlObject( { xml : grandChild, xOrigin : xOrigin, yOrigin : yOrigin } );
								childCounts++;
							}
							if( tag === 'adhocButton' ) {
								this.itemCollection.push( new ButtonModel( { xml : grandChild, xOrigin : xOrigin, yOrigin : yOrigin } ) );
							}
							if( tag === 'net.sf.jasperreports.crosstab.cell.type' ) {
								cellType = app.ws.getXmlValue( grandChild );
							}
							if( tag === 'image' ) {
								this.itemCollection.push( new ImageModel( { xml : grandChild } ) );
							}
							grandChild = grandChild.nextSibling;
						}
						if ( label && ( _.indexOf( [ 'Data', 'RowHeader', 'CrosstabHeader', 'ColumnHeader' ], cellType ) > 0 ) ) {
							var textWidth = parseInt( child.getAttribute( 'width' ), 10 );
							var textHeight = Math.max( parseInt( child.getAttribute( 'height' ), 10), label.height );
							if (childCounts > 1) {
								textWidth = label.width + ( textWidth - ( (label.x - xOrigin ) + label.width ) );
							}

							if (cellType === 'CrosstabHeader') {
								textHeight = label.height;
							}
							else if (childCounts > 1) {
								textHeight = label.height + parseInt( child.getAttribute( 'height' ), 10 ) - ( ( label.y - yOrigin ) + label.height );
							}

							label.width = Math.min( label.width, textWidth );
							label.height = textHeight;
						}
					} else if ( cTag === 'adhocButton' ) {
						this.itemCollection.push( new ButtonModel( { xml : child, xOrigin : 0, yOrigin : 0 } ) );
					} else if ( cTag === 'image' ) {
						this.itemCollection.push( new ImageModel( { xml : child, xOrigin : 0, yOrigin : 0 } ) );
					}
					child = child.nextSibling;
				}
			} catch( err ) {

			}

			child = this.xmlDoc.firstChild;
			while( child ) {
				if( child.tagName === 'chart' || child.tagName === 'gauge' ) {
					this.chartCollection.push( { xml : child, scale : false, type : 'normal' } );
				}
				else if (child.tagName === 'frame') {
					this.chartCollection.push( { xml : child, type : 'frame' } );
				}
				child = child.nextSibling;
			}

		}
	};
} );
