define( function( require, exports, module ) {
	'use strict';
	//require( 'extjs' );
	var $ = require( 'jquery' );
	var _ = require( 'lodash' );
	require( 'anychart' );
	require( 'anychartHTML5' );
	var app = require( 'app' );
	var Birst = require( 'birst' );
	var chartModule;
	var AnyChart = window.AnyChart;
	var anychart = window.anychart;

	$.fn.getRegion = function() {
		var r = this.offset();
		r.right = r.left + this.innerWidth();
		r.bottom = r.top + this.innerHeight();
		return r;
	};

	var birstCoreRenderer = function( panel, dashletCtrl ) {
		chartModule = require( 'modules/chart/index' );
		//TEMPOMIT: requires: ['Ext.dd.DragTracker', 'Ext.util.Region'],
		this.xmlDoc = null;
		this.tracker = null;
		this.maxX = 0;
		this.maxY = 0;
		this.panel = panel;
		this.panelId = panel.id;
		this.dashletCtrl = dashletCtrl;
	};

	_.extend( birstCoreRenderer.prototype, {

		parse : function( doc ) {
			this.panel.empty();
			this.xmlDoc = doc;
			this.render( this.xmlDoc );
		},

		render : function( doc, dontResize ) {
			console.log( 'render' );
			//TEMPOMIT: if (this.isVisualFilter()) {
			//	this.panel.on('afterrender', this.createDragSelectTracker, this);
			// }
			var parent = this.panel;
			var child, scaleChart = false;
			try {
				this.width = 0;
				this.height = 0;
				var numItems = 0;
				var numCharts = 0;
				child = doc.firstChild;
				console.log( child );
				while( child ) {
					this.calcMaxDims( child, 0, 0 );
					if (child.tagName ==='chart' || child.tagName ==='gauge') {
						numItems += 1;
						numCharts += 1;
					}
					else if (child.tagName ==='frame' ||
							child.tagName === 'text' ||
							child.tagName === 'html' ||
							child.tagName === 'adhocButton' ||
							child.tagName === 'image') {
						numItems += 1;
					}
					child = child.nextSibling;
				}

				if (numItems <= 3 && numCharts === 1) {
					scaleChart = true;
				}


				child = doc.firstChild;
				var lbl;
				while(child){
					if(child.tagName === 'text'){
						lbl = new chartModule.Views.ChartText( { model : new chartModule.Models.ChartTextModel( { xml : child, xOrigin : 0, yOrigin : 0, maxY : this.maxY, maxX : this.maxX } ) } );
						this.panel.append( $( '<div class="data-grid"></div>' ).append( lbl.el ) );
					}
					else if (child.tagName === 'html') {
						this.renderTextAsHTML(child, this.panel, 0, 0);
					}
					else if (child.tagName === 'frame') {
						var grandChild = child.firstChild;
						var xOrigin = child.getAttribute('x');
						var yOrigin = child.getAttribute('y');
						if (xOrigin) {
							xOrigin = parseInt(xOrigin, 10);
						} else {
							xOrigin = 0;
						}

						if (yOrigin) {
							yOrigin = parseInt(yOrigin, 10);
						} else {
							yOrigin = 0;
						}
						var label = null;
						var cellType = null;
						var childCounts = 0;
						while (grandChild) {
							if(grandChild.tagName === 'text'){
								lbl = new chartModule.Views.ChartText( { model : new chartModule.Models.ChartTextModel( { xml : child, xOrigin : 0, yOrigin : 0, maxY : this.maxY, maxX : this.maxX } ) } );
								this.panel.append( $( '<div class="data-grid"></div>' ).append( lbl.el ) );
								childCounts++;
							}
							else if (grandChild.tagName === 'html') {
								label = this.renderTextAsHTML(grandChild, this.panel, xOrigin, yOrigin);
								childCounts++;
							}
							else if (grandChild.tagName === 'adhocButton') {
								this.renderButton(grandChild, this.panel, xOrigin, yOrigin);
							}
							else if (grandChild.tagName === 'net.sf.jasperreports.crosstab.cell.type') {
								cellType = app.ws.getXmlValue(grandChild);
							}
							else if (grandChild.tagName === 'image') {
								this.renderImage(grandChild, this.panel, xOrigin, yOrigin);
							}
							grandChild = grandChild.nextSibling;
						}
						if (label && (cellType === 'Data' || cellType === 'RowHeader' || cellType === 'CrosstabHeader' || cellType === 'ColumnHeader')) {
							var textWidth = parseInt(child.getAttribute('width'), 10);
							var textHeight = Math.max(parseInt(child.getAttribute('height'), 10), label.height);
							if (childCounts > 1) {
								textWidth = label.width + (textWidth - ((label.x - xOrigin) + label.width));
							}

							if (cellType === 'CrosstabHeader') {
								textHeight = label.height;
							}
							else if (childCounts > 1) {
								textHeight = label.height + parseInt(child.getAttribute('height'), 10) - ((label.y - yOrigin) + label.height);
							}

							label.setWidth(Math.min(label.width, textWidth));
							label.setHeight(textHeight);
						}
					}
					else if ( child.tagName === 'adhocButton' ) {
						this.renderButton(child, this.panel, 0, 0);
					}
					else if ( child.tagName === 'image' ) {
						this.renderImage(child, this.panel, 0, 0);
					}
					child = child.nextSibling;
				}

			} catch (err) {
				console.log( err );
			}

			//this.panel.show();
			child = doc.firstChild;
			console.log( doc );
			while( child ) {
				if( child.tagName === 'chart' || child.tagName === 'gauge' ) {
					this.renderChart( child, 0, 0, this.panel, scaleChart );
				}
				else if (child.tagName === 'frame') {
					this.renderChartInFrame(child, 0, 0, this.panel);
				}
				child = child.nextSibling;
			}
			this.panel = parent;
			if (! dontResize ) {
				//this.panel.css( { width: this.maxX, height: this.maxY } );
			}
		},

		calcMaxDims: function( item, xOffset, yOffset ) {
			if ( item.tagName === 'frame') {
				var frameX = item.getAttribute( 'x' );
				var frameY = item.getAttribute( 'y' );
				if ( frameX ) {
					frameX = parseInt( frameX, 10 );
				} else {
					frameX = 0;
				}

				if ( frameY ) {
					frameY = parseInt( frameY, 10 );
				} else {
					frameY = 0;
				}

				var child = item.firstChild;

				while( child ){
					this.calcMaxDims( child, frameX + xOffset, frameY + yOffset );
					child = child.nextSibling;
				}
			} else {
				var x = item.getAttribute( 'x' );
				var y = item.getAttribute( 'y' );
				x = parseInt( x, 10 ) + xOffset;
				y = parseInt( y, 10 ) + yOffset;
				var width  = parseInt( item.getAttribute( 'width' ), 10 );
				var height = parseInt( item.getAttribute( 'height' ), 10 );
				if ( isFinite( width ) && isFinite( x ) ) {
					this.width = Math.max( this.width, width + x );
				}
				if ( isFinite( height ) && isFinite( y ) ) {
					this.height = Math.max( this.height, height + y );
				}
			}
		},

		renderChartInFrame: function(frameXml, xOrigin, yOrigin, surface) {
			var frameX = frameXml.getAttribute('x');
			var frameY = frameXml.getAttribute('y');
			if (frameX) {
				frameX = parseInt(frameX, 10);
			} else {
				frameX = 0;
			}

			if (frameY) {
				frameY = parseInt(frameY, 10);
			} else {
				frameY = 0;
			}

			var child = frameXml.firstChild;

			while(child){
				if(child.tagName === 'chart' || child.tagName === 'gauge'){
					this.renderChart(child, frameX + xOrigin, frameY + yOrigin, surface);
				}
				else if (child.tagName === 'frame') {
					this.renderChartInFrame(child, frameX + xOrigin, frameY + yOrigin, surface);
				}
				child = child.nextSibling;
			}
		},

		renderChart: function(chartXml, xOrigin, yOrigin, $surface, scale){
			var sprite;
			var chartTypeXml;
			var anychartXml = chartXml.children[0];
			if( anychartXml && anychartXml.tagName === 'anychart' ) {
				var chartWidth = chartXml.getAttribute( 'width' );
				var chartHeight = chartXml.getAttribute( 'height' );
				var x = parseInt( chartXml.getAttribute( 'x' ), 10 ) + xOrigin;
				var y = parseInt( chartXml.getAttribute( 'y' ), 10 ) + yOrigin;
				/*TEMPOMIT: if ( scale ) {
					var r = $surface.getRegion();
					if ( r.right != r.left && r.top != r.bottom ) {
					var w = $surface[0].offsetWidth - $surface[0].clientWidth;
					var h = $surface[0].offsetHeight - $surface[0].clientHeight;
					chartWidth = r.right - r.left - w - x - 15;
					chartHeight = r.bottom - r.top - h - y - 15;
					$surface.setAutoScroll( false );
					}
				}*/

				//Fixup for DASH-368. Client side html5 only for now. Could be server side if Flex is ok
				//with the additional parameter
				var series = chartXml.getElementsByTagName('series');
				for(var i = 0; series && i < series.length; i++){
					var se = series[i];
					se.setAttribute('missing_points', 'no_paint');
				}

				var chartType = 'gauge';
				if (chartXml.nodeName !== 'gauge') {
					chartTypeXml = chartXml.getElementsByTagName('chart');
					chartType = chartTypeXml[0].getAttribute('plot_type');
				}
				if (chartType === 'Scatter') {
					// do more to determine if its a bubble chart
					var seriesList = chartXml.getElementsByTagName('series');
					if (seriesList && seriesList.length > 0) {
						series = seriesList[0];
						var isBubble = false;
						while(series && !isBubble) {
							isBubble = (series.getAttribute('type') === 'Bubble');
							series = series.nextSibling;
						}

						if (isBubble) {
							chartType = 'Bubble';
						}
					}
				}
				if (chartType === 'Radar' || chartType === 'Map') {
					sprite = this.createChartContainer($surface, x, y, chartWidth, chartHeight);
					var label = Ext.create('Ext.form.Label', {
						text: Ext.String.format(Birst.LM.Locale.get('CHART_NOTSUPPORTED'),chartType),
						cls: 'chartError',
						y: '50%',
						anchor: '100%'
					});
					sprite.add(label);
				}
				else if(chartType === 'UMap'){
					sprite = this.createChartContainer( $surface, x, y, chartWidth, chartHeight );
					var bingMaps = Ext.create('Birst.dashboards.BingMaps', sprite.body.dom.id, chartTypeXml[0], this);
					bingMaps.render();
				} else {
					var chartData = app.ws.xml2Str( anychartXml );
					AnyChart.renderingType = anychart.RenderingType.SVG_ONLY;
					AnyChart.enabledChartMouseEvents = true;

					var chart = new AnyChart();
					chart.width =  $surface.innerWidth();
					chart.height = $surface.innerHeight();

					this.maxX = Math.max(this.maxX, chart.width + x);
					this.maxY = Math.max(this.maxY, chart.height + y);
					chart.setData(chartData);

					chart.addEventListener('pointClick', this.handleChartClick);
					//chart.addEventListener('pointSelect', this.handleChartClick);
					var obj = this;
					if ( this.isVisualFilter() ) {
						chart.addEventListener( 'multiplePointsSelect', function( e ) {
							obj.handleSelectionChange( e, chart );
						} );
					}
					chart.write( $surface.attr( 'id' ) );

					//We add the chart's location for each renderer so we can check if we are clicking
					//on a chart object and disable our dragtracker visual filtering which interferes with AnyChart
					//visual filtering.
					//TEMPOMIT: Birst.core.AnyChartTracker.add(chart.id, this,  { x: x, y: y, width: chartWidth, height: chartHeight} );

				}

			}
		},

		createChartContainer: function( $surface, x, y, chartWidth, chartHeight ){
			var $sprite = $( '<span/>' ).attr( 'id', _.uniqueId( 'sprite' ) ).css( { display : 'block', position: 'absolute', left : 0, top : 0, right: 0, bottom: 0 } );
			$surface.append( $sprite );
			return $sprite;
		},

		handleMultipleChartSelect: function(event) {

			// multiple items selected
			var e = event;
		},

		handleChartClick: function(event) {

			if (event && event.data && event.data.Attributes && event.data.Attributes.DrillType) {
				if (event.data.Attributes.DrillType === 'Drill To Dashboard') {
					var ctrlr = Dashboard.getApplication().getController('DashboardTabLists');
					if (ctrlr) {
						var dashParts = event.data.Attributes.targetURL.split(',');
						if (dashParts.length > 1) {
							ctrlr.navigateToDash(dashParts[0], dashParts[1], event.data.Attributes.filters);
						}
					}

				}
				else if (event.data.Attributes.DrillType === 'Drill Down') {
					var me = Birst.core.AnyChartTracker.getRenderer(event.target.id);
					Birst.core.Webservice.dashboardRequest('drillDown', [
							{ key : 'xmlReport', value : app.ws.xml2Str( me.dashletCtrl.adhocReportXml ) },
							{ key : 'drillCols', value : event.data.Attributes.drillBy },
							{ key: 'filters', value : event.data.Attributes.filters }
						],
						event,
						function(response, options) {
							var me = Birst.core.AnyChartTracker.getRenderer(this.target.id);
							var xml = response.responseXML;
							var xmlReportsArray = xml.getElementsByTagName('com.successmetricsinc.adhoc.AdhocReport');
							me.dashletCtrl.adhocReportXml = xmlReportsArray[0];
							var newPrompts = xml.getElementsByTagName('Prompts');
							if (newPrompts.length > 0) {
								me.dashletCtrl.promptsController.addPrompts(newPrompts[0]);
							}
							else {
								if (me.dashletCtrl.promptsController) {
									me.dashletCtrl.renderByReportXml(this.dashletCtrl.promptsController.getPromptsData());
								}
								else {
									me.dashletCtrl.renderByReportXml('<Prompts/>');
								}
							}
						});
				}

			}
		},

		filterPromptsResponse :  function( response, status, options ) {
			var newPrompts = response.getElementsByTagName('prompts');
			var numExistingPrompts = 0;
			var numNewPrompts = 0;

			if ( newPrompts.length > 0 ) {
				var values = options.additionalParam;
				var prompt = newPrompts[0].firstChild;
				while (prompt) {
					var column = app.ws.getXmlChildValue( prompt, 'ParameterName' );
					var promptControl = this.dashletCtrl.promptsController.promptControls.get( column );
					var value = values.get(column);

					var promptToDelete = null;
					if (value) {
						if(promptControl){
							promptControl.setSelectedValuesByArray(value);
							numExistingPrompts++;
							promptToDelete = prompt;
						}else{
							var selectedValues = prompt.getElementsByTagName('selectedValues');
							if (! selectedValues || selectedValues.length === 0) {
								selectedValues = prompt.ownerDocument.createElement('selectedValues');
								prompt.appendChild(selectedValues);
							}
							else {
								selectedValues = selectedValues[0];
							}

							var child = selectedValues.firstChild;
							while (child) {
								selectedValues.removeChild(child);
								child = selectedValues.firstChild;
							}

							var test = {};

							for (var i = 0; i < value.length; i++) {
								if ( test[value[i]] ){
									continue;
								}

								test[value[i]] = 1;
								var data = selectedValues.ownerDocument.createElement('selectedValue');
								data.appendChild(selectedValues.ownerDocument.createTextNode(value[i]));
								selectedValues.appendChild(data);
							}
							numNewPrompts++;
						}
					}

					prompt = prompt.nextSibling;
					if(promptToDelete){
						newPrompts[0].removeChild(promptToDelete);
					}
				}

				if(numNewPrompts > 0){
					this.dashletCtrl.promptsController.addPrompts(newPrompts[0]);
				}else{
					if(numExistingPrompts > 0){
						this.dashletCtrl.promptsController.applyPrompts();
					}
				}
			}
		},

		requestFilterPrompt : function( newPrompts ){
			if ( newPrompts && _.size( newPrompts ) > 0 ) {
				var keys = _.keys( newPrompts );
				var name = '';
				for ( var i = 0; i < keys.length; i++) {
					if (i > 0) {
						name = name + ',';
					}
					name = name + keys[i];
				}
				this.promptsResponse = Birst.webservice.getFilterPrompts( { key : 'column', value : name } );
				this.promptsResponse.done( _.bind( this.filterPromptsResponse, this ) );
			}
		},

		handleSelectionChange: function(e, chart){
			if (!e || !e.data || !e.data.points || !e.data.points.length) {
				return;
			}

			var pts = e.data.points;

			if (!pts.length) {
				return;
			}

			var newPrompts = {};//new Ext.util.HashMap();

			for(var i = 0; i < pts.length; i++){
				var point = pts[i];
				if(point.hasOwnProperty('Attributes')){
					var attr = point.Attributes;
					for(var y = 0; y < 10; y++){
						if(!attr.hasOwnProperty('column' + y)){
							break;
						}

						var dim = attr['dimension' + y];
						if( dim === 'EXPR' ) {
							continue;
						}

						var col = attr['column' + y];
						var name = dim + '.' + col;
						var value = attr['value' + y];
						var valArray;
						if( newPrompts.hasOwnProperty( name ) ) {
							valArray = newPrompts[name];
						} else {
							valArray = [];
							newPrompts[name] = valArray;
						}
						valArray.push( value );
					}
				}
			}

			this.requestFilterPrompt(newPrompts);
		},

		renderTextAsHTML: function( textXml, panel, xOrigin, yOrigin ) {

			var width = parseInt( textXml.getAttribute( 'width' ), 10 );
			var height = parseInt( textXml.getAttribute( 'height' ), 10 );
			var x = parseInt(textXml.getAttribute('x'), 10) + xOrigin;
			var y = parseInt(textXml.getAttribute('y'), 10) + yOrigin;
			var item = {
				html: Birst.core.Webservice.unescapeXml( textXml.getAttribute( 'value' ) ),
				width: width,
				height: height,
				x: x,
				y: y
			};
			var htmlFld = Ext.create('Ext.Component', item);
			panel.add(htmlFld);
			return htmlFld;
		},

		renderButton: function(textXml, panel, xOrigin, yOrigin) {

			var textFld = new txtModule.Models.Txt( textXml, xOrigin, yOrigin);

			this.maxX = Math.max(this.maxX, textFld.width + textFld.x);
			this.maxY = Math.max(this.maxY, textFld.height + textFld.y);

			var btn = Ext.create('Ext.button.Button', {
				text: textFld.text,
				width: textFld.width,
				height: textFld.height,
				x: textFld.x,
				y: textFld.y,
				style: {
					'font-family': textFld.fontName,
					'font-size': textFld.fontSize,
					'font-style': 'normal',
					'font-weight': textFld.fontWeight,
					'text-anchor': textFld.anchor
				}
			});
			btn.on('click', this.onClick, this, textXml);
			btn.on('afterrender', function(me, eOpts) {
				var id = me.getId();
				var spanId = id + '-btnInnerEl';
				var el = me.getEl();
				if (el) {
					var span = el.getById(spanId);
					if (span) {
						span.setStyle('font-family', eOpts.fontName);
						span.setStyle('font-size', eOpts.fontSize);
						span.setStyle('font-weight', eOpts.fontWeight);
						span.setStyle('color', eOpts.textColor);
					}
				}
			}, this, textFld);
			panel.add(btn);
		},

		renderImage:  function(textXml, panel, xOrigin, yOrigin) {

			var x = textXml.getAttribute('x');
			var y = textXml.getAttribute('y');
			x = parseInt(x, 10) + xOrigin;
			y = parseInt(y, 10) + yOrigin;
			var width  = textXml.getAttribute('width');
			var height = textXml.getAttribute('height');
			width = parseInt(width, 10);
			height = parseInt(height, 10);
			var ibackcolor = textXml.getAttribute('backcolor');
			var link = textXml.getAttribute('link');

			var properties = {
				x: x,
				y: y,
				width: width,
				height: height,
				src: '/SMIWeb/' + app.ws.getXmlValue(textXml),
				style: {
					'background-color': ibackcolor
				}
			};

			if (link && link !== '') {
				properties.cls = 'clickable';
			}
			var img = Ext.create('Ext.Img', properties);

			if (link && link !== '') {
				img.on('afterrender', function(me, eOpts) {
					var el = me.getEl();
					if (el) {
						el.on('click', this.onClick, this, textXml);
					}

				}, this, textXml);
			}

			panel.add(img);
		},

		onClick: function(e, t, eOpts) {

			var link = eOpts.getAttribute('link');
			if ( link.indexOf( 'javascript:AdhocContent.DrillThru(' ) === 0 ) { // jshint ignore:line
				// drill or navigate
				var parms = link.substring(link.indexOf('('), link.length);
				var paramArray = parms.split(',');
				for (var i = 0; i < paramArray.length; i++) {
					paramArray[i] = paramArray[i].trim();
				}
				if ( paramArray[1] === 'Drill Down' ) {
					// drill down
				// send this.xmlDoc, paramArray[2], paramArray[3]
					Birst.core.Webservice.dashboardRequest('drillDown',
						[
							{key: 'xmlReport', value: app.ws.xml2Str(this.dashletCtrl.adhocReportXml)},
							{key: 'drillCols', value: paramArray[3].slice(1, -1)},
							{key: 'filters', value: paramArray[2].slice(1, -1)}
						],
						this,
						function(response, options) {
							var xml = response.responseXML;
							var xmlReportsArray = xml.getElementsByTagName('com.successmetricsinc.adhoc.AdhocReport');
							this.dashletCtrl.adhocReportXml = xmlReportsArray[0];
							var newPrompts = xml.getElementsByTagName('Prompts');
							if (newPrompts.length > 0) {
								this.dashletCtrl.promptsController.addPrompts(newPrompts[0]);
							}
							else {
								if (this.dashletCtrl.promptsController) {
									this.dashletCtrl.renderByReportXml(this.dashletCtrl.promptsController.getPromptsData());
								}
								else {
									this.dashletCtrl.renderByReportXml('<Prompts/>');
								}
							}
						}
					);
				} else if ( paramArray[2] === 'Drill To Dashboard' ) {
					var ctrlr = Dashboard.getApplication().getController('DashboardTabLists');
					if (ctrlr) {
						ctrlr.navigateToDash(paramArray[0].slice(2, -1), paramArray[1].slice(1, -1), paramArray[3].slice(1, -1));
					}
				}

			}
			else if (link) {
				var dest = eOpts.getAttribute('target');
				if (!dest) {
					dest = '_blank';
				}
				window.open(link, dest);
			}
		},

		clearPanel: function(){
			console.log( this.panel );
			var cell = this.panel;
			if ( cell.hasChildNodes() ) {
				while ( cell.childNodes.length >= 1 ) {
					cell.removeChild( cell.firstChild );
				}
			}

			this.panel.removeAll();
		},

		renderInMaximizedPanel: function(maxPanel){

			var refPanel = this.panel;
			this.panel = maxPanel;
			this.render(this.xmlDoc, true);
			this.panel = refPanel;
		},

		cancelClick: function() {
			return !this.tracker.dragging;
		},

		createDragSelectTracker: function() {
			var thisRenderer = this;
			this.tracker = Ext.create('Ext.dd.DragTracker', {
				autoStart: false,
				delegate: this.panel.body,
				preventDefault: true,
				trackerPanel: null,
				renderer: this,
				panel: this.panel,

				listeners: {
					beforedragstart: function(that, e, eOpts) {
						var p = e.getXY();

						//This drag tracker interferes with chart visual filtering
						//if we point at any chart area, we do not run this drag tracker
						if( Birst.core.AnyChartTracker.pointIsInCharts(p[0], p[1], thisRenderer ) ) {
							return false;
						}

						//console.log('onBeforeStart: currentTarget is ' + e.currentTarget.id + ' panel is ' + this.panel.body.id);
						if ( that.dragTarget && that.dragTarget.id === that.panel.body.id ) {

							var r = that.el.getRegion();
							var w = that.dragTarget.offsetWidth - that.dragTarget.clientWidth;
							var h = that.dragTarget.offsetHeight - that.dragTarget.clientHeight;
							if ( p[0] < ( r.right-w ) && p[1] < ( r.bottom-h ) ) {
								return true;
							}
						}
						return false;
					}
				},

				onStart: function(e) {
					// Flag which controls whether the cancelClick method vetoes the processing of the DataView's containerclick event.
					// On IE (where else), this needs to remain set for a millisecond after mouseup because even though the mouse has
					// moved, the mouseup will still trigger a click event.
					this.dragging = true;

					this.trackerPanel = Ext.create('Ext.window.Window', {
						header: false,
						style: {
							background: '#ffcc',
							opacity: 0.6
						},
						//bodyCls: 'visual-selector',
						x: this.startXY[0],
						y: this.startXY[1],
						width: 1,
						height: 1,
						border: false,
						layout: 'absolute',
						resizable: false,
						minWidth: 1,
						minHeight: 1
					} ).show();
				},

				onDrag : function(e) {
					var minX = Math.min(e.xy[0], this.startXY[0]);
					var minY = Math.min(e.xy[1], this.startXY[1]);
					var maxX = Math.max(e.xy[0], this.startXY[0]);
					var maxY = Math.max(e.xy[1], this.startXY[1]);
					Ext.apply(this.dragRegion, {
						top: minY,
						left: minX,
						right: maxX,
						bottom: maxY
					});

					this.trackerPanel.setSize(maxX - minX, maxY - minY);
					this.trackerPanel.setPosition(minX, minY);
				},

				onEnd : window.setTimetout( _.bind( function(e) {
					this.dragging = false;
					this.trackerPanel.close();
					this.renderer.handleDragSelect(this.dragRegion, this.dragTarget.scrollTop, this.dragTarget.scrollLeft);
				}, this ), 1 )
			} );
			this.tracker.initEl(this.panel.body);
		},

		handleDragSelect: function(region, scrollTopOffset, scrollLeftOffset) {

			var i;
			// offset region by this.panel.body
			var offsetX = this.panel.body.getLeft(false) - scrollLeftOffset;
			var offsetY = this.panel.body.getTop(false) - scrollTopOffset;
			var minX = region.left - offsetX;
			var minY = region.top - offsetY;
			var maxX = region.right - offsetX;
			var maxY = region.bottom - offsetY;

			var elementArray = this.findIntersectingTextFields(minX, minY, maxX, maxY, this.xmlDoc);

			// find columns and getFilterPrompt
			var newPrompts = {};
			for (i = 0; i < elementArray.length; i++) {
				if ( app.ws.getXmlChildValue( elementArray[i], 'columnType' ) === 'Dimension' ) {
					var column = app.ws.getXmlChildValue(elementArray[i], 'dimension' ) + '.' + app.ws.getXmlChildValue(elementArray[i], 'column');
					var value = app.ws.getXmlChildValue(elementArray[i], 'valueToUse' );
					if (!value) {
						value = elementArray[i].getAttribute( 'value' );
					}
					var valArray = newPrompts.get( column );
					if ( ! valArray ) {
						valArray = [];
						newPrompts[column] = valArray;
					}
					valArray.push( value );
				}
			}

			this.requestFilterPrompt(newPrompts);
		},

		findIntersectingTextFields: function(minX, minY, maxX, maxY, parentDoc) {

			var elementArray = [];

			var child = parentDoc.firstChild;
			while(child){
				if(child.tagName === 'text'){
					var x = parseInt(child.getAttribute('x'), 10);
					var y = parseInt(child.getAttribute('y'), 10);
					var width  = parseInt(child.getAttribute('width'), 10);
					var height = parseInt(child.getAttribute('height'), 10);
					if (x <= maxX && y <= maxY && x + width > minX && y + height > minY) {
						elementArray.push(child);
					}
				}
				else if (child.tagName === 'frame') {
					var grandChild = child.firstChild;
					var xOrigin = child.getAttribute('x');
					var yOrigin = child.getAttribute('y');
					if (xOrigin) {
						xOrigin = parseInt(xOrigin, 10);
					} else {
						xOrigin = 0;
					}

					if (yOrigin) {
						yOrigin = parseInt(yOrigin, 10);
					} else {
						yOrigin = 0;
					}
					var childElements = this.findIntersectingTextFields(minX - xOrigin, minY - yOrigin, maxX - xOrigin, maxY - yOrigin, child);
					elementArray = elementArray.concat(childElements);
				}
				child = child.nextSibling;
			}

			return elementArray;
		},
		isVisualFilter: function(){
			console.log( this.dashletCtrl );
			var rectangleSelect = this.dashletCtrl.model.xmlDoc.getElementsByTagName('RectangleSelect');
			if(rectangleSelect && rectangleSelect.length > 0){
				return app.ws.getXmlValue(rectangleSelect[0]) === 'false' ? false : true;
			}
			return true;
		}
	} );

	module.exports = birstCoreRenderer;
} );
