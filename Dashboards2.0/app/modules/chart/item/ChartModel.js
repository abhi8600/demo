define( function( require, exports, module ) {
	'use strict';
	var app = require( 'app' );
	require( 'anychart' );
	require( 'anychartHTML5' );
	var AnyChart = window.AnyChart;
	var anychart = window.anychart;

	var ChartModel = module.exports = function( opts ) {
		var defaults = {
			xml : null,
			xOrigin : 0,
			yOrigin : 0,
			scale : false,
			height : 0,
			width : 0
		};

		_.extend( this, defaults, opts );
		this.parse();
	};

	ChartModel.prototype = {
		parse : function() {
			var sprite,
				chartTypeXml;

			this.anychartXml = this.xml.childNodes[0];
			if( this.anychartXml && this.anychartXml.tagName === 'anychart' ) {
				var chartWidth = this.xml.getAttribute( 'width' );
				var chartHeight = this.xml.getAttribute( 'height' );
				var x = parseInt( this.xml.getAttribute( 'x' ), 10 ) + this.xOrigin;
				var y = parseInt( this.xml.getAttribute( 'y' ), 10 ) + this.yOrigin;
				/*TEMPOMIT: if ( scale ) {
					var r = $surface.getRegion();
					if ( r.right != r.left && r.top != r.bottom ) {
					var w = $surface[0].offsetWidth - $surface[0].clientWidth;
					var h = $surface[0].offsetHeight - $surface[0].clientHeight;
					chartWidth = r.right - r.left - w - x - 15;
					chartHeight = r.bottom - r.top - h - y - 15;
					$surface.setAutoScroll( false );
					}
				}*/

				//Fixup for DASH-368. Client side html5 only for now. Could be server side if Flex is ok
				//with the additional parameter
				var series = this.xml.getElementsByTagName( 'series' );
				for( var i = 0, ln = series.length; series && i < ln; i++ ){
					var se = series[i];
					se.setAttribute( 'missing_points', 'no_paint' );
				}

				var chartType = 'gauge';
				if ( this.xml.nodeName !== 'gauge' ) {
					chartTypeXml = this.xml.getElementsByTagName( 'chart' );
					chartType = chartTypeXml[0].getAttribute( 'plot_type' );
				}
				if ( chartType === 'Scatter' ) {
					// do more to determine if its a bubble chart
					var seriesList = this.xml.getElementsByTagName( 'series' );
					if ( seriesList && seriesList.length > 0 ) {
						series = seriesList[0];
						var isBubble = false;
						while( series && !isBubble ) {
							isBubble = ( series.getAttribute( 'type' ) === 'Bubble' );
							series = series.nextSibling;
						}

						if ( isBubble ) {
							chartType = 'Bubble';
						}
					}
				}
				if ( chartType === 'Radar' || chartType === 'Map' ) {
					/*sprite = this.createChartContainer($surface, x, y, chartWidth, chartHeight);
					var label = Ext.create('Ext.form.Label', {
						text: Ext.String.format(Birst.LM.Locale.get('CHART_NOTSUPPORTED'),chartType),
						cls: 'chartError',
						y: '50%',
						anchor: '100%'
					});
					sprite.add(label);*/
				}
				else if(chartType === 'UMap'){
					/*sprite = this.createChartContainer( $surface, x, y, chartWidth, chartHeight );
					var bingMaps = Ext.create('Birst.dashboards.BingMaps', sprite.body.dom.id, chartTypeXml[0], this);
					bingMaps.render();*/
				} else {
					this.anyChart ={};
					this.anyChart.chartData = app.ws.xml2Str( this.anychartXml );
					this.anyChart.renderingType = anychart.RenderingType.SVG_ONLY;
					this.anyChart.enabledChartMouseEvents = true;
					this.anyChart.width = this.width;
					this.anyChart.height = this.height;
					this.maxX = Math.max( this.maxX, this.anyChart.width + x );
					this.maxY = Math.max( this.maxY, this.anyChart.height + y );

					// chart.width =  $surface.innerWidth();
					// chart.height = $surface.innerHeight();

					//chart.setData(chartData);

					//chart.addEventListener('pointClick', this.handleChartClick);
					//chart.addEventListener('pointSelect', this.handleChartClick);
					//var obj = this;
					/*if ( this.isVisualFilter() ) {
						chart.addEventListener( 'multiplePointsSelect', function( e ) {
							obj.handleSelectionChange( e, chart );
						} );
					}
					chart.write( $surface.attr( 'id' ) );*/

					//We add the chart's location for each renderer so we can check if we are clicking
					//on a chart object and disable our dragtracker visual filtering which interferes with AnyChart
					//visual filtering.
					//TEMPOMIT: Birst.core.AnyChartTracker.add(chart.id, this,  { x: x, y: y, width: chartWidth, height: chartHeight} );

				}

			}
		}
	};

} );
