define( function( require, exports, module ) {
	'use strict';
	var birst = require( 'birst' );
	var app = require( 'app' );
	var ldsh = require( 'ldsh' );
	var rivets = require( 'rivets' );
	var reportModule = require( 'modules/report/index' );
	var adhocSvc = reportModule.Services.AdhocReport;
	require( 'anychart' );
	require( 'anychartHTML5' );
	var AnyChart = window.AnyChart;
	var anychart = window.anychart;
	//require( 'modules/chart/item/pager' );
	//var Ext = window.Ext;
	var ChartView = module.exports = birst.View.extend( {

		autoRender : false,

		tagName : 'figure',

		events : {
			'resize window' : 'resizeChart'
		},

		initialize: function( opts ) {
			this.drawTimeout = null;
			this.module = require( 'modules/chart/index' );
			this.$el.attr( 'id', this.cid );
			this.dashlet = opts.dashlet;
			this.adhocReportXml = opts.adhocReportXml;
			this.charts = [];
			//this.setupPager();
		},

		setupPager : function() {
			this.pager = this.pager || Ext.create('Birst.dashlet.Pager', this);
			var xmlReportRenderer = this.model.xmlDoc.getElementsByTagName( 'reportRenderer' );
			if( xmlReportRenderer && xmlReportRenderer.length > 0 ) {
				var numPages = app.ws.getXmlChildValue( xmlReportRenderer[0], 'numberOfPages' );
				var currentPage = app.ws.getXmlChildValue( xmlReportRenderer[0], 'page' );
				try {
					numPages = parseInt(numPages, 10);
				}
				catch (err) {
					numPages = 1;
				}
				this.pager.setCount(numPages);

				try {
					currentPage = parseInt(currentPage, 10);
				}
				catch (err) {
					currentPage = 0;
				}
				this.pager.setCurrentPage(currentPage);

				this.pager.onLoad();
			}
		},

		resizeChart : function(){
			var width = this.$el.innerWidth();
			var height = this.$el.innerHeight();
			_.each( this.charts, function( chart ) {
				chart.resize( width, height );
			}, this );
		},

		renderFunction : function() {
			this.$el.empty();
			if( this.collection.itemCollection.length > 0 ) {
				var $dataGrid = $( '<div class="data-grid"></div>' );
				this.$el.append( $dataGrid );
				_.each( this.collection.itemCollection, function( item ) {
					var view = new this.module.Views[ item._dataType ]( { model : item } );
					$dataGrid.append( new this.module.Views[ item._dataType ]( { model : item } ).el );
				}, this );
			}
			_.each( this.collection.chartCollection, function( chartOpts ) {
				_.extend( chartOpts, { width : this.$el.innerWidth(), height : this.$el.innerHeight() } );
				var cModel = new this.module.Models.ChartModel( chartOpts );
				AnyChart.renderingType = anychart.RenderingType.SVG_ONLY;
				AnyChart.enabledChartMouseEvents = true;
				var chart = new AnyChart();
				this.charts.push( chart );
				chart.width = cModel.anyChart.width;
				chart.height = cModel.anyChart.height;
				chart.resizeMode = 'Stretch';
				chart.setData( cModel.anyChart.chartData );
				chart.write( this.el );

					/*TEMPOMIT:  chart.addEventListener('pointClick', this.handleChartClick);
					chart.addEventListener('pointSelect', this.handleChartClick);
					var obj = this;
					if ( this.isVisualFilter() ) {
						chart.addEventListener( 'multiplePointsSelect', function( e ) {
							obj.handleSelectionChange( e, chart );
						} );
					}

					We add the chart's location for each renderer so we can check if we are clicking
					on a chart object and disable our dragtracker visual filtering which interferes with AnyChart
					visual filtering.
					Birst.core.AnyChartTracker.add(chart.id, this,  { x: x, y: y, width: chartWidth, height: chartHeight} );*/
			}, this );
		}
	} );

} );
