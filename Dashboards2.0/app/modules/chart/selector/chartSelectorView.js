define( function( require, exports, module ) {
	'use strict';
	var birst = require( 'birst' );
	var app = require( 'app' );
	var formModule = require( 'modules/form/index' );

	var SelectorPanel = module.exports = birst.View.extend( {

		className : 'chartSelector',

		signals : [],

		initialize : function( opts ) {
			this.$fieldset = $( '<fieldset/>' );
			this.$el.append( this.$fieldset );
			this.dashlet = opts.dashlet;
			this.signals.push( app.editMode.add( this.editModeToggle, this ) );
		},

		editModeToggle : function( on ) {
			if( on ) {
				this.$fieldset.prop( 'disabled', true );
			} else {
				this.$fieldset.prop( 'disabled', false );
			}
		},

		renderFunction : function() {
			var chartType, chartTypeCached, index;
			if ( !this.cachedSelectorsXml ){
				this.cachedSelectorsXml = this.model;
			}

			if (  this.dashlet.model.dashletControlExpanded ) {
				this.model = this.selectorsLastState ? app.webservice.Str2Xml( this.selectorsLastState ).getElementsByTagName( 'Selectors' ) : this.model;
			}

			var includeChartSelector = app.ws.getXmlChildValue( this.model[0], 'IncludeChartSelector' );
			if (  this.dashlet.model.EnableViewSelector && includeChartSelector === 'true' ) {

				if (  this.dashlet.model.dashletControlExpanded ) {
					this.$el.empty();
				}

				chartType = app.ws.getXmlChildValue( this.model[0], 'ChartType' );

				var value = chartType;

				if (  this.dashlet.model.dashletControlExpanded ) {
					if ( !chartType && this.cachedSelectorsXml ) {
						// determine type based on cached data and resolve conflicts
						// in maximized dashlet mode resolve the chartType null case - group of chart types to show in the dropdown - the "default"
						// or "chart" ones (composite type or any other than default).
						chartTypeCached =  app.ws.getXmlChildValue(this.cachedSelectorsXml[0], 'ChartType');
						// if in an original report the chart type wasn't one of the following then we show the chart group or otherwsie we show the default group
						if ( chartTypeCached !== 'Pie' && chartTypeCached !== 'Bar' && chartTypeCached !== 'Column' && chartTypeCached !== 'Line' ) {
							chartType = 'chart';
						}else{
							chartType = 'default';
						}
						value = 'table'; //a null chartType indicates we are in the table view so we will set the selection in the dropdown to table
					}
				}

				var viewSelectorTypes = [];
				if ( chartType === 'Pie' || chartType === 'Bar' || chartType === 'Column' || chartType === 'Line' || chartType === 'default' ) {
					viewSelectorTypes.push( { label : app.locale.get( 'CHART_PIE' ), value : 'Pie' } );
					viewSelectorTypes.push( { label : app.locale.get( 'CHART_BAR' ), value : 'Bar' } );
					viewSelectorTypes.push( { label : app.locale.get( 'CHART_COL' ), value : 'Column' } );
					viewSelectorTypes.push( { label : app.locale.get( 'CHART_LINE' ), value : 'Line' } );
				} else {
					if ( chartTypeCached === undefined ) {
						// composite or any other type not in the default group
						if ( !chartType ) {
							value = 'table';
						} else {
							value = 'chart';
						}
					}

					viewSelectorTypes.push( { label : app.locale.get( 'CHART_CHART' ), value : 'chart' } );
				}
				viewSelectorTypes.push( { label : app.locale.get( 'CHART_TABLE' ), value : 'table' } );

				this.viewSelector = new formModule.Views.ComboBox( { model : viewSelectorTypes, selected : value } );

				this.$fieldset.append( this.viewSelector.el );

				// this.viewSelector.on( 'select', function( combo, records, eOpts ) {
				//		this.renderByReportXml( this.getPromptData() );
				// }, this);
			}

			var children = this.model[0].children;

			_.forEach( children, function( child ) {
				if ( child.tagName === 'ColumnSelectors' &&  this.dashlet.model.EnableSelectorsCtrl ) {
					this.columnSelectors = this.columnSelectors || {};
					var colSelectorData = [];
					var selected = app.ws.getXmlChildValue( child, 'Selected' );
					var grandChild = child.firstChild;
					var colSelectorName = app.ws.getXmlChildValue( child, 'Column' );

					while( grandChild ) {
						if ( grandChild.tagName === 'ColumnSelector' ) {
							var label = app.ws.getXmlChildValue( grandChild, 'Label' );
							var dim = app.ws.getXmlChildValue( grandChild, 'Dimension' );
							var col = app.ws.getXmlChildValue( grandChild, 'Column' );

							colSelectorData.push( { label : label, value : ( dim ? dim + '.' + col : col ) } );
						}

						grandChild = grandChild.nextSibling;
					}
					var ComboBox = new formModule.Views.ComboBox( { model : colSelectorData, selected : selected } );
					this.columnSelectors[ app.ws.xml2Str( child ) ] = ComboBox;

					this.$fieldset.append( ComboBox.el );


					/*colSelectorCombo.on('select', function(combo, records, eOpts) {
						this.renderByReportXml(this.getPromptData());
					}, this);*/
				}
			}, this );

			if ( this.dashlet.model.dashletControlExpanded) {
				delete  this.dashlet.model.dashletControlExpanded;
			}
		}

	} );

} );
