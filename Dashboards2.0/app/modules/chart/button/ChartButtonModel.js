define( function( require, exports, module ) {
	'use strict';
	var ChartTextModel = require( 'modules/chart/text/ChartTextModel' );
	var _ = require( 'lodash' );
	var ButtonModel = module.exports = function( opts ) {

		var defaults = {
			xml : null,
			xOrigin : 0,
			yOrigin : 0,
			fontFamily : null,
			fontSize : null,
			fontStyle : null,
			fontWeight : null
		};

		_.extend( this, defaults, opts );

		this.parse();

	};

	ButtonModel.prototype = _.extend( {}, ChartTextModel.prototype, { _dataType : 'ChartButton' } );

} );

/*

renderButton: function(textXml, panel, xOrigin, yOrigin) {

	var textFld = new txtModule.Models.Txt( textXml, xOrigin, yOrigin);

	this.maxX = Math.max(this.maxX, textFld.width + textFld.x);
	this.maxY = Math.max(this.maxY, textFld.height + textFld.y);

	var btn = Ext.create('Ext.button.Button', {
		text: textFld.text,
		width: textFld.width,
		height: textFld.height,
		x: textFld.x,
		y: textFld.y,
		style: {
			'font-family': textFld.fontName,
			'font-size': textFld.fontSize,
			'font-style': 'normal',
			'font-weight': textFld.fontWeight,
			'text-anchor': textFld.anchor
		}
	});
	btn.on('click', this.onClick, this, textXml);
	btn.on('afterrender', function(me, eOpts) {
		var id = me.getId();
		var spanId = id + '-btnInnerEl';
		var el = me.getEl();
		if (el) {
			var span = el.getById(spanId);
			if (span) {
				span.setStyle('font-family', eOpts.fontName);
				span.setStyle('font-size', eOpts.fontSize);
				span.setStyle('font-weight', eOpts.fontWeight);
				span.setStyle('color', eOpts.textColor);
			}
		}
	}, this, textFld);
	panel.add(btn);
},

*/
