define( function( require, exports, module ) {
	'use strict';
	var chartModule = module.exports = {
		Views : {
			Chart : require( './item/ChartView' ),
			ChartText : require( './text/ChartTextView' ),
			SelectorPanel : require( './selector/chartSelectorView' )
		},
		Models : {
			ChartTextModel : require( './text/ChartTextModel' ),
			ChartHtmlModel : require( './html/ChartHtmlModel' ),
			ChartButtonModel : require( './button/ChartButtonModel' ),
			ChartImageModel : require( './image/ChartImageModel' ),
			ChartModel : require( './item/ChartModel' )
		},
		Collections : {
			ChartCollection : require( './item/ChartCollection' )
		},
		Plugins : {
			Renderer : require( './item/Birst.core.Renderer' )
		}
	};

} );
