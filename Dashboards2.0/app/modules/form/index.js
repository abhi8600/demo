define( function( require, exports, module ) {
	'use strict';
	module.exports = {
		Views : {
			ComboBox : require( './combobox/ComboBoxView' )
		}
	};
} );
