define( function( require, exports, module ) {
	'use strict';
	var birst = require( 'birst' );
	var ldsh = require( 'ldsh' );
	var rivets = require( 'rivets' );

	var ComboBox = module.exports = birst.View.extend( {

		template : require( 'ldsh!./comboBox' ),

		initialize : function() {
			this.$el.attr( 'id', '#' + this.cid );
			this.bindRivets();
		},

		bindRivets : function() {
			this.rv = rivets.bind( this.el, {
				opts : this.model,
				selected : this.selected
			} );
		}

	} );

} );
