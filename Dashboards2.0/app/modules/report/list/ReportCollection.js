define( function( require, exports, module ) {
	'use strict';
	var app = require( 'app' );
	var ReportModel = require( 'modules/report/list/ReportModel' );

	var ReportCollection = module.exports = function( xmlDoc ) {
		this.xmlDoc = xmlDoc;
		this.elements = [];
		this.parse();
	};

	ReportCollection.prototype = {

		parse : function() {
			_.each( this.xmlDoc.querySelectorAll( 'FileNode' ), function( fileNode ) {
				if ( isReportXml( fileNode ) ) {
					this.elements.push( new ReportModel( fileNode ) );
				}
			}, this );
		}
	};

	// private helper functions
	var isReportXml = function(node) {
		return app.ws.getXmlChildValue( node, 'name' ).indexOf( '.AdhocReport' ) > -1;
	};
} );
