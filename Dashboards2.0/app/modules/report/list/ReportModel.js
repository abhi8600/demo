define( function( require, exports, module ) {
	'use strict';
	var app = require( 'app' );

	var ReportModel = module.exports = function( xml ) {
		this.parse( xml );
	};

	ReportModel.prototype = {
		parse : function( xml ) {
			_.each( xml.children, function( childNode ) {
				this[childNode.tagName] = app.ws.getXmlValue( childNode );
			}, this );
			this.meta = "Last modified: " + this.modifiedDate;
		}
	};

} );
