define( function( require, exports, module ) {
	'use strict';
	var app = require( 'app' );
	var Birst = require( 'birst' );
	var ldsh = require( 'ldsh' );
	var rivets = require( 'rivets' );
	var dragImage = document.createElement( 'div' );
	var ReportCollection = require( 'modules/report/list/ReportCollection' );
	dragImage.className = 'dashlet-box';
	dragImage.id = 'dashletBox';
	$( 'body' ).append( dragImage );

	var ReportListView = Birst.View.extend( {

		template : require( 'ldsh!./template' ),

		useTemplateEl : false,

		url : 'reports',

		model : { reports : [] },

		events : {
			'dragstart a' : 'handleDragstart',
			'drag a' : 'handleDrag',
			'dragend a' : 'handleDragend'
		},

		initialize : function() {
			var that = this;
			app.ws.getFileMethod( 'getVisibleFilesystem', [{ key : 'writeable', value : 'false' }] ).done( function( xml ) {
				var reportCollection = new ReportCollection( xml );
				that.model.reports = reportCollection.elements;
			} );
		},

		afterRender : function() {
			this.rv = rivets.bind( this.el, {
				reports : this.model
			} );
		},

		handleDragstart : function( e ) {
			var pageReport = this.model.reports[ this.$el.find( 'a' ).index( e.target ) ];
			// console.log( 'dragstart' );
			// Different for Zepto/DOM vs jQuery
			var dataTransfer = e.dataTransfer || e.originalEvent.dataTransfer;
			dataTransfer.setData( 'application/json', JSON.stringify( pageReport ) );
			/*if( this.supportsDragImage ) {
				dt.setDragImage( dragImage, 0, 0 );
			} else {
				//TODO: Handle no drag image
				console.log( 'no drag image' );
			}*/
		},
		handleDrag : function( e ) {
			// console.log( 'drag' );
		},

		handleDragend : function( e ) {
			// console.log( 'dragend' );
		},

		noop : function( e ) {
			e.preventDefault();
		}

	} );

	module.exports = ReportListView;

} );
