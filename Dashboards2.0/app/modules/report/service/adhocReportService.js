define( function( require, exports, module ) {
	'use strict';
	module.exports = {

		getAdhocReportXml : function( adhocXml ) {
			_.each( adhocXml, function( el, idx ) {
				var createdBy = el.getAttribute( 'createdBy' );
				if ( !_.isEmpty( createdBy ) ) {
					this.adhocReportXml = el;
					return;
				}
				if( idx === adhocXml.length ) {
					this.adhocReportXml = adhocXml[0];
				}
			}, this );
		}

	};

} );
