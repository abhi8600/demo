define( function( require, exports, module ) {
	'use strict';

	var reportModule = module.exports = {
		Views : {
			List : require( './list/view' )
		},
		Services : {
			AdhocReport : require( './service/adhocReportService' )
		}
	};
} );
