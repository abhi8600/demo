define( function( require, exports, module ) {
	'use strict';
	var _ = require( 'lodash' );
	
	module.exports = function( opts ) {
		var defaults = {
			EnableExcel : false,
			EnableAdhoc : false,
			EnableSelfSchedule : false,
			EnableCSV : false,
			EnableViewSelector : true,
			Title : "Hello world",
			ApplyAllPrompts : true,
			Path : "private/sfoobirst.com/Bug 13181.AdhocReport",
			ignorePromptList : null,
			EnableSelectorsCtrl : true,
			EnablePPT : false,
			EnablePivotCtrl : false,
			EnablePDF : false
		};
		defaults.Path = opts.name;
		defaults.Title = opts.label;
		_.extend( this, defaults );
	};
} );
