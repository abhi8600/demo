define( function( require, exports, module ) {
	'use strict';
	var chartModule = require( 'modules/chart/index' );
	var Birst = require( 'birst' );
	var app = require( 'app' );
	var ldsh = require( 'ldsh' );
	var rivets = require( 'rivets' );
	var reportModule = require( 'modules/report/index' );
	var adhocSvc = reportModule.Services.AdhocReport;

	var DashletItemView = module.exports = Birst.View.extend( {

		template : require( 'ldsh!./template' ),

		events : {
			'click .btn-remove-dashlet' : 'removeDashlet'
		},

		signals : [],

		initialize : function( opts ) {
			var wsParams = [{
				key : 'name',
				value : this.model.Path
			},
			{
				key : 'prompts',
				value : '<Prompts></Prompts>'
			},
			{
				key : 'page',
				value : '0'
			},
			{
				key : 'applyAllPrompts',
				value : 'true'
			},
			{
				key : 'pagePath',
				value : 'NO_LOG'
			},
			{
				key : 'guid',
				value : '0_NO_LOG'
			},
			{
				key : 'isDashletEdit',
				value : 'true'
			}];
			app.ws.getAdhocMethod( 'getDashboardRenderedReportV2', wsParams).done( _.bind( this.onResponseRender, this ) );
			this.rv = this.bindRivets();
			this.signals.push( app.editMode.add( this.chartModes, this ) );
			this.container = opts.container;
		},

		chartModes : function( on ) {
			if( on ) {
				this.enableChartEditMode();
			} else {
				this.disableChartEditMode();
			}
		},

		disableChartEditMode : function() {
			//console.log( 'disabled' );
		},

		enableChartEditMode : function() {
			//console.log( 'enabled' );
		},

		bindRivets : function() {
			return rivets.bind( this.el, { dashlet : this.model } );
		},

		redrawChart : function() {
			this.chart.resizeChart();
		},

		getAdhocReportXml : function( xmlDoc ) {
			var adhocXml = xmlDoc.getElementsByTagName( 'com.successmetricsinc.adhoc.AdhocReport' );
			return adhocSvc.getAdhocReportXml( adhocXml );
		},

		onResponseRender : function( xmlDoc, status, options ) {
			var selectors = xmlDoc.getElementsByTagName( 'Selectors' );
			if( ( selectors.length > 0 && ( this.model.EnableViewSelector || this.model.EnableSelectorsCtrl ) ) || ( this.dashletControlExpanded && this.model.EnableViewSelector ) ) {
				this.selectorPanel = new chartModule.Views.SelectorPanel( { model : selectors, dashlet : this } );
				this.$el.append( this.selectorPanel.el );
			}
			var adhocReportXml = this.getAdhocReportXml( xmlDoc );
			var xmlReportsArray = xmlDoc.getElementsByTagName('report');
			var chartCollection = new chartModule.Collections.ChartCollection( xmlReportsArray[0] );
			this.chart = new chartModule.Views.Chart( { adhocReportXml : adhocReportXml, collection : chartCollection, dashlet : this } );
			this.$el.append( this.chart.el );
			this.chart.render();
		},

		removeDashlet : function( e ) {
			e.preventDefault();
			//in reality we will need to also affect the model
			this.container.removeItem();
		}
	} );

} );
