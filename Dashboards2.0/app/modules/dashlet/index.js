define( function( require, exports, module ) {
	'use strict';

	var dashletModule = module.exports = {
		Models : {
			Item : require( './item/DashletItemModel')
		},
		Views : {
			Item : require( './item/view' )
		}
	};
} );
