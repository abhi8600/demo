define( function( require, exports, module ) {
	'use strict';
	var Birst = require( 'birst' );
	var ldsh = require( 'ldsh' );
	var rivets = require( 'rivets' );

	var PromptItemView = Birst.View.extend( {

		template : require( 'ldsh!./template' ),

		initialize: function() {
			this.rv = this.bindRivets();
		},

		bindRivets : function() {
			return rivets.bind( this.el, { prompt : this.model } );
		}
	} );

	module.exports = PromptItemView;

} );
