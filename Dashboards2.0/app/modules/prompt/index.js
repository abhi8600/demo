define( function( require, exports, module ) {
	'use strict';

	var promptModule = module.exports = {
		Views : {
			Item : require( './item/view' ),
			List : require( './list/view' )
		}
	};

} );
