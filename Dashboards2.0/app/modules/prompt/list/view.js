define( function( require, exports, module ) {
	'use strict';
	var Birst = require( 'birst' );
	var ldsh = require( 'ldsh' );
	var rivets = require( 'rivets' );

	var PromptListView = Birst.View.extend( {

		el : 'div',

		initialize : function() {
		},

		afterRender : function() {
		}

	} );

	module.exports = PromptListView;

} );
