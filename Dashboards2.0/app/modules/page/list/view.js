define( function( require, exports, module ) {
	'use strict';
	var Birst = require( 'birst' );
	var ldsh = require( 'ldsh' );
	var rivets = require( 'rivets' );
	var app = require( 'app' );
	var PageItemModel = require( 'modules/page/item/PageItemModel' );
	var PageItemView = require( 'modules/page/item/view' );
	var DashletItemModel = require ('modules/dashlet/item/DashletItemModel');
	var promptModule = require( 'modules/prompt/index' );

	var GRID_SIZE_PERCENT = 2;

	var PageListView = Birst.View.extend( {

		childViews : [],

		events: {
			'dragenter' : 'handleDragenter',
			'dragover' : 'handleDragover',
			'dragleave' : 'handleDragleave',
			'drop' : 'handleDrop',
			'bringtofront' : 'zindex'
		},

		url : 'pages',

		signals : [],

		template : require( 'ldsh!./template' ),

		initialize: function() {
			this.deferred = app.ws.get( this.url );
			this.signals.push( app.editMode.add( this.toggleEditMode, this ) );
		},

		toggleEditMode : function( on ) {
			if( on ) {
				this.el.classList.add( 'editMode' );
			}
			if( !on ) {
				this.el.classList.remove( 'editMode' );
			}
		},

		bindRivets : function() {
			return rivets.bind( this.el, { dashboard : this.model } );
		},

		renderFunction : function() {
			this.$el.html( this.template() );
			this.deferred.done( _.bind( function( data ) {
				this.model = data;
				this.model.cid = this.cid;
				this.rv = this.bindRivets();
				new promptModule.Views.List( { model : this.model.Prompts } );
			}, this ) );
		},

		zindex : function( e ) {
			var ln = this.childViews.length;
			var id = e.target.id;
			var $el = $( e.target );
			var idx = $el.css( 'zIndex' );
			$el.css( 'zIndex', ln );
			_.each( this.childViews, function( view, i ) {
				var viewIdx = view.$el.css( 'zIndex' );
				if( view.cid !== id && viewIdx > idx ) {
					view.$el.css( 'zIndex', viewIdx - 1 );
				}
			}, this );
		},

		afterRender: function() {
			rivets.binders.dashboard = function( el, value ) {
				var $el = $( el );
				$el.attr( 'id', value.cid );
			};
		},

		handleDragenter : function( e ) {
			$( this ).addClass( 'over' );
		},

		handleDragover : function( e ) {
			e.preventDefault(); //Allow Drop
			return false;
		},

		handleDragleave : function( e ) {
			$( this ).removeClass( 'over' );
		},

		handleDrop : function( e ) {
			e.preventDefault();
			var dataTransfer = e.dataTransfer || e.originalEvent.dataTransfer,
				reportData = dataTransfer.getData( 'application/json' );

			if( reportData.indexOf( '{' ) === 0 ) {
				var report = JSON.parse( reportData );
				var itemModel = new PageItemModel( {
					dashlet : new DashletItemModel( report )
				} );
				var initialItemOffset = this.getInitialItemOffset( e, itemModel );
				itemModel.percentTop = initialItemOffset.top;
				itemModel.percentLeft = initialItemOffset.left;

				var view = new PageItemView( {
					model : itemModel,
					parentView : this
				} );

				view.$el.css( 'zIndex', this.childViews.length + 1 );
				this.$el.append( view.$el );
				view.render();
				this.childViews.push( view );
			}
		},

		getInitialItemOffset : function( e, item ) {
			var canvasOffset = this.$el.offset();
			var xPosOnCanvas = e.originalEvent.pageX - canvasOffset.left;
			var yPosOnCanvas = e.originalEvent.pageY - canvasOffset.top + this.$el.scrollTop();
			var offset = {
				left : ( xPosOnCanvas / this.$el.width() ) * 100 - item.percentWidth / 2,
				top : ( yPosOnCanvas / this.$el.height() ) * 100 - item.percentHeight / 2
			};
			offset = boundedOffset( offset, item );
			offset = gridAlignedOffset( offset );
			return offset;
		},

		correctedOffset : function( offset, item ) {
			var boundOffset = boundedOffset( offset, item );
			// console.log( "boundOffset", JSON.stringify( boundOffset ) );
			var gridOffset = gridAlignedOffset( boundOffset );
			// console.log( "gridOffset", JSON.stringify( gridOffset ) );
			return gridOffset;
		},

		correctedDimensions : function( dimensions, item ) {
			var boundDimensions = boundedDimensions( dimensions, item);
			// console.log("boundDimensions:", boundDimensions);
			var gridDimensions = gridAlignedDimensions( boundDimensions, item );
			// console.log("gridDimensions:", gridDimensions);
			return gridDimensions;
		},

		convertOffsetToPercent : function( offset ) {
			return {
				left : ( offset.left / this.$el.width() ) * 100,
				top : ( offset.top / this.$el.height() ) * 100
			};
		}
	} );

	module.exports = PageListView;

	// Helper function to ensure dashlet's offset doesn't allow it to overflow in the
	// left, top, and right edges of the dashlets container.
	var boundedOffset = function( offset, item ) {
		return {
			left : boundedValue( offset.left, 0, 100, item.percentWidth ),
			top : boundedValue( offset.top, 0, Infinity )
		};
	};

	var boundedDimensions = function( dimensions, item ) {
		return {
			height : boundedValue( dimensions.height, 0, Infinity ),
			width : boundedValue( dimensions.width, 0, 100, item.percentLeft )
		};
	};

	var boundedValue = function( value, lowerBound, upperBound, upperBoundMargin ) {
		upperBoundMargin = upperBoundMargin || 0;
		if ( value < lowerBound ) {
			value = lowerBound;
		} else if ( value  > upperBound - upperBoundMargin ) {
			value = upperBound - upperBoundMargin;
		}
		return value;
	};

	var gridAlignedOffset = function( offset ) {
		return {
			left : snapToGrid( offset.left ),
			top : snapToGrid( offset.top )
		};
	};

	var gridAlignedDimensions = function( dimensions, item ) {
		return {
			height : snapToGrid( dimensions.height + item.percentTop ) - item.percentTop,
			width : snapToGrid( dimensions.width + item.percentLeft ) - item.percentLeft
		};
	};

	var snapToGrid = function( val ) {
		return GRID_SIZE_PERCENT * Math.round( val / GRID_SIZE_PERCENT );
	};

} );
