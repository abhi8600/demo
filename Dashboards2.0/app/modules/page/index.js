define( function( require, exports, module ) {
	'use strict';
	var pageModule = module.exports = {
		Views : {
			List : require( './list/view' ),
			Item : require( './item/view' )
		}
	};
} );
