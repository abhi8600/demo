define( function( require, exports, module ) {
	'use strict';
	var Birst = require( 'birst' );
	var ldsh = require( 'ldsh' );
	var rivets = require( 'rivets' );
	var dashletModule = require( 'modules/dashlet/index' );
	var promptModule = require( 'modules/prompt/index' );

	var PERCENT = '%';

	var PageListItem = module.exports = Birst.View.extend( {

		autoRender : false,

		template : require( 'ldsh!./template' ),

		events : {
			'mousedown :not(.resize-handle,.btn-remove-dashlet)' : 'handleDragstart',
			'mouseup' : 'handleDragend',
			'afterRender' : 'initResizeable',
			'mousedown .resize-handle' : 'handleResizeStart'
		},

		initialize: function( opts ) {
			this.model.cid = this.cid;
			this.el.id = this.cid;
			this.parentView = opts.parentView;
		},

		initResizeable : function( e ) {
			this.$el.append( '<span class="resize-handle"></span>' );
		},

		bindRivets : function() {
			return rivets.bind( this.el, { layoutitem : this.model } );
		},

		renderFunction : function() {
			if( this.model.dashlet ) {
				this.dashlet = new dashletModule.Views.Item( { model : this.model.dashlet, container : this  } );
			}
			if( this.model.Prompt ) {
				this.dashlet = new promptModule.Views.Item( { model : this.model.Prompt, container : this } );
			}
			this.$el.append( this.dashlet.el );
		},

		afterRender : function(){
			var that = this;
			if( !rivets.binders.hasOwnProperty( 'layoutitem' ) ){
				rivets.binders.layoutitem = function( el, value ) {
					var $el = $( el );
					$el.css( {
						width : value.percentWidth + PERCENT,
						height : value.percentHeight + PERCENT,
						left : value.percentLeft + PERCENT,
						top: value.percentTop + PERCENT
					} );
				};
			}
			this.bindRivets();
		},

		handleDragstart : function( e ) {
			e.stopPropagation();
			if( e.which !== 1 ) { return; }
			if( $( e.target ).prop( 'tagName' ).toLowerCase() === 'select' ) { return; }
			$( 'body' ).addClass( 'noselect' );
			this.$el.addClass( 'dragging' );
			this.parentOffset = this.parentView.$el.offset();
			this.elOffset = this.$el.offset();
			this.startOffX = e.clientX - this.elOffset.left;
			this.startOffY = e.clientY - this.elOffset.top;
			this.parentView.$el.on( 'mousemove.draglayout', _.bind( this.handleDrag, this ) );
			$( window ).one( 'mouseup.draglayout', _.bind( this.handleDragend, this ) );
			this.$el.trigger( 'bringtofront' );
		},

		handleDrag : function( e ) {
			var newOffset = {
				left : e.clientX - this.startOffX - this.parentOffset.left,
				top : e.clientY - this.startOffY - this.parentOffset.top + this.parentView.$el.scrollTop()
			};
			newOffset = this.parentView.convertOffsetToPercent( newOffset );
			newOffset = this.parentView.correctedOffset( newOffset, this.model );
			this.model.setOffset( newOffset );
		},

		handleDragend : function() {
			this.parentView.$el.off( '.draglayout' );
			$( window ).off( '.draglayout' );
			$( 'body' ).removeClass( 'noselect' );
			this.$el.removeClass( 'dragging' );
		},

		handleResizeStart : function( e ) {
			if ( e.which === 1 ) {
				this.resizing = true;
				$( 'body' ).addClass( 'noselect' );
				this.$el.addClass( 'dragging' );
				this.elOffset = this.$el.offset();
				this.startOffX = e.clientX;
				this.startOffY = e.clientY;
				this.startDimensions = {
					height : this.model.percentHeight,
					width : this.model.percentWidth
				};
				this.parentView.$el.on( 'mousemove.resizing', _.bind( this.handleResize, this ) );
				this.parentView.$el.on( 'mouseup.resizing', _.bind( this.handleResizeEnd, this ) );
			}
		},

		handleResize : function( e ) {
			// console.log("------------------------------------------------------");
			e.stopPropagation();
			var xChange = e.clientX - this.startOffX;
			var xChangeInPercent = ( xChange / this.parentView.$el.width() ) * 100;
			// console.log("xChange:", xChange, "xChangeInPercent:", xChangeInPercent);
			// console.log("xChange % = ( xChange / this.parentView.$el.width() (" + this.parentView.$el.width() + ") ) * 100;");
			var yChange = e.clientY - this.startOffY;
			var yChangeInPercent = ( yChange / this.parentView.$el.height() ) * 100;
			// console.log("yChange:", yChange, "yChangeInPercent:", yChangeInPercent);
			// console.log("yChange % = ( yChange / this.parentView.$el.height() (" + this.parentView.$el.height() + ") ) * 100;");
			// console.log("this.startDimensions:", this.startDimensions);
			var newDimensions = {
				height : this.startDimensions.height + yChangeInPercent,
				width : this.startDimensions.width + xChangeInPercent,
			};
			// console.log("newDimensions before:", newDimensions);
			// console.log("this.model", this.model.percentHeight, this.model.percentWidth);
			newDimensions = this.parentView.correctedDimensions( newDimensions, this.model );
			// console.log("newDimensions after:", newDimensions);
			// console.log("------------------------------------------------------");
			if ( this.model.setDimensions( newDimensions ) ) {
				this.dashlet.redrawChart();
			}
		},

		handleResizeEnd : function( e ) {
			if( this.resizing ) {
				$( 'body' ).removeClass( 'noselect' );
				this.$el.removeClass( 'dragging' );
				this.parentView.$el.off( '.resizing' );
				this.resizing = false;
			}
		},

		afterResize : function( dimensions ) {
			this.dashlet.redrawChart();
		},

		removeItem : function() {
			this.dashlet.remove();
			this.remove();
		}
	} );
} );
