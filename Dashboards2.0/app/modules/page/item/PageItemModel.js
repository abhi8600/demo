define( function( require, exports, module ) {
	'use strict';
	var _ = require( 'lodash' );
	
	module.exports = function( opts ) {
		var defaults = {
			aspectRatio : 75,
			percentWidth : 50,
			percentHeight : 50,
			percentTop : 0,
			percentLeft : 0,
			"z-index" : 1,
		};
		_.extend( this, defaults, opts );
	};

	module.exports.prototype = {
		setDimensions : function( dimensions ) {
			var changed = false;
			if ( this.percentHeight !== dimensions.height ) {
				this.percentHeight = dimensions.height;
				changed = true;
			}
			if ( this.percentWidth !== dimensions.width ) {
				this.percentWidth = dimensions.width;
				changed = true;
			}
			return changed;
		},

		setOffset : function( offset ) {
			if ( this.percentLeft !== offset.left ) {
				this.percentLeft = offset.left;
			}
			if ( this.percentTop !== offset.top ) {
				this.percentTop = offset.top;
			}
		}
	};
} );
