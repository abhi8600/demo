package com.successmetricsinc.packages;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.PackageSpaceProperties;
import com.successmetricsinc.Repository;

public class PackageDefinition implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static Logger logger = Logger.getLogger(PackageDefinition.class);
	
	private PackageSpaceProperties packageSpaceProperties;
	private PObject[] dimensionTables;
	private PObject[] measureTables;
	private PObject[] stagingTables;
	private PObject[] variables;
	private PObject[] aggregates;
	private PObject[] customSubjectAreas;
	
	public PackageDefinition(){}
	
	public PackageDefinition(Element el, Namespace ns, boolean newFormat)
	{
		if(newFormat)
		{
			parseNewFormat(el, ns);
		}
		else
		{
			parseOldFormat(el, ns);
		}
		
	}
	
	private void parseNewFormat(Element el, Namespace ns)
	{
		stagingTables = getPackageObjects(el, ns, "StagingTables");
		dimensionTables = getPackageObjects(el, ns, "DimensionTables");
		measureTables = getPackageObjects(el, ns, "MeasureTables");
		variables = getPackageObjects(el, ns, "Variables");
		aggregates = getPackageObjects(el, ns, "Aggregates");
		customSubjectAreas = getPackageObjects(el, ns, "CustomSubjectAreas");
	}
	
	
	@SuppressWarnings("unchecked")
	private PObject[] getPackageObjects(Element e, Namespace ns, String name)
	{
		List l = e.getChildren(name, ns);
		if (l.isEmpty())
			return (null);
		l = ((Element) l.get(0)).getChildren();
		PObject[] pObjects = new PObject[l.size()];
		for (int count = 0; count < l.size(); count++)
		{
			pObjects[count] = new PObject((Element) l.get(count), ns);
		}
		return pObjects;
	}
	
	private void parseOldFormat(Element pe, Namespace ns)
	{
		String[] stagingTableNames = Repository.getStringArrayChildren(pe, "StagingTables", ns);
		String[] dimensionTableNames = Repository.getStringArrayChildren(pe, "DimensionTables", ns);
		String[] dimensionTableDisplayNames = Repository.getStringArrayChildren(pe, "DimensionTableDisplayNames", ns);
		String[] measureTableNames = Repository.getStringArrayChildren(pe, "MeasureTables", ns);
		String[] measureTableDisplayNames = Repository.getStringArrayChildren(pe, "MeasureTableDisplayNames", ns);
		String[] variableNames = Repository.getStringArrayChildren(pe, "Variables", ns);
		if (stagingTableNames != null && stagingTableNames.length > 0)
		{
			stagingTables = new PObject[stagingTableNames.length];
			for(int i=0; i < stagingTableNames.length; i++)
			{
				PObject pObject = new PObject();
				pObject.setName(stagingTableNames[i]);
				stagingTables[i] = pObject;
			}
		}
		if (dimensionTableNames != null && dimensionTableNames.length > 0)
		{
			dimensionTables = new PObject[dimensionTableNames.length];
			for(int i=0; i < dimensionTableNames.length; i++)
			{
				PObject pObject = new PObject();
				pObject.setName(dimensionTableNames[i]);
				pObject.setDisplayName(dimensionTableDisplayNames[i]);
				dimensionTables[i] = pObject;
			}
		}
		if (measureTableNames != null && measureTableNames.length > 0)
		{
			measureTables = new PObject[measureTableNames.length];
			for(int i=0; i < measureTableNames.length; i++)
			{
				PObject pObject = new PObject();
				pObject.setName(measureTableNames[i]);
				pObject.setDisplayName(measureTableDisplayNames[i]);
				measureTables[i] = pObject;
			}
		}

		if (variableNames != null && variableNames.length > 0)
		{
			variables = new PObject[variableNames.length];
			for(int i=0; i < variableNames.length; i++)
			{
				PObject pObject = new PObject();
				pObject.setName(variableNames[i]);
				variables[i] = pObject;
			}
		}		
	}
	
	public PackageSpaceProperties getPackageSpaceProperties() {
		return packageSpaceProperties;
	}
	
	public PObject[] getDimensionTables() {
		return dimensionTables;
	}
	public PObject[] getMeasureTables() {
		return measureTables;
	}
	public PObject[] getStagingTables() {
		return stagingTables;
	}
	public PObject[] getVariables() {
		return variables;
	}
	public void setPackageSpaceProperties(PackageSpaceProperties packageSpaceProperties) {
		this.packageSpaceProperties = packageSpaceProperties;
	}
	
	public void setDimensionTables(PObject[] dimensionTables) {
		this.dimensionTables = dimensionTables;
	}
	public void setMeasureTables(PObject[] measureTables) {
		this.measureTables = measureTables;
	}
	public void setStagingTables(PObject[] stagingTables) {
		this.stagingTables = stagingTables;
	}
	public void setVariables(PObject[] variables) {
		this.variables = variables;
	}

	public PObject[] getAggregates() {
		return aggregates;
	}

	public PObject[] getCustomSubjectAreas() {
		return customSubjectAreas;
	}

	public void setAggregates(PObject[] aggregates) {
		this.aggregates = aggregates;
	}

	public void setCustomSubjectAreaNames(PObject[] customSubjectAreaNames) {
		this.customSubjectAreas = customSubjectAreaNames;
	}
	
	public static List<String> getPObjectNames(PObject[] pObjects)
	{
		List<String> response = null;
		if(pObjects != null && pObjects.length > 0)
		{
			response = new ArrayList<String>();
			for(PObject pObject : pObjects)
			{
				response.add(pObject.getName());
			}
		}
		return response;
	}
}
