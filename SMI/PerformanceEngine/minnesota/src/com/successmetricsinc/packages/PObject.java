package com.successmetricsinc.packages;

import java.io.Serializable;

import org.jdom.Element;
import org.jdom.Namespace;

public class PObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String name;
	private String displayName;
	private boolean hide;
	
	public PObject(){}
	public PObject(Element e, Namespace ns)
	{	
		name = e.getChildTextTrim("Name", ns);
		displayName = e.getChildTextTrim("DisplayName", ns);
		String hideNode = e.getChildText("Hide", ns);
		if(hideNode != null)
		{
			hide = Boolean.parseBoolean(hideNode);
		}
	}
	
	public String getName() {
		return name;
	}
	public String getDisplayName() {
		return displayName;
	}
	public boolean isHide() {
		return hide;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public void setHide(boolean hide) {
		this.hide = hide;
	}
	
	
}
