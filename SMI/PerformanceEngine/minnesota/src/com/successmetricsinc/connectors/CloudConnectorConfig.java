package com.successmetricsinc.connectors;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import com.successmetricsinc.connectors.ExtractionObject.Types;
import com.successmetricsinc.security.EncryptionService;

public class CloudConnectorConfig {

	private static final Logger logger = Logger.getLogger(CloudConnectorConfig.class);
	public static final int currentConfigVersion = 3;
	private int configVersion = currentConfigVersion;
	private List<CloudConnection> cloudConnections;
	private List<ExtractionGroup> extractionGroups;
	private List<ExtractionObjectWrapper> extractionObjectWrappers;
	private Date dateCreated;
	private Date dateModified;
	private String createdBy;
	private String modifiedBy;
	private static EncryptionService svc = EncryptionService.getInstance();
	
	private CloudConnectorConfig() {		
	}

	public int getConfigVersion() {
		return configVersion;
	}

	public void setConfigVersion(int configVersion) {
		this.configVersion = configVersion;
	}
	
	public List<CloudConnection> getCloudConnections() {
		return cloudConnections;
	}

	public void setCloudConnections(List<CloudConnection> cloudConnections) {
		this.cloudConnections = cloudConnections;
	}
	
	public List<ExtractionGroup> getExtractionGroups() {
		return extractionGroups;
	}

	public void setExtractionGroups(List<ExtractionGroup> extractionGroups) {
		this.extractionGroups = extractionGroups;
	}

	public List<ExtractionObjectWrapper> getExtractionObjectWrappers() {
		return extractionObjectWrappers;
	}

	public void setExtractionObjectWrappers(List<ExtractionObjectWrapper> extractionObjectWrappers) {
		this.extractionObjectWrappers = extractionObjectWrappers;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateModified() {
		return dateModified;
	}

	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	public static long getLastModified(String spaceDirectory)
	{
		String newConfigDirPath = spaceDirectory + File.separator + "connectors";
		File newConfigDir = new File(newConfigDirPath);
		if (!newConfigDir.exists() || !newConfigDir.isDirectory())
			newConfigDir.mkdir();
		String configFilePath = spaceDirectory + File.separator + "connectors" + File.separator + "connectors_config.xml";
		File file = new File(configFilePath);
		return file.lastModified();		
	}
	
	public static CloudConnectorConfig getCloudConnectorConfig(String spaceDirectory) throws UnrecoverableException {
		try
		{
			String newConfigDirPath = spaceDirectory + File.separator + "connectors";
			File newConfigDir = new File(newConfigDirPath);
			if (!newConfigDir.exists() || !newConfigDir.isDirectory())
				newConfigDir.mkdir();
			String configFilePath = spaceDirectory + File.separator + "connectors" + File.separator + "connectors_config.xml";
			File file = new File(configFilePath);
			if (!file.exists())
			{
				return new CloudConnectorConfig();
			}
			else
			{
				return loadFromCloudConnectorConfigFile(configFilePath);
			}			
		}
		catch (Exception ex)
		{
			logger.error("Exception in loading config file " + ex.getMessage(), ex);
			throw new UnrecoverableException(ex.getMessage(), ex);
		}	
	}
	
	public void saveCloudConnectorConfig(String spaceDirectory, String userName) throws UnrecoverableException {
		Date now = new Date();
		if (this.createdBy == null)
			this.createdBy = userName;
		if (this.dateCreated == null)
			this.dateCreated = now;
		this.modifiedBy = userName;
		this.dateModified = now;
		try
		{
			String newConfigDirPath = spaceDirectory + File.separator + "connectors";
			File newConfigDir = new File(newConfigDirPath);
			if (!newConfigDir.exists() || !newConfigDir.isDirectory())
				newConfigDir.mkdir();
			String configFilePath = spaceDirectory + File.separator + "connectors" + File.separator + "connectors_config.xml";
			File file = new File(configFilePath);
			if (!file.exists())
			{
				file.createNewFile();
			}
			if (this.cloudConnections != null)
			{
				for (CloudConnection conn : this.cloudConnections)
				{
					if (conn.getMigratedConnection() != null && !conn.getMigratedConnection().isEmpty())
					{
						String connectorName = conn.getMigratedConnection().toLowerCase();
						if (connectorName.equals("salesforce"))
							connectorName = "sfdc";
						ConnectorConfig oldConfig = ConnectorConfig.getConnectorConfig(spaceDirectory, connectorName);
						updateOldConfig(oldConfig, conn);
						oldConfig.saveConnectorConfig(spaceDirectory, connectorName);
					}
				}
			}
			Element cloudConnectorConfig = getCloudConnectorConfigXML();
			XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
			xmlOutputter.getFormat().setEncoding("utf-8");
			xmlOutputter.getFormat().setOmitDeclaration(false);
			xmlOutputter.getFormat().setOmitEncoding(false);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(configFilePath), "UTF-8"));
			bw.write("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
			bw.newLine();
			xmlOutputter.output(cloudConnectorConfig, bw);
			bw.flush();
			bw.close();
		}
		catch (Exception ex)
		{
			throw new UnrecoverableException(ex.getMessage(), ex);
		}
	}
	
	private void updateOldConfig(ConnectorConfig config, CloudConnection conn)
	{
		config.setConnectorAPIVersion(conn.getConnectorAPIVersion());
		config.setConnectionProperties(conn.getConnectionProperties());
		List<ExtractionObject> extractionObjects = new ArrayList<ExtractionObject>();
		if (extractionObjectWrappers != null)
		{
			for (ExtractionObjectWrapper wrp : this.getObjectsForConnection(conn.getName(), conn.getConnectorType()))
			{
				extractionObjects.add(wrp.getExtractionObject());
			}
		}
		
		config.setExtractionObjects(extractionObjects);
		config.setStartExtractionTime(conn.getStartExtractionTime());
		config.setEndExtractionTime(conn.getEndExtractionTime());		
	}
	
	public Element getCloudConnectorConfigXML()
	{
		Element cloudConnectorConfig = new Element("CloudConnectorConfig");
		Element configVersionEl = new Element("ConfigVersion");
		configVersionEl.setText(String.valueOf(configVersion));
		cloudConnectorConfig.addContent(configVersionEl);
		SimpleDateFormat dateFormat = new SimpleDateFormat(BaseConnector.dateTimeISOStandardFormatPattern);
		cloudConnectorConfig.addContent(getCloudConnectionsXml());
		cloudConnectorConfig.addContent(getExtractionGroupsXml());
		cloudConnectorConfig.addContent(getExtractionObjectWrappersXml());
		if(createdBy !=null)
		{
			Element createdUsernameEl = new Element("CreatedUsername");
			createdUsernameEl.setText(createdBy);
			cloudConnectorConfig.addContent(createdUsernameEl);
		}
		
		if(dateCreated !=null)
		{
			Element dateCreatedEl = new Element("CreatedDate");
			dateCreatedEl.setText(dateFormat.format(dateCreated));
			cloudConnectorConfig.addContent(dateCreatedEl);
		}
		if(modifiedBy != null)
		{
			Element lastModifiedUsernameEl = new Element("LastModifiedUsername");
			lastModifiedUsernameEl.setText(modifiedBy);
			cloudConnectorConfig.addContent(lastModifiedUsernameEl);
		}
		if(dateModified != null)
		{
			Element lastModifiedDateEl = new Element("LastModifiedDate");
			lastModifiedDateEl.setText(dateFormat.format(dateModified));
			cloudConnectorConfig.addContent(lastModifiedDateEl);
		}
		return cloudConnectorConfig;
	}
	
	public Element getCloudConnectionsXml()
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat(BaseConnector.dateTimeISOStandardFormatPattern);
		Element cloudConnectionsEl = new Element("CloudConnections");
		if (cloudConnections != null)
		{
			for (CloudConnection conn : cloudConnections)
			{
				Element cloudConnectionEl = new Element("CloudConnection");
				Element ccNameEl = new Element("Name");
				ccNameEl.setText(conn.getName());
				cloudConnectionEl.addContent(ccNameEl);
				Element ccGuidEl = new Element("GUID");
				ccGuidEl.setText(conn.getGuid());
				cloudConnectionEl.addContent(ccGuidEl);
				Element ccConnectorTypeEl = new Element("ConnectorType");
				ccConnectorTypeEl.setText(conn.getConnectorType());
				cloudConnectionEl.addContent(ccConnectorTypeEl);
				Element ccConnectorAPIVersionEl = new Element("ConnectorAPIVersion");
				ccConnectorAPIVersionEl.setText(conn.getConnectorAPIVersion());
				cloudConnectionEl.addContent(ccConnectorAPIVersionEl);
				Element ccConnectionPropertiesEl = new Element("ConnectionProperties");
				boolean saveAuthentication = false;
				if (conn.getConnectionProperties() != null)
				{
					for (ConnectionProperty cp : conn.getConnectionProperties())
					{
						if (cp.name.equals(BaseConnector.SAVE_AUTHENTICATION))
						{
							saveAuthentication = Boolean.parseBoolean(cp.value);
							Element ConnProperty = new Element("ConnectionProperty");
							Element cpName = new Element("Name");
							cpName.setText(cp.name);
							ConnProperty.addContent(cpName);
							Element cpValue = new Element("Value");
							if(cp.value != null && cp.value.trim().length() > 0){
								cpValue.setText(String.valueOf(cp.isEncrypted ? (cp.value == null ? cp.value : svc.decrypt(cp.value)) : cp.value));
							}
							ConnProperty.addContent(cpValue);
							if (cp.isSecret)
							{
								Element cpIsSecret = new Element("IsSecret");
								cpIsSecret.setText(String.valueOf(cp.isSecret));
								ConnProperty.addContent(cpIsSecret);
							}
							if (cp.isEncrypted)
							{
								Element cpIsEncrypted = new Element("IsEncrypted");
								cpIsEncrypted.setText(String.valueOf(cp.isEncrypted));
								ConnProperty.addContent(cpIsEncrypted);
							}
							if (cp.isRequired)
							{
								Element cpIsRequired = new Element("IsRequired");
								cpIsRequired.setText(String.valueOf(cp.isRequired));
								ConnProperty.addContent(cpIsRequired);
							}
							ccConnectionPropertiesEl.addContent(ConnProperty);
							break;
						}
					}
				}
				if (saveAuthentication)
				{
					for (ConnectionProperty cp : conn.getConnectionProperties())
					{
						if (!cp.saveToConfig)
							continue;
						if (cp.name.equals(BaseConnector.SPACE_ID) || cp.name.equals(BaseConnector.SPACE_DIR) 
								|| cp.name.equals(BaseConnector.SPACE_NAME) || cp.name.equals(BaseConnector.SAVE_AUTHENTICATION))
							continue;
						Element ConnProperty = new Element("ConnectionProperty");
						Element cpName = new Element("Name");
						cpName.setText(cp.name);
						ConnProperty.addContent(cpName);
						Element cpValue = new Element("Value");
						if(cp.value != null && cp.value.trim().length() > 0){
							cpValue.setText(String.valueOf(cp.isEncrypted ? (cp.value == null ? cp.value : svc.decrypt(cp.value)) : cp.value));
						}
						ConnProperty.addContent(cpValue);
						if (cp.isSecret)
						{
							Element cpIsSecret = new Element("IsSecret");
							cpIsSecret.setText(String.valueOf(cp.isSecret));
							ConnProperty.addContent(cpIsSecret);
						}
						if (cp.isEncrypted)
						{
							Element cpIsEncrypted = new Element("IsEncrypted");
							cpIsEncrypted.setText(String.valueOf(cp.isEncrypted));
							ConnProperty.addContent(cpIsEncrypted);
						}
						if (cp.isRequired)
						{
							Element cpIsRequired = new Element("IsRequired");
							cpIsRequired.setText(String.valueOf(cp.isRequired));
							ConnProperty.addContent(cpIsRequired);
						}
						ccConnectionPropertiesEl.addContent(ConnProperty);
					}				
				}
				cloudConnectionEl.addContent(ccConnectionPropertiesEl);
				if (conn.getStartExtractionTime() != null)
				{
					Element startExtractionTime = new Element("StartExtractionTime");
					startExtractionTime.setText(dateFormat.format(conn.getStartExtractionTime()));
					cloudConnectionEl.addContent(startExtractionTime);
				}
				if (conn.getEndExtractionTime() != null)
				{
					Element endExtractionTime = new Element("EndExtractionTime");
					endExtractionTime.setText(dateFormat.format(conn.getEndExtractionTime()));
					cloudConnectionEl.addContent(endExtractionTime);
				}
				Element createdUsernameEl = new Element("CreatedUsername");
				createdUsernameEl.setText(conn.getCreatedBy());
				cloudConnectionEl.addContent(createdUsernameEl);
				Element dateCreatedEl = new Element("CreatedDate");
				dateCreatedEl.setText(dateFormat.format(conn.getDateCreated()));
				cloudConnectionEl.addContent(dateCreatedEl);
				Element lastModifiedUsernameEl = new Element("LastModifiedUsername");
				lastModifiedUsernameEl.setText(conn.getModifiedBy());
				cloudConnectionEl.addContent(lastModifiedUsernameEl);
				Element lastModifiedDateEl = new Element("LastModifiedDate");
				lastModifiedDateEl.setText(dateFormat.format(conn.getDateModified()));
				cloudConnectionEl.addContent(lastModifiedDateEl);
				if (conn.getMigratedConnection() != null && !conn.getMigratedConnection().isEmpty())
				{
					Element migratedConnectionEl = new Element("MigratedConnection");
					migratedConnectionEl.setText(conn.getMigratedConnection());
					cloudConnectionEl.addContent(migratedConnectionEl);
				}
				cloudConnectionsEl.addContent(cloudConnectionEl);
			}
		}
		return cloudConnectionsEl;
	}
	
	public Element getExtractionGroupsXml()
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat(BaseConnector.dateTimeISOStandardFormatPattern);
		Element extractionGroupsEl = new Element("ExtractionGroups");
		if (extractionGroups != null)
		{
			for (ExtractionGroup eg : extractionGroups)
			{
				Element extractionGroupEl = new Element("ExtractionGroup");
				Element egNameEl = new Element("Name");
				egNameEl.setText(eg.getName());
				extractionGroupEl.addContent(egNameEl);
				Element egGuidEl = new Element("GUID");
				egGuidEl.setText(eg.getGuid());
				extractionGroupEl.addContent(egGuidEl);
				Element createdUsernameEl = new Element("CreatedUsername");
				createdUsernameEl.setText(eg.getCreatedBy());
				extractionGroupEl.addContent(createdUsernameEl);
				Element dateCreatedEl = new Element("CreatedDate");
				dateCreatedEl.setText(dateFormat.format(eg.getDateCreated()));
				extractionGroupEl.addContent(dateCreatedEl);
				Element lastModifiedUsernameEl = new Element("LastModifiedUsername");
				lastModifiedUsernameEl.setText(eg.getModifiedBy());
				extractionGroupEl.addContent(lastModifiedUsernameEl);
				Element lastModifiedDateEl = new Element("LastModifiedDate");
				lastModifiedDateEl.setText(dateFormat.format(eg.getDateModified()));
				extractionGroupEl.addContent(lastModifiedDateEl);
				extractionGroupsEl.addContent(extractionGroupEl);
			}
		}
		return extractionGroupsEl;
	}
	
	public Element getExtractionObjectWrappersXml()
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat(BaseConnector.dateTimeISOStandardFormatPattern);
		Element extractionObjectsEl = new Element("ExtractionObjects");
		if (extractionObjectWrappers != null)
		{
			for (ExtractionObjectWrapper wrp : extractionObjectWrappers)
			{
				Element extractionObjectWrapperEl = new Element("ExtractionObjectWrapper");
				Element connTypeEl = new Element("ConnectorType");
				connTypeEl.setText(wrp.getConnectorType());
				extractionObjectWrapperEl.addContent(connTypeEl);
				Element cldConnectionsEl = new Element("CloudConnections");
				if (wrp.getCloudConnections() != null)
				{
					for (CloudConnection conn : wrp.getCloudConnections())
					{
						Element cldConnectionEl = new Element("CloudConnection");
						cldConnectionEl.setText(conn.getGuid());
						cldConnectionsEl.addContent(cldConnectionEl);
					}
				}
				extractionObjectWrapperEl.addContent(cldConnectionsEl);
				Element extGroupsEl = new Element("ExtractionGroups");
				if (wrp.getExtractionGroups() != null)
				{
					for (ExtractionGroup eg : wrp.getExtractionGroups())
					{
						Element egEl = new Element("ExtractionGroup");
						egEl.setText(eg.getGuid());
						extGroupsEl.addContent(egEl);
					}
				}
				extractionObjectWrapperEl.addContent(extGroupsEl);
				ExtractionObject eo = wrp.getExtractionObject();
				Element extractionObject = new Element("ExtractionObject");
				Element eoName = new Element("Name");
				eoName.setText(eo.name);
				extractionObject.addContent(eoName);
				Element eoTechName = new Element("ObjectName");
				eoTechName.setText(eo.techName);
				extractionObject.addContent(eoTechName);
				Element eoType = new Element("Type");
				eoType.setText(eo.type.toString());
				extractionObject.addContent(eoType);
				Element eoQuery = new Element("Query");
				eoQuery.setText(eo.query == null ? "" : eo.query);				
				extractionObject.addContent(eoQuery);
				Element eoMapping = new Element("Mapping");
				eoMapping.setText(eo.columnMappings == null ? "" : eo.columnMappings);
				extractionObject.addContent(eoMapping);
				Element eoSelectionCriteria = new Element("SelectionCriteria");
				eoSelectionCriteria.setText(eo.selectionCriteria);				
				extractionObject.addContent(eoSelectionCriteria);
				Element eoExtractionGroupNames = new Element(ExtractionObject.EXT_GROUP_NAMES);
				eoExtractionGroupNames.setText(eo.toString());
				extractionObject.addContent(eoExtractionGroupNames);
				if (eo.lastUpdatedDate != null)
				{
					Element eoLastUpdatedDate = new Element("LastUpdatedDate");
					eoLastUpdatedDate.setText(dateFormat.format(eo.lastUpdatedDate));
					extractionObject.addContent(eoLastUpdatedDate);
				}
				Element eoUseLastModified = new Element("UseLastModified");
				eoUseLastModified.setText(String.valueOf(eo.useLastModified));
				extractionObject.addContent(eoUseLastModified);
				if (eo.lastSystemModStamp != null)
				{
					Element eoLastSystemModStamp = new Element("LastSystemModStamp");
					eoLastSystemModStamp.setText(dateFormat.format(eo.lastSystemModStamp));
					extractionObject.addContent(eoLastSystemModStamp);
				}
				if (eo.columnNames != null)
				{
					Element eoColumnNames = new Element("ColumnNames");
					for (String colName : eo.columnNames)
					{
						Element eoColumnName = new Element("ColumnName");
						eoColumnName.setText(colName);
						eoColumnNames.addContent(eoColumnName);
					}
					extractionObject.addContent(eoColumnNames);
				}
				if (eo.objectId != null)
				{
					Element eoObjId = new Element("ObjectId");
					eoObjId.setText(eo.objectId);
					extractionObject.addContent(eoObjId);
				}
				if (eo.recordType != null)
				{
					Element eoRecordType = new Element("RecordType");
					eoRecordType.setText(eo.recordType);
					extractionObject.addContent(eoRecordType);
				}
				if (eo.url != null)
				{
					Element eoURL = new Element("URL");
					eoURL.setText(eo.url);
					extractionObject.addContent(eoURL);
				}
				if (eo.profileID != null)
				{
					Element eoProfileID = new Element("ProfileID");
					eoProfileID.setText(eo.profileID);
					extractionObject.addContent(eoProfileID);
				}
				if (eo.dimensions != null)
				{
					Element eoDimensions = new Element("Dimensions");
					eoDimensions.setText(eo.dimensions);
					extractionObject.addContent(eoDimensions);
				}
				if (eo.metrics != null)
				{
					Element eoMetrics = new Element("Metrics");
					eoMetrics.setText(eo.metrics);
					extractionObject.addContent(eoMetrics);
				}
				if (eo.segmentID != null)
				{
					Element eoSegmentID = new Element("SegmentID");
					eoSegmentID.setText(eo.segmentID);
					extractionObject.addContent(eoSegmentID);
				}
				if (eo.filters != null)
				{
					Element eoFilters = new Element("Filters");
					eoFilters.setText(eo.filters);
					extractionObject.addContent(eoFilters);
				}
				if (eo.includeAllRecords != null)
				{
					Element includeAll = new Element("IncludeAllRecords");
					includeAll.setText(String.valueOf(eo.isIncludeAllRecords()));
					extractionObject.addContent(includeAll);
				}
				if (eo.startDate != null)
				{
					Element eoStartDate = new Element("StartDate");
					eoStartDate.setText(eo.startDate);
					extractionObject.addContent(eoStartDate);
				}
				if (eo.endDate != null)
				{
					Element eoEndDate = new Element("EndDate");
					eoEndDate.setText(eo.endDate);
					extractionObject.addContent(eoEndDate);
				}
				if (eo.samplingLevel != null)
				{
					Element eoSamplingLevel = new Element("SamplingLevel");
					eoSamplingLevel.setText(eo.samplingLevel);
					extractionObject.addContent(eoSamplingLevel);
				}
				extractionObjectWrapperEl.addContent(extractionObject);
				Element createdUsernameEl = new Element("CreatedUsername");
				createdUsernameEl.setText(wrp.getCreatedBy());
				extractionObjectWrapperEl.addContent(createdUsernameEl);
				Element dateCreatedEl = new Element("CreatedDate");
				dateCreatedEl.setText(dateFormat.format(wrp.getDateCreated()));
				extractionObjectWrapperEl.addContent(dateCreatedEl);
				Element lastModifiedUsernameEl = new Element("LastModifiedUsername");
				lastModifiedUsernameEl.setText(wrp.getModifiedBy());
				extractionObjectWrapperEl.addContent(lastModifiedUsernameEl);
				Element lastModifiedDateEl = new Element("LastModifiedDate");
				lastModifiedDateEl.setText(dateFormat.format(wrp.getDateModified()));
				extractionObjectWrapperEl.addContent(lastModifiedDateEl);
				extractionObjectsEl.addContent(extractionObjectWrapperEl);
			}
		}
		return extractionObjectsEl;
	}
	
	@SuppressWarnings("unchecked")
	private static CloudConnectorConfig loadFromCloudConnectorConfigFile(String configFilePath) throws Exception {
		CloudConnectorConfig config = new CloudConnectorConfig();
		SAXBuilder builder = new SAXBuilder();
		Document d = null;		
		d = builder.build(new File(configFilePath));
		Element cloudConnectorConfig = d.getRootElement();
		String temp = cloudConnectorConfig.getChildText("ConfigVersion");
		if (temp != null && !temp.trim().isEmpty())
		{
			config.configVersion = Integer.valueOf(temp);
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat(BaseConnector.dateTimeISOStandardFormatPattern);
		Element cloudConnectionsEl = cloudConnectorConfig.getChild("CloudConnections");
		List<Element> cloudConnections = cloudConnectionsEl.getChildren("CloudConnection");
		for (Element connEl : cloudConnections)
		{
			CloudConnection conn = new CloudConnection();
			conn.setGuid(connEl.getChild("GUID").getText());
			conn.setName(connEl.getChild("Name").getText());
			conn.setConnectorType(connEl.getChild("ConnectorType").getText());
			conn.setConnectorAPIVersion(connEl.getChild("ConnectorAPIVersion").getText());
			conn.setGuid(connEl.getChild("GUID").getText());
			Element connectionProp = connEl.getChild("ConnectionProperties");
			List<Element> connProperties = connectionProp.getChildren("ConnectionProperty");
			List<ConnectionProperty> connectionProperties = new ArrayList<ConnectionProperty>(); 
			for (Element cp : connProperties)
			{
				ConnectionProperty connProperty = new ConnectionProperty();
				connProperty.name = cp.getChild("Name").getText();
				connProperty.value = cp.getChild("Value").getText();
				if (cp.getChild("IsRequired") != null)
					connProperty.isRequired = Boolean.parseBoolean(cp.getChild("IsRequired").getText());
				if (cp.getChild("IsSecret") != null)
					connProperty.isSecret = Boolean.parseBoolean(cp.getChild("IsSecret").getText());
				if (cp.getChild("IsEncrypted") != null)
					connProperty.isEncrypted = Boolean.parseBoolean(cp.getChild("IsEncrypted").getText());
				if (connProperty.isEncrypted)
					connProperty.value = svc.decrypt(connProperty.value);
				connectionProperties.add(connProperty);
			}
			conn.setConnectionProperties(connectionProperties);
			temp = connEl.getChildText("StartExtractionTime");
			if (temp != null && !temp.trim().isEmpty())
			{
				conn.setStartExtractionTime(dateFormat.parse(temp));
			}
			temp = connEl.getChildText("EndExtractionTime");
			if (temp != null && !temp.trim().isEmpty())
			{
				conn.setEndExtractionTime(dateFormat.parse(temp));
			}
			temp = connEl.getChildText("CreatedUsername");
			conn.setCreatedBy(temp != null && !temp.isEmpty() ? temp : null);
			temp = connEl.getChildText("CreatedDate");
			if (temp != null && !temp.isEmpty())
			{
				conn.setDateCreated(dateFormat.parse(temp));
			}
			temp = connEl.getChildText("LastModifiedUsername");
			conn.setModifiedBy(temp != null && !temp.isEmpty() ? temp : null);
			temp = connEl.getChildText("LastModifiedDate");
			if (temp != null && !temp.isEmpty())
			{
				conn.setDateModified(dateFormat.parse(temp));
			}
			temp = connEl.getChildText("MigratedConnection");
			conn.setMigratedConnection(temp != null && !temp.isEmpty() ? temp : null);
			config.addCloudConnection(conn);
		}
		Element extractionGroupsEl = cloudConnectorConfig.getChild("ExtractionGroups");
		List<Element> extractionGroups = extractionGroupsEl.getChildren("ExtractionGroup");
		for (Element egEl : extractionGroups)
		{
			ExtractionGroup eGroup = new ExtractionGroup();
			eGroup.setGuid(egEl.getChild("GUID").getText());
			eGroup.setName(egEl.getChild("Name").getText());
			temp = egEl.getChildText("CreatedUsername");
			eGroup.setCreatedBy(temp != null && !temp.isEmpty() ? temp : null);
			temp = egEl.getChildText("CreatedDate");
			if (temp != null && !temp.isEmpty())
			{
				eGroup.setDateCreated(dateFormat.parse(temp));
			}
			temp = egEl.getChildText("LastModifiedUsername");
			eGroup.setModifiedBy(temp != null && !temp.isEmpty() ? temp : null);
			temp = egEl.getChildText("LastModifiedDate");
			if (temp != null && !temp.isEmpty())
			{
				eGroup.setDateModified(dateFormat.parse(temp));
			}
			config.addExtractionGroup(eGroup);
		}
		Element extractionObjectsEl = cloudConnectorConfig.getChild("ExtractionObjects");
		List<Element> extractionObjects = extractionObjectsEl.getChildren("ExtractionObjectWrapper");
		for (Element eoEl : extractionObjects)
		{
			ExtractionObjectWrapper wrp = new ExtractionObjectWrapper();
			wrp.setConnectorType(eoEl.getChild("ConnectorType").getText());
			Element cldConnectionsEl = eoEl.getChild("CloudConnections");
			List<Element> cldConnList = cldConnectionsEl.getChildren("CloudConnection");
			for (Element cldConnEl : cldConnList)
			{
				wrp.addCloudConnection(config.findCloudConnectionByGuid(cldConnEl.getText()));
			}
			Element extGroupsEl = eoEl.getChild("ExtractionGroups");
			List<Element> extGroupList = extGroupsEl.getChildren("ExtractionGroup");
			for (Element extGroupEl : extGroupList)
			{
				wrp.addExtractionGroup(config.findExtractGroupByGuid(extGroupEl.getText()));
			}
			Element extractionObjectEl = eoEl.getChild("ExtractionObject");
			ExtractionObject extObject = new ExtractionObject();
			extObject.name = extractionObjectEl.getChild("Name").getText();
			extObject.type = Types.valueOf(extractionObjectEl.getChild("Type").getText());
			if (extractionObjectEl.getChild("Query") != null)
				extObject.query = extractionObjectEl.getChild("Query").getText();
			if (extractionObjectEl.getChild("ObjectId") != null)
				extObject.setObjectId(extractionObjectEl.getChild("ObjectId").getText());
			if (extractionObjectEl.getChild("RecordType") != null)
				extObject.setRecordType(extractionObjectEl.getChild("RecordType").getText());
			if (extractionObjectEl.getChild("ProfileID") != null)
				extObject.setProfileID(extractionObjectEl.getChild("ProfileID").getText());
			if (extractionObjectEl.getChild("Dimensions") != null)
				extObject.setDimensions(extractionObjectEl.getChild("Dimensions").getText());
			if (extractionObjectEl.getChild("Metrics") != null)
				extObject.setMetrics(extractionObjectEl.getChild("Metrics").getText());
			if (extractionObjectEl.getChild("SegmentID") != null)
				extObject.setSegmentID(extractionObjectEl.getChild("SegmentID").getText());
			if (extractionObjectEl.getChild("Filters") != null)
				extObject.setFilters(extractionObjectEl.getChild("Filters").getText());
			if (extractionObjectEl.getChild("IncludeAllRecords") != null)
			{
				String includeAllRecStr = extractionObjectEl.getChild("IncludeAllRecords").getText();
				extObject.setIncludeAllRecords(includeAllRecStr.length() == 0? false : Boolean.valueOf(includeAllRecStr));
			}
			if (extractionObjectEl.getChild("StartDate") != null)
				extObject.setStartDate(extractionObjectEl.getChild("StartDate").getText());
			if (extractionObjectEl.getChild("EndDate") != null)
				extObject.setEndDate(extractionObjectEl.getChild("EndDate").getText());
			if (extractionObjectEl.getChild("SamplingLevel") != null)
				extObject.setSamplingLevel(extractionObjectEl.getChild("SamplingLevel").getText());
			if (extractionObjectEl.getChild("URL") != null)
				extObject.setURL(extractionObjectEl.getChild("URL").getText());
			if (extractionObjectEl.getChild("ObjectName") != null)
				extObject.techName = extractionObjectEl.getChild("ObjectName").getText();
			if(extractionObjectEl.getChild("Mapping") != null)
				 extObject.columnMappings = extractionObjectEl.getChild("Mapping").getText();
			if (extractionObjectEl.getChild("SelectionCriteria") != null)
				extObject.selectionCriteria = extractionObjectEl.getChild("SelectionCriteria").getText();
			if (extractionObjectEl.getChild("LastUpdatedDate") != null)
				extObject.lastUpdatedDate = dateFormat.parse(extractionObjectEl.getChild("LastUpdatedDate").getText());
			if (extractionObjectEl.getChild("UseLastModified") != null)
				extObject.useLastModified = Boolean.parseBoolean(extractionObjectEl.getChild("UseLastModified").getText());
			if (extractionObjectEl.getChild("LastSystemModStamp") != null)
				extObject.lastSystemModStamp = dateFormat.parse(extractionObjectEl.getChild("LastSystemModStamp").getText());
			if (extractionObjectEl.getChild(ExtractionObject.EXT_GROUP_NAMES) != null && extractionObjectEl.getChild(ExtractionObject.EXT_GROUP_NAMES).getText().trim().length() > 0) {
				extObject.setExtractionGroupNames(extractionObjectEl.getChild(ExtractionObject.EXT_GROUP_NAMES).getText().trim());
			}
			if (extractionObjectEl.getChild("ColumnNames") != null)
			{
				List<String> colNames = new ArrayList<String>();
				List<Element> columnNames = extractionObjectEl.getChild("ColumnNames").getChildren("ColumnName");
				for (Element colName : columnNames)
				{
					colNames.add(colName.getText());
				}
				extObject.columnNames = colNames.toArray(new String[] {});
			}
			wrp.setExtractionObject(extObject);
			temp = eoEl.getChildText("CreatedUsername");
			wrp.setCreatedBy(temp != null && !temp.isEmpty() ? temp : null);
			temp = eoEl.getChildText("CreatedDate");
			if (temp != null && !temp.isEmpty())
			{
				wrp.setDateCreated(dateFormat.parse(temp));
			}
			temp = eoEl.getChildText("LastModifiedUsername");
			wrp.setModifiedBy(temp != null && !temp.isEmpty() ? temp : null);
			temp = eoEl.getChildText("LastModifiedDate");
			if (temp != null && !temp.isEmpty())
			{
				wrp.setDateModified(dateFormat.parse(temp));
			}
			config.addExtractionObjectWrapper(wrp);
		}
		temp = cloudConnectorConfig.getChildText("CreatedUsername");
		if (temp != null && !temp.trim().isEmpty())
		{
			config.createdBy = temp;
		}
		temp = cloudConnectorConfig.getChildText("CreatedDate");
		if (temp != null && !temp.trim().isEmpty())
		{
			config.dateCreated = dateFormat.parse(temp);
		}
		temp = cloudConnectorConfig.getChildText("LastModifiedUsername");
		if (temp != null && !temp.trim().isEmpty())
		{
			config.modifiedBy = temp;
		}
		temp = cloudConnectorConfig.getChildText("LastModifiedDate");
		if (temp != null && !temp.trim().isEmpty())
		{
			config.dateModified = dateFormat.parse(temp);
		}
		return config;
	}
	
	public CloudConnectorConfig migrateCloudConnectorConfig(String connectorType, String connectorName, String connectorAPIVersion, ConnectorConfig oldConfig, String userName) throws UnrecoverableException {
		try
		{
			// migrate to new config
			CloudConnection conn = findCloudConnection(connectorName, connectorType);
			Date now = new Date();
			if (conn == null)
			{
				conn = new CloudConnection();
				conn.setGuid(UUID.randomUUID().toString());
				conn.setName(connectorName);
				conn.setConnectorType(connectorType);
				conn.setCreatedBy(userName);
				conn.setDateCreated(now);
				addCloudConnection(conn);
			}
			conn.setConnectorAPIVersion(connectorAPIVersion);
			conn.setConnectionProperties(oldConfig.getConnectionProperties());
			conn.setModifiedBy(userName);
			conn.setDateModified(now);
			conn.setStartExtractionTime(oldConfig.getStartExtractionTime());
			conn.setEndExtractionTime(oldConfig.getEndExtractionTime());
			conn.setMigratedConnection(connectorName);
			List<ExtractionObject> oldConfigObjects = oldConfig.getExtractionObjects();
			if (oldConfigObjects != null)
			{
				for (ExtractionObject obj: oldConfigObjects)
				{
					ExtractionObjectWrapper wrp = findExtractionObjectWrapper(obj.name, conn);
					if (wrp == null)
					{
						wrp = new ExtractionObjectWrapper();
						wrp.connectorType = conn.getConnectorType();
						wrp.addCloudConnection(conn);
						wrp.setCreatedBy(userName);
						wrp.setDateCreated(now);
						addExtractionObjectWrapper(wrp);
					}
					wrp.setExtractionObject(obj);// always update object from old config file
					wrp.setModifiedBy(userName);
					wrp.setDateModified(now);
					wrp.setExtractionGroups(new ArrayList<ExtractionGroup>());
					if (obj.getExtractionGroupNames() != null)
					{
						for (String eg: obj.getExtractionGroupNames())
						{
							ExtractionGroup eGroup = findExtractGroup(eg);
							if (eGroup == null)
							{
								eGroup = new ExtractionGroup();
								eGroup.setGuid(UUID.randomUUID().toString());
								eGroup.setName(eg);
								eGroup.setCreatedBy(userName);
								eGroup.setDateCreated(now);
								eGroup.setModifiedBy(userName);
								eGroup.setDateModified(now);
								addExtractionGroup(eGroup);											
							}
							wrp.addExtractionGroup(eGroup);
						}
					}
				}
				List<ExtractionObjectWrapper> existingObjectsForConnection = getObjectsForConnection(conn.getName(), conn.getConnectorType());
				if (oldConfigObjects != null && existingObjectsForConnection != null)
				{
					List<ExtractionObjectWrapper> toRemoveObjects = new ArrayList<>();
					for (ExtractionObjectWrapper wrp : existingObjectsForConnection)
					{
						if (!oldConfigObjects.contains(wrp.getExtractionObject()) && wrp.cloudConnections.size() == 1)
						{
							toRemoveObjects.add(wrp);
						}
					}
					this.extractionObjectWrappers.removeAll(toRemoveObjects);
				}
			}												
		}
		catch (Exception ex)
		{
			logger.error("Exception in loading config file " + ex.getMessage(), ex);
			throw new UnrecoverableException(ex.getMessage(), ex);
		}
		return this;
	}
	
	public List<ExtractionObjectWrapper> getObjectsForConnection(String connection, String connectorType)
	{
		if (connection != null && !connection.isEmpty())
		{
			CloudConnection conn = findCloudConnection(connection, connectorType);
			if (conn != null)
			{
				if (extractionObjectWrappers != null)
				{
					List<ExtractionObjectWrapper> extractionObjects = new ArrayList<ExtractionObjectWrapper>(); 
					for (ExtractionObjectWrapper eObj : extractionObjectWrappers)
					{
						if (eObj.getCloudConnections() != null && eObj.getCloudConnections().contains(conn))
						{
							extractionObjects.add(eObj);						
						}
					}
					return extractionObjects;
				}
			}
		}
		else if (connectorType != null && !connectorType.equals("All"))			
		{
			if (extractionObjectWrappers != null)
			{
				List<ExtractionObjectWrapper> extractionObjects = new ArrayList<ExtractionObjectWrapper>(); 
				for (ExtractionObjectWrapper eObj : extractionObjectWrappers)
				{
					if (eObj.getCloudConnections() != null)
					{
						for (CloudConnection conn : eObj.getCloudConnections())
						{
							if (conn.getConnectorType().equals(connectorType))
							{
								extractionObjects.add(eObj);
								break;
							}
						}
					}
				}
				return extractionObjects;
			}
		}
		return null;
	}
	
	private void addExtractionObjectWrapper(ExtractionObjectWrapper wrp)
	{
		if (extractionObjectWrappers == null)
		{
			extractionObjectWrappers = new ArrayList<ExtractionObjectWrapper>();			
		}
		extractionObjectWrappers.add(wrp);
	}
	
	public void addCloudConnection(CloudConnection conn)
	{
		if (cloudConnections == null)
		{
			cloudConnections = new ArrayList<CloudConnection>();
		}
		cloudConnections.add(conn);		
	}
	
	public CloudConnection findCloudConnectionByGuid(String guid)
	{
		if (cloudConnections != null)
		{
			for (CloudConnection conn : cloudConnections)
			{
				if (conn.getGuid().equals(guid))
					return conn;
			}
		}
		return null;
	}
	
	private CloudConnection findCloudConnection(String connection, String connectionType)
	{
		if (cloudConnections != null)
		{
			for (CloudConnection conn : cloudConnections)
			{
				if (conn.getName().equals(connection) && conn.getConnectorType().equals(connectionType))
					return conn;
			}
		}
		return null;
	}
	
	public List<ExtractionObjectWrapper> getObjectsForExtractionGroup(String extractionGroup)
	{
		ExtractionGroup eGroup = findExtractGroup(extractionGroup);
		if (eGroup != null)
		{
			if (extractionObjectWrappers != null)
			{
				List<ExtractionObjectWrapper> extractionObjects = new ArrayList<ExtractionObjectWrapper>(); 
				for (ExtractionObjectWrapper eObj : extractionObjectWrappers)
				{
					if (eObj.getExtractionGroups() != null && eObj.getExtractionGroups().contains(eGroup))
					{
						extractionObjects.add(eObj);						
					}
				}
				return extractionObjects;
			}
		}
		return null;
	}
	
	private void addExtractionGroup(ExtractionGroup eGroup)
	{
		if (extractionGroups == null)
		{
			extractionGroups = new ArrayList<ExtractionGroup>();
		}
		extractionGroups.add(eGroup);		
	}
	
	private ExtractionGroup findExtractGroupByGuid(String guid)
	{
		if (extractionGroups != null)
		{
			for (ExtractionGroup eg : extractionGroups)
			{
				if (eg.getGuid().equals(guid))
					return eg;
			}
		}
		return null;
	}
	
	private ExtractionGroup findExtractGroup(String eGroup)
	{
		if (extractionGroups != null)
		{
			for (ExtractionGroup eg : extractionGroups)
			{
				if (eg.getName().equals(eGroup))
					return eg;
			}
		}
		return null;
	}
	
	private ExtractionObjectWrapper findExtractionObjectWrapper(String objName, CloudConnection conn)
	{
		List<ExtractionObjectWrapper> objectsForConnection = getObjectsForConnection(conn.getName(), conn.getConnectorType());
		if (objectsForConnection != null)
		{
			for (ExtractionObjectWrapper wrp : objectsForConnection)
			{
				if (wrp.getExtractionObject().name.equals(objName))
					return wrp;
			}
		}
		return null;
	}
	
	
}
