package com.successmetricsinc.connectors;

import java.io.File;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

public class ConnectorUtil 
{
	private static Logger logger = Logger.getLogger(ConnectorUtil.class);
	private static final String NODE_EXTRACT_GROUP = "ExtractGroup";

	public static String getExtractGroups(String extractGroupsXML)
	{
		StringBuffer extractGroups = new StringBuffer();
		try
		{
			if (extractGroupsXML != null)
			{
				List<String> extractGroupList = null;
				SAXBuilder builder = new SAXBuilder();
				Document d = builder.build(new StringReader(extractGroupsXML));
				Element root = d.getRootElement();
				List<Element> elements = root.getChildren(NODE_EXTRACT_GROUP);
				if (elements != null)
				{
					extractGroupList = new ArrayList<String>();
					for (Element el : elements)
					{
						String extractGroup = el.getText();
						if (!extractGroupList.contains(extractGroup))
							extractGroupList.add(extractGroup);
					}
					Collections.sort(extractGroupList);
				}
				if (extractGroupList != null && !extractGroupList.isEmpty())
				{
					boolean isFirst = false;
					for (String eg : extractGroupList) 
					{
						if (isFirst)
						{
							isFirst = false;
						}
						else
						{
							extractGroups.append(",");
						}
						extractGroups.append(eg);
					}
				}
			}
		}
		catch(Exception ex)
		{
			logger.error("error while finding extract groups: " + ex.getMessage());
		}
		return extractGroups.toString();
	}
	
	public static List<String> getExtractGroupList(String extractGroups)
	{
		List<String> extractGroupList = null;
		if (extractGroups != null && extractGroups.length() > 0)
		{
			String[] extractGroupsArr = extractGroups.split(",");
			extractGroupList = Arrays.asList(extractGroupsArr);
		}
		return extractGroupList;
	}
	
	public static String getVariablesForExtractGroups(String spaceDirectory, String connectorName, String extractGroups)
	{
		StringBuffer variables = new StringBuffer();
		
		try
		{
			List<String> extractGroupsList = ConnectorUtil.getExtractGroupList(extractGroups);
			ConnectorConfig settings = ConnectorConfig.getConnectorConfig(spaceDirectory, connectorName);
			
			if(settings != null)
			{
				Map<String, List<ExtractionObject>> extractionObjectsForExtractionGroups = null;
				if (extractGroupsList != null && extractGroupsList.size() > 0)
				{
					extractionObjectsForExtractionGroups = settings.getExtractionObjectsForExtractionGroups(extractGroupsList);
				}
				else
				{
					extractionObjectsForExtractionGroups = new HashMap<String, List<ExtractionObject>>();
					extractionObjectsForExtractionGroups.put(null, settings.getExtractionObjects());
				}
				Set<ExtractionObject> uniqueExtractObjects = new HashSet<ExtractionObject>();
				for (List<ExtractionObject> lst : extractionObjectsForExtractionGroups.values())
				{
					uniqueExtractObjects.addAll(lst);
				}
				if(uniqueExtractObjects != null && uniqueExtractObjects.size() > 0)
				{
					for (ExtractionObject eobj : uniqueExtractObjects) 
			    	{
						String vars = null;
						if (eobj.type == ExtractionObject.Types.QUERY) 
						{
							if(eobj.query != null)
							{
								vars = getVariables(eobj.query);											
							}
						}
						else if (eobj.type == ExtractionObject.Types.SAVED_OBJECT) 
						{
							if(eobj.selectionCriteria != null && !eobj.selectionCriteria.isEmpty())
							{
								vars = getVariables(eobj.selectionCriteria);
							}
							if(eobj.query != null && !eobj.query.isEmpty())
							{
								vars = getVariables(eobj.query);
							}
						}
						else if (eobj.type == ExtractionObject.Types.SAVED_GOOGLE_ANALYTICS_QUERY) 
						{
							if(eobj.startDate != null)
							{
								vars = getVariables(eobj.startDate);
							}
							if(eobj.endDate != null)
							{
								String vars2 = getVariables(eobj.endDate);
								if (vars2.length() > 0)
								{
									if (vars.length() > 0)
									{
										vars += "," + vars2;
									}
									else
									{
										vars = vars2;
									}
								}
							}
							if(eobj.profileID != null)
							{
								String vars3 = getVariables(eobj.profileID);
								if (vars3.length() > 0)
								{
									if (vars.length() > 0)
									{
										vars += "," + vars3;
									}
									else
									{
										vars = vars3;
									}
								}
							}
						}
						else if (eobj.type == ExtractionObject.Types.SAVED_OMNITURE_SITECATALYST_OBJECT) 
						{
							if(eobj.startDate != null)
							{
								vars = getVariables(eobj.startDate);
							}
							if(eobj.endDate != null)
							{
								String vars2 = getVariables(eobj.endDate);
								if (vars2.length() > 0)
								{
									if (vars.length() > 0)
									{
										vars += "," + vars2;
									}
									else
									{
										vars = vars2;
									}
								}
							}
							if(eobj.reportSuiteID != null)
							{
								String vars3 = getVariables(eobj.reportSuiteID);
								if (vars3.length() > 0)
								{
									if (vars.length() > 0)
									{
										vars += "," + vars3;
									}
									else
									{
										vars = vars3;
									}
								}
							}
						}
						if (vars != null && vars.length() > 0)
						{
							if (variables.length() > 0)
							{
								variables.append(",");
							}
							variables.append(vars);
						}
			    	}
				}
			}
		}
		catch(Exception ex)
		{
			logger.error("error while searching variables for extract groups: " + ex.getMessage());
		}
		
		return variables.toString();
	}
	
	public static String getVariables(String input)
    {
       StringBuffer variableNames = new StringBuffer();
       if (input != null)
       {
           boolean isFirst = true;
           Pattern p = Pattern.compile("V\\{.*?\\}");
           Matcher m = p.matcher(input);
           while (m.find()) 
           {
               String varName = m.group();
               varName = varName.substring(2, varName.indexOf("}"));
               if (isFirst)
               {
                   isFirst = false;
               }
               else
               {
                   variableNames.append(",");
               }
               variableNames.append(varName);
           }
       }
       return variableNames.toString();
    }
	
	public static long getConnectorConfigLastModified(String connectorName, String spaceDirectory)
	{
		File oldConfigFile = null;
		String oldConfigFilePath = null;
		boolean oldConfigExists = false;
		if (connectorName.equalsIgnoreCase("sfdc"))
		{
			//check for sforce.xml
			oldConfigFilePath = spaceDirectory + File.separator + "sforce.xml";
			oldConfigFile = new File(oldConfigFilePath);
			oldConfigExists = oldConfigFile.exists();
		}
		
		String newConfigDirPath = spaceDirectory + File.separator + "connectors";
		File newConfigDir = new File(newConfigDirPath);
		if (!newConfigDir.exists() || !newConfigDir.isDirectory())
			newConfigDir.mkdir();
		String newConfigFilePath = newConfigDirPath + File.separator + connectorName + "_config.xml";
		File newConfigFile = new File(newConfigFilePath);
		if (!newConfigFile.exists())
		{
			if (oldConfigExists)
			{
				return oldConfigFile.lastModified();
			}
			else
			{
				return 0L;
			}
		}
		else
		{
			if (oldConfigExists && oldConfigFile.lastModified() > newConfigFile.lastModified())
			{
				return oldConfigFile.lastModified();
			}
			else
			{
				return newConfigFile.lastModified();
			}
		}
	}
}
