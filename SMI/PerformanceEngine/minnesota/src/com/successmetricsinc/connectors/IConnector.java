package com.successmetricsinc.connectors;

import java.io.Writer;
import java.util.Properties;

/**
 * Interface for Birst Connectors
 *  
 * @author mpandit
 *
 */
public interface IConnector {
	
	// connect using supplied additional/optional properties overriding properties defined in propertiesFilePath
	void connect(Properties props) throws RecoverableException, UnrecoverableException; // ?? auth exception also?
	
	// return an instance of ConnectorMetaData
    ConnectorMetaData getMetaData()  throws UnrecoverableException;
    
    // return a array of types (Account, Case, Contact, etc)
    String[] getCatalog() throws RecoverableException, UnrecoverableException;
    
    /*
    * fetch data for 'type', write to 'stream', use lastmodified date (stored where?)
    * writes out header and then pipe separated columns, no quotes, escaped pipe, fixed number [. for decimal, no thousands, e notation?]and date/time formats (ISO 8601)
    * returns number of items fetched
    */
    int fetchData(Writer writer, ExtractionObject type, ExtractionProgressHandler hndlr) throws RecoverableException, UnrecoverableException;
    
    public void fillObjectDetails(ExtractionObject type) throws RecoverableException, UnrecoverableException;
    
    public boolean validateQueryForObject(ExtractionObject type) throws RecoverableException, UnrecoverableException;
    
    public boolean validateObject(ExtractionObject type) throws RecoverableException, UnrecoverableException;
}
