package com.successmetricsinc.connectors;

import com.successmetricsinc.util.BaseException;

/**
 * 
 * @author mpandit
 *
 */
public class UnrecoverableException extends BaseException {
	private int errorCode = BaseException.SUCCESS;
	
	public UnrecoverableException(String message, Throwable cause)
	{
		super(message, cause);
	}
	
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	
	
	@Override
	public int getErrorCode() {
		return errorCode;
	}
}
