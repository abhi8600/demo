package com.successmetricsinc.connectors;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.WriterAppender;
import org.jdom.Document;
import org.jdom.input.SAXBuilder;

import com.successmetricsinc.Repository;
import com.successmetricsinc.connectors.ExtractionProgressHandler.ExtractionStatus;
import com.successmetricsinc.connectors.netsuite.NetSuiteConnector;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.warehouse.StagingTable;

/**
 * 
 * @author mpandit
 *
 */
public class ConnectorOperations {
	
	private Repository r;
	private String spaceID;
	private String spaceName;
	private String spaceDir;
	private String connectorName;
	private String sandBoxURL;
	private String BirstParameters;
	private String uid;
	private int thresholdRecordCount = 1000;
	
	private static WriterAppender defaultLogfileAppender = null;
	public static final String DEFAULT_LOG_FORMAT = "%n%d{yyyy-MM-dd HH:mm:ss,SSSZ} [%t] %-5p - %m";
	
	private BlockingQueue<ExtractionObject> queue;
	private static final int maxRetries = 5;	
	private static final long retrySleepInMilliseconds = 10 * 1000;
	
	private static final Logger logger = Logger.getLogger(ConnectorOperations.class);
	
	public ConnectorOperations(Repository r, String connectorName, String BirstParameters, String sandBoxURL) {
		this.r = r;
		this.spaceID = r.getApplicationIdentifier();
		this.spaceName = r.getSpaceName();
		this.spaceDir = r.getRepositoryRootPath();
		this.connectorName = connectorName;
		this.BirstParameters = BirstParameters;
		this.sandBoxURL = sandBoxURL;
		String configDirPath = spaceDir + File.separator + "connectors";
		File configDir = new File(configDirPath);
		if (!configDir.exists() || !configDir.isDirectory())
			configDir.mkdir();
	}
	
	private IConnector connectToConnector(Properties connectionProperties) throws UnrecoverableException, RecoverableException {
		try
		{
			IConnector connector = ConnectorManager.getConnector("connector:birst://" + connectorName);
			connectionProperties.put(BaseConnector.SPACE_ID, spaceID);
			connectionProperties.put(BaseConnector.SPACE_DIR, spaceDir);
			connectionProperties.put(BaseConnector.SPACE_NAME, spaceName);
			if (sandBoxURL != null && !sandBoxURL.trim().isEmpty())
				connectionProperties.put(BaseConnector.SANDBOX_URL, sandBoxURL);
			if (BirstParameters != null && !BirstParameters.trim().isEmpty())
				connectionProperties.put(BaseConnector.BIRST_PARAMETERS, BirstParameters);
			int count = 0;
			boolean retry = false;
			do{
				try{
					connector.connect(connectionProperties);
					retry = false;
				}
				catch(RecoverableException ex){
					count++;
					retry = count < maxRetries;
					if(retry){
						Thread.sleep(500);
						logger.warn("Retrying connectToConnector : " +count + " ", ex);
					}
					else{
						logger.warn("Exhausted the retries for connectToConnector : " +count + " ", ex);
					}
				}
			}while(retry);

			return connector;
		}catch(Exception ex){
			throw new UnrecoverableException("Error while login into connector", ex);
		}
	}
	
	private class ConnectorDataExtractor extends Thread
	{
		private IConnector connector;		
		
		public ConnectorDataExtractor(String connectorName, Properties connectionProperties, int threadNo) 
				throws RecoverableException, UnrecoverableException
		{
			super("ConnectorDataExtractor-" + threadNo);
			connector = connectToConnector(connectionProperties);			
		}
		
		public void run()
		{
			while(!queue.isEmpty())
			{
				String killFileName = Util.getKillFileName(r.getRepositoryRootPath(), Util.KILL_EXTRACT_FILE); 
				if(Util.isTerminated(killFileName))
				{	
					writeOutputFile("killed-extract-");
					break;
				}
				ExtractionObject type = queue.poll();
				if(type == null)
				{
					if (logger.isTraceEnabled())
						logger.trace("No object found in queue");
					return;
				}
					try
					{
					BufferedWriter writer = null;
					String fileName = type.name;
					String prefix = ((BaseConnector)connector).getSourceFileNamePrefix();
					if (prefix != null && !prefix.trim().isEmpty())
						fileName = prefix + "_" + fileName;
					fileName = spaceDir + File.separator + "data" + File.separator + fileName + ".txt";
					String statusFileName = spaceDir + File.separator + "connectors" + File.separator + ((prefix != null && !prefix.trim().isEmpty()) ? prefix + "_" : "") + type.name + "-status.txt";
					ExtractionProgressHandler hndlr = new ExtractionProgressHandler(r, thresholdRecordCount, statusFileName);
					try
					{
						logger.info("Starting extract for object : " + type.name);
						long startTime = System.currentTimeMillis();
						writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName, false), "UTF-8"));
						int numRows = connector.fetchData(writer, type, hndlr);
						long timeTakenInSeconds = (System.currentTimeMillis() - startTime);
						logger.info("Successfully extracted object " + type.name + " : Total Time taken : " + timeTakenInSeconds + "(milliseconds)" + " : numRows : " + numRows);
						type.status = ExtractionObject.SUCCESS;
						if (hndlr != null && hndlr.getLoggedStatus() != ExtractionStatus.Complete)
							hndlr.updateProgress(ExtractionStatus.Complete, numRows);
					}
					catch (RecoverableException re)
					{
						if (type.noRetries < maxRetries)
						{
							logger.warn("Recoverable Exception encountered extracting object " + type.name + " - "+ re.getMessage() + " (retrying)");
							type.noRetries++;
							logger.debug("Retrying extraction for object " + type.name + ": number of retries: " + type.noRetries);
							Thread.sleep(retrySleepInMilliseconds);
							queue.offer(type);
							continue;
						}
						else
						{	
							logger.error("Recoverable Exception encountered extracting object  " + type.name + " - maximum number of retries reached, marking extraction as failed : " + re.getMessage(), re);
							type.status = ExtractionObject.FAILED;
							if (hndlr != null && hndlr.getLoggedStatus() != ExtractionStatus.Failed)
								hndlr.updateFailedProgress();
						}
					}
					catch (UnrecoverableException ure)
					{
						logger.error("Unrecoverable Exception encountered extracting object " + type.name + " - "+ ure.getMessage(), ure);	    
						type.status = ExtractionObject.FAILED;
						if (hndlr != null)
						{
							if(hndlr.getLoggedStatus() == ExtractionStatus.Killed)
							{
								writeOutputFile("killed-extract-");
							}
							else if(hndlr.getLoggedStatus() != ExtractionStatus.Failed)
							{
								hndlr.updateFailedProgress();
							}
						}
					}
					finally
					{
						if (writer != null)
						{
							try
							{
								logger.debug("Closing the file writer for the object " + type.name);
								writer.close();
							}
							catch (IOException ie)
							{
								logger.error(ie.getMessage(), ie);
							}
							
						}
					}
				}
				catch (Exception ex)
				{
					logger.error(ex, ex);
				}
			}
		
			String name = connectorName.toLowerCase();
			if(name.contains("netsuite")){
				 ((NetSuiteConnector) connector).wrapUp();
			}
		}
	}
	
	public void setUid(String uid){
		this.uid = uid;
	}
	
	public boolean fetchDataUsingMultipleThreads(Properties connectionProperties, List<ExtractionObject> types, int noParallelThreads) 
			throws RecoverableException, UnrecoverableException
	{
		queue = new LinkedBlockingQueue<ExtractionObject>(types.size());
		queue.addAll(types);
		int noThreads = 1;
		IConnector connector = connectToConnector(connectionProperties);
		if (connector.getMetaData().supportsParallelExtraction())
		{
			noThreads = noParallelThreads;
		}
		ConnectorDataExtractor[] extractionThreads = new ConnectorDataExtractor[noThreads];
		for (int i = 0; i < noThreads; i++)
		{
			extractionThreads[i] = new ConnectorDataExtractor(connectorName, connectionProperties, i);
			extractionThreads[i].start();
		}
		for (int i = 0; i < noThreads; i++)
		{
			try
			{
				extractionThreads[i].join();
			}
			catch (InterruptedException ie)
			{
				logger.error(ie.getMessage(), ie);				
			}			
		}
		boolean allObjectsExtracted = true;
		for (ExtractionObject type : types)
		{
			if (type.status != ExtractionObject.SUCCESS)
			{
				allObjectsExtracted = false;
				break;
			}
		}
		if (!allObjectsExtracted)
		{
			logger.debug("Extraction failed for one or more objects");
		}
		else
		{
			logger.debug("Extraction successful for all objects");
		}
		
		return allObjectsExtracted;
	}
	
	// To be called by UI to prompt for connection properties to user 
	public List<ConnectionProperty> getConnectionProperties() throws UnrecoverableException {
		IConnector connector = ConnectorManager.getConnector("connector:birst://" + connectorName);
		return connector.getMetaData().getConnectionProperties();
	}
	
	// To be called by UI to show list of available objects
	public String[] getCatalog(Properties connectionProperties) throws RecoverableException, UnrecoverableException {
		IConnector connector = ConnectorManager.getConnector("connector:birst://" + connectorName);
		connector = connectToConnector(connectionProperties);
		return connector.getCatalog();
	}
	
	public void extractData(int noExtractionThreads) throws Exception {
		ConnectorConfig config = ConnectorConfig.getConnectorConfig(spaceDir, connectorName);
		boolean success = false;
		try
		{
			if (config == null)
				throw new Exception("No config file found for connector - " + connectorName + " in space - " + spaceID);
			Util.deleteFile(Util.getKillFileName(r.getRepositoryRootPath(), Util.KILL_EXTRACT_FILE));
	    	Properties props = new Properties();
	    	for (ConnectionProperty cp : config.getConnectionProperties())
	    	{
	    		props.put(cp.name, cp.value);
	    	}
	    	List<ExtractionObject> objects = config.getExtractionObjects();
	    	for (ExtractionObject obj : objects) 
	    	{
	    		String sfNamePrefix = (connectorName.equals("sfdc") ? "" : props.getProperty(BaseConnector.SOURCEFILE_NAME_PREFIX));
	    		String sfName = ((sfNamePrefix == null || sfNamePrefix.trim().isEmpty()) ? "" : (sfNamePrefix + "_")) +  obj.name;
	    		sfName = sfName.replace(" ", "_");
	    		StagingTable st = r.findStagingTable("ST_" + sfName);
	    		obj.useLastModified = (st != null && st.Transactional);
	    	}
	    	config.setStartExtractionTime(new Date());
	    	success = fetchDataUsingMultipleThreads(props, objects, noExtractionThreads);	    	
		}
		finally
		{	
			String fileNamePart = success ?  "success-extract-" : "failed-extract-";
	    	writeOutputFile(fileNamePart);
	    	
			if (config != null)
			{
				config.setEndExtractionTime(new Date());
				config.saveConnectorConfig(spaceDir, connectorName);				
			}
		}
	}
	
	protected synchronized void writeOutputFile(String fileNamePart)
	{
		String outputFileName = null;
		try
		{
			if(uid != null)
			{	
				outputFileName = r.getRepositoryRootPath() + File.separator + "slogs" + File.separator + fileNamePart + uid + ".txt";
				File file = new File(outputFileName);
				if(!file.exists())
				{	
					file.createNewFile();
				}
			}
		}
		catch(Exception ex)
		{
			logger.warn("Error while creating out the success file file " +  outputFileName);
		}
	}
	
	public static void main(String[] args)
    {
		setupCommandLineConsoleAppender();
		String propLocation = args[0];//path of propertiesfile that contains required connection properties
		String connectorName = args[1];//sfdc
		
		File propFile = new File(propLocation);
		BufferedReader br = null;
		Writer writer = null;
		
    	try
    	{
    		SAXBuilder builder = new SAXBuilder();
    		Document d = null;
    		File nf = new File(args[2]);//path of repository
    		d = builder.build(nf);
    		Repository r = new Repository(d, false, null, nf.lastModified(), false, null, null);
    		
    		//ConnectorOperations co = new ConnectorOperations(r, connectorName, SFDCConnector.SFDC_CLIENT_ID + "=dummy", "dummyURL");
    		ConnectorOperations co = new ConnectorOperations(r, connectorName, null, null);
    		
    		br = new BufferedReader(new FileReader(propFile));
    		Properties properties = new Properties();
    		properties.load(br);
    		
    		String[] catalog = co.getCatalog(properties);
	    	for (String obj : catalog)
	    	{
	    		System.out.println(obj);
	    	}
	    	
	    	List<ExtractionObject> objects = new ArrayList<ExtractionObject>();
	    	if (connectorName.equals("sfdc"))
	    	{
		    	ExtractionObject account = new ExtractionObject();
		    	account.name = "Account";
		    	account.useLastModified = false;
		    	account.type = ExtractionObject.Types.OBJECT;
		    	objects.add(account);
		    	ExtractionObject user = new ExtractionObject();
		    	user.name = "User";
		    	user.useLastModified = false;
		    	user.type = ExtractionObject.Types.OBJECT;
		    	objects.add(user);
		    	ExtractionObject userRole = new ExtractionObject();
		    	userRole.name = "UserRole";
		    	userRole.useLastModified = false;
		    	userRole.type = ExtractionObject.Types.OBJECT;
		    	objects.add(userRole);
	    	}
	    	else if (connectorName.equals("netsuite"))
	    	{
		    	ExtractionObject account = new ExtractionObject();
		    	account.name = "Account";
		    	account.useLastModified = true;
		    	account.type = ExtractionObject.Types.OBJECT;
		    	objects.add(account);
		    	ExtractionObject customer = new ExtractionObject();
		    	customer.name = "Customer";
		    	customer.useLastModified = true;
		    	customer.type = ExtractionObject.Types.OBJECT;
		    	objects.add(customer);	    	
	    	}
	    	
	    	String spaceDir = r.getRepositoryRootPath();
	    	ConnectorConfig config = ConnectorConfig.getConnectorConfig(spaceDir, connectorName);
	    	config.setStartExtractionTime(new Date());
	    	co.fetchDataUsingMultipleThreads(properties, objects, 5);	    	
	    	config.setEndExtractionTime(new Date());
	    	List<ConnectionProperty> connProps = co.getConnectionProperties();
	    	for (ConnectionProperty cp : connProps)
	    	{
	    		if (properties.containsKey(cp.name))
	    		{
	    			cp.value = properties.getProperty(cp.name);
	    		}
	    	}
	    	config.setConnectionProperties(connProps);
	    	config.setExtractionObjects(objects);
	    	config.saveConnectorConfig(spaceDir, connectorName);
    	}
    	catch(Exception ex)
    	{
    		System.err.println(ex.getMessage());
    		ex.printStackTrace(System.err);
    	}
    	finally
    	{
    		if (br != null)
    		{
    			try
    			{
    				br.close();
    			}
    			catch(Exception ex)
    			{    				
    			}
    		}
    		if (writer != null)
    		{
    			try
    			{
    				writer.close();
    			}
    			catch(Exception ex)
    			{    				
    			}
    		}	
    	}    		
    }
	
	private static void setupCommandLineConsoleAppender()
	{
		ConsoleAppender commandLineConsoleAppender = null;
		if (defaultLogfileAppender != null)
			commandLineConsoleAppender = new ConsoleAppender(defaultLogfileAppender.getLayout());
		else
			commandLineConsoleAppender = new ConsoleAppender(new PatternLayout(DEFAULT_LOG_FORMAT));
		Logger.getRootLogger().addAppender(commandLineConsoleAppender);
		if (defaultLogfileAppender != null)
		{
			// Remove the default log file appender
			Logger.getRootLogger().removeAppender(defaultLogfileAppender);			
		}
	}
}
