package com.successmetricsinc.connectors;

import java.util.LinkedHashMap;

/**
 * 
 * @author mpandit
 *
 */
public class ConnectionProperty {
	
	public String name;
	public String value;
	public boolean saveToConfig;
	public boolean isSecret;
	public boolean isRequired;
	public boolean isEncrypted;
	public int displayIndex = -1;
	public String displayLabel;
	public String displayType;
	public LinkedHashMap<String,String> options;
	
	public ConnectionProperty() {
		this.saveToConfig = true;
	}
}
