package com.successmetricsinc.connectors;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import com.successmetricsinc.connectors.ExtractionObject.Types;
import com.successmetricsinc.connectors.sdfc.SFDCConnector;
import com.successmetricsinc.security.EncryptionService;
import com.successmetricsinc.sfdc.SalesforceSettings;
import com.successmetricsinc.sfdc.SalesforceSettings.SiteType;

/**
 * 
 * @author mpandit
 *
 */
public class ConnectorConfig {
	private static final Logger logger = Logger.getLogger(ConnectorConfig.class);
	public static final int currentConfigVersion = 2;
	private int configVersion = currentConfigVersion;
	private String ConnectorAPIVersion = null;
	private List<ConnectionProperty> connectionProperties;
	private List<ExtractionObject> extractionObjects;
	private Date StartExtractionTime;
	private Date EndExtractionTime;
	private static EncryptionService svc = EncryptionService.getInstance();
	
	private ConnectorConfig() {		
	}
	
	public String getConnectorAPIVersion() {
		return ConnectorAPIVersion;
	}

	public void setConnectorAPIVersion(String connectorAPIVersion) {
		ConnectorAPIVersion = connectorAPIVersion;
	}
	
	public int getConfigVersion() {
		return configVersion;
	}
	
	public List<ConnectionProperty> getConnectionProperties() {
		return connectionProperties;
	}
	
	public void setConnectionProperties(
			List<ConnectionProperty> connectionProperties) {
		this.connectionProperties = connectionProperties;
	}
	
	public List<ExtractionObject> getExtractionObjects() {
		return extractionObjects;
	}
	
	public void setExtractionObjects(List<ExtractionObject> extractionObjects) {
		this.extractionObjects = extractionObjects;
	}
	
	public Date getStartExtractionTime() {
		return StartExtractionTime;
	}

	public void setStartExtractionTime(Date startExtractionTime) {
		StartExtractionTime = startExtractionTime;
	}

	public Date getEndExtractionTime() {
		return EndExtractionTime;
	}

	public void setEndExtractionTime(Date endExtractionTime) {
		EndExtractionTime = endExtractionTime;
	}

	private static ConnectorConfig loadConfigFromOldConfigFile(String spaceDirectory, String oldConfigFilePath) throws Exception {
		SalesforceSettings sforceSettings = SalesforceSettings.getSalesforceSettings(oldConfigFilePath);
		ConnectorConfig config = new ConnectorConfig();
		config.configVersion = sforceSettings.version;
		List<ConnectionProperty> connectionProperties = new ArrayList<ConnectionProperty>();
		ConnectionProperty username = new ConnectionProperty();
		username.name = SFDCConnector.USERNAME;
		username.value = sforceSettings.getUsername();
		username.isRequired = true;
		connectionProperties.add(username);
		ConnectionProperty password = new ConnectionProperty();
		password.name = SFDCConnector.PASSWORD;
		password.value = sforceSettings.getPassword();
		password.isRequired = true;
		password.isEncrypted = true;
		password.isSecret = true;
		connectionProperties.add(password);
		ConnectionProperty saveAuthentication = new ConnectionProperty();
		saveAuthentication.name = BaseConnector.SAVE_AUTHENTICATION;
		saveAuthentication.value = String.valueOf(sforceSettings.saveAuthentication);
		saveAuthentication.isRequired = true;
		connectionProperties.add(saveAuthentication);
		ConnectionProperty useSandbox = new ConnectionProperty();
		useSandbox.name = BaseConnector.USE_SANDBOX_URL;
		useSandbox.value = (sforceSettings.sitetype != null && sforceSettings.sitetype == SiteType.Sandbox ? Boolean.TRUE.toString() : Boolean.FALSE.toString());
		useSandbox.isRequired = true;
		connectionProperties.add(useSandbox);
		//copy sforceSettings.startExtractionTime and sforceSettings.endExtractionTime
		config.setConnectionProperties(connectionProperties);
		if (sforceSettings != null)
		{
			List<ExtractionObject> extractionObjects = new ArrayList<ExtractionObject>();
			if(sforceSettings.objects != null)
			{
				for (SalesforceSettings.SalesforceObjectConnection soc : sforceSettings.objects)
				{
					ExtractionObject eObj = new ExtractionObject();
					eObj.name = soc.name;
					eObj.query = soc.query;
					if (soc.getExtractionGroupNames() != null && soc.getExtractionGroupNames().length() > 0) {
						eObj.setExtractionGroupNames(soc.getExtractionGroupNames());
					}
					if (eObj.query != null && !eObj.query.trim().isEmpty())
					{
						eObj.type = Types.QUERY;
						eObj.columnMappings = getSFDCObjectMapping(spaceDirectory, soc.name);
					}
					else
						eObj.type = Types.OBJECT;
					eObj.lastSystemModStamp = soc.lastSystemModStamp;
					eObj.lastUpdatedDate = soc.lastUpdatedDate;
					eObj.includeAllRecords = soc.includeAllRecords;
					if (soc.columnNames != null)
					{
						eObj.columnNames = new String[soc.columnNames.length];
						System.arraycopy(soc.columnNames, 0, eObj.columnNames, 0, soc.columnNames.length);
					}
					extractionObjects.add(eObj);
				}
			}
			config.setExtractionObjects(extractionObjects);
			config.StartExtractionTime = sforceSettings.startExtractionTime;
			config.EndExtractionTime = sforceSettings.endExtractionTime;
		}
		return config;
	}

	private static String getSFDCObjectMapping(String spaceDirectory, String objectName)
	{
		BufferedReader in = null;
		try
		{
			String spaceLogsDirectory = spaceDirectory + File.separator + "mapping";
			File f = new File(spaceLogsDirectory);
			if (!f.exists() || !f.isDirectory())
			{
				return null;
			}
			String existingObjectMappingFileName =spaceLogsDirectory + File.separator + objectName + "-mapping.txt";
			File file = new File(existingObjectMappingFileName);
			StringBuilder mapping = new StringBuilder();
			if(file.exists())
			{
				in = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
				String line = null;
				while((line = in.readLine()) != null)
				{
					mapping.append(line);
				}
			}
			return mapping.toString();
		}catch(IOException ex){
			logger.warn("Exception while loading the mapping file", ex);
		}finally{
			if (in != null){
				try
				{
					in.close();
				}catch(IOException ex){
					logger.warn("Exception while closing reader for the mapping file " + ex.getMessage());
				}
			}
		}
		
		return null;
	}
	
	private static ConnectorConfig loadConfigFromNewConfigFile(String newConfigFilePath) throws Exception {	
		ConnectorConfig config = new ConnectorConfig();
		SAXBuilder builder = new SAXBuilder();
		Document d = null;		
		d = builder.build(new File(newConfigFilePath));
		Element connectorConfig = d.getRootElement();
		String temp = connectorConfig.getChildText("ConfigVersion");
		if (temp != null && !temp.trim().isEmpty())
		{
			config.configVersion = Integer.valueOf(temp);
		}
		temp = connectorConfig.getChildText("ConnectorAPIVersion");
		if (temp != null && !temp.trim().isEmpty())
		{
			config.ConnectorAPIVersion = temp;
		}
		Element connectionProp = connectorConfig.getChild("ConnectionProperties");
		List<Element> connProperties = connectionProp.getChildren("ConnectionProperty");
		List<ConnectionProperty> connectionProperties = new ArrayList<ConnectionProperty>(); 
		for (Element cp : connProperties)
		{
			ConnectionProperty connProperty = new ConnectionProperty();
			connProperty.name = cp.getChild("Name").getText();
			connProperty.value = cp.getChild("Value").getText();
			if (cp.getChild("IsRequired") != null)
				connProperty.isRequired = Boolean.parseBoolean(cp.getChild("IsRequired").getText());
			if (cp.getChild("IsSecret") != null)
				connProperty.isSecret = Boolean.parseBoolean(cp.getChild("IsSecret").getText());
			if (cp.getChild("IsEncrypted") != null)
				connProperty.isEncrypted = Boolean.parseBoolean(cp.getChild("IsEncrypted").getText());
			if (connProperty.isEncrypted)
				connProperty.value = svc.decrypt(connProperty.value);
			connectionProperties.add(connProperty);
		}
		config.setConnectionProperties(connectionProperties);
		
		Element extractionObj = connectorConfig.getChild("ExtractionObjects");
		List<Element> extObjects = extractionObj.getChildren("ExtractionObject");
		List<ExtractionObject> extractionObjects = new ArrayList<ExtractionObject>();
		SimpleDateFormat dateFormat = new SimpleDateFormat(BaseConnector.dateTimeISOStandardFormatPattern);
		for (Element cp : extObjects)
		{
			ExtractionObject extObject = new ExtractionObject();
			extObject.name = cp.getChild("Name").getText();
			extObject.type = Types.valueOf(cp.getChild("Type").getText());
			if (cp.getChild("Query") != null)
				extObject.query = cp.getChild("Query").getText();
			if (cp.getChild("ObjectId") != null)
				extObject.setObjectId(cp.getChild("ObjectId").getText());
			if (cp.getChild("RecordType") != null)
				extObject.setRecordType(cp.getChild("RecordType").getText());
			if (cp.getChild("ProfileID") != null)
				extObject.setProfileID(cp.getChild("ProfileID").getText());
			if (cp.getChild("Dimensions") != null)
				extObject.setDimensions(cp.getChild("Dimensions").getText());
			if (cp.getChild("Metrics") != null)
				extObject.setMetrics(cp.getChild("Metrics").getText());
			if (cp.getChild("SegmentID") != null)
				extObject.setSegmentID(cp.getChild("SegmentID").getText());
			if (cp.getChild("Filters") != null)
				extObject.setFilters(cp.getChild("Filters").getText());
			if (cp.getChild("IncludeAllRecords") != null)
			{
				String includeAllRecStr = cp.getChild("IncludeAllRecords").getText();
				extObject.setIncludeAllRecords(includeAllRecStr.length() == 0? false : Boolean.valueOf(includeAllRecStr));
			}
			if (cp.getChild("StartDate") != null)
				extObject.setStartDate(cp.getChild("StartDate").getText());
			if (cp.getChild("EndDate") != null)
				extObject.setEndDate(cp.getChild("EndDate").getText());
			if (cp.getChild("SamplingLevel") != null)
				extObject.setSamplingLevel(cp.getChild("SamplingLevel").getText());
			if (cp.getChild("ReportSuiteID") != null)
				extObject.setReportSuiteID(cp.getChild("ReportSuiteID").getText());
			if (cp.getChild("Elements") != null)
				extObject.setElements(cp.getChild("Elements").getText());
			if (cp.getChild("PageSize") != null)
				extObject.setPageSize(cp.getChild("PageSize").getText());
			if (cp.getChild("URL") != null)
				extObject.setURL(cp.getChild("URL").getText());
			if (cp.getChild("ObjectName") != null)
				extObject.techName = cp.getChild("ObjectName").getText();
			if(cp.getChild("Mapping") != null)
				 extObject.columnMappings = cp.getChild("Mapping").getText();
			if (cp.getChild("SelectionCriteria") != null)
				extObject.selectionCriteria = cp.getChild("SelectionCriteria").getText();
			if (cp.getChild("LastUpdatedDate") != null)
				extObject.lastUpdatedDate = dateFormat.parse(cp.getChild("LastUpdatedDate").getText());
			if (cp.getChild("UseLastModified") != null)
				extObject.useLastModified = Boolean.parseBoolean(cp.getChild("UseLastModified").getText());
			if (cp.getChild("LastSystemModStamp") != null)
				extObject.lastSystemModStamp = dateFormat.parse(cp.getChild("LastSystemModStamp").getText());
			if (cp.getChild(ExtractionObject.EXT_GROUP_NAMES) != null && cp.getChild(ExtractionObject.EXT_GROUP_NAMES).getText().trim().length() > 0) {
				extObject.setExtractionGroupNames(cp.getChild(ExtractionObject.EXT_GROUP_NAMES).getText().trim());
			}
			if (cp.getChild("ColumnNames") != null)
			{
				List<String> colNames = new ArrayList<String>();
				List<Element> columnNames = cp.getChild("ColumnNames").getChildren("ColumnName");
				for (Element colName : columnNames)
				{
					colNames.add(colName.getText());
				}
				extObject.columnNames = colNames.toArray(new String[] {});
			}
			extractionObjects.add(extObject);
		}
		config.setExtractionObjects(extractionObjects);
		
		temp = connectorConfig.getChildText("StartExtractionTime");
		if (temp != null && !temp.trim().isEmpty())
		{
			config.StartExtractionTime = dateFormat.parse(temp);
		}
		temp = connectorConfig.getChildText("EndExtractionTime");
		if (temp != null && !temp.trim().isEmpty())
		{
			config.EndExtractionTime = dateFormat.parse(temp);
		}
		
		return config;
	}
	
	public static ConnectorConfig getConnectorConfig(String spaceDirectory, String connectorName) throws UnrecoverableException {
		try
		{
			File oldConfigFile = null;
			String oldConfigFilePath = null;
			boolean oldConfigExists = false;
			if (connectorName.equalsIgnoreCase("sfdc"))
			{
				//check for sforce.xml
				oldConfigFilePath = spaceDirectory + File.separator + "sforce.xml";
				oldConfigFile = new File(oldConfigFilePath);
				oldConfigExists = oldConfigFile.exists();
			}
			
			String newConfigDirPath = spaceDirectory + File.separator + "connectors";
			File newConfigDir = new File(newConfigDirPath);
			if (!newConfigDir.exists() || !newConfigDir.isDirectory())
				newConfigDir.mkdir();
			String newConfigFilePath = newConfigDirPath + File.separator + connectorName + "_config.xml";
			File newConfigFile = new File(newConfigFilePath);
			if (!newConfigFile.exists())
			{
				if (oldConfigExists)
				{
					return loadConfigFromOldConfigFile(spaceDirectory, oldConfigFilePath);//read sforce.xml
				}
				else
				{
					return new ConnectorConfig();//return empty config					
				}
			}
			else
			{
				if (oldConfigExists && oldConfigFile.lastModified() > newConfigFile.lastModified())
				{
					return loadConfigFromOldConfigFile(spaceDirectory, oldConfigFilePath);//read sforce.xml
				}
				else
				{
					return loadConfigFromNewConfigFile(newConfigFilePath);//read new config file
				}
			}			
		}
		catch (Exception ex)
		{
			logger.error("Exception in loading config file " + ex.getMessage(), ex);
			throw new UnrecoverableException(ex.getMessage(), ex);
		}		
	}
	
	public void saveConnectorConfig(String spaceDirectory, String connectorName) throws UnrecoverableException {
		try
		{
			String configDirPath = spaceDirectory + File.separator + "connectors";
			File configDir = new File(configDirPath);
			if (!configDir.exists() || !configDir.isDirectory())
				configDir.mkdir();
			String configFilePath = configDirPath + File.separator + connectorName + "_config.xml";
			File configFile = new File(configFilePath);
			if (!configFile.exists())
				configFile.createNewFile();
			Element connectorConfig = new Element("ConnectorConfig");
			Element configVersionEl = new Element("ConfigVersion");
			configVersionEl.setText(String.valueOf(configVersion));
			connectorConfig.addContent(configVersionEl);
			if (ConnectorAPIVersion != null)
			{
				Element connectorAPIVersionEl = new Element("ConnectorAPIVersion");
				connectorAPIVersionEl.setText(ConnectorAPIVersion);
				connectorConfig.addContent(connectorAPIVersionEl);
			}
			Element connProperties = new Element("ConnectionProperties");
			boolean saveAuthentication = false;
			for (ConnectionProperty cp : connectionProperties)
			{
				if (cp.name.equals(BaseConnector.SAVE_AUTHENTICATION))
				{
					saveAuthentication = Boolean.parseBoolean(cp.value);
					Element ConnProperty = new Element("ConnectionProperty");
					Element cpName = new Element("Name");
					cpName.setText(cp.name);
					ConnProperty.addContent(cpName);
					Element cpValue = new Element("Value");
					if(cp.value != null && cp.value.trim().length() > 0){
						cpValue.setText(String.valueOf(cp.isEncrypted ? (cp.value == null ? cp.value : svc.encrypt(cp.value)) : cp.value));
					}
					ConnProperty.addContent(cpValue);
					if (cp.isSecret)
					{
						Element cpIsSecret = new Element("IsSecret");
						cpIsSecret.setText(String.valueOf(cp.isSecret));
						ConnProperty.addContent(cpIsSecret);
					}
					if (cp.isEncrypted)
					{
						Element cpIsEncrypted = new Element("IsEncrypted");
						cpIsEncrypted.setText(String.valueOf(cp.isEncrypted));
						ConnProperty.addContent(cpIsEncrypted);
					}
					if (cp.isRequired)
					{
						Element cpIsRequired = new Element("IsRequired");
						cpIsRequired.setText(String.valueOf(cp.isRequired));
						ConnProperty.addContent(cpIsRequired);
					}
					connProperties.addContent(ConnProperty);
					break;
				}
			}
			if (saveAuthentication)
			{
				for (ConnectionProperty cp : connectionProperties)
				{
					if (!cp.saveToConfig)
						continue;
					if (cp.name.equals(BaseConnector.SPACE_ID) || cp.name.equals(BaseConnector.SPACE_DIR) 
							|| cp.name.equals(BaseConnector.SPACE_NAME) || cp.name.equals(BaseConnector.SAVE_AUTHENTICATION))
						continue;
					Element ConnProperty = new Element("ConnectionProperty");
					Element cpName = new Element("Name");
					cpName.setText(cp.name);
					ConnProperty.addContent(cpName);
					Element cpValue = new Element("Value");
					if(cp.value != null && cp.value.trim().length() > 0){
						cpValue.setText(String.valueOf(cp.isEncrypted ? (cp.value == null ? cp.value : svc.encrypt(cp.value)) : cp.value));
					}
					ConnProperty.addContent(cpValue);
					if (cp.isSecret)
					{
						Element cpIsSecret = new Element("IsSecret");
						cpIsSecret.setText(String.valueOf(cp.isSecret));
						ConnProperty.addContent(cpIsSecret);
					}
					if (cp.isEncrypted)
					{
						Element cpIsEncrypted = new Element("IsEncrypted");
						cpIsEncrypted.setText(String.valueOf(cp.isEncrypted));
						ConnProperty.addContent(cpIsEncrypted);
					}
					if (cp.isRequired)
					{
						Element cpIsRequired = new Element("IsRequired");
						cpIsRequired.setText(String.valueOf(cp.isRequired));
						ConnProperty.addContent(cpIsRequired);
					}
					connProperties.addContent(ConnProperty);
				}				
			}
			connectorConfig.addContent(connProperties);
			
			SimpleDateFormat dateFormat = new SimpleDateFormat(BaseConnector.dateTimeISOStandardFormatPattern);
			Element extractionObjects = new Element("ExtractionObjects");
			if(this.extractionObjects != null && this.extractionObjects.size() > 0)
			{
				for (ExtractionObject eo : this.extractionObjects)
				{
					Element extractionObject = new Element("ExtractionObject");
					Element eoName = new Element("Name");
					eoName.setText(eo.name);
					extractionObject.addContent(eoName);
					Element eoTechName = new Element("ObjectName");
					eoTechName.setText(eo.techName);
					extractionObject.addContent(eoTechName);
					Element eoType = new Element("Type");
					eoType.setText(eo.type.toString());
					extractionObject.addContent(eoType);
					Element eoQuery = new Element("Query");
					eoQuery.setText(eo.query == null ? "" : eo.query);				
					extractionObject.addContent(eoQuery);
					Element eoMapping = new Element("Mapping");
					eoMapping.setText(eo.columnMappings == null ? "" : eo.columnMappings);
					extractionObject.addContent(eoMapping);
					Element eoSelectionCriteria = new Element("SelectionCriteria");
					eoSelectionCriteria.setText(eo.selectionCriteria);				
					extractionObject.addContent(eoSelectionCriteria);
					Element eoExtractionGroupNames = new Element(ExtractionObject.EXT_GROUP_NAMES);
					eoExtractionGroupNames.setText(eo.toString());
					extractionObject.addContent(eoExtractionGroupNames);
					if (eo.lastUpdatedDate != null)
					{
						Element eoLastUpdatedDate = new Element("LastUpdatedDate");
						eoLastUpdatedDate.setText(dateFormat.format(eo.lastUpdatedDate));
						extractionObject.addContent(eoLastUpdatedDate);
					}
					Element eoUseLastModified = new Element("UseLastModified");
					eoUseLastModified.setText(String.valueOf(eo.useLastModified));
					extractionObject.addContent(eoUseLastModified);
					if (eo.lastSystemModStamp != null)
					{
						Element eoLastSystemModStamp = new Element("LastSystemModStamp");
						eoLastSystemModStamp.setText(dateFormat.format(eo.lastSystemModStamp));
						extractionObject.addContent(eoLastSystemModStamp);
					}
					if (eo.columnNames != null)
					{
						Element eoColumnNames = new Element("ColumnNames");
						for (String colName : eo.columnNames)
						{
							Element eoColumnName = new Element("ColumnName");
							eoColumnName.setText(colName);
							eoColumnNames.addContent(eoColumnName);
						}
						extractionObject.addContent(eoColumnNames);
					}
					if (eo.objectId != null)
					{
						Element eoObjId = new Element("ObjectId");
						eoObjId.setText(eo.objectId);
						extractionObject.addContent(eoObjId);
					}
					if (eo.recordType != null)
					{
						Element eoRecordType = new Element("RecordType");
						eoRecordType.setText(eo.recordType);
						extractionObject.addContent(eoRecordType);
					}
					if (eo.url != null)
					{
						Element eoURL = new Element("URL");
						eoURL.setText(eo.url);
						extractionObject.addContent(eoURL);
					}
					if (eo.profileID != null)
					{
						Element eoProfileID = new Element("ProfileID");
						eoProfileID.setText(eo.profileID);
						extractionObject.addContent(eoProfileID);
					}
					if (eo.dimensions != null)
					{
						Element eoDimensions = new Element("Dimensions");
						eoDimensions.setText(eo.dimensions);
						extractionObject.addContent(eoDimensions);
					}
					if (eo.metrics != null)
					{
						Element eoMetrics = new Element("Metrics");
						eoMetrics.setText(eo.metrics);
						extractionObject.addContent(eoMetrics);
					}
					if (eo.segmentID != null)
					{
						Element eoSegmentID = new Element("SegmentID");
						eoSegmentID.setText(eo.segmentID);
						extractionObject.addContent(eoSegmentID);
					}
					if (eo.filters != null)
					{
						Element eoFilters = new Element("Filters");
						eoFilters.setText(eo.filters);
						extractionObject.addContent(eoFilters);
					}
					if (eo.includeAllRecords != null)
					{
						Element includeAll = new Element("IncludeAllRecords");
						includeAll.setText(String.valueOf(eo.isIncludeAllRecords()));
						extractionObject.addContent(includeAll);
					}
					if (eo.startDate != null)
					{
						Element eoStartDate = new Element("StartDate");
						eoStartDate.setText(eo.startDate);
						extractionObject.addContent(eoStartDate);
					}
					if (eo.endDate != null)
					{
						Element eoEndDate = new Element("EndDate");
						eoEndDate.setText(eo.endDate);
						extractionObject.addContent(eoEndDate);
					}
					if (eo.samplingLevel != null)
					{
						Element eoSamplingLevel = new Element("SamplingLevel");
						eoSamplingLevel.setText(eo.samplingLevel);
						extractionObject.addContent(eoSamplingLevel);
					}
					if (eo.reportSuiteID != null)
					{
						Element eoReportSuiteID = new Element("ReportSuiteID");
						eoReportSuiteID.setText(eo.reportSuiteID);
						extractionObject.addContent(eoReportSuiteID);
					}
					if (eo.elements != null)
					{
						Element eoElements = new Element("Elements");
						eoElements.setText(eo.elements);
						extractionObject.addContent(eoElements);
					}
					if (eo.pageSize != null)
					{
						Element eoPageSize = new Element("PageSize");
						eoPageSize.setText(eo.pageSize);
						extractionObject.addContent(eoPageSize);
					}
					extractionObjects.addContent(extractionObject);
				}
			}
			connectorConfig.addContent(extractionObjects);
			
			if (this.StartExtractionTime != null)
			{
				Element startExtractionTime = new Element("StartExtractionTime");
				startExtractionTime.setText(dateFormat.format(this.StartExtractionTime));
				connectorConfig.addContent(startExtractionTime);
			}
			if (this.EndExtractionTime != null)
			{
				Element endExtractionTime = new Element("EndExtractionTime");
				endExtractionTime.setText(dateFormat.format(this.EndExtractionTime));
				connectorConfig.addContent(endExtractionTime);
			}
			
			XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
			xmlOutputter.getFormat().setEncoding("utf-8");
			xmlOutputter.getFormat().setOmitDeclaration(false);
			xmlOutputter.getFormat().setOmitEncoding(false);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(configFilePath), "UTF-8"));
			bw.write("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
			bw.newLine();
			xmlOutputter.output(connectorConfig, bw);
			bw.flush();
			bw.close();
			
			if (connectorName.equalsIgnoreCase("sfdc"))
			{
				//save sforce.xml
				String oldConfigFilePath = spaceDirectory + File.separator + "sforce.xml";
				File oldConfigFile = new File(oldConfigFilePath);
				SalesforceSettings sforceSettings = null;
				if (oldConfigFile.exists())
				{
					sforceSettings = SalesforceSettings.getSalesforceSettings(oldConfigFilePath);
				}
				else
				{
					sforceSettings = new SalesforceSettings();
				}
				xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
				xmlOutputter.getFormat().setEncoding("utf-8");
				xmlOutputter.getFormat().setOmitDeclaration(false);
				xmlOutputter.getFormat().setOmitEncoding(false);
				bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(oldConfigFilePath), "UTF-8"));
				bw.write("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
				bw.newLine();
				Element salesforceSettings = new Element("SalesforceSettings");
				salesforceSettings.addNamespaceDeclaration(Namespace.getNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance"));
				salesforceSettings.addNamespaceDeclaration(Namespace.getNamespace("xsd", "http://www.w3.org/2001/XMLSchema"));
				Element version = new Element("version");
				version.setText(String.valueOf(SalesforceSettings.CurrentVersion));
				salesforceSettings.addContent(version);
				if (saveAuthentication)
				{
					EncryptionService enc = EncryptionService.getInstance();
					for (ConnectionProperty cp : this.connectionProperties)
					{
						if (!cp.saveToConfig)
							continue;
						if (cp.name.equals(BaseConnector.SPACE_ID) || cp.name.equals(BaseConnector.SPACE_DIR) 
								|| cp.name.equals(BaseConnector.SPACE_NAME) || cp.name.equals(BaseConnector.SAVE_AUTHENTICATION))
							continue;
						if (cp.name.equals(SFDCConnector.USERNAME))
						{
							Element username = new Element("username");
							username.setText(enc.encrypt(cp.value));
							salesforceSettings.addContent(username);
						}
						else if (cp.name.equals(SFDCConnector.PASSWORD))
						{
							Element password = new Element("password");
							password.setText(enc.encrypt(cp.value));
							salesforceSettings.addContent(password);
						}
						else if (cp.name.equals(BaseConnector.USE_SANDBOX_URL))
						{
							Element sitetype = new Element("sitetype");
							sitetype.setText(Boolean.valueOf(cp.value) ? SiteType.Sandbox.toString() : SiteType.Production.toString());
							salesforceSettings.addContent(sitetype);
						}						
					}					
				}
				Element saveAuthenticationEl = new Element("saveAuthentication");
				saveAuthenticationEl.setText(String.valueOf(saveAuthentication));
				salesforceSettings.addContent(saveAuthenticationEl);
				Element automatic = new Element("automatic");
				automatic.setText(String.valueOf(sforceSettings.automatic));//new config does not use this
				salesforceSettings.addContent(automatic);
				Element objects = new Element("objects");
				if (this.extractionObjects != null)
				{
					for (ExtractionObject eObj : this.extractionObjects)
					{
						Element objElement = new Element("SalesforceObjectConnection");
						Element objName = new Element("name");
						objName.setText(eObj.name);
						objElement.addContent(objName);
						Element objQuery = new Element("query");
						objQuery.setText(eObj.query == null ? "" : eObj.query);
						objElement.addContent(objQuery);
						Element objExtractionGroupNames = new Element(ExtractionObject.EXT_GROUP_NAMES);
						objExtractionGroupNames.setText(eObj.toString());
						objElement.addContent(objExtractionGroupNames);
						if (eObj.lastUpdatedDate != null)
						{
							Element objLastUpdatedDate = new Element("lastUpdatedDate");
							objLastUpdatedDate.setText(sforceSettings.lastUpdatedDateFormat.format(eObj.lastUpdatedDate));
							objElement.addContent(objLastUpdatedDate);
						}
						if (eObj.lastSystemModStamp != null)
						{
							Element objLastSystemModStamp = new Element("lastSystemModStamp");
							objLastSystemModStamp.setText(sforceSettings.lastSystemModStampFormat.format(eObj.lastSystemModStamp));
							objElement.addContent(objLastSystemModStamp);
						}
						if (eObj.columnNames != null)
						{
							Element colNames = new Element("columnNames");
							for (String colName : eObj.columnNames)
							{
								Element colname = new Element("string");
								colname.setText(colName);
								colNames.addContent(colname);
							}
							objElement.addContent(colNames);
						}
						if (eObj.includeAllRecords != null)
						{
							Element includeAllRecords = new Element("includeAllRecords");
							includeAllRecords.setText(String.valueOf(eObj.includeAllRecords));
							objElement.addContent(includeAllRecords);
						}
						objects.addContent(objElement);
					}
				}
				salesforceSettings.addContent(objects);
				if (StartExtractionTime != null)
				{
					Element startExtractionTime = new Element("startExtractionTime");
					startExtractionTime.setText(sforceSettings.dateTimeISOStandardFormat.format(StartExtractionTime));
					salesforceSettings.addContent(startExtractionTime);
				}
				if (EndExtractionTime != null)
				{
					Element endExtractionTime = new Element("endExtractionTime");
					endExtractionTime.setText(sforceSettings.dateTimeISOStandardFormat.format(EndExtractionTime));
					salesforceSettings.addContent(endExtractionTime);
				}
				xmlOutputter.output(salesforceSettings, bw);
				bw.flush();
				bw.close();
			}			
		}
		catch (Exception ex)
		{
			throw new UnrecoverableException(ex.getMessage(), ex);
		}	
	}

	public ExtractionObject findObject(String name) {
		if(name != null && extractionObjects != null && extractionObjects.size() > 0){
			for(ExtractionObject exObject : extractionObjects){
				if(name.equalsIgnoreCase(exObject.name)){
					return exObject;
				}
			}
		}
		return null;
	}
	
	public Map<String, List<ExtractionObject>> getExtractionObjectsForExtractionGroups(List<String> extractionGroups) {
		Map<String, List<ExtractionObject>> map = new HashMap<String, List<ExtractionObject>>();
		if (extractionObjects != null)
		{
			if (extractionGroups == null)
			{
				map.put(null, extractionObjects);
			}
			else
			{
				for (String extractGroup : extractionGroups)
				{
					for (ExtractionObject eobj : extractionObjects)
					{
						if (eobj.getExtractionGroupNames() != null && eobj.getExtractionGroupNames().contains(extractGroup))
						{
							if (!map.containsKey(extractGroup))
							{
								map.put(extractGroup, new ArrayList<ExtractionObject>());
							}
							map.get(extractGroup).add(eobj);
						}
					}
				}
			}
		}
		return map;
	}
}
