package com.successmetricsinc.connectors.sdfc;

import java.util.ArrayList;
import java.util.List;

import com.successmetricsinc.connectors.BaseConnector;
import com.successmetricsinc.connectors.ConnectionProperty;
import com.successmetricsinc.connectors.ConnectorMetaData;
import com.successmetricsinc.connectors.ExtractionObject;

/**
 * 
 * @author mpandit
 *
 */
public class SFDCConnectorMetaData extends ConnectorMetaData {
	private static final String name = "SFDC";
	private static final String interfaceVersion = "1.0";
	private static final String connectorVersion = "1.0";
	private static final String connectorCloudVersion = "26.0";
	private static final List<ConnectionProperty> connectionProperties = fillConnectionProperties();
	private static final List<ExtractionObject.Types> supportedTypes = fillSupportedTypes();
	
	private static List<ConnectionProperty> fillConnectionProperties() {
		List<ConnectionProperty> connectionProperties = getBaseConnectionProperties();
		List<ConnectionProperty> toRemoveList = new ArrayList<ConnectionProperty>();
		for (ConnectionProperty cp : connectionProperties)
		{
			if (cp.name.equalsIgnoreCase(BaseConnector.SOURCEFILE_NAME_PREFIX))
			{
				toRemoveList.add(cp);//No prefix to be used for SFDC
				break;
			}
		}
		connectionProperties.removeAll(toRemoveList);
		ConnectionProperty username = new ConnectionProperty();
		username.name = SFDCConnector.USERNAME;
		username.isRequired = true;
		username.displayIndex = 1;
		username.displayLabel = "Username";
		connectionProperties.add(username);
		ConnectionProperty password = new ConnectionProperty();
		password.name = SFDCConnector.PASSWORD;
		password.isRequired = true;
		password.isSecret = true;
		password.isEncrypted = true;
		password.displayIndex = 2;
		password.displayLabel = SFDCConnector.PASSWORD;
		connectionProperties.add(password);
		ConnectionProperty sfdcClientID = new ConnectionProperty();
		sfdcClientID.name = SFDCConnector.SFDC_CLIENT_ID;
		sfdcClientID.isRequired = false;
		sfdcClientID.saveToConfig = false;
		connectionProperties.add(sfdcClientID);
		ConnectionProperty sandBoxURL = new ConnectionProperty();
		sandBoxURL.name = BaseConnector.SANDBOX_URL;
		sandBoxURL.isRequired = false;
		sandBoxURL.saveToConfig = false;
		connectionProperties.add(sandBoxURL);
		return connectionProperties;
	}
	
	private static List<ExtractionObject.Types> fillSupportedTypes() {
		List<ExtractionObject.Types> types = new ArrayList<ExtractionObject.Types>();
		types.add(ExtractionObject.Types.OBJECT);
		types.add(ExtractionObject.Types.QUERY);
		return types;
	}
	
	
	@Override
	public String getConnectorName() {
		return name;
	}

	@Override
	public String getInterfaceVersion() {
		return interfaceVersion;
	}

	@Override
	public String getConnectorVersion() {
		return connectorVersion;
	}

	@Override
	public String getConnectorCloudVersion() {
		return connectorCloudVersion;
	}

	@Override
	public List<ConnectionProperty> getConnectionProperties() {
		
		return connectionProperties;
	}
	
	@Override
	public List<ExtractionObject.Types> getSupportedTypes() {
		return supportedTypes;
	}

	@Override
	public boolean supportsParallelExtraction() {
		return true;
	}

	@Override
	public boolean allowsSourceFileNamePrefix() {
		return false;
	}
}
