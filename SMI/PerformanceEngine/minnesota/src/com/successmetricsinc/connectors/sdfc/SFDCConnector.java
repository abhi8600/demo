package com.successmetricsinc.connectors.sdfc;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.axiom.om.OMElement;
import org.apache.axis2.Constants;
import org.apache.axis2.client.Options;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.log4j.Logger;

import com.sforce.soap.partner.InvalidFieldFault;
import com.sforce.soap.partner.InvalidIdFault;
import com.sforce.soap.partner.InvalidQueryLocatorFault;
import com.sforce.soap.partner.InvalidSObjectFault;
import com.sforce.soap.partner.LoginFault;
import com.sforce.soap.partner.MalformedQueryFault;
import com.sforce.soap.partner.SforceServiceStub;
import com.sforce.soap.partner.SforceServiceStub.CallOptions;
import com.sforce.soap.partner.SforceServiceStub.DescribeGlobal;
import com.sforce.soap.partner.SforceServiceStub.DescribeGlobalResponse;
import com.sforce.soap.partner.SforceServiceStub.DescribeGlobalResult;
import com.sforce.soap.partner.SforceServiceStub.DescribeGlobalSObjectResult;
import com.sforce.soap.partner.SforceServiceStub.DescribeSObject;
import com.sforce.soap.partner.SforceServiceStub.DescribeSObjectResponse;
import com.sforce.soap.partner.SforceServiceStub.DescribeSObjectResult;
import com.sforce.soap.partner.SforceServiceStub.Field;
import com.sforce.soap.partner.SforceServiceStub.Login;
import com.sforce.soap.partner.SforceServiceStub.LoginResponse;
import com.sforce.soap.partner.SforceServiceStub.LoginResult;
import com.sforce.soap.partner.SforceServiceStub.PackageVersionHeader;
import com.sforce.soap.partner.SforceServiceStub.Query;
import com.sforce.soap.partner.SforceServiceStub.QueryMore;
import com.sforce.soap.partner.SforceServiceStub.QueryMoreResponse;
import com.sforce.soap.partner.SforceServiceStub.QueryOptions;
import com.sforce.soap.partner.SforceServiceStub.QueryResponse;
import com.sforce.soap.partner.SforceServiceStub.QueryResult;
import com.sforce.soap.partner.SforceServiceStub.SessionHeader;
import com.sforce.soap.partner.UnexpectedErrorFault;
import com.successmetricsinc.connectors.BaseConnector;
import com.successmetricsinc.connectors.ConnectorMetaData;
import com.successmetricsinc.connectors.ExtractionObject;
import com.successmetricsinc.connectors.ExtractionProgressHandler;
import com.successmetricsinc.connectors.ExtractionProgressHandler.ExtractionStatus;
import com.successmetricsinc.connectors.IConnector;
import com.successmetricsinc.connectors.RecoverableException;
import com.successmetricsinc.connectors.UnrecoverableException;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.Util;

/**
 * 
 * @author mpandit
 *
 */
public class SFDCConnector extends BaseConnector implements IConnector {
	
	private ConnectorMetaData metaData = new SFDCConnectorMetaData();
	private SessionHeader sessionHeader = null;
	private CallOptions co = null;
	private String username;
	private String password;
	
	private String stubServerUrl;
	
	public static final String USERNAME = "UserName";
	public static final String PASSWORD = "Password";
	public static final String SFDC_CLIENT_ID = "SFDCClientID";
	
	public static final long timeoutInMilliseconds = 10 * 60 * 1000;
	public static Pattern soqlQueryPattern = Pattern.compile("\\s+LIMIT\\s+\\d+\\Z", Pattern.CASE_INSENSITIVE);
	
	private static final Logger logger = Logger.getLogger(SFDCConnector.class);
	
	@Override
	public void connect(Properties props) throws RecoverableException, UnrecoverableException {
		if (!metaData.containsRequiredConnectionProperties(props))
			throw new UnrecoverableException("Required connection properties not found", new Exception("Required connection properties not found"));
		try
		{
			super.connect(props, metaData);
			username = props.getProperty(USERNAME);
			password = props.getProperty(PASSWORD);
			login();						
		}
		catch (Exception ex)
		{
			if(ex instanceof RecoverableException)
				throw (RecoverableException)ex;
			else if(ex instanceof UnrecoverableException)
				throw (UnrecoverableException)ex;
			throw new UnrecoverableException(ex.getMessage(), ex);
		}
		isConnected = true;
	}
	
	@Override
	public final ConnectorMetaData getMetaData() throws UnrecoverableException {
		return metaData;
	}
	
	private void login() throws Exception
	{	
		SforceServiceStub stub = null;
		try {
			if (!useSandBoxURL)
				stub = new SforceServiceStub();
			else
				stub = (sandboxURL == null || sandboxURL.trim().isEmpty() ? new SforceServiceStub() :  new SforceServiceStub(sandboxURL));
		} catch (Exception ex) {
			throw new Exception("SFDCConnector: SFDC web service failure: " + ex.getMessage());
		}
		cleanUpIdleConnections(stub);
		Options options = stub._getServiceClient().getOptions();
		String SFDCClientID = getBirstParameter(SFDC_CLIENT_ID);
		if (SFDCClientID != null && SFDCClientID.trim().length() > 0)
		{
			co = new CallOptions();
			co.setClient(SFDCClientID);            
		}
		options.setProperty(HTTPConstants.MC_ACCEPT_GZIP, Boolean.FALSE);
		options.setProperty(HTTPConstants.MC_GZIP_REQUEST, Boolean.FALSE);
		options.setTimeOutInMilliSeconds(timeoutInMilliseconds);
		options.setProperty(HTTPConstants.AUTO_RELEASE_CONNECTION, Constants.VALUE_TRUE);
		if (username == null || password == null)
		{
			throw new Exception("SFDCConnector: no saved SFDC username/password");
		}
		Login login = new Login();
		login.setUsername(username);
		login.setPassword(password);
		LoginResponse lresp = null;
		try
		{
			lresp = stub.login(login, null, co);
		}
		catch(Exception ex)
		{
			if(doRetryOnFailure(ex)){
				if(stub != null && stub._getServiceClient() != null && stub._getServiceClient().getServiceContext() != null){
					logConfigContextInfo(stub._getServiceClient().getServiceContext().getConfigurationContext());
				}
				logger.warn("Recoverable Exception " + ex.getMessage());
				throw new RecoverableException(ex.getMessage(), ex);
			}
			logger.error(ex.getMessage(), ex);
			if (ex instanceof LoginFault)
			{
				LoginFault fault = (LoginFault)ex;
				com.sforce.soap.partner.SforceServiceStub.LoginFault loginFault = fault.getFaultMessage().getLoginFault();
				String errorMessage = loginFault.getExceptionMessage();
				UnrecoverableException e = new UnrecoverableException(errorMessage, ex);
				e.setErrorCode(BaseException.ERROR_CONNECTOR_LOGIN_FAILURE);
				throw e;				
			}
			throw ex;	
		}
		LoginResult lr	= null;
		lr = lresp.getResult();
		if (lr == null)
		{
			throw new Exception("SFDCConnector: SFDC web service login failure: null LoginResult");
		}
		if (lr.getPasswordExpired())
		{
			logger.warn("SFDC password expired for user " + username + " for space " + spaceID);
			throw new Exception("SFDCConnector: SFDC password expired for user " + username + " for space " + spaceID);
		}
		try
		{
			// now that configContext is created as well as the underlying HttpConnectionManager
			// set the parameters for the connections -- NOTE previous call to login set up the httpConnectionManager
			configureParamsForConnectionManager(stub._getServiceClient().getServiceContext().getConfigurationContext());
			/*
			 * Can't login using zip until after authentication - errors not caught correctly if zip is on
			 */
			options = stub._getServiceClient().getOptions();
			options.setProperty(HTTPConstants.MC_ACCEPT_GZIP, Boolean.TRUE);
			options.setProperty(HTTPConstants.MC_GZIP_REQUEST, Boolean.TRUE);
			options.setTimeOutInMilliSeconds(timeoutInMilliseconds);
			options.setProperty(HTTPConstants.AUTO_RELEASE_CONNECTION, Constants.VALUE_TRUE);
			lresp = stub.login(login, null, co);
			lr = lresp.getResult();
			if (lr == null)
			{
				throw new Exception("SFDCConnector: SFDC web service login failure: null LoginResult");
			}
			// set up the session information (id, endpoint, request options)
			sessionHeader = new SessionHeader();
			sessionHeader.setSessionId(lr.getSessionId());
			stubServerUrl = lr.getServerUrl();
		}
		catch (Exception ex) {
			if(doRetryOnFailure(ex)){
				logger.warn("Recoverable Exception " + ex.getMessage());
				throw new RecoverableException(ex.getMessage(), ex);
			}
			logger.error(ex.getMessage(), ex);
			throw new Exception("SFDCConnector: SFDC web service failure to login: " + ex.getMessage());
		}
		finally{
			if(stub != null){
				Util.cleanUpServiceClient(stub._getServiceClient());
			}			
		}
	}
	
	private void cleanUpIdleConnections(SforceServiceStub stub) {
		if(stub != null && stub._getServiceClient() != null && stub._getServiceClient().getServiceContext() != null && 
				stub._getServiceClient().getServiceContext().getConfigurationContext() != null){
			Util.cleanUpIdleConnections(stub._getServiceClient().getServiceContext().getConfigurationContext());
		}
	}

	private boolean doRetryOnFailure(Exception ex){
		if(ex != null && ex instanceof IOException){
			return true;
		}
		return false;
	}
	
	@Override
	public String[] getCatalog() throws RecoverableException, UnrecoverableException {
		if (!isConnected)
		{
			throw new UnrecoverableException("Not connected to SFDC", new Exception("Not connected to SFDC"));
		}
		String[] objNames = null;
		SforceServiceStub stub = null;
		// The same stub can be used by the same thread over multiple objects
		try
		{		
			stub = createStub();			
			DescribeGlobal describeGlobal = new DescribeGlobal();
			PackageVersionHeader packageVersionHeader = new PackageVersionHeader();
			DescribeGlobalResponse dgrs = stub.describeGlobal(describeGlobal, sessionHeader, co, packageVersionHeader);
			DescribeGlobalResult dgr = dgrs.getResult();
			DescribeGlobalSObjectResult[] sObjects = dgr.getSobjects();
			objNames = new String[sObjects.length];
			for (int i=0; i<sObjects.length; i++)
			{	
				if(sObjects[i].getQueryable())
				{
					objNames[i] = sObjects[i].getName();
				}
			}
		}
		catch(Exception ex)
		{
			if(doRetryOnFailure(ex)){
				logger.warn("Recoverable Exception " + ex.getMessage());
				throw new RecoverableException(ex.getMessage(), ex);
			}
			logger.error("Exception while create sforce stub ", ex);
			throw new UnrecoverableException(ex.getMessage(), ex);
		}	
		finally{
			if(stub != null){
				Util.cleanUpServiceClient(stub._getServiceClient());
			}
		}
		return objNames;
	}
	
	@Override
	public int fetchData(Writer writer, ExtractionObject type, ExtractionProgressHandler hndlr) throws RecoverableException, UnrecoverableException
	{
		if (!isConnected)
		{
			throw new UnrecoverableException("Not connected to SFDC", new Exception("Not connected to SFDC"));
		}
		SforceServiceStub stub = null;
		try
		{		
			stub = createStub();
		}
		catch(Exception ex)
		{
			logger.error("Exception while creating sforce stub ", ex);
			throw new UnrecoverableException(ex.getMessage(), ex);
		}
		logger.info("Starting extract for object : " + type.name);
		long startTime = System.currentTimeMillis();
		logger.debug("Space " + spaceDir + " - object retrieval process started for SFDC object: " + type.name);
		int numRecords = 0;
		Date maxLastModified = new Date(minDateValue.getTime());
		StringBuilder query = new StringBuilder();
		try
    	{
	        boolean first = true;
	        int lastModifiedField = -1;
	        DescribeSObjectResult dresult = null;
	        if (type.type == ExtractionObject.Types.OBJECT)
	        {
	        	DescribeSObject dsObject = new DescribeSObject();
	        	dsObject.setSObjectType(type.name);
	        	DescribeSObjectResponse dResponse = stub.describeSObject(dsObject, sessionHeader, co, null, null);
	        	if (dResponse != null)
	        	{
	        		dresult = dResponse.getResult();
	        		query.append("SELECT ");
	                for (Field f : dresult.getFields())
	                {
	                    if (first)
	                        first = false;
	                    else
	                        query.append(',');
	                    query.append(f.getName());
	                }
	                query.append(" FROM " + type.name);
	                for (int i = 0; i < dresult.getFields().length; i++)
	                {
	                    Field f = dresult.getFields()[i];
	                    if (f.getName().equals("SystemModstamp"))
	                    {
	                        lastModifiedField = i;
	                        break;
	                    }
	                }
	                if (type.lastSystemModStamp != null && !type.lastSystemModStamp.equals(minDateValue) && lastModifiedField >= 0 && type.useLastModified)
	                {
	                    // Incremental load only
	                    query.append(" WHERE SystemModstamp > " + new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss'Z'").format(type.lastSystemModStamp));
	                }
	                writeMappingFile(type.name, dresult);
	                type.columnMappings = getMapping(dresult);
	        	}                	
	        }
	        else if (type.type == ExtractionObject.Types.QUERY && type.query != null && !type.query.trim().isEmpty())
	        {
	            query.append(type.query);
	        }
	        else
	        {
	        	throw new UnrecoverableException("Unsupported object type : " + type.type.toString(), new Exception("Unsupported object type : " + type.type.toString()));
	        }
	        String soqlQuery = query.toString();	        
	        writeSFDCQuery(type.name, dresult, soqlQuery);
	        Query q = new Query();
	        q.setQueryString(soqlQuery);
	        QueryOptions qOptions = new QueryOptions();
	        qOptions.setBatchSize(2000);
	        type.lastUpdatedDate = new Date();
	        QueryResponse qResp = stub.query(q, sessionHeader, co, qOptions, null, null);
	        QueryResult qr = null;
	        if (qResp != null)
	        {
	        	qr = qResp.getResult();
	        }
	        if (qr != null && qr.getSize() > 0)
	        {	        	
        		first = true;
        		List<String> cnames = new ArrayList<String>();
        		for (OMElement e : qr.getRecords()[0].getExtraElement())
        		{
        			if (first)
                        first = false;
                    else
                        writer.write('|');
        			String n = getAllowable(e.getLocalName(), false);        			
                    int count = 0;
                    // Fix to deal with duplicate column names (which SFDC allows)
                    while (cnames.contains(count == 0 ? n : (n + count)))
                    {
                        count++;
                        if (count > 100)
                            break;
                    }
                    n = count == 0 ? n : (n + count);
                    cnames.add(n);
                    writer.write(n);
                    type.addColumnName(n);
        		}
        		writer.write(System.getProperty("line.separator"));
        		boolean done = false;
        		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                while (!done)
                {
                	for (int i = 0; i < qr.getRecords().length; i++)
                    {
                		if(hndlr != null && !hndlr.shouldContinue()){
                    		hndlr.updateProgress(ExtractionStatus.Killed, numRecords);
                    		throw new UnrecoverableException(type.name + " Extraction Killed", new Exception(type.name + " Extraction Killed"));
                    	}
                        first = true;
                        for (int j = 0; j < qr.getRecords()[i].getExtraElement().length; j++)
                        {
                            if (first)
                                first = false;
                            else
                                writer.write('|');
                            String s = qr.getRecords()[i].getExtraElement()[j].getText();
                            if (s.contains("\r") || s.contains("\n") || s.contains("\"") || s.contains("|") || s.contains("\\"))
                            	writer.write('"' + s.replace("\\", "\\\\").replace("\"", "\\\"").replace("|", "\\|") + '"');
                            else
                                writer.write(s);
                        }
                        if (lastModifiedField >= 0)
                        {
                            try
                            {
                                Date dt = sdf.parse(qr.getRecords()[i].getExtraElement()[lastModifiedField].getText());
                                if (dt.after(maxLastModified))
                                    maxLastModified = dt;
                            }
                            catch (ParseException pe)
                            {
                            	logger.error("Cannot parse lastModifiedField : " + qr.getRecords()[i].getExtraElement()[lastModifiedField].getText());
                            }
                        }
                        writer.write(System.getProperty("line.separator"));
                        numRecords++;
                        if (hndlr != null && (numRecords % hndlr.getThresholdRecordCount() == 0))
                        {
                        	hndlr.updateProgress(ExtractionStatus.Running, numRecords);
                        }                        	
                    }
                	done = qr.getDone();
                	if (!done)
                    {
                    	QueryMore qMore = new QueryMore();
                    	qMore.setQueryLocator(qr.getQueryLocator());
                    	QueryMoreResponse qmResp = stub.queryMore(qMore, sessionHeader, co, qOptions);
                    	if (qmResp != null)
                    	{
                    		qr = qmResp.getResult();
                    	}
                    }
                }
                type.lastSystemModStamp = maxLastModified;
	        }
	        else
	        {
	        	logger.info("No data returned for SFDC object " + type.name + " - " + query + " (creating empty stub file)");
	        }
        }
    	catch (Exception e)
    	{
    		logger.error("Problem extracting SFDC object - " + type.name + " - " + query);
    		if (e instanceof IOException || e instanceof InvalidQueryLocatorFault)
    		{
    			throw new RecoverableException(e.getMessage() + ": " + query, e);
    		}else
    		{
    			if (hndlr != null && hndlr.getLoggedStatus() != ExtractionStatus.Killed)
    				hndlr.updateProgress(ExtractionStatus.Failed, numRecords);
    			
    			if (e instanceof MalformedQueryFault)
    			{
    				MalformedQueryFault queryFault = (MalformedQueryFault)e;
    				String message = queryFault.getFaultMessage().getMalformedQueryFault().getExceptionMessage();
    				UnrecoverableException ex = new UnrecoverableException(message, e);
    				ex.setErrorCode(BaseException.ERROR_CONNECTOR_INVALID_QUERY);
    				throw ex;
    			}
    			else if (e instanceof InvalidFieldFault)
    			{
    				InvalidFieldFault invalidFieldFault = (InvalidFieldFault)e;
    				String message = invalidFieldFault.getFaultMessage().getInvalidFieldFault().getExceptionMessage();
    				UnrecoverableException ex = new UnrecoverableException(message, e);
    				ex.setErrorCode(BaseException.ERROR_CONNECTOR_INVALID_QUERY);
    				throw ex;
    			}
    			else if (e instanceof InvalidSObjectFault)
    			{
    				InvalidSObjectFault invalidSObjectFault = (InvalidSObjectFault)e;
    				String message = invalidSObjectFault.getFaultMessage().getInvalidSObjectFault().getExceptionMessage();
    				UnrecoverableException ex = new UnrecoverableException(message, e);
    				ex.setErrorCode(BaseException.ERROR_CONNECTOR_INVALID_QUERY);
    				throw ex;
    			}
    			else if (e instanceof UnexpectedErrorFault)
    			{
    				UnexpectedErrorFault unexpectedErrorFault = (UnexpectedErrorFault)e;
    				String message = unexpectedErrorFault.getFaultMessage().getUnexpectedErrorFault().getExceptionMessage();
    				UnrecoverableException ex = new UnrecoverableException(message, e);
    				ex.setErrorCode(BaseException.ERROR_OTHER);
    				throw ex;
    			}
    			else if (e instanceof InvalidIdFault)
    			{
    				InvalidIdFault invalidIdFault = (InvalidIdFault)e;
    				String message = invalidIdFault.getFaultMessage().getInvalidIdFault().getExceptionMessage();
    				UnrecoverableException ex = new UnrecoverableException(message, e);
    				ex.setErrorCode(BaseException.ERROR_CONNECTOR_INVALID_QUERY);
    				throw ex;
    			}
    			UnrecoverableException ex = new UnrecoverableException(e.getMessage() + ": " + type.query, e);    			
    			throw ex;
    		}
    	}
		finally
		{
			if(stub != null){
				Util.cleanUpServiceClient(stub._getServiceClient());
			}
		}
		long timeTakenInSeconds = (System.currentTimeMillis() - startTime);
		logger.info("Successfully extracted object " + type.name + " : Total Time taken : " + timeTakenInSeconds + "(milliseconds)");
		type.status = ExtractionObject.SUCCESS;
		if (hndlr != null)
			hndlr.updateProgress(ExtractionStatus.Complete, numRecords);
		return numRecords;				
	}
	
	private String getSFDCObjectMappingFilePath(String objectName)
	{
		String spaceLogsDirectory = spaceDir + File.separator + "mapping";
        File f = new File(spaceLogsDirectory);
        if (!f.exists() || !f.isDirectory())
        {
            f.mkdir();
        }
        return (spaceLogsDirectory + File.separator + objectName + "-mapping.txt");
	}
	
	private void writeMappingFile(String objectName, DescribeSObjectResult response) throws IOException {
		if(response != null)
		{
			BufferedWriter writer = null;
			String objectMappingFileName = null;
			try
			{
				objectMappingFileName = getSFDCObjectMappingFilePath(objectName);
				writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(objectMappingFileName, false), "UTF-8"));
				boolean first = true;
				for (Field f : response.getFields())
				{
					if(!first)
					{
						writer.write(',');
					}
					
					writer.write(f.getName() + "=" + f.getLabel() + "=" + f.getType().getValue().equals("id"));
					if(first)
						first = false;
				}
				logger.debug("Successfully written mapping file : " + objectMappingFileName);
			}finally
			{
				try
				{
					if(writer != null)
						writer.close();
				}
				catch(Exception ex2)
				{
					logger.warn("Error while closing writer for " + objectMappingFileName);
				}
			}
		}
	}
	
	private String getSFDCQueryLogFilePath()
	{
		String spaceLogsDirectory = spaceDir + File.separator + "logs";
        File f = new File(spaceLogsDirectory);
        if (!f.exists() || !f.isDirectory())
        {
            f.mkdir();
        }
        return (spaceLogsDirectory + File.separator + "sfdcQueryLog.txt");
	}
	
	synchronized private void writeSFDCQuery(String objectName, DescribeSObjectResult describeObjectResult, String query) throws Exception
    {
        BufferedWriter writer = null;
        String queryLogFileName = null;
        try
        {
            queryLogFileName = getSFDCQueryLogFilePath();                
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(queryLogFileName, true), "UTF-8"));

            writer.write("");
            writer.newLine();
            writer.write("Object Name : " +  objectName);
            writer.newLine();
            writer.write("Time : " + new Date().toString());
            writer.newLine();
            writer.write("Query :");
            writer.newLine();
            writer.write(query);                
            
            if(describeObjectResult != null)
            {
                writer.write("Field Descriptions : (Name, Label, Type, Length, Precision, Scale)");
                for (Field field : describeObjectResult.getFields())
                {                        
                    writer.write(field.getName());
                    writer.write(",");
                    writer.write(field.getLabel());
                    writer.write(",");
                    writer.write(field.getType().toString());                        
                    writer.write(",");
                    writer.write(field.getLength());
                    writer.write(",");
                    writer.write(field.getPrecision());
                    writer.write(",");
                    writer.write(field.getScale());
                    writer.newLine();
                }
            }            
        }
        catch (Exception ex)
        {
            logger.error("Exception in writing query information to the " + queryLogFileName, ex);
            throw new Exception("SFDCDataExtractor: Exception in writing query information to the " + queryLogFileName);
        }
        finally
        {
            if (writer != null)
            {
                try
                {
                    writer.close();
                }
                catch (Exception ex2)
                {
                    logger.warn("Unable to close writer for " + queryLogFileName, ex2);
                }
            }
        }
    }
	
	/**
	 * create a new instance of SforceServiceStub
	 * @throws Exception
	 */
	private synchronized SforceServiceStub createStub() throws Exception
	{
		SforceServiceStub stub = new SforceServiceStub(stubServerUrl);
		Options options = stub._getServiceClient().getOptions();
		options.setProperty(HTTPConstants.MC_ACCEPT_GZIP, Boolean.TRUE);
		options.setProperty(HTTPConstants.MC_GZIP_REQUEST, Boolean.TRUE);
		options.setTimeOutInMilliSeconds(timeoutInMilliseconds);
		options.setProperty(HTTPConstants.AUTO_RELEASE_CONNECTION, Constants.VALUE_TRUE);
		return stub;
	}

	@Override
	public void fillObjectDetails(ExtractionObject type) throws RecoverableException, UnrecoverableException{
		if (!isConnected)
		{
			throw new UnrecoverableException("Not connected to SFDC", new Exception("Not connected to SFDC"));
		}
		SforceServiceStub stub = null;
		try
		{		
			stub = createStub();
		}
		catch(Exception ex)
		{
			logger.error("Exception while creating sforce stub ", ex);
			throw new UnrecoverableException(ex.getMessage(), ex);
		}
		logger.info("Get details for object : " + type.name);
		
		StringBuilder query = new StringBuilder();
		StringBuilder labelsMapping = new StringBuilder();
		try
    	{
	        boolean first = true;
	        DescribeSObjectResult dresult = null;
	        DescribeSObject dsObject = new DescribeSObject();	        
        	dsObject.setSObjectType(type.name);
        	DescribeSObjectResponse dResponse = stub.describeSObject(dsObject, sessionHeader, co, null, null);
        	if (dResponse != null)
        	{
        		dresult = dResponse.getResult();
        		query.append("SELECT ");
                for (Field f : dresult.getFields())
                {
                    if (first)
                        first = false;
                    else
                    {
                        query.append(',');
                        labelsMapping.append(',');
                    }
                    
                    query.append(f.getName());
                    labelsMapping.append(f.getName() + "=" + f.getLabel() + "=" + f.getType().getValue().equals("id"));
                }
                query.append(" FROM " + type.name);
        	}
    	}
		catch(Exception e)
		{
			if(doRetryOnFailure(e)){
				logger.warn("Recoverable Exception " + e.getMessage());
				throw new RecoverableException(e.getMessage(), e);
			}
    		logger.error("Problem getting details of SFDC object - " + type.name);
    		if (e instanceof InvalidSObjectFault)
			{
				InvalidSObjectFault invalidSObjectFault = (InvalidSObjectFault)e;
				String message = invalidSObjectFault.getFaultMessage().getInvalidSObjectFault().getExceptionMessage();
				UnrecoverableException ex = new UnrecoverableException(message, e);
				ex.setErrorCode(BaseException.ERROR_CONNECTOR_INVALID_QUERY);
				throw ex;
			}
			else if (e instanceof UnexpectedErrorFault)
			{
				UnexpectedErrorFault unexpectedErrorFault = (UnexpectedErrorFault)e;
				String message = unexpectedErrorFault.getFaultMessage().getUnexpectedErrorFault().getExceptionMessage();
				UnrecoverableException ex = new UnrecoverableException(message, e);
				ex.setErrorCode(BaseException.ERROR_OTHER);
				throw ex;
			}
    		UnrecoverableException ex = new UnrecoverableException(e.getMessage() + ": " + query, e);
			throw ex;
		}
		finally
		{
			if(stub != null){
				Util.cleanUpServiceClient(stub._getServiceClient());
			}
		}
		type.query = query.toString();
		type.columnMappings = labelsMapping.toString();
	}

		private String getMapping(DescribeSObjectResult response) throws IOException {
		if(response != null)
		{
			StringBuilder sb = new StringBuilder();
			boolean first = true;
			for (Field f : response.getFields())
			{
				if(!first)
				{
					sb.append(',');
				}

				sb.append(f.getName() + "=" + f.getLabel() + "=" + f.getType().getValue().equals("id"));
				if(first)
					first = false;
			}
			return sb.toString();
		}
		return null;
	}

	@Override
	public boolean validateQueryForObject(ExtractionObject type)
			throws RecoverableException, UnrecoverableException {
		if (!isConnected)
		{
			throw new UnrecoverableException("Not connected to SFDC", new Exception("Not connected to SFDC"));
		}
		if (type.type != ExtractionObject.Types.QUERY || type.query == null || type.query.trim().isEmpty())
		{
			throw new UnrecoverableException("Not a valid SFDC Query Object", new Exception("Not a valid SFDC Query Object"));
		}
		SforceServiceStub stub = null;
		try
		{		
			stub = createStub();
		}
		catch(Exception ex)
		{
			logger.error("Exception while creating sforce stub ", ex);
			throw new UnrecoverableException(ex.getMessage(), ex);
		}
		try
		{
			String soqlQuery = type.query;
			Matcher matcher = soqlQueryPattern.matcher(soqlQuery);
			if (!matcher.find())
			{
				soqlQuery = soqlQuery + " LIMIT 0";
			}
			Query q = new Query();
	        q.setQueryString(soqlQuery);
	        QueryOptions qOptions = new QueryOptions();
	        qOptions.setBatchSize(2000);
	        QueryResponse qResp = stub.query(q, sessionHeader, co, qOptions, null, null);
	        if (qResp != null)
	        {
	        	qResp.getResult();
	        	return true;
	        }	        
		}
		catch (Exception e)
		{
			if (e instanceof MalformedQueryFault)
			{
				MalformedQueryFault queryFault = (MalformedQueryFault)e;
				String message = queryFault.getFaultMessage().getMalformedQueryFault().getExceptionMessage();
				UnrecoverableException ex = new UnrecoverableException(message, e);
				ex.setErrorCode(BaseException.ERROR_CONNECTOR_INVALID_QUERY);
				throw ex;
			}
			else if (e instanceof InvalidFieldFault)
			{
				InvalidFieldFault invalidFieldFault = (InvalidFieldFault)e;
				String message = invalidFieldFault.getFaultMessage().getInvalidFieldFault().getExceptionMessage();
				UnrecoverableException ex = new UnrecoverableException(message, e);
				ex.setErrorCode(BaseException.ERROR_CONNECTOR_INVALID_QUERY);
				throw ex;
			}
			else if (e instanceof InvalidSObjectFault)
			{
				InvalidSObjectFault invalidSObjectFault = (InvalidSObjectFault)e;
				String message = invalidSObjectFault.getFaultMessage().getInvalidSObjectFault().getExceptionMessage();
				UnrecoverableException ex = new UnrecoverableException(message, e);
				ex.setErrorCode(BaseException.ERROR_CONNECTOR_INVALID_QUERY);
				throw ex;
			}
			else if (e instanceof UnexpectedErrorFault)
			{
				UnexpectedErrorFault unexpectedErrorFault = (UnexpectedErrorFault)e;
				String message = unexpectedErrorFault.getFaultMessage().getUnexpectedErrorFault().getExceptionMessage();
				UnrecoverableException ex = new UnrecoverableException(message, e);
				ex.setErrorCode(BaseException.ERROR_CONNECTOR_INVALID_QUERY);
				throw ex;
			}
			else if (e instanceof InvalidIdFault)
			{
				InvalidIdFault invalidIdFault = (InvalidIdFault)e;
				String message = invalidIdFault.getFaultMessage().getInvalidIdFault().getExceptionMessage();
				UnrecoverableException ex = new UnrecoverableException(message, e);
				ex.setErrorCode(BaseException.ERROR_CONNECTOR_INVALID_QUERY);
				throw ex;
			}
			UnrecoverableException ex = new UnrecoverableException(e.getMessage() + ": " + type.query, e);
			throw ex;
		}
		finally
		{
			if(stub != null){
				Util.cleanUpServiceClient(stub._getServiceClient());
			}
		}
		return false;
	}
	
	@Override
	public boolean validateObject(ExtractionObject type)
			throws RecoverableException, UnrecoverableException {
		return false;
	}
}
