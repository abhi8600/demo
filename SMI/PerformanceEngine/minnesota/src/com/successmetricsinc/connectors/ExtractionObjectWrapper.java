package com.successmetricsinc.connectors;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ExtractionObjectWrapper {

	public ExtractionObject extractionObject;
	public String connectorType;
	public List<CloudConnection> cloudConnections = null;
	public List<ExtractionGroup> extractionGroups = null;
	public Date dateCreated;
	public Date dateModified;
	public String createdBy;
	public String modifiedBy;
	
	public ExtractionObjectWrapper() {
		extractionObject = new ExtractionObject();
		cloudConnections = new ArrayList<CloudConnection>();
		extractionGroups = new ArrayList<ExtractionGroup>();
	}

	public ExtractionObject getExtractionObject() {
		return extractionObject;
	}

	public void setExtractionObject(ExtractionObject object) {
		this.extractionObject = object;
	}

	public String getConnectorType() {
		return connectorType;
	}

	public void setConnectorType(String connectorType) {
		this.connectorType = connectorType;
	}

	public List<CloudConnection> getCloudConnections() {
		return cloudConnections;
	}

	public void setCloudConnections(List<CloudConnection> cloudConnections) {
		this.cloudConnections = cloudConnections;
	}

	public List<ExtractionGroup> getExtractionGroups() {
		return extractionGroups;
	}

	public void setExtractionGroups(List<ExtractionGroup> extractionGroups) {
		this.extractionGroups = extractionGroups;
	}
	
	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateModified() {
		return dateModified;
	}

	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public void addExtractionGroup(ExtractionGroup eGroup)
	{
		if (extractionGroups == null)
		{
			extractionGroups = new ArrayList<ExtractionGroup>();
		}
		extractionGroups.add(eGroup);		
	}
	
	public void addCloudConnection(CloudConnection conn)
	{
		if (cloudConnections == null)
		{
			cloudConnections = new ArrayList<CloudConnection>();
		}
		cloudConnections.add(conn);		
	}
}
