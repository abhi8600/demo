package com.successmetricsinc.connectors;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * 
 * @author mpandit
 *
 */
public abstract class ConnectorMetaData {
	public abstract String getConnectorName();      // NetSuite, SalesForce, etc
    public abstract String getInterfaceVersion(); // like JDBC version
    public abstract String getConnectorVersion(); // like JDBC driver version
    public abstract String getConnectorCloudVersion(); // like 25.0 for SFDC
    
    public abstract List<ExtractionObject.Types> getSupportedTypes();//returns list of object types supported (eg. object/query/filteredquery/savedquery)
    
    // props could be username and password (and account for netsuite), UI uses them to drive what fields need to be filled in/saved
    public abstract List<ConnectionProperty> getConnectionProperties();
    
    public abstract boolean supportsParallelExtraction();
    public abstract boolean allowsSourceFileNamePrefix();//false only for SFDC for backward compatibility
    
    protected static List<ConnectionProperty> getBaseConnectionProperties() {
		List<ConnectionProperty> connectionProperties = new ArrayList<ConnectionProperty>();
		ConnectionProperty sourceFileNamePrefix = new ConnectionProperty();
		sourceFileNamePrefix.name = BaseConnector.SOURCEFILE_NAME_PREFIX;
		sourceFileNamePrefix.isRequired = false;
		sourceFileNamePrefix.displayIndex = 6;
		sourceFileNamePrefix.displayLabel = "Source File Prefix";
		connectionProperties.add(sourceFileNamePrefix);
		ConnectionProperty useSandBoxURL = new ConnectionProperty();
		useSandBoxURL.name = BaseConnector.USE_SANDBOX_URL;
		useSandBoxURL.isRequired = false;
		useSandBoxURL.displayIndex = 3;
	    useSandBoxURL.displayLabel = "Environment";
		connectionProperties.add(useSandBoxURL);
		ConnectionProperty saveAuthentication = new ConnectionProperty();
		saveAuthentication.name = BaseConnector.SAVE_AUTHENTICATION;
		saveAuthentication.isRequired = true;
		connectionProperties.add(saveAuthentication);
		return connectionProperties;
	}
    
    public boolean containsRequiredConnectionProperties(Properties props) {
		if (props == null)
			return false;
		for (ConnectionProperty property : getConnectionProperties())
		{
			if (property.isRequired && !props.containsKey(property.name))
				return false;			
		}
		return true;
	}
}
