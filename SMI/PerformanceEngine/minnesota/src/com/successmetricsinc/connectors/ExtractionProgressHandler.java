package com.successmetricsinc.connectors;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.util.Util;

/**
 * 
 * @author mpandit
 *
 */
public class ExtractionProgressHandler {
	
	private static final Logger logger = Logger.getLogger(ExtractionProgressHandler.class);
	private int thresholdRecordCount;
	private String statusFileName;
	private ExtractionStatus loggedStatus;
	private Repository r;
	
	public enum ExtractionStatus {
		Running, Complete, Failed, Killed
	}
	
	public ExtractionProgressHandler(Repository r, int thresholdRecordCount, String statusFileName) throws UnrecoverableException
	{		
		this.r = r;
		this.thresholdRecordCount = thresholdRecordCount;
		this.statusFileName = statusFileName;
		deleteFileForcibly();//clear earlier status file
	}
	
	public int getThresholdRecordCount()
	{
		return thresholdRecordCount;
	}
	
	public void updateFailedProgress() throws UnrecoverableException
	{
		updateProgress(ExtractionStatus.Failed, -1);
	}
	
	public void updateProgress(ExtractionStatus status, int recordsExtracted) throws UnrecoverableException
	{	
		String line = null;
		switch (status)
		{
			case Running:
				line = "Running\t" + recordsExtracted;
				break;
			case Complete:
				line = "Complete\t" + recordsExtracted;
				break;
			case Failed:
				line = "Failed\t" + recordsExtracted;
				break;
			case Killed:
				line = "Killed\t" + recordsExtracted;
				break;	
		}
		boolean isWritten = false;
		int noRetries = 0;
		int maxRetries = 10;
		do
		{
			try
			{
				writeStatus(line);
				isWritten = true;
			}
			catch (UnrecoverableException ex)
			{
				//retrying after 2 seconds
				try
				{
					Thread.sleep(2000);
				}
				catch (InterruptedException ie)
				{						
				}
			}
		} while(!isWritten && noRetries++ < maxRetries);
		if (!isWritten)
		{
			logger.warn("Unable to write status file - " + statusFileName);
		}
		this.loggedStatus = status;
	}
	
	public ExtractionStatus getLoggedStatus()
	{
		return this.loggedStatus;
	}
	
	private void writeStatus(String line) throws UnrecoverableException
	{
		FileWriter writer = null;
		try
		{
			File file = new File(statusFileName);
			if(!file.exists()){
				file.createNewFile();
			}
			writer = new FileWriter(file);
			if (line!=null)
			{
				writer.write(line);
			}
			writer.flush();
		}
		catch (IOException ex)
		{
			throw new UnrecoverableException(ex.getMessage(), ex);
		}
		finally
		{
			if (writer != null)
			{
				try
				{
					writer.close();
				}
				catch (IOException ex)
				{
					logger.warn("Unable to close status file - " + statusFileName, ex);
				}
			}
		}
	}
	
	private void deleteFileForcibly() throws UnrecoverableException
	{
		File file = new File(statusFileName);
		if (file.exists())
		{
			int noRetries = 0;
			int maxRetries = 10;
			boolean isDeleted = false;
			do
			{
				try
				{
					file.delete();
					isDeleted = true;					
				}
				catch (Exception ex)
				{	
					//retrying after 2 seconds
					try
					{
						Thread.sleep(2000);
					}
					catch (InterruptedException ie)
					{						
					}
				}
			} while(!isDeleted && noRetries++ < maxRetries);
			if (!isDeleted)
			{
				logger.warn("Unable to delete status file - " + statusFileName);
			}
		}
	}
	
	public boolean shouldContinue()
	{
		String killLockFileName = null;
		try
		{
			killLockFileName = Util.getKillFileName(r.getRepositoryRootPath(), Util.KILL_EXTRACT_FILE);
			return !Util.isTerminated(killLockFileName);
		}
		catch(Exception ex)
		{
			logger.warn("Error in looking for kill lock file " + killLockFileName);
		}
		return true;
	}
}
