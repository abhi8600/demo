package com.successmetricsinc.connectors;

import java.util.Date;
import java.util.List;

public class CloudConnection {
	private String name;
	private String guid;
	private String connectorType;
	private String connectorAPIVersion;
	private List<ConnectionProperty> connectionProperties;
	public Date dateCreated;
	public Date dateModified;
	public String createdBy;
	public String modifiedBy;
	private Date StartExtractionTime;
	private Date EndExtractionTime;
	private String migratedConnection;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
	public String getConnectorType() {
		return connectorType;
	}
	public void setConnectorType(String connectorType) {
		this.connectorType = connectorType;
	}
	public String getConnectorAPIVersion() {
		return connectorAPIVersion;
	}
	public void setConnectorAPIVersion(String connectorAPIVersion) {
		this.connectorAPIVersion = connectorAPIVersion;
	}
	public List<ConnectionProperty> getConnectionProperties() {
		return connectionProperties;
	}
	public void setConnectionProperties(
			List<ConnectionProperty> connectionProperties) {
		this.connectionProperties = connectionProperties;
	}
	
	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateModified() {
		return dateModified;
	}

	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	public Date getStartExtractionTime() {
		return StartExtractionTime;
	}
	
	public void setStartExtractionTime(Date startExtractionTime) {
		StartExtractionTime = startExtractionTime;
	}
	
	public Date getEndExtractionTime() {
		return EndExtractionTime;
	}
	
	public void setEndExtractionTime(Date endExtractionTime) {
		EndExtractionTime = endExtractionTime;
	}
	
	public String getMigratedConnection() {
		return migratedConnection;
	}
	
	public void setMigratedConnection(String migratedConnection) {
		this.migratedConnection = migratedConnection;
	}
}
