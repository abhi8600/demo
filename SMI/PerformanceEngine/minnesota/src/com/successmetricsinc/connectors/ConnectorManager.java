package com.successmetricsinc.connectors;

import com.successmetricsinc.connectors.netsuite.NetSuiteConnector;
import com.successmetricsinc.connectors.sdfc.SFDCConnector;


/**
 * 
 * @author mpandit
 *
 */
public abstract class ConnectorManager {
	
	/**
	 * returns an instance of IConnector for the specific 'connector', URL could be: connector:birst://netsuite
	 * @param url
	 * @return
	 * @throws UnrecoverableException
	 */
    public static IConnector getConnector(String url) throws UnrecoverableException
    { 
    	if (url == null || url.isEmpty() || !url.startsWith("connector:birst://"))
    		throw new UnrecoverableException("Unrecognized url: " + url, new Exception("Unrecognized url: " + url));
    	String connectorUrl = url.substring("connector:birst://".length());
    	IConnector connector = null;
    	switch (connectorUrl)
    	{
    		case "sfdc"		:
    			connector = new SFDCConnector();
    			break;
    		case "netsuite"	:
    			connector = new NetSuiteConnector();
    			break;
    		default			:
    			throw new UnrecoverableException("No suitable connector found for url: " + url, new Exception("No suitable connector found for url: " + url));
    	}
    	return connector;
    }    
}
