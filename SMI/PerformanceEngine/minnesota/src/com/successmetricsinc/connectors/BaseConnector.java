package com.successmetricsinc.connectors;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Properties;

import org.apache.axis2.context.ConfigurationContext;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.log4j.Logger;

import com.successmetricsinc.connectors.sdfc.SFDCConnector;
import com.successmetricsinc.util.Util;

public abstract class BaseConnector implements IConnector {

	private static final Logger logger = Logger.getLogger(BaseConnector.class);
	public static final String SOURCEFILE_NAME_PREFIX = "SourceFileNamePrefix";
	public static final String SPACE_DIR = "SpaceDir";
	public static final String SPACE_NAME = "SpaceName";
	public static final String SPACE_ID = "SpaceID";
	public static final String SAVE_AUTHENTICATION = "SaveAuthentication";
	public static final String USE_SANDBOX_URL = "UseSandBoxURL";
	public static final String SANDBOX_URL = "SandboxURL";
	//for any BIRST specific parameters such as "SFDCClientID=xyz,BirstUserName=birst,BirstPassword=password"
	public static final String BIRST_PARAMETERS = "BirstParameters";		
	
	protected String SourceFileNamePrefix = "";
	protected String spaceDir;
	protected String spaceID;
	protected String spaceName;
	protected boolean useSandBoxURL;
	protected String sandboxURL;
	protected String birstParameters;
	
	protected boolean isConnected = false;
	
	// used for configuration axis client.
	public static final int MAX_CONNECTIONS_PER_HOST = 50;
	public static final int MAX_TOTAL_CONNECTIONS = 100;
	
	public static String dateTimeISOStandardFormatPattern = "yyyy-MM-dd'T'HH:mm:ss";
	protected static final Date minDateValue;
	
	static
	{
		Calendar c = Calendar.getInstance();
		c.set(1, 0, 1, 0, 0, 0);
		c.set(Calendar.MILLISECOND, 0);
		minDateValue = c.getTime();
	}
	
	public void connect(Properties props, ConnectorMetaData metaData) throws RecoverableException, UnrecoverableException
	{
		spaceDir = props.getProperty(SPACE_DIR);
		spaceID = props.getProperty(SPACE_ID);
		spaceName = props.getProperty(SPACE_NAME);
		sandboxURL = props.getProperty(SANDBOX_URL);
		birstParameters = props.getProperty(BIRST_PARAMETERS);
		useSandBoxURL = Boolean.parseBoolean(props.getProperty(USE_SANDBOX_URL));
		//DO NOT use sourcefile prefix for backward compatibility for SFDC sources
		if (metaData.allowsSourceFileNamePrefix())
			SourceFileNamePrefix = props.getProperty(SOURCEFILE_NAME_PREFIX); 
	}
	
	public String getBirstParameter(String paramName) {
		if (birstParameters == null)
			return null;
		String[] params = birstParameters.split(",");
		for (String parameter : params)
		{
			String[] param = parameter.split("=");
			if (param[0].equalsIgnoreCase(paramName))
				return param[1];
		}
		return null;
	}
	
	public String getSourceFileNamePrefix()
	{
		return SourceFileNamePrefix;
	}
	
	 /**
	 * values set on the configurationContext is used for all the 
	 * service clients that are generated for various requests by calling
	 * unless overriden at the serviceClient level. Any changes to this method should
	 * also be reflected in Util.configureParamsForConnectionManager also 
	 * {@link #createStub()}createStub
	 * @param configurationContext
	 */	
	protected void configureParamsForConnectionManager(ConfigurationContext configContext) {
		if(configContext != null)
		{
			Object obj = configContext.getProperty(HTTPConstants.MULTITHREAD_HTTP_CONNECTION_MANAGER);
			if(obj != null)
			{
				HttpConnectionManagerParams params = new HttpConnectionManagerParams();
				params.setDefaultMaxConnectionsPerHost(MAX_CONNECTIONS_PER_HOST);
				params.setMaxTotalConnections(MAX_TOTAL_CONNECTIONS);
				MultiThreadedHttpConnectionManager connManager = (MultiThreadedHttpConnectionManager)obj;
				connManager.setParams(params);
				HttpClient httpClient = (HttpClient) configContext.getProperty(HTTPConstants.CACHED_HTTP_CLIENT);
				if(httpClient == null){
					httpClient = new HttpClient(connManager);
					Util.initializeTimeouts(httpClient, (int)SFDCConnector.timeoutInMilliseconds);
					configContext.setProperty(HTTPConstants.CACHED_HTTP_CLIENT, httpClient);
				}				
			}
		}
	}	
	
	
	protected void logConfigContextInfo(ConfigurationContext configContext){
		if(configContext != null)
		{
			Object obj = configContext.getProperty(HTTPConstants.MULTITHREAD_HTTP_CONNECTION_MANAGER);
			if(obj != null)
			{
				MultiThreadedHttpConnectionManager connManager = (MultiThreadedHttpConnectionManager)obj;
				HttpConnectionManagerParams params = connManager.getParams();
				Iterator<String> names = configContext.getPropertyNames();
				if(names != null){
					while(names.hasNext()){
						String name = names.next();
						logger.info("ConfigContext " + name + " = " + configContext.getProperty(name));
					}
				}
				logger.info("HttpConnectionManager Params : " + params.getDefaultMaxConnectionsPerHost() + " " + params.getMaxTotalConnections());
			}
		}
	}
	
	public static char[] new_lang_allowable = new char[] { 
        '_', ' ', ':', '$', '#', '%', '-', '/', '&', '!', 
        '^', '@', ',', ';', '>', '<'};
	public static char[] old_lang_allowable = new char[] { 
        '!', '@', '#', '$', '%', '^', '&', '*', '/', '<', 
        '>', ',', '|', '=', '+', '-', '_', ';',':'};
	char[] allowable = new_lang_allowable;
	
	protected String getAllowable(String f, boolean allowSpaces)
    {
        StringBuilder sb = new StringBuilder();
        // Restrict the characters that are allowed
        for (int i = 0; i < f.length(); i++)
        {
            if ((f.charAt(i) >= 'a' && f.charAt(i) <= 'z') ||
                (f.charAt(i) >= 'A' && f.charAt(i) <= 'Z') ||
                (f.charAt(i) >= '0' && f.charAt(i) <= '9') ||
                (allowSpaces && f.charAt(i) == ' '))
            {
                sb.append(f.charAt(i));
                continue;
            }
            boolean ok = false;
            for (char c : allowable)
                if (f.charAt(i) == c)
                {
                    ok = true;
                    break;
                }
            if (!ok)
                sb.append('_');
            else
                sb.append(f.charAt(i));
        }
        return sb.toString();
    }
	
}
