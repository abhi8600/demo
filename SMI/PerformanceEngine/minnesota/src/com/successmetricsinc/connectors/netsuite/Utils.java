package com.successmetricsinc.connectors.netsuite;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.AbstractMap.SimpleEntry;

import com.netsuite.webservices.platform.common_2012_2.ItemSearchBasic;
import com.netsuite.webservices.platform.common_2012_2.TransactionSearchBasic;
import com.netsuite.webservices.platform.core_2012_2.GetAllRecord;
import com.netsuite.webservices.platform.core_2012_2.Record;
import com.netsuite.webservices.platform.core_2012_2.SearchDateField;
import com.netsuite.webservices.platform.core_2012_2.SearchEnumMultiSelectField;
import com.netsuite.webservices.platform.core_2012_2.SearchRecord;
import com.netsuite.webservices.platform.core_2012_2.types.GetAllRecordType;
import com.netsuite.webservices.platform.core_2012_2.types.SearchDateFieldOperator;
import com.netsuite.webservices.platform.core_2012_2.types.SearchEnumMultiSelectFieldOperator;
import com.successmetricsinc.connectors.ConnectorMetaData;
import com.successmetricsinc.connectors.ExtractionObject;
import com.successmetricsinc.connectors.netsuite.NetSuiteConnector.MaxLastModifiedDate;

public class Utils {

	public SearchRecord isSearchableItem(ConnectorMetaData metaData ,String[] catalogSearchItems, String technicalName, ExtractionObject type, Date minDateValue) 
			throws ClassNotFoundException, InstantiationException, IllegalAccessException, 	NoSuchMethodException, SecurityException, 
			IllegalArgumentException, InvocationTargetException, NoSuchFieldException {
		for (String searchItem : catalogSearchItems)
		{
			if (searchItem.equalsIgnoreCase(technicalName))
			{
				String nsCloudVersion = metaData.getConnectorCloudVersion();
				String itemName = String.valueOf(searchItem.charAt(0)).toUpperCase() + searchItem.substring(1);
				String className = "com.netsuite.webservices.platform.common_" + nsCloudVersion + "." + itemName + "SearchBasic";					
				Class<?> itemSearchClass = Class.forName(className);
				Object itemSearchObj = itemSearchClass.newInstance();
				if (type.lastSystemModStamp != null && !type.lastSystemModStamp.equals(minDateValue) && type.useLastModified)
				{
					try
					{
						for (Method method : itemSearchClass.getDeclaredMethods())
						{
							if (method.getName().equals("setLastModifiedDate") && method.getParameterTypes() != null && method.getParameterTypes().length == 1 && 
									method.getParameterTypes()[0].equals(Class.forName("com.netsuite.webservices.platform.core_" + nsCloudVersion + ".SearchDateField")))
							{
								Class<?> searchDateFieldClass = Class.forName("com.netsuite.webservices.platform.core_" + nsCloudVersion + ".SearchDateField");
								Object sdfObject = searchDateFieldClass.newInstance();
								Class<?> searchDateFieldOperatorClass = Class.forName("com.netsuite.webservices.platform.core_" + nsCloudVersion + ".types.SearchDateFieldOperator");
								Method setOperator = searchDateFieldClass.getMethod("setOperator", new Class[] { searchDateFieldOperatorClass });									
								setOperator.invoke(sdfObject, searchDateFieldOperatorClass.getDeclaredField("after").get(null));
								Method setSearchValue = searchDateFieldClass.getMethod("setSearchValue", new Class[] { Calendar.class });
								Calendar cal = Calendar.getInstance();
								cal.setTimeInMillis(type.lastSystemModStamp.getTime());
								setSearchValue.invoke(sdfObject, cal);
								method.invoke(itemSearchObj, sdfObject);
								break;
							}
						}
					}
					catch (ClassNotFoundException ex)
					{							
					}
				}
				return (SearchRecord)itemSearchObj;
			}
		}

		return null;
	}

	public SearchRecord isTransItem(String[] catalogTransactionItems, String technicalName, ExtractionObject type, Date minDateValue) {

		for (String transItem : catalogTransactionItems)
		{
			if (transItem.equalsIgnoreCase(technicalName))
			{
				TransactionSearchBasic transactionSearchBasic = new TransactionSearchBasic();
				SearchEnumMultiSelectField soType = new SearchEnumMultiSelectField();
				soType.setSearchValue(new String[1]);
				soType.setSearchValue(0, "_" + String.valueOf(transItem.charAt(0)).toLowerCase() + transItem.substring(1));
				soType.setOperator(SearchEnumMultiSelectFieldOperator.anyOf);
				transactionSearchBasic.setType(soType);						
				if (type.lastSystemModStamp != null && !type.lastSystemModStamp.equals(minDateValue) && type.useLastModified)
				{
					SearchDateField sdfObject = new SearchDateField();
					sdfObject.setOperator(SearchDateFieldOperator.after);
					Calendar cal = Calendar.getInstance();
					cal.setTimeInMillis(type.lastSystemModStamp.getTime());
					sdfObject.setSearchValue(cal);									
					transactionSearchBasic.setLastModifiedDate(sdfObject);
				}
				return transactionSearchBasic;						
			}
		}
		return null;
	}

	public SearchRecord isItemItems(String[] catalogItemItems, String technicalName, ExtractionObject type, Date minDateValue, List<String> itemsWithItemPrefix) {

		for (String item : catalogItemItems)
		{
			if (item.equalsIgnoreCase(technicalName))
			{
				ItemSearchBasic itemSearchBasic = new ItemSearchBasic();
				SearchEnumMultiSelectField soType = new SearchEnumMultiSelectField();
				soType.setSearchValue(new String[1]);
				String itemType = "_" + String.valueOf(item.charAt(0)).toLowerCase() + item.substring(1);
				if (!itemsWithItemPrefix.contains(itemType))
				{
					itemType = itemType.substring(0, itemType.lastIndexOf("Item"));
				}
				soType.setSearchValue(0, itemType);
				soType.setOperator(SearchEnumMultiSelectFieldOperator.anyOf);
				itemSearchBasic.setType(soType);

				if (type.lastSystemModStamp != null && !type.lastSystemModStamp.equals(minDateValue) && type.useLastModified)
				{
					SearchDateField sdfObject = new SearchDateField();
					sdfObject.setOperator(SearchDateFieldOperator.after);
					Calendar cal = Calendar.getInstance();
					cal.setTimeInMillis(type.lastSystemModStamp.getTime());
					sdfObject.setSearchValue(cal);									
					itemSearchBasic.setLastModifiedDate(sdfObject);
				}

				return itemSearchBasic;						
			}
		}

		return null;

	}

	public GetAllRecord isGetAllItem(String[] catalogGetAllItems, String technicalName, ExtractionObject type) throws IllegalArgumentException, IllegalAccessException {

		boolean found = false;
		GetAllRecord getAllRecord = null;
		for (String getAllItem : catalogGetAllItems)
		{
			if (getAllItem.equalsIgnoreCase(technicalName))
			{

				for (Field field : GetAllRecordType.class.getDeclaredFields())
				{
					if (field.getType().equals(GetAllRecordType.class))
					{
						GetAllRecordType getAllRecordType = (GetAllRecordType)field.get(null);
						String getAllItemName = getAllRecordType.getValue(); 
						if ((String.valueOf(getAllItem.charAt(0)).toLowerCase() + getAllItem.substring(1)).equals(getAllItemName))
						{
							getAllRecord = new GetAllRecord();
							getAllRecord.setRecordType(getAllRecordType);									
							found = true;
							break;
						}
					}
				}
				if (found)
					break;
			}
		}

		return getAllRecord;	
	}

	@SuppressWarnings("static-access")
	public int writeDependent(Class<?> recClass, Set<String> listTypeColumns, String technicalName, String spaceDir,Record[] record, String objName,
			String customFieldTechName, int numRecords, Map<String, Method> listTypeColumnGetter,List<String> cnames,Map<String, Method> fieldGetterMap, 
			MaxLastModifiedDate maxLastModifiedDate, NetSuiteConnector nsc, ExtractionObject type, boolean amend, boolean isDep, boolean isTrans) throws Exception{

		Method internalIDGetter = recClass.getMethod("getInternalId", new Class[] {});
		Map<String, Writer> listWriterMap = new HashMap<String, Writer>();

		try
		{
			if (listTypeColumns.size() > 0)
			{
				for (String cname : listTypeColumns)
				{
					String curFriendlyName = nsc.dos.findFriendlyName(cname,technicalName);
					if (curFriendlyName == null)
						continue;
					String sfPrefix = "";
					if (nsc.getSourceFileNamePrefix() != null && !nsc.getSourceFileNamePrefix().trim().isEmpty())
						sfPrefix = nsc.getSourceFileNamePrefix() + "_";
					String dependentSourcefileName = spaceDir + File.separator + "tempDepData";
					File dependentSourceFile = new File(dependentSourcefileName);
					if(!dependentSourceFile.exists()) 
						dependentSourceFile.mkdir();
					dependentSourcefileName = dependentSourcefileName + File.separator + sfPrefix +  curFriendlyName  + ".txt";
					dependentSourceFile = new File(dependentSourcefileName);
					if (dependentSourceFile.exists() && !amend)
					{
						dependentSourceFile.delete();
					}
					//BPD-18395 allowing the files data to be amended
					Writer dependentSourceWriter = new PrintWriter(new BufferedWriter(new FileWriter(dependentSourcefileName, amend)));

					listWriterMap.put(cname, dependentSourceWriter);
					nsc.logger.debug("NetSuite - writing dependent source : " + dependentSourcefileName);
					int recordCount = nsc.writeDependentSources(record, objName, customFieldTechName, internalIDGetter, listWriterMap, cname, listTypeColumnGetter.get(cname), 
																true, cnames, fieldGetterMap, maxLastModifiedDate, curFriendlyName, isTrans);
					nsc.logger.debug("NetSuite - finished writing dependent source : " + dependentSourcefileName);
					nsc.dos.setLastMod(objName,maxLastModifiedDate);
					nsc.dos.setLastUpdatedDate(objName);
					if( !isDep || !(nsc.dos.getDependent(type.name).equals(cname))){
						nsc.filesToDel.put(curFriendlyName, new SimpleEntry<File, Integer>(dependentSourceFile, recordCount));
					} else {
						numRecords += recordCount;
					}

				}
			}																
		}
		finally
		{
			for (String dependentSourceFileName : listWriterMap.keySet())
			{
				if (listWriterMap.get(dependentSourceFileName) != null)
				{
					listWriterMap.get(dependentSourceFileName).close();
				}
			}
		}

		return numRecords;
	}
	
	
	public void copyExtractionObject(ExtractionObject from, ExtractionObject to){
		to.columnMappings = from.columnMappings;
		to.columnNames = from.columnNames;
		to.lastSystemModStamp = from.lastSystemModStamp;
		to.lastUpdatedDate = from.lastUpdatedDate;
		to.name = from.name;
		to.noRetries = from.noRetries;
		to.query = from.query;
		to.useLastModified = from.useLastModified;
		to.status = from.status;
	}

}
