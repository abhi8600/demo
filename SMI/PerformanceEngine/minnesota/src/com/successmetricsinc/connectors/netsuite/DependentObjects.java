package com.successmetricsinc.connectors.netsuite;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.successmetricsinc.connectors.ExtractionObject;
import com.successmetricsinc.connectors.netsuite.NetSuiteConnector.MaxLastModifiedDate;

public class DependentObjects {

	private Map<String, String> FriendlyToParent ;
	private Map<String,String> FriendlyToDependent;
	private static final Logger logger = Logger.getLogger(DependentObjects.class);
	private Map<String,ExtractionObject> typeMapping;
	
	public String[] getCatalog(){
		return FriendlyToParent.keySet().toArray(new String[] {});
	}

	public DependentObjects() {
		FriendlyToParent= new HashMap<String,String>();
		FriendlyToDependent = new HashMap<String,String>();
		typeMapping  = new HashMap<String,ExtractionObject>();
		populateMaps();

	}
	public boolean isDependent(String obj) {
		return FriendlyToDependent.containsKey(obj);
	}
	
	public boolean isParent(String obj) {
		return FriendlyToParent.containsValue(obj);
	}
	
	public String getParent(String depObj) {
		return FriendlyToParent.get(depObj);
	}
	public String getDependent(String depObj) {
		return FriendlyToDependent.get(depObj);
	}
	
	//find the friendly name of a object based on its method name and its parent
	public String findFriendlyName(String method, String parent) {
		for (Entry<String, String> curEntry: FriendlyToParent.entrySet()){
			if( (curEntry.getValue()).equalsIgnoreCase(parent) && FriendlyToDependent.get(curEntry.getKey()).equalsIgnoreCase(method)){
					return curEntry.getKey();
			}
		}
		
		return null;
	}
	public Map<String, String> getFriendlyToParent() {
		return FriendlyToParent;
	}

	public Map<String, String> getFriendlyToDependent() {
		return FriendlyToDependent;
	}

	private void populateMaps(){
		//get the list of dependent objects 
		BufferedReader br = new BufferedReader(new InputStreamReader(DependentObjects.class.getResourceAsStream("DependentObjMapping.txt")));
		String line = null;

		try {
			while ((line = br.readLine()) != null)
			{
				String[] lineArr = line.split("\t");
					if (!FriendlyToParent.containsKey(lineArr[0]))
					{
						FriendlyToParent.put(lineArr[0], lineArr[1]);
						FriendlyToDependent.put(lineArr[0], lineArr[2]);
						ExtractionObject type = new ExtractionObject();
						type.name = lineArr[0];
						typeMapping.put(lineArr[0], type);
					}					
			}
		
		br.close();	
		
		} catch (IOException e) {
			logger.error("--There is an I/O issue with reading the catalog file for dependeant objects (DependentObjMapping.txt)--	" + e.getMessage(), e); 
		}
	}
	
	public boolean addColumn(String objName, String colName){
		ExtractionObject type = typeMapping.get(objName);
		if(type == null)
			return false;
		type.addColumnName(colName);
		return true;
	}
	
	public boolean setLastMod(String objName, MaxLastModifiedDate maxLastModifiedDate){
		ExtractionObject type = typeMapping.get(objName);
		if(type == null)
			return false;
		type.lastSystemModStamp = maxLastModifiedDate.maxLastModified;
		return true;
	}
	
	public boolean setLastUpdatedDate(String objName){
		ExtractionObject type = typeMapping.get(objName);
		if(type == null)
			return false;
		type.lastUpdatedDate = new Date();
		return true;
	}
	
	public ExtractionObject getType(String objName){
		 return typeMapping.get(objName);
	}
		
}