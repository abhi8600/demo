package com.successmetricsinc.connectors.netsuite;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.netsuite.webservices.platform.core_2012_2.BaseRef;
import com.netsuite.webservices.platform.core_2012_2.CustomizationRefList;
import com.netsuite.webservices.platform.core_2012_2.CustomizationType;
import com.netsuite.webservices.platform.core_2012_2.GetCustomizationIdResult;
import com.netsuite.webservices.platform.core_2012_2.Record;
import com.netsuite.webservices.platform.core_2012_2.types.GetCustomizationType;
import com.netsuite.webservices.platform.messages_2012_2.ReadResponseList;
import com.netsuite.webservices.platform_2012_2.NetSuitePortType;
import com.netsuite.webservices.setup.customization_2012_2.CrmCustomField;
import com.netsuite.webservices.setup.customization_2012_2.EntityCustomField;
import com.netsuite.webservices.setup.customization_2012_2.ItemCustomField;
import com.netsuite.webservices.setup.customization_2012_2.ItemNumberCustomField;
import com.netsuite.webservices.setup.customization_2012_2.ItemOptionCustomField;
import com.netsuite.webservices.setup.customization_2012_2.OtherCustomField;
import com.netsuite.webservices.setup.customization_2012_2.TransactionBodyCustomField;
import com.netsuite.webservices.setup.customization_2012_2.TransactionColumnCustomField;
import com.netsuite.webservices.setup.customization_2012_2.types.CustomizationFieldType;
import com.successmetricsinc.connectors.UnrecoverableException;

/**
 * 
 * @author Nima Rad
 *
 */

public class CustomFieldsMap { 

		public Map<String, List<CustomFieldInfo>> fieldMapping;
		private Map<String, List<String>> methodToTRecMap;
		private Map<String, String> inconsistentNameMap;
		/**
		 * Proxy class that abstracts the communication with the NetSuite Web
		 * Services. All NetSuite operations are invoked as methods of this class.
		 */
		private NetSuitePortType _port;

		private void populateMethodToTRecMap(){
			BufferedReader br = null;
			try
			{
				br = new BufferedReader(new InputStreamReader(NetSuiteConnector.class.getResourceAsStream("MethodToRecList.txt")));
				String line = null;
				while ((line = br.readLine()) != null)
				{
					String[] lineArr = line.split("\t");
					String[] recordsArr = lineArr[1].split(",");
					methodToTRecMap.put(lineArr[0], Arrays.asList(recordsArr));					
	
				}
			} catch (Exception e){
				e.printStackTrace();
			}
		}
		
		private void populateInconsistentNameMap() {
			BufferedReader br = null;
			try
			{
				br = new BufferedReader(new InputStreamReader(NetSuiteConnector.class.getResourceAsStream("InconsistentNameList.txt")));
				String line = null;
				while ((line = br.readLine()) != null)
				{
					String[] lineArr = line.split("\t");
					inconsistentNameMap.put(lineArr[0], lineArr[1]);					
	
				}
			} catch (Exception e){
				e.printStackTrace();
			} finally {
				try {
					if (br!=null)
					{
						br.close();
					}
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		}
		
		public CustomFieldsMap(NetSuitePortType port, String[] catalog) throws UnrecoverableException  {

			fieldMapping = new HashMap<String,List<CustomFieldInfo>>();
			methodToTRecMap = new HashMap<String,List<String>>();
			inconsistentNameMap = new HashMap<String, String>();
			
			populateMethodToTRecMap();
			populateInconsistentNameMap();

			this._port = port;
			//BPD-17943 striping "Item" from names in the catalog 
			for (String obj : catalog) {
				if(obj.lastIndexOf("Item") > 0)
					obj = obj.substring(0, obj.lastIndexOf("Item"));
				fieldMapping.put(obj, new ArrayList<CustomFieldInfo>());
			}

			try {
				getIdByClass(CrmCustomField.class, GetCustomizationType.crmCustomField,"getAppliesTo");
				getIdByClass(EntityCustomField.class, GetCustomizationType.entityCustomField,"getAppliesTo");
				getIdByClass(ItemCustomField.class, GetCustomizationType.itemCustomField,"getAppliesTo");
				getIdByClass(TransactionBodyCustomField.class, GetCustomizationType.transactionBodyCustomField,"getBody");// getBody Methods.
				getIdByClass(TransactionColumnCustomField.class, GetCustomizationType.transactionColumnCustomField,"getCol");// getCol Methods.
				getIdByClass(ItemOptionCustomField.class, GetCustomizationType.itemOptionCustomField,"getCol");// getCol plus getItemsList for picked items.
				getIdByClass(OtherCustomField.class,GetCustomizationType.otherCustomField,"get");// just one field getRecType for the record type it applies to.
				getIdByClass(ItemNumberCustomField.class,GetCustomizationType.itemNumberCustomField,"getAppliesTo");
			
			} catch (Exception e){
				throw new UnrecoverableException(e.getMessage(),e);

			}
		}

		private void getIdByClass(Class<?> customField, GetCustomizationType custType, String methodType) 
				throws RemoteException, IllegalAccessException, IllegalArgumentException, InvocationTargetException,NoSuchMethodException, SecurityException{

			CustomizationType ct = new CustomizationType();
			ct.setGetCustomizationType(custType);

			// Retrieve active custom record type IDs. The includeInactives param is set to false.
			GetCustomizationIdResult getCustIdResult = _port.getCustomizationId(ct, false);
			int total;
			try {
				total = getCustIdResult.getTotalRecords();
			}
			catch(NullPointerException e){
				return;
			}

			// Retrieve the metadata of the returned custom record types
			CustomizationRefList refList = getCustIdResult.getCustomizationRefList();
			BaseRef[] list = refList.getCustomizationRef();
			if( list == null )
				return;
			ReadResponseList readResp = _port.getList(list);

			List<Method> methods = getMethods(customField, methodType);

			if(methods.isEmpty()){
				return;
			}

			String technicalName;
			String technicalNameTemp;
			String recordTypeName;
			for (int i = 0; i < total; i++)
			{
				Record cur = readResp.getReadResponse(i).getRecord();
				if (cur instanceof OtherCustomField) {
					OtherCustomField ocf = (OtherCustomField) cur;
					for (Method m : methods) {
						recordTypeName = ocf.getRecType().getName();
						technicalNameTemp = inconsistentNameMap.get(recordTypeName);
						technicalName = NetSuiteConnector.getLogicalToTechnicalNameMap((technicalNameTemp == null) ? recordTypeName : technicalNameTemp);
						String objName = m.getName().replace(methodType, "");
						List<String> recordsList = getMethodToRecType(objName);
						for (String rec : recordsList) {
							if (technicalName != null && technicalName.equals(rec)) {
								CustomFieldInfo cfi = new CustomFieldInfo();
								cfi.setFieldType(ocf.getFieldType());
								cfi.setInternalId(ocf.getInternalId());
								cfi.setLabel(ocf.getLabel());
								if (!fieldMapping.get(rec).contains(cfi)) {
									fieldMapping.get(rec).add(cfi);
								}
								continue;
							}
						}
					}
				}

				for (Method m : methods)
				{
					String objName = m.getName().replace(methodType, "");
					List<String> recordsList = getRecType(objName);
					for (String rec : recordsList)
					{
						if (fieldMapping.containsKey(rec))
						{
							boolean appliesToObject = false;
							appliesToObject = Boolean.parseBoolean(String.valueOf(m.invoke(cur)));
							if (appliesToObject)
							{
								CustomFieldInfo cfInfo = new CustomFieldInfo();
								Method labelGetter = cur.getClass().getMethod("getLabel", (Class<?>[]) null);
								cfInfo.setLabel((String) labelGetter.invoke(cur));
								Method idGetter = cur.getClass().getMethod("getInternalId", (Class<?>[]) null);
								cfInfo.setInternalId((String) idGetter.invoke(cur));
								Method fieldTypeGetter = cur.getClass().getMethod("getFieldType", (Class<?>[]) null);
								cfInfo.setFieldType((CustomizationFieldType) fieldTypeGetter.invoke(cur));
								if (!fieldMapping.get(rec).contains(cfInfo))
									fieldMapping.get(rec).add(cfInfo);						
							}
						}
					}
				}
			}
		}
		
		private List<String> getRecType(String methodName){
			if(methodToTRecMap.containsKey(methodName))
				return methodToTRecMap.get(methodName);
			List<String> recList = new ArrayList<String>();
			recList.add(methodName);
			return recList;	
		}
		
		private List<String> getMethodToRecType(String methodName){
			if(methodToTRecMap.containsKey(methodName))
				return methodToTRecMap.get(methodName);
			return new ArrayList<String>();
		}

		private List<Method> getMethods(Class<?> clazz,String methodType) {
			List<Method> methods = new ArrayList<Method>();

			for (Method m : clazz.getMethods()){
				if (m.getName().startsWith(methodType) && m.getParameterTypes().length == 0){
					methods.add(m);				
				}
			}
			return methods;
		}

		class CustomFieldInfo {
			private String label;
			private String internalId;
			private CustomizationFieldType fieldType;
			public String getLabel() {
				return label;
			}
			public void setLabel(String label) {
				this.label = label;
			}
			public String getInternalId() {
				return internalId;
			}
			public void setInternalId(String internalId) {
				this.internalId = internalId;
			}
			public CustomizationFieldType getFieldType() {
				return fieldType;
			}
			public void setFieldType(CustomizationFieldType fieldType) {
				this.fieldType = fieldType;
			}
			
			@Override
			public boolean equals(Object other) {
				if (other == null)
					return false;
				CustomFieldInfo cfi = (CustomFieldInfo)other;
				if (!this.label.equals(cfi.label))
					return false;
				if (!this.internalId.equals(cfi.internalId))
					return false;
				if (!this.fieldType.equals(cfi.fieldType))
					return false;
				return true;
			}
		}
	}
