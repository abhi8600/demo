package com.successmetricsinc.connectors.netsuite;

import java.util.Date;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.netsuite.webservices.platform.common_2012_2.CustomRecordSearchBasic;
import com.netsuite.webservices.platform.core_2012_2.BaseRef;
import com.netsuite.webservices.platform.core_2012_2.CustomizationRef;
import com.netsuite.webservices.platform.core_2012_2.CustomizationRefList;
import com.netsuite.webservices.platform.core_2012_2.CustomizationType;
import com.netsuite.webservices.platform.core_2012_2.GetCustomizationIdResult;
import com.netsuite.webservices.platform.core_2012_2.RecordRef;
import com.netsuite.webservices.platform.core_2012_2.SearchDateField;
import com.netsuite.webservices.platform.core_2012_2.SearchResult;
import com.netsuite.webservices.platform.core_2012_2.types.GetCustomizationType;
import com.netsuite.webservices.platform.core_2012_2.types.SearchDateFieldOperator;
import com.netsuite.webservices.platform.messages_2012_2.ReadResponseList;
import com.netsuite.webservices.platform_2012_2.NetSuitePortType;
import com.netsuite.webservices.setup.customization_2012_2.CustomRecordCustomField;
import com.netsuite.webservices.setup.customization_2012_2.CustomRecordType;

public class CustomObjects {
	/**
	 * Proxy class that abstracts the communication with the NetSuite Web
	 * Services. All NetSuite operations are invoked as methods of this class.
	 */
	private NetSuitePortType _port;
	
	private HashMap<String, String[]> mapping ;
	private Map<String,String> catag;
	private Map<String, Object> techToFriendlyForFields;
	
	public CustomObjects(NetSuitePortType port) {
		_port = port;
		mapping = new HashMap<String,String[]>();
		int total;

		try {

			CustomizationType ct = new CustomizationType();
			ct.setGetCustomizationType(GetCustomizationType.customRecordType);
			GetCustomizationIdResult getCustIdResult;
			getCustIdResult = _port.getCustomizationId(ct, false);
			total = getCustIdResult.getTotalRecords();

			catag = new HashMap<String,String>();
			techToFriendlyForFields = new HashMap<String,Object>();
			// Retrieve the metadata of the returned custom object 
			CustomizationRefList refList = getCustIdResult.getCustomizationRefList();
			BaseRef[] list = refList.getCustomizationRef();
			ReadResponseList readResp = _port.getList(list);

			//Mapping of custom objects to its fields
			for (int i = 0; i < total; i++)
			{
				CustomRecordType customObject = (CustomRecordType) readResp.getReadResponse(i).getRecord();
				CustomRecordCustomField[] fields;
				try{
					fields = customObject.getCustomFieldList().getCustomField();
				}catch(NullPointerException npe){
					//empty record. skipping
					continue;
				}
				
				if(customObject != null && !customObject.getIsInactive() && fields != null){
					String[] fieldsInfo = new String[fields.length];
					
					HashMap<String,String> idToLabelHashMap = new HashMap<String,String> ();
					for(int j = 0; j < fields.length; j++){
						CustomRecordCustomField field;
						field = fields[j];

						//BPD-18109 using label instead of internal id
						fieldsInfo[j] = field.getLabel();
						idToLabelHashMap.put(field.getInternalId().toLowerCase(),field.getLabel());
					}
					techToFriendlyForFields.put(customObject.getInternalId(),idToLabelHashMap);
					
					mapping.put(customObject.getInternalId(), fieldsInfo);
					
				}
			}		
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> getTechToFriendlyForFields(String id) {
		return (Map<String, String>) techToFriendlyForFields.get(id);
	}

	public SearchResult getRecords(String obj, Date lastSystemModStamp){

		CustomRecordSearchBasic basic = new CustomRecordSearchBasic() ;
		RecordRef rec = new RecordRef();
		try {
			rec.setInternalId(obj);
			basic.setRecType(rec);
			if(lastSystemModStamp != null) {
				Calendar timeFrom = Calendar.getInstance();
				timeFrom.setTime(lastSystemModStamp);
				SearchDateField sdf = new SearchDateField();
				sdf.setOperator(SearchDateFieldOperator.after);
				sdf.setSearchValue(timeFrom);
				basic.setLastModified(sdf);
			}

			double before= System.currentTimeMillis();
			SearchResult results  = _port.search(basic);
			double after = System.currentTimeMillis();
			//logger.debug("Num Records: " + result.getTotalRecords() + ", Retrieval Time: " + (after - before) / 1000.00 + 
			//					" Seconds, Avg. Retrieval time per record: " + ((after - before) / 1000.00) / result.getTotalRecords());
			return results;
		} catch (Exception e) {
			// TODO Auto-generated catch block
		}
		return null;

	}

	public List<String> getColNames(String obj) {
		String[] cols = mapping.get(obj);
		LinkedList<String> retVal = new LinkedList<String>();

		retVal.add("InternalId");
		retVal.add("ExternalId");
		retVal.add("IsInactive");
		retVal.add("LastModifiedDate");
		retVal.add("Label");
		retVal.add("CreatedDate");
		for(int i =0; i < cols.length ; i++)
			retVal.add(cols[i]);


		return retVal;
	}

	
	public static class CustomObjectHelper{
		//Short term fix to make catalog apear faster
		public synchronized static Map<String,String> getCatalog(NetSuitePortType port){
			HashMap<String,String> catag = new HashMap<String,String>();

			CustomizationType ct = new CustomizationType();
			ct.setGetCustomizationType(GetCustomizationType.customRecordType);
			GetCustomizationIdResult getCustIdResult = null;
			try {
				getCustIdResult = port.getCustomizationId(ct, false);
			} catch (Exception e) {
				return catag;
			}

			// Retrieve the metadata of the returned custom object 
			CustomizationRefList refList = getCustIdResult.getCustomizationRefList();
			int size = refList.getCustomizationRef().length;
			for (int i = 0; i < size ; i++ ){
				CustomizationRef cur = refList.getCustomizationRef(i);
				catag.put(cur.getName(),cur.getInternalId());
			}
			
			return catag;
		}
	}
}
