package com.successmetricsinc.connectors.netsuite;

import java.util.ArrayList;
import java.util.List;

import com.successmetricsinc.connectors.BaseConnector;
import com.successmetricsinc.connectors.ConnectionProperty;
import com.successmetricsinc.connectors.ConnectorMetaData;
import com.successmetricsinc.connectors.ExtractionObject;

/**
 * 
 * @author mpandit
 *
 */
public class NetSuiteConnectorMetaData extends ConnectorMetaData {
	private static final String name = "NETSUITE";
	private static final String interfaceVersion = "1.0";
	private static final String connectorVersion = "1.0";
	private static final String connectorCloudVersion = "2012_2";
	private static final List<ConnectionProperty>  connectionProperties = fillConnectionProperties();
	private static final List<ExtractionObject.Types> supportedTypes = fillSupportedTypes();
	
	private static List<ConnectionProperty> fillConnectionProperties() {
		List<ConnectionProperty> connectionProperties = getBaseConnectionProperties();
		ConnectionProperty email = new ConnectionProperty();
		email.name = NetSuiteConnector.EMAIL;
		email.isRequired = true;
		email.displayIndex = 1;
		email.displayLabel = NetSuiteConnector.EMAIL;
		connectionProperties.add(email);
		ConnectionProperty password = new ConnectionProperty();
		password.name = NetSuiteConnector.PASSWORD;
		password.isRequired = true;
		password.isSecret = true;
		password.isEncrypted = true;
		password.displayIndex = 2;
		password.displayLabel = NetSuiteConnector.PASSWORD;
		connectionProperties.add(password);
		ConnectionProperty roleID = new ConnectionProperty();
		roleID.name = NetSuiteConnector.ROLE_INTERNAL_ID;
		roleID.isRequired = true;
		roleID.displayIndex = 5;
		roleID.displayLabel = "Role Internal Id";
		connectionProperties.add(roleID);
		ConnectionProperty accountID = new ConnectionProperty();
		accountID.name = NetSuiteConnector.ACCOUNT_ID;
		accountID.isRequired = true;
		accountID.displayIndex = 4;
		accountID.displayLabel = "Account";
		connectionProperties.add(accountID);
		ConnectionProperty sandBoxURL = new ConnectionProperty();
		sandBoxURL.name = BaseConnector.SANDBOX_URL;
		sandBoxURL.isRequired = false;
		sandBoxURL.saveToConfig = false;
		connectionProperties.add(sandBoxURL);
		return connectionProperties;
	}
	
	private static List<ExtractionObject.Types> fillSupportedTypes() {
		List<ExtractionObject.Types> types = new ArrayList<ExtractionObject.Types>();
		types.add(ExtractionObject.Types.OBJECT);
		return types;
	}
	
	@Override
	public String getConnectorName() {
		return name;
	}

	@Override
	public String getInterfaceVersion() {
		return interfaceVersion;
	}

	@Override
	public String getConnectorVersion() {
		return connectorVersion;
	}

	@Override
	public String getConnectorCloudVersion() {
		return connectorCloudVersion;
	}

	@Override
	public List<ExtractionObject.Types> getSupportedTypes() {
		return supportedTypes;
	}

	@Override
	public List<ConnectionProperty>  getConnectionProperties() {
		return connectionProperties;
	}

	@Override
	public boolean supportsParallelExtraction() {
		return false;
	}

	@Override
	public boolean allowsSourceFileNamePrefix() {
		return true;
	}

}
