package com.successmetricsinc.connectors.netsuite;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;

import javax.xml.soap.SOAPException;

import org.apache.axis.description.TypeDesc;
import org.apache.axis.message.SOAPHeaderElement;
import org.apache.log4j.Logger;

import com.netsuite.webservices.platform.common_2012_2.ItemSearchBasic;
import com.netsuite.webservices.platform.common_2012_2.TransactionSearchBasic;
import com.netsuite.webservices.platform.core_2012_2.BooleanCustomFieldRef;
import com.netsuite.webservices.platform.core_2012_2.CustomFieldList;
import com.netsuite.webservices.platform.core_2012_2.CustomFieldRef;
import com.netsuite.webservices.platform.core_2012_2.GetAllRecord;
import com.netsuite.webservices.platform.core_2012_2.GetAllResult;
import com.netsuite.webservices.platform.core_2012_2.GetDataCenterUrlsResult;
import com.netsuite.webservices.platform.core_2012_2.ListOrRecordRef;
import com.netsuite.webservices.platform.core_2012_2.Passport;
import com.netsuite.webservices.platform.core_2012_2.Record;
import com.netsuite.webservices.platform.core_2012_2.RecordList;
import com.netsuite.webservices.platform.core_2012_2.RecordRef;
import com.netsuite.webservices.platform.core_2012_2.SearchDateField;
import com.netsuite.webservices.platform.core_2012_2.SearchEnumMultiSelectField;
import com.netsuite.webservices.platform.core_2012_2.SearchRecord;
import com.netsuite.webservices.platform.core_2012_2.SearchResult;
import com.netsuite.webservices.platform.core_2012_2.Status;
import com.netsuite.webservices.platform.core_2012_2.StatusDetail;
import com.netsuite.webservices.platform.core_2012_2.types.GetAllRecordType;
import com.netsuite.webservices.platform.core_2012_2.types.SearchDateFieldOperator;
import com.netsuite.webservices.platform.core_2012_2.types.SearchEnumMultiSelectFieldOperator;
import com.netsuite.webservices.platform.faults_2012_2.ExceededRequestLimitFault;
import com.netsuite.webservices.platform.faults_2012_2.InsufficientPermissionFault;
import com.netsuite.webservices.platform.faults_2012_2.InvalidAccountFault;
import com.netsuite.webservices.platform.faults_2012_2.InvalidCredentialsFault;
import com.netsuite.webservices.platform.messages_2012_2.Preferences;
import com.netsuite.webservices.platform.messages_2012_2.SearchPreferences;
import com.netsuite.webservices.platform.messages_2012_2.SessionResponse;
import com.netsuite.webservices.platform_2012_2.NetSuiteBindingStub;
import com.netsuite.webservices.platform_2012_2.NetSuitePortType;
import com.netsuite.webservices.platform_2012_2.NetSuiteServiceLocator;
import com.netsuite.webservices.setup.customization_2012_2.CustomRecord;
import com.successmetricsinc.connectors.BaseConnector;
import com.successmetricsinc.connectors.ConnectorMetaData;
import com.successmetricsinc.connectors.ExtractionObject;
import com.successmetricsinc.connectors.ExtractionProgressHandler;
import com.successmetricsinc.connectors.ExtractionProgressHandler.ExtractionStatus;
import com.successmetricsinc.connectors.IConnector;
import com.successmetricsinc.connectors.RecoverableException;
import com.successmetricsinc.connectors.UnrecoverableException;
import com.successmetricsinc.connectors.netsuite.CustomFieldsMap.CustomFieldInfo;
import com.successmetricsinc.connectors.netsuite.CustomObjects.CustomObjectHelper;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.connectors.netsuite.Utils;

/**
 * 
 * @author mpandit
 *
 */
public class NetSuiteConnector extends BaseConnector implements IConnector {

	private ConnectorMetaData metaData = new NetSuiteConnectorMetaData();
	private String roleInternalID;
	private String email;
	private String password;
	private String accountID;
	private Passport passport;
	private SimpleDateFormat dateFormatter = new SimpleDateFormat(dateTimeISOStandardFormatPattern);
	private SimpleDateFormat dateTimeFormatter = new SimpleDateFormat(dateTimeISOStandardFormatPattern);

	public static final String EMAIL = "Email";
	public static final String PASSWORD = "Password";
	public static final String ROLE_INTERNAL_ID = "RoleInternalID";
	public static final String ACCOUNT_ID = "AccountID";

	private static final int timeoutInMilliseconds = 30 * 60 * 1000;

	protected static final Logger logger = Logger.getLogger(NetSuiteConnector.class);

	private static Map<String, String> logicalToTechnicalNameMap = new HashMap<String, String>();
	private static Map<String, String> parentTechNametologicalMap = new HashMap<String, String>();
	private static String[] catalogSearchItems;
	private static String[] catalogTransactionItems;
	private static String[] catalogItemItems;
	private static String[] catalogGetAllItems;
	private static String[] catalogDependentObjects;
	private static List<String> itemsWithItemPrefix = new ArrayList<String>();
	protected static DependentObjects dos = new DependentObjects(); 
	private Map<String, List<CustomFieldInfo>> customFiledsSchema; 
	protected Map<String, SimpleEntry<File,Integer>> filesToDel = new HashMap<String, SimpleEntry<File,Integer>>(); 

	private CustomObjects customObjects;

	static
	{
		itemsWithItemPrefix.add("_downloadItem");
		itemsWithItemPrefix.add("_giftCertificateItem");
		itemsWithItemPrefix.add("_inventoryItem");
		itemsWithItemPrefix.add("_nonInventoryItem");

		//Prepare catalog
		BufferedReader br = null;
		try
		{
			//get list of searchable objects first
			br = new BufferedReader(new InputStreamReader(NetSuiteConnector.class.getResourceAsStream("SearchItemsList.txt")));
			String searchItem = null;
			String line = null;
			List<String> searchItemsList = new ArrayList<String>();
			String nsCloudVersion = new NetSuiteConnectorMetaData().getConnectorCloudVersion();
			while ((line = br.readLine()) != null)
			{
				String[] lineArr = line.split("\t");
				searchItem = lineArr[1];
				try
				{
					String itemName = String.valueOf(searchItem.charAt(0)).toUpperCase() + searchItem.substring(1);
					String className = "com.netsuite.webservices.platform.common_" + nsCloudVersion + "." + itemName + "SearchBasic";
					Class itemSearchClass = Class.forName(className);
					Object itemSearchObj = itemSearchClass.newInstance();
					SearchRecord searchRecord = (SearchRecord)itemSearchObj;
					if (!searchItemsList.contains(itemName))
					{
						searchItemsList.add(itemName);
						logicalToTechnicalNameMap.put(lineArr[0], itemName);
						parentTechNametologicalMap.put(lineArr[1].toLowerCase(), lineArr[0]);
					}					
				}
				catch (ClassNotFoundException ex)
				{				
					logger.warn("Class Not Found : " + ex.getMessage());
				}
			}
			catalogSearchItems = searchItemsList.toArray(new String[] {});
			br.close();
			//get list of transaction objects 
			br = new BufferedReader(new InputStreamReader(NetSuiteConnector.class.getResourceAsStream("TransactionItemsList.txt")));
			String transactionItem = null;
			line = null;
			List<String> transactionItemsList = new ArrayList<String>();
			while ((line = br.readLine()) != null)
			{
				String[] lineArr = line.split("\t");
				transactionItem = lineArr[1];					
				try
				{
					TransactionSearchBasic transactionSearchBasic = new TransactionSearchBasic();
					SearchEnumMultiSelectField soType = new SearchEnumMultiSelectField();
					soType.setSearchValue(new String[1]);
					soType.setSearchValue(0, "_" + transactionItem);
					soType.setOperator(SearchEnumMultiSelectFieldOperator.anyOf);
					transactionSearchBasic.setType(soType);
					String itemName = String.valueOf(transactionItem.charAt(0)).toUpperCase() + transactionItem.substring(1);
					if (!transactionItemsList.contains(itemName))
					{
						transactionItemsList.add(itemName);
						logicalToTechnicalNameMap.put(lineArr[0], itemName);
						parentTechNametologicalMap.put(lineArr[1].toLowerCase(), lineArr[0]);
					}					
				}
				catch (Exception ex)
				{				
					logger.warn(ex.getMessage(), ex);
				}
			}
			catalogTransactionItems = transactionItemsList.toArray(new String[] {});
			br.close();
			//get list of item objects 
			br = new BufferedReader(new InputStreamReader(NetSuiteConnector.class.getResourceAsStream("ItemItemsList.txt")));
			String item = null;
			line = null;
			List<String> itemsList = new ArrayList<String>();
			while ((line = br.readLine()) != null)
			{
				String[] lineArr = line.split("\t");
				item = lineArr[1];
				try
				{
					String itemType = item;
					if (!itemsWithItemPrefix.contains(itemType))
					{
						itemType = itemType.substring(0, itemType.lastIndexOf("Item"));
					}					
					ItemSearchBasic itemSearchBasic = new ItemSearchBasic();
					SearchEnumMultiSelectField soType = new SearchEnumMultiSelectField();
					soType.setSearchValue(new String[1]);
					soType.setSearchValue(0, "_" + itemType);
					soType.setOperator(SearchEnumMultiSelectFieldOperator.anyOf);
					itemSearchBasic.setType(soType);
					String itemName = String.valueOf(item.charAt(0)).toUpperCase() + item.substring(1);
					if (!itemsList.contains(itemName))
					{
						itemsList.add(itemName);
						logicalToTechnicalNameMap.put(lineArr[0], itemName);
						parentTechNametologicalMap.put(lineArr[1].toLowerCase(), lineArr[0]);
					}					
				}
				catch (Exception ex)
				{				
					logger.warn(ex.getMessage(), ex);
				}
			}
			catalogItemItems = itemsList.toArray(new String[] {});
			br.close();
			//get list of getAll types
			br = new BufferedReader(new InputStreamReader(NetSuiteConnector.class.getResourceAsStream("GetAllItemsList.txt")));
			String getAllItem = null;
			line = null;
			List<String> getAllItemsList = new ArrayList<String>();
			while ((line = br.readLine()) != null)
			{
				String[] lineArr = line.split("\t");
				getAllItem = lineArr[1];
				try
				{
					for (Field field : GetAllRecordType.class.getDeclaredFields())
					{
						if (field.getType().equals(GetAllRecordType.class))
						{
							GetAllRecordType getAllRecordType = (GetAllRecordType)field.get(null);
							String getAllItemName = getAllRecordType.getValue(); 
							if ((getAllItem).equals(getAllItemName))
							{
								String itemName = String.valueOf(getAllItem.charAt(0)).toUpperCase() + getAllItem.substring(1);
								if (!getAllItemsList.contains(itemName))
								{
									getAllItemsList.add(itemName);
									logicalToTechnicalNameMap.put(lineArr[0], itemName);
									parentTechNametologicalMap.put(lineArr[1].toLowerCase(), lineArr[0]);
								}
								break;
							}
						}
					}										
				}
				catch (Exception ex)
				{				
					logger.warn(ex.getMessage(), ex);
				}
			}
			catalogGetAllItems = getAllItemsList.toArray(new String[] {});	

			catalogDependentObjects = dos.getCatalog();
			logicalToTechnicalNameMap.putAll(dos.getFriendlyToParent());

		}
		catch (Exception ex)
		{
			logger.error("Failed to build static catalog list for NetSuite - ", ex);
		}
		finally
		{
			if (br != null)
			{
				try
				{
					br.close();
				}
				catch(IOException ex)
				{					
				}
			}
		}
	}

	@Override
	public void connect(Properties props) throws RecoverableException, UnrecoverableException {
		if (!metaData.containsRequiredConnectionProperties(props))
			throw new UnrecoverableException("Required connection properties not found", new Exception("Required connection properties not found"));
		try
		{
			super.connect(props, metaData);
			roleInternalID = props.getProperty(ROLE_INTERNAL_ID);
			email = props.getProperty(EMAIL);
			password = props.getProperty(PASSWORD);
			accountID = props.getProperty(ACCOUNT_ID);
			login();
			isConnected = passport != null;
		}
		catch (Exception ex)
		{
			if (ex instanceof RecoverableException)
				throw (RecoverableException)ex;
			else if (ex instanceof UnrecoverableException)
				throw (UnrecoverableException)ex;
			throw new UnrecoverableException(ex.getMessage(), ex);
		}
	}

	private void login() throws Exception
	{
		try
		{
			NetSuitePortType _port = createStub();
			SessionResponse sessionResponse = _port.login(passport);
			processStatus(sessionResponse.getStatus());		
		}
		catch (Exception ex)
		{
			logger.error(ex.getMessage(), ex);
			if (ex instanceof InvalidCredentialsFault)
			{
				InvalidCredentialsFault fault = (InvalidCredentialsFault)ex;
				String errorMessage = fault.getFaultReason();
				UnrecoverableException e = new UnrecoverableException(errorMessage, ex);
				e.setErrorCode(BaseException.ERROR_CONNECTOR_LOGIN_FAILURE);
				throw e;				
			}
			else if (ex instanceof InsufficientPermissionFault)
			{
				InsufficientPermissionFault fault = (InsufficientPermissionFault)ex;
				String errorMessage = fault.getFaultReason();
				UnrecoverableException e = new UnrecoverableException(errorMessage, ex);
				e.setErrorCode(BaseException.ERROR_CONNECTOR_LOGIN_FAILURE);
				throw e;				
			}
			else if (ex instanceof InvalidAccountFault)
			{
				InvalidAccountFault fault = (InvalidAccountFault)ex;
				String errorMessage = fault.getFaultReason();
				UnrecoverableException e = new UnrecoverableException(errorMessage, ex);
				e.setErrorCode(BaseException.ERROR_CONNECTOR_LOGIN_FAILURE);
				throw e;				
			}
			else if (ex instanceof ExceededRequestLimitFault)
			{
				ExceededRequestLimitFault fault = (ExceededRequestLimitFault)ex;
				String errorMessage = fault.getFaultReason();
				UnrecoverableException e = new UnrecoverableException(errorMessage, ex);
				e.setErrorCode(BaseException.ERROR_CONNECTOR_LOGIN_FAILURE);
				throw e;				
			}
			throw ex;						
		}
	}

	private void setPreferences(NetSuitePortType _port) throws SOAPException {
		// Cast your login NetSuitePortType variable to a NetSuiteBindingStub
		NetSuiteBindingStub stub = (NetSuiteBindingStub) _port;

		// Clear the headers to make sure you know exactly what you are sending.
		// Headers do not overwrite when you are using Axis/Java
		stub.clearHeaders();

		//Create request level login user passport header
		SOAPHeaderElement userPassportHeader = new SOAPHeaderElement("urn:messages.platform.webservices.netsuite.com", "passport");
		passport = prepareLoginPassport();
		userPassportHeader.setObjectValue(passport);

		// Create a new SOAPHeaderElement, this is what the NetSuiteBindingStub
		// will accept
		// This is the same command for all preference elements, ie you might
		// substitute "useDefaults" for "searchPreferences"
		SOAPHeaderElement searchPrefHeader = new SOAPHeaderElement(
				"urn:messages.platform.webservices.netsuite.com",
				"searchPreferences");

		// Create your Actual SearchPreference Object, this contains the
		// elements you are allowed to set.
		// In this case it is PageSize (for pagination of searches) and
		// BodyFieldsOnly (reserved)
		SearchPreferences searchPrefs = new SearchPreferences();
		//searchPrefs.setPageSize(new Integer(_pageSize));
		searchPrefs.setBodyFieldsOnly(false);		

		// setObjectValue applies search preference object to the HeaderElement
		searchPrefHeader.setObjectValue(searchPrefs);

		//Create another SOAPHeaderElement to store the preference ignoreReadOnlyFields
		//This preference is used for the initialize method call.  See the
		// comments for the transformEstimateToSO() method for details.
		SOAPHeaderElement platformPrefHeader = new SOAPHeaderElement("urn:messages.platform.webservices.netsuite.com", "preferences");
		Preferences pref = new Preferences();
		pref.setIgnoreReadOnlyFields(Boolean.TRUE);
		platformPrefHeader.setObjectValue(pref);

		// setHeader applies the Header Element to the stub
		// Again, note that if you reuse your NetSuitePort object (vs logging in
		// before every request)
		// that headers are sticky, so if in doubt, call clearHeaders() first.
		stub.setHeader(userPassportHeader);
		stub.setHeader(searchPrefHeader);
		stub.setHeader(platformPrefHeader);
	}

	private Passport prepareLoginPassport() {
		Passport passport = new Passport();
		RecordRef role = new RecordRef();
		passport.setEmail(email);
		passport.setAccount(accountID);
		passport.setPassword(password);
		role.setInternalId(roleInternalID);
		passport.setRole(role);
		return passport;
	}

	@Override
	public ConnectorMetaData getMetaData() throws UnrecoverableException {
		return metaData;
	}

	@Override
	public String[] getCatalog() throws RecoverableException, UnrecoverableException {
		if (!isConnected)
		{
			throw new UnrecoverableException("Not connected to NetSuite", new Exception("Not connected to NetSuite"));
		}

		if (catalogSearchItems == null)
		{
			throw new UnrecoverableException("NetSuite Catalog does not exist", new Exception("NetSuite Catalog does not exist"));
		}
		String [] catalogItems = null;
		Map<String, String> customObjectsMap = null;
		if (catalogTransactionItems == null)
		{
			throw new UnrecoverableException("NetSuite Transaction Catalog does not exist", new Exception("NetSuite Transaction Catalog does not exist"));
		}
		if (catalogItemItems == null)
		{
			throw new UnrecoverableException("NetSuite Item Catalog does not exist", new Exception("NetSuite Item Catalog does not exist"));
		}
		if (catalogGetAllItems == null)
		{
			throw new UnrecoverableException("NetSuite GetAllItems Catalog does not exist", new Exception("NetSuite GetAllItems Catalog does not exist"));
		}
		int size = logicalToTechnicalNameMap.size();
		try {
			customObjectsMap = CustomObjectHelper.getCatalog(createStub());
			size += customObjectsMap.size();
		} catch (Exception e) {
			throw new UnrecoverableException(e.getMessage(), e);
		}

		catalogItems = new String[size];
		int count = 0;
		for (int i = 0; i < catalogSearchItems.length; i++)
		{
			catalogItems[count++] = catalogSearchItems[i];
		}
		List<String> catagItems = new ArrayList<String>(logicalToTechnicalNameMap.keySet());
		if (customObjectsMap != null)
		{
			catagItems.addAll(customObjectsMap.keySet());
		}
		catalogItems = catagItems.toArray(new String[0]);			
		List<String> catalogItemsList = Arrays.asList(catalogItems);
		Collections.sort(catalogItemsList);
		return catalogItemsList.toArray(new String[] { });				
	}

	@Override
	public int fetchData(Writer notUsedWriter, ExtractionObject type, ExtractionProgressHandler hndlr) 
			throws RecoverableException, UnrecoverableException {
		if (!isConnected)
		{
			throw new UnrecoverableException("Not connected to Netsuite", new Exception("Not connected to Netsuite"));
		}
		Map<String, String> customObjectsMap = null;
		//if Custom Objects object isnt created create it here
		if (customObjects == null){
			try {
				customObjects = new CustomObjects(createStub());
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}

		try {
			customObjectsMap = CustomObjectHelper.getCatalog(createStub());
		} catch (Exception e2) {
			throw new UnrecoverableException(e2.getMessage(), e2);
		}
		
		String technicalName = null;
		if (logicalToTechnicalNameMap.containsKey(type.name))
		{
			technicalName = logicalToTechnicalNameMap.get(type.name);
		}
		else if (customObjectsMap != null && customObjectsMap.containsKey(type.name))
		{
			technicalName = customObjectsMap.get(type.name);
		}

		boolean isDepObject = dos.isDependent(type.name) ? true : false;

		logger.info("Starting extract for object : " + type.name);
		long startTime = System.currentTimeMillis();
		logger.info("Space " + spaceDir + " - object retrieval process started for NetSuite object: " + type.name);
		dateFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
		dateTimeFormatter.setTimeZone(TimeZone.getTimeZone("America/Los_Angeles"));
		int numRecords = 0;
		MaxLastModifiedDate maxLastModifiedDate = new MaxLastModifiedDate();
		maxLastModifiedDate.maxLastModified = new Date(minDateValue.getTime());

		Utils utils = new Utils();
		
		if(filesToDel.containsKey(type.name)) {
			SimpleEntry<File, Integer> value = filesToDel.remove(type.name);
			numRecords = value.getValue();
			ExtractionObject temptype = dos.getType(type.name);
		    utils.copyExtractionObject(temptype, type);
			type.status = ExtractionObject.SUCCESS;
			return numRecords;
		}
		
		String sfPrefix = "";
		if (this.SourceFileNamePrefix != null && !this.SourceFileNamePrefix.trim().isEmpty())
			sfPrefix = this.SourceFileNamePrefix + "_";
		
		String fileName = this.spaceDir + File.separator + "tempDepData";
		File file = new File(fileName);
		
		String name;
		if(isDepObject){
			
			name =  parentTechNametologicalMap.get(technicalName.toLowerCase());
			if(name == null) 
					return 0;
		} else {
			name = type.name;
		}
		
		if(!file.exists()) 
				file.mkdir();
		fileName = fileName + File.separator + sfPrefix +  name  + ".txt";
		file = new File(fileName);
		
		if (file.exists())
		{
				file.delete();
		}

		Writer writer;
		try {
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName, false), "UTF-8"));
		} catch (UnsupportedEncodingException | FileNotFoundException e1) {
			throw new UnrecoverableException(e1.getMessage(), e1);
		}
		
		
		
		try
		{
			boolean found = false;
			SearchRecord searchRecord = null;
			GetAllRecord getAllRecord = null;
			boolean isSearchableItem = false;
			boolean isTransactionItem = false;
			boolean isItemItem = false;
			boolean isGetAllItem = false;
			boolean isCustomObject = false;
			Map<String, String> COCatalog = CustomObjectHelper.getCatalog(createStub());
			if(COCatalog.containsKey(type.name) ){
				isCustomObject = true;
				found=true;				
			}

			if (!found)
			{
				searchRecord = utils.isSearchableItem(metaData, catalogSearchItems, technicalName, type, minDateValue);
				found = isSearchableItem = (searchRecord == null) ? false : true;
			}

			if (!found)
			{
				searchRecord = utils.isTransItem(catalogTransactionItems, technicalName, type, minDateValue);
				found = isTransactionItem = (searchRecord == null) ? false : true;
			}

			if (!found)
			{
				searchRecord = utils.isItemItems(catalogItemItems, technicalName, type, minDateValue, itemsWithItemPrefix);
				found = isItemItem = (searchRecord == null) ? false : true;
			}

			if (!found)
			{
				getAllRecord = utils.isGetAllItem(catalogGetAllItems, technicalName, type);
				found = isGetAllItem = (getAllRecord == null) ? false : true;
			}

			if (found)
			{
				logger.debug("NetSuite - creating stub");
				NetSuitePortType _port = createStub();
				logger.debug("NetSuite - finished creating stub");
				// Creating a Schema/Mapping of all the custom fields to objects
				if (customFiledsSchema == null && !isCustomObject)
				{
					logger.debug("NetSuite - detecting custom columns");
					List<String> technicalNamesList = new ArrayList<String>(logicalToTechnicalNameMap.values());
					if (customObjectsMap != null)
					{
						technicalNamesList.addAll(customObjectsMap.keySet());
					}
					CustomFieldsMap cfm = new CustomFieldsMap(_port, technicalNamesList.toArray(new String[0]));
					customFiledsSchema = cfm.fieldMapping;
					logger.debug("NetSuite - finished detecting custom columns");
				}
				type.lastUpdatedDate = new Date();
				RecordList recordList = null;
				SearchResult searchResult = null;
				GetAllResult getAllResult = null;
				if (isSearchableItem || isTransactionItem || isItemItem)
				{
					logger.debug("NetSuite - starting search");
					searchResult = _port.search(searchRecord);
					processStatus(searchResult.getStatus());
					logger.debug("NetSuite - search result received");
					recordList = searchResult.getRecordList();
				}
				else if (isGetAllItem)
				{
					logger.debug("NetSuite - starting search");
					getAllResult = _port.getAll(getAllRecord);
					processStatus(getAllResult.getStatus());
					logger.debug("NetSuite - search result received");
					recordList = getAllResult.getRecordList();
				}	else if(isCustomObject){
					logger.debug("NetSuite - starting search");

					searchResult = customObjects.getRecords(technicalName, type.useLastModified ? type.lastSystemModStamp : null );

					processStatus(searchResult.getStatus());
					logger.debug("NetSuite - search result received");
					recordList = searchResult.getRecordList();
				}

				if (recordList != null)
				{
					Record[] record = recordList.getRecord();
					if (record != null && record.length > 0)
					{
						Class recClass = record[0].getClass();
						List<String> cnames = new ArrayList<String>();
						Map<String, Method> fieldGetterMap = new HashMap<String, Method>();
						Map<String, Method> fieldTypeGetterMap = new HashMap<String, Method>();
						Map<String, NestedFields> nestedFieldsMap = new HashMap<String, NestedFields>();
						Set<String> listTypeColumns = new HashSet<String>();
						Map<String, Method> listTypeColumnGetter = new HashMap<String, Method>();
						getMetaDataForClass(recClass, cnames, fieldGetterMap, fieldTypeGetterMap, nestedFieldsMap, listTypeColumns, listTypeColumnGetter, null);
						String objName = recClass.getName().substring(recClass.getName().lastIndexOf(".") + 1);
						boolean firstCol = true;
						String customFieldTechName = technicalName;
						if(customFieldTechName.lastIndexOf("Item") > 0)
						{
							customFieldTechName = customFieldTechName.substring(0, customFieldTechName.lastIndexOf("Item"));
						}


						if (isCustomObject){
							cnames =  customObjects.getColNames(technicalName);
						}
						for (String cname : cnames)
						{
							if (nestedFieldsMap.containsKey(cname))
								continue;
							if (firstCol)
								firstCol = false;
							else
								writer.write('|');
							writer.write(cname);
							type.addColumnName(cname);
						}

						if (customFiledsSchema != null && customFiledsSchema.get(customFieldTechName) != null && customFiledsSchema.get(customFieldTechName).size() > 0)
						{
							for (CustomFieldInfo customFieldInfo : customFiledsSchema.get(customFieldTechName))
							{
								if (firstCol)
									firstCol = false;
								else
									writer.write('|');
								writer.write(customFieldInfo.getLabel());
								type.addColumnName(customFieldInfo.getLabel());
							}
						}

						for (int i=0; i<record.length; i++)
						{
							List<String> readRecord = new LinkedList<String>();
							if(isCustomObject){					
								CustomRecord rec = (CustomRecord) record[i];
								// Writing out the values in order of column names.. order should not change
								readRecord.add(rec.getInternalId());
								readRecord.add(rec.getExternalId() == null? "" : rec.getExternalId() );
								readRecord.add(formatValue("IsInactive", rec.getIsInactive(), rec.getIsInactive().getClass(), i));
								//BPD-18086
								try{
									readRecord.add(formatValue("LastModifiedDate", rec.getLastModified(), rec.getLastModified().getClass(), i));
									if ((rec.getLastModified().getTime()).after(maxLastModifiedDate.maxLastModified))
									{
										maxLastModifiedDate.maxLastModified = (rec.getLastModified().getTime());
									}
								}catch (NullPointerException npe){
									readRecord.add(formatValue("LastModifiedDate", "", String.class, i));
								}
								readRecord.add(rec.getName());
								try{	
									readRecord.add(formatValue("CreatedDate", rec.getCreated(), rec.getCreated().getClass(), i));
								}catch (NullPointerException npe){
									readRecord.add(formatValue("CreatedDate", "", String.class, i));
								}

								CustomFieldRef[] customFields = rec.getCustomFieldList().getCustomField();
								for(int colNum = 6; colNum < cnames.size(); colNum++){
									boolean exists = false;
									for(int count = 0; count < customFields.length ; count++){
										//BPD-18109 using label instead of internal id
										String internalId = (String) customFields[count].getClass().getMethod("getInternalId").invoke(customFields[count]);	
										String label = customObjects.getTechToFriendlyForFields(technicalName).get(internalId.toLowerCase());

										if(label.equals(cnames.get(colNum))){
											//BPD-18111
											Object obj;
											if(customFields[count].getClass() == BooleanCustomFieldRef.class){
												obj =  customFields[count].getClass().getMethod("isValue").invoke(customFields[count]);		
											} else {
												obj = customFields[count].getClass().getMethod("getValue").invoke(customFields[count]);
											}

											readRecord.add(formatValue(internalId, obj , obj.getClass(), i));
											exists = true;
											break;
										}
									}
									if(!exists)
										readRecord.add("");
								}

							}else{
								readRecord = readRecord(record[i], cnames, fieldGetterMap, fieldTypeGetterMap, nestedFieldsMap, maxLastModifiedDate, i);
							}
							if (customFiledsSchema != null && customFiledsSchema.get(customFieldTechName) != null && customFiledsSchema.get(customFieldTechName).size() > 0)
							{
								appendCustomFieldValues(customFieldTechName, record[i], readRecord, i);
							}
							writer.write(System.getProperty("line.separator"));
							//write data
							if(hndlr != null && !hndlr.shouldContinue()){
								hndlr.updateProgress(ExtractionStatus.Killed, numRecords);
								throw new UnrecoverableException(type.name + " Extraction Killed", new Exception(type.name + " Extraction Killed"));
							}
							writeRecord(readRecord, writer);
							numRecords++;
							if (hndlr != null && (numRecords % hndlr.getThresholdRecordCount() == 0))
							{
								hndlr.updateProgress(ExtractionStatus.Running, numRecords);
							}
						}
				
						// add to list of data to be deleted. it will be removed from the list once once the parent itself is being extracted.
						if(isDepObject){
							filesToDel.put(technicalName, new SimpleEntry<File, Integer>(file, numRecords));
							numRecords = 0;
						} 
						
						numRecords = utils.writeDependent(recClass, listTypeColumns, technicalName, this.spaceDir, record, objName, customFieldTechName, 
								numRecords, listTypeColumnGetter, cnames, fieldGetterMap, maxLastModifiedDate, this, type, false, isDepObject, isTransactionItem);


						if (isSearchableItem || isTransactionItem)
						{
							int noPages = searchResult.getTotalPages();
							int page = 2;
							if (page < noPages)
							{
								logger.debug("NetSuite - TotalPages in search = " + noPages);
							}
							while (page <= noPages)
							{
								if(hndlr != null && !hndlr.shouldContinue()){
									hndlr.updateProgress(ExtractionStatus.Killed, numRecords);
									throw new UnrecoverableException(type.name + " Extraction Killed", new Exception(type.name + " Extraction Killed"));
								}
								logger.debug("NetSuite - starting searchMoreWithId for page=" + page);
								searchResult =  _port.searchMoreWithId(searchResult.getSearchId(), page);
								processStatus(searchResult.getStatus());
								logger.debug("NetSuite - finished searchMoreWithId for page=" + page);
								recordList = searchResult.getRecordList();
								if (recordList != null)
								{
									record = recordList.getRecord();
									if (record != null && record.length > 0 && !isDepObject)
									{
										for (int i=0; i<record.length; i++)
										{
											List<String> readRecord = readRecord(record[i], cnames, fieldGetterMap, fieldTypeGetterMap, nestedFieldsMap, maxLastModifiedDate, i);
											if (customFiledsSchema != null && customFiledsSchema.get(customFieldTechName) != null && customFiledsSchema.get(customFieldTechName).size() > 0)
											{
												appendCustomFieldValues(customFieldTechName, record[i], readRecord, i);
											}
											writer.write(System.getProperty("line.separator"));											
											writeRecord(readRecord, writer);
											numRecords++;
											if (hndlr != null && (numRecords % hndlr.getThresholdRecordCount() == 0))
											{
												hndlr.updateProgress(ExtractionStatus.Running, numRecords);
											}
										}							
									}

									numRecords = utils.writeDependent(recClass, listTypeColumns, technicalName, this.spaceDir, record, objName, customFieldTechName, 
											numRecords, listTypeColumnGetter, cnames, fieldGetterMap, maxLastModifiedDate, this, type, true, isDepObject, isTransactionItem);

								}
								page++;						
							}
						}
						type.lastSystemModStamp = maxLastModifiedDate.maxLastModified;
						writer.close();
					}
				}								
			}
			else
			{
				throw new Exception("Unknown object: " + type.name);
			}
			
			writer.close();
		}
		catch (Exception e)
		{
			logger.error("Problem extracting NetSuite object - " + type.name, e);
			if (e instanceof IOException)
			{
				throw new RecoverableException(e.getMessage(), e);
			}else
			{
				if (hndlr != null && hndlr.getLoggedStatus() != ExtractionStatus.Killed)
					hndlr.updateProgress(ExtractionStatus.Failed, numRecords);
				throw new UnrecoverableException(e.getMessage(), e);
			}
		}
		long timeTakenInSeconds = (System.currentTimeMillis() - startTime);
		logger.info("Successfully extracted object " + type.name + " : Total Time taken : " + timeTakenInSeconds + "(milliseconds)");
		type.status = ExtractionObject.SUCCESS;
		if (hndlr != null)
			hndlr.updateProgress(ExtractionStatus.Complete, numRecords);
		return numRecords;
	}

	public void wrapUp(){
		for(Entry<String, SimpleEntry<File, Integer>> cur : filesToDel.entrySet()){
			File toDelete = cur.getValue().getKey();
			if (toDelete.exists())
			{
				toDelete.delete();
			}
		}
		
		  // Directory path here
		  String path = spaceDir + File.separator + "tempDepData"; 
		 
		  File folder = new File(path);
		  File[] listOfFiles = folder.listFiles(); 
		 
		  for (int i = 0; i < listOfFiles.length; i++) 
		  {
			  if (listOfFiles[i].isFile()) 
			  {
				  File newFile = listOfFiles[i];
				  
				  if(newFile.length() <= 0){
					  newFile.delete();
					  continue;
				  }
				  String fileName = newFile.getName();
				  
				  // Destination directory
				  File destFile = new File(spaceDir + File.separator + "data" + File.separator + fileName);
				  //deleting the file if it exists
				  if(destFile.exists()){
					  destFile.delete();
				  }
				 
				  // Move file to new directory
				  boolean success = newFile.renameTo(destFile);
				  if (!success) {
					  logger.error("-- Netsuite Extraction -- Can't copy the new file: " + fileName +" to the \"data\" folder");
				  }
			  }
		  }
		
	}

	private void appendCustomFieldValues(String objName, Object record, List<String> readRecord, int recordNum) throws Exception
	{
		if (customFiledsSchema.get(objName).size() > 0)
		{
			Method getCustomFields = null;
			CustomFieldList customFieldList = null;
			try
			{
				getCustomFields = record.getClass().getMethod("getCustomFieldList", new Class[] { });
				customFieldList = (CustomFieldList) getCustomFields.invoke(record, new Object[] { });
			} catch (NoSuchMethodException e){} 
			for (CustomFieldInfo customFieldInfo : customFiledsSchema.get(objName))
			{
				boolean foundCustomFieldValue = false;
				if (customFieldList != null)
				{
					CustomFieldRef[] customFields = customFieldList.getCustomField();
					if (customFields != null && customFields.length > 0)
					{
						for (CustomFieldRef cf : customFields)
						{
							String internalId = (String) cf.getClass().getMethod("getInternalId").invoke(cf);
							if (internalId.equalsIgnoreCase(customFieldInfo.getInternalId()))
							{
								// BPD-17928 Handling Boolean Objects. They oddly use isValue instead of getValue  
								Method getValue = null;
								try {
									getValue = cf.getClass().getMethod("getValue");
								} catch (NoSuchMethodException e){
									getValue = cf.getClass().getMethod("isValue");
								} 

								readRecord.add(formatValue(customFieldInfo.getLabel(), getValue.invoke(cf), getValue.getReturnType(), recordNum));
								foundCustomFieldValue = true;
								break;
							}
						}
					}
				}
				if (!foundCustomFieldValue)
				{
					readRecord.add("");
				}
			}
		}
	}

	protected int writeDependentSources(Record[] record, String objName, String customFieldTechName, Method internalIDGetter, Map<String, Writer> listWriterMap, String listTypeColumn, Method listTypeColumnGetter,
			boolean writeHeader, List<String> cnames, Map<String, Method> fieldGetterMap, MaxLastModifiedDate maxLastModifiedDate, String objFriendlyName, boolean isTrans) throws Exception
			{
		Map<String, List<String>> listColumns = new HashMap<String, List<String>>();
		Map<String, Map<String, Method>> listGetterMap = new HashMap<String, Map<String, Method>>();
		Map<String, Map<String, Method>> listTypeGetterMap = new HashMap<String, Map<String, Method>>();
		Map<String, Map<String, NestedFields>> listNestedFieldsMap = new HashMap<String, Map<String, NestedFields>>();
		int recCount = 0;
		for (int i=0; i<record.length; i++)
		{
			Method idGetter = internalIDGetter;
			String internalID = null;
			try
			{
				idGetter = record[i].getClass().getMethod(internalIDGetter.getName(), new Class[] { });
				internalID = (String)idGetter.invoke(record[i], new Object[] {});
			}
			catch (NoSuchMethodException ne)
			{
				internalID = null;
			}
			Method listColumnGetter = listTypeColumnGetter;
			Object wrapperObj = null;
			try
			{
				listColumnGetter = record[i].getClass().getMethod(listTypeColumnGetter.getName(), new Class[] { });
				wrapperObj = listColumnGetter.invoke(record[i], new Object[] {});
			}
			catch (NoSuchMethodException ne)
			{
				wrapperObj = null;
			}
			if (wrapperObj != null)
			{
				Method getter = fieldGetterMap.get(listTypeColumn);
				Object arrObj = null;
				try
				{
					getter = wrapperObj.getClass().getMethod( fieldGetterMap.get(listTypeColumn).getName(), new Class[] { });
					arrObj = getter.invoke(wrapperObj, new Object[] { });
				}
				catch (NoSuchMethodException ne)
				{
					arrObj = null;
				}				
				if (arrObj != null)
				{
					Object[] array = unpackArray(arrObj);
					Writer writer = listWriterMap.get(listTypeColumn);
					if (listColumns.get(listTypeColumn) == null && array != null && array.length > 0)
					{
						List<String> colNames = new ArrayList<String>();
						Map<String, Method> colFieldGetterMap = new HashMap<String, Method>();
						Map<String, Method> colFieldTypeGetterMap = new HashMap<String, Method>();
						Map<String, NestedFields> colNestedFieldsMap = new HashMap<String, NestedFields>();
						getMetaDataForClass(array[0].getClass(), colNames, colFieldGetterMap, colFieldTypeGetterMap, colNestedFieldsMap, new HashSet<String>(), new HashMap<String, Method>(), null);
						if (colNames.size() == 0)
						{
							Method getValueMethod = null;
							try
							{
								getValueMethod = array[0].getClass().getMethod("getValue", new Class[] { });
							}
							catch (NoSuchMethodException ex)
							{}
							if (getValueMethod != null)
							{
								colNames.add("Value");
								colFieldGetterMap.put("Value", getValueMethod);
							}
						}
						listColumns.put(listTypeColumn, colNames);
						listGetterMap.put(listTypeColumn, colFieldGetterMap);
						listTypeGetterMap.put(listTypeColumn, colFieldTypeGetterMap);
						listNestedFieldsMap.put(listTypeColumn, colNestedFieldsMap);
						if (writeHeader)
						{
							StringBuilder sb = new StringBuilder();
							sb.append(objName).append("_InternalId");
							for (String colname : listColumns.get(listTypeColumn))
							{
								if (colNestedFieldsMap.containsKey(colname))
									continue;
								sb.append("|");
								sb.append(colname);	
								dos.addColumn(objFriendlyName, colname);
							}
							writer.write(sb.toString());
							if (isTrans && customFiledsSchema != null && customFiledsSchema.get(customFieldTechName) != null && customFiledsSchema.get(customFieldTechName).size() > 0)  
															{  
															boolean firstCol = true;   
															for (CustomFieldInfo customFieldInfo : customFiledsSchema.get(customFieldTechName))  
															{  
																	writer.write('|');  
																	writer.write(customFieldInfo.getLabel());																	  
																}  
														} 
						}						
					}

					for (Object obj : array)
					{
						List<String> readRecord = readRecord(obj, listColumns.get(listTypeColumn), listGetterMap.get(listTypeColumn), listTypeGetterMap.get(listTypeColumn), listNestedFieldsMap.get(listTypeColumn), maxLastModifiedDate, i);
						if (isTrans && customFiledsSchema != null && customFiledsSchema.get(customFieldTechName) != null && customFiledsSchema.get(customFieldTechName).size() > 0)  
						{  
							appendCustomFieldValues(customFieldTechName, obj, readRecord, i);  
						}
						readRecord.add(0, internalID);
						writer.write(System.getProperty("line.separator"));
						writeRecord(readRecord, writer);
						recCount++;
					}
				}
			}
		}
		return recCount;
			}
	

	private void getMetaDataForClass(Class recClass, List<String> cnames, Map<String, Method> fieldGetterMap, Map<String, Method> fieldTypeGetterMap, 
			Map<String, NestedFields> nestedFieldsMap, Set<String> listTypeColumns, Map<String, Method> listTypeColumnGetter, Map<String, Method> stdObjFieldGetterMap)
	{
		for (Field field : recClass.getDeclaredFields())
		{
			String fname = field.getName();
			if (field.getType().equals(CustomFieldList.class) || field.getType().equals(TypeDesc.class))
				continue;
			fname = fname.substring(0, 1).toUpperCase() + fname.substring(1);
			String nestedFieldName = fname;
			if (stdObjFieldGetterMap != null && stdObjFieldGetterMap.containsKey(fname))
			{
				nestedFieldName = fname + "_" + fname;
			}
			try
			{
				Method getter = recClass.getMethod("get" + fname, new Class[] {});
				Class retType = getter.getReturnType();
				if (retType != null && retType.isArray() && !retType.getComponentType().isPrimitive())
				{
					//don't add as column
					//need to create separate source for dependent list
					listTypeColumns.add(nestedFieldName);
					fieldGetterMap.put(nestedFieldName, getter);
					return;
				}
				else if (retType != null && !retType.equals(RecordRef.class) && retType.getPackage() != null && retType.getPackage().getName().startsWith("com.netsuite.webservices"))
				{
					boolean foundGetter = false;
					try
					{
						Method typeGetter = retType.getMethod("getValue", new Class[] {});
						fieldGetterMap.put(nestedFieldName, getter);
						fieldTypeGetterMap.put(nestedFieldName, typeGetter);
						cnames.add(nestedFieldName);
						foundGetter = true;
					}
					catch (NoSuchMethodException ex)
					{						
					}
					if (!foundGetter) //gets nested object - get columns from inner class
					{
						NestedFields nestedFields = new NestedFields();
						getMetaDataForClass(retType, nestedFields.nestedCnames, nestedFields.nestedFieldGetterMap, nestedFields.nestedfieldTypeGetterMap, null, nestedFields.nestedListTypeColumns, nestedFields.nestedListTypeColumnGetter, fieldGetterMap);
						if (nestedFields.nestedCnames.size() > 0)
						{
							cnames.add(nestedFieldName);
							fieldGetterMap.put(nestedFieldName, getter);
							nestedFieldsMap.put(nestedFieldName, nestedFields);
							cnames.addAll(nestedFields.nestedCnames);
							fieldTypeGetterMap.putAll(nestedFields.nestedfieldTypeGetterMap);
						}
						else
						{
							for (String col : nestedFields.nestedListTypeColumns)
								listTypeColumnGetter.put(col, getter);
						}
						listTypeColumns.addAll(nestedFields.nestedListTypeColumns);
						fieldGetterMap.putAll(nestedFields.nestedFieldGetterMap);
					}
				} 
				else
				{
					fieldGetterMap.put(nestedFieldName, getter);
					cnames.add(nestedFieldName);					
				}
			}
			catch (NoSuchMethodException ex) 
			{
				continue;
			}							
		}
	}

	public class NestedFields
	{
		List<String> nestedCnames = new ArrayList<String>();
		Map<String, Method> nestedFieldGetterMap = new HashMap<String, Method>();
		Map<String, Method> nestedfieldTypeGetterMap = new HashMap<String, Method>();
		Set<String> nestedListTypeColumns = new HashSet<String>();
		Map<String, Method> nestedListTypeColumnGetter = new HashMap<String, Method>();
	}

	private void processStatus(Status status) throws Exception
	{
		if (status != null)
		{
			if (status.isIsSuccess())
				return;
			StatusDetail[] statusDetails = status.getStatusDetail();
			if (statusDetails != null && statusDetails.length > 0)
			{
				StringBuilder sb = new StringBuilder();
				if (statusDetails != null)
				{
					for (StatusDetail statusDetail : statusDetails)
					{
						sb.append("\n").append(statusDetail.getCode()).append(" - ").append(statusDetail.getMessage());
					}
				}
				logger.error(sb.toString());
				throw new Exception(sb.toString());
			}
		}
	}

	public class MaxLastModifiedDate
	{
		public Date maxLastModified;
	}

	protected List<String> readRecord(Object record, List<String> cnames, Map<String, Method> fieldGetterMap, Map<String, Method> fieldTypeGetterMap, 
			Map<String, NestedFields> nestedFieldsMap, MaxLastModifiedDate maxLastModifiedDate, int recordNum) throws Exception
			{
		List<String> recordList = new ArrayList<String>();
		for (int i = 0; i < cnames.size(); i++)
		{
			String cname = cnames.get(i);
			Method method = fieldGetterMap.get(cname);
			Object value = null;
			try
			{
				method = record.getClass().getMethod(fieldGetterMap.get(cname).getName(), new Class[] { });
				value = method.invoke(record, new Object[] { });
			}
			catch (NoSuchMethodException ne)
			{
				value = null;
			}
			boolean isNested = false;
			if (nestedFieldsMap.containsKey(cname))
			{
				isNested = true;
				NestedFields nf = nestedFieldsMap.get(cname);
				for (String nestedCname : nf.nestedCnames)
				{
					if (value != null)
					{
						Method nestedMethod = nf.nestedFieldGetterMap.get(nestedCname);
						Object nestedValue = nestedMethod.invoke(value, new Object[] { });
						recordList.add(formatValue(nestedCname, nestedValue, nestedMethod.getReturnType(), recordNum));
					}
					else
					{
						recordList.add("");
					}
					i++;
				}				
			}
			else if (fieldTypeGetterMap.containsKey(cname) && value != null)
			{
				value = fieldTypeGetterMap.get(cname).invoke(value, new Object[] { });
			}
			if (!isNested)
			{
				recordList.add(formatValue(cname, value, method.getReturnType(), recordNum));
				if (cname.equals("LastModifiedDate") && value != null)
				{
					Date val = dateTimeFormatter.parse(recordList.get(recordList.size() - 1));
					if (val.after(maxLastModifiedDate.maxLastModified))
					{
						maxLastModifiedDate.maxLastModified = val;
					}
				}
			}
		}			
		return recordList;
			}

	private String formatValue(String name, Object val, Class valClass, int recordNum)
	{
		String formattedValue = "";
		try
		{
			// BPD-17936 if Gregorian calendar is returned then get Date.class type and allow that to be formatted.
			if (valClass == GregorianCalendar.class) {
				val = ((GregorianCalendar) val).getTime();
				valClass = Date.class;
			}

			//BPD-17932
			if (valClass == ListOrRecordRef.class){
				if (val == null)
				{
					return "";
				} else {
					return ( (ListOrRecordRef) val).getInternalId();
				}

			}
			if (valClass == ListOrRecordRef[].class){
				if (val == null)
				{
					return "";
				} else { 
					ListOrRecordRef[] temp = ((ListOrRecordRef[]) val);
					for(int i = 0; i < temp.length; i++){
						if(i == 0){
							formattedValue = temp[i].getInternalId();
						} else {
							formattedValue += "~~" + temp[i].getInternalId();
						}
					}
					return formattedValue;
				}

			}

			if (valClass == Date.class)
			{
				if (val == null)
				{
					formattedValue = "";
				}
				else
				{
					formattedValue = dateTimeFormatter.format(((Date)val));
					if (!formattedValue.endsWith("T00:00:00"))
					{
						formattedValue = dateFormatter.format(((Date)val));
					}
				}
			}
			else if (valClass == Calendar.class)
			{
				if (val == null)
				{
					formattedValue = "";
				}
				else
				{
					formattedValue = dateTimeFormatter.format(((Calendar)val).getTime());
					if (!formattedValue.endsWith("T00:00:00"))
					{
						formattedValue = dateFormatter.format(((Calendar)val).getTime());
					}
				}
			}
			else if (valClass == String.class || valClass == Integer.class || valClass == Long.class || 
					valClass == Float.class || valClass == Double.class || valClass == Number.class)
			{
				if (val == null)
				{
					formattedValue = "";
				}
				else
				{
					formattedValue = String.valueOf(val);
				}
			}
			else if (valClass == Boolean.class)
			{
				if (val == null)
					formattedValue = Boolean.FALSE.toString();
				else
					formattedValue = ((Boolean)val).toString();
			}
			else if (valClass == RecordRef.class)
			{
				if (val == null)
				{
					formattedValue = "";
				}
				else
				{
					RecordRef ref = (RecordRef)val;
					formattedValue = ref.getInternalId();
				}
			}
			else
			{
				if (val == null)
					formattedValue = "";
				else
					formattedValue = String.valueOf(val);				
			}
		}
		catch (Exception ex)
		{
			logger.warn("record # " + (recordNum + 1) + " - Could not discover metadata for field: " + name + " fieldtype: " + valClass);
			String value = String.valueOf(val);
			formattedValue = value;
		}

		return formattedValue;
	}

	private void writeRecord(List<String> record, Writer writer) throws Exception
	{
		boolean firstCol = true;
		StringBuilder sb = new StringBuilder();
		for (String s : record)
		{
			if (firstCol)
				firstCol = false;
			else
				sb.append("|");                									
			if (s.contains("\r") || s.contains("\n") || s.contains("\"") || s.contains("|") || s.contains("\\"))
				sb.append('"').append(s.replace("\\", "\\\\").replace("\"", "\\\"").replace("|", "\\|") + '"');
			else
				sb.append(s);			
		}
		writer.write(sb.toString());
	}


	private Object[] unpackArray(Object array) 
	{
		Object[] array2 = new Object[Array.getLength(array)];
		for(int i=0;i<array2.length;i++)
			array2[i] = Array.get(array, i);
		return array2;
	}


	/**
	 * create a new instance of NetSuitePortType
	 * @throws Exception
	 */
	private synchronized NetSuitePortType createStub() throws Exception
	{
		NetSuiteServiceLocator service = new NetSuiteServiceLocator();
		NetSuitePortType _port = service.getNetSuitePort();
		if (useSandBoxURL && sandboxURL != null && !sandboxURL.trim().isEmpty())
		{
			_port = service.getNetSuitePort(new URL(sandboxURL));
		}
		else
		{
			GetDataCenterUrlsResult res = _port.getDataCenterUrls(accountID);
			processStatus(res.getStatus());
			String wsEndpoint = res.getDataCenterUrls().getWebservicesDomain() + "/services/NetSuitePort_" + metaData.getConnectorCloudVersion();
			_port = service.getNetSuitePort(new URL(wsEndpoint));
		}
		((NetSuiteBindingStub) _port).setTimeout(timeoutInMilliseconds);
		setPreferences(_port);
		return _port;
	}

	public static String getLogicalToTechnicalNameMap(String key) {
		return logicalToTechnicalNameMap.get(key);
	}

	@Override
	public void fillObjectDetails(ExtractionObject type) throws RecoverableException, UnrecoverableException{
		// TODO Auto-generated method stub
	}

	@Override
	public boolean validateQueryForObject(ExtractionObject type)
			throws RecoverableException, UnrecoverableException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean validateObject(ExtractionObject type)
			throws RecoverableException, UnrecoverableException {
		return false;
	}
}
