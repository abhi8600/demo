package com.successmetricsinc.connectors;

/**
 * 
 * @author mpandit
 *
 */
public class RecoverableException extends Exception {
	public RecoverableException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
