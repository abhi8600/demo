package com.successmetricsinc.liveaccessdirectconnect;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.Statement;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;
import org.olap4j.OlapConnection;
import org.olap4j.OlapStatement;

import com.birst.dataconductor.PutResults;
import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.security.EncryptionService;

public class DirectConnectUtil {
	
	private static final Logger logger = Logger.getLogger(DirectConnectUtil.class);
	public static final String NODE_ROOT = "Root";
	public static final String NODE_SUCCESS = "Success";
	public static final String NODE_REASON = "Reason";
	public static final String NODE_RESULT = "Result";
	public static final String NODE_ROW = "Row";
	
	public OMElement importLiveAccessMetaDataUsingDirectConnection(String userName, String spaceID, Repository r, String realtimeConnection, String command, 
			String commandArgs, Session session)
	{
		OMNamespace ns = null;
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement root = factory.createOMElement(NODE_ROOT, ns);
		boolean success = false;
		String errorMessage = null;
		try
		{
			DatabaseConnection dc =  r.findConnection(realtimeConnection);
			if (dc == null)
				throw new Exception("LiveAccess connection " + realtimeConnection + " not found");
			
			com.birst.dataconductor.DatabaseConnection rDC = getBCDatabaseConnection(dc, session);
			
			PutResults pr = new com.birst.dataconductor.PutResults(dc.Name, rDC, null, -1, command, r.getServerParameters()
					.getProcessingTimeZone(), commandArgs, spaceID, null, null, null, r.getServerParameters().getMaxRecords());
			Connection connection = null;
			if (dc.ConnectionPool.isDynamicConnnection() && session != null)
				connection = dc.ConnectionPool.getDynamicConnection(session);
			else
				connection = dc.ConnectionPool.getConnection();
			Statement statement = null;
			if (dc.Driver.equals("org.apache.hadoop.hive.jdbc.HiveDriver") || dc.Driver.equals("org.olap4j.driver.xmla.XmlaOlap4jDriver"))
			{
				statement = connection.createStatement();
			}
			else
			{
				statement = connection.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
			}
			ByteArrayOutputStream baos = null;
			if (dc.Driver.equals("org.olap4j.driver.xmla.XmlaOlap4jDriver"))
			{
				baos = pr.getXMLAOutputStream((OlapConnection)connection, (OlapStatement)statement, true);
			}
			else
			{
				baos = pr.getSQLOutputStream(connection, statement, true);
			}
			if (baos != null)
			{
				String s = new String(baos.toByteArray(), "UTF-8");
				BufferedReader br = new BufferedReader(new StringReader(s));
				if (s.startsWith("Error: ") || s.equals("EMPTY"))
				{
					throw new Exception(s);
				}
				OMElement resultChild = factory.createOMElement(NODE_RESULT, ns);
				String line = null;
                while ((line = br.readLine()) != null)
                {
                    if (line.length() > 0)
                    {
                        OMElement rowChild = factory.createOMElement(NODE_ROW, ns);
                    	rowChild.setText(line);
                    	resultChild.addChild(rowChild);
                    }                    
                }
                root.addChild(resultChild);				
                success = true;
			}
			else
			{
				throw new Exception("Could not get result from live access connection");
			}
		}
		catch (Exception ex)
		{
			errorMessage = ex.getMessage();
			logger.error(ex.toString(), ex);
		}
		OMElement successChild = factory.createOMElement(NODE_SUCCESS, ns);
		successChild.setText(String.valueOf(success));
		root.addChild(successChild);
		if (!success && errorMessage != null)
		{
			OMElement errMsgChild = factory.createOMElement(NODE_REASON, ns);
			errMsgChild.setText(errorMessage);
			root.addChild(errMsgChild);
		}		
		return root;
	}
	
	public static com.birst.dataconductor.DatabaseConnection getBCDatabaseConnection(DatabaseConnection databaseConnection, Session session)
	{
		com.birst.dataconductor.DatabaseConnection rDC = new com.birst.dataconductor.DatabaseConnection();
		String connectString = databaseConnection.ConnectString;
		String userName = databaseConnection.UserName;
		String password = databaseConnection.Password;
		if (DatabaseConnection.isDynamicConnection(connectString) && session != null && databaseConnection.ConnectionPool != null
				&& databaseConnection.ConnectionPool.isDynamicConnnection())
		{
			EncryptionService enc = EncryptionService.getInstance();
			connectString = session.getRepository().replaceVariables(session, connectString);
			userName = session.getRepository().replaceVariables(session, userName);
			password = enc.encrypt(session.getRepository().replaceVariables(session, enc.decrypt(password)));
			rDC.genericDriver = true;
			rDC.setDatabaseName(connectString);
		} else
		{
			if (connectString.indexOf(";Catalog=") < 0)//in case of generic database connection, catalog is expected to be part of connectstring
			{
				rDC.serverName = connectString.substring(0, connectString.lastIndexOf("://"));
				rDC.setDatabaseName(connectString.substring(connectString.lastIndexOf("://") + "://".length()));
			}
			else
			{
				rDC.genericDriver = true;
				rDC.setDatabaseName(connectString);
			}
		}
		com.birst.dataconductor.EncryptionService.setKey("encryptionkey=" + EncryptionService.getKey());
		rDC.driverName = databaseConnection.Driver;
		rDC.setUsername(userName);
		rDC.setPassword(password);
		rDC.setDatabaseType(databaseConnection.getDBTypeNameForXMLAConnection());
		return rDC;
	}
	
	public OMElement getResponseError(String errorMessage)
	{		
		OMNamespace ns = null;
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement root = factory.createOMElement(NODE_ROOT, ns);		
		OMElement successChild = factory.createOMElement(NODE_SUCCESS, ns);
		successChild.setText(String.valueOf(false));
		root.addChild(successChild);
		if (errorMessage != null && errorMessage.trim().length() > 0)
		{
			OMElement errMsgChild = factory.createOMElement(NODE_REASON, ns);
			errMsgChild.setText(errorMessage);
			root.addChild(errMsgChild);
		}
		return root;
	}
}
