/**
 * $Id: CustomDrill.java,v 1.7 2011-09-06 21:45:47 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc;

import java.util.List;
import java.util.ArrayList;
import java.io.IOException;
import java.io.Serializable;

import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jdom.Namespace;

public class CustomDrill implements Serializable
{
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(CustomDrill.class);
	private String name;
	private List<String> urls = new ArrayList<String>();
	private List<Function> functions = new ArrayList<Function>();
	
	@SuppressWarnings("unchecked")
	public CustomDrill(Element e, Namespace ns) throws IOException
	{
		name = e.getChildText("Name", ns);
		// get list of URLs
		Element cn = e.getChild("URLs", ns);
		if (cn != null)
		{
			List<Element> urllist = cn.getChildren();
			if (urllist != null)
			{
				for (Element item : urllist)
				{
					String url = item.getTextTrim();
					urls.add(url);
				}
			}
		}

		// get list of functions
		Element fe = e.getChild("Functions", ns);
		if (fe != null)
		{
			List<Element> functionlist = fe.getChildren("Function", ns);
			if (functionlist != null)
			{
				for (Element item : functionlist)
				{
					Function tgt = new Function(item, ns);
					functions.add(tgt);
				}
			}
		}
	}
	
	public Element getCustomDrillElement(Namespace ns)
	{
	  Element e = new Element("CustomDrill",ns);
	  
	  Element child = new Element("Name",ns);
    child.setText(name);
    e.addContent(child);
    
    if (urls!=null && !urls.isEmpty())
    {
      Repository.getStringArrayElement("URLs", urls, ns);
    }
    
    if (functions!=null && !functions.isEmpty())
    {
      child = new Element("Functions",ns);
      for (Function function : functions)
      {
        child.addContent(function.getFunctionElement(ns));
      }
      e.addContent(child);
    }
	  
	  return e;
	}
	
	/**
	 * Return a list of URLs associated with this custom drill object
	 * @return List<String>
	 */
	public List<String> getUrls() {
		return urls;
	}

	/**
	 * Return a list of Functions associated with this custom drill object
	 * @return List<Function>
	 */
	public List<Function> getFunctions() {
		return functions;
	}
	
	/**
	 * Return the name of the custom drill object
	 * @return String
	 */
	public String getName() {
		return name;
	}
	
	public String toString()
	{
		StringBuilder sb = new StringBuilder("CustomDrill: " + getName() + "\n");
		List<String> urls = getUrls();
		if (urls != null)
		{
			for (String url : urls)
			{
				sb.append("\tURL: " + url + "\n");
			}
		}
		List<Function> functions = getFunctions();
		if (functions != null)
		{
			for (Function tgt : functions)
			{
				sb.append("\t" + tgt.toString() + "\n");
			}
		}
		return sb.toString();
	}
	
	public static class Function implements Serializable
	{
		private static final long serialVersionUID = 1L;
		private String name;		// name of the javascript function
		private String description;	// description - mostly list of parameters
		private String label;
		private String labelOrig;
		
		public Function(Element e, Namespace ns)
		{
			name = e.getChildTextTrim("Name", ns);
			label = e.getChildTextTrim("Label", ns);
			labelOrig = e.getChildTextTrim("Label", ns);
			if (label == null || label.isEmpty()) {
				label = name;
			}
			description = e.getChildTextTrim("Description", ns);
		}
		
		public Element getFunctionElement(Namespace ns)
		{
		  Element e = new Element("Function",ns);
	    
	    Element child = new Element("Name",ns);
	    child.setText(name);
	    e.addContent(child);
	    
	    child = new Element("Description",ns);
      child.setText(description);
      e.addContent(child);
      
      child = new Element("Label",ns);
      child.setText(labelOrig);
      e.addContent(child);
	        
	    return e;
		}
		

		/**
		 * Returns the label for the function
		 * @return	A descriptive label for the function
		 */
		public String getLabel() {
			return label;
		}
		/**
		 * Return the name of the Function
		 * @return String
		 */
		public String getName() {
			return name;
		}

		/**
		 * Return the description of the Function
		 * @return String
		 */
		public String getDescription() {
			return description;
		}
		
		public String toString() {
			return "Function: Name: " + name + ", description: " + description + ", label: " + label;
		}
	}
}