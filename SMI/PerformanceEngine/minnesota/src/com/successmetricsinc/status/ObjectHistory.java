/**
 * $Id: ObjectHistory.java,v 1.7 2012-05-21 13:22:23 mpandit Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.status;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.query.Database;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.Query;

public class ObjectHistory
{
	private static Logger logger = Logger.getLogger(ObjectHistory.class);
	private Repository r;
	private String iteration;
	private String tableName;
	private DatabaseConnection dc;
	private Map<Integer, String> physicalNameMap = null;
	List<String> outputColumns = null;
	List<String> outputColumnTypes = null;

	public ObjectHistory(Repository r, String iteration)
	{
		this.r = r;
		this.iteration = iteration;
		try
		{
			dc = r.getDefaultConnection();
			tableName = "TXN_OBJECT_HISTORY";
			outputColumns = new ArrayList<String>();
			outputColumnTypes = new ArrayList<String>();
			setColumnNamesAndTypesForObjectHistory();
			if (!Database.tableExists(r, dc, tableName, true))
			{
				Database.createTable(r, dc, tableName, outputColumns, outputColumnTypes, false, false, null, null, null);
				Database.createIndex(dc, "ITERATION", tableName);
			}
			else if (r.getCurrentBuilderVersion() < 31)
			{
				Connection conn = null;
				try
				{
					conn = dc.ConnectionPool.getConnection();
					List<Object[]> collist = Database.getTableSchema(dc, conn, null, dc.Schema, tableName);
					boolean maxLogicalNameSize = true;
					for (int i = 0; i < collist.size(); i++)
					{
						String columnName = (String) collist.get(i)[0];
						if (columnName.equalsIgnoreCase("LOGICALNAME"))
						{
							int colSize = (Integer) collist.get(i)[3];
							if (colSize < dc.getMaxVarcharWidth())
							{
								maxLogicalNameSize = false;
								break;
							}
						}
					}
					if (!maxLogicalNameSize)
					{		
						if (DatabaseConnection.isDBTypeMySQL(dc.DBType) || DatabaseConnection.isDBTypeParAccel(dc.DBType))
						{
							alterObjectHistory();
						}
						else
						{
							List<String> columnNames = new ArrayList<String>();
							List<String> columnTypes = new ArrayList<String>();
							columnNames.add("LOGICALNAME");
							columnTypes.add(dc.getVarcharType(dc.getMaxVarcharWidth(), false));
							List<Integer> alterActions = new ArrayList<Integer>();
							alterActions.add(Database.ALTER_COLUMN);
							Database.alterTable(r, dc, tableName, columnNames, columnTypes, null, alterActions);
						}
						logger.debug("Successfully modified length of logicalname column in txn_object_history to maximum");
					}
				}
				catch (SQLException e)
				{
					logger.warn("Could not modify LOGICALNAME  column in TXN_OBJECT_HISTORY table", e);
				}
			}
		} catch (SQLException e)
		{
		}
	}
	
	private void setColumnNamesAndTypesForObjectHistory()
	{
		outputColumns.add("TM");
		outputColumnTypes.add(dc.getDateTimeType());
		outputColumns.add("CLASS");
		outputColumnTypes.add(dc.getVarcharType(30, false));
		outputColumns.add("ID");
		outputColumnTypes.add("INTEGER");
		outputColumns.add("ITERATION");
		outputColumnTypes.add(dc.getVarcharType(20, false));
		outputColumns.add("LOGICALNAME");
		outputColumnTypes.add(dc.getVarcharType(dc.getMaxVarcharWidth(), false));		
		outputColumns.add("PHYSICALNAME");
		outputColumnTypes.add(dc.getVarcharType(255, false));
	}
	
	private void alterObjectHistory() throws SQLException
	{
		String tempObjHistoryName = tableName+"_TEMP";
		
		Database.createTable(r, dc, tempObjHistoryName, outputColumns, outputColumnTypes, false, false, null, null, null);
		Database.createIndex(dc, "ITERATION", tempObjHistoryName);
		
		StringBuilder sbInsertInto = new StringBuilder();
		sbInsertInto.append("INSERT INTO "+dc.Schema+"."+tempObjHistoryName+" SELECT * FROM "+dc.Schema+"."+tableName);
		
		Statement stmt = dc.ConnectionPool.getConnection().createStatement();
		try
		{
			logger.debug(Query.getPrettyQuery(sbInsertInto.toString()));
			stmt.executeUpdate(sbInsertInto.toString());
						
			Database.dropTable(r, dc, tableName, false);
			Database.renameTable(r, dc, tempObjHistoryName, tableName);
		}
		finally
		{
			if (stmt != null)
				stmt.close();
		}
	}

	public void clearHistory()
	{
		try
		{
			Map<String, String> valueByColumn = new HashMap<String, String>();
			valueByColumn.put("ITERATION", iteration);
			Database.deleteOldData(r.getDefaultConnection(), tableName, valueByColumn);
		} catch (SQLException e)
		{
			logger.error("Could not clear object history in the database");
		}
	}

	/**
	 * Get the physical name for a given table from it's ID
	 * 
	 * @param iteration
	 * @param ID
	 * @return
	 */
	public String getPhysicalNameFromID(int ID)
	{
		try
		{
			if (physicalNameMap == null)
			{
				physicalNameMap = new HashMap<Integer, String>();
				Connection conn = dc.ConnectionPool.getConnection();
				PreparedStatement pstmt = null;
				ResultSet res = null;
				try
				{
					pstmt = conn.prepareStatement("SELECT ID, PHYSICALNAME FROM " + Database.getQualifiedTableName(dc, tableName) + " WHERE ITERATION=?");
					pstmt.setString(1, iteration);
					res = pstmt.executeQuery();
					if (res != null)
					{
						physicalNameMap = new HashMap<Integer, String>();
						while (res.next())
						{
							int id = res.getInt(1);
							String pname = res.getString(2);
							physicalNameMap.put(id, pname);
						}
					} else
					{
						return null;
					}
				}
				catch (SQLException sex)
				{
					logger.error("Could not retrieve the object history information from the database");
				}
				finally
				{
					try
					{
						if (res != null)
							res.close();
						if (pstmt != null)
							pstmt.close();
					}
					catch (SQLException e)
					{
						logger.debug(e, e);
					}
				}
			}
			return physicalNameMap.get(ID);
		} catch (SQLException e)
		{
			logger.error("Could not retrieve the object history information from the database");
		}
		return null;
	}

	/**
	 * Get the physical name for a given table from it's ID
	 * 
	 * @param iteration
	 * @param ID
	 * @return
	 */
	public List<Integer> getIDsFromPhysicalName(String physicalName)
	{
		try
		{
			Connection conn = dc.ConnectionPool.getConnection();
			PreparedStatement pstmt = null;
			ResultSet res = null;
			try
			{
				List<Integer> result = new ArrayList<Integer>();
				pstmt = conn.prepareStatement("SELECT ID FROM " + Database.getQualifiedTableName(dc, tableName) + " WHERE ITERATION=? AND PHYSICALNAME=?");
				pstmt.setString(1, iteration);
				pstmt.setString(2, physicalName);
				res = pstmt.executeQuery();
				while (res.next())
				{
					int id = res.getInt(1);
					result.add(id);
				}
				return result;
			}
			catch (SQLException e)
			{
				logger.error("Could not retrieve the object history information from the database");
			}
			finally
			{
				try
				{
					if (res != null)
						res.close();
					if (pstmt != null)
						pstmt.close();
				}
				catch (SQLException sex)
				{
					logger.debug(sex);
				}
			}
		} catch (SQLException e)
		{
			logger.error("Could not retrieve the object history information from the database");
		}
		return null;
	}

	/**
	 * log status for the meta-commands only
	 * 
	 * @param step
	 * @param substep
	 * @param iteration
	 * @param status
	 * 
	 */
	public void addEntry(String classname, int ID, String logicalName, String physicalName)
	{
		try
		{
			Connection conn = dc.ConnectionPool.getConnection();
			String getdate = dc.getGetDate();
			PreparedStatement pstmt = null;
			try
			{
				pstmt = conn.prepareStatement("INSERT INTO " + Database.getQualifiedTableName(dc, tableName)
						+ " (TM, ITERATION, CLASS, ID, LOGICALNAME, PHYSICALNAME) VALUES (" + getdate + ",?,?,?,?,?)");
				pstmt.setString(1, iteration);
				pstmt.setString(2, classname);
				pstmt.setInt(3, ID);
				pstmt.setString(4, logicalName);
				pstmt.setString(5, physicalName);
				pstmt.execute();
			}
			catch (SQLException e)
			{
				logger.warn("Could not save object history information in the database");
			}
			finally
			{
				try
				{
					if (pstmt != null)
						pstmt.close();
				}
				catch (SQLException e)
				{
					logger.debug(e, e);
				}
			}
		} catch (SQLException e)
		{
			logger.warn("Could not save object history information in the database");
		}
	}

	public String getIteration()
	{
		return iteration;
	}

	public void setIteration(String iteration)
	{
		this.iteration = iteration;
	}
}