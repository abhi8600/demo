/**
 * $Id: ModelingHistory.java,v 1.12 2012-05-21 13:22:23 mpandit Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.status;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import weka.classifiers.Classifier;
import weka.core.Drawable;

import com.successmetricsinc.Repository;
import com.successmetricsinc.engine.BestFitResult;
import com.successmetricsinc.query.Database;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.util.Util;

public class ModelingHistory
{
	private static Logger logger = Logger.getLogger(ModelingHistory.class);
	private String iteration;
	private String tableName;
	private DatabaseConnection dc;
	Repository r;

	public ModelingHistory(Repository r, String iteration)
	{
		this.r = r;
		this.iteration = iteration;
		tableName = "TXN_MODELING_HISTORY";
		try
		{
			dc = r.getDefaultConnection();
			if (!Database.tableExists(r, dc, tableName, true))
			{
				List<String> outputColumns = new ArrayList<String>();
				List<String> outputColumnTypes = new ArrayList<String>();
				outputColumns.add("TM"); // date
				outputColumnTypes.add(dc.getDateTimeType());
				outputColumns.add("OUTCOME"); // outcome name
				outputColumnTypes.add(dc.getVarcharType(128, false));
				outputColumns.add("ITERATION");
				outputColumnTypes.add(dc.getVarcharType(20, false));
				outputColumns.add("ELAPSED"); // time to build/eval the model
				outputColumnTypes.add("INTEGER");
				outputColumns.add("MODEL"); // Weka class name, i.e., weka.classifiers.tree.J48
				outputColumnTypes.add(dc.getVarcharType(128, false));
				outputColumns.add("TYPE"); // classification or regression
				outputColumnTypes.add(dc.getVarcharType(16, false));
				outputColumns.add("SUMMARY");
				outputColumnTypes.add(dc.getVarcharType(256, false));
				outputColumns.add("ERRORRATE");
				outputColumnTypes.add(dc.getFloatType());
				outputColumns.add("FALSEPOSITIVERATE");
				outputColumnTypes.add(dc.getFloatType());
				outputColumns.add("FALSENEGATIVERATE");
				outputColumnTypes.add(dc.getFloatType());
				outputColumns.add("RAE");
				outputColumnTypes.add(dc.getFloatType());
				outputColumns.add("RRSE");
				outputColumnTypes.add(dc.getFloatType());
				outputColumns.add("RMSE");
				outputColumnTypes.add(dc.getFloatType());
				outputColumns.add("MAE");
				outputColumnTypes.add(dc.getFloatType());
				outputColumns.add("CORRELATION");
				outputColumnTypes.add(dc.getFloatType());
				outputColumns.add("ATTRIBUTES"); // list of attributes [attr1, attr2, ...]
				outputColumnTypes.add(dc.getLargeTextType());
				outputColumns.add("EXPLANATION"); // .toString() on the classifier
				outputColumnTypes.add(dc.getLargeTextType());
				outputColumns.add("GRAPH"); // .graph() on the classifer if Drawable
				outputColumnTypes.add(dc.getLargeTextType());
				Database.createTable(r, dc, tableName, outputColumns, outputColumnTypes, false, false, null, null, null);
				Database.createIndex(dc, "ITERATION", tableName);
			}
		}
		catch (SQLException e)
		{
			logger.error(e.getMessage(), e);
		}
	}

	public void logResults(Repository r, BestFitResult bfr, List<String> attributes)
	{
		if (logger.isTraceEnabled())
			logger.trace("Logging modeling results for " + iteration + ", outcome " + bfr.getLabel());

		String getdate = dc.getGetDate();
		String insert = "INSERT INTO " + Database.getQualifiedTableName(dc, tableName) + " (TM, OUTCOME, ITERATION, ELAPSED, MODEL, TYPE, SUMMARY,"
		+ "ERRORRATE, FALSEPOSITIVERATE, FALSENEGATIVERATE, " + "RAE, RRSE, RMSE, MAE, CORRELATION, " + "ATTRIBUTES, EXPLANATION, GRAPH)"
		+ " VALUES(" + getdate
		+ ",?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		PreparedStatement pstmt = null;
		try
		{
			Connection conn = dc.ConnectionPool.getConnection();
			pstmt = conn.prepareStatement(insert);
			// basics
			pstmt.setString(1, bfr.getLabel());
			pstmt.setString(2, iteration);
			pstmt.setLong(3, bfr.getTime());
			pstmt.setString(4, bfr.getClassifier().getClass().getName());
			pstmt.setString(5, bfr.isClassModel() ? "classification" : "value");
			pstmt.setString(6, bfr.getErrorString());
			// classification errors
			if (bfr.isClassModel())
			{
				pstmt.setDouble(7, bfr.getErrorRate());
				pstmt.setDouble(8, bfr.getFalsePositiveRate());
				pstmt.setDouble(9, bfr.getFalseNegativeRate());
				pstmt.setNull(10, Types.DOUBLE);
				pstmt.setNull(11, Types.DOUBLE);
				pstmt.setNull(12, Types.DOUBLE);
				pstmt.setNull(13, Types.DOUBLE);
				pstmt.setNull(14, Types.DOUBLE);
			} else
			{
				pstmt.setNull(7, Types.DOUBLE);
				pstmt.setNull(8, Types.DOUBLE);
				pstmt.setNull(9, Types.DOUBLE);
				pstmt.setDouble(10, bfr.getRelativeAbsoluteError());
				pstmt.setDouble(11, bfr.getRootRelativeSquaredError());
				pstmt.setDouble(12, bfr.getRootMeanSquaredError());
				pstmt.setDouble(13, bfr.getMeanAbsoluteError());
				pstmt.setDouble(14, bfr.getCorrelationCoefficient());
			}
			// attributes
			String attrs = attributes.toString();
			pstmt.setCharacterStream(15, new StringReader(attrs), attrs.length());
			// explanation
			Classifier cCls = bfr.getClassifier();
			String msg = cCls.toString();
			pstmt.setCharacterStream(16, new StringReader(msg), msg.length());
			// graph
			if (cCls instanceof Drawable)
			{
				Drawable dCls = (Drawable) cCls;
				try
				{
					String graph = dCls.graph();
					pstmt.setCharacterStream(17, new StringReader(graph), graph.length());
					// try to save it to a file for ease of analysis
					try
					{
						String name = r.createApplicationPath() + File.separator + Util.cleanFilename(bfr.getLabel()) + ".txt";
						FileWriter fw = new FileWriter(name);
						if (logger.isTraceEnabled())
							logger.trace("saving graph to " + name);
						fw.append(graph);
						fw.close();
					} catch (IOException ex)
					{
						logger.warn("Could not save the graph to a file: " + ex.getMessage(), ex);
					}
				} catch (Exception ex)
				{
					logger.warn(ex, ex);
					pstmt.setNull(17, Types.CLOB);
				}
			} else
			{
				pstmt.setNull(17, Types.CLOB);
			}
			pstmt.execute();

		} catch (SQLException e)
		{
			logger.error(e.getMessage(), e);
		}
		finally
		{
			if (pstmt != null)
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
		}
	}

	public String getIteration()
	{
		return iteration;
	}

	public void setIteration(String iteration)
	{
		this.iteration = iteration;
	}
}