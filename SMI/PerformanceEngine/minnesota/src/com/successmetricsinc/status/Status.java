/**
 * $Id: Status.java,v 1.24 2012-11-22 06:46:53 BIRST\mjani Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.status;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TimeZone;

import org.apache.log4j.Logger;

import com.successmetricsinc.ExecuteSteps.Step;
import com.successmetricsinc.ExecuteSteps.StepCommand;
import com.successmetricsinc.ExecuteSteps.StepCommandType;
import com.successmetricsinc.Repository;
import com.successmetricsinc.query.Database;
import com.successmetricsinc.query.Database.DatabaseType;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.util.DateUtil;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.warehouse.Sample;
import com.successmetricsinc.warehouse.SampleMap;
import com.successmetricsinc.warehouse.SampleSettings;
import com.successmetricsinc.warehouse.SourceFile;
import com.successmetricsinc.warehouse.StagingTable;

public class Status
{
	private static Logger logger = Logger.getLogger(Status.class);

	public enum StatusCode
	{
		None, Running, Complete, Failed, ScriptFailure
	};
	private Repository r;
	private String iteration;
	private String tableName;
	private DatabaseConnection dc;
	private boolean disableLogging;
	private Map<String, Long> startTimes = new HashMap<String, Long>();
	private Map<String, Date> sourceLoadTimesCache = null;

	public Status(Repository r, String iteration, DatabaseConnection dconn) throws SQLException
	{
		this.r = r;
		this.iteration = iteration;
		try
		{
			if(dconn != null)
			{
				dc = dconn;
			}
			else
			{
				dc = r.getDefaultConnection();
			}
			tableName = "TXN_COMMAND_HISTORY";
			if (!Database.tableExists(r, dc, tableName, true))
			{
				List<String> outputColumns = new ArrayList<String>();
				List<String> outputColumnTypes = new ArrayList<String>();
				outputColumns.add("TM");
				outputColumnTypes.add(dc.getDateTimeType());
				outputColumns.add("COMMAND_TYPE");
				outputColumnTypes.add(dc.getVarcharType(30, false));
				outputColumns.add("STEP");
				outputColumnTypes.add(dc.getVarcharType(30, false));
				outputColumns.add("SUBSTEP");
				outputColumnTypes.add(dc.getVarcharType(1024, false));
				outputColumns.add("PROCESSINGGROUP");
				outputColumnTypes.add(dc.getVarcharType(1024, false));
				outputColumns.add("ITERATION");
				outputColumnTypes.add(dc.getVarcharType(20, false));
				outputColumns.add("STATUS");
				outputColumnTypes.add("INTEGER");
				outputColumns.add("NUMROWS");
				outputColumnTypes.add(dc.getBigIntType());
				outputColumns.add("NUMERRORS");
				outputColumnTypes.add(dc.getBigIntType());
				outputColumns.add("NUMWARNINGS");
				outputColumnTypes.add(dc.getBigIntType());
				outputColumns.add("DURATION");
				outputColumnTypes.add(dc.getBigIntType());
				outputColumns.add("MESSAGE");
				outputColumnTypes.add(dc.getVarcharType(1000, false));
				if (dc.DBType == DatabaseType.Infobright || dc.DBType == DatabaseType.MYSQL)
					Database.createMysqlSchema(dc);
				Database.createTable(r, dc, tableName, outputColumns, outputColumnTypes, false, false, null, null, null);
				Database.createIndex(dc, "ITERATION", tableName);
			}
			else
			{
				Connection conn = null;
				try
				{
					conn = dc.ConnectionPool.getConnection();
					List<Object[]> collist = Database.getTableSchema(dc, conn, null, dc.Schema, tableName);
					boolean containsPG = false;
					for (int i = 0; i < collist.size(); i++)
					{
						String columnName = (String) collist.get(i)[0];
						if (columnName.equalsIgnoreCase("PROCESSINGGROUP"))
						{
							containsPG = true;
							break;
						}
					}
					if (!containsPG)
					{
						List<String> columnNames = new ArrayList<String>();
						columnNames.add("PROCESSINGGROUP");
						List<String> columnTypes = new ArrayList<String>();
						columnTypes.add(dc.getVarcharType(1024, false));
						List<Integer> actions = new ArrayList<Integer>();
						actions.add(Database.ALTER_ADD);
						Database.alterTable(r, dc, tableName, columnNames, columnTypes, null, actions);
						logger.debug("Successfully added PROCESSINGGROUP column in TXN_COMMAND_HISTORY table");
					}
				}
				catch (SQLException e)
				{
					logger.warn("Could not add PROCESSINGGROUP column in TXN_COMMAND_HISTORY table", e);
				}
			}
		} catch (SQLException e)
		{
			logger.debug(e.getMessage(), e);
			throw e;
		}
	}

	/**
	 * check status - used for the meta-commands only
	 * 
	 * @param step
	 *            step in the process (i.e., RunModels)
	 * @param substep
	 *            substep in the process (i.e., Dev - Equities)
	 * @return true for correct status, false for incorrect status
	 */
	public boolean checkStatus(StepCommand step, String substep, String processingGroup, boolean checkForProcessingGroup, StatusCode status)
	{
		try
		{
			Connection conn = dc.ConnectionPool.getConnection();
			PreparedStatement pstmt = null;
			ResultSet res = null;
			try
			{
				// get the data for the step/substep/iteration/status
				int paramCount = 4;
				pstmt = conn.prepareStatement("SELECT STATUS FROM " + Database.getQualifiedTableName(dc, tableName)
							+ " WHERE STEP=? AND ITERATION=? AND STATUS=?" 
							+ " AND " + (substep == null ? "SUBSTEP IS NULL" : "SUBSTEP = ?")
							+ (checkForProcessingGroup ? " AND " + (processingGroup == null ? "PROCESSINGGROUP IS NULL" : "PROCESSINGGROUP = ?") : ""));
				
				if(substep != null)
					pstmt.setString(paramCount++, substep);
				if (checkForProcessingGroup && processingGroup != null)
					pstmt.setString(paramCount++, processingGroup);
				pstmt.setString(1, step.toString());
				pstmt.setString(2, iteration);
				pstmt.setInt(3, mapStatusToInt(status));
				res = pstmt.executeQuery();
				if (res.next())
				{
					return true;
				}
				return false;
			}
			catch (SQLException e)
			{
				logger.warn("Could not retrieve the status information from the database", e);
			}
			finally
			{
				try
				{
					if (res != null)
						res.close();
					if (pstmt != null)
						pstmt.close();
				}
				catch (SQLException e)
				{
					logger.debug(e, e);
				}
			}
		} catch (SQLException e)
		{
			logger.warn("Could not retrieve the status information from the database", e);
		}
		return false;
	}

	/**
	 * Get the last loaded times for each staging table
	 * @return
	 */
	public Map<String, Date> getSourceLoadTimes()
	{
		if (sourceLoadTimesCache != null)
			return sourceLoadTimesCache;
		Map<String, Date> sourceLoadTimes = new HashMap<String, Date>();
		try
		{
			Connection conn = dc.ConnectionPool.getConnection();				
			PreparedStatement pstmt = null;
			ResultSet res = null;
			try
			{
				pstmt = conn.prepareStatement("SELECT SUBSTEP, MAX(TM) FROM " + Database.getQualifiedTableName(dc, tableName) + " WHERE STEP=?"
						+ " AND STATUS=? GROUP BY SUBSTEP");
				pstmt.setString(1, StepCommand.LoadStaging.toString());
				pstmt.setInt(2,  2);
				res = pstmt.executeQuery();
				TimeZone dbTimeZone = (dc.getDBTimeZone() != null) ? TimeZone.getTimeZone(dc.getDBTimeZone()) : TimeZone.getDefault();
				
				while (res.next())
				{
					String name = res.getString(1);		
					SimpleDateFormat formatter = new SimpleDateFormat(DateUtil.DEFAULT_DATETIME_FORMAT);
					String dateString = formatter.format(res.getTimestamp(2));					
					formatter.setTimeZone(dbTimeZone);
					Date date = formatter.parse(dateString);					
					sourceLoadTimes.put(name, date);					
				}
			} catch (SQLException e)
			{
				logger.warn("Could not retrieve staging table load time information from the database", e);
			} catch (ParseException e) {
				logger.warn("Could not retrieve proper staging table load time information from the database", e);
			} finally
			{
				try
				{
					if (res != null)
						res.close();
					if (pstmt != null)
						pstmt.close();
				} catch (SQLException e)
				{
					logger.debug(e, e);
				}
			}
		} catch (SQLException e)
		{
			logger.warn("Could not retrieve staging table load time information from the database", e);
		}
		sourceLoadTimesCache = sourceLoadTimes;
		return sourceLoadTimes;
	}
	
	private int mapStatusToInt(StatusCode status)
	{
		if (status == StatusCode.None)
			return 0;
		if (status == StatusCode.Running)
			return 1;
		if (status == StatusCode.Complete)
			return 2;
		if (status == StatusCode.Failed)
			return 3;
		if (status == StatusCode.ScriptFailure)
			return 4;
		return 0;
	}

	private StatusCode mapIntToStatus(int status)
	{
		if (status == 0)
			return StatusCode.None;
		if (status == 1)
			return StatusCode.Running;
		if (status == 2)
			return StatusCode.Complete;
		if (status == 3)
			return StatusCode.Failed;
		if (status == 4)
			return StatusCode.ScriptFailure;
		
		return null;
	}
	
	/*
	 * 	checks for step/substep completion ignoring processinggroup check
	 */
	public boolean isComplete(StepCommand step, String substep)
	{
		return checkStatus(step, substep, null, false, StatusCode.Complete);
	}
	
	/*
	 * 	checks for step/substep completion, optionally checks for specific processinggroup
	 */
	public boolean isComplete(StepCommand step, String substep, String processingGroup, boolean checkForProcessingGroup)
	{
		return isComplete(step, substep, processingGroup, checkForProcessingGroup, null);
	}
	
	public boolean isComplete(StepCommand step, String substep, String processingGroup, 
			boolean checkForProcessingGroup, String killFileToDetect)
	{
		if(Util.isTerminated(killFileToDetect))
		{	
			// add the failure entry before exiting
			Step loadWarehouseStep = new Step(StepCommand.LoadWarehouse, com.successmetricsinc.ExecuteSteps.StepCommandType.ETL, null);
			String loadGroup = substep != null ? substep.split(":")[0] : null; 
			logStatus(loadWarehouseStep, loadGroup, StatusCode.Failed);
			Util.deleteFile(r.getRepositoryRootPath() + File.separator + Util.PUBLISH_LOCK_FILE);
			Util.exitNow(killFileToDetect);
		}
		return checkStatus(step, substep, processingGroup, checkForProcessingGroup, StatusCode.Complete);
	}

	public void logStatus(Step step, String substep, StatusCode status)
	{
		logStatus(step, substep, null, status);
	}
	
	public void logStatus(Step step, String substep, String processingGroup, StatusCode status)
	{
		logStatus(step, substep, processingGroup, status, -1);
	}
	
	public void logStatus(Step step, String substep, String processingGroup, StatusCode status, long numRows)
	{
		long duration = -1;
		if (status == StatusCode.Running)
			startTimes.put(step.command.name() + "|" + substep, System.currentTimeMillis());
		else if ((status == StatusCode.Complete || status == StatusCode.Failed) && startTimes.containsKey(step.command.name() + "|" + substep))
			duration = System.currentTimeMillis() - startTimes.get(step.command.name() + "|" + substep);
		logStatus(step, substep, processingGroup, status, numRows, -1, -1, duration, null);
	}

	/**
	 * log status for the meta-commands only
	 * 
	 * @param step
	 * @param substep
	 * @param iteration
	 * @param status
	 * 
	 */
	public void logStatus(Step step, String substep, String processingGroup, StatusCode status, long numRows, long numErrors, long numWarnings, long duration, String message)
	{
		logger.info("Logging step " + step.command.toString() + (substep == null ? "" : " [" + substep + "]") 
				+ (processingGroup == null ? "" : " for processingGroup [" + processingGroup + "]") + " for iteration " + iteration + ", status "
				+ status.toString());
		if (disableLogging)
			return;
		try
		{
			Connection conn = dc.ConnectionPool.getConnection();
			PreparedStatement pstmt = null;
			try
			{
				pstmt = conn.prepareStatement("INSERT INTO " + Database.getQualifiedTableName(dc, tableName)
						+ " (TM, COMMAND_TYPE, STEP, SUBSTEP, PROCESSINGGROUP, ITERATION, STATUS, NUMROWS, NUMERRORS, NUMWARNINGS, DURATION, MESSAGE) VALUES ("
						+ dc.getCurrentTimestamp() + ",?,?,?,?,?,?,?,?,?,?,?)");

				// truncate the strings to make sure that they are no bigger than the fields
				String temp = step.commandType.toString();
				temp = temp.substring(0, Math.min(temp.length(), 30));
				pstmt.setString(1, temp);

				temp = step.command.toString();
				temp = temp.substring(0, Math.min(temp.length(), 30));
				pstmt.setString(2, temp);

				if (substep == null)
				{
					pstmt.setNull(3, Types.VARCHAR);
				} else
				{
					temp = substep;
					temp = temp.substring(0, Math.min(temp.length(), 1024)); // this used to be 255, existing tables could fail here
					pstmt.setString(3, temp);
				}
				
				if (processingGroup == null)
				{
					pstmt.setNull(4, Types.VARCHAR);
				} else
				{
					temp = processingGroup;
					temp = temp.substring(0, Math.min(temp.length(), 1024));
					pstmt.setString(4, temp);
				}

				if (iteration == null)
				{
					pstmt.setNull(5, Types.VARCHAR);
				}
				else
				{
					temp = iteration;
					temp = temp.substring(0, Math.min(temp.length(), 20));
					pstmt.setString(5, temp);
				}
				pstmt.setInt(6, mapStatusToInt(status));

				if (numRows < 0)
					pstmt.setNull(7, Types.BIGINT);
				else
					pstmt.setLong(7, numRows);
				if (numErrors < 0)
					pstmt.setNull(8, Types.BIGINT);
				else
					pstmt.setLong(8, numErrors);
				if (numWarnings < 0)
					pstmt.setNull(9, Types.BIGINT);
				else
					pstmt.setLong(9, numWarnings);
				if (duration < 0)
					pstmt.setNull(10, Types.BIGINT);
				else
					pstmt.setLong(10, duration);

				if (message == null)
				{
					pstmt.setNull(11, Types.VARCHAR);
				}
				else
				{
					temp = message;
					temp = temp.substring(0, Math.min(temp.length(), 1000));
					pstmt.setString(11, temp);
				}
				pstmt.execute();
			}
			catch (SQLException e)
			{
				logger.warn("Could not save status information in the database", e);
			}
			finally 
			{
				try
				{
					if (pstmt != null)
						pstmt.close();
				}
				catch (SQLException e)
				{
					logger.debug(e, e);
				}
			}
			if (status == StatusCode.Running)
			{
				logger.info("Starting " + step.command.toString() + (substep == null ? "" : " [" + substep + "]") + (processingGroup == null ? "" : " for processingGroup [" + processingGroup + "]"));
			}
			if (status == StatusCode.Complete)
			{
				logger.info("Finished " + step.command.toString() + (substep == null ? "" : " [" + substep + "]") + (processingGroup == null ? "" : " for processingGroup [" + processingGroup + "]"));
			}
		} catch (SQLException e)
		{
			logger.warn("Could not save status information in the database", e);
		}
	}

	/**
	 * Get max status for a given step i.e. if there are two entries one running and another one complete, return complete
	 * @param step
	 * @param processingGroup
	 * @param checkForProcessingGroup
	 * @return Status code that can be null, running, failed or complete only
	 */
	public Status.StatusCode getMaxStatus(StepCommand step, String substep, String processingGroup, boolean checkForProcessingGroup)
	{
		Status.StatusCode maxsc = null;

		try
		{
			Connection conn = dc.ConnectionPool.getConnection();
			PreparedStatement pstmt = null;
			ResultSet res = null;
			Integer status = null;
			Status.StatusCode sc = null;
			
			try
			{
				// get the data for the step/iteration
				int paramCount = 3;
				pstmt = conn.prepareStatement("SELECT STATUS FROM " + Database.getQualifiedTableName(dc, tableName)
							+ " WHERE STEP=? AND ITERATION=? " 
							+ " AND " + (substep == null ? "SUBSTEP IS NULL" : "SUBSTEP = ?")
							+ (checkForProcessingGroup ? " AND " + (processingGroup == null ? "PROCESSINGGROUP IS NULL" : "PROCESSINGGROUP = ?") : ""));
				
				if(substep != null)
					pstmt.setString(paramCount++, substep);
				if (checkForProcessingGroup && processingGroup != null)
					pstmt.setString(paramCount++, processingGroup);
				pstmt.setString(1, step.toString());
				pstmt.setString(2, iteration);
				res = pstmt.executeQuery();
				while(res.next())
				{
					status = Integer.valueOf(res.getInt(1));
					if(status != null)
					{
						sc = mapIntToStatus(status);
						if(sc == StatusCode.Running)
						{
							maxsc = sc;
						}
						else if(sc == StatusCode.Complete || sc == StatusCode.Failed)
						{
							maxsc = sc;
							break;
						}
					}
				}
				
			}
			catch (SQLException e)
			{
				logger.warn("Could not retrieve the status information from the database", e);
			}
			finally
			{
				try
				{
					if (res != null)
						res.close();
					if (pstmt != null)
						pstmt.close();
				}
				catch (SQLException e)
				{
					logger.debug(e, e);
				}
			}
		} catch (SQLException e)
		{
			logger.warn("Could not retrieve the status information from the database", e);
		}
		return maxsc;
	}

	public void flush()
	{
		try
		{
			if (DatabaseConnection.isDBTypeMemDB(dc.DBType))
			{
				Connection conn = dc.ConnectionPool.getConnection();
				Statement stmt = conn.createStatement();
				stmt.execute("FLUSH");
			}
		} catch (SQLException e)
		{
			logger.warn("Could not flush status information in the database", e);
		}
	}

	/**
	 * clear the modeling command history information for a specific month
	 * 
	 * @param iteration
	 */
	public void clearModelingHistory()
	{
		logger.info("Clearing modeling history for " + iteration);
		clearHistory(StepCommandType.Modeling.toString(), false);
	}

	/**
	 * clear the command history information for a specific month
	 * 
	 * @param iteration
	 */
	public void clearETLHistory(boolean onlyModifiedSources)
	{
		logger.info("Clearing ETL history for " + iteration);
		clearHistory(StepCommandType.ETL.toString(), onlyModifiedSources);
	}

	private Date getLoadTime(StagingTable st, Map<String, Date> sourceLoadTimes)
	{
		for (Entry<String, Date> loadTime : sourceLoadTimes.entrySet())
		{
			String name = loadTime.getKey();
			int index = name.indexOf(':');
			if (index < 0)
				continue;
			String sourceName = name.substring(index + 2);
			if (sourceName.equals(st.Name))
				return loadTime.getValue();
		}
		return null;
	}

	public boolean isUnmodified(StagingTable st, Map<String, Date> sourceLoadTimes, String path, StringBuilder logger)
	{
		if (st.DiscoveryTable && !st.TruncateOnLoad)
			return false;
		Date lastModifiedDate = st.LastModifiedDate;
		/*
		 * Also make sure that the data file doesn't have any new data (in case the repository hasn't been updated with
		 * the proper timestamp)
		 */
		if (st.SourceFile != null && st.SourceFile.length() > 0)
		{
			File f = new File(path + File.separator + "data" + File.separator + st.SourceFile);
			if (f.exists() && f.lastModified() > lastModifiedDate.getTime())
				lastModifiedDate = new Date(f.lastModified());
		}
		boolean unmodified = false;
		Date loadTime = getLoadTime(st, sourceLoadTimes);				
		if (lastModifiedDate != null && loadTime != null && lastModifiedDate.before(loadTime))
		{
			unmodified = true;
			if (st.isScript())
			{
				List<StagingTable> ancestors = st.getAncestors();
				if (ancestors == null)
					unmodified = false;
				else
					for (StagingTable ast : ancestors)
					{
						if (!isUnmodified(ast, sourceLoadTimes, path, logger))
						{
							unmodified = false;
							break;
						}
					}
			}
		} else
			unmodified = false;
			
		if (unmodified)
		{
			if (logger != null)
				logger.append((logger.length() > 0 ? "," : "") + "Unmodified-" + st.Name + "(modified:" + lastModifiedDate + ",loaded:" + loadTime + ")");
			return true;
		} else
		{
			if (logger != null)
				logger.append((logger.length() > 0 ? "," : "") + "Modified-" + st.Name + "(modified:" + lastModifiedDate + ",loaded:" + loadTime + ")");
			return false;
		}
	}
	
	public List<String> getUnmodifiedSources(boolean justTableNames)
	{
		List<String> unmodifiedList = new ArrayList<String>();
		if (r.getCurrentBuilderVersion() < 25)
			return unmodifiedList;
		Map<String, Date> sourceLoadTimes = getSourceLoadTimes();
		StringBuilder debugLog = new StringBuilder();
		String path = r.getServerParameters().getApplicationPath();		
		for (Entry<String, Date> loadTime : sourceLoadTimes.entrySet())
		{
			String name = loadTime.getKey();
			int index = name.indexOf(':');
			if (index < 0)
				continue;
			String sourceName = name.substring(index + 2);
			StagingTable st = r.findStagingTable(sourceName);
			if (st == null)
				continue;
			if (isUnmodified(st, sourceLoadTimes, path, debugLog))
			{
				if (justTableNames)
					unmodifiedList.add(sourceName);
				else
					unmodifiedList.add(name);
			}
				
		}
		if (debugLog.length() > 0)
			logger.debug("Sources: " + debugLog);
		return unmodifiedList;
	}
	
	/**
	 * returns list of stagingTables for which sampling is on
	 * @param path
	 * @return
	 */
	public List<String> getSampledStagingTable(String path)
	{
		List<String> sampledStagingTables = new ArrayList<String>();
		if (r.getCurrentBuilderVersion() < 25)	//not require before this builder version as before that all the stagingTables were being processed
			return sampledStagingTables;
		SampleSettings ss = Sample.getSettings(path);
		if (ss != null)
		{
			for (SampleMap sm : ss.map)
			{
				if(sm.StagingTableName != null)
				{
					sampledStagingTables.add(sm.StagingTableName);
				}
			}
		}
		
		return sampledStagingTables;
	}
		
	private void clearHistory(String commandType, boolean onlyModifiedSources)
	{
		try
		{
			Map<String, String> valueByColumn = new HashMap<String, String>();
			valueByColumn.put("ITERATION", iteration);
			valueByColumn.put("COMMAND_TYPE", commandType);
			if (onlyModifiedSources)
			{
				Map<String, List<String>> neqColumn = new HashMap<String, List<String>>();
				List<String> neqValues = new ArrayList<String>();
				neqValues.add(StepCommand.LoadStaging.toString());
				neqColumn.put("STEP", neqValues);
				// Clear all steps except loadstaging
				Database.deleteOldData(r.getDefaultConnection(), tableName, valueByColumn, neqColumn);
				// Now, clear all steps except the unmodifed staging tables
				neqColumn.clear();
				sourceLoadTimesCache = null;
				valueByColumn.put("STEP", StepCommand.LoadStaging.toString());
				neqColumn.put("SUBSTEP", getUnmodifiedSources(false));
				Database.deleteOldData(r.getDefaultConnection(), tableName, valueByColumn, neqColumn);
			} else
			{
				Database.deleteOldData(r.getDefaultConnection(), tableName, valueByColumn);
			}
		} catch (SQLException e)
		{
			logger.warn("Could not clear history in the database", e);
		}
	}

	/**
	 *  command history information for a specific month
	 * 
	 * @param iteration
	 */
	public void clearCompletionHistory()
	{
		clearCompletionHistory(false);
	}
	
	public void clearCompletionHistory(boolean keepLoadMarker)
	{
		logger.info("Clearing ETL completion history for " + iteration);
		clearCompletionHistory(StepCommandType.ETL.toString(), keepLoadMarker);
	}

	private void clearCompletionHistory(String commandType, boolean keepLoadMarker)
	{
		try
		{
			Map<String, String> valueByColumn = new HashMap<String, String>();
			valueByColumn.put("ITERATION", iteration);
			valueByColumn.put("COMMAND_TYPE", commandType);
			valueByColumn.put("NUMROWS", null);
			valueByColumn.put("STATUS", Integer.toString(mapStatusToInt(StatusCode.Running)));
			
			Map<String, List<String>> neqValueByColumn = null; 
			if(keepLoadMarker)
			{
				neqValueByColumn = new HashMap<String, List<String>>();
				List<String> neqValues = new ArrayList<String>();
				neqValues.add("LoadMarker");
				neqValueByColumn.put("STEP", neqValues);
			}			
			Database.deleteOldData(r.getDefaultConnection(), tableName, valueByColumn, neqValueByColumn);
			valueByColumn.put("STATUS", Integer.toString(mapStatusToInt(StatusCode.Complete)));			
			Database.deleteOldData(r.getDefaultConnection(), tableName, valueByColumn, neqValueByColumn);
		} catch (SQLException e)
		{
			logger.warn("Could not clear history in the database");
		}
	}

	public String getIteration()
	{
		return iteration;
	}

	public void setIteration(String iteration)
	{
		this.iteration = iteration;
	}

	public boolean isDisableLogging()
	{
		return disableLogging;
	}

	public void setDisableLogging(boolean disableLogging)
	{
		this.disableLogging = disableLogging;
	}
}