/**
 * $Id: ExecuteSteps.java,v 1.340 2012-11-22 09:51:21 BIRST\mpandit Exp $
 *
 * Copyright (C) 2007-2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import javax.crypto.SecretKey;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.input.SAXBuilder;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.BasicSessionCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.successmetricsinc.connectors.ConnectorOperations;
import com.successmetricsinc.engine.FindRules;
import com.successmetricsinc.engine.FindSegments;
import com.successmetricsinc.engine.GenerateTargetLists;
import com.successmetricsinc.engine.Outcome;
import com.successmetricsinc.engine.OutcomeAnalysis;
import com.successmetricsinc.engine.OutcomeGroup;
import com.successmetricsinc.engine.PatternSearchDefinition;
import com.successmetricsinc.engine.Peers;
import com.successmetricsinc.engine.Performance;
import com.successmetricsinc.engine.PerformanceModel;
import com.successmetricsinc.engine.RankFactors;
import com.successmetricsinc.engine.ReferencePopulation;
import com.successmetricsinc.engine.Segment;
import com.successmetricsinc.engine.SuccessModel;
import com.successmetricsinc.engine.SuccessModelInstance;
import com.successmetricsinc.engine.SuccessPatternAnalysis;
import com.successmetricsinc.metadatavalidation.MetadataValidator;
import com.successmetricsinc.metadatavalidation.RuleExecutionResult;
import com.successmetricsinc.metadatavalidation.util.SaveRepository;
import com.successmetricsinc.query.AbstractQueryString;
import com.successmetricsinc.query.Aggregate;
import com.successmetricsinc.query.CacheOperations;
import com.successmetricsinc.query.CacheProfile;
import com.successmetricsinc.query.DataUnavailableException;
import com.successmetricsinc.query.Database;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.JasperDataSourceProvider;
import com.successmetricsinc.query.MoveSpaceUtil;
import com.successmetricsinc.query.Navigation;
import com.successmetricsinc.query.NavigationException;
import com.successmetricsinc.query.Query;
import com.successmetricsinc.query.QueryAggregate;
import com.successmetricsinc.query.QueryResultSet;
import com.successmetricsinc.query.QueryString;
import com.successmetricsinc.query.ResultSetCache;
import com.successmetricsinc.query.RowMapCacheEntry;
import com.successmetricsinc.query.TempTableDrop;
import com.successmetricsinc.queueing.Message;
import com.successmetricsinc.queueing.consumer.QueueConsumer;
import com.successmetricsinc.queueing.consumer.QueueConsumerFactory;
import com.successmetricsinc.security.EncryptionService;
import com.successmetricsinc.sfdc.SFDCDataExtractor;
import com.successmetricsinc.status.ModelingHistory;
import com.successmetricsinc.status.Status;
import com.successmetricsinc.status.Status.StatusCode;
import com.successmetricsinc.testdata.TestDataFileCreator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.util.AWSUtil;
import com.successmetricsinc.util.CopyUtil;
import com.successmetricsinc.util.Emailer;
import com.successmetricsinc.util.RepositoryException;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.WekaUtil;
import com.successmetricsinc.warehouse.DeleteAllData;
import com.successmetricsinc.warehouse.DeleteData;
import com.successmetricsinc.warehouse.DeleteSpace;
import com.successmetricsinc.warehouse.ExportWarehouse;
import com.successmetricsinc.warehouse.GenerateSchema;
import com.successmetricsinc.warehouse.LoadGroup;
import com.successmetricsinc.warehouse.LoadInfoBright;
import com.successmetricsinc.warehouse.LoadTables;
import com.successmetricsinc.warehouse.LoadWarehouse;
import com.successmetricsinc.warehouse.Sample;
import com.successmetricsinc.warehouse.Script;
import com.successmetricsinc.warehouse.ScriptGroup;
import com.successmetricsinc.warehouse.SnapshotPolicy;
import com.successmetricsinc.warehouse.StagingTable;
import com.successmetricsinc.warehouse.TimeDefinition;
import com.successmetricsinc.warehouse.TxnCommandHistoryConsolidator;
import com.successmetricsinc.warehouse.partition.SourceFileSplitter;

/**
 * @author bpeters
 * 
 */
public class ExecuteSteps extends Thread
{
	private static final String VALID_REPOSITORY_NOT_SPECIFIED = "Valid Repository Not Specified";
	private static Logger logger = Logger.getLogger(ExecuteSteps.class);
	private static final String MG_HOSTNAME = "MongoHostname";
	private static final String MG_PORT = "MongoPort";
	private static final String MG_POOL_SZ = "MongoPoolSize";
	private static final String MG_USE = "EnableMongoForCacheKeys";
	private static final String FS_ROOTDIR = "FileCacheSharedRootDir";
	public static final String DO_NOT_END_JVM_ON_COMPLETION = "DoNotEndJvmOnCompletion";
	public static final String PADB_REJECT_LOG_DIR = "PADB_Reject_Log_Dir";
	private static final String GOOGLE_ANALYTICS_API_KEY = "GoogleAnalyticsAPIKey";
	
	public enum StepCommand
	{
		GetModelDataSet, WriteModelDataSet, SummarizeDataSet, FindModelDataSetRules, RankModelDataSetFactors, GenOutcomeModels, GetOutcomeModelStats,
		OutputImpacts, GeneratePeers, GetPerformanceModel, OptimizePerformance, GeneratePerformanceTable, AnalyzeOutcome, CreateOutcomeGroup,
		GenerateSegmentLists, FindSegments, ScoreSegments, SearchPatterns, GetSegmentModelStats, RunQuery, RunSeedQuery, Test, ExportReports, Repository,
		InstantiateView, DropView, InitCacheGenerator, GenerateCache, ViewCacheFile, RemoveCacheFile, Hash,
		RemoveCacheForQuery, RemoveCacheForLogicalTable, ClearCache, RemoveCacheForPhysicalTable, CreateQueryAggregate, DropQueryAggregate, PersistAggregate,
		PersistAggregates, UpdateAggregate, DropAggregate, DropAggregates, ExplainNavigation, GeneratePhysicalQuery, OptimizePerformanceDB,
		RemoveFiles, TestWrapper, RunModels, CreateOutcomeGroups, RunPerformance, RunPeers, ResetModelRun, SetRowLimit, ProfileCache,
		CompareProfiles, ScriptCache, UpdateQueryAggregate, GenerateSchema, GenerateTimeSchema, LoadStaging, LoadWarehouse, ResetETLRun, DeleteData,
		ExportQuery, SetVariable, InstantiateAggregate, ExecuteScriptGroup, AggregatePhysicalQuery, AggregateLogicalQuery, ContinueOnErrors, ExitOnErrors,
		Silent, ExplainNavigationAgg, GeneratePhysicalQueryAgg, AddPartition, Echo, ExecuteTransformation, ExplainOnFailure, ContinueETLRun,
		FillSessionVariables, CloseSession, ExportWarehouse, SetDefaultConnection, LoadInfoBright, EnforceSnapshotPolicy, Exec, Touch, ErrorContinuePoint, TranslateQuery,
		SaveDynamicRepository, Decrypt, Encrypt, LoadMarker, DeleteAllData, DeleteSpace, CreateSample, DeleteSamples, ShowSamples, ApplySample, RemoveSample,
		DeriveSample, ExtractSFDCData, ExtractConnectorData, CheckIn, EmailerClient, CreateTestDataFiles, PerformAnalyzeSchema, PerformVacuumSchema, SetProperties, 
		SplitSourceFilesForPartitions, SetPartitionConnection, ConsolidateTxnCmdHistory, CreateTimeFiles, CreateTimeSchema, LoadTimeSchema, UploadTimeFilesToS3,
		DropTempTables, UpdateStats, CopyTable, Unload, Load , CreateDiscoveryST , UpdateCompositeKeys, ValidateMetadata, SaveRepository, UnloadUsingQuery,
		CreateScriptLog, FixMetadata , GetCheckSum, ListIndices, ListAggregatesPhysical, ReInitLoadVariables, CopyCatalog, PerformAsyncTask
	};

	public enum StepCommandType
	{
		Modeling, ETL, Query, Cache, Aggregates, Report, Init, Password, Database, Async, Test
	};

	public static class Step
	{
		public StepCommand command;
		public StepCommandType commandType;
		public List<String> args;

		public Step(StepCommand command, StepCommandType commandType, String argument)
		{
			this.command = command;
			this.commandType = commandType;
			this.args = new ArrayList<String>();
			if (argument != null)
			{
				this.args.add(argument);
			}
		}

		public String toString()
		{
			StringBuilder buf = new StringBuilder();
			buf.append(command.toString());
			if (args != null)
			{
				for (String arg : args)
				{
					buf.append(' ');
					buf.append(QueryString.filterServerPassword(arg));
				}
			}
			return buf.toString();
		}
	}
	private List<Step> steps;
	private Repository r;
	private Connection conn;
	private String path;
	private String currentMonthID;
	private CommandSession cmdSession;
	private JasperDataSourceProvider jdsp;
	private ResultSetCache rsc = null;
	private boolean error = false;
	private Status status;
	private boolean silent = false;
	private ModelingHistory modelingHistory;
	private String loadGroupName = null;
	boolean persistSubQueries = false;
	boolean retainDQTs = false;
	Emailer emailer = null;
	List<PackageSpaceProperties> packageSpacePropertiesList;
	String partitionConnection = null;

	private synchronized void setJasperDataSourceProvider(JasperDataSourceProvider jdsp)
	{
		this.jdsp = jdsp;
	}

	public ExecuteSteps(List<Step> steps, CommandSession cmdSession)
	{
		this.setName("Execute - " + this.getId());
		this.steps = steps;
		this.cmdSession = cmdSession;
	}

	public void run()
	{
		boolean continueOnErrors = false;
		SuccessModelInstance smi = null;
		Performance p = null;
		CacheGenerator cg = null;
		List<String> cacheGenerationQueryList = null;
		List<String> cacheGenerationSeedQueryList = null;
		AbstractQueryString aqs = null;
		Query q = null;
		String query = null;
		String name = null;
		String pModelName = null;
		String aggName = null;
		Aggregate agg = null;
		String physicalQuery = null;
		PerformanceModel pMod;
		boolean writeCSV = false;
		boolean writeTable = false;
		boolean bulkLoad = false;
		boolean cacheGenerationMode = false;
		boolean persist;
		this.r = cmdSession.getRepository();
		if (cmdSession.getResultSetCache() != null)
		{
			this.rsc = cmdSession.getResultSetCache();
		}
		this.path = cmdSession.getPath();
		if (this.r != null)
		{
			try
			{
				this.conn = cmdSession.getConnection();
			} catch (Exception e)
			{
				logger.debug("Serious Error: getConnection exception");
				return;
			}
		}
		this.status = cmdSession.getStatus();
		this.modelingHistory = cmdSession.getModelingHistory();
		boolean printedExitMessage = false;
		for (Step step : steps)
		{
			if (step.command == StepCommand.ErrorContinuePoint)
				error = false;
			if (!continueOnErrors && error)
			{
				if(!printedExitMessage)
				{
					cmdSession.getWriter().println("Exiting due to error, see the log for details (hopefully you ran with -loglevel ALL)");
					printedExitMessage = true;
				}
				continue;
			}
			error = false;
			try
			{
				logger.debug("Starting: " + step.toString());
				long tStart = System.currentTimeMillis();
				switch (step.command)
				{
				case ContinueOnErrors:
					continueOnErrors = true;
					break;
				case ExitOnErrors:
					continueOnErrors = false; // the default
					break;
				case Echo:
					if (step.args == null || step.args.size() == 0)
						throw (new Exception("Echo string not specified"));
					// it's already been logged, so just echo it on the console
					System.out.println(step.args.get(0));
					break;
				case GetModelDataSet:
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					if (step.args == null || step.args.size() == 0)
						throw (new Exception("Model Not Specified"));
					int argpos = 1;
					boolean createaggregates = step.args.size() > argpos && step.args.get(argpos).equals("createaggregates");
					boolean remove = false;
					boolean persistsubqueries = false;
					persist = false;
					if (createaggregates)
					{
						argpos++;
						remove = step.args.size() > argpos && step.args.get(argpos).equals("remove");
						persist = step.args.size() > argpos && step.args.get(argpos).equals("persist");
						if (remove || persist)
						{
							argpos++;
							if (persist)
							{
								persistsubqueries = step.args.size() > argpos && step.args.get(argpos).equals("persistsubqueries");
								argpos++;
							}
						}
					}
					String filter = null;
					if (step.args.size() > argpos + 1 && step.args.get(argpos).equals("filter"))
					{
						filter = step.args.get(argpos + 1);
						argpos += 2;
					}
					boolean reset = true;
					if (createaggregates)
						reset = false;
					smi = new SuccessModelInstance(r, step.args.get(0), path, reset);
					if (filter != null)
						smi.setRuntimeFilter(filter);
					if (!remove)
					{
						smi.fill(createaggregates, persist, persistsubqueries);
					} else
					{
						smi.fill(true, false, false);
						r.dropQueryAggregates(smi.getSuccessModel().Name, true);
					}
					if (!createaggregates)
					{
						if (smi.isBuildPatternClusters())
							smi.buildClusters(r);
						smi.commit();
						logger.info("Writing SMI file");
						WekaUtil.saveSMI(smi);
						logger.info("Writing Model Data Set File");
						smi.saveMDS();
					}
					break;
				case WriteModelDataSet:
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					if (step.args == null || step.args.size() == 0)
						throw (new Exception("Model Not Specified"));
					smi = new SuccessModelInstance(r, step.args.get(0), path, false);
					smi.retrieveMDS();
					if (step.args.size() == 3)
						cmdSession.getWriter().print(smi.getMds().toString(Integer.parseInt(step.args.get(1)), Integer.parseInt(step.args.get(2))));
					else
						cmdSession.getWriter().print(smi.getMds().toString());
					break;
				case SummarizeDataSet:
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					if (step.args == null || step.args.size() == 0)
						throw (new Exception("Model Not Specified"));
					smi = new SuccessModelInstance(r, step.args.get(0), path, false);
					smi.retrieveMDS();
					smi.getMds().summarizeDataset(cmdSession.getWriter(), r.getModelParameters().MaxComputationThreads);
					cmdSession.getWriter().flush();
					break;
				case FindModelDataSetRules:
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					if (step.args == null || step.args.size() == 0)
						throw (new Exception("Model Not Specified"));
					smi = new SuccessModelInstance(r, step.args.get(0), path, false);
					smi.retrieveMDS();
					FindRules fr = new FindRules(smi);
					fr.getRules();
					fr.saveRules();
					fr.getSavedRules();
					fr.pruneRules(r);
					break;
				case RankModelDataSetFactors:
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					if (step.args == null || step.args.size() == 0)
						throw (new Exception("Model Not Specified"));
					smi = new SuccessModelInstance(r, step.args.get(0), path, false);
					smi.retrieveMDS();
					RankFactors rf = new RankFactors(smi);
					Double minCorr = null;
					Double minLift = null;
					Double minDev = null;
					while (step.args.size() > 1)
					{
						if (step.args.get(1).startsWith("mincorr="))
						{
							minCorr = Double.valueOf(step.args.get(1).substring(8));
						} else if (step.args.get(1).startsWith("minlift="))
						{
							minLift = Double.valueOf(step.args.get(1).substring(8));
						} else if (step.args.get(1).startsWith("mindev="))
						{
							minDev = Double.valueOf(step.args.get(1).substring(7));
						}
						step.args.remove(1);
					}
					rf.rank(0, minCorr, minLift, minDev);
					break;
				case SearchPatterns:
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					if (step.args == null || step.args.size() == 0)
						throw (new Exception("Pattern Search Definition Not Specified"));
					SuccessPatternAnalysis spa = new SuccessPatternAnalysis(null, r, r.getDefaultConnection(), step.args.get(0));
					spa.generateSuccessPatterns(r.getModelParameters().MaxComputationThreads);
					break;
				case GenOutcomeModels:
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					String it = step.args.size() <= 1 ? null : r.replaceVariables(null, step.args.get(1));
					OutcomeAnalysis eag = new OutcomeAnalysis(r, step.args.get(0), it, null, path, r.getModelParameters().BulkLoadDirectory, modelingHistory,
							false);
					if (!eag.generateModels(true))
					{
						logger.error("Generate Models failed");
					}
					break;
				case FindSegments:
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					FindSegments fs = new FindSegments(r, step.args.get(0));
					fs.generateSegments();
					break;
				case ScoreSegments:
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					fs = new FindSegments(r, step.args.get(0));
					fs.scoreSegments(true);
					break;
				case AnalyzeOutcome:
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					boolean useDataSetSegments = false;
					if (step.args != null)
					{
						writeCSV = step.args.contains("writecsv");
						writeTable = step.args.contains("writetable");
						bulkLoad = step.args.contains("bulkload");
						useDataSetSegments = step.args.contains("usedatasetsegments");
						if (useDataSetSegments)
							logger.debug("using data set segments, not the model segments");
						step.args.remove("writecsv");
						step.args.remove("writetable");
						step.args.remove("bulkload");
						step.args.remove("usedatasetsegments");
					}
					if (!writeCSV && !writeTable && !bulkLoad)
						writeTable = true; // backwards compatibility
					String itValue = step.args.size() <= 1 ? null : r.replaceVariables(null, step.args.get(1));
					OutcomeAnalysis ea = new OutcomeAnalysis(r, step.args.get(0), itValue, itValue, path, r.getModelParameters().BulkLoadDirectory,
							modelingHistory, useDataSetSegments);
					ea.scoreOutcomes(writeTable, writeCSV, bulkLoad);
					break;
				case OutputImpacts:
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					OutcomeAnalysis oea = new OutcomeAnalysis(r, step.args.get(0), null, null, path, null, null, false);
					if (oea.getSegmentMap() != null)
					{
						for (Iterator<Entry<Object, Segment>> segIt = oea.getSegmentMap().entrySet().iterator(); segIt.hasNext();)
						{
							Entry<Object, Segment> se = segIt.next();
							Segment seg = se.getValue();
							seg.setOutcomeAnalysis(oea);
							seg.generateImpactMatrix(step.args.contains("sample"));
							seg.outputImpacts(step.args.contains("summary"));
						}
					}
					break;
				case GetPerformanceModel: // getperformancemodel <name>
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					pModelName = PerformanceModel.DEFAULT_NAME;
					if (step.args != null && step.args.size() == 1)
					{
						pModelName = step.args.get(0);
					}
					p = new Performance(pModelName, r, path);
					p.generateBestFitResult();
					p.saveArff(path);
					break;
				case OptimizePerformance:
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					pModelName = step.args.get(0);
					pMod = r.findPerformanceModel(pModelName);
					p = new Performance(pModelName, r, path);
					if (pMod.Optimize)
					{
						if (step.args != null && step.args.size() == 2 && !step.args.get(1).equals("all"))
						{
							// from file
							p.setOptimizeList(WekaUtil.getIDList(step.args.get(1)));
						}
						p.optimize(null);
					}
					break;
				case OptimizePerformanceDB:
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					pModelName = step.args.get(0);
					pMod = r.findPerformanceModel(pModelName);
					p = new Performance(pModelName, r, path);
					if (pMod.Optimize)
					{
						if (step.args != null && step.args.size() == 2)
						{
							// from DB
							Variable targetVar = r.findVariable(step.args.get(1));
							if (targetVar == null)
							{
								throw (new Exception("Non-existent variable: " + step.args.get(1)));
							}
							@SuppressWarnings("unchecked")
							List<Object> keys = (List<Object>) r.fillVariable(null, targetVar, null, null);
							p.setOptimizeList(WekaUtil.getListofStrings(keys));
						}
						p.optimize(null);
					}
					break;
				case RemoveFiles:
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					WekaUtil.removeModelingFiles(path);
					break;
				case GeneratePerformanceTable:
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					pModelName = step.args.get(0);
					p = new Performance(pModelName, r, path);
					if (step.args != null && step.args.size() == 2 && !step.args.get(1).equals("all"))
					{
						// from file
						p.setOptimizeList(WekaUtil.getIDList(step.args.get(1)));
						p.setAnalyzeOnlyOptimized(true);
					}
					p.generateImpactTable();
					break;
				case CreateOutcomeGroup:
					if (r == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					OutcomeGroup og = new OutcomeGroup(r, conn, step.args.get(0));
					og.generateOutcomeGroupTable();
					break;
				case GetOutcomeModelStats:
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					String oname = step.args.get(0);
					Outcome o = r.findOutcome(oname);
					if (o == null)
						throw (new Exception("Could not find Outcome"));
					OutcomeAnalysis oa = new OutcomeAnalysis(r, o.Name, o.IterationModelValue, o.IterationAnalysisValue, path,
							r.getModelParameters().BulkLoadDirectory, null, false);
					oa.outputSegmentModels(cmdSession.getWriter());
					break;
				case RunQuery:
					if (step.args == null || step.args.size() == 0)
						throw (new Exception("No Query Specified"));
					if (cacheGenerationMode)
					{
						// Dont execute any queries - just accumulate in a list now.
						if (cacheGenerationQueryList == null)
						{
							cacheGenerationQueryList = new ArrayList<String>();
						}
						cacheGenerationQueryList.add(step.args.get(0));
					} else
					{
						initializeJdsp(r);
						query = step.args.get(0);
						String splitCacheOn = null;
						String splitCacheOnDimension = null;
						String splitCacheOnColumn = null;
						boolean formatResults = false;
						boolean printCacheUsage = false;
						boolean printutf8 = false;
						boolean simulateOuterJoinUsingPersistedDQTs = false;
						if (step.args.size() > 1)
						{
							for (String arg : step.args)
							{
								if (arg.startsWith("splitcacheon="))
								{
									splitCacheOn = arg.substring(arg.indexOf("=") + 1);
								} else if (arg.equals("formatresults=true"))
								{
									formatResults = true;
								}
								else if (arg.equals("printcacheusage=true"))
								{
									printCacheUsage = true;
								}
								else if (arg.equals("SimulateOuterJoinUsingPersistedDQTs=true"))
								{
									simulateOuterJoinUsingPersistedDQTs = true;
								}
								else if (arg.equals("printutf8=true"))
								{
									printutf8 = true;
								}
							}
						}
						if (splitCacheOn != null)
						{
							if (splitCacheOn.indexOf('.') == -1)
							{
								throw new Exception("Invalid splitcacheon column name - the format should be <Dimension-Name>.<Column-name>");
							}
							splitCacheOnDimension = splitCacheOn.substring(0, splitCacheOn.indexOf('.'));
							splitCacheOnColumn = splitCacheOn.substring(splitCacheOn.indexOf('.') + 1, splitCacheOn.length());
						}
						QueryResultSet qrs = (QueryResultSet) jdsp.create(query, splitCacheOnDimension, splitCacheOnColumn, simulateOuterJoinUsingPersistedDQTs);
						if (!silent)
						{
							PrintStream writer = (cmdSession.getResultsWriter() == null) ? cmdSession.getWriter() : cmdSession.getResultsWriter();
							if(printCacheUsage)
							{
								String whereFromStr = "Not known";
								switch(qrs.whereFrom())
								{
									case memory:
										whereFromStr = "Memory Cache";
										break;
									case exactfile:
										whereFromStr = "Exact File Cache";
										break;
									case inexactfile:
										whereFromStr = "Inexact File Cache";
										break;
									case database:
										whereFromStr = "Database";
										break;
								}
								writer.println();
								writer.println("Query Result Set Retrieved From: " + whereFromStr);
								writer.println();
							}
							qrs.print(writer, '\t', r, formatResults, null, printutf8);
						}
					}
					break;
				case ExplainNavigation:
					if (step.args == null || step.args.size() == 0)
						throw (new Exception("No Query Specified"));
					query = step.args.get(0);
					query = QueryString.preProcessQueryString(query, r, cmdSession.getRepSession());
					aqs = AbstractQueryString.getInstance(r, query);
					q = aqs.getQuery();
					cmdSession.getResultsWriter().println(Util.getNavigationExplanation(aqs, q, query, false));
					break;
				case ExplainOnFailure:
					Navigation.setExplainOnNavigationException(true);
					break;
				case GeneratePhysicalQuery:
					if (step.args == null || step.args.size() == 0)
						throw (new Exception("No Query Specified"));
					query = step.args.get(0);
					query = QueryString.preProcessQueryString(query, r, cmdSession.getRepSession());
					aqs = AbstractQueryString.getInstance(r, query);
					q = aqs.getQuery();	
					physicalQuery = Util.getPhysicalQuery(aqs, q, query);
					boolean abstractschema = step.args.contains("abstractschema");
					if(abstractschema){
						List<DatabaseConnection> repositoryConnectionsList = r.getConnections();
						for(DatabaseConnection dc : repositoryConnectionsList){
							physicalQuery = Util.replaceStr(physicalQuery, dc.Schema, dc.Name);
						}
					}
					cmdSession.getResultsWriter().println(physicalQuery);
					logger.debug("Physical Query:");
					logger.debug(physicalQuery);
					break;
				case TranslateQuery:
					if (step.args == null || step.args.size() == 0)
						throw (new Exception("No Query Specified"));
					query = step.args.get(0);
					query = QueryString.preProcessQueryString(query, r, cmdSession.getRepSession());
					aqs = AbstractQueryString.getInstance(r, query);
					q = aqs.getQuery();
					q.navigateQuery(false, false, cmdSession.getRepSession(), true); // need to do this to make type information available
					String newLogQuery = q.getNewLogicalQueryString(false, null);
					cmdSession.getResultsWriter().println(">> " + query);
					cmdSession.getResultsWriter().println("<< " + newLogQuery);
					cmdSession.getResultsWriter().println("--");
					logger.debug("New Logical Query:");
					logger.debug(newLogQuery);
					break;
					
				case ExplainNavigationAgg:
					if (step.args == null || step.args.size() == 0)
						throw (new Exception("No Aggregate Name Specified"));
					aggName = step.args.get(0);
					agg = r.findAggregate(aggName);
					if (agg == null)
					{
						throw new Exception("Aggregate \"" + aggName + "\" not found in the repository.");
					}
					if (agg.isDisabled())
					{
						logger.error("Aggregate " + agg.getName() + " is disabled. Cannot perform ExplainNavigationAgg.");
						break;
					}
					cmdSession.getWriter().println(agg.getQuery().explainNavigation(false));
					break;
				case GeneratePhysicalQueryAgg:
					if (step.args == null || step.args.size() == 0)
						throw (new Exception("No Query Specified"));
					aggName = step.args.get(0);
					agg = r.findAggregate(aggName);
					if (agg == null)
					{
						throw new Exception("Aggregate \"" + aggName + "\" not found in the repository.");
					}
					if (agg.isDisabled())
					{
						logger.error("Aggregate " + agg.getName() + " is disabled. Cannot perform GeneratePhysicalQueryAgg.");
						break;
					}
					physicalQuery = agg.getPhysicalQuery();
					cmdSession.getWriter().println(physicalQuery);
					logger.debug("Physical Query:");
					logger.debug(physicalQuery);
					break;
				case ExportQuery:
					if (step.args == null || step.args.size() < 2)
						throw (new Exception("Need to specify the query and file"));
					initializeJdsp(r);
					query = step.args.get(0);
					String fileName = step.args.get(1);
					char separator = '\t';
					if (step.args.size() == 3)
						separator = step.args.get(2).charAt(0);
					FileOutputStream fo = new FileOutputStream(fileName);
					int limit = r.getServerParameters().getMaxRecords();
					r.getServerParameters().setMaxRecords(0);
					QueryResultSet queryResultSet = (QueryResultSet) jdsp.create(query);
					queryResultSet.print(new PrintStream(fo,true,"UTF-8"), separator, r, false, null, false);
					r.getServerParameters().setMaxRecords(limit);
					fo.close();
					break;
				case CreateQueryAggregate:
					if (r == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					if (step.args == null || step.args.size() == 0)
						throw (new Exception("No Query Specified"));
					query = step.args.get(0);
					name = step.args.get(1);
					persist = false;
					if (step.args.size() > 2)
					{
						persist = true;
					}
					QueryAggregate qa = r.findQueryAggregate(name);
					if (qa != null)
					{
						cmdSession.getWriter().println("Aggregate already exists: " + name);
					} else
					{
						query = QueryString.preProcessQueryString(query, r, cmdSession.getRepSession());
						aqs = AbstractQueryString.getInstance(r, query);	
						q = aqs.getQuery();
						qa = new QueryAggregate(q, name, aqs.getExpressionList());
						boolean success = true;
						if (persist)
						{
							success = qa.persist(null, false, false, null);
							if (!success)
							{
								cmdSession.getWriter().println("Query Aggregate " + name + " Not Persisted - see log for details.");
							}
						}
						r.addQueryAggregate(qa);
						cmdSession.getWriter().println("Query Aggregate Created" + ((persist && success) ? " and Persisted" : "") + ": " + name);
					}
					break;
				case PersistAggregate:
					if (r == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					if (step.args == null || step.args.size() == 0)
						throw (new Exception("No Aggregate Specified"));
					name = step.args.get(0);
					persistSubQueries = step.args.contains("persistsubqueries");
					retainDQTs = step.args.contains("retaindqt");
					boolean forceLevelKeys = step.args.contains("forcelevelkeys");
					Aggregate a = r.findAggregate(name);
					if (a == null)
					{
						logger.error("Aggregate not found: " + name);
					} else
					{
						if (a.isDisabled())
						{
							logger.warn("Aggregate " + a.getName() + " is disabled. However, it will be still persisted." +
									" In order to navigate queries to this aggregate, it must be enabled.");
						}
						Session s = null;
						try
						{
							s = new Session();
							a.drop(r, false);
							a.setStatus(status);
							a.process(r, true, s, !persistSubQueries, retainDQTs, true, forceLevelKeys, true);
							if (a.isDisabled())
							{
								a.drop(r, false);
							}
						}
						finally
						{
							if (s != null)
								s.setCancelled(true);
						}
					}
					break;
				case PersistAggregates:
					if (r == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					String loadGroup = null;
					String subgroup = null;
					String datadir = null;
					String databasepath = null;
					for (int i = 0; i < step.args.size(); i++)
					{
						String arg = step.args.get(i);
						if (arg.toLowerCase().startsWith("loadgroup="))
							loadGroup = arg.substring(10);
						else if (arg.toLowerCase().startsWith("subgroup="))
							subgroup = arg.substring(9);
						else if (arg.toLowerCase().startsWith("datadir="))
							datadir = arg.substring(8);
						else if (arg.toLowerCase().startsWith("databasepath="))
							databasepath = arg.substring(13);
					}
					if (r.isLoadWarehouseExecuted())
					{
						r.reInitLoadVariables();
					}
					for (Aggregate ag : r.getAggregates())
					{
						List<String> lglist = new ArrayList<String>();
						if (ag.getLoadGroupNames() != null)
							for (String s : ag.getLoadGroupNames())
								lglist.add(s);
						List<String> sglist = new ArrayList<String>();
						List<String> rebuildList = new ArrayList<String>();
						if (ag.getSubGroups() != null)
							for (String s : ag.getSubGroups())
							{
								if (s.endsWith("/REBUILD"))
								{
									s = s.substring(0, s.length() - 8);
									rebuildList.add(s);
									logger.info("rebuilding:"+s);
								}
								sglist.add(s);
							}
						if ((loadGroup == null || lglist.indexOf(loadGroup) >= 0)) 
						{
							if (subgroup == null || sglist.indexOf(subgroup) >= 0)
							{
								if (ag.isDisabled())
								{
									logger.info("Aggregate " + ag.getName() + " is disabled. Cannot perform PersistAggregate.");
								} else
								{
									Session s = null;
									try
									{
										s = new Session();
										ag.drop(r, false);
										ag.setStatus(status);
										ag.setDataDir(datadir);
										ag.setDatabasePath(databasepath);
										ag.process(r, true, s, false, false, true, false, subgroup == null || sglist.size() == 0 || rebuildList.indexOf(subgroup) == -1);
									}
									finally
									{
										if (s!= null)
											s.setCancelled(true);
									}
								}
							}
						}
					}
					break;
				case AggregatePhysicalQuery:
					if (r == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					if (step.args == null || step.args.size() == 0)
						throw (new Exception("No Aggregate Specified"));
					name = step.args.get(0);
					Aggregate a2 = r.findAggregate(name);
					if (a2 == null)
					{
						logger.error("Aggregate not found: " + name);
					} else
					{
						if (a2.isDisabled())
						{
							logger.error("Aggregate " + a2.getName() + " is disabled. Cannot perform AggregatePhysicalQuery.");
							break;
						}
						Session s = null;
						try
						{
							s = new Session();
							a2.drop(r, false);
							a2.process(r, false, s);
						}
						finally
						{
							if (s != null)
								s.setCancelled(true);
						}
					}
					cmdSession.getWriter().println("Physical query for aggregate: " + name);
					cmdSession.getWriter().println(a2.getPhysicalQuery());
					cmdSession.getWriter().println();
					break;
				case AggregateLogicalQuery:
					if (r == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					if (step.args == null || step.args.size() == 0)
						throw (new Exception("No Aggregate Specified"));
					name = step.args.get(0);
					Aggregate a3 = r.findAggregate(name);
					if (a3 == null)
					{
						logger.error("Aggregate not found: " + name);
					}
					cmdSession.getWriter().println("Logical query for aggregate: " + name);
					cmdSession.getWriter().println();
					cmdSession.getWriter().println(a3.getLogicalQuery(r));
					cmdSession.getWriter().println();
					break;
				case InstantiateAggregate:
					if (r == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					if (step.args == null || step.args.size() == 0)
						throw (new Exception("No Aggregate Specified"));
					name = step.args.get(0);
					Aggregate a1 = r.findAggregate(name);
					if (a1 == null)
					{
						logger.error("Aggregate not found: " + name);
					} else
					{
						Session s = null;
						try
						{
							s = new Session();
							a1.process(r, false, s);
							// process the inherited information now that we have instantiated the aggregates
							r.processInheritedMeasureTables(true);
							r.processInheritedVirtualMeasures(true);
						}
						finally
						{
							if (s != null)
								s.setCancelled(true);
						}
					}
					break;
				case UpdateAggregate:
					if (r == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					if (step.args == null || step.args.size() == 0)
						throw (new Exception("No Aggregate Specified"));
					name = step.args.get(0);
					loadGroupName = getLoadGroupName(step.args);
					persistSubQueries = step.args.contains("persistsubqueries");
					a = r.findAggregate(name);
					if (a == null)
					{
						logger.error("Aggregate not found: " + name);
					} else
					{
						if (a.isDisabled())
						{
							logger.error("Aggregate " + a.getName() + " is disabled. Cannot perform UpdateAggregate.");
							break;
						}
						Session s = null;
						try
						{
							s = new Session();
							a.update(r, s, loadGroupName, !persistSubQueries);
						}
						finally
						{
							if (s != null)
								s.setCancelled(true);
						}
					}
					break;
				case DropAggregate:
					if (r == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					if (step.args == null || step.args.size() == 0)
						throw (new Exception("No Aggregate Specified"));
					name = step.args.get(0);
					persist = false;
					if (step.args.size() > 1)
					{
						persist = true;
					}
					a = r.findAggregate(name);
					if (a == null)
					{
						logger.error("Aggregate not found: " + name);
					} else
					{
						if (a.isDisabled())
						{
							logger.error("Aggregate " + a.getName() + " is disabled. Cannot perform DropAggregate.");
							break;
						}
						a.drop(r, persist);
						logger.info("Aggregate Dropped: " + name);
					}
					break;
				case DropAggregates:
					if (r == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					persist = false;
					if (step.args != null && step.args.size() == 1)
					{
						persist = true;
					}
					for (Aggregate ag : r.getAggregates())
					{
						if (ag.isDisabled())
						{
							logger.error("Aggregate " + ag.getName() + " is disabled. Cannot perform DropAggregate.");
							continue;
						}
						try
						{
							ag.drop(r, persist);
							if (persist)
								logger.info("Aggregate metadata and table dropped: " + ag.getName());
							else
								logger.info("Aggregate metadata dropped: " + ag.getName());
						} catch (SQLException sex)
						{
							logger.error("Aggregate table drop failed: " + ag.getName());
							logger.error(sex.getMessage());
						}
					}
					break;
				case DropQueryAggregate:
					if (r == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					name = step.args.get(0);
					persist = false;
					if (step.args.size() > 1)
					{
						persist = true;
					}
					qa = r.findQueryAggregate(name);
					if (qa == null)
					{
						logger.error("Query aggregate not found: " + name);
					} else
					{
						qa.removeAggregate(persist);
						r.removeQueryAggregate(qa);
						logger.info("Query Aggregate Dropped: " + name);
					}
					break;
				case UpdateQueryAggregate:
					if (r == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					name = step.args.get(0);
					qa = r.findQueryAggregate(name);
					if (qa == null)
					{
						logger.error("Query aggregate not found: " + name);
					} else
					{
						if (qa.updateAggregate(null, true, null, true))
						{
							logger.info("Query Aggregate Updated: " + name);
						} else
						{
							logger.error("Query Aggregate Not Updated: " + name);
						}
					}
					break;
				case RunSeedQuery:
					if (step.args == null || step.args.size() == 0)
						throw (new Exception("No Query Specified"));
					if (cacheGenerationMode)
					{
						// Dont execute any queries - just accumulate in a list now.
						if (cacheGenerationSeedQueryList == null)
						{
							cacheGenerationSeedQueryList = new ArrayList<String>();
						}
						cacheGenerationSeedQueryList.add(step.args.get(0));
					} else
					{
						error = true;
						throw new Exception("Invalid command - runseedquery can only be used with generatecache command.");
					}
					break;
				case GenerateSegmentLists:
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					if (step.args != null)
					{
						writeCSV = step.args.contains("writecsv");
						writeTable = step.args.contains("writetable");
						step.args.remove("writecsv");
						step.args.remove("writetable");
					}
					if (!writeCSV && !writeTable && !bulkLoad)
						writeTable = true; // backwards compatibility
					GenerateTargetLists gtl = new GenerateTargetLists(r, r.getDefaultConnection(), step.args.get(0));
					gtl.generateTargetLists(writeTable, writeCSV, path);
					break;
				case InitCacheGenerator:
					if (r == null)
					{
						error = true;
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					}
					List<String> keyParameterNameList = Util.getKeyParameterNames(step.args.get(3));
					List<List<String>> keyParameterValueList = Util.getKeyParameterValues(keyParameterNameList.size(), step.args.get(4));
					cg = new CacheGenerator(step.args.get(0), step.args.get(1), step.args.get(2), keyParameterNameList, keyParameterValueList, r);
					cacheGenerationMode = true;
					break;
				case GenerateCache:
					if (r == null)
					{
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					}
					String token = step.args.get(0);
					if (token == null || (!token.trim().equalsIgnoreCase("end")))
					{
						error = true;
						throw (new Exception("Invalid syntax encountered for generatecache command:<" + token + ">."));
					}
					if (cacheGenerationQueryList == null)
					{
						error = true;
						throw (new Exception("Valid Queries For Cache Generation Not Specified"));
					}
					cg.generateCache(3, cacheGenerationSeedQueryList, cacheGenerationQueryList);
					break;
				case ViewCacheFile:
					String cacheFilename = step.args.get(0);
					try
					{
						FileInputStream in = new FileInputStream(cacheFilename);
						ObjectInputStream s = new ObjectInputStream(in);
						Object object = s.readObject();
						if (object instanceof QueryResultSet)
						{
							logger.info("File " + cacheFilename + " contains result set.");
							QueryResultSet qrs = (QueryResultSet) object;
							if (qrs != null)
							{
								cmdSession.getWriter().println(qrs.toString());
							}
						} else if (object instanceof Map)
						{
							logger.info("File " + cacheFilename + " contains row cache map.");
							@SuppressWarnings("unchecked")
							Map<String, Map<String, Integer>> maps = (Map<String, Map<String, Integer>>) object;
							cmdSession.getWriter().println(RowMapCacheEntry.mapsToPrettyString(maps));
						} else
						{
							logger.error("File " + cacheFilename + " does not contain result set or row map cache.");
						}
						in.close();
					} catch (Exception e)
					{
						cmdSession.getWriter().println("Result Set: Unavailable (See log for error details)");
						logger.error("Error in processing file name " + cacheFilename);
						logger.error(e.toString(), e);
					}
					break;
				case RemoveCacheFile:
					cacheFilename = step.args.get(0);
					if (rsc == null)
					{
						rsc = ResultSetCache.getInstance(r.getApplicationIdentifier(), false);
					}
					rsc.removeCacheFileEntry(cacheFilename, true);
					break;
				case ClearCache:
				{
					if (rsc == null)
					{
						rsc = ResultSetCache.getInstance(r.getApplicationIdentifier(), false);
					}
					if(rsc != null)
					{
						rsc.resetCache();
					}
					List<String> retValues = new ArrayList<String>();
					try
					{
						r.instantiateRepositoryVariables(null, new JasperDataSourceProvider(r));
						retValues.add("Repository variables reinitialized");
					}
					catch(RepositoryException re)
					{
						logger.error(re, re);
						retValues.add("Error reinitializing repository variables: "+re.toString());
					}
					retValues.add("Cache Cleared");
					if ((retValues != null) && (retValues.size() > 0) && cmdSession.getWriter() != System.out)
					{
						for (String retStr : retValues)
						{
							cmdSession.getWriter().println(retStr);
						}
					}
					break;
				}
				case ProfileCache:
				{
					if (rsc == null)
					{
						rsc = ResultSetCache.getInstance(r.getApplicationIdentifier(), false);
					}
					CacheProfile cp = new CacheProfile(rsc);
					CacheProfile.writeProfiles(cp.generateProfile(), step.args.get(0));
					break;
				}
				case CompareProfiles:
				{
					if (rsc == null)
					{
						rsc = ResultSetCache.getInstance(r.getApplicationIdentifier(), false);
					}
					CacheProfile.compareProfiles(step.args.get(0), step.args.get(1), System.out);
					break;
				}
				case ScriptCache:
				{
					if (rsc == null)
					{
						rsc = ResultSetCache.getInstance(r.getApplicationIdentifier(), false);
					}
					CacheProfile cp = new CacheProfile(rsc);
					cp.scriptCache(step.args.get(0));
					break;
				}
				case RemoveCacheForQuery:
				{
					query = step.args.get(0);
					boolean exactMatchOnly = false;
					boolean reseed = false;
					if (step.args.size() > 1)
					{
						if ("true".equals(step.args.get(1)))
						{
							exactMatchOnly = true;
						}
					}
					if (step.args.size() > 2)
					{
						if ("true".equals(step.args.get(2)))
						{
							reseed = true;
						}
					}
					if (rsc == null)
					{
						rsc = ResultSetCache.getInstance(r.getApplicationIdentifier(), false);
					}
					CacheOperations co = new CacheOperations();
					Map<String, String> parameterMap = new HashMap<String, String>();
					parameterMap.put("cmd", "ClearCacheForQuery");
					parameterMap.put("cmdParameter", query);
					parameterMap.put("ExactMatchOnly", exactMatchOnly ? "true" : "false");
					parameterMap.put("Reseed", reseed ? "true" : "false");
					List<String> retValues = co.performCacheOperation(parameterMap, r, cmdSession.getRepSession(), rsc);
					if ((retValues != null) && (retValues.size() > 0))
					{
						for (String retStr : retValues)
						{
							cmdSession.getWriter().println(retStr);
						}
					}
					break;
				}
				case RemoveCacheForLogicalTable:
				{
					String logicalTableName = step.args.get(0);
					if (rsc == null)
					{
						rsc = ResultSetCache.getInstance(r.getApplicationIdentifier(), false);
					}
					CacheOperations co = new CacheOperations();
					Map<String, String> parameterMap = new HashMap<String, String>();
					parameterMap.put("cmd", "ClearCacheForLogicalTable");
					parameterMap.put("cmdParameter", logicalTableName);
					List<String> retValues = co.performCacheOperation(parameterMap, r, cmdSession.getRepSession(), rsc);
					if ((retValues != null) && (retValues.size() > 0))
					{
						for (String retStr : retValues)
						{
							cmdSession.getWriter().println(retStr);
						}
					}
					break;
				}
				case RemoveCacheForPhysicalTable:
				{
					String physicalTableName = step.args.get(0);
					if (rsc == null)
					{
						rsc = ResultSetCache.getInstance(r.getApplicationIdentifier(), false);
					}
					CacheOperations co = new CacheOperations();
					Map<String, String> parameterMap = new HashMap<String, String>();
					parameterMap.put("cmd", "ClearCacheForPhysicalTable");
					parameterMap.put("cmdParameter", physicalTableName);
					List<String> retValues = co.performCacheOperation(parameterMap, r, cmdSession.getRepSession(), rsc);
					if ((retValues != null) && (retValues.size() > 0))
					{
						for (String retStr : retValues)
						{
							cmdSession.getWriter().println(retStr);
						}
					}
					break;
				}
				case ExtractSFDCData:
					if (r == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					SFDCDataExtractor sfExtractor = new SFDCDataExtractor(r, step.args.get(0), step.args.get(1).equalsIgnoreCase("null") ? null : step.args.get(1), 
							Integer.parseInt(step.args.get(2)));
					String uid = step.args.get(3).equalsIgnoreCase("null") ? null : step.args.get(3);
					if(uid != null)
					{
						sfExtractor.setUid(uid);
					}
					if(emailer != null)
					{
						sfExtractor.setEmailer(emailer);
					}
					sfExtractor.extractData();
					break;
				case ExtractConnectorData:
					if (r == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					String connectorName = step.args.get(0);
					String BirstParameters = step.args.get(1).equalsIgnoreCase("null") ? null : step.args.get(1);
					String sandBoxURL = step.args.get(2).equalsIgnoreCase("null") ? null : step.args.get(2);
					int noExtractionThreads = Integer.parseInt(step.args.get(3));
					ConnectorOperations co = new ConnectorOperations(r, connectorName, BirstParameters, sandBoxURL);
					co.setUid(step.args.get(4));
					co.extractData(noExtractionThreads);
					break;
				case SetProperties:
					if(step.args != null && step.args.size() > 0)
					{
						List<String> stepArgs = step.args;
						String propertyType = stepArgs.get(0);
						if(propertyType.equalsIgnoreCase("packageSpaceProperties"))
						{
							// used for supplying the space info for various packages
							PackageSpaceProperties packageSpaceProperties = new PackageSpaceProperties();
							for(String propNameValue : stepArgs)
							{
								String nameValue[] = propNameValue.split("=");
								if(nameValue.length != 2)
								{
									logger.warn("Skipping invalid property format " + nameValue);
									continue;
								}
								
								String propName = nameValue[0];
								String propValue = nameValue[1];
								
								if(propName.equalsIgnoreCase(PackageSpaceProperties.SPACE_ID))
								{
									packageSpaceProperties.setSpaceID(propValue);
								} 
								else if(propName.equalsIgnoreCase(PackageSpaceProperties.SPACE_DIR))
								{
									packageSpaceProperties.setSpaceDirectory(propValue);
								} 
								else if(propName.equalsIgnoreCase(PackageSpaceProperties.SPACE_NAME))
								{
									packageSpaceProperties.setSpaceName(propValue);
								}
								else if(propName.equalsIgnoreCase(PackageSpaceProperties.PACKAGE_ID))
								{
									packageSpaceProperties.setPackageID(propValue);
								}
								else if(propName.equalsIgnoreCase(PackageSpaceProperties.PACKAGE_NAME))
								{
									packageSpaceProperties.setPackageName(propValue);
								}
							}
							if(!packageSpaceProperties.validateAllRequiredProperties())
							{
								throw new Exception("Not all required properties are provided for space properties");
							}

							if(this.packageSpacePropertiesList == null)
							{
								packageSpacePropertiesList = new ArrayList<PackageSpaceProperties>();
							}
							packageSpacePropertiesList.add(packageSpaceProperties);							
						}
						else
						{
							throw new Exception("Unrecongnized property type in the setProperties command");
						}
					}
					break;
				case EmailerClient:
					if(step.args != null && step.args.size() >=3)
					{
						String mailHost = step.args.get(0);
						String from = step.args.get(1);
						String to = step.args.get(2);
						emailer = new Emailer(mailHost, from, to);
					}
					break;
				case CheckIn:
					if(step.args != null && step.args.size() > 0)
					{
						Util.startCheckIn(step.args.get(0));
					}
					break;
				case Repository:
					if (cmdSession.isOffline())
					{
						Properties overrides = Util.getProperties(Util.CUSTOMER_PROPERTIES_FILENAME, true);
						if (overrides != null && overrides.size() > 0)
						{
							if (overrides.containsKey(MG_HOSTNAME))
								System.setProperty(MG_HOSTNAME, overrides.getProperty(MG_HOSTNAME));
							if (overrides.containsKey(MG_PORT))
								System.setProperty(MG_PORT, overrides.getProperty(MG_PORT));
							if (overrides.containsKey(MG_POOL_SZ))
								System.setProperty(MG_POOL_SZ, overrides.getProperty(MG_POOL_SZ));
							if (overrides.containsKey(MG_USE))
								System.setProperty(MG_USE, overrides.getProperty(MG_USE));
							if (overrides.containsKey(FS_ROOTDIR))
								System.setProperty(FS_ROOTDIR, overrides.getProperty(FS_ROOTDIR));
							if (overrides.containsKey(DO_NOT_END_JVM_ON_COMPLETION))
								System.setProperty(DO_NOT_END_JVM_ON_COMPLETION, overrides.getProperty(DO_NOT_END_JVM_ON_COMPLETION));
							if (overrides.containsKey(PADB_REJECT_LOG_DIR))
								System.setProperty(PADB_REJECT_LOG_DIR, overrides.getProperty(PADB_REJECT_LOG_DIR));
							if (overrides.containsKey(Repository.ALLOW_DYNAMIC_DATABASE_CONNECTIONS))
								System.setProperty(Repository.ALLOW_DYNAMIC_DATABASE_CONNECTIONS, overrides.getProperty(Repository.ALLOW_DYNAMIC_DATABASE_CONNECTIONS));
							if (overrides.containsKey(GOOGLE_ANALYTICS_API_KEY))
								Repository.googleAnalyticsAPIKey = overrides.getProperty(GOOGLE_ANALYTICS_API_KEY);
						}
						if(partitionConnection != null)
						{
							if(overrides == null)
							{
								overrides = new Properties();
							}
							overrides.put("DefaultConnection", partitionConnection);
						}
						SAXBuilder builder = new SAXBuilder();
						Document d = null;					
						File nf = new File(step.args.get(0));
						try
						{
							d = builder.build(nf);
						} catch (FileNotFoundException fnfe)
						{
							cmdSession.getWriter().println("Unable to find repository at location: " + step.args.get(0));
							error = true;
							break;
						}
						boolean noAggregates = false;
						boolean processmetadata = true;
						int repoarg = 1;
						if (step.args.size() > repoarg && step.args.get(repoarg).equalsIgnoreCase("noaggregates"))
						{
							noAggregates = true;
							repoarg++;
						}
						if (step.args.size() > repoarg && step.args.get(repoarg).equalsIgnoreCase("nometadata"))
						{
							processmetadata = false;
						}
						if (this.r == null)
						{
							r = new Repository(d, false, overrides, nf.lastModified(), noAggregates, packageSpacePropertiesList, partitionConnection,processmetadata);
						}
						else
						{
							logger.info("Skipping repository command. Repository at " + r.getServerParameters().getApplicationPath() + " already initialized.");
						}
						path = r.createApplicationPath();
						currentMonthID = r.replaceVariables(null, "V{CurrentMonthID}");
						if(currentMonthID.equals("V{CurrentMonthID}"))
						{
							// For issuing commands like "PersistAggregate" from Command window.
							// This makes sure that iteration column in txn_command_history is integer
							// so that any casting of iteration column to INT does not fail
							currentMonthID = "-1"; 
						}
						cmdSession.setRepository(r);
						cmdSession.setPath(path);
						cmdSession.setDocument(d);
						
						status = new Status(r, currentMonthID, null);
						modelingHistory = new ModelingHistory(r, currentMonthID);
						cmdSession.setStatus(status);
						cmdSession.setModelingHistory(modelingHistory);
						try
						{
							conn = cmdSession.getConnection();
							
						} catch (SQLException sqe)
						{
							cmdSession.getWriter().println("Unable to connect to data source");
							error = true;
							break;
						}
						// clearing out kill lock file
						Util.deleteFile(Util.getKillFileName(r.getRepositoryRootPath(), Util.KILL_PROCESS_FILE));
					}
					break;
				case SaveRepository:
					if (r==null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					SaveRepository saveRepository = new SaveRepository(r);
					saveRepository.save(step.args.get(0));
					break;
				case SaveDynamicRepository:
					Document d = cmdSession.getDocument();
					if (r == null || d == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					r.saveDynamicRep(d, step.args.get(0));
					break;
				case InstantiateView:
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					logger.info("Instantiating Table: " + step.args.get(0));
					if ((step.args.size() > 1) && step.args.get(1).equals("create"))
						r.instantiateOpaqueView(step.args.get(0), true);
					else
						r.instantiateOpaqueView(step.args.get(0), false);
					logger.debug("View Instantiated: " + step.args.get(0));
					break;
				case DropView:
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					r.dropOpaqueView(step.args.get(0));
					logger.info("View Dropped: " + step.args.get(0));
					break;
				case Hash:
					EncryptionService ps = EncryptionService.getInstance();
					cmdSession.getWriter().println("Hashed: " + ps.createDigest(ps.genSalt(), step.args.get(0)));
					break;
				case Encrypt:
					EncryptionService ps1 = EncryptionService.getInstance();
					cmdSession.getWriter().println("Encrypted value is: " + ps1.encrypt(step.args.get(0), step.args.get(1)));
					break;
				case Decrypt:
					EncryptionService ps2 = EncryptionService.getInstance();
					cmdSession.getWriter().println("Decrypted value is: " + ps2.decrypt(step.args.get(0), step.args.get(1)));
					break;					
				case SetRowLimit:
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					String lim = step.args.get(0);
					if (lim != null)
					{
						r.getServerParameters().setMaxRecords(Integer.parseInt(lim));
						if (r.getServerParameters().getMaxRecords() == -1)
						{
							r.getServerParameters().setMaxRecords(Integer.MAX_VALUE);
						}
					}
					cmdSession.setMaxRecords(r.getServerParameters().getMaxRecords());
					logger.info("Row limit now set to: " + r.getServerParameters().getMaxRecords());
					break;
				case SetVariable:
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					String var = step.args.get(0);
					String val = step.args.get(1);
					r.setRepositoryVariable(var, val);
					break;
				case Silent:
					if (step.args.size() == 2)
					{
						if ("true".equals(step.args.get(1)))
							silent = true;
						else
							silent = false;
					} else
					{
						silent = !silent;
					}
					break;
				// META COMMANDS
				case ResetModelRun:
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					// remove the history associated with this run
					status.clearModelingHistory();
					// remove the files
					WekaUtil.removeModelingFiles(path);
					// remove the modeling data that needs to be removed
					// - peers (remove the old data)
					r.removeReferencePopulationData();
					// - performance
					// - success models
					// remove the aggregates
					// - remove the modeling opaque views
					r.removeModelingOpaqueViews();
					// - remove the modeling aggregates
					r.removeModelingAggregates();
					break;
				case ResetETLRun:
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					// remove the history associated with this run
					status.setIteration(Integer.valueOf(r.replaceVariables(null, step.args.get(0))).toString());
					status.clearETLHistory(step.args.contains("onlymodifiedsources"));
					break;
				case ContinueETLRun:
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					// remove the history associated with this run
					status.setIteration(Integer.valueOf(r.replaceVariables(null, step.args.get(0))).toString());
					status.clearCompletionHistory(true);
					break;
				case GeneratePeers:
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					if (step.args != null)
					{
						writeCSV = step.args.contains("writecsv");
						writeTable = step.args.contains("writetable");
						step.args.remove("writecsv");
						step.args.remove("writetable");
					}
					if (!writeCSV && !writeTable)
						writeTable = true; // backwards compatibility
					String refName = null;
					if (step.args != null && step.args.size() > 0)
					{
						refName = step.args.get(0);
					}
					Peers peers = new Peers(r, refName);
					peers.generatePeers(true, writeTable, writeCSV, path);
					peers.generatePeers(false, writeTable, writeCSV, path);
					break;
				case RunPeers:
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					if (step.args != null)
					{
						writeCSV = step.args.contains("writecsv");
						writeTable = step.args.contains("writetable");
						step.args.remove("writecsv");
						step.args.remove("writetable");
					}
					if (!writeCSV && !writeTable)
						writeTable = true; // backwards compatibility
					if (status.isComplete(step.command, null))
					{
						logger.info(step.toString() + " already successfully run");
						break;
					}
					status.logStatus(step, null, StatusCode.Running);
					for (ReferencePopulation refpop : r.getReferencePopulations())
					{
						if (status.isComplete(step.command, refpop.getName()))
							continue;
						status.logStatus(step, refpop.getName(), StatusCode.Running);
						Peers peers2 = new Peers(r, refpop.getName());
						peers2.generatePeers(true, writeTable, writeCSV, path);
						peers2.generatePeers(false, writeTable, writeCSV, path);
						status.logStatus(step, refpop.getName(), StatusCode.Complete);
					}
					status.logStatus(step, null, StatusCode.Complete);
					break;
				case RunPerformance:
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					if (status.isComplete(step.command, null))
					{
						logger.info(step.toString() + " already successfully run");
						break;
					}
					status.logStatus(step, null, StatusCode.Running);
					for (PerformanceModel pmodel : r.getPerformanceModels())
					{
						if (status.isComplete(step.command, pmodel.getName()))
							continue;
						status.logStatus(step, pmodel.getName(), StatusCode.Running);
						Variable targetVar = null;
						if (pmodel.Optimize)
						{
							if (step.args != null && step.args.size() == 1)
							{
								// from DB
								targetVar = r.findVariable(step.args.get(0));
								if (targetVar == null)
								{
									throw (new Exception("Non-existent variable: " + step.args.get(0)));
								}
							}
						}
						p = new Performance(pmodel.getName(), r, path);
						p.generateBestFitResult();
						if (pmodel.Optimize)
						{
							if (step.args != null && step.args.size() == 1)
							{
								@SuppressWarnings("unchecked")
								List<Object> keys = (List<Object>) r.fillVariable(null, targetVar, null, null);
								p.setOptimizeList(WekaUtil.getListofStrings(keys));
							}
							p.optimize(null);
						}
						p.generateImpactTable();
						status.logStatus(step, pmodel.getName(), StatusCode.Complete);
					}
					status.logStatus(step, null, StatusCode.Complete);
					break;
				case RunModels:
					// generate the model data set, generate the outcome, analyze the outcome for each model
					boolean runmodelsfailed = false;
					// need to add support for iteration
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					if (step.args != null)
					{
						writeCSV = step.args.contains("writecsv");
						writeTable = step.args.contains("writetable");
						bulkLoad = step.args.contains("bulkLoad");
						step.args.remove("writecsv");
						step.args.remove("writetable");
						step.args.remove("bulkLoad");
					}
					if (!writeCSV && !writeTable && !bulkLoad)
						writeTable = true; // backwards compatibility
					if (status.isComplete(step.command, null))
					{
						logger.info(step.toString() + " already successfully run");
						break;
					}
					status.logStatus(step, null, StatusCode.Running);
					// loop over models
					// validate the necessary opaque views are in place
					r.validateModelingOpaqueViews(Repository.MODEL_SUCCESS_MODELS);
					for (SuccessModel sm : r.getSuccessModels())
					{
						if (!sm.isRunnable())
							continue;
						if (status.isComplete(step.command, sm.Name))
							continue;
						status.logStatus(step, sm.Name, StatusCode.Running);
						boolean smfailed = false;
						String minorStep = sm.Name + " - generating model data set";
						if (!status.isComplete(step.command, minorStep))
						{
							status.logStatus(step, minorStep, StatusCode.Running);
							SuccessModelInstance smInstance = null;
							try
							{
								smInstance = new SuccessModelInstance(r, sm, path, true);
								smInstance.fill(false, false, false); // fill model data set, do not create aggregate
								// or
								// persist it
								if (smInstance.isBuildPatternClusters())
									smInstance.buildClusters(r);
								smInstance.commit();
								WekaUtil.saveSMI(smInstance); // save meta data
								smInstance.saveMDS(); // save model data set
								status.logStatus(step, minorStep, StatusCode.Complete);
							} catch (Exception ex)
							{
								runmodelsfailed = true;
								continue;
							}
						}
						// evaluate all outcomes for the model data set
						for (Outcome out : r.getOutcomes())
						{
							String smName = out.SuccessModelName;
							if (smName == null || !smName.equals(sm.Name)) // not an outcome associated with this model
								// data set
								continue;
							minorStep = "Outcome - " + out.Name;
							if (status.isComplete(step.command, minorStep))
								continue;
							status.logStatus(step, minorStep, StatusCode.Running);
							try
							{
								OutcomeAnalysis eag2 = new OutcomeAnalysis(r, out.Name, out.IterationModelValue, out.IterationAnalysisValue, path, r
										.getModelParameters().BulkLoadDirectory, null, false);
								// generate the model
								String reallyMinorStep = "Outcome - " + out.Name + " generating model";
								if (!status.isComplete(step.command, reallyMinorStep))
								{
									status.logStatus(step, reallyMinorStep, StatusCode.Running);
									if (!eag2.generateModels(true))
									{
										logger.error("Generate Models failed");
										status.logStatus(step, reallyMinorStep, StatusCode.Failed);
									} else
										status.logStatus(step, reallyMinorStep, StatusCode.Complete);
								}
								// score outcomes
								eag2.scoreOutcomes(writeTable, writeCSV, bulkLoad);
								status.logStatus(step, minorStep, StatusCode.Complete);
							} catch (Exception ex)
							{
								logger.error("Outcome: " + out.Name + " failed");
								smfailed = true;
								runmodelsfailed = true;
							}
						}
						// evaluate all success patterns for the model
						DatabaseConnection dc = r.getDefaultConnection();
						for (PatternSearchDefinition psd : r.getSearchDefinitions())
						{
							String smName = psd.SuccessModelName;
							if (smName == null || !smName.equals(sm.Name)) // not a pattern search associated with this
								// data set
								continue;
							minorStep = "Pattern Search - " + psd.Name;
							if (status.isComplete(step.command, minorStep))
								continue;
							status.logStatus(step, minorStep, StatusCode.Running);
							try
							{
								GenerateTargetLists gtl2 = new GenerateTargetLists(r, dc, psd.Name);
								gtl2.generateTargetLists(writeTable, writeCSV, path);
								status.logStatus(step, minorStep, StatusCode.Complete);
							} catch (Exception ex)
							{
								logger.error("Failure in pattern search: " + psd.Name);
								smfailed = true;
								runmodelsfailed = true;
							}
						}
						if (!smfailed)
							status.logStatus(step, sm.Name, StatusCode.Complete);
					}
					if (!runmodelsfailed)
						status.logStatus(step, null, StatusCode.Complete);
					break;
				case CreateOutcomeGroups:
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					if (status.isComplete(step.command, null))
					{
						logger.info(step.toString() + " already successfully run");
						break;
					}
					status.logStatus(step, null, StatusCode.Running);
					// loop over the outcomes, finding the groups
					Set<String> set = new HashSet<String>();
					for (Outcome out : r.getOutcomes())
					{
						if ((out.OutcomeGroupName != null) && (out.OutcomeGroupName.length() > 0) && !set.contains(out.OutcomeGroupName))
						{
							if (status.isComplete(step.command, out.OutcomeGroupName))
								continue;
							status.logStatus(step, out.OutcomeGroupName, StatusCode.Running);
							set.add(out.OutcomeGroupName);
							OutcomeGroup og2 = new OutcomeGroup(r, conn, out.OutcomeGroupName);
							og2.generateOutcomeGroupTable();
							status.logStatus(step, out.OutcomeGroupName, StatusCode.Complete);
						}
					}
					status.logStatus(step, null, StatusCode.Complete);
					break;
				// warehouse generation/loading commands
				case GenerateSchema:
				{	
					GenerateSchema gs = new GenerateSchema(r);
					status.setIteration(Integer.valueOf(r.replaceVariables(null, step.args.get(0))).toString());
					boolean generateTime = !step.args.contains("notime");
					boolean addMissingValues = !step.args.contains("skipmissingvalues");
					String executeOnChange = null;
					String databasePath = getDatabasePath(step.args);
					String awsAccessKeyId = getParameter(step.args, "awsAccessKeyId");
					String awsSecretKey = getParameter(step.args, "awsSecretKey");
					String awsToken = getParameter(step.args, "awsToken");
					for(String s: step.args)
					{
						if (s.startsWith("execonchange="))
						{
							executeOnChange = s.substring("execonchange=".length());
						}						
					}
					AWSCredentials s3credentials = null;
					if (awsAccessKeyId != null && awsSecretKey != null && awsToken != null)
						s3credentials = new BasicSessionCredentials(awsAccessKeyId, awsSecretKey, awsToken);
					else if (awsAccessKeyId != null && awsSecretKey != null && awsToken == null)
						s3credentials = new BasicAWSCredentials(awsAccessKeyId, awsSecretKey); 
					gs.generate(status, generateTime, addMissingValues, executeOnChange, databasePath, s3credentials);
					break;
				}
				case GenerateTimeSchema:
				{
					logger.warn("Note that GenerateTimeSchema is deprecated");
					GenerateSchema gs = new GenerateSchema(r);
					status.setIteration(Integer.valueOf(r.replaceVariables(null, step.args.get(0))).toString());
					gs.generateTimeDimension();
					break;
				}	
				case CreateTimeFiles:	// create time files, but not no load them (could zip these up and ship them for live access)
				{
					String folder = getParameter(step.args, "folder");
					if (folder == null)
						throw new Exception("Missing value for 'folder'");
					TimeDefinition td = r.getTimeDefinition();
					td.createTimeFiles(folder);
					break;
				}
				
				case CreateTimeSchema:	// create the time schema tables, but do not load
				{
					TimeDefinition td = r.getTimeDefinition();
					td.createTimeSchema(r);
					break;
				}				
					
				case UploadTimeFilesToS3: // upload the time files to S3; Redshift-specific command
				{
					String s3Folder = getParameter(step.args, "s3folder");
					if (s3Folder == null)
						throw new Exception("Missing value for 's3Folder'");
					String folder = getParameter(step.args, "folder");
					if (folder == null)
						throw new Exception("Missing value for 'folder'");
					String awsAccessKeyId = getParameter(step.args, "awsAccessKeyId");
					if (awsAccessKeyId == null)
						throw new Exception("Missing value for 'awsAccessKeyId'");
					String awsSecretKey = getParameter(step.args, "awsSecretKey");
					if (awsSecretKey == null)
						throw new Exception("Missing value for 'awsSecretKey'");
					String awsToken = getParameter(step.args, "awsToken");					
					AWSCredentials s3credentials = null;
					if (awsAccessKeyId != null && awsSecretKey != null && awsToken != null)
						s3credentials = new BasicSessionCredentials(awsAccessKeyId, awsSecretKey, awsToken);
					else if (awsAccessKeyId != null && awsSecretKey != null && awsToken == null)
						s3credentials = new BasicAWSCredentials(awsAccessKeyId, awsSecretKey); 
					TimeDefinition td = r.getTimeDefinition();
					td.uploadTimeFilesToS3(r, folder, s3Folder, s3credentials);
					break;
				}	
				
				case LoadTimeSchema:	// load the time files into the time schema, assumes that createtimefiles (and optionally uploadtimefilestos3) and createtimeschema have been done
				{
					String folder = getParameter(step.args, "folder");
					if (folder == null)
						throw new Exception("Missing value for 'folder'");
					String awsAccessKeyId = getParameter(step.args, "awsAccessKeyId");
					String awsSecretKey = getParameter(step.args, "awsSecretKey");
					String awsToken = getParameter(step.args, "awsToken");
					AWSCredentials s3credentials = Util.getAWSCredentions(awsAccessKeyId, awsSecretKey, awsToken);					
					TimeDefinition td = r.getTimeDefinition();
					td.loadTimeSchema(r, folder, s3credentials);
					break;
				}

				case ExecuteTransformation:
				{
					TransformationScript sc = new TransformationScript(r, null);
					status.setIteration(Integer.valueOf(r.replaceVariables(null, step.args.get(2))).toString());
					String databasePath = getDatabasePath(step.args);
					loadGroupName = getLoadGroupName(step.args);
					try
					{
						sc.executeScript(status, step.args.get(0), loadGroupName, r.replaceVariables(null, step.args.get(1)), r.replaceVariables(null,
								databasePath), 0, null, null, null);
					} catch (ScriptException ex)
					{
						logger.error(ex.getMessage());
					}
					break;
				}
				case LoadStaging:
				{	
					LoadTables lt = new LoadTables(r);
					status.setIteration(Integer.valueOf(r.replaceVariables(null, step.args.get(1))).toString());
					loadGroupName = getLoadGroupName(step.args);
					String databasePath = getDatabasePath(step.args);
					String awsAccessKeyId = getParameter(step.args, "awsAccessKeyId");
					String awsSecretKey = getParameter(step.args, "awsSecretKey");
					String awsToken = getParameter(step.args, "awsToken");
					AWSCredentials s3credentials = null;
					if (awsAccessKeyId != null && awsSecretKey != null && awsToken != null)
						s3credentials = new BasicSessionCredentials(awsAccessKeyId, awsSecretKey, awsToken);
					else if (awsAccessKeyId != null && awsSecretKey != null && awsToken == null)
						s3credentials = new BasicAWSCredentials(awsAccessKeyId, awsSecretKey); 
					int numRows = getNumRows(step.args);
					boolean skipTransformations = isSkipTransformations(step.args);
					String[] subGroups = getSubGroups(step.args);
					List<StagingTable> bulkLoaded = Collections.synchronizedList(new ArrayList<StagingTable>());
					
					List<SecretKey> keys = new ArrayList<SecretKey>();
					
					AmazonS3 s3client = AWSUtil.getS3Client(s3credentials, keys);							
					boolean encrypted = AWSUtil.isEncrypted(s3client);
					SecretKey symKey = null;
					if (encrypted)
						symKey = keys.get(0);
					boolean result = lt.loadStaging(r.replaceVariables(null, step.args.get(0)), r.replaceVariables(null, databasePath), status, step.args
							.contains("debug"), continueOnErrors, loadGroupName, subGroups, numRows, skipTransformations, bulkLoaded, s3client, s3credentials, symKey, r);
					if (!result)
						error = true;
					break;
				}
				case CreateSample:
				{
					Sample sa = new Sample(r, r.replaceVariables(null, step.args.get(0)), r.replaceVariables(null, step.args.get(1)), r.replaceVariables(null,
							step.args.get(2)));
					double percent = 100;
					try
					{
						percent = Double.valueOf(r.replaceVariables(null, step.args.get(4)));
					} catch (Exception ex)
					{
						percent = 100;
					}
					boolean result = sa.createSample(r.replaceVariables(null, step.args.get(3)), percent);
					if (!result)
						error = true;
					break;
				}
				case DeriveSample:
				{
					Sample sa = new Sample(r, r.replaceVariables(null, step.args.get(0)), r.replaceVariables(null, step.args.get(3)), r.replaceVariables(null,
							step.args.get(4)));
					boolean result = sa.deriveSample(r.replaceVariables(null, step.args.get(1)), r.replaceVariables(null,
							step.args.get(2)),r.replaceVariables(null, step.args.get(5)));
					if (!result)
						error = true;
					break;
				}
				case ApplySample:
				{
					Sample sa = new Sample(r, r.replaceVariables(null, step.args.get(0)), r.replaceVariables(null, step.args.get(1)), r.replaceVariables(null,
							step.args.get(2)));
					sa.applySample(r.replaceVariables(null, step.args.get(3)));
					break;
				}
				case RemoveSample:
				{
					Sample sa = new Sample(r, r.replaceVariables(null, step.args.get(0)), r.replaceVariables(null, step.args.get(1)), r.replaceVariables(null,
							step.args.get(2)));
					sa.removeSample(r.replaceVariables(null, step.args.get(3)));
					break;
				}
				case ShowSamples:
					Sample.showSamples(r.replaceVariables(null, step.args.get(0)), r);
					break;
				case DeleteSamples:
					Sample.deleteSamples(r.replaceVariables(null, step.args.get(0)));
					break;
				case AddPartition:
					Database.addPartition(r, Integer.valueOf(r.replaceVariables(null, step.args.get(0))), step.args.get(1));
					break;
				case LoadWarehouse:
				{
					LoadWarehouse lw = new LoadWarehouse(r);
					try
					{
						loadGroupName = getLoadGroupName(step.args);
						String[] subGroups = getSubGroups(step.args);
						Calendar scdStartDate = getSCDDate(step.args);
						String databasePath = getDatabasePath(step.args);
						if (scdStartDate != null)
						{
							lw.setScdStartDate(scdStartDate);
							Calendar scdEndDate = (Calendar) scdStartDate.clone();
							scdEndDate.add(Calendar.MILLISECOND, -2);
							lw.setScdEndDate(scdEndDate);
						}
						status.setIteration(Integer.valueOf(r.replaceVariables(null, step.args.get(0))).toString());
						boolean noFactIndices = step.args.contains("nofactindices");
						boolean keepFacts = step.args.contains("keepfacts");
						boolean onlymodifiedsources = step.args.contains("onlymodifiedsources");
						String awsAccessKeyId = getParameter(step.args, "awsAccessKeyId");
						String awsSecretKey = getParameter(step.args, "awsSecretKey");
						String awsToken = getParameter(step.args, "awsToken");
						AWSCredentials s3credentials = null;
						if (awsAccessKeyId != null && awsSecretKey != null && awsToken != null)
							s3credentials = new BasicSessionCredentials(awsAccessKeyId, awsSecretKey, awsToken);
						else if (awsAccessKeyId != null && awsSecretKey != null && awsToken == null)
							s3credentials = new BasicAWSCredentials(awsAccessKeyId, awsSecretKey); 
						lw.load(Integer.parseInt(r.replaceVariables(null, step.args.get(0))), status, loadGroupName, subGroups, noFactIndices, keepFacts, onlymodifiedsources, databasePath, s3credentials,r);
					} catch (NumberFormatException ex)
					{
						throw (new Exception("Cannot load warehouse: Load id must be an integer"));
					}
					break;
				}
				case CopyTable :
				{
					try{
						AWSCredentials s3credentials = getS3Credential(step);	//Require for RedShift					
						Database.copyTable(r, step.args.get(2), step.args.get(0), step.args.get(1), step.args.get(3), s3credentials);
					}catch(Exception ex){
						throw(new Exception("Could not Copy Table " +  step.args.get(2) + " : " + ex));
					}
					break;
				}
				case Unload :
				{
					try{
						boolean keepFileNameasTableName = Boolean.parseBoolean(step.args.get(3));
						String targetSchema = null;
						String databaseloaddirpath = null;
						if (step.args.size()>4)
						  targetSchema = getParameter(step.args, "targetschema");
						if (step.args.size()>4)
						  databaseloaddirpath = getParameter(step.args, "databaseloaddirpath");
						AWSCredentials s3credentials = getS3Credential(step); //Require for RedShift      
						Database.bulkImportExport(r,step.args.get(0), step.args.get(1), step.args.get(2),s3credentials,true,null,null,keepFileNameasTableName,targetSchema,databaseloaddirpath);
					}catch(Exception ex){
						throw(new Exception("Could not Unload Table " +  step.args.get(2) + " : " + ex));
					}
					break;
				}
				case UnloadUsingQuery :
				{
					try{
						Database.unloadUsingQuery(r,step.args.get(0), step.args.get(1), step.args.get(2),step.args.get(3),null, true,null,null,true);
					}catch(Exception ex){
						throw(new Exception("Could not Unload Table " +  step.args.get(2) + " : " + ex));
					}
					break;
				}
				case Load :
				{
				  try{
				    String databaseloaddirpath = null;
				    String sourceschema = null;
				    if (step.args.size()>3)
				      databaseloaddirpath = getParameter(step.args, "databaseloaddirpath");
				    if (step.args.size()>3)
				      sourceschema = getParameter(step.args, "sourceschema");
				    AWSCredentials s3credentials = getS3Credential(step); //Require for RedShift
				    Database.bulkImportExport(r,sourceschema, step.args.get(1) ,null ,s3credentials,false,step.args.get(0),step.args.get(2),false,null,databaseloaddirpath);
				  }catch(Exception ex){
				    throw(new Exception("Could not Copy Table " +  step.args.get(1) + " "+ ex.getMessage()));
				  }
				  break;
				}
				case CreateDiscoveryST :
				{
					try{
						Database.createDiscoveryStagingTable(r,step.args.get(0));
					}catch(Exception ex){
						throw(new Exception("CreateStagingTable Failed for" +  step.args.get(0)));
					}
					break;
				}
				case UpdateCompositeKeys :
				{
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					try{
						MoveSpaceUtil.updateCompositeKeys(r,step.args.get(0));
					}catch(Exception ex){
						logger.error(ex.toString() ,ex);
						//Using exit code to indicate unsucess operation to MoveSpace.exe
						System.exit(-1);
					}
					break;
				}
				case GetCheckSum :
				{
					try{
						Util.getCheckSum(step.args.get(0));
					}catch(Exception ex){
						logger.error(ex.toString() ,ex);
						//Using exit code to indicate unsucess operation to MoveSpace.exe
						System.exit(-1);
					}
					break;
				}
				case CreateScriptLog :
				{
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					try{
						Database.createScriptLog(r,step.args.get(0));
					}catch(Exception ex){
						logger.error(ex.toString() ,ex);
					}
					break;
				}
				case DeleteData:
					DeleteData dd = new DeleteData(r);
					try
					{
						String loadVal = r.replaceVariables(null, step.args.get(0));
						if (!loadVal.equalsIgnoreCase("all"))
						{
							status.setIteration(Integer.valueOf(loadVal).toString());
							dd.delete(Integer.parseInt(r.replaceVariables(null, step.args.get(0))), status);
						} else
							dd.delete(-1, status);
					} catch (NumberFormatException ex)
					{
						throw (new Exception("Cannot delete from warehouse: Load id must be an integer"));
					}
					break;
				case DeleteAllData:
					DeleteAllData deleteAll = new DeleteAllData(r);
					deleteAll.delete();
					break;
				case DeleteSpace:
					DeleteSpace delSpace = new DeleteSpace(r);
					delSpace.delete();
					break;
				case ExecuteScriptGroup:
					String sgName = r.replaceVariables(null, step.args.get(0));
					ScriptGroup sg = r.getScriptGroup(sgName);
					if (sg == null)
					{
						// making sure the txn_command_history is updated properly
						status.logStatus(step, null, Status.StatusCode.Running);
						status.logStatus(step, null, Status.StatusCode.Failed);
						throw new Exception("Script Group " + sgName + " can not be found in the repository");
					}					
					try
					{	
						if (status.isComplete(step.command, sgName))
						{
							logger.debug("Step ExecuteScriptGroup " + sgName + " is already performed, this step will not be repeated.");
							break;
						}
						status.logStatus(step, sgName, Status.StatusCode.Running);
						boolean allScriptsSuccessful = true;
						if (sg.Scripts == null || sg.Scripts.length == 0)
						{
							logger.warn("Empty script group: There are no scripts defined in script group " + sg.Name);
						} else
						{
							initializeJdsp(r);
							DatabaseConnection conn = r.getDefaultConnection();							
							for (Script scr : sg.Scripts)
							{	
								String scriptName = sgName + ":" + scr.Name;
								try
								{
									if (status.isComplete(step.command, scriptName))
									{
										logger.debug("Step ExecuteScriptGroup " + scriptName + " is already performed, this step will not be repeated.");
										continue;
									}
									status.logStatus(step, scriptName , Status.StatusCode.Running);
									scr.execute(r, conn, null, jdsp);
									status.logStatus(step, scriptName , Status.StatusCode.Complete);
								}catch(Exception ex)
								{
									allScriptsSuccessful = false;
									logger.error("ExecuteScriptGroup error while running script " + scriptName + " ",  ex);
									status.logStatus(step, scriptName, Status.StatusCode.Failed);									
								}
							}
						}
						if(allScriptsSuccessful)
						{
							status.logStatus(step, sgName, Status.StatusCode.Complete);
						}
						else
						{
							status.logStatus(step, sgName, Status.StatusCode.Failed);
						}
					}catch(Exception ex)
					{						
						status.logStatus(step, sgName, Status.StatusCode.Failed);
						logger.error("ExecuteScriptGroup error ",  ex);						
					}					
					break;
				case TestWrapper:
					testWrapper();
					break;
				case FillSessionVariables:
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					rsc = ResultSetCache.getInstance(r.getApplicationIdentifier(), false);
					String userName = step.args.get(0);
					Session session = new Session(userName);
					session.fillSessionVariables(r, rsc);
					session.setCancelled(false);
					cmdSession.setRepSession(session);
					logger.debug(session.toString());
					break;
				case CloseSession:
					if (cmdSession.getRepSession() != null)
					{
						cmdSession.getRepSession().setCancelled(true);
					}
					break;
				case ExportWarehouse:
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					String loadID = step.args.get(0);
					int loadnum = -1;
					if (!loadID.equalsIgnoreCase("all"))
					{
						try
						{
							loadnum = Integer.parseInt(loadID);
						} catch (Exception ex)
						{
							throw (new Exception("Invalid load number specified"));
						}
					}
					String directory = step.args.get(1);
					ExportWarehouse ew = new ExportWarehouse(r);
					ew.export(loadnum, directory, step.args.size() > 2 ? step.args.get(2).substring(11) : null);
					break;
				case LoadInfoBright:
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					directory = step.args.get(0);
					LoadInfoBright lib = new LoadInfoBright(r);
					lib.load(directory);
					break;
				case SetDefaultConnection:
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					r.setDefaultConnection(step.args.get(0));
					cmdSession.setConnection(null);
					try
					{
						conn = cmdSession.getConnection();
						status = new Status(r, currentMonthID, null);
						cmdSession.setStatus(status);
					} catch (SQLException sqe)
					{
						cmdSession.getWriter().println("Unable to connect to data source");
						error = true;
						break;
					}
					break;
				case LoadMarker:
					String loadNumber = "-1";
					String lgName = null;
					try
					{		
						loadNumber = Integer.valueOf(r.replaceVariables(null, step.args.get(0))).toString();
						lgName = step.args.get(1);
						int markStatus = Integer.valueOf(step.args.get(2));
						Status.StatusCode statusCode = markStatus == 1 ? Status.StatusCode.Running : Status.StatusCode.None;
						statusCode = markStatus == 2 ? Status.StatusCode.Complete : statusCode;
						if (statusCode == Status.StatusCode.Running)
						{
							//set iteration
							status.setIteration(loadNumber);
						}
						status.logStatus(step, lgName, statusCode);
						status.flush();
					}catch(Exception ex)
					{						
						status.logStatus(step, lgName, Status.StatusCode.Failed);
						logger.error("ExecuteScriptGroup error ",  ex);						
					}					
					break;	
				case EnforceSnapshotPolicy:
					if (r == null || path == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					SnapshotPolicy.enforce(r);
					break;
				case CreateTestDataFiles:
					if (r == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					String testDatafilePath = step.args.get(0);
					long numRowsInEachFile = Long.parseLong(step.args.get(1));
					TestDataFileCreator tdfc = new TestDataFileCreator(r, testDatafilePath, numRowsInEachFile);
					tdfc.createAll();
					break;
				case PerformAnalyzeSchema:
					if (r == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					loadNumber = Integer.valueOf(r.replaceVariables(null, step.args.get(0))).toString();
					String lg = step.args.get(1);
					status.setIteration(loadNumber);
					if (status.isComplete(step.command, lg))
					{
						logger.debug("Step PerformAnalyseSchema is already performed, this step will not be repeated.");
						break;
					}
					if (!DatabaseConnection.isDBTypeParAccel(r.getDefaultConnection().DBType))
					{
						logger.warn("Received PerformAnalyseSchema command for non paraccel connection.");
						break;
					}
					status.logStatus(step, lg, Status.StatusCode.Running);
					try
					{
						Database.performAnalyzeSchema(r.getDefaultConnection(), loadNumber);
					}catch(Exception ex)
					{						
						status.logStatus(step, lg, Status.StatusCode.Failed);
						throw ex;						
					}
					status.logStatus(step, lg, Status.StatusCode.Complete);
					break;
				case PerformVacuumSchema:
					if (r == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					loadNumber = Integer.valueOf(r.replaceVariables(null, step.args.get(0))).toString();
					String loadgroup = step.args.get(1);
					status.setIteration(loadNumber);
					if (!DatabaseConnection.isDBTypeParAccel(r.getDefaultConnection().DBType))
					{
						logger.warn("Received PerformVacuumSchema command for non paraccel connection.");
						break;
					}
					status.logStatus(step, loadgroup, Status.StatusCode.Running);
					try
					{
						Database.performVacuumSchema(r.getDefaultConnection(), loadNumber);
					}catch(Exception ex)
					{						
						status.logStatus(step, loadgroup, Status.StatusCode.Failed);
						throw ex;						
					}
					status.logStatus(step, loadgroup, Status.StatusCode.Complete);
					break;
				case UpdateStats:
					if (r == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					if (!DatabaseConnection.isDBTypeMSSQL(r.getDefaultConnection().DBType))
					{
						logger.warn("Received updatestats command for non mssql connection - updatestats is supported only for mssql.");
						break;
					}
					try
					{
						Database.performUpdateStats(r.getDefaultConnection());
					}catch(Exception ex)
					{						
						throw ex;						
					}
					break;
				case Exec:
					logger.info("Executing command Exec: " + step);
					Process pr = Runtime.getRuntime().exec("cmd /Q /C \""+step.args.get(0)+"\"");
					BufferedReader stdOut = new BufferedReader(new InputStreamReader(pr.getInputStream(),"UTF-8"));
					String line = null;
					while ((line = stdOut.readLine()) != null)
					{
						logger.info(line);
					}
					try
					{
						pr.waitFor();
					} catch (InterruptedException e)
					{
						logger.error(e);
					}
					break;
				case Touch:
					logger.info("Executing command Exec: " + step);
					String filePath = step.args.get(0);
					File file = new File(filePath);
					if (!file.exists())
					{
						logger.error("File " + filePath + " does not exist.");
						error = true;
					}
					file.setLastModified(System.currentTimeMillis());
					break;
				case SplitSourceFilesForPartitions:
					SourceFileSplitter sfs = r.getSourceFileSplitter(step.args.get(0));
					sfs.split();
					break;
				case SetPartitionConnection:
					partitionConnection = step.args.get(0);
					break;
				case DropTempTables:
					String ttPrefix = step.args.get(0);
					TempTableDrop ttDrop = new TempTableDrop();
					ttDrop.dropTempTables(r, r.getDefaultConnection(), ttPrefix);
					break;
				case ConsolidateTxnCmdHistory:
				{
					loadGroupName = getLoadGroupName(step.args);
					String[] subGroups = getSubGroups(step.args);
					TxnCommandHistoryConsolidator consolidator = new TxnCommandHistoryConsolidator(r, 
							r.getDefaultConnection(), step.args.get(0), loadGroupName, 
							subGroups, step.args.contains("runinpollingmode"));
					consolidator.consolidate();
					break;
				}
				case ValidateMetadata:
				{
				  if (r == null)
				  {
				    throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
				  }
				  int argIndex = 0;
				  String execFlag = step.args.get(argIndex++);
				  String ruleOrGrpName = null;
				  if (execFlag.equalsIgnoreCase("rule") || execFlag.equalsIgnoreCase("rulegroup"))
				  {
				    ruleOrGrpName = step.args.get(argIndex++);						
				  }
				  MetadataValidator validateMetadata = new MetadataValidator(r, execFlag, ruleOrGrpName, true);				  
				  List<RuleExecutionResult> ruleExecutionResults = validateMetadata.validate();				  
				  PrintStream writer = (cmdSession.getResultsWriter() == null) ? cmdSession.getWriter() : cmdSession.getResultsWriter();
				  for (RuleExecutionResult ruleExecResult : ruleExecutionResults)
				  {
				    writer.println(ruleExecResult.getValidationMessage());
				    for(String msg: ruleExecResult.getErrorList())
				    {
				    	writer.println(msg);
				    }
				  }
				  break;
				}
				case FixMetadata:
				{
				  if (r == null)
				  {
				    throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
				  }
				  int fixArgumentIndex = 0;
				  String fixFlag = step.args.get(fixArgumentIndex++);
				  String fixRuleOrGrpName = null;
				  if (fixFlag.equalsIgnoreCase("rule") || fixFlag.equalsIgnoreCase("rulegroup"))
				  {
				    fixRuleOrGrpName = step.args.get(fixArgumentIndex++);            
				  }                  
				  MetadataValidator fixMetadata = new MetadataValidator(r, fixFlag,fixRuleOrGrpName, true);          
				  List<RuleExecutionResult> ruleExecutionResults = fixMetadata.fix();         
				  PrintStream writer = (cmdSession.getResultsWriter() == null) ? cmdSession.getWriter() : cmdSession.getResultsWriter();
				  for (RuleExecutionResult ruleExecResult : ruleExecutionResults)
				  {
				    writer.print(ruleExecResult.getFixMessage());
				  }
				  break;
				}
				case CopyCatalog:
				{
					CopyUtil.copyCatalogDir(step.args.get(0), step.args.get(1), step.args.get(2).split(","), step.args.get(3).split(","), Boolean.valueOf(step.args.get(4)));
					break;
				}
				case ListIndices:
				{
					if (r == null)
					{
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					}
					String dbIndices = Database.getDatabaseIndicesInfo(r);
					PrintStream writer = (cmdSession.getResultsWriter() == null) ? cmdSession.getWriter() : cmdSession.getResultsWriter();
					writer.println();
					writer.print(dbIndices);
					break;
				}
				case ListAggregatesPhysical:
				{
					if (r == null)
					{
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					}
					Aggregate aggregates[] = r.getAggregates();
					if (aggregates!=null && aggregates.length>0)
					{
						System.out.println(System.lineSeparator());
						for (Aggregate aggregate : aggregates)
						{
							System.out.println(aggregate.getPhysicalName(r));
						}
					}
					break;
				}
				case ReInitLoadVariables:
				{
					if (r == null)
						throw (new Exception(VALID_REPOSITORY_NOT_SPECIFIED));
					r.reInitLoadVariables();
					break;
				}
				case PerformAsyncTask:
				{
					int consumerIndex = -1, msgIndex = -1;
					String asyncClass = null, msgId = null, uName = null, spaceId = null, resultData = null, inputData = null;
					for(String s: step.args)
					{
						if (s.startsWith("asyncclass="))
						{
							asyncClass = s.substring("asyncclass=".length());
						}
						else if (s.startsWith("consumerindex="))
						{
							consumerIndex = Integer.parseInt(s.substring("consumerindex=".length()));
						}
						else if (s.startsWith("msgid="))
						{
							msgId = s.substring("msgid=".length());
						}												
						else if (s.startsWith("username="))
						{
							uName = s.substring("username=".length());
						}
						else if (s.startsWith("spaceid="))
						{
							spaceId = s.substring("spaceid=".length());
						}
						else if (s.startsWith("inputdata="))
						{
							inputData = s.substring("inputdata=".length());
						}
						else if (s.startsWith("resultdata="))
						{
							resultData = s.substring("resultdata=".length());
						}
					}
					Message msg = new Message();
					msg.setUserName(uName);
					msg.setSpaceId(spaceId);
					msg.setInputData(inputData);
					msg.setResultData(resultData);
					msg.setId(msgId);					
					@SuppressWarnings("unchecked")
					Class<? extends QueueConsumer> clazz = (Class<? extends QueueConsumer>) Class.forName(asyncClass);
					QueueConsumer consumer = QueueConsumerFactory.createQueueConsumer(clazz, msg, consumerIndex);
					consumer.consume();
					consumer.postConsumption();					
					break;
				}
				}
				long delta = System.currentTimeMillis() - tStart;
				int min = (int) (delta / 1000 / 60);
				int sec = (int) ((delta / 1000) % 60);
				logger.info("Elapsed Time = " + min + " minutes, " + sec + " seconds for: " + step.command);
				logger.debug("Step details: " + step.toString());
			} catch (RepositoryException ex)
			{
				error = true;
				cmdSession.getWriter().println(ex.toString());
				logger.error(ex.toString());
			} catch (NavigationException nex)
			{
				error = true;
				cmdSession.getWriter().println("Navigation error: " + nex.getMessage());
				logger.error("Navigation error: " + nex.getMessage());
			} catch (DataUnavailableException du)
			{
				error = true;
				if (du.getCause() != null)
				{
					cmdSession.getWriter().println(du.getCause().toString());
				} else
				{
					cmdSession.getWriter().println(du.toString());
				}
				logger.error(du.toString(), du);
			} catch (Exception e)
			{
				error = true;
				cmdSession.getWriter().println(e.toString());
				if (e.getCause() != null)
				{
					cmdSession.getWriter().println(e.getCause().toString());
				} else
				{
					cmdSession.getWriter().println(e.toString());
				}
				logger.error(e.toString(), e);
			}
		}
	}

	private AWSCredentials getS3Credential(Step step) throws Exception {
		AWSCredentials s3credentials = null;
		try
		{		
			String awsAccessKeyId = getParameter(step.args, "awsAccessKeyId");
			String awsSecretKey = getParameter(step.args, "awsSecretKey");
			String awsToken = getParameter(step.args, "awsToken");
		
			if (awsAccessKeyId != null && awsSecretKey != null && awsToken != null)		
				s3credentials = new BasicSessionCredentials(awsAccessKeyId, awsSecretKey, awsToken);			
			else if (awsAccessKeyId != null && awsSecretKey != null && awsToken == null)			
				s3credentials = new BasicAWSCredentials(awsAccessKeyId, awsSecretKey);			
		}catch(Exception ex)
		{
				throw(new Exception("Could not Copy Table " +  step.args.get(2) + " : " + ex));
		}
		
		return s3credentials;
	}

	private String getLoadGroupName(List<String> args) throws Exception
	{
		String lgName = null;
		for (String param : args)
		{
			if (param.startsWith("loadgroup") && param.indexOf("=") != -1)
			{
				lgName = param.substring(param.indexOf("=") + 1);
				LoadGroup lg = r.findLoadGroup(lgName);
				if (lg == null)
				{
					throw new Exception("Load group " + lgName + " not found in repository.");
				}
				break;
			}
		}
		return lgName;
	}

	private String[] getSubGroups(List<String> args) throws Exception
	{
		String subgroups = null;
		for (String param : args)
		{
			if (param.startsWith("subgroup") && param.indexOf("=") != -1)
			{
				subgroups = param.substring(param.indexOf("=") + 1);
				if (subgroups.equalsIgnoreCase("none"))
					return new String[0];
				return subgroups.split(",");
			}
		}
		return null;
	}
	
	/**
	 * get a parameter from the command line of the command
	 * @param args
	 * @param name
	 * @return
	 * @throws Exception
	 */
	private String getParameter(List<String> args, String name) throws Exception
	{
		for (String param : args)
		{
			if (param.toLowerCase().startsWith(name.toLowerCase()) && param.indexOf("=") != -1)
			{
				return param.substring(param.indexOf("=") + 1);
			}
		}
		return null;
	}

	private String getDatabasePath(List<String> args) throws Exception
	{
		return getParameter(args, "databasepath");
	}

	private int getNumRows(List<String> args) throws Exception
	{
		int result = -1;
		for (String param : args)
		{
			if (param.startsWith("numrows") && param.indexOf("=") != -1)
			{
				String val = param.substring(param.indexOf("=") + 1);
				try
				{
					result = Integer.valueOf(val);
				} catch (Exception e)
				{
					result = -1;
				}
				break;
			}
		}
		return result;
	}

	private boolean isSkipTransformations(List<String> args) throws Exception
	{
		boolean skipTransformations = false;
		for (String param : args)
		{
			if (param.equalsIgnoreCase("skiptransformations"))
			{
				skipTransformations = true;
				break;
			}
		}
		return skipTransformations;
	}

	private Calendar getSCDDate(List<String> args) throws Exception
	{
		Calendar scddate = null;
		for (String param : args)
		{
			if (param.startsWith("scddate="))
			{
				String scdstr = param.substring(param.indexOf("=") + 1);
				scdstr = r.replaceVariables(null, scdstr);
				scddate = Calendar.getInstance();
				//expects MM/dd/yyyy format
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				scddate.setTime(sdf.parse(scdstr));
				break;
			}
		}
		return scddate;
	}

	private void initializeJdsp(Repository r)
	{
		if (jdsp == null)
		{
			JasperDataSourceProvider jasperdsp = null;
			if (rsc == null)
			{
				rsc = ResultSetCache.getInstance(r.getApplicationIdentifier(), false);
			}
			jasperdsp = new JasperDataSourceProvider(r, rsc);
			if (cmdSession.getMaxRecords() != -1)
			{
				jasperdsp.setMaxRecords(cmdSession.getMaxRecords());
			}
			if (cmdSession.getRepSession() != null)
			{
				jasperdsp.setSession(cmdSession.getRepSession());
			}
			setJasperDataSourceProvider(jasperdsp);
		}
	}

	/**
	 * @return the error
	 */
	public boolean isError()
	{
		return error;
	}

	/**
	 * for easy prototyping of performance engine commands and changes
	 * 
	 */
	private void testWrapper()
	{
	}
}
