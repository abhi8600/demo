/**
 * $Id: BadColumnNameException.java,v 1.2 2009-12-22 19:01:44 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import com.successmetricsinc.util.BaseException;

/**
 * Exception thrown when a bad column is used
 * 
 * @author bpeters
 * 
 */
public class BadColumnNameException extends BaseException
{
    private static final long serialVersionUID = 1L;
    public BadColumnNameException(String columnName)
    {
        super(columnName);
    }

	@Override
	public int getErrorCode() {
		return BaseException.ERROR_BAD_COLUMN;
	}
}
