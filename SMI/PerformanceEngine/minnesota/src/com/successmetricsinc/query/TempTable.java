/**
 * 
 */
package com.successmetricsinc.query;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author bpeters
 *
 */
public class TempTable
{
	public String tableName;
	public String create;
	public String query;
	public String pname;
	public AtomicBoolean created = new AtomicBoolean();
	public AtomicInteger referenceCount = new AtomicInteger();
	public long lastAccessed;
	public boolean liveAccessTmpTable;
}
