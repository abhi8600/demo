/**
 * $Id: ResultSetCache.java,v 1.281 2012/11/02 23:14:22 BIRST\ricks Exp $
 *
 * Copyright (C) 2007-2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.CommandResult;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.query.Aggregation.AggregationType;
import com.successmetricsinc.query.QueryResultSet.RetrievedFrom;
import com.successmetricsinc.query.olap.DimensionMemberSetNav;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.RepositoryException;
import com.successmetricsinc.util.SoftHashMap;
import com.successmetricsinc.util.Util;

/**
 * Class to implement query result set caching. Two-levels of caching are provided. The first level is an exact-match
 * memory-based cache for maximum speed. The second-level is a disk-based cache that supports more fuzzy cache matching.
 * If memory cache is missed, then an attempt to find a cache entry that can satisfy the current query is made. If it
 * can be done, the new result set is created and added to the memory cache. Utilizes the QueryResultSet capabilities of
 * filtering, sorting and grouping.
 * 
 * @author bpeters
 * 
 */
public class ResultSetCache implements Serializable
{
	private static Logger logger = Logger.getLogger(ResultSetCache.class);
	private static final long serialVersionUID = 1L;

	private Map<String, QueryResultSet> mcmap; // maps the query key to a resultset
	private Set<String> processingSet; // sets of query keys for queries currently being retrieved
	RowMapCache rmap; // row map cache

	private static final int MAX_CACHE_SIZE = 1000; // do not cache any result set with more than this many rows in the memory cache, let it just go to the exact file cache
	private static final int MAX_CACHE_CHECK_TIME = 3000; // milliseconds, if the file cache takes longer than this to check, note it

	private static Map<String, ResultSetCache> rscCache = Collections.synchronizedMap(new HashMap<String, ResultSetCache>());
	
	// interesting statistics
	protected int queryCount = 0; // number of queries performed
	protected int memoryHit = 0; // number of times the memory cache was hit
	protected int exactFileHit = 0; // number of times the exact file cache was hit
	protected int inexactFileHit = 0; // number of times that inexact file cache was hit
	
	protected File publishLock;
	protected boolean importedSpacePublishingOrHasPublished = false;
	
	//MongoDB collections
	public static final String COLL_ROWMAP_CACHE = "RowCacheMap"; //rmap replacement, key: 
	public static final String COLLECTION_EXACT_QUERY = "ExactQuery"; //cfmap
	public static final String COLLECTION_IN_EXACT_QUERY = "InExactQuery"; //cmap
	public static final String COLLECTION_FILE_QUERY_INFO = "FileQueryInfo"; //entryMap
	
	//Mongo fields
	public static final String MF_ID = "_id";
	public static final String MF_KEY = "key";
	public static final String MF_FILES = "files";
	public static final String MF_FILE = "file";
	public static final String MF_EXACT_ARR = "exactColumnKeyRefs";
	public static final String MF_INEXACT_ARR = "inExactColumnKeyRefs";
	public static final String MF_CACHE_ENTRY = "cacheEntry";
	
	private static String filePrefix = null;
	
	private String repositoryId;

	/**
	 * get an instance of the cache, resetting it if necessary
	 * @param repositoryId
	 * @param resetCache
	 * @return
	 */
	public synchronized static ResultSetCache getInstance(String repositoryId, boolean resetCache)
	{	
		ResultSetCache rsc = rscCache.get(repositoryId);
		if (rsc == null)
		{
			rsc = new ResultSetCache(repositoryId, resetCache);
			rscCache.put(repositoryId, rsc);
		}
		else
		{
			if (resetCache)
			{
				rsc.resetCache();
			}
		}
		return rsc;
	}

	public String getRepositoryId(){
		return repositoryId;
	}
	
	public void setPublishLockPath(String path)
	{
		publishLock = new File(path);
	}
	
	/**
	 * return the canonical name for the cache directory  (get rid of ../)
	 * @param cacheDir
	 * @return
	 */
	private static String getCanonicalCacheDirectoryName(String cacheDir)
	{
		File dir = new File(cacheDir);
		String name;
		try
		{
			name = dir.getCanonicalPath();
		} catch (Exception ex)
		{
			name = cacheDir;
		}
		return name;
	}
	
	/**
	 * uses the shared directory
	 * @param repositoryId
	 * @return
	 */
	public static String getSharedDirectory(String repositoryId){
		String cacheDir = MongoMgr.getRootSharedCacheDir() + File.separator + repositoryId;
		return getCanonicalCacheDirectoryName(cacheDir);
	}
	

	/**
	 * create a new instance of a cache, resetting it if necessary
	 * 
	 * @param cacheDir
	 *            Directory to place cache entries
	 * @param resetCache
	 *            Reset the cache
	 */
	private ResultSetCache(String repositoryId, boolean resetCache)
	{
		this.repositoryId = repositoryId;

		// create the memory cache
		mcmap = Collections.synchronizedMap(new SoftHashMap<String, QueryResultSet>("Memory ResultSet Cache"));
		processingSet = Collections.synchronizedSet(new HashSet<String>());

		// create the shared file cache
		String dir = getSharedDirectory(repositoryId);
		Util.createDirectory(dir);
		if (resetCache)
			resetCache();
	}

	/**
	 * remove the cache from memory
	 * 
	 * @param cacheDir
	 *            key to the hashmap (cache directory)
	 */
	public synchronized static void dispose(String repositoryId)
	{
		if (rscCache.remove(repositoryId) != null)
		{
			logger.info("Removed cache from memory (not deleting): " + repositoryId);
		}
	}

	public int getMemoryResultSetCacheSize()
	{
		if (mcmap == null)
			return 0;
		synchronized (mcmap)
		{
			return mcmap.size();
		}
	}

	public int getQueryCount()
	{
		return queryCount;
	}

	public int getMemoryHitCount()
	{
		return memoryHit;
	}

	public int getExactFileHitCount()
	{
		return exactFileHit;
	}

	public int getInexactFileHitCount()
	{
		return inexactFileHit;
	}

	/**
	 * Return a filtered result set. Allows a global query to be used many times, returning unique result sets for
	 * different instances of a given column.
	 * 
	 * @param q
	 *            Query to retrieve
	 * @param statementList
	 *            List of database statements to use to fulfill query
	 * @param maxRecords
	 *            Maximum records to return
	 * @param displayFilters
	 *            Display filters to apply
	 * @param expressionList
	 *            Display expressions to apply
	 * @param displayOrderList
	 *            Display orderings to apply
	 * @param session
	 *            Current session
	 * @param save
	 *            Whether to save the resulting query result set in cache if it needs to be generated
	 * @return QueryResultSet
	 * @throws CloneNotSupportedException 
	 * @throws RepositoryException 
	 * @throws ClassNotFoundException 
	 * @throws IOException 
	 * @throws DisplayExpressionException
	 * @throws NavigationException
	 * @throws SyntaxErrorException
	 * @throws DataUnavailableException
	 * @throws ClassNotFoundException
	 * @throws SessionVariableUnavailableException
	 * @throws GoogleAnalyticsException
	 * @throws RepositoryException 
	 * @throws IOException 
	 * @throws ScriptException 
	 * @throws Exception
	 */
	public QueryResultSet getQueryResultSet(Query q, int maxRecords, int maxQueryTime,
			List<DisplayFilter> displayFilters, List<DisplayExpression> expressionList, List<DisplayOrder> displayOrderList, Session session, boolean save,
			boolean compareQueryKeys, TimeZone displayTimeZone, boolean ignoreDemoMode) throws SQLException, BaseException, CloneNotSupportedException, IOException, ClassNotFoundException
	{
		queryCount++;
		boolean demoMode = false;
		if (!ignoreDemoMode && session != null && session.isDemoMode())
		{
			demoMode = true;
		}
		if (session != null && session.isCancelled())
		{
			throw new QueryCancelledException("Query cancelled by user.");
		}
		Query vq = null;
		List<DisplayOrder> volatileSorts = null;
		if (!q.isVolatileCacheQuery())
		{
			if (displayFilters == null)
				displayFilters = new ArrayList<DisplayFilter>();
			volatileSorts = new ArrayList<DisplayOrder>();
			vq = extractVolatileCacheQuery(q, displayFilters, volatileSorts, session);
		}
		VolatileResultsThread volatileThread = null;
		if (vq != null)
		{
			/*
			 * Add a measure to the volatile query if necessary
			 */
			if (vq.addVolatileMeasure())
			{
				addVolatileMeasure(q, vq);
			}
			logger.debug("Volatile Query: " + vq.getLogicalQueryString(false, null, false));
			volatileThread = new VolatileResultsThread(this, vq, maxRecords, maxQueryTime, session, displayTimeZone);
			volatileThread.start();
		}
		boolean hitCache = false;
		boolean hitMemoryCache = false;
		int numOriginalColumns = 0;
		List<Object> clist = null;
		QueryResultSet qrs = null;
		CacheHitResult chr = null;
		boolean aggregatedResults = false;
		String key = null;
		q.replaceVariables(session);
		if (displayFilters != null)
		{
			for (int i = 0; i < displayFilters.size(); i++)
			{
				DisplayFilter df = displayFilters.get(i);
				displayFilters.set(i, df.replaceVariables(q.getRepository(), session));
			}
		}
		/*
		 * Make sure any necessary columns are extracted from the display expressions
		 */
		clist = q.getSuppliedQueryColumns();
		numOriginalColumns = clist.size();
		List<String> addedNames = new ArrayList<String>();
		if (expressionList != null)
		{
			for (DisplayExpression de : expressionList)
			{
				de.extractAllExpressions(q, addedNames);
				de.getMeasureExpressionResultSets(this, maxRecords, maxQueryTime, session, q);
			}
		}
		//Unhide measure columns for multiple queries so query filters can be converted into display 
		//filters and bound after qrs is returned from db/cache.
		if(q.hasHiddenMeasureColumns())
		{
			q.unhideMeasureColumnsForMultipleQueries(session, addedNames);
		}
		if (!addedNames.isEmpty())
			q.clearKey();
		// Get the key for the query
		key = q.getKey(session);
		//
		if (q.getRepository().isLogCacheKey())
			logger.info("Cache key: " + key);
		//		
		if (logger.isTraceEnabled())
			logger.trace("Memory Query Key: " + key);
		
		try
		{
			boolean alreadyFetching = false;
			synchronized (processingSet)
			{
				if (processingSet.contains(key))
					alreadyFetching = true;
				else
					processingSet.add(key);
			}
			if (alreadyFetching)
			{
				/*
				 * If we are already fetching this query, wait for it to execute and get it from memory rather than
				 * running multiple instances at the same time
				 */
				int waitTime = 0;
				logger.debug("query already being run, waiting for the result - " + key);
				while (processingSet.contains(key))
				{
					try
					{
						Thread.sleep(100);
						waitTime += 100;
						if (waitTime > 1000 * maxQueryTime)
							break;
					} catch (InterruptedException e)
					{
						logger.error(e);
					}
				}
			}		
			qrs = mcmap.get(key);
			if (qrs != null && qrs.hasExpired()) // cache entry has expired (allows us to remove the NotCacheable flag on *Ago)
			{
				logger.debug("Memory cache entry expired: " + key);
				mcmap.remove(key);
				qrs = null;
			}
			CacheEntry ce = null;
			List<DisplayFilter> filterList = null;
			List<QueryFilter> addQueryFilterList = null;
			List<GroupBy> aggList = null;
			if (qrs != null)
			{
				memoryHit++;
				hitCache = true;
				hitMemoryCache = true;
				removeKeyFromProcessingSet(key);			
			} else
			{
				// check the file cache
				chr = checkFileCache(q.getRepository(), session, q, compareQueryKeys, displayOrderList);
				if (chr != null && chr.ce != null)
				{
					try
					{
						ce = chr.ce;
						hitCache = true;
						
						/*
						 * Apply filters if necessary
						 */
						if (chr.dfilterList != null && !chr.dfilterList.isEmpty())
						{
							filterList = chr.dfilterList;
							addQueryFilterList = chr.replacedFilters;
						}
						if (chr.aggregate)
						{
							aggList = q.getGroupByClauses();
						}
						if (chr.dorderList != null)
						{
							if (displayOrderList == null)
								displayOrderList = new ArrayList<DisplayOrder>();
							displayOrderList.addAll(0, chr.dorderList);
						}
					}
					finally
					{
						if(ce != null)
							removeKeyFromProcessingSet(key);
					}
				}
			}
			if (qrs == null && ce == null)
			{
				// no memory cached or file cached entry, get it from the data base
				chr = null;
				boolean expressionOnly = false;
				try
				{
					if (session != null)
						q.generateQuery(session, true);
					else
						q.generateQuery(true);
				} catch (NavigationException ne)
				{
					removeKeyFromProcessingSet(key);
					if (ne.getMessage().equals(Navigation.NO_DIMENSION_EXCEPTION) && expressionList != null && expressionList.size() > 0)
					{
						// Expression-only query
						expressionOnly = true;
					} else
						throw ne;
				}
				
				if (qrs == null)
					try
					{
						if (expressionOnly)
						{
							qrs = getExpressionOnlyResultSet(q);
						} else
						{
							qrs = getNewQueryResultSet(key, q, maxRecords, maxQueryTime, save, session);
						}
					} finally
					{
						removeKeyFromProcessingSet(key);
					}
				if (qrs == null || !qrs.isValid())
				{
					return new QueryResultSet();
				}
			} else if (ce != null)
			{
				// if from the file cache, need to do some processing of the result set
				try
				{
					qrs = ce.getResultSet(getSharedDirectory(repositoryId));
				} catch (Exception ex)
				{
					// handle corrupt cache
					logger.debug("Exception in getResultSet: " + ex.getMessage(), ex);
					resetCache();
					qrs = null;
					chr = null;
				}
				
				// determine if the query result set has expired
				if (qrs != null && qrs.hasExpired())
				{
					logger.debug("File cache entry expired: " + key);
					qrs = null;
					chr = null;
				}
	
				/*
				 * Apply dimension filters that need to be applied before aggregate
				 */
				if (qrs != null && filterList != null)
				{
					try
					{
						List<DisplayFilter> dflist = new ArrayList<DisplayFilter>();
						for (DisplayFilter df : filterList)
						{
							if (df.containsOnlyAttributes())
								dflist.add(df);
						}
						qrs = qrs.returnFilteredResultSet(dflist, q.getRepository());
					}
					catch(SyntaxErrorException se)
					{
						logger.debug("Cache miss due to exception: " + se.toString());
						qrs = null;
					}
					/*
					 * Since filters need to be applied to the cache entry, the associated filter objects need to be added
					 * to the query associated with the cache entry. This will ensure that the query key associated with
					 * this cache entry matches the result set
					 */
					if (qrs != null && chr != null && chr.ce != null)
					{
						Query ceq = chr.ce.getQuery();
						List<QueryFilter> qrsFilters = new ArrayList<QueryFilter>(ceq.getFilterList());
						boolean added = false;
						for (QueryFilter qf : addQueryFilterList)
						{
							if (!qrsFilters.contains(qf))
							{
								qrsFilters.add(qf);
								added = true;
							}
						}
						if (added)
						{
							ceq = (Query) ceq.clone();
							ceq.getFilterList().clear();
							ceq.getFilterList().addAll(qrsFilters);
							ceq.clearKey();
							ceq.setRepository(q.getRepository());
							ceq.createQueryKey(session);
							qrs.setQuery(ceq);
						}
					}
				}
				if (qrs != null)
				{
					/*
					 * Aggregate if necessary
					 */
					if (aggList != null)
					{
						qrs = qrs.returnAggregatedResultSet(aggList, q, displayOrderList);
						aggregatedResults = true;
					}
					/*
					 * Apply measure filters that need to be applied after aggregation
					 */
					if (filterList != null)
					{
						List<DisplayFilter> dflist = new ArrayList<DisplayFilter>();
						for (DisplayFilter df : filterList)
						{
							if (df.containsOnlyMeasures())
								dflist.add(df);
						}
						qrs = qrs.returnFilteredResultSet(dflist, q.getRepository());
					}
				}
				// if after processing the file cache result set, it is null, grab it from the database
				if (qrs == null)
				{
					hitCache = false;
	
					// no memory cached or file cached entry, get it from the data base
					if (session != null)
						q.generateQuery(session, true);
					else
						q.generateQuery(true);
					qrs = getNewQueryResultSet(key, q, maxRecords, maxQueryTime, save, session);
					if (qrs == null)
					{
						return new QueryResultSet();
					}
				}
			}
		}
		finally
		{		    
		    removeKeyFromProcessingSet(key);
		}
		/*
		 * If the cache was hit, the columns may not be in the correct order. Determine if so and setup for building a
		 * result set from the cache entry if needed
		 */
		boolean reorder = false;
		if (hitCache)
		{
			String[] colNames = qrs.getColumnNames();
			if (numOriginalColumns != colNames.length)
				reorder = true;
			else
				for (int i = 0; i < numOriginalColumns; i++)
				{
					if (clist.get(i).getClass() == DimensionColumnNav.class)
					{
						DimensionColumnNav dcn = (DimensionColumnNav) clist.get(i);
						if (!(dcn.dimensionName.equals(qrs.getTableNames()[i]) && dcn.columnName.equals(qrs.getColumnNames()[i]) && dcn.displayName.equals(qrs.getDisplayNames()[i])))
						{
							reorder = true;
							break;
						}
					} else if (clist.get(i).getClass() == DimensionMemberSetNav.class)
					{
						DimensionMemberSetNav dms = (DimensionMemberSetNav) clist.get(i);
						if (!(dms.displayName.equals(qrs.getDisplayNames()[i])))
						{
							reorder = true;
							break;
						}
					} else if (clist.get(i).getClass() == MeasureColumnNav.class)
					{
						MeasureColumnNav mcn = (MeasureColumnNav) clist.get(i);
						if (qrs.getColumnNames().length > i
								&& !(mcn.measureName.equals(qrs.getColumnNames()[i]) && mcn.displayName.equals(qrs.getDisplayNames()[i])))
						{
							reorder = true;
							break;
						} else if (mcn.filter != null)
						{
							if (!mcn.filter.equals(qrs.measureFilters[i]))
							{
								reorder = true;
								break;
							}
						}
					}
				}
			if (expressionList != null)
			{
				for (DisplayExpression de : expressionList)
				{
					if (de.getPosition() >= 0)
					{
						if (qrs.getColumnNames().length <= de.getPosition())
						{
							reorder = true;
							break;
						}
						if (!qrs.getColumnNames()[de.getPosition()].equals(de.getName()))
						{
							reorder = true;
							break;
						}
					}
				}
			}
		}
		// Display order conversion (XMLA)
		List<DisplayOrder> dorders = q.getDisplayOrders();
		if (dorders != null && !dorders.isEmpty())
		{
			if (displayOrderList == null)
				displayOrderList = dorders;
			else
				displayOrderList.addAll(dorders);
		}
		//If the query is federated convert the order by's to display orders		
		if (q.isFederated())
		{
			List<OrderBy> orderBys = q.getOrderByClauses();
			if (orderBys!=null && !orderBys.isEmpty())
			{
				for (OrderBy orderBy : orderBys)
				{
					boolean columnFound = false;
					String[] queryColumnNames = qrs.columnNames;
					if (queryColumnNames!=null && queryColumnNames.length>0)
					{						
						for (String colName : queryColumnNames)
						{
							if (colName.equals(orderBy.getColName()))
							{
								columnFound = true;
								break;
							}
						}
					}
					if (columnFound)
					{
						displayOrderList.add(orderBy.returnDisplayOrder());
					}
					else
					{
						logger.warn("Federated queries cannot order by on columns which are not present in the project list.");
					}
				}
			}
		}		
		if ((q.containsMultipleQueries || (qrs != null && qrs.q != null && qrs.q.containsMultipleQueries) || q.isFederated())
				&& q.getFilterList() != null)
		{
			// Bubble up measure filters if multiple subqueries are used
			for (QueryFilter qf: q.getFilterList())
			{
				if (qf.containsMeasure() || qf.containsOrOnDifferentConnections(null))
				{
					if (displayFilters == null)
						displayFilters = new ArrayList<DisplayFilter>();
					DisplayFilter df = qf.returnDisplayFilter(q.getRepository(), q.getSession());
					if(df != null)
					{
						displayFilters.add(df);
						reorder = true;						
					}
				}
			}
		}
		
		if((q.containsMultipleQueries || (qrs != null && qrs.q != null && qrs.q.containsMultipleQueries) 
			|| q.requiresDispalySortingForXMLA || (qrs != null && qrs.q != null && qrs.q.requiresDispalySortingForXMLA)) 
				&& (displayOrderList == null || displayOrderList.isEmpty()))
		{
			// Bubble up any order by clauses if multiple subqueries are used or there is an XMLA source used
			List<OrderBy> obClauses = q.getOrderByClauses();			
			if (obClauses != null && !obClauses.isEmpty())
			{
				if (displayOrderList == null)
					displayOrderList = new ArrayList<DisplayOrder>();
				for (OrderBy ob : obClauses)
				{
					DisplayOrder dorder = ob.returnDisplayOrder();
					displayOrderList.add(dorder);
				}
				reorder = true;
			}
		}
		
		if ((q.containsMultipleQueries || (qrs != null && qrs.q != null && qrs.q.containsMultipleQueries)) && q.hasTop())
		{
			q.dtopn = q.topn;
		}
				
		/*
		 * See if aggregations are necessary
		 */
		QueryResultSet unprocessedResults = qrs;		
		if ((expressionList != null && !expressionList.isEmpty()) || (displayOrderList != null && !displayOrderList.isEmpty()) || reorder
				|| (q.getAggregations() != null && !q.getAggregations().isEmpty()) || aggregatedResults)
		{
			if (qrs.q != null && qrs.q.containsMultipleQueries)
			{
				q.containsMultipleQueries = true;
			}
			qrs = qrs.returnProcessedResults(this, q, reorder, expressionList, displayOrderList, displayFilters, addedNames, rmap);			
		}
				
		if (qrs != null)
		{
			qrs.resetCursor(-1);
			if (hitCache && hitMemoryCache)
				qrs.retrievedFrom(RetrievedFrom.memory);
			else if (chr != null)
				qrs.retrievedFrom(chr.inexact ? RetrievedFrom.inexactfile : RetrievedFrom.exactfile);
			else 
				qrs.retrievedFrom(RetrievedFrom.database);
		}
		if (chr != null && chr.ce != null && qrs != null)
		{
			if ((expressionList == null || expressionList.isEmpty()) && (q.getAggregations() == null || q.getAggregations().isEmpty()))
			{
					String processedKey = qrs.getQuery().getKey(session);
					updateMemoryCache(processedKey, qrs);
					String cfName = chr.ce.getFileName();
					String unprocessedKey = unprocessedResults.getQuery().getKey(session);
					saveToExactMatchCache(cfName, unprocessedKey, chr);
			}	
		}

		if (qrs == null)
			return null;
		/*
		 * If volatile data is being retrieved, wait for its thread to finish
		 */
		if (volatileThread != null)
		{
			try
			{
				volatileThread.join();
			} catch (InterruptedException e)
			{
				logger.error(e, e);
				throw (new DataUnavailableException("Unable to retrive volatile columns", e));
			}
			/*
			 * Replace mapped columns
			 */
			qrs.replaceMappedColumns(volatileThread.getResults());
		}
		List<QueryResultSet> mappedResultSets = qrs.q.getMapResultSets();
		if (mappedResultSets.isEmpty())
		{
			Repository r = q.getRepository();
			/*
			 * If there are query mapped columns, need to get the query and set the map
			 */
			boolean generateQuery = false;
			boolean addResultSets = false;
			for (DimensionColumnNav dcn : qrs.q.dimensionColumns)
			{
				DimensionColumn dc = r.findDimensionColumn(dcn.dimensionName, dcn.columnName);
				if (dc == null)
					continue; // probably a constant column
				if (dc.DisplayMap != null && dc.DisplayMap.Type == DisplayMap.MapType.Query)
				{
					addResultSets = true;
					if (dc.DisplayMap.mapResultSet == null)
					{
						generateQuery = true;
						break;
					}
					if (!mappedResultSets.contains(dc.DisplayMap.mapResultSet))
						mappedResultSets.add(dc.DisplayMap.mapResultSet);
				}
			}
			if (addResultSets)
			{
				if (generateQuery)
				{
					qrs.q.setRepository(r);
					qrs.q.generateQuery(true);
				}
				for (DimensionColumnNav dcn : qrs.q.dimensionColumns)
				{
					DimensionColumn dc = r.findDimensionColumn(dcn.dimensionName, dcn.columnName);
					if (dc.DisplayMap != null && dc.DisplayMap.Type == DisplayMap.MapType.Query && dc.DisplayMap.mapResultSet != null)
					{
						if (!mappedResultSets.contains(dc.DisplayMap.mapResultSet))
							mappedResultSets.add(dc.DisplayMap.mapResultSet);
					}
				}
			}
		}
		for (QueryResultSet mqrs : mappedResultSets)
			qrs.replaceMappedColumns(mqrs);
		if (displayFilters != null && !displayFilters.isEmpty())
			qrs = qrs.returnFilteredResultSet(displayFilters, q.getRepository());
		if (volatileSorts != null && !volatileSorts.isEmpty())
			qrs.processDisplayOrderings(volatileSorts);
		qrs.removeDisplayNamesForAddedNames(addedNames);
		logOut(qrs);
		// final check for result set size (this is new in 2.2)
		if (qrs.numRows() > maxRecords)
		{
			logger.warn("Throwing error due to query result set (from cache, post filtering) being too big");
			throw new ResultSetTooBigException("Query result set is too big.  Please add filters to reduce the size of the result set.");
		}
		// XXX put decryption somewhere here, create a top level routine that only clones once
		// if any encrypted columns, decrypt them
		if (demoMode)
		{
			QueryResultSet qrsClone = qrs.clone(true);
			qrsClone.processMasks();
			return qrsClone;
		}
		qrs = qrs.clone(false);		
		return qrs;
	}
	
	private void removeKeyFromProcessingSet(String key) 
	{
		if(key != null)
		{
			synchronized (processingSet)
			{
				processingSet.remove(key);
			}
		}
	}

	private void saveToExactMatchCache(String cfName, String unprocessedKey, CacheHitResult chr) throws IOException
	{	
		if (chr.dfilterList == null || chr.dfilterList.isEmpty())
		{
			DB db = MongoMgr.getSpaceDb(repositoryId);

			//Use connection pool, marker to ensure all requests go through the same connection
			db.requestStart();

			DBCollection exactCol = db.getCollection(COLLECTION_EXACT_QUERY);
			
			//Check if our object id using the unprocessedKey is too long
			//Use the shortened string hashcode() int otherwise
			Object hash = getHashCodeIfStringMaxed(unprocessedKey);
			Object id = (hash != null) ? hash : unprocessedKey;
			
			//Write to exact query cache if it doesn't exist
			if(exactCol.findOne(id) == null){	
				BasicDBObject query2File = createBasicDBObjectWithKey(unprocessedKey);
				query2File.append(MF_FILE, cfName);
				exactCol.insert(query2File);
				CommandResult e = db.getLastError();
				if(MongoMgr.hasError(e)){
					logger.error("Unable to save to exact query cache, query key : " + unprocessedKey + " cachefile : " + cfName, e.getException());	
				}else{
					//TODO
					//Update the cache entry, include the query key as referencing the cache entry
				}
			}
			db.requestDone();
		}
	}
	
	/**
	 * Create a BasicDBObject with the "_id" set to parameter key if it fits within the index limit (800 bytes)
	 * Otherwise, we used the key's hashCode value as the "_id" and added the additional parameter "key" with the value
	 * of our key
	 * @param key
	 * @return
	 */
	private BasicDBObject createBasicDBObjectWithKey(String key){
		Integer hash = getHashCodeIfStringMaxed(key);
		BasicDBObject obj = null;
		if(hash == null){
			obj = new BasicDBObject(MF_ID, key);
		}else{
			obj = new BasicDBObject(MF_ID, hash);
			obj.append(MF_KEY, key);
		}
		
		return obj;
	}
	
	private Integer getHashCodeIfStringMaxed(String key){
		try
		{			
			if(key != null && key.getBytes("UTF-8").length > 800){
				return stringHashCode(key);				
			}
		}
		catch (UnsupportedEncodingException e)
		{}
		return null;
	}
	
	private Integer stringHashCode(String value){
		Integer hashCode = null;
		
		if(value != null){
			int len = value.length();
			int hash = 0;
			for (int i = 0; i < len; i++) {
				// hash * 31
				hash = (hash << 5) - hash + value.charAt(i);
			}
			hashCode = Integer.valueOf(hash);
		}
		
		return hashCode;
	}
	
	protected void logOut(QueryResultSet qrs)
	{
		logger.trace("Current row count: " + qrs.numRows() + ", query count: " + getQueryCount() + ", memory hits: " + getMemoryHitCount()
				+ ", memory cache entry count: " + getMemoryResultSetCacheSize() + ", exact file hits: " + getExactFileHitCount() + ", inexact file hits: "
				+ getInexactFileHitCount() + ", java free size: " + Runtime.getRuntime().freeMemory());
	}

	/**
	 * Class to execute a thread to retrieve the volatile column result sets
	 * 
	 * @author Brad Peters
	 * 
	 */
	private static class VolatileResultsThread extends Thread
	{
		private Query vq;
		private QueryResultSet results;
		private ResultSetCache rsc;
		private int maxRecords;
		private int maxQueryTime;
		private Session session;
		private Exception ex;
		private TimeZone displayTimeZone;

		public VolatileResultsThread(ResultSetCache rsc, Query vq, int maxRecords, int maxQueryTime,
				Session session, TimeZone displayTimeZone)
		{
			this.setName("Volatile Results - " + this.getId());
			this.setDaemon(true);
			this.vq = vq;
			this.rsc = rsc;
			this.maxRecords = maxRecords;
			this.session = session;
			this.displayTimeZone = displayTimeZone;
		}

		public void run()
		{
			try
			{
				results = rsc.getQueryResultSet(vq, maxRecords, maxQueryTime, null, null, null, session, true, displayTimeZone, true);
			} catch (Exception ex)
			{
				this.ex = ex;
				return;
			}
		}

		public QueryResultSet getResults()
		{
			return (results);
		}

		@SuppressWarnings("unused")
		public Exception getException()
		{
			return (ex);
		}
	}

	/**
	 * Find the lowest set of levels for each dimension in the query
	 * 
	 * @param q
	 * @return A collection of dimension column nav structures at the lowest levels
	 */
	private Set<DimensionColumnNav> getLowestLevelGroupBys(Query q)
	{
		Map<String, List<DimensionColumnNav>> s = new HashMap<String, List<DimensionColumnNav>>(q.dimensionColumns.size());
		Repository r = q.getRepository();
		for (DimensionColumnNav dcn : q.dimensionColumns)
		{
			// Only process dimension column navigations that are in group bys
			boolean isGroupBy = false;
			for (GroupBy gb : q.getGroupByClauses())
			{
				if (gb.getDimName().equals(dcn.dimensionName) && gb.getColName().equals(dcn.columnName))
				{
					isGroupBy = true;
					break;
				}
			}
			if (!isGroupBy)
				continue;
			List<DimensionColumnNav> dcnList = s.get(dcn.dimensionName);
			if (dcnList == null)
			{
				dcnList = new ArrayList<DimensionColumnNav>();
				dcnList.add(dcn);
				s.put(dcn.dimensionName, dcnList);
			} else
			{
				Hierarchy h = r.findDimensionHierarchy(dcn.dimensionName);
				for (DimensionColumnNav dcn2 : dcnList)
				{
					/*
					 * Otherwise, see if this column should replace any of the current columns in the list because it is
					 * at a lower level. Note, a column for which no explicit level relationship is defined will only be
					 * superceded by the dimension key level
					 */
					if (!dcn2.columnName.equals(dcn.columnName))
					{
						Level dimKeyLevel = h.DimensionKeyLevel;
						Level curLevel = h.findLevelColumn(dcn2.columnName);
						/*
						 * If there's already a column at the dimension key level, don't bother looking any more because
						 * this dimension is already at the lowest level it can be
						 */
						if (curLevel == dimKeyLevel)
							break;
						Level newLevel = curLevel.findLevelColumn(dcn.columnName);
						if (newLevel != null && newLevel != curLevel)
						{
							/* If this column is below the level of the current column, then choose it */
							dcnList.remove(dcn2);
							dcnList.add(dcn);
						} else
						{
							/* Otherwise, see if this new column is at the dimension key level */
							newLevel = h.findLevelColumn(dcn.columnName);
							if (newLevel == dimKeyLevel)
							{
								dcnList.clear();
								dcnList.add(dcn);
								break;
							}
						}
					}
				}
			}
		}
		Set<DimensionColumnNav> results = new HashSet<DimensionColumnNav>();
		for (List<DimensionColumnNav> dcnList : s.values())
		{
			results.addAll(dcnList);
		}
		return (results);
	}

	/**
	 * Analyze a query to determine if there are volatile columns present. If so, construct a query that will fill those
	 * columns and replace the columns in the original query with their corresponding keys. Volatile columns cannot be
	 * used for grouping as that would change the measure results from the main query. If it cannot be determined that
	 * grouping is being done at or below the level of the volatile column, then the column cannot be extracted.
	 * 
	 * @param q
	 * @return
	 * @throws NavigationException
	 */
	private Query extractVolatileCacheQuery(Query q, List<DisplayFilter> displayFilters, List<DisplayOrder> displayOrders, Session session)
			throws NavigationException, CloneNotSupportedException, BadColumnNameException
	{
		boolean hasVolatile = false;
		Repository r = q.getRepository();
		Set<DimensionColumnNav> levelSet = null;
		for (DimensionColumnNav dcn : q.dimensionColumns)
		{
			if (r.isVolatile(dcn.dimensionName, dcn.columnName))
			{
				/*
				 * Make sure that the column is not at the lowest level of a dimension hierarchy
				 */
				if (levelSet == null)
					levelSet = getLowestLevelGroupBys(q);
				if (!levelSet.contains(dcn))
					hasVolatile = true;
				else
					logger.debug("Cannot substitute for volatile column " + dcn.dimensionName + "." + dcn.columnName
							+ " as it is being used for grouping and this can affect measures calculated in this query");
				break;
			}
		}
		if (!hasVolatile)
			return null;
		/*
		 * Since we are changing columns, clear any previous navigation
		 */
		q.clearNavigation();
		/*
		 * Go through the dimension columns and replace/create
		 */
		Query vq = new Query(r);
		vq.setVolatileCacheQuery(true);
		for (DimensionColumnNav dcn : q.dimensionColumns)
		{
			if (!dcn.navigateonly && !dcn.constant && r.isVolatile(dcn.dimensionName, dcn.columnName))
			{
				DimensionColumn dc = r.findDimensionColumn(dcn.dimensionName, dcn.columnName);
				if (dc != null && dc.VolatileKey != null && dc.VolatileKey.length() > 0)
				{
					DimensionColumnNav keydcn = vq.addDimensionColumn(dcn.dimensionName, dc.VolatileKey);
					vq.addGroupBy(new GroupBy(dcn.dimensionName, dc.VolatileKey));
					vq.addMappedDimensionColumn(keydcn);
					vq.addDimensionColumn(dcn.dimensionName, dcn.columnName);
					vq.addGroupBy(new GroupBy(dcn.dimensionName, dcn.columnName));
					// Change groupings by old column
					for (GroupBy gb : q.getGroupByClauses())
					{
						gb.replaceDimensionColumn(dcn.dimensionName, dc.ColumnName, dc.VolatileKey);
					}
					// Need to substitute display orderings and display filters
					List<QueryFilter> removeFilters = new ArrayList<QueryFilter>();
					for (QueryFilter qf : q.getFilterList())
					{
						if (qf.containsAttribute(dcn.dimensionName, dcn.columnName))
						{
							DisplayFilter df = qf.returnDisplayFilter(r, session);
							if (df != null && df.isValid())
							{
								displayFilters.add(df);
								removeFilters.add(qf);
							} else
							{
								return null;
							}
						}
					}
					q.getFilterList().removeAll(removeFilters);
					List<OrderBy> removeOrders = new ArrayList<OrderBy>();
					for (OrderBy ob : q.getOrderByClauses())
					{
						if (ob.matchesDimensionColumn(dcn.dimensionName, dcn.columnName))
						{
							removeOrders.add(ob);
							displayOrders.add(ob.returnDisplayOrder());
						}
					}
					q.getOrderByClauses().removeAll(removeOrders);
					dcn.columnName = dc.VolatileKey;
					dcn.volatileColumn = true;
				}
			}
		}
		/*
		 * If a measure column needs to be added, mark it. After navigation, add the lowest cardinality measure selected
		 */
		if (!q.measureColumns.isEmpty() || !q.derivedMeasureColumns.isEmpty())
		{
			vq.setAddVolatileMeasure(true);
		}
		/*
		 * Add all filters
		 */
		for (QueryFilter qf : q.filters)
		{
			vq.addFilter((QueryFilter) qf.clone());
		}
		return (vq);
	}

	private void addVolatileMeasure(Query q, Query vq) throws NavigationException, CloneNotSupportedException, BadColumnNameException
	{
		q.navigateQuery(true, true, null, true);
		/*
		 * Add lowest cardinality measure
		 */
		MeasureColumnNav newmcn = null;
		double curCardinality = Double.POSITIVE_INFINITY;
		for (MeasureColumnNav mcn : q.measureColumns)
		{
			if (!mcn.navigateonly)
			{
				double newcard = mcn.getMeasureColumn().MeasureTable.Cardinality;
				if (newcard < curCardinality)
				{
					newmcn = mcn;
					curCardinality = newcard;
				}
			}
		}
		vq.addMeasureColumn(newmcn.measureName, newmcn.measureName, false);
	}

	/**
	 * Return a filtered result set. Allows a global query to be used many times, returning unique result sets for
	 * different instances of a given column.
	 * 
	 * @param q
	 *            Query to retrieve
	 * @param statementList
	 *            List of database statements to use to fullfill query
	 * @param maxRecords
	 *            Maxiumum records to return
	 * @param displayFilters
	 *            Display filters to apply
	 * @param expressionList
	 *            Display expressions to apply
	 * @param displayOrderList
	 *            Display orderings to apply
	 * @param session
	 *            Current session
	 * @param cacheHitObject
	 *            The result object from previously checking the cache. If null, cache hit detection is done, otherwise,
	 *            it will use this result
	 * @return QueryResultSet
	 * @throws RepositoryException 
	 * @throws ClassNotFoundException 
	 * @throws IOException 
	 * @throws CloneNotSupportedException 
	 * @throws DisplayExpressionException
	 * @throws NavigationException
	 * @throws SyntaxErrorException
	 * @throws DataUnavailableException
	 * @throws ClassNotFoundException
	 * @throws SessionVariableUnavailableException
	 * @throws GoogleAnalyticsException
	 * @throws RepositoryException 
	 * @throws IOException 
	 * @throws ScriptException 
	 * @throws  
	 * @throws Exception
	 */
	public QueryResultSet getQueryResultSet(Query q, int maxRecords, int maxQueryTime,
			List<DisplayFilter> displayFilters, List<DisplayExpression> expressionList, List<DisplayOrder> displayOrderList, Session session,
			boolean compareQueryKeys, TimeZone displayTimeZone, boolean ignoreDemoMode) throws SQLException, BaseException, CloneNotSupportedException, IOException, ClassNotFoundException
	{
		return (getQueryResultSet(q, maxRecords, maxQueryTime, displayFilters, expressionList, displayOrderList, session, true,
				compareQueryKeys, displayTimeZone, ignoreDemoMode));
	}

	/**
	 * 
	 * @param key
	 * @param q
	 * @param statementList
	 * @param maxRecords
	 * @param maxQueryTime
	 * @param save
	 * @param session
	 * @param expressionList
	 * @return QueryResultSet instance
	 * @throws SQLException
	 * @throws IOException
	 * @throws DataUnavailableException
	 * @throws QueryCancelledException
	 * @throws ResultSetTooBigException
	 * @throws SyntaxErrorException
	 * @throws QueryTimeoutException
	 * @throws PublishingException
	 * @throws GoogleAnalyticsException
	 * @throws IOException 
	 * @throws ScriptException 
	 */
	private QueryResultSet getNewQueryResultSet(String key, Query q, int maxRecords, int maxQueryTime,
			boolean save, Session session) throws SQLException, BaseException, IOException
	{
		// check for publishing, do not go to the DB, just fail
		if (isPublishingOrHasPublished())
		{
			throw (new PublishingException("Data unavailable to satisfy request: ", new Exception("New data is being published to the database")));
		}
		if (isImportedSpacePublishingOrHasPublished())
		{
			throw (new PublishingException("Data unavailable to satisfy request: ", new Exception("New data is being published in the imported space")));
		}
		
		if (!q.available())
		{
			logger.warn("The query syntax was incorrect: " + q.getQuery());
			throw (new DataUnavailableException("Data unavailable to satisfy request: ", new Exception(
					"Query has a navigation pointing to database connection type 'None'.")));
		} else
		{
			return (getCacheEntry(key, q, maxRecords, maxQueryTime, save, session));
		}
	}

	/**
	 * clear out the cache
	 */
	public synchronized void resetCache()
	{
		logger.info("Clearing the space cache");
		mcmap = Collections.synchronizedMap(new SoftHashMap<String, QueryResultSet>("Memory ResultSet Cache"));
		try
		{
			// reset the cache keys.
			// delete the cache index and files
			logger.info("Init -  drop mongo db : " + repositoryId);
			dropMongoDb();
			logger.info("Complete - drop mongo db : " + repositoryId);
			String cacheDir = getSharedDirectory(repositoryId);
			flagAndDeleteCacheFiles(cacheDir);
			logger.info("Finished resetCache operation : " + repositoryId);
		} catch (Error er)
		{
			logger.debug("ResultSetCache.resetCache: " + er.getMessage());
		} catch (Exception ioex)
		{
			logger.debug("ResultSetCache.resetCache: " + ioex.getMessage());
		}
		return;
	}
	
	private void flagAndDeleteCacheFiles(String cacheDir)
	{
		if(cacheDir != null)
		{
			String deleteFlagFilePath = cacheDir + File.separator + "deleting.lock";
			File cd = new File(cacheDir);
			boolean clearDeletingLockFile = false;
			File deletingLockFile = null;
			try
			{
				deletingLockFile = new File(deleteFlagFilePath);
				if(cd.exists() && deletingLockFile.createNewFile())
				{
					clearDeletingLockFile = true;
					logger.info("Init - delete the cache directory " + cacheDir);
					deleteCacheFiles(cacheDir);
					logger.info("Complete - delete the cache directory " + cacheDir);
					Util.deleteDir(new File(cacheDir));
					clearDeletingLockFile = false;
				}
			}catch(Exception ex)
			{
				logger.error("Error while deleting the cache files from cache directory (probably removed by another server): " + cacheDir + " - " + ex.getMessage());	
			}finally
			{
				try
				{
					if(clearDeletingLockFile && deletingLockFile != null && deletingLockFile.exists())
					{
						deletingLockFile.delete();
					}
				}catch(Exception ex2)
				{
					logger.error("Error while clearing the deleting.lock file from cache directory: " + cacheDir + " - " + ex2.getMessage());
				}
			}
		}
	}
	
	public void dropMongoDb() throws UnknownHostException
	{
		DB db = MongoMgr.getSpaceDb(repositoryId);
		if(db != null){
			db.requestStart();
			db.dropDatabase();
			db.requestDone();
		}
	}
	
	@SuppressWarnings("unused")
	private void dropCollection(DB db, String collectionName)
	{
		DBCollection coll = db.getCollection(collectionName);
		if(coll != null){
			coll.drop();
		}
	}
	
	public static void deleteCacheFiles(String dirName)
	{
		Util.deleteFiles(dirName, "*.cache");
	}

	/**
	 * Save a query result set to file cache
	 * 
	 * @param key
	 * @param qrs
	 * @throws Exception
	 */
	private synchronized void saveCacheEntry(String key, QueryResultSet qrs, boolean saveMaps) throws IOException
	{
		// double check that the cache didn't get updated between the original check and now
		if (mcmap.containsKey(key))
		{
			logger.debug("Another thread just cached " + key + ", skipping");
			return;
		}
		updateMemoryCache(key, qrs);
		
		// Temporarily remove any aggregation settings
		AggregationType[] aggs = qrs.aggregations;
		qrs.aggregations = new AggregationType[aggs.length];
		for (int i = 0; i < aggs.length; i++)
			qrs.aggregations[i] = AggregationType.None;

		// Save cache entry file
		File cacheFile = null;
		String cacheDir = getSharedDirectory(repositoryId);
		try
		{
			cacheFile = ResultSetCache.createCacheFile("cache", cacheDir);
			FileOutputStream fo = new FileOutputStream(cacheFile);
			ObjectOutput s = new ObjectOutputStream(fo);
			s.writeObject(qrs);
			qrs.setBaseFilename(cacheFile.getName());
			s.close();
			fo.close();
		} catch (Exception ex)
		{
			logger.error("Failed to create/save cache file in " + cacheDir);
			logger.debug(ex, ex);
			qrs.aggregations = aggs;
			return;
		}
		// Restore aggregations
		qrs.aggregations = aggs;
		saveCacheKeys(key, cacheFile, qrs, saveMaps);
	}
		
	private void saveCacheKeys(String key, File cacheFile, QueryResultSet qrs, boolean saveMaps) throws IOException{
		try{
			String cacheFileName = cacheFile.getName();

			// Store the entry in the map
			CacheEntry ce = new CacheEntry(qrs.getQuery(), cacheFile);
			ce.setFileName(cacheFileName);
			ce.setNumRows(qrs.numRows());

			Set<String> keyList = null;

			// Do not add query/results with top to fuzzy cache
			keyList =  (!qrs.getQuery().hasTop()) ? qrs.getQuery().getKeyList(false) : new HashSet<String>();

			DB db = MongoMgr.getSpaceDb(repositoryId);

			//Use connection pool, marker to ensure all requests go through the same connection
			db.requestStart();

			//Write the query information, cache entry reference-able by filename as key into the cache. 
			//This should not error in duplicate row, because cache file names should be unique.
			//Eg (we only print the _id -> cacheQueryKey here, actual data is more -
			/*
			 * > db.FileQueryInfo.find().forEach( function(x){ print("\n" + x._id + "  ->\n" + x.cacheEntry.q.queryKey) });
			 * cache9214357373677056142.cache  -> D{Products}C{ProductName}GB{D{Products}C{ProductName}}||
			 * cache1982919436080150158.cache  -> D{Products}C{CategoryName}D{Products}C{ProductName}
			 *  M{OrderDate: Sum: Quantity}GB{D{Products}C{CategoryName}}GB{D{Products}C{ProductName}}|F{D{Products}C{CategoryName}OP{=}V{Beverages}}|
			 * cache95981467850300440.cache  -> D{Products}C{ProductName}GB{D{Products}C{ProductName}}||
			 * cache8712167278537573767.cache  -> D{Products}C{CategoryName}D{Products}C{ProductName}M{OrderDate: Sum: Quantity}
			 * GB{D{Products}C{CategoryName}}GB{D{Products}C{ProductName}}|F{D{Products}C{CategoryName}OP{=}V{Beverages}}|
			 */
			DBCollection coll = db.getCollection(COLLECTION_FILE_QUERY_INFO);
			BasicDBObject c = new BasicDBObject(MF_ID, cacheFileName);
			c.append(MF_CACHE_ENTRY, ce.toBasicDBObject());
			//These in exact column keys refer to this cache file
			c.append(MF_INEXACT_ARR, keyList); 
			//The exact column keys referring to this cache file.
			ArrayList<String> exactKeys = new ArrayList<String>();
			exactKeys.add(key);
			c.append(MF_EXACT_ARR, exactKeys);
			coll.insert(c);
			CommandResult r = db.getLastError();

			//If the write to mongodb for the filecache entry fails, we exit, clean up and return here.
			if(MongoMgr.hasError(r)){
				logger.error("Error saving cache entry, deleting the cache file: " + cacheFileName, r.getException());
				deleteFile(cacheFileName);
				return;
			}

			//Write to exact cache match. (Query Key for entire query to cache file)
			/* > db.ExactQuery.find();
			 * { "_id" : "D{Products}C{ProductName}GB{D{Products}C{ProductName}}||", "file" : "cache9214357373677056142.cache" }
			 * { "_id" : "D{Products}C{CategoryName}D{Products}C{ProductName}M{OrderDate: Sum:
			 * Quantity}GB{D{Products}C{CategoryName}}GB{D{Products}C{ProductName}}|F{D{Products}C{CategoryName}OP{=}V{Beverages}}|", 
			 * "file" : "cache1982919436080150158.cache"}
			 */
			DBCollection exactCol = db.getCollection(COLLECTION_EXACT_QUERY);
			BasicDBObject query2File = createBasicDBObjectWithKey(key);
			query2File.append(MF_FILE, cacheFileName);
			exactCol.insert(query2File);
			CommandResult e = db.getLastError();

			if(MongoMgr.hasError(e)){
				//log it
				if(MongoMgr.isDuplicateError(e)){
					logger.info("Duplicate exact cache key: " + key + ", removing duplicate cache file: " + cacheFileName);
				}else{
					logger.error("Unable to save exact cache key: " + key, e.getException());
				}
				//try to remove the previous entries and file
				try{
					removeCacheFileEntry(cacheFileName, true);
				}catch(Exception ex){
					logger.debug(ex, ex);
				}
				return;
			}

			//Write to inexact query key cache match. (Query key for a single query component to cache files)
			//Update the column keys -> [cacheFile1, cacheFile2..] cache, eg-
			/* > db.InExactQuery.find(); 
			 * { "_id" : "D{Products}C{ProductName}", "files" : [ "cache9214357373677056142.cache", "cache1982919436080150158.cache",
			 *       "cache95981467850300440.cache", "cache8712167278537573767.cache" ] }
			 *{ "_id" : "D{Products}C{CategoryName}", "files" : [ "cache1982919436080150158.cache", "cache8712167278537573767.cache" ] }
			 *{ "_id" : "M{OrderDate: Sum: Quantity}", "files" : [ "cache1982919436080150158.cache", "cache8712167278537573767.cache" ] }
			 */
			DBCollection columnColl = db.getCollection(COLLECTION_IN_EXACT_QUERY);
			for (String ckey : keyList){
				BasicDBObject cacheFileEntry = new BasicDBObject(MF_FILES, cacheFileName);

				//$addToSet means add to the set if it exists, create and add to a new set if it doesn't exist. 
				BasicDBObject addToSet = new BasicDBObject("$addToSet", cacheFileEntry);
				BasicDBObject cacheKey = new BasicDBObject(MF_ID, ckey);

				//3rd arg true is to do upsert = update if it is there, add if it is not.
				columnColl.update(cacheKey, addToSet, true, false);
				CommandResult rs = db.getLastError();
				if(MongoMgr.hasError(rs)){
					logger.error("Fail to save inexact query cache key: " + cacheKey, rs.getException());
					//try to remove the previous entries and file
					try{
						removeCacheFileEntry(cacheFileName, true);
					}catch(Exception ex){
						logger.debug(ex, ex);
					}
					return;
				}
			}

			db.requestDone();
		}catch(com.mongodb.MongoException.Network ex){
			logger.error("Network error, MongoDB down?", ex);
		}
	}
	
	/**
	 * Mongo replacement for entryMap.get()
	 * @param query
	 */
	private CacheEntry getMongoCacheFileEntry(String cacheFilename, Map<String, CacheEntry> map){
		if (map != null && map.containsKey(cacheFilename))
			return map.get(cacheFilename);
		BasicDBObject obj = MongoMgr.getObjectByObjectId(repositoryId, COLLECTION_FILE_QUERY_INFO, cacheFilename);
		CacheEntry ce = getCacheEntryFromObject(obj);
		if (map != null)
			map.put(cacheFilename, ce);
		return ce;
	}	
	
	private CacheEntry getMongoCacheFileEntry(DB db, String cacheFilename){
		BasicDBObject obj = MongoMgr.getObjectByObjectId(db, COLLECTION_FILE_QUERY_INFO, cacheFilename);
		return getCacheEntryFromObject(obj);
	}
	
	private CacheEntry getCacheEntryFromObject(BasicDBObject obj)
	{
		CacheEntry entry  = null;
		if(obj != null){
			entry = new CacheEntry();
			entry.fromBasicDBObject((BasicDBObject)obj.get(MF_CACHE_ENTRY));
		}
		return entry;
	}
	
	/**
	 * Returns the dbo object with the fields exactColumnKeyRefs, inExactColumnKeyRefs, etc, except cacheEntry which is big
	 * @param query
	 */
	private BasicDBObject getMongoCacheFileEntryReferences(DB db, String cacheFilename){
		ArrayList<String> fields = new ArrayList<String>();
		fields.add(MF_CACHE_ENTRY);
		BasicDBObject obj = MongoMgr.getObjectUsingKey(db, COLLECTION_FILE_QUERY_INFO, MF_ID, cacheFilename, fields);
		
		return obj;
	}
	
	private Set<String> getExactColumnKeyRefs(BasicDBObject obj){
		Set<String> exactRefs = null;
		if(obj != null){
			try{
				exactRefs = MongoMgr.convertFromDBListToSet((BasicDBList)obj.get(MF_EXACT_ARR), String.class);
			}catch(Exception e){
				logger.error(e.getMessage(), e);
			}
		}
		if(exactRefs == null){
			exactRefs = new HashSet<String>();
		}
		return exactRefs;
	}
	
	private Set<String> getInExactColumnKeyRefs(BasicDBObject obj){
		Set<String> exactRefs = null;
		if(obj != null){
			try{
				exactRefs = MongoMgr.convertFromDBListToSet((BasicDBList)obj.get(MF_INEXACT_ARR), String.class);
			}catch(Exception e){
				logger.error(e.getMessage(), e);
			}
		}
		if(exactRefs == null){
			exactRefs = new HashSet<String>();
		}
		return exactRefs;
	}
	
	/**
	 * Mongo replacement for cfmap.get()
	 * @param query
	 * @return
	 */
	private String getMongoExactQuery(String query){
		Integer hashKey = getHashCodeIfStringMaxed(query);
		BasicDBObject o = MongoMgr.getObjectByObjectId(repositoryId, COLLECTION_EXACT_QUERY , (hashKey == null) ? query : hashKey);
        if (o != null && hashKey != null)
        {
               String key = o.containsField(MF_KEY) ? o.getString(MF_KEY): null;
               if (!query.equals(key))
                     return null;
        }

		return (o != null) ? o.getString(MF_FILE) : null;
	}
	
	/**
	 * Mongo replacement for cmap.get()
	 * @param key
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private HashSet<String> getMongoInExactQueryCacheFiles(String queryKey){
		BasicDBObject obj = MongoMgr.getObjectByObjectId(repositoryId, COLLECTION_IN_EXACT_QUERY , queryKey);
		HashSet<String> value = null;
		if(obj != null){
			BasicDBList list =  (BasicDBList)obj.get(MF_FILES);
			value = new HashSet<String>();
			value.addAll((ArrayList)list);
		}
		return value;
	}
	
	private void updateMemoryCache(String key, QueryResultSet qrs)
	{
		if (qrs != null && qrs.getRows().length < MAX_CACHE_SIZE)
		{
			mcmap.put(key, qrs);
		}
	}

	@SuppressWarnings("unused")
	private void validateCacheEntry(QueryResultSet qrs, String key) throws CloneNotSupportedException, BaseException
	{
		Query q = qrs.getQuery();
		Query newq = (Query) q.clone();
		String newkey = null;
		try
		{
			newq.createQueryKey(null);
			newkey = newq.getKey(null);
		} catch (NavigationException ex)
		{
			logger.error("Navigation exception in query key validation: " + q);
		}
		// Note, this may be because the session is null
		if (!newkey.equals(key))
		{
			logger.error("Key used to save cache entry does not match that for query result set query: " + q);
		}
		// Now, validate the the query matches the query result set
		try
		{
			List<String[]> colList = QueryResultSet.determineColumns(newq, null);
			for (String[] col : colList)
			{
				if (col[2].equals("D"))
				{
					boolean found = false;
					for (int i = 0; i < qrs.columnNames.length; i++)
					{
						if (col[0].equals(qrs.tableNames[i] + "." + qrs.columnNames[i]))
						{
							found = true;
							break;
						}
					}
					if (!found)
						logger.warn("Error in validating query matches result set - column " + col[0] + " not found in query: " + q);
				} else
				{
					boolean found = false;
					for (int i = 0; i < qrs.columnNames.length; i++)
					{
						if (col[0].equals(qrs.columnNames[i]))
						{
							found = true;
							break;
						}
					}
					if (!found)
						logger.warn("Error in validating query matches result set - column " + col[0] + " not found in query: " + q);
				}
			}
		} catch (NavigationException ex)
		{
			logger.error("Navigation exception in validating query matches result set: " + q);
		}
	}
	
	private QueryResultSet getExpressionOnlyResultSet(Query q)
	{
		QueryResultSet qrs = new QueryResultSet(q);
		return qrs;
	}
	
	/**
	 * Get the resulting cache entry based on a query
	 * 
	 * @param key
	 *            Cache entry key
	 * @param q
	 *            Query to retrieve
	 * @param statementList
	 *            List of statements to use to get query result set (used for cancelling)
	 * @param maxRecords
	 *            Max number of records to retrieve
	 * @param save
	 *            Whether to save the result set in cache
	 * @return
	 * @throws ScriptException 
	 * @throws Exception
	 */
	private QueryResultSet getCacheEntry(String key, Query q, int maxRecords, int maxQueryTime, boolean save,
			Session session) throws SQLException, IOException, BaseException
	{
		QueryResultSet qrs = null;
		q.setResultSetCache(this);
		qrs = new QueryResultSet(q, maxRecords, maxQueryTime, session);
		if(qrs != null && qrs.isValid() && q.getFederatedJoinQueryHelper() != null && q.topn > 0)
		{
			qrs.applyDisplayTop(q.topn);
		}
		if (qrs != null && qrs.isValid())
		{
			if (!q.isCacheable())
				logger.debug("query result set is not cacheable, not being cached");
			if (!save)
				logger.debug("save is false, query result set not being cached");
			boolean isCancelled = session != null && session.isCancelled();
            if (q.isCacheable() && save && !isCancelled)
            {
    			List<QueryFilter> filtersToRemove = new ArrayList<QueryFilter>();            	
        		if (qrs != null && qrs.q != null && qrs.q.containsMultipleQueries && qrs.q.getFilterList() != null)
        		{
        			// Measure filters will be bubbled up later so remove them from query before saving it to cache - look
        			// for word bubble to see where it gets bubbled up later.
        			for (QueryFilter qf: qrs.q.getFilterList())
        			{
        				if (qf.containsMeasure() && !qf.includedInPhysicalQuery)
        				{
        					filtersToRemove.add(qf);
        				}
        			}
        			qrs.q.getFilterList().removeAll(filtersToRemove);

        		}
            	qrs.setExpiration();
                saveCacheEntry(key, qrs, true);
                //Now that we have saved the cache entry, we need to add the removed filters back
                if(filtersToRemove.size() > 0)
                {
                	qrs.q.getFilterList().addAll(filtersToRemove);
                }
                
            } else
			{
				// Make sure that qrs always has a base file name so that the row maps can be created
				// In this case, we are sticking a dummy file name that will never get used
				String tempFile = "dummy-" + Thread.currentThread().getId() + '-' + System.currentTimeMillis();
				qrs.setBaseFilename(tempFile);
			}
		} else
		{
			logger.debug("query result set is NULL or INVALID");
			return (null);
		}
		return (qrs);
	}

	private static class CacheHitResult
	{
		CacheEntry ce;
		List<DisplayFilter> dfilterList;
		List<QueryFilter> replacedFilters;
		List<DisplayOrder> dorderList;
		boolean aggregate;
		boolean inexact;
	}
	
	private CacheHitResult checkFileCache(Repository r, Session session, Query q, boolean compareQueryKeys, List<DisplayOrder> displayOrderList) throws NavigationException,
	CloneNotSupportedException, IOException
	{
		CacheHitResult chr = null;
		long start = System.currentTimeMillis();
		chr = checkFileCacheByMongo(r, session, q, compareQueryKeys, displayOrderList);
		long end = System.currentTimeMillis();
		long diff = end - start;
		if (diff > MAX_CACHE_CHECK_TIME)
			logger.debug("time to access file cache: " + diff + " ms");
		return chr;
	}

	/**
	 * Check the file-based cache to see if there are any matching cache entries
	 * 
	 * @param r
	 *            Repository to use
	 * @param session
	 *            Session to apply (for session variables)
	 * @param q
	 * @return
	 */
	private CacheHitResult checkFileCacheByMongo(Repository r, Session session, Query q, boolean compareQueryKeys, List<DisplayOrder> displayOrderList) throws NavigationException,
			CloneNotSupportedException, IOException
	{
		try
		{
			MongoMgr.getMongo();
		}
		catch(Exception ex)
		{
			logger.error(ex, ex);
			return null;
		}
		
		// for the duration of this method, don't refetch entries from Mongo DB
		Map<String, CacheEntry> cacheEntries = new HashMap<String, CacheEntry>();
		
		try
		{
			// Try the exact match from the file first
			String queryKey = q.getKey(session);
			String cacheEntryFile = getMongoExactQuery(queryKey);
			CacheEntry cacheEntry = (cacheEntryFile != null) ? getMongoCacheFileEntry(cacheEntryFile, cacheEntries) : null;
			if (cacheEntry != null)
			{
				CacheHitResult chr = new CacheHitResult();
				chr.ce = cacheEntry;
				chr.aggregate = false;
				chr.dfilterList = null;
				chr.dorderList = null;
				chr.inexact = false;
				if (!doesCacheFileExist(chr, r, session))
				{
					return null;
				} else
				{
					exactFileHit++;
					return chr;
				}
			}

			// Exact match failed so try the inexact match now.

			// some query types don't work well with the inexact file cache, so filter them out now

			// Can not handle queries with a TOP N
			if (q.hasTop())
				return null;

			// Can not handle queries with an IN tOperator QueryFilter
			// probably could be cleaned up in a way similar to the security filter
			for (QueryFilter qf : q.filters)
			{
				if (qf.isTooComplex())
					return null;
			}

			/*
			 * Get a list of all cache entries that contain the columns in this query
			 */
			Set<String> keylist = q.getKeyList(true);
			Set<String> celist = null;
			for (String key : keylist)
			{
				// TODO we may want to tune this for Mongo to get the whole key list values in one shot
				Set<String> lst = (Set<String>) getMongoInExactQueryCacheFiles(key);
				if (celist == null)
				{
					celist = new HashSet<String>();
					if (lst != null)
						celist.addAll(lst);
				} else if (!celist.isEmpty())
				{
					if (lst != null)
						celist.retainAll(lst);
					else
					{
						celist = null;
						break;
					}
				} else
					break;
			}
			// Compare query keys
			if (compareQueryKeys && celist != null && !celist.isEmpty())
			{
				Set<String> removeList = new HashSet<String>();
				for (Iterator<String> ceit = celist.iterator(); ceit.hasNext();)
				{
					String cename = ceit.next();
					CacheEntry ce = getMongoCacheFileEntry(cename, cacheEntries);;
					if (ce == null)
					{
						removeList.add(cename);
						continue;
					}
					Query ceq = ce.getQuery();
					String ceqKey = ceq.getKey(session);
					if (!ceqKey.equals(queryKey))
					{
						removeList.add(cename);
						continue;
					}
				}
				celist.removeAll(removeList);
			}
			if (celist == null || celist.isEmpty())
				return null;
			/*
			 * Make sure the join semantics are consistent and the session variables match
			 */
			Set<String> removeList = new HashSet<String>();
			for (Iterator<String> ceit = celist.iterator(); ceit.hasNext();)
			{
				String cename = ceit.next();
				CacheEntry ce =  getMongoCacheFileEntry(cename, cacheEntries);
				if (ce == null)
				{
					removeList.add(cename);
					continue;
				}
				Query ceq = ce.getQuery();
				if (ceq.hasTop())
				{
					removeList.add(cename);
					continue;
				}
				boolean coj = ceq.isFullOuterJoin();
				boolean qoj = q.isFullOuterJoin();
				if (!coj && qoj)	// if query is full and cached entry is not, fail
				{
					removeList.add(cename);
					continue;
				}
				Map<String, String> vmap = q.getVariableMap();
				if (vmap != null)
				{
					Map<String,String> ceqVMap = ceq.getVariableMap();
					if (vmap.size() > 0 && ceqVMap != null && ceqVMap.size() > 0 && vmap.size() != ceqVMap.size())
					{
						removeList.add(cename);
						continue;
					}
					for (Iterator<Entry<String, String>> it = vmap.entrySet().iterator(); it.hasNext();)
					{
						Entry<String, String> ve = it.next();
						String val = ceq.getVariableValue(ve.getKey());
						if (val == null || !val.equals(ve.getValue()))
						{
							removeList.add(cename);
							break;
						}
					}
				}
				// Check for QConstructs and look for exact match
				List<String> exactMatchList = q.getExactMatchList();
				List<String> ceExactMatchList = ceq.getExactMatchList();
				if (exactMatchList != null && ceExactMatchList != null)
				{
					if (exactMatchList.size() != ceExactMatchList.size())
					{
						removeList.add(cename);
						continue;
					} else
					{
						for (String qString : exactMatchList)
						{
							if (!ceExactMatchList.contains(qString))
							{
								removeList.add(cename);
								continue;
							}
						}
					}
				}
				// Applicable more for security filter - group exclusions
				// Check for case where the cached entry has variables
				// but the input query does not. Cache Entry should be rejected
				// Similar stuff for QConstructs in case of set based security filter
				// Variable Map - Check
				if (vmap == null || (vmap != null && vmap.isEmpty()))
				{
					Map<String, String> ceqVariableMap = ceq.getVariableMap();
					if (ceqVariableMap != null && !ceqVariableMap.isEmpty())
					{
						// remove this cache entry from consideration
						removeList.add(cename);
						continue;
					}
				}
				// QConstruct - check
				if (exactMatchList == null || (exactMatchList != null && exactMatchList.isEmpty()))
				{
					if (ceExactMatchList != null && !ceExactMatchList.isEmpty())
					{
						// remove this cache entry from consideration
						removeList.add(cename);
						continue;
					}
				}
			}
			celist.removeAll(removeList);
			if (celist == null || celist.isEmpty())
				return null;
			/*
			 * Go through and remove all those that don't have the same list of tables (deals with sparsity issue)
			 */
			try
			{
				// navigate the query to get the tables
				q.setRepository(r);
				q.navigateQuery(false, true, null, true);
				removeList = new HashSet<String>();
				Set<String> qlogicalTables = q.getLogicalTables(); // tables from the query
				for (String cename : celist)
				{
					CacheEntry ce = getMongoCacheFileEntry(cename, cacheEntries);
					if (ce == null)
					{
						removeList.add(cename);
						continue;
					}
					Query ceq = ce.getQuery();
					Set<String> ceqlogicalTables = ceq.getLogicalTables(); // tables from the query in the cache entry
					boolean queryTablesSubsetOfCacheTables = ceqlogicalTables.containsAll(qlogicalTables);
					boolean sameSet = ceqlogicalTables.equals(qlogicalTables);
					boolean ceqContainsInheritedDimensionTable = false;
					if(!sameSet && queryTablesSubsetOfCacheTables)
					{
						//Check for inherited dimension table in ceq that doesn't exist on q. Rolling up from
						//a query containing inherited dimension table will give incorrect results. So, in that
						//case, set the flag ceqContainsInheritedDimensionTable to true.
						for(String ceqlt : ceqlogicalTables)
						{
							if(qlogicalTables.contains(ceqlt))
							{
								//Present in both so skip over
								continue;
							}
							DimensionTable ceqdt = r.findDimensionTable(ceqlt);
							if((ceqdt != null) && (ceqdt.InheritTable != null) && (!ceqdt.InheritTable.isEmpty()))
							{
								ceqContainsInheritedDimensionTable = true;
								break;
							}
						}
					}
					/*
					 *  Decision table for FOJ and single measure queries
					 * CEQ					Q					Result
					 * 0 Measure			0 Measure			Hit Cache
					 * 0 Measure			1 Measure			Not Valid
					 * 1 Measure			0 Measure			Miss Cache (if we hit cache, we will get wrong results due to sparsity)
					 * 1 Measure			1 Measure			Hit Cache
					 * 0 Measure			FOJ					Not Valid
					 * FOJ					0 Measure			Miss Cache
					 * 1 Measure			FOJ					Not Valid
					 * FOJ					1 Measure			Miss Cache (if we hit cache, we will get wrong results due to sparsity)
					 * FOJ					FOJ					Hit Cache
					 * In a nut shell, hit cache if either both have FOJ or both have only 1 measure or both are attribute only queries.
					 */
					boolean isQFullOuter = q.isFullOuterJoin(); 
					boolean isQSingleMeasure = (q.measureColumns.size() == 1);
					boolean isQNoMeasure = (q.measureColumns.size() == 0);
					
					boolean isceQFullOuter = ceq.isFullOuterJoin();
					boolean isceQSingleMeasure = (ceq.measureColumns.size() == 1);
					boolean isceQNoMeasure = (ceq.measureColumns.size() == 0);
					
					boolean acceptableMatch = (queryTablesSubsetOfCacheTables && (!ceqContainsInheritedDimensionTable) && 
							((isQFullOuter && isceQFullOuter) || (isQSingleMeasure && isceQSingleMeasure) || (isQNoMeasure && isceQNoMeasure))) 
							|| sameSet;
					// inexact match requires that all of the logical tables be the same or that in the case of a subset, the cached query be a full outer join
					if (!acceptableMatch)
						removeList.add(cename);
				}
			} 
			catch (NavigationException nex)
			{
				// query does not navigate, this should not happen
				throw nex;
			}
			celist.removeAll(removeList);
			if (celist == null || celist.isEmpty())
				return null;

			/*
			 * Go through and pick the lowest cardinality result that matches grouping and filtering
			 */
			List<QueryFilter> flist = q.getExpandedFilterList();
			List<GroupBy> gblist = new ArrayList<GroupBy>(q.getGroupByClauses());
			CacheHitResult chr = null;

			for (String cename : celist)
			{
				boolean match = true;
				CacheEntry ce =  getMongoCacheFileEntry(cename, cacheEntries);
				if (ce == null)
					continue;
				Query ceq = ce.getQuery();
				/*
				 * First, ensure that filters match (all of the filters in the resultset are in this query as
				 * well). If there are filters in this query that aren't present in the query result set, then
				 * remember the display filters necessary to apply them
				 */
				/*
				 * also, due to how we post filter cached result sets, if the query has an OR/AND filter that contain
				 * a mix of measures and attributes, we can't match the cache
				 * - we do dimension filters pre-aggregation and measure filters post-aggregation, painful to do if they are mixed
				 */
				List<QueryFilter> ceflist = ceq.getExpandedFilterList();
				List<DisplayFilter> cedfilterList = new ArrayList<DisplayFilter>();
				List<QueryFilter> qfilterList = new ArrayList<QueryFilter>();
				if (q.containsMixedQueryFilters())
					match = false;
				else if (!flist.containsAll(ceflist))
					match = false;
				else
				{
					/*
					 * Make sure that if there are measure filters, then the grouping is at the same grain -
					 * otherwise, measure filters aren't valid
					 */
					boolean containsMeasure = false;
					for (QueryFilter qf : ceflist)
					{
						if (qf.containsMeasure())
						{
							containsMeasure = true;
							break;
						}
					}
					if (containsMeasure)
					{
						List<GroupBy> glist = q.getGroupByClauses();
						List<GroupBy> ceglist = ceq.getGroupByClauses();
						if (glist.size() == ceglist.size() && ceglist.containsAll(glist))
							match = true;
						else
							match = false;
					}
					List<Object> suppliedColumns = null;
					if (match)
					{
						/*
						 * Find the filters not in the filter list of this query result set
						 */
						for (QueryFilter qf : flist)
						{
							if (!ceflist.contains(qf))
							{
								/*
								 * Make sure this query filter can be satisfied by this result set
								 */
								if (suppliedColumns == null)
									suppliedColumns = ceq.getSuppliedQueryColumns();
								if (!qf.isSatisfiedBy(suppliedColumns))
								{
									match = false;
									break;
								}
								DisplayFilter df = qf.returnDisplayFilter(r, session);
								if (df != null && df.isValid())
								{
									cedfilterList.add(df);
									qfilterList.add(qf);
								} else
								{
									match = false;
									break;
								}
							}
						}
					}
				}
				boolean ceagg = false;
				if (match)
				{
					/*
					 * Now determine if aggregation on cache is necessary. Make sure that all group by's are in
					 * the query result set. If there are fewer group by's, then this result is more aggregated
					 * and mark the result for aggregation (make sure all measures are additive)
					 */
					List<GroupBy> cegblist = ceq.getGroupByClauses();
					if (!cegblist.containsAll(gblist))
						match = false;
					else if (cegblist.size() > gblist.size())
					{
						/*
						 * Check to see if the query has nogroupby clause i.e. has dimension columns but group
						 * by list is empty
						 */
						if (gblist.isEmpty() && !q.dimensionColumns.isEmpty())
						{
							match = false;
						} else
						{
							/*
							 * Navigate the query if needed to determine whether it requires a level-specific join. If
							 * so, the measures must be assumed to be non-additive so one cannot aggregate another
							 * result set to get it
							 * 
							 * There was a flag in here to only match when the filters also exactly match, this was
							 * added in Bugzilla 11119 by rchandarana. But we must be able to support more filters being
							 * added so we can do aggregation and bursting. Not sure what the bug was, but we must allow
							 * filters to be different.
							 */
							if (!q.usesLevelSpecificJoin() && !ceq.usesLevelSpecificJoin() && ceq.isAdditive(gblist) && q.isAdditive(null))
								ceagg = true;
							else
								match = false;
						}
					}
				}
				if (match)
				{
					if (chr == null)
						chr = new CacheHitResult();
					if (chr.ce == null)
					{
						chr.ce = ce;
						chr.dfilterList = cedfilterList;
						chr.replacedFilters = qfilterList;
						chr.aggregate = ceagg;
					} else
					{
						if (ce.getNumRows() < chr.ce.getNumRows()) // use the smaller result set
						{
							chr.ce = ce;
							chr.dfilterList = cedfilterList;
							chr.replacedFilters = qfilterList;
							chr.aggregate = ceagg;
						}
					}
				}
			}

			// we have now chosen a file cache entry to use
			if (chr == null || chr.ce == null)
				return null;
			
			/*
			 * Determine if additional sorting is necessary
			 */
			List<OrderBy> oblist = q.getOrderByClauses();
			if ((oblist != null && !oblist.isEmpty()) || chr.aggregate)
			{
				List<OrderBy> ceoblist = chr.ce.getQuery().getOrderByClauses();
				boolean obmatch = true;
				if (oblist.size() != ceoblist.size() || chr.aggregate)
					obmatch = false;
				else
				{
					for (int i = 0; i < oblist.size(); i++)
					{
						if (!oblist.get(i).equals(ceoblist.get(i)))
						{
							obmatch = false;
							break;
						}
					}
				}
				if (!obmatch)
				{
					if(displayOrderList == null || displayOrderList.isEmpty() || oblist.size() == 0)
					{
						/* Convert order by's into display orders */
						chr.dorderList = new ArrayList<DisplayOrder>();
						for (OrderBy ob : oblist)
						{
							chr.dorderList.add(ob.returnDisplayOrder());
						}
					}
					/**
					 * If there are other display orders in the query that is trying to hit this file cache, the
					 * semantics of ordering will change giving us incorrect results. e.g. order by A, display by B will
					 * ignore the ordering on A and sort the resultset on B. However, display by A, display by B first
					 * sorts on A and within same values for column A, sorts on B. In a nutshell, we need to miss the
					 * cache if the query that is requesting to hit inexact match has display order and the order bys on
					 * cache vs. query don't match exactly.
					 */
					else
					{
						chr.ce = null;
					}
				}
			}
			if (chr.ce == null || !doesCacheFileExist(chr, r, session))
			{
				return null;
			} 
			else
			{
				chr.inexact = true;
				inexactFileHit++;
				return chr;
			}
		} catch (NavigationException | BadColumnNameException ex)
		{
			// do nothing
		} catch (Exception ex)
		{
			logger.debug(ex, ex);
		}
		finally
		{
			cacheEntries.clear(); // clear out the temporary cache entry hash map
		}
		return null;
	}

	private boolean doesCacheFileExist(CacheHitResult chr, Repository r, Session session)
	{
		File cacheFile = chr.ce.getCacheFile(getSharedDirectory(repositoryId));
		boolean exists = cacheFile.exists();
		if (!exists)
		{
			logger.warn("Cache file "
					+ cacheFile.getAbsolutePath()
					+ " does not exist."
					+ "This is either due to a potential defect in the system or manual deletion of the file. Physical query to database will be used to create result set.");
			try
			{
				removeCacheFileEntry(cacheFile.getName(), true);
			} catch (Exception e)
			{
				logger.error(e, e);
			}
		}
		return exists;
	}

	/**
	 * Updates the cache with the given query and query result set. - currently only called by CacheGenerator.java
	 * 
	 * @param q
	 * @param qrs
	 * @throws IOException
	 * @throws NavigationException
	 * @throws SessionVariableUnavailableException
	 */
	public void updateCache(Query q, QueryResultSet qrs) throws IOException, BaseException, CloneNotSupportedException
	{
		String key = q.getKey(null);
		saveCacheEntry(key, qrs, true);
	}

	/**
	 * For a given logical table or physical table, this method scans thru the file-based cache to see if any of the
	 * queries are using the table. If so, it clears the cache entry both from file cache and memory cache.
	 * 
	 * @param tableName
	 * @param r
	 * @param session
	 * @param isLogicalTable
	 * @param regenerateCacheEntry
	 * @return
	 * @throws Exception
	 */		
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public synchronized boolean removeCacheForTable(String tableName, Repository r, Session session, boolean isLogicalTable, boolean regenerateCacheEntry)
	throws Exception
	{
		boolean foundMatch = false;
		long startTime = System.currentTimeMillis();
		validateTableName(tableName, r, isLogicalTable);
		Map<String, Set<CacheEntry>> entriesToBeRemovedByKey = new HashMap<String, Set<CacheEntry>>();
		
		//Query the inexact cache to match
		DB db = MongoMgr.getSpaceDb(repositoryId);
		db.requestStart();
		DBCollection coll = db.getCollection(COLLECTION_IN_EXACT_QUERY);
		DBCursor cursor = coll.find();
		while(cursor.hasNext()){
			BasicDBObject entry = (BasicDBObject)cursor.next();
			BasicDBList list =  (BasicDBList)entry.get(MF_FILES);
			HashSet<String> values = new HashSet<String>();
			values.addAll((ArrayList)list);
			
			Set<CacheEntry> entriesToBeRemoved = new HashSet<CacheEntry>();
			for (String ceName : values)
			{
				CacheEntry ce = (CacheEntry)  getMongoCacheFileEntry(db, ceName);
				boolean usesTableName = cacheEntryUsesTableName(isLogicalTable, ce, tableName);
				if (usesTableName)
				{
					entriesToBeRemoved.add(ce);
				}
				if (entriesToBeRemoved != null && !entriesToBeRemoved.isEmpty())
				{
					entriesToBeRemovedByKey.put(entry.getString(MF_ID), entriesToBeRemoved);
				}
			}
		}
		db.requestDone();
		
		List<Query> removedQueryList = new ArrayList<Query>();
		Iterator<String> keys = entriesToBeRemovedByKey.keySet().iterator();
		while (keys.hasNext())
		{
			String key = keys.next();
			Set<CacheEntry> entriesToBeRemoved = entriesToBeRemovedByKey.get(key);
			if (entriesToBeRemoved != null && !entriesToBeRemoved.isEmpty())
			{
				foundMatch = true;
				for (CacheEntry ce : entriesToBeRemoved)
				{
					String cacheFilename = ce.getFileName();
					Set<Query> queryList = removeCacheFileEntry(cacheFilename, false);
					removedQueryList.addAll(queryList);
				}
			}
		}
		
		// Now remove all the corresponding cache entries from memory cache
		for (Query q : removedQueryList)
		{
			String key = q.getKey(session);
			removeKeyFromMemoryAndFileCacheMap(key);
		}
		
		long endTime = System.currentTimeMillis();
		if (foundMatch)
		{
			logger.debug("Time to remove " + (regenerateCacheEntry ? " and regenerate " : "") + "cached queries for table " + tableName + ": "
					+ (endTime - startTime) + " milliseconds");
		}
		return foundMatch;
	}

	private boolean cacheEntryUsesTableName(boolean isLogicalTable, CacheEntry ce, String tableName)
	{
		boolean usesTableName = false;
		Query q = ce.getQuery();
		if (isLogicalTable)
		{
			usesTableName = q.usesLogicalTable(tableName);
		} else
		{
			usesTableName = q.usesPhysicalTable(tableName);
		}
		return usesTableName;
	}

	private synchronized void removeKeyFromMemoryAndFileCacheMap(String key) throws IOException
	{
		if (mcmap.containsKey(key))
		{
			mcmap.remove(key);
			logger.debug("Removed from memory cache:" + key);
		}
		removeKeyFromFileCacheMap(key);
	}
	
	private void removeKeyFromFileCacheMap(String key) throws IOException{
		String cacheFilename = getMongoExactQuery(key);
		if(cacheFilename != null){
			DB db = MongoMgr.getSpaceDb(repositoryId);
			
			//Use connection pool, marker to ensure all requests go through the same connection
			db.requestStart();
			
			//Remove from exact query cache
			DBCollection exactCol = db.getCollection(COLLECTION_EXACT_QUERY);
			BasicDBObject criteria = createBasicDBObjectWithKey(key);
			exactCol.remove(criteria);
			CommandResult rs = db.getLastError();
			
			if(!MongoMgr.hasError(rs)){
				DBCollection entryColl = db.getCollection(COLLECTION_FILE_QUERY_INFO);
				
				//Now remove the reference from the cache entry map's exact column key reference
				BasicDBObject fileEntry = new BasicDBObject(MF_ID, cacheFilename);
				BasicDBObject pullCmd = new BasicDBObject("$pull", new BasicDBObject(MF_EXACT_ARR, key));
				entryColl.update(fileEntry, pullCmd);
				
				rs = db.getLastError();
				if(MongoMgr.hasError(rs)){
					logger.error("Unable to delete cache file entry's exact reference: " + cacheFilename, rs.getException());
				}
				
				//Check if references (exact, inexact) still there. If no longer, we remove this entry and file.
				BasicDBObject obj =	getMongoCacheFileEntryReferences(db, cacheFilename);
				Set<String> exactRefs = this.getExactColumnKeyRefs(obj);
				Set<String> inExactRefs = this.getInExactColumnKeyRefs(obj);
				
				//Try to delete the file if there is no longer any references
				if(exactRefs.isEmpty() && inExactRefs.isEmpty()){
					deleteCacheEntryAndPhysicalFile(db, entryColl, cacheFilename);
				}
			}else{
				logger.error("Unable to delete key : " + key + " from exact cache collection", rs.getException());
			}
		
			db.requestDone();
		}
	}
	
	/**
	 * Iterates thru all dimensions and measures to find the given logical or physical table name. Throws an exception
	 * if table name is not found in repository.
	 * 
	 * @param tableName
	 *            Logical or Physical table name
	 * @param r
	 * @param isLogicalTable
	 *            Flag to indicate if it is a logical or a physical table.
	 * @throws Exception
	 */
	private void validateTableName(String tableName, Repository r, boolean isLogicalTable) throws Exception
	{
		boolean found = false;
		if (isLogicalTable)
		{
			if (r.DimensionTables != null)
			{
				for (DimensionTable dt : r.DimensionTables)
				{
					if (tableName.equals(dt.TableName))
					{
						found = true;
						break;
					}
				}
			}
			if ((r.MeasureTables != null) && !found)
			{
				for (MeasureTable mt : r.MeasureTables)
				{
					if (tableName.equals(mt.TableName))
					{
						found = true;
						break;
					}
				}
			}
		} else
		{
			if (r.DimensionTables != null)
			{
				for (DimensionTable dt : r.DimensionTables)
				{
					TableSource ts = dt.TableSource;
					if (ts.Tables != null)
					{
						for (TableDefinition td : ts.Tables)
						{
							if (tableName.equals(td.PhysicalName))
							{
								found = true;
								break;
							}
						}
					}
				}
			}
			if ((r.MeasureTables != null) && !found)
			{
				for (MeasureTable mt : r.MeasureTables)
				{
					TableSource ts = mt.TableSource;
					if (ts.Tables != null)
					{
						for (TableDefinition td : ts.Tables)
						{
							if (tableName.equals(td.PhysicalName))
							{
								found = true;
								break;
							}
						}
					}
				}
			}
		}
		if (!found)
		{
			throw new Exception((isLogicalTable ? "Logical " : "Physical ") + "table name " + tableName + " not found in repository.");
		}
	}

	/**
	 * Removes the file and memory cache for a given logical query. For memory cache, it removes the entry that has an
	 * exact match with the query key for the given logical query. For file cache, it removes all file cache entries
	 * that can potentially contribute to serving the query.
	 * 
	 * @param queryString
	 * @param r
	 * @param session
	 * @param reseed
	 *            - Option to reseed the given query.
	 * @throws Exception
	 */
	public synchronized boolean removeCacheForQuery(String queryString, Repository r, Session session, boolean reseed, boolean removeOnlyIfQueryKeysMatch)
			throws Exception
	{
		if (queryString == null)
			return false;
		boolean foundMatch = false;
		long startTime = System.currentTimeMillis();
		queryString = QueryString.preProcessQueryString(queryString, r, session);
		AbstractQueryString qs = null;
		qs = AbstractQueryString.getInstance(r, queryString);
		Query q = qs.getQuery();

		foundMatch = removeCacheForQuery(q, r, session, removeOnlyIfQueryKeysMatch);
		if(!foundMatch)
		{
			if (q.filters != null && q.filters.size() == 1)
			{
				QueryFilter qf = q.filters.get(0);
				if (qf.getType() == QueryFilter.TYPE_LOGICAL_OP && qf.getOperator().equals("AND"))
				{
					q.filters.remove(qf);
					for (QueryFilter filter : qf.getFilterList())
					{
						q.addFilter(filter);
					}
				}
				q.clearKey();
				foundMatch = removeCacheForQuery(q, r, session, removeOnlyIfQueryKeysMatch);				
			}
		}
		
		List<DisplayOrder> volatileSorts = null;
		Query vq = null;
		List<DisplayFilter> displayFilters = qs.getDisplayFilters();
		if (!q.isVolatileCacheQuery())
		{
			if (displayFilters == null)
				displayFilters = new ArrayList<DisplayFilter>();
			volatileSorts = new ArrayList<DisplayOrder>();
			vq = extractVolatileCacheQuery(q, displayFilters, volatileSorts, session);
		}
		if (vq != null)
		{
			/*
			 * Add a measure to the volatile query if necessary
			 */
			if (vq.addVolatileMeasure())
			{
				addVolatileMeasure(q, vq);
			}
		}
		List<DisplayExpression> expressionList = qs.getExpressionList();
		List<String> addedNames = new ArrayList<String>();
		List<Query> mecQueryList = null;
		if (expressionList != null)
		{
			mecQueryList = new ArrayList<Query>();
			for (DisplayExpression de : expressionList)
			{
				de.extractAllExpressions(q, addedNames);
				List<MeasureExpressionColumn> mecList = de.getMeasureExpressionColumns();
				if (mecList != null && !mecList.isEmpty())
				{
					for (MeasureExpressionColumn mec : mecList)
					{
						Query mecq = mec.getQuery();
						Query mecbq = mec.getBridgeQuery();
						if (mecq != null && !mecQueryList.contains(mecq))
						{
							mecQueryList.add(mecq);
						}
						if (mecbq != null && !mecQueryList.contains(mecbq))
						{
							mecQueryList.add(mecbq);
						}
					}
				}
			}
		}
		if (!addedNames.isEmpty())
		{
			q.clearKey();
		}
		if (vq != null)
		{
			foundMatch = removeCacheForQuery(vq, r, session, true) || foundMatch;
		}
		if (mecQueryList != null && !mecQueryList.isEmpty())
		{
			for (Query mecq : mecQueryList)
			{
				foundMatch = removeCacheForQuery(mecq, r, session, removeOnlyIfQueryKeysMatch) || foundMatch;
			}
		}
		if (reseed)
		{
			JasperDataSourceProvider jdsp = new JasperDataSourceProvider(r, this);
			jdsp.setSession(session);
			jdsp.create(queryString);
		}
		long endTime = System.currentTimeMillis();
		if (foundMatch)
		{
			logger.debug("Time to remove " + (reseed ? " and reseed " : "") + "query cache entry: " + (endTime - startTime) + " milliseconds");
		}
		return foundMatch;
	}

	public boolean removeCacheForQuery(Query q, Repository r, Session session, boolean removeOnlyIfQueryKeysMatch) throws Exception
	{
		boolean foundMatch = false;
		// key is a regular expression, need to match all cache entries that are like it
		String queryKey = q.getClearCacheKey(session);
		logger.debug("Cache Clear Key: " + queryKey);
		List<String> toBeRemoved = new ArrayList<String>(); // list of keys to remove
		Pattern pattern = Pattern.compile(queryKey);
		// check the memory cache
		synchronized (mcmap)
		{
			for (String key : mcmap.keySet())
			{
				boolean match = pattern.matcher(key).find();
				if (match)
				{
					toBeRemoved.add(key);
					foundMatch = true;
					logger.debug("Matched memory key: " + key);
				}
			}
		}
		// remove matched entries from the memory cache
		for (String key : toBeRemoved)
		{
			mcmap.remove(key);
		}
		toBeRemoved.clear();
		
		// check the file cache

		DB db = MongoMgr.getSpaceDb(repositoryId);
		db.requestStart();

		DBCollection exactColl = db.getCollection(COLLECTION_EXACT_QUERY);
		DBCursor cursor = exactColl.find();

		while(cursor.hasNext()){
			BasicDBObject obj = (BasicDBObject) cursor.next();
			String key = obj.containsField(MF_KEY) ? obj.getString(MF_KEY) : obj.getString(MF_ID);
			String fileName = obj.getString(MF_FILE);
			boolean match = pattern.matcher(key).find();
			logger.debug("Checking key [" + key + "] for entry [" + fileName + "] against pattern [" + queryKey + "]: " + match);
			if (match)
			{
				toBeRemoved.add(key);
				foundMatch = true;
				logger.debug("Matched file key: " + key);
			}
		}

		db.requestDone();

		// remove matched entries from the file cache
		List<CacheEntry> entries = new ArrayList<CacheEntry>();
		for (String key : toBeRemoved)
		{
			String fname = getMongoExactQuery(key);
			CacheEntry ce = getMongoCacheFileEntry(fname, null);
			entries.add(ce);
			removeKeyFromFileCacheMap(key);
			foundMatch = true;
		}

		toBeRemoved.clear();
		CacheHitResult chr = null;
		boolean firstT = true;
		while (firstT || chr != null)
		{
			if (firstT)
			{
				firstT = false;
			}
			chr = checkFileCache(r, session, q, removeOnlyIfQueryKeysMatch, null);
			if (chr != null && chr.ce != null)
			{
				foundMatch = true;
				String queryKey1 = chr.ce.getQuery().getKey(session);
				removeKeyFromMemoryAndFileCacheMap(queryKey1);
				String cacheFilename = chr.ce.getFileName();
				removeCacheFileEntry(cacheFilename, false);
			}
		}
		for (CacheEntry ce : entries)
		{
			Query q1 = ce.getQuery();
			q1.setRepository(r);
			firstT = true;
			while (firstT || chr != null)
			{
				if (firstT)
				{
					firstT = false;
				}
				chr = checkFileCache(r, session, q1, removeOnlyIfQueryKeysMatch, null);
				if (chr != null && chr.ce != null)
				{
					foundMatch = true;
					String queryKey1 = chr.ce.getQuery().getKey(session);
					removeKeyFromMemoryAndFileCacheMap(queryKey1);
					String cacheFilename = chr.ce.getFileName();
					removeCacheFileEntry(cacheFilename, false);
				}
			}
		}

		return foundMatch;
	}
	
	/**
	 * Removes the file cache entry for a given cache file name.
	 * 
	 * @param cacheFilename
	 * @param r
	 * @param session
	 * @return List of query objects removed
	 */
	public Set<Query> removeCacheFileEntry(String cacheFilename, boolean nonExistentFile) throws Exception
	{
		
		DB db = MongoMgr.getSpaceDb(repositoryId);

		//Use connection pool, marker to ensure all requests go through the same connection
		db.requestStart();

		DBCollection entryColl = db.getCollection(COLLECTION_FILE_QUERY_INFO);

		//Get the references
		BasicDBObject obj = MongoMgr.getObjectByObjectId(db, COLLECTION_FILE_QUERY_INFO, cacheFilename);
		CacheEntry cacheEntry = getCacheEntryFromObject(obj);
		Set<String> exactRefs = getExactColumnKeyRefs(obj);
		Set<String> inExactRefs = getInExactColumnKeyRefs(obj);

		CommandResult rs = db.getLastError();
		if(MongoMgr.hasError(rs)){
			logger.error("Unable to retrieve query key file references for " + cacheFilename, rs.getException());
			return null;
		}

		if (!inExactRefs.isEmpty()) {
			if(removeEntriesFromInExactCache(cacheFilename, db, inExactRefs)){
				//Remove inexact references in our entry
				BasicDBObject entry = new  BasicDBObject(MF_ID, cacheFilename);
				BasicDBObject pullAll = new BasicDBObject("$pullAll", new BasicDBObject(MF_INEXACT_ARR, inExactRefs.toArray()));
				entryColl.update(entry, pullAll);
				if(MongoMgr.hasError(rs)){
					logger.error("Unable to remove inexact references in for FileQueryInfo cache: " + cacheFilename, rs.getException());
					return null;
				}else{
					inExactRefs.clear();
				}
			}
		}
		
		//In this case, we remove the exact cache key match so we can delete everything else
		if(nonExistentFile){
			//Remove from exact query cache
			if(removeEntriesFromExactCache(cacheFilename, db, exactRefs)){
				//Remove exact references in our entry
				BasicDBObject entry = new  BasicDBObject(MF_ID, cacheFilename);
				BasicDBObject pullAll = new BasicDBObject("$pullAll", new BasicDBObject(MF_EXACT_ARR, exactRefs.toArray()));
				entryColl.update(entry, pullAll);
				if(MongoMgr.hasError(rs)){
					logger.error("Unable to remove exact references in for FileQueryInfo cache: " + cacheFilename, rs.getException());
					return null;
				}else{
					exactRefs.clear();
				}
			}
		}

		//Check if references (exact, inexact) still there. If no longer, we remove this entry and file.
		if(exactRefs.isEmpty() && inExactRefs.isEmpty()){
			deleteCacheEntryAndPhysicalFile(db, entryColl, cacheFilename);
		}
		
		db.requestDone();
		
		//Yup, looks like we only return one query.
		HashSet<Query> q = new HashSet<Query>();
		if(cacheEntry != null){
			q.add(cacheEntry.getQuery());
		}
		
		return q;
	}
	
	private void deleteCacheEntryAndPhysicalFile(DB db, DBCollection entryColl, String cacheFilename) throws IOException
	{
		BasicDBObject fileEntry = new BasicDBObject(MF_ID, cacheFilename);
		//delete entry, no more references
		//remove entry only if the size of the references are 0, just in case
		fileEntry.append(MF_EXACT_ARR, new BasicDBObject("$size", 0));
		fileEntry.append(MF_INEXACT_ARR, new BasicDBObject("$size", 0));
		fileEntry.append("$atomic", true);
		entryColl.remove(fileEntry);

		CommandResult rs = db.getLastError();
		if(MongoMgr.hasError(rs)){
			logger.error("Unable to delete cache file entry: " + cacheFilename, rs.getException());
		}else{
			int numRowDeleted = rs.getInt("n");
			if(numRowDeleted == 1){ //Entry deleted from mongodb
				//Remove physical file
				deleteFile(cacheFilename);
				logger.debug("Removed from cache file map:" + cacheFilename);
			}
		}
	}
	
	private boolean removeEntriesFromInExactCache(String cacheFilename, DB db, Set<String> inExactRefs)
	{
		Iterator<String> itr = inExactRefs.iterator();
		CommandResult rs = null;
		DBCollection inexactColl = db.getCollection(COLLECTION_IN_EXACT_QUERY);
		while(itr.hasNext()){
			String queryKey = itr.next();
			
			//Delete inexact queryKey from the inexact cache's file list
			BasicDBObject objKey = new BasicDBObject(MF_ID, queryKey);
			BasicDBObject pullCmd = new BasicDBObject("$pull", new BasicDBObject(MF_FILES, cacheFilename));
			inexactColl.update(objKey, pullCmd);
			rs = db.getLastError();
			if(MongoMgr.hasError(rs)){
				//Should we stop on exception?
				logger.error("Unable to remove key file reference of inexact cache for key " + queryKey + " to file name " + cacheFilename, rs.getException());	
				return false;
			}else{
				//Remove the cache entry if it has zero array of file names after deletion
				BasicDBObject key = new BasicDBObject(MF_ID, queryKey);
				key.append(MF_FILES, new BasicDBObject("$size", 0));
				key.append("$atomic", true);
				inexactColl.remove(key);
				rs = db.getLastError();
				if(MongoMgr.hasError(rs)){
					logger.error("Unable to attempt removal of inexact cache key if it was empty: " + queryKey, rs.getException());
					return false;
				}else{
					int numUpdated = rs.getInt("n");
					if(numUpdated == 1){
						logger.debug("Removed inexact cache key completely from cache: " + queryKey);
					}
				}
			}
		}
		return true;
	}
	
	private boolean removeEntriesFromExactCache(String cacheFilename, DB db, Set<String> inExactRefs)
	{
		Iterator<String> itr = inExactRefs.iterator();
		CommandResult rs = null;
		
		//Remove from exact query cache
		DBCollection exactCol = db.getCollection(COLLECTION_EXACT_QUERY);
		
		while(itr.hasNext()){
			String queryKey = itr.next();
			
			//Delete queryKey from exact cache's entry
			BasicDBObject criteria = createBasicDBObjectWithKey(queryKey);
			exactCol.remove(criteria);
			rs = db.getLastError();
			
			if(MongoMgr.hasError(rs)){
				logger.error("Unable to remove key file reference of exact cache for key " + queryKey + " to file name " + cacheFilename, rs.getException());	
				return false;
			}	
		}
		
		return true;
	}
	
	private void deleteFile(String filename) throws IOException
	{
		File cacheFile = new File(getSharedDirectory(repositoryId) + File.separator + filename); 
		if (cacheFile.exists())
		{
			if (Util.secureDelete(cacheFile))
			{
				logger.trace("Deleted file: " + cacheFile.getName());
			}
			else
			{
				logger.warn("Unable to delete file: " + cacheFile.getName());
			}
		}
	}

	/**
	 * Split a query result set based on a given dimension column. Save as many result sets (with appropriately added
	 * filters)
	 * 
	 * @param qrs
	 * @param dimension
	 * @param column
	 * @throws SyntaxErrorException
	 * @throws NavigationException
	 * @throws IOException
	 * @throws SessionVariableUnavailableException
	 */
	public void cacheSplitResultSet(Session session, QueryResultSet qrs, String dimension, String column) throws BaseException,
			IOException, CloneNotSupportedException
	{
		Map<String, QueryResultSet> splitResults = qrs.returnSplitResults(session, dimension, column);
		for (Map.Entry<String, QueryResultSet> entry : splitResults.entrySet())
		{
			String key = entry.getValue().getQuery().getKey(session);
			saveCacheEntry(key, entry.getValue(), false);
		}
	}

	/**
	 * create a cache file
	 */
	public static File createCacheFile(String prefix, String cacheDir) throws IOException
	{
		// create the cache directory
		Util.createDirectory(cacheDir);
		if(filePrefix == null){
			filePrefix = Util.getHostName() + "-" + Util.getProcessID() + "-";
		}
		prefix = filePrefix;
		return File.createTempFile(prefix, ".cache", new File(cacheDir));
	}

	/**
	 * tell the cache that new data is being published to the database - used to stop access to the database
	 * 
	 * @return
	 */
	private boolean isPublishingOrHasPublished()
	{
		if (publishLock != null)
			return publishLock.exists();
		return false;
	}

	public boolean isImportedSpacePublishingOrHasPublished() {
		return importedSpacePublishingOrHasPublished;
	}

	public void setImportedSpacePublishingOrHasPublished(boolean isPublishing) {
		this.importedSpacePublishingOrHasPublished = isPublishing;
	}
}