/**
 * $Id: DisplayMap.java,v 1.9 2010-12-20 12:48:06 mpandit Exp $
 *
 * Copyright (C) 2007-2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Arrays;

import org.jdom.Element;
import org.jdom.Namespace;
import com.successmetricsinc.Repository;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author Brad Peters
 * 
 */
public class DisplayMap implements Serializable
{
	private static final long serialVersionUID = 1L;
	public enum MapType
	{
		Map, Bucket, Query, Lookup
	}
	public MapType Type;
	public String[] Keys;
	public String[] MappedValues;
	public double[][] Buckets;
	private boolean isBucketsTag;
	public String Query;
	public String LookupTable;
	public String LookupExpression;
	public String LookupJoinColumn;
	public String LookupFilterExpression;
	public boolean LookupOuterJoin;
	public String Default;
	public QueryResultSet mapResultSet;
	public String DataType;

	public DisplayMap(Element e, Namespace ns)
	{
		int x = Integer.valueOf(e.getChildTextTrim("Type", ns));
		Type = x == 0 ? MapType.Map : x == 1 ? MapType.Bucket : x == 2 ? MapType.Query : MapType.Lookup;
		Keys = Repository.getStringArrayChildren(e, "Keys", ns);
		MappedValues = Repository.getStringArrayChildren(e, "MappedValues", ns);
		Buckets = Repository.getDoubleDoubleArrayChildren(e, "Buckets", ns);
		isBucketsTag = e.getChild("Buckets",ns)!=null?true:false;
		Query = e.getChildText("Query", ns);
		LookupTable = e.getChildText("LookupTable", ns);
		LookupExpression = e.getChildText("LookupExpression", ns);
		LookupJoinColumn = e.getChildText("LookupJoinColumn", ns);
		LookupFilterExpression = e.getChildText("LookupFilterExpression", ns);
		String s = e.getChildText("LookupOuterJoin", ns);
		if (s != null)
			LookupOuterJoin = Boolean.valueOf(s);
		Default = e.getChildText("Default", ns);
		DataType = e.getChildText("DataType", ns);
		if (DataType == null)
			DataType = "Varchar";
	}

	public String toString()
	{
		if (LookupTable != null)
			return (LookupTable);
		if (Query != null)
			return (Query);
		if (MappedValues != null)
			return (Arrays.toString(MappedValues));
		return (null);
	}
	
	public Element getDisplayMapElement(Namespace ns)
	{
		Element e = new Element("DisplayMap", ns);
		Element child = new Element("Type", ns);
		String type;
		switch(Type)
		{
			case Map: type = "0"; break;
			case Bucket: type = "1"; break;
			case Query: type = "2"; break;
			default : type = "3"; 
		}
		child.setText(type);
		e.addContent(child);
		
		e.addContent(Repository.getStringArrayElement("Keys", Keys, ns));
		
		e.addContent(Repository.getStringArrayElement("MappedValues", MappedValues, ns));
				
		if (Buckets!=null && Buckets.length>0)
		{
			child = new Element("Buckets",ns);
			for (double[] lvl1 : Buckets)
			{
				Element arrayOfDouble = new Element("ArrayOfDouble",ns);
				for (double lvl2 : lvl1)
				{
					Element dblElement = new Element("double",ns);
					dblElement.setText(Util.getDecimalFormatForRepository().format(lvl2));
					arrayOfDouble.addContent(dblElement);
				}
				child.addContent(arrayOfDouble);
			}
			e.addContent(child);
		}		
		else if (isBucketsTag)
		{
			child = new Element("Buckets",ns);
			e.addContent(child);
		}
		
		if (Query!=null)
		{
			child = new Element("Query",ns);
			child.setText(Query);
			e.addContent(child);
		}
		
		if (LookupTable!=null)
		{
			child = new Element("LookupTable",ns);
			child.setText(LookupTable);
			e.addContent(child);
		}
		
		if (LookupExpression!=null)
		{
			child = new Element("LookupExpression",ns);
			child.setText(LookupExpression);
			e.addContent(child);
		}
		
		if (LookupJoinColumn!=null)
		{
			child = new Element("LookupJoinColumn",ns);
			child.setText(LookupJoinColumn);
			e.addContent(child);
		}
		
		if (LookupFilterExpression!=null)
		{
			child = new Element("LookupFilterExpression",ns);
			child.setText(LookupFilterExpression);
			e.addContent(child);
		}
		
		child = new Element("LookupOuterJoin",ns);
		child.setText(String.valueOf(LookupOuterJoin));
		e.addContent(child);
		
		XmlUtils.addContent(e, "Default", Default,ns);
		
		if (DataType!=null)
		{
			child = new Element("DataType",ns);
			child.setText(DataType);
			e.addContent(child);
		}
		
		return e;
	}
}
