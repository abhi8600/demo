package com.successmetricsinc.query;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.Util;

/**
 * FederatedJoinQueryHelper class contains implementation of various steps we need to take in order to support disparate
 * joins across different data sources i.e. join as well as rollup as part of middle tier. For example, one could see 
 * "Orders Fact" table in live access connection that joins using customer id to a customers dimension in warehouse
 * and also with Employees table residing on a different live access connection using employee id. If we have a logical
 * query that has Customers.City, Employees.Company and (shipping fee fact from orders fact), we have to join these three
 * sources and perform roll up to return correct resultset. To process such a navigation, we basically create 3 different 
 * query objects.
 * 1. Customers.Customer Id, Employees.Employee ID, Shipping fee - this one would execute against "Orders Fact" on live
 * access connection.
 * 2. Customers.Customer Id, Customers.City - it would execute against Customers dimension on warehouse source.
 * 3. Employees.Employee Id, Employees.Company - it would execute against Employees dimension on a different live access
 * connection from the one mentioned in query 1 above.
 * We execute the physical queries against each connection separately and then create lookup maps for each dimension-only
 * queries. So, in this example, we will create a lookup map with key=Employee ID, value=Employee Name from query 3 above. Also,
 * another lookup map with key=Customer Id, value=Customer City from query 2 above. After these maps are available, we 
 * iterate over measure resultset (generated from query 1 above) and using lookups generated in queries 2 and 3, we do 
 * lookup replacement and rollup at the same time in target resultset.
 * @author mjani
 *
 */
public class FederatedJoinQueryHelper 
{
	private static Logger logger = Logger.getLogger(FederatedJoinQueryHelper.class);
	private static char LOOKUP_KEY_SEPARATOR_CHAR = ';';
	
	public Map<String, String> connectionToQueryMap = new HashMap<String, String> ();	
	private Repository r;
	private Session session;
	private Navigation nav;
	DatabaseConnection measureConn;
	private Map<DatabaseConnection, List<DimensionColumn>> dcToDimColsMap = new HashMap<DatabaseConnection, List<DimensionColumn>> ();
	private Map<DatabaseConnection, List<String>> dcToFjKeysMap = new HashMap<DatabaseConnection, List<String>>();
	private Map<DatabaseConnection, Query> dcToQueryMap = new HashMap<DatabaseConnection, Query>();
	private AtomicInteger startIndex = new AtomicInteger(0);
	private DataUnavailableException exception1;
	private SQLException exception2;
	private NavigationException exception3;
	private Exception exception4;
	private DerivedTableQuery dqt;
	private Query mainQuery;
	private List<String> dimensionFilterTables; //This arraylist will maintain the list of dimension tables which have a filter on one or more columns in the query
	private List<String> measureFilterTables; //This arraylist will maintain the list of measure tables which have a filter on one or more columns in the query
	
	private static String CONNECTION_SPECIFIC_DIMENSION_NAME_PREFIX = "BIRST_DYNAMIC_";
	public FederatedJoinQueryHelper(Repository r, Session session, DerivedTableQuery dqt, Query mainQuery)
	{
		this.r = r;
		this.session = session;
		this.dqt = dqt;
		this.mainQuery = mainQuery;
	}

	/**
	 * Given a navigation, go through all dimension columns and measure column to generate a different query object, one per
	 * database connection. Store the physical query for each database connection for later reference.
	 * @param nav
	 * @param filters
	 * @param specifyDataTypes
	 * @throws CloneNotSupportedException
	 * @throws BaseException
	 * @throws SQLException
	 * @throws IOException
	 */
	public void generate(Navigation nav, List<QueryFilter> filters, boolean specifyDataTypes) throws CloneNotSupportedException, BaseException, SQLException, IOException
	{
		this.nav = nav;
		DatabaseConnection dconn = nav.getNavigationConnection();
		if(!dconn.containsFederatedJoinConnections())
		{
			throw new NavigationException("No federated join connections found on navigation.");
		}
		
		Map<DimensionColumn, Join> dcToFederatedJoinMap = nav.getDcToFederatedJoinMap();
		MeasureColumnNav mcnav = nav.getMeasureColumnNav();
		MeasureColumn matchingmc = mcnav.matchingMeasures.get(mcnav.pick);
		
		//Validate for aggregation rule
		if (matchingmc.AggregationRule.equalsIgnoreCase("SUM") || matchingmc.AggregationRule.equalsIgnoreCase("COUNT") ||
				matchingmc.AggregationRule.equalsIgnoreCase("MIN") || matchingmc.AggregationRule.equalsIgnoreCase("MAX") ||
				matchingmc.AggregationRule.equalsIgnoreCase("FIRST") || matchingmc.AggregationRule.equalsIgnoreCase("LAST"))
		{
			logger.debug("Valid aggregation rule \""+ matchingmc.AggregationRule + "\" for measure column: " + matchingmc.ColumnName);
		}
		else
		{
			throw new NavigationException("Unsupported aggregation rule \""+ matchingmc.AggregationRule + "\" for measure column: " + matchingmc.ColumnName);
		}

		measureConn = matchingmc.MeasureTable.TableSource.connection;

		List<DimensionColumnNav> dimColList = nav.getDimColumns();
		List<String> measureFjkList = new ArrayList<String>();
		if(dimColList != null && dimColList.size() > 0)
		{
			for(DimensionColumnNav dcn : dimColList)
			{
				DatabaseConnection dcnConn = dcn.matchingColumns.get(dcn.pick).DimensionTable.TableSource.connection;
				List<DimensionColumn> dcList = dcToDimColsMap.get(dcnConn);
				if(dcList == null)
				{
					dcList = new ArrayList<DimensionColumn>();
					dcToDimColsMap.put(dcnConn, dcList);
				}
				DimensionColumn dc = dcn.matchingColumns.get(dcn.pick);
				Join jn = dcToFederatedJoinMap.get(dc);
				if(jn != null && jn.isFederated() && (jn.FederatedJoinKeys != null) && (jn.FederatedJoinKeys.length > 0))
				{
					for(int kidx = 0; kidx < jn.FederatedJoinKeys.length; kidx++)
					{
						if(jn.FederatedJoinKeys[kidx].indexOf('.') < 1)
						{
							throw new NavigationException("Invalid federated join key provided. Keys need to be fully qualified to include dimension name: " + jn.FederatedJoinKeys[kidx]);
						}
						List<String> fjKeysList = dcToFjKeysMap.get(dcnConn);
						if(fjKeysList == null)
						{
							fjKeysList = new ArrayList<String>();
							dcToFjKeysMap.put(dcnConn, fjKeysList);
						}
						fjKeysList.add(jn.FederatedJoinKeys[kidx]);
						measureFjkList.add(jn.FederatedJoinKeys[kidx]);
					}
				}
				dcList.add(dc);
			}
			dcToFjKeysMap.put(measureConn, measureFjkList);
		}

		Map<DatabaseConnection, List<QueryFilter>> dcToFiltersMap = new HashMap<DatabaseConnection, List<QueryFilter>> ();
		if(filters != null && filters.size() > 0)
		{
			for(QueryFilter qf : filters)
			{
				if (qf.containsOrOnDifferentConnections(null))
				{
					logger.warn("Data filters using OR on two different connections are not supported with federated queries. Birst will attempt conversion to display filter.");
				}
				else if (qf.getType() == QueryFilter.TYPE_LOGICAL_OP && (qf.containsOnlyANDOperator() || qf.isFilterOnDifferentConnections(null)))
				{
					splitFiltersAndAddToMap(dcToFiltersMap,qf);
				}
				else
				{
					DatabaseConnection filterConn = qf.getColumnConnection();
					List<QueryFilter> filterList = dcToFiltersMap.get(filterConn);
					if(filterList == null)
					{
						filterList = new ArrayList<QueryFilter>();
						dcToFiltersMap.put(filterConn, filterList);
					}
					filterList.add(qf);	
				}				
			}
		}
		
		for(int i = 0; i < dconn.federatedJoinConnections.length; i++)
		{
			DatabaseConnection fedConn = dconn.federatedJoinConnections[i];
			Query q = getQueryObject(fedConn, measureConn, matchingmc, dcToDimColsMap, dcToFiltersMap, dcToFjKeysMap);
			dcToQueryMap.put(fedConn, q);
			q.generateQuery(session, specifyDataTypes, false);
			connectionToQueryMap.put(fedConn.Name, q.getQuery());
		}
	}
	
	private void splitFiltersAndAddToMap(Map<DatabaseConnection, List<QueryFilter>> dcToFiltersMap,QueryFilter qf)
	{
		List<QueryFilter> splitFiltersList = qf.splitIntoIndividualFilters(true, true);
		if (splitFiltersList != null && !splitFiltersList.isEmpty())
		{
			for (QueryFilter sqf : splitFiltersList)
			{
				if (sqf.getType() == QueryFilter.TYPE_LOGICAL_OP && (sqf.containsOnlyANDOperator() || sqf.isFilterOnDifferentConnections(null)))
				{
					splitFiltersAndAddToMap(dcToFiltersMap,sqf);
				}
				else
				{
					DatabaseConnection filterConn = sqf.getColumnConnection();
					List<QueryFilter> filterList = dcToFiltersMap.get(filterConn);
					if(filterList == null)
					{
						filterList = new ArrayList<QueryFilter>();
						dcToFiltersMap.put(filterConn, filterList);
					}
					filterList.add(sqf);
				}
			}
		}
	}
	
	/**
	 * Generate query object for a given connection making sure that each query object contains federated join key.
	 * @param fedConn
	 * @param measureConn
	 * @param mc
	 * @param dcToDimColsMap
	 * @param dcToFiltersMap
	 * @param dcToFjKeysMap
	 * @return
	 * @throws BaseException
	 */
	private Query getQueryObject(DatabaseConnection fedConn, DatabaseConnection measureConn, MeasureColumn mc, 
			Map<DatabaseConnection, List<DimensionColumn>> dcToDimColsMap,
			Map<DatabaseConnection, List<QueryFilter>> dcToFiltersMap,
			Map<DatabaseConnection, List<String>> dcToFjKeysMap) throws BaseException
	{
		Query q = new Query(r);
		if(fedConn.Name.equals(measureConn.Name))
		{
			q.addMeasureColumn(mc.ColumnName, mc.ColumnName, false);
			List<String> fjKeys = dcToFjKeysMap.get(measureConn);
			for(String fjk : fjKeys)
			{
				String dimName = fjk.substring(0, fjk.indexOf('.'));
				String colName = fjk.substring(fjk.indexOf('.')+1, fjk.length());
				DimensionColumn finddc = null;
				String connectionSpecificDynamicDimName = CONNECTION_SPECIFIC_DIMENSION_NAME_PREFIX + mc.MeasureTable.TableSource.ConnectionName + "_" + dimName; 
				List<DimensionColumn> dckeyList = r.getDimensionColumnList(connectionSpecificDynamicDimName, colName);

				if(dckeyList != null && dckeyList.size() > 0)
				{
					for(DimensionColumn dckey : dckeyList)
					{
						if (dckey.DimensionTable.TableSource.Tables[0].PhysicalName.equals(mc.MeasureTable.TableSource.Tables[0].PhysicalName))
						{
							finddc = dckey;
							break;
						}
					}
				}
				if(finddc == null)
				{
					throw new NavigationException("Invalid federated join key column specified, could not find corresponding measure column: " + fjk);
				}

				q.addDimensionColumn(finddc.DimensionTable.DimensionName, finddc.ColumnName);
				q.addGroupBy(new GroupBy(finddc.DimensionTable.DimensionName, finddc.ColumnName));
			}
		}
		
		Set<DimensionTable> dimTables = new HashSet<DimensionTable>();
		List<DimensionColumn> dcList = dcToDimColsMap.get(fedConn);
		if(dcList != null && dcList.size() > 0)
		{
			for(DimensionColumn dimCol : dcList)
			{
				q.addDimensionColumn(dimCol.DimensionTable.DimensionName, dimCol.ColumnName);
				q.addGroupBy(new GroupBy(dimCol.DimensionTable.DimensionName, dimCol.ColumnName));
				dimTables.add(dimCol.DimensionTable);
			}
		}
		if(!fedConn.Name.equals(measureConn.Name))
		{
			List<String> fjKeys = dcToFjKeysMap.get(fedConn);
			for(String fjk : fjKeys)
			{
				String dimName = fjk.substring(0, fjk.indexOf('.'));
				String colName = fjk.substring(fjk.indexOf('.')+1, fjk.length());
				DimensionColumn finddc = null;
				List<DimensionColumn> dckeyList = r.findDimensionColumns(dimName, colName);
				if(dckeyList != null && dckeyList.size() > 0)
				{
					for(DimensionColumn dckey : dckeyList)
					{
						if(dimTables.contains(dckey.DimensionTable))
						{
							finddc = dckey;
							break;
						}
					}
				}
				if(finddc == null)
				{
					throw new NavigationException("Invalid federated join key column specified, could not find corresponding dimension column: " + fjk);
				}
				
				if(!dcList.contains(finddc))
				{
					q.addDimensionColumn(finddc.DimensionTable.DimensionName, finddc.ColumnName);
					q.addGroupBy(new GroupBy(finddc.DimensionTable.DimensionName, finddc.ColumnName));
				}
			}
		}
		
		List<QueryFilter> qfList = dcToFiltersMap.get(fedConn);
		if(qfList != null && qfList.size() > 0)
		{
			for(QueryFilter qf : qfList)
			{
				q.addFilter(qf);
			}
		}

		return q;
	}
	
	/**
	 * Federated join key for a join between measure table residing on one connection and dimension table residing on another is
	 * specified in the format Dimension.Column. The query on measure table needs to contain the joining key so that the resultsets
	 * can be merged based on joining keys at the end. If we add the measure key column as is, the aggregation rule is applied on 
	 * key column. We want group by to be applied on the key column. So, we create a dynamic dimension table just containing the
	 * key column and add that dimension table to the query. That way, the query engine automatically does a group by instead of
	 * applying aggregation rule on the key column as part of the query that executes on measure connection.
	 * @param mt
	 * @param dt
	 * @param keys
	 * @param r
	 * @return
	 */
	public static Join addDynamicDimensionAndReturnJoin(MeasureTable mt, DimensionTable dt, String[] keys, Repository r)
	{
		if(mt == null || dt == null || keys == null || keys.length == 0)
		{
			return null;
		}
		List<MeasureColumn> mcKeyList = new ArrayList<>(keys.length);
		for(String fjk : keys)
		{
			MeasureColumn mc = mt.findColumn(fjk);
			if(mc == null)
			{
				logger.error("Could not find measure column for federated join key: "+ fjk + ". This will result in error when federated join is used.");
			}
			else
			{
				mcKeyList.add(mc);
			}
		}
		
		if(keys.length != mcKeyList.size())
		{
			return null;
		}
		
		String dynamicDimensionName = CONNECTION_SPECIFIC_DIMENSION_NAME_PREFIX + mt.TableSource.ConnectionName + "_" + dt.DimensionName;
		String dynamicDimensionTableName = dynamicDimensionName + " Table";
		
		DimensionTable newdt = r.findDimensionTable(dynamicDimensionTableName);
		List<DimensionColumn> dimColList = new ArrayList<DimensionColumn>();
		if(newdt == null)
		{
			newdt = new DimensionTable(r);
			newdt.TableName = dynamicDimensionTableName;
			newdt.DimensionName = dynamicDimensionName;
			newdt.Type = 0;
			newdt.Cacheable = true;
			newdt.Level = dt.Level;
			newdt.PhysicalName = mt.PhysicalName;
	
			newdt.TableSource = new TableSource();
			newdt.TableSource.connection = mt.TableSource.connection;
			newdt.TableSource.ConnectionName = mt.TableSource.ConnectionName;
			newdt.TableSource.Tables = new TableDefinition[1];
			
			newdt.TableSource.Tables[0] = new TableDefinition();
			newdt.TableSource.Tables[0].PhysicalName = mt.TableSource.Tables[0].PhysicalName;
		}
		if((newdt.DimensionColumns != null) && (newdt.DimensionColumns.length > 0))
		{
			for(DimensionColumn existingdc : newdt.DimensionColumns)
			{
				dimColList.add(existingdc);
			}
		}

		for(int keyIdx = 0; keyIdx < keys.length; keyIdx++)
		{
			MeasureColumn mcKey = mcKeyList.get(keyIdx);
			DimensionColumn newdc = new DimensionColumn();
			newdc.ColumnName = keys[keyIdx].substring(keys[keyIdx].indexOf('.') + 1,keys[keyIdx].length());
			newdc.DataType = mcKey.DataType;
			newdc.DimensionTable = newdt;
			newdc.Key = true;
			newdc.PhysicalName = mcKey.PhysicalName;
			newdc.Qualify = mcKey.Qualify;
			newdc.TableName = newdt.TableName;
			newdc.Width = mcKey.Width;
			dimColList.add(newdc);
		}

		newdt.DimensionColumns = new DimensionColumn[dimColList.size()];
		dimColList.toArray(newdt.DimensionColumns);
		r.addDimensionTable(newdt);
		
		Join jn = new Join();
		jn.Federated = false;
		jn.FederatedJoinKeys = null;
		jn.JoinCondition = "";
		jn.Redundant = true;
		jn.Table1 = newdt;
		jn.Table2 = mt;
		jn.Table1Str = newdt.TableName;
		jn.Table2Str = mt.TableName;
		jn.Type = JoinType.Inner;

		return jn;
	}

	/**
	 * Spawns off threads to execute separate queries and return associated resultsets.
	 * @param q
	 * @param statements
	 * @param physicalQueries
	 * @param maxRecords
	 * @param maxQueryTime
	 * @return
	 * @throws DataUnavailableException
	 * @throws SQLException
	 * @throws NavigationException
	 */
	public DatabaseConnectionResultSet [] executeQueriesAndGetResultSets(Query q, DatabaseConnectionStatement [] statements, 
			String [] physicalQueries, int maxRecords, int maxQueryTime) throws DataUnavailableException, SQLException, NavigationException
	{
		DatabaseConnectionResultSet[] list = new DatabaseConnectionResultSet[statements.length];
		// Spin threads to get the variable-specific of the result sets
		int numThreads = Math.min(statements.length, 4);
		Thread[] tlist = new Thread[numThreads];
		startIndex = new AtomicInteger(0);
		exception1 = null;
		exception2 = null;
		exception3 = null;
		exception4 = null;
		for (int i = 0; i < numThreads; i++)
		{
			tlist[i] = new QueryExecutorThread(physicalQueries, statements, list, maxRecords, maxQueryTime, q);
			tlist[i].start();
		}
		try
		{
			for (int i = 0; i < numThreads; i++)
			{
				tlist[i].join();
			}
		} catch (InterruptedException ex)
		{
			logger.error("InteruptedException: " + ex.getCause());
			return null;
		}
		if(exception1 != null || exception2 != null || exception3 != null || exception4 != null)
		{
			if (statements != null)
			{
				for (int i=0; i<statements.length; i++)
				{
					if(statements[i] == null)
					{
						continue;
					}
					DatabaseConnection dconn = statements[i].getDatabaseConnection();
					Connection c = null;
					try
					{
						if(!dconn.isRealTimeConnection())
						{
							c = statements[i].getConnection();
						}
					}
					catch(Exception e)
					{
						logger.error("Error gettig connection from statement, connection will not be returned to pool:" + e.toString(), e);
					}
					if((c != null) && (dconn != null))
					{
						if(!dconn.isRealTimeConnection())
						{
							ConnectionPool.returnToParallelPoolOrCloseDynamicConnection(c, dconn, q.getSession(), true);
						}
					}
				}
			}
		}
		if (exception1 != null)
			throw exception1;
		if (exception2 != null)
			throw exception2;
		if (exception3 != null)
			throw exception3;
		return list;

	}
	
	/*
	 * This method will loop through all the QueryFilters on the mainQuery object and prepare respectively a list each of 
	 * measure and dimension tables which have any filter on them. If filter type is logical, then we will first split the 
	 * filter because looping through to identify the table names
	 */
	private void setFilterTableLists()
	{
		if (dimensionFilterTables == null)
			dimensionFilterTables = new ArrayList<String>();
		if (measureFilterTables == null)
			measureFilterTables = new ArrayList<String>();
		List<QueryFilter> qfList = null;
		if(mainQuery != null)
		{
			qfList = mainQuery.filters;
		}
		if (qfList!=null && !qfList.isEmpty())
		{
			for (QueryFilter qf : qfList)
			{
				if (qf.getType() == QueryFilter.TYPE_LOGICAL_OP)
				{
					List<QueryFilter> splitFilterList = qf.splitIntoIndividualFilters(true, false);

					if (splitFilterList!=null && !splitFilterList.isEmpty())
					{
						for (QueryFilter cqf : splitFilterList)
						{
							if (cqf.getColumnType() == QueryFilter.TYPE_DIMENSION_COLUMN)
							{
								DimensionColumn dc = (DimensionColumn)cqf.getColumnObject();
								if (dc!=null && !dimensionFilterTables.contains(dc.TableName))
									dimensionFilterTables.add(dc.TableName);
							}
							else if (cqf.getColumnType() == QueryFilter.TYPE_MEASURE)
							{
								MeasureColumn mc = (MeasureColumn)cqf.getColumnObject();
								if (mc!=null && !measureFilterTables.contains(mc.TableName))
									measureFilterTables.add(mc.TableName);
							}
						}
					}
				}
				else
				{
					if (qf.getColumnType() == QueryFilter.TYPE_DIMENSION_COLUMN)
					{
						DimensionColumn dc = (DimensionColumn)qf.getColumnObject();
						if (dc!=null && !dimensionFilterTables.contains(dc.TableName))
							dimensionFilterTables.add(dc.TableName);
					}
					else if (qf.getColumnType() == QueryFilter.TYPE_MEASURE)
					{
						MeasureColumn mc = (MeasureColumn)qf.getColumnObject();
						if (mc!=null && !measureFilterTables.contains(mc.TableName))
							measureFilterTables.add(mc.TableName);
					}
				}
			}
		}
	}
	
	/**
	 * Once we have all different resultsets from different connections, we need to merge them all, do any necessary rollup
	 * and return a resultset that matches the navigation projection list. This method does so by first creating lookup data
	 * structures from all dimension queries and then using these lookups to merge into measure resultset. In the process, it
	 * also takes care of grouping the final resultset by dimension columns in navigation's projection list i.e. rolling up
	 * as necessary. For faster execution, a merge reference object is created that is metadata for merging and rolling up
	 * resultset. Merge reference object contains indexes to different arrays so that as we go through resultset merging and
	 * rolling up process, we just need to use those indexes for indirection rather than trying to figure out the elements
	 * to be used based on names for every single row.
	 * @param dcrsList
	 * @return
	 * @throws BaseException
	 * @throws SQLException
	 */
	public DatabaseConnectionResultSet getMergedResultSets(DatabaseConnectionResultSet [] dcrsList) 
			throws BaseException, SQLException
	{
		setFilterTableLists();
		DatabaseConnectionResultSet measureResultSet = null;
		DatabaseConnectionResultSet [] dimensionResultSets = new DatabaseConnectionResultSet[dcrsList.length - 1];
		int [][] fjKeyColumnIndices = new int [dcrsList.length][]; 
		int dimResultSetIdx = 0;
		for(DatabaseConnectionResultSet dcrs : dcrsList)
		{
			DatabaseConnection dconn = dcrs.getDatabaseConnection();
			if(dconn == measureConn)
			{
				measureResultSet = dcrs;
				break;
			}
		}
		for(DatabaseConnectionResultSet dcrs : dcrsList)
		{
			DatabaseConnection dconn = dcrs.getDatabaseConnection();
			if(dconn == measureConn)
			{
				continue;
			}
			else
			{
				dimensionResultSets[dimResultSetIdx] = dcrs;
			}
			List<String> fjKeys = dcToFjKeysMap.get(dconn);
			fjKeyColumnIndices[dimResultSetIdx] = new int [fjKeys.size()];
			Query q = dcToQueryMap.get(dconn);
			List<QueryColumn> qplist = q.getReturnProjectionList();
			for(int qcIdx = 0; qcIdx < qplist.size(); qcIdx++)
			{
				QueryColumn qc = qplist.get(qcIdx);
				if(qc.type == QueryColumn.DIMENSION)
				{
					DimensionColumn dc = (DimensionColumn) qc.o;
					String qualDimColName = dc.DimensionTable.DimensionName + "." + dc.ColumnName;
					for(int fjIdx = 0; fjIdx < fjKeys.size(); fjIdx++)
					{
						if(fjKeys.get(fjIdx).equals(qualDimColName))
						{
							fjKeyColumnIndices[dimResultSetIdx][fjIdx] = qcIdx + 1;
						}
					}
				}
			}
			dimResultSetIdx++;
		}

		Map<String, Object[]> [] lookupList = createLookupLists(dimensionResultSets, fjKeyColumnIndices);
		@SuppressWarnings("unchecked")
		List<String> [] outerJoinList = new List [lookupList.length];
		for(int i = 0; i < lookupList.length; i++)
		{
			outerJoinList[i] = new ArrayList<String>();
		}
		
		List<QueryColumn> plist = null;
		
		if(mainQuery != null)
		{
			plist = mainQuery.getReturnProjectionList();
		}
		if(plist == null)
		{
			plist = nav.getProjectionList();
		}
		dqt.setResultProjectionList(plist);
		int size = plist.size();
		int [] dataTypes = new int[size];
		List<Integer> groupByColumnIndicesList = new ArrayList<Integer>();
		Map<Integer, MergeReference> qcIndexToMergeReferenceMap = new HashMap<Integer, MergeReference>();		
		boolean[] sum = new boolean[size];
		boolean[] min = new boolean[size];
		boolean[] max = new boolean[size];
		boolean[] first = new boolean[size];
		boolean[] last = new boolean[size];

		/**
		 * We need to address any outer join situations. Based on join type defined as part of federated join, we built the metadata
		 * as part of merge reference. Before iterating over measure resultset, we will populate outerJoinList with all key values 
		 * that we received from individual dimension resultsets. After that, while iterating over measure resultset, we remove each
		 * key entry present in the measure resultset. So, at the end of iteration over measure resultset, outerJoinList will contain
		 * only those key entries that are not present in measure result set. 
		 */

		for(int i = 0; i < size; i++)
		{
			QueryColumn qc = plist.get(i);
			dataTypes[i] = qc.getDataType();
			if(DatabaseConnection.isDBTypeMySQL(measureConn.DBType) && (qc.type == QueryColumn.MEASURE))
			{
				//IB/MySQL converts result set values into double if sum aggregation rule is applied on integer column
				MeasureColumn mc = (MeasureColumn) qc.o;
				if("SUM".equalsIgnoreCase(mc.AggregationRule))
				{
					dataTypes[i] = Types.DOUBLE;
				}
			}
			MergeReference mr = getMergeReference(qc, measureResultSet, dimensionResultSets);
			if(mr != null)
			{
				qcIndexToMergeReferenceMap.put(i, mr);
				if(!mr.applyAggregation)
				{
					groupByColumnIndicesList.add(i);
					if(mr.useLookupList && mr.addNullMeasuresForAdditionalLookup && (mr.lookupListIndex != -1))
					{
						outerJoinList[mr.lookupListIndex].addAll(lookupList[mr.lookupListIndex].keySet());
					}
				}
				else
				{
					MeasureColumn qmc = null;
					if(qc.type == QueryColumn.MEASURE)
					{
						qmc = (MeasureColumn) qc.o;
					}
					if((qmc != null) && (qmc.AggregationRule != null))
					{
						if (qmc.AggregationRule.equalsIgnoreCase("SUM") || qmc.AggregationRule.equalsIgnoreCase("COUNT"))
							sum[i] = true;
						else if (qmc.AggregationRule.equalsIgnoreCase("MIN"))
							min[i] = true;
						else if (qmc.AggregationRule.equalsIgnoreCase("MAX"))
							max[i] = true;
						else if (qmc.AggregationRule.equalsIgnoreCase("FIRST"))
							first[i] = true;
						else if (qmc.AggregationRule.equalsIgnoreCase("LAST"))
							last[i] = true;
						else
							throw new NavigationException("Unsupported aggregation rule \""+ qmc.AggregationRule + "\" for measure column: " + qmc.ColumnName);
					}
				}
			}
		}

		int [] groupByColumnIndices = new int [groupByColumnIndicesList.size()];
		for(int gbIdx = 0; gbIdx < groupByColumnIndicesList.size(); gbIdx++)
		{
			groupByColumnIndices[gbIdx] = groupByColumnIndicesList.get(gbIdx);
		}
		if(measureResultSet == null)
		{
			throw new NavigationException("Measure resultset not set.");
		}
		long startTime = System.currentTimeMillis();
		TreeMap<String, Object []> gbkeyToRowMap = new TreeMap<String, Object []> ();
		int colCount = measureResultSet.getColumnCount();
		int numRows = 0;
		int maxRecords = r.getServerParameters().getMaxRecords();

		while(measureResultSet.next())
		{
			Object [] measureResultSetRow = new Object[colCount];
			for(int colIdx = 1; colIdx <= colCount; colIdx++)
			{
				measureResultSetRow[colIdx - 1] = measureResultSet.getObject(colIdx);
			}

			boolean skip = false;
			Object[] row = new Object[size];
			for(int i = 0; i < size; i++)
			{
				MergeReference mr = qcIndexToMergeReferenceMap.get(i);
				if(mr == null)
				{
					row[i] = null;
				}
				else
				{
					if(mr.useMeasureResultSet)
					{
						//Aggregation to be done
						Object val = measureResultSetRow[mr.measureResultSetColumnIndex];
						row[i] = val;
					}
					else if(mr.useLookupList)
					{
						String lookupKey = getLookupKey(measureResultSetRow, mr.lookupKeyColumnIndices);
						Object [] lookupRow = lookupList[mr.lookupListIndex].get(lookupKey);
						if(lookupRow != null)
						{
							row[i] = lookupRow[mr.lookupColumnIndex];
							if(mr.addNullMeasuresForAdditionalLookup)
							{
								outerJoinList[mr.lookupListIndex].remove(lookupKey);
							}
						}
						else if(mr.skipMeasureRowForMissingLookup)
						{
							skip = true;
							break;
						}
					}
				}
			}
			if(skip)
			{
				continue;
			}
			String gbKey = getLookupKey(row, groupByColumnIndices);
			Object [] existingRow = gbkeyToRowMap.get(gbKey);
			if(existingRow == null)
			{
				gbkeyToRowMap.put(gbKey, row);
				numRows++;
			}
			else
			{
				Object [] newRow = processAggregations(existingRow, row, dataTypes, sum, min, max, first, last);
				gbkeyToRowMap.put(gbKey, newRow);
			}
			if(numRows > maxRecords)
			{
				logger.info("Too much data returned in the query (max = " + maxRecords + ").");
				throw new ResultSetTooBigException("Too much data returned in the query (max = " + maxRecords + "). Please add filters to reduce the size of the result set.");
			}
		}

		/**
		 * Now iterate over all outerJoinList objects - these should have only the values not present in measure resultset. Or in case of
		 * virtual inner join, the lists should be empty. For each entry in the list, get the lookup row that becomes a new row in the 
		 * result set if the same values are not present.
		 */
		for(int listIdx = 0; listIdx < outerJoinList.length; listIdx++)
		{
			if(outerJoinList[listIdx].isEmpty())
			{
				continue;
			}
			int ojListSize = outerJoinList[listIdx].size(); 
			for(int ojIdx = 0; ojIdx < ojListSize; ojIdx++)
			{
				Object [] newRow = new Object[colCount];
				for(int colIdx = 0; colIdx < colCount; colIdx++)
				{
					newRow[colIdx] = null;
				}
				boolean lookupRowFound = false;
				for(int colIdx = 0; colIdx < colCount; colIdx++)
				{
					MergeReference mr = qcIndexToMergeReferenceMap.get(colIdx);
					if((mr != null) && mr.useLookupList && mr.addNullMeasuresForAdditionalLookup && (mr.lookupListIndex == listIdx))
					{
						String lookupKey = outerJoinList[listIdx].get(ojIdx);
						Object [] lookupRow = lookupList[mr.lookupListIndex].get(lookupKey);
						if(lookupRow != null)
						{
							newRow[colIdx] = lookupRow[mr.lookupColumnIndex];
							lookupRowFound = true;
						}
					}
				}
				if(lookupRowFound)
				{
					String gbKey = getLookupKey(newRow, groupByColumnIndices);
					Object [] existingRow = gbkeyToRowMap.get(gbKey);
					if(existingRow == null)
					{
						gbkeyToRowMap.put(gbKey, newRow);
						numRows++;
					}
				}
				if(numRows > maxRecords)
				{
					logger.info("Too much data returned in the query (max = " + maxRecords + ").");
					throw new ResultSetTooBigException("Too much data returned in the query (max = " + maxRecords + "). Please add filters to reduce the size of the result set.");
				}
			}
		}
				
		List<Object []> rows = new ArrayList<Object []>(gbkeyToRowMap.size());		
		for (Iterator<Object[]> it = gbkeyToRowMap.values().iterator(); it.hasNext();)
		{
			Object[] row = it.next();
			rows.add(row);			
		}

		logger.debug("Merged resultset created in:" + (System.currentTimeMillis() - startTime) + "ms");
		
		DatabaseConnectionResultSet returnrs = new DatabaseConnectionResultSet(dataTypes, rows);
		
		return returnrs;
	}

	/**
	 * Based on indices of given row object, the method generates a look up key that is concatenation of all applicable
	 * column values separated by lookup key separator.
	 * @param row
	 * @param indices
	 * @return
	 */
	private String getLookupKey(Object [] row, int[] indices)
	{
		StringBuilder sb = new StringBuilder();
		
		boolean first = true;
		for(int i = 0; i < indices.length; i++)
		{
			if(first)
			{
				first = false;
			}
			else
			{
				sb.append(LOOKUP_KEY_SEPARATOR_CHAR);
			}
			sb.append(row[indices[i]]);
		}
		return sb.toString();
	}
	
	/**
	 * Given an existing row and new incoming row, it adds up or to be more precise, applies aggregation on measure column
	 * values to return a row with new aggregated values of measure column.
	 * @param existingRow
	 * @param row
	 * @param dataTypes
	 * @param sum
	 * @param min
	 * @param max
	 * @param first
	 * @param last
	 * @return
	 */
	private Object [] processAggregations(Object[] existingRow, Object [] row, int [] dataTypes,
			boolean [] sum, boolean [] min, boolean [] max, boolean [] first, boolean [] last)
	{
		int len = existingRow.length;
		
		for(int i = 0; i < len; i++)
		{
			if(existingRow[i] != null)
			{
				if(sum[i])
				{
					if (dataTypes[i] == Types.DOUBLE)
					{
						existingRow[i] = ((Double) row[i]) + ((Double) existingRow[i]);
					} else if (dataTypes[i] == Types.INTEGER)
					{
						existingRow[i] = ((Long) row[i]) + ((Long) existingRow[i]);
					}
				}
				else if (min[i])
				{
					if (dataTypes[i] == Types.DOUBLE)
						existingRow[i] = Math.min(((Double) row[i]), ((Double) existingRow[i]));
					else if (dataTypes[i] == Types.INTEGER)
						existingRow[i] = Math.min(((Long) row[i]), ((Long) existingRow[i]));
				} else if (max[i])
				{
					if (dataTypes[i] == Types.DOUBLE)
						existingRow[i] = Math.max(((Double) row[i]), ((Double) existingRow[i]));
					else if (dataTypes[i] == Types.INTEGER)
						existingRow[i] = Math.max(((Long) row[i]), ((Long) existingRow[i]));
				} else if (first[i])
				{
					if (existingRow[i] == null)
						existingRow[i] = row[i];
				} else if (last[i])
				{
					if(row[i] != null)
						existingRow[i] = row[i];
				}
			}
		}
		
		return existingRow;
	}
	
	/**
	 * Creates metadata class merge reference that is used in the process of merging resultsets. Merge reference object 
	 * contains indexes to different arrays so that as we go through resultset merging and rolling up process, we 
	 * just need to use those indexes for indirection rather than trying to figure out the elements to be used 
	 * based on names for every single row.
	 * @param qc
	 * @param measureResultSet
	 * @param dimensionResultSets
	 * @return
	 */
	private MergeReference getMergeReference(QueryColumn qc, DatabaseConnectionResultSet measureResultSet, 
			DatabaseConnectionResultSet [] dimensionResultSets)
	{
		MergeReference mr = new MergeReference();
		if(qc.type == QueryColumn.DIMENSION)
		{
			DimensionColumn dc = (DimensionColumn) qc.o;
			DatabaseConnection dconn = measureResultSet.getDatabaseConnection();
			Query q = dcToQueryMap.get(dconn);
			List<QueryColumn> plist = q.getReturnProjectionList();
			boolean found = false;
			for(int pIdx = 0; pIdx < plist.size(); pIdx++)
			{
				QueryColumn pqc = plist.get(pIdx);
				if(pqc.type == QueryColumn.DIMENSION)
				{
					DimensionColumn pdc = (DimensionColumn) pqc.o;
					if(dc == pdc)
					{
						mr.useMeasureResultSet = true;
						mr.useLookupList = false;
						mr.measureResultSetColumnIndex = pIdx;
						mr.applyAggregation = false;
						mr.lookupColumnIndex = -1;
						mr.lookupListIndex = -1;
						mr.lookupKeyColumnIndices = null;
						found = true;
						break;
					}
				}
			}
				
			if(!found)
			{
				for(int dcLookupListIdx = 0; dcLookupListIdx < dimensionResultSets.length; dcLookupListIdx++)
				{
					dconn = dimensionResultSets[dcLookupListIdx].getDatabaseConnection();
					q = dcToQueryMap.get(dconn);
					plist = q.getReturnProjectionList();
					for(int pIdx = 0; pIdx < plist.size(); pIdx++)
					{
						QueryColumn pqc = plist.get(pIdx);
						if(pqc.type == QueryColumn.DIMENSION)
						{
							DimensionColumn pdc = (DimensionColumn) pqc.o;
							if(dc == pdc)
							{
								Join jn = nav.getDcToFederatedJoinMap().get(dc);
								if(jn != null )
								{
									/**
									 * If there is any filter on either the dimension or the measure table, then the default join behavior will change. Eg
									 * If filter exists on both left hand side and right hand tables, then even if left join is specified in the repository,
									 * the query results should actually be the same as an inner join. We will follow this table in order to implement the joins
									 * Left Side Table	|Right Side Table	|Join Type	|Filter On	|Actual Join Type
									 * -------------------------------------------------------------------------------
									 * 		T1			|	T2				|Left		|T1			|Left
									 * 		T1			|	T2				|Left		|T2			|Inner
									 * 		T1			|	T2				|Left		|Both		|Inner
									 * 		T1			|	T2				|Right		|T1			|Inner
									 * 		T1			|	T2				|Right		|T2			|Right
									 * 		T1			|	T2				|Right		|Both		|Inner
									 * 		T1			|	T2				|Full Join	|T1			|Left
									 * 		T1			|	T2				|Full Join	|T2			|Right
									 * 		T1			|	T2				|Full Join	|Both		|Inner
									 **/
									String t1Name = jn.Table1 instanceof DimensionTable? ((DimensionTable)jn.Table1).TableName : ((MeasureTable)jn.Table1).TableName;
									String t2Name = jn.Table2 instanceof DimensionTable? ((DimensionTable)jn.Table2).TableName : ((MeasureTable)jn.Table2).TableName;
									
									if ((dimensionFilterTables.contains(t1Name) && measureFilterTables.contains(t2Name)) || 
											(measureFilterTables.contains(t1Name) && dimensionFilterTables.contains(t2Name)) ||
											(jn.Type == JoinType.Inner))
									{
										//Filter on both sides - always inner join
										mr.skipMeasureRowForMissingLookup = true;
										mr.addNullMeasuresForAdditionalLookup = false;
									}
									else if(dimensionFilterTables.contains(t1Name))
									{
										mr.skipMeasureRowForMissingLookup = true;
										mr.addNullMeasuresForAdditionalLookup = ((jn.Type == JoinType.LeftOuter) || (jn.Type == JoinType.FullOuter));
									}
									else if(dimensionFilterTables.contains(t2Name))
									{
										mr.skipMeasureRowForMissingLookup = true;
										mr.addNullMeasuresForAdditionalLookup = ((jn.Type == JoinType.RightOuter) || (jn.Type == JoinType.FullOuter));
									}
									else if(measureFilterTables.contains(t1Name))
									{
										mr.skipMeasureRowForMissingLookup = (jn.Type == JoinType.RightOuter);
										mr.addNullMeasuresForAdditionalLookup = false;
									}
									else if(measureFilterTables.contains(t2Name))
									{
										mr.skipMeasureRowForMissingLookup = (jn.Type == JoinType.LeftOuter);
										mr.addNullMeasuresForAdditionalLookup = false;
									}
									else
									{
										/**
										 * For join definition, we consider Table1 to be on left side and Table2 to be on right side.
										 * So for full outer join, we shouldn't skip any measure rows and add null measures whenever 
										 * there is no corresponding key matching dimension key on measure result set.
										 * For Inner join, we should skip the measure row if we can't look up corresponding dimension key.
										 * Similarly, if dimension table is on left, for a left outer join, we need to skip measure rows 
										 * and add null measure rows and vice versa for right outer join 
										 **/
										if(jn.Type == JoinType.FullOuter)
										{
											mr.skipMeasureRowForMissingLookup = false;
											mr.addNullMeasuresForAdditionalLookup = true;
										}
										else if(jn.Type == JoinType.Inner)
										{
											mr.skipMeasureRowForMissingLookup = true;
											mr.addNullMeasuresForAdditionalLookup = false;
										}
										else if(jn.Table1 == dc.DimensionTable)
										{
											mr.skipMeasureRowForMissingLookup = (jn.Type == JoinType.LeftOuter);
											mr.addNullMeasuresForAdditionalLookup = (jn.Type == JoinType.LeftOuter);
										}
										else if(jn.Table2 == dc.DimensionTable)
										{
											mr.skipMeasureRowForMissingLookup = (jn.Type == JoinType.RightOuter);
											mr.addNullMeasuresForAdditionalLookup = (jn.Type == JoinType.RightOuter);
										}
									}
								}
								else
								mr.useMeasureResultSet = false;
								mr.useLookupList = true;
								mr.measureResultSetColumnIndex = -1;
								mr.applyAggregation = false;
								mr.lookupColumnIndex = pIdx;
								mr.lookupListIndex = dcLookupListIdx;
								List<String> fjKeys = dcToFjKeysMap.get(dconn);
								DatabaseConnection mdconn = measureResultSet.getDatabaseConnection();
								Query mq = dcToQueryMap.get(mdconn);
								List<QueryColumn> mplist = mq.getReturnProjectionList();
								mr.lookupKeyColumnIndices = new int [fjKeys.size()];
								for(int lkIdx = 0; lkIdx < fjKeys.size(); lkIdx++)
								{
									mr.lookupKeyColumnIndices[lkIdx] = -1;
								}
								for(int fjkIdx = 0; fjkIdx < fjKeys.size(); fjkIdx++)
								{
									boolean lkfound = false;
									String fjk = fjKeys.get(fjkIdx);
									String dimName = fjk.substring(0, fjk.indexOf('.'));
									String colName = fjk.substring(fjk.indexOf('.')+1, fjk.length());
									String connectionSpecificDynamicDimName = CONNECTION_SPECIFIC_DIMENSION_NAME_PREFIX + mdconn.Name + "_" + dimName; 

									for(int mpIdx = 0; mpIdx < mplist.size(); mpIdx++)
									{
										QueryColumn mqc = mplist.get(mpIdx);
										if(mqc.type == QueryColumn.DIMENSION)
										{
											DimensionColumn mqdc = (DimensionColumn) mqc.o;
											if(mqdc.DimensionTable.DimensionName.equals(connectionSpecificDynamicDimName) && mqdc.ColumnName.equals(colName))
											{
												mr.lookupKeyColumnIndices[fjkIdx] = mpIdx;
												lkfound = true;
												break;
											}
										}
									}
									if(!lkfound)
									{
										logger.error("Could not find key column to lookup in measure result set for: "+ fjk);
									}
								}
								found = true;
								break;
							}
						}
					}
				}
			}
			if(!found)
			{
				logger.error("Cound not find dimension column in measure result set as well as dimension result sets:" + qc.name);
				return null;
			}
		}
		else if(qc.type == QueryColumn.MEASURE)
		{
			MeasureColumn mc = (MeasureColumn) qc.o;
			
			DatabaseConnection dconn = measureResultSet.getDatabaseConnection();
			Query q = dcToQueryMap.get(dconn);
			List<QueryColumn> plist = q.getReturnProjectionList();
			boolean found = false;
			for(int pIdx = 0; pIdx < plist.size(); pIdx++)
			{
				QueryColumn pqc = plist.get(pIdx);
				if(pqc.type == QueryColumn.MEASURE)
				{
					MeasureColumn pmc = (MeasureColumn) pqc.o;
					if(mc == pmc)
					{
						mr.useMeasureResultSet = true;
						mr.useLookupList = false;
						mr.measureResultSetColumnIndex = pIdx;
						mr.applyAggregation = true;
						mr.lookupColumnIndex = -1;
						mr.lookupListIndex = -1;
						found = true;
						break;
					}
				}
			}
			if(!found)
			{
				logger.error("Cound not find measure column in measure result set:" + qc.name);
				return null;
			}
		}
		else
		{
			logger.error("Only dimension or measure columns supported in federed join query. Null value will be set for column: " + qc.name);
			return null;
		}

		return mr;
	}
	
	/**
	 * Converts resultsets for dimension queries into a lookup structure that can be used in merging resultsets.
	 * @param dimensionResultSets
	 * @param fjKeyColumnIndices
	 * @return
	 */
	private Map<String, Object[]> [] createLookupLists(DatabaseConnectionResultSet [] dimensionResultSets, 
			int [][] fjKeyColumnIndices)
	{
		@SuppressWarnings("unchecked")
		Map<String, Object[]> [] lookupList = new Map[dimensionResultSets.length];
		// Spin threads to get the variable-specific of the result sets
		int numThreads = Math.min(dimensionResultSets.length, 4);
		Thread[] tlist = new Thread[numThreads];
		startIndex = new AtomicInteger(0);
		exception1 = null;
		exception2 = null;
		exception3 = null;
		exception4 = null;
		for (int i = 0; i < numThreads; i++)
		{
			tlist[i] = new DimensionResultSetProcessorThread(dimensionResultSets, fjKeyColumnIndices, lookupList);
			tlist[i].start();
		}
		try
		{
			for (int i = 0; i < numThreads; i++)
			{
				tlist[i].join();
			}
		} catch (InterruptedException ex)
		{
			logger.error("InteruptedException: " + ex.getCause());
			return null;
		}
		return lookupList;
	}
	
	/**
	 * Executes one or more queries till none is left in the queue and populates resultsets array.
	 * @author mjani
	 *
	 */
	public class QueryExecutorThread extends Thread
	{
		private DatabaseConnectionResultSet[] list;
		private String[] physicalQueries;
		private DatabaseConnectionStatement [] statements;
		int curIndex;
		private int maxRecords; 
		private int maxQueryTime;
		Query q = null;
		
		public QueryExecutorThread(String[] physicalQueries, DatabaseConnectionStatement [] statements,
				DatabaseConnectionResultSet[] list, int maxRecords, int maxQueryTime, Query q)
		{
			this.setName("QueryExecutorThread - " + this.getId());
			setDaemon(true); // if the JVM wants to exit due to errors, let it
			this.physicalQueries = physicalQueries;
			this.list = list;
			this.statements = statements;
			this.maxRecords = maxRecords;
			this.maxQueryTime = maxQueryTime;
			this.q = q;
		}

		public void run()
		{
			while (true)
			{
				curIndex = startIndex.getAndIncrement();
				if (curIndex >= list.length)
					return;
				try
				{
					DatabaseConnectionStatement stmt = statements[curIndex];
					// deal with DBMS's that don't support these two calls
					try
					{
						int curMaxRows = stmt.getMaxRows();
						// use some limiters to make sure we don't trash the SQL Server or the web application server
						if ((maxRecords > 0) && (maxRecords != Integer.MAX_VALUE) && ((maxRecords +2) != curMaxRows))
						{
							stmt.setMaxRows(maxRecords + 2);
						}
						if (maxQueryTime > 0 && maxQueryTime != Integer.MAX_VALUE)
						{
							stmt.setQueryTimeout(maxQueryTime);
						}
					} catch (Exception e)
					{
						if (logger.isTraceEnabled())
							logger.trace(e.getMessage(), e);
					}
					long starttime = System.currentTimeMillis();
					DatabaseConnectionResultSet dcrs = null;
					dcrs = stmt.executeQuery(physicalQueries[curIndex], q);
					logger.info("Federated Navigation Subquery:" + (System.currentTimeMillis() - starttime) + "ms:" + physicalQueries[curIndex]);
					list[curIndex] = dcrs;
				} catch (DataUnavailableException e)
				{
					exception1 = e;
				}
				catch (SQLException e)
				{
					exception2 = e;
				} catch (NavigationException e)
				{
					exception3 = e;
				} catch (Exception e)
				{
					exception4 = e;
				}				
			}
		}
	}

	/**
	 * Creates look up lists from dimension resultsets
	 * @author mjani
	 *
	 */
	public class DimensionResultSetProcessorThread extends Thread
	{
		private DatabaseConnectionResultSet[] list;
		private Map<String, Object[]> [] lookupList;
		int [][] fjKeyColumnIndices;
		int curIndex;
		public DimensionResultSetProcessorThread(DatabaseConnectionResultSet[] list, int [][] fjKeyColumnIndices, 
				Map<String, Object[]> [] lookupList)
		{
			this.setName("DimensionResultSetProcessorThread - " + this.getId());
			setDaemon(true); // if the JVM wants to exit due to errors, let it
			this.list = list;
			this.lookupList = lookupList;
			this.fjKeyColumnIndices = fjKeyColumnIndices;
		}
		
		public void run()
		{
			while(true)
			{
				curIndex = startIndex.getAndIncrement();
				if (curIndex >= list.length)
					return;
				DatabaseConnectionResultSet dcrs = list[curIndex];
				lookupList[curIndex] = new HashMap<String, Object[]>();
				try
				{
					long startTime = System.currentTimeMillis();
					while(dcrs.next())
					{
						int colCount = dcrs.getColumnCount();
						Object [] row = new Object[colCount];
						StringBuilder key = new StringBuilder();
						boolean first = true;
						for(int colIdx = 1; colIdx <= colCount; colIdx++)
						{
							Object obj = dcrs.getObject(colIdx);
							row[colIdx - 1] = obj;							
							for(int fjIdx = 0; fjIdx < fjKeyColumnIndices[curIndex].length; fjIdx++)
							{
								if(fjKeyColumnIndices[curIndex][fjIdx] == colIdx)
								{
									if(first)
									{
										first = false;
									}
									else
									{
										key.append(LOOKUP_KEY_SEPARATOR_CHAR);
									}
									key.append(obj!=null ? (obj.toString().isEmpty()? Util.BIRST_EMPTY :obj.toString()) : Util.BIRST_NULL);
								}
							}
						}
						lookupList[curIndex].put(key.toString(), row);
					}
					logger.debug("Dimension lookup created in:" + (System.currentTimeMillis() - startTime) + "ms: index = " + curIndex);
				}
				catch (SQLException e)
				{
					exception2 = e;
				} 
				catch (Exception e)
				{
					exception4 = e;
				}				
			}
		}
	}
	
	/**
	 * Class containing metadata for merging resultsets. The attributes of this class are indices pointing to arrays and maps
	 * used during the merging process.
	 * @author mjani
	 *
	 */
	class MergeReference
	{
		boolean useMeasureResultSet;
		int measureResultSetColumnIndex = -1;
		boolean applyAggregation;
		boolean useLookupList;
		int lookupListIndex = -1;
		int lookupColumnIndex = -1;
		int [] lookupKeyColumnIndices = null;
		boolean skipMeasureRowForMissingLookup;
		boolean addNullMeasuresForAdditionalLookup;
	}
}
