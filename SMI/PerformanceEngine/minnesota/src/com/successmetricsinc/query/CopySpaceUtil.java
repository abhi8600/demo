package com.successmetricsinc.query;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import oracle.sql.TIMESTAMP;

import com.successmetricsinc.Repository;
import com.successmetricsinc.query.Database.DatabaseType;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.warehouse.GenerateSchema;
import com.successmetricsinc.warehouse.StagingColumn;
import com.successmetricsinc.warehouse.StagingTable;

public class CopySpaceUtil 
{

	private static Logger logger = Logger.getLogger(CopySpaceUtil.class);
	
	public static void createTableSQLAsFile(Repository r,DatabaseConnection dconn,String tableName,String databasepath, ResultSetMetaData rsmd,String targetSchema) throws Exception
	{
		BufferedWriter bw =  null;
		StringBuilder sbSQL = null;
		try
		{
			boolean isPADB = DatabaseConnection.isDBTypeParAccel(dconn.DBType);
			boolean isIB = DatabaseConnection.isDBTypeInfoBright(dconn.DBType);
			boolean buildSQLFromMetadata = false;
			if (isPADB)
			{
				Object tableObject = r.findTableByPhysicalName(tableName,dconn);
				if (tableObject!=null)
				{
					if (StagingTable.class.isInstance(tableObject))
					{
						List<String> colNames = new ArrayList<String>();
						List<String> colTypes = new ArrayList<String>();
						List<String> columnCompressionEncodings = ((dconn.DBType == DatabaseType.ParAccel) ? new ArrayList<String>() : null); // only do this for PA, not Redshift (Redshift can auto calculate this)
						List<String> distributionKeys = new ArrayList<String>();
						Set<String> sortKeys = new HashSet<String>();
						StagingTable st = (StagingTable) tableObject;
						Map<DimensionTable, List<StagingColumn>> scmap = st.getStagingColumnMap(r);
						Database.getStagingTableMetaData(r, dconn, st, scmap, colNames, colTypes, columnCompressionEncodings, distributionKeys, sortKeys);
						sbSQL = Database.getCreateTableSQL(r, dconn, tableName.toUpperCase(), colNames, colTypes, null, null, false, columnCompressionEncodings, distributionKeys, sortKeys);
					}
					else
					{
						boolean isMeasureTable = MeasureTable.class.isInstance(tableObject);
						MeasureTable mt = isMeasureTable ? (MeasureTable) tableObject : null;
						DimensionTable dt = isMeasureTable ? null : (DimensionTable) tableObject;
						List<DimensionTable> degenerateDimensions = null;
						if (isMeasureTable)
						{
							degenerateDimensions = mt.getDegenerateDimensionsForMeasureTable(r);
						} 
						GenerateSchema gs = new GenerateSchema(r);
						List<String> columnNames = new ArrayList<String>();
						List<String> columnTypes = new ArrayList<String>();
						List<String> distributionKey = new ArrayList<String>();
						List<String> columnCompressionEncodings = new ArrayList<String>();
						List<Object> colList = gs.getColList(tableObject);
						Set<String> sortKeys = null;
						if (isPADB || isIB)
						{
							sortKeys = isMeasureTable ? gs.getMeasureTableSortKeys(r, (MeasureTable)tableObject, dconn) : gs.getDimensionTableSortKeys(r, (DimensionTable)tableObject, dconn);
						}
						gs.getCreateColumns(tableName, columnNames, columnTypes, columnCompressionEncodings, distributionKey, colList, dconn, degenerateDimensions, dt, isMeasureTable);
						if(isIB && gs.isExceedingMaxPossibleRowSize(tableName, columnTypes,dconn))
						{
							logger.error(tableName + " Exceeding Maximum Possible Row Size for Infobright");
							List<String> levelKeysList = new ArrayList<String>(sortKeys);
							columnTypes = gs.convertVarcharToText(tableName,columnTypes,dconn,levelKeysList,columnNames);
						}
						sbSQL = Database.getCreateTableSQL(r, dconn, tableName.toUpperCase(), columnNames, columnTypes, null, null, false, columnCompressionEncodings, distributionKey, sortKeys);
					}
				}
				else
				{
					buildSQLFromMetadata = true;
				}
			}	  
			else
			{
				buildSQLFromMetadata = true;
			}
			if (buildSQLFromMetadata)
			{
				List<String> columnTypes = new ArrayList<String>();
				List<String> columnNames = new ArrayList<String>();

				int columnCount = rsmd.getColumnCount();

				for (int index=1;index<=columnCount;index++)
				{
					columnNames.add(rsmd.getColumnName(index));
					if (rsmd.getColumnType(index)==Types.VARCHAR || rsmd.getColumnType(index)==Types.NVARCHAR)
					{
						if (DatabaseConnection.isDBTypeOracle(dconn.DBType) && rsmd.getColumnType(index)==Types.VARCHAR)
						{
							columnTypes.add("NVARCHAR2("+rsmd.getColumnDisplaySize(index)+")");
						}
						else
						{
							columnTypes.add(rsmd.getColumnTypeName(index)+"("+rsmd.getColumnDisplaySize(index)+")");
						}
					}
					else if (rsmd.getColumnType(index)==Types.NUMERIC)
					{
						if (DatabaseConnection.isDBTypeOracle(dconn.DBType))
						{
							int numericPrecision = rsmd.getPrecision(index);
							int numericScale = rsmd.getScale(index);
							if (numericScale!=0)
							{
								columnTypes.add("FLOAT");
							}
							else
							{
								columnTypes.add("NUMBER("+numericPrecision+","+numericScale+")");
							}
						}
						else
						{
							columnTypes.add(rsmd.getColumnTypeName(index));
						}
					}
					else
					{
						columnTypes.add(rsmd.getColumnTypeName(index));
					}     
				}

				sbSQL = Database.getCreateTableSQL(r, dconn, tableName, columnNames, columnTypes, null, null, false, null, null, null);
			}
			String createTableSQL = sbSQL.toString();

			if (targetSchema!=null)
			{
				createTableSQL = createTableSQL.replace(dconn.Schema, targetSchema);
			}

			String sqlFileName = Util.replaceStr(databasepath + File.separator + tableName + ".sql", "\\", "/");

			FileWriter fw = new FileWriter(new File(sqlFileName));
			bw = new BufferedWriter(fw);
			bw.write(createTableSQL);
		}
		finally
		{
			if (bw!=null)
				bw.close();
		}
	}

	public static void createTableRowsAsFile(String databasepath,String tableName,long numRows) throws Exception
	{
		BufferedWriter bw =  null;
		try
		{
			String rowCountFile = Util.replaceStr(databasepath + File.separator + tableName + ".rows", "\\", "/");

			FileWriter fw = new FileWriter(new File(rowCountFile));
			bw = new BufferedWriter(fw);
			bw.write(String.valueOf(numRows));
		}
		finally
		{
			if (bw!=null)
				bw.close();
		}
	}

	public static int createTableFormatAsFile(String databasepath,String tableName,ResultSetMetaData rsmd) throws Exception
	{   
		String END_OF_LINE_UNICODE = "\\r\\0\\n\\0";
		String CHARTYPE_UNICODE = "NCharTerm";
		String sep = "|";
		int length = 0;

		PrintWriter writer = null;
		try
		{
			length = rsmd.getColumnCount();

			String formatFileName = Util.replaceStr(databasepath + File.separator + tableName + ".txt.format", "\\", "/");

			writer = new PrintWriter(new FileOutputStream(formatFileName));

			writer.write("<?xml version=\"1.0\"?>\n");
			writer.write("<BCPFORMAT");
			writer.write(" xmlns=\"http://schemas.microsoft.com/sqlserver/2004/bulkload/format\"");
			writer.write(" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n");
			writer.write("\t<RECORD>\n");
			for (int i = 1; i <= rsmd.getColumnCount(); i++)
			{
				writer.write("\t\t<FIELD ID=\"" + i + "\" xsi:type=\"" + CHARTYPE_UNICODE + "\"");
				if (rsmd.getColumnDisplaySize(i)>0 && (rsmd.getColumnType(i) == Types.VARCHAR || rsmd.getColumnType(i) == Types.NVARCHAR))
				{
					writer.write(" MAX_LENGTH=\"" + (rsmd.getColumnDisplaySize(i)*2) + "\""); // length
				}
				else if (rsmd.getColumnDisplaySize(i)>0)
				{
					writer.write(" MAX_LENGTH=\"" + (rsmd.getColumnDisplaySize(i)) + "\""); // length
				}
				// end of line
				if (i == length)
				{
					writer.write(" TERMINATOR=\"" + END_OF_LINE_UNICODE + "\"");
				} else
				{
					writer.write(" TERMINATOR=\"" + sep + "\\0" + "\"");
				}
				writer.write("/>\n");
			}
			writer.write("\t</RECORD>\n");

			writer.write("\t<ROW>\n");

			for (int i = 1; i <= rsmd.getColumnCount(); i++)
			{
				String type = null;
				String precisionScale = "";

				if (rsmd.getColumnType(i) == Types.VARCHAR || rsmd.getColumnType(i) == Types.NVARCHAR)
				{
					type = "SQLNVARCHAR";
				} else if (rsmd.getColumnType(i) == Types.INTEGER || rsmd.getColumnType(i) == Types.BIGINT)
				{
					type = "SQLBIGINT";
				} else if (rsmd.getColumnType(i) == Types.NUMERIC)
				{
					type = "SQLNUMERIC";
					precisionScale = " PRECISION=\"19\" SCALE=\"6\"";
				} else if (rsmd.getColumnType(i) == Types.TIMESTAMP || rsmd.getColumnType(i) == Types.DATE)
				{
					type = "SQLDATETIM8";
				} else if (rsmd.getColumnType(i) == Types.FLOAT || rsmd.getColumnType(i) == Types.DOUBLE)
				{
					type = "SQLFLT8";
				} else
				{
					type = "SQLTINYINT";
				}        

				writer.write("\t\t<COLUMN SOURCE=\"" + i + "\" NAME=\"" + rsmd.getColumnName(i) + "\" xsi:type=\"" + type + "\""
						+ precisionScale + "/>\n");        
			}      
			writer.write("\t</ROW>\n");
			writer.write("</BCPFORMAT>\n");
		}
		finally
		{
			if (writer != null)
				writer.close();
		}
		return length;  
	}

	public static void createTableIndexAsFile(DatabaseConnection dconn,Statement stmt,String tableName,String databasepath,String targetSchema) throws Exception
	{
		BufferedWriter bw =  null;
		ResultSet rsIndex = null;
		try
		{     
			if (!dconn.supportsIndexes())
			{
				return;
			}
			String indexFileName = Util.replaceStr(databasepath + File.separator + tableName + ".index", "\\", "/");
			FileWriter fw = new FileWriter(new File(indexFileName));
			bw = new BufferedWriter(fw);

			StringBuilder strIndexQuery = new StringBuilder();
			boolean isSQL = true;

			if (DatabaseConnection.isDBTypeOracle(dconn.DBType))
			{
				strIndexQuery.append("SELECT INDEX_NAME, COLUMN_POSITION AS COLUMNNUMBER, 0, COLUMN_NAME, TABLE_NAME, INDEX_OWNER FROM DBA_IND_COLUMNS WHERE INDEX_OWNER = '");
				strIndexQuery.append(dconn.Schema).append("' AND TABLE_NAME LIKE '").append(tableName).append("' ");
				isSQL = false;
			}
			else
			{
				strIndexQuery.append("SELECT A.NAME AS INDEX_NAME,B.INDEX_COLUMN_ID AS COLUMNNUMBER,B.IS_INCLUDED_COLUMN AS INCLUDED,");
				strIndexQuery.append("C.NAME AS COLUMN_NAME,D.NAME AS TABLE_NAME,E.NAME AS SCHEMA_NAME FROM ");
				strIndexQuery.append("SYS.INDEXES A INNER JOIN SYS.INDEX_COLUMNS B ON A.INDEX_ID=B.INDEX_ID AND A.OBJECT_ID=B.OBJECT_ID ");
				strIndexQuery.append("INNER JOIN SYS.COLUMNS C ON B.OBJECT_ID=C.OBJECT_ID AND B.COLUMN_ID=C.COLUMN_ID ");
				strIndexQuery.append("INNER JOIN SYS.TABLES D ON A.OBJECT_ID=D.OBJECT_ID ");
				strIndexQuery.append("INNER JOIN SYS.SCHEMAS E ON D.SCHEMA_ID=E.SCHEMA_ID ");
				strIndexQuery.append("WHERE E.NAME='").append(dconn.Schema).append("' AND ");
				strIndexQuery.append("D.NAME LIKE '").append(tableName).append("' AND A.TYPE=2 ORDER BY A.INDEX_ID,B.INDEX_COLUMN_ID");
			}

			rsIndex = stmt.executeQuery(strIndexQuery.toString());

			StringBuilder createIndexSQL = null;
			StringBuilder indexColumns = null;
			StringBuilder includeColumns = null;
			String indexName = null;
			boolean firstColumn = true;
			boolean firstIncludeColumn = true;
			boolean firstRecord = true;

			while (rsIndex.next())
			{
				if (firstRecord)
				{
					firstRecord = false;
					createIndexSQL = new StringBuilder();
					indexName = rsIndex.getString("INDEX_NAME");
					includeColumns = new StringBuilder();
					indexColumns = new StringBuilder();
				}
				if (!rsIndex.getString("INDEX_NAME").equals(indexName))
				{         
					createIndexSQL = new StringBuilder();                         

					createIndexSQL.append("CREATE INDEX ").append(isSQL?"":(targetSchema+".")).append(indexName);
					createIndexSQL.append(" ON ").append(targetSchema!=null?targetSchema:"").append(".").append(tableName);
					createIndexSQL.append(" (").append(indexColumns).append(")");
					if (includeColumns!=null && !(includeColumns.length()==0))
					{
						createIndexSQL.append(" INCLUDE (").append(includeColumns).append(")");
					}
					bw.write(createIndexSQL.toString());
					bw.write(System.lineSeparator());

					indexName = rsIndex.getString("INDEX_NAME");
					firstColumn = true;
					firstIncludeColumn = true;
					includeColumns = new StringBuilder();
					indexColumns = new StringBuilder();
				}
				if (isSQL && rsIndex.getInt("INCLUDED")==1)
				{
					if (firstIncludeColumn)
						firstIncludeColumn = false;
					else
						includeColumns.append(",");
					includeColumns.append(rsIndex.getString("COLUMN_NAME"));
				}
				else
				{
					if (firstColumn)
						firstColumn = false;
					else
						indexColumns.append(",");
					indexColumns.append(rsIndex.getString("COLUMN_NAME"));
				}
			}

			if (!firstRecord)
			{
				createIndexSQL = new StringBuilder();
				createIndexSQL.append("CREATE INDEX ").append(isSQL?"":(targetSchema+".")).append(indexName);
				createIndexSQL.append(" ON ").append(targetSchema!=null?targetSchema:"").append(".").append(tableName);
				createIndexSQL.append(" (").append(indexColumns).append(")");
				if (includeColumns!=null && !(includeColumns.length()==0))
				{
					createIndexSQL.append(" INCLUDE (").append(includeColumns).append(")");
				}
				bw.write(createIndexSQL.toString());
			}
		}
		finally
		{
			if (bw!=null)
				bw.close();
		}
	}

	public static void createTableDataFile(ResultSet rs,ResultSetMetaData rsmd,String sourceSchema,String tableName,String dataQry,boolean append,DatabaseConnection dconn,String fileName) throws Exception
	{
		if (rs!=null)
		{
			FileOutputStream fos = null;
			OutputStreamWriter os = null;
			BufferedWriter writer = null;
			try
			{
				fos = new FileOutputStream(fileName,append);
				os = new OutputStreamWriter(fos,dconn.getBulkLoadCharacterEncoding()); 
				writer = new BufferedWriter(os);

				while (rs.next())
				{
					StringBuilder record = new StringBuilder();
					boolean first = true;
					for (int index=1;index<=rsmd.getColumnCount();index++)
					{           
						if (first)
							first = false;
						else
							record.append("|");
						Object columnObj = rs.getObject(rsmd.getColumnName(index));
						if (columnObj!=null)
						{
							if (rsmd.getColumnType(index)==Types.TIMESTAMP)
							{
								if (DatabaseConnection.isDBTypeOracle(dconn.DBType))
								{
									if (rsmd.getColumnTypeName(index).equals("DATE"))
									{
										record.append(dconn.getOutputDateFormat().format(columnObj));
									}
									else
									{
										if (columnObj!=null)
										{
											record.append(dconn.getOutputDateTimeFormat().format(((TIMESTAMP)columnObj).dateValue()));
										}                    
									}
								}
								else
								{
									record.append(dconn.getOutputDateTimeFormat().format(columnObj));
								}
							}
							else if (rsmd.getColumnType(index)==Types.DATE)
							{
								record.append(dconn.getOutputDateFormat().format(columnObj));
							}
							else if (rsmd.getColumnType(index)==Types.VARCHAR)
							{
								String s = (String) columnObj;
								boolean quote = false;
								for (char c : s.toCharArray())
								{
									if (c == '\r' || c == '\n' || c == '"' || c == '|' || c == '\\')
									{
										quote = true;
										break;
									}
								}
								if (quote)
									record.append('"' + s.replace("\\", "\\\\").replace("\"", "\\\"").replace("|", "\\|") + '"');
								else if (DatabaseConnection.isDBTypeOracle(dconn.DBType))
									record.append('"').append(s).append('"');
								else
									record.append(s);
							}
							else
							{
								record.append(columnObj.toString());
							}
						}
					}             
					writer.write(record.toString());
					if (DatabaseConnection.isDBTypeOracle(dconn.DBType))
					{
						writer.write((byte)'\r');
					}
					writer.write((byte)'\r');
					writer.write((byte)'\n');
				}
			}
			finally
			{
				if (writer!=null)
					writer.close();
				if (os!=null)
					os.close();
				if (fos!=null)
					fos.close();
			}
		}
	}

	public static String getFileContents(String file,boolean appendNewLine) throws IOException 
	{
		File newFile = new File(file);
		if (newFile.exists())
		{
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String line = null;
			StringBuilder stringBuilder = new StringBuilder();

			while ((line = reader.readLine()) != null) 
			{
				stringBuilder.append(line);
				if (appendNewLine)
				{
					stringBuilder.append(System.lineSeparator());
				}
			}

			return stringBuilder.toString();
		}
		else
		{
			return null;
		}
	}
}
