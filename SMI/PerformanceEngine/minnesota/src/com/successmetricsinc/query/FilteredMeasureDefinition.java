/**
 * 
 */
package com.successmetricsinc.query;

import java.io.Serializable;
import java.util.List;
import org.jdom.Element;
import org.jdom.Namespace;
import com.successmetricsinc.Repository;

/**
 * @author Brad Peters
 * 
 */
public class FilteredMeasureDefinition implements Serializable
{
	private static final long serialVersionUID = 1L;
	public String[] Names;
	public QueryFilter[] Filters;
	public Double[] Defaults;
	private String[] strDefaults;

	public FilteredMeasureDefinition(Element e, Namespace ns)
	{
		Names = Repository.getStringArrayChildren(e, "Names", ns);
		@SuppressWarnings("rawtypes")
		List l = e.getChild("Filters", ns).getChildren();
		this.Filters = new QueryFilter[l.size()];
		for (int count = 0; count < l.size(); count++)
		{
			Element de = (Element) l.get(count);
			QueryFilter f = new QueryFilter(de, ns);
			this.Filters[count] = f;
		}
		String[] defs = Repository.getStringArrayChildren(e, "Defaults", ns);
		Defaults = new Double[Names.length];
		strDefaults = new String[Names.length];
		if (defs != null && defs.length > 0)
		{
			for (int i = 0; i < Defaults.length; i++)
			{
				if (defs[i].trim().length() == 0 || defs[i].equalsIgnoreCase("NULL"))
					Defaults[i] = null;
				else
				{
					try
					{
						Defaults[i] = Double.parseDouble(defs[i]);
					} catch (NumberFormatException ex)
					{
					}
				}
				strDefaults [i] = defs[i];
			}
		}
	}
	
	public Element getFilteredMeasureDefElement(Namespace ns)
	{
		Element e = new Element("FilteredMeasures",ns);
		Element child = null;
		
		if (Names!=null && Names.length>0)
		{
			e.addContent(Repository.getStringArrayElement("Names", Names, ns));
		}
		
		if (Filters!=null && Filters.length>0)
		{
			child = new Element("Filters",ns);
			for (QueryFilter qf:Filters)
			{
				child.addContent(qf.getQueryFilterElement("QueryFilter", ns));
			}
			e.addContent(child);
		}
		
		if (strDefaults!=null && strDefaults.length>0)
		{
			e.addContent(Repository.getStringArrayElement("Defaults", strDefaults, ns));
		}
				
		return e;
	}
}
