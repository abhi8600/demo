/**
 * 
 */
package com.successmetricsinc.query;

import java.io.Serializable;

/**
 * @author Brad Peters
 * 
 */
public class GenericColumn implements Serializable
{
	private static final long serialVersionUID = 1L;

	public String Dimension;
	public String Name;
	public String[] Equivalents;

	public GenericColumn()
	{
	}
}
