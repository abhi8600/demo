/**
 * see http://code.google.com/apis/analytics/docs/gdata/gdataReferenceDataFeed.html
 */

package com.successmetricsinc.query;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GoogleAnalyticsQuery
{
	/**
	 * return a query string appropriate for Google Analytics queries
	 * 
	 * @param topn
	 * @param resultProjectionList
	 * @param filters
	 * @param orderBys
	 * @return
	 * @throws SyntaxErrorException
	 */
	public static StringBuilder generateQuery(int topn, List<QueryColumn> resultProjectionList, List<QueryFilter> filters, List<OrderBy> orderBys)
			throws SyntaxErrorException
	{
		StringBuilder dq = new StringBuilder();
		StringBuilder mq = new StringBuilder();
		Set<String> dimensions = new HashSet<String>();
		for (QueryColumn qc : resultProjectionList)
		{
			if (qc.type == QueryColumn.DIMENSION)
			{
				DimensionColumn dc = (DimensionColumn) qc.o;
				if (dq.length() > 0)
					dq.append(',');
				dq.append(dc.PhysicalName);
				dimensions.add(dc.DimensionTable.DimensionName);
			} else if (qc.type == QueryColumn.MEASURE)
			{
				MeasureColumn mc = (MeasureColumn) qc.o;
				if (mq.length() > 0)
					mq.append(',');
				mq.append(mc.PhysicalName);
			}
		}
		StringBuilder query = new StringBuilder();
		if (dq.length() > 0)
			query.append("dimensions=" + dq);
		if (mq.length() > 0)
		{
			if (query.length() > 0)
				query.append('&');
			query.append("metrics=" + mq);
		} else
		{
			if (query.length() > 0)
				query.append('&');
			// Need to add a metric if dimension-only
			if (dimensions.contains("Navigation") && dimensions.size() == 1)
			{
				query.append("metrics=ga:pageviews");
			} else
			{
				query.append("metrics=ga:bounces");
			}
		}
		StringBuilder fq = new StringBuilder();
		for (QueryFilter qf : filters)
		{
			if (fq.length() > 0)
				fq.append(';');
			fq.append(qf.getGAFilterString());
		}
		if (fq.length() > 0)
		{
			if (query.length() > 0)
				query.append('&');
			query.append("filters=" + fq);
		}
		StringBuilder oq = new StringBuilder();
		for (OrderBy ob : orderBys)
		{
			if (oq.length() > 0)
				oq.append(',');
			oq.append(ob.getGAOrderByString());
		}
		if (oq.length() > 0)
		{
			if (query.length() > 0)
				query.append('&');
			query.append("sort=" + oq);
		}
		if (topn >= 0)
		{
			// 0 means all, so change to 1
			if (topn == 0)
				topn = 1;
			if (query.length() > 0)
				query.append('&');
			query.append("max-results=" + topn);
		}
		return query;
	}
}
