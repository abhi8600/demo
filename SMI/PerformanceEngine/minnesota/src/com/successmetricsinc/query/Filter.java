/**
 * 
 */
package com.successmetricsinc.query;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.jdom.Element;

import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class Filter implements Cloneable {
	private static final String VALUE = "Value";
	private static final String COLUMN_NAME = "ColumnName";
	private static final String FILTER_TYPE = "FilterType";
	private static final String LOGICAL_OPERATOR = "LogicalOperator";
	private static final String OPERATOR = "Operator";
	public static final String DATA = "DATA";
	public static final String DISPLAY = "DISPLAY";
	public enum FilterType {
		DISPLAY, DATA
	};
	
	private String Operator;
	private List<String> values;
	private String LogicalOperator;
	private FilterType filterType = FilterType.DISPLAY;
	private String columnName;
	// bug9508
	private boolean createFiltersFrom = true;
	private String clazz = null;
	
	private String promptType = null;
	
	public Filter(String o, List<String>v, String lo, FilterType ft, String cn) {
		Operator = o;
		values = v;
		LogicalOperator = lo;
		filterType = ft;
		setColumnName(cn);
	}
	public Filter(String o, List<String>v, String lo, FilterType ft, String cn, boolean create, String clazz) {
		Operator = o;
		values = v;
		LogicalOperator = lo;
		filterType = ft;
		setColumnName(cn);
		createFiltersFrom = create;
		this.clazz = clazz;
	}
	public Filter(String o, List<String>v, String lo, FilterType ft, String cn, boolean create, String clazz,String promptType) {
		Operator = o;
		values = v;
		LogicalOperator = lo;
		filterType = ft;
		setColumnName(cn);
		createFiltersFrom = create;
		this.clazz = clazz;
		this.promptType = promptType;
	}
	
	public Filter() {
	}
	
	public String getOperator() {
		return Operator;
	}
	public void setOperator(String operator) {
		Operator = operator;
	}
	public List<String> getValues() {
		return values;
	}
	public void setValues(List<String> values) {
		this.values = values;
	}
	public String getClazz() {
		return clazz;
	}	
	public String getLogicalOperator() {
		return LogicalOperator;
	}
	public void setLogicalOperator(String logicalOperator) {
		LogicalOperator = logicalOperator;
	}
	public FilterType getFilterType() {
		return filterType;
	}
	public void setFilterType(FilterType filterType) {
		this.filterType = filterType;
	}
	/**
	 * @param columnName the columnName to set
	 */
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	/**
	 * @return the columnName
	 */
	public String getColumnName() {
		return columnName;
	}
	public Element getElement() {
		Element el = new Element("Filter");
		XmlUtils.addContent(el, OPERATOR, Operator, null);
		XmlUtils.addContent(el, LOGICAL_OPERATOR, LogicalOperator, null);
		XmlUtils.addContent(el, FILTER_TYPE, filterType == FilterType.DATA ? DATA : DISPLAY, null);
		XmlUtils.addContent(el, COLUMN_NAME, columnName, null);
		List<Element> list = new ArrayList<Element>();
		for (String s : values) {
			Element e = new Element(VALUE);
			e.setText(s);
			list.add(e);
		}
		el.addContent(list);
		return el;
	}
	
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		OMElement filter = fac.createOMElement("Filter", ns);
		XmlUtils.addContent(fac, filter, OPERATOR, Operator, ns);
		XmlUtils.addContent(fac, filter, LOGICAL_OPERATOR, LogicalOperator, ns);
		XmlUtils.addContent(fac, filter, FILTER_TYPE, filterType == FilterType.DATA ? DATA : DISPLAY, ns);
		XmlUtils.addContent(fac, filter, COLUMN_NAME, columnName, ns);
		for (String s : values) {
			XmlUtils.addContent(fac, filter, VALUE, s, ns);
		}
		parent.addChild(filter);
	}
	
	
	public void parseElement(OMElement parent) {
		values = new ArrayList<String>();
		for (Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String name = el.getLocalName();
			
			if (OPERATOR.equals(name)) 
				Operator = XmlUtils.getStringContent(el);
			else if (LOGICAL_OPERATOR.equals(name))
				LogicalOperator = XmlUtils.getStringContent(el);
			else if (FILTER_TYPE.equals(name))
				filterType = (DATA.equals(XmlUtils.getStringContent(el)) ? FilterType.DATA : FilterType.DISPLAY);
			else if (COLUMN_NAME.equals(name))
				columnName = XmlUtils.getStringContent(el);
			else if (VALUE.equals(name)) {
				String v = XmlUtils.getStringContent(el);
				if ("null".equals(v))
					v = null;
				values.add(v);
			}
		}
		
		if (values.size() == 1 && values.get(0) == null) {
			Operator = QueryFilter.ISNULL; 
		}
	}
	
	public boolean isEmpty() {
		List<String> vs = getValues();
		if (vs == null || vs.isEmpty())
			return true;
		
		String v = vs.get(0);
		if (vs.size() == 1 && (v == null || v.equals(QueryString.NO_FILTER_TEXT)))
			return true;

		return false;
	}
	/**
	 * @return the createFiltersFrom
	 */
	public boolean isCreateFiltersFrom() {
		return createFiltersFrom;
	}
	
	public void setCreateFiltersFrom(boolean createFiltersFrom){
		this.createFiltersFrom = createFiltersFrom;
	}
	
	public boolean requiresAValue() {
		if (QueryFilter.ISNOTNULL.equals(Operator) || QueryFilter.ISNULL.equals(Operator))
			return false;
		
		return true;
	}
	public void normalize() {
		List<String> vs = getValues();
		if (vs == null || vs.isEmpty()) {
			setValues(null);
			return;
		}
		
		List<String> newValues = new ArrayList<String>();
		for (int i = 0; i < vs.size(); i++) {
			String s = vs.get(i);
			if ((! QueryString.NO_FILTER_TEXT.equals(s)) && !newValues.contains(s))
				newValues.add(s);
		}
		setValues(newValues.isEmpty() ? null : newValues);
	}
	
	@Override
	public Filter clone() throws CloneNotSupportedException 
	{	
		Filter objNewFilter = new Filter(Operator,values,LogicalOperator,filterType,columnName,createFiltersFrom,clazz,promptType);
		return objNewFilter;
	}
	public String getPromptType() {
		return promptType;
	}
	public void setPromptType(String promptType) {
		this.promptType = promptType;
	}
}
