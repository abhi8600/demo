/**
 * $Id: DisplayExpression.java,v 1.174 2012/11/16 21:50:03 BIRST\ricks Exp $
 *
 * Copyright (C) 2007-2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import gnu.jel.CompiledExpression;
import gnu.jel.Evaluator;
import gnu.jel.Library;

import java.io.IOException;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.antlr.runtime.tree.Tree;
import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.query.Aggregation.AggregationType;
import com.successmetricsinc.query.Database.DatabaseType;
import com.successmetricsinc.query.olap.OlapMemberExpression;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.Operator.Type;
import com.successmetricsinc.transformation.Expression;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.Statement;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.transformation.object.Groups;
import com.successmetricsinc.transformation.object.IIF;
import com.successmetricsinc.transformation.object.SetLookup;
import com.successmetricsinc.transformation.object.Transform;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.RepositoryException;

/**
 * Class to encapsulate an expression contained within a query string
 * 
 * @author bpeters
 * 
 */
public class DisplayExpression implements Comparable<DisplayExpression>, Cloneable
{
	private static Logger logger = Logger.getLogger(DisplayExpression.class);
	// basic units
	private static final String DIM_NAME = "[\\p{L}0-9_ %#$:;()/-]+";
	private static final String COL_NAME = "[\\p{L}0-9_ %#$:;()/-]+";
	private static final String MEC_MEASURE_NAME_REGEXP = "[\\p{L}0-9_ #$%:;()/\\-\\+\\.&]+";
	private static final String FDC_OPERATORS = "([=<>]|<>|<=|>=| NOT LIKE | LIKE | NOT IN | IN | IS NULL| IS NOT NULL)";
	private static final String FDC_OPERANDS = "[\\p{L}%0-9_ #@$'\":\\./\\(\\)\\-,\\|&\u2013]+";
	private static final String POS_RHS = "([-+]?[=][\\p{L}0-9_ #@$:/-]*)?";
	private static final String MEC_TYPE_REGEXP = "M(DRANK|RANK|PTILE)?";
	private static final String ATTRIBUTE_NAME = DIM_NAME + "[.]" + COL_NAME;
	private static final String POS_REGEXP = "\\[" + ATTRIBUTE_NAME + POS_RHS + "\\]";
	private static final String DC_PATTERN = "DC\\{" + ATTRIBUTE_NAME + "\\}";
	private static final String MEC_MEASURE_DIM_FILTER_REGEXP = "FDC\\{" + ATTRIBUTE_NAME + FDC_OPERATORS + FDC_OPERANDS + "\\}";
	private static final String MEC_AGG_BY_REGEXP = "[,]" + ATTRIBUTE_NAME;
	private static final String MEC_POS_CALC_REGEXP = "(\\{" + MEC_MEASURE_NAME_REGEXP + "(" + MEC_AGG_BY_REGEXP + ")*\\}(" + POS_REGEXP + ")*)";
	private static final String MEC_MEASURE_COMPOUND_FILTER_REGEXP = "(" + MEC_MEASURE_DIM_FILTER_REGEXP + "|(FAND|FOR)\\{([,]?"
			+ MEC_MEASURE_DIM_FILTER_REGEXP + ")+\\})";
	private static final String MEC_MEASURE_FILTER_REGEXP = "[,]" + MEC_MEASURE_COMPOUND_FILTER_REGEXP;
	private static final String MEC_FILTERED_MEASURE_REGEXP = "(\\{" + MEC_MEASURE_NAME_REGEXP + "(" + MEC_MEASURE_FILTER_REGEXP + ")?\\})";
	private static final String MEC_REGEXP = MEC_TYPE_REGEXP + "(" + MEC_POS_CALC_REGEXP + "|" + MEC_FILTERED_MEASURE_REGEXP + "(" + POS_REGEXP + ")*" + ")";
	// Measure expression format
	private static final Pattern p = Pattern.compile(MEC_REGEXP);
	// Dimension column format
	private static final Pattern dcp = Pattern.compile(DC_PATTERN);
	// Positional calculation suffix
	private static final Pattern posp = Pattern.compile(POS_REGEXP);
	public static final byte MEASURE_TYPE = 0;
	public static final byte DIMENSION_TYPE = 1;
	public static final byte OLAP_TYPE = 2;
	private TransformationScript script;
	public boolean containsAggregateExpression;
	public transient Map<Thread, DisplayExpression> ClonedChildren;
	
	public enum PositionType
	{
		Absolute, RelativePlus, RelativeMinus, DimensionExpression
	}

	public enum MeasureExpressionType
	{
		Normal, Rank, Percentile, DenseRank
	}
	private String name;
	private String expression;
	private int position;
	private boolean rank;
	private boolean denserank;
	private boolean percentile;
	private List<String> groups;
	List<MeasureExpressionColumn> measureExpressionColumns;
	List<DimensionExpressionColumn> dimensionExpressionColumns;
	List<OlapMemberExpression> olapMemberExpressionColumns;
	private transient Object compiledExpression;
	private transient FieldProvider fields;
	private transient FieldProvider parentFields;
	private transient Object[] context;
	private transient boolean aggregatable;
	private transient Operator compiledOperator;
	private transient ResultSetCache rsc;
	private transient Map<DimensionColumn, Groups> customGroups = new HashMap<DimensionColumn, Groups>();
	private transient boolean isColumn = false;
	private transient String columnDimension;
	private transient String columnName;
	private transient Transform transform;
	private transient boolean rsum;
	private transient boolean dontProcess;
	public transient boolean resetOperatorItems;
	
	public static boolean jelSupported = false;
	
	static
	{
		try
		{
			Class.forName("gnu.jel.CompiledExpression");
			jelSupported = true;
		}
		catch (ClassNotFoundException ex)
		{
			logger.warn("Expressions in the old query language are not supported in this deployment");
			jelSupported = false;
		}
	}

	public DisplayExpression()
	{
	}
	
	public DisplayExpression(Repository r, String expr, Session session, boolean isOlapExpression) throws NullPointerException, BaseException, CloneNotSupportedException
	{
		setExpression(r, expr, session, false, isOlapExpression);
	}

	public DisplayExpression(Repository r, String expr, Session session) throws NullPointerException, BaseException, CloneNotSupportedException
	{
		setExpression(r, expr, session, false);
	}

	public Object clone()
	{
		DisplayExpression de = new DisplayExpression();
		de.name = name;
		de.expression = expression;
		de.position = position;
		de.rank = rank;
		de.denserank = denserank;
		de.percentile = percentile;
		de.groups = groups;
		de.dimensionExpressionColumns = dimensionExpressionColumns;
		de.compiledExpression = compiledExpression;
		de.fields = fields;
		de.context = context;
		de.aggregatable = aggregatable;
		if (measureExpressionColumns != null)
			de.measureExpressionColumns = new ArrayList<MeasureExpressionColumn>();
		if (olapMemberExpressionColumns != null)
			de.olapMemberExpressionColumns = new ArrayList<OlapMemberExpression>();
		if (compiledOperator != null && script != null)
		{
			synchronized(this)
			{
				script.setDisplayExpression(de);
				de.compiledOperator = compiledOperator.clone();
				script.setDisplayExpression(this);
			}
		}
		de.rsum = rsum;
		if (this.ClonedChildren == null)
			this.ClonedChildren = new HashMap<Thread, DisplayExpression>();
		this.ClonedChildren.put(Thread.currentThread(), de);
		return de;
	}

	public void setExpression(String expression)
	{
		this.expression = expression;
	}

	public void initialize()
	{
		measureExpressionColumns = new ArrayList<MeasureExpressionColumn>();
		dimensionExpressionColumns = new ArrayList<DimensionExpressionColumn>();
		olapMemberExpressionColumns = new ArrayList<OlapMemberExpression>();
	}

	public void setLogicalExpression(String expression, TransformationScript ts, Operator compiledOperator, Tree column)
	{
		this.expression = expression;
		this.compiledOperator = compiledOperator;
		this.script = ts;
	}

	/**
	 * 
	 * @param r
	 * @param expression
	 * @throws DisplayExpressionException
	 * @throws NavigationException
	 * @throws CloneNotSupportedException
	 * @throws SyntaxErrorException
	 */
	public void setExpression(Repository r, String expression, Session repSession, boolean usesOldSyntax) throws BaseException, CloneNotSupportedException,
	NullPointerException
	{
		setExpression(r, expression, repSession, usesOldSyntax, false);
	}
	
	public void setExpression(Repository r, String expression, Session repSession, boolean usesOldSyntax, boolean isOlapExpression) throws BaseException, CloneNotSupportedException,
			NullPointerException
	{
		// get rid of newlines, it is easy to enter them in the dialog box in adhoc
		if ((usesOldSyntax || !r.isUseNewQueryLanguage()) && expression != null)
			expression = expression.replace("\n", "");
		else if (expression != null && expression.toLowerCase().indexOf("mdxexpression(") >= 0)
			expression = expression.replace("\n", "");
		this.expression = expression;
		expression = r.replaceVariables(repSession, expression); // replace the variables
		// See if this is the new logical query language
		if (!usesOldSyntax && r.isUseNewQueryLanguage())
		{
			
			LogicalQueryString.setupDisplayExpression(r, repSession, expression, this, isOlapExpression);
			return;
		}
		if (expression.toLowerCase().startsWith("alt:"))
		{
			expression = expression.substring(4);
			LogicalQueryString.setupDisplayExpression(r, repSession, expression, this, isOlapExpression);
			return;
		}	
		else if (expression.toLowerCase().startsWith("savedexpression('")) {
			LogicalQueryString.setupDisplayExpression(r, repSession, expression, this, isOlapExpression);
			return;
		}
		if (!jelSupported)
		{
			logger.warn("Old query language expressions are not supported in this deployment: " + expression);
			throw new SyntaxErrorException("Old query language expressions are not supported in this deployment: " + expression);
		}
		/*
		 * Match either the regular syntax: M{$ Value}, MRANK{$ Value}, MDRANK{$ Value}, MPTILE{$ Value} or the
		 * positional calculation syntax: M{$ Value}[Time.Month-=1], M{$ Value}[Time.Month=2005 04] (relative or
		 * absolute positions). An absolute position can omit the = relation to refer to self. This is useful for
		 * referring to higher levels: DC{Time.Month},EVAL{M{$Value}/M{$ Value}[Time.Year}}. Multiple positions can also
		 * be used: M{$ Sales}[Time.Year][Product.Category]. Also supports groupings for rank and ptile: MRANK{$
		 * Value,Group A, Group B}
		 */
		Matcher m = p.matcher(expression);
		measureExpressionColumns = new ArrayList<MeasureExpressionColumn>();
		StringBuilder newExp = new StringBuilder();
		int last = 0;
		while (m.find())
		{
			String s = m.group();
			newExp.append(expression.substring(last, m.start()));
			newExp.append("getDoubleProperty(");
			newExp.append(MEASURE_TYPE);
			newExp.append(",(int)");
			newExp.append(measureExpressionColumns.size());
			newExp.append(')');
			last = m.end();
			MeasureExpressionColumn mec = new MeasureExpressionColumn();
			mec.expression = s;
			mec.positions = new ArrayList<Position>();
			measureExpressionColumns.add(mec);
		}
		newExp.append(expression.substring(last));
		expression = newExp.toString();
		newExp = new StringBuilder();
		last = 0;
		dimensionExpressionColumns = new ArrayList<DimensionExpressionColumn>();
		m = dcp.matcher(expression);
		dimensionExpressionColumns = new ArrayList<DimensionExpressionColumn>();
		List<String> unrecognizedColumns = new ArrayList<String>();
		while (m.find())
		{
			String s = m.group();
			DimensionExpressionColumn dec = new DimensionExpressionColumn();
			int pos = s.indexOf('.');
			dec.dimName = s.substring(3, pos);
			dec.colName = s.substring(pos + 1, s.length() - 1);
			DimensionColumn dc = r.findDimensionColumn(dec.dimName, dec.colName);
			newExp.append(expression.substring(last, m.start()));
			if (dc != null)
			{
				if (dc.DataType.equals("Varchar")) 
					newExp.append("getStringProperty(");
				else
					newExp.append("getDoubleProperty(");
				newExp.append(DIMENSION_TYPE);
				newExp.append(",(int)");
				newExp.append(dimensionExpressionColumns.size());
				newExp.append(')');
				last = m.end();
			} else
			{
				unrecognizedColumns.add(s);
			}
			dimensionExpressionColumns.add(dec);
		}
		newExp.append(expression.substring(last));
		expression = newExp.toString();
		String compileExpression = expression;
		for (MeasureExpressionColumn mec : measureExpressionColumns)
		{
			int measureStart = 2;
			if (mec.expression.startsWith("MDRANK"))
			{
				measureStart = 7;
				mec.type = MeasureExpressionType.DenseRank;
			} else if (mec.expression.startsWith("MRANK"))
			{
				measureStart = 6;
				mec.type = MeasureExpressionType.Rank;
			} else if (mec.expression.startsWith("MPTILE"))
			{
				measureStart = 7;
				mec.type = MeasureExpressionType.Percentile;
			} else
				mec.type = MeasureExpressionType.Normal;
			/*
			 * Process any positional references
			 */
			m = posp.matcher(mec.expression);
			while (m.find())
			{
				String s = m.group();
				Position pos = new Position();
				pos.ptype = PositionType.Absolute;
				int eqpos = s.indexOf('=');
				/*
				 * If a position is referenced with no value (no equals or relative), then use absolute positioning
				 * relative to current value
				 */
				int endeq = eqpos;
				if (eqpos < 0)
				{
					pos.ptype = PositionType.Absolute;
					pos.pVal = null;
					endeq = s.length() - 1;
				} else
				{
					if (s.charAt(eqpos - 1) == '-')
					{
						pos.ptype = PositionType.RelativeMinus;
						endeq--;
					} else if (s.charAt(eqpos - 1) == '+')
					{
						pos.ptype = PositionType.RelativePlus;
						endeq--;
					} else
					{
						pos.ptype = PositionType.Absolute;
					}
					pos.pVal = s.substring(eqpos + 1, s.length() - 1);
				}
				if (pos.pVal != null)
					try
					{
						Double d = Double.valueOf((String) pos.pVal);
						pos.pVal = d;
					} catch (Exception ex)
					{
						if (pos.ptype != PositionType.Absolute)
						{
							// Unable to process a relative value so throw an exception
							throw (new SyntaxErrorException("Syntax error: unable to parse relative position", ex));
						}
					}
				int ppos = s.indexOf('.');
				pos.pDim = s.substring(1, ppos);
				pos.pCol = s.substring(ppos + 1, endeq);
				mec.positions.add(pos);
			}
			int measureEnd = mec.expression.length() - 1;
			// Find end of measure name
			int cpos = mec.expression.indexOf('[', measureStart);
			if (cpos > 0)
			{
				measureEnd = cpos - 1;
			}
			cpos = mec.expression.indexOf(",F", measureStart);
			if ((cpos > 0) && (cpos < measureEnd))
			{
				measureEnd = cpos;
			}
			mec.columnName = mec.expression.substring(measureStart, measureEnd);
			// Process measure filters
			int start = mec.expression.indexOf(",F");
			if (start > 0)
			{
				start++;
				// Parse measure filter
				int end = mec.expression.lastIndexOf('}') - 1;
				int tokenEnd = mec.expression.indexOf('{', start);
				String measureFilter = mec.expression.substring(tokenEnd + 1, end);
				Token t = new Token();
				t.body = measureFilter;
				t.type = mec.expression.substring(start, tokenEnd);
				QueryFilter qf = null;
				try
				{
					qf = QueryString.returnFilter(t, r);
				} catch (SyntaxErrorException e)
				{
					// Unable to process so throw an exception
					logger.warn("Syntax error: measure filter in display expression" + e);
					// throw (new DisplayExpressionException("Syntax error: measure filter in display expression", e));
				}
				mec.columnName = mec.expression.substring(measureStart, start - 1);
				if (qf != null)
				{
					mec.filter = qf;
				}
				mec.displayName = mec.columnName + (mec.filter != null ? Math.abs(mec.filter.getQueryKeyStr(mec.query, null).hashCode()) : "");
			}
			if (mec.displayName == null)
				mec.displayName = mec.columnName;
			if (mec.type != MeasureExpressionType.Normal)
				mec.displayName += mec.type == MeasureExpressionType.Rank ? "_RANK" : mec.type == MeasureExpressionType.DenseRank ? "_DRANK" : "_PTILE";
			// Look for groupings
			String[] strs = mec.columnName.split(",");
			if (strs.length > 1)
			{
				mec.groups = new ArrayList<String>(strs.length);
				for (int i = 1; i < strs.length; i++)
					mec.groups.add(strs[i].trim());
				mec.columnName = strs[0];
				if (mec.type != MeasureExpressionType.Normal)
					mec.displayName += mec.type == MeasureExpressionType.Rank ? "_GRANK" : mec.type == MeasureExpressionType.DenseRank ? "_GDRANK" : "_GPTILE";
			}
			if (mec.displayName != null)
				mec.displayName = mec.displayName.replace(",", "_");
			mec.setMeasureColumn(r.findMeasureColumn(mec.columnName));
			MeasureColumn mc = mec.getMeasureColumn();
			if (mc != null)
			{
				DatabaseType dbType = null;
				if (mc.MeasureTable != null && mc.MeasureTable.TableSource != null && mc.MeasureTable.TableSource.connection != null)
				{
					dbType = mc.MeasureTable.TableSource.connection.DBType;
				}
				if (dbType == null || (mec.displayName != null && mec.displayName.length() > DatabaseConnection.getIdentifierLength(dbType)))
				{
					mec.originalDisplayName = mec.displayName;
					mec.displayName = DatabaseConnection.NCHAR + mec.displayName.hashCode();					
				}
			} else
			{
				unrecognizedColumns.add(mec.columnName);
			}
		}
		// this is the point at which bad column names are identified
		if (!unrecognizedColumns.isEmpty())
		{
			throw new BadColumnNameException("Unrecognized column(s) in expression: " + unrecognizedColumns);
		}
		try
		{
			Library lib = new Library(new Class[]
			{ Math.class, String.class, StringProcess.class, Double.class, Integer.class, Long.class, URLEncoder.class }, new Class[]
			{ FieldProvider.class }, new Class[]
			{ Math.class, String.class, Double.class, Integer.class, Long.class }, null, null);
			compiledExpression = Evaluator.compile(compileExpression, lib);
		} catch (Exception ex)
		{
			throw new SyntaxErrorException("Unable to evaluate old query language expression: " + this.expression, ex);
		}
	}

	/**
	 * 
	 * @param q
	 * @param dimName
	 * @param colName
	 * @param addedNames
	 * @param ptype
	 * @return boolean success flag
	 * @throws NavigationException
	 * @throws BadColumnNameException
	 */
	public boolean addDimensionColumn(Query q, String dimName, String colName, List<String> addedNames, PositionType ptype, boolean sparse) throws NavigationException,
			BadColumnNameException
	{
		DimensionColumn dc = q.getRepository().findDimensionColumn(dimName, colName);
		if (dc == null)
		{
			throw new BadColumnNameException("Unrecognized column: " + dimName + "." + colName);
		} else if (ptype != null && (ptype == PositionType.RelativeMinus || ptype == PositionType.RelativePlus)
				&& !(dc.DataType.equals("Number") || dc.DataType.equals("Integer")))
		{
			throw new BadColumnNameException("Cannot use non-numeric column for relative position calculations: " + dimName + "." + colName);
		}
		boolean found = false;
		for (DimensionColumnNav dcn : q.dimensionColumns)
		{
			if (dcn.dimensionName.equals(dimName) && dcn.displayName.equals(colName))
			{
				found = true;
				dcn.sparse = sparse;
				if(dcn.colIndex < 0 || dcn.navigateonly)
				{
					dcn.navigateonly = false;
					dcn.colIndex = q.colIndex++;
					q.addGroupBy(new GroupBy(dimName, colName));
					if (addedNames != null)
						addedNames.add(colName);					
				}
				break;
			}
		}
		if (!found)
		{
			DimensionColumnNav dcn = q.addDimensionColumn(dimName, colName);
			dcn.navigateonly = false;
			dcn.sparse = sparse;
			q.addGroupBy(new GroupBy(dimName, colName));
			if (addedNames != null)
				addedNames.add(colName);
			return (true);
		}
		
		return (false);
	}

	/**
	 * Extract columns from an expression list into a query if they are needed. Returns whether an expression was
	 * extracted or not
	 * 
	 * @param q
	 * @param addedNames
	 * @throws NavigationException
	 * @throws DisplayExpressionException
	 */
	public boolean extractAllExpressions(Query q, List<String> addedNames) throws NavigationException, CloneNotSupportedException, BadColumnNameException
	{
		if (measureExpressionColumns == null && dimensionExpressionColumns == null && olapMemberExpressionColumns == null)
			return false;
		boolean result = false;
		if (measureExpressionColumns != null)
		{
			List<MeasureExpressionColumn> clonedList = new ArrayList<MeasureExpressionColumn>(measureExpressionColumns);
			for (MeasureExpressionColumn mec : clonedList)
			{
				boolean found = false;
				if (mec.type == MeasureExpressionType.Normal)
				{
					for (MeasureColumnNav mcn : q.measureColumns)
					{
						if (!mcn.hidden && !mcn.navigateonly && mcn.measureName.equals(mec.columnName) && mcn.displayName.equals(mec.displayName) 
								&& ((mcn.filter == null && mec.filter == null) || (mcn.filter != null && mec.filter != null && mcn.filter.equals(mec.filter))))
						{
							found = true;
							// Make sure column isn't being aggregated
							for (Aggregation agg : q.getAggregations())
							{
								if (agg.getColumnName().equals(mec.columnName))
								{
									found = false;
									break;
								}
							}
							if (found)
								break;
						}
					}
					if (!found)
						for (MeasureColumnNav mcn : q.derivedMeasureColumns)
						{
							if (!mcn.hidden && !mcn.navigateonly && mcn.measureName.equals(mec.columnName))
							{
								found = true;
								// Make sure column isn't being aggregated
								for (Aggregation agg : q.getAggregations())
								{
									if (agg.getColumnName().equals(mec.columnName))
									{
										found = false;
										break;
									}
								}
								if (found)
									break;
							}
						}
				}
				boolean isDimensionExpression = false;
				Query mecq = q;
				Query mecbq = null;
				if (mec.positions.size() > 0)
				{
					// Make sure that the position columns aren't found
					boolean pfound = true;
					for (Position pos : mec.positions)
					{
						if (pos.ptype == PositionType.DimensionExpression)
						{
							isDimensionExpression = true;
							pfound = false;
						}
						else if (pos.ptype == PositionType.Absolute)
						{
							pfound = false;
						}
						boolean pfound2 = false;
						for (DimensionColumnNav dcn : q.dimensionColumns)
						{
							if (!dcn.navigateonly && !dcn.constant && dcn.dimensionName.equals(pos.pDim) && dcn.columnName.equals(pos.pCol))
							{
								pfound2 = true;
								break;
							}
						}
						if (!pfound2)
						{
							/*
							 * If it's a relative position then add this column to the parent query
							 */
							if (pos.ptype == PositionType.RelativeMinus || pos.ptype == PositionType.RelativePlus)
							{
								addDimensionColumn(q, pos.pDim, pos.pCol, addedNames, pos.ptype, false);
							} else
								pfound = false;
							break;
						}
					}
					if (!pfound)
					{
						/*
						 * If the position columns aren't found, or an absolute position is required (which may require
						 * aggregation), then a new measure expression query is is required
						 */
						mecq = null;
					}
					if (mecq == null)
					{
						if (isDimensionExpression)
						{
							extractDimensionalExpression(mec, q, addedNames);
						} else
						{
							buildMeasureExpressionQueries(mec, q);
							mecq = mec.query;
							mecbq = mec.bridgeQuery;
						}
					}
				}
				/*
				 * Now add the positions to both queries. If, however, the position column name is the special reserved
				 * word "Total", then don't add anything. This way, all columns within this dimension will be removed
				 * allowing for aggregation at the grand total level in that dimension
				 */
				for (Position pos : mec.positions)
				{
					if (pos.dimensionExpression == null && !pos.pCol.equals("Total"))
					{
						addDimensionColumn(mecq, pos.pDim, pos.pCol, mecq == q ? addedNames : null, pos.ptype, false);
						if (mecbq != null)
						{
							addDimensionColumn(mecbq, pos.pDim, pos.pCol, null, null, false);
						}
					}
				}
				/*
				 * If there are more than one dimensions on the bridge query, add the measure column so that it doesn't
				 * result in a cartesian product.
				 */
				if (mecbq != null && mecbq.dimensionColumns != null && mecbq.dimensionColumns.size() > 1)
				{
					if (mecbq.hasMoreThanOneDimension() && (!mecbq.containsMeasureColumn(mec.columnName)))
					{
						mecbq.addMeasureColumn(mec.columnName, mec.displayName, false);
					}
					if (mecbq.measureColumns != null && mecbq.measureColumns.size() > 1)
					{
						mecbq.setFullOuterJoin(true);
					}
				}
				/*
				 * Add the measure column if it wasn't found
				 */
				if (!found)
				{
					if (mec.type == MeasureExpressionType.Rank)
					{
						Aggregation agg = new Aggregation(AggregationType.Rank, mec.displayName, -1);
						agg.setGroups(mec.groups);
						mecq.addAggregation(agg);
					} else if (mec.type == MeasureExpressionType.DenseRank)
					{
						Aggregation agg = new Aggregation(AggregationType.DenseRank, mec.displayName, -1);
						agg.setGroups(mec.groups);
						mecq.addAggregation(agg);
					} else if (mec.type == MeasureExpressionType.Percentile)
					{
						Aggregation agg = new Aggregation(AggregationType.Percentile, mec.displayName, -1);
						agg.setGroups(mec.groups);
						mecq.addAggregation(agg);
					}
					if (mecq != null)
					{
						if (mec.filter == null)
							mecq.addMeasureColumn(mec.columnName, mec.displayName, false);
						else
							mecq.addMeasureColumn(mec.columnName, mec.displayName, false, mec.filter);
					}
					/**
					 * For queries that have more than one dimension and only positional calc expression, the main query
					 * could end up in dimensional cross joins. So, detect such scenario and add the measure from the
					 * first display expression.
					 */
					if (mecq != q && q.hasMoreThanOneDimension() && (q.measureColumns == null || q.measureColumns.isEmpty()))
					{
						q.addMeasureColumn(mec.columnName, mec.displayName, false);
						addedNames.add(mec.displayName);
					}
					if (mecq == q && addedNames != null)
						addedNames.add(mec.displayName);
					
					if(mecq != null && mecq != q && (mecq.measureColumns != null) && (mecq.measureColumns.size() > 1))
					{
						mecq.setFullOuterJoin(true);
					}
					result = true;
				}
			}
		}
		if (dimensionExpressionColumns != null)
			for (DimensionExpressionColumn dec : dimensionExpressionColumns)
			{
				result = addDimensionColumn(q, dec.dimName, dec.colName, addedNames, null, dec.sparse);
			}
		if (olapMemberExpressionColumns != null)
		{
			for (OlapMemberExpression exp : olapMemberExpressionColumns)
			{
				boolean found = false;
				if (q.olapMemberExpressions != null)
				{
					for (OlapMemberExpression qexp : q.olapMemberExpressions)
					{
						if (qexp.expression.equals(exp.expression))
						{
							found = true;
							break;
						}
					}
				}
				if (!found)
				{
					exp.colIndex = q.colIndex++;
					q.olapMemberExpressions.add(exp);
				}
			}				
		}
		return result;
	}
	
	private boolean extractDimensionalExpression(MeasureExpressionColumn mec, Query q, List<String> addedNames)
	{
		try
		{
			mec.dimensionExpressionQuery = (Query) q.clone();
			/*
			 * Remove other measure columns so that the query only has to hit one measure table
			 */
			List<MeasureColumnNav> delList = new ArrayList<MeasureColumnNav>();
			List<GroupBy> gbDelList = new ArrayList<GroupBy>();
			List<OrderBy> obDelList = new ArrayList<OrderBy>();
			for (MeasureColumnNav mcn : mec.dimensionExpressionQuery.measureColumns)
			{
				if (mec.columnName.equals(mcn.measureName))
					continue;
				delList.add(mcn);
				for (GroupBy gb : mec.dimensionExpressionQuery.getGroupByClauses())
				{
					if (mcn.measureName.equals(gb.getColName()))
					{
						gbDelList.add(gb);
						break;
					}
				}
				for (OrderBy ob : mec.dimensionExpressionQuery.getOrderByClauses())
				{
					if (ob.getType() == OrderBy.TYPE_MEASURE)
						if (mcn.measureName.equals(ob.getColName()))
						{
							obDelList.add(ob);
							break;
						}
				}
			}
			if (delList.size() > 0)
			{
				for (MeasureColumnNav mc : delList)
				{
					mec.dimensionExpressionQuery.removeMeasureColumn(mc);
				}
			}
			mec.dimensionExpressionQuery.getGroupByClauses().removeAll(gbDelList);
			mec.dimensionExpressionQuery.getOrderByClauses().removeAll(obDelList);

			DisplayExpression de = new DisplayExpression();
			TransformationScript ts = new TransformationScript(mec.dimensionExpressionQuery.getRepository(), q.getSession());
			ts.setLogicalQuery(true);
			ts.setDisplayExpression(de);
			de.initialize();
			Expression ex = new Expression(ts);
			
			ts.setParentDisplayExpression(this);
			Operator op;
			try
			{
				if (!findMeasureColumn(mec.dimensionExpressionQuery, mec))
				{
					MeasureColumnNav mcn = mec.dimensionExpressionQuery.addMeasureColumn(mec.columnName, mec.displayName, false);
					if (mec.filter != null)
						mcn.filter = mec.filter;
					addedNames.add(mec.displayName);
				}
				boolean oldInDimensionExpression = ts.isInDimensionExpression();
				ts.setInDimensionExpression(true);
				op = ex.compile(mec.positions.get(0).dimensionExpression);
				ts.setInDimensionExpression(oldInDimensionExpression);
				de.setLogicalExpression(Statement.toString(mec.positions.get(0).dimensionExpression, null), ts, op, mec.positions.get(0).dimensionExpression);
				mec.dimensionExpressionDisplayExpression = de;
				for(MeasureExpressionColumn newmec: de.getMeasureExpressionColumns())
				{
					if (!findMeasureColumn(q, newmec))
					{
						q.addMeasureColumn(newmec.columnName, newmec.displayName, false);
						addedNames.add(newmec.displayName);
					}
				}
			} catch (ScriptException e)
			{
				logger.error(e);
			}
		} catch (CloneNotSupportedException e)
		{
			return false;
		}
		return true;
	}
	
	private boolean findMeasureColumn(Query q, MeasureExpressionColumn newmec)
	{
		boolean found = false;
		for (MeasureColumnNav mcn : q.measureColumns)
		{
			if (!mcn.hidden && !mcn.navigateonly && mcn.measureName.equals(newmec.columnName) && mcn.displayName.equals(newmec.displayName)
					&& ((mcn.filter == null && newmec.filter == null) || (mcn.filter != null && newmec.filter != null && mcn.filter.equals(newmec.filter))))
			{
				found = true;
				// Make sure column isn't being aggregated
				for (Aggregation agg : q.getAggregations())
				{
					if (agg.getColumnName().equals(newmec.columnName))
					{
						found = false;
						break;
					}
				}
				if (found)
					break;
			}
		}
		return found;
	}
	
	/**
	 * If a new measure expression query is required, then create it. Copy the original query, but remove all columns in
	 * the same dimension as one of the positions (unless those columns are used in a composite filter that includes a
	 * measure - because a having clause will be needed and will require that column). Also create a bridge query that
	 * will allow mapping of rows in the main query to rows in the measure expression query.
	 * 
	 * @param mec
	 * @param q
	 * @throws CloneNotSupportedException
	 */
	private void buildMeasureExpressionQueries(MeasureExpressionColumn mec, Query q) throws CloneNotSupportedException
	{
		// First, clone the original query
		Query mecq = (Query) q.clone();
		mecq.clearKey();
		mecq.clearNavigation();
		mec.query = mecq;
		Query mecbq = (Query) q.clone();
		mecbq.clearKey();
		mecbq.clearNavigation();
		mec.bridgeQuery = mecbq;
		/*
		 * Now, strip the measure expression query of all dimension columns (and associated group bys and
		 * order bys) in the dimension of positions
		 */
		List<DimensionColumnNav> delList = new ArrayList<DimensionColumnNav>();
		List<GroupBy> gbDelList = new ArrayList<GroupBy>();
		List<OrderBy> obDelList = new ArrayList<OrderBy>();
		for (DimensionColumnNav dcn : mecq.dimensionColumns)
		{
			for (Position pos : mec.positions)
			{
				if (pos.pDim.equals(dcn.dimensionName) && !dcn.navigateonly && dcn.colIndex >= 0)
				{
					/*
					 * Make sure that this column is not used in a filter. If so, examine.
					 */
					boolean neededForFilter = false;
					for (QueryFilter qf : mecq.filters)
					{
						if (qf.containsAttribute(dcn.dimensionName, dcn.columnName))
						{
							/*
							 * Confirm that this column is at a higher level than the column in the
							 * position. If not, then this query could yield problematic results and issue a
							 * warning.
							 */
							boolean confirmed = false;
							if (!pos.pCol.equals(dcn.columnName))
							{
								Hierarchy h = mecq.getRepository().findDimensionHierarchy(dcn.dimensionName);
								if (h != null)
								{
									Level l = h.findLevelColumn(dcn.columnName);
									if (l != null)
									{
										if (l.findLevelColumn(pos.pCol) != null)
											confirmed = true;
									}
								}
								if (!confirmed)
								{
									logger.warn("Column " + dcn.dimensionName + "." + dcn.columnName
											+ " eliminated from measure expression query. Cannot confirm that it is a higher level than "
											+ pos.pDim + "." + pos.pCol + ". May yield incorrect results.");
								}
							}
							if (!qf.containsMeasure())
							{
								dcn.navigateonly = true;
								neededForFilter = true;
							}
							break;
						}
					}
					if (!neededForFilter)
						delList.add(dcn);
					for (GroupBy gb : mecq.getGroupByClauses())
					{
						if (dcn.dimensionName.equals(gb.getDimName()) && dcn.columnName.equals(gb.getColName()))
						{
							gbDelList.add(gb);
							break;
						}
					}
					for (OrderBy ob : mecq.getOrderByClauses())
					{
						if (ob.getType() == OrderBy.TYPE_DIMENSION_COLUMN)
							if (dcn.dimensionName.equals(ob.getDimName()) && dcn.columnName.equals(ob.getColName()))
							{
								obDelList.add(ob);
								break;
							}
					}
					break;
				}
			}
		}
		if(delList.size() > 0)
		{
			for(DimensionColumnNav dc : delList)
			{
				mecq.removeDimensionColumn(dc);
			}
		}
		mecq.getGroupByClauses().removeAll(gbDelList);
		mecq.getOrderByClauses().removeAll(obDelList);

	}

	/**
	 * Get the query result sets for measure expression columns
	 * 
	 * @param rsc
	 *            Current resultset cache
	 * @param statementList
	 *            List of statements to use
	 * @param maxRecords
	 *            Maximum records to return
	 * @param session
	 *            Current session
	 * @throws RepositoryException 
	 * @throws CloneNotSupportedException 
	 * @throws SessionVariableUnavailableException
	 * @throws GoogleAnalyticsException
	 * @throws RepositoryException 
	 * @throws ScriptException 
	 */
	public void getMeasureExpressionResultSets(ResultSetCache rsc, int maxRecords, int maxQueryTime, Session session, Query q) throws SQLException, NavigationException,
			IOException, BaseException, ClassNotFoundException, CloneNotSupportedException
	{
		if (measureExpressionColumns != null)
		{
			for (MeasureExpressionColumn mec : measureExpressionColumns)
			{
				if (mec.bridgeQuery != null)
				{
					logger.debug("Measure expression bridge query: " + mec.bridgeQuery.getLogicalQueryString(false, null, false));
					mec.bridgeResultSet = rsc.getQueryResultSet(mec.bridgeQuery, maxRecords, maxQueryTime, null, null, null, session, false, null, true);
				}
				if (mec.query != null)
				{
					logger.debug("Measure expression query: " + mec.query.getLogicalQueryString(false, null, false));
					mec.resultSet = rsc.getQueryResultSet(mec.query, maxRecords, maxQueryTime, null, null, null, session, false, null, true);
				}
				if (mec.dimensionExpressionQuery != null)
				{
					logger.debug("Dimension expression query: " + mec.dimensionExpressionQuery.getLogicalQueryString(false, null, false));
					DisplayExpression de = mec.dimensionExpressionDisplayExpression;
					// Don't process the expression initially
					de.setDontProcess(true);
					List<DisplayExpression> expressionList = new ArrayList<DisplayExpression>();
					expressionList.add(de);
					de.name = "{DIMENSIONEXPRESSION}";
					de.setPosition(mec.dimensionExpressionQuery.colIndex);
					mec.dimensionExpressionResultSet = rsc.getQueryResultSet(mec.dimensionExpressionQuery, maxRecords, maxQueryTime, null, expressionList,
							null, session, false, null, true);
					mec.dimensionExpressionResultSet.generateDimensionMask(mec.dimensionExpressionDisplayExpression.dimensionExpressionColumns, q);
					de.setDontProcess(false);
				}
			}
		}
	}

	/**
	 * @return Returns the name.
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name
	 *            The name to set.
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @return Returns the percentile.
	 */
	public boolean isPercentile()
	{
		return percentile;
	}

	/**
	 * @param percentile
	 *            The percentile to set.
	 */
	public void setPercentile(boolean percentile)
	{
		this.percentile = percentile;
	}

	/**
	 * @return Returns the position.
	 */
	public int getPosition()
	{
		return position;
	}

	/**
	 * @param position
	 *            The position to set.
	 */
	public void setPosition(int position)
	{
		this.position = position;
	}

	/**
	 * @return Returns the rank.
	 */
	public boolean isRank()
	{
		return rank;
	}

	/**
	 * @param rank
	 *            The rank to set.
	 */
	public void setRank(boolean rank)
	{
		this.rank = rank;
	}
	
	public void setRSum(boolean rsum)
	{
		this.rsum = rsum;
	}
	
	public boolean isRSum()
	{
		return rsum;
	}

	/**
	 * @return Returns the expression.
	 */
	public String getExpression()
	{
		return expression;
	}

	/**
	 * @return Returns the compiled expression.
	 */
	public Object getCompiledExprssion()
	{
		if (!jelSupported)
			return null;
		return compiledExpression;
	}

	@SuppressWarnings("rawtypes")
	public Class getCompiledClass()
	{
		if (compiledExpression != null)
			return ((CompiledExpression) compiledExpression).getTypeC();
		if (compiledOperator != null)
		{
			Type t = compiledOperator.getType();
			if (t == Type.Integer)
				return Long.class;
			else if (t == Type.Float)
				return Double.class;
			else if (t == Type.Varchar)
				return String.class;
			else if (t == Type.DateTime)
				return GregorianCalendar.class;
			else if (t == Type.Boolean)
				return Boolean.class;
		}
		return null;
	}

	public int compareTo(DisplayExpression de)
	{
		return (Integer.valueOf(position).compareTo(de.position));
	}

	/**
	 * @return Returns the fields.
	 */
	public synchronized FieldProvider getFields()
	{
		return fields;
	}

	/**
	 * @return Returns the fields.
	 */
	public synchronized FieldProvider getParentFields()
	{
		return parentFields;
	}

	/**
	 * @param fields
	 *            The fields to set.
	 */
	public synchronized void setFields(FieldProvider fields)
	{
		this.fields = fields;
	}

	/**
	 * @param fields
	 *            The fields to set.
	 */
	public synchronized void setParentFields(FieldProvider fields)
	{
		this.parentFields = fields;
	}

	/**
	 * @return Returns the context.
	 */
	public synchronized Object[] getContext()
	{
		return context;
	}

	/**
	 * @param context
	 *            The context to set.
	 */
	public synchronized void setContext(Object[] context)
	{
		this.context = context;
	}

	/**
	 * Returns whether the type of the expression is boolean
	 * 
	 * @return
	 */
	public boolean isBoolean()
	{
		if (compiledOperator != null)
		{
			if (compiledOperator.getType() == Operator.Type.Boolean)
				return true;
			else
				return false;
		}
		if (compiledExpression != null)
		{
			return ((CompiledExpression) compiledExpression).getTypeC() == Boolean.class;
		}
		return false;
	}

	/**
	 * Returns whether the type of the expression is numeric
	 * 
	 * @return
	 */
	public boolean isNumeric()
	{
		if (compiledOperator != null)
		{
			Operator.Type type = compiledOperator.getType();
			if (type == Operator.Type.Integer || type == Operator.Type.Float)
				return true;
			else
				return false;
		}
		if (compiledExpression != null)
		{
			return ((CompiledExpression) compiledExpression).getTypeC() == Double.class
					|| ((CompiledExpression) compiledExpression).getTypeC() == Integer.class;
		}
		return false;
	}
	
	public boolean isInteger()
	{
		if (compiledOperator != null)
		{
			Operator.Type type = compiledOperator.getType();
			if (type == Operator.Type.Integer)
				return true;
			else
				return false;
		}
		if (compiledExpression != null)
		{
			return ((CompiledExpression) compiledExpression).getTypeC() == Integer.class;
		}
		return false;
	}
	
	public boolean isFloat()
	{
		if (compiledOperator != null)
		{
			Operator.Type type = compiledOperator.getType();
			if (type == Operator.Type.Float)
				return true;
			else
				return false;
		}
		if (compiledExpression != null)
		{
			return ((CompiledExpression) compiledExpression).getTypeC() == Double.class;
		}
		return false;
	}

	public String getQueryString()
	{
		String expressionWithQuotesEscaped = expression.replace("\"", "\\\"");
		return ("EVAL{" + expressionWithQuotesEscaped + "} " + name);
	}

	private static Pattern MDX_EXPRESSION_PATTERN = Pattern.compile("mdxexpression\\s*\\(\\s*'.*'\\s*\\)", Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
	public String getNewQueryString()
	{
		Matcher m = MDX_EXPRESSION_PATTERN.matcher(expression);
		if (!m.find()) {
			String expressionWithQuotesEscaped = expression.replace("\"", "\\\"");
			return (expressionWithQuotesEscaped + " '" + name + "'");
		}
		m.reset();
		StringBuffer ret = new StringBuffer().append('(');
		int begin = 0;
		while (m.find()) {
			String prematch = expression.substring(begin, m.start()); 
			ret.append(prematch.replace("\"", "\\\""));
			ret.append(m.group());
			begin = m.end();
		}
		ret.append(expression.substring(begin).replace("\"", "\\\""));
		return ret.append(") '").append(name).append("'").toString();
	}

	/**
	 * Return the derived aggregation rule for this expression. This only comes into play if an expression contains a
	 * column at a lower level than is in the original list of columns. The expression is aggregatable if all measures
	 * have the same rule and that rule is additive. If there are no measures, and the expression is numeric, then SUM
	 * is the default rule. If there are only positional calculations and all positions are absolute, then the
	 * aggregation rule is first.
	 * 
	 * Need to call this before calling isAggregatable (runs the aggregatable logic once)
	 * 
	 * @return
	 */
	public String getAggRule()
	{
		try
		{
			if (measureExpressionColumns.isEmpty() && isNumeric())
			{
				aggregatable = true;
				return ("SUM");
			}
			String rule = null;
			boolean absolute = false;
			for (MeasureExpressionColumn mec : measureExpressionColumns)
			{
				if (mec.positions.size() > 0)
				{
					for (Position pos : mec.positions)
					{
						if (pos.ptype != PositionType.Absolute)
						{
							aggregatable = false;
							return (null);
						} else
							absolute = true;
					}
					if (rule == null)
						rule = mec.getMeasureColumn().AggregationRule;
					else if (!rule.equals(mec.getMeasureColumn().AggregationRule))
					{
						aggregatable = false;
						return (null);
					}
				}
			}
			aggregatable = true;
			if (absolute)
				rule = "FIRST";
			return (rule);
		} catch (Exception ex)
		{
			logger.warn(ex, ex);
		}
		return (null);
	}

	/**
	 * Returns whether an expression is aggregatable. (Need to call getAggRule() first).
	 * 
	 * @return
	 */
	public boolean isAggregatable()
	{
		return (aggregatable);
	}

	/**
	 * @return Returns the groups.
	 */
	public List<String> getGroups()
	{
		return groups;
	}

	/**
	 * @param groups
	 *            The groups to set.
	 */
	public void setGroups(List<String> groups)
	{
		this.groups = groups;
	}

	/**
	 * Returns whether this Display Expression contains measure expression columns
	 * 
	 * @return
	 */
	public boolean hasMeasuresExpressionColumns()
	{
		if (measureExpressionColumns != null && !measureExpressionColumns.isEmpty())
			return true;
		// do something more complex for SETLOOKUP, need to find what it returns
		Operator co = this.compiledOperator;
		if (co != null)
		{
			if (co instanceof SetLookup)
			{
				SetLookup sl = (SetLookup) co;
				return sl.returnsMeasure();
			}
			if (co instanceof IIF)
			{
				IIF sl = (IIF) co;
				return sl.returnsMeasure();
			}
		}
		return false;
	}

	public List<MeasureExpressionColumn> getMeasureExpressionColumns()
	{
		return measureExpressionColumns;
	}

	public List<DimensionExpressionColumn> getDimensionExpressionColumns()
	{
		return dimensionExpressionColumns;
	}
	
	public List<OlapMemberExpression> geOlapMemberExpressions()
	{
		return olapMemberExpressionColumns;
	}

	public void setDimensionExpressionColumns(List<DimensionExpressionColumn> dimensionExpressionColumns)
	{
		this.dimensionExpressionColumns = dimensionExpressionColumns;
	}

	public boolean isDenserank()
	{
		return denserank;
	}

	public void setDenserank(boolean denserank)
	{
		this.denserank = denserank;
	}

	public Operator getCompiledOperator()
	{
		return compiledOperator;
	}

	public ResultSetCache getResultSetCache()
	{
		return rsc;
	}

	public void setResultSetCache(ResultSetCache rsc)
	{
		this.rsc = rsc;
	}

	public Map<DimensionColumn, Groups> getCustomGroups()
	{
		return customGroups;
	}

	public TransformationScript getScript()
	{
		return script;
	}

	public void setScript(TransformationScript script)
	{
		this.script = script;
	}

	public boolean isColumn()
	{
		return isColumn;
	}

	public void setColumn(boolean isColumn)
	{
		this.isColumn = isColumn;
	}

	public String getColumnDimension()
	{
		return columnDimension;
	}

	public void setColumnDimension(String columnDimension)
	{
		this.columnDimension = columnDimension;
	}

	public String getColumnName()
	{
		return columnName;
	}

	public void setColumnName(String columnName)
	{
		this.columnName = columnName;
	}

	public Transform getTransform()
	{
		return transform;
	}

	public void setTransform(Transform transform)
	{
		this.transform = transform;
	}

	public boolean dontProcess()
	{
		return dontProcess;
	}

	public void setDontProcess(boolean dontProcess)
	{
		this.dontProcess = dontProcess;
	}
	
	public boolean containsOlapMemberSet() {
		if (this.dimensionExpressionColumns != null && dimensionExpressionColumns.size() > 0) {
			for (DimensionExpressionColumn dec : dimensionExpressionColumns) {
				if (dec.isOlapLogicalMemberSet())
					return true;
			}
		}
		
		return false;
	}
}
