/**
 * $Id: CacheEntry.java,v 1.20 2011-06-02 18:44:16 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.io.Externalizable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import org.apache.log4j.Logger;

import com.mongodb.BasicDBObject;
import com.successmetricsinc.util.Util;

/**
 * Class to contain an individual cache entry
 * 
 * @author Brad Peters
 * 
 */
public class CacheEntry implements Externalizable, BasicDBObjectSerializer
{
	private static Logger logger = Logger.getLogger(CacheEntry.class);
	private static final long serialVersionUID = 1L;
	private Query q;
	private String fileName;
	private transient File cacheFile;
	private int numRows;
	private int referenceCount = 0;

	/**
	 * Constructor used for serialization
	 */
	public CacheEntry()
	{
	}

	public CacheEntry(Query q, String fileName)
	{
		this.q = q;
		this.fileName = fileName;
		this.referenceCount = 0;
	}

	public CacheEntry(Query q, File cacheFile)
	{
		this.q = q;
		this.cacheFile = cacheFile;
		this.referenceCount = 0;
	}

	/**
	 * @return Returns the query.
	 */
	public Query getQuery()
	{
		return q;
	}

	/**
	 * @param q
	 *            The query to set.
	 */
	public void setQuery(Query q)
	{
		this.q = q;
	}

	/**
	 * @return Returns the cacheFile.
	 */
	public File getCacheFile(String cacheDir)
	{
		if (cacheFile == null)
			cacheFile = new File(cacheDir + File.separator + fileName);
		return cacheFile;
	}

	/**
	 * @param cacheFile
	 *            The cacheFile to set.
	 */
	public void setCacheFile(File cacheFile)
	{
		this.cacheFile = cacheFile;
	}

	/**
	 * @return Returns the fileName.
	 */
	public String getFileName()
	{
		return fileName;
	}

	/**
	 * @param fileName
	 *            The fileName to set.
	 */
	public void setFileName(String fileName)
	{
		this.fileName = fileName;
	}

	public void writeExternal(ObjectOutput oo) throws IOException
	{
		oo.writeObject(fileName);
		oo.writeObject(q);
		oo.writeObject(numRows);
		oo.writeObject(referenceCount);
	}

	public void readExternal(ObjectInput oi) throws IOException, ClassNotFoundException
	{
		fileName = (String) oi.readObject();
		q = (Query) oi.readObject();
		numRows = (Integer) oi.readObject();
		referenceCount = (Integer) oi.readObject();
	}

	public BasicDBObject toBasicDBObject()
	{
		BasicDBObject obj = new BasicDBObject();
		
		obj.put("fileName", fileName);
		obj.put("q", q.toBasicDBObject());
		obj.put("numRows", numRows);
		obj.put("referenceCount", referenceCount);
		
		return obj;
	}
	
	public void fromBasicDBObject(BasicDBObject obj)
	{
		fileName = obj.getString("fileName");
		numRows = obj.getInt("numRows");
		referenceCount = obj.getInt("referenceCount");
		q = new Query();
		q.fromBasicDBObject((BasicDBObject)obj.get("q"));
	}
	
	/**
	 * @return Returns the numRows.
	 */
	public int getNumRows()
	{
		return numRows;
	}

	/**
	 * @param numRows
	 *            The numRows to set.
	 */
	public void setNumRows(int numRows)
	{
		this.numRows = numRows;
	}

	public boolean equals(Object o)
	{
		if (this == o)
			return true;
		if (!(o instanceof CacheEntry))
			return false;
		CacheEntry ce = (CacheEntry) o;
		return ce.fileName.equals(fileName);
	}
	
	public int hashCode()
	{
		  assert false : "hashCode not designed";
		  return 42; // any arbitrary constant will do 
	}

	public String toString()
	{
		try
		{
			return (this.q.getKey(null));
		}
		catch (Exception e)
		{
			logger.warn(e.getMessage(), e);
			return "null";
		}
	}

	/**
	 * @return Whether the cache entry has references
	 */
	public synchronized boolean hasReferences()
	{
		return (referenceCount > 0);
	}

	/**
	 * Increment the reference count
	 */
	public synchronized void incrementReferenceCount()
	{
		referenceCount++;
	}

	public synchronized void decrementReferenceCount()
	{
		referenceCount = referenceCount == 0 ? 0 : referenceCount - 1;
	}

	/**
	 * Decrement the reference count and delete the cache file if it's the last one
	 * @throws IOException 
	 */
	public boolean decrementAndDeleteFileIfLast(String cacheDir) throws IOException
	{
		referenceCount = referenceCount == 0 ? 0 : referenceCount - 1;
		if (!hasReferences())
		{
			if (cacheFile == null)
				cacheFile = new File(cacheDir + File.separator + fileName); 
			if (cacheFile.exists())
			{
				if (Util.secureDelete(cacheFile))
				{
					logger.trace("Deleted file: " + this.cacheFile.getName());
				}
				else
				{
					logger.warn("Unable to delete file: " + this.cacheFile.getName());
				}
			}
			return (true);
		}
		return (false);
	}

	
	
	public synchronized int getReferenceCount()
	{
		return referenceCount;
	}

	/**
	 * Get the query result set associated with this cache entry
	 * 
	 * @return
	 * @throws Exception
	 */
	public QueryResultSet getResultSet(String cacheDir) throws Exception
	{
		if (cacheFile == null)
			cacheFile = new File(cacheDir + File.separator + fileName);
		FileInputStream in = null;
		ObjectInputStream s = null;
		try
		{
			in = new FileInputStream(cacheFile);
			s = new ObjectInputStream(in);
			QueryResultSet qrs = (QueryResultSet) s.readObject();
			qrs.setBaseFilename(cacheFile.getName());
			return qrs;
		}
		finally
		{
			if (s != null)
				s.close();
			if (in != null)
				in.close();
		}
	}
}
