/**
 * $Id: GroupBy.java,v 1.21 2012-04-09 14:12:20 mjani Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.List;

import com.mongodb.BasicDBObject;

/**
 * Class to hold/generate a group by clause for a query
 * 
 * @author bpeters
 * 
 */
public class GroupBy implements Externalizable, BasicDBObjectSerializer
{
	private static final long serialVersionUID = 1L;
	private static final int TYPE_DIMENSION_COLUMN = 0;
	private static final int TYPE_CONSTANT_OR_FORMULA = 1;
	public static final int TYPE_DIMENSION_MEMBER_SET = 2;
	private int type;
	private String dimName;
	private String colName;
	private String constant;
	private DimensionColumn dc;
	private QueryColumn qc;
	private int columnPosition;

	/**
	 * Constructor used for serialization
	 */
	public GroupBy()
	{
	}

	/**
	 * Create a group by for a given dimension column
	 * 
	 * @param dimName
	 * @param colName
	 * @param ascending
	 */
	public GroupBy(String dimName, String colName)
	{
		this.dimName = dimName;
		this.colName = colName;
		this.type = TYPE_DIMENSION_COLUMN;
	}
	
	public GroupBy(String dimName, String memberExpressions, int type)
	{
		this.dimName = dimName;
		this.colName = memberExpressions;
		this.type = type;
	}

	/**
	 * Create a group by for a constant/expression
	 * 
	 * @param constant
	 */
	public GroupBy(String constant)
	{
		this.constant = constant;
		this.type = TYPE_CONSTANT_OR_FORMULA;
	}

	/**
	 * Create a group by for a dimension column
	 * 
	 * @param constant
	 */
	public GroupBy(DimensionColumn dc)
	{
		this.dc = dc;
		this.dimName = dc.DimensionTable.DimensionName;
		this.colName = dc.ColumnName;
		this.type = TYPE_DIMENSION_COLUMN;
	}

	/**
	 * Create a group by for a query column
	 * 
	 * @param constant
	 */
	public GroupBy(QueryColumn qc)
	{
		if (qc.type != QueryColumn.DIMENSION)
			return;
		this.qc = qc;
		this.dc = (DimensionColumn) qc.o;
		this.dimName = dc.DimensionTable.DimensionName;
		this.colName = dc.ColumnName;
		this.type = TYPE_DIMENSION_COLUMN;
	}

	/**
	 * Set columns based on navigation list
	 * 
	 * @param queryColumnList
	 */
	public void setNavigation(List<QueryColumn> queryColumnList)
	{
		for (QueryColumn qc : queryColumnList)
		{
			if (qc.type == QueryColumn.DIMENSION)
			{
				DimensionColumn dc = (DimensionColumn) qc.o;
				if (dc.matchesDimensionName(dimName) && dc.ColumnName.equals(colName))
				{
					this.dc = dc;
					this.qc = qc;
					break;
				}
			}
		}
	}

	/**
	 * Return inner query string for group by
	 * 
	 * @param q
	 * @return
	 */
	public String getGroupByString(DatabaseConnection dbc, Query q, QueryMap tableMap, List<String> lookupJoins)
	{
		if (type == TYPE_DIMENSION_COLUMN)
		{
			return (q.getDimensionColumnString(dbc, dc, q.getVcDimensionTables(), tableMap, lookupJoins));
		} else if (type == TYPE_CONSTANT_OR_FORMULA)
		{
			return (q.columnMap.getMap(qc));
		}
		return (null);
	}

	/**
	 * Return a unique string key to represent this clause in the overall query key for cache keying
	 * 
	 * @param q
	 * @return
	 */
	public String getQueryKeyStr(Query q)
	{
		if (type == TYPE_DIMENSION_COLUMN)
		{
			return ("GB{D{" + dimName + "}C{" + colName + "}}");
		} else if (type == TYPE_DIMENSION_MEMBER_SET)
		{
			return ("GB{D{" + dimName + "}ME{" + colName + "}}");
		} else if (type == TYPE_CONSTANT_OR_FORMULA)
		{
			return ("OB{C{" + constant + "}}");
		}
		return (null);
	}

	/**
	 * Extract columns from group by for later navigation
	 * 
	 * @param q
	 */
	public void extractNavigationColumns(Query q) throws NavigationException
	{
		if (type == TYPE_DIMENSION_COLUMN)
		{
			DimensionColumnNav dcn = q.findDimensionColumnNav(dimName, colName);
			if (dcn == null)
			{
				dcn = q.addDimensionColumn(dimName, colName);
				dcn.navigateonly = true;
			}
			return;
		}
	}

	/**
	 * Ensure that a given query column is in a list of group by clauses (needed if adding a new query column to a
	 * projection list that is a dimension column and there is an aggregated measure)
	 * 
	 * @param groupByClauses
	 *            List of clauses
	 * @param qc
	 *            Query column
	 */
	public static void ensureInList(List<GroupBy> groupByClauses, QueryColumn qc, List<GroupBy> addedGroupBys)
	{
		if ((groupByClauses == null) || (qc == null) || (qc.type != QueryColumn.DIMENSION))
			return;
		{
			boolean found = false;
			for (GroupBy gb : groupByClauses)
			{
				if (gb.qc == qc)
				{
					found = true;
					break;
				}
			}
			if (!found)
			{
				DimensionColumn dc = (DimensionColumn) qc.o;
				GroupBy gb = new GroupBy(dc.DimensionTable.DimensionName, dc.ColumnName);
				// Use the same navigated column
				gb.dc = dc;
				groupByClauses.add(gb);
				addedGroupBys.add(gb);
			}
		}
	}

	/**
	 * Add all non-duplicate groupbys to a list of groupbys
	 * 
	 * @param list
	 * @param toadd
	 */
	public static void addToList(List<GroupBy> list, List<GroupBy> toadd)
	{
		for (GroupBy gb : toadd)
		{
			boolean found = false;
			for (GroupBy gb2 : list)
			{
				if ((gb2.colName != null) && gb2.colName.equals(gb.colName))
				{
					if ((gb2.dimName == null && gb.dimName == null) || (gb2.dimName != null && gb2.dimName.equals(gb.dimName)))
					{
						found = true;
					}
				}
			}
			if (!found && !gb.dc.Constant)
				list.add(gb);
		}
	}

	public void writeExternal(ObjectOutput oo) throws IOException
	{
		oo.writeObject(type);
		oo.writeObject(dimName);
		oo.writeObject(colName);
		oo.writeObject(constant);
	}

	public void readExternal(ObjectInput oi) throws IOException, ClassNotFoundException
	{
		type = (Integer) oi.readObject();
		dimName = (String) oi.readObject();
		colName = (String) oi.readObject();
		constant = (String) oi.readObject();
	}

	public void fromBasicDBObject(BasicDBObject obj)
	{
		type = obj.getInt("type");
		dimName = obj.getString("dimName");
		colName = obj.getString("colName");
		constant = obj.getString("constant");
	}
	
	public BasicDBObject toBasicDBObject()
	{
		BasicDBObject o = new BasicDBObject();
		o.put("type", type);
		o.put("dimName", dimName);
		o.put("colName", colName);
		o.put("constant", constant);
		return o;
	}
	
	/**
	 * Return whether two group bys are logically the same
	 * 
	 * @param gb
	 * @return
	 */
	public boolean equals(Object o)
	{
		if (this == o)
			return true;
		if (!(o instanceof GroupBy))
			return false;
		GroupBy gb = (GroupBy) o;
		if (gb.type != type)
			return (false);
		if (type == TYPE_DIMENSION_COLUMN || type == TYPE_DIMENSION_MEMBER_SET)
			return (gb.dimName.equals(dimName) && gb.colName.equals(colName));
		else
			return (gb.constant.equals(constant));
	}

	public int hashCode()
	{
		assert false : "hashCode not designed";
		return 42; // any arbitrary constant will do
	}

	/**
	 * Bind a group by to a given row of data
	 * 
	 * @param tableNames
	 * @param columnNames
	 */
	public void bindPredicate(String[] tableNames, String[] columnNames)
	{
		for (int i = 0; i < columnNames.length; i++)
		{
			if ((dimName == null || dimName.equals(tableNames[i])) && columnNames[i].equals(colName))
			{
				columnPosition = i;
				break;
			}
		}
	}

	/**
	 * Return the bound key for this predicate (to be used when aggregating)
	 * 
	 * @param row
	 * @return
	 */
	public String getBoundPredicate(Object[] row)
	{
		if (columnPosition < row.length)
			return ("{" + row[columnPosition] + "}");
		else
			return ("");
	}

	/**
	 * @return Returns the Dimension Name.
	 */
	public String getDimName()
	{
		return dimName;
	}

	/**
	 * @return Returns the Column Name.
	 */
	public String getColName()
	{
		return colName;
	}

	/**
	 * Replace reference to an old dimension column with a new one in the same dimension
	 * 
	 * @param dim
	 * @param oldColumn
	 * @param newColumn
	 */
	public void replaceDimensionColumn(String dim, String oldColumn, String newColumn)
	{
		if (dimName.equals(dim) && colName.equals(oldColumn))
			colName = newColumn;
	}

	/**
	 * @return the navigated dimension column
	 */
	public DimensionColumn getDimensionColumn()
	{
		return dc;
	}

	/**
	 * @param colName
	 *            the colName to set
	 */
	public void setColName(String colName)
	{
		this.colName = colName;
	}

	public QueryColumn getQueryColumn()
	{
		return qc;
	}
}
