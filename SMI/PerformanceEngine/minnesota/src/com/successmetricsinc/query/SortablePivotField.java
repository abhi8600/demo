/**
 * 
 */
package com.successmetricsinc.query;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * @author agarrison
 *
 */
public class SortablePivotField implements Comparable<SortablePivotField>, Serializable {
	
	private class SortObject implements Serializable {
		public Serializable value;
		public boolean ascending;
		
		public SortObject(Serializable value, boolean ascending) {
			this.value = value;
			this.ascending = ascending;
		}
	}
	private Object value;
	private List<SortObject> sortValues;
	public static final String PIVOT_SORT = "_PIVOT_SORT_"; 

	public SortablePivotField(Object value) {
		if (value instanceof Calendar)
			value = ((Calendar)value).getTime();
		this.value = value;
		sortValues = new ArrayList<SortObject>();
	}
	
	public void addSortValue(Serializable v, boolean ascending) {
		sortValues.add(new SortObject(v, ascending));
	}
	
	public String toString() {
		return value == null ? "" : value.toString();
	}
	
	public Object getValue() {
		return value;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (o == this)
			return true;
		if (! (o instanceof SortablePivotField))
			return false;
		
		return value.equals(((SortablePivotField)o).value);
	}
	
	@Override
	public int hashCode() {
		return value.hashCode();
	}

	@SuppressWarnings("unchecked")
	@Override
	public int compareTo(SortablePivotField o) {
		for (int i = 0; i < sortValues.size(); i++) {
			if (o.sortValues.size() > i) {
				SortObject thisS = sortValues.get(i);
				SortObject thatS = o.sortValues.get(i);
				int comparison = 0;
				if (thisS.value instanceof Comparable) {
					comparison = ((Comparable)(thisS.value)).compareTo(thatS.value);
				}
				else {
					comparison = thisS.value.toString().compareTo(thatS.value.toString());
				}
				if (comparison != 0)
					return thisS.ascending ? comparison : -comparison;
			}
		}
			
		return value.toString().compareTo(o.toString());
	}
}
