/**
 * 
 */
package com.successmetricsinc.query;

import java.io.Serializable;

import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.util.Util;

/**
 * @author Brad
 * 
 */
public class HierarchyLevel implements Serializable
{
	private static final long serialVersionUID = 1L;
	public String HierarchyName;
	public String LevelName;

	public HierarchyLevel()
	{
	}

	public HierarchyLevel(Element he, Namespace ns)
	{
		HierarchyName = Util.intern(he.getChildText("HierarchyName", ns));
		LevelName = Util.intern(he.getChildText("LevelName", ns));
	}

	public HierarchyLevel(String hierarchyName, String levelName)
	{
		this.HierarchyName = hierarchyName;
		this.LevelName = levelName;
	}
	
	public Element getHierarchyLevelElement(Namespace ns)
	{
		Element e = new Element("HierarchyLevel",ns);
		
		Element child = new Element("HierarchyName",ns);
		child.setText(HierarchyName);
		e.addContent(child);
		
		child = new Element("LevelName",ns);
		child.setText(LevelName);
		e.addContent(child);
				
		return e;
	}
}
