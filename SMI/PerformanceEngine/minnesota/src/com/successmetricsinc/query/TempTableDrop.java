/**
 * While executing queries set based filters (the one containing IN operator), we create temporary tables to store intermediate
 * results for Infobright to improve performance. We also keep them around for some time to make sure that temp tables with the
 * same exact query can be reused for further performance improvement. We do have a reaper thread specific to a given session
 * but it goes out of scope if a user logs out or server is rebooted.
 * 
 * This class implements a command that will drop all temporary tables in a given space. It should be placed in load.cmd only
 * if database connection is IB.
 */
package com.successmetricsinc.query;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;

/**
 * @author mjani
 *
 */
public class TempTableDrop
{
	private static Logger logger = Logger.getLogger(TempTableDrop.class);

	public TempTableDrop()
	{
	}
	
	public void dropTempTables(Repository r, DatabaseConnection dc, String ttPrefix) throws SQLException
	{
		List<String> tables = Database.getTables(dc, null, dc.Schema, null);
		if (tables == null || tables.isEmpty())
		{
			logger.debug(dc.Schema + " getTables returned nothing");
		} else
		{
			String lowerCaseTtPrefix = ttPrefix.toLowerCase();
			Set<String> tablesToDrop = new HashSet<String>();
			for(String tname : tables)
			{
				if(tname.toLowerCase().startsWith(lowerCaseTtPrefix))
				{
					tablesToDrop.add(tname);
				}
			}
			Database.dropTables(r, dc, tablesToDrop);
		}	
	}
}
