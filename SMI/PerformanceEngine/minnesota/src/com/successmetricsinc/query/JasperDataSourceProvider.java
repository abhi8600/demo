/**
 * $Id: JasperDataSourceProvider.java,v 1.177 2012-08-09 08:37:21 mpandit Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRAbstractBeanDataSourceProvider;
import net.sf.jasperreports.engine.design.JRDesignField;

import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.query.Filter.FilterType;
import com.successmetricsinc.query.olap.DimensionMemberSetNav;
import com.successmetricsinc.query.olap.OlapLogicalQueryString;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.util.RepositoryException;
import com.successmetricsinc.util.Util;


/**
 * Class to provide a data source as requested for JasperReports GUI editors
 * 
 * @author Brad Peters
 * 
 */
public class JasperDataSourceProvider extends JRAbstractBeanDataSourceProvider implements Serializable
{
	private static final long serialVersionUID = 1L;
	public static final String PROMPT_VALUES = "PROMPT_VALUES";
	public static final String PROMPT_VALUES_ONE_LINE = "PROMPT_VALUES_ONE_LINE";
	public static final String PROMPT_VALUES_SKIP_ALL = "PROMPT_VALUES_SKIP_ALL";
	public static final String PROMPT_VALUES_ONE_LINE_SKIP_ALL = "PROMPT_VALUES_ONE_LINE_SKIP_ALL";
	
	private static Logger logger = Logger.getLogger(JasperDataSourceProvider.class);
	private Query q;
	private Repository r;
	private transient WebServerConnectInfo serverConnectInfo;
	private QueryResultSet qrs;
	private transient ResultSetCache rsc;
	private Session session;
	private int maxRecords = -1;
	private List<Filter> params = null;
	private boolean applyAllPrompts = false;
	private Map<String, String> dimensionMemberSetDataTypeMap = null;
	private Map<String, String> dimensionMemberSetFormatMap = null;
	private transient boolean executePhysicalQueryOnly = false;
	private transient int queryTimeOut = 0;
	private transient String userDisplayTimezone = null;

	/**
	 * Simple constructor
	 */
	public JasperDataSourceProvider()
	{
		super(QueryResultSet.class);
	}

	/**
	 * Create based on another data source (using same database connection and repository)
	 * 
	 * @param jdsp
	 */
	public JasperDataSourceProvider(JasperDataSourceProvider jdsp)
	{
		super(QueryResultSet.class);
		this.r = jdsp.r;
		this.session = jdsp.session;
		this.serverConnectInfo = jdsp.serverConnectInfo;
		if (this.r != null)
		{
			this.rsc = ResultSetCache.getInstance(r.getApplicationIdentifier(), false);
			this.maxRecords = this.r.getServerParameters().getMaxRecords();
		}
		this.params = jdsp.params;
		this.applyAllPrompts = jdsp.applyAllPrompts;
	}

	/**
	 * Create given a repository
	 * 
	 * @param jdsp
	 */
	public JasperDataSourceProvider(Repository r)
	{
		super(QueryResultSet.class);
		this.r = r;
		if (this.r != null)
		{
			this.rsc = ResultSetCache.getInstance(r.getApplicationIdentifier(), false);
			this.maxRecords = this.r.getServerParameters().getMaxRecords();
		}
	}

	/**
	 * Create given a repository
	 * 
	 * @param jdsp
	 */
	public JasperDataSourceProvider(Repository r, ResultSetCache rsc)
	{
		super(QueryResultSet.class);
		this.r = r;
		this.rsc = rsc;		
	}

	/**
	 * Get fields present in a report
	 * 
	 * @see net.sf.jasperreports.engine.data.JRAbstractBeanDataSourceProvider#getFields(net.sf.jasperreports.engine.JasperReport)
	 */
	public JRField[] getFields(JasperReport report) throws JRException
	{
		if (q == null)
		{
			try
			{
				qrs = (QueryResultSet) create(report);
			} catch (Exception ex)
			{
				throw (new JRException(ex));
			}
		}
		String[] cnames = qrs.getColumnNames();
		String[] dispNames = qrs.getDisplayNames();
		String[] colNames = new String[cnames.length];
		for (int i = 0; i < dispNames.length; i++)
		{
			if (dispNames[i] != null)
				colNames[i] = dispNames[i];
			else
				colNames[i] = cnames[i];
		}
		int[] colDataTypes = qrs.getColumnDataTypes();
		JRDesignField[] fields = new JRDesignField[colNames.length + 1];
		for (int i = 0; i < colNames.length; i++)
		{
			fields[i] = new JRDesignField();
			String fn = Util.replaceWithPhysicalString(colNames[i]);
			fields[i].setName(fn);
			switch (colDataTypes[i])
			{
			case Types.INTEGER:
				fields[i].setValueClass(Integer.class);
				break;
			case Types.DOUBLE:
				fields[i].setValueClass(Double.class);
				break;
			case Types.VARCHAR:
				fields[i].setValueClass(String.class);
				break;
			case Types.TIMESTAMP:
				fields[i].setValueClass(Timestamp.class);
				break;
			}
		}
		// add PROMPT_VALUES
		fields[colNames.length] = new JRDesignField();
		fields[colNames.length].setName(PROMPT_VALUES);
		fields[colNames.length].setValueClass(String.class);
		
		return fields;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.sf.jasperreports.engine.data.JRAbstractBeanDataSourceProvider#supportsGetFieldsOperation()
	 */
	public boolean supportsGetFieldsOperation()
	{
		return true;
	}

	@SuppressWarnings("rawtypes")
	public JasperDataSourceProvider(Class arg0)
	{
		super(arg0);
	}
	
	public void setExecutePhysicalQueryOnly(boolean executePhysicalQueryOnly) {
		this.executePhysicalQueryOnly = executePhysicalQueryOnly;
	}
	
	public void setQueryTimeOut(int queryTimeOut){
		this.queryTimeOut = queryTimeOut;
	}

	public void setUserDisplayTimezone(String userDisplayTimezone) {
		this.userDisplayTimezone = userDisplayTimezone;
	}

	/**
	 * Create a data source based on a query. Parse the query string and create a new query structure. Query result set
	 * cache to get results and return
	 * 
	 * @param query
	 *            Query to retrieve
	 * @return Jasper data source
	 * @throws IOException
	 * @throws DisplayExpressionException
	 * @throws SyntaxErrorException
	 * @throws NavigationException
	 * @throws DisplayExpressionException
	 * @throws NavigationException
	 * @throws RepositoryException
	 * @throws DataUnavailableException
	 * @throws ClassNotFoundException
	 * @see net.sf.jasperreports.engine.JRDataSourceProvider#create(net.sf.jasperreports.engine.JasperReport)
	 * @throws JRException
	 */
	public JRDataSource create(String query) throws Exception
	{
		try {
			return create(query, this.session);
		}
		catch (Exception e) {
			logger.error("Logical query failed: <" + query + ">");
			throw e;
		}
	}
	
	public JRDataSource create(String query, Session session) throws Exception
	{
		this.setSession(session);
		return create(query, null, null, false);
	}

	/**
	 * Create a data source based on a query. Parse the query string and create a new query structure. Query result set
	 * cache to get results and return
	 * 
	 * @param query
	 *            Query to retrieve
	 * @param splitCacheOn
	 *            The dimension column to split result set cache on - <dimension-name>.<column-name>
	 * @return Jasper data source
	 * @throws IOException
	 * @throws SyntaxErrorException
	 * @throws NavigationException
	 * @throws RepositoryException
	 * @throws DataUnavailableException
	 * @throws ClassNotFoundException
	 * @see net.sf.jasperreports.engine.JRDataSourceProvider#create(net.sf.jasperreports.engine.JasperReport)
	 * @throws JRException
	 */
	
	public JRDataSource create(String query, String splitCacheOnDimension, String splitCacheOnColumn, boolean simulateOuterJoinUsingPersistedDQTs) throws Exception
	{
		try
		{
			if (query == null || !Util.hasNonWhiteSpaceCharacters(query))
				return null;
			if (query.equals("SingleRow{}"))
			{
				return new SingleRowDataSource(this);
			}
			boolean isNewQueryLanguage = query.toLowerCase().startsWith("select");
			if (isNewQueryLanguage)
			{
				query = Util.convertMulValueVariablesToNewLanguageSyntax(r, query);
			}
			query = QueryString.preProcessQueryString(query, r, session);
			AbstractQueryString aqs = null;
			if (isNewQueryLanguage)
			{
				Matcher matcher = AbstractQueryString.olapQueryPattern.matcher(query);
				if (matcher.find())
				{
					aqs = new OlapLogicalQueryString(r, null, query);
				}
				else
				{
					aqs = new LogicalQueryString(r, session, query);
				}
				q = aqs.getQuery();
			} else
			{			
				aqs = new QueryString(r);
				if (serverConnectInfo != null)
					aqs.setServerConnectInfo(serverConnectInfo);
				q = aqs.processQueryString(query, session);
			}
			if (this.dimensionMemberSetDataTypeMap != null && q.dimensionMemberSets != null && q.dimensionMemberSets.size() > 0)
			{
				for (DimensionMemberSetNav dms : q.dimensionMemberSets)
				{
					if (dms.displayName != null && this.dimensionMemberSetDataTypeMap.containsKey(dms.displayName))
						dms.dataType = this.dimensionMemberSetDataTypeMap.get(dms.displayName);
					if (dms.displayName != null && this.dimensionMemberSetFormatMap.containsKey(dms.displayName))
						dms.format = this.dimensionMemberSetFormatMap.get(dms.displayName);
				}
			}
			if (this.params != null && this.params.size() > 0)
			{
				List<Filter> addMeasureFilters = new ArrayList<Filter>();
				List<Filter> addDimensionFilters = new ArrayList<Filter>();
				
				// turn the filter list back into a 
				List<Filter> processedFilters = new ArrayList<Filter>();
				Map<String, Filter> map = new HashMap<String, Filter>();
				for (Filter filter : params)
				{
					String key = filter.getColumnName();
					int index = key.indexOf('.');
					if (index <= 0)
					{
						processedFilters.add(filter);
						continue;
					}
					if ("OR".equals(filter.getLogicalOperator()) && "=".equals(filter.getOperator()) && filter.getValues() != null)
					{
						if (map.containsKey(key))
						{
							Filter fl = map.get(key);
							fl.getValues().addAll(filter.getValues());
						}
						else
						{
							map.put(key, filter);
						}
					}
					else
					{
						processedFilters.add(filter);
					}
				}
				
				for (String key : map.keySet())
				{
					processedFilters.add(map.get(key));
				}
				
				for (Filter filter : processedFilters)
				{
					if (filter == null || filter.getColumnName() == null)
					{
						continue;
					}
					if (filter.isCreateFiltersFrom() == false)
						continue;
					
					if (filter.getValues().size() == 1 && filter.requiresAValue()) {
						String v = filter.getValues().get(0);
						if (v == null || v.equals("NO_FILTER"))
							continue;
					}
					String key = filter.getColumnName();
					int index = key.indexOf('.');
					if (index <= 0) {
						// measure
						// sometimes adding a filter will change q.measureColumns throwing a concurrentModificationException
						// don't do the commented for loop
						//for (MeasureColumnNav mcn : q.measureColumns) {
						boolean addToDimensionFilters = true;
						for (int j = 0; j < q.measureColumns.size(); j++) {
							MeasureColumnNav mcn = q.measureColumns.get(j);
							if (! mcn.measureName.equals(key)) {
								continue;
							}
							addToDimensionFilters = false;
							
							if(filter.getValues().size() > 0 && !mcn.navigateonly){
								if (filter.getFilterType() == FilterType.DISPLAY){
									DisplayFilter df = createMeasureDisplayFilter(mcn.measureName, filter);
									aqs.getDisplayFilters().add(df);
								}else{
									QueryFilter qf = createMeasureQueryFilter(mcn.measureName, filter);
									q.addFilter(qf);
								}
							}
						}
						
						// could be olap - check dimensionMemberSets
						if (q.dimensionMemberSets.size() > 0) {
							for (DimensionMemberSetNav dms : q.dimensionMemberSets) {
								if (dms.dimensionName.equals(key)) {
									List<QueryFilter> qfs = q.getSplitFilterList(); // deal with top level AND from new query language
									List<QueryFilter> filtersToRemove = new ArrayList<QueryFilter>();
									for (QueryFilter qf: qfs) {
										if (qf.containsOlapDimension(key))
											filtersToRemove.add(qf);
									}
									if (filtersToRemove.size() > 0) {
										qfs.removeAll(filtersToRemove);
										q.filters = qfs; // removed the filters from the split
									}

									if (filter.getValues().size() == 1) {
										QueryFilter qf = new QueryFilter(key, dms.getDisplayName(), filter.getOperator(), filter.getValues().get(0), QueryFilter.TYPE_DIMENSION_MEMBER_SET);
										q.addFilter(qf);
									}
									else {
										List<QueryFilter> qfList = new ArrayList<QueryFilter>();	
										QueryFilter qf = null;
										
										for (String s : filter.getValues()) {
											QueryFilter qf1 = new QueryFilter(key, dms.getDisplayName(), filter.getOperator(), s, QueryFilter.TYPE_DIMENSION_MEMBER_SET);
											qfList.add(qf1);
										}
										qf = new QueryFilter(filter.getLogicalOperator(), qfList);
										q.addFilter(qf);
									}
									addToDimensionFilters = false;
								}
							}
						}
						if (addToDimensionFilters == true) {
							addMeasureFilters.add(filter);
						}
						continue;
					}
					String dim = key.substring(0, index);
					String col = key.substring(index + 1);
					if ("EXPR".equals(dim)) {
						col = col.replaceAll("'", "''");
						for (DisplayExpression de : aqs.getExpressionList()) {
							if (de.getExpression().equals(col) || de.getExpression().equals("(" + col + ")")) {
								if (filter.getValues().size() > 1) {
									List<DisplayFilter> dfs = new ArrayList<DisplayFilter>();
									for (int j = 0; j < filter.getValues().size(); j++) {
										DisplayFilter df = new DisplayFilter(dim, de.getName(), filter.getOperator(), filter.getValues().get(j));
										dfs.add(df);
									}
									DisplayFilter df = new DisplayFilter(filter.getLogicalOperator(), dfs);
									aqs.getDisplayFilters().add(df);
								}
								else {
									DisplayFilter df = new DisplayFilter(dim, de.getName(), filter.getOperator(), filter.getValues().get(0));
									aqs.getDisplayFilters().add(df);
								}
							}
						}
						continue;
					}
					boolean addToMeasureFilters = true;
					for (DimensionColumnNav dcn : q.dimensionColumns)
					{
						if (dcn.dimensionName.equals(dim) && dcn.columnName.equals(col))
						{
							addToMeasureFilters = false;
							// remove any filter on this column first
							// is this right??
							List<QueryFilter> qfs = q.getSplitFilterList(); // deal with top level AND from new query language
							List<QueryFilter> filtersToRemove = new ArrayList<QueryFilter>();
							for (QueryFilter qf: qfs) {
								if (qf.containsAttribute(dim, col))
									filtersToRemove.add(qf);
							}
							if (filtersToRemove.size() > 0) {
								qfs.removeAll(filtersToRemove);
								q.filters = qfs; // removed the filters from the split
							}

							// okay to add filter - won't change grain of result
							List<String> ps = filter.getValues();
							if (ps.size() == 1) {
								if (filter.getFilterType() == FilterType.DISPLAY && !dcn.navigateonly)
								{
									DisplayFilter df = createDisplayFilter(dim, col, filter.getOperator(), ps.get(0), true); 
									df.setDisplayName(false);
									aqs.getDisplayFilters().add(df);
								}
								else 
								{
									QueryFilter qf = createQueryFilter(dim, col, filter.getOperator(), ps.get(0), true);
									q.addFilter(qf);
								}
							}
							else if(ps.size() > 0){
								if (filter.getFilterType() == FilterType.DISPLAY && !dcn.navigateonly)
								{
									List<DisplayFilter> dfList = new ArrayList<DisplayFilter>();
									for (String s : ps) {
										DisplayFilter df1 = createDisplayFilter(dim, col, filter.getOperator(), s, true); 
										df1.setDisplayName(false);
										dfList.add(df1);
									}
									DisplayFilter df = new DisplayFilter(filter.getLogicalOperator(), dfList);
									aqs.getDisplayFilters().add(df);
								}
								else 
								{
									QueryFilter qf = createDimensionQueryFilter(dim, col, filter, true);
									q.addFilter(qf);
								}
							}
						}
					}
					
					if (q.dimensionMemberSets != null && q.dimensionMemberSets.size() > 0) {
						String[] olapDims = key.split("\\]\\.\\[");
						String olapDim = null;
						if (olapDims.length >= 3) {
							olapDims[0] = olapDims[0].substring(1);
							olapDim = olapDims[0] + "-" + olapDims[1];
						}
						for (DimensionMemberSetNav dms : q.dimensionMemberSets) {
							if (dms.dimensionName.equals(olapDim)) {
								addToMeasureFilters = false;
								// remove any filter on this column first
								// is this right??
								List<QueryFilter> qfs = q.getSplitFilterList(); // deal with top level AND from new query language
								List<QueryFilter> filtersToRemove = new ArrayList<QueryFilter>();
								for (QueryFilter qf: qfs) {
									if (olapDims[0].equals(qf.getDimension()) && olapDims[1].equals(qf.getHierarchy()))
										filtersToRemove.add(qf);
								}
								if (filtersToRemove.size() > 0) {
									qfs.removeAll(filtersToRemove);
									q.filters = qfs; // removed the filters from the split
								}
	
								// okay to add filter - won't change grain of result
								List<String> ps = filter.getValues();
								if (ps.size() == 1) {
									QueryFilter qf = createQueryFilter(olapDims[0], dms.displayName, filter.getOperator(), ps.get(0), true);
									qf.setHierarchy(olapDims[1]);
									qf.setColumnType(QueryFilter.TYPE_DIMENSION_MEMBER_SET);
									q.addFilter(qf);
								}
								else if(ps.size() > 0){
									QueryFilter qf = createDimensionQueryFilter(olapDims[0], dms.displayName, filter, true);
									for (QueryFilter qf1 : qf.getFilterList()) {
										qf1.setHierarchy(olapDims[1]);
										qf1.setColumnType(QueryFilter.TYPE_DIMENSION_MEMBER_SET);
									}
									q.addFilter(qf);
								}
							}
						}
					}
					
					if (addToMeasureFilters == true) {
						addDimensionFilters.add(filter);
					}
				}
				
				// add measure filters that may or may not navigate now
				if (applyAllPrompts == true) {
					for (Filter filter:  addMeasureFilters) {
						String key = filter.getColumnName();
						List<MeasureColumn> mcs = r.findMeasureColumns(key);
						if (mcs == null || mcs.isEmpty())
							continue;
						
						Query cloneQuery = (Query)q.clone();
						QueryFilter qf = createMeasureQueryFilter(key, filter);
						cloneQuery.addFilter(qf);
						
						// now navigate
						try {
							extractMeasures(aqs, cloneQuery);
							cloneQuery.navigateQuery(true, false, this.getSession(), true);
							q.addFilter(qf);
						}
						catch (NavigationException ne) {
							logger.warn("NavigationException " + ne.getMessage() + ": Could not add filter " + qf.toString() + " to query " + query);
						}
						catch (Exception e) {
							logger.warn(e.getMessage() + ": Could not add filter " + qf.toString() + " to query " + query);
						}
					}
				
					for (Filter filter:  addDimensionFilters) {
						String key = filter.getColumnName();
						Query cloneQuery = (Query)q.clone();
						int index = key.indexOf('.');
						String dim = key.substring(0, index);
						String col = key.substring(index + 1);
						
						QueryFilter qf = createDimensionQueryFilter(dim, col, filter);
						if (qf == null)
							continue;
						
						cloneQuery.addFilter(qf);
					
						// now navigate
						try {
							extractMeasures(aqs, cloneQuery);
							cloneQuery.navigateQuery(true, false, this.getSession(), true);
							q.addFilter(qf);
						}
						catch (NavigationException ne) {
							logger.warn("NavigationException " + ne.getMessage() + ": Could not add filter " + qf.toString() + " to query " + query);
						}
						catch (Exception e) {
							logger.warn(e.getMessage() + ": Could not add filter " + qf.toString() + " to query " + query);
						}
					}
					q.clearNavigation();
				}	
			}	
			// See if this is a Web Services Query
			if (aqs.getServerConnectInfo() != null)
				serverConnectInfo = aqs.getServerConnectInfo();
			if (serverConnectInfo != null)
			{
				/*
				 * The following two lines only are needed for SSL: Need to register a new trust manager and host name
				 * verifier. The trust manager will allow all certificates to be trusted (vs. importing the certificates
				 * into a local store and then pointing the jvm to that store as both the key store and the trust store
				 * - which is much more painful for end users). This way, you can just run the software, although it is
				 * slightly less secure. Additionally the hostname verifier guarantees that regardless of the actual
				 * host name used in the URL, it will match the host name in the certficate that is used by the server
				 * (which normally, if different, would generate an error). Again, this is to ease implementation at a
				 * small cost in security.
				 */
				// Remote.registerMyTrustManager();
				// Remote.registerMyHostnameVerifier();
				String url = serverConnectInfo.serverURL + "/GetQuery?username=" + serverConnectInfo.serverUsername + "&password="
						+ serverConnectInfo.serverPassword;
				if (serverConnectInfo.spaceID != null)
					url += "&spaceID=" + URLEncoder.encode(serverConnectInfo.spaceID, "UTF-8");
				if ((splitCacheOnDimension != null) && (splitCacheOnColumn != null))
				{
					url += "&splitcacheondim=" + URLEncoder.encode(splitCacheOnDimension, "UTF-8");
					url += "&splitcacheoncol=" + URLEncoder.encode(splitCacheOnColumn, "UTF-8");
				}
				url += "&query=" + URLEncoder.encode(aqs.getServerQuery(), "UTF-8");
				URL u = new URL(url);
				URLConnection uconn = u.openConnection();
				uconn.connect();
				String type = uconn.getContentType();
				if (type.equals("application/x-smi-bin"))
				{
					ObjectInputStream s = new ObjectInputStream(uconn.getInputStream());
					try
					{
						QueryResultSet qrs = (QueryResultSet) s.readObject();
						s.close();
						qrs.setJasperDataSourceProvider(this);
						qrs.resetCursor(-1);
						return qrs;
					} catch (ClassNotFoundException ex)
					{
						throw (new IOException("Error reading query results from server"));
					}
				} else if (type.startsWith("text/html"))
				{
					BufferedReader reader = new BufferedReader(new InputStreamReader(uconn.getInputStream(),"UTF-8"));
					String line = reader.readLine();
					throw (new NavigationException(line == null || line.equals("null") ? "Unable to obtain results from server" : line));
				}
				return null;
			}
			// If there isn't a currently loaded repository, need to create a new cache
			if (rsc == null && aqs != null)
			{
				// Save the repository that was loaded with the query string
				if (r == null)
				{
					r = aqs.getRepository();
				}
				rsc = ResultSetCache.getInstance(r.getApplicationIdentifier(), false);
			}
			List<DisplayFilter> displayFilters = aqs.getDisplayFilters();
			List<DisplayExpression> expressionList = aqs.getExpressionList();
			List<DisplayOrder> displayOrderList = aqs.getDisplayOrderList();

			Repository rep = q.getRepository();
			int qrsMaxRecords = this.maxRecords == -1 ?  rep.getServerParameters().getMaxRecords() : this.maxRecords;
			Map<DatabaseConnection, List<TempTable>> tempTableMap = null;
			if (session != null)
			{
				session.setRepository(rep);
				if (session.getTempTableMap() == null)
				{
					tempTableMap = new ConcurrentHashMap<DatabaseConnection, List<TempTable>>();
					session.setTempTableMap(tempTableMap);
				}
			}
			if(simulateOuterJoinUsingPersistedDQTs)
			{
				q.setSimulateOuterJoinUsingPersistedDQTs(true);
				q.setExpandDerivedQueryTables(false);
				q.setDerivedQueryTableSuffix("_" + Long.valueOf(System.currentTimeMillis()).toString());				
			}
			if (this.executePhysicalQueryOnly)
			{
				//Do not get QRS
				return null;
			}
			qrs = rsc.getQueryResultSet(q, qrsMaxRecords, (this.queryTimeOut != 0 ? this.queryTimeOut : rep.getServerParameters().getMaxQueryTime()), displayFilters, expressionList, displayOrderList,
					session, (splitCacheOnDimension == null || splitCacheOnColumn == null), false, rep.getServerParameters().getDisplayTimeZone(), false);
			if (qrs == null || !qrs.isValid())
				return null;
			
			if (logger.isInfoEnabled())
				logger.info("Result set received (" + qrs.whereFrom().toString() + ") for query: " + getFullLogicalQueryString(q, aqs, r.isUseNewQueryLanguage()) + " (row count: " + qrs.numRows()
						+ ")");
			if ((splitCacheOnDimension != null) && (splitCacheOnColumn != null) && (qrs != null))
			{
				if (logger.isDebugEnabled())
					logger.debug("Starting to split cache result set for query: " + getFullLogicalQueryString(q, aqs, r.isUseNewQueryLanguage()));
				rsc.cacheSplitResultSet(session, qrs, splitCacheOnDimension, splitCacheOnColumn);
				if (logger.isDebugEnabled())
					logger.debug("Done splitting cache result set for query: " + getFullLogicalQueryString(q, aqs, r.isUseNewQueryLanguage()));
			}
			qrs.setJasperDataSourceProvider(this);
			return qrs;
		} catch (ArrayIndexOutOfBoundsException aie)
		{
			logger.info(aie.getMessage());
			throw aie;
		} catch (NavigationException ne)
		{
			logger.info(ne.getMessage());
			throw ne;
		} catch (SyntaxErrorException se)
		{
			logger.info(se.getMessage());
			throw se;
		} catch (BadColumnNameException se)
		{
			logger.info(se.getMessage());
			throw se;		
		} catch (ScriptException se)
		{
			logger.info(se.getMessage());
			throw se;				
		} catch (QueryCancelledException qce) // logged at the cancel, no need for a stack trace
		{
			throw qce;
		} catch (QueryTimeoutException qtoe) // logged at the failure, no need for a stack trace
		{
			throw qtoe;
		} catch (ResultSetTooBigException rstb) // logged at the failure, no need for a stack trace
		{
			throw rstb;
		} catch (Exception e)
		{
			logger.error(e.getMessage(), e);
			throw e;
		}
	}
	
	private String getFullLogicalQueryString(Query q, AbstractQueryString aqs, boolean isNewQueryLanguage) throws ScriptException
	{
		StringBuilder sb = new StringBuilder();
		if (q != null)
		{
			List<DisplayExpression> expressionList = aqs.getExpressionList();
			sb.append(q.getLogicalQueryString(false, expressionList, false));
		}
		List<DisplayFilter> displayFilters = aqs.getDisplayFilters();
		if (displayFilters != null)
		{
			sb.append(DisplayFilter.getQueryFilters(displayFilters, false, isNewQueryLanguage));
		}
		List<DisplayOrder> displayOrderList = aqs.getDisplayOrderList();
		if (displayOrderList != null)
		{
			sb.append(DisplayOrder.getQueryOrders(displayOrderList, isNewQueryLanguage));
		}
		return sb.toString();
	}
	
	private void extractMeasures(AbstractQueryString lqs, Query cloneQuery) throws NavigationException, BadColumnNameException, CloneNotSupportedException {
		List<String> addedNames = new ArrayList<String>();
		List<DisplayExpression> expressionList = lqs.getExpressionList();
		if (expressionList != null)
		{
			for (DisplayExpression de : expressionList)
			{
				de.extractAllExpressions(cloneQuery, addedNames);
			}
		}
	}
	
	public int executeAndPersistPhysicalQueryToFile(String filename) throws Exception
	{
		int numRows = 0;
		if (!this.executePhysicalQueryOnly)
		{
			throw new Exception("Cannot executeAndPersistPhysicalQueryToFile - executePhysicalQueryOnly not set for JasperDataSourceProvider");
		}
		q.replaceVariables(session);
		q.navigateQuery(false, true, session, true);
		boolean isDbTypeIB = false;
		if (q.fullOuterJoin)
		{
			List<DatabaseConnection> connectionList = q.getConnections();
			for (DatabaseConnection conn : connectionList)
			{
				if (DatabaseConnection.isDBTypeMySQL(conn.DBType))
				{
					isDbTypeIB = true;
					break;
				}			
			}
		}
		if (q.fullOuterJoin && isDbTypeIB)
		{
			q.setSimulateOuterJoinUsingPersistedDQTs(true);
			q.setExpandDerivedQueryTables(false);
			q.setDerivedQueryTableSuffix("_" + Long.valueOf(System.currentTimeMillis()).toString());
		}
		if (session != null)
			q.generateQuery(session, true);
		else
			q.generateQuery(true);
		String query = q.getQuery();
		if (query == null || query.isEmpty())
		{
			throw new Exception("Cannot executeAndPersistPhysicalQueryToFile - Generated Physical query is invalid - " + query);
		}
		if (!q.available())
		{
			logger.warn("The query syntax was incorrect: " + query);
			throw (new DataUnavailableException("Data unavailable to satisfy request: ", new Exception(
					"Query has a navigation pointing to database connection type 'None'.")));
		}
		QueryResultSet qrs = new QueryResultSet(q, maxRecords, (this.queryTimeOut != 0 ? this.queryTimeOut : r.getServerParameters().getMaxQueryTime()), session, true, filename, userDisplayTimezone);
		if (qrs != null && qrs.isValid())
		{
			numRows = qrs.getPhysicalQueryNumRows();
		}
		else
		{
			throw new Exception("Cannot executeAndPersistPhysicalQueryToFile - query result set is NULL or INVALID");
		}
		return numRows;
	}
	
	
	private QueryFilter createDimensionQueryFilter(String dim, String col, Filter filter){
		return createDimensionQueryFilter(dim, col, filter, true);
	}
	
	private QueryFilter createMeasureQueryFilter(String key, Filter filter){
		return createDimensionQueryFilter(null, key, filter, false);
	}
	
	private QueryFilter createQueryFilter(String dim, String col, String operator, String value, boolean isDimension) {
		if ("null".equals(value)) {
			if ("=".equals(operator)) {
				if (isDimension) {
					return new QueryFilter(dim, col, QueryFilter.ISNULL, value);
				}
				return new QueryFilter(col, QueryFilter.ISNULL, value);
			}
			else if ("<>".equals(operator)) {
				if (isDimension) {
					return new QueryFilter(dim, col, QueryFilter.ISNOTNULL, value);
				}
				return new QueryFilter(col, QueryFilter.ISNOTNULL, value);
			}
		}
		return (isDimension) ? 
				   new QueryFilter(dim, col, operator, value) :
				   new QueryFilter(col, operator, value);		
	}
	
	private DisplayFilter createDisplayFilter(String dim, String col, String operator, String value, boolean isDimension) {
		if ("null".equals(value)) {
			if ("=".equals(operator)) {
				if (isDimension) {
					return new DisplayFilter(dim, col, QueryFilter.ISNULL, value);
				}
				return new DisplayFilter(col, QueryFilter.ISNULL, value);
			}
			else if ("<>".equals(operator)) {
				if (isDimension) {
					return new DisplayFilter(dim, col, QueryFilter.ISNOTNULL, value);
				}
				return new DisplayFilter(col, QueryFilter.ISNOTNULL, value);
			}
		}
		return (isDimension) ? 
				   new DisplayFilter(dim, col, operator, value) :
				   new DisplayFilter(col, operator, value);		
	}
	
	private QueryFilter createDimensionQueryFilter(String dim, String col, Filter filter, boolean isDimension){
		List<String> ps = filter.getValues();
		if (ps.size() == 1) {
			return createQueryFilter(dim, col, filter.getOperator(), ps.get(0), isDimension);
		} else if(ps.size() > 0){
			List<QueryFilter> qfList = new ArrayList<QueryFilter>();	
			QueryFilter qf = null;
			
			//For Range operation (from Slider Prompt of type Range)
			// >= Value1 && <= Value2
			if(filter.getOperator().equals("Range") && ps.size() >= 2){
				qf = createRangeFilter(dim, col, qfList, ps);
			}else{
				for (String s : ps) {
					QueryFilter qf1 = createQueryFilter(dim, col, filter.getOperator(), s, isDimension); 
					qfList.add(qf1);
				}
				qf = new QueryFilter(filter.getLogicalOperator(), qfList);
			}
			return qf;
		}
		
		return null;
	}
	
	private DisplayFilter createMeasureDisplayFilter(String key, Filter filter){
		return createDisplayFilter(null, key, filter, false);
	}
	
	private DisplayFilter createDisplayFilter(String dim, String col, Filter filter, boolean isDimension){
		List<String> ps = filter.getValues();
		if (ps.size() == 1) {
			DisplayFilter df = (isDimension) ? 
								new DisplayFilter(dim, col, filter.getOperator(), ps.get(0)) :
								new DisplayFilter(col, filter.getOperator(), ps.get(0));
			df.setDisplayName(false);
			return df;
		}
		else if(ps.size() > 0){
			List<DisplayFilter> dfList = new ArrayList<DisplayFilter>();
			for (String s : ps) {
				DisplayFilter df1 = (isDimension) ?
									new DisplayFilter(dim, col, filter.getOperator(), s) :
									new DisplayFilter(col, filter.getOperator(), s);
				df1.setDisplayName(false);
				dfList.add(df1);
			}
			DisplayFilter df = new DisplayFilter(filter.getLogicalOperator(), dfList);
			return df;
		}
		return null;
	}
	
	public static QueryFilter createRangeFilter(String dim, String col, List<QueryFilter> qfList, List<String> ps){
		QueryFilter min = (dim != null) ? new QueryFilter(dim, col, ">=", ps.get(0)) : new QueryFilter(col, ">=", ps.get(0));
		qfList.add(min);
		QueryFilter max = (dim != null) ? new QueryFilter(dim, col, "<=", ps.get(1)) : new QueryFilter(col, "<=", ps.get(1));
		qfList.add(max);
		return new QueryFilter("AND", qfList);
	}

	/**
	 * Return the query string for a report with parameters replaced
	 * 
	 * @param report
	 * @param parameters
	 * @param session
	 * @return
	 */
	public String getReportQueryString(JasperReport report, @SuppressWarnings("rawtypes") Map parameters)
	{
		if (report.getQuery() == null)
			return (null);
		String query = report.getQuery().getText();
		// Do any parameter replacement necessary
		JRParameter[] jp = report.getParameters();
		if (jp != null)
		{
			for (int i = 0; i < jp.length; i++)
			{
//				if (jp[i].getDefaultValueExpression() != null)
				{
					String pname = "$P{" + jp[i].getName() + "}";
					String rvalue = null;
					String expr = jp[i].getDefaultValueExpression() == null ? QueryString.NO_FILTER_TEXT : jp[i].getDefaultValueExpression().getText();
					if (jp[i].getValueClass() == String.class)
					{
						rvalue = expr.replaceAll("\"", "");
					} else if (jp[i].getValueClass() == Integer.class)
					{
						int pos = expr.indexOf("new Integer(");
						int epos = -1;
						if (pos >= 0)
							epos = expr.indexOf(')', pos);
						if (epos >= 0)
							rvalue = expr.substring(pos + 12, epos);
					}
					if (rvalue != null)
					{
						/*
						 * See if parameter is overridden
						 */
						if (parameters != null)
						{
							Object pval = parameters.get(jp[i].getName());
							if (pval != null)
								rvalue = pval.toString();
						}
						String pname1 = "\"\\s*\\+\\s*\\$P\\{" + jp[i].getName() + "\\}\\s*\\+\\s*\"";
						query = query.replaceAll(pname1, rvalue);
						
						int pos = query.indexOf(pname);
						while (pos >= 0)
						{
							query = query.substring(0, pos) + rvalue + query.substring(pos + pname.length());
							pos = query.indexOf(pname);
						}
					}
				}
			}
		}
		return (query);
	}

	/**
	 * Create a data source based on the current report. Parse the query string and create a new query structure. Query
	 * result set cache to get results and return. If the query string is equal to "SingleRow{}" then a simple, single
	 * row data source is returned.
	 * 
	 * @return Jasper data source
	 * @see net.sf.jasperreports.engine.JRDataSourceProvider#create(net.sf.jasperreports.engine.JasperReport)
	 */
	public JRDataSource create(JasperReport report, @SuppressWarnings("rawtypes") Map parameters) throws Exception
	{
		String queryString = getReportQueryString(report, parameters);
		if (queryString == null || !Util.hasNonWhiteSpaceCharacters(queryString))
		{
			QueryResultSet qrs = (new QueryResultSet());
			qrs.jdsp = this;
			return qrs;
		}
		if (queryString.equals("SingleRow{}"))
			return (new SingleRowDataSource(this));
		return (create(queryString));
	}

	public JRDataSource create(JasperReport report) throws JRException
	{
		try
		{
			return (create(report, null));
		} catch (Exception ex)
		{
			throw (new JRException(ex));
		}
	}

	/**
	 * Return the column format for a given measure
	 * 
	 * @param colName
	 * @return
	 */
	public String getMeasureFormat(String colName)
	{
		MeasureColumn mc = r.findMeasureColumn(colName);
		if (mc == null)
			return ("");
		return (mc.Format);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.sf.jasperreports.engine.JRDataSourceProvider#dispose(net.sf.jasperreports.engine.JRDataSource)
	 */
	public void dispose(JRDataSource arg0) throws JRException
	{
	}

	/**
	 * @return Returns the session.
	 */
	public Session getSession()
	{
		return session;
	}

	/**
	 * @param session
	 *            The session to set.
	 */
	public void setSession(Session session)
	{
		this.session = session;
	}

	/**
	 * @return Returns the r.
	 */
	public Repository getRepository()
	{
		return r;
	}

	/**
	 * @return Returns the query object.
	 */
	public Query getQuery()
	{
		return q;
	}

	public void setMaxRecords(int maxRecords)
	{
		this.maxRecords = maxRecords;
	}

	public QueryResultSet getQueryResultSet()
	{
		return qrs;
	}

	public List<Filter> getParams() {
		return params;
	}

	public void setParams(List<Filter> params) {
		this.params = params;
	}

	public void setApplyAllPrompts(boolean applyAllPrompts) {
		this.applyAllPrompts = applyAllPrompts;
	}
	
	public void setDimensionMemberSetDataTypeMap(Map<String, String> dimensionMemberSetDataTypeMap) {
		this.dimensionMemberSetDataTypeMap = dimensionMemberSetDataTypeMap;
	}
	
	public void setDimensionMemberSetFormatMap(Map<String, String> dimensionMemberSetFormatMap) {
		this.dimensionMemberSetFormatMap = dimensionMemberSetFormatMap;
	}

	/**
	 * @return the rsc
	 */
	public ResultSetCache getResultSetCache() {
		return rsc;
	}
}
