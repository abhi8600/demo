package com.successmetricsinc.query;

import com.mongodb.BasicDBObject;

public interface BasicDBObjectSerializer {
	void fromBasicDBObject(BasicDBObject obj);
	BasicDBObject toBasicDBObject();
}
