/**
 * $Id: DatabaseConnectionStatement.java,v 1.36 2012-10-18 10:22:05 BIRST\cpatel Exp $
 *
 * Copyright (C) 2007-2009 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.liveaccessdirectconnect.DirectConnectUtil;
import com.successmetricsinc.query.Database.DatabaseType;
import com.successmetricsinc.security.EncryptionService;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.Util;

public class DatabaseConnectionStatement
{
	private static Logger logger = Logger.getLogger(DatabaseConnectionStatement.class);

	public enum TYPE
	{
		Database, Realtime, GoogleAnalytics
	};
	private TYPE type;
	private Statement stmt;
	private DatabaseConnectionStatement[] statements;
	private String serverName;
	private int port;
	private String id;
	private String username;
	private String password;
	private String connectionName;
	private String connectString;
	private DatabaseConnection databaseConnection;
	private String qstring;
	private com.birst.dataconductor.PutResults pr;
	private Map<Integer, String> StatementIndexToConnectionNameMap = new HashMap<Integer, String>(); 
	static
	{
		// We need to keep this encryptionkey in synch with MainAdminForm.SecretPassword
		com.birst.dataconductor.EncryptionService.setKey("encryptionkey=kashdfoiwuehrqweinauvnaweu");
	}

	public DatabaseConnectionStatement(DatabaseConnection databaseConnection, Statement stmt, Repository r)
	{
		this.type = TYPE.Database;
		this.databaseConnection = databaseConnection;
		if (databaseConnection.isPartitioned)
		{
			statements = new DatabaseConnectionStatement[databaseConnection.partitionConnections.length];
			for (int i = 0; i < statements.length; i++)
			{
				try
				{
					Connection conn = ConnectionPool.getParallelOrDynamicConnection(databaseConnection.partitionConnections[i], null, true);
					statements[i] = new DatabaseConnectionStatement(databaseConnection.partitionConnections[i], conn.createStatement(), r);
				} catch (SQLException e)
				{
					logger.error(e);
				}
			}
		} else if(databaseConnection.containsFederatedJoinConnections())
		{
			statements = new DatabaseConnectionStatement[databaseConnection.federatedJoinConnections.length];
			for (int i = 0; i < statements.length; i++)
			{
				try
				{					
					if(databaseConnection.federatedJoinConnections[i].isRealTimeConnection())
					{
						statements[i] = new DatabaseConnectionStatement(r, databaseConnection.federatedJoinConnections[i]);
					}
					else
					{
						Connection conn = ConnectionPool.getParallelOrDynamicConnection(databaseConnection.federatedJoinConnections[i], null, true);
						statements[i] = new DatabaseConnectionStatement(databaseConnection.federatedJoinConnections[i], conn.createStatement(), r);
					}
					StatementIndexToConnectionNameMap.put(i, databaseConnection.federatedJoinConnections[i].Name);
				} catch (SQLException e)
				{
					logger.error(e);
				}
			}
		}
		else
		{
			this.stmt = stmt;
		}
	}

	public DatabaseConnectionStatement(String serverName, int port, String id, String connectionName)
	{
		this.type = TYPE.Realtime;
		this.serverName = serverName;
		this.port = port;
		this.id = id;
		this.connectionName = connectionName;
	}

	public DatabaseConnectionStatement(Repository r, DatabaseConnection dc) throws SQLException
	{
		id = dc.ID;
		databaseConnection = dc;
		connectString = dc.ConnectString;
		if (dc.DBType == DatabaseType.GoogleAnalytics)
		{
			type = TYPE.GoogleAnalytics;
			connectionName = dc.Name;
			username = dc.UserName;
			password = dc.Password;
			id = dc.ID;
			connectString = dc.ConnectString;
		} else
		{
			connectionName = dc.Name;
			type = TYPE.Realtime;
			if (r.isProxyDatabaseConnectionAvailable())
			{
				Connection conn = null;
				Statement sstmt = null;
				ResultSet rs = null;
				try
				{
					conn = DriverManager.getConnection(r.getProxyDatabaseConnectString(),
							EncryptionService.getInstance().decrypt(r.getProxyDatabaseUsername()),
							EncryptionService.getInstance().decrypt(r.getProxyDatabasePassword()));
					sstmt = conn.createStatement();
					rs = sstmt.executeQuery("SELECT Server, Port FROM DBO.PROXY_REGISTRATIONS WHERE ID='" + id + "/" + connectionName + "'");
					if (rs.next())
					{
						serverName = rs.getString(1);
						port = rs.getInt(2);
					} else
					{
						logger.error("No results for fetching proxy registrations: connect string:" + r.getProxyDatabaseConnectString() + ", id: " + id
								+ ", connectionName: " + connectionName);
					}
				} catch (SQLException e)
				{
					logger.error("Exception in getting proxy registrations: connectstring: " + r.getProxyDatabaseConnectString() + ", id: " + id
							+ ", connectionName: " + connectionName);
					throw new SQLException(e);
				} finally
				{
					try
					{
						if (rs != null)
							rs.close();
						if (sstmt != null)
							sstmt.close();
						if (conn != null)
							conn.close();
					} catch (SQLException e)
					{
						throw new SQLException(e);
					}
				}
			} else
			{
				logger.error("Proxy registration database connection details not provided in repository.");
			}
		}
	}

	public TYPE getType()
	{
		return type;
	}

	public void setType(TYPE type)
	{
		this.type = type;
	}

	public boolean isDatabaseType()
	{
		return type == TYPE.Database;
	}

	public Connection getConnection() throws SQLException
	{
		return stmt.getConnection();
	}

	public void cancel() throws SQLException
	{
		if (DatabaseConnection.isDBTypeXMLA(databaseConnection.DBType) && pr != null)
			pr.kill();
		else
			stmt.cancel();
	}

	public void setMaxRows(int maxRows) throws SQLException
	{
		if (type == TYPE.Database && stmt != null && !DatabaseConnection.isDBTypeXMLA(databaseConnection.DBType))
			stmt.setMaxRows(maxRows);
	}

	public int getMaxRows() throws SQLException
	{
		if (type == TYPE.Database && stmt != null && !DatabaseConnection.isDBTypeXMLA(databaseConnection.DBType))
			return stmt.getMaxRows();
		return 0;
	}

	public void setQueryTimeout(int timeout) throws SQLException
	{
		if (type == TYPE.Database && databaseConnection.supportsQueryTimeOut() && stmt != null)
			stmt.setQueryTimeout(timeout);
	}

	/**
	 * PLACEHOLDER IMPLEMENTATION - DO NOT USE FOR PRODUCTION
	 * This method should contain logic to execute physical queries from federated join query helper and merge
	 * @param fjqh
	 * @return
	 */
	public DatabaseConnectionResultSet executeQuery(FederatedJoinQueryHelper fjqh, Query q, int maxRecords, int maxQueryTime, Session session) throws SQLException, DataUnavailableException, BaseException
	{
		try{
			//Use connection name to statement index map to figure out which statement needs to be used for each physical query from
			//fjqh. Execute each query and merge the results with aggregations etc.
			if(fjqh.connectionToQueryMap.size() != statements.length)
			{
				throw new NavigationException("Physical query for each statement not available for federated join navigation.");
			}
			if(session != null)
			{
				for(DatabaseConnectionStatement stmt : statements)
				{
					session.addCurrentStatement(stmt);
				}
			}
			String [] physicalQueries = new String[statements.length];
			for(int stmtIdx = 0; stmtIdx < statements.length; stmtIdx++)
			{
				String connName = StatementIndexToConnectionNameMap.get(stmtIdx);
				physicalQueries[stmtIdx] = fjqh.connectionToQueryMap.get(connName);
			}
			
			DatabaseConnectionResultSet [] resultSets = fjqh.executeQueriesAndGetResultSets(q, statements, physicalQueries, maxRecords, maxQueryTime);
			
			DatabaseConnectionResultSet dcrs = fjqh.getMergedResultSets(resultSets);
			return dcrs;
		} finally
		{
			if (session != null)
			{
				for (DatabaseConnectionStatement stmt : statements)
				{
					session.removeStatement(stmt);
				}
			}
		}
	}
	
	public DatabaseConnectionResultSet executeQuery(String qstring, Query q) throws SQLException, DataUnavailableException, GoogleAnalyticsException, BaseException
	{
		if ((type == TYPE.Database) || (type == TYPE.Realtime && databaseConnection.useDirectConnection))
		{
			if (DatabaseConnection.isDBTypeXMLA(databaseConnection.DBType))
			{
				// connect to cube and retrive results for mdx
				com.birst.dataconductor.DatabaseConnection rDC = DirectConnectUtil.getBCDatabaseConnection(databaseConnection, q.getSession());
				this.pr = new com.birst.dataconductor.PutResults(databaseConnection.Name, rDC, null, -1, "query", q.getRepository().getServerParameters()
						.getProcessingTimeZone(), qstring, null, null, null, null,q.getRepository().getServerParameters().getMaxRecords());
				return (new DatabaseConnectionResultSet(databaseConnection, q.getSession(), stmt, pr, qstring));
			}
			if (databaseConnection.isPartitioned)
			{
				return getCombinedResultSet(qstring, q);
			}
			return (new DatabaseConnectionResultSet(stmt.executeQuery(qstring), databaseConnection));
		} else if (type == TYPE.Realtime)
		{
			if (id == null || serverName == null)
			{
				throw new DataUnavailableException("ID or server name not available for real time connect. id:" + id + ",server:" + serverName);
			}
			logger.debug("id:" + id + ",server:" + serverName);
			if (!isRealTimeConnectionAlive(serverName, port, id, connectionName))
				throw new DataUnavailableException("Real time connect server not responding. id:" + id + ",server:" + serverName);
			this.qstring = qstring;
			DatabaseConnectionResultSet dcrs = new DatabaseConnectionResultSet(serverName, port, id, connectionName, qstring, q.getRepository()
					.getServerParameters().getProcessingTimeZone(), databaseConnection, q.getSession(),q.getRepository().getServerParameters().getMaxRecords());
			return dcrs;
		} else if (type == TYPE.GoogleAnalytics)
		{
			DatabaseConnectionResultSet dcrs = new DatabaseConnectionResultSet(username, password, id, connectString, qstring, q);
			return dcrs;
		}
		return null;
	}

	private DatabaseConnectionResultSet getCombinedResultSet(String query, Query q) throws DataUnavailableException, GoogleAnalyticsException, SQLException
	{
		int colCount = q.getNumColumns();
		/*
		 * Test to see if there are averages
		 */
		boolean hasAverages = false;
		for (int i = 0; i < colCount; i++)
		{
			QueryColumn qc = q.getProjectionListQueryColumn(i);
			if (qc.o instanceof MeasureColumn)
			{
				MeasureColumn mc = (MeasureColumn) qc.o;
				if (mc.AggregationRule.equals("AVG"))
				{
					hasAverages = true;
					break;
				}
			}
		}
		if (hasAverages)
		{
			// Add additional columns to the query for counts
			Query newq = null;
			try
			{
				newq = (Query) q.clone();
			} catch (CloneNotSupportedException e)
			{
				logger.error(e);
				throw new DataUnavailableException(e.getMessage());
			}
			for (int i = 0; i < colCount; i++)
			{
				QueryColumn qc = newq.getProjectionListQueryColumn(i);
				if (qc.o instanceof MeasureColumn)
				{
					MeasureColumn mc = (MeasureColumn) qc.o;
					if (mc.AggregationRule.equals("AVG"))
					{
						MeasureColumn sumMeasure = getMeasureColumnWithSpecificAggregationRule(mc, "SUM");
						MeasureColumn countMeasure = getMeasureColumnWithSpecificAggregationRule(mc, "COUNT");
						if (sumMeasure == null || countMeasure == null)
							throw new DataUnavailableException("Unable to locate measure with appropriate aggregation rule (SUM or COUNT): " + mc.ColumnName);
						for (MeasureColumnNav mcn : newq.measureColumns)
						{
							/*
							 * Flip measure from average to sum, and add a count
							 */
							if (mcn.measureName.equals(mc.ColumnName))
							{
								mcn.measureName = sumMeasure.ColumnName;
								newq.addMeasureColumn(countMeasure.ColumnName, "ADD:" + countMeasure.ColumnName, false);
								break;
							}
						}
						qc.o = sumMeasure;
					}
				}
			}
			try
			{
				newq.generateQuery(q.getSession(), true);
				query = newq.getQuery();
			} catch (ArrayIndexOutOfBoundsException | BaseException | CloneNotSupportedException | IOException e)
			{
				logger.error(e);
				throw new DataUnavailableException(e.getMessage());
			}
		}
		DatabaseConnectionResultSet[] resultSets = new DatabaseConnectionResultSet[databaseConnection.partitionConnections.length];
		GetResultsThread[] threads = new GetResultsThread[resultSets.length];
		LinkedBlockingQueue<Map<String, Object[]>> mergeQueue = new LinkedBlockingQueue<Map<String, Object[]>>();
		AtomicInteger toBeProcessed = new AtomicInteger(threads.length);
		/*
		 * Tag each column as 0 - not measure, 1 - additive measure, 2 - average, 3 - min, 4 - max
		 */
		int[] measureTypes = new int[colCount];
		/*
		 * Tag each column as true if it is coming from a partitioned tables, false otherwise
		 */
		boolean[] isPartitioned = new boolean[colCount];
		for (int i = 0; i < colCount; i++)
		{
			if (q.getProjectionListQueryColumn(i).o instanceof MeasureColumn)
			{
				MeasureColumn mc = (MeasureColumn) q.getProjectionListQueryColumn(i).o;
				if (mc.AggregationRule.equals("SUM"))
					measureTypes[i] = 1;
				else if (mc.AggregationRule.equals("AVG"))
					measureTypes[i] = 2;
				else if (mc.AggregationRule.equals("MIN"))
					measureTypes[i] = 3;
				else if (mc.AggregationRule.equals("MAX"))
					measureTypes[i] = 4;
				else
					measureTypes[i] = 1;
				if (mc.MeasureTable != null && mc.MeasureTable.IsPartitioned)
					isPartitioned[i] = true;
				else
					isPartitioned[i] = false;
			} else
			{
				if (q.getProjectionListQueryColumn(i).o instanceof DimensionColumn)
				{
					DimensionColumn dc = (DimensionColumn) q.getProjectionListQueryColumn(i).o;
					if (dc.DimensionTable != null && dc.DimensionTable.IsPartitioned)
						isPartitioned[i] = true;
					else
						isPartitioned[i] = false;
				}
				measureTypes[i] = 0;
			}
		}
		/*
		 * Fetch individual result sets
		 */
		for (int i = 0; i < threads.length; i++)
		{
			threads[i] = new GetResultsThread(statements, resultSets, i, query, q, mergeQueue, measureTypes);
			threads[i].start();
		}
		/*
		 * Merge the resultsets
		 */
		MergeResultSets[] mergeThreads = new MergeResultSets[Runtime.getRuntime().availableProcessors()];
		for (int i = 0; i < mergeThreads.length; i++)
		{
			mergeThreads[i] = new MergeResultSets(toBeProcessed, mergeQueue, measureTypes, isPartitioned);
			mergeThreads[i].start();
		}
		synchronized (toBeProcessed)
		{
			while (toBeProcessed.get() > 1)
			{
				try
				{
					toBeProcessed.wait();
				} catch (InterruptedException e)
				{
					logger.error(e);
					return null;
				}
			}
		}
		for (int i = 0; i < threads.length; i++)
		{
			if (threads[i].getException() != null)
				throw new DataUnavailableException(threads[i].getException().getMessage());
		}
		for (int i = 0; i < mergeThreads.length; i++)
		{
			if (mergeThreads[i].getException() != null)
				throw new DataUnavailableException(mergeThreads[i].getException().getMessage());
		}
		logger.info("Merged partition result sets");
		/*
		 * Combine these result sets
		 */
		int[] dataTypes = new int[resultSets[0].getColumnCount()];
		for (int i = 0; i < dataTypes.length; i++)
		{
			ResultSetMetaData rsmd = resultSets[0].getMetaData();
			dataTypes[i] = rsmd.getColumnType(i + 1);
		}
		List<Object[]> resultRows = new ArrayList<Object[]>();
		try
		{
			Map<String, Object[]> finalResult = mergeQueue.take();
			for (Entry<String, Object[]> entry : finalResult.entrySet())
			{
				Object[] curRow = entry.getValue();
				int avgCount = 0;
				for (int i = 0; i < measureTypes.length; i++)
				{
					if (measureTypes[i] == 2)
					{
						/*
						 * Calculate final averages
						 */
						Object sumOfCount = curRow[colCount + (avgCount++)];
						long longSumOfCount = 0;
						if (sumOfCount instanceof Long)
							longSumOfCount = (Long) sumOfCount;
						else if (sumOfCount instanceof Integer)
							longSumOfCount = (Integer) sumOfCount;
						else if (sumOfCount instanceof Double)
							longSumOfCount = ((Double) sumOfCount).longValue();
						else if (sumOfCount instanceof Float)
							longSumOfCount = ((Float) sumOfCount).longValue();
						if (longSumOfCount != 0L)
						{
							if (curRow[i] instanceof Integer)
							{
								curRow[i] = ((Integer) curRow[i]) / longSumOfCount;
							} else if (curRow[i] instanceof Double)
							{
								curRow[i] = ((Double) curRow[i]) / longSumOfCount;
							} else if (curRow[i] instanceof Float)
							{
								curRow[i] = ((Float) curRow[i]) / longSumOfCount;
							}
						} else
							curRow[i] = null;
					}
				}
				resultRows.add(entry.getValue());
			}
		} catch (InterruptedException e)
		{
			logger.error(e);
			throw new DataUnavailableException(e.getMessage());
		}
		return new DatabaseConnectionResultSet(dataTypes, resultRows);
	}

	private MeasureColumn getMeasureColumnWithSpecificAggregationRule(MeasureColumn mc, String aggRule)
	{
		for (MeasureColumn smc : mc.MeasureTable.MeasureColumns)
		{
			if (smc.PhysicalName.equals(mc.PhysicalName) && smc.AggregationRule.equals(aggRule))
				return smc;
		}
		return null;
	}

	private static class GetResultsThread extends Thread
	{
		private DatabaseConnectionStatement[] statements;
		private DatabaseConnectionResultSet[] resultSets;
		private LinkedBlockingQueue<Map<String, Object[]>> mergeQueue;
		private int index;
		private String query;
		private Query q;
		private Exception ex;
		private int[] measureTypes;

		public GetResultsThread(DatabaseConnectionStatement[] statements, DatabaseConnectionResultSet[] resultSets, int index, String query, Query q,
				LinkedBlockingQueue<Map<String, Object[]>> mergeQueue, int[] measureTypes)
		{
			this.statements = statements;
			this.resultSets = resultSets;
			this.mergeQueue = mergeQueue;
			this.index = index;
			this.query = query;
			this.q = q;
			this.measureTypes = measureTypes;
		}

		public void run()
		{
			try
			{
				long startTime = System.currentTimeMillis();
				DatabaseConnectionResultSet result = statements[index].executeQuery(query, q);
				resultSets[index] = result;
				int colCount = result.getColumnCount();
				Map<String, Object[]> mergeResult = new HashMap<String, Object[]>();
				while (result.next())
				{
					// Create merge key and result row
					StringBuilder sb = new StringBuilder();
					Object[] row = new Object[colCount];
					for (int i = 0; i < colCount; i++)
					{
						Object val = result.getObject(i + 1);
						if (i < measureTypes.length && measureTypes[i] == 0)
						{
							if (val == null)
							{
								sb.append(Util.BIRST_NULL);
							} else
							{
								sb.append(result.getObject(i + 1).toString());
							}
						}
						row[i] = val;
					}
					mergeResult.put(sb.toString(), row);
				}
				logger.info("Executed query for partition " + index + " (" + (System.currentTimeMillis() - startTime) + "ms)");
				mergeQueue.add(mergeResult);
				if (mergeQueue.size() > 1)
				{
					synchronized (mergeQueue)
					{
						mergeQueue.notify();
					}
				}
			} catch (DataUnavailableException e)
			{
				logger.error(e);
				ex = e;
			} catch (GoogleAnalyticsException e)
			{
				logger.error(e);
				ex = e;
			} catch (BaseException e)
			{
				logger.error(e);
				ex = e;
			} catch (SQLException e)
			{
				logger.error(e);
				ex = e;
			}
		}

		public Exception getException()
		{
			return ex;
		}
	}

	private static class MergeResultSets extends Thread
	{
		private AtomicInteger toBeProcessed;
		private LinkedBlockingQueue<Map<String, Object[]>> mergeQueue;
		private Exception ex;
		private int[] measureTypes;
		private boolean[] isPartitioned;

		public MergeResultSets(AtomicInteger toBeProcessed, LinkedBlockingQueue<Map<String, Object[]>> mergeQueue, int[] measureTypes, boolean[] isPartitioned)
		{
			this.toBeProcessed = toBeProcessed;
			this.mergeQueue = mergeQueue;
			this.measureTypes = measureTypes;
			this.isPartitioned = isPartitioned;
		}

		public void run()
		{
			synchronized (mergeQueue)
			{
				try
				{
					mergeQueue.wait();
				} catch (InterruptedException e)
				{
					logger.error(e);
					ex = e;
					return;
				}
				Map<String, Object[]> result1 = null;
				Map<String, Object[]> result2 = null;
				try
				{
					try
					{
						result1 = mergeQueue.take();
						result2 = mergeQueue.take();
						for (Entry<String, Object[]> entry : result2.entrySet())
						{
							String key = entry.getKey();
							Object[] curRow = result1.get(key);
							if (curRow == null)
							{
								result1.put(key, entry.getValue());
							} else
							{
								Object[] otherRow = entry.getValue();
								for (int i = 0; i < curRow.length; i++)
								{
									if (i >= measureTypes.length || measureTypes[i] > 0)
									{
										if (i >= measureTypes.length || measureTypes[i] == 1 || measureTypes[i] == 2)
										{
											if (curRow[i] == null)
												curRow[i] = otherRow[i];
											else if (isPartitioned[i])
											{
												if (curRow[i] instanceof Integer && otherRow[i] != null)
												{
													curRow[i] = (Integer) curRow[i] + (Integer) otherRow[i];
												} else if (curRow[i] instanceof Long && otherRow[i] != null)
												{
													curRow[i] = (Long) curRow[i] + (Long) otherRow[i];
												} else if (curRow[i] instanceof Double && otherRow[i] != null)
												{
													curRow[i] = (Double) curRow[i] + (Double) otherRow[i];
												} else if (curRow[i] instanceof Float && otherRow[i] != null)
												{
													curRow[i] = (Float) curRow[i] + (Float) otherRow[i];
												}
											}
										} else if (measureTypes[i] == 3)
										{
											// Min
											if (curRow[i] == null)
												curRow[i] = otherRow[i];
											else if (curRow[i] instanceof Integer && otherRow[i] != null)
											{
												curRow[i] = (Integer) curRow[i] < (Integer) otherRow[i] ? (Integer) curRow[i] : (Integer) otherRow[i];
											} else if (curRow[i] instanceof Long && otherRow[i] != null)
											{
												curRow[i] = (Long) curRow[i] < (Long) otherRow[i] ? (Long) curRow[i] : (Long) otherRow[i];
											} else if (curRow[i] instanceof Double && otherRow[i] != null)
											{
												curRow[i] = (Double) curRow[i] < (Double) otherRow[i] ? (Double) curRow[i] : (Double) otherRow[i];
											} else if (curRow[i] instanceof Float && otherRow[i] != null)
											{
												curRow[i] = (Float) curRow[i] < (Float) otherRow[i] ? (Float) curRow[i] : (Float) otherRow[i];
											}
										} else if (measureTypes[i] == 4)
										{
											// Max
											if (curRow[i] == null)
												curRow[i] = otherRow[i];
											else if (curRow[i] instanceof Integer && otherRow[i] != null)
											{
												curRow[i] = (Integer) curRow[i] > (Integer) otherRow[i] ? (Integer) curRow[i] : (Integer) otherRow[i];
											} else if (curRow[i] instanceof Long && otherRow[i] != null)
											{
												curRow[i] = (Long) curRow[i] > (Long) otherRow[i] ? (Long) curRow[i] : (Long) otherRow[i];
											} else if (curRow[i] instanceof Double && otherRow[i] != null)
											{
												curRow[i] = (Double) curRow[i] > (Double) otherRow[i] ? (Double) curRow[i] : (Double) otherRow[i];
											} else if (curRow[i] instanceof Float && otherRow[i] != null)
											{
												curRow[i] = (Float) curRow[i] > (Float) otherRow[i] ? (Float) curRow[i] : (Float) otherRow[i];
											}
										}
									}
								}
							}
						}
					} catch (InterruptedException e)
					{
						logger.error(e);
						ex = e;
						return;
					}
					try
					{
						mergeQueue.put(result1);
					} catch (InterruptedException e)
					{
						logger.error(e);
						ex = e;
						return;
					}
				} finally
				{
					toBeProcessed.decrementAndGet();
					synchronized (toBeProcessed)
					{
						toBeProcessed.notify();
					}
				}
			}
		}

		public Exception getException()
		{
			return ex;
		}
	}

	private static Socket getSocket(String serverName, int port) throws IOException
	{
		Socket s = new Socket();
		s.setSoLinger(false, 0);
		s.setSoTimeout(30 * 1000);
		s.connect(new InetSocketAddress(serverName, port));
		return s;
	}

	private static byte[] getConnectionString(String id, String connectionName) throws UnsupportedEncodingException
	{
		return (id + "/" + connectionName + "\r\n").getBytes("UTF-8");
	}

	public static void writeRequest(OutputStream os, String id, String connectionName, String body) throws UnsupportedEncodingException, IOException
	{
		os.write(getConnectionString(id, connectionName));
		os.write((body.length() + "\r\n").getBytes("UTF-8"));
		os.write(body.getBytes("UTF-8"));
	}

	public static int getLength(InputStream is) throws IOException
	{
		int length;
		length = is.read();
		// Get 32 bit length
		length = length << 8;
		length += is.read();
		length = length << 8;
		length += is.read();
		length = length << 8;
		length += is.read();
		return length;
	}

	public static byte[] readBuffer(InputStream is, int length) throws IOException
	{
		byte[] buff = new byte[length];
		int pos = 0;
		while (pos < length)
		{
			int num = is.read(buff, pos, Math.min(length - pos, 65535));
			pos += num;
		}
		return buff;
	}

	public static boolean isRealTimeConnectionAlive(String serverName, int port, String id, String connectionName)
	{
		Socket s = null;
		OutputStream os = null;
		InputStream is = null;
		try
		{
			s = getSocket(serverName, port);
			os = s.getOutputStream();
			is = s.getInputStream();
			String body = "ping";
			writeRequest(os, id, connectionName, body);
			int length = getLength(is);
			byte[] buff = readBuffer(is, length);
			return Boolean.parseBoolean(new String(buff));
		} catch (SocketTimeoutException ste)
		{
			logger.error("Server not responding: " + ste.getMessage());
		} catch (UnknownHostException e)
		{
			logger.error(e);
		} catch (IOException e)
		{
			logger.error(e);
		} catch (Exception e)
		{
			logger.error(e, e);
		} finally
		{
			try
			{
				if (os != null)
					os.close();
			} catch (IOException e)
			{
			}
			try
			{
				if (is != null)
					is.close();
			} catch (IOException e)
			{
			}
			try
			{
				if (s != null)
					s.close();
			} catch (IOException e)
			{
			}
		}
		return false;
	}

	public boolean tableExistsOnLiveAccess(String schema, String tablename)
	{
		Socket s = null;
		OutputStream os = null;
		InputStream is = null;
		try
		{
			s = getSocket(serverName, port);
			os = s.getOutputStream();
			is = s.getInputStream();
			String body = "tableexists" + "\r\n" + schema + "\t" + tablename + "\r\n";
			writeRequest(os, id, connectionName, body);
			int length = getLength(is);
			byte[] buff = readBuffer(is, length);
			return Boolean.parseBoolean(new String(buff));
		} catch (SocketTimeoutException ste)
		{
			logger.error("Server not responding: " + ste.getMessage());
		} catch (UnknownHostException e)
		{
			logger.error(e);
		} catch (IOException e)
		{
			logger.error(e);
		} catch (Exception e)
		{
			logger.error(e, e);
		} finally
		{
			try
			{
				if (os != null)
					os.close();
			} catch (IOException e)
			{
			}
			try
			{
				if (is != null)
					is.close();
			} catch (IOException e)
			{
			}
			try
			{
				if (s != null)
					s.close();
			} catch (IOException e)
			{
			}
		}
		return false;
	}

	public void CancelQueriesForLiveAccess() throws DataUnavailableException
	{
		if (!isRealTimeConnectionAlive(serverName, port, id, connectionName))
			throw new DataUnavailableException("Real time connect server not responding. id:" + id + ",server:" + serverName);
		Socket s = null;
		OutputStream os = null;
		InputStream is = null;
		try
		{
			s = getSocket(serverName, port);
			os = s.getOutputStream();
			is = s.getInputStream();
			String body = "killrequests\r\nkill:" + qstring;
			writeRequest(os, id, connectionName, body);
			int length = getLength(is);
			readBuffer(is, length);
		} catch (SocketTimeoutException ste)
		{
			logger.error("Server not responding: " + ste.getMessage());
		} catch (UnknownHostException e)
		{
			logger.error(e);
		} catch (IOException e)
		{
			logger.error(e);
		} catch (Exception e)
		{
			logger.error(e, e);
		} finally
		{
			try
			{
				if (os != null)
					os.close();
			} catch (IOException e)
			{
			}
			try
			{
				if (is != null)
					is.close();
			} catch (IOException e)
			{
			}
			try
			{
				if (s != null)
					s.close();
			} catch (IOException e)
			{
			}
		}
	}

	public void executeUpdate(String q) throws SQLException, DataUnavailableException
	{
		if (type == TYPE.Database)
		{
			stmt.executeUpdate(q);
		} else if (type == TYPE.Realtime)
		{
			if (!isRealTimeConnectionAlive(serverName, port, id, connectionName))
				throw new DataUnavailableException("Real time connect server not responding. id:" + id + ",server:" + serverName);
			Socket s = null;
			OutputStream os = null;
			try
			{
				s = getSocket(serverName, port);
				os = s.getOutputStream();
				String body = "executeupdatequery" + "\r\n" + q + "\r\n";
				writeRequest(os, id, connectionName, body);
			} catch (SocketTimeoutException ste)
			{
				logger.error("Server not responding: " + ste.getMessage());
			} catch (UnknownHostException e)
			{
				logger.error(e);
			} catch (IOException e)
			{
				logger.error(e);
			} catch (Exception e)
			{
				logger.error(e, e);
			} finally
			{
				try
				{
					if (os != null)
						os.close();
				} catch (IOException e)
				{
				}
				try
				{
					if (s != null)
						s.close();
				} catch (IOException e)
				{
				}
			}
		}
	}

	public void close() throws SQLException
	{
		if (type == TYPE.Database && stmt != null)
		{
			stmt.close();
		}
	}

	public DatabaseConnection getDatabaseConnection()
	{
		return databaseConnection;
	}

	public void setDatabaseConnection(DatabaseConnection databaseConnection)
	{
		this.databaseConnection = databaseConnection;
	}

	public List<Object[]> executeLiveAccessQuery(String query, boolean headers) throws DataUnavailableException
	{
		List<Object[]> result = new ArrayList<Object[]>();
		if (type == TYPE.Realtime)
		{
			if (!isRealTimeConnectionAlive(serverName, port, id, connectionName))
				throw new DataUnavailableException("Real time connect server not responding. id:" + id + ",server:" + serverName);
			Socket s = null;
			OutputStream os = null;
			InputStream is = null;
			try
			{
				s = getSocket(serverName, port);
				os = s.getOutputStream();
				is = s.getInputStream();
				String body = "resultsonlyquery" + (headers ? "withheaders" : "") + "\r\n" + query + "\r\n";
				writeRequest(os, id, connectionName, body);
				int length = getLength(is);
				byte[] buff = readBuffer(is, length);
				String res = new String(buff);
				int pos = 0;
				while (pos < res.length())
				{
					int end = res.indexOf('\r', pos);
					if (end < 0)
						end = res.length() - 1;
					String line = res.substring(pos, end);
					if (line.length() > 0)
					{
						String[] cols = line.split("\\|");
						result.add(cols);
					}
					pos = end + 2;
				}
			} catch (SocketTimeoutException ste)
			{
				logger.error("Server not responding: " + ste.getMessage());
			} catch (UnknownHostException e)
			{
				logger.error(e);
			} catch (IOException e)
			{
				logger.error(e);
			} catch (Exception e)
			{
				logger.error(e, e);
			} finally
			{
				try
				{
					if (os != null)
						os.close();
				} catch (IOException e)
				{
				}
				try
				{
					if (is != null)
						is.close();
				} catch (IOException e)
				{
				}
				try
				{
					if (s != null)
						s.close();
				} catch (IOException e)
				{
				}
			}
		}
		return result;
	}
}
