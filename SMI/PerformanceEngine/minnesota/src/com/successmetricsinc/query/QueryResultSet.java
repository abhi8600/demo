/**
 * $Id: QueryResultSet.java,v 1.323 2012/10/18 10:21:58 BIRST\cpatel Exp $
 *
 * Copyright (C) 2007-2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import gnu.jel.CompiledExpression;


import java.io.Externalizable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.OptionalDataException;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.JRRewindableDataSource;

import org.apache.log4j.Logger;

import com.birst.dataconductor.QueryMetaData;
import com.successmetricsinc.Console;
import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.UserBean;
import com.successmetricsinc.query.Aggregation.AggregationType;
import com.successmetricsinc.query.olap.BirstOlapField;
import com.successmetricsinc.query.olap.DimensionMemberSetNav;
import com.successmetricsinc.query.olap.OlapMemberExpression;
import com.successmetricsinc.transformation.DataElement;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.Operator.Type;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.transformation.integer.ToTime;
import com.successmetricsinc.transformation.object.Groups;
import com.successmetricsinc.transformation.object.Transform;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.DateUtil;
import com.successmetricsinc.util.JRBandOverflowException;
import com.successmetricsinc.util.TimeZoneUtil;
import com.successmetricsinc.util.Util;

/**
 * Class to contain the results of a given query (to be used with ResultSetCache for caching)
 * 
 * @author Brad Peters
 */
public class QueryResultSet implements JRRewindableDataSource, Externalizable, IJasperDataSourceProvider, Serializable
{
	private static Logger logger = Logger.getLogger(QueryResultSet.class);
	private static final long serialVersionUID = 2L; // change for new row persistence in file cache
	public static final String EXPR_TABLE_NAME = "EXPR";
	private static AtomicLong IDCount = new AtomicLong(0);
	protected String[] tableNames;
	protected String[] columnNames;
	protected String[] columnFormats;
	protected String[] displayNames;
	protected String[] physicalDisplayNames;
	protected String[] warehouseColumnsDataTypes;
	protected int[] maskable;
	protected String[] aggregationRules;
	protected List<DimensionRule>[] dimensionRules;
	protected QueryFilter[] measureFilters;
	protected int[] columnDataTypes;
	protected int[] columnTypes;
	protected AggregationType[] aggregations;
	protected Object[][] rows;
	protected int curRow;
	protected Query q;
	protected int numMeasures;
	protected transient JasperDataSourceProvider jdsp;
	private boolean ascending;
	private boolean nulltop;
	private boolean valid;
	public enum RetrievedFrom { memory, exactfile, inexactfile, database }; 
	private transient RetrievedFrom whereFrom = RetrievedFrom.database;
	private List<DisplayFilter> boundFilters;
	// Column Mapping Structures
	private boolean[] mapped;
	private Map<Object, Object>[] columnMaps;
	private String baseFilename;
	private int rewindCount = 0;
	private long expires = Long.MAX_VALUE; // default value is never expires, 0 means expires immediately

	private static boolean processSerially;
	private transient long queryID;
	private transient boolean executePhysicalQueryOnly = false;
	private transient String filename = null;
	private transient int physicalQueryNumRows = 0;
	private transient String userDisplayTimezone = null;
	// Mask info for dimension expressions
	private transient int[] dimensionMask;
	private transient Map<String, Integer> dimensionMaskMap;
	private transient List<String> dimensionMaskDimensionNames;
	private transient List<String> dimensionMaskColumnNames;
	private transient List<Integer> parentIndices;
	
	/*
	 * determine if the query result set is past it's cache expiration time
	 */
	public boolean hasExpired()
	{
		if (expires < System.currentTimeMillis())
			return true;
		return false;
	}
	
	/*
	 * set the cache expiration time for this query result set
	 * - 0 means expires immediately
	 * - MAX_VALUE means never expires
	 */
	public void setExpiration()
	{
		expires = Long.MAX_VALUE;
		List<Navigation> navList = q.getNavigationList();
		if (navList == null || navList.size() == 0)
		{
			return;
		}
		for (Navigation nav : navList)
		{
			long exp = nav.getExpires();
			expires = Math.min(exp, expires);
		}
		if (logger.isDebugEnabled())
		{
			try
			{
				if (expires == Long.MAX_VALUE)
				{
					logger.debug("cached resultset never expires");
				}
				else
				{
					Calendar cal = GregorianCalendar.getInstance();
					cal.setTimeInMillis(expires);
					SimpleDateFormat fmt =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					logger.debug("cached resultset expires at " + fmt.format(cal.getTime()));
				}
			}
			catch (Exception ex)
			{}
		}
	}
	
	/**
	 * Caller to ensure that this method is called after setExpiration is called in order
	 * to get expected value of expires. 
	 * @return current value of expires
	 */
	public long getCurrentExpiresValue()
	{
		return expires;
	}
	/*
	 * determine the expiration boundary
	 */
	public static long getExpirationFromBoundary(String boundary)
	{
		Calendar cal = GregorianCalendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, cal.getActualMaximum(Calendar.HOUR_OF_DAY));
		cal.set(Calendar.MINUTE, cal.getActualMaximum(Calendar.MINUTE));  // midnight (1 minute before for clock skew issues)
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
		if (DimensionColumn.CACHE_DAY_BOUNDARY.equals(boundary))
		{
				// nothing to do, already set to midnight tonight
		}
		else if (DimensionColumn.CACHE_WEEK_BOUNDARY.equals(boundary))
		{
			cal.set(Calendar.DAY_OF_WEEK, cal.getActualMaximum(Calendar.DAY_OF_WEEK));
		}
		else if (DimensionColumn.CACHE_MONTH_BOUNDARY.equals(boundary))
		{
			cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		}
		else if (DimensionColumn.CACHE_QUARTER_BOUNDARY.equals(boundary)) // XXX for now quarter is the same as month to make life easy
		{
			cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		}
		else if (DimensionColumn.CACHE_YEAR_BOUNDARY.equals(boundary))
		{
			cal.set(Calendar.DAY_OF_YEAR, cal.getActualMaximum(Calendar.DAY_OF_YEAR));
		}
		else
		{
			return Long.MAX_VALUE; // NONE and null
		}
		long expiration = cal.getTimeInMillis();
		return expiration;
	}

	public QueryResultSet clone(boolean cloneRows)
	{
		QueryResultSet qrs = new QueryResultSet();
		qrs.tableNames = tableNames.clone();
		qrs.columnNames = columnNames.clone();
		qrs.columnFormats = columnFormats.clone();
		qrs.columnDataTypes = columnDataTypes.clone();
		qrs.displayNames = displayNames.clone();
		qrs.maskable = maskable.clone();
		qrs.aggregationRules = aggregationRules.clone();
		if (dimensionRules != null)
			qrs.dimensionRules = dimensionRules.clone();
		qrs.measureFilters = measureFilters.clone();
		qrs.columnTypes = columnTypes.clone();
		if (warehouseColumnsDataTypes != null)
			qrs.warehouseColumnsDataTypes = warehouseColumnsDataTypes.clone();
		qrs.aggregations = aggregations.clone();
		if (cloneRows)
			qrs.rows = rows.clone();
		else
			qrs.rows = rows;
		qrs.q = q;
		qrs.numMeasures = numMeasures;
		qrs.curRow = curRow;
		qrs.valid = valid;
		qrs.whereFrom = whereFrom;
		qrs.mapped = mapped;
		qrs.columnMaps = columnMaps;
		qrs.baseFilename = baseFilename;
		qrs.expires = expires;
		return (qrs);
	}

	/**
	 * Return next row
	 * 
	 * @see net.sf.jasperreports.engine.JRDataSource#next()
	 */
	public boolean next()
	{
		curRow++;
		boolean hasnext = curRow < rows.length;
		// Automatically reset the cursor if the end is reached
		if (!hasnext)
			curRow = -1;
		return (hasnext);
	}
	
	public int getPhysicalQueryNumRows()
	{
		return physicalQueryNumRows;
	}

	/**
	 * Set row cursor
	 * 
	 * @param rnum
	 *            Row number
	 */
	public void resetCursor(int rnum)
	{
		curRow = rnum;
	}

	/**
	 * Get the value for a given field
	 * 
	 * @see net.sf.jasperreports.engine.JRDataSource#getFieldValue(net.sf.jasperreports.engine.JRField)
	 */
	public Object getFieldValue(JRField field)
	{
		Object[] row = (Object[]) rows[curRow];
		String fname = Util.replaceWithPhysicalString(field.getName());
		int index = fname.indexOf(BirstRatio.RATIO);
		if (index > 0) {
			String prefix = fname.substring(0, index);
			String numerator = prefix + BirstRatio.NUMERATOR;
			String denominator = prefix + BirstRatio.DENOMINATOR;
			Number n = null;
			Number d = null;
			
			for (int i = 0; i < displayNames.length; i++) {
				String cname = getPhysicalDisplayName(i);
				if (numerator.equals(cname))
					n = (Number)row[i];
				else if (denominator.equals(cname))
					d = (Number)row[i];
			}
			return new BirstRatio(n, d);
		}
		index = fname.indexOf(BirstOlapField.OLAP);
		if (index > 0) {
			return QueryResultSet.getBirstOlapFieldFromRow(fname, row, displayNames, this);
		}
		
		Object value = null;
		for (int i = 0; i < displayNames.length && value == null; i++)
		{
			String cname = null;
			if (displayNames[i] != null && displayNames[i].trim().length() > 0)
				cname = getPhysicalDisplayName(i);
			// Ignore dollar signs
			if (fname.equals(cname))
			{
				if (row[i] instanceof Date)
				{
					Calendar cal = Calendar.getInstance();
				    cal.setTime((Date) row[i]);
					row[i] = cal;
				}
				value = row[i];
				index = fname.indexOf(BirstInterval.INTERVAL_SUFFIX);
				if (index > 0 && value != null && value instanceof Number) {
					String format = this.columnFormats[i];
					return new BirstInterval((Number)value, format);
				}
			}
		}
		if (value != null) {
			for (int j = 0; j < 10; j++) {
				String pivotSortName = fname + SortablePivotField.PIVOT_SORT + j;
				for (int i = 0; i < displayNames.length; i++)
				{
					String cname = null;
					if (displayNames[i] != null && displayNames[i].trim().length() > 0)
						cname = getPhysicalDisplayName(i);
					// Ignore dollar signs
					if (pivotSortName.equals(cname)) {
						if (! (value instanceof SortablePivotField)) {
							value = new SortablePivotField(value);
						}
						// find the sort order
						boolean ascending = true;
						if (this.jdsp != null && this.jdsp.getQuery() != null) {
							boolean found = false;
							Query q = this.jdsp.getQuery();
							List<OrderBy> orders = q.getOrderByClauses();
							if (orders != null) {
								for (OrderBy ob : orders) {
									// i is the index to the sort field
									if (((this.tableNames[i] == null && ob.getDimName() == null) 
											|| (this.tableNames != null && this.tableNames[i].equals(ob.getDimName()))) &&
											(this.columnNames[i] != null && this.columnNames[i].equals(ob.getColName()))) {
										found = true;
										ascending = ob.getDirection();
										break;
									}
								}
							}
							List<DisplayOrder> dOrders = q.getDisplayOrders();
							if (!found && dOrders != null) {
								for (DisplayOrder dob : dOrders) {
									if (((this.tableNames[i] == null && dob.tableName == null) 
											|| (this.tableNames[i] != null && this.tableNames[i].equals(dob.tableName))) &&
											(this.columnNames[i] != null && this.columnNames[i].equals(dob.columnName))) {
										ascending = dob.ascending;
										break;
									}
								}
							}
						}
						if (row[i] instanceof Serializable) {
							((SortablePivotField)value).addSortValue((Serializable)row[i], ascending);
						}
						break;
					}
				}
			}
			return value;
		}
		if (fname.equals(JasperDataSourceProvider.PROMPT_VALUES)) {
			// return prompt values - get from jdsp
			return getPromptValueString(false, false);
		}
		if (fname.equals(JasperDataSourceProvider.PROMPT_VALUES_ONE_LINE)) {
			// return prompt values - get from jdsp - all on one line
			return getPromptValueString(true, false);
		}
		if (fname.equals(JasperDataSourceProvider.PROMPT_VALUES_SKIP_ALL)) {
			// return prompt values - get from jdsp
			return getPromptValueString(false, true);
		}
		if (fname.equals(JasperDataSourceProvider.PROMPT_VALUES_ONE_LINE_SKIP_ALL)) {
			// return prompt values - get from jdsp - all on one line
			return getPromptValueString(true, true);
		}		
		return null;
	}
	
	protected String getPhysicalDisplayName(int index) {
		if (index < 0 || index > this.displayNames.length)
			return "";
		if (physicalDisplayNames == null) {
			physicalDisplayNames = new String[this.displayNames.length];
		}
		if (physicalDisplayNames[index] == null && this.displayNames[index] != null) {
			physicalDisplayNames[index] = Util.replaceWithPhysicalString(displayNames[index]);
		}
		
		return physicalDisplayNames[index];
	}
	
	private Map<String, List<String>> _olapValueMap;
	private Map<String, List<String>> getOlapValueMap() {
		if (_olapValueMap == null) {
			_olapValueMap = new HashMap<String, List<String>>();
		}
		return _olapValueMap;
	}
	private int getRowIndex(String fname, String value) {
		List<String> stringList = getOlapValueMap().get(fname);
		if (stringList == null) {
			stringList = new ArrayList<String>();
			getOlapValueMap().put(fname, stringList);
		}
		int index = stringList.indexOf(value);
		if (index < 0) {
			index = stringList.size();
			stringList.add(value);
		}
		return index;
	}
	
	public static Object getBirstOlapFieldFromRow(String fname, Object[] row, String[] displayNames, QueryResultSet qrs) {
		String pName = null;
		int ordinal = 0;
		String uniqueName = null;
		String pUniqueName = null;
		String parentUniqueName = fname + BirstOlapField.PARENT;
		String parentName = fname + BirstOlapField.PARENTNAME;
		String ord = fname + BirstOlapField.ORDINAL;
		String uname = fname + BirstOlapField.UNIQUENAME;
		Object value = null;
		for (int i = 0; i < displayNames.length; i++) {
			String cname = qrs.getPhysicalDisplayName(i);
			if (parentUniqueName.equals(cname))
				pUniqueName = (String)row[i];
			else if (ord.equals(cname)) {
				Object o = row[i];
				try {
					ordinal = o == null ? 0 : Integer.valueOf(o.toString());
				}
				catch (Exception e) {
					ordinal = 0;
				}
			}
			else if (uname.equals(cname))
				uniqueName = (String)row[i];
			else if (parentName.equals(cname))
				pName = (String)row[i];
			else if (fname.equals(cname))
				value = row[i];
		}
		if (value instanceof Serializable) {
			return new BirstOlapField((Serializable)value, pUniqueName, ordinal, uniqueName, pName, qrs.getRowIndex(fname, value.toString()));
		}
		
		return value;
		
	}
	
	public int getIndexForDisplayName(String fieldName) {
		for (int i = 0; i < displayNames.length; i++) {
			String cname = null;
			if (displayNames[i] != null && displayNames[i].trim().length() > 0)
				cname = getPhysicalDisplayName(i);
			// Ignore dollar signs
			if (fieldName.equals(cname))
				return i;
		}
		
		return -1;
	}
	
	private String getPromptValueString(boolean oneLine, boolean skipAll) {
		List<Filter> params = jdsp.getParams();
		if (params == null)
			return "";
		
		StringBuilder builder = new StringBuilder();
		for (Filter filter : params) {
			if (skipAll)
			{
				boolean skip = false;
				List<String> filterValues = filter.getValues();
				for (String v: filterValues) {
					if (v.equals("NO_FILTER"))
					{
						skip = true;
						break;
					}
				}
				if (skip)
					continue;
			}
			String name = filter.getColumnName();
			if (builder.length() > 0)
			{
				if (oneLine)
					builder.append(" | ");
				else
					builder.append("\n");
			}
							
			builder.append(name);
			builder.append(": ");
			builder.append(filter.getOperator());
			
			List<String> filterValues = filter.getValues();
			boolean prependComma = false;
			for (String v: filterValues) {
				if (prependComma)
					builder.append(',');
				prependComma = true;
				builder.append(' ');
				if (v.equals("NO_FILTER"))
					builder.append("ALL");
				else
					builder.append(v);
			}
		}
		return builder.toString();
	}

	/**
	 * Constructor used to create special copies or null data sets
	 */
	public QueryResultSet()
	{
		rows = new Object[0][0];
	}

	/*
	 * Expression-only result set
	 */
	@SuppressWarnings("unchecked")
	public QueryResultSet(Query q)
	{
		this.q = q;
		rows = new Object[1][0];
		columnNames = new String[0];
		tableNames = new String[0];
		columnFormats = new String[0];
		columnDataTypes = new int[0];
		columnTypes = new int[0];
		displayNames = new String[0];
		warehouseColumnsDataTypes = new String[0];
		maskable = new int[0];
		aggregationRules = new String[0];
		dimensionRules = getDimensionRuleList(0);
		aggregations = new AggregationType[0];
		measureFilters = new QueryFilter[0];
		mapped = new boolean[0];
		columnMaps = new Map[0];
		valid = true;
		numMeasures = 0;
	}
	
	/**
	 * Return a processed version of this query result set. This allows for sorting, column reordering and adding
	 * expressions
	 * 
	 * @param q
	 *            Query that matched this query result set - (and to which this result set may need to be reordered).
	 * @param reorder
	 *            Whether reordering is needed
	 * @param expressionList
	 *            Expression list
	 * @param displayOrderList
	 *            Display order list
	 * @param addedNames
	 *            If addedNames is not equal to null, any columns in that list will be pruned (i.e. these are columns
	 *            that had to be added to the cache for calculation purposes, but aren't in the final result)
	 * @return
	 * @throws SyntaxErrorException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @throws ScriptException 
	 * @throws Exception
	 */
	public QueryResultSet returnProcessedResults(ResultSetCache rsc, Query q, boolean reorder, List<DisplayExpression> expressionList,
			List<DisplayOrder> displayOrderList, List<DisplayFilter> displayFilters, List<String> addedNames, RowMapCache rmap) throws BaseException,
			IOException, ClassNotFoundException, CloneNotSupportedException
	{
		/*
		 * Only copy the rows if not doing calculations or reordering, because they will be recopied anyway as part of
		 * that process.
		 */
		boolean cloneRows = reorder || (expressionList != null && !expressionList.isEmpty()) || (q.getAggregations() != null && !q.getAggregations().isEmpty());
		QueryResultSet qrs = this.clone(cloneRows);
		qrs.q = q;
		// Get the resulting column order
		List<DisplayExpression> aggregateExpressionList = null;
		List<DisplayExpression> nonAggregateExpressionList = null;
		List<String[]> colList = null;
		if (cloneRows)
		{
			colList = determineColumns(q, expressionList);
			if (expressionList != null && !expressionList.isEmpty())
			{
				nonAggregateExpressionList = new ArrayList<DisplayExpression>(expressionList);
				aggregateExpressionList = new ArrayList<DisplayExpression>();
				for (DisplayExpression de : expressionList)
				{
					//Display order must be processed before RSum expressions
		  	  		if (de.containsAggregateExpression && de.isRSum())
		  	  		{
		  	  			aggregateExpressionList.add(de);
		  	  		}
				}
				nonAggregateExpressionList.removeAll(aggregateExpressionList);
				//try to reset cloneRows flag i.e. nonAggregateExperssion list may be empty now
				cloneRows = reorder || (nonAggregateExpressionList != null && !nonAggregateExpressionList.isEmpty()) || (q.getAggregations() != null && !q.getAggregations().isEmpty());
			}
			if (cloneRows)
				qrs.processResultSet(rsc, colList, expressionList, nonAggregateExpressionList, displayFilters, addedNames, rmap, displayOrderList, q);		
		}
		if (displayOrderList != null && !displayOrderList.isEmpty())
			qrs.processDisplayOrderings(displayOrderList);
		if (aggregateExpressionList != null && !aggregateExpressionList.isEmpty())
		{
			qrs.processResultSet(rsc, colList, expressionList, aggregateExpressionList, displayFilters, addedNames, rmap, displayOrderList, q);
		}
		if(q.dtopn > 0)
		{
			qrs.applyDisplayTop(q.dtopn);
		}
		return (qrs);
	}
	
	@SuppressWarnings("unchecked")
	private List<DimensionRule>[] getDimensionRuleList(int count)
	{
		List<DimensionRule>[] list = new List[count];
		return (list);
	}
	private List<DatabaseConnectionStatement> statementList = null;

	/**
	 * Create a query result set using a given list of database statements (obtained previously by calling getStatements
	 * from the Query object). This allows the query to be cancelled.
	 * 
	 * @param q
	 * @param maxRecords
	 * @param slist
	 * @throws SQLException 
	 * @throws SQLException
	 * @throws ScriptException 
	 */
	public QueryResultSet(Query q, int maxRecords, int maxQueryTime, Session session) throws BaseException, SQLException
	{
		setupQueryResultSet(q, maxRecords, maxQueryTime, session);
	}
	
	public QueryResultSet(Query q, int maxRecords, int maxQueryTime, Session session, boolean executePhysicalQueryOnly, 
			String filename, String userDisplayTimezone) throws BaseException, SQLException
	{
		this.executePhysicalQueryOnly = executePhysicalQueryOnly;
		this.filename = filename;
		this.userDisplayTimezone = userDisplayTimezone;
		setupQueryResultSet(q, maxRecords, maxQueryTime, session);
	}

	@SuppressWarnings("unchecked")
	/*
	 * *
	 */
	private void setupQueryResultSet(Query q, int maxRecords, int maxQueryTime, Session session) throws SQLException, BaseException
	{
		queryID = QueryResultSet.IDCount.getAndIncrement();
		long startTime = System.currentTimeMillis();
		if (logger.isInfoEnabled())
		{
			String logicalQueryString = q.getLogicalQueryString(false, null, false);
			if (logicalQueryString != null)
				logger.info("Missed Cache:"+queryID+":"+QueryString.filterServerPassword(logicalQueryString));
		}
		statementList = q.getStatements();
		boolean mapNullToZero = q.getRepository().getServerParameters().isMapNullToZero();
		this.q = q;
		valid = false;
		Map<DatabaseConnection, List<TempTable>> tempTableMap = q.getTempTableMap();
		DatabaseConnectionResultSet rs = null;
		if (session != null)
		{
			for (DatabaseConnectionStatement stmt : statementList)
			{
				session.addCurrentStatement(stmt);
			}
		}
		List<DatabaseConnection> connectionList = q.getConnections();
		boolean isDbTypeIB = false;
		for (DatabaseConnection conn : connectionList)
		{
			if (DatabaseConnection.isDBTypeMySQL(conn.DBType))
			{
				isDbTypeIB = true;
				break;
			}			
		}
		
		try
		{
			// deal with DBMS's that don't support these two calls
			try
			{
				for (DatabaseConnectionStatement stmt : statementList)
				{
					int curMaxRows = stmt.getMaxRows();
					// use some limiters to make sure we don't trash the SQL Server or the web application server
					if ((maxRecords > 0) && (maxRecords != Integer.MAX_VALUE) && ((maxRecords +2) != curMaxRows))
					{
						stmt.setMaxRows(maxRecords + 2);
					}
					if (maxQueryTime > 0 && maxQueryTime != Integer.MAX_VALUE)
					{
						stmt.setQueryTimeout(maxQueryTime);
					}
				}
			} catch (Exception e)
			{
				if (logger.isTraceEnabled())
					logger.trace(e.getMessage(), e);
			}
			if (tempTableMap != null && tempTableMap.size() > 0)
			{
				for (int i = 0; i < connectionList.size(); i++)
				{
					DatabaseConnection dbc = connectionList.get(i);
					if (!tempTableMap.containsKey(dbc))
						continue;
					List<TempTable> ttlist = new ArrayList<TempTable>(tempTableMap.get(dbc));
					for (TempTable tt : ttlist)
					{
						synchronized (tt)
						{
							tt.lastAccessed = System.currentTimeMillis();
							int count = tt.referenceCount.incrementAndGet();
							if (!tt.liveAccessTmpTable)
							{								
								if (tt.created.getAndSet(true))
								{
									logger.debug("Re-using temp table: " + tt.tableName + ":" + count);
									continue;
								}
								logger.info("Temp table:");
								if (tt.create != null)
								{
									logger.info(tt.create);
									statementList.get(i).executeUpdate(tt.create);
								}
								logger.info(tt.query);
								long start = System.currentTimeMillis();
								if (tt.create == null || q.topn != 0)
									statementList.get(i).executeUpdate(tt.query);
								logger.info("Temp table created (" + (System.currentTimeMillis() - start) + "ms): " + tt.create);
							}
						}
					}
				}
			}
			if (!q.containsMultipleQueries || (q.isSimulateOuterJoinUsingPersistedDQTs()))
			{
				if(!q.isExpandDerivedQueryTables())
				{
					q.persistDerivedQueryTables(statementList.get(0).getDatabaseConnection());
				}
				long qstartTime = System.currentTimeMillis();
				if(q.containsFederatedJoin())
				{
					rs = statementList.get(0).executeQuery(q.getFederatedJoinQueryHelper(), q, maxRecords, maxQueryTime, session);
				}
				else
				{
					rs = statementList.get(0).executeQuery(q.getQuery(), q);
				}
				// Setup to be able to parse logs
				String pq = q.getPrettyQuery().replaceAll("\\n", "\n]");
				logger.info("Physical Query:" + queryID + ":" + (System.currentTimeMillis() - qstartTime) + "ms: " + pq);
				if(!q.isExpandDerivedQueryTables())
				{
					q.dropDerivedQueryTables(statementList.get(0).getDatabaseConnection());
				}
			} else
				rs = joinResultSets(session);
			if (tempTableMap != null && tempTableMap.size() > 0)
			{
				for (int i = 0; i < connectionList.size(); i++)
				{
					DatabaseConnection dbc = connectionList.get(i);
					if (!tempTableMap.containsKey(dbc))
						continue;
					List<TempTable> ttlist = new ArrayList<TempTable>(tempTableMap.get(dbc));
					List<TempTable> toRemove = new ArrayList<TempTable>();
					for (TempTable tt : ttlist)
					{
						int count = tt.referenceCount.decrementAndGet();
						tt.lastAccessed = System.currentTimeMillis();
						if (tt.liveAccessTmpTable)
						{
							try
							{
								int index = tt.tableName.indexOf('.');
								String s = tt.tableName;
								if (index >= 0)
									s = s.substring(index + 1);
								Database.dropTable(q.getRepository(), dbc, s, true);
								toRemove.add(tt);
								List<TempTable> ttlistOrig = tempTableMap.get(dbc);
								if (ttlistOrig!=null)
								{
									synchronized (ttlistOrig)
									{
										ttlistOrig.removeAll(toRemove);
									}
								}
							} catch (Exception e)
							{
								logger.error(e, e);
							}
						}
						logger.debug("Releasing temp table: " + tt.tableName + ":" + count);
					}
				}
			}
		} catch (Exception e)
		{
			if (e instanceof com.microsoft.sqlserver.jdbc.SQLServerException)
			{
				String msg = e.getMessage();
				if (msg != null && msg.indexOf("The query was canceled") >= 0)
				{
					logger.info("Query canceled by user - " + q.getPrettyQuery());
					throw new QueryCancelledException("SQL Query canceled by user");
				}
				if (msg != null && msg.indexOf("The query has timed out") >= 0)
				{
					logger.info("Query timed out (" + maxQueryTime + " seconds) - " + q.getPrettyQuery());
					throw new QueryTimeoutException("SQL Query timed out (" + maxQueryTime + " seconds)");
				}
			} else if (e instanceof  java.sql.SQLException)
			{
				String msg = e.getMessage();
				if (msg != null && msg.indexOf("Stopped by user") >= 0)
				{
					logger.info("Query timed out (" + maxQueryTime + " seconds) - " + q.getPrettyQuery());
					throw new QueryTimeoutException("SQL Query timed out (" + maxQueryTime + " seconds)");
				}
				else if (msg != null && msg.indexOf("Birst Local load being processed, cannot execute query") >= 0)
				{
					logger.info("Birst Local load being processed, cannot execute query - " + q.getPrettyQuery());
					throw new DataUnavailableException("Birst Local load being processed, cannot execute query");
				}
				else if (msg != null && msg.indexOf("Query Cancelled") >= 0)
				{
					logger.info("Query canceled by user - " + q.getPrettyQuery());
					throw new QueryCancelledException("MDX Query canceled by user");
				}
			} else if (e instanceof ResultSetTooBigException)
			{
				logger.debug(e, e);
				throw (ResultSetTooBigException) e;
			} else if (e instanceof GoogleAnalyticsException)
			{
				logger.debug(e, e);
				throw (GoogleAnalyticsException) e;
			}
			if (!Util.isValidStatement(q.getQuery()))
			{
				logger.info("Missing variable substitution(s) in query: " + q.getQuery());
				logger.info("Logical Query: " + q.getLogicalQueryString(false, null, false));
				throw new SyntaxErrorException("Missing variable substitution(s): " + q.getLogicalQueryString(false, null, false));
			}
			logger.info("Query Exception: " + e.toString() + " " + q.getPrettyQuery());
			throw new DataUnavailableException(e.getMessage(), e);
		} finally
		{
			if (session != null)
			{
				for (DatabaseConnectionStatement stmt : statementList)
				{
					session.removeStatement(stmt);
				}
			}
		}
		FileOutputStream fos = null;
		ZipOutputStream zos = null;
		OutputStreamWriter writer = null;
		int record = 0;
		List<Object[]> rowList = new ArrayList<Object[]>();
		try
		{
			valid = true;
			int count = q.getReturnProjectionListSize();
			if(q.isWrappedQueryWithOneColumn())
			{
				count = 1;
			}
			columnNames = new String[count];
			tableNames = new String[count];
			columnFormats = new String[count];
			columnDataTypes = new int[count];
			columnTypes = new int[count];
			displayNames = new String[count];
			warehouseColumnsDataTypes = new String[count];
			maskable = new int[count];
			aggregationRules = new String[count];
			dimensionRules = getDimensionRuleList(count);
			aggregations = new AggregationType[count];
			measureFilters = new QueryFilter[count];
			mapped = new boolean[count];
			columnMaps = new Map[count];
			numMeasures = 0;
			List<Aggregation> queryAggs = q.getAggregations();
			int numCols = q.getSuppliedQueryColumns().size();
			for (int i = 1; i <= count; i++)
			{
				aggregations[i - 1] = AggregationType.None;
				QueryColumn qc = q.getReturnProjectionListColumn(i - 1);
				columnTypes[i - 1] = qc.type;
				displayNames[i - 1] = i <= numCols ? qc.name : null;
				int ctype = Types.VARCHAR;
				//if (i <= rs.getColumnCount())
					//ctype = rs.getColumnType(i);
				if (qc.type == QueryColumn.MEASURE)
				{
					MeasureColumn mc = (MeasureColumn) qc.o;
					columnFormats[i - 1] = mc.Format;
					tableNames[i - 1] = mc.MeasureTable.TableName;
					columnNames[i - 1] = mc.ColumnName;
					aggregationRules[i - 1] = mc.AggregationRule;
					warehouseColumnsDataTypes[i - 1] = mc.LogicalDataType;
					if (mc.DimensionRules != null)
					{
						dimensionRules[i - 1] = mc.DimensionRules;
					}
					int aggIndex = 0;
					for (; aggIndex < queryAggs.size(); aggIndex++)
					{
						Aggregation agg = (Aggregation) queryAggs.get(aggIndex);
						if (agg.getColumnName().equals(columnNames[i - 1]))
							break;
					}
					if (mc.Display != null && mc.Display != AggregationType.None)
					{
						aggregations[i - 1] = mc.Display;
					} else if (aggIndex < queryAggs.size())
					{
						Aggregation agg = (Aggregation) queryAggs.get(aggIndex);
						if (agg.getType() == AggregationType.Rank)
						{
							aggregations[i - 1] = AggregationType.Rank;
						} else if (agg.getType() == AggregationType.DenseRank)
						{
							aggregations[i - 1] = AggregationType.DenseRank;
						} else if (agg.getType() == AggregationType.Percentile)
						{
							aggregations[i - 1] = AggregationType.Percentile;
						}
					}
					if (qc.filter != null)
						measureFilters[i - 1] = qc.filter;
					ctype = getTypeForDataTypeString(mc.LogicalDataType);
				} else if (qc.type == QueryColumn.DIMENSION)
				{
					DimensionColumn dc = (DimensionColumn) qc.o;
					columnFormats[i - 1] = dc.Format;
					tableNames[i - 1] = dc.getDimensionName();
					columnNames[i - 1] = dc.ColumnName;
					warehouseColumnsDataTypes[i - 1] = dc.DataType;
					maskable[i - 1] = dc.Maskable;
					ctype = getTypeForDataTypeString(dc.DataType);
				} else if (qc.type == QueryColumn.DIMENSION_MEMBER_SET)
				{
					DimensionMemberSetNav dms = (DimensionMemberSetNav) qc.o;
					tableNames[i - 1] = dms.dimensionName;
					columnNames[i - 1] = dms.getMemberExpressionsString();
					if (dms.dataType != null && dms.colIndex == qc.columnIndex)
					{
						ctype = getTypeForDataTypeString(dms.dataType);
						warehouseColumnsDataTypes[i - 1] = dms.dataType;
						if (dms.format != null)
						{
							columnFormats[i - 1] = dms.format;
						}
					}
				} else if (qc.type == QueryColumn.OLAP_MEMBER_EXPRESSION)
				{
					OlapMemberExpression exp = (OlapMemberExpression) qc.o;
					tableNames[i - 1] = "";
					columnNames[i - 1] = exp.expression;					
				} else if (qc.type == QueryColumn.CONSTANT_OR_FORMULA)
				{
					columnFormats[i - 1] = "";
					tableNames[i - 1] = "";
					columnNames[i - 1] = qc.name;
					if (i <= rs.getColumnCount())
						ctype = rs.getColumnType(i);
				}
				if (qc.type == 1)
					numMeasures++;
				columnDataTypes[i - 1] = getGenericSqlTypeForDataType(ctype, qc);				
			}
			
			SimpleDateFormat outputDateFormat = new SimpleDateFormat("yyyy-MM-dd HHH:mm:ss.SSS");
			if (this.executePhysicalQueryOnly && filename != null)
			{
				//create zip stream writer and write headers
				File f = new File(filename);
				logger.debug("writing report to: " + filename);
				File exportDir = f.getParentFile();
				if (!exportDir.exists())
					exportDir.mkdir();
				fos = new FileOutputStream(filename, false);
				zos = new ZipOutputStream(fos);
				ZipEntry ze = new ZipEntry(filename.substring(filename.lastIndexOf(File.separator) + 1, filename.lastIndexOf(".")) + ".txt");
				zos.putNextEntry(ze);
				writer = new OutputStreamWriter(zos);
				for (int i = 0; i < displayNames.length; i++)
				{
					if (displayNames[i] != null)
					{
						String n = null;
						// Remove possible leading field indicator for Adhoc (old style)
						if (displayNames[i].length() > 3 && displayNames[i].charAt(0) == 'F' && displayNames[i].charAt(2) == '_')
							n = displayNames[i].substring(3);
						else if (displayNames[i].length() > 4 && displayNames[i].charAt(0) == 'F' && displayNames[i].charAt(3) == '_')
							n = displayNames[i].substring(4);
						else
							n = displayNames[i];
						writer.write(n + "\t");
					}
				}
				writer.write("\r\n");
				TimeZone userTZ = null;
				if (userDisplayTimezone != null && !userDisplayTimezone.isEmpty())
				{
					userTZ = TimeZoneUtil.getTimeZoneForWindowsTZID(userDisplayTimezone);					
				}
				if (userTZ == null)
				{
					userTZ = q.getRepository().getServerParameters().getDisplayTimeZone();
				}
				outputDateFormat.setTimeZone(userTZ);
			}
			
			Calendar calProcTZ = Calendar.getInstance(q.getRepository().getServerParameters().getProcessingTimeZone());
			Calendar calData = Calendar.getInstance(q.getRepository().getServerParameters().getProcessingTimeZone());

			while (rs.next() && (++record <= maxRecords))
			{
				Object[] row = new Object[columnDataTypes.length];
				for (int i = 0; i < columnDataTypes.length; i++)
				{
					row[i] = getObjectForDataType(columnDataTypes[i], i, rs, mapNullToZero, calProcTZ, calData, isDbTypeIB);					
				}
				if (!this.executePhysicalQueryOnly)
				{
					rowList.add(row);
				}
				else
				{
					boolean first = true;
					for (int i = 0; i < row.length; i++)
					{
						if (displayNames[i] != null)
						{
							if (!first)
								writer.write('\t');
							if (row[i] != null)
							{
								if (GregorianCalendar.class.isInstance(row[i]))
								{
									String s = outputDateFormat.format(((Calendar) row[i]).getTime());
									writer.write(s);
								} else
									writer.write(Util.quote(row[i].toString(), '\t'));
							}
							first = false;
						}
					}
					if (!first)
						writer.write("\r\n");
				}
			}
			physicalQueryNumRows = record;
		}
		catch (Exception e)
		{
			logger.info("Error processing query: " + e.toString());
			this.valid = false;
		}
		finally
		{
			for (DatabaseConnectionStatement stmt : statementList)
			{
				if (tempTableMap != null && tempTableMap.size() > 0 
						&& ((session == null) || (session != null && session.getTempTableMap() == null)))
				{
					for (int i = 0; i < connectionList.size(); i++)
					{
						DatabaseConnection dbc = connectionList.get(i);
						if (!tempTableMap.containsKey(dbc))
							continue;
						for (TempTable tt : tempTableMap.get(dbc))
						{
							String ts = "DROP TABLE " + tt.tableName;
							logger.info(ts);
							statementList.get(i).executeUpdate(ts);
						}
					}
				}
				if (rs != null)
				{
					rs.close();
				}
				if (stmt != null)
				{
					stmt.close();
				}
			}
			try
			{
				if (writer != null)
					writer.close();
				if (zos != null)
					zos.close();
				if (fos != null)
					fos.close();
			}
			catch (IOException e)
			{
				logger.debug(e, e);
			}
		}
		long qtime = System.currentTimeMillis() - startTime;
		logger.info("Logical Query:" + queryID + ":" + qtime + ":" + record + ":Rows-" + record + ",Time-" + qtime + "ms");
		if (record > maxRecords)
		{
			// clear out the rows
			rowList.clear();
			rowList = null;
			logger.info("Too much data returned in the query (max = " + maxRecords + ") [" + q.getLogicalQueryString(false, null, false) + "]: " + q.getQuery());
			throw new ResultSetTooBigException("Too much data returned in the query (max = " + maxRecords + "). Please add filters to reduce the size of the result set.");
		}
		rows = (Object[][]) new Object[rowList.size()][];
		rowList.toArray(rows);
		curRow = -1;
		/*
		 * Map any columns if necessary
		 */
		if (q.hasMappedColumns())
			mapDimensionColumns();
	}
	
	public static int getTypeForDataTypeString(String datatype)
	{
		if (datatype.trim().equalsIgnoreCase("integer"))
			return Types.INTEGER;
		else if (datatype.trim().equalsIgnoreCase("float") || datatype.trim().equalsIgnoreCase("number"))
			return Types.DOUBLE;
		else if (datatype.trim().equalsIgnoreCase("varchar"))
			return Types.VARCHAR;
		else if (datatype.trim().equalsIgnoreCase("datetime") || datatype.trim().equalsIgnoreCase("date"))
			return Types.TIMESTAMP;
		else
			return Types.VARCHAR;
	}
	
	AtomicInteger startIndex = new AtomicInteger(0);
	DataUnavailableException exception1;
	GoogleAnalyticsException exception2;
	SQLException exception3;
	NavigationException exception4;
	Exception exception5;

	public class ResultSetThread extends Thread
	{
		private DatabaseConnectionResultSet[] list;
		private DerivedTableQuery[] dtables;
		private List<DatabaseConnection> connectionList;
		private List<Navigation> navList;
		int curIndex;
		private int maxRecords; 
		private int maxQueryTime;
		Connection conn = null;
		DatabaseConnection dc = null;	
		Session session;
		
		public ResultSetThread(List<Navigation> navList, DerivedTableQuery[] dtables, List<DatabaseConnection> connectionList,
				DatabaseConnectionResultSet[] list, int maxRecords, int maxQueryTime, Session session)
		{
			this.setName("ResultSetThread - " + this.getId());
			setDaemon(true); // if the JVM wants to exit due to errors, let it
			this.navList = navList;
			this.list = list;
			this.dtables = dtables;
			this.connectionList = connectionList;
			this.maxRecords = maxRecords;
			this.maxQueryTime = maxQueryTime;
			this.session = session;
		}

		public void run()
		{
			while (true)
			{
				curIndex = startIndex.getAndIncrement();
				if (curIndex >= list.length)
					return;
				Navigation nav = navList.get(curIndex);
				DatabaseConnectionStatement stmt = null;
				boolean removeStatementInFinallyBlock = false;
				try
				{					
					if (dtables[curIndex].getQuery() != null)
					{
						logger.info("Physical Query: " + dtables[curIndex].getQuery().toString());
					}
					dc = nav.getNavigationConnection();					
					if (curIndex == 0 || dc.isRealTimeConnection())
					{
						// Re-use the default one
						int sindex = connectionList.indexOf(dc);
						stmt = statementList.get(sindex);
					} else
					{
						conn = ConnectionPool.getParallelOrDynamicConnection(dc, q.getSession(), true);
						Statement jdbcstmt = conn.createStatement();
						stmt = new DatabaseConnectionStatement(dc, jdbcstmt, q.getRepository());
						dc.ConnectionPool.addStatementForConnection(conn, jdbcstmt);
						if(session != null)
						{
							removeStatementInFinallyBlock = true;
							session.addCurrentStatement(stmt);
						}
					}

					// deal with DBMS's that don't support these two calls
					try
					{
						int curMaxRows = stmt.getMaxRows();
						// use some limiters to make sure we don't trash the SQL Server or the web application server
						if ((maxRecords > 0) && (maxRecords != Integer.MAX_VALUE) && ((maxRecords +2) != curMaxRows))
						{
							stmt.setMaxRows(maxRecords + 2);
						}
						if (maxQueryTime > 0 && maxQueryTime != Integer.MAX_VALUE)
						{
							stmt.setQueryTimeout(maxQueryTime);
						}
					} catch (Exception e)
					{
						if (logger.isTraceEnabled())
							logger.trace(e.getMessage(), e);
					}
					long starttime = System.currentTimeMillis();
					DatabaseConnectionResultSet dcrs = null;
					
					if(dtables[curIndex].containsFederatedJoin())
					{
						dcrs = stmt.executeQuery(dtables[curIndex].getFederatedJoinQueryHelper(), q, maxRecords, maxQueryTime, session);
					}
					else
					{
						dcrs = stmt.executeQuery(dtables[curIndex].getQuery().toString(), q);
					}
					if (dtables[curIndex].getQuery() != null)
					{
						logger.info("Subquery:" + queryID + ":" + (System.currentTimeMillis() - starttime) + "ms:" + dtables[curIndex].getQuery().toString());
					}
					list[curIndex] = dcrs;
				} catch (DataUnavailableException e)
				{
					exception1 = e;
				} catch (GoogleAnalyticsException e)
				{
					exception2 = e;
				} catch (SQLException e)
				{
					exception3 = e;
				} catch (NavigationException e)
				{
					exception4 = e;
				} catch (Exception e)
				{
					exception5 = e;
				} finally 
				{
					if (session != null && removeStatementInFinallyBlock)
					{
						session.removeStatement(stmt);						
					}
				}
			}
		}
	}

	private DatabaseConnectionResultSet[] getResultSets(List<Navigation> navList, DerivedTableQuery[] dtables, List<DatabaseConnection> connectionList,
			int maxRecords, int maxQueryTime, Session session) throws SQLException, BaseException
	{
		DatabaseConnectionResultSet[] list = new DatabaseConnectionResultSet[navList.size()];
		// Spin threads to get the variable-specific of the result sets
		int numThreads = 4;
		Thread[] tlist = new Thread[numThreads];
		exception1 = null;
		exception2 = null;
		exception3 = null;
		exception4 = null;
		for (int i = 0; i < numThreads; i++)
		{
			tlist[i] = new ResultSetThread(navList, dtables, connectionList, list, maxRecords, maxQueryTime, session);
			tlist[i].start();
		}
		try
		{
			for (int i = 0; i < numThreads; i++)
			{
				tlist[i].join();
			}
		} catch (InterruptedException ex)
		{
			logger.error("InteruptedException: " + ex.getCause());
			return null;
		}
		if(exception1 != null || exception2 != null || exception3 != null || exception4 != null || exception5 != null)
		{
			if (tlist != null)
			{
				for (int i=0; i<tlist.length; i++)
				{
					if(tlist[i] == null)
					{
						continue;
					}
					DatabaseConnection dconn = ((ResultSetThread)tlist[i]).dc;
					Connection c = ((ResultSetThread)tlist[i]).conn;
					int index  = ((ResultSetThread)tlist[i]).curIndex; 
					if((c != null) && (dconn != null))
					{
						if((index != 0) && (!dconn.isRealTimeConnection()))
						{
							ConnectionPool.returnToParallelPoolOrCloseDynamicConnection(c, dconn, q.getSession(), true);
						}
					}
				}
			}
		}
		if (exception1 != null)
			throw exception1;
		if (exception2 != null)
			throw exception2;
		if (exception3 != null)
			throw exception3;
		if (exception4 != null)
			throw exception4;
		if (exception5 != null)
		{
			logger.error(exception5.toString(),exception5);
		}
		return list;
	}

	private DatabaseConnectionResultSet joinResultSets(Session session) throws SQLException, BaseException
	{
		DerivedTableQuery[] dtables = q.getDerivedTables();
		List<Navigation> navList = q.getNavigationList();
		TreeMap<Comparable<JoinEntry>, Object[][]> joinedSet = new TreeMap<Comparable<JoinEntry>, Object[][]>();
		List<DatabaseConnection> connectionList = q.getConnections();
		QueryMetaData qmd = new QueryMetaData();
		int numColumns = q.getReturnProjectionListSize();
		qmd.setColumnCount(numColumns);
		int[] dataTypes = new int[numColumns];
		Map<QueryColumn, Integer> queryColumnDataType = new HashMap<QueryColumn, Integer>();
		List<QueryColumn> returnProjectionList = q.getReturnProjectionList();
		boolean containsFilteredMeasure = q.containsFilteredMeasure();
		DatabaseConnectionResultSet[] rslist = null;
		try
		{
			rslist = getResultSets(navList, dtables, connectionList, q.getRepository().getServerParameters().getMaxRecords(), q
						.getRepository().getServerParameters().getMaxQueryTime(), session);		
			for (int i = 0; i < returnProjectionList.size(); i++)
			{
				QueryColumn qc = returnProjectionList.get(i);
				if (qc.type == QueryColumn.DIMENSION)
				{
					queryColumnDataType.put(qc, getTypeForDataTypeString(((DimensionColumn)qc.o).DataType));
				}
				else if (qc.type == QueryColumn.MEASURE)
				{
					queryColumnDataType.put(qc, getTypeForDataTypeString(((MeasureColumn)qc.o).DataType));
				}
			}
			Calendar calProcTZ = Calendar.getInstance(q.getRepository().getServerParameters().getProcessingTimeZone());
			Calendar calData = Calendar.getInstance(q.getRepository().getServerParameters().getProcessingTimeZone());
			boolean mapNullToZero = q.getRepository().getServerParameters().isMapNullToZero();
			Map<String, List<Integer>> surrogateKeysInNavList = new HashMap<String, List<Integer>>();
			for (int index = 0; index < navList.size(); index++)
			{
				List<QueryColumn> navplist = dtables[index].getResultProjectionList();
				for (QueryColumn qc : navplist)
				{
					if (qc.type == QueryColumn.DIMENSION && qc.o != null && qc.o instanceof DimensionColumn && ((DimensionColumn)qc.o).SurrogateKey)
					{
						String columnName = ((DimensionColumn)qc.o).ColumnName;
						List<Integer> skInNavList = null;
						if (surrogateKeysInNavList.containsKey(columnName))
						{
							skInNavList = surrogateKeysInNavList.get(columnName);
						}
						else
						{
							skInNavList = new ArrayList<Integer>();
						}
						skInNavList.add(index);
						surrogateKeysInNavList.put(columnName, skInNavList);
					}
				}
			}		
			for (int index = 0; index < navList.size(); index++)
			{
				boolean isDbTypeIB = DatabaseConnection.isDBTypeMySQL(navList.get(index).getNavigationConnection().DBType);
				List<Integer> dcindices = new ArrayList<Integer>();
				// Get columns to join on
				boolean firstRow = true;
				DatabaseConnectionResultSet dcrs = rslist[index];
				if (dcrs == null)
					continue;
				int colCount = dcrs.getColumnCount();
				List<QueryColumn> navplist = dtables[index].getResultProjectionList();
				int[] dataTypesNav = new int[colCount];
				while (dcrs.next())
				{
					if (firstRow)
					{
						// Need to do this after the first row is retrieved or no metadata is ready
						firstRow = false;
						int typeIndex = 0;
						for (int colIndex = 0; colIndex < navplist.size(); colIndex++)
						{
							while (dcrs.getColumnType(typeIndex + 1) == Types.NULL)
							{
								// This is placeholder column cannot contain values
								dataTypesNav[typeIndex++] = Types.NULL;							
							}
							QueryColumn qc = navplist.get(colIndex);
							if (qc.type == QueryColumn.DIMENSION)
							{
								if (qc.o != null && qc.o instanceof DimensionColumn && ((DimensionColumn)qc.o).SurrogateKey)
								{
									List<Integer> skInNavList = surrogateKeysInNavList.get(((DimensionColumn)qc.o).ColumnName);
									if (skInNavList.size() == navList.size())
									{
										dcindices.add(colIndex);
									}
									else
									{
										logger.warn("SurrogateKey Column '" + ((DimensionColumn)qc.o).ColumnName + "' not present on all navigation lists, " +
												"ignoring column for joining using federation.");
									}
								}
								else
								{
									dcindices.add(colIndex);
								}
							}
							//use query's datatype if querycolumn is part of query's resultprojectionlist
							if (!queryColumnDataType.containsKey(qc))
							{
								if (qc.type == QueryColumn.DIMENSION)
								{
									queryColumnDataType.put(qc, getTypeForDataTypeString(((DimensionColumn)qc.o).DataType));
								}
								else if (qc.type == QueryColumn.MEASURE)
								{
									queryColumnDataType.put(qc, getTypeForDataTypeString(((MeasureColumn)qc.o).DataType));
								}
								else
								{
									queryColumnDataType.put(qc, getGenericSqlTypeForDataType(dcrs.getColumnType(typeIndex + 1), qc));
								}
							}
							dataTypesNav[typeIndex++] = queryColumnDataType.get(qc);
						}
					}
					Object[] row = new Object[colCount];
					for (int col = 0; col < colCount; col++)
					{
						if (dataTypesNav.length > col)
							row[col] = getObjectForDataType(dataTypesNav[col], col, dcrs, mapNullToZero, calProcTZ, calData, isDbTypeIB);
						else
							row[col] = dcrs.getObject(col + 1);
						if((dataTypesNav.length > col && dataTypesNav[col] == Types.VARCHAR) && dcrs.getObject(col + 1) != null && dcrs.getObject(col + 1) instanceof byte[])
						{
							String strValue = new String((byte [])row[col]);
							row[col] = strValue;
						}
					}
					JoinEntry je = new JoinEntry();
					je.row = new Object[dcindices.size()];
					for (int i = 0; i < dcindices.size(); i++)
						je.row[i] = row[dcindices.get(i)];
					Object[][] rowset = joinedSet.get(je);
					if (rowset == null)
					{
						rowset = new Object[navList.size()][];
						joinedSet.put(je, rowset);
					}
					rowset[index] = row;
				}
			
				//The flag firstRow is still true meaning the resultset doesn't have any rows. In 
				//that case set the data types - this code is needed for TOP{0} queries that are 
				//executed for setting report design while saving a report. The correct data types
				//are needed especially for subreport that are part of another main report.
				if(firstRow)
				{
					firstRow = false;
					colCount = dcrs.getColumnCount();
					int typeIndex = 0;
					for (int colIndex = 0; colIndex < navplist.size(); colIndex++)
					{
						while (dcrs.getColumnType(typeIndex + 1) == Types.NULL)
						{
							// This is placeholder column cannot contain values
							dataTypesNav[typeIndex++] = Types.NULL;						
						}
						QueryColumn qc = navplist.get(colIndex);
						//use query's datatype if querycolumn is part of query's resultprojectionlist
						if (!queryColumnDataType.containsKey(qc))
						{
							if (qc.type == QueryColumn.DIMENSION)
							{
								queryColumnDataType.put(qc, getTypeForDataTypeString(((DimensionColumn)qc.o).DataType));
							}
							else if (qc.type == QueryColumn.MEASURE)
							{
								queryColumnDataType.put(qc, getTypeForDataTypeString(((MeasureColumn)qc.o).DataType));
							}
							else
							{
								queryColumnDataType.put(qc, getGenericSqlTypeForDataType(dcrs.getColumnType(typeIndex + 1), qc));
							}
						}
						dataTypesNav[typeIndex++] = queryColumnDataType.get(qc);
					}
				}
			}
		}
		finally
		{
			if (rslist != null)
			{
				for (int i=0; i<rslist.length; i++)
				{
					DatabaseConnectionResultSet dcrs = rslist[i];
					if (dcrs != null)
					{
						ResultSet rs = dcrs.getResultSet();
						DatabaseConnection dc = dcrs.getDatabaseConnection();
						if ((i != 0) && (dc != null) && (!dc.isRealTimeConnection())) //Do not close the connection related to query's statement list
						{
							if (rs != null && rs.getStatement() != null && rs.getStatement().getConnection() != null && dc != null)
							{
								ConnectionPool.returnToParallelPoolOrCloseDynamicConnection(rs.getStatement().getConnection(), dc, q.getSession(), true);
							}
						}
					}
				}
			}
		}
		for (int i = 0; i < returnProjectionList.size(); i++)
		{
			QueryColumn qc = returnProjectionList.get(i);
			dataTypes[i] = queryColumnDataType.get(qc);
		}
		List<Boolean> isNavigationOnCube = new ArrayList<Boolean>();
		for (Navigation nav : navList)
		{
			DatabaseConnection navConn = nav.getNavigationConnection();
			if (navConn != null)
			{
				if (DatabaseConnection.isDBTypeXMLA(navConn.DBType))
					isNavigationOnCube.add(Boolean.TRUE);
				else
					isNavigationOnCube.add(Boolean.FALSE);
			}
			else
			{
				isNavigationOnCube.add(Boolean.FALSE);
			}
		}
		List<Object[]> newRows = new ArrayList<Object[]>();
		boolean fullOuterJoin = q.isFullOuterJoin();
		for (Object[][] row : joinedSet.values())
		{
			Object[] newRow = new Object[numColumns];
			boolean skipForInnerJoin = false;
			QueryColumn foundqc = null;
			for (int i = 0; i < numColumns; i++)
			{
				QueryColumn qc = q.getReturnProjectionListColumn(i);
				int navIndex = -1;
				if (qc.type == QueryColumn.DIMENSION || qc.type == QueryColumn.CONSTANT_OR_FORMULA)
				{
					for (int j = 0; j < navList.size(); j++)
					{
						if (row[j] == null)
						{
							if (!fullOuterJoin && !containsFilteredMeasure)
							{
								skipForInnerJoin = true;
								break;
							}
							continue;
						} else
						{
							Navigation nav = navList.get(j);
							List<QueryColumn> qclist = nav.getProjectionList();
							for (int k = 0; k < qclist.size(); k++)
							{
								if (qclist.get(k).name.equals(qc.name))
								{
									navIndex = j;
									foundqc = qclist.get(k);
									break;
								}
							}
						}
					}
					if (skipForInnerJoin)
						break;
				} else if (qc.type == QueryColumn.MEASURE)
				{
					for (navIndex = 0; navIndex < navList.size(); navIndex++)
					{
						Navigation nav = navList.get(navIndex);
						if (nav.getProjectionList().indexOf(qc) >= 0)
						{
							foundqc = qc;
							break;
						}
					}
				}
				if (navIndex >= 0 && navIndex < navList.size())
				{
					int qindex = 0;
					boolean isCube = isNavigationOnCube.get(navIndex);
					List<QueryColumn> plist = dtables[navIndex].getResultProjectionList();
					// Figure out column index - pick the columns in the order they would've been issued
					for (int index = 0; index < plist.size(); index++)
					{
						if (plist.get(index) == foundqc)
						{
							if (row[navIndex] != null)
							{
								int qci = q.getReturnProjectionList().indexOf(qc);
								if (qci < 0)
									continue;
								if (isCube)
								{
									if (qc.columnIndex < 0)
										continue;
									newRow[qci] = row[navIndex][qc.columnIndex];
								}
								else
								{
									newRow[qci] = row[navIndex][qindex];
								}
							}
							break;
						}
						if (plist.get(index).columnIndex >= 0)
							qindex++;
					}
				}
			}
			if (!skipForInnerJoin)
				newRows.add(newRow);
		}
		return new DatabaseConnectionResultSet(dataTypes, newRows);
	}

	/**
	 * Process all dimension column mappings
	 */
	public void mapDimensionColumns()
	{
		List<DimensionColumnNav> mapCols = q.getMappedDimensionColumns();
		if (mapCols != null)
		{
			// Clear any previous maps
			for (int i = 0; i < columnNames.length; i++)
			{
				columnMaps[i] = null;
				mapped[i] = false;
			}
			for (DimensionColumnNav dcn : mapCols)
			{
				// Find column index
				int index = -1;
				for (int i = 0; i < columnNames.length; i++)
				{
					if (tableNames[i].equals(dcn.dimensionName) && columnNames[i].equals(dcn.columnName))
					{
						index = i;
						mapped[i] = true;
						break;
					}
				}
				if (index >= 0)
				{
					columnMaps[index] = new HashMap<Object, Object>(rows.length);
					for (int i = 0; i < rows.length; i++)
					{
						columnMaps[index].put(rows[i][index], rows[i][index + 1]);
					}
				}
			}
		}
	}

	/**
	 * Generate a list of the column names in the supplied query. Return a list of string arrays. First element is the
	 * base column, the second is the required display name, the third is a flag for measure "M" or dimension column "D"
	 * 
	 * @param q
	 * @param expressionList
	 * @return
	 * @throws NavigationException
	 * @throws ScriptException 
	 */
	protected static List<String[]> determineColumns(Query q, List<DisplayExpression> expressionList) throws NavigationException, CloneNotSupportedException,
			BadColumnNameException, ScriptException
	{
		List<Object> clist = q.getSuppliedQueryColumns();
		List<String[]> result = new ArrayList<String[]>();
		List<OlapMemberExpression> olapExpressionsFromQuery = new ArrayList<OlapMemberExpression>();
		for (int i = 0; i < clist.size(); i++)
		{
			Object item = clist.get(i);
			if (item.getClass() == DimensionColumnNav.class)
			{
				DimensionColumnNav dcn = (DimensionColumnNav) item;
				result.add(new String[]
				{ dcn.dimensionName + "." + dcn.columnName, dcn.displayName, "D", null });
			} else if (item.getClass() == DimensionMemberSetNav.class)
			{
				DimensionMemberSetNav dms = (DimensionMemberSetNav) item;
				result.add(new String[]
				{ dms.dimensionName + "." + dms.getMemberExpressionsString(), dms.displayName, "DM", null });
				if (i < clist.size() - 1 && item == clist.get(i + 1) && dms.parentDisplayName != null && !dms.parentDisplayName.isEmpty())					
				{
					result.add(new String[]
					{ dms.dimensionName + "." + dms.getMemberExpressionsString(), dms.parentDisplayName, "DMP", null });
					i++;
				}
				if (i < clist.size() - 1 && item == clist.get(i + 1) && dms.parentNameDisplayName != null && !dms.parentNameDisplayName.isEmpty())					
				{
					result.add(new String[]
					{ dms.dimensionName + "." + dms.getMemberExpressionsString(), dms.parentDisplayName, "DMPN", null });
					i++;
				}
				if (i < clist.size() - 1 && item == clist.get(i + 1) && dms.ordinalDisplayName != null && !dms.ordinalDisplayName.isEmpty())					
				{
					result.add(new String[]
					{ dms.dimensionName + "." + dms.getMemberExpressionsString(), dms.ordinalDisplayName, "DMO", null });
					i++;
				}
				if (i < clist.size() - 1 && item == clist.get(i + 1) && dms.uniqueNameDisplayName != null && !dms.uniqueNameDisplayName.isEmpty())					
				{
					result.add(new String[]
					{ dms.dimensionName + "." + dms.getMemberExpressionsString(), dms.uniqueNameDisplayName, "DMU", null });
					i++;
				}
			} else if (item.getClass() == OlapMemberExpression.class)
			{
				OlapMemberExpression exp = (OlapMemberExpression) item;
				olapExpressionsFromQuery.add(exp);
				result.add(new String[]
				{ exp.expression, (exp.displayName != null ? exp.displayName : exp.expression), "OEXPR", null });
			} else if (item.getClass() == MeasureColumnNav.class)
			{
				MeasureColumnNav mcn = (MeasureColumnNav) item;
				String add = null;
				for (Aggregation agg : q.getAggregations())
				{
					if (agg.getColumnName().equals(mcn.displayName) && mcn.colIndex == agg.getIndex())
					{
						if (agg.getType() == AggregationType.Rank)
							add = "RANK";
						else if (agg.getType() == AggregationType.DenseRank)
							add = "DRANK";
						else if (agg.getType() == AggregationType.Percentile)
							add = "PTILE";
					}
				}
				if (mcn.filter != null)
					result.add(new String[]
					{ mcn.measureName, mcn.displayName, "M", mcn.filter.getQueryKeyStr(q, null) + (add != null ? add : "") });
				else
					result.add(new String[]
					{ mcn.measureName, mcn.displayName, "M", add });
			}
		}
		/*
		 * Now, add expressions to the list
		 */
		if (expressionList != null)
		{
			Collections.sort(expressionList);
			for (DisplayExpression de : expressionList)
			{
				if (de.olapMemberExpressionColumns != null && de.olapMemberExpressionColumns.size() == 1)
				{
					boolean found = false;
					for (OlapMemberExpression exp : de.olapMemberExpressionColumns)
					{
						for (OlapMemberExpression qexp : olapExpressionsFromQuery)
						{
							if (qexp.expression.equals(exp.expression))
							{
								found = true;
								break;
							}
						}
					}
					if (found)
						continue;
				}
				int pos = de.getPosition();
				if (pos < 0 || pos > result.size())
				{
					pos = result.size();
					de.setPosition(pos);
				}
				result.add(pos, new String[]
				{ de.getName(), de.getName(), "E", null });
			}
		}
		return (result);
	}

	/**
	 * Process the result set to do aggregations, reorder columns, ensure appropriate display names and add expressions
	 * 
	 * @param q
	 * @param expressionList
	 * @throws SyntaxErrorException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private void processResultSet(ResultSetCache rsc, List<String[]> colList, List<DisplayExpression> expressionList, List<DisplayExpression> processExpressionList, List<DisplayFilter> displayFilters,
			List<String> addedNames, RowMapCache rmap, List<DisplayOrder> displayOrderList, Query q) throws NavigationException, SyntaxErrorException,
			IOException, ClassNotFoundException, CloneNotSupportedException, BadColumnNameException, NullPointerException
	{
		try
		{
			// Determine binding of original columns
			List<Integer> bindings = new ArrayList<Integer>();
			List<MeasureExpressionColumn> meColumns = new ArrayList<MeasureExpressionColumn>();
			for (int i = 0; i < colList.size(); i++)
			{
				boolean bound = false;
				if (expressionList != null)
					for (DisplayExpression de : expressionList)
					{
						if (de.getPosition() == i)
						{
							bindings.add(-1);
							bound = true;
						}
						if (de.measureExpressionColumns != null)
							meColumns.addAll(de.measureExpressionColumns);
					}
				if (!bound)
					for (int j = 0; j < columnNames.length; j++)
					{
						if (columnTypes[j] == QueryColumn.DIMENSION || columnTypes[j] == QueryColumn.DIMENSION_MEMBER_SET)
						{
							if (colList.get(i)[0].equals(tableNames[j] + "." + columnNames[j]))
							{
								bindings.add(j);
								if (columnTypes[j] == QueryColumn.DIMENSION_MEMBER_SET)
								{
									for (int k = i+1; k < colList.size(); k++)
									{
										for (int l = j+1; l < columnNames.length; l++)
										{
											if ((tableNames[j] + "." + columnNames[j]).equals(tableNames[l] + "." + columnNames[l])
													&& (colList.get(k)[0].equals(tableNames[l] + "." + columnNames[l]) && colList.get(k)[2] != null
													&& (colList.get(k)[2].equals("DMP") || colList.get(k)[2].equals("DMPN") || colList.get(k)[2].equals("DMO") || colList.get(k)[2].equals("DMU"))))
											{
												bindings.add(l);
												i = k;
												j = l;
												k++;
											}
										}
									}
								}
								break;
							}
						} else if (columnTypes[j] == QueryColumn.OLAP_MEMBER_EXPRESSION)
						{
							if (colList.get(i)[0].equals(columnNames[j]))
							{
								bindings.add(j);
								break;
							}
						} else if (columnTypes[j] == QueryColumn.MEASURE)
						{
							if (colList.get(i)[0].equals(columnNames[j]))
							{
								boolean bind = true;
								String typeParm = colList.get(i).length <= 3 ? null : colList.get(i)[3];
								String att = measureFilters[j] == null ? null : measureFilters[j].getQueryKeyStr(q, null);
								if (att == null
										&& (typeParm == null || !(typeParm.startsWith("F{") || typeParm.startsWith("FOR{") || typeParm.startsWith("FAND{"))))
									bind = true;
								else if (att != null && typeParm != null && typeParm.startsWith(att))
								{
									if (typeParm.length() > att.length())
									{
										if (typeParm.substring(att.length()).equals("RANK") || typeParm.substring(att.length()).equals("DRANK")
												|| typeParm.substring(att.length()).equals("PTILE"))
											bind = true;
										else
											bind = false;
									} else
										bind = true;
								} else
									bind = false;
								if (bind)
								{
									bindings.add(j);
									break;
								}
							}
						} else if (columnTypes[j] == QueryColumn.CONSTANT_OR_FORMULA)
						{
							if (colList.get(i)[0].equals("CONSTANT$." + columnNames[j]))
							{
								bindings.add(j);
								break;
							}
						}
					}
			}
			String[] newDisplayNames = new String[bindings.size()];
			String[] newAggregationRules = new String[bindings.size()];
			String[] newColumnNames = new String[bindings.size()];
			int[] newColumnTypes = new int[bindings.size()];
			int[] newColumnDataTypes = new int[bindings.size()];
			int[] newMaskable = new int[bindings.size()];
			String[] newWarehouseColumnsDataTypes = new String[bindings.size()];
			String[] newTableNames = new String[bindings.size()];
			String[] newColumnFormats = new String[bindings.size()];
			AggregationType[] newAggregations = new AggregationType[bindings.size()];
			QueryFilter[] newMeasureFilters = new QueryFilter[bindings.size()];
			boolean[] newMapped = new boolean[bindings.size()];
			Map<Object, Object>[] newColumnMaps = new Map[bindings.size()];
			if(bindings.size() != colList.size())
			{
				logger.warn("Size of data bindings and column list is different.");
			}
			// Copy original column information
			for (int i = 0; i < bindings.size(); i++)
			{
				if (bindings.get(i) >= 0)
				{
					newDisplayNames[i] = colList.get(i)[1];
					int binding = bindings.get(i);
					newAggregationRules[i] = aggregationRules[binding];
					newColumnNames[i] = columnNames[binding];
					newColumnTypes[i] = columnTypes[binding];
					newColumnDataTypes[i] = columnDataTypes[binding];
					newWarehouseColumnsDataTypes[i] = warehouseColumnsDataTypes[binding];
					newMaskable[i] = maskable[binding];
					newTableNames[i] = tableNames[binding];
					newColumnFormats[i] = columnFormats[binding];
					newAggregations[i] = aggregations[binding];
					newMeasureFilters[i] = measureFilters[binding];
					newMapped[i] = mapped[binding];
					newColumnMaps[i] = columnMaps[binding];
				}
			}
			// Add expression column information
			if (expressionList != null)
				for (DisplayExpression de : expressionList)
				{
					if (de.getPosition() >= 0)
					{
						if (bindings.size() <= de.getPosition())
						{
							logger.debug("Display Expression position outside of the array bounds (program bug?): "
									+ de.getPosition() + "," + bindings.size() + ": " + de.getExpression());
							continue;
						}
						newDisplayNames[de.getPosition()] = de.getName();
						newColumnNames[de.getPosition()] = de.getName();
						@SuppressWarnings("rawtypes")
						Class ctype = de.getCompiledClass();
						if (ctype == Double.class)
						{
							newColumnDataTypes[de.getPosition()] = Types.DOUBLE;
							newWarehouseColumnsDataTypes[de.getPosition()] = Repository.DOUBLE;
							newColumnTypes[de.getPosition()] = QueryColumn.MEASURE;
						} else if (ctype == Integer.class)
						{
							newColumnDataTypes[de.getPosition()] = Types.INTEGER;
							newWarehouseColumnsDataTypes[de.getPosition()] = Repository.INTEGER;
							if (!de.hasMeasuresExpressionColumns())
							{
								newColumnTypes[de.getPosition()] = QueryColumn.DIMENSION;
							}
							else
							{
								newColumnTypes[de.getPosition()] = QueryColumn.MEASURE;
							}
						} else if (ctype == Long.class)
						{
							newColumnDataTypes[de.getPosition()] = Types.BIGINT;
							newWarehouseColumnsDataTypes[de.getPosition()] = Repository.INTEGER;
							if (!de.hasMeasuresExpressionColumns())
							{
								newColumnTypes[de.getPosition()] = QueryColumn.DIMENSION;
							}
							else
							{
								newColumnTypes[de.getPosition()] = QueryColumn.MEASURE;
							}
						} else if (ctype == String.class)
						{
							newColumnDataTypes[de.getPosition()] = Types.VARCHAR;
							newColumnTypes[de.getPosition()] = QueryColumn.DIMENSION;
							newWarehouseColumnsDataTypes[de.getPosition()] = Repository.VARCHAR;
						} else if (ctype == Timestamp.class)
						{
							newColumnDataTypes[de.getPosition()] = Types.TIMESTAMP;
							newColumnTypes[de.getPosition()] = QueryColumn.DIMENSION;
							newWarehouseColumnsDataTypes[de.getPosition()] = Repository.DATETIME;
						}
						else if (ctype == GregorianCalendar.class)
						{
							newColumnDataTypes[de.getPosition()] = Types.TIMESTAMP;
							newColumnTypes[de.getPosition()] = QueryColumn.DIMENSION;
							newWarehouseColumnsDataTypes[de.getPosition()] = Repository.DATETIME;
						}
						else if (ctype == Boolean.class)
						{
							newColumnDataTypes[de.getPosition()] = Types.BOOLEAN;
							newColumnTypes[de.getPosition()] = QueryColumn.DIMENSION;
							newWarehouseColumnsDataTypes[de.getPosition()] = Repository.BOOLEAN;
						} else
						{
							newColumnDataTypes[de.getPosition()] = Types.VARCHAR;
							newColumnTypes[de.getPosition()] = QueryColumn.DIMENSION;
							newWarehouseColumnsDataTypes[de.getPosition()] = Repository.VARCHAR;
						}
						newTableNames[de.getPosition()] = EXPR_TABLE_NAME;
						newColumnFormats[de.getPosition()] = "###,###,###.##";
						newAggregations[de.getPosition()] = AggregationType.None;
						newAggregationRules[de.getPosition()] = de.getAggRule();
					}
				}
			displayNames = newDisplayNames;
			aggregationRules = newAggregationRules;
			columnNames = newColumnNames;
			columnTypes = newColumnTypes;
			columnDataTypes = newColumnDataTypes;
			warehouseColumnsDataTypes = newWarehouseColumnsDataTypes;
			maskable = newMaskable;
			tableNames = newTableNames;
			columnFormats = newColumnFormats;
			aggregations = newAggregations;
			measureFilters = newMeasureFilters;
			mapped = newMapped;
			columnMaps = newColumnMaps;
			/*
			 * Now evaluate expressions (insert into the result set as needed)
			 */
			Set<Integer> expColToProcessPositions = new HashSet<Integer>();
			if (processExpressionList != null && !processExpressionList.isEmpty())
			{
				for (DisplayExpression de : processExpressionList)
				{
					de.setFields(new FieldProvider(de));
					de.setContext(new Object[]
					{ de.getFields() });
					for (int pos=0; pos<bindings.size(); pos++)
					{
						if (bindings.get(pos) < 0)
						{
							if (pos == de.getPosition())
							{
								expColToProcessPositions.add(pos);
							}
						}
					}
				}				
			}
			/* Copy data from old result set to new one */
			for (int i = 0; i < rows.length; i++)
			{
				Object[] newRow = new Object[bindings.size()];
				for (int j = 0; j < newRow.length; j++)
				{
					if (bindings.get(j) >= 0)
						newRow[j] = rows[i][bindings.get(j)];
					else if (rows[i].length > j && !expColToProcessPositions.contains(j))
						newRow[j] = rows[i][j];
				}
				rows[i] = newRow;
			}
			
			// deal with sparse result sets
			if (q.dimensionColumns != null)
			{
				Repository r = q.getRepository();
				List <DisplayFilter> filtersForNewRows = new ArrayList<DisplayFilter>();
				
				// find all dimension tables in the query (this is so we can group queries around tables rather than individual columns - so we don't get ugly cross products)
				Map<DimensionTable, List<DimensionColumnNav>> dimTables = new HashMap<DimensionTable, List<DimensionColumnNav>>();
				for (DimensionColumnNav dcn : q.dimensionColumns)
				{
					if (dcn.navigateonly) // navigate only are not in the projection list, thus we don't need to get them for SPARSE
						continue;
					DimensionTable tbl = null;
					if (dcn.matchingColumns.size() == 0)
					{
						// Possibly a redundant column
						for(DimensionColumnNav dcn2: q.dimensionColumns)
						{
							if (dcn2.matchingColumns.size() > 0 && dcn2 != dcn && dcn2.dimensionName.equals(dcn.dimensionName) && dcn2.columnName.equals(dcn.columnName))
							{
								tbl = dcn2.matchingColumns.get(dcn.pick).DimensionTable;
								break;
							}
						}
						if (tbl == null)
							continue;
					} else
						tbl = dcn.matchingColumns.get(dcn.pick).DimensionTable;
					if (!dimTables.containsKey(tbl))
						dimTables.put(tbl, new ArrayList<DimensionColumnNav>());
					dimTables.get(tbl).add(dcn);
				}
				List<SparseSet> sparseSets = new ArrayList<SparseSet>();
				for (DimensionTable dt : dimTables.keySet())
				{
					List<DimensionColumnNav> dims = dimTables.get(dt);
					// are any of the dimension columns marked sparse for this table
					boolean hasSparse = false;
					for (DimensionColumnNav dcn : dims)
					{
						if (dcn.sparse)
						{
							hasSparse = true;
							break;
						}
					}
					// if none of the dimension columns for the table are sparse, skip it
					if (!hasSparse)
						continue;
					
					Query sparseQ = new Query(r);
					Set<QueryFilter> processedFilters = new HashSet<QueryFilter>();
					// build a query for all dimension columns in the query associated with the table
					for (DimensionColumnNav dcn : dims)
					{
						sparseQ.addDimensionColumn(dcn.dimensionName, dcn.columnName);
						GroupBy gb = new GroupBy(dcn.dimensionName, dcn.columnName);
						sparseQ.addGroupBy(gb);
						// copy any dimension only filters for the sparse query, also build up a list of other filters for determining if the new rows should be added
						if (q.filters != null && (q.filters.size() > processedFilters.size()))
						{
							for (QueryFilter qf : q.filters)
							{
								if(processedFilters.contains(qf))
								{
									continue;
								}
								QueryFilter newqf = null;
								if(qf.containsOnlyDimension(dcn.dimensionName))
								{
									newqf = (QueryFilter) qf.clone();
								}
								else
								{
									newqf = qf.getDimensionOnlyFilter(dcn.dimensionName);
								}
								if (newqf != null)
								{
									sparseQ.addFilter(newqf); 
									processedFilters.add(qf);
								}
							}
							if(q.filters.size() > processedFilters.size())
							{
								for (QueryFilter qf : q.filters)
								{
									if(processedFilters.contains(qf))
									{
										continue;
									}
									QueryFilter newqf = qf.getDimensionOnlyFilter(dcn.dimensionName);
									if (newqf == null)
									{
										DisplayFilter newdf = qf.returnDisplayFilter(r, q.getSession());
										if (newdf != null)
										{
											boolean bound = newdf.bindToData(tableNames, newdf.isDisplayName() ? displayNames : columnNames, columnDataTypes, r);
											if(!bound)
											{
												bound = newdf.bindToData(tableNames, !newdf.isDisplayName() ? displayNames : columnNames, columnDataTypes, r);
											}
											if(bound)
											{
												filtersForNewRows.add(newdf);
											}
										}
										else  // should detect this on parse, if filtered measure and SPARSE keyword, syntax error
											throw new NavigationException("Can not combine SPARSE with filtered measures");
									}
								}
							}
						}
						if (filtersForNewRows.size() > 0)
						{
							try
							{
								bindFiltersToData(filtersForNewRows, r);
							}
							catch (SyntaxErrorException ex)
							{
							}
						}
					}
					
					// get the full set
					QueryResultSet sparseQrs = rsc.getQueryResultSet(sparseQ, r.getServerParameters().getMaxRecords(), r.getServerParameters()
							.getMaxQueryTime(), null, null, null, q.getSession(), false, null, true);

					// build the sparse result set that will be used below to inject missing rows
					SparseSet sparseSet = new SparseSet(new ArrayList<Integer>(), sparseQrs);
					sparseSets.add(sparseSet);
					for (int i = 0; i < columnTypes.length; i++)
					{
						for (DimensionColumnNav dcn : dims)
						{
							if (columnTypes[i] == QueryColumn.DIMENSION && dcn.columnName.equals(columnNames[i]) && dcn.dimensionName.equals(tableNames[i]))
								if (!sparseSet.colList.contains(i))
									sparseSet.colList.add(i);
						}
					}
				}

				if (!sparseSets.isEmpty())
				{
					// if there are any sparse sets, try to inject the missing rows
					StringBuilder sb = null;
					for (SparseSet sparseSet : sparseSets)
					{
						try
						{
							/*
							 * Find all combinations of other dimension columns
							 */
							boolean[] inKey = new boolean[columnTypes.length];
							boolean[] measures = new boolean[columnTypes.length];
							for (int j = 0; j < inKey.length; j++)
							{
								if (!sparseSet.colList.contains(j) && columnTypes[j] == QueryColumn.DIMENSION && colList.get(j)[2].equals("D"))
									inKey[j] = true;
								if (columnTypes[j] == QueryColumn.MEASURE)
									measures[j] = true;
							}
							if (sb == null)
								sb = new StringBuilder(10000);
							Map<String,List<Object[]>> dataMap = new HashMap<String, List<Object[]>>();
							for(int j = 0; j < rows.length; j++)
							{
								sb.setLength(0);
								for(int k = 0; k < inKey.length; k++)
								{
									if (inKey[k])
									{
										sb.append(rows[j][k]);
										sb.append('|');
									}
								}
								String key = sb.toString();
								List<Object[]> curRows = dataMap.get(key);
								if (curRows == null)
								{
									curRows = new ArrayList<Object[]>();
									dataMap.put(key, curRows);
								}
								curRows.add(rows[j]);
							}
							int numRows = sparseSet.qrs.numRows();
							List<Object[]> newRows = new ArrayList<Object[]>(numRows);
							for(List<Object[]> rowSet: dataMap.values())
							{
								/*
								 * Scan to see if we have a value for each entry - if not, insert a null measure row
								 */
								int setSize = rowSet.size();
								Object[] baseRow = rowSet.get(0);
								// for each row in the sparse set, see if it exists in the original set
								for (int j = 0; j < numRows; j++)
								{
									boolean found = false;
									// build a list of the column values from the sparse query
									List<Object> sparseValue = new ArrayList<Object>();
									for (int l = 0; l < sparseSet.colList.size(); l++)
										sparseValue.add(sparseSet.qrs.rows[j][l]);

									// iterate over the subset of rows in the original set and see if there is a match
									for(int k = 0; k < setSize; k++)
									{
										Object[] rs = rowSet.get(k);
										List<Object> o = new ArrayList<Object>();
										for (int l = 0; l < sparseSet.colList.size(); l++)
											o.add(rs[sparseSet.colList.get(l)]);
										if (o.equals(sparseValue))
										{
											found = true;
											if(newRows.size() > r.getServerParameters().getMaxRecords()){
												throw new ResultSetTooBigException("Population of sparse values hit max records limit");
											}
											newRows.add(rs);
											break;
										}
									}
									if (!found)
									{
										// no match, inject a new row
										Object[] newRow = new Object[baseRow.length];
										for(int k = 0; k < newRow.length; k++)
										{
											int pos = sparseSet.colList.indexOf(k);
											if (pos != -1)
												newRow[k] = sparseValue.get(pos);
											else if (!measures[k])
												newRow[k] = baseRow[k];
										}
										// before adding, see if it satisfies the original query filters (turned into display filters)
										if (filtersForNewRows.size() == 0 || isRowSatisfiedByFilters(newRow, filtersForNewRows, r)){
											if(newRows.size() > r.getServerParameters().getMaxRecords()){
												throw new ResultSetTooBigException("Population of sparse values hit max records limit");
											}
											newRows.add(newRow);
										}
									}
								}
							}
							rows = (Object[][]) new Object[newRows.size()][];
							newRows.toArray(rows);
						}catch(ResultSetTooBigException ex){
							logger.info("Population of sparse values hit max records limit");							
						}
					}
					// reapply ORDER BY
					List<OrderBy> obClauses = q.getOrderByClauses();		
					if (obClauses != null && !obClauses.isEmpty())
					{
						List<DisplayOrder> spaceDisplayOrderList = new ArrayList<DisplayOrder>();
						for (OrderBy ob : obClauses)
						{
							DisplayOrder dorder = ob.returnDisplayOrder();
							spaceDisplayOrderList.add(dorder);
						}
						processDisplayOrderings(spaceDisplayOrderList);
					}
				}
			}

			/*
			 * Process aggregations
			 */
			if (q.getAggregations() != null && !q.getAggregations().isEmpty())
				processAggregations(q, colList);

			/*
			 * Point all expressions to the current result set
			 */
			if (processExpressionList != null && !processExpressionList.isEmpty())
			{
				for (DisplayExpression de : processExpressionList)
				{
					Map<DimensionColumn, Groups> groups = de.getCustomGroups();
					if (groups != null && !groups.isEmpty())
					{
						for (Entry<DimensionColumn, Groups> entry : groups.entrySet())
						{
							createGroup(entry.getKey(), entry.getValue());
						}
					}
				}
				for (DisplayExpression de : processExpressionList)
				{
					FieldProvider fp = de.getFields();
					fp.setColumnList(colList, this.rows.length);
					fp.setArrays(rsc, this, rmap, meColumns);
					fp.setNumRows(rows.length);
					de.setResultSetCache(rsc);
					if (de.getScript() != null)
						de.getScript().setNumResultsetRows(rows.length);
				}
				/*
				 * Now process expressions
				 */
				boolean done = false;
				int iterations = 0;
				while (!done && iterations < 100)
				{
					done = processExpressions(processExpressionList);
					iterations++;
					for (DisplayExpression de : processExpressionList)
					{
						if (de.getScript() != null)
						{
							de.getScript().setIteration(iterations);
						}
					}
				}
				/*
				 * Now rank or percentile any columns necessary Note that these functions are deprecated in the new
				 * logical query language and are replaced by transformation methods
				 */
				for (DisplayExpression qse : processExpressionList)
				{
					if (qse.isRank())
					{
						aggColumn(qse.getPosition(), false, false, qse.getGroups());
					} else if (qse.isDenserank())
					{
						aggColumn(qse.getPosition(), true, false, qse.getGroups());
					} else if (qse.isPercentile())
					{
						aggColumn(qse.getPosition(), false, true, qse.getGroups());
					}
				}
			}
			/*
			 * If there are any added colums from expressions that are dimension columns, then the query could be at a
			 * lower level than originally requested. Aggregate if possible.
			 */
			List<GroupBy> aggList = q.getGroupByClauses();
			if (aggList != null)
			{
				boolean agg = true;
				/*
				 * Make sure all expressions are aggregatable first
				 */
				if (processExpressionList != null && !processExpressionList.isEmpty())
				{
					for (DisplayExpression de : processExpressionList)
					{
						if (!de.isAggregatable())
						{
							agg = false;
							break;
						}
					}
				}
				if (agg)
				{
					agg = false;
					List<GroupBy> newAggList = new ArrayList<GroupBy>();
					for (GroupBy gb : aggList)
					{
						int index = 0;
						for (; index < columnNames.length; index++)
						{
							if (columnNames[index] != null && columnNames[index].equals(gb.getColName()) && (tableNames[index] != null)
									&& tableNames[index].equals(gb.getDimName()))
								break;
						}
						if (index < columnNames.length)
						{
							if (addedNames != null && addedNames.contains(displayNames[index]))
							{
								agg = true;
							}
						}
						newAggList.add(gb);
					}
					/*
					 * Confirm that all the measures are aggregatable as well. If so, aggregate
					 */
					if (agg)
					{
						/*
						 * Get a list of measures to aggregate (not including ones added by expressions)
						 */
						List<String> mlist = new ArrayList<String>();
						for (int i = 0; i < columnNames.length; i++)
						{
							if (columnTypes[i] == QueryColumn.MEASURE && !tableNames[i].equals(EXPR_TABLE_NAME) && !addedNames.contains(displayNames[i]))
								mlist.add(columnNames[i]);
						}
						if (q.isAdditive(mlist, newAggList))
							aggregateResultSet(this, newAggList, q, displayOrderList);
					}
				}
			}
		} catch (Exception ex)
		{
			logger.error(ex, ex);
		}
	}
	
	class SparseSet
	{
		List<Integer> colList; // indexes of attribute values in the actual result set: 3, 1, 5 (index in list corresponds to the index in the sparse result set)
		QueryResultSet qrs;
		
		public SparseSet(List<Integer> lst, QueryResultSet rs)
		{
			colList = lst;
			qrs = rs;
		}
	}
	
	private boolean processExpressions(List<DisplayExpression> expressionList) throws BaseException
	{
		expDone.set(true);
		expIndex.set(0);
		
        for (DisplayExpression de : expressionList)
        {
        	if (de.getTransform() != null)
				try
				{
					de.getScript().setParentResultSet(this);
					((Transform)de.getTransform()).execute();
				} catch (ScriptException e)
				{
					logger.info("Error executing transformation - line " + e.getLine() + ": " + e.getMessage());
				}
        }

        boolean serialProcessing = true; // processSerially;
        boolean allowParallelExpressions = false; //setting to true will break GETPROMPTVALUE - it relies upon being able to get the prompt values from thread-specific data structures
        for (DisplayExpression de : expressionList)
        {
      	  	if (de.containsAggregateExpression)
      	  	{
      	  		serialProcessing = true;
      	  		break;
      	  	}
      	  	if (de.getCompiledOperator() == null || de.getParentFields() != null)
      	  	{
      	  		allowParallelExpressions = false;
      	  	}
      	  	if (allowParallelExpressions && de.measureExpressionColumns != null)
      	  	{
				/*
				 * Don't allow parallel execution with dimension expressions. This is because each row needs to rewrite
				 * the result set, which in parallel would create conflicts. The actual dimension expression itself will
				 * run parallel.
				 */
      	  		for(MeasureExpressionColumn mec: de.measureExpressionColumns)
      	  		{
      	  			if (mec.dimensionExpressionDisplayExpression != null)
      	  			{
      	  				allowParallelExpressions = false;
      	  				break;
      	  			}
      	  		}
      	  	}
        }
		
        if (serialProcessing)
        {
              for (DisplayExpression de : expressionList)
              {
				BaseException ex = processExpression(de, null, null);
				if (ex != null)
					throw ex;
              }
              return expDone.get();
        }

		// Spin threads to get the variable-specific of the result sets
		int numThreads = Math.min(Runtime.getRuntime().availableProcessors(), expressionList.size());
		ExecutorService es = null;
		/*
		 * We can try to add multithreading in the future, but need to setup multiple different operators that can run
		 * in each thread first
		 * Disabling multithreading within a given expression processing i.e. different threads processing subset of rows
		 * in a given resultset - as commented above, we need to make sure we setup different operators for different thread.
		 * DisplayExpression.clone() clones the compiled operator but this clone is still a shallow copy. So, if there are
		 * nested operators, multi-threading will still cause issues e.g. the following expression gives incorrect results
		 * randomly while processing in multi-threaded environment.
		 * [Time.Months Ago]-(DateDiff(Month,[Order_Details.OrderDate],NOW)) = -1 
		 * OR [Time.Months Ago]-(DateDiff(Month,[Order_Details.OrderDate],NOW)) = 0
		 */
		if (rows.length > 75 && allowParallelExpressions)
		{
			es = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
		}
		ExpressionThread[] tlist = new ExpressionThread[numThreads];
		for (int i = 0; i < numThreads; i++)
		{
			tlist[i] = new ExpressionThread(expressionList, es);
			tlist[i].start();
		}
		try
		{
			for (int i = 0; i < numThreads; i++)
			{
				tlist[i].join();
			}
		} catch (InterruptedException ex)
		{
			logger.error("InteruptedException: " + ex.getCause());
		}
		for (int i = 0; i < numThreads; i++)
		{
			if (tlist[i].getEx() != null)
				throw tlist[i].getEx();
		}
		return expDone.get();
	}
	AtomicInteger expIndex = new AtomicInteger(0);
	AtomicBoolean expDone = new AtomicBoolean(true);

	public class ExpressionThread extends Thread
	{
		private List<DisplayExpression> expressionList;
		private BaseException ex;
		private ExecutorService es;

		public ExpressionThread(List<DisplayExpression> expressionList, ExecutorService es)
		{
			this.setName("ExpressionThread - " + this.getId());
			setDaemon(true); // if the JVM wants to exit due to errors, let it
			this.expressionList = expressionList;
			this.es = es;
		}

		public void run()
		{
			while (true)
			{
				int curIndex = expIndex.getAndIncrement();
				if (curIndex >= expressionList.size())
					return;
				DisplayExpression de = expressionList.get(curIndex);
				ex = processExpression(de, es, null);
				if (ex != null)
					return;
			}
		}

		public BaseException getEx()
		{
			return ex;
		}
	}
	
	public BaseException processExpression(DisplayExpression de, ExecutorService es, Integer dimensionExpressionMask)
	{
		TransformationScript ts = de.getScript();
		BaseException be = null;
		if (ts != null)
			ts.setParentResultSet(this);
		if (de.getPosition() >= 0 && !de.dontProcess())
		{
			if (es == null)
				be = processRows(ts, de, 0, rows.length, dimensionExpressionMask);
			else
			{
				int numprocs = Runtime.getRuntime().availableProcessors();
				int lasti = 0;
				int rowinc = rows.length / numprocs;
				RunExpressionProcessing[] processors = new RunExpressionProcessing[numprocs];
				CountDownLatch cdl = new CountDownLatch(numprocs);
				for (int i = 0; i < numprocs; i++)
				{
					int stoprow = lasti + rowinc;
					if (i == numprocs - 1)
						stoprow = rows.length;
					processors[i] = new RunExpressionProcessing(ts, de, lasti, stoprow, cdl);
					lasti = stoprow;
				}
				for (int i = 0; i < numprocs; i++)
					es.execute(processors[i]);
				try
				{
					cdl.await();
					for (int i = 0; i < numprocs; i++)
					{
						if (processors[i].getException() != null)
						{
							if (be != null)
								be = processors[i].getException();
							logger.error(processors[i].getException());
						}
					}
				} catch (InterruptedException e)
				{
					logger.error(e);
				}
				// See if done
				if (ts != null && ts.isIncomplete())
				{
					expDone.set(false);
					ts.setIncomplete(false);
				}
			}
		}
		return be;
	}
	
	private class RunExpressionProcessing implements Runnable
	{
		private TransformationScript ts;
		private DisplayExpression de;
		private int start;
		private int stop;
		private BaseException exception;
		private CountDownLatch cdl;

		RunExpressionProcessing(TransformationScript ts, DisplayExpression de, int start, int stop, CountDownLatch cdl)
		{
			this.ts = ts;
			this.de = de;
			this.start = start;
			this.stop = stop;
			this.cdl = cdl;
		}

		@Override
		public void run()
		{
			de = (DisplayExpression) de.clone();
			exception = processRows(ts, de, start, stop, null);
			cdl.countDown();
		}

		public BaseException getException()
		{
			return exception;
		}
	}
	
	private boolean isNonRepeatableLog(Map<String,Integer> errorMap, String inputError, int limit)
	{
		if(inputError != null && inputError.trim().length() > 0)
		{
			if(errorMap != null)
			{
				if(errorMap.containsKey(inputError))
				{
					int count = errorMap.get(inputError);					
					if(count > limit)
					{	
						return false;
					}
					errorMap.put(inputError, ++count);
				}
				else
				{
					errorMap.put(inputError, 1);
				}
			}
			return true;
		}
		return false;
	}
	
	private BaseException processRows(TransformationScript ts, DisplayExpression de, int start, int stop, Integer dimensionExpressionMask)
	{
		Map<String, Integer> errorMap = new HashMap<String,Integer>();
		for (int i = start; i < stop; i++)
		{
			if (dimensionExpressionMask != null)
			{
				if (dimensionMask[i] != dimensionExpressionMask)
				{
					rows[i][de.getPosition()] = null;
					continue;
				}
			}
			if (ts != null && de.getParentFields() != null)
				ts.setParentRow(de.getParentFields().getRowIndex());
			else if (ts != null)
				ts.setParentRow(i);
			Object result = null;
			Object ce = de.getCompiledExprssion();
			de.getFields().setRowIndex(i);
			if (ce != null)
			{
				try
				{
					result = ((CompiledExpression) ce).evaluate(de.getContext());
				} catch (Throwable e)
				{
					return new SyntaxErrorException("Invalid Compiled Expression: " + de.getExpression(), e);
				}
			} else
			{
				Operator op = de.getCompiledOperator();
				if (op != null)
				{
					try
					{
						if (i == 0 && de.isRSum())
						{
							de.resetOperatorItems = true; // set resetOperatorItems only if evaluating first row for
															// RSUM operator (bug 11345 Support Nesting of RSUM)
						}
						result = op.evaluate();
						if (de.resetOperatorItems) // once first row is processed set de.resetOperatorItems to false if
													// it is true
						{
							de.resetOperatorItems = false;
						}
					} catch (Throwable e)
					{
						// XXX should really do this:
						// return new ScriptException("Error in expression: " + de.getExpression() + ": " +
						// e.getMessage(), -1, -1, -1);
						if (e instanceof NullPointerException)
						{
							// pretty serious, lets drop the stack trace here
							if(isNonRepeatableLog(errorMap, "NPE: " + de.getExpression(), 1))
							{
								logger.debug(e, e);
							}
						}
						
						String errorMsg = null;
						if (op.getType() == Type.Boolean)
						{
							errorMsg = e.toString() + ": " + de.getExpression() + " - assigning default value false";							
							result = Boolean.valueOf(false);
						} else if (op.getType() == Type.DateTime)
						{
							errorMsg = e.toString() + ": " + de.getExpression() + " - assigning default value current date";							
							result = new Date();
						} else if (op.getType() == Type.Float)
						{
							errorMsg = e.toString() + ": " + de.getExpression() + " - assigning default value NaN";							
							result = Double.NaN;
						} else if (op.getType() == Type.Integer)
						{
							errorMsg = e.toString() + ": " + de.getExpression() + " - assigning default value 0";							
							result = Long.valueOf(0);
						} else if (op.getType() == Type.Varchar)
						{
							errorMsg = e.toString() + ": " + de.getExpression() + " - assigning default value null";							
							result = null;
						}
						
						if(errorMsg != null && isNonRepeatableLog(errorMap, errorMsg, 2))
						{
							logger.error(errorMsg);
						}
					}
				} else
				{
					return new ScriptException("No expression (old or new) for expression: " + de.getExpression(), -1, -1, -1);
					// result = Double.valueOf(0);
				}
			}
			rows[i][de.getPosition()] = result;
			// See if done
			if (ts != null && ts.isIncomplete())
			{
				expDone.set(false);
				ts.setIncomplete(false);
			}
		}
		return null;
	}

	/*
	 * Remove the display names of added columns (so they are marked as not part of the output)
	 */
	public void removeDisplayNamesForAddedNames(List<String> addedNames)
	{
		for (int i = 0; i < displayNames.length; i++)
			if (addedNames.contains(displayNames[i]) && !tableNames[i].equals(EXPR_TABLE_NAME))
				displayNames[i] = null;
	}
	
	public void applyDisplayTop(int dtopn)
	{
		if(dtopn <= 0)
		{
			return;
		}
		if(rows != null && rows.length > dtopn)
		{
			List<Object[]> newRows = new ArrayList<Object[]>(dtopn);
			for(int i = 0; i < dtopn; i++)
			{
				newRows.add(rows[i]);
			}
			rows = new Object[newRows.size()][];
			newRows.toArray(rows);
		}
	}
	
	private void createGroup(DimensionColumn dc, Groups groups)
	{
		// Find column
		List<Integer> colIndices = new ArrayList<Integer>();
		List<Integer> otherdimensions = new ArrayList<Integer>();
		List<Integer> measures = new ArrayList<Integer>();
		for (int index = 0; index < q.dimensionColumns.size(); index++)
		{
			DimensionColumnNav dcn = q.dimensionColumns.get(index);
			if (dcn.colIndex < 0)
				continue;
			if (dcn.dimensionName.equals(dc.DimensionTable.DimensionName) && dcn.columnName.equals(dc.ColumnName))
				colIndices.add(dcn.colIndex);
			else
				otherdimensions.add(dcn.colIndex);
		}
		for (int index = 0; index < q.measureColumns.size(); index++)
		{
			MeasureColumnNav mcn = q.measureColumns.get(index);
			measures.add(mcn.colIndex);
		}
		if (colIndices.isEmpty())
			return;
		List<Object[]> newRows = new ArrayList<Object[]>();
		for (Object[] row : rows)
			newRows.add(row);
		TransformationScript ts = groups.getScript();
		for (Entry<String, Operator> op : groups.getOpList().entrySet())
		{
			Map<String, List<Integer>> rowsets = new HashMap<String, List<Integer>>();
			for (int i = 0; i < newRows.size(); i++)
			{
				StringBuilder sb = new StringBuilder();
				for (int j = 0; j < otherdimensions.size(); j++)
				{
					if (j > 0)
						sb.append('|');
					sb.append(newRows.get(i)[otherdimensions.get(j)]);
				}
				String key = sb.toString();
				List<Integer> rowset = rowsets.get(key);
				if (rowset == null)
				{
					rowset = new ArrayList<Integer>();
					rowsets.put(key, rowset);
				}
				rowset.add(i);
			}
			for (int mindex : measures)
			{
				for (List<Integer> rowset : rowsets.values())
				{
					for (DataElement de : ts.dataElements)
					{
						de.setValue(0);
					}
					for (int i = 0; i < rowset.size(); i++)
					{
						for (DataElement de : ts.dataElements)
						{
							String dename = de.getName();
							dename = dename.substring(1, dename.length() - 1);
							if (dename.equals(newRows.get(rowset.get(i))[colIndices.get(0)]))
							{
								de.setValue(newRows.get(rowset.get(i))[mindex]);
							}
						}
					}
					try
					{
						Object[] row = new Object[columnNames.length];
						for (int i = 0; i < row.length; i++)
							row[i] = newRows.get(rowset.get(0))[i];
						if (columnDataTypes[mindex] == Types.INTEGER)
							row[mindex] = ((Double) op.getValue().evaluate()).longValue();
						else if (columnDataTypes[mindex] == Types.DOUBLE)
							row[mindex] = op.getValue().evaluate();
						for (int i = 0; i < colIndices.size(); i++)
							row[colIndices.get(i)] = op.getKey();
						newRows.add(row);
					} catch (ScriptException e)
					{
					}
				}
			}
			rows = new Object[newRows.size()][];
			newRows.toArray(rows);
		}
	}

	/**
	 * Sorts the result set using a list of column names. Sorts precedence is done in reverse order (hence sorting is
	 * first done by the last column, then the next one, and so on)
	 * 
	 * @param orderList
	 *            list of column names to sort by
	 */
	@SuppressWarnings("rawtypes")
	public void processDisplayOrderings(List<DisplayOrder> orderList)
	{
		List<SortEntry> rowList = null;
		if (orderList != null && !orderList.isEmpty())
		{
			// Create a list of sort entries
			rowList = new ArrayList<SortEntry>();
			for (int i = 0; i < rows.length; i++)
			{
				SortEntry se = new SortEntry();
				se.row = rows[i];
				se.val = null;
				rowList.add(se);
			}
		} else
		{
			return;
		}
		// Now, go through and sort in reverse order
		for (int i = orderList.size() - 1; i >= 0; i--)
		{
			DisplayOrder dorder = orderList.get(i);
			int index = 0;
			for (; index < columnNames.length; index++)
			{
				String displayName = displayNames[index] == null ? null : Util.removeSystemGeneratedFieldNumber(displayNames[index]);
				String columnName = Util.removeSystemGeneratedFieldNumber(columnNames[index]);
				if ((displayName != null && displayName.equals(dorder.columnName))
						|| ((displayNames[index] != null && displayNames[index].equals(dorder.columnName)))
						|| ((columnName.equals(dorder.columnName) && ((dorder.tableName == null) || (dorder.tableName.equals(tableNames[index]))))))
					break;
			}
			if (index < columnNames.length)
			{
				for (int j = 0; j < rowList.size(); j++)
				{
					SortEntry se = rowList.get(j);
					se.val = (Comparable) se.row[index];
				}
				this.ascending = dorder.ascending;
				this.nulltop = dorder.nulltop;
				Collections.sort(rowList);
			}
		}
		// Copy resulting row list and update filterRowList if needed
		for (int j = 0; j < rowList.size(); j++)
		{
			SortEntry se = (SortEntry) rowList.get(j);
			rows[j] = se.row;
		}
	}

	private class SortEntry implements Comparable<SortEntry>
	{
		public Object[] row;
		@SuppressWarnings("rawtypes")
		public Comparable val;

		@SuppressWarnings("unchecked")
		public int compareTo(SortEntry se)
		{
			if (val == null)
			{
				if (se.val == null)
					return 0;
				else if (nulltop)
					return -1;
				else
					return 1;
			}
			if (se.val == null)
				if (nulltop)
					return 1;
				else
					return -1;
			if (ascending)
				return (val.compareTo(se.val));
			else
				return (se.val.compareTo(val));
		}
	}

	private class JoinEntry implements Comparable<JoinEntry>
	{
		public Object[] row;

		@SuppressWarnings("unchecked")
		public int compareTo(JoinEntry je)
		{
			if (row == null)
			{
				if (je.row == null)
					return 0;
				else if (nulltop)
					return -1;
				else
					return 1;
			}
			if (je.row == null)
				if (nulltop)
					return 1;
				else
					return -1;
			for (int i = 0; i < row.length; i++)
			{
				if (row[i] == null)
				{
					if (je.row[i] == null)
						continue;
					else if (nulltop)
						return -1;
					else
						return 1;
				}
				if (je.row[i] == null)
					if (nulltop)
						return 1;
					else
						return -1;
				@SuppressWarnings("rawtypes")
				int val = ((Comparable) row[i]).compareTo((Comparable) je.row[i]);
				if (val != 0)
					return val;
			}
			return 0;
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static class RankRow implements Comparable
	{
		Comparable value;
		int index;

		public int compareTo(Object o)
		{
			if (value == null && ((RankRow) o).value == null)
			{
				return 0;
			} else if (value == null)
			{
				return -1;
			} else if (((RankRow) o).value == null)
			{
				return 1;
			}
			return (value.compareTo(((RankRow) o).value));
		}
	}
	Set<Integer> aggregated = new HashSet<Integer>();

	/**
	 * Do an aggregation on a given column
	 * 
	 * @param index
	 * @param percentile
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void aggColumn(int index, boolean denserank, boolean percentile, List<String> groups)
	{
		if (aggregated.contains(index))
			return;
		aggregated.add(index);
		HashMap<String, List<RankRow>> map = new HashMap<String, List<RankRow>>();
		String[] keys = new String[rows.length];
		int[] keyColumns = null;
		int numGroups = 0;
		if (groups != null)
		{
			keyColumns = new int[groups.size()];
			for (String g : groups)
			{
				for (int i = 0; i < displayNames.length; i++)
				{
					if (groups.contains(displayNames[i])
							|| ((tableNames[i] == null && g.equals(columnNames[i]) || g.equals(tableNames[i] + "." + columnNames[i]))))
					{
						keyColumns[numGroups++] = i;
						break;
					}
				}
			}
		}
		for (int j = 0; j < rows.length; j++)
		{
			Object[] row = rows[j];
			StringBuilder key = new StringBuilder();
			for (int i = 0; i < numGroups; i++)
			{
				key.append('{');
				key.append(rows[j][keyColumns[i]]);
				key.append('}');
			}
			keys[j] = key.toString();
			List<RankRow> list = map.get(keys[j]);
			if (list == null)
			{
				list = new ArrayList<RankRow>();
				map.put(keys[j], list);
			}
			RankRow rr = new RankRow();
			rr.value = (Comparable) row[index];
			rr.index = j;
			list.add(rr);
		}
		// Sort each list
		for (List<RankRow> list : map.values())
			Collections.sort(list);
		long maxRank = 1;
		// Replace each entry with position in sorted list
		for (List<RankRow> list : map.values())
		{
			long rank = 0;
			Comparable previousVal = null;
			for (int j = list.size() - 1; j >= 0; j--)
			{
				RankRow rr = list.get(j);
				if (!denserank || (rr.value == null || previousVal == null || previousVal.compareTo(rr.value) != 0))
				{
					rank++;
				}
				rows[rr.index][index] = rank;
				previousVal = rr.value;
			}
			maxRank = rank;
		}
		columnDataTypes[index] = Types.INTEGER;
		// Calculate percentile if necessary
		if (percentile)
		{
			for (int j = 0; j < rows.length; j++)
			{
				Object[] row = rows[j];
				int rank = ((Long) row[index]).intValue();
				double ptile = ((double) rank) / ((double) maxRank);
				row[index] = Double.valueOf(1 - ptile);
			}
			columnDataTypes[index] = Types.DOUBLE;
			columnFormats[index] = "##%";
		} else
			columnFormats[index] = "#,###";
	}

	/**
	 * Process the necessary aggregations (RANK, DRANK, PTILE)
	 * 
	 * @param q
	 *            Current query
	 */
	private void processAggregations(Query q, List<String[]> colList)
	{
		for (Aggregation agg : q.getAggregations())
		{
			if (agg.getType() != AggregationType.None)
			{
				int index = 0;
				for (; index < colList.size(); index++)
				{
					if (colList.get(index)[1].equals(agg.getColumnName()))
					{
						break;
					}
				}
				if (index < colList.size())
				{
					aggColumn(index, agg.getType() == AggregationType.DenseRank, agg.getType() == AggregationType.Percentile, agg.getGroups());
					colList.get(index)[3] = (agg.getType() == AggregationType.DenseRank) ? "DRANK" : agg.getType() == AggregationType.Rank ? "RANK" : "PTILE";
				}
			}
		}
	}

	/**
	 * @return Rows data structure (one entry per row)
	 */
	public Object[][] getRows()
	{
		return (rows);
	}

	/**
	 * @return String array of column names
	 */
	public String[] getColumnNames()
	{
		return (columnNames);
	}

	/**
	 * @return String array of table names
	 */
	public String[] getTableNames()
	{
		return (tableNames);
	}

	/**
	 * @return String array of column formats
	 */
	public String[] getColumnFormats()
	{
		return (columnFormats);
	}

	/**
	 * @return Integer array of Warehouse column data types
	 */
	public String[] getWarehouseColumnsDataTypes()
	{
		return warehouseColumnsDataTypes;
	}

	/**
	 * @return Integer array of column data types
	 */
	public int[] getColumnDataTypes()
	{
		return (columnDataTypes);
	}

	/**
	 * @return Returns an integer string of column types
	 */
	public int[] getColumnTypes()
	{
		return (columnTypes);
	}

	/**
	 * @return Returns query
	 */
	public Query getQuery()
	{
		return (q);
	}

	/**
	 * @return Sets the result set's query
	 */
	public void setQuery(Query q)
	{
		this.q = q;
	}

	/**
	 * @return Returns number of measures in the query
	 */
	public int numMeasures()
	{
		return (numMeasures);
	}

	/**
	 * Bind filters to the data. This maps each filter predicate column to a result set column. It also marks whether
	 * any filters are applied to expressions (which would prevent pre-filtering).
	 * 
	 * @param displayFilters
	 * @throws SyntaxErrorException
	 */
	private void bindFiltersToData(List<DisplayFilter> displayFilters, Repository r) throws SyntaxErrorException
	{
		/*
		 * Need to synchronize on this instance of QueryResultSet. So, if two threads are referring to the same
		 * object of QueryResultSet, they don't step on each other. An example scenario is that when we have a 
		 * two lookup value with different display filters and lookup queries hit memory cache pointing to the
		 * same exact query resultset. 
		 */
		synchronized(this)
		{
			/*
			 * If these display filters have already been bound, don't bother This is necessary for various reasons - it is
			 * NOT an optimization When using display filters, the columns are bound once and then some of the display names
			 * are nulled out binding again will fail disastrously (SyntaxErrorException)
			 */
			if (displayFilters == null)
				return;
			if (boundFilters != null)
			{
				boolean isSame = false;
				if (boundFilters.size() == displayFilters.size())
				{
					isSame = true;
					for (int i = 0; i < boundFilters.size(); i++)
					{
						if (!boundFilters.get(i).equals(displayFilters.get(i)))
							isSame = false;
					}
				}
				if (isSame)
					return;
			}
			/*
			 * Go through and make sure the filter operands are the right types
			 */
			for (DisplayFilter df : displayFilters)
			{
				// First try to bind to display names, but if cannot, then try to bind to underlying column names
				if (!df.bindToData(tableNames, df.isDisplayName() ? displayNames : columnNames, columnDataTypes, r))
					if (!df.bindToData(tableNames, !df.isDisplayName() ? displayNames : columnNames, columnDataTypes, r))
						throw new SyntaxErrorException("Unable to apply display filter: " + df.toString());
			}
			boundFilters = new ArrayList<DisplayFilter>(displayFilters.size());
			for (DisplayFilter df : displayFilters)
			{
				boundFilters.add(df);
			}
		}
	}

	/**
	 * Return whether a row satisfies a set of display filters
	 * 
	 * @param row
	 * @param displayFilters
	 * @return
	 */
	private boolean isRowSatisfiedByFilters(Object[] row, List<DisplayFilter> displayFilters, Repository r)
	{
		boolean satisfied = true;
		for (DisplayFilter df : displayFilters)
		{
			if (!df.satisfiesFilter(row, r))
			{
				satisfied = false;
				break;
			}
		}
		return (satisfied);
	}

	/**
	 * @deprecated
	 * Return a filtered version of a given result set. Filter results such that all rows have colName=value
	 * 
	 * @param displayFilters
	 *            List of display filters to apply
	 * @return filtered results or null if unable to bind filters
	 * @throws SyntaxErrorException
	 */
	// this signature is only used by Fidelity iReports
	public QueryResultSet returnFilteredResultSet(List<DisplayFilter> displayFilters) throws SyntaxErrorException
	{
		return returnFilteredResultSet(displayFilters, null);
	}

	/**
	 * Return a filtered version of a given result set. Filter results such that all rows have colName=value
	 * 
	 * @param displayFilters
	 *            List of display filters to apply
	 * @param r
	 * 				Repository
	 * @return filtered results or null if unable to bind filters
	 * @throws SyntaxErrorException
	 */
	public QueryResultSet returnFilteredResultSet(List<DisplayFilter> displayFilters, Repository r) throws SyntaxErrorException
	{
		if (r == null) {
			Session s = Session.getCurrentThreadSession();
			r = s.getRepository();
		}
		QueryResultSet newQs = this.clone(false);
		bindFiltersToData(displayFilters, r);
		// Now copy only filtered rows
		List<Object[]> newRows = new ArrayList<Object[]>();
		/*
		 * If the filters have already been processed, then use the results
		 */
		for (int i = 0; i < rows.length; i++)
		{
			Object[] row = rows[i];
			if (isRowSatisfiedByFilters(row, displayFilters, r))
			{
				newRows.add(row);
			}
		}
		newQs.rows = (Object[][]) new Object[newRows.size()][];
		newRows.toArray(newQs.rows);
		newQs.curRow = -1;
		return (newQs);
	}

	/**
	 * Split a query result set into many smaller query result sets using a dimension column
	 * 
	 * @param session
	 * @param dimension
	 * @param column
	 * @return
	 * @throws NavigationException
	 * @throws ScriptException 
	 */
	public Map<String, QueryResultSet> returnSplitResults(Session session, String dimension, String column)
	throws BaseException, CloneNotSupportedException
	{
		/* Bind to the dimension column */
		int bound = 0;
		for (; bound < columnNames.length; bound++)
		{
			if (tableNames[bound].equals(dimension) && columnNames[bound].equals(column))
			{
				break;
			}
		}
		if (bound == columnNames.length)
		{
			throw new NavigationException("Dimension column not found in result set: " + dimension + "." + column);
		}
		Map<String, QueryResultSet> qrsSet = new HashMap<String, QueryResultSet>();
		Map<String, List<Object[]>> rowSet = new HashMap<String, List<Object[]>>();
		// Now copy rows
		for (int i = 0; i < rows.length; i++)
		{
			Object[] row = (Object[]) rows[i];
			String key = row[bound].toString();
			QueryResultSet newQs = qrsSet.get(key);
			if (newQs == null)
			{
				newQs = this.clone(false);
				Query oldq = newQs.getQuery();
				Query newq = (Query) oldq.clone();
				newq.addFilter(new QueryFilter(dimension, column, "=", key));
				newq.clearKey();
				newq.navigateQuery(true, true, null, true);
				newq.getKey(session);
				newQs.setQuery(newq);
				qrsSet.put(key, newQs);
				rowSet.put(key, new ArrayList<Object[]>());
			}
			List<Object[]> newRows = rowSet.get(key);
			newRows.add(row);
		}
		/* Assign rowsets */
		for (Map.Entry<String, QueryResultSet> entry : qrsSet.entrySet())
		{
			String key = entry.getKey();
			List<Object[]> rows = rowSet.get(key);
			if (rows != null)
			{
				Object[][] rowset = new Object[rows.size()][];
				rows.toArray(rowset);
				entry.getValue().rows = rowset;
				entry.getValue().curRow = -1;
			}
		}
		return (qrsSet);
	}

	/**
	 * Return an aggregated version of a given result set
	 * 
	 * @param qs
	 *            Query Result Set to use
	 * @param groupByClauses
	 *            List of group by clauses to apply
	 * @param q
	 *            Query object
	 * @param displayOrderList
	 *            Display order list - if there are OrderBys present in the query object, they are converted to DOs and
	 *            added to display order list
	 * @return
	 */
	private void aggregateResultSet(QueryResultSet qs, List<GroupBy> groupByClauses, Query q, List<DisplayOrder> displayOrderList)
	{
		/*
		 * Go through and bind the groupings to the appropriate columns
		 */
		for (GroupBy gb : groupByClauses)
		{
			gb.bindPredicate(tableNames, columnNames);
		}
		/*
		 * Create an ordered tree for processing groupings. Each key is the group;
		 */
		TreeMap<String, Object[]> newRows = new TreeMap<String, Object[]>();
		/*
		 * Figure out aggregation rules
		 */
		boolean[] sum = new boolean[aggregationRules.length];
		boolean[] min = new boolean[aggregationRules.length];
		boolean[] max = new boolean[aggregationRules.length];
		boolean[] first = new boolean[aggregationRules.length];
		boolean[] last = new boolean[aggregationRules.length];
		for (int i = 0; i < aggregationRules.length; i++)
		{
			if (aggregationRules[i] != null)
			{
				if (aggregationRules[i].equals("SUM") || aggregationRules[i].equals("COUNT"))
					sum[i] = true;
				else if (aggregationRules[i].equals("MIN"))
					min[i] = true;
				else if (aggregationRules[i].equals("MAX"))
					max[i] = true;
				else if (aggregationRules[i].equals("FIRST"))
					first[i] = true;
				else if (aggregationRules[i].equals("LAST"))
					last[i] = true;
			}
		}
		// Aggregate
		for (int i = 0; i < rows.length; i++)
		{
			Object[] row = rows[i];
			/*
			 * Generate grouping key
			 */
			StringBuilder sb = new StringBuilder();
			for (GroupBy gb : groupByClauses)
			{
				sb.append(gb.getBoundPredicate(row));
			}
			String key = sb.toString();
			Object[] newRow = newRows.get(key);
			/*
			 * If this is the first row with it's key, add to the sorted map
			 */
			if (newRow == null)
				newRows.put(key, row);
			else
			{
				/*
				 * Otherwise, aggregate the new row to the existing row (summing)
				 */
				for (int j = 0; j < row.length; j++)
				{
					if (row[j] == null)
						continue;
					if (columnTypes[j] == QueryColumn.MEASURE)
					{
						if (newRow[j] == null)
						{
							if (columnDataTypes[j] == Types.DOUBLE)
							{
								if (sum[j])
									newRow[j] = 0.0;
								else if (min[j])
									newRow[j] = Double.MAX_VALUE;
								else if (max[j])
									newRow[j] = -Double.MAX_VALUE;
							} else if (columnDataTypes[j] == Types.INTEGER)
							{
								if (sum[j])
									newRow[j] = 0l;
								else if (min[j])
									newRow[j] = Long.MAX_VALUE;
								else if (max[j])
									newRow[j] = -Long.MAX_VALUE;
							}
						}
						if (sum[j])
						{
							if (columnDataTypes[j] == Types.DOUBLE)
							{
								newRow[j] = ((Double) row[j]) + ((Double) newRow[j]);
							} else if (columnDataTypes[j] == Types.INTEGER)
							{
								newRow[j] = ((Long) row[j]) + ((Long) newRow[j]);
							}
						} else if (min[j])
						{
							if (columnDataTypes[j] == Types.DOUBLE)
								newRow[j] = Math.min(((Double) row[j]), ((Double) newRow[j]));
							else if (columnDataTypes[j] == Types.INTEGER)
								newRow[j] = Math.min(((Long) row[j]), ((Long) newRow[j]));
						} else if (max[j])
						{
							if (columnDataTypes[j] == Types.DOUBLE)
								newRow[j] = Math.max(((Double) row[j]), ((Double) newRow[j]));
							else if (columnDataTypes[j] == Types.INTEGER)
								newRow[j] = Math.max(((Long) row[j]), ((Long) newRow[j]));
						} else if (first[j])
						{
							if (newRow[j] == null)
								newRow[j] = row[j];
						} else if (last[j])
						{
							newRow[j] = row[j];
						}
					}
				}
			}
		}
		/*
		 * Now, copy ordered/aggregated set of rows
		 */
		qs.rows = (Object[][]) new Object[newRows.size()][];
		int curRow = 0;
		for (Iterator<Object[]> it = newRows.values().iterator(); it.hasNext();)
		{
			Object[] row = it.next();
			qs.rows[curRow++] = row;
		}
		qs.curRow = -1;
		/*
		 * The regrouping and reordering of qrs rows above would result in loss of ordering that was applied earlier by
		 * OrderBy clauses. So, we need to convert all those OrderBy clauses into display orders and add them to the
		 * display order list supplied. This way, while processing the display orders the orderings are applied again
		 * and end-user receives the correctly ordered resultset.
		 */
		q.convertAndAddOrderBysAsDisplayOrders(displayOrderList);
	}

	/**
	 * Return an aggregated version of a given result set
	 * 
	 * @param groupByClauses
	 *            List of group by clauses to apply
	 * @return
	 */
	public QueryResultSet returnAggregatedResultSet(List<GroupBy> groupByClauses, Query q, List<DisplayOrder> displayOrderList)
	{
		QueryResultSet newQs = this.clone(false);
		aggregateResultSet(newQs, groupByClauses, q, displayOrderList);
		return (newQs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.successmetricsinc.query.IJasperDataSourceProvider#getJasperDataSourceProvider()
	 */
	public JasperDataSourceProvider getJasperDataSourceProvider()
	{
		return jdsp;
	}

	/**
	 * @param jdsp
	 *            The jdsp to set.
	 */
	public void setJasperDataSourceProvider(JasperDataSourceProvider jdsp)
	{
		this.jdsp = jdsp;
	}

	/**
	 * Return an individual value within a result set
	 * 
	 * @param rownum
	 * @param colnum
	 * @return
	 */
	public Object getValue(int rownum, int colnum)
	{
		if (rownum >= rows.length)
			return (null);
		if (colnum >= rows[rownum].length)
			return (null);
		return rows[rownum][colnum];
	}

	/**
	 * Return an individual value within a result set
	 * 
	 * @param rownum
	 * @param colName
	 * @return
	 */
	public Object getValue(int rownum, String colName)
	{
		if (rownum >= rows.length)
			return (null);
		for (int i = 0; i < displayNames.length; i++)
		{
			if (displayNames[i].equals(colName))
				return (rows[rownum][i]);
		}
		return (null);
	}

	/**
	 * Return the column format for a given column name
	 * 
	 * @param rownum
	 * @param colName
	 * @return
	 */
	public String getFormat(String colName)
	{
		for (int i = 0; i < columnFormats.length; i++)
		{
			if (columnNames[i].equals(colName))
				return (columnFormats[i]);
		}
		return (null);
	}

	public int getType(String colName)
	{
		for (int i = 0; i < columnFormats.length; i++)
		{
			if (columnNames[i].equals(colName))
				return (columnDataTypes[i]);
		}
		return (Types.DOUBLE);
	}

	/**
	 * Return an individual value within a result set where an index column is equal to a value
	 * 
	 * @param indexColumn
	 *            Name of index column (column to search for index value)
	 * @param value
	 *            Index value to search for
	 * @param colName
	 *            Name of column to return value for
	 * @return
	 */
	public Object getValue(String indexColumn, String value, String colName)
	{
		int indexCol = -1;
		for (int i = 0; i < columnNames.length; i++)
		{
			if (columnNames[i].equals(indexColumn))
			{
				indexCol = i;
				break;
			}
		}
		if (indexCol != -1)
		{
			for (int i = 0; i < columnNames.length; i++)
			{
				if (columnNames[i].equals(colName))
				{
					for (int j = 0; j < rows.length; j++)
					{
						if (rows[j][indexCol].equals(value))
						{
							return (rows[j][i]);
						}
					}
					break;
				}
			}
		}
		return (null);
	}

	/**
	 * Return number of rows in result set
	 * 
	 * @return
	 */
	public int numRows()
	{
		return (rows.length);
	}

	/**
	 * Return a user-readable version of a filter
	 * 
	 * @param filterString
	 * @return
	 */
	public String filterDisplayString(String filterString) throws Exception
	{
		return (Util.filterDisplayString(jdsp.getRepository(), filterString));
	}

	public void print(PrintStream stream, char separator, Repository r, boolean format, List<Integer> columns, boolean printUTF8) throws IOException
	{
		PrintStream ustream = null;
		if (printUTF8)
		{
			ustream = new PrintStream(stream, true, "UTF-8");
		}
		if (rows == null || rows.length == 0)
		{
			return;
		}
		StringBuilder sb = new StringBuilder();
		// determine the column to export and the correct order
		if (columns == null)
		{
			columns = new ArrayList<Integer>();
			for (int i = 0; i < displayNames.length; i++)
			{
				if (displayNames[i] != null)
					columns.add(Integer.valueOf(i));
			}
		}
		boolean first = true;
		for (int index = 0; index < columns.size(); index++)
		{
			int i = columns.get(index);
			if (!first)
				sb.append(separator);
			first = false;
			String name = displayNames[i];
			if (name != null)
			{
				String prefix = "F" + index + "_";
				if (name.startsWith(prefix))
				{
					name = name.substring(prefix.length());
				}
				sb.append(name);
			}
		}
		if (printUTF8)
		{
			Console.println(ustream, sb.toString());
		}
		else
		{
			stream.println(sb.toString());
		}
		DecimalFormat[] formats = new DecimalFormat[columnFormats.length];
		if (format)
		{
			for (int index = 0; index < columns.size(); index++)
			{
				int i = columns.get(index);
				try
				{	
					String pattern = QueryResultSet.getFormatPattern(columnFormats[i], columnDataTypes[i]);
					if (pattern != null)
						formats[i] = new DecimalFormat(pattern);
					else
						formats[i] = new DecimalFormat();					
				}
				catch (IllegalArgumentException ex)
				{
					logger.warn("Not able to parse into decimal format: " + columnFormats[i]);
				}
			}
		}
		int num = numRows();
		for (int j = 0; j < num; j++)
		{
			StringBuilder sb2 = new StringBuilder();
			Object[] row = rows[j];
			boolean first2 = true;
			for (int index = 0; index < columns.size(); index++)
			{
				int i = columns.get(index);
				if (displayNames[i] != null)
				{
					if (!first2)
						sb2.append(separator);
					if ((row[i] != null) && (formats[i] != null) && Number.class.isInstance(row[i]))
					{
						String columnValue = formats[i].format(row[i]).replaceAll("^-(?=0(.0*)?$)", "");
						sb2.append(Util.quote(columnValue, separator));
					} else if (row[i] != null)
					{
						if (warehouseColumnsDataTypes[i] != null
								&& (warehouseColumnsDataTypes[i].equals("Date") || warehouseColumnsDataTypes[i].equals("DateTime")))
						{
							Date dt = (row[i] instanceof Calendar ? ((Calendar) row[i]).getTime() : (Date) row[i]);
							SimpleDateFormat sdf;
							if (warehouseColumnsDataTypes[i].equals("Date"))
							{
								sdf = DateUtil.getShortDateFormat(null);
							} else
							{
								sdf = DateUtil.getShortDateTimeFormat(null);
							}
							if (r != null)
							{
								sdf.setTimeZone(r.getServerParameters().getDisplayTimeZone());
							}
							sb2.append(Util.quote(sdf.format(dt.getTime()), separator));
						} else
							sb2.append(Util.quote(row[i].toString(), separator));
					}
					first2 = false;
				}
			}
			if (printUTF8)
			{
				Console.println(ustream, sb2.toString());
			}
			else
			{
				stream.println(sb2.toString());
			}
		}
	}

	public static String getFormatPattern(String format, int type)
	{
		if (format != null)
			return format;
		else if (type == Types.INTEGER)
			return "#,###";
		else if (type == Types.DOUBLE)
			return "#,###.##";
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		return toString('\t');
	}

	public String toString(char separator)
	{
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < displayNames.length; i++)
		{
			if (displayNames[i] != null)
				sb.append(displayNames[i] + separator);
		}
		sb.append("\r\n");
		DecimalFormat[] formats = new DecimalFormat[columnFormats.length];
		// make life easier
		if (separator != ',')
		{
			for (int i = 0; i < columnFormats.length; i++)
			{
				try
				{
					formats[i] = new DecimalFormat(QueryResultSet.getFormatPattern(columnFormats[i], columnDataTypes[i]));
				}
				catch (IllegalArgumentException ex)
				{
					logger.warn("Not able to parse into decimal format: " + columnFormats[i]);
				}
			}
		}
		for (Object[] row : rows)
		{
			boolean first = true;
			for (int i = 0; i < row.length; i++)
			{
				if (displayNames[i] != null)
				{
					if (!first)
						sb.append(separator);
					if (row[i] != null && formats[i] != null && Number.class.isInstance(row[i]))
					{
						String columnValue = formats[i].format(row[i]).replaceAll("^-(?=0(.0*)?$)", "");
						sb.append(Util.quote(columnValue, separator));
					} else if (row[i] != null)
					{
						sb.append(Util.quote(row[i].toString(), separator));
					}
					first = false;
				}
			}
			if (!first)
				sb.append("\r\n");
		}
		return (sb.toString());
	}

	/**
	 * Set a display name
	 * 
	 * @param index
	 * @param name
	 */
	public void setDisplayName(int index, String name)
	{
		displayNames[index] = name;
	}

	/**
	 * @return Returns the displayNames.
	 */
	public String[] getDisplayNames()
	{
		return displayNames;
	}

	/**
	 * @return Returns the valid.
	 */
	public boolean isValid()
	{
		return valid;
	}

	public void setValid(boolean valid)
	{
		this.valid = valid;
	}

	/**
	 * @return Returns the aggregationRules.
	 */
	public String[] getAggregationRules()
	{
		return aggregationRules;
	}
	
	public RetrievedFrom whereFrom()
	{
		return whereFrom;
	}
	
	public void retrievedFrom(RetrievedFrom from)
	{
		whereFrom = from;
	}

	public void writeExternal(ObjectOutput oo) throws IOException
	{
		oo.writeObject(tableNames);
		oo.writeObject(columnNames);
		oo.writeObject(columnFormats);
		oo.writeObject(displayNames);
		oo.writeObject(maskable);
		oo.writeObject(aggregationRules);
		oo.writeObject(columnDataTypes);
		oo.writeObject(columnTypes);
		oo.writeObject(aggregations);
		oo.writeObject(measureFilters);
		oo.writeObject(numMeasures);
		oo.writeObject(q);
		oo.writeInt(rows.length);
		for (int i = 0; i < rows.length; i++)
		{
			new OurRowObject(rows[i]).writeExternal(oo);
		}
		oo.writeObject(mapped);
		oo.writeObject(columnMaps);
		oo.writeObject(warehouseColumnsDataTypes);
		oo.writeObject(expires);		
	}

	@SuppressWarnings("unchecked")
	public void readExternal(ObjectInput oi) throws IOException, ClassNotFoundException
	{
		tableNames = (String[]) oi.readObject();
		columnNames = (String[]) oi.readObject();
		columnFormats = (String[]) oi.readObject();
		displayNames = (String[]) oi.readObject();
		maskable = (int[]) oi.readObject();
		aggregationRules = (String[]) oi.readObject();
		columnDataTypes = (int[]) oi.readObject();
		columnTypes = (int[]) oi.readObject();
		aggregations = (AggregationType[]) oi.readObject();
		measureFilters = (QueryFilter[]) oi.readObject();
		numMeasures = (Integer) oi.readObject();
		q = (Query) oi.readObject();
		int len = oi.readInt();
		OurRowObject rw = new OurRowObject();
		rows = new Object[len][];
		for (int i = 0; i < len; i++)
		{
			rw.readExternal(oi);
			rows[i] = rw.getColumns();
		}
		mapped = (boolean[]) oi.readObject();
		columnMaps = (Map<Object, Object>[]) oi.readObject();
		expires = Long.MAX_VALUE;
		try
		{
			warehouseColumnsDataTypes = (String[]) oi.readObject();
			expires = (long) oi.readObject();
		} catch (OptionalDataException e)
		{
			if (!e.eof)
				throw e;
		}		
		valid = true;
	}

	/**
	 * Create a new data source using this result set (which implements JRDataSource)
	 * 
	 * @param query
	 * @return
	 */
	public JRDataSource create(String query)
	{
		if (jdsp == null)
			return (null);
		try
		{
			return (jdsp.create(query));
		} catch (Exception ex)
		{
			if (QueryCancelledException.class.isInstance(ex.getCause()) || QueryCancelledException.class.isInstance(ex))
			{
				logger.info("Query cancelled by end user (QueryResultSet::create)");
			} else if (QueryTimeoutException.class.isInstance(ex.getCause()) || QueryTimeoutException.class.isInstance(ex))
			{
				logger.info("Query timed out (QueryResultSet::create)");
			} else
			{
				logger.fatal(ex.toString(), ex);
			}
			return (null);
		}
	}

	/**
	 * Replace any mapped columns using a mapping result set
	 * 
	 * @param resultSetMap
	 */
	public void replaceMappedColumns(QueryResultSet resultSetMap)
	{
		for (int i = 0; i < resultSetMap.columnNames.length; i++)
		{
			if (resultSetMap.mapped[i])
			{
				// Replace all mapped columns
				int index = 0;
				do
				{
					for (; index < columnNames.length; index++)
					{
						if (resultSetMap.columnNames[i].equals(columnNames[index]) && resultSetMap.tableNames[i].equals(tableNames[index]))
						{
							break;
						}
					}
					if (index < columnNames.length)
					{
						/*
						 * Make sure this column was indexed as volatile (otherwise, one might be attempting to replace
						 * a key column that was intended as a key
						 */
						DimensionColumnNav vdcn = null;
						for (DimensionColumnNav dcn : q.dimensionColumns)
						{
							if (dcn.colIndex == index)
							{
								if (dcn.volatileColumn)
									vdcn = dcn;
								break;
							}
						}
						if (vdcn != null)
						{
							for (int j = 0; j < rows.length; j++)
							{
								Object o = rows[j][index];
								if (o == null && vdcn.defaultMappedValue != null)
									rows[j][index] = vdcn.defaultMappedValue;
								else
								{
									o = resultSetMap.columnMaps[i].get(o);
									if (o == null && vdcn.defaultMappedValue != null)
										rows[j][index] = vdcn.defaultMappedValue;
									else
										rows[j][index] = o;
								}
							}
							columnFormats[index] = resultSetMap.columnFormats[i + 1];
							columnDataTypes[index] = resultSetMap.columnDataTypes[i + 1];
							columnTypes[index] = resultSetMap.columnTypes[i + 1];
						}
					}
					index++;
				} while (index < columnNames.length);
			}
		}
	}

	public void processMasks()
	{
		// first, look for any maskable columns - if none, return
		boolean needToMask = false;
		for (int i = 0; i < maskable.length; i++)
		{
			if (maskable[i] > 0)
			{
				needToMask = true;
				break;
			}
		}
		if (!needToMask)
			return;
		whereFrom = RetrievedFrom.memory;
		Object[][] newRows = new Object[rows.length][];
		for (int i = 0; i < rows.length; i++)
		{
			newRows[i] = new Object[maskable.length];
			for (int j = 0; j < maskable.length; j++)
			{
				if (maskable[j] > 0)
				{
					newRows[i][j] = Util.maskField(rows[i][j], columnDataTypes[j], maskable[j]);
				} else
				{
					newRows[i][j] = rows[i][j];
				}
			}
		}
		rows = newRows;
	}

	public QueryResultSet getDateTimeFormattedQRS(Map<String, Format> formatMap, List<String> colNames)
	{
		QueryResultSet qrs = this.clone(true);
		for (int i = 0; i < qrs.rows.length; i++)
		{
			for (int j = 0; j < qrs.rows[i].length; j++)
			{
				if (qrs.rows[i][j] instanceof Calendar)
				{
					SimpleDateFormat sdf = null;
					UserBean ub = UserBean.getParentThreadUserBean();
					SimpleDateFormat fmt = null;
					if (colNames.size() > j && colNames.get(j) != null && formatMap != null && formatMap.containsKey(colNames.get(j)))
						fmt = (SimpleDateFormat) formatMap.get(colNames.get(j));
					if (warehouseColumnsDataTypes != null && warehouseColumnsDataTypes[j] != null && warehouseColumnsDataTypes[j].equals("Date"))
						if (ub != null)
							if (fmt == null)
								sdf = ub.getDateFormat();
							else
								sdf = ub.getDateFormat(fmt.toPattern());
						else
							sdf = DateUtil.getShortDateFormat(null);
					else if (ub != null)
						if (fmt == null)
							sdf = ub.getDateTimeFormat();
						else
							sdf = ub.getDateTimeFormat(fmt.toPattern());
					else
						sdf = DateUtil.getShortDateTimeFormat(null);
					qrs.rows[i][j] = sdf.format(((Calendar) qrs.rows[i][j]).getTime());
				}
			}
		}
		return qrs;
	}

	public String getBaseFilename()
	{
		return baseFilename;
	}

	public void setBaseFilename(String baseFilename)
	{
		this.baseFilename = baseFilename;
	}

	@Override
	public void moveFirst() throws JRException
	{
		this.curRow = 0;
		rewindCount++;
		if (rewindCount > 10)
		{
			throw new JRException(new JRBandOverflowException("Either the summary section or the header section data does not fit on a page (rewindCount)."));
		}
	}

	public void setColumnNames(String[] columnNames)
	{
		this.columnNames = columnNames;
	}

	public void setDisplayNames(String[] displayNames)
	{
		this.displayNames = displayNames;
	}

	public void setAggregationRules(String[] aggregationRules)
	{
		this.aggregationRules = aggregationRules;
	}

	public void setColumnTypes(int[] columnTypes)
	{
		this.columnTypes = columnTypes;
	}

	public void setColumnDataTypes(int[] columnDataTypes)
	{
		this.columnDataTypes = columnDataTypes;
	}

	public void setRows(Object[][] rows)
	{
		this.rows = rows;
	}

	public void setColumnFormats(String[] columnFormats)
	{
		this.columnFormats = columnFormats;
	}

	public Object[][] convertNullsToZeros() {
		// fix up nulls in float, double, and integer columns, map to 0
		Object[][] newRows = new Object[rows.length][columnDataTypes.length];
		for (int i = 0; i < rows.length; i++)
		{
			for (int j = 0; j < columnDataTypes.length; j++)
			{
				if (rows[i][j] == null)
				{
					if (columnDataTypes[j] == Types.INTEGER)
					{
						newRows[i][j] = Long.valueOf(0);
					} else if (columnDataTypes[j] == Types.DOUBLE)
					{
						newRows[i][j] = Double.valueOf(0);
					} else if (columnDataTypes[j] == Types.FLOAT)
					{
						newRows[i][j] = Float.valueOf(0);
					} else
					{
						newRows[i][j] = rows[i][j];
					}
				} else
				{
					if (columnDataTypes[j] == Types.DOUBLE && (Double.isNaN((Double) rows[i][j]) || Double.isInfinite((Double) rows[i][j])))
					{
						newRows[i][j] = Double.valueOf(0);
					} else
					{
						newRows[i][j] = rows[i][j];
					}
				}
			}
		}
		return newRows;
	}

	public int getCurRow()
	{
		return curRow;
	}
	
	public static void setProcessSerially(boolean processSerially) {
		QueryResultSet.processSerially = processSerially;
	}
	
	private int getGenericSqlTypeForDataType(int ctype, QueryColumn qc)
	{
		int returnType;
		switch (ctype)
		{
		case Types.BIGINT:
		case Types.INTEGER:
		case Types.TINYINT:
		case Types.SMALLINT:
		case -7: // Tinyint in MYSQL
			returnType = Types.INTEGER;
			break;
		case Types.DOUBLE:
		case Types.FLOAT:
		case Types.DECIMAL:
		case Types.NUMERIC:
		case Types.REAL:
			returnType = Types.DOUBLE;
			break;
		case -8:
		case Types.CHAR:
		case Types.NCHAR:
		case Types.NVARCHAR:
		case Types.VARCHAR:
		case Types.LONGVARCHAR:
		case Types.LONGNVARCHAR:
			returnType = Types.VARCHAR;
			break;
		case Types.BOOLEAN:
			returnType = Types.BOOLEAN; // XXX whats up here, we don't process BOOLEAN below...
			break;
		case Types.DATE:
		case Types.TIME:
		case Types.TIMESTAMP:
			returnType = Types.TIMESTAMP;
			break;
		case Types.VARBINARY:
			// The result of a null date on MySQL/Infobright
			returnType = Types.TIMESTAMP;
			break;
		case 0:
			// In the case of XMLA with no data, may not be able to determine type
			if (qc.type == QueryColumn.MEASURE)
				returnType = Types.DOUBLE;
			else if (qc.type == QueryColumn.DIMENSION)
				returnType = getTypeForDataTypeString(((DimensionColumn)qc.o).DataType);
			else
				returnType = Types.VARCHAR;
			break;
		default:
			logger.warn("query returned unknown data type (might be [N]VARCHAR(MAX): " + ctype);
			// guessing...
			if (qc.type == QueryColumn.MEASURE)
				returnType = Types.DOUBLE;
			else
				returnType = Types.VARCHAR;
			break;
		}
		return returnType;
	}
	
	private Object getObjectForDataType(int dataType, int colIndex, DatabaseConnectionResultSet dcrs, boolean mapNullToZero,
			Calendar calProcTZ, Calendar calData, boolean isDbTypeIB) throws SQLException
	{
		Object ret = null;
		switch (dataType)
		{
		case Types.INTEGER:
			ret = Long.valueOf(dcrs.getLong(colIndex + 1));
			if (dcrs.wasNull())
				if (mapNullToZero)
					ret = Long.valueOf(0);
				else
					ret = null;
			break;
		case Types.DOUBLE:
			ret = Double.valueOf(dcrs.getDouble(colIndex + 1));
			if (dcrs.wasNull())
				if (mapNullToZero)
					ret = Double.valueOf(0);
				else
					ret = null;
			break;
		case Types.VARCHAR:
			ret = dcrs.getString(colIndex + 1);
			break;
		case Types.TIMESTAMP:
			try
			{
				Timestamp ts = dcrs.getTimestamp(colIndex + 1, calProcTZ, isDbTypeIB);
				if (ts != null)
				{
					calData.setTimeInMillis(ts.getTime());
					ret = (Calendar) calData.clone();
				} else
				{
					ret = null;
				}
			}
			catch (Exception e)
			{
				ret = null; // rs.getTimestamp for a null value can sometimes return an error
			}
			break;
		}
		return ret;
	}
	
	/**
	 * Create a mask for procession dimension expressions. Need to figure out which dimension columns are not in the
	 * dimension expression and make sure we apply the expression only for rows where those columns are the same. This
	 * allows us to drill down.
	 * 
	 * @param dimensionExpressionColumns
	 */
	public void generateDimensionMask(List<DimensionExpressionColumn> dimensionExpressionColumns, Query parentQuery)
	{
		List<Integer> colIndices = new ArrayList<Integer>();
		dimensionMaskDimensionNames = new ArrayList<String>();
		dimensionMaskColumnNames = new ArrayList<String>();
		for (int i = 0; i < columnNames.length - 1; i++)
		{
			if (columnNames[i].equals("{DIMENSIONEXPRESSION}"))
				continue;
			if (columnTypes[i] == QueryColumn.DIMENSION)
			{
				boolean found = false;
				for (DimensionExpressionColumn dec : dimensionExpressionColumns)
				{
					if (dec.dimName.equals(tableNames[i]))
					{
						found = true;
						break;
					}
				}
				if (found)
					continue;
				dimensionMaskDimensionNames.add(tableNames[i]);
				dimensionMaskColumnNames.add(columnNames[i]);
				colIndices.add(i);
			}
		}
		dimensionMask = new int[rows.length];
		dimensionMaskMap = new HashMap<String, Integer>();
		for (int i = 0; i < rows.length; i++)
		{
			StringBuilder sb = new StringBuilder();
			boolean first = true;
			for (Integer index : colIndices)
			{
				if (first)
					first = false;
				else
					sb.append('|');
				if (rows[i][index] != null)
					sb.append(rows[i][index].toString());
			}
			String key = sb.toString();
			Integer val = dimensionMaskMap.get(key);
			if (val == null)
			{
				val = dimensionMaskMap.size();
				dimensionMaskMap.put(key, val);
			}
			dimensionMask[i] = val;
		}
		/*
		 * Find unreferenced parent query 
		 */
		parentIndices = new ArrayList<Integer>();
		for(int i = 0; i < parentQuery.dimensionColumns.size(); i++)
		{
			DimensionColumnNav dcn = parentQuery.dimensionColumns.get(i);
			if (dcn.colIndex < 0)
				continue;
			for (int j = 0; j < dimensionMaskDimensionNames.size(); j++)
			{
				if (dimensionMaskDimensionNames.get(j).equals(dcn.dimensionName) && dimensionMaskColumnNames.get(j).equals(dcn.columnName))
				{
					parentIndices.add(dcn.colIndex);
					break;
				}
			}
		}
	}

	public Map<String, Integer> getDimensionMaskMap()
	{
		return dimensionMaskMap;
	}

	public List<String> getDimensionMaskDimensionNames()
	{
		return dimensionMaskDimensionNames;
	}

	public List<String> getDimensionMaskColumnNames()
	{
		return dimensionMaskColumnNames;
	}

	public List<Integer> getParentIndices()
	{
		return parentIndices;
	}
}
