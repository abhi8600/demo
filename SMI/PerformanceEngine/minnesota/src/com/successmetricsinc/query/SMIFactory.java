/**
 * $Id: SMIFactory.java,v 1.4 2011-09-24 01:05:37 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.util.Map;
import com.successmetricsinc.query.QueryExecuter;
import net.sf.jasperreports.engine.JRDataset;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.query.JRQueryExecuter;
import net.sf.jasperreports.engine.query.JRQueryExecuterFactory;

public class SMIFactory implements JRQueryExecuterFactory
{
	public Object[] getBuiltinParameters()
	{
		return null;
	}

	public JRQueryExecuter createQueryExecuter(JRDataset ds, @SuppressWarnings("rawtypes") Map parameterMap) throws JRException
	{
		QueryExecuter qe = new QueryExecuter(ds.getQuery().getText());
		return qe;
	}

	public boolean supportsQueryParameterType(String arg)
	{
		if (arg.equals("java.lang.String"))
			return true;
		else if (arg.equals("java.lang.Integer"))
			return true;
		else if (arg.equals("java.lang.Double"))
			return true;
		return false;
	}
}
