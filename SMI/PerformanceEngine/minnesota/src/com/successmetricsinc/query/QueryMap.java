/**
 * $Id: QueryMap.java,v 1.30 2011-11-23 12:28:41 rchandarana Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import com.successmetricsinc.util.Util;

/**
 * Class to allow for renaming of columns and table names (allows for aliasing and re-use)
 * 
 * @author bpeters
 * 
 */
public class QueryMap
{
	private Map<Object, String> map;
	private static char terminatorCharacter = '_';
	private static final char beginCharacter = 'x';
	private transient Database.DatabaseType dbType=Database.DatabaseType.None;

	public QueryMap()
	{
		this.map = new HashMap<Object, String>(20);
	}

	/**
	 * Get mapped name for a table
	 * 
	 * @param o -
	 *            object to map, toString() for object is used for naming
	 * @return
	 */
	public String getMap(Object o)
	{
		String result = map.get(o); 
		if (result == null)
		{
			result = o.toString();
			char c = result.charAt(0);
			if (c >= '0' && c <= '9')
			{
				result = 'N' + result;
			}
			result = result.replaceAll("[^\\w\u0080-\uFFCF]", "_");	// Replace all non-word characters for ASCII.
			if (!Util.containsNonAsciiCharacters(result))
			{
				result = result.replaceAll("[\\W']", "_");	
			}
			else
			{
				result = Util.getAsciiString(result, true);
			}

			result = result + map.size();
			if (result.length() > DatabaseConnection.getIdentifierLength(dbType) - 1)
			{
				result = Database.shrinkBaseName(result, DatabaseConnection.getIdentifierLength(dbType) - 1).replaceAll("[\\+=/]", "_");
				c = result.charAt(0);
				if ((c >= '0' && c <= '9') || c == '_')
				{
					result = 'N' + result.substring(1);
				}
			}
			result = result + terminatorCharacter;
			if ((o instanceof QueryColumn) && (((QueryColumn) o).type == QueryColumn.CONSTANT_OR_FORMULA) && !result.startsWith("CONST_"))
				result = "CONST_" + result;		
			if (result.startsWith("_") && !DatabaseConnection.supportsBeginUnderScore(dbType))
				result=beginCharacter+result;
			map.put(o, result);
		}
		
		return (result);
	}

	/**
	 * Set mapped name for a table
	 * 
	 * @param table
	 * @param mappedName
	 * @return
	 */
	public void setMap(Object o, String mappedName)
	{
		map.put(o, mappedName);
	}

	/**
	 * Replace all mapped names with their original keys in a string
	 * 
	 * @param str
	 *            String to replace
	 * @param usePhysicalNames
	 *            Whether to use physical version of names (so that a query can be wrapped)
	 * @return
	 */
	public String replaceColumnNames(DatabaseConnection dbc, String str, boolean usePhysicalNames, String colAliasStartStr, boolean noQuote)
	{
		Set<Entry<Object, String>> s = map.entrySet();
		Iterator<Entry<Object, String>> it = s.iterator();
		while (it.hasNext())
		{
			Map.Entry<Object, String> me = it.next();
			if (me.getKey().getClass() == QueryColumn.class)
			{
				QueryColumn qc = (QueryColumn) me.getKey();
				String r = colAliasStartStr + dbc.getColumnName((String) me.getValue(), noQuote);
				String cname = usePhysicalNames ? Util.replaceWithPhysicalString(qc.name) : qc.name;
				if (dbc != null && dbc.requiresHashedIdentifierForColumnAlias())
					cname = dbc.getHashedIdentifier(cname);
				String v = colAliasStartStr + dbc.getColumnName(cname, noQuote);
				str = Util.replaceStr(str, r, v);
			} else if (me.getKey().getClass() == String.class)
			{
				String r = dbc.getColumnName((String) me.getValue(), noQuote);
				String cname = usePhysicalNames ? Util.replaceWithPhysicalString(me.getKey().toString()) : me.getKey().toString();
				if (dbc != null && dbc.requiresHashedIdentifierForColumnAlias())
					cname = dbc.getHashedIdentifier(cname);
				String v = dbc.getColumnName(cname, noQuote);
				str = Util.replaceStr(str, r, v);
			}
		}
		return (str);
	}

	/**
	 * @return Returns the terminatorCharacter.
	 */
	public static char getTerminatorCharacter()
	{
		return terminatorCharacter;
	}

	/**
	 * @param terminatorCharacter
	 *            The terminatorCharacter to set.
	 */
	public static void setTerminatorCharacter(char terminatorCharacter)
	{
		QueryMap.terminatorCharacter = terminatorCharacter;
	}
	
	public void setDBType(Database.DatabaseType dbType){
		this.dbType=dbType;
	}
	
	public Database.DatabaseType getDBType(){
			return this.dbType;
	}
}
