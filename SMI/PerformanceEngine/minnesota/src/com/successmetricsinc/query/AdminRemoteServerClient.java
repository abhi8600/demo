package com.successmetricsinc.query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.log4j.Logger;



public class AdminRemoteServerClient {

	private static final Logger logger = Logger.getLogger(AdminRemoteServerClient.class);
	
	private static final String ADMIN_SERVICES_NAMESPACE = "http://admin.WebServices.successmetricsinc.com";
	private ServiceClient scLogin;
	private ServiceClient scAdmin;
	private ServiceClient scLogout;
	
	private String sessionId;
	
	private String serverURL;
	private String username;
	
	
	
	public AdminRemoteServerClient(WebServerConnectInfo serverConnectInfo){
		this.username = serverConnectInfo.serverUsername;
		this.serverURL = serverConnectInfo.serverURL;		
	}
	
	public String execute(String serviceName, Map<String,String> parameterMap) {
		
		String result = ""; 
		String cmdParamter = parameterMap.get("cmdParameter");	
	
		try {
			sessionId = loginAndCreateSession();
			setAdminServiceClient();
			

			if (serviceName.equals("ClearCache")) {
				result = clearAllCaches(false);
			} else if (serviceName.equals("ClearCacheForQuery")) {
				result = clearCacheForQuery(parameterMap, false);
			} else if (serviceName.equals("ClearCacheForLogicalTable")) {
				result = clearCacheForLogicalTable(cmdParamter, false);
			} else if (serviceName.equals("ClearCacheForPhysicalTable")) {
				result = clearCacheForPhysicalTable(cmdParamter, false);//        	
			}
			
			logout();
		} catch (Exception e) {
			logger.error("Exception occured during AdminRemoteServerClient execute " +
					"for server : " + serverURL + "username : " + username +  " -" + e.getMessage(), e);
		}
		return result;
	}
	
	private String loginAndCreateSession() throws Exception {
		
		//String token = null;
		String sessionId = null;
		try {
			//token = getToken(username, serverURL);
			logger.debug("token : username=" + username);
			setLoginServiceClient();
			setLogoutServiceClient();
			sessionId =login(username);
		} catch (Exception e) {
			String msg = "Error during loginAndCreate Session " + e.getMessage();
			logger.error(msg);
			throw new Exception(e);
		}
		return sessionId; 
		
	}
	
	/*//no need for token since we will be using SSOHeader
	private String getToken(String username, String serverUrl) throws Exception {

		URL u = null;
		HttpURLConnection conn = null;
		String token = null;
		try {
			logger.debug(tokenServlet);
			tokenServlet = tokenServlet.replace("UN", username);
			
			logger.debug(tokenServlet);
			String url = serverUrl + tokenServlet;
			u = new URL(url);
			conn = (HttpURLConnection) u.openConnection();			
			
			BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			//token = URLDecoder.decode(rd.readLine(), "UTF-8");	
			token = rd.readLine();
			rd.close();
		} catch (MalformedURLException e) {	
			String msg = "Failed - Client Token Generation Call MalformedURL: " + e.getMessage();
			logger.error( msg);
			throw new Exception(e);
		} catch (IOException e) {
			String msg = "Failed - Client Token Generation Call IOException: " + e.getMessage();
			logger.error( msg);
			throw new Exception(e);						
		} 
		
		if(token == null){			
			String msg = "Token Generation failed for username=" + username + " serverUrl=" + serverURL;
			logger.error(msg);
			throw new Exception(msg);
		}
		return token;
	}	
	*/

	private void init(ServiceClient sc, String operation) throws Exception{	
	
		Options opts = sc.getOptions();
		opts.setProperty(HTTPConstants.HEADER_COOKIE, sessionId);
		opts.setAction("urn:" + operation);
		opts.setManageSession(true);		
	}
	
	public void setSession(String session){
		this.sessionId = session;
	}
		
	private void setLoginServiceClient() throws Exception{
		scLogin = new ServiceClient(null,null);
		Options opts = new Options();		
		opts.setTo(new EndpointReference(serverURL + "/services/SMIWebLogin.SMIWebLoginHttpSoap11Endpoint"));				
		scLogin.setOptions(opts);
	}
	private void setAdminServiceClient() throws Exception{		
		scAdmin = new ServiceClient(null,null);		
		Options opts = new Options();		
		opts.setTo(new EndpointReference(serverURL + "/services/Admin.AdminHttpSoap11Endpoint"));				
		scAdmin.setOptions(opts);
	}	
	
	private void setLogoutServiceClient() throws Exception {
		scLogout = new ServiceClient(null, null);
		Options opts = new Options();
		opts.setTo(new EndpointReference(serverURL + "/services/SMIWebSession.SMIWebSessionHttpSoap11Endpoint"));
		scLogout.setOptions(opts);
	}
	private OMElement createPayLoad(String command, String paramName, Object text) throws Exception{
		List<String> params = new ArrayList<String>();
		params.add(paramName);
		List<Object> values = new ArrayList<Object>();
		values.add(text);
		return createPayLoad(command, params, values);
	}
	
	private OMElement createPayLoad(String command, List<String> params, List<Object> values){
		
		if(params.size() != values.size()){
			return null;
		}		
		int size = params.size();
		
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace omNs = fac.createOMNamespace(ADMIN_SERVICES_NAMESPACE, "ns1");		
		OMElement method = fac.createOMElement(command, omNs);
		
		for(int i=0; i<size; i++){
			String paramName = params.get(i);
			String text = values.get(i).toString();
			OMElement value = fac.createOMElement(paramName, omNs);
			value.setText(text);
			method.addChild(value);			
		}
		
		return method;
	}
	
	public String clearAllCaches(boolean allServers) throws Exception {
		init(scAdmin, "clearAllCaches");				
		OMElement payLoad = createPayLoad("clearAllCaches", "allServers", Boolean.valueOf(allServers));		
		OMElement opSend = scAdmin.sendReceive(payLoad);
		return opSend.getFirstElement().toString();
	}

	public String clearCacheForQuery(Map<String, String> parameterMap, boolean allServers) throws Exception {
		
		init(scAdmin, "clearCacheForQuery");
		
		List<String> params = new ArrayList<String>();
		params.add("query");
		params.add("exactMatch");
		params.add("reseed");
		params.add("allServers");	
		
		List<Object> values = new ArrayList<Object>();
		values.add(parameterMap.get("cmdParameter"));
		String exactMatch = parameterMap.get("ExactMatchOnly");
		String reseed = parameterMap.get("Reseed");
		exactMatch = (exactMatch == null?"false":exactMatch);
		reseed = (reseed == null?"false":reseed);
		values.add(exactMatch);
		values.add(reseed);
		values.add(Boolean.valueOf(allServers));		
		OMElement payLoad = createPayLoad("clearCacheForQuery", params, values);
		OMElement opSend = scAdmin.sendReceive(payLoad);
		return opSend.getFirstElement().toString();
	}

	public String clearCacheForPhysicalTable(String tableName,boolean allServers) throws Exception {
		
		init(scAdmin,"clearCacheForPhysicalTable");
		List<String> params = new ArrayList<String>();
		params.add("tableName");
		params.add("allServers");
		
		List<Object> values = new ArrayList<Object>();
		values.add(tableName);
		values.add(Boolean.valueOf(allServers));
		OMElement payLoad = createPayLoad("clearCacheForPhysicalTable", params, values);		
		OMElement opSend = scAdmin.sendReceive(payLoad);
		return opSend.getFirstElement().toString();
	}

	public String clearCacheForLogicalTable(String tableName, boolean allServers) throws Exception {
		
		init(scAdmin,"clearCacheForLogicalTable");
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("tableName", tableName);
		map.put("allServers", Boolean.valueOf(allServers));
		
		List<String> params = new ArrayList<String>();
		params.add("tableName");
		params.add("allServers");
		
		List<Object> values = new ArrayList<Object>();
		values.add(tableName);
		values.add(Boolean.valueOf(allServers));
		OMElement payLoad = createPayLoad("clearCacheForLogicalTable", params, values);		
		OMElement opSend = scAdmin.sendReceive(payLoad);
		return opSend.getFirstElement().toString();
	}
	
	public String login(String token) throws Exception {
		//init(scLogin, "login");			
		Options opts = scLogin.getOptions();
		opts.setAction("urn:login");
		Hashtable<String, String> headers = new Hashtable<String, String>();
		headers.put("ssoUser", token);
		opts.setProperty(HTTPConstants.HTTP_HEADERS, headers);		
		scLogin.sendReceive(null);
	    String cookie = (String) scLogin.getLastOperationContext().getProperty(HTTPConstants.HEADER_COOKIE);	    
	    opts.setManageSession(true);
	    return cookie;		
	}
	
	public void logout() throws Exception{
		init(scLogout,"logout");		
		scLogout.sendReceive(null);		
	}	
}
