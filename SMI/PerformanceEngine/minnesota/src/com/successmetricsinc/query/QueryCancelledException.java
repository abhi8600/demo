/**
 * $Id: QueryCancelledException.java,v 1.4 2009-12-22 19:01:44 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import com.successmetricsinc.util.BaseException;

/**
 * Exception thrown when user cancels a running query and statement is closed.
 * 
 * 
 */
public class QueryCancelledException extends BaseException
{
	private static final long serialVersionUID = 1L;

	/**
	 * @param s
	 */
	public QueryCancelledException(String s, Throwable cause)
	{
		super(s, cause);
	}
	
	public QueryCancelledException(String s)
	{
		super(s);
	}

	@Override
	public int getErrorCode() {
		return ERROR_QUERY_CANCELLED;
	}
}
