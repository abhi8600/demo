/**
 * $Id: Aggregation.java,v 1.5 2011-01-18 18:58:14 sfoo Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.util.List;
import java.io.Serializable;

/**
 * @author bpeters
 * 
 */
public class Aggregation implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	public enum AggregationType implements Serializable
	{
		None(1), Rank(2), Percentile(3), DenseRank(4);
		AggregationType(int value){
			this.value = value;
		}
		public int value;
	};
	private AggregationType type;
	private String columnName;
    private List<String> groups;
    private int index;

	/**
	 * Create new aggregation
	 * 
	 * @param type
	 *            Type of aggregation
	 * @param columnName
	 *            Name of column to aggregate (display name)
	 */
	public Aggregation(AggregationType type, String columnName, int index)
	{
		this.type = type;
		this.columnName = columnName;
		this.index = index;
	}

	/**
	 * @return Returns the type.
	 */
	public AggregationType getType()
	{
		return type;
	}

	/**
	 * @return Returns the columnName.
	 */
	public String getColumnName()
	{
		return columnName;
	}

    /**
     * @return Returns the groups.
     */
    public List<String> getGroups()
    {
        return groups;
    }

    /**
     * @param groups The groups to set.
     */
    public void setGroups(List<String> groups)
    {
        this.groups = groups;
    }

	/**
	 * @return the index
	 */
	public int getIndex()
	{
		return index;
	}

	/**
	 * @param index the index to set
	 */
	public void setIndex(int index)
	{
		this.index = index;
	}
}
