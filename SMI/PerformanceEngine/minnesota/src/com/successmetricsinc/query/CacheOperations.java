/**
 * $Id: CacheOperations.java,v 1.27 2012-04-13 19:00:18 gsingh Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.util.RepositoryException;
import com.successmetricsinc.util.Util;

public class CacheOperations
{	
	private static Logger logger = Logger.getLogger(CacheOperations.class);
	public static final String DELIMITER_BEGIN = "$serverURL-";
	public static final String DELIMITER_END = "-serverURL$";
	public CacheOperations()
	{
	}
	
    public synchronized List<String> performCacheOperation(Map<String, String> parameterMap, Repository r, Session session, ResultSetCache rsc)
    {
    	List<String> results = new ArrayList<String> ();
    	String cmd = parameterMap.get("cmd");
    	String cmdParameter = parameterMap.get("cmdParameter");
    	if(logger.isDebugEnabled())
    	{
    		logger.debug("STARTING cache operation for local server: " + cmd + ", " + cmdParameter);
    		results.add("Cache operation starting for server: " + cmd);
    	}
    	
	    if (cmd.equals("ClearCache"))
	    {		        
        	Util.touchRepositoryFile(r);
	        logger.info("Cache Cleared");
	        results.add("Cache Cleared");
	    }
	    else if(cmd.equals("ClearCacheForQuery"))
		{
	    	String query = cmdParameter;
	    	boolean exactMatchOnly = false;
	    	String exactMatchOnlyStr = parameterMap.get("ExactMatchOnly");
	    	if(exactMatchOnlyStr != null && (exactMatchOnlyStr.equals("true") || exactMatchOnlyStr.equals("on")))
	    	{
	    		exactMatchOnly = true;
	    	}
	    	boolean reseed = false;
	    	String reseedStr = parameterMap.get("Reseed");
	    	if(reseedStr != null && (reseedStr.equals("true") || reseedStr.equals("on")))
	    	{
	    		reseed = true;
	    	}
	        if(query != null)
	        {	        	
	        	boolean success = true;
	        	boolean foundMatch = false;
	        	try
	        	{
	        		foundMatch = rsc.removeCacheForQuery(query, r, session, reseed, exactMatchOnly);
	        	}
	        	catch(Exception e)
	        	{
	        		success = false;
	        		logger.error(e, e);
	        		results.add("Error removing cache for query: "+ e.toString());
	        	}
	        	if(success)
	        	{
	        		if(foundMatch)
	        		{
		        		results.add("Cache Cleared for query: " + QueryString.filterServerPassword(query));
	        		}
	        		else
	        		{
		        		results.add("No matching cache entries found for query: " + QueryString.filterServerPassword(query));
	        		}
	        	}
	        }
		}
		else if(cmd.equals("ClearCacheForLogicalTable"))
		{
			String logicalTableName = cmdParameter;
	        if(logicalTableName != null)
	        {
	        	boolean success = true;
	        	boolean foundMatch = false;
	        	try
	        	{
	        		foundMatch = rsc.removeCacheForTable(logicalTableName, r, session, true, false);
	        	}
	        	catch(Exception e)
	        	{
	        		success = false;
	        		logger.error(e, e);
	        		results.add("Error removing cache for query: " + e.toString());
	        	}
	        	if(success)
	        	{
	        		if(foundMatch)
	        		{
	        			results.add("Cache Cleared for logical table: " + logicalTableName);
	        		}
	        		else
	        		{
	        			results.add("No matching cache entries found for logical table: " + logicalTableName);
	        		}
	        	}
	        }
		}
		else if(cmd.equals("ClearCacheForPhysicalTable"))
		{
			String physicalTableName = cmdParameter;
	        if(physicalTableName != null)
	        {
	        	boolean foundMatch = false;
	        	try
	        	{
	        		foundMatch = rsc.removeCacheForTable(physicalTableName, r, session, false, false);
	        	}
	        	catch(Exception e)
	        	{
	        		logger.error(e, e);
	        		results.add("Error removing cache for query: " + e.toString());
	        	}
        		if(foundMatch)
        		{
	                results.add("Cache Cleared for physical table: " + physicalTableName);
        		}
        		else
        		{
        			results.add("No matching cache entries found for physical table: " + physicalTableName);
        		}
	        }
		}
	    
	   	if(logger.isDebugEnabled())
    	{
    		logger.debug("FINISHED cache operation for local server: " + cmd + ", " + cmdParameter);
    	}
	    return results;
    }
}
