/**
 * 
 */
package com.successmetricsinc.query;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author agarrison
 *
 */
@SuppressWarnings("serial")
public class BirstRatio extends Number {

	private final Number numerator;
	private final Number denominator;
	private final Double ratioValue;
	
	private static final Double ZERO = Double.valueOf(0.0);
	public static final String RATIO = "_RATIO";
	public static final String DENOMINATOR = "_DENOMINATOR";
	public static final String NUMERATOR = "_NUMERATOR";
	
	public BirstRatio() {
		ratioValue = ZERO;
		numerator = 0;
		denominator = 0;
	}
	/**
	 * 
	 */
	public BirstRatio(final Number n, final Number d) {
		if (n == null || d == null)
			ratioValue = null;
		else
			ratioValue = Double.valueOf((n.doubleValue() / d.doubleValue()));
		
		numerator = n;
		denominator = d;
	}

	/**
	 * @return the numerator
	 */
	public double getNumerator() {
		return numerator == null ? 0.0 : numerator.doubleValue();
	}

	/**
	 * @return the denominator
	 */
	public double getDenominator() {
		return denominator == null ? 0.0 : denominator.doubleValue();
	}

	public Object add(Object value) {
		if (value == null)
			return this;
		
		if (value instanceof BirstRatio) {
			if (ratioValue == null)
				return value;
			else if (((BirstRatio) value).ratioValue == null)
				return this;
			return new BirstRatio(getNumerator() + ((BirstRatio)value).getNumerator(),
								  getDenominator() + ((BirstRatio)value).getDenominator());
		}
		
		return null;
	}
	@Override
	public double doubleValue() {
		return ratioValue == null ? ZERO.doubleValue() : ratioValue.doubleValue();
	}
	@Override
	public float floatValue() {
		return ratioValue == null ? ZERO.floatValue() : ratioValue.floatValue();
	}
	@Override
	public int intValue() {
		return ratioValue == null ? ZERO.intValue() : ratioValue.intValue();
	}
	@Override
	public long longValue() {
		return ratioValue == null ? ZERO.longValue() : ratioValue.longValue();
	}
	
	public boolean isNaN() {
		return ratioValue == null ? false : ratioValue.isNaN();
	}

	public boolean isInfinite() {
		return ratioValue == null ? false : ratioValue.isInfinite();
	}
	
	public boolean isNull() {
		return ratioValue == null;
	}
	public boolean equals(Object o) {
		return ratioValue == null ? o == null : ratioValue.equals(o);
	}
	
	public Number aggregate(String rule, BirstRatio value) {
		if (value == null || value.ratioValue == null || value.isNaN())
			return this;
		
		if (this.ratioValue == null || this.isNaN())
			return value;
		
		if (rule != null)
		{
			if (rule.equals("SUM") || rule.equals("COUNT") || rule.equals("AVG"))
				return (BirstRatio)add(value);
			if (rule.equals("MIN"))
				return doubleValue() < value.doubleValue() ? this : value;
			if (rule.equals("MAX"))
				return doubleValue() > value.doubleValue() ? this : value;
		}
		return (this);
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ratioValue == null ? "" : ratioValue.toString();
	}
}
