/**
 * $Id: NavigationException.java,v 1.3 2009-12-22 19:01:44 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import com.successmetricsinc.util.BaseException;

/**
 * Exception that indicates that a given query could not be navigated (different columns in the query do not join)
 * 
 * @author bpeters
 * 
 */
public class NavigationException extends BaseException
{
    private static final long serialVersionUID = 1L;

    public NavigationException(String message)
    {
        super(message);
    }
    
    public int getErrorCode()
    {
    	return ERROR_NAVIGATION;
    }
}
