package com.successmetricsinc.query;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.fill.AbstractValueProvider;
import net.sf.jasperreports.engine.fill.JRAbstractExtendedIncrementerFactory;
import net.sf.jasperreports.engine.fill.JRCalculable;
import net.sf.jasperreports.engine.fill.JRDefaultIncrementerFactory;
import net.sf.jasperreports.engine.fill.JRExtendedIncrementer;
import net.sf.jasperreports.engine.fill.JRFillVariable;
import net.sf.jasperreports.engine.type.CalculationEnum;

public class BirstRatioIncrementerFactory extends JRAbstractExtendedIncrementerFactory {

	private static final BirstRatio ZERO = new BirstRatio();
	

	public static class BirstRatioIncrementer implements JRExtendedIncrementer {

		@Override
		public Object combine(JRCalculable calculable,
				JRCalculable calculableValue,
				AbstractValueProvider valueProvider) throws JRException {
			if (calculable == null || calculable.getValue() == null)
				return calculableValue == null ? null : calculableValue.getValue();
			
			return ((BirstRatio)calculable.getValue()).add(calculableValue == null ? null : calculableValue.getValue());
		}

		@Override
		public boolean ignoresNullValues() {
			return true;
		}

		@Override
		public Object increment(JRCalculable calculable,
				Object expressionValue, AbstractValueProvider valueProvider)
				throws JRException {
			BirstRatio value = (BirstRatio)calculable.getIncrementedValue();
			BirstRatio newValue = (BirstRatio)expressionValue;

			if (newValue == null)
			{
				if (calculable.isInitialized())
				{
					return null;
				}

				return value;
			}

			if (calculable.isInitialized())
			{
				value = ZERO;
			}

			if (value == null || value.isNaN() || value.isNull())
				return newValue;
			if (newValue.isNaN() || newValue.isNull()) {
				if (calculable.isInitialized())
					return newValue;
				
				return value;
			}
			return new BirstRatio(value.getNumerator() + newValue.getNumerator(), 
					value.getDenominator() + newValue.getDenominator());
		}

		@Override
		public Object initialValue() {
			return null;
		}

		@Override
		public Object increment(JRFillVariable variable,
				Object expressionValue, AbstractValueProvider valueProvider)
				throws JRException {
			BirstRatio value = (BirstRatio)variable.getValue();
			BirstRatio newValue = (BirstRatio)expressionValue;

			if (newValue == null)
			{
				if (variable.isInitialized())
				{
					return null;
				}

				return value;
			}

			if (variable.isInitialized())
			{
				value = ZERO;
			}

			if (value == null || value.isNaN() || value.isNull())
				return newValue;
			if (newValue.isNaN() || newValue.isNull()) {
				if (variable.isInitialized())
					return newValue;
				
				return value;
			}
			return new BirstRatio(value.getNumerator() + newValue.getNumerator(), 
					value.getDenominator() + newValue.getDenominator());
		}

	}

	public JRExtendedIncrementer getExtendedIncrementer(CalculationEnum calculation) {
		if (calculation != CalculationEnum.SUM)
			return JRDefaultIncrementerFactory.getInstance().getExtendedIncrementer(calculation);
		
		return new BirstRatioIncrementer();
	}

}
