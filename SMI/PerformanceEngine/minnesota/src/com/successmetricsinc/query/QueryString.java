/**
 * $Id: QueryString.java,v 1.84 2011-10-24 12:04:50 rchandarana Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.query.Aggregation.AggregationType;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.RepositoryException;
import com.successmetricsinc.util.Util;

/**
 * Class to process Query Strings into Query structures
 * 
 * @author bpeters
 * 
 */
public class QueryString extends AbstractQueryString
{
	private static final Logger logger = Logger.getLogger(QueryString.class);
	public static final String NO_FILTER_TEXT = "NO_FILTER";
	private boolean USE_GROUP_BYS = true;
	private List<DisplayFilter> displayFilters;
	private Repository r;
	private WebServerConnectInfo serverConnectInfo;
	private String serverQuery;
	private int curColumn = 0;
	private Map<String, Integer> columnMap = new HashMap<String, Integer>();
	private List<DisplayExpression> expressionList = new ArrayList<DisplayExpression>();
	private List<DisplayOrder> displayOrderList = new ArrayList<DisplayOrder>();
	private static final String[] COMPARATORS = new String[]
	{ "<>", ">=", "<=", "=", "<", ">", " NOT LIKE ", " LIKE ", " NOT IN ", " IN ", " IS NULL", " IS NOT NULL", " EXISTS", " NOT EXISTS" };
	private static final String[] MCOMPARATORS = new String[]
	{ "<>", ">=", "<=", "=", "<", ">", " IS NULL", " IS NOT NULL" };
	public static final Map<String, String> UNARY_COMPARATORS = new HashMap<String, String>();
	private static final Pattern fmPattern = Pattern.compile("FDC[{].*[}]");
	private String _query;
	private Session session;
	static
	{
		UNARY_COMPARATORS.put("IS NULL", " IS NULL"); // key is trimmed version of the operator and value is actual
														// operator
		UNARY_COMPARATORS.put("IS NOT NULL", " IS NOT NULL");
	}

	// private Connection conn;
	/**
	 * Create a new Query String processing class.
	 * 
	 * @param r
	 *            Repository - if null, need to include the Repository{path} token in the query string to point to the
	 *            appropriate repository
	 */
	public QueryString(Repository r)
	{
		displayFilters = new ArrayList<DisplayFilter>();
		this.r = r;
	}

	public QueryString(Repository r, String _query)
	{
		displayFilters = new ArrayList<DisplayFilter>();
		this.r = r;
		this._query = _query;
	}

	/**
	 * Return a QueryFilter object best on a filter query string used in a report query. Syntax allows for: FDC{a.b=0} -
	 * filter on dimension column, FM{c=0} - filter on measure, FAND - and two other filters (e.g.
	 * FAND{FDC{a.b>0},FDC{c.d=1}}, FOR - or two other filters (same as FAND)
	 * 
	 * @param t
	 *            Token containing query string
	 * @return
	 */
	public static QueryFilter returnFilter(Token t, Repository r) throws SyntaxErrorException
	{
		if (t.type.equals("FDC"))
		{
			String[] strs = Util.splitOperator(t.body, COMPARATORS);
			if (strs != null && strs.length == 3 && strs[0] == null && strs[1] == null && strs[2] == null)
			{
				throw (new SyntaxErrorException("Error Processing Dimension Column Filter: " + t.body));
			}
			// If a dimension column filter
			if (strs[0] == null)
				return (null);
			// clean up leading/trailing spaces (the regular expressions in DisplayExpression allow leading/trailing)
			for (int i = 0; i < strs.length; i++)
				strs[i] = strs[i].trim();
			int ppos = strs[0].indexOf('.');
			if ((ppos >= 0) && (strs[0] != null))
			{
				String dimName = strs[0].substring(0, ppos).replaceAll("\"", "");
				String colName = strs[0].substring(ppos + 1).replaceAll("\"", "");
				String op = strs[1].trim();
				String value = strs[2];
				if (value == null || value.isEmpty())
				{
					if (UNARY_COMPARATORS.containsKey(op))
					{
						return (new QueryFilter(dimName, colName, UNARY_COMPARATORS.get(op), value));
					}
					return (null);
				} else if (value.equals(NO_FILTER_TEXT))
				{
					return (null);
				}
				if ((op.equals("IN") || op.equals("NOT IN")) && value.startsWith("Q{"))
				{
					return new QueryFilter(dimName, colName, op, preProcessQueryString(value, r, null));					
				}
				if (value.indexOf(r.getOrOperatorForMultiValueFilter()) != -1)
				{
					Token FORToken = convertToORToken(t, r, "FOR");
					return returnFilter(FORToken, r);
				}
				if (value.indexOf(r.getAndOperatorForMultiValueFilter()) != -1)
				{
					Token FANDToken = convertToANDToken(t, r, "FAND");
					return returnFilter(FANDToken, r);
				}
				if (op.equals(QueryFilter.LIKE))
				{
					if (value.indexOf('%') < 0)
						value = value + "%";
					else if (value.equals("%") || value.equals("%%"))
						return (null);
				}
				return (new QueryFilter(dimName, colName, op, value));
			} else
			{
				throw (new SyntaxErrorException("Error Processing Dimension Column Filter: " + t.body));
			}
		} else if (t.type.equals("FM"))
		{
			// If a measure filter
			Matcher m = fmPattern.matcher(t.body);
			String measureFilter = null;
			if (m.find())
			{
				measureFilter = m.group();
				m.replaceFirst("");
				t.body = t.body.substring(0, m.start() - 1) + t.body.substring(m.end());
			}
			String[] strs = Util.splitOperator(t.body, MCOMPARATORS);
			if (strs[0] != null)
			{
				String colName = strs[0].replaceAll("\"", "");
				String op = strs[1].trim();
				String value = strs[2];
				if (value == null || value.isEmpty())
				{
					if (UNARY_COMPARATORS.containsKey(op))
					{
						return (new QueryFilter(colName, UNARY_COMPARATORS.get(op), value));
					}
					return (null);
				} else if (value.equals(NO_FILTER_TEXT))
				{
					return (null);
				}
				if (measureFilter == null)
					return (new QueryFilter(colName, op, value));
				else
				{
					Token filterToken = new Token();
					filterToken.body = measureFilter.substring(4, measureFilter.length() - 1);
					filterToken.type = "FDC";
					QueryFilter mFilter = QueryString.returnFilter(filterToken, r);
					if (mFilter == null)
						return (null);
					return (new QueryFilter(colName, op, value, mFilter));
				}
			} else
			{
				throw (new SyntaxErrorException("Error Processing Measure Filter: " + t.body));
			}
		} else if (t.type.equals("FAND"))
		{
			// If a compound filter
			List<Token> tlist = Token.getTokens(t.body);
			List<QueryFilter> qFilterList = new ArrayList<QueryFilter>();
			for (int i = 0; i < tlist.size(); i++)
			{
				Token token = (Token) tlist.get(i);
				QueryFilter filter = returnFilter(token, r);
				if (filter == null)
				{
					logger.debug("Ignoring filter expression: " + token);
				} else
				{
					qFilterList.add(filter);
				}
			}
			if (qFilterList.isEmpty()) // No filters
			{
				return null;
			}
			if (qFilterList.size() == 1)
			{
				return qFilterList.get(0);
			}
			return (new QueryFilter("AND", qFilterList));
		} else if (t.type.equals("FOR"))
		{
			// If a compound filter
			List<Token> tlist = Token.getTokens(t.body);
			List<QueryFilter> qFilterList = new ArrayList<QueryFilter>();
			for (int i = 0; i < tlist.size(); i++)
			{
				Token token = (Token) tlist.get(i);
				QueryFilter filter = returnFilter(token, r);
				if (filter == null)
				{
					logger.debug("Ignoring filter token: " + token);
				} else
				{
					qFilterList.add(filter);
				}
			}
			if (qFilterList.isEmpty())
			{
				return null;
			}
			if (qFilterList.size() == 1)
			{
				return qFilterList.get(0);
			}
			return (new QueryFilter("OR", qFilterList));
		} else
		{
			throw (new SyntaxErrorException("Error Processing Filter Expression: " + t.body));
		}
	}

	/**
	 * Convert a single FDC or FM or DF token with "||" separated values into a single FOR or DFOR token. e.g.
	 * FDC{Time.Year=2004||2005||2006} is converted to FOR{FDC{Time.Year=2004},FDC{Time.Year=2005},FDC{Time.Year=2006}}
	 * format. An example for display filter would be - DF{Time.Year=2004||2005||2006} is converted to
	 * DFOR{DF{Time.Year=2004},DF{Time.Year=2005},DF{Time.Year=2006}}
	 * 
	 * @param t
	 *            Token to be processed
	 * @param r
	 *            Repository
	 * @param orTokenStr
	 *            Should be either FOR or DFOR
	 * @return FOR or DFOR token
	 */
	private static Token convertToORToken(Token t, Repository r, String orTokenStr)
	{
		Token ORToken = new Token();
		ORToken.type = orTokenStr;
		StringBuilder bodyBuf = new StringBuilder();
		String[] strs = Util.splitOperator(t.body, COMPARATORS);
		String value = strs[2];
		StringTokenizer tokenizer = new StringTokenizer(value, r.getOrOperatorForMultiValueFilter());
		while (tokenizer.hasMoreTokens())
		{
			bodyBuf.append(t.type);
			bodyBuf.append('{');
			bodyBuf.append(strs[0]);
			bodyBuf.append(strs[1]);
			bodyBuf.append(tokenizer.nextToken());
			bodyBuf.append('}');
			if (tokenizer.hasMoreTokens())
			{
				bodyBuf.append(',');
			}
		}
		ORToken.body = bodyBuf.toString();
		return ORToken;
	}

	/**
	 * Convert a single FDC or FM or DF token with "&&" separated values into a single FAND or DFAND token. e.g.
	 * FDC{Time.Year=2004&&2005&&2006} is converted to FAND{FDC{Time.Year=2004},FDC{Time.Year=2005},FDC{Time.Year=2006}}
	 * format. An example for display filter would be - DF{Time.Year=2004&&2005&&2006} is converted to
	 * DFAND{DF{Time.Year=2004},DF{Time.Year=2005},DF{Time.Year=2006}}
	 * 
	 * @param t
	 *            Token to be processed
	 * @param r
	 *            Repository
	 * @param orTokenStr
	 *            Should be either FOR or DFOR
	 * @return FAND or DFAND token
	 */
	private static Token convertToANDToken(Token t, Repository r, String andTokenStr)
	{
		Token ANDToken = new Token();
		ANDToken.type = andTokenStr;
		StringBuilder bodyBuf = new StringBuilder();
		String[] strs = Util.splitOperator(t.body, COMPARATORS);
		String value = strs[2];
		StringTokenizer tokenizer = new StringTokenizer(value, r.getAndOperatorForMultiValueFilter());
		while (tokenizer.hasMoreTokens())
		{
			bodyBuf.append(t.type);
			bodyBuf.append('{');
			bodyBuf.append(strs[0]);
			bodyBuf.append(strs[1]);
			bodyBuf.append(tokenizer.nextToken());
			bodyBuf.append('}');
			if (tokenizer.hasMoreTokens())
			{
				bodyBuf.append(',');
			}
		}
		ANDToken.body = bodyBuf.toString();
		return ANDToken;
	}

	/**
	 * Preprocesses a query string - replace remove new lines and carriage returns, replace repository and session
	 * variable strings
	 * 
	 * @param queryString
	 * @param r
	 * @param session
	 * @return Preprocessed query string.
	 */
	public static String preProcessQueryString(String queryString, Repository r, Session session)
	{
		if (queryString == null || queryString.equals(""))
		{
			return queryString;
		}
		String preprocessedQueryString = queryString.replaceAll("[\r\n]", "");
		if (r != null)
		{
			preprocessedQueryString = r.replaceVariables(session, preprocessedQueryString);
		}
		return preprocessedQueryString;
	}

	/**
	 * Return a DisplayFilter object best on a filter query string used in a report query. Syntax allows for: DF{a=0} -
	 * filter on a column DFAND{DF{a>0},DFOR{DF{b<5},DF{c>10}}} - combinations of ands and ors of sub-display filters
	 * 
	 * @param t
	 *            Token containing query string
	 * @return
	 */
	public static DisplayFilter returnDisplayFilter(Token t, Repository r) throws SyntaxErrorException
	{
		if (t.type.equals("DF"))
		{
			// Display filter
			String[] strs = Util.splitOperator(t.body, COMPARATORS);
			// Check for Syntax Error
			if (strs != null && strs.length == 3 && strs[0] == null && strs[1] == null && strs[2] == null)
			{
				throw (new SyntaxErrorException("Error Processing  Display Filter Token: " + t.body));
			}
			// If a dimension column filter
			int ppos = -1;
			if (!strs[0].startsWith("Rank:") && !strs[0].startsWith("DRank:") && !strs[0].startsWith("Percentile:"))
			{
				ppos = strs[0].indexOf('.');
			}
			if (strs[0] != null)
			{
				String colName = null;
				String tName = null;
				/*
				 * If this isa table and a column
				 */
				if (ppos > 0)
				{
					tName = strs[0].substring(0, ppos);
					colName = strs[0].substring(ppos + 1);
				} else
					colName = strs[0].replaceAll("\"", "");
				String op = strs[1].trim();
				String value = strs[2];
				if (value == null || value.isEmpty())
				{
					if (UNARY_COMPARATORS.containsKey(op))
					{
						if (tName != null)
							return (new DisplayFilter(tName, colName, UNARY_COMPARATORS.get(op), value));
						else
							return (new DisplayFilter(colName, UNARY_COMPARATORS.get(op), value));
					}
					return (null);
				} else if (value.equals(NO_FILTER_TEXT))
				{
					return (null);
				}
				if (value.indexOf(r.getOrOperatorForMultiValueFilter()) != -1)
				{
					Token DFORToken = convertToORToken(t, r, "DFOR");
					return returnDisplayFilter(DFORToken, r);
				}
				if (value.indexOf(r.getAndOperatorForMultiValueFilter()) != -1)
				{
					Token DFANDToken = convertToANDToken(t, r, "DFAND");
					return returnDisplayFilter(DFANDToken, r);
				}
				if (op.equals(QueryFilter.LIKE))
				{
					if (value.indexOf('%') < 0)
						value = value + "%";
					else if (value.equals("%") || value.equals("%%"))
						return (null);
				}
				if (tName != null)
					return (new DisplayFilter(tName, colName, op, value));
				else
					return (new DisplayFilter(colName, op, value));
			} else
			{
				throw (new SyntaxErrorException("Error Processing  Display Filter Token: " + t.body));
			}
		} else if (t.type.equals("DFAND"))
		{
			// If a compound filter
			List<Token> tlist = Token.getTokens(t.body);
			List<DisplayFilter> dFilterList = new ArrayList<DisplayFilter>();
			for (int i = 0; i < tlist.size(); i++)
			{
				Token token = (Token) tlist.get(i);
				DisplayFilter filter = returnDisplayFilter(token, r);
				if (filter == null)
				{
					logger.debug("Ignoring display filter expression: " + token);
				} else
				{
					dFilterList.add(filter);
				}
			}
			if (dFilterList.isEmpty()) // No filters
			{
				return null;
			}
			if (dFilterList.size() == 1)
			{
				return dFilterList.get(0);
			}
			return (new DisplayFilter(DisplayFilter.Operator.And, dFilterList));
		} else if (t.type.equals("DFOR"))
		{
			// If a compound filter
			List<Token> tlist = Token.getTokens(t.body);
			List<DisplayFilter> dFilterList = new ArrayList<DisplayFilter>();
			for (int i = 0; i < tlist.size(); i++)
			{
				Token token = (Token) tlist.get(i);
				DisplayFilter filter = returnDisplayFilter(token, r);
				if (filter == null)
				{
					logger.debug("Ignoring display filter expression: " + token);
				} else
				{
					dFilterList.add(filter);
				}
			}
			if (dFilterList.isEmpty()) // No filters
			{
				return null;
			}
			if (dFilterList.size() == 1)
			{
				return dFilterList.get(0);
			}
			return (new DisplayFilter(DisplayFilter.Operator.Or, dFilterList));
		}
		return (null);
	}

	/**
	 * Process a token for a query string. Valid options are: DC{a.b} - dimension column, M{c} - measure column, MF{c} -
	 * return measure column number format, ODC{a.b} - order by a dimension column, OM{c} - order by a measure column,
	 * TOP{n} - limit results to top n rows
	 * 
	 * @param q
	 * @param t
	 * @param makeNamesPhysical
	 * @throws NavigationException
	 * @throws DisplayExpressionException
	 * @throws Exception
	 */
	private void processToken(Query q, Token t, boolean makeNamesPhysical) throws BaseException, CloneNotSupportedException
	{
		if (q == null)
			throw (new SyntaxErrorException("Must Specify Repository"));
		if (ignoreDisplay)
			t.display = null;
		// If it is a dimension column
		if (t.type.equals("DC"))
		{
			int ppos = t.body.indexOf('.');
			DimensionColumnNav dcn = null;
			if (ppos < 0)
			{
				throw new BadColumnNameException(t.body);
			} else
			{
				String dimName = t.body.substring(0, ppos).replaceAll("\"", "").trim();
				String colName = t.body.substring(ppos + 1, t.body.length()).replaceAll("\"", "").trim();
				if (makeNamesPhysical)
				{
					String display = (t.display != null) ? t.display : colName;
					dcn = q.addDimensionColumn(dimName, colName, Util.replaceWithPhysicalString(display));
				} else if (t.display != null)
					dcn = q.addDimensionColumn(dimName, colName, t.display);
				else
					dcn = q.addDimensionColumn(dimName, colName);
				if (USE_GROUP_BYS)
				{
					// Add implicit grouping
					q.addGroupBy(new GroupBy(dimName, colName));
				}
				curColumn++;
			}
			if (dcn == null)
				throw new BadColumnNameException(t.body);
		} else if (t.type.equals("CONST"))
		{
			// If it is a constant
			if (t.display != null)
				q.addConstantColumn("'" + t.body + "'", t.display);
			else
				q.addConstantColumn("'" + t.body + "'", Util.replaceWithPhysicalString(t.body));
			curColumn++;
		} else if (t.type.equals("M"))
		{
			// If a measure
			int ppos = t.body.indexOf(',');
			String body = t.body;
			String display = (t.display != null) ? t.display : (ppos < 0) ? t.body : t.body.substring(0, ppos);
			MeasureColumnNav mcn = null;
			if (ppos <= 0)
			{
				if (makeNamesPhysical)
				{
					mcn = q.addMeasureColumn(body, Util.replaceWithPhysicalString(display), false);
				} else
				{
					mcn = q.addMeasureColumn(body, display, false);
				}
			} else
			{
				body = t.body.substring(0, ppos);
				Token nt = new Token();
				int spos = t.body.indexOf('{', ppos);
				nt.type = t.body.substring(ppos + 1, spos);
				nt.body = t.body.substring(spos + 1, t.body.length() - 1);
				QueryFilter qf = returnFilter(nt, r);
				if (makeNamesPhysical)
				{
					mcn = q.addMeasureColumn(body, Util.replaceWithPhysicalString(display), false, qf);
				} else
				{
					mcn = q.addMeasureColumn(body, display, false, qf);
				}
			}
			if (mcn == null)
				throw new BadColumnNameException(t.body);
			curColumn++;
		} else if (t.type.equals("MF"))
		{
			// If a measure format
			q.addMeasureColumnFormat(t.body);
			curColumn++;
		} else if (t.type.equals("ODC"))
		{
			// Order by dimension column
			int ppos = t.body.indexOf('.');
			int cpos = t.body.indexOf(',');
			if ((cpos < 0) || (ppos < 0))
			{
				throw (new SyntaxErrorException("Syntax Error - Order By Dimension Column: " + t.body));
			} else
			{
				String dimName = t.body.substring(0, ppos).replaceAll("\"", "").trim();
				String colName = t.body.substring(ppos + 1, cpos).replaceAll("\"", "").trim();
				String dstr = t.body.substring(cpos + 1, t.body.length()).replaceAll("\"", "").trim();
				boolean dir = dstr.toLowerCase().startsWith("asc");
				OrderBy ob = new OrderBy(dimName, colName, dir);
				q.addOrderBy(ob);
			}
		} else if (t.type.equals("OM"))
		{
			// Order by measure
			int cpos = t.body.indexOf(',');
			if (cpos < 0)
			{
				throw (new SyntaxErrorException("Syntax Error - Order By Measure: " + t.body));
			} else
			{
				String mname = t.body.substring(0, cpos).replaceAll("\"", "");
				String dstr = t.body.substring(cpos + 1, t.body.length()).trim();
				boolean dir = dstr.toLowerCase().startsWith("asc");
				OrderBy ob = new OrderBy(mname, dir);
				q.addOrderBy(ob);
			}
		} else if (t.type.equals("OC"))
		{
			// Order by constant
			int cpos = t.body.indexOf(',');
			if (cpos < 0)
			{
				throw (new SyntaxErrorException("Syntax Error - Order By Constant: " + t.body));
			} else
			{
				String constant = t.body.substring(0, cpos).replaceAll("\"", "");
				OrderBy ob = new OrderBy(constant);
				q.addOrderBy(ob);
			}
		} else if (t.type.startsWith("F"))
		{
			QueryFilter qf = returnFilter(t, r);
			if (qf != null)
				q.addFilter(qf);
		} else if (t.type.startsWith("TOP"))
		{
			// Top n
			q.addTopN(Integer.parseInt(t.body));
		} else if (t.type.startsWith("NOGROUPBY"))
		{
			this.USE_GROUP_BYS = false;
		} else if (t.type.startsWith("OUTERJOIN"))
		{
			q.setFullOuterJoin(true);
		} else if (t.type.startsWith("DF"))
		{
			DisplayFilter df = returnDisplayFilter(t, r);
			if (df != null)
				displayFilters.add(df);
		} else if ((t.type.startsWith("MDRANK")) || (t.type.startsWith("MRANK")) || (t.type.startsWith("MPTILE")))
		{
			// Aggregation measure
			String colName = t.body.replaceAll("\"", "");
			String resultName = null;
			if (t.display != null)
				resultName = t.display.trim();
			else
				resultName = (t.type.startsWith("MDRANK")) ? "DRank: " + colName : (t.type.startsWith("MRANK")) ? "Rank: " + colName : "Percentile: " + colName;
			// Look for groupings
			String[] strs = colName.split(",");
			List<String> groups = null;
			QueryFilter qf = null;
			if (strs.length > 1)
			{
				if (!strs[1].startsWith("FDC{") && !strs[1].startsWith("FAND{") && !strs[1].startsWith("FOR{"))
				{
					groups = new ArrayList<String>();
					for (int i = 1; i < strs.length; i++)
					{
						groups.add(strs[i].trim());
					}
				} else
				{
					int index = colName.indexOf(',');
					Token nt = new Token();
					int spos = colName.indexOf('{', index);
					nt.type = colName.substring(index + 1, spos);
					nt.body = colName.substring(spos + 1, t.body.length() - 1);
					qf = returnFilter(nt, r);
				}
			}
			if (makeNamesPhysical)
			{
				q.addMeasureColumn(strs[0], Util.replaceWithPhysicalString(resultName), false, qf);
			} else
			{
				q.addMeasureColumn(strs[0], resultName, false, qf);
			}
			if (t.type.startsWith("MDRANK"))
			{
				Aggregation agg = new Aggregation(AggregationType.DenseRank, resultName, curColumn);
				agg.setGroups(groups);
				q.addAggregation(agg);
			} else if (t.type.startsWith("MRANK"))
			{
				Aggregation agg = new Aggregation(AggregationType.Rank, resultName, curColumn);
				agg.setGroups(groups);
				q.addAggregation(agg);
			} else if (t.type.startsWith("MPTILE"))
			{
				Aggregation agg = new Aggregation(AggregationType.Percentile, resultName, curColumn);
				agg.setGroups(groups);
				q.addAggregation(agg);
			}
			curColumn++;
		} else if (t.type.startsWith("EVAL"))
		{
			String display = (t.display != null) ? t.display.trim() : "Eval:" + t.body;
			// Look for groupings
			String[] strs = t.body.split(",");
			List<String> groups = null;
			if (strs.length > 1)
			{
				if (!strs[1].startsWith("FDC{") && !strs[1].startsWith("FAND{") && !strs[1].startsWith("FOR{"))
				{
					groups = new ArrayList<String>();
					for (int i = 1; i < strs.length; i++)
						groups.add(strs[i]);
				}
			}
			// Evaluate an expression column
			DisplayExpression qse = new DisplayExpression();
			qse.setExpression(r, t.body.replace("\\\"", "\""), session, true);
			qse.setPosition(curColumn++);
			qse.setName(display);
			if (t.type.startsWith("EVALRANK"))
			{
				qse.setRank(true);
				qse.setGroups(groups);
			} else if (t.type.startsWith("EVALDRANK"))
			{
				qse.setDenserank(true);
				qse.setGroups(groups);
			} else if (t.type.startsWith("EVALPTILE"))
			{
				qse.setPercentile(true);
				qse.setGroups(groups);
			}
			expressionList.add(qse);
		} else if (t.type.equals("DO"))
		{
			// Display Order
			int cpos = t.body.indexOf(',');
			if (cpos < 0)
			{
				throw (new SyntaxErrorException("Syntax Error - Order By Display Column: " + t.body));
			} else
			{
				String name = t.body.substring(0, cpos).replaceAll("\"", "");
				String dstr = t.body.substring(cpos + 1, t.body.length()).trim();
				boolean dir = dstr.toLowerCase().startsWith("asc");
				DisplayOrder dorder = new DisplayOrder();
				int ppos = name.indexOf('.');
				if (ppos < 0)
					dorder.columnName = name;
				else
				{
					dorder.tableName = name.substring(0, ppos);
					dorder.columnName = name.substring(ppos + 1);
				}
				dorder.ascending = dir;
				int nullpos = dstr.indexOf(',');
				if (nullpos >= 0)
				{
					boolean nulltop = dstr.substring(nullpos + 1, dstr.length()).equalsIgnoreCase("nulltop");
					dorder.nulltop = nulltop;
				}
				displayOrderList.add(dorder);
			}
		} else
		{
			throw (new SyntaxErrorException("Error processing token: " + t.body));
		}
	}

	public Query getQuery() throws IOException, SQLException, BaseException, CloneNotSupportedException
	{
		return processQueryString(_query, null);
	}

	/**
	 * Process a query string into a Query structure
	 * 
	 * @param query
	 * @return
	 * @throws NavigationException
	 * @throws SQLException
	 * @throws IOException
	 * @throws JDOMException
	 * @throws DisplayExpressionException
	 * @throws SyntaxErrorException
	 * @throws RepositoryException
	 * @throws ScriptException 
	 * @throws Exception
	 */
	public Query processQueryString(String query, Session session) throws IOException, SQLException, BaseException, CloneNotSupportedException
	{
		return processQueryString(query, session, false);
	}

	/**
	 * Process a query string into a Query structure, adjusting output names to valid physical ones if necessary
	 * 
	 * @param query
	 * @param makeNamesPhysical
	 * @return
	 * @throws IOException
	 * @throws JDOMException
	 * @throws NavigationException
	 * @throws SQLException
	 * @throws DisplayExpressionException
	 * @throws SyntaxErrorException
	 * @throws RepositoryException
	 * @throws ScriptException 
	 * @throws Exception
	 */
	@Override
	public Query processQueryString(String query, Session session, boolean makeNamesPhysical) throws SQLException, BaseException, CloneNotSupportedException
	{
		this.session = session;
		if (logger.isTraceEnabled())
			logger.trace("Processing Query: " + filterServerPassword(query));
		List<Token> tokens = Token.getTokens(query);
		serverQuery = query;
		// First, find the repository and setup query
		if (tokens != null)
		{
			for (int i = 0; i < tokens.size(); i++)
			{
				Token t = (Token) tokens.get(i);
				if (t.type.equals("REPOSITORY"))
				{
					/*
					 * Only load query and create a connection if this hasn't been created based on an existing provider
					 */
					if (r == null)
					{
						// Get repository if haven't gotten it already
						SAXBuilder builder = new SAXBuilder();
						Document d = null;
						File tf = new File(t.body);
						try
						{
							d = builder.build(tf);
							r = new Repository(d, false, tf.lastModified());
						} catch (JDOMException e)
						{
							logger.error(e.getMessage(), e);
						} catch (IOException e)
						{
							logger.error(e.getMessage(), e);
						}
					}
					this.USE_GROUP_BYS = true;
					tokens.remove(t);
					break;
				} else if (t.type.equals("SERVER"))
				{
					/*
					 * Get server reference if provided - format is SERVER{URL,Username,Password}
					 */
					int pos1 = -1;
					int pos2 = -1;
					int pos3 = -1;
					pos1 = t.body.indexOf(',');
					if (pos1 > 0)
						pos2 = t.body.indexOf(',', pos1 + 1);
					if (pos2 > 0)
						pos3 = t.body.indexOf(',', pos2 + 1);
					if (pos1 >= 0 && pos2 >= 0)
					{
						serverConnectInfo = new WebServerConnectInfo();
						serverConnectInfo.serverURL = t.body.substring(0, pos1);
						serverConnectInfo.serverUsername = t.body.substring(pos1 + 1, pos2);
						if (pos3 >= 0)
						{
							serverConnectInfo.serverPassword = t.body.substring(pos2 + 1, pos3);
							serverConnectInfo.spaceID = t.body.substring(pos3 + 1);
						} else
							serverConnectInfo.serverPassword = t.body.substring(pos2 + 1);
						StringBuilder sq = new StringBuilder();
						for (int j = i + 1; j < tokens.size(); j++)
						{
							Token nt = tokens.get(j);
							if (sq.length() > 0)
								sq.append(',');
							sq.append(nt.type);
							sq.append('{');
							sq.append(nt.body);
							sq.append('}');
							if (nt.display != null)
							{
								sq.append(' ');
								sq.append(nt.display);
							}
						}
						serverQuery = sq.toString();
						break;
					}
				}
			}
		}
		Query q = null;
		if (r == null && serverConnectInfo == null)
		{
			throw (new NavigationException("No repository loaded"));
		}
		if (tokens == null)
		{
			throw (new SyntaxErrorException("Query syntax error: " + filterServerPassword(query)));
		}
		if (r != null)
		{
			/*
			 * If using a local repository - override any Server tokens
			 */
			serverConnectInfo = null;
			q = new Query(r);
			q.setSession(session);
			for (int i = 0; i < tokens.size(); i++)
			{
				Token t = (Token) tokens.get(i);
				if (!t.type.equals("SERVER"))
					processToken(q, t, makeNamesPhysical);
			}
			return (q);
		} else
			return null;
	}

	/**
	 * @return Returns the displayFilters.
	 */
	public List<DisplayFilter> getDisplayFilters()
	{
		return displayFilters;
	}

	/**
	 * @return Returns the repository used by this query string
	 */
	public Repository getRepository()
	{
		return r;
	}

	/**
	 * @return Returns the columnMap.
	 */
	public Map<String, Integer> getColumnMap()
	{
		return columnMap;
	}

	/**
	 * @return Returns the expressionList.
	 */
	public List<DisplayExpression> getExpressionList()
	{
		return expressionList;
	}

	/**
	 * @return Returns the displayOrderList.
	 */
	public List<DisplayOrder> getDisplayOrderList()
	{
		return displayOrderList;
	}

	/**
	 * @return Returns the serverConnectInfo.
	 */
	public WebServerConnectInfo getServerConnectInfo()
	{
		return serverConnectInfo;
	}

	/**
	 * @return Returns the serverQuery.
	 */
	public String getServerQuery()
	{
		return serverQuery;
	}

	/**
	 * @param serverConnectInfo
	 *            The serverConnectInfo to set.
	 */
	public void setServerConnectInfo(WebServerConnectInfo serverConnectInfo)
	{
		this.serverConnectInfo = serverConnectInfo;
	}

	/**
	 * filter out password from queries that have the SERVER keyword
	 */
	public static String filterServerPassword(String query)
	{
		if (query == null)
			return null;
		int pos = query.toUpperCase().indexOf("SERVER{");
		if (pos < 0) // no SERVER keyword
			return query;
		int posend = query.indexOf('}', pos + 7);
		if (posend < 0)
			return query; // malformed...
		int comma1 = query.indexOf(',', pos + 7);
		if (comma1 < 0)
			return query;
		int comma2 = query.indexOf(',', comma1 + 1);
		if (comma2 > posend)
			return query; // malformed...
		return query.substring(0, comma2 + 1) + "*****" + query.substring(posend);
	}
}
