/**
 * $Id: Query.java,v 1.436 2012/11/16 21:50:03 BIRST\ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.OptionalDataException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.successmetricsinc.Group;
import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.Variable;
import com.successmetricsinc.query.Database.DatabaseType;
import com.successmetricsinc.query.olap.DimensionMemberSetNav;
import com.successmetricsinc.query.olap.MemberSelectionExpression;
import com.successmetricsinc.query.olap.OlapMemberExpression;
import com.successmetricsinc.transformation.NoFilterValueException;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.operators.OpBinaryVarchar;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.DateUtil;
import com.successmetricsinc.util.RepositoryException;
import com.successmetricsinc.util.Util;
// import SQLinForm_200.SQLForm;

/**
 * Query generation class - general class for querying logical repository metadata and generating SQL
 * 
 * @author Brad Peters
 * 
 */
public class Query implements Externalizable, Cloneable, BasicDBObjectSerializer
{
	private static Logger logger = Logger.getLogger(Query.class);
	private static final long serialVersionUID = 2L;
	private static String variableIdentifier = "Variables";
	private static String qConstructIdentifier = "QConstructs";
	private StringBuilder query;
	private Repository r;
	private List<QueryColumn> resultProjectionList;
	public List<DimensionColumnNav> dimensionColumns;
	public List<DimensionMemberSetNav> dimensionMemberSets;
	public List<OlapMemberExpression> olapMemberExpressions;
	public List<MeasureColumnNav> measureColumns;
	public List<MeasureColumnNav> derivedMeasureColumns;
	public List<QueryFilter> filters;
	private List<OrderBy> orderByClauses;
	private List<DisplayOrder> displayOrders;
	private List<GroupBy> groupByClauses;
	public List<DimensionTable> vcDimensionTables;
	private List<Aggregation> aggregations;
	public QueryMap columnMap;
	private List<MeasureStringReplace> derivedMeasureStringReplacements;
	private List<Navigation> navList;
	public int colIndex;
	private String queryKey; // key used for looking up matching queries in the cache
	private String clearCacheKey; // key used for looking up matching queries for clearing the cache with any filter
	public boolean fullOuterJoin;
	public int topn = -1;
	public int dtopn = -1;	
	private boolean hasMeasureExpressionColumns;
	// Set ResultSetCache for caching mapped values
	ResultSetCache rsc;
	// List of physical and logical table names being used in the query
	private Set<String> physicalTableNames;
	private Set<String> logicalTableNames;
	private Session session;
	// List of bound session variables
	private Map<String, String> variableMap;
	public boolean usesLevelSpecificJoin;
	// Whether multiple sources are being used
	public boolean containsMultipleQueries;
	// Wheter there is any XMLA source being used
	public boolean requiresDispalySortingForXMLA;
	public boolean isAggregate;
	/*
	 * Is a query for a volatile cache entry (if so, ignore volatile entries
	 */
	private boolean volatileCacheQuery;
	/*
	 * Flag indicating whether a measure needs to be added to a volatile cache entry query
	 */
	private boolean addVolatileMeasure;
	/*
	 * List of columns to be mapped (for volatile cache columns)
	 */
	private List<DimensionColumnNav> mappedDimensionColumns;
	// Suffix to be added to derived query table names
	private String derivedQueryTableSuffix;
	// List of derived query tables - null if only one
	private DerivedTableQuery[] derivedTables = null;
	/*
	 * Flag of whether or not to expand derived query tables - default is true. If false, only the physical names of the
	 * derived query tables are used. It is assumed that they are physically created elsewhere
	 */
	private boolean expandDerivedQueryTables = true;
	
	private boolean simulateOuterJoinUsingPersistedDQTs = false;
	// List of columns needed to join derived tables together (one per derived table) - used for generating indices
	private List<Set<String>> whereColumns = new ArrayList<Set<String>>();
	// List of mapped columns result sets
	private List<QueryResultSet> mapResultSets = new ArrayList<QueryResultSet>();
	// Temp table map
	private Map<DatabaseConnection, List<TempTable>> tempTableMap;
	// List of strings populated for set-based security filters
	private List<String> exactListMatch;

	private DimensionColumn dcToKeepInProjectionList = null;
	
	private transient boolean isVirtualColumnQuery = false;
	private List<GroupBy> addedGroupBys = new ArrayList<GroupBy>();
	private transient boolean validateOnly = false; //do not execute query if validating physical query (i.e. do not execute MDX)
	public transient Map<DimensionColumn, Boolean> dimensionColumnIsCalculatedMember = new HashMap<DimensionColumn, Boolean>();
	private transient boolean isWrappedQueryWithOneColumn = false;
	private FederatedJoinQueryHelper fjqh = null;
	
	private static final String legal = "\\\\Q[\\[\\]\\^\\$\\|\\?\\*\\+\\(\\)\\~`\\!@#%_+{}'\"<>;,]{1,}\\\\E";
	private static final String dateRegex = "\\\\Q([0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9])\\\\E";
	// variable, same as CURRENT_MONTHID for now
	public Object clone() throws CloneNotSupportedException
	{
		Query newq = (Query) super.clone();
		if (query != null)
			newq.query = new StringBuilder(query);
		if (r != null)
			newq.r = r;
		if (resultProjectionList != null)
			newq.resultProjectionList = new ArrayList<QueryColumn>(resultProjectionList);
		if (vcDimensionTables != null)
			newq.vcDimensionTables = new ArrayList<DimensionTable>(vcDimensionTables);
		if (variableMap != null)
			newq.variableMap = new HashMap<String, String>(variableMap);
		/*
		 * Deep copy of dimension and measure columns and dimension membersets
		 */
		newq.dimensionColumns = new ArrayList<DimensionColumnNav>();
		for (DimensionColumnNav dcn : dimensionColumns)
			newq.dimensionColumns.add((DimensionColumnNav) dcn.clone());
		newq.dimensionMemberSets = new ArrayList<DimensionMemberSetNav>();
		for (DimensionMemberSetNav dms : dimensionMemberSets)
			newq.dimensionMemberSets.add((DimensionMemberSetNav) dms.clone());
		newq.olapMemberExpressions = new ArrayList<OlapMemberExpression>();
		for (OlapMemberExpression exp : olapMemberExpressions)
			newq.olapMemberExpressions.add((OlapMemberExpression) exp.clone());
		newq.measureColumns = new ArrayList<MeasureColumnNav>();
		for (MeasureColumnNav mcn : measureColumns)
			newq.measureColumns.add((MeasureColumnNav) mcn.clone());
		newq.derivedMeasureColumns = new ArrayList<MeasureColumnNav>(derivedMeasureColumns);
		/*
		 * guess we're not really doing clone here... sigh... see QueryAggregate constructor... it adds them to the
		 * clone after the clone process for (MeasureColumnNav mcn : derivedMeasureColumns)
		 * newq.derivedMeasureColumns.add((MeasureColumnNav) mcn.clone());
		 */
		newq.filters = new ArrayList<QueryFilter>(filters);
		newq.orderByClauses = new ArrayList<OrderBy>(orderByClauses);
		newq.groupByClauses = new ArrayList<GroupBy>(groupByClauses);
		newq.derivedMeasureStringReplacements = new ArrayList<MeasureStringReplace>(derivedMeasureStringReplacements);
		newq.aggregations = new ArrayList<Aggregation>(aggregations);
		newq.colIndex = colIndex;
		newq.queryKey = queryKey;
		newq.fullOuterJoin = fullOuterJoin;
		newq.columnMap = new QueryMap();
		newq.navList = new ArrayList<Navigation>(navList);
		newq.topn = topn;
		newq.hasMeasureExpressionColumns = hasMeasureExpressionColumns;
		newq.physicalTableNames = new HashSet<String>(physicalTableNames);
		newq.logicalTableNames = new HashSet<String>(logicalTableNames);
		newq.usesLevelSpecificJoin = usesLevelSpecificJoin;
		newq.processed = processed;
		if (exactListMatch != null)
		{
			newq.exactListMatch = new ArrayList<String>(exactListMatch);
		}
		newq.dcToKeepInProjectionList = dcToKeepInProjectionList;
		newq.containsMultipleQueries = containsMultipleQueries;
		newq.requiresDispalySortingForXMLA = requiresDispalySortingForXMLA;
		return (newq);
	}

	/**
	 * Read a serialized query structure
	 * 
	 * @see java.io.Externalizable#readExternal(java.io.ObjectInput)
	 */
	@SuppressWarnings("unchecked")
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException
	{
		query = new StringBuilder((String) in.readObject());
		queryKey = (String) in.readObject();
		colIndex = (Integer) in.readObject();
		dimensionColumns = (List<DimensionColumnNav>) in.readObject();
		measureColumns = (List<MeasureColumnNav>) in.readObject();
		derivedMeasureColumns = (List<MeasureColumnNav>) in.readObject();
		filters = (List<QueryFilter>) in.readObject();
		orderByClauses = (List<OrderBy>) in.readObject();
		groupByClauses = (List<GroupBy>) in.readObject();
		physicalTableNames = (Set<String>) in.readObject();
		logicalTableNames = (Set<String>) in.readObject();
		variableMap = (Map<String, String>) in.readObject();
		usesLevelSpecificJoin = (Boolean) in.readObject();
		topn = (Integer) in.readObject();
		fullOuterJoin = (Boolean) in.readObject();
		exactListMatch = (List<String>) in.readObject();
		boolean eofReached = false;
		try
		{
			containsMultipleQueries = (Boolean) in.readObject();
		} catch (OptionalDataException e)
		{
			if (!e.eof)
				throw e;
			eofReached = true;
		}
		if(!eofReached)
		{
			try
			{
				requiresDispalySortingForXMLA = (Boolean) in.readObject();
			} catch (OptionalDataException e)
			{
				if (!e.eof)
					throw e;
				eofReached = true;
			}
		}
		if(!eofReached)
		{
			try
			{
				dimensionMemberSets = (List<DimensionMemberSetNav>) in.readObject();
			} catch (OptionalDataException e)
			{
				if (!e.eof)
					throw e;
				eofReached = true;
			}
		}
		if(!eofReached)
		{
			try
			{
				olapMemberExpressions = (List<OlapMemberExpression>) in.readObject();
			} catch (OptionalDataException e)
			{
				if (!e.eof)
					throw e;
				eofReached = true;
			}
		}
		columnMap = new QueryMap();
		derivedMeasureStringReplacements = new ArrayList<MeasureStringReplace>();
		resultProjectionList = new ArrayList<QueryColumn>();
		aggregations = new ArrayList<Aggregation>();
		navList = new ArrayList<Navigation>();
	}

	/**
	 * Write a serialized version of the query structure
	 * 
	 * @see java.io.Externalizable#writeExternal(java.io.ObjectOutput)
	 */
	public void writeExternal(ObjectOutput out) throws IOException
	{
		if (query == null)
		{
			out.writeObject("");
		} else
		{
			out.writeObject(query.toString());
		}
		out.writeObject(queryKey);
		out.writeObject(colIndex);
		out.writeObject(dimensionColumns);
		out.writeObject(measureColumns);
		out.writeObject(derivedMeasureColumns);
		out.writeObject(filters);
		out.writeObject(orderByClauses);
		out.writeObject(groupByClauses);
		out.writeObject(physicalTableNames);
		out.writeObject(logicalTableNames);
		out.writeObject(variableMap);
		out.writeObject(usesLevelSpecificJoin);
		out.writeObject(topn);
		out.writeObject(fullOuterJoin);
		out.writeObject(exactListMatch);
		out.writeObject(containsMultipleQueries);
		out.writeObject(requiresDispalySortingForXMLA);
		out.writeObject(dimensionMemberSets);
		out.writeObject(olapMemberExpressions);
	}

	@SuppressWarnings("unchecked")
	public void fromBasicDBObject(BasicDBObject obj){
		
		query = new StringBuilder(obj.getString("query"));
		queryKey = obj.getString("queryKey");
		colIndex = obj.getInt("colIndex");
		try{
			dimensionColumns = MongoMgr.convertFromDBList((BasicDBList)obj.get("dimensionColumns"), DimensionColumnNav.class);
			dimensionMemberSets = MongoMgr.convertFromDBList((BasicDBList)obj.get("dimensionMemberSets"), DimensionMemberSetNav.class);
			olapMemberExpressions = MongoMgr.convertFromDBList((BasicDBList)obj.get("olapMemberExpressions"), OlapMemberExpression.class);
			measureColumns =  MongoMgr.convertFromDBList((BasicDBList)obj.get("measureColumns"), MeasureColumnNav.class);
			derivedMeasureColumns = MongoMgr.convertFromDBList((BasicDBList)obj.get("derivedMeasureColumns"), MeasureColumnNav.class);
			filters = MongoMgr.convertFromDBList((BasicDBList)obj.get("filters"), QueryFilter.class);
			orderByClauses = MongoMgr.convertFromDBList((BasicDBList)obj.get("orderByClauses"), OrderBy.class);
			groupByClauses = MongoMgr.convertFromDBList((BasicDBList)obj.get("groupByClauses"), GroupBy.class);
			exactListMatch = MongoMgr.convertFromDBList((BasicDBList)obj.get("exactListMatch"), String.class);
			physicalTableNames = MongoMgr.convertFromDBListToSet((BasicDBList)obj.get("physicalTableNames"), String.class);
			logicalTableNames  = MongoMgr.convertFromDBListToSet((BasicDBList)obj.get("logicalTableNames"), String.class);
		}catch(Exception e){
			logger.error("Fail to convert BasicDBObject", e);
		}
		
		variableMap = (Map<String, String>)obj.get("variableMap");
		usesLevelSpecificJoin = obj.getBoolean("usesLevelSpecificJoin", usesLevelSpecificJoin);
		topn = obj.getInt("topn");
		fullOuterJoin = obj.getBoolean("fullOuterJoin", fullOuterJoin);
		
		columnMap = new QueryMap();
		derivedMeasureStringReplacements = new ArrayList<MeasureStringReplace>();
		resultProjectionList = new ArrayList<QueryColumn>();
		aggregations = new ArrayList<Aggregation>();
		navList = new ArrayList<Navigation>();
	}
	
	public BasicDBObject toBasicDBObject(){
		BasicDBObject obj = new BasicDBObject();
		
		obj.put("query", query == null ? "" : query.toString());
		obj.put("queryKey", queryKey);
		obj.put("colIndex", colIndex);
		obj.put("dimensionColumns", MongoMgr.convertToDBList(dimensionColumns));
		obj.put("dimensionMemberSets", MongoMgr.convertToDBList(dimensionMemberSets));
		obj.put("olapMemberExpressions", MongoMgr.convertToDBList(olapMemberExpressions));
		obj.put("measureColumns", MongoMgr.convertToDBList(measureColumns));
		obj.put("derivedMeasureColumns", MongoMgr.convertToDBList(derivedMeasureColumns));
		obj.put("filters", MongoMgr.convertToDBList(filters));
		obj.put("orderByClauses", MongoMgr.convertToDBList(orderByClauses));
		obj.put("groupByClauses", MongoMgr.convertToDBList(groupByClauses));
		obj.put("physicalTableNames", physicalTableNames);
		obj.put("logicalTableNames", logicalTableNames);
		obj.put("variableMap", variableMap);
		obj.put("usesLevelSpecificJoin", usesLevelSpecificJoin);
		obj.put("topn", topn);
		obj.put("fullOuterJoin", fullOuterJoin);
		obj.put("exactListMatch", exactListMatch);
		
		return obj;
	}
	
	/**
	 * No-arg constructure for deserialization
	 */
	public Query()
	{
	}

	/**
	 * Create an empty query based on a given logical metadata repository
	 * 
	 * @param r
	 *            Repository structure
	 */
	public Query(Repository r)
	{
		this.r = r;
		query = null;
		columnMap = new QueryMap();
		dimensionColumns = new ArrayList<DimensionColumnNav>();
		dimensionMemberSets = new ArrayList<DimensionMemberSetNav>();
		olapMemberExpressions = new ArrayList<OlapMemberExpression>();
		measureColumns = new ArrayList<MeasureColumnNav>();
		derivedMeasureColumns = new ArrayList<MeasureColumnNav>();
		filters = new ArrayList<QueryFilter>();
		orderByClauses = new ArrayList<OrderBy>();
		groupByClauses = new ArrayList<GroupBy>();
		derivedMeasureStringReplacements = new ArrayList<MeasureStringReplace>();
		resultProjectionList = new ArrayList<QueryColumn>();
		aggregations = new ArrayList<Aggregation>();
		navList = new ArrayList<Navigation>();
		colIndex = 0;
		queryKey = null;
		fullOuterJoin = false;
		physicalTableNames = new HashSet<String>();
		logicalTableNames = new HashSet<String>();
		topn = -1;
	}

	/**
	 * Create a query key based on columns, measures, groupings, orderings and filters
	 * 
	 * @throws NavigationException
	 * @throws SessionVariableUnavailableException
	 * @throws ScriptException 
	 * @throws SecurityFilterException 
	 */
	public void createQueryKey(Session session) throws NavigationException, CloneNotSupportedException, BadColumnNameException,
			SessionVariableUnavailableException, ScriptException, SecurityFilterException
	{
		createQueryKey(session, false);
	}

	public String getClearCacheKey(Session session) throws NavigationException, CloneNotSupportedException, BadColumnNameException,
			SessionVariableUnavailableException, ScriptException, SecurityFilterException
	{
		createQueryKey(session, true);
		return clearCacheKey;
	}

	/**
	 * Create a query key based on columns, measures, groupings, orderings and filters
	 * 
	 * @throws NavigationException
	 * @throws CloneNotSupportedException
	 * @throws SessionVariableUnavailableException
	 * @throws ScriptException 
	 * @throws SecurityFilterException 
	 */
	@SuppressWarnings("unchecked")
	public void createQueryKey(Session session, boolean clearKey) throws NavigationException, CloneNotSupportedException, BadColumnNameException,
			SessionVariableUnavailableException, ScriptException, SecurityFilterException
	{
		this.session = session;
		processGenerics();
		Set<String> columns = new TreeSet<String>();
		List<String> dimMemberSets = new ArrayList<String>();
		List<String> olapMemberExpressionKeys = new ArrayList<String>();
		Set<String> measures = new TreeSet<String>();
		Set<String> filterkeys = new TreeSet<String>();
		Set<String> groupings = new TreeSet<String>();
		Set<String> orderings = new LinkedHashSet<String>();
		Set<Variable> variables = new TreeSet<Variable>();
		for (DimensionColumnNav dcn : dimensionColumns)
		{
			if (!dcn.navigateonly)
			{
				StringBuilder sb = new StringBuilder();
				if (dcn.sparse)
					sb.append('S');
				sb.append("D{");
				sb.append(dcn.dimensionName);
				sb.append("}C{");
				sb.append(dcn.columnName);
				sb.append('}');
				columns.add(sb.toString());
			}
			if (session != null && r.hasVariables(dcn.dimensionName, dcn.columnName))
			{
				/*
				 * If this column potentially contains a session variable, then navigate the query to find which
				 * physical incarnation is being used. If that version contains a session variable, then add it to
				 * the query key
				 */
				if (!hasBeenNavigated())
					this.navigateQuery(false, true, session, true);
				List<Variable> vlist = dcn.getDimensionColumn().SessionVariables;
				if (vlist != null)
					variables.addAll(vlist);
			}
		}
		for (DimensionMemberSetNav dms : dimensionMemberSets)
		{
			if (!dms.navigateonly)
			{
				String key = dms.getKey();
				if (!dimMemberSets.contains(key))
					dimMemberSets.add(key);
			}			
		}
		for (OlapMemberExpression exp : olapMemberExpressions)
		{
			String key = exp.getKey();
			if (!olapMemberExpressionKeys.contains(key))
				olapMemberExpressionKeys.add(key);						
		}
		for (MeasureColumnNav mcn : measureColumns)
		{
			if (!mcn.navigateonly && !mcn.hidden)
			{
				StringBuilder sb = new StringBuilder();
				sb.append("M{");
				sb.append(mcn.measureName);
				if (mcn.filter != null)
				{
					sb.append(',');
					sb.append(mcn.filter.getQueryKeyStr(this, variables));
				}
				sb.append('}');
				measures.add(sb.toString());
			}
			if (session != null && r.hasVariables(null, mcn.measureName))
			{
				/*
				 * If this column potentially contains a session variable, then navigate the query to find which
				 * physical incarnation is being used. If that version contains a session variable, then add it to
				 * the query key
				 */
				if (!hasBeenNavigated())
					this.navigateQuery(false, true, session, true);
				if (mcn.matchingMeasures.size() > 0)
				{
					List<Variable> vlist = mcn.getMeasureColumn().SessionVariables;
					if (vlist != null)
						variables.addAll(vlist);
				}
			}
		}
		if (!hasBeenNavigated())
		{
			navigateQuery(false, true, session, true);
		}
		Map<String, Object> map = getVariablesAndQConstructsFromNavList(session);
		List<Variable> vlist = (List<Variable>) map.get(variableIdentifier);
		List<String> qConstructs = (List<String>) map.get(qConstructIdentifier);
		if (vlist != null)
		{
			variables.addAll(vlist);
		}
		if (r.joinContainsVariable)
		{
			List<Variable> variablesInJoinsList = getVariablesInJoinsFromNavList(session);
			if (variablesInJoinsList != null)
			{
				variables.addAll(variablesInJoinsList);
			}
		}
		for (MeasureColumnNav mcn : derivedMeasureColumns)
		{
			StringBuilder sb = new StringBuilder();
			sb.append("M{");
			sb.append(mcn.measureName);
			sb.append('}');
			measures.add(sb.toString());
			if (session != null && mcn.sessionVariables != null)
			{
				variables.addAll(mcn.sessionVariables);
			}
		}
		for (QueryFilter qf : filters)
		{
			String f = qf.getQueryKeyStr(this, variables);
			if (f != null)
				filterkeys.add(f);
		}
		for (OrderBy ob : orderByClauses)
		{
			orderings.add(ob.getQueryKeyStr(this));
		}
		for (GroupBy gb : groupByClauses)
		{
			groupings.add(gb.getQueryKeyStr(this));
		}
		StringBuilder qk = new StringBuilder();
		if (fullOuterJoin)
			qk.append("FOJ{}");
		for (String column : columns)
			qk.append(column);
		for (String dimMemberSet : dimMemberSets)
			qk.append(dimMemberSet);
		for (String olapExprKey : olapMemberExpressionKeys)
			qk.append(olapExprKey);
		for (String measure : measures)
			qk.append(measure);
		for (String grouping : groupings)
			qk.append(grouping);
		for (String ordering : orderings)
			qk.append(ordering);
		qk.append('|');
		// for cache clearing keys, put in regular expressions to allow matching of any filters
		StringBuilder qkfilter = new StringBuilder();
		boolean fand = false;
		if (clearKey)
			qkfilter.append(".*");
		else if (filterkeys.size() > 1)
		{
			// Multiple keys should match an and
			fand = true;
			qkfilter.append("FAND{");
		}
		boolean first = true;
		for (String filterkey : filterkeys)
		{
			if (clearKey)
			{
				qkfilter.append(Pattern.quote(filterkey));
				qkfilter.append(".*");
			} else
			{
				if (fand && !first)
					qkfilter.append(',');
				qkfilter.append(filterkey);
			}
			first = false;
		}
		if (fand)
			qkfilter.append('}');
		StringBuilder qkend = new StringBuilder();
		qkend.append('|');
		if (topn >= 0)
		{
			qkend.append("T{");
			qkend.append(topn);
			qkend.append('}');
		}
		variableMap = new HashMap<String, String>();
		if (session != null)
		{
			for (Variable v : variables)
			{
				Object valueObject = session.getSessionVariableValue(v.getName(), r);
				if (valueObject == null)
				{
					// ignore getPromptValue that doesn't have a value
					if (v.getName().startsWith("P['"))
						continue;
					
					logger.warn("Query : Session Variable is not available : " + v.getName());
					throw new SessionVariableUnavailableException(v.getName());
				}
				String value = valueObject.toString();
				qkend.append("V{");
				qkend.append(v.getName());
				qkend.append('=');
				qkend.append(value);
				qkend.append('}');
				variableMap.put(v.getName(), value);
			}
		}
		exactListMatch = new ArrayList<String>();
		if (qConstructs != null && qConstructs.size() > 0)
		{
			qkend.append("|");
			qkend.append(qConstructs.toString());
			exactListMatch.addAll(qConstructs);
		}
		if (clearKey)
		{
			clearCacheKey = Pattern.quote(qk.toString()) + qkfilter.toString() + Pattern.quote(qkend.toString());
		} else
		{
			qk.append(qkfilter);
			qk.append(qkend);
			queryKey = qk.toString();
		}
	}
	
	private List<Variable> getVariablesInJoinsFromNavList(Session userSession)
	{
		List<Variable> response = new ArrayList<Variable>();
		for (Navigation nav : navList)
		{
			MeasureColumnNav mcNav = nav.getMeasureColumnNav();
			if (mcNav != null && mcNav.pick >= 0 && mcNav.matchingMeasures != null && mcNav.matchingMeasures.size() > mcNav.pick && mcNav.matchingMeasures.get(mcNav.pick) != null)
			{
				List<DimensionTable> dimTables = new ArrayList<DimensionTable>();
				for (DimensionColumnNav dcn : nav.getDimColumns())
				{
					if (dcn.pick >= 0)
					{
						dimTables.add(dcn.matchingColumns.get(dcn.pick).DimensionTable);
					}
				}
				MeasureTable mt = mcNav.matchingMeasures.get(mcNav.pick).MeasureTable;
				List<Join> joinList = r.getJoinList(mt.TableName);
				for (Join j : joinList)
				{
					if (mt.TableName.equals(j.Table1Str))
					{
						//Measure table on left
						for (DimensionTable dt : dimTables)
						{
							if (dt.TableName.equals(j.Table2Str))
							{
								addVariableIfJoinContainsVariables(userSession, j.JoinCondition, response);								
							}
						}						
					}
					else if (mt.TableName.equals(j.Table2Str))
					{
						//Measure table on right
						for (DimensionTable dt : dimTables)
						{
							if (dt.TableName.equals(j.Table1Str))
							{
								addVariableIfJoinContainsVariables(userSession, j.JoinCondition, response);								
							}
						}						
					}
				}
			}
			if (nav.getDimensionTables() != null)  
			{  
				for (DimensionTable dt : nav.getDimensionTables())  
			 	{  
					if (dt.snowflake && dt.TableSource != null && dt.TableSource.Tables != null && dt.TableSource.Tables.length > 1)  
					{  
						//check if snowflake join condition contains variables  
						for (TableDefinition td : dt.TableSource.Tables)  
						{  
							if (td.JoinClause != null && !td.JoinClause.trim().isEmpty())  
							{  
								if (r.containsVariable(td.JoinClause))  
								{  
									addVariableIfJoinContainsVariables(userSession, td.JoinClause, response);	  
								}  
							}  
						}  
					}  
			 	}
			}
		}
		return response;
	}
	
	private boolean addVariableIfJoinContainsVariables(Session session, String joinCondition, List<Variable> response)
	{
		String replacedJoin = r.replaceVariables(session, joinCondition);
		boolean foundVariables = false;
		if (joinCondition != null && !joinCondition.equals(replacedJoin))
		{
			int searchStartIndex = 0;
			boolean found;
			do
			{
				found = false;
				int variableStartIndex = joinCondition.indexOf("V{", searchStartIndex);
				if (variableStartIndex >= 0)
				{
					int variableEndIndex = joinCondition.indexOf('}', variableStartIndex);
					if (variableEndIndex > variableStartIndex)
					{
						String variableName = joinCondition.substring(variableStartIndex + 2, variableEndIndex);
						Variable v = r.findVariable(variableName);
						if (v != null)
						{
							response.add(v);
							found = true;
							foundVariables = true;
							searchStartIndex = variableEndIndex + 1;
						}
					}
				}
			} while (found);
		}
		return foundVariables;
	}

	/*
	 * Security Filter is set at the Table Source Level. Iterate over all the table source of dimension and measure
	 * tables Extract all the variables from all the security filters and return as a variable list Variable list will
	 * be part of query key. Applicable for both variable type and set-based filters
	 */
	@SuppressWarnings("unchecked")
	private Map<String, Object> getVariablesAndQConstructsFromNavList(Session userSession) throws SecurityFilterException
	{
		Map<String, Object> response = new HashMap<String, Object>();
		List<Variable> vars = new ArrayList<Variable>();
		List<String> qConstructs = new ArrayList<String>();
		for (Navigation nav : navList)
		{
			for (DimensionTable dt : nav.getDimensionTables())
			{
				Map<String, Object> dtVarsQConstructs = getVariablesAndQConstructsFromSecurityFilter(dt.TableSource, userSession, true, dt.Imported);
				if (dtVarsQConstructs != null)
				{
					List<Variable> dimVarList = (List<Variable>) dtVarsQConstructs.get(variableIdentifier);
					List<String> dimConstructList = (List<String>) dtVarsQConstructs.get(qConstructIdentifier);
					if (dimVarList != null)
					{
						vars.addAll(dimVarList);
					}
					if (dimConstructList != null)
					{
						qConstructs.addAll(dimConstructList);
					}
				}
			}
			for (MeasureTable mt : nav.getMeasureTables())
			{
				Map<String, Object> mtVarsQConstructs = getVariablesAndQConstructsFromSecurityFilter(mt.TableSource, userSession, true, mt.Imported);
				if (mtVarsQConstructs != null)
				{
					List<Variable> mtVarList = (List<Variable>) mtVarsQConstructs.get(variableIdentifier);
					List<String> mtConstructList = (List<String>) mtVarsQConstructs.get(qConstructIdentifier);
					if (mtVarList != null)
					{
						vars.addAll(mtVarList);
					}
					if (mtConstructList != null)
					{
						qConstructs.addAll(mtConstructList);
					}
				}
			}
		}
		if (!vars.isEmpty())
		{
			response.put(variableIdentifier, vars);
		}
		if (!qConstructs.isEmpty())
		{
			response.put(qConstructIdentifier, qConstructs);
		}
		return response;
	}
	
	public Map<String, Object> getVariablesAndQConstructsFromSecurityFilter(TableSource tableSource, Session userSession, 
			boolean deepCopy, boolean importedTable) throws SecurityFilterException
	{
		Map<String, Object> response = new HashMap<String, Object>();
		List<Variable> variables = new ArrayList<Variable>();
		List<String> qConstructs = new ArrayList<String>();
		if (tableSource != null && tableSource.Filters != null && tableSource.Filters.length > 0)
		{
			TableSourceFilter[] tableSourceFilters = tableSource.Filters;
			for (TableSourceFilter tableSourceFilter : tableSourceFilters)
			{
				// check for any exclusion group on filter with the logged in user groups
				if (!applySecurityFilter(r, tableSourceFilter, userSession, importedTable, isAggregate))
				{
					// user is part of the group that is excluded for filter
					// move to the next filter
					continue;
				}
				if (tableSourceFilter.SecurityFilter)
				{
					String filter = tableSourceFilter.Filter;
					// For new query langugage, replace GetVariable('') with 'V{}' to create query key 
					// with right variable dependent values
					filter = replaceGetVariableWithOldSyntax(filter);
					// if filter contains Logical Query , fill the variables first and extract out the QConstruct
					// otherwise its a variable based security filter, extract out the variables
					int qConstructBeginIndex = filter.indexOf("Q{");
					if (qConstructBeginIndex > 0)
					{
						filter = r.replaceVariables(userSession, filter);
						// calcualte end index after replace variables since that case change
						int qConstructEndIndex = filter.lastIndexOf('}');						
						filter = filter.substring(qConstructBeginIndex + 2, qConstructEndIndex);
						qConstructs.add(filter);
						// need to navigate and recursively get the underlying qconstructs or variables
						if(deepCopy)
						{
							fillDeepCopyQConstructsAndVariables(filter, variables, qConstructs, userSession);
						}
					} else
					{						
						// variable type security filter. Extract all the variables						
						int variableStartIndex = filter.indexOf("V{");
						if (variableStartIndex >= 0)
						{
							int variableEndIndex = filter.indexOf('}', variableStartIndex);
							if (variableEndIndex > variableStartIndex)
							{
								String variableName = filter.substring(variableStartIndex + 2, variableEndIndex);
								Variable v = r.findVariable(variableName);
								if (v != null)
								{
									variables.add(v);
								}
							}
						}						
					}
				}
			}
		}
		if (!variables.isEmpty())
		{
			response.put(variableIdentifier, variables);
		}
		if (!qConstructs.isEmpty())
		{
			response.put(qConstructIdentifier, qConstructs);
		}
		return response;
	}
	
	/**
	 * This returns all the security filter Strings by traversing through all the levels
	 * E.g. A given set-based security filter query might point to other set of dim table/measure table 
	 * with security filters on them and so on.
	 * @param logicalQuery
	 * @param variables
	 * @param qConstructs
	 * @param userSession
	 * @throws SecurityFilterException 
	 */
	private void fillDeepCopyQConstructsAndVariables(String logicalQuery, List<Variable> variables, List<String> qConstructs, Session userSession) throws SecurityFilterException
	{
		List<DimensionTable> aggregatedDimTables = new ArrayList<DimensionTable>();
		List<MeasureTable> aggregatedMeasureTables = new ArrayList<MeasureTable>();
		Map<String, Integer> aggregatedDimTableNamesMap = new HashMap<String, Integer>();
		Map<String, Integer> aggregatedMeasureTableNamesMap = new HashMap<String, Integer>();
		getAggregatedDimAndMeasureTables(logicalQuery, aggregatedDimTables, aggregatedMeasureTables, aggregatedDimTableNamesMap, aggregatedMeasureTableNamesMap);
		for (DimensionTable dt : aggregatedDimTables)
		{
			Map<String, Object> dtVarsQConstructs = getVariablesAndQConstructsFromSecurityFilter(dt.TableSource, userSession, false, dt.Imported);
			if (dtVarsQConstructs != null)
			{
				@SuppressWarnings("unchecked")
				List<Variable> dimVarList = (List<Variable>) dtVarsQConstructs.get(variableIdentifier);
				@SuppressWarnings("unchecked")
				List<String> dimConstructList = (List<String>) dtVarsQConstructs.get(qConstructIdentifier);
				if (dimVarList != null)
				{
					variables.addAll(dimVarList);
				}
				if (dimConstructList != null)
				{
					qConstructs.addAll(dimConstructList);
				}
			}
		}
		for (MeasureTable mt : aggregatedMeasureTables)
		{
			Map<String, Object> mtVarsQConstructs = getVariablesAndQConstructsFromSecurityFilter(mt.TableSource, userSession, false, mt.Imported);
			if (mtVarsQConstructs != null)
			{
				@SuppressWarnings("unchecked")
				List<Variable> mtVarList = (List<Variable>) mtVarsQConstructs.get(variableIdentifier);
				@SuppressWarnings("unchecked")
				List<String> mtConstructList = (List<String>) mtVarsQConstructs.get(qConstructIdentifier);
				if (mtVarList != null)
				{
					variables.addAll(mtVarList);
				}
				if (mtConstructList != null)
				{
					qConstructs.addAll(mtConstructList);
				}
			}
		}
	}

	private static final int COUNT_DIM_TABLE_CIRCURLAR_REFERENCE = 30;
	private static final int COUNT_MEASURE_TABLE_CIRCURLAR_REFERENCE = 30;
	/**
	 * This navigates the logical query, retrieves the dimension and measure table with security filter on them and fill up
	 * the aggreatedMeasureTables and aggregatedDimTables (Recursively). 
	 * The aggregatedDimTableNames and  aggregatedMeasureTableNames are used to detect any circular reference during 
	 * in the security filter set-based query
	 * @param logicalQuery
	 * @param aggregatedDimTables
	 * @param aggreatedMeasureTables
	 * @param aggregatedDimTableNames
	 * @param aggregatedMeasureTableNames
	 * @throws SecurityFilterException 
	 */		
	private void getAggregatedDimAndMeasureTables(String logicalQuery, 
			List<DimensionTable> aggregatedDimTables, List<MeasureTable> aggreatedMeasureTables,
			Map<String, Integer> aggregatedDimTableNamesMap, Map<String, Integer> aggregatedMeasureTableNamesMap) throws SecurityFilterException
	{
			logicalQuery = QueryString.preProcessQueryString(logicalQuery, r, session);
			AbstractQueryString queryString = AbstractQueryString.getInstance(r, logicalQuery);
		try
		{
			Query query = queryString.getQuery();
			query.navigateQuery(true, true, session, true);
			
			List<Navigation> localNavList = query.getNavigationList();
			List<TableSourceFilter> localtableSourceFilters = new ArrayList<TableSourceFilter>();
			
			List<String> localDimTableNames = new ArrayList<String>();
			List<String> localMeasureTableNames = new ArrayList<String>();
			List<DimensionTable> localDimTables = new ArrayList<DimensionTable>();
			List<MeasureTable> localMeasureTables = new ArrayList<MeasureTable>();
			for (Navigation localNav : localNavList)
			{	
				for (DimensionTable dt : localNav.getDimensionTables())
				{	
					boolean toAddDimTable = false;
					if(dt.TableSource != null && dt.TableSource.Filters != null && dt.TableSource.Filters.length > 0)
					{
						for(TableSourceFilter dimTsf : dt.TableSource.Filters)
						{
							if(!dimTsf.SecurityFilter)
							{
								continue;
							}
							// check for any exclusion group on filter with the logged in user groups
							if (!applySecurityFilter(r, dimTsf, session, dt.Imported, isAggregate))
							{
								// user is part of the group that is excluded for filter
								// move to the next filter
								continue;
							}
							toAddDimTable = true;
							localtableSourceFilters.add(dimTsf);
						}
					}
					if(toAddDimTable && !localDimTableNames.contains(dt.TableName))
					{
						localDimTableNames.add(dt.TableName);
						localDimTables.add(dt);
					}	
				}
				
				for (MeasureTable mt : localNav.getMeasureTables())
				{
					boolean toAddMeasureTable = false;
					if(mt.TableSource != null && mt.TableSource.Filters != null && mt.TableSource.Filters.length > 0)
					{
						for(TableSourceFilter mtTsf : mt.TableSource.Filters)
						{
							if(!mtTsf.SecurityFilter)
							{
								continue;
							}
							// check for any exclusion group on filter with the logged in user groups
							if (!applySecurityFilter(r, mtTsf, session, mt.Imported, isAggregate))
							{
								// user is part of the group that is excluded for filter
								// move to the next filter
								continue;
							}
							toAddMeasureTable = true;
							localtableSourceFilters.add(mtTsf);
						}
					}
					
					if(toAddMeasureTable && !localMeasureTableNames.contains(mt.TableName))
					{
						localMeasureTableNames.add(mt.TableName);
						localMeasureTables.add(mt);
					}
				}
			}
			
			if(localDimTables.isEmpty() && localMeasureTables.isEmpty())
			{
				return;
			}
			
			// Iterate over the local Dim and Measure tables & compare with the input list
			// this detects any circular reference
			for(String localDimTableName : localDimTableNames)
			{
				if(!aggregatedDimTableNamesMap.containsKey(localDimTableName))
				{
					aggregatedDimTableNamesMap.put(localDimTableName, 1);
				}
				else
				{
					Integer dimTablecount = aggregatedDimTableNamesMap.get(localDimTableName);
					if(dimTablecount >= Query.COUNT_DIM_TABLE_CIRCURLAR_REFERENCE)
					{
						// throws exception detecting circular reference
						throw new SecurityFilterException("Circular reference detected in security filter configuration : Dimension Table " + localDimTableName);
					}
					else
					{
						dimTablecount++;
						aggregatedDimTableNamesMap.put(localDimTableName, dimTablecount);
					}
				}
			}
			
			for(String localMeasureTableName : localMeasureTableNames)
			{
				if(!aggregatedMeasureTableNamesMap.containsKey(localMeasureTableName))
				{
					aggregatedMeasureTableNamesMap.put(localMeasureTableName, 1);
				}
				else
				{
					Integer measureTableCount = aggregatedMeasureTableNamesMap.get(localMeasureTableName);
					if(measureTableCount >= Query.COUNT_MEASURE_TABLE_CIRCURLAR_REFERENCE)
					{
						// throws exception detecting circular reference
						throw new SecurityFilterException("Circular reference detected in security filter configuration : Measure Table " + localMeasureTableName);
					}
					else
					{	
						measureTableCount++;
						aggregatedMeasureTableNamesMap.put(localMeasureTableName, measureTableCount);
					}
				}
			}
			
			aggregatedDimTables.addAll(localDimTables);
			aggreatedMeasureTables.addAll(localMeasureTables);
		
			for (TableSourceFilter tableSourceFilter : localtableSourceFilters)
			{
				String filterString = tableSourceFilter.Filter;
				int qConstructBeginIndex = filterString.indexOf("Q{");
				if(filterString.indexOf("Q{") > 0)
				{
					// strip out to get logical query
					int qConstructEndIndex = filterString.lastIndexOf('}');					
					filterString = filterString.substring(qConstructBeginIndex + 2, qConstructEndIndex);
					getAggregatedDimAndMeasureTables(filterString, aggregatedDimTables, aggreatedMeasureTables, aggregatedDimTableNamesMap, aggregatedMeasureTableNamesMap );
				}
				else 
				{
					return;
				}
			}
		}
		catch(Exception ex)
		{	
			if(ex instanceof SecurityFilterException)
			{
				throw new SecurityFilterException(ex.getMessage());
			}
			else
			{
				logger.error(ex, ex);
				throw new SecurityFilterException();
			}
		}
	}
	
	private String replaceGetVariableWithOldSyntax(String str)
	{	
		String modifiedString = str;
		if (this.r != null && r.isUseNewQueryLanguage())
		{
			if (modifiedString != null && modifiedString.length() > 0)
			{
				boolean found = true;
				while (found)
				{
					String getVariableStringBegin = "GETVARIABLE('";
					String getVariableStringEnd = "')";
					int startIndex = modifiedString.toUpperCase().indexOf(getVariableStringBegin);
					if (startIndex == -1)
					{
						found = false;
						break;
					} else
					{
						int endIndex = modifiedString.toUpperCase().indexOf(getVariableStringEnd, startIndex);
						if (endIndex > 0)
						{
							String preGetVariable = modifiedString.substring(0, startIndex);
							String getVariable = modifiedString.substring(startIndex+ getVariableStringBegin.length(), endIndex);
							String postGetVariable = modifiedString.substring(endIndex + getVariableStringEnd.length());
							modifiedString = preGetVariable + "'V{"	+ getVariable + "}'" + postGetVariable;
						} else
						{
							found = false;
							break;
						}
					}
				}
			}
		}
		return modifiedString;
	}

	// see if the groups associated with the security filter are part of the user groups
	// if yes, do not apply -- groups are exclusion only
	public static boolean applySecurityFilter(Repository r, TableSourceFilter filter, Session userSession, boolean imported , boolean isAggregate)
	{
		// making sure we are talking about security filter only
		// if other filter, we do not care
		if (!filter.SecurityFilter)
		{
			return true;
		}
		
		if(imported)
		{
			return true;
		}
		boolean allowed = true;
		List<Group> userGroups = r.getUserGroups(userSession);
		if (userGroups != null && userGroups.size() > 0)
		{
			List<String> userGroupNames = new ArrayList<String>(userGroups.size());
			for (Group g : userGroups)
			{
				if (g.isInternalGroup() && g.getName().equals("OWNER$"))
				{
					userGroups.clear();
					// if owner, do not apply security filter
					allowed = false;
					break;
				}
				// adding groups which can be modified/created by the end user
				// internal groups are not exposed
				if (!g.isInternalGroup())
				{
					userGroupNames.add(g.getName());
				}
			}
			if (filter.FilterGroups != null && filter.FilterGroups.length > 0)
			{
				for (String filterGroup : filter.FilterGroups)
				{
					if (userGroupNames.contains(filterGroup))
					{
						// exclusion rule. If the user is part of excluded filter group
						// don't apply the filter
						allowed = false;
						break;
					}
				}
			}
		} else
			/*
			 * Probably should always return false if there is no session, but this way just in case we apply a bad
			 * filter to ensure no data comes back
			 */
			return !isAggregate;
		return allowed;
	}

	/**
	 * Clear the query key
	 */
	public void clearKey()
	{
		queryKey = null;
		processed = false;
	}

	/**
	 * Return the query key for fast hash table lookup
	 * 
	 * @param session
	 *            Current session to use when generating query key (ignored if key is already generated). If null,
	 *            ignored, but does not result in query keys that take session variables into account
	 * @return
	 * @throws NavigationException
	 * @throws SessionVariableUnavailableException
	 * @throws ScriptException 
	 */
	public String getKey(Session session) throws BaseException, CloneNotSupportedException
	{
		if (queryKey == null)
		{
			createQueryKey(session);
		}
		return queryKey;
	}

	/**
	 * Returns whether the query has been navigated or not
	 * 
	 * @return
	 */
	protected boolean hasBeenNavigated()
	{
		return (navList.size() > 0);
	}

	/**
	 * Clear any previous navigation done to this query
	 */
	public void clearNavigation()
	{
		navList = new ArrayList<Navigation>();
		if(orderByClauses != null && orderByClauses.size() > 0)
		{
			for(OrderBy ob : orderByClauses)
			{
				ob.clearNavigation();
			}
		}
	}

	/**
	 * Returns the query string equivalent for this query
	 * 
	 * @param substitutePrompts
	 *            Whether to substitute prompts for columns in promptable filters
	 * @param expressionList
	 *            List of display expressions (need to include to ensure the query columns are in the proper order)
	 * @return
	 */
	private String getClassicLogicalQueryString(boolean substitutePrompts, List<DisplayExpression> expressionList)
	{
		StringBuilder query2 = new StringBuilder();
		List<String> tokenList = new ArrayList<String>(colIndex);
		for (int i = 0; i < colIndex; i++)
		{
			for (DimensionColumnNav dn : dimensionColumns)
			{
				if (!dn.navigateonly)
					if (dn.colIndex == i)
					{
						if (!dn.constant)
							tokenList.add("DC{" + dn.dimensionName + "." + dn.columnName + "}" + ((dn.displayName != null) ? dn.displayName : ""));
						else
							tokenList.add("CONST{" + dn.constantString + "}" + ((dn.displayName != null) ? dn.displayName : ""));
					}
			}
			for (MeasureColumnNav mn : measureColumns)
			{
				if (!mn.navigateonly)
				{
					if (mn.colIndex == i)
					{
						if (mn.filter == null)
						{
							tokenList.add("M{" + mn.measureName + "}" + ((mn.displayName != null) ? mn.displayName : ""));
						} else
						{
							tokenList.add("M{" + mn.measureName + "," + mn.filter.getQueryString(substitutePrompts) + "}"
									+ ((mn.displayName != null) ? mn.displayName : ""));
						}
					}
				}
			}
			for (MeasureColumnNav mn : derivedMeasureColumns)
			{
				if (!mn.navigateonly)
					if (mn.colIndex == i)
					{
						tokenList.add("M{" + mn.measureName + "}" + ((mn.displayName != null) ? mn.displayName : ""));
					}
			}
		}
		if (expressionList != null)
			for (DisplayExpression de : expressionList)
			{
				tokenList.add(de.getQueryString());
			}
		if (topn >= 0)
			tokenList.add(0, "TOP{" + topn + "}");
		for (QueryFilter qf : filters)
		{
			tokenList.add(qf.getQueryString(substitutePrompts));
		}
		for (OrderBy ob : orderByClauses)
		{
			tokenList.add(ob.getClassicOrderByQueryString());
		}
		boolean first = true;
		for (String s : tokenList)
		{
			if (s == null)
			{
				logger.debug("null token in query string, probably a bug, query so far: " + query2.toString());
				continue;
			}
			if (first)
				first = false;
			else
				query2.append(',');
			query2.append(s);
		}
		if (fullOuterJoin && (tokenList.size() > 0))
			query2.insert(0, "OUTERJOIN{},");
		return (query2.toString());
	}

	/**
	 * Returns the logical query string equivalent for this query
	 * 
	 * @param substitutePrompts
	 *            Whether to substitute prompts for columns in promptable filters
	 * @param expressionList
	 *            List of display expressions (need to include to ensure the query columns are in the proper order)
	 * @return
	 * @throws ScriptException 
	 */
	public String getLogicalQueryString(boolean substitutePrompts, List<DisplayExpression> expressionList, boolean navigate) throws ScriptException
	{
		return getLogicalQueryString(substitutePrompts, expressionList, navigate, false);
	}
	
	public String getLogicalQueryString(boolean substitutePrompts, List<DisplayExpression> expressionList, boolean navigate, boolean isSubReport) throws ScriptException
	{
		if (r.isUseNewQueryLanguage())
		{
			if (navigate)
			{
				try
				{
					// must extract the expressions before validating a query
					if (expressionList != null)
					{
						List<String> addedNames = new ArrayList<String>();
						for (DisplayExpression de : expressionList)
						{
							de.extractAllExpressions(this, addedNames);
						}
					}
					navigateQuery(false, true, null, true); // try to force the type information
				}
				catch (Exception ex)
				{
					// logger.debug(ex, ex);
				}
			}
			return getNewLogicalQueryString(substitutePrompts, expressionList, isSubReport);
		} else
		{
			return getClassicLogicalQueryString(substitutePrompts, expressionList);
		}
	}

	/*
	 * public for now, to help in translation
	 */
	public String getNewLogicalQueryString(boolean substitutePrompts, List<DisplayExpression> expressionList) throws ScriptException
	{
		return getNewLogicalQueryString(substitutePrompts, expressionList, false);
	}

	private String getNewLogicalQueryString(boolean substitutePrompts, List<DisplayExpression> expressionList, boolean isSubReport) throws ScriptException
	{
		StringBuilder query2 = new StringBuilder("SELECT ");
		List<String> tokenList = new ArrayList<String>(colIndex);
		if (topn >= 0)
			query2.append("TOP " + topn + " ");
		if (fullOuterJoin)
			query2.append("USING OUTER JOIN ");
		for (int i = 0; i < colIndex; i++)
		{
			for (DimensionColumnNav dn : dimensionColumns)
			{
				if (!dn.navigateonly)
					if (dn.colIndex == i)
					{
						if (!dn.constant)
							tokenList.add("[" + dn.dimensionName + "." + dn.columnName + "]" + ((dn.displayName != null) ? " '" + dn.displayName + "'" : ""));
						else
							tokenList.add("'" + dn.constantString + "'" + ((dn.displayName != null) ? " '" + dn.displayName + "'" : ""));
					}
			}
			for (DimensionMemberSetNav dms : dimensionMemberSets)
			{
				if (!dms.navigateonly)
					if (dms.colIndex == i)
					{
						tokenList.add("[" + dms.dimensionName + ".{" + dms.getMemberExpressionsString() + "}]" + ((dms.displayName != null) ? " '" + dms.displayName + "'" : ""));
						if (dms.parentDisplayName != null) {
							tokenList.add("[" + dms.dimensionName + ".{SELECTED('" + dms.parentDisplayName + "')}] '" + dms.parentDisplayName + "'");
						}
						if (dms.parentNameDisplayName != null) {
							tokenList.add("[" + dms.dimensionName + ".{SELECTED('" + dms.parentNameDisplayName + "')}] '" + dms.parentNameDisplayName + "'");
						}
						if (dms.ordinalDisplayName != null) {
							tokenList.add("[" + dms.dimensionName + ".{SELECTED('" + dms.ordinalDisplayName + "')}] '" + dms.ordinalDisplayName + "'");
						}
						if (dms.uniqueNameDisplayName != null) {
							tokenList.add("[" + dms.dimensionName + ".{SELECTED('" + dms.uniqueNameDisplayName + "')}] '" + dms.uniqueNameDisplayName + "'");
						}
					}
			}
			for (OlapMemberExpression exp : olapMemberExpressions)
			{
				if (exp.colIndex == i)
				{
					String expression = exp.expression;
					if (expression.startsWith("'") && expression.endsWith("'"))
						expression = expression.substring(1, expression.length() - 1);
					tokenList.add("MDXExpression('" + expression + "')" + (exp.displayName != null ? " '" + exp.displayName + "'": ""));
				}
			}
			for (MeasureColumnNav mn : measureColumns)
			{
				if (!mn.navigateonly)
				{
					if (mn.colIndex == i)
					{
						if (mn.filter == null)
						{
							tokenList.add("[" + mn.measureName + "]" + ((mn.displayName != null) ? " '" + mn.displayName + "'" : ""));
						} else
						{
							tokenList.add("[" + mn.measureName + "] WHERE " + mn.filter.getNewQueryString(substitutePrompts)
									+ ((mn.displayName != null) ? " '" + mn.displayName + "'" : ""));
						}
					}
				} 
			}
			for (MeasureColumnNav mn : derivedMeasureColumns)
			{
				if (!mn.navigateonly)
					if (mn.colIndex == i)
					{
						tokenList.add("[" + mn.measureName + "]" + ((mn.displayName != null) ? " '" + mn.displayName + "'" : ""));
					}
			}
		}
		boolean containsOlapExpression = false;
		if (expressionList != null)
			for (DisplayExpression de : expressionList)
			{
				if (de.containsOlapMemberSet())
					containsOlapExpression = true;
				tokenList.add(de.getNewQueryString());
			}
		boolean first = true;
		for (String s : tokenList)
		{
			if (s == null)
			{
				logger.debug("null token in query string, probably a bug, query so far: " + query2.toString());
				continue;
			}
			if (first)
				first = false;
			else
				query2.append(',');
			query2.append(s);
		}
		if (dimensionMemberSets.size() > 0 || olapMemberExpressions.size() > 0 || containsOlapExpression)
			query2.append(" FROM [CUBE]");
		else	
			query2.append(" FROM [ALL]");
		first = true;
	
		if(filters != null ){
			for (QueryFilter qfr : filters)
			{
				if (first)
				{
					query2.append(" WHERE ");
					first = false;
				} else
				{
					query2.append(" AND ");
				}
				if (dimensionMemberSets.size() > 0 || olapMemberExpressions.size() > 0 || containsOlapExpression) {
					if (qfr.getType() == QueryFilter.TYPE_PREDICATE && qfr.getColumnType() == QueryFilter.TYPE_DIMENSION_COLUMN &&
							qfr.getHierarchy() != null && qfr.getHierarchy().trim().length() > 0) {
						boolean found = false;
						String name = qfr.getDimension() + '-' + qfr.getHierarchy();
						if (dimensionMemberSets.size() > 0) {
							for (DimensionMemberSetNav dms : dimensionMemberSets) {
								if (name.equals(dms.dimensionName)) {
									qfr = new QueryFilter(name, dms.displayName, qfr.getOperator(), 
											QueryFilter.quoteOperand(qfr.getOperand(), "Varchar", isSubReport), qfr.getColumnType());
									found = true;
									break;
								}
							}
						}
						
						if (found == false && olapMemberExpressions.size() > 0) {
							///
						}
					}
					else if (qfr.getType() == QueryFilter.TYPE_LOGICAL_OP) {
						List<QueryFilter> newFilterList = new ArrayList<QueryFilter>();
						for (QueryFilter qf1 : qfr.getFilterList()) {
							if (qf1.getHierarchy() == null || qf1.getHierarchy().trim().length() == 0) {
								newFilterList.add(qf1);
							}
							else {
								String name = qf1.getDimension() + '-' + qf1.getHierarchy();
								boolean found = false;
								if (dimensionMemberSets.size() > 0) {
									for (DimensionMemberSetNav dms : dimensionMemberSets) {
										if (name.equals(dms.dimensionName)) {
											newFilterList.add(new QueryFilter(name, dms.displayName, qf1.getOperator(), 
													QueryFilter.quoteOperand(qf1.getOperand(), "Varchar", isSubReport), qf1.getColumnType()));
											found = true;
											break;
										}
									}
									
									if (!found)
										newFilterList.add(qf1);
								}
							}
						}
						
						qfr = new QueryFilter(qfr.getOperator(), newFilterList);
		
					}
				}
				query2.append(qfr.getNewQueryString(substitutePrompts, isSubReport));
			}					
		}		
			
			
		first = true;
		for (OrderBy ob : orderByClauses)
		{
			if (first)
			{
				query2.append(" ORDER BY ");
				first = false;
			} else
			{
				query2.append(",");
			}
			query2.append(ob.getNewOrderByQueryString());
		}
		return (query2.toString());
	}	
	
	/**
	 * Returns the query parameter names and default values
	 * 
	 * @return
	 */
	public Map<String, Object> getFilterParameterMap()
	{
		HashMap<String, Object> parameterMap = new HashMap<String, Object>();
		for (MeasureColumnNav mc : measureColumns)
		{
			if (mc != null && mc.filter != null)
			{
				mc.filter.getParameterMap(parameterMap);
			}
		}
		for (QueryFilter qf : filters)
		{
			qf.getParameterMap(parameterMap);
		}
		return (parameterMap);
	}

	protected static DimensionColumnNav findDimensionColumnNav(List<DimensionColumnNav> dimensionColumns, String dname, String cname)
	{
		if (dname == null || cname == null)
			return null;
		for (DimensionColumnNav dcn : dimensionColumns)
		{
			if (dcn.dimensionName == null || dcn.columnName == null)
				continue;
			if (dcn.dimensionName.equals(dname) && dcn.columnName.equals(cname))
				return dcn;
		}
		return null;
	}

	protected DimensionColumnNav findDimensionColumnNav(String dname, String cname)
	{
		return (Query.findDimensionColumnNav(dimensionColumns, dname, cname));
	}

	protected MeasureColumnNav findMeasureColumnNav(String name)
	{
		for (int i = 0; i < measureColumns.size(); i++)
		{
			MeasureColumnNav mcn = (MeasureColumnNav) measureColumns.get(i);
			if (mcn.filter == null)
			{
				if (mcn.measureName.equals(name))
					return (mcn);
			} else
			{
				if (name.equals(mcn.filter.getMeasureFilterName(mcn.measureName)))
					return (mcn);
			}
		}
		return (findDerivedMeasureColumnNav(name));
	}

	protected MeasureColumnNav findDerivedMeasureColumnNav(String name)
	{
		for (MeasureColumnNav mcn : derivedMeasureColumns)
		{
			if (mcn.measureName.equals(name))
				return (mcn);
		}
		return (null);
	}

	/**
	 * Add dimension column to the query (display name is column name)
	 * 
	 * @param dname
	 *            Dimension name
	 * @param cname
	 *            Column name
	 * @return
	 */
	public DimensionColumnNav addDimensionColumn(String dname, String cname) throws NavigationException
	{
		return (addNewDimensionColumn(dname, cname, cname, false));
	}

	/**
	 * Add a dimension column to the query
	 * 
	 * @param dname
	 *            Dimension name
	 * @param cname
	 *            Column name
	 * @param displayName
	 *            Display name of column in result set
	 * @return
	 */
	public DimensionColumnNav addDimensionColumn(String dname, String cname, String displayName) throws NavigationException
	{
		return (addNewDimensionColumn(dname, cname, displayName, false));
	}

	/**
	 * Add dimension column to the query - for navigation purposes only
	 * 
	 * @param dname
	 *            Dimension name
	 * @param cname
	 *            Column name
	 * @return
	 */
	public DimensionColumnNav addNavigationDimensionColumn(String dname, String cname) throws NavigationException
	{
		DimensionColumnNav dcn = findDimensionColumnNav(dname, cname);
		if (dcn == null)
		{
			return (addNewDimensionColumn(dname, cname, cname, true));
		}
		return (dcn);
	}

	/**
	 * Creates a new dimension column to be added to a query without adding it added)
	 * 
	 * @param dname
	 *            Dimension name
	 * @param cname
	 *            Column name
	 * @return
	 */
	public static DimensionColumnNav createNewDimensionColumn(String dname, String cname, String displayName, int colIndex)
	{
		DimensionColumnNav dcn = new DimensionColumnNav(dname, cname, displayName, colIndex);
		dcn.navigateonly = false;
		return (dcn);
	}

	/**
	 * Add a dimension column to the query, ensuring it's new (i.e. don't check to see if the column has already been
	 * added)
	 * 
	 * @param dname
	 *            Dimension name
	 * @param cname
	 *            Column name
	 * @param navigateOnly
	 * @return
	 */
	private DimensionColumnNav addNewDimensionColumn(String dname, String cname, String displayName, boolean navigateOnly)
	{
		DimensionColumnNav dcn = createNewDimensionColumn(dname, cname, displayName, navigateOnly ? -1 : colIndex++);
		dimensionColumns.add(dcn);
		return (dcn);
	}

	/**
	 * Add a hidden dimension column to a query (used for navigation purposes)
	 * 
	 * @param dname
	 *            Dimension name
	 * @param cname
	 *            Column name
	 * @return
	 */
	public DimensionColumnNav addHiddenDimensionColumn(String dname, String cname) throws NavigationException
	{
		DimensionColumnNav dcn = findDimensionColumnNav(dname, cname);
		if (dcn == null)
		{
			dcn = new DimensionColumnNav(dname, cname, colIndex++);
			dimensionColumns.add(dcn);
			dcn.navigateonly = true;
		}
		return (dcn);
	}
	
	public OlapMemberExpression addOlapMemberExpression(String olapExpression, String displayName) throws NavigationException
	{
		OlapMemberExpression ome = createNewOlapMemberExpression(olapExpression, displayName, colIndex++);
		olapMemberExpressions.add(ome);
		return (ome);
	}
	
	/**
	 * Add a dimension memberset to the query
	 * 
	 * @param dname
	 *            Dimension name
	 * @param memberExpressions
	 *            MemberExpressions
	 * @param displayName
	 *            Display name of memberset in result set
	 * @return
	 */
	public DimensionMemberSetNav addDimensionMemberSet(String dname, List<MemberSelectionExpression> memberExpressions, String displayName, String dataType, String format) throws NavigationException
	{
		return (addNewDimensionMemberSet(dname, memberExpressions, displayName, false, dataType, format));
	}
	
	/**
	 * Add a dimension memberset to the query, ensuring it's new (i.e. don't check to see if the memberset has already been
	 * added)
	 * 
	 * @param dname
	 *            Dimension name
	 * @param memberExpressions
	 *            MemberExpressions
	 * @param navigateOnly
	 * @return
	 */
	private DimensionMemberSetNav addNewDimensionMemberSet(String dname, List<MemberSelectionExpression> memberExpressions, String displayName, boolean navigateOnly, String dataType, String format)
	{
		DimensionMemberSetNav dms = createNewDimensionMemberSet(dname, memberExpressions, displayName, navigateOnly ? -1 : colIndex++,dataType,format);
		dimensionMemberSets.add(dms);
		dms.navigateonly = navigateOnly;
		return (dms);
	}
	
	public static OlapMemberExpression createNewOlapMemberExpression(String olapExpression, String displayName, int colIndex)
	{
		OlapMemberExpression ome = new OlapMemberExpression(olapExpression, displayName, colIndex);
		return ome;
	}
	
	/**
	 * Creates a new dimension memberset to be added to a query without adding it added)
	 * 
	 * @param dname
	 *            Dimension name
	 * @param memberExpressions
	 *            MemberExpressions
	 * @return
	 */
	public static DimensionMemberSetNav createNewDimensionMemberSet(String dname, List<MemberSelectionExpression> memberExpressions, String displayName, int colIndex, String dataType, String format)
	{
		DimensionMemberSetNav dms = new DimensionMemberSetNav(dname, memberExpressions, displayName, colIndex, dataType, format);
		return (dms);
	}
	
	public void addDimMemberSetParent(DimensionMemberSetNav dms,String displayName)
	{
		dms.addParentColumn(colIndex++, displayName);
	}
	
	public void addDimMemberSetParentName(DimensionMemberSetNav dms,String displayName)
	{
		dms.addParentNameColumn(colIndex++, displayName);
	}
	
	public void addDimMemberSetOrdinal(DimensionMemberSetNav dms,String displayName)
	{
		dms.addOrdinalColumn(colIndex++, displayName);
	}
	
	public void addDimMemberSetUniqueName(DimensionMemberSetNav dms,String displayName)
	{
		dms.addUniqueNameColumn(colIndex++, displayName);
	}

	/**
	 * Process a virtual column into it's base columns
	 * 
	 * @param mname
	 */
	private boolean processVirtualColumn(String mname)
	{
		VirtualColumn[] vcols = r.getVirtualColumns();
		boolean result = false;
		for (int j = 0; j < vcols.length; j++)
		{
			if (vcols[j].Type == VirtualColumn.PIVOTED)
			{
				if (mname.equals(vcols[j].Name))
				{
					// Replace virtual pivot measure with actual pivot columns
					for (int k = 0; k < vcols[j].PivotColumns.length; k++)
					{
						addMeasureColumn(vcols[j].PivotColumns[k], vcols[j].PivotColumns[k], false);
					}
					result = true;
					break;
				}
			}
		}
		return (result);
	}

	/**
	 * Add a measure column to the query
	 * 
	 * @param mname
	 *            Column name of measure
	 * @param name
	 *            Display name in query result set
	 * @param hidden
	 *            Whether this measure is hidden (i.e. only used for navigation purposes)
	 * @return
	 */
	public MeasureColumnNav addMeasureColumn(String mname, String name, boolean hidden)
	{
		List<MeasureColumn> mclist = r.findMeasureColumns(mname);
		// If didn't find it, try to find a virtual column and return
		if (mclist == null || mclist.size() == 0)
		{
			processVirtualColumn(mname);
			return (null);
		}
		MeasureColumnNav mn = new MeasureColumnNav(mname, hidden, hidden ? -1 : colIndex++);
		mn.navigateonly = false;
		mn.displayName = name;
		/*
		 * See if this is a filtered measure
		 */
		if (mclist.get(0).MeasureFilter != null)
		{
			mn.filter = mclist.get(0).MeasureFilter;
			mname = mclist.get(0).BaseMeasureName;
			mclist = r.findMeasureColumns(mname);
			// If didn't find it, try to find a virtual column and return
			if (mclist == null || mclist.size() == 0)
			{
				processVirtualColumn(mname);
				return (null);
			}
		}
		/*
		 * See if there is a derived table for this measure. If so, and there are also non-derived measures, pick the
		 * first non-derived measure. Note: In the future it would be even better to check whether the non-derived
		 * measure is at a higher or lower cardinality than the non-derived and make sure it joins - but normally this
		 * cannot be determined until after navigation.
		 */
		MeasureColumn mc = getNonDerivedMeasureIfPresent(mclist);
		if (mc == null)
		{
			logger.error("Trying to add unmapped measure column (" + mname + ").  Please remove it or map it.");
			return null;
		}
		mn.aggregationRule = mc.AggregationRule;
		mn.dimensionRules = mc.DimensionRules;
		mn.isNonAdditive = mc.NonAdditive;
		if (mc.MeasureTable.Type == MeasureTable.DERIVED)
		{
			mn.sessionVariables = mc.SessionVariables;
			derivedMeasureColumns.add(mn);
			mn.matchingMeasures.add(mc);
			return (mn);
		}
		measureColumns.add(mn);
		return (mn);
	}

	/**
	 * 
	 */
	public MeasureColumnNav addDerivedMeasure(String formula, String name)
	{
		MeasureColumnNav mn = new MeasureColumnNav(name, false, colIndex++);
		mn.navigateonly = false;
		mn.displayName = name;
		mn.aggregationRule = null;
		mn.dimensionRules = null;
		derivedMeasureColumns.add(mn);
		mn.derivedMeasureFormula = formula;
		return (mn);
	}

	/**
	 * Add a filtered measure column to the query
	 * 
	 * @param mname
	 *            Column name of measure
	 * @param name
	 *            Display name in query result set
	 * @param hidden
	 *            Whether this measure is hidden (i.e. only used for navigation purposes)
	 * @param qf
	 *            Query filter to apply to this measure
	 * @return
	 */
	public MeasureColumnNav addMeasureColumn(String mname, String name, boolean hidden, QueryFilter qf) throws NavigationException, BadColumnNameException
	{
		MeasureColumnNav mn = addMeasureColumn(mname, name, hidden);
		if (mn == null)
			throw new BadColumnNameException(mname);
		if (mn.filter == null)
			mn.filter = qf;
		else if (qf != null)
			mn.filter = new QueryFilter(mn.filter, "AND", qf);
		return (mn);
	}

	/**
	 * Add a format clause (appends the column formatting as a column to the query result set)
	 * 
	 * @param mname
	 */
	public void addMeasureColumnFormat(String mname) throws NavigationException
	{
		MeasureTable mt = r.findMeasureTable(mname);
		if (mt == null)
		{
			return;
		}
		MeasureColumn mc = mt.findColumn(mname);
		StringBuilder sb = new StringBuilder();
		sb.append('\'');
		sb.append(mc.Format);
		sb.append('\'');
		this.addConstantColumn(sb.toString(), mc.ColumnName + " Format");
	}

	/**
	 * Add a Query Filter to this query
	 * 
	 * @param qf
	 */
	public void addFilter(QueryFilter qf) throws NavigationException, BadColumnNameException
	{
		addFilter(qf, true);
	}
	
	public void addFilter(QueryFilter qf, boolean extractNavigationColumn) throws NavigationException, BadColumnNameException
	{
		if (extractNavigationColumn)
		{	
			qf.extractNavigationColumns(this, null, null, null);
		}
		filters.add(qf);
	}

	/**
	 * Add a Order By to this query
	 * 
	 * @param ob
	 */
	public void addOrderBy(OrderBy ob) throws NavigationException
	{
		// Make sure it's not a dupe
		for (OrderBy ob2 : orderByClauses)
		{
			if (ob.equals(ob2))
				return;
		}
		ob.extractNavigationColumns(this);
		orderByClauses.add(ob);
	}

	public void addDisplayBy(DisplayOrder dispO) {
		if (displayOrders == null)
			displayOrders = new ArrayList<DisplayOrder>();
		
		displayOrders.add(dispO);
	}

	/**
	 * Add a Group By to this query
	 * 
	 * @param gb
	 */
	public void addGroupBy(GroupBy gb) throws NavigationException
	{
		gb.extractNavigationColumns(this);
		boolean found = false;
		for (GroupBy cgb : groupByClauses)
		{
			if (cgb.equals(gb))
			{
				found = true;
				break;
			}
		}
		if (!found)
			groupByClauses.add(gb);
	}

	/**
	 * Add a new aggregation to the query
	 * 
	 * @param agg
	 */
	public void addAggregation(Aggregation agg)
	{
		aggregations.add(agg);
	}

	/**
	 * Add a list of repository filters to the query
	 * 
	 * @param list
	 */
	public void addFilterList(String[] list) throws NavigationException, CloneNotSupportedException, BadColumnNameException
	{
		for (String item : list)
		{
			QueryFilter f = r.findFilter(item);
			if (f == null)
				throw (new NavigationException("Filter not found: " + item));
			// Add a cloned copy (makes sure navigation information isn't copied)
			addFilter((QueryFilter) f.clone());
		}
	}

	/**
	 * Add a constant column
	 * 
	 * @param cstr
	 * @param name
	 */
	public void addConstantColumn(String constantStr, String name) throws NavigationException
	{
		DimensionColumnNav dn = addDimensionColumn("CONSTANT$", name);
		dn.constant = true;
		dn.constantString = constantStr;
	}

	/**
	 * Add a filter for top n records
	 * 
	 * @param n
	 */
	public void addTopN(int n)
	{
		this.topn = n;
	}

	/**
	 * Set whether measure queries are tied together using a full outer join. Default is false (i.e. all measure queries
	 * need to return a result for a given row). Using an outer join, if some measures don't return results, but others
	 * do, results are still returned
	 * 
	 * @param fullOuterJoin
	 */
	public void setFullOuterJoin(boolean fullOuterJoin)
	{
		this.fullOuterJoin = fullOuterJoin;
	}

	public String getQuery()
	{
		if (query == null)
			return "";
		if(containsMultipleQueries && !simulateOuterJoinUsingPersistedDQTs)
		{
			//If there are multiple queries, we join the resultsets after executing each
			//subquery and we actually don't have a meaningful main query, so build a buffer
			//based on each subquery.
			StringBuilder sb = new StringBuilder();
			if(derivedTables != null && derivedTables.length > 0)
			{
				sb.append("Resultset is generated by joining the following subqueries:");
				for(int i = 0; i < derivedTables.length; i++)
				{
					sb.append("\n\n");
					sb.append("Subquery "+ i + ":");
					sb.append(derivedTables[i].getQuery());
				}
			}
			return sb.toString();
		}
		return (query.toString());
	}

	public int getReturnProjectionListSize()
	{
		return (resultProjectionList.size());
	}

	public List<QueryColumn> getReturnProjectionList()
	{
		return (resultProjectionList);
	}

	public QueryColumn getReturnProjectionListColumn(int pos)
	{
		return ((QueryColumn) resultProjectionList.get(pos));
	}

	public DimensionColumn getNavigatedDimensionColumn(String dname, String cname)
	{
		// Go through each navigation list and look for the column
		for (int i = 0; i < navList.size(); i++)
		{
			Navigation nav = (Navigation) navList.get(i);
			for (int j = 0; j < nav.getQueryColumnList().size(); j++)
			{
				QueryColumn qc = (QueryColumn) nav.getQueryColumnList().get(j);
				if (qc.type == 0)
				{
					DimensionColumn dc = (DimensionColumn) qc.o;
					if (dc.matchesDimensionName(dname) && dc.ColumnName.equals(cname))
					{
						return (dc);
					}
				}
			}
		}
		return (null);
	}

	private String getQuotedString(String dataType, String value)
	{
		if (dataType.equals("Integer") || dataType.equals("Number"))
			return (value);
		return ("'" + value + "'");
	}

	/**
	 * Generate appropriately mapped dimension column string
	 * 
	 * @param dc
	 *            Dimension Column structure
	 * @param vcDimensionTables
	 *            List of dimension tables that contain virtual columns
	 * @return Mapped string
	 */
	protected String getDimensionColumnString(DatabaseConnection dbc, DimensionColumn dc, List<DimensionTable> vcDimensionTables, QueryMap tableMap, List<String> lookupJoins)
	{
		if (vcDimensionTables.contains(dc.DimensionTable))
		{
			String tname = tableMap.getMap(dc.DimensionTable) + ".";
			if (dc.Qualify && dc.DisplayMap == null)
			{
				String physicalColumnName = dc.PhysicalName.substring(dc.PhysicalName.indexOf('.') + 1);
				return tname + physicalColumnName;
			} else
				return (tname + Util.replaceWithPhysicalString(dbc, dc.ColumnName));
		} else
		{
			String s = dc.PhysicalName;
			if (dc.DimensionTable.Type == DimensionTable.OPAQUE_VIEW)
			{
				s = Util.replaceStr(s, dc.DimensionTable.PhysicalName + ".", tableMap.getMap(dc.DimensionTable) + ".");
			} else
			{
				for (TableDefinition td : dc.DimensionTable.TableSource.Tables)
				{
					if (r.getCurrentBuilderVersion() > 16)
					{
						if (dc.overrideDimensionName != null && td.OverrideDimension != null && !td.OverrideDimension.equals(dc.overrideDimensionName))
							continue;
					}
					s = Util.replaceStr(s, td.PhysicalName + ".", tableMap.getMap(td) + ".");
				}
			}
			if (dc.DisplayMap != null)
			{
				StringBuilder sb = new StringBuilder();
				sb.append("CASE");
				if (dc.DisplayMap.Type == DisplayMap.MapType.Map)
				{
					for (int i = 0; i < dc.DisplayMap.Keys.length; i++)
						sb.append(" WHEN " + s + "=" + getQuotedString(dc.DisplayMap.DataType, dc.DisplayMap.Keys[i]) + " THEN "
								+ getQuotedString(dc.DataType, dc.DisplayMap.MappedValues[i]));
				} else if (dc.DisplayMap.Type == DisplayMap.MapType.Bucket)
				{
					for (int i = 0; i < dc.DisplayMap.MappedValues.length; i++)
						sb.append(" WHEN " + s + ">=" + dc.DisplayMap.Buckets[i][0] + " AND " + s + "<" + dc.DisplayMap.Buckets[i][1] + " THEN "
								+ getQuotedString(dc.DataType, dc.DisplayMap.MappedValues[i]));
				} else if (dc.DisplayMap.Type == DisplayMap.MapType.Lookup)
				{
					String tmap = tableMap.getMap(dc.DisplayMap);
					String join = (dc.DisplayMap.LookupOuterJoin ? "LEFT OUTER JOIN" : dbc.getInnerJoin()) + " " + r.replaceVariables(session, dc.DisplayMap.LookupTable)
							+ " " + tmap + "  ON " + s + "=" + tmap + "." + dc.DisplayMap.LookupJoinColumn;
					if (dc.DisplayMap.LookupFilterExpression != null && dc.DisplayMap.LookupFilterExpression.length() > 0)
						join += " AND " + Util.replaceStr(dc.DisplayMap.LookupFilterExpression, dc.DisplayMap.LookupTable, tmap);
					if (!lookupJoins.contains(join))
						lookupJoins.add(join);
					return (Util.replaceStr(dc.DisplayMap.LookupExpression, dc.DisplayMap.LookupTable, tmap));
				} else if (dc.DisplayMap.Type == DisplayMap.MapType.Query)
				{
					try
					{
						String query2 = QueryString.preProcessQueryString(dc.DisplayMap.Query, r, null);
						AbstractQueryString aqs = null;
						aqs = AbstractQueryString.getInstance(r, query2);
						Query q = aqs.getQuery();
						q.generateQuery(session, true);
						QueryResultSet qrs = null;
						if (dc.DisplayMap.mapResultSet == null)
						{
							int maxRecords = r.getServerParameters().getMaxRecords();
							maxRecords = Math.min(maxRecords, 5000);
							if (rsc != null)
								qrs = rsc.getQueryResultSet(q, maxRecords, r.getServerParameters().getMaxQueryTime(), null, null, null, session, false, getRepository().getServerParameters()
										.getDisplayTimeZone(), true);
							else
								qrs = new QueryResultSet(q, maxRecords, r.getServerParameters().getMaxQueryTime(), null);
						} else
							qrs = dc.DisplayMap.mapResultSet;
						if (qrs != null && qrs.rows.length > 100)
						{
							DimensionColumnNav mdcn = null;
							for (DimensionColumnNav dcn : dimensionColumns)
							{
								if (dcn.columnName.equals(dc.ColumnName) && dcn.dimensionName.equals(dc.DimensionTable.DimensionName))
								{
									mdcn = dcn;
									mdcn.volatileColumn = true;
									if (dc.DisplayMap.Default != null && dc.DisplayMap.Default.length() > 0)
										mdcn.defaultMappedValue = dc.DisplayMap.Default;
									break;
								}
							}
							if (dc.DisplayMap.mapResultSet == null)
							{
								// Clone it because we are changing it
								qrs = qrs.clone(false);
								dc.DisplayMap.mapResultSet = qrs;
								qrs.columnNames[0] = dc.ColumnName;
								qrs.tableNames[0] = dc.DimensionTable.DimensionName;
								qrs.q.addMappedDimensionColumn(mdcn);
								qrs.mapDimensionColumns();
								mapResultSets.add(qrs);
								dc.DisplayMap.mapResultSet = qrs;
							}
							return (s);
						}
						if (qrs != null && qrs.columnNames.length > 1)
							for (int i = 0; i < qrs.rows.length; i++)
							{
								String reps = qrs.rows[i][1].toString().replace('\'', '`');
								if (dc.DataType.equals("Integer"))
									sb.append(" WHEN " + s + "=" + qrs.rows[i][0] + " THEN '" + reps + "'");
								else
									sb.append(" WHEN " + s + "='" + qrs.rows[i][0] + "' THEN '" + reps + "'");
							}
					} catch (IOException e)
					{
						logger.error(e, e);
					} catch (SQLException e)
					{
						logger.error(e, e);
					} catch (CloneNotSupportedException e)
					{
						logger.error(e, e);
					} catch (ClassNotFoundException e)
					{
						logger.error(e, e);
					} catch (BaseException e)
					{
						logger.error(e, e);
					}
				}
				if (dc.DisplayMap.Default != null)
					sb.append(" ELSE '" + dc.DisplayMap.Default + "'");
				sb.append(" END");
				return (sb.toString());
			}
			return (s);
		}
	}

	/**
	 * Get appropriately mapped measure column
	 * 
	 * @param aggregate
	 *            Whether or not to apply the aggregation rule
	 * @param mc
	 *            The measure column used
	 * @param melist
	 *            The measure expression list
	 * @param q
	 *            The current query
	 * @param filter
	 *            Any measuure filter
	 * @param tableMap
	 *            The current table map
	 * @return
	 * @throws SyntaxErrorException 
	 */
	public String getMeasureString(DatabaseConnection dbc, boolean aggregate, MeasureColumn mc, List<String> melist, Query q, QueryFilter filter,
			QueryMap tableMap, List<String> lookupJoins, List<String> whereStrings) throws SyntaxErrorException
	{
		String formula = mc.PhysicalName;
		if (mc.MeasureTable.Type == MeasureTable.OPAQUE_VIEW)
		{
			/*
			 * If this measure table is from an opaque view, replace with it's name
			 */
			formula = Util.replaceStr(formula, mc.MeasureTable.PhysicalName + ".", tableMap.getMap(mc.MeasureTable) + ".");
		} else
		{
			/*
			 * If it's from table sources, process each separately
			 */
			for (TableDefinition td : mc.MeasureTable.TableSource.Tables)
			{
				formula = Util.replaceStr(formula, td.PhysicalName + ".", tableMap.getMap(td) + ".");
			}
		}
		if (filter != null && (mc.BaseMeasureName == null || mc.MeasureTable.QueryAgg == null))
		{
			StringBuilder sb = new StringBuilder();
			String clause = filter.getWhereFilterString(dbc, aggregate, q, tableMap, null, lookupJoins);
			if (clause == null) // prompted filters can appear here
			{
				sb.append(formula);
			} else
			{
				if ((DatabaseConnection.isDBTypeInfoBright(dbc.DBType) || r.getCurrentBuilderVersion() >= Repository.PushDownMeasureFilters) 
						&& whereStrings != null)
				{
					/*
					 * Push measure filters into the where clause
					 */
					if (!whereStrings.contains(clause))
						whereStrings.add(clause);
					sb.append(formula);
				}else
				{
					sb.append("CASE WHEN ");
					sb.append(clause);
					sb.append(" THEN ");
					sb.append(formula);
					Double def = filter.getMeasureFilterDefault();
					if (def != null)
					{
						sb.append(" ELSE " + def + " END");
					}
					else
					{
						if (mc.AggregationRule.equals("AVG") || mc.AggregationRule.equals("MIN") || mc.AggregationRule.equals("MAX")
								|| mc.AggregationRule.equals("COUNT") || mc.AggregationRule.equals("COUNT DISTINCT"))
						{
							sb.append(" ELSE NULL END");
						}
						else
						{
							if (DatabaseConnection.isDBTypeHiveVariant(dbc.DBType))
							{
								// Hive can't handle type differences
								if ("Integer".equals(mc.DataType))
									sb.append(" ELSE 0 END");
								else
									sb.append(" ELSE 0.0 END");
							}
							else
							{
								sb.append(" ELSE 0 END");
							}
						}
					}
				}
			}
			formula = sb.toString();
		}
		return (aggregate ? mc.getAggregationString(dbc, formula, melist, null) : formula);
	}

	/**
	 * Get the query string for a derived measure
	 * 
	 * @param name
	 * @return
	 */
	protected StringBuilder getDerivedMeasureString(String name)
	{
		for (int j = 0; j < derivedMeasureColumns.size(); j++)
		{
			MeasureColumnNav mcn = (MeasureColumnNav) derivedMeasureColumns.get(j);
			if (mcn.getMeasureName().equals(name))
			{
				return (getProjectionListDerivedItem(j));
			}
		}
		return (null);
	}

	/**
	 * Returns whether a name is that of a derived measure column within a query
	 * 
	 * @param name
	 * @return
	 */
	protected boolean isDerivedMeasure(String name)
	{
		for (MeasureColumnNav mcn : derivedMeasureColumns)
		{
			if (mcn.displayName.equals(name))
			{
				return (true);
			}
		}
		return (false);
	}

	/**
	 * Determine the additional levels of aggregation needed in a query and the associated columns that need to be added
	 * to the query to reduce to the table levels
	 * 
	 * @param dtProjectionList
	 *            Projection list in query
	 * @param dimColumns
	 *            Dimension columns in query (whether in projection list or not)
	 * @param levelList
	 *            List of levels in the query (return)
	 * @param levelqcList
	 *            Level query columns (return)
	 * @param aggRuleList
	 *            Aggregation rule for each level (return)
	 */
	public void getAggregationLevels(List<QueryColumn> dtProjectionList, List<DimensionColumnNav> dimColumns, List<Level> levelList,
			List<QueryColumn> levelqcList, List<DimensionRule> aggRuleList)
	{
		/*
		 * Figure out if multiple levels of aggregation are necessary
		 */
		Set<DimensionRule> possibleDimensionRules = new HashSet<DimensionRule>();
		for (QueryColumn qc : dtProjectionList)
		{
			if (qc.type == QueryColumn.MEASURE)
			{
				if (((MeasureColumn) qc.o).DimensionRules != null)
					possibleDimensionRules.addAll(((MeasureColumn) qc.o).DimensionRules);
			}
		}
		/*
		 * Prune rules for dimensions not in query
		 */
		Set<DimensionRule> dimensionRules = new HashSet<DimensionRule>();
		for (DimensionRule rule : possibleDimensionRules)
		{
			boolean found = false;
			for (DimensionColumnNav dcn : dimColumns)
			{
				if (dcn.dimensionName.equals(rule.getDimension()))
				{
					found = true;
					break;
				}
			}
			if (found)
				dimensionRules.add(rule);
		}
		possibleDimensionRules = null;
		/*
		 * For each rule, add the level-key that corresponds to the lowest table level in that dimension
		 */
		for (DimensionRule rule : dimensionRules)
		{
			// Find dimension level
			Level l = null;
			DimensionTable ldt = null;
			for (DimensionColumnNav dcn : dimColumns)
			{
				if (dcn.constant)
					continue; // can not navigate to constant columns
				DimensionColumn dc = dcn.getDimensionColumn();
				// If this column is in this dimension, process
				if (dc.DimensionTable.DimensionName.equals(rule.getDimension()))
				{
					// First, get the level associated with the table
					String dtlevel = dc.DimensionTable.Level;
					/*
					 * If a level has already been found for another column in the same dimension as this rule, see if
					 * this new level is higher or lower
					 */
					if (l != null)
					{
						Level l2 = l.findLevel(dtlevel);
						if (l2 != null)
						{
							l = l2;
							ldt = dc.DimensionTable;
						}
					} else
					{
						ldt = dc.DimensionTable;
						l = r.findLevel(ldt.DimensionName, dtlevel);
					}
				}
			}
			/*
			 * Add all columns in the key if not in query and a level exists, otherwise add all columns from dimension
			 */
			if (l != null)
			{
				for (String colName : l.getPrimaryKey().ColumnNames)
				{
					DimensionColumn leveldc = ldt.findColumn(colName);
					if (leveldc != null)
					{
						levelqcList.add(new QueryColumn(QueryColumn.DIMENSION, -1, leveldc, leveldc.ColumnName));
						levelList.add(l);
						aggRuleList.add(rule);
					}
				}
			} else
			{
				for (DimensionColumnNav dcn : dimColumns)
				{
					if (dcn.dimensionName.equals(rule.getDimension()))
					{
						levelqcList.add(new QueryColumn(QueryColumn.DIMENSION, -1, dcn.getDimensionColumn(), dcn.columnName));						
						levelList.add(l);						
						aggRuleList.add(rule);
					}
				}
			}
		}
		/*
		 * Now, go through this list and eliminate any that have columns in the query at or below these levels. Also,
		 * eliminate any that are at the identical levels as their dimension table sources (because no aggregation is
		 * necessary)
		 */
		for (int i = 0; i < levelList.size(); i++)
		{
			if (levelList.get(i) != null)
			{
				Level l = levelList.get(i);
				for (int j = 0; j < dtProjectionList.size(); j++)
				{
					if (dtProjectionList.get(j).type == QueryColumn.DIMENSION)
					{
						DimensionColumn dc = (DimensionColumn) dtProjectionList.get(j).o;
						Level findLevel = l.findLevelColumn(dc.ColumnName);
						if (findLevel != null)
						{
							levelList.remove(i);
							levelqcList.remove(i);
							aggRuleList.remove(i);
							i--;
							break;
						}
					}
				}
			}
		}
	}

	/**
	 * Generate a derived table query for a given measure table
	 * 
	 * @param dtProjectionList
	 *            Projection list for derived table
	 * @param mt
	 *            Measure table
	 * @param nav
	 *            Navigation data structure
	 * @param noOuterQuery
	 *            Whether there is an outer query or not
	 * @param dbType
	 *            Database type of the navigation connection
	 * @return Query string
	 * @throws RepositoryException 
	 * @throws SQLException 
	 * @throws IOException 
	 * @throws ArrayIndexOutOfBoundsException 
	 */
	private DerivedTableQuery generateDerivedTableQuery(List<QueryColumn> dtProjectionList, MeasureTable mt, Navigation nav, boolean noOuterQuery,
			DatabaseConnection dbc, boolean specifyDataTypes, boolean isSubQuery) 
	throws CloneNotSupportedException, BaseException, ArrayIndexOutOfBoundsException, SQLException, IOException
	{
		DerivedTableQuery dtq = new DerivedTableQuery(this, navList, r, orderByClauses, groupByClauses, derivedMeasureColumns, filters, session);
		dtq.generate(dtProjectionList, mt, nav, noOuterQuery, dbc, specifyDataTypes, isSubQuery);
		Map<DatabaseConnection, List<TempTable>> ttableMap = dtq.getTempTableMap();
		if (ttableMap != null && ttableMap.size() > 0)
		{
			if (tempTableMap == null)
				tempTableMap = new HashMap<DatabaseConnection, List<TempTable>>();
			for (Entry<DatabaseConnection, List<TempTable>> ten : ttableMap.entrySet())
			{
				List<TempTable> ttlist = tempTableMap.get(dbc);
				if (ttlist == null)
				{
					ttlist = new ArrayList<TempTable>();
					tempTableMap.put(dbc, ttlist);
				}
				for (TempTable tt : ten.getValue())
					ttlist.add(tt);
			}
		}
		return dtq;
	}

	private MeasureColumn findAndAddMeasure(String measureStr)
	{
		MeasureColumn mc = r.findMeasureColumn(measureStr);
		if (mc == null)
			return null; // unable to find the measure column in repository
		// Add column if needed
		for (int m = 0; m < measureColumns.size(); m++)
		{
			MeasureColumnNav mn = (MeasureColumnNav) measureColumns.get(m);
			if (mn.measureName.equals(mc.ColumnName))
			{
				return mc; // Already present, no need to add.
			}
		}
		addMeasureColumn(mc.ColumnName, mc.ColumnName, true);
		return mc;
	}

	/**
	 * Process all the derived measures in this query. Find the appropriate columns that are referred to in the derived
	 * measures, add them to the query column list and then setup to replace their references in the derived query.
	 */
	private void processDerivedMeasures() throws NavigationException
	{
		List<MeasureColumn> foundProjectionColumns = new ArrayList<MeasureColumn>();
		List<MeasureColumn> foundMeasureColumns = new ArrayList<MeasureColumn>();
		List<String> foundMeasureStrings = new ArrayList<String>();
		List<Integer> foundIndexes = new ArrayList<Integer>();
		/*
		 * Go through the measure column list and add any new measure tables needed
		 */
		for (int j = 0; j < derivedMeasureColumns.size(); j++)
		{
			MeasureColumnNav mcn = (MeasureColumnNav) derivedMeasureColumns.get(j);
			String formula = null;
			MeasureColumn mc = null;
			if (mcn.derivedMeasureFormula != null)
				formula = mcn.derivedMeasureFormula;
			else
			{
				mc = r.findMeasureColumn(mcn.measureName);
				formula = mc.PhysicalName;
			}
			/*
			 * Go through each non-derived measure and see if it can be replaced and note it if so
			 */
			int pos = 0;
			while ((pos >= 0) && ((pos = formula.indexOf("M{", pos)) >= 0))
			{
				int epos = formula.indexOf('}', pos);
				if (epos > pos + 3)
				{
					String measureStr = formula.substring(pos + 2, epos);
					MeasureColumn replacemc = findAndAddMeasure(measureStr);
					if (replacemc != null)
					{
						// Add to the projection list
						foundProjectionColumns.add(mc);
						foundMeasureColumns.add(replacemc);
						foundMeasureStrings.add("M{" + measureStr + "}");
						foundIndexes.add(Integer.valueOf(j));
					} else
					{
						throw (new NavigationException("Unable to process derived measure: " + mcn.measureName + " (unknown measure: " + measureStr
								+ ", in formula: " + formula + ")"));
					}
				} else
				{
					throw (new NavigationException("Unable to process derived measure: " + mcn.measureName + " (bad formula: " + formula + ")"));
				}
				pos = epos;
			}
		}
		/*
		 * When adding the projection column, replace the name with the appropriate physical name, then make sure it's
		 * qualified. If after all measure tables are added and there's only one, use it's qualifier for all base
		 * measures. Otherwise, use the derived table qualifiers for the base measures Add to the list of strings to
		 * replace
		 */
		for (int i = 0; i < foundProjectionColumns.size(); i++)
		{
			MeasureStringReplace msr = new MeasureStringReplace();
			msr.foundmc = (MeasureColumn) foundProjectionColumns.get(i);
			msr.replacemc = (MeasureColumn) foundMeasureColumns.get(i);
			msr.foundStr = (String) foundMeasureStrings.get(i);
			msr.index = ((Integer) foundIndexes.get(i)).intValue();
			derivedMeasureStringReplacements.add(msr);
		}
	}

	/**
	 * Class to represent a string replacement that needs to happen in a derived measure. Indicates the query column of
	 * the derived measure, the measure column that is being replaced, the string that represents that measure column
	 * (need to look up that new measure in the navigated list and fully qualify it with the appropriate derived query
	 * table prefix-$DQTx
	 * 
	 * @author Brad Peters
	 */
	private static class MeasureStringReplace
	{
		MeasureColumn foundmc;
		MeasureColumn replacemc;
		String foundStr;
		int index;
	}

	/**
	 * Process an query projection list item. Identifies which derived table query contains the column and creates
	 * appropriately fully qualified reference along with final name for column. Also handles the case where outer joins
	 * return null values for dimension columns - uses COALESCE syntax to pick the non-null values
	 * 
	 * @param qc
	 *            Query column to process
	 * @param dqtIndex
	 *            Index of current derived table being processed
	 * @param replaceStr
	 *            Derived table string replacement
	 * @param navList
	 *            List of navigations (necessary for outer joins and dimension columns - to be able to pick the non-null
	 *            values
	 * @param qindex
	 *            Index of query column in navigation projection lists
	 * @param derived
	 *            Whether this column is a derived measure (so don't surround by aggregation in a full outer join)
	 * @return
	 */
	protected StringBuilder getProjectionListItem(QueryColumn qc, int dqtIndex, String replaceStr, List<Navigation> navList, int qindex, boolean derived)
	{
		StringBuilder result = new StringBuilder();
		if (qc.type == QueryColumn.MEASURE)
		{
			MeasureColumn mc = (MeasureColumn) qc.o;
			/*
			 * If it's not derived, then add qualifier, else, just include
			 */
			if (fullOuterJoin && !derived)
			{
				if (mc.DataType.equalsIgnoreCase("VARCHAR") || mc.DataType.equalsIgnoreCase("DATETIME") || mc.DataType.equalsIgnoreCase("DATE"))
				{
					result.append("MAX(");
				} else
				{
					result.append("SUM(");
				}
			}
			if (mc != null && mc.MeasureTable.Type != 2)
			{
				result.append(getDerivedQueryTableName(dqtIndex));
				result.append('.');
				/*
				 * Add the name that is used in derived tables
				 */
				result.append(columnMap.getMap(qc));
			} // else, add the derived formula
			else
			{
				result.append(replaceStr);
			}
			if (fullOuterJoin && !derived)
				result.append(')');
		} else
		{
			if ((navList != null) && (navList.size() > 1))
			{
				int count = 0;
				StringBuilder cstr = new StringBuilder();
				for (int i = 0; i < navList.size(); i++)
				{
					QueryColumn qc2 = findQC(qc, navList.get(i).getProjectionList());
					if (qc2 != null)
					{
						if (i > 0)
							cstr.append(',');
						cstr.append(getDerivedQueryTableName(i));
						cstr.append('.');
						cstr.append(columnMap.getMap(qc2));
						count++;
					}
				}
				if (count > 1)
					result.append("COALESCE(" + cstr + ") ");
				else
					result.append(cstr);
			} else
			{
				result.append(getDerivedQueryTableName(dqtIndex));
				result.append('.');
				result.append(columnMap.getMap(qc));
			}
		}
		return (result);
	}

	QueryColumn findQC(QueryColumn qc, List<QueryColumn> qclist)
	{
		for (QueryColumn qc2 : qclist)
		{
			if (qc2.type == qc.type)
			{
				if (qc.type == QueryColumn.MEASURE && qc.name.equals(qc2.name))
					return (qc2);
				else if (qc.type == QueryColumn.DIMENSION && qc.name.equals(qc2.name)
						&& ((DimensionColumn) qc.o).DimensionTable.DimensionName.equals(((DimensionColumn) qc2.o).DimensionTable.DimensionName))
					return (qc2);
				else if (qc.name.equals(qc2.name))
					return (qc2);
			}
		}
		return (null);
	}

	/**
	 * Find a dimension column by name from the projection list
	 * 
	 * @param name
	 * @return
	 */
	protected QueryColumn findDCProjectionListItem(String name)
	{
		// Search for query column (search first derived table)
		List<QueryColumn> pList = navList.get(0).getProjectionList();
		for (QueryColumn qc : pList)
		{
			if (qc.type == QueryColumn.DIMENSION)
			{
				DimensionColumn dc = (DimensionColumn) qc.o;
				if (dc.ColumnName.equals(name))
				{
					return (qc);
				}
			}
		}
		return (null);
	}

	/*
	 * Get a dimension column projection list item by name - needed for outer filters
	 */
	protected StringBuilder getProjectionListItemString(String name)
	{
		int index = -1;
		int dqtindex = -1;
		// Search for query column (search first derived table)
		for (int j = 0; j < navList.size(); j++)
		{
			List<QueryColumn> pList = navList.get(j).getProjectionList();
			for (int i = 0; i < pList.size(); i++)
			{
				QueryColumn qc = (QueryColumn) pList.get(i);
				String columnName = "";
				if (qc.type == QueryColumn.DIMENSION)
				{
					columnName = ((DimensionColumn) qc.o).ColumnName;
				} else if (qc.type == QueryColumn.MEASURE)
				{
					columnName = ((MeasureColumn) qc.o).ColumnName;
				} else if (qc.type == QueryColumn.CONSTANT_OR_FORMULA)
				{
					columnName = (String) qc.o;
				}
				//Choose the qc with no filter over the one with filter if there are multiple options available for the same column
				if (columnName.equals(name) && ((dqtindex == -1 && index == -1) || (qc.filter == null)))
				{
					dqtindex = j;
					index = i;
					break;
				}
			}
		}
		if(index == -1)
		{
			index = 0;
		}
		if(dqtindex == -1)
		{
			dqtindex = 0;
		}
		if (index < navList.get(dqtindex).getProjectionList().size())
		{
			return (getProjectionListItem(navList.get(dqtindex).getProjectionList().get(index), dqtindex, null, fullOuterJoin ? navList : null, index, false));
		}
		return (null);
	}

	/**
	 * Process an outer query projection list item. Identifies which derived table query contains the column and creates
	 * appropriately fully qualified reference along with final name for column
	 * 
	 * @param qc
	 *            Query column to process
	 * @param dqtIndex
	 *            Index of current derived table being processed
	 * @param replaceStr
	 *            Derived table string replacement
	 * @param navList
	 *            List of navigations (necessary for outer joins and dimension columns - to be able to pick the non-null
	 *            values
	 * @param qindex
	 *            Index of query column in navigation projection lists
	 * @param usePhysicalName
	 *            Whether or not to use a valid physical name of a column as its name (e.g. in order to wrap the query)
	 * @param dbType
	 *            Database type being used by navigation connection
	 * @return
	 */
	private StringBuilder processProjectionListQueryItem(QueryColumn qc, int dqtIndex, String replaceStr, List<Navigation> navList, int qindex,
			boolean usePhysicalName, DatabaseConnection dbc, boolean specifyDataTypes, boolean isSubQuery)
	{
		StringBuilder retStr = new StringBuilder();
		String prlItem = getProjectionListItem(qc, dqtIndex, replaceStr, navList, qindex, false).toString();
		if (specifyDataTypes)
		{
			prlItem = getNameWithSpecificDataType(prlItem, qc, dbc);
		}
		retStr.append(prlItem);
		String name = (usePhysicalName ? Util.replaceWithPhysicalString(qc.name) : qc.name);
		if (dbc != null && dbc.requiresHashedIdentifierForColumnAlias())
			name = dbc.getHashedIdentifier(name);
		if (dbc!=null)
		{
			retStr.append(dbc.getTableReference(name, isSubQuery));
		}
		return retStr;
	}

	/**
	 * Return the query column for a given dimension column
	 * 
	 * @param dimension
	 * @param column
	 * @return
	 */
	protected QueryColumn getQueryColumnListItem(String dimension, String column)
	{
		for (int i = 0; i < navList.size(); i++)
		{
			Navigation nav = (Navigation) navList.get(i);
			List<QueryColumn> qcList = nav.getQueryColumnList();
			for (int j = 0; j < qcList.size(); j++)
			{
				QueryColumn qc = (QueryColumn) qcList.get(j);
				if (qc.type == QueryColumn.DIMENSION)
				{
					DimensionColumn dc = (DimensionColumn) qc.o;
					if (dc.matchesDimensionName(dimension) && dc.ColumnName.equals(column))
						return (qc);
				} else if (qc.type == QueryColumn.MEASURE)
				{
					MeasureColumn mc = (MeasureColumn) qc.o;
					if (mc.ColumnName.equals(column))
						return (qc);
				}
			}
		}
		return (null);
	}

	/**
	 * Return the query column for a given position in the projection list
	 * 
	 * @param position
	 * @return
	 */
	protected QueryColumn getProjectionListQueryColumn(int position)
	{
		return (this.resultProjectionList.get(position));
	}

	/**
	 * Process the query string for a derived measure with a given index (in the derived measure columns list)
	 * 
	 * @param index
	 * @return
	 */
	private StringBuilder getProjectionListDerivedItem(int index)
	{
		MeasureColumnNav mcn = (MeasureColumnNav) derivedMeasureColumns.get(index);
		MeasureColumn mc = null;
		if (mcn.matchingMeasures.size() > 0)
			mc = mcn.matchingMeasures.get(0);
		QueryColumn qc = (QueryColumn) new QueryColumn(QueryColumn.MEASURE, resultProjectionList.size(), mc, mcn.displayName);
		String replace = mc == null ? mcn.derivedMeasureFormula : mc.PhysicalName;
		for (int i = 0; i < derivedMeasureStringReplacements.size(); i++)
		{
			MeasureStringReplace msr = (MeasureStringReplace) derivedMeasureStringReplacements.get(i);
			if (msr.index == index)
			{
				// Find the query column for the replaced measure
				QueryColumn replaceqc = null;
				int k = 0;
				boolean found = false;
				for (k = 0; k < navList.size(); k++)
				{
					Navigation nav = (Navigation) navList.get(k);
					List<QueryColumn> plist = nav.getProjectionList();
					for (int l = 0; l < plist.size(); l++)
					{
						replaceqc = (QueryColumn) plist.get(l);
						if (replaceqc.type == 1)
							if (((MeasureColumn) replaceqc.o).ColumnName.equals(msr.replacemc.ColumnName))
							{
								found = true;
								break;
							}
					}
					if (found)
						break;
				}
				if (found)
				{
					StringBuilder colStr = new StringBuilder(getDerivedQueryTableName(k));
					colStr.append('.');
					colStr.append(columnMap.getMap(replaceqc));
					if (fullOuterJoin)
					{
						colStr.insert(0, "SUM(");
						colStr.append(')');
					}
					replace = Util.replaceStr(replace, msr.foundStr, colStr.toString());
				} else
				{
					logger.warn("Error: could not find " + msr.replacemc.ColumnName + ", more than likely multiple levels of derived measures (replace: "
							+ replace + ")");
				}
			}
		}
		return (getProjectionListItem(qc, 0, replace, null, 0, true));
	}

	private StringBuilder processProjectionListDerivedQueryItem(QueryColumn qc, int index, boolean usePhysicalName, DatabaseConnection dbc,
			boolean specifyDataTypes, boolean isSubQuery)
	{
		StringBuilder retStr = new StringBuilder();
		String prlItem = getProjectionListDerivedItem(index).toString();
		if (specifyDataTypes)
		{
			prlItem = getNameWithSpecificDataType(prlItem, qc, dbc);
		}
		retStr.append(prlItem);
		String name = (usePhysicalName ? Util.replaceWithPhysicalString((resultProjectionList.get(resultProjectionList.size() - 1)).name)
				: (resultProjectionList.get(resultProjectionList.size() - 1)).name);
		if (dbc != null && dbc.requiresHashedIdentifierForColumnAlias())
			name = dbc.getHashedIdentifier(name);
		if (dbc!=null)
		{
			retStr.append(dbc.getTableReference(name, isSubQuery));
		}
		return retStr;
	}

	/**
	 * Process exclusion filters. These filters are applied when a dimension is excluded from a query. This is useful
	 * when you need to take the last value in a dimension as current. For example, current cash balance. Normally that
	 * would sum by other dimensions, but by time, you want to take the last value. Using an exclusion filter, this
	 * dimension can automatically be filtered for the last period to accomplish this.
	 */
	private void processExclusionFilters() throws NavigationException, CloneNotSupportedException, BadColumnNameException
	{
		Set<String> set = new HashSet<String>();
		// Get list of dimensions
		for (int i = 0; i < dimensionColumns.size(); i++)
		{
			DimensionColumnNav dcn = (DimensionColumnNav) dimensionColumns.get(i);
			set.add(dcn.dimensionName);
		}
		/*
		 * Now, go through each dimension and see if it has an exclusion filter and that dimension has not been
		 * referenced in this query
		 */
		for (int i = 0; i < r.getHierarchies().length; i++)
		{
			String exFilter = r.getHierarchies()[i].ExclusionFilter;
			if ((exFilter != null) && (exFilter.length() > 0))
			{
				if (!set.contains(r.getHierarchies()[i].DimensionName))
				{
					QueryFilter f = r.findFilter(exFilter);
					// Make sure the filter applies if it's a dimension filter
					if ((f.getType() == QueryFilter.TYPE_PREDICATE) && (f.getColumnType() == QueryFilter.TYPE_DIMENSION_COLUMN))
					{
						boolean OK = true;
						for (int j = 0; j < measureColumns.size(); j++)
						{
							MeasureColumnNav mcnav = (MeasureColumnNav) measureColumns.get(j);
							List<MeasureColumn> mclist = r.findMeasureColumns(mcnav.measureName);
							MeasureColumn mc = getNonDerivedMeasureIfPresent(mclist);
							if (!r.joinedToDimensionColumn(mc.MeasureTable, f.getDimension(), f.getColumn()))
							{
								OK = false;
								break;
							}
						}
						if (OK && measureColumns.size() > 0)
						{
							addFilterList(new String[]
							{ exFilter });
						}
					} else
					{
						addFilterList(new String[]
						{ exFilter });
					}
				}
			}
		}
	}

	/**
	 * Add filters needed if there are no measures in the query
	 */
	private void processNoMeasureFilters() throws NavigationException, CloneNotSupportedException, BadColumnNameException
	{
		Set<String> set = new HashSet<String>();
		Set<String> vcset = new HashSet<String>();
		// Get list of dimensions
		for (int i = 0; i < dimensionColumns.size(); i++)
		{
			DimensionColumnNav dcn = (DimensionColumnNav) dimensionColumns.get(i);
			DimensionColumn dc = r.findDimensionColumn(dcn.dimensionName, dcn.columnName);
			if (dc != null)
			{
				if (dc.VC != null)
				{
					vcset.add(dcn.dimensionName);
					set.remove(dcn.dimensionName);
				} else if (!vcset.contains(dcn.dimensionName))
				{
					set.add(dcn.dimensionName);
				}
			}
		}
		/*
		 * Now, go through each dimension and see if it has a no-measure filter and that dimension has been referenced
		 * in this query
		 */
		for (int i = 0; i < r.getHierarchies().length; i++)
		{
			String nmFilter = r.getHierarchies()[i].NoMeasureFilter;
			if ((nmFilter != null) && (nmFilter.length() > 0))
			{
				if (set.contains(r.getHierarchies()[i].DimensionName))
				{
					addFilterList(new String[]
					{ nmFilter });
				}
			}
		}
	}

	/**
	 * Navigate all clauses based on current query column list
	 * 
	 * @param queryColumnList
	 */
	private void navigateClauses(List<QueryColumn> queryColumnList)
	{
		for (int i = 0; i < filters.size(); i++)
		{
			QueryFilter qf = filters.get(i);
			qf.setNavigation(queryColumnList);
		}
		for (int i = 0; i < orderByClauses.size(); i++)
		{
			OrderBy ob = orderByClauses.get(i);
			ob.setNavigation(queryColumnList, this, r);
		}
		for (int i = 0; i < groupByClauses.size(); i++)
		{
			GroupBy gb = groupByClauses.get(i);
			gb.setNavigation(queryColumnList);
		}
		for (int i = 0; i < measureColumns.size(); i++)
		{
			if (measureColumns.get(i).filter != null)
			{
				measureColumns.get(i).filter.setNavigation(queryColumnList);
			}
		}
	}
	private boolean processedImplicitColumns;

	public void processImplicitColumns() throws NavigationException, CloneNotSupportedException, BadColumnNameException
	{
		if (processedImplicitColumns)
			return;
		processDerivedMeasures();
		// Add exclusion filters to filter measures (if there are any)
		if (measureColumns.size() > 0)
			processExclusionFilters();
		else if (measureColumns.size() == 0 && !hasMeasureExpressionColumns)
			processNoMeasureFilters();
		processedImplicitColumns = true;
	}
	boolean processed = false;

	/**
	 * Process any generic columns
	 */
	private void processGenerics()
	{
		if (processed)
			return;
		processed = true;
		List<GenericColumn> gclist = new ArrayList<GenericColumn>();
		for (DimensionColumnNav dcn : dimensionColumns)
		{
			GenericColumn gc = r.findGenericColumn(dcn.dimensionName, dcn.columnName);
			if (gc != null)
			{
				// If there is a match, see if any other columns are in that dimension
				boolean found = false;
				for (DimensionColumnNav dcn2 : dimensionColumns)
				{
					if (dcn2 != dcn && dcn2.dimensionName.equals(dcn.dimensionName))
					{
						found = true;
						break;
					}
				}
				// If not, just take the highest level
				if (!found)
				{
					Hierarchy h = r.findDimensionHierarchy(dcn.dimensionName);
					// If no hierarchy exists, pick first
					if (h == null)
					{
						dcn.columnName = gc.Equivalents[0];
					} else
					{
						// Otherwise, replace with the highest level equivalent
						Level l = null;
						String eq = null;
						for (String s : gc.Equivalents)
						{
							Level nl = null;
							DimensionColumn dc = r.findDimensionColumn(gc.Dimension, s);
							if (dc != null && dc.DimensionTable.Level != null)
							{
								nl = h.findLevel(dc.DimensionTable.Level);
							}
							if (nl != null)
							{
								if (l == null)
								{
									l = nl;
									eq = s;
								} else if (l != nl && nl.findLevel(l) != null)
								{
									l = nl;
									eq = s;
								}
							}
						}
						dcn.columnName = eq;
						replaceGenericClauses(gc.Dimension, gc.Name, eq);
					}
				} else
				{
					/*
					 * If another column in the same dimension is found, add to list
					 */
					gclist.add(gc);
				}
			}
		}
		// If there are generic columns to replace, then do so
		if (gclist.size() > 0)
		{
			// First, determine dimension levels
			Map<String, Level> levelMap = new HashMap<String, Level>();
			for (DimensionColumnNav dcn : dimensionColumns)
			{
				Hierarchy h = r.findDimensionHierarchy(dcn.dimensionName);
				Level l = h.findLevelColumn(dcn.columnName);
				if (l != null)
				{
					Level cl = levelMap.get(dcn.dimensionName);
					if (cl == null)
						levelMap.put(dcn.dimensionName, l);
					else if (cl != l && cl.findLevel(l) != null)
						levelMap.put(dcn.dimensionName, l);
				}
			}
			updateLevelMapForLevelKeys(r, levelMap, dimensionColumns);
			// Now, process replacements
			for (DimensionColumnNav dcn : dimensionColumns)
			{
				for (GenericColumn gc : gclist)
				{
					if (dcn.dimensionName.equals(gc.Dimension) && dcn.columnName.equals(gc.Name))
					{
						// Reset the display name
						if (dcn.displayName == null || dcn.displayName.equals(dcn.columnName))
							dcn.displayName = dcn.columnName;
						Level l = levelMap.get(gc.Dimension);
						// If no level is found, pick the first equivalent
						dcn.columnName = gc.Equivalents[0];
						boolean foundLevelMatch = false;
						if (l != null)
						{
							Hierarchy h = r.findDimensionHierarchy(gc.Dimension);
							Map<Level, String> equivalentByLevelMap = new HashMap<Level, String>();
							// Otherwise, pick the equivalent at the right level
							for (String s : gc.Equivalents)
							{
								DimensionColumn dc = r.findDimensionColumn(gc.Dimension, s);
								if (dc != null && dc.DimensionTable.Level != null)
								{
									Level nl = h.findLevel(dc.DimensionTable.Level);
									if (nl == l)
									{
										dcn.columnName = s;
										// Replace any group by, order by or filter clauses
										replaceGenericClauses(gc.Dimension, gc.Name, s);
										foundLevelMatch = true;
										break;
									} else if (nl != null)
									{
										equivalentByLevelMap.put(nl, s);
									}
								}
							}
							if (!foundLevelMatch)
							{
								// Go thru parents of the query level to see if an equivalent can be found there. The
								// level list should
								// already be ordered by closest parent first.
								List<Level> parentList = new ArrayList<Level>();
								l.getAllParents(parentList);
								for (Level parent : parentList)
								{
									String s = equivalentByLevelMap.get(parent);
									if (s != null)
									{
										dcn.columnName = s;
										replaceGenericClauses(gc.Dimension, gc.Name, s);
										foundLevelMatch = true;
										break;
									}
								}
								if (!foundLevelMatch)
								{
									// No luck with parents - examine child levels if an equivalent can be found there.
									// The level list
									// should already be ordered by closest child first.
									List<Level> childList = new ArrayList<Level>();
									l.getAllDecendents(childList);
									for (Level child : childList)
									{
										String s = equivalentByLevelMap.get(child);
										if (s != null)
										{
											dcn.columnName = s;
											replaceGenericClauses(gc.Dimension, gc.Name, s);
											foundLevelMatch = true;
											break;
										}
									}
								}
							}
						}
						if (!foundLevelMatch)
						{
							replaceGenericClauses(gc.Dimension, gc.Name, gc.Equivalents[0]);
						}
					}
				}
			}
		}
	}

	/**
	 * Navigate the current query (but don't generate the query itself). Can be used if only navigation is required. Use
	 * generateQuery if the query is needed for actual use.
	 * 
	 * @param forceNew
	 *            Whether to force a new navigation (i.e. if it has already been navigated, still navigate again)
	 * @param consolidateNavigation
	 *            Whether to consolidate measures that are navigated to the same measure tables. Choosing false for this
	 *            is useful to examine how each measure was navigated independently (useful for explainnavigation
	 *            command)
	 */
	public void navigateQuery(boolean forceNew, boolean consolidateNavigation, Session session, boolean allowMultipleSources) throws NavigationException,
			CloneNotSupportedException, BadColumnNameException
	{
		if (hasBeenNavigated() && !forceNew)
			return;
		// Clear results if query was generated previously
		resultProjectionList.clear();
		navList.clear();
		clearMatchingColumnsInfo();
		clearAddedGroupBys();
		processImplicitColumns();
		if (measureColumns.size() <= 1 && derivedMeasureColumns.size() == 0)
			fullOuterJoin = false;

		// See if any of the dimension columns are generic
		processGenerics();
		// If no measure table then generate simple query
		if (measureColumns.size() == 0)
		{
			Navigation nav = new Navigation(null, dimensionColumns, dimensionMemberSets, olapMemberExpressions, r, session, filters);
			nav.navigate(false);
			navigateClauses(nav.getQueryColumnList());
			navList.add(nav);
			updateTableNamesInfo();
			updateMatchingColumnsInfo(nav);
			usesLevelSpecificJoin = nav.usesLevelSpecificJoin();
			return;
		}
		// If more than one measure table, then generate derived table sytax
		else
		{
			// Create navigation lists - one for each measure
			boolean renavigate = false;
			/*
			 * Loop while re-navigation is necessary. Re-navigation is required if a given measure is both non-derived
			 * and derived. Initially, an attempt is made to use the non-derived version. If that cannot navigate, then
			 * the derived version is attempted.
			 */
			do
			{
				renavigate = false;
				for (int i = 0; i < measureColumns.size(); i++)
				{
					Navigation nav = new Navigation(measureColumns.get(i), dimensionColumns, dimensionMemberSets, olapMemberExpressions, r, session, filters);
					MeasureColumn derivedMc = nav.navigate(true);
					if (derivedMc != null)
					{
						// encountered a derived measure column, so process it and renavigate from the begining.
						MeasureColumnNav mcnav = measureColumns.get(i);
						measureColumns.remove(mcnav);
						derivedMeasureColumns.add(mcnav);
						mcnav.pick = 0;
						mcnav.matchingMeasures.clear();
						mcnav.matchingMeasures.add(derivedMc);
						mcnav.sessionVariables = derivedMc.SessionVariables;
						processDerivedMeasures();
						renavigate = true;
						break;
					}
					usesLevelSpecificJoin = usesLevelSpecificJoin || nav.usesLevelSpecificJoin();
					navList.add(nav);
				}
			} while (renavigate);
			updateTableNamesInfo();
			if(navList.size() > 0)
			{
				for(Navigation nav : navList)
				{
					updateMatchingColumnsInfo(nav);
				}
			}
			if (consolidateNavigation)
			{
				consolidateNavigation();
				if (navList.size() == 1)
				{
					// If consolidated down to 1 remove any added level key columns
					Navigation nav = navList.get(0);
					List<QueryColumn> plist = nav.getProjectionList();
					List<QueryColumn> removeList = new ArrayList<QueryColumn>(plist.size());
					for (QueryColumn qc : plist)
						if (qc.type == QueryColumn.DIMENSION && qc.levelKey && qc.columnIndex < 0)
							removeList.add(qc);
					for (QueryColumn qc : removeList)
						plist.remove(qc);
				}
				/*
				 * Prune projection lists - eliminate any unneeded columns where a level key already exists If it is a
				 * full outer join, no need to prune any columns. This of course takes away some of the optimization
				 * which are important when it comes to virtual columns, but this is the right thing to do. See bug 2148
				 * for more details.
				 */
				boolean isDBTypeRequiresMultipleQueries = false;
				boolean isDBTypeRequiresMultipleQueriesForInnerJoin = false;
				for (Navigation nav : navList)
				{
					DatabaseConnection dconn = nav.getNavigationConnection();
					if (DatabaseConnection.isDBTypeMySQL(dconn.DBType) || dconn.DBType == DatabaseType.Infobright)
					{
						isDBTypeRequiresMultipleQueries = true;
					}
					if (DatabaseConnection.isDBTypeMemDB(dconn.DBType))
						isDBTypeRequiresMultipleQueriesForInnerJoin = true;
				}
				if (navList.size() > 1 && !fullOuterJoin && !(isDBTypeRequiresMultipleQueries && this.containsFilteredMeasure())
						&& !isDBTypeRequiresMultipleQueriesForInnerJoin)
				{
					Navigation nav = navList.get(0);
					List<QueryColumn> plist = nav.getProjectionList();
					Set<String> dimensionColumnNamesInFilters = nav.getAllDimensionColumnNamesInFilters();
					List<Integer> removeIndices = new ArrayList<Integer>();
					for (int i = 0; i < plist.size(); i++)
					{
						QueryColumn qc = plist.get(i);
						if (qc.type == QueryColumn.DIMENSION && !qc.levelKey)
						{
							DimensionColumn dc = (DimensionColumn) qc.o;
							String dcName = dc.DimensionTable.DimensionName + "." + dc.ColumnName;
							if (dimensionColumnNamesInFilters.contains(dcName))
							{
								continue; // if it is used in the filter, do not prune.
							}
							/*
							 * Make sure a level key exists at or below this level and if it does, remove this query
							 * column from every subquery other than the first
							 */
							Hierarchy h = r.findDimensionHierarchy(dc.DimensionTable.DimensionName);
							if (h != null)
							{
								Level l = h.findLevelColumn(dc.ColumnName);
								if (l != null)
								{
									List<Level> levelList = new ArrayList<Level>();
									levelList.add(l);
									l.getAllDecendents(levelList);
									for (Level sl : levelList)
									{
										if (lookForIndexToRemoveAtLevel(i, removeIndices, sl, plist))
											break;
									}
								} else
								{
									/*
									 * If cannot identify the level, then check to see if the dimension key is in the
									 * list. If cannot do that, look at the level of the navigated dimension table
									 */
									if (!lookForIndexToRemoveAtLevel(i, removeIndices, h.DimensionKeyLevel, plist))
									{
										Level sl = h.findLevel(dc.DimensionTable.Level);
										if (sl != null)
											lookForIndexToRemoveAtLevel(i, removeIndices, sl, plist);
									}
								}
							}
						}
					}
					for (int i = 1; i < navList.size(); i++)
					{
						List<QueryColumn> removeList = new ArrayList<QueryColumn>(removeIndices.size());
						plist = navList.get(i).getProjectionList();
						List<QueryColumn> qclist = navList.get(i).getQueryColumnList();
						for (int index : removeIndices)
						{
							removeList.add(plist.get(index));
						}
						for (QueryColumn qc : removeList)
						{
							plist.remove(qc);
							qclist.remove(qc);
						}
					}
				}
			}
			DatabaseConnection navConn = null;
			for (int i = 0; i < navList.size(); i++)
			{
				Navigation nav = (Navigation) navList.get(i);
				if (navConn == null)
					navConn = nav.getNavigationConnection();
				else if (navConn != nav.getNavigationConnection() && !allowMultipleSources)
					throw (new NavigationException("Request cannot be satisfied using a single data source"));
				// Navigate filters
				navigateClauses(nav.getQueryColumnList());
				List<QueryColumn> navpList = new ArrayList<QueryColumn>();
				navpList.addAll(nav.getProjectionList());
				/*
				 * Go through Order By clauses and add any to inner projection lists that need to be added (i.e. are not
				 * in the outer projection list)
				 */
				for (int j = 0; j < orderByClauses.size(); j++)
				{
					OrderBy ob = (OrderBy) orderByClauses.get(j);
					ob.ensureInProjectionList(navpList, nav.getQueryColumnList(), groupByClauses, addedGroupBys);
				}
			}
		}
	}

	private boolean lookForIndexToRemoveAtLevel(int index, List<Integer> removeIndices, Level l, List<QueryColumn> plist)
	{
		for (LevelKey lk : l.Keys)
		{
			boolean isMatch = true;
			for (String s : lk.ColumnNames)
			{
				boolean found = false;
				for (QueryColumn sqc : plist)
				{
					if (sqc.type == QueryColumn.DIMENSION && sqc.name.equals(s))
					{
						found = true;
						break;
					}
				}
				if (!found)
				{
					isMatch = false;
					break;
				}
			}
			if (isMatch)
			{
				removeIndices.add(index);
				return (true);
			}
		}
		return (false);
	}

	/**
	 * Replace any group by, order by or filter clauses
	 * 
	 * @param dimension
	 * @param genericName
	 * @param colName
	 */
	private void replaceGenericClauses(String dimension, String genericName, String colName)
	{
		for (GroupBy gb : groupByClauses)
			if (gb.getDimName().equals(dimension) && gb.getColName().equals(genericName))
				gb.setColName(colName);
		for (OrderBy ob : orderByClauses)
			if (ob.getDimName() != null && ob.getDimName().equals(dimension) && ob.getColName().equals(genericName))
				ob.setColName(colName);
		for (QueryFilter qf : filters)
			qf.replaceGenericAttribute(dimension, genericName, colName);
	}

	/**
	 * Determine if a second measure column navigation filter can consolidate into the first measure column navigation
	 * 
	 * @param mcn1
	 * @param mcn2
	 * @return
	 */
	private boolean consolidateFilters(Navigation nav1, Navigation nav2)
	{
		MeasureColumnNav mcn2 = nav2.getMeasureColumnNav();
		MeasureColumnNav mcn1 = nav1.getMeasureColumnNav();
		if (mcn1!=null && mcn2!=null)
		{
			String pf1 = mcn1.matchingMeasures.get(mcn1.pick).PhysicalFilter;
			String pf2 = mcn2.matchingMeasures.get(mcn2.pick).PhysicalFilter;
			// If either measure contains a physical query filter, then don't consolidate
			if ((pf1 != null && pf1.length() > 0) || (pf2 != null && pf2.length() > 0))
				if (!mcn1.sessionVariableExpand && !mcn2.sessionVariableExpand)
					return false;
			if (mcn1.filter == null && mcn2.filter == null)
				return true;
			try
			{
				DatabaseConnection dbc1 = nav1.getNavigationConnection();
				DatabaseConnection dbc2 = nav2.getNavigationConnection();
				if (DatabaseConnection.isDBTypeInfoBright(dbc1.DBType) || DatabaseConnection.isDBTypeInfoBright(dbc2.DBType) 
						|| r.getCurrentBuilderVersion() >= Repository.PushDownMeasureFilters)
				{
					if (mcn1.filter!=null || mcn2.filter!=null)
					if((mcn1.filter == null && mcn2.filter != null) || (mcn1.filter != null && mcn2.filter == null) 
							|| (mcn1.filter != null && !mcn1.filter.equals(mcn2.filter)))
					if (!mcn1.sessionVariableExpand && !mcn2.sessionVariableExpand)
						return false;
				}
			} catch (NavigationException e)
			{
				return false;
			}
		}

		/*
		 * See if the columns in the measure filter of the second query navigate to tables in the first
		 */
		Set<DimensionTable> dtables = new HashSet<DimensionTable>();
		for (DimensionColumnNav dcn1 : nav1.getDimColumns())
		{
			if (dcn1.constant)
			{
				continue;
			}
			dtables.add(dcn1.getDimensionColumn().DimensionTable);
		}
		for (DimensionColumnNav dcn2 : nav2.getDimColumns())
		{
			if (dcn2.constant)
			{
				continue;
			}
			DimensionTable dt2 = dcn2.getDimensionColumn().DimensionTable;
			if (!dtables.contains(dt2))
			{
				boolean match = false;
				for (DimensionTable dt1 : dtables)
				{
					if(dt1.QueryAgg != null && dt2.QueryAgg != null 
						&& dt1.PhysicalName != null && dt2.PhysicalName != null && dt1.PhysicalName.equals(dt2.PhysicalName))
					{
						match = true;
						break;
					}
				}
				if(!match)
				{
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Go through the navigation for each measure. If another measure exists from the same measure table, collapse into
	 * the same navigation
	 */
	private void consolidateNavigation()
	{
		for (int i = 0; i < navList.size(); i++)
		{
			Navigation nav1 = navList.get(i);
			MeasureTable mt1 = nav1.getMeasureTables().get(0);
			for (int j = 0; j < navList.size(); j++)
			{
				Navigation nav2 = navList.get(j);
				if (nav2 == nav1)
					continue;
				MeasureTable mt2 = nav2.getMeasureTables().get(0);
				if (mt2 == mt1 && consolidateFilters(nav1, nav2))
				{
					/*
					 * Copy the dimension columns from the second that aren't in the first. Measure columns can be
					 * duplicated
					 */
					List<QueryColumn> plist1 = nav1.getProjectionList();
					List<QueryColumn> plist2 = nav2.getProjectionList();
					for (QueryColumn qc2 : plist2)
					{
						boolean found = false;
						for (QueryColumn qc1 : plist1)
						{
							if (((qc1.o == qc2.o) && (qc1.name == qc2.name))
									|| (((qc1.type == QueryColumn.DIMENSION && qc2.type == QueryColumn.DIMENSION) 
											|| (qc1.type == QueryColumn.DIMENSION_MEMBER_SET && qc2.type == QueryColumn.DIMENSION_MEMBER_SET) 
											|| (qc1.type == QueryColumn.OLAP_MEMBER_EXPRESSION && qc2.type == QueryColumn.OLAP_MEMBER_EXPRESSION))
													&& qc1.name != null && qc2.name != null && qc1.name.equals(qc2.name)))
							{
								if (qc1.filter == null && qc2.filter == null)
									found = true;
								else if (qc1.filter != null && qc1.filter.equals(qc2.filter))
									found = true;
								if (found)
									break;
							}
						}
						if (!found)
							plist1.add(qc2);
					}
					List<QueryColumn> qclist1 = nav1.getQueryColumnList();
					List<QueryColumn> qclist2 = nav2.getQueryColumnList();
					for (QueryColumn qc2 : qclist2)
					{
						boolean found = false;
						for (QueryColumn qc1 : qclist1)
						{
							if ((qc1.o == qc2.o) && (qc1.name == qc2.name))
							{
								if (qc1.filter == null && qc2.filter == null)
									found = true;
								else if (qc1.filter != null && qc1.filter.equals(qc2.filter))
									found = true;
								if (found)
									break;
							}
						}
						if (!found)
							qclist1.add(qc2);
					}
					/*
					 * Copy dimension tables from second to first
					 */
					List<DimensionTable> dtlist1 = nav1.getDimensionTables();
					List<DimensionTable> dtlist2 = nav2.getDimensionTables();
					for (DimensionTable dt2 : dtlist2)
					{
						boolean found = false;
						for (DimensionTable dt1 : dtlist1)
						{
							if (dt1 == dt2)
							{
								found = true;
								break;
							}
						}
						if (!found)
							dtlist1.add(dt2);
					}
					/*
					 * Copy filter if it exists on second to first
					 */
					if (nav2.getMeasureColumnNav().filter != null)
					{
						nav1.addMeasureFilterRemovedInConsolidation(nav2.getMeasureColumnNav().filter);
					}
					// Remove second navigation and projection list
					navList.remove(nav2);
					j--;
				}
			}
		}
	}
	boolean alreadyGenerated = false;

	/**
	 * Generates the query string for this instance
	 * @throws ScriptException 
	 * @throws ArrayIndexOutOfBoundsException 
	 */
	public void generateQuery(boolean allowMultipleSources) throws BaseException, CloneNotSupportedException, SQLException, ArrayIndexOutOfBoundsException, IOException
	{
		generateQuery(null, false, allowMultipleSources);
	}

	/**
	 * Generates the query string for this instance
	 * @throws ScriptException 
	 * @throws ArrayIndexOutOfBoundsException 
	 */
	public void generateQuery(Session session, boolean specifyDataTypes, boolean allowMultipleSources) throws BaseException, CloneNotSupportedException, SQLException, ArrayIndexOutOfBoundsException, IOException
	{
		Set<QueryColumn> addedColumns = new HashSet<QueryColumn>(dimensionColumns.size());
		/*
		 * Only navigate if necessary or the query has already been generated (in which case, re-generating should force
		 * a re-navigation)
		 */
		if (navList.size() == 0 || alreadyGenerated)
			navigateQuery(true, true, session, allowMultipleSources);
		Database.DatabaseType dbType = Database.DatabaseType.None;
		DatabaseConnection dbc = navList.get(0).getNavigationConnection();
		if (dbc != null)
		{
			dbType = dbc.DBType;
			columnMap.setDBType(dbType);
		}
		// deal with Impala, which needs LIMIT if there is a sort
		if (dbc.requiresLimitOnOrderBy() && orderByClauses.size() > 0 && !hasTop())
		{
			this.setTopn(dbc.getOrderByLimit());
		}
		if(DatabaseConnection.isDBTypeXMLA(dbType))
		{
			boolean filtersChanged = false;
			List<QueryFilter> toAddList = new ArrayList<QueryFilter> ();
			List<QueryFilter> toRemoveList = new ArrayList<QueryFilter> ();
			for (QueryFilter qf : filters)
			{
				QueryFilter newQF = MDXQuery.convertMultiValueFilterToComplexFilter(r, session, qf);
				if (qf != newQF)
				{
					filtersChanged = true;
					toAddList.add(newQF);
					toRemoveList.add(qf);
				}
			}
			filters.removeAll(toRemoveList);
			filters.addAll(toAddList);
			boolean filtersAdded = addSourceFiltersForMDX(navList.get(0), filters, r, session);
			if(filtersAdded || filtersChanged)
			{
				//Renavigate if there are any new filters added.
				this.clearNavigation();
				navigateQuery(true, true, session, allowMultipleSources);
			}
		}
		List<QueryFilter> newList = new ArrayList<QueryFilter>();
		for (QueryFilter qf : filters)
		{
			Set<DimensionColumn> list = new HashSet<DimensionColumn>(dimensionColumns.size());
			qf.returnDimensionColumns(list);
			if (list.size() > 0 && qf.containsMeasure())
			{
				// Make sure that all dimension columns are exposed
				for (Navigation nav : navList)
				{
					List<QueryColumn> plist = nav.getProjectionList();
					List<QueryColumn> qclist = nav.getQueryColumnList();
					for (DimensionColumn dc : list)
					{
						// Find the query column
						for (QueryColumn qc : qclist)
						{
							if (qc.type == QueryColumn.DIMENSION
									&& dc.matchesDimensionName(((DimensionColumn) qc.o))
									&& dc.ColumnName.equals(((DimensionColumn) qc.o).ColumnName))
							{
								if (!plist.contains(qc))
								{
									/*
									 * First, make sure all metrics that are in the projection list are additive. If
									 * not, cannot wrap the query and re-aggregate so throw an exception.
									 */
									for (QueryColumn pqc : plist)
									{
										if (pqc.type == QueryColumn.MEASURE)
										{
											MeasureColumn mc = (MeasureColumn) pqc.o;
											if (!mc.isAdditive(dc.DimensionTable.DimensionName))
												throw (new NavigationException(
														"Query filter requires implicit re-aggregation of non-additive measure - cannot evaluate, Dimension: " + dc.DimensionTable.DimensionName + ", Measure: " + mc.ColumnName));
										}
									}
									/*
									 * Otherwise, add the column for later wrapping
									 */
									qc.hidden = false;
									qc.columnIndex = colIndex++;
									plist.add(qc);
									addedColumns.add(qc);
									boolean found = false;
									for (GroupBy gb : groupByClauses)
									{
										if (gb.getDimensionColumn() == dc)
										{
											found = true;
											break;
										}
									}
									if (!found)
									{
										GroupBy gb = new GroupBy(dc.DimensionTable.DimensionName, dc.ColumnName);
										gb.setNavigation(qclist);
										groupByClauses.add(gb);
									}
								}
							}
						}
					}
				}
				/*
				 * Optimization, if more than one query, try to push down parts of the filter to inner queries. Since
				 * the filter contains a measure and dimension columns, it must be done at the outer query level
				 * (meaning that inner queries might not be tightly filtered). Pushing parts of this filter into the
				 * inner queries will allow earlier filtering by the database and make the queries run faster. If the
				 * measure is used in a clause with an AND (as opposed to OR), then the measure can be removed and a
				 * partial filter can be pushed down without the measure.
				 */
				QueryFilter pushDown = qf.getPushDownFilter(this);
				if (pushDown != null)
					newList.add(pushDown);
			}
		}
		filters.addAll(newList);
		boolean usePhysicalNames = addedColumns.size() > 0 || (dcToKeepInProjectionList != null);
		boolean isSubQuery = addedColumns.size() > 0 || (dcToKeepInProjectionList != null) || isVirtualColumnQuery() || isAggregate;
		// If zero or one measure table and no derived measures, then generate simple query
		if (navList.size() == 1 && derivedMeasureColumns.size() == 0)
		{
			Navigation nav = navList.get(0);
			List<QueryColumn> pList = nav.getProjectionList();
			if (pList.size() > 0 && colIndex > 0) {
				for (int j = 0; j >= -1; j = (j < 0) ? (j - 1) : (j == colIndex - 1 ? -1 : j + 1))
				{
					for (int k = 0; k < pList.size(); k++)
					{
						QueryColumn qc = (QueryColumn) pList.get(k);
						//Lookup queries for XMLA may contain qc in resultProjectionList already, not adding it again if it exists
						if (qc.columnIndex == j && !addedColumns.contains(qc) && !qc.hidden && 
								(!resultProjectionList.contains(qc) || !DatabaseConnection.isDBTypeXMLA(dbType)))
						{
							resultProjectionList.add(qc);
						}
					}
				}
			}
			if (dbType == DatabaseType.GoogleAnalytics)
			{
				query = GoogleAnalyticsQuery.generateQuery(topn, resultProjectionList, filters, orderByClauses);
				return;
			} else if (DatabaseConnection.isDBTypeXMLA(dbType))
			{
				query = MDXQuery.generateQuery(r, dbType, topn, resultProjectionList, filters, dbc, orderByClauses, validateOnly, this);
				requiresDispalySortingForXMLA = dbc.requiresDispalySortingForXMLA();
				return;
			}
			MeasureTable mt = nav.getMeasureColumnNav() == null ? null : nav.getMeasureColumnNav().getMeasureColumn().MeasureTable;
			DerivedTableQuery dqt = generateDerivedTableQuery(nav.getProjectionList(), mt, nav, true, dbc, specifyDataTypes, isSubQuery);
			if(dqt.containsFederatedJoin())
			{
				fjqh = dqt.getFederatedJoinQueryHelper();
				if(fjqh == null)
				{
					fjqh = new FederatedJoinQueryHelper(r, session, dqt, this);
					fjqh.generate(nav, filters, specifyDataTypes);
				}
				alreadyGenerated = true;
				return;
			}
			query = dqt.getQuery();
			int p = query.indexOf(" FROM ");
			String colAliasStartStr = dbc.getColumnAliasStartStr();
			String res = columnMap.replaceColumnNames(dbc, query.substring(0, p), usePhysicalNames, colAliasStartStr, isSubQuery) + query.substring(p);
			query = new StringBuilder(res);
			boolean topApplied = false;
			if(hasTop() && DatabaseConnection.isDBTypeInfoBright(dbc.DBType) && dcToKeepInProjectionList != null && dbc.supportsLimit())
			{
				query.append(" LIMIT " + getPhysicalTopn(dbc) + " ");
				topApplied = true;
			}
			// Wrap query if necessary
			if (addedColumns.size() > 0 || dcToKeepInProjectionList != null)
			{
				query = wrapQuery(dbc, pList, addedColumns, dcToKeepInProjectionList);
			}
			if (hasTop() && !topApplied)
			{
				if (dbc.supportsTop())
					query.replace(0, "SELECT".length(), "SELECT TOP " + getPhysicalTopn(dbc));
				else if (dbc.supportsLimit())
					query.append(" LIMIT " + getPhysicalTopn(dbc) + " ");
				else if (dbc.supportsFetchFirstRows())
					query.append(" FETCH FIRST " + getPhysicalTopn(dbc) + " ROWS ONLY ");
				else if (dbc.supportsRowNum())
					wrapWithRownum(query, getPhysicalTopn(dbc));
			}
			String qv = r.replaceVariables(session, query.toString());
			query = new StringBuilder(qv);
			alreadyGenerated = true;
			return;
		}
		// If more than one measure table, then generate derived table syntax
		else
		{
			/*
			 * See if this is a query is an outer join and a having clause is required that includes a dimension column
			 * not in the projection list (and hence must be included in the projection list, but filtered out in the
			 * final result set) If so, temporarily expose those columns
			 */
			alreadyGenerated = true;
			derivedTables = new DerivedTableQuery[navList.size()];
			// Don't create outer query if more than one connection
			if (getConnections().size() > 1)
				containsMultipleQueries = true;
			// Only generate temps for the first query
			for (int i = 0; i < navList.size(); i++)
			{
				// Find the measure table for each
				MeasureTable mt = null;
				Navigation nav = (Navigation) navList.get(i);
				// Navigate filters (need to renavigate as filters are re-used across navigations)
				navigateClauses(nav.getQueryColumnList());
				List<QueryColumn> navpList = new ArrayList<QueryColumn>();
				navpList.addAll(nav.getProjectionList());
				/*
				 * Go through Order By clauses and add any to inner projection lists that need to be added (i.e. are not
				 * in the outer projection list)
				 */
				for (int j = 0; j < orderByClauses.size(); j++)
				{
					OrderBy ob = (OrderBy) orderByClauses.get(j);
					ob.ensureInProjectionList(navpList, nav.getQueryColumnList(), groupByClauses, addedGroupBys);
				}
				for (int j = 0; j < navpList.size(); j++)
				{
					QueryColumn qc = (QueryColumn) navpList.get(j);
					if (qc.type == 1)
					{
						mt = ((MeasureColumn) qc.o).MeasureTable;
					}
				}
				DatabaseConnection navdbc = nav.getNavigationConnection();
				DatabaseType navDbType = navdbc.DBType;
				if (navDbType != DatabaseType.GoogleAnalytics && !DatabaseConnection.isDBTypeXMLA(navDbType))
					derivedTables[i] = generateDerivedTableQuery(navpList, mt, nav, false, navdbc, ((!expandDerivedQueryTables) ? specifyDataTypes : false), true);
				else
				{
					if (containsMultipleQueries && !simulateOuterJoinUsingPersistedDQTs)
					{
						List<DisplayOrder> dorders = new ArrayList<DisplayOrder>();
						derivedTables[i] = new DerivedTableQuery(this, r, navDbType, topn, nav.getProjectionList(), filters, orderByClauses, dorders, dbc);
						if (dorders.size() > 0)
							displayOrders = dorders;
					}
				}
			}
		}
		if (containsMultipleQueries && !simulateOuterJoinUsingPersistedDQTs)
		{
			// Create the result projection list
			for (int i = 0; i < colIndex; i++)
			{
				for (Navigation nav : navList)
				{
					for (QueryColumn qc : nav.getProjectionList())
					{
						if (qc.columnIndex == i)
						{
							boolean found = false;
							for (QueryColumn qc2 : resultProjectionList)
							{
								if (qc2.columnIndex == qc.columnIndex)
								{
									found = true;
									break;
								}
							}
							if (!found)
								resultProjectionList.add(qc);
							break;
						}
					}
				}
			}
			return;
		}
		// Create outer query
		query = new StringBuilder();
		query.append("SELECT ");
		StringBuilder[] whereClauses = new StringBuilder[navList.size() - 1];
		String[] orderedOrderBys = new String[orderByClauses.size()];
		boolean anyOrderBys = false;
		List<String> groupByClauses = new ArrayList<String>();
		boolean firstp = true;
		boolean containsFilteredMeasure = this.containsFilteredMeasure();
		/*
		 * Create projection list and where clause First add dimension columns from first derived query (all the same)
		 * Note, currently order bys not in projection list aren't added
		 */
		for (Navigation nav : navList)
		{
			//If navigation uses federated join, we need to execute multiple queries against different connection
			if(nav.containsFederatedJoin() && navList.size() > 1)
			{
				containsMultipleQueries = true;
			}
			// Disallow full outer join for MySQL and Infobright (not allowed). Disallow subqueries for MemDB
			DatabaseConnection dconn = nav.getNavigationConnection();
			if (DatabaseConnection.isDBTypeMemDB(dconn.DBType) && navList.size() > 1)
				containsMultipleQueries = true;
			else if (DatabaseConnection.isDBTypeMySQL(dconn.DBType) || dconn.DBType == DatabaseType.Infobright || dconn.DBType == DatabaseType.Vertica) // XXX hack for now
			{
				if ((fullOuterJoin || containsFilteredMeasure) && navList.size() > 1)
					containsMultipleQueries = true;
			}
		}
		if (!isAggregate && !isVirtualColumnQuery)
		{
			for (MeasureColumnNav mcnav : this.measureColumns)
			{
				String pf = mcnav.matchingMeasures.get(mcnav.pick).PhysicalFilter;
				if (pf != null && pf.length() > 0)
					containsMultipleQueries = true;
			}
		}
		Navigation nav = navList.get(0);
		List<QueryColumn> pList = nav.getProjectionList();
		Set<String> orderByClausesAdded = new HashSet<String>();
		for (int count = 0; count < colIndex; count++)
		{
			for (int j = 0; j < pList.size(); j++)
			{
				QueryColumn qc = (QueryColumn) pList.get(j);
				if ((qc.columnIndex == count) && (qc.type != QueryColumn.MEASURE))
				{
					if (firstp)
						firstp = false;
					else
						query.append(',');
					if (!addedColumns.contains(qc) && !qc.hidden)
						resultProjectionList.add(qc);
					query.append(processProjectionListQueryItem(qc, 0, null, fullOuterJoin ? navList : null, j, usePhysicalNames, dbc, specifyDataTypes,
							isSubQuery));
					if (fullOuterJoin)
					{
						groupByClauses.add(getProjectionListItem(qc, 0, null, fullOuterJoin ? navList : null, j, false).toString());
					}
				}
			}
			// Then add measure column from each derived query (last in query)
			// Navigate order bys back to the first subquery
			navigateClauses(navList.get(0).getQueryColumnList());
			for (int j = 0; j < navList.size(); j++)
			{
				nav = (Navigation) navList.get(j);
				List<QueryColumn> navPList = nav.getProjectionList();
				/*
				 * Go through all columns in the projection list for this navigation and include all of the measure
				 * columns
				 */
				for (int k = 0; k < navPList.size(); k++)
				{
					QueryColumn qc = (QueryColumn) navPList.get(k);
					if (qc.columnIndex == count)
					{
						if (qc.type == QueryColumn.MEASURE)
						{
							if (firstp)
								firstp = false;
							else
								query.append(',');
							if (!addedColumns.contains(qc) && !qc.hidden)
								resultProjectionList.add(qc);
							query.append(processProjectionListQueryItem(qc, j, null, null, 0, usePhysicalNames, dbc, specifyDataTypes, isSubQuery));
							// See if need to add an order by
							for (int i = 0; i < orderByClauses.size(); i++)
							{
								OrderBy ob = (OrderBy) orderByClauses.get(i);
								String obc = ob.getOuterQueryClause(dbc, this, qc, j, null, 0, fullOuterJoin);
								//Choose the qc with no filter over the one with filter if there are multiple options available for the same column
								if (obc != null && (orderedOrderBys[i] == null || qc.filter == null))
								{
									orderedOrderBys[i] = obc;
									anyOrderBys = true;
								}
							}
						}
						else
						{
							// Add the appropriate order by clauses
							for (int i = 0; i < orderByClauses.size(); i++)
							{
								OrderBy ob = (OrderBy) orderByClauses.get(i);
								String obc = ob.getOuterQueryClause(dbc, this, qc, j, fullOuterJoin ? navList : null, k, fullOuterJoin);
								if (obc != null && !orderByClausesAdded.contains(obc))
								{
									orderedOrderBys[i] = obc;
									orderByClausesAdded.add(obc);
									anyOrderBys = true;
								}
							}
						}
					}
				}
			}
			// Then add derived measure columns
			for (int j = 0; j < derivedMeasureColumns.size(); j++)
			{
				MeasureColumnNav mcn = (MeasureColumnNav) derivedMeasureColumns.get(j);
				if (mcn.colIndex == count)
				{
					MeasureColumn mc = null;
					if (mcn.matchingMeasures.size() > 0)
						mc = mcn.matchingMeasures.get(0);
					QueryColumn qc = new QueryColumn(QueryColumn.MEASURE, resultProjectionList.size(), mc, mcn.displayName);
					qc.filter = mcn.filter;
					// Don't add to the projection list if it's a hidden column (e.g. only used in a filter)
					if (!mcn.hidden)
					{
						if (firstp)
							firstp = false;
						else
							query.append(',');
						if (!addedColumns.contains(qc) && !qc.hidden)
							resultProjectionList.add(qc);
						query.append(processProjectionListDerivedQueryItem(qc, j, usePhysicalNames, dbc, specifyDataTypes, isSubQuery));
					}
					// See if need to add an order by
					for (int i = 0; i < orderByClauses.size(); i++)
					{
						OrderBy ob = (OrderBy) orderByClauses.get(i);
						String obc = ob.getOuterQueryClause(dbc, this, qc, 0, null, 0, fullOuterJoin);
						if (obc != null)
						{
							orderedOrderBys[i] = obc;
							anyOrderBys = true;
						}
					}
				}
			}
		}
		// Stop here if doing the join in the middle tier
		if (containsMultipleQueries && !simulateOuterJoinUsingPersistedDQTs)
		{
			return;
		}
		// Google Analytics
		if (dbType == DatabaseType.GoogleAnalytics)
		{
			query = GoogleAnalyticsQuery.generateQuery(topn, resultProjectionList, filters, orderByClauses);
			return;
		} else if (DatabaseConnection.isDBTypeXMLA(dbType))
		{
			// Microsoft Analysis Services
			query = MDXQuery.generateQuery(r, dbType, topn, resultProjectionList, filters, dbc, orderByClauses, validateOnly, this);
			requiresDispalySortingForXMLA = dbc.requiresDispalySortingForXMLA();
			return;
		}
		/*
		 * Use all non-derived dimension columns as keys to join measure tables and derived attribute query tables (if
		 * they are key columns)
		 */
		List<QueryColumn> qcList = navList.get(0).getProjectionList();
		// Save a list of all columns that are used in the joins
		whereColumns = new ArrayList<Set<String>>();
		for (int i = 0; i < navList.size(); i++)
			whereColumns.add(new HashSet<String>());
		for (QueryColumn qc : qcList)
		{
			if (qc.type == QueryColumn.DIMENSION)
			{
				DimensionColumn dc = (DimensionColumn) qc.o;
				if (qc.levelKey)
				{
					for (int i = 1; i < navList.size(); i++)
					{
						QueryColumn qc2 = null;
						List<QueryColumn> qcList2 = navList.get(i).getProjectionList();
						for (QueryColumn qci : qcList2)
						{
							if (qci.type == QueryColumn.DIMENSION)
							{
								DimensionColumn dc2 = null;
								dc2 = (DimensionColumn) qci.o;
								if (dc2.matchesDimensionName(dc) && dc2.ColumnName.equals(dc.ColumnName))
								{
									qc2 = qci;
									break;
								}
							}
						}
						if (qc2 != null)
						{
							StringBuilder where = new StringBuilder();
							/*
							 * This change to join on null columns is probably right everywhere, but just doing now for
							 * Infobright. Better yet, we should probably test to see if this column is indeed a level
							 * key (the flag just means that we need to use it in the join - for queries where the level
							 * key is actually not present, all dimension columns are marked as level keys). If it
							 * really is a level key then it must be unique and a null check is not necessary.
							 */
							if (DatabaseConnection.isDBTypeInfoBright(dbc.DBType) || DatabaseConnection.isDBTypeSAPHana(dbc.DBType))
								where.append("COALESCE(");
							where.append(getDerivedQueryTableName(i));
							where.append('.');
							String qc2str = columnMap.getMap(qc2);
							where.append(qc2str);
							if (DatabaseConnection.isDBTypeInfoBright(dbc.DBType))
							{
								where.append(",0)");
							}
							if(DatabaseConnection.isDBTypeSAPHana(dbc.DBType))
							{
								if(qc2.getDataType()==Types.TIMESTAMP)
									where.append(",'" + DateUtil.unUsedDate + "')");
								else
									where.append(",0)");
							}
							where.append('=');
							if (DatabaseConnection.isDBTypeInfoBright(dbc.DBType) || DatabaseConnection.isDBTypeSAPHana(dbc.DBType))
								where.append("COALESCE(");
							where.append(getDerivedQueryTableName(0));
							where.append('.');
							String qcstr = columnMap.getMap(qc);
							where.append(qcstr);
							if (DatabaseConnection.isDBTypeInfoBright(dbc.DBType))
							{
								where.append(",0)");
							}
							if(DatabaseConnection.isDBTypeSAPHana(dbc.DBType))
							{
								if(qc2.getDataType()==Types.TIMESTAMP)
									where.append(",'" + DateUtil.unUsedDate + "')");
								else
									where.append(",0)");
							}
							if (!whereColumns.get(0).contains(qcstr))
								whereColumns.get(0).add(qcstr);
							if (!whereColumns.get(i).contains(qc2str))
								whereColumns.get(i).add(qc2str);
							if (whereClauses[i - 1] != null)
							{
								whereClauses[i - 1].append(" AND ");
								whereClauses[i - 1].append(where);
							} else
							{
								whereClauses[i - 1] = where;
							}
						}
					}
				}
			}
		}
		// Add outer order by clauses not contained in the projection list
		qcList = ((Navigation) navList.get(0)).getQueryColumnList();
		for (int j = 0; j < qcList.size(); j++)
		{
			QueryColumn qc = (QueryColumn) qcList.get(j);
			if (!pList.contains(qc))
			{
				// See if need to add an order by
				for (int i = 0; i < orderByClauses.size(); i++)
				{
					OrderBy ob = (OrderBy) orderByClauses.get(i);
					String obc = ob.getOuterQueryClause(dbc, this, qc, 0, null, 0, fullOuterJoin);
					if (obc != null)
					{
						orderedOrderBys[i] = obc;
						anyOrderBys = true;
						
						// Also add a group by if there are measures
						boolean measures = false;
						for (QueryColumn pqc : pList)
						{
							if (pqc.type == QueryColumn.MEASURE)
							{
								measures = true;
								break;
							}
						}
						if (measures)
							groupByClauses.add(ob.getOuterItem(dbc, this, qc, 0, null, 0, fullOuterJoin));
					}
				}
			}
		}
		/*
		 * Make sure there are where clauses if there is an outer join. Otherwise, treat as inner join. Also if the flag
		 * simulateOuterJoinUsingPersistedDQTs is set to true, outer join would have been already simulated using content
		 * of individual persisted DQTs so dont need to use full join syntax here.
		 */
		boolean useOuterJoin = fullOuterJoin;
		if ((whereClauses.length == 0) || (whereClauses[0] == null) || simulateOuterJoinUsingPersistedDQTs)
			useOuterJoin = false;
		// Add FROM clause
		query.append(" FROM ");
		for (int i = 0; i < navList.size(); i++)
		{
			if (i > 0)
			{
				if (useOuterJoin)
				{
					query.append(' ' + dbc.getFullJoin() + ' ');
				} else
				{
					query.append(',');
				}
			}
			if (expandDerivedQueryTables)
			{
				query.append('(');
				query.append(derivedTables[i].getQuery());
				query.append(") ");
				query.append(getDerivedQueryTableName(i));				
			}
			else
			{
				query.append(Database.getQualifiedTableName(dbc, getDerivedQueryTableName(i)));
			}

			if (useOuterJoin && (i > 0) && whereClauses[i - 1] != null)
			{
				query.append(" ON ");
				query.append(whereClauses[i - 1]);
			}
		}
		// Add WHERE clause if needed
		boolean firstWhere = true;
		if (!useOuterJoin && (whereClauses.length > 0))
		{
			for (int k = 0; k < whereClauses.length; k++)
			{
				if (whereClauses[k] != null)
				{
					if (!firstWhere)
						query.append(" AND ");
					else
					{
						query.append(" WHERE ");
						firstWhere = false;
					}
					query.append(whereClauses[k]);
				}
			}
		}
		/*
		 * If doing an outer join, then use a group by to coalesce duplicates
		 */
		if (fullOuterJoin && (groupByClauses.size() > 0))
		{
			query.append(" GROUP BY ");
			for (int i = 0; i < groupByClauses.size(); i++)
			{
				if (i > 0)
					query.append(',');
				query.append(groupByClauses.get(i));
			}
		}
		/*
		 * Add any outer join filters if necessary
		 */
		StringBuilder outerFilter = new StringBuilder();
		for (QueryFilter qf : filters)
		{
			String s = qf.getOuterQueryFilterString(this, fullOuterJoin);
			if (s != null)
			{
				if (outerFilter.length() > 0)
					outerFilter.append(" AND ");
				outerFilter.append(s);
			}
		}
		if (outerFilter.length() > 0)
		{
			// If it's a full outer join, use a having clause, otherwise just append it to the where clause
			if (fullOuterJoin)
			{
				query.append(" HAVING ");
				query.append(outerFilter);
			} else
			{
				if (!firstWhere)
					query.append(" AND ");
				else
				{
					query.append(" WHERE ");
					firstWhere = false;
				}
				query.append(outerFilter);
			}
		}
		// Add ORDER BY clause
		if (anyOrderBys)
		{
			query.append(" ORDER BY ");
			boolean first = true;
			for (int i = 0; i < orderedOrderBys.length; i++)
			{
				if (orderedOrderBys[i] == null)
					continue;
				if (!first)
					query.append(',');
				first = false;
				query.append(orderedOrderBys[i]);
			}
		}
		boolean topApplied = false;
		if(hasTop() && DatabaseConnection.isDBTypeInfoBright(dbc.DBType) && dcToKeepInProjectionList != null && dbc.supportsLimit())
		{
			query.append(" LIMIT " + getPhysicalTopn(dbc) + " ");
			topApplied = true;
		}
		// Wrap query if necessary
		if (addedColumns.size() > 0 || dcToKeepInProjectionList != null)
		{
			query = wrapQuery(dbc, pList, addedColumns, dcToKeepInProjectionList);
		}
		if (hasTop() && !topApplied)
		{
			if (dbc.supportsTop())
				query.replace(0, "SELECT".length(), "SELECT TOP " + getPhysicalTopn(dbc));
			else if (dbc.supportsLimit())
				query.append(" LIMIT " + getPhysicalTopn(dbc) + " ");
			else if (dbc.supportsFetchFirstRows())
				query.append(" FETCH FIRST " + getPhysicalTopn(dbc) + " ROWS ONLY ");
			else if (dbc.supportsRowNum())
				wrapWithRownum(query, getPhysicalTopn(dbc));
		}
		String qv = r.replaceVariables(null, query.toString());
		query = new StringBuilder(qv);
	}

	/**
	 * Wrap an inner query to remove columns
	 * 
	 * @param pList
	 * @param addedColumns
	 * @return StringBuilder
	 */
	private StringBuilder wrapQuery(DatabaseConnection dbc, List<QueryColumn> pList, Set<QueryColumn> addedColumns, 
			DimensionColumn dcToKeep)
	{
		// First, get a list of all added dimension (to know how to aggregate)
		Set<String> addedDimensions = new HashSet<String>();
		Map<OrderBy, String> pnameByOrderByClause = new HashMap<OrderBy, String>();
		String name = null;
		StringBuilder sb = new StringBuilder();
		StringBuilder gb = new StringBuilder();		
		sb.append("SELECT ");

		if(dcToKeep != null)
		{
			String dname = dcToKeep.getDimensionName();
			String cname = dcToKeep.ColumnName;
			boolean dcToKeepFound = false;
			for (int j = 0; j < pList.size(); j++)
			{
				QueryColumn qc = (QueryColumn) pList.get(j);
				if(qc.type == QueryColumn.DIMENSION)
				{
					if(!dcToKeepFound)
					{
						DimensionColumn dc = (DimensionColumn) qc.o;
						if (dc.getDimensionName().equals(dname) && dc.ColumnName.equals(cname))
						{
							sb.append("S.");
							name = Util.replaceWithPhysicalString(dbc, qc.name);
							sb.append(name);
							sb.append(dbc.getTableReference(qc.name, false));
							dcToKeepFound = true;
							isWrappedQueryWithOneColumn = true;
						}
					}
				}
				OrderBy.addToProjectionNameByOrderByClauseMap(orderByClauses, qc, "S." + Util.replaceWithPhysicalString(dbc, qc.name), pnameByOrderByClause);
			}
		}
		else
		{
			for (QueryColumn qc : pList)
			{
				if (qc.type == QueryColumn.DIMENSION)
				{
					addedDimensions.add(((DimensionColumn) qc.o).DimensionTable.DimensionName);
				}
			}
			boolean firstp = true;
			boolean firstgb = true;
			for (int count = 0; count < colIndex; count++)
			{
				for (int j = 0; j < pList.size(); j++)
				{
					QueryColumn qc = (QueryColumn) pList.get(j);
					
					if (!addedColumns.contains(qc))
					{
						if ((qc.columnIndex == count) && (qc.type != QueryColumn.MEASURE))
						{
							if (firstp)
								firstp = false;
							else
							{
								sb.append(',');
							}
							if (firstgb)
								firstgb = false;
							else
							{
								gb.append(',');
							}
							sb.append("S.");
							name = Util.replaceWithPhysicalString(dbc, qc.name);
							sb.append(name);
							sb.append(dbc.getTableReference(qc.name, false));
							gb.append("S.");
							gb.append(name);
							OrderBy.addToProjectionNameByOrderByClauseMap(orderByClauses, qc, "S." + name, pnameByOrderByClause);
						}
					}
				}
				// Then add measure column from each derived query (last in query)
				for (int j = 0; j < navList.size(); j++)
				{
					Navigation nav = navList.get(j);
					List<QueryColumn> navPList = nav.getProjectionList();
					/*
					 * Go through all columns in the projection list for this navigation and include all of the measure
					 * columns
					 */
					for (int k = 0; k < navPList.size(); k++)
					{
						QueryColumn qc = (QueryColumn) navPList.get(k);
						if (qc.columnIndex == count)
						{
							if (qc.type == QueryColumn.MEASURE)
							{
								if (firstp)
									firstp = false;
								else
									sb.append(',');
								MeasureColumn mc = (MeasureColumn) qc.o;
								name = Util.replaceWithPhysicalString(dbc, qc.name);
								//Promote the aggregation rule from count to sum since we are wrapping the query
								String aggRule = mc.getAggregationRule(addedDimensions);
								if(aggRule != null && aggRule.equalsIgnoreCase("COUNT"))
								{
									aggRule = "SUM";
								}
								String aggString = mc.getAggregationString(dbc, "S." + name, null, aggRule);
								sb.append(aggString);
								sb.append(dbc.getTableReference(qc.name, false));
								OrderBy.addToProjectionNameByOrderByClauseMap(orderByClauses, qc, aggString, pnameByOrderByClause);
							}
						}
					}
				}
				// Then add derived measure columns
				for (int j = 0; j < derivedMeasureColumns.size(); j++)
				{
					MeasureColumnNav mcn = (MeasureColumnNav) derivedMeasureColumns.get(j);
					if (mcn.colIndex == count)
					{
						// Don't add to the projection list if it's a hidden column (e.g. only used in a filter)
						if (!mcn.hidden)
						{
							if (firstp)
								firstp = false;
							else
								sb.append(',');
							MeasureColumn mc = mcn.getMeasureColumn();
							name = Util.replaceWithPhysicalString(dbc, mcn.displayName);
							String aggString = mc.getAggregationString(dbc, "S." + name, null, mc.getAggregationRule(addedDimensions));
							sb.append(aggString);
							sb.append(dbc.getTableReference(mcn.displayName, false));
							OrderBy.addToProjectionNameByOrderByClauseMap(orderByClauses, new QueryColumn(QueryColumn.MEASURE, resultProjectionList.size(), mc,
									mcn.displayName), aggString, pnameByOrderByClause);
						}
					}
				}
			}
		}
		sb.append(" FROM (");
		String querystr = query.toString();
		// Get rid of the previously generated order by clause since we are wrapping the query.
		int indexob = querystr.lastIndexOf("ORDER BY");
		if (indexob > 0 && orderByClauses.size() > 0 && !DatabaseConnection.isDBTypeInfoBright(dbc.DBType))
		{
			querystr = querystr.substring(0, indexob - 1);
		}
		sb.append(querystr);
		sb.append(") S ");
		if (gb != null && gb.length() > 0)
		{
			sb.append(" GROUP BY ");
			sb.append(gb);
		}
		if(dcToKeep == null || (hasTop() && !DatabaseConnection.isDBTypeInfoBright(dbc.DBType)))
		{
			String orderby = OrderBy.buildOrderByStrFromMap(orderByClauses, pnameByOrderByClause);
			sb.append(orderby);
		}
		return (sb);
	}

	/**
	 * Generates the query string for this instance using a session. Does session variable replacement
	 * 
	 * @throws NavigationException
	 * @throws RepositoryException 
	 * @throws SQLException 
	 * @throws ScriptException 
	 * @throws ArrayIndexOutOfBoundsException 
	 * @throws IOException 
	 */
	public void generateQuery(Session session, boolean allowMultipleSources) throws BaseException, CloneNotSupportedException,
			SQLException, ArrayIndexOutOfBoundsException, IOException
	{
		this.session = session;
		generateQuery(session, false, allowMultipleSources);
		if (query != null)
		{
			String qv = r.replaceVariables(session, query.toString());
			qv = r.replaceVariables(session, qv);
			query = new StringBuilder(qv);
		} else if (containsMultipleQueries && !simulateOuterJoinUsingPersistedDQTs)
		{
			for (DerivedTableQuery dtq : derivedTables)
			{
				if (dtq == null)
					continue;
				StringBuilder qvb = dtq.getQuery();
				if (qvb == null)
					continue;
				String qv = r.replaceVariables(session, qvb.toString());
				qv = r.replaceVariables(session, qv);
				dtq.setQuery(new StringBuilder(qv));
			}
		}
	}

	/**
	 * @return Returns the vcDimensionTables.
	 */
	public List<DimensionTable> getVcDimensionTables()
	{
		return vcDimensionTables;
	}

	/**
	 * @return Returns the aggregations.
	 */
	public List<Aggregation> getAggregations()
	{
		return aggregations;
	}

	/**
	 * Return a list of the repository columns that have been supplied to this query (in order). No navigation is done.
	 * 
	 * @return A list of DimensionColumnNav and MeasureColumnNav objects
	 */
	public List<Object> getSuppliedQueryColumns()
	{
		List<Object> list = new ArrayList<Object>();
		for (int count = 0; count < colIndex; count++)
		{
			for (DimensionColumnNav dcn : dimensionColumns)
			{
				if (dcn.colIndex == count && !dcn.navigateonly)
					list.add(dcn);
			}
			for (DimensionMemberSetNav dms : dimensionMemberSets)
			{
				if (dms.colIndex == count && !dms.navigateonly)
					list.add(dms);
				else if (dms.parentColIndex > 0 && dms.parentColIndex == count && !dms.navigateonly)
					list.add(dms);
				else if (dms.parentNameColIndex > 0 && dms.parentNameColIndex == count && !dms.navigateonly)
					list.add(dms);
				else if (dms.ordinalColIndex > 0 && dms.ordinalColIndex == count && !dms.navigateonly)
					list.add(dms);
				else if (dms.uniqueNameColIndex > 0 && dms.uniqueNameColIndex == count && !dms.navigateonly)
					list.add(dms);
			}
			for (OlapMemberExpression exp : olapMemberExpressions)
			{
				if (exp.colIndex == count)
					list.add(exp);
			}
			for (MeasureColumnNav mcn : measureColumns)
			{
				if (mcn.colIndex == count && !mcn.navigateonly && !mcn.hidden)
					list.add(mcn);
			}
			for (MeasureColumnNav mcn : derivedMeasureColumns)
			{
				if (mcn.colIndex == count && !mcn.navigateonly && !mcn.hidden)
					list.add(mcn);
			}
		}
		return (list);
	}

	/**
	 * Return the number of columns that have been added to the query (hidden or not)
	 * 
	 * @return
	 */
	public int getNumColumns()
	{
		return (measureColumns.size() + dimensionColumns.size() + derivedMeasureColumns.size());
	}

	/**
	 * @return Returns the fullOuterJoin.
	 */
	public boolean isFullOuterJoin()
	{
		return fullOuterJoin;
	}

	/**
	 * @return Returns the repository.
	 */
	public Repository getRepository()
	{
		return r;
	}

	public void setRepository(Repository r)
	{
		this.r = r;
	}

	/**
	 * Return the number of measure columns
	 * 
	 * @return
	 */
	public int getNumMeasures()
	{
		return (measureColumns.size());
	}

	/**
	 * Returns a list of column keys for a given query
	 * 
	 * @param includeOrderBys
	 *            Whether to include columns contained in the order by clause (needed if one has to order a given cache
	 *            result)
	 * @return
	 */
	public Set<String> getKeyList(boolean includeOrderBys)
	{
		Set<String> list = new HashSet<String>();
		for (DimensionColumnNav dcn : dimensionColumns)
			if (!dcn.navigateonly)
				list.add(dcn.getKey());
		for (DimensionMemberSetNav dms : dimensionMemberSets)
			if (!dms.navigateonly)
				list.add(dms.getKey());
		for (OlapMemberExpression exp : olapMemberExpressions)
			list.add(exp.getKey());
		for (MeasureColumnNav mcn : measureColumns)
			if (!mcn.hidden && !mcn.navigateonly)
				list.add(mcn.getKey());
		for (MeasureColumnNav mcn : derivedMeasureColumns)
			if (!mcn.hidden && !mcn.navigateonly)
				list.add(mcn.getKey());
		if (includeOrderBys)
			for (OrderBy ob : orderByClauses)
			{
				list.add(ob.getKey());
			}
		return (list);
	}

	/**
	 * Returns list of filters
	 * 
	 * @return
	 */
	public List<QueryFilter> getFilterList()
	{
		return (this.filters);
	}
	
	/**
	 * Get all filters in the query, but expand ands into individual filters 
	 * @return
	 */
	public List<QueryFilter> getExpandedFilterList()
	{
		List<QueryFilter> resultList= new ArrayList<QueryFilter>();
		for(QueryFilter qf: filters)
		{
			if (qf.getType() == QueryFilter.TYPE_LOGICAL_OP && qf.getOperator().equals("AND"))
				resultList.addAll(getExpandedFilterList(qf));
			else
				resultList.add(qf);
		}
		return resultList;
	}
	
	private List<QueryFilter> getExpandedFilterList(QueryFilter qf)
	{
		List<QueryFilter> resultList = new ArrayList<QueryFilter>();
		if (qf.getType() != QueryFilter.TYPE_LOGICAL_OP || (qf.getType() == QueryFilter.TYPE_LOGICAL_OP && !qf.getOperator().equals("AND")))
		{
			resultList.add(qf);
			return resultList;
		}
		for (QueryFilter qf2 : qf.getFilterList())
		{
			if (qf2.getType() == QueryFilter.TYPE_LOGICAL_OP && qf2.getOperator().equals("AND"))
				resultList.addAll(getExpandedFilterList(qf2));
			else
				resultList.add(qf2);
		}
		return resultList;
	}
	
	/**
	 * does the query contain any query filters that are AND/OR and contain a mixture of measure and attribute filters
	 * - those do not match the inexact cache very well
	 * @return
	 */
	public boolean containsMixedQueryFilters()
	{
		for(QueryFilter qf: filters)
		{
			if (!qf.containsOnlyMeasures() && !qf.containsOnlyAttributes())
				return true;
		}
		return false;
	}
	
	private List<QueryFilter> getSplitFilterList(List<QueryFilter> list)
	{
		List<QueryFilter> splitFilterList = new ArrayList<QueryFilter>();
		for (QueryFilter qf : list)
		{
			if (qf.getType() != QueryFilter.TYPE_LOGICAL_OP)
			{
				splitFilterList.add(qf);
			}
			else
			{
				if (qf.getOperator().equals("AND"))
				{
					splitFilterList.addAll(getSplitFilterList(qf.getFilterList()));
				}
				else
				{
					splitFilterList.add(qf); // do we need to collapse a tree of OR's into a single OR?
				}
			}
		}
		return splitFilterList;
	}
	
	public List<QueryFilter> getSplitFilterList()
	{
		return getSplitFilterList(getFilterList());
	}

	/**
	 * Return list of group by clauses
	 * 
	 * @return
	 */
	public List<GroupBy> getGroupByClauses()
	{
		return (this.groupByClauses);
	}

	/**
	 * Return list of order by clauses
	 * 
	 * @return
	 */
	public List<OrderBy> getOrderByClauses()
	{
		return (this.orderByClauses);
	}

	/**
	 * Returns whether the results of this query can be aggregated (i.e. all the measures in the query are additive
	 * along the grouped dimensions)
	 * 
	 * @return
	 */
	public boolean isAdditive(List<GroupBy> gblist)
	{
		for (MeasureColumnNav mcn : measureColumns)
			if (!mcn.isAdditive(gblist))
				return (false);
		for (MeasureColumnNav mcn : derivedMeasureColumns)
			if (!mcn.isAdditive(gblist))
				return (false);
		return (true);
	}

	/**
	 * Returns whether the results of this query can be aggregated (i.e. all the measures in the query are additive
	 * along the grouped dimensions) - only considers list of measures provided
	 * 
	 * @return
	 */
	public boolean isAdditive(List<String> measureList, List<GroupBy> gblist)
	{
		for (MeasureColumnNav mcn : measureColumns)
			if (measureList.contains(mcn.measureName) && !mcn.isAdditive(gblist))
				return (false);
		for (MeasureColumnNav mcn : derivedMeasureColumns)
			if (measureList.contains(mcn.measureName) && !mcn.isAdditive(gblist))
				return (false);
		return (true);
	}

	/**
	 * Return whether this query is available for execution - the query must be both navigated and contain data
	 * connections that are valid for queries (i.e. are not of type None)
	 * 
	 * @return
	 */
	public boolean available()
	{
		if (navList == null || navList.size() == 0)
			return false;
		try
		{
			for (Navigation nav : navList)
			{
				DatabaseConnection dc = nav.getNavigationConnection();
				if ((dc == null) || !dc.isValid())
					return false;
			}
		} catch (NavigationException ex)
		{
			logger.error("Navigation Exception", ex);
			return false;
		}
		return true;
	}

	/**
	 * Get a list of the database connections associated with a query
	 * 
	 * @return
	 */
	public List<DatabaseConnection> getConnections()
	{
		if (navList == null || navList.size() == 0)
			return null;
		List<DatabaseConnection> list = new ArrayList<DatabaseConnection>();
		try
		{
			for (Navigation nav : navList)
			{
				DatabaseConnection c = nav.getNavigationConnection();
				if ((c != null) && c.isValid())
					if (!list.contains(c))
						list.add(c);
			}
		} catch (NavigationException ex)
		{
			logger.error("Navigation Exception", ex);
			return null;
		}
		return list;
	}

	/**
	 * Create a list of database connection statements associated with a query - these can be retrieved before executing
	 * a query in order to execute the query asynchronously and reserve the ability to cancel
	 * 
	 * @return
	 */
	public List<DatabaseConnectionStatement> getStatements()
	{
		if (navList == null || navList.size() == 0)
			return (null);
		List<DatabaseConnection> dclist = getConnections();
		List<DatabaseConnectionStatement> list = new ArrayList<DatabaseConnectionStatement>();
		for (DatabaseConnection dc : dclist)
		{
			DatabaseConnectionStatement statement = null;
			try
			{
				if (dc.isRealTimeConnection() || dc.DBType == DatabaseType.GoogleAnalytics)
				{
					statement = new DatabaseConnectionStatement(r, dc);
				} else
				{
					if (dc.ConnectionPool.isDynamicConnnection() && session != null)
						statement = new DatabaseConnectionStatement(dc, dc.ConnectionPool.getDynamicConnection(session).createStatement(), r);
					else
						statement = new DatabaseConnectionStatement(dc, dc.ConnectionPool.getConnection().createStatement(), r);
				}
			} catch (Exception e)
			{
				/*
				 * Consume and report exception to the log file - this way, if a query can potentially be served from
				 * the cache, the execution will continue. If not, it will fail at a later stage, throwing data
				 * unavailable exception.
				 */
				logger.error(e, e);
			}
			if (statement != null)
			{
				list.add(statement);
			}
		}
		return (list);
	}

	/**
	 * Returns whether this query is cacheable (must be called after the query is navigated)
	 * 
	 * @return
	 */
	public boolean isCacheable()
	{
		if (this.topn == 0) // do not try to cache top(0) since that is used only for fetching the meta data
		{
			return false;
		}
		if (navList == null || navList.size() == 0)
			return (false);
		for (Navigation nav : navList)
		{
			if (!nav.isCacheable())
				return (false);
		}
		return (true);
	}

	/**
	 * Replaces session variables in the query filters. Only replaces if the query key hasn't been generated (don't want
	 * to replace after a key is generated as the key may be invalidated)
	 * 
	 * @param session
	 */
	public void replaceVariables(Session session) throws CloneNotSupportedException
	{
		if (this.queryKey != null)
			return;
		if ((filters != null) && (filters.size() > 0))
		{	
			for (int i = 0; i < filters.size(); i++)
			{
				QueryFilter filter = filters.get(i);
				filters.set(i, filter.replaceVariables(r, session));
			}
		}
		if((measureColumns != null) && (measureColumns.size() > 0))
        {
              for(MeasureColumnNav mcnav : measureColumns)
              {
                    if(mcnav.filter != null)
                    {
                    	mcnav.filter = mcnav.filter.replaceVariables(r, session);
                    }
              }
        }
        if((derivedMeasureColumns != null) && (derivedMeasureColumns.size() > 0))
        {
              for(MeasureColumnNav mcnav : derivedMeasureColumns)
              {
                    if(mcnav.filter != null)
                    {
                    	mcnav.filter = mcnav.filter.replaceVariables(r, session);
                    }
              }
        }
	}

	/**
	 * Method to determine if the query is using a given logical table.
	 * 
	 * @param logicalTableName
	 * @return true/false
	 */
	public boolean usesLogicalTable(String logicalTableName)
	{
		if ((logicalTableNames == null) || (logicalTableNames.isEmpty()))
		{
			return false;
		}
		return logicalTableNames.contains(logicalTableName);
	}

	public Set<String> getLogicalTables()
	{
		return logicalTableNames;
	}

	public Set<String> getPhysicalTables()
	{
		return physicalTableNames;
	}

	/**
	 * Method to determine if the query is using a given physical table.
	 * 
	 * @param physicalTableName
	 * @return true/false
	 */
	public boolean usesPhysicalTable(String physicalTableName)
	{
		if ((physicalTableNames == null) || (physicalTableNames.isEmpty()))
		{
			return false;
		}
		return physicalTableNames.contains(physicalTableName);
	}

	/**
	 * Two array lists are maintained and serialized within the query objects for tracking the logical and physical
	 * table names being used by the query - for performance reasons. Gathers all logical and physical table names that
	 * are used during the query. This method should be called only after the query has been navigated. It browses thru
	 * the data structures of dimension/measure columns' navigations, matching dimensions/measures, table source and
	 * table definition.
	 */
	private void updateTableNamesInfo()
	{
		logicalTableNames.clear();
		physicalTableNames.clear();
		for (int i = 0; i < navList.size(); i++)
		{
			Navigation nav = navList.get(i);
			List<DimensionTable> dtList = nav.getDimensionTables();
			if ((dtList != null) && (!dtList.isEmpty()))
			{
				for (DimensionTable dt : dtList)
				{
					logicalTableNames.add(dt.TableName);
					if ((dt.Type == 0) && (dt.TableSource != null)) // Normal table type
					{
						TableDefinition[] tables = dt.TableSource.Tables;
						if ((tables != null) && (tables.length > 0))
						{
							for (TableDefinition td : tables)
							{
								physicalTableNames.add(td.PhysicalName);
							}
						}
					}
				}
			}
			List<MeasureTable> mtList = nav.getMeasureTables();
			if ((mtList != null) && (!mtList.isEmpty()))
			{
				for (MeasureTable mt : mtList)
				{
					logicalTableNames.add(mt.TableName);
					if ((mt.Type == 0) && (mt.TableSource != null)) // Normal table type
					{
						TableDefinition[] tables = mt.TableSource.Tables;
						if ((tables != null) && (tables.length > 0))
						{
							for (TableDefinition td : tables)
							{
								physicalTableNames.add(td.PhysicalName);
							}
						}
					}
				}
			}
		}
	}

	private void clearMatchingColumnsInfo()
	{
		if(dimensionColumns != null && dimensionColumns.size() > 0)
		{
			for(DimensionColumnNav dcn : dimensionColumns)
			{
				if(dcn.matchingColumns != null)
				{
					dcn.matchingColumns.clear();
				}
			}
		}
		if(measureColumns != null && measureColumns.size() > 0)
		{
			for(MeasureColumnNav mcn : measureColumns)
			{
				if(mcn.matchingMeasures != null)
				{
					mcn.matchingMeasures.clear();
				}
			}
		}
	}
	
	private void clearAddedGroupBys()
	{
		if(addedGroupBys != null && addedGroupBys.size() > 0)
		{
			for(GroupBy gb : addedGroupBys)
			{
				groupByClauses.remove(gb);
			}
			addedGroupBys.clear();
		}
	}
	
	private void updateMatchingColumnsInfo(Navigation nav)
	{
		if (nav!=null)
		{
			List<DimensionColumnNav> navDimColumns = nav.getDimColumns();
			if(navDimColumns != null && navDimColumns.size() > 0 && dimensionColumns != null && dimensionColumns.size() > 0)
			{
				for(DimensionColumnNav navdcn: navDimColumns)
				{
					for(DimensionColumnNav dcn : dimensionColumns)
					{
						if(navdcn.dimensionName.equals(dcn.dimensionName) && navdcn.columnName.equals(dcn.columnName) 
							&& ((navdcn.displayName == null && dcn.displayName == null)|| (navdcn.displayName!=null && navdcn.displayName.equals(dcn.displayName))))
						{
							if(dcn.matchingColumns == null || dcn.matchingColumns.size() == 0)
							{
								dcn.matchingColumns = navdcn.matchingColumns;
							}
							break;
						}
					}
				}
			}
			
			MeasureColumnNav navmcn = nav.getMeasureColumnNav();
			if(navmcn != null && measureColumns != null && measureColumns.size() > 0)
			{
				for(MeasureColumnNav mcn : measureColumns)
				{
					if(navmcn.measureName.equals(mcn.measureName) 
							&& ((navmcn.displayName == null && mcn.displayName == null)|| (navmcn.displayName!=null && navmcn.displayName.equals(mcn.displayName))))
					{
						if(mcn.matchingMeasures == null || mcn.matchingMeasures.size() == 0)
						{
							mcn.matchingMeasures = navmcn.matchingMeasures;
						}
						break;
					}
				}
			}
		}
	}
	
	/**
	 * Return the value of a session variable that was bound to this query during key creation. Returns null if variable
	 * not set.
	 * 
	 * @param name
	 * @return
	 */
	public String getVariableValue(String name)
	{
		if (variableMap == null)
			return (null);
		return (variableMap.get(name));
	}

	/**
	 * Return the variable map (mapped session variables during key creation)
	 * 
	 * @return
	 */
	public Map<String, String> getVariableMap()
	{
		return (variableMap);
	}

	/**
	 * @return the addVolatileMeasure
	 */
	public boolean addVolatileMeasure()
	{
		return addVolatileMeasure;
	}

	/**
	 * @param addVolatileMeasure
	 *            the addVolatileMeasure to set
	 */
	public void setAddVolatileMeasure(boolean addVolatileMeasure)
	{
		this.addVolatileMeasure = addVolatileMeasure;
	}

	/**
	 * Add a dimension column to the list of columns to be mapped after the query result set is returned.
	 * 
	 * @param dcn
	 */
	public void addMappedDimensionColumn(DimensionColumnNav dcn)
	{
		if (mappedDimensionColumns == null)
			mappedDimensionColumns = new ArrayList<DimensionColumnNav>();
		mappedDimensionColumns.add(dcn);
	}

	/**
	 * Return whether this query has mapped dimension columns
	 * 
	 * @return
	 */
	public boolean hasMappedColumns()
	{
		return (mappedDimensionColumns != null);
	}

	/**
	 * @return the mappedDimensionColumns
	 */
	public List<DimensionColumnNav> getMappedDimensionColumns()
	{
		return mappedDimensionColumns;
	}

	/**
	 * @return the volatileCacheQuery
	 */
	public boolean isVolatileCacheQuery()
	{
		return volatileCacheQuery;
	}

	/**
	 * @param volatileCacheQuery
	 */
	public void setVolatileCacheQuery(boolean volatileCacheQuery)
	{
		this.volatileCacheQuery = volatileCacheQuery;
	}

	/**
	 * Explain the navigation for a given query
	 * 
	 * @return
	 */
	public String explainNavigation(boolean isSuperUser)
	{
		if (!this.hasBeenNavigated())
			return "Query not navigated";
		StringBuilder sb = new StringBuilder();
		if (derivedMeasureStringReplacements.size() > 0)
		{
			sb.append("Query contains derived measures:\n");
			Set<MeasureColumn> replaceSet = new HashSet<MeasureColumn>(derivedMeasureStringReplacements.size());
			for (MeasureStringReplace msr : derivedMeasureStringReplacements)
			{
				if (msr.foundmc != null && !replaceSet.contains(msr.replacemc))
				{
					sb.append("\tMeasure M{" + msr.foundmc.ColumnName + "} contains measure M{" + msr.replacemc.ColumnName + "}\n");
					replaceSet.add(msr.replacemc);
				}
			}
		}
		for (Navigation nav : navList)
		{
			sb.append(nav.explain(isSuperUser));
		}
		return (sb.toString());
	}

	/**
	 * Turn an existing column, which is navigate-only and therefore hidden, into an exposed column - column is added to
	 * the end of the query
	 * 
	 * @param dcn
	 */
	public void exposeNavigateOnlyDimensionColumn(DimensionColumnNav dcn)
	{
		dcn.navigateonly = false;
		int maxIndex = -1;
		for (DimensionColumnNav curDcn : dimensionColumns)
		{
			if (curDcn.colIndex > maxIndex)
				maxIndex = curDcn.colIndex;
		}
		dcn.colIndex = maxIndex + 1;
		if (colIndex <= dcn.colIndex)
			colIndex = dcn.colIndex + 1;
	}

	/**
	 * Return a formatted version of the SQL query string that is easier to read
	 * 
	 * @return
	 */
	public String getPrettyQuery()
	{
		return getPrettyQuery(getQuery(), false);
	}

	public static String getPrettyQuery(String qry)
	{
		return getPrettyQuery(qry, false);
	}

	public static String getPrettyQuery(StringBuilder qry)
	{
		return getPrettyQuery(qry.toString(), false);
	}

	public static String getPrettyQuery(String qry, boolean fancy)
	{
		return getNotQuiteSoPrettyQuery(qry);
	}

	/**
	 * 
	 * @param qry
	 * @return String
	 */
	private static String getNotQuiteSoPrettyQuery(String qry)
	{
		if (qry == null)
			return (null);
		int indent = 0;
		// do some initial cleanup of the query - removing newlines and tabs
		qry = qry.replaceAll("[\n\t]+", " ");
		StringBuilder sb = new StringBuilder();
		for (int pos = 0; pos < qry.length(); pos++)
		{
			if (qry.charAt(pos) == '(')
				indent++;
			else if (qry.charAt(pos) == ')')
				indent--;
			if (qry.startsWith("SELECT", pos) || qry.startsWith("FROM", pos) || qry.startsWith("WHERE", pos) || qry.startsWith("GROUP BY", pos)
					|| qry.startsWith("ORDER BY", pos) || qry.startsWith("HAVING", pos) || qry.startsWith("INNER JOIN", pos)
					|| qry.startsWith("LEFT OUTER JOIN", pos) || qry.startsWith("RIGHT OUTER JOIN", pos) || qry.startsWith("FULL OUTER JOIN", pos)
					|| qry.startsWith("UNION", pos) || qry.startsWith("INTO", pos))
			{
				if (pos > 0 && qry.startsWith("SELECT", pos) && qry.charAt(pos - 1) == '(')
				{
					sb.insert(sb.length() - 1, '\n');
					for (int j = 0; j < indent; j++)
						sb.insert(sb.length() - 1, '\t');
				} else
				{
					sb.append('\n');
					for (int j = 0; j < indent; j++)
						sb.append('\t');
				}
			}
			sb.append(qry.charAt(pos));
		}
		return (sb.toString());
	}

	/**
	 * @return the usesLevelSpecificJoin
	 */
	public boolean usesLevelSpecificJoin()
	{
		return usesLevelSpecificJoin;
	}

	/**
	 * Return whether a top n parameter was used
	 * 
	 * @return
	 */
	public boolean hasTop()
	{
		return (topn >= 0);
	}

	/**
	 * Return whether a display top n parameter was used
	 * 
	 * @return
	 */
	public boolean hasDTop()
	{
		return (dtopn >= 0);
	}
	
	public Set<Level> getJoinLevels()
	{
		if (!hasBeenNavigated())
		{
			return null;
		}
		Set<Level> queryJoinLevels = new HashSet<Level>();
		for (Navigation nav : navList)
		{
			queryJoinLevels.addAll(nav.getJoinLevels());
		}
		return queryJoinLevels;
	}

	public String getDerivedQueryTableName(int index)
	{
		StringBuilder sb = new StringBuilder("DQT");
		sb.append(index);
		sb.append("_D");
		if (derivedQueryTableSuffix != null)
			sb.append(derivedQueryTableSuffix);
		return sb.toString();
	}

	public String getTempTableNameForDQTProcessing()
	{
		return "temp_" + derivedQueryTableSuffix; 
	}
	
	public String getNameWithSpecificDataType(String name, QueryColumn qc, DatabaseConnection dbc)
	{
		Database.DatabaseType dbType = dbc.DBType;
		StringBuilder sb = new StringBuilder();
		String dataType = null;
		if ((DatabaseConnection.isDBTypeMSSQL(dbType)) && (qc.type == QueryColumn.DIMENSION || qc.type == QueryColumn.MEASURE))
		{
			if (qc.type == QueryColumn.DIMENSION)
			{
				DimensionColumn dc = (DimensionColumn) qc.o;
				dataType = Util.getDBDataType(dc.DataType);
			} else if (qc.type == QueryColumn.MEASURE)
			{
				MeasureColumn mc = (MeasureColumn) qc.o;
				dataType = Util.getDBDataType(mc.DataType);
				String rule = mc.getAggregationRule(null);
				if (rule != null && dataType != null && dataType.equalsIgnoreCase("Integer") && rule.equalsIgnoreCase("SUM"))
				{
					dataType = "BIGINT";
				}
			}
			if (dataType != null && !dataType.equalsIgnoreCase("VARCHAR"))
			{
				sb.append("CONVERT(");
				sb.append(dataType);
				sb.append(",");
				sb.append(name);
				sb.append(")");
			} else
			{
				sb.append(name);
			}
		} else if (DatabaseConnection.isDBTypeInfoBright(dbType) && qc.type == QueryColumn.MEASURE)
		{
			MeasureColumn mc = (MeasureColumn) qc.o;
			String rule = mc.getAggregationRule(null);
			boolean isNumber = mc.DataType != null && (mc.DataType.equalsIgnoreCase("Integer") || mc.DataType.equalsIgnoreCase("Float") || mc.DataType.equalsIgnoreCase("Number"));
			if (rule != null && (rule.equalsIgnoreCase("SUM") || rule.equalsIgnoreCase("AVG") || isNumber))
			{
				dataType = dbc.getNumericType();
				sb.append("CAST(");
				sb.append(name);
				sb.append(" AS ");
				sb.append(dataType);
				sb.append(")");
			}
			else
			{
				sb.append(name);
			}
		} else
		{
			sb.append(name);
		}
		return sb.toString();
	}

	/**
	 * @return the session
	 */
	public Session getSession()
	{
		return session;
	}

	/**
	 * set the session
	 */
	public void setSession(Session session)
	{
		this.session = session;
	}
	
	public boolean hasOuterQuery()
	{
		return (!(navList.size() == 1 && derivedMeasureColumns.size() == 0));
	}

	/**
	 * Go thru the list of measure columns to see if there is a non-derived measure available - if yes, return it. If
	 * no, return the derived measure.
	 * 
	 * @param mclist
	 *            List of measure columns
	 * @return selected measure column - first non-derived measure if available, if not, the derived measure
	 */
	private MeasureColumn getNonDerivedMeasureIfPresent(List<MeasureColumn> mclist)
	{
		/*
		 * See if there is a derived table for this measure. If so, and there are also non-derived measures, pick the
		 * first non-derived measure. Note: In the future it would be even better to check whether the non-derived
		 * measure is at a higher or lower cardinality than the non-derived and make sure it joins - but normally this
		 * cannot be determined until after navigation.
		 */
		MeasureColumn mc = null;
		MeasureColumn dmc = null;
		for (MeasureColumn mci : mclist)
		{
			if (mci.MeasureTable.Type == MeasureTable.UNMAPPED)
			{
				continue;
			}
			if (mci.MeasureTable.Type != MeasureTable.DERIVED)
			{
				mc = mci;
				break;
			} else
				dmc = mci;
		}
		if (mc == null)
		{
			mc = dmc;
		}
		return mc;
	}

	/**
	 * @return the topn
	 */
	public int getTopn()
	{
		return topn;
	}
	
	/**
	 * @return a topN that works with all DBs (i.e., turn 0 to 1 for IB)
	 */
	public int getPhysicalTopn(DatabaseConnection dc)
	{
		if (topn != 0)
			return topn;
		if (dc.supportsLimit0())
			return 0;
		return 1;
	}

	/**
	 * @param topn
	 *            the topn to set
	 */
	public void setTopn(int topn)
	{
		this.topn = topn;
	}

	/**
	 * @return the resultsetcache
	 */
	public ResultSetCache getResultSetCache()
	{
		return rsc;
	}

	/**
	 * @param rsc
	 *            the resultsetcache to set
	 */
	public void setResultSetCache(ResultSetCache rsc)
	{
		this.rsc = rsc;
	}

	public static void updateLevelMapForLevelKeys(Repository r, Map<String, Level> lmap, List<DimensionColumnNav> dimColumns)
	{
		// Prepare a map containing dimension column names by dimension name
		Map<String, List<String>> dimColNamesMap = new HashMap<String, List<String>>();
		for (DimensionColumnNav dn : dimColumns)
		{
			if (dn.constant)
			{
				continue;
			}
			List<String> dimColNamesList = dimColNamesMap.get(dn.dimensionName);
			if (dimColNamesList == null)
			{
				dimColNamesList = new ArrayList<String>();
				dimColNamesMap.put(dn.dimensionName, dimColNamesList);
			}
			dimColNamesList.add(dn.columnName);
		}
		// Check to see if there are level keys present in the query and using the level keys, if we can find a level
		// lower than
		// the currently assigned one for each dimension.
		for (String dim : dimColNamesMap.keySet())
		{
			Level dimLevel = lmap.get(dim);
			Hierarchy h = r.findDimensionHierarchy(dim);
			if (h != null)
			{
				List<String> dimCols = dimColNamesMap.get(dim);
				Level lkMatchLevel = h.findLevelContainingKeyColumns(dimCols);
				if (lkMatchLevel != null)
				{
					if (dimLevel == null || (dimLevel != null && dimLevel.findLevel(lkMatchLevel) != null))
					{
						lmap.put(dim, lkMatchLevel);
					}
				}
			}
		}
	}

	/**
	 * @return the derivedQueryTableSuffix
	 */
	public String getDerivedQueryTableSuffix()
	{
		return derivedQueryTableSuffix;
	}

	/**
	 * @param derivedQueryTableSuffix
	 *            the derivedQueryTableSuffix to set
	 */
	public void setDerivedQueryTableSuffix(String derivedQueryTableSuffix)
	{
		this.derivedQueryTableSuffix = derivedQueryTableSuffix;
	}

	/**
	 * @return the derivedTables
	 */
	public DerivedTableQuery[] getDerivedTables()
	{
		return derivedTables;
	}

	/**
	 * @return the expandDerivedQueryTables
	 */
	public boolean isExpandDerivedQueryTables()
	{
		return expandDerivedQueryTables;
	}

	/**
	 * @param expandDerivedQueryTables
	 *            the expandDerivedQueryTables to set
	 */
	public void setExpandDerivedQueryTables(boolean expandDerivedQueryTables)
	{
		this.expandDerivedQueryTables = expandDerivedQueryTables;
	}

	public boolean isSimulateOuterJoinUsingPersistedDQTs()
	{
		return simulateOuterJoinUsingPersistedDQTs;
	}

	public void setSimulateOuterJoinUsingPersistedDQTs(boolean simulateOuterJoinUsingPersistedDQTs)
	{
		this.simulateOuterJoinUsingPersistedDQTs = simulateOuterJoinUsingPersistedDQTs;
	}
	
	/**
	 * @return the whereColumns
	 */
	public List<Set<String>> getWhereColumns()
	{
		return whereColumns;
	}

	public List<QueryResultSet> getMapResultSets()
	{
		return mapResultSets;
	}

	public boolean hasMoreThanOneDimension()
	{
		boolean hasMoreThanOneDimension = false;
		String previousDimension = null;
		for (DimensionColumnNav dcn : dimensionColumns)
		{
			if (previousDimension != null && !(previousDimension.equals(dcn.dimensionName)))
			{
				hasMoreThanOneDimension = true;
				break;
			}
			previousDimension = dcn.dimensionName;
		}
		return hasMoreThanOneDimension;
	}

	public boolean containsDimensionColumn(String dimName, String colName)
	{
		DimensionColumnNav dcn = this.findDimensionColumnNav(dimName, colName);
		if (dcn == null)
		{
			return false;
		}
		return true;
	}

	public boolean containsMeasureColumn(String measureColName)
	{
		MeasureColumnNav mcn = this.findMeasureColumnNav(measureColName);
		if (mcn == null)
		{
			return false;
		}
		return true;
	}

	/**
	 * Convert all OrderBy clauses to display order and add the display orders to the given list
	 * 
	 * @param q
	 *            Query
	 * @param displayOrderList
	 */
	protected void convertAndAddOrderBysAsDisplayOrders(List<DisplayOrder> displayOrderList)
	{
		if (orderByClauses != null && orderByClauses.size() > 0)
		{
			for (OrderBy ob : orderByClauses)
			{
				DisplayOrder dispOrder = ob.returnDisplayOrder();
				/*
				 * Add display order to the end of the list since the display orders are processed from bottom of the
				 * list and we want these display orders (derived from orderbys) to be processed first.
				 */
				if (displayOrderList == null)
					displayOrderList = new ArrayList<DisplayOrder>();
				displayOrderList.add(dispOrder);
			}
		}
	}

	/**
	 * Remove all order by clauses
	 */
	protected void removeAllOrderBys()
	{
		if (orderByClauses != null && orderByClauses.size() > 0)
		{
			orderByClauses.clear();
		}
	}

	public Map<DatabaseConnection, List<TempTable>> getTempTableMap()
	{
		if (tempTableMap == null && session != null)
		{
			Map<DatabaseConnection, List<TempTable>> ttmap = session.getTempTableMap();
			if (ttmap != null)
				return ttmap;
		}			
		return tempTableMap;
	}

	public void setTempTableMap(Map<DatabaseConnection, List<TempTable>> tempTableMap)
	{
		this.tempTableMap = tempTableMap;
	}

	/**
	 * @return the exactListMatch used for set-based security filter
	 */
	public List<String> getExactMatchList()
	{
		return exactListMatch;
	}

	public static void wrapWithRownum(StringBuilder query, int topN)
	{
		query.insert(0, "Select * FROM ( ");
		query.append(" ) ");
		query.append(" WHERE ROWNUM <= " + topN + " ");
	}

	/**
	 * Return the primary navigation
	 * 
	 * @return
	 */
	public Navigation getPrimaryNavigation()
	{
		if (navList.size() > 0)
			return navList.get(0);
		return null;
	}

	public List<DisplayOrder> getDisplayOrders()
	{
		return displayOrders;
	}

	public void setDisplayOrders(List<DisplayOrder> displayOrders)
	{
		this.displayOrders = displayOrders;
	}

	public List<Navigation> getNavigationList()
	{
		return navList;
	}
	
	/**
	 * Marks all dimension as well as measure columns as navigateonly except for the one dimension column that is supplied as
	 * dcToKeep parameter
	 * @param dcToKeep Dimension Column to keep in the projection list
	 * @return false if the dcToKeep was not found in the dimension column list, true otherwise
	 */
	void setDimensionColumnToKeep(DimensionColumn dcToKeep) throws SyntaxErrorException
	{
		//See if there is only one dimension column that will eventually end up in projection list. If that is the case
		//we don't need to set dcToKeep and wrap the query later.
		
		int numMeasureColumns = 0;
		for(MeasureColumnNav mcn : measureColumns)
		{
			if(!mcn.navigateonly && !mcn.hidden)
			{
				numMeasureColumns++;
			}
		}
		
		for(MeasureColumnNav mcn : derivedMeasureColumns)
		{
			if(!mcn.navigateonly && !mcn.hidden)
			{
				numMeasureColumns++;
			}
		}
		
		if(numMeasureColumns == 0)
		{
			int numDimColumns = 0;			
			//No measures or derived measures, now look at dimension columns to see how may of those will be exposed in projection list
			for(DimensionColumnNav dcn : dimensionColumns)
			{
				if(!dcn.navigateonly)
				{
					numDimColumns++;
				}
			}
			if(numDimColumns == 1)
			{
				//Only one dimension column in projection list - return without setting dcToKeep
				return;
			}
		}
		
		//Make sure that we can find the dimension column to keep
		boolean foundDcToKeep = false;
		for(DimensionColumnNav dcn : dimensionColumns)
		{
			if(dcn.dimensionName.equals(dcToKeep.DimensionTable.DimensionName) && dcn.columnName.equals(dcToKeep.ColumnName) && !dcn.navigateonly)
			{
				foundDcToKeep = true;
				this.dcToKeepInProjectionList = dcToKeep;
				break;
			}
		}
		//See if there any dim column with matching data type, if so keep it
		if(!foundDcToKeep)
		{
			String dcToKeepDataType = dcToKeep.DataType;
			for(DimensionColumnNav dcn : dimensionColumns)
			{
				if(dcn.navigateonly)
				{
					continue;
				}
				DimensionColumn curdc = r.findDimensionColumn(dcn.dimensionName, dcn.columnName);
				if(curdc != null && dcToKeepDataType.equals(curdc.DataType))
				{
					foundDcToKeep = true;
					this.dcToKeepInProjectionList = curdc;
					break;
				}
			}
			
		}
		
		//Didnt find anything, throw an exception
		if(!foundDcToKeep)
		{
			throw new SyntaxErrorException("Dimension column " + dcToKeep.DimensionTable.DimensionName+"." + dcToKeep.ColumnName +
					" does not exist on the filter query.");
		}
	}
	
	public boolean isVirtualColumnQuery()
	{
		return isVirtualColumnQuery;
	}
	
	public void setVirtualColumnQuery(boolean hasVirtualColumn)
	{
		this.isVirtualColumnQuery = hasVirtualColumn; 
	}
	
	public void setValidateOnly(boolean validateOnly)
	{
		this.validateOnly = validateOnly; 
	}
	
	public boolean isValidateOnly()
	{
		return validateOnly; 
	}

	/**
	 * To determine if there are any hidden measure columns that were added because of a filter on measure
	 * column that doesnt exist in the select list of the query.
	 * @return
	 */
	public boolean hasHiddenMeasureColumns()
	{
		if(this.measureColumns != null && this.measureColumns.size() > 0)
		{
			for(MeasureColumnNav mcn : measureColumns)
			{
				if(mcn.hidden)
				{
					return true;
				}
			}
		}
		return false;
	}
	/**
	 * For multiple queries scenario, we join the resultsets in memory and then push the query filters up
	 * to become display filters. Display filters can be bound only if the given column is actually part
	 * of the result projection list.
	 * Check to see if the query is a potential candidate for containing multiple queries. If so, 
	 * identify all hidden measure columns in the query that have corresponding query filters and unhide
	 * them. At the same time, add those display names to addedNames list which is later used to take the
	 * columns out from the resultset that is returned to client. 
	 * @param session
	 * @param addedNames
	 * @throws NavigationException
	 * @throws BadColumnNameException
	 * @throws CloneNotSupportedException
	 */
	public void unhideMeasureColumnsForMultipleQueries(Session session, List<String> addedNames) throws NavigationException, BadColumnNameException, CloneNotSupportedException
	{
		if(canResultInMultipleQueries(session))
		{
			for(MeasureColumnNav mcn : measureColumns)
			{
				//Fix display names to address the case when you have same measure twice one with 
				//and other one without filter to avoid duplicate display names.
				if(mcn.measureName.equals(mcn.displayName) && mcn.filter != null)
				{
					mcn.displayName = mcn.displayName + Math.abs(mcn.filter.toString().hashCode());
				}
			}			
			boolean foundMeasuresToUnhide = false;
			if(fullOuterJoin && filters != null && filters.size() > 0)
			{
				for(QueryFilter qf : filters)
				{
					if(!qf.containsMeasure())
					{
						continue;
					}
					for(MeasureColumnNav mcn : measureColumns)
					{
						if(mcn.hidden && qf.containsMeasure(mcn.measureName))
						{
							foundMeasuresToUnhide = true;
							mcn.hidden = false;
							mcn.colIndex = colIndex;
							colIndex++;
							addedNames.add(mcn.displayName);
						}
					}
				}
			}
			if(foundMeasuresToUnhide)
			{
				clearNavigation();
			}
		}
	}
	
	/**
	 * Whether this query can potentially result in multiple queries
	 * @param session  
	 * @return true - can result in multuple queries, false - not
	 * @throws NavigationException
	 * @throws BadColumnNameException
	 * @throws CloneNotSupportedException
	 */
	public boolean canResultInMultipleQueries(Session session) throws NavigationException, BadColumnNameException, CloneNotSupportedException
	{
		if(!hasBeenNavigated())
		{
			navigateQuery(false, true, session, true);
		}

		if (getConnections().size() > 1)
		{
			return true;
		}
		for (Navigation nav : navList)
		{
			// Disallow full outer join for MySQL and Infobright (not allowed)
			DatabaseConnection dconn = nav.getNavigationConnection();
			if (DatabaseConnection.isDBTypeMySQL(dconn.DBType) || dconn.DBType == DatabaseType.Infobright || DatabaseConnection.isDBTypeMemDB(dconn.DBType))
			{
				if (fullOuterJoin)
					return true;
			}
		}
		for(MeasureColumnNav mcnav: this.measureColumns)
		{
			String pf = mcnav.getMeasureColumn().PhysicalFilter;
			if (pf != null && pf.length() > 0)
				return true;
		}
		return false;
	}
	
	public boolean hasOrderBys()
	{
		if(orderByClauses != null && orderByClauses.size() > 0)
		{
			return true;
		}
		return false;
	}
	
	public boolean containsFilterOnMeasureColumn()
	{
		if(filters == null || filters.size() == 0)
		{
			return false;
		}
		boolean contains = false;		
		for(QueryFilter qf : filters)
		{
			if(qf.containsMeasure())
			{
				contains = true;
				break;
			}
		}
		return contains;
		
	}
	
	public boolean containsFilteredMeasure()
	{
		boolean contains = false;
		boolean hasMeasureFilter = false;
		if(filters != null && filters.size() > 0)
		{
			for(QueryFilter qf : filters)
			{
				if(qf.hasMeasureFilter())
				{
					hasMeasureFilter = true;
					break;
				}
			}
		}
		if(!hasMeasureFilter && navList != null && navList.size() > 0)
		{
			for (Navigation nav : navList)
			{
				MeasureColumnNav mcn = nav.getMeasureColumnNav();
				if(mcn != null && mcn.filter != null)
				{
					contains = true;
					break;
				}
			}
		}
		return contains;
	}
	
	public void removeDimensionColumn(DimensionColumnNav dc)
	{
		if ((dimensionColumns == null || dimensionColumns.size() == 0) || (!dimensionColumns.contains(dc)))
		{
			// Nothing to remove
			return;
		}
		int removeDcColIndex = dc.colIndex;
		dimensionColumns.remove(dc);
		this.colIndex--;
		for (DimensionColumnNav remainingDc : dimensionColumns)
		{
			if (remainingDc.colIndex > removeDcColIndex)
			{
				remainingDc.colIndex--;
			}
		}
		for (MeasureColumnNav remainingMc : measureColumns)
		{
			if (remainingMc.colIndex > removeDcColIndex)
			{
				remainingMc.colIndex--;
			}
		}
	}
	
	public void removeMeasureColumn(MeasureColumnNav mc)
	{
		if ((measureColumns == null || measureColumns.size() == 0) || (!measureColumns.contains(mc)))
		{
			// Nothing to remove
			return;
		}
		int removeDcColIndex = mc.colIndex;
		measureColumns.remove(mc);
		this.colIndex--;
		for (DimensionColumnNav remainingDc : dimensionColumns)
		{
			if (remainingDc.colIndex > removeDcColIndex)
			{
				remainingDc.colIndex--;
			}
		}
		for (MeasureColumnNav remainingMc : measureColumns)
		{
			if (remainingMc.colIndex > removeDcColIndex)
			{
				remainingMc.colIndex--;
			}
		}
	}
	
	private boolean addSourceFiltersForMDX(Navigation nav, List<QueryFilter> filters, Repository r, Session session) throws CloneNotSupportedException, NavigationException, BadColumnNameException
	{
		boolean filtersAdded = false;
		List<QueryFilter> tsfList = MDXQuery.extractTableSourceFilters(this, r, session);
		if(tsfList != null && !tsfList.isEmpty())
		{
			for(QueryFilter tsf : tsfList)
			{
				if(!filters.contains(tsf))
				{
					addFilter(tsf, true);
					filtersAdded = true;
				}
			}
		}
		return filtersAdded;
	}

	/**
	 * Persist the derived tables for a given query
	 * 
	 * @param conn
	 * @throws SQLException
	 */
	public void persistDerivedQueryTables(DatabaseConnection conn) throws SQLException
	{
		/*
		 * Persist derived query tables if needed
		 */
		DerivedTableQuery[] derivedTables = getDerivedTables();
		List<Set<String>> whereColumns = getWhereColumns();
		if (!isExpandDerivedQueryTables())
		{
			if (derivedTables != null)
			{
				for (int i = 0; i < derivedTables.length; i++)
				{
					// Create table
					String queryString = r.replaceVariables(null, derivedTables[i].getQuery().toString());
					Database.createTableAs(r, conn, getDerivedQueryTableName(i), queryString, true);
					// Create index for join - use all columns in a single index
					if (whereColumns.size() > 0 && whereColumns.get(i).size() > 0)
					{
						LevelKey lk = new LevelKey();
						lk.ColumnNames = new String[whereColumns.get(i).size()];
						int count = 0;
						for (String s : whereColumns.get(i))
							lk.ColumnNames[count++] = s;
						Database.createIndex(conn, lk, getDerivedQueryTableName(i), false, false, null, null);
					}
				}
				if(fullOuterJoin && simulateOuterJoinUsingPersistedDQTs)
				{
					/**
					 * IB/MySQL doesn't allow full outer join syntax. So, for those cases, we usually issue separate
					 * sub-queries and join the resultsets as part of QueryResultSet.java. However, when processing 
					 * query aggregates as well as for modeling queries, these individual subquery resultsets can be 
					 * huge and hence the in-memory joining won't be feasible.
					 * So, below is the implementation of an alternate route that is executed when the flag 
					 * simulateOuterJoinUsingPersistedDQTs is set to true. The name of the flag is kind of 
					 * self-explanatory but here are more details anyway:
					 * 1. Each subquery resultset has already been presisted as a result of flag expandDerivedQueryTables
					 * that is set to false. So, if we have two subqueries, DQT0_D_... and DQT1_D_... tables have already
					 * been presisted in the previous step - see the block of code immediately preceding the if condition
					 * above.
					 * 2. In order to apply inner join on these subquery tables (since we can't do a full outerjoin), we
					 * have to make sure that all persisted subquery tables contain the "master-set" of dimension column
					 * values. e.g. in case of Quantity and YAGO Quantity with Time.Year as attribute, DQT0 will contain 
					 * 1994, 1995 and 1996 as opposed to DQT1 that contains 1995, 1996, 1997. In order to simulate the 
					 * outerjoin, we need to make sure that both DQT0 and DQT1 contain 1994, 1995, 1996 and 1997. That
					 * way, when we do inner join on DQT0 and DQT1, the final resulset will contain 1994 thru 1997.
					 * 3. So, the first step is to create a temp table containing the masterset i.e. 1994 thru 1997
					 * in the above example.
					 * 4. Next, we left join each DQT with temp table and figure the row to be inserted into that DQT.
					 * Use of having count(...) = 0 helps us identify the row to be inserted to each DQT. So, again, for
					 * example above, we will insert 1997 to DQT0 and 1994 to DQT1.
					 * 5. Now, since both DQT0 and DQT1 contain all rows, we can safely do an inner join between these 
					 * to achieve the desired results i.e. an effective full outer join.
					 * P.S. Although this feature is implemented keeping in mind IB/MySQL, there is nothing that prevents
					 * usage for other databases like SQL server.
					 */
					String tempTableName = getTempTableNameForDQTProcessing();
					String qualTempTableName = Database.getQualifiedTableName(conn, tempTableName);
					//Create a temporary table holding all combinations of dimension columns across all DQTs
					StringBuilder tempTableSelect = new StringBuilder();
					int navListSize = navList.size();
					for(int i = 0; i < navListSize; i++)
					{
						if(i > 0)
						{
							tempTableSelect.append(" UNION ");
						}
						tempTableSelect.append("SELECT ");
						String plistForDQT = getDQTNonMeasureColumns(conn, i);
						tempTableSelect.append(plistForDQT);
						tempTableSelect.append(" FROM ");
						String dqtName = getDerivedQueryTableName(i);
						tempTableSelect.append(Database.getQualifiedTableName(conn, dqtName));
						tempTableSelect.append(' ');
						tempTableSelect.append(dqtName);
					}
					Database.createTableAs(r, conn, tempTableName, tempTableSelect.toString(), true);
					
					for(int i = 0; i < navListSize; i++)
					{
						List<String> ttColList = getNonMeasureColumnListForTempTable(true);
						List<String> dqtColList = getNonMeasureColumnListForDQT(i);
						//Insert additional rows into the persisted DQTs
						//insert into  S_e20c86f7_efcc_4ee7_8d82_738fb33937b7.DQT0_D_1314779655399 (F10_, CatName1_)
						//select temp1.F10_, temp1.CatName1_ from S_e20c86f7_efcc_4ee7_8d82_738fb33937b7.temp1 temp1
						//left join S_e20c86f7_efcc_4ee7_8d82_738fb33937b7.DQT0_D_1314779655399 DQT0_D_1314768047156
						//on temp1.F10_ = DQT0_D_1314768047156.F10_  and temp1.CatName1_ = DQT0_D_1314768047156.CatName1_
						//group by temp1.F10_, temp1.CatName1_
						//having count(DQT0_D_1314768047156.F10_) = 0
						StringBuilder insertq = new StringBuilder();
						insertq.append("INSERT INTO ");
						String dqtName = getDerivedQueryTableName(i);
						String qualDqtName = Database.getQualifiedTableName(conn, dqtName);
						insertq.append(qualDqtName);
						insertq.append(" (");
						for(int colIdx = 0; colIdx < dqtColList.size(); colIdx++)
						{
							if(colIdx > 0)
							{
								insertq.append(',');
							}
							insertq.append(dqtColList.get(colIdx));
						}
						insertq.append(") ");
						insertq.append(" SELECT ");
						for(int colIdx = 0; colIdx < ttColList.size(); colIdx++)
						{
							if(colIdx > 0)
							{
								insertq.append(',');
							}
							insertq.append(tempTableName);
							insertq.append('.');
							insertq.append(ttColList.get(colIdx));
						}
						//insertq.append(Temp table column list);
						insertq.append(" FROM ");
						insertq.append(qualTempTableName);
						insertq.append(' ');
						insertq.append(tempTableName);
						insertq.append(" LEFT JOIN ");
						insertq.append(qualDqtName);
						insertq.append(' ');
						insertq.append(dqtName);
						for(int colIdx = 0; colIdx < ttColList.size(); colIdx++)
						{
							if(colIdx == 0)
							{
								insertq.append(" ON ");
							}
							else
							{
								insertq.append(" AND ");
							}
							insertq.append(tempTableName);
							insertq.append('.');
							insertq.append(ttColList.get(colIdx));
							insertq.append('=');
							insertq.append(dqtName);
							insertq.append('.');
							insertq.append(dqtColList.get(colIdx));
						}
						insertq.append(" GROUP BY ");
						for(int colIdx = 0; colIdx < ttColList.size(); colIdx++)
						{
							if(colIdx > 0)
							{
								insertq.append(',');
							}
							insertq.append(tempTableName);
							insertq.append('.');
							insertq.append(ttColList.get(colIdx));
						}
						insertq.append(" HAVING COUNT(");
						insertq.append(dqtName);
						insertq.append('.');
						insertq.append(dqtColList.get(0));
						insertq.append(") = 0");

						Connection dc = conn.ConnectionPool.getConnection();
						if (dc == null)
						{
							throw new SQLException("Unable to create table - Cannot connect to database");
						}
						String insertQuery = insertq.toString();
						Statement stmt = dc.createStatement();
						logger.debug(Query.getPrettyQuery(insertQuery));
						int rows = stmt.executeUpdate(insertQuery);
						stmt.close();
						logger.info("Inserted " + rows + " into " + qualDqtName);
					}
					//Drop the temporary table now that we are done with it
					Database.dropTable(r, conn, tempTableName, true);
				}
			}
		}
	}

	private List<String> getNonMeasureColumnListForTempTable(boolean usePhysicalName)
	{
		List<String> colList = new ArrayList<String>();
		Navigation nav = navList.get(0);
		List<QueryColumn> pList = nav.getProjectionList();
		
		for(int i = 0; i < pList.size(); i++)
		{
			QueryColumn qc = pList.get(i);
			if(qc.type == QueryColumn.MEASURE)
			{
				continue;
			}
			colList.add((usePhysicalName ? Util.replaceWithPhysicalString(qc.name) : qc.name));
		}
		
		return colList;
	}
	
	private List<String> getNonMeasureColumnListForDQT(int dqtindex)
	{
		List<String> colList = new ArrayList<String>();
		Navigation nav = navList.get(dqtindex);
		List<QueryColumn> pList = nav.getProjectionList();
		
		for(int i = 0; i < pList.size(); i++)
		{
			QueryColumn qc = pList.get(i);
			if(qc.type == QueryColumn.MEASURE)
			{
				continue;
			}
			colList.add(this.columnMap.getMap(qc));
		}
		
		return colList;
	}
	
	private String getDQTNonMeasureColumns(DatabaseConnection dbc, int dqtindex)
	{
		StringBuilder sb = new StringBuilder();
		Navigation nav = navList.get(dqtindex);
		List<QueryColumn> pList = nav.getProjectionList();
		Set<String> qcNames = new HashSet<String>();
		
		boolean first = true;
		for(int i = 0; i < pList.size(); i++)
		{
			QueryColumn qc = pList.get(i);
			if(qc.type == QueryColumn.MEASURE)
			{
				continue;
			}
			//Make sure not to add duplicates
			if(qcNames.contains(qc.name))
			{
				continue;
			}
			if(!first)
			{
				sb.append(',');
			}
			else
			{
				first = false;
			}
			sb.append(processProjectionListQueryItem(qc, dqtindex, null, null, i, true, dbc, false, true));
			qcNames.add(qc.name);
		}
		
		return sb.toString();
	}
	
	/**
	 * Drops derived query tables for a given query
	 * 
	 * @param conn
	 * @throws SQLException
	 */
	public void dropDerivedQueryTables(DatabaseConnection conn) throws SQLException
	{
		DerivedTableQuery[] derivedTables = getDerivedTables();
		if (!isExpandDerivedQueryTables())
		{
			if (derivedTables != null)
			{
				for (int i = 0; i < derivedTables.length; i++)
				{
					Database.dropTable(r, conn, getDerivedQueryTableName(i), true);
				}
			}
		}
	}

	public boolean isWrappedQueryWithOneColumn() 
	{
		return isWrappedQueryWithOneColumn;
	}

	/**
	 * This method is used only for LiveAccess set based filter
	 * @param dc
	 * @return
	 * @throws SyntaxErrorException 
	 */
	public String getColumnToKeep(DimensionColumn dcToKeep) throws SyntaxErrorException 
	{
		//See if there is only one dimension column that will eventually end up in projection list. If that is the case
				//we don't need to set dcToKeep and wrap the query later.				
		int numMeasureColumns = 0;
		for(MeasureColumnNav mcn : measureColumns)
		{
			if(!mcn.navigateonly && !mcn.hidden)
			{
				numMeasureColumns++;
			}
		}
		
		for(MeasureColumnNav mcn : derivedMeasureColumns)
		{
			if(!mcn.navigateonly && !mcn.hidden)
			{
				numMeasureColumns++;
			}
		}
		
		if(numMeasureColumns == 0)
		{
			int numDimColumns = 0;			
			//No measures or derived measures, now look at dimension columns to see how may of those will be exposed in projection list
			for(DimensionColumnNav dcn : dimensionColumns)
			{
				if(!dcn.navigateonly)
				{
					numDimColumns++;
				}
			}
			if(numDimColumns == 1)
			{
				//Only one dimension column in projection list - return without setting dcToKeep
				return null;
			}	
		}
	
		//Make sure that we can find the dimension column to keep
		boolean foundDcToKeep = false;
		for(DimensionColumnNav dcn : dimensionColumns)
		{
			if(dcn.dimensionName.equals(dcToKeep.DimensionTable.DimensionName) && dcn.columnName.equals(dcToKeep.ColumnName) && !dcn.navigateonly)
			{
				foundDcToKeep = true;
				return dcn.columnName;				
			}
		}
						
		//See if there any dim column with matching data type, if so keep it
		if(!foundDcToKeep)
		{
			String dcToKeepDataType = dcToKeep.DataType;
			for(DimensionColumnNav dcn : dimensionColumns)
			{
				if(dcn.navigateonly)
				{
					continue;
				}
				DimensionColumn curdc = r.findDimensionColumn(dcn.dimensionName, dcn.columnName);
				if(curdc != null && dcToKeepDataType.equals(curdc.DataType))
				{
					foundDcToKeep = true;
					return curdc.ColumnName;					
				}
			}
			
		}
		
		//Didnt find anything, throw an exception
		if(!foundDcToKeep)
		{
			throw new SyntaxErrorException("Dimension column " + dcToKeep.DimensionTable.DimensionName+"." + dcToKeep.ColumnName +
					" does not exist on the filter query.");
		}
		return null;
	}

	public boolean containsFederatedJoin()
	{
		return (fjqh != null);
	}
	
	public FederatedJoinQueryHelper getFederatedJoinQueryHelper()
	{
		return fjqh;
	}
	
	public void setFederatedJoinQueryHelper(FederatedJoinQueryHelper fjqh)
	{
		this.fjqh = fjqh;
	}
	
	public boolean isFederated()
	{
		if (fjqh != null)
			return true;
		if (navList!=null && !navList.isEmpty())
		{
			for (Navigation nav : navList)
			{
				if (nav.containsFederatedJoin())
					return true;
			}
		}
		return false;
	}
}
