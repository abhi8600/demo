/**
 * $Id: DataUnavailableException.java,v 1.2 2009-12-22 19:01:44 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import com.successmetricsinc.util.BaseException;

/**
 * Exception thrown when requested data is unavailable
 * 
 * @author Brad Peters
 * 
 */
public class DataUnavailableException extends BaseException
{
	private static final long serialVersionUID = 1L;

	/**
	 * @param s
	 */
	public DataUnavailableException(String s, Throwable cause)
	{
		super(s, cause);
	}
	
	public DataUnavailableException(String s)
	{
		super(s);
	}

	@Override
	public int getErrorCode() {
		return ERROR_DATA_UNAVAILABLE;
	}
}
