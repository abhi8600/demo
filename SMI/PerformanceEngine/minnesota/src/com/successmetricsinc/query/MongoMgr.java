package com.successmetricsinc.query;

import java.net.UnknownHostException;
import java.util.List;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.Collection;

import org.apache.log4j.Logger;

import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBList;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.Mongo;
import com.mongodb.MongoException;
import com.mongodb.WriteResult;
import com.mongodb.CommandResult;
import com.mongodb.ServerAddress;
import com.mongodb.MongoOptions;

public class MongoMgr<T> {
	protected static Logger logger = Logger.getLogger(MongoMgr.class);
	private static Mongo mongo = null;
	
	//System parameters
	public final static String PR_HOSTNAME = "MongoHostname";
	public final static String PR_PORT = "MongoPort";
	public final static String PR_POOL_SZ = "MongoPoolSize";
	public final static String PR_FS_ROOTDIR = "FileCacheSharedRootDir";
	
	//Default values
	public final static int DEFAULT_PORT = 27017;
	public final static int DEFAULT_POOLSIZE = 50;
	public final static String DEFAULT_HOST = "localhost";
	public final static boolean DEFAULT_USE_FOR_CACHE_KEY = false;
	public final static String DEFAULT_ROOT_FILE_CACHE_DIR = "C://BirstCacheDir";
	
	public static Mongo getMongo() throws UnknownHostException{
		if(mongo == null){
			String hostname = System.getProperty(PR_HOSTNAME);
			mongo = createMongoByHostname(hostname);
		}
		return mongo;
	}
	
	public static Mongo createMongoByHostname(String hostname) throws MongoException, UnknownHostException{
		int port = DEFAULT_PORT;
		try{
			String mPort = System.getProperty(PR_PORT);
			if(mPort != null){
				port = Integer.parseInt(mPort);
			}
		}catch(Exception e){}

		int poolSize = DEFAULT_POOLSIZE;
		try{
			String maxConns = System.getProperty(PR_POOL_SZ);
			if(maxConns != null){
				poolSize = Integer.parseInt(maxConns);
			}
		}catch(Exception e){}

		MongoOptions opts = new MongoOptions();
		opts.connectionsPerHost = poolSize;

		if(hostname == null){
			logger.debug("Using default hostname, MongoDB is at: " + DEFAULT_HOST + ":" + port + " with connection poolsize: " + poolSize);
			return new Mongo(new ServerAddress(DEFAULT_HOST, port), opts);
		}else{
			//Replica sets?
			ArrayList<ServerAddress> addresses = new ArrayList<ServerAddress>();
			String[] addrStrs = hostname.split(",");
			//Single host
			if(addrStrs.length == 1){
				ServerAddress addr = parseHostname(hostname, port);
				if(addr != null){
					logger.debug("Using MongoDB at: " + addr.getHost() + ":" + addr.getPort() + " with connection poolsize: " + poolSize);
					return new Mongo(addr, opts);
				}else{
					logger.debug("Using default hostname, MongoDB is " + DEFAULT_HOST + " at port " + port);
					return new Mongo(new ServerAddress(DEFAULT_HOST, port), opts);
				}
			}else{
				//Multiple hosts
				for(String hostPortStr : addrStrs){
					ServerAddress addr = parseHostname(hostPortStr, port);
					if(addr != null){
						addresses.add(addr);
						logger.debug("Using MongoDB replica set server " + addr.getHost() + " at port " + addr.getPort());
					}
				}
				logger.debug("MongoDB connection poolsize: " + poolSize);
				if(addresses.size() > 0){
					return new Mongo(addresses, opts);
				}else{
					logger.debug("Using default hostname, MongoDB is " + DEFAULT_HOST + " at port " + port);
					return new Mongo(new ServerAddress(DEFAULT_HOST, port), opts);
				}
			}
		}
	}
	
	private static ServerAddress parseHostname(String addrStr, int port) throws UnknownHostException{
		ServerAddress addr = null;
		String[] hostPort = addrStr.split(":");
		if(hostPort.length == 2){
			try{
				port = Integer.parseInt(hostPort[1]);
				addrStr = hostPort[0];
				addr = new ServerAddress(addrStr, port);
			}catch(Exception e){
				logger.error("Unable to parse Mongo hostname: " + addrStr, e);
			}
		}else{
			//User didn't supply the port # in the hostname, use the default port
			try{
				addr = new ServerAddress(addrStr, port);
			}catch(Exception e){
				logger.error("Unable to parse Mongo hostname: " + addrStr, e);
			}
		}
		return addr;
	}
	
	public static String getRootSharedCacheDir(){
		String cacheDir = System.getProperty(PR_FS_ROOTDIR);
		if(cacheDir == null){
			cacheDir = DEFAULT_ROOT_FILE_CACHE_DIR; //assign a default
		}
		return cacheDir;
	}
	
	public static<T> BasicDBList convertToDBList(List<T> l){
		BasicDBList dblist = new BasicDBList();
		if(l != null){
			for(int i = 0; i < l.size(); i++){
				dblist.put(i, ((BasicDBObjectSerializer)l.get(i)).toBasicDBObject());
			}
		}
		return dblist;
	}
	
	public static<T> List<T> convertFromDBList(BasicDBList l, Class<T> clz) throws InstantiationException, IllegalAccessException{
		ArrayList<T> arr = new ArrayList<T>();
		addToCollection(l, arr, clz);
		return arr;
	}
	
	public static<T> Set<T> convertFromDBListToSet(BasicDBList l, Class<T> clz) throws InstantiationException, IllegalAccessException{
		HashSet<T> set = new HashSet<T>();
		addToCollection(l, set, clz);
		return set;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static<T> void addToCollection(BasicDBList l, Collection coll, Class<T> clz) throws InstantiationException, IllegalAccessException{
		if(l != null){
			for(int i = 0; i < l.size(); i++){
				Object o = l.get(i);
				if(o instanceof BasicDBObject){
					T t = clz.newInstance();
					BasicDBObject obj = (BasicDBObject)l.get(i);
					((BasicDBObjectSerializer)t).fromBasicDBObject(obj);
					coll.add(t);
				}else{
					coll.add(o);
				}
			}
		}
	}
	
	public static DB getSpaceDb(String dbName) throws UnknownHostException{
		Mongo m = MongoMgr.getMongo();
		if(!dbName.startsWith("birst-")){
			dbName = "birst-" + dbName;
		}
		return m.getDB(dbName);
	}
	
	public static String getStringByKey(String dbName, String collection, String key){
		String value = null;
		BasicDBObject obj = MongoMgr.getObjectByKey(dbName, collection, key);
		if(obj != null){
			return obj.getString("value");
		}
		return value;
	}
	
	public static BasicDBObject getObjectByKey(String dbName, String collection, String key){
		return getObjectUsingKey(dbName, collection, "key", key);
	}
	
	public static BasicDBObject getObjectByObjectId(String dbName, String collection, Object key){
		return getObjectUsingKey(dbName,  collection, "_id", key);
	}
	
	public static BasicDBObject getObjectByObjectId(DB db, String collection, String key){
		return getObjectUsingKey(db, collection, "_id", key, null);
	}
	
	public static BasicDBObject getObjectUsingKey(String dbName, String collection, String id, Object key){
		DB db;
		
		try{
			db = getSpaceDb(dbName);
		}catch(Exception e){
			logger.error("Cannot get MongoDB database " + dbName, e);
			return null;
		}
		
		return getObjectUsingKey(db, collection, id, key, null);
	}
		
	public static BasicDBObject getObjectUsingKey(DB db, String collection, String id, Object key, List<String> excludeFields){
		try{
			//Use connection pool
			db.requestStart();

			DBCollection coll = db.getCollection(collection);

			BasicDBObject query = new BasicDBObject();
			query.put(id, key);

			DBCursor cursor = null;

			//Exclude certain fields which may be big
			if(excludeFields != null){
				BasicDBObject k = new BasicDBObject();
				for(int i = 0; i < excludeFields.size(); i++){
					k.put(excludeFields.get(i), 0);
				}
				cursor = coll.find(query, k);
			}else{
				cursor = coll.find(query);
			}

			BasicDBObject obj =  cursor.hasNext() ? (BasicDBObject)cursor.next() : null;
						
			return obj;
		}catch(Exception e){
			logger.error("getObjectUsingKey failed", e);
			return null;
		}finally{
			if(db != null){
				db.requestDone();
			}
		}
		
	}
	
	public static WriteResult putObjectByKey(String dbName, String collection, String key, Object obj){
		DB db;
		
		try{
			db = getSpaceDb(dbName);
		}catch(Exception e){
			logger.error("Cannot get MongoDB database " + dbName, e);
			return null;
		}
		
		//Use connection pool
		db.requestStart();
		
		DBCollection coll = db.getCollection(collection);
		
		BasicDBObject query = new BasicDBObject();
		query.put("key", key);
		
		BasicDBObject add = new BasicDBObject();
		add.put("key", key);
		add.put("value", obj);
		
		WriteResult result = coll.update(query, add, true, false);
		
		db.requestDone();
		
		return result;
	}
	
	public static DBCursor find(String dbName, String collection) throws UnknownHostException{
		DB db = getSpaceDb(dbName);
		DBCollection coll =  db.getCollection(collection);
		return coll.find();
	}
	
	public static boolean hasError(CommandResult rs){
		if(!rs.ok() || rs.getErrorMessage() != null || rs.getString("err") != null)
			return true;
		
		return false;
	}
	
	public static int getCode(CommandResult rs)
	{
		return rs.getInt("code");
	}
	
	public static boolean isDuplicateError(CommandResult rs)
	{
		return getCode(rs) == 11000;
	}
}
