/**
 * $Id: MeasureTableGrain.java,v 1.9 2012-11-12 18:25:20 BIRST\ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.io.Serializable;
import java.util.Collection;
import java.util.Comparator;

import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.Repository;
import com.successmetricsinc.util.RepositoryException;
import com.successmetricsinc.util.Util;

/**
 * @author Brad Peters
 * 
 */
public class MeasureTableGrain implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String DimensionName;
	public String DimensionLevel;

	public MeasureTableGrain()
	{
		
	}
	
	public MeasureTableGrain(MeasureTable mt, Element e, Namespace ns) throws RepositoryException
	{
		DimensionName = Util.intern(e.getChildTextTrim("DimensionName", ns));
		DimensionLevel = Util.intern(e.getChildTextTrim("DimensionLevel", ns));
	}	
	
	public MeasureTableGrain(Element e, Namespace ns) throws RepositoryException
  {
    DimensionName = Util.intern(e.getChildTextTrim("DimensionName", ns));
    DimensionLevel = Util.intern(e.getChildTextTrim("DimensionLevel", ns));
  } 
	
	public String toString()
	{
		return (DimensionName + "." + DimensionLevel);
	}
	
	public boolean equals(Object other)
	{
	   if (other == null)
	   {
	      return false;
	   }

	   if (this.getClass() != other.getClass())
	   {
	      return false;
	   }
	   
	   MeasureTableGrain o = (MeasureTableGrain) other;

	   if (!DimensionName.equals(o.DimensionName))
	   {
	      return false;
	   }

	   if (!DimensionLevel.equals(o.DimensionLevel))
	   {
	      return false;
	   }

	   return true;
	}

	public Level getLevel(Repository r)
	{
		Level l = null;
		Hierarchy h = r.findHierarchy(DimensionName);
		if(h != null)
		{
			l = h.findLevel(DimensionLevel);
		}
		return l;
	}
	/**
	 * Return whether grain1 is at a higher grain than grain2
	 * @param r
	 * @param grain1
	 * @param grain2
	 * @return
	 */
	public static boolean isAtHigherGrain(Repository r, Collection<MeasureTableGrain> grain1, Collection<MeasureTableGrain> grain2)
	{
		int lowerCount = 0;
		for (MeasureTableGrain mtg1 : grain1)
		{
			boolean foundDimension = false;
			for (MeasureTableGrain mtg2 : grain2)
			{
				if (mtg1.DimensionName.equals(mtg2.DimensionName))
				{
					foundDimension = true;
					if (!mtg1.DimensionLevel.equals(mtg2.DimensionLevel))
					{
						boolean lower = false;
						Hierarchy h = r.findDimensionHierarchy(mtg1.DimensionName);
						if (h != null)
						{
							Level l1 = h.findLevel(mtg1.DimensionLevel);
							if (l1 != null)
								if (l1.findLevel(mtg2.DimensionLevel) != null)
								{
									lowerCount++;
									lower = true;
									break;
								}
						}
						if (!lower)
							return false;
					}
					break;
				}
			}
			if (!foundDimension)
				return false;
		}
		return (lowerCount > 0 || grain1.size() < grain2.size());
	}
	
	public Element getMeasureTableGrainElement(Namespace ns)
	{
		Element e = new Element("MeasureTableGrain", ns);
		Element child = new Element("DimensionName", ns);		
		child.setText(DimensionName);
		e.addContent(child);		
		child = new Element("DimensionLevel", ns);		
		child.setText(DimensionLevel);
		e.addContent(child);
		return e;
	}
	
	public static Comparator<MeasureTableGrain> MeasureTableGrainComparator 
	= new Comparator<MeasureTableGrain>() {

		public int compare(MeasureTableGrain grain1, MeasureTableGrain grain2) {

			String levelName1 = grain1.DimensionLevel;
			String levelName2 = grain2.DimensionLevel;

			//ascending order
			return levelName1.compareTo(levelName2);
		}

	};
	
}
