/**
 * $Id: QueryColumn.java,v 1.6 2012-08-09 08:37:16 mpandit Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.sql.Types;

import com.successmetricsinc.query.Aggregation.AggregationType;
import com.successmetricsinc.query.olap.DimensionMemberSetNav;
import com.successmetricsinc.query.olap.OlapMemberExpression;

/**
 * General structure for holding a query column
 * @author bpeters
 *
 */
public class QueryColumn
{
    public static final int DIMENSION = 0;
    public static final int MEASURE = 1;
    public static final int CONSTANT_OR_FORMULA = 2;
    public static final int DIMENSION_MEMBER_SET = 3;
    public static final int OLAP_MEMBER_EXPRESSION = 4;
    public int type;
    public int columnIndex;
    String name;
    public Object o;
    boolean levelKey = false;
    boolean hidden = false;
    QueryFilter filter = null;

    /**
     * Initialize a query column
     * 
     * @param type
     *            Type of column
     * @param index
     *            Index within the query (used for final ordering to make sure
     *            result is in the order intended - navigation can reorder
     *            internal structures)
     * @param o
     *            Object (MeasureColumn,DimensionColumn or String)
     * @param name
     *            Name of column
     */
    QueryColumn(int type, int index, Object o, String name)
    {
        this.type = type;
        this.columnIndex = index;
        this.o = o;
        this.name = name;
    }
    
    public String toString()
    {
        return(name);
    }
    
    /**
     * Derives the data type from underlying dimension or measure column. For a constant or formula or any type other than dimension
     * or measure column, this method will return Types.NULL.
     * @return data type
     */
    public int getDataType()
    {
    	int dataType = Types.NULL;
		if (type == MEASURE)
		{
			MeasureColumn mc = (MeasureColumn) o;
			dataType = QueryResultSet.getTypeForDataTypeString(mc.DataType);
		} else if (type == QueryColumn.DIMENSION)
		{
			DimensionColumn dc = (DimensionColumn) o;
			dataType = QueryResultSet.getTypeForDataTypeString(dc.DataType);
		} else if (type == QueryColumn.DIMENSION_MEMBER_SET)
		{
			DimensionMemberSetNav dms = (DimensionMemberSetNav) o;
			if (dms.dataType != null && dms.colIndex == columnIndex)
			{
				dataType = QueryResultSet.getTypeForDataTypeString(dms.dataType);
			}
		}
		return dataType;
	}
}
