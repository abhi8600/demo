package com.successmetricsinc.query;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

public class WrapperDatabaseStatement {

	private static Logger logger = Logger.getLogger(WrapperDatabaseStatement.class);

	private Statement stmt;
	private DatabaseConnection dc;
	private static final int retryCount = 3;	

	public WrapperDatabaseStatement(DatabaseConnection _dc, Statement _stmt) {
		this.dc = _dc;
		this.stmt = _stmt;
	}

	public ResultSet executeQuery(String query) throws SQLException {
		logger.debug("Using WrapperStatement Query:" + query );
		int retries = 0;
		boolean retryError = false;
		ResultSet rs = null;
		do {
			try {

				// only enabled for testing
				if (getIsTesting()) {
					logger.error("SHOULD NEVER HAPPEN IN PRODUCTION");
					throw new SQLException(getTestString());
				}

				rs = stmt.executeQuery(query);
				retryError = false;
			} catch (SQLException e) {
				String msg = e.getMessage();				
				// retry for reset connection -- might add more messages
				if (shouldRetry(msg,retries)) {
					try {
						retries++;
						logger.debug("Retrying connection on reset: retries="+retries);
						Connection conn = dc.ConnectionPool.getConnection();						
						stmt = conn.createStatement();
						retryError = true;
						
					} catch (SQLException e1) {
						logger.error("Error during getting connection for retries");						
						retryError = false;
						throw new SQLException(e1);
					}
				} else {
					if(retries == retryCount){
						logger.error("Number of retries exhausted : " + retries);
					}
					else{
						logger.error("SQLException other than reset encountered: "); 
					}
					logger.error(msg,e);		
					retryError = false;
					throw new SQLException(e);
				}
			}
		} while (retryError == true && (retries < retryCount + 1));

		return rs;
	}

	public int executeUpdate(String query) throws SQLException {
		logger.debug("Using WrapperStatement Update :" + query );
		int retries = 0;
		boolean retryError = false;
		int result = 0;
		do {
			try {

				// only enabled for testing
				if (getIsTesting()) {
					logger.error("SHOULD NEVER HAPPEN IN PRODUCTION");
					throw new SQLException(getTestString());
				}

				result = stmt.executeUpdate(query);
				retryError = false;
			} catch (SQLException e) {
				String msg = e.getMessage();
				// retry for reset connection -- might add more messages
				if (shouldRetry(msg,retries)) {
					try {
						retries++;
						logger.debug("Retrying connection: retries="+retries);
						Connection conn = dc.ConnectionPool.getConnection();
						stmt = conn.createStatement();
						retryError = true;						
					} catch (SQLException e1) {
						logger.error("Error during getting connection for retries");
						retryError = false;
						throw new SQLException(e1);
					}
				} else {
					if(retries == retryCount){
						logger.error("Number of retries exhausted : " + retries);
					}
					else{
						logger.error("SQLException other than reset encountered: "); 
					}						
					logger.error(msg,e);
					retryError = false;
					throw new SQLException(e);
				}
			}
		} while (retryError == true && (retries < retryCount + 1));

		return result;
	}

	private boolean shouldRetry(String message, int retries) {
		boolean retryDecision = false;
		if (message != null && message.contains("Connection reset") && retries < retryCount) {
			retryDecision = true;
		}
		return retryDecision;
	}

	public void close() {
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				// Do nothing
				logger.warn("Unable to close stmt. No need to do anything");
			}
		}
	}
	
	// Used for ONLY TESTING. DO NOT USE ANYWHERE ELSE.
	// Returns false unless overridden by sub class.
	protected boolean getIsTesting(){		
		return false;
	}
	
	// Used for testing. Overridden by test subclasses.
	public String getTestString(){
		return null;
	}
}
