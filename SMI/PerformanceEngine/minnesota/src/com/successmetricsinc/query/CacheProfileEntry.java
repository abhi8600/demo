/**
 * $Id: CacheProfileEntry.java,v 1.2 2007-09-19 20:34:31 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

/**
 * @author Brad Peters
 * 
 */
public class CacheProfileEntry implements Comparable
{
	public String QueryString;
	public int numRows;
	public String[] ColumnNames;
	public int[] ColumnDataTypes;
	public int[] numDistinct;
	public double[] minimums;
	public double[] maximums;
	public double[] averages;
	public double[] deviations;

	public CacheProfileEntry(int numColumns, int numRows)
	{
		this.numRows = numRows;
		ColumnNames = new String[numColumns];
		ColumnDataTypes = new int[numColumns];
		numDistinct = new int[numColumns];
		minimums = new double[numColumns];
		averages = new double[numColumns];
		maximums = new double[numColumns];
		deviations = new double[numColumns];
	}

	public int compareTo(Object o)
	{
		CacheProfileEntry cpe = (CacheProfileEntry) o;
		if (numRows == cpe.numRows)
			return (QueryString.compareTo(cpe.QueryString));
		else
			return (numRows > cpe.numRows ? 1 : -1);
	}
}
