/**
 * $Id: OurRowObject.java,v 1.9 2009-12-18 18:50:44 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.sql.Types;
import java.util.Calendar;

/**
 * Class to wrap row objects for persistence - faster than readObject/writeObject and reflection
 */

public class OurRowObject implements Externalizable {
	private static final long serialVersionUID = 2L;
	private Object[] columns;

	public OurRowObject() {
	}

	public OurRowObject(Object[] cols) {
		columns = cols;
	}

	public Object[] getColumns() {
		return columns;
	}

	public void writeExternal(ObjectOutput stream) throws java.io.IOException {
		int len = columns.length;
		stream.writeInt(len);
		for (int i = 0; i < len; i++) {
			int type = getColumnType(columns[i]);
			stream.writeInt(type);
			switch (type) {
			case Types.INTEGER:
				stream.writeLong((Long) columns[i]);
				break;
			case Types.DOUBLE:
				stream.writeDouble(((Double) columns[i]).doubleValue());
				break;
			case Types.VARCHAR:
				stream.writeUTF((String) columns[i]);
				break;
			case Types.TIMESTAMP:
				stream.writeLong(((Calendar) columns[i]).getTimeInMillis());				
				break;
			default:
				break;
			}
		}
	}

	public void readExternal(ObjectInput stream) throws java.io.IOException {
		int len = stream.readInt();
		columns = new Object[len];
		for (int i = 0; i < len; i++) {
			int type = stream.readInt();
			switch (type) {
			case Types.INTEGER:
				try 
				{
					columns[i] = Long.valueOf(stream.readLong());
				}
				catch (Exception e) 
				{
					columns[i] = Long.valueOf(((Integer)stream.readInt()).longValue());	
				}
				break;
			case Types.DOUBLE:
				columns[i] = Double.valueOf(stream.readDouble());
				break;
			case Types.VARCHAR:
				String str = stream.readUTF();
				if (str != null && str.length() < 128)
					str = str.intern();  // lots of replicated string values, so let's intern them
				columns[i] = str;
				break;
			case Types.TIMESTAMP:
				Calendar cal = Calendar.getInstance();
				cal.setTimeInMillis(stream.readLong());
				columns[i] = cal;				
				break;
			case Types.NULL:
				columns[i] = null;
				break;
			}
		}
	}

	private int getColumnType(Object col) {
		if (col == null) {
			return Types.NULL;
		} else if (col instanceof String) {
			return Types.VARCHAR;
		} else if (col instanceof Integer || col instanceof Long) {
			return Types.INTEGER;
		} else if (col instanceof Double) {
			return Types.DOUBLE;
		} else {
			return Types.TIMESTAMP;
		}
	}
}
