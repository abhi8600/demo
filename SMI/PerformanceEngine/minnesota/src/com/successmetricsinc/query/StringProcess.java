/**
 * $Id: StringProcess.java,v 1.3 2007-09-19 20:34:31 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

public class StringProcess
{
	public static String format(String s, double d)
	{
		return (String.format(s, d));
	}
	public static String format(String s, int d)
	{
		return (String.format(s, d));
	}
}