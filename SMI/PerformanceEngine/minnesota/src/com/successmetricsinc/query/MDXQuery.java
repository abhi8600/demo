/**
 * 
 */
package com.successmetricsinc.query;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.Variable;
import com.successmetricsinc.query.Database.DatabaseType;
import com.successmetricsinc.query.olap.DimensionMemberSetNav;
import com.successmetricsinc.query.olap.MemberSelectionExpression;
import com.successmetricsinc.query.olap.OlapMemberExpression;
import com.successmetricsinc.query.olap.OlapLogicalQueryString.MEMBEREXPRESSIONTYPE;

/**
 * @author bpeters
 * 
 */
public class MDXQuery
{
	private static Logger logger = Logger.getLogger(MDXQuery.class);
	public static StringBuilder generateQuery(Repository r, DatabaseType dbType, int topn, List<QueryColumn> resultProjectionList, List<QueryFilter> filters,
			DatabaseConnection dbc, List<OrderBy> orderByClauses, boolean validateOnly, Query q) throws SyntaxErrorException
	{
		Pattern baseColumn = Pattern.compile("\\[(\\w|\\s)+\\]");
		StringBuilder result = new StringBuilder();
		StringBuilder with = new StringBuilder();
		StringBuilder measureClause = new StringBuilder();
		StringBuilder where = new StringBuilder();
		StringBuilder from = new StringBuilder();
		String cube = null;
		Map<String, List<Integer>> measurePoslist = new LinkedHashMap<String, List<Integer>>();
		Map<String, List<Integer>> dimPoslist = new LinkedHashMap<String, List<Integer>>();
		Map<String, String> ctypeMap = new HashMap<String, String>();
		Map<String, String> cformatMap = new HashMap<String, String>();
		Map<String, String> dimColumnMap = new HashMap<String, String>();
		List<DimensionMemberSetNav> dimMemberSets = new ArrayList<DimensionMemberSetNav>();
		Map<String, String> calculatedMemberMap = new HashMap<String, String>();
		Map<String, String> calculatedMemberQualifiedNameMap = new HashMap<String, String>();
		StringBuilder filterClause = new StringBuilder();
		Random rand = new Random();
		// Find lowest level in each dimension
		Map<String, List<DimensionColumn>> dimMap = new HashMap<String, List<DimensionColumn>>();
		Set<String> projectionListDimensions = new HashSet<String>();
		String NON_EMPTY_FUNCTION = "NON EMPTY";
		boolean useNonEmptyFunction = false;
		int noItemsInMeasureClause = 0;
		int noDimMemberSetsInMeasureClause = 0;
		MeasureTable mt = null;
		for (QueryColumn qc : resultProjectionList)
		{
			if (qc.type == QueryColumn.DIMENSION && qc.o != null)
			{
				DimensionColumn dc = (DimensionColumn) qc.o;
				String dname = dc.DimensionTable.DimensionName;
				if(!projectionListDimensions.contains(dname))
				{
					projectionListDimensions.add(dname);
				}
				updateDimMap(dimMap, dname, dc, r);
			}
			if (qc.type == QueryColumn.DIMENSION_MEMBER_SET && qc.o != null)
			{
				DimensionMemberSetNav dms = (DimensionMemberSetNav)qc.o;
				String dname = dms.dimensionName;
				if(!projectionListDimensions.contains(dname))
				{
					projectionListDimensions.add(dname);
				}				
			}
			if(mt == null && qc.type == QueryColumn.MEASURE && qc.o != null)
			{
				MeasureColumn mc = (MeasureColumn) qc.o;
				mt = mc.MeasureTable;
			}
		}
		Map<DimensionColumn, DimensionColumn> lowestLevelColumnMap = new HashMap<DimensionColumn, DimensionColumn>();
		Map<String, List<String>> additionalMembers = new HashMap<String, List<String>>();
		// De-duplicate column references and build query
		for (QueryColumn qc : resultProjectionList)
		{
			if (qc.type == QueryColumn.DIMENSION)
			{
				DimensionColumn dc = (DimensionColumn) qc.o;
				/*
				 * Find the lowest level in a given dimension and use that. For higher levels, need calculated members
				 */
				String cname = dc.PhysicalName;
				String calcName = null;
				List<DimensionColumn> lowestLevelColumns = dimMap.get(dc.DimensionTable.DimensionName);
				boolean calculated = false;
				if (lowestLevelColumns != null && lowestLevelColumns.indexOf(dc) < 0)
				{
					if (dbType == DatabaseType.MSASXMLA || dbType == DatabaseType.SAPBWXMLA || dbType == DatabaseType.MONDRIANXMLA)
					{
						String calname = "M" + rand.nextInt();
						Set<DimensionColumn> dcset = new HashSet<DimensionColumn>();
						DimensionColumn lowestLevelColumn = addToLowestLevelColumnMap(r, dbType, dc, lowestLevelColumns, dcset, lowestLevelColumnMap, new ArrayList<DimensionColumn>());
						if (lowestLevelColumn != null)
						{
							int index = lowestLevelColumn.PhysicalName.lastIndexOf('.');
							String hierarchy = lowestLevelColumn.PhysicalName.substring(0, index);
							if (with.length() > 0)
								with.append(' ');
							String withStr = "ANCESTOR(" + hierarchy + ".CurrentMember," + dc.PhysicalName + ").Name";
							withStr = withStr.replace("'", "''");
							with.append("MEMBER [Measures].[" + calname + "] AS '" + withStr + "'");
							calcName = "[Measures].[" + calname + "]";
							cname = calname;
							calculated = true;
							calculatedMemberMap.put(dc.PhysicalName, "[" + calname + "]");
							calculatedMemberQualifiedNameMap.put("[" + calname + "]", calcName);
						}
					}
					else if (dbType == DatabaseType.EssbaseXMLA)
					{
						addToLowestLevelColumnMap(r, dbType, dc, lowestLevelColumns, new HashSet<DimensionColumn>(), lowestLevelColumnMap, new ArrayList<DimensionColumn>());						
					}
				}
				if (dbType == DatabaseType.EssbaseXMLA)
				{
					// Prune hierarchy out of dimension formula
					int index1 = cname.indexOf('.');
					int index2 = cname.indexOf('.', index1 + 1);
					if (index1 >= 0 && index2 >= 0)
						cname = cname.substring(0, index1) + cname.substring(index2);
				}
				ctypeMap.put(cname, dc.DataType);
				cformatMap.put(cname, dc.Format);
				List<Integer> plist = dimPoslist.get(cname);
				if (plist == null)
				{
					plist = new ArrayList<Integer>();
					dimPoslist.put(cname, plist);
					if (cube == null)
						cube = dc.DimensionTable.TableSource.Tables[0].PhysicalName;
					if (calculated)
					{
						if (measureClause.length() > 0)
							measureClause.append(',');
						measureClause.append(calcName);
						noItemsInMeasureClause++;
					} else
					{
						if (lowestLevelColumns == null || lowestLevelColumns.indexOf(dc) >= 0 || dbType == DatabaseType.MSASXMLA || dbType == DatabaseType.MONDRIANXMLA)
							addToDimensionList(dbType, dc.PhysicalName, dimColumnMap, additionalMembers, false);
					}
				}
				plist.add(qc.columnIndex);
			} else if (qc.type == QueryColumn.DIMENSION_MEMBER_SET)
			{
				DimensionMemberSetNav dms = (DimensionMemberSetNav) qc.o;
				boolean isParent = false, isParentName = false, isOrdinal = false, isMember = false, isUniqueName = false;
				if (dms.colIndex == qc.columnIndex)
				{
					isMember = true;
				}
				else if (dms.parentColIndex == qc.columnIndex)
				{
					isParent = true;
				}
				else if (dms.parentNameColIndex == qc.columnIndex)
				{
					isParentName = true;
				}
				else if (dms.ordinalColIndex == qc.columnIndex)
				{
					isOrdinal = true;
				}
				else if (dms.uniqueNameColIndex == qc.columnIndex)
				{
					isUniqueName = true;
				}
				String dimHierarchy = dms.dimensionName;
				Hierarchy h = r.findHierarchy(dimHierarchy);
				String dName = h.OlapDimensionName;
				String hName = dimHierarchy.substring((h.OlapDimensionName + "-").length());
				if (cube == null)
				{
					DimensionTable dt = dms.findDimensionTableForDimensionMemberSet(r);
					if (dt != null)
						cube = dt.TableSource.Tables[0].PhysicalName;
				}				
				if (with.length() > 0)
					with.append(' ');
				List<Integer> plist = null;
				String calcName = hName + (isMember ? "-MemberName" : (isParent ? "-MemberParent" : (isParentName ? "-MemberParentName" : isOrdinal ? "-MemberOrdinal" : "-MemberUniqueName")));				
				if (!calculatedMemberMap.containsKey(calcName))
				{
					plist = new ArrayList<Integer>();
					String memberExpression = (isMember ? "[" + dName + "].[" + hName + "].CurrentMember.Name" : 
								(isParent ? "[" + dName + "].[" + hName + "].CurrentMember.Parent.UniqueName" :
								(isParentName ? "[" + dName + "].[" + hName + "].CurrentMember.Parent.Name" :
									isOrdinal ? "Cstr([" + dName + "].[" + hName + "].CurrentMember.Level.Ordinal)"
									: "[" + dName + "].[" + hName + "].CurrentMember.UniqueName")));
					with.append("MEMBER [Measures].[").append(calcName).append("] AS '").append(memberExpression).append("'");
					if (measureClause.length() > 0)
						measureClause.append(',');
					measureClause.append("[Measures].[" + calcName + "]");
					noItemsInMeasureClause++;
					noDimMemberSetsInMeasureClause++;
					if (dms.dataType == null || isParent || isParentName|| isOrdinal || isUniqueName)
						ctypeMap.put(calcName, "Varchar");
					else
						ctypeMap.put(calcName, dms.dataType);
					if (isParent || isParentName || isOrdinal || isUniqueName)
						cformatMap.put(calcName, null);
					else
						cformatMap.put(calcName, dms.format);
					if (!dimMemberSets.contains(dms))
					{
						dimMemberSets.add(dms);
					}
					calculatedMemberMap.put(calcName, calcName);
					measurePoslist.put(calcName, plist);
				}
				else
				{
					plist = measurePoslist.get(calcName);
				}
				plist.add(qc.columnIndex);				
			} else if (qc.type == QueryColumn.OLAP_MEMBER_EXPRESSION)
			{
				OlapMemberExpression exp = (OlapMemberExpression)qc.o;
				String displayName = (exp.displayName != null ? exp.displayName : "M" + rand.nextInt());
				if (with.length() > 0)
					with.append(' ');
				String expression = exp.expression;
				if (exp.expression.startsWith("'") && exp.expression.endsWith("'"))
				{
					expression = exp.expression.substring(1, exp.expression.length() - 1);
				}
				String withStr = expression.replace("'", "''");
				with.append("MEMBER [Measures].[" + displayName + "] AS '" + withStr + "'");
				calculatedMemberMap.put(displayName, "[" + displayName + "]");
				List<Integer> plist = null;
				plist = measurePoslist.get(displayName);
				ctypeMap.put(displayName, "VARCHAR");//XXX
				//cformatMap.put(displayName, null);//XXX
				if (plist == null)
				{
					plist = new ArrayList<Integer>();
					measurePoslist.put(displayName, plist);
					if (measureClause.length() > 0)
						measureClause.append(',');
					measureClause.append("[Measures].[" + displayName + "]");
					noItemsInMeasureClause++;
				}
				plist.add(qc.columnIndex);				
			} else if (qc.type == QueryColumn.MEASURE)
			{
				MeasureColumn mc = (MeasureColumn) qc.o;
				String basepname = null;
				if (baseColumn.matcher(mc.PhysicalName).matches())
				{
					basepname = mc.PhysicalName.substring(1, mc.PhysicalName.length() - 1);
				} else
				{
					// Custom formula (doesn't match a base column name)
					String fname = "M" + rand.nextInt();
					if (with.length() > 0)
						with.append(' ');
					String withStr = mc.PhysicalName.replace("'", "''");
					with.append("MEMBER [Measures].[" + fname + "] AS '" + withStr + "'");
					basepname = fname;
					calculatedMemberMap.put(mc.PhysicalName, "[" + fname + "]");
					calculatedMemberQualifiedNameMap.put("[" + fname + "]", "[Measures].[" + fname + "]");
				}
				String cname = basepname;
				if (qc.filter != null)
				{
					int foundindex = -1;
					int index = 0;
					for (QueryColumn qc2: resultProjectionList)
					{
						// Scan to see if you can re-use any earlier columns
						if (qc2 == qc)
							break;
						if (qc2.type == QueryColumn.MEASURE && qc2.filter != null)
						{
							MeasureColumn mc2 = (MeasureColumn) qc2.o;
							if (mc.ColumnName.equals(mc2.ColumnName) && qc.filter.equalsUsingVariables(qc2.filter, null, r, false))
							{
								foundindex = index;
								break;
							}
						}
						index++;
					}
					if (foundindex >= 0)
					{
						for (Entry<String,List<Integer>> en: measurePoslist.entrySet())
						{
							if (en.getValue().indexOf(foundindex) >= 0)
								cname = en.getKey();
						}
					} else
						cname = cname + rand.nextInt();
				}
				List<Integer> plist = null;
				if (dbType == DatabaseType.EssbaseXMLA)
				{
					plist = measurePoslist.get('[' + cname + ']');
					ctypeMap.put('[' + cname + ']', mc.DataType);
					cformatMap.put('[' + cname + ']', mc.Format);
				}
				else
				{
					plist = measurePoslist.get(cname);
					ctypeMap.put(cname, mc.DataType);
					cformatMap.put(cname, mc.Format);
				}
				if (plist == null)
				{
					plist = new ArrayList<Integer>();
					if (dbType == DatabaseType.EssbaseXMLA)
					{
						measurePoslist.put('[' + cname + ']', plist);
					} else
					{
						measurePoslist.put(cname, plist);
					}
					if (cube == null)
						cube = mc.MeasureTable.TableSource.Tables[0].PhysicalName;
					if (measureClause.length() > 0)
						measureClause.append(',');
					String pname = basepname;
					if (qc.filter != null)
					{
						if (with.length() > 0)
							with.append(' ');
						Set<DimensionColumn> dcset = new HashSet<DimensionColumn>();
						qc.filter.getAllMDXDimensionColumnsNotInProjectionList(dcset, resultProjectionList);
						List<DimensionColumn> toRemoveLst = new ArrayList<DimensionColumn>();
						for (DimensionColumn dc : dcset) {
							List<DimensionColumn> lowestLevelColumns = dimMap.get(dc.DimensionTable.DimensionName);
							if (lowestLevelColumns != null && lowestLevelColumns.indexOf(dc) < 0)
							{
								addToLowestLevelColumnMap(r, dbType, dc, lowestLevelColumns, dcset, lowestLevelColumnMap, toRemoveLst);
							}
						}
						dcset.removeAll(toRemoveLst);
						StringBuilder dimensionSet = new StringBuilder();
						if (dcset.size() > 1)
						{
							int dccount = 0;
							for (DimensionColumn dc : dcset)
							{
								if (dccount > 0)
								{
									if (dbType == DatabaseType.EssbaseXMLA)
									{
										dimensionSet.insert(0, "CROSSJOIN(");
										dimensionSet.append("," + dc.PhysicalName + ".AllMembers)");
									} else
									{
										dimensionSet.append("*" + dc.PhysicalName + ".AllMembers");
									}
								} else
									dimensionSet.append(dc.PhysicalName + ".AllMembers");
								dccount++;
							}
						} else if (dcset.size() == 1)
						{
							dimensionSet.append(dcset.iterator().next().PhysicalName + ".AllMembers");
						}
						// Filtered measure
						StringBuilder filterStr = new StringBuilder();
						filterStr.append(qc.filter.getMDXFilterString(dbType, lowestLevelColumnMap, dimColumnMap, true, dimMemberSets, r));
						//if we set nullStr to NULL then NON EMPTY Function will remove rows with nulls
						String nullStr = "0";
						if (dimensionSet.length() > 0)
						{
							if (with.length() > 0)
								with.append(' ');
							String withStr = mc.getAggregationRule(null) + "({" + dimensionSet + "},IIF(" + filterStr + ",[Measures].[" + pname + "]," + nullStr + "))";
							withStr = withStr.replace("'", "''");
							with.append("MEMBER [Measures].[" + cname + "] AS '" + withStr + "'");
							measureClause.append("[Measures].[" + cname + "]");
							calculatedMemberMap.put(mc.PhysicalName, "[" + cname + "]");
							calculatedMemberQualifiedNameMap.put("[" + cname + "]", "[Measures].[" + cname + "]");
						} else if (filterStr.length() > 0)
						{
							if (with.length() > 0)
								with.append(' ');
							String withStr = "IIF(" + filterStr + ",[Measures].[" + pname + "]," + nullStr + ")";
							withStr = withStr.replace("'", "''");
							with.append("MEMBER [Measures].[" + cname + "] AS '" + withStr + "'");
							measureClause.append("[Measures].[" + cname + "]");
							calculatedMemberMap.put(mc.PhysicalName, "[" + cname + "]");
							calculatedMemberQualifiedNameMap.put("[" + cname + "]", "[Measures].[" + cname + "]");
						} else
							measureClause.append("[Measures].[" + pname + "]");
					} else
						measureClause.append("[Measures].[" + pname + "]");
					noItemsInMeasureClause++;
				}
				plist.add(qc.columnIndex);
			}
		}
		
		Set<String> dimensionalHierarchiesInProjection = new HashSet<String>();
		for(QueryColumn qc : resultProjectionList)
		{
			if (qc.type == QueryColumn.DIMENSION)
			{
				DimensionColumn qdc = (DimensionColumn)qc.o;
				if (qdc != null && qdc.PhysicalName != null && qdc.PhysicalName.lastIndexOf(".") >= 0)
				{
					dimensionalHierarchiesInProjection.add(qdc.PhysicalName.substring(0, qdc.PhysicalName.lastIndexOf(".")));
				}
			}
		}
		
		//List of filters to be included in "where" clause of the query
		List<QueryFilter> slicerAxisFilters = new ArrayList<QueryFilter>();
		//List of filters to be included in "filter" clause of the query
		List<QueryFilter> mainAxisFilters = new ArrayList<QueryFilter>();
		//Filters that can be added to sub queries in MSAS case
		List<QueryFilter> subQueryFilters = new ArrayList<QueryFilter>();
		//Remaining filters that cannot be included in either main or slicer axis or in subquery, the 
		//corresponding columns will have to be added to projection list
		List<QueryFilter> grainChangingFilters = new ArrayList<QueryFilter>();
		//Filters needs to be added in Main Axis to avoid cross joins
		List<QueryFilter> dimensionSetFilters = new ArrayList<QueryFilter>();
		StringBuilder filteredDimensionSet = new StringBuilder();
		
		if (filters != null && filters.size() > 0)
		{
			classifyFilters(filters, dimensionSetFilters, slicerAxisFilters, mainAxisFilters, subQueryFilters, grainChangingFilters, 
					dimMap, lowestLevelColumnMap, projectionListDimensions, resultProjectionList, dimensionalHierarchiesInProjection, dbType, r, dbc, 
					validateOnly, topn, mt, q);
			if(slicerAxisFilters.size() > 0)
			{
				appendToWhereClause(where, slicerAxisFilters, dbType);
			}

			//Add all projection list changing filters dim column to result projection list and transfer 
			//subquery filters to main axis filter. This piece of code, if executed, will drop the 
			//grain of the query but there is no other way around.
			if(grainChangingFilters.size() > 0)
			{
				//First update the dimension map with lowest level column for a given dimension
				for(QueryFilter qf : grainChangingFilters)
				{
					Set<DimensionColumn> dcset = new HashSet<DimensionColumn>();
					qf.getAllMDXDimensionColumnsNotInProjectionList(dcset, resultProjectionList);
					for (DimensionColumn dc : dcset) 
					{
						DimensionColumn hdc = updateDimMap(dimMap, dc.DimensionTable.DimensionName, dc, r);
						if (hdc != null)
							lowestLevelColumnMap.put(hdc, dc);
					}					
				}
				
				for(QueryFilter qf : grainChangingFilters)
				{
					Set<DimensionColumn> dcset = new HashSet<DimensionColumn>();
					qf.getAllMDXDimensionColumnsNotInProjectionList(dcset, resultProjectionList);
					for (DimensionColumn dc : dcset) 
					{
						List<DimensionColumn> lowestLevelColumns = dimMap.get(dc.DimensionTable.DimensionName);
						if (lowestLevelColumns != null && lowestLevelColumns.indexOf(dc) < 0)
						{
							addToLowestLevelColumnMap(r, dbType, dc, lowestLevelColumns, dcset, lowestLevelColumnMap, new ArrayList<DimensionColumn>());
						}
						if (lowestLevelColumns == null || lowestLevelColumns.indexOf(dc) >= 0 || dbType == DatabaseType.MSASXMLA || dbType == DatabaseType.MONDRIANXMLA)
						{
							addToDimensionList(dbType, dc.PhysicalName, dimColumnMap, additionalMembers, false);
						}
						logger.warn("Added dimension column \""+dc.DimensionTable.DimensionName + "." + dc.ColumnName 
								+"\" to the grain of the query. The result set may contain repeating rows containing same "
								+"values across multiple rows.");
					}
				}
				mainAxisFilters.addAll(grainChangingFilters);
			}
			//Add sub-queries for MSXMLA
			if(subQueryFilters.size() > 0)
			{
				appendSubQueryToFromClause(subQueryFilters, from, r, dbc);
			}
			if (dimensionSetFilters.size() > 0)
			{
				processDimensionSetFilters(dbType, dimensionSetFilters, dimColumnMap, filteredDimensionSet);
			}
			if(mainAxisFilters.size() > 0)
			{
				appendToFilterClause(filterClause, mainAxisFilters, dbType, lowestLevelColumnMap, dimColumnMap, dimMemberSets, r);
			}
		}
		
		if (measureClause.length() > 0 && noItemsInMeasureClause > noDimMemberSetsInMeasureClause)
		{
			useNonEmptyFunction = true;
		}
		
		StringBuilder measurePositions = new StringBuilder();
		StringBuilder dimensionPositions = new StringBuilder();
		StringBuilder columnFormats = new StringBuilder();
		for (Entry<String, List<Integer>> en : dimPoslist.entrySet())
		{
			if (dimensionPositions.length() > 0)
				dimensionPositions.append(',');
			dimensionPositions.append('(' + en.getKey() + "|");
			dimensionPositions.append(ctypeMap.get(en.getKey()));
			dimensionPositions.append('|');
			for (int i = 0; i < en.getValue().size(); i++)
			{
				if (i > 0)
					dimensionPositions.append('|');
				dimensionPositions.append(en.getValue().get(i));
			}
			dimensionPositions.append(')');
			String format = cformatMap.get(en.getKey());
			if (format != null && !format.trim().isEmpty())
			{
				if (columnFormats.length() > 0)
					columnFormats.append('^');
				columnFormats.append(en.getKey()).append("|").append(format);
			}
		}
		for (Entry<String, List<Integer>> en : measurePoslist.entrySet())
		{
			if (measurePositions.length() > 0)
				measurePositions.append(',');
			measurePositions.append('(' + en.getKey() + "|");
			measurePositions.append(ctypeMap.get(en.getKey()));
			measurePositions.append('|');
			for (int i = 0; i < en.getValue().size(); i++)
			{
				if (i > 0)
					measurePositions.append('|');
				measurePositions.append(en.getValue().get(i));
			}
			measurePositions.append(')');
			String format = cformatMap.get(en.getKey());
			if (format != null && !format.trim().isEmpty())
			{
				if (columnFormats.length() > 0)
					columnFormats.append('^');
				columnFormats.append(en.getKey()).append("|").append(format);
			}
		}
		if (with.length() > 0)
		{
			with.insert(0, "WITH ");
			with.append(" ");
		}
		String topnstr = null;
		boolean addTopToMDX = topn >= 0 && !dbc.requiresDispalyTOPForXMLA();
		if (!addTopToMDX)
		{
			topnstr = "TOPN" + topn + " ";
		}
		if (dbc.DBType == DatabaseType.MONDRIANXMLA && measureClause.toString().isEmpty())
		{
			//workaround to use dummy calculated member for mondrian as suggested by pentaho
			if (with.length() == 0)
			{
				with.append("WITH ");				
			}
			with.append("MEMBER [Measures].[dummy] as '1' ");
			measureClause.append("[Measures].[dummy]");
		}
		result.append(dimensionPositions + "~" + measurePositions + "~" + columnFormats + "~" + (topnstr != null ? topnstr : "") + with + "SELECT {" + measureClause + "} ON 0");
		StringBuilder dimensionClause = generateDimensionClause(dbc, dimColumnMap, additionalMembers, dimMemberSets, orderByClauses, calculatedMemberMap, r);
		if (dimensionClause.length() > 0)
		{
			dimensionClause.insert(0, "{");
			dimensionClause.append("}");
		}
		if (filteredDimensionSet.length() > 0)
		{
			if (dimensionClause.length() > 0)
				filteredDimensionSet.insert(0, "*");
			dimensionClause.append(filteredDimensionSet);
		}
		if (dimensionClause.length() > 0)
		{
			result.append(", " + (useNonEmptyFunction? NON_EMPTY_FUNCTION : "") + " ");
			if (filterClause.length() > 0)
			{
				dimensionClause.insert(0, "FILTER(" );
				dimensionClause.append(", " + filterClause + ")");
			}
			dimensionClause = OrderBy.getMDXOrderByClause(dbc, dimensionClause, orderByClauses, calculatedMemberMap, calculatedMemberQualifiedNameMap);	
			if (addTopToMDX)
			{
				dimensionClause.insert(0, "HEAD(");
				dimensionClause.append(", " + topn + ")");
			}
			result.append(dimensionClause + " ON 1");
		}
		result.append(from);
		result.append(" FROM [" + cube + "]");
		if(subQueryFilters.size() > 0)
		{
			result.append(')');
		}
		if (where.length() > 0)
			result.append(" WHERE " + where);
		if (dbType == DatabaseType.SAPBWXMLA)
			result.append(" CELL PROPERTIES VALUE");
		return result;
	}

	/**
	 * Based on sub query filters identified, just mimick the behavior of generating the main query.
	 * Create sub-query specific data structures (various maps) and use them to generate dimension
	 * clause as well as filter clause.
	 * @param subQueryFilters
	 * @param from
	 * @param r
	 * @param dbType
	 * @throws SyntaxErrorException
	 */
	private static void appendSubQueryToFromClause(List<QueryFilter> sqFilters, StringBuilder from, Repository r, DatabaseConnection dbc)
	throws SyntaxErrorException
	{
		DatabaseType dbType = dbc.DBType;
		Map<String, List<DimensionColumn>> subQueryDimMap = new HashMap<String, List<DimensionColumn>>();
		Map<DimensionColumn, DimensionColumn> subQueryLowestLevelColumnMap = new HashMap<DimensionColumn, DimensionColumn>();
		Map<String, String> subQueryDimColumnMap = new HashMap<String, String>();		
		Set<DimensionColumn> dcset = new HashSet<DimensionColumn>();
		List<QueryFilter> subQueryFilters = new ArrayList<QueryFilter>(sqFilters);
		for(QueryFilter qf : subQueryFilters)
		{
			qf.getAllMDXDimensionColumns(dcset);
		}
		for(DimensionColumn dc : dcset)
		{
			updateDimMap(subQueryDimMap,dc.DimensionTable.DimensionName, dc, r);
		}
		for (DimensionColumn dc : dcset) 
		{
			List<DimensionColumn> lowestLevelColumns = subQueryDimMap.get(dc.DimensionTable.DimensionName);
			DimensionColumn ldc = null;
			if (lowestLevelColumns != null && lowestLevelColumns.indexOf(dc) < 0)
			{
				ldc = addToLowestLevelColumnMap(r, dbType, dc, lowestLevelColumns, dcset, subQueryLowestLevelColumnMap, new ArrayList<DimensionColumn>());
			}
			if (lowestLevelColumns == null || lowestLevelColumns.indexOf(dc) >= 0 || ldc == null)
			{
				addToDimensionList(dbType, dc.PhysicalName, subQueryDimColumnMap, null, true);
			}
		}
		
		Map<QueryFilter, Set<String>> dimensionsByQueryFilter = new HashMap<QueryFilter, Set<String>>();
		for(QueryFilter qf : subQueryFilters)
		{
			Set<String> qfDimList = new HashSet<String>();
			qf.getDimensions(qfDimList);
			dimensionsByQueryFilter.put(qf, qfDimList);
		}
		
		List<QueryFilter> subQueryDimensionSetFilters = new ArrayList<QueryFilter>();
		for(QueryFilter qf: subQueryFilters)
		{
			Set<String> qfDimList = dimensionsByQueryFilter.get(qf);
			boolean includeFilterAsDimensionSet = qf.includeFilterAsMDXDimensionSet(r,dbType, subQueryDimMap);
			if (includeFilterAsDimensionSet)
			{
				String qfd = qfDimList.iterator().next();
				//Make sure that this is the only filter with the given dimension
				for(QueryFilter otherqf : subQueryFilters)
				{
					if(qf == otherqf)
					{
						continue;
					}
					Set<String> otherqfDimList = dimensionsByQueryFilter.get(otherqf);
					if(otherqfDimList.contains(qfd))
					{
						includeFilterAsDimensionSet = false;
						break;
					}
				}
			}
			if (includeFilterAsDimensionSet)
				subQueryDimensionSetFilters.add(qf);
		}
		
		subQueryFilters.removeAll(subQueryDimensionSetFilters);
		StringBuilder subQueryFilteredDimensionSet = new StringBuilder();
		processDimensionSetFilters(dbType, subQueryDimensionSetFilters, subQueryDimColumnMap, subQueryFilteredDimensionSet);
		
		StringBuilder subQueryDimensionClause = generateDimensionClause(dbc, subQueryDimColumnMap, new HashMap<String, List<String>>(), null, null, null, r);
		if (subQueryFilteredDimensionSet.length() > 0)
		{
			if (subQueryDimensionClause.length() > 0)
				subQueryFilteredDimensionSet.insert(0, "*");
			subQueryDimensionClause.append(subQueryFilteredDimensionSet);
		}
		
		StringBuilder subQueryFilterClause = new StringBuilder();
		appendToFilterClause(subQueryFilterClause, subQueryFilters, dbType, subQueryLowestLevelColumnMap, subQueryDimColumnMap, null, null);
		from.append(" FROM (SELECT");
		if (subQueryFilterClause.length() > 0)
		{
			from.append(" FILTER( " + subQueryDimensionClause + ", " + subQueryFilterClause + ") ON 0");
		}
		else
		{
			from.append(" " + subQueryDimensionClause + " ON 0");
		}
	}
	
	private static DimensionColumn updateDimMap(Map<String, List<DimensionColumn>> dimMap, String dname, DimensionColumn dc, Repository r)
	{
		List<DimensionColumn> dclist = dimMap.get(dname);
		if (dclist == null)
		{
			dclist = new ArrayList<DimensionColumn>();
			dclist.add(dc);
			dimMap.put(dname, dclist);
		} else
		{
			for (DimensionColumn curdc : dclist)
			{
				Level l = r.findDimensionLevelColumn(dname, dc.ColumnName);
				Level cl = r.findDimensionLevelColumn(dname, curdc.ColumnName);
				if (l != null && cl != null)
				{
					if (cl.findLevel(l) != null)
					{
						dclist.remove(curdc);
						dclist.add(dc);
						return curdc;
					}
				}
			}
		}
		return null;
	}

	/**
	 * Given a list of filters, this method separates out filters that can be used in where clause i.e. 
	 * slicer axis vs. in filter clause i.e. main axis vs. in subquery vs. remaining ones that cannot be used
	 * in any of those axis. Filters that cannot be included in either category are likely to change the grain
	 * of the query and hence the name grainChangingFilters
	 * @param filters
	 * @param slicerAxisFilters
	 * @param mainAxisFilters
	 * @param subQueryFilters
	 * @param grainChangingFilters
	 * @param dimMap
	 * @param lowestLevelColumnMap
	 * @param projectionListDimensions
	 * @param resultProjectionList
	 * @param dbType
	 * @param r
	 */
	private static void classifyFilters(List<QueryFilter> filters, 
			List<QueryFilter> dimensionSetFilters, List<QueryFilter> slicerAxisFilters, List<QueryFilter> mainAxisFilters, 
			List<QueryFilter> subQueryFilters, List<QueryFilter> grainChangingFilters,
			Map<String, List<DimensionColumn>> dimMap, Map<DimensionColumn, DimensionColumn> lowestLevelColumnMap,
			Set<String> projectionListDimensions, List<QueryColumn> resultProjectionList, Set<String> dimensionalHierarchiesInProjection,
			DatabaseType dbType, Repository r, DatabaseConnection dbc, boolean validateOnly, 
			int topn, MeasureTable mt, Query q)
	{
		List<QueryFilter> processedFilters = new ArrayList<QueryFilter>();
		for(QueryFilter qf : filters)
		{
			//Ignore the measure filters the potentially belong to a different source - this situation
			//will come up in queries that require federation of resultsets. Make sure that if the filter
			//contains a measure, the measure is part of the cube's measure table.
			if(mt != null && qf.containsMeasure() && qf.containsMeasureNotNavigated(mt))
			{
				continue;
			}
			if(qf.isLogicalANDWithDimCol())
			{
				qf.splitFilters(processedFilters);
			}
			else
			{
				processedFilters.add(qf);
			}
		}
		
		Map<QueryFilter, Set<String>> dimensionsByQueryFilter = new HashMap<QueryFilter, Set<String>>();
		for(QueryFilter qf : processedFilters)
		{
			Set<String> qfDimList = new HashSet<String>();
			qf.getDimensions(qfDimList);
			dimensionsByQueryFilter.put(qf, qfDimList);
		}
		
		for(QueryFilter qf : processedFilters)
		{
			if (qf.isPrompted() && QueryString.NO_FILTER_TEXT.equals(qf.getOperand()))
			{
				continue;
			}
			
			Set<String> qfDimList = dimensionsByQueryFilter.get(qf);
			
			//In order to be included in main axis filter clause, all of the query filter dimensions 
			//have to be included in the projection list at equal or lower level.
			boolean includeInMainAxis = true;
			boolean includeInSlicerAxis = true;
			boolean includeInSubQuery = (dbType == DatabaseType.EssbaseXMLA || dbType == DatabaseType.SAPBWXMLA) ? false : true;
			boolean includeFilterAsDimensionSet = qf.includeFilterAsMDXDimensionSet(r,dbType, dimMap);
			boolean belongsToMDXDimensionalHiearchyInProjection = qf.belongsToMDXDimensionalHiearchy(dimensionalHierarchiesInProjection);
			if(qfDimList.size() > 0)
			{
				Iterator<String> iter = qfDimList.iterator();
				Set<DimensionColumn> dcset = new HashSet<DimensionColumn> ();
				qf.getAllMDXDimensionColumnsNotInProjectionList(dcset, resultProjectionList);
				boolean belongsToDiffDimensioanlHierarchy = false;
				while(iter.hasNext())
				{
					String qfd = iter.next();
					List<DimensionColumn> toRemoveLst = new ArrayList<DimensionColumn>();				
					for (DimensionColumn dc : dcset) 
					{
						List<DimensionColumn> lowestLevelColumns = dimMap.get(dc.DimensionTable.DimensionName);
						if (lowestLevelColumns != null && lowestLevelColumns.indexOf(dc) < 0)
						{
							addToLowestLevelColumnMap(r, dbType, dc, lowestLevelColumns, dcset, lowestLevelColumnMap, toRemoveLst);
						}
					}
					dcset.removeAll(toRemoveLst);
					if(projectionListDimensions.contains(qfd))
					{
						//Dimension exists in projection list
						//Now further check to see if we have any column in projection that belongs to this Dimension AND hierarchy
						//If found, filter cannot be added to slicer axis.
						if (qf.containsOnlyEqualsOp())
						{
							if (belongsToMDXDimensionalHiearchyInProjection)
							{
								includeInSlicerAxis = false;
							}
							else
							{
								belongsToDiffDimensioanlHierarchy = true;
							}
						}
						else
						{
							includeInSlicerAxis = false;
						}
					}
					else
					{
						for (DimensionColumn dc : dcset) 
						{
							if ((dc.DataType.equals("Date") || dc.DataType.equals("DateTime")) && (dbType == DatabaseType.MSASXMLA || dbType == DatabaseType.MONDRIANXMLA))
							{
								includeInSlicerAxis = false;
								break;
							}
						}
					}
				}					
					
				if(dcset.size() > 0 && !belongsToDiffDimensioanlHierarchy)
				{
					includeInMainAxis = false;
					includeFilterAsDimensionSet = false;
				}
			}
			if (includeInSubQuery && (!validateOnly) && (topn != 0)) //do not execute MDX on validating query 
			{
				// Figure out if calculated members are being used, if so, cannot be used in a subquery
				Set<DimensionColumn> dcSet = new HashSet<DimensionColumn>();
				qf.getAllMDXDimensionColumns(dcSet);
				if (dcSet.size() > 0)
				{
					for (DimensionColumn dc : dcSet)
					{
						if (q.dimensionColumnIsCalculatedMember != null && q.dimensionColumnIsCalculatedMember.containsKey(dc))
						{
							if (q.dimensionColumnIsCalculatedMember.get(dc))
							{
								includeInSubQuery = false;								
							}
							break;
						}

						int numCalcMembers = 0;
						numCalcMembers = numCalculatedMembers(r, dbc, q.getSession(), dc);
						if (numCalcMembers > 0)
						{
							if (q.dimensionColumnIsCalculatedMember != null)
							{
								q.dimensionColumnIsCalculatedMember.put(dc, Boolean.TRUE);
							}
							includeInSubQuery = false;
							break;
						} else if (numCalcMembers < 0)
						{
							includeInSubQuery = false;
							break;
						} else
						{
							if (q.dimensionColumnIsCalculatedMember != null)
							{
								q.dimensionColumnIsCalculatedMember.put(dc, Boolean.FALSE);
							}
						}
					}
				}
			}
			if (includeFilterAsDimensionSet)
			{
				String qfd = qfDimList.iterator().next();
				//Make sure that this is the only filter with the given dimension
				for(QueryFilter otherqf : processedFilters)
				{
					if(qf == otherqf)
					{
						continue;
					}
					Set<String> otherqfDimList = dimensionsByQueryFilter.get(otherqf);
					if(otherqfDimList.contains(qfd))
					{
						if (qf.containsOnlyEqualsOp() && otherqf.containsOnlyEqualsOp())
						{
							Set<String> dimHierarchies = new HashSet<String>(); 
							qf.getMDXDimensionalHierarchies(dimHierarchies);
							Set<String> otherqfDimHierarchies = new HashSet<String>();
							otherqf.getMDXDimensionalHierarchies(otherqfDimHierarchies);
							if (dimHierarchies.size() > 0 && otherqfDimHierarchies.size() > 0)
							{
								if (dimHierarchies.size() == otherqfDimHierarchies.size() && dimHierarchies.containsAll(otherqfDimHierarchies))
								{
									includeFilterAsDimensionSet = false;
									break;
								}							
							}
							else
							{
								includeFilterAsDimensionSet = false;
								break;
							}
						}
						else
						{
							includeFilterAsDimensionSet = false;
							break;
						}
					}
				}
			}
			if (includeFilterAsDimensionSet && belongsToMDXDimensionalHiearchyInProjection)
			{
				dimensionSetFilters.add(qf);
				continue;
			}
			else if(includeInMainAxis && belongsToMDXDimensionalHiearchyInProjection)
			{
				mainAxisFilters.add(qf);
				continue;
			}
			else if(includeInSlicerAxis && !belongsToMDXDimensionalHiearchyInProjection)
			{
				if(qf.isPredicateWithEqualsOp() || (qf.isLogicalORWithSameDimCol() && (dbType == DatabaseType.MSASXMLA || dbType == DatabaseType.SAPBWXMLA || dbType == DatabaseType.MONDRIANXMLA)))
				{
					String qfd = qfDimList.iterator().next();
					//Make sure that this is the only filter with the given dimension
					for(QueryFilter otherqf : processedFilters)
					{
						if(qf == otherqf)
						{
							continue;
						}
						Set<String> otherqfDimList = dimensionsByQueryFilter.get(otherqf);
						if(otherqfDimList.contains(qfd))
						{
							if (qf.containsOnlyEqualsOp() && otherqf.containsOnlyEqualsOp())
							{
								Set<String> dimHierarchies = new HashSet<String>(); 
								qf.getMDXDimensionalHierarchies(dimHierarchies);
								Set<String> otherqfDimHierarchies = new HashSet<String>();
								otherqf.getMDXDimensionalHierarchies(otherqfDimHierarchies);
								if (dimHierarchies.size() > 0 && otherqfDimHierarchies.size() > 0)
								{
									if (dimHierarchies.size() == otherqfDimHierarchies.size() && dimHierarchies.containsAll(otherqfDimHierarchies))
									{
										includeInSlicerAxis = false;
										break;
									}
								}
								else
								{
									includeInSlicerAxis = false;
									break;
								}
							}
							else
							{
								includeInSlicerAxis = false;
								break;
							}								
						}
					}
				}
				else
				{
					includeInSlicerAxis = false;
				}
			}
			else if (belongsToMDXDimensionalHiearchyInProjection)
			{
				includeInSlicerAxis = false;
			}

			//Passed the test for being included in slicer axis - so include it.
			if(includeInSlicerAxis)
			{
				slicerAxisFilters.add(qf);
			}
			else if(includeInSubQuery && dbType == DatabaseType.MSASXMLA || dbType == DatabaseType.MONDRIANXMLA)
			{
				subQueryFilters.add(qf);
			}
			else
			{
				grainChangingFilters.add(qf);
			}
		}
	}
	
	private static int numCalculatedMembers(Repository r, DatabaseConnection dbc, Session s, DimensionColumn dc)
	{
		if (dc.numCalculatedMembers >= 0)
			return dc.numCalculatedMembers;
		DatabaseConnectionStatement statement = null;
		try
		{
			if (dbc.isRealTimeConnection())
			{
				statement = new DatabaseConnectionStatement(r, dbc);
			} else
			{
				if (dbc.ConnectionPool.isDynamicConnnection() && s != null)
					statement = new DatabaseConnectionStatement(dbc, dbc.ConnectionPool.getDynamicConnection(s).createStatement(), r);
				else
					statement = new DatabaseConnectionStatement(dbc, dbc.ConnectionPool.getConnection().createStatement(), r);
				
			}							
		} catch (Exception e)
		{
			/*
			 * Consume and report exception to the log file - this way, if a query can potentially be
			 * served from the cache, the execution will continue. If not, it will fail at a later
			 * stage, throwing data unavailable exception.
			 */
			logger.error(e, e);
		}
		Query query = new Query(r);
		try
		{
			// Get a list of all calculated members (difference between members and allmembers)
			DatabaseConnectionResultSet dcrs = statement.executeQuery("(" + dc.PhysicalName + "|Varchar|0)~~" + 
					(dbc.DBType == DatabaseType.MONDRIANXMLA? "WITH MEMBER [Measures].[dummy] as '1' " : "") + 
					"SELECT {" + (dbc.DBType == DatabaseType.MONDRIANXMLA ? "[Measures].[dummy]" : "") + "} ON 0, EXCEPT("
					+ dc.PhysicalName + ".AllMembers," + dc.PhysicalName + ".Members) ON 1 FROM ["
					+ dc.DimensionTable.TableSource.Tables[0].PhysicalName + "]", query);
			dc.numCalculatedMembers = dcrs.getNumRows();
			return dc.numCalculatedMembers;
		} catch (Exception e)
		{
			logger.error(e, e);
		} 
		return -1;
	}
	
	/**
	 * Build a where clause based on given filters and database type
	 * @param where
	 * @param filters
	 * @param dbType
	 */
	private static void appendToWhereClause(StringBuilder where, List<QueryFilter> filters, DatabaseType dbType)
	{
		for(QueryFilter qf : filters)
		{
			if (where.length() > 0)
			{
				if(dbType == DatabaseType.EssbaseXMLA)
				{
					where.insert(0, "CROSSJOIN(");
					where.append(',');
				}
				else
				{
					where.append('*');
				}
				where.append("{" + qf.getMDXOperandList(dbType) + "}");
				if(dbType == DatabaseType.EssbaseXMLA)
				{
					where.append(')');
				}
			}
			else
			{
				where.append("{" + qf.getMDXOperandList(dbType) + "}");
			}
		}
	}

	private static void appendToFilterClause(StringBuilder filterClause, List<QueryFilter> filters, 
			DatabaseType dbType, Map<DimensionColumn, DimensionColumn> lowestLevelColumnMap, 
			Map<String, String> dimColumnMap, List<DimensionMemberSetNav> dimMemberSets, Repository repository) 
	throws SyntaxErrorException
	{
		boolean first = true;
		
		for(QueryFilter qf : filters)
		{
			if (!first)
			{
				filterClause.append(" AND ");
			}
			filterClause.append(qf.getMDXFilterString(dbType, lowestLevelColumnMap, dimColumnMap, true, dimMemberSets, repository));	
			first = false;
		}
		
	}
	
	private static void processDimensionSetFilters(DatabaseType dbType, List<QueryFilter> filters, Map<String, String> dimColumnMap,StringBuilder filteredDimensionSet)
	{
		Set<DimensionColumn> dcSet = new HashSet<DimensionColumn>();
		boolean first = true;
		String filterStr = null;
		for(QueryFilter qf : filters)
		{
			filterStr = qf.getMDXDimensionSet(dbType);
			if (filterStr == null)
				continue;
			if (first)
				first = false;
			else
				filteredDimensionSet.append("*");
			filterStr = '{' + filterStr + '}';
			filteredDimensionSet.append(filterStr);
			qf.returnDimensionColumns(dcSet);
			removeFromDimensionList(dcSet, dimColumnMap);
		}
	}
	
	static Pattern MEMBERKEY = Pattern.compile(".+[&]\\[.+\\]"); 
	
	private static void addToDimensionList(DatabaseType dbType, String physicalName, Map<String, String> dimColumnMap, Map<String, List<String>> additionalMembers, boolean from)
	{
		int index = physicalName.lastIndexOf('.');
		String hname = physicalName.substring(0, index);
		if (MEMBERKEY.matcher(physicalName).matches())
		{
			if (additionalMembers != null)
			{
				List<String> mlist = additionalMembers.get(hname);
				if (mlist == null)
				{
					mlist = new ArrayList<String>(1);
					additionalMembers.put(hname, mlist);
				}
				mlist.add(physicalName);
			}
		} else
		{
			if (from)
				dimColumnMap.put(hname, physicalName + ".Members");
			else
				dimColumnMap.put(hname, physicalName + ".AllMembers");
		}
	}
	
	public static void removeFromDimensionList(Set<DimensionColumn> dcSet, Map<String, String> dimColumnMap)
	{
		for(DimensionColumn dc: dcSet)
		{
			String physicalName = dc.PhysicalName;
			int index = physicalName.lastIndexOf('.');
			if (index > 0)
			{
				String hname = physicalName.substring(0, index);
				dimColumnMap.remove(hname);
			}
		}
	}
	
	//Adding column to LowestLevelColumnMap if it is ancestor , remove column from dcset and return the lowest level column
	public static DimensionColumn addToLowestLevelColumnMap(Repository r, DatabaseType dbType, DimensionColumn dc, List<DimensionColumn> lowestLevelColumns, Set<DimensionColumn> dcset, Map<DimensionColumn, DimensionColumn> lowestLevelColumnMap, List<DimensionColumn> toRemoveLst)
	{
		for (DimensionColumn curdc : lowestLevelColumns)
		{
			Level l = r.findDimensionLevelColumn(dc.DimensionTable.DimensionName, dc.ColumnName);
			Level cl = r.findDimensionLevelColumn(dc.DimensionTable.DimensionName, curdc.ColumnName);
			if (l != null && cl != null)
			{
				if (l.findLevel(cl) != null)
				{
					lowestLevelColumnMap.put(dc, curdc);
					toRemoveLst.add(dc);
					return curdc;
				}
			}
		}
		return null;
	}
	
	public static StringBuilder generateDimensionClause(DatabaseConnection dbc, Map<String, String> dimColumnMap, Map<String, List<String>> additionalMembers, 
			List<DimensionMemberSetNav> dimMemberSets, List<OrderBy> orderByClauses, Map<String, String> calculatedMemberMap, Repository r)
	{
		DatabaseType dbType = dbc.DBType;
		StringBuilder dimensionClause = new StringBuilder();
		boolean first = true;
		List<String> dimSet = new ArrayList<String>();
		Map<DimensionMemberSetNav, OrderBy> dimMemberSetOrderBy = new HashMap<DimensionMemberSetNav, OrderBy>();
		for (String key : dimColumnMap.keySet())
		{
			if (!dimSet.contains(key))
				dimSet.add(key);
		}
		for (String key : additionalMembers.keySet())
		{
			if (!dimSet.contains(key))
				dimSet.add(key);
		}
		if (dimMemberSets != null && dimMemberSets.size() > 0)
		{
			if (dbc.requiresDispalySortingForXMLA() || orderByClauses == null || orderByClauses.size() == 0)
			{
				for (DimensionMemberSetNav memSet : dimMemberSets)
					dimSet.add(memSet.dimensionName);
			}
			else if (orderByClauses != null && orderByClauses.size() > 0)
			{
				//need to generate dimension clause using order by specified
				for (OrderBy ob : orderByClauses)
				{
					if (ob.getType() == OrderBy.TYPE_DIMENSION_MEMBER_SET)
					{
						for (DimensionMemberSetNav memSet : dimMemberSets)
						{
							if (ob.getColName().equals(memSet.displayName))
							{
								{
									if (!dimSet.contains(memSet.dimensionName))
										dimSet.add(memSet.dimensionName);
									dimMemberSetOrderBy.put(memSet, ob);
								}
								break;
							}
						}
					}
				}			
			}
			for (DimensionMemberSetNav memSet : dimMemberSets)
			{
				if (!dimSet.contains(memSet.dimensionName))
					dimSet.add(memSet.dimensionName);
			}
		}
		for (String key : dimSet)
		{
			StringBuilder memberset = new StringBuilder();
			if (dimColumnMap != null && dimColumnMap.get(key) != null)
				memberset.append(dimColumnMap.get(key));
			if (additionalMembers != null && additionalMembers.containsKey(key))
			{
				StringBuilder newMemberset = new StringBuilder('{');
				if (memberset != null)
					newMemberset.append(memberset);
				for (String s : additionalMembers.get(key))
				{
					if (newMemberset.length() > 1)
						newMemberset.append(',');
					newMemberset.append(s);
				}
				memberset.append(newMemberset);
				memberset.append('}');
			}
			if (dimMemberSets != null)
			{
				DimensionMemberSetNav dmSet = null;
				for (DimensionMemberSetNav memSet : dimMemberSets)
				{
					if (memSet.dimensionName.equals(key))
					{
						dmSet = memSet;
						break;
					}
				}
				if (dmSet != null)
				{
					StringBuilder newMemberset = new StringBuilder("{");
					if (memberset != null)
						newMemberset.append(memberset);
					String dimHierarchy = dmSet.dimensionName;
					Hierarchy h = r.findHierarchy(dimHierarchy);
					String dName = h.OlapDimensionName;
					String hName = dimHierarchy.substring((h.OlapDimensionName + "-").length());
					boolean isFirst = true;
					for (MemberSelectionExpression expr : dmSet.memberExpressions)
					{
						if (isFirst)
							isFirst = false;							
						else
							newMemberset.append(",");
						String memberName = expr.memberName;
						switch (expr.selectionType)
						{
							case SELECTED:
							{
								newMemberset.append(memberName);								
								break;
							}
							case CHILDREN:
							{
								newMemberset.append(memberName).append(".CHILDREN");
								break;
							}
							case ALLCHILDREN:
							{
								newMemberset.append("DESCENDANTS(").append(memberName).append(", ");
								newMemberset.append("[").append(dName).append("].[").append(hName).append("].LEVELS(").append(memberName).append(".LEVEL.ORDINAL), AFTER)");
								break;
							}
							case LEAVES:
							{
								newMemberset.append("DESCENDANTS(").append(memberName).append(", ");
								newMemberset.append("[").append(dName).append("].[").append(hName).append("].LEVELS([").append(dName).append("].[").append(hName).append("]").append(".LEVELS.COUNT - 1), LEAVES)");
								break;
							}
							case PARENTS:
							{
								newMemberset.append(memberName).append(".PARENT");
								break;
							}
						}
					}
					memberset.append(newMemberset);
					memberset.append('}');
					
					if (dimMemberSetOrderBy.containsKey(dmSet))
					{
						//process order by
						OrderBy ob = dimMemberSetOrderBy.get(dmSet);
						memberset.insert(0, "ORDER(");
						memberset.append(", [").append(calculatedMemberMap.get(ob.getDimName())).append("], ");
						memberset.append(ob.getDirection() ? "ASC" : "DESC").append(")");
					}
				}
			}
			if (memberset != null)
			{
				if (!first)
				{
					if (dbType == DatabaseType.MSASXMLA || dbType == DatabaseType.SAPBWXMLA || dbType == DatabaseType.MONDRIANXMLA)
					{
						dimensionClause.append("*");
						dimensionClause.append(memberset);
					} else if (dbType == DatabaseType.EssbaseXMLA)
					{
						dimensionClause.insert(0, "CROSSJOIN(");
						dimensionClause.append(',');
						dimensionClause.append(memberset);
						dimensionClause.append(')');
					}
				} else
				{
					dimensionClause.append(memberset);
					first = false;
				}
			}
		}
		return dimensionClause;
	}
	
	public static List<QueryFilter> extractTableSourceFilters(Query q, Repository r, Session session) throws CloneNotSupportedException
	{
		List<QueryFilter> tsfList = new ArrayList<QueryFilter> ();
		List<QueryFilter> mdxFilters = r.getMdxSourceFilters();
		if(!q.hasBeenNavigated() || mdxFilters == null || mdxFilters.isEmpty())
		{
			return tsfList;
		}

		List<Navigation> navList = q.getNavigationList();
		if(navList != null && navList.size() > 0)
		{
			Navigation nav = navList.get(0);
			MeasureColumnNav mcnav = nav.getMeasureColumnNav();
			if(mcnav == null)
			{
				Set<String> dims = nav.getDimensions();
				for(QueryFilter qf : mdxFilters)
				{
					Set<String> qfdims = new HashSet<String>(); 
					qf.getDimensions(qfdims);
					if(!qfdims.isEmpty() && dims.containsAll(qfdims))
					{
						QueryFilter clonedqf = (QueryFilter)qf.clone();
						clonedqf = convertMultiValueFilterToComplexFilter(r, session, clonedqf);
						clonedqf = clonedqf.replaceVariables(r, session);
						boolean discardFilter = false;
						if(clonedqf.getType() == QueryFilter.TYPE_PREDICATE)
						{
							String operand = clonedqf.getOperand();
							if(operand == null || operand.trim().isEmpty())
							{
								discardFilter = true;
							}
						}
						if(!discardFilter && !tsfList.contains(clonedqf))
						{
							tsfList.add(clonedqf);
						}
					}
				}				
				//Dimension-only query - add the mdxFilters from repository if a given dimension is being used in the query			
			}
			else 
			{
				for(QueryFilter qf : mdxFilters)
				{
					QueryFilter clonedqf = (QueryFilter)qf.clone();
					clonedqf = convertMultiValueFilterToComplexFilter(r, session, clonedqf);
					clonedqf = clonedqf.replaceVariables(r, session);
					boolean discardFilter = false;
					if(clonedqf.getType() == QueryFilter.TYPE_PREDICATE)
					{
						String operand = clonedqf.getOperand();
						if(operand == null || operand.trim().isEmpty())
						{
							discardFilter = true;
						}
					}
					if(!discardFilter && !tsfList.contains(clonedqf))
					{
						tsfList.add(clonedqf);
					}
				}
			}
		}
		
		return tsfList;
	}
	
	public static QueryFilter convertMultiValueFilterToComplexFilter(Repository r, Session session, QueryFilter qf)
	{
		QueryFilter result = qf;
		if (r.getVariables() != null && qf.getOperand() != null)
		{
			for (Variable v : r.getVariables())
			{
				if (v.isMultiValue())
				{
					if (qf.getOperand().equals("V{" + v.getName() + "}") || qf.getOperand().equals(r.replaceVariables(session, "V{" + v.getName() + "}")))
					{
						String operandValues = r.replaceVariables(session, qf.getOperand());
						if (operandValues.contains(","))
						{
							List<String> lst = Arrays.asList(operandValues.split(","));
							List<QueryFilter> listQf = new ArrayList<QueryFilter>(lst.size());
							for (int i = 0; i < lst.size(); i++)
							{
								String itemString = lst.get(i);
								listQf.add(new QueryFilter(qf.getDimension(), qf.getColumn(), qf.getOperator(), itemString));
							}
							result = new QueryFilter ("OR", listQf);
							break;
						}
					}
				}
			}
		}
		return result;
		
	}
}
