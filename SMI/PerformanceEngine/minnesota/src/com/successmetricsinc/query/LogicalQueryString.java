/**
 * $Id: LogicalQueryString.java,v 1.81 2012-11-16 00:59:49 birst\bpeters Exp $
 *
 * Copyright (C) 2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.MismatchedSetException;
import org.antlr.runtime.MismatchedTokenException;
import org.antlr.runtime.NoViableAltException;
import org.antlr.runtime.ParserRuleReturnScope;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.Tree;
import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.query.DisplayExpression.PositionType;
import com.successmetricsinc.query.olap.DimensionMemberSetNav;
import com.successmetricsinc.query.olap.OlapLogicalQueryString;
import com.successmetricsinc.query.olap.OlapMemberExpression;
import com.successmetricsinc.transformation.BirstScriptLexer;
import com.successmetricsinc.transformation.BirstScriptParser;
import com.successmetricsinc.transformation.BirstScriptParser.logicalExpression_return;
import com.successmetricsinc.transformation.BirstScriptParser.logicalMemberSet_return;
import com.successmetricsinc.transformation.Expression;
import com.successmetricsinc.transformation.NoFilterValueException;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.Statement;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.transformation.BirstScriptParser.logicalColumn_return;
import com.successmetricsinc.transformation.BirstScriptParser.logicalquery_return;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.DateUtil;

/**
 * @author Brad
 * 
 */
public class LogicalQueryString extends AbstractQueryString
{
	private static final Logger logger = Logger.getLogger(LogicalQueryString.class);
	public static String BEGINMACRO = "<EVAL!";
	public static String ENDMACRO = "!>";
	protected Repository r;
	protected Session session;
	protected String logicalQuery;
	protected Tree queryTree;
	protected List<DisplayFilter> displayFilters = new ArrayList<DisplayFilter>();
	protected List<DisplayExpression> expressionList = new ArrayList<DisplayExpression>();
	protected List<DisplayOrder> displayOrderList = new ArrayList<DisplayOrder>();
	protected int curColumn;

	public LogicalQueryString(Repository r, Session session, String logicalQuery)
	{
		this.r = r;
		this.session = session;
		try
		{
			logicalQuery = processMacros(r, session, logicalQuery);
		} catch (SyntaxErrorException e)
		{
			logger.error("Unable to parse macro");
		} catch (ScriptException e)
		{
			logger.error("Unable to parse macro");
		}
		this.logicalQuery = logicalQuery;
	}

	public LogicalQueryString(Repository r, Session session, Tree queryTree)
	{
		this.r = r;
		this.session = session;
		this.queryTree = queryTree;
	}

	public void clear()
	{
		curColumn = 0;
		displayFilters.clear();
		expressionList.clear();
		displayOrderList.clear();
	}

	public Query getQuery() throws BaseException
	{
		if (queryTree != null)
		{
			return processQuery(queryTree, false);
		}
		logger.info("Logical Query: " + logicalQuery);
		ANTLRStringStream stream = new ANTLRStringStream(logicalQuery);
		BirstScriptLexer lex = new BirstScriptLexer(stream);
		CommonTokenStream cts = new CommonTokenStream(lex);
		BirstScriptParser parser = new BirstScriptParser(cts);
		try
		{
			logicalquery_return ret = parser.logicalquery();
			if (lex.hasError())
				processLexError(lex, logicalQuery);
			return processQuery((Tree) ret.getTree(), false);
		} catch (RecognitionException e)
		{
			processException(e, lex, logicalQuery);
		}
		return null;
	}

	public static void setupDisplayExpression(Repository r, Session session, String expression, DisplayExpression qse, boolean isOlapExpression) throws SyntaxErrorException
	{
		try
		{
			expression = processMacros(r, session, expression);
		} catch (SyntaxErrorException e)
		{
			logger.error("Unable to parse macro");
		} catch (ScriptException e)
		{
			logger.error("Unable to parse macro");
		}
		ANTLRStringStream stream = new ANTLRStringStream(expression);
		BirstScriptLexer lex = new BirstScriptLexer(stream);
		CommonTokenStream cts = new CommonTokenStream(lex);
		BirstScriptParser parser = new BirstScriptParser(cts);
		try
		{
			ParserRuleReturnScope ret = null;
			if (isOlapExpression)
			{
				ret = parser.logicalMemberSet();				
			}
			else
			{
				ret = parser.logicalColumn();			
			}
			if (lex.hasError())
				processLexError(lex, expression);
			Tree column = (Tree) ret.getTree();
			if ((column.getType() == BirstScriptParser.IDENT_NAME || column.getType() == BirstScriptParser.COLUMN_NAME) && column.getChildCount() == 0)
			{
				qse.setColumn(true);
				String s = column.getText().substring(1, column.getText().length() - 1);
				int index = s.indexOf('.');
				if (index < 0)
					qse.setColumnName(s);
				else
				{
					qse.setColumnName(s.substring(index+1));
					qse.setColumnDimension(s.substring(0,index));
				}
			}
			else if (column.getType() == BirstScriptParser.LOGICALMEMBERSET)
			{
				String logicalQuery = "SELECT " + expression + " FROM [CUBE]";
				ANTLRStringStream qstream = new ANTLRStringStream(logicalQuery);
				BirstScriptLexer qlex = new BirstScriptLexer(qstream);
				AbstractQueryString aqs = new OlapLogicalQueryString(r, session, logicalQuery);
				try
				{
					Query q = aqs.getQuery();
					if (q.dimensionMemberSets != null && q.dimensionMemberSets.size() > 0)
					{
						DimensionMemberSetNav dnav = q.dimensionMemberSets.get(0);
						qse.setColumn(false);
						qse.setColumnName("{" + dnav.getMemberExpressionsString() + "}");
						qse.setColumnDimension(dnav.dimensionName);
					}
				}
				catch (Exception e)
				{
					processException(e, lex, expression);
				}
			}
			TransformationScript ts = new TransformationScript(r, session);
			ts.setDisplayExpression(qse);
			ts.setLogicalQuery(true);
			// XXX add columns to transformation script - need to add InputDataElement's
			// see ProjectionlistResult, around line 233, to see how to properly create an InputDataElement
			Expression ex = new Expression(ts);
			qse.initialize();
			try
			{
				Operator op = ex.compile(column);
				if (r.isUseNewQueryLanguage())
					qse.setLogicalExpression(qse.getExpression(), ts, op, column);
				else {
					String exp = qse.getExpression();
					qse.setLogicalExpression(((exp.startsWith("alt:") ? "" : "alt:") + exp), ts, op, column);
				}
				if (ex.isRank)
					qse.setRank(true);
				else if (ex.isPTile)
					qse.setPercentile(true);
				else if (ex.isRSum)
					qse.setRSum(true);
				if(ex.isAggregate)
					qse.containsAggregateExpression = true;
				return;
			} catch (ScriptException e)
			{
				throw new SyntaxErrorException(e.getMessage());
			}
		} catch (RecognitionException e)
		{
			processException(e, lex, expression);
		}		
		return;
	}
	
	protected Query processQuery(Tree tree, boolean makeNamesPhysical) throws NavigationException, BadColumnNameException, SyntaxErrorException, ScriptException
	{
		Query q = new Query(r);
		processQuery(q, tree, makeNamesPhysical); 
		return q;
	}

	protected void processQuery(Query q, Tree tree, boolean makeNamesPhysical) throws NavigationException, BadColumnNameException, SyntaxErrorException, ScriptException
	{
		TransformationScript ts = null;
		for (int i = 0; i < tree.getChildCount(); i++)
		{
			Tree ctree = tree.getChild(i);
			if (ctree.getType() == BirstScriptParser.PROJECTIONLIST)
			{
				for (int j = 0; j < ctree.getChildCount(); j++)
				{
					Tree column = ctree.getChild(j);
					int type = column.getType();
					String display = ignoreDisplay ? null : getAlias(column);

					if (type == BirstScriptParser.COLUMN_NAME)
					{
						String name = column.getText();
						name = name.substring(1, name.length() - 1);
						int ppos = name.indexOf('.');
						DimensionColumnNav dcn = null;
						String dimName = name.substring(0, ppos);
						String colName = name.substring(ppos + 1, name.length());
						if (makeNamesPhysical)
						{
							if (display == null)
								display = colName;
							dcn = q.addDimensionColumn(dimName, colName, Util.replaceWithPhysicalString(display));
						} else if (display != null)
							dcn = q.addDimensionColumn(dimName, colName, display);
						else
							dcn = q.addDimensionColumn(dimName, colName);
						if (dcn == null)
							throw new BadColumnNameException(name);
						q.addGroupBy(new GroupBy(dimName, colName));
						curColumn++;
					} else if (type == BirstScriptParser.IDENT_NAME
							&& (column.getChildCount() == 0 || column.getChild(0).getType() != BirstScriptParser.POSITION))
					{
						String name = column.getText();
						name = name.substring(1, name.length() - 1);
						MeasureColumnNav mcn = null;
						if (display == null)
							display = name;
						if (makeNamesPhysical)
						{
							mcn = q.addMeasureColumn(name, Util.replaceWithPhysicalString(display), false);
						} else
						{
							mcn = q.addMeasureColumn(name, display, false);
						}
						if (mcn == null)
							throw new BadColumnNameException(name);
						// Measure filter
						if (column.getChildCount() > 0)
						{
							for (int ci = 0; ci < column.getChildCount(); ci++)
							{
								if (column.getChild(ci).getType() != BirstScriptParser.WHERE)
									continue;
								QueryFilter qf;
								try
								{
									qf = validateTreeFilter(column.getChild(ci).getChild(0), r, session, true);
								} catch (ScriptException e)
								{
									throw new SyntaxErrorException("Unable to apply filter to measure");
								}
								mcn.filter = qf;
								break;
							}
						}
						curColumn++;
					} else if (type == BirstScriptParser.MDXEXPRESSION)
					{
						if (display == null)
							display = "M" + new Random().nextInt();
						OlapLogicalQueryString.addOlapMemberExpression(q, column.getChild(0).getText(), display);
						curColumn++;
					} else
					{
						DisplayExpression qse = new DisplayExpression();
						if (ts == null)
						{
							ts = new TransformationScript(r, session);
							ts.setLogicalQuery(true);
						}
						ts.setDisplayExpression(qse);
						Expression ex = new Expression(ts);
						qse.initialize();
						String exp = getDisplayExpression(column);
						if (display == null)
							display = exp;
						try
						{
							Operator op = ex.compile(column);
							qse.setLogicalExpression(exp, ts, op, column);
							qse.setPosition(curColumn++);
							qse.setName(display);
							if (ex.isRank)
								qse.setRank(true);
							else if (ex.isPTile)
								qse.setPercentile(true);
							if(ex.isAggregate)
								qse.containsAggregateExpression = true;
							if (ex.isRSum)
								qse.setRSum(true);
							expressionList.add(qse);
							if (qse.olapMemberExpressionColumns != null && qse.olapMemberExpressionColumns.size() == 1)									
							{
								String olapExpDisplay = qse.olapMemberExpressionColumns.get(0).displayName;
								if (olapExpDisplay == null)
									olapExpDisplay = "M" + new Random().nextInt();
								OlapLogicalQueryString.addOlapMemberExpression(q, qse.olapMemberExpressionColumns.get(0).expression, olapExpDisplay);								
							}
						} catch (ScriptException e)
						{
							throw new SyntaxErrorException(e.getMessage());
						}
					}
				}
			} else if (ctree.getType() == BirstScriptParser.WHERE)
			{
				try
				{
					QueryFilter qf = validateTreeFilter(q, ctree.getChild(0), r, session, false);
					if (qf != null)
					{
						List<QueryFilter> splitFiltersList = qf.splitIntoIndividualFilters(false, true);
						if(splitFiltersList != null && splitFiltersList.size() > 0)
						{
							for(QueryFilter sqf : splitFiltersList)
							{
								q.addFilter(sqf);
							}
						}
						else
						{
							q.addFilter(qf);
						}
					}
				} catch (ScriptException e)
				{
					throw new SyntaxErrorException(e.getMessage());
				}
			} else if (ctree.getType() == BirstScriptParser.DISPLAYWHERE)
			{
				try
				{
					DisplayFilter df = validateTreeDisplayFilter(ctree.getChild(0), r, session);
					if (df != null)
						displayFilters.add(df);
				} catch (ScriptException e)
				{
					throw new SyntaxErrorException(e.getMessage());
				}
			} else if (ctree.getType() == BirstScriptParser.ORDERBY)
			{
				Tree projectionList = ctree.getChild(0);
				for (int j = 0; j < projectionList.getChildCount(); j++)
				{
					Tree projection = projectionList.getChild(j);
					if (projection.getType() == BirstScriptParser.COLUMN_NAME)
					{
						String name = projection.getText();
						name = name.substring(1, name.length() - 1);
						int ppos = name.indexOf('.');
						String dimName = name.substring(0, ppos);
						String colName = name.substring(ppos + 1, name.length());
						boolean dir = projection.getChildCount() == 0 ? true : projection.getChild(0).getText().toLowerCase().startsWith("asc");
						OrderBy ob = new OrderBy(dimName, colName, dir);
						q.addOrderBy(ob);
					} else if (projection.getType() == BirstScriptParser.IDENT_NAME)
					{
						String name = projection.getText();
						name = name.substring(1, name.length() - 1);
						boolean dir = projection.getChildCount() == 0 ? true : projection.getChild(0).getText().toLowerCase().startsWith("asc");
						boolean found = false;
						if (q.dimensionMemberSets != null && q.dimensionMemberSets.size() > 0)
						{
							for (DimensionMemberSetNav dms : q.dimensionMemberSets)
							{
								if (dms.getDisplayName() != null && dms.getDisplayName().equals(name))
								{
									found = true;
									dms.setSortDirection(dir);
									String dimHierarchy = dms.dimensionName;
									Hierarchy h = r.findHierarchy(dimHierarchy);
									if (h == null)
									{
										throw new BadColumnNameException(name);
									}
									String hName = dimHierarchy.substring((h.OlapDimensionName + "-").length());
									String calcName = hName + "-MemberName";
									OrderBy ob = new OrderBy(name, dir, calcName);
									q.addOrderBy(ob);
									break;
								}
							}
						}
						if (!found)
						{
							OrderBy ob = new OrderBy(name, dir);
							q.addOrderBy(ob);
						}
					}
				}
			} else if (ctree.getType() == BirstScriptParser.DISPLAYBY)
			{
				Tree projectionList = ctree.getChild(0);
				for (int j = 0; j < projectionList.getChildCount(); j++)
				{
					Tree projection = projectionList.getChild(j);
					if (projection.getType() == BirstScriptParser.COLUMN_NAME)
					{
						String name = projection.getText();
						name = name.substring(1, name.length() - 1);
						int ppos = name.indexOf('.');
						String dimName = name.substring(0, ppos);
						String colName = name.substring(ppos + 1, name.length());
						boolean dir = true;
						boolean nulltop = false;
						for (int k = 0; k < projection.getChildCount(); k++)
						{
							Tree t = projection.getChild(k);
							int type = t.getType();
							if (type == BirstScriptParser.NULLS_FIRST)
								nulltop = true;
							else if (type == BirstScriptParser.NULLS_LAST)
								nulltop = false;
							else if (type == BirstScriptParser.DESCENDING)
								dir = false;
							else if (type == BirstScriptParser.ASCENDING)
								dir = true;
						}
						DisplayOrder dorder = new DisplayOrder();
						dorder.ascending = dir;
						dorder.tableName = dimName;
						dorder.columnName = colName;
						dorder.nulltop = nulltop;
						displayOrderList.add(dorder);
					} else if (projection.getType() == BirstScriptParser.IDENT_NAME)
					{
						String name = projection.getText();
						name = name.substring(1, name.length() - 1);
						boolean dir = projection.getChildCount() == 0 ? true : projection.getChild(0).getText().toLowerCase().startsWith("asc");
						DisplayOrder dorder = new DisplayOrder();
						dorder.ascending = dir;
						dorder.columnName = name;
						displayOrderList.add(dorder);
					}
				}
			} else if (ctree.getType() == BirstScriptParser.TOP)
			{
				if (ts == null)
				{
					ts = new TransformationScript(r, session);
					ts.setLogicalQuery(true);
				}
				Expression ex = new Expression(ts);
				Operator op = ex.compile(ctree.getChild(0));
				try {
					Object value = op.evaluate();
					if (value != null) {
						if (value instanceof Number) {
							try {
								q.setTopn(((Number)value).intValue());
							}
							catch (Exception e) {}
						}
						else if (value instanceof String) {
							try {
								q.setTopn(Integer.valueOf((String)value));
							}
							catch (Exception e) {}
						}
					}
				}
				catch (NoFilterValueException nfe) {}
			} else if (ctree.getType() == BirstScriptParser.USINGOUTERJOIN)
			{
				q.setFullOuterJoin(true);
			}
		}
		pickDimensionExpressionLevels(q);
		q.setSession(session);		
	}
	
	/*
	 * For Dimension Expressions that have level specifiers "{@[dimension.column][dimension.column]:..." pick the ones
	 * that will be used in this expression. If a column is specified, make sure it is present in the query, if not look
	 * for a default.
	 */
	private void pickDimensionExpressionLevels(Query q)
	{
		if (expressionList == null)
			return;
		for (DisplayExpression de: expressionList)
		{
			if (de.measureExpressionColumns == null)
				continue;
			for (MeasureExpressionColumn mec : de.measureExpressionColumns)
			{
				if (mec.positions.size() <= 1 || mec.positions.get(0).ptype != PositionType.DimensionExpression)
					continue;
				Position picked = null;
				Position defaultPick = null;
				for (int i = 0; i < mec.positions.size(); i++)
				{
					Position p = mec.positions.get(i);
					if (p.dimensionExpressionLevels == null || p.dimensionExpressionLevels.length == 0)
					{
						defaultPick = p;
						continue;
					}
					boolean match = true;
					for(String column: p.dimensionExpressionLevels)
					{
						boolean found = false;
						for(DimensionColumnNav dcn: q.dimensionColumns)
						{
							if (column.equals("[" + dcn.dimensionName + "." + dcn.columnName + "]"))
							{
								found = true;
								break;
							}
						}
						if (!found)
						{
							match = false;
							break;
						}
					}
					if (match)
					{
						picked = p;
						break;
					}
				}
				if (defaultPick == null)
					defaultPick = mec.positions.get(0);
				mec.positions.clear();
				if (picked != null)
					mec.positions.add(picked);
				else
					mec.positions.add(defaultPick);
			}
		}
	}

	/*
	 * find the alias, if it exists, delete it, and return the alias value
	 */
	protected String getAlias(Tree column)
	{
		for (int i = 0; i < column.getChildCount(); i++)
		{
			Tree child = column.getChild(i);
			if (child.getType() == BirstScriptParser.ALIAS)
			{
				String display = child.getChild(0).getText();
				display = display.substring(1, display.length() - 1);
				column.deleteChild(i); // remove the alias
				return display;
			}
		}
		return null;
	}
	
	private String getDisplayExpression(Tree column) throws ScriptException
	{
		return Statement.toString(column, null);
	}
	
	public static QueryFilter validateTreeFilter(Tree t, Repository r, Session session, boolean measureFilter) throws SyntaxErrorException, ScriptException
	{
		return validateTreeFilter(null, t, r, session, measureFilter);
	}

	public static QueryFilter validateTreeFilter(Query q, Tree t, Repository r, Session session, boolean measureFilter) throws SyntaxErrorException, ScriptException
	{
		int type = t.getType();
		if (type == BirstScriptParser.AND || type == BirstScriptParser.OR)
		{
			Query query = (q != null && q.dimensionMemberSets != null && q.dimensionMemberSets.size() > 0 ? q : null);
			QueryFilter qf1 = validateTreeFilter(query, t.getChild(0), r, session, measureFilter);
			QueryFilter qf2 = validateTreeFilter(query, t.getChild(1), r, session, measureFilter);
			if (qf1 == null && qf2 == null)
				return null;
			if (qf1 == null)
				return qf2;
			if (qf2 == null)
				return qf1;
			if (type == BirstScriptParser.AND)
				return new QueryFilter(qf1, "AND", qf2);
			else
				return new QueryFilter(qf1, "OR", qf2);
		} else if (type == BirstScriptParser.PAREN)
		{
			Query query = (q != null && q.dimensionMemberSets != null && q.dimensionMemberSets.size() > 0 ? q : null);
			return validateTreeFilter(query, t.getChild(0), r, session, measureFilter);
		} else if (type == BirstScriptParser.GETPROMPTFILTER)
		{
			if (session != null)
			{
				List<Filter> params = session.getParams();
				if (params != null)
				{
					TransformationScript ts = new TransformationScript(r, session);
					Expression ex = new Expression(ts);
					Operator op = ex.compile(t.getChild(0));
					Object o = op.evaluate();
					String cname = o.toString();
					Filter f = session.getFilter(cname);
					if (f != null)
					{
						if (f.getFilterType() == Filter.FilterType.DATA)
						{
							int dotindex = cname.indexOf('.');
							List<String> values = f.getValues();
							if (dotindex >= 0)
							{
								if (values != null)
								{
									String dim = cname.substring(0, dotindex);
									String att = cname.substring(dotindex + 1);
									if (values.size() == 1)
									{
										if (values.get(0).equals("NO_FILTER"))
											return null;
										return new QueryFilter(dim, att, f.getOperator(), values.get(0));
									} else if (values.size() > 1)
									{
										List<QueryFilter> qflist = new ArrayList<QueryFilter>();
										for (String s : values)
											qflist.add(new QueryFilter(dim, att, f.getOperator(), s));
										return new QueryFilter(f.getLogicalOperator(), qflist);
									}
								} else 
									return null;
							} else
							{
								if (measureFilter)
								{
									throw new SyntaxErrorException("Cannot filter on a measure column inside a measure filter");
								}
								if (values.size() == 1)
								{
									if (values.get(0).equals("NO_FILTER"))
										return null;
									return new QueryFilter(cname, f.getOperator(), values.get(0));
								} else if (values.size() > 1)
								{
									List<QueryFilter> qflist = new ArrayList<QueryFilter>();
									for (String s : values)
										qflist.add(new QueryFilter(cname, f.getOperator(), s));
									return new QueryFilter(f.getLogicalOperator(), qflist);
								}
							}
						}
					}
				}
			}
			return null;
		} else if (type == BirstScriptParser.EQUALS || type == BirstScriptParser.NOTEQUALS || type == BirstScriptParser.LT || type == BirstScriptParser.GT
				|| type == BirstScriptParser.LTEQ || type == BirstScriptParser.GTEQ || type == BirstScriptParser.LIKE || type == BirstScriptParser.NOTLIKE)
		{
			String operator = getOperatorString(type);
			TransformationScript ts = new TransformationScript(r, session);
			if (t.getChild(0).getType() == BirstScriptParser.COLUMN_NAME || t.getChild(0).getType() == BirstScriptParser.IDENT_NAME)
			{
				if (containsColumn(t.getChild(1)))
					throw new SyntaxErrorException("Unable to parse WHERE clause (RHS contains column)");
				Expression ex = new Expression(ts);
				Operator op = ex.compile(t.getChild(1));
				Object o = null;
				try
				{
					o = op.evaluate();
				}
				catch (NoFilterValueException nfve)
				{
					// there was a NO_FILTER_TEXT value in the evaluation stack, don't add the filter to the query
				}
				if (o != null)
				{
					String operandString = getValue(o, r.getServerParameters().getProcessingTimeZone());
					if (t.getChild(0).getType() == BirstScriptParser.COLUMN_NAME)
					{
						String colname = t.getChild(0).getText();
						colname = colname.substring(1, colname.length() - 1);
						int index = colname.indexOf('.');
						String dim = colname.substring(0, index);
						String atr = colname.substring(index + 1);
						if (o instanceof List) // occurs as a result of GETPROMPTVALUE returning a list of items (multiple select)
						{
							@SuppressWarnings("unchecked")
							List<Object> lst = (List<Object>) o;
							List<QueryFilter> listQf = new ArrayList<QueryFilter>(lst.size());
							for (int i = 0; i < lst.size(); i++)
							{
								Object item = lst.get(i);
								String itemString = getValue(item, r.getServerParameters().getProcessingTimeZone());
								listQf.add(new QueryFilter(dim, atr, operator, itemString));
							}
							return new QueryFilter("OR", listQf);
						}
						else if ("null".equals(o) && "=".equals(operator)) {
							return new QueryFilter(dim, atr, QueryFilter.ISNULL, operandString);
						}
						else if ("null".equals(o) && "<>".equals(operator)){
							return new QueryFilter(dim, atr, QueryFilter.ISNOTNULL, operandString);
						}
						return new QueryFilter(dim, atr, operator, operandString);
					} else
					{
						if (measureFilter)
						{
							throw new SyntaxErrorException("Cannot filter on a measure column inside a measure filter");
						}
						Tree mtree = t.getChild(0);
						String mname = mtree.getText();
						mname = mname.substring(1, mname.length() - 1);
						
						if (q != null && q.dimensionMemberSets != null && !q.dimensionMemberSets.isEmpty())
						{
							for (DimensionMemberSetNav dms : q.dimensionMemberSets)
							{
								if (dms.displayName != null && dms.displayName.equals(mname))
								{
									return new QueryFilter(dms.dimensionName, mname, operator, operandString, QueryFilter.TYPE_DIMENSION_MEMBER_SET);
								}
							}
						}						
						if (mtree.getChildCount() == 0)
							return new QueryFilter(mname, operator, operandString);
						QueryFilter mFilter = validateTreeFilter(mtree.getChild(0).getChild(0), r, session, true);
						return new QueryFilter(mname, operator, operandString, mFilter);
					}
				}
				return null;
			} else if (t.getChild(1).getType() == BirstScriptParser.COLUMN_NAME || t.getChild(1).getType() == BirstScriptParser.IDENT_NAME)
			{
				if (containsColumn(t.getChild(0)))
					throw new SyntaxErrorException("Unable to parse WHERE clause (LHS contains column)");
				Expression ex = new Expression(ts);
				Operator op = ex.compile(t.getChild(1));
				Object o = op.evaluate();
				String operandString = getValue(o, r.getServerParameters().getProcessingTimeZone());
				if (t.getChild(1).getType() == BirstScriptParser.COLUMN_NAME)
				{
					String colname = t.getChild(1).getText();
					colname = colname.substring(1, colname.length() - 1);
					int index = colname.indexOf('.');
					return new QueryFilter(colname.substring(0, index), colname.substring(index + 1), operator, operandString);
				} else
				{
					if (measureFilter)
					{
						throw new SyntaxErrorException("Cannot filter on a measure column inside a measure filter");
					}
					Tree mtree = t.getChild(1);
					String mname = mtree.getText();
					mname = mname.substring(1, mname.length() - 1);
					if (mtree.getChildCount() == 0)
						return new QueryFilter(mname, operator, operandString);
					QueryFilter mFilter = validateTreeFilter(mtree.getChild(0).getChild(0), r, session, true);
					return new QueryFilter(mname, operator, operandString, mFilter);
				}
			} else
				throw new SyntaxErrorException("Unable to parse WHERE clause (no column on LHS or RHS)");
		} else if (type == BirstScriptParser.IN)
		{
			String colname = t.getChild(0).getText();
			colname = colname.substring(0, colname.length() - 1);
			int index = colname.indexOf('.');
			Tree child1 = t.getChild(1);
			if (child1.getType() == BirstScriptParser.VALUELIST)
			{
				String vallist = buildValueList(child1);
				return new QueryFilter(colname.substring(1, index), colname.substring(index + 1), QueryFilter.IN, vallist);
			} else
			{
				return new QueryFilter(colname.substring(1, index), colname.substring(index + 1), QueryFilter.IN, child1);
			}
		} else if (type == BirstScriptParser.NOTIN)
		{
			String colname = t.getChild(0).getText();
			colname = colname.substring(0, colname.length() - 1);
			int index = colname.indexOf('.');
			Tree child1 = t.getChild(1);
			if (child1.getType() == BirstScriptParser.VALUELIST)
			{
				String vallist = buildValueList(child1);
				return new QueryFilter(colname.substring(1, index), colname.substring(index + 1), QueryFilter.NOTIN, vallist);
			} else
			{
				return new QueryFilter(colname.substring(1, index), colname.substring(index + 1), QueryFilter.NOTIN, child1);
			}
		} else if (type == BirstScriptParser.ISNULL)
		{
			if (t.getChild(0).getType() == BirstScriptParser.COLUMN_NAME)
			{
				String colname = t.getChild(0).getText();
				colname = colname.substring(0, colname.length() - 1);
				int index = colname.indexOf('.');
				return new QueryFilter(colname.substring(1, index), colname.substring(index + 1), QueryFilter.ISNULL, (String) null);
			} else
			{
				Tree mtree = t.getChild(0);
				String mname = mtree.getText();
				mname = mname.substring(1, mname.length() - 1);
				if (mtree.getChildCount() != 0)
					throw new SyntaxErrorException("Measure filter syntax not allowed in WHERE clauses");
				return new QueryFilter(mname, QueryFilter.ISNULL, (String) null);
			}			
		} else if (type == BirstScriptParser.ISNOTNULL)
		{
			if (t.getChild(0).getType() == BirstScriptParser.COLUMN_NAME)
			{
				String colname = t.getChild(0).getText();
				colname = colname.substring(0, colname.length() - 1);
				int index = colname.indexOf('.');
				return new QueryFilter(colname.substring(1, index), colname.substring(index + 1), QueryFilter.ISNOTNULL, (String) null);
			} else
			{
				Tree mtree = t.getChild(0);
				String mname = mtree.getText();
				mname = mname.substring(1, mname.length() - 1);
				if (mtree.getChildCount() != 0)
					throw new SyntaxErrorException("Measure filter syntax not allowed in WHERE clauses");
				return new QueryFilter(mname, QueryFilter.ISNOTNULL, (String) null);
			}	
		} else
			throw new SyntaxErrorException("Unable to parse WHERE clause (unknown operator: " + type + ')');
	}

	public static DisplayFilter validateTreeDisplayFilter(Tree t, Repository r, Session session) throws SyntaxErrorException, ScriptException
	{
		int type = t.getType();
		if (type == BirstScriptParser.AND || type == BirstScriptParser.OR)
		{
			DisplayFilter df1 = validateTreeDisplayFilter(t.getChild(0), r, session);
			DisplayFilter df2 = validateTreeDisplayFilter(t.getChild(1), r, session);
			if (df1 == null && df2 == null)
				return null;
			if (df1 == null)
				return df2;
			if (df2 == null)
				return df1;
			if (type == BirstScriptParser.AND)
				return new DisplayFilter(df1, DisplayFilter.Operator.And, df2);
			else
				return new DisplayFilter(df1, DisplayFilter.Operator.Or, df2);
		} else if (type == BirstScriptParser.PAREN)
		{
			return validateTreeDisplayFilter(t.getChild(0), r, session);
		} else if (type == BirstScriptParser.GETPROMPTFILTER)
		{
			if (session != null)
			{
				List<Filter> params = session.getParams();
				if (params != null)
				{
					TransformationScript ts = new TransformationScript(r, session);
					Expression ex = new Expression(ts);
					Operator op = ex.compile(t.getChild(0));
					Object o = op.evaluate();
					String cname = o.toString();
					Filter f = session.getFilter(cname);
					if (f != null)
					{
						if (f.getFilterType() == Filter.FilterType.DISPLAY)
						{
							int dotindex = cname.indexOf('.');
							List<String> values = f.getValues();
							if (dotindex >= 0)
							{
								String dim = cname.substring(0, dotindex);
								String att = cname.substring(dotindex + 1);
								if (values.size() == 1)
								{
									if (values.get(0).equals("NO_FILTER"))
										return null;
									return new DisplayFilter(dim, att, f.getOperator(), values.get(0));
								} else if (values.size() > 1)
								{
									List<DisplayFilter> qflist = new ArrayList<DisplayFilter>();
									for (String s : values)
										qflist.add(new DisplayFilter(dim, att, f.getOperator(), s));
									return new DisplayFilter(f.getLogicalOperator(), qflist);
								}
							} else
							{
								if (values.size() == 1)
								{
									if (values.get(0).equals("NO_FILTER"))
										return null;
									return new DisplayFilter(cname, f.getOperator(), values.get(0));
								} else if (values.size() > 1)
								{
									List<DisplayFilter> qflist = new ArrayList<DisplayFilter>();
									for (String s : values)
										qflist.add(new DisplayFilter(cname, f.getOperator(), s));
									return new DisplayFilter(f.getLogicalOperator(), qflist);
								}
							}
						}
					}
				}
			}
			return null;
		} else if (type == BirstScriptParser.EQUALS || type == BirstScriptParser.NOTEQUALS || type == BirstScriptParser.LT || type == BirstScriptParser.GT
				|| type == BirstScriptParser.LTEQ || type == BirstScriptParser.GTEQ || type == BirstScriptParser.LIKE || type == BirstScriptParser.NOTLIKE)
		{
			String operator = getOperatorString(type);
			TransformationScript ts = new TransformationScript(r, session);
			if (t.getChild(0).getType() == BirstScriptParser.COLUMN_NAME || t.getChild(0).getType() == BirstScriptParser.IDENT_NAME)
			{
				if (containsColumn(t.getChild(1)))
					throw new SyntaxErrorException("Unable to parse DISPLAY WHERE clause");
				Expression ex = new Expression(ts);
				Operator op = ex.compile(t.getChild(1));
				Object o = null;
				try
				{
					o = op.evaluate();
				}
				catch (NoFilterValueException nfve)
				{
					// there was a NO_FILTER_TEXT value in the evaluation stack, don't add the filter to the query
				}
				if (o != null)
				{
					if (t.getChild(0).getType() == BirstScriptParser.COLUMN_NAME)
					{
						String colname = t.getChild(0).getText();
						colname = colname.substring(1, colname.length() - 1);
						int index = colname.indexOf('.');
						String dim = colname.substring(0, index);
						String atr = colname.substring(index + 1);
						return LogicalQueryString.buildDisplayFilter(r, o, operator, dim, atr);
					} else
					{
						// aliases end up here (XXX they currently look like measures, [F1])
						Tree mtree = t.getChild(0);
						if (mtree.getChildCount() != 0)
							throw new SyntaxErrorException("Measure filter syntax not allowed in DISPLAY WHERE clauses");						
						String mname = mtree.getText();
						mname = mname.substring(1, mname.length() - 1);
						return LogicalQueryString.buildDisplayFilter(r, o, operator, null, mname);
					}
				}
				return null;
			} else if (t.getChild(1).getType() == BirstScriptParser.COLUMN_NAME || t.getChild(1).getType() == BirstScriptParser.IDENT_NAME)
			{
				if (containsColumn(t.getChild(0)))
					throw new SyntaxErrorException("Unable to parse DISPLAY WHERE clause");
				Expression ex = new Expression(ts);
				Operator op = ex.compile(t.getChild(1));
				Object o = op.evaluate();
				String operandString = getValue(o, r.getServerParameters().getProcessingTimeZone());
				if (t.getChild(1).getType() == BirstScriptParser.COLUMN_NAME)
				{
					String colname = t.getChild(1).getText();
					colname = colname.substring(1, colname.length() - 1);
					int index = colname.indexOf('.');
					return new DisplayFilter(colname.substring(0, index), colname.substring(index + 1), operator, operandString);
				} else
				{
					Tree mtree = t.getChild(1);
					if (mtree.getChildCount() != 0)
						throw new SyntaxErrorException("Measure filter syntax not allowed in DISPLAY WHERE clauses");					
					String mname = mtree.getText();
					mname = mname.substring(1, mname.length() - 1);
					return new DisplayFilter(mname, operator, operandString);
				}
			} else
				throw new SyntaxErrorException("Unable to parse DISPLAY WHERE clause");
		} else if (type == BirstScriptParser.IN)
		{
			String colname = t.getChild(0).getText();
			colname = colname.substring(0, colname.length() - 1);
			int index = colname.indexOf('.');
			Tree child1 = t.getChild(1);
			if (child1.getType() == BirstScriptParser.VALUELIST)
			{
				String vallist = buildValueList(child1);
				return new DisplayFilter(colname.substring(1, index), colname.substring(index + 1), QueryFilter.IN, vallist);
			} else
			{
				return new DisplayFilter(colname.substring(1, index), colname.substring(index + 1), QueryFilter.IN, child1);
			}
		} else if (type == BirstScriptParser.NOTIN)
		{
			String colname = t.getChild(0).getText();
			colname = colname.substring(0, colname.length() - 1);
			int index = colname.indexOf('.');
			Tree child1 = t.getChild(1);
			if (child1.getType() == BirstScriptParser.VALUELIST)
			{
				String vallist = buildValueList(child1);
				return new DisplayFilter(colname.substring(1, index), colname.substring(index + 1), QueryFilter.NOTIN, vallist);
			} else
			{
				return new DisplayFilter(colname.substring(1, index), colname.substring(index + 1), QueryFilter.NOTIN, child1);
			}
		} else if (type == BirstScriptParser.ISNULL)
		{
			if (t.getChild(0).getType() == BirstScriptParser.COLUMN_NAME)
			{
				String colname = t.getChild(0).getText();
				colname = colname.substring(0, colname.length() - 1);
				int index = colname.indexOf('.');
				return new DisplayFilter(colname.substring(1, index), colname.substring(index + 1), QueryFilter.ISNULL, null);
			} else
			{
				Tree mtree = t.getChild(0);
				String mname = mtree.getText();
				mname = mname.substring(1, mname.length() - 1);
				if (mtree.getChildCount() != 0)
					throw new SyntaxErrorException("Measure filter syntax not allowed in DISPLAY WHERE clauses");
				return new DisplayFilter(mname, QueryFilter.ISNULL, null);
			}
		} else if (type == BirstScriptParser.ISNOTNULL)
		{
			if (t.getChild(0).getType() == BirstScriptParser.COLUMN_NAME)
			{
				String colname = t.getChild(0).getText();
				colname = colname.substring(0, colname.length() - 1);
				int index = colname.indexOf('.');
				return new DisplayFilter(colname.substring(1, index), colname.substring(index + 1), QueryFilter.ISNOTNULL, null);
			} else
			{
				Tree mtree = t.getChild(0);
				String mname = mtree.getText();
				mname = mname.substring(1, mname.length() - 1);
				if (mtree.getChildCount() != 0)
					throw new SyntaxErrorException("Measure filter syntax not allowed in DISPLAY WHERE clauses");
				return new DisplayFilter(mname, QueryFilter.ISNOTNULL, null);
			}		
		} else
			throw new SyntaxErrorException("Unable to parse DISPLAY WHERE clause");
	}
	
	/*
	 * build display filters for columns, measures, and aliases
	 * - deal with date formats and lists of items
	 */
	private static DisplayFilter buildDisplayFilter(Repository r, Object o, String operator, String dim, String atr)
	{
		String operandString = getValue(o, r.getServerParameters().getProcessingTimeZone());
		if (o instanceof List) // occurs as a result of GETPROMPTVALUE returning a list of items (multiple select)
		{
			@SuppressWarnings("unchecked")
			List<Object> lst = (List<Object>) o;
			List<DisplayFilter> listQf = new ArrayList<DisplayFilter>(lst.size());
			for (int i = 0; i < lst.size(); i++)
			{
				Object item = lst.get(i);
				String itemString = getValue(item, r.getServerParameters().getProcessingTimeZone());
				DisplayFilter df = null;
				if (dim == null)
				{
					MeasureColumn mc = r.findMeasureColumn(atr);
					String dataType = (mc != null) ? mc.DataType : null; 
					df = new DisplayFilter(null, atr, operator, itemString, dataType);
				}
				else
				{
					DimensionColumn dc = r.findDimensionColumn(dim, atr);
					String dataType = (dc != null) ? dc.DataType : null;
					df = new DisplayFilter(dim, atr, operator, itemString, dataType);
				}
				listQf.add(df);
			}
			return new DisplayFilter("OR", listQf);
		}	
		String dataType = null;
		if (dim == null)
		{
			MeasureColumn mc = r.findMeasureColumn(atr);
			if (mc != null)
				dataType = mc.DataType;
		}
		else
		{
			DimensionColumn dc = r.findDimensionColumn(dim, atr);
			if (dc != null)
				dataType = dc.DataType;
		}		
		return new DisplayFilter(dim, atr, operator, operandString, dataType);
	}
	
	/*
	 * deal with formatting date/datetime
	 */
	public static String getValue(Object o, TimeZone processingTZ)
	{
		String operandString = null;
		if (Date.class.isInstance(o))
		{
			SimpleDateFormat outputdatetimeformat = new SimpleDateFormat(DateUtil.DEFAULT_DATETIME_FORMAT); // not thread safe, moved to here from static
			outputdatetimeformat.setTimeZone(processingTZ);
			operandString = outputdatetimeformat.format((Date) o);
		}
		else if (GregorianCalendar.class.isInstance(o))
		{
			SimpleDateFormat outputdatetimeformat = new SimpleDateFormat(DateUtil.DEFAULT_DATETIME_FORMAT); // not thread safe, moved to here from static
			outputdatetimeformat.setTimeZone(processingTZ);
			operandString = outputdatetimeformat.format(((GregorianCalendar) o).getTime());
		}
		else
			operandString = o.toString().trim();
		return operandString;
	}

	/**
	 * build a valuestring that has the leading/trailng quotes (per item) removed
	 * 
	 * @param child
	 * @return
	 */
	private static String buildValueList(Tree child)
	{
		// need to build string and unescape elements
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < child.getChildCount(); i++)
		{
			String val = child.getChild(i).getText();
			if ((val.charAt(0) == '\'' && val.charAt(val.length() - 1) == '\'') || (val.charAt(0) == '\"' && val.charAt(val.length() - 1) == '\"'))
			{
				val = val.substring(1, val.length() - 1);
			}
			if (sb.length() > 0)
				sb.append(',');
			sb.append(val);
		}
		return sb.toString();
	}

	private static String getOperatorString(int type)
	{
		switch (type)
		{
		case BirstScriptParser.EQUALS:
			return "=";
		case BirstScriptParser.NOTEQUALS:
			return "<>";
		case BirstScriptParser.LT:
			return "<";
		case BirstScriptParser.GT:
			return ">";
		case BirstScriptParser.LTEQ:
			return "<=";
		case BirstScriptParser.GTEQ:
			return ">=";
		case BirstScriptParser.LIKE:
			return "LIKE";
		case BirstScriptParser.NOTLIKE:
			return "NOT LIKE";
		}
		logger.debug("LogicalQueryString:getOperatorString on unknown type: " + type);
		return "="; // XXX this is unlikely to be correct
	}

	private static boolean containsColumn(Tree t)
	{
		if (t.getType() == BirstScriptParser.COLUMN_NAME || t.getType() == BirstScriptParser.IDENT_NAME)
			return true;
		for (int i = 0; i < t.getChildCount(); i++)
		{
			if (containsColumn(t.getChild(i)))
				return true;
		}
		return false;
	}

	@SuppressWarnings("unused")
	public static void processLexError(BirstScriptLexer lex, String queryString) throws SyntaxErrorException
	{
		RecognitionException e = lex.errorMessage();
		String error = null;
		int line = -1;
		int start = -1;
		int end = -1;
		if (MismatchedTokenException.class.isInstance(e))
		{
			MismatchedTokenException mte = (MismatchedTokenException) e;
			error = "Syntax error at line " + mte.line + (mte.charPositionInLine >= 0 ? ", column " + mte.charPositionInLine : "");
			line = mte.line;
			start = mte.charPositionInLine;
			logger.info(error);
		} else if (MismatchedSetException.class.isInstance(e))
		{
			MismatchedSetException mse = (MismatchedSetException) e;
			error = "Syntax error at line " + mse.line + (mse.charPositionInLine >= 0 ? ", column " + mse.charPositionInLine : "");
			line = mse.line;
			start = mse.charPositionInLine;
			logger.info(error);
		} else if (NoViableAltException.class.isInstance(e))
		{
			NoViableAltException nva = (NoViableAltException) e;
			error = "Syntax error at line " + nva.line + (nva.charPositionInLine >= 0 ? ", column " + nva.charPositionInLine : "");
			line = nva.line;
			start = nva.charPositionInLine;
			logger.info(error);
		} else
			error = e.getMessage();
		error = error + "\n" + queryString;
		throw new SyntaxErrorException(error);
	}

	@SuppressWarnings("unused")
	public static void processException(Exception e, BirstScriptLexer lex, String queryString) throws SyntaxErrorException
	{
		String error = null;
		int line = -1;
		int start = -1;
		int end = -1;
		if (MismatchedTokenException.class.isInstance(e))
		{
			if (lex != null && lex.hasError())
			{
				processLexError(lex, queryString);
			} else
			{
				MismatchedTokenException mte = (MismatchedTokenException) e;
				if (mte.charPositionInLine == -1)
					error = "Syntax error - unable to validate script";
				else
					error = "Syntax error at line " + mte.line + (mte.charPositionInLine >= 0 ? ", column " + mte.charPositionInLine : "")
							+ (mte.token.getText() != null ? ": " + mte.token.getText() : "");
				logger.info(error);
				line = mte.line;
				start = mte.charPositionInLine;
			}
		} else if (MismatchedSetException.class.isInstance(e))
		{
			MismatchedSetException mse = (MismatchedSetException) e;
			error = "Syntax error at line " + mse.line + (mse.charPositionInLine >= 0 ? ", column " + mse.charPositionInLine : "");
			line = mse.line;
			start = mse.charPositionInLine;
			logger.info(error);
		} else if (NoViableAltException.class.isInstance(e))
		{
			NoViableAltException nva = (NoViableAltException) e;
			line = nva.line;
			start = nva.charPositionInLine;
			error = "Syntax error at line " + nva.line + (nva.charPositionInLine >= 0 ? ", column " + nva.charPositionInLine : "");
			logger.info(error);
		} else
			error = e.getMessage();
		error = error + "\n" + queryString;
		throw new SyntaxErrorException(error);
	}

	public List<DisplayFilter> getDisplayFilters()
	{
		return displayFilters;
	}

	public void setDisplayFilters(List<DisplayFilter> displayFilters)
	{
		this.displayFilters = displayFilters;
	}

	public List<DisplayExpression> getExpressionList()
	{
		return expressionList;
	}

	public void setExpressionList(List<DisplayExpression> expressionList)
	{
		this.expressionList = expressionList;
	}

	public List<DisplayOrder> getDisplayOrderList()
	{
		return displayOrderList;
	}

	public void setDisplayOrderList(List<DisplayOrder> displayOrderList)
	{
		this.displayOrderList = displayOrderList;
	}

	public Tree getQueryTree()
	{
		return queryTree;
	}

	public void setQueryTree(Tree queryTree)
	{
		this.queryTree = queryTree;
	}

	@Override
	public Query processQueryString(String query, Session session, boolean makeNamesPhysical) throws SQLException, BaseException, CloneNotSupportedException
	{
		LogicalQueryString lqs = new LogicalQueryString(r, session, query);
		return lqs.getQuery();
	}
	
	private static String processMacros(Repository r, Session session, String logicalQuery) throws SyntaxErrorException, ScriptException
	{
		int start = 0;
		int index = logicalQuery.indexOf(BEGINMACRO, start);
		int end = 0;
		StringBuilder result = new StringBuilder();
		TransformationScript ts = null;
		while (index >= 0)
		{
			end = logicalQuery.indexOf(ENDMACRO, index);
			if (end < 0)
				break;
			String macro = logicalQuery.substring(index + BEGINMACRO.length(), end);
			result.append(logicalQuery.substring(start, index));
			if (ts == null)
				ts = new TransformationScript(r, session);
			ts.setLogicalQuery(true);
			//
			ANTLRStringStream stream = new ANTLRStringStream(macro);
			BirstScriptLexer lex = new BirstScriptLexer(stream);
			CommonTokenStream cts = new CommonTokenStream(lex);
			BirstScriptParser parser = new BirstScriptParser(cts);
			Tree t = null;
			try
			{
				logicalExpression_return ret = parser.logicalExpression();
				if (lex.hasError())
					processLexError(lex, macro);
				t = (Tree) ret.getTree();
			} catch (RecognitionException e)
			{
				processException(e, lex, macro);
			}
			//
			Expression ex = new Expression(ts);
			Operator op = ex.compile(t);
			result.append(op.evaluate());
			start = end + ENDMACRO.length();
			index = logicalQuery.indexOf(BEGINMACRO, start);
		}
		if (result.length() == 0)
			return logicalQuery;
		result.append(logicalQuery.substring(start));
		return result.toString();
	}
	
}
