/**
 * $Id: DatabaseConnectionResultSet.java,v 1.67 2012-10-11 09:08:34 BIRST\cpatel Exp $
 *
 * Copyright (C) 2007-2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipException;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.olap4j.OlapConnection;
import org.olap4j.OlapStatement;

import com.birst.dataconductor.QueryMetaData;
import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.query.Database.DatabaseType;
import com.successmetricsinc.security.EncryptionService;
import com.successmetricsinc.util.BaseException;

public class DatabaseConnectionResultSet
{
	private static Logger logger = Logger.getLogger(DatabaseConnectionResultSet.class);
	private ResultSet rs;
	private QueryMetaData qmd;
	private int curRow = -1;
	private List<Object[]> data;
	private boolean wasNull;
	private static int MAX_SOCKET_WAIT_TIME = 30 * 60 * 1000;
	private DatabaseConnection dc;
	private static final int GOOGLE_COLUMN_LIMIT = 100;

	public DatabaseConnectionResultSet(ResultSet rs, DatabaseConnection dc)
	{
		this.rs = rs;
		this.dc = dc;
	}

	public DatabaseConnectionResultSet(int[] dataTypes, List<Object[]> rows)
	{
		qmd = new QueryMetaData();
		qmd.setColumnCount(dataTypes.length);
		qmd.setColumnTypes(dataTypes);
		data = rows;
	}
	
	private void setupDatabaseConnectionResultSetForLiveAccess(byte[] rawbuff) throws Exception
	{
		ByteArrayInputStream bais = null;
		ObjectInputStream ois = null;
		try
		{
			GZIPInputStream gzis = new GZIPInputStream(new ByteArrayInputStream(rawbuff));
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			while (gzis.available() == 1)
			{
				baos.write(gzis.read());
			}
			baos.flush();
			byte[] unzippedarr = baos.toByteArray();
			ois = new ObjectInputStream(new ByteArrayInputStream(unzippedarr));
			qmd = (QueryMetaData) ois.readObject();
			if (qmd == null)
			{
				throw new SQLException("Unable to generate result set");
			}
			if (qmd.isError())
			{
				String eMessage = qmd.errorMessage();
				if (eMessage.contains("Too much data returned in the query"))
				{
					eMessage = eMessage.replace("java.lang.Exception: ", "");
					throw new ResultSetTooBigException(eMessage);
				}
				throw new SQLException(eMessage);
			}
			data = (List<Object[]>) ois.readObject();
		} 
		catch (Exception e)
		{
			if (e instanceof ClassNotFoundException)
				throw new SQLException("Unable to read data: " + e.getMessage());
			// In case stream wasn't compressed
			if (e instanceof ZipException)
			{
				bais = new ByteArrayInputStream(rawbuff);
				ois = new ObjectInputStream(bais);
				try
				{
					qmd = (QueryMetaData) ois.readObject();
					if (qmd == null)
					{
						throw new SQLException("Unable to generate result set");
					}
					if (qmd.isError())
					{
						throw new SQLException(qmd.errorMessage());
					}
					data = (List<Object[]>) ois.readObject();
				} catch (ClassNotFoundException e2)
				{
					throw new SQLException("Unable to read data: " + e2.getMessage());
				}
			}
			else
			{
				throw e;
			}
		}
	}
	
	public DatabaseConnectionResultSet(DatabaseConnection dc, Session session, Statement stmt, com.birst.dataconductor.PutResults pr, String mdxQuery) throws SQLException
	{
		try
		{
			Connection connection = null;
			if (dc.ConnectionPool.isDynamicConnnection() && session != null)
				connection = dc.ConnectionPool.getDynamicConnection(session);
			else
				connection = dc.ConnectionPool.getConnection();
			ByteArrayOutputStream baos = pr.getXMLAOutputStream((OlapConnection)connection, (OlapStatement)stmt, true);
			if (baos == null && pr.isCancelled())
			{
                baos = new ByteArrayOutputStream();
                GZIPOutputStream gzis = new GZIPOutputStream(baos);
                ObjectOutputStream oo = new ObjectOutputStream(gzis);
                QueryMetaData qmd = new QueryMetaData("Query Cancelled");
                oo.writeObject(qmd);
                oo.flush();
                oo.close();  
			}
			else if (baos!=null)
			{
				setupDatabaseConnectionResultSetForLiveAccess(baos.toByteArray());
			}
		}
		catch (IOException e)
		{
			throw new SQLException("IO Exception for Query: " + mdxQuery + "/" + e.getMessage(), e);
		} 
		catch (Exception e)
		{
			throw new SQLException("Exception for Query: " + mdxQuery + "/" + e.getMessage(), e);
		}
	}

	@SuppressWarnings("unchecked")
	public DatabaseConnectionResultSet(String serverName, int port, String id, String connectionName, String query, TimeZone processingTZ, DatabaseConnection dc, Session session, int maxRecords) throws SQLException, BaseException
	{
		Socket s = null;
		OutputStream os = null;
		InputStream is = null;
		try
		{
			this.dc = dc;
			String connectString = null, userName = null, password = null;
			boolean isDynamic = DatabaseConnection.isDynamicConnection(dc.ConnectString);
			if (isDynamic && session != null && session.getRepository() != null)
			{
				EncryptionService enc = EncryptionService.getInstance();
				connectString = session.getRepository().replaceVariables(session, dc.ConnectString);
				userName = session.getRepository().replaceVariables(session, dc.UserName);
				password = enc.encrypt(session.getRepository().replaceVariables(session, enc.decrypt(dc.Password)));			
			}
			s = new Socket();
			s.setSoLinger(false, 0);
			s.setSoTimeout(MAX_SOCKET_WAIT_TIME);
			s.connect(new InetSocketAddress(serverName, port));
			os = s.getOutputStream();
			is = s.getInputStream();
			StringBuilder body = new StringBuilder("query\r\n");
			body.append("ProcessingTZ=").append(processingTZ.getID()).append("\r\n");
			if (isDynamic && connectString != null && !connectString.isEmpty())
			{
				body.append("QueryConnectionParameters=").append(connectString).append("\t").append(userName).append("\t").append(password).append("\r\n");
			}
			body.append("MaxRecords=").append(maxRecords).append("\r\n");
			body.append(query);
			DatabaseConnectionStatement.writeRequest(os, id, connectionName, body.toString());
			s.setSoTimeout(20 * 60 * 1000);
			int length = DatabaseConnectionStatement.getLength(is);
			byte[] rawbuff = DatabaseConnectionStatement.readBuffer(is, length);
			setupDatabaseConnectionResultSetForLiveAccess(rawbuff);
		} catch (SocketTimeoutException ste)
		{
			throw new SQLException("Server not responding: " + ste.getMessage());
		} catch (UnknownHostException e)
		{
			throw new SQLException("Unknown Host: " + serverName + ":" + port);
		} catch (IOException e)
		{
			throw new SQLException("IO Exception for Query: " + query + "/" + e.getMessage(), e);
		} catch (ResultSetTooBigException sqle)
		{
			throw sqle;
		} catch (Exception e)
		{
			throw new SQLException("Exception for Query: " + query + "/" + e.getMessage(), e);
		} finally
		{
			try
			{
				if (os != null)
					os.close();
			} catch (IOException e)
			{
			}
			try
			{
				if (is != null)
					is.close();
			} catch (IOException e)
			{
			}
			try
			{
				if (s != null)
					s.close();
			} catch (IOException e)
			{
			}
		}
	}

	/**
	 * see http://code.google.com/apis/analytics/docs/gdata/gdataReferenceDataFeed.html
	 * 
	 * @param username
	 * @param password
	 * @param tableID
	 * @param dateString
	 * @param q
	 * @param query
	 * @throws SQLException
	 * @throws GoogleAnalyticsException
	 */
	public DatabaseConnectionResultSet(String username, String password, String tableID, String dateString, String q, Query query) throws SQLException,
			GoogleAnalyticsException
	{
		try
		{
			URL u = new URL("https://www.google.com/accounts/ClientLogin");
			HttpURLConnection conn = (HttpURLConnection) u.openConnection();
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream(),"UTF-8");
			writer.write("accountType=GOOGLE&Email=" + username + "&Passwd=" + password + "&service=analytics&source=Birst\r\n");
			writer.close();
			BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(),"UTF-8"));
			String line = null;
			Map<String, String> parms = new HashMap<String, String>();
			while ((line = reader.readLine()) != null)
			{
				String[] vals = line.split("=");
				if (vals.length == 2)
					parms.put(vals[0], vals[1]);
			}
			reader.close();
			conn.disconnect();
			
			String googleAnalyticsAPIKey = Repository.googleAnalyticsAPIKey;
			u = new URL("https://www.googleapis.com/analytics/v3/data/ga?ids=" + tableID + dateString + (q.length() > 0 ? "&" + q : "") + "&key="+googleAnalyticsAPIKey);
			logger.debug(u.getQuery());
			conn = (HttpURLConnection) u.openConnection();
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
			conn.setRequestProperty("Authorization", "GoogleLogin auth=" + parms.get("Auth"));
			
			StringWriter sw = new StringWriter();
			InputStream inputStream = conn.getInputStream();
			reader = new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
			while ((line = reader.readLine()) != null) {
 	    		sw.append(line).append("\n");
	    	} 
 	    	reader.close();
 	    	sw.flush();
 	    	sw.close();
 	    	
 	    	data = new ArrayList<Object[]>();
 	    	JSONParser parser = new JSONParser();
 	    	Object object = parser.parse(sw.toString());
 	    	JSONObject jsonObject = (JSONObject) object;
 	    	JSONArray colHeaders = (JSONArray) jsonObject.get("columnHeaders");
 	    		    	
			List<String> columnTypes = new ArrayList<String>();
			for(int i =0;i<colHeaders.size();i++)
			{
				JSONObject jObj = (JSONObject)colHeaders.get(i);
				String columnType = jObj.get("columnType").toString();
				columnTypes.add(columnType);
			}
			
			int coloumnSize = columnTypes.size();
			Navigation nav = query.getPrimaryNavigation();
			List<DimensionColumnNav> dclist = nav.getDimColumns();
			
			List<String> columnNames = new ArrayList<String>();
			for(int i =0;i<colHeaders.size();i++)
			{
				JSONObject jObj = (JSONObject)colHeaders.get(i);
				String name = jObj.get("name").toString();
				columnNames.add(name);
			}
			int[] types = new int[coloumnSize];
			if(jsonObject.containsKey("rows"))
			{
				JSONArray jArray_rows = (JSONArray) jsonObject.get("rows");
				for(int x =0;x<jArray_rows.size();x++)
				{
					JSONArray jRow = (JSONArray) jArray_rows.get(x);
					Object[] row = new Object[GOOGLE_COLUMN_LIMIT];
					for(int y=0;y<coloumnSize;y++)
					{
						String value = jRow.get(y).toString();
						String columnType = columnTypes.get(y).toString();
						if(columnType.equals("DIMENSION"))
						{
							int type = Types.VARCHAR;
							for (int i = 0; i < dclist.size(); i++)
							{
								DimensionColumn dc = dclist.get(i).matchingColumns.get(dclist.get(i).pick);
								if (dc.PhysicalName.equals(columnNames.get(y)))
								{
									if (dc.DataType.equals("Date"))
										type = Types.DATE;
									break;
								}
							}
							types[y] = type;
							if (types[y] == Types.DATE)
							{
								try
								{
									// date formats are not thread safe, can't use a static for this
									SimpleDateFormat googleAnalyticsFormat = new SimpleDateFormat("yyyyMMdd");
									Date d = googleAnalyticsFormat.parse(value);
									row[y] = (Object) new Timestamp(d.getTime());
								} catch (ParseException e)
								{
									row[y] = null; // value defaults to null if the date can not be converted to
														// a timestamp
								}
							} else
							{
								row[y] = value;
							}						
						}
						else if(columnType.equals("METRIC"))
						{
							int type = Types.FLOAT;
							double doubleValue = 0;
							try
							{
								if(value != null)
								{
									doubleValue = Double.valueOf(value);	
								}
							} catch (Exception e)
							{
								// value defaults to 0.0 if can not be parsed as a double
							}
							types[y] = type;
							row[y] = doubleValue;
						}
						
					}
					data.add(row);
				}
				
			}
			if (coloumnSize >= 1)
			{
				qmd = new QueryMetaData();
				qmd.setColumnCount(coloumnSize);
				qmd.setColumnTypes(types);
			}
		}
		catch (MalformedURLException e)
		{
			throw new SQLException(e.getMessage());
		}
		catch (IOException e)
		{
			if (e.getMessage() != null && e.getMessage().startsWith("Server returned HTTP response code: 400"))
			{
				throw new GoogleAnalyticsException(
						"Google Analytics responded that it is unable to query that combination of columns - please try a different combination");
			}
			throw new SQLException(e.getMessage());
		}
		catch (org.json.simple.parser.ParseException e) 
		{
			throw new SQLException(e.getMessage());
		} 
		
	}

	public int getColumnCount()
	{
		if (rs != null)
			try
			{
				return (rs.getMetaData().getColumnCount());
			} catch (SQLException e)
			{
				return 0;
			}
		if (qmd != null)
			return qmd.getColumnCount();
		return 0;
	}

	public int getColumnType(int index)
	{
		if (rs != null)
			try
			{
				return (rs.getMetaData().getColumnType(index));
			} catch (SQLException e)
			{
				return 0;
			}
		if (qmd != null)
			return qmd.getColumnType(index - 1);
		return 0;
	}

	public ResultSetMetaData getMetaData() throws SQLException
	{
		if (rs != null)
			return rs.getMetaData();
		return (null);
	}

	public boolean next() throws SQLException
	{
		if (rs != null)
			return rs.next();
		if (qmd != null)
		{
			return (++curRow < data.size());
		}
		return false;
	}

	public void close() throws SQLException
	{
		if (rs != null)
		{
			rs.close();
		}
	}

	public Long getLong(int index) throws SQLException
	{
		if (rs != null)
		{
			return rs.getLong(index);
		}
		if (qmd != null)
		{
			wasNull = data.get(curRow)[index - 1] == null;
			Object o = data.get(curRow)[index - 1];
			try
			{
				return o == null ? 0 : (Long)o;
			} catch (ClassCastException cce)
			{
				if (Integer.class.isInstance(o))
					return ((Integer) o).longValue();
				if (Double.class.isInstance(o))
					return ((Double) o).longValue();
				if (Float.class.isInstance(o))
					return ((Float) o).longValue();
				if (BigDecimal.class.isInstance(o))
					return ((BigDecimal) o).longValue();
				if (String.class.isInstance(o))
					return Long.valueOf((String) o);
				logger.debug("returning 0 for: " + (o != null ? o.toString() : "null") + " - " + cce.getMessage());
				return 0L;
			}
		}
		return 0L;
	}
	
	public double getDouble(int index) throws SQLException
	{
		if (rs != null)
		{
			return rs.getDouble(index);
		}
		if (qmd != null)
		{
			wasNull = data.get(curRow)[index - 1] == null;
			Object o = data.get(curRow)[index - 1];
			try
			{
				return o == null ? 0 : (Double) o;
			} catch (ClassCastException cce)
			{
				if (Integer.class.isInstance(o))
					return ((Integer) o).doubleValue();
				if (Long.class.isInstance(o))
					return ((Long) o).doubleValue();
				if (String.class.isInstance(o))
					return Double.valueOf((String) o);
				if (BigDecimal.class.isInstance(o))
					return ((BigDecimal) o).doubleValue();
				if (Float.class.isInstance(o))
					return ((Float) o).doubleValue();
				logger.debug("returning NaN for: " + (o != null ? o.toString() : "null") + " - " + cce.getMessage());
				return Double.NaN;
			}
		}
		logger.debug("returning NaN - no rs/qmd");
		return Double.NaN;
	}

	public Object getObject(int index) throws SQLException
	{
		if (rs != null)
		{
			if (dc != null && dc.DBType == DatabaseType.Infobright)
			{
				Object o = rs.getObject(index);
				if (BigDecimal.class.isInstance(o))
				{
					return ((BigDecimal) o).doubleValue();
				}
			}
			return rs.getObject(index);
		}
		if (qmd != null)
		{
			wasNull = data.get(curRow)[index - 1] == null;
			Object o = data.get(curRow)[index - 1];
			return o;
		}
		return null;
	}

	public String getString(int index) throws SQLException
	{
		if (rs != null)
		{
			return rs.getString(index);
		}
		if (qmd != null)
		{
			wasNull = data.get(curRow)[index - 1] == null;
			try
			{
				return (String) data.get(curRow)[index - 1];
			} catch (ClassCastException cce)
			{
				Object o = data.get(curRow)[index - 1];
				return o.toString();
			}
		}
		return null;
	}

	public Timestamp getTimestamp(int index, Calendar cal, boolean isDBTypeIB) throws SQLException
	{
		if (rs != null)
		{
			if (isDBTypeIB)
				return rs.getTimestamp(index);
			else
				return rs.getTimestamp(index, cal);
		}
		if (qmd != null)
		{
			Object o = data.get(curRow)[index - 1];
			wasNull = data.get(curRow)[index - 1] == null;
			try
			{
				return (Timestamp) o;
			} catch (ClassCastException cce)
			{
				if (Date.class.isInstance(o))
					return new Timestamp(((Date) o).getTime());
				if (Calendar.class.isInstance(o))
					return new Timestamp(((Calendar) o).getTime().getTime());
				logger.debug("returning current datetime for: " + (o != null ? o.toString() : "null") + " - " + cce.getMessage());
				return new Timestamp(System.currentTimeMillis());
			}
		}
		logger.debug("returning current datetime - no rs/qmd");
		return new Timestamp(System.currentTimeMillis());
	}

	public boolean wasNull() throws SQLException
	{
		if (rs != null)
		{
			return rs.wasNull();
		} else if (qmd != null)
		{
			return wasNull;
		}
		return false;
	}

	public int getNumRows()
	{
		if (data != null)
			return data.size();
		return 0;
	}
	
	public ResultSet getResultSet() {
		return rs;
	}
	
	public DatabaseConnection getDatabaseConnection() {
		return dc;
	}
}
