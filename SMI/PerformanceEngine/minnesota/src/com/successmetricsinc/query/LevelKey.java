/**
 * $Id: LevelKey.java,v 1.16 2012-11-12 18:25:20 BIRST\ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.io.Serializable;

import org.jdom.Element;
import org.jdom.Namespace;
import com.successmetricsinc.Repository;
import com.successmetricsinc.util.Util;

/**
 * @author Brad Peters
 * 
 */
public class LevelKey implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String[] ColumnNames;
	public boolean SurrogateKey;
	public Level Level;

	public LevelKey()
	{
	}

	public LevelKey(Element e, Namespace ns)
	{
		ColumnNames = Repository.getInternedStringArrayChildren(e, "ColumnNames", ns);
		SurrogateKey = Boolean.valueOf(e.getChildText("SurrogateKey", ns));
	}
	
	public Element getLevelKeyElement(Namespace ns)
	{
		Element e = new Element("LevelKey",ns);
		
		Element child = new Element("SurrogateKey",ns);
		child.setText(String.valueOf(SurrogateKey));
		e.addContent(child);
		
		child = new Element("ColumnNames",ns);
		if (ColumnNames!=null && ColumnNames.length>0)
		{
			for (String colName : ColumnNames)
			{
				Element stringElement = new Element("string",ns);
				stringElement.setText(colName);
				child.addContent(stringElement);
			}
		}
		e.addContent(child);
		
		return e;
	}

	public boolean isPartitionIndex(Repository r)
	{
		return (r.getTimeDefinition().PartitionFacts && ColumnNames.length == 1 && ColumnNames[0].equals(r.getTimeDefinition().getPartitionIDColumnName()));
	}

	public String getCompoundKeyName(Repository r, DatabaseConnection dc)
	{
		StringBuilder sb = new StringBuilder();				
		for (String s : ColumnNames)
		{
			if (sb.length() > 0)
				sb.append('_');
			sb.append(Util.replaceWithPhysicalString(s));
		}
		sb.append("_KEY");
		if (r == null || r.getCurrentBuilderVersion() < 12)
			return dc.getHashedIdentifier(sb.toString());
		else
			return dc.getHashedIdentifier(sb.toString(), DatabaseConnection.getIdentifierLength(dc.DBType)-4);
	}
	
	public int getCompoundKeyWidth(Repository r)
	{
		int width = 0;
		for (String s : ColumnNames)
		{
			DimensionColumn dc = r.findDimensionColumn(Level.Hierarchy.DimensionName, s);
			if (dc == null)
				continue;
			if(r.getCurrentBuilderVersion() < Repository.IntToLongBuilderVersion)
			{
				width += dc.Width + 1;
			}
			else if(dc.DataType != null)
			{
				if (dc.DataType.equals("Varchar"))
					width += dc.Width + 1;
				else if (dc.DataType.equals("Integer"))
					width += 25;
				else if (dc.DataType.equals("Float"))
					width += 25;
				else if (dc.DataType.equals("Number"))
					width += 25;
				else if (dc.DataType.equals("Date"))
					width += 20;
				else if (dc.DataType.equals("DateTime"))
					width += 20;
				else
					width += dc.Width + 1;
			}
			else
			{
				width += dc.Width + 1;
			}
		}
		return width;
	}
	
	/**
	 * Is at least one of the columns in LevelKey is Varchar then only compound key should be created
	 * @param lk
	 */

	public boolean isCompoundKeyRequired(Repository r)
	{
		for (String s : ColumnNames)
		{
			DimensionColumn dc = r.findDimensionColumn(Level.Hierarchy.DimensionName, s);
			if (dc == null)
				continue;
			
			if(dc.DataType != null)
			{
				if (!dc.DataType.equals("Integer"))
				{
					return true;
				}
			}
		}
		return false;
	}
	
}
