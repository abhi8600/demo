package com.successmetricsinc.query;

import com.successmetricsinc.util.BaseException;

public class SecurityFilterException extends BaseException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public SecurityFilterException()
	{
		super();
	}
	
	public SecurityFilterException(String s)
	{
		super(s);
	}
	
	public SecurityFilterException(String s, Throwable cause)
	{
		super(s, cause);
	}
	
	public int getErrorCode()
	{
		return ERROR_SECURITY_FILTER;
	}
}
