/**
 * $Id: QueryExecuter.java,v 1.6 2010-05-12 01:07:05 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import org.apache.log4j.Logger;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.query.JRQueryExecuter;

public class QueryExecuter implements JRQueryExecuter
{
	private static Logger logger = Logger.getLogger(QueryExecuter.class);
	String query = null;

	public QueryExecuter(String query)
	{
		this.query = query;
	}

	public JRDataSource createDatasource() throws JRException
	{
		try
		{
			return (new JasperDataSourceProvider().create(query));
		} catch (Exception ex)
		{
			if (QueryCancelledException.class.isInstance(ex.getCause())
	                ||QueryCancelledException.class.isInstance(ex))
			{
				logger.debug("Query cancelled by end user (QueryExecutor::createDatasource)");
			}
			else
			{
				logger.error(ex, ex);
			}
			return (null);
		}
	}

	public void close()
	{
	}

	public boolean cancelQuery() throws JRException
	{
		return false;
	}
}
