/**
 * $Id: Position.java,v 1.3 2009-09-13 20:01:47 bpeters Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.query.DisplayExpression.PositionType;

/**
 * Information related to a positional reference (example M{$ Sales}[Region=South]
 * 
 * @author Brad Peters
 * 
 */
public class Position
{
	public PositionType ptype;
	public String pDim;
	public String pCol;
	public Object pVal;
	public Tree dimensionExpression;
	public String[] dimensionExpressionLevels;
}