/**
 * 
 */
package com.successmetricsinc.query;

import java.io.Serializable;

/**
 * @author bpeters
 * 
 */
public class TableSourceFilter implements Cloneable, Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String Filter;
    public boolean CurrentLoadFilter;
    public boolean SecurityFilter;
    public String[] FilterGroups;
    public String LogicalColumnName;
                  
	public Object clone() throws CloneNotSupportedException
	{
		TableSourceFilter tsf = (TableSourceFilter) super.clone();
		tsf.Filter = this.Filter;
		tsf.CurrentLoadFilter = CurrentLoadFilter;
		tsf.SecurityFilter = SecurityFilter;
		if(FilterGroups != null)
		{
			tsf.FilterGroups = new String[FilterGroups.length];
			for (int i=0; i < FilterGroups.length; i++)
			{
				tsf.FilterGroups[i] = FilterGroups[i];
			}
		}
		tsf.LogicalColumnName = LogicalColumnName;
		return (tsf);
	}

	public String toString()
	{
		return (Filter);
	}
}
