/**
 * $Id: ColumnSubstitution.java,v 1.11 2012-11-12 18:25:19 BIRST\ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.io.Serializable;
import java.util.List;
import java.util.regex.Pattern;
import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.util.Util;

/**
 * Class to maintain and process substitutions in inherited tables
 * 
 * @author bpeters
 * 
 */
public class ColumnSubstitution implements Cloneable, Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String Original;
    public String New;
    private boolean originalIsSimpleColumn;

	public Object clone() throws CloneNotSupportedException
	{
		ColumnSubstitution cs = (ColumnSubstitution) super.clone();
        cs.New = New;
        cs.Original = Original;
        return (cs);
    }

    public static ColumnSubstitution[] getColumnSubstitutions(Element e, Namespace ns)
    {
        ColumnSubstitution[] cslist = null;
        if (e!=null)
        {
            @SuppressWarnings("rawtypes")
			List l = e.getChildren();
            cslist = new ColumnSubstitution[l.size()];
            for (int count = 0; count<l.size(); count++)
            {
                cslist[count] = new ColumnSubstitution();
                cslist[count].Original = Util.intern(((Element) l.get(count)).getChildText("Original", ns));
                cslist[count].New = Util.intern(((Element) l.get(count)).getChildText("New", ns));
                if (Pattern.matches("[a-zA-Z_0-9{}$]+[.][a-zA-Z_0-9{}$]+", cslist[count].Original))
                    cslist[count].originalIsSimpleColumn = true;
            }
        }
        return (cslist);
    }

    public static String makeSubstitutions(String formula, ColumnSubstitution[] list)
    {
        if (list==null)
            return formula;
        for (ColumnSubstitution cs : list)
        {
            formula = Util.replaceStr(formula, cs.Original, cs.New);
        }
        return (formula);
    }

    public static String makeJoinSubstitutions(String logicalTableName, TableSource ts, String condition,
            ColumnSubstitution[] list)
    {
        if (list == null)
            return condition;
        
    	StringBuilder base = new StringBuilder();
    	base.append('\"');
    	base.append(logicalTableName);
    	base.append("\".");
    	
        for (ColumnSubstitution cs : list)
        {
            /*
             * Replace fully qualified join conditions
             */
        	StringBuilder orig = new StringBuilder(base);
        	orig.append(cs.Original);
        	StringBuilder new2 = new StringBuilder(base);
        	new2.append(cs.New);
            if (condition != null) {
				condition = Util.replaceStr(condition, orig.toString(),new2.toString());
			}
			/*
             * If the old condition is simply a column qualified by the primary physical table source, then replace it too
             */
            if (cs.originalIsSimpleColumn)
            {
                int i = cs.Original.indexOf('.');
                if (cs.Original.substring(0, i).equals(ts.Tables[0].PhysicalName))
                {
                    String s = cs.Original.substring(i+1);
                	StringBuilder orig2 = new StringBuilder(base);
                	orig2.append(s);
                	if (condition != null)
                	{
                		condition = Util.replaceStr(condition, orig2.toString(), new2.toString());
                	}
                }
            }
        }
        return condition;
    }
    
    public Element getColumnSubstitutionElement(Namespace ns)
    {
    	Element e = new Element("ColumnSubstitution", ns);
    	Element child = new Element("Original", ns);
    	child.setText(Original);
    	e.addContent(child);
    	child = new Element("New", ns);
    	child.setText(New);
    	e.addContent(child);
    	return e;
    }
}
