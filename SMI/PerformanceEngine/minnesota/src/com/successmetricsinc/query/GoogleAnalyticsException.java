/**
 * $Id: GoogleAnalyticsException.java,v 1.1 2010-01-12 22:58:03 bpeters Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import com.successmetricsinc.util.BaseException;

/**
 * @author bpeters
 *
 */
public class GoogleAnalyticsException extends BaseException
{
    private static final long serialVersionUID = 1L;

	public GoogleAnalyticsException(String s, Throwable cause)
	{
		super(s, cause);
	}

    public GoogleAnalyticsException(String s)
    {
        super(s);
    }

	@Override
	public int getErrorCode() {
		return ERROR_GOOGLE_ANALYTICS;
	}
}
