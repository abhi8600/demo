package com.successmetricsinc.query;

public interface IJasperDataSourceProvider {

	/**
	 * @return Returns the jdsp.
	 */
	public abstract JasperDataSourceProvider getJasperDataSourceProvider();

}