/**
 * $Id: RowMapCache.java,v 1.21 2011-09-27 00:26:17 ricks Exp $
 *
 * Copyright (C) 2007-2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.successmetricsinc.util.Util;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
/**
 * @author Brad Peters
 * 
 */
public class RowMapCache implements Serializable
{
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(RowMapCache.class);
	private String path;
	private transient Map<RowMapCacheEntry, Map<String, Map<String, Integer>>> mapsMemoryCache;
	private String repositoryId = null;
	
	public RowMapCache(){
	}

	/**
	 * Load a new row map cache (or create if doesn't exist)
	 * 
	 * @param filename
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public RowMapCache(String cacheDir, String filename, ResultSetCache cache, boolean create)
	{
		repositoryId = cache.getRepositoryId();
		path = cacheDir;
		mapsMemoryCache = new HashMap<RowMapCacheEntry, Map<String, Map<String, Integer>>>(1000);
	}

	public void setRepositoryId(String repositoryId){
		this.repositoryId = repositoryId;
	}
	
	/**
	 * Get a row map associated with a query result set and a map key
	 * 
	 * @param compositeKey - made up of query key + result set cache file name
	 * @param mapKey
	 * @return
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public synchronized Map<String, Integer> getRowMap(ResultSetCache rsc, String compositeKey, String mapKey) throws IOException, ClassNotFoundException
	{
		RowMapCacheEntry entry = getMongoRowMapCacheEntry(compositeKey);
		if(entry == null){
			return null;
		}
		Map<String, Map<String, Integer>> maps = mapsMemoryCache.get(entry);
		if (maps == null)
		{
			maps = entry.getMaps(path);
			mapsMemoryCache.put(entry, maps);
		}
		return maps.get(mapKey);
	}
	
	private void saveMongoRowMapCacheEntry(String compositeKey, RowMapCacheEntry entry){
		MongoMgr.putObjectByKey(repositoryId, ResultSetCache.COLL_ROWMAP_CACHE, compositeKey, entry.toBasicDBObject());
	}

	private RowMapCacheEntry getMongoRowMapCacheEntry(String compositeKey){
		BasicDBObject obj =  MongoMgr.getObjectByKey(repositoryId, ResultSetCache.COLL_ROWMAP_CACHE, compositeKey);
		RowMapCacheEntry entry = new RowMapCacheEntry();
		entry.fromBasicDBObject((BasicDBObject)obj.get("key"));
		return entry;
	}
	
	/**
	 * Add a row map to the cache
	 * 
	 * @param compositeKey - made up of query key + result set cache file name
	 * @param maps
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */	
	public synchronized void addRowMap(ResultSetCache rsc, String compositeKey, String mapKey, Map<String, Integer> map) throws IOException, ClassNotFoundException
	{
		RowMapCacheEntry entry = getMongoRowMapCacheEntry(compositeKey);
		if (entry == null)
		{
			entry = new RowMapCacheEntry(compositeKey, path);
			Map<String, Map<String, Integer>> maps = new HashMap<String, Map<String, Integer>>();
			maps.put(mapKey, map);
			entry.saveMaps(maps);
			saveMongoRowMapCacheEntry(compositeKey, entry);
		} else
		{
			Map<String, Map<String, Integer>> maps = mapsMemoryCache.get(entry);
			if (maps == null)
				maps = entry.getMaps(path);
			maps.put(mapKey, map);
			entry.saveMaps(maps);
		}
	}

	public String toPrettyString()
	{
		try
		{
			DBCursor cursor = MongoMgr.find(repositoryId, ResultSetCache.COLL_ROWMAP_CACHE);
			
			StringBuilder sb = new StringBuilder();
			while(cursor.hasNext()){
				BasicDBObject obj = (BasicDBObject)cursor.next();
				String key = obj.getString("key");
				
				RowMapCacheEntry rce = new RowMapCacheEntry();
				rce.fromBasicDBObject((BasicDBObject)obj.get("value"));
				sb.append("Key=");
				sb.append(key);
				sb.append(Util.LINE_SEPARATOR);
				sb.append("Filename=");
				sb.append(rce.getFileName());
				sb.append(Util.LINE_SEPARATOR);
			}
			return sb.toString();
			
		}
		catch (Exception ex)
		{
			logger.error(ex.getMessage(), ex);
		}
		return "";
	}
}
