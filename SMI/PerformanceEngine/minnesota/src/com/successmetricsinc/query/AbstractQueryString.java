package com.successmetricsinc.query;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.query.Database.DatabaseType;
import com.successmetricsinc.query.olap.OlapLogicalQueryString;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.RepositoryException;

/**
 * encapsulate the two different query string syntaxes
 * @author ricks
 *
 */
public class AbstractQueryString {	
	protected boolean ignoreDisplay;
	public static Pattern olapQueryPattern = Pattern.compile("\\s+FROM\\s+\\[CUBE\\]", Pattern.CASE_INSENSITIVE);
	public static AbstractQueryString getInstance(Repository r, String query)
	{
		if (query.toLowerCase().startsWith("select"))
		{
			Matcher matcher = olapQueryPattern.matcher(query);
			
			if (matcher.find())
			{
				return new OlapLogicalQueryString(r, null, query);
			}
			return new LogicalQueryString(r, null, query);
		} else
		{
			return new QueryString(r, query);
		}
	}

	public static AbstractQueryString getInstance(Repository r, String query, boolean useNewQueryLanguage)
	{
		if (useNewQueryLanguage)
		{
			return new LogicalQueryString(r, null, query);
		} else
		{
			return new QueryString(r, query);
		}
	}
	
	public List<DisplayFilter> getDisplayFilters() { return null; }
	public List<DisplayExpression> getExpressionList() { return null; }
	public List<DisplayOrder> getDisplayOrderList() { return null; }
	
	public WebServerConnectInfo getServerConnectInfo() { return null; }
	public void setServerConnectInfo(WebServerConnectInfo info) { };
	public String getServerQuery() { return null; };
	
	public Query getQuery() throws IOException, SQLException, BaseException, CloneNotSupportedException { return null; };
	
	public Query processQueryString(String query, Session session) throws IOException, SQLException, BaseException, CloneNotSupportedException { return null; };
	public Query processQueryString(String query, Session session, boolean makeNamesPhysical) throws IOException, SQLException, BaseException, CloneNotSupportedException { return null; };	
	
	public Repository getRepository() { return null; };

	/**
	 * Replace any query string instances with their physical queries. Query strings are denoted by Q{LOGICAL QUERY}
	 * syntax
	 * 
	 * @param str
	 * @param makeNamesPhysical
	 *            Flag indicating whether to adjust names of columns to make them valid physical names (replace spaces
	 *            with underscores and replace special characters)
	 * @param removeOrderBys
	 *            If the query string to be replaced has order by clauses, they will be translated to physical
	 *            "ORDER BY" SQL and sql server (and probably other databases too) doesnt support order by in
	 *            subqueries. If this flag is set to true, all order by clauses from resultant query object will be
	 *            removed before generating physical query.
	 * @return
	 * @throws DisplayExpressionException
	 * @throws SyntaxErrorException
	 * @throws SQLException
	 * @throws RepositoryException
	 * @throws ScriptException 
	 */
	public String replaceQueryString(String str, boolean makeNamesPhysical, Session session, boolean removeOrderBys, boolean stripProjectionListColumns,
			DimensionColumn dcToKeep, DatabaseConnection dbc, Map<DatabaseConnection, List<TempTable>> tempTableMap) throws BaseException, SQLException, CloneNotSupportedException, IOException
	{
		int pos = 0;
		/*
		 * Replace all repository and session variable references
		 */
		while ((pos = str.indexOf("Q{", pos)) >= 0)
		{
			// scan to find end of query string
			int epos = pos + 2;
			int nesting = 0;
			for (; epos < str.length(); epos++)
			{
				if (str.charAt(epos) == '{')
					nesting++;
				else if ((nesting == 0) && (str.charAt(epos) == '}'))
					break;
				else if (str.charAt(epos) == '}')
					nesting--;
			}
			if ((epos > pos) && (epos < str.length()))
			{
				String queryString = str.substring(pos + 2, epos);
				Query q = processQueryString(queryString, session, makeNamesPhysical);
				if (removeOrderBys)
				{
					q.removeAllOrderBys();
				}
				if (stripProjectionListColumns)
				{
					q.setDimensionColumnToKeep(dcToKeep);
				}
				if(dbc != null && (dbc.DBType == DatabaseType.Infobright || dbc.DBType == DatabaseType.MemDB))
				{
					boolean containsFilterOnMeasureColumn = q.containsFilterOnMeasureColumn();
					/*
					 * If the query contains a filter on measure column, for IB, outer join will result into
					 * multiple subqueries. Only one of the subquery that includes the measure in question 
					 * will yield a filtered resultset. So, when we merge these two resultsets using UNION 
					 * into temptable, the filter on measure column will be ineffective and the main query
					 * will return wrong results.
					 * So, just set the outer join to false which apply filter on measure column as part
					 * of physical query, returning correct results.
					 */
					if(containsFilterOnMeasureColumn)
					{
						q.setFullOuterJoin(false);
					}
				}
				q.generateQuery(session, true, true);
				Map<DatabaseConnection, List<TempTable>> qtm = q.getTempTableMap();
				if(qtm != null && tempTableMap != null && qtm != tempTableMap)
				{
					List<TempTable> qttlist = qtm.get(dbc);
					if(qttlist != null && !qttlist.isEmpty())
					{
						List<TempTable> ttlist = tempTableMap.get(dbc);
						if (ttlist == null)
						{
							ttlist = new ArrayList<TempTable>();
							tempTableMap.put(dbc, ttlist);
						}
						ttlist.addAll(qttlist);
					}
				}
				String physicalQuery = "";
				if(q.containsMultipleQueries)
				{
					physicalQuery = createTempTablesForMultipleQueries(q, dbc, tempTableMap, dcToKeep);
				}
				else
				{
					physicalQuery = q.getQuery();
				}
				str = str.substring(0, pos) + physicalQuery + str.substring(epos + 1);
				/*
				 * Check to see if the query navigated to the same object for which we are trying to replace the logical
				 * query with physical one e.g. while replacing logical part of opaque view query for a measure table A,
				 * if the inline logical query navigates to measure table A itself, we could end up in in an endless
				 * loop. We need to throw a repository exception if that is the case.
				 */
				if (physicalQuery.contains(queryString))
				{
					throw new RepositoryException("Circular reference detected while generating physical query for logical" + " query: " + queryString
							+ ". Make sure that this logical query does not navigate to the same object"
							+ " that is being evaluated. e.g. measure table opaque view query contains a logical query that"
							+ " navigates to the same measure table being evaluated.");
				}
			} else
			{
				// If didn't find the end of the string, then ignore
				pos += 2;
			}
		}
		return (str);
	}
	
	private String createTempTablesForMultipleQueries(Query q, DatabaseConnection dbc, Map<DatabaseConnection, List<TempTable>> tempTableMap, DimensionColumn dcToKeep)
	{
		StringBuilder physicalQueryBuf = new StringBuilder();

		DerivedTableQuery [] dtables = q.getDerivedTables();
		if(dtables != null && dtables.length > 0)
		{
			boolean first = true;
			for(DerivedTableQuery dqt : dtables)
			{
				List<TempTable> ttlist = tempTableMap.get(dbc);
				if (ttlist == null)
				{
					ttlist = Collections.synchronizedList(new ArrayList<TempTable>());
					tempTableMap.put(dbc, ttlist);
				}
				StringBuilder dqtquery = new StringBuilder();
				List<QueryColumn> resultProjectionList = dqt.getResultProjectionList();
				if(dcToKeep != null && resultProjectionList != null && !resultProjectionList.isEmpty())
				{
					String dname = dcToKeep.DimensionTable.DimensionName;
					String cname = dcToKeep.ColumnName;
					for(QueryColumn qc : resultProjectionList)
					{
						if(qc.type == QueryColumn.DIMENSION)
						{
							DimensionColumn dc = (DimensionColumn) qc.o;
							if (dc.matchesDimensionName(dname) && dc.ColumnName.equals(cname))
							{
								dqtquery.append("SELECT S.");
								dqtquery.append(q.columnMap.getMap(qc));
								dqtquery.append(" FROM ( ");
								dqtquery.append(dqt.getQuery());
								dqtquery.append(" ) S");
								break;
							}
						}		
					}
				}
				if(dqtquery.length() == 0)
				{
					dqtquery = dqt.getQuery();
				}
				long x = System.currentTimeMillis();				
				TempTable tt = dbc.getTempTableForQueryFilter(q.getRepository(), dcToKeep, x, ttlist, dqtquery.toString());
				if(first)
				{
					physicalQueryBuf.append(" SELECT * FROM "+ tt.tableName);
					first = false;					
				}
				else
				{
					physicalQueryBuf.append(" UNION ");
					physicalQueryBuf.append(" SELECT * FROM "+ tt.tableName);
				}
				if (!ttlist.contains(tt))
				{
					ttlist.add(tt);
				}
			}
		}
		return physicalQueryBuf.toString();
	}
	
	public void setIgnoreDisplay(boolean ignoreDisplay)
	{
		this.ignoreDisplay = ignoreDisplay;
	}

}
