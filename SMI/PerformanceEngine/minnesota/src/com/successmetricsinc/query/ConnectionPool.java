/**
 * $Id: ConnectionPool.java,v 1.68 2012-10-25 00:25:56 BIRST\ricks Exp $
 *
 * Copyright (C) 2007-2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.OptionalDataException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.TimeZone;

import org.apache.log4j.Logger;
import org.olap4j.OlapConnection;
import org.olap4j.OlapWrapper;

import com.successmetricsinc.Session;
import com.successmetricsinc.query.Database.DatabaseType;
import com.successmetricsinc.security.EncryptionService;
import com.successmetricsinc.util.Util;

/**
 * Class to implement connection pooling
 * 
 * @author bpeters
 * 
 */
public class ConnectionPool implements Externalizable
{
	private static Logger logger = Logger.getLogger(ConnectionPool.class);
	private Map<Thread, Connection> poolForThreads;
	private Map<Session, Connection> poolForSessions;
	private List<Connection> parallelPool;
	private List<Connection> parallelPoolInUse;
	private DatabaseConnection dbc;
	private String connectString;
	private String username;
	private String password;
	private int size;
	private String driverName;
	private String pingQuery = "select 1";
	private boolean isDynamicConnnection = false;
	// pool per unique connection string, across the server - that way, if multiple users have the same DB instance,
	// they can share a pool
	private static Map<String, ConnectionPool> connectionPoolCache = new HashMap<String, ConnectionPool>();
	private Map<Connection, List<Statement>> statementsByConnection = new HashMap<Connection, List<Statement>> ();

	public static ConnectionPool getInstance(DatabaseConnection dbc, String name, String driverName, String connectStr, String username, String password,
			int size, TimeZone processingTimeZone) throws Exception
	{
		boolean isDynamicConnnection = false;
		if (DatabaseConnection.isDynamicConnection(connectStr))
		{
			isDynamicConnnection = true;
		}
		if (!isDynamicConnnection)
		{
			// if infobright, need to add the database to the connection string
			if (dbc.DBType == DatabaseType.Infobright || dbc.DBType == DatabaseType.MYSQL)
			{
				String schema = dbc.Schema;
				boolean isFirst = true;
				if (schema != null && !schema.isEmpty())
				{
					if(!connectStr.contains(schema))
					{
						// if it ends in '/', assume we need to add the schema
						if (!connectStr.endsWith("/"))
						{
							connectStr += "/";
						}
						connectStr = connectStr + schema + "?zeroDateTimeBehavior=convertToNull";
						isFirst = false;
					}
				}
				connectStr = connectStr + (isFirst ? "?" : "&") +"useUnicode=true&characterEncoding=utf8&useTimezone=true&serverTimezone=" + processingTimeZone.getID();
			}
			else if (DatabaseConnection.isDBTypeXMLA(dbc.DBType))
			{
				if (dbc.ConnectString.indexOf(";Catalog=") < 0)//in case of generic database connection, catalog is expected to be part of connectstring
				{
					String serverUrl = dbc.ConnectString.substring(0, dbc.ConnectString.lastIndexOf("://"));
					String catalog = dbc.ConnectString.substring(dbc.ConnectString.lastIndexOf("://") + "://".length());
					connectStr = "jdbc:xmla:Server=" + serverUrl;
					if (catalog != null && !catalog.isEmpty())
		                connectStr += ";Catalog=" + catalog;
				}
				if (dbc.getDBTypeNameForXMLAConnection().equals(com.birst.dataconductor.DatabaseConnection.HYPERIAN_ESSBASE))
	                connectStr += ";Provider=Essbase";
	            if (dbc.getDBTypeNameForXMLAConnection().equals(com.birst.dataconductor.DatabaseConnection.SAP_BW))
	                connectStr += ";Provider=SAP_BW";	            
			}
		}
		String key = connectStr; // should be sufficient for a key
		if(password != null){
			key = key + ";Pwd=" + EncryptionService.getMD5(password, false);
		}
		
		ConnectionPool connectionPool = connectionPoolCache.get(key);
		if (connectionPool == null)
		{
			connectionPool = new ConnectionPool(dbc, name, driverName, connectStr, username, password, size);
			connectionPoolCache.put(key, connectionPool);
		} else
		{
			logger.debug("Using existing database connection pool using connection string " + connectStr);
		}
		if(dbc.DBType == DatabaseType.Oracle)
		{
			connectionPool.pingQuery = "select 1 from dual";
		}
		if(dbc.DBType == DatabaseType.Hana)
		{
			connectionPool.pingQuery = "select 1 from dummy";
		}
		connectionPool.isDynamicConnnection = isDynamicConnnection;
		return connectionPool;
	}
	
	public ConnectionPool()
	{
	}
	
	public boolean isDynamicConnnection()
	{
		return isDynamicConnnection;
	}

	/**
	 * Create a connection pool
	 * 
	 * @param driverName
	 *            Name of driver
	 * @param connectStr
	 *            Connect string to use
	 * @param username
	 *            Username
	 * @param password
	 *            Password
	 * @param size
	 *            Size of pool
	 * @throws Exception
	 */
	private ConnectionPool(DatabaseConnection dbc, String name, String driverName, String connectStr, String username, String password, int size)
			throws Exception
	{
		logger.debug("Database connection pool created for " + name + " (" + dbc.Type + ") using connection string " + connectStr + " and driver " + driverName);
		if (driverName.equalsIgnoreCase("none") || driverName.isEmpty())
			return;
		poolForThreads = new HashMap<Thread, Connection>();
		poolForSessions = new HashMap<Session, Connection>();
		parallelPool = new ArrayList<Connection>();
		parallelPoolInUse = new ArrayList<Connection>();
		try
		{
			Class.forName(driverName);
		}
		catch (ClassNotFoundException ex)
		{
			logger.fatal("Could not load JDBC driver: " + driverName);
			throw new ClassNotFoundException(driverName);
		}
		connectString = connectStr;
		this.dbc = dbc;
		this.username = username;
		this.password = password;
		this.size = size;
		this.driverName = driverName;
		
		Connection conn = null;
		// get version information for logging purposes
		try
		{
			// note that this is only for version information, no need to add any of the properties that we add for real
			// connection
			String url = connectString;
			Properties p = new Properties();
			p.setProperty("user", username);
			if (password != null && !password.isEmpty())
				p.setProperty("password", EncryptionService.getInstance().decrypt(password));
			if (dbc.DBType == Database.DatabaseType.Redshift) // force Redshift connections to use SSL and keepalive
			{
				// http://docs.aws.amazon.com/redshift/latest/mgmt/connecting-ssl-support.html
				p.setProperty("ssl", "true");
				p.setProperty("sslfactory", "org.postgresql.ssl.NonValidatingFactory");
				p.setProperty("tcpKeepAlive", "true");
			}			
			conn = DriverManager.getConnection(url, p);
			DatabaseMetaData dmd = conn.getMetaData();
			try
			{
				logger.info("JDBC Driver information: " + dmd.getDriverName() + ", " + dmd.getDriverVersion() + ", JDBC Version: " + dmd.getJDBCMajorVersion()
						+ "." + dmd.getJDBCMinorVersion());
			}
			catch (Exception e)
			{
				logger.error("failure to get JDBC information - " + e.getMessage());
			}
			try
			{
				logger.info("Database information: " + dmd.getDatabaseProductName() + ", " + dmd.getDatabaseProductVersion() + ", " + dmd.getDatabaseMajorVersion() + "." + dmd.getDatabaseMinorVersion());
			}
			catch (Exception e)
			{
				logger.error("failure to get Database information - " + e.getMessage());
			}
			// get the server uptime
			if (DatabaseConnection.isDBTypeMySQL(dbc.DBType))
			{
				Statement stmt = null;
				ResultSet rs = null;
				try
				{
					stmt = conn.createStatement();
					rs = stmt.executeQuery("SHOW GLOBAL STATUS LIKE 'Uptime';");
					while (rs.next())
					{
						logger.debug(rs.getString(1) + ": " + rs.getString(2) + " seconds");
					}
				} catch (Exception e)
				{
					logger.error("failure to get server uptime - " + e.getMessage());
				}
				finally
				{
					if (rs != null)
						rs.close();
					if (stmt != null)
						stmt.close();
				}
			}
		} catch (Exception e)
		{
			logger.error("failure to get information about the database and driver - " + e.getMessage());
		}
		finally
		{
			if (conn != null)
			{
				try
				{
					conn.close();
				}
				catch (Exception e)
				{}
			}	
		}
	}
	
	private Connection getDynamicConnectionFromDatabase(Session session, boolean setDBSessionTimeouts) throws SQLException
	{
		Connection conn;
		EncryptionService enc = EncryptionService.getInstance();
		String url = session.getRepository().replaceVariables(session, connectString);
		String uname = session.getRepository().replaceVariables(session, username);
		String passwd = enc.encrypt(session.getRepository().replaceVariables(session, enc.decrypt(password)));
		Properties p = new Properties();
		p.setProperty("user", uname);
		if (passwd != null && !passwd.isEmpty())
			p.setProperty("password", enc.decrypt(passwd));
		if (dbc.DBType == Database.DatabaseType.Redshift) // force Redshift connections to use SSL and keepalive
		{
			// http://docs.aws.amazon.com/redshift/latest/mgmt/connecting-ssl-support.html
			p.setProperty("ssl", "true");
			p.setProperty("sslfactory", "org.postgresql.ssl.NonValidatingFactory");
			p.setProperty("tcpKeepAlive", "true");
		}
		try
		{
			if (DatabaseConnection.isDBTypeXMLA(dbc.DBType))
			{
				String xmlaType = dbc.getDBTypeNameForXMLAConnection();
				if (xmlaType != null && xmlaType.equals(com.birst.dataconductor.DatabaseConnection.HYPERIAN_ESSBASE))
					url += ";Provider=Essbase";
				else if (xmlaType != null && xmlaType.equals(com.birst.dataconductor.DatabaseConnection.SAP_BW))
					url += ";Provider=SAP_BW";
			}
			conn = DriverManager.getConnection(url, p);
			if (DatabaseConnection.isDBTypeXMLA(dbc.DBType))
			{
				OlapWrapper wrapper = (OlapWrapper) conn;
                conn = wrapper.unwrap(OlapConnection.class);
                if (url.indexOf("Roles=") >= 0)
                {
                    int startIndex = url.indexOf("Roles=") + "Roles=".length() ;
                    int endIndex = url.indexOf(";", startIndex);
                    String roles = url.substring(startIndex, (endIndex == -1 ? url.length(): endIndex));
                    ((OlapConnection)conn).setRoleName(roles);                    
                }
                if (url.indexOf("ExtendedConnectionProperties=\"") >= 0)
                {
                    int startIndex = url.indexOf("ExtendedConnectionProperties=\"") + "ExtendedConnectionProperties=\"".length();
                    int endIndex = url.indexOf("\"", startIndex);
                    String extendedProperties = url.substring(startIndex, (endIndex == -1 ? url.length(): endIndex));
                    if (extendedProperties != null && extendedProperties.trim().length() > 0)
                    {
                        Map<String, String> extendedPropertiesMap = new HashMap<>();
                        String[] properties = extendedProperties.trim().split(";");
                        if (properties != null)
                        {
                            for (String prop : properties)
                            {
                                String[] property = prop.split("=");
                                if (property != null && property.length == 2)
                                {
                                    extendedPropertiesMap.put(property[0], property[1]);
                                }
                            }
                        }
                        ((OlapConnection)conn).setExtendedPropertiesMap(extendedPropertiesMap);
                    }                                        
                }
			}
		} catch (SQLException sqe)
		{
			logger.error(sqe.getMessage());
			// try it a second time... really should be a loop that repeats and sleeps
			conn = DriverManager.getConnection(url, p);
			if (DatabaseConnection.isDBTypeXMLA(dbc.DBType))
			{
				OlapWrapper wrapper = (OlapWrapper) conn;
                conn = wrapper.unwrap(OlapConnection.class);
			}
		}
		
		if(setDBSessionTimeouts)
		{
			setWaitTimeoutOnDB(dbc, conn);
		}
		return conn;
	}
	
	public void addStatementForConnection(Connection conn, Statement stmt)
	{
		if((dbc.DBType != DatabaseType.MYSQL) || (!isDynamicConnnection))
		{
			return;
		}
		List<Statement> stmtList = statementsByConnection.get(conn);
		if(stmtList == null)
		{
			stmtList = new ArrayList<Statement>();
			statementsByConnection.put(conn, stmtList);
		}
		stmtList.add(stmt);
		logger.debug("Added statement for connection: " + connectString +". New size: " + stmtList.size());
	}

	public void cancelStatementsForConnection(Connection conn)
	{
		if((dbc.DBType != DatabaseType.MYSQL) || (!isDynamicConnnection))
		{
			return;
		}
		List<Statement> stmtList = statementsByConnection.get(conn);
		if(stmtList != null && stmtList.size() > 0)
		{
			for(Statement stmt : stmtList)
			{
				if(stmt != null)
				{
					try
					{
						stmt.cancel();
					}
					catch (SQLException sqle)
					{
						logger.error("Error trying to cancel statement: " + sqle.toString());
					}
					finally
					{
						try
						{
							stmt.close();
						}
						catch (SQLException sqle)
						{
							logger.error("Error trying to close statement: " + sqle.toString());
						}
					}
				}
				logger.debug("Cancelled and closed statement for connection: " + connectString);
			}
			stmtList.clear();
			statementsByConnection.remove(conn);
			logger.debug("Cleared statement map for connection: " + connectString);
		}
	}
	
	private Connection getConnectionFromDatabase() throws SQLException
	{
		return getConnectionFromDatabase(null);
	}

	public Connection getConnectionFromDatabase(Properties p) throws SQLException
	{
		Connection conn;
		String url = connectString;
		if (p == null)
		{
			p = new Properties();
		}
		p.setProperty("user", username);
		if (password != null && !password.isEmpty())
			p.setProperty("password", EncryptionService.getInstance().decrypt(password));
		if ((dbc.DBType == Database.DatabaseType.Infobright) || (dbc.DBType == Database.DatabaseType.MYSQL))
		{
			p.setProperty("jdbcCompliantTruncation", "false"); // needed for MySQL/Infobright on LOAD DATA INFILE
			p.setProperty("zeroDateTimeBehavior", "convertToNull");
		}
		
		if (dbc.DBType == Database.DatabaseType.Redshift) // force Redshift connections to use SSL and keepalive
		{
			// http://docs.aws.amazon.com/redshift/latest/mgmt/connecting-ssl-support.html
			p.setProperty("ssl", "true");
			p.setProperty("sslfactory", "org.postgresql.ssl.NonValidatingFactory");
			p.setProperty("tcpKeepAlive", "true");
		}
		try
		{
			if (DatabaseConnection.isDBTypeXMLA(dbc.DBType))
			{
				String connStringUrl = url;
				if (connStringUrl.indexOf("user=") < 0 && connStringUrl.indexOf("password=") < 0 && !username.isEmpty() && !password.isEmpty())
				{
					connStringUrl = url + ";user=" + username + ";password=" + EncryptionService.getInstance().decrypt(password);
				}
				conn = DriverManager.getConnection(connStringUrl);
				OlapWrapper wrapper = (OlapWrapper) conn;
                conn = wrapper.unwrap(OlapConnection.class);
                if (url.indexOf("Roles=") >= 0)
                {
                    int startIndex = url.indexOf("Roles=") + "Roles=".length() ;
                    int endIndex = url.indexOf(";", startIndex);
                    String roles = url.substring(startIndex, (endIndex == -1 ? url.length(): endIndex));
                    ((OlapConnection)conn).setRoleName(roles);                    
                }
                if (url.indexOf("ExtendedConnectionProperties=\"") >= 0)
                {
                    int startIndex = url.indexOf("ExtendedConnectionProperties=\"") + "ExtendedConnectionProperties=\"".length();
                    int endIndex = url.indexOf("\"", startIndex);
                    String extendedProperties = url.substring(startIndex, (endIndex == -1 ? url.length(): endIndex));
                    if (extendedProperties != null && extendedProperties.trim().length() > 0)
                    {
                        Map<String, String> extendedPropertiesMap = new HashMap<>();
                        String[] properties = extendedProperties.trim().split(";");
                        if (properties != null)
                        {
                            for (String prop : properties)
                            {
                                String[] property = prop.split("=");
                                if (property != null && property.length == 2)
                                {
                                    extendedPropertiesMap.put(property[0], property[1]);
                                }
                            }
                        }
                        ((OlapConnection)conn).setExtendedPropertiesMap(extendedPropertiesMap);
                    }                                        
                }
			}
			else
			{
				conn = DriverManager.getConnection(url, p);
			}
		} catch (SQLException sqe)
		{
			logger.error(sqe.getMessage());
			// try it a second time... really should be a loop that repeats and sleeps
			conn = DriverManager.getConnection(url, p);
			if (DatabaseConnection.isDBTypeXMLA(dbc.DBType))
			{
				OlapWrapper wrapper = (OlapWrapper) conn;
                conn = wrapper.unwrap(OlapConnection.class);
			}
		}
		setWaitTimeoutOnDB(dbc, conn);
		return conn;
	}	
	
	private void setWaitTimeoutOnDB(DatabaseConnection dbc, Connection conn) 
	{
		if(conn == null){
			return;
		}
		
		if(DatabaseConnection.isDBTypeMySQL(dbc.DBType))
		{
			int dbWaitTimeout = Util.getDBWaitTimeout();
			if(dbWaitTimeout > 0)
			{
				Statement stmt = null;
				try
				{				
					stmt = conn.createStatement();
					stmt.execute("set session wait_timeout=" + dbWaitTimeout);				 
				}
				catch(SQLException ex){
					logger.error("Exception while setting wait time out ", ex);
				}
				finally
				{
					if(stmt != null)
					{
						try
						{
							stmt.close();
						}
						catch(SQLException ex){
							logger.warn("Error in stmt.close()", ex);
						}
					}
				}
			}
		}
	}
	
	public void closeAllConnections()
	{
		logger.debug("closing all connections for the connection pool");
		for (Entry<Thread, Connection> entry : poolForThreads.entrySet())
		{
			try
			{
				Connection conn = entry.getValue();
				if (!conn.isClosed())
					conn.close();
			}
			catch (Exception ex)
			{
			}
		}
		
		for (Entry<Session, Connection> entry : poolForSessions.entrySet())
		{
			try
			{
				Connection conn = entry.getValue();
				if (!conn.isClosed())
					conn.close();
			}
			catch (Exception ex)
			{
			}
		}
		
		for (Connection c : parallelPool)
		{
			try
			{
				if (!c.isClosed())				
					c.close();
			}
			catch (Exception ex)
			{
			}
		}
		
		for (Connection c : parallelPoolInUse)
		{
			try
			{
				if (!c.isClosed())				
					c.close();
			}
			catch (Exception ex)
			{
			}
		}
	}

	/**
	 * clean up connections associated with dead threads
	 */
	private void reapConnections()
	{
		try
		{
			Set<Thread> remove = new HashSet<Thread>();
			for (Entry<Thread, Connection> entry : poolForThreads.entrySet())
			{
				if (!entry.getKey().isAlive()) // if the thread is not alive, try to reuse the connection
				{
					try
					{
						entry.getValue().close();
					} catch (Exception ex)
					{
						logger.error(ex.getMessage(), ex);
					}
					logger.debug("Reaping connection associated with dead thread: " + entry.getKey().toString());
					remove.add(entry.getKey());							
				}
			}
			for (Thread th : remove)
			{
				poolForThreads.remove(th);
			}
		} catch (Exception ex)
		{
			logger.error(ex, ex);
		}
		return;
	}
	
	private void reapDynamicConnections()
	{
		try
		{
			Set<Session> remove = new HashSet<Session>();
			for (Entry<Session, Connection> entry : poolForSessions.entrySet())
			{
				if (entry.getKey().isCancelled()) // if the session is not alive, try to reuse the connection
				{
					try
					{
						Connection conn = entry.getValue(); 
						cancelStatementsForConnection(conn);
						conn.close();
					} catch (Exception ex)
					{
						logger.error(ex.getMessage(), ex);
					}
					logger.debug("Reaping connection associated with dead session: " + entry.getKey().getUserName());
					remove.add(entry.getKey());							
				}
			}
			for (Session ss : remove)
			{
				poolForSessions.remove(ss);
			}
		} catch (Exception ex)
		{
			logger.error(ex, ex);
		}
	}
	
	private void reapParallelConnections()
	{
		try
		{
			List <Connection> removeMe = new ArrayList<Connection>();
			for (Connection c : parallelPool)
			{
				try
				{
					if (!isValidConnection(c, this.pingQuery))
					{
						removeMe.add(c);
					}
				} catch (Exception ex)
				{
					logger.error(ex, ex);
				}
			}
			// remove the bad connections
			for (Connection c : removeMe)
			{
				parallelPool.remove(c);
			}
		} catch (Exception ex)
		{
			logger.error(ex, ex);
		}
	}

	/**
	 * Get a connection from the pool. Fill the pool as new connections are needed. Round-robin if pool is full.
	 * 
	 * @return
	 * @throws Exception
	 */
	public synchronized Connection getConnection() throws SQLException
	{
		if (!isDynamicConnnection)
		{
			// use a thread-based connection pool
			if (poolForThreads == null)
				return null;
		
			Connection conn = poolForThreads.get(Thread.currentThread());
			if (!isValidConnection(conn, this.pingQuery))
			{
				logger.debug("Connection is closed or not valid for thread (" + Thread.currentThread().toString() + ")");
				poolForThreads.remove(Thread.currentThread());
				reapConnections();
				conn = getConnectionFromDatabase();
				poolForThreads.put(Thread.currentThread(), conn);
			}
			logger.trace("Connections in the thread pool - " + poolForThreads.size());				
			return conn;
		}
		else
		{
			return null;
		}
	}
	
	public Connection getDynamicConnection(Session session) throws SQLException
	{
		if (!isDynamicConnnection || session == null)
			return null;
		// use a session-based connection pool
		if (poolForSessions == null)
			return null;
		Connection conn = poolForSessions.get(session);
		if (!isValidConnection(conn, this.pingQuery))
		{
			logger.debug("Connection is closed or not valid for session of user (" + session.getUserName() + ")");
			poolForSessions.remove(session);
			reapDynamicConnections();
			conn = getDynamicConnectionFromDatabase(session, true);
			poolForSessions.put(session, conn);
		}
		logger.trace("Connections in the session pool - " + poolForSessions.size());
		return conn;		
	}
	
	/**
	 * Get a connection from the pool. Fill the pool as new connections are needed. Round-robin if pool is full.
	 * 
	 * @return
	 * @throws Exception
	 */
	private synchronized Connection getParallelConnection() throws SQLException
	{
		// use a thread-based connection pool
		if (parallelPool == null)
			return null;
		Connection conn = null;
		while (!parallelPool.isEmpty() && conn == null)
		{
			conn = parallelPool.get(0);
			parallelPool.remove(0);
			if (!isValidConnection(conn, this.pingQuery))
			{
				logger.debug("Connection is closed or not valid in parallel pool");
				conn = null;
			}
		}
		if (conn == null)
		{
			reapParallelConnections(); // clean up any dead connections
			conn = getConnectionFromDatabase();			
		}
		parallelPoolInUse.add(conn);
		logger.debug("Connections in the parallel pool - " + parallelPoolInUse.size() + '/' + parallelPool.size());		
		return conn;
	}
	
	public static Connection getParallelOrDynamicConnection(DatabaseConnection dc, Session session, boolean usesParallelPool) throws SQLException
	{
		if (dc.ConnectionPool.isDynamicConnnection() && session != null)
		{
			return dc.ConnectionPool.getNewDynamicConnection(session);
		}
		else if (usesParallelPool)
		{
			return dc.ConnectionPool.getParallelConnection();
		}
		else
		{
			return dc.ConnectionPool.getConnection();
		}
	}
	
	public static synchronized void returnToParallelPoolOrCloseDynamicConnection(Connection conn, DatabaseConnection dc, Session session, 
			boolean usesParallelPool)
	{
		if (dc.ConnectionPool.isDynamicConnnection() && session != null)
		{
			try
			{
				if (conn != null)
				{
					dc.ConnectionPool.cancelStatementsForConnection(conn);
					conn.close();
				}
			}
			catch (SQLException e)
			{	
				logger.warn("Unable to close dynamic connection for user:" + session.getUserName());
			}
		}
		else if (usesParallelPool)
		{
			dc.ConnectionPool.parallelPoolInUse.remove(conn);
			try
			{
				if (!conn.isClosed() && isValidConnection(conn, dc.ConnectionPool.pingQuery))
				{
					dc.ConnectionPool.parallelPool.add(conn);
				}
			} catch (SQLException e)
			{
				logger.warn("Unable to return connection to parallel pool");
			}
		}
		else
		{
			try
			{
				if (conn != null)
					conn.close();
			}
			catch (SQLException e)
			{	
				logger.warn("Unable to close connection:" + e.getMessage());
			}
		}
	}

	public Connection getNewConnection() throws SQLException
	{
		Connection conn = getConnectionFromDatabase();
		logger.debug("Connection directly allocated - app must free");
		return conn;
	}
	
	public Connection getNewDynamicConnection(Session session) throws SQLException
	{
		Connection conn = getDynamicConnectionFromDatabase(session, false);
		logger.debug("Dynamic Connection directly allocated for user:" + session.getUserName() + " - app must free");
		return conn;
	}

	private static boolean isValidConnection(Connection connection, String pq)
	{
		if (connection == null)
			return false;
		try
		{
			if (connection.isClosed())
				return false;
		}
		catch (Exception ex)
		{
			logger.debug(ex, ex);
		}
		Statement stmt = null;
		ResultSet rs = null;
		long startTime = System.currentTimeMillis();
		try
		{
			if (connection instanceof OlapConnection)
			{
				boolean valid = connection.isValid(120);
				if (!valid)
				{
					logger.warn("Ping query to DB failed");
					try
					{
						connection.close();
					}
					catch (Exception ex)
					{
						logger.debug(ex, ex);
					}
					return false;
				}
			}
			else
			{
				stmt = connection.createStatement();
				rs = stmt.executeQuery(pq);
			}
		}
		catch (SQLException e)
		{
			logger.warn("Ping query to DB failed - " + e.getMessage());
			try
			{
				connection.close();
			}
			catch (Exception ex)
			{
				logger.debug(ex, ex);
			}
			return false;
		}
		finally
		{
			try
			{
				if (rs != null)
					rs.close();
				if (stmt != null)
					stmt.close();
			}
			catch (SQLException e)
			{
				logger.debug(e, e);
				return false;
			}
		}		
		long endTime = System.currentTimeMillis();
		long delta = endTime - startTime;
		if (delta > 100)
		{
			logger.warn("Ping to database took " + delta + " milliseconds.");
		} else if (delta > 50)
		{
			if (logger.isTraceEnabled())
				logger.trace("Ping to database took " + delta + " milliseconds.");
		}
		return true;
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException
	{
		connectString = (String) in.readObject();
		username = (String) in.readObject();
		password = (String) in.readObject();
		size = (int) in.readInt();
		driverName = (String) in.readObject();
		dbc = (DatabaseConnection) in.readObject();
		logger.debug("Database connection pool created using connection string " + connectString + " and driver " + driverName);
		if (driverName.equalsIgnoreCase("none") || driverName.isEmpty())
			return;
		poolForThreads = new HashMap<Thread, Connection>();
		poolForSessions = new HashMap<Session, Connection>();
		parallelPool = new ArrayList<Connection>();
		parallelPoolInUse = new ArrayList<Connection>();
		try
		{
			isDynamicConnnection = (Boolean) in.readObject();
		} catch (OptionalDataException e)
		{
			if (!e.eof)
				throw e;
		}
		Class.forName(driverName);
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException
	{
		out.writeObject(connectString);
		out.writeObject(username);
		out.writeObject(password);
		out.writeInt(size);
		out.writeObject(driverName);
		out.writeObject(dbc);
		out.writeObject(isDynamicConnnection);
	}
}
