/**
 * $Id: TableSource.java,v 1.41 2012-11-12 18:25:20 BIRST\ricks Exp $
 *
 * Copyright (C) 2007-2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jdom.Element;
import org.jdom.Namespace;
import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.query.Database.DatabaseType;
import com.successmetricsinc.util.RepositoryException;
import com.successmetricsinc.util.Util;

public class TableSource implements Cloneable, Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public TableDefinition[] Tables;
	public String ConnectionName;
	public String Schema;
	public DatabaseConnection connection;
	public String[] contentFilterNames;
	public List<QueryFilter> contentFilters;
	public TableSourceFilter[] Filters;
	private boolean isContentFiltersTag;

	public TableSource()
	{
	}

	/**
	 * Instantiate a table source based on a DOM element
	 * 
	 * @param e
	 *            Element
	 * @param ns
	 *            Namespace
	 * @param requireTables
	 *            Whether tables need to be defined (in the case of opaque views they do not need to be defined)
	 * @throws RepositoryException
	 */
	public TableSource(Element e, Namespace ns, boolean requireTables) throws RepositoryException
	{
		if (e != null)
		{
			this.ConnectionName = Util.intern(e.getChildText("Connection", ns));
			this.Schema = Util.intern(e.getChildText("Schema", ns));
			@SuppressWarnings("rawtypes")
			List l = e.getChildren("Tables", ns);
			if (l.isEmpty() && requireTables)
				throw (new RepositoryException("No tables defined for table source"));
			else
			{
				if (!l.isEmpty())
				{
					l = ((Element) l.get(0)).getChildren();
					if (l.isEmpty() && requireTables)
						throw (new RepositoryException("No tables defined for table source"));
					Tables = new TableDefinition[l.size()];
					for (int count = 0; count < l.size(); count++)
					{
						Tables[count] = new TableDefinition();
						Tables[count].Schema = Util.intern(((Element) l.get(count)).getChildText("Schema", ns));				
						Tables[count].PhysicalName = Util.intern(((Element) l.get(count)).getChildText("PhysicalName", ns));
						String typestr = ((Element) l.get(count)).getChildText("JoinType", ns);
						Tables[count].JoinType = typestr == null ? JoinType.Inner : Join.getJoinType(Integer.parseInt(typestr));
						Tables[count].JoinClause = ((Element) l.get(count)).getChildText("JoinClause", ns);
						Tables[count].isJoinClauseTag = ((Element) l.get(count)).getChild("JoinClause",ns)!=null?true:false;
						Tables[count].strJoinClause = ((Element) l.get(count)).getChildText("JoinClause", ns);
						if (Tables[count].JoinClause != null)
						{
							Tables[count].JoinClause = Util.replaceStr(Tables[count].JoinClause, "  ", " ");
							Tables[count].JoinClause = Util.replaceStr(Tables[count].JoinClause, " = ", "=");
							Tables[count].JoinClause = Util.replaceStr(Tables[count].JoinClause, " > ", ">");
							Tables[count].JoinClause = Util.replaceStr(Tables[count].JoinClause, " < ", "<");
							Tables[count].JoinClause = Util.replaceStr(Tables[count].JoinClause, " <> ", "<>");
							Tables[count].JoinClause = Util.replaceStr(Tables[count].JoinClause, "  ", " ");
						}
					}
				} else
				{
					Tables = new TableDefinition[0];
				}
			}
			this.contentFilterNames = Repository.getStringArrayChildren(e, "ContentFilters", ns);
			isContentFiltersTag = e.getChild("ContentFilters",ns)!=null?true:false;
			l = e.getChildren("Filters", ns);
			if (l.size() > 0)
			{
				l = ((Element) l.get(0)).getChildren();
				Filters = new TableSourceFilter[l.size()];
				for (int count = 0; count < l.size(); count++)
				{
					Filters[count] = new TableSourceFilter();
					Filters[count].Filter = ((Element) l.get(count)).getChildText("Filter", ns);				
					String clfilter = ((Element) l.get(count)).getChildText("CurrentLoadFilter", ns);
					if (clfilter != null)
						Filters[count].CurrentLoadFilter = Boolean.valueOf(clfilter);
					clfilter = ((Element) l.get(count)).getChildText("SecurityFilter", ns);
					if (clfilter != null)
						Filters[count].SecurityFilter = Boolean.valueOf(clfilter);
					@SuppressWarnings("rawtypes")
					List filterGroupsList = ((Element)l.get(count)).getChildren("FilterGroups", ns);
					if(filterGroupsList != null && filterGroupsList.size() > 0)
					{
						@SuppressWarnings("rawtypes")
						List filterGroupNamesList = ((Element) filterGroupsList.get(0)).getChildren();
						if(filterGroupNamesList!= null && filterGroupNamesList.size() > 0 )
						{
							String[] result = new String[filterGroupNamesList.size()];
							for (int i = 0; i < filterGroupNamesList.size(); i++)
							{
								result[i] = ((Element) filterGroupNamesList.get(i)).getTextTrim();
							}
							Filters[count].FilterGroups = result;
						}
						
						
					}
					Filters[count].LogicalColumnName = ((Element) l.get(count)).getChildText("LogicalColumnName", ns);
				}
			}
		} else
			throw (new RepositoryException("No tables defined for table source"));
	}
	
	public Element getTableSourceElement(Namespace ns)
	{
		Element e = new Element ("TableSource", ns);
		Element child = new Element("Tables", ns);
		if (Tables != null)
		{
			for (TableDefinition td : Tables) {
				Element tabDef = new Element("TableDefinition", ns);
				Element temp;
				temp = new Element("PhysicalName", ns);
				temp.setText(td.PhysicalName);
				tabDef.addContent(temp);				
				int joinType = td.JoinType == JoinType.Inner ? 0 : td.JoinType == JoinType.LeftOuter ? 1 : td.JoinType == JoinType.RightOuter ? 2 : td.JoinType == JoinType.FullOuter ? 3 : 0;
				if (joinType!=0)
				{
					temp = new Element("JoinType", ns);
					temp.setText(td.JoinType == JoinType.Inner ? String.valueOf(0) : td.JoinType == JoinType.LeftOuter ? String.valueOf(1) : td.JoinType == JoinType.RightOuter ? String.valueOf(2) : td.JoinType == JoinType.FullOuter ? String.valueOf(3) : "");
					tabDef.addContent(temp);
				}				
				if (td.JoinClause!=null && !td.JoinClause.isEmpty())
				{
					temp = new Element("JoinClause", ns);
					temp.setText(td.strJoinClause);
					tabDef.addContent(temp);
				}
				else if (td.isJoinClauseTag)
				{
					temp = new Element("JoinClause", ns);
					tabDef.addContent(temp);
				}
				child.addContent(tabDef);
			}
		}
		e.addContent(child);
		if (this.ConnectionName!=null && !this.ConnectionName.isEmpty())
		{
			child = new Element("Connection", ns);
			child.setText(this.ConnectionName);
			e.addContent(child);
		}
		if (this.Schema!=null && !this.Schema.isEmpty())
		{
			child = new Element("Schema", ns);
			child.setText(this.Schema);
			e.addContent(child);
		}
		
		if (contentFilterNames != null && contentFilterNames.length>0)
		{
			child = new Element("ContentFilters", ns);
			boolean firsttime = true;
			StringBuilder values = new StringBuilder();
			for (String value : contentFilterNames) {
				if(!firsttime)
				{
					values.append(',');
				}
				firsttime = false;
				values.append(value);
			}
			child.setText(values.toString());
			e.addContent(child);
		}
		else if (isContentFiltersTag)
		{
			child = new Element("ContentFilters", ns);
			e.addContent(child);
		}
		if (Filters != null)
		{
			child = new Element("Filters", ns);			
				for (TableSourceFilter filter : Filters) {
					Element temp;
					temp = new Element("TableSourceFilter",ns);
					Element elemFilter = new Element("Filter", ns);
					elemFilter.setText(filter.Filter);
					temp.addContent(elemFilter);
					if (filter.CurrentLoadFilter)
					{
						Element currLoadFilter = new Element("CurrentLoadFilter",ns);
						currLoadFilter.setText(String.valueOf(filter.CurrentLoadFilter));
						temp.addContent(currLoadFilter);
					}
					if (filter.SecurityFilter)
					{
						Element secFilter = new Element("SecurityFilter",ns);
						secFilter.setText(String.valueOf(filter.SecurityFilter));
						temp.addContent(secFilter);
					}					
					if (filter.FilterGroups!=null && filter.FilterGroups.length>0)
					{
						Element filterGroups = Repository.getStringArrayElement("FilterGroups", filter.FilterGroups, ns);
						temp.addContent(filterGroups);
					}
					if (filter.LogicalColumnName!=null && !filter.LogicalColumnName.isEmpty())
					{
						elemFilter = new Element("LogicalColumnName", ns);
						elemFilter.setText(filter.LogicalColumnName);
						temp.addContent(elemFilter);
					}
					child.addContent(temp);
				}
			
			e.addContent(child);
		}
		return e;
	}

	/**
	 * Set the content filter structures
	 * 
	 * @param filterList
	 */
	public void setFilters(List<QueryFilter> filterList)
	{
		if (contentFilterNames.length == 0)
			return;
		this.contentFilters = new ArrayList<QueryFilter>(contentFilterNames.length);
		for (int i = 0; i < contentFilterNames.length; i++)
		{
			for (QueryFilter qf : filterList)
			{
				if (qf.getName().equals(contentFilterNames[i]))
				{
					this.contentFilters.add(qf);
					break;
				}
			}
		}
		if (this.contentFilters.size() > 1)
		{
			List<QueryFilter> newList = new ArrayList<QueryFilter>();
			newList.add(new QueryFilter("AND", this.contentFilters));
			this.contentFilters = newList;
		}
	}

	public Object clone() throws CloneNotSupportedException
	{
		TableSource ts = (TableSource) super.clone();
		ts.Tables = new TableDefinition[Tables.length];
		for (int i = 0; i < Tables.length; i++)
		{
			ts.Tables[i] = (TableDefinition) Tables[i].clone();
		}
		ts.Schema = this.Schema;
		ts.connection = this.connection;
		ts.ConnectionName = this.ConnectionName;
		ts.Filters = Filters;
		return (ts);
	}

	/**
	 * Return the query string fragment that specifies a logical table source
	 * 
	 * @param c
	 *            Database connection being used
	 * @param tableMap
	 *            Table map to use
	 * @param pruneSource
	 *            TableSource used for pruning - if a physical table is used in this table source, use the table
	 *            definition from this source rather than creating a new one
	 * @return
	 */
	public String getTableString(DatabaseConnection c, QueryMap tableMap, TableSource pruneSource, JoinResult jr, Repository r, Session session)
	{
		StringBuilder result = new StringBuilder();
		boolean first = true;
		String startCondition = jr == null ? null : jr.joinCondition;
		Map<TableDefinition, String> replacedPhysicalNameMap = new HashMap<TableDefinition, String>();
		if (r.joinContainsVariable)
		{
			for (int i = 0; i < Tables.length; i++)
			{
				String replacedPhysicalName = r.replaceVariables(session, Tables[i].PhysicalName);
				if (r.containsVariable(Tables[i].PhysicalName) && !Tables[i].PhysicalName.equals(replacedPhysicalName))
				{
					replacedPhysicalNameMap.put(Tables[i], replacedPhysicalName);
				}
			}
		}
		for (int i = 0; i < Tables.length; i++)
		{
			/*
			 * See if this table source should be pruned
			 */
			boolean pruned = false;
			if (pruneSource != null && jr != null)
			{
				for (TableDefinition td : pruneSource.Tables)
				{
					if (td.PhysicalName.equals(Tables[i].PhysicalName))
					{
						/*
						 * Compare and only prune if clauses are the same
						 */
						String newClause = td.JoinClause;
						if (newClause != null)
						{
							for (TableDefinition rtd : Tables)
							{
								if (rtd.PhysicalName.equals(td.PhysicalName))
									newClause = Util.replaceStr(newClause, rtd.PhysicalName + '.', tableMap.getMap(rtd) + '.');
							}
							for (TableDefinition rtd : pruneSource.Tables)
							{
								if (!rtd.PhysicalName.equals(td.PhysicalName))
									newClause = Util.replaceStr(newClause, rtd.PhysicalName + '.', tableMap.getMap(rtd) + '.');
							}
						}
						if ((newClause == null && startCondition == null) || (newClause != null && newClause.equals(startCondition)))
						{
							pruned = true;
							String replaceInJoin = tableMap.getMap(Tables[i]);
							if (replaceInJoin != null)
								jr.joinCondition = Util.replaceStr(jr.joinCondition, tableMap.getMap(Tables[i]), tableMap.getMap(td));
							tableMap.setMap(Tables[i], tableMap.getMap(td));
							/*
							 * If there is a join condition for this part of the table source, tack on the the
							 * consolidated join
							 */
							String joinstr = Tables[i].JoinClause;
							if ((joinstr != null) && (joinstr.length() > 0))
							{
								for (int j = 0; j < Tables.length; j++)
								{
									joinstr = Util.replaceStr(joinstr, Tables[j].PhysicalName + ".", tableMap.getMap(Tables[j]) + ".");
								}
								jr.joinCondition = jr.joinCondition + " AND (" + joinstr + ")";
							}
							break;
						}
					}
				}
			}
			if (!pruned && Tables[i].Valid)
			{
				if (!first)
				{
					result.append(' ');
					result.append(Join.getJoinTypeStr(Tables[i].JoinType, c));
					result.append(' ');
				}
				StringBuilder name = new StringBuilder();
				if (replacedPhysicalNameMap.containsKey(Tables[i]))
				{
					name.append(replacedPhysicalNameMap.get(Tables[i]));
				}
				else
				{
					name.append(Tables[i].PhysicalName);
				}
				if (c.DBType != DatabaseType.ODBCExcel && (name.indexOf(" ") != -1 ||  
					((c.DBType == DatabaseType.PostgreSQL || c.DBType == DatabaseType.Hana) && c.isRealTimeConnection())))
				{
					if (DatabaseConnection.isDBTypeMySQL(c.DBType))
					{
						name=name.toString().startsWith("`")?name:name.insert(0, '`');
						name=name.toString().endsWith("`")?name:name.append('`');
					}
					else
					{
						name=name.toString().startsWith("\"")?name:name.insert(0, '"');
						name=name.toString().endsWith("\"")?name:name.append('"');
					}
				}
				if (Tables[i].Schema != null && Tables[i].Schema.length() > 0)
					name.insert(0, Tables[i].Schema + ".");
				else if (Schema != null && Schema.length() > 0)
					name.insert(0, Schema + ".");
				else if (c.Schema != null && c.Schema.length() > 0)
					name.insert(0, c.Schema + ".");
				if (c.DBType == DatabaseType.ODBCExcel)
				{
					name.insert(0, '[');
					name.append(']');
				}
				if (Tables[i].OpaqueView != null)
				{
					result.append('(');
					result.append(Tables[i].OpaqueView);
					result.append(") ");
					result.append(tableMap.getMap(Tables[i]));
				} else
				{
					result.append(name);
					result.append(' ');
					if (replacedPhysicalNameMap.containsKey(Tables[i]))
					{
						
						result.append(tableMap.getMap(replacedPhysicalNameMap.get(Tables[i])));
					}
					else
					{
						result.append(tableMap.getMap(Tables[i]));
					}
				}
				if (!first)
				{
					String joinstr = Tables[i].JoinClause;
					if ((joinstr != null) && (joinstr.length() > 0))
					{
						boolean joinClauseContainsVariable = false;
						if (r.joinContainsVariable && Repository.containsVariable(joinstr))
						{
							String replacedJoinStr = r.replaceVariables(session, joinstr);
							if (!replacedJoinStr.equals(joinstr))
							{
								joinClauseContainsVariable = true;
								joinstr = replacedJoinStr;
							}
						}
						if (joinClauseContainsVariable)
						{
							for (TableDefinition jtd : Tables)
							{
								if (jtd.TableName != null)
								{
									if (replacedPhysicalNameMap.containsKey(jtd))
									{
										joinstr = joinstr.replace("\"" + jtd.TableName + "\"." + replacedPhysicalNameMap.get(jtd) + ".", replacedPhysicalNameMap.get(jtd) + ".");
									}
									else
									{
										joinstr = joinstr.replace("\"" + jtd.TableName + "\"." + jtd.PhysicalName + ".", jtd.PhysicalName + ".");
									}
								}
							}							
						}
						if (replacedPhysicalNameMap.containsKey(Tables[i]))
						{
							joinstr = Util.replaceStr(joinstr, replacedPhysicalNameMap.get(Tables[i]) + ".", tableMap.getMap(replacedPhysicalNameMap.get(Tables[i])) + ".");
						}
						else
						{
							if(Util.startsWithAnAlias(joinstr,Tables[i].PhysicalName + "."))
							{
								joinstr = Util.replaceFirstStr(joinstr, Tables[i].PhysicalName + ".", tableMap.getMap(Tables[i]) + ".");
							}							
							joinstr = Util.replaceStr(joinstr, " "+Tables[i].PhysicalName + ".", " " + tableMap.getMap(Tables[i]) + ".");
							joinstr = Util.replaceStr(joinstr, "="+Tables[i].PhysicalName + ".", "=" + tableMap.getMap(Tables[i]) + ".");
						}
						if (joinClauseContainsVariable && Tables[i].TableName != null)
						{
							joinstr = Util.replaceStr(joinstr, "\"" + Tables[i].TableName + "\".", tableMap.getMap(Tables[i]) + ".");
						}
						for (int j = 0; j < Tables.length; j++)
						{
							if (j == i)
								continue;
							if (replacedPhysicalNameMap.containsKey(Tables[j]))
							{
								joinstr = Util.replaceStr(joinstr, replacedPhysicalNameMap.get(Tables[j]) + ".", tableMap.getMap(replacedPhysicalNameMap.get(Tables[j])) + ".");
							}
							else
							{
								joinstr = Util.replaceStr(joinstr, Tables[j].PhysicalName + ".", tableMap.getMap(Tables[j]) + ".");
							}
							if (joinClauseContainsVariable && Tables[j].TableName != null)
							{
								joinstr = Util.replaceStr(joinstr, "\"" + Tables[j].TableName + "\".", tableMap.getMap(Tables[j]) + ".");
							}
						}
						result.append(" ON ");
						result.append(joinstr);
					} else if (DatabaseConnection.requiresRedundantJoinCondition(c.DBType))
					{
						result.append(" ON 1=1");
					}
				}
				first = false;
			}
		}
		return (Util.replaceStr(result.toString(), "V{SCHEMA}", Schema));
	}

	public String getTableString(DatabaseConnection c, QueryMap tableMap, TableSource pruneSource, Repository r, Session session)
	{
		return (getTableString(c, tableMap, pruneSource, null, r, session));
	}
}