/**
 * $Id: VirtualColumn.java,v 1.9 2011-09-24 01:05:37 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.io.Serializable;
import java.util.List;
import org.jdom.Element;
import org.jdom.Namespace;
import com.successmetricsinc.Repository;
import com.successmetricsinc.engine.MeasureBucket;
import com.successmetricsinc.util.XmlUtils;

public class VirtualColumn implements Serializable
{
	private static final long serialVersionUID = 1L;
	public final static int PIVOTED = 2;
	public final static int BUCKETED = 3;
	public String Name;
	public String TableName;
	public String Measure;
	public String MeasurePrefix;
	public String[] Filters;
	public int Type;
	public String PivotDimension;
	public String PivotLevel;
	public boolean PivotPercent;
	public MeasureBucket[] Buckets;
	public String nullBucketName;
	public String defaultBucketName;
	public String[] PivotColumns;	
	
	private String Guid;
	private String CreatedDate;
	private String strLastModifiedDate;
	private String CreatedUsername;
	private String LastModifiedUsername;

	public VirtualColumn()
	{
	}

	public VirtualColumn(Element vce, Namespace ns)
	{
		Guid = vce.getChildText("Guid",ns);
		CreatedDate = vce.getChildText("CreatedDate",ns);
		strLastModifiedDate = vce.getChildText("LastModifiedDate",ns);
		CreatedUsername = vce.getChildText("CreatedUsername",ns);
		LastModifiedUsername = vce.getChildText("LastModifiedUsername",ns);
		Name = vce.getChildTextTrim("Name", ns);
		Measure = vce.getChildTextTrim("Measure", ns);
		MeasurePrefix = vce.getChildText("MeasurePrefix", ns);
		TableName = vce.getChildTextTrim("TableName", ns);
		Filters = Repository.getStringArrayChildren(vce, "Filters", ns);
		PivotDimension = vce.getChildTextTrim("PivotDimension", ns);
		PivotLevel = vce.getChildTextTrim("PivotLevel", ns);
		PivotPercent = Boolean.parseBoolean(vce.getChildTextTrim("PivotPercent", ns));
		Type = Integer.parseInt(vce.getChildTextTrim("Type", ns));
		Element be = vce.getChild("Buckets", ns);
		if (be != null)
		{
			@SuppressWarnings("rawtypes")
			List l2 = be.getChildren();
			Buckets = new MeasureBucket[l2.size()];
			for (int count2 = 0; count2 < l2.size(); count2++)
			{
				Buckets[count2] = new MeasureBucket((Element) l2.get(count2), ns);
			}
		}
		nullBucketName = vce.getChildTextTrim("measureBucketNullName", ns);
		defaultBucketName = vce.getChildTextTrim("measureBucketDefaultName", ns);
	}
	
	public Element getVirtualColumnElement(Namespace ns)
	{
		Element e = new Element("VirtualColumn",ns);
		
		Repository.addBaseAuditObjectAttributes(e, Guid, CreatedDate, strLastModifiedDate, CreatedUsername, LastModifiedUsername, ns);
		
		Element child = new Element("Name",ns);
		child.setText(Name);
		e.addContent(child);
		
		XmlUtils.addContent(e, "TableName", TableName,ns);	
		
		if (MeasurePrefix!=null)
		{
			child = new Element("MeasurePrefix",ns);
			child.setText(MeasurePrefix);
			e.addContent(child);
		}
		
		child = new Element("Measure",ns);
		child.setText(Measure);
		e.addContent(child);
		
		if (Filters!=null && Filters.length>0)
		{
			e.addContent(Repository.getStringArrayElement("Filters", Filters, ns));
		}
		else
		{
			child = new Element("Filters",ns);
			e.addContent(child);
		}
		
		child = new Element("Type",ns);
		child.setText(String.valueOf(Type));
		e.addContent(child);
		
		XmlUtils.addContent(e, "PivotDimension", PivotDimension,ns);
		
		XmlUtils.addContent(e, "PivotLevel", PivotLevel,ns);				
		
		child = new Element("PivotPercent",ns);
		child.setText(String.valueOf(PivotPercent));
		e.addContent(child);
		
		if (Buckets!=null && Buckets.length>0)
		{
			child = new Element("Buckets",ns);
			for (MeasureBucket mb : Buckets)
			{
				child.addContent(mb.getMeasureBucketElement(ns));
			}
			e.addContent(child);
		}		
		
		if (defaultBucketName!=null)
		{
		  XmlUtils.addContent(e, "measureBucketDefaultName", defaultBucketName,ns);
		}
		
		if (nullBucketName!=null)
		{
		  XmlUtils.addContent(e, "measureBucketNullName", nullBucketName,ns);
		}
		
		return e;
	}

	public String toString()
	{
		return this.Name;
	}
}