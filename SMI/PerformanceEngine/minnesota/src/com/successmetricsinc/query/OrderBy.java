/**
 * $Id: OrderBy.java,v 1.40 2012-04-19 12:44:52 rchandarana Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.io.Externalizable;


import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.mongodb.BasicDBObject;
import com.successmetricsinc.Repository;
import com.successmetricsinc.query.Database.DatabaseType;

/**
 * Class to hold/generate an order by clause
 * 
 * @author bpeters
 * 
 */
public class OrderBy implements Externalizable, BasicDBObjectSerializer
{
	private static final long serialVersionUID = 1L;
	public static final int TYPE_DIMENSION_COLUMN = 0;
	public static final int TYPE_MEASURE = 1;
	public static final int TYPE_CONSTANT_OR_FORMULA = 2;
	public static final int TYPE_DIMENSION_MEMBER_SET = 4;
	private int type;
	private String dimName;
	private String colName;
	private String alias;
	private String constant;
	private boolean ascending;
	private DimensionColumn dc;
	private MeasureColumn mc;

	/**
	 * Constructor used for serialization
	 */
	public OrderBy()
	{
	}

	/**
	 * Create an order by for a given dimension column
	 * 
	 * @param dimName
	 * @param colName
	 * @param ascending
	 */
	public OrderBy(String dimName, String colName, boolean ascending)
	{
		this.dimName = dimName;
		this.colName = colName;
		this.ascending = ascending;
		this.type = TYPE_DIMENSION_COLUMN;
	}
	
	public OrderBy(String dimName, String colName, String alias, boolean ascending)
	{
		this.dimName = dimName;
		this.colName = colName;
		this.alias = alias;
		this.ascending = ascending;
		this.type = TYPE_DIMENSION_COLUMN;
	}
	/**
	 * Create an order by for a given measure
	 * 
	 * @param measure
	 * @param ascending
	 */
	public OrderBy(String displayName, boolean ascending, String calculatedMemberName)
	{
		this.colName = displayName;
		this.ascending = ascending;
		this.dimName = calculatedMemberName;
		this.type = TYPE_DIMENSION_MEMBER_SET;
	}

	/**
	 * Create an order by for a given measure
	 * 
	 * @param measure
	 * @param ascending
	 */
	public OrderBy(String measure, boolean ascending)
	{
		this.colName = measure;
		this.ascending = ascending;
		this.type = TYPE_MEASURE;
	}

	/**
	 * Create an order by for a constant/expression
	 * 
	 * @param constant
	 */
	public OrderBy(String constant)
	{
		this.constant = constant;
		this.type = TYPE_CONSTANT_OR_FORMULA;
	}
	
	public boolean getDirection()
	{
		return this.ascending;
	}

	/**
	 * Set columns based on navigation list
	 * 
	 * @param queryColumnList
	 */
	public void setNavigation(List<QueryColumn> queryColumnList, Query q, Repository r)
	{
		/*
		 * See if this is a derived measure first - if so, won't be in the queryColumnList
		 */
		if (type == TYPE_MEASURE)
		{
			MeasureColumnNav mcn = q.findDerivedMeasureColumnNav(colName);
			if (mcn != null)
			{
				this.mc = r.findMeasureColumn(colName);
				return;
			}
		}
		/*
		 * Normal Column
		 */
		for (QueryColumn qc : queryColumnList)
		{
			if ((type == TYPE_DIMENSION_COLUMN) && (qc.type == QueryColumn.DIMENSION))
			{
				DimensionColumn dc = (DimensionColumn) qc.o;
				if (dc.matchesDimensionName(dimName) && dc.ColumnName.equals(colName))
				{
					// Either the currently set dimension is null or an inherited dimension, set the matching one to
					// current otherwise leave it alone
					if ((this.dc == null)
							|| ((this.dc != null) && ((this.dc.DimensionTable.InheritTable != null) || (this.dc.DimensionTable.VirtualMeasuresInheritTable != null))))
					{
						this.dc = dc;
						break;
					}
				}
			} else if ((type == TYPE_MEASURE) && (qc.type == QueryColumn.MEASURE))
			{
				MeasureColumn mc = (MeasureColumn) qc.o;
				if (mc.ColumnName.equals(colName))
				{
					this.mc = mc;
					break;
				}
			}
		}
	}

	/**
	 * Return query string for order by
	 * 
	 * @param q
	 * @param mt
	 * @param melist
	 * @return
	 * @throws SyntaxErrorException 
	 */
	public String getOrderByString(DatabaseConnection dbc, boolean aggregate, Query q, MeasureTable mt, List<String> melist, QueryMap tableMap, List<String> lookupJoins) throws SyntaxErrorException
	{
		String asc = " " + (ascending ? "ASC" : "DESC");
		if (type == TYPE_DIMENSION_COLUMN)
		{
			return (q.getDimensionColumnString(dbc, dc, q.getVcDimensionTables(), tableMap, lookupJoins) + asc);
		} else if ((type == TYPE_MEASURE) && (mc.MeasureTable == mt))
		{
			return (q.getMeasureString(dbc, aggregate, mc, melist, null, null, tableMap, lookupJoins, null) + asc);
		} else if (type == TYPE_CONSTANT_OR_FORMULA)
		{
			return (constant + asc);
		}
		return (null);
	}

	/**
	 * Return query string for order by for wrapped queries where projection name is already known
	 * 
	 * @param q
	 * @param mt
	 * @param melist
	 * @return
	 */
	public String getOrderByString(QueryColumn qc, String pname)
	{
		String asc = " " + (ascending ? "ASC" : "DESC");
		if (type == TYPE_DIMENSION_COLUMN && dc == qc.o)
		{
			return (pname + asc);
		} else if ((type == TYPE_MEASURE) && (mc == qc.o))
		{
			return (pname + asc);
		} else if (type == TYPE_CONSTANT_OR_FORMULA && constant == qc.o)
		{
			return (pname + asc);
		}
		return (null);
	}
	
	/**
	 * Return query string for order by for Google Analytics
	 * 
	 * @param q
	 * @param mt
	 * @param melist
	 * @return
	 */
	public String getGAOrderByString()
	{
		String asc = ascending ? "" : "-";
		if (type == TYPE_DIMENSION_COLUMN)
			return asc + dc.PhysicalName;
		else if (type == TYPE_MEASURE)
			return asc + mc.PhysicalName;
		return (null);
	}

	/**
	 * Return QueryString format for order by
	 * 
	 * @return
	 */
	public String getClassicOrderByQueryString()
	{
		String asc = " " + (ascending ? "ASC" : "DESC");
		if (type == TYPE_DIMENSION_COLUMN)
		{
			return ("ODC{" + dimName + "." + colName + "," + asc + "}");
		} else if (type == TYPE_MEASURE)
		{
			return ("OM{" + colName + "," + asc + "}");
		} else if (type == TYPE_CONSTANT_OR_FORMULA)
		{
			return ("OC{" + constant + "," + asc + "}");
		}
		return (null);
	}

	/**
	 * Return QueryString format for order by
	 * 
	 * @return
	 */
	public String getNewOrderByQueryString()
	{
		String asc = " " + (ascending ? "ASC" : "DESC");
		if (type == TYPE_DIMENSION_COLUMN)
		{
			if (alias != null)
				return ("[" + alias + "] " + asc);
			return ("[" + dimName + "." + colName + "] " + asc);
		} else if (type == TYPE_MEASURE || type == TYPE_DIMENSION_MEMBER_SET)
		{
			return ("[" + colName + "] " + asc);
		}
		return (null);
	}

	/**
	 * Return the order by clause for the outer query
	 * 
	 * @param q
	 *            Query
	 * @param qc
	 *            Query column to match - if this clause does not represent this query column, then return null
	 * @param index
	 *            Index of subquery to refer to
	 * @return
	 */
	public String getOuterQueryClause(DatabaseConnection dbc, Query q, QueryColumn qc, int index, List<Navigation> navList, int qindex, boolean fullOuterJoin)
	{
		String asc = " " + (ascending ? "ASC" : "DESC");
		String item = getOuterItem(dbc, q, qc, index, navList, qindex, fullOuterJoin);
		return item == null ? null : item + asc;
	}

	public String getOuterItem(DatabaseConnection dbc, Query q, QueryColumn qc, int index, List<Navigation> navList, int qindex, boolean fullOuterJoin)
	{
		if (type == TYPE_DIMENSION_COLUMN)
		{
			if (qc.o == dc)
			{
				String result = q.getProjectionListItem(qc, index, null, navList, qindex, false).toString();
				return (result);
			} else
				return (null);
		} else if (type == TYPE_MEASURE)
		{
			if (qc.o == mc)
			{
				if (mc.MeasureTable.Type == MeasureTable.DERIVED) // Derived
				{
                    return dbc.getColumnName(qc.name);
				} else
				{
					if (fullOuterJoin)
						return "SUM(" + q.getDerivedQueryTableName(index) + "." + q.columnMap.getMap(qc) + ")";
					else
						return q.getDerivedQueryTableName(index) + "." + q.columnMap.getMap(qc);
				}
			} else
				return (null);
		} else
			return (constant);
	}

	/**
	 * Return a unique string key to represent this clause in the overall query key for cache keying
	 * 
	 * @param q
	 * @return
	 */
	public String getQueryKeyStr(Query q)
	{
		if (type == TYPE_DIMENSION_COLUMN)
		{
			StringBuilder sb = new StringBuilder("OB{D{");
			sb.append(dimName);
			sb.append("}C{");
			sb.append(colName);
			sb.append("}A{");
			sb.append(ascending);
			sb.append("}}");
			return sb.toString();
		} else if (type == TYPE_MEASURE || type == TYPE_DIMENSION_MEMBER_SET)
		{
			StringBuilder sb = new StringBuilder("OB{M{");
			sb.append(colName);
			sb.append("}A{");
			sb.append(ascending);
			sb.append("}}");
			return sb.toString();
		} else if (type == TYPE_CONSTANT_OR_FORMULA)
		{
			StringBuilder sb = new StringBuilder("OB{C{");
			sb.append(constant);
			sb.append("}A{");
			sb.append(ascending);
			sb.append("}}");
			return sb.toString();
		}
		return (null);
	}

	/**
	 * Extract columns from order by for later navigation
	 * 
	 * @param q
	 */
	public void extractNavigationColumns(Query q) throws NavigationException
	{
		if (type == TYPE_DIMENSION_COLUMN && alias == null)
		{
			DimensionColumnNav dcn = q.findDimensionColumnNav(dimName, colName);
			if (dcn == null)
			{
				dcn = q.addDimensionColumn(dimName, colName);
				dcn.navigateonly = true;
			}
			return;
		} else if (type == TYPE_MEASURE)
		{
			MeasureColumnNav mcn = q.findMeasureColumnNav(colName);
			if (mcn == null)
			{
				/*
				 * Use hidden measure not navigate-only, to ensure proper joins (ensures that measure table is added to
				 * the query) (navigate-only) should work now - need to test) mcn = q.addMeasureColumn(colName, colName,
				 * true);
				 */
				mcn = q.addMeasureColumn(colName, colName, false);
				if (mcn == null)
				{
					throw (new NavigationException("Unrecognized measure: " + colName));
				}
				mcn.navigateonly = true;
			}
		}
	}

	/**
	 * Ensure that this order by clause is included in a projection list if it is contained in a query's full column
	 * list (also, add any implicit grouping if needed). This allows for the necessary inner clause to be added to an
	 * inner query if the outer query needs it for ordering, but the column is not included in the outer query
	 * projection list.
	 * 
	 * @param plist
	 *            Current projection list
	 * @param qclist
	 *            Query column list
	 * @param groupByClauses
	 *            List of current group by clauses
	 */
	public void ensureInProjectionList(List<QueryColumn> plist, List<QueryColumn> qclist, List<GroupBy> groupByClauses, List<GroupBy> addedGroupBys)
	{
		if (qclist == null)
			return;
		boolean found = false;
		for (QueryColumn qc : plist)
		{
			if (type == TYPE_DIMENSION_COLUMN)
			{
				if (qc.o == dc)
				{
					found = true;
					break;
				}
			} else if (type == TYPE_MEASURE)
			{
				if (qc.o == mc)
				{
					found = true;
					break;
				}
			}
		}
		// If not found in the projection list, get the right query column from the full query column list and add it to
		// the projection list
		if (!found)
		{
			for (QueryColumn qc : qclist)
			{
				if (type == TYPE_DIMENSION_COLUMN)
				{
					if (qc.o == dc)
					{
						plist.add(qc);
						GroupBy.ensureInList(groupByClauses, qc, addedGroupBys);
						break;
					}
				} else if (type == TYPE_MEASURE)
				{
					if (qc.o == mc)
					{
						plist.add(qc);
						break;
					}
				}
			}
		}
	}

	/**
	 * Return whether two Order By clauses are the same
	 * 
	 * @param ob
	 * @return
	 */
	public boolean equals(Object o)
	{
		if (this == o)
			return true;
		if (!(o instanceof OrderBy))
			return false;
		OrderBy ob = (OrderBy) o;
		if (ascending != ob.ascending)
			return false;
		if (type != ob.type)
			return false;
		if (type == TYPE_CONSTANT_OR_FORMULA)
		{
			return (ob.constant.equals(this.constant));
		} else if (type == TYPE_DIMENSION_COLUMN)
		{
			return (ob.colName.equals(this.colName) && ob.dimName.equals(this.dimName));
		} else if (type == TYPE_MEASURE)
		{
			return (ob.colName.equals(this.colName));
		}
		return false;
	}

	public int hashCode()
	{
		assert false : "hashCode not designed";
		return 42; // any arbitrary constant will do
	}

	public void writeExternal(ObjectOutput oo) throws IOException
	{
		oo.writeObject(type);
		oo.writeObject(dimName);
		oo.writeObject(colName);
		oo.writeObject(constant);
		oo.writeObject(ascending);
	}

	public void readExternal(ObjectInput oi) throws IOException, ClassNotFoundException
	{
		type = (Integer) oi.readObject();
		dimName = (String) oi.readObject();
		colName = (String) oi.readObject();
		constant = (String) oi.readObject();
		ascending = (Boolean) oi.readObject();
	}
	
	public void fromBasicDBObject(BasicDBObject obj)
	{
		type = obj.getInt("type");
		dimName = obj.getString("dimName");
		colName = obj.getString("colName");
		constant = obj.getString("constant");
		ascending = obj.getBoolean("ascending", ascending);
	}
	
	public BasicDBObject toBasicDBObject()
	{
		BasicDBObject o = new BasicDBObject();
		
		o.put("type", type);
		o.put("dimName", dimName);
		o.put("colName", colName);
		o.put("constant", constant);
		o.put("ascending", ascending);
		
		return o;
	}

	/**
	 * Returns a display order based on the current query order. Assumes display names are the same as column names in
	 * the query filter
	 */
	public DisplayOrder returnDisplayOrder()
	{
		DisplayOrder disporder = null;
		if (type == TYPE_DIMENSION_COLUMN)
			disporder = new DisplayOrder(dimName, colName, ascending);
		else
			disporder = new DisplayOrder(null, colName, ascending);
		return (disporder);
	}

	/**
	 * @return the colName
	 */
	public String getColName()
	{
		return colName;
	}

	/**
	 * @return the dimName
	 */
	public String getDimName()
	{
		return dimName;
	}

	/**
	 * @return the type
	 */
	public int getType()
	{
		return type;
	}

	/**
	 * Returns the column key string for this order by
	 * 
	 * @return
	 * @throws NavigationException
	 */
	public String getKey()
	{
		if (type == TYPE_DIMENSION_COLUMN)
		{
			return ("D{" + dimName + "}C{" + colName + "}");
		} else if (type == TYPE_MEASURE || type == TYPE_DIMENSION_MEMBER_SET)
		{
			return ("M{" + colName + "}");
		} 
		return (null);
	}

	/**
	 * Returns whether the order by matches a dimension column
	 * 
	 * @param dim
	 * @param col
	 * @return
	 */
	public boolean matchesDimensionColumn(String dim, String col)
	{
		if (type == TYPE_DIMENSION_COLUMN)
		{
			return (dimName.equals(dim) && colName.equals(col));
		} else
			return (false);
	}

	/**
	 * @param colName
	 *            the colName to set
	 */
	public void setColName(String colName)
	{
		this.colName = colName;
	}

	/**
	 * Given a list of order by clauses and a query column, see if the query column matches with any order by clauses
	 * and if so, add the supplied name to the map that is keyed by order by clause.
	 * @param orderByClauses
	 * @param qc
	 * @param name
	 * @param pnameByOrderByClause
	 */
	public static void addToProjectionNameByOrderByClauseMap(List<OrderBy> orderByClauses, QueryColumn qc, String name, Map<OrderBy, String> pnameByOrderByClause)
	{
		for (int i = 0; i < orderByClauses.size(); i++)
		{
			OrderBy ob = (OrderBy) orderByClauses.get(i);
			if (!pnameByOrderByClause.containsKey(ob))
			{
				String s = ob.getOrderByString(qc, name);
				if(s != null)
				{
					pnameByOrderByClause.put(ob, s);
				}
			}
		}
	}
	
	/**
	 * Given a map of projection names keyed by order by clauses, build an order by string to be used in physical SQL
	 * @param orderByClauses
	 * @param pnameByOrderByClause
	 * @return Order by String
	 */
	public static String buildOrderByStrFromMap(List<OrderBy> orderByClauses, Map<OrderBy, String> pnameByOrderByClause)
	{
		if(pnameByOrderByClause.isEmpty())
		{
			return "";
		}
		
		StringBuilder orderby = new StringBuilder();
		/* Make sure no duplicates - no two order bys that navigate to the same underlying column */
		Set<String> obList = new HashSet<String>();
		if (orderByClauses.size() > 0)
			orderby.append(" ORDER BY ");
		boolean first = true;
		for (int i = 0; i < orderByClauses.size(); i++)
		{
			OrderBy ob = (OrderBy) orderByClauses.get(i);
			String s = pnameByOrderByClause.get(ob);					
			if(s != null && !obList.contains(s))
			{
				if(!first)
				{
					orderby.append(',');
				}
				orderby.append(s);
				obList.add(s);
				first = false;
			}
		}
		return orderby.toString();
	}
	
	public static StringBuilder getMDXOrderByClause(DatabaseConnection dbc, StringBuilder dimensionClause, List<OrderBy> orderByClauses, Map<String, String> calculatedMemberMap, Map<String, String> calculatedMemberQualifiedNameMap)
	{
		if (dbc.requiresDispalySortingForXMLA() || orderByClauses == null || orderByClauses.size() == 0)
			return dimensionClause;
		StringBuilder orderByClause = new StringBuilder();
		boolean first = true;
		for (int i = orderByClauses.size()-1; i >=0 ; i--)
		{
			OrderBy ob = (OrderBy) orderByClauses.get(i);
			if (ob.type == TYPE_DIMENSION_MEMBER_SET)
				continue;
			String asc = ob.ascending ? "BASC" : "BDESC";
			if (first)
			{
				orderByClause.append("ORDER(").append(dimensionClause).append(", ");
				first = false;
			}
			else
			{
				orderByClause.append(", ");
				orderByClause.insert(0, "ORDER(" );
			}
			if (ob.type == TYPE_MEASURE)
			{
				MeasureColumn mc = ob.mc;
				String name = calculatedMemberMap.containsKey(mc.PhysicalName) ? calculatedMemberMap.get(mc.PhysicalName) : mc.PhysicalName;
				name = "[Measures]." + name;
				orderByClause.append(name).append(", ").append(asc).append(")");
			}
			else if (ob.type == TYPE_DIMENSION_COLUMN)
			{
				DimensionColumn dc = ob.dc;
				String columnName = null;
				if (calculatedMemberMap.containsKey(dc.PhysicalName))
				{
					columnName = calculatedMemberMap.get(dc.PhysicalName);
					if (dbc.DBType == DatabaseType.MONDRIANXMLA)
					{
						columnName = calculatedMemberQualifiedNameMap.get(columnName);
					}
				}
				else
				{
					columnName = dc.PhysicalName;
					int index = columnName.lastIndexOf('.');
					String hname = columnName.substring(0, index);
					columnName = hname + ".CurrentMember" + (dbc.DBType == DatabaseType.SAPBWXMLA ? ".Properties(\"Member_Caption\")" : ("." + DatabaseConnection.getMemberValueMDXFunction(dbc.DBType)));
				}
				orderByClause.append(columnName).append(", ").append(asc).append(")");
			}
			else if (ob.type == TYPE_DIMENSION_MEMBER_SET)
			{
				orderByClause.append("[").append(calculatedMemberMap.get(ob.getDimName())).append("], ").append(ob.ascending ? "ASC" : "DESC").append(")");
			}
			else
			{
				orderByClause.append(ob.constant).append(", ").append(asc).append(")");
			}
		}
		if (orderByClause.length() == 0)
			return dimensionClause;
		return orderByClause;
	}
	
	public void clearNavigation()
	{
		if(type == TYPE_DIMENSION_COLUMN)
		{
			this.dc = null;
		}
		else if(type == TYPE_MEASURE)
		{
			this.mc = null;
		}
	}
}
