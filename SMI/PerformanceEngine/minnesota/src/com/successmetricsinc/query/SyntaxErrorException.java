/**
 * $Id: SyntaxErrorException.java,v 1.2 2009-12-22 19:01:44 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import com.successmetricsinc.util.BaseException;

/**
 * @author bpeters
 *
 */
public class SyntaxErrorException extends BaseException
{
    private static final long serialVersionUID = 1L;

	public SyntaxErrorException(String s, Throwable cause)
	{
		super(s, cause);
	}

    public SyntaxErrorException(String s)
    {
        super(s);
    }

	@Override
	public int getErrorCode() {
		return ERROR_SYNTAX;
	}
}
