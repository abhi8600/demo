/**
 * $Id: QueryFilter.java,v 1.282 2012-12-04 12:51:01 BIRST\mpandit Exp $
 *
 * Copyright (C) 2007-2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.regex.Pattern;

import javax.xml.stream.XMLStreamReader;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.Tree;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jdom.Namespace;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.Variable;
import com.successmetricsinc.query.Database.DatabaseType;
import com.successmetricsinc.query.olap.DimensionMemberSetNav;
import com.successmetricsinc.security.EncryptionService;
import com.successmetricsinc.transformation.BirstScriptLexer;
import com.successmetricsinc.transformation.BirstScriptParser;
import com.successmetricsinc.transformation.Expression;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.Statement;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.transformation.BirstScriptParser.logicalColumn_return;
import com.successmetricsinc.transformation.BirstScriptParser.logicalExpression_return;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.DateUtil;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

/**
 * Class to encapsulate/generate a query filter (including parenthetical expressions)
 * 
 * @author bpeters
 * 
 */
public class QueryFilter implements Comparable<QueryFilter>, Externalizable, Cloneable, Serializable, BasicDBObjectSerializer
{
	private static final String HIERARCHY = "Hierarchy";
	public static final String FILTER_NAME = "Filter";
	private static final String IS_PROMPTED = "isPrompted";
	private static final String PROMPT_NAME = "PromptName";
	private static final String NAME = "Name";
	private static final String FILTER_LIST = "FilterList";
	private static final String OPERAND = "Operand";
	private static final String OPERATOR = "Operator";
	private static final String COLUMN = "Column";
	private static final String INPUT_TEXT = "InputText";
	private static final String DIMENSION = "Dimension";
	private static final String COLUMN_TYPE = "ColumnType";
	private static final String TYPE = "Type";
	private static Logger logger = Logger.getLogger(QueryFilter.class);
	private static final long serialVersionUID = 2L;
	public static final int TYPE_PREDICATE = 0;
	public static final int TYPE_LOGICAL_OP = 1;
	public static final int TYPE_DIMENSION_COLUMN = 0;
	public static final int TYPE_MEASURE = 1;
	public static final int TYPE_DIMENSION_MEMBER_SET = 2;
	public static final int TYPE_INPUT_TEXT = 2;
	private static final String DATATYPE = "DataType";
	private static final String OPERAND_DISPLAYVALUE = "Operand_DisplayValue";
	private static final String DATETIME_FORMAT = "DateTimeFormat";
	private static final String TRELLIS_CHART_FILTER = "IsTrellisChart";
	private String Name;
	private transient DimensionColumn dc;
	private transient MeasureColumn mc;
	private String Dimension;
	private String hierarchy;
	private String Column;
	private String Operator = "=";
	private String Operand;
	private Tree tOperand;
	private int Type;
	private int ColumnType;
	private List<QueryFilter> FilterList;
	private String promptName;
	private QueryFilter MeasureFilter;
	private Double MeasureFilterDefault;
	private boolean pushDown;
	public transient String dataType;
	public transient String Operand_DisplayValue;
	public transient String DateTime_Format;
	public transient boolean includedInPhysicalQuery;
	
	public static final String IN = "IN";
	public static final String NOTIN = "NOT IN";
	public static final String LIKE = "LIKE";
	public static final String NOTLIKE = "NOT LIKE";
	public static final String ISNULL = "IS NULL";
	public static final String ISNOTNULL = "IS NOT NULL";

	private boolean isTrellisChartFilter = false;
	private boolean isQueryOnSameConn = true;
	private String inputText;
	
	private QueryFilter QF1;
	private QueryFilter QF2;
	
	/**
	 * Constructor used for serialization
	 */
	public QueryFilter()
	{
	}

	public QueryFilter(Element e, Namespace ns)
	{
		Type = Integer.parseInt(e.getChildTextTrim(TYPE, ns));
		if (Type == QueryFilter.TYPE_PREDICATE)
		{
			int coltype = Integer.parseInt(e.getChildTextTrim(COLUMN_TYPE, ns));
			if (coltype == QueryFilter.TYPE_DIMENSION_COLUMN)
			{
				this.Dimension = e.getChildTextTrim(DIMENSION, ns);
				this.hierarchy = e.getChildTextTrim(HIERARCHY, ns);
				this.Column = e.getChildTextTrim(COLUMN, ns);
				this.Operator = e.getChildTextTrim(OPERATOR, ns);
				this.Operand = e.getChildTextTrim(OPERAND, ns);
				this.Type = TYPE_PREDICATE;
				this.ColumnType = TYPE_DIMENSION_COLUMN;
			}
			if (coltype == QueryFilter.TYPE_MEASURE)
			{
				this.Column = e.getChildTextTrim(COLUMN, ns);
				this.Operator = e.getChildTextTrim(OPERATOR, ns);
				this.Operand = e.getChildTextTrim(OPERAND, ns);
				this.Type = TYPE_PREDICATE;
				this.ColumnType = TYPE_MEASURE;
			}
			if (coltype == QueryFilter.TYPE_INPUT_TEXT) {
				this.inputText = e.getChildTextTrim(INPUT_TEXT);
				this.Type = TYPE_INPUT_TEXT;
			}
		} else if (Type == QueryFilter.TYPE_LOGICAL_OP)
		{
			Element e1 = e.getChild("QF1", ns);
			Element e2 = e.getChild("QF2", ns);
			QF1 = new QueryFilter(e1, ns);
			QF2 = new QueryFilter(e2, ns);
			this.Operator = e.getChildTextTrim(OPERATOR, ns);
			// Create new list with initial capacity of 2
			this.FilterList = new ArrayList<QueryFilter>(2);
			// See if filters can be collapsed
			if (QF1.Type == TYPE_LOGICAL_OP && QF1.Operator.equals(this.Operator))
				FilterList.addAll(QF1.FilterList);
			else
				FilterList.add(QF1);
			// See if filters can be collapsed
			if (QF2.Type == TYPE_LOGICAL_OP && QF2.Operator.equals(this.Operator))
				FilterList.addAll(QF2.FilterList);
			else
				FilterList.add(QF2);
		}
		setName(e.getChildTextTrim(NAME, ns));
	}

	public void parseElement(XMLStreamReader parser) throws Exception {
		boolean typeSet = false;
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
			String elName = parser.getLocalName();
			if (TYPE.equals(elName))
				this.Type = XmlUtils.getIntContent(parser);
			else if (COLUMN_TYPE.equals(elName)) {
				Integer ct = XmlUtils.getIntContent(parser);
				if (ct != null) {
					this.ColumnType = ct.intValue(); 
					typeSet = true;
				}
			}
			else if (DIMENSION.equals(elName))
				this.Dimension = XmlUtils.getStringContent(parser);
			else if (HIERARCHY.equals(elName))
				this.hierarchy = XmlUtils.getStringContent(parser);
			else if (COLUMN.equals(elName))
				this.Column = XmlUtils.getStringContent(parser);
			else if (OPERAND.equals(elName))
				this.Operand = XmlUtils.getStringContent(parser);
			else if (OPERATOR.equals(elName))
				this.Operator = XmlUtils.getStringContent(parser);
			else if (INPUT_TEXT.equals(elName))
				this.inputText = XmlUtils.getStringContent(parser);
			else if (FILTER_LIST.equals(elName)) {
				this.FilterList = new ArrayList<QueryFilter>();
				for (XMLStreamIterator iter2 = new XMLStreamIterator(parser); iter2.hasNext(); ) {
					if (QueryFilter.FILTER_NAME.equals(parser.getLocalName())) {
						QueryFilter filter = new QueryFilter();
						filter.parseElement(parser);
						this.FilterList.add(filter);
					}
				}
			}
			else if (NAME.equals(elName))
				this.Name = XmlUtils.getStringContent(parser);
			else if (PROMPT_NAME.equals(elName))
				this.promptName = XmlUtils.getStringContent(parser);
			else if (IS_PROMPTED.equals(elName)) {
				if (this.promptName == null)
					this.promptName = getDefaultPromptName();
			}
			else if (DATATYPE.equals(elName))
				this.dataType = XmlUtils.getStringContent(parser);
			else if (OPERAND_DISPLAYVALUE.equals(elName))
				this.Operand_DisplayValue = XmlUtils.getStringContent(parser);
			else if (DATETIME_FORMAT.equals(elName))
				this.DateTime_Format = XmlUtils.getStringContent(parser);
			else if (TRELLIS_CHART_FILTER.equals(elName))
				this.isTrellisChartFilter = XmlUtils.getBooleanContent(parser);
		}
		if (typeSet == false) {
			if (Dimension == null || !Util.hasNonWhiteSpaceCharacters(Dimension))
				ColumnType = TYPE_MEASURE;
			else 
				ColumnType = TYPE_DIMENSION_COLUMN;
		}
	}
	
	@SuppressWarnings("unchecked")
	public void parseElement(OMElement parent)
	{
		boolean typeSet = false;
		for (Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext();)
		{
			OMElement el = iter.next();
			String elName = el.getLocalName();
			if (TYPE.equals(elName))
			{
				this.Type = XmlUtils.getIntContent(el);
			} else if (COLUMN_TYPE.equals(elName))
			{
				this.ColumnType = XmlUtils.getIntContent(el);
				typeSet = true;
			} else if (DIMENSION.equals(elName))
			{
				this.Dimension = XmlUtils.getStringContent(el);
			} else if (HIERARCHY.equals(elName)) {
				this.hierarchy = XmlUtils.getStringContent(el);
			} else if (COLUMN.equals(elName))
			{
				this.Column = XmlUtils.getStringContent(el);
			} else if (OPERAND.equals(elName))
			{
				this.Operand = XmlUtils.getStringContent(el);
			} else if (OPERATOR.equals(elName))
			{
				this.Operator = XmlUtils.getStringContent(el);
			} else if (INPUT_TEXT.equals(elName)) {
				this.inputText = XmlUtils.getStringContent(el);
			} else if (FILTER_LIST.equals(elName))
			{
				this.FilterList = new ArrayList<QueryFilter>();
				for (Iterator<OMElement> iter1 = el.getChildElements(); iter1.hasNext();)
				{
					QueryFilter filter = new QueryFilter();
					filter.parseElement(iter1.next());
					this.FilterList.add(filter);
				}
			} else if (NAME.equals(elName))
			{
				this.Name = XmlUtils.getStringContent(el);
			} else if (PROMPT_NAME.equals(elName))
			{
				this.promptName = XmlUtils.getStringContent(el);
			} else if (IS_PROMPTED.equals(elName))
			{
				if (this.promptName == null)
				{
					this.promptName = getDefaultPromptName();
				}
			} else if (DATATYPE.equals(elName))
			{
				this.dataType = XmlUtils.getStringContent(el);
			} else if (OPERAND_DISPLAYVALUE.equals(elName))
			{
				this.Operand_DisplayValue = XmlUtils.getStringContent(el);
			} else if (DATETIME_FORMAT.equals(elName))
			{
				this.DateTime_Format = XmlUtils.getStringContent(el);
			}else if (TRELLIS_CHART_FILTER.equals(elName)){
				this.isTrellisChartFilter = XmlUtils.getBooleanContent(el);
			}
		}
		if (typeSet == false) {
			if (Dimension == null || !Util.hasNonWhiteSpaceCharacters(Dimension))
				ColumnType = TYPE_MEASURE;
			else 
				ColumnType = TYPE_DIMENSION_COLUMN;
		}
	}
	
	public Element getQueryFilterElement(String elementName,Namespace ns)
	{
		Element e = new Element(elementName,ns);
		Element child = null;
		
		if (Name!=null)
		{
			child = new Element("Name",ns);
			child.setText(Name);
			e.addContent(child);
		}
		
		child = new Element("Type",ns);
		child.setText(String.valueOf(Type));
		e.addContent(child);
		
		child = new Element("ColumnType",ns);
		child.setText(String.valueOf(ColumnType));
		e.addContent(child);
		
		if (Dimension!=null)
		{
			child = new Element("Dimension",ns);
			child.setText(Dimension);
			e.addContent(child);
		}
		
		if (Column!=null)
		{
			child = new Element("Column",ns);
			child.setText(Column);
			e.addContent(child);
		}
		
		child = new Element("Operator",ns);
		child.setText(Operator);
		e.addContent(child);
		
		if (Operand!=null)
		{
			child = new Element("Operand",ns);
			child.setText(Operand);
			e.addContent(child);
		}
		
		if (QF1!=null)
		{			
			e.addContent(QF1.getQueryFilterElement("QF1", ns));
		}
		
		if (QF2!=null)
		{			
			e.addContent(QF2.getQueryFilterElement("QF2", ns));
		}
				
		return e;
	}

	public String getDefaultPromptName()
	{
		if (ColumnType == TYPE_DIMENSION_COLUMN)
		{
			return Dimension + "_" + Column;
		} else if (ColumnType == TYPE_MEASURE)
		{
			return Column;
		}
		return "";
	}

	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns, TimeZone procTimeZone, SimpleDateFormat dateFormat, SimpleDateFormat dateTimeFormat)
	{
		OMElement filter = fac.createOMElement(FILTER_NAME, ns);
		XmlUtils.addContent(fac, filter, TYPE, Integer.toString(Type), ns);
		if (Type == QueryFilter.TYPE_PREDICATE)
		{
			XmlUtils.addContent(fac, filter, COLUMN_TYPE, Integer.toString(this.ColumnType), ns);
			if (this.ColumnType == QueryFilter.TYPE_DIMENSION_COLUMN)
			{
				XmlUtils.addContent(fac, filter, DIMENSION, this.Dimension, ns);
				if (this.hierarchy != null && this.hierarchy.trim().length() > 0)
					XmlUtils.addContent(fac, filter, HIERARCHY, this.hierarchy, ns);
			}
			if (this.ColumnType == QueryFilter.TYPE_DIMENSION_COLUMN || this.ColumnType == QueryFilter.TYPE_MEASURE)
			{
				XmlUtils.addContent(fac, filter, COLUMN, this.Column, ns);
				XmlUtils.addContent(fac, filter, OPERATOR, this.Operator, ns);
				XmlUtils.addContent(fac, filter, OPERAND, this.Operand, ns);
				XmlUtils.addContent(fac, filter, DATATYPE, this.dataType, ns);
				// Need to format the displayvalue of date/datetime operand to reflect user's timezone and locale
				boolean noOperand = this.Operator.equalsIgnoreCase(QueryFilter.ISNULL) || this.Operator.equalsIgnoreCase(QueryFilter.ISNOTNULL);
				if (!noOperand && this.Operand != null && dataType != null && (this.dataType.equals("Date") || this.dataType.equals("DateTime")))
				{
					SimpleDateFormat sdf = new SimpleDateFormat(DateUtil.DB_FORMAT_STRING);
					sdf.setTimeZone(procTimeZone);
					sdf.setLenient(false);
					try
					{
						if (this.dataType.equals("Date"))
						{
							SimpleDateFormat df = new SimpleDateFormat(dateFormat.toPattern());
							df.setTimeZone(procTimeZone);
							df.setLenient(false);
							this.Operand_DisplayValue = df.format(sdf.parse(this.Operand));
							this.DateTime_Format = dateFormat.toPattern();
						} else
						{
							this.Operand_DisplayValue = dateTimeFormat.format((sdf.parse(this.Operand)));
							this.DateTime_Format = dateTimeFormat.toPattern();
						}
					} catch (ParseException pe)
					{
						this.Operand_DisplayValue = this.Operand;
					}
				}
				XmlUtils.addContent(fac, filter, OPERAND_DISPLAYVALUE, this.Operand_DisplayValue, ns);
				XmlUtils.addContent(fac, filter, DATETIME_FORMAT, this.DateTime_Format, ns);
			}
		} else if (Type == QueryFilter.TYPE_LOGICAL_OP)
		{
			XmlUtils.addContent(fac, filter, OPERATOR, this.Operator, ns);
			if (this.FilterList != null)
			{
				OMElement type = fac.createOMElement(FILTER_LIST, ns);
				for (QueryFilter child : this.FilterList)
				{
					child.addContent(fac, type, ns, procTimeZone, dateFormat, dateTimeFormat);
				}
				filter.addChild(type);
			}
		}
		else if (Type == QueryFilter.TYPE_INPUT_TEXT) {
			XmlUtils.addContent(fac, filter, INPUT_TEXT, inputText, ns);
		}
		XmlUtils.addContent(fac, filter, NAME, this.getName(), ns);
		XmlUtils.addContent(fac, filter, PROMPT_NAME, this.getPromptName(), ns);
		XmlUtils.addContent(fac, filter, TRELLIS_CHART_FILTER, this.isTrellisChartFilter, ns);
		parent.addChild(filter);
	}

	/**
	 * Clone filter - does not copy any navigation information. Does a deep copy.
	 * 
	 * @see java.lang.Object#clone()
	 */
	public Object clone() throws CloneNotSupportedException
	{
		QueryFilter newqf = (QueryFilter) super.clone();
		newqf.Name = this.Name;
		newqf.Dimension = this.Dimension;
		newqf.hierarchy = this.hierarchy;
		newqf.Column = this.Column;
		newqf.Operator = this.Operator;
		newqf.Operand = this.Operand;
		newqf.Type = this.Type;
		newqf.ColumnType = this.ColumnType;
		newqf.pushDown = pushDown;
		newqf.inputText = inputText;
		if (this.FilterList != null)
		{
			newqf.FilterList = new ArrayList<QueryFilter>();
			for (QueryFilter qf : this.FilterList)
			{
				newqf.FilterList.add((QueryFilter) qf.clone());
			}
		}
		return (newqf);
	}

	public void copy(QueryFilter filter) throws CloneNotSupportedException
	{
		QueryFilter qf = (QueryFilter)filter.clone();
		
		Name = qf.Name;
		Dimension = qf.Dimension;
		hierarchy = qf.hierarchy;
		Column = qf.Column;
		Operator = qf.Operator;
		Operand = qf.Operand;
		Type = qf.Type;
		ColumnType = qf.ColumnType;
		pushDown = qf.pushDown;
		inputText = qf.inputText;
					
		if (qf.FilterList != null)
		{
			FilterList = new ArrayList<QueryFilter>();
			for (QueryFilter q : qf.FilterList)
			{
				FilterList.add((QueryFilter) q.clone());
			}
		}
	}
	
	/**
	 * Create a dimension column filter predicate
	 * 
	 * @param dimension
	 *            Dimension name
	 * @param column
	 *            Column name
	 * @param operator
	 *            Operator
	 * @param operand
	 *            Comparison Operand
	 */
	public QueryFilter(String dimension, String column, String operator, String operand)
	{
		this.Dimension = dimension;
		this.Column = column;
		if (operator != null)
			this.Operator = operator.trim();
		this.Operand = operand;
		this.Type = TYPE_PREDICATE;
		this.ColumnType = TYPE_DIMENSION_COLUMN;
	}

	/**
	 * Create a dimension column filter predicate
	 * 
	 * @param dimension
	 *            Dimension name
	 * @param column
	 *            Column name
	 * @param operator
	 *            Operator
	 * @param operand
	 *            Comparison Operand
	 */
	public QueryFilter(String dimension, String column, String operator, Tree operand)
	{
		this.Dimension = dimension;
		this.Column = column;
		if (operator != null)
			this.Operator = operator.trim();
		this.tOperand = operand;
		this.Type = TYPE_PREDICATE;
		this.ColumnType = TYPE_DIMENSION_COLUMN;
	}
	
	public QueryFilter(String dimension, String aliasName, String operator, String operand, int columnType)
	{
		this.Dimension = dimension;
		this.Column = aliasName;
		this.Operator = operator.trim();
		this.Operand = operand;
		this.Type = TYPE_PREDICATE;
		this.ColumnType = columnType;
	}

	/**
	 * Create a measure predicate filter
	 * 
	 * @param measureName
	 *            Measure name
	 * @param operator
	 *            Operator (=,<,>, etc.)
	 * @param operand
	 *            Comparison Operand
	 */
	public QueryFilter(String measureName, String operator, String operand)
	{
		this.Column = measureName;
		this.Operator = operator.trim();
		this.Operand = operand;
		this.Type = TYPE_PREDICATE;
		this.ColumnType = TYPE_MEASURE;
	}

	/**
	 * Create a measure predicate filter with a filter on the measure
	 * 
	 * @param measureName
	 *            Measure name
	 * @param operator
	 *            Operator (=,<,>, etc.)
	 * @param operand
	 *            Comparison Operand
	 * @param measureFilter
	 *            Measure filter to apply to measure
	 */
	public QueryFilter(String measureName, String operator, String operand, QueryFilter measureFilter)
	{
		this.Column = measureName;
		this.Operator = operator;
		this.Operand = operand.trim();
		this.Type = TYPE_PREDICATE;
		this.ColumnType = TYPE_MEASURE;
		this.MeasureFilter = measureFilter;
	}

	/**
	 * Create a compound filter from subfilter predicates or other filters
	 * 
	 * @param QF1
	 *            First query filter
	 * @param operator
	 *            Operator (AND, OR)
	 * @param QF2
	 */
	public QueryFilter(QueryFilter QF1, String operator, QueryFilter QF2)
	{
		this.Type = TYPE_LOGICAL_OP;
		this.Operator = operator;
		// Create new list with initial capacity of 2
		this.FilterList = new ArrayList<QueryFilter>(2);
		// See if filters can be collapsed
		if (QF1.Type == TYPE_LOGICAL_OP && QF1.Operator.equals(operator))
			FilterList.addAll(QF1.FilterList);
		else
			FilterList.add(QF1);
		// See if filters can be collapsed
		if (QF2.Type == TYPE_LOGICAL_OP && QF2.Operator.equals(operator))
			FilterList.addAll(QF2.FilterList);
		else
			FilterList.add(QF2);
	}

	/**
	 * Create a compound filter from subfilter predicates or other filters
	 * 
	 * @param operator
	 *            Operator (AND, OR)
	 * @param queryFilterList
	 *            List of query filters
	 */
	public QueryFilter(String operator, List<QueryFilter> queryFilterList)
	{
		this.Type = TYPE_LOGICAL_OP;
		this.Operator = operator;
		// Create new list with initial capacity of 2
		this.FilterList = queryFilterList;
	}
	
	public static QueryFilter createFilter(Filter f) {
		List<QueryFilter> list = new ArrayList<QueryFilter>();
		String name = f.getColumnName();
		String operator = f.getOperator();
		String parts[] = name.split("\\.");
		List<String> values = f.getValues();
		
		if(operator.equals("Range") && values.size() >=2){
			return JasperDataSourceProvider.createRangeFilter((parts.length == 1) ? null : parts[1], parts[0], list, values);
		}else{
			for (String s: values) {
				QueryFilter f1 = null;
				if (parts.length == 1)
					f1 = new QueryFilter(parts[0], operator, s);
				else
					f1 = new QueryFilter(parts[0], parts[1], operator, s);
				list.add(f1);
			}
			if (list.size() == 1) {
				return list.get(0);
			}
		}
		
		return new QueryFilter(f.getLogicalOperator(), list);
	}

	/**
	 * does the filter contain IN/NOTIN tOperand - can't use inexact file cache now
	 */
	public boolean isTooComplex()
	{
		if(Type == TYPE_PREDICATE)
		{
			if ((Operator.equals(QueryFilter.IN) || Operator.equals(QueryFilter.NOTIN)) && 
			      (tOperand != null || (Operand != null && (Operand.toLowerCase().startsWith("select") || Operand.indexOf("Q{") >= 0)))
			   )
				return true;
		}
		else
		{
			// Otherwise, recursively set subfilters
			if (FilterList != null)
				for (QueryFilter qf : FilterList)
				{
					if(qf.isTooComplex())
					{
						return true;
					}
				}
		}
		return false;
	}

	/**
	 * Sets the filter parameters based on navigated QueryColumnList
	 * 
	 * @param queryColumnList
	 *            Navigated QueryColumnList
	 */
	public void setNavigation(List<QueryColumn> queryColumnList)
	{
		// If it's a predicate, search for appropriate column and set it
		if (Type == TYPE_PREDICATE)
		{
			for (QueryColumn qc : queryColumnList)
			{
				if ((ColumnType == TYPE_DIMENSION_COLUMN) && (qc.type == QueryColumn.DIMENSION))
				{
					DimensionColumn dc = (DimensionColumn) qc.o;
					if (dc.matchesDimensionName(Dimension) && dc.ColumnName.equals(Column))
					{
						this.dc = dc;
						break;
					}
				} else if ((ColumnType == TYPE_MEASURE) && (qc.type == QueryColumn.MEASURE))
				{
					MeasureColumn mc = (MeasureColumn) qc.o;
					if (mc.ColumnName.equals(Column) || (qc.filter != null && Column.equals(qc.filter.getMeasureFilterName(mc.ColumnName))))
					{
						this.mc = mc;
						if (MeasureFilter != null)
							MeasureFilter.setNavigation(queryColumnList);
						break;
					}
				}
			}
		} else
		{
			// Otherwise, recursively set subfilters
			if (FilterList != null)
				for (QueryFilter qf : FilterList)
				{
					qf.setNavigation(queryColumnList);
				}
		}
	}

	/**
	 * Escape all non-quoted commas with quotes
	 * 
	 * @param s
	 * @param dataType 
	 * @param dbc 
	 * @return
	 */
	private String escapeElements(String s, String dataType, DatabaseConnection dbc)
	{
		StringBuilder sb = new StringBuilder();
		char quote = 0;
		for (int i = 0; i < s.length(); i++)
		{
			if (s.charAt(i) == ',' && quote == 0)
			{
				sb.append("',");
				sb.append(dbc.getAsciiLiteralOperand("", dataType));
				sb.append("'");
			} else if (quote == 0 && (s.charAt(i) == '"' || s.charAt(i) == '\''))
			{
				quote = s.charAt(i);
			} else if (quote != 0 && s.charAt(i) == quote)
			{
				if (i + 1 < s.length() && s.charAt(i + 1) == quote)
				{
					// escaped quote
					sb.append(quote).append(quote); // just pass it on
					i++;
				} else
				{
					quote = 0;
				}
			} else
				sb.append(s.charAt(i));
		}
		return (sb.toString());
	}
	
	private transient String savedOperand = null;
	
	private String getOperand(Repository r, Session session, String operand)
	{
		if (operand != null && r != null)
		{
			String evalop = operand;
			if (evalop.startsWith("'") && evalop.endsWith("'") && evalop.length() > 2)
			{
				evalop = evalop.substring(1, evalop.length() - 1);
			}
			boolean isEvaluate = evalop.startsWith("exp:") && evalop.length() > 4;
			if (isEvaluate)
				evalop = evalop.substring(4);
			if (isEvaluate)
			{
				if (r.getCurrentBuilderVersion() >= 10 || DatabaseConnection.isDBTypeInfoBright(r.getDefaultConnection().DBType))
				{
					if (savedOperand == null)
					{
						ANTLRStringStream stream = new ANTLRStringStream(evalop);
						BirstScriptLexer lex = new BirstScriptLexer(stream);
						CommonTokenStream cts = new CommonTokenStream(lex);
						BirstScriptParser parser = new BirstScriptParser(cts);
						try
						{
							logicalColumn_return ret;
							ret = parser.logicalColumn();
							if (!lex.hasError())
							{
								TransformationScript ts = new TransformationScript(r, session);
								ts.setLogicalQuery(true);
								Expression ex = new Expression(ts);
								Tree column = (Tree) ret.getTree();
								Operator op = ex.compile(column);
								Object o = op.evaluate();
								if (Date.class.isInstance(o))
								{
									SimpleDateFormat sdf = new SimpleDateFormat(DateUtil.DB_FORMAT_STRING);
									operand = sdf.format((Date)o);
								} else if (java.util.GregorianCalendar.class.isInstance(o))
								{
									SimpleDateFormat sdf = new SimpleDateFormat(DateUtil.DB_FORMAT_STRING);
									operand = sdf.format(((java.util.GregorianCalendar) o).getTime());
								} else
								{
									operand = o.toString();
								}
							} else
							{
								logger.info("Unable to parse operand: " + evalop);
							}
						} catch (Exception e)
						{
							logger.info("Unable to parse operand: " + e.getMessage());
						}
						savedOperand = operand;
					} else
						operand = savedOperand;
				}
			}
		}
		return operand;
	}

	private String getFilterString(DatabaseConnection dbc, boolean aggregate, Query q, MeasureTable mt, List<String> melist, QueryMap tableMap, Navigation nav,
			List<String> lookupJoins) throws SyntaxErrorException
	{
		// If it's a predicate, search for appropriate column and set it
		if (Type == TYPE_PREDICATE)
		{
			Repository r = q.getRepository();
			Session session = q.getSession();
			String operand = getOperand(r, session, Operand);
			if (isPrompted() && QueryString.NO_FILTER_TEXT.equals(operand))
				return null;
			if (ColumnType == TYPE_DIMENSION_COLUMN)
			{
				if (dc == null)
					return null;
				String dataType = dc.DataType;
				if (dc.DisplayMap != null)
					dataType = dc.DisplayMap.DataType;
				if (Operator.equals(QueryFilter.IN) || Operator.equals(QueryFilter.NOTIN))
				{
					Map<DatabaseConnection, List<TempTable>> tempTableMap = q.getTempTableMap();
					if (tempTableMap == null)
						tempTableMap = new HashMap<DatabaseConnection, List<TempTable>>();
					List<TempTable> ttlist = tempTableMap.get(dbc);
					if (ttlist == null)
					{
						ttlist = new ArrayList<TempTable>();
						tempTableMap.put(dbc, ttlist);
					}
					q.setTempTableMap(tempTableMap);
					if (tOperand != null || operand.indexOf("Q{") >= 0 || operand.toLowerCase().startsWith("select"))
					{
						String replacestr = null;
						DatabaseConnection dconn = dc.DimensionTable.TableSource.connection;						
						if (tOperand != null || operand.toLowerCase().startsWith("select"))
						{
							LogicalQueryString lqs = null;
							if (tOperand != null)
								lqs = new LogicalQueryString(r, session, tOperand);
							else
								lqs = new LogicalQueryString(r, session, operand);
							try
							{
								Query lq = lqs.getQuery();
								if (dconn.DBType == DatabaseType.Infobright)
								{
									lq.navigateQuery(false, true, null, true); // try to force the type information
									String lqstr = "Q{" + lq.getNewLogicalQueryString(false, null) + "}";
									replacestr = lqs.replaceQueryString(lqstr, false, q.getSession(), !lq.hasTop(), true, dc, dbc, q.getTempTableMap());
								} else
								{
									if (!DatabaseConnection.isDBTypeMemDB(dconn.DBType))
										lq.setDimensionColumnToKeep(dc);
									lq.generateQuery(false);
									replacestr = lq.getQuery();
									if (DatabaseConnection.isDBTypeMemDB(dconn.DBType) && lq.containsMultipleQueries)
										throw new SyntaxErrorException("Invalid IN subquery: contains multiple subquery aggregations");
									if (!lq.hasTop())
									{
										int indexob = replacestr.lastIndexOf("ORDER BY");
										if (indexob > 0 && lq.hasOrderBys() && (replacestr.endsWith("ASC") || replacestr.endsWith("DESC")))
										{
											replacestr = replacestr.substring(0, indexob - 1);
										}
									}
								}
								// support filtering in data federation across disparate databases with non-conforming dimensions
								List<DatabaseConnection> connectionList = lq.getConnections();
								if (connectionList!=null && !connectionList.isEmpty())
								{
									DatabaseConnection queryConn = connectionList.get(0);	
									if ((queryConn.ConnectString != null) && (dconn.ConnectString != null) && (!queryConn.ConnectString.trim().equals(dconn.ConnectString.trim())))
									{
										isQueryOnSameConn = false;
										logger.info("creating temp table as subquery needs to be executed on different connection");										
										replacestr = getInnerPhyicalQuery(r, session, nav, lq, dconn, queryConn, q.getTempTableMap(), lookupJoins, 
												tableMap, q);
										if(replacestr.equals("1=1")){
											return replacestr;
										}
									}
								}
																
							} catch (SyntaxErrorException e)
							{
								throw e;
							} catch (Exception e)
							{
								logger.warn(e, e);
								return (q.getDimensionColumnString(dbc, dc, q.getVcDimensionTables(), tableMap, lookupJoins) + " " + Operator + " (" + operand + ")");
							}
						} else
						{
							QueryString qs = new QueryString(r);
							try
							{
								replacestr = qs.replaceQueryString(operand, false, q.getSession(), !(operand.toUpperCase().contains("TOP{")),
										!DatabaseConnection.isDBTypeMemDB(dbc.DBType), dc, dbc, q.getTempTableMap());
							} catch (Exception e)
							{
								logger.warn(e, e);
								return (q.getDimensionColumnString(dbc, dc, q.getVcDimensionTables(), tableMap, lookupJoins) + " " + Operator + " (" + operand + ")");
							}
						}
						if (dconn.DBType == DatabaseType.Infobright && q.topn != 0 && isQueryOnSameConn)
						{
							long x = System.currentTimeMillis();
							ttlist = tempTableMap.get(dbc);
							if (ttlist == null)
							{
								ttlist = Collections.synchronizedList(new ArrayList<TempTable>());
								tempTableMap.put(dbc, ttlist);
							}
							TempTable tt = dbc.getTempTableForQueryFilter(q.getRepository(), dc, x, ttlist, replacestr);
							replacestr = "SELECT * FROM " + tt.tableName;
							if (!ttlist.contains(tt))
							{
								ttlist.add(tt);
							}
							// Need to convert IN FILTER to inner join due to Infobright BUG (IN doesn't work)
							if (Operator.equals(QueryFilter.IN))
							{
								lookupJoins.add(dbc.getInnerJoin() + " " + tt.tableName + " ON " + tt.tableName + "." + tt.pname + "="
										+ q.getDimensionColumnString(dbc, dc, q.getVcDimensionTables(), tableMap, lookupJoins));
								return ("1=1");
							}
						}
						/*
						 * Replace any references to dimension columns in dimension column filters (they refer back to
						 * outer query). E.g. Q{FDC{Time.Month Number=Time.Month Number}} the latter refers to the value
						 * in the parent query
						 */
						String result = "";
						if(!replacestr.isEmpty()){
							result = q.getDimensionColumnString(dbc, dc, q.getVcDimensionTables(), tableMap, lookupJoins) + " " + Operator + " (" + replacestr
								+ ")";
						}else{
							//result = q.getDimensionColumnString(dbc, dc, q.getVcDimensionTables(), tableMap, lookupJoins) + " 1=0";
							return ("1=0");
						}
						
						if (nav != null)
							for (DimensionColumnNav dcn : nav.getDimColumns())
							{
								if (!dcn.navigateonly && dcn.colIndex >= 0)
									result = Util.replaceStr(result, "DC{" + dcn.dimensionName + "." + dcn.columnName + "}", q.getDimensionColumnString(
											dbc, dcn.getDimensionColumn(), q.getVcDimensionTables(), tableMap, lookupJoins));
							}
						return (result);
					} else if (dataType.equals("Number") || dataType.equals("Integer") || dataType.equals("Float"))
						return (q.getDimensionColumnString(dbc, dc, q.getVcDimensionTables(), tableMap, lookupJoins) + " " + Operator + " (" + operand + ")");
					else
					{
						return (q.getDimensionColumnString(dbc, dc, q.getVcDimensionTables(), tableMap, lookupJoins) + " " + Operator + " ("+ dbc.getAsciiLiteralOperand("", dataType) + "'"
								+ escapeElements(operand,dc.DataType,dbc) + "')");
					}
				} else if (Operator.equals(QueryFilter.LIKE))
				{
					if (dataType.equals("Number") || dataType.equals("Integer") || dataType.equals("Float"))
						throw new SyntaxErrorException("LIKE and NOT LIKE operators are not allowed on numeric values");
					else
						return (q.getDimensionColumnString(dbc, dc, q.getVcDimensionTables(), tableMap, lookupJoins) + " LIKE " + dbc.getAsciiLiteralOperand("", dataType) + "'" + dbc.escapeLiteral(operand) + "'");
				} else if (Operator.equals(QueryFilter.NOTLIKE))
				{
					if (dataType.equals("Number") || dataType.equals("Integer") || dataType.equals("Float"))
						throw new SyntaxErrorException("LIKE and NOT LIKE operators are not allowed on numeric values");
					else
						return (q.getDimensionColumnString(dbc, dc, q.getVcDimensionTables(), tableMap, lookupJoins) + " NOT LIKE "
								+ dbc.getAsciiLiteralOperand("", dataType) +("'" + dbc.escapeLiteral(operand) + "'"));
				} else if (Operator.equalsIgnoreCase(QueryFilter.ISNULL) || Operator.equalsIgnoreCase(QueryFilter.ISNOTNULL))
				{
					return (q.getDimensionColumnString(dbc, dc, q.getVcDimensionTables(), tableMap, lookupJoins) + " " + Operator);
				} else
				{
					if (dataType.equals("Number") || dataType.equals("Integer") || dataType.equals("Float") || quoted(operand))
						return (q.getDimensionColumnString(dbc, dc, q.getVcDimensionTables(), tableMap, lookupJoins) + Operator + operand);
					else
					{
						if (DatabaseConnection.isDBTypeOracle(dbc.DBType) && (dataType.equals("Date") || dataType.equals("DateTime")))
							return ("CAST(" + q.getDimensionColumnString(dbc, dc, q.getVcDimensionTables(), tableMap, lookupJoins) + " AS TIMESTAMP)" + 
									Operator + dbc.getQuotedLiteral(dbc.escapeLiteral(operand), dc.DataType));
						else
							return (q.getDimensionColumnString(dbc, dc, q.getVcDimensionTables(), tableMap, lookupJoins) + Operator + dbc.getAsciiLiteralOperand(dbc.getQuotedLiteral(dbc
									.escapeLiteral(operand), dc.DataType), dc.DataType));
					}
				}
			} else if (ColumnType == TYPE_MEASURE)
			{
				// If it hasn't navigated, return null
				if (mc == null)
					return null;
				String dataType = mc.DataType;
				if (mc.MeasureTable == mt)
				{
					if (Operator.equals(QueryFilter.LIKE) || Operator.equals(QueryFilter.NOTLIKE))
						throw new SyntaxErrorException("LIKE and NOT LIKE operators are not allowed on numeric values");
					if (DatabaseConnection.isDBTypeOracle(dbc.DBType) && (dataType.equals("Date") || dataType.equals("DateTime")))
					{
						return "CAST(" + q.getMeasureString(dbc, aggregate, mc, melist, (MeasureFilter == null && mc.MeasureFilter == null) ? null : q,
								MeasureFilter == null ? (mc.MeasureFilter == null ? null : mc.MeasureFilter) : MeasureFilter, tableMap, lookupJoins, null)
								+ " AS TIMESTAMP)" + ' ' + Operator + ((operand == null) ? "" : (' ' + operand));
					}
					return q.getMeasureString(dbc, aggregate, mc, melist, (MeasureFilter == null && mc.MeasureFilter == null) ? null : q,
							MeasureFilter == null ? (mc.MeasureFilter == null ? null : mc.MeasureFilter) : MeasureFilter, tableMap, lookupJoins, null)
							+ ' ' + Operator + ((operand == null) ? "" : (' ' + operand));
				}
			}
		} else
		{
			/*
			 * If subfilters are dimension or logical operations, recursively evaluate
			 */
			int count = 0;
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < FilterList.size(); i++)
			{
				QueryFilter qf = (QueryFilter) FilterList.get(i);
				String s = qf.getFilterString(dbc, aggregate, q, mt, melist, tableMap, nav, lookupJoins);
				if (s != null)
				{
					if (count == 0)
					{
						sb.append(s);
					} else if (count == 1)
					{
						sb.append(' ');
						sb.append(Operator);
						sb.append(' ');
						sb.append(s);
						sb.append(')');
						sb.insert(0, "(");
					} else if (count > 1)
					{
						sb.deleteCharAt(sb.length() - 1);
						sb.append(' ');
						sb.append(Operator);
						sb.append(' ');
						sb.append(s);
						sb.append(')');
					}
					count++;
				}
			}
			if (sb.length() > 0)
				return (sb.toString());
		}
		return (null);
	}

	/**
	 * This method supports filtering in data federation across disparate databases with non-conforming dimensions
	 * @param r
	 * @param session
	 * @param nav
	 * @param lq
	 * @param dconn
	 * @param queryConn
	 * @param tempTableMap
	 * @param lookupJoins 
	 * @param tableMap 
	 * @param q 
	 * @return
	 * @throws Exception
	 */
	private String getInnerPhyicalQuery(Repository r, Session session, Navigation nav, Query lq, DatabaseConnection dconn, 
			DatabaseConnection queryConn, Map<DatabaseConnection, List<TempTable>> tempTableMap, List<String> lookupJoins, QueryMap tableMap, 
			Query q) throws Exception 
	{		
		ResultSetCache rsc = ResultSetCache.getInstance(r.getApplicationIdentifier(), false);
		int qrsMaxRecords = r.getServerParameters().getMaxRecords();			
		QueryResultSet qrs = rsc.getQueryResultSet(lq, qrsMaxRecords, r.getServerParameters().getMaxQueryTime(), null, null, null,
				session, false, false, r.getServerParameters().getDisplayTimeZone(), false);
		
		if (qrs == null || !qrs.isValid())
			throw new Exception ("Exception executing query against live access ");
		
		nav.setNavExpires(qrs.getCurrentExpiresValue());
		int columnCount = qrs.getColumnNames().length;
		List<String> columnTypes = new ArrayList<String>();
		List<String> physicalColumnNames = new ArrayList<String>();
		String columnNames[] = qrs.getColumnNames();		
		int columnDataTypes[] = qrs.getColumnDataTypes();
		String column = lq.getColumnToKeep(dc);
		int indx = 0;
		if(column != null){
			indx = getIndex(column, columnNames);
		}
		boolean useInClause = true;
		
		if (qrs.rows!=null && qrs.rows.length > 1000)
		{
			useInClause = false;
		}
		
		/**
		 * if it's a real time connection then instead creating temp table, use values directly in IN operator.
		 */
		if(dconn.Realtime || useInClause)
		{
			String commaSeperatedVal = getCommaSeparatedVals(qrs, columnCount, indx);
			return commaSeperatedVal;
		}
		else
		{	// create temp table in outer query's connection and populate it with result of inner query
			for (int index=0;index<columnCount;index++)
			{
				String columnType = Util.getJDBCType(columnDataTypes[index]);				
				columnType = Util.getColumnType(dconn, columnType, false, null, null, r, dconn.getMaxVarcharWidth());
				columnTypes.add(columnType);
				physicalColumnNames.add(Util.replaceWithPhysicalString(queryConn, columnNames[index]));
			}
		
			List<String> columnCompressionEncodingsTemp = DatabaseConnection.isDBTypeParAccel(queryConn.DBType) ? new ArrayList<String>() : null;
		
			String tableLogicalName = lq.getKey(session);
			String tablePhysicalName = "TMP_"+ EncryptionService.getMD5(tableLogicalName, false).toUpperCase();
			int identifierMaxLen = DatabaseConnection.getIdentifierLength(dconn.DBType);
			if (tablePhysicalName.length()>identifierMaxLen)
			{				
				tablePhysicalName = dconn.getHashedIdentifier(tablePhysicalName);
				char[] cs = tablePhysicalName.toCharArray();
				if (cs[0] >= '0' && cs[0] <= '9')
					tablePhysicalName = 'X' + tablePhysicalName;
			}
		
			boolean createTempTable;
			String replacestr;
			if(dconn.Schema != null){
				createTempTable = Database.createTable(r, dconn, tablePhysicalName,physicalColumnNames,columnTypes, true, false, columnCompressionEncodingsTemp, null, null);
				Database.populateLiveAccessTmpTable(qrs, tablePhysicalName, dconn, physicalColumnNames);
				replacestr = "SELECT " + physicalColumnNames.get(indx) + " FROM " + dconn.Schema + "." + tablePhysicalName;
				logger.debug("Temp table for query connection : " + dconn.Schema + "." + tablePhysicalName);
			}else{						
				createTempTable = Database.createTable(r, dconn, dc.DimensionTable.TableSource.Schema + "." + tablePhysicalName,physicalColumnNames,columnTypes, true, false, columnCompressionEncodingsTemp, null, null);
				Database.populateLiveAccessTmpTable(qrs, dc.DimensionTable.TableSource.Schema + "." + tablePhysicalName, dconn, physicalColumnNames);
				replacestr = "SELECT " +  physicalColumnNames.get(indx) + " FROM " + dc.DimensionTable.TableSource.Schema + "." + tablePhysicalName;
				logger.debug("Temp table for query connection : " + dc.DimensionTable.TableSource.Schema + "." + tablePhysicalName);
			}
													
			List<TempTable> ttlist = tempTableMap.get(dconn);
			if (ttlist==null)
			{
				ttlist = new ArrayList<TempTable>();
				tempTableMap.put(dconn, ttlist);
			}
			TempTable tt = new TempTable();
			String schema;
			if(dconn.Schema != null)
			{
				tt.tableName = tablePhysicalName;	
				schema = dconn.Schema;
			}else{
				tt.tableName = dc.DimensionTable.TableSource.Schema + "." + tablePhysicalName;		
				schema = dc.DimensionTable.TableSource.Schema;
			}	
			tt.liveAccessTmpTable = true;
			ttlist.add(tt);		
			
			if (dconn.DBType == DatabaseType.Infobright && q.topn != 0)
			{
				if (Operator.equals(QueryFilter.IN))
				{
					lookupJoins.add(dconn.getInnerJoin() + " " + schema + "." + tablePhysicalName + " ON " 
							+ schema + "." + tablePhysicalName + "." + physicalColumnNames.get(0) + "="
							+ q.getDimensionColumnString(dconn, dc, q.getVcDimensionTables(), tableMap, lookupJoins));
					return ("1=1");
				}
			}
			return replacestr;
		}
	}
	
	private int getIndex(String column, String[] columnNames) throws SyntaxErrorException 
	{
		int index = -1;
		if(column != null)
		{
			for(int i=0; i <columnNames.length; i++)
			{
				if(column.equals(columnNames[i]))
				{
					index = i;
				}
			}			
		}
		//Didnt find anything, throw an exception
		if(index == -1)
		{
			throw new SyntaxErrorException("column " + column +	" does not exist on the filter query.");
		}
		return index;
	}
	
	/**
	 * Parse the output and return comma separated values.
	 * @param qrs
	 * @param columnCount
	 * @param indx 
	 * @return
	 */
	private String getCommaSeparatedVals(QueryResultSet qrs, int columnCount, int indx) 
	{
		String symbol = "";
		if (dc.DataType.equals("Varchar") || dc.DataType.equals("Date") || dc.DataType.equals("DateTime"))
			symbol = "'";	
		
		StringBuilder sb = new StringBuilder();
		boolean first = true;
		int rowCount = qrs.rows.length;
		if (rowCount>0 && (columnCount-1 >= indx))
		{
			for (int rowIndex=0;rowIndex<rowCount;rowIndex++)
			{				
				Object objData = qrs.rows[rowIndex][indx];
				if(objData != null)
				{
					if (objData instanceof GregorianCalendar)
					{
						GregorianCalendar calObj = (GregorianCalendar) objData;
						objData = new java.sql.Date(calObj.getTimeInMillis());
					}		
					if(first){
						first = false;
						sb.append(symbol).append(objData).append(symbol);
					}else{
						sb.append(",").append(symbol).append(objData).append(symbol);
					}
				}
			}
		}
		return sb.toString();
	}

	/**
	 * return a filter string appropriate for Google Analytics queries
	 * 
	 * @return
	 * @throws SyntaxErrorException
	 */
	public String getGAFilterString() throws SyntaxErrorException
	{
		// If it's a predicate, search for appropriate column and set it
		if (Type == TYPE_PREDICATE)
		{
			if (isPrompted() && QueryString.NO_FILTER_TEXT.equals(this.Operand))
				return null;
			String lhs = null;
			String result = null;
			if (ColumnType == TYPE_DIMENSION_COLUMN)
			{
				if (dc == null)
					return null;
				lhs = dc.PhysicalName;
				if (Operator.equals(">") || Operator.equals(">=") || Operator.equals("<") || Operator.equals("<=") 
						|| Operator.equals(QueryFilter.IN)
						|| Operator.equals(QueryFilter.NOTIN))
					throw new SyntaxErrorException(">, >=, <, <=, IN, and NOT IN are not allowed operators for dimension filters in Google Analytics");
				// use the Google operators
				if (Operator.equals(QueryFilter.LIKE))
				{
					// Regular expression
					result = "=~" + Operand;
				} else if (Operator.equals(QueryFilter.NOTLIKE))
				{
					// Regular expression
					result = "!~" + Operand;
				} else if (Operator.equals("<>"))
					result = "!=" + Operand;
				else if (Operator.equals("="))
					result = "==" + Operand;
				else
					result = Operator + Operand;
			} else if (ColumnType == TYPE_MEASURE)
			{
				// If it hasn't navigated, return null
				if (mc == null)
					return null;
				lhs = mc.PhysicalName;
				// use the Google operators
				if (Operator.equals("<>"))
					result = "!=" + Operand;
				else if (Operator.equals("="))
					result = "==" + Operand;
				else
					result = Operator + Operand;
			}
			if (result != null)
				try
				{
					result = URLEncoder.encode(result, "UTF-8");
					result = result.replace("+", "%20");
				} catch (UnsupportedEncodingException e)
				{
					logger.warn(e, e);
				}
			return lhs + result;
		} else
		{
			/*
			 * If subfilters are dimension or logical operations, recursively evaluate
			 */
			int count = 0;
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < FilterList.size(); i++)
			{
				QueryFilter qf = (QueryFilter) FilterList.get(i);
				String s = qf.getGAFilterString();
				if (s != null)
				{
					if (count == 0)
						sb.append(s);
					else if (Operator.equals("OR"))
						sb.append(',' + s);
					else
						// AND
						sb.append(';' + s);
					count++;
				}
			}
			if (sb.length() > 0)
				return (sb.toString());
		}
		return (null);
	}
	
	/**
	 * return a filter string appropriate for MDX queries
	 * 
	 * @return
	 * @throws SyntaxErrorException
	 */
	public String getMDXFilterString(DatabaseType dbType, Map<DimensionColumn, DimensionColumn> lowestLevelColumnMap, Map<String, String> dimColumnMap, boolean isAddNotNullCheck, List<DimensionMemberSetNav> dimMemberSets, Repository r) throws SyntaxErrorException
	{
		// If it's a predicate, search for appropriate column and set it
		if (Type == TYPE_PREDICATE)
		{
			if (isPrompted() && QueryString.NO_FILTER_TEXT.equals(this.Operand))
				return null;
			String lhs = null;
			String result = null;
			DimensionMemberSetNav dms = null;
			if (ColumnType == TYPE_DIMENSION_COLUMN || (ColumnType == TYPE_DIMENSION_MEMBER_SET && dimMemberSets != null && !dimMemberSets.isEmpty()))
			{
				if (ColumnType == TYPE_DIMENSION_COLUMN && dc == null)
					return null;
				String opvalue = Operand == null ? "" : Operand;
				opvalue = getDbSpecificOperandValue(dbType, opvalue);
				boolean appendTimeComponent = false;
				if (ColumnType == TYPE_DIMENSION_COLUMN)
				{
					appendTimeComponent = dc.DataType.equals("Date") || dc.DataType.equals("DateTime");
					DimensionColumn lowestLevelColumn = lowestLevelColumnMap.get(dc);
					if (lowestLevelColumn != null && lowestLevelColumn != dc)
					{
						int index = lowestLevelColumn.PhysicalName.lastIndexOf('.');
						String hierarchy = lowestLevelColumn.PhysicalName.substring(0, index);
						if (dbType == DatabaseType.MSASXMLA || dbType == DatabaseType.SAPBWXMLA || dbType == DatabaseType.MONDRIANXMLA)
							lhs = "ANCESTOR(" + hierarchy + ".CurrentMember," + dc.PhysicalName + ").Name";
						else
							lhs = "ANCESTOR(" + hierarchy + ".CurrentMember," + dc.PhysicalName + ").MEMBER_ALIAS";
					} else
					{
						int index = dc.PhysicalName.lastIndexOf('.');
						String hname = dc.PhysicalName.substring(0, index);
						if (dbType == DatabaseType.MSASXMLA || dbType == DatabaseType.SAPBWXMLA || dbType == DatabaseType.MONDRIANXMLA)
							lhs = hname + ".CurrentMember.Name";
						else
							lhs = hname + ".CurrentMember.MEMBER_ALIAS";
					}
				}
				else if (ColumnType == TYPE_DIMENSION_MEMBER_SET && dimMemberSets != null && !dimMemberSets.isEmpty())
				{
					for (DimensionMemberSetNav dimMemberSet : dimMemberSets)
					{
						if (dimMemberSet.displayName != null && dimMemberSet.displayName.equals(this.Column))
						{
							dms = dimMemberSet;
							appendTimeComponent = dms.dataType != null && (dms.dataType.equals("Date") || dms.dataType.equals("DateTime"));
							String dimHierarchy = dms.dimensionName;
							Hierarchy h = r.findHierarchy(dimHierarchy);
							String dName = h.OlapDimensionName;
							String hName = dimHierarchy.substring((h.OlapDimensionName + "-").length());
							lhs = "[" + dName + "].[" + hName + "].CurrentMember.Name";
							break;
						}
					}
				}
				if (Operator.equals(QueryFilter.LIKE) || Operator.equals(QueryFilter.NOTLIKE))
				{
					return getMDXLikeFilterString(dbType, Operator, lhs, opvalue);
				} else if (Operator.equals(QueryFilter.ISNOTNULL) || Operator.equals(QueryFilter.ISNULL))
				{
					String newOperator = "<>";
					if (Operator.equals(QueryFilter.ISNULL))
						newOperator = "=";
					result =  newOperator + '"' + '"';
				}
				else 
				{
					if((ColumnType == TYPE_DIMENSION_COLUMN && (dc.DataType.equals("Date") || dc.DataType.equals("DateTime")))
							|| (ColumnType == TYPE_DIMENSION_MEMBER_SET && dms.dataType != null && (dms.dataType.equals("Date") || dms.dataType.equals("DateTime"))))
					{
						if((ColumnType == TYPE_DIMENSION_COLUMN && dc.Format != null && !dc.Format.isEmpty()) 
								|| (ColumnType == TYPE_DIMENSION_MEMBER_SET && dms.format != null && !dms.format.isEmpty()))
						{
							String defaultFormat = DateUtil.DEFAULT_DATE_FORMAT;
							if((dc != null && dc.DataType.equals("DateTime")) || (dms != null && dms.dataType != null && dms.dataType.equals("DateTime")))
							{
								defaultFormat = DateUtil.DEFAULT_DATETIME_FORMAT;
							}
							SimpleDateFormat defaultsdf = new SimpleDateFormat(defaultFormat);
							defaultsdf.setLenient(false);
							Date dateOpValue = null;
							try
							{
								dateOpValue = defaultsdf.parse(opvalue);
							}
							catch(ParseException pe)
							{
								dateOpValue = null;
							}
							if(dateOpValue != null)
							{
								SimpleDateFormat sdf = new SimpleDateFormat(dc != null ? dc.Format : dms.format);
								opvalue = sdf.format(dateOpValue);
								appendTimeComponent = false;
							}
						}
						if(appendTimeComponent)
						{
							if ((dc != null && dc.DataType.equals("Date")) || (dms != null && dms.dataType != null && dms.dataType.equals("Date")))
							{
								opvalue += " 00:00:00.000";
							}
							else if ((dc != null && dc.DataType.equals("DateTime")) || (dms != null && dms.dataType != null && dms.dataType.equals("DateTime")))
							{
								opvalue += ".000";
							}
						}
					}
					if ((ColumnType == TYPE_DIMENSION_COLUMN && ("Integer".equals(dc.DataType) || "Float".equals(dc.DataType) || "Number".equals(dc.DataType)))
							|| (ColumnType == TYPE_DIMENSION_MEMBER_SET && dms.dataType != null && ("Integer".equals(dms.dataType) || "Float".equals(dms.dataType) || "Number".equals(dms.dataType))))
					{
						String conversionFunction = DatabaseConnection.getStrToNumberMDXFunction(dbType);
						String notNullOnLHSStr = lhs + "<>" + '"' + '"';
						String mdxFunctionOnLHSStr = conversionFunction+ "(" + lhs + ")";
						if (isAddNotNullCheck)
						{
							lhs = notNullOnLHSStr + " AND " + mdxFunctionOnLHSStr;
						}
						else
						{
							lhs =  mdxFunctionOnLHSStr;
						}
						result =  Operator + conversionFunction + "(\"" + opvalue.trim()  + "\")";
					} 
					else if ((ColumnType == TYPE_DIMENSION_COLUMN && (("Date".equals(dc.DataType) || "DateTime".equals(dc.DataType)) && (dbType == DatabaseType.MSASXMLA || dbType == DatabaseType.MONDRIANXMLA) && !appendTimeComponent))
							|| (ColumnType == TYPE_DIMENSION_MEMBER_SET && dms.dataType != null && (("Date".equals(dms.dataType) || "DateTime".equals(dms.dataType)) && (dbType == DatabaseType.MSASXMLA || dbType == DatabaseType.MONDRIANXMLA) && !appendTimeComponent)))
					{
						String conversionFunction = "Cdate";
						String notNullOnLHSStr = lhs + "<>" + '"' + '"';
						String mdxFunctionOnLHSStr = conversionFunction+ "(" + lhs + ")";
						if (isAddNotNullCheck)
						{
							lhs = notNullOnLHSStr + " AND " + mdxFunctionOnLHSStr;
						}
						else
						{
							lhs =  mdxFunctionOnLHSStr;
						}
						result =  Operator + conversionFunction + "(\"" + opvalue  + "\")";
					}
					else	
					{
						result = Operator + '"' + opvalue + '"';
					}
				}
			} else if (ColumnType == TYPE_MEASURE)
			{
				// If it hasn't navigated, return null
				if (mc == null)
					return null;
				lhs = "[Measures]." + mc.PhysicalName;
				if (Operator.equals(QueryFilter.ISNULL))
				{
					return "ISEMPTY(" + lhs + ")";
				}
				else if (Operator.equals(QueryFilter.ISNOTNULL))
				{
					return "NOT ISEMPTY(" + lhs + ")";
				}
				result = Operator + Operand;
			}
			return lhs + result;
		} else
		{
			/*
			 * If subfilters are dimension or logical operations, recursively evaluate
			 */
			int count = 0;
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < FilterList.size(); i++)
			{
				QueryFilter qf = (QueryFilter) FilterList.get(i);
				String s = qf.getMDXFilterString(dbType, lowestLevelColumnMap, dimColumnMap, (i == 0), dimMemberSets, r);
				if (s != null)
				{
					if (count == 0)
						sb.append(s);
					else if (Operator.equals("OR"))
						sb.append(" OR " + s);
					else
						sb.append(" AND " + s);
					count++;
				}
			}
			if (count > 1)
			{
				sb.insert(0, '(');
				sb.append(')');
			}
			if (sb.length() > 0)
				return (sb.toString());
		}
		return (null);
	}
	
	public String getMDXDimensionSet(DatabaseType dbType)
	{
		if (Type == TYPE_PREDICATE)
		{
			if (isPrompted() && QueryString.NO_FILTER_TEXT.equals(this.Operand))
				return null;
			if (ColumnType == TYPE_DIMENSION_COLUMN)
			{
				if (dc == null)
					return null;
				String opvalue = Operand == null ? "" : Operand;
				opvalue = getDbSpecificOperandValue(dbType, opvalue);
				return dc.PhysicalName + ".[" + opvalue + "]";
			}	
		} else
		{
			int count = 0;
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < FilterList.size(); i++)
			{
				QueryFilter qf = (QueryFilter) FilterList.get(i);
				String s = qf.getMDXDimensionSet(dbType);
				if (s != null)
				{
					if (count == 0)
						sb.append(s);
					else if (Operator.equals("OR"))
						sb.append("," + s);
					count++;
				}
			}
			if (sb.length() > 0)
				return (sb.toString());
		}
		return (null);
	}
	
	private String getMDXLikeFilterString(DatabaseType dbType, String operator, String lhs, String opvalue)
	{
		StringBuilder filterString=new StringBuilder();
		String result = null;
		if (QueryFilter.NOTLIKE.equalsIgnoreCase(operator))
		{
			filterString.append("NOT").append(" ");
		}
		if (opvalue.startsWith("%") && opvalue.endsWith("%"))
		{
			if (Operand.length() > 1)
			{
				if (dbType == DatabaseType.EssbaseXMLA)
				{
					result =  filterString.append("INSTR(1," + lhs + ",\"" + opvalue.substring(1, opvalue.length()-1) + "\")>0").toString();
				}
				else
				{
					result =  filterString.append("INSTR(" + lhs + ",\"" + opvalue.substring(1, opvalue.length()-1) + "\")>0").toString();
				}
			}
			else
				result =  filterString.append("1=1").toString();
		}
		else if (opvalue.startsWith("%"))
		{
			result =  filterString.append("RIGHT(" + lhs + "," + (opvalue.length()-1) + ")=\"" + opvalue.substring(1) + "\"").toString();	
		}
		else if (opvalue.endsWith("%"))
		{
			result =  filterString.append("LEFT(" + lhs + "," + (opvalue.length()-1) + ")=\"" + opvalue.substring(0, opvalue.length()-1) + "\"").toString();
		} else
		{
			if (dbType == DatabaseType.EssbaseXMLA)
			{
				result =  filterString.append("INSTR(1," + lhs + ",\"" + opvalue + "\")>0").toString();
			}
			else
			{
				result =  filterString.append("INSTR(" + lhs + ",\"" + opvalue + "\")>0").toString();
			}
		}
		return result;
	}
	
	private boolean quoted(String s)
	{
		if (s == null || s.isEmpty())
			return false;
		return (s.charAt(0) == '\'' && s.charAt(s.length() - 1) == '\'');
	}

	private String getOuterFilterString(Query q) throws SyntaxErrorException
	{
		// If it's a predicate, search for appropriate column and set it
		if (Type == TYPE_PREDICATE)
		{
			if (isPrompted() && QueryString.NO_FILTER_TEXT.equals(this.Operand))
				return null;
			String op = Operator;
			if (Operator.equals(QueryFilter.LIKE) || Operator.equals(QueryFilter.NOTLIKE) || Operator.equals(QueryFilter.IN) || Operator.equals(QueryFilter.NOTIN))
				op = ' ' + Operator + ' ';
			if (Operator.equals(QueryFilter.ISNULL) || Operator.equals(QueryFilter.ISNOTNULL))
				op = ' ' + Operator;

			/* Must drop dimension columns in outer join for SQLServer - bug in SQL Server */
			DatabaseConnection dbc = q.getConnections().get(0);
			Database.DatabaseType dbType = dbc.DBType;
			String operand = getOperand(q.getRepository(), q.getSession(), Operand);
			if ((ColumnType == TYPE_DIMENSION_COLUMN) && !((dbType == DatabaseType.MSSQL) && q.isFullOuterJoin()))
			{
				// Get the query column to determine type
				QueryColumn qc = q.findDCProjectionListItem(Column);
				if (qc == null)
					return (null);
				DimensionColumn dc = (DimensionColumn) qc.o;
				if (Operator.equals(QueryFilter.IN) || Operator.equals(QueryFilter.NOTIN))
				{
					if (dc.DataType.equals("Number") || dc.DataType.equals("Integer") || dc.DataType.equals("Float"))
						return (q.getProjectionListItemString(Column) + op + "(" + operand + ")");
					else
						return (q.getProjectionListItemString(Column) + op + "('" + operand.replaceAll(",", "','") + "')");
				} else
				{
					if (dc.DataType.equals("Number") || dc.DataType.equals("Integer") || dc.DataType.equals("Float"))
					{
						if (Operator.equals(QueryFilter.LIKE) || Operator.equals(QueryFilter.NOTLIKE))
							throw new SyntaxErrorException("LIKE and NOT LIKE operators are not allowed on numeric values");
						return (q.getProjectionListItemString(Column) + op + ((operand == null) ? "" : operand));
					} else
					{
						return (q.getProjectionListItemString(Column) + op + ((operand == null) ? "" : dbc.getQuotedLiteral(operand, dc.DataType)));
					}
				}
			} else if (ColumnType == TYPE_MEASURE)
			{
				if (Operator.equals(QueryFilter.LIKE) || Operator.equals(QueryFilter.NOTLIKE))
					throw new SyntaxErrorException("LIKE and NOT LIKE operators are not allowed on numeric values");
				// If it's a derived measure, then it's not navigated
				if (mc == null)
					return ("(" + q.getDerivedMeasureString(Column) + op + ((operand == null) ? "" : operand) + ")");
				else
					return ("(" + q.getProjectionListItemString(Column) + op + ((operand == null) ? "" : operand) + ")");
			}
		} else
		{
			/*
			 * If subfilters are dimension or logical operations, recursively evaluate
			 */
			int count = 0;
			StringBuilder sb = new StringBuilder();
			for (QueryFilter qf : FilterList)
			{
				String s = qf.getOuterFilterString(q);
				if (s != null)
				{
					if (count == 0)
					{
						sb.append(s);
					} else if (count == 1)
					{
						sb.append(' ');
						sb.append(Operator);
						sb.append(' ');
						sb.append(s);
						sb.append(')');
						sb.insert(0, "(");
					} else if (count > 1)
					{
						sb.deleteCharAt(sb.length() - 1);
						sb.append(' ');
						sb.append(Operator);
						sb.append(' ');
						sb.append(s);
						sb.append(')');
					}
					count++;
				}
			}
			if (sb.length() > 0)
				return (sb.toString());
		}
		return (null);
	}

	/**
	 * Return where clause filter string for a given query (ignores filters on measures - those will be returned in the
	 * having clause)
	 * 
	 * @param q
	 *            Query
	 * @return
	 * @throws SyntaxErrorException 
	 */
	public String getWhereFilterString(DatabaseConnection dbc, boolean aggregate, Query q, QueryMap tableMap, Navigation nav, List<String> lookupJoins) throws SyntaxErrorException
	{
		if (!containsMeasure())
			return (getFilterString(dbc, aggregate, q, null, null, tableMap, nav, lookupJoins));
		else
			return (null);
	}

	/**
	 * Get having clause filter string for measures (if a filter contains a measure, then it must be placed in the
	 * having clause)
	 * 
	 * @param q
	 *            Query
	 * @param mt
	 *            Measure table to get filters for - ignore other measures
	 * @param melist
	 *            Measure expression list
	 * @param outerJoin
	 *            Whether or not this filter is being used in an outer join query (if so, all havings must be applied at
	 *            the outer query)
	 * @return
	 * @throws SyntaxErrorException 
	 */
	public String getHavingFilterString(DatabaseConnection dbc, Query q, MeasureTable mt, List<String> melist, boolean outerJoin, QueryMap tableMap) throws SyntaxErrorException
	{
		if (containsMeasure() && !containsMeasureNotNavigated(mt) && !containsDerivedMeasure(q)
		// Optimization to push down measure-only filters when federating subqueries - do not push down is null or is not null
				&& (!outerJoin || (Type == QueryFilter.TYPE_PREDICATE && dbc.isFederateOuterjoinSubqueries()) &&
					!Operator.equalsIgnoreCase(QueryFilter.ISNULL) && !Operator.equalsIgnoreCase(QueryFilter.ISNOTNULL)))
		{
			includedInPhysicalQuery = true;
			return getFilterString(dbc, true, q, mt, melist, tableMap, null, null);
		}
		else
			return (null);
	}

	/**
	 * Get outer having clause filter string for measures (if a filter contains a measure, then it must be placed in the
	 * having clause)
	 * 
	 * @param q
	 *            Query
	 * @param outerJoin
	 *            Whether or not this filter is being used in an outer join query (if so, all havings must be applied at
	 *            the outer query)
	 * @return
	 * @throws SyntaxErrorException 
	 */
	public String getOuterQueryFilterString(Query q, boolean outerJoin) throws SyntaxErrorException
	{
		if (pushDown)
			return (null);
		List<MeasureTable> mtList = new ArrayList<MeasureTable>();
		getMeasureTablesNavigated(mtList);
		if (containsDerivedMeasure(q) || (containsMeasure() && outerJoin) || (mtList.size() > 1))
		{
			return (getOuterFilterString(q));
		} else
			return (null);
	}

	/**
	 * Return a version of a filter that strips out measures when the filter can be pushed from an outer to an inner
	 * filter
	 * 
	 * @param q
	 * @return
	 */
	public QueryFilter getPushDownFilter(Query q)
	{
		// If it's a predicate, search for appropriate column and set it
		if (Type == TYPE_PREDICATE)
		{
			/* Must drop dimension columns in outer join for SQLServer - bug in SQL Server */
			if ((ColumnType == TYPE_DIMENSION_COLUMN) && !(q.isFullOuterJoin() && (q.getConnections().get(0).DBType == DatabaseType.MSSQL)))
			{
				return (this);
			} else if (ColumnType == TYPE_MEASURE)
			{
				return (null);
			}
		} else
		{
			/*
			 * If subfilters are dimension or logical operations, recursively evaluate
			 */
			List<QueryFilter> newList = new ArrayList<QueryFilter>(FilterList.size());
			for (QueryFilter qf : FilterList)
			{
				if (qf.Type == TYPE_PREDICATE)
				{
					if (qf.ColumnType == TYPE_DIMENSION_COLUMN)
						newList.add(qf);
					else if (qf.ColumnType == TYPE_MEASURE)
					{
						if (!Operator.equals("AND"))
							return (null);
					}
				} else
				{
					QueryFilter child = qf.getPushDownFilter(q);
					if (child == null)
						return (null);
					else
						newList.add(child);
				}
			}
			if (newList.size() == 1)
				return (newList.get(0));
			QueryFilter newqf = new QueryFilter();
			newqf.Operator = Operator;
			newqf.Type = Type;
			newqf.ColumnType = ColumnType;
			newqf.promptName = promptName;
			newqf.FilterList = newList;
			newqf.pushDown = true;
			return (newqf);
		}
		return (null);
	}
	
	/*
	 * just like Q{...}
	 */
	public String getQueryKeyStrForTree(Tree tree, Set<Variable> vlist) throws ScriptException
	{
		String str = Statement.toString(tree, null);
		Statement.extractPrompts(tree, vlist);
		return str;
	}

	/**
	 * Return a unique string key to represent this filter in the overall query key for cache keying
	 * 
	 * @param q
	 *            Current query - needed if navigation is required to identify columns for potential session variable
	 *            replacement
	 * @param vlist
	 *            List of session variables. Add any session variables present in the filter
	 * @return
	 * @throws NavigationException
	 * @throws ScriptException 
	 */
	public String getQueryKeyStr(Query q, Set<Variable> vlist) throws NavigationException, CloneNotSupportedException, BadColumnNameException, ScriptException
	{
		// If it's a predicate, search for appropriate column and set it
		if (Type == TYPE_PREDICATE)
		{
			if (ColumnType == TYPE_DIMENSION_COLUMN)
			{
				if (q != null && q.getRepository().hasVariables(Dimension, Column))
				{
					/*
					 * If this column potentially contains a session variable, then navigate the query to find which
					 * physical incarnation is being used. If that version contains a session variable, then add it to
					 * the query key
					 */
					if (!q.hasBeenNavigated())
						q.navigateQuery(false, true, null, true);
					if (dc != null && dc.SessionVariables != null && vlist != null)
						vlist.addAll(dc.SessionVariables);
				}
				if (QueryString.NO_FILTER_TEXT.equals(Operand))
					return null;
				
				StringBuilder sb = new StringBuilder("F{D{");
				sb.append(Dimension);
				sb.append("}C{");
				sb.append(Column);
				sb.append("}OP{");
				sb.append(Operator);
				sb.append("}V{");
				sb.append(getDimensionOperandKey(q, vlist));
				sb.append("}}");
				return sb.toString();
			} else if (ColumnType == TYPE_DIMENSION_MEMBER_SET)
			{
				StringBuilder sb = new StringBuilder("F{DM{");
				sb.append(Column);
				sb.append("}OP{");
				sb.append(Operator);
				sb.append("}V{");
				sb.append(getDimensionOperandKey(q, vlist));
				sb.append("}}");
				return sb.toString();
			} else if (ColumnType == TYPE_MEASURE)
			{
				if (q != null && q.getRepository().hasVariables(null, Column))
				{
					/*
					 * If this column potentially contains a session variable, then navigate the query to find which
					 * physical incarnation is being used. If that version contains a session variable, then add it to
					 * the query key
					 */
					if (!q.hasBeenNavigated())
						q.navigateQuery(false, true, null, true);
					if (mc.SessionVariables != null && vlist != null)
						vlist.addAll(mc.SessionVariables);
				}
				if (QueryString.NO_FILTER_TEXT.equals(Operand))
					return null;
				StringBuilder sb = new StringBuilder("F{M{");
				sb.append(Column);
				sb.append("}OP{");
				sb.append(Operator);
				sb.append("}V{");
				String operand = (q == null) ? Operand : getOperand(q.getRepository(), q.getSession(), Operand);
				sb.append(operand);
				sb.append('}');
				if (MeasureFilter == null)
				{
					sb.append('}');
				} else
				{
					sb.append("MF{");
					sb.append(MeasureFilter.getQueryKeyStr(q, vlist));
					sb.append("}}");
				}
				return sb.toString();
			}
		} else
		{
			StringBuilder sb = new StringBuilder();
			sb.append('F');
			sb.append(Operator);
			sb.append('{');
			// build up array of filters, and then sort
			List<String> filters = new ArrayList<String>();
			for (QueryFilter qf : FilterList)
			{
				String s = qf.getQueryKeyStr(q, vlist);
				if (s != null)
					filters.add(s);
			}
			Collections.sort(filters);
			boolean comma = false;
			for (String filter : filters)
			{
				if (comma)
					sb.append(',');
				sb.append(filter);
				comma = true;
			}
			sb.append('}');
			return (sb.toString());
		}
		return (null);
	}
	
	private StringBuilder getDimensionOperandKey(Query q, Set<Variable> vlist) throws ScriptException, CloneNotSupportedException
	{
		StringBuilder sb = new StringBuilder();
		if (Operand != null)
		{
			String operand = (q == null) ? Operand : getOperand(q.getRepository(), q.getSession(), Operand);
			if (Operator.equals("IN") && operand.startsWith("Q{"))
			{
				AbstractQueryString aqs = null;
				Repository r = q.getRepository();
				String op = QueryString.preProcessQueryString(operand.substring(2, operand.length()-1), r, q.getSession());
				Query oq;
				try
				{
					if (op.toLowerCase().startsWith("select"))
					{
						aqs = new LogicalQueryString(r, q.getSession(), op);
						oq = aqs.getQuery();
					} else
					{
						aqs = new QueryString(r);
						oq = aqs.processQueryString(op, q.getSession());
						sb.append(oq.getKey(q.getSession()));
					}
					for(DimensionColumnNav dcn: oq.dimensionColumns)
					{
						if (r.hasVariables(dcn.dimensionName, dcn.columnName))
						{
							DimensionColumn dc = r.findDimensionColumn(dcn.dimensionName, dcn.columnName);
							if (dc.SessionVariables != null)
								vlist.addAll(dc.SessionVariables);
						}
					}
					for(MeasureColumnNav mcn: oq.measureColumns)
					{
						if (r.hasVariables(null, mcn.measureName))
						{
							MeasureColumn mc = r.findMeasureColumn(mcn.measureName);
							if (mc.SessionVariables != null)
								vlist.addAll(mc.SessionVariables);
						}
					}
				} catch (BaseException ex)
				{
					logger.warn("Unable to parse query string: " + op);
				} catch (SQLException ex)
				{
					logger.warn("Unable to parse query string: " + op);
				} catch (IOException ex)
				{
					logger.warn("Unable to parse query string: " + op);
				}
			} else
				sb.append(operand.replace('\'', '_'));
		}
		else if (tOperand != null)
			sb.append(getQueryKeyStrForTree(tOperand, vlist));
		else
			sb.append("null");
		return sb;
	}

	/**
	 * Return the LOGICAL query string equivalent of this filter
	 * 
	 * @param substitutePrompts
	 *            Whether to substitute prompts for operands
	 * @return
	 */
	public String getQueryString(boolean substitutePrompts)
	{
		// If it's a predicate, search for appropriate column and set it
		if (Type == TYPE_PREDICATE)
		{
			String op = Operator;
			String opValue = Operand == null ? "" : Operand.replaceAll("\\\\", "\\\\\\\\");
			if (Operator.equals(QueryFilter.LIKE) || Operator.equals(QueryFilter.NOTLIKE) || Operator.equals(QueryFilter.IN) || Operator.equals(QueryFilter.NOTIN))
				op = ' ' + Operator + ' ';
			if (Operator.equals(QueryFilter.ISNULL) || Operator.equals(QueryFilter.ISNOTNULL))
				op = ' ' + Operator;
			if (ColumnType == TYPE_DIMENSION_COLUMN)
			{
				if (substitutePrompts && promptName != null && Util.hasNonWhiteSpaceCharacters(promptName))
					return ("FDC{" + Dimension + "." + Column + op + "\"+$P{" + promptName + "}+\"}");
				else
					return ("FDC{" + Dimension + "." + Column + op + opValue + "}");
			} else if (ColumnType == TYPE_MEASURE)
			{
				if (MeasureFilter == null)
				{
					if (substitutePrompts && promptName != null && Util.hasNonWhiteSpaceCharacters(promptName))
					{
						return ("FM{" + Column + op + "\"+$P{" + promptName + "}+\"}");
					} else
						return ("FM{" + Column + op + opValue + "}");
				} else
				{
					if (substitutePrompts && promptName != null && Util.hasNonWhiteSpaceCharacters(promptName))
					{
						return ("FM{" + Column + op + "\"+$P{" + promptName + "}+\"," + MeasureFilter.getQueryString(substitutePrompts) + "}");
					} else
						return ("FM{" + Column + op + opValue + "," + MeasureFilter.getQueryString(substitutePrompts) + "}");
				}
			}
		} else if (Type == TYPE_INPUT_TEXT) {
			return (inputText);
		}
		else
		{
			StringBuilder sb = new StringBuilder();
			sb.append('F');
			sb.append(Operator);
			sb.append('{');
			for (int i = 0; i < FilterList.size(); i++)
			{
				QueryFilter qf = (QueryFilter) FilterList.get(i);
				if (i > 0)
					sb.append(',');
				sb.append(qf.getQueryString(substitutePrompts));
			}
			sb.append('}');
			return (sb.toString());
		}
		return (null);
	}

	/**
	 * generate the code for getting a prompt value
	 */
	
	public static String getPromptValueString(String promptName, String dataType)
	{
		String promptValue = "GETPROMPTVALUE('" + promptName + "')";
		if (dataType == null)
			return promptValue;
		// GETPROMPTVALUE returns a string, see if we need to cast
		if (dataType.equals("Integer"))
			promptValue = "INTEGER(" + promptValue + ')';
		else if (dataType.equals("Float"))
			promptValue = "FLOAT(" + promptValue + ')';
		else if (dataType.equals("DateTime") || dataType.equals("Date")) // XXX there is not currently a DATE cast
			promptValue = "DATETIME(" + promptValue + ')';
		return promptValue;
	}

	/**
	 * Return the new logical query string equivalent of this filter
	 * 
	 * @param substitutePrompts
	 *            Whether to substitute prompts for operands
	 * @return
	 * @throws ScriptException 
	 */
	public String getNewQueryString(boolean substitutePrompts)throws ScriptException
	{
		return getNewQueryString(substitutePrompts, false);
	}
	
	public String getNewQueryString(boolean substitutePrompts, boolean isSubReport) throws ScriptException
	{
		// If it's a predicate, search for appropriate column and set it
		if (Type == TYPE_PREDICATE)
		{
			String op = Operator;
			if (Operator.equals(QueryFilter.LIKE) || Operator.equals(QueryFilter.NOTLIKE)
					|| Operator.equals(QueryFilter.IN) || Operator.equals(QueryFilter.NOTIN) 
					|| Operator.equals(QueryFilter.ISNULL) || Operator.equals(QueryFilter.ISNOTNULL))
				op = ' ' + Operator + ' ';
			if (ColumnType == TYPE_DIMENSION_COLUMN)
			{
				String operand = Operand;
				if (dc != null && ((operand != null) || (tOperand != null)))
				{
					operand = getOperandString(dc.DataType, isSubReport);
				}
				if (substitutePrompts && promptName != null && Util.hasNonWhiteSpaceCharacters(promptName))
				{
					// need to properly 'quote' the prompt value
					return ("[" + Dimension + "." + Column + "]" + op + getPromptValueString(promptName, (dc != null ? dc.DataType : dataType)));
				}
				else
				{
					return getDimFilterQueryString(operand, op);
				}
			} else if (ColumnType == TYPE_DIMENSION_MEMBER_SET)
			{
				String operand = Operand;
				if (operand != null || tOperand != null)
				{
					operand = getOperandString("Varchar", isSubReport);//XXX use datatype of DimensionMemberSet
				}
				return getDimFilterQueryString(operand, op);
			} else if (ColumnType == TYPE_MEASURE)
			{
				if (MeasureFilter == null)
				{
					if (substitutePrompts && promptName != null && Util.hasNonWhiteSpaceCharacters(promptName))
					{
						return ("[" + Column + "]" + op + getPromptValueString(promptName, mc.DataType));
					} else if(Operator.equals(QueryFilter.ISNULL) || Operator.equals(QueryFilter.ISNOTNULL))
					{
						return ("[" + Column + "]" + op);
					}else 
						return ("[" + Column + "]" + op + Operand);
				} else
				{
					if (substitutePrompts && promptName != null && Util.hasNonWhiteSpaceCharacters(promptName))
					{
						return ("[" + Column + "]" + op + getPromptValueString(promptName , mc.DataType) + " WHERE " + MeasureFilter.getNewQueryString(substitutePrompts));
					} else
						return ("[" + Column + "]" + op + Operand + " WHERE " + MeasureFilter.getNewQueryString(substitutePrompts));
				}
			}
		}
		else if (Type == TYPE_INPUT_TEXT) {
			return ("(" + inputText + ")");
		}
		else
		{
			StringBuilder sb = new StringBuilder();
			sb.append('(');
			boolean first = true;
			for (int i = 0; i < FilterList.size(); i++)
			{
				QueryFilter qf = (QueryFilter) FilterList.get(i);
				String newFilter = qf.getNewQueryString(substitutePrompts, isSubReport);
				if (newFilter == null)
					continue;
				
				if (!first)
					sb.append(" " + Operator + " ");
				first = false;
				sb.append(newFilter);
			}
			sb.append(')');
			return (sb.toString());
		}
		return (null);
	}
	
	private String getOperandString(String datatype, boolean isSubReport) throws ScriptException
	{
		String operand = Operand;
		if (datatype != null && (Operand != null || tOperand != null))
		{
			if (tOperand != null)
			{
				operand = '(' + Statement.toString(tOperand, null) + ')';
			} else if (Operator.equals(QueryFilter.IN) || Operator.equals(QueryFilter.NOTIN))
				// XXX ugly hack, really should pass query objects or parse trees around, as in above, or make as a report filter
				if (operand.toLowerCase().startsWith("select "))
				{
					operand = '(' + operand + ')';
				}
				else
				{
				operand = '(' + quoteInOperands(operand, datatype) + ')';
				}
			else {
				if (operand.isEmpty() && datatype != null && (datatype.equals("Date") || datatype.equals("DateTime"))) 
					return null;
				operand = quoteOperand(operand, datatype, isSubReport);
			}
		}
		return operand;
	}
	
	private String getDimFilterQueryString(String operand, String op)
	{
		String val = operand;
		if (operand == null)
		{
			val = "";
		}
		else if ((Operator.equals(QueryFilter.IN) || Operator.equals(QueryFilter.NOTIN)) && !operand.startsWith("("))
		{
			val = '(' + operand + ')';
		}
		if(Operator.equals(QueryFilter.ISNULL) || (Operator.equals(QueryFilter.ISNOTNULL)))
			return ((dc != null ? "[" + Dimension + "." + Column + "]" : "[" + Column + "]") + op);
		
		return ((dc != null ? "[" + Dimension + "." + Column + "]" : "[" + Column + "]") + op + val);
	}
	
	/**
	 * take a comma separated list (as a string), quote the items, and pack back into a string
	 * @param operand
	 * @param dataType
	 * @return
	 */
	public static String quoteInOperands(String operand, String dataType)
	{
		if (operand == null)
		{
			return null;
		}
		StringBuilder sb = new StringBuilder();
		StringTokenizer st = new StringTokenizer(operand, ",");
		while (st.hasMoreTokens())
		{
			if (sb.length() > 0)
			{
				sb.append(',');
			}
			String item = st.nextToken();
			sb.append(quoteOperand(item, dataType));
		}
		return sb.toString();
	}
	
	public static String quoteOperand(String operand, String dataType)
	{
		return quoteOperand(operand, dataType, false);
	}
	
	// in the case of subreport, we need to extra escape the backslashes
	public static String quoteOperand(String operand, String dataType, boolean isSubReport)
	{
		if (dataType != null && (dataType.equals("Date") || dataType.equals("DateTime"))) {
			if (operand.isEmpty())
				return operand;
			
			return "#" + operand + "#";
		}
		else if ((dataType != null && dataType.equals("Varchar")) || QueryString.NO_FILTER_TEXT.equals(operand))
			return '\'' + (isSubReport ? Util.quoteForSubReport(operand) : Util.quote(operand)) + '\'';
		return operand;
	}

	/**
	 * Return the string equivalent of this filter
	 * 
	 * @param includeColumnName
	 *            Flag indicating whether to include column names or just filter values
	 * @return
	 */
	public String getString(boolean includeColumnName)
	{
		// If it's a predicate, search for appropriate column and set it
		if (Type == TYPE_PREDICATE)
		{
			if (includeColumnName)
				if (MeasureFilter == null)
					return ((ColumnType == TYPE_DIMENSION_COLUMN ? Dimension + "_" : "") + Column + Operator + (this.isPrompted() ? "(" + this.getPromptName()
							+ ")" : Operand));
				else
					return (Column + "(" + MeasureFilter.getString(includeColumnName) + ")" + Operator + (this.isPrompted() ? "(" + this.getPromptName() + ")"
							: Operand));
			else
				return (Operand);
		} else if (Type == TYPE_INPUT_TEXT) {
			return this.inputText;
		} else
		{
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < FilterList.size(); i++)
			{
				QueryFilter qf = (QueryFilter) FilterList.get(i);
				if (i > 0)
				{
					sb.append(' ');
					sb.append(Operator);
					sb.append(' ');
				}
				sb.append(qf.getString(includeColumnName));
			}
			sb.append('}');
			return (sb.toString());
		}
	}

	public String toString()
	{
		return (getString(true));
	}

	/**
	 * Return the name of a filter if it contains this filter as a measure filter
	 * 
	 * @param measure
	 *            Name of measure
	 * @return
	 */
	public String getMeasureFilterName(String measure)
	{
		if (this.isPrompted() == false)
		{
			return (measure + ": " + getString(false));
		}
		return measure;
	}

	/**
	 * Return a map of parameter names and default values
	 * 
	 * @return
	 */
	public void getParameterMap(Map<String, Object> parameterMap)
	{
		if (Type == TYPE_INPUT_TEXT)
			return;
		
		if (Type == TYPE_PREDICATE)
		{
			if (isPrompted())
			{
				parameterMap.put(promptName, Operand);
				if (MeasureFilter != null)
					MeasureFilter.getParameterMap(parameterMap);
			}
			return;
		} else
		{
			for (QueryFilter qf : FilterList)
			{
				qf.getParameterMap(parameterMap);
			}
			return;
		}
	}

	/**
	 * Return the string equivalent of a filter suitable for display to a user
	 * 
	 * @param q
	 * @return
	 */
	public String getDisplayString(Repository r)
	{
		// If it's a predicate, search for appropriate column and set it
		if (Type == TYPE_PREDICATE)
		{
			String value = Operand;
			if (Operator.equals(QueryFilter.LIKE) || Operator.equals(QueryFilter.NOTLIKE))
			{
				value = value.replace('%', '*').replace('_', '?');
			}
			if (ColumnType == TYPE_DIMENSION_COLUMN)
			{
				return (Dimension + "_" + Column + " " + Operator + " " + value);
			} else if (ColumnType == TYPE_MEASURE)
			{
				MeasureColumn mc = r.findMeasureColumn(Column);
				if (MeasureFilter == null)
				{
					if (mc != null && mc.Format != null)
					{
						if (Operand != null && (Operand.equals(QueryString.NO_FILTER_TEXT)))
						{
							return (Column + " " + Operator + " " + value);
						}
						if (Operand == null)
						{
							return Column + " " + Operator;
						}
						DecimalFormat df = new DecimalFormat(mc.Format);
						return (Column + " " + Operator + " " + df.format(Double.valueOf(Operand)));
					} else
						return (Column + " " + Operator + " " + value);
				} else
				{
					if (mc != null && mc.Format != null)
					{
						if (Operand != null && (Operand.equals(QueryString.NO_FILTER_TEXT)))
						{
							return (Column + " " + Operator + " " + value);
						}
						if (Operand == null)
						{
							return Column + " " + Operator;
						}
						DecimalFormat df = new DecimalFormat(mc.Format);
						return (Column + "(" + MeasureFilter.getDisplayString(r) + ") " + Operator + " " + df.format(Double.valueOf(Operand)));
					} else
						return (Column + "(" + MeasureFilter.getDisplayString(r) + ") " + Operator + " " + value);
				}
			}
		} else
		{
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < FilterList.size(); i++)
			{
				QueryFilter qf = (QueryFilter) FilterList.get(i);
				if (i > 0)
				{
					if (Operator.equals("OR"))
						sb.append(';');
					sb.append(' ');
					sb.append(Operator.toLowerCase());
					sb.append(' ');
				}
				sb.append(qf.getDisplayString(r));
			}
			return (sb.toString());
		}
		return (null);
	}

	/**
	 * Extract columns from filter for later navigation
	 * 
	 * @param q
	 *            Query to add extracted columns to
	 * @param dimensionColumns
	 *            Optional list of dimension columns to extract into (to be used instead of adding extracted dimension
	 *            columns to a query)
	 * @param containsMeasure
	 *            Whether this filter contains a measure (only used in recursion)
	 * @param containsDerivedMeasure
	 *            Whether the filter contains a derived measure (only used in recursion)
	 * @param measureFilter
	 *            Whether this is being extracted for a measure filter
	 * @throws NavigationException
	 */
	public void extractNavigationColumns(Query q, List<DimensionColumnNav> dimensionColumns, Boolean containsMeasure, Boolean containsDerivedMeasure)
			throws NavigationException, BadColumnNameException
	{
		if ((containsMeasure == null) || (containsDerivedMeasure == null))
		{
			containsMeasure = containsMeasure();
			containsDerivedMeasure = containsDerivedMeasure(q);
		}
		// If it's a predicate, search for appropriate column and set it
		if (Type == TYPE_PREDICATE)
		{
			if (ColumnType == TYPE_DIMENSION_COLUMN)
			{
				DimensionColumnNav dcn = dimensionColumns == null ? q.findDimensionColumnNav(Dimension, Column) : Query.findDimensionColumnNav(
						dimensionColumns, Dimension, Column);
				if (dcn == null)
				{
					if (dimensionColumns == null)
					{
						dcn = q.addNavigationDimensionColumn(Dimension, Column);
					} else
					{
						dcn = Query.createNewDimensionColumn(Dimension, Column, Column, -1);
						dimensionColumns.add(dcn);
					}
					dcn.navigateonly = true;
				}
				return;
			} else if (ColumnType == TYPE_MEASURE)
			{
				if (MeasureFilter != null)
				{
					MeasureColumn mc = q.getRepository().findMeasureColumn(Column);
					if (mc == null)
					{
						throw new BadColumnNameException(Column);
					}
					if (mc.MeasureTable.Type == MeasureTable.DERIVED)
					{
						throw new NavigationException("Derived measure " + Column + " can not be used with filtered metric.");
					}
				}
				if (dimensionColumns == null)
				{
					MeasureColumnNav mcn = null;
					if (MeasureFilter != null)
						mcn = q.findMeasureColumnNav(MeasureFilter.getMeasureFilterName(Column));
					else
						mcn = q.findMeasureColumnNav(Column);
					if (mcn == null)
					{
						/*
						 * Add as hidden (not just navigate-only) because need to make sure the appropriate joins are
						 * included. Hidden adds to query column list - ensuring that the measure table is added to the
						 * query. (navigate-only can lead to queries that don't include any measure (if filter is for
						 * measure not in query - e.g. query for one measure, filter for another)
						 * 
						 * Extract any columns that need to be extracted from the measure filter
						 */
						if (MeasureFilter != null)
							q.addMeasureColumn(Column, Column, true, MeasureFilter);
						else
						{
							mcn = q.addMeasureColumn(Column, Column, true);
							if (mcn == null)
								throw (new BadColumnNameException(Column));
						}
					}
				}
			}
		}
		else if (Type == TYPE_INPUT_TEXT) {
			// probably need to do something, but don't know what
			return;
		}
		else
		{
			for (QueryFilter qf : FilterList)
			{
				qf.extractNavigationColumns(q, dimensionColumns, containsMeasure, containsDerivedMeasure);
			}
			return;
		}
	}

	/**
	 * Generate list of all Query objects contained in this filter.
	 * 
	 * @param r
	 *            Repository
	 * @param qoSet
	 *            Set to save Query Objects into
	 */
	public void generateQueryObjectList(Repository r, Set<Object> qoSet) throws Exception
	{
		// If it's a predicate, search for appropriate column and set it
		if (Type == TYPE_PREDICATE)
		{
			if (ColumnType == TYPE_MEASURE)
			{
				MeasureColumn mc = r.findMeasureColumn(Column);
				if (mc == null)
				{
					logger.fatal("Measure column does not exist: " + Column);
					throw new BadColumnNameException(Column);
				}
				qoSet.add(mc);
				if (MeasureFilter != null)
					MeasureFilter.generateQueryObjectList(r, qoSet);
			} else if (ColumnType == TYPE_DIMENSION_COLUMN)
			{
				qoSet.add(r.findDimensionColumn(Dimension, Column));
			}
		} else
		{
			for (QueryFilter qf : FilterList)
			{
				qf.generateQueryObjectList(r, qoSet);
			}
		}
	}

	/**
	 * Returns a display filter based on the current query filter. Assumes display names are the same as column names in
	 * the query filter
	 * 
	 * @throws NavigationException
	 */
	public DisplayFilter returnDisplayFilter(Repository r, Session session)
	{
		// If it's a predicate, search for appropriate column and set it
		if (Type == TYPE_INPUT_TEXT) {
			return null;
		}
		if (Type == TYPE_PREDICATE)
		{
			if (MeasureFilter != null)
			{
				logger.warn("Unable to convert filter to display filter, contains a filtered measure: " + getQueryString(false));
				return null;
			}
			/*
			 * Replace any variables if needed
			 */
			String operand = r.replaceVariables(null, Operand);
			if (session != null)
				operand = r.replaceVariables(session, operand);
			DisplayFilter df = new DisplayFilter(Dimension, Column, Operator, operand);
			df.setDisplayName(false);
			return (df);
		} else
		{
			List<DisplayFilter> dfList = new ArrayList<DisplayFilter>();
			for (QueryFilter qf : FilterList)
			{
				DisplayFilter df2 = qf.returnDisplayFilter(r, session);
				if (df2 == null)
					return null;
				dfList.add(df2);
			}
			DisplayFilter df = new DisplayFilter(Operator, dfList);
			df.setDisplayName(false);
			return (df);
		}
	}

	/**
	 * @return Returns the name.
	 */
	public String getName()
	{
		return Name;
	}

	/**
	 * @param name
	 *            The name to set.
	 */
	public void setName(String name)
	{
		Name = name;
	}

	/**
	 * @return Returns the columnType.
	 */
	public int getColumnType()
	{
		return ColumnType;
	}

	public void setColumnType(int columnType) {
		ColumnType = columnType;
	}

	/**
	 * @return Returns the type.
	 */
	public int getType()
	{
		return Type;
	}

	/**
	 * @return Returns the operator.
	 */
	public String getOperator()
	{
		return Operator;
	}

	/**
	 * @return Returns the column.
	 */
	public String getColumn()
	{
		return Column;
	}

	/**
	 * @return Returns the dimension.
	 */
	public String getDimension()
	{
		return Dimension;
	}

	/**
	 * @return Returns the operand.
	 */
	public String getOperand()
	{
		return Operand;
	}

	/**
	 * Returns whether the filter contains a given attribute
	 * 
	 * @param dimension
	 * @param column
	 * @return
	 */
	public boolean containsAttribute(String dimension, String column)
	{
		if ((Type == QueryFilter.TYPE_PREDICATE) && (ColumnType == QueryFilter.TYPE_DIMENSION_COLUMN) && (Dimension.equals(dimension) && Column.equals(column)))
		{
			return (true);
		} else if (Type == QueryFilter.TYPE_LOGICAL_OP)
		{
			for (QueryFilter qf : FilterList)
			{
				if (qf.containsAttribute(dimension, column))
					return (true);
			}
		}
		return (false);
	}
	
	public boolean containsOlapDimension(String dimension) {
		if ((Type == QueryFilter.TYPE_PREDICATE) && (ColumnType == QueryFilter.TYPE_DIMENSION_MEMBER_SET) && Dimension.equals(dimension)){
			return true;
		}
		else if (Type == QueryFilter.TYPE_LOGICAL_OP) {
			for (QueryFilter qf : FilterList)
			{
				if (qf.containsOlapDimension(dimension))
					return (true);
			}
		}
		return (false);
	}
	
	/**
	 * Replaces a generic column name in a filter
	 * 
	 * @param dimension
	 * @param column
	 * @return
	 */
	public void replaceGenericAttribute(String dimension, String genericName, String column)
	{
		if ((Type == QueryFilter.TYPE_PREDICATE) && (ColumnType == QueryFilter.TYPE_DIMENSION_COLUMN)
				&& (Dimension.equals(dimension) && Column.equals(genericName)))
		{
			Column = column;
		} else if (Type == QueryFilter.TYPE_LOGICAL_OP)
		{
			for (QueryFilter qf : FilterList)
			{
				qf.replaceGenericAttribute(dimension, genericName, column);
			}
		}
	}

	/**
	 * Returns whether the filter contains a given attribute
	 * 
	 * @param dimension
	 * @param column
	 * @return
	 */
	public boolean containsMeasure(String measurename)
	{
		if ((Type == QueryFilter.TYPE_PREDICATE) && (ColumnType == QueryFilter.TYPE_MEASURE) && Column.equals(measurename))
		{
			return (true);
		} else if (Type == QueryFilter.TYPE_LOGICAL_OP)
		{
			for (QueryFilter qf : FilterList)
			{
				if (qf.containsMeasure(measurename))
					return (true);
			}
		}
		return (false);
	}

	/**
	 * Returns whether the navigated filter contains a session variable
	 * 
	 * @return
	 */
	public boolean containsSessionVariable()
	{
		if ((Type == QueryFilter.TYPE_PREDICATE))
		{
			if (ColumnType == QueryFilter.TYPE_MEASURE)
			{
				return (mc != null && mc.SessionVariables != null && mc.SessionVariables.size() > 0);
			}
			if (ColumnType == QueryFilter.TYPE_DIMENSION_COLUMN)
			{
				return (dc != null && dc.SessionVariables != null && dc.SessionVariables.size() > 0);
			}
			return false;
		} else if (Type == QueryFilter.TYPE_LOGICAL_OP)
		{
			for (QueryFilter qf : FilterList)
			{
				if (qf.containsSessionVariable())
					return (true);
			}
		}
		return (false);
	}

	/**
	 * Returns whether the filter contains a measure
	 * 
	 * @return
	 */
	public boolean containsMeasure()
	{
		if ((Type == QueryFilter.TYPE_PREDICATE) && (ColumnType == TYPE_MEASURE))
		{
			return (true);
		} else if (Type == QueryFilter.TYPE_LOGICAL_OP)
		{
			for (QueryFilter qf : FilterList)
			{
				if (qf.containsMeasure())
					return (true);
			}
		}
		return (false);
	}

	/**
	 * Returns whether the filter contains only a single measure
	 * 
	 * @return
	 */
	public boolean containsOnlyMeasure()
	{
		if ((Type == QueryFilter.TYPE_PREDICATE) && (ColumnType == TYPE_MEASURE))
		{
			return true;
		} else if (Type == QueryFilter.TYPE_LOGICAL_OP)
		{
			return false;
		}
		return false;
	}

	/**
	 * Returns whether the filter contains a measure that hasn't been navigated to a given measure table
	 * 
	 * @return
	 */
	public boolean containsMeasureNotNavigated(MeasureTable mt)
	{
		if ((Type == QueryFilter.TYPE_PREDICATE) && (ColumnType == TYPE_MEASURE))
		{
			if ((mc == null) || (mc.MeasureTable != mt))
				return (true);
			else
				return (false);
		} else if (Type == QueryFilter.TYPE_LOGICAL_OP)
		{
			for (QueryFilter qf : FilterList)
			{
				if (qf.containsMeasureNotNavigated(mt))
					return (true);
			}
		}
		return (false);
	}

	/**
	 * Returns a list of the measure tables that have been navigated to in this filter
	 * 
	 * @return
	 */
	public void getMeasureTablesNavigated(List<MeasureTable> mtList)
	{
		if ((Type == QueryFilter.TYPE_PREDICATE) && (ColumnType == TYPE_MEASURE))
		{
			if (mc != null)
			{
				if (!mtList.contains(mc.MeasureTable))
					mtList.add(mc.MeasureTable);
			}
		} else if (Type == QueryFilter.TYPE_LOGICAL_OP)
		{
			for (QueryFilter qf : FilterList)
			{
				qf.getMeasureTablesNavigated(mtList);
			}
		}
		return;
	}

	/**
	 * Returns whether the filter contains a dimension
	 * 
	 * @return
	 */
	public boolean containsDimension(String dim)
	{
		if ((Type == QueryFilter.TYPE_PREDICATE) && (ColumnType == TYPE_DIMENSION_COLUMN) && (Dimension.equals(dim)))
		{
			return (true);
		} else if(Type == QueryFilter.TYPE_MEASURE && this.MeasureFilter != null && this.MeasureFilter.containsDimension(dim))
		{
			return (true);
		}
		else if (Type == QueryFilter.TYPE_LOGICAL_OP)
		{
			for (QueryFilter qf : FilterList)
			{
				if (qf.containsDimension(dim))
					return (true);
			}
			return (false);
		}
		return (false);
	}

	/**
	 * Returns a list of the dimension columns in the filter (navigation must have been done first)
	 * 
	 * @return
	 */
	public void returnDimensionColumns(Collection<DimensionColumn> set)
	{
		if ((Type == QueryFilter.TYPE_PREDICATE) && (ColumnType == TYPE_DIMENSION_COLUMN))
		{
			if (!set.contains(dc))
				set.add(dc);
			return;
		} else if (Type == QueryFilter.TYPE_LOGICAL_OP)
		{
			for (QueryFilter qf : FilterList)
				qf.returnDimensionColumns(set);
		}
		return;
	}

	/**
	 * Returns a list of the dimension columns in the filter (navigation may not have been done first)
	 * 
	 * @return
	 */
	public void getDimensionColumnNames(Set<String[]> set)
	{
		if ((Type == QueryFilter.TYPE_PREDICATE) && (ColumnType == TYPE_DIMENSION_COLUMN))
		{
			boolean found = false;
			for (String[] dcname : set)
			{
				if (dcname[0].equals(Dimension) && dcname[1].equals(Column))
				{
					found = true;
					break;
				}
			}
			if (!found)
				set.add(new String[]
				{ Dimension, Column });
			return;
		} else if (Type == QueryFilter.TYPE_LOGICAL_OP)
		{
			for (QueryFilter qf : FilterList)
				qf.getDimensionColumnNames(set);
		}
		return;
	}

	/**
	 * Find dimension or column values in a filter
	 * 
	 * @param dimName
	 * @param colOrValue
	 * @return
	 */
	public boolean searchFilter(String dimName, String colOrValue)
	{
		if (Type == QueryFilter.TYPE_PREDICATE)
		{
			String lower = colOrValue.toLowerCase();
			if (ColumnType == TYPE_DIMENSION_COLUMN)
			{
				if (dimName != null && Dimension.toLowerCase().indexOf(lower) >= 0)
					return true;
				if (Column.toLowerCase().indexOf(lower) >= 0)
					return true;
				if (Operand.toLowerCase().indexOf(lower) >= 0)
					return true;
			} else if (ColumnType == TYPE_MEASURE)
			{
				if (Column.toLowerCase().indexOf(lower) >= 0)
					return true;
				if (Operand.toLowerCase().indexOf(lower) >= 0)
					return true;
			}
		} else if (Type == QueryFilter.TYPE_LOGICAL_OP)
		{
			for (QueryFilter qf : FilterList)
				if (qf.searchFilter(dimName, colOrValue))
					return true;
		}
		return false;
	}

	/**
	 * Replace dimension or column values in a filter
	 * 
	 * @param dimName
	 * @param colOrValue
	 * @param replaceDimName
	 * @param replaceColOrValue
	 * @return
	 */
	public boolean replaceFilter(String dimName, String colOrValue, String replaceDimName, String replaceColOrValue)
	{
		if (Type == QueryFilter.TYPE_PREDICATE)
		{
			if (ColumnType == TYPE_DIMENSION_COLUMN)
			{
				String lower = colOrValue.toLowerCase();
				if (dimName != null && Dimension.toLowerCase().indexOf(lower) >= 0)
				{
					String oldDim = Dimension;
					Dimension = Util.replaceStrIgnoreCase(Dimension, dimName, replaceDimName);
					return !oldDim.equalsIgnoreCase(Dimension);
				}
				if (Column.toLowerCase().indexOf(lower) >= 0)
				{
					String oldCol = Column;
					Column = Util.replaceStrIgnoreCase(Column, colOrValue, replaceColOrValue);
					return !oldCol.equalsIgnoreCase(Column);
				}
				if (Operand.toLowerCase().indexOf(lower) >= 0)
				{
					String oldOp = Operand;
					Operand = Util.replaceStrIgnoreCase(Operand, colOrValue, replaceColOrValue);
					return !oldOp.equalsIgnoreCase(Operand);
				}
			} else if (ColumnType == TYPE_MEASURE)
			{
				String lower = colOrValue.toLowerCase();
				if (Column.toLowerCase().indexOf(lower) >= 0)
				{
					String oldCol = Column;
					Column = Util.replaceStrIgnoreCase(Column, colOrValue, replaceColOrValue);
					return !oldCol.equalsIgnoreCase(Column);
				}
				if (Operand.toLowerCase().indexOf(lower) >= 0)
				{
					String oldOp = Operand;
					Operand = Util.replaceStrIgnoreCase(Operand, colOrValue, replaceColOrValue);
					return !oldOp.equalsIgnoreCase(Operand);
				}
			}
		} else if (Type == QueryFilter.TYPE_LOGICAL_OP)
		{
			boolean replaced = false;
			for (QueryFilter qf : FilterList)
				replaced |= qf.replaceFilter(dimName, colOrValue, replaceDimName, replaceColOrValue);
			return replaced;
		}
		return false;
	}

	/**
	 * Returns a list of the dimension columns in the filter (navigation may not have been done first)
	 * 
	 * @return
	 */
	public void getMeasureColumnNames(Set<String[]> set)
	{
		if ((Type == QueryFilter.TYPE_PREDICATE) && (ColumnType == TYPE_MEASURE))
		{
			boolean found = false;
			for (String[] dcname : set)
			{
				if (dcname[0].equals(Dimension) && dcname[1].equals(Column))
				{
					found = true;
					break;
				}
			}
			if (!found)
				set.add(new String[]
				{ Dimension, Column });
			return;
		} else if (Type == QueryFilter.TYPE_LOGICAL_OP)
		{
			for (QueryFilter qf : FilterList)
				qf.getDimensionColumnNames(set);
		}
		return;
	}

	/**
	 * Returns whether the filter contains only a given dimension
	 * 
	 * @return
	 */
	public boolean containsOnlyDimension(String dim)
	{
		if ((Type == QueryFilter.TYPE_PREDICATE) && (ColumnType == TYPE_DIMENSION_COLUMN) && (Dimension.equals(dim)))
		{
			return (true);
		} else if (Type == QueryFilter.TYPE_LOGICAL_OP)
		{
			for (QueryFilter qf : FilterList)
			{
				if (!qf.containsOnlyDimension(dim))
					return (false);
			}
			return (true);
		}
		return (false);
	}

	/**
	 * Extracts the filter relevant to a specific dimension
	 * 
	 * @return
	 */
	public QueryFilter getDimensionOnlyFilter(String dim)
	{
		if ((Type == QueryFilter.TYPE_PREDICATE) && (ColumnType == TYPE_DIMENSION_COLUMN) && (Dimension.equals(dim)))
		{
			return this;
		} else if (Type == QueryFilter.TYPE_LOGICAL_OP && Operator.equals("AND"))
		{
			List<QueryFilter> newList = new ArrayList<QueryFilter>();
			for (QueryFilter qf : FilterList)
			{
				QueryFilter newqf = qf.getDimensionOnlyFilter(dim);
				if (newqf != null)
					newList.add(newqf);
			}
			if (newList.size() > 0)
				return new QueryFilter("AND", newList);
		}
		return null;
	}

	/**
	 * Returns whether the filter contains a derived measure
	 * 
	 * @return
	 */
	public boolean containsDerivedMeasure(Query q)
	{
		if (q == null)
			return false;
		if ((Type == QueryFilter.TYPE_PREDICATE) && (ColumnType == TYPE_MEASURE))
		{
			return (q.isDerivedMeasure(Column));
		} else if (Type == QueryFilter.TYPE_LOGICAL_OP)
		{
			for (QueryFilter qf : FilterList)
			{
				if (qf.containsDerivedMeasure(q))
					return (true);
			}
		}
		return (false);
	}

	private int compareOperands(QueryFilter qf, Repository r, Session session, String datatype)
	{
		if (r != null)
		{
			boolean isLikeOperator = qf.Operator.equals(QueryFilter.LIKE);
			boolean isLikeOnBothSides = qf.Operator.equals(QueryFilter.LIKE) && Operator.equals(QueryFilter.LIKE);
			return (compareOperandsAfterReplacingVariables(qf.Operand, Operand, r, session, datatype,isLikeOperator,isLikeOnBothSides));
		} else
		{
			if (qf.Operand == null)
			{
				if (Operand == null)
					return 0;
				return -1;
			}
			if (Operand == null)
			{
				// already know that qf.Operand is not null
				return 1;
			}
			return qf.Operand.compareTo(Operand);
		}
	}

	/**
	 * Return whether two filters are logically equal. If
	 * 
	 * @param qf
	 * @param r
	 *            If included, do repository variable replacement (of filter being compared to)
	 * @param session
	 *            If included, do session variable replacement (must also have a repository)
	 * @param lessRestrictiveThan returns whether the supplied filter is less restrictive than this one 
	 * @return
	 */
	public boolean equalsUsingVariables(QueryFilter qf, Session session, Repository r, boolean lessRestrictiveThan)
	{
		if (Type != qf.Type)
			return (false);
		if (Type == QueryFilter.TYPE_PREDICATE)
		{
			if (ColumnType == QueryFilter.TYPE_DIMENSION_COLUMN)
			{
				if (qf.Column.equals(Column) && qf.Dimension.equals(Dimension))
				{
					if (qf.Operator.equals(Operator))
					{
						/*
						 * Operand is null for these operators
						 */
						if (qf.Operator.equals(QueryFilter.ISNULL) || qf.Operator.equals(QueryFilter.ISNOTNULL))
							return true;
						return compareOperands(qf, r, session, null) == 0;
					}
					else if (r != null)
					{
						if (Operator.equals("=")) 
						{
							DimensionColumn fdc = r.findDimensionColumn(Dimension, Column);
							String dtype = fdc != null ? fdc.DataType : null;
							// see if the supplied filter is less restrictive
							if (qf.Operator.equals(">=") )
							{
								return compareOperands(qf, r, session, dtype) <= 0;
							}
							else if (qf.Operator.equals("<="))
							{
								return compareOperands(qf, r, session, dtype) >= 0;
							}
							else if (qf.Operator.equals("<"))
							{
								return compareOperands(qf, r, session, dtype) > 0;
							}
							else if (qf.Operator.equals(">"))
							{
								return compareOperands(qf, r, session, dtype) < 0;
							}
							else if (qf.Operator.equals(QueryFilter.LIKE) && Operator.equals("="))
							{
								return compareOperands(qf, r, session, dtype) == 0;
							}
							else
								return false;
						} else if (Operator.equals(">")) 
						{
							DimensionColumn fdc = r.findDimensionColumn(Dimension, Column);
							String dtype = fdc != null ? fdc.DataType : null;
							// see if the supplied filter is less restrictive
							if (qf.Operator.equals(">") )
							{
								return compareOperands(qf, r, session, dtype) < 0;
							}
							else if (qf.Operator.equals(">="))
							{
								return compareOperands(qf, r, session, dtype) <= 0;
							}
							else
								return false;
						} else if (Operator.equals("<")) 
						{
							DimensionColumn fdc = r.findDimensionColumn(Dimension, Column);
							String dtype = fdc != null ? fdc.DataType : null;
							// see if the supplied filter is less restrictive
							if (qf.Operator.equals("<") )
							{
								return compareOperands(qf, r, session, dtype) > 0;
							}
							else if (qf.Operator.equals("<="))
							{
								return compareOperands(qf, r, session, dtype) >= 0;
							}
							else
								return false;
						} else if (Operator.equals(">=")) 
						{
							DimensionColumn fdc = r.findDimensionColumn(Dimension, Column);
							String dtype = fdc != null ? fdc.DataType : null;
							// see if the supplied filter is less restrictive
							if (qf.Operator.equals(">") )
							{
								return compareOperands(qf, r, session, dtype) < 0;
							}
							else if (qf.Operator.equals(">="))
							{
								return compareOperands(qf, r, session, dtype) <= 0;
							}
							else
								return false;
						} else if (Operator.equals("<=")) 
						{
							DimensionColumn fdc = r.findDimensionColumn(Dimension, Column);
							String dtype = fdc != null ? fdc.DataType : null;
							// see if the supplied filter is less restrictive
							if (qf.Operator.equals("<") )
							{
								return compareOperands(qf, r, session, dtype) > 0;
							}
							else if (qf.Operator.equals("<="))
							{
								return compareOperands(qf, r, session, dtype) >= 0;
							}
							else
								return false;
						} else
							return false;
					}
				}
				else
					return false;
			} else if (ColumnType == QueryFilter.TYPE_MEASURE)
			{
				if (qf.Column.equals(Column) && qf.Operator.equals(Operator))
				{
					if (MeasureFilter != null)
					{
						if (qf.MeasureFilter != null)
							return (MeasureFilter.equals(qf.MeasureFilter));
						else
							return false;
					} else
					{
						if (qf.Operator.equals(Operator))
						{
							/*
							 * Operand is null for these operators
							 */
							if (qf.Operator.equals(QueryFilter.ISNULL) || qf.Operator.equals(QueryFilter.ISNOTNULL))
								return true;
							return compareOperands(qf, r, session, null) == 0;
						}
						else if(r != null)
						{
							if (Operator.equals("=")) 
							{
								// see if the supplied filter is less restrictive
								if (qf.Operator.equals(">=") )
								{
									return compareOperands(qf, r, session, null) <= 0;
								}
								else if (qf.Operator.equals("<="))
								{
									return compareOperands(qf, r, session, null) >= 0;
								}
								else if (qf.Operator.equals("<"))
								{
									return compareOperands(qf, r, session, null) > 0;
								}
								else if (qf.Operator.equals(">"))
								{
									return compareOperands(qf, r, session, null) < 0;
								}
								else
									return false;
							} else
								return false;
						}
					}
				} else
					return false;
			}
		} else
		{
			if (FilterList.size() != qf.FilterList.size())
			{
				return false;
			}
			if (this.Operator!=null && qf.getOperator()!=null && !this.Operator.equals(qf.getOperator()))
				return false;
			for (int i = 0; i < FilterList.size(); i++)
			{
				if (!FilterList.get(i).equalsUsingVariables(qf.FilterList.get(i), session, r, lessRestrictiveThan))
					return false;
			}
			return true;
		}
		return false;
	}

	private int compareOperandsAfterReplacingVariables(String operand1, String operand2, Repository r, Session session, String dtype,boolean isLikeOperator,boolean isLikeOnBothSides)
	{
		// replaceVariables checks for null
		operand1 = r.replaceVariables(null, operand1);
		operand2 = r.replaceVariables(null, operand2);
		if (session != null)
		{
			operand1 = r.replaceVariables(session, operand1);
			operand2 = r.replaceVariables(session, operand2);
			operand1 = getOperand(r, session, operand1);
			operand2 = getOperand(r, session, operand2);
		}
		if (operand1 == null)
		{
			if (operand2 == null)
				return 0;
			return -1;
		}
		if (operand2 == null)
		{
			// already know that operand1 is not null
			return 1;
		}
		if (dtype != null)
		{
			if (dtype.equals("Date") || dtype.equals("DateTime"))
			{
				if (operand1.startsWith("'") && operand1.endsWith("'") && operand1.length() > 2)
					operand1 = operand1.substring(1, operand1.length() - 1);
				if (operand2.startsWith("'") && operand2.endsWith("'") && operand2.length() > 2)
					operand2 = operand2.substring(1, operand2.length() - 1);
				Date d1 = DateUtil.parseUsingPossibleFormats(operand1);
				Date d2 = DateUtil.parseUsingPossibleFormats(operand2);
				if (d1 == null && d2 != null)
					return -1;
				if (d1 != null && d2 == null)
					return 1;
				if (d1 == null && d2 == null)
					return 0;
				if(d1 != null)
					return d1.compareTo(d2);
			}
			else if (dtype.equalsIgnoreCase("Varchar") && isLikeOperator)
			{
				Pattern likePattern = setLikeOperator(operand1);
				boolean test = likePattern.matcher(operand2.toUpperCase()).find();
				return test ? 0 : 1;
			}
		}
		else 
		{
			if (isLikeOperator && isLikeOnBothSides && !operand1.equalsIgnoreCase(operand2))
			{
				/*
				 * See if either of the like expressions is a subset of the other
				 */
				String replacedOperand1 = operand1.toLowerCase();
				String replacedOperand2 = operand2.toLowerCase();
				if (replacedOperand1.startsWith("%") && replacedOperand2.startsWith("%") && 
					replacedOperand1.endsWith("%") && replacedOperand2.endsWith("%"))
				{
					/*
					 * To support cases such as where aggregate filter is something like '%AC%' and incoming filter is something like '%A%' or '%C%'
					 */
					replacedOperand1 = replacedOperand1.substring(1);
					replacedOperand2 = replacedOperand2.substring(1);
					replacedOperand1 = replacedOperand1.substring(0, replacedOperand1.length() - 1);
					replacedOperand2 = replacedOperand2.substring(0, replacedOperand2.length() - 1);
					return (replacedOperand2.contains(replacedOperand1) ? 0 : 1);
				}
				else if (replacedOperand1.startsWith("%") && replacedOperand1.endsWith("%"))
				{
					/*
					 * To support cases such as where aggregate filter is something like '%C%' and incoming filter is something like '%C' or 'C%' or just 'C'
					 */
					if (replacedOperand2.startsWith("%"))
					{
						replacedOperand1 = replacedOperand1.substring(1);
						replacedOperand1 = replacedOperand1.substring(0, replacedOperand1.length() - 1);
						replacedOperand2 = replacedOperand2.substring(1);
						return (replacedOperand2.startsWith(replacedOperand1) ? 0 : 1);
					}
					else if (replacedOperand2.endsWith("%"))
					{
						replacedOperand1 = replacedOperand1.substring(1);
						replacedOperand1 = replacedOperand1.substring(0, replacedOperand1.length() - 1);
						replacedOperand2 = replacedOperand2.substring(0, replacedOperand2.length() - 1);
						return (replacedOperand2.endsWith(replacedOperand1) ? 0 : 1);
					}
					else
					{
						replacedOperand1 = replacedOperand1.substring(1);
						replacedOperand1 = replacedOperand1.substring(0, replacedOperand1.length() - 1);
						return (replacedOperand2.equals(replacedOperand1) ? 0: 1);
					}
				}
				else if (replacedOperand1.startsWith("%") && replacedOperand2.startsWith("%"))
				{
					/*
					 * Where incoming filter is %C and aggregate filter is %AC
					 */
					replacedOperand1 = replacedOperand1.substring(1);
					replacedOperand2 = replacedOperand2.substring(1);
					
					return (replacedOperand2.endsWith(replacedOperand1) ? 0 : 1);
				}
				else if (replacedOperand1.endsWith("%") && replacedOperand2.endsWith("%"))
				{
					/*
					 * Where incoming filter is C% and aggregate filter is CA%
					 */
					replacedOperand1 = replacedOperand1.substring(0, replacedOperand1.length() - 1);
					replacedOperand2 = replacedOperand2.substring(0, replacedOperand2.length() - 1);
					
					return (replacedOperand2.startsWith(replacedOperand1) ? 0 : 1);					
				}
			}
		}
		return (operand1.compareTo(operand2));
	}
	
	private Pattern setLikeOperator(String operand)
	{
		if (!String.class.isInstance(operand))
			return null;
		String str = (String) operand;
		str = str.toUpperCase();
		str = str.replaceAll("%", "\\\\E.*\\\\Q").replaceAll("_", "\\\\E.\\\\Q");
		if (str.startsWith("\\E")) {
			str = str.substring(2);
		}
		else {
			str = "\\Q" + str;
		}
		if (str.endsWith("\\Q")) {
			str = str.substring(0, str.length() - 2);
		}
		else {
			str = str + "\\E";
		}
		str = "^" + str + "$";
		Pattern pattern = Pattern.compile(str);
		return pattern;
	}
		

	public boolean equals(Object o)
	{
		if (this == o)
			return true;
		if (!(o instanceof QueryFilter))
			return false;
		return (equalsUsingVariables((QueryFilter) o, null, null, false));
	}

	public int hashCode()
	{
		assert false : "hashCode not designed";
		return 42; // any arbitrary constant will do
	}

	public int compareTo(QueryFilter qf)
	{
		if (Type == qf.Type)
		{
			if (Type == QueryFilter.TYPE_PREDICATE)
			{
				return (Column.compareTo(qf.Column));
			} else
			{
				return (0);
			}
		}
		return (0);
	}

	/**
	 * @return Returns whether filter is promptable.
	 */
	public boolean isPrompted()
	{
		return promptName != null && promptName.trim().length() > 0;
	}
	
	public boolean isPromptedFilterWithNoValue()
	{
		if (this.getType() == TYPE_PREDICATE)
		{
			if (isPrompted() && QueryString.NO_FILTER_TEXT.equals(Operand))
				return true;
		}
		return false;
	}

	/**
	 * Determine whether this filter can be satisfied by a list of query columns
	 * 
	 * @param queryColummns
	 * @return
	 */
	public boolean isSatisfiedBy(List<Object> queryColumns)
	{
		if (Type == QueryFilter.TYPE_PREDICATE)
		{
			boolean found = false;
			for (Object o : queryColumns)
			{
				if (DimensionColumnNav.class.isInstance(o))
				{
					DimensionColumnNav dcn = (DimensionColumnNav) o;
					if (dcn.dimensionName.equals(Dimension) && dcn.columnName.equals(Column))
					{
						found = true;
						break;
					}
				} else if (MeasureColumnNav.class.isInstance(o))
				{
					MeasureColumnNav mcn = (MeasureColumnNav) o;
					if (mcn.measureName.equals(Column))
					{
						found = true;
						break;
					}
				}
			}
			return (found);
		} else
		{
			for (QueryFilter qf : FilterList)
			{
				if (!qf.isSatisfiedBy(queryColumns))
					return (false);
			}
			return (true);
		}
	}

	public void writeExternal(ObjectOutput oo) throws IOException
	{
		oo.writeObject(Name);
		oo.writeObject(Dimension);
		oo.writeObject(Column);
		oo.writeObject(Operator);
		oo.writeObject(Operand);
		oo.writeObject(Type);
		oo.writeObject(ColumnType);
		oo.writeObject(FilterList);
		oo.writeObject(promptName);
	}

	@SuppressWarnings("unchecked")
	public void readExternal(ObjectInput oi) throws IOException, ClassNotFoundException
	{
		Name = (String) oi.readObject();
		Dimension = (String) oi.readObject();
		Column = (String) oi.readObject();
		Operator = (String) oi.readObject();
		Operand = (String) oi.readObject();
		Type = (Integer) oi.readObject();
		ColumnType = (Integer) oi.readObject();
		FilterList = (List<QueryFilter>) oi.readObject();
		promptName = (String) oi.readObject();
	}
	
	public void fromBasicDBObject(BasicDBObject obj){
		if(obj != null){
			Name = obj.getString("Name");
			Dimension = obj.getString("Dimension");
			Column = obj.getString("Column");
			Operator = obj.getString("Operator");
			Operand = obj.getString("Operand");
			Type = obj.getInt("Type");
			ColumnType = obj.getInt("ColumnType");

			BasicDBList l = (BasicDBList)obj.get("FilterList");
			try{
				FilterList = MongoMgr.convertFromDBList(l, QueryFilter.class);
			}catch(Exception ex){
				logger.error(ex.getMessage(), ex);
			}
			promptName = obj.getString("promptName");
		}
	}
	
	public BasicDBObject toBasicDBObject(){
		BasicDBObject oo = new BasicDBObject();
		
		oo.put("Name", Name);
		oo.put("Dimension", Dimension);
		oo.put("Column", Column);
		oo.put("Operator", Operator);
		oo.put("Operand", Operand);
		oo.put("Type", Type);
		oo.put("ColumnType", ColumnType);
		oo.put("FilterList", MongoMgr.convertToDBList(FilterList));
		oo.put("promptName", promptName);
		
		return oo;
	}

	public QueryFilter replaceVariables(Repository r, Session session) throws CloneNotSupportedException
	{
		QueryFilter returnFilter = this;
		if (Operand != null)
		{
			String operandWithVariablesReplaced = r.replaceVariables(null, Operand);
			if (session != null)
			{
				operandWithVariablesReplaced = r.replaceVariables(session, operandWithVariablesReplaced);
			}
			if (operandWithVariablesReplaced != null && !operandWithVariablesReplaced.isEmpty() && operandWithVariablesReplaced.charAt(0) == '\'' && operandWithVariablesReplaced.charAt(operandWithVariablesReplaced.length() - 1) == '\'')
			{
				operandWithVariablesReplaced = operandWithVariablesReplaced.substring(1, operandWithVariablesReplaced.length() - 1);
			}
			if (!Operand.equals(operandWithVariablesReplaced))
			{
				returnFilter = (QueryFilter) this.clone();
				returnFilter.Operand = operandWithVariablesReplaced;
			}
		}
		if (FilterList != null)
		{
			for (int i = 0; i < returnFilter.FilterList.size(); i++)
			{
				QueryFilter filterListItem = returnFilter.FilterList.get(i);
				QueryFilter returnFilterListItem = filterListItem.replaceVariables(r, session);
				returnFilter.FilterList.set(i, returnFilterListItem);
			}
		}
		return returnFilter;
	}

	public boolean hasMeasureFilter()
	{
		return (MeasureFilter != null);
	}

	public boolean hasMeasureFilter(QueryFilter qf)
	{
		if (MeasureFilter != null && qf != null)
		{
			return (MeasureFilter.equals(qf));
		}
		return false;
	}

	public boolean hasMeasureFilter(Set<QueryFilter> qfList)
	{
		if (qfList != null)
		{
			for (QueryFilter qf : qfList)
			{
				if (hasMeasureFilter(qf))
				{
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Returns whether the first list contains all the filters in the second list. If repository is provided, do
	 * repository variable replacement of filters in the second list when comparing. If session is provided do session
	 * variable replacement of filters in the second list.
	 * 
	 * @param list1
	 * @param list2
	 * @param r
	 * @param session
	 * @return
	 */
	public static boolean containsAll(List<QueryFilter> list1, List<QueryFilter> list2, Repository r, Session session)
	{
		if (list1 == null)
			return (false);
		if (list2 == null)
			return (true);
		if (r == null)
			return (list1.containsAll(list2));
		for (QueryFilter qf2 : list2)
		{
			boolean found = false;
			for (QueryFilter qf1 : list1)
			{
				if (qf1.equalsUsingVariables(qf2, session, r, true))
				{
					found = true;
					break;
				} else if ((qf2.Type == QueryFilter.TYPE_LOGICAL_OP) && ("OR".equals(qf2.Operator)))
				{
					/**
					 * If a given filter in list2 is part of a logical operator OR filter, it is considered as found.
					 * E.g. filter2 FOR{FDC{Time.Year=2006},FDC{Time.Year=2007}} is a super set of filter1
					 * FDC{Time.Year=2006} in terms of data it returns and hence, found is set to true.
					 */
					for (QueryFilter qfInLogicalOp : qf2.FilterList)
					{
						if (qfInLogicalOp.equalsUsingVariables(qf1, session, r, true))
						{
							found = true;
							break;
						}
					}
					if (found)
					{
						break;
					}
				} else if ((qf1.Type == QueryFilter.TYPE_LOGICAL_OP) && ("OR".equals(qf1.Operator)))
				{
					/**
					 * If qf1 is a series of equal values with an or, make sure they are all satisfied
					 */
					boolean allEqual = true;
					for (QueryFilter qfInLogicalOp : qf1.FilterList)
					{
						if (qfInLogicalOp.Type == QueryFilter.TYPE_PREDICATE && qfInLogicalOp.Operator.equals("="))
						{
							allEqual = allEqual && qfInLogicalOp.equalsUsingVariables(qf2, session, r, true);
						} else
						{
							allEqual = false;
							break;
						}
					}
					if (allEqual)
					{
						found = true;
						break;
					}
				}
				else if(qf2.isSuperSetOf(r, session, qf1))
				{
					found = true;
					break;
				}
			}
			if (!found)
				return (false);
		}
		return (true);
	}

	/**
	 * If this query filter is a measure filter, allows setting of the default value. If not set, the default is null.
	 * 
	 * @param measureFilterDefault
	 *            the measureFilterDefault to set
	 */
	public void setMeasureFilterDefault(Double measureFilterDefault)
	{
		MeasureFilterDefault = measureFilterDefault;
	}

	/**
	 * Return the default value for a measure filter
	 * 
	 * @return the measureFilterDefault
	 */
	public Double getMeasureFilterDefault()
	{
		return MeasureFilterDefault;
	}

	Set<String> getAllDimensionColumnNames(Set<String> dcSet)
	{
		if (ColumnType == TYPE_DIMENSION_COLUMN && Type == QueryFilter.TYPE_PREDICATE)
		{
			dcSet.add(this.Dimension + "." + this.Column);
		} else
		{
			if (FilterList != null)
			{
				for (QueryFilter qf : FilterList)
					qf.getAllDimensionColumnNames(dcSet); // recursive call
			}
		}
		return dcSet;
	}

	Set<DimensionColumn> getAllMDXDimensionColumnsNotInProjectionList(Set<DimensionColumn> dcSet, List<QueryColumn> resultProjectionList)
	{
		if (ColumnType == TYPE_DIMENSION_COLUMN && Type == QueryFilter.TYPE_PREDICATE)
		{
			boolean found = false;
			for (QueryColumn qc : resultProjectionList)
			{
				if (qc.type == QueryColumn.DIMENSION)
				{
					DimensionColumn dc = (DimensionColumn) qc.o;
					if (dc.DimensionTable.DimensionName.equals(this.Dimension) && dc.ColumnName.equals(this.Column))
					{
						found = true;
						break;
					}
				}
			}
			if (!found)
				dcSet.add(dc);
		} else
		{
			if (FilterList != null)
			{
				for (QueryFilter qf : FilterList)
					qf.getAllMDXDimensionColumnsNotInProjectionList(dcSet, resultProjectionList); // recursive call
			}
		}
		return dcSet;
	}
	
	Set<DimensionColumn> getAllMDXDimensionColumns(Set<DimensionColumn> dcSet)
	{
		if (ColumnType == TYPE_DIMENSION_COLUMN && Type == QueryFilter.TYPE_PREDICATE)
		{
			dcSet.add(dc);
		} else
		{
			if (FilterList != null)
			{
				for (QueryFilter qf : FilterList)
				{
					qf.getAllMDXDimensionColumns(dcSet);
				}
			}
		}
		return dcSet;
	}	
	public Map<String, String> getMDXDimensionClauseMap(Map<String, String> dcMap)
	{ 
		if (ColumnType == TYPE_DIMENSION_COLUMN && Type == QueryFilter.TYPE_PREDICATE)
		{
			int index = dc.PhysicalName.lastIndexOf('.');
			String hname = dc.PhysicalName.substring(0, index);
			if (!dcMap.containsKey(hname))
				dcMap.put(hname, dc.PhysicalName + ".Members" );
		} else
		{
			if (FilterList != null)
			{
				for (QueryFilter qf : FilterList)
					qf.getMDXDimensionClauseMap(dcMap); // recursive call
			}
		}
		return dcMap;
	}
	
	public boolean includeFilterAsMDXDimensionSet(Repository r, DatabaseType dbtype, Map<String, List<DimensionColumn>> dimMap)
	{
		if (dbtype != DatabaseType.MSASXMLA && dbtype != DatabaseType.MONDRIANXMLA)
			return false;
		String dtype = null;
		DimensionColumn filterDC = null;
		if (isPredicateWithEqualsOp())
		{
			filterDC = dc;
			dtype = dc == null ? null : dc.DataType ;
		} else
		{
			dtype = getDatatypeForLogicalORWithSameDimCol();
			if (dtype != null && FilterList != null)
				filterDC = FilterList.get(0).dc;
		}

		if((dtype == null) || (!dtype.equals("Varchar")) && (!dtype.equals("Integer")))
		{
			return false;
		}
		if (filterDC == null)
			return false;
		else
		{
			List<DimensionColumn> lowestLevelColumns = dimMap.get(filterDC.DimensionTable.DimensionName);
			if (lowestLevelColumns != null && lowestLevelColumns.indexOf(filterDC) < 0)
			{
				boolean match = false;
				for (DimensionColumn dc : lowestLevelColumns)
				{
					if (dc != null && dc.PhysicalName != null && dc.PhysicalName.lastIndexOf(".") >= 0)
					{
						Set<String> dcDimensionalHierarchy = new HashSet<String>();
						dcDimensionalHierarchy.add(dc.PhysicalName.substring(0, dc.PhysicalName.lastIndexOf(".")));
						if (belongsToMDXDimensionalHiearchy(dcDimensionalHierarchy))
						{
							match = true;
							break;
						}
					}
				}
				if (match)
					return false;
			}
		}
		return true;
	}
	
	public void getMDXDimensionalHierarchies(Set<String> filterMDXDimensionalHierarchies)
	{
		if (isPredicateWithEqualsOp())
		{
			if (dc.PhysicalName != null && dc.PhysicalName.lastIndexOf(".") >= 0)
				filterMDXDimensionalHierarchies.add(dc.PhysicalName.substring(0, dc.PhysicalName.lastIndexOf(".")));
		}
		else if (Type == QueryFilter.TYPE_LOGICAL_OP && FilterList != null && !FilterList.isEmpty())
		{
			for (QueryFilter qf : FilterList)
			{
				qf.getMDXDimensionalHierarchies(filterMDXDimensionalHierarchies);
			}
		}		
	}
	
	public boolean belongsToMDXDimensionalHiearchy(Set<String> dimHierarchy)
	{
		Set<String> filterMDXDimensionalHierarchies = new HashSet<String>();
		getMDXDimensionalHierarchies(filterMDXDimensionalHierarchies);
		return dimHierarchy.containsAll(filterMDXDimensionalHierarchies);		
	}
	
	public boolean containsOnlyEqualsOp()
	{
		if (Type == QueryFilter.TYPE_PREDICATE)
			return isPredicateWithEqualsOp();
		else if (Type == QueryFilter.TYPE_LOGICAL_OP && FilterList != null && !FilterList.isEmpty())
		{ 
			for (QueryFilter qf : FilterList)
			{
				if (!qf.containsOnlyEqualsOp())
					return false;
			}
		}
		return true;
	}

	public boolean isPredicateWithEqualsOp()
	{
		if (Type == QueryFilter.TYPE_PREDICATE && ColumnType == TYPE_DIMENSION_COLUMN && Operator.equals("="))
		{
			return true;
		}
		return false;
	}

	public boolean isPredicateWithDimensionColumn()
	{
		if (Type == QueryFilter.TYPE_PREDICATE && ColumnType == TYPE_DIMENSION_COLUMN)
		{
			return true;
		}
		return false;
	}
	
	public boolean isLogicalORWithSameDimCol()
	{
		if(Type != QueryFilter.TYPE_LOGICAL_OP || FilterList == null || FilterList.isEmpty())
		{
			return false;
		}
		String dim = FilterList.get(0).Dimension;
		String col = FilterList.get(0).Column;
		if(dim == null || col == null)
		{
			return false;
		}
		
		for(QueryFilter qf : FilterList)
		{
			if(!qf.isPredicateWithEqualsOp())
			{
				return false;
			}
			String qfdim = qf.Dimension;
			String qfcol = qf.Column;
			if(qfdim == null || qfcol == null)
			{
				return false;
			}
			if((!dim.equals(qfdim)) || (!col.equals(qfcol)))
			{
				return false;
			}
		}
		return true;
	}
	
	public String getDatatypeForLogicalORWithSameDimCol()
	{
		if(Type != QueryFilter.TYPE_LOGICAL_OP || FilterList == null || FilterList.isEmpty())
		{
			return null;
		}
		String dim = FilterList.get(0).Dimension;
		String col = FilterList.get(0).Column;
		String dtype =  FilterList.get(0).dc == null ? null :  FilterList.get(0).dc.DataType;
		
		if(dim == null || col == null || dtype == null)
		{
			return null;
		}
		
		for(QueryFilter qf : FilterList)
		{
			if(!qf.isPredicateWithEqualsOp())
			{
				return null;
			}
			String qfdim = qf.Dimension;
			String qfcol = qf.Column;
			if(qfdim == null || qfcol == null)
			{
				return null;
			}
			String qfdtype = qf.dc == null ? null : qf.dc.DataType;
			if((qfdtype == null) || (!dim.equals(qfdim)) || (!col.equals(qfcol)) || (!dtype.equals(qfdtype)))
			{
				return null;
			}
		}
		return dtype;
	}
	
	public boolean isLogicalANDWithDimCol()
	{
		if(Type != QueryFilter.TYPE_LOGICAL_OP || !Operator.equals("AND") || FilterList == null || FilterList.isEmpty())
		{
			return false;
		}
		
		for(QueryFilter qf : FilterList)
		{
			if(!qf.isPredicateWithDimensionColumn() && !qf.isLogicalORWithSameDimCol())
			{
				return false;
			}
		}
		return true;
	}
	
	public void splitFilters(List<QueryFilter> splitFilterList)
	{
		if(FilterList == null || FilterList.isEmpty())
		{
			splitFilterList.add(this);
		}
		for(QueryFilter qf : FilterList)
		{
			splitFilterList.add(qf);
		}
	}
	
	public void getDimensions(Set<String> dimensionList)
	{
		if (Type == QueryFilter.TYPE_PREDICATE)
		{
			if (ColumnType == TYPE_DIMENSION_COLUMN || ColumnType == TYPE_DIMENSION_MEMBER_SET)
			{
				if(!dimensionList.contains(Dimension))
				{
					dimensionList.add(Dimension);
				}
			}
		} 
		else
		{
			for (QueryFilter qf : FilterList)
			{
				qf.getDimensions(dimensionList);
			}
		}
	}
	public String getMDXOperandList(DatabaseType dbType)
	{
		if (Type == QueryFilter.TYPE_PREDICATE)
		{
			if (ColumnType == TYPE_DIMENSION_COLUMN)
			{
				String opvalue = Operand;
				opvalue = getDbSpecificOperandValue(dbType, opvalue);				
				if (dc.DataType.equals("Date"))
				{
					opvalue += " 00:00:00.000";
				}
				else if (dc.DataType.equals("DateTime"))
				{
					opvalue += ".000";
				}
				if (dbType == DatabaseType.MSASXMLA || dbType == DatabaseType.MONDRIANXMLA)
					return dc.PhysicalName+".["+opvalue+"]";
				else if (dbType == DatabaseType.SAPBWXMLA)
				{
					String physicalname = dc.PhysicalName;
					int index = physicalname.indexOf('.');
					if (index > 0)
						physicalname = physicalname.substring(0, index);
					return physicalname+".["+opvalue+"]";
				}
				else
					return "["+opvalue+"]";
			}
			return "";
		} else
		{
			StringBuilder sb = new StringBuilder();
			for (QueryFilter qf : FilterList)
			{
				if(sb.length() >0)
					sb.append(',');
				sb.append(qf.getMDXOperandList(dbType));
			}
			return sb.toString();
		}
	}
	
	public boolean containsVirtualColumn(Repository r)
	{
		if (this.getType() == QueryFilter.TYPE_PREDICATE && QueryString.NO_FILTER_TEXT.equals(this.getOperand()))
		{
			String dim = this.getDimension();
			String col = this.getColumn();
			if (dim != null && col != null)
			{
				List<DimensionColumn> dimCols = r.findDimensionColumns(dim, col);
				if (dimCols != null)
				{
					for (DimensionColumn dc : dimCols)
					{
						if (dc.VC != null)
						{
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	public String getPromptName()
	{
		return promptName;
	}

	public void setPromptName(String promptName)
	{
		if (promptName != null && !Util.hasNonWhiteSpaceCharacters(promptName))
		{
			promptName = null;
		}
		this.promptName = promptName;
	}
	
	public void setOperand(String op) {
		this.Operand = op;
	}

	public List<QueryFilter> getFilterList()
	{
		return FilterList;
	}

	public QueryFilter replaceMeasureFilter(String column, QueryFilter filterToReplace) {
		if (this.Type == TYPE_PREDICATE)  {
			if (containsMeasure(column))
				return filterToReplace;
		}
		else {
			for (ListIterator<QueryFilter> iter = FilterList.listIterator(); iter.hasNext(); ) {
				QueryFilter qf = iter.next();
				if (qf.containsMeasure(column)) {
					iter.set(qf.replaceMeasureFilter(column, filterToReplace));
				}
			}
		}
		
		return this;
	}
	
	public QueryFilter removeMeasureFilter(String column) {
		if (this.Type == TYPE_PREDICATE)  {
			if (containsMeasure(column))
				return null;
		}
		else {
			for (ListIterator<QueryFilter> iter = FilterList.listIterator(); iter.hasNext(); ) {
				QueryFilter qf = iter.next();
				if (qf.containsMeasure(column)) {
					if (qf.Type == TYPE_PREDICATE) {
						iter.remove();
					}
					else {
						QueryFilter newFilter = qf.removeMeasureFilter(column); 
						if (newFilter == null)
							iter.remove();
						else
							iter.set(newFilter);
					}
				}
				return cleanUpFilterList();
			}
		}
		
		return this;
	}
	public QueryFilter removeAttributeFilter(String dimension, String column) {
		if (this.Type == TYPE_PREDICATE)  {
			if (containsAttribute(dimension, column))
				return null;
		}
		else {
			for (ListIterator<QueryFilter> iter = FilterList.listIterator(); iter.hasNext(); ) {
				QueryFilter qf = iter.next();
				if (qf.containsAttribute(dimension, column)) {
					if (qf.Type == TYPE_PREDICATE) {
						iter.remove();
					}
					else {
						QueryFilter newFilter = qf.removeAttributeFilter(dimension, column); 
						if (newFilter == null)
							iter.remove();
						else
							iter.set(newFilter);
					}
				}
				return cleanUpFilterList();
			}
		}
		
		return this;
	}
	
	private QueryFilter cleanUpFilterList() {
		if (FilterList.isEmpty())
			return null;
		else if (FilterList.size() == 1) {
			return FilterList.get(0);
		}
		return this;
	}
	
	public QueryFilter replaceAttributeFilter(String dimension, String column, QueryFilter filterToReplace) throws CloneNotSupportedException{
		if (this.Type == TYPE_PREDICATE)  {
			if (containsAttribute(dimension, column)){
				copy(filterToReplace);
			}
			return this;
		}
		else {
			for (ListIterator<QueryFilter> iter = FilterList.listIterator(); iter.hasNext(); ) {
				QueryFilter qf = iter.next();
				if (qf.containsAttribute(dimension, column)) {
					iter.set(qf.replaceAttributeFilter(dimension, column, filterToReplace));
				}
			}
		}
		
		return this;
	}
	
	/**
	 * In some cases, query parsing can generate a logical operator filter containing multiple filters. If one of
	 * the filters in the children is a measure filter, the physical query generated ends up placing the entire condition
	 * in the having clause and wrapping such queries can cause issues. So, if one of the filters contains a measure
	 * and if all the logical operators are actually "AND", we can split them into multiple filters and then add each
	 * one individually to the query. That way, the dimension part can be in where condition and measure part can be
	 * in having clause - generating correct physical queries.
	 * This method is used by new logical query string parsing code after it has parsed the entire where clause and
	 * all the conditions mentioned in the where clause are part of one single filter object as logical operator filter.
	 * @param alwaysSplit - if true, split into filters for logical operator, if false, only split if the logical filter
	 * tree contains any measures and all of them are AND operators. true is passed for the recursive call from within the 
	 * method for optimization.
	 * @return null if the filters can't or don't need to be split, otherwise an array list containing individual filters
	 */
	public List<QueryFilter> splitIntoIndividualFilters(boolean alwaysSplit, boolean oneLevel)
	{
		List<QueryFilter> qfList = new ArrayList<QueryFilter>();
		if(Type == TYPE_LOGICAL_OP && (alwaysSplit || (containsMeasure() && Operator.equals("AND"))))
		{
			if(FilterList != null && FilterList.size() > 0)
			{
				for(QueryFilter qf : FilterList)
				{
					if(qf.Type == TYPE_LOGICAL_OP && !oneLevel)
					{
						//We have already checked the entire filter tree to see that one of the filters contains
						//a measure and all of them 
						qfList.addAll(qf.splitIntoIndividualFilters(true, false));
					}
					else
					{
						qfList.add(qf);
					}
				}
			}
		}
		else
		{
			return null;
		}
		
		return qfList;
	}
	
	/**
	 * Find out if any of the logical operator filters contains an operator other than "AND".  
	 * @return true if an operator other than "AND" is found, false otherwise
	 */
	public boolean containsOnlyANDOperator()
	{
		if(this.Type == TYPE_LOGICAL_OP)
		{
			if(!("AND".equals(Operator)))
			{
				return false;
			}
			if(FilterList != null && FilterList.size() > 0)
			{
				for(QueryFilter qf : FilterList)
				{
					if(qf.Type == TYPE_LOGICAL_OP)
					{
						if(!qf.containsOnlyANDOperator())
						{
							//Found a logical filter with operator other than "AND"
							return false;
						}
					}
				}
			}
		}
		return true;
	}
	
	public boolean containsOrOnDifferentConnections(DatabaseConnection fconn)
	{
		if (this.Type == TYPE_LOGICAL_OP && "OR".equals(Operator))
		{
			if (FilterList != null && FilterList.size() > 0)
			{
				for (QueryFilter qf : FilterList)
				{
					if (qf.Type == TYPE_LOGICAL_OP)
					{
						if (qf.containsOrOnDifferentConnections(fconn))
						{
							return true;
						}
					}	
					if (fconn == null)
					{
						fconn = qf.getColumnConnection();
					}
					if (fconn != qf.getColumnConnection())
					{
						return true;
					}					
				}
			}
		}
		return false;
	}
	
	public boolean isFilterOnDifferentConnections(DatabaseConnection fconn)
	{
		if (this.Type == TYPE_LOGICAL_OP)
		{
			if (FilterList != null && FilterList.size() > 0)
			{
				for (QueryFilter qf : FilterList)
				{
					if (qf.Type == TYPE_LOGICAL_OP)
					{
						if (qf.isFilterOnDifferentConnections(fconn))
						{
							return true;
						}
					}	
					if (fconn == null)
					{
						fconn = qf.getColumnConnection();
					}
					if (fconn != qf.getColumnConnection())
					{
						return true;
					}					
				}
			}
		}
		return false;
	}
		
	private String getDbSpecificOperandValue(DatabaseType dbType, String opvalue)
	{
		if (dbType == DatabaseType.SAPBWXMLA)
		{
			//For SAP BW, we need to use member name only for filtering - query resultset should 
			//contain member name enclosed between [ and ] at the end of the value provided.
			if(opvalue.contains("[") && opvalue.endsWith("]"))
			{
				opvalue = opvalue.substring(opvalue.lastIndexOf("[") + 1, opvalue.length() - 1);
			}
		}
		return opvalue;
	}

	@SuppressWarnings("unchecked")
	private boolean isSuperSetOf(Repository r, Session session, QueryFilter otherqf)
	{
		if (Type == TYPE_LOGICAL_OP && Operator.equals("AND"))
		{
			boolean isSuper = true;
			for (QueryFilter child : FilterList)
			{
				isSuper = isSuper && child.isSuperSetOf(r, session, otherqf);
			}
			return isSuper;
		}
		if (Type == TYPE_DIMENSION_COLUMN && otherqf.Type == TYPE_LOGICAL_OP && otherqf.Operator.equals("AND"))
		{
			for (QueryFilter ochild : otherqf.FilterList)
			{
				if (isSuperSetOf(r, session, ochild))
					return true;
			}
			return false;
		}
		//Check to see if both filters are on dimension columns with the same operator and operand
		if(Type != TYPE_DIMENSION_COLUMN || otherqf.Type != TYPE_DIMENSION_COLUMN)
		{
			return false;
		}
		if (!Operator.equals(otherqf.Operator))
		{
			if (Operator.equals(">=") && !(otherqf.Operator.equals(">") || otherqf.Operator.equals("=")))
				return false;
			if (Operator.equals("<=") && !(otherqf.Operator.equals("<") || otherqf.Operator.equals("=")))
				return false;
			if ((Operator.equals(QueryFilter.LIKE) && otherqf.Operator.equals(QueryFilter.NOTLIKE)) || (Operator.equals(QueryFilter.NOTLIKE) && otherqf.Operator.equals(QueryFilter.LIKE)))
				return false;
		}
		//Dont proceed further if both filters are not on the same dimension column
		DimensionColumn thisdc = r.findDimensionColumn(this.Dimension, this.Column);
		DimensionColumn otherdc = r.findDimensionColumn(otherqf.Dimension, otherqf.Column);
		
		//Make sure both dimension columns can be found in the repository have the same data type
		if(thisdc == null || otherdc == null || thisdc != otherdc 
			|| thisdc.DataType == null || otherdc.DataType == null || !thisdc.DataType.equals(otherdc.DataType))
		{
			return false;
		}
		
		//Must have valid operands
		if(this.Operand == null || otherqf.Operand == null)
		{
			return false;
		}
		
		Comparable thisTypedOperand = null;
		Comparable otherTypedOperand = null;
		if(thisdc.DataType.equalsIgnoreCase("VARCHAR"))
		{
			thisTypedOperand = r.replaceVariables(session, this.Operand);
			otherTypedOperand = r.replaceVariables(session, otherqf.Operand);
		}
		else if(thisdc.DataType.equalsIgnoreCase("INTEGER"))
		{
			thisTypedOperand = new Integer(r.replaceVariables(session, this.Operand));
			otherTypedOperand = new Integer(r.replaceVariables(session, otherqf.Operand));
		}
		else if (thisdc.DataType.equalsIgnoreCase("DATE"))
		{
			//Get rid of any quotes around the date string, if present
			if (this.Operand != null && this.Operand.startsWith("'") && this.Operand.endsWith("'") && this.Operand.length() > 2)
				this.Operand = this.Operand.substring(1, this.Operand.length() - 1);
			thisTypedOperand = DateUtil.parseUsingPossibleFormats(r.replaceVariables(session, this.Operand));
			if(thisTypedOperand == null)
			{
				logger.warn("Could not parse date " + this.Operand + " while processing query filter. This may result in incorrect, different or failed query navigation.");
				return false;
			}

			//Get rid of any quotes around the date string, if present			
			if (otherqf.Operand != null && otherqf.Operand.startsWith("'") && otherqf.Operand.endsWith("'") && otherqf.Operand.length() > 2)
				otherqf.Operand = otherqf.Operand.substring(1, otherqf.Operand.length() - 1);
			otherTypedOperand = DateUtil.parseUsingPossibleFormats(r.replaceVariables(session, otherqf.Operand));
			if(otherTypedOperand == null)
			{
				logger.warn("Could not parse date " + otherqf.Operand + " while processing query filter. This may result in incorrect, different or failed query navigation.");
				return false;
			}
			
		}
		else
		{
			//For now, we don't do float - it would be rare case for dimension columns anyway
			return false;
		}

		boolean isSuperSet = false;
		int compareResult = thisTypedOperand.compareTo(otherTypedOperand);
		if(compareResult == 0)
		{
			//No further check necessary - they are the same, we are in the neighborhood probably by mistake
			isSuperSet = true;
		}
		else if(compareResult < 0 && (this.Operator.equals(">") || this.Operator.equals(">=")))
		{
			//this operator is less than the other one and condition is greater than e.g. thisyear > 2010 & otheryear > 2011
			isSuperSet = true;
		}
		else if(compareResult > 0 && (this.Operator.equals("<") || this.Operator.equals("<=")))
		{
			//this operator is greater than the other one and condition is less than e.g. thisyear < 2011 & otheryear < 2011
			isSuperSet = true;
		}
		
		return isSuperSet;
	}
	
	public static QueryFilter createFilterUsingNQL(String filterStr, Repository r) 
	{
		QueryFilter qf = null;
		ANTLRStringStream stream = new ANTLRStringStream(filterStr);
		BirstScriptLexer lex = new BirstScriptLexer(stream);
		CommonTokenStream cts = new CommonTokenStream(lex);
		BirstScriptParser parser = new BirstScriptParser(cts);
		try
		{
			logicalExpression_return ret = parser.logicalExpression();
			if (lex.hasError())
				LogicalQueryString.processLexError(lex, filterStr);
			qf = LogicalQueryString.validateTreeFilter((Tree) ret.getTree(), r, null, false);
			return qf;
		} catch (SyntaxErrorException e)
		{
			logger.error("Could not parse filter string: " + filterStr);
		} catch (RecognitionException e)
		{
			logger.error("Could not parse filter string: " + filterStr);
		} catch (ScriptException e)
		{
			logger.error("Could not parse filter string: " + filterStr);
		}
		return null;
	}
	
	public void setIsTrellisChart(boolean isTrellis)
	{
		this.isTrellisChartFilter = isTrellis;
	}
	
	public boolean isTrellisChart()
	{
		return isTrellisChartFilter;
	}
	
	/**
	 * return true if all predicates in this filter contain dimension attributes, false if there are any measures
	 * - no clue how to handle Text
	 * @return
	 */
	public boolean containsOnlyAttributes()
	{
		if (Type == QueryFilter.TYPE_MEASURE)
			return false;
		else if (Type == QueryFilter.TYPE_DIMENSION_COLUMN)
			return true;
		else if (Type == QueryFilter.TYPE_LOGICAL_OP)
		{
			for (int i = 0; i < FilterList.size(); i++)
			{
				QueryFilter qf = FilterList.get(i);
				boolean res = qf.containsOnlyAttributes();
				if (!res)
					return false;
			}
			return true;
		}
		else
			return false; // Text
	}
	
	/**
	 * return true if all predicates in this filter contain measures, false if there are any dimension attributes
	 * - no clue how to handle Text
	 * @return
	 */
	public boolean containsOnlyMeasures()
	{
		if (Type == QueryFilter.TYPE_MEASURE)
			return true;
		else if (Type == QueryFilter.TYPE_DIMENSION_COLUMN)
			return false;
		else if (Type == QueryFilter.TYPE_LOGICAL_OP)
		{
			for (int i = 0; i < FilterList.size(); i++)
			{
				QueryFilter qf = FilterList.get(i);
				boolean res = qf.containsOnlyMeasures();
				if (!res)
					return false;
			}
			return true;
		}
		else
			return false; // Text
	}

	/**
	 * Get all filters in the query, but expand ands into individual filters 
	 * @return
	 */
	public static List<QueryFilter> getExpandedFilterList(List<QueryFilter> FilterList)
	{
		List<QueryFilter> resultList= new ArrayList<QueryFilter>();
		for(QueryFilter qf: FilterList)
		{
			if (qf.getType() == QueryFilter.TYPE_LOGICAL_OP && qf.getOperator().equals("AND"))
				resultList.addAll(QueryFilter.getExpandedFilterList(qf.FilterList));
			else
				resultList.add(qf);
		}
		return resultList;
	}

	/**
	 * Test for unary operators (IS NULL, IS NOT NULL )
	 * @return true or false
	 */
	public boolean isUnaryOperator() {
		if(Operator != null){
		    if(Operator.equals(ISNULL) || (Operator.equals(ISNOTNULL))){
			    return true;
		    }
		}
		return false;
	}
	
	public String getHierarchy() {
		return hierarchy;
	}
	
	public void setHierarchy(String h) {
		hierarchy = h;
	}

	public DatabaseConnection getColumnConnection()
	{
		DatabaseConnection dconn = null;
		if(Type == TYPE_LOGICAL_OP)
		{
			for (QueryFilter qf : FilterList)
			{
				dconn = qf.getColumnConnection();
				if (dconn != null)
					return (dconn);
			}
		}
			
		if(Type == TYPE_PREDICATE)
		{
			if(ColumnType == TYPE_DIMENSION_COLUMN)
			{
				dconn = dc.DimensionTable.TableSource.connection;
			}
			else if(ColumnType == TYPE_MEASURE)
			{
				dconn = mc.MeasureTable.TableSource.connection;
			}
		}

		return dconn;
	}
	
	public Object getColumnObject()
	{
		if (ColumnType == TYPE_DIMENSION_COLUMN)
		{
			return dc;
		}
		else if (ColumnType == TYPE_MEASURE)
		{
			return mc;
		}
		return null;
	}
}
