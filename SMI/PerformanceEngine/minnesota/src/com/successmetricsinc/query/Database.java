/**
 * $Id: Database.java,v 1.344 2012-12-06 18:57:25 BIRST\ricks Exp $
 *
 * Copyright (C) 2007-2009 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.SecretKey;

import org.apache.log4j.Logger;
import org.postgresql.util.PSQLException;
import org.postgresql.util.PSQLState;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.successmetricsinc.Repository;
import com.successmetricsinc.security.EncryptionService;
import com.successmetricsinc.transformation.ScriptLog;
import com.successmetricsinc.util.AWSUtil;
import com.successmetricsinc.util.SimpleMRUCache;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.warehouse.DateID;
import com.successmetricsinc.warehouse.GenerateSchema;
import com.successmetricsinc.warehouse.SourceFile;
import com.successmetricsinc.warehouse.StagingColumn;
import com.successmetricsinc.warehouse.StagingTable;
import com.successmetricsinc.warehouse.StagingTableBulkLoadInfo;
import com.successmetricsinc.warehouse.TimeDefinition;
/**
 * Class to generate database-specific SQL
 * 
 * what we should do is have this be referenced only through DatabaseConnection, so all operations are scoped by the
 * connection type
 * 
 * @author bpeters
 * 
 */
public class Database
{
	// Connection Info
	public enum DatabaseType
	{
		None, MSSQL, MSSQL2005, MYSQL, ODBCExcel, SiebelAnalytics, MSSQL2008, GoogleAnalytics, Infobright, Oracle, MonetDB, ODBCOracle, ODBCMySQL, ODBCMSSQL,
		ODBCDB2, MSASXMLA, MONDRIANXMLA, EssbaseXMLA, SAPBWXMLA, Vertica, Teradata, MemDB, SybaseIQ, ParAccel, HiveHadoop, MSSQL2012, MSSQL2012ColumnStore, PostgreSQL,
		IMPALA, Redshift, Hana, MSSQL2014
	}
	private static Logger logger = Logger.getLogger(Database.class);
	private static Map<DatabaseConnection,List<String>> tableCache = Collections.synchronizedMap(new SimpleMRUCache<DatabaseConnection,List<String>>(1000));
	public static String MEM_AGG_CONNECTION = "In-memory Aggregate";

	public static void clearCacheForConnection(DatabaseConnection dc)
	{
		tableCache.remove(dc);
	}

	/**
	 * Create an index for a table
	 * 
	 * @param c
	 *            Database connection to use
	 * @param columnName
	 *            Column to index
	 * @param tableName
	 *            Table to index
	 * @param clustered
	 *            Create a clustered index (if database supports it)
	 * @return
	 */
	public static boolean createIndex(DatabaseConnection dc, String columnName, String tableName, boolean clustered, boolean qualifyNew) throws SQLException
	{
		if (!dc.supportsIndexes())
			return true;
		boolean res = Database.indexExistsOnColumn(dc, tableName, columnName);
		if (res)
		{
			logger.debug("Index on " + getQualifiedTableName(dc, tableName) + ":" + columnName + " already exists");
			return false;
		} else
		{
			LevelKey lk = new LevelKey();
			lk.ColumnNames = new String[]
			{ Util.unQualifyPhysicalName(columnName) };
			return (createIndex(dc, lk, tableName, clustered, false, null, null, qualifyNew));
		}
	}

	public static boolean createIndex(DatabaseConnection dc, LevelKey lk, String tableName, boolean clustered, boolean measuretable, DimensionTable dt, Level l)
			throws SQLException
	{
		return (createIndex(dc, lk, tableName, clustered, measuretable, dt, l, false));
	}

	/**
	 * Create an index for a table
	 * 
	 * @param c
	 *            Database connection to use
	 * @param lk
	 *            Level key to index
	 * @param tableName
	 *            Table to index
	 * @param clustered
	 *            Create a clustered index (if database supports it)
	 * @return
	 */
	public static boolean createIndex(DatabaseConnection dc, LevelKey lk, String tableName, boolean clustered, boolean measuretable, DimensionTable dt,
			Level l, boolean qualifyNew) throws SQLException
	{
		 return createIndex(dc, lk, tableName,  clustered, measuretable, dt, l,  qualifyNew,null);
	}
	public static boolean createIndex(DatabaseConnection dc, LevelKey lk, String tableName, boolean clustered, boolean measuretable, DimensionTable dt,
			Level l, boolean qualifyNew,Repository r) throws SQLException
	{
		if (!dc.supportsIndexes())
			return true;
		if (lk.ColumnNames.length >= 16)
		{
			logger.warn("Skipping creation of index for: " + getQualifiedTableName(dc, tableName)
					+ "; There are too many columns in the index definition, maximum allowed is 16");
			if (logger.isDebugEnabled())
			{
				logger.debug("Skipping creation of index for: " + getQualifiedTableName(dc, tableName) + "; Columns requested in index are:"
						+ Arrays.asList(lk.ColumnNames));
			}
			return false;
		}
		boolean res = Database.indexExistsOnKey(dc, tableName, lk, measuretable, qualifyNew,r);
		StringBuilder sb = new StringBuilder();
		boolean first = true;
		boolean requiresHashedIdentifier = dc.requiresHashedIdentifier();
		for (String s : lk.ColumnNames)
		{
			String col = null;
			if (first)
				first = false;
			else
				sb.append(',');
			col = s;
			if (!col.equals("LOAD_ID"))
				col = (measuretable ? Util.replaceWithPhysicalString(lk.Level.Hierarchy.DimensionName,r) + dc.getPhysicalSuffix() : "")
						+ (qualifyNew ? Util.replaceWithNewPhysicalString(s, dc,r) : Util.replaceWithPhysicalString(s,r));
			if (requiresHashedIdentifier)
			{
				String hashedStr;
				if(col != null && col.endsWith("CKSUM$"))
				{
					col = dc.getChecksumName(col);
				}
				else
				{
					DimensionColumn curdc = dt == null ? null : dt.findColumn(s);
					if (curdc == null)
					{
						hashedStr = dc.getHashedIdentifier(col);
						if (!col.equalsIgnoreCase(hashedStr))
							col = qualifyNew ? Util.replaceWithNewPhysicalString(hashedStr, dc,r) : Util.replaceWithPhysicalString(hashedStr);
					}
					else
					{
						col = curdc.PhysicalName;
						if(col.indexOf(".") >= 0)
						{
							col = col.substring(col.indexOf(".")+1);
						}
					}	
				}
			}
			sb.append(col);
		}
		if (res)
		{
			logger.debug("Index on " + getQualifiedTableName(dc, tableName) + ":" + sb.toString() + " already exists");
			return false;
		} else
		{
			if (clustered)
			{
				if (!dc.supportsClusteredIndexes())
				{
					logger.warn("Clustered indexes are only supported on SQL 2000 and SQL 2005, creating non clustered index");
					clustered = false;
				}
			}
			Connection conn = dc.ConnectionPool.getConnection();
			if (conn.getMetaData().isReadOnly())
			{
				logger.warn("Read-only database, will not create index on table " + tableName);
				return true;
			}
			String indexName = getIndexName(dc, tableName, lk, measuretable, dc.DBType == DatabaseType.MYSQL ? MAX_MYSQL_INDEX_NAME_LENGTH
					: (dc.DBType == DatabaseType.Oracle ? MAX_ORACLE_INDEX_NAME_LENGTH : MAX_SQL_SERVER_INDEX_NAME_LENGTH),r);
			StringBuilder query = new StringBuilder("CREATE ");
			if (clustered)
				query.append("CLUSTERED ");
			query.append("INDEX ");
			query.append(dc.DBType == DatabaseType.Oracle ? dc.Schema + "." : "");
			query.append(indexName);
			query.append(" ON ");
			query.append(getQualifiedTableName(dc, tableName));
			query.append(" (");
			query.append(sb);
			query.append(')');
			if (l != null && !measuretable && dt != null && dc.supportsIndexIncludes())
			{
				/*
				 * If a level (and a dimension table) are supplied, then this key being used on a created dimension
				 * table. Add higher level surrogate keys to the index to speed up surrogate key lookups
				 */
				first = true;
				List<Level> llist = new ArrayList<Level>();
				llist.add(l);
				l.getAllParents(llist);
				Set<String> seen = new HashSet<String>(); // make sure you only add the column once
				for (Level keyl : llist)
					for (LevelKey slk : keyl.Keys)
					{
						if (slk != lk && slk.SurrogateKey)
						{
							for (String s : slk.ColumnNames)
							{
								if (seen.contains(s))
									continue;
								if (Arrays.asList(lk.ColumnNames).indexOf(s) < 0)
								{
									boolean exists = false;
									for (DimensionColumn col : dt.DimensionColumns)
									{
										if (col.ColumnName.equals(s))
										{
											exists = true;
											break;
										}
									}
									if (exists)
									{
										if (first)
										{
											first = false;
											query.append(" INCLUDE (");
										} else
											query.append(',');
										query.append(Util.replaceWithNewPhysicalString(s, dc,r));
										seen.add(s);
									}
								}
							}
						}
					}
				if (!first)
					query.append(')');
			}
			if (dc.useCompression() && dc.supportsMSSQLDBCompression())
			{
				query.append(" WITH (DATA_COMPRESSION = PAGE)");
			}
			logger.debug(Query.getPrettyQuery(query.toString()));
			Statement stmt = null;
			try
			{
				stmt = conn.createStatement();
				stmt.executeUpdate(query.toString());
			} catch (Exception e)
			{
				logger.error(e, e);
			} finally
			{
				closeStatement(stmt);
			}
			return true;
		}
	}

	/**
	 * Drops an index for a given level key
	 * 
	 * @param dc
	 *            database connection object
	 * @param lk
	 *            level key to be indexed
	 * @param tableName
	 *            name of the table to be indexed
	 * @param measuretable
	 *            is this a measure table
	 * @throws SQLException
	 */
	public static boolean dropIndex(DatabaseConnection dc, LevelKey lk, String tableName, boolean measuretable) throws SQLException
	{
		return dropIndex(dc, lk, tableName, measuretable,false);
	}
	public static boolean dropIndex(DatabaseConnection dc, LevelKey lk, String tableName, boolean measuretable,boolean qualifynew) throws SQLException
	{
		return dropIndex(dc, lk, tableName, measuretable,qualifynew, null);
	}

	public static boolean dropIndex(DatabaseConnection dc, LevelKey lk, String tableName, boolean measuretable,boolean qualifynew,Repository r) throws SQLException
	{
		if (!dc.supportsIndexes())
			return true;
		boolean res = Database.indexExistsOnKey(dc, tableName, lk, measuretable, qualifynew,r);
		if (!res)
			return (false);
		String indexName = getIndexName(dc, tableName, lk, measuretable, dc.DBType == DatabaseType.MYSQL ? MAX_MYSQL_INDEX_NAME_LENGTH
				: (dc.DBType == DatabaseType.Oracle ? MAX_ORACLE_INDEX_NAME_LENGTH : MAX_SQL_SERVER_INDEX_NAME_LENGTH),r);
		Connection conn = dc.ConnectionPool.getConnection();
		String qualifiedTableName = getQualifiedTableName(dc, tableName);
		return dropIndex(dc, conn, indexName, qualifiedTableName);
	}
	
	/**
	 * drop an index for a given column on a table
	 * 
	 * @param dc
	 *            database connection object
	 * @param columnName
	 *            column to be indexed
	 * @param tableName
	 *            name of the table to be indexed
	 * @return
	 * @throws SQLException
	 */
	public static boolean dropIndex(DatabaseConnection dc, String columnName, String tableName) throws SQLException
	{
		if (!dc.supportsIndexes())
			return true;
		boolean res = Database.indexExistsOnColumn(dc, tableName, columnName);
		if (!res)
		{
			logger.debug("Index on " + getQualifiedTableName(dc, tableName) + ":" + columnName + " does not exist, no need to drop it");
			return (false);
		} else
		{
			LevelKey lk = new LevelKey();
			lk.ColumnNames = new String[]
			{ Util.unQualifyPhysicalName(columnName) };
			return (dropIndex(dc, lk, tableName, false));
		}
	}

	public static boolean dropIndex(DatabaseConnection dbc, Connection conn, String indexName, String qualifiedTableName) throws SQLException
	{
		if (!dbc.supportsIndexes())
			return true;
		logger.info("Dropping index " + indexName);
		String query = null;
		if (DatabaseConnection.isDBTypeOracle(dbc.DBType))
		{
			query = "DROP INDEX " + getQualifiedIndexName(dbc, indexName);
		}
		else
		{
			query = "DROP INDEX " + indexName + " ON " + qualifiedTableName;
		}
		logger.debug(Query.getPrettyQuery(query));
		if (conn.getMetaData().isReadOnly())
		{
			logger.warn("Read-only database, will not drop index on table " + qualifiedTableName);
			return true;
		}
		Statement stmt = null;
		try
		{
			stmt = conn.createStatement();
			stmt.executeUpdate(query);
		} catch (Exception msee)
		{
			logger.debug(msee, msee);
		} finally
		{
			closeStatement(stmt);
		}
		return (true);
	}
	
	public static boolean dropColumnStoreIndex(DatabaseConnection dc, String tableName) throws SQLException
	{
		if (dc.DBType != DatabaseType.MSSQL2012ColumnStore && dc.DBType != DatabaseType.MSSQL2014)
			return true;

		String indexName = dc.getColumnStoreIndexName(tableName);
		String qualifiedTableName = Database.getQualifiedTableName(dc, tableName);

		Connection conn = dc.ConnectionPool.getConnection();
		if (conn.getMetaData().isReadOnly())
		{
			logger.warn("Read-only database, will not drop index on table " + qualifiedTableName);
			return true;
		}

		String query = "DROP INDEX [" + indexName + "] ON " + qualifiedTableName;
		logger.debug(Query.getPrettyQuery(query));

		Statement stmt = null;
		try
		{
			stmt = conn.createStatement();
			stmt.executeUpdate(query);
		} 
		catch (Exception ex)
		{
			logger.debug(ex, ex);
		} 
		finally
		{
			closeStatement(stmt);
		}
		return (true);
	}


	/**
	 * Drops all indices that reference a given column
	 * 
	 * @param dc
	 * @param columnName
	 * @param tableName
	 * @throws SQLException
	 */
	private static void dropAllIndicesOnColumn(DatabaseConnection dc, String columnName, String tableName) throws SQLException
	{
		if (!dc.supportsIndexes())
			return;
		String qualifiedTableName = getQualifiedTableName(dc, tableName);
		Connection conn = dc.ConnectionPool.getConnection();
		java.sql.DatabaseMetaData meta = conn.getMetaData();
		ResultSet set = meta.getIndexInfo(dc.DBType == DatabaseType.MYSQL ? dc.Schema : null, dc.DBType == DatabaseType.MYSQL ? null : dc.Schema, tableName,
				false, true);
		Set<String> indicesToBeDropped = new HashSet<String>();
		while (set.next())
		{
			String indexName = set.getString("INDEX_NAME");
			String indexColumnName = set.getString("COLUMN_NAME");
			if (indexColumnName != null && indexColumnName.equalsIgnoreCase(columnName))
			{
				indicesToBeDropped.add(indexName);
			}
		}
		set.close();
		for (String indexName : indicesToBeDropped)
		{
			dropIndex(dc, conn, indexName, qualifiedTableName);
		}
	}

	/**
	 * Create an index for a table
	 * 
	 * @param c
	 *            Database connection to use
	 * @param columnName
	 *            Column to index
	 * @param tableName
	 *            Table to index
	 * @return
	 */
	public static boolean createIndex(DatabaseConnection dc, String columnName, String tableName) throws SQLException
	{
		return (createIndex(dc, columnName, tableName, false, false));
	}

	/**
	 * Drop a table
	 * 
	 * @param dc
	 *            Database connection to use
	 * @param tableName
	 *            Table to drop
	 * @return
	 */
	public static boolean dropTable(Repository r, DatabaseConnection dc, String tableName, boolean checkTableExists) throws SQLException
	{
		logger.debug("Dropping table: " + tableName);
		if (checkTableExists && !tableExists(r, dc, tableName, false))
			return false;
		tableCache.remove(dc);
		String query = "DROP TABLE " + getQualifiedTableName(dc, tableName);
		if (dc.isRealTimeConnection())
		{
			DatabaseConnectionStatement statement = null;
			try
			{
				statement = new DatabaseConnectionStatement(r, dc);
				logger.debug(Query.getPrettyQuery(query));
				statement.executeUpdate(query);
			} catch (Exception e)
			{
				logger.error(e, e);
			}
		}
		else
		{
			Connection conn = dc.ConnectionPool.getConnection();
			if (conn.getMetaData().isReadOnly())
			{
				logger.warn("Read-only database, will not drop table " + tableName);
				return true;
			}
			logger.debug(Query.getPrettyQuery(query));
			Statement stmt = null;
			try
			{
				stmt = conn.createStatement();
				stmt.executeUpdate(query);
			} catch (Exception e)
			{
				logger.error(e, e);
			} finally
			{
				closeStatement(stmt);
			}
		}
		return true;
	}

	public static void dropTables(Repository r, DatabaseConnection dc, Set<String> tnames) throws SQLException
	{
		tableCache.remove(dc);
		Statement stmt = null;
		try
		{
			if (DatabaseConnection.isDBTypeInfoBright(dc.DBType) && tnames.size() > 0)
			{
				Connection conn = dc.ConnectionPool.getConnection();
				if (conn.getMetaData().isReadOnly())
				{
					logger.warn("Read-only database, will not drop tables.");
					return;
				}
				stmt = conn.createStatement();
				String dropTempSQL = "DROP TABLE IF EXISTS ";
				for (String tableName : tnames)
				{
					String qualifiedTableName = getQualifiedTableName(dc, tableName);
					logger.debug("Dropping table: " + qualifiedTableName);
					stmt.executeUpdate(dropTempSQL + qualifiedTableName);
				}
			}
		} catch (Exception e)
		{
			logger.error(e, e);
		} finally
		{
			closeStatement(stmt);
		}
	}

	/**
	 * Rename a table
	 * 
	 * @param dc
	 *            Database connection to use
	 * @param tableName
	 *            Table to drop
	 * @return
	 */
	public static boolean renameTable(Repository r, DatabaseConnection dc, String oldName, String newName) throws SQLException
	{
		logger.info("Renaming table: " + getQualifiedTableName(dc, oldName) + " to " + getQualifiedTableName(dc, newName));
		if (!tableExists(r, dc, oldName, false))
			return false;
		Connection conn = dc.ConnectionPool.getConnection();
		if (conn.getMetaData().isReadOnly())
		{
			logger.warn("Read-only database, will not rename table " + oldName);
			return true;
		}
		String query = null;
		if (dc.DBType == DatabaseType.MYSQL || dc.DBType == DatabaseType.Infobright  || dc.DBType == DatabaseType.MemDB)
		{
			query = "RENAME TABLE " + getQualifiedTableName(dc, oldName) + " TO " + getQualifiedTableName(dc, newName);
		} else if (DatabaseConnection.isDBTypeMSSQL(dc.DBType))
		{
			query = "sp_RENAME '" + Database.getQualifiedTableName(dc, oldName) + "','" + newName + "'";
		} else if (DatabaseConnection.isDBTypeSAPHana(dc.DBType))
		{
			query = "RENAME TABLE " + Database.getQualifiedTableName(dc, oldName) + " TO " + newName;
		} else if (dc.supportsAlterRename())
		{
			query = "ALTER TABLE " + getQualifiedTableName(dc, oldName) + " RENAME TO " + newName;
		} else
		{
			logger.warn("Database does not support rename, will not rename table " + oldName);
			return true;
		}
		logger.debug(Query.getPrettyQuery(query));
		Statement stmt = null;
		try
		{
			stmt = conn.createStatement();
			stmt.executeUpdate(query);
			clearCacheForConnection(dc);
		} catch (Exception e)
		{
			logger.error(e, e);
		} finally
		{
			closeStatement(stmt);
		}
		return true;
	}
	
	public static boolean renameIndex(DatabaseConnection dc, String tableName, String oldName, String newName) throws SQLException
	{
		logger.info("Renaming index: " + oldName + " to " + newName + " for table " + tableName);
		if (!indexExists(dc, tableName, oldName) || indexExists(dc, tableName, newName))
			return false;
		Connection conn = dc.ConnectionPool.getConnection();
		if (conn.getMetaData().isReadOnly())
		{
			logger.warn("Read-only database, will not rename index " + oldName);
			return true;
		}
		String query = null;
		if (DatabaseConnection.isDBTypeMSSQL(dc.DBType))
		{
			oldName = tableName + "." + oldName;
			query = "sp_RENAME '" + getQualifiedIndexName(dc, oldName) + "','" + newName + "','INDEX'";
		} else if (DatabaseConnection.isDBTypeOracle(dc.DBType))
		{
			query = "ALTER INDEX " +  getQualifiedIndexName(dc, oldName) + " RENAME TO " + newName;
		} else
		{
			logger.warn("Database does not support rename, will not rename index " + oldName);
			return true;
		}
		logger.debug(Query.getPrettyQuery(query));
		Statement stmt = null;
		try
		{
			stmt = conn.createStatement();
			stmt.executeUpdate(query);
		} catch (Exception e)
		{
			logger.error(e, e);
		} finally
		{
			closeStatement(stmt);
		}
		return true;
	}

	public static boolean deleteOldData(DatabaseConnection dc, String tableName, Map<String, String> valueByColumn) throws SQLException
	{
		return deleteOldData(dc, tableName, valueByColumn, null);
	}

	/**
	 * Delete old data from the table
	 * 
	 * @param dc
	 *            Database connection
	 * @param tableName
	 *            table to delete from
	 * @param valueByColumn
	 *            list of columns and values to be used in the WHERE clause of the DELETE
	 * @param neqValueByColumn
	 *            list of columns and values to be ignored during delete
	 */
	public static boolean deleteOldData(DatabaseConnection dc, String tableName, Map<String, String> valueByColumn, Map<String, List<String>> neqValueByColumn)
			throws SQLException
	{
		logger.info("Deleting previous data from " + tableName);
		StringBuilder queryBuf = new StringBuilder();
		queryBuf.append("DELETE FROM ");
		queryBuf.append(getQualifiedTableName(dc, tableName));
		List<String> columnList = new ArrayList<String>();
		if (valueByColumn != null)
		{
			for (Iterator<String> iter = valueByColumn.keySet().iterator(); iter.hasNext();)
			{
				columnList.add(iter.next());
			}
		}
		List<String> neqColumnList = new ArrayList<String>();
		if (neqValueByColumn != null && neqValueByColumn.size() > 0)
		{
			for (Iterator<String> neqIter = neqValueByColumn.keySet().iterator(); neqIter.hasNext();)
			{
				neqColumnList.add(neqIter.next());
			}
		}
		boolean firstT = true;
		for (String column : columnList)
		{
			if (firstT)
			{
				queryBuf.append(" WHERE ");
				firstT = false;
			} else
			{
				queryBuf.append(" AND ");
			}
			queryBuf.append(column);
			String value = valueByColumn.get(column);
			if (value != null)
			{
				queryBuf.append("='");
				queryBuf.append(value);
				queryBuf.append("'");
			} else
			{
				queryBuf.append(" IS NULL");
			}
		}
		if (neqColumnList != null && neqColumnList.size() > 0)
		{
			for (String neqColumn : neqColumnList)
			{
				List<String> neqValues = neqValueByColumn.get(neqColumn);
				if (neqValues == null)
				{
					continue;
				}
				for (String neqValue : neqValues)
				{
					if (firstT)
					{
						queryBuf.append(" WHERE ");
						firstT = false;
					} else
					{
						queryBuf.append(" AND ");
					}
					queryBuf.append(neqColumn);
					queryBuf.append("!='");
					queryBuf.append(neqValue);
					queryBuf.append("'");
				}
			}
		}
		String query = queryBuf.toString();
		logger.debug(Query.getPrettyQuery(query));
		Connection conn = dc.ConnectionPool.getConnection();
		Statement stmt = null;
		try
		{
			stmt = conn.createStatement();
			int count = stmt.executeUpdate(query);
			logger.debug("Deleted " + count + " rows from " + tableName);
		} finally
		{
			closeStatement(stmt);
		}
		return true;
	}

	/**
	 * Return whether a table exists or not
	 * 
	 * @param c
	 *            Database connection to use
	 * @param tableName
	 * @return
	 * @throws SQLException
	 */
	public static boolean tableExists(Repository r, DatabaseConnection dc, String tableName, boolean useCache) throws SQLException
	{
		return tableExists(r, dc, dc.Schema, tableName, useCache);
	}

	/**
	 * Return whether a table exists or not
	 * 
	 * @param c
	 *            Database connection to use
	 * @param tableName
	 * @return
	 * @throws SQLException
	 */
	public static boolean tableExists(Repository r, DatabaseConnection dc, String schema, String tableName, boolean useCache) throws SQLException
	{
		// if no DB, just assume that the table exists (needed for processAggregateTables in Repository)
		if (dc!=null && !dc.isValid())
			return true;
		if (dc!=null && dc.isRealTimeConnection())
		{
			return tableExistsOnLiveAccess(r, dc, tableName);
		} else
		{
			if (dc!=null && (schema == null || schema.length() == 0))
				schema = dc.Schema;
			// Get tables from current catalog
			List<String> tables = getTables(dc, null, schema, (useCache ? null: tableName));
			if (tables == null || tables.isEmpty())
			{
				logger.debug(schema + " getTables returned nothing");
				return (false);
			} else
			{
				for (String s : tables)
				{
					if (s.equalsIgnoreCase(tableName))
					{
						logger.debug(schema + "/" + tableName + " exists");
						return true;
					}
				}
				logger.debug(schema + "/" + tableName + " does not exist");
				return false;
			}
		}
	}

	public static boolean tableExistsOnLiveAccess(Repository r, DatabaseConnection dc, String tableName) throws SQLException
	{
		if (dc.isRealTimeConnection())
		{
			try
			{
				DatabaseConnectionStatement statement = new DatabaseConnectionStatement(r, dc);
				return statement.tableExistsOnLiveAccess(dc.Schema, tableName);
			} catch (Exception e)
			{
				logger.error(e, e);
			}
		}
		return false;
	}

	/**
	 * Create a table as a query
	 * 
	 * @param dc
	 * @param tableName
	 * @param query
	 * @param dropIfExists
	 * @return
	 * @throws SQLException
	 */
	public static boolean createTableAs(Repository r, DatabaseConnection dc, String tableName, String query, boolean dropIfExists) throws SQLException
	{
		return (createTableAs(r, dc, tableName, query, dropIfExists, null, null, false, false));
	}
	
	public static boolean createTableAs(Repository r, DatabaseConnection dc, String tableName, String query, boolean dropIfExists, String partitionSchemeName,
			String partitionKeyColumn, boolean createBlankTableForIB, boolean createInMemory) throws SQLException
	{
		return (createTableAs(r, dc, tableName, query, dropIfExists, partitionSchemeName, partitionKeyColumn, createBlankTableForIB, createInMemory, null, null, false));
	}

	/**
	 * map meta data about a column to a column type string that can be used in CREATE TABLE
	 * 
	 * -- only used for recreating a table before partitioning, only for MSSQL Server
	 * 
	 * @param type
	 *            SQL Type for the column
	 * @param size
	 *            size of the column (for character and numeric types)
	 * @param digits
	 *            precision of the column (for numeric types)
	 * @return
	 */
	private static String getTypeString(DatabaseConnection conn, Integer type, String typeName, Integer size, Integer digits)
	{
		switch (type)
		{
		case Types.INTEGER:
			return "INT";
		case Types.BIGINT:
			return "BIGINT";
		case Types.VARCHAR:
			return conn.getVarcharType(size, conn.DBType != DatabaseType.MonetDB);			
		case Types.CHAR:
			return "CHAR(" + size + ")";
		case Types.NVARCHAR:
			return conn.getVarcharType(size, conn.DBType != DatabaseType.MonetDB);
		case Types.NCHAR:
			return "NCHAR(" + size + ")"; // NCHAR for Unicode support
		case Types.NUMERIC:
			return conn.getNumericType(size, digits);			
		case Types.DECIMAL:
			if (DatabaseConnection.isDBTypeMSSQL(conn.DBType))
				return "MONEY";
			else
				return conn.getNumericType();
		case Types.DOUBLE: // float in SQL Server
			if (DatabaseConnection.isDBTypeMSSQL(conn.DBType))
				return "FLOAT";
			return "DOUBLE";
		case Types.FLOAT:
			if (DatabaseConnection.isDBTypeMySQL(conn.DBType))
				return "DOUBLE"; //Double in MySQL/IB
			return "FLOAT";
		case Types.DATE:
			return conn.getDateType();
		case 93:
			return conn.getDateTimeType();		
		}
		if (typeName.equals("TEXT"))
			return "TEXT"; // unclear if this is correct XXX
		return null;
	}
	
	public static int copyTable(Repository r, String tableName, String sourceSchema, String targetSchema) throws Exception
	{
		return copyTable(r, tableName, sourceSchema, targetSchema,null, null);
	}
	
	public static void bulkImportExport(Repository r,String sourceSchema ,String tableName, String databasepath, AWSCredentials s3credentials,boolean isExport,String targetSchema,String sourceFileName,boolean keepexportFileNameasTableName,String targetSchemaForExport,String datastoredir) throws Exception
	{
		Statement stmt = null;
		DatabaseConnection dc = null;
		String[] unloadSummary = new String[2];
		StringBuilder query = new StringBuilder();
		try{
			dc = r.getDefaultConnection();
			if(isExport)
			{
				query.append("SELECT * FROM ").append(sourceSchema).append('.').append(tableName); //set select query on source schema table
			}
			else{
				query.append("SELECT * FROM ").append(targetSchema).append('.').append(tableName); //set select query on target schema table
			}
			Connection conn = dc.ConnectionPool.getConnection();
			if (conn == null)
			{
				logger.error("Unable to unload- Cannot connect to database");
			}
			stmt = conn.createStatement();
			
			List<Object[]> schema = getTableSchema(dc, conn, null, sourceSchema, tableName);
			boolean isFirst=true;
			
			StringBuilder selectColumns = new StringBuilder();
			for (int colno = 0; colno < schema.size(); colno++)
			{
				if(!isFirst)
					selectColumns.append(",");
				isFirst=false;
				String physicalColName = (String) schema.get(colno)[0];
				selectColumns.append(physicalColName);
			}
			if(isExport)
			{
				unloadSummary = unloadTableToFile(r,query.toString(), tableName, selectColumns.toString(), databasepath, dc, stmt, s3credentials, sourceSchema,keepexportFileNameasTableName,targetSchemaForExport,datastoredir);
				
			}
			else{
				int numOfRowsAfterCopy = loadTableFromFile(r,query.toString(), tableName, selectColumns.toString(), databasepath, true, dc, stmt, s3credentials, targetSchema, sourceFileName,true,datastoredir);
			}
		}
		finally{
			closeStatement(stmt);
		}
	}
	
	/**
	 * Unload table to a file but only columns that are available in the repository and standard columns like LOAD_ID,TRANS_ID or any CKSUM columns. 
	 * 
	 * @param r
	 * @param sourceSchema
	 * @param physicalTableName
	 * @param databasepath
	 * @param s3credentials
	 * @param isExport
	 * @param targetSchema
	 * @param sourceFileName
	 * @param keepexportFileNameasTableName
	 * @throws Exception
	 */
	public static void unloadUsingQuery(Repository r,String scratchSchema,String sourceSchema, String physicalTableName, String databasepath, AWSCredentials s3credentials,boolean isExport,String targetSchema,String sourceFileName,boolean keepexportFileNameasTableName) throws Exception
	{
		Statement stmt = null;
		DatabaseConnection dc = null;
		String[] unloadSummary = new String[2];
		StringBuilder query = new StringBuilder();
		try{
			dc = r.getDefaultConnection();
			Connection conn = dc.ConnectionPool.getConnection();
			if (conn == null)
			{
				logger.error("Unable to unload- Cannot connect to database");
			}
			stmt = conn.createStatement();

			List<Object[]> schema = getTableSchema(dc, conn, null, scratchSchema, physicalTableName);
			
			List<String> repositoryColumns = new ArrayList<String>();
	

			boolean isFirst=true;
			StringBuilder selectColumns = new StringBuilder();
			for (int colno = 0; colno < schema.size(); colno++)
			{
				String physicalColName = physicalTableName+"."+((String) schema.get(colno)[0]);
				if(!isFirst)
					selectColumns.append(",");
				isFirst=false;
				selectColumns.append(physicalColName);
			}
			query.append("SELECT ").append(selectColumns).append(" FROM ").append(sourceSchema).append(".").append(physicalTableName);
			unloadSummary = unloadTableToFile(r,query.toString(), physicalTableName, selectColumns.toString(), databasepath, dc, stmt, s3credentials, sourceSchema,keepexportFileNameasTableName,null,null);
		}
		finally{
			closeStatement(stmt);
		}
	}
	
	public static String[] unloadTableToFile(Repository r,String query, String tableName, String columnList, String databasepath, 
			 DatabaseConnection dconn, Statement stmt, AWSCredentials s3credentials,String sourceSchema,boolean keepexportFileNameasTableName,String targetSchema,String databaseloaddirpath) throws Exception{
		int numRows = -1;
		String[] unloadSummary = new String[2]; //0the element = number of rows. 1st element - fileName for unloaded table
		ResultSet rs = null;
	try{
		String fileName = keepexportFileNameasTableName ?  Util.replaceStr(databasepath + File.separator + tableName + ".txt", "\\", "/") :  Util.replaceStr(databasepath + File.separator + tableName +"_" + System.currentTimeMillis() + ".txt", "\\", "/");
		unloadSummary[1] = fileName;
		String selectCountQuery = sourceSchema!=null ? ("SELECT COUNT(*) FROM " + sourceSchema+"."+tableName) :  ("SELECT COUNT(*) FROM " + Database.getQualifiedTableName(dconn, tableName));
		String SMI_HOME = System.getProperty("smi.home");
		String absFilePath = Util.replaceStr(SMI_HOME + "/data/" + tableName + "_" + System.currentTimeMillis() + ".txt", "/", File.separator);
		File file = new File(absFilePath);
		if (file.exists())
			file.delete();
		
		switch (dconn.DBType)
		{
			case ParAccel:
			{
				StringBuilder unloadQuery = new StringBuilder("UNLOAD ('");
				unloadQuery.append(Util.replaceStr(query, "'", "\\'"));
				unloadQuery.append("')");
				unloadQuery.append(" TO '").append(fileName).append("'");
				unloadQuery.append(" WITH");
				unloadQuery.append(" GZIP");
				unloadQuery.append(" DELIMITER AS '|'");
				unloadQuery.append(" ADDQUOTES");
				unloadQuery.append(" ESCAPE");
				unloadQuery.append(" NULL AS '\\\\N'");
				unloadQuery.append(" LEADER");
				logger.debug(Query.getPrettyQuery(unloadQuery.toString()));
				rs = stmt.executeQuery(selectCountQuery);
				if (rs.next())
					numRows = rs.getInt(1);
				stmt.executeUpdate(unloadQuery.toString());
				
				ResultSetMetaData rsmd = null;
				ResultSet resultSetData = null;
				try
				{        
					if (r!=null && databaseloaddirpath!=null && targetSchema!=null)
					{
						String dataQry = "SELECT * FROM "+sourceSchema+"."+tableName + " LIMIT 1";
						resultSetData = stmt.executeQuery(dataQry);
						rsmd = resultSetData.getMetaData();
						CopySpaceUtil.createTableSQLAsFile(r,dconn,tableName,databaseloaddirpath,rsmd,targetSchema);
					}
					else
					{
						logger.debug("File for creation of table will not be generated due to missing command parameters ");
					}
				}
				finally
				{
					if (resultSetData!=null)
						resultSetData.close();
				}

				break;
			}	
			case Redshift:
			{
				if (s3credentials == null)
					throw new SQLException("AWS Credentials are null for Redshift UNLOAD/COPY operation");
				// dbloaddir in spaces.config file/table will be s3://bucketName (e.g., s3://birst_dev2)
				// dbloaddir in space table will be s3://bucketName/spaceId
				// filename will be s3://bucketName/spaceId/data/table_time.txt
				String s3Location = AWSUtil.cleanupPath(fileName);
				logger.debug("Unloading to: " + s3Location);
				
				List<SecretKey> keys = null; // XXX new ArrayList<SecretKey>();
				AmazonS3 s3client = AWSUtil.getS3Client(s3credentials, keys);
				
				boolean encrypted = AWSUtil.isEncrypted(s3client);
				SecretKey symKey = null;
				if (encrypted)
					symKey = keys.get(0);
				
				String credentials = AWSUtil.getCopyCommandCredentials(s3credentials, symKey);
				String dummyCredentials = AWSUtil.getCopyCommandDummyCredentials(symKey);
				
				// delete old objects
				AWSUtil.s3CleanupObjects(s3Location, s3client);

				// unload new objects
				StringBuilder unloadQuery = new StringBuilder("UNLOAD ('");
				unloadQuery.append(Util.replaceStr(query, "'", "\\'"));
				unloadQuery.append("')");
				unloadQuery.append(" TO '").append(s3Location).append("'"); // TO S3:/bucket/.....
				unloadQuery.append(" WITH");
				unloadQuery.append(" GZIP");
				unloadQuery.append(" ESCAPE");
				if (encrypted)
				 	unloadQuery.append(" ENCRYPTED");
				logger.debug(unloadQuery.toString() + dummyCredentials);

				unloadQuery.append(credentials);
				rs = stmt.executeQuery(selectCountQuery);
				if (rs.next())
					numRows = rs.getInt(1);
				stmt.executeUpdate(unloadQuery.toString());
				
				ResultSetMetaData rsmd = null;
				ResultSet resultSetData = null;
				try
				{        
					if (r!=null && databaseloaddirpath!=null && targetSchema!=null)
					{
						String dataQry = "SELECT * FROM "+sourceSchema+"."+tableName + " LIMIT 1";
						resultSetData = stmt.executeQuery(dataQry);
						rsmd = resultSetData.getMetaData();
						CopySpaceUtil.createTableSQLAsFile(r,dconn,tableName,databaseloaddirpath,rsmd,targetSchema);
					}
					else
					{
						logger.debug("File for creation of table will not be generated due to missing command parameters ");
					}
				}
				finally
				{
					if (resultSetData!=null)
						resultSetData.close();
				}
				break;	
			}
			case Infobright:
			{
			  StringBuilder unloadQuery = new StringBuilder();
			  unloadQuery.append(query);
			  unloadQuery.append(" INTO OUTFILE '").append(fileName).append("'");
			  unloadQuery.append(" FIELDS TERMINATED BY '|'");
			  unloadQuery.append(" ENCLOSED BY '\"'");
			  logger.debug(Query.getPrettyQuery(unloadQuery.toString()));
			  stmt.execute("set @bh_dataformat='mysql'");
			  rs = stmt.executeQuery(selectCountQuery);
			  if (rs.next())
			    numRows = rs.getInt(1);
			  stmt.execute(unloadQuery.toString());

			  ResultSetMetaData rsmd = null;
			  ResultSet resultSetData = null;
			  try
			  {        
				  if (r!=null && databaseloaddirpath!=null && targetSchema!=null)
				  {
					  String dataQry = "SELECT * FROM "+sourceSchema+"."+tableName + " LIMIT 1";
					  resultSetData = stmt.executeQuery(dataQry);
					  rsmd = resultSetData.getMetaData();
					  CopySpaceUtil.createTableSQLAsFile(r,dconn,tableName,databaseloaddirpath,rsmd,targetSchema);
				  }
				  else
				  {
					  logger.debug("File for creation of table will not be generated due to missing command parameters ");
				  }
			  }
			  finally
			  {
				  if (resultSetData!=null)
					  resultSetData.close();
			  }

			  break;
			}
			case MSSQL:
			case MSSQL2005:
			case MSSQL2008:
			case MSSQL2012:
			case MSSQL2014:
			{
				rs = stmt.executeQuery(selectCountQuery);
				if (rs.next())
					numRows = rs.getInt(1);

				ResultSetMetaData rsmd = null;
				ResultSet resultSetData = null;
				ResultSet rsLoad = null;
				try
				{			   
					if (r!=null && databaseloaddirpath!=null && targetSchema!=null)
					{
						String dataQry = "SELECT * FROM "+sourceSchema+"."+tableName;
						resultSetData = stmt.executeQuery(dataQry);
						rsmd = resultSetData.getMetaData();
						CopySpaceUtil.createTableDataFile(resultSetData, rsmd, sourceSchema, tableName, dataQry, false, dconn, fileName);

						CopySpaceUtil.createTableSQLAsFile(r,dconn,tableName,databaseloaddirpath,rsmd,targetSchema);
						CopySpaceUtil.createTableFormatAsFile(databaseloaddirpath, tableName, rsmd);
						CopySpaceUtil.createTableRowsAsFile(databaseloaddirpath,tableName,numRows);
						CopySpaceUtil.createTableIndexAsFile(dconn, stmt, tableName, databaseloaddirpath, targetSchema);
					}
					else
					{
						logger.debug("Data for table "+tableName+" cannot be unloaded from schema " + sourceSchema + " due to missing command parameters");
					}
				}
				finally
				{
					if (resultSetData!=null)
						resultSetData.close();
					if (rsLoad != null)
						rsLoad.close();
				}			  
				break;
			}
			case Oracle:
			{
				rs = stmt.executeQuery(selectCountQuery);
				if (rs.next())
					numRows = rs.getInt(1);

				ResultSetMetaData rsmd = null;
				ResultSet resultSetData = null;
				ResultSet rsLoad = null;
				try
				{        
					if (r!=null && databaseloaddirpath!=null && targetSchema!=null)
					{
						String dataQry = "SELECT * FROM "+sourceSchema+"."+tableName;
						resultSetData = stmt.executeQuery(dataQry);
						rsmd = resultSetData.getMetaData();
						CopySpaceUtil.createTableDataFile(resultSetData, rsmd, sourceSchema, tableName, dataQry, false, dconn, fileName);

						CopySpaceUtil.createTableSQLAsFile(r,dconn,tableName,databaseloaddirpath,rsmd,targetSchema);
						CopySpaceUtil.createTableIndexAsFile(dconn, stmt, tableName, databaseloaddirpath, targetSchema);
					}
					else
					{
						logger.debug("Data for table "+tableName+" cannot be unloaded from schema " + sourceSchema + " due to missing command parameters");
					}
				}
				finally
				{
					if (resultSetData!=null)
						resultSetData.close();
					if (rsLoad != null)
						rsLoad.close();
				}       
				break;
			}
			default:
			{
				throw new SQLException("Attempt to UNLOAD a table from a database that does not support this");
			}
		}
	}
		finally
		{
			if(rs!=null)
			{
				rs.close();
			}			
		}
		logger.debug("rows before copy:  " + numRows);
		unloadSummary[0] = String.valueOf(numRows);
		return unloadSummary;
	}
	
	public static int loadTableFromFile(Repository r,String query, String tableName, String columnList, String databasepath, 
			boolean includesIdentityColumn, DatabaseConnection dconn, Statement stmt, AWSCredentials s3credentials,String targetSchema,String fileName,boolean createTableIfNotExists,String databaseloaddirpath) throws Exception{
		int numRows = -1;
		ResultSet rs = null;
		String selectCountQuery =  (targetSchema!=null) ? "SELECT COUNT(*) FROM " + targetSchema+"."+tableName : "SELECT COUNT(*) FROM " + Database.getQualifiedTableName(dconn, tableName);
		try{
			switch (dconn.DBType)
			{
				case ParAccel:
				{
					if (createTableIfNotExists)
					{
						if (r!=null && databaseloaddirpath!=null && !tableExists(r, dconn, tableName, true))
						{
							String createTableFileName = Util.replaceStr(databaseloaddirpath + File.separator + tableName + ".sql", "\\", "/");
							String createTableSQL = CopySpaceUtil.getFileContents(createTableFileName,false);

							if (createTableSQL!=null && !createTableSQL.isEmpty())
							{
								logger.debug("[CREATE TABLE "+tableName+":"+createTableSQL+"]");
								stmt.execute(createTableSQL);
							}
						}
						else
						{
							logger.debug("Table "+tableName+" cannot be created on schema "+targetSchema+" due to missing command parameters or it already exists");
						}
					}

					fileName = Util.replaceStr(fileName, "\\", "/");
					StringBuilder copyQuery = new StringBuilder("COPY ");
					//In case it's a Copy Space, use a different schema
					if(targetSchema!=null && targetSchema.length()>0){
						copyQuery.append(targetSchema).append(".").append(tableName);
					}else{
						copyQuery.append(Database.getQualifiedTableName(dconn, tableName));
					}
					copyQuery.append(" (").append(columnList).append(")");
					copyQuery.append(" FROM '").append(fileName).append("'");
					copyQuery.append(" WITH");
					copyQuery.append(" GZIP");
					copyQuery.append(" DELIMITER AS '|'");
					copyQuery.append(" REMOVEQUOTES");
					copyQuery.append(" ESCAPE");
					if (includesIdentityColumn)
						copyQuery.append(" EXPLICIT_IDS");
					logger.debug(Query.getPrettyQuery(copyQuery.toString()));
					stmt.executeUpdate(copyQuery.toString());
					rs = stmt.executeQuery(selectCountQuery);
					if (rs.next())
						numRows = rs.getInt(1);
					if (rs !=null)
					{
						rs.close();
					}
					break;
				}	
				case Redshift:
				{
					if (s3credentials == null)
						throw new SQLException("AWS Credentials are null for Redshift UNLOAD/COPY operation");
					
					if (createTableIfNotExists)
					{
						if (r!=null && databaseloaddirpath!=null && !tableExists(r, dconn, tableName, true))
						{
							String createTableFileName = Util.replaceStr(databaseloaddirpath + File.separator + tableName + ".sql", "\\", "/");
							String createTableSQL = CopySpaceUtil.getFileContents(createTableFileName,false);

							if (createTableSQL!=null && !createTableSQL.isEmpty())
							{
								logger.debug("[CREATE TABLE "+tableName+":"+createTableSQL+"]");
								stmt.execute(createTableSQL);
							}
						}
						else
						{
							logger.debug("Table "+tableName+" cannot be created on schema "+targetSchema+" due to missing command parameters or it already exists");
						}
					}
					
					// dbloaddir in spaces.config file/table will be s3://bucketName (e.g., s3://birst_dev2)
					// dbloaddir in space table will be s3://bucketName/spaceId
					// filename will be s3://bucketName/spaceId/data/table_time.txt
					String s3Location = AWSUtil.cleanupPath(fileName);
					logger.debug("Copying from : " + s3Location);
					
					List<SecretKey> keys = null; // XXX new ArrayList<SecretKey>();
					AmazonS3 s3client = AWSUtil.getS3Client(s3credentials, keys);
					
					boolean encrypted = AWSUtil.isEncrypted(s3client);
					SecretKey symKey = null;
					if (encrypted)
						symKey = keys.get(0);
					
					String credentials = AWSUtil.getCopyCommandCredentials(s3credentials, symKey);
					String dummyCredentials = AWSUtil.getCopyCommandDummyCredentials(symKey);
					
					// bulk load the unloaded data	
					try
					{
						String qualifiedTableName = (targetSchema!=null && targetSchema.length()>0) ? (targetSchema+"."+tableName) : (Database.getQualifiedTableName(dconn, tableName));
						String baseBulkInsert = AWSUtil.getCopyCommand(qualifiedTableName, columnList.toString(), s3Location, s3client, includesIdentityColumn);
						String bulkInsert = baseBulkInsert + credentials;
						String outputBulkInsert = baseBulkInsert + dummyCredentials;
						logger.info(outputBulkInsert);
						stmt.executeUpdate(bulkInsert);
					}
					catch (SQLException sqle)
					{
						AWSUtil.dumpErrors(tableName.toLowerCase(), Database.getQualifiedTableName(dconn, tableName), sqle, stmt);
						throw sqle;
					}
					finally
					{
						// delete any S3 objects that we created
						if (s3client != null)
						{
							AWSUtil.s3CleanupObjects(s3Location, s3client);
						}
					}
					rs = stmt.executeQuery(selectCountQuery);
					if (rs.next())
						numRows = rs.getInt(1);
					
					break;	
				}
				case Infobright:
				{
					if (createTableIfNotExists)
					{
						if (r!=null && databaseloaddirpath!=null && !tableExists(r, dconn, tableName, true))
						{
							String createTableFileName = Util.replaceStr(databaseloaddirpath + File.separator + tableName + ".sql", "\\", "/");
							String createTableSQL = CopySpaceUtil.getFileContents(createTableFileName,false);

							if (createTableSQL!=null && !createTableSQL.isEmpty())
							{
								logger.debug("[CREATE TABLE "+tableName+":"+createTableSQL+"]");
								stmt.execute(createTableSQL);
							}
						}
						else
						{
							logger.debug("Table "+tableName+" cannot be created on schema "+targetSchema+" due to missing command parameters or it already exists");
						}
					}

					fileName = Util.replaceStr(fileName, "\\", "/");
					StringBuilder loadQuery = new StringBuilder("LOAD DATA INFILE '");
					loadQuery.append(fileName).append("' INTO TABLE ");
					//In case it's a Copy Space, use a different schema
					if(targetSchema!=null && targetSchema.length()>0){
						loadQuery.append(targetSchema).append(".").append(tableName);
					}else{
						loadQuery.append(Database.getQualifiedTableName(dconn, tableName));
					}
					loadQuery.append(" CHARACTER SET utf8");
					loadQuery.append(" FIELDS TERMINATED BY '|'");
					loadQuery.append(" ENCLOSED BY '\"'");
					loadQuery.append(" (").append(columnList).append(")");
					logger.debug(Query.getPrettyQuery(loadQuery.toString()));
					stmt.execute("set @bh_dataformat='mysql'");
					numRows = stmt.executeUpdate(loadQuery.toString());		
					rs = stmt.executeQuery(selectCountQuery);
					if (rs.next())
						numRows = rs.getInt(1);
					break;
				}
				case MSSQL:
				case MSSQL2005:
				case MSSQL2008:
				case MSSQL2012:
				case MSSQL2014:
				{
					if (createTableIfNotExists)
					{
						if (r!=null && databaseloaddirpath!=null && !tableExists(r, dconn, tableName, true))
						{
							String createTableFileName = Util.replaceStr(databaseloaddirpath + File.separator + tableName + ".sql", "\\", "/");
							String createTableSQL = CopySpaceUtil.getFileContents(createTableFileName,false);

							if (createTableSQL!=null && !createTableSQL.isEmpty() && !tableExists(r, dconn, tableName, true))
							{
								logger.debug("[CREATE TABLE "+tableName+":"+createTableSQL+"]");
								stmt.execute(createTableSQL);
							}

							String createIndexFileName = Util.replaceStr(databaseloaddirpath + File.separator + tableName + ".index", "\\", "/");          
							String createIndexesSQL = CopySpaceUtil.getFileContents(createIndexFileName,true);
							if (createIndexesSQL!=null)
							{
								String[] indexScripts = createIndexesSQL.split(System.lineSeparator());
								if (indexScripts!=null && indexScripts.length>0)
								{
									for (String indexSQL : indexScripts)
									{
										logger.debug("[CREATE INDEX ON"+tableName+":"+indexSQL+"]");
										stmt.execute(indexSQL);
									}
								}
							}
						}
						else
						{
							logger.debug("Table and/or Indexes for "+tableName+" cannot be created on schema "+targetSchema+" due to missing command parameters or it already exists");
						}
					}

					long numRowsPerBatch = 10000;
					String rowFileName = Util.replaceStr(databaseloaddirpath + File.separator + tableName + ".rows", "\\", "/");
					File rowFile = new File(rowFileName);
					if (rowFile.exists())
					{
						String strRowsPerBatch = CopySpaceUtil.getFileContents(rowFileName,false);
						if (strRowsPerBatch!=null && !strRowsPerBatch.isEmpty())
						{
							numRowsPerBatch = Long.parseLong(strRowsPerBatch);
						}
					}
					else
					{
						logger.debug("Rows file not found. Default batch size of "+numRowsPerBatch+" will be used for bulk insert");
					}

					File bcpFile = new File(Util.replaceStr(databaseloaddirpath + File.separator + tableName + ".txt.format", "\\", "/"));
					if (!bcpFile.exists())
					{
						logger.debug("Format file for table "+tableName+" not found.Data might not be loaded correctly");
					}

					StringBuilder loadQuery = new StringBuilder("BULK INSERT ");
					loadQuery.append(targetSchema).append(".").append(tableName);
					loadQuery.append(" FROM ");
					loadQuery.append("'").append(fileName).append("'");
					loadQuery.append(" WITH ");
					loadQuery.append("(");
					loadQuery.append("FORMATFILE=");
					loadQuery.append("'").append(bcpFile.getAbsolutePath()).append("'");
					loadQuery.append(",FIRSTROW=1,");
					loadQuery.append("ROWS_PER_BATCH="+numRowsPerBatch+",");
					loadQuery.append("DATAFILETYPE='widechar'"); 
					loadQuery.append(")");
					logger.debug(Query.getPrettyQuery(loadQuery.toString()));
					numRows = stmt.executeUpdate(loadQuery.toString());   
					rs = stmt.executeQuery(selectCountQuery);
					if (rs.next())
						numRows = rs.getInt(1);
					break;                    
				}
				case Oracle:
				{
					String oracleExtDataDir = null;
					String oracleExtLogDir = null;
					String oracleExtTableName = null;
					try
					{
						File tempFile = new File(fileName);
						String timeInMillis = String.valueOf(System.currentTimeMillis());
						oracleExtDataDir = "EXT_DATA_DIR_" + timeInMillis;
						oracleExtLogDir = "EXT_LOG_DIR_" + timeInMillis;            
						String dataPath = tempFile.getParent().replace('\\', '/');
						String logPath = tempFile.getParentFile().getParentFile().getPath() + File.separator + "logs";
						logPath = logPath. replace('\\', '/');
						String createDirSQL = "CREATE OR REPLACE DIRECTORY " + oracleExtDataDir + " AS '" + dataPath + "'";
						logger.debug(createDirSQL);
						stmt.executeUpdate(createDirSQL);
						createDirSQL = "CREATE OR REPLACE DIRECTORY " + oracleExtLogDir + " AS '" + logPath + "'";
						logger.debug(createDirSQL);
						stmt.executeUpdate(createDirSQL);
						//create table            
						String createTableSQL = null;
						if (createTableIfNotExists)
						{
							if (r!=null && databaseloaddirpath!=null && !tableExists(r, dconn, tableName, true))
							{
								String createTableFileName = Util.replaceStr(databaseloaddirpath + File.separator + tableName + ".sql", "\\", "/");
								createTableSQL = CopySpaceUtil.getFileContents(createTableFileName,false);

								if (createTableSQL!=null && !createTableSQL.isEmpty() && !tableExists(r, dconn, tableName, true))
								{
									logger.debug("[CREATE TABLE "+tableName+":"+createTableSQL+"]");
									stmt.execute(createTableSQL);
								}

								String createIndexFileName = Util.replaceStr(databaseloaddirpath + File.separator + tableName + ".index", "\\", "/");          
								String createIndexesSQL = CopySpaceUtil.getFileContents(createIndexFileName,true);
								if (createIndexesSQL!=null)
								{
									String[] indexScripts = createIndexesSQL.split(System.lineSeparator());
									if (indexScripts!=null && indexScripts.length>0)
									{
										for (String indexSQL : indexScripts)
										{
											logger.debug("[CREATE INDEX ON"+tableName+":"+indexSQL+"]");
											stmt.execute(indexSQL);
										}
									}
								}
							}
							else if (r!=null && databaseloaddirpath!=null)
							{
								String createTableFileName = Util.replaceStr(databaseloaddirpath + File.separator + tableName + ".sql", "\\", "/");
								createTableSQL = CopySpaceUtil.getFileContents(createTableFileName,false);
							}
							else
							{
								logger.debug("Table "+tableName+" cannot be created on schema "+targetSchema+" due to missing command parameters or it already exists");
							}
						}

						oracleExtTableName = "EXT_TABLE_" + timeInMillis;
						createTableSQL = createTableSQL.replace(tableName, oracleExtTableName);
						StringBuilder loadQuerySQL = new StringBuilder(createTableSQL);
						loadQuerySQL.append("ORGANIZATION EXTERNAL (").append("\n");
						loadQuerySQL.append("TYPE ORACLE_LOADER").append("\n");
						loadQuerySQL.append("DEFAULT DIRECTORY ").append(oracleExtDataDir).append("\n");
						loadQuerySQL.append("ACCESS PARAMETERS (").append("\n");
						loadQuerySQL.append("RECORDS DELIMITED BY '\\r\\r\\n' CHARACTERSET 'UTF16'").append("\n");           
						loadQuerySQL.append("BADFILE ").append(oracleExtLogDir).append(":'BAD_").append(tempFile.getName()).append(".bad'").append("\n");
						loadQuerySQL.append("LOGFILE ").append(oracleExtLogDir).append(":'LOG_").append(tempFile.getName()).append(".log'").append("\n");
						loadQuerySQL.append("FIELDS TERMINATED BY '|' OPTIONALLY ENCLOSED BY '\"'").append("\n");				    
						loadQuerySQL.append("MISSING FIELD VALUES ARE NULL").append(")").append("\n");                
						loadQuerySQL.append("LOCATION ('").append(tempFile.getName()).append("')").append("\n");
						loadQuerySQL.append(")REJECT LIMIT 0").append("\n");
						logger.debug(loadQuerySQL.toString());
						numRows = stmt.executeUpdate(loadQuerySQL.toString());

						StringBuilder copyQuery = new StringBuilder();
						copyQuery.append("INSERT INTO ").append(dconn.Schema).append(".").append(tableName);
						copyQuery.append(" (SELECT * FROM ").append(dconn.Schema).append(".").append(oracleExtTableName);
						copyQuery.append(")");
						logger.debug(copyQuery.toString());

						numRows = stmt.executeUpdate(copyQuery.toString());				    				 
					}
					finally
					{
						if (oracleExtTableName != null)
						{
							String dropTableSQL = "DROP TABLE " + dconn.Schema + "." + oracleExtTableName;
							logger.debug(dropTableSQL);
							stmt.executeUpdate(dropTableSQL);
						}
						if (oracleExtDataDir != null)
						{
							String dropDirSQL = "DROP DIRECTORY " + oracleExtDataDir;
							logger.debug(dropDirSQL);
							stmt.executeUpdate(dropDirSQL);
						}
						if (oracleExtLogDir != null)
						{
							String dropDirSQL = "DROP DIRECTORY " + oracleExtLogDir;
							logger.debug(dropDirSQL);
							stmt.executeUpdate(dropDirSQL);
						}
					}
					break;
				}
				default:
				{
					throw new SQLException("Attempt to Copy a table from a database that does not support this");
				}
			}
		}
		finally{
			if(rs!=null){
				rs.close();
			}
		}
		logger.debug("rows after copy: " + numRows);
		return numRows;
	}
	
	public static int copyTable(Repository r, String tableName, String sourceSchema, String targetSchema,String databasePath,
			AWSCredentials s3credentials) throws Exception
	{
		DatabaseConnection dc = r.getDefaultConnection();
		tableCache.remove(dc);
		Statement stmt = null;
		int numOfRows=-1;
		Connection conn = dc.ConnectionPool.getConnection();
		if (conn == null)
		{
			logger.error("Unable to Copy - Cannot connect to database");
			return numOfRows;
		}
		try
		{
			stmt = conn.createStatement();
		
			StringBuilder query = new StringBuilder();
			if(DatabaseConnection.isDBTypeMSSQL(dc.DBType) || DatabaseConnection.isDBTypeInfoBright(dc.DBType))
			{
				if (DatabaseConnection.isDBTypeMSSQL(dc.DBType))
				{
					query.append("SELECT * INTO ").append(targetSchema).append('.').append(tableName).append(" FROM ").append(sourceSchema).append('.')
							.append(tableName);
				} else if (DatabaseConnection.isDBTypeInfoBright(dc.DBType))
				{
					query.append("CREATE TABLE ").append(targetSchema).append('.').append(tableName).append(" AS SELECT * FROM ").append(sourceSchema).append('.')
							.append(tableName);
				}
				logger.debug(Query.getPrettyQuery(query));
				numOfRows = stmt.executeUpdate(query.toString());
			}
			else if(DatabaseConnection.isDBTypeParAccel(dc.DBType)){
				StagingTable[] stagingTablesArray = r.getStagingTables();
				//ignoring case sensitivity for paraccel
				StagingTable st = null;
				for(StagingTable s : stagingTablesArray){
					if(s.Name.equalsIgnoreCase(tableName)){
						st = s;
					}
				}
				if(st!=null){
					query.append("SELECT * INTO ").append(targetSchema).append('.').append(tableName).append(" FROM ").append(sourceSchema).append('.')
					.append(tableName);
					logger.debug(Query.getPrettyQuery(query));
					stmt.executeUpdate(query.toString());
				}else{
					query.append("SELECT * FROM ").append(sourceSchema).append('.').append(tableName); //set select query on source schema table
					List<Object[]> schema = getTableSchema(dc, conn, null, sourceSchema, tableName);
					boolean isFirst=true;
					
					StringBuilder selectColumns = new StringBuilder();
					for (int colno = 0; colno < schema.size(); colno++)
					{
						if(!isFirst)
							selectColumns.append(",");
						isFirst=false;
						String physicalColName = (String) schema.get(colno)[0];
						selectColumns.append(physicalColName);
					}
					
					String[] unloadSummary  = unloadTableToFile(r,query.toString(),tableName,selectColumns.toString(),databasePath,dc,stmt,s3credentials,sourceSchema,false,null,null);
					int numOfRowsBeforeUnload =-1, numOfRowsAfterCopy=-1;
					try{
						numOfRowsBeforeUnload = Integer.parseInt(unloadSummary[0]);
						if(numOfRowsBeforeUnload!=-1 && unloadSummary[1]!=null){
							numOfRowsAfterCopy = loadTableFromFile(r,query.toString(), tableName, selectColumns.toString(), databasePath, true, dc, stmt, s3credentials, targetSchema,  unloadSummary[1],false,null);
							logger.debug("Total Difference between tables before and after load " + (numOfRowsAfterCopy - numOfRowsBeforeUnload));
						}else{
							logger.debug("Could not determine a successful Last unload result : " + numOfRowsBeforeUnload);
							throw (new Exception("Unable to determine last unload Results"));
						}
					}catch(NumberFormatException ex){
					}catch(ArrayIndexOutOfBoundsException ex){
						throw (new Exception("Unable to determine last unload Results"));
					}finally{
						File file = new File(unloadSummary[1]);
						String SMI_HOME = System.getProperty("smi.home");
						String absFilePath = Util.replaceStr(SMI_HOME + "/data/" + file.getName(), "/", File.separator);
						file = new File(absFilePath);
						if (file.exists())
							file.delete();
					}
					numOfRows = numOfRowsAfterCopy-numOfRowsBeforeUnload;
				}
			}
			return numOfRows;
		} finally
		{
			closeStatement(stmt);
		}
	}

	/**
	 * Create a table as a query
	 * 
	 * @param tableName
	 * @param query
	 * @param dropIfExists
	 * @param partitionSchemeName
	 * @param partitionKeyColumn
	 * @param createBlankTableForIB - this parameter applies to Infobright database only, if set to true, create an empty table - no rows will be inserted into the table
	 * @return
	 * @throws SQLException
	 */
	public static boolean createTableAs(Repository r, DatabaseConnection sourceConnection, String tableName, String query, boolean dropIfExists, String partitionSchemeName,
			String partitionKeyColumn, boolean createBlankTableForIB, boolean createInMemory, String dataDir, String databasePath, boolean append) throws SQLException
	{
		DatabaseConnection targetConnectionToUse = sourceConnection;
		if (createInMemory)
		{
			// Find the in-memory database connection
			for(DatabaseConnection sdc: r.getConnections())
			{
				if (sdc.Name.equals(MEM_AGG_CONNECTION))
				{
					targetConnectionToUse = sdc;
					break;
				}
			}
		}
		tableCache.remove(targetConnectionToUse);
		boolean exists = tableExists(r, targetConnectionToUse, tableName, false);
		if (!exists || dropIfExists || append)
		{
			if (exists && dropIfExists)
			{
				dropTable(r, targetConnectionToUse, tableName, false);
			}
			if (DatabaseConnection.isDBTypeMSSQL(sourceConnection.DBType) )
			{
				StringBuilder sb = new StringBuilder();
				String uq = query.toUpperCase();
				Pattern p = Pattern.compile("\\sFROM\\s");
				Matcher m = p.matcher(uq);
				boolean found = m.find();
				if (!found)
					return (false);
				int index = m.start();
				sb.append(query.substring(0, index));
				sb.append(" INTO " + getQualifiedTableName(sourceConnection, tableName) + " ");
				sb.append(query.substring(index));
				Connection conn = sourceConnection.ConnectionPool.getConnection();
				if (conn == null)
				{
					logger.error("Unable to create table - Cannot connect to database");
					return (false);
				}
				Statement stmt = null;
				try
				{
					stmt = conn.createStatement();
					String createQuery = null;
					List<String> columnNames = new ArrayList<String>();
					List<String> columnTypes = new ArrayList<String>();
					boolean getTableSchema = (partitionSchemeName != null && partitionKeyColumn != null) || createInMemory;
					if (getTableSchema)
					{
						/*
						 * If using partitioning, create a blank table first with the right schema, then recreate the
						 * table using the partitioning scheme
						 */
						sb.insert(6, " TOP 0"); // after SELECT
						createQuery = sb.toString();
						logger.debug(Query.getPrettyQuery(createQuery));
						stmt.executeUpdate(createQuery);
						// Now, get the schema
						List<Object[]> schema = Database.getTableSchema(sourceConnection, conn, conn.getCatalog(), sourceConnection.Schema, tableName);
						// Recreate the table with partitioning
						for (Object[] row : schema)
						{
							columnNames.add(row[0].toString());
							columnTypes.add(getTypeString(sourceConnection, (Integer) row[1], (String) row[2], (Integer) row[3], (Integer) row[4]));
						}
						// Now drop it
						Database.dropTable(r, sourceConnection, tableName, true);
					}
					if (partitionSchemeName != null && partitionKeyColumn != null)
					{
						// will drop it before creating it
						Database.createTable(r, sourceConnection, tableName, columnNames, columnTypes, true, partitionSchemeName, partitionKeyColumn, false, null, null, null);
						createQuery = "INSERT INTO " + getQualifiedTableName(sourceConnection, tableName) + " " + query;
						logger.info("Inserting Rows Into Table");
					} else
					{
						createQuery = sb.toString();
					}
					logger.debug(Query.getPrettyQuery(createQuery));
					// Create the table
					if (createInMemory && targetConnectionToUse != sourceConnection)
					{
						String baseFile = "agg" + System.currentTimeMillis() + ".txt";
						String fileName = dataDir + File.separator + baseFile;
						String databaseFileName = databasePath + File.separator + baseFile;
						bulkMSSQLQueryToFile(sourceConnection, query, fileName);
						// Convert column types from MSSQL types to MemDB types
						for (int i = 0; i < columnTypes.size(); i++)
						{
							String ctype = columnTypes.get(i).toUpperCase();
							if (ctype.startsWith("NVARCHAR"))
							{
								ctype = Util.replaceStr(ctype, "NVARCHAR", "VARCHAR");
								columnTypes.set(i, ctype);
							} else if (ctype.equals("INT"))
							{
								columnTypes.set(i, "INTEGER");
							}
						}
						bulkInsertIntoMemDB(r, targetConnectionToUse, databaseFileName, tableName, columnNames, columnTypes, !append);
						File f = new File(fileName);
						f.delete();
					} else
					{
						stmt.executeUpdate(createQuery);
					}
				} finally
				{
					closeStatement(stmt);
				}
			} else if (DatabaseConnection.isDBTypeInfoBright(sourceConnection.DBType))
			{
				Statement stmt = null;
				try {
					Connection conn = sourceConnection.ConnectionPool.getConnection();
					if (conn == null)
					{
						logger.error("Unable to create table - Cannot connect to database");
						return (false);
					}
					/** Use a temp table approach in case of a subquery as Infobright doesn't support subquery with create view in from **/
					boolean hasSubquery = containsSubQuery(query);
					String tempAggregate = hasSubquery ? "tempAggregate" : "tempView";
					stmt = conn.createStatement();
					String strTempAggregateName = getQualifiedTableName(sourceConnection, tempAggregate);
					
					/** Create Temporary View **/
					StringBuilder sb = new StringBuilder();
					sb.append("CREATE  " + (hasSubquery ? " TABLE " : " OR REPLACE VIEW ") + strTempAggregateName + " AS ");
					sb.append(query);
					if(hasSubquery)
					{
						sb.append(" LIMIT 1");
					}
					logger.debug(Query.getPrettyQuery(sb.toString()) );
					stmt.executeUpdate(sb.toString());
					
					/** Get Meta data From Temporary Table or View **/
					List<Object[]> schema = getTableSchema(sourceConnection, conn, null, sourceConnection.Schema, strTempAggregateName);
					List<String> columnNames = new ArrayList<String>();
					List<String> columnTypes = new ArrayList<String>();
				
					for (Object[] row : schema)
					{
						columnNames.add(row[0].toString());
						String colType = getTypeString(sourceConnection, (Integer) row[1], (String) row[2], (Integer) row[3], (Integer) row[4]);
						if (colType.toUpperCase().startsWith("NVARCHAR") || colType.equals("BIGINT"))
						{
							columnTypes.add(colType + " comment 'for_insert' ");
						}
						else
						{
							columnTypes.add(colType);
						}						
					}
					
					/** Dropping Temporary Table **/
					sb = new StringBuilder();
					sb.append("DROP " + (hasSubquery ? " TABLE " : "  VIEW ")).append(tempAggregate);
					logger.debug(Query.getPrettyQuery(sb.toString()));
					stmt.executeUpdate(sb.toString());
					
					sb = Database.getCreateTableSQL(r, sourceConnection, tableName, columnNames, columnTypes, partitionSchemeName, partitionKeyColumn, false, null, null, null);
					logger.debug(Query.getPrettyQuery(sb.toString()));
					stmt.executeUpdate(sb.toString());
					
					if(!createBlankTableForIB)
					{
						sb = new StringBuilder("INSERT INTO " + getQualifiedTableName(sourceConnection, tableName) + " " + query);
						logger.debug(Query.getPrettyQuery(sb.toString()));
						stmt.executeUpdate(sb.toString());
					}
					
				}
				finally
				{
					closeStatement(stmt);
				}
			} else if (DatabaseConnection.isDBTypeMySQL(sourceConnection.DBType) || DatabaseConnection.isDBTypeMemDB(sourceConnection.DBType) 
					|| DatabaseConnection.isDBTypeOracle(sourceConnection.DBType) || DatabaseConnection.isDBTypeParAccel(sourceConnection.DBType) 
					|| DatabaseConnection.isDBTypeSAPHana(sourceConnection.DBType))
			{
				if(DatabaseConnection.isDBTypeSAPHana(sourceConnection.DBType))
				{
					query = "( " + query + " ) "; //SAP Hana needs select query in ()
				}
				StringBuilder sb = new StringBuilder("CREATE TABLE " + getQualifiedTableName(sourceConnection, tableName) + " AS " + query);
				Connection conn = sourceConnection.ConnectionPool.getConnection();
				if (conn == null)
				{
					logger.error("Unable to create table - Cannot connect to database");
					return (false);
				}
				Statement stmt = null;
				try
				{
					stmt = conn.createStatement();
					String createQuery = null;
					createQuery = sb.toString();
					logger.debug(Query.getPrettyQuery(createQuery));
					// Create the table
					stmt.executeUpdate(createQuery);
				} finally
				{
					closeStatement(stmt);
				}
			}
			return (true);
		} else
		{
			if (exists && !dropIfExists)
			{
				logger.info("Table " + getQualifiedTableName(sourceConnection, tableName) + " exists - not creating");
			}
		}
		return (false);
	}
	
	private static boolean containsSubQuery(String query)
	{
		return (query!=null && (query.toLowerCase().indexOf("from") != -1) && query.toLowerCase().substring(query.toLowerCase().indexOf("from")).contains("select "));
	}
	
	public static boolean bulkInsertIntoMemDB(Repository r, DatabaseConnection dc, String fileName, String tableName, List<String> columnNames, List<String> columnTypes, boolean dropIfExists) throws SQLException
	{
		Database.createTable(r, dc, tableName, columnNames, columnTypes, dropIfExists, false, null, null, null);
		StringBuilder bulkInsert = new StringBuilder("LOAD DATA INFILE '" + fileName.replace('\\', '/') + "' INTO TABLE " + dc.Schema + "." + tableName
				+ " DELIMITED BY '|' " + "TERMINATED BY '\\r\\n' " + "ENCLOSED BY '\"'" + " (");
		for (int i = 0; i < columnNames.size(); i++)
		{
			if (i > 0)
				bulkInsert.append(',');
			bulkInsert.append(columnNames.get(i));
		}
		bulkInsert.append(')');
		Statement stmt = dc.ConnectionPool.getConnection().createStatement();
		logger.debug(bulkInsert);
		@SuppressWarnings("unused")
		int result = stmt.executeUpdate(bulkInsert.toString());
		return true;
	}
	
	public static boolean bulkMSSQLQueryToFile(DatabaseConnection dc, String query, String fileName)
	{
		int index1 = dc.ConnectString.indexOf("//") + 2;
		int index2 = dc.ConnectString.indexOf(";", index1);
		if (index1 < 0 || index2 < 0)
			return false;
		String serverName = dc.ConnectString.substring(index1, index2);
		String password = dc.Password;
		if (password != null && !password.isEmpty())
			password = EncryptionService.getInstance().decrypt(password);
		index1 = dc.ConnectString.toLowerCase().indexOf("databasename=") + 13;
		index2 = dc.ConnectString.indexOf(";", index1);
		if (index1 < 0)
			return false;
		String databaseName = index2 > 0 ? dc.ConnectString.substring(index1, index2) : dc.ConnectString.substring(index1);
		// Make sure that tables are fully qualified
		query = Util.replaceStr(query, dc.Schema, databaseName + "." + dc.Schema);
		String command = "bcp \"" + query + "\" queryout " + fileName + " -S" + serverName + " -U" + dc.UserName + " -P" + password
				+ " \"-t|\" -r\\n -c -C1252";
		logger.debug(command);
		logger.info("Exporting: " + query + " to " + fileName);
		Process p = null;
		try
		{
			p = Runtime.getRuntime().exec(command);
			BufferedReader stdOut = new BufferedReader(new InputStreamReader(p.getInputStream(),"UTF-8"));
			@SuppressWarnings("unused")
			String line = null;
			while ((line = stdOut.readLine()) != null)
			{
				// logger.info(line);
			}
			stdOut.close();
		} catch (IOException e1)
		{
			logger.error(e1);
			return false;
		}
		try
		{
			p.waitFor();
		} catch (InterruptedException e)
		{
			logger.error(e);
		}
		int exitvalue = p.exitValue();
		if (exitvalue != 0)
		{
			try
			{
				BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream(),"UTF-8"));
				StringBuilder sb = new StringBuilder();
				String s = null;
				while ((s = stdError.readLine()) != null)
				{
					sb.append(s);
				}
				if (sb.length() > 0)
					logger.error(sb.toString());
				else
					logger.error("General error bulk unloading table");
				stdError.close();
				return false;
			} catch (IOException e)
			{
				logger.error(e);
			}
		}
		p.destroy();
		return true;
	}
	
	public static boolean sameTableIBMySQL(Repository r, DatabaseConnection dc, String physicalName, DatabaseConnection queryConnection, String query) throws SQLException
	{	
		if (DatabaseConnection.isDBTypeInfoBright(dc.DBType))
		{
			query = query + " LIMIT 0";
			query = query.replace("SELECT", "SELECT ROUGHLY");
		}
		else
		{
			query = query + " LIMIT 1";
		}
		Connection conn = null;
		try
		{
			Properties p = new Properties();
			p.setProperty("useServerPrepStmts", "true");
			conn = dc.ConnectionPool.getConnectionFromDatabase(p);
			List<Object[]> tableSchema = Database.getTableSchema(dc, conn, conn.getCatalog(), dc.Schema, physicalName);
			StringBuilder columnsList = new StringBuilder();
			boolean first = true;
			for (int i=0; i< tableSchema.size(); i++)
			{
				if (first)
					first = false;
				else
					columnsList.append(",");
				columnsList.append(tableSchema.get(i)[0]);
			}
			String limitClause = " LIMIT 1";
			if (DatabaseConnection.isDBTypeInfoBright(dc.DBType))
			{
				limitClause = " LIMIT 0";
			}
			String schemaOrigQry = "SELECT " + columnsList.toString() + " FROM " + getQualifiedTableName(dc, physicalName) + limitClause;			
			if (DatabaseConnection.isDBTypeInfoBright(dc.DBType))
			{
				schemaOrigQry = schemaOrigQry.replace("SELECT", "SELECT ROUGHLY");
			}
			logger.debug("Existing aggregate query "+schemaOrigQry);
			List<Object[]> schemaOrig = Database.getQuerySchema(conn, schemaOrigQry);
			if (logger.isTraceEnabled())
				dumpSchema(schemaOrig);
			if (dc != queryConnection)
				conn = queryConnection.ConnectionPool.getConnectionFromDatabase(p);
			List<Object[]> schemaNew = Database.getQuerySchema(conn, query);
			if (logger.isTraceEnabled())
				dumpSchema(schemaNew);
			// Only compare types if same dbs
			return compareSchemas(schemaOrig, schemaNew, dc.DBType != queryConnection.DBType,queryConnection);
		}
		finally
		{
			if (conn != null)
				conn.close();
			logger.debug("finished comparing the table created with a query with an existing table: " + physicalName + ", " + query);
		}		
	}

	/**
	 * used to determine if an aggregate table needs to be dropped and recreated
	 * 
	 * @param r
	 * @param dc
	 * @param physicalName
	 * @param query
	 * @return
	 * @throws SQLException
	 */
	public static boolean sameTable(Repository r, DatabaseConnection dc, String physicalName, DatabaseConnection queryConnection, String query) throws SQLException
	{
		if (Database.tableExists(r, dc, physicalName, false))
		{
			logger.debug("comparing the table created with a query with an existing table: " + physicalName + ", " + query);
			if (DatabaseConnection.isDBTypeMySQL(queryConnection.DBType))
				return sameTableIBMySQL(r, dc, physicalName, queryConnection, query);
			// Generate alter table if needed
			String tempTable = "temp" + Thread.currentThread().getId() + System.currentTimeMillis();
			StringBuilder sb = new StringBuilder();
			boolean topAdded = false;
			if (DatabaseConnection.isDBTypeMySQL(queryConnection.DBType) || DatabaseConnection.isDBTypeMemDB(queryConnection.DBType) || DatabaseConnection.isDBTypeOracle(queryConnection.DBType) 
					|| DatabaseConnection.isDBTypeSAPHana(queryConnection.DBType))
			{
				sb.append("CREATE TABLE " + getQualifiedTableName(queryConnection, tempTable) + " AS ");
				if (dc.supportsRowNum())
				{
					topAdded = true;
					query = "SELECT * FROM (" + query + ") WHERE ROWNUM <= 0"; 
				}
				if(DatabaseConnection.isDBTypeSAPHana(queryConnection.DBType))
				{
					sb.append (" ( ");
				}
				sb.append(query);
			} else
			{
				String uq = query.toUpperCase();
				Pattern p = Pattern.compile("\\sFROM\\s");
				Matcher m = p.matcher(uq);
				boolean found = m.find();
				if (!found)
					return (false);
				int index = m.start();
				sb.append(query.substring(0, index));
				sb.append(" INTO " + getQualifiedTableName(dc, tempTable) + " ");
				sb.append(query.substring(index));
			}
			if (!topAdded)
			{
				if (queryConnection.supportsTop())
				{
					int index = sb.indexOf("SELECT");
					sb.insert(index + 6, " TOP 0");
				} else
				{
					if (dc.supportsLimit0())
						sb.append(" LIMIT 0");
					else
						sb.append(" LIMIT 1");
					if(DatabaseConnection.isDBTypeSAPHana(queryConnection.DBType))
					{
						sb.append (" ) ");
					}
				}
			}
			String createQuery = sb.toString();
			Connection conn = null;
			Statement stmt = null;
			try
			{
				conn = dc.ConnectionPool.getConnection();
				List<Object[]> schemaOrig = Database.getTableSchema(dc, conn, conn.getCatalog(), dc.Schema, physicalName);
				if (logger.isTraceEnabled())
					dumpSchema(schemaOrig);
				if (dc != queryConnection)
					conn = queryConnection.ConnectionPool.getConnection();
				stmt = conn.createStatement();
				logger.debug(Query.getPrettyQuery(createQuery));
				stmt.executeUpdate(createQuery);
				tableCache.remove(dc);
				List<Object[]> schemaNew = Database.getTableSchema(queryConnection, conn, conn.getCatalog(), queryConnection.Schema, tempTable);
				if (logger.isTraceEnabled())
					dumpSchema(schemaNew);
				// Only compare types if same dbs
				return compareSchemas(schemaOrig, schemaNew, dc.DBType != queryConnection.DBType,queryConnection);				
			} finally
			{
				closeStatement(stmt);
				Database.dropTable(r, queryConnection, tempTable, true);
				logger.debug("finished comparing the table created with a query with an existing table: " + physicalName + ", " + query);
			}
		}
		return false;
	}
	
	private static boolean compareSchemas(List<Object[]> schemaOrig, List<Object[]> schemaNew, boolean namesOnly, DatabaseConnection queryConnection)
	{
		if (schemaNew.size() != schemaOrig.size())
		{
			logger.debug("tables differ, different column counts: " + schemaNew.size() + ", " + schemaOrig.size());
			return false;
		}
		for (int i = 0; i < schemaNew.size(); i++)
		{
			Object[] colNew = schemaNew.get(i);
			Object[] colOrig = schemaOrig.get(i);
			for (int j = 0; j < (namesOnly ? 1 : colOrig.length); j++)
			{
				if (colNew[j] != null)
				{
					if (!colNew[j].equals(colOrig[j]))
					{
						logger.debug("tables differ, attribute " + j + " different for column " + i + " (" + colNew[j] + "," + colOrig[j] + ")");
						return false;
					}
					else
					{
						if (DatabaseConnection.isDBTypeMySQL(queryConnection.DBType) && j==2 && colNew[j].equals("BIGINT"))
						{
							break;
						}
					}
				} else if (colOrig[j] != null)
				{
					logger.debug("tables differ, attribute " + j + " different for column " + i + " (" + colNew[j] + "," + colOrig[j] + ")");
					return false;
				}
			}
		}
		return true;
	}

	private static void dumpSchema(List<Object[]> list)
	{
		for (Object[] obj : list)
		{
			for (int i = 0; i < obj.length; i++)
			{
				logger.trace(i + ": " + obj[i]);
			}
			logger.trace("--");
		}
	}

	/**
	 * Create a database table
	 * 
	 * @param c
	 *            Database connection
	 * @param tableName
	 *            Name of table to create
	 * @param columnNames
	 *            Column names
	 * @param columnTypes
	 *            Column Types
	 * @param dropIfExists
	 *            Drop table if it already exists
	 * @return
	 * @throws SQLException
	 */
	public static boolean createTable(Repository r, DatabaseConnection dbc, String tableName, List<String> columnNames, List<String> columnTypes,
			boolean dropIfExists, boolean transientTable, List<String> columnCompressionEncodings, List<String> distributionKey, Set<String> sortKeys) throws SQLException
	{
		return (createTable(r, dbc, tableName, columnNames, columnTypes, dropIfExists, null, null, transientTable, columnCompressionEncodings, distributionKey, sortKeys));
	}

	/**
	 * build a 'dummy' level key for the composite column (dim$name)
	 * 
	 * @param colName
	 *            composite column name (dim$name)
	 * @return a level key corresponding to the column
	 */
	public static LevelKey buildLevelKey(String colName)
	{
		LevelKey lk = new LevelKey();
		if (colName.indexOf('$') > 0)
		{
			String[] parts = colName.split("\\$");
			lk.ColumnNames = new String[]
			{ parts[1] };
			lk.Level = new Level();
			lk.Level.Hierarchy = new Hierarchy();
			lk.Level.Hierarchy.Name = parts[0];
			lk.Level.Hierarchy.DimensionName = lk.Level.Hierarchy.Name;
		} else
		{
			lk.ColumnNames = new String[]
			{ colName };
		}
		return lk;
	}

	public static LevelKey buildLevelKey(String dimension, String colName)
	{
		LevelKey lk = new LevelKey();
		lk.ColumnNames = new String[]
		{ colName };
		lk.Level = new Level();
		lk.Level.Hierarchy = new Hierarchy();
		lk.Level.Hierarchy.Name = dimension;
		lk.Level.Hierarchy.DimensionName = lk.Level.Hierarchy.Name;
		return lk;
	}
	
	public static void createColumnStoreIndex(DatabaseConnection dc, String tableName, List<String> columnNames)
	{
		if (dc.DBType != DatabaseType.MSSQL2012ColumnStore && dc.DBType != DatabaseType.MSSQL2014)
			return;
		String iname = dc.getColumnStoreIndexName(tableName); 
		try
		{
			if (columnStoreIndexExists(dc, tableName, iname))
				return;
		} catch (SQLException e1)
		{
			logger.error(e1);
			return;
		}
		StringBuilder sb = new StringBuilder("CREATE CLUSTERED COLUMNSTORE INDEX [" + iname + "] ON ");
		sb.append(getQualifiedTableName(dc, tableName));
		if (dc.DBType == DatabaseType.MSSQL2012ColumnStore)
		{
			// Only specify columns in 2012
			sb.append(" (");
			for (int i = 0; i < columnNames.size(); i++)
			{
				if (i != 0)
					sb.append(',');
				sb.append(dc.getQuotedColumn(columnNames.get(i)));
			}
			sb.append(')');
		}		
		logger.debug(Query.getPrettyQuery(sb.toString()));
		Connection conn = null;
		Statement stmt = null;
		try
		{
			conn = dc.ConnectionPool.getConnection();
			stmt = conn.createStatement();
			stmt.executeUpdate(sb.toString());
		} catch (Exception e)
		{
			logger.error(e, e);
		} finally
		{
			closeStatement(stmt);
		}
	}

	/**
	 * Create a database table
	 * 
	 * @param c
	 *            Database connection
	 * @param tableName
	 *            Name of table to create
	 * @param columnNames
	 *            Column names
	 * @param columnTypes
	 *            Column Types
	 * @param dropIfExists
	 *            Drop table if it already exists
	 * @param partitionSchemeName
	 *            Partition scheme name to use (ignore if null)
	 * @param partitionKeyColumn
	 *            Partition key column to use (ignore if null)
	 * @return
	 * @throws SQLException
	 */
	public static boolean createTable(Repository r, DatabaseConnection dc, String tableName, List<String> columnNames, List<String> columnTypes,
			boolean dropIfExists, String partitionSchemeName, String partitionKeyColumn, boolean transientTable, List<String> columnCompressionEncodings, List<String> distributionKey, Set<String> sortKeys) throws SQLException
	{
		Connection conn = dc.ConnectionPool.getConnection();
		if (conn == null)
		{
			throw (new SQLException("No database connection present"));
		}
		if (conn.getMetaData().isReadOnly())
		{
			logger.warn("Read-only database, will not create table " + tableName);
			return true;
		}
		boolean exists = tableExists(r, dc, tableName, false);
		if (!exists || dropIfExists)
		{
			if (exists && dropIfExists)
			{
				dropTable(r, dc, tableName, false);
			}
			// Table cache is cleared here so no need to clear at top of method
			StringBuilder sb = getCreateTableSQL(r, dc, tableName, columnNames, columnTypes, partitionSchemeName, partitionKeyColumn, transientTable, columnCompressionEncodings, distributionKey, sortKeys);
			Statement stmt = null;
			try
			{
				stmt = conn.createStatement();
				logger.debug(Query.getPrettyQuery(sb.toString()));
				Database.retryExecuteUpdate(dc, stmt, sb.toString());
			} finally
			{
				closeStatement(stmt);
			}
			// create clustered index here
			if (dc.supportsPartitioning() && partitionSchemeName != null && partitionKeyColumn != null)
			{
				LevelKey lk = buildLevelKey(partitionKeyColumn);
				Database.createIndex(dc, lk, tableName, true, true, null, null, true);
			}
			// Add clustered column store index if appropriate
			if (dc.DBType == DatabaseType.MSSQL2014)
			{
				createColumnStoreIndex(dc, tableName, columnNames);
			}
			return true;
		} else
		{
			logger.info("Table " + getQualifiedTableName(dc, tableName) + " exists - not creating");
			// Get the columns and types - for only check on the number, not the types
			List<Object[]> columns = getTableSchema(dc, conn, null, dc.Schema, tableName);
			if (columns == null || (columns.size() != columnNames.size()))
			{
				logger.error("Column Mismatch - Columns in existing table:");
				for (Object[] obj : columns)
				{
					logger.error((String) obj[0]);
				}
				logger.error("Columns in new table:");
				for (String col : columnNames)
				{
					logger.error(col);
				}
				throw new SQLException("Mismatch in column counts between the old table and the new table: " + getQualifiedTableName(dc, tableName));
			}
			return false;
		}
	}

	public static StringBuilder getCreateTableSQL(Repository r, DatabaseConnection dc, String tableName, List<String> columnNames, List<String> columnTypes,
			String partitionSchemeName, String partitionKeyColumn, boolean transientTable, List<String> columnCompressionEncodings, List<String> distributionKey, Set<String> sortKeys)
	{
		StringBuilder sb = new StringBuilder();
		Database.clearCacheForConnection(dc);
		if (dc.DBType == DatabaseType.Hana)
			sb.append("CREATE COLUMN TABLE ");
		else
			sb.append("CREATE TABLE ");
		sb.append(getQualifiedTableName(dc, tableName) + " (");

		for (int i = 0; i < columnNames.size(); i++)
		{
			if (dc.DBType == DatabaseType.Infobright || dc.DBType == DatabaseType.MYSQL)
			{
				if (columnNames.get(i).length() > 64)
				{
					logger.warn("Truncating column " + columnNames.get(i) + " to 64 characters - this will probably cause other operations to fail");
					columnNames.set(i, columnNames.get(i).substring(0, 64));
				}
			}
			if (i > 0)
			{
				sb.append(',');
			}
			sb.append(dc.getQuotedColumn(columnNames.get(i)));
			sb.append(' ');
			sb.append(columnTypes.get(i));
			if (DatabaseConnection.isDBTypeInfoBright(dc.DBType) && (columnTypes.get(i).toUpperCase().startsWith("NVARCHAR") || columnTypes.get(i).toUpperCase().equals("BIGINT")) 
						&& (!tableName.startsWith("ST_") && !tableName.startsWith("TXN_")))
			{
				sb.append(" comment 'for_insert'");
			}
			if (DatabaseConnection.isDBTypeMSSQL(dc.DBType) && r.getServerParameters()!=null && columnTypes.get(i).toUpperCase().contains("CHAR")
					&& !tableName.startsWith("TXN_"))
			{
				String collation = r.getServerParameters().getCollation();
				if (collation!=null && collation.length()>0)
				{
					sb.append(" COLLATE "+collation);
				}
			}
			if (dc.supportsColumnCompression() && columnCompressionEncodings != null && columnCompressionEncodings.get(i) != null)
			{
				// bad performance when SORTKEY is RUNLENGTH encoded
				// Redshift documentation:
				// "We do not recommend applying runlength encoding on any column that is designated as a SORTKEY because
				//  range-restricted scans might perform poorly when SORTKEY columns are compressed much more highly than other columns."
				String encoding = columnCompressionEncodings.get(i);
				if (encoding.equalsIgnoreCase("runlength") && sortKeys.contains(columnNames.get(i)))
					encoding = "RAW";
				sb.append(" ENCODE ").append(encoding);				
			}
			if (DatabaseConnection.isDBTypeParAccel(dc.DBType) && distributionKey != null && distributionKey.size() == 1 && columnNames.get(i).equals(distributionKey.get(0)))
			{
				// http://docs.aws.amazon.com/redshift/latest/dg/c_choosing_dist_sort.html
				sb.append(" DISTKEY");
			}
			boolean isLookup = false;
			if (isLookup && dc.DBType == DatabaseType.Infobright)
			{
				// improves performance and compression where the column has less than 10k values with
				// lots of duplicates (gender, state, occupation, etc)
				sb.append(" comment 'lookup'");
			}
		}
		sb.append(')');
		if (DatabaseConnection.isDBTypeParAccel(dc.DBType) && distributionKey != null && distributionKey.size() == 1)
		{
			sb.append(" DISTSTYLE KEY");
		}
		if (DatabaseConnection.isDBTypeParAccel(dc.DBType) && sortKeys != null && sortKeys.size() > 0)
		{
			// http://docs.aws.amazon.com/redshift/latest/dg/t_Sorting_data.html
			boolean first = true;
			for (String sk : sortKeys)
			{
				if (!columnNames.contains(sk))
				{
					logger.warn("Sort Key column " + sk + " not found in table " + getQualifiedTableName(dc, tableName));
					continue;
				}
				if (first)
				{
					sb.append(" SORTKEY(");
					first = false;
				}
				else
				{
					sb.append(", ");
				}
				sb.append(sk);
			}
			if (!first)
				sb.append(")");
		}
		if ((dc.supportsPartitioning()) && partitionSchemeName != null && partitionKeyColumn != null)
			sb.append(" ON " + partitionSchemeName + "(" + partitionKeyColumn + ")");
		if (dc.DBType == DatabaseType.Infobright)
		{
			sb.append(" engine=brighthouse");
			if(dc.isIBLoadWarehouseUsingUTF8())
			{
				sb.append(" CHARSET=utf8");
			}
		}
		if (dc.DBType == DatabaseType.MemDB)
		{
			sb.append(" DISK COMPRESSED");
			//sb.append(" MEMORY UNCOMPRESSED");
		}
		if (dc.useCompression() && dc.supportsMSSQLDBCompression())
		{
			sb.append(" WITH (DATA_COMPRESSION = PAGE)");
		}
		if (dc.DBType == DatabaseType.MYSQL)
		{
			// sb.append(" ENGINE=InnoDB ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=4");
			sb.append(" ENGINE=MyISAM");
		}
		if (DatabaseConnection.isDBTypeMemDB(dc.DBType) && transientTable)
		{
			sb.append(" TRANSIENT");
		}
		if (dc.DBType == DatabaseType.Hana)
			sb.append(" PARTITION BY ROUNDROBIN PARTITIONS GET_NUM_SERVERS()");

		return sb;
	}

	public static void createMysqlSchema(DatabaseConnection dc) throws SQLException
	{
		Connection conn = dc.ConnectionPool.getConnection();
		if (conn == null)
		{
			throw (new SQLException("No database connection present"));
		}
		Statement stmt = null;
		try
		{
			stmt = conn.createStatement();
			String sql = "create database if not exists " + dc.Schema;
			logger.debug(sql);
			stmt.executeUpdate(sql);
		} finally
		{
			closeStatement(stmt);
		}
	}

	/**
	 * Set any flags allowing a table to insert into identity columns
	 * 
	 * @param tableName
	 *            Table name
	 * @param c
	 *            Database connection
	 * @param on
	 *            Whether identity inserts are allowed or not
	 * @throws SQLException
	 */
	public static void setIdentityInsert(String tableName, DatabaseConnection dc, boolean on) throws SQLException
	{
		if (DatabaseConnection.supportsIdentityInsert(dc.DBType))
		{
			String query = "SET IDENTITY_INSERT " + getQualifiedTableName(dc, tableName) + (on ? " ON" : " OFF");
			Connection conn = dc.ConnectionPool.getConnection();
			Statement stmt = null;
			try
			{
				stmt = conn.createStatement();
				logger.debug(query);
				stmt.executeUpdate(query);
			} finally
			{
				closeStatement(stmt);
			}
		} else
		{
			logger.error("SET IDENTITY_INSERT is only supported on SQL Server");
		}
	}
	public static final int ALTER_ADD = 0;
	public static final int ALTER_DROP = 1;
	public static final int ALTER_COLUMN = 2;

	/**
	 * Alter a table by adding or removing columns
	 * 
	 * @param c
	 *            Database connection to use
	 * @param tableName
	 * @param columnNames
	 * @param columnTypes
	 * @param actions
	 *            Actions to take for each column ALTER_ADD, ALTER_DROP or ALTER_COLUMN
	 * @return
	 * @throws SQLException
	 */
	public static boolean alterTable(Repository r, DatabaseConnection dc, String tableName, List<String> columnNames, List<String> columnTypes,
			List<String> columnCompressionEncodings, List<Integer> actions) throws SQLException
	{
		Connection conn = dc.ConnectionPool.getConnection();
		if (conn == null)
		{
			throw (new SQLException("No database connection present"));
		}
		boolean exists = tableExists(r, dc, tableName, false);
		if (exists)
		{
			for (int i = 0; i < columnNames.size(); i++)
			{
				/**
				 * Drop all indices that refer to the column
				 */
				if (actions.get(i) != ALTER_ADD)
				{
					dropAllIndicesOnColumn(dc, columnNames.get(i), tableName);
				}
				StringBuilder sb = new StringBuilder();
				sb.append("ALTER TABLE " + getQualifiedTableName(dc, tableName));
				String cname = columnNames.get(i);
				if (dc.DBType == DatabaseType.Infobright || dc.DBType == DatabaseType.MYSQL)
				{
					if (cname.length() > 64)
					{
						logger.warn("Truncating column " + columnNames.get(i) + " to 64 characters - this will probably cause other operations to fail");
						cname = cname.substring(0, 64);
					}
				}
				if (actions.get(i) == ALTER_ADD)
				{
					sb.append(" ADD " + columnNames.get(i) + " " + columnTypes.get(i));
					if (dc.supportsColumnCompression() && columnCompressionEncodings != null && columnCompressionEncodings.get(i) != null)
						sb.append(" ENCODE ").append(columnCompressionEncodings.get(i));
				} else if (actions.get(i) == ALTER_DROP)
				{
					if (DatabaseConnection.isDBTypeOracle(dc.DBType))
					{
						sb.append(" DROP COLUMN " + columnNames.get(i));
					}
					else
					{
						sb.append(" DROP " + columnNames.get(i));
					}
				} else if (actions.get(i) == ALTER_COLUMN)
				{
					if (DatabaseConnection.isDBTypeOracle(dc.DBType))
					{
						sb.append(" MODIFY( " + columnNames.get(i) + " " + columnTypes.get(i) + ")");
					}
					else if (DatabaseConnection.isDBTypeSAPHana(dc.DBType))
					{
						sb.append(" ALTER( " + columnNames.get(i) + " " + columnTypes.get(i) + ")");
					}
					else
					{
						sb.append(" ALTER COLUMN " + columnNames.get(i) + " " + columnTypes.get(i));
					}
				}
				Statement stmt = null;
				try
				{
					stmt = conn.createStatement();
					logger.info(Query.getPrettyQuery(sb.toString()));
					stmt.executeUpdate(sb.toString());
				} finally
				{
					closeStatement(stmt);
				}
			}
			return true;
		} else
		{
			logger.info("Table " + getQualifiedTableName(dc, tableName) + " does not exist - cannot alter");
			return false;
		}
	}

	/**
	 * Get the schema for a table in the database. Returns an array of Object[5], containing: column name, numeric data
	 * type, type name, column size, decimal digits
	 * 
	 * @param r
	 * @param tableName
	 * @return
	 * @throws SQLException
	 */
	public static List<Object[]> getTableSchema(DatabaseConnection dc, Connection conn, String catalog, String schema, String tableName) throws SQLException
	{
		if (conn == null)
		{
			return (null);
		}
		List<Object[]> results = new ArrayList<Object[]>();
		java.sql.DatabaseMetaData meta = conn.getMetaData();
		if (dc.upCaseMetadataQueryParameters())
		{
			if (catalog != null)
				catalog = catalog.toUpperCase();
			if (schema != null)
				schema = schema.toUpperCase();
			if (tableName != null)
				tableName = tableName.toUpperCase();
		} else if (dc.lowerCaseMetadataQueryParameters())
		{
			if (catalog != null)
				catalog = catalog.toLowerCase();
			if (schema != null)
				schema = schema.toLowerCase();
			if (tableName != null)
				tableName = tableName.toLowerCase();
		}
		ResultSet set = dc.DBType == DatabaseType.Infobright || dc.DBType == DatabaseType.MYSQL || dc.DBType == DatabaseType.MemDB ? 
				meta.getColumns(schema, null, tableName, null) : meta.getColumns(catalog, schema, tableName, null);
		while (set.next())
		{
			Object[] row = new Object[5];
			row[0] = set.getString("COLUMN_NAME");
			row[1] = set.getInt("DATA_TYPE");
			row[2] = set.getString("TYPE_NAME");
			row[3] = set.getInt("COLUMN_SIZE");
			row[4] = set.getInt(dc.getDecDigitsName());
			results.add(row);
		}
		set.close();
		return (results);
	}		
	
	public static List<Object[]> getQuerySchema(Connection conn, String query) throws SQLException
	{
		if (conn == null)
		{
			return (null);
		}
		List<Object[]> results = new ArrayList<Object[]>();
		PreparedStatement pstmt = null;
		try
		{
			pstmt = conn.prepareStatement(query, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			pstmt.setFetchSize(Integer.MIN_VALUE);
			pstmt.setMaxRows(1);
			ResultSetMetaData rsmd = pstmt.getMetaData();
			for (int i=1; i<=rsmd.getColumnCount(); i++)
			{
				Object[] row = new Object[6];
				row[0] = rsmd.getColumnLabel(i);
				row[1] = rsmd.getColumnType(i);
				row[2] = rsmd.getColumnTypeName(i);
				row[3] = rsmd.getColumnDisplaySize(i);
				row[4] = rsmd.getPrecision(i);
				row[5] = rsmd.getScale(i);
				results.add(row);
			}
		}
		finally
		{
			closePreparedStatement(pstmt);
		}
		return (results);
	}

	/**
	 * Return a list of all the catalogs in a database
	 * 
	 * @param dtype
	 *            database type
	 * @param conn
	 *            database connection
	 * @return
	 * @throws SQLException
	 */
	public static List<String> getCatalogs(DatabaseConnection dbc, Connection conn) throws SQLException
	{
		if (conn == null)
		{
			return (null);
		}
		List<String> results = new ArrayList<String>();
		// SQL Server returns all databases in the server, which is definitely not what we want, we only want the DB
		// associated with the connection
		if (DatabaseConnection.isDBTypeMSSQL(dbc.DBType))
		{
			String name = conn.getCatalog();
			results.add(name);
		} else
		{
			java.sql.DatabaseMetaData meta = conn.getMetaData();
			ResultSet set = meta.getCatalogs();
			while (set.next())
			{
				results.add(set.getString(1));
			}
			set.close();
		}
		return (results);
	}

	private static boolean useTableCache(DatabaseConnection dbc, String schema, String tableName)
	{
		if ((schema == null || schema.length() == 0 || (dbc.Schema != null && dbc.Schema.equals(schema))) && tableName == null){
			return true;
		}
		return false;
	}
	
	public static List<String> getTables(DatabaseConnection dbc, String catalog, String schema, String tableName) throws SQLException
	{
		return getTables(dbc, catalog, schema, tableName, -1);
	}
	
	/**
	 * Return a list of all the tables in a catalog
	 * 
	 * @param dtype
	 *            Type of database connecting to
	 * @param conn
	 *            Connection
	 * @param catalog
	 *            Catalog to import Catalog to get tables from
	 * @param tableName
	 *            Table name to filter on
	 * @return
	 * @throws SQLException
	 */
	public static List<String> getTables(DatabaseConnection dbc, String catalog, String schema, String tableName,int retryAttempt) throws SQLException
	{
		List<String> results = null;
		if (useTableCache(dbc, schema, tableName))
		{
			results = tableCache.get(dbc);
			if (results != null)
			{
				return results;
			}
		}
		results = new ArrayList<String>();
		Connection conn = dbc.ConnectionPool.getConnection();
		if (conn == null)
		{
			return null;
		}
		DatabaseType dtype = dbc.DBType;
		if (dtype == DatabaseType.MemDB)	// this really should have been folded into MemDB rather than complicating the base code XXX
		{
			Statement stmt = null;
			ResultSet rs = null;

			try
			{
				stmt = conn.createStatement();
				String query = "select TABLE_NAME from INFORMATION_SCHEMA.TABLES";
				if (schema != null)
					query += " WHERE TABLE_SCHEMA='" + schema + "'";
				else if (catalog != null)
					query += " WHERE TABLE_CATALOG='" + catalog + "'";
				if (tableName != null)
				{
					if (schema != null || catalog != null)
						query += " AND TABLE_NAME='" + tableName + "'";
					else
						query += " WHERE TABLE_NAME='" + tableName + "'";
				}
				rs = stmt.executeQuery(query);
				while (rs.next())
				{
					results.add(rs.getString(1).toLowerCase());
				}
			}
			finally
			{
				if (rs != null)
					rs.close();
				if (stmt != null)
					stmt.close();
			}
		}
		else if (DatabaseConnection.isDBTypeMSSQL(dtype))
		{
			PreparedStatement stmt = null;
			ResultSet rs = null;
			String query = 
					"select o.name, s.name " +
					"from sys.objects o join sys.schemas s on o.schema_id = s.schema_id  " +
					"where o.type in ('S', 'U', 'V')";
			if (schema != null)
				query += " and s.name=?";
			if (tableName != null)
				query += " and o.name=?";

			try
			{
				stmt = conn.prepareStatement(query);
				if (schema != null)
					stmt.setString(1, schema);
				if (tableName != null)
					stmt.setString(schema != null ? 2 : 1, tableName);
				rs = stmt.executeQuery();
				while (rs.next())
				{
					String result = rs.getString(1).toLowerCase();
					results.add(result);
				}
			}
			finally
			{
				if (rs != null)
					rs.close();
				if (stmt != null)
					stmt.close();
			}
		}
		else
		{
			if (dbc.upCaseMetadataQueryParameters())
			{
				if (catalog != null)
					catalog = catalog.toUpperCase();
				if (schema != null)
					schema = schema.toUpperCase();
				if (tableName != null)
					tableName = tableName.toUpperCase();
			} 
			else if (dbc.lowerCaseMetadataQueryParameters())
			{
				if (catalog != null)
					catalog = catalog.toLowerCase();
				if (schema != null)
					schema = schema.toLowerCase();
				if (tableName != null)
					tableName = tableName.toLowerCase();
			}
			java.sql.DatabaseMetaData meta = conn.getMetaData();
			ResultSet set = null;
			try
			{							
				if (dtype == DatabaseType.MYSQL || dtype == DatabaseType.Infobright)
					set = meta.getTables(schema, null, tableName, null);
				else if (dtype == DatabaseType.SiebelAnalytics || catalog == null || catalog.isEmpty())
					set = meta.getTables(null, schema, tableName, null);
				else
					set = meta.getTables(catalog, schema, tableName, null);
				while (set.next())
				{
					String catalogName = set.getString(1);
					String schemaName = set.getString(2);
					String tname = set.getString(3);
					String tableType = set.getString(4);
					if ((tableType != null && tableType.equals("TABLE")) || (dtype == DatabaseType.ODBCExcel && tableType.equals("SYSTEM TABLE")))
					{
						if (DatabaseConnection.isDBTypeParAccel(dtype) && schema != null && !schema.isEmpty() && schemaName.equalsIgnoreCase(schema))
						{
							results.add(tname.toLowerCase());
						} 
						else if (catalog == null || catalog.isEmpty() || catalogName.equalsIgnoreCase(catalog))
						{
							results.add(tname.toLowerCase());
						}
					}
				}
			}
			catch (PSQLException psqlException)
			{				
				if (retryAttempt < 0)
					retryAttempt = 0;
				if ((retryAttempt < 5) && (dtype == DatabaseType.Redshift))
				{
					logger.error("Exception encountered while getting list of tables, Retrying "+psqlException.getMessage());
					try
					{
						if (set != null)
							set.close();
					}
					catch(Exception ex)
					{
						logger.error("Exception while closing resultset "+ex.toString());
					}
					try {
						Thread.sleep(5000); //Sleep for 5 seconds before retrying
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					return getTables(dbc, catalog, schema, tableName,++retryAttempt);
				}
				else if (retryAttempt >= 5)
				{
					logger.error("Maximum number of retries reached for getting table list ",psqlException);					
				}				
				throw psqlException; // For non-redshift and after maximum tries have been reached
			}
			finally
			{
				if (set != null)
					set.close();
			}
		}
		// use the same condition check while retrieving
		if (useTableCache(dbc, schema, tableName))
		{ 
			tableCache.put(dbc, results);
		}
		return results;
	}
	

	/**
	 * Return true if one of our indexes exists on table/column pair
	 * 
	 * @param dtype
	 *            Type of the database connecting to
	 * @param conn
	 *            Connection
	 * @param catalog
	 *            Catalog to get table from
	 * @param table
	 *            Name of the table
	 * @param column
	 *            Name of the column
	 * @return true if there is an index on this table/column pair
	 * @throws SQLException
	 */
	private static boolean indexExistsOnColumn(DatabaseConnection dc, String table, String column) throws SQLException
	{
		if (!dc.supportsIndexes())
			return true;
		LevelKey lk = new LevelKey();
		lk.ColumnNames = new String[]
		{ Util.unQualifyPhysicalName(column) };
		return (indexExistsOnKey(dc, table, lk, false, false));
	}

	public static String getIndexName(DatabaseConnection dconn, String tableName, LevelKey lk, boolean measureTable, int length,Repository r)
	{
		StringBuilder sb = new StringBuilder();
		boolean isUnicodeHashRequired = (r!=null) ? (r.getCurrentBuilderVersion()>=22):false;
		String base = (measureTable ? "M" : "D") + "X_" + tableName;
		if (base.length() > length)
			base = shrinkBaseName(base, length);
		String result = null;
		for (String s : lk.ColumnNames)
		{
			if (lk.Level != null && lk.Level.Hierarchy != null)
			{
				if(isUnicodeHashRequired && Util.containsNonAsciiCharacters(lk.Level.Hierarchy.toString()))
				{
					sb.append(Util.replaceWithPhysicalString(lk.Level.Hierarchy.toString(),r) + "$" + Util.replaceWithPhysicalString(s,r));
				}else{
					sb.append(lk.Level.Hierarchy + "$" + s);
				}
			}
			else
			{
				if(isUnicodeHashRequired && Util.containsNonAsciiCharacters(s))
				{
					sb.append(Util.replaceWithPhysicalString(s,r));
				}else{
					sb.append(s);
				}
			}
		}
		result = sb.toString().toUpperCase();
		return getIndexName(dconn, base, result, length).toUpperCase();
	}
	public static final int MAX_SQL_SERVER_INDEX_NAME_LENGTH = 127;
	public static final int MAX_MYSQL_INDEX_NAME_LENGTH = 60;
	public static final int MAX_ORACLE_INDEX_NAME_LENGTH = 30;

	public static String getIndexName(DatabaseConnection dconn, String base, String result, int length)
	{
		if (result.length() >= length - base.length())
		{
			result = EncryptionService.getInstance().createDigest(result);
			if (result.length() > length - base.length())
				result = result.substring(0, length - base.length());
		}
		result = Util.replaceWithPhysicalString(base + result);
		if (result.startsWith("_") && !DatabaseConnection.supportsBeginUnderScore(dconn.DBType))
		{
			result = "X" + result;
		}
		if (result.length() > length)
			result = result.substring(0, length);
		return result;
	}

	public static String shrinkBaseName(String base, int length)
	{
		String result = EncryptionService.getInstance().createDigest(base);
		if (result.length() > length)
			result = result.substring(0, length);
		return result;
	}

	/**
	 * Return true if one of our indexes exists on table/level key pair
	 * 
	 * @param dtype
	 *            Type of the database connecting to
	 * @param conn
	 *            Connection
	 * @param catalog
	 *            Catalog to get table from
	 * @param tableName
	 *            Name of the table
	 * @param lk
	 *            Level key
	 * @return true if there is an index on this table/column pair
	 * @throws SQLException
	 */
	public static boolean indexExistsOnKey(DatabaseConnection dc, String tableName, LevelKey lk, boolean measureTable, boolean qualifyNew) throws SQLException
	{
		return indexExistsOnKey(dc, tableName, lk, measureTable, qualifyNew,null);
	}
	public static boolean indexExistsOnKey(DatabaseConnection dc, String tableName, LevelKey lk, boolean measureTable, boolean qualifyNew,Repository r) throws SQLException
	{
		if (!dc.supportsIndexes())
			return true;
		String[] lkColNames = null;
		boolean requiresHashedIdentifier = dc.requiresHashedIdentifier();
		if (lk.ColumnNames != null && lk.ColumnNames.length > 0)
		{
			lkColNames = new String[lk.ColumnNames.length];
			for (int i = 0; i < lk.ColumnNames.length; i++)
			{
		String colname = (measureTable ? Util.replaceWithPhysicalString(lk.Level.Hierarchy.DimensionName,r) + dc.getPhysicalSuffix() : "")
						+ (qualifyNew ? Util.replaceWithNewPhysicalString(lk.ColumnNames[i], dc,r) : Util.replaceWithPhysicalString(lk.ColumnNames[i],r));			
		if (requiresHashedIdentifier)
		{
					String hashedStr = null;
					if(colname != null && colname.endsWith("CKSUM$"))
					{
						colname = dc.getChecksumName(colname);
					}
					else
					{
						hashedStr = dc.getHashedIdentifier(colname);
						if (!colname.equalsIgnoreCase(hashedStr))
							colname = qualifyNew ? Util.replaceWithNewPhysicalString(hashedStr, dc,r) : Util.replaceWithPhysicalString(hashedStr);
					}
				}
				lkColNames[i] = colname;
			}
		}
		String expectedIndexName = getIndexName(dc, tableName, lk, measureTable, dc.DBType == DatabaseType.MYSQL ? MAX_MYSQL_INDEX_NAME_LENGTH
				: (dc.DBType == DatabaseType.Oracle ? MAX_ORACLE_INDEX_NAME_LENGTH : MAX_SQL_SERVER_INDEX_NAME_LENGTH),r);
		return indexExistsOnColumns(dc, tableName, lkColNames,r,expectedIndexName);
	}
	
	public static boolean indexExistsOnColumns(DatabaseConnection dc, String tableName, String[] lkColNames, Repository r, String expectedIndexName) throws SQLException
	{
		Connection conn = dc.ConnectionPool.getConnection();
		java.sql.DatabaseMetaData meta = conn.getMetaData();
		boolean isOracle = DatabaseConnection.isDBTypeOracle(dc.DBType);
		if (isOracle)
			tableName = tableName.toUpperCase();
		ResultSet set = meta.getIndexInfo(dc.DBType == DatabaseType.MYSQL ? dc.Schema : null, dc.DBType == DatabaseType.MYSQL ? null : dc.Schema, tableName,
				false, true);
		int matchCount = 0;
		Map<String, Set<String>> indexNameToColsMap = new WeakHashMap<String, Set<String>>();
		boolean matchingIndexNameFound = false;
		if (set != null)
		{
			if (isOracle)
			{
				List<String> colNames = new ArrayList<String>();
				for (String colName : lkColNames)
				{
					colNames.add(colName.toUpperCase());
				}
				lkColNames = colNames.toArray(new String[0]);
			}
			while (set.next())
			{
				String indexName = set.getString("INDEX_NAME");
				String columnName = set.getString("COLUMN_NAME");
				if (indexName != null && columnName != null && lkColNames != null)//prepare index name to index columns map
				{
					Set<String> indexColumns = indexNameToColsMap.get(indexName);
					if (indexColumns == null)
						indexColumns = new HashSet<String>();
					indexColumns.add(columnName);
					indexNameToColsMap.put(indexName, indexColumns);
				}
				if (indexName != null && indexName.equalsIgnoreCase(expectedIndexName))
				{
					matchingIndexNameFound = true;
					if (lkColNames != null && Arrays.asList(lkColNames).indexOf(columnName) >= 0)
						matchCount++;
				} 
			}
			set.close();
		}
		if (!matchingIndexNameFound && lkColNames != null) // rename index if columns are already indexed and index name not matching with expected index name
		{
			List<String> lkColumns = Arrays.asList(lkColNames);
			for (String indexName : indexNameToColsMap.keySet())
			{
				Set<String> indexColumns = indexNameToColsMap.get(indexName);
				if (indexColumns != null)
				{
					if (lkColNames.length == indexColumns.size() && indexColumns.containsAll(lkColumns))
					{
						return Database.renameIndex(dc, tableName, indexName, expectedIndexName);
					}
				}
			}
		}
		return (lkColNames!=null? (matchCount == lkColNames.length):false);
	}

	/**
	 * Return true if an index with the given name exists on the given table
	 * 
	 * @param dc
	 * @param tableName
	 * @param indexName
	 * @return
	 * @throws SQLException
	 */
	public static boolean indexExists(DatabaseConnection dc, String tableName, String indexName) throws SQLException
	{
		if (!dc.supportsIndexes())
			return true;
		Connection conn = dc.ConnectionPool.getConnection();
		java.sql.DatabaseMetaData meta = conn.getMetaData();
		if (DatabaseConnection.isDBTypeOracle(dc.DBType))
			tableName = tableName.toUpperCase();
		ResultSet set = meta.getIndexInfo(dc.DBType == DatabaseType.MYSQL ? dc.Schema : null, dc.DBType == DatabaseType.MYSQL ? null : dc.Schema, tableName,
				false, true);
		boolean exists = false;
		while (set.next())
		{
			String idxName = set.getString("INDEX_NAME");
			if (idxName != null && idxName.equalsIgnoreCase(indexName))
			{
				exists = true;
				break;
			}
		}
		set.close();
		return (exists);
	}

	/**
	 * Return true if an index with the given name exists on the given table
	 * 
	 * @param dc
	 * @param tableName
	 * @param indexName
	 * @return
	 * @throws SQLException
	 */
	public static boolean columnStoreIndexExists(DatabaseConnection dc, String tableName, String indexName) throws SQLException
	{
		if (dc.DBType != DatabaseType.MSSQL2012ColumnStore && dc.DBType != DatabaseType.MSSQL2014)
			return false;
		Connection conn = dc.ConnectionPool.getConnection();
		java.sql.DatabaseMetaData meta = conn.getMetaData();
		if (DatabaseConnection.isDBTypeOracle(dc.DBType))
			tableName = tableName.toUpperCase();
		ResultSet set = meta.getIndexInfo(dc.DBType == DatabaseType.MYSQL ? dc.Schema : null, dc.DBType == DatabaseType.MYSQL ? null : dc.Schema, tableName,
				false, true);
		boolean exists = false;
		while (set.next())
		{
			String idxName = set.getString("INDEX_NAME");
			if (idxName != null && idxName.equalsIgnoreCase(indexName))
			{
				exists = true;
				break;
			}
		}
		set.close();
		return (exists);
	}

	/**
	 * create the formula for calculating the checksum BINARY_CHECKSUM for MSSQL Summ of the CRC32 of columns for
	 * MySQL/Infobright, modulo 2^32-l
	 * 
	 * @param list
	 *            list of columns to be included in the checksum calculation
	 * @return SQL scalar function for the checksum
	 */
	public static String getChecksumFormula(DatabaseConnection dc, List<StagingColumn> list,Repository r)
	{
		if (!DatabaseConnection.isDBTypeMSSQL(dc.DBType) && !DatabaseConnection.isDBTypeMySQL(dc.DBType))
			return null; // XXX should really throw a 'OperationNotSupported' exception.. future..
		StringBuilder sb = new StringBuilder();
		boolean first = true;
		for (StagingColumn sc : list)
		{
			/*
			 * Skip the ones with column transformations since they wont have corresponding columns on the staging table
			 */
			if ((sc.Transformations != null) && (sc.Transformations.length > 0))
			{
				continue;
			}
			/*
			 * Skip the ones that are specifically marked to ignore changes.
			 */
			if (sc.ExcludeFromChecksum)
			{
				continue;
			}
			if (first)
				first = false;
			else
			{
				if (DatabaseConnection.isDBTypeMySQL(dc.DBType))
					sb.append('+');
				else
					sb.append(',');
			}
			if (DatabaseConnection.isDBTypeMySQL(dc.DBType))
				sb.append("CRC32(IFNULL(" + Util.replaceWithNewPhysicalString(sc.getPhysicalName(), dc,r) + ",0))");
			else
			{
				/*
				 * SQL function binary_checksum returns non-unique values for numeric data types so convert any numerics
				 * to float Example: select binary_checksum(cast(200 as numeric(19,6))), binary_checksum(cast(2000 as
				 * numeric(19,6))) returns checksum value of 1546536714 for both the select columns although the
				 * underlying values are different
				 */
				boolean isNumeric = (sc.DataType != null && sc.DataType.equals("Number"));
				if (isNumeric)
				{
					sb.append("CONVERT(FLOAT, ");
				}
				sb.append(Util.replaceWithNewPhysicalString(sc.getPhysicalName(), dc,r));
				if (isNumeric)
				{
					sb.append(")");
				}
			}
		}
		if (sb.length() == 0)
			return null;
		if (DatabaseConnection.isDBTypeMySQL(dc.DBType))
		{
			return "(" + sb.toString() + ")%2147483647";
		} else
		{
			return "BINARY_CHECKSUM(" + sb.toString() + ")";
		}
	}

	/**
	 * Create physical definition for a given staging table in the database
	 * 
	 * @param dc
	 *            Database connection
	 * @param st
	 *            Staging table
	 * @param scmap
	 *            Staging column map
	 * @return
	 * @throws Exception 
	 */
	public static boolean createPhysicalStagingTable(Repository r, DatabaseConnection dc, StagingTable st, Map<DimensionTable, List<StagingColumn>> scmap)
			throws Exception
	{
		List<String> colNames = new ArrayList<String>();
		List<String> colTypes = new ArrayList<String>();
		boolean isPADB = DatabaseConnection.isDBTypeParAccel(dc.DBType);
		boolean isIB = DatabaseConnection.isDBTypeInfoBright(dc.DBType);
		List<String> columnCompressionEncodings = ((dc.DBType == DatabaseType.ParAccel) ? new ArrayList<String>() : null); // only do this for PA, not Redshift (Redshift can auto calculate this)
		List<String> distributionKeys = isPADB ? new ArrayList<String>() : null;
		Set<String> sortKeys = ( isPADB  || isIB ) ? new HashSet<String>() : null;
		getStagingTableMetaData(r, dc, st, scmap, colNames, colTypes, columnCompressionEncodings, distributionKeys, sortKeys);
		String tableName = Util.replaceWithPhysicalString(st.Name);
		// Create the table
		if (tableExists(r, dc, tableName, false))
		{
			if (st.DiscoveryTable && !st.TruncateOnLoad)
			{
				List<String> modifiedColumnNames = new ArrayList<String>();
				List<String> modifiedColumnTypes = new ArrayList<String>();
				List<String> modifiedColumnCompressionEncodings = (isPADB ? new ArrayList<String>() : null);
				List<Integer> actions = new ArrayList<Integer>();
				Connection conn = dc.ConnectionPool.getConnection();
				boolean containsModifiedColumn = false;
				/*
				 * Preserve the staging table and alter any columns as needed.
				 */
				List<Object[]> schema = Database.getTableSchema(dc, conn, conn.getCatalog(), dc.Schema, tableName);
				for (int colno = 0; colno < colNames.size(); colno++)
				{
					String physicalColName = colNames.get(colno);
					String type = colTypes.get(colno);
					boolean found = false;
					boolean match = false;
					String actualType = null;
					for (int i = 0; i < schema.size(); i++)
					{
						if (((String) schema.get(i)[0]).equalsIgnoreCase(physicalColName))
						{
							found = true;
							actualType = schema.get(i)[2].toString().toUpperCase();
							int actualWidth = schema.get(i)[3] instanceof Integer ? (Integer) (schema.get(i)[3]) : 0;
							int actualPrecision = schema.get(i)[4] instanceof Integer ? (Integer) (schema.get(i)[4]) : 0;
							int width = -1;
							if (Database.isVarcharType(actualType))
							{
								int index1 = type.indexOf('(');
								int index2 = type.indexOf(')');
								if (index1 > 0 && index2 > index1)
								{	
									width = Integer.valueOf(type.substring(index1 + 1, index2));
									//type = type.substring(0, index1);
								}
							}
							type = type.toUpperCase();
							boolean ignoreTextToVarchar =  DatabaseConnection.isDBTypeInfoBright(dc.DBType) && r.getCurrentBuilderVersion()>=30;
							match = Database.checkColumnMatch(actualType, actualWidth, actualPrecision, type, width, dc, false, ignoreTextToVarchar);
							break;
						}
					}
					if (!found)
					{
						logger.debug("adding to " + tableName + " column " + physicalColName + " of type " + type);
						modifiedColumnNames.add(physicalColName);
						modifiedColumnTypes.add(type);
						if (isPADB)
							modifiedColumnCompressionEncodings.add(columnCompressionEncodings.get(colno));
						actions.add(Database.ALTER_ADD);
					
					} else if (!match)
					{
						containsModifiedColumn = true;
						if (!isPADB)
						{
							// If incompatible types, remove existing data to proceed
							if ((actualType.toLowerCase().startsWith("varchar") || actualType.toLowerCase().startsWith("nvarchar"))
									&& !DatabaseConnection.isDBTypeMemDB(dc.DBType))
							{
								String ptname = Database.getQualifiedTableName(dc, tableName);
								String query = "SELECT COUNT(T.C) FROM (SELECT CAST(" + physicalColName + " AS " + type + ") AS C FROM " + ptname + ") T";
								Statement stmt = null;
								ResultSet rs = null;
								try
								{
									stmt = dc.ConnectionPool.getConnection().createStatement();
									logger.debug("Verifying datatype conversion from " + actualType + " to " + type + ": " + Query.getPrettyQuery(query));
									rs = stmt.executeQuery(query);
									rs.next();
								} catch (SQLException ex)
								{
									if(!DatabaseConnection.isDBTypeInfoBright(dc.DBType))
									{
										logger.error("Can not covert data type from " + actualType + " to " + type);
										throw ex;
									}
								} finally
								{
									if (rs != null)
										rs.close();
									if (stmt != null)
										stmt.close();
								}
							}
						}
						modifiedColumnNames.add(physicalColName);
						modifiedColumnTypes.add(type);
						if (isPADB)
							modifiedColumnCompressionEncodings.add(columnCompressionEncodings.get(colno));
						logger.info("altering in " + tableName + " column " + physicalColName + " from " + actualType + " to " + type);
						actions.add(Database.ALTER_COLUMN);
					}
				}
					
				if(modifiedColumnNames != null && modifiedColumnNames.size() > 0)
				{
					if (dc.supportsAlterTable() && (!containsModifiedColumn || dc.supportsAlterTableModifyColumn()))
					{
						Database.alterTable(r, dc, tableName, modifiedColumnNames, modifiedColumnTypes, modifiedColumnCompressionEncodings, actions);
					} else
					{
						
						// Do a create table with a new name, then a select into, drop and rename
						String physicalNameTemp = dc.getHashedIdentifier(tableName + "_TEMP");
						StringBuilder sb = new StringBuilder("Altering table: " + tableName + ": ");
						for (int i = 0; i < modifiedColumnNames.size(); i++)
							sb.append((i > 0 ? ", " : "") + modifiedColumnNames.get(i) + ": " + modifiedColumnTypes.get(i));
						logger.info(sb.toString());
						// Generate create table
						List<String> columnNamesTemp = colNames;
						List<String> columnTypesTemp = colTypes;
						List<String> columnCompressionEncodingsTemp = columnCompressionEncodings;
						GenerateSchema gs = new GenerateSchema(r);
						if(isIB && gs.isExceedingMaxPossibleRowSize(physicalNameTemp, columnTypesTemp,dc))
						{
							logger.error(tableName + " Exceeding Maximum Possible Row Size for Infobright.");
							List<String> levelKeysList = new ArrayList<String>(sortKeys);
							columnTypesTemp = gs.convertVarcharToText(physicalNameTemp,columnTypesTemp,dc,levelKeysList,columnNamesTemp);
						}
						Database.createTable(r, dc, physicalNameTemp, columnNamesTemp, columnTypesTemp, true, true, columnCompressionEncodingsTemp, null, null);
						StringBuilder insertInto = new StringBuilder("INSERT INTO " + Database.getQualifiedTableName(dc, physicalNameTemp) + " SELECT ");
						boolean first = true;
						for (String cname : columnNamesTemp)
						{
							String val = cname;
							if (first)
								first = false;
							else
								insertInto.append(',');
							for (int index = 0; index < modifiedColumnNames.size(); index++)
							{
								if (modifiedColumnNames.get(index).equals(cname))
								{
									if (actions.get(index) == Database.ALTER_ADD)
										val = "NULL";
									else if (actions.get(index) == Database.ALTER_COLUMN)
									{
										String toModifiyColumnType = modifiedColumnTypes.get(index);
										if (toModifiyColumnType.toUpperCase().startsWith("VARCHAR"))
											toModifiyColumnType = Util.replaceStr(toModifiyColumnType, "VARCHAR", dc.getCastableCharType());
										else if (toModifiyColumnType.toUpperCase().startsWith("NVARCHAR"))
											toModifiyColumnType = Util.replaceStr(toModifiyColumnType, "NVARCHAR", dc.getCastableCharType());
										else if (toModifiyColumnType.equalsIgnoreCase("BIGINT"))
											toModifiyColumnType = dc.getCastableIntegerType();
										else if (toModifiyColumnType.equalsIgnoreCase("INT") || toModifiyColumnType.equalsIgnoreCase("INTEGER"))
											toModifiyColumnType = dc.getCastableIntegerType();
										else if (toModifiyColumnType.toUpperCase().startsWith("FLOAT") || toModifiyColumnType.toUpperCase().startsWith("DOUBLE")
												|| toModifiyColumnType.toUpperCase().startsWith("DEC"))
											toModifiyColumnType = dc.getNumericType();
										
										val = "CAST(" + cname + " AS " + toModifiyColumnType + ")";
									}
									break;
								}
							}
							insertInto.append(val);
						}
						insertInto.append(" FROM " + Database.getQualifiedTableName(dc, tableName));
						logger.info("Moving database rows from " + tableName);
						logger.debug(Query.getPrettyQuery(insertInto.toString()));
						Statement stmt = dc.ConnectionPool.getConnection().createStatement();
						stmt.executeUpdate(insertInto.toString());						
						//No need to check if tableName exists since we just inserted from it.
						Database.dropTable(r, dc, tableName, false);
						if (dc.supportsRenameTable())
							Database.renameTable(r, dc, physicalNameTemp, tableName);
						else
						{
							// cannot rename table, need to create original table with same schema as temp table,
							// insert all the rows from temp table to original table and then drop temp table
							String createTableAsQuery = "CREATE TABLE " + tableName + " AS SELECT * FROM " + physicalNameTemp;
							logger.debug(createTableAsQuery);
							stmt.executeUpdate(createTableAsQuery);
							//No need to check if tableName exists since we just inserted from it.
							Database.dropTable(r, dc, physicalNameTemp, false);
						}						
					}					
				}
				
				return true;
			} else
			{
				dropTable(r, dc, Util.replaceWithPhysicalString(st.Name), false);
			}
		}
	
		GenerateSchema gs = new GenerateSchema(r);
		if(isIB && gs.isExceedingMaxPossibleRowSize(tableName, colTypes,dc))
		{
			logger.error(tableName + " Exceeding Maximum Possible Row Size for Infobright.");
			List<String> levelKeysList = new ArrayList<String>(sortKeys);
			colTypes = gs.convertVarcharToText(tableName,colTypes,dc,levelKeysList,colNames);
		}
		if (!Database.createTable(r, dc, tableName, colNames, colTypes, true, !st.DiscoveryTable, columnCompressionEncodings,distributionKeys, sortKeys))
		{
			return false;
		}
		return true;
	}
	
	public static void createScriptLog(Repository r, String tableName) throws SQLException
	{
		if(tableName!=null && tableName.equals("TXN_SCRIPT_LOG"))
		{
			ScriptLog sc = new ScriptLog(r);
		}
	}
	public static void createDiscoveryStagingTable(Repository r,String tableName) throws Exception
	{
		DatabaseConnection dc = r.getDefaultConnection();
		for (StagingTable st : r.getStagingTables())
		{
			if (st.Disabled || st.Imported || st.LiveAccess)
				continue;
			if (tableName != null && !st.Name.equals(tableName))
				continue;
			if(!st.DiscoveryTable)
			{
				logger.warn(tableName + " is not a discovery table. Skipping table creation.");
				continue;
			}
			Map<DimensionTable, List<StagingColumn>> scmap = st.getStagingColumnMap(r);
			Database.createPhysicalStagingTable(r, dc, st, scmap);	
		}
		
	}
	
	public static boolean isVarcharType(String actualType)
	{
		return (actualType.equals("VARCHAR") || actualType.equals("NVARCHAR") || actualType.equals("NCHAR")	|| actualType.equals("NVARCHAR2")); 
	}
	
	public static boolean checkColumnMatch(String actualType, int actualWidth, int actualPrecision, String type, int width, DatabaseConnection dconn, boolean isSurrogateKey,boolean ignoreTextToVarchar)
	{
		boolean match = false;
		// hack for (N)VARCHAR size decrease - do not allow it since the CAST 'SUCCEEDS' and
		// the ALTER TABLE 'FAILS' for SQL Server
		if ((actualType.equals("VARCHAR") && type.toLowerCase().startsWith("varchar"))
				|| (actualType.equals("NVARCHAR") && type.toLowerCase().startsWith("nvarchar"))
				|| (actualType.equals("NCHAR") && type.toLowerCase().startsWith("nchar"))
				|| (actualType.equals("NVARCHAR2") && type.toLowerCase().startsWith("nvarchar")))
		{
			if (width <= actualWidth)
			{
				return true;
			}
		}
		if (actualType.equals("VARCHAR") || actualType.equals("NVARCHAR"))
			actualType = actualType + '(' + actualWidth + ')';
		if (actualType.equals("NUMERIC") || actualType.equals("DECIMAL") || actualType.equals("NUMBER"))
			actualType = actualType + '(' + actualWidth + ',' + actualPrecision + ')';
		// Infobright doesn't support unicode XXX (why is this hack here? shouldn't we just
		// stop the DB from being marked UNICODE?)
		if ((dconn.DBType == DatabaseType.Infobright || dconn.DBType == DatabaseType.MYSQL) && type.startsWith("NVARCHAR"))
			type = type.substring(1);
		if (Database.equalTypes(actualType, type, dconn, isSurrogateKey,ignoreTextToVarchar))
			match = true;
		return match;
	}

	public static void getStagingTableMetaData(Repository r, DatabaseConnection dc, StagingTable st, Map<DimensionTable, List<StagingColumn>> scmap,
			List<String> colNames, List<String> colTypes, List<String> columnCompressionEncodings, List<String> distributionKeys, Set<String> sortKeys) throws Exception
	{
		List<String> ckcolNames = new ArrayList<String>();
		List<String> ckcolTypes = new ArrayList<String>();
		// Create checksums
		for (Entry<DimensionTable, List<StagingColumn>> en : scmap.entrySet())
		{
			String ckname = en.getKey().getChecksumColumnName(dc);
			ckcolNames.add(ckname);
			ckcolTypes.add("INTEGER");
		}
		st.getCreateColumns(dc, r, colNames, colTypes, dc.isUnicodeDatabase(), ckcolNames, ckcolTypes, columnCompressionEncodings, distributionKeys, sortKeys);
		for (int i = 0; i < colTypes.size(); i++)
		{
			if (colTypes.get(i).equals("Number"))
			{
				colTypes.set(i, dc.getNumericType());
			} else if (colTypes.get(i).equals("Float"))
			{
				if (dc.DBType == DatabaseType.MonetDB)
					colTypes.set(i, dc.getNumericType());
				else if (DatabaseConnection.isDBTypeMySQL(dc.DBType))
					colTypes.set(i, "DOUBLE");
				else
					colTypes.set(i, "FLOAT");
			} else if (colTypes.get(i).equals("Date"))
				colTypes.set(i, dc.getDateType());
			else if (colTypes.get(i).equals("DateTime"))
				colTypes.set(i, dc.getDateTimeType());
			else if (colTypes.get(i).startsWith(DateID.Prefix))
				colTypes.set(i, "INTEGER");
			// Smallest type implicitly convertible to any other type
			else if (colTypes.get(i).equals("None"))
			{
				if (dc.DBType == DatabaseType.MonetDB || DatabaseConnection.isDBTypeOracle(dc.DBType) || DatabaseConnection.isDBTypeParAccel(dc.DBType))
					colTypes.set(i, "SMALLINT");
				else if (dc.DBType == DatabaseType.MemDB)
					colTypes.set(i, "INTEGER");
				else
					colTypes.set(i, "TINYINT");
			}
		}
		if (dc.requiresTransactionID())
		{
			st.r = r;
			Map<Level, String> compoundKeys = st.getCompoundKeys();
			for (Entry<Level, String> en : compoundKeys.entrySet())
			{
				String colName = en.getKey().getNaturalKey().getCompoundKeyName(r, dc);
				if(colNames.contains(colName)){
					logger.error("Duplicate compound column, " + colName + " found on stagingTable, " + st.Name);
					throw new Exception("Duplicate compound column, " + colName + " found on stagingTable, " + st.Name);
				}
				colNames.add(colName);
				colTypes.add(en.getValue());
			}
			SourceFile sf = r.findSourceFile(st.SourceFile);
			StagingTableBulkLoadInfo stbl = new StagingTableBulkLoadInfo(st, sf, dc, r);
			stbl.setupGrainKeys(sf);
			boolean isOracle = DatabaseConnection.isDBTypeOracle(dc.DBType);
			String type = isOracle ? "NVarchar2" : "NVarchar";
			for (int i = 0; i < stbl.grainKeyNames.size(); i++)
			{
				colNames.add(dc.getHashedIdentifier(stbl.grainKeyNames.get(i)));
				colTypes.add(type + "(" + stbl.grainKeyWidths.get(i) + ")");
			}
		}
	}

	/**
	 * Update the checksums for a given staging table
	 * 
	 * @param c
	 * @param st
	 * @param scmap
	 * @return
	 */
	public void updateChecksums(DatabaseConnection dc, StagingTable st, Map<DimensionTable, List<StagingColumn>> scmap,Repository r) throws SQLException
	{
		StringBuilder sb = new StringBuilder("UPDATE " + getQualifiedTableName(dc, Util.replaceWithPhysicalString(st.Name)) + " SET ");
		// Calculate checksums
		boolean first = true;
		for (Entry<DimensionTable, List<StagingColumn>> en : scmap.entrySet())
		{
			String cFormula = getChecksumFormula(dc, en.getValue(),r);
			if (cFormula == null)
				continue;
			if (first)
				first = false;
			else
				sb.append(',');
			String ckname = en.getKey().getChecksumColumnName(dc);
			sb.append(ckname + "=" + cFormula);
		}
		if (!first)
		{
			logger.info("Updating checksums: " + st.Name);
			logger.debug(Query.getPrettyQuery(sb));
			Connection conn = dc.ConnectionPool.getConnection();
			Statement stmt = null;
			try
			{
				stmt = conn.createStatement();
				int rows = stmt.executeUpdate(sb.toString());
				logger.info("Updated " + rows + " rows");
			} finally
			{
				closeStatement(stmt);
			}
		}
	}

	public static boolean equalTypes(String a, String b, DatabaseConnection dc, boolean isSurrogateKey,boolean ignoreTextToVarchar)
	{
		if (isSurrogateKey)
		{
			//From repository builder version 15 onwards, we changed surrogate key type to BIGINT (or equivalent) from the earlier use
			//of Integer. So, any new spaces created would always use BIGINT for all surrogate key columns. However, we do not want to
			//attempt any alter table commands for changing existing surrogate integer data types to BIGINT.
			if (DatabaseConnection.isDBTypeMSSQL(dc.DBType))
			{
				if(a.toUpperCase().endsWith("IDENTITY") && b.toUpperCase().endsWith("IDENTITY"))
					return true;
				if ((a.equals("INT") || a.equals("BIGINT")) && (b.equals("INTEGER") || b.equals("BIGINT")))
					return true;
			}									
		}
		
		if (DatabaseConnection.isDBTypeMySQL(dc.DBType) && a.equals("FLOAT") && b.equals("DOUBLE"))
			return true;
		else if (DatabaseConnection.isDBTypeOracle(dc.DBType))
		{
			if (a.equals("TIMESTAMP(6)") && b.equals("TIMESTAMP"))
				return true;
		}
		else if (DatabaseConnection.isDBTypeParAccel(dc.DBType))
		{
			if (a.equals("INT4") && b.equals("INTEGER"))
				return true;
			if (a.equals("INT8") && b.equals("BIGINT"))
				return true;
			if (a.equals("FLOAT8") && b.equals("FLOAT"))
				return true;
			if (a.equals("INT8") && b.equals("BIGINT IDENTITY"))
				return true;
			if (a.equals("INT2") && b.equals("SMALLINT"))
				return true;
		}
		else if (dc.DBType == DatabaseType.Hana)
		{
			if (a.equals("TIMESTAMP") && b.equals("SECONDDATE")) // SECONDDATE in Hana is yyyy-MM-dd HH24:mm:ss, TIMESTAMP goes to nanosconds, but Hana returns it as TIMESTAMP
				return true;
			if (a.equals("SECONDDATE") && b.equals("TIMESTAMP"))
				return true;
			if (a.equals("DOUBLE") && b.equals("FLOAT"))
				return true;
			if (a.equals("FLOAT") && b.equals("DOUBLE"))
				return true;
		}
		else if (DatabaseConnection.isDBTypeInfoBright(dc.DBType))
		{
			if(a.equals("TEXT") && b.startsWith("VARCHAR(") && ignoreTextToVarchar)
				return true;
		}
		else if (DatabaseConnection.isDBTypeMemDB(dc.DBType))
		{
			if (a.startsWith("VARCHAR(") && b.startsWith("VARCHAR("))
				return true;
			if (a.equals("DOUBLE") && b.equals("FLOAT"))
				return true;
			if (a.equals("DATETIME") && b.equals("TIMESTAMP"))
				return true;
			if (a.equals("TIMESTAMP") && b.equals("DATETIME"))
				return true;
			if (a.indexOf("PRIMARY KEY") >= 0)
			{
				a = Util.replaceStr(a, "PRIMARY KEY", "");
				a = a.trim();
			}
			if (b.indexOf("PRIMARY KEY") >= 0)
			{
				b = Util.replaceStr(b, "PRIMARY KEY", "");
				b = b.trim();
			}
			if (a.indexOf("FOREIGN KEY") >= 0)
			{
				a = Util.replaceStr(a, "FOREIGN KEY", "");
				a = a.trim();
			}
			if (b.indexOf("FOREIGN KEY") >= 0)
			{
				b = Util.replaceStr(b, "FOREIGN KEY", "");
				b = b.trim();
			}			
		}
		a = Util.replaceStr(a, "INTEGER", "INT");
		b = Util.replaceStr(b, "INTEGER", "INT");
		if (a.equals("NTEXT"))
			a = "NVARCHAR(MAX)";
		else if (dc.DBType != DatabaseType.Infobright && a.equals("TEXT"))
			a = "VARCHAR(MAX)";
		return (a.equals(b));
	}

	/**
	 * Return a modified version of the type string to create an autonumbered identity type column
	 * 
	 * @param dc
	 * @param type
	 * @return
	 */
	public static String getIdentityType(DatabaseConnection dc, String type, String tableName, String colName, Repository r)
	{
		Database.identityColumns.put(tableName, colName);
		if (!dc.supportsIdentity())
			return type;
		if(r.getCurrentBuilderVersion() > 14)
		{
			type = dc.getIdentityDataType();
		}
		if (dc.DBType == Database.DatabaseType.MYSQL)
			return type + " AUTO_INCREMENT UNIQUE KEY";
		if (dc.DBType == Database.DatabaseType.MonetDB)
			return type + " AUTO_INCREMENT";
		if (DatabaseConnection.isDBTypeMSSQL(dc.DBType) || DatabaseConnection.isDBTypeParAccel(dc.DBType))
			return type + " IDENTITY";
		if (dc.DBType == Database.DatabaseType.MemDB)
			return type + " PRIMARY KEY";
		return type;
	}
	
	private static Map<String, String> identityColumns = new HashMap<String, String>();

	public static String getIdentityColumn(String tableName) {
		return identityColumns.get(tableName);
	}

	public static String getForeignKeyType(DatabaseConnection dc, String currentType)
	{
		if (dc.DBType == Database.DatabaseType.MemDB)
			return currentType + " FOREIGN KEY";
		return currentType;
	}

	/**
	 * add the identity column values to the table for those DBs that do not support IDENTITY - assume only one identity
	 * column per table
	 * 
	 * @param dc
	 * @param tableName
	 */
	public static void setIdentityColumns(DatabaseConnection dc, Statement stmt, String tableName) throws SQLException
	{
		if (dc.supportsIdentity())
			return;

		if (!Database.identityColumns.containsKey(tableName))
			return;

		String col = Database.identityColumns.get(tableName);
		if (dc.DBType == DatabaseType.Infobright)
		{
			String setVariable = "set @count=(SELECT " + dc.getIsNullScalarFunction() + "(MAX(" + col + "), 0) FROM "
					+ Database.getQualifiedTableName(dc, tableName) + ")";
			String updateVariable = "UPDATE " + Database.getQualifiedTableName(dc, tableName) + " SET " + col + "=(select @count:=@count+1) WHERE " + col + " IS NULL";
			logger.debug(setVariable);
			stmt.executeUpdate(setVariable);
			logger.debug(updateVariable);
			int cnt = stmt.executeUpdate(updateVariable);
			logger.debug("set " + cnt + " identity columns");
		}
		if (dc.useSequenceForIdentity())
		{
			/** Get Max Number for Sequence START WITH  **/
			String setVariable = "SELECT " + dc.getIsNullScalarFunction() + "(MAX(" + col + "), 0) FROM " + Database.getQualifiedTableName(dc, tableName);
			logger.debug(setVariable);

			ResultSet rs = null;
			long maxValue = 0;
			try
			{
				rs = stmt.executeQuery(setVariable);
				if (rs.next())
				{
					maxValue = rs.getLong(1);
				}
			} finally
			{
				if (rs != null)
					rs.close();
			}
			
			/** Create Temporary Sequence **/
			String tempSequenceName = Database.getQualifiedTableName(dc, "SEQUENCE_" + System.currentTimeMillis());
			String strSequence = "CREATE SEQUENCE " + tempSequenceName  + " START WITH "+ (maxValue + 1);
			if (dc.DBType == DatabaseType.Oracle)
				strSequence += " MINVALUE -1";
			logger.debug(strSequence);
			stmt.executeUpdate(strSequence);
			String updateVariable = "UPDATE " + Database.getQualifiedTableName(dc, tableName) + " SET " + col + "="+tempSequenceName+".nextval  WHERE " + col + " IS NULL";
			logger.debug(updateVariable);
			try
			{
				int cnt = stmt.executeUpdate(updateVariable);
				logger.debug("set " + cnt + " identity columns");
			}
			finally
			{
				stmt.executeUpdate("DROP SEQUENCE " + tempSequenceName);	
			}
			
		}
	}

	/**
	 * Create a partitioning scheme in the database as defined by the parameters of a time definition structure
	 * 
	 * @param dc
	 *            Database connection to use
	 * @param td
	 *            Time Definition structure defining partitioning ranges
	 * @param name
	 *            Scheme name to use (if appropriate)
	 * @return Whether successful
	 */
	public static boolean createPartitionScheme(DatabaseConnection dc, TimeDefinition td, String name)
	{
		if (!dc.supportsPartitioning())
			return false;
		if (!td.PartitionFacts)
			return false;
		/*
		 * Code for setting up partitioning structures for Microsoft SQL Server 2005.
		 */
		Statement stmt = null;
		try
		{
			stmt = dc.ConnectionPool.getConnection().createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM sys.partition_schemes");
			while (rs.next())
			{
				String nm = rs.getString("name");
				if (nm != null && nm.equalsIgnoreCase(name))
				{
					return true; // Found, no need to create it.
				}
			}
			/*
			 * If the partition scheme already exists, no need to recreate. In the future, may want to inspect the
			 * partition scheme to update it (e.g. add more time periods) if necessary
			 */
			// Setup partition function
			StringBuilder sb = new StringBuilder("CREATE PARTITION FUNCTION PF_" + name + " (int) AS RANGE LEFT FOR VALUES (0)");
			logger.info("Creating partitioning function: " + sb.toString());
			stmt.executeUpdate(sb.toString());
			// Now, create partition scheme
			sb = new StringBuilder("CREATE PARTITION SCHEME [" + name + "] AS PARTITION PF_" + name + " TO ([PRIMARY],[PRIMARY])");
			logger.info("Creating partitioning scheme: " + sb.toString());
			stmt.executeUpdate(sb.toString());
		} catch (SQLException e)
		{
			logger.error(e.getMessage(), e);
			return false;
		} finally
		{
			closeStatement(stmt);
		}
		return true;
	}

	/**
	 * add a new partition and filegroup/file
	 * 
	 * @param r
	 *            Repository
	 * @param endingLoadID
	 *            last LoadId to be placed in this partition
	 * @param directory
	 *            directory where the new file is to be placed
	 * @return
	 */
	public static boolean addPartition(Repository r, Integer endingLoadID, String directory)
	{
		// create a new file group
		DatabaseConnection dc = r.getDefaultConnection();
		if (!r.getTimeDefinition().PartitionFacts)
			return false;
		if (!dc.supportsPartitioning())
			return false;
		String dbName = dc.decodeDatabase();
		String fileGroupName = dbName + "_" + endingLoadID;
		String fileName = dbName + "_" + endingLoadID;
		String fileLocation = directory + File.separatorChar + fileName + ".ndf";
		String partName = r.getTimeDefinition().getPartitionSchemeName(r);
		String newFileGroup = "ALTER DATABASE " + dbName + " ADD FILEGROUP " + fileGroupName;
		String newFile = "ALTER DATABASE " + dbName + " ADD FILE (NAME=" + fileName + " ,FILENAME='" + fileLocation + "') TO FILEGROUP " + fileGroupName;
		String alterScheme = "ALTER PARTITION SCHEME " + partName + " NEXT USED " + fileGroupName;
		String alterFunction = "ALTER PARTITION FUNCTION PF_" + partName + " () SPLIT RANGE (" + endingLoadID + ")";
		logger.info("Adding new partition for ending month " + endingLoadID + " in directory " + directory);
		Statement stmt = null;
		try
		{
			Connection conn = dc.ConnectionPool.getConnection();
			stmt = conn.createStatement();
			logger.debug(newFileGroup);
			stmt.executeUpdate(newFileGroup);
			logger.debug(newFile);
			stmt.executeUpdate(newFile);
			logger.debug(alterScheme);
			stmt.executeUpdate(alterScheme);
			logger.debug(alterFunction);
			stmt.executeUpdate(alterFunction);
		} catch (SQLException ex)
		{
			logger.error(ex.getMessage(), ex);
			logger.error("Error occurred that must be manually cleaned up.");
			return false;
		} finally
		{
			closeStatement(stmt);
		}
		logger.info("It is recommended that you mark the previous filegroup as READONLY (assuming that is was not [PRIMARY])");
		logger.info("- via the SQL Server Management Studio (Database Properties dialog, Filegroups panel) or via T-SQL (ALTER DATABASE " + dbName
				+ " MODIFY FILEGROUP <previousFileGroupName> READONLY)");
		return true;
	}

	/**
	 * construct a table name the includes the schema (if specified)
	 * 
	 * @param dc
	 *            database connection object (contains the schema information)
	 * @param tableName
	 *            name of the table
	 * @return
	 */
	public static String getQualifiedTableName(DatabaseConnection dc, String tableName)
	{
		return (dc.Schema == null ? tableName : dc.Schema + "." + tableName);
	}
	
	public static String getQualifiedIndexName(DatabaseConnection dc, String indexName)
	{
		return (dc.Schema == null ? indexName : dc.Schema + "." + indexName);
	}

	/**
	 * Get the row count of the given table.
	 * 
	 * @param dc
	 * @param name
	 * @throws SQLException
	 */
	public static long getRowCount(Repository r, DatabaseConnection dc, String tableName) throws SQLException
	{
		logger.info("Getting row count for: " + getQualifiedTableName(dc, tableName));
		// For Excel source type, tableExists() does not work. So, do not check it.
		if (dc.DBType != DatabaseType.ODBCExcel && !tableExists(r, dc, tableName, false))
			return -1;
		Connection conn = dc.ConnectionPool.getConnection();
		String qualifiedTableName = getQualifiedTableName(dc, tableName);
		if (DatabaseConnection.isDBTypeMSSQL(dc.DBType))
		{
			return getRowCountForSqlServer(conn, qualifiedTableName);
		}
		// for all other databases return regular row count
		return getRowCount(conn, qualifiedTableName);
	}

	/**
	 * @param conn
	 * @param qualifiedTableName
	 */
	private static long getRowCount(Connection conn, String qualifiedTableName)
	{
		String query = "select count(*) FROM " + qualifiedTableName;
		Statement stmt = null;
		ResultSet rs = null;
		try
		{
			stmt = conn.createStatement();
			long start = System.currentTimeMillis();
			logger.debug("Executing row count query:" + query);
			rs = stmt.executeQuery(query);
			while (rs.next())
			{
				long rowcount = rs.getLong(1);
				return rowcount;
			}
			if (logger.isDebugEnabled())
				logger.debug("Execution of the query:" + query + " took " + (System.currentTimeMillis() - start) + " ms.");
		} catch (SQLException e)
		{
			logger.warn("Exception while getting the row count:", e);
		} finally
		{
			try
			{
				if (rs != null)
					rs.close();
				if (stmt != null)
					stmt.close();
			} catch (SQLException e)
			{
				logger.debug(e, e);
			}
		}
		return -1;
	}

	public static long getRowCountForSqlServer(Connection conn, String qualifiedTableName)
	{
		String query = "EXEC SP_SPACEUSED " + qualifiedTableName;
		Statement stmt = null;
		try
		{
			stmt = conn.createStatement();
			long start = System.currentTimeMillis();
			logger.debug("Executing stored procedure:" + query);
			boolean hasResults = stmt.execute(query);
			while (hasResults)
			{
				ResultSet rs = stmt.getResultSet();
				int idx = rs.findColumn("rows");
				if (idx >= 0)
				{
					while (rs.next())
					{
						long rowcount = rs.getLong(idx);
						return rowcount;
					}
				}
			}
			if (logger.isDebugEnabled())
				logger.debug("Execution of the query:" + query + " took " + (System.currentTimeMillis() - start) + " ms.");
		} catch (SQLException e)
		{
			logger.warn("Exception while getting the row count:", e);
		} finally
		{
			closeStatement(stmt);
		}
		return -1;
	}
	
	/**
	 * @param pstmt
	 */
	private static void closePreparedStatement(PreparedStatement pstmt)
	{
		if (pstmt != null)
		{
			try
			{
				pstmt.close();
			} catch (SQLException e)
			{
				logger.warn("Exception during pstmt.close()", e);
			}
		}
	}

	/**
	 * @param stmt
	 */
	private static void closeStatement(Statement stmt)
	{
		if (stmt != null)
		{
			try
			{
				stmt.close();
			} catch (SQLException e)
			{
				logger.warn("Exception during stmt.close()", e);
			}
		}
	}

	public static List<DatabaseIndex> getIndicesFromMetadata(DatabaseConnection dbc, Statement stmt, String schema, String tname)
	{
	  List<DatabaseIndex> ilist = new ArrayList<DatabaseIndex>();
	  if (!dbc.supportsIndexes())
	    return ilist;
	  Connection dconn = null;
	  ResultSet rs = null;
	  try
	  {
	    dconn = dbc.ConnectionPool.getConnection();
	    DatabaseMetaData dbmd = dconn.getMetaData();

	    Map<String, DatabaseIndex> indexes = new HashMap<String, DatabaseIndex>();
	    rs = dbmd.getIndexInfo(null, dbc.Schema, tname.toUpperCase(), false, false);

	    while (rs.next()) 
	    {
	      if (rs.getString("INDEX_NAME")!=null && rs.getString("COLUMN_NAME")!=null)
	      {
	        String indexName = rs.getString("INDEX_NAME");
	        String typeDesc = rs.getString("TYPE");
	        String colName = rs.getString("COLUMN_NAME");
	        String tableName = rs.getString("TABLE_NAME");
	        String schemaName = rs.getString("TABLE_SCHEM");
	        DatabaseIndex di = null;
	        if (indexes.containsKey(indexName + dbc.getPhysicalSuffix() + tableName))
	          di = indexes.get(indexName + dbc.getPhysicalSuffix() + tableName);
	        else 
	        {
	          di = new DatabaseIndex();
	          di.name = indexName;
	          di.schemaName = schemaName;
	          di.tableName = tableName;
	          di.columns = new String[0];
	          di.includedColumns = new String[0];
	          di.clustered = typeDesc.equals("CLUSTERED");
	          indexes.put(indexName + dbc.getPhysicalSuffix() + tableName, di);
	        }
	        List<String> nlist = new ArrayList<String>();
	        for (String s : di.columns)
	          nlist.add(s);
	        nlist.add(colName);
	        di.columns = new String[nlist.size()];
	        nlist.toArray(di.columns);
	      }
	    }     
	    for (DatabaseIndex di : indexes.values())
	      ilist.add(di);
	  }
	  catch (Exception e)
	  {
	    logger.error(e, e);
	  }
	  finally
	  {
	    try
	    {
	      if (rs != null)
	        rs.close();
	    } catch (SQLException e)
	    {
	      logger.debug(e, e);
	    }
	  }
	  return ilist;
	}

	public static List<DatabaseIndex> getIndices(DatabaseConnection dbc, Statement stmt, String schema, String tname)
	{
		return getIndices(dbc, stmt, schema, tname, false,true);
	}
	
	public static List<DatabaseIndex> getIndices(DatabaseConnection dbc, Statement stmt, String schema, String tname,boolean ignoreSupportIndexes,boolean addTypeFlag)
	{
		List<DatabaseIndex> ilist = new ArrayList<DatabaseIndex>();
		if (!dbc.supportsIndexes() && !ignoreSupportIndexes)
			return ilist;
		ResultSet rs = null;
		try
		{
			if (schema == null)
				schema = "dbo";
			String query = "select a.name AS INDEX_NAME,a.type_desc AS TYPE,b.index_column_id AS ColumnNumber,b.is_included_column AS Included,"
					+ "c.name AS COLUMN_NAME,d.name AS TABLE_NAME,e.name AS SCHEMA_NAME from "
					+ "sys.indexes a inner join sys.index_columns b on a.index_id=b.index_id and a.object_id=b.object_id "
					+ "inner join sys.columns c on b.object_id=c.object_id and b.column_id=c.column_id "
					+ "inner join sys.tables d on a.object_id=d.object_id " + "inner join sys.schemas e on d.schema_id=e.schema_id " + "where e.name='"
					+ schema + "'";
			//Moving the table name filter to a query level
			if (tname!=null && !tname.isEmpty())
			{
			  query  = query + " and d.name='"+tname.toUpperCase()+"'";
			}
			if (addTypeFlag)
			{
				query	= query +	" and a.type=2";
			}
			query	= query +	" order by a.index_id,b.index_column_id";
			Map<String, DatabaseIndex> indexes = new HashMap<String, DatabaseIndex>();
			rs = stmt.executeQuery(query);
			while (rs.next())
			{
				String indexName = rs.getString(1);
				String typeDesc = rs.getString(2);
				// int colNum = rs.getInt(3);
				boolean included = rs.getBoolean(4);
				String colName = rs.getString(5);
				String tableName = rs.getString(6);
				String schemaName = rs.getString(7);
				DatabaseIndex di = null;
				if (indexes.containsKey(indexName + dbc.getPhysicalSuffix() + tableName))
					di = indexes.get(indexName + dbc.getPhysicalSuffix() + tableName);
				else
				{
					di = new DatabaseIndex();
					di.name = indexName;
					di.schemaName = schemaName;
					di.tableName = tableName;
					di.columns = new String[0];
					di.includedColumns = new String[0];
					di.clustered = typeDesc.equals("CLUSTERED");
					indexes.put(indexName + dbc.getPhysicalSuffix() + tableName, di);
				}
				if (included)
				{
					List<String> nlist = new ArrayList<String>();
					for (String s : di.includedColumns)
						nlist.add(s);
					nlist.add(colName);
					di.includedColumns = new String[nlist.size()];
					nlist.toArray(di.includedColumns);
				} else
				{
					List<String> nlist = new ArrayList<String>();
					for (String s : di.columns)
						nlist.add(s);
					nlist.add(colName);
					di.columns = new String[nlist.size()];
					nlist.toArray(di.columns);
				}
			}
			for (DatabaseIndex di : indexes.values())
				ilist.add(di);
		} catch (SQLException e)
		{
			logger.error(e, e);
		} finally
		{
			try
			{
				if (rs != null)
					rs.close();
			} catch (SQLException e)
			{
				logger.debug(e, e);
			}
		}
		return (ilist);		
	}

	public static void copyIndex(DatabaseConnection dbc, Statement stmt, DatabaseIndex di, String targetSchema) throws SQLException
	{
		createIndex(dbc, stmt, targetSchema, di.tableName, di.name, di.columns, di.includedColumns, di.clustered);
	}

	public static void createIndex(DatabaseConnection dbc, Statement stmt, DatabaseIndex di) throws SQLException
	{
		createIndex(dbc, stmt, di.schemaName, di.tableName, di.name, di.columns, di.includedColumns, di.clustered);
	}

	public static void createIndex(DatabaseConnection dbc, Statement stmt, String schema, String tableName, String indexName, String[] columns,
			String[] includedColumns, boolean clustered) throws SQLException
	{
		if (!dbc.supportsIndexes())
			return;
		StringBuilder query = new StringBuilder("CREATE ");
		query.append((clustered ? "CLUSTERED" : "NONCLUSTERED"));
		query.append(" INDEX ");
		query.append(indexName);
		query.append(" ON ");
		query.append(schema);
		query.append('.');
		query.append(tableName);
		query.append(" (");
		for (int i = 0; i < columns.length; i++)
		{
			if (i > 0)
				query.append(',');
			query.append('[');
			query.append(columns[i]);
			query.append("] ASC");
		}
		query.append(')');
		if (dbc.supportsIndexIncludes() && includedColumns != null && includedColumns.length > 0)
		{
			query.append(" INCLUDE (");
			for (int i = 0; i < includedColumns.length; i++)
			{
				if (i > 0)
					query.append(',');
				query.append('[');
				query.append(includedColumns[i]);
				query.append(']');
			}
			query.append(')');
		}
		if (dbc.useCompression() && dbc.supportsMSSQLDBCompression())
		{
			query.append(" WITH (DATA_COMPRESSION = PAGE)");
		}
		logger.debug(Query.getPrettyQuery(query.toString()));
		try
		{
		  stmt.executeUpdate(query.toString());
		}
		catch (Exception e)
		{
		  logger.error(e, e);
		}
	}

	public static int retryExecuteUpdate(DatabaseConnection dc, Statement stmt, String updateSQL) throws SQLException
	{
		if (!dc.canRetryOperation())
		{
			return stmt.executeUpdate(updateSQL);
		}
		int count = 0;
		while (true)
		{
			try
			{
				return stmt.executeUpdate(updateSQL);
			} catch (SQLException sex)
			{
				count++;
				logger.error(sex, sex);
				if (count > 2)
					throw new SQLException(sex);
				logger.info("Sleeping 30 seconds and retrying (" + count + ")");
				try
				{
					Thread.sleep(30 * 1000);
				} catch (InterruptedException e)
				{
				}
			}
		}
	}
	
	public static void performAnalyzeSchema(DatabaseConnection dc, String loadID) throws SQLException
	{
		List<String> tables = Database.getTables(dc, null, dc.Schema, null);
		Connection conn = dc.ConnectionPool.getConnection();
		Statement stmt = conn.createStatement();
		for (String tableName : tables)
		{
			logger.debug("ANALYZE " + Database.getQualifiedTableName(dc, tableName));
			stmt.executeUpdate("ANALYZE " + Database.getQualifiedTableName(dc, tableName));
		}		
	}
	
	public static void performVacuumSchema(DatabaseConnection dc, String loadID) throws SQLException
	{
		List<String> tables = Database.getTables(dc, null, dc.Schema, null);
		Connection conn = dc.ConnectionPool.getConnection();
		Statement stmt = conn.createStatement();
		for (String tableName : tables)
		{
			logger.debug("VACUUM " + Database.getQualifiedTableName(dc, tableName));
			stmt.executeUpdate("VACUUM " + Database.getQualifiedTableName(dc, tableName));
		}
	}
	
	public static void performUpdateStats(DatabaseConnection dc) throws SQLException
	{
		List<String> tables = Database.getTables(dc, null, dc.Schema, null);
		Connection conn = dc.ConnectionPool.getConnection();
		Statement stmt = conn.createStatement();
		for (String tableName : tables)
		{
			logger.debug("UPDATE STATISTICS " + Database.getQualifiedTableName(dc, tableName));
			stmt.executeUpdate("UPDATE STATISTICS " + Database.getQualifiedTableName(dc, tableName));
		}
	}
	
	/**
	 * choose appropriate compress types for the databases that support it - ParAccel and Redshift
	 * - not optimal, but a good set of choices, XXX should expose to the user in the manage sources panel
	 * @param dconn
	 * @param declaredType
	 * @param width
	 * @return
	 */
	public static String getColumnCompressionEncoding(DatabaseConnection dconn, String physicalName, String declaredType, int width)
	{
		if(!dconn.supportsColumnCompression())
		{
			return null;
		}
		String encoding = null;
		if (DatabaseConnection.isDBTypeParAccel(dconn.DBType))
		{
			String dType = declaredType.toLowerCase();
			switch(dType)
			{
				case "varchar":
				{
					if (dconn.DBType == DatabaseType.ParAccel)
						encoding = "DEFLATE";
					else if (dconn.DBType == DatabaseType.Redshift)
					{
						if (width <= 255)
							encoding = "TEXT255";
						else if (width <= 32677)
							encoding = "TEXT32K";
						else
							encoding = "LZO"; // http://docs.aws.amazon.com/redshift/latest/dg/lzo-encoding.html
					}
					break;
				}
				case "integer":
				{
					if (dconn.DBType == DatabaseType.Redshift && physicalName != null)
					{
						if (physicalName.equals("LOAD_ID"))
							encoding = "BYTEDICT";
						if (physicalName.startsWith("Time$") && physicalName.endsWith("_ID$"))
							encoding = "DELTA32K";
						else
							encoding = "MOSTLY16";
					}
					else
					{
						encoding = "MOSTLY16";
					}
					break;
				}
				case "number":
				{
					encoding = "MOSTLY32";
					break;
				}
				case "float":
				{
					encoding = "RUNLENGTH";
					break;
				}
				case "date":
				{
					encoding = "DELTA";
					break;
				}
				case "datetime":
				{
					encoding = "DELTA32K";
					break;
				}				
				default:
				{
					encoding = null;
					break;
				}
			}			
		}
		return encoding;
	}
	
	// random values, no use in encoding them
	public static String getChecksumColumnCompression()
	{
		return "RAW";
	}
	
	public static int persistQueryToFileAndLoadIntoTable(String query, String tableName, String columnList, String databasepath, 
			boolean includesIdentityColumn, DatabaseConnection dconn, Statement stmt, AWSCredentials s3credentials,String targetSchema,String sourceSchema,Repository r) throws Exception
	{
		int result =-1;
		String[] unloadSummary = unloadTableToFile(r,query, tableName, columnList, databasepath, dconn, stmt, s3credentials, sourceSchema,false,null,null);
		try{
			int numOfRowsAfterLoad = loadTableFromFile(r,query, tableName, columnList, databasepath, includesIdentityColumn, dconn, stmt, s3credentials, targetSchema, unloadSummary[1],false,null);
			result = numOfRowsAfterLoad - Integer.parseInt(unloadSummary[0]);
			logger.debug("Difference between unload and load : " + result);						
		}catch(ArrayIndexOutOfBoundsException ex){
			throw new Exception("Unable to determine last unload results " + ex);
		}catch(NumberFormatException ex){
			throw new Exception("Unable to parse Number of rows from the last unload " + ex);
		}finally{
			File file = new File(unloadSummary[1]);
			String SMI_HOME = System.getProperty("smi.home");
			String absFilePath = Util.replaceStr(SMI_HOME + "/data/" + file.getName(), "/", File.separator);
			file = new File(absFilePath);
			if (file.exists())
				file.delete();
		}
		return result;
	}
	
	public static boolean schemaExists(DatabaseConnection dc) throws Exception
    {
        boolean useInformationSchema = (dc.DBType == DatabaseType.Infobright || dc.DBType == DatabaseType.Redshift);
        boolean isMemDB = dc.DBType == DatabaseType.MemDB;        
        if (isMemDB)
            return true;
        String schemaExistsSQL = null;
        if(dc.DBType == DatabaseType.Hana)
        {
            schemaExistsSQL = "SELECT * FROM SYS.SCHEMAS WHERE SCHEMA_NAME='" + dc.Schema.toUpperCase() + "'";
        }
        else
        {
	        if (useInformationSchema)
	        {
	            schemaExistsSQL = "SELECT * FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME='" + dc.Schema + "'";
	        } else
	        {
	            schemaExistsSQL = "SELECT * FROM SYS.SCHEMAS WHERE NAME='" + dc.Schema + "'";
	        }
        }
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;        
        try        
        {
        	conn = dc.ConnectionPool.getConnection();
        	stmt = conn.createStatement();
        	logger.debug(Query.getPrettyQuery(schemaExistsSQL));
        	rs = stmt.executeQuery(schemaExistsSQL);
        	if (rs.next())
        	{
        		return true;
        	}
        }
        finally
        {
        	if (rs != null)
        		rs.close();
        	if (stmt != null)
        		stmt.close();        	
        }
        return false;
    }
	
	public static void createSchema(DatabaseConnection dc) throws Exception
	{
		boolean isIB = dc.DBType == DatabaseType.Infobright;
		boolean isRedshift = dc.DBType == DatabaseType.Redshift;
		boolean isSAPHana = dc.DBType == DatabaseType.Hana;
        String createSchemaSQL = null;
        if (isIB)
            createSchemaSQL = "CREATE DATABASE IF NOT EXISTS " + dc.Schema;
        else if (isRedshift)
        	createSchemaSQL = "CREATE SCHEMA [" + dc.Schema + "]";
        else if (isSAPHana)
        	createSchemaSQL = "CREATE SCHEMA " + dc.Schema;
        else
            createSchemaSQL = "CREATE SCHEMA [" + dc.Schema + "] AUTHORIZATION [dbo]";
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;        
        try        
        {
        	conn = dc.ConnectionPool.getConnection();
        	stmt = conn.createStatement();
        	logger.debug(Query.getPrettyQuery(createSchemaSQL));
        	stmt.executeUpdate(createSchemaSQL);        	
        }
        finally
        {
        	if (rs != null)
        		rs.close();
        	if (stmt != null)
        		stmt.close();        	
        }        
	}
	
	public static Map<String,List<String>> getDistinctTableCollationForIB(DatabaseConnection dc)
	{
		if(dc.DBType != DatabaseType.Infobright)
		{
			return null;
		}
		Map<String,List<String>> collations = new HashMap<String,List<String>>();
		String getDistinctCollationSQL = "SELECT DISTINCT TABLE_COLLATION, TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='" + dc.Schema + "' " +
				"AND TABLE_NAME NOT LIKE 'temp%' AND TABLE_NAME NOT LIKE 'TXN_%' AND TABLE_NAME NOT LIKE 'birstx_temp%'";
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;        
        try        
        {
        	conn = dc.ConnectionPool.getConnection();
        	stmt = conn.createStatement();
        	logger.debug(Query.getPrettyQuery(getDistinctCollationSQL));
        	rs = stmt.executeQuery(getDistinctCollationSQL);
        	while(rs.next())
        	{
        		String currentCollation = rs.getString(1);
        		String tableName = rs.getString(2); 
        		if(collations.containsKey(currentCollation))
        		{
        			List<String> currentTables = collations.get(currentCollation);
        			currentTables.add(tableName);
        			collations.put(currentCollation, currentTables);
        		}
        		else
        		{
        			List<String> currentTables = new ArrayList<String>();
        			currentTables.add(tableName);
        			collations.put(currentCollation,currentTables);
        		}
        	}
        }
        catch(SQLException sqle)
        {
        	logger.error(sqle.toString(), sqle);
        	return null;
        }
        finally
        {
        	if (rs != null)
        	{
	        	try
	        	{
	        		rs.close();
	        	}
	        	catch(SQLException sqle){}
        	}
        	if (stmt != null)
        	{
        		try
        		{
        			stmt.close();
        		}
        		catch(SQLException sqle){}
        	}        
        }
		return collations;
	}
	
	public static boolean isUtf8CollationPossible(DatabaseConnection dconn)
	{
		boolean isUtf8CollationPossible = false;
		if((!dconn.isRealTimeConnection()) && DatabaseConnection.isDBTypeInfoBright(dconn.DBType))
		{
			Map<String,List<String>> collations = getDistinctTableCollationForIB(dconn);
			if(collations != null)
			{
				if(collations.isEmpty())
				{
					//No tables created yet so we can safely use UTF8 for loading IB warehouse
					isUtf8CollationPossible = true;
				}
				else if(collations.size() == 1)
				{
					Iterator<Entry<String, List<String>>> it = collations.entrySet().iterator();
					while(it.hasNext())
					{
						Map.Entry<String,List<String>> pairs = (Entry<String, List<String>>)it.next();
						if(pairs.getKey() != null && ((String) pairs.getKey()).toLowerCase().startsWith("utf8"))
						{
							isUtf8CollationPossible = true;
						}
					}
				}
				else
				{
					logger.warn("Multiple collation values found in database - this might cause issues while querying.");
					Iterator<Entry<String, List<String>>> it = collations.entrySet().iterator();
					while(it.hasNext())
					{
						Map.Entry<String,List<String>> pairs = (Entry<String, List<String>>)it.next();
						if(pairs.getKey() != null && !(((String) pairs.getKey()).toLowerCase().startsWith("utf8")))
						{
							List<String> tablesForCollation = (List<String>) pairs.getValue();
							logger.warn("Collation: " + pairs.getKey() + ( tablesForCollation.size() >1 ? " Tables: " : " Table:") +  tablesForCollation );
						}
					}
					isUtf8CollationPossible = false;
				}
				collations = null; //mark it for gc
			}
		}
		logger.debug("IBLoadWarehouseUsingUTF8= " + isUtf8CollationPossible);
		return isUtf8CollationPossible;
	}

	/**
	 * Given two tables of the exact same table structure, this method synthesizes an insert statement from a table into another table.
	 * In order to do so, it uses all columns provided in the schema.
	 * @param schema
	 * @param intoTable
	 * @param fromTable
	 * @return insert sql statement
	 */
	public static String getInsertStatement(List<Object[]> schema, String intoTable, String fromTable)
	{
		StringBuilder collistsb = new StringBuilder();
		//Loop thru columns on schema.
		boolean first = true;
		for (int i = 0; i < schema.size(); i++)
		{
			String colName = ((String) schema.get(i)[0]);
			if(first)
			{
				first = false;
			}
			else
			{
				collistsb.append(", ");
			}
			collistsb.append(colName);
		}
		
		StringBuilder insertsb = new StringBuilder();
		insertsb.append("INSERT INTO ");
		insertsb.append(intoTable);
		insertsb.append(" ( ");
		insertsb.append(collistsb);
		insertsb.append(" ) ");
		
		//Select part
		insertsb.append(" SELECT ");
		insertsb.append(collistsb);
		insertsb.append(" FROM ");
		insertsb.append(fromTable);
		return insertsb.toString();
	}

	public static int getMaxIndexNameLength(DatabaseType DBType)
	{
		return DBType == DatabaseType.MYSQL ? Database.MAX_MYSQL_INDEX_NAME_LENGTH
				: (DBType == DatabaseType.Oracle ? MAX_ORACLE_INDEX_NAME_LENGTH : MAX_SQL_SERVER_INDEX_NAME_LENGTH);
	}
	
	public static String getDatabaseIndicesInfo(Repository r) throws Exception
	{
	  String databaseIndices = null;
	  Statement stmt = null;
	  try
	  {
	    DatabaseConnection dbc = r.getDefaultConnection();
	    if (DatabaseConnection.isDBTypeMSSQL(dbc.DBType) || DatabaseConnection.isDBTypeOracle(dbc.DBType))
	    {
	      StringBuilder sbIndices = new StringBuilder();
	      stmt = dbc.ConnectionPool.getConnection().createStatement();
	      List<String> tableList = getTables(dbc, null, dbc.Schema, null);
	      sbIndices.append("Table Name").append("\t");
	      sbIndices.append("Index Name").append("\t");
	      sbIndices.append("Column Name").append("\t");
	      sbIndices.append("Included");	      
	      sbIndices.append(System.lineSeparator());
	      for (String tname : tableList)
	      {
	        List<DatabaseIndex> dIndices = null;
	        if (DatabaseConnection.isDBTypeOracle(dbc.DBType))
	        {
	          dIndices = getIndicesFromMetadata(dbc, stmt, dbc.Schema, tname);
	        }
	        else if (DatabaseConnection.isDBTypeMSSQL(dbc.DBType))
	        {
	          dIndices = getIndices(dbc, stmt, dbc.Schema, tname,true,false);
	        }
	        if (dIndices != null)
	        {
		        for (DatabaseIndex dbIndex : dIndices)
		        {
		          if (dbIndex.columns!=null)
		          {
		            for (String column : dbIndex.columns)
		            {
		              sbIndices.append(tname.toUpperCase()).append("\t");
		              sbIndices.append(dbIndex.name).append("\t");
		              sbIndices.append(column).append("\t");
		              sbIndices.append("N").append("\t");
		              sbIndices.append(System.lineSeparator());
		            }	            
		          }
		          if (dbIndex.includedColumns!=null)
		          {
		            for (String includedCol : dbIndex.includedColumns)
		            {
		              sbIndices.append(tname.toUpperCase()).append("\t");
		              sbIndices.append(dbIndex.name).append("\t");
		              sbIndices.append(includedCol).append("\t");
		              sbIndices.append("Y").append("\t");
		              sbIndices.append(System.lineSeparator());
		            }             
		          }
		        }	     
	        }
	      }
	      databaseIndices = sbIndices.toString();
	    }
	  }
	  finally
	  {
	    if (stmt!=null)
	      stmt.close();
	  }
	  return databaseIndices;
	}
	
	public static String getQuotedString(String s)
	{
		if (s == null || s.equals("NULL"))
			return ("NULL");
		else
			return ("'" + s + "'");
	}

	public static String getDataType(DatabaseConnection dbc, String declaredType)
	{
		if (declaredType.equals("Varchar"))
		{
			return ("VARCHAR");
		} else if (declaredType.equals("Integer"))
		{
			return dbc.getCastableIntegerType();
		} else if (declaredType.equals("Number"))
		{
			return dbc.getNumericType();
		} else if (declaredType.equals("DateTime"))
		{
			return (dbc.getDateTimeType());
		} else if (declaredType.equals("Date"))
		{
			return (dbc.getDateType());
		} else if (declaredType.equals("Float"))
		{
			return ("FLOAT");
		}
		return (null);
	}
	
	public static boolean populateLiveAccessTmpTable(QueryResultSet qrs, String tempTablePhyName, DatabaseConnection connection,List<String> columnNames) throws Exception
	{
		boolean dataAdded = true;
		Connection dbConnection = connection.ConnectionPool.getConnection();
		PreparedStatement prepStmt = null;
		boolean autoCommit = dbConnection.getAutoCommit();
		try
		{
			int columnCount = columnNames.size();			
			int rowCount = qrs.rows.length;			
			dbConnection.setAutoCommit(false);			
			if (rowCount>0)
			{
				for (int rowIndex=0;rowIndex<rowCount;rowIndex++)
				{
					if (prepStmt==null)
					{
						String tblQualifiedName = connection.Schema==null? tempTablePhyName : connection.Schema+"."+tempTablePhyName;
						StringBuilder insertQry = new StringBuilder("INSERT INTO "+tblQualifiedName+"(");
						for (int index=0;index<columnCount;index++)
						{
							if ((index+1)==columnCount)
							{
								insertQry.append(columnNames.get(index));
							}
							else
							{
								insertQry.append(columnNames.get(index)+",");
							}
						}
						insertQry.append(" ) VALUES (");
						for (int index=1;index<=columnCount;index++)
						{
							if (index==columnCount)
							{
								insertQry.append("?");
							}
							else
							{
								insertQry.append("?,");
							}
						}
						insertQry.append(")");
						prepStmt = dbConnection.prepareStatement(insertQry.toString());	
					}
					for (int index=1;index<=columnCount;index++)
					{
						Object objData = qrs.rows[rowIndex][index-1];
						if (objData instanceof GregorianCalendar)
						{
							GregorianCalendar calObj = (GregorianCalendar) objData;
							prepStmt.setObject(index, new java.sql.Date(calObj.getTimeInMillis()));
						}
						else
						{
							prepStmt.setObject(index,objData);	
						}
					}
					prepStmt.addBatch();
				}
				prepStmt.executeBatch();
			}
			if (dbConnection!=null)
			{
				dbConnection.commit();
			}
		}
		catch(Exception e)
		{
			logger.error(e.getMessage());
			dataAdded = false;
			if (dbConnection!=null)
			{
				dbConnection.rollback();
			}
		}
		finally
		{
			dbConnection.setAutoCommit(autoCommit);
			if (prepStmt!=null)
			{
				prepStmt.close();				
			}
			if (dbConnection!=null)
			{
				dbConnection.close();
			}
		}
		return dataAdded;
	}
}
