/**
 * $Id: DimensionColumnNav.java,v 1.25 2012/01/19 02:16:11 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.mongodb.BasicDBObject;

/**
 * Class used to process dimension column navigation. Create the structure with a given dimension name and column name
 * and the navigation process picks the appropriate logical tabale and column
 * 
 * @author bpeters
 * 
 */
public class DimensionColumnNav implements Externalizable, Cloneable, BasicDBObjectSerializer
{
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(DimensionColumnNav.class);
	String dimensionName;
	String columnName;
	String displayName;
	List<DimensionColumn> matchingColumns;
	Level level;
	int pick = 0;
	int cardinality = 1;
	boolean navigateonly = false;
	boolean constant = false;
	boolean levelKey = false;
	boolean volatileColumn = false;
	String constantString = null;
	int colIndex;
	String defaultMappedValue;
	boolean sparse;

	/**
	 * Constructor used for serialization
	 */
	public DimensionColumnNav()
	{
	}

	public Object clone() throws CloneNotSupportedException
	{
		DimensionColumnNav dcn = (DimensionColumnNav) super.clone();
		dcn.dimensionName = dimensionName;
		dcn.columnName = columnName;
		dcn.displayName = displayName;
		dcn.navigateonly = navigateonly;
		dcn.constant = constant;
		dcn.constantString = constantString;
		dcn.colIndex = colIndex;
		dcn.matchingColumns = matchingColumns == null ? null : new ArrayList<DimensionColumn>(matchingColumns);
		dcn.level = level;
		dcn.pick = pick;
		dcn.cardinality = cardinality;
		dcn.levelKey = levelKey;
		dcn.volatileColumn = volatileColumn;
		dcn.sparse = sparse;
		return (dcn);
	}

	DimensionColumnNav(String dname, String cname, String dispname, int colIndex)
	{
		this.columnName = cname;
		this.dimensionName = dname;
		this.displayName = dispname;
		this.matchingColumns = new ArrayList<DimensionColumn>();
		this.colIndex = colIndex;
	}

	DimensionColumnNav(String dname, String cname, int colIndex)
	{
		this.columnName = cname;
		this.dimensionName = dname;
		this.displayName = cname;
		this.matchingColumns = new ArrayList<DimensionColumn>();
		this.colIndex = colIndex;
	}

	public void writeExternal(ObjectOutput oo) throws IOException
	{
		oo.writeObject(dimensionName);
		oo.writeObject(columnName);
		oo.writeObject(displayName);
		oo.writeObject(colIndex);
		oo.writeObject(volatileColumn);
	}
	
	public BasicDBObject toBasicDBObject()
	{
		BasicDBObject o = new BasicDBObject();
		
		o.put("dimensionName", dimensionName);
		o.put("columnName", columnName);
		o.put("displayName", displayName);
		o.put("colIndex", colIndex);
		o.put("volatileColumn", volatileColumn);
		
		return o;
	}

	public void fromBasicDBObject(BasicDBObject obj)
	{
		dimensionName = obj.getString("dimensionName");
		columnName = obj.getString("columnName");
		displayName = obj.getString("displayName");
		colIndex = obj.getInt("colIndex");
		volatileColumn = obj.getBoolean("volatileColumn", volatileColumn);
		matchingColumns = new ArrayList<DimensionColumn>();
	}
	
	public void readExternal(ObjectInput oi) throws IOException, ClassNotFoundException
	{
		dimensionName = (String) oi.readObject();
		columnName = (String) oi.readObject();
		displayName = (String) oi.readObject();
		colIndex = (Integer) oi.readObject();
		volatileColumn = (Boolean) oi.readObject();
		matchingColumns = new ArrayList<DimensionColumn>();
	}
	
	/**
	 * Returns the key string for this dimension column
	 * 
	 * @return
	 */
	public String getKey()
	{
		return ((sparse ? 'S' : "") + "D{" + dimensionName + "}C{" + columnName + "}");
	}

	/**
	 * Returns whether the underlying navigated dimension column is cacheable (assumes that this is done after
	 * navigation)
	 * 
	 * @return
	 */
	public boolean isCacheable()
	{
		if (constant)
			return true;
		if (matchingColumns == null || matchingColumns.isEmpty())
			return (false);
		return (getDimensionColumn().DimensionTable.Cacheable && !getDimensionColumn().NotCacheable);
	}
	
	/*
	 * determine the expiration date of the column in a cache
	 */
	public long getExpires()
	{
		long ttl = Long.MAX_VALUE;
		if (constant)
		{
			return ttl; // constants never expire
		}
		DimensionColumn dc = getDimensionColumn();
		if (!isCacheable()) // not cacheable, expiration is immediate
		{
			logger.debug("dimension column not cachable, expires immediately - " + dc.TableName + "/" + dc.ColumnName);
			return 0;
		}
		if (dc.DimensionTable.TTL != -1)
		{
			if (dc.DimensionTable.TTL == 0)
				logger.debug("Zero TTL for " + dc.DimensionTable.PhysicalName + " / " + dc.PhysicalName);
			ttl = (long) dc.DimensionTable.TTL + System.currentTimeMillis();
		}
		return Math.min(ttl, QueryResultSet.getExpirationFromBoundary(dc.CacheBoundary));
	}

	public String toString()
	{
		return (this.dimensionName + "." + this.columnName);
	}

	public String getDisplayName()
	{
		return displayName;
	}

	public void setDisplayName(String displayName)
	{
		this.displayName = displayName;
	}

	public String getColumnName()
	{
		return columnName;
	}

	public void setColumnName(String columnName)
	{
		this.columnName = columnName;
	}

	public int getColIndex() {
		return colIndex;
	}

	public void setColIndex(int colIndex) {
		this.colIndex = colIndex;
	}
	
	public DimensionColumn getDimensionColumn()
	{
		return matchingColumns.get(pick);
	}

	public String getDimensionName() {
		return dimensionName;
	}
}
