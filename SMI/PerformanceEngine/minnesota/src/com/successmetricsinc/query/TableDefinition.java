/**
 * $Id: TableDefinition.java,v 1.5 2012-05-21 02:07:11 bpeters Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.io.Serializable;

public class TableDefinition implements Cloneable, Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;
	public String PhysicalName;
	public String Schema;
	public JoinType JoinType;
	public String JoinClause;
	public boolean Valid = true;
	public String OpaqueView;
	public transient String OverrideDimension;
	public String TableName;
	public boolean isJoinClauseTag;
	public String strJoinClause;

	public Object clone() throws CloneNotSupportedException
	{
		TableDefinition td = (TableDefinition) super.clone();
		td.PhysicalName = this.PhysicalName;
		td.JoinType = this.JoinType;
		td.JoinClause = this.JoinClause;
		td.Valid = this.Valid;
		td.OverrideDimension = OverrideDimension;
		td.TableName = TableName;
		td.Schema = Schema;
		return (td);
	}

	public String toString()
	{
		return (PhysicalName);
	}
}