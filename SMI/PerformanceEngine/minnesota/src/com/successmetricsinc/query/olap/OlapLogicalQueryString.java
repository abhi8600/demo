package com.successmetricsinc.query.olap;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.Tree;
import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.query.BadColumnNameException;
import com.successmetricsinc.query.DisplayExpression;
import com.successmetricsinc.query.GroupBy;
import com.successmetricsinc.query.Hierarchy;
import com.successmetricsinc.query.LogicalQueryString;
import com.successmetricsinc.query.NavigationException;
import com.successmetricsinc.query.Query;
import com.successmetricsinc.query.SyntaxErrorException;
import com.successmetricsinc.transformation.BirstScriptLexer;
import com.successmetricsinc.transformation.BirstScriptParser;
import com.successmetricsinc.transformation.Expression;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.transformation.BirstScriptParser.olaplogicalquery_return;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.Statement;
import com.successmetricsinc.util.BaseException;

public class OlapLogicalQueryString extends LogicalQueryString {

	private static final Logger logger = Logger.getLogger(OlapLogicalQueryString.class);
	
	public OlapLogicalQueryString(Repository r, Session session, String logicalQuery)
	{
		super(r, session, logicalQuery);		
	}
	
	public OlapLogicalQueryString(Repository r, Session session, Tree queryTree)
	{
		super(r, session, queryTree);
	}
	
	public enum MEMBEREXPRESSIONTYPE {
		SELECTED, CHILDREN, ALLCHILDREN, LEAVES, PARENTS
	}
	
	@Override
	public Query getQuery() throws BaseException
	{
		if (queryTree != null)
		{
			return processQuery(queryTree, false);
		}
		logger.info("Logical Query: " + logicalQuery);
		ANTLRStringStream stream = new ANTLRStringStream(logicalQuery);
		BirstScriptLexer lex = new BirstScriptLexer(stream);
		CommonTokenStream cts = new CommonTokenStream(lex);
		BirstScriptParser parser = new BirstScriptParser(cts);
		try
		{
			olaplogicalquery_return ret = parser.olaplogicalquery();
			if (lex.hasError())
				processLexError(lex, logicalQuery);
			return processQuery((Tree) ret.getTree(), false);
		} catch (RecognitionException e)
		{
			processException(e, lex, logicalQuery);
		}
		return null;
	}
	
	@Override
	protected Query processQuery(Tree tree, boolean makeNamesPhysical) throws NavigationException, BadColumnNameException, SyntaxErrorException, ScriptException
	{
		Query q = new Query(r);
		for (int i = 0; i < tree.getChildCount(); i++)
		{
			Tree ctree = tree.getChild(i);
			switch (ctree.getType())
			{
				case BirstScriptParser.PROJECTIONLISTS:
				{
					for (int j = 0; j < ctree.getChildCount(); j++)
					{
						Tree pList = ctree.getChild(j);
						if (pList.getType() == BirstScriptParser.PROJECTIONLIST)
						{
							super.processQuery(q, ctree, makeNamesPhysical);
						}
						else if (pList.getType() ==BirstScriptParser.OLAPPROJECTIONLIST)
						{
							for (int k = 0; k < pList.getChildCount(); k++)
							{
								Tree olapExpression = pList.getChild(k);
								int type = olapExpression.getType();
								String display = ignoreDisplay ? null : super.getAlias(olapExpression);
								if (type == BirstScriptParser.LOGICALMEMBERSET)
								{
									if (display != null && !display.isEmpty() && (display.endsWith("_PARENT_UNIQUE_NAME") || display.endsWith("_PARENT_NAME") || display.endsWith("_ORDINAL") || display.endsWith("_UNIQUE_NAME")))
									{
										String alias = null;
										boolean isParent = false, isParentName = false, isOrdinal = false, isUniqueName = false;
										if (display.endsWith("_PARENT_UNIQUE_NAME"))
										{
											alias = display.substring(0, display.indexOf("_PARENT_UNIQUE_NAME"));
											isParent = true;
										}
										else if (display.endsWith("_PARENT_NAME"))
										{
											alias = display.substring(0, display.indexOf("_PARENT_NAME"));
											isParentName = true;
										}
										else if (display.endsWith("_ORDINAL"))
										{
											alias = display.substring(0, display.indexOf("_ORDINAL"));
											isOrdinal = true;										
										}
										else if (display.endsWith("_UNIQUE_NAME"))
										{
											alias = display.substring(0, display.indexOf("_UNIQUE_NAME"));
											isUniqueName = true;
										}
										for (DimensionMemberSetNav dms : q.dimensionMemberSets)
										{
											if (dms.displayName != null && dms.displayName.equals(alias))
											{
												if (isParent)
												{
													addDimensionMemberSetParent(q, dms, display);
													curColumn++;
												}
												else if (isParentName)
												{
													addDimensionMemberSetParentName(q, dms, display);
													curColumn++;
												}
												else if (isOrdinal)
												{
													addDimensionMemberSetOrdinal(q, dms, display);
													curColumn++;
												}
												else if (isUniqueName)
												{
													addDimensionMemberSetUniqueName(q, dms, display);
													curColumn++;
												}
												break;
											}
										}
									}
									else if (type == BirstScriptParser.LOGICALMEMBERSET)
									{
										DimensionMemberSetNav dms = addDimensionMemberSet(q, olapExpression, display);
										GroupBy gb = new GroupBy(dms.dimensionName, dms.getMemberExpressionsString(), GroupBy.TYPE_DIMENSION_MEMBER_SET);
										q.addGroupBy(gb);
										String dimHierarchy = dms.dimensionName;
										Hierarchy h = r.findHierarchy(dimHierarchy);
										curColumn++;
										if (h == null)
										{
											throw new BadColumnNameException(Statement.toString(olapExpression, null));
										}
									}
								}
								else if (type == BirstScriptParser.MDXEXPRESSION)
								{
									if (display == null)
										display = "M" + new Random().nextInt();
									addOlapMemberExpression(q, olapExpression.getChild(0).getText(), display);
									curColumn++;
								}
							}
						}
					}
					break;
				}				
			}
		}
		super.processQuery(q, tree, makeNamesPhysical);
		return q;
	}
	
	public static DimensionMemberSetNav addDimensionMemberSet(Query q, Tree dimensionMemberSet, String displayName) throws NavigationException
	{
		String dname = getDimensionName(dimensionMemberSet);
		List<MemberSelectionExpression> memberExpressions = getMemberExpressions(dimensionMemberSet);
		return q.addDimensionMemberSet(dname, memberExpressions, displayName, null, null);	
	}
	
	public static OlapMemberExpression addOlapMemberExpression(Query q, String olapExpression, String displayName) throws NavigationException
	{
		return q.addOlapMemberExpression(olapExpression, displayName);	
	}
	
	public static void addDimensionMemberSetParent(Query q, DimensionMemberSetNav dms,String displayName) throws NavigationException
	{
		q.addDimMemberSetParent(dms, displayName);
	}
	
	public static void addDimensionMemberSetParentName(Query q, DimensionMemberSetNav dms,String displayName) throws NavigationException
	{
		q.addDimMemberSetParentName(dms, displayName);
	}

	public static void addDimensionMemberSetOrdinal(Query q, DimensionMemberSetNav dms,String displayName) throws NavigationException
	{
		q.addDimMemberSetOrdinal(dms, displayName);
	}
	
	public static void addDimensionMemberSetUniqueName(Query q, DimensionMemberSetNav dms,String displayName) throws NavigationException
	{
		q.addDimMemberSetUniqueName(dms, displayName);
	}
	
	@Override
	protected void processQuery(Query q, Tree tree, boolean makeNamesPhysical) throws NavigationException, BadColumnNameException, SyntaxErrorException, ScriptException
	{
		super.processQuery(q, tree, makeNamesPhysical);
	}
	
	public static String getDimensionName(Tree memberSet)
	{
		String memberSetPrefix = memberSet.getChild(0).getText();
		return memberSetPrefix.substring(1, memberSetPrefix.indexOf("."));
	}
	
	private static List<MemberSelectionExpression> getMemberExpressions(Tree memberSet)
	{
		List<MemberSelectionExpression> memberExpressions = new ArrayList<MemberSelectionExpression>();
		for (int i = 0; i < memberSet.getChildCount(); i++)
		{
			Tree child = memberSet.getChild(i);
			if (child.getType() == BirstScriptParser.MEMBEREXPRESSIONS)
			{
				for (int j = 0; j < child.getChildCount(); j++)
				{
					String exp = child.getChild(j).getChild(0).getText();
					String memberName = exp.substring(1, exp.length() - 1);
					switch (child.getChild(j).getType())
					{
						case  BirstScriptParser.SELECTED:
						{
							memberExpressions.add(new MemberSelectionExpression(MEMBEREXPRESSIONTYPE.SELECTED, memberName));
							break;
						}
						case  BirstScriptParser.CHILDREN:
						{
							memberExpressions.add(new MemberSelectionExpression(MEMBEREXPRESSIONTYPE.CHILDREN, memberName));
							break;
						}
						case  BirstScriptParser.ALLCHILDREN:
						{
							memberExpressions.add(new MemberSelectionExpression(MEMBEREXPRESSIONTYPE.ALLCHILDREN, memberName));
							break;
						}
						case  BirstScriptParser.LEAVES:
						{
							memberExpressions.add(new MemberSelectionExpression(MEMBEREXPRESSIONTYPE.LEAVES, memberName));
							break;
						}
						case  BirstScriptParser.PARENTS:
						{
							memberExpressions.add(new MemberSelectionExpression(MEMBEREXPRESSIONTYPE.PARENTS, memberName));
							break;
						}
					}
				}
			}
		}
		return memberExpressions;
	}
}