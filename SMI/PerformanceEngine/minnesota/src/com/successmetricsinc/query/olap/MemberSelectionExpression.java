package com.successmetricsinc.query.olap;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import com.mongodb.BasicDBObject;
import com.successmetricsinc.query.BasicDBObjectSerializer;
import com.successmetricsinc.query.olap.OlapLogicalQueryString.MEMBEREXPRESSIONTYPE;

public class MemberSelectionExpression implements Externalizable, BasicDBObjectSerializer, Comparable
{
	public MEMBEREXPRESSIONTYPE selectionType;
	public String memberName;
	
	public MemberSelectionExpression()
	{		
	}
	
	public MemberSelectionExpression(MEMBEREXPRESSIONTYPE selectionType, String memberName)
	{
		this.selectionType = selectionType;
		this.memberName = memberName;
	}
	
	@Override
	public void fromBasicDBObject(BasicDBObject obj) {
		selectionType = MEMBEREXPRESSIONTYPE.valueOf((String)obj.get("selectionType"));
		memberName = obj.getString("memberName");
	}

	@Override
	public BasicDBObject toBasicDBObject() {
		BasicDBObject o = new BasicDBObject();
		o.put("selectionType", selectionType.toString());
		o.put("memberName", memberName);
		return o;
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeObject(selectionType);
		out.writeObject(memberName);
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException,
			ClassNotFoundException {
		selectionType = (MEMBEREXPRESSIONTYPE)in.readObject();
		memberName = (String)in.readObject();
	}

	@Override
	public int compareTo(Object o) {
		MemberSelectionExpression other = (MemberSelectionExpression)o;
		if (this.selectionType.equals(other.selectionType))
			return this.memberName.compareTo(other.memberName);
		return this.selectionType.compareTo(other.selectionType);
	}
	
	@Override
	public String toString() {
		return selectionType.toString() + "(" + memberName + ")";
	}
}