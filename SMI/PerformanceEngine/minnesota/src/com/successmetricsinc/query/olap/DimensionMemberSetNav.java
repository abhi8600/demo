package com.successmetricsinc.query.olap;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.OptionalDataException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.successmetricsinc.Repository;
import com.successmetricsinc.query.BasicDBObjectSerializer;
import com.successmetricsinc.query.Database.DatabaseType;
import com.successmetricsinc.query.DimensionTable;
import com.successmetricsinc.query.Hierarchy;
import com.successmetricsinc.query.Level;
import com.successmetricsinc.query.MongoMgr;

public class DimensionMemberSetNav implements Externalizable, Cloneable, BasicDBObjectSerializer
{
	private static final Logger logger = Logger.getLogger(DimensionMemberSetNav.class);
	private static final long serialVersionUID = 2L;
	public String dimensionName;
	public List<MemberSelectionExpression> memberExpressions;
	public String displayName;
	public Level level;	
	public boolean navigateonly = false;
	public int colIndex;
	public String dataType;
	public String format;
	public int parentColIndex;
	public String parentDisplayName;
	public int parentNameColIndex;
	public String parentNameDisplayName;
	public int ordinalColIndex;
	public String ordinalDisplayName;
	public int uniqueNameColIndex;
	public String uniqueNameDisplayName;
	public String sortDirection = "UNSORTED";
	
	/**
	 * Constructor used for serialization
	 */
	public DimensionMemberSetNav()
	{
	}
	
	public DimensionMemberSetNav(String dname, List<MemberSelectionExpression> memberExpressions, String displayName, int colIndex, String dataType, String format)
	{
		this.dimensionName = dname;
		this.memberExpressions = memberExpressions;
		this.displayName = displayName;
		this.colIndex = colIndex;
		this.dataType = dataType;
		this.format = format;
		if (this.displayName == null)
		{
			String dimHierarchy = dimensionName;
			this.displayName = dimHierarchy.substring(dimHierarchy.indexOf("-") + 1);
		}		
	}
	
	public void addParentColumn(int colIndex, String displayName)
	{
		this.parentColIndex = colIndex;
		this.parentDisplayName = displayName;
	}
	
	public void addParentNameColumn(int colIndex, String displayName)
	{
		this.parentNameColIndex = colIndex;
		this.parentNameDisplayName = displayName;
	}
	
	public void addOrdinalColumn(int colIndex, String displayName)
	{
		this.ordinalColIndex = colIndex;
		this.ordinalDisplayName = displayName;
	}
	
	public void addUniqueNameColumn(int colIndex, String displayName)
	{
		this.uniqueNameColIndex = colIndex;
		this.uniqueNameDisplayName = displayName;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if (o == null)
			return false;
		if (!(o instanceof DimensionMemberSetNav))
			return false;
		DimensionMemberSetNav dms = (DimensionMemberSetNav)o;
		if (!this.dimensionName.equals(dms.dimensionName))
			return false;
		if (!this.memberExpressions.equals(dms.memberExpressions))
			return false;
		if (this.displayName != null && dms.displayName == null)
			return false;
		if (this.displayName == null && dms.displayName != null)
			return false;
		if (this.displayName != null && dms.displayName != null && !this.displayName.equals(dms.displayName))
			return false;
		if (this.dataType != null && dms.dataType == null)
			return false;
		if (this.dataType == null && dms.dataType != null)
			return false;
		if (this.dataType != null && dms.dataType != null && !this.dataType.equals(dms.dataType))
			return false;
		if (this.format != null && dms.format == null)
			return false;
		if (this.format == null && dms.format != null)
			return false;
		if (this.format != null && dms.format != null && !this.format.equals(dms.format))
			return false;
		if (this.colIndex != dms.colIndex)
			return false;
		if (this.parentColIndex != dms.parentColIndex)
			return false;
		if (this.parentDisplayName != null && dms.parentDisplayName == null)
			return false;
		if (this.parentDisplayName == null && dms.parentDisplayName != null)
			return false;
		if (this.parentDisplayName != null && dms.parentDisplayName != null && !this.parentDisplayName.equals(dms.parentDisplayName))
			return false;
		if (this.parentNameColIndex != dms.parentNameColIndex)
			return false;
		if (this.parentNameDisplayName != null && dms.parentNameDisplayName == null)
			return false;
		if (this.parentNameDisplayName == null && dms.parentNameDisplayName != null)
			return false;
		if (this.parentNameDisplayName != null && dms.parentNameDisplayName != null && !this.parentNameDisplayName.equals(dms.parentNameDisplayName))
			return false;
		if (this.ordinalColIndex != dms.ordinalColIndex)
			return false;
		if (this.ordinalDisplayName != null && dms.ordinalDisplayName == null)
			return false;
		if (this.ordinalDisplayName == null && dms.ordinalDisplayName != null)
			return false;
		if (this.ordinalDisplayName != null && dms.ordinalDisplayName != null && !this.ordinalDisplayName.equals(dms.ordinalDisplayName))
			return false;
		if (this.uniqueNameColIndex != dms.uniqueNameColIndex)
			return false;
		if (this.uniqueNameDisplayName != null && dms.uniqueNameDisplayName == null)
			return false;
		if (this.uniqueNameDisplayName == null && dms.uniqueNameDisplayName != null)
			return false;
		if (this.uniqueNameDisplayName != null && dms.uniqueNameDisplayName != null && !this.uniqueNameDisplayName.equals(dms.uniqueNameDisplayName))
			return false;
		if (this.navigateonly != dms.navigateonly)
			return false;
		if (this.level != null && dms.level == null)
			return false;
		if (this.level == null && dms.level != null)
			return false;
		if (this.level != null && dms.level != null && !this.level.equals(dms.level))
			return false;
		if (this.sortDirection != null && dms.sortDirection == null)
			return false;
		if (this.sortDirection == null && dms.sortDirection != null)
			return false;
		if (this.sortDirection != null && dms.sortDirection != null && !this.sortDirection.equals(dms.sortDirection))
			return false;
		return true;
	}
	
	@Override
	public void readExternal(ObjectInput oi) throws IOException, ClassNotFoundException
	{
		dimensionName = (String) oi.readObject();
		memberExpressions = (List<MemberSelectionExpression>) oi.readObject();
		displayName = (String) oi.readObject();
		colIndex = (Integer) oi.readObject();
		boolean eofReached = false;
		try
		{
			parentColIndex = (Integer) oi.readObject();
		}
		catch (OptionalDataException e)
		{
			if (!e.eof)
				throw e;
			eofReached = true;
		}
		if(!eofReached)
		{
			try
			{
				parentDisplayName = (String) oi.readObject();
			} catch (OptionalDataException e)
			{
				if (!e.eof)
					throw e;
				eofReached = true;
			}
		}
		try
		{
			parentNameColIndex = (Integer) oi.readObject();
		}
		catch (OptionalDataException e)
		{
			if (!e.eof)
				throw e;
			eofReached = true;
		}
		if(!eofReached)
		{
			try
			{
				parentNameDisplayName = (String) oi.readObject();
			} catch (OptionalDataException e)
			{
				if (!e.eof)
					throw e;
				eofReached = true;
			}
		}
		if(!eofReached)
		{
			try
			{
				ordinalColIndex = (Integer) oi.readObject();
			} catch (OptionalDataException e)
			{
				if (!e.eof)
					throw e;
				eofReached = true;
			}
		}
		if(!eofReached)
		{
			try
			{
				ordinalDisplayName = (String) oi.readObject();
			} catch (OptionalDataException e)
			{
				if (!e.eof)
					throw e;
				eofReached = true;
			}
		}
		try
		{
			uniqueNameColIndex = (Integer) oi.readObject();
		}
		catch (OptionalDataException e)
		{
			if (!e.eof)
				throw e;
			eofReached = true;
		}
		if(!eofReached)
		{
			try
			{
				uniqueNameDisplayName = (String) oi.readObject();
			} catch (OptionalDataException e)
			{
				if (!e.eof)
					throw e;
				eofReached = true;
			}
		}
		if(!eofReached)
		{
			try
			{
				dataType = (String) oi.readObject();
			} catch (OptionalDataException e)
			{
				if (!e.eof)
					throw e;
				eofReached = true;
			}
		}
		if(!eofReached)
		{
			try
			{
				format = (String) oi.readObject();
			} catch (OptionalDataException e)
			{
				if (!e.eof)
					throw e;
				eofReached = true;
			}
		}
		if(!eofReached)
		{
			try
			{
				sortDirection = (String) oi.readObject();
			} catch (OptionalDataException e)
			{
				if (!e.eof)
					throw e;
				eofReached = true;
			}
		}
	}

	@Override
	public void writeExternal(ObjectOutput oo) throws IOException
	{
		oo.writeObject(dimensionName);
		oo.writeObject(memberExpressions);
		oo.writeObject(displayName);
		oo.writeObject(colIndex);
		oo.writeObject(parentColIndex);
		oo.writeObject(parentDisplayName);
		oo.writeObject(parentNameColIndex);
		oo.writeObject(parentNameDisplayName);
		oo.writeObject(ordinalColIndex);
		oo.writeObject(ordinalDisplayName);
		oo.writeObject(uniqueNameColIndex);
		oo.writeObject(uniqueNameDisplayName);
		oo.writeObject(dataType);
		oo.writeObject(format);
		oo.writeObject(sortDirection);
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException
	{
		DimensionMemberSetNav dms = (DimensionMemberSetNav)super.clone();
		dms.dimensionName = dimensionName;
		if (memberExpressions == null)
		{
			dms.memberExpressions = null;			
		}
		else
		{
			dms.memberExpressions = new ArrayList<MemberSelectionExpression>();
			dms.memberExpressions.addAll(memberExpressions);			
		}
		dms.displayName = displayName;
		dms.level = level;
		dms.navigateonly = navigateonly;
		dms.colIndex = colIndex;
		dms.parentColIndex = parentColIndex;
		dms.parentDisplayName = parentDisplayName;
		dms.parentNameColIndex = parentNameColIndex;
		dms.parentNameDisplayName = parentNameDisplayName;
		dms.ordinalColIndex = ordinalColIndex;
		dms.ordinalDisplayName = ordinalDisplayName;
		dms.uniqueNameColIndex = uniqueNameColIndex;
		dms.uniqueNameDisplayName = uniqueNameDisplayName;
		dms.dataType = dataType;
		dms.format = format;
		dms.sortDirection = sortDirection;
		return dms;
	}

	@Override
	public void fromBasicDBObject(BasicDBObject obj) {
		dimensionName = obj.getString("dimensionName");
		try{
			memberExpressions = MongoMgr.convertFromDBList((BasicDBList)obj.get("memberExpressions"), MemberSelectionExpression.class);
		}catch(Exception e){
			logger.error("Fail to convert BasicDBObject", e);
		}
		displayName = obj.getString("displayName");
		colIndex = obj.getInt("colIndex");
		parentColIndex = obj.getInt("parentColIndex");
		parentDisplayName = obj.getString("parentDisplayName");
		parentNameColIndex = obj.getInt("parentNameColIndex");
		parentNameDisplayName = obj.getString("parentNameDisplayName");
		ordinalColIndex = obj.getInt("ordinalColIndex");
		ordinalDisplayName = obj.getString("ordinalDisplayName");
		uniqueNameColIndex = obj.getInt("uniqueNameColIndex");
		uniqueNameDisplayName = obj.getString("uniqueNameDisplayName");
		dataType = obj.getString(dataType);
		format = obj.getString(format);
		sortDirection = obj.getString("sortDirection");
	}

	@Override
	public BasicDBObject toBasicDBObject() {
		BasicDBObject o = new BasicDBObject();
		o.put("dimensionName", dimensionName);
		o.put("memberExpressions", MongoMgr.convertToDBList(memberExpressions));
		o.put("displayName", displayName);
		o.put("colIndex", colIndex);
		o.put("parentColIndex", parentColIndex);
		o.put("parentDisplayName", parentDisplayName);
		o.put("parentNameColIndex", parentNameColIndex);
		o.put("parentNameDisplayName", parentNameDisplayName);
		o.put("ordinalColIndex", ordinalColIndex);
		o.put("ordinalDisplayName", ordinalDisplayName);
		o.put("uniqueNameColIndex", uniqueNameColIndex);
		o.put("uniqueNameDisplayName", uniqueNameDisplayName);
		o.put("dataType", dataType);
		o.put("format", format);
		o.put("sortDirection", sortDirection);
		return o;
	}
	
	public void setSortDirection(boolean direction)
	{
		if (direction)
		{
			this.sortDirection = "ASCENDING";
		}
		else
		{
			this.sortDirection = "DESCENDING";
		}
	}
	
	/**
	 * Returns the key string for this dimension member set
	 * 
	 * @return
	 */
	public String getKey()
	{
		return ("D{" + dimensionName + "}ME{" + getMemberExpressionsString() + "}" + (this.dataType != null ? "DT{" + this.dataType + "}" + (this.format != null ? "F{" + this.format + "}" : "") : "") + ("O{" + this.sortDirection + "}") );
	}
	
	public String getMemberExpressionsString()
	{
		StringBuilder memberExpresssionsBuilder = new StringBuilder();
		boolean first = true;
		for (MemberSelectionExpression expr : memberExpressions)
		{
			if (first)
				first = false;
			else
				memberExpresssionsBuilder.append(",");
			memberExpresssionsBuilder.append(expr.selectionType.toString()).append("(");
			if (!expr.memberName.startsWith("'"))
				memberExpresssionsBuilder.append("'");
			memberExpresssionsBuilder.append(expr.memberName);
			if (!expr.memberName.endsWith("'"))
				memberExpresssionsBuilder.append("'");
			memberExpresssionsBuilder.append(")");	
		}
		return memberExpresssionsBuilder.toString();
	}
	
	public DimensionTable findDimensionTableForDimensionMemberSet(Repository r)
	{
		String dimHierarchy = dimensionName;
		Hierarchy h = r.findHierarchy(dimHierarchy);
		if (h != null)
		{
			level = h.DimensionKeyLevel;
			DimensionTable dt = r.findDimensionTableAtLevel(dimHierarchy, level.Name);
			if (dt != null && dt.TableName.startsWith("Cube: ") && dt.TableName.endsWith(", Dimension: " + dimensionName) 
					&& dt.TableSource != null && dt.TableSource.connection != null 
					&& dt.TableSource.connection.DBType  == DatabaseType.MSASXMLA)
			{
				return dt;
			}
		}
		return null;
	}

	/**
	 * Returns whether the underlying navigated dimension member set is cacheable (assumes that this is done after
	 * navigation)
	 * 
	 * @return
	 */
	public boolean isCacheable(Repository r)
	{
		DimensionTable dt = findDimensionTableForDimensionMemberSet(r);
		if (dt != null)
			return dt.Cacheable;
		return false;
	}

	public String toString()
	{
		return (this.dimensionName + ".{" + this.getMemberExpressionsString() + "}");		
	}

	public String getDisplayName()
	{
		return displayName;
	}

	public void setDisplayName(String displayName)
	{
		this.displayName = displayName;
	}
}
