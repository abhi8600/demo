package com.successmetricsinc.query.olap;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import com.mongodb.BasicDBObject;
import com.successmetricsinc.query.BasicDBObjectSerializer;

public class OlapMemberExpression implements Externalizable, BasicDBObjectSerializer, Cloneable
{

	public String expression;
	public String displayName;
	public int colIndex;
	
	public OlapMemberExpression(){		
	}
	
	public OlapMemberExpression(String expression, String displayName, int colIndex)
	{
		this.expression = expression;
		this.displayName = displayName;
		this.colIndex = colIndex;
	}

	@Override
	public void fromBasicDBObject(BasicDBObject obj) {
		expression = obj.getString("expression");
		displayName = obj.getString("displayName");
		colIndex = obj.getInt("colIndex");
	}

	@Override
	public BasicDBObject toBasicDBObject() {
		BasicDBObject o = new BasicDBObject();
		o.put("expression", expression);
		o.put("displayName", displayName);
		o.put("colIndex", colIndex);
		return o;
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeObject(expression);
		out.writeObject(displayName);
		out.writeObject(colIndex);
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException,
			ClassNotFoundException {
		expression = (String)in.readObject();
		displayName = (String)in.readObject();
		colIndex = (Integer)in.readObject();
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException
	{
		OlapMemberExpression exp = new OlapMemberExpression();
		exp.expression = this.expression;
		exp.displayName = this.displayName;
		exp.colIndex = this.colIndex;
		return exp;
	}
	
	public String getKey()
	{
		return ("OEXPR{" + expression + "}");
	}
}
