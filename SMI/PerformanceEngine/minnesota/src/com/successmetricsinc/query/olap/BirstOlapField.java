/**
 * 
 */
package com.successmetricsinc.query.olap;

import java.io.Serializable;

import com.successmetricsinc.Session;

/**
 * @author agarrison
 *
 */
public class BirstOlapField implements Comparable<BirstOlapField>, Serializable {
	private static String SPACES_PER_ORDINAL = "     ";
	public static final String OLAP = "_OLAP";
	public static final String ORDINAL = "_ORDINAL";
	public static final String PARENT = "_PARENT_UNIQUE_NAME";
	public static final String UNIQUENAME = "_UNIQUE_NAME";
	public static final String PARENTNAME = "_PARENT_NAME";

	private Serializable value;
	private String parent;
	private int ordinal;
	private String uniqueName;
	private String parentName;
	private int index;

	public BirstOlapField() { }
	
	public BirstOlapField(Serializable value, String parent, int ordinal, String uniqueName, String parentName, int index) {
		this.value = value;
		this.parent = parent;
		this.ordinal = ordinal;
		this.uniqueName = uniqueName;
		this.parentName = parentName;
		this.index = index;
	}
	
	public String toString() {
		StringBuffer ret = new StringBuffer();
		ret.append(value == null ? "null" : value.toString());
		return ret.toString();
	}
	
	public Object getValue() {
		return value;
	}
	
	public void setValue(Serializable o) {
		this.value = o;
	}
	
	public String getPrefix() {
		if (Session.isUseOlapIndentation() == false) {
			return "";
		}
		StringBuffer ret = new StringBuffer();
		for (int i = 0; i < ordinal; i++) {
			ret.append(SPACES_PER_ORDINAL);
		}
		return ret.toString();
	}

	public String getOrderByString() {
		return String.valueOf(ordinal) + ": " + toString();
	}
	
	public String getTechName() {
		return uniqueName;
	}
	
	@Override
	public int compareTo(BirstOlapField o) {
		if (o == null)
			return 1;
		return index - o.index;
		/*
		if (ordinal == o.ordinal + 1) {
			// could be child
			if (parentName == null) {
				if (parent.compareTo(o.value.toString()) < 0)
					return -1;
				else
					return 1;
			}
			else {
				if (parentName.compareTo(o.value.toString()) < 0)
					return -1;
				else
					return 1;
			}
		}
		if (ordinal == o.ordinal - 1) {
			if (o.parentName == null) {
				if (o.parent.compareTo(value.toString()) >= 0)
					return -1;
				else
					return 1;
			}
			else {
				if (o.parentName.compareTo(value.toString()) >= 0)
					return -1;
				else
					return 1;
			}
		}
		if (ordinal != o.ordinal)
			return ordinal - o.ordinal;
		
		int r = parent.compareTo(o.parent);
		if (parentName != null) {
			r = parentName.compareTo(o.parentName);
		}
		if (r == 0)
			return toString().compareTo(o.toString());
		
		return r;
		*/
	}
}
