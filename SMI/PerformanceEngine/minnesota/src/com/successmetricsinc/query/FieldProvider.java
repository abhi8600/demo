/**
 * $Id: FieldProvider.java,v 1.26 2011-12-02 00:32:39 ricks Exp $
 *
 * Copyright (C) 2007-2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.successmetricsinc.query.olap.OlapMemberExpression;
import com.successmetricsinc.util.BaseException;

public class FieldProvider
{
	private Map<String, Integer> fieldMap;
	private Object[][] array;
	private Map<Thread,Integer> rowIndex = new HashMap<Thread,Integer>();
	private int numRows = 0;
	private DisplayExpression de;
	private boolean isNewLogicalQueryLanguage;

	public FieldProvider(DisplayExpression de)
	{
		this.de = de;
		isNewLogicalQueryLanguage = de.getCompiledExprssion() == null;
	}
	
	public FieldProvider clone(DisplayExpression de)
	{
		FieldProvider fp = new FieldProvider(de);
		fp.fieldMap = fieldMap;
		return fp;
	}
	
	public Double getDoubleProperty(byte type, int deIndex) throws Exception
	{
		Integer index = null;
		String name = null;
		int rowIndex = this.rowIndex.get(Thread.currentThread());
		Object result = null;
		
		if (rowIndex >= array.length)
			return (Double.NaN);
		
		if (type == DisplayExpression.MEASURE_TYPE)
		{
			MeasureExpressionColumn mec = de.measureExpressionColumns.get(deIndex);
			result = mec.getValue(array, rowIndex);
		} else
		{
			name = de.dimensionExpressionColumns.get(deIndex).dimName + "." + de.dimensionExpressionColumns.get(deIndex).colName;
			index = fieldMap.get(name);
			if (index == null)
			{
				throw (new Exception("Unrecognized column: " + name));
			}
			result = array[rowIndex][index];
		}
		if (Double.class.isInstance(result))
			return (Double) result;
		else if (Integer.class.isInstance(result))
			return ((Integer) result).doubleValue();
		else if (Long.class.isInstance(result))
			return ((Long) result).doubleValue();
		else if (isNewLogicalQueryLanguage)
			return null;
		else
			return (Double.NaN);
	}

	public Long getIntProperty(byte type, int deIndex) throws Exception
	{
		Integer index = null;
		String name = null;
		int rowIndex = this.rowIndex.get(Thread.currentThread());
		Object result = null;
		
		if (rowIndex >= array.length)
			return 0l;
		
		if (type == DisplayExpression.MEASURE_TYPE)
		{
			MeasureExpressionColumn mec = de.measureExpressionColumns.get(deIndex);
			result = mec.getValue(array, rowIndex);
		} else
		{
			name = de.dimensionExpressionColumns.get(deIndex).dimName + "." + de.dimensionExpressionColumns.get(deIndex).colName;
			index = fieldMap.get(name);
			if (index == null)
			{
				throw (new Exception("Unrecognized column: " + name));
			}
			result = array[rowIndex][index];
		}
		if (Integer.class.isInstance(result))
			return Long.valueOf(((Integer) result).toString());
		else if (Long.class.isInstance(result))
			return (Long) result;
		else if (Double.class.isInstance(result))
			return ((Double) result).longValue();
		else if (isNewLogicalQueryLanguage)
			return null;
		else
			return 0l;
	}

	public String getStringProperty(byte type, int deIndex) throws Exception
	{
		Object result = null;
		String name = null;
		Integer index = null;
		
		int rowIndex = this.rowIndex.get(Thread.currentThread());
		if (rowIndex >= array.length)
			return null;
		
		if (type == DisplayExpression.MEASURE_TYPE)
		{
			MeasureExpressionColumn mec = de.measureExpressionColumns.get(deIndex);
			result = mec.getValue(array, rowIndex);
		} else if (type == DisplayExpression.OLAP_TYPE)
		{
			OlapMemberExpression exp = de.olapMemberExpressionColumns.get(deIndex);
			name = exp.expression;
			index = fieldMap.get(name);
			if (index == null)
			{
				throw (new Exception("Unrecognized column: " + name));
			}
			result = array[rowIndex][index];
		} else
		{
			name = de.dimensionExpressionColumns.get(deIndex).dimName + "." + de.dimensionExpressionColumns.get(deIndex).colName;
			index = fieldMap.get(name);
			if (index == null)
			{
				throw (new Exception("Unrecognized column: " + name));
			}
			result = array[rowIndex][index];
		}
		if (result == null)
		{
			if (isNewLogicalQueryLanguage)
				return null;
			return "null";
		}
		else
			return (result.toString());
	}

	public Object getObjectProperty(byte type, int deIndex) throws Exception
	{
		Object result = null;
		String name = null;
		Integer index = null;
		
		int rowIndex = this.rowIndex.get(Thread.currentThread());
		if (rowIndex >= array.length)
			return null;
		
		if (type == DisplayExpression.MEASURE_TYPE)
		{
			MeasureExpressionColumn mec = de.measureExpressionColumns.get(deIndex);
			result = mec.getValue(array, rowIndex);
		} else
		{
			name = de.dimensionExpressionColumns.get(deIndex).dimName + "." + de.dimensionExpressionColumns.get(deIndex).colName;
			index = fieldMap.get(name);
			if (index == null)
			{
				throw (new Exception("Unrecognized column: " + name));
			}
			result = array[rowIndex][index];
		}
		return result;
	}

	/**
	 * Create column lists for each element of this field provider. Column lists are used for binding the appropriate
	 * columns in both expression calculations and in joining the main result set to measure expression result sets.
	 * 
	 * @param list
	 *            The column list for the main query
	 * @param resultSetSize
	 *            Size of main result set (used for determining whether position is at a higher or lower level than main
	 *            result set)
	 * @throws NavigationException
	 */
	public void setColumnList(List<String[]> list, int resultSetSize)
	throws BaseException, CloneNotSupportedException
	{
		// Map columns
		this.fieldMap = new HashMap<String, Integer>();
		for (int i = 0; i < list.size(); i++)
		{
			String[] item = list.get(i);
			String name = null;
			if (item[3] == null)
				name = item[0];
			else
				name = item[1];
			Integer index = this.fieldMap.get(name);
			if (index == null)
			{
				if (!item[2].equals("E"))
					this.fieldMap.put(name, i);
				else if (name.startsWith("{DIMENSIONEXPRESSION}"))
					this.fieldMap.put(name, i);
			}
		}
		for (MeasureExpressionColumn mec : this.de.measureExpressionColumns)
		{
			mec.fieldMap = null;
			if (mec.positions.size() > 0)
			{
				if (mec.resultSet != null)
				{
					/* Mark whether the measure expression is at a higher level (bridge table has fewer rows) */
					mec.bridgeAtHigherLevel = mec.bridgeResultSet.getRows().length <= resultSetSize;
					mec.fieldMap = new HashMap<String, Integer>();
					// Get the resulting column order
					mec.colList = QueryResultSet.determineColumns(mec.query, null);
					mec.brColList = QueryResultSet.determineColumns(mec.bridgeQuery, null);
					for (int i = 0; i < mec.colList.size(); i++)
					{
						mec.fieldMap.put(mec.colList.get(i)[3] == null ? mec.colList.get(i)[0] : mec.colList.get(i)[1], i);
					}
				}
				if (mec.dimensionExpressionResultSet != null)
				{
					mec.fieldMap = new HashMap<String, Integer>();
					for (Entry<String, Integer> en : fieldMap.entrySet())
						mec.fieldMap.put(en.getKey(), en.getValue());
					if (!mec.fieldMap.containsKey(mec.displayName))
					{
						for (int i = 0; i < mec.dimensionExpressionResultSet.displayNames.length; i++)
						{
							if (mec.displayName.equals(mec.dimensionExpressionResultSet.displayNames[i]))
								mec.fieldMap.put(mec.displayName, i);
						}
					}
				}
			}
			if (mec.fieldMap == null)
				mec.fieldMap = fieldMap;
			if (mec.dimensionExpressionQuery == null)
				mec.setBindings(list);
		}
	}

	/**
	 * Set the arrays associated with the expression and any measure expression columns. For measure expression columns,
	 * create a mapping from rows of the main query to measure expression array rows
	 * 
	 * @param array
	 *            The array to set.
	 * @throws DisplayExpressionException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @throws SessionVariableUnavailableException 
	 */
	public synchronized void setArrays(ResultSetCache rsc, QueryResultSet qrs, RowMapCache rmap, List<MeasureExpressionColumn> meColumns)
			throws IOException, ClassNotFoundException, CloneNotSupportedException, BaseException
	{
		this.array = qrs.getRows();
		if (de.measureExpressionColumns != null)
			for (MeasureExpressionColumn mec : de.measureExpressionColumns)
			{
				if (mec.dimensionExpressionQuery !=null)
					continue;
				// Create all the maps necessary for each expression
				mec.createRowMaps(rsc, qrs, rmap, meColumns);
			}
	}

	/**
	 * @param index
	 *            Index of current row to process
	 */
	public synchronized void setRowIndex(int index)
	{
		this.rowIndex.put(Thread.currentThread(),  index);
	}

	public synchronized int getRowIndex()
	{
		return this.rowIndex.get(Thread.currentThread());
	}

	public int getNumRows()
	{
		return numRows;
	}

	public void setNumRows(int numRows)
	{
		this.numRows = numRows;
	}

	public Map<String, Integer> getFieldMap()
	{
		return fieldMap;
	}
}