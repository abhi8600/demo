/**
 * $Id: MeasureColumnNav.java,v 1.28 2012/01/19 02:16:12 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBList;

import com.successmetricsinc.Variable;


/**
 * Class to process measure column navigation. Initialize with a measure column name and the navigation process will
 * select the appropriate logical table and column
 * 
 * @author bpeters
 * 
 */
public class MeasureColumnNav implements Externalizable, Cloneable, BasicDBObjectSerializer
{
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(MeasureColumnNav.class);
	String measureName;
	String displayName;
	List<MeasureColumn> matchingMeasures;
	boolean hidden;
	int pick = 0;
	int[] dimpicks;
	double cardinality = 1;
	int colIndex;
	boolean navigateonly = false;
	QueryFilter filter;
	String aggregationRule;
	List<DimensionRule> dimensionRules;
	String derivedMeasureFormula;
	List<Variable> sessionVariables;
	Level [] dimpickJoinLevels = null;
	boolean sessionVariableExpand;
	transient boolean isNonAdditive = false;

	/**
	 * Constructor used for serialization
	 */
	public MeasureColumnNav()
	{
	}

	public long getExpires()
	{
		MeasureColumn mc = getMeasureColumn();
		MeasureTable mt = mc.MeasureTable;
		
		if (matchingMeasures == null || matchingMeasures.isEmpty()) // not navigated, expires immediately
		{
			logger.debug("measure column has no matching measures, expires immediately - " + mt.DisplayName + "/" + mc.ColumnName);			
			return 0;
		}
		if (mt.Cacheable && !mc.NotCacheable)
		{
			if (mt.TTL == -1)
				return Long.MAX_VALUE;
			else
			{
				if (mt.TTL == 0)
					logger.debug("Zero TTL for " + mt.PhysicalName + " / " + mc.PhysicalName);
				return (long) mt.TTL + System.currentTimeMillis();
			}
		}
		logger.debug("measure column or table not cacheable, expires immediately - " + mt.DisplayName + "/" + mc.ColumnName);
		return 0;
	}
	
	public Object clone() throws CloneNotSupportedException
	{
		MeasureColumnNav mcn = (MeasureColumnNav) super.clone();
		mcn.measureName = measureName;
		mcn.displayName = displayName;
		mcn.hidden = hidden;
		mcn.colIndex = colIndex;
		mcn.navigateonly = navigateonly;
		mcn.filter = filter;
		mcn.aggregationRule = aggregationRule;
		mcn.dimensionRules = dimensionRules;
		mcn.matchingMeasures = new ArrayList<MeasureColumn>();
		mcn.derivedMeasureFormula = derivedMeasureFormula;
		mcn.sessionVariables = sessionVariables == null ? null : new ArrayList<Variable>(sessionVariables);
		mcn.isNonAdditive = isNonAdditive;
		return (mcn);
	}

	MeasureColumnNav(String name, boolean hidden, int colIndex)
	{
		this.measureName = name;
		this.hidden = hidden;
		this.matchingMeasures = new ArrayList<MeasureColumn>();
		this.colIndex = colIndex;
	}

	public void writeExternal(ObjectOutput oo) throws IOException
	{
		oo.writeObject(measureName);
		oo.writeObject(displayName);
		oo.writeObject(filter);
		oo.writeObject(aggregationRule);
		oo.writeObject(dimensionRules);
		oo.writeObject(derivedMeasureFormula);
	}

	public void fromBasicDBObject(BasicDBObject obj){
		if(obj != null){
			measureName = obj.getString("measureName");
			displayName = obj.getString("displayName");

			BasicDBObject fObj = (BasicDBObject)obj.get("filter");
			if(fObj != null){
				filter = new QueryFilter();
				filter.fromBasicDBObject(fObj);
			}
			aggregationRule = obj.getString("aggregationRule");
			try{
				dimensionRules = MongoMgr.convertFromDBList((BasicDBList)obj.get("dimensionRules"), DimensionRule.class);
			}catch(Exception e){
				logger.error("Fail to convert from BasicDBObject", e);
			}

			derivedMeasureFormula = obj.getString("derivedMeasureFormula");
			matchingMeasures = new ArrayList<MeasureColumn>();
		}
	}
	
	public BasicDBObject toBasicDBObject(){
		BasicDBObject o = new BasicDBObject();
		
		o.put("measureName", this.measureName);
		o.put("displayName", this.displayName);
		o.put("filter", filter != null ? filter.toBasicDBObject() : null);
		o.put("aggregationRule", this.aggregationRule);
		o.put("dimensionRules", MongoMgr.convertToDBList(dimensionRules));
		o.put("derivedMeasureFormula", this.derivedMeasureFormula);

		return o;
	}
	
	@SuppressWarnings("unchecked")
	public void readExternal(ObjectInput oi) throws IOException, ClassNotFoundException
	{
		measureName = (String) oi.readObject();
		displayName = (String) oi.readObject();
		filter = (QueryFilter) oi.readObject();
		aggregationRule = (String) oi.readObject();
		dimensionRules = (List<DimensionRule>) oi.readObject();
		derivedMeasureFormula = (String) oi.readObject();
		matchingMeasures = new ArrayList<MeasureColumn>();
	}

	/**
	 * Returns the key string for this dimension column
	 * 
	 * @return
	 * @throws NavigationException
	 */
	public String getKey()
	{
		try
		{
			StringBuilder sb = new StringBuilder();
			sb.append("M{");
			sb.append(measureName);
			if (filter != null)
			{
				sb.append(',');
				sb.append(filter.getQueryKeyStr(null, null));
			}
			sb.append('}');
			return sb.toString();
		} 
		catch (NavigationException e)
		{
			StringBuilder sb = new StringBuilder();
			sb.append("M{");
			sb.append(measureName);
			sb.append('}');
			return sb.toString();
		}
		catch (Exception e)
		{
			logger.debug(e.getMessage(), e);
			return null;
		}
	}

	/**
	 * Returns whether the measure column is additive or not (SUM, COUNT, MIN and MAX vs. AVG, COUNT DISTINCT or STDEV),
	 * along the grouped dimensions
	 * 
	 * @return
	 */
	public boolean isAdditive(List<GroupBy> gblist)
	{
		if(isNonAdditive)
		{
			return false;
		}
		if (gblist == null)
			return (aggregationRule != null && (aggregationRule.equals("SUM") || aggregationRule.equals("COUNT") || aggregationRule.equals("MIN") || aggregationRule
					.equals("MAX")));
		else
		{
			/*
			 * If there is dimension-specific aggregation, see if there is a dimension that is not additive
			 */
			if (dimensionRules != null)
				for (DimensionRule rule : dimensionRules)
				{
					/*
					 * If a rule is non-additive, make sure it is included in a group by (so that it is not aggregated)
					 */
					if ((rule.getAggRule().equals("COUNT DISTINCT") || rule.getAggRule().equals("AVG") || rule.getAggRule().equals("STDEV")))
					{
						boolean found = false;
						for (GroupBy gb : gblist)
						{
							if (gb.getDimName().equals(rule.getDimension()))
							{
								found = true;
								break;
							}
						}
						if (found)
							return (false);
					}
				}
			return (aggregationRule != null && (aggregationRule.equals("SUM") || aggregationRule.equals("COUNT") || aggregationRule.equals("MIN") || aggregationRule
					.equals("MAX")));
		}
	}

	/**
	 * Returns whether the underlying navigated measure column is cacheable (assumes that this is done after navigation)
	 * 
	 * @return
	 */
	public boolean isCacheable()
	{
		if (matchingMeasures == null || matchingMeasures.isEmpty())
			return (false);
		return (getMeasureColumn().MeasureTable.Cacheable && !getMeasureColumn().NotCacheable);
	}

	public String toString()
	{
		return (measureName);
	}

	public String getDisplayName()
	{
		return displayName;
	}

	public void setDisplayName(String displayName)
	{
		this.displayName = displayName;
	}

	public String getMeasureName()
	{
		return measureName;
	}

	public void setMeasureName(String measureName)
	{
		this.measureName = measureName;
	}

	public int getColIndex() {
		return colIndex;
	}

	public void setColIndex(int colIndex) {
		this.colIndex = colIndex;
	}
	
	public MeasureColumn getMeasureColumn()
	{
		return matchingMeasures.get(pick);
	}
}
