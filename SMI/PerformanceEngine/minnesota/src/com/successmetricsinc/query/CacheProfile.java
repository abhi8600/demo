/**
 * $Id: CacheProfile.java,v 1.15 2011-09-28 17:01:58 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 * Class to profile a cache
 * 
 * @deprecated
 * 
 */
public class CacheProfile
{
	private static Logger logger = Logger.getLogger(CacheProfile.class);

	@SuppressWarnings("unused")
	private ResultSetCache rsc;

	public CacheProfile(ResultSetCache rsc)
	{
		this.rsc = rsc;
	}

	public List<CacheProfileEntry> generateProfile() throws Exception
	{
		throw new Exception ("This feature is not implemented by MongoDB managed caches");
		/*
		@SuppressWarnings("rawtypes")
		FastIterator iter = null; // rsc.getCacheEntries();
		List<CacheProfileEntry> list = new ArrayList<CacheProfileEntry>();
		CacheEntry entry = (CacheEntry) iter.next();
		while (entry != null)
		{
			QueryResultSet qrs = null; // entry.getResultSet(rsc.getCacheDir());
			CacheProfileEntry cpe = new CacheProfileEntry(qrs.getColumnNames().length, qrs.getRows().length);
			cpe.QueryString = qrs.getQuery().getLogicalQueryString(false, null, false);
			cpe.ColumnNames = qrs.getColumnNames();
			cpe.ColumnDataTypes = qrs.getColumnDataTypes();
			Object[][] rows = qrs.getRows();
			for (int i = 0; i < cpe.ColumnNames.length; i++)
			{
				if (cpe.ColumnDataTypes[i] == Types.VARCHAR)
				{
					Set<Object> valueSet = new HashSet<Object>();
					for (int j = 0; j < rows.length; j++)
						valueSet.add(rows[j][i]);
					cpe.numDistinct[i] = valueSet.size();
				} else if (cpe.ColumnDataTypes[i] == Types.DOUBLE || cpe.ColumnDataTypes[i] == Types.INTEGER)
				{
					double sum = 0;
					double sumsq = 0;
					cpe.minimums[i] = Double.NaN;
					cpe.maximums[i] = Double.NaN;
					for (int j = 0; j < rows.length; j++)
					{
						if (rows[j][i] == null)
							continue;
						double val = ((Number) rows[j][i]).doubleValue();
						sum += val;
						sumsq += val * val;
						if (Double.isNaN(cpe.maximums[i]) || val > cpe.maximums[i])
							cpe.maximums[i] = val;
						if (Double.isNaN(cpe.minimums[i]) || val < cpe.minimums[i])
							cpe.minimums[i] = val;
					}
					if (rows.length == 0)
					{
						cpe.averages[i] = Double.NaN;
						cpe.deviations[i] = Double.NaN;
					} else
					{
						cpe.averages[i] = sum / rows.length;
						cpe.deviations[i] = Math.sqrt((sumsq / rows.length) - (cpe.averages[i] * cpe.averages[i]));
					}
				}
			}
			list.add(cpe);
			entry = (CacheEntry) iter.next();
		}
		Collections.sort(list);
		return (list);
		*/
	}

	/**
	 * Script the cache to a file (if "console" then output to console)
	 * 
	 * @param out
	 * @throws IOException
	 */
	public void scriptCache(String filename) throws Exception
	{
		OutputStream out = filename.equals("console") ? System.out : new FileOutputStream(filename);
		PrintWriter writer = new PrintWriter(out);
		for (CacheProfileEntry cpe : generateProfile())
		{
			writer.println(cpe.QueryString);
		}
		writer.close();
	}

	/**
	 * Save a set of cache profile entries to a file (if "console" then output to console)
	 * 
	 * @param list
	 * @param filename
	 * @throws FileNotFoundException
	 */
	public static void writeProfiles(List<CacheProfileEntry> list, String filename) throws FileNotFoundException
	{
		OutputStream out = filename.equals("console") ? System.out : new FileOutputStream(filename);
		PrintWriter writer = new PrintWriter(out);
		for (CacheProfileEntry cpe : list)
		{
			writer.println(cpe.QueryString + "\t" + cpe.ColumnNames.length + "\t" + cpe.numRows);
			for (int i = 0; i < cpe.ColumnNames.length; i++)
			{
				writer.println(cpe.ColumnNames[i] + "\t" + cpe.ColumnDataTypes[i] + "\t" + cpe.numDistinct[i] + "\t" + cpe.minimums[i] + "\t" + cpe.averages[i]
						+ "\t" + cpe.maximums[i] + "\t" + cpe.deviations[i]);
			}
		}
		writer.close();
	}

	/**
	 * Retrieve a saved cache profile
	 * 
	 * @param filename
	 * @return
	 * @throws IOException
	 */
	private static List<CacheProfileEntry> getProfiles(String filename) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(filename));
		List<CacheProfileEntry> list = new ArrayList<CacheProfileEntry>();
		String line = null;
		while ((line = reader.readLine()) != null)
		{
			String[] tokens = line.split("\t");
			int numColumns = Integer.valueOf(tokens[1]);
			int numRows = Integer.valueOf(tokens[2]);
			CacheProfileEntry cpe = new CacheProfileEntry(numColumns, numRows);
			cpe.QueryString = tokens[0];
			for (int i = 0; i < numColumns; i++)
			{
				line = reader.readLine();
				if (line == null)
				{
					logger.error("Corrupt Cache Profile file");
					reader.close();
					return null;
				}
				tokens = line.split("\t");
				cpe.ColumnNames[i] = tokens[0];
				cpe.ColumnDataTypes[i] = Integer.valueOf(tokens[1]);
				cpe.numDistinct[i] = Integer.valueOf(tokens[2]);
				cpe.minimums[i] = Double.valueOf(tokens[3]);
				cpe.averages[i] = Double.valueOf(tokens[4]);
				cpe.maximums[i] = Double.valueOf(tokens[5]);
				cpe.deviations[i] = Double.valueOf(tokens[6]);
			}
			list.add(cpe);
		}
		reader.close();
		return (list);
	}

	/**
	 * Compare two saved cache profiles
	 * 
	 * @param file1
	 * @param file2
	 * @param out
	 * @throws IOException
	 */
	public static void compareProfiles(String file1, String file2, OutputStream out) throws IOException
	{
		List<CacheProfileEntry> list1 = getProfiles(file1);
		List<CacheProfileEntry> list2 = getProfiles(file2);
		if (list1 == null || list2 == null)
		{
			logger.error("Corrupt Cache Profile File");
			return;
		}
		Map<String, Integer> map = new HashMap<String, Integer>();
		PrintWriter writer = new PrintWriter(out);
		for (int i = 0; i < list2.size(); i++)
		{
			map.put(list2.get(i).QueryString, i);
		}
		writer.println(file1 + ": " + list1.size() + " entries");
		writer.println(file2 + ": " + list2.size() + " entries");
		for (CacheProfileEntry cpe1 : list1)
		{
			Integer index2 = map.get(cpe1.QueryString);
			if (index2 != null)
			{
				StringBuilder sb = new StringBuilder();
				CacheProfileEntry cpe2 = list2.get(index2);
				if (cpe1.ColumnNames.length != cpe2.ColumnNames.length)
				{
					writer.println("Critical Difference: " + file1 + " " + cpe1.ColumnNames.length + " columns, " + file2 + " " + cpe2.ColumnNames.length
							+ " columns");
				} else
				{
					for (int i = 0; i < cpe1.ColumnNames.length; i++)
					{
						if (cpe1.ColumnDataTypes[i] != cpe2.ColumnDataTypes[i])
						{
							sb.append("Critical Difference Column " + i + ": " + file1 + " " + cpe1.ColumnDataTypes[i] + " datatype, " + file2 + " "
									+ cpe2.ColumnDataTypes[i] + " datatype\n");
						} else
						{
							if (cpe1.ColumnDataTypes[i] == Types.VARCHAR)
							{
								if (cpe1.numDistinct[i] != cpe2.numDistinct[i])
									sb.append("Difference Column " + i + ": " + file1 + " " + cpe1.numDistinct[i] + " distinct, " + file2 + " "
											+ cpe2.numDistinct[i] + " distinct\n");
							} else if (cpe1.ColumnDataTypes[i] == Types.DOUBLE || cpe1.ColumnDataTypes[i] == Types.INTEGER)
							{
								double range1 = cpe1.maximums[i] - cpe1.minimums[i];
								double range2 = cpe2.maximums[i] - cpe2.minimums[i];
								if (range2 != range1)
								{
									if ((range2 == 0 && range1 != 0) || (range1 == 0 && range2 != 0))
									{
										if (range2 == 0)
											sb.append("Difference Column " + i + ": " + file1 + " " + " nonzero range, " + file2 + " " + " zero range\n");
										else
											sb.append("Difference Column " + i + ": " + file1 + " " + " zero range, " + file2 + " " + " nonzero range\n");
									} else if (range2 / range1 > 2 || range1 / range2 > 2)
									{
										sb.append("Difference Column " + i + ": " + file1 + " range " + range1 + ", " + file2 + " range " + range2 + "\n");
									}
								} else
								{
									double avgrange = (range1 + range2) / 2;
									double diff = avgrange * 0.1;
									if (Math.abs(cpe1.minimums[i] - cpe2.minimums[i]) > diff)
									{
										sb.append("Difference Column " + i + ": " + file1 + " miniumum " + cpe1.minimums[i] + ", " + file2 + " minimum "
												+ cpe2.minimums[i] + "\n");
									} else if (Math.abs(cpe1.maximums[i] - cpe2.maximums[i]) > diff)
									{
										sb.append("Difference Column " + i + ": " + file1 + " maximum " + cpe1.maximums[i] + ", " + file2 + " maximum "
												+ cpe2.maximums[i] + "\n");
									} else if (Math.abs(cpe1.averages[i] - cpe2.averages[i]) > diff)
									{
										sb.append("Difference Column " + i + ": " + file1 + " average " + cpe1.averages[i] + ", " + file2 + " average "
												+ cpe2.averages[i] + "\n");
									} else if (Math.abs(cpe1.deviations[i] - cpe2.deviations[i]) > diff)
									{
										sb.append("Difference Column " + i + ": " + file1 + " deviation " + cpe1.deviations[i] + ", " + file2 + " deviation "
												+ cpe2.deviations[i] + "\n");
									}
								}
							}
						}
					}
				}
				if (sb.length() > 0)
				{
					writer.println(cpe1.QueryString);
					writer.print(sb);
				}
			}
			else
			{
				writer.println("not found: " + cpe1.QueryString);
			}
		}
		writer.close();
	}
}
