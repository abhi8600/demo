/**
 * $Id: DatabaseConnection.java,v 1.158 2012-12-06 18:57:26 BIRST\ricks Exp $
 *
 * Copyright (C) 2007-2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.Repository;
import com.successmetricsinc.query.Database.DatabaseType;
import com.successmetricsinc.security.EncryptionService;
import com.successmetricsinc.util.RepositoryException;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XmlUtils;

public class DatabaseConnection implements Serializable, Cloneable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(DatabaseConnection.class);
	private String DBTimeZone; //database server timezone
	public String Name; // Connection name
	public String Type; // DB type
	public String VisibleName; // Connection visible name
	public String Driver; // JDBC driver (java class name, fully qualified)
	public String ConnectString; // JDBC connection string, contains database name
	public String UserName; // database user
	public String Password; // database password, encrypted until used
	public String Schema; // database schema (schema for MSSQL, database for MySQL/Infobright)	
	public int ConnectionPoolSize; // XXX this should be removed
	public int MaxConnectionThreads; // XXX this should be removed
	public DatabaseType DBType;
	public ConnectionPool ConnectionPool;
	public boolean Realtime; // is the connection realtime
	public String ID; // ID of the realtime connection
	private boolean unicodeDatabase; // is the database set up for Unicode strings
	public boolean Disabled = false;
	public static final String NCHAR = "N_";
	private SimpleDateFormat sdf;
	public boolean useDirectConnection;
	public boolean useCompression = false;
	public boolean isPartitioned = false;
	public String [] partitionConnectionNames;
	public DatabaseConnection [] partitionConnections;
	private transient boolean IBLoadWarehouseUsingUTF8 = false;
	public transient DatabaseConnection [] federatedJoinConnections = null;
	
	private String strConnectString;
	private String Database;
	private String strVisibleName;
	private String encryptedPassword;
	
	public DatabaseConnection()
	{
	}

	public DatabaseConnection(Element e, Namespace ns, Repository r) throws RepositoryException
	{
		Name = e.getChildText("Name", ns);
		strVisibleName = e.getChildText("VisibleName", ns);
		VisibleName = e.getChildText("VisibleName", ns);
		if (VisibleName == null)
			VisibleName = Name;
		Driver = e.getChildText("Driver", ns);
		ConnectString = e.getChildText("ConnectString", ns);
		strConnectString = e.getChildText("ConnectString", ns);
		Database = e.getChildText("Database", ns);
		UserName = e.getChildText("UserName", ns);
		Password = e.getChildText("Password", ns);
		encryptedPassword = e.getChildText("Password", ns);
		Type = e.getChildText("Type", ns);
		if (Type != null)
		{
			setDBType(Type);
		}
		else
		{
			// XXX missing type, not sure how, but seems to be with live access connections
			setDBTypeFromDriver();
		}
		Driver = fixupDriverClassName(Driver); // for databases with the same drivers, we encode the database type in the driver name and then fix it up here (Redshift specifically)
		Schema = e.getChildText("Schema", ns);
		DBTimeZone = e.getChildText("DBTimeZone", ns);
		String realtimeStr = e.getChildTextTrim("Realtime", ns);
		Realtime = realtimeStr == null ? false : Boolean.parseBoolean(realtimeStr);
		useDirectConnection = Boolean.parseBoolean(e.getChildTextTrim("useDirectConnection", ns));
		unicodeDatabase = Boolean.parseBoolean(e.getChildTextTrim("UnicodeDatabase", ns));
		ConnectionPoolSize = Integer.parseInt(e.getChildText("ConnectionPoolSize", ns));
		MaxConnectionThreads = Integer.parseInt(e.getChildText("MaxConnectionThreads", ns));
		String database = e.getChildTextTrim("Database", ns);
		if (database != null && DBType == DatabaseType.MemDB && ConnectString.toLowerCase().equals("inprocess"))
		{
			// Include the database directory in the connect string if this is an inprocess, in-memory db
			ConnectString = ConnectString + "://" + database;
		}
		if (database != null && isDBTypeXMLA(DBType) && (ConnectString.toLowerCase().startsWith("http://") ||
				ConnectString.toLowerCase().startsWith("https://")))
		{
			// Include the database in the connect string if this is an xmla connection
			ConnectString = ConnectString + "://" + database;
		}
		ID = e.getChildText("ID", ns);
		// put a ceiling on the number of database processing threads to the number of available processors
		// XXX really doesn't make sense in MT, need a MT pool
		int numberOfProcessors = Runtime.getRuntime().availableProcessors();
		if (MaxConnectionThreads > numberOfProcessors)
		{
			// if we had separate DB servers, then this could be increased a bit
			logger.warn("Limiting the number of database processing threads to the number of processors: " + numberOfProcessors + ", requested value was: "
					+ MaxConnectionThreads);
			MaxConnectionThreads = numberOfProcessors;
		}
		useCompression = Boolean.parseBoolean(e.getChildTextTrim("useCompression", ns));
		isPartitioned = Boolean.parseBoolean(e.getChildTextTrim("IsPartitioned", ns));
		if(isPartitioned)
		{
			partitionConnectionNames = 	Repository.getStringArrayChildren(e, "PartitionConnectionNames", ns);
		}		
	}
	
	public Element getDatabaseConnectionElement(Namespace ns)
	{
		Element e = new Element("DatabaseConnection",ns);
		
		Element child = new Element("Name",ns);
		child.setText(Name);
		e.addContent(child);
		
		child = new Element("Type",ns);
		child.setText(Type);
		e.addContent(child);
		
		child = new Element("Driver",ns);
		child.setText(Driver);
		e.addContent(child);
		
		child = new Element("ConnectString",ns);
		child.setText(strConnectString);
		e.addContent(child);
		
		child = new Element("UserName",ns);
		child.setText(UserName);
		e.addContent(child);
		
		child = new Element("Password",ns);
		child.setText(encryptedPassword);
		e.addContent(child);
		
		child = new Element("Schema",ns);
		child.setText(Schema);
		e.addContent(child);
		
		if (Database!=null)
		{
			child = new Element("Database",ns);
			child.setText(Database);
			e.addContent(child);
		}
		
		if (DBTimeZone!=null)
		{
			child = new Element("DBTimeZone",ns);
			child.setText(DBTimeZone);
			e.addContent(child);
		}
		
		child = new Element("ConnectionPoolSize",ns);
		child.setText(String.valueOf(ConnectionPoolSize));
		e.addContent(child);
		
		child = new Element("MaxConnectionThreads",ns);
		child.setText(String.valueOf(MaxConnectionThreads));
		e.addContent(child);
		
		child = new Element("UnicodeDatabase",ns);
		child.setText(String.valueOf(unicodeDatabase));
		e.addContent(child);
		
		if (ID!=null)
		{
			XmlUtils.addContent(e, "ID", ID,ns);
		}
		
		child = new Element("Realtime",ns);
		child.setText(String.valueOf(Realtime));
		e.addContent(child);
		
		if (strVisibleName!=null)
		{
			child = new Element("VisibleName",ns);
			child.setText(strVisibleName);
			e.addContent(child);
		}
		
		if (useDirectConnection)
		{
			child = new Element("useDirectConnection",ns);
			child.setText(String.valueOf(useDirectConnection));
			e.addContent(child);
		}
		
		child = new Element("useCompression",ns);
		child.setText(String.valueOf(useCompression));
		e.addContent(child);
		
		if (partitionConnectionNames!=null && partitionConnectionNames.length>0)
		{
			child = new Element("PartitionConnectionNames",ns);
			for (String partitionConnectionName : partitionConnectionNames)
			{
				Element stringElement = new Element("string",ns);
				stringElement.setText(partitionConnectionName);
				child.addContent(stringElement);
			}
			e.addContent(child);
		}		
		
		if (isPartitioned)
		{
			child = new Element("IsPartitioned",ns);
			child.setText(String.valueOf(isPartitioned));
			e.addContent(child);
		}
		
		return e;
	}
	
	/**
	 * We use the same driver for multiple database types and sometimes use the driver class name to get the database type.
	 * Therefore we encode the database type in the driver class name and fix it up here.
	 * @param driver
	 * @return
	 */
	public static String fixupDriverClassName(String driver)
	{
		if (driver.equals("org.postgresql.Driver.Redshift"))
		{
			return "org.postgresql.Driver";
		}
		return driver;
	}

	/**
	 * Based on partition connection names, the method finds the corresponding connections from repository and
	 * fills in partition connection array.
	 */
	public void initPartitionConnections(Repository r) throws RepositoryException
	{
		if((!isPartitioned))
		{
			return;
		}
		if(partitionConnectionNames == null || partitionConnectionNames.length == 0)
		{
			throw new RepositoryException("Partitioned flag set to true for connection \"" + Name 
					+ "\" but no partition connection names are defined.");
		}
		
		partitionConnections = new DatabaseConnection[partitionConnectionNames.length];
		for(int i = 0; i < partitionConnectionNames.length; i++)
		{
			String pcn = partitionConnectionNames[i];
			DatabaseConnection dc = r.findConnection(pcn);
			if(dc == null)
			{
				throw new RepositoryException("No partition connection found by name \"" + pcn 
						+ "\" - it is referred in partitioned connection \"" + Name +"\".");
			}
			partitionConnections[i] = dc;
		}
	}
	/**
	 * convert a string name for the database type into a enumerated value
	 * 
	 * @param type
	 */
	private void setDBType(String type)
	{
		if (type.equalsIgnoreCase("MySQL"))
		{
			DBType = DatabaseType.MYSQL;
		} else if (type.equalsIgnoreCase("MS SQL Server 2000"))
		{
			DBType = DatabaseType.MSSQL;
		} else if (type.equalsIgnoreCase("MS SQL Server 2005"))
		{
			DBType = DatabaseType.MSSQL2005;
		} else if (type.equalsIgnoreCase("MS SQL Server 2008") || type.equalsIgnoreCase("MS SQL Server 2008 R2"))
		{
			DBType = DatabaseType.MSSQL2008;
		} else if (type.equalsIgnoreCase("MS SQL Server 2012"))
		{
			DBType = DatabaseType.MSSQL2012;
		} else if (type.equalsIgnoreCase("MS SQL Server 2012 Column Store"))
		{
			DBType = DatabaseType.MSSQL2012ColumnStore;			
		} else if (type.equalsIgnoreCase("MS SQL Server 2014"))
		{
			DBType = DatabaseType.MSSQL2014;			
		} else if (type.equalsIgnoreCase("Infobright"))
		{
			DBType = DatabaseType.Infobright;
		} else if (type.equalsIgnoreCase("Microsoft Excel ODBC"))
		{
			DBType = DatabaseType.ODBCExcel;
		} else if (type.equalsIgnoreCase("Siebel Analytics ODBC"))
		{
			DBType = DatabaseType.SiebelAnalytics;
		} else if (type.equalsIgnoreCase("Google Analytics"))
		{
			DBType = DatabaseType.GoogleAnalytics;
			Password = EncryptionService.getInstance().decrypt(Password);
		} else if (type.equalsIgnoreCase("Oracle 11g") || type.equalsIgnoreCase("Oracle"))
		{
			DBType = DatabaseType.Oracle;
			Password = EncryptionService.getInstance().decrypt(Password);
		} else if (type.equalsIgnoreCase("MonetDB"))
		{
			DBType = DatabaseType.MonetDB;
		} else if (type.equalsIgnoreCase("Oracle ODBC"))
		{
			DBType = DatabaseType.ODBCOracle;
		} else if (type.equalsIgnoreCase("MySQL ODBC"))
		{
			DBType = DatabaseType.ODBCMySQL;
		} else if (type.equalsIgnoreCase("Microsoft SQL Server ODBC"))
		{
			DBType = DatabaseType.ODBCMSSQL;
		} else if (type.equalsIgnoreCase("DB2 ODBC"))
		{
			DBType = DatabaseType.ODBCDB2;
		} else if (type.equalsIgnoreCase("Microsoft Analysis Services (XMLA)"))
		{
			DBType = DatabaseType.MSASXMLA;
		} else if (type.equalsIgnoreCase("Pentaho (Mondrian) Analysis Services (XMLA)"))
		{
			DBType = DatabaseType.MONDRIANXMLA;
		} else if (type.equalsIgnoreCase("Hyperion Essbase (XMLA)"))
		{
			DBType = DatabaseType.EssbaseXMLA;
		} else if (type.equalsIgnoreCase("SAP BW (XMLA)"))
		{
			DBType = DatabaseType.SAPBWXMLA;
		} else if (type.equalsIgnoreCase("Vertica"))
		{
			DBType = DatabaseType.Vertica;	
		} else if (type.equalsIgnoreCase("PostgreSQL"))
		{
			DBType = DatabaseType.PostgreSQL;			
		} else if (type.equalsIgnoreCase("Redshift"))
		{
			DBType = DatabaseType.Redshift;			
		} else if (type.equalsIgnoreCase("Teradata"))
		{
			DBType = DatabaseType.Teradata;
		} else if (type.equalsIgnoreCase("MemDB"))
		{
			DBType = DatabaseType.MemDB;
		} else if (type.equalsIgnoreCase("Sybase IQ"))
		{
			DBType = DatabaseType.SybaseIQ;			
		} else if (type.equalsIgnoreCase("ParAccel"))
		{
			DBType = DatabaseType.ParAccel;		
		} else if (type.equals("Hive/Hadoop"))
		{
			DBType = DatabaseType.HiveHadoop;
		} else if (type.equalsIgnoreCase("Impala"))
		{
			DBType = DatabaseType.IMPALA;			
		} else if (type.equalsIgnoreCase("SAP Hana"))
		{
			DBType = DatabaseType.Hana;			
		} else
		{
			DBType = DatabaseType.None;
		}
	}
	
	public String getDBTypeNameForXMLAConnection()
	{
		switch (DBType)
		{
			case MSASXMLA : return com.birst.dataconductor.DatabaseConnection.MICROSOFT_ANALYSIS_SERVICES;
			case MONDRIANXMLA : return com.birst.dataconductor.DatabaseConnection.PENTAHO_MONDRIAN;
			case EssbaseXMLA : return com.birst.dataconductor.DatabaseConnection.HYPERIAN_ESSBASE;
			case SAPBWXMLA : return com.birst.dataconductor.DatabaseConnection.SAP_BW;
		default:
			break;
		}
		return null;
	}
	
	private void setDBTypeFromDriver()
	{
		if (Driver.equals("com.microsoft.sqlserver.jdbc.SQLServerDriver"))
		{
			DBType = DatabaseType.MSSQL;
		} else if (Driver.equals("com.sybase.jdbc4.jdbc.SybDriver"))
		{
			DBType = DatabaseType.SybaseIQ;	
		} else if (Driver.equals("com.paraccel.Driver"))
		{
			DBType = DatabaseType.ParAccel;
		} else if (Driver.equals("org.postgresql.Driver.Redshift"))
		{
			DBType = DatabaseType.Redshift;
		} else if (Driver.equals("org.postgresql.Driver"))
		{
			DBType = DatabaseType.PostgreSQL;
		} else if (Driver.equals("org.apache.hadoop.hive.jdbc.HiveDriver"))
		{
			DBType = DatabaseType.HiveHadoop;
		} else if (Driver.equals("oracle.jdbc.OracleDriver"))
		{
			DBType = DatabaseType.Oracle;
			Password = EncryptionService.getInstance().decrypt(Password);	
		} else if (Driver.equals("com.sap.db.jdbc.Driver"))
		{
			DBType = DatabaseType.Hana;			
		} else
		{
			DBType = DatabaseType.None;
		}
		logger.error("Missing database connection Type in repository for connection: " + Name + ", trying to set from driver: " + Driver + ", type set to: " + DBType);
	}

	public boolean isValid()
	{
		return DBType != DatabaseType.None;
	}
	
	public boolean useCompression()
	{
		return useCompression;
	}

	public boolean isUnicodeDatabase()
	{
		if (DBType == DatabaseType.MonetDB || isDBTypeTeradata(DBType))
			return false;
		return unicodeDatabase;
	}

	public String decodeDatabase()
	{
		int index = ConnectString.toLowerCase().indexOf("databasename=");
		if (index < 0)
			return null;
		int index1 = ConnectString.indexOf('=', index) + 1;
		int index2 = ConnectString.indexOf(';', index);
		if (index2 >= 0)
			return ConnectString.substring(index1, index2);
		return ConnectString.substring(index1);
	}
	
	/**
	 * returns TimeZone of Database server
	 * @return
	 */
	public String getDBTimeZone(){
		if(DBTimeZone != null)
			return DBTimeZone.trim();
		else{
			return DBTimeZone;
		}
	}
	
	/**
	 * most of the stuff in this file should eventually be driven by a configuration file / database
	 */

	/**
	 * return the name of the decimal digits column in the JDBC meta data - should always be DECIMAL_DIGITS, but LucidDB
	 * has a different name
	 * 
	 * @return
	 */
	public String getDecDigitsName()
	{
		return "DECIMAL_DIGITS";
	}

	private String columnQuoteLeft()
	{
		if (DatabaseConnection.isDBTypeMSSQL(DBType))
			return "[";
		if (DBType == DatabaseType.Infobright || DBType == DatabaseType.MYSQL || DBType == DatabaseType.MonetDB
				|| DBType == DatabaseType.Oracle || DBType == DatabaseType.MemDB || DatabaseConnection.isDBTypeParAccel(DBType) || DBType == DatabaseType.Hana)
			return "";
		return "'";
	}

	private String columnQuoteRight()
	{
		if (DatabaseConnection.isDBTypeMSSQL(DBType))
			return "]";
		if (DBType == DatabaseType.Infobright || DBType == DatabaseType.MYSQL || DBType == DatabaseType.MonetDB
				|| DBType == DatabaseType.Oracle || DBType == DatabaseType.MemDB || DatabaseConnection.isDBTypeParAccel(DBType) || DBType == DatabaseType.Hana)
			return "";
		return "'";	
	}
	
	/**
	 * quote a column name - allows reserved words and special characters to be used in names
	 * 
	 * @param column
	 * @return
	 */
	public String getQuotedColumn(String column)
	{
		return columnQuoteLeft() + column + columnQuoteRight();
	}

	/**
	 * XXX why doesn't this use getQuoteColumn for the column?
	 * 
	 * @return
	 */
	public String getQualifiedColumn(String table, String column)
	{
		if (DBType == DatabaseType.ODBCExcel)
			return table + ".[" + column + "]";
		else
			return table + "." + column;
	}
	
	public boolean lowerCaseMetadataQueryParameters()
	{
		if (isDBTypeParAccel(DBType))
			return true;
		return false;
	}
	
	public boolean upCaseMetadataQueryParameters()
	{
		if (isDBTypeOracle(DBType) || DBType == DatabaseType.Hana)
			return true;
		return false;
	}

	/**
	 * return a SQL table reference
	 * 
	 * @param dbType
	 * @param name
	 * @return StringBuilder instance
	 */
	public String getTableReference(String name, boolean isSubQuery)
	{
		StringBuilder plist = new StringBuilder();
		if (DBType == DatabaseType.ODBCExcel || isDBTypeHiveVariant(DBType) )
		{
			plist.append(" AS ");
			plist.append(Util.replaceWithPhysicalString(name)); // name is not quoted, so we need to clean it up
		} else if (DBType == DatabaseType.SiebelAnalytics)
		{
			plist.append(" \"");
			plist.append(name);
			plist.append('\"');
		} else if (isDBTypeOracle(DBType) || isDBTypeDB2(DBType) || isDBTypePostgreSQLVariant(DBType) || isDBTypeTeradata(DBType) || isDBTypeParAccel(DBType) || DBType == DatabaseType.Hana)
		{
			if (isSubQuery)
			{
				plist.append(" AS ");
				plist.append(name);
			} else
			{
				plist.append(" AS \"");
				plist.append(name);
				plist.append('\"');
			}
		} else
		{
			plist.append(" AS '");
			plist.append(name);
			plist.append('\'');
		}
		return plist.toString();
	}

	/**
	 * XXX how does this differ from getQuotedColumn
	 * 
	 * @param name
	 * @return
	 */
	public String getColumnName(String name)
	{
		return getColumnName(name, false);
	}

	public String getColumnName(String name, boolean noQuote)
	{
		if (isDBTypeHiveVariant(DBType))
			return name;
		if (DBType == DatabaseType.SiebelAnalytics)
		{
			return "\"" + name + "\"";
		} else if (isDBTypeOracle(DBType) || isDBTypeDB2(DBType) || isDBTypePostgreSQLVariant(DBType) || isDBTypeTeradata(DBType) || isDBTypeParAccel(DBType) || isDBTypeSAPHana(DBType))
		{
			if (noQuote)
				return name;
			else
				return "\"" + name + "\"";
		} else
			return "'" + name + "'";
	}

	/**
	 * return a literal with quotes, mainly for Excel dates
	 * 
	 * @param literal
	 * @param dataType
	 * @return
	 */
	public String getQuotedLiteral(String literal, String dataType)
	{
		char quote = '\'';
		if (isDBTypeTeradata(DBType))
		{
			if (dataType.equals("Date"))
				return "DATE " + quote + literal + quote;
			if (dataType.equals("DateTime"))
				return "TIMESTAMP " + quote + literal + quote;
		} else if ((DBType == DatabaseType.ODBCExcel || DBType == DatabaseType.MemDB) && dataType.equals("DateTime"))
		{
			quote = '#'; // Excel (Jet engine) timestamps are quoted with '#'
		} else if (isDBTypeOracle(DBType) && (dataType.equals("Date") || dataType.equals("DateTime")))
		{
			return "TO_TIMESTAMP(" + quote +  literal + quote + ",'yyyy-MM-dd hh24:mi:ss.ff3')";
		}
		return quote + literal + quote;
	}
	
	/**
	 * return a literal with N character for non-ASCII comparison, mainly for MSSQL
	 * 
	 * @param literal
	 * @param dataType
	 * @return
	 */
	public String getAsciiLiteralOperand(String literal, String dataType)
	{
		String asciiString = literal;
		if (isDBTypeMSSQL(DBType))
		{
			if (dataType.equals("Varchar"))
			{
				asciiString = 'N' + asciiString;
			}
		}
		return asciiString;
	}

	/**
	 * return a literal with quotes, mainly for Excel dates
	 * 
	 * @param literal
	 * @param dataType
	 * @return
	 */
	public String escapeLiteral(String literal)
	{
		String str = Util.unquote(literal);
		if (isDBTypeMemDB(DBType))
		{
			return Util.replaceStr(str, "'", "\\'");
		}
		return Util.replaceStr(str, "'", "''");
	}

	/**
	 * return the SQL syntax for a full join - note that MySQL does not support full join and our use of cross join is
	 * wrong XXX
	 * 
	 * @return
	 */
	public String getFullJoin()
	{
		if (isDBTypeMySQL(DBType))
			return "CROSS JOIN"; // XXX note that this is wrong, CROSS JOIN == INNER JOIN in MySQL
		else if (isDBTypeHiveVariant(DBType) || isDBTypePostgreSQLVariant(DBType))
			return "FULL OUTER JOIN";
		else
			return "FULL JOIN";
	}
	
	public String getInnerJoin()
	{
		if (isDBTypeHiveHadoop(DBType))
			return "JOIN";
		else
			return "INNER JOIN";
	}

	public boolean requiresTransactionID()
	{
		if (DBType == DatabaseType.Infobright)
			return true;
		return false;
	}

	/*
	 * Physical name suffix
	 */
	public String getPhysicalSuffix()
	{
		if (DBType == DatabaseType.MonetDB)
			return "_SMI";
		return "$";
	}

	/**
	 * return the current date
	 * 
	 * @return
	 */
	public String getGetDate()
	{
		if (DBType == DatabaseType.MonetDB)
			return "CURRENT_TIMESTAMP";
		else if (DBType == DatabaseType.MYSQL || DBType == DatabaseType.Infobright)
			return "curdate()";
		else if (isDBTypeTeradata(DBType) || isDBTypeOracle(DBType) || DBType == DatabaseType.Hana)
			return "current_date";
		else if (DBType == DatabaseType.MemDB)	// XXX this scalar should have been implemented in MemDB so that the base system does not have to change
		{
			if (sdf == null)
				sdf = new SimpleDateFormat();
			return "'" + sdf.format(new Date()) + "'";
		}
		return "getdate()";
	}

	/**
	 * return the current date and time
	 * 
	 * @return
	 */
	public String getCurrentTimestamp()
	{
		if (isDBTypeMySQL(DBType) || DBType == DatabaseType.MonetDB)
			return "current_timestamp()";
		else if (isDBTypeTeradata(DBType) || isDBTypeOracle(DBType) || DBType == DatabaseType.Hana)
			return "current_timestamp";
		else if (DBType == DatabaseType.MemDB) // XXX this scalar should have been implemented in MemDB so that the base system does not have to change -- doing it here also has time skew issues
		{
			if (sdf == null)
				sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS z");
			return "'" + sdf.format(new Date()) + "'";
		}
		return "getdate()";
	}

	public String getNVarCharType()
	{
		if (DBType == DatabaseType.Oracle)
			return "NVARCHAR2";
		return "NVARCHAR";
	}

	/**
	 * DOUBLE / FLOAT
	 * - REAL is generally the smaller version
	 * @return
	 */
	public String getFloatType()
	{
		if (DBType == DatabaseType.Oracle)
			return "FLOAT(49)";
		return "FLOAT";
	}

	public String getBigIntType()
	{
		if (DBType == DatabaseType.Oracle)
			return "NUMBER(19)";
		return "BIGINT";
	}

	/**
	 * return a SQL type that has both DATE and TIME
	 * 
	 * @return
	 */
	public String getDateTimeType()
	{
		if (DBType == DatabaseType.MonetDB || isDBTypeTeradata(DBType) || DBType == DatabaseType.Oracle || DatabaseConnection.isDBTypeParAccel(DBType))
			return "TIMESTAMP";
		else if (DBType == DatabaseType.Hana)
			return "SECONDDATE"; // Hana TIMESTAMP has nanoseconds in it
		return "DATETIME";
	}

	/**
	 * return a SQL type that has DATE, for backwards compatibility we return DATETIME for MS SQL Server 2005/2008
	 * 
	 * @return
	 */
	public String getDateType()
	{
		if (DBType == DatabaseType.MSSQL2012 || DBType == DatabaseType.MSSQL2012ColumnStore || DBType == DatabaseType.MSSQL2014)
			return "DATE";
		if (DatabaseConnection.isDBTypeMSSQL(DBType)) // historical
			return "DATETIME";
		return "DATE";
	}

	private static Calendar MIN_DATETIME = new GregorianCalendar(1, 0, 1, 0, 0, 0);					// 0001-01-01
	private static Calendar MYSQL_MIN_DATETIME = new GregorianCalendar(1000, 0, 1, 0, 0);			// 1000-01-01
	private static Calendar SQL_SERVER_MIN_DATETIME = new GregorianCalendar(1753, 0, 1, 0, 0, 0);	// 1753-01-01
	private static Calendar MAX_DATETIME = new GregorianCalendar(9999, 11, 31, 23, 59, 59);			// 9999-12-31

	public Calendar getMinDatetime()
	{
		if (isDBTypeMSSQL(DBType))
			return SQL_SERVER_MIN_DATETIME;
		else if (DatabaseConnection.isDBTypeMySQL(DBType))
			return MYSQL_MIN_DATETIME;
		return MIN_DATETIME; // this is okay for MS 2012 since we use DATETIME2 (XXX but not for the bulkloader)
	}
	
	public Calendar getMaxDatetime()
	{
		return MAX_DATETIME;
	}

	/**
	 * return the default import/export format for DATETIME
	 * 
	 * @return
	 */
	public SimpleDateFormat getOutputDateTimeFormat()
	{
		if (DBType == DatabaseType.Infobright || DBType == DatabaseType.MYSQL || DBType == DatabaseType.MonetDB 
				|| DBType == DatabaseType.Teradata || DatabaseConnection.isDBTypeParAccel(DBType) || DBType == DatabaseType.Hana)
			return new SimpleDateFormat("yyyy-MM-dd H:mm:ss");
		else if (DBType == DatabaseType.Oracle)
			return new SimpleDateFormat("dd-MMM-yyyy h:mm:ss.SSS a");
		else
			return new SimpleDateFormat("MM/dd/yyyy h:mm:ss.SSS a");
	}
	
	public String getOutputDateTimeString(String literal)
	{
		if (isDBTypeOracle(DBType))
			return "CAST('" + literal + "'" + " AS " + getDateTimeType() + ")";  
		return "'" + literal + "'";
	}

	/**
	 * return the default import/export format for DATE
	 * 
	 * @return
	 */
	public SimpleDateFormat getOutputDateFormat()
	{
		if (DBType == DatabaseType.Infobright || DBType == DatabaseType.MYSQL || DBType == DatabaseType.MonetDB 
				|| DBType == DatabaseType.Teradata || DatabaseConnection.isDBTypeParAccel(DBType) || DBType == DatabaseType.Hana)
			return new SimpleDateFormat("yyyy-MM-dd");
		else if (DBType == DatabaseType.Oracle)
			return new SimpleDateFormat("dd-MMM-yyyy");
		else
			return new SimpleDateFormat("MM/dd/yyyy");
	}

	/**
	 * does the RDBMS support bulk load
	 * 
	 * @return
	 */
	public boolean supportsBulkLoad()
	{
		if (DatabaseConnection.isDBTypeMSSQL(DBType) || DBType == DatabaseType.Infobright || DBType == DatabaseType.MYSQL
				|| DBType == DatabaseType.MonetDB || DBType == DatabaseType.Oracle || DBType == DatabaseType.MemDB
				|| DatabaseConnection.isDBTypeParAccel(DBType)
				|| DBType == DatabaseType.Hana)
			return true;
		return false;
	}
	
	public boolean supportsQueryTimeOut()
	{
		if (DatabaseConnection.isDBTypeParAccel(DBType))
			return false;
		return true;
	}

	/**
	 * return a type that works for our use of dimensional keys
	 * 
	 * @return
	 */
	public String getDimensionalKeyType()
	{
		return getDimensionalKeyType(null);		
	}
	
	public String getDimensionalKeyType(Repository repository)
	{
			if (DBType == DatabaseType.Infobright || DBType == DatabaseType.MemDB || isDBTypeParAccel(DBType) || DBType == DatabaseType.Hana)
				return "BIGINT";
			else if (DBType == DatabaseType.Oracle)
				return "NUMBER(38,0)";
			return ((repository != null && repository.getCurrentBuilderVersion() >= Repository.IntToLongBuilderVersion) ? (DBType == DatabaseType.Oracle ? "NUMBER(38,0)" : "BIGINT") : "INTEGER");	
	}
	
	public String getIntegerType(Repository repository)
	{
		return repository.getCurrentBuilderVersion() >= Repository.IntToLongBuilderVersion ? (DBType == DatabaseType.Oracle ? "NUMBER(38,0)" : "BIGINT") : "INTEGER";	
	}

	/**
	 * does the RDBMS support partitioning
	 * - XXX not actually used for anything
	 * 
	 * @return
	 */
	public boolean supportsPartitioning()
	{
		if (DatabaseConnection.isDBTypeMSSQL(DBType))
			return true;
		return false;
	}

	/**
	 * does the RDBMS support identity columns
	 * 
	 * @return
	 */
	public boolean supportsIdentity()
	{
		if (DBType == DatabaseType.Infobright || DBType == DatabaseType.Oracle || DBType == DatabaseType.Hana)
			return false;
		return true;
	}
	
	/**
	 * for those databases that do not support identity, they might support sequences and we can use those in place of identity columns
	 * @return
	 */
	public boolean useSequenceForIdentity()
	{
		if (DBType == DatabaseType.Oracle || DBType == DatabaseType.Hana)
			return true;
		return false;
	}
	
	/**
	 * Birst generates INSERT INTO table (table.col1, table.col2, ...)
	 * - why, not clear, but some databases can't handle table. in the column list
	 * @return
	 */
	public boolean stripTableNameFromInsertColumnList()
	{
		if (DBType == DatabaseType.MonetDB || DBType == DatabaseType.MYSQL || DBType == DatabaseType.Infobright || DatabaseConnection.isDBTypeParAccel(DBType) || DBType == DatabaseType.Hana)
			return true;
		return false;
	}
	
	/**
	 * does the RDBMS support LIMIT rather than TOP - select ... from ... LIMIT NNN - select TOP NNN ... from ...
	 * 
	 * @return
	 */
	public boolean supportsLimit()
	{
		if (DBType == DatabaseType.Infobright || isDBTypeMySQL(DBType) || DBType == DatabaseType.MonetDB
				|| isDBTypePostgreSQLVariant(DBType) || isDBTypeHiveVariant(DBType) || isDBTypeParAccel(DBType) || DBType == DatabaseType.Hana)
			return true;
		return false;
	}

	public boolean supportsRowNum()
	{
		if (isDBTypeOracle(DBType))
			return true;
		return false;
	}

	public boolean supportsFetchFirstRows()
	{
		if (isDBTypeDB2(DBType))
			return true;
		return false;
	}

	public boolean supportsTop()
	{
		if (isDBTypeMSSQL(DBType) || isDBTypeTeradata(DBType) || isDBTypeSybaseIQ(DBType) || isDBTypeMemDB(DBType))
			return true;
		return false;
	}
	
	/*
	 * XXX remove this once IB gets it's act together (release 4.0 of IB failed with LIMIT 0)
	 */
	public boolean supportsLimit0()
	{
		if (isDBTypeInfoBright(DBType))
			return false;
		return true;
	}
	
	/*
	 * Impala requires a LIMIT on all ORDER BY operations
	 */
	public boolean requiresLimitOnOrderBy()
	{
		if (isDBTypeImpala(DBType))
			return true;
		return false;
	}
	
	/*
	 * initial guess at a reasonable limit for the ORDER BY
	 */
	public int getOrderByLimit()
	{
		return 10000000; 
	}

	/**
	 * return a literal that can be used for null in bulk loads
	 * 
	 * @return
	 */
	public String getBulkLoadNull()
	{
		if (DBType == DatabaseType.Infobright || DBType == DatabaseType.MYSQL || DatabaseConnection.isDBTypeParAccel(DBType))
			return "\\N";
		return "";
	}

	/**
	 * return the SQL scalar function for checking for null
	 * 
	 * @return
	 */
	public String getIsNullScalarFunction()
	{
		if (DBType == DatabaseType.Infobright || DBType == DatabaseType.MYSQL || DBType == DatabaseType.Hana)
			return "IFNULL";
		else if (isDBTypeTeradata(DBType))
			return "COALESCE";
		else if (isDBTypeOracle(DBType))
			return "NVL";
		return "ISNULL";
	}

	/**
	 * return the default numeric type for Birst (Infobright has a limit on the number of digits)
	 * 
	 * @return
	 */
	public String getNumericType()
	{
		if (DBType == DatabaseType.Infobright || DBType == DatabaseType.MSSQL2012ColumnStore || DBType == DatabaseType.MSSQL2014)
			return getNumericType(18, 5);
		else if (DBType == DatabaseType.MonetDB)
			return getNumericType(18, 4);
		return getNumericType(19, 6); // XXX hana can go up to 34 for precision, but no reason to use that much space...
	}

	/**
	 * return an INTEGER type that can be used in CAST operations
	 * 
	 * @return
	 */
	public String getCastableIntegerType()
	{
		if (DBType == DatabaseType.Infobright || DBType == DatabaseType.MYSQL)
			return "SIGNED INTEGER";
		return "INTEGER";
	}

	/**
	 * return an FLOAT type that can be used in CAST operations
	 * 
	 * @return
	 */
	public String getCastableFloatType()
	{
		if (isDBTypeTeradata(DBType))
			return "FLOAT";
		return "DOUBLE";
	}

	/**
	 * return an Varchar type that can be used in CAST operations
	 * 
	 * @return
	 */
	public String getCastableCharType()
	{
		if (DBType == DatabaseType.Infobright || DBType == DatabaseType.MYSQL)
			return "CHAR";
		return "VARCHAR";
	}

	public boolean turnOffAutoCommit()
	{
		if (DBType == DatabaseType.Infobright)
			return true;
		return false;
	}

	/**
	 * return the SQL type for fixed precision numbers
	 * 
	 * @param scale
	 * @param precision
	 * @return
	 */
	public String getNumericType(int scale, int precision)
	{
		if (DBType == DatabaseType.MemDB)
			return "DECIMAL";
		if (DBType == DatabaseType.Infobright)
			return "DECIMAL(" + scale + "," + precision + ")";
		return "NUMERIC(" + scale + "," + precision + ")";
	}

	/**
	 * return the SQL type for very large text fields
	 * 
	 * @return
	 */
	public String getLargeTextType()
	{
		if (DBType == DatabaseType.MSSQL2012ColumnStore || DBType == DatabaseType.MSSQL2014)
		{
			if (isUnicodeDatabase())
				return "NVARCHAR(4000)";
			return "VARCHAR(8000)"; // VARCHAR(MAX) can not be used in column store indexes
		}
		else if (DBType == DatabaseType.Infobright || DBType == DatabaseType.MYSQL)
		{
			return "TEXT";
		}
		else if (isDBTypeTeradata(DBType))
		{
			return "CLOB";
		}
		else if (DBType == DatabaseType.Hana)
		{
			if (isUnicodeDatabase())
				return "NCLOB";
			else
				return "CLOB";
		}
		else if (isDBTypeOracle(DBType)) //cant use CLOB - Issue 1 Not able to load record into warehouse 
		    							 //Issue 2 Restrictions on LOBs - Cannot be used in the following part of a SQL statement: GROUP BY, ORDER BY, SELECT DISTINCT, joins, aggregate functions 
		{
			if (isUnicodeDatabase())
				return "NVARCHAR2(2000)";
			else
				return "VARCHAR2(4000)";
		}
		else if (DatabaseConnection.isDBTypeParAccel(DBType))
			return "VARCHAR(4096)";
		else if (DBType == DatabaseType.MemDB)
			return "VARCHAR(10000)";
		else if (isUnicodeDatabase())
			return "NVARCHAR(MAX)";
		return "VARCHAR(MAX)";
	}
	
	public String getIdentityDataType()
	{
		if (DBType == DatabaseType.Oracle)
			return "NUMBER(38,0)";
		return "BIGINT";
	}

	/**
	 * get a varchar type using the width
	 */
	public String getVarcharType(int width, boolean useMax)
	{
		if (width > getMaxVarcharWidth() && useMax)
		{
			return getLargeTextType();
		} else
		{
			if (width == 0)
				width = 100;
			if (isUnicodeDatabase())
			{
				if (DatabaseConnection.isDBTypeParAccel(DBType))
					return "VARCHAR(" + 2 * width + ")";
				if (DBType == DatabaseType.MemDB)
					return "VARCHAR(" + width + ")";
				if (DBType == DatabaseType.Oracle)
					return "NVARCHAR2(" + width + ")";
				return "NVARCHAR(" + width + ")";
			}
			if (DBType == DatabaseType.Oracle)
				return "VARCHAR2(" + width + ")";
			return "VARCHAR(" + width + ")";
		}
	}

	/**
	 * return the encoding type for bulk load files
	 * 
	 * @return
	 */
	public String getBulkLoadCharacterEncoding()
	{
		if (DatabaseConnection.isDBTypeParAccel(DBType) || DBType == DatabaseType.MemDB || DBType == DatabaseType.Hana) // XXX not sure about Hana here
			return "UTF-8";
		if (isUnicodeDatabase() && DBType != DatabaseType.Infobright && DBType != DatabaseType.MYSQL && DBType != DatabaseType.MemDB)
		{
			return "Unicode";
		}
		if (DBType == DatabaseType.MonetDB)
			return "US-ASCII";// "UTF8";
		if (isUnicodeDatabase() && DBType == DatabaseType.Infobright)
		{
			if (IBLoadWarehouseUsingUTF8)
				return "UTF-8";
		}
		return "iso-8859-1";
	}
	
	/**
	 * ParAccel support bulk loading of GZIP files
	 * @return
	 */
	public boolean supportsZippedDataForBulkLoading()
	{
		if (DBType == DatabaseType.ParAccel || DBType == DatabaseType.Redshift)
			return true;
		return false;
	}
	
	public String getUploadFilename(String tempFullFilename)
	{
		String filename = tempFullFilename + ".tmp";
		if (supportsZippedDataForBulkLoading())
			filename += ".gz";
		return filename;
	}

	/**
	 * SQL Server uses a different UPDATE syntax when updating from a query containing multiple tables
	 * 
	 * @return
	 */
	public boolean useWatcomOracleUpdateSyntax()
	{
		if (DBType == DatabaseType.Infobright || DBType == DatabaseType.MYSQL || DBType == DatabaseType.MonetDB)
			return true;
		return false;
	}

	/**
	 * does the RDBMS support indexes (column stores do not)0
	 * 
	 * @return
	 */
	public boolean supportsIndexes()
	{
		if (DBType == DatabaseType.Infobright || DBType == DatabaseType.MonetDB || DBType == DatabaseType.MemDB 
				|| DatabaseConnection.isDBTypeParAccel(DBType) || DBType == DatabaseType.MSSQL2012ColumnStore || DBType == DatabaseType.MSSQL2014 ||DBType == DatabaseType.Hana)
			return false;
		return true;
	}

	/**
	 * does the RDBMS support clustered indexes
	 * 
	 * @return
	 */
	public boolean supportsClusteredIndexes()
	{
		if (supportsIndexes())
		{
			if (DatabaseConnection.isDBTypeMSSQL(DBType))
			{
				return true;
			}
		}
		return false;
	}
	
	public int getInputScriptFetchSize()
	{
		// set the fetch size
		if (DBType == DatabaseType.Infobright || DBType == DatabaseType.MYSQL)
			return Integer.MIN_VALUE;	// Value to set to force streaming for MySQL
		else if (DBType == DatabaseType.Redshift)
			return 25000; // http://docs.aws.amazon.com/redshift/latest/dg/jdbc-fetch-size-parameter.html
		else
			return 2500;
	}

	/**
	 * does the RDBMS support index includes
	 * 
	 * @return
	 */
	public boolean supportsIndexIncludes()
	{
		if (supportsIndexes())
		{
			if (DatabaseConnection.isDBTypeMSSQL(DBType))
			{
				return true;
			}
		}
		return false;
	}

	public boolean supportsFullOuterJoin()
	{
		if (DBType == DatabaseType.Infobright || isDBTypeMySQL(DBType))
			return false;
		return true;
	}

	public static int getIdentifierLength(Database.DatabaseType dbType)
	{
		if (dbType == DatabaseType.Infobright || isDBTypeMySQL(dbType))
			return 64;
		else if (isDBTypeOracle(dbType))
			return 30;
		else if (dbType == DatabaseType.Hana)
			return 127;
		return 128; // SQL Server default
	}

	/**
	 * this routine is bad, but since we already use it and the risk is low, we can leave it as is...
	 * 
	 * From findbugs:
	 * 
	 * Bad attempt to compute absolute value of signed 32-bit hashcode
	 * 
	 * This code generates a hashcode and then computes the absolute value of that hashcode. 
	 * If the hashcode is Integer.MIN_VALUE, then the result will be negative as well (since Math.abs(Integer.MIN_VALUE) == Integer.MIN_VALUE).
	 * One out of 2^32 strings have a hashCode of Integer.MIN_VALUE, including "polygenelubricants" "GydZG_" and ""DESIGNING WORKHOUSES".
	 * 
	 * @param s
	 * @return
	 */
	public String getHashedIdentifier(String s)
	{
		return getHashedIdentifier(s, getIdentifierLength(DBType));
	}
	
	public String getHashedIdentifier(String s, int length)
	{
		if (s.length() > length)
		{
			if (DBType == DatabaseType.Oracle)
				s = ("X" + Math.abs(s.hashCode())).toUpperCase();
			else
				s = "x" + Math.abs(s.hashCode());
		}
		return s;
	}
	
	public String getColumnStoreIndexName(String tableName)
	{
		return getHashedIdentifier("CS_" + tableName);
	}
	
	public String getChecksumName(String name)
	{
		if (DBType == DatabaseType.Infobright || DBType == DatabaseType.MYSQL)
		{
			if (name.length() > 61)
			{
				name = "x" + Integer.toHexString(name.hashCode());
			}
		}
		else if (DBType == DatabaseType.Oracle)
		{
			if (name.length() > 27)
			{
				name = "x" + Integer.toHexString(name.hashCode());
			}
			name = name.toUpperCase();
		}

		return name;
	}

	public static boolean supportsBeginUnderScore(DatabaseType dbType)
	{
		if (isDBTypeOracle(dbType) || isDBTypeDB2(dbType))
			return false;
		return true;
	}
	
	public static boolean isDBTypeSybaseIQ(DatabaseType dbType)
	{
		if (dbType == DatabaseType.SybaseIQ)
			return true;
		return false;
	}

	public static boolean isDBTypeOracle(DatabaseType dbType)
	{
		if (dbType == DatabaseType.Oracle || dbType == DatabaseType.ODBCOracle)
			return true;
		return false;
	}

	public static boolean isDBTypeMySQL(DatabaseType dbType)
	{
		if (dbType == DatabaseType.MYSQL || dbType == DatabaseType.ODBCMySQL || dbType == DatabaseType.Infobright)
			return true;
		return false;
	}

	public static boolean isDBTypeInfoBright(DatabaseType dbType)
	{
		if (dbType == DatabaseType.Infobright)
			return true;
		return false;
	}

	public static boolean isDBTypeMSSQL(DatabaseType dbType)
	{
		if (dbType == DatabaseType.MSSQL || dbType == DatabaseType.MSSQL2005 || dbType == DatabaseType.MSSQL2008 || dbType == DatabaseType.ODBCMSSQL
				|| dbType == DatabaseType.MSSQL2012 || dbType == DatabaseType.MSSQL2012ColumnStore || dbType == DatabaseType.MSSQL2014)
			return true;
		return false;
	}
	
	public static boolean isDBTypeSAPHana(DatabaseType dbType)
	{
		if (dbType == DatabaseType.Hana)
			return true;
		return false;
	}
	
	public static boolean supportsIdentityInsert(DatabaseType dbType)
	{
		if (isDBTypeMSSQL(dbType))
			return true;
		return false;
	}
	
	public static boolean recreateTableOnAlteringToIdentity(DatabaseType dbType)
	{
		if (DatabaseConnection.isDBTypeMSSQL(dbType) || DatabaseConnection.isDBTypeParAccel(dbType))
			return true;
		return false;
	}

	public static boolean isDBTypeDB2(DatabaseType dbType)
	{
		if (dbType == DatabaseType.ODBCDB2)
			return true;
		return false;
	}

	public static boolean isDBTypeODBC(DatabaseType dbType)
	{
		if (dbType == DatabaseType.ODBCDB2 || dbType == DatabaseType.ODBCMSSQL || dbType == DatabaseType.ODBCMySQL || dbType == DatabaseType.ODBCOracle)
			return true;
		return false;
	}

	public static boolean isDBTypeVertica(DatabaseType dbType)
	{
		if (dbType == DatabaseType.Vertica)
			return true;
		return false;
	}
	
	public static boolean isDBTypePostgreSQL(DatabaseType dbType)
	{
		if (dbType == DatabaseType.PostgreSQL)
			return true;
		return false;
	}
	
	public static boolean isDBTypePostgreSQLVariant(DatabaseType dbType)
	{
		if (isDBTypeVertica(dbType) || isDBTypePostgreSQL(dbType))
			return true;
		return false;
	}	

	public static boolean isDBTypeTeradata(DatabaseType dbType)
	{
		if (dbType == DatabaseType.Teradata)
			return true;
		return false;
	}
	
	/**
	 * for most situations, ParAccel and Redshift are the same
	 * - there are a few issues related to bulk loading and column encoding/compression that are different
	 * @param dbType
	 * @return
	 */
	public static boolean isDBTypeParAccel(DatabaseType dbType)
	{
		if (dbType == DatabaseType.ParAccel || dbType == DatabaseType.Redshift)
			return true;
		return false;
	}
	
	public static boolean isDBTypeHiveHadoop(DatabaseType dbType)
	{
		if (dbType == DatabaseType.HiveHadoop)
			return true;
		return false;
	}
	
	public static boolean isDBTypeImpala(DatabaseType dbType)
	{
		if (dbType == DatabaseType.IMPALA)
			return true;
		return false;
	}
	
	public static boolean isDBTypeHiveVariant(DatabaseType dbType)
	{
		if (dbType == DatabaseType.IMPALA || dbType == DatabaseType.HiveHadoop)
			return true;
		return false;
	}

	public static boolean isDBTypeMemDB(DatabaseType dbType)
	{
		if (dbType == DatabaseType.MemDB)
			return true;
		return false;
	}

	public static boolean isDBTypeXMLA(DatabaseType dbType)
	{
		if (dbType == DatabaseType.MSASXMLA || dbType == DatabaseType.EssbaseXMLA || dbType == DatabaseType.SAPBWXMLA || dbType == DatabaseType.MONDRIANXMLA)
			return true;
		return false;
	}
	
	public boolean supportsColumnCompression()
	{
		if (DatabaseConnection.isDBTypeParAccel(DBType))
			return true;
		return false;
	}
	
	public boolean supportsMSSQLDBCompression()
	{
		if (DBType == DatabaseType.MSSQL2008 || DBType == DatabaseType.MSSQL2012 || DBType == DatabaseType.MSSQL2014)
			return true;
		return false;
	}
	
	public boolean supportsAlterTableModifyColumn()
	{
		if (DatabaseConnection.isDBTypeParAccel(DBType))
			return false;
		return supportsAlterTable();
	}

	public boolean supportsAlterTable()
	{
		// due to ugly locking issues between ALTER TABLE and getting catalog meta data for SQL Server, we are saying that we don't support alter table (does it manually)
		if (isDBTypeMSSQL(DBType))
			return false;
		if (DBType == DatabaseType.Infobright)
			return false;
		//Modify column across datatype requires temp table approach
		if (DBType == DatabaseType.Oracle) 
			return false;
		if (DBType == DatabaseType.Hana) 
			return false;
		return true;
	}

	public boolean supportsRenameTable()
	{
		return true;
	}
	
	public boolean supportsAlterRename()
	{
		return (DBType == DatabaseType.Oracle || DatabaseConnection.isDBTypeParAccel(DBType));
	}

	public static boolean requiresEnclosingJoinClause(DatabaseType dbType)
	{
		if (isDBTypeMySQL(dbType) || isDBTypeInfoBright(dbType) || isDBTypeSybaseIQ(dbType))
			return true;
		return false;
	}
	
	public static boolean requiresRedundantJoinCondition(DatabaseType dbType)
	{
		if (DatabaseConnection.isDBTypeMSSQL(dbType) || DatabaseConnection.isDBTypeOracle(dbType) || DatabaseConnection.isDBTypeParAccel(dbType) || DatabaseConnection.isDBTypeSAPHana(dbType))
			return true;
		return false;
	}

	public boolean requiresQuotedIdentifier()
	{
		if (isDBTypeOracle(DBType) || isDBTypeDB2(DBType) || isDBTypePostgreSQLVariant(DBType) || isDBTypeTeradata(DBType))
			return true;
		return false;
	}
	
	public boolean requiresHashedIdentifierForColumnAlias()
	{
		if (isDBTypeOracle(DBType) || isDBTypeMySQL(DBType))
			return true;
		return false;
	}
	
	public boolean requiresHashedIdentifier()
	{
		if (isDBTypeOracle(DBType) || isDBTypeMySQL(DBType))
			return true;
		return false;
	}

	public String getColumnAliasStartStr()
	{
		if (isDBTypeOracle(DBType) || isDBTypeDB2(DBType) || isDBTypePostgreSQLVariant(DBType) || isDBTypeTeradata(DBType)
				|| isDBTypeParAccel(DBType) || isDBTypeHiveVariant(DBType) || DBType == DatabaseType.Hana)
			return " AS ";
		return "";
	}

	public boolean isFederateOuterjoinSubqueries()
	{
		return DBType == DatabaseType.MYSQL || DBType == DatabaseType.Infobright;
	}

	public static String getStrToNumberMDXFunction(DatabaseType dbType)
	{
		if (dbType == DatabaseType.MSASXMLA || dbType == DatabaseType.SAPBWXMLA || dbType == DatabaseType.MONDRIANXMLA)
			return "StrToValue";
		else
			return "StrTONum";
	}
	
	public static String getMemberValueMDXFunction(DatabaseType dbType)
	{
		if (dbType == DatabaseType.MONDRIANXMLA)
			return "Value";
		else
			return "MemberValue";
	}
	
	
	public TempTable getTempTableForQueryFilter(Repository r, DimensionColumn dc, long x, List<TempTable> ttlist, String replacestr)
	{
		String pquery = Util.replaceStr(replacestr, "SELECT ", "SELECT DISTINCT ");
		// Re-use a temp table if already there
		for (TempTable stt : ttlist)
		{
			if (stt.query.endsWith(pquery))
			{
				return stt;
			}
		}
		TempTable tt = new TempTable();
		tt.tableName = Schema + ".birstx_temp" + x + "" + Thread.currentThread().getId() + "" + ttlist.size();
		String pname = dc.PhysicalName;
		if (dc.Qualify)
		{
			int index = dc.PhysicalName.indexOf('.');
			if (index >= 0)
				pname = dc.PhysicalName.substring(index + 1);
		} else
		{
			pname = Util.replaceWithNewPhysicalString(dc.ColumnName, this,r);
		}
		if (dc.DataType.equals("Varchar"))
			tt.create = "CREATE TABLE " + tt.tableName + " (" + pname + " " + getVarcharType(dc.Width, DBType != DatabaseType.MonetDB) + ")";
		else if (dc.DataType.equals("Integer"))
			tt.create = "CREATE TABLE " + tt.tableName + " (" + pname + " " + getIntegerType(r) + ")";
		else if (dc.DataType.equals("Float") || dc.DataType.equals("Number"))
			tt.create = "CREATE TABLE " + tt.tableName + " (" + pname + " " + getCastableFloatType() + ")";
		else if (dc.DataType.equals("Date"))
			tt.create = "CREATE TABLE " + tt.tableName + " (" + pname + " " + getDateType() + ")";
		else if (dc.DataType.equals("DateTime"))
			tt.create = "CREATE TABLE " + tt.tableName + " (" + pname + " " + getDateTimeType() + ")";
		if(isDBTypeInfoBright(this.DBType) && isIBLoadWarehouseUsingUTF8())
		{
			tt.create +=" CHARSET=utf8";
		}
		tt.query = "INSERT INTO " + tt.tableName + " " + pquery;
		tt.pname = pname;
		return tt;
	}
	
	public TempTable getTempTableForQueryFilter(Repository r, MeasureColumn mc, long x, List<TempTable> ttlist, String replacestr)
	{
		String pquery = Util.replaceStr(replacestr, "SELECT ", "SELECT DISTINCT ");
		// Re-use a temp table if already there
		for (TempTable stt : ttlist)
		{
			if (stt.query.endsWith(pquery))
			{
				return stt;
			}
		}
		TempTable tt = new TempTable();
		tt.tableName = Schema + ".birstx_temp" + x + "" + Thread.currentThread().getId() + "" + ttlist.size();
		String pname = mc.PhysicalName;
		if (mc.Qualify)
		{
			int index = mc.PhysicalName.indexOf('.');
			if (index >= 0)
				pname = mc.PhysicalName.substring(index + 1);
		} else
		{
			pname = Util.replaceWithNewPhysicalString(mc.ColumnName, this,r);
		}
		if (mc.DataType.equals("Varchar"))
			tt.create = "CREATE TABLE " + tt.tableName + " (" + pname + " " + getVarcharType(mc.Width, DBType != DatabaseType.MonetDB) + ")";
		else if (mc.DataType.equals("Integer"))
			tt.create = "CREATE TABLE " + tt.tableName + " (" + pname + " " + getIntegerType(r) + ")";
		else if (mc.DataType.equals("Float") || mc.DataType.equals("Number"))
			tt.create = "CREATE TABLE " + tt.tableName + " (" + pname + " " + getCastableFloatType() + ")";
		else if (mc.DataType.equals("Date"))
			tt.create = "CREATE TABLE " + tt.tableName + " (" + pname + " " + getDateType() + ")";
		else if (mc.DataType.equals("DateTime"))
			tt.create = "CREATE TABLE " + tt.tableName + " (" + pname + " " + getDateTimeType() + ")";
		if(isDBTypeInfoBright(this.DBType) && isIBLoadWarehouseUsingUTF8())
		{
			tt.create +=" CHARSET=utf8";
		}
		tt.query = "INSERT INTO " + tt.tableName + " " + pquery;
		tt.pname = pname;
		return tt;
	}
	
	public boolean canRetryOperation()
	{
		if (DBType == DatabaseType.Infobright)
			return true;
		return false;
	}
	
	public boolean requiresDispalySortingForXMLA()
	{
		return DBType == DatabaseType.EssbaseXMLA;
	}
	
	public boolean requiresDispalyTOPForXMLA()
	{
		return DBType == DatabaseType.EssbaseXMLA;
	}
	
	public boolean requiresPersistQueryToFileAndLoadIntoTable(Repository r)
	{
		if (DBType == DatabaseType.ParAccel)
			return true;
		if (DBType == DatabaseType.Redshift) // works, but is slower than INSERT/SELECT (unload/copy is still used for a few places where it's needed)
		 	return false;
		// if (DBType == DatabaseType.Infobright && r.getCurrentBuilderVersion() >= 16 && r.isUseNewETLSchemeForIB())
		//	return true;
		return false;
	}
	
	public int getMaxVarcharWidth()
	{
		if (DBType == DatabaseType.Oracle)
			return 2000;
		if (DBType == DatabaseType.Hana)
			return 5000;
		return 2048;
	}
	
	public boolean hasVarcharMax()
	{
		if (DBType == DatabaseType.MSSQL2012ColumnStore || DBType == DatabaseType.MSSQL2014 || DBType == DatabaseType.Oracle || DatabaseConnection.isDBTypeParAccel(DBType) || DBType == DatabaseType.MonetDB)
			return false;
		return true;
	}
	
	//XXX also check for username=V{UserName} and password=V{Password} (password in encrypted form)
	public static boolean isDynamicConnection(String connectString)
	{
		if (Boolean.parseBoolean(System.getProperty(Repository.ALLOW_DYNAMIC_DATABASE_CONNECTIONS)))
		{
			if (connectString == null)
			{
				return false;
			}
			else if (connectString.startsWith("V{") && connectString.endsWith("}"))
			{
				return true;				
			}
		}
		return false;
	}
	
	public boolean isRealTimeConnection()
	{
		return (this.Realtime && !this.useDirectConnection);
	}

	public boolean isIBLoadWarehouseUsingUTF8() 
	{
		return IBLoadWarehouseUsingUTF8;
	}

	public void setIBLoadWarehouseUsingUTF8(boolean iBLoadWarehouseUsingUTF8) 
	{
		IBLoadWarehouseUsingUTF8 = iBLoadWarehouseUsingUTF8;
	}
	
	public boolean containsFederatedJoinConnections()
	{
		return (federatedJoinConnections != null && federatedJoinConnections.length > 0);
	}
}