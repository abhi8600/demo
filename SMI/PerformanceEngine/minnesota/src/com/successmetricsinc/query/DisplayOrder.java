/**
 * $Id: DisplayOrder.java,v 1.8 2011-02-24 00:30:31 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.io.Serializable;
import java.util.List;

import com.successmetricsinc.util.Util;

/**
 * Class to contain order by information for display result sets
 * 
 * @author bpeters
 * 
 */
public class DisplayOrder implements Serializable
{
    private static final long serialVersionUID = 1L;
    public String tableName;
    public String columnName;
    public boolean ascending;
    public boolean nulltop = false; // by default, all nulls appear at the bottom; nulltop allows you to put them at the top

    public DisplayOrder()
    {
    }

    public DisplayOrder(String columnName, boolean ascending)
    {
        this.columnName = columnName;
        this.ascending = ascending;
    }

    public DisplayOrder(String tableName, String columnName, boolean ascending)
    {
        this.tableName = tableName;
        this.columnName = columnName;
        this.ascending = ascending;
    }
    
    public DisplayOrder(String tableName, String columnName, boolean ascending, boolean nulltop)
    {
        this.tableName = tableName;
        this.columnName = columnName;
        this.ascending = ascending;
        this.nulltop = nulltop;
    }

	public String getQueryString()
	{
		if (tableName != null)
		{
			return ("DO{" + tableName + "." + columnName +
					"," + (ascending ? "ASCENDING" : "DESCENDING") +
					"," + (nulltop ? "NULLTOP" : "NULLBOTTOM") +
					"}");
		} else
		{
			return ("DO{" + columnName +
					"," + (ascending ? "ASCENDING" : "DESCENDING") + 
					"," + (nulltop ? "NULLTOP" : "NULLBOTTOM") +
					"}");
		}
	}
	
	public String getNewQueryString()
	{
		if (tableName != null)
		{
			return ("[" + tableName + "." + columnName + "] " + (ascending ? "ASCENDING" : "DESCENDING") + (nulltop ? " NULLS FIRST " : " NULLS LAST "));

		} else
		{
			return ("[" + columnName + "] " + (ascending ? "ASCENDING" : "DESCENDING") + (nulltop ? " NULLS FIRST " : " NULLS LAST "));
		}
	}
    
	public static String getQueryOrders(List<DisplayOrder> displayOrders, boolean isNewQueryLanguage)
	{
		if (displayOrders == null || displayOrders.isEmpty())
		{		
			return "";
		}
		
		StringBuilder sb = new StringBuilder();
		if (isNewQueryLanguage)
			sb.append(" DISPLAY BY ");
		boolean firstTime = true;
		for (DisplayOrder dorder : displayOrders)
		{
			String processedColumnName = Util.removeSystemGeneratedFieldNumber(dorder.columnName);
			dorder.columnName = processedColumnName;
			if (isNewQueryLanguage)
			{
				if (firstTime)
				{
					firstTime = false;
				}
				else
				{
					sb.append(", ");
				}
				sb.append(dorder.getNewQueryString());
			}
			else
			{
				sb.append("," + dorder.getQueryString());
			}
		}
		return sb.toString();
	}
}
