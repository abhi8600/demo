/**
 * $Id: DimensionRule.java,v 1.4 2011-01-21 11:32:20 rchandarana Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.io.Serializable;

import com.mongodb.BasicDBObject;

public class DimensionRule implements Serializable, BasicDBObjectSerializer
{
    private static final long serialVersionUID = 1L;
	private String dimension;
	private String aggRule;
	
	public DimensionRule()
	{
	}
	
	public DimensionRule(String d, String a)
	{
		dimension = d;
		aggRule = a;
	}
	
	public String getDimension()
	{
		return dimension;
	}
	
	public String getAggRule()
	{
		return aggRule;
	}
	
	public void fromBasicDBObject(BasicDBObject obj){
		dimension = obj.getString("dimension");
		aggRule = obj.getString("aggRule");
	}
	
	public BasicDBObject toBasicDBObject(){
		BasicDBObject o = new BasicDBObject();
		o.put("dimension", dimension);
		o.put("aggRule", aggRule);
		return o;
	}
}