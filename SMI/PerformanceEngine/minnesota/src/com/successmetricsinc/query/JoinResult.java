/**
 * $Id: JoinResult.java,v 1.7 2011-09-06 21:45:46 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.util.ArrayList;
import java.util.List;

public class JoinResult
{
	public static class BridgeTable
	{
		String btable;

		BridgeTable(String table)
		{
			btable = table;
		}

		public String toString()
		{
			return btable;
		}
	}
	public String joinCondition;
	public JoinType type;
	public boolean redundant;
	public List<BridgeTable> bridgeTables;
	public boolean levelSpecificJoin;

	public void setBridgeTables(String[] tables)
	{
		if (tables == null)
		{
			bridgeTables = null;
			return;
		}
		bridgeTables = new ArrayList<BridgeTable>(tables.length);
		for (String t : tables)
		{
			bridgeTables.add(new BridgeTable(t));
		}
	}

	public List<BridgeTable> getBridgeTables()
	{
		return bridgeTables;
	}
}