/**
 * $Id: Navigation.java,v 1.150 2012/12/12 15:07:46 BIRST\mjani Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.query.olap.DimensionMemberSetNav;
import com.successmetricsinc.query.olap.OlapMemberExpression;

/**
 * Data structure for query navigation - picking the appropriate instances of measures and dimension columns that create
 * the lowest cardinality query
 * 
 * @author Brad Peters
 */
public class Navigation
{
	private static boolean EXPLAIN_ON_NAVIGATION_EXCEPTION = false;
	private static Logger logger = Logger.getLogger(Navigation.class);
	public static String NO_DIMENSION_EXCEPTION = "No Dimension Columns: ";
	private List<QueryColumn> queryColumnList;
	private MeasureColumnNav mcnav;
	private List<DimensionColumnNav> dimColumns;
	private List<DimensionMemberSetNav> dimMemberSets;
	public List<OlapMemberExpression> olapMemberExpressions;
	private Repository r;
	private List<QueryColumn> projectionList;
	private List<DimensionTable> dimensionTables;
	private List<Boolean> outerJoins;
	private List<MeasureTable> measureTables;
	private List<GroupBy> newGroupBys;
	private int baseMeasureTables = 0;
	// Sets of dimension columns used for each dimension table
	private Map<DimensionTable, Set<DimensionColumn>> dimColSets;
	// Levels for each dimension
	private Map<String, Level> levelMap;
	private List<QueryFilter> contentFilters;
	private Session session;
	private Set<Level> joinLevels;
	private Set<QueryFilter> measureFiltersRemovedInConsolidation;
	private boolean isFederatedJoinPresent = false;
	private Map<DimensionColumn, Join> dcToFederatedJoinMap = new HashMap<DimensionColumn, Join> ();
	private DatabaseConnection placeholderConnection = null;
	long navExpires = Long.MAX_VALUE;
	/**
	 * Initialize a new structure for navigation. Navigate relative to one measure column.
	 * 
	 * @param mcnav
	 *            Selected measure
	 * @param dimColumns
	 *            Dimension columns to navigate
	 * @param r
	 *            Repository
	 */
	public Navigation(MeasureColumnNav mcnav, List<DimensionColumnNav> dimColumns, List<DimensionMemberSetNav> dimMemberSets, List<OlapMemberExpression> olapMemberExpressions, Repository r, Session session, List<QueryFilter> contentFilters)
			throws CloneNotSupportedException
	{
		this.mcnav = mcnav;
		this.dimColumns = new ArrayList<DimensionColumnNav>(dimColumns.size());
		for (DimensionColumnNav dcn : dimColumns)
		{
			this.dimColumns.add((DimensionColumnNav) dcn.clone());
		}
		this.dimMemberSets = new ArrayList<DimensionMemberSetNav>(dimMemberSets.size());
		for (DimensionMemberSetNav dms : dimMemberSets)
		{
			this.dimMemberSets.add((DimensionMemberSetNav) dms.clone());
		}
		this.olapMemberExpressions = new ArrayList<OlapMemberExpression>(olapMemberExpressions.size());
		for (OlapMemberExpression exp : olapMemberExpressions)
		{
			this.olapMemberExpressions.add((OlapMemberExpression) exp.clone());
		}
		this.r = r;
		projectionList = new ArrayList<QueryColumn>();
		queryColumnList = new ArrayList<QueryColumn>();
		this.queryColumnList = new ArrayList<QueryColumn>();
		this.dimensionTables = new ArrayList<DimensionTable>();
		this.measureTables = new ArrayList<MeasureTable>();
		this.outerJoins = new ArrayList<Boolean>();
		this.newGroupBys = new ArrayList<GroupBy>();
		this.contentFilters = QueryFilter.getExpandedFilterList(contentFilters);
		this.session = session;
		this.joinLevels = new HashSet<Level>();
		this.measureFiltersRemovedInConsolidation = new HashSet<QueryFilter>();
	}

	public List<QueryColumn> getQueryColumnList()
	{
		return (queryColumnList);
	}

	public List<QueryColumn> getProjectionList()
	{
		return (projectionList);
	}

	public List<MeasureTable> getMeasureTables()
	{
		return (measureTables);
	}

	public int getBaseMeasureTableCount()
	{
		return (baseMeasureTables);
	}

	public List<DimensionTable> getDimensionTables()
	{
		return (dimensionTables);
	}
	
	public Set<String> getDimensions()
	{
		Set<String> dims = new HashSet<String>();
		if(dimensionTables != null && !dimensionTables.isEmpty())
		{
			for(DimensionTable dt : dimensionTables)
			{
				if(dt.DimensionName != null)
					dims.add(dt.DimensionName);
			}
		}
		return dims;
	}

	public List<Boolean> getOuterJoins()
	{
		return (outerJoins);
	}

	private void addMeasureTable(MeasureTable mt)
	{
		boolean found = false;
		for (MeasureTable mt2 : measureTables)
		{
			if (mt2 == mt)
			{
				found = true;
				break;
			}
		}
		if (!found)
		{
			measureTables.add(mt);
			if (mt.Type != 2) // Not derived
				baseMeasureTables++;
		}
	}

	private void addDimensionTable(DimensionTable d, boolean outerJoin)
	{
		boolean found = false;
		for (int i = 0; i < dimensionTables.size(); i++)
		{
			if ((DimensionTable) dimensionTables.get(i) == d)
			{
				found = true;
				outerJoins.set(i, Boolean.valueOf(outerJoin));
				break;
			}
		}
		if (!found)
		{
			dimensionTables.add(d);
			outerJoins.add(Boolean.valueOf(outerJoin));
		}
	}

	/**
	 * Add a measure to the query. Hidden specifies whether the column should be excluded from an outer query projection
	 * list. This is useful for derived measures. Navigate-only excludes the column completely from inner and outer
	 * projection lists, but ensures the column is in the querycolumnlist to make sure that the measure table is
	 * properly joined.
	 * 
	 * @param mc
	 * @param name
	 * @param hidden
	 * @param colIndex
	 * @param filter
	 * @return
	 */
	protected boolean addMeasure(MeasureColumn mc, String name, boolean hidden, int colIndex, QueryFilter filter)
	{
		for (QueryColumn qc : queryColumnList)
		{
			if ((qc.type == QueryColumn.MEASURE) && (((MeasureColumn) qc.o) == mc))
				return false;
		}
		if (!measureTables.contains(mc.MeasureTable))
		{
			addMeasureTable(mc.MeasureTable);
		}
		QueryColumn qc = new QueryColumn(QueryColumn.MEASURE, colIndex, mc, name);
		qc.hidden = hidden;
		addToQueryColumnList(qc);
		projectionList.add(qc);
		qc.filter = filter;
		return true;
	}
	
	protected boolean addDimensionMemberSet(DimensionMemberSetNav dms)
	{
		QueryColumn qc = new QueryColumn(QueryColumn.DIMENSION_MEMBER_SET, dms.colIndex, dms, dms.displayName);
		addToQueryColumnList(qc);
		projectionList.add(qc);
		if (dms.parentDisplayName != null && dms.parentColIndex > 0)
		{
			qc = new QueryColumn(QueryColumn.DIMENSION_MEMBER_SET, dms.parentColIndex, dms, dms.parentDisplayName);
			addToQueryColumnList(qc);
			projectionList.add(qc);			
		}
		if (dms.parentNameDisplayName != null && dms.parentNameColIndex > 0)
		{
			qc = new QueryColumn(QueryColumn.DIMENSION_MEMBER_SET, dms.parentNameColIndex, dms, dms.parentNameDisplayName);
			addToQueryColumnList(qc);
			projectionList.add(qc);			
		}
		if (dms.ordinalDisplayName != null && dms.ordinalColIndex > 0)
		{
			qc = new QueryColumn(QueryColumn.DIMENSION_MEMBER_SET, dms.ordinalColIndex, dms, dms.ordinalDisplayName);
			addToQueryColumnList(qc);
			projectionList.add(qc);
		}
		if (dms.uniqueNameDisplayName != null && dms.uniqueNameColIndex > 0)
		{
			qc = new QueryColumn(QueryColumn.DIMENSION_MEMBER_SET, dms.uniqueNameColIndex, dms, dms.uniqueNameDisplayName);
			addToQueryColumnList(qc);
			projectionList.add(qc);			
		}
		return (true);
	}
	
	protected boolean addOlapMemberExpression(OlapMemberExpression exp)
	{
		QueryColumn qc = new QueryColumn(QueryColumn.OLAP_MEMBER_EXPRESSION, exp.colIndex, exp, exp.displayName);
		addToQueryColumnList(qc);
		projectionList.add(qc);
		return (true);
	}

	protected boolean addDimensionTableColumn(DimensionColumn dc, String name, int colIndex, boolean levelKey)
	{
		boolean found = false;
		QueryColumn qc;
		for (DimensionTable dt : dimensionTables)
		{
			if (dc.DimensionTable == dt)
			{
				found = true;
				break;
			}
		}
		if (!found)
		{
			addDimensionTable(dc.DimensionTable, false);
		}
		qc = new QueryColumn(QueryColumn.DIMENSION, colIndex, dc, name);
		qc.levelKey = levelKey;
		addToQueryColumnList(qc);
		projectionList.add(qc);
		return (true);
	}

	protected void addNavigationDimensionColumn(DimensionColumn dc, String name)
	{
		boolean found = false;
		for (QueryColumn qc : projectionList)
		{
			if ((qc.type == 0) && (((DimensionColumn) qc.o) == dc))
			{
				found = true;
				break;
			}
		}
		boolean added = addDimensionTableColumn(dc, name, -1, false);
		if (!found && added)
			projectionList.remove(projectionList.size() - 1);
	}

	protected void addNavigationMeasure(MeasureColumn mc, String name)
	{
		boolean found = false;
		for (QueryColumn qc : projectionList)
		{
			if ((qc.type == QueryColumn.MEASURE) && (((MeasureColumn) qc.o) == mc))
			{
				found = true;
				break;
			}
		}
		boolean added = addMeasure(mc, name, false, -1, null);
		if (!found && added)
			projectionList.remove(projectionList.size() - 1);
	}

	protected void addConstant(String s, String name, int colIndex)
	{
		QueryColumn qc = new QueryColumn(QueryColumn.CONSTANT_OR_FORMULA, colIndex, s, name);
		queryColumnList.add(qc);
		projectionList.add(qc);
	}

	private void addToQueryColumnList(QueryColumn qc)
	{
		for (QueryColumn qc2 : projectionList)
		{
			if (qc2.o == qc.o)
			{
				return;
			}
		}
		queryColumnList.add(qc);
	}

	/**
	 * Pick the best measures and dimensions to use
	 * 
	 * @param numPicks
	 * @param addedMeasureFilterDimensionColumns
	 * @return Derived measure column to renavigate (i.e. a non-derived measure needs to be converted into a derived
	 *         measure)
	 * @throws NavigationException
	 */
	private MeasureColumn makePicks(int numConstants, List<DimensionColumnNav> addedMeasureFilterDimensionColumns) throws NavigationException
	{
		MeasureColumn mc = null;
		boolean isFilteredMeasureFromAggregate = false;
		if (mcnav != null)
		{
			MeasureColumn mcDerived = makePicksForMeasureColumn(numConstants, addedMeasureFilterDimensionColumns);
			if (mcDerived != null)
				return mcDerived;
			mc = mcnav.getMeasureColumn();
			if (mc != null)
			{
				isFilteredMeasureFromAggregate = (mc.BaseMeasureName != null || mc.MeasureFilter != null) && mc.MeasureTable.QueryAgg != null;
			}
		} else
		{
			makePicksForAttributeOnlyQuery();
		}
		/*
		 * Look through picks. If any pick column can be contained in the table of another Column, switch the pick.
		 * Don't prune any derived attributes
		 */
		Set<String> unchangedDimTables = new HashSet<String>();
		for (int i = 0; i < dimColumns.size(); i++)
		{
			boolean changedPick = false;
			DimensionColumnNav dn = (DimensionColumnNav) dimColumns.get(i);
			// don't let the added columns force a change in the picks if it will be removed after this
			if (isFilteredMeasureFromAggregate && addedMeasureFilterDimensionColumns != null && addedMeasureFilterDimensionColumns.contains(dn))
			{
				continue;
			}
			if (!dn.constant)
			{
				if (dn.matchingColumns.size() == 0)
					throw (new NavigationException("Unable to process column: " + dn.columnName));
				DimensionColumn dc = dn.getDimensionColumn();
				for (int j = 0; j < dimColumns.size(); j++)
				{
					DimensionColumnNav dn2 = (DimensionColumnNav) dimColumns.get(j);
					// don't let the added columns force a change in the picks if it will be removed after this
					if (isFilteredMeasureFromAggregate && addedMeasureFilterDimensionColumns != null && addedMeasureFilterDimensionColumns.contains(dn2))
					{
						continue;
					}
					if ((i != j) && (!dn2.constant))
					{
						if (dn2.matchingColumns.size() == 0)
							throw (new NavigationException("Unable to find column: " + dn2.columnName));
						DimensionColumn dc2 = dn2.getDimensionColumn();
						if (!dc2.TableName.equals(dc.TableName) && dc2.DimensionTable.DimensionName.equals(dc.DimensionTable.DimensionName))
						{
							for (int k = 0; k < dc2.DimensionTable.DimensionColumns.length; k++)
							{
								/*
								 * If another column is in a table with the same column (but likely at a lower level,
								 * pick it so you don't have two tables supplying what can be supplied by one
								 */
								if (dc2.DimensionTable.DimensionColumns[k].ColumnName.equals(dc.ColumnName))
								{
									for (int l = 0; l < dn.matchingColumns.size(); l++)
									{
										DimensionColumn matchdc = (DimensionColumn) dn.matchingColumns.get(l);
										if ((matchdc) == dc2.DimensionTable.DimensionColumns[k] && dc2.DimensionTable.DimensionColumns[k].VC == null)
										{
											Level keylevel = null;
											if (dn.level != null)
											{
												keylevel = dn.level.findLevel(matchdc.DimensionTable.Level);
											}
											if (keylevel != null || dn.level == null)
											{
												String dimTableKey = dn.getDimensionColumn().DimensionTable.DimensionName + "."
														+ dn.getDimensionColumn().DimensionTable.TableName;
												/*
												 * Do not change away from current pick if, for some other column, the
												 * dimension table pick could not be changed earlier. If it is exactly
												 * the same column appearing in the query twice, change it to use the
												 * same table.
												 */
												if (!unchangedDimTables.contains(dimTableKey) || dn.columnName.equals(dn2.columnName))
												{
													dn.pick = l;
													changedPick = true;
													break;
												}
											}
										}
									}
									break;
								}
							}
						}
					}
				}
			}
			/*
			 * We tried but could not change the pick for current entry so add the given dimension table name to the
			 * list of entries that we are going to use anyway. Make sure to fully qualify the dimension table name by
			 * prepending the dimension name.
			 */
			if (!changedPick && (dn.pick != -1))
			{
				unchangedDimTables.add(dn.getDimensionColumn().DimensionTable.DimensionName + "."
						+ dn.getDimensionColumn().DimensionTable.TableName);
			}
		}
		
		/* After going through the consolidation exercise as above, we still might end up with a scenario
		 * where two instances of same exact dimension column navs end up with a different pick of matching
		 * dimension column. So, go thru the list once more and make sure that all instances of same dimension
		 * column navs are using the same exact pick.
		 */
		Map<String, Integer> dupeColumnProcessingMap = new HashMap<String, Integer>();
		for(DimensionColumnNav dn : dimColumns)
		{
			String key = dn.dimensionName + "." + dn.columnName;
			if(dupeColumnProcessingMap.containsKey(key))
			{
				int pick = dupeColumnProcessingMap.get(key);
				dn.pick = pick;
				continue;
			}
			dupeColumnProcessingMap.put(key, dn.pick);
		}
		return (null);
	}

	private void makePicksForAttributeOnlyQuery()
	{
		/*
		 * Otherwise, see if any dimension columns can be combined into the same table (take the largest groupings). If
		 * not, then select the picks with the lowest cardinality
		 */
		for (DimensionColumnNav dn : dimColumns)
		{
			// Mark each column as not picked
			dn.pick = -1;
		}
		Map<DimensionTable, List<DimensionColumnNav>> groupLists = new HashMap<DimensionTable, List<DimensionColumnNav>>();
		/*
		 * Generate all sets of matching columns that come from the same dimension tables
		 */
		for (DimensionColumnNav dn : dimColumns)
		{
			for (DimensionColumn dc : dn.matchingColumns)
			{
				List<DimensionColumnNav> dcnlist = groupLists.get(dc.DimensionTable);
				if (dcnlist == null)
				{
					dcnlist = new ArrayList<DimensionColumnNav>();
					groupLists.put(dc.DimensionTable, dcnlist);
				}
				dcnlist.add(dn);
			}
		}
		// Pick columns from the same dimension table
		for (DimensionColumnNav dn : dimColumns)
		{
			for (int i = 0; i < dn.matchingColumns.size(); i++)
			{
				DimensionColumn dc = dn.matchingColumns.get(i);
				int proposedSize = groupLists.get(dc.DimensionTable).size();
				int proposedNumTables = dc.DimensionTable.TableSource.Tables.length;
				int curSize = 0;
				int curNumTables = Integer.MAX_VALUE;
				if (dn.pick >= 0)
				{
					curSize = groupLists.get(dn.getDimensionColumn().DimensionTable).size();
					curNumTables = dn.getDimensionColumn().DimensionTable.TableSource.Tables.length;
				}
				/*
				 * If the new pick comes from a table that supplies more columns, pick it. If this table supplies the
				 * same number of columns as the existing one AND either the existing pick is a virtual column (so that
				 * this is likely an aggregate) or the new pick comes from a table source with fewer physical tables,
				 * then pick the new one.
				 */
				if (dn.pick < 0 || (curSize < proposedSize || (curSize == proposedSize && curNumTables > proposedNumTables))
						|| (dn.getDimensionColumn().VC != null && dc.VC == null))
				{
					dn.pick = i;
				}
			}
		}
	}

	private MeasureColumn makePicksForMeasureColumn(int numConstants, List<DimensionColumnNav> addedMeasureFilterDimensionColumns) throws NavigationException
	{
		/*
		 * If there is a measure, pick the matches that combine with lowest overall cardinality in combination with
		 * available dimensions. At this point, all measures and dimensions join with existing columns, just need to
		 * minimize cardinality among navigated dimensions
		 */
		mcnav.pick = -1;
		double curLowMc = Double.POSITIVE_INFINITY;
		mcnav.dimpicks = new int[dimColumns.size()];
		mcnav.dimpickJoinLevels = null;
		int numOfDimsToMatch = dimColumns.size() - numConstants;
		/*
		 * For each potential measure column, calculate a total cardinality (based on lowest level that joins) and
		 * record it as an option
		 */
		int[] dimpicks = null;
		for (int m = 0; m < mcnav.matchingMeasures.size(); m++)
		{
			MeasureColumn mc = mcnav.matchingMeasures.get(m);
			/*
			 * Go through each dimension column and pick the one with the smallest cardinality that joins to the current
			 * measure table
			 */
			dimpicks = new int[dimColumns.size()];
			// Mark with -1 so can report error on columns not picked
			for (int i = 0; i < dimpicks.length; i++)
				dimpicks[i] = -1;
			Level[] dimJoinLevels = null;
			dimJoinLevels = new Level[dimColumns.size()];
			int pickedDimensions = findDimensionJoinLevels(addedMeasureFilterDimensionColumns, dimpicks, mc, dimJoinLevels);
			/*
			 * If not all dimensions picked, and they didn't all join, then reject
			 */
			if (pickedDimensions == numOfDimsToMatch)
			{
				/*
				 * Comparison result of >= 0 signifies that proposed picks are better than the current picks because
				 * there are more level-specific joins in the proposed pick as opposed to the current pick
				 */
				int result = compareJoinLevels(dimJoinLevels, mcnav.dimpickJoinLevels);
				/*
				 * Only pay attention to level-specific joins if there are dimensions in the query (measure-only queries
				 * should use cardinality only)
				 */
				if (((result == 0 || pickedDimensions == 0) && (mc.MeasureTable.Cardinality < curLowMc)) || (result > 0 && pickedDimensions > 0))
				{
					// Pick current measure and low dimension tables
					curLowMc = mc.MeasureTable.Cardinality;
					mcnav.dimpickJoinLevels = new Level[dimColumns.size()];
					for (int k = 0; k < dimColumns.size(); k++)
					{
						mcnav.dimpickJoinLevels[k] = dimJoinLevels[k];
						mcnav.dimpicks[k] = dimpicks[k];
						if (dimJoinLevels[k] != null)
						{
							joinLevels.add(dimJoinLevels[k]);
						}
					}
					mcnav.pick = m;
					mcnav.cardinality = mc.MeasureTable.Cardinality;
					mcnav.aggregationRule = mc.AggregationRule;
				}
			}
		}
		/*
		 * If didn't find matches, throw navigation error, if it is not a derived measure
		 */
		if (mcnav.pick < 0)
		{
			/*
			 * If this is a measure that is both a derived measure and a non-derived one (e.g. an aggregate table
			 * includes a derived measure), and this current measure is the non-derived one, flip the measure to a
			 * derived measure and renavigate
			 */
			List<MeasureColumn> mclist = r.findMeasureColumns(mcnav.measureName);
			for (MeasureColumn mc : mclist)
			{
				if (mc.MeasureTable.Type == MeasureTable.DERIVED)
				{
					return (mc);
				}
			}
			throwNavigationException(dimpicks);
		}
		for (int i = 0; i < dimColumns.size(); i++)
		{
			DimensionColumnNav dn = (DimensionColumnNav) dimColumns.get(i);
			dn.pick = mcnav.dimpicks[i];
		}
		return null;
	}

	private int findDimensionJoinLevels(List<DimensionColumnNav> addedMeasureFilterDimensionColumns, int[] dimpicks, MeasureColumn mc, Level[] dimJoinLevels)
	{
		int pickedDimensions = 0;
		for (int j = 0; j < dimColumns.size(); j++)
		{
			DimensionColumnNav dn = dimColumns.get(j);
			int curLow = Integer.MAX_VALUE;
			boolean picked = false;
			boolean pickedRedundant = false;
			boolean pickedLevelBased = false;
			boolean pickedFederated = false;
			for (int k = 0; k < dn.matchingColumns.size(); k++)
			{
				// Make sure it joins, and get level info
				DimensionColumn matchdc = dn.matchingColumns.get(k);
				boolean joined = r.joinedTables(mc.MeasureTable, matchdc.DimensionTable, false, false, levelMap.get(dn.dimensionName));
				if (!joined)
				{
					joined = matchesFilteredMetric(dn, mc, addedMeasureFilterDimensionColumns);
				}
				Level l = null;
				if (matchdc.DimensionTable.Level != null)
				{
					l = r.findDimensionLevel(matchdc.getDimensionName(), matchdc.DimensionTable.Level);
				}
				if (l == null)
				{
					l = dn.level;
				}
				if (joined)
				{
					Join jn = r.getJoin(mc.MeasureTable, matchdc.DimensionTable, levelMap.get(dn.dimensionName));
					if (l == null)
					{
						if (!picked
								|| (!pickedRedundant && (jn == null ? false : jn.isRedundant()))
								|| (pickedFederated && (jn == null ? false : (!jn.isFederated())))
								|| (!pickedLevelBased && (pickedLevelBased = (jn == null ? false : jn.isLevelSpecific())))
								|| (matchdc.VC == null && dn.matchingColumns.get(dimpicks[j]).VC != null)
								|| (matchdc.DimensionTable.TableSource.Tables.length < dn.matchingColumns.get(dimpicks[j]).DimensionTable.TableSource.Tables.length))
						{
							curLow = 1;
							dimpicks[j] = k;
							dimJoinLevels[j] = ((jn == null) ? null : jn.JoinLevel);
							picked = true;
						}
					} else if (l.Cardinality < curLow
							|| (!pickedRedundant && (jn == null ? false : jn.isRedundant()))
							|| (!pickedLevelBased && (jn == null ? false : jn.isLevelSpecific()))
							|| (matchdc.VC == null && dn.matchingColumns.get(dimpicks[j]).VC != null)
							|| (matchdc.DimensionTable.TableSource.Tables.length < dn.matchingColumns.get(dimpicks[j]).DimensionTable.TableSource.Tables.length))
					/*
					 * Pick this table if it is lower cardinality than the other currently picked table or it has a
					 * redundant join and current one does not
					 */
					{
						/*
						 * If you can reach the level in the match dimension column Table from the level for the current
						 * column, then it is below the current column, and therefore supported. Also if the matching
						 * table is a snowflake that crosses multiple dimensions, don't try to match levels.
						 */
						Level keylevel = l.findLevel(matchdc.DimensionTable.Level);
						if (keylevel != null || matchdc.DimensionTable.NumSnowflakeDimensions > 1)
						{
							curLow = l.Cardinality;
							dimpicks[j] = k;
							dimJoinLevels[j] = ((jn == null) ? null : jn.JoinLevel);
							picked = true;
						}
					}
					if (picked)
					{
						pickedLevelBased = pickedLevelBased || (jn != null && jn.isLevelSpecific());
						pickedRedundant = pickedRedundant || (jn != null && jn.isRedundant());
						pickedFederated = pickedFederated || (jn != null && jn.isFederated());
						if(jn.Federated)
						{
							dcToFederatedJoinMap.put(matchdc, jn);
						}
					}
				}
			}
			if (picked)
			{
				pickedDimensions++;
				isFederatedJoinPresent = (isFederatedJoinPresent || pickedFederated);
			}
		}
		return pickedDimensions;
	}

	private void throwNavigationException(int[] dimpicks) throws NavigationException
	{
		StringBuilder err = new StringBuilder("Unable to Navigate Request: measure [");
		err.append(mcnav.measureName);
		err.append("] does not relate to the following dimension columns: ");
		boolean first = true;
		for (int j = 0; j < dimColumns.size(); j++)
		{
			DimensionColumnNav dn = (DimensionColumnNav) dimColumns.get(j);
			if (!dn.constant && (dimpicks[j] < 0))
			{
				if (first)
					first = false;
				else
					err.append(',');
				err.append('[');
				err.append(dn.dimensionName);
				err.append('.');
				err.append(dn.columnName);
				err.append(']');
			}
		}
		throw new NavigationException(err.toString());
	}

	/**
	 * Compare two arrays of join levels in context of null and non-null values
	 * 
	 * @param currentPickJoinLevels
	 * @param proposedPickJoinLevels
	 * @return comparison result -1 if current picks are better than proposed picks 1 if proposed picks are better than
	 *         current picks 0 if they are equally good.
	 */
	private int compareJoinLevels(Level[] proposedPickJoinLevels, Level[] currentPickJoinLevels)
	{
		int retVal = 0;
		// If the current join levels are not available at all, return 1.
		if (currentPickJoinLevels == null || currentPickJoinLevels.length == 0)
		{
			retVal = 1;
		} else if (proposedPickJoinLevels == null || proposedPickJoinLevels.length == 0)
		{
			retVal = -1;
		} else
		{
			for (int i = 0; i < currentPickJoinLevels.length; i++)
			{
				if (i >= proposedPickJoinLevels.length)
				{
					return -1; // if the proposed pick level size is smaller, we keep the current one.
				}
				if (currentPickJoinLevels[i] != null && proposedPickJoinLevels[i] == null)
				{
					retVal = -1;
					break;
				} else if (currentPickJoinLevels[i] == null && proposedPickJoinLevels[i] != null)
				{
					retVal = 1;
					break;
				}
			}
		}
		return retVal;
	}

	private boolean matchesFilteredMetric(DimensionColumnNav dn, MeasureColumn mc, List<DimensionColumnNav> addedMeasureFilterDimensionColumns)
	{
		boolean OK = false;
		if (mc.MeasureTable.QueryAgg != null && addedMeasureFilterDimensionColumns.contains(dn))
		{
			OK = mc.BaseMeasureName != null;
			if (!OK && mc.MeasureFilter != null)
			{
				OK = mc.MeasureFilter.equalsUsingVariables(mcnav.filter, session, r, false);
			}
		}
		return (OK);
	}

	/**
	 * For each measure and dimension column thats been added as a name, not a direct table/column addition, need to
	 * navigate to the actual table and column for each. Need to find all matching dimensioncolumns and measurecolumns
	 * for each. Then, pick the measurecolumns and dimensioncolumns that allow all the columns (including directly added
	 * ones) to join and that minimize cardinality. Process the existing querycolumn list, and the appropriate columns
	 * to it.
	 * 
	 * @param addLevelKeys
	 *            Add level keys to the query that have not already been added
	 * @return Derived measure column to re-navigate (whether the query should be re-navigated - e.g. a non-derived
	 *         measure was converted into a derived measure)
	 * @throws NavigationException
	 */
	public MeasureColumn navigate(boolean addLevelKeys) throws BadColumnNameException, NavigationException
	{
		try
		{
			int numConstants = 0;
			// Find matching measures
			List<DimensionColumnNav> addedMeasureFilterDimensionColumns = new ArrayList<DimensionColumnNav>();
			if (mcnav != null)
			{
				mcnav.matchingMeasures.clear();
				List<MeasureColumn> foundmclist = r.findMeasureColumns(mcnav.measureName);
				/*
				 * Filter any matching measures that don't match the content filters
				 */
				for (MeasureColumn mc : foundmclist)
				{
					if (mc.MeasureTable.Type != MeasureTable.UNMAPPED)
						if (mc.MeasureTable.TableSource.contentFilters == null
								|| (contentFilters != null && QueryFilter.containsAll(contentFilters, QueryFilter.getExpandedFilterList(mc.MeasureTable.TableSource.contentFilters), r, session)))
							mcnav.matchingMeasures.add(mc);
				}
				/*
				 * Process any measure filters (extract any columns in measure filters that require navigation - Need to
				 * extract them during each navigation so that if a dimension column is required, it must only be
				 * navigated for the measure in question, not all the others in a query
				 */
				if (mcnav.filter != null)
				{
					mcnav.filter.extractNavigationColumns(null, addedMeasureFilterDimensionColumns, null, null);
					//Make sure that we don't add dupes of added measure filter dim columns if it already exists in dim columns list
					if(addedMeasureFilterDimensionColumns != null && addedMeasureFilterDimensionColumns.size() > 0)
					{
						for(int i = 0; i < addedMeasureFilterDimensionColumns.size(); i++)
						{
							DimensionColumnNav dn = addedMeasureFilterDimensionColumns.get(i);
							DimensionColumnNav founddn = Query.findDimensionColumnNav(dimColumns, dn.dimensionName, dn.columnName);
							if(founddn != null)
							{
								addedMeasureFilterDimensionColumns.remove(i);
								i--;
							}
							else
							{
								dimColumns.add(dn);
							}
						}
					}
				}
			}
			// Count the number of constants
			for (int i = 0; i < dimColumns.size(); i++)
			{
				DimensionColumnNav dn = (DimensionColumnNav) dimColumns.get(i);
				// Make sure matching list is clear to start
				dn.matchingColumns.clear();
				if (dn.constant)
					numConstants++;
			}
			// Set the dimension columns and levels for those not set
			setDimensionColumnsAndLevels(dimColumns, r, session);
			setDimensionSetsAndLevels(dimMemberSets, r, session);
			/*
			 * Filter measure column matches to those that join with existing dimension tables
			 */
			if (mcnav != null)
			{
				for (int k = 0; k < mcnav.matchingMeasures.size(); k++)
				{
					MeasureColumn mc = (MeasureColumn) mcnav.matchingMeasures.get(k);
					if (mc.MeasureTable.Type == MeasureTable.DERIVED)
					{
						continue;
					}
					for (DimensionColumnNav dn : dimColumns)
					{
						if (dn.constant)
						{
							continue;
						}
						boolean hasJoins = false;
						for (DimensionColumn dc : dn.matchingColumns)
						{
							if (r.joinedTables(mc.MeasureTable, dc.DimensionTable, false, true, levelMap.get(dc.DimensionTable.DimensionName)))
							{
								hasJoins = true;
								break;
							}
						}
						if (!hasJoins)
						{
							/*
							 * If this instance of the measure is in a QueryAggregate and is a named filtered metric
							 * based on this dimension column, then it's OK
							 */
							if (!matchesFilteredMetric(dn, mcnav.matchingMeasures.get(k), addedMeasureFilterDimensionColumns))
							{
								mcnav.matchingMeasures.remove(k);
								k--;
								break;
							}
						}
					}
				}
				if (mcnav.matchingMeasures.size() == 0)
				{
					throw (new NavigationException("Unable to find joins to dimensions for measure " + mcnav.measureName + " at query levels " + levelMap));
				}
			}
			/*
			 * Filter dimension columns to those that join with existing measure tables
			 */
			if (mcnav != null)
				for (DimensionColumnNav dn : dimColumns)
				{
					// Evaluate all columns that have been added to the query
					if (!dn.constant)
					{
						for (int k = 0; k < dn.matchingColumns.size(); k++)
						{
							DimensionColumn dc = (DimensionColumn) dn.matchingColumns.get(k);
							boolean hasDerivedMeasure = false;
							boolean hasJoin = false;
							for (MeasureColumn mc : mcnav.matchingMeasures)
							{
								if (mc.MeasureTable.Type != MeasureTable.DERIVED)
								{
									if (r.joinedTables(mc.MeasureTable, dc.DimensionTable, false, true, levelMap.get(dc.DimensionTable.DimensionName)))
									{
										hasJoin = true;
										break;
									}
								} else
								{
									hasDerivedMeasure = true;
								}
							}
							if ((!hasJoin) && (!hasDerivedMeasure))
							{
								dn.matchingColumns.remove(k);
								k--;
								dimColSets.remove(dc.DimensionTable);
								break;
							}
						}
						if (dn.matchingColumns.size() == 0)
						{
							throw (new NavigationException("Unable to find joins to measure " + mcnav.measureName + " for dimension column " + dn.columnName
									+ " at level " + levelMap.get(dn.dimensionName)));
						}
					}
				}
			/*
			 * Pick physical dimensions and measures
			 */
			MeasureColumn dmc = makePicks(numConstants, addedMeasureFilterDimensionColumns);
			if (dmc != null)
				return (dmc);
			/*
			 * If a dimension only query, throw a navigation error if cross joins aren't allowed and the query hits more
			 * than one dimension table
			 */
			Set<DimensionTable> dimensionTables = new HashSet<DimensionTable>();
			if (mcnav == null)
			{
				for (DimensionColumnNav dn : dimColumns)
				{
					if (!dn.constant)
						dimensionTables.add(dn.getDimensionColumn().DimensionTable);
				}
				if (dimensionTables.size() > 1)
				{
					if (!r.getServerParameters().isAllowDimensionalCrossjoins())
					{
						if (!r.getServerParameters().isForceDimensionCrossJoinsThroughFacts())
							throw (new NavigationException("Joining two dimensions without a fact is not allowed"));
						/*
						 * Conventionally you cannot join two dimensions without a fact. Often, that fact is implicit.
						 * This will determine a fact to use to pull off the join
						 */
						Set<MeasureTable> masterSet = getJoiningMeasureTables(dimensionTables);
						if (masterSet.size() == 0)
						{
							masterSet = makePicksForDimensionJoinsThroughFacts();
						}
						if (masterSet.size() == 0)
						{
							throw (new NavigationException("Joining two dimensions without a fact is not allowed"));
						}
						/*
						 * Pick the measure table with the smallest grain
						 */
						MeasureTable pickedmt = null;
						int pickedSize = Integer.MAX_VALUE;
						for (MeasureTable mt : masterSet)
						{
							int sz = mt.getGrainInfo(r).size();
							if (sz < pickedSize)
							{
								pickedmt = mt;
								pickedSize = sz;
							}
						}
						for(MeasureTable mt: r.MeasureTables)
						{
							// Pick the first in order
							if (!masterSet.contains(mt))
								continue;
							pickedmt = mt;
							break;
						}
						mcnav = new MeasureColumnNav(pickedmt.MeasureColumns[0].ColumnName, true, -1);
						mcnav.matchingMeasures.add(pickedmt.MeasureColumns[0]);
						mcnav.pick = 0;
						mcnav.navigateonly = true;
					}
				}
			}
			/*
			 * If a named filtered measure was picked, remove any added dimension columns that apply only to that filter
			 */
			if (mcnav != null)
			{
				MeasureColumn mc = mcnav.getMeasureColumn();
				if ((mc.BaseMeasureName != null || mc.MeasureFilter != null) && mc.MeasureTable.QueryAgg != null)
				{
					dimColumns.removeAll(addedMeasureFilterDimensionColumns);
				}
			}
			/*
			 * Search for levels
			 */
			Set<Level> levelSet = new HashSet<Level>();
			if (addLevelKeys)
			{
				for (DimensionColumnNav dn : dimColumns)
				{
					if (!dn.constant && !dn.navigateonly)
					{
						if (dn.level == null)
						{
							Hierarchy h = r.findDimensionHierarchy(dn.dimensionName);
							/*
							 * If no level found, then mark this column as a level key if the dimension key is not also
							 * present. Any column not explicitly part of a level is assumed to be its own level between
							 * the dimension key and Grand Total. Parallel levels are both included in a query.
							 */
							boolean found = false;
							for (DimensionColumnNav dn2 : dimColumns)
							{
								if (dn2.dimensionName.equals(dn.dimensionName) && (!dn2.constant && !dn2.navigateonly && dn2.level != null && dn2.level.Name.equals(h.DimensionKeyLevel.Name)))
								{
									found = true;
									break;
								}
							}
							if (!found)
								dn.levelKey = true;
						} else
						{
							levelSet.add(dn.level);
						}
					}
				}
				/*
				 * Now, go through levels and see if any need keys added or columns need to be used as level keys
				 */
				for (Level l : levelSet)
				{
					Hierarchy h = l.getHierarchy();
					/*
					 * Go through each key and count how many matches there are in the current query. Find the key
					 * that is exact or with the most matches. Then need to add, unless the key is being
					 * substituted. If being substituted, use another key (if available). If not available, don't
					 * add the level key and issue a warning.
					 */
					LevelKey maxKey = null;
					int minMissing = Integer.MAX_VALUE;
					List<String> missingColumns = null;
					List<DimensionColumnNav> foundColumns = null;
					for (LevelKey lkey : l.Keys)
					{
						List<String> missing = new ArrayList<String>(lkey.ColumnNames.length);
						List<DimensionColumnNav> foundCols = new ArrayList<DimensionColumnNav>(lkey.ColumnNames.length);
						for (String colName : lkey.ColumnNames)
						{
							boolean foundKeyColumn = false;
							for (DimensionColumnNav ldn : dimColumns)
							{
								if (!ldn.navigateonly && ldn.dimensionName.equals(h.DimensionName) && ldn.columnName.equals(colName))
								{
									foundCols.add(ldn);
									foundKeyColumn = true;
									break;
								}
							}
							if (!foundKeyColumn)
								missing.add(colName);
						}
						if (missing.size() < minMissing)
						{
							if (!isBeingSubstituted(dimColumns, lkey))
							{
								missingColumns = missing;
								maxKey = lkey;
								foundColumns = foundCols;
								minMissing = missing.size();
							}
						}
					}
					/*
					 * If no matches were found, use the primary key
					 */
					if (maxKey == null)
					{
						maxKey = l.getPrimaryKey();
						if (maxKey == null)
							throw (new NavigationException("Primary key does not exist for level: " + l.Name));
						if (isBeingSubstituted(dimColumns, maxKey))
						{
							LevelKey primaryKey = maxKey;
							maxKey = null;
							for (LevelKey lk : l.Keys)
							{
								if (lk != primaryKey && !isBeingSubstituted(dimColumns, lk))
								{
									maxKey = lk;
									break;
								}
							}
						}
						if (maxKey != null)
						{
							missingColumns = new ArrayList<String>(maxKey.ColumnNames.length);
							foundColumns = new ArrayList<DimensionColumnNav>(0);
							for (String s : maxKey.ColumnNames)
								missingColumns.add(s);
						}
					}
					if (maxKey != null)
					{
						/*
						 * Mark all found columns as level keys
						 */
						for (DimensionColumnNav ldn : foundColumns)
							ldn.levelKey = true;
						/*
						 * Add all key columns were found
						 */
						for (String keyColumn : missingColumns)
						{
							// Add the dimension column from the picked table
							DimensionColumn ldc = null;
							DimensionTable dt = null;
							for (DimensionColumnNav ldn : dimColumns)
							{
								if (ldn.dimensionName.equals(h.DimensionName) && !ldn.constant && !ldn.navigateonly)
								{
									dt = ldn.getDimensionColumn().DimensionTable;
								}
								if(dt != null && l.Name.equals(dt.Level))
								{
									break;
								}
							}
							ldc = dt.findColumn(keyColumn);
							if (ldc == null)
							{
								logger.warn("Could not find level key column: " + h.DimensionName + "." + keyColumn + " in table " + dt.TableName
										+ " - unable to add to query.");
							} else
							{
								addDimensionTableColumn(ldc, ldc.ColumnName, -1, true);
								// Add a group by for this dimension column
								GroupBy gb = new GroupBy(ldc);
								newGroupBys.add(gb);
							}
						}
					}
				}
			}
			// Add the navigated columns
			for (DimensionColumnNav dn : dimColumns)
			{
				if (!dn.constant)
				{
					DimensionColumn dc = dn.getDimensionColumn();
					if (!dn.navigateonly)
					{
						addDimensionTableColumn(dc, dn.displayName, dn.colIndex, dn.levelKey);
					} else
					{
						addNavigationDimensionColumn(dc, dn.displayName);
					}
				} else
				{
					if (!dn.navigateonly)
					{
						addConstant(dn.constantString, dn.columnName, dn.colIndex);
					}
				}
			}
			for (DimensionMemberSetNav dms : dimMemberSets)
			{
				if (!dms.navigateonly)
				{
					addDimensionMemberSet(dms);
				}
			}
			for (OlapMemberExpression exp : olapMemberExpressions)
			{
				addOlapMemberExpression(exp);				
			}
			// If there is a measure column, add it
			if (mcnav != null)
			{
				MeasureColumn mc = mcnav.getMeasureColumn();
				if (!mcnav.navigateonly)
				{
					addMeasure(mc, mcnav.displayName, mcnav.hidden, mcnav.colIndex, mcnav.filter);
				} else
				{
					addNavigationMeasure(mc, mcnav.displayName);
				}
			}
		} catch (NavigationException ex)
		{
			if (EXPLAIN_ON_NAVIGATION_EXCEPTION)
				logger.warn(explainFailed());
			throw ex;
		}
		return (null);
	}

	/**
	 * For a given query containing , iterate over all matching columns to see if the combination of underlying 
	 * dimension tables join through any measure tables. If there are any joining measure tables found, change 
	 * the dimension column pick for both the dimension columns.
	 * @return joining measure tables
	 */
	private Set<MeasureTable> makePicksForDimensionJoinsThroughFacts()
	{
		Set<MeasureTable> mtSet = new HashSet<MeasureTable>();
		Set<DimensionTable> dimTables = new HashSet<DimensionTable>();
		int [] currentMatchIndices = new int[dimColumns.size()];
		int [] matchingColumnsSizes = new int[dimColumns.size()];
		int dimColsSize = dimColumns.size();
		for(int dcIdx=0; dcIdx < dimColsSize; dcIdx++)
		{
			matchingColumnsSizes[dcIdx] = dimColumns.get(dcIdx).matchingColumns.size();
			currentMatchIndices[dcIdx] = 0; 
		}
		boolean done = false;
		int numIterations = 0;

		int col = (dimColsSize - 1);
		while(col >= 0)
		{
			dimTables.clear();
			for(int dcIdx = 0; dcIdx < dimColsSize; dcIdx++)
			{
				DimensionColumnNav dn = dimColumns.get(dcIdx);
				DimensionColumn matchdc = dn.matchingColumns.get(currentMatchIndices[dcIdx]);
				dimTables.add(matchdc.DimensionTable);
				mtSet = getJoiningMeasureTables(dimTables);
			}
			if(mtSet != null && mtSet.size() > 0)
			{
				for(int dcIdx = 0; dcIdx < dimColsSize; dcIdx++)
				{
					DimensionColumnNav dn = dimColumns.get(dcIdx);
					dn.pick = currentMatchIndices[dcIdx];
				}				
				done = true;
			}
			numIterations++;
			if(numIterations > 10000)
			{
				logger.debug("Could not find a navigable path through measures in number of iterations: " + numIterations );
				done = true;;
			}
			if(done)
			{
				break;
			}

			currentMatchIndices[col]++;
			
			if(col != (dimColsSize - 1) && (currentMatchIndices[col] < 	matchingColumnsSizes[col]))
			{
				col++;
				for(int i = col; i < dimColsSize; i++)
				{
					currentMatchIndices[i] = 0;
				}
				col = (dimColsSize - 1);
				continue;
			}
			
			if(currentMatchIndices[col] > (matchingColumnsSizes[col] -1))
			{
				currentMatchIndices[col] = (matchingColumnsSizes[col] -1);
				col--;
			}
		}
		
		return mtSet;
	}

	/**
	 * Find all measure tables that join to all dimension tables provided 
	 * @param dimTables dimension tables
	 * @return joining measure tables
	 */
	private Set<MeasureTable> getJoiningMeasureTables(Set<DimensionTable> dimTables)
	{
		Set<MeasureTable> masterSet = null;
		for (DimensionTable bdt : dimTables)
		{
			List<Join> jlist = r.getJoinList(bdt.TableName);
			Set<MeasureTable> mset = new HashSet<MeasureTable>();
			//Join list can be null for auto-generated time dimension in discovery mode 
			//or a stand-alone dimensional source with no facts
			if(jlist != null)
			{
				for (Join j : jlist)
				{
					if (j.Table1 == bdt && j.Table2 instanceof MeasureTable)
						mset.add((MeasureTable) j.Table2);
					else if (j.Table2 == bdt && j.Table1 instanceof MeasureTable)
						mset.add((MeasureTable) j.Table1);
				}
			}
			if (masterSet == null)
				masterSet = mset;
			else
				masterSet.retainAll(mset);
		}
		
		return masterSet;
	}
	
	private boolean isBeingSubstituted(List<DimensionColumnNav> foundColumns, LevelKey lk)
	{
		for (DimensionColumnNav dcn : foundColumns)
		{
			if (dcn.navigateonly || dcn.constant)
				continue;
			DimensionTable dt = dcn.getDimensionColumn().DimensionTable;
			if (dt.ColumnSubstitutions == null || dt.ColumnSubstitutions.length == 0)
				continue;
			for (String s : lk.ColumnNames)
			{
				DimensionColumn dc = dt.findColumn(s);
				if (dc != null)
					for (ColumnSubstitution cs : dt.ColumnSubstitutions)
					{
						if (dc.PhysicalName.contains(cs.New))
							return (true);
					}
			}
		}
		return (false);
	}
	
	
	
	public void setDimensionSetsAndLevels(List<DimensionMemberSetNav> dimMemberSets, Repository r, Session session) throws BadColumnNameException
	{
		if (levelMap == null)
			levelMap = new HashMap<String, Level>();
		for (DimensionMemberSetNav dms : dimMemberSets)
		{
			DimensionTable dt = dms.findDimensionTableForDimensionMemberSet(r);
			if (dt != null)
			{
				if (!levelMap.containsKey(dms.dimensionName))
				{
					levelMap.put(dms.dimensionName, dms.level);						
				}
			}
			else
			{
				throw (new BadColumnNameException(dms.dimensionName + ".{" + dms.getMemberExpressionsString() + "}"));
			}
		}
	}

	/**
	 * Find the possible dimension columns for each navigation (and add). Find the levels associated with each of those
	 * columns (and add to navigation).
	 * 
	 * @param dimColumns
	 * @param r
	 * @return
	 * @throws NavigationException
	 */
	public void setDimensionColumnsAndLevels(List<DimensionColumnNav> dimColumns, Repository r, Session session) throws BadColumnNameException
	{
		dimColSets = new HashMap<DimensionTable, Set<DimensionColumn>>(dimColumns.size());
		levelMap = new HashMap<String, Level>();
		// Find matching dimension columns and levels
		for (DimensionColumnNav dn : dimColumns)
		{
			/*
			 * Only process ones not already processed (i.e. those processed ahead of time externally)
			 */
			if (dn.matchingColumns.size() == 0)
			{
				// Make sure matching list is clear to start
				dn.matchingColumns.clear();
				if (!dn.constant)
				{
					List<DimensionColumn> founddclist = r.findDimensionColumns(dn.dimensionName, dn.columnName);
					if (founddclist == null)
						throw (new BadColumnNameException(dn.dimensionName + "." + dn.columnName));
					/*
					 * Add only dimension columns that are part of tables that match the content filters
					 */
					List<DimensionColumn> dclist = null;
					dclist = new ArrayList<DimensionColumn>(founddclist.size());
					for (DimensionColumn dc : founddclist)
					{
						if (dc.DimensionTable.TableSource.contentFilters == null
								|| (contentFilters != null && QueryFilter.containsAll(contentFilters, QueryFilter.getExpandedFilterList(dc.DimensionTable.TableSource.contentFilters), r, session)))
							dclist.add(dc);
					}
					dn.matchingColumns.addAll(dclist);
					/*
					 * Keep a list of all dimension columns used for each matching dimension table (used for pruning
					 * later)
					 */
					for (DimensionColumn dc : dclist)
					{
						Set<DimensionColumn> dcset = dimColSets.get(dc.DimensionTable);
						if (dcset == null)
						{
							dcset = new HashSet<DimensionColumn>(10);
							dimColSets.put(dc.DimensionTable, dcset);
						}
						dcset.add(dc);
					}
					// Find level if exists
					dn.level = r.findDimensionLevelColumn(dn.dimensionName, dn.columnName);
					// Update the dimension level
					if (!levelMap.containsKey(dn.dimensionName))
					{
						/*
						 * If no level has been added to the map, then this is the first and add it (even if null -
						 * which marks that there is a column for which no official level exists
						 */
						levelMap.put(dn.dimensionName, dn.level);
					} else
					{
						Level l = levelMap.get(dn.dimensionName);
						/*
						 * If a null level has been stored, then leave it as null - a column with no official level
						 * cannot match an existing level
						 */
						if (l != null)
						{
							/*
							 * If there is a level, and this one is lower, then replace
							 */
							if (l != dn.level && l.findLevel(dn.level) != null)
							{
								levelMap.put(dn.dimensionName, dn.level);
							}
						}
					}
				}
			}
		}
		Query.updateLevelMapForLevelKeys(r, levelMap, dimColumns);
	}

	/**
	 * @return Returns the newGroupBys.
	 */
	public List<GroupBy> getNewGroupBys()
	{
		return newGroupBys;
	}

	/**
	 * @return Returns the measure column navigation associated with this navigation.
	 */
	public MeasureColumnNav getMeasureColumnNav()
	{
		return mcnav;
	}

	/**
	 * Returns the connection that is associated with this navigation. This assumes that the query has been navigated
	 * before this call is made. It also assumes all tables are from the same connection (which should be the case if
	 * joins are setup properly)
	 * 
	 * @return
	 */
	public DatabaseConnection getNavigationConnection() throws NavigationException
	{
		if(containsFederatedJoin())
		{
			if(placeholderConnection != null)
			{
				return placeholderConnection;
			}
			DatabaseConnection dconn = null;
			DatabaseConnection defaultConnection = r.getDefaultConnection(); 
			placeholderConnection = new DatabaseConnection();
			placeholderConnection.ConnectionPool = defaultConnection.ConnectionPool;
			List<DatabaseConnection> dconnList = new ArrayList<DatabaseConnection>();
			if (mcnav != null)
			{
				dconnList.add(mcnav.getMeasureColumn().MeasureTable.TableSource.connection);
			}
			if (dimColumns.size() > 0)
			{
				for (DimensionColumnNav dcn : dimColumns)
				{
					if (dcn.constant)
						continue;
					dconn = dcn.matchingColumns.get(dcn.pick).DimensionTable.TableSource.connection;
					if(!dconnList.contains(dconn))
					{
						dconnList.add(dconn);
					}
				}
			}
			if(dconnList.size() == 1)
			{
				logger.error("Navigation containing federated join has only a single connection. Check configuration of federated joins to ensure join across sources.");
				return dconnList.get(0);
			}
			DatabaseConnection [] dconnArr = new DatabaseConnection [dconnList.size()]; 
			dconnList.toArray(dconnArr);
			placeholderConnection.federatedJoinConnections = dconnArr;
			return placeholderConnection;
		}
		
		if (mcnav != null)
		{
			return (mcnav.getMeasureColumn().MeasureTable.TableSource.connection);
		} else
		{
			if (dimColumns.size() == 0 && dimMemberSets.size() == 0)
			{
				throw new NavigationException(NO_DIMENSION_EXCEPTION);
			}
			// deal with constant columns...
			for (DimensionColumnNav dcn : dimColumns)
			{
				if (dcn.constant)
					continue;
				return (dcn.matchingColumns.get(dcn.pick).DimensionTable.TableSource.connection);
			}
			for (DimensionMemberSetNav dms : dimMemberSets)
			{
				DimensionTable dt = dms.findDimensionTableForDimensionMemberSet(r);
				if (dt != null)
				{
					return dt.TableSource.connection;
				}
				throw new NavigationException("No Dimension Table found on Microsoft Analysis Services (XMLA) connection for Dimension Member Set: ");
			}
			return null;
		}
	}

	/**
	 * Returns whether this navigation is cacheable (should be called after navigation)
	 * 
	 * @return
	 */
	public boolean isCacheable()
	{
		try
		{
			DatabaseConnection dc = getNavigationConnection();
			// do not cache data for external data sources (we don't know when they change)
			if (dc.DBType == Database.DatabaseType.GoogleAnalytics)
				return false;
		}
		catch (NavigationException ne)
		{
			// this should not happen
			logger.debug("NavigationException in isCacheable()");
			return false;
		}
		for (DimensionColumnNav dcn : dimColumns)
		{
			if (!dcn.isCacheable())
				return false;
		}
		if (mcnav == null)
			return true;
		return mcnav.isCacheable();
	}
	
	/*
	 * determine when this navigation expires in the cache
	 */
	public long getExpires()
	{
		if (!isCacheable())	// if not cacheable, expire immediately
		{
			logger.debug("navigation not cachable, expire immeidately");
			return 0;
		}

		long expires = navExpires;
		
		if (dimColumns != null)
		{
			for (DimensionColumnNav dcn : dimColumns)
			{
				expires = Math.min(dcn.getExpires(), expires);
			}
		}
		if (mcnav != null)
		{
			expires = Math.min(mcnav.getExpires(), expires);
		}
		return expires;
	}

	/**
	 * Returns the first navigated dimension table. This is useful for getting the connection information associated
	 * with a navigated result when no measure table is used
	 * 
	 * @return
	 */
	public DimensionTable getFirstNavigatedDimensionTable()
	{
		return (dimColumns.get(0).getDimensionColumn().DimensionTable);
	}

	/**
	 * Returns a string "explanation" showing how this was navigated
	 * 
	 * @return
	 */
	public String explain(boolean isSuperUser)
	{
		StringBuilder sb = new StringBuilder();
		if (mcnav != null)
		{
			sb.append("Measure Column: " + mcnav.measureName + ((mcnav.filter != null) ?  (" ( Filter: " + mcnav.filter.toString() + " )") : "") + "\n");
			if (mcnav.matchingMeasures.size() == 0)
				sb.append("\tNo matching measures found in repository!\n");
			else
			{
				sb.append("\tMeasure Tables (cardinality):\n");
				for (int i = 0; i < mcnav.matchingMeasures.size(); i++)
				{
					MeasureColumn mc = mcnav.matchingMeasures.get(i);
					sb.append("\t\t" + (mcnav.pick == i ? "* " : "  ") + mc.TableName + " (" + ((double) mc.MeasureTable.Cardinality) + ")");
					boolean noTable = true;
					if(mc.MeasureTable.TableSource.connection.Realtime || isSuperUser)
					{
						if(noTable)
						{
							noTable = false;
						}
						sb.append(" [");
						for (int j = 0; j < mc.MeasureTable.TableSource.Tables.length; j++)
						{
							TableDefinition td = mc.MeasureTable.TableSource.Tables[j];
							if (j > 0)
								sb.append(',');
							sb.append(td.PhysicalName);
						}
						sb.append("]\n");
					}
					if(noTable)
					{
						sb.append("\n");
					}
				}
			}
		}
		Set<DimensionTable> tableSet = new LinkedHashSet<DimensionTable>(dimColumns.size()); // have a predictable order
		for (DimensionColumnNav dcn : dimColumns)
		{
			if (dcn.matchingColumns.size() != 0)
			{
				tableSet.add(dcn.getDimensionColumn().DimensionTable);
			}
		}
		sb.append("\tDimension Tables Selected:\n");
		for (DimensionTable dt : tableSet)
		{
			sb.append("\t\t" + dt.TableName);
			boolean noTable = true;
			if(dt.TableSource.connection.Realtime || isSuperUser)
			{
				if(noTable)
				{
					noTable = false;
				}
				sb.append(" [");
				for (int j = 0; j < dt.TableSource.Tables.length; j++)
				{
					TableDefinition td = dt.TableSource.Tables[j];
					if (j > 0)
						sb.append(',');
					sb.append(td.PhysicalName);
				}
				sb.append("]\n");
			}
			if(noTable)
			{
				sb.append("\n");
			}
		}		
		for (DimensionColumnNav dcn : dimColumns)
		{
			sb.append("\tDimension Column: " + dcn.dimensionName + "." + dcn.columnName + "\n");
			if (dcn.matchingColumns.size() == 0)
				sb.append("\t\tNo matching dimension columns found in repository!\n");
			else
			{
				sb.append("\t\tDimension Tables (level):\n");
				for (int i = 0; i < dcn.matchingColumns.size(); i++)
				{
					DimensionColumn dc = dcn.matchingColumns.get(i);
					sb.append("\t\t\t" + (dcn.pick == i ? "* " : "  ") + dc.TableName + " ("
							+ (dc.DimensionTable.Level == null ? "no level specified" : dc.DimensionTable.Level) + ")");									
					if(dc.DimensionTable.TableSource.connection.Realtime || isSuperUser)					
					{						
						sb.append(" [");
						for (int j = 0; j < dc.DimensionTable.TableSource.Tables.length; j++)
						{
							TableDefinition td = dc.DimensionTable.TableSource.Tables[j];
							if (j > 0)
								sb.append(',');
							sb.append(td.PhysicalName);
						}
						sb.append("]");
					}
					sb.append(" [");
					if(mcnav != null)
					{
						for (int j = 0; j < mcnav.matchingMeasures.size(); j++)
						{
							if (r.joinedTables(mcnav.matchingMeasures.get(j).MeasureTable, dcn.matchingColumns.get(i).DimensionTable, false, false, levelMap
									.get(dcn.matchingColumns.get(i).DimensionTable.DimensionName)))
								sb.append('X');
							else
								sb.append(' ');
						}
					}

					sb.append("]\n");
				}
			}
		}
		return (sb.toString());
	}

	/**
	 * Returns a string "explanation" showing options for navigation
	 * 
	 * @return
	 */
	public String explainFailed()
	{
		StringBuilder sb = new StringBuilder();
		if (mcnav != null)
		{
			sb.append("Measure Column: " + mcnav.measureName + "\n");
			List<MeasureColumn> foundmclist = r.findMeasureColumns(mcnav.measureName);
			if (foundmclist == null || foundmclist.size() == 0)
			{
				sb.append("\tNo matching measures found in repository!\n");
				return sb.toString();
			} else
			{
				sb.append("\tMeasure Tables (cardinality):\n");
				for (int i = 0; i < foundmclist.size(); i++)
				{
					MeasureColumn mc = foundmclist.get(i);
					sb.append("\t\t" + mc.TableName + " (" + ((double) mc.MeasureTable.Cardinality) + ") [");
					for (int j = 0; j < mc.MeasureTable.TableSource.Tables.length; j++)
					{
						TableDefinition td = mc.MeasureTable.TableSource.Tables[j];
						if (j > 0)
							sb.append(',');
						sb.append(td.PhysicalName);
					}
					sb.append("]\n");
				}
			}
			// predictable
			Set<DimensionTable> tableSet = new HashSet<DimensionTable>();
			for (DimensionColumnNav dcn : dimColumns)
			{
				List<DimensionColumn> founddclist = r.findDimensionColumns(dcn.dimensionName, dcn.columnName);
				dcn.matchingColumns = founddclist;
				for (DimensionColumn dc : founddclist)
					tableSet.add(dc.DimensionTable);
			}
			sb.append("\tDimension Tables:\n");
			for (DimensionTable dt : tableSet)
			{
				sb.append("\t\t" + dt.TableName + " [");
				for (int j = 0; j < dt.TableSource.Tables.length; j++)
				{
					TableDefinition td = dt.TableSource.Tables[j];
					if (j > 0)
						sb.append(',');
					sb.append(td.PhysicalName);
				}
				sb.append("]\n");
			}
			for (DimensionColumnNav dcn : dimColumns)
			{
				sb.append("\tDimension Column: " + dcn.dimensionName + "." + dcn.columnName + "\n");
				if (dcn.matchingColumns.size() == 0)
				{
					sb.append("\t\tNo matching dimension columns found in repository!\n");
					continue;
				} else
				{
					sb.append("\t\tDimension Tables (level):\n");
					for (int i = 0; i < dcn.matchingColumns.size(); i++)
					{
						DimensionColumn dc = dcn.matchingColumns.get(i);
						sb.append("\t\t\t" + dc.TableName + " (" + (dc.DimensionTable.Level == null ? "no level specified" : dc.DimensionTable.Level) + ") [");
						for (int j = 0; j < dc.DimensionTable.TableSource.Tables.length; j++)
						{
							TableDefinition td = dc.DimensionTable.TableSource.Tables[j];
							if (j > 0)
								sb.append(',');
							sb.append(td.PhysicalName);
						}
						sb.append("] [");
						for (int j = 0; j < foundmclist.size(); j++)
						{
							if (r.joinedTables(foundmclist.get(j).MeasureTable, dcn.matchingColumns.get(i).DimensionTable, false, false, levelMap
									.get(dcn.matchingColumns.get(i).DimensionTable.DimensionName)))
								sb.append('X');
							else
								sb.append(' ');
						}
						sb.append("]\n");
					}
				}
			}
		}
		return (sb.toString());
	}

	/**
	 * @return the contentFilters
	 */
	public List<QueryFilter> getContentFilters()
	{
		return contentFilters;
	}

	/**
	 * @param contentFilters
	 *            the contentFilters to set
	 */
	public void setContentFilters(List<QueryFilter> contentFilters)
	{
		this.contentFilters = contentFilters;
	}

	/**
	 * @return the levelMap
	 */
	public Map<String, Level> getLevelMap()
	{
		return levelMap;
	}

	/**
	 * @return the usesLevelSpecificJoin
	 */
	public boolean usesLevelSpecificJoin()
	{
		return (joinLevels.size() > 0);
	}

	/**
	 * @return the dimColumns
	 */
	public List<DimensionColumnNav> getDimColumns()
	{
		return dimColumns;
	}

	public Set<Level> getJoinLevels()
	{
		return joinLevels;
	}

	public Set<String> getAllDimensionColumnNamesInFilters()
	{
		Set<String> dcSet = new HashSet<String>();
		if (contentFilters != null)
		{
			for (QueryFilter qf : contentFilters)
				qf.getAllDimensionColumnNames(dcSet);
		}
		return dcSet;
	}

	public Set<QueryFilter> getMeasureFiltersRemovedInConsolidation()
	{
		return measureFiltersRemovedInConsolidation;
	}

	public void addMeasureFilterRemovedInConsolidation(QueryFilter qf)
	{
		measureFiltersRemovedInConsolidation.add(qf);
	}

	public static boolean isExplainOnNavigationException()
	{
		return EXPLAIN_ON_NAVIGATION_EXCEPTION;
	}

	public static void setExplainOnNavigationException(boolean explain_on_navigation_exception)
	{
		EXPLAIN_ON_NAVIGATION_EXCEPTION = explain_on_navigation_exception;
	}

	public void setNavExpires(long navExpires)
	{
		this.navExpires = navExpires;
	}
	
	public boolean containsAllRealTimeTableSources() {
		boolean allRealTime = true;
		if (mcnav != null) {
			for (int i = 0; i < mcnav.matchingMeasures.size(); i++) 
			{
				MeasureColumn mc = mcnav.matchingMeasures.get(i);
				if(mc == null)
				{
					continue;
				}
				if (!mc.MeasureTable.TableSource.connection.Realtime) 
				{
					logger.warn("Not a RealTime Connectin for MeasureTable" + mc.MeasureTable);
					allRealTime = false;
					break;
				}
			}
		}
		if (allRealTime && dimColumns != null) {
			for (DimensionColumnNav dcn : dimColumns) 
			{
				for (int i = 0; i < dcn.matchingColumns.size(); i++) 
				{
					DimensionColumn dc = dcn.matchingColumns.get(i);
					if(dc == null)
					{
						continue;
					}
					if (!dc.DimensionTable.TableSource.connection.Realtime) 
					{
						logger.warn("Not a RealTime Connectin for DimensionTable" + dc.DimensionTable);
						allRealTime = false;
						break;
					}
				}
				if(!allRealTime)
				{
					break;
				}
			}
		}

		return allRealTime;
	}
	
	public boolean containsFederatedJoin()
	{
		return isFederatedJoinPresent;
	}
	
	public Map<DimensionColumn, Join> getDcToFederatedJoinMap()
	{
		return dcToFederatedJoinMap;
	}
}
