/**
 * $Id: JoinType.java,v 1.2 2007-09-19 20:34:31 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

public enum JoinType
{
	Inner, LeftOuter, RightOuter, FullOuter
}