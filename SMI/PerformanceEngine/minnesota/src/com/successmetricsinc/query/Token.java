/**
 * $Id: Token.java,v 1.7 2011-02-14 06:31:25 mjani Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.util.List;
import java.util.ArrayList;

/**
 * Class to encapsulate a query string token
 * 
 * @author bpeters
 * 
 */
public class Token
{
    public String type;
    public String body;
    public String display;

    public String toString()
    {
        return type + ":" + body + " " + display;
    }

    /**
     * Process a string into a list of tokens
     * 
     * @param s
     * @return
     */
    public static List<Token> getTokens(String s)
    {
        List<Token> list = new ArrayList<Token>();
        int pos = 0;
        int left = s.indexOf('{');
        if (left<0)
            return (null);
        while ((pos>=0)&&(left>=0))
        {
            int depth = 0;
            int right = left+1;
            for (; right<s.length(); right++)
            {
                if (s.charAt(right)=='{')
                    depth++;
                else if (s.charAt(right)=='}')
                {
                    if (depth>0)
                        depth--;
                    else
                        break;
                }
            }
            int next = right+1;
            for (; next<s.length(); next++)
            {
                if (s.charAt(next)==',')
                    break;
            }
            Token t = new Token();
            t.type = s.substring(pos, left).trim().toUpperCase();
            t.body = s.substring(left+1, right).trim();
            if (next>right+1)
                t.display = s.substring(right+1, next).trim();
            if(t.display != null && t.display.equals(""))
            {
            	t.display = null;
            }
            list.add(t);
            pos = next+1;
            if (pos>=s.length())
                break;
            left = s.indexOf('{', pos);
            if ((left>=0)&&(s.charAt(pos)==','))
                pos++;
        }
        return (list);
    }
}
