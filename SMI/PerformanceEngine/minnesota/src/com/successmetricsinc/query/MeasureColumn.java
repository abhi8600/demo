/**
 * $Id: MeasureColumn.java,v 1.53 2012-11-12 18:25:20 BIRST\ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Variable;
import com.successmetricsinc.query.Aggregation.AggregationType;
import com.successmetricsinc.util.RepositoryException;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XmlUtils;

public class MeasureColumn implements Cloneable, Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;
	private static Logger logger = Logger.getLogger(MeasureColumn.class);
	public String TableName;
	public String ColumnName;
	public String Description;
	public String PhysicalName;
	private String physicalNameOrig;
	public boolean Qualify;
	public String DataType;
	public String dType;
	public String LogicalDataType;
	public int Width;
	public String AggregationRule;
	public String aggRule;
	public List<DimensionRule> DimensionRules;
	public AggregationType Display;
	public String PerformanceCategory;
	public boolean Key;
	public int Improve;
	public String Maximum;
	public String Minimum;
	public String MinTarget;
	public String MaxTarget;
	public String MinImprovement;
	public String MaxImprovement;
	public String Format;
	public MeasureTable MeasureTable;
	public String Opportunity;
	public boolean EnforceLimits;
	public String AttributeWeightMeasure;
	public String ValidMeasure;
	private boolean isValidMeasureTag;
	public String AnalysisMeasure;
	public String SegmentMeasure;
	public List<Variable> SessionVariables;
	public String BaseMeasureName;
	public QueryFilter MeasureFilter;
	public boolean AutoGenerated;
	private boolean replaceWithZeroWhenNull = false;
	public FilteredMeasureDefinition FilteredMeasures;
	// Name or original measure before inheritance
	public String RootMeasure;
	public List<String> Prefixes;
	// For derived measures, keep track of all underlying base measures
	public List<String> BaseMeasures;
	public boolean NotCacheable = false;	
	public String PhysicalFilter;
	transient public boolean NonAdditive = false;
	public boolean LogicalAutoGenerated;
	public boolean ManuallyEdited;
	private String strOppurtunity;
	private String strColumnName;

	public MeasureColumn()
	{
	}

	public MeasureColumn(MeasureTable mt, Element e, Namespace ns) throws RepositoryException
	{
		ColumnName = Util.intern(e.getChildTextTrim("ColumnName", ns));
		strColumnName = e.getChildText("ColumnName", ns);
		Description = e.getChildTextTrim("Description", ns);
		TableName = Util.intern(e.getChildTextTrim("TableName", ns));
		PhysicalName = Util.intern(e.getChildTextTrim("PhysicalName", ns));
		physicalNameOrig= e.getChildText("PhysicalName", ns);
		String qstr = e.getChildTextTrim("Qualify", ns);
		Qualify = qstr == null ? false : Boolean.parseBoolean(qstr);
		DataType = e.getChildTextTrim("DataType", ns);
		dType = e.getChildTextTrim("DataType", ns);
		if (DataType == null)
			DataType = "Integer";
		DataType = Util.intern(DataType);
		String widthStr = e.getChildTextTrim("Width", ns);
		if (widthStr != null)
			Width = Integer.parseInt(widthStr);
		Format = Util.intern(e.getChildTextTrim("Format", ns));
		String keyStr = e.getChildTextTrim("Key", ns);
		Key = keyStr == null ? false : Boolean.parseBoolean(keyStr);
		String enfstr = e.getChildTextTrim("EnforceLimits", ns);
		EnforceLimits = enfstr == null ? false : Boolean.parseBoolean(enfstr);
		String impStr = e.getChildTextTrim("Improve", ns);
		Improve = impStr == null ? 0 : Integer.parseInt(impStr);
		Maximum = e.getChildTextTrim("Maximum", ns);
		Minimum = e.getChildTextTrim("Minimum", ns);
		MinTarget = e.getChildTextTrim("MinTarget", ns);
		MaxTarget = e.getChildTextTrim("MaxTarget", ns);
		MinImprovement = e.getChildTextTrim("MinImprovement", ns);
		MaxImprovement = e.getChildTextTrim("MaxImprovement", ns);
		AggregationRule = e.getChildTextTrim("AggregationRule", ns);
		aggRule = e.getChildTextTrim("AggregationRule", ns);
		if (AggregationRule == null || AggregationRule.isEmpty())
			AggregationRule = "SUM";
		AggregationRule = Util.intern(AggregationRule);
		LogicalDataType = DataType;
		if (AggregationRule.equals("COUNT") || AggregationRule.equals("COUNT DISTINCT"))
			LogicalDataType = "Integer";
		setDimensionRules(e.getChildTextTrim("DimensionRules", ns));
		String df = e.getChildTextTrim("DisplayFunction", ns);
		if (df != null)
		{
			if (df.equals("RANK"))
				Display = AggregationType.Rank;
			else if (df.equals("DRANK"))
				Display = AggregationType.DenseRank;
			else if (df.equals("PTILE"))
				Display = AggregationType.Percentile;
			else
				Display = AggregationType.None;
		} else
			Display = AggregationType.None;
		PerformanceCategory = e.getChildTextTrim("PerformanceCategory", ns);
		Opportunity = e.getChildTextTrim("Opportunity", ns);
		strOppurtunity = e.getChildText("Opportunity", ns);
		AttributeWeightMeasure = e.getChildTextTrim("AttributeWeightMeasure", ns);
		AnalysisMeasure = e.getChildTextTrim("AnalysisMeasure", ns);
		SegmentMeasure = e.getChildTextTrim("SegmentMeasure", ns);
		ValidMeasure = e.getChildTextTrim("ValidMeasure", ns);
		isValidMeasureTag = e.getChild("ValidMeasure",ns)!=null?true:false;
		// Make sure no-lengths strings are interpreted as nulls
		if ((MinTarget != null) && (MinTarget.length() == 0))
			MinTarget = null;
		if ((MaxTarget != null) && (MaxTarget.length() == 0))
			MaxTarget = null;
		if ((ValidMeasure != null) && (ValidMeasure.length() == 0))
			ValidMeasure = null;
		MeasureTable = mt;
		// determine mapping for null
		String rule = AggregationRule.toUpperCase();
		if (rule.startsWith("SUM") || rule.startsWith("COUNT"))
		{
			replaceWithZeroWhenNull = true;
		}
		String autoGr = e.getChildTextTrim("AutoGenerated", ns);
		AutoGenerated = autoGr == null ? false : Boolean.parseBoolean(autoGr);
		Element fme = e.getChild("FilteredMeasures", ns);
		if (fme != null)
		{
			FilteredMeasures = new FilteredMeasureDefinition(fme, ns);
		}
		RootMeasure = e.getChildTextTrim("RootMeasure", ns);
		if ((RootMeasure != null) && (RootMeasure.length() == 0))
			RootMeasure = null;
		RootMeasure = Util.intern(RootMeasure);
		String notCacheable = e.getChildTextTrim("NotCacheable", ns);
		if (notCacheable != null)
			NotCacheable = Boolean.parseBoolean(notCacheable);
		PhysicalFilter = e.getChildTextTrim("PhysicalFilter", ns);
		String logicalAutogen = e.getChildTextTrim("LogicalAutoGenerated", ns);
		if (logicalAutogen!=null)
		{
			LogicalAutoGenerated = Boolean.parseBoolean(logicalAutogen);
		}
		String manuallyEdited = e.getChildTextTrim("ManuallyEdited", ns);
		if (manuallyEdited!=null)
		{
			ManuallyEdited = Boolean.parseBoolean(manuallyEdited);
		}
	}

	private String getDimensionRules()
	{
		StringBuilder dimRules = new StringBuilder();
		if (DimensionRules != null)
		{
			boolean first = true;
			for (DimensionRule dimRule : DimensionRules) {
				if (!first)
					dimRules.append(",");
				dimRules.append(dimRule.getDimension()).append("=").append(dimRule.getAggRule());
				first = false;
			}
			return dimRules.toString();
		}
		return null;
	}

	public Element getMeasureColumnElement(Namespace ns, Repository r)
	{
		Element e = new Element("MeasureColumn", ns);
		Element child = null;
		
		if (TableName!=null)
		{
			XmlUtils.addContent(e, "TableName", TableName,ns);
		}
		
		child = new Element("ColumnName",ns);
		child.setText(strColumnName);
		e.addContent(child);
		
		if (Description!=null)
		{
			XmlUtils.addContent(e, "Description", Description,ns);
		}
		
		if (PhysicalName!=null)
		{
			XmlUtils.addContent(e, "PhysicalName", physicalNameOrig,ns);
		}
		if (aggRule!=null)
		{
			child = new Element("AggregationRule",ns);
			child.setText(aggRule);
			e.addContent(child);
		}
		String dimRules = getDimensionRules();
		if (dimRules!=null && !dimRules.isEmpty())
		{
			child = new Element("DimensionRules",ns);
			child.setText(dimRules);
			e.addContent(child);
		}
		String displayFunction = null;
		if (Display == AggregationType.Rank)
			displayFunction = "RANK";
		else if (Display == AggregationType.DenseRank)
			displayFunction = "DRANK";
		else if (Display == AggregationType.Percentile)
			displayFunction = "PTILE";
		if (displayFunction!=null)
		{
			child = new Element("DisplayFunction",ns);			
			child.setText(displayFunction);
			e.addContent(child);
		}
		if (dType!=null)
		{
			child = new Element("DataType",ns);
			child.setText(dType);
			e.addContent(child);
		}
		if (Width>0)
		{
			child = new Element("Width",ns);
			child.setText(String.valueOf(Width));
			e.addContent(child);
		}
		
		XmlUtils.addContent(e, "Format", Format,ns);
		
		if (Qualify)
		{
			child = new Element("Qualify",ns);
			child.setText(String.valueOf(Qualify));
			e.addContent(child);
		}
		if (Key)
		{
			child = new Element("Key",ns);
			child.setText(String.valueOf(Key));
			e.addContent(child);
		}
		if (PerformanceCategory!=null)
		{
		  XmlUtils.addContent(e, "PerformanceCategory", PerformanceCategory,ns);
		}
		if (Improve!=0)
		{
			child = new Element("Improve",ns);
			child.setText(String.valueOf(Improve));
			e.addContent(child);
		}
		if (Minimum!=null)
		{
		  XmlUtils.addContent(e, "Minimum", Minimum,ns);
		}
		if (Maximum!=null)
		{
		  XmlUtils.addContent(e, "Maximum", Maximum,ns);
		}
		if (MinTarget!=null)
		{
		  XmlUtils.addContent(e, "MinTarget", MinTarget,ns);
		}
		if (MaxTarget!=null)
		{
		  XmlUtils.addContent(e, "MaxTarget", MaxTarget,ns);
		}
		if (MinImprovement!=null)
		{
		  XmlUtils.addContent(e, "MinImprovement", MinImprovement,ns);
		}
		if (MaxImprovement!=null)
		{
		  XmlUtils.addContent(e, "MaxImprovement", MaxImprovement,ns);
		}
		
		if (ValidMeasure!=null)
		{
			child = new Element("ValidMeasure",ns);
			child.setText(ValidMeasure);
			e.addContent(child);
		}
		else if (isValidMeasureTag)
		{
			child = new Element("ValidMeasure",ns);
			e.addContent(child);
		}
		
		if (strOppurtunity!=null)
		{
			child = new Element("Opportunity",ns);
			child.setText(strOppurtunity);
			e.addContent(child);
		}
		if (EnforceLimits)
		{
			child = new Element("EnforceLimits",ns);
			child.setText(String.valueOf(EnforceLimits));
			e.addContent(child);
		}
		if (AttributeWeightMeasure!=null)
		{
			child = new Element("AttributeWeightMeasure",ns);
			child.setText(AttributeWeightMeasure);
			e.addContent(child);
		}
		if (AnalysisMeasure!=null)
		{
		  XmlUtils.addContent(e, "AnalysisMeasure", AnalysisMeasure,ns);
		}
		if (SegmentMeasure!=null)
		{
		  XmlUtils.addContent(e, "SegmentMeasure", SegmentMeasure,ns);
		}
		
		if (FilteredMeasures!=null)
		{
			e.addContent(FilteredMeasures.getFilteredMeasureDefElement(ns));
		}
		
		if (AutoGenerated)
		{
			child = new Element("AutoGenerated",ns);
			child.setText(String.valueOf(AutoGenerated));
			e.addContent(child);
		}
		if (LogicalAutoGenerated)
		{
			child = new Element("LogicalAutoGenerated",ns);
			child.setText(String.valueOf(LogicalAutoGenerated));
			e.addContent(child);
		}
		if (RootMeasure!=null)
		{
			child = new Element("RootMeasure",ns);
			child.setText(RootMeasure);
			e.addContent(child);
		}
		if (NotCacheable)
		{
			child = new Element("NotCacheable",ns);
			child.setText(String.valueOf(NotCacheable));
			e.addContent(child);
		}
		if (PhysicalFilter!=null)
		{
		  XmlUtils.addContent(e, "PhysicalFilter", PhysicalFilter,ns);
		}
		if (ManuallyEdited)
		{
			child = new Element("ManuallyEdited",ns);
			child.setText(String.valueOf(ManuallyEdited));
			e.addContent(child);
		}
		return e;
	}

	public Object clone() throws CloneNotSupportedException
	{
		MeasureColumn mc = (MeasureColumn) super.clone();
		mc.TableName = this.TableName;
		mc.PhysicalName = this.PhysicalName;
		mc.ColumnName = this.ColumnName;
		mc.Qualify = this.Qualify;
		mc.DataType = this.DataType;
		mc.AggregationRule = this.AggregationRule;
		mc.DimensionRules = this.DimensionRules == null ? null : new ArrayList<DimensionRule>(this.DimensionRules);
		mc.Display = this.Display;
		mc.PerformanceCategory = this.PerformanceCategory;
		mc.Key = this.Key;
		mc.Improve = this.Improve;
		mc.Maximum = this.Maximum;
		mc.Minimum = this.Minimum;
		mc.MinTarget = this.MinTarget;
		mc.MaxTarget = this.MaxTarget;
		mc.MinImprovement = this.MinImprovement;
		mc.MaxImprovement = this.MaxImprovement;
		mc.Format = this.Format;
		mc.MeasureTable = this.MeasureTable;
		mc.Opportunity = this.Opportunity;
		mc.EnforceLimits = this.EnforceLimits;
		mc.ValidMeasure = this.ValidMeasure;
		mc.AttributeWeightMeasure = this.AttributeWeightMeasure;
		mc.AnalysisMeasure = this.AnalysisMeasure;
		mc.SegmentMeasure = this.SegmentMeasure;
		mc.SessionVariables = this.SessionVariables == null ? null : new ArrayList<Variable>(this.SessionVariables);
		mc.replaceWithZeroWhenNull = this.replaceWithZeroWhenNull;
		mc.AutoGenerated = this.AutoGenerated;
		mc.MeasureFilter = this.MeasureFilter;
		mc.BaseMeasureName = this.BaseMeasureName;
		mc.RootMeasure = this.RootMeasure;
		mc.Prefixes = this.Prefixes == null ? null : new ArrayList<String>(this.Prefixes);
		mc.PhysicalFilter = PhysicalFilter;
		return (mc);
	}

	public String toString()
	{
		return (this.ColumnName + "(" + this.TableName + ")");
	}

	/**
	 * Set the dimension-specific aggregation rules based on a comma-delimited string of name-value pairs. The first
	 * name is the dimension and the second is the aggregation rule: Time=AVG,Product=SUM
	 * 
	 * @param dimRuleString
	 */
	public void setDimensionRules(String dimRuleString)
	{
		List<DimensionRule> dimRules = new ArrayList<DimensionRule>();
		if (dimRuleString != null)
			for (String s : dimRuleString.split(","))
			{
				int index = s.indexOf('=');
				if (index > 0)
				{
					dimRules.add(new DimensionRule(s.substring(0, index), s.substring(index + 1)));
				}
			}
		this.DimensionRules = dimRules;
	}

	/*
	 * Do the string replacements for measure expressions. {M} is replaced for all aggregation rules. {S} is replaced
	 * only for sums and counts (not count distinct - additive measures)
	 */
	private String processMeasureExpression(String rule, String measureExpr, List<String> measureExpressionList)
	{
		if ((measureExpressionList == null) || (measureExpressionList.isEmpty()))
			return (measureExpr);
		String expr = measureExpr;
		boolean processadditive = false;
		if (rule.equals("COUNT") || rule.equals("SUM"))
			processadditive = true;
		for (String me : measureExpressionList)
		{
			if (me.indexOf("{M}") >= 0)
				expr = Util.replaceStr(me, "{M}", expr);
			if (processadditive && (me.indexOf("{S}") >= 0))
				expr = Util.replaceStr(me, "{S}", expr);
		}
		return (expr);
	}

	public String getAggregationString(DatabaseConnection dbc, String formula, List<String> measureExpressionList, String rule)
	{
		if (this.EnforceLimits)
		{
			double min = Double.parseDouble(this.Minimum);
			double max = Double.parseDouble(this.Maximum);
			boolean hasmin = !Double.isInfinite(min);
			boolean hasmax = !Double.isInfinite(max);
			if (hasmin || hasmax)
			{
				StringBuilder sb = new StringBuilder();
				sb.append("(CASE");
				if (hasmin)
				{
					sb.append(" WHEN (");
					sb.append(formula);
					sb.append(")<");
					sb.append(min);
					sb.append(" THEN ");
					sb.append(min);
				}
				if (hasmax)
				{
					sb.append(" WHEN (");
					sb.append(formula);
					sb.append(")>");
					sb.append(max);
					sb.append(" THEN ");
					sb.append(max);
				}
				sb.append(" ELSE (");
				sb.append(formula);
				sb.append(") END)");
				formula = sb.toString();
			}
		}
		if (rule == null)
		{
			rule = AggregationRule.toUpperCase();		
			// if the table is an aggregate and this measure rule is COUNT, change to SUM
			if (MeasureTable.QueryAgg != null && MeasureTable.QueryAgg.measureList != null && rule.equals("COUNT"))
			{
				/*
				 * Make sure that this isn't a logical column definition in the aggregate (i.e. that it isn't an
				 * aggregation of an underlying measure). The measurelist in the queryagg should only have columns that
				 * it aggregated, not new logical columns
				 */
				for (MeasureColumnNav nav : MeasureTable.QueryAgg.measureList)
				{
					if (nav.getMeasureName().equals(ColumnName))
					{
						rule = "SUM";
						logger.debug("Promoting COUNT to SUM - using aggregation on aggregate for measure: " + this.toString());
						break;
					}
				}
			}
		}
		if (rule.startsWith("RANK:"))
		{
			rule = rule.substring(5);
		} else if (rule.startsWith("DRANK:"))
		{
			rule = rule.substring(6);
		} else if (rule.startsWith("PTILE:"))
		{
			rule = rule.substring(6);
		}
		formula = processMeasureExpression(rule, formula, measureExpressionList);
		if (DataType.equals("Integer") && rule.equals("SUM") && DatabaseConnection.isDBTypeMSSQL(dbc.DBType))
		{
			// Account for integer overflow
			formula = "CAST(" + formula + " AS BIGINT)";
		}
		if (rule.equals("COUNT DISTINCT"))
		{
			StringBuilder sb = new StringBuilder();
			sb.append("COUNT(DISTINCT ");
			sb.append(formula);
			sb.append(')');
			return sb.toString();
		} else
		{
			StringBuilder sb = new StringBuilder(rule);
			sb.append('(');
			sb.append(formula);
			sb.append(')');
			return sb.toString();
		}
	}

	public boolean mapNullToZeroBasedUponAggregationRule()
	{
		return replaceWithZeroWhenNull;
	}

	/**
	 * Return whether this measure column has an additive rule along a given dimension.
	 * 
	 * @return
	 */
	public boolean isAdditive(String dimension)
	{
		if (dimension == null)
			return (AggregationRule != null && (AggregationRule.equals("SUM") || AggregationRule.equals("COUNT") || AggregationRule.equals("MIN") || AggregationRule
					.equals("MAX")));
		else
		{
			/*
			 * If there is dimension-specific aggregation, see if there is a dimension that is not additive
			 */
			if (DimensionRules != null)
				for (DimensionRule rule : DimensionRules)
				{
					/*
					 * If a rule is non-additive, make sure it is included in a group by (so that it is not aggregated)
					 */
					if ((rule.getAggRule().equals("COUNT DISTINCT") || rule.getAggRule().equals("AVG") || rule.getAggRule().equals("STDEV")))
					{
						if (rule.getDimension().equals(dimension))
							return (false);
					}
				}
			return (AggregationRule != null && (AggregationRule.equals("SUM") || AggregationRule.equals("COUNT") || AggregationRule.equals("MIN") || AggregationRule
					.equals("MAX")));
		}
	}

	/**
	 * Return whether this measure column has an additive rule along a given dimension.
	 * 
	 * @return
	 */
	public String getAggregationRule(Collection<String> dimensions)
	{
		if (dimensions == null)
			return (AggregationRule);
		else
		{
			/*
			 * If there is dimension-specific aggregation, see if there is a dimension that is not additive
			 */
			if (DimensionRules != null)
				for (DimensionRule rule : DimensionRules)
				{
					if (dimensions.contains(rule.getDimension()))
						return (rule.getAggRule());
				}
			return (AggregationRule);
		}
	}

	public boolean equals(Object o)
	{
		if (o==null)
			return false;
		if (this == o)
			return true;
		if (!(o instanceof MeasureColumn))
			return false;
		MeasureColumn mc = (MeasureColumn) o;
		if ((mc.TableName == null && TableName != null) || (mc.TableName != null && TableName == null))
		{
			return false;
		}		
		return (mc.ColumnName.equals(ColumnName) && ((mc.TableName == null && TableName == null) || (mc.TableName!=null && mc.TableName.equals(TableName))));
		
	}

	public int hashCode()
	{
		return ((TableName != null) ? TableName.hashCode() : 0) + ColumnName.hashCode();
	}

	public List<String> getBaseMeasure()
	{
		return BaseMeasures;
	}

	public void setBaseMeasures(List<String> baseMeasures)
	{
		this.BaseMeasures = baseMeasures;
	}

	public String getQualifiedPhysicalFormula(String alias)
	{
		if (Qualify)
		{
			int index = PhysicalName.indexOf('.');
			if (index > 0)
				return (alias != null ? alias + "." : "") + PhysicalName.substring(index + 1);
			else
				return (alias != null ? alias + "." : "") + PhysicalName;
		} else
		{
			return Util.replaceStr(PhysicalName, MeasureTable.TableSource.Tables[0].PhysicalName + ".", alias != null ? alias + "." : "");
		}
	}
}