/**
 * $Id: Aggregate.java,v 1.111 2012-12-06 22:38:26 BIRST\gsingh Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jdom.Namespace;
import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.status.Status;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.RepositoryException;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XmlUtils;
import com.successmetricsinc.warehouse.AggTransformation;
import com.successmetricsinc.warehouse.LoadGroup;
import com.successmetricsinc.warehouse.LogicalColumnDefinition;
import com.successmetricsinc.warehouse.Procedure;
import com.successmetricsinc.warehouse.Rank;
import com.successmetricsinc.warehouse.Expression;
import com.successmetricsinc.warehouse.StringReplacement;
import com.successmetricsinc.warehouse.Transformation;

public class Aggregate implements StringReplacement, Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(Aggregate.class);
	private String Name;
	private boolean Query;
	private String[] Measures;
	private AggColumn[] Columns;
	private String[] Filters;
	private boolean added = false;
	private QueryAggregate qa;
	private QueryAggregate tqa;
	private String LogicalQuery;
	private String IncrementalFilter;
	private String BuildFilter;	
	private AggTransformation[] Transformations;
	private String[] LoadGroupNames;
	private String[] SubGroups;
	private LoadGroup[] LoadGroups;
	private boolean processed = false;
	private boolean OuterJoin = false;
	private boolean disabled = false;
	private boolean inMemory = false;
	private DatabaseConnection connection;
	private String connectionName;
	private transient String dataDir;
	private transient String databasePath;
	private transient Status status;
	private boolean imported = false;
	
	private String Guid;
	private String CreatedDate;
	private String strLastModifiedDate;
	private String CreatedUsername;
	private String LastModifiedUsername;
	private boolean isMeasuresTag;
	private boolean isTransformationsTag;

	private static class AggColumn implements Serializable
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		public String Dimension;
		public String Column;
		
		public Element getAggColumnElement(Namespace ns)
		{
			Element e = new Element("AggColumn",ns);	
			
			Element child = new Element("Dimension",ns);
			child.setText(Dimension);
			e.addContent(child);
			
			child = new Element("Column",ns);
			child.setText(Column);
			e.addContent(child);
			
			return e;
		}
	}

	/**
	 * Create an aggregate table structure based on repository XML
	 * 
	 * @param ae
	 */
	public Aggregate(Element ae, Namespace ns)
	{
		Name = ae.getChildTextTrim("Name", ns);
		Query = Boolean.parseBoolean(ae.getChildTextTrim("Query", ns));
		if (Query)
		{
			LogicalQuery = ae.getChildTextTrim("LogicalQuery", ns);
			IncrementalFilter = ae.getChildTextTrim("IncrementalFilter", ns);
			BuildFilter = ae.getChildTextTrim("BuildFilter", ns);
		} else
		{
			Element cae = ae.getChild("Measures", ns);
			isMeasuresTag = ae.getChild("Measures", ns) !=null?true : false;
			List ml = cae.getChildren();
			Measures = new String[ml.size()];
			for (int count = 0; count < ml.size(); count++)
			{
				Measures[count] = ((Element) ml.get(count)).getTextTrim();
			}
			cae = ae.getChild("Columns", ns);
			if (cae != null)
			{
				List l2 = cae.getChildren();
				Columns = new AggColumn[l2.size()];
				for (int count2 = 0; count2 < l2.size(); count2++)
				{
					AggColumn ac = new AggColumn();
					Element ale = (Element) l2.get(count2);
					ac.Dimension = ale.getChildTextTrim("Dimension", ns);
					ac.Column = ale.getChildTextTrim("Column", ns);
					Columns[count2] = ac;
				}
			}
			Filters = Repository.getStringArrayChildren(ae, "Filters", ns);
			cae = ae.getChild("Transformations", ns);
			if (cae != null)
			{
			  isTransformationsTag = true;
				List l2 = cae.getChildren();
				Transformations = new AggTransformation[l2.size()];
				for (int count2 = 0; count2 < l2.size(); count2++)
				{
					Element ale = (Element) l2.get(count2);
					Transformations[count2] = new AggTransformation(this, ale, ns);
				}
			}
			OuterJoin = Boolean.valueOf(ae.getChildTextTrim("OuterJoin", ns)).booleanValue();
		}
		Guid = ae.getChildText("Guid",ns);
		CreatedDate = ae.getChildText("CreatedDate",ns);
		strLastModifiedDate = ae.getChildText("LastModifiedDate",ns);
		CreatedUsername = ae.getChildText("CreatedUsername",ns);
		LastModifiedUsername = ae.getChildText("LastModifiedUsername",ns);
		
		if (ae.getChildTextTrim("Disabled", ns) != null)
			setDisabled(Boolean.valueOf(ae.getChildTextTrim("Disabled", ns)).booleanValue());
		if (ae.getChildTextTrim("InMemory", ns) != null)
			setInMemory(Boolean.valueOf(ae.getChildTextTrim("InMemory", ns)).booleanValue());
		LoadGroupNames = Repository.getStringArrayChildren(ae, "LoadGroups", ns);
		SubGroups = Repository.getStringArrayChildren(ae, "SubGroups", ns);
		String temp = ae.getChildTextTrim("Connection", ns);
		if (temp != null && !temp.isEmpty())
			connectionName = temp;
		
	}
	
	public Element getAggregateElement(Namespace ns)
	{
		Element e = new Element("Aggregate",ns);
		
		Repository.addBaseAuditObjectAttributes(e, Guid, CreatedDate, strLastModifiedDate, CreatedUsername, LastModifiedUsername, ns);
		
		Element child = new Element("Name",ns);
		child.setText(Name);
		e.addContent(child);
		
		child = new Element("Query",ns);
		child.setText(String.valueOf(Query));
		e.addContent(child);
		
		if (Measures!=null && Measures.length>0)
		{
			child = Repository.getStringArrayElement("Measures", Measures, ns);
			e.addContent(child);
		}
		else  if (isMeasuresTag)
		{
			child = new Element("Measures",ns);
			e.addContent(child);
		}
		
		if (Columns!=null && Columns.length>0)
		{
			child = new Element("Columns",ns);
			for (AggColumn aggColumn : Columns)
			{
				child.addContent(aggColumn.getAggColumnElement(ns));
			}
			e.addContent(child);
		}
		
		if (Filters!=null && Filters.length>0)
		{
			child = Repository.getStringArrayElement("Filters", Filters, ns);
			e.addContent(child);
		}
		
		if (LogicalQuery!=null)
		{
			child = new Element("LogicalQuery",ns);
			child.setText(LogicalQuery);
			e.addContent(child);
		}
		
		if (IncrementalFilter!=null)
		{
			XmlUtils.addContent(e, "IncrementalFilter", IncrementalFilter,ns);
		}
		
		if (BuildFilter!=null)
		{
		  XmlUtils.addContent(e, "BuildFilter", BuildFilter,ns);
		}
		
		if (Transformations!=null && Transformations.length>0)
		{
			child = new Element("Transformations",ns);
			for (AggTransformation aggTransformation : Transformations)
			{
				child.addContent(aggTransformation.getAggTransformationElement(ns));
			}
			e.addContent(child);
		}
		else if (isTransformationsTag)
		{
			child = new Element("Transformations",ns);
			e.addContent(child);
		}
		
		child = Repository.getStringArrayElement("LoadGroups", LoadGroupNames, ns);
		e.addContent(child);
		
		child = new Element("OuterJoin",ns);
		child.setText(String.valueOf(OuterJoin));
		e.addContent(child);
		
		child = new Element("Disabled",ns);
		child.setText(String.valueOf(disabled));
		e.addContent(child);
		
		child = new Element("InMemory",ns);
		child.setText(String.valueOf(inMemory));
		e.addContent(child);
		
		if (connectionName!=null)
		{
			child = new Element("Connection",ns);
			child.setText(connectionName);
			e.addContent(child);
		}
		
		if (SubGroups!=null && SubGroups.length>0)
		{
			child = Repository.getStringArrayElement("SubGroups", SubGroups, ns);
			e.addContent(child);
		}
				
		return e;
	}
	
	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}
	
	public void setName(String aggregateName){
		this.Name = aggregateName;
	}
	public String toString()
	{
		return (this.Name);
	}

	/**
	 * Process an aggregate
	 * 
	 * @param r
	 * @param persist
	 *            Whether to persist the aggregate in the database (vs. just create the metadata for it)
	 * @throws NavigationException
	 * @throws SQLException
	 * @throws RepositoryException
	 * @throws DisplayExpressionException
	 * @throws SyntaxErrorException
	 * @throws IOException
	 * @throws ScriptException 
	 */
	public void process(Repository r, boolean persist, Session session)
	throws BaseException, SQLException, IOException, CloneNotSupportedException
	{
		process(r, persist, session, true, false, false, false, true);
	}

	/**
	 * Process an aggregate
	 * 
	 * @param r
	 * @param persist
	 *            Whether to persist the aggregate in the database (vs. just create the metadata for it)
	 * @param session
	 *            Session
	 * @param expandDerivedQueryTables
	 *            Whether to expand the derived query tables as part of the physical SQL generated
	 * @throws NavigationException
	 * @throws SQLException
	 * @throws RepositoryException
	 * @throws SyntaxErrorException
	 * @throws IOException
	 * @throws ScriptException 
	 */
	public void process(Repository r, boolean persist, Session session, boolean expandDerivedQueryTables, boolean retainDQTs, boolean dropIfExists,
			boolean forceLevelKeys, boolean allowIncremental) throws BaseException, SQLException, IOException, CloneNotSupportedException
	{
		if (connectionName != null && !connectionName.isEmpty())
			connection = r.findConnection(connectionName);
		if (connection == null)
			connection = r.getDefaultConnection();
		
		LoadGroups = findLoadGroups(r, LoadGroupNames);
		// Create a new query
		Query q = null;
		Query tq = null;
		boolean hasTransformations = Transformations != null && Transformations.length > 0;
		List<DisplayExpression> expressionList = null;
		expandDerivedQueryTables = expandDerivedQueryTables && !DatabaseConnection.isDBTypeMySQL(connection.DBType);
		if (Query)
		{
			String query2 = QueryString.preProcessQueryString(LogicalQuery, r, null);
			AbstractQueryString aqs = null;
			aqs = AbstractQueryString.getInstance(r, query2);
			//aqs.setIgnoreDisplay(true);
			q = aqs.getQuery();
			expressionList = aqs.getExpressionList();
			q.setExpandDerivedQueryTables(expandDerivedQueryTables);
			if (!expandDerivedQueryTables)
			{
				q.setDerivedQueryTableSuffix("_" + Name.hashCode());
			}
		} else
		{
			q = new Query(r);
			q.setFullOuterJoin(OuterJoin);
			if (hasTransformations)
			{
				tq = new Query(r);
				tq.setFullOuterJoin(OuterJoin);
			}
			/*
			 * Add all dimension columns
			 */
			for (AggColumn ac : Columns)
			{
				if (hasTransformations)
				{
					Transformation tr = getTransformation(ac.Dimension, ac.Column);
					if (tr == null) // Not a transformation column
					{
						q.addDimensionColumn(ac.Dimension, ac.Column);
						q.addGroupBy(new GroupBy(ac.Dimension, ac.Column));
						tq.addDimensionColumn(ac.Dimension, ac.Column);
						tq.addGroupBy(new GroupBy(ac.Dimension, ac.Column));
					} else if (Procedure.class.isInstance(tr))
					{
						// Procedure transformation column
						DimensionColumn dc = r.findDimensionColumn(ac.Dimension, ac.Column);
						String dataType = Util.getDBDataType(dc.DataType);
						StringBuilder sb = new StringBuilder();
						if (dataType != null)
						{
							if(DatabaseConnection.isDBTypeMySQL(connection.DBType) || DatabaseConnection.isDBTypeOracle(connection.DBType))
							{
								sb.append("CAST(NULL AS ");
								sb.append(dataType);
								sb.append(")");
							}
							else
							{
								sb.append("CONVERT(");
								sb.append(dataType);
								sb.append(", NULL)");
							}
						} else
						{
							sb.append("NULL");
						}
						tq.addConstantColumn(sb.toString(), Util.replaceWithPhysicalString(ac.Column));
					}
				} else
				{
					q.addDimensionColumn(ac.Dimension, ac.Column);
					q.addGroupBy(new GroupBy(ac.Dimension, ac.Column));
				}
			}
			/* Add all measures */
			for (String s : Measures)
			{
				if (hasTransformations)
				{
					Transformation tr = getTransformation(null, s);
					if (tr == null) // Not a transformation column
					{
						q.addMeasureColumn(s, s, false);
						tq.addMeasureColumn(s, s, false);
					} else if (Procedure.class.isInstance(tr) || Rank.class.isInstance(tr))
					{
						// Procedure or Rank transformation column
						MeasureColumn mc = r.findMeasureColumn(s);
						if (mc == null)
						{
							logger.warn("Trying to use non-existent (or unmapped) measure (" + s + ") in an aggregate (" + Name
									+ "), not processing the aggregate");
							return;
						}
						String dataType = Util.getDBDataType(mc.DataType);
						StringBuilder sb = new StringBuilder();
						if (dataType != null)
						{
							String rule = mc.getAggregationRule(null);
							if(DatabaseConnection.isDBTypeMySQL(connection.DBType))
							{
								if (rule != null)
								{
									dataType = connection.getNumericType();
								}
								sb.append("CAST(NULL AS ");
								sb.append(dataType);
								sb.append(")");
							}
							else
							{
								if (rule != null && rule.equalsIgnoreCase("SUM") && dataType.equalsIgnoreCase("INTEGER"))
								{
									dataType = "BIGINT";
								}
								sb.append("CONVERT(");
								sb.append(dataType);
								sb.append(", NULL)");
							}
						} else
						{
							sb.append("NULL");
						}
						tq.addConstantColumn(sb.toString(), Util.replaceWithPhysicalString(s));
					}
				} else
				{
					q.addMeasureColumn(s, s, false);
				}
			}
			/* Add all filters */
			for (String s : Filters)
			{
				QueryFilter qf = r.findFilter(s);
				if (qf != null)
				{
					q.addFilter((QueryFilter) qf.clone());
					if (hasTransformations)
						tq.addFilter((QueryFilter) qf.clone());
				}
			}
		}
		if (q.isFullOuterJoin() && !connection.supportsFullOuterJoin())
			q.setSimulateOuterJoinUsingPersistedDQTs(true);
		if (tq != null)
		{
			/* Create the query aggregate with place holders for transformation columns */
			tqa = new QueryAggregate(tq, Name, expressionList, expandDerivedQueryTables, forceLevelKeys, connection, BuildFilter, dataDir, databasePath, imported);
			added = true;
			tqa.setStatus(status);			
			tqa.setRetainDQTs(retainDQTs);
			tqa.setInMemory(this.inMemory);
			if (persist && !imported)
				tqa.persist(LoadGroups, dropIfExists, allowIncremental, IncrementalFilter);
			tqa.removeAggregate(false);
			/* Create the query aggregate */
			qa = new QueryAggregate(q, Name, expressionList, expandDerivedQueryTables, forceLevelKeys, connection, BuildFilter, dataDir, databasePath, imported);
			qa.setStatus(status);
			// Make sure that both tqa and qa refer to the exactly same object that is added to the repository
			// This way, we can ensure that when removeAggregate method is called on tqa, it removes the metadata
			// from the repository correctly.
			tqa.setDimensionTables(qa.getDimensionTables());
			tqa.setMeasureTable(qa.getMeasureTable());
		} else
		{
			/* Create the query aggregate */
			qa = new QueryAggregate(q, Name, expressionList, expandDerivedQueryTables, forceLevelKeys, connection, BuildFilter, dataDir, databasePath, imported);
			qa.setStatus(status);
			qa.setInMemory(this.inMemory);
			added = true;
			qa.setRetainDQTs(retainDQTs);
			if (persist && !imported)
				qa.persist(LoadGroups, dropIfExists, allowIncremental, IncrementalFilter);
		}
		if (hasTransformations)
		{
			addTransformationColumns(r);
		}
		if (persist && hasTransformations && !imported)
		{
			JasperDataSourceProvider jdsp = new JasperDataSourceProvider(r);
			Map<String, Transformation> rankTransformations = new HashMap<String, Transformation>();
			for (AggTransformation agt : Transformations)
			{
				if (Rank.class.isInstance(agt.Transformations[0]))
					rankTransformations.put(agt.ColumnName, agt.Transformations[0]);
			}
			if (!rankTransformations.isEmpty())
			{
				// Return level keys
				List<String> keys = new ArrayList<String>();
				// Prune any unnecessary dimension columns from level keys
				for (DimensionColumnNav dcn : qa.getQuery().dimensionColumns)
				{
					if (!dcn.navigateonly && !dcn.constant)
					{
						Hierarchy h = r.findDimensionHierarchy(dcn.dimensionName);
						Level l = h.findLevelColumn(dcn.columnName);
						if (l != null)
						{
							boolean found = false;
							for (DimensionColumnNav dcn2 : qa.getQuery().dimensionColumns)
							{
								if (dcn.dimensionName.equals(dcn2.dimensionName) && !dcn.columnName.equals(dcn2.columnName) && !dcn2.navigateonly
										&& !dcn2.constant)
								{
									Level l2 = h.findLevelColumn(dcn2.columnName);
									if (l == l2)
									{
										if (l.Keys.length > 0 && l.Keys[0].ColumnNames.length == 1 && dcn2.columnName.equals(l.Keys[0].ColumnNames[0]))
											found = true;
									} else if (l2 != null && l.findLevel(l2) != null)
										found = true;
								}
							}
							if (!found)
								keys.add(dcn.displayName);
						}
					}
				}
				Rank.processRanks(r, connection, Database.getQualifiedTableName(connection, Util.replaceWithPhysicalString(Name)), rankTransformations, keys);				
			}
			for (AggTransformation agt : Transformations)
			{
				if (!Rank.class.isInstance(agt.Transformations[0]))
					agt.Transformations[0].executeTransformation(r, connection, session, jdsp);
			}
		}
		if (!added)
		{
			r.addQueryAggregate(qa);
			added = true;
		}
		processed = true;
	}

	public String getConnectionName() {
		return connectionName;
	}
	
	public String getPhysicalQuery()
	{
		return qa.getPhysicalQuery();
	}

	public String getLogicalQuery(Repository r)
	{
		if (Query)
			return LogicalQuery;
		StringBuilder sb = new StringBuilder();
		for (AggColumn ac : Columns)
		{
			sb.append("DC{").append(ac.Dimension).append(".").append(ac.Column).append("},");
		}
		for (String mc : Measures)
		{
			sb.append("M{").append(mc).append("},");
		}
		for (String f : Filters)
		{
			QueryFilter qf = r.findFilter(f);
			if (qf != null)
			{
				sb.append(qf.getQueryString(true)).append(',');
			}
		}
		if (sb.length() > 0)
			return sb.substring(0, sb.length() - 1);
		return sb.toString();
	}

	public Query getQuery()
	{
		return qa.getQuery();
	}

	public String[] getMeasures()
	{
		return Measures;
	}

	public boolean usesMeasureTable(String measureTableName)
	{
		return qa.usesLogicalTable(measureTableName);
	}

	public boolean usesDimensionTable(String dimTableName)
	{
		return qa.usesLogicalTable(dimTableName);
	}

	/**
	 * Create the necessary metadata for logical column definitions. Add the mappings to the dimension/measure tables as
	 * necessary.
	 */
	private void addTransformationColumns(Repository r) throws CloneNotSupportedException
	{
		for (AggTransformation agt : Transformations)
		{
			if (agt.Transformations != null)
			{
				// Make sure it is a logical column definition
				if ((agt.Transformations.length > 0) && (!Expression.class.isInstance(agt.Transformations[0])))
				{
					if (agt.Dimension != null) // Dimension column
					{
						DimensionTable dt = r.findDimensionTable(agt.Dimension + "_" + Util.replaceWithPhysicalString(Name));
						DimensionColumn[] newDimCols = new DimensionColumn[dt.DimensionColumns.length + 1];
						System.arraycopy(dt.DimensionColumns, 0, newDimCols, 0, dt.DimensionColumns.length);
						DimensionColumn propDC = r.findDimensionColumn(agt.Dimension, agt.ColumnName);
						DimensionColumn dc = (DimensionColumn) propDC.clone();
						dc.DisplayMap = null;
						if (LogicalColumnDefinition.class.isInstance(agt.Transformations[0]))
						{
							dc.PhysicalName = ((LogicalColumnDefinition) agt.Transformations[0]).getPhysicalFormula();
						} else
						{
							dc.PhysicalName = Util.replaceWithPhysicalString(dc.ColumnName);
						}
						dc.TableName = dt.TableName;
						dc.DimensionTable = dt;
						/* Clear any virtual column definitions as they are now instantiated */
						dc.VC = null;
						newDimCols[dt.DimensionColumns.length] = dc;
						r.addDimensionColumnToMap(dc);
					} else
					// Measure column
					{
						MeasureTable mt = r.findMeasureTableName(Util.replaceWithPhysicalString(Name) + "_AGG");
						MeasureColumn[] newMeasureCols = new MeasureColumn[mt.MeasureColumns.length + 1];
						System.arraycopy(mt.MeasureColumns, 0, newMeasureCols, 0, mt.MeasureColumns.length);
						MeasureColumn propMC = r.findMeasureColumn(agt.ColumnName);
						if (propMC == null)
						{
							logger.warn("Trying to use non-existent (or unmapped) measure (" + agt.ColumnName + ") in an aggregate (" + Name
									+ ") transformation, not processing the transformation");
							continue;
						}
						MeasureColumn mc = (MeasureColumn) propMC.clone();
						if (LogicalColumnDefinition.class.isInstance(agt.Transformations[0]))
						{
							mc.PhysicalName = ((LogicalColumnDefinition) agt.Transformations[0]).getPhysicalFormula();
						} else
						{
							mc.PhysicalName = Util.replaceWithPhysicalString(mc.ColumnName);
						}
						mc.TableName = mt.TableName;
						mc.MeasureTable = mt;
						newMeasureCols[mt.MeasureColumns.length] = mc;
						mt.MeasureColumns = newMeasureCols;
						r.addMeasureColumnToMap(mc);
					}
				}
			}
		}
	}

	public void update(Repository r, Session session, String loadGroupName, boolean expandDerivedQueryTables) throws Exception
	{
		if (!isProcessed())
		{
			logger.warn("Cannot update aggregate " + Name + " - it has not been processed.");
			return;
		}
		LoadGroup[] loadGroupsToUse = null;
		if (loadGroupName != null)
		{
			boolean found = false;
			if (LoadGroups != null && LoadGroups.length > 0)
			{
				for (LoadGroup lg : LoadGroups)
				{
					if (loadGroupName.equals(lg.getName()))
					{
						loadGroupsToUse = new LoadGroup[1];
						loadGroupsToUse[0] = lg;
						found = true;
						break;
					}
				}
			}
			if (!found)
			{
				throw new Exception("Load group \"" + loadGroupName + "\" is not applicable for aggregate " + this.getName() + ".");
			}
		}
		boolean hasTransformations = Transformations != null && Transformations.length > 0;
		if (hasTransformations)
		{
			tqa.updateAggregate(loadGroupsToUse, expandDerivedQueryTables, IncrementalFilter, true);
			if (hasTransformations)
			{
				addTransformationColumns(r);
			}
			JasperDataSourceProvider jdsp = new JasperDataSourceProvider(r);
			for (AggTransformation agt : Transformations)
			{
				agt.Transformations[0].executeTransformation(r, connection, session, jdsp);
			}
		} else
			qa.updateAggregate(loadGroupsToUse, expandDerivedQueryTables, IncrementalFilter, true);
	}

	private Transformation getTransformation(String dimName, String colName)
	{
		if (colName == null)
			return null;
		if (Transformations != null)
		{
			for (AggTransformation agt : Transformations)
			{
				if ((dimName == null && agt.Dimension == null) || (dimName != null && dimName.equals(agt.Dimension)))
				{
					if (colName.equals(agt.ColumnName))
					{
						if ((agt.Transformations != null) && (agt.Transformations.length > 0))
						{
							return agt.Transformations[0];
						}
					}
				}
			}
		}
		return null;
	}

	/**
	 * Drop the aggregate table from the database and remove associated metadata
	 * 
	 * @param r
	 * @throws SQLException
	 */
	public void drop(Repository r, boolean persist) throws SQLException
	{
		if (qa != null)
		{
			qa.removeAggregate(persist);
		}
		added = false;
		qa = null;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return Name;
	}

	public boolean isProcessed()
	{
		return processed;
	}

	public String getReplacedString(String result)
	{
		for (AggColumn ac : Columns)
		{
			if (result.indexOf(Name + ".\"" + ac.Column + "\"") >= 0)
				result = Util.replaceStr(result, Name + ".\"" + ac.Column + "\"", Util.replaceWithPhysicalString(Name) + "."
						+ Util.buildDimensionColumn(connection, ac.Dimension, ac.Column));
		}
		for (String s : Measures)
		{
			if (result.indexOf(Name + ".\"" + s + "\"") >= 0)
				result = Util.replaceStr(result, Name + ".\"" + s + "\"", Util.replaceWithPhysicalString(Name) + "." + Util.replaceWithPhysicalString(s));
		}
		return (result);
	}

	private LoadGroup[] findLoadGroups(Repository r, String[] lgNames)
	{
		if ((lgNames == null) || (lgNames.length <= 0))
		{
			return null;
		}
		LoadGroup[] lgs = new LoadGroup[lgNames.length];
		for (int i = 0; i < lgNames.length; i++)
		{
			lgs[i] = r.findLoadGroup(lgNames[i]);
			if (lgs[i] == null)
			{
				logger.warn("Could not find load group " + lgNames[i] + " that is referenced in aggregate " + Name);
			}
		}
		return lgs;
	}

	public boolean hasFilters()
	{
		return (Filters != null && Filters.length > 0);
	}

	public String[] getLoadGroupNames()
	{
		return LoadGroupNames;
	}

	public String[] getSubGroups()
	{
		return SubGroups;
	}

	public void setSubGroups(String[] subGroups)
	{
		SubGroups = subGroups;
	}

	public Status getStatus()
	{
		return status;
	}

	public void setStatus(Status status)
	{
		this.status = status;
	}
	
	
	public String getIncrementalFilter()
	{
		return IncrementalFilter;
	}

	public void setIncrementalFilter(String incrementalFilter)
	{
		IncrementalFilter = incrementalFilter;
	}

	public boolean isInMemory()
	{
		return inMemory;
	}

	public void setInMemory(boolean inMemory)
	{
		this.inMemory = inMemory;
	}

	public String getDataDir()
	{
		return dataDir;
	}

	public void setDataDir(String dataDir)
	{
		this.dataDir = dataDir;
	}

	public String getDatabasePath()
	{
		return databasePath;
	}

	public void setDatabasePath(String databasePath)
	{
		this.databasePath = databasePath;
	}

	public void setConnectionName(String connectionName)
	{
		this.connectionName = connectionName;
	}
	
	public void setConnection(DatabaseConnection dc)
	{
		connection = dc;
	}

	public boolean isImported() {
		return imported;
	}

	public void setImported(boolean imported) {
		this.imported = imported;
	}
	
	public boolean isQuery(){
		return Query;
	}
	
	public String getPhysicalName(Repository r)
	{		
		String physicalName =  r.getCurrentBuilderVersion()>=26 ? Util.replaceWithPhysicalString(connection,Name,r) :  Util.replaceWithPhysicalString(Name);
		if(r.getCurrentBuilderVersion()>=26)
		{
			if(Name!=null && Name.length() >= DatabaseConnection.getIdentifierLength(connection.DBType)-4){
				physicalName = connection.getHashedIdentifier(Name);
			}
		}
		return physicalName;
	}
}