package com.successmetricsinc.query;

import com.successmetricsinc.util.BaseException;

public class SessionVariableUnavailableException extends BaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public SessionVariableUnavailableException()
	{
		super();
	}
	
	public SessionVariableUnavailableException(String s)
	{
		super(s);
	}
	
	public SessionVariableUnavailableException(String s, Throwable cause)
	{
		super(s, cause);
	}
	
	public int getErrorCode()
	{
		return ERROR_SESSION_VARIABLE_UNAVAILABLE;
	}

}
