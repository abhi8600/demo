package com.successmetricsinc.query;

import java.io.Serializable;
import java.util.List;

import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.warehouse.SecurityFilter;

public class MeasureGrain implements Serializable 
{
	  public MeasureTableGrain[] measureTableGrains;
    public String measureTableName;
    public String measureColumnName;
    public boolean transactional;
    public boolean currentSnapshotOnly;
    public String[] LoadGroups;
    public SecurityFilter filter;
    
    public MeasureGrain(Element e, Namespace ns) throws Exception
    {
      Element mtgRoot = e.getChild("measureTableGrains", ns);
      if (mtgRoot != null)
      {
        @SuppressWarnings("rawtypes")
        List mtgList = mtgRoot.getChildren();
        measureTableGrains = new MeasureTableGrain[mtgList.size()];
        for (int mtgIdx = 0; mtgIdx < mtgList.size(); mtgIdx++)
        {
          Element mtge = (Element) mtgList.get(mtgIdx);
          MeasureTableGrain mtg = new MeasureTableGrain(mtge, ns);
          measureTableGrains[mtgIdx] = mtg;
        }
      }
      String s = e.getChildText("transactional",ns);
      if (s!=null)
      {
        transactional = Boolean.parseBoolean(s);
      }
      s = e.getChildText("currentSnapshotOnly",ns);
      if (s!=null)
      {
        currentSnapshotOnly = Boolean.parseBoolean(s);
      }
      Element eLoadGrps = e.getChild("LoadGroups",ns);
      if (eLoadGrps!=null)
      {
        List<Element> strLoadGrps = eLoadGrps.getChildren();
        LoadGroups = new String[strLoadGrps.size()];
        int index=0;
        for (Element elemLoadGrp : strLoadGrps)
        {         
          LoadGroups[index++] = elemLoadGrp.getText();
        }
      }
      if (e.getChild("filter",ns)!=null)
      {
        filter = new SecurityFilter(e.getChild("filter",ns), ns);
      }
      measureTableName = e.getChildText("measureTableName",ns);
    }
    
    public Element getMeasureGrainElement(Namespace ns)
    {
      Element e = new Element("Grain",ns);      
      Element childLvl2 = new Element("measureTableGrains", ns);
      e.addContent(childLvl2);
      
      for (MeasureTableGrain mtg : measureTableGrains) 
      {
        childLvl2.addContent(mtg.getMeasureTableGrainElement(ns));          
      }
      
      if (measureTableName!=null)
      {
        childLvl2 = new Element("measureTableName",ns);
        childLvl2.setText(measureTableName);
        e.addContent(childLvl2);
      }
      
      childLvl2 = new Element("transactional",ns);
      childLvl2.setText(String.valueOf(transactional));
      e.addContent(childLvl2);
      
      childLvl2 = new Element("currentSnapshotOnly",ns);
      childLvl2.setText(String.valueOf(currentSnapshotOnly));
      e.addContent(childLvl2);
      
      if (LoadGroups!=null)
      {
        childLvl2 = new Element("LoadGroups",ns);
        for (String grain_loadGrp : LoadGroups)
        {
          Element stringElement = new Element("string",ns);
          stringElement.addContent(grain_loadGrp);
          childLvl2.addContent(stringElement);
        }
        e.addContent(childLvl2);
      }
          
      if (filter!=null)
      {
        e.addContent(filter.getSecurityFilterElement("filter", ns));
      }
      
      return e;
    }
}
