/**
 * 
 */
package com.successmetricsinc.query;

import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.chrono.ISOChronology;

import com.successmetricsinc.transformation.integer.ToTime;

/**
 * @author agarrison
 *
 */
public class BirstInterval extends Number {
	public static final String INTERVAL_SUFFIX = "BirstInterval";

	private final Number value;
	private final String fields;
	private final String format;
	/**
	 * 
	 */
	public BirstInterval() {
		value = 0;
		fields = "";
		format = "";
	}
	
	public BirstInterval(Number value, String format) {
		this.value = value;
		
		if (format != null && format.toUpperCase().startsWith(ToTime.TO_TIME_FORMAT_PREFIX)) {
			String formatArgs[]=(format.substring(format.indexOf("(") + 1, format.lastIndexOf(")"))).split("','");
			String toTimeFields = formatArgs[0].substring(formatArgs[0].indexOf("'") + 1);
			String toTimeFormat = formatArgs[1].substring(0, formatArgs[1].indexOf("'"));
			this.fields = toTimeFields;
			this.format = toTimeFormat;
		}
		else {
			this.fields = null;
			this.format = format;
		}
	}
	
	public BirstInterval(Number value, String fields, String format) {
		this.value = value;
		this.fields = fields;
		this.format = format;
	}

	/* (non-Javadoc)
	 * @see java.lang.Number#doubleValue()
	 */
	@Override
	public double doubleValue() {
		return value.doubleValue();
	}

	/* (non-Javadoc)
	 * @see java.lang.Number#floatValue()
	 */
	@Override
	public float floatValue() {
		return value.floatValue();
	}

	/* (non-Javadoc)
	 * @see java.lang.Number#intValue()
	 */
	@Override
	public int intValue() {
		return value.intValue();
	}
	
	public Number getValue() {
		return value;
	}

	/* (non-Javadoc)
	 * @see java.lang.Number#longValue()
	 */
	@Override
	public long longValue() {
		return value.longValue();
	}

	public String getFields() {
		return fields;
	}

	public String getFormat() {
		return format;
	}

	public Object add(Object other) {
		if (other == null)
			return this;
		
		if (other instanceof BirstInterval) {
			if (this.value == null)
				return other;
			return new BirstInterval(value.doubleValue() + ((BirstInterval)other).value.doubleValue(), getFields(), getFormat());
		}
		
		return null;
	}
	
	public Number aggregate(String rule, BirstInterval other) {
		if (other == null || other.value == null || other.isNaN())
			return this;
		
		if (this.value == null || this.isNaN())
			return value;
		
		if (rule != null)
		{
			if (rule.equals("SUM") || rule.equals("COUNT") || rule.equals("AVG"))
				return (BirstInterval)add(value);
			if (rule.equals("MIN"))
				return doubleValue() < value.doubleValue() ? this : value;
			if (rule.equals("MAX"))
				return doubleValue() > value.doubleValue() ? this : value;
		}
		return (this);
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return value == null ? "" : value.toString();
	}
	
	public boolean isNaN() {
		return value == null ? false : Double.isNaN(value.doubleValue());
	}

	public boolean isInfinite() {
		return value == null ? false : Double.isInfinite(value.doubleValue());
	}
	
	public boolean isNull() {
		return value == null;
	}
	
	public String getFormattedValue(String pattern) {
		String fields = this.fields;
		String format = this.format;
		if (pattern != null && pattern.toUpperCase().startsWith(ToTime.TO_TIME_FORMAT_PREFIX)) {
			String formatArgs[]=(pattern.substring(format.indexOf("(") + 1, pattern.lastIndexOf(")"))).split("','");
			String toTimeFields = formatArgs[0].substring(formatArgs[0].indexOf("'") + 1);
			String toTimeFormat = formatArgs[1].substring(0, formatArgs[1].indexOf("'"));
			fields = toTimeFields;
			format = toTimeFormat;
		}
		return ToTime.evaluate(longValue(), fields, format);
	}
}
