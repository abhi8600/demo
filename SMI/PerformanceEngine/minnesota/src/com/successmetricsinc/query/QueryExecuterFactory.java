/**
 * 
 */
package com.successmetricsinc.query;

import java.util.Map;

import net.sf.jasperreports.engine.JRDataset;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.query.JRQueryExecuter;
import net.sf.jasperreports.engine.query.JRQueryExecuterFactory;

/**
 * @author bpeters
 * 
 */
public class QueryExecuterFactory implements JRQueryExecuterFactory
{
	/*
	 * (non-Javadoc)
	 * 
	 * @see net.sf.jasperreports.engine.query.JRQueryExecuterFactory#createQueryExecuter(net.sf.jasperreports.engine.JRDataset,
	 *      java.util.Map)
	 */
	@Override
	public JRQueryExecuter createQueryExecuter(JRDataset dataset, Map parameterMap) throws JRException
	{
		QueryExecuter qe = new QueryExecuter(dataset.getQuery().getText());
		return qe;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.sf.jasperreports.engine.query.JRQueryExecuterFactory#getBuiltinParameters()
	 */
	@Override
	public Object[] getBuiltinParameters()
	{
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.sf.jasperreports.engine.query.JRQueryExecuterFactory#supportsQueryParameterType(java.lang.String)
	 */
	@Override
	public boolean supportsQueryParameterType(String arg0)
	{
		// TODO Auto-generated method stub
		return false;
	}
}
