/**
 * $Id: DimensionExpressionColumn.java,v 1.7 2010-08-20 21:14:00 bpeters Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

public class DimensionExpressionColumn
{
	public String dimName;
	public String colName;
	public boolean sparse;
	public boolean olapLogicalMemberSet;
	
	public String getDimension()
	{
		return dimName;
	}
	
	public String getColumn()
	{
		return colName;
	}
	
	public boolean isSparse()
	{
		return sparse;
	}
	
	public boolean isOlapLogicalMemberSet()
	{
		return olapLogicalMemberSet;
	}
}