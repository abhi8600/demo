/**
 * $Id: Level.java,v 1.29 2012-11-12 18:25:20 BIRST\ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.jdom.Element;
import org.jdom.Namespace;
import com.successmetricsinc.Repository;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.warehouse.ForeignKey;

public class Level implements Serializable
{
	private static final long serialVersionUID = 1L;
	public String Name;
	public int Cardinality;
	public String[] ColumnNames;
	public boolean[] HiddenColumns;
	public Level[] Children;
	public Level[] ChildrenOrig;
	public List<Object> Parents;
	public LevelKey[] Keys;
	public String[] SharedChildren;
	private boolean isSharedChildrenTag;
	public boolean Degenerate;
	public int SCDType = 1;
	public Hierarchy Hierarchy;
	public boolean GenerateDimensionTable;
	public boolean GenerateInheritedCurrentDimTable;
	public boolean Locked;
	public boolean AutomaticallyCreatedAsDegenerate;
	public LevelAlias[] Aliases;
	public ForeignKey[] ForeignKeys;
	public String[] ForeignKeySources;

	public Level()
	{
	}

	public Level(Element e, Namespace ns, Object parent, Hierarchy h)
	{
		Name = e.getChildTextTrim("Name", ns);
		Parents = new ArrayList<Object>();
		Parents.add(parent);
		Hierarchy = h;
		Cardinality = Integer.parseInt(e.getChildText("Cardinality", ns));
		Degenerate = Boolean.valueOf(e.getChildText("Degenerate", ns));
		String s = e.getChildText("SCDType", ns);
		SCDType = s == null ? 1 : Integer.parseInt(s);
		Element cn = e.getChild("ColumnNames", ns);
		@SuppressWarnings("rawtypes")
		List childlist = cn.getChildren();
		int size = childlist.size();
		ColumnNames = new String[size];
		for (int j = 0; j < size; j++)
		{
			ColumnNames[j] = Util.intern(((Element) childlist.get(j)).getTextTrim());
		}
		cn = e.getChild("HiddenColumns", ns);
		if (cn != null)
		{
			childlist = cn.getChildren();
			size = childlist.size();
			HiddenColumns = new boolean[size];
			for (int j = 0; j < size; j++)
			{
				HiddenColumns[j] = Boolean.parseBoolean(((Element) childlist.get(j)).getTextTrim());
			}
		} else
		{
			HiddenColumns = new boolean[size];
		}
		Element elemFKeys = e.getChild("ForeignKeys",ns);
    if (elemFKeys!=null)
    {
      List l2 = elemFKeys.getChildren();
      ForeignKeys = new ForeignKey[l2.size()];
      for (int count2 = 0; count2 < l2.size(); count2++)
      {
        ForeignKey fk = new ForeignKey((Element) l2.get(count2), ns);
        ForeignKeys[count2] = fk;
      }
    }
    ForeignKeySources = Repository.getStringArrayChildren(e, "ForeignKeySources", ns);
    
    Element elemAliases = e.getChild("Aliases",ns);
    if (elemAliases!=null)
    {
      List l2 = elemAliases.getChildren();
      Aliases = new LevelAlias[l2.size()];
      for (int count2 = 0; count2 < l2.size(); count2++)
      {
        LevelAlias la = new LevelAlias((Element) l2.get(count2), ns);
        Aliases[count2] = la;
      }
    }
    
		cn = e.getChild("Children", ns);
		Children = new Level[cn.getChildren().size()];
		ChildrenOrig = new Level[cn.getChildren().size()];
		for (int i = 0; i < Children.length; i++)
		{
			Children[i] = new Level((Element) cn.getChildren().get(i), ns, this, h);
			ChildrenOrig[i] = Children[i];
		}
		SharedChildren = Repository.getStringArrayChildren(e, "SharedChildren", ns);
		isSharedChildrenTag = e.getChild("SharedChildren",ns)!=null?true:false;
		cn = e.getChild("Keys", ns);
		if (cn != null)
		{
			Keys = new LevelKey[cn.getChildren().size()];
			for (int i = 0; i < Keys.length; i++)
			{
				Keys[i] = new LevelKey((Element) cn.getChildren().get(i), ns);
				Keys[i].Level = this;
				if (i == 0)
					for (String ks : Keys[i].ColumnNames)
						h.primaryLevelKeys.put(ks, this);
			}
		}
		String temp = e.getChildText("GenerateDimensionTable",ns);
		if (temp!=null)
		{
			GenerateDimensionTable = Boolean.parseBoolean(temp);
		}
		temp = e.getChildText("GenerateInheritedCurrentDimTable",ns);
		if (temp!=null)
		{
			GenerateInheritedCurrentDimTable   = Boolean.parseBoolean(temp);
		}
		temp = e.getChildText("Locked",ns);
		if (temp!=null)
		{
			Locked   = Boolean.parseBoolean(temp);
		}
		temp = e.getChildText("AutomaticallyCreatedAsDegenerate",ns);
		if (temp!=null)
		{
			AutomaticallyCreatedAsDegenerate   = Boolean.parseBoolean(temp);
		}
	}
	
	public Element getLevelElement(Namespace ns)
	{
		Element e = new Element("Level",ns);
		
		Element child = new Element("Name",ns);
		child.setText(Name);
		e.addContent(child);
		
		child = new Element("Cardinality",ns);
		child.setText(String.valueOf(Cardinality));
		e.addContent(child);
		
		child = new Element("ColumnNames",ns);
		if (ColumnNames!=null && ColumnNames.length>0)
		{
			for (String colName : ColumnNames)
			{
				Element stringElement = new Element("string",ns);
				stringElement.setText(colName);
				child.addContent(stringElement);
			}
		}
		e.addContent(child);
		
		child = new Element("HiddenColumns",ns);
		if (HiddenColumns!=null && HiddenColumns.length>0)
		{
			for (boolean hidden : HiddenColumns)
			{
				Element boolElement = new Element("boolean",ns);
				boolElement.setText(String.valueOf(hidden));
				child.addContent(boolElement);
			}
		}
		e.addContent(child);
		
		child = new Element("Children",ns);
		if (ChildrenOrig != null && ChildrenOrig.length>0)
		{
			for (Level childLvl : ChildrenOrig)
			{
				Element childLvlElement = childLvl.getLevelElement(ns);				
				child.addContent(childLvlElement);
			}
		}
		e.addContent(child);
		
		child = new Element("Keys",ns);
		if (Keys!=null && Keys.length>0)
		{
			for (LevelKey lk : Keys)
			{
				Element lkElement = lk.getLevelKeyElement(ns);				
				child.addContent(lkElement);
			}
		}
		e.addContent(child);
		
		if (Aliases!=null && Aliases.length>0)
		{
		  child = new Element("Aliases",ns);
		  for (LevelAlias la : Aliases)
		  {
		    child.addContent(la.getLevelAliasElement(ns));
		  }
		  e.addContent(child);		  
		}
		
		if (SharedChildren!=null && SharedChildren.length>0)
		{		
			child = new Element("SharedChildren",ns);
			for (String sharedChild : SharedChildren)
			{
				Element stringElement = new Element("string",ns);
				stringElement.setText(sharedChild);
				child.addContent(stringElement);
			}			
			e.addContent(child);
		}	
		else if (isSharedChildrenTag)
		{
			child = new Element("SharedChildren",ns);
			e.addContent(child);
		}		
		
		child = new Element("GenerateDimensionTable",ns);
		child.setText(String.valueOf(GenerateDimensionTable));
		e.addContent(child);
		
		child = new Element("Degenerate",ns);
		child.setText(String.valueOf(Degenerate));
		e.addContent(child);
		
		child = new Element("SCDType",ns);
		child.setText(String.valueOf(SCDType));
		e.addContent(child);
		
		child = new Element("GenerateInheritedCurrentDimTable",ns);
		child.setText(String.valueOf(GenerateInheritedCurrentDimTable));
		e.addContent(child);
		
		child = new Element("Locked",ns);
		child.setText(String.valueOf(Locked));
		e.addContent(child);
		
		if (AutomaticallyCreatedAsDegenerate)
		{
			child = new Element("AutomaticallyCreatedAsDegenerate",ns);
			child.setText(String.valueOf(AutomaticallyCreatedAsDegenerate));
			e.addContent(child);
		}
		
		if (ForeignKeys!=null && ForeignKeys.length>0)
    {
      child = new Element("ForeignKeys",ns);
      for (ForeignKey fk : ForeignKeys)
      {
        child.addContent(fk.getForeignKeyElement(ns));
      }
      e.addContent(child);
    }
		
		if (ForeignKeySources!=null && ForeignKeySources.length>0)
    {
      e.addContent(Repository.getStringArrayElement("ForeignKeySources", ForeignKeySources, ns));
    }
		
		return e;
	}

	public String toString()
	{
		return (this.Name);
	}

	/**
	 * Return the number of level keys
	 * 
	 * @return
	 */
	public int numKeys()
	{
		return (Keys.length);
	}

	/**
	 * Find a level with a given column
	 * 
	 * @param cname
	 * @return
	 */
	public Level findLevelColumn(String cname)
	{
		for (int i = 0; i < ColumnNames.length; i++)
		{
			if (ColumnNames[i].equals(cname))
				return (this);
		}
		for (int i = 0; i < Children.length; i++)
		{
			Level l = Children[i].findLevelColumn(cname);
			if (l != null)
				return (l);
		}
		return (null);
	}

	/**
	 * Find a level with a given level key
	 * 
	 * @param cname
	 * @return
	 */
	public Level findLevelKeyColumn(String cname)
	{
		for (int i = 0; i < Keys.length; i++)
		{
			for (String s : Keys[i].ColumnNames)
				if (s.equals(cname))
					return (this);
		}
		for (int i = 0; i < Children.length; i++)
		{
			Level l = Children[i].findLevelKeyColumn(cname);
			if (l != null)
				return (l);
		}
		return (null);
	}

	/**
	 * Return whether a column is a surrogate key
	 * 
	 * @param cname
	 * @return
	 */
	public boolean isSurrogateKeyColumn(String cname)
	{
		for (int i = 0; i < Keys.length; i++)
		{
			if (Keys[i].SurrogateKey && Keys[i].ColumnNames[0].equals(cname))
				return (true);
		}
		for (int i = 0; i < Children.length; i++)
		{
			if (Children[i].isSurrogateKeyColumn(cname))
				return (true);
		}
		return (false);
	}

	/**
	 * Find a level with a given column used as primary level key
	 * 
	 * @param cname
	 * @return
	 */
	public Level findPrimaryLevelKey(String cname)
	{
		if (Keys != null && Keys.length > 0)
		{
			LevelKey plk = Keys[0];
			if (plk.ColumnNames != null && plk.ColumnNames.length > 0)
			{
				for (String colName : plk.ColumnNames)
				{
					if (colName.equals(cname))
					{
						return this;
					}
				}
			}
		}
		for (int i = 0; i < Children.length; i++)
		{
			Level l = Children[i].findPrimaryLevelKey(cname);
			if (l != null)
				return (l);
		}
		return (null);
	}

	/**
	 * Find a level with a given name at or below a given level
	 * 
	 * @param levelName
	 * @return
	 */
	public Level findLevel(String levelName)
	{
		if (Name.equals(levelName))
			return (this);
		for (Level cl : Children)
		{
			Level l = cl.findLevel(levelName);
			if (l != null)
				return (l);
		}
		return (null);
	}

	/**
	 * Find a level at or below a given level
	 * 
	 * @param levelName
	 * @return
	 */
	public Level findLevel(Level l)
	{
		if (this == l)
			return (this);
		for (int i = 0; i < Children.length; i++)
		{
			Level cl = Children[i].findLevel(l);
			if (cl != null)
				return (cl);
		}
		return (null);
	}

	/**
	 * Return the primary (first) level key
	 * 
	 * @return
	 */
	public LevelKey getPrimaryKey()
	{
		if (Keys.length == 0)
			return (null);
		return (Keys[0]);
	}

	public LevelKey getNaturalKey()
	{
		if (Keys.length == 0)
			return (null);
		for (LevelKey lk : Keys)
			if (!lk.SurrogateKey)
				return lk;
		return null;
	}

	public LevelKey getSurrogateKey()
	{
		if (Keys.length == 0)
			return (null);
		for (LevelKey lk : Keys)
			if (lk.SurrogateKey)
				return lk;
		return null;
	}

	/**
	 * Return the hierarchy for this level
	 * 
	 * @return
	 */
	public Hierarchy getHierarchy()
	{
		return (Hierarchy);
	}

	/**
	 * Expand metadata for shared level children
	 * 
	 * @param h
	 */
	public void setSharedChildren(Hierarchy h)
	{
		List<Level> llist = new ArrayList<Level>();
		for (String s : SharedChildren)
		{
			Level l = h.findLevel(s);
			if (l != null)
			{
				llist.add(l);
			}
		}
		if (llist.size() > 0)
		{
			Level[] oldList = Children;
			Children = new Level[oldList.length + llist.size()];
			for (int i = 0; i < oldList.length; i++)
				Children[i] = oldList[i];
			for (int i = 0; i < llist.size(); i++)
			{
				Children[oldList.length + i] = llist.get(i);
				llist.get(i).Parents.add(this);
			}
		}
		for (Level l : Children)
			l.setSharedChildren(h);
	}

	/**
	 * Get all decendent levels
	 * 
	 * @param list
	 */
	public void getAllDecendents(List<Level> list)
	{
		for (Level l : Children)
		{
			if (!list.contains(l))
				list.add(l);
			l.getAllDecendents(list);
		}
	}

	/**
	 * Get all parent levels
	 * 
	 * @param list
	 */
	public void getAllParents(List<Level> list)
	{
		for (Object o : Parents)
		{
			if (Level.class.isInstance(o))
			{
				if (list.contains((Level) o))
				{
					// Handle branching within hierarchy
					int index = list.indexOf((Level) o);
					list.remove(this);
					list.add(index, this);
				} else
				{
					list.add((Level) o);
				}
				((Level) o).getAllParents(list);
			}
		}
	}

	/**
	 * Return whether a single column is a level key (does not match for compound level keys)
	 * 
	 * @param columnName
	 * @return
	 */
	public boolean isKey(String columnName)
	{
		for (LevelKey lk : Keys)
		{
			if (lk.ColumnNames.length == 1 && lk.ColumnNames[0].equals(columnName))
				return (true);
		}
		return (false);
	}
	
	public class LevelAlias
	{
	  public String AliasName;
    public int AliasType; 
    public AliasLevelKey[] AliasKeys;
    
    public LevelAlias(Element e, Namespace ns)
    {
      AliasName= e.getChildText("AliasName",ns);
      if (e.getChildText("AliasType", ns)!=null)
      {
        AliasType = Integer.parseInt(e.getChildText("AliasType", ns));
      }
      if (e.getChild("AliasKeys", ns)!=null)
      {
        List keyList = e.getChild("AliasKeys", ns).getChildren();
        AliasKeys = new AliasLevelKey[keyList.size()];
        for (int index = 0; index < keyList.size(); index++)
        {
          AliasLevelKey alk = new AliasLevelKey((Element) keyList.get(index), ns);
          AliasKeys[index] = alk;
        }
      }
    }
    
    public Element getLevelAliasElement(Namespace ns)
    {
      Element e = new Element("AliasLevelKey",ns);
      
      Element child = new Element("AliasName",ns);
      child.setText(AliasName);
      e.addContent(child);
      
      if (AliasType!=0)
      {
        child = new Element("AliasType",ns);
        child.setText(String.valueOf(AliasType));
        e.addContent(child);
      }
      
      if (AliasKeys!=null && AliasKeys.length>0)
      {
        child = new Element("AliasKeys",ns);
        for (AliasLevelKey alk : AliasKeys)
        {
          child.addContent(alk.getAliasLevelKeyElement(ns));
        }
        e.addContent(child);
      }
      
      return e;
    }
	}
	
	public class AliasLevelKey
	{
	  public String LevelKeyColumn;
    public String AliasKeyColumn;
    
    public AliasLevelKey(Element e, Namespace ns)
    {
      LevelKeyColumn= e.getChildText("LevelKeyColumn",ns);
      AliasKeyColumn= e.getChildText("AliasKeyColumn",ns);
    }
    
    public Element getAliasLevelKeyElement(Namespace ns)
    {
      Element e = new Element("AliasLevelKey",ns);
      
      Element child = new Element("LevelKeyColumn",ns);
      child.setText(LevelKeyColumn);
      e.addContent(child);
      
      child = new Element("AliasKeyColumn",ns);
      child.setText(AliasKeyColumn);
      e.addContent(child);
      
      return e;
    }
    
	}
}