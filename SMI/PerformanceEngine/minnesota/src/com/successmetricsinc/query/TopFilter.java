	/**
	 * $Id: TopFilter.java,v 1.1 2012-05-22 00:19:30 agarrison Exp $
	 *
	 * Copyright (C) 2007-2012 Birst, Inc. All rights reserved.
	 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
	 */
package com.successmetricsinc.query;

import gnu.jel.CompiledExpression;
import gnu.jel.Evaluator;
import gnu.jel.Library;

import java.io.Serializable;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.regex.Pattern;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.engine.Header;
import com.successmetricsinc.engine.ModelDataSet;
import com.successmetricsinc.engine.SuccessModelInstance;
import com.successmetricsinc.util.BirstXMLSerializable;
import com.successmetricsinc.util.DateUtil;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class TopFilter extends BirstXMLSerializable implements Cloneable {


		private static final long serialVersionUID = 1L;
		private static final String TOP = "Top";
		private static final String PROMPTED = "Prompted";
		private static final String PROMPT_NAME = "PromptName";
		private static final String OPERAND = "Operand";
		private static final String OPERAND_DISPLAYVALUE = "Operand_DisplayValue";
		private static Logger logger = Logger.getLogger(DisplayFilter.class);

		private String promptName;
		private Object operand;
		private transient Pattern pattern;
		private transient boolean containsExpression;
		private transient Object compiledExpression;
		public transient String Operand_DisplayValue;
		public transient String alias;
		
		static {
		}
		
		public TopFilter()
		{
			// for cloning...
		}

		public void parseElement(XMLStreamReader parser) throws Exception {
			for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
				String elName = parser.getLocalName();
				if (PROMPT_NAME.equals(elName)) {
					this.promptName = XmlUtils.getStringContent(parser);
				} else if (OPERAND_DISPLAYVALUE.equals(elName))
				{
					this.Operand_DisplayValue = XmlUtils.getStringContent(parser);
				}
				else if (OPERAND.equals(elName)) {
					this.operand = XmlUtils.getStringContent(parser);
				}
			}
		}
		
		@SuppressWarnings("unchecked")
		public void parseElement(OMElement parent)
		{
			for (Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
				OMElement el = iter.next();
				String elName = el.getLocalName();
				if (OPERAND.equals(elName)) {
					this.operand = XmlUtils.getStringContent(el);
				}
				else if (PROMPT_NAME.equals(elName)) {
					this.promptName = XmlUtils.getStringContent(el);
				} else if (OPERAND_DISPLAYVALUE.equals(elName))
				{
					this.Operand_DisplayValue = XmlUtils.getStringContent(el);
				}
			}
		}
		public void parseElement(Element parent, Namespace ns) {
			this.operand = XmlUtils.getStringContent(parent, "Operand", ns);
			this.promptName = XmlUtils.getStringContent(parent, "PromptName", ns);
			
		}
		
		@Override
		public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
			OMElement filter = fac.createOMElement(TOP, ns);
			XmlUtils.addContent(fac, filter, OPERAND, this.getOperand() == null ? null : this.getOperand().toString(), ns);
			XmlUtils.addContent(fac, filter, PROMPT_NAME, this.promptName, ns);
			XmlUtils.addContent(fac, filter, OPERAND_DISPLAYVALUE, this.Operand_DisplayValue, ns);
			parent.addChild(filter);
		}

		/**
		 * Create a display filter predicate
		 * 
		 * @param columnName
		 * @param operator
		 * @param operand
		 */
		public TopFilter(String promptName, Object operand)
		{
			this.promptName = promptName;
			this.operand = operand;
		}

		/**
		 * set up the JEL compiled expression for this filter
		 */
		private boolean setExpression()
		{
			if (!DisplayExpression.jelSupported)
			{
				compiledExpression = null;
				return false;
			}
			try
			{
				Library lib = new Library(new Class[] { Math.class, String.class, StringProcess.class, Double.class, Integer.class },
										  new Class[] { FieldProvider.class },
										  new Class[] { Math.class, String.class, Double.class, Integer.class },
										  null, null);
				compiledExpression = Evaluator.compile(operand.toString(), lib);
			} catch (Exception ex)
			{
				logger.warn("Parser error in display filter for old query language expression: " + ex.getMessage() + ":" + operand.toString());
				return false;
			}
			return (compiledExpression != null);
		}

		public Object clone() throws CloneNotSupportedException
		{
			TopFilter df = (TopFilter) super.clone();
			df.operand = operand;
			df.promptName = promptName;
			df.pattern = pattern;
			df.compiledExpression = compiledExpression;
			
			return df;
		}
		
		public String toString()
		{
			if (isPrompted())
				return "TOP(" + promptName + ")";
			return "TOP(" + operand.toString() + ")";
		}

		
		public TopFilter replaceVariables(Repository r, Session session) throws CloneNotSupportedException
		{
			TopFilter returnFilter = this;
			if (operand != null)
			{
				String operandWithVariablesReplaced = r.replaceVariables(null, operand.toString());
				if (session != null)
				{
					operandWithVariablesReplaced = r.replaceVariables(session, operandWithVariablesReplaced);
				}
				if (!operandWithVariablesReplaced.equals(operand.toString()))
					
				if (operandWithVariablesReplaced.charAt(0) == '\'' && operandWithVariablesReplaced.charAt(operandWithVariablesReplaced.length() - 1) == '\'')
				{
					operandWithVariablesReplaced = operandWithVariablesReplaced.substring(1, operandWithVariablesReplaced.length() - 1);
				}
				if (!operandWithVariablesReplaced.equals(operand.toString()))
				{
					returnFilter = (TopFilter) this.clone();
					returnFilter.operand = operandWithVariablesReplaced;
				}
			}
			return returnFilter;
		}

		/**
		 * @return Returns whether filter is promptable.
		 */
		public boolean isPrompted()
		{
			return promptName != null && Util.hasNonWhiteSpaceCharacters(promptName);
		}

		/**
		 * Return whether two filters are logically equal
		 * 
		 * @param qf
		 * @return
		 */
		public boolean equals(Object o)
		{
			if (this == o)
				return true;
			if (!(o instanceof TopFilter))
				return false;
			TopFilter df = (TopFilter) o;
			return ((df.isPrompted() && isPrompted() && df.promptName.equals(promptName)) ||
					(!df.isPrompted() && !isPrompted() && df.operand.toString().equals(operand.toString())));
		}
		
		public int hashCode()
		{
			  assert false : "hashCode not designed";
			  return 42; // any arbitrary constant will do 
		}
		
		/**
		 * @return Returns the operand.
		 */
		public Object getOperand()
		{
			return operand;
		}


		/**
		 * @return the containsExpression
		 */
		public boolean containsExpression()
		{
			return containsExpression;
		}
		
		/**
		 * for filter display in adhoc
		 * @param columnName
		 */
		public String getDisplayString()
		{
			return toString();
		}
		

		public String getPromptName() {
			return promptName;
		}

		public void setPromptName(String promptName) {
			if (promptName != null && !Util.hasNonWhiteSpaceCharacters(promptName)) {
				promptName = null;
			}
			this.promptName = promptName;
		}

		@Override
		public BirstXMLSerializable createNew() {
			return new TopFilter();
		}
	}

