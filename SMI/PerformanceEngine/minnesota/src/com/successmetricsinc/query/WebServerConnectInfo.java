/**
 * $Id: WebServerConnectInfo.java,v 1.4 2010-04-27 13:46:10 mpandit Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

/**
 * @author Brad Peters
 * 
 */
public class WebServerConnectInfo
{
	public String serverURL;
	public String serverUsername;
	public String serverPassword;
	public String spaceID;
	
	public String toString()
	{
		return serverURL + " (" + serverUsername + ", " + (serverPassword != null ? "****" : "**") + spaceID == null ? "" : (", " + spaceID) +")";
	}
}
