/**
 * $Id: RowMapCacheEntry.java,v 1.13 2011-01-18 18:58:14 sfoo Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.io.Externalizable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import java.util.Map;

import com.mongodb.BasicDBObject;
import com.successmetricsinc.util.Util;

/**
 * @author Brad Peters
 * 
 */
public class RowMapCacheEntry implements Externalizable, BasicDBObjectSerializer
{
	private static final long serialVersionUID = 1L;
	private String key;
	private String fileName;
	private transient File cacheFile;

	public RowMapCacheEntry()
	{
	}

	/**
	 * Initialize a row map cache entry
	 * 
	 * @param key
	 * @param name
	 *            Either the filename for the cache entry (if not new) or the directory of the cache entry (if new)
	 * @param newEntry
	 * @throws IOException
	 */
	public RowMapCacheEntry(String key, String cacheDir) throws IOException
	{
		this.key = key;
		cacheFile = ResultSetCache.createCacheFile("rowmap", cacheDir);
		this.fileName = cacheFile.getName();
	}

	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException
	{
		key = (String) in.readObject();
		fileName = (String) in.readObject();
		cacheFile = null;
	}

	public void writeExternal(ObjectOutput out) throws IOException
	{
		out.writeObject(key);
		out.writeObject(fileName);
	}

	public void fromBasicDBObject(BasicDBObject obj)
	{
		key = obj.getString("key");
		fileName = obj.getString("fileName");
		cacheFile = null;
	}
	
	public BasicDBObject toBasicDBObject()
	{
		BasicDBObject o = new BasicDBObject();
		o.put("key", key);
		o.put("fileName", fileName);
		return o;
	}
	
	/**
	 * Retrieve the row maps associated with an entry
	 * 
	 * @return
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public synchronized Map<String, Map<String, Integer>> getMaps(String path) throws IOException, ClassNotFoundException
	{
		if (cacheFile == null)
			cacheFile = new File(path + File.separator + fileName);
		FileInputStream in = null;
		ObjectInputStream s = null;
		try
		{
			in = new FileInputStream(cacheFile);
			s = new ObjectInputStream(in);
			Map<String, Map<String, Integer>> loadMap = (Map<String, Map<String, Integer>>) s.readObject();
			return loadMap;
		}
		finally
		{
			if (s != null)
				s.close();
			if (in != null)
				in.close();
		}
	}

	/**
	 * Save the row maps associated with an entry
	 * 
	 * @param maps
	 * @throws IOException
	 */
	public synchronized void saveMaps(Map<String, Map<String, Integer>> maps) throws IOException
	{
		// Save new copy of cache file map
		FileOutputStream fo = new FileOutputStream(cacheFile);
		ObjectOutput s = new ObjectOutputStream(fo);
		s.writeObject(maps);
		s.close();
		fo.close();
	}
	
	public static String mapsToPrettyString(Map<String, Map<String, Integer>> maps)
	{
		StringBuilder sb = new StringBuilder();
		Iterator<String> iterator = maps.keySet().iterator();
		List<String> keys = new ArrayList<String>();
		while(iterator.hasNext())
		{
			keys.add(iterator.next());
		}
		
		for(String key : keys)
		{
			sb.append("Key Name:"+key+Util.LINE_SEPARATOR);
			sb.append("\t"+Util.LINE_SEPARATOR);
			Map<String, Integer> subMap = maps.get(key);
			Iterator<String> iter = subMap.keySet().iterator();
			List<String> subKeys = new ArrayList<String>();
			while(iter.hasNext())
			{
				subKeys.add(iter.next());
			}
			for(String subKey: subKeys)
			{
				Integer rowIndex = subMap.get(subKey);
				sb.append("Key Value:"+subKey+", Row Index="+rowIndex+Util.LINE_SEPARATOR);
			}
		}

		return sb.toString();
	}
	
	public String getFileName()
	{
		return fileName;
	}
}
