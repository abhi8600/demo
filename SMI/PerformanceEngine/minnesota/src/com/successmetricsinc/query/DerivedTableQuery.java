/**
 * 
 */
package com.successmetricsinc.query;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.query.Database.DatabaseType;
import com.successmetricsinc.security.EncryptionService;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.RepositoryException;
import com.successmetricsinc.util.Util;

/**
 * @author bpeters
 * 
 */
public class DerivedTableQuery
{
	private static Logger logger = Logger.getLogger(DerivedTableQuery.class);
	private Query q;
	private StringBuilder query;
	private Repository r;
	private List<Navigation> navList;
	private List<MeasureColumnNav> derivedMeasureColumns;
	private List<OrderBy> orderByClauses;
	private List<GroupBy> groupByClauses;
	private List<QueryFilter> filters;
	private Session session;
	private Map<DatabaseConnection, List<TempTable>> tempTableMap;
	private List<QueryColumn> resultProjectionList = new ArrayList<QueryColumn>();
	private FederatedJoinQueryHelper fjqh = null;

	public DerivedTableQuery(Query q, List<Navigation> navList, Repository r, List<OrderBy> orderByClauses, List<GroupBy> groupByClauses,
			List<MeasureColumnNav> derivedMeasureColumns, List<QueryFilter> filters, Session session)
	{
		this.q = q;
		this.r = r;
		this.navList = navList;
		this.orderByClauses = orderByClauses;
		this.groupByClauses = groupByClauses;
		this.derivedMeasureColumns = derivedMeasureColumns;
		this.filters = filters;
		this.session = session;
	}
	
	public DerivedTableQuery(Query q, Repository r, DatabaseType dbType, int topn, List<QueryColumn> resultProjectionList, List<QueryFilter> filters, 
			List<OrderBy> orderByClauses, List<DisplayOrder> dorders, DatabaseConnection dbc) throws SyntaxErrorException
	{
		this.resultProjectionList = resultProjectionList;
		// Google Analytics
		if (dbType == DatabaseType.GoogleAnalytics)
		{
			query = GoogleAnalyticsQuery.generateQuery(topn, resultProjectionList, filters, orderByClauses);
			return;
		} else if (DatabaseConnection.isDBTypeXMLA(dbType))
		{
			// Microsoft Analysis Services
			query = MDXQuery.generateQuery(r, dbType, topn, resultProjectionList, filters, dbc, orderByClauses, q.isValidateOnly(), q);
			q.requiresDispalySortingForXMLA = dbc.requiresDispalySortingForXMLA();
			return;
		}
	}

	/**
	 * Generate a derived table query for a given measure table
	 * 
	 * @param dtProjectionList
	 *            Projection list for derived table
	 * @param mt
	 *            Measure table
	 * @param nav
	 *            Navigation data structure
	 * @param noOuterQuery
	 *            Whether there is an outer query or not
	 * @param dbType
	 *            Database type of the navigation connection
	 * @return Query string
	 * @throws SyntaxErrorException
	 * @throws RepositoryException 
	 * @throws SQLException 
	 * @throws ScriptException 
	 * @throws IOException
	 */
	public void generate(List<QueryColumn> dtProjectionList, MeasureTable mt, Navigation nav, boolean noOuterQuery, DatabaseConnection dbc,
			boolean specifyDataTypes, boolean isSubQuery) throws CloneNotSupportedException, BaseException, SQLException, IOException
	{
		if(nav.containsFederatedJoin())
		{
			fjqh = new FederatedJoinQueryHelper(r, session, this, (noOuterQuery ? q : null));
			q.setFederatedJoinQueryHelper(fjqh);
			fjqh.generate(nav, filters, specifyDataTypes);
			return;
		}
		if(!q.isExpandDerivedQueryTables())
		{
			specifyDataTypes = true;
		}
		/*
		 * See if this is a query that is at the grain of an aggregate and doesn't need to be aggregated
		 */
		boolean isOnlyAggregate = false;
		if (mt != null && mt.QueryAgg != null && !mt.QueryAgg.containsCountDistinctMeasure())
		{
			isOnlyAggregate = true;
			for (DimensionTable dt : nav.getDimensionTables())
			{
				if (dt.QueryAgg != mt.QueryAgg)
				{
					isOnlyAggregate = false;
					break;
				}
			}
			if (!mt.QueryAgg.isAtGrain(nav))
				isOnlyAggregate = false;
			if(mt.TableSource != null && mt.TableSource.contentFilters != null && mt.TableSource.contentFilters.size() > 0)
			{
				isOnlyAggregate = false;
			}
			if (filters != null && isOnlyAggregate)
			{
				for (QueryFilter qf : filters)
				{
					if (qf.isTooComplex() || qf.containsMeasure())
					{
						isOnlyAggregate = false;
						break;
					}
				}
			}
		}
		QueryMap tableMap = new QueryMap();
		if (dbc != null)
			tableMap.setDBType(dbc.DBType);
		/*
		 * Figure out if multiple levels of aggregation are necessary
		 */
		List<Level> levelList = new ArrayList<Level>();
		List<QueryColumn> levelqcList = new ArrayList<QueryColumn>();
		List<DimensionRule> aggRuleList = new ArrayList<DimensionRule>();
		q.getAggregationLevels(dtProjectionList, nav.getDimColumns(), levelList, levelqcList, aggRuleList);
		// Process FROM clause to ensure tables are mapped first
		StringBuilder from = new StringBuilder();
		from.append(" FROM ");
		boolean firstT = true;
		q.vcDimensionTables = new ArrayList<DimensionTable>();
		AtomicInteger encloseOuterClause = new AtomicInteger(0);
		if (dbc.DBType == DatabaseType.ODBCExcel)
		{
			for (int l = 0; l < nav.getDimensionTables().size(); l++)
			{
				encloseOuterClause.incrementAndGet();
				from.append('(');
			}
		}
		/*
		 * Add the measure table if necessary
		 */
		List<String> lookupJoins = new ArrayList<String>();
		StringBuilder sourceFilterWhere = null;
		int aliasIndex = 1;
		if (mt != null)
		{
			if (firstT)
				firstT = false;
			else
				from.append(',');
			if (mt.TableSource.Filters != null)
			{
				sourceFilterWhere = new StringBuilder();
				for (TableSourceFilter tsf : mt.TableSource.Filters)
				{
					/*
					 * Make sure that this isn't a current load filter that shouldn't be applied
					 */
					if (tsf.CurrentLoadFilter)
					{
						boolean excludeFilter = false;
						for (Hierarchy h : q.getRepository().getHierarchies())
						{
							if (!h.FilterForAllLoads)
								continue;
							for (DimensionColumnNav dcn : q.dimensionColumns)
							{
								if (dcn.dimensionName.equals(h.DimensionName))
								{
									excludeFilter = true;
									break;
								}
							}
							//None of the dimension columns refer to a transaction hierarchy directly so now
							//check if there is a reference in filters
							if(!excludeFilter && q.filters != null && !q.filters.isEmpty())
							{
								for(QueryFilter qf : q.filters)
								{
									if(qf.containsDimension(h.DimensionName))
									{
										excludeFilter = true;
										break;
									}
								}
							}
							//Check the measure column navs for any filters
							if(!excludeFilter && q.measureColumns != null && !q.measureColumns.isEmpty())
							{
								for(MeasureColumnNav mcn : q.measureColumns)
								{
									if(mcn.filter != null && mcn.filter.containsDimension(h.DimensionName))
									{
										excludeFilter = true;
										break;
									}
								}
							}
							
						}
						if (excludeFilter)
							continue;
					}
					// if security filter , apply exclusion rule principle
					boolean isSecurityFilter = false;
					if (tsf.SecurityFilter)
					{
						isSecurityFilter = true;
						if (!Query.applySecurityFilter(r, tsf, session, mt.Imported, q.isAggregate))
						{
							// skip to the next filter if the logged in user groups contain the excluded group
							// for security filter
							continue;
						}
					}
					if (sourceFilterWhere.length() > 0)
						sourceFilterWhere.append(" AND ");
					String filter = tsf.Filter;
					for (int j = 0; j < mt.TableSource.Tables.length; j++)
					{				
						if(mt.Type == MeasureTable.OPAQUE_VIEW)
						{
							filter = Util.replaceStr(filter, mt.TableSource.Tables[j].PhysicalName + ".", tableMap.getMap(mt) + ".");
						}
						else
						{
							filter = Util.replaceStr(filter, mt.TableSource.Tables[j].PhysicalName + ".", tableMap.getMap(mt.TableSource.Tables[j]) + ".");
						}
					}
					// de-construct the Q construct when logical query is directly inserted into table source filter
					// It will replace the session variables also
					boolean isWhereFilter = true;
					if (filter.indexOf("Q{") > 0)
					{	
						boolean isQueryOnSameConn = true;
						if (r.isLiveAccessSecurityFilter())
						{							
							try
							{
								String filterLogicalQry = filter.substring(filter.indexOf("Q{") + 2, filter.lastIndexOf("}"));
								boolean isNewQueryLanguage = filterLogicalQry.toLowerCase().startsWith("select");
								if (isNewQueryLanguage)
								{
									filterLogicalQry = Util.convertMulValueVariablesToNewLanguageSyntax(r, filterLogicalQry);
								}
								filterLogicalQry = QueryString.preProcessQueryString(filterLogicalQry, r, session);

								AbstractQueryString filteraqs = AbstractQueryString.getInstance(r, filterLogicalQry);
								Query filterQryObj = null;
								if (isNewQueryLanguage)
								{
									filterQryObj = filteraqs.getQuery();
								}
								else
								{
									filterQryObj=  filteraqs.processQueryString(filterLogicalQry, session);
								}
								filterQryObj.navigateQuery(true, false, session, true);
								List<DatabaseConnection> connectionList = filterQryObj.getConnections();
								if (connectionList!=null && !connectionList.isEmpty())
								{
									DatabaseConnection liveAccessConn = connectionList.get(0);
									if (!liveAccessConn.equals(mt.TableSource.connection))
									{
										ResultSetCache rsc = ResultSetCache.getInstance(r.getApplicationIdentifier(), false);
										int qrsMaxRecords = r.getServerParameters().getMaxRecords();
										QueryResultSet qrs = rsc.getQueryResultSet(filterQryObj, qrsMaxRecords, r.getServerParameters().getMaxQueryTime(), null, null, null,
												session, true, false, r.getServerParameters().getDisplayTimeZone(), false);
										if (qrs == null || !qrs.isValid())
											throw new Exception ("Exception executing query against live access ");
										
										nav.setNavExpires(qrs.getCurrentExpiresValue());

										int columnCount = qrs.getColumnNames().length;
										List<String> columnTypes = new ArrayList<String>();
										List<String> physicalColumnNames = new ArrayList<String>();
										String columnNames[] = qrs.getColumnNames();
										int columnDataTypes[] = qrs.getColumnDataTypes();

										for (int index=0;index<columnCount;index++)
										{
											String columnType = Util.getJDBCType(columnDataTypes[index]);
											if (Database.isVarcharType(columnType))
											{
												columnType = columnType + "(2000)";
											}
											else if (columnType.equalsIgnoreCase("timestamp") || columnType.equalsIgnoreCase("datetime"))												
											{
												columnType = "date";
											}
											columnTypes.add(columnType);
											physicalColumnNames.add(Util.replaceWithPhysicalString(liveAccessConn, columnNames[index]));
										}

										List<String> columnCompressionEncodingsTemp = DatabaseConnection.isDBTypeParAccel(liveAccessConn.DBType) ? new ArrayList<String>() : null;

										String tableLogicalName = filterQryObj.getKey(session);
										String tablePhysicalName = "TMP_"+ EncryptionService.getMD5(tableLogicalName, false).toUpperCase();
										int identifierMaxLen = DatabaseConnection.getIdentifierLength(mt.TableSource.connection.DBType);
										if (tablePhysicalName.length()>identifierMaxLen)
										{
											tablePhysicalName = EncryptionService.getMD5(tablePhysicalName, false).toUpperCase();
											char[] cs = tablePhysicalName.toCharArray();
											if (cs[0] >= '0' && cs[0] <= '9')
												tablePhysicalName = 'X' + tablePhysicalName;
										}

										boolean createTempTable = Database.createTable(r, mt.TableSource.connection, tablePhysicalName,physicalColumnNames,columnTypes, false, false, columnCompressionEncodingsTemp, null, null);

										if (createTempTable)
											logger.debug("Table created : " + tablePhysicalName);
										else 
											logger.debug("Table already exists : " + tablePhysicalName);

										Database.populateLiveAccessTmpTable(qrs, tablePhysicalName, mt.TableSource.connection, physicalColumnNames);

										if (DatabaseConnection.isDBTypeInfoBright(dbc.DBType))
										{
											MeasureColumn mc = null;
											
											if (tsf.LogicalColumnName!=null)
											{
												mc = mt.findColumn(mt.InheritPrefix + tsf.LogicalColumnName);
											}
											
											if (mc!=null)
											{			
												String ttAliasName = tableMap.getMap(tablePhysicalName) + (aliasIndex++);	
												lookupJoins.add(dbc.getInnerJoin() + " " + tablePhysicalName + " " + ttAliasName + " ON " + ttAliasName + "." + physicalColumnNames.get(0) + "="
													+ q.getMeasureString(dbc, false, mc, null, null, null, tableMap, lookupJoins, null));
												isWhereFilter = false;
											}
										}
										if (isWhereFilter)
										{
											String liveAccessTmpTableQry = getTempTablePhyQry(mt.TableSource.connection, tablePhysicalName, physicalColumnNames);
											
											filter = filter.replace(filter.substring(filter.indexOf("Q{"), filter.lastIndexOf("}")+1), liveAccessTmpTableQry);
										}
										
										Map<DatabaseConnection, List<TempTable>> tempTableMap = q.getTempTableMap();
										
										if (tempTableMap == null)
										{
											tempTableMap = new HashMap<DatabaseConnection, List<TempTable>>();
										}
										List<TempTable> ttlist = tempTableMap.get(mt.TableSource.connection);
										if (ttlist==null)
										{
											ttlist = new ArrayList<TempTable>();
										}
										TempTable tt = new TempTable();
										tt.tableName = tablePhysicalName;
										tt.liveAccessTmpTable = true;
										ttlist.add(tt);
										tempTableMap.put(mt.TableSource.connection, ttlist);
										q.setTempTableMap(tempTableMap);

										isQueryOnSameConn = false;
										
									}
								}						

							} catch (Exception ex)
							{
								logger.warn("Exception during Q{LogicalQuery} to physical query conversion. Logical Query :" + filter, ex);
							}
						}
						if (!r.isLiveAccessSecurityFilter() || isQueryOnSameConn)
						{
							if (DatabaseConnection.isDBTypeInfoBright(dbc.DBType))
							{								
								MeasureColumn mc = null;
								
								if (tsf.LogicalColumnName!=null)
								{
									mc = mt.findColumn(mt.InheritPrefix + tsf.LogicalColumnName);
								}
												
								if (mc!=null)
								{
									Map<DatabaseConnection, List<TempTable>> tempTableMap = q.getTempTableMap();
									if (tempTableMap == null)
										tempTableMap = new HashMap<DatabaseConnection, List<TempTable>>();
									List<TempTable> ttlist = tempTableMap.get(dbc);
									if (ttlist == null)
									{
										ttlist = new ArrayList<TempTable>();
										tempTableMap.put(dbc, ttlist);
									}
									q.setTempTableMap(tempTableMap);
									
									String logicalQry = filter.substring(filter.indexOf("Q{"), filter.lastIndexOf("}")+1);
									AbstractQueryString aqs = AbstractQueryString.getInstance(r, logicalQry, r.isUseNewQueryLanguage());
									String replacestr = aqs.replaceQueryString(logicalQry, true, session, true, false, null, null, null);	
									long x = System.currentTimeMillis();
									ttlist = tempTableMap.get(dbc);
									if (ttlist == null)
									{
										ttlist = Collections.synchronizedList(new ArrayList<TempTable>());
										tempTableMap.put(dbc, ttlist);
									}
									TempTable tt = dbc.getTempTableForQueryFilter(q.getRepository(), mc, x, ttlist, replacestr);
									replacestr = "SELECT * FROM " + tt.tableName;
									if (!ttlist.contains(tt))
									{
										ttlist.add(tt);
									}
									
									String ttAliasName = tableMap.getMap(tt.tableName) + (aliasIndex++);									
									lookupJoins.add(dbc.getInnerJoin() + " " + tt.tableName + " " + ttAliasName + " ON " + ttAliasName + "." + tt.pname + "="
											+ q.getMeasureString(dbc, false, mc, null, null, null, tableMap, lookupJoins, null));
									isWhereFilter = false;
								}
							}
							if (isWhereFilter)
							{
								AbstractQueryString aqs = AbstractQueryString.getInstance(r, filter, r.isUseNewQueryLanguage());
								filter = aqs.replaceQueryString(filter, true, session, true, false, null, null, null);
							}
						}
					} else
					{
						// Replace any session variables - pass the flag to process specific for security filter
						// quote the varchar and datetime values
						if(isSecurityFilter)
						{
							filter = r.replaceVariables(session, filter, false, null, null, true, dbc);
						}
						else
						{
							filter = r.replaceVariables(session, filter);
						}
						
					}
					if (isWhereFilter)
						sourceFilterWhere.append(filter);
				}
			}
			String mtQueryTableString = mt.getQueryTableString(mt.TableSource.connection, tableMap, null, r, session);
			/*
			 * If there is no source filter or there are multiple table source definitions, use the measure table
			 * query string as is.
			 */
			boolean useTableString = true;
			useTableString = useTableString
					|| (sourceFilterWhere == null || sourceFilterWhere.length() <= 0 || (mt.TableSource != null && mt.TableSource.Tables != null && mt.TableSource.Tables.length > 1));
			if (useTableString)
			{
				from.append(mtQueryTableString);
			} else
			{
				/**
				 * If there is a source filter present, convert into a subquery for measure table in the form of (
				 * SELECT * FROM <measure-table-phyical-name> ) <meaure-table-physical-alias>. We are
				 * doing this whenever there is a source filter.
				 */
				int idx = mtQueryTableString.lastIndexOf(' ');
				String tableMapString = "";
				if (idx > 0)
				{
					tableMapString = mtQueryTableString.substring(idx + 1);
				}
				from.append(" ( SELECT * FROM ");
				from.append(mtQueryTableString);
				from.append(" WHERE ");
				from.append(sourceFilterWhere.toString());
				from.append(" ) ");
				from.append(tableMapString);
				sourceFilterWhere = null;
			}
		}
		/*
		 * Now, add all the dimension columns
		 */
		List<JoinResult> joinList = new ArrayList<JoinResult>();
		List<DimensionTable> joinDimensionTables = new ArrayList<DimensionTable>();
		List<Boolean> joinVirtual = new ArrayList<Boolean>();
		for (int i = 0; i < nav.getDimensionTables().size(); i++)
		{
			DimensionTable dim = (DimensionTable) nav.getDimensionTables().get(i);
			// Add security filters for dimension tables if any
			if (dim.TableSource.Filters != null)
			{
				if (sourceFilterWhere == null)
				{
					sourceFilterWhere = new StringBuilder();
				}
				for (TableSourceFilter tsf : dim.TableSource.Filters)
				{
					if (tsf.SecurityFilter)
					{
						if (session == null || !Query.applySecurityFilter(r, tsf, session, dim.Imported, q.isAggregate))
						{
							// skip to the next filter if the logged in user
							// groups contain the excluded group for security filter
							continue;
						}
					}
					
					String dimTablefilter = tsf.Filter;
					for (int j = 0; j < dim.TableSource.Tables.length; j++)
					{
						dimTablefilter = Util.replaceStr(dimTablefilter, dim.TableSource.Tables[j].PhysicalName + ".", tableMap
								.getMap(dim.TableSource.Tables[j])
								+ ".");
					}
					// de-construct the Q construct when logical query is directly inserted into table source filter
					// It will replace the session variables also
					boolean isWhereFilter = true;
					if (dimTablefilter.indexOf("Q{") > 0)
					{	
						boolean isQueryOnSameConn = true;
						if (r.isLiveAccessSecurityFilter())
						{							
							try
							{
								String filterLogicalQry = dimTablefilter.substring(dimTablefilter.indexOf("Q{") + 2, dimTablefilter.lastIndexOf("}"));
								boolean isNewQueryLanguage = filterLogicalQry.toLowerCase().startsWith("select");
								if (isNewQueryLanguage)
								{
									filterLogicalQry = Util.convertMulValueVariablesToNewLanguageSyntax(r, filterLogicalQry);
								}
								filterLogicalQry = QueryString.preProcessQueryString(filterLogicalQry, r, session);

								AbstractQueryString filteraqs = AbstractQueryString.getInstance(r, filterLogicalQry);
								Query filterQryObj = null;
								if (isNewQueryLanguage)
								{
									filterQryObj = filteraqs.getQuery();
								}
								else
								{
									filterQryObj=  filteraqs.processQueryString(filterLogicalQry, session);
								}
								filterQryObj.navigateQuery(true, false, session, true);
								List<DatabaseConnection> connectionList = filterQryObj.getConnections();
								if (connectionList!=null && !connectionList.isEmpty())
								{
									DatabaseConnection liveAccessConn = connectionList.get(0);
									if (!liveAccessConn.equals(dim.TableSource.connection))
									{
										ResultSetCache rsc = ResultSetCache.getInstance(r.getApplicationIdentifier(), false);
										int qrsMaxRecords = r.getServerParameters().getMaxRecords();
										QueryResultSet qrs = rsc.getQueryResultSet(filterQryObj, qrsMaxRecords, r.getServerParameters().getMaxQueryTime(), null, null, null,
												session, true, false, r.getServerParameters().getDisplayTimeZone(), false);
										if (qrs == null || !qrs.isValid())
											throw new Exception ("Exception executing query against live access ");

										nav.setNavExpires(qrs.getCurrentExpiresValue());
										
										int columnCount = qrs.getColumnNames().length;
										List<String> columnTypes = new ArrayList<String>();
										List<String> physicalColumnNames = new ArrayList<String>();
										String columnNames[] = qrs.getColumnNames();
										int columnDataTypes[] = qrs.getColumnDataTypes();
										
										for (int index=0;index<columnCount;index++)
										{
											String columnType = Util.getJDBCType(columnDataTypes[index]);
											if (Database.isVarcharType(columnType))
											{
												columnType = columnType + "(2000)";
											}
											else if (columnType.equalsIgnoreCase("timestamp") || columnType.equalsIgnoreCase("datetime"))												
											{
												columnType = "date";
											}
											columnTypes.add(columnType);
											physicalColumnNames.add(Util.replaceWithPhysicalString(liveAccessConn, columnNames[index]));
										}

										List<String> columnCompressionEncodingsTemp = DatabaseConnection.isDBTypeParAccel(liveAccessConn.DBType) ? new ArrayList<String>() : null;

										String tableLogicalName = filterQryObj.getKey(session);
										String tablePhysicalName = "TMP_"+ EncryptionService.getMD5(tableLogicalName, false).toUpperCase();
										int identifierMaxLen = DatabaseConnection.getIdentifierLength(dim.TableSource.connection.DBType);
										if (tablePhysicalName.length()>identifierMaxLen)
										{
											tablePhysicalName = EncryptionService.getMD5(tablePhysicalName, false).toUpperCase();
											char[] cs = tablePhysicalName.toCharArray();
											if (cs[0] >= '0' && cs[0] <= '9')
												tablePhysicalName = 'X' + tablePhysicalName;
										}

										boolean createTempTable = Database.createTable(r, dim.TableSource.connection, tablePhysicalName,physicalColumnNames,columnTypes, true, false, columnCompressionEncodingsTemp, null, null);

										if (createTempTable)
											logger.debug("Table created : " + tablePhysicalName);
										else 
											logger.debug("Table already exists : " + tablePhysicalName);

										Database.populateLiveAccessTmpTable(qrs, tablePhysicalName, dim.TableSource.connection, physicalColumnNames);
										
										if (DatabaseConnection.isDBTypeInfoBright(dbc.DBType))
										{
											DimensionColumn dc = null;
											if(tsf.LogicalColumnName != null)
											{
												dc = r.findDimensionColumn(dim.DimensionName, tsf.LogicalColumnName);
											}
											if(dc != null)
											{			
												String ttAliasName = tableMap.getMap(tablePhysicalName) + (aliasIndex++);
												lookupJoins.add(dbc.getInnerJoin() + " " + tablePhysicalName + " " + ttAliasName + " ON " + ttAliasName + "." + physicalColumnNames.get(0) + "="
													+ q.getDimensionColumnString(dbc, dc, q.getVcDimensionTables(), tableMap, lookupJoins));
												isWhereFilter = false;
											}
										}
										if (isWhereFilter)
										{
											String liveAccessTmpTableQry = getTempTablePhyQry(dim.TableSource.connection, tablePhysicalName, physicalColumnNames);

											dimTablefilter = dimTablefilter.replace(dimTablefilter.substring(dimTablefilter.indexOf("Q{"), dimTablefilter.lastIndexOf("}")+1), liveAccessTmpTableQry);
										}
										
										Map<DatabaseConnection, List<TempTable>> tempTableMap = q.getTempTableMap();
										
										if (tempTableMap == null)
										{
											tempTableMap = new HashMap<DatabaseConnection, List<TempTable>>();
										}
										List<TempTable> ttlist = tempTableMap.get(dim.TableSource.connection);
										if (ttlist==null)
										{
											ttlist = new ArrayList<TempTable>();
										}
										TempTable tt = new TempTable();
										tt.tableName = tablePhysicalName;
										tt.liveAccessTmpTable = true;
										ttlist.add(tt);
										tempTableMap.put(dim.TableSource.connection, ttlist);
										q.setTempTableMap(tempTableMap);
										
										isQueryOnSameConn = false;
									}
								}						

							} catch (Exception ex)
							{
								logger.warn("Exception during Q{LogicalQuery} to physical query conversion. Logical Query :" + dimTablefilter, ex);
							}
						}
						if (!r.isLiveAccessSecurityFilter() || isQueryOnSameConn)
						{
							try
							{								
								if (DatabaseConnection.isDBTypeInfoBright(dbc.DBType))
								{
									DimensionColumn dc = null;
									if(tsf.LogicalColumnName != null)
									{
										dc = r.findDimensionColumn(dim.DimensionName, tsf.LogicalColumnName);
									}
									if(dc != null)
									{
										Map<DatabaseConnection, List<TempTable>> tempTableMap = q.getTempTableMap();
										if (tempTableMap == null)
											tempTableMap = new HashMap<DatabaseConnection, List<TempTable>>();
										List<TempTable> ttlist = tempTableMap.get(dbc);
										if (ttlist == null)
										{
											ttlist = new ArrayList<TempTable>();
											tempTableMap.put(dbc, ttlist);
										}
										q.setTempTableMap(tempTableMap);
										
										String logicalQry = dimTablefilter.substring(dimTablefilter.indexOf("Q{"), dimTablefilter.lastIndexOf("}")+1);
										AbstractQueryString aqs = AbstractQueryString.getInstance(r, logicalQry, r.isUseNewQueryLanguage());
										String replacestr = aqs.replaceQueryString(logicalQry, true, session, true, false, null, null, null);	
										long x = System.currentTimeMillis();
										ttlist = tempTableMap.get(dbc);
										if (ttlist == null)
										{
											ttlist = Collections.synchronizedList(new ArrayList<TempTable>());
											tempTableMap.put(dbc, ttlist);
										}
										TempTable tt = dbc.getTempTableForQueryFilter(q.getRepository(), dc, x, ttlist, replacestr);										
										replacestr = "SELECT * FROM " + tt.tableName;
										if (!ttlist.contains(tt))
										{
											ttlist.add(tt);
										}
										
										String ttAliasName = tableMap.getMap(tt.tableName) + (aliasIndex++);
										lookupJoins.add(dbc.getInnerJoin() + " " + tt.tableName + " " + ttAliasName + " ON " + ttAliasName + "." + tt.pname + "="
												+ q.getDimensionColumnString(dbc, dc, q.getVcDimensionTables(), tableMap, lookupJoins));
										isWhereFilter = false;
									}
									else
									{
										logger.debug("Couldn't find logical dimension column for: " + tsf.LogicalColumnName + ". Will use IN clause instead of inner join.");
									}
								}
								if (isWhereFilter)
								{
									AbstractQueryString aqs = AbstractQueryString.getInstance(r, dimTablefilter, r.isUseNewQueryLanguage());
									dimTablefilter = aqs.replaceQueryString(dimTablefilter, true, session, true, false, null, null, null);
								}
							} catch (Exception ex)
							{
								logger.warn("Exception during Q{LogicalQuery} to physical query conversion. Logical Query :" + dimTablefilter, ex);
							}
						}
					} else
					{
						// Replace any session variables
						if(tsf.SecurityFilter)
						{
							// hack to pass dbc only for security filter so that for security filters, varible string gets db specific escaped							
							dimTablefilter = r.replaceVariables(session, dimTablefilter, false, null, null, true, dbc);
						}
						else
						{
							dimTablefilter = r.replaceVariables(session, dimTablefilter);
						}
					}
					// Replace any session variables
					// dimTablefilter = r.replaceVariables(session, dimTablefilter);
					if (isWhereFilter)
					{
						if (sourceFilterWhere.length() > 0)
						{
							sourceFilterWhere.append(" AND ");
						}
						sourceFilterWhere.append(dimTablefilter);
					}
				}
				if (sourceFilterWhere.length() == 0)
					sourceFilterWhere = null;
			}
			// Add dimension table to list if has virtual columns
			if (dim.hasVirtualColumns(nav.getQueryColumnList()))
			{
				q.vcDimensionTables.add(dim);
			}
			/*
			 * Add the join
			 */
			if (mt != null)
			{
				boolean virtual = false;
				if (dim.hasVirtualColumns(nav.getQueryColumnList()))
				{
					virtual = true;
				}
				JoinResult jr = r.getJoin(mt, dim, tableMap, virtual, nav.getLevelMap().get(dim.DimensionName), session);
				if (jr == null)
				{
					logger.warn("Missing join for " + mt.TableName + " (" + mt.PhysicalName + ") and " + dim.DimensionName);
					return;
				}
				if (jr.levelSpecificJoin)
					q.usesLevelSpecificJoin = true;
				if (!jr.redundant || virtual)
				{
					// from.append(getJoinString(dbc, jr, nav, dim, mt, tableMap, virtual, encloseOuterClause));
					joinList.add(jr);
					joinDimensionTables.add(dim);
					joinVirtual.add(virtual);
				} else
				{
					/*
					 * If this is a redundant join and both the dimension table and measure table are opaque view
					 * queries, add the measure table's map for the dimension table. We do not compare the underlying
					 * opaque view queries because they might differ in formatting details (spaces, tabs, new lines
					 * etc.). The assumption here is that the configuration will use a redundant join between opaque
					 * views of dimension table and measure table only if the queries match exactly.
					 */
					if (dim.Type == DimensionTable.OPAQUE_VIEW && mt.Type == MeasureTable.OPAQUE_VIEW)
					{
						tableMap.setMap(dim, tableMap.getMap(mt));
					} else
					{
						/*
						 * If this is a redundant join, don't specify the table source, but add all the maps for the
						 * measure table for all of the table source names of the dimension table
						 */
						for (TableDefinition td : dim.TableSource.Tables)
						{
							boolean found = false;
							for (TableDefinition mtd : mt.TableSource.Tables)
							{
								if (mtd.PhysicalName.equals(td.PhysicalName))
								{
									found = true;
									if (sourceFilterWhere != null)
										sourceFilterWhere = new StringBuilder(Util.replaceStr(sourceFilterWhere.toString(), tableMap.getMap(td), tableMap.getMap(mtd)));
									tableMap.setMap(td, tableMap.getMap(mtd));
									break;
								}
							}
							/*
							 * If the table name in the dimension is different than the one in the fact, use the first
							 * one in the fact (the first table is assumed primary) - could be an alias
							 */
							if (!found)
								tableMap.setMap(td, tableMap.getMap(mt.TableSource.Tables[0]));
						}
					}
				}
			} else
			{
				if (i > 0)
					from.append(',');
				// Reference the dimension table
				from.append(dim.getQueryTableString(r, nav.getQueryColumnList(), tableMap, null, null, session));
			}
		}
		
		if (DatabaseConnection.isDBTypeParAccel(dbc.DBType))
		{
			/*
			 * For ParAccel, optimization to move outer joins first in the join path. Having them later slows down
			 * queries
			 */
			List<JoinResult> newJoinList = new ArrayList<JoinResult>();
			List<DimensionTable> newJoinDimensionTables = new ArrayList<DimensionTable>();
			List<Boolean> newJoinVirtual = new ArrayList<Boolean>();
			for (int j = 0; j < joinList.size(); j++)
			{
				if (joinList.get(j).type != JoinType.Inner)
				{
					newJoinList.add(joinList.get(j));
					newJoinDimensionTables.add(joinDimensionTables.get(j));
					newJoinVirtual.add(joinVirtual.get(j));
				}
			}
			for (int j = 0; j < joinList.size(); j++)
			{
				if (!newJoinList.contains(joinList.get(j)))
				{
					newJoinList.add(joinList.get(j));
					newJoinDimensionTables.add(joinDimensionTables.get(j));
					newJoinVirtual.add(joinVirtual.get(j));
				}
			}
			joinList = newJoinList;
			joinDimensionTables = newJoinDimensionTables;
			joinVirtual = newJoinVirtual;
		}
		for (int j = 0; j < joinList.size(); j++)
		{
			from.append(getJoinString(dbc, joinList.get(j), nav, joinDimensionTables.get(j), mt, tableMap, joinVirtual.get(j), encloseOuterClause));
		}
		while (encloseOuterClause.getAndDecrement() > 0)
			from.append(')');
		// Get list of measure expressions
		List<String> melist = new ArrayList<String>();
		Set<DimensionColumn> meColumns = new HashSet<DimensionColumn>();
		for (int i = 0; i < nav.getQueryColumnList().size(); i++)
		{
			QueryColumn qc = (QueryColumn) nav.getQueryColumnList().get(i);
			// Only for dimension columns
			if (qc.type == QueryColumn.DIMENSION)
			{
				DimensionColumn dc = (DimensionColumn) qc.o;
				if ((dc.MeasureExpression != null) && (dc.MeasureExpression.length() > 0))
				{
					if (!meColumns.contains(dc))
					{
						melist.add(dc.MeasureExpression);
						meColumns.add(dc);
					}
				}
			}
		}
		// Generate WHERE/HAVING clauses
		boolean firstWhere = true;
		StringBuilder where = new StringBuilder();
		// Generate Projection List
		StringBuilder plist = new StringBuilder();
		boolean firstColumn = true;		
		Set<String> measureWheres = new HashSet<String>();
		if (q.colIndex > 0) {
			for (int j = 0; j >= -1; j = (j < 0) ? (j - 1) : (j == q.colIndex - 1 ? -1 : j + 1))
			{
				for (int i = 0; i < dtProjectionList.size(); i++)
				{
					QueryColumn qc = (QueryColumn) dtProjectionList.get(i);
					if (qc.columnIndex == j)
					{
						String name = q.columnMap.getMap(qc);
						if (qc.type == QueryColumn.DIMENSION)
						{
							DimensionColumn dc = (DimensionColumn) qc.o;
							if (firstColumn)
								firstColumn = false;
							else
								plist.append(',');
							String colName = q.getDimensionColumnString(dbc, dc, q.vcDimensionTables, tableMap, lookupJoins);
							if (specifyDataTypes)
							{
								colName = q.getNameWithSpecificDataType(colName, qc, dbc);
							}
							plist.append(colName);
							plist.append(dbc.getTableReference(name, isSubQuery));
							resultProjectionList.add(qc);
						} else if (qc.type == QueryColumn.MEASURE)
						{
							/*
							 * Note, don't qualify measure columns - allows for formula
							 */
							MeasureColumn mc = (MeasureColumn) qc.o;
							/*
							 * If it's a hidden column and there is only one derived table query, then don't put in
							 * projection list
							 */
							if (!(qc.hidden && navList.size() == 1 && derivedMeasureColumns.isEmpty()))
							{
								if (mc.MeasureTable == mt)
								{
									if (firstColumn)
										firstColumn = false;
									else
										plist.append(',');
									List<String> whereStrings = new ArrayList<String>();
									String colName = q.getMeasureString(dbc, !isOnlyAggregate, mc, melist, q, qc.filter, tableMap, lookupJoins, whereStrings).toString();
									if (specifyDataTypes)
									{
										colName = q.getNameWithSpecificDataType(colName, qc, dbc);
									}
									plist.append(colName);
									resultProjectionList.add(qc);
									if (qc.name != null)
									{
										plist.append(dbc.getTableReference(name, isSubQuery));
									}
									/*
									 * Physical filters are generated on measure columns in order to efficiently deal with case when statements.
									 * Previously, we were persisting the measure columns used in physical filters along with aggregate and then
									 * appending the physical filters as where conditions to the physical filters. However, it was not generating
									 * the proper query in some cases and hence we have persisted the physical filters as a part of the aggregate 
									 * We are putting this behind a builder version to make sure it does not break existing cases.
									 * The below code will make sure that the physical filter gets appended during time of aggregate persist
									 */
									if (mc.PhysicalFilter != null && mc.PhysicalFilter.length() > 0 
											&& ((r.getCurrentBuilderVersion()<=31 && !q.isAggregate) || (r.getCurrentBuilderVersion()>=32 && (!q.isAggregate || (q.isAggregate && (mc.SessionVariables == null || mc.SessionVariables.isEmpty()))))))
									{
										if (!firstWhere)
											where.append(" AND ");
										else
										{
											where.append(" WHERE ");
											firstWhere = false;
										}
										String formula = mc.PhysicalFilter;
										for (TableDefinition td : mc.MeasureTable.TableSource.Tables)
											formula = Util.replaceStr(formula, td.PhysicalName + ".", tableMap.getMap(td) + ".");
										where.append("(" + formula + ")");
									}
									if (!whereStrings.isEmpty())
									{
										String formula = whereStrings.get(0);
										if (!measureWheres.contains(formula))
										{
											if (!firstWhere)
												where.append(" AND ");
											else
											{
												where.append(" WHERE ");
												firstWhere = false;
											}
												measureWheres.add(formula);
											for (TableDefinition td : mc.MeasureTable.TableSource.Tables)
												formula = Util.replaceStr(formula, td.PhysicalName + ".", tableMap.getMap(td) + ".");
											where.append("(" + formula + ")");
										}
									}
								}
							}
						} else if (qc.type == QueryColumn.CONSTANT_OR_FORMULA)
						{
							if (firstColumn)
								firstColumn = false;
							else
								plist.append(',');
							plist.append((String) qc.o);
							resultProjectionList.add(qc);
							if (qc.name != null)
							{
								plist.append(dbc.getTableReference(name, isSubQuery));
							}
						}
					}
				}
			}
		}
		// Add the additional clauses for multiple levels of aggregation
		for (int i = 0; i < levelqcList.size(); i++)
		{
			DimensionColumn dc = (DimensionColumn) levelqcList.get(i).o;
			if (firstColumn)
				firstColumn = false;
			else
				plist.append(',');
			String colName = q.getDimensionColumnString(dbc, dc, q.vcDimensionTables, tableMap, lookupJoins).toString();
			if (specifyDataTypes)
			{
				colName = q.getNameWithSpecificDataType(colName, levelqcList.get(i), dbc);
			}
			plist.append(colName);
			resultProjectionList.add(levelqcList.get(i));
			plist.append(dbc.getTableReference(q.columnMap.getMap(levelqcList.get(i)), isSubQuery));
		}
		// Add the filters
		StringBuilder having = new StringBuilder();
		boolean firstHaving = true;
		for (QueryFilter qf : filters)
		{
			/* Make sure this filter doesn't contain a measure filter that hasn't been navigated */
			if (!qf.hasMeasureFilter() || qf.hasMeasureFilter(nav.getMeasureColumnNav().filter)
					|| qf.hasMeasureFilter(nav.getMeasureFiltersRemovedInConsolidation()))
			{
				boolean exclude = false;
				// Exclude filters that are redundant with content filters
				if (mt != null && mt.TableSource.contentFilters != null)
					for (QueryFilter cf : mt.TableSource.contentFilters)
					{
						if (cf.getOperator().equals("=") && qf.equalsUsingVariables(cf, session, r, false))
						{
							exclude = true;
							break;
						}
					}
				if (!exclude)
				{
					String whereStr = qf.getWhereFilterString(dbc, !isOnlyAggregate, q, tableMap, nav, lookupJoins);
					String havingStr = qf.getHavingFilterString(dbc, q, mt, melist, q.fullOuterJoin && !noOuterQuery, tableMap);
					if (whereStr != null)
					{
						if (!firstWhere)
							where.append(" AND ");
						else
						{
							where.append(" WHERE ");
							firstWhere = false;
						}
						where.append(whereStr);
					}
					if (havingStr != null)
					{
						if (!firstHaving)
							having.append(" AND ");
						else
						{
							having.append(" HAVING ");
							firstHaving = false;
						}
						having.append(havingStr);
					}
				}
			}
		}
		if (sourceFilterWhere != null && sourceFilterWhere.length() > 0)
		{
			if (where.length() == 0)
				where.append(" WHERE " + sourceFilterWhere);
			else
				where.append(" AND " + sourceFilterWhere);
		}
		// Add GROUP BYs
		StringBuilder groupby = new StringBuilder();
		if (!isOnlyAggregate)
		{
			/*
			 * Ensure that the group by still applies (may have to be pruned because the projection list item was pruned
			 * - in a subquery other than the first one. Also, don't add group by's for dimension columns that are
			 * constants
			 */
			List<GroupBy> gbClauses = new ArrayList<GroupBy>(groupByClauses.size());
			for (GroupBy gb : groupByClauses)
			{
				for (QueryColumn qc : dtProjectionList)
				{
					QueryColumn gbqc = gb.getQueryColumn();
					if (gbqc == null || qc == gbqc || qc.o == gbqc.o)
					{
						if (!gb.getDimensionColumn().Constant)
							gbClauses.add(gb);
						break;
					}
				}
			}
			boolean firstGroupBy = true;
			/*
			 * Add any new group bys generated from navigation
			 */
			List<GroupBy> gbs = new ArrayList<GroupBy>(gbClauses);
			GroupBy.addToList(gbs, nav.getNewGroupBys());
			if (gbs.size() + levelqcList.size() > 0)
				groupby.append(" GROUP BY ");
			for (int i = 0; i < gbs.size(); i++)
			{
				if (firstGroupBy)
					firstGroupBy = false;
				else
					groupby.append(',');
				GroupBy gb = (GroupBy) gbs.get(i);
				groupby.append(gb.getGroupByString(dbc, q, tableMap, lookupJoins));
			}
			// Add additional GROUP BYs if needed for multiple levels of aggregation
			for (int i = 0; i < levelqcList.size(); i++)
			{
				GroupBy gb = new GroupBy(levelqcList.get(i));
				if (firstGroupBy)
					firstGroupBy = false;
				else
					groupby.append(',');
				groupby.append(gb.getGroupByString(dbc, q, tableMap, lookupJoins));
			}
		}
		boolean pushdownTopAndOrderBy = q.canResultInMultipleQueries(session) && q.hasTop();
		
		// Add ORDER BYs (Only if not superseded by order by in an outer query)
		StringBuilder orderby = new StringBuilder();
		if (noOuterQuery || pushdownTopAndOrderBy)
		{
			/* Make sure no duplicates - no two order bys that navigate to the same underlying column */
			Set<String> obList = new HashSet<String>();
			if (!orderByClauses.isEmpty())
				orderby.append(" ORDER BY ");
			boolean added = false;
			for (int i = 0; i < orderByClauses.size(); i++)
			{
				OrderBy ob = (OrderBy) orderByClauses.get(i);
				String s = ob.getOrderByString(dbc, !isOnlyAggregate, q, mt, melist, tableMap, lookupJoins);
				if (s!= null && !obList.contains(s))
				{
					if (i > 0 && added)
					{
						orderby.append(',');
					}
					orderby.append(s);
					obList.add(s);
					added = true;
				}
			}
			if(!added)
			{
				orderby = new StringBuilder();
			}
		}
		query = new StringBuilder();
		query.append("SELECT ");
		query.append(plist);
		query.append(from);
		// Add lookup joins
		for (String s : lookupJoins)
			query.append(" " + s);
		query.append(where);
		query.append(groupby);
		// Correct for Infobright bug with having clause and no group by
		if (!DatabaseConnection.isDBTypeInfoBright(dbc.DBType) || groupby.length() > 0)
			query.append(having);
		if((noOuterQuery || pushdownTopAndOrderBy) && levelqcList.isEmpty())
		{
			query.append(orderby);
		}
		// Now, loop through and add any additional levels of aggregation needed
		boolean useQuotedString = dbc.requiresQuotedIdentifier();
		while (!levelqcList.isEmpty())
		{
			// Generate Projection List
			StringBuilder wrapPList = new StringBuilder();
			StringBuilder wrapGBList = new StringBuilder();
			Map<OrderBy, String> pnameByOrderByClause = new HashMap<OrderBy, String>();
			boolean wrapFirstColumn = true;
			boolean wrapFirstGBColumn = true;
			if (q.colIndex > 0) {
				for (int j = 0; j >= -1; j = (j < 0) ? (j - 1) : (j == q.colIndex - 1 ? -1 : j + 1))
				{
					for (int k = 0; k < dtProjectionList.size(); k++)
					{
						QueryColumn qc = (QueryColumn) dtProjectionList.get(k);
						if (qc.columnIndex == j)
						{
							String name = useQuotedString ? dbc.getColumnName(q.columnMap.getMap(qc), isSubQuery) : q.columnMap.getMap(qc);
							if (wrapFirstColumn)
								wrapFirstColumn = false;
							else
								wrapPList.append(',');
							if (qc.type == QueryColumn.DIMENSION || qc.type == QueryColumn.CONSTANT_OR_FORMULA)
							{
								wrapPList.append("T.");
								wrapPList.append(name);
								wrapPList.append(dbc.getTableReference(q.columnMap.getMap(qc), isSubQuery));
								if (qc != levelqcList.get(0))
								{
									if (wrapFirstGBColumn)
										wrapFirstGBColumn = false;
									else
										wrapGBList.append(',');
									wrapGBList.append("T.");
									wrapGBList.append(name);
									if (noOuterQuery)
									{
										OrderBy.addToProjectionNameByOrderByClauseMap(orderByClauses, qc, "T." + name, pnameByOrderByClause);
									}
								}
							} else if (qc.type == QueryColumn.MEASURE)
							{
								MeasureColumn mc = (MeasureColumn) qc.o;
								/*
								 * Use aggregation, if needed
								 */
								String aggString = mc.getAggregationString(dbc, "T." + name, null, aggRuleList.get(0).getAggRule());
								wrapPList.append(aggString);
								wrapPList.append(dbc.getTableReference(q.columnMap.getMap(qc), isSubQuery));
								if (noOuterQuery)
								{
									OrderBy.addToProjectionNameByOrderByClauseMap(orderByClauses, qc, aggString, pnameByOrderByClause);
								}
							}
						}
					}
				}
			}
			/*
			 * Add additional columns if needed
			 */
			for (int j = 1; j < levelqcList.size(); j++)
			{
				QueryColumn qc = levelqcList.get(j);
				String name = useQuotedString ? dbc.getColumnName(q.columnMap.getMap(qc), isSubQuery) : q.columnMap.getMap(qc);
				if (wrapFirstColumn)
					wrapFirstColumn = false;
				else
					wrapPList.append(',');
				if (qc.type == QueryColumn.DIMENSION)
				{
					wrapPList.append("T.");
					wrapPList.append(name);
					wrapPList.append(dbc.getTableReference(q.columnMap.getMap(qc), isSubQuery));
					if (qc != levelqcList.get(0))
					{
						if (wrapFirstGBColumn)
							wrapFirstGBColumn = false;
						else
							wrapGBList.append(',');
						wrapGBList.append("T.");
						wrapGBList.append(name);
					}
				}
			}
			String obStr = "";
			if(noOuterQuery)
			{
				obStr = OrderBy.buildOrderByStrFromMap(orderByClauses, pnameByOrderByClause);
			}
			StringBuilder nquery = new StringBuilder();
			nquery.append("SELECT ");
			nquery.append(wrapPList);
			nquery.append(" FROM (");
			nquery.append(query);
			nquery.append(") T");
			if (wrapGBList.length() > 0)
			{
				nquery.append(" GROUP BY ");
				nquery.append(wrapGBList);
			}
			query = new StringBuilder(nquery);
			levelqcList.remove(0);
			levelList.remove(0);
			aggRuleList.remove(0);
			query.append(obStr);
		}
		if (!noOuterQuery && q.hasTop() && pushdownTopAndOrderBy)
		{
			if (dbc.supportsTop())
				query.replace(0, "SELECT".length(), "SELECT TOP " + q.getPhysicalTopn(dbc));
			else if (dbc.supportsLimit())
				query.append(" LIMIT " + q.getPhysicalTopn(dbc) + " ");
			else if (dbc.supportsFetchFirstRows())
				query.append(" FETCH FIRST " + q.getPhysicalTopn(dbc) + " ROWS ONLY ");
			else if (dbc.supportsRowNum())
				Query.wrapWithRownum(query, q.getPhysicalTopn(dbc));
		}
		
		String s = r.replaceSynonyms(query.toString());
		query = new StringBuilder(r.replaceVariables(session, s));
	}		
	
	private String getTempTablePhyQry(DatabaseConnection dbc,String phyTblName,List<String> columnNames)
	{
		StringBuilder strBuilder = new StringBuilder("");
		try
		{
			String tblQualifiedName = dbc.Schema==null? phyTblName : dbc.Schema+"."+phyTblName;
			strBuilder.append("SELECT ");
			int columnCount = columnNames.size();
			for (int index=0;index<columnCount;index++)
			{
				if ((index+1)==columnCount)
				{
					strBuilder.append(columnNames.get(index));
				}
				else
				{
					strBuilder.append(columnNames.get(index)+",");
				}
			}
			strBuilder.append(" FROM "+tblQualifiedName);
		}
		catch (Exception e)
		{
			throw e;
		}
		return strBuilder.toString();
	}
	
	private String getJoinString(DatabaseConnection dbc, JoinResult jr, Navigation nav, DimensionTable dim, MeasureTable mt, QueryMap tableMap,
			boolean virtual, AtomicInteger encloseOuterClause) throws ArrayIndexOutOfBoundsException, BaseException, CloneNotSupportedException, SQLException,
			IOException
	{
		StringBuilder from = new StringBuilder();
		/*
		 * Get the dimension table reference (either derived table, opaque view or physical table name)
		 */
		StringBuilder tableSourceString = new StringBuilder(dim.getQueryTableString(r, nav.getQueryColumnList(), tableMap, mt.TableSource, jr, session));
		/*
		 * Add the join definition if not all of the source tables have been pruned (which would result in a zero-length
		 * table source string)
		 */
		if (tableSourceString.length() > 0)
		{
			/*
			 * If there are bridge tables, be sure to add them
			 */
			if (jr.bridgeTables != null)
			{
				for (JoinResult.BridgeTable bt : jr.bridgeTables)
				{
					tableSourceString.append(' ');
					tableSourceString.append(dbc.getInnerJoin());
					tableSourceString.append(' ');
					tableSourceString.append(bt.toString());
					tableSourceString.append(' ');
					tableSourceString.append(tableMap.getMap(bt));
					if (DatabaseConnection.requiresRedundantJoinCondition(dbc.DBType))
						tableSourceString.append(" ON 1=1");
				}
			}
			boolean encloseClause = false;
			if (DatabaseConnection.requiresEnclosingJoinClause(dbc.DBType))
			{
				encloseClause = true;
			}
			from.append(' ');
			from.append(Join.getJoinTypeStr(jr.type, dbc));
			from.append(encloseClause ? '(' : ' ');
			from.append(tableSourceString);
			if (encloseClause)
				from.append(')');
			if (jr.redundant)
			{
				if (virtual && jr.joinCondition != null && !jr.joinCondition.trim().isEmpty())
					from.append(" ON " + jr.joinCondition);
				else if (dbc == null || DatabaseConnection.requiresRedundantJoinCondition(dbc.DBType))
					from.append(" ON 1=1");
			} else
			{
				from.append(" ON ");
				from.append(jr.joinCondition);
			}
			if (encloseOuterClause.getAndDecrement() > 0)
				from.append(')');
		}
		return from.toString();
	}	

	public Map<DatabaseConnection, List<TempTable>> getTempTableMap()
	{
		return tempTableMap;
	}

	public void setTempTableMap(Map<DatabaseConnection, List<TempTable>> tempTableMap)
	{
		this.tempTableMap = tempTableMap;
	}

	public StringBuilder getQuery()
	{
		return query;
	}

	public List<QueryColumn> getResultProjectionList()
	{
		return resultProjectionList;
	}

	public void setQuery(StringBuilder query)
	{
		this.query = query;
	}
	
	public boolean containsFederatedJoin()
	{
		return (fjqh != null);
	}
	
	public FederatedJoinQueryHelper getFederatedJoinQueryHelper()
	{
		return fjqh;
	}
	
	public void setResultProjectionList(List<QueryColumn> resultProjectionList)
	{
		this.resultProjectionList = resultProjectionList;
	}
}
