/**
 * $Id: SingleRowDataSource.java,v 1.8 2011-09-26 17:57:55 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.io.Serializable;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.data.JRAbstractBeanDataSource;

/**
 * Class to provide a single row of data to a report (usefull if a report needs a summary section but no detail - this
 * allows the report to still print)
 * 
 * @author Brad Peters
 * 
 */
public class SingleRowDataSource extends JRAbstractBeanDataSource implements IJasperDataSourceProvider, Serializable
{
	private static final long serialVersionUID = 1L;
	private boolean gotrow = false;
	JasperDataSourceProvider jdsp;

	public SingleRowDataSource(JasperDataSourceProvider jdsp)
	{
		super(false);
		this.jdsp = jdsp;
	}

	/**
	 * @return Returns the jdsp.
	 */
	public JasperDataSourceProvider getJasperDataSourceProvider()
	{
		return new JasperDataSourceProvider(jdsp);
	}

	public void moveFirst() throws JRException
	{
	}

	public boolean next() throws JRException
	{
		if (gotrow)
			return false;
		gotrow = true;
		return true;
	}

	public Object getFieldValue(JRField arg0) throws JRException
	{
		return null;
	}
}
