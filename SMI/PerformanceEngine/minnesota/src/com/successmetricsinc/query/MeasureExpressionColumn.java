/**
 * $Id: MeasureExpressionColumn.java,v 1.40 2011-09-27 00:26:17 ricks Exp $
 *
 * Copyright (C) 2007-2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.successmetricsinc.query.DisplayExpression.MeasureExpressionType;
import com.successmetricsinc.query.DisplayExpression.PositionType;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.util.BaseException;

/**
 * Information on a measure expression column within this display expression
 * 
 * @author Brad Peters
 * 
 */
public class MeasureExpressionColumn
{
	private static Logger logger = Logger.getLogger(MeasureExpressionColumn.class);

	public enum AggregationRule
	{
		None, Sum, Count, Min, Max, Avg, CountDistinct
	};
	enum MeasureDataType
	{
		Integer, Float, Number
	}
	public MeasureExpressionType type;
	public String expression;
	public String columnName;
	public String displayName;
	public String originalDisplayName;
	public List<Position> positions;
	public QueryFilter filter;
	private transient MeasureColumn mc;
	private transient MeasureDataType measureDataType;
	private transient AggregationRule aggRule;
	public transient DisplayExpression displayExpression; 
	transient private Map<String, Integer> rowMap;
	public List<String> groups;
	/* Bridge query tying row keys from main query to keys of measure expression query */
	transient Query bridgeQuery;
	transient QueryResultSet bridgeResultSet;
	transient boolean bridgeAtHigherLevel;
	/*
	 * Dimension Expression query
	 */
	transient Query dimensionExpressionQuery;
	transient QueryResultSet dimensionExpressionResultSet;
	transient DisplayExpression dimensionExpressionDisplayExpression;
	/*
	 * Measure expression query - used when the main query does not include all columns necessary to calculate positions
	 * for this measure expression
	 */
	public transient Query query;
	transient QueryResultSet resultSet;
	// List of columns in bridge data array
	transient List<String[]> brColList;
	// List of columns in data array
	transient List<String[]> colList;
	// Field map - for getting column indexes by name
	transient Map<String, Integer> fieldMap;
	
	/**
	 * Column binding information
	 * 
	 * @author Brad Peters
	 * 
	 */
	private static class ColumnBinding
	{
		int index;
		Position pos;
	}
	// Key column bindings in main array
	transient private List<ColumnBinding> mainBindings;
	// Key column bindings for generating keys in the bridge table using main array rows
	transient private List<ColumnBinding> mainToBridgeBindings;
	// Key column bindings in bridge array
	transient private List<ColumnBinding> bridgeBindings;
	// Key column bindings for generating keys in result table using bridge array rows
	transient private List<ColumnBinding> bridgeToResultBindings;
	// Key column bindings in measure expression array
	transient private List<ColumnBinding> resultBindings;
	// Map into bridge table
	transient private Map<String, Integer> bridgeMap;
	
	public MeasureExpressionColumn clone()
	{
		MeasureExpressionColumn mec = new MeasureExpressionColumn();
		mec.type = type;
		mec.expression = expression;
		mec.columnName = columnName;
		mec.displayName = displayName;
		mec.originalDisplayName = originalDisplayName;
		mec.positions = positions;
		mec.filter = filter;
		mec.mc = mc;
		mec.measureDataType = measureDataType;
		mec.aggRule = aggRule;
		mec.displayExpression = displayExpression;
		mec.rowMap = rowMap;
		mec.groups = groups;
		mec.bridgeQuery = bridgeQuery;
		mec.bridgeResultSet = bridgeResultSet;
		mec.bridgeAtHigherLevel = bridgeAtHigherLevel;
		if (dimensionExpressionQuery != null)
		{
			try
			{
				mec.dimensionExpressionQuery = (Query) dimensionExpressionQuery.clone();
			} catch (CloneNotSupportedException e)
			{
				mec.dimensionExpressionQuery = dimensionExpressionQuery;
			}
			mec.dimensionExpressionResultSet = dimensionExpressionResultSet.clone(true);
			mec.dimensionExpressionDisplayExpression = (DisplayExpression) dimensionExpressionDisplayExpression.clone();
		}
		mec.query = query;
		mec.resultSet = resultSet;
		mec.brColList = brColList;
		mec.colList = colList;
		mec.fieldMap = fieldMap;
		mec.mainBindings = mainBindings;
		mec.mainToBridgeBindings = mainToBridgeBindings;
		mec.bridgeBindings = bridgeBindings;
		mec.bridgeToResultBindings = bridgeToResultBindings;
		mec.resultBindings = resultBindings;
		mec.bridgeMap = bridgeMap;
		return mec;
	}

	public boolean isAggregatable()
	{
		if (mc.DimensionRules != null && mc.DimensionRules.size() > 0)
			return (false);
		if (mc.AggregationRule.equals("COUNT DISTINCT") || mc.AggregationRule.equals("AVG"))
			return (false);
		if (mc.AggregationRule.equals("SUM") || mc.AggregationRule.equals("COUNT"))
		{
			return (true);
		}
		if (mc.AggregationRule.equals("MIN"))
		{
			return (true);
		}
		if (mc.AggregationRule.equals("MAX"))
		{
			return (true);
		}
		return false;
	}

	/**
	 * Get bindings (i.e. column indexes used to create keys) for a given target column list based on an input list.
	 * Order is based on the order of columns in the target list, but indices are from the source list. If the source
	 * does not contain the column, it's index is set to -1 (presumably such a column has an absolute position
	 * associated with it so that a key can be constructed)
	 * 
	 * @param clist
	 * @return
	 */
	private List<ColumnBinding> getBindings(List<String[]> sourceList, List<String[]> targetList, List<String[]> includeList)
	{
		List<ColumnBinding> list = new ArrayList<ColumnBinding>();
		for (String[] col : targetList)
		{
			boolean include = includeList == null ? true : false;
			if (includeList != null)
				for (String[] item : includeList)
				{
					if (item[0] != null && item[0].equals(col[0]))
					{
						include = true;
						break;
					}
				}
			if (!include)
				continue;
			// If it is a dimension column
			if (col[2].charAt(0) == 'D')
			{
				ColumnBinding cb = new ColumnBinding();
				cb.index = -1;
				// Find the corresponding index in the source list
				for (int i = 0; i < sourceList.size(); i++)
				{
					if (sourceList.get(i)[0] != null && sourceList.get(i)[0].equals(col[0]))
					{
						cb.index = i;
						break;
					}
				}
				// See if it matches a Position
				Position matchPos = null;
				for (Position pos : positions)
				{
					if (col[0].equals(pos.pDim + "." + pos.pCol))
					{
						matchPos = pos;
						break;
					}
				}
				if (matchPos != null)
				{
					// If this column matches a positional calc, include
					cb.pos = matchPos;
					list.add(cb);
				} else
				{
					/*
					 * Make sure there aren't any other columns that are in the same dimension as a position (unless
					 * it's an absolute position without a value - hence another column is required to generate the
					 * position)
					 */
					boolean found = false;
					for (Position pos : positions)
					{
						boolean inc = includeList == null ? true : false;
						if (includeList != null)
							for (String[] incCol : includeList)
							{
								if (incCol[0] != null && incCol[0].equals(pos.pDim + "." + pos.pCol))
								{
									inc = true;
									break;
								}
							}
						if (inc)
							if (col[0].startsWith(pos.pDim))
							{
								found = true;
								break;
							}
					}
					if (!found && cb.index >= 0)
						list.add(cb);
				}
			}
		}
		return (list);
	}

	/**
	 * Bind key columns to the appropriate ones in each data array. This assumes that the column lists are set for the
	 * bridge and result data arrays
	 * 
	 * @param colList
	 *            Column list for the main data array
	 */
	public void setBindings(List<String[]> parColList)
	{
		// Main array bindings
		mainBindings = getBindings(parColList, parColList, null);
		if (bridgeQuery != null)
		{
			/*
			 * If the bridge table is at a higher level than the main result set, then make sure any keys into it
			 * include only columns from the main result set.
			 */
			List<String[]> includeList = bridgeAtHigherLevel ? parColList : null;
			mainToBridgeBindings = getBindings(parColList, brColList, includeList);
			bridgeBindings = getBindings(brColList, brColList, includeList);
		}
		if (query != null)
		{
			bridgeToResultBindings = getBindings(brColList, colList, null);
			resultBindings = getBindings(colList, colList, null);
		}
	}

	/**
	 * Calculate a new position (column value) based on an existing column value and a positional calc
	 * 
	 * @param curVal
	 * @param pos
	 * @return
	 */
	private Object calcPosition(Object curVal, Position pos)
	{
		Object val = curVal;
		if (pos.ptype == PositionType.RelativeMinus)
		{
			if (Double.class.isInstance(curVal))
				val = ((Double) curVal) - ((Double) pos.pVal);
			else if (Integer.class.isInstance(curVal))
				val = ((Integer) curVal) - ((Double) pos.pVal).intValue();
			else if (Long.class.isInstance(curVal))
				val = ((Long) curVal) - ((Double) pos.pVal).intValue();
		} else if (pos.ptype == PositionType.RelativePlus)
		{
			if (Double.class.isInstance(curVal))
				val = ((Double) curVal) + ((Double) pos.pVal);
			else if (Integer.class.isInstance(curVal))
				val = ((Integer) curVal) + ((Double) pos.pVal).intValue();
			else if (Long.class.isInstance(curVal))
				val = ((Long) curVal) + ((Double) pos.pVal).intValue();
		} else if (pos.pVal != null)
		{
			if (Double.class.isInstance(curVal))
				val = ((Double) pos.pVal);
			else if (Integer.class.isInstance(curVal))
				val = ((Double) pos.pVal).intValue();
			else if (Long.class.isInstance(curVal))
				val = ((Double) pos.pVal).intValue();
			else
				val = pos.pVal;
		}
		return (val);
	}
	protected static final int MAIN_KEY = 0;
	protected static final int MAIN_TO_BRIDGE_KEY = 1;
	protected static final int BRIDGE_KEY = 2;
	protected static final int BRIDGE_TO_RESULT_KEY = 3;
	protected static final int RESULT_KEY = 4;

	/**
	 * Get the row key for a given measure expression column into a data array
	 * 
	 * @param mainArray
	 *            Main query data results
	 * @param rowIndex
	 *            Row index to analyze
	 * @param dataArray
	 *            Which data array to use to generate the key: MAIN_KEY - main array, MAIN_TO_BRIDGE_KEY - bridge array
	 *            key using main array index, BRIDGE_KEY - bridge array key using bridge array index,
	 *            BRIDGE_TO_RESULT_KEY - measure expression result key using bridge array index, RESULT_KEY - measure
	 *            expression result key using measure expression result index
	 * @param calcPosition
	 *            Whether to return a key for the current row, or the calculated key based on positional calculations
	 * @return
	 */
	public String getRowKey(Object[][] mainArray, int rowIndex, int dataArray, boolean calcPosition)
	{
		Object[][] data = null;
		List<ColumnBinding> bindings = null;
		switch (dataArray)
		{
		case MAIN_KEY:
			data = mainArray;
			bindings = mainBindings;
			break;
		case MAIN_TO_BRIDGE_KEY:
			data = mainArray;
			bindings = mainToBridgeBindings;
			break;
		case BRIDGE_KEY:
			data = bridgeResultSet.getRows();
			bindings = bridgeBindings;
			break;
		case BRIDGE_TO_RESULT_KEY:
			data = bridgeResultSet.getRows();
			bindings = bridgeToResultBindings;
			break;
		case RESULT_KEY:
			data = resultSet.getRows();
			bindings = resultBindings;
			break;
		}
		StringBuilder sb = new StringBuilder();
		if (bindings!=null)
		{
			for (ColumnBinding cb : bindings)
			{
				Object val = cb.index >= 0 ? (data[rowIndex][cb.index] == null ? "null" : data[rowIndex][cb.index]) : null;
				if (calcPosition && cb.pos != null)
					val = calcPosition(val, cb.pos);
				/* For absolute positioning, add or exclude if necessary when mapping from the main to the bridge */
				if ((dataArray == MAIN_TO_BRIDGE_KEY || dataArray == BRIDGE_KEY) && cb.pos != null && cb.pos.ptype == PositionType.Absolute)
				{
					// If this is a specific position and the column doesn't exist, then add it
					if (val == null && cb.pos.pVal != null)
						val = cb.pos.pVal;
				}
				if (val != null)
				{
					sb.append('{');
					sb.append(val);
					sb.append('}');
				}
			}
		}
		return (sb.toString());
	}

	/**
	 * Generates a key that defines a rowmap
	 * 
	 * @param colNames
	 * @param dataArray
	 * @return
	 */
	public String getMapKey(String[] colNames, int dataArray)
	{
		List<ColumnBinding> bindings = null;
		switch (dataArray)
		{
		case MAIN_KEY:
			bindings = mainBindings;
			break;
		case MAIN_TO_BRIDGE_KEY:
			bindings = mainToBridgeBindings;
			break;
		case BRIDGE_KEY:
			bindings = bridgeBindings;
			break;
		case BRIDGE_TO_RESULT_KEY:
			bindings = bridgeToResultBindings;
			break;
		case RESULT_KEY:
			bindings = resultBindings;
			break;
		}
		StringBuilder sb = new StringBuilder();
		sb.append(dataArray);
		if (bindings!=null)
		{
			for (ColumnBinding cb : bindings)
			{
				String name = cb.index >= 0 ? colNames[cb.index] : null;
				if (name != null)
				{
					sb.append('{');
					sb.append(name);
					sb.append('}');
				}
			}
		}
		return (sb.toString());
	}

	/**
	 * Create row key maps if necessary (i.e. if there are positional calculations)
	 * 
	 * @param mainArray
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @throws SessionVariableUnavailableException
	 * @throws ScriptException
	 */
	public void createRowMaps(ResultSetCache rsc, QueryResultSet qrs, RowMapCache rmap, List<MeasureExpressionColumn> meColumns) throws IOException,
			ClassNotFoundException, CloneNotSupportedException, BaseException
	{
		/* If using the main result set for positional calculations, create simple map */
		Object[][] mainArray = qrs.getRows();
		String qrsBaseFilename = qrs.getBaseFilename();
		String mecqrsBaseFilename = "";
		String mecbqrsBaseFilename = "";
		if (resultSet != null)
		{
			mecqrsBaseFilename = resultSet.getBaseFilename();
		}
		if (bridgeResultSet != null)
		{
			mecbqrsBaseFilename = bridgeResultSet.getBaseFilename();
		}
		String qkey = null;
		String compositeKey = null;
		if (query == null && positions.size() > 0)
		{
			String mainKey = getMapKey(qrs.getColumnNames(), MAIN_KEY);
			try
			{
				qkey = qrs.getQuery().getKey(null);
			} catch (NavigationException ex)
			{
			}
			if (qkey != null)
			{
				compositeKey = qkey + qrsBaseFilename + "|" + mecqrsBaseFilename + "|" + mecbqrsBaseFilename;
			}
			if (rmap != null && compositeKey != null)
			{
				try
				{
					rowMap = rmap.getRowMap(rsc, compositeKey, mainKey);
				} catch (Error er)
				{
					logger.debug("MeasureExpressionColumn.createRowMaps: " + er.getMessage());
					rowMap = null;
				} catch (Exception ex)
				{
					logger.debug("MeasureExpressionColumn.createRowMaps: " + ex.getMessage());
					rowMap = null;
				}
			}
			if (rowMap == null)
			{
				rowMap = new HashMap<String, Integer>();
				for (int i = 0; i < mainArray.length; i++)
				{
					String key = getRowKey(mainArray, i, MAIN_KEY, false);
					rowMap.put(key, i);
				}
				if (rmap != null)
				{
					try
					{
						rmap.addRowMap(rsc, compositeKey, mainKey, rowMap);
					} catch (Exception ex)
					{
						logger.error(ex, ex);
					}
				}
			}
		} else if (positions.size() > 0)
		{
			/* Otherwise, create row map based on bridge table and new result set */
			String rkey = getMapKey(resultSet.getColumnNames(), RESULT_KEY);
			try
			{
				qkey = resultSet.getQuery().getKey(null);
			} catch (NavigationException ex)
			{
			}
			compositeKey = qkey + qrsBaseFilename + "|" + mecqrsBaseFilename + "|" + mecbqrsBaseFilename;
			if (rmap != null)
			{
				try
				{
					rowMap = rmap.getRowMap(rsc, compositeKey, rkey);
				} catch (Exception ex)
				{
					logger.error(ex, ex);
					rowMap = null;
				}
			} else
				rowMap = null;
			Object[][] array = null;
			if (rowMap == null)
			{
				// First, create table map for result table
				rowMap = new HashMap<String, Integer>();
				array = resultSet.getRows();
				for (int i = 0; i < array.length; i++)
				{
					String key = getRowKey(array, i, RESULT_KEY, false);
					rowMap.put(key, i);
				}
				if (rmap != null)
				{
					try
					{
						rmap.addRowMap(rsc, compositeKey, rkey, rowMap);
					} catch (Exception ex)
					{
						logger.error(ex, ex);
					}
				}
			}
			// Create map from main array into bridge table
			rkey = getMapKey(qrs.getColumnNames(), MAIN_TO_BRIDGE_KEY);
			try
			{
				qkey = bridgeResultSet.getQuery().getKey(null);
			} catch (NavigationException ex)
			{
			}
			compositeKey = qkey + qrsBaseFilename + "|" + mecqrsBaseFilename + "|" + mecbqrsBaseFilename;
			if (rmap != null)
			{
				try
				{
					bridgeMap = rmap.getRowMap(rsc, compositeKey, rkey);
				} catch (Exception ex)
				{
					logger.error(ex, ex);
					bridgeMap = null;
				}
			} else
				bridgeMap = null;
			/*
			 * Count the maximum number of instances a given column appears in the bridge column list. If it's more than
			 * one time, then multiple keys for the same column exist. If that's the case, and there is a bridge map
			 * already created, continue mapping because the previous map will not necessarily contain the indexes for
			 * this particular measure expression column
			 */
			int maxCount = 0;
			if (bridgeMap != null)
			{
				for (String[] col : brColList)
				{
					// If it is a dimension column
					if (col[2].charAt(0) == 'D')
					{
						maxCount = 0;
						for (MeasureExpressionColumn mec : meColumns)
						{
							for (Position pos : mec.positions)
							{
								if (col[0].equals(pos.pDim + "." + pos.pCol))
								{
									maxCount++;
								}
							}
						}
						if (maxCount > 1)
						{
							break;
						}
					}
				}
			}
			/*
			 * Create the bridge map if necessary (may need to do multiple passes because multiple positions may be for
			 * the same columns and those positions might be below the grain of the main query - all absolute positions
			 * must be indexed. If not, only the first instance of an absolute position for a given column will be
			 * indexed.)
			 */
			if (bridgeMap == null || maxCount > 1)
			{
				// Create temporary bridge table map
				array = bridgeResultSet.getRows();
				Map<String, Integer> brMap = new HashMap<String, Integer>();
				for (int i = 0; i < array.length; i++)
				{
					String key = getRowKey(array, i, BRIDGE_KEY, false);
					brMap.put(key, i);
				}
				// Create map from main array into bridge table
				Map<String, Integer> tempBridgeMap = bridgeMap == null ? new HashMap<String, Integer>() : bridgeMap;
				// Populate map
				for (int i = 0; i < mainArray.length; i++)
				{
					String key = getRowKey(mainArray, i, MAIN_TO_BRIDGE_KEY, false);
					Integer index = brMap.get(key);
					/*
					 * Don't re-index if not necessary. Detect if this key is already in the map, if so, then this
					 * position has already been indexed (you cannot tell from bridgeMap not being null because multiple
					 * passes - one per measure expression column - would be required to fully index
					 */
					if (i == 0 && tempBridgeMap.get(key) != null)
						break;
					if (index != null)
						tempBridgeMap.put(key, index);
				}
				if (bridgeMap == null)
				{
					bridgeMap = tempBridgeMap;
					if (rmap != null)
					{
						try
						{
							rmap.addRowMap(rsc, compositeKey, rkey, bridgeMap);
						} catch (Exception ex)
						{
							logger.error(ex, ex);
						}
					}
				}
			}
		}
	}

	/**
	 * Return a positional calculation value using an index into the main array
	 * 
	 * @param mainArray
	 * @param index
	 * @return
	 */
	public Object getValue(Object[][] mainArray, int index)
	{
		Integer fi = originalDisplayName != null ? fieldMap.get(originalDisplayName) : fieldMap.get(displayName);
		if (fi == null && originalDisplayName != null)
			fi = fieldMap.get(displayName);
		if (dimensionExpressionQuery != null)
		{
			Object rows[][] = dimensionExpressionResultSet.rows;
			double floatResult = 0;
			long intResult = 0;
			FieldProvider fields = dimensionExpressionDisplayExpression.getFields();
			try
			{
				fields.setArrays(null, dimensionExpressionResultSet, null, null);
			} catch (ClassNotFoundException | IOException | CloneNotSupportedException | BaseException e)
			{
				logger.error(e);
				return null;
			}
			int pos = fields.getFieldMap().get("{DIMENSIONEXPRESSION}");
			dimensionExpressionDisplayExpression.setParentFields(displayExpression.getFields());
			StringBuilder sb = new StringBuilder();
			boolean first = true;
			for (int idx : dimensionExpressionResultSet.getParentIndices())
			{
				if (first)
					first = false;
				else
					sb.append('|');
				if (mainArray[index][idx] != null)
					sb.append(mainArray[index][idx].toString());
			}
			String key = sb.toString();
			Integer val = dimensionExpressionResultSet.getDimensionMaskMap().get(key);
			if (val == null)
				return null;
			synchronized (dimensionExpressionResultSet)
			{
				dimensionExpressionResultSet.processExpression(dimensionExpressionDisplayExpression, null, val);
			}
			first = true;
			for (int i = 0; i < rows.length; i++)
			{
				boolean validRow = false;
				
				if (rows[i][pos] != null)
				{
					if (rows[i][pos] instanceof Boolean)
					{
						validRow = (boolean) rows[i][pos];
					} else
					{
						validRow = Boolean.valueOf(rows[i][pos].toString());
					}
				}
				if (validRow)
				{
					if (measureDataType == MeasureDataType.Float || measureDataType == MeasureDataType.Number)
					{
						if (aggRule == AggregationRule.Sum || aggRule == AggregationRule.Count)
						{
							if (rows[i][fi] instanceof Double)
								floatResult += (double) rows[i][fi];
							else if (rows[i][fi] instanceof Integer)
								floatResult += (int) rows[i][fi];
							else if (rows[i][fi] instanceof Long)
								floatResult += (long) rows[i][fi];
							else if (rows[i][fi] instanceof Number)
								floatResult += ((Number) rows[i][fi]).doubleValue();
						} else if (aggRule == AggregationRule.Max)
						{
							if (rows[i][fi] instanceof Double)
								floatResult = (double) rows[i][fi] > floatResult || first ? (double) rows[i][fi] : floatResult;
							else if (rows[i][fi] instanceof Integer)
								floatResult = (int) rows[i][fi] > floatResult || first ? (int) rows[i][fi] : floatResult;
							else if (rows[i][fi] instanceof Long)
								floatResult = (long) rows[i][fi] > floatResult || first ? (long) rows[i][fi] : floatResult;
							else if (rows[i][fi] instanceof Number)
								floatResult = ((Number) rows[i][fi]).doubleValue() > floatResult || first ? ((Number) rows[i][fi]).doubleValue() : floatResult;
							first = false;
						} else if (aggRule == AggregationRule.Min)
						{
							if (rows[i][fi] instanceof Double)
								floatResult = (double) rows[i][fi] < floatResult || first ? (double) rows[i][fi] : floatResult;
							else if (rows[i][fi] instanceof Integer)
								floatResult = (int) rows[i][fi] < floatResult || first ? (int) rows[i][fi] : floatResult;
							else if (rows[i][fi] instanceof Long)
								floatResult = (long) rows[i][fi] < floatResult || first ? (long) rows[i][fi] : floatResult;
							else if (rows[i][fi] instanceof Number)
								floatResult = ((Number) rows[i][fi]).doubleValue() < floatResult || first ? ((Number) rows[i][fi]).doubleValue() : floatResult;
							first = false;
						}
					} else if (measureDataType == MeasureDataType.Integer)
					{
						if (aggRule == AggregationRule.Sum || aggRule == AggregationRule.Count)
						{
							if (rows[i][fi] instanceof Integer)
								intResult += (int) rows[i][fi];
							else if (rows[i][fi] instanceof Double)
								intResult += (double) rows[i][fi];
							else if (rows[i][fi] instanceof Long)
								intResult += (long) rows[i][fi];
							else if (rows[i][fi] instanceof Number)
								intResult += ((Number) rows[i][fi]).doubleValue();
						} else if (aggRule == AggregationRule.Max)
						{
							if (rows[i][fi] instanceof Double)
								intResult = (int) ((double) rows[i][fi] > intResult || first ? (double) rows[i][fi] : intResult);
							else if (rows[i][fi] instanceof Integer)
								intResult = (int) rows[i][fi] > intResult || first ? (int) rows[i][fi] : intResult;
							else if (rows[i][fi] instanceof Long)
								intResult = (long) rows[i][fi] > intResult || first ? (long) rows[i][fi] : intResult;
							else if (rows[i][fi] instanceof Number)
								intResult = (int) (((Number) rows[i][fi]).doubleValue() > floatResult || first ? ((Number) rows[i][fi]).doubleValue()
										: intResult);
							first = false;
						} else if (aggRule == AggregationRule.Min)
						{
							if (rows[i][fi] instanceof Double)
								intResult = (int) ((double) rows[i][fi] < intResult || first ? (double) rows[i][fi] : intResult);
							else if (rows[i][fi] instanceof Integer)
								intResult = (int) rows[i][fi] < intResult || first ? (int) rows[i][fi] : intResult;
							else if (rows[i][fi] instanceof Long)
								intResult = (long) rows[i][fi] < intResult || first ? (long) rows[i][fi] : intResult;
							else if (rows[i][fi] instanceof Number)
								intResult = (int) (((Number) rows[i][fi]).doubleValue() < floatResult || first ? ((Number) rows[i][fi]).doubleValue()
										: intResult);
							first = false;
						}
					}
				}
			}
			if (measureDataType == MeasureDataType.Float || measureDataType == MeasureDataType.Number)
			{
				return floatResult;
			} else if (measureDataType == MeasureDataType.Integer)
			{
				return intResult;
			}
			return null;
		}
		fi = originalDisplayName != null ? fieldMap.get(originalDisplayName) : fieldMap.get(displayName);
		if (fi == null && originalDisplayName != null)
			fi = fieldMap.get(displayName);
		if (query == null)
		{
			if (rowMap == null)
				return (mainArray[index][fi]);
			String key = getRowKey(mainArray, index, MAIN_KEY, true);
			Integer rowindex = rowMap.get(key);
			if (rowindex != null)
			{
				Object[] row = mainArray[rowindex];
				return (row[fi]);
			} else
				return (null);
		}
		String key = getRowKey(mainArray, index, MAIN_TO_BRIDGE_KEY, true);
		Integer bridgeIndex = bridgeMap.get(key);
		if (bridgeIndex == null)
			return (null);
		String mainKey = null;
		Integer rownumObj = null;
		Integer fieldnumObj = null;
		int rownum = 0;
		int fieldnum = 0;
		mainKey = getRowKey(bridgeResultSet.getRows(), bridgeIndex, BRIDGE_TO_RESULT_KEY, false);
		rownumObj = rowMap.get(mainKey);
		fieldnumObj = fi;
		if ((rownumObj == null) || (fieldnumObj == null))
		{
			return null;
		}
		rownum = rownumObj.intValue();
		fieldnum = fieldnumObj.intValue();
		return (resultSet.getRows()[rownum][fieldnum]);
	}

	public Query getQuery()
	{
		return query;
	}

	public Query getBridgeQuery()
	{
		return bridgeQuery;
	}

	public String getDisplayName()
	{
		return displayName;
	}

	public void setDisplayName(String displayName)
	{
		this.displayName = displayName;
	}

	public MeasureColumn getMeasureColumn()
	{
		return mc;
	}

	public void setMeasureColumn(MeasureColumn mc)
	{
		if (mc.DataType.equals("Integer"))
			measureDataType = MeasureDataType.Integer;
		else if (mc.DataType.equals("Number"))
			measureDataType = MeasureDataType.Number;
		else
			measureDataType = MeasureDataType.Float;
		if (mc.AggregationRule.equals("SUM"))
			aggRule = AggregationRule.Sum;
		else if (mc.AggregationRule.equals("COUNT"))
			aggRule = AggregationRule.Count;
		else if (mc.AggregationRule.equals("MIN"))
			aggRule = AggregationRule.Min;
		else if (mc.AggregationRule.equals("MAX"))
			aggRule = AggregationRule.Max;
		else if (mc.AggregationRule.equals("AVG"))
			aggRule = AggregationRule.Avg;
		else if (mc.AggregationRule.equals("COUNT DISTINCT"))
			aggRule = AggregationRule.CountDistinct;
		this.mc = mc;
	}

	public AggregationRule getAggRule()
	{
		return aggRule;
	}
}