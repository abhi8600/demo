package com.successmetricsinc.query;

public class DatabaseIndex
{
	public String name;
	public String schemaName;
	public String tableName;
	public boolean clustered;
	public String[] columns;
	public String[] includedColumns;
}
