/**
 * $Id: DisplayFilter.java,v 1.105 2012-12-02 19:08:23 birst\bpeters Exp $
 *
 * Copyright (C) 2007-2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import gnu.jel.CompiledExpression;
import gnu.jel.Evaluator;
import gnu.jel.Library;

import java.io.Serializable;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.regex.Pattern;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.engine.Header;
import com.successmetricsinc.engine.ModelDataSet;
import com.successmetricsinc.engine.SuccessModelInstance;
import com.successmetricsinc.util.DateUtil;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;


import com.successmetricsinc.query.QueryFilter;

/**
 * Class to contain a result set display filter. This filter is applied to query result sets.
 * 
 * @author bpeters
 * 
 */
public class DisplayFilter implements Cloneable, Serializable
{
	private static final String INPUT_TEXT = "InputText";
	private static final String NAME = "Name";
	private static final String REPORT_INDEX = "ReportIndex";
	public static final String FILTER = "Filter";
	private static final long serialVersionUID = 1L;
	private static final String OPERATOR = "Operator";
	private static final String DISPLAY_NAME = "DisplayName";
	private static final String VALID = "Valid";
	private static final String PROMPTED = "Prompted";
	private static final String PROMPT_NAME = "PromptName";
	private static final String COLUMN_POSITION = "ColumnPosition";
	private static final String FILTER_LIST = "FilterList";
	private static final String OPERAND = "Operand";
	private static final String COLUMN_NAME = "ColumnName";
	private static final String TABLE_NAME = "TableName";
	private static final String TYPE = "Type";
	private static final String DATATYPE = "DataType";
	private static final String OPERAND_DISPLAYVALUE = "Operand_DisplayValue";
	private static final String DATETIME_FORMAT = "DateTimeFormat";
	private static Logger logger = Logger.getLogger(DisplayFilter.class);
	private static HashMap<Type, String> typeNames;
	private static HashMap<Operator, String> operatorNames;
	public static final boolean CASE_INSENSITIVE_LIKE = true;
	private String strType;
	private String strOperator;

	public enum Type
	{
		Predicate, LogicalOperator, Text
	};

	public enum Operator
	{
		Equal, GreaterThan, LessThan, GreaterThanOrEqual, LessThanOrEqual, NotEqual, And, Or, Like, NotLike, In, NotIn, IsNull, IsNotNull
	};
	private Type type;
	private String tableName;
	private String columnName;
	private Operator operator;
	private Object operand;
	private List<DisplayFilter> FilterList;
	private int columnPosition = -2;
	private String promptName;
	private boolean valid;
	private boolean displayName = true;
	private int reportIndex = -1;
	private transient Pattern pattern;
	private transient boolean containsExpression;
	private transient Object compiledExpression;
	public transient String dataType;
	public transient String Operand_DisplayValue;
	public transient String DateTime_Format;
	public transient String alias;
	
	private String name;
	private String inputText;
	
	static {
		typeNames = new HashMap<Type, String>();
		typeNames.put(Type.Predicate, "Predicate");
		typeNames.put(Type.LogicalOperator, "LogicalOperator");
		typeNames.put(Type.Text, "3");
		
		// upcased IN/LIKE, etc to handle issue on the Flex Client - should really get rid of this from the XML XXX
		operatorNames = new HashMap<Operator, String>();
		operatorNames.put(Operator.And, "And");
		operatorNames.put(Operator.Equal, "Equal");
		operatorNames.put(Operator.GreaterThan, "Greater than");
		operatorNames.put(Operator.GreaterThanOrEqual, "Greater than or equal");
		operatorNames.put(Operator.In, "IN");
		operatorNames.put(Operator.LessThan, "Less than");
		operatorNames.put(Operator.LessThanOrEqual, "Less than or equal");
		operatorNames.put(Operator.Like, QueryFilter.LIKE);
		operatorNames.put(Operator.NotEqual, "Not equal");
		operatorNames.put(Operator.NotIn, QueryFilter.NOTIN);
		operatorNames.put(Operator.NotLike, QueryFilter.NOTLIKE);
		operatorNames.put(Operator.Or, "Or");
		operatorNames.put(Operator.IsNull, QueryFilter.ISNULL);
		operatorNames.put(Operator.IsNotNull, QueryFilter.ISNOTNULL);
	}
	
	public DisplayFilter()
	{
		// for cloning...
	}

	public void parseElement(XMLStreamReader parser) throws Exception {
		boolean isPrompted = false;
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
			String elName = parser.getLocalName();
			if (TYPE.equals(elName)) {
				String type = XmlUtils.getStringContent(parser);
				strType = type;
				for (Map.Entry<Type,String> el1 : typeNames.entrySet()) {
					if (el1.getValue().equals(type)) {
						this.type = el1.getKey();
						break;
					}
				}
			}
			else if (NAME.equals(elName)) {
				this.name = XmlUtils.getStringContent(parser);
			}
			else if (INPUT_TEXT.equals(elName)) {
				this.inputText = XmlUtils.getStringContent(parser);
			}
			else if (TABLE_NAME.equals(elName)) {
				this.tableName = XmlUtils.getStringContent(parser);
			}
			else if (COLUMN_NAME.equals(elName)) {
				this.columnName = XmlUtils.getStringContent(parser);
			}
			else if (OPERAND.equals(elName)) {
				this.operand = XmlUtils.getStringContent(parser);
			}
			else if (FILTER_LIST.equals(elName)) {
				addFilterList(parser);
			}
			else if (COLUMN_POSITION.equals(elName)) {
				Integer i = XmlUtils.getIntContent(parser);
				if (i != null)
					this.columnPosition = i.intValue(); 
			}
			else if (PROMPT_NAME.equals(elName)) {
				this.promptName = XmlUtils.getStringContent(parser);
			}
			else if (PROMPTED.equals(elName)) {
				isPrompted = true;
			}
			else if (VALID.equals(elName)) {
				this.valid = XmlUtils.getBooleanContent(parser);
			}
			else if (DISPLAY_NAME.equals(elName)) {
				this.displayName = XmlUtils.getBooleanContent(parser);
			}
			else if (OPERATOR.equals(elName)) {
				String operator = XmlUtils.getStringContent(parser);
				strOperator = operator;
				for (Map.Entry<Operator,String> op : operatorNames.entrySet()) {
					if (op.getValue().equalsIgnoreCase(operator)) {
						this.operator = op.getKey();
						break;
					}
				}
			} else if (DATATYPE.equals(elName))
			{
				this.dataType = XmlUtils.getStringContent(parser);
			} else if (OPERAND_DISPLAYVALUE.equals(elName))
			{
				this.Operand_DisplayValue = XmlUtils.getStringContent(parser);
			} else if (DATETIME_FORMAT.equals(elName))
			{
				this.DateTime_Format = XmlUtils.getStringContent(parser);
			}
			else if (REPORT_INDEX.equals(elName)) {
				Integer v = XmlUtils.getIntContent(parser);
				if (v != null)
					this.reportIndex = v.intValue();
			}
		}
		if (isPrompted && this.promptName == null) {
			this.promptName = getDefaultPromptName();
		}
		if (this.operator == Operator.Like || this.operator == Operator.NotLike)
			this.valid = setLikeOperator();
		else if (this.operator == Operator.In || this.operator == Operator.NotIn)
			this.valid = setInOperator();
		else
			this.valid = true;
	}
	
	public Element getDisplayFilterElement(String elementName,Namespace ns)
	{
		Element e = new Element(elementName,ns);
		
		Element child = new Element("TableName",ns);
		child.setText(tableName);
		e.addContent(child);
		
		child = new Element("ColumnName",ns);
		child.setText(columnName);
		e.addContent(child);
		
		child = new Element("Type",ns);
		child.setText(strType);
		e.addContent(child);
		
		child = new Element("Operator",ns);
		child.setText(strOperator);
		e.addContent(child);
		
		child = new Element("Operand",ns);
		child.setText(String.valueOf(this.operand));
		e.addContent(child);	
		
		child = new Element("DataType",ns);
		child.setText(dataType);
		e.addContent(child);
		
		if (FilterList!=null && !FilterList.isEmpty())
		{
			child = new Element("FilterList",ns);
			for (DisplayFilter df : FilterList)
			{
				child.addContent(df.getDisplayFilterElement("DisplayFilter", ns));
			}
			e.addContent(child);
		}
					
		return e;
	}
	
	@SuppressWarnings("unchecked")
	public void parseElement(OMElement parent)
	{
		boolean isPrompted = false;
		for (Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();
			if (TYPE.equals(elName)) {
				String type = XmlUtils.getStringContent(el);
				strType = type;
				for (Map.Entry<Type,String> el1 : typeNames.entrySet()) {
					if (el1.getValue().equals(type)) {
						this.type = el1.getKey();
						break;
					}
				}
			}
			else if (NAME.equals(elName)) {
				this.name = XmlUtils.getStringContent(el);
			}
			else if (INPUT_TEXT.equals(elName)) {
				this.inputText = XmlUtils.getStringContent(el);
			}
			else if (TABLE_NAME.equals(elName)) {
				this.tableName = XmlUtils.getStringContent(el);
			}
			else if (COLUMN_NAME.equals(elName)) {
				this.columnName = XmlUtils.getStringContent(el);
			}
			else if (OPERAND.equals(elName)) {
				this.operand = XmlUtils.getStringContent(el);
			}
			else if (FILTER_LIST.equals(elName)) {
				addFilterList(el);
			}
			else if (COLUMN_POSITION.equals(elName)) {
				Integer i = XmlUtils.getIntContent(el);
				if (i != null)
					this.columnPosition = i.intValue();
			}
			else if (PROMPT_NAME.equals(elName)) {
				this.promptName = XmlUtils.getStringContent(el);
			}
			else if (PROMPTED.equals(elName)) {
				isPrompted = true;
			}
			else if (VALID.equals(elName)) {
				this.valid = XmlUtils.getBooleanContent(el);
			}
			else if (DISPLAY_NAME.equals(elName)) {
				this.displayName = XmlUtils.getBooleanContent(el);
			}
			else if (OPERATOR.equals(elName)) {
				String operator = XmlUtils.getStringContent(el);
				strOperator = operator;
				for (Map.Entry<Operator,String> op : operatorNames.entrySet()) {
					if (op.getValue().equalsIgnoreCase(operator)) {
						this.operator = op.getKey();
						break;
					}
				}
			} else if (DATATYPE.equals(elName))
			{
				this.dataType = XmlUtils.getStringContent(el);
			} else if (OPERAND_DISPLAYVALUE.equals(elName))
			{
				this.Operand_DisplayValue = XmlUtils.getStringContent(el);
			} else if (DATETIME_FORMAT.equals(elName))
			{
				this.DateTime_Format = XmlUtils.getStringContent(el);
			}
			else if (REPORT_INDEX.equals(elName))
			{
				Integer v = XmlUtils.getIntContent(el);
				if (v != null)
					this.reportIndex = v.intValue();
			}
		}
		if (isPrompted && this.promptName == null) {
			this.promptName = getDefaultPromptName();
		}
		if (this.operator == Operator.Like || this.operator == Operator.NotLike)
			this.valid = setLikeOperator();
		else if (this.operator == Operator.In || this.operator == Operator.NotIn)
			this.valid = setInOperator();
		else
			this.valid = true;
	}
	public void parseElement(Element parent, Namespace ns) {
		String type = XmlUtils.getStringContent(parent, "Type", ns);
		strType = type;
		for (Map.Entry<Type,String> el : typeNames.entrySet()) {
			if (el.getValue().equals(type)) {
				this.type = el.getKey();
				break;
			}
		}
		this.tableName = XmlUtils.getStringContent(parent, "TableName", ns);
		if (this.tableName != null && this.tableName.trim().length() == 0) {
			this.tableName = null;
		}
		this.columnName = XmlUtils.getStringContent(parent, "ColumnName", ns);
		this.operand = XmlUtils.getStringContent(parent, "Operand", ns);
		this.dataType = XmlUtils.getStringContent(parent, "DataType", ns);
		if (this.dataType == null || this.dataType.length() == 0)
			this.dataType = "Integer"; // backwards compatibility (only use of this was for MRANK comparison

		this.FilterList = new ArrayList<DisplayFilter>();
		Element el = parent.getChild("FilterList", ns);
		if (el != null) {
			for (Object o : el.getChildren()) {
				DisplayFilter filter = new DisplayFilter();
				filter.parseElement((Element) o, ns);
				this.FilterList.add(filter);
			}
		}
		
		Integer iTemp = XmlUtils.getIntContent(parent, "ColumnPosition", ns);
		if (iTemp != null)
			this.columnPosition = iTemp.intValue();
		
		this.promptName = XmlUtils.getStringContent(parent, "PromptName", ns);
		if (this.promptName == null && (XmlUtils.getBooleanContent(parent, "Prompted", ns).equals(Boolean.TRUE))) {
			this.promptName = getDefaultPromptName();
		}
		
		Boolean bTemp = XmlUtils.getBooleanContent(parent, "Valid", ns);
		if (bTemp != null)
			this.valid = bTemp.booleanValue();
		bTemp = XmlUtils.getBooleanContent(parent, "DisplayName", ns);;
		if (bTemp != null)
			this.displayName = bTemp.booleanValue(); 
		
		String operator = XmlUtils.getStringContent(parent, "Operator", ns);
		strOperator = operator;
		for (Map.Entry<Operator,String> op : operatorNames.entrySet()) {
			if (op.getValue().equalsIgnoreCase(operator)) {
				this.operator = op.getKey();
				break;
			}
		}
		if (this.operator == Operator.Like || this.operator == Operator.NotLike)
			this.valid = setLikeOperator();
		else if (this.operator == Operator.In || this.operator == Operator.NotIn)
			this.valid = setInOperator();
		else
			this.valid = true;
	}
	
	@SuppressWarnings("unchecked")
	private void addFilterList(OMElement parent) {
		this.FilterList = new ArrayList<DisplayFilter>();
		for (Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			DisplayFilter filter = new DisplayFilter();
			filter.parseElement(el);
			this.FilterList.add(filter);
		}
	}
	private void addFilterList(XMLStreamReader parser) throws Exception {
		this.FilterList = new ArrayList<DisplayFilter>();
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
			String elName = parser.getLocalName();
			if (DisplayFilter.FILTER.equals(elName)) {
				DisplayFilter filter = new DisplayFilter();
				filter.parseElement(parser);
				this.FilterList.add(filter);
			}
		}
	}
	
	
	private String getDefaultPromptName(){
		return columnName;
	}
	
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns, TimeZone procTimeZone, 
			SimpleDateFormat dateFormat, SimpleDateFormat dateTimeFormat) {
		OMElement filter = fac.createOMElement(FILTER, ns);
		XmlUtils.addContent(fac, filter, TYPE, typeNames.get(this.type), ns);
		if (this.type == Type.Text) {
			XmlUtils.addContent(fac, filter, NAME, this.name, ns);
			XmlUtils.addContent(fac, filter, INPUT_TEXT, this.inputText, ns);
		}
		else {
			XmlUtils.addContent(fac, filter, TABLE_NAME, this.getTableName(), ns);
			XmlUtils.addContent(fac, filter, COLUMN_NAME, this.getColumnName(), ns);
			XmlUtils.addContent(fac, filter, OPERATOR, operatorNames.get(this.getOperator()), ns);
			XmlUtils.addContent(fac, filter, OPERAND, this.getOperand() == null ? null : this.getOperand().toString(), ns);
			if (this.FilterList != null && this.FilterList.size() > 0) {
				OMElement type = fac.createOMElement(FILTER_LIST, ns);			
				for (DisplayFilter f : this.FilterList) {
					f.addContent(fac, type, ns, procTimeZone, dateFormat, dateTimeFormat);
				}
				filter.addChild(type);
			}
			XmlUtils.addContent(fac, filter, COLUMN_POSITION, Integer.toString(this.columnPosition), ns);
			XmlUtils.addContent(fac, filter, PROMPT_NAME, this.promptName, ns);
			XmlUtils.addContent(fac, filter, VALID, Boolean.toString(this.valid), ns);
			XmlUtils.addContent(fac, filter, DISPLAY_NAME, Boolean.toString(this.displayName), ns);
			XmlUtils.addContent(fac, filter, DATATYPE, this.dataType, ns);
			//Need to format the displayvalue of date/datetime operand to reflect user's timezone and locale
			boolean noOperand = operatorNames.get(this.getOperator()).equalsIgnoreCase(QueryFilter.ISNULL) || operatorNames.get(this.getOperator()).equalsIgnoreCase(QueryFilter.ISNOTNULL);
			if (!noOperand && dataType != null && (this.dataType.equals("Date") || this.dataType.equals("DateTime")))
			{
				SimpleDateFormat sdf = new SimpleDateFormat(DateUtil.DB_FORMAT_STRING);
				sdf.setTimeZone(procTimeZone);
				sdf.setLenient(false);
				try {
					if (this.dataType.equals("Date"))
					{
						SimpleDateFormat df = new SimpleDateFormat(dateFormat.toPattern());
						df.setTimeZone(procTimeZone);
						df.setLenient(false);
						this.Operand_DisplayValue = df.format(sdf.parse(this.getOperand().toString()));					
						this.DateTime_Format = dateFormat.toPattern();
					}
					else
					{
						this.Operand_DisplayValue = dateTimeFormat.format((sdf.parse(this.getOperand().toString())));
						this.DateTime_Format = dateTimeFormat.toPattern();
					}
				} catch (ParseException pe) {
					this.Operand_DisplayValue = this.getOperand().toString();
				}								
			}
			XmlUtils.addContent(fac, filter, OPERAND_DISPLAYVALUE, this.Operand_DisplayValue, ns);
			XmlUtils.addContent(fac, filter, DATETIME_FORMAT, this.DateTime_Format, ns);
			if (reportIndex >= 0)
				XmlUtils.addContent(fac, filter, REPORT_INDEX, reportIndex, ns);
		}
		parent.addChild(filter);
	}

	/**
	 * Create a display filter predicate
	 * 
	 * @param columnName
	 * @param operator
	 * @param operand
	 */
	public DisplayFilter(String columnName, String operator, Object operand)
	{
		this(null, columnName, operator, operand, null);
	}

	/**
	 * Create a display filter predicate
	 * 
	 * @param columnName
	 * @param operator
	 * @param operand
	 */
	public DisplayFilter(String tableName, String columnName, String operator, Object operand)
	{
		this(tableName, columnName, operator, operand, null);
	}
	
	public DisplayFilter(String tableName, String columnName, String operator, Object operand, String dataType)
	{
		this.type = Type.Predicate;
		this.tableName = tableName;
		this.columnName = columnName;
		this.operand = operand;
		this.dataType = dataType;
		valid = setOperator(operator.trim());
	}
	/**
	 * convert name of operator to actual Operator value
	 * @param operator
	 * @return	true if a valid operator name, false if not
	 */
	private boolean setOperator(String operator)
	{
		if (operator.equals("="))
			this.operator = Operator.Equal;
		else if (operator.equals(">"))
			this.operator = Operator.GreaterThan;
		else if (operator.equals("<"))
			this.operator = Operator.LessThan;
		else if (operator.equals(">="))
			this.operator = Operator.GreaterThanOrEqual;
		else if (operator.equals("<="))
			this.operator = Operator.LessThanOrEqual;
		else if (operator.equals("<>") || operator.equals("><") || operator.equals("!="))
			this.operator = Operator.NotEqual;
		else if (operator.equalsIgnoreCase(QueryFilter.ISNULL))
			this.operator = Operator.IsNull;
		else if (operator.equalsIgnoreCase(QueryFilter.ISNOTNULL))
			this.operator = Operator.IsNotNull;
		else if (operator.equals(QueryFilter.IN))
		{
			this.operator = Operator.In;
			return setInOperator();
		}
		else if (operator.equals(QueryFilter.NOTIN))
		{
			this.operator = Operator.NotIn;
			return setInOperator();
		}
		else if (operator.equals(QueryFilter.LIKE))
		{
			this.operator = Operator.Like;
			return setLikeOperator();
		}
		else if (operator.equals(QueryFilter.NOTLIKE))
		{
			this.operator = Operator.NotLike;
			return setLikeOperator();
		}
		else
			return false;
		return true;
	}

	/**
	 * set up the JEL compiled expression for this filter
	 */
	private boolean setExpression()
	{
		if (!DisplayExpression.jelSupported)
		{
			compiledExpression = null;
			return false;
		}
		try
		{
			Library lib = new Library(new Class[] { Math.class, String.class, StringProcess.class, Double.class, Integer.class },
									  new Class[] { FieldProvider.class },
									  new Class[] { Math.class, String.class, Double.class, Integer.class },
									  null, null);
			compiledExpression = Evaluator.compile(operand.toString(), lib);
		} catch (Exception ex)
		{
			logger.warn("Parser error in display filter for old query language expression: " + ex.getMessage() + ":" + operand.toString());
			return false;
		}
		return (compiledExpression != null);
	}

	/**
	 * Create a display filter logical operation
	 * 
	 * @param filter1
	 * @param logicalOperator
	 * @param filter2
	 */
	public DisplayFilter(DisplayFilter filter1, Operator logicalOperator, DisplayFilter filter2)
	{
		this.type = Type.LogicalOperator;
		this.FilterList = new ArrayList<DisplayFilter>();
		this.FilterList.add(filter1);
		this.FilterList.add(filter2);
		this.operator = logicalOperator;
		valid = filter1.isValid() && filter2.isValid();
	}

	/**
	 * Create a display filter logical operation
	 * 
	 * @param logicalOperator
	 * @param filterList
	 */
	public DisplayFilter(Operator logicalOperator, List<DisplayFilter> filterList)
	{
		this.type = Type.LogicalOperator;
		this.FilterList = filterList;
		this.operator = logicalOperator;
		valid = true;
		for (DisplayFilter df : filterList)
		{
			if (!df.isValid())
			{
				valid = false;
				break;
			}
		}
	}

	/**
	 * Create a display filter logical operation
	 * 
	 * @param filter1
	 * @param logicalOperator
	 * @param filter2
	 */
	public DisplayFilter(DisplayFilter filter1, String logicalOperator, DisplayFilter filter2)
	{
		this.type = Type.LogicalOperator;
		this.FilterList = new ArrayList<DisplayFilter>();
		if (logicalOperator.equals("AND"))
			this.operator = Operator.And;
		else if (logicalOperator.equals("OR"))
			this.operator = Operator.Or;
		if (filter1.type == Type.LogicalOperator && filter1.operator == operator)
			FilterList.addAll(filter1.FilterList);
		else
			FilterList.add(filter1);
		if (filter2.type == Type.LogicalOperator && filter2.operator == operator)
			FilterList.addAll(filter2.FilterList);
		else
			FilterList.add(filter2);
		valid = filter1.isValid() && filter2.isValid();
	}

	/**
	 * Create a display filter logical operation
	 * 
	 * @param logicalOperator
	 * @param filterList
	 */
	public DisplayFilter(String logicalOperator, List<DisplayFilter> filterList)
	{
		this.type = Type.LogicalOperator;
		this.FilterList = filterList;
		if (logicalOperator.equals("AND"))
			this.operator = Operator.And;
		else if (logicalOperator.equals("OR"))
			this.operator = Operator.Or;
		valid = true;
		for (DisplayFilter df : filterList)
		{
			if (!df.isValid())
			{
				valid = false;
				break;
			}
		}
	}

	public Object clone() throws CloneNotSupportedException
	{
		DisplayFilter df = (DisplayFilter) super.clone();
		df.type = type;
		df.tableName = tableName;
		df.columnName = columnName;
		df.operator = operator;
		df.operand = operand;
		if (FilterList != null)
		{
			df.FilterList = new ArrayList<DisplayFilter>();
			for (DisplayFilter dfc : FilterList)
			{
				df.FilterList.add((DisplayFilter) dfc.clone());
			}
		}
		df.columnPosition = columnPosition;
		df.promptName = promptName;
		df.valid = valid;
		df.displayName = displayName;
		df.pattern = pattern;
		df.compiledExpression = compiledExpression;
		df.name = name;
		df.inputText = inputText;
		
		return df;
	}
	
	/**
	 * If operator is LIKE/NOT LIKE then setup a regular expression
	 * 
	 * @return
	 */
	private boolean setLikeOperator()
	{
		if (!String.class.isInstance(operand))
			return (false);
		String str = (String) operand;
		if (CASE_INSENSITIVE_LIKE)
			str = str.toUpperCase();
		str = str.replaceAll("%", "\\\\E.*\\\\Q").replaceAll("_", "\\\\E.\\\\Q");
		if (str.startsWith("\\E")) {
			str = str.substring(2);
		}
		else {
			str = "\\Q" + str;
		}
		if (str.endsWith("\\Q")) {
			str = str.substring(0, str.length() - 2);
		}
		else {
			str = str + "\\E";
		}
		str = "^" + str + "$";
		pattern = Pattern.compile(str);
		return (pattern != null);
	}
	
	/**
	 * If operator is IN/NOT IN then setup a regular expression
	 * 
	 * operand is A,B,C
	 * transform to
	 * pattern (A|B|C) - escape magic chars?
	 * 
	 * @return
	 */
	private boolean setInOperator()
	{
		if (operand == null)
			return false;
		if (!String.class.isInstance(operand))
			return false;
		if (operand.toString().startsWith("Q{") || operand.toString().toLowerCase().startsWith("select"))
			return false;  // don't support a subquery as a display filter
		String[] items = ((String) operand).split(",");
		StringBuilder sb = new StringBuilder();
		for (String item : items)
		{
			if (sb.length() > 0)
				sb.append('|');
			sb.append(Pattern.quote(item));
		}
		sb.insert(0, "^(");
		sb.append(")$");
		pattern = Pattern.compile(sb.toString());
		return (pattern != null);
	}

	private String getLogicalOperatorString()
	{
		if (operator == Operator.And)
			return "AND";
		else if (operator == Operator.Or)
			return "OR";
		return null;
	}

	public String toString()
	{
		if (type == Type.LogicalOperator)
		{
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < FilterList.size(); i++)
			{
				if (i > 0)
					sb.append(getLogicalOperatorString());
				sb.append("(" + ((DisplayFilter) FilterList.get(i)).toString() + ")");
			}
			return sb.toString();
		}
		if (tableName != null)
			return (tableName + "." + columnName + ((columnPosition != -2) ? " [" + columnPosition + "] " : "")
					+ getOperatorString() + (this.isPrompted() ? "(" + this.getPromptName() + ")" : (operand == null ? "" : operand)));
		else
			return (columnName + ((columnPosition != -2) ? " [" + columnPosition + "] " : "") + getOperatorString() + 
					(operand == null ? "" : operand));
	}

	/**
	 * Returns whether the filter contains a given attribute
	 * 
	 * @param dimension
	 * @param column
	 * @return
	 */
	public boolean containsMeasure(String measurename)
	{
		if ((type == Type.Predicate) && columnName.equals(measurename))
		{
			return (true);
		} else if (type == Type.LogicalOperator)
		{
			for (DisplayFilter df : FilterList)
			{
				if (df.containsMeasure(measurename))
					return (true);
			}
		}
		else if (type == Type.Text)
			return true;
		return (false);
	}

	/**
	 * Returns whether the filter contains a given attribute
	 * 
	 * @param dimension
	 * @param column
	 * @return
	 */
	public boolean containsAttribute(String dimension, String column)
	{
		if ((type == Type.Predicate) && 
				(((tableName != null && tableName.equals(dimension)) || (tableName == null && dimension == null)) 
				&& columnName.equals(column)))
		{
			return (true);
		} else if (type == Type.LogicalOperator)
		{
			for (DisplayFilter df : FilterList)
			{
				if (df.containsAttribute(dimension, column))
					return (true);
			}
		}
		else if (type == Type.Text)
			return true;
		return (false);
	}
	/**
	 * Binds a single predicate to a data set. Sets the operand type to the type of the supplied object (if conversion
	 * is possible - otherwise, leaves as-is). Saves column position.
	 * 
	 * @param c
	 * @return true if successfully bound
	 */
	private boolean bindPredicate(String[] tableNames, String[] columnNames, int[] columnDataTypes, Repository r)
	{
		if (!valid)
			return false;
		try
		{
			columnPosition = -1;
			String physicalColumnName = Util.replaceWithPhysicalString(columnName);
			for (int i = 0; i < columnNames.length; i++)
			{
				if (columnNames[i] == null)
					continue;
				
				boolean found = false;
				
				if (alias != null && alias.equals(columnNames[i]))
					found = true;
				if ((tableName == null || tableName.equals(tableNames[i])) && 
						((columnNames[i].endsWith(columnName)) || columnNames[i].endsWith(physicalColumnName))) {
					if (! columnNames[i].equals(columnName)) {
						// at this point the column name might be "F" + position + "_" + name
						// if not continue
						int index = columnNames[i].indexOf('_');
						String suffix = columnNames[i].substring(index + 1);
						if (! suffix.equals(columnName) && !suffix.equals(physicalColumnName)) {
							continue;
						}
					}
					found = true;
				}
				
				if (found)
				{
					if (tableNames[i].equals(QueryResultSet.EXPR_TABLE_NAME))
						this.containsExpression = true;
					columnPosition = i;
					if (operator == Operator.IsNull || operator == Operator.IsNotNull || operand == null)
						return true;
					switch (columnDataTypes[i])
					{
					case Types.INTEGER:
					case Types.BIGINT:
						if (operand.getClass() != Integer.class)
							operand = Long.valueOf(Double.valueOf(operand.toString()).longValue());
						break;
					case Types.DOUBLE:
						if (operand.getClass() != Double.class)
							operand = Double.valueOf(operand.toString());
						break;
					case Types.VARCHAR:
						if (operand.getClass() != String.class)
							operand = operand.toString();
						break;
					case Types.TIMESTAMP:
						if (operand.getClass() != Timestamp.class) {
							try {
								if(! QueryString.NO_FILTER_TEXT.equals(operand)){
									String operandValue = operand.toString();
									String format = (operandValue.indexOf(' ') != -1) ? DateUtil.DEFAULT_DATETIME_FORMAT : DateUtil.DEFAULT_DATE_FORMAT;
									SimpleDateFormat sdf = new SimpleDateFormat(format);
									if (r != null)
										sdf.setTimeZone(r.getServerParameters().getProcessingTimeZone());
									sdf.setLenient(false);
									operand = new Timestamp(sdf.parse(operandValue).getTime());
								}
							}
							catch (ParseException pe) {
								logger.warn("Invalid display filter operand supplied : " + operand.toString(), pe);
							}
						}
						break;
					}
					break;
				}
			}
		}
		catch (NumberFormatException nfe)
		{
			// ignore if no filter.
			if (! QueryString.NO_FILTER_TEXT.equals(operand)) {
			
				// failed due to bad number format, could be an expression, give it a try...
				if (compiledExpression != null)
					// already tried and failed
					return false;
				// try to create a compiled expression
				if (!setExpression())
					// bad expression
					return false;
				// try to evaluate the expression
				try
				{
					operand = ((CompiledExpression) compiledExpression).evaluate(null);
				}
				catch (Throwable e)
				{
					// evaluation failure
					return false;
				}
				return bindPredicate(tableNames, columnNames, columnDataTypes, r);
			}
		}
		catch (IllegalArgumentException e) {
			if (! QueryString.NO_FILTER_TEXT.equals(operand)) {
				throw e;
			}
		}
		if (columnPosition >= 0)
			return true;
		return false;
	}
	
	public DisplayFilter replaceVariables(Repository r, Session session) throws CloneNotSupportedException
	{
		DisplayFilter returnFilter = this;
		if (operand != null)
		{
			String operandWithVariablesReplaced = r.replaceVariables(null, operand.toString());
			if (session != null)
			{
				operandWithVariablesReplaced = r.replaceVariables(session, operandWithVariablesReplaced);
			}
			if (!operandWithVariablesReplaced.equals(operand.toString()))
				
			if (operandWithVariablesReplaced.charAt(0) == '\'' && operandWithVariablesReplaced.charAt(operandWithVariablesReplaced.length() - 1) == '\'')
			{
				operandWithVariablesReplaced = operandWithVariablesReplaced.substring(1, operandWithVariablesReplaced.length() - 1);
			}
			if (!operandWithVariablesReplaced.equals(operand.toString()))
			{
				returnFilter = (DisplayFilter) this.clone();
				returnFilter.operand = operandWithVariablesReplaced;
			}
		}
		if (FilterList != null && !FilterList.isEmpty())
		{
			for (int i = 0; i < returnFilter.FilterList.size(); i++)
			{
				DisplayFilter filterListItem = returnFilter.FilterList.get(i);
				DisplayFilter returnFilterListItem = filterListItem.replaceVariables(r, session);
				returnFilter.FilterList.set(i, returnFilterListItem);
			}
		}
		return returnFilter;
	}
	
	private boolean bindPredicate(SuccessModelInstance smi, List<Integer> colList, int classIndex)
	{
		columnPosition = -1;
		
		// iterate over the headers
		ModelDataSet mds = smi.getMds();
		int numHeaders = mds.numHeaders();
		
		for (int i = 0; i < numHeaders; i++)
		{
			Header hdr = mds.getHeader(i);
			
			if (i == classIndex)
			{
				if (tableName == null && columnName.equalsIgnoreCase(hdr.Name))
				{
					columnPosition = 0;
					if (operand.getClass() != Double.class)
						operand = Double.valueOf(operand.toString());
					return true;
				}
			}
			
			if (hdr.Type == Header.HeaderType.Attribute)
			{
				String dimension = smi.getSuccessModel().TargetDimension;
				StringTokenizer st = new StringTokenizer(hdr.Name, "=");
				String attribute = st.nextToken();
				if (tableName != null && tableName.equalsIgnoreCase(dimension) && columnName.equalsIgnoreCase(attribute))
				{
					// handle numeric attributes (string valued attributes can turned into nominals, ATTR=VALUE)
					if (!st.hasMoreTokens())
					{
						columnPosition = getInstancePosition(i, colList);
						if (columnPosition == -1)
						{
							logger.warn("bindToData failed, column not in the dataset: " + columnName);
							return false;
						}
						if (operand.getClass() != Double.class)
							operand = Double.valueOf(operand.toString());
						return true;
					}
					String value = st.nextToken();
					if (value != null)
					{
						if (operator == Operator.Equal)
						{
							columnName = hdr.Name;
							operand = Double.valueOf(1.0);
							columnPosition = getInstancePosition(i, colList);
							if (columnPosition == -1)
							{
								logger.warn("bindToData failed, column not in the dataset: " + columnName);
								return false;
							}
							return true;
						}
						else if (operator == Operator.NotEqual)
						{
							columnName = hdr.Name;
							operator = Operator.Equal;
							operand = Double.valueOf(0.0);
							columnPosition = getInstancePosition(i, colList);
							if (columnPosition == -1)
							{
								logger.warn("bindToData failed, column not in the dataset: " + columnName);
								return false;
							}
							return true;
						}
					}
				}
			}
			else if (hdr.Type == Header.HeaderType.Pattern)
			{
				if (tableName == null && columnName.equalsIgnoreCase(hdr.Name))
				{
					columnPosition = getInstancePosition(i, colList);
					if (columnPosition == -1)
					{
						logger.warn("bindToData failed, column not in the dataset: " + columnName);
						return false;
					}
					if (operand.getClass() != Double.class)
						operand = Double.valueOf(operand.toString());
					return true;
				}
			}
		}
		return false;
	}
	
	private int getInstancePosition(int indx, List<Integer> colList) 
	{
		for (int i = 0; i < colList.size(); i++)
		{
			if (colList.get(i) == indx)
				return i + 1; // + 1 for the target
		}
		return -1;
	}

	/**
	 * Binds the filter to a given result set. Ensures that operand data types match and column positions are set
	 * 
	 * @param tableNames
	 * @param columnNames
	 * @param columnDataTypes
	 * @return true if successfully bound
	 */
	public boolean bindToData(String[] tableNames, String[] columnNames, int[] columnDataTypes, Repository r)
	{
		if (!valid)
		{
			logger.warn("Invalid DisplayFilter, could not be bound: " + this.toString());
			return false;
		}
		this.containsExpression = false;
		if (type == Type.LogicalOperator)
		{
			for (int i = 0; i < FilterList.size(); i++)
			{
				DisplayFilter df = (DisplayFilter) FilterList.get(i);
				if (!df.bindToData(tableNames, columnNames, columnDataTypes, r))
					return (false);
				this.containsExpression = this.containsExpression || df.containsExpression;
			}
			return true;
		} else
			return (bindPredicate(tableNames, columnNames, columnDataTypes, r));
	}

	public boolean bindToData(SuccessModelInstance smi, List<Integer> colList, int classIndex)
	{
		this.containsExpression = false;
		if (type == Type.LogicalOperator)
		{
			for (int i = 0; i < FilterList.size(); i++)
			{
				DisplayFilter df = (DisplayFilter) FilterList.get(i);
				if (!df.bindToData(smi, colList, classIndex))
					return false;
				this.containsExpression = this.containsExpression || df.containsExpression;
			}
			return true;
		} else
			return (bindPredicate(smi, colList, classIndex));
	}

	/**
	 * Tests to see whether a particular filter predicate is satisfied
	 * 
	 * @param row
	 * @return
	 */
	private boolean predicateSatisfied(Object[] row, Repository r)
	{
		if (!valid)
		{
			return false;
		}
		Object o = row[columnPosition];
		if (operator == Operator.IsNull)
		{
			if (o == null)
				return true;
			return false;
		}
		else if (operator == Operator.IsNotNull)
		{
			if (o != null)
				return true;
			return false;
		}
		if (o == null || operand == null)
			return (false);
		int result = 0;
		@SuppressWarnings("rawtypes")
		Class cls = operand.getClass();
		if (cls == Long.class)
		{
			long val = 0L;
			
			try
			{
				val = ((Long) o).longValue();	
			}
			catch (ClassCastException e) 
			{
				val = Long.valueOf(o.toString());
			}
			
			long oper = 0L;
			try
			{
				oper = ((Long) operand).longValue();
			}
			catch (ClassCastException e) 
			{
				oper = Long.valueOf(operand.toString());
			}
			
			if (val > oper)
				result = 1;
			else if (val == oper)
				result = 0;
			else
				result = -1;
		} else if (cls == Integer.class)
		{
			long val = 0L;
			
			if(o instanceof Integer)
			{
				val = ((Integer) o).longValue();
			}
			else
			{
				val = Integer.valueOf(o.toString());
			}
			
			long oper = 0L;
			try
			{
				oper = ((Integer) operand).longValue();
			}
			catch (ClassCastException e) 
			{
				oper = Integer.valueOf(operand.toString());
			}
			
			if (val > oper)
				result = 1;
			else if (val == oper)
				result = 0;
			else
				result = -1;
		} else if (cls == Double.class)
		{
			double val;
			if (Double.class.isInstance(o))
				val = (Double) o;
			else if (Number.class.isInstance(o))
				val = ((Number) o).doubleValue();
			else
				val = 0;
			double oper = ((Double) operand).doubleValue();
			if (val > oper)
				result = 1;
			else if (val == oper)
				result = 0;
			else
				result = -1;
		} else if (cls == String.class)
		{
			String val = LogicalQueryString.getValue(o, r.getServerParameters().getProcessingTimeZone()); // make sure Dates for formatted correctly (really need to keep them as Dates)			
			if (operator == Operator.Like || operator == Operator.NotLike)
			{
				if (CASE_INSENSITIVE_LIKE)
					val = val.toUpperCase();
				boolean test = pattern.matcher(val).find();
				result = test ? 0 : 1;
			} else if (operator == Operator.In || operator == Operator.NotIn)
			{
				boolean test = pattern.matcher(val).find();
				result = test ? 0 : 1;
			}
			else
			{
				String oper = operand.toString();
				String str1= val;
				String str2 = oper;
				if (str1.indexOf('\'')>=0)
				{
					str1 = Util.escapeQuotesIfExists(str1);
				}
				if (str2.indexOf('\'')>=0)
				{
					str2 = Util.escapeQuotesIfExists(str2);
				}
				result = str1.compareTo(str2);
			}
		} else if (cls == Timestamp.class)
		{
			// o is of type GregorianCalendar
			long operandTime = ((Timestamp)operand).getTime();
			Date date = (o instanceof Date) ? ((Date)o) : ((GregorianCalendar)o).getTime();
			long dateTime = date.getTime();
			result = dateTime == operandTime ? 0 : dateTime > operandTime ? 1 : -1;
		}
		if (operator == Operator.Equal)
			return (result == 0);
		else if (operator == Operator.GreaterThan)
			return (result > 0);
		else if (operator == Operator.LessThan)
			return (result < 0);
		else if (operator == Operator.GreaterThanOrEqual)
			return ((result == 0) || (result > 0));
		else if (operator == Operator.LessThanOrEqual)
			return ((result == 0) || (result < 0));
		else if (operator == Operator.NotEqual)
			return (result != 0);
		else if (operator == Operator.Like || operator == Operator.In)
			return (result == 0);
		else if (operator == Operator.NotLike || operator == Operator.NotIn)
			return (result != 0);
		return false;
	}

	/**
	 * Return whether the given object satisfied this display filter
	 * 
	 * @param columnNames
	 * @param row
	 * @return
	 */
	public boolean satisfiesFilter(Object[] row, Repository r)
	{
		if (QueryString.NO_FILTER_TEXT.equals(operand)) {
			return true;
		}
		
		if (type == Type.Predicate)
		{
			return predicateSatisfied(row, r);
		} else if (type == Type.LogicalOperator)
		{
			if ((operator != Operator.And) && (operator != Operator.Or))
				return false;
			for (int i = 0; i < FilterList.size(); i++)
			{
				boolean res = FilterList.get(i).satisfiesFilter(row, r);
				if ((operator == Operator.And) && (res == false))
					return false;
				if ((operator == Operator.Or) && (res == true))
					return true;
			}
			if (operator == Operator.And)
				return true;
			if (operator == Operator.Or)
				return false;
		}
		return false;
	}

	/**
	 * @return Returns whether filter is promptable.
	 */
	public boolean isPrompted()
	{
		return promptName != null && Util.hasNonWhiteSpaceCharacters(promptName);
	}

	/**
	 * Return whether two filters are logically equal
	 * 
	 * @param qf
	 * @return
	 */
	public boolean equals(Object o)
	{
		if (this == o)
			return true;
		if (!(o instanceof DisplayFilter))
			return false;
		DisplayFilter df = (DisplayFilter) o;
		if (type != df.type)
			return false;
		if (type == Type.Predicate)
		{
			if (tableName != null)
			{
				if (df.columnName.equals(columnName) && df.tableName.equals(tableName) && (df.operator == operator) && (df.operand == operand) && (df.columnPosition == columnPosition))
					return (true);
				else
					return (false);
			} else
			{
				if (df.columnName.equals(columnName) && (df.operator == operator) && (df.operand == operand) && (df.columnPosition == columnPosition))
					return (true);
				else
					return (false);
			}
		} else if (type == Type.Text) {
			return inputText.equals(df.inputText);
		}
		else
		{
			for (int i = 0; i < FilterList.size(); i++)
			{
				if (!FilterList.get(i).equals(df.FilterList.get(i)))
					return (false);
			}
			return (true);
		}
	}
	
	public int hashCode()
	{
		  assert false : "hashCode not designed";
		  return 42; // any arbitrary constant will do 
	}
	
	public static String getQueryFilters(List<DisplayFilter> filters, boolean substitutePrompts, boolean newQueryLanguage)
	{
		if (filters == null || filters.isEmpty())
			return "";
		boolean firstTime = true;
		StringBuilder sb = new StringBuilder();
		if (newQueryLanguage)
			sb.append(" DISPLAY WHERE ");
		
		// call get processed filter here - pass display filters to it - loop over what gets returned
		List<DisplayFilter> returnedFilters = new ArrayList<DisplayFilter>();
	    returnedFilters = getProcessedFilters(filters);
		
	    for ( DisplayFilter df : returnedFilters)
		{
			if (newQueryLanguage)
			{
				if (firstTime)
				{
					firstTime = false;
				}
				else
				{
					sb.append(" AND ");
				}
				sb.append(df.getNewQueryString(substitutePrompts, true));
			}
			else
			{
				sb.append("," + df.getQueryString(substitutePrompts));
			}

		}
		return sb.toString();
	}
	
	public static List<DisplayFilter> getProcessedFilters(List<DisplayFilter> filters) {
	 	List<DisplayFilter> processedFilters = new ArrayList<DisplayFilter>();
	 	List<DisplayFilter> returnedFilters = new ArrayList<DisplayFilter>();
	 	
	 	for (int i = 0; i < filters.size(); i++)
	 	{
	 		DisplayFilter qf = (DisplayFilter) filters.get(i);
	 		
	 		if (!processedFilters.contains(qf))
	 		{   
	 			if( qf.getType() == DisplayFilter.Type.Predicate)
	 			{	
	 				if ( qf.getOperator() == (DisplayFilter.Operator.Equal) || qf.getOperator() == (DisplayFilter.Operator.Like) || qf.getOperator() == (DisplayFilter.Operator.IsNull))
	 				{
	 					/*
	 					 * Go thru rest of filters
	 					 * Look for implicit ors - i.e. other filters on same dimension and column with an equal or like
	 					 */
	 					// Save original qf - it will change
	 				 	DisplayFilter qfOrig = qf;				
	 					for (int j = i + 1; j < filters.size(); j++)
	 					{  
	 					    DisplayFilter qf2 = (DisplayFilter) filters.get(j);
	 					    
	 					   if ( isSameColumn(qfOrig, qf2) &&
	 								(qf2.getOperator() == (DisplayFilter.Operator.Equal ) || qf2.getOperator() == (DisplayFilter.Operator.Like ) || (qf2.getOperator() == (DisplayFilter.Operator.IsNull ))
	 										|| (qf2.getOperator() == (DisplayFilter.Operator.IsNull ))))
	 						{
	 								
	 						    qf = new DisplayFilter(qf, "OR", qf2);
	 							processedFilters.add(qf2);
	 						}
	 				    	    
	 					}
	 				}
	 			    returnedFilters.add(qf);
	 		    }else
	 		    	returnedFilters.add(qf);
	 	    }
	     }
	 	
	     return returnedFilters;
	 }	
	/**
	 * Determine if both QueryFilter's column names are the same 
	 * 
	 * @return true or false
	 */
	public static boolean isSameColumn(DisplayFilter qf, DisplayFilter qf2)
	{
		if( qf.alias != null &&  qf.alias.equals(qf2.alias))
			return true;
	
		String qfName;
		if( qf.getTableName() != null)
			qfName = qf.getTableName() + "." + qf.getColumnName();
		else
			qfName = qf.getColumnName();
				
		String qf2Name;
		if( qf2.getTableName() != null)
			qf2Name = qf2.getTableName() + "." + qf2.getColumnName();
		else
			qf2Name = qf2.getColumnName();
		
		if(qfName.equals(qf2Name))
			return true;
		else
			return false;
		
	}
	/**
	 * Return the query string equivalent of this filter
	 * 
	 * @param substitutePrompts
	 *            Whether to substitute prompts for operands
	 * @return
	 */
	public String getQueryString(boolean substitutePrompts)
	{
		StringBuilder sb = new StringBuilder();
		// If it's a predicate, search for appropriate column and set it
		if (type == Type.Predicate)
		{
			sb.append("DF{");
			if (alias != null) {
				sb.append(alias);
			}
			else if (tableName != null)
			{
				sb.append(tableName);
				sb.append('.');
				sb.append(columnName);
			}
			else
			{
				sb.append(columnName);
			}
			sb.append(getOperatorString());
			if (substitutePrompts && this.isPrompted())
			{
				sb.append('"');
				sb.append("+$P{");
				sb.append(this.promptName);
				sb.append('}');
				sb.append('+');
				sb.append('"');
			}
			else
			{
				if (operand != null)
					sb.append(operand);
			}
			sb.append('}');
		}
		else if (type == Type.Text) {
			return inputText;
		}
		else
		{
			sb.append("DF");
			sb.append(operator);
			sb.append('{');
			for (int i = 0; i < FilterList.size(); i++)
			{
				DisplayFilter df = (DisplayFilter) FilterList.get(i);
				if (i > 0)
					sb.append(',');
				sb.append(df.getQueryString(substitutePrompts));
			}
			sb.append('}');
		}
		return sb.toString();
	}

	public String getNewQueryString(boolean substitutePrompts, boolean useAlias)
	{
		StringBuilder sb = new StringBuilder();
		// If it's a predicate, search for appropriate column and set it
		if (type == Type.Predicate)
		{
			sb.append('[');
			if (alias != null && useAlias) {
				sb.append(alias);
			}
			else if (tableName != null)
			{
				sb.append(tableName);
				sb.append('.');
				sb.append(columnName);
			}
			else
			{
				sb.append(columnName);
			}
			sb.append(']');
			sb.append(getOperatorString());
			if (substitutePrompts && this.isPrompted())
			{
				sb.append(QueryFilter.getPromptValueString(promptName, dataType));
			}
			else
			{
				if (operand != null)
				{
					sb.append(QueryFilter.quoteOperand(operand.toString(), dataType));
				}
			}
		}
		else if (type == Type.Text) {
			return inputText;
		}
		else
		{
			sb.append('(');
			for (int i = 0; i < FilterList.size(); i++)
			{
				DisplayFilter df = (DisplayFilter) FilterList.get(i);
				if (i > 0)
				{
					sb.append(' ');
					sb.append(operator);
					sb.append(' ');
				}
				sb.append(df.getNewQueryString(substitutePrompts, useAlias));
			}
			sb.append(')');
		}
		return sb.toString();
	}
	
	/**
	 * Replace in this and all subfilters any prompted values that match a given name
	 * 
	 * @return
	 */
	public void replacePrompted(String colName, String value)
	{
		if (type == Type.Predicate)
		{
			if (this.isPrompted() && columnName.equals(colName))
			{
				operand = value;
			}
			return;
		} else if (type == Type.LogicalOperator)
		{
			for (int i = 0; i < FilterList.size(); i++)
			{
				DisplayFilter df = (DisplayFilter) FilterList.get(i);
				df.replacePrompted(colName, value);
			}
			return;
		}
	}

	/**
	 * Return a map of parameter names and default values
	 * 
	 * @return
	 */
	public void getParameterMap(Map<String, Object> parameterMap)
	{
		if (type == Type.Predicate)
		{
			if (this.isPrompted())
			{
				parameterMap.put(this.promptName, operand);
			}
			return;
		} else if (type == Type.Text) {
			return;
		}
		else
		{
			for (int i = 0; i < FilterList.size(); i++)
			{
				DisplayFilter df = (DisplayFilter) FilterList.get(i);
				df.getParameterMap(parameterMap);
			}
			return;
		}
	}

	/**
	 * @return Returns the tableName.
	 */
	public String getTableName()
	{
		return tableName;
	}
	
	public void setTableName(String s) {
		tableName = s;
	}

	/**
	 * @return Returns the columnName.
	 */
	public String getColumnName()
	{
		return columnName;
	}
	
	/**
	 * @return Returns the columnName.  To be consistent with QueryFilter
	 */
	public String getColumn()
	{
		return columnName;
	}

	/**
	 * @return Returns the operand.
	 */
	public Object getOperand()
	{
		return operand;
	}

	/**
	 * @return Returns the operator.
	 */
	public Operator getOperator()
	{
		return operator;
	}

	public String getOperatorString()
	{
		String oper = null;
		if (operator == Operator.Equal)
			oper = "=";
		else if (operator == Operator.GreaterThan)
			oper = ">";
		else if (operator == Operator.LessThan)
			oper = "<";
		else if (operator == Operator.GreaterThanOrEqual)
			oper = ">=";
		else if (operator == Operator.LessThanOrEqual)
			oper = "<=";
		else if (operator == Operator.NotEqual)
			oper = "<>";
		else if (operator == Operator.Like)
			oper = " LIKE ";
		else if (operator == Operator.NotLike)
			oper = " NOT LIKE ";
		else if (operator == Operator.In)
			oper = " IN ";
		else if (operator == Operator.NotIn)
			oper = " NOT IN ";
		else if (operator == Operator.IsNull)
			oper = " IS NULL";
		else if (operator == Operator.IsNotNull)
			oper = " IS NOT NULL";
		return (oper);
	}

	/**
	 * @return Returns whether the display filter was validly constructed (i.e. uses a recognized operator).
	 */
	public boolean isValid()
	{
		return valid;
	}

	/**
	 * @return the displayName
	 */
	public boolean isDisplayName()
	{
		return displayName;
	}

	/**
	 * @param displayName
	 *            the displayName to set
	 */
	public void setDisplayName(boolean displayName)
	{
		this.displayName = displayName;
	}

	/**
	 * @return the containsExpression
	 */
	public boolean containsExpression()
	{
		return containsExpression;
	}
	
	public List<String> getMeasures()
	{
		List<String> lst = new ArrayList<String>();

		if (type == Type.Predicate)
		{
			if (tableName == null)
			{

				lst.add(columnName);
			}
		} else if (type == Type.LogicalOperator)
		{
			for (int i = 0; i < FilterList.size(); i++)
			{
				DisplayFilter df = (DisplayFilter) FilterList.get(i);
				lst.addAll(df.getMeasures());
			}
		}
		return lst;
	}
	
	public List<String> getAttributes()
	{
		List<String> lst = new ArrayList<String>();

		if (type == Type.Predicate)
		{
			if (tableName != null)
			{
				lst.add(tableName + "." + columnName);
			}
		} else if (type == Type.LogicalOperator)
		{
			for (int i = 0; i < FilterList.size(); i++)
			{
				DisplayFilter df = (DisplayFilter) FilterList.get(i);
				lst.addAll(df.getAttributes());
			}
		}
		return lst;
	}
	
	/**
	 * Find dimension or column values in a filter
	 * 
	 * @param dimName
	 * @param colOrValue
	 * @return
	 */
	public boolean searchFilter(String dimName, String colOrValue)
	{
		if (type == Type.Predicate)
		{
			String lower = colOrValue.toLowerCase();
			if (tableName != null)
			{
				if (dimName != null && tableName.toLowerCase().indexOf(lower) >= 0)
					return true;
				if (columnName.toLowerCase().indexOf(lower) >= 0)
					return true;
				if (operand.toString().toLowerCase().indexOf(lower) >= 0)
					return true;
			} else
			{
				if (columnName.toLowerCase().indexOf(lower) >= 0)
					return true;
				if (operand.toString().toLowerCase().indexOf(lower) >= 0)
					return true;
			}
		} else if (type == Type.LogicalOperator)
		{
			for (DisplayFilter df : FilterList)
				if (df.searchFilter(dimName, colOrValue))
					return true;
		}
		return false;
	}

	/**
	 * Replace dimension or column values in a filter
	 * 
	 * @param dimName
	 * @param colOrValue
	 * @param replaceDimName
	 * @param replaceColOrValue
	 * @return
	 */
	public boolean replaceFilter(String dimName, String colOrValue, String replaceDimName, String replaceColOrValue)
	{
		if (type == Type.Predicate)
		{
			if (tableName != null)
			{
				String lower = colOrValue.toLowerCase();
				if (dimName != null && tableName.toLowerCase().indexOf(lower) >= 0)
				{
					String oldDim = tableName;
					tableName = Util.replaceStrIgnoreCase(tableName, dimName, replaceDimName);
					return !oldDim.equalsIgnoreCase(tableName);
				}
				if (columnName.toLowerCase().indexOf(lower) >= 0)
				{
					String oldCol = columnName;
					columnName = Util.replaceStrIgnoreCase(columnName, colOrValue, replaceColOrValue);
					return !oldCol.equalsIgnoreCase(columnName);
				}
				if (operand.toString().toLowerCase().indexOf(lower) >= 0)
				{
					String oldOp = operand.toString();
					operand = Util.replaceStrIgnoreCase(operand.toString(), colOrValue, replaceColOrValue);
					return !oldOp.equalsIgnoreCase(operand.toString());
				}
			} else
			{
				String lower = colOrValue.toLowerCase();
				if (columnName.toLowerCase().indexOf(lower) >= 0)
				{
					String oldCol = columnName;
					columnName = Util.replaceStrIgnoreCase(columnName, colOrValue, replaceColOrValue);
					return !oldCol.equalsIgnoreCase(columnName);
				}
				if (operand.toString().toLowerCase().indexOf(lower) >= 0)
				{
					String oldOp = operand.toString();
					operand = Util.replaceStrIgnoreCase(operand.toString(), colOrValue, replaceColOrValue);
					return !oldOp.equalsIgnoreCase(operand.toString());
				}
			}
		} else if (type == Type.LogicalOperator)
		{
			boolean replaced = false;
			for (DisplayFilter df : FilterList)
				replaced |= df.replaceFilter(dimName, colOrValue, replaceDimName, replaceColOrValue);
			return replaced;
		}
		return false;
	}

	
	/**
	 * for filter display in adhoc
	 * @return
	 */
	public String getPromptedIndicator()
	{
		return isPrompted() ? "(" + this.promptName + ")" : "";
	}
	
	/**
	 * for filter display in adhoc
	 * @param columnName
	 */
	public String getDisplayString()
	{
		return getColumn() + getOperatorString() + getOperand();
	}
	

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getPromptName() {
		return promptName;
	}

	public void setPromptName(String promptName) {
		if (promptName != null && !Util.hasNonWhiteSpaceCharacters(promptName)) {
			promptName = null;
		}
		this.promptName = promptName;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public int getColumnPosition() {
		return columnPosition;
	}

	public static DisplayFilter createFilter(Filter f) {
		List<DisplayFilter> list = new ArrayList<DisplayFilter>();
		String name = f.getColumnName();
		String parts[] = name.split("\\.");
		for (String s: f.getValues()) {
			DisplayFilter f1 = null;
			if (parts.length == 1)
				f1 = new DisplayFilter(parts[0], f.getOperator(), s);
			else
				f1 = new DisplayFilter(parts[0], parts[1], f.getOperator(), s);
			list.add(f1);
		}
		if (list.size() == 1) {
			return list.get(0);
		}
		
		return new DisplayFilter(f.getLogicalOperator(), list);
	}

	public int getReportIndex() {
		return reportIndex;
	}
	
	public void setReportIndex(int index) {
		reportIndex = index;
	}
	
	/**
	 * return true if all predicates in this filter contain dimension attributes, false if there are any measures
	 * - no clue how to handle Text
	 * @return
	 */
	public boolean containsOnlyAttributes()
	{
		if (type == Type.Predicate)
		{
			return (this.getTableName() != null);
		} else if (type == Type.Text) {
			return false;
		}
		else
		{
			for (int i = 0; i < FilterList.size(); i++)
			{
				DisplayFilter df = (DisplayFilter) FilterList.get(i);
				boolean res = df.containsOnlyAttributes();
				if (!res)
					return false;
			}
			return true;
		}
	}
	
	/**
	 * return true if all predicates in this filter contain measures, false if there are any dimension attributes
	 * - no clue how to handle Text
	 * @return
	 */
	public boolean containsOnlyMeasures()
	{
		if (type == Type.Predicate)
		{
			return (this.getTableName() == null);
		} else if (type == Type.Text) {
			return false;
		}
		else
		{
			for (int i = 0; i < FilterList.size(); i++)
			{
				DisplayFilter df = (DisplayFilter) FilterList.get(i);
				boolean res = df.containsOnlyMeasures();
				if (!res)
					return false;
			}
			return true;
		}
	}
	
	public List<DisplayFilter> getFilterList() {
		return FilterList;
	}
}
