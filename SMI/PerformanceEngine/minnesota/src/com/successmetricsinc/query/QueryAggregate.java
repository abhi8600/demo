/**
 * $Id: QueryAggregate.java,v 1.219 2012-12-06 22:38:26 BIRST\gsingh Exp $
 *
 * Copyright (C) 2007-2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.Tree;
import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Variable;
import com.successmetricsinc.status.Status;
import com.successmetricsinc.transformation.BirstScriptLexer;
import com.successmetricsinc.transformation.BirstScriptParser;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.BirstScriptParser.logicalExpression_return;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.DateUtil;
import com.successmetricsinc.util.RepositoryException;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.warehouse.LoadGroup;
import com.successmetricsinc.warehouse.LoadGroupCondition;
import com.successmetricsinc.warehouse.SecurityFilter;
import com.successmetricsinc.warehouse.StagingColumn;
import com.successmetricsinc.warehouse.StagingTable;

/**
 * Class to encapsulate functionality to create aggregates based on Query objects
 * 
 * @author Brad Peters
 * 
 */
public class QueryAggregate implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(QueryAggregate.class);
	private Query q;
	private String name;
	private Map<String, List<DimensionColumnNav>> dimensions;
	List<MeasureColumnNav> measureList;
	private Map<String, Level> levels;
	private List<DimensionTable> dtables;
	private MeasureTable mt;
	private List<Join> joins;
	private transient Repository r;
	private transient List<MeasureColumnNav> addedMeasures = new ArrayList<MeasureColumnNav>();
	private boolean retainDQTs = false;
	private DatabaseConnection connection;
	private transient DatabaseConnection memConnection;
	private boolean containsCountDistinctMeasure = false;
	private transient Status status;
	private String physicalQueryWithDQTs = null;
	private QueryFilter buildQueryFilter = null;
	private boolean inMemory = false;
	private transient String dataDir;
	private transient String databasePath;
	private boolean imported = false;
	
	public QueryAggregate(Query qry, String nm, List<DisplayExpression> expressionList) throws BaseException, CloneNotSupportedException, SQLException, IOException
	{
		this(qry, nm, expressionList, true, true, null, null, null, null);
	}

	/**
	 * Create a new aggregate table based on a Query object. Identify dimensions present in the query and create new
	 * dimension tables to represent those columns and create a new measure table to contain all of the measures. Create
	 * logical (and redundant) joins between these tables.
	 * 
	 * @param q
	 * @param name
	 * @throws NavigationException
	 * @throws SyntaxErrorException 
	 * @throws RepositoryException 
	 * @throws SQLException 
	 * @throws ScriptException 
	 * @throws IOException 
	 * @throws DisplayExpressionException
	 */
	public QueryAggregate(Query qry, String nm, List<DisplayExpression> expressionList, boolean expandDerivedQueryTables, boolean forceLevelKeys,
			DatabaseConnection conn, String buildFilter, String dataDir, String databasePath) throws CloneNotSupportedException, BaseException, SQLException,
			IOException
	{
		this(qry, nm, expressionList, expandDerivedQueryTables, forceLevelKeys, conn, buildFilter, dataDir, databasePath, false);
	}
	
	public QueryAggregate(Query qry, String nm, List<DisplayExpression> expressionList, boolean expandDerivedQueryTables, boolean forceLevelKeys,
			DatabaseConnection conn, String buildFilter, String dataDir, String databasePath, boolean imported) throws CloneNotSupportedException, BaseException, SQLException,
			IOException
		{	
		Query clonedQForDQT = null;
		q = (Query) qry.clone();
		q.isAggregate = true;
		this.imported = imported;
		r = q.getRepository();
		if (conn == null)
			connection = r.getDefaultConnection();
		else
			connection = conn;
		this.dataDir = dataDir;
		this.databasePath = databasePath;
		//Check to see if there are any build filters - keep a copy of created build filter so that we avoid
		//adding it as content filter in meta data (dim & measure tables) that we create subsequently.
		if(buildFilter != null && !buildFilter.isEmpty())
		{
			buildQueryFilter = createFilter(buildFilter);
			if(buildQueryFilter != null)
			{
				if(buildQueryFilter.containsSessionVariable())
				{
					logger.warn("BuildFilter for aggregate " + name + " contains session variables, will be ignored: " + buildFilter);
				}
				else
				{
					q.addFilter(buildQueryFilter);
				}
			}
		}
		q.replaceVariables(null);
		name =  r.getCurrentBuilderVersion()>=26 ? Util.replaceWithPhysicalString(connection,nm,r) :  Util.replaceWithPhysicalString(nm);
		if(r.getCurrentBuilderVersion()>=26)
		{
			if(name!=null && name.length() >= DatabaseConnection.getIdentifierLength(connection.DBType)-4){
				name = connection.getHashedIdentifier(name);
			}
		}
		dimensions = new HashMap<String, List<DimensionColumnNav>>();
		dtables = new ArrayList<DimensionTable>();
		joins = new ArrayList<Join>();
		measureList = new ArrayList<MeasureColumnNav>(q.measureColumns.size() + q.derivedMeasureColumns.size());
		// Extract any expressions
		if (expressionList != null)
			for (DisplayExpression de : expressionList)
			{
				de.extractAllExpressions(q, new ArrayList<String>());
			}
		for (MeasureColumnNav mcn : q.measureColumns)
			addMeasureColumn(measureList, mcn);
		for (MeasureColumnNav mcn : q.derivedMeasureColumns)
			addMeasureColumn(measureList, mcn);
		/*
		 * Do initial navigation (forces all clauses to be navigated)
		 */
		q.processImplicitColumns();
		replaceNames();
		findDimensionsAndLevels(forceLevelKeys);
		if (measureList.isEmpty())
		{
			if (dimensions.size() == 1)
			{
				logger.debug("Creating snowflake query aggregate " + nm + " for query: " + QueryString.filterServerPassword(q.getLogicalQueryString(false, null, false)));
			} else
			{
				logger.warn("No measures and more than one dimension supplied for query aggregate - unable to create:"
						+ QueryString.filterServerPassword(q.toString()));
				throw (new NavigationException("Unable to create query aggregate - no measures specified"));
			}
		} else
			logger.info("Processing query aggregate " + nm + " for query: " + QueryString.filterServerPassword(q.getLogicalQueryString(false, null,false)));
		q.setExpandDerivedQueryTables(expandDerivedQueryTables);
		if (!expandDerivedQueryTables)
		{
			q.setDerivedQueryTableSuffix("_" + Long.valueOf(System.currentTimeMillis()).toString());
		}
		Map<String, DimensionColumn> addedPhysicalDimColumns = new HashMap<String,DimensionColumn>();
		Set<String> addedDimColumnDisplayNames = new HashSet<String>();
		List<DimensionColumnNav> addedDimensionColumns = new ArrayList<DimensionColumnNav>();
		for(DimensionColumnNav dcn: q.dimensionColumns)
		{
			logger.debug("processing dimension columns for: " + dcn.dimensionName + "." + dcn.columnName);
			if (dcn.constant)
				continue;			
			for(DimensionColumn dc: r.findDimensionColumns(dcn.dimensionName, dcn.columnName))
			{
				boolean hasDynamicVariables = hasDynamicVariablesInPhysicalName(dc);
				if (hasDynamicVariables)
				{
					dcn.navigateonly = true;
					// Remove from any filters
					List<QueryFilter> toRemove = new ArrayList<QueryFilter>();
					for(QueryFilter qf: q.filters)
						if (qf.containsAttribute(dc.DimensionTable.DimensionName, dc.ColumnName))
							toRemove.add(qf);
					q.filters.removeAll(toRemove);
					// Extract any columns in the formula if there are session variables
					for (DimensionColumn dc2: dc.DimensionTable.DimensionColumns)
					{
						if (!dc2.AutoGenerated || !dc2.Qualify)
							continue;
						boolean secFilter = false;
						if (dc.DimensionTable.TableSource.Filters != null)
						{
							for (TableSourceFilter tsf : dc.DimensionTable.TableSource.Filters)
							{
								if (tsf.SecurityFilter && tsf.Filter.contains(dc2.PhysicalName))
								{
									secFilter = true;
								}
							}
						}
						if ((dc.PhysicalName.contains(dc2.PhysicalName) || secFilter) && !addedPhysicalDimColumns.containsKey(dc2.PhysicalName))
						{
							int index = dc2.PhysicalName.indexOf('.');
							String pname = dc2.PhysicalName.substring(index + 1);
							boolean found = false;
							for(MeasureColumnNav smcn: q.measureColumns)
							{
								if (smcn.displayName.equals(pname))
								{
									found = true;
									break;
								}
							}
							for(DimensionColumnNav sdcn: q.dimensionColumns)
							{
								if (sdcn.displayName.equals(pname))
								{
									found = true;
									break;
								}
							}
							if (!found && !addedDimColumnDisplayNames.contains(pname))
							{
								addedPhysicalDimColumns.put(dc2.PhysicalName, dc2);
								DimensionColumnNav dcn2 = new DimensionColumnNav();
								dcn2.columnName = dc2.ColumnName;
								dcn2.dimensionName = dc2.DimensionTable.DimensionName;
								dcn2.matchingColumns = new ArrayList<DimensionColumn>();
								dcn2.matchingColumns.add(dc2);
								dcn2.displayName = pname;
								addedDimensionColumns.add(dcn2);
								addedDimColumnDisplayNames.add(pname);
							}
						}
					}
					break;
				}
			}
		}
		q.dimensionColumns.addAll(addedDimensionColumns);
		for(DimensionColumnNav dcn: addedDimensionColumns)
		{
			GroupBy gb = new GroupBy(dcn.dimensionName, dcn.columnName);
			q.getGroupByClauses().add(gb);
		}
		/*
		 * Remove any columns with session variables from the projection list (there will still be issues if people try
		 * to use session variables in columns in order by or filters)
		 */
		Map<String, MeasureColumn> addedPhysicalColumns = new HashMap<String,MeasureColumn>();
		for(MeasureColumnNav mcn: q.measureColumns)
		{
			for(MeasureColumn mc: r.findMeasureColumns(mcn.measureName))
			{
				if (mc.SessionVariables != null && mc.SessionVariables.size() > 0)
				{
					mcn.navigateonly = true;
					// Remove from any filters
					List<QueryFilter> toRemove = new ArrayList<QueryFilter>();
					for(QueryFilter qf: q.filters)
						if (qf.containsMeasure(mc.ColumnName))
							toRemove.add(qf);
					q.filters.removeAll(toRemove);
					// Extract any columns in the formula if there are session variables
					for (MeasureColumn mc2: mc.MeasureTable.MeasureColumns)
					{
						if (!mc2.AutoGenerated || !mc2.Qualify)
							continue;
						boolean secFilter = false;
						if (mc.MeasureTable.TableSource.Filters != null)
						{
							for(TableSourceFilter tsf: mc.MeasureTable.TableSource.Filters)
							{
								if (tsf.SecurityFilter && tsf.Filter.contains(mc2.PhysicalName))
								{
									secFilter = true;
								}
							}
						}
						if ((mc.PhysicalName.contains(mc2.PhysicalName) || (mc.PhysicalFilter != null && mc.PhysicalFilter.contains(mc2.PhysicalName)) || secFilter)
								&& !addedPhysicalColumns.containsKey(mc2.PhysicalName))
						{
							int index = mc2.PhysicalName.indexOf('.');
							String pname = mc2.PhysicalName.substring(index + 1);
							// Don't add if a column with the same name is there
							boolean found = false;
							for(MeasureColumnNav smcn: q.measureColumns)
							{
								if (smcn.displayName.equals(pname))
								{
									found = true;
									break;
								}
							}
							for(DimensionColumnNav sdcn: addedDimensionColumns)
							{
								if (sdcn.displayName.equals(pname))
								{
									found = true;
									break;
								}
							}
							for(DimensionColumnNav sdcn: q.dimensionColumns)
							{
								if (sdcn.displayName.equals(pname))
								{
									found = true;
									break;
								}
							}
							if (!found)
							{
								addedPhysicalColumns.put(mc2.PhysicalName, mc2);
								MeasureColumnNav mcn2 = new MeasureColumnNav();
								mc2 = (MeasureColumn) mc2.clone();
								mcn2.measureName = mc2.ColumnName;
								mcn2.matchingMeasures = new ArrayList<MeasureColumn>();
								mcn2.matchingMeasures.add(mc2);
								mcn2.displayName = pname;
								mcn2.sessionVariableExpand = true;
								addedMeasures.add(mcn2);
							}
						}
					}
					break;
				}
			}
		}
		List<MeasureColumnNav> originalMeasures = new ArrayList<MeasureColumnNav>(q.measureColumns);
		q.measureColumns.addAll(addedMeasures);
		measureList.addAll(addedMeasures);
		if(!q.isExpandDerivedQueryTables())
		{
			clonedQForDQT = (Query)q.clone();
		}
		q.navigateQuery(true, true, null, false);
		if(clonedQForDQT != null)
		{
			clonedQForDQT.navigateQuery(true, true, null, false);
		}
		// Remove any added measures that were otherwise navigated to
		List<MeasureColumnNav> toRemove = new ArrayList<MeasureColumnNav>();
		{
			for(MeasureColumnNav mcn: addedMeasures)
			{
				boolean found = false;
				for (MeasureColumnNav smcn : originalMeasures)
				{
					if (smcn.matchingMeasures.get(smcn.pick).PhysicalName.equals(mcn.matchingMeasures.get(mcn.pick).PhysicalName))
					{
						found = true;
						break;
					}
				}
				if (found)
					toRemove.add(mcn);
			}
		}
		addedMeasures.removeAll(toRemove);
		overrideRules(q);
		if(clonedQForDQT != null)
		{
			overrideRules(clonedQForDQT);
		}
		if(clonedQForDQT != null)
		{
			clonedQForDQT.setExpandDerivedQueryTables(true);
			clonedQForDQT.setDerivedQueryTableSuffix(null);
			clonedQForDQT.generateQuery(null, true, false);
			physicalQueryWithDQTs = clonedQForDQT.getQuery();
		}
		q.generateQuery(null, true, false);		
		process();
	}
	
	private void overrideRules(Query q) throws CloneNotSupportedException
	{
		/* 
		 * Override aggregation rules for columns that have been added (since they are part of formulas they are not presumed to be aggregated)
		 */
		for (Navigation n: q.getNavigationList())
			for (QueryColumn qc : n.getProjectionList())
			{
				if (qc.type != QueryColumn.MEASURE)
					continue;
				boolean isClonedMC = false;
				MeasureColumn mc = (MeasureColumn) qc.o;
				for (MeasureColumnNav mcnav : addedMeasures)
				{
					if (mcnav.measureName.equals(mc.ColumnName))
					{
						mc = (MeasureColumn) mc.clone();
						isClonedMC = true;
						mc.AggregationRule = "MAX";
						// Use an existing aggregate rule if present in another column
						for (MeasureColumnNav smcn : q.measureColumns)
						{
							boolean found = false;
							for (MeasureColumnNav smcn2 : addedMeasures)
							{
								if (smcn2.measureName.equals(smcn.measureName))
								{
									found = true;
								}
							}
							if (!found && smcn != mcnav && !addedMeasures.contains(smcn)
									&& smcn.matchingMeasures.get(smcn.pick).PhysicalName.equals(mc.PhysicalName))
							{
								mc.AggregationRule = smcn.matchingMeasures.get(smcn.pick).AggregationRule;
								break;
							}
						}
						qc.o = mc;
						break;
					}
				}
				if(mc.AggregationRule != null && mc.AggregationRule.equalsIgnoreCase("COUNT DISTINCT"))
				{
					if(!isClonedMC)
					{
						mc = (MeasureColumn) mc.clone();
					}
					mc.AggregationRule = "MAX";
					qc.o = mc;
					containsCountDistinctMeasure = true;
				}
			}
	}

	/**
	 * Add a measure column without duplicates
	 * 
	 * @param list
	 * @param mcn
	 */
	private void addMeasureColumn(List<MeasureColumnNav> list, MeasureColumnNav mcn)
	{
		boolean found = false;
		for (MeasureColumnNav mn : list)
		{
			if (mn.measureName.equals(mcn.measureName))
			{
				found = true;
				break;
			}
		}
		if (!found)
			list.add(mcn);
	}

	private void replaceNames() throws NavigationException
	{
		/* Replace all display names with new names that will be physically persisted as column names */
		for (DimensionColumnNav dcn : q.dimensionColumns)
		{
			if (!dcn.constant)
			{
				if (dcn.navigateonly)
				{
					/* Make sure any columns that are just being navigated for filters are included */
					for (QueryFilter qf : q.getFilterList())
					{
						if (qf.containsAttribute(dcn.dimensionName, dcn.columnName))
						{
							dcn.displayName = getDimensionColumnPhysicalName(dcn);
							q.exposeNavigateOnlyDimensionColumn(dcn);
							q.addGroupBy(new GroupBy(dcn.dimensionName, dcn.columnName));
							break;
						}
					}
				} else
				{
					dcn.displayName = getDimensionColumnPhysicalName(dcn);
				}
			}
		}
		for (MeasureColumnNav mcn : measureList)
		{
			if (!mcn.hidden)
			{
				if (mcn.navigateonly)
				{
					/* Make sure any columns that are just being navigated for filters are included */
					for (QueryFilter qf : q.getFilterList())
					{
						if (qf.containsMeasure(mcn.measureName))
						{
							mcn.navigateonly = false;
							break;
						}
					}
				}
				if (!mcn.navigateonly && mcn.filter == null)
				{
					MeasureColumn mc = r.findMeasureColumn(mcn.measureName);
					if (mc == null)
						throw (new NavigationException("Unable to locate measure: " + mcn.measureName));
					mcn.displayName = Util.replaceWithPhysicalString(mc.ColumnName);
				} else if (mcn.filter != null)
				{
					mcn.displayName = Util.replaceWithPhysicalString(mcn.displayName);
				}
			}
		}
	}

	private String getDimensionColumnPhysicalName(DimensionColumnNav dcn) throws NavigationException
	{
		DimensionColumn dc = r.findDimensionColumn(dcn.dimensionName, dcn.columnName);
		if (dc == null)
			throw (new NavigationException("Unable to locate dimension column: " + dcn.dimensionName + "." + dcn.columnName));
		if ((dcn.displayName == null) || ((dcn.displayName != null) && (dcn.displayName.equals(dc.ColumnName))))
		{
			return Util.buildDimensionColumn(connection, dc.DimensionTable.DimensionName, dc.ColumnName).replaceAll("/", "_");
		} else
		{
			return Util.replaceWithPhysicalString(dcn.displayName);
		}
	}

	private void findDimensionsAndLevels(boolean forceDimensionLevelKeys) throws NavigationException
	{
		levels = new HashMap<String, Level>();
		for (DimensionColumnNav dcn : q.dimensionColumns)
		{
			/* Find the appropriate levels for each dimension */
			if (!dcn.navigateonly && !dcn.constant)
			{
				Level l = levels.get(dcn.dimensionName);
				if (l == null)
				{
					Hierarchy h = r.findDimensionHierarchy(dcn.dimensionName);
					if (h != null)
					{
						l = h.findLevelColumn(dcn.columnName);
						levels.put(dcn.dimensionName, l);
					}
				} else
				{
					Level newLevel = l.findLevelColumn(dcn.columnName);
					if (newLevel != null)
					{
						l = newLevel;
						levels.put(dcn.dimensionName, l);
					}
				}
			}
		}
		replaceNullLevels(levels, q.dimensionColumns, r);
		/*
		 * Now, make sure the query has primary level keys for each dimension
		 */
		for (Map.Entry<String, Level> entry : levels.entrySet())
		{
			if (!forceDimensionLevelKeys)
				break;
			String dimName = entry.getKey();
			Level l = entry.getValue();
			LevelKey lk = l.getPrimaryKey();
			if (lk == null)
				throw (new NavigationException("Level " + l.Name + " does not contain a level key"));
			for (int c = 0; c < lk.ColumnNames.length; c++)
			{
				String levelKeyCol = lk.ColumnNames[c];
				DimensionColumnNav dcn = q.findDimensionColumnNav(dimName, levelKeyCol);
				if (dcn == null || dcn.navigateonly)
				{
					q.addDimensionColumn(dimName, levelKeyCol, keyColumnName(dimName, levelKeyCol));
					q.addGroupBy(new GroupBy(dimName, levelKeyCol));
				} else
				{
					dcn.displayName = keyColumnName(dimName, levelKeyCol);
				}
			}
		}
		/*
		 * Add all the query dimension columns to the dimension
		 */
		for (DimensionColumnNav dcn : q.dimensionColumns)
		{
			/* Separate out dimension columns by dimension */
			List<DimensionColumnNav> dcolumns = dimensions.get(dcn.dimensionName);
			if (dcolumns == null)
			{
				dcolumns = new ArrayList<DimensionColumnNav>();
				dimensions.put(dcn.dimensionName, dcolumns);
			}
			// Don't add duplicates
			boolean found = false;
			for (DimensionColumnNav dn : dcolumns)
			{
				if (dn.dimensionName.equals(dcn.dimensionName) && dn.columnName.equals(dcn.columnName))
				{
					found = true;
					break;
				}
			}
			if (!found)
				dcolumns.add(dcn);
		}
	}

	private String keyColumnName(String dimName, String colName)
	{
		return Util.buildDimensionColumn(connection, dimName, colName);
	}

	private void process() throws NavigationException, CloneNotSupportedException
	{
		/* Create new dimension tables */
		List<TableSourceFilter> mtflist = createDimensionTables();		
		/* Create metadata for measure table */
		if (measureList.size() > 0)
		{
			createMeasureTable(mtflist);
		} else
		{
			/*
			 * Otherwise, create snowflake
			 */
			createSnowflakeDimension();
		}
		return;
	}

	private void createSnowflakeDimension() throws CloneNotSupportedException
	{
		r.addDimensionTables(dtables);
		r.addJoins(joins);
		// First, find the table it should join to
		DimensionTable dt = dtables.get(0);
		Level l = levels.entrySet().iterator().next().getValue();
		/*
		 * Snowflake for each appropriate dimension table at this level
		 */
		for (DimensionTable mdt : r.DimensionTables)
		{
			if (mdt != dt && mdt.DimensionName.equals(dt.DimensionName) && mdt.Level != null && mdt.Level.equals(l.Name))
			{
				Join j = new Join();
				j.Table1 = dt;
				j.Table1Str = dt.TableName;
				j.Table2 = mdt;
				j.Table2Str = mdt.TableName;
				j.Redundant = false;
				j.Type = JoinType.Inner;
				boolean addJoin = false;
				/*
				 * Join by all columns in whatever key matches
				 */
				for (LevelKey key : l.Keys)
				{
					int numfound = 0;
					for (String levelKey : key.ColumnNames)
					{
						/*
						 * Find the physical column in the table associated with the level keys
						 */
						String key1 = null;
						for (DimensionColumn dc : mdt.DimensionColumns)
						{
							if (dc.ColumnName.equals(levelKey))
							{
								key1 = dc.PhysicalName;
								break;
							}
						}
						/* Only join if level key exists in dimension table */
						if (key1 != null)
						{
							String joinStr = "\"" + dt.TableName + "\"." + keyColumnName(dt.DimensionName, levelKey) + "=\"" + mdt.TableName + "\"." + key1;
							if (j.JoinCondition == null)
								j.JoinCondition = joinStr;
							else
								j.JoinCondition = j.JoinCondition + " AND " + joinStr;
							numfound++;
							addJoin = true;
						} else
						{
							logger.warn("Unable to create a snowflake join between " + dt.TableName + " and " + mdt.TableName + " - Level key column "
									+ levelKey + " not present in table " + mdt.TableName);
							addJoin = false;
							break;
						}
					}
					if (numfound == key.ColumnNames.length)
					{
						joins.add(j);
						break;
					} else
						j.JoinCondition = null;
				}
				if (addJoin)
				{
					joins.add(j);
					r.addJoin(j);
					Set<DimensionTable> tableSet = new LinkedHashSet<DimensionTable>();
					tableSet.add(mdt);
					/*
					 * Add new dimension table second - ensures proper join order. That way the measure tables are
					 * first.
					 */
					tableSet.add(dt);
					List<DimensionTable> sdt = r.createSnowflake(tableSet);
					if (sdt != null)
						this.dtables.addAll(sdt);
				}
			}
		}
	}

	private void createMeasureTable(List<TableSourceFilter> mtflist) throws CloneNotSupportedException
	{
		/* Create measure table */
		mt = new MeasureTable();
		mt.QueryAgg = this;
		mt.Imported = imported;
		mt.Cacheable = true;
		mt.TTL = -1;
		mt.PhysicalName = name;
		mt.TableName = name + "_AGG";
		mt.Type = MeasureTable.NORMAL;
		mt.temped = false;
		/* Create table source */
		mt.TableSource = new TableSource();
		mt.TableSource.connection = connection;
		mt.TableSource.ConnectionName = mt.TableSource.connection.Name;
		List<QueryFilter> contentFilters = new ArrayList<QueryFilter>();
		for(QueryFilter qf: q.filters)
		{
			if (!qf.containsSessionVariable() && (buildQueryFilter == null || (!qf.equals(buildQueryFilter))))
				contentFilters.add(qf);
		}
		mt.TableSource.contentFilters = contentFilters;
		mt.TableSource.Tables = new TableDefinition[1];
		mt.TableSource.Tables[0] = new TableDefinition();
		mt.TableSource.Tables[0].PhysicalName = mt.PhysicalName;
		mt.TableSource.Tables[0].Valid = true;
		/*
		 * Use the measure list from navigated query for calculating the cardinality so that the list includes the base
		 * measures used in derived measures as well.
		 */
		List<MeasureColumnNav> cardinalityMeasureList = new ArrayList<MeasureColumnNav>(this.q.measureColumns.size());
		for (MeasureColumnNav mcn : this.q.measureColumns)
			addMeasureColumn(cardinalityMeasureList, mcn);
		/*
		 * Calculate cardinality - should be the lowest cardinality of all measures selected - in order to ensure that
		 * the aggregate is used
		 */
		mt.Cardinality = 0;
		for (MeasureColumnNav mcn : cardinalityMeasureList)
		{
			if (!mcn.navigateonly)
			{
				if (mt.Cardinality == 0 || (mcn.pick >= 0 && mcn.matchingMeasures.get(mcn.pick).MeasureTable.Cardinality < mt.Cardinality))
				{
					mt.Cardinality = mcn.matchingMeasures.get(mcn.pick).MeasureTable.Cardinality;
					logger.debug("Assigned cardinality value " + mt.Cardinality + " for agg measure table: " + mt.TableName + ", source measure table: "
							+ mcn.matchingMeasures.get(mcn.pick).MeasureTable.TableName + ", source measure column: "
							+ mcn.matchingMeasures.get(mcn.pick).ColumnName);
				}
			}
		}
		mt.Cardinality *= 0.99;
		int numMeasures = 0;
		for (MeasureColumnNav mcn : measureList)
		{
			boolean isSessionVariable = mcn.matchingMeasures.get(mcn.pick).SessionVariables != null && mcn.matchingMeasures.get(mcn.pick).SessionVariables.size()>0; 
			if ((!mcn.navigateonly && !mcn.hidden) || isSessionVariable)
			{
				// if (mcn.filter == null || mcn.matchingMeasures.get(mcn.pick).BaseMeasureName != null)
				numMeasures++;
			}
		}
		int curMeasureCol = 0;
		mt.MeasureColumns = new MeasureColumn[numMeasures];
		for (MeasureColumnNav mcn : measureList)
		{
			boolean isSessionVariable = mcn.matchingMeasures.get(mcn.pick).SessionVariables != null && mcn.matchingMeasures.get(mcn.pick).SessionVariables.size()>0; 
			if ((!mcn.navigateonly && !mcn.hidden) || isSessionVariable)
			{
				MeasureColumn mc = (MeasureColumn) mcn.matchingMeasures.get(mcn.pick).clone();
				if (hasDynamicVariablesInPhysicalNameOrFilter(mc))
				{
					String physicalName = Util.replaceStr(mc.PhysicalName, mc.MeasureTable.TableSource.Tables[0].PhysicalName, name);
					mc.PhysicalName = mc.Qualify ? connection.getHashedIdentifier(physicalName) : physicalName;
				} else
				{
					mc.PhysicalName = name + "." + (mc.Qualify ? connection.getHashedIdentifier(mcn.displayName) : mcn.displayName);
				}				
				if (Util.unQualifyPhysicalName(mc.PhysicalName).length() > DatabaseConnection.getIdentifierLength((connection.DBType)) && connection.requiresHashedIdentifier())
				{
					mc.PhysicalName = Util.replaceWithPhysicalString(connection, mc.ColumnName);
				}
				/*
				 * Physical filters are generated on measure columns in order to efficiently deal with case when statements.
				 * Previously, we were persisting the measure columns used in physical filters along with aggregate and then
				 * appending the physical filters as where conditions to the physical filters. However, it was not generating
				 * the proper query in some cases and hence we have persisted the physical filters as a part of the aggregate 
				 * We are putting this behind a builder version to make sure it does not break existing cases. The below code will 
				 * make sure that physical filters are stripped when the measure column does not have session variables and else carried forward.
				 */
				if (mc.PhysicalFilter != null && r.getCurrentBuilderVersion()>=32)
				{
					if(mc.SessionVariables == null || mc.SessionVariables.isEmpty())
					{
						mc.PhysicalFilter = null;
					}
					else
					{
						mc.PhysicalFilter = Util.replaceStr(mc.PhysicalFilter, mc.MeasureTable.TableSource.Tables[0].PhysicalName, name);
					}
				}
				else if (mc.PhysicalFilter != null)
				{
					mc.PhysicalFilter = Util.replaceStr(mc.PhysicalFilter, mc.MeasureTable.TableSource.Tables[0].PhysicalName, name);
				}
				mc.TableName = mt.TableName;
				mc.MeasureTable = mt;
				if (mc.BaseMeasureName == null)
					mc.MeasureFilter = mcn.filter;
				mt.MeasureColumns[curMeasureCol++] = mc;
				// Search for security filters
				if (!isSessionVariable)
					for (StagingTable st : r.getStagingTables())
					{
						for (StagingColumn sc : st.Columns)
						{
							if (sc.SecFilter != null && sc.Name.equals(mc.ColumnName) && sc.TargetTypes != null)
							{
								boolean isOK = sc.TargetTypes.contains("Measure");
								if (isOK)
								{
									TableSourceFilter tsf = new TableSourceFilter();
									tsf.SecurityFilter = true;
									if (sc.SecFilter.Type == SecurityFilter.TYPE_VARIABLE)
									{
										tsf.Filter = mc.PhysicalName + " IN (V{" + sc.SecFilter.SessionVariable + "})";
									} else
									{
										// set based filter. Put logical query directly
										tsf.Filter = mc.PhysicalName + " IN (Q{" + sc.SecFilter.SessionVariable + "})";
									}
									boolean found = false;
									for (TableSourceFilter stsf : mtflist)
									{
										if (stsf.Filter.equals(tsf.Filter) && stsf.SecurityFilter && tsf.SecurityFilter)
										{
											found = true;
											break;
										}
									}
									tsf.FilterGroups = sc.SecFilter.FilterGroups;
									if (!found)
										mtflist.add(tsf);
								}
							}
						}
					}
			}
		}
		// Add security filters
		if (mtflist.size() > 0)
		{
			mt.TableSource.Filters = new TableSourceFilter[mtflist.size()];
			mtflist.toArray(mt.TableSource.Filters);
		}
		/* Create redundant joins */
		for (DimensionTable dt : dtables)
		{
			Join j = new Join();
			j.Table1 = mt;
			j.Table1Str = mt.TableName;
			j.Table2 = dt;
			j.Table2Str = dt.TableName;
			j.Redundant = true;
			j.Type = JoinType.Inner;
			j.JoinCondition = "";
			j.JoinLevel = getJoinLevel(dt);
			if (j.JoinLevel != null)
			{
				j.LevelStr = j.JoinLevel.Name;
			} else
			{
				j.LevelStr = null;
			}
			joins.add(j);
		}
		/*
		 * Create joins between measure table and other dimension tables that joined to all component measure tables
		 */
		// First, get a list of component measure tables
		Set<MeasureTable> measureTables = new HashSet<MeasureTable>();
		for (MeasureColumnNav mcn : this.q.measureColumns)
		{
			measureTables.add(mcn.matchingMeasures.get(mcn.pick).MeasureTable);
		}
		// Examine each dimension in the query
		for (DimensionTable dt : dtables)
		{
			/*
			 * Go through all dimension tables in the repository and create a map that contains a list of inheriting
			 * dimension tables keyed by base dimension table. If a given dimension table does not inherit from any
			 * other dimension table, place itself in the list of inheriting dimension tables.
			 */
			Map<DimensionTable, List<DimensionTable>> dtablesMap = new HashMap<DimensionTable, List<DimensionTable>>();
			for (DimensionTable repdt : r.DimensionTables)
			{
				DimensionTable basedt = null;
				if (repdt.VirtualMeasuresInheritTable == null || repdt.VirtualMeasuresInheritTable.equals(""))
				{
					basedt = repdt;
				} else
				{
					basedt = r.findDimensionTable(repdt.VirtualMeasuresInheritTable);
					if (basedt == null)
					{
						logger.warn("Can not find inherit dimension table " + repdt.VirtualMeasuresInheritTable
								+ " - corresponding join will not be processed for aggregate.");
						continue;
					}
				}
				// See if each is in the dimension, and at the right level
				if (basedt.DimensionName != null && basedt.Level != null && basedt.DimensionName.equals(dt.DimensionName) && basedt.Level.equals(dt.Level)
						&& (basedt.InheritTable == null || basedt.InheritTable.equals("")))
				{
					List<DimensionTable> inheritingTables = dtablesMap.get(basedt);
					if (inheritingTables == null)
					{
						inheritingTables = new ArrayList<DimensionTable>();
						dtablesMap.put(basedt, inheritingTables);
					}
					if (!inheritingTables.contains(repdt))
					{
						inheritingTables.add(repdt);
					}
				}
			}
			/*
			 * If any of the inheriting dimension tables join with a measure table, create a join with the base table
			 */
			for (DimensionTable basedt : dtablesMap.keySet())
			{
				List<DimensionTable> inheritingTables = dtablesMap.get(basedt);
				boolean isJoined = false;
				search: for (DimensionTable inheritingdt : inheritingTables)
				{
					for (MeasureTable qmt : measureTables)
					{
						if (r.joinedTables(qmt, inheritingdt))
						{
							isJoined = true;
							break search;
						}
					}
				}
				if (isJoined)
				{
					Join j = new Join();
					j.Table1 = mt;
					j.Table1Str = mt.TableName;
					j.Table2 = basedt;
					j.Table2Str = basedt.TableName;
					j.Redundant = false;
					j.Type = JoinType.Inner;
					j.JoinLevel = getJoinLevel(basedt);
					if (j.JoinLevel != null)
					{
						j.LevelStr = j.JoinLevel.Name;
					} else
					{
						j.LevelStr = null;
					}
					Level l = levels.get(basedt.DimensionName);
					/*
					 * Join by all columns in whatever key matches
					 */
					for (LevelKey key : l.Keys)
					{
						int numfound = 0;
						for (String levelKey : key.ColumnNames)
						{
							/*
							 * Find the physical column in the table associated with the level keys
							 */
							String key1 = null;
							for (DimensionColumn dc : basedt.DimensionColumns)
							{
								if (dc.ColumnName.equals(levelKey))
								{
									key1 = dc.PhysicalName;
									break;
								}
							}
							String key2 = null;
							for (DimensionColumn dc : dt.DimensionColumns)
							{
								if (dc.ColumnName.equals(levelKey))
								{
									key2 = dc.PhysicalName;
									break;
								}
							}
							/* Only join if level key exists in dimension table and the fact table */
							if (key1 != null && key2 != null)
							{
								String joinstr = "\"" + mt.TableName + "\"." + keyColumnName(dt.DimensionName, levelKey) + "=\"" + basedt.TableName + "\"."
										+ key1;
								if (j.JoinCondition == null)
									j.JoinCondition = joinstr;
								else
									j.JoinCondition += " AND " + joinstr;
								numfound++;
							}
						}
						if (numfound == key.ColumnNames.length)
						{
							joins.add(j);
							break;
						} else
							j.JoinCondition = null;
					}
				}
			}
		}
		r.addMeasureTables(Arrays.asList(new MeasureTable[]
		{ mt }));
		r.addDimensionTables(dtables);
		r.addJoins(joins);
		return;
	}

	// /**
	// * Update the cardinality of the measure table representing this aggregate
	// */
	// private void updateCardinality() {
	// assert(mt != null);
	// // call database to get the rowcount.
	// DatabaseConnection dc = r.findConnection(Repository.DEFAULT_DB_CONNECTION);
	// Database.getRowCount(dc,name);
	//		
	// }
	private List<TableSourceFilter> createDimensionTables() throws CloneNotSupportedException, NavigationException
	{
		List<TableSourceFilter> mtflist = new ArrayList<TableSourceFilter>();
		for (Map.Entry<String, List<DimensionColumnNav>> entry : dimensions.entrySet())
		{
			DimensionTable dt = new DimensionTable(r);
			dt.QueryAgg = this;
			dt.Imported = imported;
			dt.DimensionName = entry.getKey();
			dt.TableName = dt.DimensionName + "_" + name;
			dt.Cacheable = true;
			dt.TTL = -1;
			dt.PhysicalName = name;
			dt.Type = DimensionTable.NORMAL;
			List<DimensionColumn> dimclist = new ArrayList<DimensionColumn>();
			Level l = levels.get(dt.DimensionName);
			if (l != null)
				dt.Level = l.Name;
			/* Create Table Source */
			dt.TableSource = new TableSource();
			dt.TableSource.connection = connection;
			dt.TableSource.ConnectionName = dt.TableSource.connection.Name;
			List<QueryFilter> contentFilters = new ArrayList<QueryFilter>();
			for(QueryFilter qf: q.filters)
			{
				if (qf.containsSessionVariable() && (buildQueryFilter == null || (!qf.equals(buildQueryFilter))))
					contentFilters.add(qf);
			}
			dt.TableSource.contentFilters = contentFilters;
			dt.TableSource.Tables = new TableDefinition[1];
			dt.TableSource.Tables[0] = new TableDefinition();
			dt.TableSource.Tables[0].PhysicalName = dt.PhysicalName;
			dt.TableSource.Tables[0].Valid = true;
			List<TableSourceFilter> flist = new ArrayList<TableSourceFilter>();
			/* Create columns */
			for (DimensionColumnNav dcn : entry.getValue())
			{
				if (dcn.constant)
					continue;
				DimensionColumn propDC = r.findDimensionColumn(dcn.dimensionName, dcn.columnName);
				boolean isSessionVariable = propDC.SessionVariables != null && propDC.SessionVariables.size() > 0;
				if ((!dcn.navigateonly || isSessionVariable) && !dcn.constant)
				{
					if (propDC.VC != null && measureList.size() > 0)
					{
						/*
						 * If it's a virtual column, check all other dimension columns and ensure that are at or above
						 * the level of this dimension table and in this dimension - otherwise, don't add
						 */
						Hierarchy proph = r.findDimensionHierarchy(dcn.dimensionName);
						if (proph == null || propDC.DimensionTable.Level == null)
							continue;
						Level propl = proph.findLevel(propDC.DimensionTable.Level);
						if (propl == null)
							continue;
						boolean valid = true;
						for (DimensionColumnNav dcn2 : entry.getValue())
						{
							if ((!dcn2.navigateonly || isSessionVariable) && !dcn2.constant)
							{
								if (!dcn2.dimensionName.equals(dcn.dimensionName))
								{
									valid = false;
									break;
								} else
								{
									Level l2 = proph.findLevelColumn(dcn2.columnName);
									if (l2 == null)
									{
										valid = false;
										break;
									}
									/*
									 * Only valid if all columns are in the same dimension and at the same or higher
									 * level than this one
									 */
									if (l2.findLevel(propl) == null)
									{
										valid = false;
										break;
									}
								}
							}
						}
						if (!valid)
						{
							logger.warn("Virtual dimension column \"" + propDC.ColumnName + "\" is not included in the aggregate \"" + this.name
									+ "\"because there are other dimension columns from another dimension or at lower level");
							continue;
						}
					}
					DimensionColumn dc = (DimensionColumn) propDC.clone();
					dc.DisplayMap = null;
					if (hasDynamicVariablesInPhysicalName(dc))
					{
						String physicalName = Util.replaceStr(dc.PhysicalName, dc.DimensionTable.TableSource.Tables[0].PhysicalName, name);
						dc.PhysicalName = dc.Qualify ? connection.getHashedIdentifier(physicalName) : physicalName;
					} else
					{
						dc.PhysicalName = name + "." + (dc.Qualify ? connection.getHashedIdentifier(dcn.displayName) : dcn.displayName);
					}
					dc.TableName = dt.TableName;
					dc.DimensionTable = dt;
					/* Clear any virtual column definitions as they are now instantiated */
					dc.VC = null;
					dimclist.add(dc);
					// Search for security filters
					for(StagingTable st: r.getStagingTables())
					{
						for(StagingColumn sc: st.Columns)
						{
							String dcColumnCompareName = dc.ColumnName;
							if ((propDC.DimensionTable.InheritTable != null) && (propDC.DimensionTable.InheritTable.length() > 0)  
								&& (propDC.DimensionTable.InheritPrefix != null) && (propDC.DimensionTable.InheritPrefix.length() > 0))
							{
								if(dcColumnCompareName.startsWith(propDC.DimensionTable.InheritPrefix))
								{
									dcColumnCompareName = dcColumnCompareName.substring(propDC.DimensionTable.InheritPrefix.length());
								}
							}
							
							if (sc.SecFilter != null && sc.Name.equals(dcColumnCompareName) && sc.TargetTypes != null)
							{
								boolean isOK = sc.TargetTypes.contains(dcn.dimensionName);
								if (!isOK && propDC.DimensionTable.InheritTable != null && propDC.DimensionTable.InheritTable.length() > 0)
								{
									DimensionTable idt = r.findDimensionTable(propDC.DimensionTable.InheritTable);
									if (sc.TargetTypes.contains(idt.DimensionName))
										isOK = true;
								}
								if (!isOK && sc.AnalyzeMeasure)
								{
									MeasureTable measureTableAtGrain = st.getMeasureTableAtGrain();
									DimensionTable dimDegenTable = propDC.DimensionTable;
									if (measureTableAtGrain!=null && dimDegenTable!=null)
									{
										if (measureTableAtGrain.PhysicalName!=null && measureTableAtGrain.PhysicalName.equals(dimDegenTable.PhysicalName))
										{
											isOK = true;
										}
									}
								}
								if (isOK)
								{
									TableSourceFilter tsf = new TableSourceFilter();
									tsf.SecurityFilter = true;
									if (sc.SecFilter.Type == SecurityFilter.TYPE_VARIABLE)
									{
										tsf.Filter = dc.PhysicalName + " IN (V{" + sc.SecFilter.SessionVariable + "})";
									} else
									{
										// set based filter. Put logical query directly
										tsf.Filter = dc.PhysicalName + " IN (Q{" + sc.SecFilter.SessionVariable + "})";
									}
									boolean found = false;
									for (TableSourceFilter stsf : flist)
									{
										if (stsf.Filter.equals(tsf.Filter) && stsf.SecurityFilter && tsf.SecurityFilter)
										{
											found = true;
											break;
										}
									}
									if (!found)
										flist.add(tsf);
									found = false;
									for (TableSourceFilter stsf : mtflist)
									{
										if (stsf.Filter.equals(tsf.Filter) && stsf.SecurityFilter && tsf.SecurityFilter)
										{
											found = true;
											break;
										}
									}
									tsf.FilterGroups = sc.SecFilter.FilterGroups;
									if (!found)
										mtflist.add(tsf);
								}
							}
						}
					}
				}
			}
			dt.TableSource.Filters = new TableSourceFilter[flist.size()];
			flist.toArray(dt.TableSource.Filters);			
			dt.DimensionColumns = new DimensionColumn[dimclist.size()];
			dimclist.toArray(dt.DimensionColumns);
			dtables.add(dt);
			/*
			 * Add implicit joins for dimension tables. Go through all measure tables in the repository. No need in
			 * snowflake because routine to add snowflake does this automatically
			 */
			if (l != null && measureList.size() > 0)
			{
				// First, get a list of component dimension tables
				Set<DimensionTable> dimTables = new HashSet<DimensionTable>();
				for (DimensionColumnNav dcn : this.q.dimensionColumns)
				{
					if (!dcn.constant)
					{
						DimensionColumn propDC = r.findDimensionColumn(dcn.dimensionName, dcn.columnName);
						if (propDC == null)
							throw (new NavigationException("Unable to create aggregate metadata. Column " + dcn.dimensionName + "." + dcn.columnName
									+ " not found"));
						dimTables.add(propDC.DimensionTable);
					}
				}
				for (MeasureTable repmt : r.MeasureTables)
				{
					/* Make sure the table joins to all dimension tables in this aggregate */
					boolean isJoined = true;
					for (DimensionTable repdt : dimTables)
					{
						if (!r.joinedTables(repmt, repdt))
						{
							isJoined = false;
							break;
						}
					}
					if (isJoined)
					{
						Join j = new Join();
						j.Table1 = repmt;
						j.Table1Str = repmt.TableName;
						j.Table2 = dt;
						j.Table2Str = dt.TableName;
						j.Redundant = false;
						j.Type = JoinType.Inner;
						j.JoinLevel = getJoinLevel(dt);
						if (j.JoinLevel != null)
						{
							j.LevelStr = j.JoinLevel.Name;
						} else
						{
							j.LevelStr = null;
						}
						/*
						 * Join by all columns in whatever key matches
						 */
						for (LevelKey key : l.Keys)
						{
							int numfound = 0;
							for (String levelKey : key.ColumnNames)
							{
								/*
								 * Find the physical column in the table associated with the level keys
								 */
								String key1 = null;
								for (MeasureColumn mc : repmt.MeasureColumns)
								{
									if (mc.ColumnName.equals(levelKey))
									{
										key1 = mc.PhysicalName;
										break;
									}
								}
								/* Only join if level key exists in dimension table */
								if (key1 != null)
								{
									String joinstr = "\"" + dt.TableName + "\"." + keyColumnName(dt.DimensionName, levelKey) + "=\"" + repmt.TableName + "\"."
											+ key1;
									if (j.JoinCondition == null)
										j.JoinCondition = joinstr;
									else
										j.JoinCondition += " AND " + joinstr;
									numfound++;
								}
							}
							if (numfound == key.ColumnNames.length)
							{
								joins.add(j);
								break;
							} else
								j.JoinCondition = null;
						}
					}
				}
			}
		}
		return mtflist;
	}

	/**
	 * return the physical query for the aggregate
	 */
	public String getPhysicalQuery()
	{
		return q.getPrettyQuery();
	}

	/**
	 * Physically persist this aggregate to the database
	 * 
	 * @throws SQLException
	 */
	public boolean persist(LoadGroup[] loadGroups, boolean dropIfExists, boolean allowIncremental, String incrementalFilter) throws SQLException
	{
		com.successmetricsinc.ExecuteSteps.Step step = new com.successmetricsinc.ExecuteSteps.Step(com.successmetricsinc.ExecuteSteps.StepCommand.PersistAggregate, com.successmetricsinc.ExecuteSteps.StepCommandType.ETL, null);
		status.logStatus(step, "Persist", Status.StatusCode.Running);

		String physicalQuery = physicalQueryWithDQTs == null ? q.getQuery() : physicalQueryWithDQTs;
		DatabaseConnection connectionToUse = inMemory ? memConnection : connection;
		if (allowIncremental && Database.sameTable(r, connectionToUse, name, connection, physicalQuery))
		{
			try
			{
				return updateAggregate(loadGroups, false, incrementalFilter, false);
			} 
			catch (Exception e)
			{
				logger.warn("Failed updating aggregate [" + name + "] - " + e.getMessage());
			}
			return false;
		}

		Database.dropTable(r, connectionToUse, name, true);

		q.persistDerivedQueryTables(connectionToUse);
		/* Create table */
		boolean result = false;
		if (r.getTimeDefinition().PartitionFacts)
		{
			// Don't allow in-memory for partitioned tables
			String colName = Util.buildDimensionColumn(connection, r.getTimeDefinition().Name, r.getTimeDefinition().getPartitionIDColumnName());
			result = Database.createTableAs(r, connection, name, q.getQuery(), true, r.getTimeDefinition().getPartitionSchemeName(r), colName, false, false);
		} else
		{
			logger.info("Creating Aggregate [" + name + "]: " + q.getQuery());
			result = Database.createTableAs(r, connection, name, q.getQuery(), true, null, null, false, inMemory, dataDir, databasePath, false);
		}
		if (!retainDQTs)
		{
			q.dropDerivedQueryTables(connection);
		}
		if (result && !inMemory)
			createIndices(connection);
		status.logStatus(step, "Persist", Status.StatusCode.Complete);
		return true;
	}


	private void createIndices(DatabaseConnection conn) throws SQLException
	{
		/* Create necessary indexes */
		for (DimensionTable dt : dtables)
		{
			if (dt.PhysicalName == null)
			{
				continue;
			}
			for (Map.Entry<String, Level> entry : levels.entrySet())
			{
				Level l = entry.getValue();
				if (l != null)
				{
					if (!entry.getKey().equals(dt.DimensionName))
						continue;
					if (l.Keys != null && l.Keys.length > 0)
					{
						for (LevelKey lk : l.Keys)
						{
							if (lk.ColumnNames != null && lk.ColumnNames.length > 0)
							{
								// make sure that the level keys are in the dimension columns
								boolean foundAll = true;
								for (String key : lk.ColumnNames)
								{
									boolean found = false;
									for (DimensionColumn dc : dt.DimensionColumns)
									{
										if (key.equals(dc.ColumnName))
										{
											found = true;
											break;
										}
									}
									if (!found)
									{
										foundAll = false;
										break;
									}
								}
								if (foundAll)
									Database.createIndex(conn, lk, dt.PhysicalName, false, true, null, null, true);
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Drop the physical aggregate table if it exists
	 * 
	 * @throws SQLException
	 * 
	 */
	public void drop() throws SQLException
	{
		Database.dropTable(r, connection, name, true);
	}

	/**
	 * Remove any metadata associated with this aggregate from the repository and drop the table from the database
	 * 
	 * @throws SQLException
	 */
	public void removeAggregate(boolean persist) throws SQLException
	{
		logger.debug("Dropping query aggregate " + name);
		List<MeasureTable> measureTablesToBeRemoved = null;
		if (mt != null)
		{
			measureTablesToBeRemoved = new ArrayList<MeasureTable>();
			measureTablesToBeRemoved.add(mt);
		}
		/**
		 * Iterate over all measure tables in the repository to see if there are any measure tables generated that
		 * inherit the current aggregate's measure table. If so, add them to the measure table list that needs to be
		 * removed from metadata.
		 */
		for (MeasureTable rmt : r.MeasureTables)
		{
			if ((rmt.QueryAgg != null) && (this.name.equals(rmt.QueryAgg.name)) && (!measureTablesToBeRemoved.contains(rmt)))
			{
				measureTablesToBeRemoved.add(rmt);
			}
		}
		if (measureTablesToBeRemoved != null)
		{
			r.removeJoins(measureTablesToBeRemoved);
		}
		r.removeDimensionTableJoins(dtables);
		r.removeDimensionTables(dtables);
		if (mt != null)
		{
			r.removeMeasureTables(measureTablesToBeRemoved);
		}
		if (persist)
		{
			drop();
		}
	}

	/**
	 * Incrementally update the query aggregate
	 * 
	 * remove the aggregate meta data select new aggregate data into a temp table delete old data from the aggregate
	 * insert new aggregate data into the aggregate drop the temp table put back the aggregate meta data
	 * 
	 * @throws SQLException
	 * @throws NavigationException
	 * @throws SyntaxErrorException 
	 * @throws RepositoryException 
	 * @throws ScriptException 
	 * @throws ArrayIndexOutOfBoundsException 
	 * @throws IOException 
	 * @throws ArrayIndexOutOfBoundsException 
	 */
	public boolean updateAggregate(LoadGroup[] loadGroups, boolean expandDerivedQueryTables, String incrementalFilter, boolean checkForSameTable) throws CloneNotSupportedException, SQLException, BaseException, ArrayIndexOutOfBoundsException, IOException
	{
		com.successmetricsinc.ExecuteSteps.Step step = new com.successmetricsinc.ExecuteSteps.Step(com.successmetricsinc.ExecuteSteps.StepCommand.PersistAggregate, com.successmetricsinc.ExecuteSteps.StepCommandType.ETL, null);
		status.logStatus(step, "Update", Status.StatusCode.Running);

		String physicalQuery = physicalQueryWithDQTs == null ? q.getQuery() : physicalQueryWithDQTs;
		DatabaseConnection connectionToUse = inMemory ? memConnection : connection;
		if (checkForSameTable && !Database.sameTable(r, connectionToUse, name, connection, physicalQuery))
		{
			try
			{
				Database.dropTable(r, connectionToUse, name, true);
				persist(null, true, false, null);
			} catch (SQLException e)
			{
				status.logStatus(step, "Update", Status.StatusCode.Failed);
				logger.error(e.getMessage(), e);
				return false;
			}
		} else
		{
			/*
			 * Create delete query to delete any existing rows that satisfy the load increment filters (i.e. for the
			 * current increment values)
			 */
			boolean addedFilter = false;
			Query dq = new Query(r);
			try
			{
				Connection conn = connectionToUse.ConnectionPool.getConnection();
				for (DimensionColumnNav dcn : q.dimensionColumns)
					if (!dcn.navigateonly && !dcn.constant)
					{
						boolean found = false;
						for (DimensionTable dt: dtables)
						{
							for(DimensionColumn dc: dcn.matchingColumns)
							{
								if (dt == dc.DimensionTable)
								{
									found = true;
									break;
								}
							}
							if (found)
								break;
						}
						if (found)
						{
							dq.dimensionColumns.add((DimensionColumnNav) dcn.clone());
						}
					}
				for (MeasureColumnNav mcn : q.measureColumns)
				{
					if (!mcn.navigateonly && !mcn.hidden)
					{
						dq.measureColumns.add((MeasureColumnNav) mcn.clone());
						break;
					}
				}
				for (QueryFilter qf : q.filters)
				{
					if (!qf.containsSessionVariable())
					{
						dq.addFilter(qf);
					}
				}
				if ((loadGroups != null) && (loadGroups.length > 0))
				{
					for (LoadGroup lg : loadGroups)
					{
						if (lg != null)
						{
							LoadGroupCondition[] conditions = lg.getConditions();
							if ((conditions != null) && (conditions.length > 0))
							{
								for (LoadGroupCondition cond : conditions)
								{
									String value = cond.getValue();
									value = r.replaceVariables(null, value);
									DimensionColumn dc = r.findDimensionColumn(cond.getDimension(), cond.getColumn());
									if (dc != null)
									{
										if (dc.DataType.equals("Date") || dc.DataType.equals("DateTime"))
										{
											value = DateUtil.convertToDBFormat(value, null, 
													r.getServerParameters().getProcessingTimeZone(), 
													r.getServerParameters().getProcessingTimeZone(),
													true);
										}
										QueryFilter qf = new QueryFilter(cond.getDimension(), cond.getColumn(), cond.getOperator(), value);
										dq.addFilter(qf);
										addedFilter = true;
									}
								}
							}
						}
					}
				}
				QueryFilter incrementalQueryFilter = null;
				if (incrementalFilter != null && incrementalFilter.length() > 0)
				{
					incrementalQueryFilter = createFilter(incrementalFilter);
					if(incrementalQueryFilter != null)
					{
						dq.addFilter(incrementalQueryFilter);
						addedFilter = true;
					}
				}
				if (addedFilter) // incremental or load group filtered added
				{
					dq.isAggregate = true;
					dq.navigateQuery(true, true, null, false);
					dq.generateQuery(null, true, false);
					String query = dq.getQuery();
					int index = query.indexOf(" FROM ");
					if (index > 0)
					{
						index = query.indexOf(" ", index + 7);
					}
					if (index > 0)
					{
						logger.info("Deleting rows from existing aggregate " + name);
						String tail = query.substring(index);
						int aliasindex = tail.indexOf(' ', 1);
						if (aliasindex > 0)
						{
							logger.debug(Query.getPrettyQuery(query));
							logger.debug(tail.substring(1, aliasindex));
							logger.debug(Database.getQualifiedTableName(connectionToUse, name));
							logger.debug(tail);
							String physicalAggTableName = Database.getQualifiedTableName(connectionToUse, name);
							String deleteQuery = null;
							int indexOfWhere = tail.indexOf(" WHERE");
							String parAccelTail = tail.substring(indexOfWhere,tail.length());
							parAccelTail = Util.replaceStr(parAccelTail, tail.substring(1, aliasindex),Database.getQualifiedTableName(connectionToUse, name));
							deleteQuery = "DELETE " + ((DatabaseConnection.isDBTypeOracle(connectionToUse.DBType) || DatabaseConnection.isDBTypeParAccel(connectionToUse.DBType) || DatabaseConnection.isDBTypeSAPHana(connectionToUse.DBType)) ? "" : tail.substring(1, aliasindex)) + " FROM " + physicalAggTableName + ((DatabaseConnection.isDBTypeParAccel(connectionToUse.DBType)) ? parAccelTail : tail);
							logger.debug(Query.getPrettyQuery(deleteQuery));
							Statement stmt = conn.createStatement();
							int rows = stmt.executeUpdate(deleteQuery);
							stmt.close();
							logger.info("Deleted " + rows + " rows from " + name);
						} 
					}
				}
				else
				{
					String deleteQuery = "TRUNCATE TABLE " + Database.getQualifiedTableName(connectionToUse, name);
					logger.debug(Query.getPrettyQuery(deleteQuery));
					Statement stmt = conn.createStatement();
					stmt.executeUpdate(deleteQuery);
					stmt.close();
					logger.info("Truncated table " + name);
				}
				/*
				 * Now, insert new records into the table that satisfy the current load increment filters. Simply clone
				 * the query for this aggregate and add the filters
				 */
				Query iq = (Query) q.clone();
				iq.setExpandDerivedQueryTables(expandDerivedQueryTables);
				if (!expandDerivedQueryTables)
				{
					iq.setDerivedQueryTableSuffix("_" + Long.valueOf(System.currentTimeMillis()).toString());
				}
				if ((loadGroups != null) && (loadGroups.length > 0))
				{
					for (LoadGroup lg : loadGroups)
					{
						if (lg != null)
						{
							LoadGroupCondition[] conditions = lg.getConditions();
							if ((conditions != null) && (conditions.length > 0))
							{
								for (LoadGroupCondition cond : conditions)
								{
									String value = cond.getValue();
									value = r.replaceVariables(null, value);
									DimensionColumn dc = r.findDimensionColumn(cond.getDimension(), cond.getColumn());
									if (dc != null)
									{
										if (dc.DataType.equals("Date") || dc.DataType.equals("DateTime"))
										{
											value = DateUtil.convertToDBFormat(value, null, 
													r.getServerParameters().getProcessingTimeZone(), 
													r.getServerParameters().getProcessingTimeZone(),
													true);
										}
										QueryFilter qf = new QueryFilter(cond.getDimension(), cond.getColumn(), cond.getOperator(), value);
										iq.addFilter(qf);
									}
								}
							}
						}
					}
				}
				if (incrementalQueryFilter != null)
				{
					iq.addFilter(incrementalQueryFilter);
				}
				/*
				 * Remove the existing aggregate metadata so that the new query doesn't navigate to the existing
				 * aggregate
				 */
				removeAggregate(false);
				// Renavigate
				iq.navigateQuery(true, true, null, false);
				overrideRules(iq);
				iq.alreadyGenerated = false;
				iq.generateQuery(null, true, false);		
				
				
				iq.persistDerivedQueryTables(connectionToUse);
				StringBuilder insertQuery = new StringBuilder(iq.getQuery());
				StringBuilder columnNames = new StringBuilder();
				for (int i = 0; i < iq.getReturnProjectionListSize(); i++)
				{
					if (i != 0)
						columnNames.append(", ");
					columnNames.append(connectionToUse.getHashedIdentifier(iq.getReturnProjectionListColumn(i).name));
				}
				logger.info("Inserting new rows into aggregate " + name);
				if (inMemory)
				{
					Database.createTableAs(r, connection, name, insertQuery.toString(), false, null, null, false, true,
							dataDir, databasePath, true);
				} else
				{
					Statement stmt = conn.createStatement();
					insertQuery.insert(0, "INSERT INTO " + Database.getQualifiedTableName(connectionToUse, name) + " (" + columnNames.toString() + ") ");
					int rows = stmt.executeUpdate(insertQuery.toString());
					logger.info("Inserted " + rows + " rows into " + name);
					stmt.close();
				}
				logger.debug(Query.getPrettyQuery(insertQuery));
				if(!retainDQTs)
				{
					iq.dropDerivedQueryTables(connection);
				}
				// Recreate the metadata for this aggregate
				process();
			} catch (NavigationException e)
			{
				status.logStatus(step, "Update", Status.StatusCode.Failed);
				logger.error(e.getMessage(), e);
				throw e;
			} catch (SQLException e)
			{
				status.logStatus(step, "Update", Status.StatusCode.Failed);
				logger.error(e.getMessage(), e);
				throw e;
			}
		}
		status.logStatus(step, "Update", Status.StatusCode.Complete);
		return true;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	public Level getJoinLevel(DimensionTable dt)
	{
		Level retLevel = null;
		Set<Level> queryJoinLevels = this.q.getJoinLevels();
		for (Level l : queryJoinLevels)
		{
			if (l.getHierarchy().DimensionName.equals(dt.DimensionName))
			{
				retLevel = l;
				break;
			}
		}
		return retLevel;
	}

	/**
	 * Returns whether a given navigation is at the same grain as this aggregate. If so, then no grouping/aggregation is
	 * needed - each dimension column in navigation must be present in the aggregate
	 * 
	 * @param nav
	 * @return
	 */
	public boolean isAtGrain(Navigation nav)
	{
		for (DimensionColumnNav dcn : q.dimensionColumns)
		{
			boolean found = false;
			for (DimensionColumnNav dcn2 : nav.getDimColumns())
			{
				if (dcn.dimensionName.equals(dcn2.dimensionName) && dcn.columnName.equals(dcn2.columnName))
				{
					found = true;
					break;
				}
			}
			if (!found && !dcn.constant)
				return (false);
		}
		return (true);
	}

	/*
	 * Returns whether this aggregate is at the grain of the given levels
	 */
	public boolean isAtGrain(Map<String, Level> levelMap)
	{
		if (levelMap.size() != levels.size())
		{
			return false;
		}
		for (String key : levelMap.keySet())
		{
			Level l = levelMap.get(key);
			Level lagg = levels.get(key);
			if (l != lagg)
			{
				return false;
			}
		}
		return true;
	}

	public boolean usesLogicalTable(String logicalTableName)
	{
		return this.q.usesLogicalTable(logicalTableName);
	}

	/**
	 * @return the query for this query aggregate
	 */
	public Query getQuery()
	{
		return q;
	}

	public List<DimensionTable> getDimensionTables()
	{
		return dtables;
	}

	public MeasureTable getMeasureTable()
	{
		return mt;
	}

	public void setDimensionTables(List<DimensionTable> dtables)
	{
		this.dtables = dtables;
	}

	public void setMeasureTable(MeasureTable mt)
	{
		this.mt = mt;
	}

	public boolean isRetainDQTs()
	{
		return retainDQTs;
	}

	public void setRetainDQTs(boolean retainDQTs)
	{
		this.retainDQTs = retainDQTs;
	}
	
	public static void replaceNullLevels(Map<String, Level> levels, List<DimensionColumnNav> dimColumns, Repository r)
	{
		/*
		 * Replace any null levels with the highest level that satisfies all columns in the dimension
		 */
		for (Map.Entry<String, Level> entry : levels.entrySet())
		{
			if (entry.getValue() == null)
			{
				// For each column, find the highest level of a dimension table that contains it
				for (DimensionColumnNav dcn : dimColumns)
				{
					Level l = null;
					if (dcn.dimensionName.equals(entry.getKey()))
					{
						for (DimensionTable dt: r.DimensionTables)
						{
							if (!dt.DimensionName.equals(dcn.dimensionName))
								continue;
							boolean found = false;
							for (DimensionColumn dc: dt.DimensionColumns)
							{
								if (dc.ColumnName.equals(dcn.columnName))
								{
									found = true;
									break;
								}
							}
							if (found)
							{
								if (l == null)
									l = r.findDimensionLevel(dcn.dimensionName, dt.Level);
								else
								{
									Level dtl = r.findDimensionLevel(dcn.dimensionName, dt.Level);
									if (dtl != null && dtl != l && dtl.findLevel(l) != null)
										l = dtl;
								}
							}
						}
						if (l == null)
							continue;
						if (entry.getValue() == null)
							entry.setValue(l);
						else
						{
							if (entry.getValue().findLevel(l) != null)
								entry.setValue(l);
						}
					}
				}
			}
		}
		/*
		 * Replace any null levels with the dimension key
		 */
		for (Map.Entry<String, Level> entry : levels.entrySet())
		{
			if (entry.getValue() == null)
			{
				entry.setValue(r.findDimensionHierarchy(entry.getKey()).DimensionKeyLevel);
			}
		}		
	}

	public boolean containsCountDistinctMeasure()
	{
		return containsCountDistinctMeasure;
	}

	public Status getStatus()
	{
		return status;
	}

	public void setStatus(Status status)
	{
		this.status = status;
	}

	private QueryFilter createFilter(String filterStr) throws SyntaxErrorException, ScriptException
	{
		if (filterStr != null && (!filterStr.isEmpty()) && filterStr.toLowerCase().startsWith("alt:"))
		{
			filterStr = filterStr.substring(4);
		}
		QueryFilter qf = null;
		ANTLRStringStream stream = new ANTLRStringStream(filterStr);
		BirstScriptLexer lex = new BirstScriptLexer(stream);
		CommonTokenStream cts = new CommonTokenStream(lex);
		BirstScriptParser parser = new BirstScriptParser(cts);
		try
		{
			logicalExpression_return ret = parser.logicalExpression();
			if (lex.hasError())
				LogicalQueryString.processLexError(lex, filterStr);
			qf = LogicalQueryString.validateTreeFilter((Tree) ret.getTree(), r, null, false);
		} catch (RecognitionException e)
		{
			LogicalQueryString.processException(e, lex, filterStr); // throws on exception
		}
		return qf;
	}

	/**
	 * This method is introduced for keeping the dimension column names on aggregates the same from 5.1.x to 5.2.x. In 5.2.x, we 
	 * introduced the dummy session variable called "USER" in session and also started storing session variables mentioned in table
	 * source filters as part of dc.SessionVariables list. Due to this, there was a subtle change to the way dimension
	 * columns were names. In 5.1.x, they were named in dimension key column format like "DimName$ColName$" and we want to continue
	 * the same way in 5.2.x as well to make sure that aggregate persisting and querying do not break. So, this method looks at
	 * a given dimension column to see if the physical name has session variables and the new dummy session variable "USER" is ignored 
	 * while deciding if a given dimension column has dynamic session variables.
	 * @param dc Dimension column
	 * @return if the dimension column has session variables except for USER, return true else false.
	 */
	private boolean hasDynamicVariablesInPhysicalName(DimensionColumn dc)
	{
		if(!dc.PhysicalName.contains("V{"))
		{
			return false;
		}
		List<Variable> dcPhysicalSessionVariables = r.getSessionVariableList(dc.PhysicalName);
		boolean hasDynamicVariables = dcPhysicalSessionVariables != null && dcPhysicalSessionVariables.size() > 0;
		if (hasDynamicVariables)
		{
			boolean found = false;
			for(Variable v: dcPhysicalSessionVariables)
			{
				if (v.getType() == Variable.VariableType.Session && (v.getName() != null && !v.getName().equals("USER")))
				{
					found = true;
					break;
				}
			}
			if (!found)
				hasDynamicVariables = false;
		}		
		return hasDynamicVariables;
	}
	
	private boolean hasDynamicVariablesInPhysicalNameOrFilter(MeasureColumn mc)
	{
		if(!mc.PhysicalName.contains("V{") && (mc.PhysicalFilter == null || mc.PhysicalFilter.length() == 0 || (!mc.PhysicalFilter.contains("V{"))))
		{
			return false;
		}

		List<Variable> mcPhysicalSessionVariables = r.getSessionVariableList(mc.PhysicalName);
		if(mcPhysicalSessionVariables == null)
		{
			mcPhysicalSessionVariables = new ArrayList<Variable>();
		}
		if(mc.PhysicalFilter != null && mc.PhysicalFilter.length() > 0 && mc.PhysicalFilter.contains("V{"))
		{
			List<Variable> mcSourceFilterPhysicalSessionVariables = r.getSessionVariableList(mc.PhysicalFilter);
			mcPhysicalSessionVariables.addAll(mcSourceFilterPhysicalSessionVariables);
		}
		
		boolean hasDynamicVariables = mcPhysicalSessionVariables != null && mcPhysicalSessionVariables.size() > 0;
		if (hasDynamicVariables)
		{
			boolean found = false;
			for(Variable v: mcPhysicalSessionVariables)
			{
				if (v.getType() == Variable.VariableType.Session && (v.getName() != null && !v.getName().equals("USER")))
				{
					found = true;
					break;
				}
			}
			if (!found)
				hasDynamicVariables = false;
		}		
		return hasDynamicVariables;
	}

	public boolean isInMemory()
	{
		return inMemory;
	}

	public void setInMemory(boolean inMemory)
	{
		this.inMemory = inMemory;
		if(inMemory)
		{
			DatabaseConnection mc = null;
			for(DatabaseConnection c: r.getConnections())
			{
				if (c.Name.equals(Database.MEM_AGG_CONNECTION))
				{
					mc = c;
					break;
				}
			}
			if (mc == null)
				return;
			memConnection = mc;
			if (mt != null)
				mt.TableSource.connection = mc;
			for (DimensionTable dt: dtables)
				dt.TableSource.connection = mc;
		}
	}
	
}
