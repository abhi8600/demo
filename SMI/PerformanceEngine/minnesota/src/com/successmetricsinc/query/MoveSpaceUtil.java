package com.successmetricsinc.query;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.warehouse.DimensionLoader;
import com.successmetricsinc.warehouse.LoadWarehouse;
import com.successmetricsinc.warehouse.LoadWarehouseHelper;
import com.successmetricsinc.warehouse.SourceColumn;
import com.successmetricsinc.warehouse.SourceFile;
import com.successmetricsinc.warehouse.StagingColumn;
import com.successmetricsinc.warehouse.StagingTable;
import com.successmetricsinc.warehouse.StagingTableBulkLoadInfo;

public class MoveSpaceUtil {

	private static Logger logger = Logger.getLogger(MoveSpaceUtil.class);
	
	/**
	 * Update composite keys for Fact and Dimension tables. This is exclusively used for MoveSpace from SQL Server to IB. We export the SQL tables but when
	 * we do a generateSchema we also add CompoundKeys for IB. This method populates these columns.
	 * @throws SQLException 
	 */
	public static void updateCompositeKeys(Repository r,String physicalTableName) throws SQLException
	{
		
		DatabaseConnection dc = r.getDefaultConnection();
		Connection con = dc.ConnectionPool.getConnection();
		
		LoadWarehouse lw = new LoadWarehouse(r);
		if(dc.requiresTransactionID())
		{
			List<MeasureTable> generatedTables = LoadWarehouseHelper.getGeneratedMeasureTables(r);
			for (MeasureTable mt : generatedTables)
			{
				if(mt.PhysicalName.equals(physicalTableName) &&  !mt.Calculated && mt.InheritTable == null && mt.InheritPrefix == null)
				{
					
				
					Collection<MeasureTableGrain> grainSet = mt.getGrainInfo(r);
					for (StagingTable st : r.getStagingTables())
					{
	
						if (st.isAtMatchingGrain(grainSet))
						{
							
							SourceFile sf = r.findSourceFile(st.SourceFile);
							StagingTableBulkLoadInfo stbl = new StagingTableBulkLoadInfo(st, sf, dc, r);
							stbl.setupGrainKeys(sf);
							List<String> nlist = new ArrayList<String>();
							for (StagingColumn sc : st.Columns)
							{
								for (String s : stbl.grainKeyMap.get(mt).keySet())
								{
									String value = stbl.grainKeyMap.get(mt).get(s);
									if (sc.SourceFileColumn == null || sc.SourceFileColumn.length() == 0 
											|| (sc.Transformations != null && sc.Transformations.length > 0) 
											|| !sc.getQualifiedPhysicalFormula(r, "B", null).equals(value))
										continue;
									for (SourceColumn sfc : sf.Columns)
									{
										if (sc.SourceFileColumn.equals(sfc.Name))
										{
											nlist.add(s);
										}
									}
								}
							}
							
							StringBuffer sqlQueryBuffer = new StringBuffer("");
							for (String s : stbl.grainKeyNames)
							{
								String compositeKeyName = dc.getHashedIdentifier(s);
								List<String> colList =  getPhysicalColumnsForTable(physicalTableName,con,dc);
								boolean allColumnsExist = true;
								for(String sCol : nlist){
									if(!colList.contains((sCol)))
									{
										allColumnsExist = false;
									}
								}
								if(!colList.isEmpty() && colList.contains(compositeKeyName) && allColumnsExist)
								{
									System.out.println("Composite Column : " + compositeKeyName  + " for columns " + nlist.toString());
									sqlQueryBuffer.append("Update " + dc.Schema +"."+mt.PhysicalName + " SET " + compositeKeyName + " = ");
									sqlQueryBuffer.append("CONCAT_WS('"+ (char)254 +"'");
									for(String sCol : nlist){
										sqlQueryBuffer.append(","+sCol);
									}
									sqlQueryBuffer.append(")");
									PreparedStatement pstmt = con.prepareStatement(sqlQueryBuffer.toString());
									logger.debug("UpdateCompositeKeys Query :"+ Query.getPrettyQuery(sqlQueryBuffer.toString()));
									pstmt.executeUpdate();
								}
								else
								{
									logger.warn("Could not find composteKey " + compositeKeyName + " for table "+physicalTableName +" . Skipping Update Composite Keys");
								}
							}
						}
					}
				}
			}
			
			DimensionTable[] dimensionTables = r.DimensionTables;
			for(int i=0;i<dimensionTables.length;i++)
			{
				DimensionTable dt = dimensionTables[i];
				if(dt.PhysicalName!=null && dt.PhysicalName.equals(physicalTableName) && !dt.Calculated && dt.InheritTable == null && dt.InheritPrefix == null)
				{
					if (dt.TableSource.connection.requiresTransactionID())
					{
						
						Level l = r.findLevel(dt.DimensionName, dt.Level);
						if (l != null)
						{
							LevelKey lk = l.getNaturalKey();
							if (lk.ColumnNames.length > 1)
							{
								List<String> colList =  getPhysicalColumnsForTable(physicalTableName,con,dc);
								boolean allColumnsExist = true;
								for(String sCol : lk.ColumnNames){
									if(!colList.contains(Util.replaceWithNewPhysicalString(sCol,dc,r)))
									{
										allColumnsExist = false;
									}
								}
								String compositeKeyName = lk.getCompoundKeyName(r, dt.TableSource.connection);
								if(!colList.isEmpty() && colList.contains(compositeKeyName) && allColumnsExist)
								{
									StringBuffer sqlQueryBuffer = new StringBuffer("");
									System.out.println("Composite Column : " + compositeKeyName  + " for columns " + lk.ColumnNames.toString());
									sqlQueryBuffer.append("Update " + dc.Schema +"."+dt.PhysicalName + " SET " + compositeKeyName + " = ");
									sqlQueryBuffer.append("CONCAT_WS('"+ (char)254 +"'");
									for(String sCol : lk.ColumnNames){
										sqlQueryBuffer.append(","+Util.replaceWithNewPhysicalString(sCol,dc,r));
									}
									sqlQueryBuffer.append(")");
									PreparedStatement pstmt = con.prepareStatement(sqlQueryBuffer.toString());
									logger.debug("UpdateCompositeKeys For DimensionTable Query :"+ Query.getPrettyQuery(sqlQueryBuffer.toString()));
									pstmt.executeUpdate();
								}else{
									logger.warn("Could not find composteKey " + compositeKeyName + " for table "+physicalTableName +" . Skipping Update Composite Keys");
								}
								
							}
						}
					}
				}
			}
		}
		else{
			logger.warn("updateCompositeKeys can only be executed for Infobright.");
		}
	}
	
	private static List<String> getPhysicalColumnsForTable(String tableName,Connection con,DatabaseConnection dc){
		List<String> colList = new ArrayList<String>();
		try {
			List<Object[]> schemaObjectList = Database.getTableSchema(dc, con, null, dc.Schema, tableName);
			for (int colno = 0; colno < schemaObjectList.size(); colno++)
			{
				String physicalColName = (String) schemaObjectList.get(colno)[0];
				colList.add(physicalColName);
			}
		} catch (SQLException e) {
			logger.warn("Could not determine Physical Columns for table " + tableName+ " " + e);
		}
		return colList;
	}
}
