/**
 * $Id: QueryTimeoutException.java,v 1.2 2009-12-30 22:18:20 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.query;

import com.successmetricsinc.util.BaseException;

/**
 * Exception thrown when a query times out
 */
public class QueryTimeoutException extends BaseException
{
	private static final long serialVersionUID = 1L;

	/**
	 * @param s
	 */
	public QueryTimeoutException(String s, Throwable cause)
	{
		super(s, cause);
	}
	
	public QueryTimeoutException(String s)
	{
		super(s);
	}
	
	@Override
	public int getErrorCode() {
		return ERROR_QUERY_TIMEOUT;
	}
}
