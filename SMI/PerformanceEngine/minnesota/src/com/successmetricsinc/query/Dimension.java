package com.successmetricsinc.query;

import java.io.Serializable;

import org.jdom.Element;
import org.jdom.Namespace;

public class Dimension implements Serializable
{
	public String Name;
	
	public Dimension(Element e, Namespace ns)
	{
		this.Name = e.getChildText("Name",ns);
	}
	
	public Element getDimensionElement(Namespace ns)
	{
		Element e = new Element("Dimension", ns);
		Element child = new Element("Name", ns);
		child.setText(Name);
		e.addContent(child);
		return e;
	}
}
