package com.successmetricsinc.metadatavalidation;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.input.SAXBuilder;

import com.successmetricsinc.Repository;
import com.successmetricsinc.metadatavalidation.rules.BaseRule;
import com.successmetricsinc.metadatavalidation.util.SaveRepository;

/**
 * @author pbindal
 * This class is the entry point for the metadata validation engine. 
 */
public class MetadataValidator 
{
	private static Logger logger = Logger.getLogger(MetadataValidator.class);
	private Repository r;
	private String execFlag;
	private String ruleOrGrpName;
	private List<RuleExecutionResult> ruleExecutionResults = null;
	private boolean saveRepository = false;
	private boolean isUserRepoAdmin;
	
	public MetadataValidator(Repository r,String execFlag,String ruleOrGrpName, boolean isUserRepoAdmin)
	{
		this.r = r;
		this.execFlag = execFlag;
		this.ruleOrGrpName = ruleOrGrpName;
		this.isUserRepoAdmin = isUserRepoAdmin;
	}
		
	
	public MetadataValidator(boolean isUserRepoAdmin)
	{
		this.isUserRepoAdmin = isUserRepoAdmin;
	}
		
	public MetadataRuleSet getRulesForValidation()
	{
		MetadataRuleSet mdValidator=null;
		try 
		{
			SAXBuilder builder = new SAXBuilder();
			InputStream xmlInputStream = getClass().getResourceAsStream("/com/successmetricsinc/metadatavalidation/xml/MetadataValidation.xml");
			Document d = builder.build(xmlInputStream);

			if (d!=null)
			{
				mdValidator = new MetadataRuleSet(d, isUserRepoAdmin);
			}
		} 
		catch (Exception e) 
		{
			logger.error("Exception in getMetadataRules ",e);
		}
		return mdValidator;
	}
	
	public List<RuleExecutionResult> fix() throws Exception
	{   
		return processRules(true);
	} 
	
	public List<RuleExecutionResult> validate() throws Exception
	{		
		return processRules(false);
	}	
	
	private List<RuleExecutionResult> processRules(boolean isFix) throws Exception
	{
	  try
    {
    MetadataRuleSet mdValidator=getRulesForValidation();
      
    if (mdValidator!=null)
    {
        ruleExecutionResults = new ArrayList<RuleExecutionResult>();
        List<RuleGroup> ruleGroups = mdValidator.getRuleGroups();
        if (execFlag.equalsIgnoreCase("rule"))
        {
          /*
           * Look for specific rule to execute
           */
          Rule ruleToExecute=null;
          for (RuleGroup ruleGroup : ruleGroups)
          {
            for (Rule rule : ruleGroup.getRules())
            {
              if (rule.getName().equalsIgnoreCase(ruleOrGrpName))
              {
                ruleToExecute = rule;
                break;
              }             
            }
            if (ruleToExecute!=null)
            {
              break;
            }
          }
          if (ruleToExecute!=null)
          {
            if (isFix)
            {
              fixRule(ruleToExecute);
            }
            else
            {
              validateRule(ruleToExecute);
            }
          }
        }
        else if (execFlag.equalsIgnoreCase("rulegroup"))
        {
          for (RuleGroup ruleGroup : ruleGroups)
          {
            if (ruleGroup.getGroupName().equalsIgnoreCase(ruleOrGrpName))
            {
              for (Rule rule : ruleGroup.getRules())
              {
                if (isFix)
                {
                  fixRule(rule);
                }
                else
                {
                  validateRule(rule);
                }
              }
              break;
            }
          }
        }
        else if (execFlag.equals("all"))
        {
          /*
           * Execute each rule of each group
           */
          for (RuleGroup ruleGroup : ruleGroups)
          {
            for (Rule rule : ruleGroup.getRules())
            {
              if (isFix)
              {
                fixRule(rule);
              }
              else
              {
                validateRule(rule);
              }
            }
          }
        }
        if (saveRepository)
        {
          logger.info("Finished fixing all rules. Will now save repository with backup ");
          saveFixedRepository(r);
        }
      }         
    }
    catch(Exception e)
    {
      logger.error("Exception in MetadataValidator ",e);
      throw e;
    }
    return ruleExecutionResults;
	}
	
	private void validateRule(Rule rule)
	{
	  RuleExecutionResult ruleExecutionResult = null;
    BaseRule newRule = null;
    try
    {
      newRule = (BaseRule) Class.forName(rule.getRuleClass()).newInstance();
      newRule.setRuleName(rule.getRuleName());
      boolean result = newRule.validateRepository(r);
      ruleExecutionResult = new RuleExecutionResult(result,newRule.getValidationMsg(),rule.getDisplayname(), newRule.getInvalidList(), 
    		  rule.getCriticality(), rule.getGroupName());                      
      ruleExecutionResults.add(ruleExecutionResult);
    }
    catch (ClassCastException cce)
    {
      logger.error("Class "+rule.getRuleClass()+" does not extend BaseRule. Rule will not be executed.");
    }
    catch (ClassNotFoundException cnfe)
    {
      logger.error("Class "+rule.getRuleClass()+ " could not be found. Rule will not be executed.");
    }
    catch (Exception e)
    {
      logger.error("Exception while validating rule ",e);
    }
	}
	
	private void fixRule(Rule rule)
  {
    RuleExecutionResult ruleExecutionResult = null;
    BaseRule newRule = null;
    try
    {
      newRule = (BaseRule) Class.forName(rule.getRuleClass()).newInstance();
      newRule.setRuleName(rule.getRuleName());
      boolean result = newRule.fixRepository(r);
      if (!saveRepository && (result && newRule.isInvalid()))
      {
        saveRepository = true;
      }
      ruleExecutionResult = new RuleExecutionResult(newRule.getFixMsg(),rule.getName(),newRule.isInvalid(),result);                      
      ruleExecutionResults.add(ruleExecutionResult);
    }
    catch (ClassCastException cce)
    {
      logger.error("Class "+rule.getRuleClass()+" does not extend BaseRule. Rule will not be executed.");
    }
    catch (ClassNotFoundException cnfe)
    {
      logger.error("Class "+rule.getRuleClass()+ " could not be found. Rule will not be executed.");
    }
    catch (Exception e)
    {
      logger.error("Exception while fixing rule ",e);
    }
  }
	
	private boolean saveFixedRepository(Repository r)
  {
    boolean saved=true;   
    String repositoryFileName = r.getServerParameters().getApplicationPath() + File.separator + "repository_dev.xml";   
    SaveRepository sr = new SaveRepository(r);
    try
    {
      sr.save(repositoryFileName,true);
    }
    catch (Exception e)
    {
      saved=false;   
      logger.error("Could not save repository "+e);
    }
    return saved;
  }
}
