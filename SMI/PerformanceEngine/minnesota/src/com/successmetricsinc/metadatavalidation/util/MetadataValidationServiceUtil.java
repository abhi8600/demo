package com.successmetricsinc.metadatavalidation.util;

import java.util.List;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMAttribute;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.metadatavalidation.MetadataRuleSet;
import com.successmetricsinc.metadatavalidation.MetadataValidator;
import com.successmetricsinc.metadatavalidation.Rule;
import com.successmetricsinc.metadatavalidation.RuleExecutionResult;
import com.successmetricsinc.metadatavalidation.RuleGroup;
import com.successmetricsinc.util.XmlUtils;

public class MetadataValidationServiceUtil
{
	private static final String NODE_RULE_OR_GROUP_NAME = "RuleOrGroupName";
	private static final String NODE_RULE_SET = "RuleSet";
	private static final String NODE_ERROR = "Error";
	private static final String NODE_VALIDATION_ERRORS = "ValidationErrors";
	private static final Logger logger = Logger.getLogger(MetadataValidationServiceUtil.class);
	public static final String NODE_ROOT = "Root";
	public static final String NODE_SUCCESS = "Success";
	public static final String NODE_REASON = "Reason";
	public static final String NODE_RESULT = "Result";
	public static final String NODE_RULE_VALIDATION_RESULT = "RuleValidationResult";
	public static final String NODE_RULE_FIX_RESULT = "RuleFixResult";
	public static final String NODE_RULE_FIXED = "RuleFixed";
	public static final String NODE_RULE_IS_INVALID = "RuleRequiredFix";
	public static final String NODE_RULE_NAME = "RuleName";
	public static final String NODE_RULE_RESULT = "OutCome";
	public static final String NODE_RULE_AREA = "Area";
	public static final String NODE_VALIDATION_MSG = "ValidationMessage";
	public static final String NODE_FIX_MESSAGE = "FixMessage";
	
	public static final String NODE_RULE_GROUP = "RuleGroup";
	public static final String NODE_DESCRIPTION = "Description";
	public static final String NODE_RULE = "Rule";
	public static final String NODE_RULE_CLASS = "RuleClass";
	public static final String NODE_RULE_CRITICALITY = "Criticality";
	public static final String NODE_RULE_ENABLED = "Enabled";
	public static final String NODE_RULE_IS_USER_REPO_ADMIN = "isRepoAdmin";
	
	public static final String ATTRIBUTE_NAME = "name";
	public static final String ATTRIBUTE_DISPLAYNAME = "displayname";	
	
	public OMElement getMetadataValidationRules(boolean isUserRepoAdmin)
	{
		OMNamespace ns = null;
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement root = factory.createOMElement(NODE_ROOT, ns);
		boolean success = false;
		String errorMessage = null;
		try
		{
			MetadataValidator mdValidator = new MetadataValidator(isUserRepoAdmin);
			
			MetadataRuleSet mdRuleSet = mdValidator.getRulesForValidation();
			
			if (mdRuleSet!=null)
			{
				OMElement resultChild = factory.createOMElement(NODE_RESULT, ns);
				List<RuleGroup> ruleGroups = mdRuleSet.getRuleGroups();
				for (RuleGroup ruleGroup : ruleGroups)
				{
					OMElement elemRuleGrp = factory.createOMElement(NODE_RULE_GROUP, ns);
					elemRuleGrp.addAttribute(factory.createOMAttribute(ATTRIBUTE_NAME, ns, ruleGroup.getGroupName()));
					elemRuleGrp.addAttribute(factory.createOMAttribute(ATTRIBUTE_DISPLAYNAME, ns, ruleGroup.getDisplayname()));
					OMElement elemRuleGrpDesc = factory.createOMElement(NODE_DESCRIPTION, ns);
					elemRuleGrpDesc.setText(ruleGroup.getDescription());
					elemRuleGrp.addChild(elemRuleGrpDesc);
					List<Rule> rules = ruleGroup.getRules();
					for (Rule rule : rules)
					{
						OMElement elemRule = factory.createOMElement(NODE_RULE, ns);
						elemRule.addAttribute(factory.createOMAttribute(ATTRIBUTE_NAME, ns, rule.getName()));
						elemRule.addAttribute(factory.createOMAttribute(ATTRIBUTE_DISPLAYNAME, ns, rule.getDisplayname()));
						OMElement elemRuleDesc = factory.createOMElement(NODE_DESCRIPTION, ns);
						elemRuleDesc.setText(rule.getDescription());
						elemRule.addChild(elemRuleDesc);
						OMElement elemRuleClass = factory.createOMElement(NODE_RULE_CLASS, ns);
						elemRuleClass.setText(rule.getRuleClass());
						elemRule.addChild(elemRuleClass);
						OMElement elemRuleCriticality = factory.createOMElement(NODE_RULE_CRITICALITY, ns);
						elemRuleCriticality.setText(rule.getCriticality());
						elemRule.addChild(elemRuleCriticality);
						OMElement elemRuleEnabled = factory.createOMElement(NODE_RULE_ENABLED, ns);
						elemRuleEnabled.setText(String.valueOf(rule.isEnabled()));
						elemRule.addChild(elemRuleEnabled);
						OMElement elemIsUserRepoAdmin = factory.createOMElement(NODE_RULE_IS_USER_REPO_ADMIN, ns);
						elemIsUserRepoAdmin.setText(String.valueOf(rule.isRepoAdmin()));
						elemRule.addChild(elemIsUserRepoAdmin);
						elemRuleGrp.addChild(elemRule);
					}
					resultChild.addChild(elemRuleGrp);
				}
				root.addChild(resultChild);				
                success = true;
			}
		}
		catch (Exception e)
		{
			errorMessage = e.getMessage();
			logger.error(e.toString(), e);
		}
		OMElement successChild = factory.createOMElement(NODE_SUCCESS, ns);
		successChild.setText(String.valueOf(success));
		root.addChild(successChild);
		if (!success && errorMessage != null)
		{
			OMElement errMsgChild = factory.createOMElement(NODE_REASON, ns);
			errMsgChild.setText(errorMessage);
			root.addChild(errMsgChild);
		}		
		return root;
	}

	public OMElement validateRepository(Repository r,String ruleSet,String ruleOrGrpName, boolean isUserRepoAdmin)
	{
		OMNamespace ns = null;
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement root = factory.createOMElement(NODE_ROOT, ns);
		boolean success = false;
		String errorMessage = null;
		
		try
		{
		  MetadataValidator mdValidator = new MetadataValidator(r, ruleSet, ruleOrGrpName, isUserRepoAdmin);
			
			List<RuleExecutionResult> ruleExecutionResults = mdValidator.validate();
			
			if (ruleExecutionResults!=null)
			{
			  OMElement resultChild = factory.createOMElement(NODE_RESULT, ns);
			  resultChild.addAttribute(MetadataValidationServiceUtil.NODE_RULE_SET, ruleSet, ns);
			  if (ruleOrGrpName != null)
				  resultChild.addAttribute(MetadataValidationServiceUtil.NODE_RULE_OR_GROUP_NAME, ruleOrGrpName, ns);
			  for (RuleExecutionResult ruleExecutionResult: ruleExecutionResults)
			  {
			    OMElement ruleExecution = factory.createOMElement(NODE_RULE_VALIDATION_RESULT, ns);
			    OMElement nodeRuleName = factory.createOMElement(NODE_RULE_NAME, ns);
			    nodeRuleName.setText(ruleExecutionResult.getRuleName());
			    OMElement nodeRuleResult = factory.createOMElement(NODE_RULE_RESULT, ns);
			    if(ruleExecutionResult.isExecutionResult()){
			    	nodeRuleResult.setText(ResourceBundleUtil.getValidationLabel("RULE_INFO"));	
			    }else{
			    	nodeRuleResult.setText(ruleExecutionResult.getCriticality());
			    }
			    OMElement nodeRuleArea = factory.createOMElement(NODE_RULE_AREA, ns);
			    nodeRuleArea.setText(ruleExecutionResult.getRuleGroupName());
			    OMElement nodeValidationMsg = factory.createOMElement(NODE_VALIDATION_MSG, ns);
			    nodeValidationMsg.setText(ruleExecutionResult.getValidationMessage());
			    
			    ruleExecution.addChild(nodeRuleName);
			    ruleExecution.addChild(nodeRuleResult);
			    ruleExecution.addChild(nodeRuleArea);
			    ruleExecution.addChild(nodeValidationMsg);

			    List<String> errorList = ruleExecutionResult.getErrorList();
			    if (errorList != null && errorList.size() > 0) {
			    	OMElement nodeErrorList = factory.createOMElement(MetadataValidationServiceUtil.NODE_VALIDATION_ERRORS, ns);
			    	for (String s : errorList) {
			    		XmlUtils.addContent(factory, nodeErrorList, MetadataValidationServiceUtil.NODE_ERROR, s, ns);
			    	}
			    	ruleExecution.addChild(nodeErrorList);
			    }

			    resultChild.addChild(ruleExecution);
			  }
			  root.addChild(resultChild);				
			  success = true;
			}
		}
		catch (Exception e)
		{
			errorMessage = e.getMessage();
			logger.error(e.toString(), e);
		}
		OMElement successChild = factory.createOMElement(NODE_SUCCESS, ns);
		successChild.setText(String.valueOf(success));
		root.addChild(successChild);
		if (!success && errorMessage != null)
		{
			OMElement errMsgChild = factory.createOMElement(NODE_REASON, ns);
			errMsgChild.setText(errorMessage);
			root.addChild(errMsgChild);
		}		
		return root;
	}
	
	public OMElement fixRepository(Repository r,String ruleSet,String ruleOrGrpName)
  {
    OMNamespace ns = null;
    OMFactory factory = OMAbstractFactory.getOMFactory();
    OMElement root = factory.createOMElement(NODE_ROOT, ns);
    boolean success = false;
    String errorMessage = null;
    
    try
    {
      MetadataValidator mdValidator = new MetadataValidator(r, ruleSet,ruleOrGrpName,true);
      
      List<RuleExecutionResult> ruleExecutionResults = mdValidator.fix();
      
      if (ruleExecutionResults!=null)
      {
        OMElement resultChild = factory.createOMElement(NODE_RESULT, ns);
        for (RuleExecutionResult ruleExecutionResult: ruleExecutionResults)
        {
          OMElement ruleExecution = factory.createOMElement(NODE_RULE_FIX_RESULT, ns);
          OMElement nodeRuleName = factory.createOMElement(NODE_RULE_NAME, ns);
          nodeRuleName.setText(ruleExecutionResult.getRuleName());
          OMElement nodeRuleFixed = factory.createOMElement(NODE_RULE_FIXED, ns);
          nodeRuleFixed.setText(String.valueOf(ruleExecutionResult.isFixResult()));
          OMElement nodeRuleRequiredFix = factory.createOMElement(NODE_RULE_IS_INVALID, ns);
          nodeRuleRequiredFix.setText(String.valueOf(ruleExecutionResult.isInvalid()));
          OMElement nodeFixMessage = factory.createOMElement(NODE_FIX_MESSAGE, ns);
          nodeFixMessage.setText(ruleExecutionResult.getFixMessage());

          ruleExecution.addChild(nodeRuleName);
          ruleExecution.addChild(nodeRuleFixed);
          ruleExecution.addChild(nodeRuleRequiredFix);
          ruleExecution.addChild(nodeFixMessage);

          resultChild.addChild(ruleExecution);
        }
        root.addChild(resultChild);       
        success = true;
      }
    }
    catch (Exception e)
    {
      errorMessage = e.getMessage();
      logger.error(e.toString(), e);
    }
    OMElement successChild = factory.createOMElement(NODE_SUCCESS, ns);
    successChild.setText(String.valueOf(success));
    root.addChild(successChild);
    if (!success && errorMessage != null)
    {
      OMElement errMsgChild = factory.createOMElement(NODE_REASON, ns);
      errMsgChild.setText(errorMessage);
      root.addChild(errMsgChild);
    }   
    return root;
  }
	
	public OMElement getGeneralErrorMessage(String errorMessage)
	{
		OMNamespace ns = null;
	    OMFactory factory = OMAbstractFactory.getOMFactory();
	    OMElement root = factory.createOMElement(NODE_ROOT, ns);
	    OMElement successChild = factory.createOMElement(NODE_SUCCESS, ns);
		successChild.setText(String.valueOf(false));
		root.addChild(successChild);
		if (errorMessage != null && errorMessage.trim().length() > 0)
		{
			OMElement errMsgChild = factory.createOMElement(NODE_REASON, ns);
			errMsgChild.setText(errorMessage);
			root.addChild(errMsgChild);
		}   
	    return root;
	}
}
