package com.successmetricsinc.metadatavalidation.util;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.successmetricsinc.UserBean;
import com.successmetricsinc.metadatavalidation.MetadataValidator;

public class ResourceBundleUtil 
{
	private static Logger logger = Logger.getLogger(MetadataValidator.class);
	private static ResourceBundle validationMessages;
	private static ResourceBundle validationLabels;
	
	static 
	{
		Locale locale = Locale.getDefault();
		
		UserBean userBean = UserBean.getUserBean();
		
		if (userBean!=null && userBean.getLocale()!=null)
		{
			locale = UserBean.getUserBean().getLocale();
		}
		
		validationMessages = ResourceBundle.getBundle("com.successmetricsinc.metadatavalidation.i18n.MetadataMessages",locale);
		validationLabels = ResourceBundle.getBundle("com.successmetricsinc.metadatavalidation.i18n.MetadataLabels",locale);
	}	
	
	private ResourceBundleUtil()
	{
	}
	
	/**
	 * Substitute parameters in resourcebundle string with params passed and return the string
	 * @param key
	 * @param params
	 * @return
	 */
	public static String getValidationMessageFromBundle(String key, Object... params  )
	{
		String messageValue = null;
        try
        {
        	if (params!=null)
        	{
        		messageValue = MessageFormat.format(validationMessages.getString(key), params);        		
        	}
        	else
        	{
        		messageValue = validationMessages.getString(key);
        	}
        }
        catch (MissingResourceException e)
        {
            logger.error("Validation message for key "+key+ " not found ");
        }
        return messageValue;
    }
	
	public static String getValidationLabel(String key)
	{
		String labelValue = null;
        try
        {
        	labelValue = validationLabels.getString(key);
        }
        catch (MissingResourceException e)
        {
            logger.error("Validation label for key "+key+ " not found ");
        }
        return labelValue;
    }
}

