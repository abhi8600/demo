package com.successmetricsinc.metadatavalidation.util;

import java.io.File;
import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.List;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.output.Format;
import org.jdom.output.Format.TextMode;
import org.jdom.output.XMLOutputter;

import com.successmetricsinc.Broadcast;
import com.successmetricsinc.CustomDrill;
import com.successmetricsinc.Group;
import com.successmetricsinc.LogicalExpression;
import com.successmetricsinc.PackageImport;
import com.successmetricsinc.Repository;
import com.successmetricsinc.Repository.ACLItem;
import com.successmetricsinc.Repository.Resource;
import com.successmetricsinc.SubjectArea;
import com.successmetricsinc.User;
import com.successmetricsinc.Variable;
import com.successmetricsinc.engine.Outcome;
import com.successmetricsinc.engine.PatternSearchDefinition;
import com.successmetricsinc.engine.PerformanceModel;
import com.successmetricsinc.engine.ReferencePopulation;
import com.successmetricsinc.engine.SuccessModel;
import com.successmetricsinc.engine.operation.Operation;
import com.successmetricsinc.query.Aggregate;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.Dimension;
import com.successmetricsinc.query.DimensionTable;
import com.successmetricsinc.query.Hierarchy;
import com.successmetricsinc.query.HierarchyLevel;
import com.successmetricsinc.query.Join;
import com.successmetricsinc.query.MeasureGrain;
import com.successmetricsinc.query.MeasureTable;
import com.successmetricsinc.query.QueryFilter;
import com.successmetricsinc.query.VirtualColumn;
import com.successmetricsinc.util.QuickDashboard;
import com.successmetricsinc.util.XmlUtils;
import com.successmetricsinc.warehouse.LoadGroup;
import com.successmetricsinc.warehouse.ScriptGroup;
import com.successmetricsinc.warehouse.SourceFile;
import com.successmetricsinc.warehouse.StagingFilter;
import com.successmetricsinc.warehouse.StagingTable;
import com.successmetricsinc.warehouse.partition.WarehousePartition;

public class SaveRepository 
{
	private Repository r;
	private static Namespace ns;
	private static Logger logger = Logger.getLogger(SaveRepository.class);
	
	public SaveRepository(Repository r) 
	{
		this.r = r;
	}
	
	public void save(String saveFileName) throws Exception
	{
	  save(saveFileName,false);
	}
	
	public void save(String saveFileName,boolean backupBeforeSave) throws Exception
	{
		try
		{						
		  if (backupBeforeSave)
		  {
		    backupFile(saveFileName, "bkup", 10);
		  }
			ns = Namespace.getNamespace("http://www.successmetricsinc.com");
			Element rootElement = new Element("Repository");					
			rootElement.addNamespaceDeclaration(Namespace.getNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance"));
			rootElement.addNamespaceDeclaration(Namespace.getNamespace("xsd", "http://www.w3.org/2001/XMLSchema"));
			rootElement.setNamespace(ns);
			
			XmlUtils.addContent(rootElement, "Version", r.RepositoryVersion, ns);
			XmlUtils.addContent(rootElement, "BuilderVersion", r.getCurrentBuilderVersion(),ns);
			XmlUtils.addContent(rootElement, "SourceCodeControlVersion", r.getSourceCodeControlVersion(),ns);
			XmlUtils.addContent(rootElement, "CurrentReferenceID", r.getCurrentReferenceID(),ns);
			if (r.getLastEdit()!=null)
			{
				XmlUtils.addContent(rootElement, "LastEdit", r.getLastEdit(),ns);
			}
			XmlUtils.addContent(rootElement, "ApplicationName", r.getApplicationName(),ns);
			XmlUtils.addContent(rootElement, "TargetDimension", r.getTargetDimension(),ns);
			XmlUtils.addContent(rootElement, "OptimizationGoal", r.getOptimizationGoal(),ns);
			if (r.repoDefaultConn!=null)
			{
				XmlUtils.addContent(rootElement, "DefaultConnection", r.repoDefaultConn,ns);
			}
			
			XmlUtils.addContent(rootElement, "GenerateSubjectArea", r.getGenerateSubectArea(),ns);
			XmlUtils.addContent(rootElement, "IsDynamic", r.isDynamic(),ns);			
			XmlUtils.addContent(rootElement, "QueryLanguageVersion", r.getQueryLanguageVersion(),ns);
			XmlUtils.addContent(rootElement, "MaxPhysicalTableNameLength", r.getMaxPhysicalTableNameLength(),ns);
			XmlUtils.addContent(rootElement, "MaxPhysicalColumnNameLength", r.getMaxPhysicalColumnNameLength(),ns);
			XmlUtils.addContent(rootElement, "UseNewETLSchemeForIB", r.isUseNewETLSchemeForIB(),ns);
			XmlUtils.addContent(rootElement, "MinYear", r.getMinYear(),ns);
			XmlUtils.addContent(rootElement, "MaxYear", r.getMaxYear(),ns);
			if (r.AllowNonUniqueGrains)
			{
				XmlUtils.addContent(rootElement, "AllowNonUniqueGrains", r.AllowNonUniqueGrains,ns);
			}
			if (r.isLogCacheKey())
			{
				XmlUtils.addContent(rootElement, "LogCacheKey", r.isLogCacheKey(),ns);
			}
			if (r.getDimensionalStructure()!=0)
			{
				XmlUtils.addContent(rootElement, "DimensionalStructure", r.getDimensionalStructure(),ns);
			}
			
			Element elemRefPopulations = new Element("ReferencePopulations",ns);
			ReferencePopulation[] refPopulations = r.getReferencePopulations();
			if (refPopulations!=null && refPopulations.length>0)
			{
				for (ReferencePopulation refPopulation : refPopulations)
				{
					elemRefPopulations.addContent(refPopulation.getReferencePopulationElement(ns));
				}
			}
			rootElement.addContent(elemRefPopulations);
			
			XmlUtils.addContent(rootElement, "NumPerformancePeers", r.getNumPerformancePeers(),ns);
			XmlUtils.addContent(rootElement, "NumSuccessPatternPeers", r.getNumSuccessPatternPeers(),ns);
			
			if (r.SuccessMeasure!=null)
			{
				XmlUtils.addContent(rootElement, "SuccessMeasure", r.SuccessMeasure,ns);
			}
			
			if (r.PeeringFilters!=null && r.PeeringFilters.length>0)
			{
				rootElement.addContent(Repository.getStringArrayElement("PeeringFilters", r.PeeringFilters, ns));
			}
			
			Element elemPerfModels = new Element("PerformanceModels",ns);
			PerformanceModel[] perfModels = r.getPerformanceModels();
			if (perfModels!=null && perfModels.length>0)
			{
				for (PerformanceModel perfModel : perfModels)
				{
					Element elemPerfModel = perfModel.getPerformanceModelElement(ns);
					elemPerfModels.addContent(elemPerfModel);
				}
			}
			rootElement.addContent(elemPerfModels);
			
			Element elemSuccessModels = new Element("SuccessModels",ns);
			SuccessModel[] successModels = r.getSuccessModels();
			if (successModels!=null && successModels.length>0)
			{
				for (SuccessModel successModel : successModels)
				{
					elemSuccessModels.addContent(successModel.getSuccessModelElement(ns));
				}
			}
			rootElement.addContent(elemSuccessModels);
			
			Element elemSearchDefs = new Element("SearchDefinitions",ns);
			PatternSearchDefinition[] searchDefs = r.getSearchDefinitions();
			if (searchDefs!=null && searchDefs.length>0)
			{
				for (PatternSearchDefinition searchDef : searchDefs)
				{
					elemSearchDefs.addContent(searchDef.getPatternSearchDefinitionElement(ns));
				}
			}
			rootElement.addContent(elemSearchDefs);
			
			Element elemFilters = new Element("Filters",ns);
			QueryFilter queryFilters[] = r.getFilters();					
			if (queryFilters!=null && queryFilters.length>0)
			{
				for (QueryFilter qf : queryFilters)
				{
					elemFilters.addContent(qf.getQueryFilterElement("QueryFilter", ns));
				}
			}
			rootElement.addContent(elemFilters);
			
			Element elemOutcomes = new Element("Outcomes",ns);
			Outcome[] outcomes = r.getOutcomes();
			if (outcomes!=null && outcomes.length>0)
			{
				for (Outcome outcome : outcomes)
				{
					elemOutcomes.addContent(outcome.getOutcomeElement(ns));
				}
			}
			rootElement.addContent(elemOutcomes);		
			
			Element elemMeasureTables = new Element("MeasureTables",ns);
			MeasureTable[] measureTables = r.MeasureTables;
			if (measureTables!=null && measureTables.length>0)
			{
				for (MeasureTable mt : measureTables)
				{
					Element elm = mt.getMeasureTableElement(ns, r);
					elemMeasureTables.addContent(elm);
				}
			}
			rootElement.addContent(elemMeasureTables);
			
			Element elemDimensionTables = new Element("DimensionTables",ns);
			DimensionTable[] dimensionTables = r.allDimensionTables;
			if (dimensionTables!=null && dimensionTables.length>0)
			{
				for (DimensionTable dt : dimensionTables)
				{
					Element elm = dt.getDimensionTableElement(ns, r);
					elemDimensionTables.addContent(elm);
				}
			}
			rootElement.addContent(elemDimensionTables);
			
			Element elemDimensions = new Element("Dimensions",ns);
			Dimension[] dimensions = r.Dimensions;
			if (dimensions!=null && dimensions.length>0)
			{
				for (Dimension dm : dimensions)
				{
					Element elm = dm.getDimensionElement(ns);
					elemDimensions.addContent(elm);
				}
			}
			rootElement.addContent(elemDimensions);
			
			Element joinElements = new Element("Joins",ns);
			Join[] joins = r.getJoins();
			if (joins!=null && joins.length>0)
			{
				for (Join j : joins)
				{
					Element joinElemnent = j.getJoinElement(ns, r);
					joinElements.addContent(joinElemnent);
				}
			}
			rootElement.addContent(joinElements);
			
			Element elemVirtualCols = new Element("VirtualColumns",ns);
			VirtualColumn[] virtualCols = r.getVirtualColumns();
			if (virtualCols!=null && virtualCols.length>0)
			{
				for (VirtualColumn vc : virtualCols)
				{
					Element vcElement = vc.getVirtualColumnElement(ns);
					elemVirtualCols.addContent(vcElement);
				}
			}
			rootElement.addContent(elemVirtualCols);
			
			Element eHierarchies = new Element("Hierarchies",ns);
			Hierarchy[] hierarchies = r.getHierarchies();
			if (hierarchies!=null && hierarchies.length>0)
			{
				for (Hierarchy hrchy : hierarchies)
				{
					Element childElement = hrchy.getHierarchyElement(ns);
					eHierarchies.addContent(childElement);
				}
			}
			rootElement.addContent(eHierarchies);
			
			Element eAggregates = new Element("Aggregates",ns);
			Aggregate[] aggregates = r.getAggregates();
			if (aggregates!=null && aggregates.length>0)
			{
				for (Aggregate agg : aggregates)
				{
					eAggregates.addContent(agg.getAggregateElement(ns));
				}
			}
			rootElement.addContent(eAggregates);
			
			Element eSubAreas = new Element("SubjectAreas",ns);
			SubjectArea[] subAreas = r.getAllSubjectAreas();
			if (subAreas!=null && subAreas.length>0)
			{
				for (SubjectArea subarea : subAreas)
				{
					if (subarea!=null)
					{
						eSubAreas.addContent(subarea.getSubjectAreaElement(ns));
					}
				}
			}
			rootElement.addContent(eSubAreas);			
			
			Element serverParams = r.getServerParameters().getServerParametersElement(ns); 
			rootElement.addContent(serverParams);
			
			Element eConnections = new Element("Connections",ns);
			List<DatabaseConnection> databaseConnections = r.getConnections();
			if (databaseConnections!=null && !databaseConnections.isEmpty())
			{
				for (DatabaseConnection dbConn : databaseConnections)
				{
					Element childElement = dbConn.getDatabaseConnectionElement(ns);
					eConnections.addContent(childElement);
				}
			}
			rootElement.addContent(eConnections);
			
			Element modelParameters = r.getModelParameters().getModelParametersElement(ns);
			rootElement.addContent(modelParameters);
			
			Element optimizerParameters = r.getOptimizerParameters().getOptimizerParameter(ns);
			rootElement.addContent(optimizerParameters);
			
			Element eGroups = new Element("Groups",ns);
			Group[] groups = r.repositoryGroups;
			if (groups!=null && groups.length>0)
			{
				for (Group group : groups)
				{
					Element grpElement = group.getGroupElement(ns);
					eGroups.addContent(grpElement);
				}
			}
			rootElement.addContent(eGroups);
			
			Element eUsers = new Element("Users",ns);
			User[] users = r.repositoryUsers;
			if (users!=null && users.length>0)
			{
				for (User user : users)
				{
					eUsers.addContent(user.getUserElement(ns));
				}
			}
			rootElement.addContent(eUsers);
						
			Variable[] variables = r.getVariables();
			if (variables!=null && variables.length>0)
			{
				Element eVariables = new Element("Variables",ns);				
				for (Variable var : variables)
				{
					Element childElement = var.getVariableElement(ns);
					eVariables.addContent(childElement);
				}
				rootElement.addContent(eVariables);
			}			
			
			Element eResources = new Element("Resources",ns);
			Resource resources[] = r.rResources;
			if (resources!=null && resources.length>0)
			{
				for (Resource re : resources)
				{
					Element childElement = re.getResourceElement(ns);
					eResources.addContent(childElement);
				}
			}
			rootElement.addContent(eResources);
			
			Element elemBroadcasts = new Element("Broadcasts",ns);
			Broadcast[] broadcasts = r.getBroadcasts();
			if (broadcasts!=null && broadcasts.length>0)
			{
				for (Broadcast bc : broadcasts)
				{
				  elemBroadcasts.addContent(bc.getBroadcastElement(ns));
				}
			}
			rootElement.addContent(elemBroadcasts);
			
			Element elemACL = new Element("ACL",ns);
			ACLItem[] arrACLItems = r.repositoryACLItems;
			if (arrACLItems!=null && arrACLItems.length>0)
			{
				for (ACLItem aclItem : arrACLItems)
				{
					elemACL.addContent(aclItem.getACLItemElement(ns));
				}
			}			
			rootElement.addContent(elemACL);
			
			Element elemOperations = new Element("Operations",ns);
			Operation[] operations = r.getOperations();
			if (operations!=null && operations.length>0)
			{
				for (Operation op : operations)
				{
					elemOperations.addContent(op.getOperationElement(ns));
				}
			}
			rootElement.addContent(elemOperations);
			
			Element elemSourceFiles = new Element("SourceFiles",ns);
			SourceFile[] sourceFiles = r.getSourceFiles();
			if (sourceFiles!=null && sourceFiles.length>0)
			{
				for (SourceFile sf : sourceFiles)
				{
					elemSourceFiles.addContent(sf.getSourceFileElement(ns));
				}
			}
			rootElement.addContent(elemSourceFiles);
			
			Element elemStTables = new Element("StagingTables",ns);
			StagingTable[] stTables = r.getStagingTables();
			if (stTables!=null && stTables.length>0)
			{
				for (StagingTable st : stTables)
				{
					elemStTables.addContent(st.getStagingTableElement(ns));
				}
			}
			rootElement.addContent(elemStTables);
			
			Element elemStFilters = new Element("StagingFilters",ns);
			StagingFilter stFilters[] = r.stFilters;
			if (stFilters!=null && stFilters.length>0)
			{
				for (StagingFilter sf : stFilters)
				{
					elemStFilters.addContent(sf.getStagingFilterElement(ns));
				}
			}
			rootElement.addContent(elemStFilters);
			
			Element unmappedMg = new Element("UnmappedMeasureGrains",ns);
			MeasureGrain[] measureGrains = r.UnmappedMeasureGrains;
			if (measureGrains!=null && measureGrains.length>0)
			{
			  for (MeasureGrain mg : measureGrains)
			  {
			    unmappedMg.addContent(mg.getMeasureGrainElement(ns));
			  }
			}
			rootElement.addContent(unmappedMg);
			
			rootElement.addContent(r.getTimeDefinition().getTimeDefinitionElement(ns));
			
			Element elemLoadGrps = new Element("LoadGroups",ns);
			LoadGroup[] loadGroups = r.getLoadGroups();
			if (loadGroups !=null && loadGroups.length>0)
			{
				for (LoadGroup lg : loadGroups)
				{
					elemLoadGrps.addContent(lg.getLoadGroupElement(ns));
				}
			}
			rootElement.addContent(elemLoadGrps);
			
			Element newElement = new Element("ScriptGroups",ns);
			if (r.getScriptGroups()!=null && r.getScriptGroups().length>0)
			{
				for (ScriptGroup sg : r.getScriptGroups())
				{
					newElement.addContent(sg.getScriptGroupElement(ns));
				}
			}
			rootElement.addContent(newElement);
						
			HierarchyLevel[][] hLevels = r.getDependencies();
			if (hLevels!=null && hLevels.length>0)
			{
				newElement = new Element("Dependencies",ns);
				for (HierarchyLevel[] hLevelArr : hLevels)
				{
					Element arrayHLevel = new Element("ArrayOfHierarchyLevel",ns);
					for (HierarchyLevel hLevel : hLevelArr)
					{
						arrayHLevel.addContent(hLevel.getHierarchyLevelElement(ns));
					}
					newElement.addContent(arrayHLevel);
				}
				rootElement.addContent(newElement);
			}	
			
			List<QuickDashboard> quickDashboards = r.getQuickDashboards();
			if (quickDashboards!=null && !quickDashboards.isEmpty())
			{
				newElement = new Element("QuickDashboards",ns);
				for (QuickDashboard qd : quickDashboards)
				{
					newElement.addContent(qd.getQuickDashboardElement(ns));
				}
				rootElement.addContent(newElement);
			}
			
			List<CustomDrill> customDrills = r.getCustomDrills();
			if (customDrills!=null && !customDrills.isEmpty())
			{
			  newElement = new Element("CustomDrills",ns);
        for (CustomDrill cd : customDrills)
        {
          newElement.addContent(cd.getCustomDrillElement(ns));
        }
        rootElement.addContent(newElement);
			}
			
			if (r.logicalExpressions!=null && r.logicalExpressions.length>0)
			{
				newElement = new Element("Expressions",ns);
				for (LogicalExpression logicExp : r.logicalExpressions)
				{
					newElement.addContent(logicExp.getLogicalExpressionElement(ns));
				}
				rootElement.addContent(newElement);
			}
			
			if (r.Imports!=null && !r.Imports.isEmpty())
			{
			  newElement = new Element("Imports",ns);        
        for (PackageImport pi : r.Imports)
        {
          newElement.addContent(pi.getPackageImportElement(ns));
        }
        rootElement.addContent(newElement);
			}
			
			
			XmlUtils.addContent(rootElement, "RequiresPublish", r.isRequiresPublish(),ns);			
			XmlUtils.addContent(rootElement, "RequiresRebuild", r.isRequiresRebuild(),ns);
			
			rootElement.addContent(r.getBuildProperties().getBuildPropertiesElement(ns));
			
			if (r.DynamicGroups!=null)
			{
				newElement = new Element("DynamicGroups",ns);
				for (String[] lvl1 : r.DynamicGroups)
				{
					Element arrayOfString = new Element("ArrayOfString",ns);
					for (String lvl2 : lvl1)
					{
						Element stringElement = new Element("string",ns);
						stringElement.setText(lvl2);
						arrayOfString.addContent(stringElement);
					}
					newElement.addContent(arrayOfString);
				}
				rootElement.addContent(newElement);
			}
			
			XmlUtils.addContent(rootElement, "Hybrid", r.isHybrid(),ns);
			XmlUtils.addContent(rootElement, "NoBirstApplicationRebuild", r.isNoBirstApplicationRebuild(),ns);
			
			if (r.isDisablePublishLock())
			{
				XmlUtils.addContent(rootElement, "NoBirstApplicationRebuild", r.isDisablePublishLock(),ns);	
			}
			
			if (r.isEnableLiveAccessToDefaultConnection())
			{
				XmlUtils.addContent(rootElement, "EnableLiveAccessToDefaultConnection", r.isEnableLiveAccessToDefaultConnection(),ns);
			}
			
			if (r.isAllowExternalIntegrations())
			{
				XmlUtils.addContent(rootElement, "AllowExternalIntegrations", r.isAllowExternalIntegrations(),ns);
			}
			
			if (r.getExternalIntegrationTimeout()!=-1)
			{
				XmlUtils.addContent(rootElement, "ExternalIntegrationTimeout", r.getExternalIntegrationTimeout(),ns);
			}
			
			if (r.getNumThresholdFailedRecords()!=0)
			{
				XmlUtils.addContent(rootElement, "NumThresholdFailedRecords", r.getNumThresholdFailedRecords(),ns);
			}
			XmlUtils.addContent(rootElement, "SkipDBAConnectionMerge", r.SkipDBAConnectionMerge,ns);
			
			if (r.isDisableImpliedGrainsProcessing())
			{
				XmlUtils.addContent(rootElement, "DisableImpliedGrainsProcessing", r.isDisableImpliedGrainsProcessing(),ns);
			}
			
			if (r.getWarehousePartitions()!=null && !r.getWarehousePartitions().isEmpty())
			{
			  newElement = new Element("WarehousePartitions",ns);        
        for (WarehousePartition wp : r.getWarehousePartitions())
        {
          newElement.addContent(wp.getWarehousePartitionElement(ns));
        }
        rootElement.addContent(newElement);
			}
			
			if (r.isPartitioned())
			{
				XmlUtils.addContent(rootElement, "IsPartitioned", r.isPartitioned(),ns);
			}
			
			XmlUtils.addContent(rootElement, "LiveAccessSecurityFilter", r.isLiveAccessSecurityFilter(),ns);
			
			Document repositoryDoc = new Document(rootElement);
			repositoryDoc.setRootElement(rootElement);
			Format xmlFormat = Format.getPrettyFormat();
			xmlFormat.setTextMode(TextMode.PRESERVE);			
			xmlFormat.setEncoding("utf-8");
			xmlFormat.setOmitDeclaration(false);
			xmlFormat.setOmitEncoding(false);
			xmlFormat.setExpandEmptyElements(false);
			xmlFormat.setIndent("  ");
			XMLOutputter xmlOutputter = new XMLOutputter(xmlFormat);
			xmlOutputter.output(repositoryDoc, new FileWriter(saveFileName));
		}
		catch (Exception e)
		{
			logger.error("Exception in saveFile ",e);
			throw e;
		}
	}
	
	public static boolean backupFile(String filePath,String bkupPrefix,int versionCount)
  {
    boolean fileBackedUp = true;
    
    try
    {
      File srcFile = new File(filePath);
      int index=0;
      
      for (;index < versionCount;index++)
      {
        String bkupFilePath = filePath + "." + index + "." + bkupPrefix;
        File bkupFile = new File(bkupFilePath);
        if (bkupFile.exists())
        {
          continue;
        }
        else
        {
          Files.copy(srcFile.toPath(), bkupFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
          //FileUtils.copyFile(srcFile, bkupFile);
          break;
        }
      }
      
      if (index==versionCount)
      {
        /*
         * All files exist so do the rolling backup
         */
        index = 0;
        for (;index < versionCount;index++)
        { 
          String bkupFilePath = filePath + "." + index + "." + bkupPrefix;
          File bkupFile = new File(bkupFilePath);
          if (index == 0)
          {
            /*
             * Delete the oldest file
             */
            bkupFile.delete();
          }
          else
          {
            /*
             * Rename file
             */
            String modifiedFilePath = filePath + "." + (index - 1)+ "." + bkupPrefix;
            bkupFile.renameTo(new File(modifiedFilePath));
          }
          if (index == (versionCount-1))
          {
            /*
             * Copy the newest file to this one
             */
            //FileUtils.copyFile(srcFile, bkupFile);
            Files.copy(srcFile.toPath(), bkupFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
          }
        }
      }
    }
    catch(Exception e)
    {
      fileBackedUp = false;
      logger.error("Exception while backing up file ",e);
    }
    
    return fileBackedUp;
  }
		
}