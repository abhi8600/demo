package com.successmetricsinc.metadatavalidation.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.successmetricsinc.LogicalExpression;
import com.successmetricsinc.Repository;
import com.successmetricsinc.Variable;
import com.successmetricsinc.Variable.VariableType;
import com.successmetricsinc.query.Level;
import com.successmetricsinc.query.LevelKey;
import com.successmetricsinc.warehouse.StagingTable;
import com.successmetricsinc.warehouse.TimeDefinition;


public class RuleUtil {

	/**
	 * Get the level keys for a given dimension and level
	 * 
	 * @param r
	 * @param dimensionName
	 * @param levelName
	 * @return
	 */
	public static List<String> getlevelKeys(Repository r, String dimensionName, String levelName)
	{
		List<String> levelKeys = new ArrayList<String>();		
		Level l = r.findDimensionLevel(dimensionName, levelName);			
		
		if(l != null)
		{									
			if(l.Keys != null)
			{
				for (LevelKey lk : l.Keys)
				{	
					if(!lk.SurrogateKey)
					{
						levelKeys.addAll(Arrays.asList(lk.ColumnNames));
					}
				}
			}
			
		}
		
		return levelKeys;
	}

	/**
	 * It returns list of SessionVariable names from the repository 
	 * @param r
	 * @return
	 */
	public static List<String> getSessionVariables(Repository r) {
		List<String> sessionVariables = new ArrayList<>();
		if(r.getVariables() != null)
		{
			for(Variable var: r.getVariables())
			{
				if(var.getType() == VariableType.Session)
				{
					sessionVariables.add(var.getName());
				}
			}
		}
		
		return sessionVariables;
	}
	
	/**
	 * Verifies whether both staging tables are on the same grain
	 * 
	 * @param grainSet
	 * @return
	 */
	public static boolean isAtMatchingGrain(StagingTable st1, StagingTable st2)
	{
		boolean match = false;
		// Make sure grains match
		if(st1.Levels != null && st2.Levels != null)
		{
			match = st1.Levels.length == st2.Levels.length;
			if (match) {
				for (String[] level1 : st1.Levels) {
					boolean found = false;
					for (String[] level2 : st2.Levels) {
						if (level1[0].equals(level2[0])	&& level1[1].equals(level2[1])) {
							found = true;
							break;
						}
					}
					if (!found) {
						match = false;
						break;
					}
				}
			}
		}
		return match;
	}
	
	/**
	 * compares level of logical expression and staging table
	 * 
	 * @param grainSet
	 * @return
	 */
	public static boolean isAtMatchingGrain(StagingTable st, LogicalExpression le, Repository r)
	{		
		boolean match = false;
		// Make sure grains match
		if(st.Levels != null && le.Levels != null)
		{
			if(r.getTimeDefinition() != null && le.Type == 1)
				match = st.Levels.length-1 == le.Levels.length;  //don't count Time dimension for custom attribute
			else
				match = st.Levels.length == le.Levels.length;
			
			if (match) 
			{
				for (String[] level1 : st.Levels) 
				{
					if(le.Type == 1 && r.getTimeDefinition() != null && level1[0].equals(r.getTimeDefinition().Name))
					{
						continue;
					}
					boolean found = false;
					for (String[] level2 : le.Levels) 
					{
						if (level1[0].equals(level2[0])	&& level1[1].equals(level2[1])) 
						{
							found = true;
							break;
						}
					}
					if (!found) {
						match = false;
						break;
					}					
				}
			}
		}
		return match;
	}
	
	public static Set<String> getLevelKeysForStagingTable(StagingTable st, Repository r)
	{
		Set<String> levelCols = new TreeSet<String>();
		if(st.Levels != null)
		{
			for(String[] level : st.Levels)
			{
				if(r.getTimeDefinition() != null && level[0].equals(r.getTimeDefinition().Name)){					
					continue;
				}				
				levelCols.addAll(getlevelKeys(r, level[0], level[1]));						
			}

		}		
		return levelCols;
	}		
}
