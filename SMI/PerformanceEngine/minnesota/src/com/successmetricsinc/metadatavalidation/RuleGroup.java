package com.successmetricsinc.metadatavalidation;

import java.util.ArrayList;
import java.util.List;

import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.metadatavalidation.util.ResourceBundleUtil;

/**
 * @author pbindal
 * This class is a java object representation of the rule group configuration from the metadata xml
 */
public class RuleGroup 
{
	private List<Rule> rules;
	private String groupName;
	private String Description;
	private String displayname;
	
	public RuleGroup(Element element, Namespace ns, boolean isUserRepoAdmin) 
	{		
		groupName = element.getAttributeValue("name");
		displayname = element.getAttributeValue("displayname");
		if (displayname!=null)
		{
			displayname = ResourceBundleUtil.getValidationLabel(displayname);					
		}
		Description = element.getChildText("Description", ns);
		if (Description!=null)
		{
			Description = ResourceBundleUtil.getValidationLabel(Description);					
		}
		List children = element.getChildren();
		
		if (children!=null && !children.isEmpty())
		{
			this.setRules(new ArrayList<Rule>());
			for (int count = 0; count < children.size(); count++)
			{
				Element elem = (Element) children.get(count);
				if (elem.getName().equals("Rule"))
				{
					Rule rule = new Rule(elem, ns, displayname);
					if(!isUserRepoAdmin){
						if(!rule.isRepoAdmin()){
							getRules().add(rule);
						}
					}else{
						getRules().add(rule);
					}
				}
			}
		}		
	}

	public List<Rule> getRules() 
	{
		return rules;
	}
	
	public void setRules(List<Rule> rules) 
	{
		this.rules = rules;
	}
	
	public String getGroupName() 
	{
		return groupName;
	}
	
	public void setGroupName(String groupName) 
	{
		this.groupName = groupName;
	}

	public String getDescription() 
	{
		return Description;
	}

	public void setDescription(String description) 
	{
		Description = description;
	}

	public String getDisplayname() 
	{
		return displayname;
	}

	public void setDisplayname(String displayname) 
	{
		this.displayname = displayname;
	}	
}
