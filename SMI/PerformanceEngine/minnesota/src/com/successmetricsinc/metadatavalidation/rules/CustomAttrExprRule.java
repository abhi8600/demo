/**
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
 
package com.successmetricsinc.metadatavalidation.rules;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.successmetricsinc.LogicalExpression;
import com.successmetricsinc.Repository;
import com.successmetricsinc.metadatavalidation.util.ResourceBundleUtil;
import com.successmetricsinc.metadatavalidation.util.RuleUtil;
import com.successmetricsinc.warehouse.StagingColumn;
import com.successmetricsinc.warehouse.StagingTable;

/**
 * This class validates expression of each CustomAttribute.
 * Desc: Expression name shouldn't be the same as column from staging tables and Case expressions should not be nested to more than 10 level
 * 
 * @author jshah
 *
 */
public class CustomAttrExprRule extends BaseRule {

	boolean valid = true;	
	private String LOGICAL_EXPRESSION_CHECK = "LOGICAL_EXPRESSION_CHECK_RULE";
	private String LOGICAL_EXPRESSION_CHECK1 = "LOGICAL_EXPRESSION_CHECK1_RULE";
	
	/**
	 * It validates CaseExpression by counting number of consecutive END. 
	 * If count exceeds 10 then set valid = false.
	 */
	@Override
	protected boolean validate(Repository r) {
		validateLogicalExprName(r);
		if(r.logicalExpressions != null)
		{
			//Pattern to find consecutive END more than 10 times			
			Pattern p = Pattern.compile("(END\\)*\\s*){11}");
			for(LogicalExpression le: r.logicalExpressions)
			{
				if(le.Type == 1)
				{
					Matcher m = p.matcher(le.Expression);
					if (m.find()) {
						valid = false;
						String msg = ResourceBundleUtil.getValidationMessageFromBundle(LOGICAL_EXPRESSION_CHECK, le.getName());
						invalidList.add(msg);
					}
				}
			}
		}
		
		return valid;
	}

	/**
	 * set valid=false
	 * if  Custom attribute name using the same name as column names (even untargeted) from staging table. 
	 * @param r
	 */
	private void validateLogicalExprName(Repository r) 
	{
		if(r.logicalExpressions != null)
		{			
			for(LogicalExpression le: r.logicalExpressions)
			{
				if(le.Type == 1)
				{
					for (StagingTable st : r.getStagingTables()) {
						if (RuleUtil.isAtMatchingGrain(st, le, r)) {
							if (getStagingColNames(st).contains(le.getName())) {
								valid = false;
								String msg = ResourceBundleUtil.getValidationMessageFromBundle(LOGICAL_EXPRESSION_CHECK1, le.getName(), st.Name);
								invalidList.add(msg);
							}
						}
					}
				}
			}
		}
	}

	/**
	 * returns set of staging column names
	 * @param st
	 * @return
	 */
	private Set<String> getStagingColNames(StagingTable st) 
	{
		Set<String> stagingColumns = new HashSet<String>();
		for(StagingColumn sc: st.Columns)
		{
			stagingColumns.add(sc.Name);
		}

		return stagingColumns;
	}
	

	@Override
	protected boolean fix(Repository r) {
		// TODO Auto-generated method stub
		return false;
	}

}
