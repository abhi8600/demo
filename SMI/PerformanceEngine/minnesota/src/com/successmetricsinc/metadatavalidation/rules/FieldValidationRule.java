package com.successmetricsinc.metadatavalidation.rules;

import com.successmetricsinc.Repository;
import com.successmetricsinc.metadatavalidation.util.ResourceBundleUtil;
import com.successmetricsinc.warehouse.StagingTable;

/**
 * This class validates sources against ExcludeFromModel and Disabled fields
 * @author jshah
 *
 */
public class FieldValidationRule extends BaseRule {

	boolean valid = true;
	private String DISABLED_FIELD_SET_ON_SOURCE = "DISABLED_FIELD_SET_ON_SOURCE";
	private String EXCLUDEFROMMODEL_FIELD_SET_ON_SOURCE = "EXCLUDEFROMMODEL_FIELD_SET_ON_SOURCE";
	
	/**
	 * Fail validation if either Disabled or ExcludeFromModel field is set to true
	 */
	@Override
	protected boolean validate(Repository r) 
	{
		for(StagingTable st : r.getStagingTables())
		{
			if(st.Disabled)
			{
				valid = false;
				String msg = ResourceBundleUtil.getValidationMessageFromBundle(DISABLED_FIELD_SET_ON_SOURCE, st.Name);
				invalidList.add(msg);
			}
			
			if(st.ExcludeFromModel)
			{
				valid = false;
				String msg = ResourceBundleUtil.getValidationMessageFromBundle(EXCLUDEFROMMODEL_FIELD_SET_ON_SOURCE, st.Name);
				invalidList.add(msg);
			}
		}
		
		return valid;
	}

	@Override
	protected boolean fix(Repository r) {
		// TODO Auto-generated method stub
		return false;
	}

}
