/**
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.successmetricsinc.metadatavalidation.rules;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.successmetricsinc.Repository;
import com.successmetricsinc.metadatavalidation.util.ResourceBundleUtil;
import com.successmetricsinc.metadatavalidation.util.RuleUtil;
import com.successmetricsinc.transformation.ScriptDefinition;
import com.successmetricsinc.warehouse.StagingTable;

/**
 * This class validates ETL script of scripted source
 * @author jshah
 *
 */
public class ETLScriptValidationRule extends BaseRule {

	private boolean valid = true;
	private String ETL_SCRIPT_VALIDATION_RULE = "ETL_SCRIPT_VALIDATION_RULE";
	
	/**
	 * validates ETL script
	 */
	@Override
	protected boolean validate(Repository r) 
	{	
		List<String> sessionVariables = RuleUtil.getSessionVariables(r);
		if(!sessionVariables.isEmpty())
		{
			validateScriptedSource(sessionVariables, r);
		}
		
		return valid;
	}

	/**
	 * Iterate over each scripted source and validates ETL script against session variables.
	 * Session variable should not be used in ETL script.
	 * 
	 * @param sessionVariables
	 * @param r
	 */
	private void validateScriptedSource(List<String> sessionVariables, Repository r) 
	{
		//fetch GetVariable('varName') from script - ignore case
		Pattern p = Pattern.compile("(?i)GetVariable\\s*\\(.*?\\)");
		for(StagingTable st: r.getStagingTables())
		{
			if(st.Script != null && st.Script.Script != null && !st.Script.Script.isEmpty())
			{
				List<String> variables = new ArrayList<String>();						
				Matcher m = p.matcher(st.Script.Script);
				while(m.find())
				{
					//fetch varName from GetVariable('varName')
					if(m.group().indexOf("'") != m.group().lastIndexOf("'"))
					{
						String var = m.group().substring(m.group().indexOf("'") + 1, m.group().lastIndexOf("'"));
						if(variables.contains(var))
						{
							continue;
						}
						variables.add(var);
						if(var != null && sessionVariables.contains(var.trim()))
						{
							valid = false;
							String msg = ResourceBundleUtil.getValidationMessageFromBundle(ETL_SCRIPT_VALIDATION_RULE, var, st.Name);
							invalidList.add(msg);
						}
					}
				}
				variables.clear();				
			}
		}		
	}

	@Override
	protected boolean fix(Repository r) {
		// TODO Auto-generated method stub
		return false;
	}

}
