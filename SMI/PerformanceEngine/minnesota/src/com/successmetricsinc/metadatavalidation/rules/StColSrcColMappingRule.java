/**
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.successmetricsinc.metadatavalidation.rules;

import java.util.ArrayList;
import java.util.List;

import com.successmetricsinc.Repository;
import com.successmetricsinc.metadatavalidation.util.ResourceBundleUtil;
import com.successmetricsinc.warehouse.SequenceNumber;
import com.successmetricsinc.warehouse.SourceColumn;
import com.successmetricsinc.warehouse.SourceFile;
import com.successmetricsinc.warehouse.StagingColumn;
import com.successmetricsinc.warehouse.StagingTable;

/**
 * It validates StagingColumn-SourceColumn mappings
 * 
 * @author jshah
 *
 */
public class StColSrcColMappingRule extends BaseRule {

	boolean valid = true;
	private String STCOL_SRCCOL_MAPPING_CHECK_RULE = "STCOL_SRCCOL_MAPPING_CHECK_RULE";
	private String STCOL_SRCCOL_MAPPING_CHECK2_RULE = "STCOL_SRCCOL_MAPPING_CHECK2_RULE";
	
	/**
	 * returns false if source column referred by staging column doesn't exist in associated source file
	 */
	@Override
	protected boolean validate(Repository r) {
				
		for(StagingTable st: r.getStagingTables())
		{
			SourceFile sf = r.findSourceFile(st.SourceFile);
			if(sf == null)
			{
				valid = false;
				String msg = ResourceBundleUtil.getValidationMessageFromBundle(STCOL_SRCCOL_MAPPING_CHECK2_RULE, st.SourceFile, st.Name);
				invalidList.add(msg);
				continue;
			}
			List<String> sourceFileColumns = new ArrayList<String>();
			for(SourceColumn sc: sf.Columns)
			{
				sourceFileColumns.add(sc.Name);
			}
			
			for(StagingColumn sc : st.Columns)
			{
				if (!sc.isPersistable())
				{
					continue;
				}
				if(!sourceFileColumns.contains(sc.SourceFileColumn) && !sf.isValidColumnExpression(sc.SourceFileColumn))
				{
					String dim = null;
					if (!(sc.SourceFileColumn != null || !sc.NaturalKey || (sc.Transformations != null && sc.Transformations.length > 0) || !sc.DataType.equals("Integer")))
					{
						dim = SequenceNumber.getDimension(sc, r);
					}
											
					if(dim == null)
					{
						if (sc.SourceFileColumn != null)
						{		
							valid = false;
							String msg = ResourceBundleUtil.getValidationMessageFromBundle(STCOL_SRCCOL_MAPPING_CHECK_RULE, 
									sc.SourceFileColumn, sf.FileName, st.Name);
							invalidList.add(msg);
						}
					}
				}												
			}
		}
		
		return valid;
	}

	@Override
	protected boolean fix(Repository r) {
		// TODO Auto-generated method stub
		return false;
	}

}
