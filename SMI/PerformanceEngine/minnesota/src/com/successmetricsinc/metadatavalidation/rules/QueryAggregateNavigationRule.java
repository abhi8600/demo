/**
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.metadatavalidation.rules;

import java.io.IOException;
import java.sql.SQLException;

import com.successmetricsinc.Repository;
import com.successmetricsinc.metadatavalidation.util.ResourceBundleUtil;
import com.successmetricsinc.query.AbstractQueryString;
import com.successmetricsinc.query.Aggregate;
import com.successmetricsinc.query.Query;
import com.successmetricsinc.query.QueryAggregate;
import com.successmetricsinc.query.QueryString;
import com.successmetricsinc.util.BaseException;

/**
 * Validates aggregates
 * @author jshah
 *
 */
public class QueryAggregateNavigationRule extends BaseRule {

	private boolean valid = true;
	private String QUERY_AGGREGATE_NAVIGATION_RULE = "QUERY_AGGREGATE_NAVIGATION_RULE";
	
	/**
	 * Validates logical query used in aggregate
	 */
	@Override
	protected boolean validate(Repository r) 
	{
		if(r.getAggregates() != null)
		{
			for(Aggregate aggregate : r.getAggregates())
			{
				if(aggregate.isQuery())
				{
					String query = QueryString.preProcessQueryString(aggregate.getLogicalQuery(r), r, null);
					AbstractQueryString aqs = AbstractQueryString.getInstance(r, query);	
					Query q = null;
					try {
						q = aqs.getQuery();
						q.navigateQuery(true, false, null, true);					
					} catch (IOException | SQLException | BaseException	| CloneNotSupportedException e) {
						/*
						 * fail validation if QueryAggregate unable to navigate logical query
						 */
						valid = false;
						String msg = ResourceBundleUtil.getValidationMessageFromBundle(QUERY_AGGREGATE_NAVIGATION_RULE, aggregate.getName(), aggregate.getLogicalQuery(r), e.getMessage());
						invalidList.add(msg);
					}
				}
			}
		}
		return valid;
	}

	@Override
	protected boolean fix(Repository r) {
		// TODO Auto-generated method stub
		return false;
	}

}
