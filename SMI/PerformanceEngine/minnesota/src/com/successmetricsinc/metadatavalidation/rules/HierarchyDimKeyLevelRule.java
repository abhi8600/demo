package com.successmetricsinc.metadatavalidation.rules;

import java.util.ArrayList;

import com.successmetricsinc.Repository;
import com.successmetricsinc.metadatavalidation.util.ResourceBundleUtil;
import com.successmetricsinc.query.Hierarchy;
import com.successmetricsinc.query.Level;

/**
 * @author pbindal
 *
 */
public class HierarchyDimKeyLevelRule extends BaseRule 
{

	@Override
	public boolean validate(Repository r) 
	{		
		boolean valid = true;
		for (Hierarchy h : r.getHierarchies())
		{
			Level dimKeyLevel = h.DimensionKeyLevel;
			if (dimKeyLevel.Children!=null && dimKeyLevel.Children.length>0)
			{
				String msg = ResourceBundleUtil.getValidationMessageFromBundle("HIERARCHY_DIM_LEVEL_RULE", dimKeyLevel.Name,h.Name);
				if (invalidList==null)
				{
					invalidList = new ArrayList<String>();
				}				
				invalidList.add(msg);
				valid = false;
			}
		}
		return valid;
	}	

	@Override
	public boolean fix(Repository r) 
	{
		boolean fixed = false;
		try
		{
  		for (Hierarchy h : r.getHierarchies())
      {
        Level dimKeyLevel = h.DimensionKeyLevel;
        if (dimKeyLevel.ChildrenOrig!=null && dimKeyLevel.ChildrenOrig.length>0)
        {          
          h.DimensionKeyLevel = findLowestLevelChild(dimKeyLevel);
          h.setDimensionKeyLevel(h.DimensionKeyLevel.Name);
          fixed = true;
          isInvalid = true;
          String msg = ResourceBundleUtil.getValidationMessageFromBundle("HIERARCHY_DIM_LEVEL_RULEFIX", h.DimensionKeyLevel.Name,h.Name);
          if (fixList==null)
          {
            fixList = new ArrayList<String>();
          }       
          fixList.add(msg);
        }
      }
		}
		catch (Exception ex)
		{
		  logger.error("Exception while fixing rule "+ruleName+",",ex);
		}
		return fixed;
	}
	
	private Level findLowestLevelChild(Level dimKeyLevel)
	{
	  Level[] childLevels = dimKeyLevel.ChildrenOrig;
	  for (Level childLevel : childLevels)
	  {
	    if (childLevel.ChildrenOrig!=null && childLevel.ChildrenOrig.length>0)
	    {
	      return findLowestLevelChild(childLevel);
	    }
	    else
	    {
	      return childLevel;
	    }
	  }
	  return null;
	}

}