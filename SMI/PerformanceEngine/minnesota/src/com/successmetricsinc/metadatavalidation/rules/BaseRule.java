package com.successmetricsinc.metadatavalidation.rules;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.metadatavalidation.util.ResourceBundleUtil;
import com.successmetricsinc.metadatavalidation.util.SaveRepository;

/**
 * @author pbindal
 * @version 1.00
 * Abstract class with common code for all rules. All other classes must implement the validate and fix methods
 */
public abstract class BaseRule 
{
	protected static Logger logger = Logger.getLogger(BaseRule.class);
	protected String ruleName;
	protected String validationMsg;
	protected String fixMsg;
	protected List<String> invalidList = new ArrayList<String>();	
	protected List<String> fixList = new ArrayList<String>();
	protected boolean isInvalid;
	
	/**
	 * Initialize message bundle
	 */
	public BaseRule()
	{
		
	}
		
	/**
	 * This method needs to be implemented by each of the rules extending this class. Contains the main rule functionality
	 * @param r
	 * @return
	 */
	protected abstract boolean validate(Repository r);	
	
	/**
	 * Method to fix to be implemented by each rule class. 
	 * @param r
	 * @return
	 */
	protected abstract boolean fix(Repository r);
	
	/**
	 * @param r - Repository to validate
	 * @return - true if valid, else false
	 */
	public boolean validateRepository(Repository r)
	{
		StringBuilder logMsg = null;
		long startTime = 0;
		long endTime = 0;
		if (logger.isDebugEnabled())
		{
			startTime = System.currentTimeMillis();
			logMsg = new StringBuilder();
			logMsg.append("Starting ");
			logMsg.append(" rule ").append(this.ruleName);
			logMsg.append(" at ").append(startTime);
			logger.debug(logMsg);
		}
		
		boolean isValid = validate(r);
		
		setValidationMsg(isValid);
		
		if (logger.isDebugEnabled())
		{
		  //logger.debug(validationMsg);
		}
		
		if (logger.isDebugEnabled())
		{
			endTime = System.currentTimeMillis();
			logMsg = new StringBuilder();
			logMsg.append("Finished ");
			logMsg.append(" rule ").append(this.ruleName);
			logMsg.append(" at ").append(endTime).append(System.lineSeparator());
			logMsg.append("Total Time : ").append((endTime-startTime)).append(" milliseconds");
			logger.debug(logMsg);
		}
		
		return isValid;
	}
	
	public boolean fixRepository(Repository r)
	{
	  StringBuilder logMsg = null;
    long startTime = 0;
    long endTime = 0;
    if (logger.isDebugEnabled())
    {
      startTime = System.currentTimeMillis();
      logMsg = new StringBuilder();
      logMsg.append("Starting fix for ");
      logMsg.append(" rule ").append(this.ruleName);
      logMsg.append(" at ").append(startTime);
      logger.debug(logMsg);
    }
    
    boolean fixed = false;
    fixed = fix(r);
    
    setFixMsg(isInvalid,fixed);
    
    if (logger.isDebugEnabled())
    {
      //logger.debug(fixMsg);
    }
    
    if (logger.isDebugEnabled())
    {
      endTime = System.currentTimeMillis();
      logMsg = new StringBuilder();
      logMsg.append("Finished fix for ");
      logMsg.append(" rule ").append(this.ruleName);
      logMsg.append(" at ").append(endTime).append(System.lineSeparator());
      logMsg.append("Total Time : ").append((endTime-startTime)).append(" milliseconds");
      logger.debug(logMsg);
    }
    
    return fixed;
	}
	
	/**
	 * Depending on the result of validation, set a logical validation message to be consumed by calling methods
	 * @param valid
	 */
	protected void setValidationMsg(boolean valid)
	{
		if (valid)
		{				
			validationMsg = ResourceBundleUtil.getValidationMessageFromBundle("REPOSITORY_VALID", ruleName);
		}
		else
		{
			StringBuilder invalidMsg = new StringBuilder();			
			if (invalidList!=null && !invalidList.isEmpty())
			{								
				invalidMsg.append(ResourceBundleUtil.getValidationMessageFromBundle("REPOSITORY_INVALID_FIX", ruleName));
				/*
				 * Please leave in separate list
				for (String invalidString : invalidList)
				{
					invalidMsg.append(invalidString).append("\n");					
				}
				*/
			}
			else
			{
				invalidMsg.append(ResourceBundleUtil.getValidationMessageFromBundle("REPOSITORY_INVALID", ruleName));
			}					
			validationMsg = invalidMsg.toString();
		}
	}
	
	protected void setFixMsg(boolean isInvalid,boolean fixed)
  {    
    if (!isInvalid)
    {
      fixMsg = System.lineSeparator()+ResourceBundleUtil.getValidationMessageFromBundle("REPOSITORY_VALID", ruleName);
    }
    else if (isInvalid && !fixed)
    {
      fixMsg = System.lineSeparator()+ResourceBundleUtil.getValidationMessageFromBundle("REPOSITORY_INVALID_NO_FIX", ruleName);
    }
    else      
    {
      StringBuilder repositoryFixMessage = new StringBuilder();     
      if (fixList!=null && !fixList.isEmpty())
      {
        repositoryFixMessage.append(ResourceBundleUtil.getValidationMessageFromBundle("REPOSITORY_APPLIED_FIXES", ruleName)).append(System.lineSeparator());
        for (String fixString : fixList)
        {
          repositoryFixMessage.append(fixString).append(System.lineSeparator());
        }
      }
      else
      {
        repositoryFixMessage.append(ResourceBundleUtil.getValidationMessageFromBundle("REPOSITORY_INVALID_NO_FIX", ruleName));
      }
      fixMsg = System.lineSeparator()+repositoryFixMessage.toString();
    }
  }
	
	public String getValidationMsg()
	{
		return validationMsg;
	}	
	
	public void setRuleName(String ruleName) 
	{
		this.ruleName = ruleName;
	}
	
	public boolean isInvalid()
	{
	  return isInvalid;
	}

  public String getFixMsg() {
    return fixMsg;
  }

  public void setFixMsg(String fixMsg) {
    this.fixMsg = fixMsg;
  }

	public List<String> getInvalidList() {
		return invalidList;
	}
	
}