package com.successmetricsinc.metadatavalidation.rules;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.successmetricsinc.Repository;
import com.successmetricsinc.metadatavalidation.util.ResourceBundleUtil;
import com.successmetricsinc.metadatavalidation.util.RuleUtil;
import com.successmetricsinc.warehouse.StagingColumn;
import com.successmetricsinc.warehouse.StagingTable;

public class LevelKeyDatatypeWidthValidationRule extends BaseRule {
	private boolean valid = true;
	private String STAGINGCOLUMN_LEVELKEY_VALIDATION_HAVING_SAME_NAME_AND_MARKED_ON_SAME_HIERARCHY = "STAGINGCOLUMN_LEVELKEY_VALIDATION_HAVING_SAME_NAME_AND_MARKED_ON_SAME_HIERARCHY";
	private final String MEASURE = "Measure";
	private final String TIME = "Time";
		
	List<DatatypeWidth> datatypeWidthList = new ArrayList<DatatypeWidth>();	
	
	@Override
	protected boolean validate(Repository r) 
	{				
		for(StagingTable st: r.getStagingTables())
		{			
			Set<String> levelKeys = RuleUtil.getLevelKeysForStagingTable(st, r);
			if(!levelKeys.isEmpty())
			{
				for(StagingColumn sc: st.Columns)
				{
					if(levelKeys.contains(sc.Name))
					{
						String hierarchy = getHierarchyName(sc.TargetTypes);
						if(hierarchy != null)
						{
							DatatypeWidth datatypeWidth = new DatatypeWidth(st.Name, hierarchy, sc.Name, sc.DataType, sc.Width);
							validateDatatypeWidth(datatypeWidth);
						}
					}
				}
			}
		}			
		return valid;
	}

	/**
	 * compares data type and width of stagingColumns having same name and marked on same hierarchy
	 * 
	 * @param hierarchy
	 * @param sc
	 * @param st
	 */
	private void validateDatatypeWidth(DatatypeWidth datatypeWidth) 
	{	
		int i;
		if((i = datatypeWidthList.indexOf(datatypeWidth)) != -1)
		{
			DatatypeWidth existedDatatypeWidth = datatypeWidthList.get(i);
			if(!existedDatatypeWidth.datatype.equals(datatypeWidth.datatype) || existedDatatypeWidth.width != datatypeWidth.width)
			{
				valid = false;
				String msg = ResourceBundleUtil.getValidationMessageFromBundle(STAGINGCOLUMN_LEVELKEY_VALIDATION_HAVING_SAME_NAME_AND_MARKED_ON_SAME_HIERARCHY, 
						existedDatatypeWidth.stagingName, datatypeWidth.stagingName, datatypeWidth.stagingColumnName);
				invalidList.add(msg);				
			}
		}else{
			datatypeWidthList.add(datatypeWidth);
		}
			
		
	}

	@Override
	protected boolean fix(Repository r) {
		// TODO Auto-generated method stub
		return false;
	}
	
	/**
	 * returns hierarchy name of staging column
	 * 
	 * @param targetTypes
	 * @return
	 */
	private String getHierarchyName(List<String> targetTypes) 
	{		
		for(String target : targetTypes)
		{
			if(!target.equals(MEASURE) && !target.equals(TIME))
			{
				return target;
			}
		}
		return null;
	}
	
	/**
	 * 
	 * @author jshah
	 */
	static class DatatypeWidth
	{
		String stagingName;
		String hierarchyName;
		String stagingColumnName;
		String datatype;
		int width;
		
		public DatatypeWidth(String stagingName, String hierarchyName, String stagingColumnName, String datatype, int width) 
		{
			super();
			this.stagingName = stagingName;
			this.hierarchyName = hierarchyName;
			this.stagingColumnName = stagingColumnName;
			this.datatype = datatype;
			this.width = width;
		}

		@Override
		public int hashCode() 
		{
			final int prime = 31;
			int result = 1;
			result = prime * result	+ ((hierarchyName == null) ? 0 : hierarchyName.hashCode());
			result = prime * result	+ ((stagingColumnName == null) ? 0 : stagingColumnName.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) 
		{
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			DatatypeWidth other = (DatatypeWidth) obj;
			if (hierarchyName == null) {
				if (other.hierarchyName != null)
					return false;
			} else if (!hierarchyName.equals(other.hierarchyName))
				return false;
			if (stagingColumnName == null) {
				if (other.stagingColumnName != null)
					return false;
			} else if (!stagingColumnName.equals(other.stagingColumnName))
				return false;
			return true;
		}					
	}

}
