package com.successmetricsinc.metadatavalidation.rules;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.successmetricsinc.Repository;
import com.successmetricsinc.metadatavalidation.util.ResourceBundleUtil;
import com.successmetricsinc.metadatavalidation.util.RuleUtil;
import com.successmetricsinc.query.DimensionColumn;
import com.successmetricsinc.query.DimensionTable;
import com.successmetricsinc.warehouse.StagingColumn;
import com.successmetricsinc.warehouse.StagingTable;

/**
 * It validates "Level key column is present against a hierarchy level with valid Birst data type and length" rule
 * 
 * @author jshah
 *
 */
public class LevelKeyDataTypeLengthCheckRule extends BaseRule 
{
	boolean valid = true;
	
	private String LEVELKEY_DATATYPE_LENGTH_CHECK1_RULE = "LEVELKEY_DATATYPE_LENGTH_CHECK1_RULE";
	private String LEVELKEY_DATATYPE_LENGTH_CHECK2_RULE = "LEVELKEY_DATATYPE_LENGTH_CHECK2_RULE";
	
	/**
	 *  It gets DimensionTable associated with StagingTable's level and 
	 *  compare DataType and Length of levelKey on StagingTable and DimensionTable 
	 */
	@Override
	protected boolean validate(Repository r) 
	{
		Map<String, List<String>> levelKeysbyLevel = new HashMap<String, List<String>>();
		
		for(StagingTable st : r.getStagingTables())
		{
			if(st.Levels != null)
			{
				for(String[] level : st.Levels)
				{	
					if(level.length != 2 || (r.getTimeDefinition() != null && level[0].equals(r.getTimeDefinition().Name)))
					{
						continue;
					}										
				
					List<String> levelkeys = levelKeysbyLevel.get(level[0]+"."+level[1]);
					if(levelkeys == null)
					{
						levelkeys = RuleUtil.getlevelKeys(r, level[0], level[1]);	
						levelKeysbyLevel.put(level[0]+"."+level[1], levelkeys);
					}
				
					if(levelkeys != null && !levelkeys.isEmpty())
					{
						DimensionTable dt = r.findDimensionTableAtLevel(level[0], level[1]);								
						if(dt == null)
						{
							continue;
						}
						compareLevelKeyDatatypeAndLength(st, dt, levelkeys);										
					}																															
				}
			}
		}
		return valid;
	}

	/**
	 * Compare DataType and Length of LevelKey on StagingTable and DimensionTable
	 * 
	 * @param st 
	 * @param dt
	 * @param levelColumns - List of columns at given level
	 */
	private void compareLevelKeyDatatypeAndLength(StagingTable st, DimensionTable dt, List<String> levelkeys) 
	{
		for(String col: levelkeys)
		{
			StagingColumn sc = st.getStagingColumn(col);
			DimensionColumn dc = dt.findColumn(col);			
			if(sc != null && dc != null && sc.DataType != null)
			{
				if(!sc.DataType.equalsIgnoreCase(dc.DataType))
				{
					valid = false;
					String msg = ResourceBundleUtil.getValidationMessageFromBundle(LEVELKEY_DATATYPE_LENGTH_CHECK1_RULE, col, st.Name, dt.PhysicalName);
					invalidList.add(msg);							
				}
				
				if(sc.DataType.equalsIgnoreCase("varchar") && sc.Width != dc.Width)
				{
					valid = false;
					String msg = ResourceBundleUtil.getValidationMessageFromBundle(LEVELKEY_DATATYPE_LENGTH_CHECK2_RULE, col, st.Name, dt.PhysicalName);
					invalidList.add(msg);							
				}
			}
		}		
	}

	@Override
	protected boolean fix(Repository r) 
	{
		// TODO Auto-generated method stub
		return false;
	}

}
