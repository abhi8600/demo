/**
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
 
package com.successmetricsinc.metadatavalidation.rules;

import java.util.ArrayList;
import java.util.List;

import com.successmetricsinc.Repository;
import com.successmetricsinc.metadatavalidation.util.ResourceBundleUtil;
import com.successmetricsinc.query.Level;
import com.successmetricsinc.query.LevelKey;
import com.successmetricsinc.warehouse.SourceColumn;
import com.successmetricsinc.warehouse.SourceFile;
import com.successmetricsinc.warehouse.StagingTable;

/**
 * Validates existence of LevelKey column in source files
 * @author jshah
 *
 */
public class LvlKeyColSrcColMappingRule extends BaseRule {

	private boolean valid = true;
	private String LEVELKEY_COL_SOURCE_COL_MAPPING = "LEVELKEY_COL_SOURCE_COL_MAPPING";
	
	@Override
	protected boolean validate(Repository r) 
	{
		for(StagingTable st : r.getStagingTables())
		{
			SourceFile sf = r.findSourceFile(st.SourceFile);
			List<String> sourceColumns = getAssociatedSourceColumns(sf, st, r);
			validateLevelKeyColumn(sf, st, sourceColumns, r);						
		}
		return valid;
	}

	/**
	 * returns source columns associated with staging table
	 * @param sf
	 * @param st
	 * @param r
	 * @return
	 */
	private List<String> getAssociatedSourceColumns(SourceFile sf, StagingTable st, Repository r) {
		List<String> sourceColumns = new ArrayList<String>();		
		for(SourceColumn sc : sf.Columns)
		{
			sourceColumns.add(sc.Name);
		}
		return sourceColumns;
	}

	/**
	 * it validates existence of levelKey columns in source files associated with given staging table
	 * @param sf
	 * @param st
	 * @param sourceColumns
	 * @param r
	 */
	private void validateLevelKeyColumn(SourceFile sf, StagingTable st, List<String> sourceColumns, Repository r) 
	{
		if(st.Levels != null)
		{
			for(String[] levels : st.Levels)
			{		
				if(levels.length != 2 || (r.getTimeDefinition() != null && levels[0].equals(r.getTimeDefinition().Name)))
				{
					continue;
				}
				Level l = r.findLevel(levels[0], levels[1]);
				if(l != null && l.Keys != null)
				{
					for(LevelKey lk: l.Keys)
					{
						if(!lk.SurrogateKey)
						{
							for(String col: lk.ColumnNames)
							{
								if(!sourceColumns.contains(col))
								{
									valid = false;
									String msg = ResourceBundleUtil.getValidationMessageFromBundle(LEVELKEY_COL_SOURCE_COL_MAPPING, col, l.Name, sf.FileName, st.Name);
									invalidList.add(msg);
								}
							}
						}
					}
				}
			}
		}
	}

	@Override
	protected boolean fix(Repository r) {
		// TODO Auto-generated method stub
		return false;
	}

}
