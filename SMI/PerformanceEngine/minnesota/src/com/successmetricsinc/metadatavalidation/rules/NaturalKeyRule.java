package com.successmetricsinc.metadatavalidation.rules;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.successmetricsinc.Repository;
import com.successmetricsinc.metadatavalidation.util.ResourceBundleUtil;
import com.successmetricsinc.metadatavalidation.util.RuleUtil;
import com.successmetricsinc.query.Hierarchy;
import com.successmetricsinc.query.Level;
import com.successmetricsinc.query.LevelKey;
import com.successmetricsinc.warehouse.StagingColumn;
import com.successmetricsinc.warehouse.StagingTable;

/**
 * it validates whether level key(of level or parent level in that hierarchy) exists and corresponding stagingColumn is marked as natural key
 * @author jshah
 *
 */
public class NaturalKeyRule extends BaseRule {

	private String LEVELKEY_NATURALKEY_CHECKS1_RULE = "LEVELKEY_NATURALKEY_CHECKS1_RULE";
	private String LEVELKEY_NATURALKEY_CHECKS2_RULE = "LEVELKEY_NATURALKEY_CHECKS2_RULE";
	private String LEVELKEY_NATURALKEY_CHECKS3_RULE = "LEVELKEY_NATURALKEY_CHECKS3_RULE";
	
	@Override
	protected boolean validate(Repository r) {		
		boolean valid = true;
		for(StagingTable st : r.getStagingTables())
		{	
			Set<String> levelCols = new TreeSet<String>();
			if(st.Levels != null)
			{
				for(String[] level : st.Levels)
				{
					if(r.getTimeDefinition() != null && level[0].equals(r.getTimeDefinition().Name)){
						levelCols.add(r.getTimeDefinition().getDayIDColumnName());	//DAY ID should be marked as natural key on each source
						continue;
					}				
					levelCols.addAll(RuleUtil.getLevelKeysForStagingTable(st, r));						
				}
				
				if(levelCols.isEmpty())
				{
					valid = false;
					String msg = ResourceBundleUtil.getValidationMessageFromBundle(LEVELKEY_NATURALKEY_CHECKS1_RULE, st.Name);
					addInvalidMsg(msg);	
				}
			
				for(String levelCol : levelCols)
				{
					StagingColumn sc = st.getStagingColumn(levelCol);
					if(sc == null)
					{
						valid = false;
						String msg = ResourceBundleUtil.getValidationMessageFromBundle(LEVELKEY_NATURALKEY_CHECKS3_RULE, levelCol, st.Name);
						addInvalidMsg(msg);
						continue;
					}
					if(sc != null && !sc.NaturalKey)
					{
						valid = false;
						String msg = ResourceBundleUtil.getValidationMessageFromBundle(LEVELKEY_NATURALKEY_CHECKS2_RULE, sc.Name, st.Name);
						addInvalidMsg(msg);							
					}
				}
			}
		}
		return valid;
	}
	
	@Override
	protected boolean fix(Repository r) 
	{
	  boolean fixed = false;
	  try
	  {
	    for(StagingTable st : r.getStagingTables())
	    { 
	      Set<String> levelCols = new TreeSet<String>();
	      if(st.Levels != null)
	      {
	        for(String[] level : st.Levels)
	        {
	          if(r.getTimeDefinition() != null && level[0].equals(r.getTimeDefinition().Name))
	          {
	            continue;
	          }       
	          levelCols.addAll(RuleUtil.getLevelKeysForStagingTable(st, r));       
	        }
	      
	        if(levelCols.isEmpty())
	        {
	          fixed = false;
	          //TODO How do we fix this? 
	        }
	      
	        for(String levelCol : levelCols)
	        {
	          StagingColumn sc = st.getStagingColumn(levelCol);
	          if(sc != null && !sc.NaturalKey)
	          {
	            sc.NaturalKey = true;
	            fixed = true;
	            isInvalid = true;
	            String msg = ResourceBundleUtil.getValidationMessageFromBundle("LEVELKEY_NATURALKEY_CHECKS2_RULEFIX", sc.Name, st.Name);
	            if (fixList==null)
	            {
	              fixList = new ArrayList<String>();
	            }       
	            fixList.add(msg);
	          }
	        }
	      }
	    }
	  }
	  catch (Exception ex)
	  {
	    logger.error("Exception while fixing rule "+ruleName+",",ex);
	  }
	  return fixed;
	}
	
	/**
	 * Add invalid message to list
	 * @param key
	 * @param st
	 */
	private void addInvalidMsg(String msg) {		
		if (invalidList==null)
		{
			invalidList = new ArrayList<String>();
		}				
		invalidList.add(msg);
	}

}
