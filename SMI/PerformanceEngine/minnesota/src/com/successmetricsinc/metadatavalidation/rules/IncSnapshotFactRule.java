package com.successmetricsinc.metadatavalidation.rules;

import java.util.ArrayList;

import com.successmetricsinc.Repository;
import com.successmetricsinc.metadatavalidation.util.ResourceBundleUtil;
import com.successmetricsinc.warehouse.StagingColumn;
import com.successmetricsinc.warehouse.StagingTable;

/**
 * Rule to validate IncrementalSnapshotFact
 * @author jshah
 *
 */
public class IncSnapshotFactRule extends BaseRule 
{
	private String INCREMENTAL_SNAPSHOT_FACT1_RULE = "INCREMENTAL_SNAPSHOT_FACT1_RULE";
	private String INCREMENTAL_SNAPSHOT_FACT2_RULE = "INCREMENTAL_SNAPSHOT_FACT2_RULE";
	private String INCREMENTAL_SNAPSHOT_FACT3_RULE = "INCREMENTAL_SNAPSHOT_FACT3_RULE";
	private String msg;
	
	/**
	 * validates delete keys are marked or not.
	 * if marked then checks marked delete key(s) must be primary key. 
	 */
	@Override
	protected boolean validate(Repository r) 
	{
		boolean valid = true;
		for(StagingTable st : r.getStagingTables())
		{			
			if(st.IncrementalSnapshotFact)
			{		
				if(st.SnapshotDeleteKeys == null)
				{
					valid = false;
					msg = ResourceBundleUtil.getValidationMessageFromBundle(INCREMENTAL_SNAPSHOT_FACT1_RULE, st.getDisplayName());
					addInvalidMsg(msg);	
					continue;
				}
				
				int len = st.SnapshotDeleteKeys.length;
				switch(len)
				{
				case 0:
					valid = false;
					msg = ResourceBundleUtil.getValidationMessageFromBundle(INCREMENTAL_SNAPSHOT_FACT1_RULE, st.getDisplayName());
					addInvalidMsg(msg);					
					break;					
				default:
					if(!isValidDelKeys(st, r)) valid=false;
					break;
				}																
			}			
		}
						
		return valid;
	}
		
	@Override
	protected boolean fix(Repository r) 
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	/**
	 * validate delete keys against stagingColumns, level key and measure
	 * 
	 * @param st
	 * @param r
	 * @return true, if it's part of staging table and marked it as either level key or measure
	 */
	private boolean isValidDelKeys(StagingTable st, Repository r) 
	{
		boolean valid = true;
		for(String delKey : st.SnapshotDeleteKeys)
		{
			StagingColumn sc = st.getStagingColumn(delKey);
			if(sc == null)
			{
				valid = false;
				msg = ResourceBundleUtil.getValidationMessageFromBundle(INCREMENTAL_SNAPSHOT_FACT2_RULE, delKey, st.getDisplayName());
				addInvalidMsg(msg);
			}else if(!sc.TargetTypes.contains("Measure") && !sc.NaturalKey){
				valid = false;
				msg = ResourceBundleUtil.getValidationMessageFromBundle(INCREMENTAL_SNAPSHOT_FACT3_RULE, delKey, st.getDisplayName());
				addInvalidMsg(msg);
			}
		}
		return valid;
	}
	
	/**
	 * Add invalid message to list
	 * @param key
	 * @param st
	 */
	private void addInvalidMsg(String msg) {		
		if (invalidList==null)
		{
			invalidList = new ArrayList<String>();
		}				
		invalidList.add(msg);
	}

}
