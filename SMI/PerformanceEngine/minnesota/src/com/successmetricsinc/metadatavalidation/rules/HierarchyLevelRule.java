package com.successmetricsinc.metadatavalidation.rules;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.successmetricsinc.Repository;
import com.successmetricsinc.metadatavalidation.util.ResourceBundleUtil;
import com.successmetricsinc.query.Hierarchy;
import com.successmetricsinc.query.Level;

public class HierarchyLevelRule extends BaseRule {

	private String HIERARCHY_LEVEL_CHECKS1_RULE = "HIERARCHY_LEVEL_CHECKS1_RULE";
	private String HIERARCHY_LEVEL_CHECKS2_RULE = "HIERARCHY_LEVEL_CHECKS2_RULE";
	
	@Override
	protected boolean validate(Repository r) {
		boolean valid = true;		
		Map<String, List<String>> hierarchyLevelbyHierarchy = new HashMap<String, List<String>>();
		for (Hierarchy h : r.getHierarchies())
		{		
			if(!isHierarchyNameUnique(h, hierarchyLevelbyHierarchy))
			{
				valid = false;
				continue;
			}
						
			List<Level> levels = new ArrayList<Level>();
			h.getAllDecendents(levels);
			for(Level l : levels)
			{
				List<String> levelNames = hierarchyLevelbyHierarchy.get(h.Name);
				if(levelNames.contains(l.Name))
				{
					String msg = ResourceBundleUtil.getValidationMessageFromBundle(HIERARCHY_LEVEL_CHECKS2_RULE, l.Name, h.Name);
					invalidList.add(msg);
					valid = false;
				}else{
					levelNames.add(l.Name);
				}
			}
		}
		
		return valid;
	}

	/**
	 * 
	 * @param h
	 * @param hierarchyLevelbyHierarchy
	 * @return false, if hierarchyLevelbyHierarchy already contains HierarchyName
	 */
	private boolean isHierarchyNameUnique(Hierarchy h, Map<String, List<String>> hierarchyLevelbyHierarchy) 
	{
		if(hierarchyLevelbyHierarchy.containsKey(h.Name))
		{			
			String msg = ResourceBundleUtil.getValidationMessageFromBundle(HIERARCHY_LEVEL_CHECKS1_RULE, h.Name);
			invalidList.add(msg);
			return false;
		}else{
			hierarchyLevelbyHierarchy.put(h.Name, new ArrayList<String>());
		}
		
		return true;
	}

	@Override
	protected boolean fix(Repository r) {
		// TODO Auto-generated method stub
		return false;
	}
	
}
