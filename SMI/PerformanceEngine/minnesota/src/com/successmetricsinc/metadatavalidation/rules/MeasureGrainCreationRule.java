/**
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.metadatavalidation.rules;

import com.successmetricsinc.Repository;
import com.successmetricsinc.metadatavalidation.util.ResourceBundleUtil;
import com.successmetricsinc.warehouse.StagingTable;

/**
 * Rule to check pre-requisite of measure grain creation
 * @author jshah
 *
 */
public class MeasureGrainCreationRule extends BaseRule {

	boolean valid = true;
	private String MEASURE_GRAIN_CREATION_CHECK_RULE = "MEASURE_GRAIN_CREATION_CHECK_RULE";
	
	/**
	 * Validates whether more than one sources are at the same level name(s) of different hierarchies
	 * If yes, then set valid=false
	 */
	@Override
	protected boolean validate(Repository r) {
		StagingTable[] stagingTables =  r.getStagingTables();
		
		for(int i=0; i<stagingTables.length; i++)
		{
			StagingTable st = stagingTables[i];
			if(st.Levels != null)
			{
				for(int j=i+1; j<stagingTables.length; j++)
				{
					StagingTable stable = stagingTables[j];
					if(!st.Name.equals(stable.Name) && stable.Levels != null && st.Levels.length==stable.Levels.length)
					{				
						int dim = 0, lvl = 0;						
						for(String[] l: st.Levels)
						{										
							for(String[] level: stable.Levels)
							{
								if(l[1].equals(level[1]))
								{
									lvl++;
									if(l[0].equals(level[0]))
									{
										dim++;
									}
								}																										
							}
						}
						
						/**
						 * Check whether sources are at the same level name of different hierarchies.
						 * If yes, then Measure table will be created only once due to name conflict which is not valid.
						 */
						if(lvl == st.Levels.length && lvl != dim)
						{
							valid = false;
							String msg = ResourceBundleUtil.getValidationMessageFromBundle(MEASURE_GRAIN_CREATION_CHECK_RULE, 
									st.Name, stable.Name);
							invalidList.add(msg);
						}
					}
				}
			}
		}
		
		return valid;
	}

	

	@Override
	protected boolean fix(Repository r) {
		// TODO Auto-generated method stub
		return false;
	}

}
