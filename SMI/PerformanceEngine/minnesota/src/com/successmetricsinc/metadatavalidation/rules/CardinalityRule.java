package com.successmetricsinc.metadatavalidation.rules;

import java.util.ArrayList;
import java.util.List;

import com.successmetricsinc.Repository;
import com.successmetricsinc.metadatavalidation.util.ResourceBundleUtil;
import com.successmetricsinc.query.Hierarchy;
import com.successmetricsinc.query.Level;

public class CardinalityRule extends BaseRule {

	private String CARDINALITY_CHECKS_RULE = "CARDINALITY_CHECKS_RULE";
	
	@Override
	protected boolean validate(Repository r) {
		boolean valid = true;		
		for (Hierarchy h : r.getHierarchies())
		{
			List<Level> levels = new ArrayList<Level>();
			h.getAllDecendents(levels);
			for(Level l : levels)
			{
				if(l.Cardinality <= 0)
				{
					valid = false;
					String msg = ResourceBundleUtil.getValidationMessageFromBundle(CARDINALITY_CHECKS_RULE, h.Name+"-"+l.Name);
					invalidList.add(msg);
				}
			}
		}
		
		return valid;
	}

	@Override
	protected boolean fix(Repository r) {
		// TODO Auto-generated method stub
		return false;
	}
		
}
