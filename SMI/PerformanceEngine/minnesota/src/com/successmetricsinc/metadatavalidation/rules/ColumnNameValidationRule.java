package com.successmetricsinc.metadatavalidation.rules;


import java.util.HashSet;
import java.util.Set;

import com.successmetricsinc.Repository;
import com.successmetricsinc.metadatavalidation.util.ResourceBundleUtil;
import com.successmetricsinc.metadatavalidation.util.RuleUtil;
import com.successmetricsinc.transformation.DataType;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.warehouse.StagingColumn;
import com.successmetricsinc.warehouse.StagingTable;

/**
 * It validates column names when data sources have the same grain, and have date columns whose names are same or differ only 
 * by underscores being used in one column name whereas spaces are used in the other column name.
 *  
 * @author jshah
 *
 */
public class ColumnNameValidationRule extends BaseRule 
{
	boolean valid = true;
	private String COLUMN_NAME_VALIDATION1_RULE = "COLUMN_NAME_VALIDATION1_RULE";
	
	@Override
	protected boolean validate(Repository r) 
	{
		StagingTable[] stagingTables =  r.getStagingTables();
		for(int i=0; i<stagingTables.length; i++)
		{
			StagingTable st1 = stagingTables[i];
			for(int j=i+1; j<stagingTables.length; j++)
			{
				StagingTable st2 = stagingTables[j];
				if(RuleUtil.isAtMatchingGrain(st1, st2))
				{
					validateColumnNames(st1, st2, r);
				}
			}						
		}
		return valid;
	}

	/**
	 * validates physical name of date columns
	 * if it's same then set valid=false
	 *  
	 * @param st1
	 * @param st2
	 * @param r
	 */
	private void validateColumnNames(StagingTable st1, StagingTable st2, Repository r) 
	{
		Set<String> columnNames = new HashSet<String>();
		for(StagingColumn col: st1.Columns)
		{	
			if(col.DataType.equals(DataType.DType.Date.name()) || col.DataType.equals(DataType.DType.DateTime.name()))
			{
				columnNames.add(Util.replaceWithNewPhysicalString(col.Name, r.getDefaultConnection(), r));
			}
		}		
		for(StagingColumn col: st2.Columns)
		{		
			if(col.DataType.equals(DataType.DType.Date.name()) || col.DataType.equals(DataType.DType.DateTime.name()))
			{
				String phyName = Util.replaceWithNewPhysicalString(col.Name, r.getDefaultConnection(), r);
				if (columnNames.contains(phyName) && st1.getStagingColumn(col.Name)==null)  //physical name should be same but logical name should be different
				{
					valid = false;
					String msg = ResourceBundleUtil.getValidationMessageFromBundle(COLUMN_NAME_VALIDATION1_RULE, st1.Name, st2.Name, phyName);
					invalidList.add(msg);
				}
			}
		}
	}

		
	@Override
	protected boolean fix(Repository r) 
	{
		// TODO Auto-generated method stub
		return false;
	}

}
