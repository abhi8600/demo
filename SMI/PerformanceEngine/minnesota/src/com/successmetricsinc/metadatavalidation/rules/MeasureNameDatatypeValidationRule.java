/**
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.metadatavalidation.rules;

import java.util.HashMap;
import java.util.Map;

import com.successmetricsinc.Repository;
import com.successmetricsinc.metadatavalidation.util.ResourceBundleUtil;
import com.successmetricsinc.warehouse.StagingColumn;
import com.successmetricsinc.warehouse.StagingTable;

/**
 * This class compares data type of column(marked as measure) having same name
 * @author jshah
 *
 */
public class MeasureNameDatatypeValidationRule extends BaseRule {

	private boolean valid = true;
	private final String VALIDATE_DUPLICATE_MEASURENAME_DATATYPE = "VALIDATE_DUPLICATE_MEASURENAME_DATATYPE";
	
	/**
	 * Validation fails if column having same name marked as measure but have different data types 
	 */
	@Override
	protected boolean validate(Repository r) 
	{
		Map<String, String> dataTypeByMeasureName = new HashMap<String, String>();
		Map<String, String> stagingTableByMeasureName = new HashMap<String, String>();
		
		for(StagingTable st: r.getStagingTables())
		{
			for(StagingColumn sc: st.Columns)
			{
				if(sc.TargetTypes != null && sc.TargetTypes.contains("Measure")) //check if it's marked as measure
				{
					if(dataTypeByMeasureName.containsKey(sc.Name)) //if column having same name already exist
					{
						validateDatatype(dataTypeByMeasureName.get(sc.Name), stagingTableByMeasureName.get(sc.Name), sc, st);
					}else{
						dataTypeByMeasureName.put(sc.Name, sc.DataType);
						stagingTableByMeasureName.put(sc.Name, st.Name);
					}
				}
			}
		}
		return valid;
	}

	/**
	 * Validates data type of a column having same name in different sources
	 * 
	 * @param datatypeAndStaingTable
	 * @param sc
	 * @param st
	 */
	private void validateDatatype(String datatype, String stagingTable, StagingColumn sc, StagingTable st) 
	{			
		if(!datatype.equals(sc.DataType)) //compare data type
		{
			//data type is different for a column having same name
			valid = false;
			String msg = ResourceBundleUtil.getValidationMessageFromBundle(VALIDATE_DUPLICATE_MEASURENAME_DATATYPE, 
					sc.Name, stagingTable, st.Name);
			invalidList.add(msg);
		}
	}

	@Override
	protected boolean fix(Repository r) {
		// TODO Auto-generated method stub
		return false;
	}

}
