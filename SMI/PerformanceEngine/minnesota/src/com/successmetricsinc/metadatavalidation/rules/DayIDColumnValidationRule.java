/**
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.successmetricsinc.metadatavalidation.rules;

import com.successmetricsinc.Repository;
import com.successmetricsinc.metadatavalidation.util.ResourceBundleUtil;
import com.successmetricsinc.warehouse.StagingColumn;
import com.successmetricsinc.warehouse.StagingTable;
import com.successmetricsinc.warehouse.StaticDateID;
import com.successmetricsinc.warehouse.Transformation;

/**
 * This class validates "Day ID" column on staging table
 * @author jshah
 *
 */
public class DayIDColumnValidationRule extends BaseRule 
{
	private boolean valid = true;
	private boolean fixed = false;
	private String DAYID_COLUMN_VALIDATION_RULE = "DAYID_COLUMN_VALIDATION_RULE";
	private String DAYID_COLUMN_VALIDATION_RULE_FIX = "DAYID_COLUMN_VALIDATION_RULE_FIX";
	
	/**
	 * Validation for "SourceFileColumn for 'Day ID' should not be exist if transformation tag is set to V{LoadDate}"
	 */
	@Override
	protected boolean validate(Repository r) 
	{
		if(r.getTimeDefinition() != null)
		{
			for(StagingTable st : r.getStagingTables())
			{
				StagingColumn sc = st.getStagingColumn(r.getTimeDefinition().getDayIDColumnName());
				if(sc != null && sc.SourceFileColumn != null && !sc.SourceFileColumn.isEmpty() && sc.Transformations != null)
				{
					for(Transformation t: sc.Transformations)
					{
						if (t.getClass().equals(StaticDateID.class))
						{
							StaticDateID dateID = (StaticDateID) t;
							if (dateID.Expression != null && dateID.Expression.equals("V{LoadDate}"))
							{
								valid = false;
								String msg = ResourceBundleUtil.getValidationMessageFromBundle(DAYID_COLUMN_VALIDATION_RULE, st.Name);
								invalidList.add(msg);
							}
						}
					}
				}				
			}
		}
		
		return valid;
	}

	/**
	 * remove SourceFileColumn field for 'Day ID' staging column if transformation tag is set to V{LoadDate}"
	 */
	@Override
	protected boolean fix(Repository r) {
		
		if(r.getTimeDefinition() != null)
		{
			for(StagingTable st : r.getStagingTables())
			{
				StagingColumn sc = st.getStagingColumn(r.getTimeDefinition().getDayIDColumnName());
				if(sc != null && sc.SourceFileColumn != null && !sc.SourceFileColumn.isEmpty() && sc.Transformations != null)
				{
					for(Transformation t: sc.Transformations)
					{
						if (t.getClass().equals(StaticDateID.class))
						{
							StaticDateID dateID = (StaticDateID) t;
							if (dateID.Expression != null && dateID.Expression.equals("V{LoadDate}"))
							{
								sc.SourceFileColumn = null;
								fixed = true;
					            isInvalid = true;
								String msg = ResourceBundleUtil.getValidationMessageFromBundle(DAYID_COLUMN_VALIDATION_RULE_FIX, st.Name);
								fixList.add(msg);
							}
						}
					}
				}				
			}
		}
		
		return fixed;
	}	
}
