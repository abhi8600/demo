package com.successmetricsinc.metadatavalidation.rules;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.successmetricsinc.Repository;
import com.successmetricsinc.metadatavalidation.util.ResourceBundleUtil;
import com.successmetricsinc.query.Hierarchy;
import com.successmetricsinc.query.Level;
import com.successmetricsinc.query.LevelKey;
import com.successmetricsinc.warehouse.StagingColumn;
import com.successmetricsinc.warehouse.StagingTable;


/**
 * it validates whether level key of higher levels in the same hierarchy exists as staging column and is marked as natural key
 * @author jshah
 *
 */
public class LevelKeyNaturalKeyRule extends BaseRule 
{	
	private String HIGHERLEVEL_LEVELKEY_NATURALKEY_CHECKS2_RULE = "HIGHERLEVEL_LEVELKEY_NATURALKEY_CHECKS2_RULE";
	private String HIGHERLEVEL_LEVELKEY_NATURALKEY_CHECKS3_RULE = "HIGHERLEVEL_LEVELKEY_NATURALKEY_CHECKS3_RULE";
	
	@Override
	protected boolean validate(Repository r) {		
		boolean valid = true;
		for(StagingTable st : r.getStagingTables())
		{	
			Set<String> levelCols = new TreeSet<String>();
			if(st.Levels != null)
			{
				for(String[] level : st.Levels)
				{
					if(r.getTimeDefinition() != null && level[0].equals(r.getTimeDefinition().Name)){						
						continue;
					}				
					levelCols.addAll(getHigherlevelColumns(r, level[0], level[1]));						
				}
											
				for(String levelCol : levelCols)
				{
					StagingColumn sc = st.getStagingColumn(levelCol);
					if(sc == null)
					{
						valid = false;
						String msg = ResourceBundleUtil.getValidationMessageFromBundle(HIGHERLEVEL_LEVELKEY_NATURALKEY_CHECKS3_RULE, levelCol, st.Name);
						addInvalidMsg(msg);
						continue;
					}
					if(sc != null && !sc.NaturalKey)
					{
						valid = false;
						String msg = ResourceBundleUtil.getValidationMessageFromBundle(HIGHERLEVEL_LEVELKEY_NATURALKEY_CHECKS2_RULE, sc.Name, st.Name);
						addInvalidMsg(msg);							
					}
				}
			}
		}
		return valid;
	}
	
	@Override
	protected boolean fix(Repository r) 
	{
	  boolean fixed = false;
	  try
	  {
	    for(StagingTable st : r.getStagingTables())
	    { 
	      Set<String> levelCols = new TreeSet<String>();
	      if(st.Levels != null)
	      {
	        for(String[] level : st.Levels)
	        {
	          if(r.getTimeDefinition() != null && level[0].equals(r.getTimeDefinition().Name))
	          {
	            continue;
	          }       
	          levelCols.addAll(getHigherlevelColumns(r, level[0], level[1]));       
	        }
	      
	        if(levelCols.isEmpty())
	        {
	          fixed = false;
	          //TODO How do we fix this? 
	        }
	      
	        for(String levelCol : levelCols)
	        {
	          StagingColumn sc = st.getStagingColumn(levelCol);
	          if(sc != null && !sc.NaturalKey)
	          {
	            sc.NaturalKey = true;
	            fixed = true;
	            isInvalid = true;
	            String msg = ResourceBundleUtil.getValidationMessageFromBundle("LEVELKEY_NATURALKEY_CHECKS2_RULEFIX", sc.Name, st.Name);
	            if (fixList==null)
	            {
	              fixList = new ArrayList<String>();
	            }       
	            fixList.add(msg);
	          }
	        }
	      }
	    }
	  }
	  catch (Exception ex)
	  {
	    logger.error("Exception while fixing rule "+ruleName+",",ex);
	  }
	  return fixed;
	}
	
	/**
	 * Get the level columns for a given dimension and level
	 * 
	 * @param r
	 * @param dimensionName
	 * @param levelName
	 * @return
	 */
	private Set<String> getHigherlevelColumns(Repository r, String dimensionName, String levelName)
	{
		Set<String> levelColumns = new TreeSet();
		List<Object> levelList = new ArrayList<Object>();
		Level l = r.findDimensionLevel(dimensionName, levelName);							
		
		if(l != null)
		{			
			getParentLevels(l, levelList);
			for(Object level : levelList )
			{
				l = (Level)level;
				if(l.Keys != null)
				{
					for (LevelKey lk : l.Keys)
					{	
						if(!lk.SurrogateKey)
						{
							levelColumns.addAll(Arrays.asList(lk.ColumnNames));
						}
					}
				}
			}
		}
		
		return levelColumns;
	}
	
	/**
	 * get all the parent levels upto hierarchy
	 * @param l
	 * @param levelList
	 */
	private void getParentLevels(Level l, List<Object> levelList) {
		if(l != null)
		{
			List<Object> parents = l.Parents;
			if(parents.get(0) instanceof Hierarchy)
			{
				return;
			}else
			{
				levelList.addAll(parents);
				getParentLevels((Level)(parents.get(0)), levelList);
			}
		}else{
			return;
		}		
	}

	/**
	 * Add invalid message to list
	 * @param key
	 * @param st
	 */
	private void addInvalidMsg(String msg) {		
		if (invalidList==null)
		{
			invalidList = new ArrayList<String>();
		}				
		invalidList.add(msg);
	}

}
