package com.successmetricsinc.metadatavalidation.rules;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.successmetricsinc.Repository;
import com.successmetricsinc.metadatavalidation.util.ResourceBundleUtil;
import com.successmetricsinc.query.Hierarchy;
import com.successmetricsinc.query.Level;

/**
 * It validates "Availability of unique surrogate key column name" rule
 * 
 * @author jshah
 *
 */
public class SurrogateKeyRule extends BaseRule {

	private boolean valid = true;
	private String UNIQUE_SURROGATE_KEY_CHECK_RULE = "UNIQUE_SURROGATE_KEY_CHECK_RULE";
	
	/**
	 * It validates availability of unique surrogateKey columnName within hierarchy.
	 */
	@Override
	protected boolean validate(Repository r) {
		
		for(Hierarchy h : r.getHierarchies())
		{
			Set<String> surrogateKeys = new HashSet<String>();
			List<Level> levelList = new ArrayList<Level>();
			h.getAllDecendents(levelList);
			for(Level l : levelList)
			{
				if(l != null && l.getSurrogateKey() != null && l.getSurrogateKey().ColumnNames != null)
				{
					String[] levelKeys = l.getSurrogateKey().ColumnNames;
					if(levelKeys == null)
					{
						continue;
					}					
					for(String lKey : levelKeys)
					{
						if(!surrogateKeys.add(lKey))
						{
							valid = false;
							String msg = ResourceBundleUtil.getValidationMessageFromBundle(UNIQUE_SURROGATE_KEY_CHECK_RULE, lKey, h.Name);
							invalidList.add(msg);	
						}
					}					
				}				
			}
		}
		
		return valid;
	}

	@Override
	protected boolean fix(Repository r) {
		// TODO Auto-generated method stub
		return false;
	}

}
