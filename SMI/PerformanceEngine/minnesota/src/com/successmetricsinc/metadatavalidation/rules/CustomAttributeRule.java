/**
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
 
package com.successmetricsinc.metadatavalidation.rules;

import com.successmetricsinc.LogicalExpression;
import com.successmetricsinc.Repository;
import com.successmetricsinc.metadatavalidation.util.ResourceBundleUtil;

/**
 * Rule to validate custom attributes
 *  
 * @author jshah
 *
 */
public class CustomAttributeRule extends BaseRule {

	boolean valid = true;
	private final int CUSTOM_ATTRIBUTE = 1;
	private String CUSTOM_ATTRIBUTE_CHECK = "CUSTOM_ATTRIBUTE_CHECK_RULE";
	
	/**
	 * returns false if any of the CuatomAttribute is derived from any other CustomAttribute at the same level
	 */
	@Override
	protected boolean validate(Repository r) {

		if(r.logicalExpressions != null)
		{			
			for(LogicalExpression le: r.logicalExpressions)
			{
				if(le.Type == CUSTOM_ATTRIBUTE)
				{
					String customAttrName = "["+le.getName()+"]";	//[CustomAttributeName]
					StringBuilder levelNames = new StringBuilder();
					for(String[] level: le.Levels)
					{
						levelNames.append(level[0]).append(level[1]);
					}
				
					for(LogicalExpression lExpr: r.logicalExpressions)
					{
						
						StringBuilder lNames = new StringBuilder();
						//Check whether CustomAttribute expression refers any other CustomAttribute 
						if(lExpr.Type == CUSTOM_ATTRIBUTE && lExpr.Expression.contains(customAttrName))
						{	
							for(String[] level: lExpr.Levels)
							{
								lNames.append(level[0]).append(level[1]);								
							}
							//Compare levels of both the CustomAttributes
							if(levelNames.toString().equals(lNames.toString()))
							{
								valid = false;
								String msg = ResourceBundleUtil.getValidationMessageFromBundle(CUSTOM_ATTRIBUTE_CHECK, "'"+lExpr.getName()+"'", customAttrName);
								invalidList.add(msg);
							}
						}							
					}
				}
			}
		}
		
		return valid;
	}

	@Override
	protected boolean fix(Repository r) {
		// TODO Auto-generated method stub
		return false;
	}

}
