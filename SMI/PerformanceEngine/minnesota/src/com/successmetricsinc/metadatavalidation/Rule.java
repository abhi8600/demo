package com.successmetricsinc.metadatavalidation;

import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.metadatavalidation.util.ResourceBundleUtil;

public class Rule 
{
	private String name;
	private String groupName;
	private String RuleClass;
	private boolean Enabled;
	private boolean isRepoAdmin;
	private String Description;
	private String displayname;	
	private String criticality;

	public Rule(Element element, Namespace ns, String groupName) 
	{
		name = element.getAttributeValue(RuleConstants.NAME);
		this.groupName = groupName;
		displayname = element.getAttributeValue(RuleConstants.DISPLAY_NAME);
		if (displayname!=null)
		{
			displayname = ResourceBundleUtil.getValidationLabel(displayname);					
		}
		RuleClass = element.getChildText(RuleConstants.RULE_CLASS, ns);
		criticality = element.getChildText(RuleConstants.CRITICALITY, ns);
		if (criticality!=null)
		{
			criticality = ResourceBundleUtil.getValidationLabel(criticality);					
		}
		String ruleEnabled = element.getChildText(RuleConstants.ENABLED, ns);
		if (ruleEnabled!=null && !ruleEnabled.isEmpty())
		{
			Enabled = Boolean.parseBoolean(ruleEnabled);
		}
		String isUserRepoAdmin = element.getChildText(RuleConstants.IS_REPO_ADMIN, ns);
		if (isUserRepoAdmin!=null && !isUserRepoAdmin.isEmpty())
		{
			isRepoAdmin = Boolean.parseBoolean(isUserRepoAdmin);
		}
		Description = element.getChildText(RuleConstants.DESCRIPTION, ns);
		if (Description!=null)
		{
			Description = ResourceBundleUtil.getValidationLabel(Description);					
		}
	}

	public String getCriticality() {
		return criticality;
	}
	
	public String getRuleName() 
	{
		return name;
	}
	
	public String getGroupName() {
		return groupName;
	}
	
	public String getRuleClass() 
	{
		return RuleClass;
	}
		
	public boolean isEnabled() 
	{
		return Enabled;
	}
	
	public boolean isRepoAdmin() 
	{
		return isRepoAdmin;
	}
		
	public String getName() 
	{
		return name;
	}
	
	public String getDescription() 
	{
		return Description;
	}
	
	public String getDisplayname() 
	{
		return displayname;
	}	
}