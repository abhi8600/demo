package com.successmetricsinc.metadatavalidation;

public class RuleConstants 
{	
	public static final String NAME = "name";
	public static final String DISPLAY_NAME = "displayname";
	public static final String RULE_CLASS = "RuleClass";
	public static final String ENABLED = "Enabled";
	public static final String IS_REPO_ADMIN = "isRepoAdmin";
	public static final String IS_BEST_PRACTICE = "isBestPractice";
	public static final String PRIORITY = "Priority";
	public static final String RISK = "Risk";
	public static final String DESCRIPTION = "Description";
	public static final String CRITICALITY = "Criticality";	
		
}
