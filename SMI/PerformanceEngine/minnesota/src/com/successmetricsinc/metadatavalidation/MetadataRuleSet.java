package com.successmetricsinc.metadatavalidation;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;

/**
 * @author pbindal
 * This class 
 */
public class MetadataRuleSet
{
	private static Logger logger = Logger.getLogger(MetadataRuleSet.class);
	private List<RuleGroup> ruleGroups;
	
	public MetadataRuleSet(Document d, boolean isUserRepoAdmin) throws Exception
	{
		long startTime = 0;
		long endTime = 0;
		if (logger.isDebugEnabled())
		{
			startTime = System.currentTimeMillis();
			logger.debug("Start MetadataRuleSet Xml Parsing "+startTime);
		}
		
		parseXmldoc(d, isUserRepoAdmin);
		
		if (logger.isDebugEnabled())
		{
			endTime = System.currentTimeMillis();
			logger.info("Finished MetadataRuleSet Xml Parsing "+endTime);
			logger.info("Total Time : "+(endTime-startTime) + " milliseconds ");
		}
	}
	
	/**
	 * Parse the xml and initialize the object for metadatavalidation
	 * @param d
	 * @param isUserRepoAdmin 
	 * @throws Exception
	 */
	private void parseXmldoc(Document d, boolean isUserRepoAdmin) throws Exception
	{
		Element root = d.getRootElement();
		List children = root.getChildren();
		Namespace ns = Namespace.getNamespace("http://www.successmetricsinc.com");
		
		if (children!=null && !children.isEmpty())
		{
			this.setRuleGroups(new ArrayList<RuleGroup>(children.size()));
			for (int count = 0; count < children.size(); count++)
			{
				RuleGroup ruleGroup = new RuleGroup((Element) children.get(count), ns, isUserRepoAdmin);
				if(ruleGroup.getRules() != null && ruleGroup.getRules().size() > 0){
					getRuleGroups().add(ruleGroup);
				}
			}
		}				
	}

	public List<RuleGroup> getRuleGroups()
	{
		return ruleGroups;
	}

	public void setRuleGroups(List<RuleGroup> ruleGroups)
	{
		this.ruleGroups = ruleGroups;
	}
}