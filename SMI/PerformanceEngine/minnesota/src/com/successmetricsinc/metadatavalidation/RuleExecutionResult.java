package com.successmetricsinc.metadatavalidation;

import java.util.List;

/**
 * An array of RuleExecutionResult will be returned from the service after performing the Validations
 * @author pbindal
 *
 */
public class RuleExecutionResult 
{
	private boolean executionResult;
	private String validationMessage;
	private String fixMessage;
	private String ruleName;
	private boolean isInvalid;
	private boolean fixResult;
	private List<String> errorList;
	private String criticality;
	private String ruleGroupName;
		
	public RuleExecutionResult(boolean executionResult,String validationMessage,String ruleName, List<String> errorList, String criticality,
			String ruleGrpName)
	{
		this.executionResult = executionResult;
		this.validationMessage = validationMessage;
		this.ruleName = ruleName;
		this.errorList = errorList;
		this.criticality = criticality;
		this.ruleGroupName = ruleGrpName;
	}
	
	public RuleExecutionResult(String fixMessage,String ruleName,boolean isInvalid,boolean fixResult)
	{
	  this.fixMessage = fixMessage;
	  this.isInvalid = isInvalid;
	  this.ruleName = ruleName;
	  this.fixResult = fixResult;
	}
	
	public boolean isExecutionResult()
	{
		return executionResult;
	}
	
	
	public String getValidationMessage() 
	{
		return validationMessage;
	}
	
	public void setValidationMessage(String validationMessage) 
	{
		this.validationMessage = validationMessage;
	}
	
	public String getRuleName() 
	{
		return ruleName;
	}
	
	public void setRuleName(String ruleName) 
	{
		this.ruleName = ruleName;
	}

	public void setFixed(boolean fixed) {
		this.isInvalid = fixed;
	}

	public boolean isFixResult() {
		return fixResult;
	}

	public void setFixResult(boolean fixResult) {
		this.fixResult = fixResult;
	}

	public String getFixMessage() {
		return fixMessage;
	}

	public void setFixMessage(String fixMessage) {
		this.fixMessage = fixMessage;
	}

	public boolean isInvalid() {
		return isInvalid;
	}

	public void setInvalid(boolean isInvalid) {
		this.isInvalid = isInvalid;
	}

	public List<String> getErrorList() {
		return errorList;
	}
	
	public String getCriticality() {
		return criticality;
	}

	public String getRuleGroupName() {
		return ruleGroupName;
	}
}