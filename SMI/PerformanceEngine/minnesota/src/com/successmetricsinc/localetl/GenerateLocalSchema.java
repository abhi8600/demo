package com.successmetricsinc.localetl;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;

import com.successmetricsinc.Repository;
import com.successmetricsinc.query.Database;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.DatabaseIndex;
import com.successmetricsinc.status.Status;
import com.successmetricsinc.util.XmlUtils;
import com.successmetricsinc.warehouse.GenerateSchema;

public class GenerateLocalSchema {
	private Repository r;
	public boolean generate(Repository r) throws Exception
	{
		if (r == null)
		{
			throw new Exception("Repository invalid");
		}
		this.r = r;
		GenerateSchema gs = new GenerateSchema(r);
		Status status = new Status(r, Integer.toString(1), null);
		// Don't log when executing for testing purposes
		status.setDisableLogging(true);
		gs.generate(status, false, true , null, null, null);
		return true;
	}
	
	public boolean createIndices(String indicesXMLFile) throws Exception
	{
		File xmlFile = new File(indicesXMLFile);
		XMLInputFactory inputFactory = XmlUtils.getSecureXMLInputFactory();
		XMLStreamReader parser = inputFactory.createXMLStreamReader(new FileInputStream(xmlFile));
		StAXOMBuilder builder = new StAXOMBuilder(parser);
		OMElement doc = builder.getDocumentElement();
		doc.build();
		doc.detach();
		List<DatabaseIndex> indices = new ArrayList<DatabaseIndex>();
		for (Iterator<OMElement> iter = doc.getChildElements();iter.hasNext();)
		{
			OMElement databaseIndexEl = iter.next();
			DatabaseIndex di = new DatabaseIndex();
			for (Iterator<OMElement> dis = databaseIndexEl.getChildElements();dis.hasNext();)
			{
				OMElement el = dis.next();
				if ("Name".equals(el.getLocalName()))
				{
					di.name = XmlUtils.getStringContent(el);
				}
				else if ("SchemaName".equals(el.getLocalName()))
				{
					di.schemaName = XmlUtils.getStringContent(el);
				}
				else if ("TableName".equals(el.getLocalName()))
				{
					di.tableName = XmlUtils.getStringContent(el);
				}
				else if ("Columns".equals(el.getLocalName()))
				{
					List<String> columnsList = new ArrayList<String>();
					for (Iterator<OMElement> cols = el.getChildElements();cols.hasNext();)
					{
						OMElement colEl = cols.next();
						columnsList.add(XmlUtils.getStringContent(colEl));
					}
					di.columns = columnsList.toArray(new String[0]);
				}
				else if ("IncludedColumns".equals(el.getLocalName()))
				{
					List<String> includedColumnsList = new ArrayList<String>();
					for (Iterator<OMElement> cols = el.getChildElements();cols.hasNext();)
					{
						OMElement colEl = cols.next();
						includedColumnsList.add(XmlUtils.getStringContent(colEl));
					}
					di.includedColumns = includedColumnsList.toArray(new String[0]);
				}
			}
			indices.add(di);
		}
		
		for (DatabaseIndex di : indices)
		{
			DatabaseConnection dbc = r.getDefaultConnection();
			Connection conn = null;
			Statement stmt = null;
			try
			{
				conn = dbc.ConnectionPool.getConnection();
				stmt = conn.createStatement();
				Database.createIndex(dbc, stmt, di);
			}
			finally
			{
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			}
		}
		return true;
	}
}
