package com.successmetricsinc.localetl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.successmetricsinc.CommandSession;
import com.successmetricsinc.ExecuteSteps;
import com.successmetricsinc.Repository;
import com.successmetricsinc.ExecuteSteps.Step;
import com.successmetricsinc.ExecuteSteps.StepCommand;
import com.successmetricsinc.ExecuteSteps.StepCommandType;

public class RepositoryInitializer {
	private static Logger logger = Logger.getLogger(RepositoryInitializer.class);
	public static Repository initRepository(String repositoryFileName) throws Exception
	{
		logger.debug("Initializing repository at " + repositoryFileName);
		CommandSession cmdsession = new CommandSession(true);
		List<Step> steps = new ArrayList<Step>();
		steps.add(new Step(StepCommand.Repository, StepCommandType.Init, repositoryFileName));
		ExecuteSteps es = new ExecuteSteps(steps, cmdsession);
		es.start();
		es.join();
		if (es.isError())
		{
			throw new Exception("Error initializing repository");
		}
		Repository r = cmdsession.getRepository();
		if (r == null)
		{
			throw new Exception("Repository invalid");
		}
		return r;
	}
}
