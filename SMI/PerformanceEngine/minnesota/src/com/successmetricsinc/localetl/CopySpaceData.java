package com.successmetricsinc.localetl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;

import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.query.Database;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.DatabaseIndex;

/**
 * 
 * @author mpandit
 * 
 */
public class CopySpaceData {

	private static Logger logger = Logger.getLogger(CopySpaceData.class);
	DatabaseConnection dconn;
	Repository r;
	String newSchema;
	
	public CopySpaceData(Repository r, String newSchema)
	{
		this.r = r;
		this.newSchema = newSchema;
	}
	
	public boolean copyData() throws Exception
	{
		dconn = r.getDefaultConnection();
		Connection conn = dconn.ConnectionPool.getConnection();
		Statement stmt = null;
		ResultSet rs = null;
		try
		{
			boolean isIB = DatabaseConnection.isDBTypeInfoBright(dconn.DBType);
			String schemaExistsSQL = null;
	        if (isIB)
	        {
	            schemaExistsSQL = "SELECT * FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME='" + newSchema + "'";
	        }
	        else
	        {
	            schemaExistsSQL = "SELECT * FROM SYS.SCHEMAS WHERE NAME='" + newSchema + "'";
	        }
			stmt = conn.createStatement();
			rs = stmt.executeQuery(schemaExistsSQL);
			boolean schemaExists = false;
			if (rs.next())
			{
				schemaExists = true;
			}
			if (schemaExists)
			{
				throw new Exception("Schema " + newSchema + " already exists, cannot copy space data");
			}
			String createSchemaSQL = null;
			if (isIB)
                createSchemaSQL = "CREATE DATABASE IF NOT EXISTS " + newSchema;
            else
                createSchemaSQL = "CREATE SCHEMA [" + newSchema + "] AUTHORIZATION [dbo]";
			stmt.executeUpdate(createSchemaSQL);
			logger.debug("Schema " + newSchema + " created successfully");
			HashSet<String> tnames = new HashSet<String>(Database.getTables(dconn, null, dconn.Schema, null));
			for (String table : tnames)
			{
				Database.copyTable(r, table, dconn.Schema, newSchema);
			}
			HashSet<DatabaseIndex> indices = null;
			if (!isIB)
			{
				indices = new HashSet<DatabaseIndex>(Database.getIndices(dconn, stmt, dconn.Schema, null));
				for (DatabaseIndex di : indices)
				{
					Database.copyIndex(dconn, stmt, di, newSchema);
				}						
			}
			logger.debug("Space data copied successfully to " + newSchema + " schema");
			return true;
		}
		catch (SQLException sqle)
		{
			logger.error(sqle.getMessage(), sqle);
			throw new SQLException("SQLException during CopySpaceData copyData method ", sqle);
		}
		finally
		{
			try
			{
				if (rs != null)
					rs.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			}
			catch (SQLException e)
			{
				throw e;
			}
		}		
	}
}
