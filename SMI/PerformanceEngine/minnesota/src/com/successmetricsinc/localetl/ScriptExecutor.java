package com.successmetricsinc.localetl;

import com.successmetricsinc.Repository;
import com.successmetricsinc.status.Status;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.transformation.TransformationScript.ScriptResult;

public class ScriptExecutor {
	ScriptResult sr = null;
	
	public boolean executeScript(Repository r, Integer loadNumber, String path, String databasePath, String loadGroupName, String inputQuery,
			String output, String script, Integer numRows) throws Exception
	{
		if (r == null)
		{
			throw new Exception("Repository invalid");
		}
		TransformationScript sc = new TransformationScript(r, null);
		Status status = new Status(r, Integer.toString(loadNumber), null);
		// Don't log when executing for testing purposes
		status.setDisableLogging(true);
		sc.setLoadAllBaseTables(true);//when executing script from UI, allow all base tables to be loaded even if they are already loaded
		sc.setDoNotLoadVisulationSources(true);
		sr = sc.executeScript(status, inputQuery, output, script, loadGroupName, path + "\\data", r.replaceVariables(null, databasePath), numRows, null, null, null);
		return true;		
	}
	
	public long getNumInputRows()
	{
		return sr.numInputRows;
	}
	
	public long getnumOutputRows()
	{
		return sr.numOutputRows;
	}
}
