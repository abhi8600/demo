package com.successmetricsinc.chart;

import org.apache.log4j.Logger;

/**
 * A singleton factory for the creation of ChartRenderer.
 * @author pconnolly
 */
public class ChartRendererFactory {
	
	private static Logger logger = Logger.getLogger(ChartRendererFactory.class);
	private static ChartRenderer chartRenderer = null;
	
	/**
	 * No trespassing constructor.
	 */
	private ChartRendererFactory() {
	}

	/**
	 * Creates a single instance of ChartRenderer and returns it to the requester.
	 * @param flashFilePath is the filepath for the ChartRenderer.swf file.
	 * @return the singleton instance of ChartRenderer.
	 */
	public static synchronized ChartRenderer createInstance(final String flashFilePath) {
		if (chartRenderer == null) {
			logger.debug("Creating instance of ChartRenderer.");
			chartRenderer = new ChartRenderer(flashFilePath);
		} else {
			logger.trace("Instance of ChartRenderer already exists.  Returning it.");
		}
		return chartRenderer;
	}
	
	public static void destroyInstance()
	{
		if (chartRenderer != null)
			chartRenderer.dispose();
	}
}
