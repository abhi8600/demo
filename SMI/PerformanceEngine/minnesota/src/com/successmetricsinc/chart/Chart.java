/**
 * $Id: Chart.java,v 1.208 2011-09-26 17:57:15 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.chart;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.font.FontRenderContext;
import java.awt.geom.Ellipse2D;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Types;
import java.text.AttributedString;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Map.Entry;

import net.sf.jasperreports.engine.JRDataSource;

import org.apache.log4j.Logger;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.LogarithmicAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.PieSectionLabelGenerator;
import org.jfree.chart.labels.PieToolTipGenerator;
import org.jfree.chart.labels.StandardCategoryToolTipGenerator;
import org.jfree.chart.labels.StandardXYZToolTipGenerator;
import org.jfree.chart.labels.XYItemLabelGenerator;
import org.jfree.chart.labels.XYToolTipGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.DatasetRenderingOrder;
import org.jfree.chart.plot.DialShape;
import org.jfree.chart.plot.MeterInterval;
import org.jfree.chart.plot.MeterPlot;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.SpiderWebPlot;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.plot.XYQuadrantPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.BarRenderer3D;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.renderer.category.LineRenderer3D;
import org.jfree.chart.renderer.category.StackedBarRenderer3D;
import org.jfree.chart.renderer.xy.XYBubbleRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.title.LegendTitle;
import org.jfree.chart.title.TextTitle;
import org.jfree.chart.urls.CategoryURLGenerator;
import org.jfree.chart.urls.PieURLGenerator;
import org.jfree.chart.urls.StandardCategoryURLGenerator;
import org.jfree.chart.urls.XYURLGenerator;
import org.jfree.data.Range;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.general.DefaultValueDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.data.general.ValueDataset;
import org.jfree.data.xy.SeriesXYZDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.ui.RectangleInsets;

import com.successmetricsinc.DrillPath;
import com.successmetricsinc.Repository;
import com.successmetricsinc.UserBean;
import com.successmetricsinc.chart.ChartGradientPaint.Direction;
import com.successmetricsinc.query.DimensionColumnNav;
import com.successmetricsinc.query.IJasperDataSourceProvider;
import com.successmetricsinc.query.JasperDataSourceProvider;
import com.successmetricsinc.query.QueryCancelledException;
import com.successmetricsinc.query.QueryColumn;
import com.successmetricsinc.query.QueryFilter;
import com.successmetricsinc.query.QueryResultSet;
import com.successmetricsinc.query.QueryTimeoutException;
import com.successmetricsinc.query.SingleRowDataSource;
import com.successmetricsinc.util.ColorUtils;
import com.successmetricsinc.util.DateUtil;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author bpeters
 * 
 */
public class Chart
{
	public static final String DRILL_TYPE_DRILL = "Drill Down";
	public static final String DRILL_TYPE_NONE = "None";
	public static final String DRILL_TYPE_NAVIGATE = "Drill To Dashboard";
	
	public static String CVSId = "$Id";
	private static Logger logger = Logger.getLogger(Chart.class);
	private static final double SMALL_NUMBER = 0.000001;
	
	private static final String INTERNAL_COLUMN_HEADER = "_Birst_Internal_";
	/*
	 * Jfreechart datasets are very inefficient - need to protect against large, meaningless charts with lots of
	 * categories
	 */
	public static int MAX_DATASET_SIZE = 10000;
/**
 * 
 * @param qrs
 * @return List of strings
 */
	public static List<String> getColumnNames(QueryResultSet qrs)
	{
		int col = 0;
		String[] colNames = qrs.getDisplayNames();
		List<String> colList = new ArrayList<String>(colNames.length);
		for (String s : colNames)
		{
			if (s != null)
			{
				colList.add(Util.removeSystemGeneratedFieldNumber(s));
			}
			else
			{
				colList.add(INTERNAL_COLUMN_HEADER + col);
			}
			col++;
		}
		return (colList);
	}
	
	public static String xmlify(String s) {
		if (s == null)
			return s;
		return XmlUtils.encode(s);
	}
	
	public static String encodeParamForDrilling(String s) {
		if (s == null)
			return s;
		
		try {
			s = URLEncoder.encode(s, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error(e,e);
		}

		s = s.replace("\\", "\\\\");
		return s;
	}
	
	public static void reportError(String message) 
	{
		logger.error(message);
	}

	/**
	 * fix up nulls in the dataset - map nulls and Double.NaN to 0 - not perfect, but...
	 */
	private static Object[][] fixUpRows(int[] colDataTypes, Object[][] rows)
	{
		// fix up nulls in float, double, and integer columns, map to 0
		Object[][] newRows = new Object[rows.length][colDataTypes.length];
		for (int i = 0; i < rows.length; i++)
		{
			for (int j = 0; j < colDataTypes.length; j++)
			{
				if (rows[i][j] == null)
				{
					if (colDataTypes[j] == Types.INTEGER)
					{
						newRows[i][j] = Long.valueOf(0);
					} else if (colDataTypes[j] == Types.DOUBLE)
					{
						newRows[i][j] = Double.valueOf(0);
					} else if (colDataTypes[j] == Types.FLOAT)
					{
						newRows[i][j] = Float.valueOf(0);
					} else
					{
						newRows[i][j] = rows[i][j];
					}
				} else
				{
					if (colDataTypes[j] == Types.DOUBLE && (Double.isNaN((Double) rows[i][j]) || Double.isInfinite((Double) rows[i][j])))
					{
						newRows[i][j] = Double.valueOf(0);
					} else
					{
						newRows[i][j] = rows[i][j];
					}
				}
			}
		}
		return newRows;
	}
	
	private static String formatObject(String name, Object val, Map<String,Format> formats, String colDataType)
	{
		if (val == null)
			return null;
		Format f = null;
		if (formats.containsKey(name))
			f = formats.get(name);
		try
		{
			UserBean ub = UserBean.getParentThreadUserBean();
			// try/catch because sometimes a number is not a number, a year could be a string...
			if (f != null)
			{
				if (val instanceof java.util.Calendar)
				{
					val = ((java.util.Calendar) val).getTime();
					if (ub != null)
					{
						if (colDataType != null && colDataType.equals("Date"))
							return ub.getDateFormat(((SimpleDateFormat)f).toPattern()).format(val);						
						else
							return ub.getDateTimeFormat(((SimpleDateFormat)f).toPattern()).format(val);						
					}
				}
				return f.format(val);
			}
			else
			{
				if (val instanceof java.util.Calendar)
				{
					SimpleDateFormat sdf = null;
					if (colDataType != null && colDataType.equals("Date"))
						if (ub != null)
							sdf = ub.getDateFormat();
						else
							sdf = DateUtil.getShortDateFormat(Locale.getDefault());
					else
						if (ub != null)
							sdf = ub.getDateTimeFormat();
						else
							sdf = DateUtil.getShortDateTimeFormat(Locale.getDefault());
					return sdf.format(((java.util.Calendar)val).getTime());
				}
			}
		} catch (Exception ex)
		{
		}
		return val.toString();
	}
	
	public static Map<String,Format> buildFormats(Map<String,String> formatMap, List<String> colNames, int[] colTypes, int[] colDataTypes, String defaultFormat)
	{
		Map<String, Format> formats = new HashMap<String, Format>();
		if (formatMap != null)
		{
			for (String key : formatMap.keySet())
			{
				int index = colNames.indexOf(key);
				if (index >= 0 && colTypes[index] != QueryColumn.MEASURE)
				{
					try
					{
						Format ft = null;
						String fmt = formatMap.containsKey(key) ? formatMap.get(key) : defaultFormat;
						if (colDataTypes[index] == Types.TIMESTAMP)
						{
							ft = new SimpleDateFormat(fmt);
						} else
						{
							ft = new DecimalFormat(fmt);
						}
						formats.put(key, ft);
					} catch (Exception ex)
					{
						// catch bad format strings
						logger.warn("Bad chart format string: " + formatMap.get(key) + " for " + key + " - " + ex.getMessage());
					}
				}
			}
		}
		return formats;
	}

	/**
	 * Create a category data set from a query result set
	 * 
	 * @param qrs
	 *            Query result set
	 * @param categories
	 *            List of column names of categories (must be dimension columns)
	 * @param series
	 *            List of column names of series - Can be either measures or dimension columns. Series will be the
	 *            cross-product of the measures and the dimension columns. For example, if 2 dimension columns are
	 *            chosen and two measures, then each series will be a measure plus a combination of dimension column
	 *            attribute values
	 * @param categoryFormat
	 *            Format for category strings (if they are numeric or date) - ignored if null or more than one category
	 * @param swapSeriesAndCategories
	 *            Swap series and categories in the dataset
	 * @return CategoryDataset instance
	 */
	public static BirstCategoryDataset createCategoryDataset(QueryResultSet qrs, 
														 List<String> categories, 
														 List<String> series, 
														 String categoryFormat,
														 boolean swapSeriesAndCategories, 
														 List<String> seriesGroups, 
														 Map<String, String> seriesGroupBySeries, 
														 Map<String, String> seriesNameBySeries,
														 Map<String, String> formatMap,
														 Map<String, String> aggregateMap,
														 String colorColumn)
	{
		if (qrs == null || categories == null || series == null)
			return (null);
		
		BirstCategoryDataset cds = new BirstCategoryDataset();
		List<String> colNames = getColumnNames(qrs);
		int[] colTypes = qrs.getColumnTypes();
		int[] colDataTypes = qrs.getColumnDataTypes();
		String[] rules = qrs.getAggregationRules();
		String defaultFormat = null;
		// backwards compatibility
		if (categoryFormat != null && categories.size() == 1)
		{
			int index = colNames.indexOf(categories.get(0));
			if (index >= 0 && colTypes[index] != QueryColumn.MEASURE)
			{
				defaultFormat = categoryFormat;
			}
		}
		/*
		 * set up the format map with real formats, if available
		 */
		Map<String, Format> formats = buildFormats(formatMap, colNames, colTypes, colDataTypes, defaultFormat);
		/*
		 * Fill out data set
		 */
		Object[][] rows = fixUpRows(colDataTypes, qrs.getRows());
		int colorIndex = getColumnIndex(colorColumn, qrs);
		if (colorIndex >= 0) {
			cds.categoryColorMap = new HashMap<String, Color>();
		}
			
		Map<String, Cell> cellMap = new LinkedHashMap<String, Cell>();
		int rowCount = 0;
		for (Object[] row : rows)
		{
			if (rowCount++ > MAX_DATASET_SIZE)
				break;
			StringBuilder category = new StringBuilder();
			/*
			 * Determine the category
			 */
			for (String s : categories)
			{
				int index = colNames.indexOf(s);
				if (index >= 0)
				{
					if (colTypes[index] != QueryColumn.MEASURE)
					{
						if (category.length() > 0)
							category.append('/');
						String dataType = null;
						if (qrs.getWarehouseColumnsDataTypes() != null && index < qrs.getWarehouseColumnsDataTypes().length)
							dataType = qrs.getWarehouseColumnsDataTypes()[index];
						category.append(formatObject(s, row[index], formats, dataType));
					}
				}
			}
			StringBuilder dcseries = new StringBuilder();
			/*
			 * Determine the dimension column part of series
			 */
			for (String s : series)
			{
				int index = colNames.indexOf(s);
				if (index >= 0)
				{
					if (colTypes[index] != QueryColumn.MEASURE)
					{
						if (dcseries.length() > 0)
							dcseries.append('/');
						String dataType = null;
						if (qrs.getWarehouseColumnsDataTypes() != null && index < qrs.getWarehouseColumnsDataTypes().length)
							dataType = qrs.getWarehouseColumnsDataTypes()[index];
						dcseries.append(formatObject(s, row[index], formats, dataType));
					}
				}
			}
			/*
			 * Now add values for each series entry (measure)
			 */
			try {
			for (String sg : seriesGroups)
			{
				for (String s : series)
				{
					String seriesName = seriesNameBySeries.get(s);
					String seriesGroupName = seriesGroupBySeries.get(s);
					if (seriesGroupName == null)
					{
						seriesGroupName = "NO_SERIES_GROUP";
					}
					if (!sg.equals(seriesGroupName))
					{
						continue;
					}
					int index = colNames.indexOf(s);
					if (index >= 0)
					{
						if (colTypes[index] == QueryColumn.MEASURE)
						{
							String seriesStr = null;
							if (dcseries.length() > 0)
								seriesStr = dcseries.toString();
							else
								seriesStr = colNames.get(index);
							seriesStr = Util.replaceStr(seriesStr, "_EXPR", "");
							if (seriesName != null)
							{
								seriesStr = seriesName;
							}
							String categoryName = category.toString();
							if (!seriesGroupName.equals("NO_SERIES_GROUP"))
							{
								if (categoryName.length() > 0)
								{
									categoryName = categoryName + "/" + seriesGroupName;
								} else
								{
									categoryName = seriesGroupName;
								}
							}
							String rowStr = swapSeriesAndCategories ? categoryName : seriesStr;
							String colStr = swapSeriesAndCategories ? seriesStr : categoryName;
							
							// bug 7318
							rowStr = rowStr + "|" + s;
							
							String key = rowStr + "|" + colStr;
							Cell c = cellMap.get(key);
							if (c == null)
							{
								c = new Cell();
								c.rowKey = rowStr;
								c.colKey = colStr;
								c.value = (Number) row[index];
								c.count = 1;
								if (colorIndex >= 0) {
									c.color = row[colorIndex].toString();
								}
								cellMap.put(key, c);
							} else
							{
								String rule = rules[index];
								if (aggregateMap.containsKey(s))
									rule = aggregateMap.get(s);
								if (row[index].getClass() == Double.class)
								{
									c.value = (double) aggregate(rule, (Double) c.value, (Double) row[index]);
									c.count++;
									c.aggRule = rule;
								} else if (row[index].getClass() == Integer.class)
								{
									c.value = (int) aggregate(rule, (Integer) c.value, (Integer) row[index]);
									c.count++;
									c.aggRule = rule;
								}
							}
						}
					}
				}
			}
			} catch (ClassCastException e){
				logger.error("Class cast exception: " + e.getMessage());
				reportError("The chart cannot be generated because some of the attributes and/or measures have been defined as the wrong type. \n Please edit using Chart Options and try again.\n");
				return cds;	
			}
			for (Cell c : cellMap.values())
			{
				cds.addValue(c.value, c.rowKey, c.colKey);
				if (c.color != null) {
					Color clr = ColorUtils.stringToColor(c.color);
					if (clr != null)
						cds.categoryColorMap.put(c.colKey, clr);
				}
			}
		}
		for (Cell c : cellMap.values())
		{
			if (c.aggRule != null && c.aggRule.equals("AVG")) {
				if (c.value instanceof Double) {
					c.value = (Double)c.value / c.count;
				}
				else if (c.value instanceof Integer) {
					c.value = (Integer)c.value / c.count;
				}
				else if(c.value instanceof Long) {
					c.value = (Long)c.value / c.count;
				}
				cds.addValue(c.value, c.rowKey, c.colKey);
			}
		}
		return (cds);
	}

	public static int getColumnIndex(String colorColumn, QueryResultSet qrs) {
		int colorIndex = -1;
		if (colorColumn != null && Util.hasNonWhiteSpaceCharacters(colorColumn)) {
			String[] displayNames = qrs.getDisplayNames();
			for (int i = 0; i < displayNames.length; i++) {
				if (colorColumn.equals(displayNames[i])) {
					colorIndex = i;
					break;
				}
			}
			if (colorIndex < 0) {
				// try column names
				String[] columnNames = qrs.getColumnNames();
				for (int i = 0; i < columnNames.length; i++) {
					if (colorColumn.equals(columnNames[i])) {
						colorIndex = i;
						break;
					}
				}
			}
		}
		return colorIndex;
	}
	
	private static class Cell
	{
		String rowKey;
		String colKey;
		Number value;
		String color;
		int count;
		String aggRule;
	}

	/**
	 * Create an XY data set from a query result set
	 * 
	 * @param qrs
	 *            Query result set
	 * @param categories
	 *            List of column names of categories (must be dimension columns) - each will define a category
	 * @param series
	 *            List of column names of series - Can be either measures or dimension columns. XY Dataset will be built
	 *            using the first 2 measures in the series
	 * @return XYDataSet instance
	 */
	private static BirstXYSeriesCollection createXYDataset(QueryResultSet qrs,
											List<String> categories, 
											List<String> series, 
											Map<String,String> formatMap, 
											String categoryFormat,
											String colorColumn)
	{
		if (qrs == null || categories == null || series == null || series.size() < 2)
			return (null);
		List<String> colNames = getColumnNames(qrs);
		int[] colDataTypes = qrs.getColumnDataTypes();
		int[] colTypes = qrs.getColumnTypes();
		String defaultFormat = null;
		// backwards compatibility
		if (categoryFormat != null && categories.size() == 1)
		{
			int index = colNames.indexOf(categories.get(0));
			if (index >= 0 && colTypes[index] != QueryColumn.MEASURE)
			{
				defaultFormat = categoryFormat;
			}
		}
		/*
		 * set up the format map with real formats, if available
		 */
		Map<String, Format> formats = buildFormats(formatMap, colNames, colTypes, colDataTypes, defaultFormat);
				
		
		/*
		 * Create the individual XY Series
		 */
		HashMap<String, XYSeries> seriesMap = new HashMap<String, XYSeries>();
		BirstXYSeriesCollection xyds = new BirstXYSeriesCollection();
		int colorIndex = getColumnIndex(colorColumn, qrs);
		if (colorIndex >= 0) {
			xyds.categoryColorMap = new HashMap<String, Color>();
		}

		/*
		 * Find the x and y columns
		 */
		int x = -1;
		int y = -1;
		for (int i = 0; i < colNames.size(); i++)
			if (colNames.get(i).equals(series.get(0)))
				x = i;
		for (int i = 0; i < colNames.size(); i++)
			if (colNames.get(i).equals(series.get(1)))
				y = i;
		if (x < 0 || y < 0)
			return (null);
		if (qrs.getColumnTypes()[x] != QueryColumn.MEASURE)
			return (null);
		if (qrs.getColumnTypes()[y] != QueryColumn.MEASURE)
			return (null);
		/*
		 * Determine series names from categories and fill out data
		 */
		Object[][] rows = fixUpRows(colDataTypes, qrs.getRows());
		int rowCount = 0;
		for (Object[] row : rows)
		{
			if (rowCount++ > MAX_DATASET_SIZE)
				break;
			XYSeries xys = null;
			/*
			 * Figure out the series
			 */
			StringBuilder cname = new StringBuilder();
			for (String category : categories)
			{
				for (int i = 0; i < colNames.size(); i++)
				{
					if (colNames.get(i).equals(category))
						if (qrs.getColumnTypes()[i] == QueryColumn.DIMENSION && (categories.indexOf(colNames.get(i)) >= 0))
						{
							if (cname.length() > 0)
								cname.append('/');
							// cname.append(colNames.get(i));
							// cname.append('=');
							String dataType = null;
							if (qrs.getWarehouseColumnsDataTypes() != null && i < qrs.getWarehouseColumnsDataTypes().length)
								dataType = qrs.getWarehouseColumnsDataTypes()[i];
							cname.append(formatObject(category, row[i], formats, dataType));
						}
				}
			}
			String cnamestr = cname.toString();
			if (!seriesMap.containsKey(cnamestr))
			{
				xys = new XYSeries(cnamestr, true, true);
				seriesMap.put(cnamestr, xys);
				xyds.addSeries(xys);
				if (colorIndex >= 0 && colorIndex < qrs.getColumnTypes().length) {
					String color = row[colorIndex].toString();
					if (color != null && Util.hasNonWhiteSpaceCharacters(color)) {
						Color clr = ColorUtils.stringToColor(color);
						if (clr != null)
							xyds.categoryColorMap.put(cnamestr, clr);
					}
				}
			}
			xys = seriesMap.get(cnamestr);
			/*
			 * Now, get the x and y
			 */
			xys.add((Number) row[x], (Number) row[y]);
		}
		return (xyds);
	}


	/**
	 * Create an contour data set from a query result set - only considers series
	 * 
	 * @param qrs
	 *            Query result set
	 * @param series
	 *            Requires 3 series and takes them in order for the X, Y and Z values
	 * @return
	 */
	private static BirstSeriesXYZDataset createSeriesXYZDataset(QueryResultSet qrs, 
														   List<String> categories, 
														   List<String> series, 
														   Map<String, String> formatMap, 
														   String categoryFormat,
														   String colorColumn)
	{
		if (qrs == null || series == null || series.size() < 3)
			return (null);
		List<String> colNames = getColumnNames(qrs);
		int[] colDataTypes = qrs.getColumnDataTypes();
		int[] colTypes = qrs.getColumnTypes();
		String defaultFormat = null;
		// backwards compatibility
		if (categoryFormat != null && categories.size() == 1)
		{
			int index = colNames.indexOf(categories.get(0));
			if (index >= 0 && colTypes[index] != QueryColumn.MEASURE)
			{
				defaultFormat = categoryFormat;
			}
		}
		/*
		 * set up the format map with real formats, if available
		 */
		Map<String, Format> formats = buildFormats(formatMap, colNames, colTypes, colDataTypes, defaultFormat);
		
		/*
		 * Find the x, y and z columns
		 */
		int x = -1;
		int y = -1;
		int z = -1;
		for (int i = 0; i < colNames.size(); i++)
			if (colNames.get(i).equals(series.get(0)))
				x = i;
		for (int i = 0; i < colNames.size(); i++)
			if (colNames.get(i).equals(series.get(1)))
				y = i;
		for (int i = 0; i < colNames.size(); i++)
			if (colNames.get(i).equals(series.get(2)))
				z = i;
		if (x < 0 || y < 0 || z < 0)
			return (null);
		if (qrs.getColumnTypes()[x] != QueryColumn.MEASURE)
			return (null);
		if (qrs.getColumnTypes()[y] != QueryColumn.MEASURE)
			return (null);
		if (qrs.getColumnTypes()[z] != QueryColumn.MEASURE)
			return (null);
		/*
		 * Identify the series names
		 */
		Object[][] rows = fixUpRows(colDataTypes, qrs.getRows());
		List<Integer> catList = new ArrayList<Integer>();
		for (int i = 0; i < colNames.size(); i++)
			if (categories.contains(colNames.get(i)))
			{
				catList.add(i);
			}
		/*
		 * Fill out data
		 */
		List<String> nameList = new ArrayList<String>();
		if (catList.size() == 0)
			nameList.add(series.get(2));
		else
			for (int i = 0; i < rows.length; i++)
			{
				StringBuilder sb = new StringBuilder();
				for (int index : catList)
				{
					if (sb.length() > 0)
						sb.append("/");
					String dataType = null;
					if (qrs.getWarehouseColumnsDataTypes() != null && index < qrs.getWarehouseColumnsDataTypes().length)
						dataType = qrs.getWarehouseColumnsDataTypes()[index];
					sb.append(formatObject(colNames.get(index), rows[i][index], formats, dataType));
				}
				String name = sb.toString();
				int index = nameList.indexOf(name);
				if (index < 0)
				{
					index = nameList.size();
					nameList.add(name);
				}
			}
		BirstSeriesXYZDataset xyzds = new BirstSeriesXYZDataset();
		int colorIndex = getColumnIndex(colorColumn, qrs);
		if (colorIndex >= 0) {
			xyzds.categoryColorMap = new HashMap<String, Color>();
		}
		for (String name : nameList)
		{
			List<Double> xvals = new ArrayList<Double>();
			List<Double> yvals = new ArrayList<Double>();
			List<Double> zvals = new ArrayList<Double>();
			for (int i = 0; i < rows.length; i++)
			{
				StringBuilder sb = new StringBuilder();
				boolean found = false;
				if (catList.size() > 0)
				{
					for (int j : catList)
					{
						if (sb.length() > 0)
							sb.append("/");
						String dataType = null;
						if (qrs.getWarehouseColumnsDataTypes() != null && i < qrs.getWarehouseColumnsDataTypes().length)
							dataType = qrs.getWarehouseColumnsDataTypes()[i];
						sb.append(formatObject(colNames.get(j), rows[i][j], formats, dataType));
					}
					String rowName = sb.toString();
					found = rowName.equals(name);
				} else
					found = true;
				if (found)
				{
					double xv = Double.NaN;
					double yv = Double.NaN;
					double zv = Double.NaN;
					if (Double.class.isInstance(rows[i][x]))
						xv = (Double) rows[i][x];
					else if (Integer.class.isInstance(rows[i][x]))
						xv = ((Integer) rows[i][x]).doubleValue();
					else if (Long.class.isInstance(rows[i][x]))
						xv = ((Long) rows[i][x]).doubleValue();
					
					if (Double.class.isInstance(rows[i][y]))
						yv = (Double) rows[i][y];
					else if (Integer.class.isInstance(rows[i][y]))
						yv = ((Integer) rows[i][y]).doubleValue();
					else if (Long.class.isInstance(rows[i][y]))
						yv = ((Long) rows[i][y]).doubleValue();
					
					if (Double.class.isInstance(rows[i][z]))
						zv = (Double) rows[i][z];
					else if (Integer.class.isInstance(rows[i][z]))
						zv = ((Integer) rows[i][z]).doubleValue();
					else if (Long.class.isInstance(rows[i][z]))
						zv = ((Long) rows[i][z]).doubleValue();
					
					xvals.add(xv);
					yvals.add(yv);
					zvals.add(zv);
					if (colorIndex >= 0 && colorIndex < qrs.getColumnTypes().length
							&& ! xyzds.categoryColorMap.containsKey(name)) {
						String color = rows[i][colorIndex].toString();
						if (color != null && Util.hasNonWhiteSpaceCharacters(color)) {
							Color clr = ColorUtils.stringToColor(color);
							if (clr != null)
								xyzds.categoryColorMap.put(name, clr);
						}
					}
				}
			}
			Double[] xvalarr = new Double[xvals.size()];
			Double[] yvalarr = new Double[yvals.size()];
			Double[] zvalarr = new Double[zvals.size()];
			xvals.toArray(xvalarr);
			yvals.toArray(yvalarr);
			zvals.toArray(zvalarr);
			xyzds.addSeries(name, xvalarr, yvalarr, zvalarr);
		}
		return (xyzds);
	}

	/**
	 * Create a category data set from a query result set
	 * 
	 * @param qrs
	 *            Query result set
	 * @param categories
	 *            List of column names of categories (must be dimension columns)
	 * @param series
	 *            List of column names of series - Can be either measures or dimension columns. Series will be the
	 *            cross-product of the measures and the dimension columns. For example, if 2 dimension columns are
	 *            chosen and two measures, then each series will be a measure plus a combination of dimension column
	 *            attribute values
	 * @param percent
	 *            (Whether to normalize numbers to 100%)
	 * @return
	 */
	private static BirstPieDataset createPieDataset(QueryResultSet qrs, 
												     List<String> categories,
												     List<String> series, 
												     boolean percent,
												     Map<String,String> formatMap,
												     Map<String,String> aggregateMap,
												     String colorColumn)
	{
		if (qrs == null || categories == null || series == null)
			return (null);
		int[] colDataTypes = qrs.getColumnDataTypes();
		Object[][] rows = fixUpRows(colDataTypes, qrs.getRows());
		if (series.size() == 0 && rows.length > 1)
			return (null);
		BirstPieDataset pds = new BirstPieDataset();
		int colorIndex = getColumnIndex(colorColumn, qrs);
		if (colorIndex >= 0) {
			pds.categoryColorMap = new HashMap<String, Color>();
		}
		List<String> colNames = getColumnNames(qrs);
		int[] colTypes = qrs.getColumnTypes();
		String[] rules = qrs.getAggregationRules();
		/* If only one row, then take from each measure in the row */
		double total = 0;
		/*
		 * Fill out data set
		 */
		if (percent)
		{
			if (rows.length == 1 && moreThanOneMeasureMappedToSeries(colNames, series))
			{
				for (int i = 0; i < colTypes.length; i++)
				{
					if (colTypes[i] == QueryColumn.MEASURE && !colNames.get(i).startsWith(INTERNAL_COLUMN_HEADER))
					{
						Object[] row = rows[0];
						if (row[i].getClass() == Double.class)
						{
							double val = ((Double) row[i]).doubleValue();
							total += Math.abs(val);
						} else if (row[i].getClass() == Integer.class)
						{
							double val = ((Integer) row[i]).doubleValue();
							total += Math.abs(val);
						}
					}
				}
			} else
			{
				for (Object[] row : rows)
				{
					/*
					 * Just take the first series
					 */
					int index = colNames.indexOf(series.get(0));
					if (index >= 0)
					{
						if (colTypes[index] == QueryColumn.MEASURE)
						{
							if (row[index].getClass() == Double.class)
							{
								double val = ((Double) row[index]).doubleValue();
								total += Math.abs(val);
							} else if (row[index].getClass() == Integer.class)
							{
								double val = ((Integer) row[index]).doubleValue();
								total += Math.abs(val);
							}
						}
					}
				}
			}
		}
		if (rows.length == 1 && moreThanOneMeasureMappedToSeries(colNames, series))
		{
			for (int i = 0; i < colTypes.length; i++)
			{
				if (colTypes[i] == QueryColumn.MEASURE && !colNames.get(i).startsWith(INTERNAL_COLUMN_HEADER))
				{
					Object[] row = rows[0];
					double val = 0;
					if (row[i].getClass() == Double.class)
						val = ((Double) row[i]).doubleValue();
					else if (row[i].getClass() == Integer.class)
						val = ((Integer) row[i]).doubleValue();
					if (percent && (total != 0))
					{
						double absVal = Math.abs(val);
						val = absVal / total;
					}
					pds.setValue(colNames.get(i), Math.abs(val) + ((percent ? (double) 1.0 : total) * SMALL_NUMBER));
				}
			}
		} else
		{
			/*
			 * set up the format map with real formats, if available
			 */
			Map<String, Format> formats = buildFormats(formatMap, colNames, colTypes, colDataTypes, null);

			Map<String, Double> valmap = new LinkedHashMap<String, Double>();
			for (Object[] row : rows)
			{
				StringBuilder category = new StringBuilder();
				/*
				 * Determine the category
				 */
				for (String s : categories)
				{
					int index = colNames.indexOf(s);
					if (index >= 0)
					{
						if (colTypes[index] != QueryColumn.MEASURE)
						{
							if (category.length() > 0)
								category.append('/');
							String dataType = null;
							if (qrs.getWarehouseColumnsDataTypes() != null && index < qrs.getWarehouseColumnsDataTypes().length)
								dataType = qrs.getWarehouseColumnsDataTypes()[index];
							category.append(formatObject(s, row[index], formats, dataType));
						}
					}
				}
				String cat = category.toString();
				if (colorIndex >= 0 && ! pds.categoryColorMap.containsKey(cat)) {
					String color = row[colorIndex].toString();
					if (color != null && Util.hasNonWhiteSpaceCharacters(color)) {
						Color clr = ColorUtils.stringToColor(color);
						if (clr != null)
							pds.categoryColorMap.put(cat, clr);
					}
				}
				/*
				 * Just take the first series
				 */
				int index = colNames.indexOf(series.get(0));
				if (index >= 0)
				{
					if (colTypes[index] == QueryColumn.MEASURE)
					{
						double val = 0;
						if (row[index].getClass() == Double.class)
							val = ((Double) row[index]).doubleValue();
						else if (row[index].getClass() == Integer.class)
							val = ((Integer) row[index]).doubleValue();
						if (percent && (total != 0))
						{
							double absVal = Math.abs(val);
							val = absVal / total;
						}
						Double curVal = valmap.get(cat);
						if (curVal == null)
							valmap.put(cat, val);
						else {
							String rule = rules[index];
							if (aggregateMap.containsKey(series.get(0)))
								rule = aggregateMap.get(series.get(0));
							valmap.put(cat, aggregate(rule, curVal, val));
						}
					}
				}
			}
			for (Entry<String, Double> en : valmap.entrySet()){
				pds.setValue(en.getKey(), Math.abs(en.getValue()) + ((percent ? (double) 1.0 : total) * SMALL_NUMBER));
			}
		}
		return (pds);
	}

	/**
	 * handle special case of pie data set mapping - 1 row with multiple measures
	 * @param colNames
	 * @param series
	 * @return
	 */
	private static boolean moreThanOneMeasureMappedToSeries(List<String> colNames, List<String> series)
	{
		int count = 0;
		for (String col : colNames)
		{
			if (series.contains(col))
				count++;
			if (count > 1)
				return true;
		}
		return false;
	}

	/**
	 * Do running aggregation. For non-additive rules, just keep current value
	 * 
	 * @param rule
	 * @param curVal
	 * @param newVal
	 * @return
	 */
	public static double aggregate(String rule, double curVal, double newVal)
	{
		if (rule != null)
		{
			if (rule.equals("SUM") || rule.equals("COUNT") || rule.equals("AVG"))
				return (curVal + newVal);
			if (rule.equals("MIN"))
				return (Math.min(curVal, newVal));
			if (rule.equals("MAX"))
				return (Math.max(curVal, newVal));
		}
		return (curVal);
	}

	/**
	 * Create a value data set. Chose the first measure in the first row
	 * 
	 * @param qrs
	 *            Query result set
	 * @return
	 */
	private static ValueDataset createValueDataset(String series, QueryResultSet qrs)
	{
		if (qrs == null || qrs.numMeasures() == 0 || qrs.numRows() == 0)
			return null;
		
		int[] colDataTypes = qrs.getColumnDataTypes();
		DefaultValueDataset vds = new DefaultValueDataset();
		int[] types = qrs.getColumnTypes();
		Object[][] rows = fixUpRows(colDataTypes, qrs.getRows());
		
		List<String> colNames = getColumnNames(qrs);
		for (int index = 0; index < colNames.size(); index++)
		{
			if (series.equals(colNames.get(index)))
			{
				if (types[index] == QueryColumn.MEASURE || types[index] == QueryColumn.CONSTANT_OR_FORMULA)
				{
					Object val = rows[0][index];
					vds.setValue((Number) val);
					return vds;
				}
				else
				{
					return null;
				}
			}
		}
		return null;
	}

	/**
	 * Get the number format for a value data set
	 * 
	 * @param qrs
	 *            Query result set
	 * @return
	 */
	private static String getValueFormat(QueryResultSet qrs, ChartOptions co)
	{
		if (qrs == null || qrs.numMeasures() == 0 || qrs.numRows() == 0)
			return null;
		int[] types = qrs.getColumnTypes();
		String[] formats = qrs.getColumnFormats();
		List<String> names = getColumnNames(qrs);
		Map<String, String> formatMap = co.getFormatMap();
		int i = 0;
		for (String name : names)
		{
			if (types[i] == QueryColumn.MEASURE)
			{
				String format = formats[i];
				if (formatMap != null && formatMap.containsKey(name))
					format = formatMap.get(name);
				return format;
			}
			i++;
		}
		return null;
	}

	/**
	 * Set standard chart options
	 * 
	 * @param chart
	 * @param co
	 */
	private static void setChartOptions(JFreeChart chart, ChartOptions co)
	{
		Font titleFont = co.getTitleFont();
		if (titleFont != null)
		{
			TextTitle tt = chart.getTitle();
			if (tt == null)
			{
				tt = new TextTitle();
				chart.setTitle(tt);
			}
			tt.setFont(titleFont);
			chart.setTitle(tt);
		}
		if (co.title != null)
		{
			TextTitle tt = chart.getTitle();
			if (tt == null)
			{
				tt = new TextTitle();
			}
			tt.setText(co.title);
			if (titleFont != null)
			{
				tt.setFont(titleFont);
			}
			chart.setTitle(tt);
		}
		if (co.legend)
		{
			LegendTitle lt = chart.getLegend();
			if (lt != null)
			{
				Font legendFont = co.getLegendFont();
				Font labelFont = co.getLabelFont();
				if (legendFont != null)
				{
					lt.setItemFont(legendFont);
				} else if (labelFont != null)
				{
					lt.setItemFont(labelFont);
				}
				lt.setPosition(co.legendAnchor);
			}
		}
		chart.setBackgroundPaint(Color.WHITE);
	}

	private static void setCategoryColors(Plot p, ChartOptions co)
	{
		ChartColors cc = new ChartColors(1, 0, null);
		if (co.chartColors != null)
		{
			Color[] colorArr = new Color[co.chartColors.size()];
			co.chartColors.toArray(colorArr);
			cc.setColors(colorArr);
		}
		if (co.gradient != null)
		{
			cc.setGradient(true);
			boolean threed = co.gradient.equals("3D");
			boolean plotHorizontal = ((CategoryPlot) p).getOrientation() != PlotOrientation.HORIZONTAL;
			cc.setThreeD(threed);
			boolean horizontal = threed ? !plotHorizontal : plotHorizontal;
			cc.setDirection(horizontal ? Direction.horizontal : Direction.vertical);
		}
		p.setDrawingSupplier(cc);
	}

	/**
	 * Set standards options for category-driven charts
	 * 
	 * @param chart
	 * @param co
	 * @param cds
	 */
	private static void setCategoryOptions(boolean stacked, 
										   JFreeChart chart, 
										   ChartOptions co, 
										   CategoryDataset cds, 
										   QueryResultSet qrs,
										   boolean series2)
	{
		setChartOptions(chart, co);
		CategoryPlot p = (CategoryPlot) chart.getPlot();
		setCategoryColors(p, co);
		CategoryAxis cax = p.getDomainAxis();
		if (co.domainLabels)
		{
			if (co.domainLabel != null)
				cax.setLabel(co.domainLabel);
			else
			{
				StringBuilder categoryLabel = new StringBuilder();
				boolean notFirst = false;
				for (int k = 0; k < co.categories.size(); k++)
				{
					if (notFirst)
						categoryLabel.append('/');
					else
						notFirst = true;
					String lbl = co.categories.get(k);
					categoryLabel.append(lbl);
				}
				cax.setLabel(categoryLabel.toString());
			}
		}
		cax.setMaximumCategoryLabelLines(3);
		cax.setTickMarkOutsideLength(0);
		cax.setLowerMargin(co.lowerMargin);
		cax.setUpperMargin(co.upperMargin);
		if (co.categoryGapWidth >= 0)
		{
			cax.setCategoryMargin(co.categoryGapWidth);
		}
		cax.setLabelInsets(new RectangleInsets(0, 0, 0, 0));
		if ((Math.max(cds.getRowCount(), cds.getColumnCount()) > 12) && co.vertical)
		{
			cax.setCategoryLabelPositions(CategoryLabelPositions.createDownRotationLabelPositions(Math.PI / 2));
		}
		if (co.categoryLabelAngle > 0)
		{
			cax.setCategoryLabelPositions(CategoryLabelPositions.createUpRotationLabelPositions(Math.PI * 2 * (co.categoryLabelAngle / 360)));
		}
		cax.setTickLabelsVisible(co.domainTickMarkLabels);
		cax.setTickMarksVisible(co.tickmarksVisible);
		cax.setMaximumCategoryLabelWidthRatio(0.85f);
		if (co.threeD)
		{
			BarRenderer3D cir;
			if (stacked)
			{
				cir = new StackedBarRenderer3D(3, 3);
			} else
			{
				cir = new BarRenderer3D(3, 3);
			}
			cir.setItemMargin(co.gapWidth);
			p.setRenderer(cir);
		}
		if (co.rangeBottomOrLeft)
		{
			p.setRangeAxisLocation(AxisLocation.BOTTOM_OR_LEFT);
		} else
		{
			p.setRangeAxisLocation(AxisLocation.TOP_OR_RIGHT);
		}
		if (co.logarithmic)
		{
			LogarithmicAxis la = new LogarithmicAxis("");
			Range ra = new Range(1, 10);
			la.setRange(ra);
			p.setRangeAxis(la);
			p.setRangeGridlinesVisible(false);
		}
		NumberAxis va = (NumberAxis) p.getRangeAxis();
		va.setTickLabelsVisible(co.rangeTickMarkLabels);
		va.setTickMarksVisible(co.tickmarksVisible);
		if (co.rangeLabels)
		{
			if (co.rangeLabel != null)
				va.setLabel(co.rangeLabel);
			else
			{
				List<String> colNames = getColumnNames(qrs);
				StringBuilder rangeStr = new StringBuilder();
				for (String s : series2 ? co.series2 : co.series)
				{
					int index = colNames.indexOf(s);
					if (index >= 0)
					{
						if (qrs.getColumnTypes()[index] == QueryColumn.MEASURE)
						{
							if (rangeStr.length() > 0)
								rangeStr.append(" / ");
							String lbl = colNames.get(index);
							rangeStr.append(lbl);
						}
					}
				}
				va.setLabel(rangeStr.toString());
			}
		}
		Font labelFont = co.getLabelFont();
		if (labelFont != null)
		{
			cax.setLabelFont(labelFont);
			cax.setTickLabelFont(labelFont);
			va.setLabelFont(labelFont);
			va.setTickLabelFont(labelFont);
		}
		double min = Double.MAX_VALUE;
		double max = Double.MIN_VALUE;
		if (co.scaleRangeToData)
		{
			for (int i = 0; i < cds.getRowCount(); i++)
				for (int j = 0; j < cds.getColumnCount(); j++)
				{
					if (cds.getValue(i, j) != null)
					{
						double val = cds.getValue(i, j).doubleValue();
						if (val < min)
							min = val;
						if (val > max)
							max = val;
					}
				}
			va.setLowerBound(min > 0 ? min * 0.975 : min * 1.025);
			va.setUpperBound(max > 0 ? max * 1.025 : max * 0.975);
		}
		if (co.valueMin != null && co.valueMax != null && !co.valueMin.equals(co.valueMax))
		{
			va.setRange(co.valueMin, co.valueMax);
		}
		if (co.tickmarkFormat != null)
		{
			DecimalFormat df = new DecimalFormat(co.tickmarkFormat);
			va.setNumberFormatOverride(df);
			if ((co.tickmarkFormat.indexOf(';') >= 0) && (co.tickmarkFormat.indexOf('-') < 0))
				df.setNegativePrefix("");
		} else
		{
			if (co.tickmarkFormatColumn != null)
			{
				int[] colDataTypes = qrs.getColumnDataTypes();
				Object[][] rows = fixUpRows(colDataTypes, qrs.getRows());
				String[] colNames = qrs.getColumnNames();
				for (int j = 0; j < rows.length; j++)
				{
					Object[] row = (Object[]) rows[j];
					for (int k = 0; k < colNames.length; k++)
					{
						if (co.tickmarkFormatColumn.equals(colNames[k]))
						{
							String tickmarkFormat = row[k].toString();
							if (tickmarkFormat != null)
							{
								DecimalFormat df = new DecimalFormat(tickmarkFormat);
								va.setNumberFormatOverride(df);
								if ((tickmarkFormat.indexOf(';') >= 0) && (tickmarkFormat.indexOf('-') < 0))
									df.setNegativePrefix("");
								break;
							}
						}
					}
				}
			}
			if ((co.tickmarkFormat == null) && (co.tickmarkFormatColumn == null))
			{
				Map<String, String> formatMap = co.getFormatMap();
				String[] colNames = qrs.getColumnNames();
				String[] formats = qrs.getColumnFormats();
				for (int k = 0; k < colNames.length; k++)
				{
					List<String> series = series2 ? co.series2 : co.series;
					if (series.contains(colNames[k]) && !qrs.getTableNames()[k].equals("EXPR"))
					{
						String format = formats[k];
						String name = Util.removeSystemGeneratedFieldNumber(colNames[k]);
						if (formatMap != null && formatMap.containsKey(name))
							format = formatMap.get(name);
						DecimalFormat df = new DecimalFormat(format);
						va.setNumberFormatOverride(df);
						if ((format.indexOf(';') >= 0) && (format.indexOf('-') < 0))
							df.setNegativePrefix("");
						break;
					}
				}
			}
		}
		/*
		 * If this plot is horizontal, reserve a little extra space on the right hand side because JFreeChart will clip
		 * the label of the farthest right tickmark (somewhat of a JFreeChart bug)
		 */
		if (p.getOrientation() == PlotOrientation.HORIZONTAL)
		{
			Font cf = cax.getLabelFont();
			FontRenderContext frc = new FontRenderContext(null, false, false);
			int width = (int) cf.getStringBounds("100,000,000", frc).getWidth() / 2;
			p.setInsets(new RectangleInsets(0, 0, 0, width));
		}
	}

	/**
	 * Set standard options for XY charts
	 * 
	 * @param chart
	 * @param co
	 * @param xyds
	 */
	private static void setXYOptions(JFreeChart chart, ChartOptions co, XYDataset xyds)
	{
		setChartOptions(chart, co);
		XYPlot p = (XYPlot) chart.getPlot();
		XYItemRenderer renderer = p.getRenderer();
		ChartColors cc = new ChartColors(1, 0, null);
		if (co.chartColors != null)
		{
			Color[] colorArr = new Color[co.chartColors.size()];
			co.chartColors.toArray(colorArr);
			cc.setColors(colorArr);
		}
		for (int i = 0; i < xyds.getSeriesCount(); i++)
		{
			renderer.setSeriesPaint(i, cc.getNextPaint());
			renderer.setSeriesShape(i, new Ellipse2D.Double(-3, -3, 6, 6));
		}
		NumberAxis ra = (NumberAxis) p.getRangeAxis();
		NumberAxis da = (NumberAxis) p.getDomainAxis();
		Font labelFont = co.getLabelFont();
		if (labelFont != null)
		{
			renderer.setBaseItemLabelFont(labelFont);
			da.setTickLabelFont(labelFont);
			ra.setTickLabelFont(labelFont);
			da.setLabelFont(labelFont);
			ra.setLabelFont(labelFont);
		}
		da.setTickLabelsVisible(co.domainTickMarkLabels);
		da.setTickMarksVisible(co.tickmarksVisible);
		ra.setTickLabelsVisible(co.rangeTickMarkLabels);
		ra.setTickMarksVisible(co.tickmarksVisible);
		if (co.logarithmic)
		{
			LogarithmicAxis la = new LogarithmicAxis("");
			la.setAutoRange(true);
			ra = la;
			p.setRangeAxis(la);
			la = new LogarithmicAxis("");
			la.setAutoRange(true);
			da = la;
			p.setDomainAxis(la);
		}
		if (co.rangeLabels)
		{
			if (co.domainLabel != null)
				da.setLabel(co.domainLabel);
			else
				da.setLabel(co.series.get(0));
			if (co.rangeLabel != null)
				ra.setLabel(co.rangeLabel);
			else
				ra.setLabel(co.series.get(1));
		}
	}

	/**
	 * Set a chart's title based on the value of a given column (as specified in the chart options)
	 * 
	 * @param chart
	 * @param qrs
	 * @param co
	 */
	private static void setTitleFromColumn(JFreeChart chart, QueryResultSet qrs, ChartOptions co)
	{
		if (co.titleColumn != null)
		{
			int index = 0;
			for (; index < qrs.getDisplayNames().length; index++)
			{
				if (qrs.getDisplayNames()[index].equals(co.titleColumn))
					break;
			}
			if (index < qrs.getDisplayNames().length)
			{
				TextTitle tt = chart.getTitle();
				Object val = qrs.getValue(0, index);
				if (val != null)
				{
					if (tt == null)
						chart.setTitle(val.toString());
					else
						tt.setText(val.toString());
				}
			}
		}
	}

	/**
	 * Set standard options for bar charts
	 * 
	 * @param chart
	 * @param co
	 * @param cds
	 * @param qrs
	 * @param targetURL
	 * @param dtype
	 */
	private static void setBarOptions(boolean stacked, JFreeChart chart, ChartOptions co, 
									  CategoryDataset cds, QueryResultSet qrs, String targetURL,
									  String dtype, String JSFunc, String drillTo)
	{
		if (cds != null)
			setCategoryOptions(stacked, chart, co, cds, qrs, false);
		// Set margin between bars within category
		CategoryPlot plot = (CategoryPlot) chart.getPlot();
		CategoryItemRenderer renderer = plot.getRenderer();
		if (BarRenderer.class.isInstance(renderer))
		{
			BarRenderer br = (BarRenderer) plot.getRenderer();
			if (co.gapWidth >= 0)
			{
				br.setItemMargin(co.gapWidth);
			}
		}
		// Get the formatting from the first series that is a measure
		String format = null;
		if (co.tickmarkFormat != null)
			format = co.tickmarkFormat;
		else
		{
			format = getFirstSeriesFormat(qrs, co);
		}
		if (co.toolTips)
		{
			if (format == null)
				renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator());
			else
				renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator("({0}, {1}) = {2}", new DecimalFormat(format)));
		}
		if (JSFunc != null)
		{
			renderer.setBaseItemURLGenerator(new JSGen(qrs, co, targetURL, dtype, JSFunc, drillTo));
		} else if (targetURL != null)
		{
			if (Chart.DRILL_TYPE_NONE.equals(dtype))
				renderer.setBaseItemURLGenerator(new StandardCategoryURLGenerator(targetURL));
			else
				renderer.setBaseItemURLGenerator(new URLGen(qrs, co, targetURL, dtype));
		}
		setTitleFromColumn(chart, qrs, co);
	}

	/**
	 * Class to generate drill and navigate URL targets for bar charts
	 * 
	 * @author bpeters
	 * 
	 */
	protected static class JSGen extends URLGen
	{
		private String JSFunctionName;
		private String drillTo;

		public JSGen(QueryResultSet _qrs, ChartOptions _co, String _targetURL, String _dtype, String _JSFunctionName, String drillTo)
		{
			super(_qrs, _co, _targetURL, _dtype);
			// stop drilling forever
			// - if drill thru and result set is 1, change to none
			if (Chart.DRILL_TYPE_DRILL.equals(dtype) && qrs != null && qrs.getRows().length <= 1)
			{
				dtype = Chart.DRILL_TYPE_NONE;
			}
			JSFunctionName = _JSFunctionName;
			this.drillTo = drillTo;
		}

		public String generateURL(String seriesKey, String categoryKey)
		{
			if (Chart.DRILL_TYPE_NONE.equals(this.dtype))
			{
				return "javascript:{ return false; }";
			}
			String filters = getFilters();
			HashMap<String, String> fmap = Chart.getFilterMap(co, seriesKey, categoryKey, mcolumns);
			
			StringBuilder sb = new StringBuilder();
			for (Iterator<Entry<String, String>> it = fmap.entrySet().iterator(); it.hasNext();)
			{
				Entry<String, String> e = it.next();
				String key = e.getKey();
				String val = e.getValue();
				String replacementValueColumn = co.columnValueReplacementsInURL.get(key);
				if (replacementValueColumn != null)
				{
					Object valObj = qrs.getValue(key, val, replacementValueColumn);
					if (valObj != null)
					{
						val = valObj.toString();
					}
				}
				String replacementVal = co.columnValueStrReplacementsInURL.get(val);
				if (replacementVal != null)
				{
					val = replacementVal;
				}
				boolean found = false;
				String[] columnNames = qrs.getColumnNames();
				String[] names = qrs.getDisplayNames();
				int nameIndex = 0;
				for (; nameIndex < names.length; nameIndex++)
				{
					String s = names[nameIndex];
					if (s == null) {
						s = columnNames[nameIndex];
					}
					if (s.equals(key))
					{
						found = true;
						break;
					}
				}
				if (found)
				{
					sb.setLength(0);
					sb.append("\"");
					sb.append(columnNames[nameIndex]);
					sb.append("\"");
					sb.append(",");
					sb.append("\"");
					// URLEncode the value because it could have single and double quotes in it
					try
					{
						val = URLEncoder.encode(val, "UTF-8");
					} catch (UnsupportedEncodingException mex)
					{
					}
					sb.append(val);
					sb.append("\"");
				}
			}
			return "javascript:{" + JSFunctionName + "(" + this.drillTo + "," + this.dtype + ")" + sb.toString()
					+ "," + filters + ");}";
		}

		private String getFilters()
		{
			StringBuilder filterB = new StringBuilder();
			for (QueryFilter qf : qrs.getQuery().getFilterList())
			{
				if (filterB.length() > 0)
				{
					filterB.append('&');
				}
				filterB.append(getFilterString(qf));
			}
			return filterB.toString();
		}

		private String getFilterString(QueryFilter qf)
		{
			StringBuilder filterB = new StringBuilder();
			if (qf.getType() == QueryFilter.TYPE_LOGICAL_OP)
			{
				for (QueryFilter qf1 : qf.getFilterList())
				{
					if (filterB.length() > 0)
					{
						filterB.append('&');
					}
					filterB.append(getFilterString(qf1));
				}
			} else
			{
				String dim = qf.getDimension();
				if (dim != null && Util.hasNonWhiteSpaceCharacters(dim))
				{
					filterB.append(dim);
					filterB.append('.');
				}
				filterB.append(qf.getColumn());
				filterB.append('=');
				filterB.append(qf.getOperand());
			}
			return filterB.toString();
		}

		public boolean isURL()
		{
			return false; // this.dtype == DrillType.Navigate;
		}

		public String generateNavigateURL(String seriesKey, String categoryKey)
		{
			if (Chart.DRILL_TYPE_NONE.equals(this.dtype))
			{
				return null;
			}
			String filters = getFilters();
			List<DimensionColumnNav> dimCols = qrs.getJasperDataSourceProvider().getQuery().dimensionColumns;
			HashMap<String, String> fmap = Chart.getFilterMap(co, seriesKey, categoryKey, mcolumns);
			StringBuilder result = new StringBuilder();
			result.append("Launch?module=dashboard&name=");
			result.append(drillTo);
			result.append('&');
			for (Iterator<Entry<String, String>> it = fmap.entrySet().iterator(); it.hasNext();)
			{
				Entry<String, String> e = it.next();
				String key = e.getKey();
				String val = e.getValue();
				String replacementValueColumn = co.columnValueReplacementsInURL.get(key);
				if (replacementValueColumn != null)
				{
					Object valObj = qrs.getValue(key, val, replacementValueColumn);
					if (valObj != null)
					{
						val = valObj.toString();
					}
				}
				String replacementVal = co.columnValueStrReplacementsInURL.get(val);
				if (replacementVal != null)
				{
					val = replacementVal;
				}
				boolean found = false;
				String[] columnNames = qrs.getColumnNames();
				String[] names = qrs.getDisplayNames();
				int nameIndex = 0;
				for (; nameIndex < names.length; nameIndex++)
				{
					String s = names[nameIndex];
					if (s == null) {
						s = columnNames[nameIndex];
					}
					if (s.equals(key))
					{
						found = true;
						break;
					}
				}
				if (found)
				{
					found = false;
					for (DimensionColumnNav dcn : dimCols)
					{
						String s = names[nameIndex];
						if (s == null) {
							s = columnNames[nameIndex];
						}
						if (s.equals(dcn.getDisplayName()))
						{
							found = true;
							result.append(xmlify(dcn.toString()));
						}
					}
					if (found == false)
					{
						result.append(xmlify(columnNames[nameIndex]));
						found = true;
					}
					/*
					 * // URLEncode the value because it could have single and double quotes in it try { val =
					 * URLEncoder.encode(val, "UTF-8"); } catch (UnsupportedEncodingException mex) { }
					 */
					result.append('=');
					result.append(xmlify(val));
				}
			}
			result.append('&');
			result.append(xmlify(filters));
			return result.toString();
		}
		

		public List<String> generateCall(String seriesKey, String categoryKey)
		{
			if (Chart.DRILL_TYPE_NONE.equals(this.dtype))
			{
				return null;
			}
//			StringBuilder filters = new StringBuilder(getFilters());
//			String filters = getFilters();
			List<DimensionColumnNav> dimCols = qrs.getJasperDataSourceProvider().getQuery().dimensionColumns;
			HashMap<String, String> fmap = Chart.getFilterMap(co, seriesKey, categoryKey, mcolumns);
			List<String> result = new ArrayList<String>();
			if (Chart.DRILL_TYPE_DRILL.equals(this.dtype) || Chart.DRILL_TYPE_NAVIGATE.equals(this.dtype)) {
				result.add(xmlify(JSFunctionName));
				result.add(xmlify(this.drillTo));
				result.add(this.dtype);
				StringBuilder drillBy = new StringBuilder();
				StringBuilder filters = new StringBuilder();
				for (Iterator<Entry<String, String>> it = fmap.entrySet().iterator(); it.hasNext();)
				{
					Entry<String, String> e = it.next();
					String key = e.getKey();
					String val = e.getValue();
					String replacementValueColumn = co.columnValueReplacementsInURL.get(key);
					if (replacementValueColumn != null)
					{
						Object valObj = qrs.getValue(key, val, replacementValueColumn);
						if (valObj != null)
						{
							val = valObj.toString();
						}
					}
					String replacementVal = co.columnValueStrReplacementsInURL.get(val);
					if (replacementVal != null)
					{
						val = replacementVal;
					}
					boolean found = false;
					String[] columnNames = qrs.getColumnNames();
					String[] names = qrs.getDisplayNames();
					int nameIndex = 0;
					for (; nameIndex < names.length; nameIndex++)
					{
						String s = names[nameIndex];
						if (s == null) {
							s = columnNames[nameIndex];
						}
						if (s.equals(key))
						{
							found = true;
							break;
						}
					}
					if (found)
					{
						found = false;
						for (DimensionColumnNav dcn : dimCols)
						{
							String s = names[nameIndex];
							if (s == null) {
								s = columnNames[nameIndex];
							}
							if (s.equals(dcn.getDisplayName()))
							{
								found = true;
								String c = dcn.toString();
								if (co.columnNameReplacementsInURL.containsKey(c)) {
									c = co.columnNameReplacementsInURL.get(c);
								}
								c = encodeParamForDrilling(c);
								if (co.addFilters.contains(key)) {
									if (filters.length() > 0) 
										filters.append("&amp;");

									filters.append(c);
									filters.append("=");
									
									int index = val == null ? -1 : val.indexOf('|');
									filters.append(encodeParamForDrilling(index > 0 ? val.substring(0, index) : val));
								}
								if (co.drillOn.contains(key)) {
									if (drillBy.length() > 0) 
										drillBy.append("&amp;");
									drillBy.append(c);
								}
								break;
							}
						}
						if (found == false)
						{
							String name = "";
							if (qrs.getColumnTypes()[nameIndex] == QueryColumn.DIMENSION) {
								name = qrs.getTableNames()[nameIndex];
								if (name != null && Util.hasNonWhiteSpaceCharacters(name)) {
									name = name + '.';
								}
							}
							name = name + columnNames[nameIndex];
							if (co.columnNameReplacementsInURL.containsKey(name)) {
								name = co.columnNameReplacementsInURL.get(name);
							}
							name = encodeParamForDrilling(name);
							if (co.addFilters.contains(key)) {
								if (filters.length() > 0) 
									filters.append("&amp;");
								filters.append(name);
								filters.append('=');
								int index = val == null ? -1 : val.indexOf('|');
								filters.append(encodeParamForDrilling(index > 0 ? val.substring(0, index) : val));
							}
							if (co.drillOn.contains(key)) {
								if (drillBy.length() > 0) 
									drillBy.append("&amp;");
								drillBy.append(name);
							}
							found = true;
						}
						/*
						 * // URLEncode the value because it could have single and double quotes in it try { val =
						 * URLEncoder.encode(val, "UTF-8"); } catch (UnsupportedEncodingException mex) { }
						 */
//						result.add(xmlify(val));
					}
					else {
						// not found
						String name = this.co.columnNameReplacementsInURL.get(key);
						if (name != null) {
							name = encodeParamForDrilling(name);
							if (result.size() < 5) {
								if (co.addFilters.contains(key)) {
									if (filters.length() > 0) 
										filters.append("&amp;");
									filters.append(name);
									filters.append('=');
									int index = val == null ? -1 : val.indexOf('|');
									filters.append(encodeParamForDrilling(index > 0 ? val.substring(0, index) : val));
								}
								if (co.drillOn.contains(key)) {
									if (drillBy.length() > 0) 
										drillBy.append("&amp;");
									drillBy.append(name);
								}
							}
						}
					}
				}
				result.add(filters.toString());
				result.add(drillBy.toString());
			}
			else {
				return null;
				
				/*
				// custom drill
				result.add(encoder == null ? this.dtype : encoder.encode(this.dtype));
				
				// this.drillTo contains the parameters
				// need to parse and add the result
				if (this.drillTo != null) {
					String[] params = this.drillTo.split(",");
					for (String p : params) {
						result.add(encoder == null ? p : encoder.encode(p));
					}
				}
				*/
			}
			return result;
		}
	}

	protected static class URLGen implements CategoryURLGenerator, PieURLGenerator, XYURLGenerator
	{
		protected QueryResultSet qrs;
		protected ChartOptions co;
		protected String targetURL;
		protected List<String> mcolumns;
		protected String dtype;
		protected Repository r;

		public URLGen(QueryResultSet _qrs, ChartOptions _co, String _targetURL, String _dtype)
		{
			qrs = _qrs;
			co = _co;
			targetURL = _targetURL;
			dtype = _dtype;
			r = qrs.getQuery().getRepository();
			mcolumns = new ArrayList<String>();
			/*
			 * Gather the measure columns that are series (shouldn't include in the drills)
			 */
			for (int i = 0; i < qrs.getColumnNames().length; i++)
			{
				if ((qrs.getColumnTypes()[i] == QueryColumn.MEASURE) && (co.series.indexOf(qrs.getColumnNames()[i]) >= 0))
					mcolumns.add(qrs.getColumnNames()[i]);
			}
		}

		public String generateURL(String seriesKey, String categoryKey)
		{
			if (Chart.DRILL_TYPE_NONE.equals(dtype))
			{
				return "javascript:{ return false; }";
			}
			HashMap<String, String> fmap = Chart.getFilterMap(co, seriesKey, categoryKey, mcolumns);
			boolean firstParameter = targetURL.indexOf('?') == -1;
			StringBuilder sb = new StringBuilder();
			sb.append(targetURL);
			sb.append(firstParameter ? '?' : '&');
			boolean first = true;
			for (Iterator<Entry<String, String>> it = fmap.entrySet().iterator(); it.hasNext();)
			{
				Entry<String, String> e = it.next();
				String key = e.getKey();
				String val = e.getValue();
				String replacementValueColumn = co.columnValueReplacementsInURL.get(key);
				if (replacementValueColumn != null)
				{
					Object valObj = qrs.getValue(key, val, replacementValueColumn);
					if (valObj != null)
					{
						val = valObj.toString();
					}
				}
				String replacementVal = co.columnValueStrReplacementsInURL.get(val);
				if (replacementVal != null)
				{
					val = replacementVal;
				}
				int index = 0;
				for (; index < qrs.getColumnNames().length; index++)
				{
					if (qrs.getColumnNames()[index].equals(key))
						break;
				}
				if (index < qrs.getColumnNames().length)
				{
					if (Chart.DRILL_TYPE_DRILL.equals(dtype))
					{
						DrillPath dp = new DrillPath(r, qrs.getTableNames()[index], qrs.getColumnNames()[index]);
						if (dp.isValid())
						{
							try
							{
								String s = dp.getDrillPathString(null, val);
								if (s != null)
								{
									if (first)
										first = false;
									else
										sb.append('&');
									sb.append(s);
								}
							} catch (UnsupportedEncodingException e1)
							{
							}
						}
					}
				}
				if (Chart.DRILL_TYPE_NAVIGATE.equals(dtype))
				{
					try
					{
						String replacementKey = co.columnNameReplacementsInURL.get(key);
						if (first)
							first = false;
						else
							sb.append('&');
						sb.append("P_");
						if (replacementKey != null)
							sb.append(replacementKey);
						else
							sb.append(key);
						sb.append('=');
						sb.append(URLEncoder.encode(val, "UTF-8"));
					} catch (UnsupportedEncodingException e1)
					{
					}
				}
			}
			return (sb.toString());
		}

		public String generateURL(CategoryDataset cds, int series, int category)
		{
			String seriesKey = null;
			String categoryKey = null;
			if (co.swapSeriesAndCategories)
			{
				seriesKey = cds.getColumnKey(category).toString();
				categoryKey = cds.getRowKey(series).toString();
			} else
			{
				seriesKey = cds.getRowKey(series).toString();
				categoryKey = cds.getColumnKey(category).toString();
			}
			/*
			 * Only drill on the series if that's selected
			 */
			if (!co.drillSeries)
				seriesKey = null;
			return (generateURL(seriesKey, categoryKey));
		}

		public List<String> generateCall(CategoryDataset cds, int series, int category)
		{
			String seriesKey = null;
			String categoryKey = null;
			if (co.swapSeriesAndCategories)
			{
				seriesKey = cds.getColumnKey(category).toString();
				categoryKey = cds.getRowKey(series).toString();
			} else
			{
				seriesKey = cds.getRowKey(series).toString();
				categoryKey = cds.getColumnKey(category).toString();
			}
			/*
			 * Only drill on the series if that's selected
			 */
			if (!co.drillSeries)
				seriesKey = null;
			return (((JSGen) this).generateCall(seriesKey, categoryKey));
		}

		public String generateNavigateURLCall(CategoryDataset cds, int series, int category)
		{
			String seriesKey = null;
			String categoryKey = null;
			if (co.swapSeriesAndCategories)
			{
				seriesKey = cds.getColumnKey(category).toString();
				categoryKey = cds.getRowKey(series).toString();
			} else
			{
				seriesKey = cds.getRowKey(series).toString();
				categoryKey = cds.getColumnKey(category).toString();
			}
			/*
			 * Only drill on the series if that's selected
			 */
			if (!co.drillSeries)
				seriesKey = null;
			return (((JSGen) this).generateNavigateURL(seriesKey, categoryKey));
		}

		public List<String> generateCall(PieDataset pds, int category)
		{
			String categoryKey = null;
			categoryKey = pds.getKey(category).toString();
			return (((JSGen) this).generateCall(null, categoryKey));
		}

		public String generateURL(PieDataset pds, @SuppressWarnings("rawtypes") Comparable key, int pieindex)
		{
			return (generateURL(null, key.toString()));
		}

		public String generateURL(XYDataset arg0, int arg1, int arg2)
		{
			return (generateURL(null, arg0.getSeriesKey(arg1).toString()));
		}
	}

	/**
	 * Create a bar chart
	 * 
	 * @param qrs
	 *            Query result set
	 * @param co
	 *            Chart options
	 * @param targetURL
	 *            Target for image maps
	 * @return
	 */
	private static JCommonDrawableRenderer barChart(QueryResultSet qrs, ChartOptions co, String targetURL, 
			                                       boolean stacked, String dtype, String JSFunc,
			                                       String drillTo)
	{
		BirstCategoryDataset cds = createCategoryDataset(qrs, co.categories, co.series, co.rangeTickmarkFormat, co.swapSeriesAndCategories, co.seriesGroups,
				co.seriesGroupBySeries, co.seriesNameBySeries, co.getFormatMap(), co.aggregateMap, co.colorColumn);
		if (cds == null)
			return null;
		JFreeChart chart = null;
		if (stacked)
		{
			if (co.threeD)
			{
				chart = ChartFactory.createStackedBarChart3D(co.title, "", "", cds, co.vertical ? PlotOrientation.VERTICAL : PlotOrientation.HORIZONTAL,
						co.legend, false, false);
			} else
			{
				chart = ChartFactory.createStackedBarChart(co.title, "", "", cds, co.vertical ? PlotOrientation.VERTICAL : PlotOrientation.HORIZONTAL,
						co.legend, false, false);
			}
		} else
		{
			if (co.threeD)
			{
				chart = ChartFactory.createBarChart3D(co.title, "", "", cds, co.vertical ? PlotOrientation.VERTICAL : PlotOrientation.HORIZONTAL, co.legend,
						false, false);
			} else
			{
				chart = ChartFactory.createBarChart(co.title, "", "", cds, co.vertical ? PlotOrientation.VERTICAL : PlotOrientation.HORIZONTAL, co.legend,
						false, false);
			}
		}
		setBarOptions(stacked, chart, co, cds, qrs, targetURL, dtype, JSFunc, drillTo);
		FlashRenderer jcdr = new FlashRenderer(chart);
		jcdr.setCategoryDataset(cds);
		jcdr.setQueryResultSet(qrs);
		jcdr.setChartOptions(co);
		jcdr.setImageMap(true);
		jcdr.setCategoryKeyColorMap(cds.categoryColorMap);
		return (jcdr);
	}
	
	private static JCommonDrawableRenderer funnelChart(QueryResultSet qrs, ChartOptions co, String targetURL, 
			String dtype, String JSFunc,
			String drillTo, boolean cone)
	{
		BirstCategoryDataset cds = createCategoryDataset(qrs, co.categories, co.series, co.rangeTickmarkFormat, co.swapSeriesAndCategories, co.seriesGroups,
				co.seriesGroupBySeries, co.seriesNameBySeries, co.getFormatMap(), co.aggregateMap, co.colorColumn);
		if (cds == null)
			return null;
		JFreeChart chart = null;
		if (co.threeD)
		{
			chart = ChartFactory.createBarChart3D(co.title, "", "", cds, co.vertical ? PlotOrientation.VERTICAL : PlotOrientation.HORIZONTAL, co.legend,
					false, false);
		} else
		{
			chart = ChartFactory.createBarChart(co.title, "", "", cds, co.vertical ? PlotOrientation.VERTICAL : PlotOrientation.HORIZONTAL, co.legend,
					false, false);
		}
		setBarOptions(false, chart, co, cds, qrs, targetURL, dtype, JSFunc, drillTo);
		FlashRenderer jcdr = new FlashRenderer(chart);
		jcdr.setCategoryDataset(cds);
		jcdr.setQueryResultSet(qrs);
		jcdr.setChartOptions(co);
		jcdr.setImageMap(true);
		jcdr.setCategoryKeyColorMap(cds.categoryColorMap);
		return (jcdr);
	}
	
	private static JCommonDrawableRenderer treeMapChart(QueryResultSet qrs, ChartOptions co, String targetURL, 
			String dtype, String JSFunc,
			String drillTo)
	{
		BirstCategoryDataset cds = createCategoryDataset(qrs, co.categories, co.series, co.rangeTickmarkFormat, co.swapSeriesAndCategories, co.seriesGroups,
				co.seriesGroupBySeries, co.seriesNameBySeries, co.getFormatMap(), co.aggregateMap, co.colorColumn);
		if (cds == null)
			return null;
		JFreeChart chart = null;
		chart = ChartFactory.createBarChart(co.title, "", "", cds, co.vertical ? PlotOrientation.VERTICAL : PlotOrientation.HORIZONTAL, co.legend,
						false, false);
		setBarOptions(false, chart, co, cds, qrs, targetURL, dtype, JSFunc, drillTo);
		FlashRenderer jcdr = new FlashRenderer(chart);
		jcdr.setCategoryDataset(cds);
		jcdr.setQueryResultSet(qrs);
		jcdr.setChartOptions(co);
		jcdr.setImageMap(true);
		jcdr.setCategoryKeyColorMap(cds.categoryColorMap);
		return (jcdr);
	}
	
	private static JCommonDrawableRenderer heatMapChart(QueryResultSet qrs, ChartOptions co, String targetURL, 
			String dtype, String JSFunc,
			String drillTo)
	{
		// XXX Bogus - will always be true
		if (co.categories.size() != 2 || co.series.size() != 1)
			return null;
		return new FlashRenderer(qrs, co, true);
	}


	private static JCommonDrawableRenderer barChart(JRDataSource qrs, String chartOptions, String targetURL, boolean stacked, String dtype, String JSFunc,
			String drillTo)
	{
		return (barChart((QueryResultSet) qrs, new ChartOptions(chartOptions), targetURL, stacked, dtype, JSFunc, drillTo));
	}
	
	private static JCommonDrawableRenderer funnelChart(JRDataSource qrs, String chartOptions, String targetURL, String dtype, String JSFunc,
			String drillTo, boolean cone)
	{
		return (funnelChart((QueryResultSet) qrs, new ChartOptions(chartOptions), targetURL, dtype, JSFunc, drillTo, cone));
	}
	
	private static JCommonDrawableRenderer treeMapChart(JRDataSource qrs, String chartOptions, String targetURL, String dtype, String JSFunc,
			String drillTo)
	{
		return (treeMapChart((QueryResultSet) qrs, new ChartOptions(chartOptions), targetURL, dtype, JSFunc, drillTo));
	}
	
	private static JCommonDrawableRenderer heatMapChart(JRDataSource qrs, String chartOptions, String targetURL, String dtype, String JSFunc,
			String drillTo)
	{
		return (heatMapChart((QueryResultSet) qrs, new ChartOptions(chartOptions), targetURL, dtype, JSFunc, drillTo));
	}


	private static JCommonDrawableRenderer geoMapChart(QueryResultSet qrs,
			ChartOptions co, boolean b, String targetURL,
			String dtype, String func, String drillTo) {
		BirstCategoryDataset cds = createCategoryDataset(qrs, co.categories, co.series, co.rangeTickmarkFormat, co.swapSeriesAndCategories,
				co.seriesGroups, co.seriesGroupBySeries, co.seriesNameBySeries, co.getFormatMap(), co.aggregateMap, null);
		if (cds == null)
			return null;
		FlashRenderer renderer = new FlashRenderer(qrs, co, true);
		PiePlot p = (PiePlot)((JFreeChart)renderer.getDrawable()).getPlot();
		if (func != null)
			p.setURLGenerator(new JSGen(qrs, co, targetURL, dtype, func, drillTo));
		return renderer;
	}
	/**
	 * Create a line chart
	 * 
	 * @param qrs
	 *            Query result set
	 * @param co
	 *            Chart options
	 * @return
	 */
	private static JCommonDrawableRenderer lineChart(QueryResultSet qrs, ChartOptions co, boolean series2, String targetURL, String dtype, String JSFunc,
			String drillTo)
	{
		BirstCategoryDataset cds = createCategoryDataset(qrs, co.categories, series2 ? co.series2 : co.series, co.rangeTickmarkFormat, co.swapSeriesAndCategories,
				co.seriesGroups, co.seriesGroupBySeries, co.seriesNameBySeries, co.getFormatMap(), co.aggregateMap, co.colorColumn);
		if (cds == null)
			return null;
		JFreeChart chart = null;
		if (co.threeD)
		{
			chart = ChartFactory.createLineChart3D(co.title, "", "", cds, co.vertical ? PlotOrientation.VERTICAL : PlotOrientation.HORIZONTAL, co.legend,
					false, false);
		} else
		{
			chart = ChartFactory.createLineChart(co.title, "", "", cds, co.vertical ? PlotOrientation.VERTICAL : PlotOrientation.HORIZONTAL, co.legend, false,
					false);
		}
		setCategoryOptions(false, chart, co, cds, qrs, series2);
		CategoryPlot plot = (CategoryPlot) chart.getPlot();
		CategoryItemRenderer renderer = null;
		if (co.threeD)
		{
			LineRenderer3D rend = new LineRenderer3D();
			plot.setRenderer(rend);
			renderer = rend;
		} else
		{
			LineAndShapeRenderer rend = (LineAndShapeRenderer) plot.getRenderer();
			rend.setBaseStroke(new BasicStroke(co.lineWidth));
			if (co.drawShapes)
			{
				rend.setBaseShape(new Rectangle(-co.shapeSize / 2, -co.shapeSize / 2, co.shapeSize, co.shapeSize), true);
				rend.setBaseShapesVisible(true);
			}
			renderer = rend;
		}
		if (co.stroke != null)
		{
			renderer.setBaseStroke(co.stroke);
		}
		// Get the formatting from the first series that is a measure
		String format = null;
		if (co.tickmarkFormat != null)
			format = co.tickmarkFormat;
		else
		{
			format = getFirstSeriesFormat(qrs, co);
		}
		if (co.toolTips)
		{
			if (format == null)
				renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator());
			else
				renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator("({0}, {1}) = {2}", new DecimalFormat(format)));
		}
		if (JSFunc != null)
		{
			renderer.setBaseItemURLGenerator(new JSGen(qrs, co, targetURL, dtype, JSFunc, drillTo));
		} else if (targetURL != null)
		{
			if (Chart.DRILL_TYPE_NONE.equals(dtype))
				renderer.setBaseItemURLGenerator(new StandardCategoryURLGenerator(targetURL));
			else
				renderer.setBaseItemURLGenerator(new URLGen(qrs, co, targetURL, dtype));
		}
		setTitleFromColumn(chart, qrs, co);
		FlashRenderer jcdr = new FlashRenderer(chart);
		jcdr.setCategoryDataset(cds);
		jcdr.setQueryResultSet(qrs);
		jcdr.setChartOptions(co);
		jcdr.setImageMap(true);
		jcdr.setCategoryKeyColorMap(cds.categoryColorMap);
		return (jcdr);
	}

	private static JCommonDrawableRenderer lineChart(QueryResultSet qrs, ChartOptions co, boolean series2)
	{
		return (lineChart(qrs, co, series2, null, null, null, null));
	}

	private static JCommonDrawableRenderer lineChart(JRDataSource qrs, String chartOptions, String targetURL, String dtype, String JSFunc, String drillTo)
	{
		return (lineChart((QueryResultSet) qrs, new ChartOptions(chartOptions), false, targetURL, dtype, JSFunc, drillTo));
	}
	private static JCommonDrawableRenderer geoMapChart(JRDataSource jrds,
			String chartOptions, String targetURL, String dtype, String JSFunc, String drillTo) {
		return geoMapChart((QueryResultSet)jrds, new ChartOptions(chartOptions), false, targetURL, dtype, JSFunc, drillTo);
	}


	/**
	 * Create a bar/line chart
	 * 
	 * @param qrs
	 *            Query result set
	 * @param co
	 *            Chart options
	 * @param targetURL
	 *            Target for image maps
	 * @return
	 */
	private static JCommonDrawableRenderer barLineChart(QueryResultSet qrs, ChartOptions co, String targetURL, boolean stacked, String dtype, String JSFunc,
			String drillTo)
	{
		JCommonDrawableRenderer jcdr = barChart(qrs, co, targetURL, stacked, dtype, JSFunc, drillTo);
		if (jcdr == null)
			return null;
		JFreeChart chart1 = (JFreeChart) jcdr.getDrawable();
		JCommonDrawableRenderer jcdr1 = lineChart(qrs, co, true);
		JFreeChart chart2 = (JFreeChart) jcdr1.getDrawable();
		if (jcdr1 instanceof FlashRenderer) {
			((FlashRenderer)jcdr1).setCategoryKeyColorMap(null);
		}
		if (co.shareAxis)
		{
			shareAxis(chart1, chart2);
		}
		CategoryPlot plot1 = chart1.getCategoryPlot();
		CategoryPlot plot2 = chart2.getCategoryPlot();
		plot1.setDataset(1, plot2.getDataset());
		plot1.mapDatasetToRangeAxis(1, 1);
		plot1.setRenderer(1, plot2.getRenderer());
		plot1.setRangeAxis(1, plot2.getRangeAxis());
		plot1.setDomainAxisLocation(AxisLocation.BOTTOM_OR_RIGHT);
		plot1.setDatasetRenderingOrder(DatasetRenderingOrder.FORWARD);
		// make the color sets consistent
		plot1.setDrawingSupplier(plot2.getDrawingSupplier());
		if (jcdr instanceof FlashRenderer)
		{
			((FlashRenderer) jcdr).setDrawable2(chart2);
		}
		return (jcdr);
	}

	private static void shareAxis(JFreeChart barChart, JFreeChart lineChart)
	{
		CategoryPlot barPlot = barChart.getCategoryPlot();
		CategoryPlot linePlot = lineChart.getCategoryPlot();
		NumberAxis barAxis = (NumberAxis) barPlot.getRangeAxis();
		NumberAxis lineAxis = (NumberAxis) linePlot.getRangeAxis();
		double barValueMin = barAxis.getLowerBound();
		double barValueMax = barAxis.getUpperBound();
		double lineValueMin = lineAxis.getLowerBound();
		double lineValueMax = lineAxis.getUpperBound();
		double valueMin = barValueMin;
		if (lineValueMin < barValueMin)
		{
			valueMin = lineValueMin;
		}
		double valueMax = barValueMax;
		if (lineValueMax > barValueMax)
		{
			valueMax = lineValueMax;
		}
		barAxis.setLowerBound(valueMin);
		lineAxis.setLowerBound(valueMin);
		barAxis.setUpperBound(valueMax);
		lineAxis.setUpperBound(valueMax);
		barAxis.setRange(valueMin, valueMax);
		lineAxis.setRange(valueMin, valueMax);
		lineAxis.setTickLabelsVisible(false);
	}

	private static JCommonDrawableRenderer barLineChart(JRDataSource qrs, String chartOptions, String targetURL, boolean stacked, String dtype,
			String JSFunc, String drillTo)
	{
		return (barLineChart((QueryResultSet) qrs, new ChartOptions(chartOptions), targetURL, stacked, dtype, JSFunc, drillTo));
	}

	/**
	 * Create a scatter plot chart
	 * 
	 * @param qrs
	 *            Query result set
	 * @param co
	 *            Chart options
	 * @param targetURL
	 *            Target for image maps
	 * @return
	 */
	private static JCommonDrawableRenderer scatterPlot(QueryResultSet qrs, ChartOptions co, String targetURL, String dtype, String JSFunc, String drillTo)
	{
		BirstXYSeriesCollection xyds = createXYDataset(qrs, co.categories, co.series, co.getFormatMap(), co.rangeTickmarkFormat, co.colorColumn);
		if (xyds == null)
		{
			logger.debug("Dataset conditions not met");
			return null;
		}
		JFreeChart chart = null;
		chart = ChartFactory.createScatterPlot(co.title, "", "", xyds, co.vertical ? PlotOrientation.VERTICAL : PlotOrientation.HORIZONTAL, co.legend, true,
				false);
		if (co.quadrantNames != null)
		{
			XYPlot pl = chart.getXYPlot();
			XYQuadrantPlot newpl = new XYQuadrantPlot(pl.getDataset(), pl.getDomainAxis(), pl.getRangeAxis(), pl.getRenderer());
			newpl.setQuadrantNames(co.quadrantNames);
			chart = new JFreeChart(co.title, co.getTitleFont(), (XYPlot) newpl, chart.getLegend() != null);
		}
		setXYOptions(chart, co, xyds);
		XYLineAndShapeRenderer r = (XYLineAndShapeRenderer) chart.getXYPlot().getRenderer();
		if (co.shapeSize > 0)
		{
			for (int i = 0; i < xyds.getSeriesCount(); i++)
			{
				r.setSeriesShape(i, new Ellipse2D.Double(0, 0, co.shapeSize, co.shapeSize));
			}
		}
		Font labelFont = co.getLabelFont();
		if (labelFont != null)
		{
			ScatterToolTipGen sttg = new ScatterToolTipGen();
			for (int i = 0; i < xyds.getSeriesCount(); i++)
			{
				r.setSeriesItemLabelFont(i, labelFont);
				r.setSeriesToolTipGenerator(i, sttg);
			}
		}
		FlashRenderer jcdr = new FlashRenderer(chart);
		jcdr.setImageMap(true);
		jcdr.setChartOptions(co);
		jcdr.setXYDataset(xyds);
		jcdr.setQueryResultSet(qrs);
		jcdr.setCategoryKeyColorMap(xyds.categoryColorMap);
		if (JSFunc != null)
		{
			r.setURLGenerator(new JSGen(qrs, co, targetURL, dtype, JSFunc, drillTo));
		} else if (targetURL != null)
			r.setURLGenerator(new URLGen(qrs, co, targetURL, dtype));
		return (jcdr);
	}

	private static JCommonDrawableRenderer scatterPlot(JRDataSource qrs, String chartOptions, String targetURL, String dtype, String JSFunc, String drillTo)
	{
		return (scatterPlot((QueryResultSet) qrs, new ChartOptions(chartOptions), targetURL, dtype, JSFunc, drillTo));
	}

	public static class ScatterToolTipGen implements XYToolTipGenerator
	{
		public ScatterToolTipGen()
		{
		}

		public String generateToolTip(XYDataset dataset, int series, int item)
		{
			return (dataset.getSeriesKey(series).toString());
		}
	}

	/**
	 * Create a bubble chart
	 * 
	 * @param qrs
	 *            Query result set
	 * @param co
	 *            Chart options
	 * @param targetURL
	 *            Target for image maps
	 * @return
	 */
	private static JCommonDrawableRenderer bubbleChart(QueryResultSet qrs, ChartOptions co, String targetURL, String dtype, String JSFunctionName,
			String drillTo)
	{
		BirstSeriesXYZDataset xyds = createSeriesXYZDataset(qrs, co.categories, co.series, co.getFormatMap(), co.rangeTickmarkFormat, co.colorColumn);
		if (xyds == null)
		{
			logger.debug("Dataset conditions not met - atleast 3 measures for a bubbleChart");
			return null;
		}
		JFreeChart chart = null;
		chart = ChartFactory.createBubbleChart(co.title, "", "", xyds, PlotOrientation.VERTICAL, co.legend, false, false);
		NumberAxis zAxis = new NumberAxis(null);
		zAxis.setAutoRangeIncludesZero(false);
		XYBubbleRenderer renderer = new XYBubbleRenderer();
		renderer.setZRange(xyds.getMinZValue(), xyds.getMaxZValue());
		XYPlot p = chart.getXYPlot();
		p.setRenderer(renderer);
		renderer.setBaseToolTipGenerator(new StandardXYZToolTipGenerator());
		Font labelFont = co.getLabelFont();
		if (labelFont != null)
		{
			BubbleLabelGen blg = new BubbleLabelGen(qrs.getFormat(co.series.get(2)), co.series.get(2));
			renderer.setBaseItemLabelFont(labelFont);
			renderer.setBaseItemLabelGenerator(blg);
		}
		if (JSFunctionName != null)
		{
			renderer.setURLGenerator(new JSGen(qrs, co, targetURL, dtype, JSFunctionName, drillTo));
		} else if (targetURL != null)
			renderer.setURLGenerator(new URLGen(qrs, co, targetURL, dtype));
		setXYOptions(chart, co, xyds);
		FlashRenderer jcdr = new FlashRenderer(chart);
		jcdr.setImageMap(true);
		jcdr.setChartOptions(co);
		jcdr.setXYDataset(xyds);
		jcdr.setQueryResultSet(qrs);
		jcdr.setCategoryKeyColorMap(xyds.categoryColorMap);
		return (jcdr);
	}

	public static class BubbleLabelGen implements XYItemLabelGenerator
	{
		DecimalFormat df;
		String zValueLabel = null;

		public BubbleLabelGen(String format, String zValueLabel)
		{
			if (format != null)
				this.df = new DecimalFormat(format);
			else
				this.df = new DecimalFormat("#,###");
			this.zValueLabel = zValueLabel;
		}

		public String generateLabel(SeriesXYZDataset dataset, int series, int item)
		{
			Number z = dataset.getZ(series, item);
			if (zValueLabel != null)
				return (zValueLabel + "=" + df.format(z));
			else
				return (df.format(z));
		}

		public String generateLabel(XYDataset dataset, int series, int item)
		{
			if (SeriesXYZDataset.class.isInstance(dataset))
			{
				return (generateLabel((SeriesXYZDataset) dataset, series, item));
			}
			return null;
		}
	}

	/**
	 * Create a contour chart
	 * 
	 * @param qrs
	 *            Query result set
	 * @param co
	 *            Chart options
	 * @param targetURL
	 *            Target for image maps
	 * @return
	 */
	private static JCommonDrawableRenderer spiderWebPlot(QueryResultSet qrs, ChartOptions co, String targetURL)
	{
		BirstCategoryDataset cds = createCategoryDataset(qrs, co.categories, co.series, co.rangeTickmarkFormat, co.swapSeriesAndCategories, co.seriesGroups,
				co.seriesGroupBySeries, co.seriesNameBySeries, co.getFormatMap(), co.aggregateMap, co.colorColumn);
		if (cds == null)
			return  null;
		SpiderWebPlot p = new SpiderWebPlot(cds);
		JFreeChart chart = new JFreeChart(co.title, p);
		setChartOptions(chart, co);
		Font labelFont = co.getLabelFont();
		if (labelFont != null)
			p.setLabelFont(labelFont);
		setCategoryColors(p, co);
		p.setOutlinePaint(Color.WHITE);
		Font titleFont = co.getTitleFont();
		if ((titleFont != null) && (chart.getTitle() != null))
			chart.getTitle().setFont(titleFont);
		JCommonDrawableRenderer jcdr = new JCommonDrawableRenderer(chart);
		jcdr.setImageMap(true);
		return (jcdr);
	}

	private static JCommonDrawableRenderer spiderWebPlot(JRDataSource qrs, String chartOptions, String targetURL)
	{
		return (spiderWebPlot((QueryResultSet) qrs, new ChartOptions(chartOptions), targetURL));
	}

	/**
	 * Create an area chart
	 * 
	 * @param qrs
	 *            Query result set
	 * @param co
	 *            Chart options
	 * @param targetURL
	 *            Target for image maps
	 * @return
	 */
	private static JCommonDrawableRenderer areaChart(QueryResultSet qrs, ChartOptions co, String targetURL, boolean stacked)
	{
		BirstCategoryDataset cds = createCategoryDataset(qrs, co.categories, co.series, co.rangeTickmarkFormat, co.swapSeriesAndCategories, co.seriesGroups,
				co.seriesGroupBySeries, co.seriesNameBySeries, co.getFormatMap(), co.aggregateMap, co.colorColumn);
		if (cds == null)
		{
			return null;
		}
		JFreeChart chart = null;
		if (stacked)
		{
			chart = ChartFactory.createStackedAreaChart(co.title, "", "", cds, co.vertical ? PlotOrientation.VERTICAL : PlotOrientation.HORIZONTAL, co.legend,
					false, false);
		} else
		{
			chart = ChartFactory.createAreaChart(co.title, "", "", cds, co.vertical ? PlotOrientation.VERTICAL : PlotOrientation.HORIZONTAL, co.legend, false,
					false);
		}
		chart.getCategoryPlot().getDomainAxis().setCategoryMargin(0.0);
		setBarOptions(stacked, chart, co, cds, qrs, targetURL, Chart.DRILL_TYPE_NONE, null, null);
		FlashRenderer jcdr = new FlashRenderer(chart);
		jcdr.setCategoryDataset(cds);
		jcdr.setQueryResultSet(qrs);
		jcdr.setChartOptions(co);
		setBarOptions(stacked, chart, co, cds, qrs, targetURL, Chart.DRILL_TYPE_NONE, null, null);
		jcdr.setCategoryKeyColorMap(cds.categoryColorMap);
		jcdr.setImageMap(true);
		return (jcdr);
	}

	private static JCommonDrawableRenderer areaChart(JRDataSource qrs, String chartOptions, String targetURL, boolean stacked)
	{
		return (areaChart((QueryResultSet) qrs, new ChartOptions(chartOptions), targetURL, stacked));
	}

	/*
	 * set the colors for a pie chart based upon a column in the result set
	 * 
	 * @param p pie chart plot @param category name of the category column @param color name of the color column @param
	 * qrs query result set @param ds pie data set
	 */
	@SuppressWarnings("rawtypes")
	private static void setPieColors(PiePlot p, String category, String color, QueryResultSet qrs, PieDataset ds)
	{
		String[] colNames = qrs.getDisplayNames();
		int categoryIndex = -1;
		int colorIndex = -1;
		for (int i = 0; i < colNames.length; i++)
		{
			if (category.equals(colNames[i]))
			{
				categoryIndex = i;
			}
			if (color.equals(colNames[i]))
			{
				colorIndex = i;
			}
		}
		// could not find the columns for the category or color, return early
		if (categoryIndex == -1 || colorIndex == -1)
			return;
		// get the category - color mapping
		HashMap<String, Color> colors = new HashMap<String, Color>();
		for (Object[] row : qrs.getRows())
		{
			String cat = (String) row[categoryIndex];
			String clr = (String) row[colorIndex];
			if (cat == null || clr == null)
				continue;
			try
			{
				Color c = new Color(Integer.parseInt(clr, 16));
				colors.put(cat, c);
			} catch (Exception ex)
			{
				logger.warn("Bad color specification for '" + cat + "': '" + clr + "'");
			}
		}
		// set the colors for the pie slices
		for (Object obj : ds.getKeys())
		{
			Color clr = colors.get(obj.toString());
			if (clr == null)
				continue;
			p.setSectionPaint((java.lang.Comparable) obj, clr);
			p.setSectionOutlinePaint((java.lang.Comparable) obj, clr);
		}
	}

	/**
	 * Create a pie chart
	 * 
	 * @param qrs
	 *            Query result set
	 * @param co
	 *            Chart options
	 * @param targetURL
	 *            Target for image maps
	 * @return
	 */
	private static JCommonDrawableRenderer pieChart(QueryResultSet qrs, ChartOptions co, String targetURL, String dtype, String JSFunctionName, String drillTo)
	{
		BirstPieDataset ds = createPieDataset(qrs, co.categories, co.series, co.percent, co.getFormatMap(), co.aggregateMap, co.colorColumn);
		if (ds == null)
			return null;
		JFreeChart chart = null;
		PiePlot p = null;
		if (co.threeD)
		{
			chart = ChartFactory.createPieChart3D("", ds, co.legend, false, false);
			p = (PiePlot) chart.getPlot();
		} else
		{
			chart = ChartFactory.createPieChart("", ds, co.legend, false, false);
			p = (PiePlot) chart.getPlot();
		}
		Font labelFont = co.getLabelFont();
		if (labelFont != null)
			p.setLabelFont(labelFont);
		p.setLabelOutlineStroke(co.labelOutline);
		double total = 0;
		for (int i = 0; i < ds.getItemCount(); i++)
			total += ds.getValue(i).doubleValue();
		PieLabelGen lg = new PieLabelGen(co.pieLabelFormat, co.showPieLabelKey, true, ((co.percent ? 1 : total) * SMALL_NUMBER));
		p.setLabelGenerator(lg);
		if (JSFunctionName != null)
		{
			p.setURLGenerator(new JSGen(qrs, co, targetURL, dtype, JSFunctionName, drillTo));
		} else if (targetURL != null)
			p.setURLGenerator(new URLGen(qrs, co, targetURL, dtype));
		if (co.toolTips)
			p.setToolTipGenerator(lg);
		if (co.legend)
		{
			p.setLegendLabelGenerator(new PieLabelGen(co.pieLabelFormat, true, false, 0));
		}
		if (co.labelOutline == null)
			p.setLabelShadowPaint(null);
		p.setLabelGap(0);
		p.setLabelBackgroundPaint(Color.WHITE);
		ChartColors cc = new ChartColors(1, 1, null);
		if (co.chartColors != null)
		{
			Color[] colorArr = new Color[co.chartColors.size()];
			co.chartColors.toArray(colorArr);
			cc.setColors(colorArr);
		}
		if (co.gradient != null)
		{
			cc.setGradient(true);
			boolean threed = co.gradient.equals("3D");
			cc.setThreeD(threed);
			cc.setDirection(Direction.radial);
			// set rotation angles
			double[] startAngles = new double[ds.getItemCount()];
			double[] endAngles = new double[ds.getItemCount()];
			double runningTotal = 0;
			for (int i = 0; i < ds.getItemCount(); i++)
			{
				startAngles[i] = runningTotal * 2 * Math.PI / total;
				runningTotal += ds.getValue(i).doubleValue();
				endAngles[i] = runningTotal * 2 * Math.PI / total;
			}
			cc.setStartAngles(startAngles);
			cc.setEndAngles(endAngles);
		}
		p.setDrawingSupplier(cc);
		p.setOutlineStroke(null);
		p.setInteriorGap(co.interiorGap);
		p.setStartAngle(co.startAngle);
		chart.setBackgroundPaint(Color.WHITE);
		p.setShadowPaint(null);
		setTitleFromColumn(chart, qrs, co);
		Font titleFont = co.getTitleFont();
		if (titleFont != null)
		{
			TextTitle tt = chart.getTitle();
			if (tt == null)
			{
				tt = new TextTitle();
				chart.setTitle(tt);
			}
			tt.setFont(titleFont);
			chart.setTitle(tt);
		}
		if (co.title != null)
		{
			TextTitle tt = chart.getTitle();
			if (tt == null)
			{
				tt = new TextTitle();
			}
			tt.setText(co.title);
			if (titleFont != null)
			{
				tt.setFont(titleFont);
			}
			chart.setTitle(tt);
		}
		if (co.legend)
		{
			LegendTitle lt = chart.getLegend();
			if (lt != null)
			{
				Font legendFont = co.getLegendFont();
				if (legendFont != null)
				{
					lt.setItemFont(legendFont);
				} else if (labelFont != null)
				{
					lt.setItemFont(labelFont);
				}
				lt.setPosition(co.legendAnchor);
			}
		}
		// if the color column is set try to set the pie slice colors
		if (co.colorColumn != null)
			setPieColors(p, co.categories.get(0), co.colorColumn, qrs, ds);
		FlashRenderer jcdr = new FlashRenderer(chart);
		jcdr.setPieDataset(ds);
		jcdr.setQueryResultSet(qrs);
		jcdr.setChartOptions(co);
		jcdr.setImageMap(true);
		jcdr.setCategoryKeyColorMap(ds.categoryColorMap);
		return (jcdr);
	}

	private static JCommonDrawableRenderer pieChart(JRDataSource qrs, String chartOptions, String targetURL, String drillType, String JSFunc, String drillTo)
	{
		return (pieChart((QueryResultSet) qrs, new ChartOptions(chartOptions), targetURL, drillType, JSFunc, drillTo));
	}

	private static class PieLabelGen implements PieSectionLabelGenerator, PieToolTipGenerator, Serializable
	{
		private static final long serialVersionUID = 1L;
		private boolean showKey;
		private boolean showValue;
		DecimalFormat df;
		boolean noZero = false;
		double total;

		public PieLabelGen(String format, boolean showKey, boolean showValue, double total)
		{
			String fmt = format;
			if (format.indexOf("nz") >= 0)
			{
				noZero = true;
				fmt = format.replaceAll("nz", "");
			}
			this.showKey = showKey;
			this.showValue = showValue;
			this.df = new DecimalFormat(fmt);
			this.total = total;
		}

		public String generateSectionLabel(PieDataset ds, @SuppressWarnings("rawtypes") Comparable key)
		{
			Number num = ds.getValue(key);
			if (noZero && (num.doubleValue() <= total))
				return (null);
			if (showKey)
				if (showValue)
					return key + "-" + df.format(num.doubleValue());
				else
					return key.toString();
			else
				return (df.format(num.doubleValue()));
		}

		public String generateToolTip(PieDataset ds, @SuppressWarnings("rawtypes") Comparable key)
		{
			return (generateSectionLabel(ds, key));
		}

		public AttributedString generateAttributedSectionLabel(PieDataset ds, @SuppressWarnings("rawtypes") Comparable key)
		{
			AttributedString as = new AttributedString(generateSectionLabel(ds, key));
			return (as);
		}
	}

	/**
	 * Create a meter chart
	 * 
	 * @param qrs
	 *            Query result set
	 * @param co
	 *            Chart options
	 * @return
	 */
	private static JCommonDrawableRenderer meterChart(QueryResultSet qrs, ChartOptions co)
	{
		if (co.series == null || co.series.size() == 0)
			return null;
		ValueDataset vds = createValueDataset(co.series.get(0), qrs);
		if (vds == null)
		{
			return null;
		}
		MeterPlot mp = new MeterPlot(vds);
		DialShape dialShape = DialShape.CHORD;
		mp.setDialShape(dialShape);
		mp.setDialBackgroundPaint(new Color(245, 245, 245));
		mp.setDrawBorder(false);
		mp.setNeedlePaint(Color.BLACK);
		mp.setTickLabelFont(new Font("SansSerif", Font.PLAIN, 10));
		mp.setTickLabelPaint(Color.BLACK);
		mp.setTickPaint(Color.BLACK);
		mp.setValuePaint(Color.BLACK);
		mp.setUnits("");
		mp.setRange(new Range(co.meterMin, co.meterMax));
		mp.setTickLabelsVisible(true);
		mp.setTickSize(co.meterMax / 10);
		String format = getValueFormat(qrs, co);
		if (format != null)
			mp.setTickLabelFormat(new DecimalFormat(format));
		MeterInterval mi = new MeterInterval("Label1", new Range(co.meterMin, co.meterCritical), Color.BLACK, new BasicStroke(3), new Color(230, 0, 0, 150));
		mp.addInterval(mi);
		mi = new MeterInterval("Label2", new Range(co.meterCritical, co.meterNormal), Color.BLACK, new BasicStroke(3), new Color(200, 200, 0, 150));
		mp.addInterval(mi);
		mi = new MeterInterval("Label3", new Range(co.meterNormal, co.meterMax), Color.BLACK, new BasicStroke(3), new Color(0, 200, 0, 150));
		mp.addInterval(mi);
		JFreeChart chart = new JFreeChart(co.title, new Font("SansSerif", Font.BOLD, ChartOptions.DEFAULT_FONT_SIZE), mp, false);
		setChartOptions(chart, co);
		FlashRenderer jcdr = new FlashRenderer(chart);
		jcdr.setQueryResultSet(qrs);
		jcdr.setChartOptions(co);
		jcdr.setVds(vds);
		return (jcdr);
	}

	private static JCommonDrawableRenderer meterChart(JRDataSource qrs, String chartOptions)
	{
		return (meterChart((QueryResultSet) qrs, new ChartOptions(chartOptions)));
	}

	/**
	 * Parse URL series and category strings to return implied filters
	 * 
	 * @param co
	 *            Chart Options
	 * @param seriesStr
	 *            String with series filter
	 * @param categoryStr
	 *            String with category filter
	 * @param measureColumns
	 *            Measure columns in data set (need to be ignored in series list)
	 * @return
	 */
	public static HashMap<String, String> getFilterMap(ChartOptions co, String seriesStr, String categoryStr, List<String> measureColumns)
	{
		HashMap<String, String> fmap = new LinkedHashMap<String, String>();
		/*
		 * Make sure the string is not the name of the series, but the value
		 */
		if (co.series.indexOf(seriesStr) < 0)
		{
			/*
			 * One series is always a measure
			 */
			if (co.series.size() > 2 && (seriesStr != null) && (seriesStr.indexOf('/') > 0))
			{
				StringTokenizer st = new StringTokenizer(seriesStr, "/");
				if (st.countTokens() == co.series.size())
				{
					int count = 0;
					while (st.hasMoreTokens())
					{
						String t = st.nextToken();
						while ((count < co.series.size()) && (measureColumns.indexOf(co.series.get(count)) >= 0))
							count++;
						if (count < co.series.size())
							fmap.put(co.series.get(count++), t);
					}
				}
			} else if (seriesStr != null)
			{
				int count = 0;
				while ((count < co.series.size()) && (measureColumns.indexOf(co.series.get(count)) >= 0))
					count++;
				if (count < co.series.size())
					fmap.put(co.series.get(count), seriesStr);
			}
		}
		/*
		 * Make sure the string is not the name of the categories, but the value
		 */
		if (co.categories.indexOf(categoryStr) < 0)
		{
			if (co.categories.size() > 1)
			{
				StringTokenizer st = new StringTokenizer(categoryStr, "/");
				if (st.countTokens() == co.categories.size())
				{
					int count = 0;
					while (st.hasMoreTokens())
					{
						String t = st.nextToken();
						fmap.put(co.categories.get(count++), t);
					}
				}
			} else if (co.categories.size() > 0)
			{
				fmap.put(co.categories.get(0), categoryStr);
			} else if (co.seriesGroups.size() > 1)
			{
				fmap.put("SeriesGroup", categoryStr);
			}
		}
		return (fmap);
	}

	/**
	 * Helper method to create charts using simple parameters passed from reports
	 * 
	 * @param type
	 *            (pie,line,bar,stackedbar,area,stackedarea,scatterplot,contourplot,bubblechart,spiderwebplot,meterplot,heatmap
	 *            )
	 * @param dataSource
	 *            Either a query result set (existing data source to clone)
	 * @param queryString
	 *            Query string to use. If null, use the data set from the data source
	 * @param chartOptions
	 *            String specifying chart options
	 * @return
	 * @throws Exception 
	 */
	public static JCommonDrawableRenderer newChart(String type, JRDataSource dataSource, String queryString, String chartOptions, String targetURL,
			DrillType drillType)  throws Exception {
		return newChart(type, dataSource, queryString, chartOptions, targetURL, drillType.toString());
	}
	public static JCommonDrawableRenderer newChart(String type, JRDataSource dataSource, String queryString, String chartOptions, String targetURL,
			String drillType) throws Exception
	{
		return newChart(type, dataSource, queryString, chartOptions, targetURL, drillType, null, null);
	}

	public static JCommonDrawableRenderer newChart(String type, String[] colNames, int[] colTypes, int[] colDataTypes, String[] columnFormats, String[] aggRules, Object[][] rows, String chartOptions, String targetURL,
			DrillType drillType)  throws Exception {
		return newChart(type, colNames, colTypes, colDataTypes, columnFormats, aggRules, rows, chartOptions, targetURL,
				drillType.toString());
	}
	public static JCommonDrawableRenderer newChart(String type, String[] colNames, int[] colTypes, int[] colDataTypes, String[] columnFormats, String[] aggRules, Object[][] rows, String chartOptions, String targetURL,
			String drillType) throws Exception
	{
		QueryResultSet qrs = new QueryResultSet();
		qrs.setColumnNames(colNames);
		qrs.setDisplayNames(colNames);
		qrs.setColumnDataTypes(colDataTypes);
		qrs.setColumnTypes(colTypes);
		qrs.setAggregationRules(aggRules);
		qrs.setColumnFormats(columnFormats);
		qrs.setRows(rows);
		return newChart(type, qrs, chartOptions, null, targetURL, drillType);
	}

	public static JCommonDrawableRenderer newChart(String type, JRDataSource dataSource, String queryString, String chartOptions, String targetURL,
			DrillType drillType, String JSFunc, String drillTo) throws Exception
	{
		return newChart(type, dataSource, queryString, chartOptions, targetURL,
				drillType.toString(), JSFunc, drillTo);
	}
	public static JCommonDrawableRenderer newChart(String type, JRDataSource dataSource, String queryString, String chartOptions, String targetURL,
			String drillType, String JSFunc, String drillTo) throws Exception
	{
		try
		{
			JasperDataSourceProvider jdsp = null;
			if (QueryResultSet.class.isInstance(dataSource) && queryString != null)
				jdsp = ((IJasperDataSourceProvider) dataSource).getJasperDataSourceProvider();
			if (SingleRowDataSource.class.isInstance(dataSource))
				jdsp = ((IJasperDataSourceProvider) dataSource).getJasperDataSourceProvider();
			if (jdsp == null && queryString != null)
				jdsp = new JasperDataSourceProvider(com.successmetricsinc.UserBean.getParentThreadUserBean().getRepository());
			type = type.toLowerCase();

			JRDataSource jrds = queryString == null ? dataSource : jdsp.create(queryString);
			if (jrds == null)
			{
				return null;
			}
			if (type.equals("column"))
			{
				chartOptions += ";charttype=column";
				return (barChart(jrds, chartOptions, targetURL, false, drillType, JSFunc, drillTo));
			} else if (type.equals("bar"))
			{
				chartOptions += ";charttype=bar";
				return (barChart(jrds, chartOptions, targetURL, false, drillType, JSFunc, drillTo));
			} else if (type.equals("funnel"))
			{
				chartOptions += ";charttype=funnel";
				return (funnelChart(jrds, chartOptions, targetURL, drillType, JSFunc, drillTo, false));		
			} else if (type.equals("pyramid"))
			{
				chartOptions += ";charttype=pyramid";
				return (funnelChart(jrds, chartOptions, targetURL, drillType, JSFunc, drillTo, true));	
			} else if (type.equals("treemap"))
			{
				chartOptions += ";charttype=treemap";
				return (treeMapChart(jrds, chartOptions, targetURL, drillType, JSFunc, drillTo));												
			} else if (type.equals("line"))
			{
				chartOptions += ";charttype=line";
				return (lineChart(jrds, chartOptions, targetURL, drillType, JSFunc, drillTo));
			} else if (type.equals("barline"))
			{
				chartOptions += ";charttype=barline";
				return (barLineChart(jrds, chartOptions, targetURL, false, drillType, JSFunc, drillTo));
			} else if (type.equals("pie"))
			{
				chartOptions += ";charttype=pie";
				return (pieChart(jrds, chartOptions, targetURL, drillType, JSFunc, drillTo));
			} else if (type.equals("stackedbar"))
			{
				chartOptions += ";charttype=stackedbar;stacked=true";
				return (barChart(jrds, chartOptions, targetURL, true, drillType, JSFunc, drillTo));
			} else if (type.equals("stackedcolumn"))
			{
				chartOptions += ";charttype=stackedcolumn;stacked=true";
				return (barChart(jrds, chartOptions, targetURL, true, drillType, JSFunc, drillTo));
			} else if (type.equals("stackedcolumnline"))
			{
				chartOptions += ";charttype=stackedcolumnline;stacked=true";
				return (barLineChart(jrds, chartOptions, targetURL, true, drillType, JSFunc, drillTo));
			} else if (type.equals("area"))
			{
				chartOptions += ";charttype=area";
				return (areaChart(jrds, chartOptions, targetURL, false));
			} else if (type.equals("stackedarea"))
			{
				chartOptions += ";charttype=stackedarea;stacked=true";
				return (areaChart(jrds, chartOptions, targetURL, true));
			} else if (type.equals("scatterplot"))
			{
				chartOptions += ";charttype=scatterplot";
				return (scatterPlot(jrds, chartOptions, targetURL, drillType, JSFunc, drillTo));
			} else if (type.equals("bubblechart"))
			{
				chartOptions += ";charttype=bubblechart";
				return (bubbleChart((QueryResultSet)jrds, new ChartOptions(chartOptions), targetURL, drillType, JSFunc, drillTo));
			} else if (type.equals("spiderwebplot"))
			{
				chartOptions += ";charttype=spiderwebplot";
				return (spiderWebPlot(jrds, chartOptions, targetURL));
			} else if (type.equals("meterplot"))
			{
				chartOptions += ";charttype=meterplot";
				return (meterChart(jrds, chartOptions));
			} else if (type.equals("heatmap"))
			{
				chartOptions += ";charttype=heatmap";
				return heatMapChart(jrds, chartOptions, targetURL, null, null, null);
			} else if (type.equals("map")) {
				chartOptions += ";chartytpe=map";
				return geoMapChart(jrds, chartOptions, targetURL, drillType, JSFunc, drillTo);
			} else
				return (null);
		} catch (Exception e)
		{
			if (QueryCancelledException.class.isInstance(e.getCause()) || QueryCancelledException.class.isInstance(e))
			{
				logger.debug("Query cancelled by end user.");
			} else if (QueryTimeoutException.class.isInstance(e.getCause()) || QueryTimeoutException.class.isInstance(e))
			{
				logger.debug("Query timed out.");
			} else
			{
				logger.error("Error creating chart: " + e.toString(), e);
				if (queryString != null) {
					logger.error("Chart query is " + queryString);
				}
			}
		}
		return null;
	}


	public static JCommonDrawableRenderer newChart(String type, JRDataSource dataSource, String queryString, String chartOptions) throws Exception
	{
		return (newChart(type, dataSource, queryString, chartOptions, null, Chart.DRILL_TYPE_NONE, null, null));
	}

	public static JCommonDrawableRenderer newChart(String type, JRDataSource dataSource, String chartOptions, String targetURL, DrillType drillType,
			String JSFunc, String drillTo) throws Exception
	{
		return newChart(type, dataSource, chartOptions, targetURL, drillType.toString(),
				JSFunc, drillTo);
	}
	public static JCommonDrawableRenderer newChart(String type, JRDataSource dataSource, String chartOptions, String targetURL, String drillType,
			String JSFunc, String drillTo) throws Exception
	{
		return (newChart(type, dataSource, null, chartOptions, targetURL, drillType, JSFunc, drillTo));
	}

	/**
	 * Helper method to create charts using simple parameters passed from reports
	 * 
	 * @param type
	 *            (pie,line,bar,stackedbar,area,stackedarea,scatterplot)
	 * @param queryString
	 * @param chartOptions
	 * @return
	 * @throws Exception 
	 */
	public static JCommonDrawableRenderer newChart(String type, String queryString, String chartOptions) throws Exception
	{
		return (newChart(type, null, queryString, chartOptions));
	}

	/**
	 * get the format for the first measure in the result set
	 */
	private static String getFirstSeriesFormat(QueryResultSet qrs, ChartOptions co)
	{
		String format = null;
		List<String> colNames = getColumnNames(qrs);
		Map<String, String> formatMap = co.getFormatMap();
		for (String s : co.series)
		{
			int index = colNames.indexOf(s);
			if (index >= 0)
			{
				format = qrs.getColumnFormats()[index];
				if (formatMap != null && formatMap.containsKey(s))
					format = formatMap.get(s);
				break;
			}
		}
		return format;
	}
}
