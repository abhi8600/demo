/**
 * 
 */
package com.successmetricsinc.chart;

import java.awt.Color;
import java.awt.LinearGradientPaint;
import java.awt.MultipleGradientPaint;
import java.awt.Paint;
import java.awt.PaintContext;
import java.awt.RadialGradientPaint;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.MultipleGradientPaint.CycleMethod;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.ColorModel;

/**
 * @author Brad Peters
 * 
 */
public class ChartGradientPaint implements Paint
{
	private MultipleGradientPaint mgp;
	private float[] fractions;
	private Color[] colors;
	private Color baseColor;
	private CycleMethod cycleMethod;
	private double startAngle;
	private double endAngle;
	private Radius radius;
	private boolean resetRadius;

	public enum Direction
	{
		horizontal, vertical, radial
	};

	public static class Radius
	{
		double value;
	}
	private Direction direction;

	/**
	 * @param horizontal
	 *            Whether the object is extending horizontally or vertically
	 * @param fractions
	 *            3 floats indicating start, mid and stop
	 * @param colors
	 *            colors to use
	 * @param cycleMethod
	 * @param colorSpace
	 */
	public ChartGradientPaint(Direction direction, float[] fractions, Color[] colors, CycleMethod cycleMethod, Color baseColor)
	{
		this.fractions = fractions;
		this.colors = colors;
		this.cycleMethod = cycleMethod;
		this.direction = direction;
		this.baseColor = baseColor;
	}

	public int getTransparency()
	{
		return (java.awt.Transparency.OPAQUE);
	}

	public PaintContext createContext(ColorModel cm, Rectangle deviceBounds, Rectangle2D userBounds, AffineTransform at, RenderingHints ht)
	{
		if (direction == Direction.horizontal)
		{
			Point2D start = new Point2D.Double(0, userBounds.getY());
			Point2D end = new Point2D.Double(0, userBounds.getMaxY());
			if (start.distance(end) == 0)
				return (colors[1].createContext(cm, deviceBounds, userBounds, at, ht));
			mgp = new LinearGradientPaint(end, start, fractions, colors, cycleMethod);
		} else if (direction == Direction.vertical)
		{
			Point2D start = new Point2D.Double(userBounds.getX(), 0);
			Point2D end = new Point2D.Double(userBounds.getMaxX(), 0);
			if (start.distance(end) == 0)
				return (colors[1].createContext(cm, deviceBounds, userBounds, at, ht));
			mgp = new LinearGradientPaint(start, end, fractions, colors, cycleMethod);
		} else if (direction == Direction.radial)
		{
			double x = userBounds.getX();
			double y = userBounds.getY();
			double width = userBounds.getWidth();
			double height = userBounds.getHeight();
			if (startAngle < Math.PI / 2)
			{
				y = userBounds.getMaxY();
				if (endAngle > Math.PI / 2)
				{
					if (endAngle > Math.PI)
					{
						if (resetRadius)
							radius.value = height / 2;
						if (endAngle > Math.PI * 3 / 2)
						{
							x += radius.value;
							double s = Math.sin((Math.PI / 2) - startAngle);
							double s2 = Math.sin(endAngle - (3 * Math.PI / 2));
							y = userBounds.getMinY() + Math.max(s * radius.value, s2 * radius.value);
						} else
						{
							double s = Math.sin(endAngle - Math.PI);
							x += s * radius.value;
							s = Math.sin((Math.PI / 2) - startAngle);
							y = userBounds.getMinY() + s * radius.value;
						}
					} else
					{
						double s = Math.sin(endAngle - (Math.PI / 2));
						y -= s * width;
						if (resetRadius)
							radius.value = width;
					}
				} else if (resetRadius)
					radius.value = height;
			} else if (startAngle < Math.PI)
			{
				if (endAngle > Math.PI)
				{
					if (endAngle > Math.PI * 3 / 2)
					{
						double s = Math.sin(endAngle - (Math.PI * 3 / 2));
						y += s * radius.value;
						x = userBounds.getX() + radius.value;
					} else
					{
						double s = Math.sin(endAngle - (Math.PI));
						x += s * radius.value;
					}
				}
			} else if (startAngle < Math.PI * 3 / 2)
			{
				x = userBounds.getMaxX();
				if (endAngle > Math.PI * 3 / 2)
				{
					double s = Math.sin(endAngle - (Math.PI * 3 / 2));
					y += s * radius.value;
				}
			} else
			{
				x = userBounds.getMaxX();
				y = userBounds.getMaxY();
			}
			Point2D center = new Point2D.Double(x, y);
			mgp = new RadialGradientPaint(center, (float) radius.value, fractions, colors, cycleMethod);
		}
		PaintContext pc = mgp.createContext(cm, deviceBounds, userBounds, at, ht);
		return (pc);
	}

	/**
	 * @return the startAngle
	 */
	public double getStartAngle()
	{
		return startAngle;
	}

	/**
	 * @param startAngle
	 *            the startAngle to set
	 */
	public void setStartAngle(double startAngle)
	{
		this.startAngle = startAngle;
	}

	/**
	 * @return the endAngle
	 */
	public double getEndAngle()
	{
		return endAngle;
	}

	/**
	 * @param endAngle
	 *            the endAngle to set
	 */
	public void setEndAngle(double endAngle)
	{
		this.endAngle = endAngle;
	}

	/**
	 * @return the radius
	 */
	public Radius getRadius()
	{
		return radius;
	}

	/**
	 * @param radius
	 *            the radius to set
	 */
	public void setRadius(Radius radius)
	{
		this.radius = radius;
	}

	/**
	 * @return the resetRadius
	 */
	public boolean isResetRadius()
	{
		return resetRadius;
	}

	/**
	 * @param resetRadius
	 *            the resetRadius to set
	 */
	public void setResetRadius(boolean resetRadius)
	{
		this.resetRadius = resetRadius;
	}

	public Color getBaseColor()
	{
		return baseColor;
	}

	public void setBaseColor(Color baseColor)
	{
		this.baseColor = baseColor;
	}
}
