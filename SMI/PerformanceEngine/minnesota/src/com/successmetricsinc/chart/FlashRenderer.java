/**
 * $Id: FlashRenderer.java,v 1.175 2012-03-21 23:43:22 ricks Exp $
 *
 * Copyright (C) 2007-2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.chart;

import java.awt.Color;
import java.awt.Font;
import java.awt.Paint;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.Format;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.export.XMLExporter;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.DrawingSupplier;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.urls.CategoryURLGenerator;
import org.jfree.chart.urls.PieURLGenerator;
import org.jfree.chart.urls.XYURLGenerator;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.data.general.ValueDataset;
import org.jfree.data.xy.SeriesXYZDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.Drawable;
import org.jfree.ui.RectangleEdge;

import com.successmetricsinc.UserBean;
import com.successmetricsinc.chart.Chart.JSGen;
import com.successmetricsinc.chart.ChartOptions.ChartType;
import com.successmetricsinc.query.QueryColumn;
import com.successmetricsinc.query.QueryResultSet;
import com.successmetricsinc.transformation.integer.ToTime;
import com.successmetricsinc.util.ColorUtils;
import com.successmetricsinc.util.DebugUtils;
import com.successmetricsinc.util.Util;

public class FlashRenderer extends JCommonDrawableRenderer implements XMLExporter
{
	/** 
	 * Default location of ChartRenderer.swf file relative to SMIWeb's tomcat directory.
	 * This is of use for instances of Tomcat that are started from the command line.
	 * {@link ChartRenderer#ChartRenderer(String)} to see how the default values are used.
	 * TODO: Should be externalized to config file. 
	 */
	public static final String FLASH_FILE_PATH = "webapps/SMIWeb/swf/ChartRenderer.swf";
	public static final String SMI_FLASH_FILE_PATH = "tomcat" + File.separator + FLASH_FILE_PATH;
	/** System property containing the path to the SMIWeb directory. */
	public static final String SMI_INSTALL_DIR = "smi.home";
	/** Environment variable containing the full path for the SMIWeb's Tomcat directory. */
	public static final String CATALINA_HOME = "catalina.home";
	private static final long serialVersionUID = 1L;
    public static final int MAX_LABEL_LENGTH = 50;
    public static final int LABEL_END = 5;
	private static final double PIE_LABEL_THRESHOLD = 0.01; // do not display values less than this percentage
	public static final String XML_EOL = "&#xD;";
	private static final String EMPTY_CHART = "<charts><chart><data></data></chart></charts></anychart>";
	
	private static Logger logger = Logger.getLogger(FlashRenderer.class);
	private static boolean CREATE_DEBUG_XML = false;
	
	/* limits to stop AnyChart from spinning forever or drawing crap */
	private static final int MAX_FUNNEL_PYRAMID_DATA_POINTS = 100;
	private static final int MAX_PIE_DATA_POINTS = 100;
	private static final int MAX_BUBBLE_SCATTER_DATA_POINTS = 10000;
	private static final int MAX_NON_LINE_DATA_POINTS = 1000;
	
	private static String chartServerSwfFilepath; 

	private ChartOptions co;
	private CategoryDataset cds;
	private CategoryDataset cds2;
	private XYDataset xyds;
	private PieDataset pds;
	private ValueDataset vds;
	private Drawable drawable2;
	private QueryResultSet qrs;
	private boolean renderFlash = true;
	private double scaleFactor;
	private Map<String, Color> categoryKeyColorMap = null;
	private boolean animation = false;
	/**
	 * This constructor is the main one used in the non-test classes (@see Chart).
	 * @param drawable is the drawable context to use.
	 */
	public FlashRenderer(Drawable drawable) {
		this(drawable, getDefaultFlashFilePath());
	}

	/**
	 * This constructor is used mainly in testing.  It requires a filepath
	 * to the ChartRenderer.swf file that overrides the default SMIWeb Tomcat value.
	 * @param drawable is the drawable context to use.
	 * @param flashFilePath is the overriding filepath for the .swf file.
	 */
	protected FlashRenderer(Drawable drawable, final String flashFilePath) {
		super(drawable, flashFilePath);
		this.drawable = drawable;
	}

	/**
	 * {@link Chart#heatMapChart} is currently the only user of this constructor.
	 * TODO: Can this be refactored to use the {@link #FlashRenderer(Drawable)} constructor? 
	 * @param qrs is the QueryResultSet instance.
	 * @param co is the ChartOptions instance.
	 * @param imageMap is a boolean indicating whether or not an imageMap should be used.
	 */
	public FlashRenderer(final QueryResultSet qrs, final ChartOptions co, final boolean imageMap) {
		this(new JFreeChart(new PiePlot()));  // Can this be any kind of plot?  Does AnyChart even care?
		List<String> colNames = Chart.getColumnNames(qrs);
		int[] colTypes = qrs.getColumnTypes();
		Map<String, Format> formats = Chart.buildFormats(co.getFormatMap(), colNames, colTypes, qrs.getColumnDataTypes(), co.rangeTickmarkFormat);
		this.qrs = qrs.getDateTimeFormattedQRS(formats, colNames);
		this.co = co;
		setImageMap(imageMap);
	}

	public void setAnimation(boolean animation) {
		this.animation = animation;
	}

	public byte getImageType() {
		byte result = super.getImageType();
		if (renderFlash)
			return ((byte) 255);
		return (result);
	}
	
	public ChartOptions getChartOptions() {
		return co;
	}

	public void setChartOptions(ChartOptions co) {
		this.co = co;
	}

	public CategoryDataset getCategoryDataSet() {
		return cds;
	}

	public void setCategoryDataset(CategoryDataset cds) {
		this.cds = cds;
	}

	public void setQueryResultSet(QueryResultSet qrs) {
		this.qrs = qrs;
	}

	public String getXML() {
		return (getXML(1));
	}
	
	/**
	 * chart types supported by anychart
	 * @return
	 */
	public boolean isSupportedByAnyChart()
	{
		return (co.type == ChartType.Bar || co.type == ChartType.Column || co.type == ChartType.Pie || co.type == ChartType.Doughnut || 
				co.type == ChartType.Line || co.type == ChartType.BarLine || co.type == ChartType.StackedBar ||
				co.type == ChartType.StackedColumn || co.type == ChartType.StackedColumnLine || co.type == ChartType.Area || 
				co.type == ChartType.StackedArea || co.type == ChartType.Scatterplot || co.type == ChartType.BubbleChart ||
				co.type == ChartType.MeterPlot || co.type == ChartType.Funnel || co.type == ChartType.Pyramid ||
				co.type == ChartType.TreeMap || co.type == ChartType.HeatMap || co.type == ChartType.Map || co.type == ChartType.Radar);
	}
	
	/**
	 * chart types that use the jfreechart category data set (CDS)
	 * @return
	 */
	public boolean usesCategoryDataSet()
	{
		return (co.type == ChartType.Bar || co.type == ChartType.Column 
				|| co.type == ChartType.StackedBar || co.type == ChartType.StackedColumn || co.type == ChartType.Line
				|| co.type == ChartType.Funnel || co.type == ChartType.Pyramid || co.type == ChartType.TreeMap
				|| co.type == ChartType.Radar);
	}

	/**
	 * generate an anychart xml representation of the chart
	 */
	public String getXML(double scaleFactor)
	{
		return getXML(scaleFactor, false, false);
	}
	
	public String getXML(double scaleFactor, boolean animation, boolean isPrinted) {
		try
		{
			this.setAnimation(animation);
			this.scaleFactor = scaleFactor;
			if (isSupportedByAnyChart())
			{
				StringBuilder sb = new StringBuilder();
				sb.append("<anychart>");
				sb.append("<margin"
						+ " left=\"" + co.leftOutsideMargin + "\""
						+ " top=\"" + co.topOutsideMargin + "\""
						+ " right=\"" + co.rightOutsideMargin + "\""
						+ " bottom=\"" + co.bottomOutsideMargin + "\""
						+ "/>");
				sb.append("<settings><animation enabled=\"" + animation
						+ "\" /><no_data show_waiting_animation=\"False\"><label enabled=\"True\"><text>No data or too much data, please check your query.</text></label></no_data>");
				if (isPrinted) {
					sb.append("<maps path_type=\"Absolute\" path=\"");
					sb.append(ChartServerContext.chartServerAmapFilepath);
					sb.append("\" />");
				}
				else {
					sb.append("<maps path_type=\"RelativeToSWF\"/>");
				}
				sb.append("</settings>");
				if (drawable instanceof JFreeChart)
				{
					Plot p = ((JFreeChart) drawable).getPlot();
					DrawingSupplier ds = p.getDrawingSupplier();
					if (ds instanceof ChartColors)
					{
						((ChartColors) ds).resetCurColor();
					}
				}

				// handle gauges
				if (co.type == ChartType.MeterPlot)
					addMeterPlot(sb);
				else
				{		
					// check for no data or too much data
					// - for charts that use CDS
					if (usesCategoryDataSet())
					{
						int rc = cds.getRowCount();
						int cc = cds.getColumnCount();
						if (rc == 0 || cc == 0)
						{
							sb.append(EMPTY_CHART);
							return sb.toString();
						}
						if (co.type == ChartType.Funnel || co.type == ChartType.Pyramid)
						{
							// anything more than this and funnel/pyramid charts are unreadable
							if (cc > MAX_FUNNEL_PYRAMID_DATA_POINTS)
							{
								sb.append(EMPTY_CHART);
								return sb.toString();
							}
						}
						if (co.type != ChartType.Line)
						{
							// except for line charts, anything more than this is unreadable and takes too long
							if (cc > MAX_NON_LINE_DATA_POINTS)
							{
								sb.append(EMPTY_CHART);
								return sb.toString();
							}
						}
					}
					// - for charts that use PDS
					if (co.type == ChartType.Pie)
					{
						int ic = pds.getItemCount();
						if (ic == 0 || ic > MAX_PIE_DATA_POINTS)
						{
							sb.append(EMPTY_CHART);
							return sb.toString();
						}
					}
					if (co.type == ChartType.BubbleChart || co.type == ChartType.Scatterplot)
					{
						int ic = xyds.getItemCount(0);
						// anything more than this and bubble/scatter plots are unreadable
						if (ic == 0 || ic > MAX_BUBBLE_SCATTER_DATA_POINTS)
						{
							sb.append(EMPTY_CHART);
							return sb.toString();
						}
					}
					sb.append("<charts>");
					if (co.type == ChartType.Bar || co.type == ChartType.Column || co.type == ChartType.StackedBar || co.type == ChartType.StackedColumn)
						addBarChart(sb);
					else if (co.type == ChartType.BarLine || co.type == ChartType.StackedColumnLine)
						addBarLineChart(sb);
					else if (co.type == ChartType.Pie)
						addPieChart(sb);
					else if (co.type == ChartType.Line)
						addLineChart(sb);
					else if (co.type == ChartType.Area || co.type == ChartType.StackedArea)
						addAreaChart(sb);
					else if (co.type == ChartType.Scatterplot)
						addScatterPlot(sb);
					else if (co.type == ChartType.BubbleChart)
						addBubblePlot(sb);
					else if (co.type == ChartType.Funnel || co.type == ChartType.Pyramid)
						addFunnelChart(sb);
					else if (co.type == ChartType.TreeMap)
						addTreeMapChart(sb);
					else if (co.type == ChartType.HeatMap)
						addHeatMapChart(sb);
					else if (co.type == ChartType.Map)
						addGeoMapChart(sb);
					else if (co.type == ChartType.Radar)
						addRadarChart(sb);
					sb.append("</charts>");
				}
				sb.append("</anychart>");
				if (CREATE_DEBUG_XML)
				{
					try
					{
						logger.debug("Writing chart XML to c:\\output.xml");
						FileWriter writer = new FileWriter("c:\\output.xml");
						writer.write(sb.toString());
						writer.close();
					} catch (FileNotFoundException e)
					{
						logger.error(e, e);
					} catch (IOException e)
					{
						logger.error(e, e);
					}
				}
				return sb.toString();
			} 
		}
		catch (Exception ex)
		{
			logger.error(ex.getMessage(), ex);
		}
		StringBuilder sb = new StringBuilder();
		sb.append("<anychart><settings><animation enabled=\"False\"/><no_data show_waiting_animation=\"False\">");
		sb.append("<label enabled=\"True\"><text>Internal error, please contact support</text></label></no_data></settings>");
		sb.append("<charts><chart><data></data></chart></charts></anychart>");
		return sb.toString();
	}
	
	private void addFunnelChart(StringBuilder sb) {
		sb.append("<chart plot_type=\"Funnel\">");
		addChartSettings(sb, false, false);
		addFunnelDataPlotSettings(sb);
		addCategoryData(sb);
		sb.append("</chart>");
	}
	
	private void addRadarChart(StringBuilder sb) {
		sb.append("<chart plot_type=\"Radar\">");
		addChartSettings(sb, false, false);
		// addFunnelDataPlotSettings(sb);
		addCategoryData(sb);
		sb.append("</chart>");
	}
	
	private void addTreeMapChart(StringBuilder sb) {
		sb.append("<chart plot_type=\"TreeMap\">");
		addChartSettings(sb, false, false);
		addTreeMapDataPlotSettings(sb);
		addThresholdSettings(sb);
		addCategoryData(sb);
		sb.append("</chart>");
	}

	private void addHeatMapChart(StringBuilder sb) {
		sb.append("<chart plot_type=\"HeatMap\">");
		addChartSettings(sb, false, false);
		addHeatMapDataPlotSettings(sb);
		addThresholdSettings(sb);
		addHeatMapData(sb);
		sb.append("</chart>");
	}
	
	private void addGeoMapChart(StringBuilder sb) throws JRException {
		sb.append("<chart plot_type=\"Map\">");
		addChartSettings(sb, false, false);
		addGeoMapDataPlotSettings(sb);
		addThresholdSettings(sb);
		addGeoMapData(sb);
		sb.append("</chart>");
	}
	
	private void addGeoMapDataPlotSettings(StringBuilder sb) throws JRException {
		
		sb.append("<data_plot_settings><map_series source=\"");
		sb.append(co.getMapLocation(qrs, co.geoMapType, co.subMapName, co.mapNameColumn));
		sb.append(".amap\" id_column=\"");
		sb.append(co.mapColumnName);
		sb.append("\">");
		if (co.hideCoordinateGrid) 
			sb.append("<grid><parallels enabled=\"False\"/><meridians enabled=\"False\"/><background enabled=\"False\"/></grid>");
		Map<String, String> formatMap = co.getFormatMap();
		String seriesName = co.series.get(0);
		String format = getNumberFormat("Value", formatMap != null ? formatMap.get(seriesName) : null);
		String nameFormat = getNumberFormat("Name", formatMap != null ? formatMap.get(co.categories.get(0)) : null);
		sb.append("<undefined_map_region><interactivity hoverable=\"False\" use_hand_cursor=\"False\" allow_select=\"False\"/></undefined_map_region>");
		sb.append("<defined_map_region>");
		sb.append("<tooltip_settings enabled=\"True\"><format>" + nameFormat + " - " + format + "</format></tooltip_settings>");
		if (co.showGeoMapLabel) {
			sb.append("<label_settings enabled=\"True\"><format>" + format + "</format><position anchor=\"Center\" valign=\"Center\" halign=\"Center\"/></label_settings>");
		}
		sb.append("</defined_map_region>");
		sb.append("<zoom minimum=\"1\" maximum=\"100\"");
		// remove -- test for zoom
//		sb.append(" factor=\"15\" geo_x=\"-122.23\" geo_y=\"37.37\" ");
		sb.append(" />");
		sb.append("<projection type=\"");
		sb.append(co.mapProjection);
		sb.append("\" />");
		sb.append("</map_series>");
		sb.append("</data_plot_settings>");
		
	}
	
	private void addGeoMapData(StringBuilder sb) {
		// Get categories
		PiePlot p = (PiePlot) ((JFreeChart) drawable).getPlot();
		PieURLGenerator urlgen = p.getURLGenerator();
		String[] colNames = qrs.getColumnNames();
		String[] dispNames = qrs.getDisplayNames();
		List<String> columnNames = new ArrayList<String>(colNames.length);
		for (int i = 0; i < colNames.length; i++) {
			if (dispNames[i] == null)
				columnNames.add(colNames[i]);
			else
				columnNames.add(dispNames[i]);
		}
		int category1 = columnNames.indexOf(co.categories.get(0));
		int series = -1;
		if (co.series.size() > 0)
			series = columnNames.indexOf(co.series.get(0));
		
		sb.append("<data>");
		sb.append("<series threshold=\"MyThreshold\">");
		for (Object[] row : qrs.getRows()) {
			String c1 = row[category1] == null ? "null" : row[category1].toString();
			Object d = row[series];
			c1 = cleanLabel(c1);
			String y = (d == null) ? "y=\"\" " : "y=\"" + d.toString() + "\" ";
			sb.append("<point name=\"" + c1 + "\" " + y);
			// heatmap (geomap) no point color - determined by value
			sb.append(">");
			if (urlgen != null)
			{
				if (urlgen instanceof JSGen)
				{
					List<String> call = ((JSGen) urlgen).generateCall(null, c1);
					if (call != null && call.size() >= 4)
					{
						sb.append("<attributes>");
						sb.append("<attribute name=\"DrillType\">" + call.get(2) + "</attribute>");
						sb.append("<attribute name=\"targetURL\">" + call.get(1) + "</attribute>");
						sb.append("<attribute name=\"filters\">" + call.get(3) + "</attribute>");
						sb.append("<attribute name=\"drillBy\">" + call.get(4) + "</attribute>");
						sb.append("<attribute name=\"shortName\">" + c1 + "</attribute>");
						sb.append("</attributes>");
					}
				}
			}
			sb.append("</point>");
		}
		sb.append("</series>");
		sb.append("</data>");
		
	}
	
	/**
	 * create heatmap dataset
	 */
	private void addHeatMapData(StringBuilder sb) {
		// Get categories
		String[] colNames = qrs.getColumnNames();
		String[] dispNames = qrs.getDisplayNames();
		List<String> columnNames = new ArrayList<String>(colNames.length);
		for (int i = 0; i < colNames.length; i++) {
			if (dispNames[i] == null)
				columnNames.add(colNames[i]);
			else
				columnNames.add(dispNames[i]);
		}
		int category1 = columnNames.indexOf(co.categories.get(0));
		int category2 = columnNames.indexOf(co.categories.get(1));
		int series = -1;
		if (co.series.size() > 0)
			series = columnNames.indexOf(co.series.get(0));
		
		sb.append("<data>");
		sb.append("<series threshold=\"MyThreshold\">");
		for (Object[] row : qrs.getRows()) {
			String c1 = row[category1] == null ? "null" : row[category1].toString();
			String c2 = row[category2] == null ? "null" : row[category2].toString();
			Object d = row[series];
			c1 = cleanLabel(c1);
			c2 = cleanLabel(c2);
			String y = (d == null) ? "y=\"\" " : "y=\"" + d.toString() + "\" ";
			sb.append("<point row=\"" + c1 + "\" column=\"" + c2 + "\" " + y);
			// heatmap - no point color
			sb.append("/>");
		}
		sb.append("</series>");
		sb.append("</data>");
	}
	
	public static String cleanLabel(String c1) {
		return Chart.xmlify(c1.replace("\"", ""));
	}

	private void addBarChart(StringBuilder sb) {
		if (co.type == ChartType.Column || co.type == ChartType.StackedColumn || co.type == ChartType.StackedColumnLine)
			sb.append("<chart plot_type=\"CategorizedVertical\">");
		else
			sb.append("<chart plot_type=\"CategorizedHorizontal\">");
		addChartSettings(sb, false, false);
		addBarDataPlotSettings(sb);
		addCategoryData(sb);
		sb.append("</chart>");
	}

	private void addBarLineChart(StringBuilder sb) {
		sb.append("<chart plot_type=\"CategorizedVertical\">");
		addChartSettings(sb, false, false);
		addBarDataPlotSettings(sb);
		addCategoryData(sb);
		sb.append("</chart>");
	}

	private void addAreaChart(StringBuilder sb) {
		if (co.vertical)
			sb.append("<chart plot_type=\"CategorizedVertical\">");
		else
			sb.append("<chart plot_type=\"CategorizedHorizontal\">");
		addChartSettings(sb, false, false);
		addAreaPlotSettings(sb);
		addCategoryData(sb);
		sb.append("</chart>");
	}

	private void addScatterPlot(StringBuilder sb) {
		sb.append("<chart plot_type=\"Scatter\">");
		addScatterPlotSettings(sb);
		addChartSettings(sb, true, false);
		addXYData(sb, false);
		sb.append("</chart>");
	}

	private void addBubblePlot(StringBuilder sb) {
		sb.append("<chart plot_type=\"Scatter\">");
		addBubblePlotSettings(sb);
		addChartSettings(sb, true, false);
		addXYData(sb, true);
		sb.append("</chart>");
	}

	private void addMeterPlot(StringBuilder sb) {
		sb.append("<gauges><gauge>");
		addChartSettings(sb, false, true);
		addMeterPlotSettings(sb);
		sb.append("</gauge></gauges>");
	}

	private void addMeterPlotSettings(StringBuilder sb)
	{
		sb.append("<circular>");
		sb.append("<axis radius=\"42\" start_angle=\"90\" sweep_angle=\"180\" size=\"3\">");
		sb.append("<labels align=\"Outside\" rotate_circular=\"true\" auto_orientation=\"true\">");
		String seriesName = co.series.size() > 0 ? co.series.get(0) : null;
		String format = null;
		Map<String, String> formatMap = co.getFormatMap();
		if (seriesName != null)
		{
			format = getNumberFormat("Value", formatMap != null ? formatMap.get(seriesName) : null);
			sb.append("<format>" + format + "</format>");
		}
		sb.append("</labels>");
		sb.append("<scale_bar><fill color=\"#393939\" /></scale_bar>");
		sb.append("<major_tickmark width=\"2\" length=\"10\"/>");
		sb.append("<minor_tickmark width=\"1\" length=\"3\"><fill color=\"#777777\" /></minor_tickmark>");
		double maj = Math.floor(Math.log10(co.meterMax)) - 1;
		double major = Math.pow(10, maj);
		// sanity check, try for 10 major ticks (the above can sometimes get up to 100 - hard to read :))
		double ticks = co.meterMax / major;
		major = major * Math.floor(ticks / 10);
		double minor = major / 10;
		sb.append("<scale minimum=\"" + co.meterMin + "\" maximum=\"" + co.meterMax + "\" major_interval=\"" + major + "\" minor_interval=\"" + minor + "\"/>");

		if (co.meterNormal != co.meterCritical || co.meterNormal != 0)
		{
			double normal = Math.min(co.meterMax, co.meterNormal);
			normal = Math.max(co.meterMin, normal);
			double critical = Math.min(co.meterMax, co.meterCritical);
			critical = Math.max(co.meterMin, critical);
			double first = 0;
			double second = 0;
			String color1 = null;
			String color2 = null;
			String color3 = null;
			if (co.meterNormal >= co.meterCritical)
			{
				first = critical;
				second = normal;
				color1 = "Red";
				color2 = "Yellow";
				color3 = "Green";
			} else
			{
				first = normal;
				second = critical;
				color1 = "Green";
				color2 = "Yellow";
				color3 = "Red";
			}
			sb.append("<color_ranges><color_range start=\"" + co.meterMin + "\" end=\"" + first + "\" color=\"" + color1
					+ "\" start_size=\"40\" end_size=\"40\" align=\"Inside\" padding=\"5\"><fill opacity=\"0.6\" /></color_range><color_range start=\"" + first
					+ "\" end=\"" + second + "\" color=\"" + color2
					+ "\" start_size=\"40\" end_size=\"40\" align=\"Inside\" padding=\"5\"><fill opacity=\"0.6\" /></color_range><color_range start=\""
					+ second + "\" end=\"" + co.meterMax + "\" color=\"" + color3
					+ "\" start_size=\"40\" end_size=\"40\" align=\"Inside\" padding=\"5\"><fill opacity=\"0.6\" /></color_range></color_ranges>");
		}
		sb.append("</axis>");
		if (vds != null)
		{
			sb.append("<pointers editable=\"false\" allow_select=\"false\">");
			if (co.toolTips)
			{
				sb.append("<tooltip enabled=\"true\">");
				if (seriesName != null)
				{
					format = getNumberFormat("Value", formatMap != null ? formatMap.get(seriesName) : null);
					sb.append("<format>" + format + "</format>");
				}
				sb.append("</tooltip>");
			}
			else
				sb.append("<tooltip enabled=\"False\" />");
			sb.append("<pointer name=\"blah\" type=\"needle\" value=\"" + vds.getValue() + "\">");
			sb.append("</pointer>");
			sb.append("</pointers>");
		}
		sb.append("</circular>");
	}

	private void addScatterPlotSettings(StringBuilder sb)
	{
		sb.append("<data_plot_settings>");
		sb.append("<marker_series>");
		sb.append("<marker_style marker_type=\"TriangleUp\">");
		sb.append("<marker enabled=\"true\" size=\"20\" />");
		sb.append("</marker_style>");
		setXYTooltip(sb, false);
		sb.append("</marker_series>");
		sb.append("</data_plot_settings>");
	}

	private void setXYTooltip(StringBuilder sb, boolean bubble)
	{
		if (!co.toolTips)
		{
			sb.append("<tooltip_settings enabled=\"False\" />");
			return;
		}
		Map<String, String> formatMap = co.getFormatMap();
		sb.append("<tooltip_settings enabled=\"True\"><format>");
		if (co.series.size() > 1)
			sb.append("{%SeriesName}");
		if (co.domainLabels)
		{
			String name1 = co.series.size() >= 1 ? co.series.get(0) : "Domain";
			String format1 = getNumberFormat("XValue", name1 != null ? (formatMap != null ? formatMap.get(name1) : null) : null);			
			sb.append(XML_EOL + "{%XAxisName}: " + format1);
		}
		if (co.rangeLabels)	
		{
			String name2 = co.series.size() >= 2 ? co.series.get(1) : "Range";
			String format2 = getNumberFormat("YValue", name2 != null ? (formatMap != null ? formatMap.get(name2) : null) : null);			
			sb.append(XML_EOL + "{%YAxisName}: " + format2);	
		}
		if (bubble)
		{
			String name3 = co.series.size() >= 3 ? co.series.get(2) : "Value";
			String format3 = getNumberFormat("BubbleSize", name3 != null ? (formatMap != null ? formatMap.get(name3) : null) : null);
			sb.append(XML_EOL + name3 + ": " + format3);
		}
		sb.append("</format></tooltip_settings>");
	}

	private void addBubblePlotSettings(StringBuilder sb)
	{
		sb.append("<data_plot_settings default_series_type=\"Bubble\">");
		sb.append("<bubble_series style=\"Aqua\">");
		sb.append("<bubble_style><states><hover color=\"White\"/></states></bubble_style>");
		setXYTooltip(sb, true);
		sb.append("</bubble_series></data_plot_settings>");
	}

	private void addChartSettings(StringBuilder sb, boolean scatter, boolean gauge)
	{
		sb.append("<chart_settings>");
		addChartBackground(sb);
		if (co.title != null)
		{
			Font f = co.getTitleFont();
			sb.append("<title enabled=\"True\"><text>" + co.title + "</text>" + getFontFormatString(f) + "</title>");
		} else
			sb.append("<title enabled=\"False\"></title>");
		if (!gauge)
		{
			if (scatter)
				addScatterAxis(sb);
			else if (co.type == ChartOptions.ChartType.HeatMap)
				addHeatMapAxis(sb);
			else
				addAxisLabels(sb);
		}
		if (co.legend && !gauge)
		{
			Font f = co.getLegendFont();
			String position = null;
			if (co.legendAnchor == RectangleEdge.BOTTOM)
			{
				position = "Bottom";
//				sb.append("<controls align_bottom_by=\"Chart\" />");
			} else if (co.legendAnchor == RectangleEdge.RIGHT)
			{
				position = "Right";
			} else if (co.legendAnchor == RectangleEdge.TOP)
			{
				position = "Top";
//				sb.append("<controls align_top_by=\"Chart\" />");
			} else if (co.legendAnchor == RectangleEdge.LEFT)
			{
				position = "Left";
			}
			sb.append("<legend enabled=\"True\" padding=\"5\" rows_padding=\"0\" ignore_auto_item=\"True\" columns_padding=\"15\" align=\"Spread\" position=\"" + position + "\">"
					+ "<title enabled=\"False\"/>"
					+ getFontFormatString(f)
					+ "<columns_separator enabled=\"false\" /><rows_separator enabled=\"false\" />" );
			if (co.type == ChartOptions.ChartType.Pie || co.type == ChartOptions.ChartType.Funnel || co.type == ChartOptions.ChartType.Pyramid)
			{
				sb.append("<format>");
				sb.append("{%Icon} {%Name}{enabled:False}");
				sb.append("</format>");
				sb.append("<items><item source=\"Points\"/></items>");
			}
			else if (co.type == ChartOptions.ChartType.TreeMap || co.type == ChartOptions.ChartType.HeatMap || co.type == ChartOptions.ChartType.Map) // thresholds
			{
				sb.append("<format>");
				if (co.customThresholds != null && co.customThresholds.length() > 0) {
					sb.append("{%Icon} {%Name}");
				}
				else
					sb.append("{%Icon} {%RangeMin} - {%RangeMax}");
				
				sb.append("</format>");
				sb.append("<items><item source=\"Thresholds\"/></items>");
			}
			else if ((co.type == ChartType.Bar || co.type == ChartType.StackedBar || 
					co.type == ChartType.Column || co.type == ChartType.StackedColumn) && getGroupKeys(cds, qrs).size() > 1 && seriesContainDimension(cds, qrs, co.series))
			{
				CategoryPlot p = (CategoryPlot) ((JFreeChart) drawable).getPlot();
				DrawingSupplier ds = p.getDrawingSupplier();
				sb.append("<format>");
				sb.append("{%Icon} {%sName}{enabled:False}");
				sb.append("</format><items>");
				if (categoryKeyColorMap == null) {
					categoryKeyColorMap = new HashMap<String, Color>();
				}
				for (int i = 0; i < cds.getRowKeys().size(); i++) {
					String seriesSliceKey = cds.getRowKeys().get(i).toString();
					int index = seriesSliceKey.lastIndexOf('|');
					if (index >= 0) {
						seriesSliceKey = seriesSliceKey.substring(0, index);
					}
					Color clr = categoryKeyColorMap.get(seriesSliceKey);
					if (clr != null) 
					{
						continue;
					}
					else
					{
						clr = getNextColor(ds);
						categoryKeyColorMap.put(seriesSliceKey, clr);
					}
					sb.append("<item><attribute name=\"sName\">");
					sb.append(seriesSliceKey);
					sb.append("</attribute>");
					sb.append("<icon type=\"SeriesIcon\" series_type=\"Bar\" width=\"20\" height=\"10\" color=\"Rgb(");
					sb.append(clr.getRed() + "," + clr.getGreen() + "," + clr.getBlue() + ")\" />");
					sb.append("<text>{%Icon} ");
					sb.append(seriesSliceKey);
					sb.append("</text>");
					sb.append("</item>");
				}
				sb.append("</items>");
				
			}
			else
			{
				if (co.type == ChartOptions.ChartType.Scatterplot)
				{
					sb.append("<icon type=\"Box\"/>");
				}
				sb.append("<format>");
				sb.append("{%Icon} {%Name}{enabled:False}");
				sb.append("</format>");
				sb.append("<items><item source=\"Series\"/></items>");
			}
			sb.append("</legend>");
		}
		if (co.type == ChartType.Map || co.type == ChartType.HeatMap || co.type == ChartType.TreeMap) {
			// add zoom and navigation panels
			sb.append("<controls>");
			if (co.enableMapZooming && co.type == ChartType.Map) {
				sb.append("<zoom_panel enabled=\"True\"/>");
			}
			if (co.enableMapScrolling && co.type == ChartType.Map) {
				sb.append("<navigation_panel enabled=\"True\" />");
			}
			if (co.enableColorSwatch && (co.customThresholds == null || co.customThresholds.length() == 0)) {
				// color swatch only works for automatic thresholds
				sb.append("<color_swatch threshold=\"MyThreshold\" position=\"Bottom\" width=\"100%\">");
				Map<String, String> formatMap = co.getFormatMap();
				String seriesName = co.series.get(0);
				String format = getNumberFormat("Value", formatMap != null ? formatMap.get(seriesName) : null);
				sb.append("<labels enabled=\"True\"><format>" + format + "</format></labels>");
				sb.append("</color_swatch>");
			}
			sb.append("</controls>");
		}
		sb.append("</chart_settings>");
	}
	
	
	private void addAxisPart(String label, String axisTitleFontStr, String labelFontStr, String formatStr, StringBuilder sb)
	{
		if (co.domainLabels && label != null)
		{
			sb.append("<title padding=\"0\"><text>" + label + "</text>");
			if (axisTitleFontStr != null)
			{
				sb.append(axisTitleFontStr);
			}
			sb.append("</title>");
		}
		else
			sb.append("<title enabled=\"false\"/>");
		if (!co.domainTickMarkLabels)
			sb.append("<labels enabled=\"False\"></labels>");
		else
		{
			sb.append("<labels enabled=\"True\">");
			if (formatStr != null)
			{
				sb.append("<format>" + formatStr + "</format>");
			}
			if (labelFontStr != null)
				sb.append(labelFontStr);
			sb.append("</labels>");
		}
		if (co.showMajorGrid == false) {
			sb.append("<major_grid enabled=\"False\" />");
		}
		if (co.showMinorGrid == false) {
			sb.append("<minor_grid enabled=\"False\" />");
		}
		if (co.showMajorTickmarks == false) {
			sb.append("<major_tickmark enabled=\"False\" />");
		}
		if (co.showMinorTickmarks == false) {
			sb.append("<minor_tickmark enabled=\"False\" />");
		}
	}

	private void addHeatMapAxis(StringBuilder sb)	
	{
		Font f = co.getLabelFont();
		String labelFontStr = null;
		if (f != null)
			labelFontStr = this.getFontFormatString(f);
		Font axisTitleFont = null;
		if (f != null)
			axisTitleFont = f.deriveFont(Font.BOLD);
		String axisTitleFontStr = null;
		if (axisTitleFont != null)
			axisTitleFontStr = this.getFontFormatString(axisTitleFont);

		sb.append("<axes>");
		sb.append("<x_axis>");
		String xlabel = co.categories.get(1);
		addAxisPart(xlabel, axisTitleFontStr, labelFontStr, null, sb);
		sb.append("</x_axis>");
		sb.append("<y_axis>");
		String ylabel = co.categories.get(0);
		addAxisPart(ylabel, axisTitleFontStr, labelFontStr, null, sb);
		addAxisScale(sb, co.getMajorInterval(), co.getMinorInterval());
		sb.append("</y_axis>");
		sb.append("</axes>");
	}
	
	
	private void addAxisScale(StringBuilder sb, double majorInterval,
			double minorInterval) {
		if (majorInterval > 0.0) {
			sb.append("<scale major_interval=\"");
			sb.append(majorInterval);
			sb.append("\" ");
			if (minorInterval > 0.0) {
				sb.append("minor_interval=\"");
				sb.append(minorInterval);
				sb.append("\" ");
			}
			sb.append("/>");
		}
		
	}

	private void addQuadrants(StringBuilder sb, String font, boolean labels)
	{
		if (co.quadrantNames == null || co.quadrantNames.size() != 4)
			return;

		// labels true means X axis, false means Y axis
		double min = Double.POSITIVE_INFINITY;
		double max = Double.NEGATIVE_INFINITY;
		for (int j = 0; j < xyds.getSeriesCount(); j++)
		{
			for (int i = 0; i < xyds.getItemCount(j); i++)
			{
				Number val = null;
				if (labels)
					val = xyds.getX(j, i);
				else
					val = xyds.getY(j, i);
				if (val == null)
					val = 0;
				double x = val.doubleValue();
			
				if (x < min)
					min = x;
				if (x > max)
					max = x;
			}
		}
		
		double middle = (max + min) / 2;
		
		// add a slight margin to make sure the markers appear
		double margin = 0.075;
		min = (min > 0 ? min * (1.0 - margin) : min * (1.0 + margin));
		max = (max > 0 ? max * (1.0 + margin) : max * (1.0 - margin));

		double padding = (co.height / 4) * 0.8;
		
		sb.append("<scale minimum=\"" + min + "\" maximum=\"" + max + "\"/>"); // ranges will expand the scale  
		sb.append("<axis_markers>");
		sb.append("<ranges>");
		sb.append("<range minimum=\"" + min + "\" maximum=\"" + middle + "\">");
		// sb.append("<minimum_line enabled=\"false\"/>");
		sb.append("<fill enabled=\"false\"/>");
		if (labels)
		{
			sb.append("<label padding=\"" + padding + "\" position=\"Near\" enabled=\"true\">");
			sb.append("<format>" + co.quadrantNames.get(2) + "</format>");
			if (font != null)
				sb.append(font);			
			sb.append("</label>");
		}
		sb.append("</range>");
		sb.append("<range minimum=\"" + min + "\" maximum=\"" + middle + "\">");
		// sb.append("<minimum_line enabled=\"false\"/>");		
		sb.append("<fill enabled=\"false\"/>");
		if (labels)
		{
			sb.append("<label padding=\"" + padding + "\" position=\"Far\" enabled=\"true\">");
			sb.append("<format>" + co.quadrantNames.get(0) + "</format>");
			if (font != null)
				sb.append(font);
			sb.append("</label>");
		}
		sb.append("</range>");		
		sb.append("<range minimum=\"" + middle + "\" maximum=\"" + max + "\">");
		// sb.append("<maximum_line enabled=\"false\"/>");
		sb.append("<fill enabled=\"false\"/>");
		if (labels)
		{
			sb.append("<label padding=\"" + padding + "\" position=\"Near\" enabled=\"true\">");
			sb.append("<format>" + co.quadrantNames.get(3) + "</format>");
			if (font != null)
				sb.append(font);			
			sb.append("</label>");
		}
		sb.append("</range>");
		sb.append("<range minimum=\"" + middle + "\" maximum=\"" + max + "\">");
		// sb.append("<maximum_line enabled=\"false\"/>");		
		sb.append("<fill enabled=\"false\"/>");
		if (labels)
		{
			sb.append("<label padding=\"" + padding + "\" position=\"Far\" enabled=\"true\">");
			sb.append("<format>" + co.quadrantNames.get(1) + "</format>");
			if (font != null)
				sb.append(font);			
			sb.append("</label>");
		}
		sb.append("</range>");
		sb.append("</ranges>");
		sb.append("</axis_markers>");
	}

	private void addScatterAxis(StringBuilder sb)
	{
		sb.append("<axes>");
		XYPlot p = (XYPlot) ((JFreeChart) drawable).getPlot();
		String domainLabel = p.getDomainAxis().getLabel();
		String rangeLabel = p.getRangeAxis().getLabel();
		Font f = co.getLabelFont();
		Font axisTitleFont = f.deriveFont(Font.BOLD);
		String fontFormatString = getFontFormatString(f);
		String axisTitleFormatString = getFontFormatString(axisTitleFont);
		Map<String, String> formatMap = co.getFormatMap();
		String seriesName = co.series.size() > 0 ? co.series.get(0) : null;
		String format = null;
		if (seriesName != null && formatMap != null && formatMap.get(seriesName) != null)
		{
			format = getNumberFormat("Value", formatMap.get(seriesName));
		}
		sb.append("<x_axis>");
		addAxisPart(domainLabel, axisTitleFormatString, fontFormatString, format, sb);
		addQuadrants(sb, axisTitleFormatString, true);
		sb.append("</x_axis>");
		String seriesName2 = co.series.size() > 1 ? co.series.get(1) : null;
		String format2 = null;
		if (seriesName2 != null && formatMap != null && formatMap.get(seriesName2) != null)
		{
			format2 = getNumberFormat("Value", formatMap.get(seriesName2));
		}
		sb.append("<y_axis>");
		addAxisPart(rangeLabel, axisTitleFormatString, fontFormatString, format2, sb);
		addQuadrants(sb, null, false);
		addAxisScale(sb, co.getMajorInterval(), co.getMinorInterval());
		sb.append("</y_axis>");
		sb.append("</axes>");
	}

	private void addXAxis(StringBuilder sb, CategoryPlot p, String fontFormatString, String axisTitleFormatString) {
		String domainLabel = p.getDomainAxis().getLabel();

		if (!co.domainLabels && !co.domainTickMarkLabels && !co.tickmarksVisible && !co.rangeLabels && !co.rangeTickMarkLabels)
			sb.append("<x_axis enabled=\"False\">");
		else
			sb.append("<x_axis enabled=\"True\">");
		if (co.domainLabels && domainLabel != null)
			sb.append("<title padding=\"0\"><text>" + domainLabel + "</text>" + axisTitleFormatString + "</title>");
		else
			sb.append("<title enabled=\"False\"/>");
		if (!co.domainTickMarkLabels)
			sb.append("<labels enabled=\"False\"></labels>");
		else
			sb.append("<labels" + (co.categoryLabelAngle > 0 ? " display_mode=\"Normal\" rotation=\"" + ((int) co.categoryLabelAngle) + "\"" : "") + ""
					+ (co.type == ChartType.Bar || co.type == ChartType.StackedBar ? " align=\"Inside\"" : "") + ">" + fontFormatString + "</labels>");
		if (co.showMajorGrid == false) {
			sb.append("<major_grid enabled=\"False\" />");
		}
		if (co.showMinorGrid == false) {
			sb.append("<minor_grid enabled=\"False\" />");
		}
		if (co.showMajorTickmarks == false) {
			sb.append("<major_tickmark enabled=\"False\" />");
		}
		if (co.showMinorTickmarks == false) {
			sb.append("<minor_tickmark enabled=\"False\" />");
		}
/*		if (!co.tickmarksVisible)
		{
			sb.append("<major_tickmark enabled=\"False\" /><minor_tickmark enabled=\"False\" />");
			sb.append("<major_grid enabled=\"True\" /><minor_grid enabled=\"True\" />");
		}
		*/
		sb.append("</x_axis>");
		
	}
	private void add2ndYAxis(StringBuilder sb, Map<String, String> formatMap, String fontFormatString, String axisTitleFormatString) {
		if (cds2 != null)
		{
			String seriesName2 = co.series2.size() > 0 ? co.series2.get(0) : null;
			String format2 = null;
			if (seriesName2 != null)
			{
				format2 = getNumberFormat("Value", formatMap != null ? formatMap.get(seriesName2) : null);
			}
			CategoryPlot p2 = (CategoryPlot) ((JFreeChart) drawable2).getPlot();
			String rangeLabel2 = p2.getRangeAxis().getLabel();
			boolean hiddenSecondAxis = co.shareAxis && co.series2.size() > 1 && co.stacked;
			if ((seriesName2 != null) && (!co.shareAxis || hiddenSecondAxis))
			{
				sb.append("<extra>");
				addYAxis(sb, "y2", rangeLabel2, format2, axisTitleFormatString, fontFormatString, p2, true);
				sb.append("</extra>");
			}
		}
	}
	
	private void addAxisLabels(StringBuilder sb)
	{
		if (co.type == ChartType.Bar || co.type == ChartType.Column || co.type == ChartType.Line || co.type == ChartType.BarLine
				|| co.type == ChartType.StackedBar || co.type == ChartType.StackedColumn || co.type == ChartType.StackedColumnLine
				|| co.type == ChartType.StackedArea || co.type == ChartType.Area) {
			CategoryPlot p = (CategoryPlot) ((JFreeChart) drawable).getPlot();
			Font f = co.getLabelFont();
			Font axisTitleFont = f.deriveFont(Font.BOLD);
			String fontFormatString = getFontFormatString(f);
			String axisTitleFormatString = getFontFormatString(axisTitleFont);
			String rangeLabel = p.getRangeAxis().getLabel();
			Map<String, String> formatMap = co.getFormatMap();
			sb.append("<axes>");
			
			addXAxis(sb, p, fontFormatString, axisTitleFormatString);
			add2ndYAxis(sb, formatMap, fontFormatString, axisTitleFormatString);
			String seriesName = null;
			if (co.series.size() > 0) {
				int[] colTypes = qrs.getColumnTypes();
				int numCols = colTypes.length;
				List<String> dims = new ArrayList<String>();
				int i = 0;
				for (; i < numCols; i++) {
					if (colTypes[i] == QueryColumn.DIMENSION) {
						dims.add(qrs.getDisplayNames()[i]);
					}
				}
				for (i = 0; i < co.series.size(); i++) {
					String s = co.series.get(i);
					if (dims.contains(s))
						continue;
					
					seriesName = s;
					break;
				}
			}
			String format = null;
			// yet another hack for bug 8320
			for (Iterator<String> iter = formatMap.keySet().iterator(); iter.hasNext(); ) {
				String key = iter.next();
				if (rangeLabel.indexOf(key) >= 0) {
					// range could be a concatenation of all the series, i.e. 1995/Sum/Min (Year/Series1/Series2)
					// if we find something, use it
					format = getNumberFormat("Value", formatMap.get(key));
					break;
				}
			}
			// XXX hack because we don't have the format information directly associated with the chart, but indirectly via 
			// the result set.  Fix 5750
			if (rangeLabel != null && formatMap.containsKey(rangeLabel))
			{
				format = getNumberFormat("Value", formatMap.get(rangeLabel));
			}
			else if (seriesName != null && formatMap.containsKey(seriesName))
			{
				format = getNumberFormat("Value", formatMap.get(seriesName));
			}
			else if (format == null) {
				format = getNumberFormat("Value", null);
			}
			
			
			// handle grouped charts.
			if (co.type == ChartType.Bar || co.type == ChartType.Column || co.type == ChartType.BarLine
					|| co.type == ChartType.StackedBar || co.type == ChartType.StackedColumn 
					|| co.type == ChartType.StackedColumnLine) {
				List<String> groupKeys = getGroupKeys(cds, qrs);
				boolean addExtra = groupKeys.size() > 1 && seriesContainDimension(cds, qrs, co.series); 
				addYAxis(sb, null, rangeLabel, format, axisTitleFormatString, fontFormatString, p, false, addExtra ? Boolean.TRUE : null);
				if (addExtra) {
					// for each group key, add a y axis.
					sb.append("<extra>");
					for (int i = 1; i < groupKeys.size(); i++) {
						// skip first one, since we'll use the default y_axis for that.
						sb.append("<y_axis name=\"");
						sb.append(groupKeys.get(i));
						sb.append("\" enabled=\"false\">");
						StringBuilder scale = getScale(true, true, false);
						if (scale.length() > 0)
							sb.append("<scale" + scale + "/>");
						sb.append("</y_axis>");
					}
					sb.append("</extra>");
				}
			}
			else {
				addYAxis(sb, null, rangeLabel, format, axisTitleFormatString, fontFormatString, p, false);
			}
			
			sb.append("</axes>");
		}
	}

	/*
	 * do unnatural acts when the follow is true in order to work around issues with how we use AnyChart
	 * co.shareAxis && co.stacked && co.series2.size() > 1
	 */
	private void addYAxis(StringBuilder sb, String name, String rangeLabel, String format, String axisTitleFormatString, String fontFormatString, CategoryPlot p, boolean second) {
		addYAxis(sb, name, rangeLabel, format, axisTitleFormatString, fontFormatString, p, second, null);
	}
	private void addYAxis(StringBuilder sb, String name, String rangeLabel, String format, String axisTitleFormatString, String fontFormatString, CategoryPlot p, boolean second, Boolean hasMoreThanOne)
	{
		boolean hiddenSecondAxis = ((hasMoreThanOne == null) ? (co.shareAxis && co.series2.size() > 1 && co.stacked) : hasMoreThanOne.booleanValue());
		boolean noAxisLabelsOrMarks = !co.domainLabels && !co.domainTickMarkLabels && !co.tickmarksVisible && !co.rangeLabels && !co.rangeTickMarkLabels;
		if ((second && hiddenSecondAxis) || noAxisLabelsOrMarks)
			sb.append("<y_axis enabled=\"False\" ");
		else
			sb.append("<y_axis enabled=\"True\" ");
		sb.append(((co.type == ChartType.Bar || co.type == ChartType.StackedBar) ? " position=\"Opposite\"" : "") + "" + (name == null ? "" : " name=\"" + name + "\"") + ">");
		if (rangeLabel != null && co.rangeLabels == true)
			sb.append("<title padding=\"0\"><text>" + rangeLabel + "</text>" + axisTitleFormatString + "</title>");
		else
			sb.append("<title enabled=\"false\"/>");
		if (!co.rangeTickMarkLabels)
			sb.append("<labels enabled=\"False\"></labels>");
		else if (format != null)
			sb.append("<labels><format>" + format + "</format>" + fontFormatString + "</labels>");
		if (!co.tickmarksVisible)
		{
			sb.append("<major_tickmark enabled=\"False\" /><minor_tickmark enabled=\"False\" />");
			sb.append("<major_grid enabled=\"False\" /><minor_grid enabled=\"False\" />");
		}
		if (co.showMajorGrid == false) {
			sb.append("<major_grid enabled=\"False\" />");
		}
		if (co.showMinorGrid == false) {
			sb.append("<minor_grid enabled=\"False\" />");
		}
		if (co.showMajorTickmarks == false) {
			sb.append("<major_tickmark enabled=\"False\" />");
		}
		if (co.showMinorTickmarks == false) {
			sb.append("<minor_tickmark enabled=\"False\" />");
		}
		StringBuilder scale = getScale(hiddenSecondAxis, second, ((second && hiddenSecondAxis) || (name != null)));
		if (scale.length() > 0)
			sb.append("<scale" + scale + "/>");
		else
			addAxisScale(sb, second ? co.getSecondAxisMajorInterval() : co.getMajorInterval(), 
				second ? co.getSecondAxisMinorInterval() : co.getMinorInterval());
		sb.append("</y_axis>");
	}
	
	private StringBuilder getScale(boolean hiddenSecondAxis, boolean second, boolean isNormal) {
		StringBuilder scale = new StringBuilder();
		if (co.jfreechartsemantics || hiddenSecondAxis)
		{
			// simulate the basic scaling that was done by JFREECHARTS			
			// calculate the min and max (for both yaxes if necessary)
			double min = Double.POSITIVE_INFINITY;
			double max = Double.NEGATIVE_INFINITY;
			double sum[][] = null;
			List<String> groupKeys = getGroupKeys(cds, qrs);
			if (groupKeys.size() > 1 && seriesContainDimension(cds, qrs, co.series) &&
					(co.type == ChartType.StackedBar || 
							co.type == ChartType.StackedColumnLine || co.type == ChartType.StackedColumn)) {
				int measureCount = 0;
				List<String> colNames = Chart.getColumnNames(qrs);
				List<String> measureNames = new ArrayList<String>();
				int[] colTypes = qrs.getColumnTypes();
				for (String s : co.series) {
					int index = colNames.indexOf(s);
					if (index >= 0)
					{
						if (colTypes[index] == QueryColumn.MEASURE)
						{
							measureNames.add(s);
							measureCount++;
						}
					}
				}
				
				double seriesValues[][] = new double[measureCount][2];
				int columnCount = cds.getColumnCount();
				for (int k = 0; k < columnCount; k++) {

					for (int i = 0; i < measureCount; i++) {
						seriesValues[i][0] = 0;
						seriesValues[i][1] = 0;
					}
					for (int i = 0; i < cds.getRowKeys().size(); i++) {
						String key = cds.getRowKey(i).toString();
						String[] keyParts = key.split("\\|");
						int seriesValueIndex = -1;
						for (int l = 0; l < keyParts.length; l++) {
							int keyIndex = measureNames.indexOf(keyParts[l]);
							if (keyIndex >= 0) {
								seriesValueIndex = keyIndex;
							}
						}
						if (seriesValueIndex < 0)
							continue;
						
						Number d = cds.getValue(i, k);
						// null value not supported in charts.
						if (d == null)
							continue;
						if (d.doubleValue() > 0)
							seriesValues[seriesValueIndex][1] += d.doubleValue();
						else
							seriesValues[seriesValueIndex][0] += d.doubleValue();
							
					}
					
					for (int i = 0; i < seriesValues.length; i++) {
						min = Math.min(seriesValues[i][0], min);
						max = Math.max(seriesValues[i][1], max);
					}
				}
			}
			else {
				if (co.stacked)
					sum = new double[cds.getColumnCount()][2];
				for (int i = 0; i < cds.getRowCount(); i++)
				{
					for (int j = 0; j < cds.getColumnCount(); j++)
					{
						if (cds.getValue(i, j) != null)
						{
							double val = cds.getValue(i, j).doubleValue();
							if (!co.stacked)
							{
								if (val < min)
									min = val;
								if (val > max)
									max = val;
							}
							else
							{
								if (val > 0)
									sum[j][1] += val;
								else 
									sum[j][0] += val;
							}
						}
					}
				}
				if (co.stacked)
				{
					for (int j = 0; j < cds.getColumnCount(); j++)
					{
						if (sum[j][0] < min)
							min = sum[j][0];
						if (sum[j][1] > max)
							max = sum[j][1];					
					}
				}
			}

			// include the min/max from the second category dataset when the axis is shared
			if (hiddenSecondAxis && cds2 != null)
			{
				for (int i = 0; i < cds2.getRowCount(); i++)
				{
					for (int j = 0; j < cds2.getColumnCount(); j++)
					{
						if (cds2.getValue(i, j) != null)
						{
							double val = cds2.getValue(i, j).doubleValue();
							if (val < min)
								min = val;
							if (val > max)
								max = val;
						}
					}
				}
			}
			if (min == Double.POSITIVE_INFINITY)
			{
				min = 0;
			}
			if (max == Double.NEGATIVE_INFINITY)
			{
				max = 0;
			}
			// apply margins
			double margin = 0.025;
			if (co.lowerMargin > 0)
				margin = co.lowerMargin;
			min = (min > 0 ? min * (1.0 - margin) : min * (1.0 + margin));
			margin = 0.025;
			if (co.upperMargin > 0)
				margin = co.upperMargin;
			max = (max > 0 ? max * (1.0 + margin) : max * (1.0 - margin));
			
			// see if the values need to be overridden
			if (co.valueMin != co.valueMax && co.valueMin != null && co.valueMax != null) {
				if (min > co.valueMin)
					min = co.valueMin;
				if (max < co.valueMax)
					max = co.valueMax;
			}
			if (co.stacked) {
				min = Math.min(0, min);
				max = Math.max(0, max);
			}
			scale.append(" minimum=\"" + min + "\" maximum=\"" + max + "\"");
		}
		else
		{
			if (co.lowerMargin > 0)
				scale.append(" minimum_offset=\"" + co.lowerMargin + "\"");
			if (co.upperMargin > 0)
				scale.append(" maximum_offset=\"" + co.upperMargin + "\"");
			if (!second && co.valueMin != co.valueMax && co.valueMax > co.valueMin)
			{
				// if margins (offsets) are specified, ignore min/max (min/max overrides offsets in AnyChart)
				if (co.lowerMargin == 0)
					scale.append(" minimum=\"" + co.valueMin + "\"");
				if (co.upperMargin == 0)
					scale.append(" maximum=\"" + co.valueMax + "\"");
			}
			if (second && co.valueSecondMin != co.valueSecondMax && co.valueSecondMax > co.valueSecondMin)
			{
				// if margins (offsets) are specified, ignore min/max (min/max overrides offsets in AnyChart)
				if (co.lowerMargin == 0)
					scale.append(" minimum=\"" + co.valueSecondMin + "\"");
				if (co.upperMargin == 0)
					scale.append(" maximum=\"" + co.valueSecondMax + "\"");
			}			
		}
		if (co.type == ChartType.StackedBar || co.type == ChartType.StackedColumn || co.type == ChartType.StackedColumnLine || co.type == ChartType.StackedArea)
		{
			if (isNormal)
				scale.append(" mode=\"Normal\"");
			else
				scale.append(" mode=\"Stacked\"");
		}
		
		double majorInterval = second ? co.getSecondAxisMajorInterval() : co.getMajorInterval();
		if (majorInterval > 0.0) {
			scale.append(" major_interval=\"");
			scale.append(majorInterval);
			scale.append("\" ");
			double minorInterval = second ? co.getSecondAxisMinorInterval() : co.getMinorInterval();
			if (minorInterval > 0.0) {
				scale.append("minor_interval=\"");
				scale.append(minorInterval);
				scale.append("\" ");
			}
		}
		return scale;
	}

	private String getFontFormatString(Font f)
	{
		return ("<font family=\"" + f.getFamily() + "\" size=\"" + Math.round(((double) f.getSize() * scaleFactor)) + "\" bold=\"" + f.isBold()
				+ "\" italic=\"" + f.isItalic() + "\"/>");
	}

	private void addChartBackground(StringBuilder sb)
	{
		sb.append("<chart_background>");
		if (co.noborder)
		{
			sb.append("<border enabled=\"False\" />");
			sb.append("<corners type=\"Square\" />");
			sb.append("<inside_margin all=\"0\" /> ");
		}
		else
		{
			sb.append("<border enabled=\"True\" color=\"#" +  ColorUtils.canonicalizeHex(co.borderColor) + "\" type=\"Solid\" />");
			sb.append("<corners type=\"Square\" />");
			sb.append("<inside_margin all=\"5\" /> ");
		}
		sb.append("<effects enabled=\"False\"/>");
		if (co.chartBackgroundColor != null)
		{
			sb.append("<fill type=\"Solid\" color=\"#" + ColorUtils.canonicalizeHex(co.chartBackgroundColor) + "\" opacity=\"" + co.chartBackgroundOpacity + "\" />");
		}
		sb.append("</chart_background>");
		if (co.plotBackgroundColor != null)
		{
			sb.append("<data_plot_background enabled=\"true\" >");
			sb.append("<fill type=\"Solid\" color=\"#" + ColorUtils.canonicalizeHex(co.plotBackgroundColor) + "\" opacity=\"" + co.plotBackgroundOpacity + "\" />");
			sb.append("<border enabled=\"False\" />");
		}
		else
		{
			sb.append("<data_plot_background enabled=\"false\" >");
		}
		sb.append("</data_plot_background>");
	}

	private void addFunnelDataPlotSettings(StringBuilder sb)
	{
		sb.append("<data_plot_settings default_series_type=\"Bar\" enable_3d_mode=\"" + (co.threeD ? "True" : "False") + "\">");
		addFunnelSeriesSettings(sb);
		sb.append("</data_plot_settings>");
	}

	private void addTreeMapDataPlotSettings(StringBuilder sb)
	{
		sb.append("<data_plot_settings enable_3d_mode=\"" + (co.threeD ? "True" : "False") + "\">");
		addTreeMapSeriesSettings(sb);
		sb.append("</data_plot_settings>");
	}
	
	private void addHeatMapDataPlotSettings(StringBuilder sb)
	{
		sb.append("<data_plot_settings enable_3d_mode=\"" + (co.threeD ? "True" : "False") + "\">");
		addHeatMapSeriesSettings(sb);
		sb.append("</data_plot_settings>");
	}
	
	private void addThresholdSettings(StringBuilder sb)
	{
		int numGradients = co.numSegments;
		if (co.customThresholds != null && co.customThresholds.length() > 0) {
			// do custom thresholds
			String[] customThresholds = co.customThresholds.split("#");
			if (customThresholds.length >= 2) {
				sb.append("<thresholds>");
				sb.append("<threshold name=\"MyThreshold\">");
				for (int i = 0; i < customThresholds.length; i++) {
					String[] ct = customThresholds[i].split(":");
					if (ct.length < 4)
						continue;
					
					try {
						Integer clr = Integer.valueOf(ct[3]);
						String customThresholdColors = Integer.toHexString(clr.intValue());
						// prepend enough 0s so its an RGB triplet
						while (customThresholdColors.length() < 6) {
							customThresholdColors = "0" + customThresholdColors;
						}
						
						Double greaterThan = ct[2].length() > 0 ? Double.valueOf(ct[2]) : null;
						Double lessThan = ct[1].length() > 0 ? Double.valueOf(ct[1]) : null;

						if (greaterThan == null && lessThan == null) {
							continue;
						}
						
						sb.append("<condition name=\"");
						sb.append(ct[0].length() > 0 ? Chart.xmlify(ct[0]) : "Condition" + i);
						if (lessThan == null) {
							sb.append("\" type=\"lessThan\" value_1=\"{%Value}\" value_2=\"");
							sb.append(greaterThan);
						}
						else if (greaterThan == null) {
							sb.append("\" type=\"greaterThanOrEqualTo\" value_1=\"{%Value}\" value_2=\"");
							sb.append(lessThan);
						}
						else {
							sb.append("\" type=\"between\" value_1=\"{%Value}\" value_2=\"");
							sb.append(lessThan);
							sb.append("\" value_3=\"");
							sb.append(greaterThan);
						}
						sb.append("\" color=\"#");
						sb.append(customThresholdColors);
						sb.append("\"/>");
					}
					catch (NumberFormatException e) { }
				}
				sb.append("</threshold>");
				sb.append("</thresholds>");
				return;
			}
		}
		// thresholds only have one series
		Object[][] rows = qrs.getRows();
		String[] colNames = qrs.getColumnNames();
		String[] dispNames = qrs.getDisplayNames();
		List<String> columnNames = new ArrayList<String>(colNames.length);
		for (int i = 0; i < colNames.length; i++) {
			if (dispNames[i] == null)
				columnNames.add(colNames[i]);
			else
				columnNames.add(dispNames[i]);
		}
		int series = -1;
		if (co.series.size() > 0)
			series = columnNames.indexOf(co.series.get(0));
		if (series >= 0) {
			HashMap<Number, Number> valueMap = new HashMap<Number, Number>();
			for (int i = 0; i < rows.length && valueMap.size() < numGradients; i++) {
				Number n = (Number)rows[i][series];
				valueMap.put(n, n);
			}
			numGradients = valueMap.size();
		}
		sb.append("<palettes>");
		sb.append("<palette name=\"MyColorPalette\" type=\"ColorRange\" color_count=\"" + numGradients + "\">");
		sb.append("<gradient>");
		if (co.chartColors != null && co.chartColors.size() >= 2)
		{
			for (Color c: co.chartColors)
			{
				sb.append("<key color=\"Rgb(" + c.getRed() + "," + c.getGreen() + "," + c.getBlue() + ")\"/>");
			}
		}
		else if (co.type == ChartType.Map) {
			sb.append("<key color=\"green\"/>");
			sb.append("<key color=\"red\"/>");
		}
		else
		{
			sb.append("<key color=\"green\"/>");
			sb.append("<key color=\"yellow\"/>");
			sb.append("<key color=\"red\"/>");
		}
		sb.append("</gradient>");
		sb.append("</palette>");
		sb.append("</palettes>");
		sb.append("<thresholds>");
		sb.append("<threshold name=\"MyThreshold\" type=\"" + co.thresholdType + "\" palette=\"MyColorPalette\" range_count=\"" + numGradients + "\" />");
		sb.append("</thresholds>");
	}
	
	private void addBarDataPlotSettings(StringBuilder sb)
	{
		sb.append("<data_plot_settings default_series_type=\"Bar\" enable_3d_mode=\"" + (co.threeD ? "True" : "False") + "\">");
		addBarSeriesSettings(sb);
		sb.append("</data_plot_settings>");
	}

	private void addAreaPlotSettings(StringBuilder sb)
	{
		sb.append("<data_plot_settings default_series_type=\"Area\" >");
		addAreaSeriesSettings(sb);
		sb.append("</data_plot_settings>");
	}

	private String getNumberFormat(String keyword, String formatStr)
	{
		if (formatStr == null)
		{
			if (co.tickmarkFormat == null)
			{
				return "{%" + keyword + "}";
			}
			else
			{
				formatStr = co.tickmarkFormat;
			}
		}
		return getAnychartNumberFormat(keyword, formatStr);
	}
		
	public static String getAnychartNumberFormat(String keyword, String formatStr) {
		StringBuilder format = new StringBuilder();
		if (formatStr == null || !Util.hasNonWhiteSpaceCharacters(formatStr))
			formatStr = "#";
		if (formatStr.toUpperCase().startsWith(ToTime.TO_TIME_FORMAT_PREFIX)) {
			if (keyword.startsWith("{%"))
				format.append(keyword);
			else
				format.append("{%" + keyword + "}");
			format.append("{scale:(1)(1000)(60)(60)(24)|( ms)( s)( m)( h)( d)}");
			return format.toString();
		}
		UserBean ub = com.successmetricsinc.UserBean.getParentThreadUserBean();
		DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance(ub != null ? ub.getLocale() : Locale.getDefault());
		DecimalFormat df = new DecimalFormat(formatStr, symbols);
		if (df.getPositivePrefix().length() > 0)
			format.append(df.getPositivePrefix());
		if (keyword.startsWith("{%"))
			format.append(keyword);
		else
			format.append("{%" + keyword + "}");
		StringBuilder suffix = new StringBuilder();
		if (df.getNegativePrefix().length() > 0)
		{
			if (suffix.length() > 0)
				suffix.append(',');
			suffix.append("useNegativeSign:" + (df.getNegativePrefix().equals("-") ? "true" : "false"));
		}
		if (df.getMinimumIntegerDigits() > 0)
		{
			if (suffix.length() > 0)
				suffix.append(',');
			suffix.append("leadingZeros:" + df.getMinimumIntegerDigits());
		}
		/*
		 * this does bad things - 9609
		if (df.getMinimumFractionDigits() > 0)
		{
			if (suffix.length() > 0)
				suffix.append(',');
			suffix.append("trailingZeros:" + df.getMinimumFractionDigits());
		}
		*/
		if (suffix.length() > 0)
			suffix.append(',');
		suffix.append("numDecimals:" + df.getMaximumFractionDigits());
		if (suffix.length() > 0)
			suffix.append(',');
		suffix.append("decimalSeparator:" + getEscapedSymbol(df.getDecimalFormatSymbols().getDecimalSeparator()));
		boolean isPercent = false;
		if (df.getPositiveSuffix().equals("%"))
		{
			if (suffix.length() > 0)
				suffix.append(',');
			suffix.append("scale:(0.01)|(%)");
			isPercent = true;
		}
		if (df.isGroupingUsed() && df.getGroupingSize() == 3)
		{
			if (suffix.length() > 0)
				suffix.append(',');
			suffix.append("thousandsSeparator:" + getEscapedSymbol(df.getDecimalFormatSymbols().getGroupingSeparator()));
		}
		else if (!df.isGroupingUsed()) // remove thousandsSeperator
		{
			if (suffix.length() > 0)
				suffix.append(',');
			suffix.append("thousandsSeparator:");
		}
		format.append('{');
		format.append(suffix);
		format.append('}');
		if (!isPercent && df.getPositiveSuffix().length() > 0)
			format.append(df.getPositiveSuffix());
		return (format.toString());
	}
	
	public static String getEscapedSymbol(Character ch) {
		if (ch.equals(',') || ch.equals(':') || ch.equals('{') || ch.equals('}'))
			return "\\" + ch.toString();
		else
			return ch.toString();
	}
	
	private void addFunnelSeriesSettings(StringBuilder sb)
	{
		double cgr = co.categoryGapWidth > 0 ? co.categoryGapWidth : 0.5;
		String shape = " mode=\"" + (co.gradient != null && co.gradient.equals("3D") ? "Circular" : "Square") + "\"";
		sb.append("<funnel_series point_padding=\"" + (co.gapWidth / 2) + "\" group_padding=\"" + cgr + "\"" + shape);
		if (co.type == ChartType.Pyramid)
			sb.append(" neck_height=\"0\" min_width=\"0\" inverted=\"true\" ");
		sb.append(">");
		if (co.domainLabels == true)
			sb.append("<label_settings enabled=\"true\" mode=\"Outside\" multi_line_align=\"Center\">");
		else
			sb.append("<label_settings enabled=\"false\">");
		sb.append("<background enabled=\"false\" />");
		sb.append("<position anchor=\"Center\" valign=\"Center\" halign=\"Center\" padding=\"5\" />");
		sb.append("<format>{%Name}{enabled:False}</format>");

		Font f = co.getLabelFont();
		String fontFormatString = getFontFormatString(f);
		sb.append(fontFormatString + "</label_settings>");
		if (co.toolTips) // note that this is overiden in the data series...
		{
			sb.append("<tooltip_settings enabled=\"True\">");
			if (co.rangeTickmarkFormat != null)
			{
				sb.append("<format>{%SeriesName}{enabled:False}/{%Name}{enabled:False}: "
						+ getNumberFormat("Value", co.rangeTickmarkFormat) + "</format>");
			}
			sb.append("</tooltip_settings>");
		}
		else
		{
			sb.append("<tooltip_settings enabled=\"False\" />");
		}
		sb.append("<interactivity allow_select=\"False\" />");
		sb.append("</funnel_series>");
	}

	private void addTreeMapSeriesSettings(StringBuilder sb)
	{
		sb.append("<tree_map sort=\"none\">");
		if (co.domainLabels)
			sb.append("<label_settings enabled=\"true\" multi_line_align=\"Center\">");
		else
			sb.append("<label_settings enabled=\"false\">");
		sb.append("<background enabled=\"false\" />");
		sb.append("<position anchor=\"Center\" valign=\"Center\" halign=\"Center\" padding=\"5\" />");
		sb.append("<format>{%Name}{enabled:False}</format>");
		Font f = co.getLabelFont();
		String fontFormatString = getFontFormatString(f);
		sb.append(fontFormatString + "</label_settings>");
		if (co.toolTips)
		{
			sb.append("<tooltip_settings enabled=\"True\">");
			if (co.rangeTickmarkFormat != null)
			{
				sb.append("<format>{%Name}{enabled:False}: "
						+ getNumberFormat("Value", co.rangeTickmarkFormat) + "</format>");
			}
			sb.append("</tooltip_settings>");
		}
		else
		{
			sb.append("<tooltip_settings enabled=\"False\" />");
		}
		sb.append("<interactivity allow_select=\"False\" />");

		sb.append("<tree_map_style/>");
		sb.append("</tree_map>");
	}
	
	private void addHeatMapSeriesSettings(StringBuilder sb)
	{
		sb.append("<heat_map sort=\"none\">");
		if (co.toolTips)
		{
			sb.append("<tooltip_settings enabled=\"True\"><format>{%Row}{enabled:False}, {%Column}{enabled:False}: "
					+ getNumberFormat("YValue", co.rangeTickmarkFormat));
			sb.append("</format></tooltip_settings>");
		}
		else
		{
			sb.append("<tooltip_settings enabled=\"False\" />");
		}
		sb.append("<interactivity allow_select=\"False\" />");

		sb.append("<heat_map_style/>");
		sb.append("</heat_map>");
	}

	private void addBarSeriesSettings(StringBuilder sb)
	{
		double cgr = co.categoryGapWidth > 0 ? co.categoryGapWidth : 0.75;
		String shape = " shape_type=\"" + (co.gradient != null && co.gradient.equals("3D") ? "Cylinder" : "Box") + "\"";
		// at some point add these as setting in the chart properties dialog
//		sb.append("<bar_series point_padding=\"" + co.gapWidth + "\" group_padding=\"" + cgr + "\"" + shape + ">");
		sb.append("<bar_series point_padding=\"0.25\" group_padding=\"" + cgr + "\"" + shape + ">");
		if (co.toolTips)
		{
			sb.append("<tooltip_settings enabled=\"True\"><format>{%SeriesName}{enabled:False}/{%Name}{enabled:False}: "
					+ getNumberFormat("Value", co.rangeTickmarkFormat));
			sb.append("</format></tooltip_settings>");
		}
		else
		{
			sb.append("<tooltip_settings enabled=\"False\" />");
		}
		sb.append("<interactivity allow_select=\"False\" />");
		if (co.displayValues)
		{
			// the last series is the one that is displayed in the display values - if you try {%Value} it doesn't work
			String seriesName = co.series.size() > 0 ? co.series.get(co.series.size() - 1) : null;
			Map<String, String> formatMap = co.getFormatMap();
			sb.append("<label_settings enabled=\"true\">");
			sb.append("<background enabled=\"false\"/>");
			sb.append("<format>" + getNumberFormat("Value", formatMap != null ? formatMap.get(seriesName) : null) + "</format>");
			sb.append("</label_settings>");
		}
		sb.append("</bar_series>");
		if (cds2 != null)
			addLineSeriesSettings(sb, true);
	}

	private void addAreaSeriesSettings(StringBuilder sb)
	{
		sb.append("<area_series>");
		if (co.toolTips)
		{
			sb.append("<tooltip_settings enabled=\"True\">");
			if (co.rangeTickmarkFormat != null)
			{
				sb.append("<format>{%SeriesName}{enabled:False}/{%Name}{enabled:False}: "
							+ getNumberFormat("Value", co.rangeTickmarkFormat) + "</format>");
			}
			sb.append("</tooltip_settings>");
		}
		else
		{
			sb.append("<tooltip_settings enabled=\"False\" />");
		}
		sb.append("<interactivity allow_select=\"False\" />");
		sb.append("</area_series>");
	}

	private void addCategoryData(StringBuilder sb)
	{
		sb.append("<data>");
		List<String> colNames = Chart.getColumnNames(qrs);
		int[] colTypes = qrs.getColumnTypes();
		String name = null;
		for (String s : co.series)
		{
			int index = colNames.indexOf(s);
			if (index >= 0)
			{
				if (colTypes[index] == QueryColumn.MEASURE)
				{
					name = s;
					break;
				}
			}
		}
		DrawingSupplier ds = null;
		if (cds2 != null)
		{
			String name2 = null;
			for (String s : co.series2)
			{
				int index = colNames.indexOf(s);
				if (index >= 0)
				{
					if (colTypes[index] == QueryColumn.MEASURE)
					{
						name2 = s;
						break;
					}
				}
			}
			ds = addDatasetSeries(cds2, drawable2, sb, true, true, name2, null);
		}
		List<String> groupKeys = getGroupKeys(cds, qrs);
		if (groupKeys.size() > 1 && seriesContainDimension(cds, qrs, co.series) &&
				(co.type == ChartType.Bar || co.type == ChartType.StackedBar || 
						co.type == ChartType.Column || co.type == ChartType.StackedColumn || co.type == ChartType.StackedColumnLine)) {
			CategoryPlot p = (CategoryPlot) ((JFreeChart) drawable).getPlot();
			CategoryItemRenderer cir = p.getRenderer();
			CategoryURLGenerator urlgen = cir.getBaseItemURLGenerator();
			if (ds == null) {
				ds = p.getDrawingSupplier();
			}

			// each group key is a series.  Each category is a point in that series
			// the first series uses the default y-axis, 
			// the rest use the name of the group key as the y-axis name
			int size = cds.getRowKeys().size();
			for (int i = 0; i < size; i++) {
				String seriesName = cleanLabel(cds.getRowKeys().get(i).toString());
				sb.append("<series name=\"");
				sb.append(seriesName);
				int j = 0;
				for ( ; j < groupKeys.size(); j++) {
					if (seriesName.indexOf(groupKeys.get(j)) >= 0) {
						break;
					}
				}
				if (co.type == ChartOptions.ChartType.StackedColumnLine)
				{
					if (i == size - 1 && (co.series2 == null || co.series2.isEmpty()))
					{
						// last row - switch to line
						sb.append("\" type=\"Line");
					}
				}
				
				if (j > 0 && j < groupKeys.size()) {
					sb.append("\" y_axis=\"");
					sb.append(groupKeys.get(j));
				}
				String seriesSliceKey = seriesName;
				int index = seriesName.lastIndexOf('|');
				if (index >= 0) {
					seriesSliceKey = seriesSliceKey.substring(0, index);
				}
				Color clr = categoryKeyColorMap == null ? null : categoryKeyColorMap.get(seriesSliceKey);
				
				if (clr == null) {
					clr = getNextColor(ds);
					if (categoryKeyColorMap == null) {
						categoryKeyColorMap = new HashMap<String, Color>();
					}
					categoryKeyColorMap.put(seriesSliceKey, clr);
				}
				if (clr != null) {
					sb.append("\" color=\"Rgb(");
					sb.append(clr.getRed() + "," + clr.getGreen() + "," + clr.getBlue() + ")");
				}
				sb.append("\">");
					// add each point with the name of each category
					for (int k = 0; k < cds.getColumnCount(); k++) {
						Object d = cds.getValue(i, k);
						// null value not supported in charts.
						if (d == null)
							continue;
						
						Object ko = cds.getColumnKey(k);
						String key = (ko == null) ? "" : ko.toString();
						String cleanKey = cleanLabel(key);
						String y = "y=\"" + d.toString() + "\" ";
						sb.append("<point name=\"");
						sb.append(cleanKey);
						sb.append("\" ");
						sb.append(y);
						addPointColor(sb, key);
						sb.append(">");
						addURL(sb, urlgen, i, k, key);
						if (co.domainGroupLabels == true) {
							if (j >= 0 && j < groupKeys.size()) {
								sb.append("<extra_labels><label enabled=\"true\"");
								if (co.type != ChartType.Bar && co.type != ChartType.StackedBar) {
									sb.append(" rotation=\"90\"");
								}
								sb.append("><format>");
								sb.append(groupKeys.get(j));
								sb.append("</format><position anchor=\"XAxis\" halign=\"Right\"/>");
								// add font
								Font f = co.getLabelFont();
								sb.append(getFontFormatString(f));
								sb.append("</label></extra_labels>");
							}
						}
						sb.append("</point>");
					}
				
				sb.append("</series>");
			}
		}
		else {
			addDatasetSeries(cds, drawable, sb, false, false, name, ds);
		}
		sb.append("</data>");
	}

	public static boolean unsupportedValueToChart(Object d) {
		// null value not supported in charts.
		if (d == null || !(d instanceof Number))
			return true;
		
		if (d instanceof Double && (((Double)d).isInfinite() || ((Double)d).isNaN()))
			return true;
		else if (d instanceof Float && (((Float)d).isInfinite() || ((Float)d).isNaN()))
			return true;
		else if (d instanceof Integer && (((Integer)d).intValue() == Integer.MAX_VALUE))
			return true;
		else if (d instanceof Long && (((Long)d).longValue() == Long.MAX_VALUE))
			return true;
		
		return false;
	}

	private Color getNextColor(DrawingSupplier ds) {
		Color clr = null;
		if (clr == null) {
			Paint pnt = ds.getNextPaint();
			if (pnt instanceof Color)
				clr = (Color) pnt;
			else if (pnt instanceof ChartGradientPaint)
			{
				ChartGradientPaint cgp = (ChartGradientPaint) pnt;
				clr = cgp.getBaseColor();
			}
		}
		return clr;
	}
	
	private void addPointColor(StringBuilder sb, String key) {
		Color c = getPointColor(key);
		if (c != null)
			sb.append(" color=\"Rgb(" + c.getRed() + ","
						+ c.getGreen() + "," + c.getBlue() + ")\" ");
	}
	
	private Color getPointColor(String key) {
		Color color = null;
		if (getCategoryKeyColorMap() != null) {
			color = getCategoryKeyColorMap().get(key);
		}
		return color;
	}

	public static List<String> getGroupKeys(CategoryDataset cs, QueryResultSet qrs) {
		int[] colTypes = qrs.getColumnTypes();
		int numCols = colTypes.length;
		List<String> measures = new ArrayList<String>();
		int i = 0;
		for (; i < numCols; i++) {
			if (colTypes[i] != QueryColumn.DIMENSION) {
				measures.add(qrs.getDisplayNames()[i]);
			}
		}
		return getGroupKeys(cs, qrs, measures);
	}

	public static List<String> getGroupKeys(CategoryDataset cs, QueryResultSet qrs, List<String> measures) {
		// determine number of groups
		List<String> groupKeys = new ArrayList<String>();
		if (cs.getRowKeys() != null && cs.getRowKeys().size() > 0) {
			for (int j = 0; j < cs.getRowKeys().size(); j++) {
				String rowKey = (String)cs.getRowKeys().get(j);
				String[] splitKeys = rowKey.split("\\|");
				for (int i = 0; i < splitKeys.length; i++) {
					if (measures.contains(splitKeys[i]) && ! groupKeys.contains(splitKeys[i])) {
						groupKeys.add(splitKeys[i]);
					}
				}
			}
		}
		return groupKeys;
	}
	
	public static boolean seriesContainDimension(CategoryDataset cs, QueryResultSet qrs, List<String> series) {
		int[] colTypes = qrs.getColumnTypes();
		int numCols = colTypes.length;
		List<String> dims = new ArrayList<String>();
		int i = 0;
		for (; i < numCols; i++) {
			if (colTypes[i] == QueryColumn.DIMENSION) {
				dims.add(qrs.getDisplayNames()[i]);
			}
		}
		
		// determine number of groups
//		List<String> series = this.co.series;
		for (String s: series) {
			if (dims.contains(s))
				return true;
		}
		return false;
	}
	
	private DrawingSupplier addDatasetSeries(CategoryDataset cs, Drawable dr, StringBuilder sb, boolean second, boolean line, String seriesName,
			DrawingSupplier curds)
	{
		CategoryPlot p = (CategoryPlot) ((JFreeChart) dr).getPlot();
		CategoryItemRenderer cir = p.getRenderer();
		CategoryURLGenerator urlgen = cir.getBaseItemURLGenerator();
		
		DrawingSupplier ds = curds != null ? curds : p.getDrawingSupplier();
		for (int j = 0; j < cs.getRowCount(); j++)
		{
			Object o = cs.getRowKey(j);
			String sname = (o == null) ? "" : o.toString();
			
			// 7318
			int index = sname.indexOf('|');
			if (index > 0) {
				sname = sname.substring(0, index);
			}
			Color c = null;
			if (categoryKeyColorMap != null)
				c = categoryKeyColorMap.get(sname);
			if (c == null)
				c = getNextColor(ds);
			sname = Chart.xmlify(sname);
			if (co.type != ChartType.TreeMap)
			{
				sb.append("<series name=\"" + sname + "\"");
				sb.append((second ? " y_axis=\"y2\"" : "") + (line ? " type=\"Line\"" : "") + " color=\"Rgb(" + c.getRed() + ","
						+ c.getGreen() + "," + c.getBlue() + ")\"");
				if (co.type == ChartOptions.ChartType.BarLine || co.type == ChartOptions.ChartType.StackedColumnLine)
				{
					if (line == false && j == cs.getRowCount() - 1 && (co.series2 == null || co.series2.isEmpty()))
					{
						// last row - switch to line
						sb.append(" type=\"Line\"");
					}
				}
				sb.append(">");
			}
			Map<String, String> formatMap = co.getFormatMap();
			if (co.toolTips && seriesName != null)
			{
				int seriesCount = co.series.size();
				if (co.series2 != null) {
					seriesCount = seriesCount + co.series2.size();
				}
				sb.append("<tooltip><format>" + (seriesCount > 1 ? "{%SeriesName}{enabled:False}/" : "")
						+ "{%fullName}{enabled:False}: " + getNumberFormat("Value", formatMap != null ? formatMap.get(seriesName) : null) 
						+ ((co.type == ChartOptions.ChartType.Funnel || co.type == ChartOptions.ChartType.Pyramid) ? " ({%YPercentOfSeries}{numDecimals:1}%)" : "") + "</format></tooltip>");
			}
			if (line)
				sb.append("<marker_settings color=\"Rgb(" + c.getRed() + "," + c.getGreen() + "," + c.getBlue() + ")\"/>");
			for (int i = 0; i < cs.getColumnCount(); i++)
			{
				Object d = cs.getValue(j, i);
				Object ko = cs.getColumnKey(i);
				String key = (ko == null) ? "" : ko.toString();
				String cleanKey = cleanLabel(key);
				String y = null;
				if ((d == null) || (d.toString().length() == 0))
				{
					// fix for 7991 - these chart types don't play well with y=""
					if (co.type == ChartOptions.ChartType.Area || co.type == ChartOptions.ChartType.StackedArea)
					{
						y = "y=\"0\"";
					}
					else
					{
						y = "y=\"\"";
					}
				}
				else
				{
					y = "y=\"" + d.toString() + "\" ";
				}
				String threshold = "";
				if (co.type == ChartOptions.ChartType.TreeMap)
					threshold = " threshold=\"MyThreshold\"";
				sb.append("<point name=\"" + cleanKey + "\" " + y + threshold);
				addPointColor(sb, key);
				sb.append(">");
				boolean attributesTagAdded = addURL(sb, urlgen, j, i, key);
				if (!attributesTagAdded)
				{
					sb.append("<attributes>");
					sb.append("<attribute name=\"fullName\">" + cleanLabel(key) + "</attribute>");
					sb.append("</attributes>");
				}
				sb.append("</point>");
			}
			if (co.type != ChartType.TreeMap)
				sb.append("</series>");
		}
		return (ds);
	}
	
	private boolean addURL(StringBuilder sb, CategoryURLGenerator urlgen, int row, int column, String key) {
		if (urlgen == null) 
			return false;
		
		if (urlgen instanceof JSGen)
		{
			if (((JSGen) urlgen).isURL())
			{
				sb.append("<actions><action type=\"navigateToURL\" url=\"" + 
						((JSGen) urlgen).generateNavigateURLCall(cds, row, column)
						+ "\" target=\"_blank\" /></actions>");
			} else
			{
				List<String> call = ((JSGen) urlgen).generateCall(cds, row, column);
				if (call != null)
				{
					sb.append("<attributes><attribute name=\"DrillType\">" + call.get(2) + "</attribute>");
					sb.append("<attribute name=\"targetURL\">" + call.get(1) + "</attribute>");
					sb.append("<attribute name=\"filters\">" + call.get(3) + "</attribute>");
					sb.append("<attribute name=\"drillBy\">" + call.get(4) + "</attribute>");
					sb.append("<attribute name=\"fullName\">" + cleanLabel(key) + "</attribute>");
					sb.append("</attributes>");
					return true;
				}
			}
		} else
		{
			sb.append("<actions><action type=\"navigateToURL\" url=\"" + 
					urlgen.generateURL(cds, row, column) + "\" target=\"_blank\" /></actions>");
		}
		return false;
	}

	private void addXYData(StringBuilder sb, boolean bubble)
	{
		sb.append("<data>");
		XYPlot p = (XYPlot) ((JFreeChart) drawable).getPlot();
		XYItemRenderer renderer = p.getRenderer();
		XYURLGenerator urlgen = renderer.getURLGenerator();
		DrawingSupplier ds = p.getDrawingSupplier();
		for (int j = 0; j < xyds.getSeriesCount(); j++)
		{
			Color c = getNextColor(ds);
			Object o = xyds.getSeriesKey(j);
			String key = (o == null ? "" : o.toString());
			key = Chart.xmlify(key);
			sb.append("<series name=\"" + key + "\"" + " color=\"Rgb(" + c.getRed() + "," + c.getGreen() + "," + c.getBlue() + ")\""
					+ (bubble ? " type=\"Bubble\"" : " type=\"Marker\"") + ">");
			for (int i = 0; i < xyds.getItemCount(j); i++)
			{
				Object x = xyds.getX(j, i);
				if (x == null)
					x = 0;
				Object y = xyds.getY(j, i);
				if (y == null)
					y = 0;
				if (bubble)
				{
					Object z = ((SeriesXYZDataset) xyds).getZ(j, i);
					if (z == null)
						z = 0;
					sb.append("<point x=\"" + x.toString() + "\" y=\"" + y.toString() + "\" size=\"" + z.toString() + "\"");
				} else {
					sb.append("<point x=\"" + x.toString() + "\" y=\"" + y.toString() + "\"");
				}
				addPointColor(sb, key);
				sb.append(">");
				addXYZDatasetURLS(sb, key, urlgen);
				sb.append("</point>");
				
			}
			sb.append("</series>");
		}
		sb.append("</data>");
	}
	
	private void addXYZDatasetURLS(StringBuilder sb, String key, XYURLGenerator urlgen) {
		if (urlgen != null && urlgen instanceof JSGen)
		{
			if (((JSGen) urlgen).isURL())
			{
				sb.append("<actions><action type=\"navigateToURL\" url=\"" + 
						((JSGen) urlgen).generateNavigateURL(null, key)
						+ "\" target=\"_blank\" /></actions>");
			} else
			{
				List<String> call = ((JSGen) urlgen).generateCall(null, key);
				if (call != null)
				{
					sb.append("<attributes><attribute name=\"DrillType\">" + call.get(2) + "</attribute>");
					sb.append("<attribute name=\"targetURL\">" + call.get(1) + "</attribute>");
					sb.append("<attribute name=\"filters\">" + call.get(3) + "</attribute>");
					sb.append("<attribute name=\"drillBy\">" + call.get(4) + "</attribute>");
					sb.append("<attribute name=\"fullName\">" + cleanLabel(key) + "</attribute>");
					sb.append("</attributes>");
				}
			}
		}
		
	}
	
	public PieDataset getPieDataset()
	{
		return pds;
	}

	public void setPieDataset(PieDataset pds)
	{
		this.pds = pds;
	}

	private void addPieChart(StringBuilder sb)
	{
		sb.append("<chart plot_type=\"Pie\">");
		addChartSettings(sb, false, false);
		addPieDataPlotSettings(sb);
		addPieData(sb);
		sb.append("</chart>");
	}

	private void addPieDataPlotSettings(StringBuilder sb)
	{
		sb.append("<data_plot_settings enable_3d_mode=\"" + (co.threeD ? "True" : "False") + "\">");
		sb.append("<pie_series default_series_type=\"Pie\" sort=\"Desc\" start_angle=\"180\">");
		if (co.domainLabels == true)
			sb.append("<label_settings enabled=\"true\" mode=\"Outside\" multi_line_align=\"Center\">");
		else
			sb.append("<label_settings enabled=\"false\">");
		sb.append("<format>");
		if (co.showPieLabelKey)
		{
			sb.append("{%shortName}{enabled:False}");
		}
		else if (co.percent)
		{
			sb.append("{%YPercentOfSeries}{numDecimals:0}%");
		}
		else
		{
			sb.append("{%Value}");			
		}
		sb.append("</format>");
		Font f = co.getLabelFont();
		String fontFormatString = getFontFormatString(f);
		sb.append(fontFormatString + "</label_settings>");
		if (co.toolTips)
		{
			Map<String,String> formatMap = co.getFormatMap();
			String formatStr = co.series != null && co.series.size() > 0 ? co.series.get(0) : null;
			sb.append("<tooltip_settings enabled=\"True\"><format>{%Name}{enabled:False}: " + 
					getNumberFormat("Value", formatMap != null ? formatMap.get(formatStr) : null) + 
					" (" + getNumberFormat("YPercentOfSeries", "##.#") + "%)</format></tooltip_settings>");
		}
		else
		{
			sb.append("<tooltip_settings enabled=\"False\" />");
		}
		sb.append("</pie_series>");
		sb.append("</data_plot_settings>");
	}

	private void addPieData(StringBuilder sb)
	{
		if (co.series != null && co.series.size() > 0)
		{
			sb.append("<data>");
			sb.append("<series name=\"" + co.series.get(0) + "\" type=\"Pie\">");
			PiePlot p = (PiePlot) ((JFreeChart) drawable).getPlot();
			DrawingSupplier ds = p.getDrawingSupplier();
			PieURLGenerator urlgen = p.getURLGenerator();
			// calculate the sum of the values
			double total = 0;
			for (int i = 0; i < pds.getItemCount(); i++)
			{
				double val = pds.getValue(i).doubleValue();
				total += val;
			}
			int count = 0;
			for (int i = 0; i < pds.getItemCount(); i++)
			{
				Color c = getNextColor(ds);
				Object o = pds.getKey(i);
				String key = (o == null) ? "" : o.toString();
				String cleanKey = Chart.xmlify(key);
				String shortKey = cleanKey;
				boolean attributesTagAdded = false;
				Color color = getPointColor(key);
				if (color != null)
					c = color;

				sb.append("<point name=\"" + cleanKey + "\" y=\"" + pds.getValue(i) + "\" color=\"Rgb(" + c.getRed() + "," + c.getGreen() + "," + c.getBlue()
						+ ")\">");
				if (urlgen != null)
				{
					if (urlgen instanceof JSGen)
					{
						List<String> call = ((JSGen) urlgen).generateCall(pds, i);
						if (call != null)
						{
							sb.append("<attributes>");
							sb.append("<attribute name=\"DrillType\">" + call.get(2) + "</attribute>");
							sb.append("<attribute name=\"targetURL\">" + call.get(1) + "</attribute>");
							sb.append("<attribute name=\"filters\">" + call.get(3) + "</attribute>");
							sb.append("<attribute name=\"drillBy\">" + call.get(4) + "</attribute>");
							sb.append("<attribute name=\"shortName\">" + shortKey + "</attribute>");
							sb.append("<attribute name=\"fullName\">" + cleanLabel(key) + "</attribute>");
							sb.append("</attributes>");
							attributesTagAdded = true;
						}
					} else
					{
						sb.append("<actions><action type=\"navigateToURL\" url='" + urlgen.generateURL(pds, pds.getKey(i), i)
								+ "' target=\"_blank\" /></actions>");
					}
				}
				// do not show values less than PIE_LABEL_THRESHOLD of the pie (AnyChart has a really bad label layout algorithm)
				// bug in AnyChart - must have atleast two labels displayed to get any labels
				if (((pds.getValue(i).doubleValue() / total) < PIE_LABEL_THRESHOLD) && (count < (pds.getItemCount() - 2)))
				{
					count++;
					sb.append("<label enabled=\"false\"/>");
				}
				if (!attributesTagAdded)
				{
					sb.append("<attributes>");
					sb.append("<attribute name=\"shortName\">" + shortKey + "</attribute>");
					sb.append("<attribute name=\"fullName\">" + cleanLabel(key) + "</attribute>");
					sb.append("</attributes>");
				}
				sb.append("</point>");
			}
			sb.append("</series>");
			sb.append("</data>");
		}
	}

	private void addLineChart(StringBuilder sb)
	{
		sb.append("<chart plot_type=\"CategorizedVertical\">");
		addChartSettings(sb, false, false);
		addLineDataPlotSettings(sb);
		addCategoryData(sb);
		sb.append("</chart>");
	}

	private void addLineDataPlotSettings(StringBuilder sb)
	{
		sb.append("<data_plot_settings default_series_type=\"Line\">");
		addLineSeriesSettings(sb, false);
		sb.append("</data_plot_settings>");
	}

	private void addLineSeriesSettings(StringBuilder sb, boolean useSecondSeries)
	{
		sb.append("<line_series>");
		if (co.toolTips)
		{
			sb.append("<tooltip_settings enabled=\"True\" /><format>{%SeriesName}{enabled:False}/{%Name}{enabled:False}: {%YValue}</format>");
		}
		else
		{
			sb.append("<tooltip_settings enabled=\"False\" />");
		}
		sb.append("<line_style>");
		if (co.lineWidth == 0)
		{
			sb.append("<line enabled=\"false\"/>"); // anychart does not support linewidth = 0
		} else {
			sb.append("<line thickness=\"" + co.lineWidth + "\"/>");
		}
		sb.append("</line_style>");
		if (co.displayValues)
		{
			String seriesName = null;
			if (useSecondSeries)
				seriesName = co.series2.size() > 0 ? co.series2.get(0) : null;
			else
				seriesName = co.series.size() > 0 ? co.series.get(0) : null;
			Map<String, String> formatMap = co.getFormatMap();
			sb.append("<label_settings enabled=\"true\">");
			sb.append("<background enabled=\"false\"/>");
			sb.append("<format>" + getNumberFormat("Value", formatMap != null ? formatMap.get(seriesName) : null) + "</format>");
			sb.append("</label_settings>");
		}
		sb.append("</line_series>");
	}

	public Drawable getDrawable2()
	{
		return drawable2;
	}

	public void setDrawable2(Drawable drawable2)
	{
		this.drawable2 = drawable2;
		if (drawable2 instanceof JFreeChart)
		{
			CategoryPlot cp = ((JFreeChart) drawable2).getCategoryPlot();
			cds2 = cp.getDataset();
		}
	}

	public XYDataset getXYDataset()
	{
		return xyds;
	}

	public void setXYDataset(XYDataset xyds)
	{
		this.xyds = xyds;
	}

	public ValueDataset getVds()
	{
		return vds;
	}

	public void setVds(ValueDataset vds)
	{
		this.vds = vds;
	}
	
	/**
	 * First looks for the ChartRenderer.swf file in the following locations in 
	 * the following order:
	 * <ul>
	 * <li>ChartServerContext.chartServerSwfFilepath provides the full file path
	 * to the ChartRenderer.swf file</li>
	 * <li>SMI_INSTALL_DIR property, if it exists, provides the relative directory
	 * from which the full path can be derived assuming an SMIWeb Tomcat configuration</li>
	 * <li>CATALINA_HOME property, if it exists, provides the relative directory
	 * from which the full path can be derived assuming an SMIWeb Tomcat configuration</li>
	 * </ul> 
	 * If none of these is found, the method errors out.
	 * Method is protected to enable unit testing.
	 * TODO: This is more easily handled with PlaceholderConfigurer in Spring.
	 * @return a string giving the full file path to the ChartRenderer.swf file.
	 */
	protected static String getDefaultFlashFilePath() {
		// If the path has already been determined, return it
		if (chartServerSwfFilepath != null) {
			return chartServerSwfFilepath;
		}
			
		// Use the ChartServerContext to get the 'customer.properties' values
		chartServerSwfFilepath = ChartServerContext.chartServerSwfFilepath;
		if (chartServerSwfFilepath != null) {
			if (new File(chartServerSwfFilepath).exists()) {
				logger.trace("Found ChartServerContext.chartServerSwfFilepath; .swf filepath: " + chartServerSwfFilepath);
			} else {
				logger.warn("ChartServerContext.chartServerSwfFilepath given, but file didn't exist: " + chartServerSwfFilepath);
			}
		} else {
			logger.warn("ChartServerContext.chartServerSwfFilepath was not set.");
		}
		
		// Dump environment if we couldn't set the chartServerSwfFilepath
		if (chartServerSwfFilepath == null && logger.isTraceEnabled()) {
			// The system property should always be set
			DebugUtils.logEnviroment(logger, Level.TRACE);
		}

		return chartServerSwfFilepath;
	}
	
	/**
	 * Encapsulates the logic to determine if this webapp is running on a 64-bit OS.
	 * If it is, the 64-bit JVM cannot communicate with 32-bit native binary code.
	 * Since Flash Player (and therefore the JFlashPlayer DLLs) are only available
	 * for 32-bit Windows, render requests using Flash Player must be shunted out
	 * to a separate ChartServer that is running in 32-bit mode.
	 * @return true if running on 64-bit; false otherwise.
	 */
	protected static boolean useChartServer() {
		// Determine if app is running on a 64-bit OS (i.e., QA & Birst live)
		if (logger.isTraceEnabled()) {
			DebugUtils.logEnviroment(logger, Level.TRACE);
		}
		// First check for web.xml/customer.properties override 
        boolean is64Bit = false;
        if (ChartServerContext.useStandaloneChartServer) {
            logger.debug("*** This instance of SMIWeb has requested to 'useStandaloneChartServer'. ***");
            is64Bit = true;
        } else {
        	// Then decide based on whether JVM is 32-bit (inline rendering) or 64-bit (chart server)
            final String sixtyFourBit = System.getProperty("sun.arch.data.model");
            if (sixtyFourBit != null && sixtyFourBit.equals("64")) {
                logger.debug("*** SMIWeb is running under a 64-bit JVM. ***");
			    is64Bit = true;
            }
        }
        
        if (is64Bit) {
            logger.debug("Forwarding all PDF export chart rendering requests to a stand-alone chart server.");
        }
        return is64Bit;
	}

	public Map<String, Color> getCategoryKeyColorMap() {
		return categoryKeyColorMap;
	}

	public void setCategoryKeyColorMap(Map<String, Color> categoryKeyColorMap) {
		this.categoryKeyColorMap = categoryKeyColorMap;
	}
}
