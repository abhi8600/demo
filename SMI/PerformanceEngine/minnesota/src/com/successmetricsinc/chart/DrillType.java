/**
 * $Id: DrillType.java,v 1.3 2009-02-20 19:51:18 agarrison Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.chart;

public class DrillType
{
	public String type;
	
	public static DrillType None = new DrillType(Chart.DRILL_TYPE_NONE);
	public static DrillType Drill = new DrillType(Chart.DRILL_TYPE_DRILL);
	public static DrillType Navigate = new DrillType(Chart.DRILL_TYPE_NAVIGATE);
	
	public DrillType(String t) {
		this.type = t;
	}
	
	public String toString() {
		return type;
	}
}