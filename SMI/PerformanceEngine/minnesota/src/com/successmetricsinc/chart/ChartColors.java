/**
 * $Id: ChartColors.java,v 1.21 2012-03-31 17:30:04 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.chart;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.Stroke;
import java.util.Random;
import java.awt.MultipleGradientPaint.CycleMethod;

import org.jfree.chart.plot.DrawingSupplier;

import com.successmetricsinc.chart.ChartGradientPaint.Direction;
import com.successmetricsinc.chart.ChartGradientPaint.Radius;

/**
 * Class to return chart colors and other elements to be supplied to charting components
 * 
 * @author bpeters
 * 
 */
public class ChartColors implements DrawingSupplier
{
	public static Color[] colors =
	{ new Color(78, 128, 77), new Color(153, 50, 51), new Color(254, 153, 0), new Color(252, 203, 4), new Color(153, 153, 101), new Color(0, 101, 153),
			new Color(0, 47, 47), new Color(153, 204, 205), new Color(239, 236, 202), new Color(182, 73, 38), new Color(88, 38, 28) };
	private Color[] colorsToUse = colors;
	private int weight = 3;
	private int outline = 0;
	private Color defaultColor;
	private boolean isGradient;
	private Direction direction;
	private boolean threeD;
	private int curColor = -1;
	private double[] startAngles;
	private double[] endAngles;
	private int angleIndex;
	private Radius radius;
	private boolean defaultColors = true;
	private Random r = new Random();

	public void resetCurColor()
	{
		curColor = -1;
	}
	
	/**
	 * @return the startAngles
	 */
	public double[] getStartAngles()
	{
		return startAngles;
	}

	/**
	 * @param startAngles
	 *            the startAngles to set
	 */
	public void setStartAngles(double[] startAngles)
	{
		this.startAngles = startAngles;
	}

	/**
	 * @return the endAngles
	 */
	public double[] getEndAngles()
	{
		return endAngles;
	}

	/**
	 * @param endAngles
	 *            the endAngles to set
	 */
	public void setEndAngles(double[] endAngles)
	{
		this.endAngles = endAngles;
	}

	public ChartColors(int weight, int outline)
	{
		this.weight = weight;
		this.outline = outline;
	}

	public ChartColors(int weight, int outline, Color defaultColor)
	{
		this.weight = weight;
		this.outline = outline;
		this.defaultColor = defaultColor;
	}

	public void setColors(Color[] colorsToUse)
	{
		this.colorsToUse = colorsToUse;
		defaultColors = false;
	}
	
	public int getNumColors() {
		return (defaultColors == true || this.colorsToUse == null) ? 0 : this.colorsToUse.length;
	}
	
	public Paint getNextPaint()
	{
		return getNextPaint(-1);
	}

	public Paint getNextPaint(int index)
	{
		Color returnColor = null;

		if (index == -1)
		{
			if (defaultColor != null)
				returnColor = defaultColor;
			else
			{
				if (curColor == (colorsToUse.length - 1))
				{
					returnColor = new Color(r.nextInt(255), r.nextInt(255), r.nextInt(255));
				}
				else
				{
					returnColor = colorsToUse[++curColor];
				}
			}
		}
		else
		{
			returnColor = colorsToUse[index];
		}
		if (isGradient)
		{
			float[] dist = null;
			Color[] colors = null;
			if (startAngles != null)
			{
				dist = new float[]
				{ 0.0f, 0.93f, 1f };
				Color shade1 = new Color(Math.min(255, returnColor.getRed() * 14 / 10), Math.min(255, returnColor.getGreen() * 14 / 10), Math.min(255,
						returnColor.getBlue() * 14 / 10));
				Color shade2 = new Color(2 * returnColor.getRed() / 3, 2 * returnColor.getGreen() / 3, 2 * returnColor.getBlue() / 3);
				colors = new Color[]
				{ shade1, returnColor, shade2 };
			} else if (threeD)
			{
				Color shade = new Color(returnColor.getRed() / 3, returnColor.getGreen() / 3, returnColor.getBlue() / 3);
				dist = new float[]
				{ 0.0f, 0.35f, 1.0f };
				colors = new Color[]
				{ shade, returnColor, shade };
			} else
			{
				dist = new float[]
				{ (float) 0.0, (float) 1 };
				colors = new Color[]
				{ Color.black, returnColor };
			}
			ChartGradientPaint p = new ChartGradientPaint(direction, dist, colors, direction == Direction.radial ? CycleMethod.NO_CYCLE : CycleMethod.REFLECT,
					returnColor);
			if (startAngles != null && endAngles != null && angleIndex < startAngles.length)
			{
				if (radius == null)
					radius = new Radius();
				p.setRadius(radius);
				if (angleIndex == 0)
					p.setResetRadius(true);
				p.setStartAngle(startAngles[angleIndex]);
				p.setEndAngle(endAngles[angleIndex++]);
				if (angleIndex >= startAngles.length)
				{
					angleIndex = 0;
					radius = null;
				}
			}
			return (p);
		}
		return (returnColor);
	}

	public Paint getNextOutlinePaint()
	{
		// TODO Auto-generated method stub
		return (Color.BLACK);
	}

	// XXX added with Jfreechart 1.0.9
	public Paint getNextFillPaint()
	{
		return (Color.BLACK);
	}

	public Stroke getNextStroke()
	{
		// TODO Auto-generated method stub
		return (new BasicStroke(weight));
	}

	public Stroke getNextOutlineStroke()
	{
		// TODO Auto-generated method stub
		return (new BasicStroke(outline));
	}

	public Shape getNextShape()
	{
		// TODO Auto-generated method stub
		return (new Rectangle());
	}

	/**
	 * @return the isGradient
	 */
	public boolean isGradient()
	{
		return isGradient;
	}

	/**
	 * @param isGradient
	 *            the isGradient to set
	 */
	public void setGradient(boolean isGradient)
	{
		this.isGradient = isGradient;
	}

	/**
	 * @param horizontalGradient
	 *            the horizontalGradient to set
	 */
	public void setDirection(Direction direction)
	{
		this.direction = direction;
	}

	/**
	 * @return the threeD
	 */
	public boolean isThreeD()
	{
		return threeD;
	}

	/**
	 * @param threeD
	 *            the threeD to set
	 */
	public void setThreeD(boolean threeD)
	{
		this.threeD = threeD;
	}

	/**
	 * @return the direction
	 */
	public Direction getDirection()
	{
		return direction;
	}
}
