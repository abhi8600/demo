package com.successmetricsinc.chart;

import java.awt.BorderLayout;
import java.io.File;
import java.io.FileNotFoundException;

import javax.swing.JFrame;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.jpackages.jflashplayer.FlashPanel;
import com.jpackages.jflashplayer.JFlashInvalidFlashException;
import com.jpackages.jflashplayer.JFlashLibraryLoadFailedException;
import com.successmetricsinc.util.DebugUtils;

public class ChartRenderer extends JFrame {
	/** 
	 * Required Flash Player version. 
	 * TODO: Should be externalized to config file. 
	 */
	public static final String REQUIRED_FLASH_VERSION = "10";
	
	/** 
	 * JFlashPlayer registration key for Birst. 
	 * TODO: Should be externalized to config file. 
	 */
	public static final String JFLASHPLAYER_REGISTRATION_KEY = "L6L4-36K9-94NI-4B7N-268N-84YK";
	
	/** 
	 * The number of milliseconds to pause before checking to see if the .swf is ready. 
	 * TODO: Should be externalized to config file. 
	 */
	private static final long SLEEP_INTERVAL = 100L;
	
	/** 
	 * The maximum number of milliseconds to pause before failing. (10 seconds)
	 * TODO: Should be externalized to config file. 
	 */
	private static final long MAX_SLEEP_INTERVAL = 10000L;
	
	private static final String PANEL_TITLE = "ChartRenderer instance for JFlashPlayer AnyChart chart generation";
	private static final String BAD_FLASH_VERSION 
	= "!!! Server does not have proper version of Flash installed! Should be version: %s. Functionality may be diminished.";
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(ChartRenderer.class);
	
	private FlashPanel flashPanel;
	private boolean swfReady;
	private long sleepTime;
	/** Not needed except for verification in unit tests. See FlashRendererTest. */
	private String flashFilePath;

	/**
	 * Allows the override of the default ChartRenderer.swf file location.
	 * There are three ways in which the flashFilePath can be set:
	 * <ul>
	 * <li>(unit testing) The FlashRenderer constructor can use a specific filepath.
	 * See FlashRendererTest for examples.</li>
	 * <li>If the environment variable -- SMI_INSTALL_DIR -- exists (which is normally the case
	 * for SMIWeb under Tomcat running as a service.</li>
	 * <li>If neither of these are provided, then the FlashRenderer constructor can use the 
	 * default filepath: {@link FlashRenderer#FLASH_FILE_PATH}.</li>
	 * </ul>
	 * @param flashFilePath is the new, non-default location of the .swf file.
	 * null value assumes the default (Tomcat) location for the .swf file.
	 */
	public ChartRenderer(final String flashFilePath) {
		super(PANEL_TITLE);

		// Check for valid filepath to ChartRenderer.swf
		validateSwfFileExists(flashFilePath);
		
		// Seed JFlashPlayer with registration key before any other access
		FlashPanel.setRegistrationKey(JFLASHPLAYER_REGISTRATION_KEY);

		// Check to see if the correct version of Flash is installed
		logger.debug("Flash Player, version: " + REQUIRED_FLASH_VERSION + " is required.");
		try {
			if (!FlashPanel.hasFlashVersion(REQUIRED_FLASH_VERSION)) {
				logger.error(String.format(BAD_FLASH_VERSION, REQUIRED_FLASH_VERSION));
			}
		} catch (JFlashLibraryLoadFailedException e) {
			logger.error("Attempting to check version of Flash Player, but load of JFlashPlayer library failed.");
			DebugUtils.logException(logger, e, Level.FATAL);
			if (logger.isTraceEnabled())
				DebugUtils.logEnviroment(logger, Level.FATAL);
			// throw new RuntimeException("Load of JFlashPlayer library failed.", e);
		}

		// Initialize the FlashPlayer .swf file using the override location
		this.flashFilePath = flashFilePath;
		try {
			// Construct a FlashPanel displaying the SWF flash file
			logger.debug("Creating instance of FlashPanel with path: " + flashFilePath);
			flashPanel = new FlashPanel(new File(flashFilePath));
			flashPanel.setScale(FlashPanel.SCALE_SHOWALL);

			// add the FlashPanel to the frame
			this.getContentPane().add(flashPanel, BorderLayout.CENTER);
		
			// specify the object for Flash ExternalInterface.call events to search for the called method on
			flashPanel.setFlashCallObject(this);		
		} catch (JFlashLibraryLoadFailedException e) {			
			logger.fatal("A required library (DLL) is missing or damaged.", e);
			DebugUtils.logEnviroment(logger, Level.FATAL);
			DebugUtils.logException(logger, e, Level.FATAL);
		} catch (FileNotFoundException e) {
			logger.fatal("Failed to find SWF file specified - " + flashFilePath, e);
			DebugUtils.logEnviroment(logger, Level.FATAL);
			DebugUtils.logException(logger, e, Level.FATAL);
		} catch (JFlashInvalidFlashException e) {
			logger.fatal("Required version " + REQUIRED_FLASH_VERSION + " of Flash is not installed.", e);
			DebugUtils.logEnviroment(logger, Level.FATAL);
			DebugUtils.logException(logger, e, Level.FATAL);
		}
	}

	/**
	 * Render the AnyChart chart using the XML input and the height and width provided.
	 * The output is a String representation of the PNG image.
	 * @param xml is the AnyChart XML input.
	 * @param height is the height of the rendered chart in pixels.
	 * @param width is the width of the rendered chart in pixels.
	 * @return the String representation of the PNG image of the requested chart.
	 */
	public synchronized String renderChart(String xml, int height, int width) {
		sleepTime = 0;
		while (true) {
			if (swfReady) {
				flashPanel.callFlashFunction("setXMLString", new Object[] { width, height, xml });
				flashPanel.setScale(FlashPanel.SCALE_SHOWALL);
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
				}
		        return (String)flashPanel.callFlashFunction("getImage", new Object[] { });
			}
			// Sleep before checking again. Time out if over the maximum time limit.
			try {
				Thread.sleep(SLEEP_INTERVAL);
				sleepTime += SLEEP_INTERVAL;
				if (sleepTime > MAX_SLEEP_INTERVAL) {
					throw new RuntimeException("Maximum time elapsed waiting for .swf file to be ready.");
				}
			} catch (InterruptedException e) {
				throw new RuntimeException("Failure while waiting for .swf file to be ready.", e);
			}
		}
	}
	
	/**
	 * Required by JFlashPlayer interface.
	 */
	public void setSWFIsReady() {
		this.swfReady = true;
    }

	/**
	 * Used in unit tests.
	 * @return the flashFilePath
	 */
	public String getFlashFilePath() {
		return flashFilePath;
	}
	
	/**
	 * Checks that a valid CheckRenderer.swf file exists. 
	 * @param flashFilePath is the fully-qualified file path to the ChartRenderer.swf file.
	 */
	private void validateSwfFileExists(String flashFilePath) {
		// Basic check
		if (flashFilePath == null || flashFilePath.equals("")) {
			if (ChartServerContext.chartServerSwfFilepath == null) {
				throw new IllegalArgumentException("Null or empty 'flashFilePath' passed to ChartRenderer().");
			} else {
				flashFilePath = ChartServerContext.chartServerSwfFilepath;
			}
		}
		
		// Now check to see that it exists
		if (!new File(flashFilePath).exists()) {
			throw new IllegalStateException("ChartRenderer.swf file was not found using path: " + flashFilePath);
		}
	}
}
