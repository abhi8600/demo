package com.successmetricsinc.chart;

import java.util.Properties;

import org.apache.log4j.Logger;

import com.successmetricsinc.util.Util;

/**
 * A minimal class that:
 * <ul>
 * <li>a conduit for customer.properties set from standalone batch PerformanceEngine
 * processing</li>
 * <li>serves to bridge the web.xml/customer.properties parameters back from the SMIWeb to 
 * the PerformanceEngine charting classes</li> 
 * <ul/>
 * In the case of taking data from the Tomcat webapp, the SMIWeb InitializationServlet loads the web.xml 
 * data via the ServerContext class.  When loading the four parameters needed by the Chart Server:
 * <dl>
 * <dt>useStandaloneChartServer</dt>
 * <dd>is 'true' if this instance of the webapp should use the standalone Chart Server
 * exclusively.  'false' will let the PerformanceEngine charting classes (specifically, FlashRenderer)
 * decide whether to do in-line rendering or not, based on the 32 or 64-bit mode of the SMIWeb app.</dd>
 * <dt>chartServerHostName</dt>
 * <dd>is the Chart Server host name to use for sending rendering requests</dd>
 * <dt>chartServerPortNumber</dt>
 * <dd>is the integer port number to use for sending rendering requests</dd>
 * <dt>chartServerTimeOut</dt>
 * <dd>is the number of milliseconds to wait before timing out the request to the chart server</dd>
 * <dt>chartServerSwfFilepath</dt>
 * <dd>is the full file path to the ChartRenderer.swf file</dd>
 * </dl>
 * As a default that will cover the case where offline PerformanceEngine reporting is being performed,
 * this class attempts to load the 'customer.properties' values at JVM initialization time.  If the
 * properties are not available, they will stay uninitialized unless the InitializationServlet 
 * sets their values.  In this way, batch processes can run Performance Engine without a ServletContext
 * or a ServerContext that initializes these values.
 * @author pconnolly
 */
public class ChartServerContext {

	public static final String USE_STANDALONE_KEY = "UseStandaloneChartServer";
	public static final String HOST_NAME_KEY = "ChartServerHostName";
	public static final String PORT_KEY = "ChartServerPortNumber";
	public static final String TIME_OUT_KEY = "ChartServerTimeOut";
	public static final String SWF_FILE_KEY = "ChartServerSwfFilepath";
	public static final String AMAP_FILE_KEY = "ChartServerAmapFilepath";
	private static final String INFO_PREFIX = "ChartServerContext: 'customer.properties - ";
	
	/** 
	 * Flag indicating whether or not to use the standalone chart server exclusively.
	 * This is necessary in certain cases like the Enterprise FMR webapp where, although it
     * runs under the 32-bit JVM, it cannot render charts in-line and must shunt all render 
     * requests out to a separate chart server. 
	 */
	public static boolean useStandaloneChartServer;
	/** The host name of the chart server. */
	public static String chartServerHostName;
	/** The port number of the chart server. */
	public static int chartServerPortNumber;
	/** The number of milliseconds before timing out a render request. */
	public static int chartServerTimeOut;
	/** The full filepath to the ChartRenderer.swf AnyChart SWF file. */
	public static String chartServerSwfFilepath;
	/** The full path to the root AnyMap .amap file directory. */
	public static String chartServerAmapFilepath;

	private static Logger logger = Logger.getLogger(ChartServerContext.class);
	
	static {
		// Get the 'customer.properties' location
		final Properties properties = Util.getProperties(Util.CUSTOMER_PROPERTIES_FILENAME, true);
		if (properties == null) {
			// If 'customer.properties' not provided issue warning
			logger.error("Could not read chart server parameters from the customer.properties file.");
		} else {
			// Load the properties into the static fields
			final String standalone = properties.getProperty(ChartServerContext.USE_STANDALONE_KEY);
			if (standalone != null) {
				useStandaloneChartServer = standalone.equalsIgnoreCase("true") ? true : false;
			}
			logger.debug(INFO_PREFIX + "'UseStandaloneChartServer' to: " + useStandaloneChartServer);
			chartServerHostName = properties.getProperty(ChartServerContext.HOST_NAME_KEY);
			logger.debug(INFO_PREFIX + "'ChartServerHostName' to: " + chartServerHostName);
			final String port = properties.getProperty(ChartServerContext.PORT_KEY);
			if (port != null) {
				chartServerPortNumber = Integer.valueOf(port);
			}
			logger.debug(INFO_PREFIX + "'ChartServerPortNumber to: " + chartServerPortNumber);
			final String timeout = properties.getProperty(ChartServerContext.TIME_OUT_KEY);
			if (timeout != null) {
				chartServerTimeOut = Integer.valueOf(timeout);
			}
			logger.debug(INFO_PREFIX + "'ChartServerTimeOut' to: " + chartServerTimeOut);
			chartServerSwfFilepath = properties.getProperty(ChartServerContext.SWF_FILE_KEY);
			logger.debug(INFO_PREFIX + "'ChartServerSwfFilepath' to: " + chartServerSwfFilepath);
			chartServerAmapFilepath = properties.getProperty(ChartServerContext.AMAP_FILE_KEY);
			logger.debug(INFO_PREFIX + "'ChartServerAmapFilepath' to: " + chartServerAmapFilepath);
		}
	}
}
