/**
 * $Id: JCommonDrawableRenderer.java,v 1.63 2012-06-18 23:56:28 agarrison Exp $
 *
 * Copyright (C) 2007-2009 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.chart;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.geom.Dimension2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import net.sf.jasperreports.engine.JRAbstractSvgRenderer;
import net.sf.jasperreports.engine.export.XMLExporter;

import org.apache.log4j.Logger;
import org.apache.xerces.impl.dv.util.Base64;
import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.entity.StandardEntityCollection;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.XYQuadrantPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.title.LegendTitle;
import org.jfree.chart.title.TextTitle;
import org.jfree.ui.Drawable;

import com.successmetricsinc.util.FileUtils;

public class JCommonDrawableRenderer extends JRAbstractSvgRenderer
{
	private static final int WAIT_FOR_CHART_SERVER_TO_RECOVER = 30000;
	private static final String CHART_END = "</anychart>";
	private static Logger logger = Logger.getLogger(JCommonDrawableRenderer.class);
	private static final long serialVersionUID = 1L;
	private static final String LOG_INTRO = "Entering render() method with rectangle, width: %f and height: %f";
	/** Factor by which to scale up the image for clarity purposes. */
	private static final double SCALE_FACTOR = 1.625;
//	private static final int INT_SCALE_FACTOR = 1;
	private static final double UNSCALE_FACTOR = 1/SCALE_FACTOR;
	
	private double scaleFactor = 1.0;
	
	private boolean imageMap = false;
	private ChartRenderingInfo cri;
	private Font titleFont;
	private Font legendFont;
	private List<Font> calist = new ArrayList<Font>();
	private List<Font> valist = new ArrayList<Font>();
	private Font pieLabelFont = null;
	private Rectangle2D chartRectangle;
	private BufferedImage bi;
	
	protected Drawable drawable = null;
	private ChartRenderer chartRenderer; 
	private String flashFilePath;
	private String chartServerHostName;
	private int chartServerPortNumber;
	private int chartServerTimeOut;
	
	/**
	 * Used by SpiderWebPlot and ContourPlot.
	 * @param drawable
	 */
	public JCommonDrawableRenderer(Drawable drawable, final String flashFilePath) {
		this.drawable = drawable;
		this.flashFilePath = flashFilePath;
	}

	/**
	 * Used by SpiderWebPlot and ContourPlot.
	 * @param drawable
	 */
	public JCommonDrawableRenderer(Drawable drawable) {
		this.drawable = drawable;
		this.flashFilePath = FlashRenderer.getDefaultFlashFilePath();
	}

	public JCommonDrawableRenderer() {
		this.flashFilePath = FlashRenderer.getDefaultFlashFilePath();
	}
	
	/**
	 * The main render method that transforms the chart request into a rendered chart image.
	 * There are three main options:
	 * <ul>
	 * <li>If running in a 32-bit JVM, the charts can be rendered in-line
	 * <p/>
	 * Chart graphics can be rendered in-line (i.e., the JVM is running in 32-bit mode and
	 * can communicate with the 32-bit AnyChart Flash Player DLLs from JFlashPlayer).
	 * </li>
	 * <li>If running in a 64-bit JVM, or if there has been an intentional override
	 * the charts will be rendered in a stand-alone 32-bit Chart Server
	 * <p/>
	 * Chart graphics must be rendered in a separate JVM (either because this process is running
	 * under a 64-bit JVM which cannot communicate with the 32-bit Flash Player DLLs) or the 
	 * configuration has specified an override that all chart requests go to a stand-alone chart
	 * server.  In either case, the AnyChart XML is forwarded via a TCP socket to the listening
	 * Chart Server and a rendered PNG is returned.
	 * </li>
	 * <li>If, in the case of dashboards, the PNG has already been rendered, no
	 * re-rendering is required and the cached PNG can be immediately converted to the
	 * final BufferedImage.
	 * <p/>
	 * The chart may have already been rendered and been cached. In this case there is no 
	 * need to re-render and the PNG will be reconverted to the BufferedImage, bypassing
	 * the render step.
	 * </li>
	 * </ul>
	 */
	public void render(Graphics2D grx, Rectangle2D rectangle) {
		if (logger.isTraceEnabled())
			logger.trace(String.format(LOG_INTRO, rectangle.getWidth(), rectangle.getHeight()));
		this.chartRectangle = rectangle;

		// null byteArray can only happen when called by JRPdfExporter
		byte[] byteArray = generateByteArray(rectangle);
		if (byteArray != null)
			convertByteArrayToBufferedImage(grx, byteArray);
		else {
			if (drawable != null)
			{
				// Use JFreeChart if ChartServer not functioning
				logger.error("Attempted to render chart using Chart Server. Falling back to JFreeChart because of error.");
				drawable.draw(grx, rectangle);
			}
		}
	}
	
	private byte[] generateByteArray(Rectangle2D rectangle) {
		XMLExporter flashRenderer = (XMLExporter) this;
//		flashRenderer.setAnimation(false);
		String xml = flashRenderer.getXML(SCALE_FACTOR * scaleFactor, false, true);
		if (xml == null)
			return null;
		
		// split out
		int lenOfEnd = CHART_END.length();
		int endIndex = xml.indexOf(CHART_END);
		if (endIndex >= 0)
			endIndex = xml.indexOf(CHART_END, endIndex+1);
		if (endIndex >= 0 && endIndex < xml.length() + lenOfEnd) {
			int imageWidth = (int)(rectangle.getWidth() * SCALE_FACTOR * scaleFactor);
			int imageHeight = (int)(rectangle.getHeight() * SCALE_FACTOR * scaleFactor);
			BufferedImage image = new BufferedImage(imageWidth,	imageHeight, BufferedImage.TYPE_INT_RGB);
			Graphics g = image.getGraphics();
			g.setColor(Color.white);
			g.fillRect(0, 0, imageWidth, imageHeight);
			
			String[] chartXmls = xml.split(CHART_END);
			for (int i = 0; i < chartXmls.length - 1; i++) {
				int x = 0;
				int y = 0;
				int width = (int)rectangle.getWidth();
				int height = (int)rectangle.getHeight();
				int widthIndex = chartXmls[i].indexOf("width=");
				if (widthIndex > 0) {
					endIndex = chartXmls[i].indexOf('"', widthIndex + 7);
					String wStr = chartXmls[i].substring(widthIndex + 7, endIndex);
					try {
						width = Integer.parseInt(wStr);
					}
					catch (Exception e) {}
				}
				int heightIndex = chartXmls[i].indexOf("height=");
				if (heightIndex > 0) {
					endIndex = chartXmls[i].indexOf('"', heightIndex + 8);
					String hStr = chartXmls[i].substring(heightIndex + 8, endIndex);
					try {
						height = Integer.parseInt(hStr);
					}
					catch (Exception e) {}
				}
				int xIndex = chartXmls[i].indexOf("x=");
				if (xIndex > 0) {
					endIndex = chartXmls[i].indexOf('"', xIndex + 3);
					String xStr = chartXmls[i].substring(xIndex + 3, endIndex);
					try {
						x = Integer.parseInt(xStr);
					}
					catch (Exception e) {}
				}
				int yIndex = chartXmls[i].indexOf("y=");
				if (yIndex > 0) {
					endIndex = chartXmls[i].indexOf('"', yIndex + 3);
					String yStr = chartXmls[i].substring(yIndex + 3, endIndex);
					try {
						y = Integer.parseInt(yStr);
					}
					catch (Exception e) {}
				}
				// we have more than 1
				
				// render each one and combine
				Rectangle2D newRectangle = new Rectangle(width, height);
				endIndex = chartXmls[i].indexOf("<anychart");
				if (endIndex < 0)
					break;
				String firstXML = chartXmls[i].substring(endIndex) + "</anychart>";
				String chart = render(newRectangle, firstXML);
		        byte[] chartBytes = Base64.decode(chart);
		        try {
			        Image imagePart = ImageIO.read(new ByteArrayInputStream(chartBytes));
			        imagePart = imagePart.getScaledInstance(width, height, Image.SCALE_SMOOTH);
			        g.drawImage(imagePart, x, y, null);
		        }
		        catch (Exception e) {
		        	logger.error(e, e);
		        }
			}
			try {
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				ImageIO.write(image, "png", baos);
				return baos.toByteArray();
			}
			catch (Exception e) {
				logger.error(e, e);
				return null;
			}
			finally {
				g.dispose();
			}
		}
		
		String chart = render(rectangle, xml);
        return Base64.decode(chart);
	}
	
	private static final int NUMBER_OF_CHART_SERVER_RETRIES = 20;
	private String render(final Rectangle2D rectangle, final String xml) {
		String chart = null;
		// Either render in-line or via the Chart Server
		if (FlashRenderer.useChartServer())
			// try a few times in case there's a recoverable error on the chart server
			for (int i = 0; i < NUMBER_OF_CHART_SERVER_RETRIES && chart == null; i++) {
				try {
					chart = renderViaChartServer(rectangle, xml);
				} catch (Exception e) {
					logger.error(e, e);
					chart = null;
					try {
						Thread.sleep(WAIT_FOR_CHART_SERVER_TO_RECOVER);
					} catch (InterruptedException e1) {
					}
				}
			}

		if (chart == null)
			chart = renderInLine(rectangle, xml);
		
		return chart;
	}
	
	/**
	 * Chart graphics can be rendered in-line (i.e., the JVM is running in 32-bit mode and
	 * can communicate with the 32-bit AnyChart Flash Player DLLs from JFlashPlayer).
	 * @param grx is the graphics context.
	 * @param rectangle is the dimension into which the chart will be rendered.
	 * @param xml is the AnyChart XML to be rendered.
	 */
	protected String renderInLine(final Rectangle2D rectangle, final String xml) {
		// Log only at 'trace' level. Large output!
		if (logger.isTraceEnabled())
			logger.trace("XML input to chart renderer:\n" + xml);
		// Get the singleton ChartRenderer instance and plug it into parent.
		if (chartRenderer == null) {
			chartRenderer = ChartRendererFactory.createInstance(flashFilePath);
			chartRenderer.setVisible(true);
			chartRenderer.setVisible(false);
		}
		final String chart = chartRenderer.renderChart(xml, 
				(int)(rectangle.getHeight() * SCALE_FACTOR), 
				(int)(rectangle.getWidth() * SCALE_FACTOR));
        if (chart == null)
        {
        	logger.warn("chartRenderer.renderChart returned null, probably a configuration issue");
        	return null;
        }
		// logger.debug("'chart' String output from ChartRenderer.renderChart():\n" + chart);
        return chart;
	}

	/**
	 * Chart graphics must be rendered in a separate JVM (either because this process is running
	 * under a 64-bit JVM which cannot communicate with the 32-bit Flash Player DLLs) or the 
	 * configuration has specified an override that all chart requests go to a stand-alone chart
	 * server.  In either case, the AnyChart XML is forwarded via a TCP socket to the listening
	 * Chart Server and a rendered PNG is returned.
	 * @param grx is the graphics context.
	 * @param rectangle is the dimension into which the chart will be rendered.
	 * @param xml is the AnyChart XML to be rendered.
	 * @throws RemoteException 
	 */
	protected String renderViaChartServer(final Rectangle2D rectangle, final String xml) throws RemoteException {
		if (this instanceof XMLExporter) {
			chartServerHostName = ChartServerContext.chartServerHostName;
			chartServerPortNumber = ChartServerContext.chartServerPortNumber;
			ChartServiceStub cs = new ChartServiceStub(chartServerHostName + ":" + chartServerPortNumber + "/SMIChartServer/services/ChartService.ChartServiceHttpSoap12Endpoint/");
			ChartServiceStub.RenderChart rc = new ChartServiceStub.RenderChart();
			rc.setHeight((int)(rectangle.getHeight() * SCALE_FACTOR * scaleFactor));
			rc.setWidth((int)(rectangle.getWidth() * SCALE_FACTOR * scaleFactor));
			rc.setXml(xml);
			ChartServiceStub.RenderChartResponse resp = cs.renderChart(rc);
			String str = resp.get_return();
			return str;

		}
		return null;
	}

	/**
	 * The chart has already been rendered and has been cached. In this case there is no 
	 * need to re-render and the PNG will be reconverted to the BufferedImage, bypassing
	 * the render step.
	 * @param grx is the graphics context.
	 * @param rectangle is the dimension into which the chart will be rendered.
	 * @param xml is the AnyChart XML to be rendered.
	 */
	protected void convertByteArrayToBufferedImage(final Graphics2D grx, final byte[] byteArray) {
		ByteArrayInputStream bais = null;
		FileOutputStream fos = null;
		try 
		{
			bais = new ByteArrayInputStream(byteArray);
			bi = ImageIO.read(bais);
			grx.drawImage(bi, AffineTransform.getScaleInstance(UNSCALE_FACTOR/scaleFactor, UNSCALE_FACTOR/scaleFactor), null);
			if (logger.isDebugEnabled()) 
			{
				// Write out the PNG file for debugging purposes
				try
				{
					File pngFile = FileUtils.createTempFile(getClass().getSimpleName() + "_render_", ".png");
					fos = new FileOutputStream(pngFile);
					fos.write(byteArray);
					logger.debug("Wrote out chart's PNG file: " + pngFile.getCanonicalPath());
				}
				catch (IOException e)
				{
					logger.error("Failed to write debug chart image. ", e);
				}
			}
		} catch (IOException e) {
			logger.error("Failed to read chart image. ", e);
		}
		finally
		{
			try
			{
				if (bais != null)
					bais.close();
				if (fos != null)
					fos.close();
			}
			catch (IOException e)
			{
				logger.debug(e, e);
			}
		}
	}
	
	/**
	 * note that this must be synchronized so that we don't double scale fonts
	 * 
	 * @param width
	 * @param height
	 * @param scaleFactor
	 * @return
	 * @throws IOException
	 */
	public synchronized ChartRenderingInfo renderPNG(int width, int height, double scaleFactor) throws IOException {
		if (cri == null) {
			getJFreeChart(width, height, scaleFactor);
		}
		return (cri);
	}

	private ChartRenderingInfo getJFreeChart(int width, int height, double scaleFactor) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		cri = new ChartRenderingInfo(new StandardEntityCollection());
		JFreeChart chart = (JFreeChart) drawable;
		// Scale the fonts if necessary
		if (scaleFactor != 1) {
			TextTitle tt = chart.getTitle();
			if (tt.getText().length() == 0)
				tt = null;
			if (tt != null) {
				titleFont = tt.getFont();
				float tsize = titleFont.getSize2D();
				tt.setFont(titleFont.deriveFont((float) (tsize * scaleFactor)));
			}
			Plot p = chart.getPlot();
			if (PiePlot.class.isInstance(p)) {
				PiePlot pp = (PiePlot) p;
				pieLabelFont = pp.getLabelFont();
				float size = pieLabelFont.getSize2D();
				pp.setLabelFont(pieLabelFont.deriveFont((float) (size * scaleFactor)));
			} else if (CategoryPlot.class.isInstance(p)) {
				CategoryPlot cp = (CategoryPlot) p;
				for (int i = 0; i < cp.getDomainAxisCount(); i++) {
					CategoryAxis ca = cp.getDomainAxis(i);
					Font ft = ca.getLabelFont();
					calist.add(ft);
					float size = ft.getSize2D();
					ca.setLabelFont(ft.deriveFont((float) (size * scaleFactor)));
					ft = ca.getTickLabelFont();
					calist.add(ft);
					size = ft.getSize2D();
					ca.setTickLabelFont(ft.deriveFont((float) (size * scaleFactor)));
				}
				for (int i = 0; i < cp.getRangeAxisCount(); i++) {
					ValueAxis va = cp.getRangeAxis(i);
					Font ft = va.getLabelFont();
					valist.add(ft);
					float size = ft.getSize2D();
					va.setLabelFont(ft.deriveFont((float) (size * scaleFactor)));
					ft = va.getTickLabelFont();
					valist.add(ft);
					size = ft.getSize2D();
					va.setTickLabelFont(ft.deriveFont((float) (size * scaleFactor)));
				}
			} else if (XYQuadrantPlot.class.isInstance(p)) {
				XYQuadrantPlot cp = (XYQuadrantPlot) p;
				for (int i = 0; i < cp.getDomainAxisCount(); i++) {
					ValueAxis ca = cp.getDomainAxis(i);
					Font ft = ca.getLabelFont();
					calist.add(ft);
					float size = ft.getSize2D();
					ca.setLabelFont(ft.deriveFont((float) (size * scaleFactor)));
					ft = ca.getTickLabelFont();
					calist.add(ft);
					size = ft.getSize2D();
					ca.setTickLabelFont(ft.deriveFont((float) (size * scaleFactor)));
				}
				for (int i = 0; i < cp.getRangeAxisCount(); i++) {
					ValueAxis va = cp.getRangeAxis(i);
					Font ft = va.getLabelFont();
					valist.add(ft);
					float size = ft.getSize2D();
					va.setLabelFont(ft.deriveFont((float) (size * scaleFactor)));
					ft = va.getTickLabelFont();
					valist.add(ft);
					size = ft.getSize2D();
					va.setTickLabelFont(ft.deriveFont((float) (size * scaleFactor)));
				}
				XYLineAndShapeRenderer r = (XYLineAndShapeRenderer) cp.getRenderer();
				for (int i = 0; i < cp.getSeriesCount(); i++) {
					java.awt.Shape sh = r.getSeriesShape(i);
					java.awt.geom.Rectangle2D rect = sh.getBounds2D();
					double sz = rect.getHeight();
					double newSz = sz * scaleFactor;
					// cheat knowing that it is a circle (ellipse actually)
					r.setSeriesShape(i, new Ellipse2D.Double(0, 0, newSz, newSz));
				}
			}
			LegendTitle lt = chart.getLegend();
			if (lt != null) {
				float lsize = lt.getItemFont().getSize2D();
				legendFont = lt.getItemFont();
				lt.setItemFont(lt.getItemFont().deriveFont((float) (lsize * scaleFactor)));
			}
		}
		ChartUtilities.writeChartAsPNG(baos, chart, width, height, cri);
		baos.close();
		@SuppressWarnings("unused")
		byte[] byteArray = baos.toByteArray();
		// Return the fonts to their original scale, if necessary
		if (scaleFactor != 1) {
			// If necessary, scale all fonts
			TextTitle tt = chart.getTitle();
			if (tt.getText().length() == 0)
				tt = null;
			if (tt != null) {
				tt.setFont(titleFont);
			}
			Plot p = chart.getPlot();
			if (PiePlot.class.isInstance(p)) {
				PiePlot pp = (PiePlot) p;
				pp.setLabelFont(pieLabelFont);
			} else if (CategoryPlot.class.isInstance(p)) {
				CategoryPlot cp = (CategoryPlot) p;
				for (int i = 0; i < cp.getDomainAxisCount(); i++) {
					CategoryAxis ca = cp.getDomainAxis(i);
					ca.setLabelFont(calist.get(2 * i));
					ca.setTickLabelFont(calist.get(2 * i + 1));
				}
				for (int i = 0; i < cp.getRangeAxisCount(); i++) {
					ValueAxis va = cp.getRangeAxis(i);
					va.setLabelFont(valist.get(2 * i));
					va.setTickLabelFont(valist.get(2 * i + 1));
				}
			} else if (XYQuadrantPlot.class.isInstance(p)) {
				XYQuadrantPlot cp = (XYQuadrantPlot) p;
				for (int i = 0; i < cp.getDomainAxisCount(); i++) {
					ValueAxis ca = cp.getDomainAxis(i);
					ca.setLabelFont(calist.get(2 * i));
					ca.setTickLabelFont(calist.get(2 * i + 1));
				}
				for (int i = 0; i < cp.getRangeAxisCount(); i++) {
					ValueAxis va = cp.getRangeAxis(i);
					va.setLabelFont(valist.get(2 * i));
					va.setTickLabelFont(valist.get(2 * i + 1));
				}
				XYLineAndShapeRenderer r = (XYLineAndShapeRenderer) cp.getRenderer();
				for (int i = 0; i < cp.getSeriesCount(); i++) {
					java.awt.Shape sh = r.getSeriesShape(i);
					r.setSeriesShape(i, sh);
				}
			}

			LegendTitle lt = chart.getLegend();
			if (lt != null) {
				lt.setItemFont(legendFont);
			}
		}
		return cri;
	}

	/**
	 * @return Returns the byteArray.
	 */
	public byte[] getByteArray() {
		return getImageData();
	}

	/**
	 * @return Returns the byteArray.
	 */
	public byte[] getImageData() {
		Dimension2D dimension = getDimension();
		Rectangle2D r = chartRectangle;
		if (dimension != null)
			r = new Rectangle((int)dimension.getWidth(), (int)dimension.getHeight());
		
		return this.generateByteArray(r);
	}

	/**
	 * @return Returns the imageMap.
	 */
	public boolean isImageMap() {
		return imageMap;
	}

	/**
	 * @param imageMap The imageMap to set.
	 */
	public void setImageMap(boolean imageMap) {
		this.imageMap = imageMap;
	}

	/**
	 * @return Returns the drawable.
	 */
	public Drawable getDrawable() {
		return drawable;
	}

	/**
	 * @param drawable The drawable to set.
	 */
	public void setDrawable(Drawable drawable) {
		this.drawable = drawable;
	}
	
	/**
	 * Sets the instance of the ChartRenderer.
	 * @param an instance of the ChartRenderer.
	 */
	public void setChartRenderer(final ChartRenderer chartRenderer) {
		this.chartRenderer = chartRenderer;
	}
	
	/**
	 * Returns an instance of the ChartRenderer.
	 * @return an instance of the ChartRenderer.
	 */
	public ChartRenderer getChartRenderer() {
		return this.chartRenderer;
	}
	
	/**
	 * Returns the current value of the chart's Rectangle.
	 * @return the current value of the chart's Rectangle.
	 */
	public Rectangle2D getChartRectangle() {
		return chartRectangle;
	}
	
	/**
	 * Returns the AnyChart BufferedImage after the render() method is invoked.
	 * @return can return null if called before the render() method.
	 */
	public BufferedImage getBufferedImage() {
		return bi;
	}
	
	/**
	 * Returns the ChartServer's hostname.
	 * @return string representation of the ChartServer's hostname.
	 */
	public String getChartServerHostName() {
		return chartServerHostName;
	}
	
	/**
	 * Returns the ChartServer's port.
	 * @return string representation of the ChartServer's port.
	 */
	public int getChartServerPortNumber() {
		return chartServerPortNumber;
	}
	
	/**
	 * Returns the ChartServer's timeout in milliseconds.
	 * @return integer milliseconds of the ChartServer's timeout limit.
	 */
	public int getChartServerTimeOut() {
		return chartServerTimeOut;
	}
	
	/* (non-Javadoc)
	 * @see net.sf.jasperreports.engine.JRAbstractSvgRenderer#getType()
	 */
	@Override
	public byte getType() {
		return TYPE_BIRST;
	}

	/* (non-Javadoc)
	 * @see net.sf.jasperreports.engine.JRAbstractSvgRenderer#setScale(double)
	 */
	@Override
	public void setScale(double d) {
		//scaleFactor = d;
	}
	
	
}
