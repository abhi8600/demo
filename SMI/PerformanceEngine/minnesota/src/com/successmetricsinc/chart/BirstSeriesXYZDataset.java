/**
 * 
 */
package com.successmetricsinc.chart;

import java.awt.Color;
import java.util.Map;

import org.jfree.data.xy.SeriesXYZDataset;

/**
 * @author agarrison
 *
 */
public class BirstSeriesXYZDataset extends SeriesXYZDataset {
	private static final long serialVersionUID = 1L;
	public Map<String, Color> categoryColorMap = null;

}
