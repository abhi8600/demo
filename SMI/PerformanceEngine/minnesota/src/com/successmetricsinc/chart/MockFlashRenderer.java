package com.successmetricsinc.chart;

import org.jfree.ui.Drawable;

/**
 * Basically creates an adapter over the original FlashRenderer that 
 * allows unit testing of pre-built AnyChart XML inputs.
 * @author pconnolly
 */
public class MockFlashRenderer extends FlashRenderer {
	
	private static final long serialVersionUID = 8094042671646264546L;
	
	private String anyChartXML;
	
	public MockFlashRenderer(final Drawable drawable, final String anyChartXML, final String flashFilePath) {
		super(drawable, flashFilePath);
		this.anyChartXML = anyChartXML;
	}
	
	@Override
	public String getXML(final double scaleFactor) {
		return this.anyChartXML;
	}
}
