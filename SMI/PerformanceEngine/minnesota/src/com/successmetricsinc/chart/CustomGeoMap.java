/**
 * 
 */
package com.successmetricsinc.chart;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.util.BirstXMLSerializable;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class CustomGeoMap extends BirstXMLSerializable implements Serializable {

	private static final long serialVersionUID = 1L;

	private static Logger logger = Logger.getLogger(CustomGeoMap.class);
	
	private static final String LOCATION = "Location";
	private static final String VALUE = "Value";
	private static final String LABEL = "Label";
	private static final String FILENAME = "CustomGeoMaps.xml";
	
	private String label;
	private String value;
	private String amapLocation;
	
	/* (non-Javadoc)
	 * @see com.successmetricsinc.util.BirstXMLSerializable#addContent(org.apache.axiom.om.OMFactory, org.apache.axiom.om.OMElement, org.apache.axiom.om.OMNamespace)
	 */
	@Override
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, LABEL, label, ns);
		XmlUtils.addContent(fac, parent, VALUE, value, ns);
		XmlUtils.addContent(fac, parent, LOCATION, amapLocation, ns);
	}

	/* (non-Javadoc)
	 * @see com.successmetricsinc.util.BirstXMLSerializable#createNew()
	 */
	@Override
	public BirstXMLSerializable createNew() {
		return new CustomGeoMap();
	}

	/* (non-Javadoc)
	 * @see com.successmetricsinc.util.BirstXMLSerializable#parseElement(org.apache.axiom.om.OMElement)
	 */
	@Override
	public void parseElement(OMElement parent) {
		for (@SuppressWarnings("unchecked")
		Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();
			if (LABEL.equals(elName)) {
				label = XmlUtils.getStringContent(el);
			}
			else if (VALUE.equals(elName)) {
				value = XmlUtils.getStringContent(el);
			}
			else if (LOCATION.equals(elName)) {
				amapLocation = XmlUtils.getStringContent(el);
			}
		}
	}
	
	@Override
	public void parseElement(XMLStreamReader parser) throws Exception {
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
			String elName = parser.getLocalName();
			if (LABEL.equals(elName)) {
				label = XmlUtils.getStringContent(parser);
			}
			else if (VALUE.equals(elName)) {
				value = XmlUtils.getStringContent(parser);
			}
			else if (LOCATION.equals(elName)) {
				amapLocation = XmlUtils.getStringContent(parser);
			}
		}
	}

	public static void initialize(Repository repository) {
		File f = new File(repository.getServerParameters().getApplicationPath(), FILENAME);
		if (f.exists()) {
			XMLStreamReader parser = null;
			FileInputStream fileInputStream = null;
			try {
				fileInputStream = new FileInputStream(f);
				XMLInputFactory inputFactory = XmlUtils.getSecureXMLInputFactory();
				parser = inputFactory.createXMLStreamReader(fileInputStream);
				StAXOMBuilder builder = new StAXOMBuilder(parser);
				OMElement doc = null;
				doc = builder.getDocumentElement();
				List<CustomGeoMap> maps = new ArrayList<CustomGeoMap>();
				XmlUtils.parseAllChildren(doc, new CustomGeoMap(), maps);
				repository.setCustomGeoMaps(maps);
			} catch (FileNotFoundException e) {
				logger.error(e, e);
			} catch (XMLStreamException e) {
				logger.error(e, e);
			} catch (FactoryConfigurationError e) {
				logger.error(e, e);
			}finally{
				try	{
					if(parser != null){
						parser.close();
					}
				}catch(Exception ex){
					logger.warn("Error in closing parser object");
				}
				try {
					if(fileInputStream != null)	{
						fileInputStream.close();
					}
				}catch(Exception ex){
					logger.warn("Error in closing fileinputStream object");
				}
			}
		}
	}
	
	public static String getAmapLocation(Repository repository, String value) {
		List<CustomGeoMap> maps = repository.getCustomGeoMaps();
		if (maps != null) {
			for (CustomGeoMap map: maps) {
				if (map.value != null && map.value.equals(value))
					return map.amapLocation;
			}
		}
		return null;
	}

	public static OMElement getCustomGeoMaps(Repository r) {
		List<CustomGeoMap> maps = r.getCustomGeoMaps();
		if (maps != null) {
			OMFactory fac = OMAbstractFactory.getOMFactory();
			OMNamespace ns = null;
			OMElement parent = fac.createOMElement("CustomGeoMaps", ns);
			XmlUtils.addContent(fac, parent, "CustomGeoMapList", maps, ns);
			return parent;
		}
		return null;
	}

}
