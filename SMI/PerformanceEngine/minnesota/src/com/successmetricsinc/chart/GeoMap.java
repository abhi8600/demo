/**
 * 
 */
package com.successmetricsinc.chart;

import java.util.HashMap;

/**
 * @author agarrison
 *
 */
public class GeoMap {
	
	private static final String[] _validMapNames = {
		"africa/algeria", 
		"africa/angola",
		"africa/benin",
		"africa/botswana",
		"africa/burkina_faso",
		"africa/burundi",
		"africa/cameroon",
		"africa/central_african_republic",
		"africa/chad",
		"africa/congo",
		"africa/djibouti",
		"africa/egypt",
		"africa/equatorial_guinea",
		"africa/eritrea",
		"africa/ethiopia",
		"africa/gabon",
		"africa/gambia",
		"africa/ghana",
		"africa/guinea",
		"africa/guinea_bissau",
		"africa/ivory_coast",
		"africa/kenya",
		"africa/lesotho",
		"africa/liberia",
		"africa/madagascar",
		"africa/malawi",
		"africa/mali",
		"africa/mauritania",
		"africa/morocco",
		"africa/mozambique",
		"africa/namibia",
		"africa/niger",
		"africa/nigeria",
		"africa/rwanda",
		"africa/senegal",
		"africa/sierra_leone",
		"africa/somalia",
		"africa/south_africa",
		"africa/sudan",
		"africa/swaziland",
		"africa/tanzania",
		"africa/togo",
		"africa/tunisia",
		"africa/uganda",
		"africa/zaire",
		"africa/zambia",
		
		"asia/afghanistan",
		"asia/bahrain",
		"asia/bangladesh",
		"asia/bhutan",
		"asia/cambodia",
		"asia/china",
		"asia/cyprus",
		"asia/india",
		"asia/indonesia",
		"asia/iran",
		"asia/iraq",
		"asia/israel",
		"asia/japan",
		"asia/jordan",
		"asia/kazakhstan",
		"asia/kuwait",
		"asia/laos",
		"asia/lebanon",
		"asia/malaysia",
		"asia/maldives",
		"asia/mongolia",
		"asia/myanmar",
		"asia/nepal",
		"asia/north_korea",
		"asia/pakistan",
		"asia/philippines",
		"asia/russia",
		"asia/saudi_arabia",
		"asia/south_korea",
		"asia/syria",
		"asia/taiwan",
		"asia/thailand",
		"asia/turkey",
		"asia/united_arab_emirates",
		"asia/vietnam",
		"asia/yemen",
		
		"europe/albania",
		"europe/austria",
		"europe/austria_second_level",
		"europe/belgium",
		"europe/belgium_second_level",
		"europe/bulgaria",
		"europe/byelarus",
		"europe/czech_republic",
		"europe/denmark",
		"europe/finland",
		"europe/france",
		"europe/france_second_level",
		"europe/germany",
		"europe/germany_second_level",
		"europe/greece",
		"europe/hungary",
		"europe/iceland",
		"europe/ireland",
		"europe/italy",
		"europe/italy_second_level",
		"europe/netherlands",
		"europe/norway",
		"europe/norway_second_level",
		"europe/poland",
		"europe/portugal",
		"europe/romania",
		"europe/spain",
		"europe/sweden",
		"europe/sweden_second_level",
		"europe/switzerland",
		"europe/uk_england",
		"europe/uk_northern_ireland",
		"europe/uk_scotland",
		"europe/uk_wales",
		"europe/ukraine",
		"europe/united_kingdom",
		"north_america/belize",
		"north_america/canada",
		"north_america/costa_rica",
		"north_america/cuba",
		"north_america/el_salvador",
		"north_america/greenland",
		"north_america/guatemala",
		"north_america/haiti",
		"north_america/honduras",
		"north_america/jamaica",
		"north_america/mexico",
		"north_america/nicaragua",
		"north_america/panama",
		"north_america/usa_and_canada",
		"north_america/usa_canada_and_mexico",
		"oceania/australia",
		"oceania/new_zealand",
		"oceania/papua_new_guinea",
		"south_america/argentina",
		"south_america/bolivia",
		"south_america/brazil",
		"south_america/chile",
		"south_america/colombia",
		"south_america/ecuador",
		"south_america/french_guiana",
		"south_america/guyana",
		"south_america/paraguay",
		"south_america/peru",
		"south_america/surname",
		"south_america/uruguay",
		"south_america/venezuela",
		"world/africa",
		"world/asia5",
		"world/australia1",
		"world/americas",
		"world/europe",
		"world/europe_without_russia",
		"world/north_america",
		"world/south_america",
		"world/world",
		"world/world_wo_gr",
		"usa/country/states",
		"usa/country/states_48",
		"usa/country/sub_regisons",
		"usa/regions/states/midwest",
		"usa/regions/states/northeast",
		"usa/regions/states/south",
		"usa/regions/states/west",
		"usa/regions/zip3/midwest",
		"usa/regions/zip3/northeast",
		"usa/regions/zip3/south",
		"usa/regions/zip3/west",
		"usa/states/counties/ak",
		"usa/states/counties/al",
		"usa/states/counties/ar",
		"usa/states/counties/az",
		"usa/states/counties/ca",
		"usa/states/counties/co",
		"usa/states/counties/ct",
		"usa/states/counties/de",
		"usa/states/counties/fl",
		"usa/states/counties/ga",
		"usa/states/counties/hi",
		"usa/states/counties/ia",
		"usa/states/counties/id",
		"usa/states/counties/il",
		"usa/states/counties/in",
		"usa/states/counties/ks",
		"usa/states/counties/ky",
		"usa/states/counties/la",
		"usa/states/counties/ma",
		"usa/states/counties/md",
		"usa/states/counties/me",
		"usa/states/counties/mi",
		"usa/states/counties/mn",
		"usa/states/counties/mo",
		"usa/states/counties/ms",
		"usa/states/counties/mt",
		"usa/states/counties/nc",
		"usa/states/counties/nd",
		"usa/states/counties/ne",
		"usa/states/counties/nh",
		"usa/states/counties/nj",
		"usa/states/counties/nm",
		"usa/states/counties/nv",
		"usa/states/counties/ny",
		"usa/states/counties/oh",
		"usa/states/counties/ok",
		"usa/states/counties/or",
		"usa/states/counties/pa",
		"usa/states/counties/ri",
		"usa/states/counties/sc",
		"usa/states/counties/sd",
		"usa/states/counties/tn",
		"usa/states/counties/tx",
		"usa/states/counties/ut",
		"usa/states/counties/va",
		"usa/states/counties/vt",
		"usa/states/counties/wa",
		"usa/states/counties/wi",
		"usa/states/counties/wv",
		"usa/states/counties/wy",
		"usa/states/zip3/ak",
		"usa/states/zip3/al",
		"usa/states/zip3/ar",
		"usa/states/zip3/az",
		"usa/states/zip3/ca",
		"usa/states/zip3/co",
		"usa/states/zip3/ct",
		"usa/states/zip3/de",
		"usa/states/zip3/fl",
		"usa/states/zip3/ga",
		"usa/states/zip3/hi",
		"usa/states/zip3/ia",
		"usa/states/zip3/id",
		"usa/states/zip3/il",
		"usa/states/zip3/in",
		"usa/states/zip3/ks",
		"usa/states/zip3/ky",
		"usa/states/zip3/la",
		"usa/states/zip3/ma",
		"usa/states/zip3/md",
		"usa/states/zip3/me",
		"usa/states/zip3/mi",
		"usa/states/zip3/mn",
		"usa/states/zip3/mo",
		"usa/states/zip3/ms",
		"usa/states/zip3/mt",
		"usa/states/zip3/nc",
		"usa/states/zip3/nd",
		"usa/states/zip3/ne",
		"usa/states/zip3/nh",
		"usa/states/zip3/nj",
		"usa/states/zip3/nm",
		"usa/states/zip3/nv",
		"usa/states/zip3/ny",
		"usa/states/zip3/oh",
		"usa/states/zip3/ok",
		"usa/states/zip3/or",
		"usa/states/zip3/pa",
		"usa/states/zip3/ri",
		"usa/states/zip3/sc",
		"usa/states/zip3/sd",
		"usa/states/zip3/tn",
		"usa/states/zip3/tx",
		"usa/states/zip3/ut",
		"usa/states/zip3/va",
		"usa/states/zip3/vt",
		"usa/states/zip3/wa",
		"usa/states/zip3/wi",
		"usa/states/zip3/wv",
		"usa/states/zip3/wy",
		"usa/states/zip3/dc",
		"usa/states/zip3/pr",
		"usa/sub_regions/counties/east_north_central",
		"usa/sub_regions/counties/east_soutth_central",
		"usa/sub_regions/counties/middle_atlantic",
		"usa/sub_regions/counties/mountain",
		"usa/sub_regions/counties/new_england",
		"usa/sub_regions/counties/pacific",
		"usa/sub_regions/counties/south_atlantic",
		"usa/sub_regions/counties/west_north_central",
		"usa/sub_regions/counties/west_south_central",
		"usa/sub_regions/states/east_north_central",
		"usa/sub_regions/states/east_soutth_central",
		"usa/sub_regions/states/middle_atlantic",
		"usa/sub_regions/states/mountain",
		"usa/sub_regions/states/new_england",
		"usa/sub_regions/states/pacific",
		"usa/sub_regions/states/south_atlantic",
		"usa/sub_regions/states/west_north_central",
		"usa/sub_regions/states/west_south_central",
		"usa/sub_regions/zip3/east_north_central",
		"usa/sub_regions/zip3/east_soutth_central",
		"usa/sub_regions/zip3/middle_atlantic",
		"usa/sub_regions/zip3/mountain",
		"usa/sub_regions/zip3/new_england",
		"usa/sub_regions/zip3/pacific",
		"usa/sub_regions/zip3/south_atlantic",
		"usa/sub_regions/zip3/west_north_central",
		"usa/sub_regions/zip3/west_south_central"
		};
	
	public static final HashMap<String, String> countryMapLocationMap;
	public static final HashMap<String, String> validMapNames; 
	
	static {
		validMapNames = new HashMap<String, String>();
		for (String n : _validMapNames) {
			validMapNames.put(n, n);
		}
		
		countryMapLocationMap = new HashMap<String, String>();
		countryMapLocationMap.put("DZ", "africa/algeria");
		countryMapLocationMap.put("ALGERIA", "africa/algeria");
		countryMapLocationMap.put("AO", "africa/angola");
		countryMapLocationMap.put("ANGOLA", "africa/angola");
		countryMapLocationMap.put("BJ", "africa/benin");
		countryMapLocationMap.put("BENIN", "africa/benin");
		countryMapLocationMap.put("BW", "africa/botswana");
		countryMapLocationMap.put("BOTSWANA", "africa/botswana");
		countryMapLocationMap.put("BF", "africa/burkina_faso");
		countryMapLocationMap.put("BURKINA FASO", "africa/burkina_faso");
		countryMapLocationMap.put("BI", "africa/burundi");
		countryMapLocationMap.put("BURUNDI", "africa/burundi");
		countryMapLocationMap.put("CM", "africa/cameroon");
		countryMapLocationMap.put("CAMEROON", "africa/cameroon");
		countryMapLocationMap.put("CF", "africa/central_african_republic");
		countryMapLocationMap.put("CENTRAL AFRICAN REPUBLIC", "africa/central_african_republic");
		countryMapLocationMap.put("TD", "africa/chad");
		countryMapLocationMap.put("CHAD", "africa/chad");
		countryMapLocationMap.put("CG", "africa/congo");
		countryMapLocationMap.put("CONGO", "africa/congo");
		countryMapLocationMap.put("DJ", "africa/djibouti");
		countryMapLocationMap.put("DJIBOUTI", "africa/djibouti");
		countryMapLocationMap.put("EG", "africa/egypt");
		countryMapLocationMap.put("EGYPT", "africa/egypt");
		countryMapLocationMap.put("GQ", "africa/equatorial_guinea");
		countryMapLocationMap.put("EQUATORIAL GUINEA", "africa/equatorial_guinea");
		countryMapLocationMap.put("ER", "africa/eritrea");
		countryMapLocationMap.put("ERITREA", "africa/eritrea");
		countryMapLocationMap.put("ET", "africa/ethiopia");
		countryMapLocationMap.put("ETHIOPIA", "africa/ethiopia");
		countryMapLocationMap.put("GA", "africa/gabon");
		countryMapLocationMap.put("GABON", "africa/gabon");
		countryMapLocationMap.put("GM", "africa/gambia");
		countryMapLocationMap.put("GAMBIA", "africa/gambia");
		countryMapLocationMap.put("GH", "africa/ghana");
		countryMapLocationMap.put("GHANA", "africa/ghana");
		countryMapLocationMap.put("GN", "africa/guinea");
		countryMapLocationMap.put("GUINEA", "africa/guinea");
		countryMapLocationMap.put("GW", "africa/guinea_bissau");
		countryMapLocationMap.put("GUINEA-BISSAU", "africa/guinea_bissau");
		countryMapLocationMap.put("CI", "africa/ivory_coast");
		countryMapLocationMap.put("COTE D'IVOIRE", "africa/ivory_coast");
		countryMapLocationMap.put("IVORY COAST", "africa/ivory_coast");
		countryMapLocationMap.put("KE", "africa/kenya");
		countryMapLocationMap.put("KENYA", "africa/kenya");
		countryMapLocationMap.put("LS", "africa/lesotho");
		countryMapLocationMap.put("LESOTHO", "africa/lesotho");
		countryMapLocationMap.put("LR", "africa/liberia");
		countryMapLocationMap.put("LIBERIA", "africa/liberia");
		countryMapLocationMap.put("MG", "africa/madagascar");
		countryMapLocationMap.put("MADAGASCAR", "africa/madagascar");
		countryMapLocationMap.put("MW", "africa/malawi");
		countryMapLocationMap.put("MALAWI", "africa/malawi");
		countryMapLocationMap.put("ML", "africa/mali");
		countryMapLocationMap.put("MALI", "africa/mali");
		countryMapLocationMap.put("MR", "africa/mauritania");
		countryMapLocationMap.put("MAURITANIA", "africa/mauritania");
		countryMapLocationMap.put("MA", "africa/morocco");
		countryMapLocationMap.put("MOROCCO", "africa/morocco");
		countryMapLocationMap.put("MZ", "africa/mozambique");
		countryMapLocationMap.put("MOZAMBIQUE", "africa/mozambique");
		countryMapLocationMap.put("NA", "africa/namibia");
		countryMapLocationMap.put("NAMIBIA", "africa/namibia");
		countryMapLocationMap.put("NE", "africa/niger");
		countryMapLocationMap.put("NIGER", "africa/niger");
		countryMapLocationMap.put("NG", "africa/nigeria");
		countryMapLocationMap.put("NIGERIA", "africa/nigeria");
		countryMapLocationMap.put("RW", "africa/rwanda");
		countryMapLocationMap.put("RWANDA", "africa/rwanda");
		countryMapLocationMap.put("SN", "africa/senegal");
		countryMapLocationMap.put("SENEGAL", "africa/senegal");
		countryMapLocationMap.put("SL", "africa/sierra_leone");
		countryMapLocationMap.put("SIERRA LEONE", "africa/sierra_leone");
		countryMapLocationMap.put("SO", "africa/somalia");
		countryMapLocationMap.put("SOMALIA", "africa/somalia");
		countryMapLocationMap.put("ZA", "africa/south_africa");
		countryMapLocationMap.put("SOUTH AFRICA", "africa/south_africa");
		countryMapLocationMap.put("SD", "africa/sudan");
		countryMapLocationMap.put("SUDAN", "africa/sudan");
		countryMapLocationMap.put("SZ", "africa/swaziland");
		countryMapLocationMap.put("SWAZILAND", "africa/swaziland");
		countryMapLocationMap.put("TZ", "africa/tanzania");
		countryMapLocationMap.put("TANZANIA", "africa/tanzania");
		countryMapLocationMap.put("UNITED REPUBLIC OF TANZANIA", "africa/tanzania");
		countryMapLocationMap.put("TG", "africa/togo");
		countryMapLocationMap.put("TOGO", "africa/togo");
		countryMapLocationMap.put("TN", "africa/tunisia");
		countryMapLocationMap.put("TUNISIA", "africa/tunisia");
		countryMapLocationMap.put("UG", "africa/uganda");
		countryMapLocationMap.put("UGANDA", "africa/uganda");
		countryMapLocationMap.put("CD", "africa/zaire");
		countryMapLocationMap.put("CONGO", "africa/zaire");
		countryMapLocationMap.put("THE DEMOCRATIC REPUBLIC OF THE CONGO", "africa/zaire");
		countryMapLocationMap.put("ZAIRE", "africa/zaire");
		countryMapLocationMap.put("ZM", "africa/zambia");
		countryMapLocationMap.put("ZAMBIA", "africa/zambia");
		// where's zimbabwe
	
		countryMapLocationMap.put("AF", "asia/afghanistan");
		countryMapLocationMap.put("AFGHANISTAN", "asia/afghanistan");
		countryMapLocationMap.put("BH", "asia/bahrain");
		countryMapLocationMap.put("BAHRAIN", "asia/bahrain");
		countryMapLocationMap.put("BD", "asia/bangladesh");
		countryMapLocationMap.put("BANGLADESH", "asia/bangladesh");
		countryMapLocationMap.put("BT", "asia/bhutan");
		countryMapLocationMap.put("BHUTAN", "asia/bhutan");
		countryMapLocationMap.put("KH", "asia/cambodia");
		countryMapLocationMap.put("CAMBODIA", "asia/cambodia");
		countryMapLocationMap.put("CN", "asia/china");
		countryMapLocationMap.put("CHINA", "asia/china");
		countryMapLocationMap.put("CY", "asia/cyprus");
		countryMapLocationMap.put("CYPRUS", "asia/cyprus");
		countryMapLocationMap.put("IN", "asia/india");
		countryMapLocationMap.put("INDIA", "asia/india");
		countryMapLocationMap.put("ID", "asia/indonesia");
		countryMapLocationMap.put("INDONESIA", "asia/indonesia");
		countryMapLocationMap.put("IR", "asia/iran");
		countryMapLocationMap.put("IRAN", "asia/iran");
		countryMapLocationMap.put("ISLAMIC RUPUBLIC OF IRAN", "asia/iran");
		countryMapLocationMap.put("IQ", "asia/iraq");
		countryMapLocationMap.put("IRAQ", "asia/iraq");
		countryMapLocationMap.put("IL", "asia/israel");
		countryMapLocationMap.put("ISRAEL", "asia/israel");
		countryMapLocationMap.put("JP", "asia/japan");
		countryMapLocationMap.put("JAPAN", "asia/japan");
		countryMapLocationMap.put("JO", "asia/jordan");
		countryMapLocationMap.put("JORDAN", "asia/jordan");
		countryMapLocationMap.put("KZ", "asia/kazakhstan");
		countryMapLocationMap.put("KAZAKHSTAN", "asia/kazakhstan");
		countryMapLocationMap.put("KW", "asia/kuwait");
		countryMapLocationMap.put("KUWAIT", "asia/kuwait");
		countryMapLocationMap.put("LA", "asia/laos");
		countryMapLocationMap.put("LAOS", "asia/laos");
		countryMapLocationMap.put("LAO PEOPLE'S DEMOCRATIC REPUBLIC", "asia/laos");
		countryMapLocationMap.put("LB", "asia/lebanon");
		countryMapLocationMap.put("LEBANON", "asia/lebanon");
		countryMapLocationMap.put("MY", "asia/malaysia");
		countryMapLocationMap.put("MALAYSIA", "asia/malaysia");
		countryMapLocationMap.put("MV", "asia/maldives");
		countryMapLocationMap.put("MALDIVES", "asia/maldives");
		countryMapLocationMap.put("MN", "asia/mongolia");
		countryMapLocationMap.put("MONGOLIA", "asia/mongolia");
		countryMapLocationMap.put("MM", "asia/myanmar");
		countryMapLocationMap.put("MYANMAR", "asia/myanmar");
		countryMapLocationMap.put("NP", "asia/nepal");
		countryMapLocationMap.put("NEPAL", "asia/nepal");
		countryMapLocationMap.put("KP", "asia/north_korea");
		countryMapLocationMap.put("NORTH KOREA", "asia/north_korea");
		countryMapLocationMap.put("DEMOCRATIC PEOPLE'S REPUBLIC OF KOREA", "asia/north_korea");
		countryMapLocationMap.put("PK", "asia/pakistan");
		countryMapLocationMap.put("PAKISTAN", "asia/pakistan");
		countryMapLocationMap.put("PH", "asia/philippines");
		countryMapLocationMap.put("PHILIPPINES", "asia/philippines");
		countryMapLocationMap.put("RU", "asia/russia");
		countryMapLocationMap.put("RUSSIA", "asia/russia");
		countryMapLocationMap.put("RUSSIAN FEDERATION", "asia/russia");
		countryMapLocationMap.put("SA", "asia/saudi_arabia");
		countryMapLocationMap.put("SAUDI ARABIA", "asia/saudi_arabia");
		countryMapLocationMap.put("KR", "asia/south_korea");
		countryMapLocationMap.put("SOUTH KOREA", "asia/south_korea");
		countryMapLocationMap.put("REPUBLIC OF KOREA", "asia/south_korea");
		countryMapLocationMap.put("SY", "asia/syria");
		countryMapLocationMap.put("SYRIA", "asia/syria");
		countryMapLocationMap.put("SYRIAN ARAB REPUBLIC", "asia/syria");
		countryMapLocationMap.put("TW", "asia/taiwan");
		countryMapLocationMap.put("TAIWAN", "asia/taiwan");
		countryMapLocationMap.put("TAIWAN, PROVINCE OF CHINA", "asia/taiwan");
		countryMapLocationMap.put("TH", "asia/thailand");
		countryMapLocationMap.put("THAILAND", "asia/thailand");
		countryMapLocationMap.put("TR", "asia/turkey");
		countryMapLocationMap.put("TURKEY", "asia/turkey");
		countryMapLocationMap.put("AE", "asia/united_arab_emirates");
		countryMapLocationMap.put("UNITED ARAB EMIRATES", "asia/united_arab_emirates");
		countryMapLocationMap.put("VN", "asia/vietnam");
		countryMapLocationMap.put("VIET NAM", "asia/vietnam");
		countryMapLocationMap.put("VIETNAM", "asia/vietnam");
		countryMapLocationMap.put("YE", "asia/yemen");
		countryMapLocationMap.put("YEMEN", "asia/yemen");
		
		countryMapLocationMap.put("AL", "europe/albania");
		countryMapLocationMap.put("ALBANIA", "europe/albania");
		countryMapLocationMap.put("AT", "europe/austria");
		countryMapLocationMap.put("AUSTRIA", "europe/austria");
		countryMapLocationMap.put("BE", "europe/belgium");
		countryMapLocationMap.put("BELGIUM", "europe/belgium");
		countryMapLocationMap.put("BG", "europe/bulgaria");
		countryMapLocationMap.put("BULGARIA", "europe/bulgaria");
		countryMapLocationMap.put("BY", "europe/byelarus");
		countryMapLocationMap.put("BELARUS", "europe/byelarus");
		countryMapLocationMap.put("CZ", "europe/czech_republic");
		countryMapLocationMap.put("CZECH REPUBLIC", "europe/czech_republic");
		countryMapLocationMap.put("DK", "europe/denmark");
		countryMapLocationMap.put("DENMARK", "europe/denmark");
		countryMapLocationMap.put("FI", "europe/finland");
		countryMapLocationMap.put("FINLAND", "europe/finland");
		countryMapLocationMap.put("FR", "europe/france");
		countryMapLocationMap.put("FRANCE", "europe/france");
		countryMapLocationMap.put("DE", "europe/germany");
		countryMapLocationMap.put("GERMANY", "europe/germany");
		countryMapLocationMap.put("GR", "europe/greece");
		countryMapLocationMap.put("GREECE", "europe/greece");
		countryMapLocationMap.put("HU", "europe/hungary");
		countryMapLocationMap.put("HUNGARY", "europe/hungary");
		countryMapLocationMap.put("IS", "europe/iceland");
		countryMapLocationMap.put("ICELAND", "europe/iceland");
		countryMapLocationMap.put("IE", "europe/ireland");
		countryMapLocationMap.put("IRELAND", "europe/ireland");
		countryMapLocationMap.put("IT", "europe/italy");
		countryMapLocationMap.put("ITALY", "europe/italy");
		countryMapLocationMap.put("NL", "europe/netherlands");
		countryMapLocationMap.put("NETHERLANDS", "europe/netherlands");
		countryMapLocationMap.put("NO", "europe/norway");
		countryMapLocationMap.put("NORWAY", "europe/norway");
		countryMapLocationMap.put("PL", "europe/poland");
		countryMapLocationMap.put("POLAND", "europe/poland");
		countryMapLocationMap.put("PT", "europe/portugal");
		countryMapLocationMap.put("PORTUGAL", "europe/portugal");
		countryMapLocationMap.put("RO", "europe/romania");
		countryMapLocationMap.put("ROMANIA", "europe/romania");
		countryMapLocationMap.put("ES", "europe/spain");
		countryMapLocationMap.put("SPAIN", "europe/spain");
		countryMapLocationMap.put("SE", "europe/sweden");
		countryMapLocationMap.put("SWEDEN", "europe/sweden");
		countryMapLocationMap.put("CH", "europe/switzerland");
		countryMapLocationMap.put("SWITZERLAND", "europe/switzerland");
		countryMapLocationMap.put("UA", "europe/ukraine");
		countryMapLocationMap.put("UKRAINE", "europe/ukraine");
		countryMapLocationMap.put("GB", "europe/united_kingdom");
		countryMapLocationMap.put("UNITED KINGDOM", "europe/united_kingdom");
		
		countryMapLocationMap.put("BZ", "north_america/belize");
		countryMapLocationMap.put("BELIZE", "north_america/belize");
		countryMapLocationMap.put("CA", "north_america/canada");
		countryMapLocationMap.put("CANADA", "north_america/canada");
		countryMapLocationMap.put("CR", "north_america/costa_rica");
		countryMapLocationMap.put("COSTA RICA", "north_america/costa_rica");
		countryMapLocationMap.put("CU", "north_america/cuba");
		countryMapLocationMap.put("CUBA", "north_america/cuba");
		countryMapLocationMap.put("SV", "north_america/el_salvador");
		countryMapLocationMap.put("EL SALVADOR", "north_america/el_salvador");
		countryMapLocationMap.put("GL", "north_america/greenland");
		countryMapLocationMap.put("GREENLAND", "north_america/greenland");
		countryMapLocationMap.put("GT", "north_america/guatemala");
		countryMapLocationMap.put("GUATEMALA", "north_america/guatemala");
		countryMapLocationMap.put("HT", "north_america/haiti");
		countryMapLocationMap.put("HAITI", "north_america/haiti");
		countryMapLocationMap.put("HN", "north_america/honduras");
		countryMapLocationMap.put("HONDURAS", "north_america/honduras");
		countryMapLocationMap.put("JM", "north_america/jamaica");
		countryMapLocationMap.put("JAMAICA", "north_america/jamaica");
		countryMapLocationMap.put("MX", "north_america/mexico");
		countryMapLocationMap.put("MEXICO", "north_america/mexico");
		countryMapLocationMap.put("NI", "north_america/nicaragua");
		countryMapLocationMap.put("NICARAGUA", "north_america/nicaragua");
		countryMapLocationMap.put("PA", "north_america/panama");
		countryMapLocationMap.put("PANAMA", "north_america/panama");

		countryMapLocationMap.put("AU", "oceania/australia");
		countryMapLocationMap.put("AUSTRALIA", "oceania/australia");
		countryMapLocationMap.put("NZ", "oceania/new_zealand");
		countryMapLocationMap.put("NEW ZEALAND", "oceania/new_zealand");
		countryMapLocationMap.put("PG", "oceania/papua_new_guinea");
		countryMapLocationMap.put("PAPUA NEW GUINEA", "oceania/papua_new_guinea");

		countryMapLocationMap.put("AR", "south_america/argentina");
		countryMapLocationMap.put("ARGENTINA", "south_america/argentina");
		countryMapLocationMap.put("BO", "south_america/bolivia");
		countryMapLocationMap.put("BOLIVIA", "south_america/bolivia");
		countryMapLocationMap.put("PLURINATIONAL STATE OF BOLIVIA", "south_america/bolivia");
		countryMapLocationMap.put("BR", "south_america/brazil");
		countryMapLocationMap.put("BRAZIL", "south_america/brazil");
		countryMapLocationMap.put("CL", "south_america/chile");
		countryMapLocationMap.put("CHILE", "south_america/chile");
		countryMapLocationMap.put("CO", "south_america/colombia");
		countryMapLocationMap.put("COLOMBIA", "south_america/colombia");
		countryMapLocationMap.put("EC", "south_america/ecuador");
		countryMapLocationMap.put("ECUADOR", "south_america/ecuador");
		countryMapLocationMap.put("GF", "south_america/french_guiana");
		countryMapLocationMap.put("FRENCH GUIANA", "south_america/french_guiana");
		countryMapLocationMap.put("GY", "south_america/guyana");
		countryMapLocationMap.put("GUYANA", "south_america/guyana");
		countryMapLocationMap.put("PY", "south_america/paraguay");
		countryMapLocationMap.put("PARAGUAY", "south_america/paraguay");
		countryMapLocationMap.put("PE", "south_america/peru");
		countryMapLocationMap.put("PERU", "south_america/peru");
		countryMapLocationMap.put("SR", "south_america/suriname");
		countryMapLocationMap.put("SURINAME", "south_america/suriname");
		countryMapLocationMap.put("UY", "south_america/uruguay");
		countryMapLocationMap.put("URUGUAY", "south_america/uruguay");
		countryMapLocationMap.put("VE", "south_america/venezuela");
		countryMapLocationMap.put("VENEZUELA", "south_america/venezuela");
		countryMapLocationMap.put("BOLIVARIAN REPUBLIC OF VENEZUELA", "south_america/venezuela");
		countryMapLocationMap.put("US", "usa/country/states");
		countryMapLocationMap.put("UNITED STATES", "usa/country/states");
		
	}
		

}
