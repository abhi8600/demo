/**
 * $Id: ChartOptions.java,v 1.110 2012-03-09 01:53:37 ricks Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.chart;

import java.awt.Color;
import java.awt.Font;
import java.awt.Stroke;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.jasperreports.engine.JRException;

import org.apache.log4j.Logger;
import org.jfree.ui.RectangleEdge;

import com.successmetricsinc.query.QueryColumn;
import com.successmetricsinc.query.QueryResultSet;
import com.successmetricsinc.util.Util;

/**
 * Class to encapsulate chart options
 * 
 * Options include (delimited by ;):
 * <ul>
 * <li>title - Chart title
 * <li>titlefont - Title font
 * <li>height - Chart height(in pixels)
 * <li>titlecolumn - Name of column to use to supply chart title
 * <li>threed - (true/false)
 * <li>vertical - (true/false)
 * <li>legend - (true/false)
 * <li>legendanchor - (top/bottom/left/right)
 * <li>legendfont - Font for legend
 * <li>domainlabels - (true/false)
 * <li>rangelabels - (true/false)
 * <li>domainlabel - Label for domain (overrides default)
 * <li>rangelabel - Label for range (overrides default)
 * <li>domaintickmarklabels - (true/false)
 * <li>rangetickmarklabels - (true/false)
 * <li>tickmarkformat - Format string for tick mark labels
 * <li>tickmarkformatcolumn - Column name used to supply format string for tickmark labels
 * <li>rangetickmarkformat - Format string for tick mark labels on the range
 * <li>color - Color to be used (can supply several - used in order to set series colors)
 * <li>rangebottomorleft - (true/false)
 * <li>valuemin - Minimum range value
 * <li>valuemax - Maximum range value
 * <li>gapwidth - Gap between bars (scale 0-1)
 * <li>labelfont - Font for labels
 * <li>catagorygapwidth - Gap between categories
 * <li>categorylabelangle - Angle of category labels
 * <li>logarithmic - (true/false)
 * <li>drawshapes - Whether to draw shapes at line chart vertices (true/false)
 * <li>linewidth - Width of lines to draw
 * <li>shapesize - Size of shapes to draw
 * <li>gradient - Whether to fill a bar/column with a gradient (null/3D/fade)
 * <li>scalerangetodata - (true/false) drillseries - (true/false)
 * <li>pielabelformat - Format string for pie section labels (if it starts with nz, then only non-zero labels will be
 * generated)
 * <li>percent - (true/false) (whether to show pie values or percent)
 * <li>interiorgap - Gap between edge and chart (scale 0-1)
 * <li>startangle - Angle to start pie
 * <li>showpielabelkey - Show key for pie chart
 * <li>quadrant - Name of quadrant for scatter plots. Quadrants are added in order: upper left, upper right, lower
 * left, lower right
 * <li>swapseriesandcategories - Swap what is a series and what is a category (for category data sets)
 * <li>series - Column name of series to use (can supply more than one)
 * <li>category - Column name of category to use (can supply more than one)
 * </ul>
 * 
 * @author bpeters
 * 
 */
public class ChartOptions implements Serializable
{
	public static final String DEFAULT_DIMENSION_FORMAT = "#";
	private static final String DEFAULT_MAP_PROJECTION = "Equirectangular";
	private static final String USSTATE_WITH_ZIP3_MAP = "USStateWithZip3Map";
	private static final String USSTATE_WITH_COUNTIES_MAP = "USStateWithCountiesMap";
	private static final String COUNTRY_MAP = "CountryMap";
	private static final String WORLD_MAP = "WorldMap";
	private static final String CONTINENT_MAP = "ContinentMap";
	private static final String DRILL_ON = "drillOn";
	private static final String ADD_FILTER = "addFilter";
	private static final long serialVersionUID = 1L;
	public static final String DEFAULT_FONT_NAME = "Arial";
	public static final int DEFAULT_FONT_STYLE = Font.PLAIN;
	public static final int DEFAULT_FONT_SIZE = 8;
	public static final String DEFAULT_FONT_STRING = DEFAULT_FONT_NAME + "-PLAIN-" + DEFAULT_FONT_SIZE;
	public static final String DEFAULT_LEGEND_FONT_STRING = DEFAULT_FONT_NAME + "-PLAIN-" + String.valueOf(DEFAULT_FONT_SIZE-2);
	public static final int DEFAULT_CHART_HEIGHT = 250;
	public static final int DEFAULT_CHART_WIDTH = 300;
	public static final String CHART_TYPE = "CHART_TYPE";

	public enum ChartType
	{
		None, Bar, Column, Line, BarLine, Pie, Doughnut, StackedBar, StackedColumn, Area, StackedArea, Scatterplot, BubbleChart, ContourPlot, SpiderWebPlot, MeterPlot,
		HeatMap, StackedColumnLine, Funnel, Pyramid, TreeMap, Map, Radar, PercentStackedBar, PercentStackedColumn
	};
	public ChartType type = ChartType.None;
	public String title = null;
	public String titleColumn = null;
	public int height = DEFAULT_CHART_HEIGHT;
	public int width = DEFAULT_CHART_WIDTH;
	
	// default 0 options
	public Double valueMin;
	public Double valueMax;
	public double valueSecondMin = 0;
	public double valueSecondMax = 0;
	public double lowerMargin = 0;
	public double upperMargin = 0;
	public double categoryLabelAngle = 0;
	public double chartBackgroundOpacity = 0;
	public double plotBackgroundOpacity = 0;
	public int leftOutsideMargin = 0;
	public int rightOutsideMargin = 0;
	public int topOutsideMargin = 0;
	public int bottomOutsideMargin = 0;
	
	// default false options
	public boolean threeD = false;
	public boolean vertical = false;
	public boolean legend = false;
	public boolean logarithmic = false;
	public boolean percent = false;	
	public boolean swapSeriesAndCategories = false;
	public boolean shareAxis = false;
	public boolean jfreechartsemantics = false;
	public boolean stacked = false;
	public boolean noborder = true;
	public boolean displayValues = false;

	// default true options
	public boolean imageMap = true;
	public boolean domainLabels = true;
	public boolean rangeLabels = true;
	public boolean domainTickMarkLabels = true;
	public boolean rangeTickMarkLabels = true;
	public boolean domainGroupLabels = true;
	public boolean tickmarksVisible = true;
	public boolean showMajorGrid = false;
	public boolean showMinorGrid = false;
	public boolean showMajorTickmarks = false;
	public boolean showMinorTickmarks = false;
	boolean rangeBottomOrLeft = true;
	public boolean drawShapes = true;
	public boolean scaleRangeToData = true;
	public boolean drillSeries = true;
	public boolean toolTips = true;
	public boolean showPieLabelKey = true;
	public boolean colorNegative = true;

	public String domainLabel = null;
	public String rangeLabel = null;
	public String secondAxisRangeLabel = null;
	public String tickmarkFormat = null;
	public String tickmarkFormatColumn = null;
	public String rangeTickmarkFormat = null;
	public List<Color> chartColors = null;
	public Stroke labelOutline = null;
	public Stroke stroke = null;
	public String gradient = null;
	public String colorColumn = null;
	public List<String> quadrantNames;
	public List<String> series = null;
	public List<String> series2 = null;
	public List<String> categories = null;
	public List<String> addFilters = null;
	public List<String> drillOn = null;
	public String drillToDashboard = null; // not actually set or processed here

	public String chartBackgroundColor = "FFFFFF";
	public String plotBackgroundColor = "FFFFFF";
	public String borderColor = "CCCCCC";

	public RectangleEdge legendAnchor = RectangleEdge.BOTTOM;
	public String imageMapName = "name";
	public String imageMapPrefix = "prefix";
	public String pieLabelFormat = "####"; // "##%";
	
	public String thresholdType = "Quantiles";  // EqualSteps, Quantiles, AbsoluteDeviation
	public String customThresholds = "";
	public String footerLabel = null;
	
	public double meterMin = 0, meterCritical = 50, meterNormal = 75, meterMax = 100;
	public int numSegments = 3;
	public int lineWidth = 2;
	public double interiorGap = 0.15;
	public double startAngle = 90;
	public int shapeSize = 4;
	public double gapWidth = 0.05;
	public double categoryGapWidth = -1;
	public int zoomRangeMin = 1;
	public int zoomRangeMax = 10;
	public int zoomFactor = 1;
	public String latitude = null;
	public String longitude = null;
	public String geoMapTypeKml = null;
	
	public enum GeoMapType {
		WorldMap, ContinentMap, CountryMap, USStateWithCountiesMap, USStateWithZip3Map, Other, Kml
	}
	public GeoMapType geoMapType = GeoMapType.WorldMap;
	public String subMapName;
	
	// which column in the .amap file should we map the category column to
	public String mapColumnName = "REGION_NAME";
	// which column in the qrs contains the name of the map (country or state).
	public String mapNameColumn = null;
	public boolean showGeoMapLabel = true;
	public boolean hideCoordinateGrid = false;
		
	public boolean enableMapZooming = true;
	public boolean enableMapScrolling = true;
	public boolean enableColorSwatch = true;
	public String mapProjection = DEFAULT_MAP_PROJECTION;

	// Fonts
	private String titleFontStr = DEFAULT_FONT_STRING;
	private String labelFontStr = DEFAULT_FONT_STRING;
	private String legendFontStr = DEFAULT_LEGEND_FONT_STRING;
	private transient Font titleFont = null;
	private transient Font labelFont = null;
	private transient Font legendFont = null;
	
	private double majorInterval = 0.0;
	private double minorInterval = 0.0;
	private double secondAxisMajorInterval = 0.0;
	private double secondAxisMinorInterval = 0.0;
	
	public transient Map<String, String> seriesGroupBySeries = new HashMap<String, String>();
	public transient Map<String, String> seriesNameBySeries = new HashMap<String, String>();
	public transient List<String> seriesGroups = new ArrayList<String>();
	public transient Map<String, String> columnNameReplacementsInURL = new HashMap<String, String>();
	public transient Map<String, String> columnValueReplacementsInURL = new HashMap<String, String>();
	public transient Map<String, String> columnValueStrReplacementsInURL = new HashMap<String, String>();
	private Map<String, String> formatMap = new HashMap<String, String>();
	public Map<String, String> aggregateMap = new HashMap<String, String>();
	
	private static Logger logger = Logger.getLogger(ChartOptions.class);

	public ChartOptions()
	{
		init();
	}

	public ChartOptions(String options)
	{
		init();
		setOptions(options);
	}

	public ChartOptions(QueryResultSet qrs)
	{
		init();
		setupNew(qrs);
	}

	public void init()
	{
		seriesGroupBySeries = new HashMap<String, String>();
		seriesNameBySeries = new HashMap<String, String>();
		seriesGroups = new ArrayList<String>();
		columnNameReplacementsInURL = new HashMap<String, String>();
		columnValueReplacementsInURL = new HashMap<String, String>();
		columnValueStrReplacementsInURL = new HashMap<String, String>();
		formatMap = new HashMap<String, String>();
		addFilters = new ArrayList<String>();
		drillOn = new ArrayList<String>();
	}

	private void setupNew(QueryResultSet qrs)
	{
		series = new ArrayList<String>();
		series2 = new ArrayList<String>();
		categories = new ArrayList<String>();
		String[] names = qrs.getDisplayNames(); // use the display names for the series and categories
		for (int i = 0; i < names.length; i++)
		{
			int type = qrs.getColumnTypes()[i];
			if (names[i] == null)
			{
				continue;
			}
			String name = Util.removeSystemGeneratedFieldNumber(names[i]);
			if ((type == QueryColumn.DIMENSION) && (categories.size() == 0))
			{
				categories.add(name);
			} else if ((type == QueryColumn.MEASURE) && (series.size() == 0))
			{
				series.add(name);
			}
		}
		if (series.size() > 1)
			legend = true;
	}

	/**
	 * Validate the current series and categories in the chart options. If one has been deleted, pick another if needed
	 * 
	 * @param qrs
	 */
	public void validateSeriesAndCategories(QueryResultSet qrs)
	{
		if (qrs == null)
			return;
		if (qrs.getColumnNames() == null)
			return;
		List<String> colNames = Chart.getColumnNames(qrs); // use the display names for the series and categories
		boolean newList = false;
		if ((series == null || series.size() == 0) && (categories == null || categories.size() == 0))
		{
			setupNew(qrs);
			return;
		}
		for (String s : series)
		{
			int equalToIndex = s.indexOf('=');
			if (equalToIndex > 0)
			{
				s = s.substring(equalToIndex + 1, s.length());
			}
			if (!(colNames.indexOf(s) >= 0))
			{
				newList = true;
				break;
			}
		}
		if (series2 != null)
			for (String s : series2)
			{
				int equalToIndex = s.indexOf('=');
				if (equalToIndex > 0)
				{
					s = s.substring(equalToIndex + 1, s.length());
				}
				if (!(colNames.indexOf(s) >= 0))
				{
					newList = true;
					break;
				}
			}
		if (newList)
		{
			List<String> newSeries = new ArrayList<String>();
			for (String s : series)
			{
				if (colNames.indexOf(s) >= 0)
				{
					newSeries.add(s);
				}
			}
			series = newSeries;
			series2 = new ArrayList<String>();
			if (series.size() == 0)
			{
				String[] names = qrs.getDisplayNames();
				int[] types = qrs.getColumnTypes();
				for (int i = 0; i < names.length; i++)
				{
					int type = types[i];
					if ((type == QueryColumn.MEASURE) && (series.size() == 0))
					{
						series.add(names[i]);
						break;
					}
				}
			}
		}
		newList = false;
		for (String s : categories)
		{
			if (!(colNames.indexOf(s) >= 0))
			{
				newList = true;
				break;
			}
		}
		if (newList)
		{
			List<String> newCategories = new ArrayList<String>();
			for (String s : categories)
			{
				if (colNames.indexOf(s) >= 0)
				{
					newCategories.add(s);
				}
			}
			categories = newCategories;
			if (categories.size() == 0)
			{
				String[] names = qrs.getDisplayNames();
				int[] types = qrs.getColumnTypes();
				for (int i = 0; i < names.length; i++)
				{
					int type = types[i];
					if ((type == QueryColumn.DIMENSION) && (categories.size() == 0))
					{
						categories.add(names[i]);
						break;
					}
				}
			}
		}
	}

	private String escapeSemi(String str)
	{
		return escape(str, ';');
	}

	/**
	 * replace character with % followed by it's hex equivalent - do not use on hex characters less then 0x10
	 * 
	 * @param str
	 * @return
	 */
	private String escape(String str, char chr)
	{
		if (str == null)
			return str;
		String val = '%' + Integer.toHexString((int) chr).toUpperCase();
		return str.replace(String.valueOf(chr), val);
	}

	private List<String> getOptions()
	{
		List<String> plist = new ArrayList<String>();
		plist.add("chartType=" + this.getTypeString());
		if (height == 0)
			height = 250;
		plist.add("height=" + height);
		plist.add("width=" + width);
		if (legend)
		{
			if (legendAnchor == RectangleEdge.BOTTOM)
				plist.add("legendanchor=bottom");
			else if (legendAnchor == RectangleEdge.TOP)
				plist.add("legendanchor=top");
			else if (legendAnchor == RectangleEdge.LEFT)
				plist.add("legendanchor=left");
			else if (legendAnchor == RectangleEdge.RIGHT)
				plist.add("legendanchor=right");
		}
		if (this.type == ChartType.MeterPlot)
		{
			plist.add("meterMin=" + meterMin);
			plist.add("meterCritical=" + meterCritical);
			plist.add("meterNormal=" + meterNormal);
			plist.add("meterMax=" + meterMax);
		}
		plist.add("noborder=" + noborder);
		plist.add("gapWidth=" + gapWidth);
		plist.add("categoryGapWidth=" + categoryGapWidth);
		plist.add("zoomRangeMin=" + zoomRangeMin);
		plist.add("zoomRangeMax=" + zoomRangeMax);
		plist.add("zoomFactor=" + zoomFactor);
		plist.add("latitude=" + latitude);
		plist.add("longitude=" + longitude);
		plist.add("shapeSize=" + shapeSize);
		plist.add("lineWidth=" + lineWidth);
		plist.add("interiorGap=" + interiorGap);
		plist.add("startAngle=" + startAngle);
		if (this.type == ChartType.HeatMap || this.type == ChartType.TreeMap || this.type == ChartType.Map)
			plist.add("numSegments=" + numSegments);

		// default 0 values
		if (valueMin != null && valueMin != 0)
			plist.add("valueMin=" + valueMin);
		if (valueMax != null && valueMax != 0)
			plist.add("valueMax=" + valueMax);
		if (valueSecondMin != 0)
			plist.add("valueSecondMin=" + valueSecondMin);
		if (valueSecondMax != 0)
			plist.add("valueSecondMax=" + valueSecondMax);		
		if (lowerMargin != 0)
			plist.add("lowerMargin=" + lowerMargin);
		if (upperMargin != 0)
			plist.add("upperMargin=" + upperMargin);
		if (categoryLabelAngle != 0)
			plist.add("categorylabelangle=" + categoryLabelAngle);
		if (chartBackgroundOpacity != 0)
			plist.add("chartBackgroundOpacity=" + chartBackgroundOpacity);
		if (plotBackgroundOpacity != 0)
			plist.add("plotBackgroundOpacity=" + plotBackgroundOpacity);
		if (leftOutsideMargin != 0)
			plist.add("leftOutsideMargin=" + leftOutsideMargin);
		if (rightOutsideMargin != 0)
			plist.add("rightOutsideMargin=" + rightOutsideMargin);
		if (leftOutsideMargin != 0)
			plist.add("topOutsideMargin=" + topOutsideMargin);
		if (leftOutsideMargin != 0)
			plist.add("bottomOutsideMargin=" + bottomOutsideMargin);

		// default null values
		if (chartColors != null)
			for (Color c : chartColors)
				plist.add("color=" + c.getRGB());
		if (this.type == ChartType.Scatterplot && quadrantNames != null)
			for (String s : quadrantNames)
				plist.add("quadrant=" + s);
		if (title != null)
			plist.add("title=" + title);
		if (titleColumn != null)
			plist.add("titleColumn=" + titleColumn);
		if (titleFontStr != null)
			plist.add("titlefont=" + titleFontStr);
		if (domainLabel != null)
			plist.add("domainLabel=" + domainLabel);
		if (rangeLabel != null)
			plist.add("rangeLabel=" + rangeLabel);
		if (secondAxisRangeLabel != null && Util.hasNonWhiteSpaceCharacters(secondAxisRangeLabel))
			plist.add("secondAxisRangeLabel=" + secondAxisRangeLabel);
		if (!domainTickMarkLabels)
			plist.add("domainTickMarkLabels=" + domainTickMarkLabels);
		if (tickmarkFormat != null)
			plist.add("tickmarkFormat=" + tickmarkFormat);
		if (rangeTickmarkFormat != null)
			plist.add("rangeTickmarkFormat=" + rangeTickmarkFormat);
		if (tickmarkFormatColumn != null)
			plist.add("tickmarkFormatColumn=" + tickmarkFormatColumn);
		if (gradient != null)
			plist.add("gradient=" + gradient);
		if (colorColumn != null)
			plist.add("colorColumn=" + colorColumn);
		if (chartBackgroundColor != null)
			plist.add("chartBackgroundColor=" + chartBackgroundColor);
		if (plotBackgroundColor != null)
			plist.add("plotBackgroundColor=" + plotBackgroundColor);
		if (borderColor != null)
			plist.add("borderColor=" + borderColor);
		if (this.type == ChartType.Pie && pieLabelFormat != null)
			plist.add("pieLabelFormat=" + pieLabelFormat);
		if (thresholdType != null){
			plist.add("thresholdType=" + thresholdType);
			if(thresholdType.equalsIgnoreCase("Custom")){
				plist.add("customThresholds=" + customThresholds);
			}
		}
		if (footerLabel != null) {
			plist.add("footerLabel=" + footerLabel);
		}
		if (labelFontStr != null)
			plist.add("labelFont=" + labelFontStr);
		if (legend && legendFontStr != null)
			plist.add("legendFont=" + legendFontStr);
		if (mapColumnName != null)
			plist.add("mapColumnName=" + mapColumnName);
		if (mapNameColumn != null)
			plist.add("mapNameColumn=" + mapNameColumn);
		if (mapProjection != null)
			plist.add("mapProjection=" + mapProjection);

		// default true values (must include these since the UI does not know that they default to on)
		plist.add("domainLabels=" + domainLabels);
		plist.add("rangeLabels=" + rangeLabels);
		plist.add("rangeTickMarkLabels=" + rangeTickMarkLabels);
		plist.add("domainGroupLabels=" + domainGroupLabels);
		plist.add("geoMapType=" + getGeoMapTypeString());
		plist.add("subMapName=" + subMapName);
		plist.add("enableMapZooming=" + enableMapZooming);
		plist.add("enableMapScrolling=" + enableMapScrolling);
		plist.add("enableColorSwatch=" + enableColorSwatch);
		plist.add("showGeoMapLabel=" + showGeoMapLabel);
		plist.add("hideCoordinateGrid=" + hideCoordinateGrid);
		
		plist.add("tickmarksVisible=" + tickmarksVisible);
		plist.add("showMajorGrid=" + showMajorGrid);
		plist.add("showMinorGrid=" + showMinorGrid);
		plist.add("showMajorTickmarks=" + showMajorTickmarks);
		plist.add("showMinorTickmarks=" + showMinorTickmarks);

		if (!toolTips)
			plist.add("toolTips=" + toolTips);
		if (!rangeBottomOrLeft)
			plist.add("rangeBottomOrLeft=" + rangeBottomOrLeft);
		if (!drillSeries)
			plist.add("drillSeries=" + drillSeries);
		if (!colorNegative)
			plist.add("colorNegative=" + colorNegative);
		if (!drawShapes)
			plist.add("drawShapes=" + drawShapes);
		if (!scaleRangeToData)
			plist.add("scaleRangeToData=" + scaleRangeToData);
		if (this.type == ChartType.Pie && !showPieLabelKey)
			plist.add("showPieLabelKey=" + showPieLabelKey);

		// default false values
		if (threeD)
			plist.add("threeD=" + threeD);
		if (vertical)
			plist.add("vertical=" + vertical);
		if (legend)
			plist.add("legend=" + legend);
		if (logarithmic)
			plist.add("logarithmic=" + logarithmic);		
		if (percent)
			plist.add("percent=" + percent);
		if (jfreechartsemantics)
			plist.add("jfreechartsemantics=" + jfreechartsemantics);
		if (stacked)
			plist.add("stacked=" + stacked);
		if (shareAxis)
			plist.add("shareAxis=" + shareAxis);
		if (swapSeriesAndCategories)
			plist.add("swapseriesandcategories=" + swapSeriesAndCategories);	
		if (displayValues)
			plist.add("displayValues=" + displayValues);
		if (majorInterval > 0) 
			plist.add("majorInterval=" + majorInterval);
		if (minorInterval > 0)
			plist.add("minorInterval=" + minorInterval);
		if (secondAxisMajorInterval > 0)
			plist.add("secondAxisMajorInterval=" + secondAxisMajorInterval);
		if (secondAxisMinorInterval > 0)
			plist.add("secondAxisMinorInterval=" + secondAxisMinorInterval);
		
		return (plist);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		return getToString(null);
	}
	public String getToString(Map<String, Object> ps)
	{
		List<String> plist = getOptions();
		StringBuilder sb = new StringBuilder();
		if (series != null)
			for (String s : series)
			{
				StringBuilder seriesBuf = new StringBuilder();
				String seriesGroup = (seriesGroupBySeries == null) ? null : seriesGroupBySeries.get(s);
				String seriesName = (seriesNameBySeries == null) ? null : seriesNameBySeries.get(s);
				if (seriesGroup != null)
				{
					seriesBuf.append(seriesGroup);
					if (seriesName != null)
					{
						seriesBuf.append("|");
						seriesBuf.append(seriesName);
					}
					seriesBuf.append("=");
				}
				seriesBuf.append(s);
				plist.add("series=" + seriesBuf.toString());
			}
		if (series2 != null)
			for (String s : series2)
			{
				plist.add("series2=" + s);
			}
		if (categories != null)
			for (String s : categories)
			{
				plist.add("category=" + s);
			}
		if (formatMap != null)
			for (String s : formatMap.keySet())
			{
				plist.add("formatMap=" + s + '=' + formatMap.get(s));
			}
		if (aggregateMap != null) {
			for (String s: aggregateMap.keySet()) {
				plist.add("aggregateMap=" + s + '=' + aggregateMap.get(s));
			}
		}
		if (this.addFilters != null) {
			for (String s : addFilters) {
				plist.add(ChartOptions.ADD_FILTER + "=" + s);
			}
		}
		if (this.drillOn != null) {
			for (String s : drillOn) {
				plist.add(ChartOptions.DRILL_ON + "=" + s);
			}
		}
		if (columnValueReplacementsInURL != null && columnValueReplacementsInURL.size() > 0)
		{
			Set<String> replacementKeyList = new HashSet<String>();
			for (Iterator<String> it = columnValueReplacementsInURL.keySet().iterator(); it.hasNext();)
			{
				replacementKeyList.add(it.next());
			}
			for (String s : replacementKeyList)
			{
				plist.add("replacecolvalueinurl=" + s + '=' + columnValueReplacementsInURL.get(s));
			}
		}
		if (columnValueStrReplacementsInURL != null && columnValueStrReplacementsInURL.size() > 0)
		{
			Set<String> replacementKeyList = new HashSet<String>();
			for (Iterator<String> it = columnValueStrReplacementsInURL.keySet().iterator(); it.hasNext();)
			{
				replacementKeyList.add(it.next());
			}
			for (String s : replacementKeyList)
			{
				plist.add("replacecolvaluestrinurl=" + s + "=" + columnValueStrReplacementsInURL.get(s));
			}
		}
		if (columnNameReplacementsInURL != null && columnNameReplacementsInURL.size() > 0)
		{
			Set<String> replacementKeyList = new HashSet<String>();
			for (Iterator<String> it = columnNameReplacementsInURL.keySet().iterator(); it.hasNext();)
			{
				replacementKeyList.add(it.next());
			}
			for (String s : replacementKeyList)
			{
				plist.add("replacecolnameinurl=" + s + "=" + columnNameReplacementsInURL.get(s));
			}
		}
		for (int i = 0; i < plist.size(); i++)
		{
			if (i > 0)
				sb.append(';');
			String newVal = escapeSemi(plist.get(i));
			sb.append(newVal);
		}
		return (sb.toString());
	}

	/**
	 * Set chart options based on a parameter string (delimted by ;)
	 * 
	 * @param pstring
	 */
	public void setOptions(String pstring)
	{
		List<String> slist = new ArrayList<String>();
		if (seriesGroups.isEmpty())
		{
			seriesGroups.add("NO_SERIES_GROUP");
		}
		StringBuilder sb = new StringBuilder();
		if (pstring == null) return;
		for (int i = 0; i < pstring.length(); i++)
		{
			// semicolon has been replaced with it's ascii hex value %3B
			if ((i < pstring.length() - 3) && pstring.substring(i, i + 3).equals("%3B"))
			{
				i = i + 2;
				sb.append(';');
			} else if (pstring.charAt(i) == ';')
			{
				slist.add(sb.toString());
				sb = new StringBuilder();
			} else
			{
				sb.append(pstring.charAt(i));
			}
		}
		slist.add(sb.toString());
		if (series == null)
			series = new ArrayList<String>();
		if (series2 == null)
			series2 = new ArrayList<String>();
		if (categories == null)
			categories = new ArrayList<String>();
		if (formatMap == null)
			formatMap = new HashMap<String, String>();
		if (aggregateMap == null)
			aggregateMap = new HashMap<String, String>();
		if (addFilters == null)
			addFilters = new ArrayList<String>();
		if (drillOn == null)
			drillOn = new ArrayList<String>();
		for (String s : slist)
		{
			int i = s.indexOf('=');
			if ((i > 0) && (i < s.length() - 1))
			{
				String name = s.substring(0, i).toLowerCase();
				String val = s.substring(i + 1);
				if (name.equals("title"))
					title = val;
				else if (name.equals("charttype"))
					this.decodeTypeString(val);
				else if (name.equals("height"))
					height = Integer.parseInt(val);
				else if (name.equals("width"))
				{
					width = Integer.parseInt(val);
				} else if (name.equals("titlefont"))
				{
					titleFontStr = val;
					titleFont = Font.decode(val);
				} else if (name.equals("titlecolumn"))
					titleColumn = val;
				else if (name.equals("threed"))
					threeD = Boolean.parseBoolean(val);
				else if (name.equals("vertical"))
					vertical = Boolean.parseBoolean(val);
				else if (name.equals("legend"))
					legend = Boolean.parseBoolean(val);
				else if (name.equals("legendanchor"))
				{
					if (val.equals("bottom"))
						legendAnchor = RectangleEdge.BOTTOM;
					else if (val.equals("top"))
						legendAnchor = RectangleEdge.TOP;
					else if (val.equals("left"))
						legendAnchor = RectangleEdge.LEFT;
					else if (val.equals("right"))
						legendAnchor = RectangleEdge.RIGHT;
				} else if (name.equals("domainlabels"))
					domainLabels = Boolean.parseBoolean(val);
				else if (name.equals("rangelabels"))
					rangeLabels = Boolean.parseBoolean(val);
				else if (name.equals("secondaxisrangelabel"))
					secondAxisRangeLabel = val;
				else if (name.equals("domainlabel"))
					domainLabel = val;
				else if (name.equals("rangelabel"))
					rangeLabel = val;
				else if (name.equals("tooltips"))
					toolTips = Boolean.parseBoolean(val);
				else if (name.equals("domaintickmarklabels"))
					domainTickMarkLabels = Boolean.parseBoolean(val);
				else if (name.equals("rangetickmarklabels"))
					rangeTickMarkLabels = Boolean.parseBoolean(val);
				else if (name.equals("domaingrouplabels"))
					domainGroupLabels = Boolean.parseBoolean(val);
				else if (name.equals("geomaptype"))
					decodeGeoMapTypeString(val);
				else if (name.equals("submapname"))
					subMapName = val;
				else if (name.equals("mapcolumnname"))
					mapColumnName = val;
				else if (name.equals("mapnamecolumn"))
					mapNameColumn = val;
				else if (name.equals("mapprojection"))
					mapProjection = val;
				else if (name.equals("enablemapzooming"))
					enableMapZooming = Boolean.parseBoolean(val);
				else if (name.equals("enablemapscrolling"))
					enableMapScrolling = Boolean.parseBoolean(val);
				else if (name.equals("enablecolorswatch"))
					enableColorSwatch = Boolean.parseBoolean(val);
				else if (name.equals("showgeomaplabel"))
					showGeoMapLabel = Boolean.parseBoolean(val);
				else if (name.equals("hidecoordinategrid"))
					hideCoordinateGrid = Boolean.parseBoolean(val);
				else if (name.equals("tickmarksvisible"))
					tickmarksVisible = Boolean.parseBoolean(val);
				else if (name.equals("showmajorgrid"))
					showMajorGrid = Boolean.parseBoolean(val);
				else if (name.equals("showminorgrid"))
					showMinorGrid = Boolean.parseBoolean(val);
				else if (name.equals("showmajortickmarks"))
					showMajorTickmarks = Boolean.parseBoolean(val);
				else if (name.equals("showminortickmarks"))
					showMinorTickmarks = Boolean.parseBoolean(val);
				else if (name.equals("tickmarkformat"))
					tickmarkFormat = val;
				else if (name.equals("rangetickmarkformat"))
					rangeTickmarkFormat = val;
				else if (name.equals("tickmarkformatcolumn"))
					tickmarkFormatColumn = val;
				else if (name.equals("gradient"))
					gradient = val;
				else if (name.equals("chartbackgroundcolor"))
					chartBackgroundColor = val;
				else if (name.equals("chartbackgroundopacity"))
					chartBackgroundOpacity = Double.parseDouble(val);				
				else if (name.equals("plotbackgroundcolor"))
					plotBackgroundColor = val;
				else if (name.equals("bordercolor"))
					borderColor = val;				
				else if (name.equals("plotbackgroundopacity"))
					plotBackgroundOpacity = Double.parseDouble(val);
				else if (name.equals("color"))
				{
					if (chartColors == null)
						chartColors = new ArrayList<Color>();
					String[] splitval = val.replace("(", "").replace(")", "").split(",");
					if (splitval.length == 3)
					{
						int red = Integer.parseInt(splitval[0]);
						int green = Integer.parseInt(splitval[1]);
						int blue = Integer.parseInt(splitval[2]);
						chartColors.add(new Color(red, green, blue));
					} else
						chartColors.add(new Color(Integer.parseInt(val)));
				} else if (name.equals("quadrant"))
				{
					if (quadrantNames == null)
						quadrantNames = new ArrayList<String>();
					quadrantNames.add(val);
				} else if (name.equals("rangebottomorleft"))
					rangeBottomOrLeft = Boolean.parseBoolean(val);
				else if (name.equals("valuemin"))
					valueMin = (val == null ? null : Double.parseDouble(val));
				else if (name.equals("valuemax"))
					valueMax = (val == null ? null : Double.parseDouble(val));
				else if (name.equals("valuesecondmin"))
					valueSecondMin = Double.parseDouble(val);
				else if (name.equals("valuesecondmax"))
					valueSecondMax = Double.parseDouble(val);				
				else if (name.equals("metermin"))
					meterMin = Double.parseDouble(val);
				else if (name.equals("metercritical"))
					meterCritical = Double.parseDouble(val);
				else if (name.equals("meternormal"))
					meterNormal = Double.parseDouble(val);
				else if (name.equals("metermax"))
					meterMax = Double.parseDouble(val);
				else if (name.equals("gapwidth"))
					gapWidth = Double.parseDouble(val);
				else if (name.equals("labelfont"))
				{
					labelFontStr = val;
					labelFont = Font.decode(val);
				} else if (name.equals("legendfont"))
				{
					legendFontStr = val;
					legendFont = Font.decode(val);
				} else if (name.equals("categorygapwidth"))
					categoryGapWidth = Double.parseDouble(val);
				else if (name.equals("zoomrangemin"))
					zoomRangeMin = Double.valueOf(val).intValue();
				else if (name.equals("zoomrangemax"))
					zoomRangeMax = Double.valueOf(val).intValue();
				else if (name.equals("zoomfactor"))
					zoomFactor = Double.valueOf(val).intValue();
				else if (name.equals("longitude"))
					longitude = val;
				else if (name.equals("latitude"))
					latitude = val;
				else if (name.equals("logarithmic"))
					logarithmic = Boolean.parseBoolean(val);
				else if (name.equals("drawshapes"))
					drawShapes = Boolean.parseBoolean(val);
				else if (name.equals("shapesize"))
					shapeSize = Double.valueOf(val).intValue();
				else if (name.equals("linewidth"))
					lineWidth = Double.valueOf(val).intValue();
				else if (name.equals("scalerangetodata"))
					scaleRangeToData = Boolean.parseBoolean(val);
				else if (name.equals("swapseriesandcategories"))
					swapSeriesAndCategories = Boolean.parseBoolean(val);
				else if (name.equals("drillseries"))
					drillSeries = Boolean.parseBoolean(val);
				else if (name.equals("colornegative"))
					colorNegative = Boolean.parseBoolean(val);
				else if (name.equals("numsegments"))
					numSegments = Double.valueOf(val).intValue();
				else if (name.equals("pielabelformat"))
					pieLabelFormat = val;
				else if (name.equals("thresholdtype"))
					thresholdType = val;
				else if (name.equals("customthresholds"))
					customThresholds = val;
				else if (name.equals("footerlabel"))
					footerLabel = val;
				else if (name.equals("percent"))
					percent = Boolean.parseBoolean(val);
				else if (name.equals("interiorgap"))
					interiorGap = Double.parseDouble(val);
				else if (name.equals("startangle"))
					startAngle = Double.parseDouble(val);
				else if (name.equals("showpielabelkey"))
					showPieLabelKey = Boolean.parseBoolean(val);
				else if (name.equals("series"))
				{
					String seriesStr = extractSeriesAndUpdateMaps(val);
					series.add(seriesStr);
				} else if (name.equals("series2"))
					series2.add(val);
				else if (name.equals("category"))
					categories.add(val);
				else if (name.equals("formatmap"))
				{
					int index = val.indexOf('=');
					formatMap.put(val.substring(0, index), val.substring(index + 1));
				} else if (name.equals("aggregatemap")) {
					int index = val.indexOf('=');
					aggregateMap.put(val.substring(0, index), val.substring(index + 1));
				} else if (name.equals("colorcolumn"))
					colorColumn = val;
				else if (name.equals("lowermargin"))
					lowerMargin = Double.parseDouble(val);
				else if (name.equals("uppermargin"))
					upperMargin = Double.parseDouble(val);
				else if (name.equals("leftoutsidemargin"))
					leftOutsideMargin = Integer.parseInt(val);
				else if (name.equals("rightoutsidemargin"))
					rightOutsideMargin = Integer.parseInt(val);
				else if (name.equals("topoutsidemargin"))
					topOutsideMargin = Integer.parseInt(val);				
				else if (name.equals("bottomoutsidemargin"))
					bottomOutsideMargin = Integer.parseInt(val);
				else if (name.equals("shareaxis"))
					shareAxis = Boolean.parseBoolean(val);
				else if (name.equals("categorylabelangle"))
					categoryLabelAngle = Double.parseDouble(val);
				else if (name.equals("replacecolnameinurl"))
					updateColumnNameReplacementMap(val);
				else if (name.equals("replacecolvalueinurl"))
					updateColumnValueReplacementMap(val);
				else if (name.equals("replacecolvaluestrinurl"))
					updateColumnValueStrReplacementMap(val);
				else if (name.equals("jfreechartsemantics"))
					this.jfreechartsemantics = Boolean.parseBoolean(val);
				else if (name.equals("stacked"))
					this.stacked = Boolean.parseBoolean(val);
				else if (name.equals("noborder"))
					this.noborder = Boolean.parseBoolean(val);
				else if (name.equals("displayvalues"))
					this.displayValues = Boolean.parseBoolean(val);
				else if (name.equals("addfilter"))
					this.addFilters.add(val);
				else if (name.equals("drillon"))
					this.drillOn.add(val);
				else if (name.equals("majorinterval")) 
					majorInterval = Double.parseDouble(val);
				else if (name.equals("minorinterval"))
					minorInterval = Double.parseDouble(val);
				else if (name.equals("secondaxismajorinterval"))
					secondAxisMajorInterval = Double.parseDouble(val);
				else if (name.equals("secondaxisminorinterval"))
					secondAxisMinorInterval = Double.parseDouble(val);
			}
		}
		if (height == 0)
			height = 250;
		// XXX work around bug
		if (this.type == ChartType.StackedArea)
			vertical = true;
	}

	public String getChartCreateString(String dtype, String queryString, String drillTo)
	{
		return getChartCreateString(dtype, queryString, drillTo, null);
	}
	public String getChartCreateString(String dtype, String queryString, String drillTo, Map<String, Object> ps) {
		String expstr = null;
		String dtypestr = dtype;
		String typeStr = getTypeString();
		String JSFunc = "com.birst.flex.core.ChartDriller.DrillThru";
		if (! Chart.DRILL_TYPE_DRILL.equals(dtype) && ! Chart.DRILL_TYPE_NAVIGATE.equals(dtype)) {
			JSFunc = dtype;
		}
		if (type == ChartType.Bar || type == ChartType.Column || type == ChartType.Line || type == ChartType.Pie) {
			typeStr = "$P{" + CHART_TYPE + "}";
		}
		else {
			typeStr = "\"" + typeStr + "\"";
		}
		expstr = "com.successmetricsinc.chart.Chart.newChart(" + typeStr + ", $P{REPORT_DATA_SOURCE}, \"" + queryString + "\", \"" + getToString(ps) 
			+ "\",null, \"" + dtypestr + "\", \"" + JSFunc + "\", \"" + (drillTo == null ? "" : drillTo) + "\")";
		return (expstr);
	}

	public String getGeoMapTypeString() {
		String typeStr = WORLD_MAP;
		if (geoMapType == GeoMapType.ContinentMap)
			typeStr = CONTINENT_MAP;
		else if (geoMapType == GeoMapType.CountryMap)
			typeStr = COUNTRY_MAP;
		else if (geoMapType == GeoMapType.USStateWithCountiesMap)
			typeStr = USSTATE_WITH_COUNTIES_MAP;
		else if (geoMapType == GeoMapType.USStateWithZip3Map)
			typeStr = USSTATE_WITH_ZIP3_MAP;
		return typeStr;
	}
	public void decodeGeoMapTypeString(String typeString) {
		if (WORLD_MAP.equals(typeString))
			geoMapType = GeoMapType.WorldMap;
		else if (CONTINENT_MAP.equals(typeString))
			geoMapType = GeoMapType.ContinentMap;
		else if (COUNTRY_MAP.equals(typeString))
			geoMapType = GeoMapType.CountryMap;
		else if (USSTATE_WITH_COUNTIES_MAP.equals(typeString))
			geoMapType = GeoMapType.USStateWithCountiesMap;
		else if (USSTATE_WITH_ZIP3_MAP.equals(typeString))
			geoMapType = GeoMapType.USStateWithZip3Map;
		else {
			logger.warn("Could not parse geo map type " + typeString + ".  Defaulting to " + getGeoMapTypeString());
		}
	}
	
	
	public String getTypeString()
	{
		String typeStr = "";
		if (type == ChartType.Column)
		{
			vertical = true;
			typeStr = "column";
		} else if (type == ChartType.Bar)
		{
			vertical = false;
			typeStr = "bar";
		} else if (type == ChartType.Funnel)
		{
			vertical = false;
			typeStr = "funnel";		
		} else if (type == ChartType.Pyramid)
		{
			vertical = false;
			typeStr = "pyramid";
		} else if (type == ChartType.Radar)
		{
			vertical = false;
			typeStr = "radar";			
		} else if (type == ChartType.TreeMap)
		{
			vertical = false;
			typeStr = "treemap";					
		} else if (type == ChartType.Line)
		{
			vertical = true;
			typeStr = "line";
		} else if (type == ChartType.BarLine)
		{
			vertical = true;
			typeStr = "barline";
		} else if (type == ChartType.Pie)
		{
			typeStr = "pie";
		} else if (type == ChartType.Doughnut)
		{
			typeStr = "doughnut";			
		} else if (type == ChartType.StackedBar)
		{
			vertical = false;
			typeStr = "stackedbar";
		} else if (type == ChartType.StackedColumn)
		{
			vertical = true;
			typeStr = "stackedcolumn";
		} else if (type == ChartType.StackedColumnLine)
		{
			vertical = true;
			typeStr = "stackedcolumnline";
		} else if (type == ChartType.Area)
		{
			typeStr = "area";
		} else if (type == ChartType.StackedArea)
		{
			vertical = true; // XXX bug with stackedarea charts and not vertical
			typeStr = "stackedarea";
		} else if (type == ChartType.Scatterplot)
		{
			typeStr = "scatterplot";
		} else if (type == ChartType.ContourPlot)
		{
			typeStr = "contourplot";
		} else if (type == ChartType.BubbleChart)
		{
			typeStr = "bubblechart";
		} else if (type == ChartType.SpiderWebPlot)
		{
			typeStr = "spiderwebplot";
		} else if (type == ChartType.MeterPlot)
		{
			typeStr = "meterplot";
		} else if (type == ChartType.HeatMap)
		{
			typeStr = "heatmap";
		}
		else if (type == ChartType.Map) {
			typeStr = "map";
		}
		return typeStr;
	}

	private void decodeTypeString(String typeStr)
	{
		// scaleRangeToData = false;
		if ("column".equalsIgnoreCase(typeStr))
		{
			type = ChartType.Column;
			vertical = true;
		} else if ("bar".equalsIgnoreCase(typeStr))
		{
			vertical = false;
			type = ChartType.Bar;
		} else if ("funnel".equalsIgnoreCase(typeStr))
		{
			type = ChartType.Funnel;
		} else if ("pyramid".equalsIgnoreCase(typeStr))
		{
			type = ChartType.Pyramid;	
		} else if ("radar".equalsIgnoreCase(typeStr))
		{
			type = ChartType.Radar;		
		} else if ("treemap".equalsIgnoreCase(typeStr))
		{
			type = ChartType.TreeMap;						
		} else if ("line".equalsIgnoreCase(typeStr))
		{
			vertical = true;
			type = ChartType.Line;
		} else if ("barline".equalsIgnoreCase(typeStr))
		{
			vertical = true;
			type = ChartType.BarLine;
		} else if ("pie".equalsIgnoreCase(typeStr))
		{
			type = ChartType.Pie;
		} else if ("doughnut".equalsIgnoreCase(typeStr))
		{
			type = ChartType.Doughnut;
			
		} else if ("stackedbar".equalsIgnoreCase(typeStr))
		{
			vertical = false;
			type = ChartType.StackedBar;
		} else if ("stackedcolumn".equalsIgnoreCase(typeStr))
		{
			vertical = true;
			type = ChartType.StackedColumn;
		} else if ("stackedcolumnline".equalsIgnoreCase(typeStr))
		{
			vertical = true;
			type = ChartType.StackedColumnLine;
		} else if ("area".equalsIgnoreCase(typeStr))
		{
			type = ChartType.Area;
		} else if ("stackedarea".equalsIgnoreCase(typeStr))
		{
			type = ChartType.StackedArea;
		} else if ("scatterplot".equalsIgnoreCase(typeStr))
		{
			type = ChartType.Scatterplot;
		} else if ("contourplot".equalsIgnoreCase(typeStr))
		{
			type = ChartType.ContourPlot;
		} else if ("bubblechart".equalsIgnoreCase(typeStr))
		{
			type = ChartType.BubbleChart;
		} else if ("spiderwebplot".equalsIgnoreCase(typeStr))
		{
			type = ChartType.SpiderWebPlot;
		} else if ("meterplot".equalsIgnoreCase(typeStr))
		{
			type = ChartType.MeterPlot;
		} else if ("heatmap".equalsIgnoreCase(typeStr))
		{
			type = ChartType.HeatMap;
		}
		else if ("map".equalsIgnoreCase(typeStr)) {
			type = ChartType.Map;
		}
	}

	private String extractSeriesAndUpdateMaps(String val)
	{
		String seriesStr = val;
		int equalToIndex = val.indexOf('=');
		if (equalToIndex > 0)
		{
			// XXX hack for old style replace...url='key=val'
			String seriesGroup = val.substring(0, equalToIndex);
			if (seriesGroup.startsWith("'"))
				seriesGroup = seriesGroup.substring(1);
			seriesStr = val.substring(equalToIndex + 1, val.length());
			if (seriesStr.endsWith("'"))
				seriesStr = seriesStr.substring(0, seriesStr.length() - 1);
			String seriesName = null;
			int idx = seriesGroup.indexOf('|');
			if (idx > 0)
			{
				seriesName = seriesGroup.substring(idx + 1, seriesGroup.length());
				seriesGroup = seriesGroup.substring(0, idx);
			}
			if (!seriesGroups.contains(seriesGroup))
			{
				seriesGroups.add(seriesGroup);
			}
			seriesGroupBySeries.put(seriesStr, seriesGroup);
			if (seriesName != null)
			{
				seriesNameBySeries.put(seriesStr, seriesName);
			}
		}
		return seriesStr;
	}

	/**
	 * generic routine to parse and set values in a replacment map
	 * @param map
	 * @param val
	 */
	private void updateReplacementMap(Map<String,String> map, String val)
	{
		if ((val == null) || (val.indexOf('=') <= 0))
		{
			return;
		}
		String original = val.substring(0, val.indexOf('='));
		String toBeReplacedWith = val.substring(val.indexOf('=') + 1, val.length());
		// XXX hack for old style replace...url='key=val'
		if (original.startsWith("'") && toBeReplacedWith.endsWith("'"))
		{
			original = original.substring(1);
			toBeReplacedWith = toBeReplacedWith.substring(0, toBeReplacedWith.length() - 1);
		}
		map.put(original, toBeReplacedWith);
	}
	
	private void updateColumnValueReplacementMap(String val)
	{
		updateReplacementMap(columnValueReplacementsInURL, val);
	}

	private void updateColumnValueStrReplacementMap(String val)
	{
		updateReplacementMap(columnValueStrReplacementsInURL, val);
	}

	private void updateColumnNameReplacementMap(String val)
	{
		updateReplacementMap(columnNameReplacementsInURL, val);
	}

	public void setTitleFont(String titleFontStr)
	{
		if (titleFontStr == null || !Util.hasNonWhiteSpaceCharacters(titleFontStr))
		{
			titleFontStr = DEFAULT_FONT_STRING;
		}
		this.titleFontStr = titleFontStr;
		titleFont = Font.decode(titleFontStr);
	}

	public Font getTitleFont()
	{
		if ((titleFont == null) && (titleFontStr != null))
		{
			titleFont = Font.decode(titleFontStr);
		}
		return titleFont;
	}

	public void setLabelFont(String labelFontStr)
	{
		if (labelFontStr == null || !Util.hasNonWhiteSpaceCharacters(labelFontStr))
		{
			labelFontStr = DEFAULT_FONT_STRING;
		}
		this.labelFontStr = labelFontStr;
		labelFont = Font.decode(labelFontStr);
	}

	public Font getLabelFont()
	{
		if ((labelFont == null) && (labelFontStr != null))
		{
			labelFont = Font.decode(labelFontStr);
		}
		return labelFont;
	}

	public void setLegendFont(String legendFontStr)
	{
		if (legendFontStr == null || !Util.hasNonWhiteSpaceCharacters(legendFontStr))
		{
			legendFontStr = DEFAULT_LEGEND_FONT_STRING;
		}
		this.legendFontStr = legendFontStr;
		legendFont = Font.decode(legendFontStr);
	}

	public Font getLegendFont()
	{
		if ((legendFont == null) && (legendFontStr != null))
		{
			legendFont = Font.decode(legendFontStr);
		}
		return legendFont;
	}

	public Map<String, String> getFormatMap()
	{
		return formatMap;
	}

	public void setFormatMap(Map<String, String> formatMap)
	{
		this.formatMap = formatMap;
	}

	public int getWidth()
	{
		return width;
	}

	public void setWidth(int width)
	{
		this.width = width;
	}

	public static String getMapLocation(QueryResultSet qrs, GeoMapType geoMapType, String subMapName, String mapNameColumn) throws JRException {
		StringBuilder ret = new StringBuilder();
		if (geoMapType == GeoMapType.WorldMap) {
			// use world map
			ret.append("world/world");
		}
		else {
			if (subMapName == null || !Util.hasNonWhiteSpaceCharacters(subMapName) || "null".equals(subMapName)) {
				// get the value from the qrs
				if (qrs != null && qrs.numRows() > 0 && mapNameColumn != null && Util.hasNonWhiteSpaceCharacters(mapNameColumn)) {
					int columnIndex = -1;
					String[] displayNames = qrs.getDisplayNames();
					for (int i = 0; i < displayNames.length; i++) {
						if (mapNameColumn.equals(displayNames[i])) {
							columnIndex = i;
							break;
						}
					}
					if (columnIndex < 0) {
						// try column names
						String[] columnNames = qrs.getColumnNames();
						for (int i = 0; i < columnNames.length; i++) {
							if (mapNameColumn.equals(columnNames[i])) {
								columnIndex = i;
								break;
							}
						}
					}
					
					if (columnIndex >= 0) {
						Object val = qrs.getRows()[0][columnIndex];
						if (val != null) {
							subMapName = val.toString();
						}
					}
				}
			}
			
			if (geoMapType == GeoMapType.ContinentMap) {
				ret.append("world/" + subMapName);
			}
			else if (geoMapType == GeoMapType.CountryMap) {
				String r = GeoMap.countryMapLocationMap.get(subMapName.toUpperCase());
				if (r != null) {
					ret.append(r);
				}
			}
			else if (geoMapType == GeoMapType.USStateWithCountiesMap) {
				ret.append("usa/states/counties/");
				ret.append(subMapName.toUpperCase());
			}
			else if (geoMapType == GeoMapType.USStateWithZip3Map) {
				ret.append("usa/states/zip3/");
				ret.append(subMapName.toUpperCase());
			}
			else if (geoMapType == GeoMapType.Other) {
				ret.append(CustomGeoMap.getAmapLocation(qrs.getJasperDataSourceProvider().getRepository(), subMapName));
				return ret.toString().toLowerCase();
			}
		}
		
		// validate that ret is valid and an actual file
		// if not anyChart barfs and won't recover
		String mapName = ret.toString().toLowerCase();
		if (! GeoMap.validMapNames.containsKey(mapName)) {
			throw new JRException("Map " + mapName + " does not exist.  Can not render Geo Map.");
		}
		
		return mapName;
	}

	/**
	 * @return the majorInterval
	 */
	public double getMajorInterval() {
		return majorInterval;
	}

	/**
	 * @param majorInterval the majorInterval to set
	 */
	public void setMajorInterval(double majorInterval) {
		this.majorInterval = majorInterval;
	}

	/**
	 * @return the minorInterval
	 */
	public double getMinorInterval() {
		return minorInterval;
	}

	/**
	 * @param minorInterval the minorInterval to set
	 */
	public void setMinorInterval(double minorInterval) {
		this.minorInterval = minorInterval;
	}

	/**
	 * @return the secondAxisMajorInterval
	 */
	public double getSecondAxisMajorInterval() {
		return secondAxisMajorInterval;
	}

	/**
	 * @param secondAxisMajorInterval the secondAxisMajorInterval to set
	 */
	public void setSecondAxisMajorInterval(double secondAxisMajorInterval) {
		this.secondAxisMajorInterval = secondAxisMajorInterval;
	}

	/**
	 * @return the secondAxisMinorInterval
	 */
	public double getSecondAxisMinorInterval() {
		return secondAxisMinorInterval;
	}

	/**
	 * @param secondAxisMinorInterval the secondAxisMinorInterval to set
	 */
	public void setSecondAxisMinorInterval(double secondAxisMinorInterval) {
		this.secondAxisMinorInterval = secondAxisMinorInterval;
	}


}
