/**
 * $Id: CacheGenerator.java,v 1.19 2011-09-28 17:01:58 ricks Exp $
 *
 * Copyright (C) 2007-2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;

import org.apache.log4j.Logger;

import com.successmetricsinc.query.AbstractQueryString;
import com.successmetricsinc.query.JasperDataSourceProvider;
import com.successmetricsinc.query.Query;
import com.successmetricsinc.query.QueryResultSet;
import com.successmetricsinc.query.QueryString;
import com.successmetricsinc.query.ResultSetCache;
import com.successmetricsinc.query.QueryResultSet.RetrievedFrom;
import com.successmetricsinc.util.Util;

/**
 * Class to generate file cache entries
 * 
 * @author mjani
 * @deprecated
 * 
 */
public class CacheGenerator
{
	private static Logger logger = Logger.getLogger(CacheGenerator.class);
	private String outputDir;
	private String workingDir;
	private String outputFilePrefix;
	private List<String> keyParameterNameList;
	private List<List<String>> keyParameterValueList;
	private int curKeyIndex;
	private Repository r;
	private Vector<Integer> queriesServedFromDatabase = null;

	public CacheGenerator(String outputDir, String workingDir, String outputFilePrefix, List<String> keyParameterNameList,
			List<List<String>> keyParameterValueList, Repository r) throws IllegalArgumentException
	{
		if (outputDir == null || outputDir.equals(""))
		{
			throw new IllegalArgumentException("Invalid outputDir provided: <" + outputDir + ">.");
		}
		if (workingDir == null || workingDir.equals(""))
		{
			throw new IllegalArgumentException("Invalid workingDir provided: <" + workingDir + ">.");
		}
		if (outputFilePrefix == null || outputFilePrefix.equals(""))
		{
			throw new IllegalArgumentException("Invalid outputFilePrefix provided: <" + outputFilePrefix + ">.");
		}
		if (r == null)
		{
			throw new IllegalArgumentException("Invalid repository provided.");
		}
		if (workingDir.endsWith(File.separator))
		{
			this.workingDir = workingDir;
		} else
		{
			this.workingDir = workingDir + File.separator;
		}
		if (outputDir.endsWith(File.separator))
		{
			this.outputDir = outputDir;
		} else
		{
			this.outputDir = outputDir + File.separator;
		}
		this.outputFilePrefix = outputFilePrefix;
		this.keyParameterNameList = keyParameterNameList;
		this.keyParameterValueList = keyParameterValueList;
		this.r = r;
		File workingDirFile = new File(this.workingDir);
		if (!workingDirFile.exists())
		{
			workingDirFile.mkdir();
		}
		File outputDirFile = new File(this.outputDir);
		if (!outputDirFile.exists())
		{
			outputDirFile.mkdir();
		}
		queriesServedFromDatabase = new Vector<Integer>();
	}

	public void generateCache(int numThreads, List<String> seedQueryList, List<String> queryList) throws IllegalArgumentException
	{
		if (numThreads <= 0)
		{
			throw new IllegalArgumentException("Invalid numThreads provided: <" + numThreads + ">.");
		}
		ResultSetCache rsc = ResultSetCache.getInstance(r.getApplicationIdentifier(), false);
		JasperDataSourceProvider jdsp = new JasperDataSourceProvider(r, rsc);
		try
		{
			runSeedQueries(jdsp, seedQueryList);
		} catch (Exception e)
		{
			System.out.println("Exception occured while running seed queries - aborting process. See log for more details.");
			logger.error("Exception occured while running seed queries:", e);
			return;
		}
		// Spin threads to get the variable-specific of the result sets
		Thread[] tlist = new Thread[numThreads];
		curKeyIndex = 0;
		for (int i = 0; i < numThreads; i++)
		{
			tlist[i] = new ThreadCache(jdsp, new ArrayList<String>(queryList));
			tlist[i].start();
		}
		try
		{
			for (int i = 0; i < numThreads; i++)
			{
				tlist[i].join();
			}
		} catch (Exception ex)
		{
			logger.error(ex.toString());
			return;
		}
		if ((queriesServedFromDatabase != null) && (queriesServedFromDatabase.size() > 0))
		{
			logger
					.error("Warning: The following queries were served from the data source instead of cache - please review seed queries to prevent such situations.");
			System.out.println();
			System.out
					.println("Warning: The following queries were served from the data source instead of cache - please review seed queries to prevent such situations.");
			for (int qIdx : queriesServedFromDatabase)
			{
				logger.error(queryList.get(qIdx));
				System.out.println(queryList.get(qIdx));
			}
		}
	}

	private void runSeedQueries(JasperDataSourceProvider jdsp, List<String> seedQueryList) throws Exception
	{
		if (seedQueryList == null || (seedQueryList.size() <= 0))
		{
			return;
		}
		for (String query : seedQueryList)
		{
			System.out.print('#');
			jdsp.create(query);
		}
	}

	private synchronized int getNextKeyListIndex()
	{
		if (curKeyIndex >= keyParameterValueList.size())
			return (-1);
		return (curKeyIndex++);
	}

	public class ThreadCache extends Thread
	{
		JasperDataSourceProvider jdsp;
		List<String> queryList;

		public ThreadCache(JasperDataSourceProvider jdsp, List<String> queryList) throws IllegalArgumentException
		{
			this.setName("Cache Generation - " + this.getId());
			this.queryList = queryList;
			this.jdsp = jdsp;
		}

		public void run()
		{
			while (true)
			{
				int keyListIndex = getNextKeyListIndex();
				if (keyListIndex < 0)
				{
					return;
				}
				List<String> keyParameterValues = keyParameterValueList.get(keyListIndex);
				logger.debug("Processing: " + keyParameterValues.get(0));
				String cacheDir = workingDir + outputFilePrefix + keyParameterValues.get(0);
				File cacheDirFile = new File(cacheDir);
				if (!cacheDirFile.exists())
				{
					cacheDirFile.mkdir();
				}
				try
				{
					ResultSetCache rsc = ResultSetCache.getInstance(r.getApplicationIdentifier(), true);
					// Run all the queryList in the list so that the file cache is built
					int qIdx = 0;
					for (String query : queryList)
					{
						query = query.trim();
						for (int i = 0; i < keyParameterNameList.size(); i++)
						{
							String keyParameterName = keyParameterNameList.get(i);
							String keyParameterVariable = "V{" + keyParameterName + "}";
							query = query.replace(keyParameterVariable, keyParameterValues.get(i));
						}
						QueryResultSet qrs = (QueryResultSet) jdsp.create(query);
						if (qrs.whereFrom() == RetrievedFrom.database && !queriesServedFromDatabase.contains(qIdx))
						{
							queriesServedFromDatabase.add(qIdx);
						}
						query = QueryString.preProcessQueryString(query, r, null);
						AbstractQueryString aqs = AbstractQueryString.getInstance(r, query);
						Query q = aqs.getQuery();	
						rsc.updateCache(q, qrs);
						System.out.print('#');
						qIdx++;
					}
					// Build Jar file containing all cache entries
					String outputFilename = outputDir + outputFilePrefix + keyParameterValues.get(0) + ".jar";
					JarOutputStream out = new JarOutputStream(new FileOutputStream(outputFilename));
					logger.debug("Creating Jar File: " + outputFilename);
					String[] cacheEntries = cacheDirFile.list();
					for (int i = 0; i < cacheEntries.length; i++)
					{
						logger.debug("Adding to jar: " + cacheEntries[i]);
						File cacheFile = new File(cacheDirFile, cacheEntries[i]);
						JarEntry file = new JarEntry(cacheFile.getName());
						out.putNextEntry(file);
						FileInputStream fis = new FileInputStream(cacheFile);
						byte[] buf = new byte[1024];
						int j = 0;
						while ((j = fis.read(buf)) != -1)
						{
							out.write(buf, 0, j);
						}
						fis.close();
					}
					out.close();
					Util.deleteDir(cacheDirFile);
				} catch (Exception ex)
				{
					logger.error(ex.toString(), ex);
				}
			}
		}
	}
}
