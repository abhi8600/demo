/**
 * 
 */
package com.successmetricsinc.R;

import java.io.Serializable;

import org.rosuda.REngine.REXP;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RFileInputStream;
import org.rosuda.REngine.Rserve.RFileOutputStream;
import org.rosuda.REngine.Rserve.RserveException;

import com.successmetricsinc.transformation.BirstScriptParser;

/**
 * @author bpeters
 * 
 */
public class RServer implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String URL;
	public int Port = 6311;
	public String Username;
	public String Password;
	private RConnection c;

	public Object eval(int dataType, String expression) throws Exception
	{
		Object result = null;
		getConnection();
		try
		{
			REXP rexp = c.eval(expression);
			if (dataType == BirstScriptParser.NUMBERTYPE)
				result = rexp.asDouble();
			else if (dataType == BirstScriptParser.FLOATTYPE)
				result = rexp.asDouble();
			else if (dataType == BirstScriptParser.VARCHARTYPE)
				result = rexp.asString();
			else if (dataType == BirstScriptParser.INTEGERTYPE)
				result = rexp.asInteger();
		} catch (RserveException e)
		{
			throw new Exception("Unable to execute R Expression: " + e.getMessage());
		}
		return result;
	}

	public String getConnectionError()
	{
		try
		{
			getConnection();
			if (c != null && c.isConnected())
				return null;
		} catch (Exception e)
		{
			return e.getMessage();
		}
		return "Unable to connect to R Server";
	}

	private void getConnection() throws Exception
	{
		if (c == null || !c.isConnected())
		{
			try
			{
				c = new RConnection(URL, Port);
				c.login(Username, Password);
			} catch (RserveException e)
			{
				throw new Exception("Unable to connect to R Server [" + URL + ":" + Port + ", " + e.getMessage());
			}
		}
	}

	public RFileInputStream getInputStream(String path) throws Exception
	{
		getConnection();
		GetRStreamIThread t = new GetRStreamIThread();
		t.path = path;
		t.start();
		try
		{
			// Put a 5 second timeout into connecting in case R locks for some reason
			t.join(5000);
		} catch (InterruptedException ie)
		{
			// Timed out - perhaps it's a bad R Server, try to reconnect
			getConnection();
			t = new GetRStreamIThread();
			t.path = path;
			t.start();
			try
			{
				// Put a 5 second timeout into connecting in case R locks for some reason
				t.join(5000);
			} catch (InterruptedException ie2)
			{
				throw new Exception("R Server connection timed out");
			}
		}
		return t.rfis;
	}

	class GetRStreamIThread extends Thread
	{
		public RFileInputStream rfis = null;
		public String path;
		public Exception e;

		public void run()
		{
			try
			{
				rfis = c.openFile(path);
			} catch (Exception ex)
			{
				this.e = ex;
			}
		}
	}

	public RFileOutputStream getOutputStream(String path) throws Exception
	{
		getConnection();
		GetRStreamOThread t = new GetRStreamOThread();
		t.path = path;
		t.start();
		try
		{
			// Put a 5 second timeout into connecting in case R locks for some reason
			t.join(5000);
		} catch (InterruptedException ie)
		{
			// Timed out - perhaps it's a bad R Server, try to reconnect
			getConnection();
			t = new GetRStreamOThread();
			t.path = path;
			t.start();
			try
			{
				// Put a 5 second timeout into connecting in case R locks for some reason
				t.join(5000);
			} catch (InterruptedException ie2)
			{
				throw new Exception("R Server connection timed out");
			}
		}
		return t.rfos;
	}

	class GetRStreamOThread extends Thread
	{
		public RFileOutputStream rfos = null;
		public String path;
		public Exception e;

		public void run()
		{
			try
			{
				rfos = c.createFile(path);
			} catch (Exception ex)
			{
				this.e = ex;
			}
		}
	}
}
