package com.successmetricsinc;

import java.io.Serializable;
import java.util.List;

import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.warehouse.ForeignKey;
import com.successmetricsinc.warehouse.Location;

public class ImportedItem implements Serializable
{
  public String ItemName;
  public ForeignKey[] ForeignKeys;
  public Location[] Locations;
  
  public ImportedItem(Element e,Namespace ns)
  {
    ItemName = e.getChildText("ItemName",ns);
    
    Element fKeys = e.getChild("ForeignKeys",ns);
    if (fKeys!=null)
    {
      List children = fKeys.getChildren();
      if (children!=null && !children.isEmpty())
      {
        ForeignKeys = new ForeignKey[children.size()];
        for (int count =0;count<children.size();count++)
        {
          ForeignKey fk = new ForeignKey((Element) children.get(count), ns);
          ForeignKeys[count] = fk;
        }
      }
    }
    
    Element locations = e.getChild("Locations",ns);
    if (locations!=null)
    {
      List children = locations.getChildren();
      if (children!=null && !children.isEmpty())
      {
        Locations = new Location[children.size()];
        for (int count =0;count<children.size();count++)
        {
          Location lc = new Location((Element) children.get(count), ns);
          Locations[count] = lc;
        }
      }
    }
    
  }
  
  public Element getImportedItemElement(Namespace ns)
  {
    Element e = new Element("ImportedItem",ns);
    
    Element child = new Element("ItemName",ns);
    child.setText(ItemName);
    e.addContent(child);
    
    if (ForeignKeys!=null && ForeignKeys.length>0)
    {
      child = new Element("ForeignKeys",ns);
      for (ForeignKey fk : ForeignKeys)
      {
        child.addContent(fk.getForeignKeyElement(ns));
      }
      e.addContent(child);
    }
    
    if (Locations!=null && Locations.length>0)
    {
      child = new Element("Locations",ns);
      for (Location lc : Locations)
      {
        child.addContent(lc.getLocationElement(ns));
      }
      e.addContent(child);
    }
        
    return e;
  }
    
}
