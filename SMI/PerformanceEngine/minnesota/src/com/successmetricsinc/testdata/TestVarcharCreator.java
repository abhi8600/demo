package com.successmetricsinc.testdata;

import java.util.*;

public class TestVarcharCreator
{
	public static TestVarcharCreator tvc = null;
	private static final char[] symbols = new char[52];    
	static 
	{     
		for (int idx = 0; idx < 26; ++idx)
		{
			symbols[idx] = (char) ('a' + idx);   
		}
		for (int idx = 26; idx < 52; ++idx)
		{
			symbols[idx] = (char) ('A' + idx - 26);   
		}
	}
	
	Random rg = null;
	Map<Integer, Integer> colToSymbolIndexMap = null;
	public static TestVarcharCreator getInstance()
	{
		if(tvc == null)
		{
			tvc = new TestVarcharCreator();
		}
		return tvc;
	}
	
	private TestVarcharCreator()
	{
		rg = new Random();
		colToSymbolIndexMap = new HashMap<Integer, Integer>();
	}
	
	public void resetIndex()
	{
		colToSymbolIndexMap.clear();
	}
	
	public String getNextVarchar(int colIndex, int width)
	{
		StringBuilder sb = new StringBuilder();
		Integer idxObj = colToSymbolIndexMap.get(colIndex);
		if(idxObj == null)
		{
			idxObj = new Integer(0);
		}
		
		int idx = idxObj.intValue();

		for(int i = 0; i < width; i++)
		{
			if(idx < 52)
			{
				sb.append(symbols[idx]);
			}
			else
			{
				sb.append(symbols[rg.nextInt(symbols.length - 1)]);
			}
		}
		if(idx < 52)
		{
			idx++;
			colToSymbolIndexMap.put(colIndex, idx);
		}
		return sb.toString();
	}
}

