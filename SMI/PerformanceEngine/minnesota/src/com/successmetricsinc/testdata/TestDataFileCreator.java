package com.successmetricsinc.testdata;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.warehouse.*;

public class TestDataFileCreator 
{
	private static Logger logger = Logger.getLogger(TestDataFileCreator.class);

	private static long DEFAULT_NUM_ROWS_IN_EACH_FILE = 100;
	private static String DEFAULT_ENCODING = "UTF-8";
	private static final String DEFAULT_DATE_FORMAT = "MM/dd/yyyy";
	private static final String DEFAULT_DATETIME_FORMAT = "MM/dd/yyyy HH:mm:ss";
	
	private Repository r;
	private String dataFilePath;
	private long numRowsInEachFile = DEFAULT_NUM_ROWS_IN_EACH_FILE;
	Random rg = null;
	SimpleDateFormat sdfDate = null;
	SimpleDateFormat sdfDateTime = null;
	TestIntCreator tic = null;
	TestVarcharCreator tvc = null;
	
	public TestDataFileCreator(Repository r, String dataFilePath, long numRowsInEachFile)
	{
		this.r  = r;
		this.dataFilePath = dataFilePath;
		this.numRowsInEachFile = numRowsInEachFile;
		if(this.numRowsInEachFile <= 0)
		{
			this.numRowsInEachFile = DEFAULT_NUM_ROWS_IN_EACH_FILE;
		}
		if(dataFilePath != null && (dataFilePath.endsWith(File.separator)))
		{
			dataFilePath = dataFilePath.substring(0, (dataFilePath.length() - File.separator.length() - 1));
		}
		rg = new Random();
		sdfDate = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
		sdfDateTime = new SimpleDateFormat(DEFAULT_DATETIME_FORMAT);
		tic = TestIntCreator.getInstance();
		tic.resetIndex();
		tvc = TestVarcharCreator.getInstance();
		tvc.resetIndex();
	}

	public void createAll()
	{
		SourceFile[] sourceFiles = r.getSourceFiles();
		if(sourceFiles == null || sourceFiles.length == 0)
		{
			logger.warn("No source file definitions found in repository - exiting without creating any data files");
			return;
		}
		if(dataFilePath == null || dataFilePath.trim().length() == 0)
		{
			logger.warn("Null or blank data file path provided - exiting without creating any data files");
			return;
		}
		File dfp = new File(dataFilePath);
		if(!dfp.exists())
		{
			logger.warn("Directory for data file path does not exist - exiting without creating any data files");
			return;
		}
		for(SourceFile sf : sourceFiles)
		{
			create(sf);
		}
		logger.info("Starting to create test data files containing random data in directory: " + dataFilePath);
		logger.info("Each file created will contain " + numRowsInEachFile + " rows in addition to header row (if applicable)");
	}
	
	public void create(SourceFile sf)
	{
		tic.resetIndex();
		tvc.resetIndex();
		
		StagingTable st = r.findStagingTableForSourceFile(sf.FileName);
		if(st != null && st.Script != null && st.Script.InputQuery != null && st.Script.InputQuery.length() > 0)
		{
			//Scripted source - dont generate data file.
			logger.error("Skipping creation of data file - scripted source: " + st.Name);
			return;
		}
		
		File physicalsf = new File(this.dataFilePath + File.separator + sf.FileName);
		if(physicalsf.exists())
		{
			logger.error("Skipping creation of data file - it already exists: " + physicalsf.getAbsolutePath());
			return;
		}
		if(sf.Columns == null || sf.Columns.length == 0)
		{
			logger.error("No column definitions exist for : " + sf.FileName + " - skipping");
			return;
		}
		logger.info("Starting to create test data file: " + sf.FileName + " as " + physicalsf.getAbsolutePath());
		String encoding = sf.getEncoding();
		if(encoding == null || encoding.trim().length() == 0)
		{
			logger.info("Null or blank encoding encountered for file: " + sf.FileName + " - defaulting to " + DEFAULT_ENCODING);
			encoding = DEFAULT_ENCODING;
		}
		
		Map<SourceColumn, SimpleDateFormat> sourceColumnToDateFormatMap = new HashMap<SourceColumn, SimpleDateFormat>();
		for(SourceColumn sc : sf.Columns)
		{
			if((sc.Format != null) && (!sc.Format.isEmpty())
				&& ((sc.DataType == SourceColumn.Type.Date) || (sc.DataType == SourceColumn.Type.DateTime)))
			{
				SimpleDateFormat sdfCol = null;
				
				try
				{
					sdfCol = new SimpleDateFormat(sc.Format);
				}
				catch(Exception e)
				{
					logger.error("Illegal format provided for column: " + sf.FileName + "::" + sc.Name + ". Format value: " + sc.Format);
					sdfCol = null;
				}
				if(sdfCol != null)
				{
					sourceColumnToDateFormatMap.put(sc, sdfCol);
				}
			}
		}
		
		FileOutputStream fos = null;
		OutputStreamWriter os = null;
		BufferedWriter bw = null;

		try
		{
			fos = new FileOutputStream(physicalsf);
			os = new OutputStreamWriter(fos, encoding);
			bw = new BufferedWriter(os);
			
			if(sf.HasHeaders)
			{
				boolean firstCol = true;
				for(SourceColumn sc : sf.Columns)
				{
					if(firstCol)
					{
						firstCol = false;
					}
					else
					{
						bw.write(sf.Separator);
					}
					bw.write(sc.Name);
				}
				bw.write(((byte) '\n'));
				bw.flush();
			}
			for(int i = 0; i < this.numRowsInEachFile; i++)
			{
				boolean firstCol = true;
				int colIdx = 0;
				for(SourceColumn sc : sf.Columns)
				{
					if(firstCol)
					{
						firstCol = false;
					}
					else
					{
						bw.write(sf.Separator);
					}
					if(sc.DataType == null || sc.DataType == SourceColumn.Type.BadDataType || sc.DataType == SourceColumn.Type.None)
					{
						bw.write(getTestNoneValue());
					}
					else if(sc.DataType == SourceColumn.Type.Number)
					{
						bw.write(getTestNumberValue());
					}
					else if(sc.DataType == SourceColumn.Type.Integer)
					{
						bw.write(getTestIntegerValue());
					}
					else if(sc.DataType == SourceColumn.Type.Varchar)
					{
						bw.write(getTestVarcharValue(colIdx, sc.Width));
					}
					else if(sc.DataType == SourceColumn.Type.Date)
					{
						SimpleDateFormat sdf = sourceColumnToDateFormatMap.get(sc);
						if(sdf == null)
						{
							bw.write(getTestDateValue(sdfDate));
						}
						else
						{
							bw.write(getTestDateValue(sdf));
						}
					}
					else if(sc.DataType == SourceColumn.Type.DateTime)
					{
						SimpleDateFormat sdf = sourceColumnToDateFormatMap.get(sc);
						if(sdf == null)
						{
							bw.write(getTestDateTimeValue(sdfDateTime));
						}
						else
						{
							bw.write(getTestDateTimeValue(sdf));
						}
					}
					else if(sc.DataType == SourceColumn.Type.Float)
					{
						bw.write(getTestFloatValue());
					}
					colIdx++;
				}
				bw.write(((byte) '\n'));
			}
			
			logger.info("Test data file created successfully: " + sf.FileName + " as " + physicalsf.getAbsolutePath());
		}
		catch(Exception e)
		{
			logger.error("Test data file could not be created: " + sf.FileName + " as " + physicalsf.getAbsolutePath());			
			logger.error(e.toString(), e);
		}
		finally
		{
			try
			{
				if (bw != null)
					bw.close();
				if (os != null)
					os.close();
				if (fos != null)
					fos.close();
			}
			catch(Exception ex)
			{
				logger.error(ex.toString(), ex);
			}
		}
	}

	private String getTestNoneValue()
	{
		return "";
	}

	private String getTestNumberValue()
	{
		Double d = rg.nextDouble();
		if(d < 0)
		{
			d *= -1;
		}
		return d.toString();
	}

	private String getTestIntegerValue()
	{
		Integer i = rg.nextInt();
		if(i < 0)
		{
			i *= -1;
		}
		return i.toString();
	}
	
	private String getTestVarcharValue(int colIdx, int len)
	{
		if(len <= 0)
		{
			return "";
		}
		if(len > 2000)
		{
			len = 2000;
		}
		return tvc.getNextVarchar(colIdx, len);
	}

	private String getTestDateValue(SimpleDateFormat sdf)
	{
		return sdf.format(getTestCalendar().getTime());
		
	}

	private String getTestDateTimeValue(SimpleDateFormat sdf)
	{
		return sdf.format(getTestCalendar().getTime());
	}
	
	private String getTestFloatValue()
	{
		Float f = rg.nextFloat();
		if(f < 0)
		{
			f *= -1;
		}
		return f.toString();
	}

	private Calendar getTestCalendar()
	{
		Calendar cal = Calendar.getInstance();
		Integer i = rg.nextInt();
		if(i < 0)
		{
			i *= -1;
		}
		long ms = cal.getTimeInMillis();
		cal.setTimeInMillis(ms - i);
		return cal;
	}	
	
	public Repository getRepository() 
	{
		return r;
	}

	public void setRepository(Repository r) 
	{
		this.r = r;
	}

	public String getDataFilePath() 
	{
		return dataFilePath;
	}

	public void setDataFilePath(String dataFilePath) 
	{
		this.dataFilePath = dataFilePath;
	}

	public long getNumRowsInEachFile() 
	{
		return numRowsInEachFile;
	}

	public void setNumRowsInEachFile(long numRowsInEachFile) 
	{
		this.numRowsInEachFile = numRowsInEachFile;
	}
}
