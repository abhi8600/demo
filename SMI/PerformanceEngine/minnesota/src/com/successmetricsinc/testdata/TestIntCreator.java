package com.successmetricsinc.testdata;

public class TestIntCreator
{
	public static TestIntCreator tic = null;
	int index = 100000;
	public static TestIntCreator getInstance()
	{
		if(tic == null)
		{
			tic = new TestIntCreator();
		}
		return tic;
	}
	
	private TestIntCreator()
	{
	}
	
	public void resetIndex()
	{
		index = 100000;
	}
	public int getNextInt()
	{
		return index++;
	}
}

