package com.successmetricsinc;

import java.io.Serializable;

import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.util.XmlUtils;

public class BuildProperties implements Serializable
{
	public String postBuildReminder;
	
	public Element getBuildPropertiesElement(Namespace ns)
	{
		Element e = new Element("BuildProperties",ns);
		
		XmlUtils.addContent(e, "postBuildReminder", postBuildReminder,ns);
		
		return e;
	}
}
