package com.successmetricsinc.util;

public class TrellisTooLargeException extends BaseException {

	private static final long serialVersionUID = 1L;

	public TrellisTooLargeException()
	{
		super();
	}
	
	public TrellisTooLargeException(String s)
	{
		super(s);
	}
	
	public TrellisTooLargeException(String s, Throwable cause)
	{
		super(s, cause);
	}

	public int getErrorCode()
	{
		return BaseException.ERROR_TRELLIS_TOO_LARGE;
	}
}
 