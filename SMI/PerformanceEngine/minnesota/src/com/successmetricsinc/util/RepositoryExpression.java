package com.successmetricsinc.util;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.Tree;
import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.query.LogicalQueryString;
import com.successmetricsinc.query.SyntaxErrorException;
import com.successmetricsinc.transformation.BirstScriptLexer;
import com.successmetricsinc.transformation.BirstScriptParser;
import com.successmetricsinc.transformation.Expression;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.transformation.BirstScriptParser.logicalColumn_return;

public class RepositoryExpression
{
	private static Logger logger = Logger.getLogger(RepositoryExpression.class);
	Repository r;
	Session session;
	logicalColumn_return ret;
	String expression;

	public RepositoryExpression(Repository r, Session session, String expression)
	{
		this.r = r;
		this.session = session;
		this.expression = expression;
		ANTLRStringStream stream = new ANTLRStringStream(expression);
		BirstScriptLexer lex = new BirstScriptLexer(stream);
		CommonTokenStream cts = new CommonTokenStream(lex);
		BirstScriptParser parser = new BirstScriptParser(cts);
		try
		{
			ret = parser.logicalColumn();
			if (lex.hasError())
			{
				LogicalQueryString.processLexError(lex, expression);
			}
		} catch (RecognitionException e)
		{
			try {
				LogicalQueryString.processException(e, lex, expression);
			} catch (SyntaxErrorException e1) {
				logger.info("Unable to processException for RepositoryExpression: " + expression + ", " + e1.getMessage());
			}		
		} catch (Exception e)
		{
			logger.info("Unable to parse RepositoryExpression (V{=expression}): " + expression + ", "  + e.getMessage());
		}
	}

	public Object evaluate()
	{
		try
		{
			TransformationScript ts = new TransformationScript(r, session);
			ts.setLogicalQuery(true);
			Expression ex = new Expression(ts);
			Tree column = (Tree) ret.getTree();
			Operator op = ex.compile(column);
			Object o = op.evaluate();
			return o;
		} catch (Exception e)
		{
			logger.info("Unable to evaluate RepositoryExpression (V{=expression}): " + expression + ", " + e.getMessage());
		}
		return null;
	}
}
