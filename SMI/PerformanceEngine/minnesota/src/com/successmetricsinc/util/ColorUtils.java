/**
 * $Id: ColorUtils.java,v 1.2 2011-09-28 17:01:58 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */


package com.successmetricsinc.util;

import java.awt.Color;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;


/**
 * @author agarrison
 *
 */
public class ColorUtils {

	private final static String COLOR_CONVERSION_FORMAT = "%02x%02x%02x";
	private static Map<String, Color> map = new HashMap<String, Color>();

	static
	{
		try
		{
			// might be better to build this from a nice file of color name / rgb values XXX
			for (Field f : Color.class.getFields()) {
				if (f.getType() == Color.class) {
					Color c = (Color) f.get(null);
					map.put(f.getName().toLowerCase(), c); 
				}
			}
		}
		catch (Exception ex)
		{}
	}
		
	public static String colorToString(Color color) {
		if (color == null) 
			return null;
		return String.format(COLOR_CONVERSION_FORMAT, color.getRed(), color.getGreen(), color.getBlue());
	}
	
	public static Color stringToColor(String v) {
		if (v == null)
		{
			return null;
		}
		
		// see if it is a known color name, a bit of a hack
		Color c = map.get(v.toLowerCase());
		if (c != null)
			return c;

		v = ColorUtils.canonicalizeHex(v);
		if (v.length() == 6)
		{
			int red   = Integer.parseInt(v.substring(0, 2), 16);
			int green = Integer.parseInt(v.substring(2, 4), 16);
			int blue  = Integer.parseInt(v.substring(4, 6), 16);
			return new Color(red, green, blue);
		}
		return null;
	}

	public static String canonicalizeHex(String v)
	{
		if (v == null)
		{
			return null;
		}
		// mx:colorPicker in FLEX will strip leading zeros
		if (v.startsWith("#"))
			v = v.substring(1);
		if (v.startsWith("0x"))
			v = v.substring(2);
		if (v.length() == 2)
			v = "00" + v;
		if (v.length() == 4)
			v = "00" + v;
		return v;
	}
}
