package com.successmetricsinc.util;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

/**
 * File utilities.
 * @author pconnolly
 */
public final class FileUtils {

    public static final String UTF8 = "UTF8";
    public static final char SLASH = File.separatorChar;
	public static final String ILLEGAL_CHARACTERS = "\\\\/:*?\"<>|";

	public static final String SWAP_TMP_FILE = "swapTemp.lock";
    private static Logger logger = Logger.getLogger(FileUtils.class);
    
    private static File TMP_DIR;                      // set on 1st invocation of getHomeDir
    private static File HOME_DIR;                     // set on 1st invocation of getUserResourceDir
    private static File HOME_RESOURCE_DIR;

    /**
     * XML format to render.
     */
    public enum XMLFormat {

        /** no whitespace or newlines; preserves integrity. */
        RAW(Format.getRawFormat()),
        /** normalizes whitespace, not very many lines. */
        COMPACT(Format.getCompactFormat()),
        /** pretty print, human readable. */
        PRETTY(Format.getPrettyFormat());
        private Format format;

        /**
         * constructor.
         * @param format not null
         */
        XMLFormat(final Format format) {
            this.format = format;
        }

        /**
         * the format of xml to render.
         * @return never null
         */
        private Format getFormat() {
            return format;
        }
    }

	public static final String COMPILED_REPORT_EXTENSION = ".JASPER440";
	public static final String OLD_COMPILED_REPORT_EXTENSION = ".JASPER";

    /** Non constructor. */
    private FileUtils() {
    }
    
    /**
     * Get the relative path of a subdirectory or file relative to the given 'root' directory.
     * @param rootDirectory is the anchor directory on which to base the relative path of the 
     * provided subdirectoryFile.
     * @param subdirectoryFile is the full path of the subdirectory or file which will be 
     * used to determine its relative path. 
     * @return the relative path of the provided subdirectory or file.
     */
    public static String getRelativePath(final String rootDirectory, final String subdirectoryFile) {
    	final int index = subdirectoryFile.indexOf(rootDirectory);
    	if (index == -1) {
    		throw new IllegalArgumentException("subdir: " + subdirectoryFile + " not a subdir of: " + rootDirectory);
    	}
    	String relativePath = subdirectoryFile.substring(index);
    	return relativePath.replace("\\", "/");
    }

    /**
     * Get the system tmp dir.
     * @return The tmp dir
     * @throws IOException If tmp dir couldn't be determined
     */
    public static File getTmpDir()
            throws IOException {

        if (TMP_DIR == null) {
            final File tmpFile = File.createTempFile("foo", "bah");
            TMP_DIR = tmpFile.getParentFile();
            final String username = System.getProperty("temp");
            if (username != null) {
                TMP_DIR = new File(TMP_DIR.getAbsolutePath() + File.separator + username + File.separator);
                TMP_DIR.mkdirs();
            }
            tmpFile.delete();
        }
        return TMP_DIR;
    }

    /**
     * {@link File#createTempFile(String, String)} blended with {@link #getTmpDir()}.
     * @param prefix not null
     * @param suffix not null
     * @return not null
     * @throws IOException If the file couldn't be created
     */
    public static File createTempFile(final String prefix, final String suffix)
            throws IOException {
        return File.createTempFile(prefix, suffix, getTmpDir());
    }

    /**
     * Return the user's home directory.
     * @return Never null
     * @throws IOException
     */
    public static File getHomeDir() {
        if (HOME_DIR == null) {
            final String home = System.getProperty("user.home");
            HOME_DIR = new File(home);
        }
        return HOME_DIR;
    }

    /**
     * Get a directory (usually hidden) within the user's home directory which can be written to.
     * @return Never null
     * @throws IOException
     */
    public static File getHomeResourceDir() {
        if (HOME_RESOURCE_DIR == null) {
            final String homeResource = getHomeDir().getAbsolutePath() + File.separator + ".benetech";
            File temp = new File(homeResource);
            temp.mkdirs();
            HOME_RESOURCE_DIR = temp;
        }
        return HOME_RESOURCE_DIR;
    }

    /**
     * Get a subdirectory in the user's home resource directory, the subdirectory will be
     * created if it doesn't exit.
     * @param subDirName name of the subdirectory.
     * @return Never null
     */
    public static File getHomeResourceSubdir(final String subDirName) {
        final File homeDir = getHomeResourceDir();
        final File resourceSubdir = new File(homeDir, subDirName);
        resourceSubdir.mkdirs();
        return resourceSubdir;
    }

    /**
     * Useful in bean factory as a factory method for injecting subdirectories into classes based on values obtained by
     * our environment processors.
     * @param subDirname the subdirectory name under the tmpDir returned from getTmpDir.
     * @return a File object indicating this temp subdirectory, mkdirs() will be executed on it.
     * @throws IOException file or system problems, maybe invalid subDirname, it's your job to find out.
     */
    public static File createTmpSubdir(final String subDirname)
            throws IOException {
        final File tmpDir = new File(getTmpDir(), subDirname);
        tmpDir.mkdirs();
        return tmpDir;
    }

    /**
     * Create a temp subdirectory with a random name in the current temp directory.
     * @return a File object representing the new directory.
     * @throws IOException if file access fails
     */
    public static File createTmpSubdir()
            throws IOException {
        final Random rand = new Random();
        String thisRand = null;
        File tmpPath = null;
        do {
            int randomInt = rand.nextInt();
            if (randomInt < 0) {
                randomInt *= -1;
            }
            thisRand = Integer.valueOf(randomInt).toString();
            tmpPath = new File(FileUtils.getTmpDir(), thisRand);
        } while (tmpPath.exists());
        tmpPath = FileUtils.createTmpSubdir(thisRand);

        return tmpPath;
    }

    /**
     * Finds files within a given directory (and optionally its subdirectories) which match an array of extensions.
     * @param directory the directory to search in
     * @param extensions an array of extensions, ex. {"java","xml"}. If this parameter is <code>null</code>, all
     * files are returned.
     * @param recursive if true all subdirectories are searched as well
     * @return List of java.io.File with the matching files
     */
    public static List<File> listFiles(final File directory, final String[] extensions, final boolean recursive) {

        final Collection<?> fileCollection = org.apache.commons.io.FileUtils.listFiles(directory, extensions, recursive);
        final List<File> files = new ArrayList<File>(fileCollection.size());
        for (final Object o : fileCollection) {
            files.add((File) o);
        }
        return files;
    }

    /**
     * Save a given document to a String.  This method allows you to do some of the fancy formatting that
     * XMLOutputter is capable of and yet not write to a file.
     * @param document The document to save
     * @param printFormat boolean to set format: raw if false, compact if true
     * @throws IOException if file access fails
     */
    public static String saveDocumentToString(final Document document, final XMLFormat printFormat)
            throws IOException {

        final Format format = printFormat.getFormat();
        final XMLOutputter xmlOutputter = new XMLOutputter(format);
        final StringWriter oWriter = new StringWriter(4096);
        xmlOutputter.output(document, oWriter);

        return oWriter.getBuffer().toString();
    }

    /**
     * If needed, creates necessary set of nested directories.
     * @param dir Proposed to directory.
     * @return Directory after created.
     */
    public static File mkdir(final File dir) {

        if (!dir.exists()) {
            dir.mkdirs();
        }
        return dir;
    }

    /**
     * Returns the file basename, separating it from the full file path.
     * It does this by looking for the last '/' in the 'filepath'.
     * If no '/' is encountered the original 'filepath' is returned without
     * modification.  Otherwise, the file base name is extracted from the
     * 'filepath' and returned.
     * @param filepath is the file path to be processed. It can be an 
     * absolute path or a relative path.
     * @return the base name of the provided file path.
     */
    public static String getFileBasename(final String filepath) {
    	final int index = filepath.lastIndexOf('/');
    	if (index == -1) {
    		return filepath;
    	}
    	return filepath.substring(index + 1);
    }
    
    /**
     * Returns the filename without the extension after extracting the filename
     * from the file path.  If the filepath = "/private/user@domain/daily/ABC.AdhocReport"
     * then the result of this method will be "ABC".
     * @param filepath is the file path to be processed. It can be an 
     * absolute path or a relative path.
     * @return the basename of the file with the extension removed.
     */
    public static String getFileBasenameWithoutExtension(final String filepath) {
    	final String basename = getFileBasename(filepath);
    	final int index = basename.lastIndexOf('.');
    	if (index == -1) {
    		return basename;
    	}
    	return basename.substring(0, index);
    }
    
    /**
     * Convenience method that calls {@link #getFileBasename(String)}.
     * @param filepath is the file path to be processed. It can be an 
     * absolute path or a relative path.
     * @return the base name of the provided file path.
     */
    public static String getFileName(final String filepath) {
    	return getFileName(filepath, false);
    }

    /**
     * Convenience method that calls {@link #getFileBasename(String)} or
     * {@link #getFileBasenameWithoutExtension(String)} depending on the 
     * setting of the 'stripExtension' flag.
     * @param filepath is the file path to be processed. It can be an 
     * absolute path or a relative path.
     * @param stripExtension if 'true' then strip off the file extension from the
     * base file name; otherwise leave it as is.
     * @return the base file name, either with or without its file extension.
     */
    public static String getFileName(final String filepath, final boolean stripExtension) {
    	if (stripExtension) {
    		return getFileBasenameWithoutExtension(filepath);
    	} else {
    		return getFileBasename(filepath);
    	}
    }
    
    /**
     * Returns the extension of the passed in filename, or empty.
     * @param filename should not be null or empty
     * @return empty string or extension (without '.')
     */
    public static String getFileExtension(final String filename) {
    	String ext = "";
    	if (filename != null && !filename.equals("")) {
	    	final int index = filename.lastIndexOf('.');
	    	if (index > 0) {
	    		ext = filename.substring(index + 1);
	    	}
    	}
    	return ext;
    }
    
    /**
     * Returns the parent directory of the provided relative 'filePath' (e.g., private/user@...).
     * @param filePath is the current directory or file whose parent directory is requested.
     * @return the parent directory path of the provided 'filePath'.
     */
    public static String getFileParentRelative(final String filePath) {
    	final int index = filePath.lastIndexOf('/');
    	if (index == -1) {
    		return (String) null;
    	}
    	return filePath.substring(0, index);
    }
    
    /**
     * Returns the parent directory of the provided 'filePath' (e.g., ...private\\user@domain\\Dashboards will return
     * ...private\\user@domain).
     * @param filePath is the current directory or file whose parent directory is requested.
     * @return the parent directory path of the provided 'filePath'.
     */
    public static String getFileParent(final String filePath) {
    	final int index = filePath.lastIndexOf(File.separator);
    	if (index == -1) {
    		return (String) null;
    	}
    	return filePath.substring(0, index);
    }
    
    /**
     * This method is the complement of {@link #getFileParent(String)}.  It returns the leaf
     * directory of the provided 'filePath' (e.g., ...private\\user@domain\\Dashboards will return
     * Dashboards).
     * @param filePath is the file path whose leaf directory is requested.
     * @return the leaf directory path of the provided 'filePath'.
     */
    public static String getFileLeaf(final String filePath) {
    	final int index = filePath.lastIndexOf(File.separator);
    	if (index == -1) {
    		return (String) null;
    	}
    	return filePath.substring(index + 1);
    }
    
    /**
     * Converts any file path into its canonical form.
     * @param path the file path to be normalized.
     * @return the normalized version of the provided file path.
     */
    public static String normalizePath(final String path) {
    	final File file = new File(path);
    	String normalizedPath = null;
    	try {
    		normalizedPath = file.getCanonicalPath();
    	} catch (IOException e) {
    		logger.error("Could not get canonical path for file: " + path, e);
    	}
    	return normalizedPath;
    }

	/**
	 * Validates that the input fullFilePath is not null and not an empty String and
	 * that the file or directory actually exists.
	 * @param fullFilePath is the full filepath to be examined.
	 * @return an empty String ("") if the input fullFilePath represents a valid
	 * file or directory; a non-empty error message if there is a problem.
	 */
	public static String isValidPathInput(final String fullFilePath) {
		String errorMessage = "";
		if (fullFilePath == null || !Util.hasNonWhiteSpaceCharacters(fullFilePath)) {
			errorMessage = "Invalid file path: " + fullFilePath;
		} else if (!new File(fullFilePath).exists()) {
			errorMessage = "File does not exist: " + fullFilePath;
		}
		return errorMessage;
	}
	
	/**
	 * Checks to see if the provided 'filename' has any invalid characters for a 
	 * Windows file name or folder name.
	 * @param filename is the file name or folder name string to be validated.
	 * @return true if there are no illegal Windows characters in the 'filename'; 
	 * false otherwise.
	 */
	public static boolean isValidWindowsFilename(final String filename) {
		final Pattern pattern = Pattern.compile("[" + ILLEGAL_CHARACTERS + "]");
		final Matcher matcher = pattern.matcher(filename);
		if (matcher.find()) {
			return false;
		}
		return true;
	}
    
    /**
     * Remove punctuation that may cause problems from filenames, including dots.
     * Can use together with {@link #getFileBasename(String)} to exclude converting 
     * the file extension. 
     * @param fileName never null
     * @return never null
     */
    public static String sanitizeFileName(final String fileName) {
        final StringBuilder out = new StringBuilder();
        for (int i = 0; i < fileName.length(); i++) {
            final char c = fileName.charAt(i);
            if (!Character.isLetter(c) && !Character.isDigit(c)) {
                out.append("_");
            } else {
                out.append(c);
            }
        }
        return out.toString();
    }

	public static String getFileNamewithExtension(String filename, String extnToLookFor, String extnToReplace)
	{
		if (filename.endsWith(extnToLookFor))
		{
			return filename.substring(0, filename.length() - extnToLookFor.length()) + extnToReplace;
		}
		return filename;
	}
	
    public static Element readFileToDocument(final File xmlFile) {
        SAXBuilder builder = new SAXBuilder();
        Document document;
        try {
            document = builder.build(xmlFile);
        } catch (JDOMException e) {
            throw new RuntimeException("Problem parsing XML file: " + xmlFile.getAbsolutePath());
        } catch (IOException e) {
        	throw new RuntimeException("Problem reading XML file: " + xmlFile.getAbsolutePath());
        }
        return document.getRootElement();
    }

    public static boolean isSwapSpaceInProgress(String spaceDir) {

    	String swapTmpFile = FileUtils.getSwapTmpFilePath(spaceDir);
    	try
    	{
    		File file = new File(swapTmpFile);
    		if(file.exists())
    		{
    			return true;
    		}
    	}catch(Exception ex)
    	{
    		logger.error("Error checking for swap temp file in space dir " + spaceDir, ex);
    	}
    	return false;
    }
	
	public static String getSwapTmpFilePath(String spaceDir)
	{
		return spaceDir != null ?  spaceDir + File.separator + FileUtils.SWAP_TMP_FILE : null;
	}
}
