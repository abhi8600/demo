package com.successmetricsinc.util;

public class UserOperationNotAuthorizedException extends BaseException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserOperationNotAuthorizedException()
	{
		super();
	}
	
	public UserOperationNotAuthorizedException(String s)
	{
		super(s);
	}
	
	public UserOperationNotAuthorizedException(String s, Throwable cause)
	{
		super(s, cause);
	}
	
	public int getErrorCode()
	{
		return ERROR_UNAUTHORIZED_OPERATION;
	}
	
}
