package com.successmetricsinc.util;

public class SwapSpaceInProgressException extends BaseException {

	private static final long serialVersionUID = 1L;

	public SwapSpaceInProgressException()
	{
		super();
	}
	
	public SwapSpaceInProgressException(String s)
	{
		super(s);
	}
	
	public SwapSpaceInProgressException(String s, Throwable cause)
	{
		super(s, cause);
	}

	public int getErrorCode()
	{
		return ERROR_SWAP_SPACE_IN_PROGRESS;
	}
}
