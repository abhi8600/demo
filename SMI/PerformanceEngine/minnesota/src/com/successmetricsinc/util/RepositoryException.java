/**
 * $Id: RepositoryException.java,v 1.2 2010-06-24 17:51:18 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.util;

/**
 * @author Brad Peters
 * 
 */
public class RepositoryException extends BaseException
{
	private static final long serialVersionUID = 1L;

	public RepositoryException(String s)
	{
		super(s);
	}
	
	public RepositoryException(String s, Throwable t)
	{
		super(s, t);
	}

	public int getErrorCode()
	{
		return ERROR_REPOSITORY_ERROR;
	}
}