/**
 * helper class for AWS and Redshift support
 * 
 * for AWS SDK
 * - http://aws.amazon.com/documentation/sdkforjava/
 * 
 * for Redshift
 * - http://aws.amazon.com/documentation/redshift/
 */

package com.successmetricsinc.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.security.AlgorithmParameters;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidParameterSpecException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

import com.amazonaws.AmazonClientException;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicSessionCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3EncryptionClient;
import com.amazonaws.services.s3.model.DeleteObjectsRequest;
import com.amazonaws.services.s3.model.DeleteObjectsRequest.KeyVersion;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.DeleteObjectsResult;
import com.amazonaws.services.s3.model.EncryptionMaterials;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ProgressEvent;
import com.amazonaws.services.s3.model.ProgressListener;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3VersionSummary;
import com.amazonaws.services.s3.model.StorageClass;
import com.amazonaws.services.s3.model.VersionListing;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.Upload;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.transformation.DataType.DType;
import com.successmetricsinc.warehouse.Bulkload;
import com.successmetricsinc.warehouse.SourceFile;
import com.successmetricsinc.warehouse.SourceFile.FileFormatType;

public class AWSUtil
{
	// AWS constants
	private static StorageClass AWSStorageClass = StorageClass.Standard;	// reduced seemed to cause problems on earlier tests, might want to revisit

	private static final int MinSizeOfSplitFile = 10000000;		// minimum size (uncompressed) of any split file (10MB) - S3 startup costs are high - this may be too small
	private static final int EXECUTOR_TIMEOUT = 0; 				// in seconds, for parallel script execution - 0 is the correct value for the type of executor being used
	private static final int MaxRetriesPerSlice = 20;			// number of S3 upload tries on a split before giving up
	
	private static Logger logger = Logger.getLogger(AWSUtil.class);
	
	//
	// figure out the correct number of splits based upon the # of slices
	// - should be multiple of the # of slices in the cluster
	// - each XL node has 2 slices, each 8XL node has 16 slices - so a 4 x 8XL cluster has 64 slices
	// - http://docs.aws.amazon.com/redshift/latest/dg/t_splitting-data-files.html
	//
	public static int getNumberOfSlices(DatabaseConnection dc)
	{
		int slices = 16; // default slices (2 8XL nodes)

		Connection conn = null;
		java.sql.Statement stmt = null;
		ResultSet rs = null;
		try
		{
			conn = dc.ConnectionPool.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery("select count(*) from stv_slices");
			if (rs.next())
			{
				slices = rs.getInt(1);
				logger.info("Redshift cluster slice count: " + slices);
			}
		}
		catch (Exception ex)
		{
			logger.debug("Exception in determining number of Redshift slices", ex);
		}
		finally
		{
			// close down the result set and statement
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (SQLException e) {
				}
		}
		return slices;
	}
	
	/**
	 * split and upload the file, really should be incorporated into the original .tmp file writer
	 * @param localFile
	 * @param number
	 * @param tm
	 * @param bucket
	 * @param key
	 * @param request
	 * @param files
	 * @param splitKeys
	 * @return
	 * @throws IOException
	 */
	private static List<Upload> splitAndUploadFile(File localFile, int number, TransferManager tm, String bucket, String key, DeleteObjectsRequest request, List<File> files, List<String> splitKeys) throws IOException
	{
		List<Upload> transfers = new ArrayList<Upload>();
		List<KeyVersion> keys = new ArrayList<KeyVersion>();
		logger.debug("Starting the split of " + localFile.getName() + ", bucket: " + bucket + ", key: " + key);
		
		BufferedReader br = null;
		InputStreamReader is = null;
		FileInputStream fis = null;
		FileOutputStream fos = null;
		GZIPOutputStream zos = null;
		OutputStreamWriter os = null;
		BufferedWriter bw = null;
		try
		{
			fis = new FileInputStream(localFile);
			is = new InputStreamReader(fis, "UTF-8");
			br = new BufferedReader(is);
			
			long size = localFile.length(); 
			// should really be number of lines, but this is a good proxy, keep them atleast a minimum size
			long sizePerFile = Math.max(MinSizeOfSplitFile, (long) Math.ceil((double) size / (double) number));
			logger.debug("Size per file (uncompressed) " + sizePerFile);
			
			for (int i = 0; i < number; i++)
			{
				long currentFileSize = 0;
				String splitName = localFile.getAbsolutePath() + ".gz." + (i + 1);
				logger.debug("Writing " + splitName);

				File fl = new File(splitName);
				if (fl.exists())
					fl.delete();
				try
				{
					fos = new FileOutputStream(fl);
					// write a gzip compressed UTF-8 file
					zos = new GZIPOutputStream(fos);
					os = new OutputStreamWriter(zos, "UTF-8");
					bw = new BufferedWriter(os);
					String line = null;
					while ((line = br.readLine()) != null)
					{
						bw.write(line);
						currentFileSize += line.length();
						bw.write((byte) '\n');
						currentFileSize++;
						if (currentFileSize > sizePerFile && (i < number - 1)) // make sure the last file takes the rest
						{
							break;
						}
					}
					if (line == null) // nothing more, end early
						break;
				}
				finally
				{
					if (bw != null)
						bw.close();
					if (os != null)
						os.close();
					if (zos != null)
						zos.close();
					if (fos != null)
						fos.close();	
					
					// doing this in here so that the early exit break hits this code also
					logger.debug("final file size: " + fl.length());
					
					String splitKey = key + ".gz." + (i + 1);
					splitKey = Util.replaceStr(splitKey, " ", "_"); // spaces seem to cause an S3ServiceException speed limit error - https://forums.aws.amazon.com/thread.jspa?messageID=412696&#412696
					splitKeys.add(splitKey);
					keys.add(new KeyVersion(splitKey));
					files.add(fl);

					// start the upload of the split file	
					PutObjectRequest por = new PutObjectRequest(bucket, splitKey, fl);
					por.setStorageClass(AWSStorageClass);
					logger.info("uploading: " + por.getBucketName() + '/' + por.getKey());
					final Upload myUpload = tm.upload(por);	// asynchronous operation, waiting occurs in another method
					myUpload.addProgressListener(new ProgressListener() {
					    // This method is called periodically as your transfer progresses
						int last = -1;
					    public void progressChanged(ProgressEvent progressEvent) 
					    {
					    	int percent = (int) myUpload.getProgress().getPercentTransferred();
							String fileName = myUpload.getDescription();
							int pos = fileName.lastIndexOf('/'); 
							if (progressEvent.getEventCode() == ProgressEvent.COMPLETED_EVENT_CODE)
								logger.info("uploaded: " + fileName.substring(pos + 1));
							else
							{
								if ((int) (last/25) != (int) (percent/25) && percent != 100)
								{
									logger.info("uploading: " + fileName.substring(pos + 1) + ", state: " + myUpload.getState() +
											", bytes: " + myUpload.getProgress().getBytesTransferred() + " (" + percent + "%)");
									last = percent;
								}
							}
					    }
					});
					transfers.add(myUpload);	
				}
			}
		}
		finally
		{
			try
			{
				if (br != null)
					br.close();
				if (is != null)
					is.close();
				if (fis != null)
					fis.close();
			}
			catch(Exception e)
			{
				logger.error(e.getMessage(), e);
			}
		}
		logger.debug("Finished the split of " + localFile.getAbsolutePath() + " into " + keys.size() + " pieces"); 
		request.setKeys(keys);
		return transfers;
	}
	
	/**
	 * encapsulate the uploading to Amazon S3
	 * @author ricks
	 *
	 */
	public static class BirstTransferManager
	{
		AmazonS3 client;
		TransferManager tm;
		List<Upload> transfers;
		List<File> files;
		List<KeyVersion> keys;
		String bucket;
		String folder;
		DeleteObjectsRequest request;
		ThreadPoolExecutor tp = null;
		int uploadThreads = 0;
		int splits = 1;
		
		public BirstTransferManager(AmazonS3 s3client2)
		{
			// used for just S3 data sources XXX
			client = s3client2;
		}
		
		public BirstTransferManager(AmazonS3 s3client2, String _s3Location, int _uploadThreads, int _splits)
		{
			String items[] = splitS3Location(_s3Location);
			bucket = items[0];
			folder = items[1];

			client = s3client2;
			transfers = new ArrayList<Upload>();
			files = new ArrayList<File>();
			keys = new ArrayList<KeyVersion>();
			request = new DeleteObjectsRequest(bucket);
			uploadThreads = _uploadThreads;
			splits = _splits;
			
			// get rid of any old objects in the place we are about to upload to
			s3CleanupObjects(_s3Location, client);

			// create executor for the upload
			BlockingQueue<Runnable> workQueue = new ArrayBlockingQueue<Runnable>(uploadThreads);
			tp = new ThreadPoolExecutor(uploadThreads, uploadThreads, EXECUTOR_TIMEOUT, TimeUnit.SECONDS, workQueue, new ThreadPoolExecutor.CallerRunsPolicy());
			tm = new TransferManager(client, tp);
		}
		
		public AmazonS3 getClient()
		{
			return client;
		}
		
		public int getNumberOfSplits()
		{
			return splits;
		}
		
		/**
		 * cleanup uploaded S3 items (after bulk loading)
		 */
		public void cleanup()
		{
			if (tm == null)
				return;
			if (request != null && request.getKeys() != null && request.getKeys().size() > 0)
			{
				DeleteObjectsResult res = client.deleteObjects(request);
				logger.debug("Deleted " + res.getDeletedObjects().size() + " S3 object(s) from " + request.getBucketName());
			}
	    	
	    	tm.shutdownNow(); // shut everything down, hopefully releasing all resources (and closing files) - shuts down client also

	    	if (files != null)
	    	{
	    		for (File fl : files)
	    		{
	    			if (fl.exists())
	    			{
	    				if (fl.delete())
	    					logger.debug("Deleted " + fl.getName());
	    				else
	    					fl.deleteOnExit(); // AWS transfer manager keeps file handles open in some cases, so use this to clean up at the end
	    			}
	    		}
	    	}
		}

		/**
		 * upload a file using the AWS transfer manager
		 * @param file
		 * @throws Exception
		 */
		public void uploadFile(File file) throws Exception
		{		
			if (tm == null)
				return;
			String name = file.getName();
			String splitKey = buildSafeS3Key(name);
			keys.add(new KeyVersion(splitKey));
			files.add(file);
						
			PutObjectRequest por = new PutObjectRequest(bucket, splitKey, file);
			por.setStorageClass(AWSStorageClass);
			logger.info("uploading: " + por.getKey());
			final Upload myUpload = tm.upload(por);	// asynchronous operation, waiting occurs in another method
			addProgressListener(myUpload);
			transfers.add(myUpload);
		}
		
		/** 
		 * wait for all of the files that have started the uploading process to finish
		 * @throws Exception
		 */
		public void waitForUploads() throws Exception
		{		
			if (tm == null)
				return;
			int retryCount = 0;

			for (int i = 0; i < transfers.size(); i++)
			{
				Upload upload = transfers.get(i);
				File fl = files.get(i);
				AmazonClientException ex = upload.waitForException();
				if (ex != null)
				{
					logger.debug(ex.getMessage());
					// retry failures
					if (++retryCount > MaxRetriesPerSlice)
					{
						throw new IOException("Failed to upload to S3 (hit max retry count): " + upload.getDescription());
					}
					logger.warn("Failed to upload (retrying): " + upload.getDescription());
					KeyVersion keyVersion = keys.get(i);
					String splitKey = keyVersion.getKey();

					PutObjectRequest por = new PutObjectRequest(bucket, splitKey, fl);
					por.setStorageClass(AWSStorageClass);
					final Upload myUpload = tm.upload(por);
					addProgressListener(myUpload);

					transfers.add(myUpload);
					files.add(fl);
					keys.add(keyVersion);
				}
			}
			request.setKeys(keys);
		}
		
		private String buildSafeS3Key(String name)
		{
			// spaces seem to cause an S3ServiceException speed limit error - https://forums.aws.amazon.com/thread.jspa?messageID=412696&#412696
			return folder + '/' + Util.replaceStr(name, " ", "_");
		}
	}
	
	/**
	 * add a progress listener to an upload request
	 * @param myUpload
	 */
	private static void addProgressListener(final Upload myUpload)
	{
		myUpload.addProgressListener(new ProgressListener() {
		    // this method is called periodically as the transfer progresses
			int last = -1;
		    public void progressChanged(ProgressEvent progressEvent) 
		    {
		    	int percent = (int) myUpload.getProgress().getPercentTransferred();
				String fileName = myUpload.getDescription();
				int pos = fileName.lastIndexOf('/'); 
				if (progressEvent.getEventCode() == ProgressEvent.COMPLETED_EVENT_CODE)
					logger.info("uploading complete: " + fileName.substring(pos + 1));
				else
				{
					// log every 25% upload
					if ((int) (last/25) != (int) (percent/25) && percent < 100)
					{
						logger.debug("uploading: " + fileName.substring(pos + 1) + ", state: " + myUpload.getState() +
									 ", bytes: " + myUpload.getProgress().getBytesTransferred() + " (" + percent + "%)");
						last = percent;
					}
				}
		    }
		});
	}
	
	/**
	 * thread to read data from an S3 object and place in a queue for futher processing (see BirstResultSet)
	 * - does just enough parsing for the needs of ETL scripts
	 * @author ricks
	 *
	 */
	public static class BirstS3ReadThread extends Thread
	{
		S3ObjectInputStream sis = null;
		GZIPInputStream zis = null;
		InputStreamReader is = null;
		BufferedReader br = null;
		S3Object s3Object;
		DType[] types;
		
		AmazonS3 client;
		BlockingQueue<Object[]> readQueue;
		List<GetObjectRequest> objects;
		AtomicInteger index;
		List<Exception> exceptionList;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SourceFile sf;
		
		public BirstS3ReadThread(AmazonS3 s3client, List<GetObjectRequest> objects, BlockingQueue<Object[]> readQueue, DType[] types, AtomicInteger index, List<Exception> exceptionList,SourceFile sf)
		{
			this.setName("Birst S3 Reader - " + this.getId());
			this.setDaemon(true);
			this.client = s3client;
			this.objects = objects;
			this.readQueue = readQueue;
			this.types = types;
			this.index = index;
			this.exceptionList = exceptionList;
			this.sf = sf;
		}

		public void run()
		{
			int idx = index.getAndIncrement();

			while (idx < objects.size())
			{
				long count = 0;
				GetObjectRequest req = null;
				try
				{
					// open input stream on bucket/location
					req = objects.get(idx);
					s3Object = client.getObject(req);
					long size = s3Object.getObjectMetadata().getContentLength();
					logger.debug("Reading from S3 object: " + s3Object.getBucketName() + "/" + s3Object.getKey() + ", size: " + size);
					sis = s3Object.getObjectContent();
					zis = new GZIPInputStream(new Bulkload.S3ObjectInputStreamWrapper(sis, client));
					is = new InputStreamReader(zis, "UTF-8");
					br = new BufferedReader(is);
					// iterate over the lines in the input stream
					StringBuilder line = null;
					StringBuilder sb = new StringBuilder(2000);
					while((line = Bulkload.readLine(sb, sf, sf.Separator.toCharArray(), br, sf.Type != FileFormatType.FixedWidth, sf.ForceNumColumns ? sf.Columns.length - 1 : 0,sf.OnlyQuoteAfterSeparator)) != null)
					{							
        				 String lineStr = line.toString().replaceAll("\\\\\n", String.valueOf((char) 254));
        				 lineStr = lineStr.replaceAll("\\\\\r", String.valueOf((char) 255));
						// tokenize each line
        				 //lineStr.replaceAll("\\\\", "\\\\\\\\")
						 String[] tokens = lineStr.split("(?<!\\\\)\\|"); // doesn't handle field like abc\
						if (tokens == null)
						{
							logger.debug(req.getKey() + " (" + count + "): null result from split");
							continue;
						}

						// iterate over the tokens and convert to the proper type
						Object[] objs = new Object[types.length];
						for (int i = 0; i < types.length && i < tokens.length; i++)
						{
							if(tokens[i]!=null && tokens[i].toString().startsWith("\"") && tokens[i].toString().endsWith("\"")) 
							{
								tokens[i] = tokens[i].substring(1,tokens[i].length()-1); 
							}
							if (tokens[i] == null || tokens[i].length() == 0 || tokens[i] == "\\N" || tokens[i] == "\\\\N") // Redshift returns NULL as \N, ESCAPE on UNLOAD adds an extra \
							{
								objs[i] = null;
							}
							else
							{
								try
								{
									switch (types[i])
									{
									case Integer:
										objs[i] = Long.valueOf(tokens[i]);
										break;
									case Date:
										objs[i] = sdf.parse(tokens[i]);
										break;
									case DateTime:
										objs[i] = sdft.parse(tokens[i]);
										break;
									case Float:
										objs[i] = Double.valueOf(tokens[i]);
										break;
									default:
										if(tokens[i].indexOf(String.valueOf(((char)254)))!=-1){
											tokens[i] = Util.replaceStr(tokens[i],String.valueOf(((char)254)),"\n");
										}
										if(tokens[i].indexOf(String.valueOf(((char)255)))!=-1){
											tokens[i] = Util.replaceStr(tokens[i],String.valueOf(((char)255)),"\r");
										}
										tokens[i] = tokens[i].replaceAll("\\\\\\\\","\\\\");
										objs[i] = tokens[i];
										break;
									}
								}
								catch (Exception ex)
								{
									if (req!=null)
									{
										logger.warn(req.getKey() + " (" + count + "): " + tokens[i] + "/" + types[i]+ ": " + ex.getMessage(), ex);
									}
									objs[i] = null;
								}
							}
						}
						while (!readQueue.offer(objs))
						{
							try
							{
								Thread.sleep(100);
							}
							catch (InterruptedException ex)
							{}
						}
					}
				}
				catch (Exception ex)
				{
					exceptionList.add(ex);
					if (req!=null)
					{
						logger.warn(req.getKey() + " (" + count + "): " + ex.getMessage(), ex);
					}
					if (sis != null)
					{
						try { sis.abort(); } 
						catch (Exception ex1) 
						{
							logger.debug("Could not abort S3 object stream");
						};
					}
					break;
				}
				finally
				{
					try
					{
						if (br != null)
							br.close();
						if (is != null)
							is.close();
						if (zis != null)
							zis.close();
						if (sis != null)
							sis.close();
						if (s3Object != null)
							s3Object.close();
					}
					catch (Exception ex1)
					{
						logger.debug(ex1, ex1);
					}
				}
				try
				{
					// delete the object after successful processing
					logger.debug("Deleting: " + req.getBucketName() + '/' + req.getKey());
					DeleteObjectRequest request = new DeleteObjectRequest(req.getBucketName(), req.getKey());						
					client.deleteObject(request);
				}
				catch (Exception ex)
				{
					logger.debug(ex, ex);
				}
				idx = index.getAndIncrement();
			}
			addEndToken();
		}
		
		private void addEndToken()
		{
			Object[] endToken = new Object[] { Bulkload.END_TOKEN };
			while (!readQueue.offer(endToken))
			{
				try
				{
					Thread.sleep(100);
				}
				catch (InterruptedException ex)
				{}
			}
		}
	}

	
	/**
	 * copy a local file to an S3 bucket as a set of split and compressed files
	 * @param slices number of slices/splits
	 * @param localFile	file to split and upload
	 * @param s3Location	path to the entity (excluding the 'file name') - s3://bucketname/folder
	 * @param s3client	AWS S3 client
	 * @return	object that can be used to delete the uploaded files after processing
	 * @throws Exception
	 */
	public static DeleteObjectsRequest s3FileCopy(int sliceCount, File localFile, String s3Location, AmazonS3 s3client) throws Exception
	{
		int retryCount = 0;
		
		String[] items = splitS3Location(s3Location);
		String s3Bucket = items[0];
		String folder = items[1];
		
		logger.info("Copying " + localFile.getName() + " to " + s3Location);

		String key = folder + '/' + localFile.getName();
		DeleteObjectsRequest request = null;
		
		// deleting any left over objects for this bucket/key prefix
		s3CleanupObjects(s3Location + '/' + localFile.getName(), s3client);

		// create executor for the upload - should really this across splits and also for parallel processing the file (doesn't use TP currently) and doing parallel script execution (does use TP) XXX
		BlockingQueue<Runnable> workQueue = new ArrayBlockingQueue<Runnable>(sliceCount); // not CPU intensive, so allow the full number of threads
		logger.debug("Creating transfer queue of length " + workQueue.remainingCapacity() + " with " + sliceCount + " threads");
		ThreadPoolExecutor tp = new ThreadPoolExecutor(sliceCount, sliceCount, EXECUTOR_TIMEOUT, TimeUnit.SECONDS, workQueue, new ThreadPoolExecutor.CallerRunsPolicy());

		TransferManager tm = new TransferManager(s3client, tp);

		request = new DeleteObjectsRequest(s3Bucket); // keep track of objects to delete post bulk load
		List<File> files = new ArrayList<File>();
		List<String> splitKeys = new ArrayList<String>();

		// request is used to delete the uploaded S3 objects, files is used to delete the split files
		List<Upload> transfers = splitAndUploadFile(localFile, sliceCount, tm, s3Bucket, key, request, files, splitKeys);

		// wait for transfers to finish and delete the split files
		for (int i = 0; i < transfers.size(); i++)
		{
			Upload upload = transfers.get(i);
			File fl = files.get(i);
			AmazonClientException ex = upload.waitForException();
			if (ex != null)
			{
				logger.debug(ex.getMessage());
				// retry failures
				if (++retryCount > MaxRetriesPerSlice)
				{
					throw new IOException("Failed to upload to S3 (hit max retry count): " + upload.getDescription());
				}
				logger.info("Failed to upload (retrying): " + upload.getDescription());
				String splitKey = splitKeys.get(i);

				PutObjectRequest por = new PutObjectRequest(s3Bucket, splitKey, fl);
				por.setStorageClass(AWSStorageClass);
				final Upload myUpload = tm.upload(por);
				myUpload.addProgressListener(new ProgressListener() {
				    // This method is called periodically as your transfer progresses
					int last = -1;
				    public void progressChanged(ProgressEvent progressEvent) 
				    {
				    	int percent = (int) myUpload.getProgress().getPercentTransferred();
						String fileName = myUpload.getDescription();
						int pos = fileName.lastIndexOf('/'); 
						if (progressEvent.getEventCode() == ProgressEvent.COMPLETED_EVENT_CODE)
							logger.info("uploaded: " + fileName.substring(pos + 1));
						else
						{
							if ((int) (last/25) != (int) (percent/25) && percent != 100)
							{
								logger.info("uploading: " + fileName.substring(pos + 1) + ", state: " + myUpload.getState() +
										", bytes: " + myUpload.getProgress().getBytesTransferred() + " (" + percent + "%)");
								last = percent;
							}
						}
				    }
				});
				transfers.add(myUpload);
				files.add(fl);
				splitKeys.add(splitKey);
			}
			else
			{
				if (fl.exists())
				{
					logger.debug("cleaning up file: " + fl.getName());
					try
					{
						boolean res = fl.delete(); // cleanup of file streams from upload? XXXX
						if (!res)
							logger.debug("delete returned false");
					}
					catch (Exception e)
					{
						logger.debug("Exception in delete: " + e.getMessage());
					}
				}
			}
		}
		return request;
	}
	
	/**
	 * delete a specific set of S3 objects
	 * - to delete all objects in a folder, see s3CleanupObjects
	 * @param request	List of keys in a bucket to delete
	 * @param myCredentials	AWS Credentials
	 */
	public static void s3DeleteObjects(DeleteObjectsRequest request, AmazonS3 s3)
	{
		if (request == null || request.getKeys() == null || request.getKeys().size() == 0)
			return;
		logger.info("Deleting S3 objects created during this load: " + request.getBucketName());
		DeleteObjectsResult res = s3.deleteObjects(request);
    	logger.debug("Deleted S3 objects - count: " + res.getDeletedObjects().size());
	}
	
	/**
	 * clean up the S3 location specification and split into bucket and folder/key
	 * @param s3Location
	 * @return
	 * @throws Exception
	 */
	public static String[] splitS3Location(String s3Location)
	{
		String temp = cleanupPath(s3Location);
		
		if (temp.startsWith("s3://"))
			temp = temp.substring("s3://".length());

		int pos = temp.indexOf('/');
		String bucketName = pos < 0 ? temp : temp.substring(0, pos);
		String key = pos < 0 ? null : temp.substring(pos + 1);
		return new String[] { bucketName, key };
	}
	
	/**
	 * clean up an S3 path
	 * - \\ to /, deal with S3 naming issues
	 */
	public static String cleanupPath(String s3Location)
	{
		String temp = Util.replaceStr(s3Location, "\\", "/"); // deal with windows style path names
		temp = Util.replaceStr(temp, " ", "_"); // spaces seem to cause an S3ServiceException speed limit error - https://forums.aws.amazon.com/thread.jspa?messageID=412696&#412696
		return temp;
	}
	
	public static List<GetObjectRequest> getObjectList(AmazonS3 client, String s3Bucket, String s3Key)
	{
		List<GetObjectRequest> objects = new ArrayList<GetObjectRequest>();
		VersionListing versionListing = client.listVersions(s3Bucket, s3Key);
		List<S3VersionSummary> versionSummaries = versionListing.getVersionSummaries();
        while (versionSummaries != null && versionSummaries.size() > 0)
        {
            for (S3VersionSummary objectSummary : versionSummaries)
            {
            	String keyName = objectSummary.getKey();
                String versionId = objectSummary.getVersionId();
                logger.debug("Found: " + s3Bucket + '/' + keyName + (versionId != null ? (';' + versionId) : ""));
                GetObjectRequest req = new GetObjectRequest(s3Bucket, keyName, versionId);
                objects.add(req);
            }
            // handle case of more than 1000 keys (we only split 1 x # of slices, so unlikely, but for completeness)
            versionListing = client.listNextBatchOfVersions(versionListing);
            versionSummaries = versionListing.getVersionSummaries();
        }
        return objects;
	}
	
	public static void s3DeleteObjects(AmazonS3 client, String s3Bucket, String s3Key)
	{
		List<GetObjectRequest> objects = getObjectList(client, s3Bucket, s3Key);
		List<KeyVersion> keys = new ArrayList<KeyVersion>();
		if (objects.size() > 0)
		{
			for (GetObjectRequest req : objects)
			{
				keys.add(new KeyVersion(req.getKey(), req.getVersionId()));
			}
		}
		
        if (keys.size() > 0)
        {
    		DeleteObjectsRequest request = new DeleteObjectsRequest(s3Bucket);
        	request.setKeys(keys);
        	DeleteObjectsResult res = client.deleteObjects(request);
        	logger.debug("Deleted S3 objects - count: " + res.getDeletedObjects().size());
        }
	}

	/**
	 * clean up any S3 objects left in the bucket
	 * @param bucketName	S3 bucket
	 * @param prefix	path to items (excludes the .### ending)
	 * @param client	s3 client object
	 * @throws Exception 
	 */
	public static void s3CleanupObjects(String s3Location, AmazonS3 client)
	{
		logger.info("Cleaning up S3 objects from previous runs in: " + s3Location);
		String[] items = AWSUtil.splitS3Location(s3Location);
		String s3Bucket = items[0];
		String key = items[1];

		s3DeleteObjects(client, s3Bucket, key);
	}
	
	/**
	 * log load failure messages from Redshift
	 * - output stl_load_errors contents
	 * @throws SQLException 
	 */
	public static void dumpErrors(String tableNameInLoadErrors, String tableName, SQLException sqle, java.sql.Statement stmt) throws SQLException
	{
		logger.debug(sqle.getMessage());
		if (sqle.getCause() != null)
			logger.debug(sqle.getCause().getMessage());
		//
		// Redshift is parallel load, so there can be multiple load errors
		// - http://docs.aws.amazon.com/redshift/latest/dg/t_Troubleshooting_load_errors.html
		//
		String sql = "select distinct tbl, trim(name) as table_name, query, starttime, trim(filename) as input, " +
				"line_number, err_code, trim(err_reason) as reason, trim(raw_line) as line, trim(raw_field_value) as field_value " +
				"from stl_load_errors sl, stv_tbl_perm sp where sl.tbl = sp.id and " +
				"trim(name) = '" + tableNameInLoadErrors + "' ORDER BY starttime DESC LIMIT 64";
		ResultSet rs = stmt.executeQuery(sql);
		logger.error(" -----------------------------------------");
		while (rs.next())
		{
			logger.error("Failed to bulk load " + tableName);
			logger.error(" - Table Name: " + rs.getString("table_name"));
			logger.error(" - Table ID: " + rs.getInt("tbl"));
			logger.error(" - S3 Object: " + rs.getString("input"));
			logger.error(" - Query ID: " + rs.getInt("query"));
			logger.error(" - Start Time: " + rs.getString("starttime"));
			logger.error(" - Line Number: " + rs.getString("line_number"));
			logger.error(" - Error Code: " + rs.getString("err_code"));
			logger.error(" - Reason: " + rs.getString("reason"));
			logger.error(" - Line: " + rs.getString("line"));
			logger.error(" - Field: " + rs.getString("field_value"));
			logger.error(" -----------------------------------------");
		}
	}
	
	/**
	 * return a Redshift COPY command, minus the credentials
	 * @param tableName
	 * @param columns	can be null, in which case the load file and the table definition must match
	 * @param s3Location
	 * @param s3client
	 * @param explicit_ids	if true, add the EXPLICT_IDS option (the file will contain values for IDENTITY columns)
	 * @return
	 */
	public static String getCopyCommand(String tableName, String columns, String s3Location, AmazonS3 s3client, boolean explicit_ids)
	{ 
		// http://docs.aws.amazon.com/redshift/latest/dg/r_COPY.html
		// http://docs.aws.amazon.com/redshift/latest/dg/t_Loading_tables_with_the_COPY_command.html
		// http://docs.aws.amazon.com/redshift/latest/dg/t_Loading-data-from-S3.html
		// http://docs.aws.amazon.com/redshift/latest/dg/t_preparing-input-data.html
		//
		// note that S3 COPY time can be long and for small files that time for S3 setup can be larger than the time to actually do the COPY
		// - https://forums.aws.amazon.com/thread.jspa?threadID=128491&tstart=0
		//
		// note that the S3 bucket must be in the same region as the Redshift cluster
		//
		String baseBulkInsert = "COPY " + tableName
				+ (columns != null ? (" (" + columns + ")") : "")
				+ " FROM '"	+ s3Location + "'" 
				+ " WITH"
				+ " GZIP"	// http://docs.aws.amazon.com/redshift/latest/dg/t_loading-gzip-compressed-data-files-from-S3.html
				+ " DELIMITER AS '|'"
				+ " REMOVEQUOTES"
				+ " DATEFORMAT AS 'YYYY-MM-DD'"	// this is the default
				+ " TIMEFORMAT AS 'YYYY-MM-DD HH24:MI:SS'"	// this is the default
				+ " ACCEPTINVCHARS AS '?'"	// map all bad 4 byte UTF-8 characters to ? 
				+ " ACCEPTANYDATE" 
				+ " IGNOREBLANKLINES"
				+ " ESCAPE"
				+ (explicit_ids ? " EXPLICIT_IDS" : "")
				+ " COMPUPDATE ON"	// calculate the compression encodings, http://docs.aws.amazon.com/redshift/latest/dg/c_Loading_tables_auto_compress.html
				+ (isEncrypted(s3client) ? " ENCRYPTED" : "");	// http://docs.aws.amazon.com/redshift/latest/dg/c_loading-encrypted-files.html
		return baseBulkInsert;
	}
	
	/**
	 * return the CREDENTIALS part of a Redshift COPY command, potentially including the master_symmetric_key
	 * @param s3credentials
	 * @param symKey
	 * @return
	 */
	public static String getCopyCommandCredentials(AWSCredentials s3credentials, SecretKey symKey)
	{
		String masterKey = null;
		if (symKey != null)
			masterKey = new String(Base64.encodeBase64(symKey.getEncoded()));

		String token = null;
		if (s3credentials instanceof BasicSessionCredentials)
			token = ((BasicSessionCredentials) s3credentials).getSessionToken();

		String credentials = " CREDENTIALS AS 'aws_access_key_id=" + s3credentials.getAWSAccessKeyId() 
				+ ";aws_secret_access_key=" + s3credentials.getAWSSecretKey() 
				+ (token != null ? (";token=" + token) : "")
				+ (symKey != null ? (";master_symmetric_key=" + masterKey) : "")
				+ "'";
		return credentials;
	}
	
	/**
	 * return a dummy CREDENTIALS part of a Redshift COPY command, for logging purposes
	 * @param symKey	secret key, used to determine if we show the master_symmetric_key in the log file
	 * @return
	 */
	public static String getCopyCommandDummyCredentials(SecretKey symKey)
	{
		String credentials = " CREDENTIALS AS 'aws_access_key_id=***"
				+ ";aws_secret_access_key=***" 
				+ ";token=***"
				+ (symKey != null ? ";master_symmetric_key=***" : "") 
				+ "'";
		return credentials;
	}
	
	/**
	 * determine if the S3 client is set up for encryption
	 * @param s3client
	 * @return true/false
	 */
	public static boolean isEncrypted(AmazonS3 s3client)
	{
		return s3client instanceof AmazonS3EncryptionClient;
	}

	/**
	 * create an S3 client, encrypted if possible
	 * @param s3credentials (permanent, session, federation)
	 * @param keys list of SecretKey objects, used for returning the encryption key (if it exists) for Redshift COPY
	 * @return Amazon S3 client (potentially encrypted client)
	 * @throws UnsupportedEncodingException
	 * @throws SQLException
	 */
	public static AmazonS3Client getS3Client(AWSCredentials s3credentials, List<SecretKey> keys) throws UnsupportedEncodingException
	{
		AmazonS3Client s3client = null;

		boolean encrypted = (keys != null);
		if (encrypted)
		{
			try
			{
				// generate a one-time use random AES 256 shared key
				// Redshift requires 256 bit AES (http://docs.aws.amazon.com/redshift/latest/mgmt/uploading-aws-sdk-for-java-encrypted.html)
				// and that requires US export agreement (http://www.oracle.com/technetwork/java/javase/downloads/jce-7-download-432124.html)
				KeyGenerator symKeyGenerator = KeyGenerator.getInstance("AES");
				symKeyGenerator.init(256);
				SecretKey symKey = symKeyGenerator.generateKey();

				Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
				cipher.init(Cipher.ENCRYPT_MODE, symKey);
				AlgorithmParameters params = cipher.getParameters();
				@SuppressWarnings("unused")
				byte[] iv = params.getParameterSpec(IvParameterSpec.class).getIV();
				@SuppressWarnings("unused")
				byte[] ciphertext = cipher.doFinal("Test Support for AES-256".getBytes("UTF-8")); // will throw an exception if not supported

				EncryptionMaterials materials = new EncryptionMaterials(symKey);
				if (keys != null)
					keys.add(symKey);
				s3client = new AmazonS3EncryptionClient(s3credentials, materials);
				logger.info("Using encrypted S3 client - all data will be encrypted on the client and kept encrypted in S3");
			}
			catch (InvalidKeyException | NoSuchPaddingException | NoSuchAlgorithmException | InvalidParameterSpecException | IllegalBlockSizeException | BadPaddingException ex)
			{
				logger.warn("S3 encryption for Redshift not supported (requires AES-256, see http://docs.aws.amazon.com/redshift/latest/mgmt/uploading-aws-sdk-for-java-encrypted.html and http://www.oracle.com/technetwork/java/javase/downloads/jce-7-download-432124.html): " + ex.getMessage());
				// AES 256 is not supported, don't encrypt
				encrypted = false;
			}
		}
		if (!encrypted)
		{
			s3client = new AmazonS3Client(s3credentials);
		}
		
		ClientConfiguration config = new ClientConfiguration();
		/*
		logger.debug("getConnectionTimeout() = " + config.getConnectionTimeout());
		logger.debug("getMaxConnections() = " + config.getMaxConnections());
		logger.debug("getMaxErrorRetry() = " + config.getMaxErrorRetry());
		logger.debug("getSocketTimeout() = " + config.getSocketTimeout());
		*/
		// a little aggressive and not recommended, but I was getting some socket timeouts...
		config.setConnectionTimeout(0);
		config.setSocketTimeout(0);
		s3client.setConfiguration(config);
		return s3client;
	}
}