/**
 * $Id: JRBandOverflowException.java,v 1.2 2011-09-15 17:31:53 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */


package com.successmetricsinc.util;

import net.sf.jasperreports.engine.JRException;

/**
 * @author agarrison
 *
 */
public class JRBandOverflowException extends JRException {
	private static final long serialVersionUID = 1L;

	public JRBandOverflowException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

}
