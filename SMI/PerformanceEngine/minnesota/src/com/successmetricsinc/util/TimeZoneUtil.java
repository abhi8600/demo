package com.successmetricsinc.util;

import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

/**
 * @author mpandit
 */
public class TimeZoneUtil {
	
	private static Map<String, String> mapTZ=null;
	
	static
	{
		mapTZ = new HashMap<String, String>();
		mapTZ.put("AUS Central Standard Time","Australia/Darwin");
		mapTZ.put("AUS Eastern Standard Time","Australia/Sydney");
		mapTZ.put("Afghanistan Standard Time","Asia/Kabul");
		mapTZ.put("Alaskan Standard Time","America/Anchorage");
		mapTZ.put("Arab Standard Time","Asia/Riyadh");
		mapTZ.put("Arabian Standard Time","Asia/Dubai");
		mapTZ.put("Arabic Standard Time","Asia/Baghdad");
		mapTZ.put("Argentina Standard Time","America/Buenos_Aires");
		mapTZ.put("Armenian Standard Time","Asia/Yerevan");
		mapTZ.put("Atlantic Standard Time","America/Halifax");
		mapTZ.put("Azerbaijan Standard Time","Asia/Baku");
		mapTZ.put("Azores Standard Time","Atlantic/Azores");
		mapTZ.put("Canada Central Standard Time","America/Regina");
		mapTZ.put("Cape Verde Standard Time","Atlantic/Cape_Verde");
		mapTZ.put("Caucasus Standard Time","Asia/Tbilisi");
		mapTZ.put("Cen. Australia Standard Time","Australia/Adelaide");
		mapTZ.put("Central America Standard Time","America/Guatemala");
		mapTZ.put("Central Asia Standard Time","Asia/Dhaka");
		mapTZ.put("Central Brazilian Standard Time","America/Manaus");
		mapTZ.put("Central Europe Standard Time","Europe/Budapest");
		mapTZ.put("Central European Standard Time","Europe/Warsaw");
		mapTZ.put("Central Pacific Standard Time","Pacific/Guadalcanal");
		mapTZ.put("Central Standard Time","America/Chicago");
		mapTZ.put("Central Standard Time (Mexico)","America/Mexico_City");
		mapTZ.put("China Standard Time","Asia/Shanghai");
		mapTZ.put("Dateline Standard Time","Etc/GMT+12");
		mapTZ.put("E. Africa Standard Time","Africa/Nairobi");
		mapTZ.put("E. Australia Standard Time","Australia/Brisbane");
		mapTZ.put("E. Europe Standard Time","Europe/Minsk");
		mapTZ.put("E. South America Standard Time","America/Sao_Paulo");
		mapTZ.put("Eastern Standard Time","America/New_York");
		mapTZ.put("Egypt Standard Time","Africa/Cairo");
		mapTZ.put("Ekaterinburg Standard Time","Asia/Yekaterinburg");
		mapTZ.put("FLE Standard Time","Europe/Kiev");
		mapTZ.put("Fiji Standard Time","Pacific/Fiji");
		mapTZ.put("GMT Standard Time","Europe/London");
		mapTZ.put("GTB Standard Time","Europe/Istanbul");
		mapTZ.put("Georgian Standard Time","Etc/GMT-3");
		mapTZ.put("Greenland Standard Time","America/Godthab");
		mapTZ.put("Greenwich Standard Time","Africa/Casablanca");
		mapTZ.put("Hawaiian Standard Time","Pacific/Honolulu");
		mapTZ.put("India Standard Time","Asia/Calcutta");
		mapTZ.put("Iran Standard Time","Asia/Tehran");
		mapTZ.put("Israel Standard Time","Asia/Jerusalem");
		mapTZ.put("Jordan Standard Time","Asia/Amman");
		mapTZ.put("Korea Standard Time","Asia/Seoul");
		mapTZ.put("Mexico Standard Time","America/Mexico_City");
		mapTZ.put("Mexico Standard Time 2","America/Chihuahua");
		mapTZ.put("Mid-Atlantic Standard Time","Atlantic/South_Georgia");
		mapTZ.put("Middle East Standard Time","Asia/Beirut");
		mapTZ.put("Montevideo Standard Time","America/Montevideo");
		mapTZ.put("Mountain Standard Time","America/Denver");
		mapTZ.put("Mountain Standard Time (Mexico)","America/Chihuahua");
		mapTZ.put("Myanmar Standard Time","Asia/Rangoon");
		mapTZ.put("N. Central Asia Standard Time","Asia/Novosibirsk");
		mapTZ.put("Namibia Standard Time","Africa/Windhoek");
		mapTZ.put("Nepal Standard Time","Asia/Katmandu");
		mapTZ.put("New Zealand Standard Time","Pacific/Auckland");
		mapTZ.put("Newfoundland Standard Time","America/St_Johns");
		mapTZ.put("North Asia East Standard Time","Asia/Irkutsk");
		mapTZ.put("North Asia Standard Time","Asia/Krasnoyarsk");
		mapTZ.put("Pacific SA Standard Time","America/Santiago");
		mapTZ.put("Pacific Standard Time","America/Los_Angeles");
		mapTZ.put("Pacific Standard Time (Mexico)","America/Tijuana");
		mapTZ.put("Romance Standard Time","Europe/Paris");
		mapTZ.put("Russian Standard Time","Europe/Moscow");
		mapTZ.put("SA Eastern Standard Time","Etc/GMT+3");
		mapTZ.put("SA Pacific Standard Time","America/Bogota");
		mapTZ.put("SA Western Standard Time","America/La_Paz");
		mapTZ.put("SE Asia Standard Time","Asia/Bangkok");
		mapTZ.put("Samoa Standard Time","Pacific/Apia");
		mapTZ.put("Singapore Standard Time","Asia/Singapore");
		mapTZ.put("South Africa Standard Time","Africa/Johannesburg");
		mapTZ.put("Sri Lanka Standard Time","Asia/Colombo");
		mapTZ.put("Taipei Standard Time","Asia/Taipei");
		mapTZ.put("Tasmania Standard Time","Australia/Hobart");
		mapTZ.put("Tokyo Standard Time","Asia/Tokyo");
		mapTZ.put("Tonga Standard Time","Pacific/Tongatapu");
		mapTZ.put("US Eastern Standard Time","Etc/GMT+5");
		mapTZ.put("US Mountain Standard Time","America/Phoenix");
		mapTZ.put("Venezuela Standard Time","America/Caracas");
		mapTZ.put("Vladivostok Standard Time","Asia/Vladivostok");
		mapTZ.put("W. Australia Standard Time","Australia/Perth");
		mapTZ.put("W. Central Africa Standard Time","Africa/Lagos");
		mapTZ.put("W. Europe Standard Time","Europe/Berlin");
		mapTZ.put("West Asia Standard Time","Asia/Karachi");
		mapTZ.put("West Pacific Standard Time","Pacific/Port_Moresby");
		mapTZ.put("Yakutsk Standard Time","Asia/Yakutsk");
	}
	
	/**
	 * @param windowsTZID as is stored on Repository
	 * @return Java TimeZone object
	 */
	public static TimeZone getTimeZoneForWindowsTZID(String windowsTZID)
	{
		// TimeZone.getTimeZone will NPE if it gets a null value
		if (windowsTZID == null)
			return null;
		String tz = mapTZ.get(windowsTZID);
		if (tz == null)
			return null;
		return TimeZone.getTimeZone(tz);
	}
	
}
