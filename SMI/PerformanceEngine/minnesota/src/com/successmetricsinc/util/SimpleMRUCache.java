package com.successmetricsinc.util;

import java.util.LinkedHashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JasperPrint;
import org.apache.log4j.Logger;

public class SimpleMRUCache<K,V> extends LinkedHashMap<K,V>
{
	private static Logger logger = Logger.getLogger(SimpleMRUCache.class);
	private static final long serialVersionUID = 1L;
	private int maxSize;

	private static final long[] byteTable;
	private static final long HSTART = 0xBB40E64DA205B064L;
	private static final long HMULT = 7664345821815920749L;

	static {
		byteTable = new long[256];
		long h = 0x544B2FBACAAF1684L;
		for (int i = 0; i < 256; i++) {
			for (int j = 0; j < 31; j++) {
				h = (h >>> 7) ^ h;
				h = (h << 11) ^ h;
				h = (h >>> 10) ^ h;
			}
			byteTable[i] = h;
		}
	}

	public SimpleMRUCache(int max)
	{
		super();
		maxSize = max;
	}

	@SuppressWarnings("rawtypes")
	protected boolean removeEldestEntry(Map.Entry eldest)
	{
		return size() > maxSize;
	}

	/*
	 * http://www.java-forums.org/new-java/17026-i-need-32-64-bit-hash-function.html
	 * 
	 * Java implementation of the Numerical Recipies 64 bit hash
	 */
	public static long hash(byte[] data) {
		long h = HSTART;
		final long hmult = HMULT;
		final long[] ht = byteTable;
		for (int len = data.length, i = 0; i < len; i++) {
			h = (h * hmult) ^ ht[data[i] & 0xff];
		}
		return h;
	}
}