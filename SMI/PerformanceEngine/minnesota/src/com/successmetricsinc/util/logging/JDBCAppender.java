
package com.successmetricsinc.util.logging;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCAppender extends org.apache.log4j.jdbc.JDBCAppender
{
	/**
	 * Override this to link with your connection pooling system.
	 *
	 * By default this creates a single connection which is held open
	 * until the object is garbage collected.
	 */
	protected Connection getConnection() throws SQLException
	{	
		if (connection == null || !connection.isValid(5))
		{
			connection = DriverManager.getConnection(databaseURL, databaseUser, databasePassword);
		}
		return connection;
	}
}

