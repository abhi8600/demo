package com.successmetricsinc.util.logging;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.Appender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;

public class LogUtil
{
	public static String getLogfileName()
	{
		String filename = null;
		Logger logger = Logger.getRootLogger();
		Appender app = logger.getAppender("logfile"); // appender for normal logging
		if (app instanceof FileAppender)
		{
			filename = ((FileAppender) app).getFile();
			if (filename != null)
			{
				File f = new File(filename);
				try {
					filename = f.getCanonicalPath();
				} catch (IOException e) {
					logger.debug(e, e);
				}
			}
		}
		return filename;
	}
}
	