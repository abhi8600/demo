
package com.successmetricsinc.util.logging;

import java.util.Properties;

import org.apache.log4j.Appender;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;

import com.successmetricsinc.security.EncryptionService;

/**
 * audit logging (usage tracking)
 * - see log4j.xml 
 */
public class AuditLog
{
	private static Logger logger = Logger.getLogger(AuditLog.class);
	private static Logger audit = Logger.getLogger("audit");
	private static boolean init = false;

	/**
	 * create a usage tracking log entry
	 * 
	 * @param mode	String - designer, dashboard, export, bulk, scheduled
	 * @param name	String - name of the report
	 * @param sessionid	String - id of the current session
	 * @param elapsedTime	long - elapsed time in milliseconds for the report execution (server-side only)
	 */
	public static void log(String mode, String name, String sessionid, long elapsedTime)
	{
		log(mode, name, null, null, sessionid, elapsedTime);
	}

	/**
	 * create a usage tracking log entry
	 * 
	 * @param mode	String - designer, dashboard, export, bulk, scheduled
	 * @param name	String - name of the report
	 * @param dashboard	String - name of the dashboard, can be null
	 * @param guid	String - unique ID for a specific invocation of a dashboard (to associate all reports on a dashboard)
	 * @param sessionid	String - id of the current session
	 * @param elapsedTime	long - elapsed time in milliseconds for the report execution (server-side only)
	 */
	public static void log(String mode, String name, String dashboard, String guid, String sessionid, long elapsedTime)
	{
		try
		{
			if (audit == null)
				return;

			// skip bogus calls - generally for not yet saved adhoc reports
			if (mode == null || name == null)
				return;

			if (!init)
			{
				// decrypt the username and password
				Appender app = audit.getAppender("auditdb");
				if (app instanceof JDBCAppender)
				{
					EncryptionService es = EncryptionService.getInstance();
					JDBCAppender jdbcapp = (JDBCAppender) app;
					String url = jdbcapp.getURL();

					// decrypt the user and password
					String username =  es.decrypt(jdbcapp.getUser());
					jdbcapp.setUser(username);
					String password =  es.decrypt(jdbcapp.getPassword());
					jdbcapp.setPassword(password);
					Properties p = new Properties();
					p.setProperty("user", username);
					if (password != null && !password.isEmpty())
						p.setProperty("password", password);
					init = true;
				}
			}

			if (!init)
				return;

			// JDBC appender is very simple, need to escape single quotes if they are in the values

			MDC.put("mode", quote(mode));
			MDC.put("elapsedTime", Long.toString(elapsedTime));

			// dashboard and guid are not quoted in the log4j.xml pattern so that 'null' can be passed, need to quote here
			if (dashboard != null)
				MDC.put("dashboard", "'" + quote(dashboard) + "'");
			else
				MDC.put("dashboard", "null");
			if (guid != null)
				MDC.put("guid", "'" + quote(guid) + "'");
			else
				MDC.put("guid", "null");

			if (sessionid != null)
				MDC.put("sessionid", "'" + quote(sessionid) + "'");
			else
				MDC.put("sessionid", "null");

			// clean up report name
			name = name.replace('\\', '/');  // clean up path
			int catalogIndex = name.indexOf("catalog/");
			if (catalogIndex > 0)
				name = name.substring(catalogIndex + 8); // strip off anything before catalog/
			if (name.endsWith(".AdhocReport"))
				name = name.substring(0, name.length() - ".AdhocReport".length());
			// remove leading slash
			if (name.startsWith("/"))
				name = name.substring(1);
			MDC.put("name", quote(name));

			audit.info(""); // currently not using the 'message' - %m in the pattern language
		}
		catch (Exception ex)
		{
			logger.warn("Audit logging failure: " + ex.getMessage());
		}
		finally
		{
			reset(false); // get rid of the MDC settings made during this method
		}
	}	
	
	/**
	 * quote strings for MS SQL
	 * @param str
	 * @return
	 */
	public static String quote(String str)
	{
		if (str == null)
			return null;
		return str.replace("'", "''");
	}

	/**
	 * clear out the MDC
	 * @param all	boolean - true means clear out all values including username and spaceID
	 */
	public static void reset(boolean all)
	{
		if (all)
		{
			MDC.remove("username");
			MDC.remove("spaceID");
		}
		MDC.remove("mode");
		MDC.remove("name");
		MDC.remove("elapsedTime");
		MDC.remove("guid");
		MDC.remove("dashboard");
		MDC.remove("sessionid");
	}
}
