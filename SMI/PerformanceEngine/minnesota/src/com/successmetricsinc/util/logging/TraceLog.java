package com.successmetricsinc.util.logging;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import com.successmetricsinc.util.Util;

public class TraceLog
{
	private static Logger logger = Logger.getLogger(TraceLog.class);
	private static String newline = System.getProperty("line.separator");
	
	private int LOG_LIMIT = 100000; // max # of lines to log per run

	private boolean enabled = false;
	private String logFileName = null;
	private FileWriter writer = null;
	private BufferedWriter out = null;
	private int lineCount = 0;
	
	public TraceLog(String path, int limit)
	{
		path = path + "/logs";
		Util.createDirectory(path);
		logFileName = path + "/trace.log";
		try
		{
			writer = new FileWriter(logFileName);
		}
		catch (Exception e)
		{
			logger.debug(e, e);
		}
		finally
		{
			if (writer != null)
				try {
					writer.close();
				} catch (IOException e) {
				}
		}
		if (limit > LOG_LIMIT)
			LOG_LIMIT = limit;
	}
	
	public void enable()
	{
		try {
			writer = new FileWriter(logFileName, true);
			out = new BufferedWriter(writer);
			enabled = true;
		} catch (IOException e) {
			logger.debug(e, e);
		}
	}
	
	public void disable()
	{
		if (!enabled)
			return;
		try {
			if (out != null)
				out.close();
			if (writer != null)
				writer.close();
		} catch (IOException e) {
			logger.debug(e, e);
		}
		enabled = false;
		out = null;
		writer = null;
	}

	public void log(String message)
	{
		if (enabled)
		{
			if (lineCount > LOG_LIMIT || out == null)
			{
				disable();
				return;
			}
			try {
				Date now = new Date();
		        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		        out.write(sdf.format(now));
				out.write(" - ");
				out.write(message);
				out.write(newline);
			} catch (IOException e) {
				logger.debug(e, e);
			}
			lineCount++;
		}
	}
}