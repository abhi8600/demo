/**
 * $Id: UnterminatedQuoteException.java,v 1.1 2011-06-14 00:50:17 ricks Exp $
 *
 * Copyright (C) 2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.util;

public class UnterminatedQuoteException extends BaseException
{
	private static final long serialVersionUID = 1L;

	public UnterminatedQuoteException(String s)
	{
		super(s);
	}
	
	public UnterminatedQuoteException(String s, Throwable t)
	{
		super(s, t);
	}

	public int getErrorCode()
	{
		return ERROR_UNTERMINATED_QUOTE;
	}
}