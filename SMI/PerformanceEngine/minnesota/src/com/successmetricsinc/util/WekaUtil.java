/**
 * $Id: WekaUtil.java,v 1.3 2010-10-27 23:00:33 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.util;

/**
 * All utility methods that refer to weka library should be placed here. Util.java class is used by web application and we do not
 * deploy weka.jar on web application. So, if Util.java has a method referring to a weka class, the web application will throw a
 * class not found exception for the wekaclass. To avoid such exceptions, we have created this class that should never be referred 
 * from the web application. 
 * @author Manish Jani
 *
 */
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import cern.jet.stat.Probability;

import com.successmetricsinc.engine.SuccessModelInstance;

import weka.core.Instances;
import weka.core.converters.ArffSaver;

public class WekaUtil
{
	private static Logger logger = Logger.getLogger(WekaUtil.class);
	/**
	 * Write a dataset to an ARFF file
	 * 
	 * @param i
	 *            Instances to use
	 * @param name
	 *            Name of file
	 * @param comment
	 *            Comment
	 */
	public static void writeArff(Instances i, String name, String comment)
	{
		try
		{
			logger.info("Writing ARFF file " + name + ".arff");
			ArffSaver as = new ArffSaver();
			as.setInstances(i);
			as.resetWriter();
			File f = new File(name + ".arff");
			as.setFile(f);
			BufferedWriter w = as.getWriter();
			if (comment != null)
				w.write("% " + comment + "\n");
			as.writeBatch();
		}
		catch (Exception e)
		{
			// a modeling run should never fail due to not being able to write the ARFF file
			logger.error(e.getMessage(), e);
		}
	}

	/**
	 * Write a dataset to an ARFF file
	 * 
	 * @param i
	 *            Instances to use
	 * @param name
	 *            Name of file
	 * @param comment
	 *            Comment
	 * @param count
	 *            Number to write
	 */
	public static void writeArff(Instances i, String name, String comment, int count)
	{
		try
		{
			if (count < i.numInstances())
			{
				i = new Instances(i, 0, count);
			}
			writeArff(i, name, comment);
		}
		catch (Exception e)
		{
			// a modeling run should never fail due to not being able to write the ARFF file
			logger.error(e.getMessage(), e);
		}
	}

	/**
	 * Filter to sample without replacement
	 * 
	 * @param dataSet
	 * @param samplePct
	 * @param seed
	 * @return
	 */
	public static Instances sampleWithoutReplacement(Instances dataSet, double samplePct, long seed)
	{
		samplePct = Math.max(0, Math.min(100, samplePct));
		int sampleSize = (int) ((samplePct / 100) * dataSet.numInstances());
		Instances newSample = new Instances(dataSet, sampleSize);
		List<Integer> list = new ArrayList<Integer>();
		for (int i = 0; i < dataSet.numInstances(); i++)
			list.add(Integer.valueOf(i));
		Random r = new Random(seed);
		for (int i = 0; i < sampleSize; i++)
		{
			if (list.size() > 0)
			{
				int index = r.nextInt(list.size());
				int rowIndex = ((Integer) list.get(index)).intValue();
				list.remove(index);
				newSample.add(dataSet.instance(rowIndex));
			}
		}
		return (newSample);
	}

	/**
	 * Stream a success model from disk
	 * 
	 * @param path
	 * @param curModel
	 * @param retrieveMDS
	 * @return
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static SuccessModelInstance retrieveSMI(String path, String curModel, boolean retrieveMDS) throws FileNotFoundException, IOException,
			ClassNotFoundException
	{
		String fname = path + File.separator + curModel + ".mod";
		logger.info("Reading file " + fname);
		File f = new File(fname);
		if (!f.exists())
			throw (new FileNotFoundException());
		FileInputStream in = new FileInputStream(fname);
		ObjectInputStream s = new ObjectInputStream(in);
		SuccessModelInstance smi = (SuccessModelInstance) s.readObject();
		in.close();
		smi.setInstanceDir(path);
		if (retrieveMDS)
			smi.retrieveMDS();
		return (smi);
	}

	/**
	 * Stream a success model to disk
	 * 
	 * @param smi
	 * @return
	 * @throws IOException
	 */
	public static String saveSMI(SuccessModelInstance smi) throws IOException
	{
		String fileName = smi.getFileName();
		File file = new File(fileName);
		File directory = file.getParentFile();
		File tempFile = File.createTempFile("modelDataFile", null, directory);
		logger.info("Writing model to temporary file " + tempFile.getName());
		FileOutputStream fo = new FileOutputStream(tempFile);
		ObjectOutput s = new ObjectOutputStream(fo);
		s.writeObject(smi);
		s.close();
		fo.close();
		logger.info("Renaming temporary file to " + fileName);
		File f2 = new File(fileName);
		Util.secureDelete(f2);
		if (tempFile.renameTo(f2))
		{
			Util.secureDelete(tempFile);
		} else
		{
			// logger.error("Rename of temporary file failed, keeping temporary file around");
		}
		return (f2.getName());
	}

	/**
	 * Calculate the probability that two distributions are different. Uses the Chi-square test for probability two
	 * distributes are different.
	 * 
	 * @param row1
	 * @param row2
	 * @return
	 */
	public static double calcDiff(double[] row1, double[] row2)
	{
		int count = row1.length;
		// First, calculate proportions
		double[] prop1 = new double[count];
		double[] prop2 = new double[count];
		double total1 = 0, total2 = 0;
		for (int i = 0; i < count; i++)
		{
			total1 += WekaUtil.propVal(row1[i]);
			total2 += WekaUtil.propVal(row2[i]);
		}
		for (int i = 0; i < count; i++)
		{
			prop1[i] = WekaUtil.propVal(row1[i] / total1);
			prop2[i] = WekaUtil.propVal(row2[i] / total2);
		}
		// Now, calculate the Chi-square statistic
		double chiStat = 0;
		int numDegrees = 0;
		for (int i = 0; i < count; i++)
		{
			// Can't include 0 proportion values in ChiSq statistic
			if (prop1[i] != 0)
			{
				double val = (prop2[i] - prop1[i]);
				chiStat += val * val / prop1[i];
				numDegrees++;
			}
		}
		double chiVal = Probability.chiSquare((double) (numDegrees - 1), chiStat);
		return (chiVal);
	}

	/**
	 * Process a double to return zero if it is NaN
	 * 
	 * @param val
	 * @return
	 */
	public static double propVal(double val)
	{
		return (Double.isNaN(val) ? 0 : val);
	}

	/**
	 * remove the modeling files in the directory (*.mod, *.dat, *.arff)
	 * @param directoryName
	 */
	public static void removeModelingFiles(String directoryName)
	{
		logger.info("removing files in directory: " + directoryName);
		File root = new File(directoryName);
		File[] allChildren = root.listFiles();
		for (int i = 0; i < allChildren.length; ++i)
		{
			String lcName = allChildren[i].getName().toLowerCase();
			if (lcName.endsWith(".mod") || lcName.endsWith(".dat") || lcName.endsWith(".arff"))
			{
				logger.trace("removing: " + allChildren[i].getName());
				try
				{
					Util.secureDelete(allChildren[i]);
				} catch (IOException e)
				{
					logger.warn("Failed to delete: " + allChildren[i].getAbsolutePath(), e);
				}
			}
		}
	}

	/**
	 * Convert a list of generic objects into a list of strings (useful for creating lists of keys)
	 * 
	 * @param list
	 * @return
	 */
	public static List<String> getListofStrings(List<Object> list)
	{
		List<String> newList = new ArrayList<String>(list.size());
		for (Object o : list)
			newList.add(o.toString());
		return (newList);
	}

	/**
	 * Get a list of IDs from a text file
	 * 
	 * @param filename
	 * @return
	 * @throws Exception
	 */
	public static List<String> getIDList(String filename) throws Exception
	{
		FileInputStream stream = new FileInputStream(filename);
		if (stream != null)
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null)
			{
				sb.append(line);
				sb.append(',');
			}
			reader.close();
			stream.close();
			StringTokenizer st = new StringTokenizer(sb.toString(), ",");
			List<String> list = new ArrayList<String>();
			while (st.hasMoreTokens())
			{
				list.add(st.nextToken());
			}
			return (list);
		}
		return (null);
	}
}
