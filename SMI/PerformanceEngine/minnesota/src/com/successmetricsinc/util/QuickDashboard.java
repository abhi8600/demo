/**
 * $Id: QuickDashboard.java,v 1.9 2011-09-24 01:11:00 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */


package com.successmetricsinc.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.query.QueryFilter;


/**
 * @author agarrison
 *
 */
public class QuickDashboard implements Serializable {
	private static final long serialVersionUID = 1L;
    public static int PROMPT_TYPE_DIMENSION_COLUMN = 0;
    
    public static int QUICK_REPORT_HEIGHT = 300;

	/**
	 * @author agarrison
	 *
	 */
	public static class QuickPrompt implements Serializable {
		private static final long serialVersionUID = 1L;
        public int Type;
        public QuickReportDimension DimensionColumn;
        public String VisibleName;
        
		public QuickPrompt(int type,
				QuickReportDimension dimensionColumn, String visibleName) {
			super();
			Type = type;
			DimensionColumn = dimensionColumn;
			if (visibleName == null || !Util.hasNonWhiteSpaceCharacters(visibleName)) {
				visibleName = dimensionColumn.getDimensionName();
			}
			VisibleName = visibleName;
		}
		
		public Element getQuickPromptElement(Namespace ns)
		{
			Element e = new Element("QuickPrompt",ns);
			
			Element child = new Element("Type",ns);
			child.setText(String.valueOf(Type));
			e.addContent(child);
			
			e.addContent(DimensionColumn.getQuickReportDimensionElement("DimensionColumn", ns));
			
			child = new Element("VisibleName",ns);
			child.setText(VisibleName);
			e.addContent(child);
					
			return e;
		}
		
		public int getType() {
			return Type;
		}
		public void setType(int type) {
			Type = type;
		}
		public QuickReportDimension getDimensionColumn() {
			return DimensionColumn;
		}
		public void setDimensionColumn(QuickReportDimension dimensionColumn) {
			DimensionColumn = dimensionColumn;
		}
		public String getVisibleName() {
			return VisibleName;
		}
		public void setVisibleName(String visibleName) {
			VisibleName = visibleName;
		}

	}
	private int Type;
	private List<QuickReport> Reports = new ArrayList<QuickReport>();
    private String dashboardName;
    private String pageName;
    private List<QuickPrompt> Prompts = new ArrayList<QuickPrompt>();
    private QueryFilter filter;

    @SuppressWarnings({ "unchecked", "rawtypes" })
	public QuickDashboard(Element parent, Namespace ns) {
    	Type = XmlUtils.getIntContent(parent,"Type",ns);
    	dashboardName = XmlUtils.getStringContent(parent, "DashboardName", ns);
		pageName = XmlUtils.getStringContent(parent, "PageName", ns);
		Element f = parent.getChild("Filter", ns);
		if (f != null) {
			filter = new QueryFilter(f, ns);
		}
		List reports = parent.getChildren("Reports", ns);
		for (Iterator<Element> reportIter = reports.iterator(); reportIter.hasNext(); ) {
			Element Report = reportIter.next();
			List qrs = Report.getChildren("QuickReport", ns);
			for (Iterator<Element> qrIter = qrs.iterator(); qrIter.hasNext(); ) {
				QuickReport report = new QuickReport(qrIter.next(), ns);
				Reports.add(report);
			}
		}
    	
		List prompts = parent.getChildren("Prompts", ns);
		for (Iterator<Element> promptsIter = prompts.iterator(); promptsIter.hasNext(); ) {
			Element qpEl = promptsIter.next();
			List<Element> qps = qpEl.getChildren("QuickPrompt", ns);
			for (Iterator<Element> qpIter = qps.iterator(); qpIter.hasNext(); ) {
				Element qp = qpIter.next();
				int type = XmlUtils.getIntContent(qp, "Type", ns);
				String visibleName = XmlUtils.getStringContent(qp, "VisibleName", ns);
				Element dimCol = qp.getChild("DimensionColumn", ns);
				QuickReportDimension dimension = new QuickReportDimension(dimCol, ns);
				QuickPrompt prompt = new QuickPrompt(type, dimension, visibleName);
				this.Prompts.add(prompt);
			}
		}
    }
	public List<QuickReport> getReports() {
		return Reports;
	}

	public void setReports(List<QuickReport> reports) {
		Reports = reports;
	}
	public String getDashboardName() {
		return dashboardName;
	}
	public void setDashboardName(String dashboardName) {
		this.dashboardName = dashboardName;
	}
	public String getPageName() {
		return pageName;
	}
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}
	public List<QuickPrompt> getPrompts() {
		return Prompts;
	}
	public void setPrompts(List<QuickPrompt> prompts) {
		Prompts = prompts;
	}
	public QueryFilter getFilter() {
		return filter;
	}
	public void setFilter(QueryFilter filters) {
		this.filter = filters;
	}
	
	public Element getQuickDashboardElement(Namespace ns)
	{
    	Element e = new Element("QuickDashboard",ns);
    	
    	Element child = new Element("Type",ns);
		child.setText(String.valueOf(Type));
		e.addContent(child);
		
		child = new Element("DashboardName",ns);
		child.setText(dashboardName);
		e.addContent(child);
		
		child = new Element("PageName",ns);
		child.setText(pageName);
		e.addContent(child);
		
		if (Prompts!=null && !Prompts.isEmpty())
		{
			child = new Element("Prompts",ns);
			for (QuickPrompt quickPrompt : Prompts)
			{
				child.addContent(quickPrompt.getQuickPromptElement(ns));
			}
			e.addContent(child);
		}
		
		if (Reports!=null && !Reports.isEmpty())
		{
			child = new Element("Reports",ns);
			for (QuickReport quickReport : Reports)
			{
				child.addContent(quickReport.getQuickReportElement(ns));
			}
			e.addContent(child);
		}
		
		if (filter!=null)
		{
			e.addContent(filter.getQueryFilterElement("Filter", ns));
		}
		
    	return e;    
	}

//    private QueryFilter Filter;


}
