package com.successmetricsinc.util;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;

public class Emailer {

	private static final Logger logger = Logger.getLogger(Emailer.class);
	
	String mailHost;
	String to;
	String from;
	
	public Emailer(String mailHost, String from, String to)
	{	
		this.mailHost = mailHost;
		this.from = from;
		this.to = to;
	}
	
	public void sendEmail(String subject, String bodyText)
    {
		try
		{
			String host = null;
			String port = null;
			String array[] = mailHost.split(":");
			host = array[0];
			if(array.length == 2)
			{
				port = array[1];
			}
			
			Properties props = new Properties();
			props.put("mail.smtp.host", host);
			if(port != null)
			{
				props.put("mail.smtp.port", port);
			}
			javax.mail.Session mailSession = javax.mail.Session.getInstance(props, null);
			
			Message msg = new MimeMessage(mailSession);
			InternetAddress addressFrom = new InternetAddress(from);
			msg.setFrom(addressFrom);
			msg.setReplyTo(new InternetAddress[]
			{ addressFrom });
			String[] recipients = to.split(";");
			InternetAddress[] addressTo = new InternetAddress[recipients.length];
			for (int i = 0; i < recipients.length; i++)
			{
				addressTo[i] = new InternetAddress(recipients[i]);
			}
			msg.setRecipients(Message.RecipientType.TO, addressTo);
			msg.setSubject(subject);
			msg.setSentDate(new Date());
			MimeMultipart multipart = new MimeMultipart();
			MimeBodyPart bodyPart = new MimeBodyPart();
			bodyPart.setContent(bodyText, "text/html");
			multipart.addBodyPart(bodyPart);
			
			msg.setContent(multipart);
			Transport.send(msg);
		} catch (Exception ex)
		{
			logger.error(ex.getMessage(), ex);
		}
    }
}
