package com.successmetricsinc.util;

import java.io.Serializable;

import org.jdom.Element;
import org.jdom.Namespace;

/**
 * @author agarrison
 *
 */
public class QuickReportDimension implements Serializable {
	private static final long serialVersionUID = 1L;
    private String DimensionName;
    private String ColumnName;
    private int OrderBy;
    
    public QuickReportDimension(Element parent, Namespace ns) {
    	DimensionName = XmlUtils.getStringContent(parent, "DimensionName", ns);
    	ColumnName = XmlUtils.getStringContent(parent, "ColumnName", ns);
    	OrderBy = XmlUtils.getIntContent(parent, "OrderBy", ns);
    }
    
    public Element getQuickReportDimensionElement(String elementName,Namespace ns)
	{
		Element e = new Element(elementName,ns);
		
		Element child = new Element("DimensionName",ns);
		child.setText(DimensionName);
		e.addContent(child);
		
		child = new Element("ColumnName",ns);
		child.setText(ColumnName);
		e.addContent(child);
		
		child = new Element("OrderBy",ns);
		child.setText(String.valueOf(OrderBy));
		e.addContent(child);
				
		return e;
	}
    
    public QuickReportDimension(String name, String column, int orderby) {
    	this.DimensionName = name;
    	this.ColumnName = column;
    	this.OrderBy = orderby;
    }
	public String getDimensionName() {
		return DimensionName;
	}
	public void setDimensionName(String dimensionName) {
		DimensionName = dimensionName;
	}
	public String getColumnName() {
		return ColumnName;
	}
	public void setColumnName(String columnName) {
		ColumnName = columnName;
	}
	public int getOrderBy() {
		return OrderBy;
	}
	public void setOrderBy(int orderBy) {
		OrderBy = orderBy;
	}

	public boolean equals(Object obj) {
		
		if (obj == null || !QuickReportDimension.class.isInstance(obj))
			return false;
		QuickReportDimension d = (QuickReportDimension) obj;
		boolean ret = true;
		if (DimensionName != null) {
			ret = DimensionName.equals(d.getDimensionName());
		}
		else if (d.getDimensionName() != null) {
			ret = false;
		}
		
		if (ret == true && ColumnName != null) {
			ret = ColumnName.equals(d.getColumnName());
		}
		else if (d.ColumnName != null) {
			ret = false;
		}
		
		return ret;
	}

}