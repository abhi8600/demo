package com.successmetricsinc.util;

public class TimeCreationException extends BaseException {
	
	private static final long serialVersionUID = 1L;
	private int errorCode = -1;

	public TimeCreationException()
	{
		super();
	}
	
	public TimeCreationException(String s)
	{
		super(s);
	}
	
	public TimeCreationException(String s, int errorCode)
	{
		super(s);
		this.errorCode = errorCode;
	}
	
	
	public TimeCreationException(String s, Throwable cause)
	{
		super(s, cause);
	}

	public int getErrorCode()
	{
		return errorCode == -1 ? ERROR_TIME_CREATION_EXCEPTION : errorCode;
	}
}
