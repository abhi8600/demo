/**
 * $Id: QuickReport.java,v 1.6 2011-09-06 21:45:47 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */


package com.successmetricsinc.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.query.DisplayFilter;

/**
 * @author agarrison
 *
 */
public class QuickReport implements Serializable {
    /**
	 * @author agarrison
	 *
	 */
	private static final long serialVersionUID = 1L;
	public static class QuickReportExpression implements Serializable {
		private static final long serialVersionUID = 1L;
		public String Formula;
		public String Label;
		public int OrderBy;
		
		public QuickReportExpression(String formula, String label, int orderBy) {
			super();
			Formula = formula;
			Label = label;
			OrderBy = orderBy;
		}
		public String getFormula() {
			return Formula;
		}
		public void setFormula(String formula) {
			Formula = formula;
		}
		public String getLabel() {
			return Label;
		}
		public void setLabel(String label) {
			Label = label;
		}
		public int getOrderBy() {
			return OrderBy;
		}
		public void setOrderBy(int orderBy) {
			OrderBy = orderBy;
		}

		public Element getQuickReportExpression(Namespace ns)
		{
			Element e = new Element("QuickReportExpression",ns);
			
			Element child = new Element("Formula",ns);
			child.setText(Formula);
			e.addContent(child);
			
			child = new Element("Label",ns);
			child.setText(Label);
			e.addContent(child);
			
			child = new Element("OrderBy",ns);
			child.setText(String.valueOf(OrderBy));
			e.addContent(child);
					
			return e;
		}

	}


	/**
	 * @author agarrison
	 *
	 */
	public static class QuickReportMeasure implements Serializable {
		private static final long serialVersionUID = 1L;
		private String MeasureName;
		private String MeasureLabel;
        private int OrderBy;
        
        private String BaseMeasure;
        private String AggregationRule;
        private String TimePrefix;
        
        public QuickReportMeasure(String name, String label, int orderby) {
        	this.MeasureName = name;
        	this.MeasureLabel = label;
        	this.OrderBy = orderby;
        }
        
        public QuickReportMeasure(String name, String label, int orderby,String BaseMeasure,String AggregationRule,String TimePrefix) {
        	this.MeasureName = name;
        	this.MeasureLabel = label;
        	this.OrderBy = orderby;
        	this.BaseMeasure = BaseMeasure;
        	this.AggregationRule = AggregationRule;
        	this.TimePrefix = TimePrefix;
        }
        
		public String getMeasureName() {
			return MeasureName;
		}
		public void setMeasureName(String measureName) {
			MeasureName = measureName;
		}
		public String getMeasureLabel() {
			return MeasureLabel;
		}
		public void setMeasureLabel(String measureLabel) {
			MeasureLabel = measureLabel;
		}
		public int getOrderBy() {
			return OrderBy;
		}
		public void setOrderBy(int orderBy) {
			OrderBy = orderBy;
		}
		
		public Element getQuickReportMeasureElement(Namespace ns)
		{
			Element e = new Element("QuickReportMeasure",ns);
			
			Element child = new Element("MeasureName",ns);
			child.setText(MeasureName);
			e.addContent(child);
			
			child = new Element("MeasureLabel",ns);
			child.setText(MeasureLabel);
			e.addContent(child);
			
			child = new Element("OrderBy",ns);
			child.setText(String.valueOf(OrderBy));
			e.addContent(child);
			
			child = new Element("BaseMeasure",ns);
			child.setText(BaseMeasure);
			e.addContent(child);
			
			child = new Element("AggregationRule",ns);
			child.setText(AggregationRule);
			e.addContent(child);
			
			if (TimePrefix!=null)
			{
				child = new Element("TimePrefix",ns);
				child.setText(TimePrefix);
				e.addContent(child);
			}
					
			return e;
		}

	}
	
	
	public static final int TYPE_MEASURE_BY_DIMENSION = 0;

    public static final int SORT_NONE = 0;
    public static final int SORT_ASC = 1;
    public static final int SORT_DESC = 2;

    private String chartType = "None";
    private String title = null;
    private int Type = TYPE_MEASURE_BY_DIMENSION;
    private List<QuickReportDimension> DimensionColumns = new ArrayList<QuickReportDimension>();
    private List<QuickReportMeasure> Measures = new ArrayList<QuickReportMeasure>();
    private List<QuickReportExpression> Expressions = new ArrayList<QuickReportExpression>();
    private List<DisplayFilter> displayFilters = new ArrayList<DisplayFilter>();
    private boolean ShowTable = true;
    
    @SuppressWarnings("unchecked")
	public QuickReport(Element parent, Namespace ns) {
		title = XmlUtils.getStringContent(parent, "Title", ns);
		ShowTable =  XmlUtils.getBooleanContent(parent, "ShowTable", ns);
		
		List<Element> dimensionColumns = parent.getChildren("DimensionColumns", ns);
		for (Iterator<Element> dimColsIter = dimensionColumns.iterator(); dimColsIter.hasNext(); ) {
			List<Element> dimCols = dimColsIter.next().getChildren("QuickReportDimension", ns);
			for (Iterator<Element> dimCIter = dimCols.iterator(); dimCIter.hasNext(); ) {
				Element quickReportDimension = dimCIter.next();
				QuickReportDimension dim = new QuickReportDimension(quickReportDimension, ns);
				DimensionColumns.add(dim);
			}
		}
		List<Element> measureColumns = parent.getChildren("Measures", ns);
		for (Iterator<Element> measureColumnsIter = measureColumns.iterator(); measureColumnsIter.hasNext(); ) {
			List<Element> mcs = measureColumnsIter.next().getChildren("QuickReportMeasure", ns);
			for (Iterator<Element> mc = mcs.iterator(); mc.hasNext(); ) {
				Element quickReportMeasure = mc.next();
				String dimName = XmlUtils.getStringContent(quickReportMeasure, "MeasureName", ns);
				String colName = XmlUtils.getStringContent(quickReportMeasure, "MeasureLabel", ns);
				Integer orderBy = XmlUtils.getIntContent(quickReportMeasure, "OrderBy", ns);
				String BaseMeasure = XmlUtils.getStringContent(quickReportMeasure, "BaseMeasure", ns);
				String AggregationRule = XmlUtils.getStringContent(quickReportMeasure, "AggregationRule", ns);
				String TimePrefix = XmlUtils.getStringContent(quickReportMeasure, "TimePrefix", ns);
				QuickReportMeasure measure = new QuickReportMeasure(dimName, colName, orderBy,BaseMeasure,AggregationRule,TimePrefix);
				this.Measures.add(measure);
			}
		}
		List<Element> expressionColumns = parent.getChildren("Expressions", ns);
		for (Iterator<Element> expressionColsIter = expressionColumns.iterator(); expressionColsIter.hasNext(); ) {
			List<Element> exs = expressionColsIter.next().getChildren("QuickReportExpression", ns);
			for (Iterator<Element> ex = exs.iterator(); ex.hasNext(); ) {
				Element quickReportExpression = ex.next();
				String formula = XmlUtils.getStringContent(quickReportExpression, "Formula", ns);
				String label = XmlUtils.getStringContent(quickReportExpression, "Label", ns);
				Integer orderBy = XmlUtils.getIntContent(quickReportExpression, "OrderBy", ns);
				QuickReportExpression expression = new QuickReportExpression(formula, label, orderBy);
				this.Expressions.add(expression);
			}
		}
		List<Element> displayFilters = parent.getChildren("DisplayFilter", ns);
		for (Iterator<Element> displayFiltersIter = displayFilters.iterator(); displayFiltersIter.hasNext();) {
			Element df = displayFiltersIter.next();
			DisplayFilter displayFilter = new DisplayFilter();
			displayFilter.parseElement(df, ns);
			this.displayFilters.add(displayFilter);
		}
		
		chartType = XmlUtils.getStringContent(parent, "ChartType", ns);
    	
    }
    
    public Element getQuickReportElement(Namespace ns)
    {
    	Element e = new Element("QuickReport",ns);
    	
    	Element child = new Element("ChartType",ns);
		child.setText(chartType);
		e.addContent(child);
		
		child = new Element("Title",ns);
		child.setText(title);
		e.addContent(child);
		
		child = new Element("Type",ns);
		child.setText(String.valueOf(Type));
		e.addContent(child);
		
		if (DimensionColumns!=null && !DimensionColumns.isEmpty())
		{
			child = new Element("DimensionColumns",ns);
			for (QuickReportDimension qrd : DimensionColumns)
			{
				child.addContent(qrd.getQuickReportDimensionElement("QuickReportDimension", ns));
			}
			e.addContent(child); 	
		}
		
		if (Measures!=null && !Measures.isEmpty())
		{
			child = new Element("Measures",ns);
			for (QuickReportMeasure qrm : Measures)
			{
				child.addContent(qrm.getQuickReportMeasureElement(ns));
			}
			e.addContent(child); 	
		}
		
		if (Expressions!=null && !Expressions.isEmpty())
		{
			child = new Element("Expressions",ns);
			for (QuickReportExpression qre : Expressions)
			{
				child.addContent(qre.getQuickReportExpression(ns));
			}
			e.addContent(child); 	
		}
		
		child = new Element("ShowTable",ns);
		child.setText(String.valueOf(ShowTable));
		e.addContent(child);		
		
		if (displayFilters!=null && !displayFilters.isEmpty())
		{
			/*
			 * Since we have a single displayfilter on QuickReport in AppBuilder, I am just picking the first instance and
			 * serializing that
			 */
			DisplayFilter displayFilter = displayFilters.get(0);
			e.addContent(displayFilter.getDisplayFilterElement("DisplayFilter", ns));	
		}
    	
    	return e;
    }
    
	public String getChartType() {
		return chartType;
	}
	public void setChartType(String chartType) {
		this.chartType = chartType;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getType() {
		return Type;
	}
	public void setType(int type) {
		Type = type;
	}
	public List<QuickReportDimension> getDimensionColumns() {
		return DimensionColumns;
	}
	public void setDimensionColumns(List<QuickReportDimension> dimensionColumns) {
		DimensionColumns = dimensionColumns;
	}
	public List<QuickReportMeasure> getMeasures() {
		return Measures;
	}
	public void setMeasures(List<QuickReportMeasure> measures) {
		Measures = measures;
	}
	public boolean isShowTable() {
		return ShowTable;
	}
	public void setShowTable(boolean showTable) {
		ShowTable = showTable;
	}

	public List<QuickReportExpression> getExpressions() {
		return Expressions;
	}

	public void setExpressions(List<QuickReportExpression> expressions) {
		Expressions = expressions;
	}

	public List<DisplayFilter> getDisplayFilters() {
		return displayFilters;
	}

	public void setDisplayFilters(List<DisplayFilter> displayFilters) {
		this.displayFilters = displayFilters;
	}

}
