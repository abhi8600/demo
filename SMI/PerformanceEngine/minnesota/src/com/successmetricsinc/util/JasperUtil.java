/**
 * $Id: JasperUtil.java,v 1.17 2011-11-08 02:06:00 agarrison Exp $
 *
 * Copyright (C) 2007-2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.util;

import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JRPropertiesMap;
import net.sf.jasperreports.engine.design.JRDesignTextField;

/**
 * Utilities for handling JasperReports objects
 * 
 * @author bpeters
 * 
 */
public class JasperUtil
{	
	/**
	 * Create a new text field based on an existing one - preserving the formatting
	 * 
	 * @param field
	 * @return
	 */
	public static JRDesignTextField createTextFieldSameFormat(JRDesignTextField field)
	{
		JRDesignTextField result = new JRDesignTextField();
		JasperUtil.copyTextFieldFormat(field, result, false, false);
		return (result);
	}

	/**
	 * Copy formatting from a source text field to a target one
	 * 
	 * @param source
	 * @param target
	 * @param ignoreBackground
	 *            Whether to ignore these settings (since cannot determine whether these were actually set or there is a
	 *            default)
	 * @param ignoreBorder
	 * @return
	 */
	public static void copyTextFieldFormat(JRDesignTextField source, JRDesignTextField target, boolean ignoreBackground, boolean ignoreBorder)
	{
		if (!ignoreBackground)
			target.setBackcolor(source.getBackcolor());
		target.setForecolor(source.getForecolor());
		target.setFontName(source.getFontName());
		target.setFontSize(source.getFontSize());
		target.setBold(source.isBold());
		target.setItalic(source.isItalic());
		if (!ignoreBorder)
		{
			target.getLineBox().getLeftPen().setLineWidth(source.getLineBox().getLeftPen().getLineWidth());
			target.getLineBox().getRightPen().setLineWidth(source.getLineBox().getRightPen().getLineWidth());
			target.getLineBox().getTopPen().setLineWidth(source.getLineBox().getTopPen().getLineWidth());
			target.getLineBox().getBottomPen().setLineWidth(source.getLineBox().getBottomPen().getLineWidth());

			target.getLineBox().getLeftPen().setLineColor(source.getLineBox().getLeftPen().getLineColor());
			target.getLineBox().getRightPen().setLineColor(source.getLineBox().getRightPen().getLineColor());
			target.getLineBox().getTopPen().setLineColor(source.getLineBox().getTopPen().getLineColor());
			target.getLineBox().getBottomPen().setLineColor(source.getLineBox().getBottomPen().getLineColor());
		}
		target.getLineBox().setPadding(source.getLineBox().getPadding());
		target.setHeight(source.getHeight());
		target.setWidth(source.getWidth());
		target.setMode(source.getModeValue());
		target.setPattern(source.getPattern());
		target.getLineBox().setLeftPadding(source.getLineBox().getLeftPadding());
		target.getLineBox().setRightPadding(source.getLineBox().getRightPadding());
		target.getLineBox().setTopPadding(source.getLineBox().getTopPadding());
		target.getLineBox().setBottomPadding(source.getLineBox().getBottomPadding());
		target.setHorizontalAlignment(source.getHorizontalAlignmentValue());
		target.setStretchType(source.getStretchTypeValue());
		target.setPositionType(source.getPositionTypeValue());
		target.setStretchWithOverflow(source.isStretchWithOverflow());
		target.setBlankWhenNull(source.isBlankWhenNull());
		target.setHyperlinkReferenceExpression(source.getHyperlinkReferenceExpression());
		target.setLinkTarget(source.getLinkTarget());
		target.setLinkType(source.getLinkType());
		target.setUnderline(source.isUnderline());
		JRPropertiesMap propMap = source.getPropertiesMap();
		JRPropertiesMap targetProps = target.getPropertiesMap();
		if (propMap != null)
		{
			String[] names = propMap.getPropertyNames();
			if (names != null)
			{
				for (String name : names)
				{
					targetProps.setProperty(name, propMap.getProperty(name));
				}
			}
		}
	}
	
	public static Map<String, Object> createParameterMap() {
		Map<String, Object> ret = new HashMap<String, Object>();
		/*  Too many things are not Serializable.  Now getting  .io.NotSerializableException: net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer
		JRSwapFile swapFile = new JRSwapFile(System.getProperty("java.io.tmpdir"), 2, 2);
		JRSwapFileVirtualizer virtualizer = new JRSwapFileVirtualizer(10, swapFile);
		ret.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
		*/
		
		return ret;
	}
}
