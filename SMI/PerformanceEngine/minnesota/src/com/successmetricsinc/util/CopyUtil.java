/**
 * Copyright (C) 2007-2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * 
 * @author jshah
 *
 */
public class CopyUtil 
{			
	private static Logger logger = Logger.getLogger(CopyUtil.class);
	private static boolean isFirst = true;
	static long repositoryCounter = 0;
	
	/**
	 * This method is being used to copy catalog directory
	 * @param source
	 * @param target
	 * @param skipExtensions
	 * @throws Exception 
	 */
	public static void copyCatalogDir(String source, String target, String[] skipExtensions, String[] allowedItems, boolean replicate) throws Exception
	{
		 File sd = new File(source);
		 File td = new File(target);
         if (!sd.isDirectory() || !sd.exists())
         {
             logger.debug("copy catalog Dir: source directory does not exist: " + source);
             return;
         }else
         {
        	 try
        	 {
             	copyDir(sd, td, skipExtensions, allowedItems, replicate);
             }catch(IOException e){
               	throw new Exception("Exception caught while copying catalog dir : " + e.getMessage());
             }
         }
         
         return;
	}
	
	/**
	 * 
	 * @param src
	 * @param dest
	 * @param skipExtensions
	 * @throws IOException
	 */
	public static void copyDir(File src, File dest, String[] skipExtensions, String[] allowedItems, boolean replicate) throws IOException
	{	 
	   	if(src.isDirectory())
	   	{	 
	   		//if directory not exists, create it
	   		if(!dest.exists())
	   		{
	   		   dest.mkdir();	  		   
	   		}	
	    	//list all the directory contents
	    	String files[] = src.list();
	    	if(isFirst){
	    		isFirst = false;
	    		files = filterDirOrFile(allowedItems, files, dest, replicate);
	    	}
	    	
	    	for (String file : files) 
	    	{
	    		//construct the src and dest file structure
	    		File srcFile = new File(src, file);
	    		File destFile = new File(dest, file);
	    		//recursive copy
	    		copyDir(srcFile, destFile, skipExtensions, allowedItems, replicate);
	    	}
	    }else
	    {
	    	if(isValidExtension(src, skipExtensions))
	    	{
	    		int count = 0;
	    		while(count++ < 10)
	    		{
	    			try{
	    				copyFile(src, dest, true);
	    				break;
	    			}catch(IOException ex)
	    			{
	    				logger.debug("copyDir: " + src.getAbsolutePath() + ", " + dest.getAbsolutePath() + ": " + ex.getMessage());
	    				try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							logger.debug(e.getMessage());
						}
	    			}
	    		}
	    	}
	    }	    
	}

	/**
	 * This is being used only when copyspace command is invoked from command line and not from UI
	 * Here, only allowedItems need to be copied from catalog directory.
	 * 
	 * @param allowedItems
	 * @param files
	 * @param dest
	 * @param replicate
	 * @return
	 */
	private static String[] filterDirOrFile(String[] allowedItems, String[] files, File dest, boolean replicate) 
	{	
		List<String> filteredFileList = new ArrayList<String>();
		if(allowedItems != null)
		{
			if(allowedItems.length == 1 && allowedItems[0].equals("NONE"))
			{
				return files;
			}else{
				List<String> allowed = Arrays.asList(allowedItems);
				for(int i = 0; i<files.length; i++)
				{
					if(allowed.contains(files[i]))
					{
						File destFile = new File(dest, files[i]);
						if(replicate)
						{
							destFile.deleteOnExit();
						}
						filteredFileList.add(files[i]);
					}
				}
			}
		}else{
			return files;
		}
		
		return filteredFileList.toArray(new String[filteredFileList.size()]);
	}

	/**
	 * Copy files from source to destination
	 * @param src
	 * @param dest
	 * @param overwrite
	 * @throws IOException
	 */
	public static void copyFile(File src, File dest, boolean overwrite) throws IOException
	{
		long val = repositoryCounter++;
		String dname = dest.getAbsolutePath() + ".new" + val;
		File destFile = new File(dname);
		
		if(!overwrite && dest.exists())
		{
			throw new IOException("File exists and overwrite flag was false");
		}
		if(overwrite)
		{
			destFile.deleteOnExit();
		}
		
		InputStream in = null;
		OutputStream out = null;
		try
		{
			//if file, then copy it
			//Use bytes stream to support all file types
			in = new FileInputStream(src);
			out = new FileOutputStream(destFile); 	 
			byte[] buffer = new byte[1024];	 
			int length;
			//copy the file content in bytes 
			while ((length = in.read(buffer)) > 0)
			{
				out.write(buffer, 0, length);
			}
		}finally{
			if(in != null)
				in.close();
			if(out != null)
				out.close();
		}
		logger.trace("File copied from " + src + " to " + dest);
		
		// save the old version, move the new version to the proper location, delete the old version        
        String dname2 = dest.getAbsolutePath() + ".old" + val;
        File destFile2 = new File(dname2);
        if (dest.exists())
        {
        	destFile2.deleteOnExit();   // delete old (paranoid, should never be there)
            dest.renameTo(destFile2); // move current to old
        }
        destFile.renameTo(dest);  // move new to current
        destFile2.deleteOnExit();     // delete old
        return;
	}
	
	/**
	 * 
	 * @param src
	 * @param skipExtensions
	 * @return
	 */
	private static boolean isValidExtension(File src, String[] skipExtensions) 
	{
		if(!(skipExtensions.length == 1 && skipExtensions[0].equals("NONE")))
		{
			for(String ext: skipExtensions)
			{
				if(src.getName().endsWith(ext))
				{	
					return false;
				}
			}
		}
		return true;
	}	
}
