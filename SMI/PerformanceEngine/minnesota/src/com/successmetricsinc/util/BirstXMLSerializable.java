/**
 * 
 */
package com.successmetricsinc.util;

import java.awt.Font;
import java.io.Serializable;
import java.util.Map;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.jdom.Element;

/**
 * @author agarrison
 *
 */
public abstract class BirstXMLSerializable implements Serializable {

	private static final long serialVersionUID = 1L;

	public abstract void parseElement(OMElement parent);
	public abstract void parseElement(XMLStreamReader parent) throws Exception;
	
	public abstract void addContent(OMFactory fac, OMElement parent, OMNamespace ns);	
	
	public abstract BirstXMLSerializable createNew();
	
	public static <T>T parseTypeFromString(String s, Map<T, String> map, T defaultValue) {
		for (T t : map.keySet()) {
			if (map.get(t).equals(s))
				return t;
		}
		return defaultValue;
	}
	
	public static Element getFontElement(Font f, double scaleFactor) {
		Element font = new Element("font");
		font.setAttribute("family", f.getFamily());
		long size = Math.round(((double) f.getSize() * scaleFactor));
		font.setAttribute("size", String.valueOf(size));
		font.setAttribute("bold", f.isBold() ? "True" : "False");
		font.setAttribute("italic", f.isItalic() ? "True" : "False");
		return font;
	}
	
	
	public static Element addFontElement(Element label, Font f, double scaleFactor) {
		Element font = XmlUtils.findOrCreateChild(label, "font");
		font.setAttribute("family", f.getFamily());
		long size = Math.round(((double) f.getSize() * scaleFactor));
		font.setAttribute("size", String.valueOf(size));
		font.setAttribute("bold", f.isBold() ? "True" : "False");
		font.setAttribute("italic", f.isItalic() ? "True" : "False");
		return font;
	}
}
