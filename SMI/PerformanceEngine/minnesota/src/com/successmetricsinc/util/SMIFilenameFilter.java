/**
 * $Id: SMIFilenameFilter.java,v 1.3 2011-02-19 01:28:43 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.util;

import java.io.*;

public class SMIFilenameFilter implements FilenameFilter 
{
	String searchPattern = null;
	
	public void setSearchPattern(String searchPattern)
	{
		this.searchPattern = searchPattern;
	}
	
	public boolean accept(File dir, String name)
	{
		if((searchPattern == null) || !Util.hasNonWhiteSpaceCharacters(searchPattern) || (name == null) || !Util.hasNonWhiteSpaceCharacters(name))
		{
			return false;
		}

		if (searchPattern.equals(name))
			return true; // fail-safe

		int starIndex = searchPattern.indexOf('*');
		if(starIndex >= 0)
		{
			String prefix = searchPattern.substring(0, starIndex);
			String suffix = searchPattern.substring(starIndex + 1, searchPattern.length());
			if(name.startsWith(prefix) && name.endsWith(suffix))
			{
				return true;
			}
		}
		return false;
	}
}
