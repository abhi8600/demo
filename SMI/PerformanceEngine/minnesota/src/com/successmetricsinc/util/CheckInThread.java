package com.successmetricsinc.util;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import org.apache.log4j.Logger;

public class CheckInThread extends Thread 
{
	public static final String CHECK_IN_PARAM_TIME = "time";
	
	// default is 30 seconds
	public static final long CHECK_IN_SLEEP_DURATION_MILLISECONDS = 30*1000;
	
	private static final Logger logger = Logger.getLogger(CheckInThread.class);
	
	private String checkInFileName;
	public CheckInThread(String checkInFileName)
	{
		this.checkInFileName = checkInFileName;
	}
	
	public void run()
	{	
		while(true)
		{
			BufferedWriter writer = null;
			try {
				writer = new BufferedWriter(new FileWriter(checkInFileName, false));
				writer.write(CHECK_IN_PARAM_TIME + "=" + System.currentTimeMillis());
			} catch (IOException e) {
				logger.error("Error in writing to file : " + checkInFileName, e);
			}
			finally
			{
				if(writer != null)
				{
					try {
						writer.close();
					} catch (IOException e) {
						logger.warn("Error in closing file writer : " + checkInFileName, e);
					}
				}
				
				try {
					Thread.sleep(CHECK_IN_SLEEP_DURATION_MILLISECONDS);
				} catch (InterruptedException e) {
					logger.warn("Error while sleeping during check in : " + checkInFileName, e);
				}
			}
		}	
	}
}
