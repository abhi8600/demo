/**
 * $Id: Util.java,v 1.76 2012-12-02 18:40:54 birst\bpeters Exp $
 *
 * Copyright (C) 2007-2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.sql.Date;
import java.sql.Types;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.concurrent.atomic.AtomicLong;
import java.nio.channels.FileChannel;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.context.ConfigurationContext;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.log4j.Logger;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.BasicSessionCredentials;
import com.successmetricsinc.Group;
import com.successmetricsinc.PackageSpaceProperties;
import com.successmetricsinc.Repository;
import com.successmetricsinc.Variable;
import com.successmetricsinc.connectors.BaseConnector;
import com.successmetricsinc.connectors.sdfc.SFDCConnector;
import com.successmetricsinc.query.AbstractQueryString;
import com.successmetricsinc.query.Database;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.DimensionColumn;
import com.successmetricsinc.query.DimensionTable;
import com.successmetricsinc.query.DisplayExpression;
import com.successmetricsinc.query.MeasureColumn;
import com.successmetricsinc.query.MeasureExpressionColumn;
import com.successmetricsinc.query.MeasureTable;
import com.successmetricsinc.query.NavigationException;
import com.successmetricsinc.query.Query;
import com.successmetricsinc.query.QueryString;
import com.successmetricsinc.query.Token;
import com.successmetricsinc.query.Database.DatabaseType;
import com.successmetricsinc.security.EncryptionService;
import com.successmetricsinc.util.logging.LogUtil;
import com.successmetricsinc.warehouse.GenerateSchema;
import com.successmetricsinc.warehouse.SourceColumn;
import com.successmetricsinc.warehouse.SourceFile;
import com.successmetricsinc.warehouse.SourceFile.FileFormatType;

/**
 * Utility class - contains several utility methods.
 */
public class Util
{
	public static final String CUSTOMER_PROPERTIES_FILENAME = "customer.properties";
	public static final String SMI_HOME = "smi.home";
	public static final String LINE_SEPARATOR = System.getProperty("line.separator");
	private static String buildString;
	private static Logger logger = Logger.getLogger(Util.class);
	private static String hostName = null;
	private static String ipAddress = null;
	private static Map<String, Properties> propertiesMap = new HashMap<String, Properties>();
	private static Map<String, String> dataTypeMap = new HashMap<String, String>();
	private static Map<Integer, String> jdbcTypeMap = new HashMap<Integer, String>();
	public static final String KILL_PROCESS_FILE = "Load_kill.lock";
	public static final String KILL_EXTRACT_FILE = "SFDC_kill.lock";
	public static final String PUBLISH_LOCK_FILE = "publish.lock";
	public static final String BIRST_NULL = "BIRST_NULL";
	public static final String BIRST_EMPTY = "BIRST_EMPTY";
	
	private static int dbWaitTimeout = -1;
	
	static
	{
		dataTypeMap.put("Number", "NUMERIC(19,6)");
		dataTypeMap.put("Varchar", "VARCHAR");
		dataTypeMap.put("Integer", "BIGINT");
		dataTypeMap.put("Date", "DATETIME");
		dataTypeMap.put("DateTime", "DATETIME");
		dataTypeMap.put("Float", "FLOAT");
	}
	
	static
	{
		try
		{			
			for (Field field : Types.class.getFields()) 
			{
				jdbcTypeMap.put((Integer)field.get(null), field.getName());
			}
		}
		catch (Exception e1)
		{
			logger.error(e1);
		}
	}
	
	public static List<File> getFileList(String baseName)
	{
		List<File> files = new ArrayList<File>();
		File fl = new File(baseName);
		File dir = fl.getParentFile();
		if (dir == null)
		{
			logger.debug("null parentfile");
			return files;
		}
		File[] children = dir.listFiles();
		if (children == null)
		{
			logger.debug("null listFiles");
			return files;
		}
		//Do a case insensitive file search
		baseName = baseName.toLowerCase();
		for (File child : children)
		{
			String childName = child.getAbsolutePath().toLowerCase();
			if (childName.startsWith(baseName))
			{
				// XXX ugly hack having to do with format file being here... we really need to do a better job, it's only needed for MSSQL, the meta data could be stored elsewhere
				if (childName.endsWith(".format"))
					continue;
				files.add(child);
			}
		}
		return files;
	}

	public static String getDBDataType(String dataType)
	{
		if (dataType == null)
		{
			return dataType;
		}
		return dataTypeMap.get(dataType);
	}
	
	public static String getJDBCType(int jdbcType)
	{
		return jdbcTypeMap.get(jdbcType);
	}
	
	/**
	 * convert JDBC type to database specific type
	 * 
	 * @param dconn
	 * @param declaredType
	 * @param isMeasureColumn
	 * @param o
	 * @param dt
	 * @param r
	 * @param width
	 * @return
	 */
	public static String getColumnType(DatabaseConnection dconn, String declaredType, boolean isMeasureColumn, Object o, DimensionTable dt, Repository r,
			int width)
	{
		if (declaredType.equalsIgnoreCase("Varchar"))
		{
			if (width == -1 && o != null)
				width = GenerateSchema.getColumnWidth(dconn, declaredType, isMeasureColumn, o, dt, r);
			
			// XXX why aren't we using DatabaseConnection::getVarcharType???
			if (width > dconn.getMaxVarcharWidth())
			{
				return dconn.getLargeTextType();
			}
			if (dconn.DBType == DatabaseType.Oracle)
				return ("NVARCHAR2(" + width + ")");
			else if (DatabaseConnection.isDBTypeParAccel(dconn.DBType))
				return ("VARCHAR(" + width + ")");
			else if (dconn.DBType == DatabaseType.MemDB)
				return ("VARCHAR(" + width + ")");			
			else if (dconn.isUnicodeDatabase() && dconn.DBType != DatabaseType.MonetDB)
				return ("NVARCHAR(" + width + ")");
			else
				return ("VARCHAR(" + width + ")");
		} else if (declaredType.equalsIgnoreCase("Integer"))
		{
			// special case LOAD_ID and DateID columns for Redshift (should do it for all, but this is a safe way to test)
			if (dconn.DBType == Database.DatabaseType.Redshift && o != null)
			{
				String columnName = isMeasureColumn ? ((MeasureColumn) o).ColumnName : ((DimensionColumn) o).ColumnName;
				if (columnName.equals("LOAD_ID") || (columnName.startsWith("Time.") && columnName.endsWith(" ID")))
					return "INTEGER";
			}
			return dconn.getDimensionalKeyType(r);
		} else if (declaredType.equalsIgnoreCase("DOUBLE"))
		{		
			if (DatabaseConnection.isDBTypeMSSQL(dconn.DBType) || dconn.DBType == DatabaseType.Oracle)
				return "FLOAT";
			return "DOUBLE";
		} else if (declaredType.equalsIgnoreCase("Number"))
		{
			return dconn.getNumericType();
		} else if (declaredType.equalsIgnoreCase("DateTime") || declaredType.equalsIgnoreCase("TimeStamp"))
		{
			return (dconn.getDateTimeType());
		} else if (declaredType.equalsIgnoreCase("Date"))
		{
			return (dconn.getDateType());
		} else if (declaredType.equalsIgnoreCase("Float"))
		{
			if (DatabaseConnection.isDBTypeMySQL(dconn.DBType))
				return "DOUBLE";
			return "FLOAT";
		} else if (declaredType.equalsIgnoreCase("None"))
		{
			// Smallest type implicitly convertible to any other type
			if (dconn.DBType == DatabaseType.MonetDB || DatabaseConnection.isDBTypeParAccel(dconn.DBType))
				return ("SMALLINT");
			else if (dconn.DBType == DatabaseType.Oracle)
				return ("NUMBER(3)");
			else if (dconn.DBType == DatabaseType.MemDB)
				return ("INTEGER");
			return ("TINYINT");
		}
		return (declaredType);
	}
	
	/**
	 * intern a string if not null
	 * - in Java 7 interned strings are managed on the heap, so they are garbage collected when necessary
	 * - using this for strings from the repository saves 50MB+ for the Kaplan repository
	 */
	public static String intern(String s)
	{
		if (s != null)
			return s.intern();
		else
			return null;
	}

	/**
	 * Replace illegal characters in a string with ones that are valid for SQL database physical names. Replace spaces
	 * with underscores, dollar signs with D, number signs with N and forward slashes with underscores
	 * 
	 * @param s
	 * @return
	 */
	public static String replaceWithPhysicalString(String s)
	{
		return replaceWithPhysicalString(s, null);
	}
	
	public static String replaceWithPhysicalString(String s, Repository r)
	{
		return replaceWithPhysicalString(null,s,r);
	}
		
    /**
     * WARNING - this renaming must match the renaming in Util.generatePhysicalName in Performance Optimization Administration
     * - if you get SQL errors about bad column names when updating a dimension table from a staging table, it is probably because these two methods have drifted apart
     * - dbc.getHashedIdentifier is not on the C# side, that could cause issues in the future (but not now since for this specific issue we pass dbc as null)
     */
	public static String replaceWithPhysicalString(DatabaseConnection dbc, String s)
	{
		return replaceWithPhysicalString(dbc, s, null);
	}
	

	
	public static String replaceWithPhysicalString(DatabaseConnection dbc, String s, Repository r)
	{
		if (s == null)
			return null;
		if (s.length() == 0)
			return s;
		boolean isUnicodeHashRequired = ((r!=null) && r.getCurrentBuilderVersion()>=22) ? true : false;
		char c = s.charAt(0);
		if ((c >= '0' && c <= '9'))
		{
			s = 'N' + s;
		}
		s = s.replace('$', 'D').replace('#', 'N');
		s = s.replaceAll("[^\\w\u0080-\uFFCF]", "_");	// Replace all non-word characters for ASCII.
				
		if (!Util.containsNonAsciiCharacters(s))
		{
			s = s.replaceAll("[^\\w]", "_");	// Replace all non-word characters for ASCII.
		}
		else
		{
			return getAsciiString(s, isUnicodeHashRequired);
		}
		if (dbc != null && dbc.requiresHashedIdentifierForColumnAlias())
				s = dbc.getHashedIdentifier(s);
		return (s);
	}
	
	public static String replaceWithPhysicalString(DatabaseConnection dbc, String s, Repository r, int maxLength)
	{
		if (s == null)
			return null;
		if (s.length() == 0)
			return s;
		boolean isUnicodeHashRequired = ((r!=null) && r.getCurrentBuilderVersion()>=22) ? true : false;
		char c = s.charAt(0);
		if ((c >= '0' && c <= '9'))
		{
			s = 'N' + s;
		}
		s = s.replace('$', 'D').replace('#', 'N');
		s = s.replaceAll("[^\\w\u0080-\uFFCF]", "_");	// Replace all non-word characters for ASCII.
				
		if (!Util.containsNonAsciiCharacters(s) && s.length()<maxLength)
		{
			s = s.replaceAll("[^\\w]", "_");	// Replace all non-word characters for ASCII.
		}
		else if (dbc != null && dbc.requiresHashedIdentifierForColumnAlias())
		{
			s = dbc.getHashedIdentifier(s);
		}
		else 
		{
			s = getAsciiString(s, isUnicodeHashRequired);
		}
		
		return (s);
	}

	public static String getAsciiString(String s, boolean isUnicodeHashRequired)
	{
		String md5 = EncryptionService.getMD5(s, isUnicodeHashRequired).toUpperCase();
		char[] cs = md5.toCharArray();
		if (cs[0] >= '0' && cs[0] <= '9')
			md5 = 'X' + md5;
		return md5;
	}

	/**
	 * Determine if a given string contains any non-ascii characters.
	 * 
	 * @param s
	 * @return
	 */
	public static boolean containsNonAsciiCharacters(String s)
	{
		boolean result = false;
		if (s == null)
			return false;
		char c = 0;
		for (int i = 0; i < s.length(); i++)
		{
			c = s.charAt(i);
			if (c > 0x007F)
			{
				result = true;
				break;
			}
		}
		return result;
	}

	/**
	 * Replace illegal characters in a string for the purposes of creating a new physical name. Additional character is
	 * appended to the end to ensure to reserved word collisions
	 * 
	 * @param s
	 * @return
	 */
	
	public static String replaceWithNewPhysicalString(String s, DatabaseConnection dc, Repository r)
	{
		return (replaceWithPhysicalString(s, r) + dc.getPhysicalSuffix());
	}
	

	

	/**
	 * Return a user-readable version of a filter
	 * 
	 * @param r
	 * @param s
	 * @return
	 * @throws Exception
	 */
	public static String filterDisplayString(Repository r, String s) throws Exception
	{
		StringBuilder sb = new StringBuilder();
		List<Token> list = Token.getTokens(s);
		for (int i = 0; i < list.size(); i++)
		{
			if (i > 0)
				sb.append(" and ");
			sb.append(QueryString.returnFilter(list.get(i), r).getDisplayString(r));
		}
		return (sb.toString());
	}

	/**
	 * Parse a comma-separated list of key parameters into an array list.
	 * 
	 * @param keyParameters
	 *            - comma-separated list of key parameters
	 * @return ArrayList containing key parameter names.
	 * @throws Exception
	 */
	public static List<String> getKeyParameterNames(String keyParameters) throws Exception
	{
		if (keyParameters == null || keyParameters.length() <= 0 || keyParameters.trim().equals(""))
		{
			throw new Exception("Key parameters must contain atleast one value.");
		}
		StringTokenizer st = new StringTokenizer(keyParameters, ",");
		List<String> paramNameList = new ArrayList<String>();
		while (st.hasMoreTokens())
		{
			paramNameList.add(st.nextToken());
		}
		return paramNameList;
	}

	/**
	 * Get a map containing Key Parameter Name vs. ID List
	 * 
	 * @param numParams
	 *            - number of parameters
	 * @param filename
	 *            - file containing comma-separated lines of parameter values
	 * @return Two-dimensional array list containig key parameter values.
	 * @throws Exception
	 */
	public static List<List<String>> getKeyParameterValues(int numParams, String filename) throws Exception
	{
		List<List<String>> paramValueList = new ArrayList<List<String>>();
		FileInputStream stream = null;
		BufferedReader reader = null;
		try
		{
			stream = new FileInputStream(filename);
			reader = new BufferedReader(new InputStreamReader(stream));
			String line = null;
			int lineNumber = 0;
			while ((line = reader.readLine()) != null)
			{
				lineNumber++;
				if (line.trim().equals(""))
				{
					continue;
				}
				StringTokenizer stLine = new StringTokenizer(line, ",");
				int numToken = 0;
				List<String> paramValueListItem = new ArrayList<String>();
				while (stLine.hasMoreTokens())
				{
					numToken++;
					if (numToken > numParams)
					{
						throw new Exception("Error: Too many parameter values at line number " + lineNumber + " in file " + filename
								+ ": Expected number of parameter values = " + numParams);
					}
					paramValueListItem.add(stLine.nextToken());
				}
				if (numToken < numParams)
				{
					throw new Exception("Error: Too few parameter valuess at line number " + lineNumber + " in file " + filename
							+ ": Expected number of parameter values = " + numParams);
				}
				paramValueList.add(paramValueListItem);
			}
			return paramValueList;
		}
		finally
		{
			if (reader != null)
				reader.close();
			if (stream != null)
				stream.close();
		}
	}

	/**
	 * Replace instances of a substring within another string
	 * 
	 * @param a
	 *            String to process
	 * @param b
	 *            String to search for
	 * @param c
	 *            String to replace with
	 * @return New string
	 */
	public static String replaceStr(String a, String b, String c)
	{
		if (a == null || b == null || c == null || a.indexOf(b) < 0)
			return a;
		StringBuilder sb = new StringBuilder(a);
		int len = b.length();
		int i = sb.indexOf(b);
		while (i >= 0)
		{
			sb.replace(i, i + len, c);
			i = sb.indexOf(b, i + c.length());
		}
		return (sb.toString());
	}
	
	/**
	 * Replace first instances of a substring within another string
	 * 
	 * @param a
	 *            String to process
	 * @param b
	 *            String to search for
	 * @param c
	 *            String to replace with
	 * @return New string
	 */
	public static String replaceFirstStr(String a, String b, String c)
	{
		if (a == null || b == null || c == null || a.indexOf(b) < 0)
			return a;
		StringBuilder sb = new StringBuilder(a);
		int len = b.length();
		int i = sb.indexOf(b);
		if (i >= 0)
		{
			sb.replace(i, i + len, c);
		}
		return (sb.toString());
	}

	/**
	 * Replace instances of a substring ignoring case
	 * 
	 * @param a
	 *            String to process
	 * @param b
	 *            String to search for
	 * @param c
	 *            String to replace with
	 * @return New string
	 */
	public static String replaceStrIgnoreCase(String a, String b, String c)
	{
		if (a == null || b == null || c == null)
			return a;
		StringBuilder sb = new StringBuilder(a);
		int len = b.length();
		b = b.toLowerCase();
		int i = findIgnoreCase(sb, b);
		while (i >= 0)
		{
			sb.replace(i, i + len, c);
			i = sb.indexOf(b, i + c.length());
		}
		return (sb.toString());
	}
	
	private static int findIgnoreCase(StringBuilder sb, String value)
	{
		String s = sb.toString().toLowerCase();
		return s.indexOf(value);
	}

	/**
	 * Determines whether or not a given set of groups matches another set. Always allow Administrator and OWNER$
	 * access. Also, if allowed groups contains All, then all groups have access. Main client is {@link SubjectAreaWS}
	 * which allows access to both Administrator and OWNER$ group members.
	 * 
	 * @param memberGroups
	 *            the groups to which a member belongs.
	 * @param allowedGroups
	 *            are the groups that are allowed access for a particular resource.
	 * @return true if access is allowed, false otherwise.
	 */
	public static boolean hasAccess(List<Group> memberGroups, List<String> allowedGroups)
	{
		if (memberGroups == null)
		{
			return false;
		}
		for (Group g : memberGroups)
		{
			if (g.getName().equals(Group.GROUP_ADMINISTRATORS) || g.getName().equals(Group.GROUP_OWNER$))
			{
				return true;
			}
		}
		if (allowedGroups == null)
		{
			return false;
		}
		if (allowedGroups.contains(Group.GROUP_ALL))
			return true;
		for (Group g : memberGroups)
		{
			if (allowedGroups != null)
			{
				if (allowedGroups.contains(g.getName()))
				{
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Splits a string around an operator. For example, a=b would return 3 strings {"a","=","b"}
	 * 
	 * @param s
	 * @param operators
	 * @return
	 */
	public static String[] splitOperator(String s, String[] operators)
	{
		String[] result = new String[3];
		int searchLen = -1;
		if (s.contains("Q{"))
		{
			searchLen = s.indexOf("Q{");
		} else
		{
			searchLen = s.length();
		}
		for (int j = 0; j < operators.length; j++)
		{
			for (int i = 0; i < searchLen; i++)
			{
				if (s.regionMatches(true, i, operators[j], 0, operators[j].length()))
				{
					result[0] = s.substring(0, i);
					result[1] = operators[j];
					result[2] = s.substring(i + operators[j].length());
					return (result);
				}
			}
		}
		return (result);
	}

	/**
	 * delete a directory and it's contents, delete as much as is possible
	 * - only used in the CacheGenerator (which is not used anymore)
	 * 
	 * @param dir
	 * @return true if all was deleted, false if some
	 */
	public static boolean deleteDir(File dir)
	{
		boolean res = true;
		if (!dir.exists()) // don't try to delete a directory that does not exist
			return res;
		if (dir.isDirectory())
		{
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++)
			{
				if (!deleteDir(new File(dir, children[i])))
				{
					res = false;
				}
			}
		}
		boolean deleted = false;
		if (dir.isFile())
		{
			try
			{
				deleted = Util.secureDelete(dir);
			} catch (IOException e)
			{
				logger.warn("Unable to delete " + dir.getAbsolutePath());
			}
		} else
		{
			deleted = dir.delete();
		}
		if (!deleted)
		{
			logger.warn("Unable to delete " + dir.getAbsolutePath());
			return false;
		} else
		{
			if (logger.isTraceEnabled())
				logger.trace("Deleted " + dir.getAbsolutePath());
		}
		return res;
	}

	/**
	 * read in properties file
	 * @param file
	 * @return
	 */
	public static Properties getProperties(String file)
	{
		return getProperties(file, false);
	}

	/**
	 * read in a properties file, optionally search for it in 'standard' locations (smi.home/conf, classpath)
	 * @param file
	 * @param search
	 * @return
	 */
	public static Properties getProperties(String file, boolean search)
	{
		// 0) See if this property file has already been loaded
		if (propertiesMap.containsKey(file))
		{
			return propertiesMap.get(file);
		}
		Properties result = new Properties();
		// 1) try the provided filename
		FileInputStream fis = null;
		try
		{
			fis = new FileInputStream(file);
			result.load(fis);
			logger.debug("Using properties file: " + file);
		} catch (FileNotFoundException e)
		{
			logger.debug("Properties file was not found: " + file);
		} catch (IOException e)
		{
			logger.debug("IOException reading properties file: " + file);
		} finally
		{
			if (fis != null)
			{
				try
				{
					fis.close();
				} catch (Exception e)
				{
				}
			}
		}
		// If no 'search' requested, end search here.
		if (result.isEmpty() && !search)
		{
			logger.debug("No properties file - " + file + " in current directory and no request to continue search.");
			return null;
		}
		String smiHome = null;
		// 2a) Try <smi.home>/conf
		if (result.isEmpty())
		{
			smiHome = System.getProperty(Util.SMI_HOME);
			if (smiHome != null && smiHome.length() > 0)
			{
				String path = smiHome + "/conf/" + file;
				fis = null;
				try
				{
					fis = new FileInputStream(path);
					result.load(fis);
					logger.debug("Using properties file: " + path);
					return result;
				} catch (FileNotFoundException e)
				{
					logger.debug("smi.home/conf properties file (" + path + ") was not found.");
				} catch (IOException e)
				{
					logger.debug("smi.home/conf properties file (" + path + ") found but errors reading file");
				} finally
				{
					if (fis != null)
					{
						try
						{
							fis.close();
						} catch (Exception e)
						{
						}
					}
				}
			}
			// 2b) Try <smi.home>/../conf (for performance engine smi.home is the space directory)
			if (result.isEmpty())
			{
				if (smiHome != null && smiHome.length() > 0)
				{
					String path = smiHome + "/../conf/" + file;
					fis = null;
					try
					{
						fis = new FileInputStream(path);
						result.load(fis);
						logger.debug("Using properties file: " + path);
						return result;
					} catch (FileNotFoundException e)
					{
						logger.debug("smi.home/../conf properties file (" + path + ") was not found.");
					} catch (IOException e)
					{
						logger.debug("smi.home/../conf properties file (" + path + ") found but errors reading file");
					} finally
					{
						if (fis != null)
						{
							try
							{
								fis.close();
							} catch (Exception e)
							{
							}
						}
					}
				}	
			}
			// 3) try the classpath
			logger.trace("Continuing search for properties file in classpath.");
			InputStream in = null;
			try
			{
				in = (new Util()).getClass().getClassLoader().getResourceAsStream(file);
				if (in != null)
				{
					result.load(in);
					logger.debug("Using properties file: " + file + " (found via the classpath)");
				}
			} catch (IOException e)
			{
				logger.debug("classpath properties file (" + file + ") not found.");
			} finally
			{
				if (in != null)
				{
					try
					{
						in.close();
					} catch (Exception e)
					{
					}
				}
			}
		}
		// Report results of search
		if (result.isEmpty())
		{
			logger.debug("Properties file (" + file + ") not found or empty after full search was performed.");
		} else
		{
			// Add to properties cache
			propertiesMap.put(file, result);
			if (logger.isDebugEnabled())
				printProperties(result);
		}
		return result;
	}

	/**
	 * dump the properties to the log file
	 * @param props
	 */
	private static void printProperties(Properties props)
	{
		logger.debug("== Properties files ==");
		for (Entry<Object, Object> item : props.entrySet())
		{
			logger.debug(item.getKey().toString() + "=" + item.getValue().toString());
		}
		logger.debug("====");
	}

	/**
	 * return the build and release numbers (from the build.number file)
	 * @return
	 */
	public static String getBuildString()
	{
		if (buildString != null)
			return buildString;
		Properties props = null;
		try
		{
			props = getProperties("build.number", true);
		} catch (Exception e)
		{
			logger.error(e.getMessage(), e);
			props = new Properties();
		}
		String buildNumber = props.getProperty("build.number");
		String releaseNumber = props.getProperty("release.number");
		buildString = "Release: " + releaseNumber + ", Build: " + buildNumber;
		return buildString;
	}

	/**
	 * Convert HTML designed for points to incorporate pixels for images (so that they don't stretch)
	 * 
	 * @param sb
	 * @return
	 */
	public static StringBuffer convertImageSizesToPixels(StringBuffer sb)
	{
		int pos = sb.indexOf("<img ");
		while (pos >= 0)
		{
			// Find the end of the image tag
			int epos = sb.indexOf(">", pos);
			// First, convert references in pt (point) to px (pixels)
			int hpos = sb.indexOf("height", pos);
			if (hpos >= 0 && hpos < epos)
			{
				int ptpos = sb.indexOf("pt", hpos);
				if (ptpos >= 0)
				{
					sb.setCharAt(ptpos + 1, 'x');
				}
			}
			hpos = sb.indexOf("width", pos);
			if (hpos >= 0 && hpos < epos)
			{
				int ptpos = sb.indexOf("pt", hpos);
				if (ptpos >= 0)
				{
					sb.setCharAt(ptpos + 1, 'x');
				}
			}
			// Now, add the alignment tags
			int tpos = pos;
			for (; tpos >= 0; tpos--)
			{
				if (sb.substring(tpos, tpos + 3).equals("<td"))
				{
					break;
				}
			}
			if (tpos >= 0)
			{
				int endToken = sb.indexOf(">", tpos);
				sb.insert(endToken, " align=\"center\" valign=\"center\"");
				pos += 31;
			}
			pos = sb.indexOf("<img", pos + 1);
		}
		return (sb);
	}


	/**
	 * Scale a pixel coordinate by scaleFactor for HTML output
	 * 
	 * @param px
	 * @return
	 */
	public static int scalePx(double scaleFactor, int px)
	{
		return ((int) (scaleFactor * ((double) px)));
	}

	public static final int MaskNone = 0;
	public static final int MaskAxxx = 1;
	public static final int MaskAxxA = 2;
	public static final int Mask1000 = 3;
	public static final int Mask1001 = 4;

	/**
	 * mask a field
	 * @param obj
	 * @param dataType
	 * @param maskType
	 * @return
	 */
	public static Object maskField(Object obj, int dataType, int maskType)
	{
		if (obj == null || maskType == MaskNone)
			return null;
		Character pad = 'x';
		if (maskType == Mask1000 || maskType == Mask1001)
		{
			pad = '0';
		}
		// leave dashes alone
		if (dataType == Types.VARCHAR)
		{
			String str = (String) obj;
			int len = str.length();
			// handle the case of the zero length string
			if (len < 1)
				return obj;
			// handle the case of A with nothing else
			if (len == 1)
				return (Object) str.substring(0, 1);
			StringBuilder st = new StringBuilder(str.substring(0, 1));
			for (int i = 1; i < len - 1; i++)
			{
				if (str.charAt(i) == '-')
					st.append('-');
				else
					st.append(pad);
			}
			if (maskType == MaskAxxx || maskType == Mask1000)
			{
				if (str.charAt(len - 1) == '-')
					st.append('-');
				else
					st.append(pad);
			} else if (maskType == MaskAxxA || maskType == Mask1001)
			{
				st.append(str.charAt(len - 1));
			}
			return (Object) st.toString();
		}
		return obj;
	}

	public static String unQualifyPhysicalName(String s)
	{
		int index = s.indexOf('.');
		if (index > 0)
		{
			return (s.substring(index + 1));
		}
		return (s);
	}

	public static String buildDimensionColumn(DatabaseConnection dc, String dim, String col)
	{
		return Util.replaceWithPhysicalString(dim) + dc.getPhysicalSuffix() + col.replace(" ", "_").replace("-", "_").replace(":", "_") + dc.getPhysicalSuffix();
	}

	public static String removeSystemGeneratedFieldNumber(String columnDisplayName)
	{
		if ((columnDisplayName == null) || (columnDisplayName.trim().length() <= 0))
		{
			logger.info("The column name was empty, continuing...");
			return "--enter valid column name here--";
		}
		String processedColumnDisplayName = columnDisplayName;
		if (columnDisplayName.charAt(0) == 'F')
		{
			int index = columnDisplayName.indexOf('_');
			if ((index <= 4) && (index > 1))
			{
				boolean found = true;
				String colNumStr = columnDisplayName.substring(1, index);
				try
				{
					Integer.parseInt(colNumStr);
				} catch (NumberFormatException nfe)
				{
					found = false;
				}
				if (found)
				{
					processedColumnDisplayName = columnDisplayName.substring(index + 1);
				}
			}
		}
		return processedColumnDisplayName;
	}

	//Replaces all "+$P{param}+", then +P{param}+
	public static String replaceAllParameters(String s, Map<String, Object> parameters)
	{
		if (s == null)
			return null;
		
		for (Entry<String, Object> entry : parameters.entrySet())
		{
			String key = entry.getKey();
			Object val = entry.getValue();
			String value = (val == null) ? "NO_FILTER" : val.toString();
			
			String param = "+$P{" + key + "}+";
			String doubleQuote = "\"" + param + "\"";
			
			s = s.replace(doubleQuote, value);
			s = s.replace(param, value);
		}
		
		return (s);
	}
	
	/**
	 * replace $P{key} with the value in parameters, using key as the key
	 * @param s
	 * @param parameters
	 * @return
	 */
	public static String replaceParameters(String s, Map<String, Object> parameters)
	{
		if (s == null)
			return null;
		for (Entry<String, Object> entry : parameters.entrySet())
		{
			String p = "$P{" + entry.getKey() + "}";
			if (s.contains(p))
			{
				// get rid of "+ (prefix) and +" (suffix)
				int index = s.indexOf(p);
				if (index > 0)
				{
					index--;
					StringBuilder prefix = new StringBuilder();
					while (index > 0)
					{
						char c = s.charAt(index);
						if (c == '"')
						{
							prefix.insert(0, c);
							break;
						}
						if (Character.isWhitespace(c) || c == '+')
						{
							prefix.insert(0, c);
						} else
						{
							prefix = null;
							break;
						}
						index--;
					}
					StringBuilder suffix = new StringBuilder();
					index = s.indexOf(p);
					if (index >= 0)
					{
						index += p.length();
					}
					int size = s.length();
					while (index < size)
					{
						char c = s.charAt(index);
						if (c == '"')
						{
							suffix.append(c);
							break;
						}
						if (Character.isWhitespace(c) || c == '+')
						{
							suffix.append(c);
						} else
						{
							suffix = null;
							break;
						}
						index++;
					}
					if (prefix != null)
					{
						p = prefix + p;
					}
					if (suffix != null)
					{
						p = p + suffix;
					}
				}
				Object val = entry.getValue();
				if(val == null)
				{
					val = "NO_FILTER";
				}
				s = s.replace(p, val.toString());
			}
		}
		return (s);
	}

	public static String getNavigationExplanation(AbstractQueryString qs, Query q, String originalQueryString, boolean isSuperUser) throws Exception
	{
		StringBuilder sb = new StringBuilder();
		if (qs != null && qs.getServerConnectInfo() != null)
		{
			throw new Exception("Server directive can not be specified in query for explaining navigation.");
		}
		try
		{
			if (qs != null && qs.getExpressionList() != null)
			{
				for (DisplayExpression de : qs.getExpressionList())
				{
					de.extractAllExpressions(q, null);
					List<MeasureExpressionColumn> mecList = de.getMeasureExpressionColumns();
					if (mecList != null && mecList.size() > 0)
					{
						for (MeasureExpressionColumn mec : mecList)
						{
							if (mec.getBridgeQuery() != null)
							{
								sb.append("Navigation for measure expression bridge query: ");
								sb.append(mec.getBridgeQuery().getLogicalQueryString(false, null, false));
								sb.append(LINE_SEPARATOR);
								sb.append(getNavigationExplanation(null, mec.getBridgeQuery(), null, isSuperUser));
							}
							if (mec.getQuery() != null)
							{
								sb.append("Navigation for measure expression query: ");
								sb.append(mec.getQuery().getLogicalQueryString(false, null, false));
								sb.append(LINE_SEPARATOR);
								sb.append(getNavigationExplanation(null, mec.getQuery(), null, isSuperUser));
							}
						}
					}
				}
			}
			q.navigateQuery(true, false, null, true);
		} catch (NavigationException ex)
		{
			sb.append(ex.toString());
		}
		if (qs != null && qs.getExpressionList() != null && qs.getExpressionList().size() > 0)
		{
			sb.append("Navigation for main query:");
			if (originalQueryString != null)
				sb.append(originalQueryString);
			sb.append(LINE_SEPARATOR);
		}
		sb.append(q.explainNavigation(isSuperUser));
		return sb.toString();
	}

	/**
	 * Returns the physical query
	 * 
	 * @param qs
	 * @param q
	 * @param originalQueryString
	 * @return String
	 * @throws Exception
	 */
	public static String getPhysicalQuery(AbstractQueryString qs, Query q, String originalQueryString) throws Exception
	{
		StringBuilder sb = new StringBuilder();
		boolean failed = false;
		if (qs != null && qs.getServerConnectInfo() != null)
		{
			throw new Exception("Server directive can not be specified in query for generating physical query.");
		}
		try
		{
			if (qs != null && qs.getExpressionList() != null)
			{
				List<String> addedNames = new ArrayList<String>();
				for (DisplayExpression de : qs.getExpressionList())
				{
					de.extractAllExpressions(q, addedNames);
					List<MeasureExpressionColumn> mecList = de.getMeasureExpressionColumns();
					if (mecList != null && mecList.size() > 0)
					{
						for (MeasureExpressionColumn mec : mecList)
						{
							if (mec.getBridgeQuery() != null)
							{
								sb.append(LINE_SEPARATOR);
								sb.append("Physical query for measure bridge logical query: ");
								sb.append(mec.getBridgeQuery().getLogicalQueryString(false, null, false));
								sb.append(LINE_SEPARATOR);
								sb.append(getPhysicalQuery(null, mec.getBridgeQuery(), null));
							}
							if (mec.getQuery() != null)
							{
								sb.append(LINE_SEPARATOR);
								sb.append("Physical query for measure expression logical query: ");
								sb.append(mec.getQuery().getLogicalQueryString(false, null, false));
								sb.append(LINE_SEPARATOR);
								sb.append(getPhysicalQuery(null, mec.getQuery(), null));
							}
						}
					}
				}
			}
			q.generateQuery(true);
		} catch (NavigationException ex)
		{
			logger.error(ex.toString(), ex);
			sb.append(ex.toString());
			failed = true;
		}
		if (qs != null && qs.getExpressionList() != null && qs.getExpressionList().size() > 0)
		{
			sb.append(LINE_SEPARATOR);
			sb.append("Physical query for main logical query:");
			if (originalQueryString != null)
				sb.append(originalQueryString);
			sb.append(LINE_SEPARATOR);
		}
		if (!failed)
		{
			String pretty = q.getPrettyQuery();
			if (pretty != null)
				sb.append(pretty);
		}
		sb.append(LINE_SEPARATOR);
		return sb.toString();
	}

	/**
	 * Basic routine to see if any variables still exist, use for error reporting
	 * 
	 * @param query
	 * @return
	 */
	public static boolean isValidStatement(String query)
	{
		if (query.indexOf("V{") >= 0)
		{
			return false;
		}
		return true;
	}

	/**
	 * quote a string if it contains the separator
	 * 
	 * @param str
	 * @param separator
	 * @return
	 */
	public static String quote(String str, char separator)
	{
		if (str.indexOf(separator) >= 0)
		{
			str = str.replaceAll("\"", "\"\"");
			str = "\"" + str + "\"";
		}
		return str;
	}
	
	/**
	 * replace \ with \\ and ' with \'
	 * @param str
	 * @return
	 */
	public static String quote(String str)
	{
		if (str == null)
			return null;
		return str.replace("\\", "\\\\").replace("'", "\\'");
	}
	
	/**
	 * replace \ with \\ and ' with \\\' (subreport strings are embedded in a java string in the Jasper .java file and the first backslash is eaten)
	 * @param str
	 * @return
	 */
	public static String quoteForSubReport(String str)
	{
		if (str == null)
			return null;
		return str.replace("\\", "\\\\").replace("'", "\\'").replace("\'", "\\\'");
	}
	
	/**
	 * replace \' with ' and \\ with \
	 * @param str
	 * @return
	 */
	public static String unquote(String str)
	{
		if (str == null)
			return null;
		return str.replace("\\'", "'").replace("\\\\", "\\");
	}
	
	/**
	 * replace ' with \' where not already done
	 * @param str
	 * @return 
	 */
	public static String escapeQuotesIfExists(String str)
	{
		if (str == null)
			return null;
		if(str.indexOf("'") < 0)
			return str;

		StringBuilder sb = new StringBuilder();
		int strlen = str.length();
		for (int index = 0; index < strlen; index++)
		{
			char c = str.charAt(index);
			if (c == '\'')
			{
				if (!((index - 1) >= 0 && str.charAt(index - 1) == '\\'))
				{
					sb.append("\\");
				}
			}
			sb.append(c);
		}
		return sb.toString();
	}
	
	/**
	 * log code and java version information
	 */
	public static void logRuntimeInfo()
	{
		logger.debug("========");
		logger.debug("Birst Build: " + Util.getBuildString());
		logger.debug("Birst Engine Repository Version: " + Repository.RepositoryVersion);
		logger.debug("smi.home: " + System.getProperty("smi.home"));
		logger.debug("Host: " + Util.getHostName() + "/" + Util.getIPAddress());
		logger.debug("OS: " + System.getProperty("os.name") + " / " + System.getProperty("os.version") + " / " + System.getProperty("sun.os.patch.level"));
		logger.debug("Architecture: " + System.getProperty("os.arch"));
		logger.debug("Java VM Name: " + System.getProperty("java.vm.name"));
		logger.debug("Java VM Vendor: " + System.getProperty("java.vm.vendor"));		
		logger.debug("Java version: " + System.getProperty("java.version")
				+ " / " + System.getProperty("java.runtime.version") 
				+ " / " + System.getProperty("java.specification.version")
				+ " / " + System.getProperty("java.vm.version"));
		logger.debug("Java maximum memory setting: " + (int) (((double) Runtime.getRuntime().maxMemory()) / 1000000) + " Mbytes");
		logger.debug("Available processors: " + Runtime.getRuntime().availableProcessors());
		logger.debug("Log File: " + LogUtil.getLogfileName());
		logger.debug("========");		
	}

	/**
	 * return a 'clean' file name that works on windows, mac, and unix
	 * @param fileName
	 * @return
	 */
	public static String cleanFilename(String fileName)
	{
		if (fileName == null || fileName.isEmpty())
			return "file.txt"; // fail-safe
		return fileName.replace('/', '_').replace('\\', '_').replace('*', '_').replace('&', '_').replace(';', '_').replace(':', '_').replace('|', '_').replace(
				'?', '_').replace(',', '_').replace(' ', '_').replace('<', '_').replace('>', '_').replace('[', '_').replace(']', '_').replace('(', '_')
				.replace(')', '_').replace('\n', '_').replace('\r', '_');
	}

	/**
	 * return the hostname for this host, if problems return 'localhost'
	 * 
	 * @return
	 */
	public static String getHostName()
	{
		if (hostName == null)
		{
			try
			{
				hostName = java.net.InetAddress.getLocalHost().getHostName();
			} catch (IOException ie)
			{
				hostName = "localhost";
			}
		}
		return hostName;
	}
	
	private static String getIPAddress()
	{
		if (ipAddress == null)
		{
			try
			{
				InetAddress tmp = java.net.InetAddress.getLocalHost();
				ipAddress = tmp.toString();
			} catch (IOException ie)
			{
				ipAddress = "localhost/127.0.0.l";
			}
		}
		return ipAddress;
	}

	/**
	 * Returns first capital letter as string
	 * 
	 * @param s
	 * @return String
	 */
	public static String firstCap(String s)
	{
		return (s.charAt(0) + (s.length() > 0 ? s.substring(1).toLowerCase() : ""));
	}

	/**
	 * create directory if it does not exist
	 * 
	 * @param dir
	 *            the directory string
	 * @return boolean true if successful
	 */
	public static boolean createDirectory(String dir)
	{
		if (dir == null || dir.isEmpty())
			return (false);
		File cd = new File(dir);
		if (!cd.exists())
		{
			String parent = cd.getParent();
			if (!Util.createDirectory(parent))
				return false;
			logger.debug("Creating directory:" + dir);
			if (!cd.mkdir())
			{
				logger.error("Could not create directory: " + dir);
				return false;
			}
		}
		return true;
	}

	public static void deleteFiles(String path, String searchPattern)
	{
		deleteFiles(path, searchPattern, Long.MAX_VALUE);
	}

	/**
	 * Deletes files from a given directory that matches the search pattern provided
	 * 
	 * @param path
	 *            Directory
	 * @param searchPattern
	 */
	public static void deleteFiles(String path, String searchPattern, long olderThanDate)
	{
		File pathf = new File(path);
		if (!pathf.exists() || !pathf.isDirectory())
		{
			logger.warn("Directory \"" + path + "\" does not exist. Files with search pattern " + searchPattern + " will not be deleted.");
			return;
		}
		if ((searchPattern != null) && (!searchPattern.trim().equals("")))
		{
			SMIFilenameFilter ff = new SMIFilenameFilter();
			ff.setSearchPattern(searchPattern);
			String[] fnames = pathf.list(ff);
			if ((fnames != null) && (fnames.length > 0))
			{
				for (String fname : fnames)
				{
					File f = new File(path + File.separator + fname);
					if (f.isFile() && f.exists())
					{
						if (f.lastModified() < olderThanDate)
						{
							try
							{
								Util.secureDelete(f);
								if (logger.isTraceEnabled())
									logger.trace("Deleted file " + f.getAbsolutePath());
							} catch (IOException ioex)
							{
								logger.warn("Unable to delete file " + f.getAbsolutePath(), ioex);
							}
						}
					}
				}
			} else
			{
				if (logger.isTraceEnabled())
					logger.trace("No files found with search pattern " + searchPattern);
			}
		}
	}
	
	public static String convertToString(Object o, boolean encloseValueInQuotes, Repository r, boolean ignoreQueryLanguageRestriction)
	{
		return convertToString(o, encloseValueInQuotes, r, ignoreQueryLanguageRestriction, null);
	}
	
	/**
	 * converts the object to string and encose in quotes based on the flag
	 * If database connection is supplied, then the String object is formatted to escape single quotes
	 * @param o
	 * @param encloseValueInQuotes
	 * @param r
	 * @param ignoreQueryLanguageRestriction
	 * @param dbc
	 * @return
	 */
	public static String convertToString(Object o, boolean encloseValueInQuotes, Repository r, boolean ignoreQueryLanguageRestriction, DatabaseConnection dbc)
	{
		String optionalQuoteChar = null;
		if(ignoreQueryLanguageRestriction)
		{
			optionalQuoteChar = encloseValueInQuotes ? "'" : "";
		}
		else
		{
			optionalQuoteChar = (encloseValueInQuotes && !r.isUseNewQueryLanguage())? "'" : "";
		}
		
		// If it's a string, return it with quotes
		if (o.getClass() == String.class)
		{
			String value = (String)o;
			if(optionalQuoteChar == "'")
			{
				value = dbc != null ? dbc.escapeLiteral(value) : value;
			}
			return (optionalQuoteChar + value + optionalQuoteChar);
		}
		// If it's a datetime variable, convert it to a string and quote it
		if (o.getClass() == Date.class)
			return (optionalQuoteChar + o.toString() + optionalQuoteChar);
		if (o instanceof Calendar)
		{
			SimpleDateFormat sdf = new SimpleDateFormat(DateUtil.DB_FORMAT_STRING);
			sdf.setTimeZone(r.getServerParameters().getProcessingTimeZone());
			sdf.setLenient(false);
			return (optionalQuoteChar + sdf.format(((Calendar) o).getTime()) + optionalQuoteChar);
		}
		// Otherwise, it's a number so don't quote
		return (o.toString());
	}

	/**
	 * 
	 * @param o
	 *            object to be determined if its not a String,Date or Calender Instance
	 * @return boolean
	 */
	public static boolean isNumber(Object o)
	{
		if (o == null)
		{
			return false;
		}
		boolean numberType = false;
		if (o.getClass() == String.class || o.getClass() == Date.class || o instanceof Calendar)
		{
			numberType = false;
		} else
		{
			// If not a String, Date or Calendar assume it's an integer
			numberType = true;
		}
		return numberType;
	}

	/**
	 * securely delete a file - write over with zeros, then ones, then random
	 * 
	 * @param file
	 * @throws IOException
	 */
	public static boolean secureDelete(File file) throws IOException
	{
		if (file.exists())
		{
			/*
			 * This secure delete algorithm is not workable - commenting out
			 */
			return file.delete();
			/*
			 * logger.info("Securely deleting " + file.getAbsolutePath()); long length = file.length(); byte[] data =
			 * new byte[64];
			 */
			/*
			 * one pass with random is good enough with the current disk technology // write zeros
			 * java.util.Arrays.fill(data, (byte) 0x00); writePattern(file, data);
			 * 
			 * // write ones data = new byte[64]; java.util.Arrays.fill(data, (byte) 0xff); writePattern(file, data);
			 */
			// write random
			/*
			 * RandomAccessFile raf = new RandomAccessFile(file, "rws"); int pos = 0; raf.seek(0); SecureRandom random =
			 * new SecureRandom(); while (pos < length) { random.nextBytes(data); raf.write(data); pos += data.length; }
			 * raf.close(); return file.delete();
			 */
		}
		return true;
	}

	@SuppressWarnings("unused")
	private static void writePattern(File file, byte[] data) throws IOException
	{
		long length = file.length();
		RandomAccessFile raf = new RandomAccessFile(file, "rws");
		int pos = 0;
		raf.seek(0);
		while (pos < length)
		{
			raf.write(data);
			pos += data.length;
		}
		raf.close();
	}

	public static void startCheckIn(String checkInFileName) 
	{
		CheckInThread checkInThread = new CheckInThread(checkInFileName);
		checkInThread.setDaemon(true);
		checkInThread.start();
	}
	
	public static boolean isTerminated(String killFileName)
	{
		boolean terminate = false;
		if(killFileName != null && killFileName.trim().length() > 0)
		{
			try
			{
				terminate = new File(killFileName).exists();
			}catch(Exception ex)
			{
				logger.warn("Exception while checking for terminate request: "  + killFileName);
			}
		}
		return terminate;
	}
	
	public static String getKillFileName(String repositoryRootPath, String name)
	{
		return repositoryRootPath + File.separator + name;
	}
	
	public static void deleteFile(String killFileName)
	{		
		try
		{	
			File file = new File(killFileName);
			if(file.exists())
			{
				logger.debug("Clearing out the lock file" + killFileName);
				file.delete();
			}	
		}
		catch(Exception ex)
		{
			logger.warn("Error while clearing out the kill lock file " +  killFileName);
		}
	}
	
	public static void exitNow(String killFileName)
	{
		if(killFileName != null)
		{
			try
			{
				File file = new File(killFileName);
				if(file.exists())
				{
					file.delete();
				}
			}catch(Exception ex)
			{
				logger.warn("Error while deleting file " + killFileName, ex);
			}
		}
		logger.warn("Process exiting due to detection of : " + killFileName + " : at " + new Date(System.currentTimeMillis()));
		System.exit(-222);
	}
	
	public static boolean containsVariable(List<Variable> variableList, Variable variable)
	{
		boolean contains = false;
		if (variableList == null || variableList.isEmpty())
		{
			return false;
		}
		for (Variable v : variableList)
		{
			if (v.getName().equals(variable.getName()) && v.getType() == variable.getType())
			{
				contains = true;
				break;
			}
		}
		return contains;
	}
	
	public static long getProcessID()
	{
		RuntimeMXBean bean = ManagementFactory.getRuntimeMXBean();
		return Long.valueOf(bean.getName().split("@")[0]);
	}
	
	/**
	 * much more efficient than str.trim().length() > 0
	 * @param str
	 * @return
	 */
	public static boolean hasNonWhiteSpaceCharacters(String str)
	{
		if (str == null)
			return false;
		char[] arr = str.toCharArray();
		int len = arr.length;
		for (int i = 0; i < len; i++)
		{
			if (!Character.isWhitespace(arr[i]))
				return true;
		}
		return false;
	}
	
	public static boolean containsOnlyConnection(List<DatabaseConnection> connectionList, String connectionName)
	{
		if (connectionName != null && connectionList != null)
		{
			if (connectionList.size() == 1 && connectionName.equalsIgnoreCase(connectionList.get(0).Name))
				return true;
		}
		return false;
	}
	
	public static String convertMulValueVariablesToNewLanguageSyntax(Repository r, String query)
	{
		//Pattern V{.*?} means V{... and first(smallest possible) match for }
		//i.e. logical query can have multiple variables using and/or operators in where clause
		if (r != null && query != null  && query.indexOf("V{") > 0)
		{
			Pattern p = Pattern.compile("'V\\{.*?\\}'");
            Matcher m = p.matcher(query);
            while (m.find())
            {
                String oldVarName = m.group();
                String newVarName = oldVarName.substring(3, oldVarName.length()-2);
                Variable v = r.findVariable(newVarName);
                if (v != null && v.isMultiValue())
                	query = query.replace(oldVarName, "GetVariable('" + newVarName + "')");
            }
            p = Pattern.compile("#V\\{.*?\\}#");
            m = p.matcher(query);
            while (m.find())
            {
                String oldVarName = m.group();
                String newVarName = oldVarName.substring(3, oldVarName.length()-2);
                Variable v = r.findVariable(newVarName);
                if (v != null && v.isMultiValue())
                	query = query.replace(oldVarName, "GetVariable('" + newVarName + "')");
            }
            p = Pattern.compile("V\\{.*?\\}");
            m = p.matcher(query);
            while (m.find())
            {
                String oldVarName = m.group();
                String newVarName = oldVarName.substring(2, oldVarName.length()-1);
                Variable v = r.findVariable(newVarName);
                if (v != null && v.isMultiValue())
                	query = query.replace(oldVarName, "GetVariable('" + newVarName + "')");
            }
		}
		return query;
	}
	
	public static String removeCRLF(String str)
	{
		if (str == null)
			return null;
		return str.replace('\n', '_').replace('\r', '_'); 
	}

	public static void touchRepositoryFile(Repository r) {
		String fileName = null;
		try
		{
			fileName = r.getRepositoryRootPath() + File.separator + "repository.xml";
			File file = new File(fileName);
			if(file.exists())
			{
				file.setLastModified(System.currentTimeMillis());
			}
		}
		catch(Exception ex)
		{
			logger.warn("Could not touch the repository file : " + fileName, ex);
		}
	}
	
	public static String nullifyString(String a){
		if(a != null && a.trim().length() == 0){
			return null;
		}
		return a;
	}

	/***
	 * returns list of space properties
	 * 
	 * @param otherSpacesInfo - Input in the below format for multiple spaces
	 * spaceID=<spaceID1>,spaceName=<spaceName1,spaceDirectory=<spaceDirectory1>;spaceID=<spaceID2>,spaceName=<spaceName2>,spaceDirectory=<spaceDirectory2>;
	 */	
	public static List<PackageSpaceProperties> getPackageSpacePropertiesList(String[] packageSpacePropertiesArray) {
		List<PackageSpaceProperties> response = new ArrayList<PackageSpaceProperties>();
		if(packageSpacePropertiesArray != null && packageSpacePropertiesArray.length > 0)
		{	
			for(String packageSpaceInfo : packageSpacePropertiesArray)
			{
				String[] properties = packageSpaceInfo.split(",");
				PackageSpaceProperties packageSpaceProperties = new PackageSpaceProperties();
				for(String property : properties)
				{
					String[] nameValue = property.split("=");
					if(nameValue.length != 2)
					{
						logger.warn("Skipping the space property. Invalid format : " + nameValue);
						continue;
					}

					String propName = nameValue[0];
					String propValue = nameValue[1];

					if(propName.equalsIgnoreCase(PackageSpaceProperties.SPACE_ID))
					{
						packageSpaceProperties.setSpaceID(propValue);
					} 
					else if(propName.equalsIgnoreCase(PackageSpaceProperties.PACKAGE_ID))
					{
						packageSpaceProperties.setPackageID(propValue);
					}
					else if(propName.equalsIgnoreCase(PackageSpaceProperties.PACKAGE_NAME))
					{
						packageSpaceProperties.setPackageName(propValue);
					}
					else if(propName.equalsIgnoreCase(PackageSpaceProperties.SPACE_DIR))
					{
						packageSpaceProperties.setSpaceDirectory(propValue);
					} 
					else if(propName.equalsIgnoreCase(PackageSpaceProperties.SPACE_NAME))
					{
						packageSpaceProperties.setSpaceName(propValue);
					}
				}

				if(packageSpaceProperties.validateAllRequiredProperties())
				{
					response.add(packageSpaceProperties);
				}
				else
				{
					logger.warn("Skipping the space information : not all properties are given ");
					packageSpaceProperties.logProperties();
				}
			}
		}
		return response;
	}

	public static String anyMatch(String[] array1, String[] array2) {
		if(array1 != null && array1.length > 0  && array2 != null && array2.length > 0)
		{
			for(String str1 : array1)
			{
				for(String str2 : array2)
				{
					if(str1.equals(str2))
					{
						return str1;
					}
				}
			}
		}
		return null;
	}
	
	public static String[] splitLine (String line, SourceFile sf, int maxErrorsAndWarnings, AtomicLong errorCount) throws Exception
	{
		char[] linearr = line.toCharArray();
		int pos = 0;
		int sepchar = 0;
		char[] separr = sf.Separator.toCharArray();

		String[] pline = new String[sf.Columns.length];
		if (sf.Type == FileFormatType.FixedWidth)
		{
			for (int i = 0; i < sf.Columns.length; i++)
			{
				SourceColumn sc = sf.Columns[i];
				pline[i] = line.substring(pos, pos + sc.Width);
				pos += sc.Width;
			}
		} else
		{
			int col = 0;
			boolean quote = false;
			boolean lastEscape = false;
			boolean replaceEscapedReturns = false;
			boolean lastSep = true;
			boolean isPreviousCharQuote = false;
			for (int i = 0; i < linearr.length; i++)
			{
				char ch = linearr[i];
				if (quote && sf.OnlyQuoteAfterSeparator)
				{
					if (isPreviousCharQuote && ch == separr[sepchar])//found separator after quote
					{
						quote = false;
					}
				}
				
				if (ch == sf.Quote && !lastEscape && (!sf.OnlyQuoteAfterSeparator || lastSep || quote))
					quote = !quote;
				else if (!quote)
				{
					if (ch == separr[sepchar])
					{
						sepchar++;
					} else if (sepchar > 0)
					{
						sepchar = 0;
					}
					if (sepchar > 0 && sepchar == sf.Separator.length())
					{
						if (line.charAt(pos) == sf.Quote && line.charAt(i - sepchar) == sf.Quote)
						{
							if((pos + 1) > (i - sepchar))
							{
								if (errorCount.get() < maxErrorsAndWarnings)
									logger.warn("Unable to process line [" + line + "]: pos=" + pos +", i=" + i + ",sepchar=" + sepchar);
								errorCount.incrementAndGet();
								return null;
								// throw new Exception("Line processing error: pos=" + pos +", i=" + i + ",sepchar=" + sepchar + ",line=" + line);
							}
							pline[col] = line.substring(pos + 1, i - sepchar);
							pline[col] = Util.replaceStr(pline[col], "\"\"", "\"");
						} else
							pline[col] = line.substring(pos, i - sepchar + 1);
						if (replaceEscapedReturns)
						{
							pline[col] = Util.replaceStr(pline[col], "\\\n", "\n");
							pline[col] = Util.replaceStr(pline[col], "\\\"", "\"");
							pline[col] = Util.replaceStr(pline[col], "\\\\", "\\");
						}
						col++;
						pos = i + 1;
						lastSep = true;
						if (col == sf.Columns.length)
							break;
						sepchar = 0;
					} else
						lastSep = false;
				}
				if (lastEscape && (ch == '\n' || ch == sf.Quote || ch == '\\'))
					replaceEscapedReturns = true;
				if (ch == '\\' && !lastEscape)
					lastEscape = true;
				else
					lastEscape = false;
				if (ch == sf.Quote)
				{
					isPreviousCharQuote = true;
				}
				else
				{
					isPreviousCharQuote = false;
				}
			}
			if (pos < linearr.length && col < sf.Columns.length)
			{
				try
				{
					if (linearr[pos] == sf.Quote && linearr[linearr.length - sepchar - 1] == sf.Quote)
						pline[col] = line.substring(pos + 1, linearr.length - sepchar - 1);
					else
						pline[col] = line.substring(pos);
					if (replaceEscapedReturns)
					{
						pline[col] = Util.replaceStr(pline[col], "\\\n", "\n");
						pline[col] = Util.replaceStr(pline[col], "\\\"", "\"");
						pline[col] = Util.replaceStr(pline[col], "\\\\", "\\");
					}
					col++;
				} catch (Exception ex)
				{
					if (errorCount.get() < maxErrorsAndWarnings)
						logger.warn("Unable to process line [" + line + "] " + ex.toString());
					errorCount.incrementAndGet();
					return null;
				}
			}
			/*
			 * If the last column is an empty string, it may not have been processed earlier so set it to empty
			 * string
			 */
			if (col < pline.length)
			{
				for (int i = col; i < pline.length; i++)
				{
					pline[i] = "";
				}
			}
		}
		return pline;
	}
	
	public static void cleanUpServiceClient(ServiceClient client){
		try{
			if(client != null){
				client.cleanupTransport();
			}
		}catch(Exception ex){
			logger.warn("cleanUpServiceClient() : Exception while cleaning up transport", ex);
		}
	}

	public static void cleanUpIdleConnections(ConfigurationContext configContext){
		try{
			if(configContext != null)
			{
				Object obj = configContext.getProperty(HTTPConstants.MULTITHREAD_HTTP_CONNECTION_MANAGER);
				if(obj != null)
				{	
					MultiThreadedHttpConnectionManager connManager = (MultiThreadedHttpConnectionManager)obj;
					connManager.closeIdleConnections(0);
				}
			}
		}catch(Exception ex){
			logger.warn("cleanUpIdleConnections() : Exception while cleaning up idle connections", ex);
		}
	}
	

	/**
	 * Any changes to this should also be reflected in BaseConnector.configureConnectionParams(..) also
	 * @param configContext
	 */
	public static void configureConnectionParams(ConfigurationContext configContext) {
		if(configContext != null)
		{
			Object obj = configContext.getProperty(HTTPConstants.MULTITHREAD_HTTP_CONNECTION_MANAGER);
			if(obj != null)
			{
				HttpConnectionManagerParams params = new HttpConnectionManagerParams();
				params.setDefaultMaxConnectionsPerHost(BaseConnector.MAX_CONNECTIONS_PER_HOST);
				params.setMaxTotalConnections(BaseConnector.MAX_TOTAL_CONNECTIONS);
				MultiThreadedHttpConnectionManager connManager = (MultiThreadedHttpConnectionManager)obj;
				connManager.setParams(params);
				HttpClient httpClient = (HttpClient) configContext.getProperty(HTTPConstants.CACHED_HTTP_CLIENT);
				if(httpClient == null){
					httpClient = new HttpClient(connManager);
					httpClient.getParams().setConnectionManagerTimeout(30000);			
					// same timeout that is used for other outgoing axis2 client calls
					Util.initializeTimeouts(httpClient, (int)SFDCConnector.timeoutInMilliseconds);
					configContext.setProperty(HTTPConstants.CACHED_HTTP_CLIENT, httpClient);
				}				
			}
		}
	}
	
	/**
	 * Initialize timeouts for httpClient.
	 * This will set both the connection and socket timeouts
	 * @param httpClient
	 * @param timeout
	 */
	public static void initializeTimeouts(HttpClient httpClient, int timeout) {
		httpClient.getParams().setConnectionManagerTimeout(30000);
		httpClient.getHttpConnectionManager().getParams().setConnectionTimeout((int) timeout);
		httpClient.getHttpConnectionManager().getParams().setSoTimeout(timeout);
		httpClient.getParams().setSoTimeout(timeout);
	}

	public static int getDBWaitTimeout()
	{
		return dbWaitTimeout;
	}
	
	public static void setDBWaitTimeout(int waitOutValue)
	{
		dbWaitTimeout = waitOutValue;
	}
	
	public static boolean startsWithAnAlias(String s,String alias){
		return ((s!=null && s.startsWith(alias)) ? true : false);
	}
	
	public static String getSurrogateKeyColumnPhysicalName(String columnName, Object table, boolean isMeasuretable, DatabaseConnection dconn,Repository r,int maxLength)
	{
		String matchLogicalCol = columnName;
		Object currentCol = null;
		if (isMeasuretable)
		{
			currentCol = ((MeasureTable)table).findColumn(matchLogicalCol);
		}
		else
		{
			currentCol = ((DimensionTable)table).findColumn(matchLogicalCol);
		}
		String skcol = null;
		if(currentCol == null)
		{
			skcol = Util.replaceWithNewPhysicalString(Util.replaceWithPhysicalString( dconn,columnName,r,maxLength),dconn,r);
		}
		else
		{
			skcol = isMeasuretable ? ((MeasureColumn)currentCol).PhysicalName : ((DimensionColumn)currentCol).PhysicalName;
			if (skcol.indexOf(".") >= 0)
			{
				skcol = skcol.substring(skcol.indexOf('.') + 1);
			}
		}
		return skcol;
	}

	public static AWSCredentials getAWSCredentions(String awsAccessKeyId,
			String awsSecretKey, String awsToken) {
		
		AWSCredentials s3credentials = null;
		if (awsAccessKeyId != null && awsSecretKey != null && awsToken != null)
			s3credentials = new BasicSessionCredentials(awsAccessKeyId, awsSecretKey, awsToken);
		else if (awsAccessKeyId != null && awsSecretKey != null && awsToken == null)
			s3credentials = new BasicAWSCredentials(awsAccessKeyId, awsSecretKey);
		
		return s3credentials; 
	}

	public static void copyFile(File sourceFile, File destFile) throws IOException
	{
		if (!destFile.exists())
		{
			destFile.createNewFile();
		}
		FileChannel source = null;
		FileChannel destination = null;
		try
		{
			source = new FileInputStream(sourceFile).getChannel();
			destination = new FileOutputStream(destFile).getChannel();
			destination.transferFrom(source, 0, source.size());
		} finally
		{
			if (source != null)
			{
				source.close();
			}
			if (destination != null)
			{
				destination.close();
			}
		}
	}
	
	public static DecimalFormat getDecimalFormatForRepository()
	{
		DecimalFormat format = new DecimalFormat();
        format.setDecimalSeparatorAlwaysShown(false);
        format.setGroupingUsed(false);
        return format;
	}
	
	/**
	 * Forked by MoveSpace get a matching hash to generate schema.
	 * Do not use in any regular Processing Engine Code
	 * */
	public static void getCheckSum(String name){
		name = "x" + Integer.toHexString(name.hashCode());
		System.out.println("[HashCode]:"+name);
	}
}
