/**
 * 
 */
package com.successmetricsinc.util;

import java.util.Iterator;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.log4j.Logger;

/**
 * @author agarrison
 *
 */
public class XMLStreamIterator implements Iterator<Object> {
	private static Logger logger = Logger.getLogger(XMLStreamIterator.class);
	private XMLStreamReader parser;
	private String lastLocalName = null;
	private String firstLocalName;
	
	public XMLStreamIterator(XMLStreamReader p) {
		parser = p;
		if (parser.hasName())
			firstLocalName = parser.getLocalName();
	}
	@Override
	public boolean hasNext() {
		if (! (parser.getEventType() == XMLStreamConstants.END_ELEMENT && parser.hasName() && parser.getLocalName().equals(firstLocalName)))
			iterate();
		return (parser.getEventType() != XMLStreamConstants.END_ELEMENT && parser.getEventType() != XMLStreamConstants.END_DOCUMENT);
	}

	@Override
	public Object next() {
		return null;
	}
	
	private void iterate() {
		try {
			for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser.next()) {
				switch (event) {
				case XMLStreamConstants.END_ELEMENT:
					// processing didn't getText, need to skip to next thing.
					if (parser.getLocalName().equals(lastLocalName))
						continue;
					
					if (parser.getLocalName().equals(firstLocalName))
						return;
					break;
					
				case XMLStreamConstants.START_ELEMENT:
					if (firstLocalName == null)
						firstLocalName = parser.getLocalName();
					lastLocalName = parser.getLocalName();
					return;
				}
			}
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			logger.warn(e, e);
		}
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException("Can not remove from an XMLStreamIterator");
		
	}

}
