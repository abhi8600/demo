package com.successmetricsinc.util;

public class UserNotLoggedInException extends BaseException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserNotLoggedInException()
	{
		super();
	}
	
	public UserNotLoggedInException(String s)
	{
		super(s);
	}
	
	public UserNotLoggedInException(String s, Throwable cause)
	{
		super(s, cause);
	}

	public int getErrorCode()
	{
		return ERROR_NOT_LOGGED_IN;
	}
}
