/**
 * $Id: SMIRegExFileFilter.java,v 1.1 2010-05-11 16:27:21 ricks Exp $
 *
 * Copyright (C) 2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.util;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.log4j.Logger;

public class SMIRegExFileFilter implements FilenameFilter 
{
	private static Logger logger = Logger.getLogger(SMIRegExFileFilter.class);
	Pattern pattern = null;
	
	public void setSearchPattern(String searchPattern)
	{
		try
		{
			pattern = Pattern.compile(searchPattern);
		}
		catch (PatternSyntaxException pse)
		{
			logger.warn("Bad search pattern in SMIRegExFileFilter: " + searchPattern);
		}
	}
	
	public boolean accept(File dir, String name)
	{
		if (pattern == null || name == null)
		{
			return false;
		}
		Matcher matcher = pattern.matcher(name);
		return matcher.find();
	}
}
