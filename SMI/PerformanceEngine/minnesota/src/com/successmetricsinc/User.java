/**
 * $Id: User.java,v 1.6 2011-09-28 16:05:14 ricks Exp $
 *
 * Copyright (C) 2007-2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc;

import java.io.Serializable;

import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.util.XmlUtils;

public class User implements Serializable
{
    private static final long serialVersionUID = 1L;
    private String userName;
    private String hashedPassword; // password is hashed
    private String emailAddress;
    private String Fullname;
    private String password;
    
    public boolean Changeable = false;
    
    public User()
    {
    	
    }
    
    public User(Element e,Namespace ns)
    {
    	userName = e.getChildText("Username", ns);
    	Fullname = e.getChildText("Fullname", ns);
    	password = e.getChildText("Password", ns);
    }
    
    public Element getUserElement(Namespace ns)
    {
      Element e = new Element("User",ns);

      Element child = new Element("Username",ns);
      child.setText(userName);
      e.addContent(child);

      if (password!=null)
      {
        child = new Element("Password",ns);
        child.setText(password);
        e.addContent(child);
      }

      if (Fullname!=null)
      {
        XmlUtils.addContent(e, "Fullname", Fullname,ns);
      }		

      return e;
    }
	
	public String toString()
	{
		return getUserName();
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserName() {
		return userName;
	}

	public void setHashedPassword(String hashedPassword) {
		this.hashedPassword = hashedPassword;
	}

	public String getHashedPassword() {
		return hashedPassword;
	}

	public String getEmailAddress() {
		return emailAddress;
	}
}
