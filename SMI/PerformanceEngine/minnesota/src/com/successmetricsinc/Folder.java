/**
 * $Id: Folder.java,v 1.11 2012-12-13 09:31:04 BIRST\mjani Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.successmetricsinc;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.Collection;
import java.util.regex.Matcher;

import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.util.Util;

public class Folder implements GenericFolder, Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private Folder parent;
	private Folder[] subfolders;
	private Column[] columns;
	private String[] groups;
	
	public enum FolderType {
		normal,
		olapDimension,
		olapHierarchy,
		olapMember
	}
	private FolderType type = FolderType.normal;

	public Folder(Element e, Namespace ns, Folder parent)
	{
		name = e.getChildTextTrim("Name", ns);
		this.parent = parent;
		groups = Repository.getStringArrayChildren(e, "Groups", ns);
		Element fe = e.getChild("Subfolders", ns);
		if (fe != null)
		{
			@SuppressWarnings("rawtypes")
			List l2 = fe.getChildren();
			subfolders = new Folder[l2.size()];
			for (int count2 = 0; count2 < l2.size(); count2++)
			{
				Element cfe = (Element) l2.get(count2);
				subfolders[count2] = new Folder(cfe, ns, this);
			}
		}
		Element ce = e.getChild("Columns", ns);
		if (ce != null)
		{
			@SuppressWarnings("rawtypes")
			List l2 = ce.getChildren();
			columns = new Column[l2.size()];
			for (int count2 = 0; count2 < l2.size(); count2++)
			{
				Element cce = (Element) l2.get(count2);
				Column c = new Column(cce, ns);
				columns[count2] = c;
			}
		}
	}
	
	public Element getFolderElement(Namespace ns)
	{
		Element e = new Element("Folder",ns);
		
		Element child = new Element("Name",ns);
		child.setText(name);
		e.addContent(child);
		
		if (subfolders!=null)
		{
			child = new Element("Subfolders",ns);
			for (Folder subFolder : subfolders)
			{
				child.addContent(subFolder.getFolderElement(ns));
			}
			e.addContent(child);
		}
		
		if (columns!=null)
		{
			child = new Element("Columns",ns);
			for (Column column : columns)
			{
				child.addContent(column.getColumnElement(ns));
			}
			e.addContent(child);
		}
		
		if (groups!=null && groups.length>0)
		{
			e.addContent(Repository.getStringArrayElement("Groups", groups, ns));
		}
				
		return e;
	}

	public Folder()
	{
	}

	public String toString()
	{
		return (this.getName());
	}

	@Override
	public List<GenericFolder> getSubFolders(List<Group> memberGroups)
	{
		if (getSubfolders() == null)
			return (null);
		List<GenericFolder> folderList = new ArrayList<GenericFolder>();
		for (Folder f : getSubfolders())
		{
			/*
			 * By default, grant access to a folder. If no groups then, just allow access
			 */
			if ((f.getGroups() == null) || (f.getGroups().length == 0) || Util.hasAccess(memberGroups, Arrays.asList(f.getGroups())))
			{
				folderList.add(f);
			}
		}
		return (folderList);
	}

	public List<GenericFolderEntry> getChildren(String filter, Map<String, Set<Object>> dimRelationshipMap, Map<String, Set<Object>> measureRelationshipMap)
	{
		List<GenericFolderEntry> list = new ArrayList<GenericFolderEntry>();
		java.util.regex.Pattern p = null;
		if (filter != null)
			p = java.util.regex.Pattern.compile(filter);
		if (getColumns() != null)
		{
			for (Column c : getColumns())
			{
				boolean matches = columnIsNavigatable(c, dimRelationshipMap, measureRelationshipMap);
				if (matches && p != null)
				{
					Matcher m = p.matcher(c.getName());
					matches = m.matches();
				}
				if (matches)
				{
					list.add(c);
				}
			}
		}
		return (list);
	}
	
	public boolean columnIsNavigatable(Column c, Map<String, Set<Object>> dimRelationshipMap, Map<String, Set<Object>> measureRelationshipMap) {
		/*
		 * See if a relationship exists. In this context relationship maps include measure or dimension tables
		 * to evaluate for a join relationship. 
		 */
		boolean isDimCol = c.getDimension() != null;				
		boolean matches = false;
		if (dimRelationshipMap.isEmpty() && measureRelationshipMap.isEmpty() && c.getJoinableTables() != null)
		{
			matches = true;
		}
		else
		{
			if(isDimCol)
			{
				if(measureRelationshipMap.isEmpty())
				{
					// There are no measures, so make sure that this dimension column joins with at least one dimension
					matches = matchOneRelationship(c, dimRelationshipMap);
				}
				else
				{
					//Make sure that this dimension column joins with all measures
					matches = matchAllRelationships(c, measureRelationshipMap);
				}
			}
			else
			{
				if(dimRelationshipMap.isEmpty())
				{
					// There are no dimensions, so make sure that this measure column joins with at least one measure
					matches = matchOneRelationship(c, measureRelationshipMap);
				}
				else
				{
					//Make sure that this measure column joins with all dimensions
					matches = matchAllRelationships(c, dimRelationshipMap);
				}
			}
		}
		return matches;
	}

	public static boolean isNavigatable(Column c, Map<String, Set<Object>> dimRelationshipMap, Map<String, Set<Object>> measureRelationshipMap) {
		/*
		 * See if a relationship exists. In this context relationship maps include measure or dimension tables
		 * to evaluate for a join relationship. 
		 */
		boolean isDimCol = c.getDimension() != null;				
		boolean matches = false;
		if (dimRelationshipMap.isEmpty() && measureRelationshipMap.isEmpty() && c.getJoinableTables() != null)
		{
			matches = true;
		}
		else
		{
			if(isDimCol)
			{
				if(measureRelationshipMap.isEmpty())
				{
					// There are no measures, so make sure that this dimension column joins with all dimensions
					matches = statMatchAllRelationships(c, dimRelationshipMap);
				}
				else
				{
					//Make sure that this dimension column joins with all measures
					matches = statMatchAllRelationships(c, measureRelationshipMap);
				}
			}
			else
			{
				if(dimRelationshipMap.isEmpty())
				{
					// There are no dimensions, so make sure that this measure column joins with at least one measure
					matches = statMatchOneRelationship(c, measureRelationshipMap);
				}
				else
				{
					//Make sure that this measure column joins with all dimensions
					matches = statMatchAllRelationships(c, dimRelationshipMap);
				}
			}
		}
		return matches;
	}
	
	/**
	 * For a given column, go through the relation ship map and see if the column can be joined with any one entry in the relationship
	 * map. Return true if it joins, false otherwise
	 * @param c Column
	 * @param relationshipMap Key - column name, value - list of all dimension/measure tables that the column joins to
	 * @return true or false
	 */
	private static boolean statMatchOneRelationship(Column c, Map<String, Set<Object>> relationshipMap)
	{
		boolean matches = false;
		Collection<Set<Object>> values = relationshipMap.values();
		for(Set<Object> relationshipSet : values)
		{
			for (Object o : relationshipSet)
			{
				if((c.getJoinableTables().contains(o)))
				{
					matches = true;
					break;
				}
			}
			if(matches)
			{
				break;
			}
		}
		return matches;
	}
	
	/**
	 * For a given column, go through the relation ship map and see if the column can be joined with all entries in the relationship
	 * map. Return true if it joins, false otherwise
	 * @param c Column
	 * @param relationshipMap Key - column name, value - list of all dimension/measure tables that the column joins to
	 * @return true or false
	 */
	private static boolean statMatchAllRelationships(Column c, Map<String, Set<Object>> relationshipMap)
	{
		boolean matches = false;
		Collection<Set<Object>> values = relationshipMap.values();
		for(Set<Object> relationshipSet : values)
		{
			matches = false;
			for (Object o : relationshipSet)
			{
				if((c.getJoinableTables().contains(o)))
				{
					matches = true;
					break;
				}
			}
			if(!matches)
			{
				break;
			}
		}						
		return matches;
	}
	/**
	 * For a given column, go through the relation ship map and see if the column can be joined with any one entry in the relationship
	 * map. Return true if it joins, false otherwise
	 * @param c Column
	 * @param relationshipMap Key - column name, value - list of all dimension/measure tables that the column joins to
	 * @return true or false
	 */
	private boolean matchOneRelationship(Column c, Map<String, Set<Object>> relationshipMap)
	{
		boolean matches = false;
		Collection<Set<Object>> values = relationshipMap.values();
		for(Set<Object> relationshipSet : values)
		{
			for (Object o : relationshipSet)
			{
				if((c.getJoinableTables().contains(o)))
				{
					matches = true;
					break;
				}
			}
			if(matches)
			{
				break;
			}
		}
		return matches;
	}

	/**
	 * For a given column, go through the relation ship map and see if the column can be joined with all entries in the relationship
	 * map. Return true if it joins, false otherwise
	 * @param c Column
	 * @param relationshipMap Key - column name, value - list of all dimension/measure tables that the column joins to
	 * @return true or false
	 */
	private boolean matchAllRelationships(Column c, Map<String, Set<Object>> relationshipMap)
	{
		boolean matches = false;
		Collection<Set<Object>> values = relationshipMap.values();
		for(Set<Object> relationshipSet : values)
		{
			matches = false;
			for (Object o : relationshipSet)
			{
				if((c.getJoinableTables().contains(o)))
				{
					matches = true;
					break;
				}
			}
			if(!matches)
			{
				break;
			}
		}						
		return matches;
	}
	
	public String getDefaultFilter()
	{
		return (null);
	}

	public String getPath()
	{
		StringBuilder sb = new StringBuilder();
		Folder f = this;
		while (f != null)
		{
			sb.insert(0, f.getName() + File.separator);
			f = f.getParent();
		}
		return (sb.toString());
	}

	public void setGroups(String[] groups)
	{
		this.groups = groups;
	}

	public String[] getGroups()
	{
		return groups;
	}

	public void setColumns(Column[] columns)
	{
		this.columns = columns;
	}

	public Column[] getColumns()
	{
		return columns;
	}

	public void setSubfolders(Folder[] subfolders)
	{
		this.subfolders = subfolders;
	}

	public Folder[] getSubfolders()
	{
		return subfolders;
	}

	public void setParent(Folder parent)
	{
		this.parent = parent;
	}

	public Folder getParent()
	{
		return parent;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getName()
	{
		return name;
	}

	public FolderType getType() {
		return type;
	}

	public void setType(FolderType type) {
		this.type = type;
	}
}