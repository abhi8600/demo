/**
 * $Id: GenericFolder.java,v 1.3 2008-07-15 08:21:41 mjani Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc;

import java.util.List;
import java.util.Set;
import java.util.Map;

/**
 * @author bpeters
 * 
 */
public interface GenericFolder
{
	public List<GenericFolder> getSubFolders(List<Group> memberGroups);

	/**
	 * Get all children for the folder
	 * 
	 * @param filter
	 *            Filter to apply (null if none)
	 * @param dimRelationshipMap
	 *            A map containing all related tables for dimension columns
	 * @param measureRelationshipMap
	 *            A map containing all related tables for measure columns
	 * @return
	 */
	public List<GenericFolderEntry> getChildren(String filter, Map<String, Set<Object>> dimRelationshipMap, Map<String, Set<Object>> measureRelationshipMap);

	public String getName();

	public String getPath();

	/**
	 * Returns the default filter settings when getting children
	 * 
	 * @return
	 */
	public String getDefaultFilter();
}
