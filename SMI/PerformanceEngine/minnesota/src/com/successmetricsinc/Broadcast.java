/**
 * $Id: Broadcast.java,v 1.11 2012-01-19 18:06:19 gsingh Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
/*
 * Support Email Broadcasting
 */
package com.successmetricsinc;

import java.io.Serializable;

import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.util.RepositoryException;

public class Broadcast implements Serializable
{
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(Broadcast.class);
	private String name;
	private String from;
	private String username;
	private String subject;
	private String body;
	private String report;
	private String[] toList;
	private String toReport;
	private String query;
	private String triggerReport;
	private String type;
	private String spaceID;
	private String csvSeparator;
	private String csvExtension;
	private String compressionFormat;
	private String promptXmlString;
	private String overrideVariablesXmlString;

	public Broadcast(Element e, Namespace ns) throws RepositoryException
	{
		name = e.getChildTextTrim("Name", ns);
		from = e.getChildTextTrim("From", ns);
		subject = e.getChildTextTrim("Subject", ns);
		body = e.getChildTextTrim("Body", ns);
		report = e.getChildTextTrim("Report", ns);
		query = e.getChildTextTrim("Query", ns);
		toReport = e.getChildTextTrim("ToReport", ns);
		triggerReport = e.getChildTextTrim("TriggerReport", ns);
		username = e.getChildTextTrim("Username", ns);
		type = e.getChildTextTrim("Type", ns);
		if (type == null)
			type = "html";
		toList = Repository.getStringArrayChildren(e, "ToList", ns);
	}		

	public Broadcast(String name, String spaceID, String username, String from, String subject, String body, String report, String[] toList, String toReport, String triggerReport, String type)
	{
		this(name, spaceID, username, from, subject, body, report, toList, toReport, triggerReport, type, null, null, null, null, null);
	}
	
	public Broadcast(String name, String spaceID, String username, String from, String subject, 
			String body, String report, String[] toList, String toReport, String triggerReport, String type,
			String csvSeparator, String csvExtension, String compressionFormat, String promptXmlString, String overrideVariablesXmlString)
	{
		this.name = name;
		this.from = from;
		this.subject = subject;
		this.body = body;
		this.report = report;
		this.toList = toList;
		this.query = null;
		this.toReport = toReport;
		this.triggerReport = triggerReport;
		this.username = username;
		this.type = type;
		this.spaceID = spaceID;
		this.csvSeparator = csvSeparator;
		this.csvExtension = csvExtension;
		this.compressionFormat = compressionFormat;
		this.promptXmlString = promptXmlString;
		this.overrideVariablesXmlString = overrideVariablesXmlString;
	}
	
	public Element getBroadcastElement(Namespace ns)
	{
	  Element e = new Element("Broadcast",ns);
	  
	  Element child = new Element("Name",ns);
	  child.setText(name);
	  e.addContent(child);
	  
	  child = new Element("From",ns);
    child.setText(from);
    e.addContent(child);
    
    child = new Element("Subject",ns);
    child.setText(subject);
    e.addContent(child);
    
    child = new Element("Body",ns);
    child.setText(body);
    e.addContent(child);
    
    child = new Element("Report",ns);
    child.setText(report);
    e.addContent(child);
    
    child = new Element("Query",ns);
    child.setText(query);
    e.addContent(child);
	  
	  return e;
	}
	
	/**
	 * return the name of the Broadcast
	 * 
	 * @return
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * return the From header of the Broadcast
	 * 
	 * @return
	 */
	public String getFrom()
	{
		return from;
	}

	/**
	 * return the Subject header of the Broadcast
	 * 
	 * @return
	 */
	public String getSubject()
	{
		return subject;
	}

	/**
	 * return the Body of the Broadcast, note that this is optional
	 * 
	 * @return
	 */
	public String getBody()
	{
		return body;
	}

	/**
	 * return the Report of the Broadcast, note that this is optional
	 * 
	 * @return
	 */
	public String getReport()
	{
		return report;
	}

	/**
	 * return the Query of the Broadcast, used for determing who gets the Email - the result set must have atleast one
	 * column, the username (Username) and if a column named Email exists it will use that for the email address
	 * 
	 * @return
	 */
	public String getQuery()
	{
		return query;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setFrom(String from)
	{
		this.from = from;
	}

	public void setSubject(String subject)
	{
		this.subject = subject;
	}

	public void setBody(String body)
	{
		this.body = body;
	}

	public void setReport(String report)
	{
		this.report = report;
	}

	public void setQuery(String query)
	{
		this.query = query;
	}

	public String[] getToList()
	{
		return toList;
	}

	public void setToList(String[] toList)
	{
		this.toList = toList;
	}

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}

	public String getToReport()
	{
		return toReport;
	}

	public void setToReport(String toReport)
	{
		this.toReport = toReport;
	}

	public String getTriggerReport()
	{
		return triggerReport;
	}

	public void setTriggerReport(String triggerReport)
	{
		this.triggerReport = triggerReport;
	}

	public String getSpaceID() {
		return spaceID;
	}

	public void setSpaceID(String spaceID) {
		this.spaceID = spaceID;
	}

	public String getCsvSeparator() {
		return csvSeparator;
	}

	public void setCsvSeparator(String csvSeparator) {
		this.csvSeparator = csvSeparator;
	}

	public String getCsvExtension() {
		return csvExtension;
	}

	public void setCsvExtension(String csvExtension) {
		this.csvExtension = csvExtension;
	}

	public String getCompressionFormat() {
		return compressionFormat;
	}

	public void setCompressionFormat(String compressionFormat) {
		this.compressionFormat = compressionFormat;
	}

	public String getPromptXmlString() {
		return promptXmlString;
	}

	public void setPromptXmlString(String promptXmlString) {
		this.promptXmlString = promptXmlString;
	}
	
	public String getOverrideVariablesXmlString() {
		return overrideVariablesXmlString;
	}

	public void setOverrideVariablesXmlString(String overrideVariablesXmlString) {
		this.overrideVariablesXmlString = overrideVariablesXmlString;
	}
}