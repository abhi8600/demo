/**
 * $Id: IRealm.java,v 1.3 2009-12-09 19:05:57 ricks Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.successmetricsinc.security;

import java.util.List;

import com.successmetricsinc.Group;
import com.successmetricsinc.User;

/**
 * Interface that defines Realm's (authentication/user classes)
 * @author ricks
 *
 */
public interface IRealm
{
	public User authenticate(String username, String credentials);
    
	public User findUser(String username);
    
    public Group findGroup(String name);
    public List<Group> getUserGroups(User u);
    public List<Group> getGroups(boolean filterInternal);
    
    public boolean hasRole(User user, String tag);
    public boolean isAdministrator(User user);
    
    public enum RealmStatus
    {Okay, FailedNoSuchUser, FailedImmutableUser, FailedInternalError, FailedAuthentication, FailedAlreadyExists, FailedBadPassword;
    public static String getMessage(RealmStatus status)
    {
    	switch (status)
    	{
    	case Okay:
    		return "Operation successful";
    	case FailedNoSuchUser:
    		return "No such user";
    	case FailedImmutableUser:
    		return "User not able to be changed";
    	case FailedInternalError:
    		return "Internal error";
    	case FailedAuthentication:
    		return "Authentication failed";
    	case FailedAlreadyExists:
    		return "User already exists";
    	case FailedBadPassword:
    		return "Password does not meet the security criteria\n";
    	default:
    		return "No message";
    	}
    }
    };
}
