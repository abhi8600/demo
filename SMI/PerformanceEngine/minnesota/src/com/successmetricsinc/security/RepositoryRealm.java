/**
 * $Id: RepositoryRealm.java,v 1.13 2012-11-28 01:24:41 BIRST\ricks Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.successmetricsinc.security;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.successmetricsinc.Group;
import com.successmetricsinc.Repository;
import com.successmetricsinc.User;

/**
 * realm for handling users in the Repository
 * @author ricks
 *
 */
public class RepositoryRealm implements IRealm, Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(RepositoryRealm.class);
	private Repository r;
	private static List<Group> demoGroupList = new ArrayList<Group>();
	private static User demoUser = new User();
	private static final String DemoUserName = "BirstDemoUser@birst.com";
	
	static
	{
		demoUser.setUserName(DemoUserName);
		Group g = new Group();
    	g.setName("USER$");
    	g.setInternalGroup(true);
    	demoGroupList.add(g);
	}
	
	public RepositoryRealm(Repository rep)
	{
		r = rep;
	}
	
    /**
     * Find a given user
     * 
     * @param username
     * @return
     */
    public User findUser(String username)
    {
    	if (username.equals(DemoUserName)) // special case for 'demo' user
    	{
    		return demoUser;
    	}
        if (r.getUsers() != null)
        {
        	for (int count = 0; count < r.getUsers().size(); count++)
        	{
        		User u = r.getUsers().get(count);
        		if (u.getUserName().equalsIgnoreCase(username))
        		{
        			return u;
        		}
        	}
        }
        
        return null;
    }

    /**
     * Return a list of the groups that a user belongs to
     * 
     * @param u
     * @return
     */
    public List<Group> getUserGroups(User u)
    {
    	List<Group> result = new ArrayList<Group>();
    	Group[] groups = r.getGroups();
    	if (u == null)
    	{
    		logger.warn("User is null in getUserGroups");
    		return result;
    	}
		String userName = u.getUserName();
    	if (userName == null)
    	{
    		logger.warn("UserName is null in getUserGroups");
    		return result;
    	}
    	if (userName.equals(DemoUserName)) // special case for 'demo' user
    	{
    		return demoGroupList;
    	}
    	if (groups != null)
    	{	
    		for (int count = 0; count < groups.length; count++)
    		{
    			Group gr = groups[count];
    			List<String> names = gr.getUserNames();
    			if (names != null)
    			{
    				int sz = names.size();
    				for (int count2 = 0; count2 < sz; count2++)
    				{
    					String name = names.get(count2);
    					if (name == null)
    					{
    						logger.warn("Found null username in group " + gr.getName());
    						continue;
    					}
    					if (name.equalsIgnoreCase(userName))
    					{
    						result.add(gr);
    						break;
    					}
    				}
    			}
    		}
    	}
        return result;
    }
    
    /**
     * Return whether a list of groups have access to this tag (Administrator group has access to everything)
     * 
     * @param u
     * @return
     */
    public boolean hasRole(User user, String tag)
    {
    	return false;
    }

    /**
     * is the user a 'super user' Administrator
     */
    public boolean isAdministrator(User user)
    {
    	return false;
    }

    /**
     * Authenticate a given username and password and return their user
     * 
     * @param username
     * @param password
     * @return
     */
    public User authenticate(String username, String password)
    {
        User u = findUser(username);
        if (u != null)
        {
            if ((u.getHashedPassword() == null) || "".equals(u.getHashedPassword()))
            {
            	// no password for this user
                return (u);
            }
            
            // hash the user entered password and compare against the hashed one in the data store
            if (EncryptionService.getInstance().equals(u.getHashedPassword(), password))
            {
                return (u);
            }
        }
        return (null);
    }

    /**
     * Find a given group
     * 
     * @param name
     * @return
     */
    public Group findGroup(String name)
    {
        if (r.getGroups() == null)
            return (null);
        for (int count = 0; count < r.getGroups().length; count++)
        {
            if (r.getGroups()[count].getName().equals(name))
                return (r.getGroups()[count]);
        }
        return (null);
    }
    
    /**
     * Return an array of group names
     * 
     * @param filterInternal filter out internal groups
     */
    public List<Group> getGroups(boolean filterInternal)
    {
    	List<Group> groups = new ArrayList<Group>();
    	
        if (r.getGroups() == null)
            return groups;
        
        for (int i = 0; i < r.getGroups().length; i++)
        {
        	// make USER$ group a special case so that everyone can see it
        	if ((filterInternal && !r.getGroups()[i].isInternalGroup()) || !filterInternal || r.getGroups()[i].getName().equals("USER$"))
        		groups.add(r.getGroups()[i]);
        }
        return groups;
    }
}
