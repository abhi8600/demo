/**
 * $Id: SMIRealm.java,v 1.16 2011-09-26 19:03:48 ricks Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.successmetricsinc.security;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.successmetricsinc.Group;
import com.successmetricsinc.Repository;
import com.successmetricsinc.User;
import com.successmetricsinc.Repository.ACLItem;
import com.successmetricsinc.util.RepositoryException;

public class SMIRealm implements IRealm, Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(SMIRealm.class);	
	private RepositoryRealm repRealm = null;
	private Repository r = null;

	private static Hashtable<String,User> userCache = null;
	private static Hashtable<String,List<Group>> groupCache = null;
	
	public SMIRealm(Repository r, boolean forceRepOnly, Properties overrides) throws RepositoryException
	{
		this.r = r;
		repRealm = new RepositoryRealm(r);
	}
	
	public User authenticate(String username, String password)
	{
		User user = repRealm.authenticate(username, password);
		if (userCache != null && user != null)
			userCache.put(username, user);
		return user;
	}
	
    public User findUser(String username)
    {
    	if (username == null)
    	{
    		logger.debug("SMIRealm::findUser with null name");
    		return null;
    	}
    	User user = null;
    	if (userCache != null)
    	{
    		user = userCache.get(username);
    	   	if (user != null)
        		return user;
    	}
     	user = repRealm.findUser(username);
		if (userCache != null && user != null)
			userCache.put(username, user);
		return user;
    }
    
    public Group findGroup(String name)
    {
    	if (name == null)
    	{
    		logger.debug("SMIRealm::findGroup with null name");
    		return null;
    	}
       	Group group = repRealm.findGroup(name);
		return group;
    }
    public List<Group> getUserGroups(User u)
    {
    	if (u == null)
    	{
    		logger.debug("SMIRealm::getUserGroups with null User");
    		return null;
    	}
    	if (u.getUserName() == null)
    	{
    		logger.debug("SMIRealm::getUserGroups with a User with a null Username");
    		return null;
    	}
    	
   		List<Group> groups = null;
    	if (groupCache != null)
    	{
    		groups = groupCache.get(u.getUserName());
    		if (groups != null)
    			return groups;
    	}
    	
    	groups = repRealm.getUserGroups(u);
    	if (groupCache != null && groups != null)
    		groupCache.put(u.getUserName(), groups);
    	return groups;
    }
    
    public List<Group> getGroups(boolean filterInternal)
    {
    	/* groups only exist in the repository */
       	List<Group> groups = repRealm.getGroups(filterInternal);
    	return groups;
    }
    
    /**
     * Return whether a list of groups have access to this tag (Administrator group has access to everything)
     * 
     * @param u
     * @return
     */
    public boolean hasRole(User user, String tag)
    {
        if (user == null)
            return false;
        
        if ("User".equals(tag))
        	return true;
        
        if (isAdministrator(user))
        	return true;

        if (tag == null || r.getAccessControlLists() == null)
        {
        	return false;
        }
        List<ACLItem> list = r.getAccessControlLists().get(tag);
        if (list == null)
        {
         	return false;
        }
        
        List<Group> groupList = getUserGroups(user);
        if (groupList == null)
        {
        	return false;
        }
        
        for (ACLItem item : list)
        {
            if (item.isAccess() && item.getTag().equals(tag))
            {
                for (Group g : groupList)
                {
                    if (g.getName().equals(item.getGroup()))
                    {
                    	return (true);
                    }
                }
            }
        }
        
        return (false);
    }
    
    /**
     * is the user a 'super user' Administrator
     */
    public boolean isAdministrator(User user)
    {
    	return isGroupMember(user, "Administrators");
    }
    
    private boolean isGroupMember(User user, String group)
    {
    	if (user == null || group == null)
    		return false;
    	List<Group> groups = getUserGroups(user);
    	for (Group g : groups)
    	{
    		if (g.getName().equals(group))
    		{
    			return true;
    		}
    	}
    	return false;
    }
}
