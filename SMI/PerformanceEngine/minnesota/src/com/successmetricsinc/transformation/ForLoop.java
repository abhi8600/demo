/**
 * 
 */
package com.successmetricsinc.transformation;

import java.util.List;

import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.warehouse.StagingTable;

/**
 * @author bpeters
 * 
 */
public class ForLoop extends Statement
{
	private TransformationScript script;
	private String counterName;
	private Operator startvalue;
	private Operator endvalue;
	private Operator step;
	private StatementBlock statementBlock;
	private boolean isdouble;

	public ForLoop(TransformationScript script, Tree t, List<StagingTable> scriptPath) throws ScriptException
	{
		this.script = script;
		counterName = t.getChild(0).getText();
		DataElement de = script.findElement(counterName);
		if (de == null)
		{
			if (t.getChild(0).getChildCount() == 0)
				throw new ScriptException("Error line " + t.getChild(0).getLine() + ", " + counterName + " not found", t.getChild(0).getLine(), t.getChild(0)
						.getCharPositionInLine(), t.getChild(0).getCharPositionInLine() + t.getChild(0).getText().length());
			VariableDataElement vde = new VariableDataElement();
			vde.name = t.getChild(0).getText();
			vde.type = new DataType(t.getChild(0).getChild(0).getText(), 0);
			script.dataElements.add(vde);
			de = vde;
		} else if (t.getChild(0).getChildCount() > 0)
		{
			throw new ScriptException("Error line " + t.getChild(0).getLine() + ", " + counterName + " already defined", t.getChild(0).getLine(), t.getChild(0)
					.getCharPositionInLine(), t.getChild(0).getCharPositionInLine() + t.getChild(0).getText().length());
		} else
		{
			if (de.getType().type != DataType.DType.Integer && de.getType().type != DataType.DType.Float)
				throw new ScriptException("Error, variable in for loop must be integer or float", t.getChild(0).getLine(), t.getChild(0)
						.getCharPositionInLine(), t.getChild(0).getCharPositionInLine() + t.getChild(0).getText().length());
		}
		Expression ex = new Expression(script);
		startvalue = ex.compile(t.getChild(1).getChild(0));
		ex = new Expression(script);
		endvalue = ex.compile(t.getChild(1).getChild(1));
		if (startvalue.getType() != Operator.Type.Integer && startvalue.getType() != Operator.Type.Float)
			throw new ScriptException("Start value of for loop is not an integer or float", t.getChild(0).getLine(), t.getChild(0).getCharPositionInLine(), t
					.getChild(0).getCharPositionInLine()
					+ t.getChild(0).getText().length());
		if (endvalue.getType() != Operator.Type.Integer && endvalue.getType() != Operator.Type.Float)
			throw new ScriptException("End value of for loop is not an integer or float", t.getChild(0).getLine(), t.getChild(0).getCharPositionInLine(), t
					.getChild(0).getCharPositionInLine()
					+ t.getChild(0).getText().length());
		if (t.getChild(1).getChildCount() == 3)
		{
			ex = new Expression(script);
			step = ex.compile(t.getChild(1).getChild(2));
			if (step.getType() != Operator.Type.Integer && step.getType() != Operator.Type.Float)
				throw new ScriptException("Step value of for loop is not an integer or float", t.getChild(0).getLine(), t.getChild(0).getCharPositionInLine(),
						t.getChild(0).getCharPositionInLine() + t.getChild(0).getText().length());
		}
		if (t.getChildCount() == 4)
		{
			String stepName = t.getChild(3).getText();
			DataElement stepde = script.findElement(stepName);
			if (stepde == null)
				throw new ScriptException("Error line " + t.getChild(3).getLine() + ", " + stepName + " not found", t.getChild(3).getLine(), t.getChild(3)
						.getCharPositionInLine(), t.getChild(3).getCharPositionInLine() + t.getChild(3).getText().length());
			if (stepde != de)
				throw new ScriptException("Error line " + t.getChild(3).getLine() + ", next statement must refer to the same variable as the for statement", t
						.getChild(3).getLine(), t.getChild(3).getCharPositionInLine(), t.getChild(3).getCharPositionInLine() + t.getChild(3).getText().length());
		}
		statementBlock = new StatementBlock(script, t.getChild(2), scriptPath);
		this.line = t.getLine();
		this.start = t.getCharPositionInLine();
		this.stop = t.getText().length() + this.start;
		isdouble = de.getType().type == DataType.DType.Float;
	}

	@Override
	public void execute() throws ScriptException
	{
		Number start = (Number) startvalue.evaluate();
		DataElement counter = script.findElement(counterName);
		counter.setValue(start);
		Number end = (Number) endvalue.evaluate();
		boolean execute;
		if (isdouble)
		{
			execute = start.doubleValue() <= end.doubleValue();
		} else
		{
			execute = start.intValue() <= end.intValue();
		}
		while (execute)
		{
			try
			{
				statementBlock.execute();
			} catch (ScriptException se)
			{
				if (se.isExitFor())
					break;
				throw se;
			}
			Number stp = null;
			if (step != null)
				stp = (Number) step.evaluate();
			else
				stp = 1;
			if (isdouble)
			{
				double d = (Double) counter.getValue(null);
				d += stp.doubleValue();
				counter.setValue(d);
				execute = d <= end.doubleValue();
			} else
			{
				long i = (Long) counter.getValue(null);
				i += stp.intValue();
				counter.setValue(i);
				execute = i <= end.intValue();
			}
		}
	}
}
