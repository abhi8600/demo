/**
 * 
 */
package com.successmetricsinc.transformation;

/**
 * @author Brad
 * 
 */
public class ParseResult
{
	public ScriptException InputError;
	public ScriptException OutputError;
	public ScriptException ScriptError;
	public String[] LoadGroups;
}
