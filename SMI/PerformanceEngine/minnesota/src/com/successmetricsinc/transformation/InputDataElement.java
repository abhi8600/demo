/**
 * 
 */
package com.successmetricsinc.transformation;

import java.sql.Timestamp;
import java.util.Date;


/**
 * @author bpeters
 * 
 */
public class InputDataElement implements DataElement
{
	public String name;
	public DataType type;
	private ColData coldata;
	private Object value;

	@Override
	public String getName()
	{
		return name;
	}

	@Override
	public boolean canGet()
	{
		return true;
	}

	@Override
	public boolean canSet()
	{
		return false;
	}

	@Override
	public DataType getType()
	{
		return type;
	}

	@Override
	public Object getValue(Operator lookup) throws ScriptException
	{
		return value;
	}

	@Override
	public void setValue(Object o)
	{
		if (Timestamp.class.isInstance(o))
			this.value =  new Date(((Timestamp)o).getTime());
		else
			this.value = o;
	}

	public ColData getColdata()
	{
		return coldata;
	}

	public void setColdata(ColData coldata)
	{
		this.coldata = coldata;
	}
}
