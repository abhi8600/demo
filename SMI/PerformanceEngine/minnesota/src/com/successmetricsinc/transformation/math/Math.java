package com.successmetricsinc.transformation.math;

import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.transformation.BirstScriptParser;
import com.successmetricsinc.transformation.Expression;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.Statement;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.transformation.operators.OpUnaryFloat;

public class Math extends Operator
{
	private Operator floatValue;
	private Operator secondValue;
	private Tree t;
	private int type;
	private TransformationScript script;

	public Math(TransformationScript script, Tree t, int type) throws ScriptException
	{
		super(script, t);
		this.t = t;
		this.script = script;
		this.type = type;
		Expression ex = new Expression(script);
		floatValue = ex.compile(t.getChild(0));
		if (floatValue.getType() == Operator.Type.Integer)
			floatValue = new OpUnaryFloat(floatValue, BirstScriptParser.FLOATTYPE);
		else if (floatValue.getType() != Operator.Type.Float)
			throw new ScriptException(t.getText() + " may only be applied to numeric data elements: " + Statement.toString(t.getChild(0), null), t.getLine(),
					t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		if (type == BirstScriptParser.MATHMAX || type == BirstScriptParser.MATHMIN || type == BirstScriptParser.POWER)
		{
			ex = new Expression(script);
			secondValue = ex.compile(t.getChild(1));
			if (secondValue.getType() == Operator.Type.Integer)
				secondValue = new OpUnaryFloat(secondValue, BirstScriptParser.FLOATTYPE);
			else if (secondValue.getType() != Operator.Type.Float)
				throw new ScriptException(t.getText() + " may only be applied to numeric data elements: " + Statement.toString(t.getChild(1), null),
						t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
	}

	@Override
	public Operator clone()
	{
		try
		{
			return new Math(script, t, type);
		} catch (ScriptException e)
		{
			return null;
		}
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		try
		{
			return ((Double) evaluate()).compareTo((Double) b.evaluate());
		} catch (Exception e)
		{
			if (e instanceof ScriptException)
				throw (ScriptException) e;
			throw new ScriptException("Bad data type: ", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		try
		{
			return ((Double) evaluate()).equals((Double) b.evaluate());
		} catch (Exception e)
		{
			throw new ScriptException("Bad data type", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		Object o = floatValue.evaluate();
		if (o == null)
			throw new ScriptException("Cannot use " + t.getText() + " on null values: " + Statement.toString(t.getChild(0), null), t.getLine(),
					t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		double d = (Double) o;
		switch (type)
		{
		case BirstScriptParser.SIN:
			return java.lang.Math.sin(d);
		case BirstScriptParser.COS:
			return java.lang.Math.cos(d);
		case BirstScriptParser.TAN:
			return java.lang.Math.tan(d);
		case BirstScriptParser.ARCSIN:
			return java.lang.Math.asin(d);
		case BirstScriptParser.ARCCOS:
			return java.lang.Math.acos(d);
		case BirstScriptParser.ARCTAN:
			return java.lang.Math.atan(d);
		case BirstScriptParser.ABS:
			return java.lang.Math.abs(d);
		case BirstScriptParser.CEILING:
			return java.lang.Math.ceil(d);
		case BirstScriptParser.FLOOR:
			return java.lang.Math.floor(d);
		case BirstScriptParser.SQRT:
			return java.lang.Math.sqrt(d);
		case BirstScriptParser.LN:
			return java.lang.Math.log(d);
		case BirstScriptParser.LOG:
			return java.lang.Math.log10(d);
		case BirstScriptParser.EXP:
			return java.lang.Math.exp(d);
		case BirstScriptParser.POWER:
			Object o2 = secondValue.evaluate();
			if (o2 == null)
				throw new ScriptException("Cannot use " + t.getText() + " on null values: " + Statement.toString(t.getChild(1), null), t.getLine(),
						t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
			double d2 = (Double) o2;
			return java.lang.Math.pow(d, d2);
		case BirstScriptParser.MATHMAX:
			o2 = secondValue.evaluate();
			if (o2 == null)
				throw new ScriptException("Cannot use " + t.getText() + " on null values: " + Statement.toString(t.getChild(1), null), t.getLine(),
						t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
			d2 = (Double) o2;
			return java.lang.Math.max(d, d2);
		case BirstScriptParser.MATHMIN:
			o2 = secondValue.evaluate();
			if (o2 == null)
				throw new ScriptException("Cannot use" + t.getText() + " on null values: " + Statement.toString(t.getChild(1), null), t.getLine(),
						t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
			d2 = (Double) o2;
			return java.lang.Math.min(d, d2);
		}
		throw new ScriptException("Invalid math operator (" + type + "): " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(),
				t.getCharPositionInLine() + t.getText().length());
	}

	@Override
	public int getSize()
	{
		return Double.SIZE / 8;
	}

	@Override
	public Type getType()
	{
		return Operator.Type.Float;
	}
}
