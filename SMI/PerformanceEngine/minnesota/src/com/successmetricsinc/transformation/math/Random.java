package com.successmetricsinc.transformation.math;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import org.antlr.runtime.tree.Tree;
import org.apache.log4j.Logger;

import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.Statement;
import com.successmetricsinc.transformation.TransformationScript;

public class Random extends Operator
{
	private static Logger logger = Logger.getLogger(Random.class);
	private Tree t;
	private SecureRandom rand;
	private TransformationScript script;

	public Random(TransformationScript script, Tree t) throws ScriptException
	{
		super(script, t);
		this.t = t;
		if (t.getChildCount() != 0)
		{
			throw new ScriptException("Random does not have any arguments: " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(),
					t.getCharPositionInLine() + t.getText().length());
		}
		try
		{
			rand = SecureRandom.getInstance("SHA1PRNG");
			byte bytes[] = new byte[20];
			rand.nextBytes(bytes); // force the seeding
		} catch (NoSuchAlgorithmException e)
		{
			logger.warn("Could not find the SHA1PNG random algorithm, falling back on Math.random");
		}
	}

	@Override
	public Operator clone()
	{
		try
		{
			return new Random(script, t);
		} catch (ScriptException e)
		{
			return null;
		}
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		try
		{
			return ((Double) evaluate()).compareTo((Double) b.evaluate());
		} catch (Exception e)
		{
			if (e instanceof ScriptException)
				throw (ScriptException) e;
			throw new ScriptException("Bad data type: ", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		try
		{
			return ((Double) evaluate()).equals((Double) b.evaluate());
		} catch (Exception e)
		{
			throw new ScriptException("Bad data type", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		if (rand != null)
			return rand.nextDouble();
		else
			return java.lang.Math.random();
	}

	@Override
	public int getSize()
	{
		return Double.SIZE / 8;
	}

	@Override
	public Type getType()
	{
		return Operator.Type.Float;
	}
}
