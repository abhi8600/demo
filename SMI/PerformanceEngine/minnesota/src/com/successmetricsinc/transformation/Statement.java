package com.successmetricsinc.transformation;

import java.util.List;
import java.util.Set;

import org.antlr.runtime.Token;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.Tree;
import org.apache.log4j.Logger;

import com.successmetricsinc.Variable;
import com.successmetricsinc.util.Util;

public class Statement
{
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(Statement.class);
	protected TransformationScript script;
	protected int line;
	protected int start;
	protected int stop;

	public void execute() throws ScriptException
	{
	}

	public int getLine()
	{
		return line;
	}

	public int getStart()
	{
		return start;
	}

	public int getStop()
	{
		return stop;
	}

	public static String toStringInfixFunction(Tree tree, boolean spaces, FormatContext format) throws ScriptException
	{
		if (tree.getType() == BirstScriptParser.NEGATE)
			return '-' + toString(tree.getChild(0), format);
		StringBuilder sb = new StringBuilder();
		if (tree.getChildCount() > 0 && tree.getChild(0) != null)
			sb.append(toString(tree.getChild(0), format));
		if (spaces)
			sb.append(' ');
		sb.append(tree.getText().toUpperCase());
		if (spaces)
			sb.append(' ');
		if (tree.getChildCount() > 1 && tree.getChild(1) != null)
			sb.append(toString(tree.getChild(1), format));
		return sb.toString();
	}

	public static String toStringScalarFunction(Tree tree, FormatContext format) throws ScriptException
	{
		StringBuilder sb = new StringBuilder(tree.getText().toUpperCase());
		sb.append('(');
		boolean first = true;
		for (int i = 0; i < tree.getChildCount(); i++)
		{
			if (!first)
				sb.append(',');
			first = false;
			sb.append(toString(tree.getChild(i), format));
		}
		sb.append(')');
		return sb.toString();
	}

	private static int getFirstIndex(CommonTree tree)
	{
		if (tree.getChildCount() > 0)
			return (getFirstIndex((CommonTree) tree.getChild(0)));
		return tree.getToken().getTokenIndex();
	}

	private static String checkComment(FormatContext format, Tree tree)
	{
		/*
		 * Allow for preservation of comments which are filtered to a hidden channel (not in normal token stream)
		 */
		if (format != null && tree instanceof CommonTree)
		{
			int tindex = getFirstIndex((CommonTree) tree);
			if (format.lastToken >= 0)
			{
				@SuppressWarnings("rawtypes")
				List tlist = format.cts.getTokens(format.lastToken, tindex - 1);
				if (tindex > format.lastToken)
					format.lastToken = tindex;
				if (tlist != null && tlist.size() > 0)
				{
					StringBuilder sb = new StringBuilder();
					int count = 0;
					for (Object o : tlist)
					{
						if (!(o instanceof Token))
							continue;
						Token st = ((Token) o);
						if (st.getChannel() == Token.HIDDEN_CHANNEL)
						{
							String text = st.getText();
							if (text.startsWith("//") && (text.endsWith("\n") || text.endsWith("\r")))
							{
								sb.append(" " + text.trim());
								count++;
							} else if (text.startsWith("/*") && text.endsWith("*/"))
							{
								sb.append("\r" + text.trim());
								count++;
							}
						}
					}
					if (count == 0)
						return null;
					return sb.toString();
				}
			}
		}
		return null;
	}

	private static String startLine(FormatContext format, int indent, Tree tree, String precedingComment)
	{
		if (format != null)
		{
			StringBuilder sb = new StringBuilder();
			// Include comments
			if (tree != null && indent == 0)
			{
				if (precedingComment == null)
				{
					// Break things down by blocks
					if (indent == 0 && format.lastType >= 0)
					{
						if (format.lastType != tree.getType())
						{
							switch (tree.getType())
							{
							// Situations with extra lines to demark blocks
							case BirstScriptParser.DIM:
							case BirstScriptParser.IF:
							case BirstScriptParser.FOR:
							case BirstScriptParser.WHILE:
							case BirstScriptParser.ASSIGNMENT:
								sb.append("\r");
							}
						}
						switch (tree.getType())
						{
						// Situations with extra lines to demark blocks
						case BirstScriptParser.WRITERECORD:
						case BirstScriptParser.FUNCTIONDEF:
							sb.append("\r");
						}
					}
				}
				format.lastType = tree.getType();
			}
			// Start new line with proper indent
			sb.append("\r");
			for (int i = 0; i < indent; i++)
				sb.append("   ");
			return sb.toString();
		}
		return "";
	}

	public static String toString(Tree tree, FormatContext format) throws ScriptException
	{
		return toString(tree, format, 0);
	}

	public static String toString(Tree tree, FormatContext format, int indent) throws ScriptException
	{
		if (tree == null)
			return null;
		StringBuilder sb = new StringBuilder();
		String comment = checkComment(format, tree);
		if (comment != null)
			sb.append(comment);
		int type = tree.getType();
		switch (type)
		{
		case BirstScriptParser.ASSIGNMENT:
			sb.append(startLine(format, indent, tree, comment) + toString(tree.getChild(0), format) + " = " + toString(tree.getChild(1), format));
			break;
		case BirstScriptParser.PAREN:
			sb.append('(' + toString(tree.getChild(0), format) + ')');
			break;
		case BirstScriptParser.INTEGER:
		case BirstScriptParser.FLOAT:
		case BirstScriptParser.BOOLEAN:
			sb.append(tree.getText());
			break;
		case BirstScriptParser.STRING:
			String s = tree.getText();
			s = s.substring(1, s.length() - 1);
			s = Util.replaceStr(s, "\\", "\\\\");
			s = Util.replaceStr(s, "\r", "\\r");
			s = Util.replaceStr(s, "\n", "\\n");
			s = Util.replaceStr(s, "\t", "\\t");
			s = Util.replaceStr(s, "\'", "\\\'");
			sb.append('\'').append(s).append('\'');
			break;
		case BirstScriptParser.DATETIME:
			if (tree.getText().toLowerCase().equals("nowdate"))
				sb.append("NOWDATE");
			else
				sb.append(tree.getText());
			break;
		case BirstScriptParser.NULL:
			sb.append("NULL");
			break;
		case BirstScriptParser.EQUALS:
		case BirstScriptParser.LT:
		case BirstScriptParser.GT:
		case BirstScriptParser.LTEQ:
		case BirstScriptParser.GTEQ:
		case BirstScriptParser.NOTEQUALS:
		case BirstScriptParser.PLUS:
		case BirstScriptParser.MINUS:
		case BirstScriptParser.DIV:
		case BirstScriptParser.MULT:
		case BirstScriptParser.MOD:
		case BirstScriptParser.BITAND:
		case BirstScriptParser.BITOR:
		case BirstScriptParser.NEGATE:
			sb.append(Statement.toStringInfixFunction(tree, true, format));
			break;
		case BirstScriptParser.AND:
		case BirstScriptParser.OR:
			sb.append(Statement.toStringInfixFunction(tree, true, format));
			break;
		case BirstScriptParser.ISNULL:
		case BirstScriptParser.ISNOTNULL:
			sb.append(toString(tree.getChild(0), format) + tree.getText());
			break;
		case BirstScriptParser.SAVEDEXPRESSION:
			sb.append("SavedExpression('" + toString(tree.getChild(0), format) + "')");
			break;
		case BirstScriptParser.COLUMN_NAME:
		case BirstScriptParser.IDENT_NAME:
			sb.append(tree.getText());
			if (tree.getChildCount() > 0 && tree.getChild(0) != null)
				sb.append(" ").append(toString(tree.getChild(0), format));
			break;
		case BirstScriptParser.GRAIN_COLUMN:
		case BirstScriptParser.LEVEL_COLUMN:
			sb.append(tree.getText());
			break;
		case BirstScriptParser.LOGICALMEMBERSET:
			for(int i=0;i<tree.getChildCount();i++)
			{
				sb.append(toString(tree.getChild(i), format));
			}
			sb.append("]");
			break;
		case BirstScriptParser.MEMBEREXPRESSIONS:
			{
				boolean first = true;
				for(int i=0;i<tree.getChildCount();i++)
				{
					if (first)
						first = false;
					else
						sb.append(",");
					sb.append(toString(tree.getChild(i), format));
				}
				break;
			}
		case BirstScriptParser.SELECTED:
			sb.append("SELECTED(").append(tree.getChild(0)).append(")");
			break;
		case BirstScriptParser.CHILDREN:
			sb.append("CHILDREN(").append(tree.getChild(0)).append(")");
			break;
		case BirstScriptParser.ALLCHILDREN:
			sb.append("ALLCHILDREN(").append(tree.getChild(0)).append(")");
			break;
		case BirstScriptParser.LEAVES:
			sb.append("LEAVES(").append(tree.getChild(0)).append(")");
			break;
		case BirstScriptParser.PARENTS:
			sb.append("PARENTS(").append(tree.getChild(0)).append(")");
			break;
		case BirstScriptParser.AVG:
		case BirstScriptParser.SUM:
		case BirstScriptParser.COUNT:
		case BirstScriptParser.COUNTDISTINCT:
		case BirstScriptParser.MIN:
		case BirstScriptParser.MAX:
		case BirstScriptParser.PTILE:
		case BirstScriptParser.RANK:
		case BirstScriptParser.DRANK:
		case BirstScriptParser.STATMEDIAN:
		case BirstScriptParser.RSUM:
		case BirstScriptParser.SUBSTRING:
		case BirstScriptParser.LENGTH:
		case BirstScriptParser.TOLOWER:
		case BirstScriptParser.TOUPPER:
		case BirstScriptParser.TOSTRING:
		case BirstScriptParser.IIF:
		case BirstScriptParser.IFNULL:
		case BirstScriptParser.ROWNUMBER:
		case BirstScriptParser.NUMROWS:
		case BirstScriptParser.SIN:
		case BirstScriptParser.COS:
		case BirstScriptParser.TAN:
		case BirstScriptParser.ARCSIN:
		case BirstScriptParser.ARCCOS:
		case BirstScriptParser.ARCTAN:
		case BirstScriptParser.ABS:
		case BirstScriptParser.CEILING:
		case BirstScriptParser.FLOOR:
		case BirstScriptParser.SQRT:
		case BirstScriptParser.LOG:
		case BirstScriptParser.LN:
		case BirstScriptParser.EXP:
		case BirstScriptParser.POWER:
		case BirstScriptParser.RANDOM:
		case BirstScriptParser.TRIM:
		case BirstScriptParser.GETVARIABLE:
		case BirstScriptParser.GETPROMPTVALUE:
		case BirstScriptParser.NEXTCHILD:
		case BirstScriptParser.GETLEVELVALUE:
		case BirstScriptParser.GETLEVELATTRIBUTE:
		case BirstScriptParser.GETMONTHID:
		case BirstScriptParser.ISNAN:
		case BirstScriptParser.ISINF:
			sb.append(Statement.toStringScalarFunction(tree, format));
			break;
		case BirstScriptParser.GETPROMPTFILTER:
		case BirstScriptParser.SPARSE:
		case BirstScriptParser.SETLOOKUP:
		case BirstScriptParser.SETLOOKUPROW:
		case BirstScriptParser.SETFIND:
		case BirstScriptParser.SETSTAT:
			if (format != null)
				throw new ScriptException("Unable to format script", -1, -1, -1);
			sb.append(Statement.toStringScalarFunction(tree, format));
			break;
		case BirstScriptParser.CAST:
			sb.append(tree.getChild(0).getText().toUpperCase() + '(' + toString(tree.getChild(1), format) + ')');
			break;
		case BirstScriptParser.PARAMETER:
			sb.append(tree.getText());
			break;
		case BirstScriptParser.TOTIME:
			sb.append("TOTIME(").append(toString(tree.getChild(0), format)).append(",").append(tree.getChild(1).getText()).append(",").append(tree.getChild(2).getText()).append(")");
			break;
		case BirstScriptParser.DATEPART:
			sb.append("DATEPART(" + tree.getChild(0).getText() + ',' + toString(tree.getChild(1), format) + ')');
			break;
		case BirstScriptParser.POSITION:
			if (tree.getChildCount() == 1 && tree.getChild(0).getType() == BirstScriptParser.POSITION_COLUMN_START)
				sb.append(toString(tree.getChild(0), format) + '}');
			else if (tree.getChildCount() == 3
					&& (tree.getChild(1).getText().equals("=") || tree.getChild(1).getText().equals("+=") || tree.getChild(1).getText().equals("-=")))
				sb.append(toString(tree.getChild(0), format) + toString(tree.getChild(1), format) + toString(tree.getChild(2), format) + '}');
			else
				sb.append('{' + toString(tree.getChild(0), format) + '}');
			break;
		case BirstScriptParser.DATEADD:
		case BirstScriptParser.DATEDIFF:
			sb.append(tree.getText().toUpperCase() + '(' + tree.getChild(0).getText() + ',' + toString(tree.getChild(1), format) + ','
					+ toString(tree.getChild(2), format) + ')');
			break;
		case BirstScriptParser.SELECT:
			if (format != null)
				throw new ScriptException("Unable to format script", -1, -1, -1);
			sb.append("SELECT");
			for (int i = 0; i < tree.getChildCount(); i++)
			{
				sb.append(' ');
				sb.append(toString(tree.getChild(i), format));
			}
			break;
		case BirstScriptParser.FUNCTION:
		case BirstScriptParser.FUNCTIONLOOKUP:
		case BirstScriptParser.TRANSFORM:
			sb.append(startLine(format, indent, tree, comment) + tree.getText().toUpperCase() + "(" + toString(tree.getChild(0), format, indent));
			for (int i = 1; i < tree.getChildCount(); i++)
			{
				sb.append(',' + toString(tree.getChild(i), format));
			}
			sb.append(')');
			break;
		case BirstScriptParser.PROJECTIONLIST:
			for (int i = 0; i < tree.getChildCount(); i++)
			{
				if (sb.length() > 0)
					sb.append(',');
				sb.append(toString(tree.getChild(i), format));
			}
			break;
		case BirstScriptParser.FROM:
			sb.append("FROM");
			for (int i = 0; i < tree.getChildCount(); i++)
			{
				sb.append(' ');
				sb.append(toString(tree.getChild(i), format));
			}
			break;
		case BirstScriptParser.WHERE:
			sb.append("WHERE");
			for (int i = 0; i < tree.getChildCount(); i++)
			{
				sb.append(' ');
				sb.append(toString(tree.getChild(i), format));
			}
			break;
		case BirstScriptParser.LIKE:
		{
			boolean first = true;
			for (int i = 0; i < tree.getChildCount(); i++)
			{
				sb.append(' ');
				sb.append(toString(tree.getChild(i), format));
				if (first)
				{
					sb.append(" LIKE");
					first = false;
				}
			}
			break;
		}
		case BirstScriptParser.NOTLIKE:
		{
			boolean first = true;
			for (int i = 0; i < tree.getChildCount(); i++)
			{
				sb.append(' ');
				sb.append(toString(tree.getChild(i), format));
				if (first)
				{
					sb.append(" NOT LIKE");
					first = false;
				}
			}
			break;
		}
		case BirstScriptParser.TOP:
			sb.append(tree.getText() + toString(tree.getChild(0), format));
			break;
		case BirstScriptParser.ALIAS:
			sb.append(toString(tree.getChild(0), format));
			break;
		case BirstScriptParser.USINGOUTERJOIN:
			sb.append("USING OUTER JOIN");
			break;
		case BirstScriptParser.PARALLEL:
			sb.append("PARALLEL");
			break;
		case BirstScriptParser.IN:
			boolean first = true;
			for (int i = 0; i < tree.getChildCount(); i++)
			{
				sb.append(toString(tree.getChild(i), format));
				if (first)
				{
					sb.append(" IN (");
					first = false;
				}
			}
			sb.append(")");
			break;
		case BirstScriptParser.NOTIN:
			first = true;
			for (int i = 0; i < tree.getChildCount(); i++)
			{
				sb.append(toString(tree.getChild(i), format));
				if (first)
				{
					sb.append(" NOT IN (");
					first = false;
				}
			}
			sb.append(")");
			break;
		case BirstScriptParser.VALUELIST:
			first = true;
			for (int i = 0; i < tree.getChildCount(); i++)
			{
				if (first)
					first = false;
				else
					sb.append(",");
				sb.append(toString(tree.getChild(i), format));
			}
			break;
		case BirstScriptParser.ORDERBY:
			sb.append("ORDER BY");
			for (int i = 0; i < tree.getChildCount(); i++)
			{
				sb.append(' ');
				sb.append(toString(tree.getChild(i), format));
			}
			break;
		case BirstScriptParser.DISPLAYBY:
			sb.append("DISPLAY BY");
			for (int i = 0; i < tree.getChildCount(); i++)
			{
				sb.append(' ');
				sb.append(toString(tree.getChild(i), format));
			}
			break;
		case BirstScriptParser.SCRIPT:
			for (int i = 0; i < tree.getChildCount(); i++)
				sb.append(toString(tree.getChild(i), format, indent + 1));
			break;
		case BirstScriptParser.EXITWHILE:
		case BirstScriptParser.EXITFOR:
		case BirstScriptParser.WRITERECORD:
			sb.append(startLine(format, indent, tree, comment) + tree.getText().toUpperCase());
			break;
		case BirstScriptParser.PRINT:
			sb.append(startLine(format, indent, tree, comment) + tree.getText().toUpperCase() + " " + toString(tree.getChild(0), format));
			break;
		case BirstScriptParser.HALT:
			sb.append(startLine(format, indent, tree, comment) + tree.getText().toUpperCase() + "(" + toString(tree.getChild(0), format) + ")");
			break;
		case BirstScriptParser.COMPLETE:
			sb.append(startLine(format, indent, tree, comment) + "COMPLETE");
			sb.append(toString(tree.getChild(0), format));
			sb.append(startLine(format, indent, tree, comment) + "END COMPLETE");
			break;
		case BirstScriptParser.DIM:
			sb.append(startLine(format, indent, tree, comment) + "DIM " + toString(tree.getChild(0), format) + " AS " + toString(tree.getChild(1), format));
			if (tree.getChildCount() > 2)
			{
				sb.append(" = " + toString(tree.getChild(2), format));
			}
			break;
		case BirstScriptParser.LET:
			sb.append(startLine(format, indent, tree, comment) + "LET(");
			for(int i=0;i<tree.getChild(0).getChildCount();i++)
			{
				sb.append(toString(tree.getChild(0).getChild(i), format));
				if(i<tree.getChild(0).getChildCount()-1)
					sb.append(" ");
			}
			sb.append("," + toString(tree.getChild(1), format) + ")");
			break;
		case BirstScriptParser.IF:
			for (int i = 0; i < tree.getChildCount(); i++)
			{
				Tree conditionalTree = tree.getChild(i);
				if (conditionalTree.getType() == BirstScriptParser.IFTHEN)
					sb.append(startLine(format, indent, i == 0 ? tree : null, comment) + (i == 0 ? "IF " : "ELSEIF ")
							+ toString(conditionalTree.getChild(0), format, indent) + " THEN" + toString(conditionalTree.getChild(1), format, indent));
				else
					sb.append(startLine(format, indent, null, null) + "ELSE " + toString(conditionalTree.getChild(0), format, indent));
			}
			sb.append(startLine(format, indent, null, null) + "END IF");
			break;
		case BirstScriptParser.FOR:
			sb.append(startLine(format, indent, tree, comment) + "FOR " + toString(tree.getChild(0), format, indent) + " = "
					+ toString(tree.getChild(1).getChild(0), format, indent) + " TO " + toString(tree.getChild(1).getChild(1), format, indent));
			if (tree.getChild(1).getChildCount() == 3)
			{
				sb.append(" STEP " + toString(tree.getChild(1).getChild(2), format, indent));
			}
			sb.append(toString(tree.getChild(2), format, indent));
			sb.append(startLine(format, indent, null, null) + "NEXT" + (tree.getChildCount() == 4 ? " " + toString(tree.getChild(3), format, indent) : ""));
			break;
		case BirstScriptParser.WHILE:
			sb.append(startLine(format, indent, tree, comment) + "WHILE " + toString(tree.getChild(0), format) + toString(tree.getChild(1), format, indent));
			sb.append(startLine(format, indent, tree, comment) + "END WHILE");
			break;
		case BirstScriptParser.INTEGERTYPE:
		case BirstScriptParser.FLOATTYPE:
		case BirstScriptParser.NUMBERTYPE:
		case BirstScriptParser.DATETYPE:
		case BirstScriptParser.DATETIMETYPE:
			sb.append(tree.getText().toUpperCase());
			break;
		case BirstScriptParser.VARCHARTYPE:
			sb.append(tree.getText().toUpperCase() + "(" + toString(tree.getChild(0), format, indent) + ")");
			break;
		case BirstScriptParser.RETURN:
			sb.append(startLine(format, indent, tree, comment) + tree.getText().toUpperCase() + " " + toString(tree.getChild(0), format, indent));
			break;
		case BirstScriptParser.FUNCTIONCALL:
			if (tree.getParent().getType() == BirstScriptParser.SCRIPT)
				sb.append(startLine(format, indent, tree, comment));
			sb.append(tree.getChild(0).getText() + "(");
			for (int i = 1; i < tree.getChildCount(); i++)
			{
				if (i > 1)
					sb.append(',');
				sb.append(toString(tree.getChild(i), format, indent));
			}
			sb.append(')');
			break;
		case BirstScriptParser.FUNCTIONDEF:
			sb.append(startLine(format, indent, tree, comment) + "FUNCTION " + tree.getChild(0).getText() + "(");
			int count = 0;
			for (int i = 1; i < tree.getChildCount(); i++)
			{
				if (tree.getChild(i).getType() != BirstScriptParser.IDENT_NAME)
					break;
				if (i > 1)
					sb.append(',');
				sb.append(toString(tree.getChild(i), format, indent));
				count++;
			}
			sb.append(')');
			if (tree.getChildCount() == count + 3)
			{
				sb.append(" AS " + toString(tree.getChild(count + 2), format, indent));
			}
			sb.append(toString(tree.getChild(count + 1), format, indent));
			sb.append(startLine(format, indent, null, null) + "END FUNCTION");
			break;
		case BirstScriptParser.MDXEXPRESSION:
			String expression = tree.getChild(0).getText();
			if (expression.startsWith("'") && expression.endsWith("'"))
				expression = expression.substring(1, expression.length() - 1);
			sb.append("MDXExpression('").append(tree.getChild(0).getText()).append("'");
			if (tree.getChildCount() > 1)
			{
				sb.append(" ").append(toString(tree.getChild(1), format, indent));
			}
			break;
		case BirstScriptParser.RWRITE:
			sb.append(startLine(format, indent, tree, comment) + tree.getText().toUpperCase() + "(" + toString(tree.getChild(0), format) + ","
					+ toString(tree.getChild(1), format) + ")");
			break;
		default:
			/*
			 * Throw an exception to ensure that if there's a token we don't explicitly handle, we won't damage the
			 * script
			 */
			if (format != null)
				throw new ScriptException("Unable to format script", -1, -1, -1);
			sb.append(tree.getText());
			break;
		}
		return sb.toString();
	}

	public static void extractPrompts(Tree tree, Set<Variable> vlist) throws ScriptException
	{
		if (tree == null)
			return;
		int count = tree.getChildCount();
		if (count > 0)
		{
			int type = tree.getType();
			if (type == BirstScriptParser.GETPROMPTVALUE)
			{
				Tree child = tree.getChild(0);
				Variable var = new Variable();
				String name = toString(child, null);
				var.setName("P[" + name + "]");
				vlist.add(var);
			} else
			{
				for (int i = 0; i < count; i++)
					extractPrompts(tree.getChild(i), vlist);
			}
		}
	}
}
