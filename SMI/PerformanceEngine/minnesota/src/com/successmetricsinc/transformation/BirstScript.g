grammar BirstScript;

options 
{
	backtrack=true;
	output=AST;
	ASTLabelType=CommonTree;
}

tokens
{
	PROJECTIONLIST;
	VALUELIST;
	COLUMN;
	OUTPUTCOLUMNLIST;
	OUTPUTCOLUMN;
	NEGATE;
	PAREN;
	ASSIGNMENT;
	SCRIPT;
	JOIN;
	IFTHEN;
	TREND;
	MDXEXPRESSION;
	METHOD;
	CAST;
	POSITION;
	SINGLEQUERY;
	ALIAS;
	MATHMAX;
	MATHMIN;
	SELECTED;
	CHILDREN;
	ALLCHILDREN;
	LEAVES;
	PARENTS;
	PROJECTIONLISTS;
	OLAPPROJECTIONLIST;
	LOGICALMEMBERSET;
	MEMBEREXPRESSIONS;
	FUNCTIONDEF;
	FUNCTIONCALL;
}

@lexer::header { package com.successmetricsinc.transformation; }
@header { package com.successmetricsinc.transformation; }

@rulecatch 
{
	catch (RecognitionException e) 
	{
		throw e;
	}
} 

@lexer::members
{
	List<RecognitionException> exceptions = new ArrayList<RecognitionException>();

	@Override
	public void reportError(RecognitionException e)
	{
	    exceptions.add(e);
	}

	public boolean hasError()
	{
		return exceptions.size() > 0;
	}

	public RecognitionException errorMessage()
	{
		return exceptions.get(0);
	}
	
	/*Hack to control max iterations in predict method for a dfa*/
	class DFA extends org.antlr.runtime.DFA
	{
		@Override
        	public int predict(IntStream input)
        		throws RecognitionException
        	{
        		if ( debug ) {
        			System.err.println("Enter DFA.predict for decision "+decisionNumber);
        		}
        		int mark = input.mark(); // remember where decision started in input
        		int s = 0; // we always start at s0
        		int lastIndex = 0;
        		int currentIndex = 0;
        		int iterationCounter = 0;
        		final int MAX_ITERATIONS_ALLOWED = 1000;
        		try {
        			while ( true ) {
        				if ( debug ) System.err.println("DFA "+decisionNumber+" state "+s+" LA(1)="+(char)input.LA(1)+"("+input.LA(1)+
        												"), index="+input.index());
        				int specialState = special[s];
        				lastIndex = input.index();
        				if ( specialState>=0 ) {
        					if ( debug ) {
        						System.err.println("DFA "+decisionNumber+
        							" state "+s+" is special state "+specialState);
        					}
        					s = specialStateTransition(specialState,input);
        					if ( debug ) {
        						System.err.println("DFA "+decisionNumber+
        							" returns from special state "+specialState+" to "+s);
        					}
        					if ( s==-1 ) {
        						noViableAlt(s,input);
        						return 0;
        					}
        					input.consume();
        					continue;
        				}
        				if ( accept[s] >= 1 ) {
        					if ( debug ) System.err.println("accept; predict "+accept[s]+" from state "+s);
        					return accept[s];
        				}
        				// look for a normal char transition
        				char c = (char)input.LA(1); // -1 == \uFFFF, all tokens fit in 65000 space
        				if (c>=min[s] && c<=max[s]) {
        					int snext = transition[s][c-min[s]]; // move to next state
        					if ( snext < 0 ) {
        						// was in range but not a normal transition
        						// must check EOT, which is like the else clause.
        						// eot[s]>=0 indicates that an EOT edge goes to another
        						// state.
        						if ( eot[s]>=0 ) {  // EOT Transition to accept state?
        							if ( debug ) System.err.println("EOT transition");
        							s = eot[s];
        							input.consume();
        							// TODO: I had this as return accept[eot[s]]
        							// which assumed here that the EOT edge always
        							// went to an accept...faster to do this, but
        							// what about predicated edges coming from EOT
        							// target?
        							continue;
        						}
        						noViableAlt(s,input);
        						return 0;
        					}
        					s = snext;
        					input.consume();
        					/* Allow 1000 iterations on the same character then exit */
        					currentIndex = input.index();
        					if(currentIndex == lastIndex)
        					{
        						iterationCounter++;
        						if(iterationCounter > MAX_ITERATIONS_ALLOWED)
        						{
        							noViableAlt(s,input);
        							return 0;
        						}
        					}
        					else
        					{
        						iterationCounter = 0;
        					}
        					continue;
        					
        				}
        				if ( eot[s]>=0 ) {  // EOT Transition?
        					if ( debug ) System.err.println("EOT transition");
        					s = eot[s];
        					input.consume();
        					continue;
        				}
        				if ( c==(char)Token.EOF && eof[s]>=0 ) {  // EOF Transition to accept state?
        					if ( debug ) System.err.println("accept via EOF; predict "+accept[eof[s]]+" from "+eof[s]);
        					return accept[eof[s]];
        				}
        				// not in range and not EOF/EOT, must be invalid symbol
        				if ( debug ) {
        					System.err.println("min["+s+"]="+min[s]);
        					System.err.println("max["+s+"]="+max[s]);
        					System.err.println("eot["+s+"]="+eot[s]);
        					System.err.println("eof["+s+"]="+eof[s]);
        					for (int p=0; p<transition[s].length; p++) {
        						System.err.print(transition[s][p]+" ");
        					}
        					System.err.println();
        				}
        				noViableAlt(s,input);
        				return 0;
        			}
        		}
        		finally {
        			input.rewind(mark);
        		}
        	}
    	
	    }
		
}

// LEXER

INPUT	:	('I'|'i') ('N'|'n') ('P'|'p') ('U'|'u') ('T'|'t') ':';
OUTPUT	:	('O'|'o') ('U'|'u') ('T'|'t') ('P'|'p')('U'|'u')('T'|'t') ':' ;
COLUMNS	:	('C'|'c') ('O'|'o') ('L'|'l') ('U'|'u') ('M'|'m') ('N'|'n') ('S'|'s');
BEGIN	:	('B'|'b') ('E'|'e') ('G'|'g') ('I'|'i') ('N'|'n') ;
END	:	('E'|'e') ('N'|'n') ('D'|'d');
SELECT	:	('S'|'s') ('E'|'e') ('L'|'l') ('E'|'e') ('C'|'c') ('T'|'t');

WRITERECORD
	:	('W'|'w') ('R'|'r') ('I'|'i') ('T'|'t') ('E'|'e') ('R'|'r') ('E'|'e') ('C'|'c') ('O'|'o') ('R'|'r') ('D'|'d');
	
UPDATESFDC	:	('U'|'u') ('P'|'p') ('D'|'d') ('A'|'a') ('T'|'t') ('E'|'e') ('S'|'s') ('F'|'f') ('D'|'d') ('C'|'c');
	
DIM	:	('D'|'d') ('I'|'i') ('M'|'m');
AS	:	('A'|'a') ('S'|'s');

COMMA	:	',';
LEFT_PAREN: 	'(';
RIGHT_PAREN: 	')';
QUALIFIER
	:	'.';

FROM	:	('F'|'f') ('R'|'r') ('O'|'o') ('M'|'m');

SUM	:	('S'|'s') ('U'|'u') ('M'|'m');
AVG	:	('A'|'a') ('V'|'v') ('G'|'g');
COUNT	:	('C'|'c') ('O'|'o') ('U'|'u') ('N'|'n') ('T'|'t');
COUNTDISTINCT	
	:	('C'|'c') ('O'|'o') ('U'|'u') ('N'|'n') ('T'|'t') ('D'|'d') ('I'|'i') ('S'|'s') ('T'|'t') ('I'|'i') ('N'|'n') ('C'|'c') ('T'|'t');
MIN	:	('M'|'m') ('I'|'i') ('N'|'n');
MAX	:	('M'|'m') ('A'|'a') ('X'|'x');
DISTINCT	:	('D'|'d') ('I'|'i') ('S'|'s') ('T'|'t') ('I'|'i') ('N'|'n') ('C'|'c') ('T'|'t');


COLUMN_NAME
	:	'[' IDENT '.' IDENT ']';
	
GRAIN_COLUMN
	:	'[' GRAIN '(' IDENT ')' '.' IDENT ']' |
		'[' MEASURETABLE '(' IDENT ')' '.' IDENT ']';
	
LEVEL_COLUMN
	:	'[' LEVEL '(' IDENT '.' IDENT ')' '.' IDENT ']';

OLAPCUBE	
	:	'[' CUBE ']';
		
IDENT_NAME
	:	'[' IDENT ']';
	
GRAIN_TABLE
	:	'[' GRAIN '(' IDENT ')' ']' |
		'[' MEASURETABLE '(' IDENT ')' ']';
	
LEVEL_TABLE
	:	'[' LEVEL '(' IDENT '.' IDENT ')' ']';
	
POSITION_COLUMN_START
	:	'{' '[' IDENT '.' IDENT ']';
	
LOGICAL_MEMBER_SET_PREFIX
	:	'[' IDENT '.' '{';

SPARSE	:	('S'|'s') ('P'|'p') ('A'|'a') ('R'|'r') ('S'|'s') ('E'|'e');
	
fragment GRAIN	:	('G'|'g') ('R'|'r') ('A'|'a') ('I'|'i') ('N'|'n');
fragment MEASURETABLE	:
			('M'|'m') ('E'|'e') ('A'|'a') ('S'|'s') ('U'|'u') ('R'|'r') ('E'|'e') ('T'|'t') ('A'|'a') ('B'|'b') ('L'|'l') ('E'|'e');
fragment LEVEL	:	('L'|'l') ('E'|'e') ('V'|'v') ('E'|'e') ('L'|'l');
	
VARCHARTYPE	:	('V'|'v') ('A'|'a') ('R'|'r') ('C'|'c') ('H'|'h') ('A'|'a') ('R'|'r');
INTEGERTYPE	:	('I'|'i') ('N'|'n') ('T'|'t') ('E'|'e') ('G'|'g') ('E'|'e') ('R'|'r');
FLOATTYPE	:	('F'|'f') ('L'|'l') ('O'|'o') ('A'|'a') ('T'|'t');
NUMBERTYPE	:	('N'|'n') ('U'|'u') ('M'|'m') ('B'|'b') ('E'|'e') ('R'|'r');
DATETYPE	:	('D'|'d') ('A'|'a') ('T'|'t') ('E'|'e');
DATETIMETYPE	:	('D'|'d') ('A'|'a') ('T'|'t') ('E'|'e') ('T'|'t') ('I'|'i') ('M'|'m') ('E'|'e');
NONETYPE	:	('N'|'n') ('O'|'o') ('N'|'n') ('E'|'e');
STRING 
@init{StringBuilder lBuf = new StringBuilder();}
    	:  	'\'' ( 
    	EscapeSequence {lBuf.append(getText());} | 
    	(options {greedy=false;} : normal=~('\u0000'..'\u001f' | '\\' | '\'' ) {lBuf.appendCodePoint(normal);}) 
    	)* 
    	'\''  {setText(lBuf.insert(0,'\'').append('\'').toString());};
    	
INTEGER	
	:	('0'..'9')+;
FLOAT
	:	('0'..'9')* '.' ('0'..'9')+;
DATETIME
 	:	'#' (~'#')* '#' | ('N'|'n') ('O'|'o') ('W'|'w') 
 		| ('N'|'n') ('O'|'o') ('W'|'w') ('D'|'d') ('A'|'a') ('T'|'t') ('E'|'e') 
 		| ('N'|'n') ('O'|'o') ('W'|'w') ('D'|'d') ('A'|'a') ('T'|'t') ('E'|'e') ('T'|'t') ('I'|'i') ('M'|'m') ('E'|'e');
BOOLEAN
	:	(('T'|'t') ('R'|'r') ('U'|'u') ('E'|'e'))
	|	(('F'|'f') ('A'|'a') ('L'|'l') ('S'|'s') ('E'|'e'));
NULL	:	('N'|'n') ('U'|'u') ('L'|'l') ('L'|'l');
PARAMETER
	:	'%' INTEGER;

fragment IDENT
	:	('a'..'z' | 'A'..'Z' | '_' | ' ' | ':' | '$' | '#' | '%' | '-' | '/' | '&' | '!' | '^' | '@' | ',' | ';' | '>' | '<' | '0'..'9' | '\u0080'..'\uFFFF')+;

fragment EscapeSequence 
	:	'\\'
  	(	
  		'n'  {setText("\n");}
	|	'r'  {setText("\r");}
	|	't'  {setText("\t");}
	|	'\'' {setText("\'");}
	|	'\\' {setText("\\");}
	|	UnicodeEscape
	);

fragment UnicodeEscape
    	:    	'u' i=HexDigit j=HexDigit k=HexDigit l=HexDigit 
                   {setText(Character.toString((char)Integer.parseInt(i.getText()+j.getText()+k.getText()+l.getText(),16)));};

fragment HexDigit 
	: 	('0'..'9'|'a'..'f'|'A'..'F') ;

/* Ignore white spaces */	
WS	
	:  (' '|'\r'|'\t'|'\u000C'|'\n') {$channel=HIDDEN;};

PLUS	:	'+';
MINUS	:	'-';
MULT	:	'*';
DIV	:	'/';
MOD	:	'%';
POW	:	'^';
LT	:	'<';
LTEQ	:	'<=';
GT	:	'>';
GTEQ	:	'>=';
EQUALS	
	:	'=';
NOTEQUALS 
	:	'!=' | '<>';
BITAND:	'&';
BITOR : '|';
	
IN:
	('I'|'i') ('N'|'n');
NOTIN:
	('N'|'n') ('O'|'o') ('T'|'t') WS ('I'|'i') ('N'|'n');
LIKE:
	('L'|'l') ('I'|'i') ('K'|'k') ('E'|'e');
NOTLIKE:
	('N'|'n') ('O'|'o') ('T'|'t') WS ('L'|'l') ('I'|'i') ('K'|'k') ('E'|'e');	
	
PLUSEQUALS
	:	'+=';
MINUSEQUALS
	:	'-=';
NOT	:	'!' | ('N'|'n') ('O'|'o') ('T'|'t');
OR 	: 	'||' | ('O'|'o') ('R'|'r');
AND 	: 	'&&' | ('A'|'a') ('N'|'n') ('D'|'d');

ISNULL:
	('I'|'i') ('S'|'s') WS ('N'|'n') ('U'|'u') ('L'|'l') ('L'|'l');
ISNOTNULL
	:	('I'|'i') ('S'|'s') WS ('N'|'n') ('O'|'o') ('T'|'t') WS ('N'|'n') ('U'|'u') ('L'|'l') ('L'|'l');
	
INNERJOIN
	:	('I'|'i') ('N'|'n') ('N'|'n') ('E'|'e') ('R'|'r') WS ('J'|'j') ('O'|'o') ('I'|'i') ('N'|'n');
FULLOUTERJOIN
	:	('F'|'f') ('U'|'u') ('L'|'l') ('L'|'l')           WS ('O'|'o') ('U'|'u') ('T'|'t') ('E'|'e') ('R'|'r') WS ('J'|'j') ('O'|'o') ('I'|'i') ('N'|'n');
LEFTOUTERJOIN
	:	('L'|'l') ('E'|'e') ('F'|'f') ('T'|'t')           WS ('O'|'o') ('U'|'u') ('T'|'t') ('E'|'e') ('R'|'r') WS ('J'|'j') ('O'|'o') ('I'|'i') ('N'|'n');
RIGHTOUTERJOIN
	:	('R'|'r') ('I'|'i') ('G'|'g') ('H'|'h') ('T'|'t') WS ('O'|'o') ('U'|'u') ('T'|'t') ('E'|'e') ('R'|'r') WS ('J'|'j') ('O'|'o') ('I'|'i') ('N'|'n');
USINGOUTERJOIN
	:	('U'|'u') ('S'|'s') ('I'|'i') ('N'|'n') ('G'|'g') WS ('O'|'o') ('U'|'u') ('T'|'t') ('E'|'e') ('R'|'r') WS ('J'|'j') ('O'|'o') ('I'|'i') ('N'|'n');

UNION	:	('U'|'u') ('N'|'n') ('I'|'i') ('O'|'o') ('N'|'n');
UNIONALL
	:	('U'|'u') ('N'|'n') ('I'|'i') ('O'|'o') ('N'|'n') ('A'|'a') ('L'|'l') ('L'|'l');
	
ON	:	('O'|'o') ('N'|'n');
WHERE	:	('W'|'w') ('H'|'h') ('E'|'e') ('R'|'r') ('E'|'e');
ORDERBY	:	('O'|'o') ('R'|'r') ('D'|'d') ('E'|'e') ('R'|'r') WS ('B'|'b') ('Y'|'y');
ASCENDING
	:	('A'|'a') ('S'|'s') ('C'|'c') (('E'|'e') ('N'|'n') ('D'|'d') ('I'|'i') ('N'|'n') ('G'|'g'))?;
DESCENDING
	:	('D'|'d') ('E'|'e') ('S'|'s') ('C'|'c') (('E'|'e') ('N'|'n') ('D'|'d') ('I'|'i') ('N'|'n') ('G'|'g'))?;
PARTITION
	:	('P'|'p') ('A'|'a') ('R'|'r') ('T'|'t') ('I'|'i') ('T'|'t') ('I'|'i') ('O'|'o') ('N'|'n');
PARALLEL
	:	('P'|'p') ('A'|'a') ('R'|'r') ('A'|'a') ('L'|'l') ('L'|'l') ('E'|'e') ('L'|'l');

NULLS_LAST  : ('N'|'n') ('U'|'u') ('L'|'l') ('L'|'l') ('S'|'s') WS ('L'|'l') ('A'|'a') ('S'|'s') ('T'|'t');
NULLS_FIRST : ('N'|'n') ('U'|'u') ('L'|'l') ('L'|'l') ('S'|'s') WS ('F'|'f') ('I'|'i') ('R'|'r') ('S'|'s') ('T'|'t');
	
DISPLAYWHERE	
	:	('D'|'d') ('I'|'i') ('S'|'s') ('P'|'p') ('L'|'l') ('A'|'a') ('Y'|'y') WS ('W'|'w') ('H'|'h') ('E'|'e') ('R'|'r') ('E'|'e');
DISPLAYBY	
	:	('D'|'d') ('I'|'i') ('S'|'s') ('P'|'p') ('L'|'l') ('A'|'a') ('Y'|'y') WS ('B'|'b') ('Y'|'y');
TOP	:	('T'|'t') ('O'|'o') ('P'|'p');

IF	:	('I'|'i') ('F'|'f');
THEN	:	('T'|'t') ('H'|'h') ('E'|'e') ('N'|'n');
ELSE	:	('E'|'e') ('L'|'l') ('S'|'s') ('E'|'e');
ELSEIF	:	('E'|'e') ('L'|'l') ('S'|'s') ('E'|'e') ('I'|'i') ('F'|'f');
ENDIF	:	('E'|'e') ('N'|'n') ('D'|'d') WS ('I'|'i') ('F'|'f');

WHILE 	:	('W'|'w') ('H'|'h') ('I'|'i') ('L'|'l') ('E'|'e');
ENDWHILE:	('E'|'e') ('N'|'n') ('D'|'d') WS ('W'|'w') ('H'|'h') ('I'|'i') ('L'|'l') ('E'|'e');
EXITWHILE:	('E'|'e') ('X'|'x') ('I'|'i') ('T'|'t') WS ('W'|'w') ('H'|'h') ('I'|'i') ('L'|'l') ('E'|'e');

FOR	:	('F'|'f') ('O'|'o') ('R'|'r');
TO	:	('T'|'t') ('O'|'o');
NEXT	:	('N'|'n') ('E'|'e') ('X'|'x') ('T'|'t');
EXITFOR	:	('E'|'e') ('X'|'x') ('I'|'i') ('T'|'t') WS ('F'|'f') ('O'|'o') ('R'|'r');
STEP	:	('S'|'s') ('T'|'t') ('E'|'e') ('P'|'p');

COMPLETE
	:	('C'|'c') ('O'|'o') ('M'|'m') ('P'|'p') ('L'|'l') ('E'|'e') ('T'|'t') ('E'|'e');
ENDCOMPLETE
	:	('E'|'e') ('N'|'n') ('D'|'d') WS ('C'|'c') ('O'|'o') ('M'|'m') ('P'|'p') ('L'|'l') ('E'|'e') ('T'|'t') ('E'|'e');

LIST	:	('L'|'l') ('I'|'i') ('S'|'s') ('T'|'t');
ADD	:	('A'|'a') ('D'|'d') ('D'|'d');
REMOVEAT
	:	('R'|'r') ('E'|'e') ('M'|'m') ('O'|'o') ('V'|'v') ('E'|'e') ('A'|'a') ('T'|'t');
REMOVEALL
	:	('R'|'r') ('E'|'e') ('M'|'m') ('O'|'o') ('V'|'v') ('E'|'e') ('A'|'a') ('L'|'l') ('L'|'l');

MAP	:	('M'|'m') ('A'|'a') ('P'|'p');
PUT	:	('P'|'p') ('U'|'u') ('T'|'t');
REMOVEITEM
	:	('R'|'r') ('E'|'e') ('M'|'m') ('O'|'o') ('V'|'v') ('E'|'e') ('I'|'i') ('T'|'t') ('E'|'e') ('M'|'m');
DELETE	:	('D'|'d') ('E'|'e') ('L'|'l') ('E'|'e') ('T'|'t') ('E'|'e');

PRINT	:	('P'|'p') ('R'|'r') ('I'|'i') ('N'|'n') ('T'|'t');
HALT	:	('H'|'h') ('A'|'a') ('L'|'l') ('T'|'t');

/* string methods */
SUBSTRING
	:	('S'|'s') ('U'|'u') ('B'|'b') ('S'|'s') ('T'|'t') ('R'|'r') ('I'|'i') ('N'|'n') ('G'|'g');
LENGTH	:	('L'|'l') ('E'|'e') ('N'|'n') ('G'|'g') ('T'|'t') ('H'|'h');
POSITION:	('P'|'p') ('O'|'o') ('S'|'s') ('I'|'i') ('T'|'t') ('I'|'i') ('O'|'o') ('N'|'n');

TOUPPER:	('T'|'t') ('O'|'o') ('U'|'u') ('P'|'p') ('P'|'p') ('E'|'e') ('R'|'r');
TOLOWER:	('T'|'t') ('O'|'o') ('L'|'l') ('O'|'o') ('W'|'w') ('E'|'e') ('R'|'r');
TRIM	:	('T'|'t') ('R'|'r') ('I'|'i') ('M'|'m');
FORMAT	:	('F'|'f') ('O'|'o') ('R'|'r') ('M'|'m') ('A'|'a') ('T'|'t');
REPLACE	:	('R'|'r') ('E'|'e') ('P'|'p') ('L'|'l') ('A'|'a') ('C'|'c') ('E'|'e');
REPLACEALL	:
		('R'|'r') ('E'|'e') ('P'|'p') ('L'|'l') ('A'|'a') ('C'|'c') ('E'|'e') ('A'|'a') ('L'|'l') ('L'|'l');
URLENCODE	:
		('U'|'u') ('R'|'r') ('L'|'l') ('E'|'e') ('N'|'n') ('C'|'c') ('O'|'o') ('D'|'d') ('E'|'e');

/* int methods */
TOTIME	:	('T'|'t') ('O'|'o') ('T'|'t') ('I'|'i') ('M'|'m') ('E'|'e');

/* date methods */
DATEPART:	('D'|'d') ('A'|'a') ('T'|'t') ('E'|'e') ('P'|'p') ('A'|'a') ('R'|'r') ('T'|'t');
DATEDIFF:	('D'|'d') ('A'|'a') ('T'|'t') ('E'|'e') ('D'|'d') ('I'|'i') ('F'|'f') ('F'|'f');
DATEADD:	('D'|'d') ('A'|'a') ('T'|'t') ('E'|'e') ('A'|'a') ('D'|'d') ('D'|'d');

IIF	:	('I'|'i') ('I'|'i') ('F'|'f');
IFNULL:	('I'|'i') ('F'|'f') ('N'|'n') ('U'|'u') ('L'|'l') ('L'|'l');

DATEFIELD
	:	('Y'|'y') ('E'|'e') ('A'|'a') ('R'|'r') |
		('Q'|'q') ('U'|'u') ('A'|'a') ('R'|'r') ('T'|'t') ('E'|'e') ('R'|'r') |
		('M'|'m') ('O'|'o') ('N'|'n') ('T'|'t') ('H'|'h') |
		('W'|'w') ('E'|'e') ('E'|'e') ('K'|'k') |
		('D'|'d') ('A'|'a') ('Y'|'y') |		
		('H'|'h') ('O'|'o') ('U'|'u') ('R'|'r') |
		('M'|'m') ('I'|'i') ('N'|'n') ('U'|'u') ('T'|'t') ('E'|'e') |
		('S'|'s') ('E'|'e') ('C'|'c') ('O'|'o') ('N'|'n') ('D'|'d');
GETCOLUMNVALUE
	:	('G'|'g') ('E'|'e') ('T'|'t') ('C'|'c') ('O'|'o') ('L'|'l') ('U'|'u') ('M'|'m') ('N'|'n') ('V'|'v') ('A'|'a') ('L'|'l') ('U'|'u') ('E'|'e');
GETVARIABLE
	:	('G'|'g') ('E'|'e') ('T'|'t') ('V'|'v') ('A'|'a') ('R'|'r') ('I'|'i') ('A'|'a') ('B'|'b') ('L'|'l') ('E'|'e');
SAVEDEXPRESSION
	:   ('S'|'s') ('A'|'a') ('V'|'v') ('E'|'e') ('D'|'d') ('E'|'e') ('X'|'x') ('P'|'p') ('R'|'r') ('E'|'e') ('S'|'s') ('S'|'s') ('I'|'i') ('O'|'o') ('N'|'n');
ADDPARENTCHILD
	:	('A'|'a') ('D'|'d') ('D'|'d') ('P'|'p') ('A'|'a') ('R'|'r') ('E'|'e') ('N'|'n') ('T'|'t') ('C'|'c') ('H'|'h') ('I'|'i') ('L'|'l') ('D'|'d');
NEXTCHILD
	:	('N'|'n') ('E'|'e') ('X'|'x') ('T'|'t') ('C'|'c') ('H'|'h') ('I'|'i') ('L'|'l') ('D'|'d');
GETLEVELVALUE
	:	('G'|'g') ('E'|'e') ('T'|'t') ('L'|'l') ('E'|'e') ('V'|'v') ('E'|'e') ('L'|'l') ('V'|'v') ('A'|'a') ('L'|'l') ('U'|'u') ('E'|'e');
GETLEVELATTRIBUTE
	:	('G'|'g') ('E'|'e') ('T'|'t') ('L'|'l') ('E'|'e') ('V'|'v') ('E'|'e') ('L'|'l') ('A'|'a') ('T'|'t') ('T'|'t') ('R'|'r') ('I'|'i') ('B'|'b') ('U'|'u') ('T'|'t') ('E'|'e');
GETDAYID
	:	('G'|'g') ('E'|'e') ('T'|'t') ('D'|'d') ('A'|'a') ('Y'|'y') ('I'|'i') ('D'|'d');
GETWEEKID
	:	('G'|'g') ('E'|'e') ('T'|'t') ('W'|'w') ('E'|'e') ('E'|'e') ('K'|'k') ('I'|'i') ('D'|'d');
GETMONTHID
	:	('G'|'g') ('E'|'e') ('T'|'t') ('M'|'m') ('O'|'o') ('N'|'n') ('T'|'t') ('H'|'h') ('I'|'i') ('D'|'d');
SETLOOKUP	:	('L'|'l') ('O'|'o') ('O'|'o') ('K'|'k') ('U'|'u') ('P'|'p') ('V'|'v') ('A'|'a') ('L'|'l') ('U'|'u') ('E'|'e');
SETLOOKUPROW	:	('L'|'l') ('O'|'o') ('O'|'o') ('K'|'k') ('U'|'u') ('P'|'p') ('R'|'r') ('O'|'o') ('W'|'w');
SETFIND	:	('F'|'f') ('I'|'i') ('N'|'n') ('D'|'d');
SETSTAT	:	('S'|'s') ('T'|'t') ('A'|'a') ('T'|'t');
LET	:	('L'|'l') ('E'|'e') ('T'|'t');
FUNCTION	
	:	('F'|'f') ('U'|'u') ('N'|'n') ('C'|'c') ('T'|'t') ('I'|'i') ('O'|'o') ('N'|'n');
RETURN	:	('R'|'r') ('E'|'e') ('T'|'t') ('U'|'u') ('R'|'r') ('N'|'n');
ENDFUNCTION	
	:	('E'|'e') ('N'|'n') ('D'|'d') WS ('F'|'f') ('U'|'u') ('N'|'n') ('C'|'c') ('T'|'t') ('I'|'i') ('O'|'o') ('N'|'n');
FUNCTIONLOOKUP	
	:	('F'|'f') ('U'|'u') ('N'|'n') ('C'|'c') ('T'|'t') ('I'|'i') ('O'|'o') ('N'|'n') ('L'|'l') ('O'|'o') ('O'|'o') ('K'|'k') ('U'|'u') ('P'|'p');
TRANSFORM
	:	('T'|'t') ('R'|'r') ('A'|'a') ('N'|'n') ('S'|'s') ('F'|'f') ('O'|'o') ('R'|'r') ('M'|'m');

TREND
	:	('T'|'t') ('R'|'r') ('E'|'e') ('N'|'n') ('D'|'d');

MDXEXPRESSION
	:	('M'|'m') ('D'|'d') ('X'|'x') ('E'|'e') ('X'|'x') ('P'|'p') ('R'|'r') ('E'|'e') ('S'|'s') ('S'|'s') ('I'|'i') ('O'|'o') ('N'|'n');
		
STATMEDIAN
	:	('M'|'m') ('E'|'e') ('D'|'d') ('I'|'i') ('A'|'a') ('N'|'n');
STATSTDDEV
	:	('S'|'s') ('T'|'t') ('D'|'d') ('D'|'d') ('E'|'e') ('V'|'v');
NUMROWS	:	('N'|'n') ('U'|'u') ('M'|'m') ('R'|'r') ('O'|'o') ('W'|'w') ('S'|'s');
ROWNUMBER
	:	('R'|'r') ('O'|'o') ('W'|'w') ('N'|'n') ('U'|'u') ('M'|'m') ('B'|'b') ('E'|'e') ('R'|'r');
GROUPS	:	('G'|'g') ('R'|'r') ('O'|'o') ('U'|'u') ('P'|'p') ('S'|'s');
GETPROMPTVALUE
	:	('G'|'g') ('E'|'e') ('T'|'t') ('P'|'p') ('R'|'r') ('O'|'o') ('M'|'m') ('P'|'p') ('T'|'t') ('V'|'v') ('A'|'a') ('L'|'l') ('U'|'u') ('E'|'e');
GETPROMPTFILTER
	:	('G'|'g') ('E'|'e') ('T'|'t') ('P'|'p') ('R'|'r') ('O'|'o') ('M'|'m') ('P'|'p') ('T'|'t') ('F'|'f') ('I'|'i') ('L'|'l') ('T'|'t') ('E'|'e') ('R'|'r');
TOSTRING
	:	('T'|'t') ('O'|'o') ('S'|'s') ('T'|'t') ('R'|'r') ('I'|'i') ('N'|'n') ('G'|'g');
	
SELECTED
	:	('S'|'s') ('E'|'e') ('L'|'l') ('E'|'e') ('C'|'c') ('T'|'t') ('E'|'e') ('D'|'d');
CHILDREN
	:	('C'|'c') ('H'|'h') ('I'|'i') ('L'|'l') ('D'|'d') ('R'|'r') ('E'|'e') ('N'|'n');
ALLCHILDREN
	:	('A'|'a') ('L'|'l') ('L'|'l') ('C'|'c') ('H'|'h') ('I'|'i') ('L'|'l') ('D'|'d') ('R'|'r') ('E'|'e') ('N'|'n');
LEAVES
	:	('L'|'l') ('E'|'e') ('A'|'a') ('V'|'v') ('E'|'e') ('S'|'s');	
PARENTS
	:	('P'|'p') ('A'|'a') ('R'|'r') ('E'|'e') ('N'|'n') ('T'|'t') ('S'|'s');		
CUBE	
	:	('C'|'c') ('U'|'u') ('B'|'b') ('E'|'e');
	
CURRENTVALUE
	:	('C'|'c') ('U'|'u') ('R'|'r') ('R'|'r') ('E'|'e') ('N'|'n') ('T'|'t') ('V'|'v') ('A'|'a') ('L'|'l') ('U'|'u') ('E'|'e');

// math functions
SIN	:	('S'|'s') ('I'|'i') ('N'|'n');
COS	:	('C'|'c') ('O'|'o') ('S'|'s');
TAN	:	('T'|'t') ('A'|'a') ('N'|'n');
ARCSIN	:	('A'|'a') ('R'|'r') ('C'|'c') ('S'|'s') ('I'|'i') ('N'|'n');
ARCCOS	:	('A'|'a') ('R'|'r') ('C'|'c') ('C'|'c') ('O'|'o') ('S'|'s');
ARCTAN	:	('A'|'a') ('R'|'r') ('C'|'c') ('T'|'t') ('A'|'a') ('N'|'n');
ABS	:	('A'|'a') ('B'|'b') ('S'|'s');
CEILING	:	('C'|'c') ('E'|'e') ('I'|'i') ('L'|'l') ('I'|'i') ('N'|'n') ('G'|'g');
FLOOR	:	('F'|'f') ('L'|'l') ('O'|'o') ('O'|'o') ('R'|'r');
SQRT	:	('S'|'s') ('Q'|'q') ('R'|'r') ('T'|'t');
LN	:	('L'|'l') ('N'|'n');
LOG	:	('L'|'l') ('O'|'o') ('G'|'g');
EXP	:	('E'|'e') ('X'|'x') ('P'|'p');
POWER	:	('P'|'p') ('O'|'o') ('W'|'w');
RANDOM : ('R'|'r') ('A'|'a') ('N'|'n') ('D'|'d') ('O'|'o') ('M'|'m');

// boolean functions
ISNAN : ('I'|'i') ('S'|'s') ('N'|'n') ('A'|'a') ('N'|'n');
ISINF : ('I'|'i') ('S'|'s') ('I'|'i') ('N'|'n') ('F'|'f');

// Logical query modifiers
DRANK	:	('D'|'d') ('R'|'r') ('A'|'a') ('N'|'n') ('K'|'k');
RANK	:	('R'|'r') ('A'|'a') ('N'|'n') ('K'|'k');
PTILE	:	('P'|'p') ('T'|'t') ('I'|'i') ('L'|'l') ('E'|'e');
RSUM	:	('R'|'r') ('S'|'s') ('U'|'u') ('M'|'m');

BY	:	('B'|'b') ('Y'|'y');

REXP	:	('R'|'r') ('E'|'e') ('X'|'x') ('P'|'p');
RWRITE	:	('R'|'r') ('W'|'w') ('R'|'r') ('I'|'i') ('T'|'t') ('E'|'e');

// Comments
LINE_COMMENT : '//' .* '\n' {$channel=HIDDEN;};
ML_COMMENT
    :   '/*' (options {greedy=false;} : .)* '*/' {$channel=HIDDEN;};
    
// ETL Statements

definition
	:	 input output script;
	
input	:	INPUT! inputquery;

inputquery
	:	inputsinglequery unionquery* orderby? partitionby?;
	
unionquery
	:	unionrelation^ inputsinglequery;
	
unionrelation
	:	UNION | UNIONALL;

inputsinglequery
	:	query joinquery* -> ^(SINGLEQUERY query joinquery*);
	
joinquery
	:	joinrelation^ query joincondition;

joinrelation
	:	INNERJOIN|LEFTOUTERJOIN|RIGHTOUTERJOIN|FULLOUTERJOIN;
	
joincondition
	:	jointerm logicalExpression -> ^(jointerm logicalExpression);
	
jointerm:	ON;
	
query	:	SELECT PARALLEL? DISTINCT? top? projectionlist FROM from (WHERE logicalExpression)? -> ^(SELECT PARALLEL? DISTINCT? top? projectionlist ^(FROM from) ^(WHERE logicalExpression)?);

from	:	IDENT_NAME | GRAIN_TABLE | LEVEL_TABLE;

orderby	:	ORDERBY^ orderbyprojectlist;

partitionby
	:	PARTITION BY partitionbylist -> ^(PARTITION partitionbylist);
partitionbylist:
		projection (COMMA projection)* -> projection projection*;

orderbyprojectlist
	:	orderbyprojection (COMMA orderbyprojection)* -> ^(PROJECTIONLIST orderbyprojection*);

orderbyprojection
	:	projection order?;

order	:	ASCENDING | DESCENDING;

projectionlist
	:	nullprojection (COMMA nullprojection)* -> ^(PROJECTIONLIST nullprojection*);

nullprojection
	:	projection | NULL | STRING | INTEGER | FLOAT;

projection
	:	COLUMN_NAME | GRAIN_COLUMN | LEVEL_COLUMN |
	aggrule LEFT_PAREN COLUMN_NAME RIGHT_PAREN -> ^(aggrule COLUMN_NAME) |
	aggrule LEFT_PAREN GRAIN_COLUMN RIGHT_PAREN -> ^(aggrule GRAIN_COLUMN) |
	aggrule LEFT_PAREN LEVEL_COLUMN RIGHT_PAREN -> ^(aggrule LEVEL_COLUMN);

aggrule	:	SUM | AVG | COUNTDISTINCT | COUNT | MIN | MAX;

output	:	OUTPUT! outputquery;

outputquery
	:	 IDENT_NAME COLUMNS outputcolumnlist? -> ^(OUTPUT IDENT_NAME outputcolumnlist?);

outputcolumnlist
	:	outputcolumnspec (COMMA outputcolumnspec)* -> ^(OUTPUTCOLUMNLIST outputcolumnspec*);

outputcolumnspec
	:	IDENT_NAME outputdatatype -> ^(OUTPUTCOLUMN IDENT_NAME outputdatatype);
	
outputdatatype
	:	datatype | NONETYPE;
	
datatype:	VARCHARTYPE^ LEFT_PAREN! INTEGER RIGHT_PAREN! | INTEGERTYPE | NUMBERTYPE | FLOATTYPE | DATETYPE | DATETIMETYPE;

script	:	BEGIN! statementlist END!;

statementlist
	:	statement* -> ^(SCRIPT statement*);

statement
	:	assignment | writerecord | variabledefinition | ifthenelse | whileloop | exitwhile | fornext | exitfor | oncomplete | arraystatement | objectstatement | 
	mapstatement | dmlstatement | print | halt | updatesfdc | subroutinedeclaration | subreturn | subroutinecall | rstatement;
	
assignment
	:	IDENT_NAME EQUALS logicalExpression -> ^(ASSIGNMENT IDENT_NAME logicalExpression) |
		subroutinecall EQUALS logicalExpression -> ^(ASSIGNMENT subroutinecall logicalExpression) |
		trendmethod EQUALS logicalExpression -> ^(ASSIGNMENT trendmethod logicalExpression);

print : PRINT logicalExpression -> ^(PRINT logicalExpression);

halt	:	HALT LEFT_PAREN logicalExpression RIGHT_PAREN -> ^(HALT logicalExpression);

writerecord
	:	WRITERECORD;
	
updatesfdc
	: UPDATESFDC IDENT_NAME -> ^(UPDATESFDC IDENT_NAME);
	
variableValue:	logicalExpression | query;

variabledefinition
	:	simplevariabledefinition |
		DIM IDENT_NAME AS arraydef (EQUALS query)? -> ^(DIM IDENT_NAME arraydef query?) |
		DIM IDENT_NAME AS mapdef (EQUALS query)?-> ^(DIM IDENT_NAME mapdef query?);
		
simplevariabledefinition
	:	DIM IDENT_NAME AS datatype (EQUALS variableValue)? -> ^(DIM IDENT_NAME datatype variableValue?);
		
arraydef:	LIST LEFT_PAREN outputdatatype RIGHT_PAREN -> ^(LIST outputdatatype);
	
mapdef:		MAP LEFT_PAREN datatype COMMA outputdatatype RIGHT_PAREN -> ^(MAP datatype outputdatatype);
	
ifthenelse
 	:	ifthenexpr elseifexpr* elseexpr? ENDIF -> ^(IF ifthenexpr elseifexpr* elseexpr?);
 	
ifthenexpr	:	
		IF logicalExpression THEN statementlist -> ^(IFTHEN logicalExpression statementlist);
 
thenexpr
 	:	THEN! statementlist;
 	
elseifexpr
 	:	ELSEIF logicalExpression THEN statementlist -> ^(IFTHEN logicalExpression statementlist);
		
elseexpr:	ELSE statementlist -> ^(ELSE  statementlist);

whileloop
	:	WHILE logicalExpression statementlist ENDWHILE -> ^(WHILE logicalExpression statementlist);
	
exitwhile
	:	EXITWHILE;
	
fornext	:	FOR forvar EQUALS toexp statementlist NEXT IDENT_NAME? -> ^(FOR forvar toexp statementlist IDENT_NAME?);

forvar	:	IDENT_NAME (AS fortype)? -> ^(IDENT_NAME fortype?);

fortype	:	INTEGERTYPE | FLOATTYPE;

toexp	:	logicalExpression TO logicalExpression (STEP logicalExpression)? -> ^(TO logicalExpression logicalExpression logicalExpression?);

exitfor	:	EXITFOR;

oncomplete
	:	COMPLETE statementlist ENDCOMPLETE -> ^(COMPLETE statementlist);

subroutinedeclaration
	:	FUNCTION IDENT_NAME '(' subinputvar* ')' (AS datatype)? statementlist ENDFUNCTION ->
		^(FUNCTIONDEF IDENT_NAME subinputvar* statementlist datatype?);
		
subinputvar
	:	','? IDENT_NAME AS datatype -> ^(IDENT_NAME datatype);
	
subroutinecall
	:	IDENT_NAME '(' ')' -> ^(FUNCTIONCALL IDENT_NAME) |
		IDENT_NAME '(' logicalExpression (',' logicalExpression)* ')' -> ^(FUNCTIONCALL IDENT_NAME logicalExpression*);
		
subreturn	
	:	RETURN^ logicalExpression;
	

// ETL Methods

objectstatement
	:	removeall | addparentchild;

arraystatement
	:	add | removeat;
	
dmlstatement
	:	delete;
	
add	:	ADD LEFT_PAREN IDENT_NAME ',' logicalExpression RIGHT_PAREN -> ^(ADD IDENT_NAME logicalExpression);

removeat
	:	REMOVEAT LEFT_PAREN IDENT_NAME ',' logicalExpression RIGHT_PAREN -> ^(REMOVEAT IDENT_NAME logicalExpression);

removeall
	:	REMOVEALL LEFT_PAREN IDENT_NAME RIGHT_PAREN -> ^(REMOVEALL IDENT_NAME);
	
mapstatement
	:	removeitem;
	
removeitem
	:	REMOVEITEM LEFT_PAREN IDENT_NAME ',' logicalExpression RIGHT_PAREN -> ^(REMOVEITEM IDENT_NAME logicalExpression);
	
delete	:	DELETE FROM from (WHERE logicalExpression)? -> ^(DELETE ^(FROM from) ^(WHERE logicalExpression)?);

rstatement
	:	RWRITE LEFT_PAREN logicalExpression ',' logicalExpression RIGHT_PAREN -> ^(RWRITE logicalExpression logicalExpression);

// Olap Logical query
olaplogicalquery	:
	SELECT top? USINGOUTERJOIN? projections FROM OLAPCUBE logicalQueryModifiers* -> 
	^(SELECT top? USINGOUTERJOIN? projections ^(FROM OLAPCUBE) logicalQueryModifiers*);

projections
	:	olapProjection (COMMA olapProjection)* -> ^(PROJECTIONLISTS (olapProjection)*);

olapProjection
	:	(logicalMemberSets | logicalColumns)+;

logicalMemberSets
	:	olapLogicalExpression (COMMA olapLogicalExpression)* -> ^(OLAPPROJECTIONLIST olapLogicalExpression*);
	
olapLogicalExpression
	:	logicalMemberSet | mdxexpression;

mdxexpression
	:	MDXEXPRESSION LEFT_PAREN STRING RIGHT_PAREN alias? -> ^(MDXEXPRESSION ^(STRING) ^(ALIAS alias)?);

logicalMemberSet
	:	LOGICAL_MEMBER_SET_PREFIX memberExpressions '}' ']' alias? -> ^(LOGICALMEMBERSET ^(LOGICAL_MEMBER_SET_PREFIX) memberExpressions ^(ALIAS alias)?);

memberExpressions
	:	memberExpression (COMMA memberExpression)* -> ^(MEMBEREXPRESSIONS memberExpression*);
	
memberExpression
	:	selectedMemberExpression | childMembersExpression | allChildMembersExpression | leafMembersExpression | parentMembersExpression;

selectedMemberExpression
	:	SELECTED LEFT_PAREN STRING RIGHT_PAREN -> ^(SELECTED ^(STRING));

childMembersExpression
	:	CHILDREN LEFT_PAREN STRING RIGHT_PAREN -> ^(CHILDREN ^(STRING));

allChildMembersExpression
	:	ALLCHILDREN LEFT_PAREN STRING RIGHT_PAREN -> ^(ALLCHILDREN ^(STRING));

leafMembersExpression
	:	LEAVES LEFT_PAREN STRING RIGHT_PAREN -> ^(LEAVES ^(STRING));

parentMembersExpression
	:	PARENTS LEFT_PAREN STRING RIGHT_PAREN -> ^(PARENTS ^(STRING));

// Logical query

logicalquery	:
	SELECT top? USINGOUTERJOIN? logicalColumns FROM subjectarea logicalQueryModifiers* -> 
	^(SELECT top? USINGOUTERJOIN? logicalColumns ^(FROM subjectarea) logicalQueryModifiers*);

subjectarea	:	
	IDENT_NAME;
	
top	:	TOP^ logicalExpression;

logicalColumns
	:	logicalColumn (COMMA logicalColumn)* -> ^(PROJECTIONLIST logicalColumn*);
	
logicalColumn
	:	logicalExpression (WHERE logicalExpression)? alias? 
			-> ^(logicalExpression ^(WHERE logicalExpression)? ^(ALIAS alias)?); 			

alias:
	STRING;

logicalQueryModifiers
	:	logicalWhere | displayWhere | logicalOrderBy | displayBy;

logicalWhere
	:	WHERE^ logicalExpression;

displayWhere
	:	DISPLAYWHERE^ logicalExpression;
	
logicalOrderBy
	:	ORDERBY^ logicalOrderByProjectionList;

logicalOrderByProjectionList
	:	logicalOrderByProjection (COMMA logicalOrderByProjection)* -> ^(PROJECTIONLIST logicalOrderByProjection*);

logicalOrderByProjection
	:	logicalOrderByColumn^ order?;
	
logicalOrderByColumn
	:	IDENT_NAME | COLUMN_NAME;
	
displayBy
	:	DISPLAYBY^ logicalDisplayByProjectionList;

logicalDisplayByProjectionList
	:	logicaDisplayByProjection (COMMA logicaDisplayByProjection)* -> ^(PROJECTIONLIST logicaDisplayByProjection*);

logicaDisplayByProjection
	:	logicalDisplayByColumn^ order? nulls?;
	
nulls	: NULLS_FIRST | NULLS_LAST;
	
logicalDisplayByColumn
	:	IDENT_NAME | COLUMN_NAME;
	
// Expression logic

valuelist
	:	value (COMMA value)* -> ^(VALUELIST value*);

logicalExpression
	:	booleanAndExpression (OR^ booleanAndExpression )* ;

booleanAndExpression
	:	equalityExpression (AND^ equalityExpression)* ;

equalityExpression
	:	nullExpression ((EQUALS|NOTEQUALS|LIKE|NOTLIKE)^ nullExpression)*;


nullExpression
	:	relationalExpression ((ISNULL|ISNOTNULL)^)?;


relationalExpression
	:	additiveExpression ( (LT|LTEQ|GT|GTEQ)^ additiveExpression )*;

additiveExpression
	:	multiplicativeExpression ( (PLUS|MINUS)^ multiplicativeExpression )*;


multiplicativeExpression 
	:	powerExpression ( (MULT|DIV|MOD|BITAND|BITOR)^ powerExpression )*;
	
powerExpression 
	:	unaryExpression ( POW^ unaryExpression )*;
	
unaryExpression
	:	primaryExpression
    	|	NOT^ primaryExpression
    	|	MINUS primaryExpression -> ^(NEGATE primaryExpression)
    	|	COLUMN_NAME IN '(' logicalquery ')' -> ^(IN COLUMN_NAME logicalquery)
    	|	COLUMN_NAME NOTIN '(' logicalquery ')' -> ^(NOTIN COLUMN_NAME logicalquery)
    	|	COLUMN_NAME IN '(' valuelist ')' -> ^(IN COLUMN_NAME valuelist)
    	|	COLUMN_NAME NOTIN '(' valuelist ')' -> ^(NOTIN COLUMN_NAME valuelist);  

primaryExpression
	:	'(' logicalExpression ')' -> ^(PAREN logicalExpression)
	| 	subroutinecall
	|	trendmethod
	|	mdxexpression
	|	measureFilter
	|	aggmethod
	|	projection
	| 	stringmethod
	|	intmethod
	|	objectmethod
	| 	mathmethod
	| 	boolmethod
	|	datemethod
	|	cast
	|	getvaluemethod
	|	getpromptfilter
	|	sparsemethod
	|	dimensionexpressionmethod
	|	rmethod
	|	value ;
	
trendmethod
	:	TREND^ LEFT_PAREN! logicalExpression ','! logicalExpression ','! logicalExpression ','! logicalquery RIGHT_PAREN!;

value	
	: 	INTEGER
	|	FLOAT
	| 	DATETIME
	|	BOOLEAN
	|	STRING
	| 	IDENT_NAME^ positionalreference*
	|	PARAMETER
	|	NULL;
	
measureFilter
	:	'(' IDENT_NAME WHERE logicalExpression ')' -> ^(IDENT_NAME ^(WHERE logicalExpression));
	
//rank	:	(RANK^ | PTILE^) '('! logicalExpression rankwhere? rankby? ')'!;

//rankwhere
//	:	WHERE logicalExpression -> ^(WHERE logicalExpression);

//rankby	:	BY COLUMN_NAME (',' COLUMN_NAME)* -> ^(BY COLUMN_NAME COLUMN_NAME*);


positionalreference options {backtrack=false;}
	:
		// Positional Calc
		POSITION_COLUMN_START (positionaloperator positionalvalue)? '}' -> ^(POSITION POSITION_COLUMN_START (positionaloperator positionalvalue)?) |
		// Dimensional Expressions
		'{' '@' (COLUMN_NAME+ ':')? logicalExpression '}' -> ^(POSITION logicalExpression COLUMN_NAME*);
		
dimensionexpressionmethod
	:	CURRENTVALUE '(' COLUMN_NAME ')' -> ^(CURRENTVALUE COLUMN_NAME) |
		CURRENTVALUE '(' IDENT_NAME ')' -> ^(CURRENTVALUE IDENT_NAME);
		
positionalvalue
	:	STRING | INTEGER | BOOLEAN | NULL;
		
positionaloperator
	:	EQUALS | PLUSEQUALS | MINUSEQUALS;

// Functions

sparsemethod
	:	SPARSE '(' projection ')' -> ^(SPARSE projection);

objectmethod
	:	length | nextchild | getlevelvalue | getlevelattribute | getdayid | getweekid | getmonthid | iif | ifnull | 
		setlookupvalue | setlookuprow | setfind | numrows | rownum | groups | setstatistic |
		function | functionlookup | transform | let;

aggmethod
	:	rankmethod | rsum;
	
rankmethod
	:	(RANK^ | PTILE^ | STATMEDIAN^ | DRANK^) '('! logicalExpression rankby? ')'!;
	
rsum
	:	RSUM^ '('! logicalExpression ','! logicalExpression rankby? ')'!;

rankby	:	BY logicalExpression (',' logicalExpression)* -> ^(BY logicalExpression logicalExpression*);

length	:	LENGTH^ LEFT_PAREN! logicalExpression RIGHT_PAREN!;

stringmethod
	:	substring | position | tolower | toupper | trim | format | replace | replaceall | tostring | urlencode;
	
tostring
    :   TOSTRING^ LEFT_PAREN! logicalExpression RIGHT_PAREN!;

substring
	:	SUBSTRING^ LEFT_PAREN! logicalExpression ','! logicalExpression (RIGHT_PAREN! | ','! logicalExpression RIGHT_PAREN!);
	
position:	POSITION^ LEFT_PAREN! logicalExpression ','! logicalExpression (','! logicalExpression)? RIGHT_PAREN!;

toupper:	TOUPPER^ LEFT_PAREN! logicalExpression RIGHT_PAREN!;

tolower:	TOLOWER^ LEFT_PAREN! logicalExpression RIGHT_PAREN!;

trim:		TRIM^ LEFT_PAREN! logicalExpression RIGHT_PAREN!;

format:		FORMAT^ LEFT_PAREN! logicalExpression ','! logicalExpression RIGHT_PAREN!;

urlencode:	URLENCODE^ LEFT_PAREN! logicalExpression ','! logicalExpression RIGHT_PAREN!;

replace:	REPLACE^ LEFT_PAREN! logicalExpression ','! logicalExpression ','! logicalExpression RIGHT_PAREN!;

replaceall:	REPLACEALL^ LEFT_PAREN! logicalExpression ','! logicalExpression ','! logicalExpression RIGHT_PAREN!;

intmethod
	:	totime;
	
totime	:	TOTIME^ LEFT_PAREN! logicalExpression ','! STRING ','! STRING RIGHT_PAREN!;
	
datemethod
	:	datepart | datediff | dateadd;
	
datepart:	DATEPART^ LEFT_PAREN! DATEFIELD ','! logicalExpression RIGHT_PAREN!;

datediff:	DATEDIFF^ LEFT_PAREN! DATEFIELD ','! logicalExpression ','! logicalExpression RIGHT_PAREN!;

dateadd:	DATEADD^ LEFT_PAREN! DATEFIELD ','! logicalExpression ','! logicalExpression RIGHT_PAREN!;

iif	:	IIF^ LEFT_PAREN! logicalExpression ','! logicalExpression ','! logicalExpression RIGHT_PAREN!;

ifnull	:	IFNULL^ LEFT_PAREN! logicalExpression ','! logicalExpression RIGHT_PAREN!;

grainname
	:	GRAIN^ LEFT_PAREN!;
	
levelname
	:	LEVEL^ LEFT_PAREN!;

cast	:	INTEGERTYPE LEFT_PAREN logicalExpression RIGHT_PAREN -> ^(CAST INTEGERTYPE logicalExpression) |
		FLOATTYPE LEFT_PAREN logicalExpression RIGHT_PAREN -> ^(CAST FLOATTYPE logicalExpression) |
		DATETIMETYPE LEFT_PAREN logicalExpression RIGHT_PAREN -> ^(CAST DATETIMETYPE logicalExpression) |
		VARCHARTYPE LEFT_PAREN logicalExpression RIGHT_PAREN -> ^(CAST VARCHARTYPE logicalExpression);

getvaluemethod
	:	getvariable | getpromptvalue | getcolumnvalue | savedexpression;

getvariable
	:	GETVARIABLE LEFT_PAREN STRING (',' logicalExpression)? RIGHT_PAREN -> ^(GETVARIABLE STRING logicalExpression?);
	
getpromptvalue
	:	GETPROMPTVALUE LEFT_PAREN STRING (',' logicalExpression)? RIGHT_PAREN -> ^(GETPROMPTVALUE STRING logicalExpression?);

getcolumnvalue
	:	GETCOLUMNVALUE LEFT_PAREN INTEGER RIGHT_PAREN -> ^(GETCOLUMNVALUE INTEGER);

savedexpression
	:	SAVEDEXPRESSION LEFT_PAREN STRING RIGHT_PAREN -> ^(SAVEDEXPRESSION STRING);
	
getpromptfilter
	:	GETPROMPTFILTER LEFT_PAREN STRING RIGHT_PAREN -> ^(GETPROMPTFILTER STRING);
nextchild
	:	NEXTCHILD LEFT_PAREN! RIGHT_PAREN!;
	
addparentchild
	:	ADDPARENTCHILD^ LEFT_PAREN! logicalExpression ','! logicalExpression RIGHT_PAREN!;
	
getlevelvalue
	:	GETLEVELVALUE^ LEFT_PAREN! logicalExpression RIGHT_PAREN!;
	
getlevelattribute
	:	GETLEVELATTRIBUTE^ LEFT_PAREN! logicalExpression RIGHT_PAREN!;
	
getdayid:	GETDAYID^ LEFT_PAREN! logicalExpression RIGHT_PAREN!;

getweekid:	GETWEEKID^ LEFT_PAREN! logicalExpression RIGHT_PAREN!;

getmonthid:	GETMONTHID^ LEFT_PAREN! logicalExpression RIGHT_PAREN!;

setlookupvalue	:	SETLOOKUP^ LEFT_PAREN! logicalExpression ','! logicalExpression ','! logicalExpression (','! logicalExpression)* ','! logicalquery RIGHT_PAREN!;

setlookuprow	:	SETLOOKUPROW^ LEFT_PAREN! logicalExpression ','! logicalExpression (','! logicalExpression)* ','! logicalquery RIGHT_PAREN!;

setfind	:	SETFIND^ LEFT_PAREN! logicalExpression ','! logicalExpression ','! logicalExpression (','! logicalExpression)* ','! logicalquery RIGHT_PAREN!;

stattype
	:	STATMEDIAN | MIN | MAX | AVG | SUM | COUNT | COUNTDISTINCT | STATSTDDEV;

setstatistic
	:	SETSTAT^ LEFT_PAREN! stattype ','! logicalExpression (','! logicalExpression)* ','! logicalquery RIGHT_PAREN!;

function:	FUNCTION^ LEFT_PAREN! datatype ','! statementlist (','! logicalExpression)* (','! logicalquery)? RIGHT_PAREN!;

functionlookup:	FUNCTIONLOOKUP^ LEFT_PAREN! datatype ','! logicalExpression ','! logicalExpression ','! statementlist (','! logicalExpression)* ','! logicalquery RIGHT_PAREN!;

transform
	:	TRANSFORM^  LEFT_PAREN! statementlist RIGHT_PAREN!;

let	:	LET LEFT_PAREN simplevariabledefinition+ ',' logicalExpression RIGHT_PAREN -> ^(LET ^(DIM simplevariabledefinition+) logicalExpression);

numrows	:	NUMROWS^ LEFT_PAREN! RIGHT_PAREN!;

rownum	:	ROWNUMBER^ LEFT_PAREN! RIGHT_PAREN!;

groups	:	GROUPS^ LEFT_PAREN! COLUMN_NAME (','! logicalExpression)+ RIGHT_PAREN!;

mathmethod
	:	sin | cos | tan | arcsin | arccos | arctan | abs | ceiling | floor | squareroot | ln | log | exp | power | random | max | min;
	
sin	:	SIN^ LEFT_PAREN! logicalExpression RIGHT_PAREN!;
cos	:	COS^ LEFT_PAREN! logicalExpression RIGHT_PAREN!;
tan	:	TAN^ LEFT_PAREN! logicalExpression RIGHT_PAREN!;
arcsin	:	ARCSIN^ LEFT_PAREN! logicalExpression RIGHT_PAREN!;
arccos	:	ARCCOS^ LEFT_PAREN! logicalExpression RIGHT_PAREN!;
arctan	:	ARCTAN^ LEFT_PAREN! logicalExpression RIGHT_PAREN!;
abs	:	ABS^ LEFT_PAREN! logicalExpression RIGHT_PAREN!;
ceiling	:	CEILING^ LEFT_PAREN! logicalExpression RIGHT_PAREN!;
floor	:	FLOOR^ LEFT_PAREN! logicalExpression RIGHT_PAREN!;
squareroot	:	SQRT^ LEFT_PAREN! logicalExpression RIGHT_PAREN!;
ln	:	LN^ LEFT_PAREN! logicalExpression RIGHT_PAREN!;
log	:	LOG^ LEFT_PAREN! logicalExpression RIGHT_PAREN!;
exp	:	EXP^ LEFT_PAREN! logicalExpression RIGHT_PAREN!;
power	:	POWER^ LEFT_PAREN! logicalExpression ','! logicalExpression RIGHT_PAREN!;
max	:	MAX LEFT_PAREN logicalExpression ',' logicalExpression RIGHT_PAREN -> ^(MATHMAX logicalExpression logicalExpression);
min	:	MIN LEFT_PAREN logicalExpression ',' logicalExpression RIGHT_PAREN -> ^(MATHMIN logicalExpression logicalExpression);
random 	: 	RANDOM^ LEFT_PAREN! RIGHT_PAREN!;

boolmethod : isnan | isinf;

isnan: ISNAN^ LEFT_PAREN! logicalExpression RIGHT_PAREN!;
isinf: ISINF^ LEFT_PAREN! logicalExpression RIGHT_PAREN!;

rmethod	:	REXP LEFT_PAREN datatype ',' logicalExpression RIGHT_PAREN -> ^(REXP datatype logicalExpression);