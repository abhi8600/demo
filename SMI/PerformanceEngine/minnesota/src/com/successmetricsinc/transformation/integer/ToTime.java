package com.successmetricsinc.transformation.integer;

import java.util.ArrayList;
import java.util.IllegalFormatException;
import java.util.List;
import java.util.StringTokenizer;

import org.antlr.runtime.tree.Tree;
import org.apache.log4j.Logger;
import org.joda.time.Duration;
import org.joda.time.DurationFieldType;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.chrono.ISOChronology;

import com.successmetricsinc.transformation.Expression;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.Statement;
import com.successmetricsinc.transformation.TransformationScript;

public class ToTime extends Operator{
	private Operator millisecondValue;
	private PeriodType periodType;
	private static String pattern;
	private TransformationScript script;
	private Tree t;
	private static final Logger logger = Logger.getLogger(ToTime.class);
	public static final String TO_TIME_FORMAT_PREFIX = "TO_TIME_FORMAT";

	public ToTime(TransformationScript script, Tree t) throws ScriptException {
		super(script, t);
		this.script = script;
		this.t = t;
		Expression ex = new Expression(script);
		millisecondValue = ex.compile(t.getChild(0));
		if (millisecondValue.getType() != Operator.Type.Integer)
		{
			throw new ScriptException("Millisecond value must be of type integer: " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
		
		ex = new Expression(script);
		Operator periodFieldList = ex.compile(t.getChild(1));
		if (periodFieldList.getType() != Operator.Type.Varchar)
		{
			throw new ScriptException("Period field list must be of String type: " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
		
		Object o = periodFieldList.evaluate();
		if (o == null)
			throw new ScriptException("Period field list must not be null: " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(),
					t.getCharPositionInLine() + t.getText().length());
		try
		{
			periodType = getPeriodType(o.toString());
		}
		catch (IllegalArgumentException ex2)
		{
			throw new ScriptException("Invalid period field list: " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
		
		ex = new Expression(script);
		Operator pat = ex.compile(t.getChild(2));
		if (pat.getType() != Operator.Type.Varchar)
		{
			throw new ScriptException("Format must be of String type: " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}		
		
		o = pat.evaluate();
		if (o == null)
			throw new ScriptException("Format must not be null: " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(),
					t.getCharPositionInLine() + t.getText().length());
		pattern = o.toString();
		try
		{
			String.format(pattern, (Object[] ) null);
		}
		catch (IllegalFormatException ex1)
		{
			throw new ScriptException("Invalid format specification: " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(),
					t.getCharPositionInLine() + t.getText().length());
		}
	}
	
	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		return evaluate().toString().compareTo(b.evaluate().toString());
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		return evaluate().toString().equals(b.evaluate().toString());
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		long ms = (Long) millisecondValue.evaluate();
		Duration dur = new Duration(ms);
		Period per = dur.toPeriod(periodType, ISOChronology.getInstanceUTC());
		return formatPeriod(per);
	}
	
	public static String evaluate(long milliseconds,String fields, String format)
	{
		Duration dur = new Duration(milliseconds);
		PeriodType pType = getPeriodType(fields);
		pattern = format;
		Period per = dur.toPeriod(pType, ISOChronology.getInstanceUTC());
		return formatPeriod(per);
	}
	
	public static String evaluate(double milliseconds,String fields, String format)
	{
		/*Duration dur = new Duration(milliseconds);
		PeriodType pType = getPeriodType(fields);
		pattern = format;
		Period per = dur.toPeriod(pType, ISOChronology.getInstanceUTC());
		return formatPeriod(per);*/
		return evaluate(new Double(milliseconds).longValue(), fields, format);
	}
	
	public static void validateColumnFormat(String columnFormat) throws Exception
	{
		try
		{
			String formatArgs[]=(columnFormat.substring(columnFormat.indexOf("(") + 1, columnFormat.lastIndexOf(")"))).split("','");
			String toTimeFields = formatArgs[0].substring(formatArgs[0].indexOf("'") + 1);
			String toTimeFormat = formatArgs[1].substring(0, formatArgs[1].indexOf("'"));
			if (toTimeFields==null || toTimeFields.length()<=0 || toTimeFormat==null || toTimeFormat.length()<=0)
				throw new Exception();
		}
		catch (Exception e1)
		{
			logger.error("Invalid TO_TIME_FORMAT for column "+columnFormat);
			throw new Exception("Invalid TO_TIME_FORMAT for column "+columnFormat);
		}
	}

	@Override
	public int getSize()
	{
		try
		{
			return evaluate().toString().length();
		} catch (ScriptException e)
		{
			return 0;
		}
	}

	@Override
	public Type getType()
	{
		return Operator.Type.Varchar;
	}

	@Override
	public Operator clone() {
		try
		{
			return new ToTime(script, t);
		} catch (ScriptException e)
		{
			return null;
		}
	}
	
	
	/** 
	 * get a period type based upon the comma separated list of period field names
	 * - years, months, weeks, days, hours, minutes, seconds, milliseconds
	 * @param types
	 * @return
	 * @throws ScriptException 
	 */
	private static PeriodType getPeriodType(String types)
	{
		StringTokenizer tokenizer = new StringTokenizer(types, ",");
		List<DurationFieldType> lst = new ArrayList<DurationFieldType>();
		while (tokenizer.hasMoreTokens())
		{
			String val = tokenizer.nextToken();
			if (val != null)
			{
				val = val.trim();
				DurationFieldType ty = getDurationFieldTypeFromName(val);
				if (ty != null)
					lst.add(ty);
			}
		}
		DurationFieldType[] fields = new DurationFieldType[lst.size()];
		lst.toArray(fields);
		return PeriodType.forFields(fields);
	}
	
	/**
	 * return the duration field type associated with the string name
	 * @param val
	 * @return
	 * @throws ScriptException 
	 */
	private static DurationFieldType getDurationFieldTypeFromName(String val)
	{
		if (val.equals("years"))
			return DurationFieldType.years();
		else if (val.equals("months"))
			return DurationFieldType.months();
		else if (val.equals("weeks"))
			return DurationFieldType.weeks();
		else if (val.equals("days"))
			return DurationFieldType.days();
		else if (val.equals("hours"))
			return DurationFieldType.hours();
		else if (val.equals("minutes"))
			return DurationFieldType.minutes();
		else if (val.equals("seconds"))
			return DurationFieldType.seconds();
		else if (val.equals("milliseconds") || val.equals("millis"))
			return DurationFieldType.millis();
		throw new IllegalArgumentException(val);
	}
	
	/**
	 * build an object array of the period field values
	 * @param period
	 * @return
	 */
	private static Integer[] getDurationFieldTypeValues(Period period)
	{
		List<Integer> vals = new ArrayList<Integer>();
		DurationFieldType[] fields = period.getFieldTypes();
		for (int i = 0; i < fields.length; i++)
		{
			vals.add(period.get(fields[i]));
		}
		Integer[] res = new Integer[vals.size()];
		vals.toArray(res);
		return res;
	}
	
	/**
	 * format the period
	 * @param pattern
	 */
	private static String formatPeriod(Period per)
	{
		Object[] args = getDurationFieldTypeValues(per);
		return String.format(pattern, args);
	}
}
