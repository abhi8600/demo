/**
 * 
 */
package com.successmetricsinc.transformation;

/**
 * @author bpeters
 * 
 */
public class ExitWhile extends Statement
{
	@Override
	public void execute() throws ScriptException
	{
		ScriptException se = new ScriptException();
		se.setExitWhile(true);
		throw se;
	}
}
