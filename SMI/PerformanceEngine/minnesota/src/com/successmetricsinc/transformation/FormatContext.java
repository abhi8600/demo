/**
 * 
 */
package com.successmetricsinc.transformation;

import org.antlr.runtime.CommonTokenStream;

/**
 * @author bpeters
 * 
 */
public class FormatContext
{
	public int lastToken;
	public int lastType = -1;
	public CommonTokenStream cts;
}
