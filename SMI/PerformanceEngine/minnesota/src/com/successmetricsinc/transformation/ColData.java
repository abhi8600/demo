/**
 * 
 */
package com.successmetricsinc.transformation;

public class ColData
{
	public static int SORT_NONE = 0;
	public static int SORT_ASCENDING = 1;
	public static int SORT_DESCENDING = 2;
	public String tname;
	public String logicalColumnName;
	public String physicalColumnName;
	public String datatype;
	public int width;
	public boolean unqualified;
	public String constantValue;
	public int sortDirection;
}