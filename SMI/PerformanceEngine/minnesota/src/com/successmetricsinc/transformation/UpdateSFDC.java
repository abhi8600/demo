/**
 * 
 */
package com.successmetricsinc.transformation;

import java.util.Map;

import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.sfdc.SFDCUpdate;
import com.successmetricsinc.transformation.DataType.DType;

public class UpdateSFDC extends Statement
{
	private VariableDataElement vde;

	public UpdateSFDC(TransformationScript script, Tree t) throws ScriptException
	{
		if (t.getChildCount() != 1)
		{
			throw new ScriptException("UpdateSFDC takes 1 argument, a MAP: " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
		this.script = script;
		String vname = t.getChild(0).getText();
		DataElement de = script.findElement(vname);
		if (de == null)
			throw new ScriptException("Unknown variable " + vname + ": " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(), t
					.getCharPositionInLine()
					+ t.getText().length());
		if (!VariableDataElement.class.isInstance(de) || ((VariableDataElement) de).getType().type != DType.Map)
			throw new ScriptException("Invalid data element " + vname + ": " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(), t
					.getCharPositionInLine()
					+ t.getText().length());
		vde = (VariableDataElement) de;
		
		script.setSFDC(new SFDCUpdate(t, script.getRepository()));
	}

	@SuppressWarnings("unchecked")
	@Override
	public void execute() throws ScriptException
	{
		Map<Object, Object> omap = (Map<Object, Object>) vde.getValue(null);
		script.getSFDC().add(omap);
	}
}
