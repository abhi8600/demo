package com.successmetricsinc.transformation;

public class NoFilterValueException extends ScriptException
{	
	private static final long serialVersionUID = 1L;

	public NoFilterValueException()
	{		
		super("NoFilterValueException", -1, -1, -1);
	}
	
	public NoFilterValueException(String reason)
	{		
		super(reason, -1, -1, -1);
	}
}
