package com.successmetricsinc.transformation;

import java.util.List;

public class SourceQuery
{
	public boolean hasAgg;
	public String projectionList;
	public String table;
	public String physicalTableName;
	public String physicalTableSchema;
	public String physicalTableConnection;
	public String groupByClause;
	public String whereClause;
	public boolean isTime;
	public List<Object> tables;
	public List<String> sourceTableNames;
	public boolean distinct;
	public int topN = -1;
	public List<DataType> dataTypes;
}