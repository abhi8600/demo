package com.successmetricsinc.transformation;

import org.antlr.runtime.tree.Tree;

public class Return extends Statement
{
	private Operator op;

	public Return(TransformationScript script, Tree tree) throws ScriptException
	{
		this.script = script;
		Expression ex = new Expression(script);
		op = ex.compile(tree.getChild(0));
	}

	@Override
	public void execute() throws ScriptException
	{
		script.setFunctionResult(op.evaluate());
		ScriptException se = new ScriptException();
		se.setFunctionReturn(true);
		throw se;
	}
}
