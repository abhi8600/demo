/**
 * 
 */
package com.successmetricsinc.transformation;

import java.io.Serializable;

import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.util.RepositoryException;

/**
 * @author bpeters
 * 
 */
public class ScriptDefinition implements Serializable
{

	private static final long serialVersionUID = 1L;
	public String InputQuery;
	public String Output;
	public String Script;

	public ScriptDefinition()
	{
	}

	public ScriptDefinition(Element e, Namespace ns) throws RepositoryException
	{
		InputQuery = e.getChildTextTrim("InputQuery", ns);
		Output = e.getChildTextTrim("Output", ns);
		Script = e.getChildTextTrim("Script", ns);
	}
	
	public Element getScriptDefinitionElement(Namespace ns)
	{
		Element e = new Element("Script",ns);
		
		Element child = null;
		
		if (InputQuery!=null)
		{
			child = new Element("InputQuery",ns);
			child.setText(InputQuery);
			e.addContent(child);
		}
		
		if (Output!=null)
		{
			child = new Element("Output",ns);
			child.setText(Output);
			e.addContent(child);
		}

		if (Script!=null)
		{
			child = new Element("Script",ns);
			child.setText(Script);
			e.addContent(child);
		}
				
		return e;
	}

	public String getFullScript()
	{
		return ("INPUT: " + InputQuery + " OUTPUT: " + Output + " BEGIN " + Script + " END");
	}
}
