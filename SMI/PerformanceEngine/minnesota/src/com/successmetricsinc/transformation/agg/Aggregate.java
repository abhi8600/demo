/**
 * 
 */
package com.successmetricsinc.transformation.agg;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.antlr.runtime.tree.Tree;
import org.apache.log4j.Logger;

import com.successmetricsinc.query.DisplayExpression;
import com.successmetricsinc.transformation.BirstScriptParser;
import com.successmetricsinc.transformation.DataType;
import com.successmetricsinc.transformation.Expression;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.Statement;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.transformation.DataType.DType;
import com.successmetricsinc.transformation.object.SetLookup;

/**
 * @author bpeters
 * 
 */
public class Aggregate extends Operator
{
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(SetLookup.class);
	private Tree t;
	private TransformationScript script;
	private int iteration = -1;
	private boolean sorted;
	private Operator value;
	private Operator rsumCount;
	@SuppressWarnings("rawtypes")
	private Map<Object, List<Comparable>> items = null;
	@SuppressWarnings("rawtypes")
	private Map<Object, Set<Comparable>> ditems = null;
	private List<Operator> bylist = new ArrayList<Operator>();
	private boolean isRank;

	public Aggregate(TransformationScript script, Tree t) throws ScriptException
	{
		super(script, t);
		this.t = t;
		this.script = script;
		Expression ex = null;
		int byindex = 1;
		int valueindex = 0;
		if (t.getType() == BirstScriptParser.RSUM)
		{
			byindex = 2;
			valueindex = 1;
			ex = new Expression(script);
			rsumCount = ex.compile(t.getChild(0));
			if (rsumCount.getType() != Operator.Type.Integer)
				throw new ScriptException("Rolling sum must specify an integer number of items to sum: " + Statement.toString(t.getChild(0), null),
						t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
		ex = new Expression(script);
		value = ex.compile(t.getChild(valueindex));
		if (t.getType() == BirstScriptParser.RSUM)
		{
			if (value.getType() != Operator.Type.Integer && value.getType() != Operator.Type.Float)
				throw new ScriptException("Rolling sum can only apply to integer and float values: " + Statement.toString(t.getChild(valueindex), null),
						t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
		if (t.getChildCount() > byindex)
		{
			for (int i = 0; i < t.getChild(byindex).getChildCount(); i++)
			{
				ex = new Expression(script);
				bylist.add(ex.compile(t.getChild(byindex).getChild(i)));
			}
		}
		isRank = t.getType() == BirstScriptParser.RANK || t.getType() == BirstScriptParser.DRANK || t.getType() == BirstScriptParser.PTILE
				|| t.getType() == BirstScriptParser.STATMEDIAN;
	}

	@Override
	public Operator clone()
	{
		try
		{
			return new Aggregate(script, t);
		} catch (ScriptException e)
		{
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.successmetricsinc.transformation.Operator#compareTo(com.successmetricsinc.transformation.Operator)
	 */
	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		try
		{
			Object aval = evaluate();
			Object bval = b.evaluate();
			if (aval.getClass() != bval.getClass())
				throw new ScriptException("Bad data type: ", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
			if (Integer.class.isInstance(aval))
				return ((Integer) aval).compareTo((Integer) bval);
			else if (Double.class.isInstance(aval))
				return ((Double) aval).compareTo((Double) bval);
			else if (Long.class.isInstance(aval))
				return ((Long) aval).compareTo((Long) bval);
			else if (Boolean.class.isInstance(aval))
				return ((Boolean) aval).compareTo((Boolean) bval);
			else if (String.class.isInstance(aval))
				return ((String) aval).compareTo((String) bval);
			else if (Date.class.isInstance(aval))
				return ((Date) aval).compareTo((Date) bval);
			else if (Calendar.class.isInstance(aval))
				return ((Calendar) aval).compareTo((Calendar) bval);
			return 0;
		} catch (Exception e)
		{
			if (e instanceof ScriptException)
				throw (ScriptException) e;
			throw new ScriptException("Bad data type: ", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.successmetricsinc.transformation.Operator#equals(com.successmetricsinc.transformation.Operator)
	 */
	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		try
		{
			Object aval = evaluate();
			Object bval = b.evaluate();
			if (aval.getClass() != bval.getClass())
				throw new ScriptException("Bad data type: ", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
			if (Integer.class.isInstance(aval))
				return ((Integer) aval).equals((Integer) bval);
			else if (Double.class.isInstance(aval))
				return ((Double) aval).equals((Double) bval);
			else if (Long.class.isInstance(aval))
				return ((Long) aval).equals((Long) bval);
			else if (Boolean.class.isInstance(aval))
				return ((Boolean) aval).equals((Boolean) bval);
			else if (String.class.isInstance(aval))
				return ((String) aval).equals((String) bval);
			else if (Date.class.isInstance(aval))
				return ((Date) aval).equals((Date) bval);
			else if (Calendar.class.isInstance(aval))
				return ((Calendar) aval).equals((Calendar) bval);
			return false;
		} catch (Exception e)
		{
			throw new ScriptException("Bad data type", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.successmetricsinc.transformation.Operator#evaluate()
	 */
	@SuppressWarnings(
	{ "unchecked", "rawtypes" })
	@Override
	public Object evaluate() throws ScriptException
	{
		DisplayExpression de = script.getDisplayExpression();
		if (de != null && de.resetOperatorItems)
		{
			items = null;
		}
		if (items == null)
		{
			items = new HashMap<Object, List<Comparable>>();
		}
		if (ditems == null)
		{
			ditems = new HashMap<Object, Set<Comparable>>();
		}
		Object o = value.evaluate();
		int count = 0;
		if (rsumCount != null)
		{
			Object evalOb = rsumCount.evaluate();
			if (evalOb instanceof Integer)
			{
				count = (Integer) evalOb;
			} else if (evalOb instanceof Long)
			{
				count = ((Long) evalOb).intValue();
			}
		}
		StringBuilder sb = new StringBuilder();
		for (Operator op : bylist)
		{
			if (sb.length() > 0)
				sb.append('|');
			sb.append(op.evaluate().toString());
		}
		String key = sb.toString();
		if (iteration >= 0 && script.getIteration() > iteration)
		{
			if (isRank && !sorted)
			{
				for (List<Comparable> l : items.values())
				{
					Collections.sort(l);
				}
				sorted = true;
			}
			if (t.getType() == BirstScriptParser.RANK)
			{
				if (o == null)
					return Long.MAX_VALUE;
				List<Comparable> l = items.get(key);
				if (l == null)
					return Long.MAX_VALUE;
				List<Comparable> nl = new ArrayList<Comparable>(l);
				Collections.reverse(nl); // rank is a different order than ptile
				long rank = nl.indexOf((Comparable) o);
				if (rank < 0)
					return Long.MAX_VALUE;
				return rank + 1; // rank is 1-based
			} else if (t.getType() == BirstScriptParser.DRANK)
			{
				if (o == null)
					return Long.MAX_VALUE;
				Set<Comparable> s = ditems.get(key);
				if (s == null)
					return Long.MAX_VALUE;
				int cnt = 0;
				for (Comparable item : s)
				{
					if (item.compareTo((Comparable) o) == 0)
					{
						return Long.valueOf(((Integer)(s.size() - cnt)).toString());
					}
					cnt++;
				}
				return Long.MAX_VALUE;
			} else if (t.getType() == BirstScriptParser.PTILE)
			{
				List<Comparable> l = items.get(key);
				if (l == null || l.isEmpty())
					return 0.0;
				int result = l.indexOf((Comparable) o);
				return ((double) result) / ((double) l.size());
			} else if (t.getType() == BirstScriptParser.STATMEDIAN)
			{
				List<Comparable> l = items.get(key);
				if (l == null || l.isEmpty())
					return 0.0;
				if (l.size() % 2 == 1)
				{
					Object mid = l.get(l.size() / 2);
					if (Integer.class.isInstance(mid))
						return ((Integer) mid).doubleValue();
					else if (Long.class.isInstance(mid))
						return ((Long) mid).doubleValue();
					else if (Double.class.isInstance(mid))
						return (Double) mid;
					return 0.0;
				} else
				{
					Object mid1 = l.get(l.size() / 2);
					Object mid2 = l.get((l.size() / 2) - 1);
					double v1 = 0;
					double v2 = 0;
					if (Integer.class.isInstance(mid1))
						v1 = ((Integer) mid1).doubleValue();
					else if (Double.class.isInstance(mid1))
						v1 = (Double) mid1;
					else if (Long.class.isInstance(mid1))
						v1 = (Long) mid1;
					if (Integer.class.isInstance(mid2))
						v2 = ((Integer) mid2).doubleValue();
					else if (Double.class.isInstance(mid2))
						v2 = (Double) mid2;
					else if (Long.class.isInstance(mid2))
						v2 = (Long) mid2;
					return (v1 + v2) / 2;
				}
			} else if (t.getType() == BirstScriptParser.RSUM)
			{
				List<Comparable> l = items.get(key);
				if (l == null)
				{
					l = new ArrayList<Comparable>();
					items.put(key, l);
				}
				l.add((Comparable) o);
				if (l.size() > count)
					l.remove(0);
				if (value.getType() == Type.Integer)
				{
					long iresult = 0;
					if (l.get(0) instanceof Long)
						for (int i = 0; i < l.size(); i++)
							iresult += (Long) l.get(i);
					else
						for (int i = 0; i < l.size(); i++)
							iresult += (Integer) l.get(i);
					return iresult;
				} else
				{
					double dresult = 0;
					for (int i = 0; i < l.size(); i++)
						dresult += (Double) l.get(i);
					return dresult;
				}
			}
		} else if (!script.isIncomplete())
		{
			iteration = script.getIteration();
			script.setIncomplete(true);
			script.resetDefinitions();
			if (isRank && o != null)
			{
				List<Comparable> l = items.get(key);
				if (l == null)
				{
					l = new ArrayList<Comparable>();
					items.put(key, l);
				}
				l.add((Comparable) o);
				if (t.getType() == BirstScriptParser.DRANK)
				{
					Set<Comparable> s = ditems.get(key);
					if (s == null)
					{
						s = new TreeSet<Comparable>();
						ditems.put(key, s);
					}
					s.add((Comparable) o);
				}
			}
		}
		return -1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.successmetricsinc.transformation.Operator#getSize()
	 */
	@Override
	public int getSize()
	{
		if (t.getType() == BirstScriptParser.RANK)
			return Integer.SIZE / 8;
		if (t.getType() == BirstScriptParser.DRANK)
			return Integer.SIZE / 8;
		if (t.getType() == BirstScriptParser.PTILE)
			return Double.SIZE / 8;
		if (t.getType() == BirstScriptParser.STATMEDIAN)
			return Double.SIZE / 8;
		return Integer.SIZE / 8;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.successmetricsinc.transformation.Operator#getType()
	 */
	@Override
	public Type getType()
	{
		if (t.getType() == BirstScriptParser.RANK)
			return Operator.Type.Integer;
		if (t.getType() == BirstScriptParser.PTILE)
			return Operator.Type.Float;
		if (t.getType() == BirstScriptParser.STATMEDIAN)
			return Operator.Type.Float;
		if (t.getType() == BirstScriptParser.RSUM)
			return value.getType();
		return Operator.Type.Integer;
	}

	public static DataType getType(TransformationScript script, Tree t) throws ScriptException
	{
		if (t.getType() == BirstScriptParser.RANK)
			return new DataType(DType.Integer, 0);
		if (t.getType() == BirstScriptParser.PTILE)
			return new DataType(DType.Float, 0);
		if (t.getType() == BirstScriptParser.STATMEDIAN)
			return new DataType(DType.Float, 0);
		if (t.getType() == BirstScriptParser.RSUM)
		{
			Expression ex = new Expression(script);
			Operator op = ex.compile(t.getChild(1));
			if (op.getType() == Type.Integer)
				return new DataType(DType.Integer, 0);
			return new DataType(DType.Float, 0);
		}
		return new DataType(DType.Integer, 0);
	}
}
