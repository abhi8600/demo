/**
 * 
 */
package com.successmetricsinc.transformation;

import java.util.List;
import java.util.Map;

import com.successmetricsinc.transformation.DataType.DType;

/**
 * @author bpeters
 * 
 */
public class VariableDataElement implements DataElement
{
	public String name;
	public DataType type;
	private Object value;

	@Override
	public boolean canGet()
	{
		return true;
	}

	@Override
	public boolean canSet()
	{
		return true;
	}

	@Override
	public String getName()
	{
		return name;
	}

	@Override
	public DataType getType()
	{
		return type;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object getValue(Operator lookup) throws ScriptException
	{
		if (lookup != null && value != null)
		{
			Object o = lookup.evaluate();
			if (type.type == DType.List)
			{
				if (o == null)
				{
					throw new ScriptException("Null value for list index: " + name, -1, -1, -1);
				}
				int i = -1;
				if (o instanceof Integer)
				{
					i = (Integer) o;
				}
				else if (o instanceof Long)
				{
					i = ((Long) o).intValue();				
				}
				List<Object> list = (List<Object>) value;
				try
				{
					return list.get(i);
				} catch (IndexOutOfBoundsException ex)
				{
					throw new ScriptException("Array/List index out of bounds (" + name + "): " + i, -1, -1, -1);
				}
			} else if (type.type == DType.Map)
			{
				Map<Object, Object> map = (Map<Object, Object>) value;
				return map.get(o);
			}
		}
		return value;
	}

	@Override
	public void setValue(Object o)
	{
		value = o;
	}
	
	private static final int MAX_DISPLAY_LIMIT = 20;
	
	@SuppressWarnings("unchecked")
	public String toString()
	{
		int counter = 0;
		StringBuilder sb = new StringBuilder(name);
		sb.append(" AS ").append(type.toString()).append(" = ");
		if (value == null)
		{
			sb.append("null");
		} else if (type.type == DType.List)
		{
			List<Object> list = (List<Object>) value;
		    sb.append('(');
		    String sep = "";
		    for (Object object : list) {
		    	if (counter++ > MAX_DISPLAY_LIMIT)
		    	{
		    		sb.append(sep).append("...");
		    		break;
		    	}
		    	sb.append(sep).append(object.toString());
		    	sep = ",";
		    }
		    sb.append(')');
		} else if (type.type == DType.Map)
		{
			Map<Object, Object> map = (Map<Object, Object>) value;
	        sb.append('[');
	        String sep = "";
	        for (Object object : map.keySet()) {
		    	if (counter++ > MAX_DISPLAY_LIMIT)
		    	{
		    		sb.append(sep).append("...");
		    		break;
		    	}
	            sb.append(sep).append(object.toString()).append("->").append(map.get(object).toString());
	            sep = ",";
	        }
	        sb.append(']');
		}
		else
		{
			sb.append(value.toString());
		}
		return sb.toString();
	}
}
