package com.successmetricsinc.transformation.operators;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.successmetricsinc.transformation.BirstScriptParser;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;

public class OpUnaryFloat extends Operator
{
	private static Logger logger = Logger.getLogger(OpUnaryFloat.class);
	private Operator a;
	private int type;

	public OpUnaryFloat(Operator a, int type)
	{
		super(null, null);
		this.a = a;
		this.type = type;
	}

	@Override
	public Operator clone()
	{
		return new OpUnaryFloat(a, type);
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		if (a == null)
		{
			if (type == BirstScriptParser.FLOATTYPE)
				return (double) 0.0;
			throw new ScriptException("Cannot perform float operation on a null value", line, -1, -1);
		}
		Object ea = a.evaluate();
		if (ea == null)
		{
			if (type == BirstScriptParser.FLOATTYPE)
				return (double) 0.0;
			throw new ScriptException("Cannot perform float operation on a null value", line, -1, -1);
		}
		switch (type)
		{
		case BirstScriptParser.NEGATE:
			return -((Number) ea).doubleValue();
		case BirstScriptParser.FLOATTYPE:
			if (ea instanceof List) // return value from GETPROMPTVALUE with multiple select (we CAST the result for
									// type safety)
			{
				@SuppressWarnings("unchecked")
				List<String> eaLst = (List<String>) ea;
				List<Double> retLst = new ArrayList<Double>(eaLst.size());
				for (int i = 0; i < eaLst.size(); i++)
				{
					Double d = 0.0;
					Object obj = eaLst.get(i);
					try
					{
						d = Double.valueOf(obj.toString());
					} catch (NumberFormatException e)
					{
						logger.debug("OpUnaryFloat: failed to evaluate the argument for the FLOAT cast, returning 0.0: " + obj, e);
					} catch (NullPointerException e)
					{
						logger.debug("OpUnaryFloat: failed to evaluate the argument for the FLOAT cast, returning 0.0: " + obj, e);
					}
					retLst.add(d);
				}
				return retLst;
			}
			try
			{
				return Double.valueOf(ea.toString());
			} catch (NumberFormatException e)
			{
				throw new ScriptException("Failed to convert to a FLOAT value: " + ea.toString(), -1, -1, -1);
			}
		default:
			throw new ScriptException("Bad type (" + type + ") for FLOAT operation", -1, -1, 1);
		}
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		return (new OpFloat((Double) evaluate())).compareTo(b);
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		return (new OpFloat((Double) evaluate())).equals(b);
	}

	@Override
	public Type getType()
	{
		return Operator.Type.Float;
	}

	@Override
	public int getSize()
	{
		return Double.SIZE / 8;
	}
}
