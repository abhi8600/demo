package com.successmetricsinc.transformation.operators;

import com.successmetricsinc.transformation.ScriptException;

public class InvalidOperandException extends ScriptException
{
	private static final long serialVersionUID = 1L;
	
	public InvalidOperandException()
	{
		super();
	}

	public InvalidOperandException(String message, int line, int start, int end)
	{
		super(message, line, start, end);
	}
}
