package com.successmetricsinc.transformation.operators;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.Variable;
import com.successmetricsinc.query.Filter;
import com.successmetricsinc.query.QueryString;
import com.successmetricsinc.query.ResultSetCache;
import com.successmetricsinc.transformation.BirstScriptParser;
import com.successmetricsinc.transformation.NoFilterValueException;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.TransformationScript;

public class OpBinaryVarchar extends Operator
{
	private static Logger logger = Logger.getLogger(OpBinaryVarchar.class);
	private Operator a;
	private Operator b;
	private int type;
	private Repository r;
	private Object varObject;
	private TransformationScript script;
	private static final String ILLEGAL_CHARACTERS =  "\\\\Q[&=]{1,}\\\\E";
	private static final String legal = "\\\\Q[\\[\\]\\^\\$\\|\\?\\*\\+\\(\\)\\~`\\!@#%_+{}'\"<>;,]{1,}\\\\E";
	private static final String dateRegex = "\\\\Q([0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9])\\\\E";


	public OpBinaryVarchar(Operator a, Operator b, int type, TransformationScript script)
	{
		super(script, null);
		this.a = a;
		this.b = b;
		this.type = type;
		if (script != null)
		{
			this.r = script.getRepository();
			this.script = script;
		}
	}

	@Override
	public Operator clone()
	{
		Operator cloneda = null;
		Operator clonedb = null;
		if(a != null)
		{
			cloneda = (Operator) a.clone();
		}
		if(b != null)
		{
			clonedb = (Operator) b.clone();
		}
		OpBinaryVarchar op = new OpBinaryVarchar(cloneda, clonedb, type, script);
		op.r = r;
		op.varObject = varObject;
		return op;
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		switch (type)
		{
		case BirstScriptParser.PLUS:
			Object ao = a.evaluate();
			Object bo = b.evaluate();
			return (ao == null ? "" : ao.toString()) + (bo == null ? "" : bo.toString());
		case BirstScriptParser.GETVARIABLE:
			if (a == null)
				throw new ScriptException("Invalid null variable", line, -1, -1);
			if (varObject != null)
				return varObject;
			ao = a.evaluate();
			if (ao == null)
				return "";
			String vname = ao.toString();
			Variable v = r.findVariable(vname);
			String uname = null;
			// All the variables should be in the repository except USER variable which
			// is created for each logged in user.
			if (v == null && !vname.equalsIgnoreCase("user"))
			{
				throw new ScriptException("Unknown variable: " + vname, line, -1, -1);
			}
			Session session = null;
			if(v!=null && v.getType() == Variable.VariableType.Repository)
			{
				varObject = r.replaceVariables(null, "V{" + vname + "}");
			}
			else
			{
				if (b != null)
				{
					uname = b.evaluate().toString();
				}
				boolean cancelSessionAfterOperation = false;
				try
				{
					if (script.getSession() == null)
					{
						session = new Session(uname);
						cancelSessionAfterOperation = true;
						ResultSetCache rsc = script.getResultSetCache();
						if (rsc == null)
						{
							rsc = ResultSetCache.getInstance(r.getApplicationIdentifier(), false);
							script.setResultSetCache(rsc);
						}
						session.fillSessionVariables(r, rsc);
					} else
					{
						session = script.getSession();
					}
				} catch (Exception ex)
				{
					throw new ScriptException("Unable to retreive variable: " + v.getName(), line, -1, -1);
				} finally
				{
					if (cancelSessionAfterOperation && session != null)
					{
						session.setCancelled(true);
					}
				}
				varObject = r.replaceVariables(session, "V{" + vname + "}");
			}
			return varObject;
		case BirstScriptParser.GETPROMPTVALUE:
			Object key = a.evaluate();			
			if (key != null)			{
				
				if (script == null)
				{
					if (b != null)
						return b.evaluate();
					logger.trace("Attempt to call GETPROMPTVALUE(" + key.toString() + ") without a valid script object and no default value");
					throw new NoFilterValueException("No valid script object GETPROMPTVALUE(" + key.toString() + ")");
				}
				session = script.getSession();
				if (session == null)
				{
					if (b != null)
						return b.evaluate();
					logger.trace("Attempt to call GETPROMPTVALUE(" + key.toString() + ") without a valid session object and no default value");
					throw new NoFilterValueException("No valid session object for GETPROMPTVALUE(" + key.toString() + ")");
				}
				List<Filter> prompts = session.getParams();					
				if (prompts != null && !prompts.isEmpty())
				{					
					Filter f = session.getFilter(key.toString());
					if (f != null)
					{
						List<String> list = f.getValues();
						if (list != null)
						{
							if (list.size() <= 1)
							{
								if (list.isEmpty() || QueryString.NO_FILTER_TEXT.equals(list.get(0)))
								{
									if (b != null)
										return b.evaluate();
									throw new NoFilterValueException("No prompt value for: " + key.toString()); // if no
																												// prompt
																												// value,
																												// throw
																												// a
																												// special
																												// exception
																												// that
																												// is
																												// caught
																												// at
																												// the
																												// point
																												// where
																												// you
																												// add
																												// the
																												// query
																												// filter
								}
								return list.get(0);
							} else
								return list; // LogicalQueryString:validateTreeFilter handles this
						} else
						{
							logger.trace("Filter.getValues returned null for: " + key.toString());
						}
					} else
					{
						logger.trace("No prompt in map (via session.getFilter) for key: " + key.toString());
					}
				} else
				{					
					String promptsForSSO;
					String separator;
					
					if(session.getDashboardParams() !=null )
					{	
						promptsForSSO = session.getDashboardParams();						
					
						if(session.getDashboardParamsSeperator() != null)
						{
							separator = session.getDashboardParamsSeperator();
							if (separator.length() != separator.replaceAll(
									ILLEGAL_CHARACTERS.substring(3, ILLEGAL_CHARACTERS.length() - 3), "").length()) {
									logger.error("Illegal Separator: " + separator);
									return null;							
							}						
						}
						else
						{							
							separator = ";";					
						}				
						List<String> list = getrDashboardParamsPromptsList(promptsForSSO, separator, key);								
						if(list != null)
						{							
							if(list.size() <= 1)
							{
								if(list.isEmpty())
								{
									throw new NoFilterValueException("No prompt value for: " + key.toString()); // if no
									// prompt
									// value,
									// throw
									// a
									// special
									// exception
									// that
									// is
									// caught
									// at
									// the
									// point
									// where
									// you
									// add
									// the
									// query
									// filter								
								}
								return list.get(0);								
							}
							else
								return list;		
						}
						else
						{
							logger.trace("DashBoardParams are not valid");
						}
					}
				}
				if (b != null)
					return b.evaluate();
				if (prompts == null)
				{
					throw new NoFilterValueException("No prompts in the session: " + key.toString()); // if no prompts
																										// or prompt
																										// value, throw
																										// a special
																										// exception
																										// that is
																										// caught at the
																										// point where
																										// you add the
																										// query filter
				} else
				{
					throw new NoFilterValueException("No prompt in the session with this name: " + key.toString()); // if
																													// no
																													// prompts
																													// or
																													// prompt
																													// value,
																													// throw
																													// a
																													// special
																													// exception
																													// that
																													// is
																													// caught
																													// at
																													// the
																													// point
																													// where
																													// you
																													// add
																													// the
																													// query
																													// filter
				}
			} else
			{
				logger.trace("Attempt to call GETPROMPTVALUE with a null key argument");
			}
			break;
		}
		return null;
	}	
	public static List<String> getrDashboardParamsPromptsList(String promptsForSSO, String separator, Object key)
	{
		List<String> list = null ;		
		Map<Integer,Object> dict = new HashMap<Integer, Object>();
		int count = 0;
		String separatorLiteral = "\\\\Q[" + separator + "]{1,}\\\\E";	
		
			char match = '=';
			char[] charArray = promptsForSSO.toCharArray();
			for(int i=0; i<charArray.length; i++)
			{
				if(charArray[i] == match)
				{
					count++;
				}
			}
			
		if(count > 2)
		{
			if (promptsForSSO.length() == promptsForSSO.replaceAll(
					separatorLiteral.substring(3, separatorLiteral.length() - 3), "").length()) {											
					return null;
			}
			
		}
		Pattern p1 = Pattern.compile(separatorLiteral.substring(3,
				separatorLiteral.length() - 3));				
		String[] result = p1.split(promptsForSSO);	
		List<String> arrays = Arrays.asList(result);	
		list = new ArrayList<String>();		
		for (int i = 0; i < result.length; i++) {
			dict.put(i, result[i]);
		}

		for (int j = 0; j < dict.size(); j++) {
			String s = (String) dict.get(j);
			String[] split = ((String) s).split("=");
			if (split[0].equalsIgnoreCase((String) key))
				list.add(split[1]);

		}
		return list;
	}
	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		return (new OpVarchar((String) evaluate())).compareTo(b);
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		return (new OpVarchar((String) evaluate())).equals(b);
	}

	@Override
	public Type getType()
	{
		return Operator.Type.Varchar;
	}

	@Override
	public int getSize()
	{
		switch (type)
		{
		case BirstScriptParser.PLUS:
			return a.getSize() + b.getSize();
		}
		return 0;
	}
}
