package com.successmetricsinc.transformation.operators;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;

import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.util.DateUtil;

public class OpUnaryVarchar extends Operator
{
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(OpUnaryVarchar.class);
	private Operator a;
	private int type;
	private TransformationScript script;

	public OpUnaryVarchar(Operator a, int type, TransformationScript script)
	{
		super(null, null);
		this.a = a;
		this.type = type;
		this.script = script;
	}

	@Override
	public Operator clone()
	{
		Operator cloneda = null;
		if(a != null)
		{
			cloneda = a.clone();
		}
		return new OpUnaryVarchar(cloneda, type, script);
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		if (a == null)
		{
			return null;
		}
		Object ea = a.evaluate();
		if (ea == null)
		{
			return null;
		}
		if (ea instanceof Calendar || ea instanceof Date)
		{
			SimpleDateFormat sdf = new SimpleDateFormat(DateUtil.DB_FORMAT_STRING);
			sdf.setTimeZone(script.getRepository().getServerParameters().getProcessingTimeZone());
			sdf.setLenient(false);
			if (ea instanceof Calendar)
				return sdf.format(((Calendar) ea).getTime());
			else
				return sdf.format((Date) ea);
		}
		return ea.toString();
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		String s = (String) evaluate();
		if (s == null)
			return b == null || b.evaluate() == null ? 0 : -1;
		return (new OpVarchar(s)).compareTo(b);
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		String s = (String) evaluate();
		if (s == null)
			return b.evaluate() == null;
		return (new OpVarchar(s)).equals(b);
	}

	@Override
	public Type getType()
	{
		return Operator.Type.Varchar;
	}

	@Override
	public int getSize()
	{
		return a.getSize();
	}
}
