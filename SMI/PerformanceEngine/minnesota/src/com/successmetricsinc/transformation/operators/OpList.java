package com.successmetricsinc.transformation.operators;

import java.util.ArrayList;
import java.util.List;

import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;

public class OpList extends Operator
{
	private List<Object> list;
	private int size = 0;

	public OpList(List<Operator> lst) throws ScriptException
	{
		super(null, null);
		list = new ArrayList<Object>(lst.size());
		for (int i = 0; i < lst.size(); i++)
		{
			size += lst.get(i).getSize();
			list.add(lst.get(i).evaluate());
		}
	}

	public OpList()
	{
		super(null, null);
	}

	@Override
	public Operator clone()
	{
		OpList op = new OpList();
		op.list = list;
		op.size = size;
		return op;
	}

	public List<Object> getValue()
	{
		return list;
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		return list;
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		throw new ScriptException("compareTo operations not supported for Lists", line, -1, -1);
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		throw new ScriptException("equals operations not supported for Lists", line, -1, -1);
	}

	@Override
	public Type getType()
	{
		return Operator.Type.List;
	}

	@Override
	public int getSize()
	{
		return size;
	}
}
