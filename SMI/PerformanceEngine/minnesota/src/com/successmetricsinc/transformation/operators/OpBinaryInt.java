package com.successmetricsinc.transformation.operators;

import com.successmetricsinc.transformation.BirstScriptParser;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;

public class OpBinaryInt extends Operator
{
	private Operator a;
	private Operator b;
	private int type;

	public OpBinaryInt(Operator a, Operator b, int type)
	{
		super(null, null);
		this.a = a;
		this.b = b;
		this.type = type;
	}

	@Override
	public Operator clone()
	{
		Operator cloneda = null;
		Operator clonedb = null;
		if(a != null)
		{
			cloneda = (Operator) a.clone();
		}
		if(b != null)
		{
			clonedb = (Operator) b.clone();
		}
		return new OpBinaryInt(cloneda, clonedb, type);
	}

	@Override
	public Object evaluate() throws InvalidOperandException, ScriptException
	{
		if (a == null)
			throw new InvalidOperandException("Unable to evaluate integer expression (" + getTypeString() + ") with null value as the first argument", line,
					-1, -1);
		if (b == null)
			throw new InvalidOperandException("Unable to evaluate integer expression (" + getTypeString() + ") with null value as the second argument", line,
					-1, -1);
		Object tempea = a.evaluate();
		if (tempea == null)
			throw new InvalidOperandException("Unable to evaluate integer expression (" + getTypeString() + ") with null value as the first argument", line, -1, -1);
		Object tempeb = b.evaluate();
		if (tempeb == null)
			throw new InvalidOperandException("Unable to evaluate integer expression (" + getTypeString() + ") with null value as the second argument", line, -1, -1);
		
		Object ea = ((tempea instanceof Integer) ? Long.valueOf(String.valueOf(tempea)): tempea);
		Object eb = ((tempeb instanceof Integer) ? Long.valueOf(String.valueOf(tempeb)): tempeb);

		switch (type)
		{
		case BirstScriptParser.PLUS:
			return ((Long) ea) + ((Long) eb);
		case BirstScriptParser.MINUS:
			return ((Long) ea) - ((Long) eb);
		case BirstScriptParser.MULT:
			return ((Long) ea) * ((Long) eb);
		case BirstScriptParser.DIV:
			return ((Long) ea) / ((Long) eb);
		case BirstScriptParser.MOD:
			return ((Long) ea) % ((Long) eb);
		case BirstScriptParser.BITAND:
			return ((Long) ea) & ((Long) eb);
		case BirstScriptParser.BITOR:
			return ((Long) ea) | ((Long) eb);
		}
		return null;
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		Long a = (Long) evaluate();
		if (a == null)
		{
			if (b == null)
				return 0;
			else
				return -1;
		} else if (b == null)
			return 1;
		return (new OpInt(a)).compareTo(b);
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		Long a = (Long) evaluate();
		if (a == null)
		{
			if (b == null)
				return true;
			else
				return false;
		} else if (b == null)
			return false;
		return (new OpInt(a)).equals(b);
	}

	@Override
	public Type getType()
	{
		return Operator.Type.Integer;
	}

	@Override
	public int getSize()
	{
		return Integer.SIZE / 8;
	}

	private String getTypeString()
	{
		switch (type)
		{
		case BirstScriptParser.PLUS:
			return "+";
		case BirstScriptParser.MINUS:
			return "-";
		case BirstScriptParser.MULT:
			return "*";
		case BirstScriptParser.DIV:
			return "/";
		case BirstScriptParser.MOD:
			return "%";
		case BirstScriptParser.BITAND:
			return "&";
		case BirstScriptParser.BITOR:
			return "|";
		}
		return "unknown";
	}
}
