package com.successmetricsinc.transformation.operators;

import com.successmetricsinc.transformation.BirstScriptParser;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;

public class OpUnaryBoolean extends Operator
{
	private Operator a;
	private int type;

	public OpUnaryBoolean(Operator a, int type)
	{
		super(null, null);
		this.a = a;
		this.type = type;
	}

	@Override
	public Operator clone()
	{
		Operator cloneda = null;
		if(a != null)
		{
			cloneda = a.clone();
		}
		return new OpUnaryBoolean(cloneda, type);
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		Object o = null;
		try
		{
			o = a.evaluate();
		} catch (Exception e)
		{
			o = null;
		}
		switch (type)
		{
		case BirstScriptParser.ISNULL:
			return o == null;
		case BirstScriptParser.ISNOTNULL:
			return o != null;
		case BirstScriptParser.ISNAN:
		{
			if (o == null)
				return true;
			if (Double.class.isInstance(o))
				return ((Double) o).isNaN();
				if (Integer.class.isInstance(o) || Long.class.isInstance(o))
				return false;
			throw new ScriptException("only numeric or null values allowed in ISNAN", line, -1, -1);
		}
		case BirstScriptParser.ISINF:
		{
			if (o == null)
				return false;
			if (Double.class.isInstance(o))
				return ((Double) o).isInfinite();
				if (Integer.class.isInstance(o) || Long.class.isInstance(o))
				return false;
			throw new ScriptException("only numeric or null values allowed in ISINF", line, -1, -1);
		}
		}
		return null;
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		return (new OpBoolean((Boolean) evaluate())).compareTo(b);
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		return (new OpBoolean((Boolean) evaluate())).equals(b);
	}

	@Override
	public Type getType()
	{
		return Operator.Type.Boolean;
	}

	@Override
	public int getSize()
	{
		return 1;
	}
}
