package com.successmetricsinc.transformation.operators;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.successmetricsinc.transformation.BirstScriptParser;
import com.successmetricsinc.transformation.NoFilterValueException;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.util.DateUtil;

public class OpUnaryDateTime extends Operator
{
	private Operator a;
	private int type;
	private SimpleDateFormat sdf;
	private static Logger logger = Logger.getLogger(OpUnaryDateTime.class);

	public OpUnaryDateTime(Operator a, int type)
	{
		super(null, null);
		this.a = a;
		this.type = type;
	}

	@Override
	public Operator clone()
	{
		return new OpUnaryDateTime(a, type);
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		switch (type)
		{
		case BirstScriptParser.DATETIMETYPE:
			try
			{
				if (a == null)
				{
					logger.debug("OpUnaryDateTime null argument, returning NOW");
					return new Date();
				}
				Object ea = a.evaluate();
				if (ea == null)
				{
					logger.debug("OpUnaryDateTime null argument, returning NOW");
					return new Date();
				}
				if (Date.class.isInstance(ea))
					return ea;
				if (GregorianCalendar.class.isInstance(ea))
				{
					return ((GregorianCalendar) ea).getTime();
				}
				if (ea instanceof List) // return value from GETPROMPTVALUE with multiple select (we CAST the result for
										// type safety)
				{
					@SuppressWarnings("unchecked")
					List<String> eaLst = (List<String>) ea;
					List<Date> retLst = new ArrayList<Date>(eaLst.size());
					for (int i = 0; i < eaLst.size(); i++)
					{
						retLst.add(convertToDate(eaLst.get(i)));
					}
					return retLst;
				}
				if (Double.class.isInstance(ea))
				{
					return new Date(((Double) ea).longValue());
				}
				return convertToDate(ea);
			} catch (Exception e)
			{
				if (e instanceof NoFilterValueException)
				{
					throw ((NoFilterValueException) e);
				}
				logger.warn("OpUnaryDateTime: evaluate failed, returning NOW", e);
				return new Date();
			}
		}
		return null;
	}
	private Map<Object, Date> cachedDates = null;

	private Date convertToDate(Object ea)
	{
		if (cachedDates == null)
			cachedDates = new HashMap<Object, Date>();
		else
		{
			Date d = cachedDates.get(ea);
			if (d != null)
				return d;
		}
		String s = ea.toString();
		if (sdf != null)
		{
			try
			{
				Date d = sdf.parse(s);
				cachedDates.put(ea, d);
				return d;
			} catch (ParseException ex2)
			{
			} catch (NumberFormatException ex2)
			{
			}
		}
		
		for (SimpleDateFormat sdf : DateUtil.getPossibleDateFormats())
		{
			try
			{
				Date d = sdf.parse(s);
				this.sdf = sdf;
				cachedDates.put(ea, d);
				return d;
			} catch (ParseException ex2)
			{
			} catch (NumberFormatException ex2)
			{
			}
		}
		logger.warn("OpUnaryDateTime: failed converting object to date, returning NOW: " + s);
		return new Date();
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		return (new OpDateTime((Date) evaluate())).compareTo(b);
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		return (new OpDateTime((Date) evaluate())).equals(b);
	}

	@Override
	public Type getType()
	{
		return Operator.Type.DateTime;
	}

	@Override
	public int getSize()
	{
		return OpDateTime.DATE_SIZE;
	}
}
