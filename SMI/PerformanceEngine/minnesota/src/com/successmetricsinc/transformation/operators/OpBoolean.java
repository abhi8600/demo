package com.successmetricsinc.transformation.operators;

import org.apache.log4j.Logger;

import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;

public class OpBoolean extends Operator
{
	private static Logger logger = Logger.getLogger(OpBoolean.class);
	public boolean value;

	public OpBoolean(String text)
	{
		super(null, null);
		value = Boolean.valueOf(text);
	}

	public OpBoolean(boolean value)
	{
		super(null, null);
		this.value = value;
	}

	@Override
	public Operator clone()
	{
		return new OpBoolean(value);
	}

	@Override
	public Object evaluate()
	{
		return value;
	}

	@Override
	public int compareTo(Operator b)
	{
		if (b == null)
			return 1;
		if (b.getType() == Type.Boolean)
		{
			boolean br;
			try
			{
				Object o = b.evaluate();
				if (o == null)
					return -1;
				br = (Boolean) o;
			} catch (ScriptException e)
			{
				logger.debug("OpBoolean: this can not be correct, evaluation exception in compareTo returns 0 (note that equals returns false");
				return 0;
			}
			return value == br ? 0 : (br ? 1 : 0);
		}
		return 0;
	}

	@Override
	public boolean equals(Operator b)
	{
		if (b == null)
			return false;
		if (b.getType() == Type.Boolean)
		{
			Object o;
			try
			{
				o = b.evaluate();
			} catch (ScriptException e)
			{
				logger.debug("OpBoolean: this can not be correct, evaluation exception in compareTo returns false (note that compareTo returns 0");
				return false;
			}
			if (o == null)
				return false;
			return value == (Boolean) o;
		}
		return false;
	}

	public static boolean isTrue(Object o)
	{
		if (o == null)
			return false;
		if (Boolean.class.isInstance(o))
			return ((Boolean) o);
		else if (Integer.class.isInstance(o))
			return ((Integer) o) > 0;
		else if (Long.class.isInstance(o))
			return ((Long) o) > 0;
		else if (Double.class.isInstance(o))
			return ((Double) o) > 0;
		else
			return false;
	}

	@Override
	public Type getType()
	{
		return Operator.Type.Boolean;
	}

	@Override
	public int getSize()
	{
		return 1;
	}
}
