package com.successmetricsinc.transformation.operators;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.successmetricsinc.transformation.DataElement;
import com.successmetricsinc.transformation.Definition;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.util.DateUtil;

public class OpDateTime extends Operator
{
	public static final int DATE_SIZE = 20;	
	private Date value;
	private DataElement de;
	private Operator lookup;

	public OpDateTime(String text, TransformationScript script) throws ScriptException
	{
		super(script, null);
		if (text == null || "#NO_FILTER#".equals(text))
			return;
		java.util.Date d = null;
		if (text.equalsIgnoreCase("now") || text.equalsIgnoreCase("nowdatetime"))
			d = new Date(System.currentTimeMillis());
		else if (text.equalsIgnoreCase("nowdate"))
		{
			GregorianCalendar cal = new GregorianCalendar();
			cal.set(Calendar.HOUR, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			d = cal.getTime();
		} else
			d = Definition.getDate(text, script.getRepository(), script.getSession());
		if (d != null)
		{
			value = d;
		} else
		{
			throw new ScriptException("Unable to parse date(time) value: " + text, line, -1, -1);
		}
	}

	@Override
	public Operator clone()
	{
		if (de != null)
		{
			Operator clonedLookup = null;
			if(lookup != null)
			{
				clonedLookup = lookup.clone();
			}
			return new OpDateTime(de, clonedLookup);
		}
		else
			return new OpDateTime(value);
	}

	public OpDateTime(Date dt)
	{
		super(null, null);
		this.value = dt;
	}

	public OpDateTime(DataElement de, Operator lookup)
	{
		super(null, null);
		this.de = de;
		this.lookup = lookup;
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		if (de != null)
			return de.getValue(lookup);
		return value;
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		if (de != null)
		{
			Object o = de.getValue(lookup);
			if (o == null)
			{
				if (b == null)
					return 0;
				return -1;
			}
			if (o instanceof GregorianCalendar)
			{
				value = ((GregorianCalendar) o).getTime();
			} else
			{
				value = (Date) o;
			}
		}
		if (b == null)
			return -1;
		Object o = b.evaluate();
		if (o == null)
			return -1;
		if (b.getType() == Type.DateTime)
		{
			Date d = null;
			if (o instanceof GregorianCalendar)
			{
				d = ((GregorianCalendar) o).getTime();
			} else
			{
				d = (Date) o;
			}
			return value.compareTo(d);
		}
		return 0;
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		if (de != null)
		{
			Object o = de.getValue(lookup);
			if (o == null)
			{
				if (b == null)
					return true;
				return false;
			}
			if (o instanceof GregorianCalendar)
			{
				value = ((GregorianCalendar) o).getTime();
			} else
			{
				value = (Date) o;
			}
		}
		if (b == null)
			return false;
		Object o = b.evaluate();
		if (o == null)
			return false;
		if (b.getType() == Type.DateTime)
		{
			Date d = null;
			if (o instanceof GregorianCalendar)
			{
				d = ((GregorianCalendar) o).getTime();
			} else
			{
				d = (Date) o;
			}
			return value.equals(d);
		}
		return false;
	}

	@Override
	public Type getType()
	{
		return Operator.Type.DateTime;
	}

	@Override
	public int getSize()
	{
		return DATE_SIZE;
	}

	public DataElement getDataElement()
	{
		return de;
	}
}
