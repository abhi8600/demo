package com.successmetricsinc.transformation.operators;

import com.successmetricsinc.transformation.DataElement;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.util.Util;

public class OpVarchar extends Operator
{
	public static int MAX_VARCHAR_LENGTH = 50000;
	private String value;
	private DataElement de;
	private Operator lookup;

	public OpVarchar(String text)
	{
		super(null, null);
		// deal with backslashes
		value = Util.unquote(text);
	}

	public OpVarchar(DataElement de, Operator lookup)
	{
		super(null, null);
		this.de = de;
		this.lookup = lookup;
	}

	public Operator clone()
	{
		if (de != null)
		{
			Operator clonedLookup = null;
			if(lookup != null)
			{
				clonedLookup = lookup.clone();
			}
			return new OpVarchar(de, clonedLookup);
		}
		else
			return new OpVarchar(value);
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		if (de != null)
			return de.getValue(lookup);
		return value;
	}

	/*
	 * Note that OpVarchar compareTo is case-INSENSITIVE (non-Javadoc)
	 * 
	 * @see com.successmetricsinc.transformation.Operator#compareTo(com.successmetricsinc.transformation.Operator)
	 */
	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		if (de != null)
			value = (String) de.getValue(lookup);
		if (b == null)
			return value == null ? 0 : 1;
		else if (value == null)
			return -1;
		Object bo = b.evaluate();
		if (bo == null)
			return -1;
		return value.compareToIgnoreCase(bo.toString());
	}

	/*
	 * Note that OpVarchar equals is case-INSENSITIVE (non-Javadoc)
	 * 
	 * @see com.successmetricsinc.transformation.Operator#equals(com.successmetricsinc.transformation.Operator)
	 */
	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		if (de != null)
			value = (String) de.getValue(lookup);
		if (b == null)
			return value == null;
		Object bo = b.evaluate();
		if (value == null)
		{
			if (bo == null)
				return true;
			return false;
		} else if (bo == null)
			return false;
		return value.equalsIgnoreCase(bo.toString());
	}

	@Override
	public Type getType()
	{
		return Operator.Type.Varchar;
	}

	@Override
	public int getSize()
	{
		if (de != null)
			return de.getType().width;
		return value.length();
	}

	public DataElement getDataElement()
	{
		return de;
	}
}
