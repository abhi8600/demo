package com.successmetricsinc.transformation.operators;

import com.successmetricsinc.transformation.DataElement;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;

public class OpInt extends Operator
{
	private long value;
	private DataElement de;
	private Operator lookup;

	public OpInt(String text) throws ScriptException
	{
		super(null, null);
		if (text == null)
		{
			value = 0;
		} else
		{
			try
			{
				value = Long.valueOf(text);
			} catch (NumberFormatException e)
			{
				throw new ScriptException("Invalid integer value: " + text, line, -1, -1);
			}
		}
	}

	public OpInt(long value)
	{
		super(null, null);
		this.value = value;
	}

	@Override
	public Operator clone()
	{
		if (de != null)
		{
			Operator clonedLookup = null;
			if(lookup != null)
			{
				clonedLookup = lookup.clone();
			}
			return new OpInt(de, clonedLookup);
		}
		else
			return new OpInt(value);
	}

	public long getValue()
	{
		return value;
	}

	public OpInt(DataElement de, Operator lookup)
	{
		super(null, null);
		this.de = de;
		this.lookup = lookup;
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		if (de != null)
			return de.getValue(lookup);
		return value;
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		if (de != null)
		{
			Object o = de.getValue(lookup);
			if (o == null)
			{
				if (b == null)
					return 0;
				Object bo = b.evaluate();
				return bo == null ? 0 : -1;
			}
			if (Integer.class.isInstance(o))
				value = ((Integer) o).longValue();
			else if (Long.class.isInstance(o))
				value = (Long) o;
		}
		if (b == null)
			return -1;
		Object o = b.evaluate();
		if (o == null)
			return -1;
		if (b.getType() == Type.Integer)
		{
			if (o instanceof Integer)
			{  
				long br = ((Integer) o).longValue();  
				return value > br ? 1 : (value == br ? 0 : -1);  
			}
			else if (o instanceof Long)  
			{
				long br = (Long) o;
				return value > br ? 1 : (value == br ? 0 : -1);
			}
		} else if (b.getType() == Type.Float)
		{
			double br = (Double) o;
			return value > br ? 1 : (value == br ? 0 : -1);
		}
		return 0;
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		if (de != null)
		{
			Object o = de.getValue(lookup);
			if (o == null)
				return b == null || b.evaluate() == null;
			if (Integer.class.isInstance(o))
				value = ((Integer) o).longValue();
			else if (Long.class.isInstance(o))
				value = (Long) o;			
		}
		if (b == null)
			return false;
		Object o = b.evaluate();
		if (o == null)
			return false;
		if (b.getType() == Type.Integer)
		{
			if (o instanceof Integer)  
			{  
				long br = ((Integer) o).longValue();  
				return value == br;  
		 	}  
			else if (o instanceof Long)  
		 	{  
			 	long br = (Long) o;  
			 	return value == br;
		 	}			
		} else if (b.getType() == Type.Float)
		{
			double br = (Double) o;
			return value == br;
		}
		return false;
	}

	@Override
	public Type getType()
	{
		return Operator.Type.Integer;
	}

	@Override
	public int getSize()
	{
		return Integer.SIZE / 8;
	}

	public DataElement getDataElement()
	{
		return de;
	}
}
