package com.successmetricsinc.transformation.operators;

import java.util.List;
import java.util.regex.Pattern;

import com.successmetricsinc.transformation.BirstScriptParser;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;

public class OpBinaryBoolean extends Operator
{
	private Operator a;
	private Operator b;
	private int type;

	public OpBinaryBoolean(Operator a, Operator b, int type)
	{
		super(null, null);
		this.a = a;
		this.b = b;
		this.type = type;
	}

	@Override
	public Operator clone()
	{
		Operator cloneda = null;
		Operator clonedb = null;
		if(a != null)
		{
			cloneda = (Operator) a.clone();
		}
		if(b != null)
		{
			clonedb = (Operator) b.clone();
		}
		return new OpBinaryBoolean(cloneda, clonedb, type);
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		switch (type)
		{
		case BirstScriptParser.EQUALS:
			return a == null ? (b == null ? true : false) : a.compareTo(b) == 0;
		case BirstScriptParser.NOTEQUALS:
			return a == null ? (b == null ? false : true) : a.compareTo(b) != 0;
		case BirstScriptParser.LIKE:
			return a == null ? (b == null ? true : false) : like(a, b);
		case BirstScriptParser.NOTLIKE:
			return a == null ? (b == null ? false : true) : !like(a, b);
		case BirstScriptParser.LT:
			return a == null ? (b == null ? true : false) : a.compareTo(b) < 0;
		case BirstScriptParser.GT:
			return a == null ? false : a.compareTo(b) > 0;
		case BirstScriptParser.LTEQ:
			return a == null ? false : a.compareTo(b) <= 0;
		case BirstScriptParser.GTEQ:
			return a == null ? false : a.compareTo(b) >= 0;
		case BirstScriptParser.IN:
			return a == null ? (b == null ? true : false) : in(a, b);
		case BirstScriptParser.NOTIN:
			return a == null ? (b == null ? false : true) : !in(a, b);
		case BirstScriptParser.AND:
		{
			if (a == null || b == null)
				return false;
			if (a.getType() != Operator.Type.Boolean || b.getType() != Operator.Type.Boolean) // throw an invalid type exception
				throw new ScriptException("Argument type for AND arguments must be Boolean", line, -1, -1);
			Boolean ea = (Boolean) a.evaluate();
			if (ea == null)
				return false;
			if (!ea) 
				return false;
			Boolean eb = (Boolean) b.evaluate();
			if (eb == null)
				return false;
			if (!eb) 
				return false;
			return true;			
		}
		case BirstScriptParser.OR:
		{
			if (a == null && b == null)
				return false;
			if (a == null || b == null)
				break;
			if (a.getType() != Operator.Type.Boolean || b.getType() != Operator.Type.Boolean) // throw an invalid type exception
				throw new ScriptException("Argument type for OR arguments must be Boolean", line, -1, -1);
			Boolean ea = (Boolean) a.evaluate();
			if (ea == null)
				return null;
			if (ea)
				return true;
			Boolean eb = (Boolean) b.evaluate();
			if (eb == null)
				return null;
			if (eb) 
				return true;
			return false;
		}
		}
		return null;
	}

	/**
	 * very simple LIKE operator, same semantics as SQL (% as the wild card character)
	 * 
	 * @param a
	 * @param b
	 * @return
	 * @throws ScriptException
	 */
	private boolean like(Operator a, Operator b) throws ScriptException
	{
		if (a == null || b == null)
			return false;
		if (a.getType() != Operator.Type.Varchar || b.getType() != Operator.Type.Varchar)
			throw new ScriptException("Argument type for LIKE/NOT LIKE arguments must be Varchar", line, -1, -1);
		String ea = (String) a.evaluate();
		if (ea == null)
			return false;
		String eb = (String) b.evaluate();
		if (eb == null)
			throw new ScriptException("Pattern for LIKE/NOT LIKE operation can not be null", line, -1, -1);
		Pattern pattern = getLikeRegExPattern(eb, false); // get a case-INSENSITIVE pattern
		ea = ea.toUpperCase(); // fix 10299, upcase value since the regular expression is upcased
		return pattern.matcher(ea).find();
	}

	/**
	 * very simple IN operator, no type checking
	 * 
	 * @param a
	 * @param b
	 * @return
	 * @throws ScriptException
	 */
	@SuppressWarnings("unchecked")
	private boolean in(Operator a, Operator b) throws ScriptException
	{
		if (a == null || b == null)
			return false;
		Object ea = (Object) a.evaluate();
		Object eb = (Object) b.evaluate();
		if (ea == null)
			return false;
		if (eb != null && eb instanceof List)
		{
			return ((List<Object>) eb).contains(ea);
		}
		throw new ScriptException("Right hand side of the IN/NOT IN operator must be a LIST", line, -1, -1);
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		return (new OpBoolean((Boolean) evaluate())).compareTo(b);
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		return (new OpBoolean((Boolean) evaluate())).equals(b);
	}

	@Override
	public Type getType()
	{
		return Operator.Type.Boolean;
	}

	@Override
	public int getSize()
	{
		return 1;
	}

	/*
	 * Return a regular expression pattern that simulates the SQL like operator
	 */
	private static Pattern getLikeRegExPattern(String str, boolean caseSensitive)
	{
		if (!caseSensitive)
			str = str.toUpperCase();
		str = str.replaceAll("%", "\\\\E.*\\\\Q").replaceAll("_", "\\\\E.\\\\Q");
		if (str.startsWith("\\E"))
		{
			str = str.substring(2);
		} else
		{
			str = "\\Q" + str;
		}
		if (str.endsWith("\\Q"))
		{
			str = str.substring(0, str.length() - 2);
		} else
		{
			str = str + "\\E";
		}
		str = "^" + str + "$";
		return Pattern.compile(str);
	}
}
