package com.successmetricsinc.transformation.operators;

import com.successmetricsinc.transformation.DataElement;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;

public class OpFloat extends Operator
{
	private double value;
	private DataElement de;
	private Operator lookup;

	public OpFloat(String text) throws ScriptException
	{
		super(null, null);
		if (text == null)
		{
			value = 0.0;
		} else
		{
			try
			{
				value = Double.valueOf(text);
			} catch (NumberFormatException ex)
			{
				throw new ScriptException("Invalid float value: " + text, line, -1, -1);
			}
		}
	}

	public OpFloat(double value)
	{
		super(null, null);
		this.value = value;
	}

	public OpFloat(DataElement de, Operator lookup)
	{
		super(null, null);
		this.de = de;
		this.lookup = lookup;
	}

	@Override
	public Operator clone()
	{
		if (de != null)
		{
			Operator clonedLookup = null;
			if(lookup != null)
			{
				clonedLookup = lookup.clone();
			}
			return new OpFloat(de, clonedLookup);
		}
		else
			return new OpFloat(value);
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		if (de != null)
			return de.getValue(lookup);
		return value;
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		if (de != null)
		{
			Object o = de.getValue(lookup);
			if (o == null)
			{
				if (b == null)
					return 0;
				return -1;
			}
			if (Float.class.isInstance(o))
				value = ((Float) o).doubleValue();
			else if (Double.class.isInstance(o))
				value = (Double) o;
			else
				value = Double.valueOf(o.toString());
		}
		if (b == null)
		{
			return -1;
		}
		Object o = b.evaluate();
		if (o == null)
			return -1;
		if (b.getType() == Type.Float)
		{
			double bval = 0;
			try
			{
				bval = (Double) o;
			} catch (ClassCastException cce)
			{
				if (Integer.class.isInstance(o))
				{
					bval = ((Integer) o).doubleValue();
				} else if (Long.class.isInstance(o))
				{
					bval = ((Long) o).doubleValue();
				} else
				{
					bval = Double.valueOf(o.toString());
				}
			}
			return value == bval ? 0 : (value > bval ? 1 : -1);
		} else if (b.getType() == Type.Integer)
		{
			Long bi = null;
			try
			{
				bi = (Long) o;
			} catch (ClassCastException cce)
			{
				bi = Long.valueOf(o.toString());
			}
			return value == bi ? 0 : (value > bi ? 1 : -1);
		}
		return 0;
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		if (de != null)
		{
			Object o = de.getValue(lookup);
			if (o == null)
			{
				if (b == null)
					return true;
				return false;
			}
			value = (Double) o;
		}
		if (b == null)
			return false;
		Object o = b.evaluate();
		if (o == null)
			return false;
		if (b.getType() == Type.Float)
		{
			double bval = 0;
			try
			{
				bval = (Double) o;
			} catch (ClassCastException cce)
			{
				if (Integer.class.isInstance(o))
				{
					bval = ((Integer) o).doubleValue();
				} else if (Long.class.isInstance(o))
				{
					bval = ((Long) o).longValue();
				} else
				{
					bval = Double.valueOf(o.toString());
				}
			}
			return value == bval;
		} else if (b.getType() == Type.Integer)
		{
			Long bi = null;
			try
			{
				bi = (Long) o;
			} catch (ClassCastException cce)
			{
				bi = Long.valueOf(o.toString());
			}
			return value == bi;
		}
		return false;
	}

	@Override
	public Type getType()
	{
		return Operator.Type.Float;
	}

	@Override
	public int getSize()
	{
		return Double.SIZE / 8;
	}

	public DataElement getDataElement()
	{
		return de;
	}
}
