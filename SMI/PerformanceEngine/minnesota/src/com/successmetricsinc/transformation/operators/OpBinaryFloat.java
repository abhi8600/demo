package com.successmetricsinc.transformation.operators;

import com.successmetricsinc.transformation.BirstScriptParser;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;

public class OpBinaryFloat extends Operator
{
	private Operator a;
	private Operator b;
	private int type;

	public OpBinaryFloat(Operator a, Operator b, int type)
	{
		super(null, null);
		this.a = a;
		this.b = b;
		this.type = type;
	}

	@Override
	public Operator clone()
	{
		return new OpBinaryFloat(a, b, type);
	}

	@Override
	public Object evaluate() throws InvalidOperandException, ScriptException
	{
		if (a == null)
			throw new InvalidOperandException("Unable to evaluate float expression (" + getTypeString() + ") with null value as the first argument", line, -1,
					-1);
		if (b == null)
			throw new InvalidOperandException("Unable to evaluate float expression (" + getTypeString() + ") with null value as the second argument", line, -1,
					-1);
		Object ea = a.evaluate();
		if (ea == null)
			throw new InvalidOperandException("Unable to evaluate float expression (" + getTypeString() + ") with null value as the first argument", line, -1,
					-1);
		Object eb = b.evaluate();
		if (eb == null)
			throw new InvalidOperandException("Unable to evaluate float expression (" + getTypeString() + ") with null value as the second argument", line, -1,
					-1);
		switch (type)
		{
		case BirstScriptParser.PLUS:
			return ((Number) ea).doubleValue() + ((Number) eb).doubleValue();
		case BirstScriptParser.MINUS:
			return ((Number) ea).doubleValue() - ((Number) eb).doubleValue();
		case BirstScriptParser.MULT:
			return ((Number) ea).doubleValue() * ((Number) eb).doubleValue();
		case BirstScriptParser.DIV:
			return ((Number) ea).doubleValue() / ((Number) eb).doubleValue();
		case BirstScriptParser.MOD:
			return ((Number) ea).doubleValue() % ((Number) eb).doubleValue();
		}
		return null;
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		Double a = (Double) evaluate();
		if (a == null)
		{
			if (b == null)
				return 0;
			else
				return -1;
		} else if (b == null)
			return 1;
		return (new OpFloat(a)).compareTo(b);
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		Double a = (Double) evaluate();
		if (a == null)
		{
			if (b == null)
				return true;
			else
				return false;
		} else if (b == null)
			return false;
		return (new OpFloat(a)).equals(b);
	}

	@Override
	public Type getType()
	{
		return Operator.Type.Float;
	}

	@Override
	public int getSize()
	{
		return Double.SIZE / 8;
	}

	private String getTypeString()
	{
		switch (type)
		{
		case BirstScriptParser.PLUS:
			return "+";
		case BirstScriptParser.MINUS:
			return "-";
		case BirstScriptParser.MULT:
			return "*";
		case BirstScriptParser.DIV:
			return "/";
		case BirstScriptParser.MOD:
			return "%";
		}
		return "unknown";
	}
}
