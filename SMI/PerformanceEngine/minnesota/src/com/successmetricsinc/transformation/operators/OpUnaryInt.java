package com.successmetricsinc.transformation.operators;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.transformation.BirstScriptParser;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;

public class OpUnaryInt extends Operator
{
	private static Logger logger = Logger.getLogger(OpUnaryFloat.class);
	private Operator a;
	private int type;
	private Calendar c;
	private Repository r;

	public OpUnaryInt(Operator a, int type)
	{
		super(null, null);
		this.a = a;
		this.type = type;
	}

	public OpUnaryInt(Operator a, int type, Repository r) throws ScriptException
	{
		super(null, null);
		this.a = a;
		this.type = type;
		if (type == BirstScriptParser.GETDAYID || type == BirstScriptParser.GETWEEKID || type == BirstScriptParser.GETMONTHID)
		{
			c = Calendar.getInstance();
			this.r = r;
			if (r.getTimeDefinition() == null)
				throw new ScriptException("The time dimension must be defined in order to use time-based IDs", line, -1, -1);
		}
	}

	@Override
	public Operator clone()
	{
		try
		{
			Operator cloneda = null;
			if(a != null)
			{
				cloneda = a.clone();
			}
			return new OpUnaryInt(cloneda, type, r);
		} catch (ScriptException e)
		{
			return null;
		}
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		if (a == null)
		{
			if (type == BirstScriptParser.INTEGERTYPE)
				return 0L;
			throw new ScriptException("Cannot perform integer operation on a null value", line, -1, -1);
		}
		Object ea = a.evaluate();
		if (ea == null)
		{
			if (type == BirstScriptParser.INTEGERTYPE)
				return 0L;
			throw new ScriptException("Cannot perform integer operation on a null value", line, -1, -1);
		}
		switch (type)
		{
		case BirstScriptParser.NEGATE:
			return -((Long) ea);
		case BirstScriptParser.INTEGERTYPE:
			if (Double.class.isInstance(ea))
			{
				try
				{
					return ((Double) ea).longValue();
				} catch (NumberFormatException ex)
				{
					throw new ScriptException("Failed to convert to an INTEGER value: " + ea.toString(), -1, -1, -1);
				}
			} else if (BigDecimal.class.isInstance(ea))
			{
				try
				{
					return ((BigDecimal) ea).longValue();
				} catch (NumberFormatException ex)
				{
					throw new ScriptException("Failed to convert to an INTEGER value: " + ea.toString(), -1, -1, -1);
				}
			}
			if (ea instanceof List) // return value from GETPROMPTVALUE with multiple select (we CAST the result for
									// type safety)
			{
				@SuppressWarnings("unchecked")
				List<String> eaLst = (List<String>) ea;
				List<Long> retLst = new ArrayList<Long>(eaLst.size());
				for (int i = 0; i < eaLst.size(); i++)
				{
					Long val = 0L;
					Object obj = eaLst.get(i);
					try
					{
						val = Long.valueOf(obj.toString());
					} catch (NumberFormatException e)
					{
						logger.debug("OpUnaryInt: failed to evaluate the argument for the Long cast, returning 0: " + obj, e);
					} catch (NullPointerException e)
					{
						logger.debug("OpUnaryInt: failed to evaluate the argument for the Long cast, returning 0: " + obj, e);
					}
					retLst.add(val);
				}
				return retLst;
			}
			try
			{
				return Long.valueOf(ea.toString());
			} catch (NumberFormatException e)
			{
				throw new ScriptException("Failed to convert to an Long value: " + ea.toString(), -1, -1, -1);
			}
		case BirstScriptParser.GETDAYID:
			c.setTime((Date) ea);
			return (long) r.getTimeDefinition().getDayID(c);
		case BirstScriptParser.GETWEEKID:
			c.setTime((Date) ea);
			return (long) r.getTimeDefinition().getWeekID(c);
		case BirstScriptParser.GETMONTHID:
			c.setTime((Date) ea);
			return (long) r.getTimeDefinition().getMonthID(c);
		}
		return null;
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		return (new OpInt((Long) evaluate())).compareTo(b);
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		return (new OpInt((Long) evaluate())).equals(b);
	}

	@Override
	public Type getType()
	{
		return Operator.Type.Integer;
	}

	@Override
	public int getSize()
	{
		return Integer.SIZE / 8;
	}
}
