/**
 * 
 */
package com.successmetricsinc.transformation;

import java.util.List;

import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.warehouse.StagingTable;

/**
 * @author bpeters
 * 
 */
public class OnComplete extends Statement
{
	private StatementBlock statementBlock;

	public OnComplete(TransformationScript script, Tree t, List<StagingTable> scriptPath) throws ScriptException
	{
		if (script.onComplete != null)
			throw new ScriptException("OnComplete already defined for the script", t.getChild(0).getLine(), t.getChild(0).getCharPositionInLine(), t.getChild(0)
					.getCharPositionInLine()
					+ t.getChild(0).getText().length());
		script.onComplete = this;
		statementBlock = new StatementBlock(script, t.getChild(0), scriptPath);
		this.line = t.getLine();
		this.start = t.getCharPositionInLine();
		this.stop = t.getText().length() + this.start;
	}

	@Override
	public void execute() throws ScriptException
	{
		statementBlock.execute();
	}
}
