package com.successmetricsinc.transformation.date;

import java.util.Calendar;
import java.util.Date;

import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.transformation.Expression;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.Statement;
import com.successmetricsinc.transformation.TransformationScript;

public class DatePart extends Operator
{
	private String part;
	private Operator dateValue;
	private Calendar c = Calendar.getInstance();
	private Tree t;
	private TransformationScript script;

	public DatePart(TransformationScript script, Tree t) throws ScriptException
	{
		super(script, t);
		this.t = t;
		this.script = script;
		part = t.getChild(0).getText().toLowerCase();
		Expression ex = new Expression(script);
		dateValue = ex.compile(t.getChild(1));
		if (dateValue.getType() != Operator.Type.DateTime)
			throw new ScriptException("DatePart may only be applied to date/datetime data elements: " + Statement.toString(t, null), t.getLine(),
					t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
	}

	@Override
	public Operator clone()
	{
		try
		{
			return new DatePart(script, t);
		} catch (ScriptException e)
		{
			return null;
		}
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		long result = (Long) evaluate();
		if (b.getType() == Operator.Type.Integer)
			return ((Long) result).compareTo((Long) b.evaluate());
		return 0;
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		long result = (Long) evaluate();
		if (b.getType() == Operator.Type.Integer)
			return ((Long) result).equals((Long) b.evaluate());
		return false;
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		Date dt = null;
		Object val = dateValue.evaluate();
		if (val == null)
			throw new ScriptException("Cannot use DatePart on a null date: " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(),
					t.getCharPositionInLine() + t.getText().length());
		if (val instanceof Calendar)
			dt = ((Calendar) val).getTime();
		else
			dt = (Date) val;
		c.setTime(dt);
		if (part.equals("year"))
			return ((Integer) c.get(Calendar.YEAR)).longValue();
		else if (part.equals("quarter"))
			return ((Integer) (c.get(Calendar.MONTH)/3 + 1)).longValue(); // months start at 0 for Java, for 1 for SQL
		else if (part.equals("month"))
			return ((Integer) (c.get(Calendar.MONTH) + 1)).longValue(); // months start at 0 for Java, for 1 for SQL
		else if (part.equals("week"))
			return ((Integer) c.get(Calendar.WEEK_OF_YEAR)).longValue();
		else if (part.equals("day"))
			return ((Integer) c.get(Calendar.DAY_OF_MONTH)).longValue();
		else if (part.equals("hour"))
			return ((Integer) c.get(Calendar.HOUR_OF_DAY)).longValue();
		else if (part.equals("minute"))
			return ((Integer) c.get(Calendar.MINUTE)).longValue();
		else if (part.equals("second"))
			return ((Integer) c.get(Calendar.SECOND)).longValue();
		throw new ScriptException("Invalid date part (" + part + ") for DatePart: " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(),
				t.getCharPositionInLine() + t.getText().length());
	}

	@Override
	public int getSize()
	{
		return Integer.SIZE / 8;
	}

	@Override
	public Type getType()
	{
		return Operator.Type.Integer;
	}
}
