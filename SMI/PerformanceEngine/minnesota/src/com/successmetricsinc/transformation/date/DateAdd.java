package com.successmetricsinc.transformation.date;

import java.util.Calendar;
import java.util.Date;

import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.transformation.BirstScriptParser;
import com.successmetricsinc.transformation.Expression;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.Statement;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.transformation.operators.OpDateTime;

public class DateAdd extends Operator
{
	private String part;
	private Operator value;
	private Operator dateValue;
	private Calendar c = Calendar.getInstance();
	private Tree t;
	private TransformationScript script;

	public DateAdd(TransformationScript script, Tree t) throws ScriptException
	{
		super(script, t);
		this.t = t;
		this.script = script;
		part = t.getChild(0).getText().toLowerCase();
		Expression ex = new Expression(script);
		value = ex.compile(t.getChild(1));
		if (value.getType() != Operator.Type.Integer)
		{
			if (t.getChild(1).getType() != BirstScriptParser.GETPROMPTVALUE)
				throw new ScriptException("Value to add in DateAdd must be of type integer or the result of GETPROMPTVALUE: " + Statement.toString(t, null),
						t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
		dateValue = ex.compile(t.getChild(2));
		if (dateValue.getType() != Operator.Type.DateTime)
			throw new ScriptException("DateAdd may only be applied to date/datetime data elements: " + Statement.toString(t, null), t.getLine(),
					t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
	}

	@Override
	public Operator clone()
	{
		try
		{
			return new DateAdd(script, t);
		} catch (ScriptException e)
		{
			return null;
		}
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		if (OpDateTime.class.isInstance(b))
			return ((Date) evaluate()).compareTo((Date) b.evaluate());
		return 0;
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		if (OpDateTime.class.isInstance(b))
			return ((Date) evaluate()).equals((Date) b.evaluate());
		return false;
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		Date dt = null;
		Object val = dateValue.evaluate();
		if (val == null)
			throw new ScriptException("Cannot use DateAdd on a null date: " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(),
					t.getCharPositionInLine() + t.getText().length());
		if (val instanceof Calendar)
			dt = ((Calendar) val).getTime();
		else
			dt = (Date) val;
		Integer amount = null;
		Object obj = value.evaluate();
		if (obj == null)
			throw new ScriptException("Cannot add a null value to a date using DateAdd: " + Statement.toString(t, null), t.getLine(),
					t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		if (obj instanceof String)
			amount = Integer.valueOf((String) obj);
		else if (obj instanceof Long)
			amount = ((Long) obj).intValue();
		else
			amount = (Integer) obj;
		c.setTime(dt);
		if (part.equals("year"))
		{
			c.add(Calendar.YEAR, amount);
		} else if (part.equals("quarter"))
		{
			c.add(Calendar.MONTH, amount * 3);
		} else if (part.equals("month"))
		{
			c.add(Calendar.MONTH, amount);
		} else if (part.equals("week"))
		{
			c.add(Calendar.WEEK_OF_YEAR, amount);
		} else if (part.equals("day"))
		{
			c.add(Calendar.DAY_OF_YEAR, amount);
		} else if (part.equals("hour"))
		{
			c.add(Calendar.HOUR, amount);
		} else if (part.equals("minute"))
		{
			c.add(Calendar.MINUTE, amount);
		} else if (part.equals("second"))
		{
			c.add(Calendar.SECOND, amount);
		} else
		{
			throw new ScriptException("Invalid date part (" + part + ") for DateAdd: " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(),
					t.getCharPositionInLine() + t.getText().length());
		}
		return c.getTime();
	}

	@Override
	public int getSize()
	{
		return OpDateTime.DATE_SIZE;
	}

	@Override
	public Type getType()
	{
		return Operator.Type.DateTime;
	}
}
