package com.successmetricsinc.transformation.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.transformation.Expression;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.Statement;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.transformation.operators.OpBinaryInt;
import com.successmetricsinc.transformation.operators.OpInt;
import com.successmetricsinc.transformation.operators.OpUnaryInt;

public class DateDiff extends Operator
{
	private String part;
	private Operator dateValue1;
	private Operator dateValue2;
	private Calendar c1 = Calendar.getInstance();
	private Calendar c2 = Calendar.getInstance();
	private Tree t;
	TransformationScript script;

	public DateDiff(TransformationScript script, Tree t) throws ScriptException
	{
		super(script, t);
		this.t = t;
		this.script = script;
		part = t.getChild(0).getText().toLowerCase();
		Expression ex = new Expression(script);
		dateValue1 = ex.compile(t.getChild(1));
		if (dateValue1.getType() != Operator.Type.DateTime)
			throw new ScriptException("DateDiff may only be applied to date/datetime data elements: " + Statement.toString(t, null), t.getLine(),
					t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		dateValue2 = ex.compile(t.getChild(2));
		if (dateValue2.getType() != Operator.Type.DateTime)
			throw new ScriptException("DateDiff may only be applied to date/datetime data elements: " + Statement.toString(t, null), t.getLine(),
					t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
	}

	@Override
	public Operator clone()
	{
		try
		{
			return new DateDiff(script, t);
		} catch (ScriptException e)
		{
			return null;
		}
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		long result = (Long) evaluate();
		if (OpInt.class.isInstance(b) || OpUnaryInt.class.isInstance(b) || OpBinaryInt.class.isInstance(b))
			return ((Long) result).compareTo((Long) b.evaluate());
		return 0;
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		long result = (Long) evaluate();
		if (OpInt.class.isInstance(b) || OpUnaryInt.class.isInstance(b) || OpBinaryInt.class.isInstance(b))
			return ((Long) result).equals((Long) b.evaluate());
		return false;
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		TimeZone processingTimeZone = script.getRepository().getServerParameters().getProcessingTimeZone();
		c1 = Calendar.getInstance(processingTimeZone);
		c2 = Calendar.getInstance(processingTimeZone);
		Date dt1 = null;
		Object val = dateValue1.evaluate();
		if (val == null)
			throw new ScriptException("Cannot use DateDiff on null values (second argument): " + Statement.toString(t, null), t.getLine(),
					t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		if (val instanceof Calendar)
			dt1 = ((Calendar) val).getTime();
		else
			dt1 = (Date) val;
		Date dt2 = null;
		val = dateValue2.evaluate();
		if (val == null)
			throw new ScriptException("Cannot use DateDiff on null values (third argument): " + Statement.toString(t, null), t.getLine(),
					t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		if (val instanceof Calendar)
			dt2 = ((Calendar) val).getTime();
		else
			dt2 = (Date) val;
		SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		sdf.setTimeZone(processingTimeZone);
		try
		{
			c1.setTime(sdf.parse(sdf.format(dt1)));
		} catch (ParseException pe)
		{
			c1.setTime(dt1);
		}
		try
		{
			c2.setTime(sdf.parse(sdf.format(dt2)));
		} catch (ParseException pe)
		{
			c2.setTime(dt2);
		}
		c1.set(Calendar.HOUR_OF_DAY, 0);
		c1.set(Calendar.MINUTE, 0);
		c1.set(Calendar.SECOND, 0);
		c1.set(Calendar.MILLISECOND, 0);
		c2.set(Calendar.HOUR_OF_DAY, 0);
		c2.set(Calendar.MINUTE, 0);
		c2.set(Calendar.SECOND, 0);
		c2.set(Calendar.MILLISECOND, 0);
		if (part.equals("year"))
		{
			int year1 = c1.get(Calendar.YEAR);
			int year2 = c2.get(Calendar.YEAR);
			return (((Integer)(year2 - year1)).longValue());
		} else if (part.equals("quarter"))
		{
			int quarter1 = c1.get(Calendar.YEAR) * 4 + c1.get(Calendar.MONTH) / 3;
			int quarter2 = c2.get(Calendar.YEAR) * 4 + c2.get(Calendar.MONTH) / 3;
			return (((Integer)(quarter2 - quarter1)).longValue());
		} else if (part.equals("month"))
		{
			int month1 = (c1.get(Calendar.YEAR) * 12) + c1.get(Calendar.MONTH);
			int month2 = (c2.get(Calendar.YEAR) * 12) + c2.get(Calendar.MONTH);
			return (((Integer)(month2 - month1)).longValue());
		} else if (part.equals("week"))
		{
			if (c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
				return (((Integer)(c2.get(Calendar.WEEK_OF_YEAR) - c1.get(Calendar.WEEK_OF_YEAR))).longValue());
			int weekDiff = 0, weekDiffStart = 0, weekDiffEnd = 0;
			int multiplier = 1;
			Calendar clonedc1 = (Calendar) c1.clone();
			Calendar clonedc2 = (Calendar) c2.clone();
			if (c1.get(Calendar.YEAR) > c2.get(Calendar.YEAR))
			{
				multiplier = -1;
				Calendar temp = clonedc1;
				clonedc1 = clonedc2;
				clonedc2 = temp;
			}
			Calendar endOfYearDate = (Calendar) clonedc1.clone();
			weekDiffStart = endOfYearDate.get(Calendar.WEEK_OF_YEAR);
			weekDiffEnd = endOfYearDate.getActualMaximum(Calendar.WEEK_OF_YEAR);
			weekDiff += weekDiffEnd - weekDiffStart;
			endOfYearDate.set(endOfYearDate.get(Calendar.YEAR) + 1, 0, 1);
			while (endOfYearDate.get(Calendar.YEAR) < clonedc2.get(Calendar.YEAR))
			{
				weekDiffEnd = endOfYearDate.getActualMaximum(Calendar.WEEK_OF_YEAR);
				weekDiff += weekDiffEnd;
				endOfYearDate.set(Calendar.YEAR, endOfYearDate.get(Calendar.YEAR) + 1);
			}
			weekDiff += clonedc2.get(Calendar.WEEK_OF_YEAR);
			return (((Integer)(weekDiff * multiplier)).longValue());
		} else if (part.equals("day"))
		{
			if (c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR))
				return (((Integer)(c2.get(Calendar.DAY_OF_YEAR) - c1.get(Calendar.DAY_OF_YEAR))).longValue());
			int dayDiff = 0, dayDiffStart = 0, dayDiffEnd = 0;
			int multiplier = 1;
			Calendar clonedc1 = (Calendar) c1.clone();
			Calendar clonedc2 = (Calendar) c2.clone();
			if (c1.get(Calendar.YEAR) > c2.get(Calendar.YEAR))
			{
				multiplier = -1;
				Calendar temp = clonedc1;
				clonedc1 = clonedc2;
				clonedc2 = temp;
			}
			Calendar endOfYearDate = (Calendar) clonedc1.clone();
			dayDiffStart = endOfYearDate.get(Calendar.DAY_OF_YEAR);
			dayDiffEnd = endOfYearDate.getActualMaximum(Calendar.DAY_OF_YEAR);
			dayDiff += dayDiffEnd - dayDiffStart;
			endOfYearDate.set(endOfYearDate.get(Calendar.YEAR) + 1, 0, 1);
			while (endOfYearDate.get(Calendar.YEAR) < clonedc2.get(Calendar.YEAR))
			{
				dayDiffEnd = endOfYearDate.getActualMaximum(Calendar.DAY_OF_YEAR);
				dayDiff += dayDiffEnd;
				endOfYearDate.set(Calendar.YEAR, endOfYearDate.get(Calendar.YEAR) + 1);
			}
			dayDiff += clonedc2.get(Calendar.DAY_OF_YEAR);
			return (((Integer)(dayDiff * multiplier)).longValue());
		} else if (part.equals("hour"))
		{
			long offset = getZoneOffsetBetweenDates(dt2, dt1);
			return (long)((dt2.getTime() + offset - dt1.getTime()) / (1000 * 60 * 60));
		} else if (part.equals("minute"))
		{
			long offset = getZoneOffsetBetweenDates(dt2, dt1);
			return (long)((dt2.getTime() + offset - dt1.getTime()) / (1000 * 60));
		} else if (part.equals("second"))
		{
			long offset = getZoneOffsetBetweenDates(dt2, dt1);
			return (long)((dt2.getTime() + offset - dt1.getTime()) / 1000);
		}
		throw new ScriptException("Invalid date part (" + part + ") for DateDiff: " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(),
				t.getCharPositionInLine() + t.getText().length());
	}

	private long getZoneOffsetBetweenDates(Date dt2, Date dt1)
	{
		c1.setTime(dt1);
		c2.setTime(dt2);
		long offset1 = c1.getTimeZone().getOffset(dt1.getTime());
		long offset2 = c2.getTimeZone().getOffset(dt2.getTime());
		return (offset2 - offset1);
	}

	@Override
	public int getSize()
	{
		return Integer.SIZE / 8;
	}

	@Override
	public Type getType()
	{
		return Operator.Type.Integer;
	}
}
