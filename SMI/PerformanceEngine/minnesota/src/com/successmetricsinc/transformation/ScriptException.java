package com.successmetricsinc.transformation;

import com.successmetricsinc.util.BaseException;

public class ScriptException extends BaseException
{
	private static final long serialVersionUID = 1L;
	private String message;
	private int line;
	private int start;
	private int end;
	private boolean exitWhile;
	private boolean exitFor;
	private boolean functionReturn;

	public int getStart()
	{
		return start;
	}

	public void setStart(int start)
	{
		this.start = start;
	}

	public int getEnd()
	{
		return end;
	}

	public void setEnd(int end)
	{
		this.end = end;
	}

	public ScriptException()
	{
	}

	public ScriptException(String message, int line, int start, int end)
	{
		this.message = message;
		this.line = line;
		this.start = start;
		this.end = end;
	}

	public String getMessage()
	{
		return message;
	}

	public void setLine(int line)
	{
		this.line = line;
	}

	public int getLine()
	{
		return line;
	}

	public boolean isExitWhile()
	{
		return exitWhile;
	}

	public void setExitWhile(boolean exitWhile)
	{
		this.exitWhile = exitWhile;
	}

	public boolean isExitFor()
	{
		return exitFor;
	}

	public void setExitFor(boolean exitFor)
	{
		this.exitFor = exitFor;
	}

	public int getErrorCode()
	{
		return BaseException.ERROR_SYNTAX; // XXX lie for now
	}

	public String toString()
	{
		return "Exception (" + "line: " + line + ") " + message;
	}

	public boolean isFunctionReturn()
	{
		return functionReturn;
	}

	public void setFunctionReturn(boolean functionReturn)
	{
		this.functionReturn = functionReturn;
	}
}
