/**
 * 
 */
package com.successmetricsinc.transformation;


/**
 * @author bpeters
 * 
 */
public interface DataElement
{
	public String getName();

	public DataType getType();

	public boolean canGet();

	public boolean canSet();

	public Object getValue(Operator lookup) throws ScriptException;

	public void setValue(Object o);
}
