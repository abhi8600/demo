/**
 * 
 */
package com.successmetricsinc.transformation.object;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.transformation.DataType;
import com.successmetricsinc.transformation.Definition;
import com.successmetricsinc.transformation.Expression;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.warehouse.StagingTable;

/**
 * @author bpeters
 * 
 */
public class Let extends Operator
{
	private Tree t;
	private TransformationScript script;
	private TransformationScript functionScript;
	private Operator exp;
	private DataType type;
	private List<Definition> dlist = new ArrayList<Definition>();

	public Let(TransformationScript script, Tree t) throws ScriptException
	{
		super(script, t);
		this.t = t;
		this.script = script;
		functionScript = new TransformationScript(script.getRepository(), script.getSession());
		functionScript.initializeScriptResults();
		functionScript.setLogicalQuery(true);
		functionScript.setDisplayExpression(script.getDisplayExpression());
		Expression ex = null;
		Tree expression = null;
		for (int i = 0; i < t.getChild(0).getChildCount(); i++)
		{
			List<StagingTable> scriptPath = new ArrayList<StagingTable>();
			Definition def = new Definition(functionScript, t.getChild(0).getChild(i), scriptPath);
			dlist.add(def);
		}
		expression = t.getChild(1);
		type = Expression.getLogicalQueryType(functionScript, expression);
		if (type == null)
			throw new ScriptException("Unable to evaluate expression " + expression.getText() + ": line " + expression.getLine() + ", ", expression.getLine(),
					expression.getCharPositionInLine(), expression.getCharPositionInLine() + expression.getText().length());
		ex = new Expression(functionScript);
		exp = ex.compile(expression);
	}

	@Override
	public Operator clone()
	{
		try
		{
			return new Let(script, t);
		} catch (ScriptException e)
		{
			return null;
		}
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		try
		{
			Object aval = evaluate();
			Object bval = b.evaluate();
			if (aval == null && bval == null)
				return 0;
			if (bval == null)
				return -1;
			if (aval == null)
				return 1;
			if (aval.getClass() != bval.getClass())
				throw new ScriptException("Bad data type: ", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
			if (Integer.class.isInstance(aval))
				return ((Integer) aval).compareTo((Integer) bval);
			else if (Long.class.isInstance(aval))
				return ((Long) aval).compareTo((Long) bval);
			else if (Double.class.isInstance(aval))
				return ((Double) aval).compareTo((Double) bval);
			else if (Boolean.class.isInstance(aval))
				return ((Boolean) aval).compareTo((Boolean) bval);
			else if (String.class.isInstance(aval))
				return ((String) aval).compareTo((String) bval);
			else if (Date.class.isInstance(aval))
				return ((Date) aval).compareTo((Date) bval);
			else if (Calendar.class.isInstance(aval))
				return ((Calendar) aval).compareTo((Calendar) bval);
			
			return 0;
		} catch (Exception e)
		{
			if (e instanceof ScriptException)
				throw (ScriptException) e;
			throw new ScriptException("Bad data type: ", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		try
		{
			Object aval = evaluate();
			Object bval = b.evaluate();
			if (aval.getClass() != bval.getClass())
				throw new ScriptException("Bad data type: ", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
			if (Integer.class.isInstance(aval))
				return ((Integer) aval).equals((Integer) bval);
			else if (Long.class.isInstance(aval))
				return ((Long) aval).equals((Long) bval);
			else if (Double.class.isInstance(aval))
				return ((Double) aval).equals((Double) bval);
			else if (Boolean.class.isInstance(aval))
				return ((Boolean) aval).equals((Boolean) bval);
			else if (String.class.isInstance(aval))
				return ((String) aval).equals((String) bval);
			else if (Date.class.isInstance(aval))
				return ((Date) aval).equals((Date) bval);
			else if (Calendar.class.isInstance(aval))
				return ((Calendar) aval).equals((Calendar) bval);
			
			return false;
		} catch (Exception e)
		{
			throw new ScriptException("Bad data type", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		for (Definition def : dlist)
		{
			def.setInitialized(false);
			def.execute();
		}
		return exp.evaluate();
	}

	@Override
	public int getSize()
	{
		return exp.getSize();
	}

	@Override
	public Type getType()
	{
		return exp.getType();
	}
}
