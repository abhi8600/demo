/**
 * 
 */
package com.successmetricsinc.transformation.object;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.UUID;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;

import com.successmetricsinc.util.BirstXMLSerializable;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class SavedExpressions extends HashMap<String, SavedExpressions.Expression> {
	private static Logger logger = Logger.getLogger(SavedExpressions.class);
	private static final String SAVED_EXPRESSION = "SavedExpression";
	
	public static final String fileName = "SavedExpressions.xml";
	public enum ColumnType
	{
		Dimension, Measure, Expression, Ratio
	};
	private static HashMap<ColumnType, String> columnTypeNames;
	static {
		columnTypeNames = new HashMap<ColumnType, String>();
		columnTypeNames.put(ColumnType.Dimension, "Dimension");
		columnTypeNames.put(ColumnType.Measure, "Measure");
	}
	
	private long lastModified = 0;
	private File file;
	
	public class Expression extends BirstXMLSerializable {
		private static final String CREATED_DATE = "CreatedDate";
		private static final String CREATED_BY = "CreatedBy";
		private static final String MODIFIED_BY = "ModifiedBy";
		private static final String UUID_STR = "UUID";
		private static final String LAST_MODIFIED = "LastModified";
		private static final String LOCATION = "Location";
		private static final String EXPRESSION = "Expression";
		private static final String COLUMN_TYPE = "ColumnType";
		private static final String FORMAT = "Format";
		private static final String NAME = "Name";
		
		private String name;
		private String format;
		private ColumnType columnType; 
		private String expression;
		private String location;
		private UUID guid;
		private Date created;
		private String createdBy;
		private Date lastModified;
		private String modifiedBy;
		
		// location
		
		public String getLocation() {
			return location;
		}

		public void setLocation(String location) {
			this.location = location;
		}

		public String getFormat() {
			return format;
		}

		public void setFormat(String format) {
			this.format = format;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public ColumnType getColumnType() {
			return columnType;
		}

		public void setColumnType(ColumnType columnType) {
			this.columnType = columnType;
		}

		public String getExpression() {
			return expression;
		}

		public void setExpression(String expression) {
			this.expression = expression;
		}

		public Expression() {
		}
		
		
		@Override
		public void addContent(OMFactory fac, OMElement grandParent, OMNamespace ns) {
			OMElement parent = fac.createOMElement(SAVED_EXPRESSION, ns);
			XmlUtils.addContent(fac, parent, NAME, name, ns);
			XmlUtils.addContent(fac, parent, FORMAT, format, ns);
			XmlUtils.addContent(fac, parent, COLUMN_TYPE, columnTypeNames.get(columnType), ns);
			XmlUtils.addContent(fac, parent, EXPRESSION, expression, ns);
			XmlUtils.addContent(fac, parent, LOCATION, location, ns);
			XmlUtils.addContent(fac, parent, LAST_MODIFIED, this.lastModified, ns);
			XmlUtils.addContent(fac, parent, UUID_STR, guid, ns);
			XmlUtils.addContent(fac, parent, MODIFIED_BY, modifiedBy, ns);
			XmlUtils.addContent(fac, parent, CREATED_BY, createdBy, ns);
			XmlUtils.addContent(fac, parent, CREATED_DATE, created, ns);
			grandParent.addChild(parent);
		}

		@Override
		public BirstXMLSerializable createNew() {
			return new Expression();
		}

		@Override
		public void parseElement(OMElement parent) {
			for (Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
				OMElement el = iter.next();
				String elName = el.getLocalName();

				if (NAME.equals(elName))
					name = XmlUtils.getStringContent(el);
				else if (FORMAT.equals(elName))
					format = XmlUtils.getStringContent(el);
				else if (COLUMN_TYPE.equals(elName)) 
					columnType = BirstXMLSerializable.parseTypeFromString(XmlUtils.getStringContent(el), 
							columnTypeNames, ColumnType.Expression);
				else if (EXPRESSION.equals(elName))
					expression = XmlUtils.getStringContent(el);
				else if (LOCATION.equals(elName))
					location = XmlUtils.getStringContent(el);
				else if (LAST_MODIFIED.equals(elName))
					this.lastModified = XmlUtils.getDateContent(el);
				else if (UUID_STR.equals(elName))
					guid = XmlUtils.getUUIDContent(el);
				else if (MODIFIED_BY.equals(elName))
					modifiedBy = XmlUtils.getStringContent(el);
				else if (CREATED_BY.equals(elName))
					createdBy = XmlUtils.getStringContent(el);
				else if (CREATED_DATE.equals(elName))
					created = XmlUtils.getDateContent(el);
			}
		}

		@Override
		public void parseElement(XMLStreamReader parent) throws Exception {
			for (Iterator<Object> iter = new XMLStreamIterator(parent); iter.hasNext(); ) {
				String elName = parent.getLocalName();
			
				if (NAME.equals(elName))
					name = XmlUtils.getStringContent(parent);
				else if (FORMAT.equals(elName))
					format = XmlUtils.getStringContent(parent);
				else if (COLUMN_TYPE.equals(elName)) {
					String ctype = XmlUtils.getStringContent(parent);
					columnType = BirstXMLSerializable.parseTypeFromString(ctype, columnTypeNames, ColumnType.Expression);
				}
				else if (EXPRESSION.equals(elName))
					expression = XmlUtils.getStringContent(parent);
				else if (LOCATION.equals(elName))
					location = XmlUtils.getStringContent(parent);
				else if (LAST_MODIFIED.equals(elName))
					this.lastModified = XmlUtils.getDateContent(parent);
				else if (UUID_STR.equals(elName))
					guid = XmlUtils.getUUIDContent(parent);
				else if (MODIFIED_BY.equals(elName))
					modifiedBy = XmlUtils.getStringContent(parent);
				else if (CREATED_BY.equals(elName))
					createdBy = XmlUtils.getStringContent(parent);
				else if (CREATED_DATE.equals(elName))
					created = XmlUtils.getDateContent(parent);
			}
		}
		
		public String toString() {
			StringBuilder build = new StringBuilder();
			build.append("SavedExpression: ");
			build.append(name);
			build.append('(');
			build.append(guid.toString());
			build.append(") <");
			build.append(expression);
			build.append("> location: ");
			build.append(location);
			return build.toString();
		}
	}

	
	public SavedExpressions(File f) {
		file = f;
		if (file.exists()) {
			lastModified = file.lastModified();
			FileInputStream fis = null;
			try {
				fis = new FileInputStream(file);
				XMLInputFactory inputFactory = XmlUtils.getSecureXMLInputFactory();
				XMLStreamReader reader = inputFactory.createXMLStreamReader(fis);
				convert(reader);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error(e,e);
			}
			finally {
				if (fis != null) {
					try {
						fis.close();
					} catch (IOException e) {
					}
				}
			}
		}
	}

	public SavedExpressions() {
	}

	private void convert(XMLStreamReader reader) {
		try {
			for (Iterator<Object> iter = new XMLStreamIterator(reader); iter.hasNext(); ) {
				String elName = reader.getLocalName();
				if (SAVED_EXPRESSION.equals(elName)) {
					Expression e = new Expression();
					e.parseElement(reader);
					put(e.name, e);
				}
			}
		} catch (Exception e) {
			logger.error(e,e);
		}
	}

	public void updateSavedExpression(Expression exp, String user) throws DuplicateNameException, OutOfDateException, IOException {
		File lockFile = lock(file);
		if (lockFile != null) {
			try {
				Expression existing = get(exp.getName());
				if (existing != null && ! existing.guid.equals(exp.guid)) {
					throw new DuplicateNameException("There already is a different saved expression with the name " + exp.getName());
				}
				
				if (existing != null && existing.lastModified.after(exp.lastModified)) {
					throw new OutOfDateException("Saved expression " + exp.getName() + " was modified by " + exp.modifiedBy + " on " + DateFormat.getDateTimeInstance().format(exp.lastModified));
				}
				
				if (existing == null && exp.guid != null) {
					// no saved expression by that name exists - look for guid
					String remove = null;
					for (String s : keySet()) {
						Expression e = get(s);
						if (exp.guid.equals(e.guid)) {
							remove = s;
						}
					}
					
					if (remove != null)
						remove(remove);
				}
				put(exp.getName(), exp);
				
				if (exp.guid == null)
					exp.guid = UUID.randomUUID();
				
				if (exp.created == null)
					exp.created = new Date();
				if (exp.createdBy == null)
					exp.createdBy = user;
				
				exp.lastModified = new Date();
				exp.modifiedBy = user;
				
				logger.info("Saved Expression saved " + exp.toString());
				
				save();
			}
			finally {
				unlock(lockFile);
			}
		}
	}
	
	private File lock(File f) {
		try {
			File lockFile = new File(f.getParentFile(), fileName + ".lock");
			for (int i = 0; i < 10; i++) {
				 if (lockFile.createNewFile() == true)
					 return lockFile;
				 
				 Thread.sleep(1000);
			}
		}
		catch (Exception e) {}
		return null;
	}
	private void unlock(File f) {
		f.delete();
	}
	
	private void save() throws IOException {
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;
		OMElement parent = factory.createOMElement(this.getClass().getName(), ns);
		for (Expression exp : values()) {
			exp.addContent(factory, parent, ns);
		}
		
		FileOutputStream fos = null;
		
		try {
			if (! file.exists()) {
				file.createNewFile();
			}
			fos = new FileOutputStream(file);
			XMLStreamWriter writer = XMLOutputFactory.newInstance().createXMLStreamWriter(fos);
			parent.serialize(writer);
			writer.flush();
			lastModified = file.lastModified();
		} catch (IOException e) {
			logger.error("Error saving saved expressions: (with attempted file name: " + file.toString() +")", e);
			throw (e);
		} catch (XMLStreamException e) {
			logger.error(e, e);
		} catch (FactoryConfigurationError e) {
			logger.error(e, e);
		}
		finally {
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
				}
			}
		}
	}

	public boolean isOlder(File file) {
		return file.lastModified() > lastModified;
	}

	public void delete(String name) throws IOException {
		File lockFile = lock(file);
		if (lockFile != null) {
			try {
				remove(name); 
				
				logger.info("Removed Saved Expression " + name);
				
				save();
			}
			finally {
				unlock(lockFile);
			}
		}
	}

}
