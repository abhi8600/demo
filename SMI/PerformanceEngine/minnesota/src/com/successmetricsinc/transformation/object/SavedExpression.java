/**
 * 
 */
package com.successmetricsinc.transformation.object;

import java.util.Calendar;
import java.util.Date;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.query.LogicalQueryString;
import com.successmetricsinc.query.SyntaxErrorException;
import com.successmetricsinc.transformation.BirstScriptLexer;
import com.successmetricsinc.transformation.BirstScriptParser;
import com.successmetricsinc.transformation.Expression;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.Statement;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.transformation.BirstScriptParser.logicalColumn_return;

/**
 * @author agarrison
 * 
 */
public class SavedExpression extends Operator
{
	private Tree tree;
	private TransformationScript script;
	private String name;
	private Operator expression;

	public SavedExpression(TransformationScript script, Tree t) throws ScriptException
	{
		super(script, t);
		tree = t;
		this.script = script;
		if (t.getChildCount() != 1)
			throw new ScriptException("A SavedExpression must have one and only 1 argument: " + Statement.toString(t, null), t.getLine(),
					t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		Tree ch = t.getChild(0);
		if (ch.getType() != BirstScriptParser.STRING)
			throw new ScriptException("A SavedExpression must be a string constant: " + Statement.toString(ch, null), t.getLine(), t.getCharPositionInLine(),
					t.getCharPositionInLine() + t.getText().length());
		name = t.getChild(0).getText();
		if (name == null || name.trim().length() == 0)
		{
			throw new ScriptException("A SavedExpression can not be null: " + Statement.toString(ch, null), t.getLine(), t.getCharPositionInLine(),
					t.getCharPositionInLine() + t.getText().length());
		}
		if (name.startsWith("'"))
			name = name.substring(1);
		if (name.endsWith("'"))
			name = name.substring(0, name.length() - 1);
		if (name == null || name.trim().length() == 0)
		{
			throw new ScriptException("A SavedExpression can not be null: " + Statement.toString(ch, null), t.getLine(), t.getCharPositionInLine(),
					t.getCharPositionInLine() + t.getText().length());
		}
		try
		{
			expression = findSavedExpression(script, script.getRepository(), script.getSession());
		} catch (Exception e)
		{
			throw new ScriptException("Error compiling the SavedExpression: " + name + ": " + e.getMessage(), t.getLine(), t.getCharPositionInLine(),
					t.getCharPositionInLine() + t.getText().length());
		}
		if (expression == null)
		{
			throw new ScriptException("Error finding the SavedExpression: " + name, t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine()
					+ t.getText().length());
		}
	}

	@Override
	public Operator clone()
	{
		try
		{
			return new SavedExpression(script, tree);
		} catch (ScriptException e)
		{
			return null;
		}
	}

	private Operator findSavedExpression(TransformationScript script, Repository r, Session session) throws RecognitionException, SyntaxErrorException,
			ScriptException
	{
		SavedExpressions expressions = null;
		if (session != null)
			expressions = session.getPrivateSavedExpressions();
		Operator op = getSavedExpressionFromMap(script, expressions);
		if (op == null)
		{
			op = getSavedExpressionFromMap(script, r.getSavedExpressions());
		}
		return op;
	}

	private Operator getSavedExpressionFromMap(TransformationScript script, SavedExpressions expressions) throws ScriptException, SyntaxErrorException,
			RecognitionException
	{
		if (expressions != null)
		{
			SavedExpressions.Expression expression = expressions.get(name);
			if (expression != null)
			{
				Expression ex = new Expression(script);
				String exprText = expression.getExpression();
				if (exprText.startsWith("alt:"))
					exprText = exprText.substring(4);
				ANTLRStringStream stream = new ANTLRStringStream(exprText);
				BirstScriptLexer lex = new BirstScriptLexer(stream);
				CommonTokenStream cts = new CommonTokenStream(lex);
				BirstScriptParser parser = new BirstScriptParser(cts);
				logicalColumn_return ret = parser.logicalColumn();
				if (lex.hasError())
					LogicalQueryString.processLexError(lex, exprText);
				return ex.compile((Tree) ret.getTree());
			}
		}
		return null;
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		try
		{
			Object aval = evaluate();
			Object bval = b.evaluate();
			if (aval.getClass() != bval.getClass())
				if (Number.class.isInstance(aval) && Number.class.isInstance(bval))
					return ((Double) ((Number) aval).doubleValue()).compareTo(((Number) bval).doubleValue());
				else
					throw new ScriptException("Bad data types in the SavedExpression", tree.getLine(), tree.getCharPositionInLine(),
							tree.getCharPositionInLine() + tree.getText().length());
			if (Integer.class.isInstance(aval))
				return ((Integer) aval).compareTo((Integer) bval);
			else if (Long.class.isInstance(aval))
				return ((Long) aval).compareTo((Long) bval);
			else if (Double.class.isInstance(aval))
				return ((Double) aval).compareTo((Double) bval);
			else if (Boolean.class.isInstance(aval))
				return ((Boolean) aval).compareTo((Boolean) bval);
			else if (String.class.isInstance(aval))
				return ((String) aval).compareTo((String) bval);
			else if (Date.class.isInstance(aval))
				return ((Date) aval).compareTo((Date) bval);
			else if (Calendar.class.isInstance(aval))
				return ((Calendar) aval).compareTo((Calendar) bval);
			
			return 0;
		} catch (Exception e)
		{
			if (e instanceof ScriptException)
				throw (ScriptException) e;
			throw new ScriptException("Bad data types in the SavedExpression", tree.getLine(), tree.getCharPositionInLine(), tree.getCharPositionInLine()
					+ tree.getText().length());
		}
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		try
		{
			Object aval = evaluate();
			Object bval = b.evaluate();
			if (aval.getClass() != bval.getClass())
				if (Number.class.isInstance(aval) && Number.class.isInstance(bval))
					return ((Double) ((Number) aval).doubleValue()).equals(((Number) bval).doubleValue());
				else
					throw new ScriptException("Bad data types in the SavedExpression", tree.getLine(), tree.getCharPositionInLine(),
							tree.getCharPositionInLine() + tree.getText().length());
			if (Integer.class.isInstance(aval))
				return ((Integer) aval).equals((Integer) bval);
			else if (Long.class.isInstance(aval))
				return ((Long) aval).equals((Long) bval);
			else if (Double.class.isInstance(aval))
				return ((Double) aval).equals((Double) bval);
			else if (Boolean.class.isInstance(aval))
				return ((Boolean) aval).equals((Boolean) bval);
			else if (String.class.isInstance(aval))
				return ((String) aval).equals((String) bval);
			else if (Date.class.isInstance(aval))
				return ((Date) aval).equals((Date) bval);
			else if (Calendar.class.isInstance(aval))
				return ((Calendar) aval).equals((Calendar) bval);
			
			return false;
		} catch (Exception e)
		{
			throw new ScriptException("Bad data types in the SavedExpression", tree.getLine(), tree.getCharPositionInLine(), tree.getCharPositionInLine()
					+ tree.getText().length());
		}
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		return expression.evaluate();
	}

	@Override
	public int getSize()
	{
		return expression.getSize();
	}

	@Override
	public Type getType()
	{
		return expression.getType();
	}
}
