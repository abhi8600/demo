/**
 * 
 */
package com.successmetricsinc.transformation.object;

import java.util.List;
import java.util.Map;

import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.transformation.DataElement;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.Statement;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.transformation.VariableDataElement;
import com.successmetricsinc.transformation.DataType.DType;

/**
 * @author bpeters
 * 
 */
public class RemoveAll extends Statement
{
	private VariableDataElement vde;

	public RemoveAll(TransformationScript script, Tree t) throws ScriptException
	{
		this.script = script;
		String vname = t.getChild(0).getText();
		DataElement de = script.findElement(vname);
		if (de == null)
			throw new ScriptException("Unknown variable " + vname + " at line " + t.getLine(), t.getLine(), t.getCharPositionInLine(), t
					.getCharPositionInLine()
					+ t.getText().length());
		if (!VariableDataElement.class.isInstance(de)
				|| (((VariableDataElement) de).getType().type != DType.List && ((VariableDataElement) de).getType().type != DType.Map))
			throw new ScriptException("Invalid data element " + vname + " at line " + t.getLine(), t.getLine(), t.getCharPositionInLine(), t
					.getCharPositionInLine()
					+ t.getText().length());
		vde = (VariableDataElement) de;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void execute() throws ScriptException
	{
		Object o = vde.getValue(null);
		if (List.class.isInstance(o))
		{
			List<Object> olist = (List<Object>) o;
			script.curMemory -= vde.getType().getInitializedSize() * olist.size();
			olist.clear();
		} else if (Map.class.isInstance(o))
		{
			Map<Object, Object> omap = (Map<Object, Object>) o;
			script.curMemory -= vde.getType().getInitializedSize() * omap.size();
			omap.clear();
		}
	}
}
