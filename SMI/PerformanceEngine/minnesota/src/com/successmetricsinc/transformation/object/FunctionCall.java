package com.successmetricsinc.transformation.object;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.transformation.Expression;
import com.successmetricsinc.transformation.Function;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.TransformationScript;

public class FunctionCall extends Operator
{
	Tree t;
	TransformationScript script;
	Function f;
	List<Operator> values = new ArrayList<Operator>();

	public FunctionCall(TransformationScript script, Tree t) throws ScriptException
	{
		super(script, t);
		this.t = t;
		this.script = script;
		f = script.getFunction(t.getChild(0).getText());
		if (f == null)
			throw new ScriptException("Unable to find function " + t.getChild(0).getText() + ": line " + t.getLine() + ", ", t.getLine(),
					t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		Expression ex = new Expression(script);
		for (int i = 1; i < t.getChildCount(); i++)
		{
			Operator op = ex.compile(t.getChild(i));
			if (Expression.getOperatorType(op.getType()).type != f.getParameterType(i - 1).type)
			{
				throw new ScriptException("Incompatible types : line " + t.getLine() + ", ", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine()
						+ t.getText().length());
			}
			values.add(op);
		}
	}

	@Override
	public Operator clone()
	{
		try
		{
			return new FunctionCall(script, t);
		} catch (ScriptException e)
		{
			return null;
		}
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		try
		{
			Object aval = evaluate();
			Object bval = b.evaluate();
			if (aval == null && bval == null)
				return 0;
			if (bval == null)
				return -1;
			if (aval == null)
				return 1;
			if (aval.getClass() != bval.getClass())
				throw new ScriptException("Incompatible types: ", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
			if (Integer.class.isInstance(aval))
				return ((Integer) aval).compareTo((Integer) bval);
			else if (Long.class.isInstance(aval))
				return ((Long) aval).compareTo((Long) bval);
			else if (Double.class.isInstance(aval))
				return ((Double) aval).compareTo((Double) bval);
			else if (Boolean.class.isInstance(aval))
				return ((Boolean) aval).compareTo((Boolean) bval);
			else if (String.class.isInstance(aval))
				return ((String) aval).compareTo((String) bval);
			else if (Date.class.isInstance(aval))
				return ((Date) aval).compareTo((Date) bval);
			else if (Calendar.class.isInstance(aval))
				return ((Calendar) aval).compareTo((Calendar) bval);
			return 0;
		} catch (Exception e)
		{
			if (e instanceof ScriptException)
				throw (ScriptException) e;
			throw new ScriptException("Bad data type: ", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		try
		{
			Object aval = evaluate();
			Object bval = b.evaluate();
			if (aval.getClass() != bval.getClass())
				throw new ScriptException("Incompatible types: ", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
			if (Integer.class.isInstance(aval))
				return ((Integer) aval).equals((Integer) bval);
			else if (Long.class.isInstance(aval))
				return ((Long) aval).equals((Long) bval);
			else if (Double.class.isInstance(aval))
				return ((Double) aval).equals((Double) bval);
			else if (Boolean.class.isInstance(aval))
				return ((Boolean) aval).equals((Boolean) bval);
			else if (String.class.isInstance(aval))
				return ((String) aval).equals((String) bval);
			else if (Date.class.isInstance(aval))
				return ((Date) aval).equals((Date) bval);
			else if (Calendar.class.isInstance(aval))
				return ((Calendar) aval).equals((Calendar) bval);
			
			return false;
		} catch (Exception e)
		{
			throw new ScriptException("Bad data type", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		for (int i = 0; i < values.size(); i++)
		{
			f.setParameterValue(i, values.get(i).evaluate());
		}
		return f.evaluate();
	}

	@Override
	public int getSize()
	{
		return f.getReturnSize();
	}

	@Override
	public Type getType()
	{
		return f.getReturnType();
	}
}
