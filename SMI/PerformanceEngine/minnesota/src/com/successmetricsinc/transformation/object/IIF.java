package com.successmetricsinc.transformation.object;

import java.util.Calendar;
import java.util.Date;

import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.transformation.BirstScriptParser;
import com.successmetricsinc.transformation.DataType;
import com.successmetricsinc.transformation.Expression;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.Statement;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.transformation.DataType.DType;
import com.successmetricsinc.transformation.operators.OpBoolean;
import com.successmetricsinc.transformation.operators.OpUnaryFloat;

public class IIF extends Operator
{
	private Tree t;
	private TransformationScript script;
	private Operator booleanExpression;
	private Operator trueExpression;
	private Operator falseExpression;

	public IIF(TransformationScript script, Tree t) throws ScriptException
	{
		super(script, t);
		this.t = t;
		this.script = script;
		Tree expression = t.getChild(0);
		DataType type = Expression.getType(script, expression);
		if (type == null)
			throw new ScriptException("Unable to evaluate expression in IIF: " + Statement.toString(expression, null), expression.getLine(),
					expression.getCharPositionInLine(), expression.getCharPositionInLine() + expression.getText().length());
		if (type.type != DType.Boolean && type.type != DType.Integer && type.type != DType.Float)
			throw new ScriptException("IIF expression does not return a valid data type: " + Statement.toString(expression, null), expression.getLine(),
					expression.getCharPositionInLine(), expression.getCharPositionInLine() + expression.getText().length());
		Expression ex = new Expression(script);
		booleanExpression = ex.compile(expression);
		ex = new Expression(script);
		trueExpression = ex.compile(t.getChild(1));
		ex = new Expression(script);
		falseExpression = ex.compile(t.getChild(2));
		if (trueExpression != null && falseExpression != null)
		{
			if (trueExpression.getType() == Type.Integer && falseExpression.getType() == Type.Float)
				trueExpression = new OpUnaryFloat(trueExpression, BirstScriptParser.FLOATTYPE);
			if (falseExpression.getType() == Type.Integer && trueExpression.getType() == Type.Float)
				falseExpression = new OpUnaryFloat(falseExpression, BirstScriptParser.FLOATTYPE);
		}
	}

	@Override
	public Operator clone()
	{
		try
		{
			return new IIF(script, t);
		} catch (ScriptException e)
		{
			return null;
		}
	}

	// XXX need to determine why anything other than the base objects have compareTo and equals operators, seems odd and
	// introduces lots of
	// extra code and unexpected issues
	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		try
		{
			Object aval = evaluate();
			Object bval = b.evaluate();
			if (aval == null && bval != null)
				return -1;
			if (bval == null && aval != null)
				return 1;
			if ((aval!=null && bval!=null) && (aval.getClass() != bval.getClass()))
				if (Number.class.isInstance(aval) && Number.class.isInstance(bval))
					return ((Double) ((Number) aval).doubleValue()).compareTo(((Number) bval).doubleValue());
				else
					throw new ScriptException("Result of IFF compared against incorrect data type: " + aval.getClass().getName() + ", "
							+ bval.getClass().getName() + ", " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine()
							+ t.getText().length());
			if (Integer.class.isInstance(aval))
				return ((Integer) aval).compareTo((Integer) bval);
			else if (Long.class.isInstance(aval))
				return ((Long) aval).compareTo((Long) bval);
			else if (Double.class.isInstance(aval))
				return ((Double) aval).compareTo((Double) bval);
			else if (Boolean.class.isInstance(aval))
				return ((Boolean) aval).compareTo((Boolean) bval);
			else if (String.class.isInstance(aval))
				return ((String) aval).compareTo((String) bval);
			else if (Date.class.isInstance(aval))
				return ((Date) aval).compareTo((Date) bval);
			else if (Calendar.class.isInstance(aval))
				return ((Calendar) aval).compareTo((Calendar) bval);
			
			return 0;
		} catch (Exception e)
		{
			if (e instanceof ScriptException)
				throw (ScriptException) e;
			throw new ScriptException("Exception thrown in IIF compareTo: " + e.getMessage() + ", " + Statement.toString(t, null), t.getLine(),
					t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		try
		{
			Object aval = evaluate();
			Object bval = b.evaluate();
			if (aval.getClass() != bval.getClass())
				if (Number.class.isInstance(aval) && Number.class.isInstance(bval))
					return ((Double) ((Number) aval).doubleValue()).equals(((Number) bval).doubleValue());
				else
					throw new ScriptException("Result of IIF compared against incorrect data type: " + aval.getClass().getName() + ", "
							+ bval.getClass().getName() + ", " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine()
							+ t.getText().length());
			if (Integer.class.isInstance(aval))
				return ((Integer) aval).equals((Integer) bval);
			else if (Long.class.isInstance(aval))
				return ((Long) aval).equals((Long) bval);
			else if (Double.class.isInstance(aval))
				return ((Double) aval).equals((Double) bval);
			else if (Boolean.class.isInstance(aval))
				return ((Boolean) aval).equals((Boolean) bval);
			else if (String.class.isInstance(aval))
				return ((String) aval).equals((String) bval);
			else if (Date.class.isInstance(aval))
				return ((Date) aval).equals((Date) bval);
			else if (Calendar.class.isInstance(aval))
				return ((Calendar) aval).equals((Calendar) bval);
			
			return false;
		} catch (Exception e)
		{
			if (e instanceof ScriptException)
				throw (ScriptException) e;
			throw new ScriptException("Exception thrown in IIF equals: " + e.getMessage() + ", " + Statement.toString(t, null), t.getLine(),
					t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		Object oboolean = booleanExpression.evaluate();
		if (OpBoolean.isTrue(oboolean))
		{
			return trueExpression == null ? null : trueExpression.evaluate();
		} else
		{
			return falseExpression == null ? null : falseExpression.evaluate();
		}
	}

	public boolean returnsMeasure()
	{
		if (trueExpression != null)
			return internalReturnsMeasure(trueExpression);
		if (falseExpression != null)
			return internalReturnsMeasure(falseExpression);
		return false;
	}

	private boolean internalReturnsMeasure(Operator expression)
	{
		if (expression != null)
		{
			if (expression instanceof SetLookup)
				return ((SetLookup) expression).returnsMeasure();
			if (expression instanceof IIF)
				return ((IIF) expression).returnsMeasure();
		}
		return false;
	}

	@Override
	public int getSize()
	{
		if (trueExpression != null)
			return trueExpression.getSize();
		if (falseExpression != null)
			return falseExpression.getSize();
		return 0;
	}

	@Override
	public Type getType()
	{
		if (trueExpression != null)
			return trueExpression.getType();
		if (falseExpression != null)
			return falseExpression.getType();
		return Type.Varchar; // XXX hack, should throw
	}
}
