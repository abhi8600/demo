package com.successmetricsinc.transformation.object;

import java.util.Calendar;
import java.util.Date;

import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.transformation.Expression;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.Statement;
import com.successmetricsinc.transformation.TransformationScript;

public class Ifnull extends Operator
{
	private Tree t;
	private TransformationScript script;
	private Operator baseExpression;
	private Operator nullExpression;

	public Ifnull(TransformationScript script, Tree t) throws ScriptException
	{
		super(script, t);
		this.t = t;
		this.script = script;
		Tree expression = t.getChild(0);
		Expression ex = new Expression(script);
		baseExpression = ex.compile(expression);
		ex = new Expression(script);
		nullExpression = ex.compile(t.getChild(1));
		if (baseExpression == null)
			throw new ScriptException("The first argument to IFNULL can not be null: " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(),
					t.getCharPositionInLine() + t.getText().length());
		if (nullExpression == null)
			throw new ScriptException("The second argument to IFNULL can not be null: " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(),
					t.getCharPositionInLine() + t.getText().length());
		if (baseExpression.getType() != nullExpression.getType())
		{
			throw new ScriptException("The types of the expressions in the IFNULL must be the same: " + baseExpression.getType().toString() + "!="
					+ nullExpression.getType().toString() + ", " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(),
					t.getCharPositionInLine() + t.getText().length());
		}
	}

	@Override
	public Operator clone()
	{
		try
		{
			return new Ifnull(script, t);
		} catch (ScriptException e)
		{
			return null;
		}
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		try
		{
			Object aval = evaluate();
			Object bval = b.evaluate();
			if (aval.getClass() != bval.getClass())
				if (Number.class.isInstance(aval) && Number.class.isInstance(bval))
					return ((Double) ((Number) aval).doubleValue()).compareTo(((Number) bval).doubleValue());
				else
					throw new ScriptException("Result of IFNULL compared against incorrect data type: " + aval.getClass().getName() + ", "
							+ bval.getClass().getName() + ", " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine()
							+ t.getText().length());
			if (Integer.class.isInstance(aval))
				return ((Integer) aval).compareTo((Integer) bval);
			else if (Long.class.isInstance(aval))
				return ((Long) aval).compareTo((Long) bval);
			else if (Double.class.isInstance(aval))
				return ((Double) aval).compareTo((Double) bval);
			else if (Boolean.class.isInstance(aval))
				return ((Boolean) aval).compareTo((Boolean) bval);
			else if (String.class.isInstance(aval))
				return ((String) aval).compareTo((String) bval);
			else if (Date.class.isInstance(aval))
				return ((Date) aval).compareTo((Date) bval);
			else if (Calendar.class.isInstance(aval))
				return ((Calendar) aval).compareTo((Calendar) bval);
			
			return 0;
		} catch (Exception e)
		{
			if (e instanceof ScriptException)
				throw (ScriptException) e;
			throw new ScriptException("Exception thrown in IFNULL compareTo: " + e.getMessage() + ", " + Statement.toString(t, null), t.getLine(),
					t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		try
		{
			Object aval = evaluate();
			Object bval = b.evaluate();
			if (aval.getClass() != bval.getClass())
				if (Number.class.isInstance(aval) && Number.class.isInstance(bval))
					return ((Double) ((Number) aval).doubleValue()).equals(((Number) bval).doubleValue());
				else
					throw new ScriptException("Result of IFNULL compared against incorrect data type: " + aval.getClass().getName() + ", "
							+ bval.getClass().getName() + ", " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine()
							+ t.getText().length());
			if (Integer.class.isInstance(aval))
				return ((Integer) aval).equals((Integer) bval);
			else if (Long.class.isInstance(aval))
				return ((Long) aval).equals((Long) bval);
			else if (Double.class.isInstance(aval))
				return ((Double) aval).equals((Double) bval);
			else if (Boolean.class.isInstance(aval))
				return ((Boolean) aval).equals((Boolean) bval);
			else if (String.class.isInstance(aval))
				return ((String) aval).equals((String) bval);
			else if (Date.class.isInstance(aval))
				return ((Date) aval).equals((Date) bval);
			else if (Calendar.class.isInstance(aval))
				return ((Calendar) aval).equals((Calendar) bval);
			
			return false;
		} catch (Exception e)
		{
			if (e instanceof ScriptException)
				throw (ScriptException) e;
			throw new ScriptException("Exception thrown in IFNULL equals: " + e.getMessage() + ", " + Statement.toString(t, null), t.getLine(),
					t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		Object base = baseExpression.evaluate();
		if (base == null)
		{
			return nullExpression.evaluate();
		}
		return base;
	}

	@Override
	public int getSize()
	{
		return baseExpression.getSize();
	}

	@Override
	public Type getType()
	{
		return baseExpression.getType();
	}
}
