package com.successmetricsinc.transformation.object;

import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicInteger;

import org.antlr.runtime.tree.Tree;
import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.query.BadColumnNameException;
import com.successmetricsinc.query.DisplayExpression;
import com.successmetricsinc.query.LogicalQueryString;
import com.successmetricsinc.query.NavigationException;
import com.successmetricsinc.query.Query;
import com.successmetricsinc.query.QueryResultSet;
import com.successmetricsinc.query.ResultSetCache;
import com.successmetricsinc.query.SyntaxErrorException;
import com.successmetricsinc.transformation.BirstScriptParser;
import com.successmetricsinc.transformation.DataType;
import com.successmetricsinc.transformation.DataType.DType;
import com.successmetricsinc.transformation.Expression;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.Statement;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.util.BaseException;

public class Trend extends Operator
{
	private static Logger logger = Logger.getLogger(Trend.class);
	private static int MAX_QUERY_TIME_SECONDS = 60 * 3;
	private Tree t;
	private TransformationScript script;
	private boolean invalid = false;
	private int knownYindex;
	private int knownXindex;
	private LogicalQueryString lqs;
	private Query q;
	private QueryResultSet qrs;
	private String querykey;
	private double[] knownY;
	private double[] knownX;
	private Operator newXLookupExpression;
	private double a;
	private double b;

	public Trend(TransformationScript script, Tree t) throws ScriptException
	{
		super(script, t);
		this.t = t;
		this.script = script;
		Expression ex = null;
		Tree expression = null;
		DataType type = null;
		if (t.getChild(0).getType() != BirstScriptParser.INTEGER)
			throw new ScriptException("Cannot process trend, known y's column index must be an integer", t.getLine(), t.getCharPositionInLine(),
					t.getCharPositionInLine() + t.getText().length());
		knownYindex = Integer.valueOf(t.getChild(0).getText());
		if (t.getChild(1).getType() != BirstScriptParser.INTEGER)
			throw new ScriptException("Cannot process trend, known x's column index must be an integer", t.getLine(), t.getCharPositionInLine(),
					t.getCharPositionInLine() + t.getText().length());
		knownXindex = Integer.valueOf(t.getChild(1).getText());
		expression = t.getChild(2);
		type = Expression.getLogicalQueryType(script, expression);
		if (type == null)
			throw new ScriptException("Unable to evaluate expression " + expression.getText() + ": line " + expression.getLine() + ", ", expression.getLine(),
					expression.getCharPositionInLine(), expression.getCharPositionInLine() + expression.getText().length());
		ex = new Expression(script);
		newXLookupExpression = ex.compile(expression);
		Tree logicalQuery = t.getChild(3);
		lqs = new LogicalQueryString(script.getRepository(), script.getSession(), logicalQuery);
		try
		{
			generateQuery();
		} catch (Exception e)
		{
			logger.debug(Statement.toString(logicalQuery, null));
			throw new ScriptException("Unable to evaluate expression: " + e.getMessage(), t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine()
					+ t.getText().length());
		}
	}

	@Override
	public Operator clone()
	{
		try
		{
			return new Trend(script, t);
		} catch (ScriptException e)
		{
			return null;
		}
	}

	private void generateQuery() throws BaseException, SQLException, IOException
	{
		try
		{
			lqs.clear();
			q = lqs.getQuery();
			q.generateQuery(script.getSession(), true);
		} catch (CloneNotSupportedException e)
		{
			throw new ScriptException("Cannot process trend", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		} catch (SyntaxErrorException e)
		{
			throw new ScriptException("Cannot process trend, syntax error: " + e.getMessage(), t.getLine(), t.getCharPositionInLine(),
					t.getCharPositionInLine() + t.getText().length());
		} catch (NavigationException e)
		{
			throw new ScriptException("Cannot process trend, unable to navigate query: " + e.getMessage(), t.getLine(), t.getCharPositionInLine(),
					t.getCharPositionInLine() + t.getText().length());
		} catch (BadColumnNameException e)
		{
			throw new ScriptException("Cannot process trend, bad column name: " + e.getMessage(), t.getLine(), t.getCharPositionInLine(),
					t.getCharPositionInLine() + t.getText().length());
		}
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		if (q == null || invalid)
			return null;
		DisplayExpression de = script.getDisplayExpression();
		ResultSetCache rsc = null;
		if (de != null)
			rsc = de.getResultSetCache();
		Repository r = script.getRepository();
		if (rsc == null)
		{
			try
			{
				rsc = ResultSetCache.getInstance(r.getApplicationIdentifier(), false);
			} catch (Exception ex)
			{
				logger.error(ex.getMessage(), ex);
				return null;
			}
		}
		int maxQueryTime = r.getServerParameters().getMaxQueryTime();
		maxQueryTime = Math.min(maxQueryTime, MAX_QUERY_TIME_SECONDS);
		try
		{
			Session session = q.getSession();
			if (qrs == null) // make sure to populate known Y's and X's only once from given logical query
			{
				String qkey = q.getKey(session);
				if (session != null && (querykey == null || !qkey.equals(querykey)))
				{
					/*
					 * Synchronize to make sure that we don't issue the same query more than once
					 */
					AtomicInteger synch = session.getQuerySynchronization(qkey);
					synchronized (synch)
					{
						qrs = rsc.getQueryResultSet(q, r.getServerParameters().getMaxRecords(), maxQueryTime, lqs.getDisplayFilters(), lqs.getExpressionList(),
								lqs.getDisplayOrderList(), script.getSession(), false, r.getServerParameters().getDisplayTimeZone(), false);
					}
					session.removeSynchronization(qkey);
					querykey = qkey;
				} else
				{
					qrs = rsc.getQueryResultSet(q, r.getServerParameters().getMaxRecords(), maxQueryTime, lqs.getDisplayFilters(), lqs.getExpressionList(),
							lqs.getDisplayOrderList(), script.getSession(), false, r.getServerParameters().getDisplayTimeZone(), false);
				}
				int numColumns = qrs.getColumnNames().length;
				if (knownYindex < numColumns && knownXindex < numColumns)
				{
					if (knownY == null && knownX == null)
					{
						Object[][] rows = qrs.getRows();
						int n = rows.length;
						knownY = new double[n];
						knownX = new double[n];
						double sumX = 0, sumY = 0, sumXX = 0, sumYY = 0, sumXY = 0;
						for (int i = 0; i < rows.length; i++)
						{
							double y = 0;
							double x = 0;
							if (rows[i][knownYindex] != null)
							{
								if (rows[i][knownYindex] instanceof Integer)
									y = (Integer) rows[i][knownYindex];
								else if (rows[i][knownYindex] instanceof Long)  
								 	y = (Long) rows[i][knownYindex];  
								else if (rows[i][knownYindex] instanceof Double)
									y = (Double) rows[i][knownYindex];
								knownY[i] = y;
								sumY += y;
								sumYY += y * y;
							}
							if (rows[i][knownXindex] != null)
							{
								if (rows[i][knownXindex] instanceof Integer)
									x = (Integer) rows[i][knownXindex];
								else if (rows[i][knownXindex] instanceof Long)  
								 	x = (Long) rows[i][knownXindex];
								else if (rows[i][knownXindex] instanceof Double)
									x = (Double) rows[i][knownXindex];
								knownX[i] = x;
								sumX += x;
								sumXX += x * x;
							}
							sumXY += x * y;
						}
						double meanX = sumX / n, meanY = sumY / n;
						double sumDevX = 0, sumDevY = 0, sumDevXX = 0, sumDevYY = 0, sumDevXY = 0;
						if (knownY != null && knownX != null)
						{
							for (int i = 0; i < knownY.length; i++)
							{
								double devX = knownX[i] - meanX;
								double devY = knownY[i] - meanY;
								sumDevX += devX;
								sumDevY += devY;
								sumDevXX += devX * devX;
								sumDevYY += devY * devY;
								sumDevXY += devX * devY;
							}
							// now calculate constant values a and b
							a = sumDevXY / sumDevXX;
							b = meanY - a * meanX;
						}
					}
				}
			}
			if (knownY == null || knownX == null)
				return null;
			Object o = newXLookupExpression.evaluate();
			if (o == null)
				return null;
			if (o instanceof Integer)
				return a * (Integer) o + b;
			else if (o instanceof Long)  
			 	return a * (Long) o + b; 
			else if (o instanceof Double)
				return a * (Double) o + b;
		} catch (Exception e)
		{
			invalid = true;
			logger.debug("marking trend logical query as invalid: " + e.getMessage(), e);
			throw new ScriptException("Cannot process trend query: " + e.getMessage(), t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine()
					+ t.getText().length());
		}
		return null;
	}

	@Override
	public Type getType()
	{
		return Operator.Type.Float;
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		try
		{
			Object aval = evaluate();
			Object bval = b.evaluate();
			if (aval.getClass() != bval.getClass())
				throw new ScriptException("Bad data type: ", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
			if (Integer.class.isInstance(aval))
				return ((Integer) aval).equals((Integer) bval);
			else if (Double.class.isInstance(aval))
				return ((Double) aval).equals((Double) bval);
			else if (Long.class.isInstance(aval))
				return ((Long) aval).equals((Long) bval);
			return false;
		} catch (Exception e)
		{
			throw new ScriptException("Bad data type", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		try
		{
			Object aval = evaluate();
			Object bval = b.evaluate();
			if (aval == null && bval == null)
				return 0;
			if (bval == null)
				return -1;
			if (aval == null)
				return 1;
			if (aval.getClass() != bval.getClass())
				throw new ScriptException("Bad data type: ", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
			if (Integer.class.isInstance(aval))
				return ((Integer) aval).compareTo((Integer) bval);
			else if (Double.class.isInstance(aval))
				return ((Double) aval).compareTo((Double) bval);
			else if (Long.class.isInstance(aval))
				return ((Long) aval).compareTo((Long) bval);
			return 0;
		} catch (Exception e)
		{
			if (e instanceof ScriptException)
				throw (ScriptException) e;
			throw new ScriptException("Bad data type: ", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
	}

	@Override
	public int getSize()
	{
		return Double.SIZE / 8;
	}

	public DataType getDataType()
	{
		return new DataType(DType.Float, 0);
	}
}
