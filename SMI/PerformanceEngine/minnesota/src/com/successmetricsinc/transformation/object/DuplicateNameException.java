/**
 * 
 */
package com.successmetricsinc.transformation.object;

/**
 * @author agarrison
 *
 */
public class DuplicateNameException extends Exception {

	public DuplicateNameException(String msg) {
		super(msg);
	}
}
