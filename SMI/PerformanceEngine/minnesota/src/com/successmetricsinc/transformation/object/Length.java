package com.successmetricsinc.transformation.object;

import java.util.List;
import java.util.Map;

import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.transformation.BirstScriptParser;
import com.successmetricsinc.transformation.DataElement;
import com.successmetricsinc.transformation.Expression;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.Statement;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.transformation.VariableDataElement;
import com.successmetricsinc.transformation.DataType.DType;

public class Length extends Operator
{
	private Operator stringValue;
	private VariableDataElement vde;
	private Tree t;
	private TransformationScript script;

	public Length(TransformationScript script, Tree t) throws ScriptException
	{
		super(script, t);
		this.t = t;
		this.script = script;
		if (t.getChild(0).getType() == BirstScriptParser.IDENT_NAME)
		{
			String vname = t.getChild(0).getText();
			DataElement de = script.findElement(vname);
			// See if it's array length (if not then string length)
			if (de == null)
				throw new ScriptException("Unknown variable " + vname + ": " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(),
						t.getCharPositionInLine() + t.getText().length());
			if (VariableDataElement.class.isInstance(de)
					&& (((VariableDataElement) de).getType().type == DType.List || ((VariableDataElement) de).getType().type == DType.Map))
			{
				vde = (VariableDataElement) de;
				return;
			}
		}
		Expression ex = new Expression(script);
		stringValue = ex.compile(t.getChild(0));
		if (stringValue.getType() != Operator.Type.Varchar)
			throw new ScriptException("Length may only be applied to varchar data elements, lists or maps: " + Statement.toString(t, null), t.getLine(),
					t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
	}

	@Override
	public Operator clone()
	{
		try
		{
			return new Length(script, t);
		} catch (ScriptException e)
		{
			return null;
		}
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		try
		{
			return ((Long) evaluate()).compareTo((Long) b.evaluate());
		} catch (Exception e)
		{
			if (e instanceof ScriptException)
				throw (ScriptException) e;
			throw new ScriptException("Bad data type: ", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		try
		{
			return ((Long) evaluate()).equals((Long) b.evaluate());
		} catch (Exception e)
		{
			throw new ScriptException("Bad data type", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object evaluate() throws ScriptException
	{
		// If array length
		if (vde != null)
		{
			if (vde.getType().type == DType.List)
			{
				List<Object> olist = (List<Object>) vde.getValue(null);
				return ((Integer) olist.size()).longValue();
			} else if (vde.getType().type == DType.Map)
			{
				Map<Object, Object> omap = (Map<Object, Object>) vde.getValue(null);
				return ((Integer) omap.size()).longValue();
			}
		}
		// Otherwise string length
		Object o = stringValue.evaluate();
		if (o == null)
			throw new ScriptException("Cannot use Length on null values: " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(),
					t.getCharPositionInLine() + t.getText().length());
		return ((Integer) o.toString().length()).longValue();
	}

	@Override
	public int getSize()
	{
		return Integer.SIZE / 8;
	}

	@Override
	public Type getType()
	{
		return Operator.Type.Integer;
	}
}
