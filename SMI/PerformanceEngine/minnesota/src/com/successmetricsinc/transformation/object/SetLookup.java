/**
 * $Id: SetLookup.java,v 1.62 2012-11-16 00:59:49 birst\bpeters Exp $
 *
 * Copyright (C) 2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.transformation.object;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.antlr.runtime.CommonToken;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.Tree;
import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.query.BadColumnNameException;
import com.successmetricsinc.query.DimensionColumn;
import com.successmetricsinc.query.DimensionColumnNav;
import com.successmetricsinc.query.DisplayExpression;
import com.successmetricsinc.query.DisplayFilter;
import com.successmetricsinc.query.LogicalQueryString;
import com.successmetricsinc.query.MeasureColumn;
import com.successmetricsinc.query.MeasureColumnNav;
import com.successmetricsinc.query.NavigationException;
import com.successmetricsinc.query.Query;
import com.successmetricsinc.query.QueryResultSet;
import com.successmetricsinc.query.ResultSetCache;
import com.successmetricsinc.query.SyntaxErrorException;
import com.successmetricsinc.transformation.BirstScriptParser;
import com.successmetricsinc.transformation.ColData;
import com.successmetricsinc.transformation.DataType;
import com.successmetricsinc.transformation.DataType.DType;
import com.successmetricsinc.transformation.Expression;
import com.successmetricsinc.transformation.InputDataElement;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.OutputDataElement;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.Statement;
import com.successmetricsinc.transformation.StatementBlock;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.warehouse.StagingTable;

public class SetLookup extends Operator
{
	private static Logger logger = Logger.getLogger(SetLookup.class);
	private static int MAX_QUERY_TIME_SECONDS = 15 * 60; // 15 minutes (way too long)
	private static int MAX_ERRORS = 10;
	private Tree t;
	private TransformationScript script;
	private TransformationScript functionScript;
	private Operator lookupExpression;
	private StatementBlock mainBlock;
	private List<Operator> parameterExpressions = new ArrayList<Operator>();
	private LogicalQueryString lqs;
	private Query q;
	private int lookupIndex = -1;
	private int resultIndex = -1;
	private QueryResultSet qrs;
	private boolean invalid;
	private Tree logicalQuery;
	private boolean exact;
	private boolean useRow;
	private boolean isStatistic;
	private boolean isFunction;
	private Tree statType;
	private long cumTime = 0;
	private long maxTime = 0;
	private long maxErrors = 0;
	private Double statisticValue;
	private String querykey;	
	private int numInputRows;
	private int maxInputRows;
	List<InputDataElement> functionInputElements;
	List<InputDataElement> parentInputElements;
	private List<Operator.Type> columnTypeList = new ArrayList<Operator.Type>();
	private List<String> columnNameList = new ArrayList<String>();
	private int[] statLookupIndices;

	public SetLookup(TransformationScript script, Tree t, boolean exact, boolean useRow, boolean isStatistic, boolean isFunction) throws ScriptException
	{
		super(script, t);
		this.t = t;
		this.script = script;
		this.exact = exact;
		this.useRow = useRow;
		this.isStatistic = isStatistic;
		this.isFunction = isFunction;
		int start = 0;
		int numChildren = t.getChildCount();
		Expression ex = null;
		Tree expression = null;
		DataType type = null;
		if (isStatistic)
		{
			start = 2;
			this.statType = t.getChild(0);
			if (t.getChild(1).getType() != BirstScriptParser.INTEGER)
				throw new ScriptException("Cannot process " + t.getText() + ", lookup column index must be an integer: "
						+ Statement.toString(t.getChild(1), null), t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
			lookupIndex = Integer.valueOf(t.getChild(1).getText());
		} else if (isFunction)
		{
			type = script.getType(t.getChild(0));
			functionScript = new TransformationScript(script.getRepository(), script.getSession());
			functionScript.initializeScriptResults();
			OutputDataElement ode = new OutputDataElement();
			ode.setName("[Result]");
			ode.setType(type);
			functionScript.addOutputDataElement(ode);
			start = 2;
			maxInputRows = script.getRepository().getServerParameters().getMaxRecords();
			numInputRows = 0;
			functionScript.setLogicalQuery(false);
			functionInputElements = new ArrayList<InputDataElement>();
			parentInputElements = new ArrayList<InputDataElement>();
			if (exact)
			{
				start += 2;
				if (t.getChild(1).getType() != BirstScriptParser.INTEGER)
					throw new ScriptException("Cannot process " + t.getText() + ", lookup column index must be an integer: "
							+ Statement.toString(t.getChild(1), null), t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
				lookupIndex = Integer.valueOf(t.getChild(1).getText());
				expression = t.getChild(2);
				type = Expression.getLogicalQueryType(script, expression);
				if (type == null)
					throw new ScriptException("Unable to evaluate " + t.getText() + " expression " + expression.getText() + ": line " + expression.getLine()
							+ ", ", expression.getLine(), expression.getCharPositionInLine(), expression.getCharPositionInLine()
							+ expression.getText().length());
				ex = new Expression(script);
				lookupExpression = ex.compile(expression);
			}
		} else
		{
			start = useRow ? 2 : 3;
			if (!useRow)
			{
				if (t.getChild(0).getType() != BirstScriptParser.INTEGER)
					throw new ScriptException("Cannot process " + t.getText() + ", lookup column index must be an integer: "
							+ Statement.toString(t.getChild(0), null), t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
				lookupIndex = Integer.valueOf(t.getChild(0).getText());
			}
			if (t.getChild(useRow ? 1 : 2).getType() != BirstScriptParser.INTEGER)
				throw new ScriptException("Cannot process " + t.getText() + ", result column index must be an integer: "
						+ Statement.toString(t.getChild(useRow ? 1 : 2), null), t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine()
						+ t.getText().length());
			resultIndex = Integer.valueOf(t.getChild(useRow ? 1 : 2).getText());
			expression = t.getChild(useRow ? 0 : 1);
			type = Expression.getLogicalQueryType(script, expression);
			if (type == null)
				throw new ScriptException(
						"Unable to evaluate " + t.getText() + " expression " + expression.getText() + ": line " + expression.getLine() + ", ",
						expression.getLine(), expression.getCharPositionInLine(), expression.getCharPositionInLine() + expression.getText().length());
			ex = new Expression(script);
			lookupExpression = ex.compile(expression);
		}
		/*
		 * handle case of 'alias' appearing in the list of children (XXX this is a bug at a higher level that we need to
		 * fix)
		 */
		for (int j = numChildren - 1; j > 0; j--)
		{
			Tree child = t.getChild(j);
			if (child.getType() == BirstScriptParser.SELECT)
			{
				logicalQuery = cloneTree(child);
				break;
			}
			numChildren--;
		}
		if (logicalQuery != null)
			lqs = new LogicalQueryString(script.getRepository(), script.getSession(), logicalQuery);
		List<String> dimensionParameters = new ArrayList<String>();
		// Get parameters
		for (int i = start; i < numChildren - 1; i++)
		{
			expression = t.getChild(i);
			type = Expression.getLogicalQueryType(script, expression);
			if (type == null)
				throw new ScriptException(
						"Unable to evaluate " + t.getText() + " expression " + expression.getText() + ": line " + expression.getLine() + ", ",
						expression.getLine(), expression.getCharPositionInLine(), expression.getCharPositionInLine() + expression.getText().length());
			ex = new Expression(script);
			if (expression.getType() == BirstScriptParser.COLUMN_NAME)
				dimensionParameters.add(expression.getText());
			parameterExpressions.add(ex.compile(expression));
		}
		if (lqs != null)
		{
			try
			{
				generateQuery(false);
				// populate column list
				setColumnList();
				if (isStatistic && dimensionParameters.size() > 0 && dimensionParameters.size() == parameterExpressions.size())
				{
					/*
					 * If all parameters are display wheres on dimension columns - convert into a lookup (much faster than
					 * running a query each time)
					 */
					Set<String> displayWhereEquals = new HashSet<String>();
					scanChildParameters(t, displayWhereEquals);
					if (displayWhereEquals.containsAll(dimensionParameters))
					{
						/*
						 * Remove parameters and convert to a lookup. Do this by removing he parameters and the display
						 * where and finding the lookup indices
						 */
						List<DisplayFilter> filterList = lqs.getDisplayFilters();
						List<DisplayFilter> newFilterList = new ArrayList<DisplayFilter>();
						for(DisplayFilter df: filterList)
						{
							if (df.getOperator() == DisplayFilter.Operator.Equal && df.getTableName() != null)
							{
								String colName = "["+df.getTableName()+"."+df.getColumn()+"]";
								if (dimensionParameters.contains(colName))
									continue;
							}
							newFilterList.add(df);
						}
						lqs.setDisplayFilters(newFilterList);
						statLookupIndices = new int[dimensionParameters.size()];
						for(int i = 0; i < statLookupIndices.length; i++)
						{
							statLookupIndices[i] = columnNameList.indexOf(dimensionParameters.get(i));
						}
					}
				}
			} catch (Exception e)
			{
				logger.debug(Statement.toString(logicalQuery, null));
				throw new ScriptException("Unable to evaluate " + t.getText() + " expression: " + e.getMessage(), t.getLine(), t.getCharPositionInLine(),
						t.getCharPositionInLine() + t.getText().length());
			}
		}
		maxTime = MAX_QUERY_TIME_SECONDS * 1000;
	}

	@Override
	public Operator clone()
	{
		try
		{
			return new SetLookup(script, t, exact, useRow, isStatistic, isFunction);
		} catch (ScriptException e)
		{
			return null;
		}
	}

	private void replaceParameters(boolean replace)
	{
		Tree newTree = cloneTree(logicalQuery);
		replaceChildParameters(newTree, replace);
		lqs.setQueryTree(newTree);
		return;
	}

	private Tree cloneTree(Tree t)
	{
		Tree newTree = t.dupNode();
		for (int i = 0; i < t.getChildCount(); i++)
		{
			newTree.addChild(cloneTree(t.getChild(i)));
		}
		return newTree;
	}

	private void replaceChildParameters(Tree tree, boolean replace)
	{
		if (tree.getType() == BirstScriptParser.PARAMETER)
		{
			for (int i = 0; i < parameterExpressions.size(); i++)
			{
				if (tree.getText().equals("%" + i))
				{
					String result = null;
					Type type = parameterExpressions.get(i).getType();
					Object o = null;
					try
					{
						o = parameterExpressions.get(i).evaluate();
					}
					catch (ScriptException e)
					{
						o = null;
					}
					CommonToken to = null;
					if (type == Operator.Type.Integer)
					{
						if (o != null && replace)
							result = o.toString();
						else
							result = "0";
						to = new CommonToken(BirstScriptParser.INTEGER);
					} else if (type == Operator.Type.Float)
					{
						if (o != null && replace)
							result = o.toString();
						else
							result = "0";
						to = new CommonToken(BirstScriptParser.FLOAT);
					} else if (type == Operator.Type.Boolean)
					{
						if (o != null && replace)
							result = o.toString();
						else
							result = "false";
						to = new CommonToken(BirstScriptParser.BOOLEAN);
					} else if (type == Operator.Type.Varchar)
					{
						if (o != null && replace)
							result = o.toString();
						else
							result = "";
						result = "'" + result + "'";
						to = new CommonToken(BirstScriptParser.STRING);
					} else if (type == Operator.Type.DateTime)
					{
							if (o != null && replace)
							{
								if (Calendar.class.isInstance(o))
								{
									SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
									result = df.format(((Calendar) o).getTime());
								} else if (Date.class.isInstance(o))
								{
									SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
									result = df.format(((Date) o));
								} else
									result = o.toString();
							} else
							{
								SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
								result = df.format(new Date());
							}
						result = "#" + result + "#";
						to = new CommonToken(BirstScriptParser.DATETIME);
					}
					if (to != null)
					{
						to.setText(result);
						CommonTree newTree = new CommonTree(to);
						tree.getParent().setChild(tree.getChildIndex(), newTree);
					}
				}
			}
		} else
		{
			for (int j = 0; j < tree.getChildCount(); j++)
				replaceChildParameters(tree.getChild(j), replace);
		}
	}

	private void scanChildParameters(Tree tree, Set<String> displayWhereEquals)
	{
		if (tree.getType() == BirstScriptParser.PARAMETER)
		{
			if (tree.getParent() != null && tree.getParent().getParent() != null && tree.getParent().getParent().getType() == BirstScriptParser.DISPLAYWHERE && tree.getParent().getType() == BirstScriptParser.EQUALS)
			{
				Tree col = tree.getParent().getChild(0);
				if (col.getType() == BirstScriptParser.COLUMN_NAME && !displayWhereEquals.contains(col.getText()))
					displayWhereEquals.add(col.getText());
			}
		} else
		{
			for (int j = 0; j < tree.getChildCount(); j++)
				scanChildParameters(tree.getChild(j), displayWhereEquals);
		}
	}
	
	private void generateQuery(boolean replace) throws BaseException, SQLException, IOException
	{
		try
		{
			if (parameterExpressions.size() > 0)
				replaceParameters(replace);
			lqs.clear();
			q = lqs.getQuery();
			List<String> addedNames = new ArrayList<String>();
			List<DisplayExpression> expressionList = lqs.getExpressionList();
			if (expressionList != null)
			{
				for (DisplayExpression de : expressionList)
				{
					de.extractAllExpressions(q, addedNames);
				}
			}
			q.generateQuery(script.getSession(), true);
		} catch (CloneNotSupportedException e)
		{
			throw new ScriptException("Cannot process " + t.getText(), t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		} catch (SyntaxErrorException e)
		{
			throw new ScriptException("Cannot process " + t.getText() + ", syntax error: " + e.getMessage(), t.getLine(), t.getCharPositionInLine(),
					t.getCharPositionInLine() + t.getText().length());
		} catch (NavigationException e)
		{
			throw new ScriptException("Cannot process " + t.getText() + ", unable to navigate query: " + e.getMessage(), t.getLine(),
					t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		} catch (BadColumnNameException e)
		{
			throw new ScriptException("Cannot process " + t.getText() + ", bad column name: " + e.getMessage(), t.getLine(), t.getCharPositionInLine(),
					t.getCharPositionInLine() + t.getText().length());
		}
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		try
		{
			Object aval = evaluate();
			Object bval = b.evaluate();
			if (aval == null && bval == null)
				return 0;
			if (bval == null)
				return -1;
			if (aval == null)
				return 1;
			if (aval.getClass() != bval.getClass())
				if (Number.class.isInstance(aval) && Number.class.isInstance(bval))
					return ((Double) ((Number) aval).doubleValue()).compareTo(((Number) bval).doubleValue());
				else
					throw new ScriptException("Bad data type: ", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
			if (Integer.class.isInstance(aval))
				return ((Integer) aval).compareTo((Integer) bval);
			else if (Long.class.isInstance(aval))
				return ((Long) aval).compareTo((Long) bval);
			else if (Double.class.isInstance(aval))
				return ((Double) aval).compareTo((Double) bval);
			else if (Boolean.class.isInstance(aval))
				return ((Boolean) aval).compareTo((Boolean) bval);
			else if (String.class.isInstance(aval))
				return ((String) aval).compareTo((String) bval);
			else if (Date.class.isInstance(aval))
				return ((Date) aval).compareTo((Date) bval);
			else if (Calendar.class.isInstance(aval))
				return ((Calendar) aval).compareTo((Calendar) bval);
			
			return 0;
		} catch (Exception e)
		{
			if (e instanceof ScriptException)
				throw (ScriptException) e;
			throw new ScriptException("Bad data type: ", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		try
		{
			Object aval = evaluate();
			Object bval = b.evaluate();
			if (aval.getClass() != bval.getClass())
				if (Number.class.isInstance(aval) && Number.class.isInstance(bval))
					return ((Double) ((Number) aval).doubleValue()).equals(((Number) bval).doubleValue());
				else
					throw new ScriptException("Bad data type: ", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
			if (Integer.class.isInstance(aval))
				return ((Integer) aval).equals((Integer) bval);
			else if (Long.class.isInstance(aval))
				return ((Long) aval).equals((Long) bval);
			else if (Double.class.isInstance(aval))
				return ((Double) aval).equals((Double) bval);
			else if (Boolean.class.isInstance(aval))
				return ((Boolean) aval).equals((Boolean) bval);
			else if (String.class.isInstance(aval))
				return ((String) aval).equals((String) bval);
			else if (Date.class.isInstance(aval))
				return ((Date) aval).equals((Date) bval);
			else if (Calendar.class.isInstance(aval))
				return ((Calendar) aval).equals((Calendar) bval);
			
			return false;
		} catch (Exception e)
		{
			throw new ScriptException("Bad data type", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
	}
	private Map<Object, Object> exactMap;
	private Map<Object, Object[][]> lookupMap;
	private Map<Object, List<Object[]>> lookupMapList;

	private Object evaluateStaticFunction() throws ScriptException
	{
		mainBlock = new StatementBlock(functionScript, t.getChild(1), new ArrayList<StagingTable>());
		mainBlock.execute();
		if (functionScript.onComplete != null)
			functionScript.onComplete.execute();
		Object result = functionScript.findElement("[Result]").getValue(null);
		return result;
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		if (q == null)
		{
			if (isFunction)
			{
				return evaluateStaticFunction();
			}
			if (maxErrors++ < MAX_ERRORS)
				logger.info("SetLookup evaluate failed because query was null, please check the log for a failure during SetLookup initialization");
			return null;
		}
		if (invalid)
		{
			if (maxErrors++ < MAX_ERRORS)
				logger.info("SetLookup evaluate failed because a previous evaluation of this SetLookup produced an exception, please check the log");
			return null;
		}
		Object o = isStatistic || (isFunction && !exact) ? null : lookupExpression.evaluate();
		DisplayExpression de = script.getDisplayExpression();
		ResultSetCache rsc = null;
		if (de != null)
			rsc = de.getResultSetCache();
		Repository r = script.getRepository();
		if (rsc == null)
		{
			try
			{
				rsc = ResultSetCache.getInstance(r.getApplicationIdentifier(), false);
			} catch (Exception ex)
			{
				if (maxErrors++ < MAX_ERRORS)
					logger.error(ex.getMessage(), ex);
				return null;
			}
		}
		int maxQueryTime = r.getServerParameters().getMaxQueryTime();
		maxQueryTime = Math.min(maxQueryTime, MAX_QUERY_TIME_SECONDS);
		try
		{
			Session session = q.getSession();
			// Prevent abuse
			if (cumTime > maxTime)
			{
				if (maxErrors++ < MAX_ERRORS)
					logger.info("SetLookup evaluate failed because the cumulative time was exhausted: " + cumTime + " > " + maxTime);
				return null;
			}
			boolean hasparams = parameterExpressions.size() > 0 && (statLookupIndices == null || statLookupIndices.length == 0);
			if (qrs == null || hasparams)
			{
				if (hasparams)
					generateQuery(true);
				long start = System.currentTimeMillis();
				String qkey = q.getKey(session);
				if (session != null && (querykey == null || !qkey.equals(querykey)))
				{
					/*
					 * Synchronize to make sure that we don't issue the same query more than once
					 */
					AtomicInteger synch = session.getQuerySynchronization(qkey);
					synchronized (synch)
					{
						qrs = rsc.getQueryResultSet(q, r.getServerParameters().getMaxRecords(), maxQueryTime, lqs.getDisplayFilters(), lqs.getExpressionList(),
								lqs.getDisplayOrderList(), script.getSession(), false, r.getServerParameters().getDisplayTimeZone(), false);
					}
					session.removeSynchronization(qkey);
					querykey = qkey;
				} else
				{
					qrs = rsc.getQueryResultSet(q, r.getServerParameters().getMaxRecords(), maxQueryTime, lqs.getDisplayFilters(), lqs.getExpressionList(),
							lqs.getDisplayOrderList(), script.getSession(), false, r.getServerParameters().getDisplayTimeZone(), false);
				}
				long stop = System.currentTimeMillis();
				cumTime += stop - start;
				if (isFunction && mainBlock == null)
				{
					for (int i = 0; i < qrs.getDisplayNames().length; i++)
					{
						InputDataElement ide = new InputDataElement();
						ColData cd = new ColData();
						int type = qrs.getColumnDataTypes()[i];
						switch (type)
						{
						case Types.INTEGER:
							cd.datatype = "Integer";
							ide.type = new DataType(DType.Integer, 0);
							break;
						case Types.DOUBLE:
							cd.datatype = "Float";
							ide.type = new DataType(DType.Float, 0);
							break;
						case Types.VARCHAR:
							cd.datatype = "Varchar";
							cd.width = 1024;
							ide.type = new DataType(DType.Varchar, 1024);
							break;
						case Types.BOOLEAN:
							cd.datatype = "Boolean";
							ide.type = new DataType(DType.Boolean, 0);
							break;
						case Types.TIMESTAMP:
							cd.datatype = "DateTime";
							ide.type = new DataType(DType.DateTime, 0);
							break;
						}
						cd.logicalColumnName = qrs.getDisplayNames()[i];
						cd.physicalColumnName = qrs.getColumnNames()[i];
						ide.name = '[' + cd.logicalColumnName + ']';
						ide.setColdata(cd);
						functionScript.addInputDataElement(ide);
						functionInputElements.add(ide);
					}
					QueryResultSet parentResultSet = script.getParentResultSet();
					if (parentResultSet != null)
					{
						for (int i = 0; i < parentResultSet.getDisplayNames().length; i++)
						{
							InputDataElement ide = new InputDataElement();
							ColData cd = new ColData();
							int type = parentResultSet.getColumnDataTypes()[i];
							switch (type)
							{
							case Types.INTEGER:
								cd.datatype = "Integer";
								ide.type = new DataType(DType.Integer, 0);
								break;
							case Types.DOUBLE:
								cd.datatype = "Float";
								ide.type = new DataType(DType.Float, 0);
								break;
							case Types.VARCHAR:
								cd.datatype = "Varchar";
								cd.width = 1024;
								ide.type = new DataType(DType.Varchar, 1024);
								break;
							case Types.BOOLEAN:
								cd.datatype = "Boolean";
								ide.type = new DataType(DType.Boolean, 0);
								break;
							case Types.TIMESTAMP:
								cd.datatype = "DateTime";
								ide.type = new DataType(DType.DateTime, 0);
								break;
							}
							cd.logicalColumnName = parentResultSet.getColumnNames()[i];
							cd.physicalColumnName = parentResultSet.getColumnNames()[i];
							ide.name = '[' + cd.logicalColumnName + ']';
							ide.setColdata(cd);
							functionScript.addInputDataElement(ide);
							parentInputElements.add(ide);
						}
					}
					List<StagingTable> scriptPath = new ArrayList<StagingTable>();
					mainBlock = new StatementBlock(functionScript, t.getChild(exact ? 3 : 1), scriptPath);
				}
			}
			int numColumns = qrs.getColumnNames().length;
			if (isStatistic)
			{
				if (parameterExpressions.isEmpty() && statisticValue != null)
					return statisticValue;
				Object[][] rows = qrs.getRows();
				if (statLookupIndices != null && statLookupIndices.length > 0 && !parameterExpressions.isEmpty())
				{
					// Cache lookup
					StringBuilder lookupkey = new StringBuilder();
					for(int j = 0; j < parameterExpressions.size(); j++)
					{
						lookupkey.append(parameterExpressions.get(j).evaluate()).append('|');
					}
					if (lookupMap == null)
					{
						Map<Object, List<Object[]>> lookupMapSetup = new HashMap<Object, List<Object[]>>(rows.length);
						for (int i = 0; i < rows.length; i++)
						{
							StringBuilder key = new StringBuilder();
							for (int j = 0; j < statLookupIndices.length; j++)
							{
								if (rows[i][statLookupIndices[j]] == null)
									key.append('|');
								else
									key.append(rows[i][statLookupIndices[j]]).append('|');
							}
							String keystring = key.toString();
							List<Object[]> list = lookupMapSetup.get(keystring);
							if (list == null)
							{
								list = new ArrayList<Object[]>();
								lookupMapSetup.put(keystring, list);
							}
							list.add(rows[i]);
						}
						lookupMap = new HashMap<Object, Object[][]>(lookupMapSetup.size());
						for(Entry<Object, List<Object[]>> entry: lookupMapSetup.entrySet())
						{
							List<Object[]> list = entry.getValue();
							Object[][] arr = new Object[list.size()][];
							list.toArray(arr);
							lookupMap.put(entry.getKey(), arr);
						}
					}
					rows = lookupMap.get(lookupkey.toString());
					if (rows == null)
						return null;
				}
				int stattype = statType.getType();
				if (stattype == BirstScriptParser.STATMEDIAN && lookupIndex < numColumns)
				{
					List<Double> vlist = new ArrayList<Double>();
					for (int i = 0; i < rows.length; i++)
					{
						if (Double.class.isInstance(rows[i][lookupIndex]))
						{
							vlist.add((Double) rows[i][lookupIndex]);
						} else if (Long.class.isInstance(rows[i][lookupIndex]))
						{
							vlist.add(((Long) rows[i][lookupIndex]).doubleValue());
						} else if (Integer.class.isInstance(rows[i][lookupIndex]))
						{
							vlist.add(((Integer) rows[i][lookupIndex]).doubleValue());
						}
					}
					Collections.sort(vlist);
					double result = 0;
					if (vlist.size() % 2 == 1)
					{
						result = vlist.get(vlist.size() / 2);
					} else if (vlist.size() > 0)
					{
						result = (vlist.get(vlist.size() / 2) + vlist.get(vlist.size() / 2 - 1)) / 2;
					}
					if (parameterExpressions.isEmpty())
						statisticValue = result;
					return result;
				} else if ((stattype == BirstScriptParser.SUM || stattype == BirstScriptParser.AVG || stattype == BirstScriptParser.COUNT
						|| stattype == BirstScriptParser.MIN || stattype == BirstScriptParser.MAX || stattype == BirstScriptParser.COUNTDISTINCT || stattype == BirstScriptParser.STATSTDDEV)
						&& lookupIndex < numColumns)
				{
					double sum = 0;
					double sumsq = 0;
					double min = Double.MAX_VALUE;
					double max = Double.MIN_VALUE;
					long suml = 0;
					long sumsql = 0;
					long minl = Long.MAX_VALUE;
					long maxl = Long.MIN_VALUE;
					int sumi = 0;
					int sumsqi = 0;
					int mini = Integer.MAX_VALUE;
					int maxi = Integer.MIN_VALUE;
					int count = 0;
					Set<Object> objs = null;
					if (stattype == BirstScriptParser.COUNTDISTINCT)
						objs = new HashSet<Object>();
					// Process in native data type, then convert at the end (efficiency)
					for (int i = 0; i < rows.length; i++)
					{
						Object v = rows[i][lookupIndex];
						if (v == null)
							continue;
						if (stattype == BirstScriptParser.COUNTDISTINCT)
							objs.add(v);
						else
						{
							if (Double.class.isInstance(v))
							{
								double d = (Double) v;
								sum += d;
								sumsq += d * d;
								if (d > max)
									max = d;
								if (d < min)
									min = d;
							} else if (Long.class.isInstance(v))
							{
								long l = (Long) v;
								suml += l;
								sumsql += l * l;
								if (l > maxl)
									maxl = l;
								if (l < minl)
									minl = l;
							} else if (Integer.class.isInstance(v))
							{
								int in = (Integer) v;
								sumi += in;
								sumsqi += in * in;
								if (in > maxi)
									maxi = in;
								if (in < mini)
									mini = in;
							}
							count++;
						}
					}
					if (suml > 0)
						sum += (new Long(suml)).doubleValue();
					if (sumsql > 0)
						sumsq += (new Long(sumsql)).doubleValue();
					if (maxl > Long.MIN_VALUE)
					{
						Double newmax = (new Long(maxl)).doubleValue();
						if (newmax > max)
							max = newmax;
					}
					if (minl < Long.MAX_VALUE)
					{
						Double newmin = (new Long(minl)).doubleValue();
						if (newmin < min)
							min = newmin;
					}
					if (sumi > 0)
						sum += (new Integer(sumi)).doubleValue();
					if (sumsqi > 0)
						sumsq += (new Integer(sumsqi)).doubleValue();
					if (maxi > Integer.MIN_VALUE)
					{
						Double newmax = (new Integer(maxi)).doubleValue();
						if (newmax > max)
							max = newmax;
					}
					if (mini < Integer.MAX_VALUE)
					{
						Double newmin = (new Integer(mini)).doubleValue();
						if (newmin < min)
							min = newmin;
					}
					double result = 0;
					if (stattype == BirstScriptParser.SUM)
						result = sum;
					else if (stattype == BirstScriptParser.AVG)
						result = sum / ((double) count);
					else if (stattype == BirstScriptParser.MIN)
						result = min;
					else if (stattype == BirstScriptParser.MAX)
						result = max;
					else if (stattype == BirstScriptParser.COUNT)
						result = count;
					else if (stattype == BirstScriptParser.COUNTDISTINCT)
						result = objs.size();
					else if (stattype == BirstScriptParser.STATSTDDEV)
						result = count == 0 ? 0 : Math.sqrt(sumsq / ((double) count) - ((sum / ((double) count)) * (sum / ((double) count))));
					if (parameterExpressions.isEmpty())
						statisticValue = result;
					return result;
				}
			} else if (isFunction)
			{
				if (r.getServerParameters().getMaxRecords() > 0 && numInputRows > maxInputRows)
					throw new ScriptException("Function halted. Exceeded maximum allowed number of input rows: " + numInputRows, -1, -1, -1);
				Object[][] rows = qrs.getRows();
				functionScript.resetDefinitions();
				QueryResultSet parentResultSet = script.getParentResultSet();
				if (parentResultSet != null && parentResultSet.getCurRow() < parentResultSet.numRows() && script.hasParentRow())
				{
					Object[][] prows = parentResultSet.getRows();
					Object[] row = prows[script.getParentRow()];
					for (int i = 0; i < parentInputElements.size() && i < row.length; i++)
					{
						((InputDataElement) parentInputElements.get(i)).setValue(row[i]);
					}
				}
				if (lookupIndex < numColumns && resultIndex < numColumns)
				{
					if (exact && parameterExpressions.isEmpty())
					{
						// Cache lookup
						if (lookupMapList == null)
						{
							lookupMapList = new HashMap<Object, List<Object[]>>(rows.length);
							for (int i = 0; i < rows.length; i++)
							{
								if (rows[i][lookupIndex] != null)
								{
									List<Object[]> list = lookupMapList.get(rows[i][lookupIndex]);
									if (list == null)
									{
										list = new ArrayList<Object[]>();
										lookupMapList.put(rows[i][lookupIndex], list);
									}
									list.add(rows[i]);
								}
							}
						}
						if (o == null)
							return null;
						List<Object[]> rowstouse = lookupMapList.get(o);
						if (rowstouse != null)
						{
							for (Object[] row : rowstouse)
							{
								for (int i = 0; i < functionInputElements.size(); i++)
								{
									((InputDataElement) functionInputElements.get(i)).setValue(row[i]);
								}
								mainBlock.execute();
							}
							if (functionScript.onComplete != null)
								functionScript.onComplete.execute();
							Object result = functionScript.findElement("[Result]").getValue(null);
							return result;
						}
						return null;
					}
				}
				for (int ri = 0; ri < rows.length; ri++)
				{
					for (int i = 0; i < functionInputElements.size(); i++)
					{
						((InputDataElement) functionInputElements.get(i)).setValue(rows[ri][i]);
					}
					mainBlock.execute();
				}
				if (functionScript.onComplete != null)
					functionScript.onComplete.execute();
				Object result = functionScript.findElement("[Result]").getValue(null);
				return result;
			} else
			{
				// Is Lookup
				if (lookupIndex < numColumns && resultIndex < numColumns)
				{
					Object[][] rows = qrs.getRows();
					if (exact && !useRow && parameterExpressions.isEmpty())
					{
						// Cache lookup
						if (exactMap == null)
						{
							exactMap = new HashMap<Object, Object>(rows.length);
							for (int i = 0; i < rows.length; i++)
							{
								if (rows[i][lookupIndex] != null)
									exactMap.put(rows[i][lookupIndex], rows[i][resultIndex]);
							}
						}
						if (o == null)
							return null;
						return exactMap.get(o);
					}
					for (int i = 0; i < rows.length; i++)
					{
						if (exact && !useRow)
						{
							if (rows[i][lookupIndex] != null && rows[i][lookupIndex].equals(o))
								return rows[i][resultIndex];
							if (rows[i][lookupIndex] == null && o == null)
								return null;
						} else if (useRow)
						{
							int rowindex = 0;
							if (Integer.class.isInstance(o))
								rowindex = (Integer) o;
							else if (Long.class.isInstance(o))
								rowindex = ((Long) o).intValue();
							else if (Double.class.isInstance(o))
								rowindex = ((Double) o).intValue();
							else
								return null;
							if (i == rowindex)
								return rows[i][resultIndex];
						} else
						{
							if (Integer.class.isInstance(o) && Integer.class.isInstance(rows[i][lookupIndex]))
							{
								if (((Integer) rows[i][lookupIndex]).compareTo((Integer) o) >= 0)
								{
									return rows[i][resultIndex];
								}
							} else if (Double.class.isInstance(o) && Double.class.isInstance(rows[i][lookupIndex]))
							{
								if (((Double) rows[i][lookupIndex]).compareTo((Double) o) >= 0)
								{
									return rows[i][resultIndex];
								}
							} else if (Long.class.isInstance(o) && Long.class.isInstance(rows[i][lookupIndex]))
							{
								if (((Long) rows[i][lookupIndex]).compareTo((Long) o) >= 0)
								{
									return rows[i][resultIndex];
								}
							} else if (String.class.isInstance(o) && String.class.isInstance(rows[i][lookupIndex]))
							{
								if (((String) rows[i][lookupIndex]).compareTo((String) o) >= 0)
								{
									return rows[i][resultIndex];
								}
							} else if (Date.class.isInstance(o) && Date.class.isInstance(rows[i][lookupIndex]))
							{
								if (((Date) rows[i][lookupIndex]).compareTo((Date) o) >= 0)
								{
									return rows[i][resultIndex];
								}
							} else if (Calendar.class.isInstance(o) && Calendar.class.isInstance(rows[i][lookupIndex]))
							{
								if (((Calendar) rows[i][lookupIndex]).compareTo((Calendar) o) >= 0)
								{
									return rows[i][resultIndex];
								}
							} else if (rows[i][lookupIndex].equals(o))
							{
								return rows[i][resultIndex];
							}
						}
					}
				} else
				{
					if (lookupIndex >= numColumns)
					{
						logger.debug("lookupIndex >= numColumns: " + lookupIndex + "," + numColumns + ": " + Statement.toString(t, null));
					}
					if (resultIndex >= numColumns)
					{
						logger.debug("resultIndex >= numColumns: " + resultIndex + "," + numColumns + ": " + Statement.toString(t, null));
					}
				}
			}
		} catch (Exception e)
		{
			e.printStackTrace();
			invalid = true;
			throw new ScriptException("Cannot process " + t.getText() + " query: " + e.getMessage(), t.getLine(), t.getCharPositionInLine(),
					t.getCharPositionInLine() + t.getText().length());
		}
		return null;
	}

	public boolean returnsMeasure()
	{
		if (isStatistic)
			return true; // no resultIndex for isStatistics, assume measure
		if (resultIndex == -1)
		{
			logger.debug("returns measure called with a resultIndex equal to -1");
			return true; // safety check
		}
		Tree qt = lqs.getQueryTree();
		for (int i = 0; i < qt.getChildCount(); i++)
		{
			Tree child = qt.getChild(i);
			if (child.getType() == BirstScriptParser.PROJECTIONLIST)
			{
				if (child.getChildCount() <= resultIndex)
				{
					logger.debug("returns measure called with a resultIndex >= the child count for the projection list");
					return true; // safety check
				}
				Tree projCol = child.getChild(resultIndex);
				if (projCol.getType() == BirstScriptParser.IDENT_NAME)
				{
					return true;
				} else if (projCol.getType() == BirstScriptParser.COLUMN_NAME)
				{
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public int getSize()
	{
		if (isStatistic)
			return Double.SIZE / 8;
		return Integer.SIZE / 8;
	}

	@Override
	public Type getType()
	{
		if (isStatistic)
			return Operator.Type.Float;
		if (isFunction)
		{
			DataType dt = script.getType(t.getChild(0));
			if (dt.type == DType.Float)
				return Operator.Type.Float;
			else if (dt.type == DType.Integer)
				return Operator.Type.Integer;
			else if (dt.type == DType.Varchar)
				return Operator.Type.Varchar;
			else if (dt.type == DType.Boolean)
				return Operator.Type.Boolean;
			else if (dt.type == DType.Date)
				return Operator.Type.DateTime;
			else if (dt.type == DType.DateTime)
				return Operator.Type.DateTime;
		}
		return getColumnType(resultIndex);
	}

	private Operator.Type getColumnType(int index)
	{
		if (index < columnTypeList.size())
			return columnTypeList.get(index);
		else
			return Operator.Type.Varchar;
	}

	/**
	 * build a list of types for the columns
	 */
	private void setColumnList()
	{
		List<Object> clist = q.getSuppliedQueryColumns();
		for (Object item : clist)
		{
			if (item.getClass() == DimensionColumnNav.class)
			{
				DimensionColumnNav dcn = (DimensionColumnNav) item;
				DimensionColumn dc = dcn.getDimensionColumn();
				columnNameList.add("[" + dc.DimensionTable.DimensionName + "." + dc.ColumnName + "]");
				if (dc.DataType.equals("Varchar"))
					columnTypeList.add(Operator.Type.Varchar);
				else if (dc.DataType.equals("Integer"))
					columnTypeList.add(Operator.Type.Integer);
				else if (dc.DataType.equals("Float") || dc.DataType.equals("Number"))
					columnTypeList.add(Operator.Type.Float);
				else if (dc.DataType.equals("Date") || dc.DataType.equals("DateTime"))
					columnTypeList.add(Operator.Type.DateTime);
			} else if (item.getClass() == MeasureColumnNav.class)
			{
				MeasureColumnNav mcn = (MeasureColumnNav) item;
				MeasureColumn mc = mcn.getMeasureColumn();
				columnNameList.add("[" + mc.ColumnName + "]");
				// Irrespective of the measure column's data type, return Integer if counting.
				if (mc.AggregationRule != null && (mc.AggregationRule.equals("COUNT") || (mc.AggregationRule.equals("COUNT DISTINCT"))))
				{
					columnTypeList.add(Operator.Type.Integer);
				} else if (mc.DataType.equals("Varchar"))
					columnTypeList.add(Operator.Type.Varchar);
				else if (mc.DataType.equals("Integer"))
				{
					columnTypeList.add(Operator.Type.Integer);
				} else if (mc.DataType.equals("Float") || mc.DataType.equals("Number"))
					columnTypeList.add(Operator.Type.Float);
				else if (mc.DataType.equals("Date") || mc.DataType.equals("DateTime"))
					columnTypeList.add(Operator.Type.DateTime);
			}
		}
		/*
		 * Now, add expressions to the list
		 */
		List<DisplayExpression> expressionList = lqs.getExpressionList();
		if (expressionList != null)
		{
			Collections.sort(expressionList);
			for (DisplayExpression de : expressionList)
			{
				int pos = de.getPosition();
				if (pos < 0 || pos > columnTypeList.size())
				{
					pos = columnTypeList.size();
					de.setPosition(pos);
				}
				if (de.isInteger())
					columnTypeList.add(pos, Operator.Type.Integer);
				else if (de.isFloat())
					columnTypeList.add(pos, Operator.Type.Float);
				else
					columnTypeList.add(pos, Operator.Type.Varchar);
				columnNameList.add(pos, "[" + de.getName() + "]");
			}
		}
	}
}
