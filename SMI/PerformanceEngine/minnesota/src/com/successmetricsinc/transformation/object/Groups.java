package com.successmetricsinc.transformation.object;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.query.DimensionColumn;
import com.successmetricsinc.transformation.BirstScriptParser;
import com.successmetricsinc.transformation.DataType;
import com.successmetricsinc.transformation.Expression;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.transformation.VariableDataElement;
import com.successmetricsinc.transformation.DataType.DType;

public class Groups extends Operator
{
	private Tree t;
	private TransformationScript script;
	private TransformationScript newscript;
	private Operator dimensionColumn;
	private DimensionColumn dc;
	private Map<String, Operator> opList = new HashMap<String, Operator>();

	public Groups(TransformationScript script, Tree t) throws ScriptException
	{
		super(script, t);
		this.t = t;
		this.script = script;
		String cname = t.getChild(0).getText();
		cname = cname.substring(1, cname.length() - 1);
		int index = cname.indexOf('.');
		String dimName = cname.substring(0, index);
		String colName = cname.substring(index + 1);
		dc = script.getRepository().findDimensionColumn(dimName, colName);
		if (dc == null)
			throw new ScriptException("Unknown data element: " + cname, t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine()
					+ t.getText().length());
		Expression ex = new Expression(script);
		dimensionColumn = ex.compile(t.getChild(0));
		if (script.getDisplayExpression() != null)
		{
			Map<DimensionColumn, Groups> gmap = script.getDisplayExpression().getCustomGroups();
			Groups curgroups = gmap.get(dc);
			if (curgroups == null)
			{
				newscript = new TransformationScript(script.getRepository(), script.getSession());
				gmap.put(dc, this);
			} else
			{
				opList = curgroups.getOpList();
				newscript = curgroups.getScript();
			}
			for (int i = 1; i < t.getChildCount(); i++)
			{
				Tree ct = t.getChild(i);
				if (ct.getType() != BirstScriptParser.EQUALS)
					throw new ScriptException("Invalid group expression", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine()
							+ t.getText().length());
				Tree ctexp = ct.getChild(1);
				List<String> identList = new ArrayList<String>();
				getIdentifiers(ctexp, identList);
				for (String s : identList)
				{
					VariableDataElement vde = new VariableDataElement();
					vde.name = s;
					vde.type = new DataType(DType.Float, 0);
					newscript.dataElements.add(vde);
				}
				ex = new Expression(newscript);
				Operator group = ex.compile(ctexp);
				if (group.getType() != Type.Integer && group.getType() != Type.Float)
					throw new ScriptException("Invalid group expression", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine()
							+ t.getText().length());
				String gname = ct.getChild(0).getText();
				gname = gname.substring(1, gname.length() - 1);
				opList.put(gname, group);
			}
		}
	}

	@Override
	public Operator clone()
	{
		try
		{
			return new Groups(script, t);
		} catch (ScriptException e)
		{
			return null;
		}
	}

	private void getIdentifiers(Tree t, List<String> list)
	{
		if (t.getType() == BirstScriptParser.IDENT_NAME)
		{
			list.add(t.getText());
		}
		for (int i = 0; i < t.getChildCount(); i++)
			getIdentifiers(t.getChild(i), list);
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		try
		{
			Object aval = evaluate();
			Object bval = b.evaluate();
			if (aval.getClass() != bval.getClass())
				throw new ScriptException("Bad data type: ", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
			if (Integer.class.isInstance(aval))
				return ((Integer) aval).compareTo((Integer) bval);
			else if (Long.class.isInstance(aval))
				return ((Long) aval).compareTo((Long) bval);
			else if (Double.class.isInstance(aval))
				return ((Double) aval).compareTo((Double) bval);
			else if (Boolean.class.isInstance(aval))
				return ((Boolean) aval).compareTo((Boolean) bval);
			else if (String.class.isInstance(aval))
				return ((String) aval).compareTo((String) bval);
			else if (Date.class.isInstance(aval))
				return ((Date) aval).compareTo((Date) bval);
			else if (Calendar.class.isInstance(aval))
				return ((Calendar) aval).compareTo((Calendar) bval);
			
			return 0;
		} catch (Exception e)
		{
			if (e instanceof ScriptException)
				throw (ScriptException) e;
			throw new ScriptException("Bad data type: ", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		try
		{
			Object aval = evaluate();
			Object bval = b.evaluate();
			if (aval.getClass() != bval.getClass())
				throw new ScriptException("Bad data type: ", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
			if (Integer.class.isInstance(aval))
				return ((Integer) aval).equals((Integer) bval);
			else if (Long.class.isInstance(aval))
				return ((Long) aval).equals((Long) bval);
			else if (Double.class.isInstance(aval))
				return ((Double) aval).equals((Double) bval);
			else if (Boolean.class.isInstance(aval))
				return ((Boolean) aval).equals((Boolean) bval);
			else if (String.class.isInstance(aval))
				return ((String) aval).equals((String) bval);
			else if (Date.class.isInstance(aval))
				return ((Date) aval).equals((Date) bval);
			else if (Calendar.class.isInstance(aval))
				return ((Calendar) aval).equals((Calendar) bval);
			
			return false;
		} catch (Exception e)
		{
			throw new ScriptException("Bad data type", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		return dimensionColumn.evaluate();
	}

	@Override
	public int getSize()
	{
		if (dimensionColumn == null)
			return 0;
		return dimensionColumn.getSize();
	}

	@Override
	public Type getType()
	{
		return dimensionColumn.getType();
	}

	public Map<String, Operator> getOpList()
	{
		return opList;
	}

	public TransformationScript getScript()
	{
		return newscript;
	}
}
