package com.successmetricsinc.transformation.object;

import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.query.DisplayExpression;
import com.successmetricsinc.query.FieldProvider;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.TransformationScript;

public class RowNumber extends Operator
{
	private TransformationScript script;
	private Tree t;

	public RowNumber(TransformationScript script, Tree t) throws ScriptException
	{
		super(script, t);
		this.script = script;
		this.t = t;
	}

	@Override
	public Operator clone()
	{
		try
		{
			return new RowNumber(script, t);
		} catch (ScriptException e)
		{
			return null;
		}
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		try
		{
			return ((Long) evaluate()).compareTo((Long) b.evaluate());
		} catch (Exception e)
		{
			if (e instanceof ScriptException)
				throw (ScriptException) e;
			throw new ScriptException("Bad data type: ", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		try
		{
			return ((Long) evaluate()).equals((Long) b.evaluate());
		} catch (Exception e)
		{
			throw new ScriptException("Bad data type", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		DisplayExpression de = script.getDisplayExpression();
		if (de != null)
		{
			FieldProvider fp = de.getFields();
			if (fp != null)
			{
				int retVal = fp.getRowIndex();
				return new Integer(retVal).longValue();
			}
		}
		return 0l;
	}

	@Override
	public int getSize()
	{
		return Integer.SIZE / 8;
	}

	@Override
	public Type getType()
	{
		return Operator.Type.Integer;
	}
}
