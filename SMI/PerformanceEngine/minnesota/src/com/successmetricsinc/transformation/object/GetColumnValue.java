package com.successmetricsinc.transformation.object;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.transformation.DataType;
import com.successmetricsinc.transformation.InputDataElement;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.Statement;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.transformation.operators.OpDateTime;

public class GetColumnValue extends Operator
{
	private TransformationScript script;
	private Tree t;
	private int index;
	private DataType type;

	public GetColumnValue(TransformationScript script, Tree t) throws ScriptException
	{
		super(script, t);
		this.script = script;
		this.t = t;
		index = Integer.valueOf(t.getChild(0).getText());
		InputDataElement ide = null;
		List<InputDataElement> list = script.getInputElements();
		if (index >= 0 && index < list.size())
			ide = (InputDataElement) list.get(index);
		if (ide == null)
			throw new ScriptException("Unknown data element in GETCOLUMNVALUE: " + t.getText() + "(" + t.getChild(0).getText() + ")", t.getLine(),
					t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		type = ide.getType();
	}

	@Override
	public Operator clone()
	{
		try
		{
			return new GetColumnValue(script, t);
		} catch (ScriptException e)
		{
			return null;
		}
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		try
		{
			Object aval = evaluate();
			Object bval = b.evaluate();
			if (aval.getClass() != bval.getClass())
				if (Number.class.isInstance(aval) && Number.class.isInstance(bval))
					return ((Double) ((Number) aval).doubleValue()).compareTo(((Number) bval).doubleValue());
				else
					throw new ScriptException("Result of GETCOLUMVALUE compared against incorrect data type: " + aval.getClass().getName() + ", "
							+ bval.getClass().getName() + ", " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine()
							+ t.getText().length());
			if (Integer.class.isInstance(aval))
				return ((Integer) aval).compareTo((Integer) bval);
			else if (Long.class.isInstance(aval))
				return ((Long) aval).compareTo((Long) bval);
			else if (Double.class.isInstance(aval))
				return ((Double) aval).compareTo((Double) bval);
			else if (Boolean.class.isInstance(aval))
				return ((Boolean) aval).compareTo((Boolean) bval);
			else if (String.class.isInstance(aval))
				return ((String) aval).compareTo((String) bval);
			else if (Date.class.isInstance(aval))
				return ((Date) aval).compareTo((Date) bval);
			else if (Calendar.class.isInstance(aval))
				return ((Calendar) aval).compareTo((Calendar) bval);
			
			return 0;
		} catch (Exception e)
		{
			if (e instanceof ScriptException)
				throw (ScriptException) e;
			throw new ScriptException("Exception thrown in GETCOLUMVALUE compareTo: " + e.getMessage() + ", " + Statement.toString(t, null), t.getLine(),
					t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		try
		{
			Object aval = evaluate();
			Object bval = b.evaluate();
			if (aval.getClass() != bval.getClass())
				if (Number.class.isInstance(aval) && Number.class.isInstance(bval))
					return ((Double) ((Number) aval).doubleValue()).equals(((Number) bval).doubleValue());
				else
					throw new ScriptException("Result of GETCOLUMVALUE compared against incorrect data type: " + aval.getClass().getName() + ", "
							+ bval.getClass().getName() + ", " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine()
							+ t.getText().length());
			if (Integer.class.isInstance(aval))
				return ((Integer) aval).equals((Integer) bval);
			else if (Long.class.isInstance(aval))
				return ((Long) aval).equals((Long) bval);
			else if (Double.class.isInstance(aval))
				return ((Double) aval).equals((Double) bval);
			else if (Boolean.class.isInstance(aval))
				return ((Boolean) aval).equals((Boolean) bval);
			else if (String.class.isInstance(aval))
				return ((String) aval).equals((String) bval);
			else if (Date.class.isInstance(aval))
				return ((Date) aval).equals((Date) bval);
			else if (Calendar.class.isInstance(aval))
				return ((Calendar) aval).equals((Calendar) bval);
			
			return false;
		} catch (Exception e)
		{
			if (e instanceof ScriptException)
				throw (ScriptException) e;
			throw new ScriptException("Exception thrown in GETCOLUMVALUE equals: " + e.getMessage() + ", " + Statement.toString(t, null), t.getLine(),
					t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		InputDataElement ide = null;
		List<InputDataElement> list = script.getInputElements();
		if (index >= 0 && index < list.size())
		{
			ide = (InputDataElement) list.get(index);
			return ide.getValue(null);
		}
		return null;
	}

	@Override
	public int getSize()
	{
		if (type.type == DataType.DType.Date || type.type == DataType.DType.DateTime)
			return OpDateTime.DATE_SIZE;
		if (type.type == DataType.DType.Integer)
			return Integer.SIZE / 8;
		if (type.type == DataType.DType.Float)
			return Double.SIZE / 8;
		return 0;
	}

	@Override
	public Type getType()
	{
		if (type.type == DataType.DType.Date || type.type == DataType.DType.DateTime)
			return Operator.Type.DateTime;
		if (type.type == DataType.DType.Integer)
			return Operator.Type.Integer;
		if (type.type == DataType.DType.Float)
			return Operator.Type.Float;
		return Operator.Type.Varchar;
	}
}
