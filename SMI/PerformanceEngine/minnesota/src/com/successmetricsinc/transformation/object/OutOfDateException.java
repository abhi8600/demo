/**
 * 
 */
package com.successmetricsinc.transformation.object;

/**
 * @author agarrison
 *
 */
public class OutOfDateException extends Exception {

	public OutOfDateException(String msg) {
		super(msg);
	}
}
