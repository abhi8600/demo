/**
 * $Id: Transform.java,v 1.10 2011-09-27 00:26:16 ricks Exp $
 *
 * Copyright (C) 2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.transformation.object;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.antlr.runtime.tree.Tree;
import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.query.QueryResultSet;
import com.successmetricsinc.query.ResultSetCache;
import com.successmetricsinc.transformation.ColData;
import com.successmetricsinc.transformation.DataType;
import com.successmetricsinc.transformation.InputDataElement;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.OutputDataElement;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.StatementBlock;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.transformation.DataType.DType;
import com.successmetricsinc.util.logging.TraceLog;
import com.successmetricsinc.warehouse.StagingTable;

public class Transform extends Operator
{
	private static Logger logger = Logger.getLogger(Transform.class);
	private Tree t;
	private TransformationScript script;
	private TransformationScript functionScript;
	List<InputDataElement> parentInputElements;
	private DataType type;
	private StatementBlock mainBlock;
	private static final int MAX_RECORD_MULTIPLIER = 100;

	public Transform(TransformationScript script, Tree t) throws ScriptException
	{
		super(script, t);
		this.t = t;
		this.script = script;
	}

	@Override
	public Operator clone()
	{
		try
		{
			return new Transform(script, t);
		} catch (ScriptException e)
		{
			return null;
		}
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		try
		{
			Object aval = evaluate();
			Object bval = b.evaluate();
			if (aval == null && bval == null)
				return 0;
			if (bval == null)
				return -1;
			if (aval == null)
				return 1;
			if (aval.getClass() != bval.getClass())
				throw new ScriptException("Bad data type: ", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
			if (Integer.class.isInstance(aval))
				return ((Integer) aval).compareTo((Integer) bval);
			else if (Long.class.isInstance(aval))
				return ((Long) aval).compareTo((Long) bval);
			else if (Double.class.isInstance(aval))
				return ((Double) aval).compareTo((Double) bval);
			else if (Boolean.class.isInstance(aval))
				return ((Boolean) aval).compareTo((Boolean) bval);
			else if (String.class.isInstance(aval))
				return ((String) aval).compareTo((String) bval);
			else if (Date.class.isInstance(aval))
				return ((Date) aval).compareTo((Date) bval);
			else if (Calendar.class.isInstance(aval))
				return ((Calendar) aval).compareTo((Calendar) bval);
			
			return 0;
		} catch (Exception e)
		{
			if (e instanceof ScriptException)
				throw (ScriptException) e;
			throw new ScriptException("Bad data type: ", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		try
		{
			Object aval = evaluate();
			Object bval = b.evaluate();
			if (aval.getClass() != bval.getClass())
				throw new ScriptException("Bad data type: ", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
			if (Integer.class.isInstance(aval))
				return ((Integer) aval).equals((Integer) bval);
			else if (Long.class.isInstance(aval))
				return ((Long) aval).equals((Long) bval);
			else if (Double.class.isInstance(aval))
				return ((Double) aval).equals((Double) bval);
			else if (Boolean.class.isInstance(aval))
				return ((Boolean) aval).equals((Boolean) bval);
			else if (String.class.isInstance(aval))
				return ((String) aval).equals((String) bval);
			else if (Date.class.isInstance(aval))
				return ((Date) aval).equals((Date) bval);
			else if (Calendar.class.isInstance(aval))
				return ((Calendar) aval).equals((Calendar) bval);
			
			return false;
		} catch (Exception e)
		{
			throw new ScriptException("Bad data type", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		Object result = functionScript.findElement("[Result]").getValue(null);
		return result;
	}

	@Override
	public int getSize()
	{
		return Integer.SIZE / 8;
	}

	@Override
	public Type getType()
	{
		return Operator.Type.Integer;
	}

	public void execute() throws ScriptException
	{
		functionScript = new TransformationScript(script.getRepository(), script.getSession());
		int maxRecords = script.getRepository().getServerParameters().getMaxRecords();
		if (maxRecords <= 0)
		{
			throw new ScriptException("Script halted. Invalid max record value ==> " + maxRecords, -1, -1, -1);
		}
		functionScript.setMaxOutputRows(maxRecords);
		long curMaxStatements = functionScript.getMaxStatements();
		functionScript.setMaxStatements(Math.min((maxRecords * MAX_RECORD_MULTIPLIER), curMaxStatements));
		functionScript.initializeScriptResults();
		OutputDataElement ode = new OutputDataElement();
		ode.setName("[Result]");
		type = new DataType(DType.Integer, 0);
		ode.setType(type);
		functionScript.addOutputDataElement(ode);
		functionScript.setLogicalQuery(false);
		functionScript.setDisplayExpression(script.getDisplayExpression());
		parentInputElements = new ArrayList<InputDataElement>();
		ResultSetCache rsc = null;
		Repository r = script.getRepository();
		if (rsc == null)
		{
			try
			{
				rsc = ResultSetCache.getInstance(r.getApplicationIdentifier(), false);
			} catch (Exception ex)
			{
				logger.error(ex.getMessage(), ex);
			}
		}
		QueryResultSet parentResultSet = script.getParentResultSet();
		if (parentResultSet != null)
		{
			for (int i = 0; i < parentResultSet.getDisplayNames().length; i++)
			{
				InputDataElement ide = new InputDataElement();
				ColData cd = new ColData();
				int type = parentResultSet.getColumnDataTypes()[i];
				switch (type)
				{
				case Types.INTEGER:
					cd.datatype = "Integer";
					ide.type = new DataType(DType.Integer, 0);
					break;
				case Types.DOUBLE:
					cd.datatype = "Float";
					ide.type = new DataType(DType.Float, 0);
					break;
				case Types.VARCHAR:
					cd.datatype = "Varchar";
					cd.width = 1024;
					ide.type = new DataType(DType.Varchar, 1024);
					break;
				case Types.BOOLEAN:
					cd.datatype = "Boolean";
					ide.type = new DataType(DType.Boolean, 0);
					break;
				case Types.TIMESTAMP:
					cd.datatype = "DateTime";
					ide.type = new DataType(DType.DateTime, 0);
					break;
				}
				cd.logicalColumnName = parentResultSet.getColumnNames()[i];
				cd.physicalColumnName = parentResultSet.getColumnNames()[i];
				ide.name = '[' + cd.logicalColumnName + ']';
				ide.setColdata(cd);
				functionScript.addInputDataElement(ide);
				ode = new OutputDataElement();
				ode.setName(ide.getName());
				ode.setType(ide.getType());
				functionScript.addOutputDataElement(ode);
				parentInputElements.add(ide);
			}
		}
		TraceLog traceLog = null;
		try
		{
			traceLog = new TraceLog(r.getRepositoryRootPath(), r.getServerParameters().getMaxRecords() * 10);
			functionScript.setTraceLog(traceLog);
			traceLog.enable();
			List<StagingTable> scriptPath = new ArrayList<StagingTable>();
			mainBlock = new StatementBlock(functionScript, t.getChild(0), scriptPath);
			if (parentResultSet == null)
				return;
			Object[][] rows = parentResultSet.getRows();
			if (rows == null)
				return;
			for (int ri = 0; ri < rows.length; ri++)
			{
				for (int i = 0; i < parentInputElements.size() && i < rows[ri].length; i++)
				{
					((InputDataElement) parentInputElements.get(i)).setValue(rows[ri][i]);
				}
				mainBlock.execute();
			}
			if (functionScript.onComplete != null)
			{
				if (traceLog != null)
					traceLog.log("Starting complete block");
				functionScript.onComplete.execute();
			}
		} finally
		{
			if (traceLog != null)
			{
				traceLog.log((parentResultSet != null ? parentResultSet.numRows() : 0) + " input rows, "
						+ (functionScript.getOutputRows() != null ? functionScript.getOutputRows().size() : 0) + " output rows");
				traceLog.disable();
			}
		}
		if (functionScript.getOutputRows() != null)
		{
			Object[][] newRows = new Object[functionScript.getOutputRows().size()][];
			functionScript.getOutputRows().toArray(newRows);
			parentResultSet.setRows(newRows);
		}
	}
}
