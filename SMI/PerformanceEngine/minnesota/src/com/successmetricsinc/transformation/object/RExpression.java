package com.successmetricsinc.transformation.object;

import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.R.RServer;
import com.successmetricsinc.transformation.BirstScriptParser;
import com.successmetricsinc.transformation.Expression;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.TransformationScript;

public class RExpression extends Operator
{
	private TransformationScript script;
	private Tree t;
	private int dataType;
	private Operator exp;
	private RServer rserv;

	public RExpression(TransformationScript script, Tree t) throws ScriptException
	{
		super(script, t);
		this.script = script;
		this.t = t;
		dataType = t.getChild(0).getType();
		if (dataType != BirstScriptParser.NUMBERTYPE && dataType != BirstScriptParser.FLOATTYPE && dataType != BirstScriptParser.INTEGERTYPE
				&& dataType != BirstScriptParser.VARCHARTYPE)
			throw new ScriptException("Bad return data type for R Expression: ", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine()
					+ t.getText().length());
		Tree expression = t.getChild(1);
		Expression ex = new Expression(script);
		exp = ex.compile(expression);
		rserv = script.getRepository().getRServer();
	}

	@Override
	public Operator clone()
	{
		try
		{
			return new RExpression(script, t);
		} catch (ScriptException e)
		{
			return null;
		}
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		try
		{
			return (evaluate().toString()).compareTo(b.evaluate().toString());
		} catch (Exception e)
		{
			if (e instanceof ScriptException)
				throw (ScriptException) e;
			if (e instanceof NullPointerException)
				throw new ScriptException("Bad Expression", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
			throw new ScriptException("Bad data type: ", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		try
		{
			return (evaluate().toString()).equals(b.evaluate().toString());
		} catch (Exception e)
		{
			if (e instanceof NullPointerException)
				throw new ScriptException("Bad Expression", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
			throw new ScriptException("Bad data type", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		if (rserv == null)
			throw new ScriptException("Exception thrown in R Expression - no valid connection to R Server setup", t.getLine(), t.getCharPositionInLine(),
					t.getCharPositionInLine() + t.getText().length());
		Object expression = exp.evaluate();
		if (expression == null)
			throw new ScriptException("Exception thrown in R Expression - invalid expression", t.getLine(), t.getCharPositionInLine(),
					t.getCharPositionInLine() + t.getText().length());
		Object result = null;
		try
		{
			result = rserv.eval(dataType, expression.toString());
		} catch (Exception e)
		{
			throw new ScriptException(e.getMessage(), t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
		return result;
	}

	@Override
	public int getSize()
	{
		return Integer.SIZE / 8;
	}

	@Override
	public Type getType()
	{
		if (dataType == BirstScriptParser.INTEGERTYPE)
			return Operator.Type.Integer;
		if (dataType == BirstScriptParser.NUMBERTYPE)
			return Operator.Type.Float;
		if (dataType == BirstScriptParser.FLOATTYPE)
			return Operator.Type.Float;
		if (dataType == BirstScriptParser.VARCHARTYPE)
			return Operator.Type.Varchar;
		return Operator.Type.Varchar;
	}
}
