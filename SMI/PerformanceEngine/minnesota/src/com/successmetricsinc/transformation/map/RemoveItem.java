/**
 * 
 */
package com.successmetricsinc.transformation.map;

import java.util.Map;

import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.transformation.DataElement;
import com.successmetricsinc.transformation.DataType.DType;
import com.successmetricsinc.transformation.Expression;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.Statement;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.transformation.VariableDataElement;

/**
 * @author bpeters
 * 
 */
public class RemoveItem extends Statement
{
	private VariableDataElement vde;
	private Operator expression;

	public RemoveItem(TransformationScript script, Tree t) throws ScriptException
	{
		this.script = script;
		String vname = t.getChild(0).getText();
		DataElement de = script.findElement(vname);
		if (de == null)
			throw new ScriptException("Unknown variable " + vname + " at line " + t.getLine(), t.getLine(), t.getCharPositionInLine(), t
					.getCharPositionInLine()
					+ t.getText().length());
		if (!VariableDataElement.class.isInstance(de) || ((VariableDataElement) de).getType().type != DType.Map)
			throw new ScriptException("Invalid data element " + vname + " at line " + t.getLine(), t.getLine(), t.getCharPositionInLine(), t
					.getCharPositionInLine()
					+ t.getText().length());
		vde = (VariableDataElement) de;
		Expression ex = new Expression(script);
		expression = ex.compile(t.getChild(1));
	}

	@SuppressWarnings("unchecked")
	@Override
	public void execute() throws ScriptException
	{
		Object o = expression.evaluate();
		Map<Object, Object> omap = (Map<Object, Object>) vde.getValue(null);
		try
		{
			omap.remove(o);
			script.curMemory -= vde.getType().getInitializedSize();
		} catch (NullPointerException ex)
		{
		}
	}
}
