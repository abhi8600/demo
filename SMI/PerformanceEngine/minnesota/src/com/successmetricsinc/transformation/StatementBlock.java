/**
 * 
 */
package com.successmetricsinc.transformation;

import java.util.ArrayList;
import java.util.List;

import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.transformation.TransformationScript.ScriptResult;
import com.successmetricsinc.transformation.dml.Delete;
import com.successmetricsinc.transformation.list.Add;
import com.successmetricsinc.transformation.list.RemoveAt;
import com.successmetricsinc.transformation.map.RemoveItem;
import com.successmetricsinc.transformation.object.RemoveAll;
import com.successmetricsinc.transformation.transforms.AddParentChild;
import com.successmetricsinc.warehouse.StagingTable;

/**
 * @author bpeters
 * 
 */
public class StatementBlock extends Statement
{
	private List<Statement> statementList = new ArrayList<Statement>();
	private ScriptResult sr;

	public StatementBlock(TransformationScript script, Tree scriptTree, List<StagingTable> scriptPath) throws ScriptException
	{
		this.script = script;
		this.sr = script.getScriptResults();
		int statement = 0;
		for (; statement < scriptTree.getChildCount(); statement++)
		{
			Tree stree = scriptTree.getChild(statement);
			if (stree.getType() == BirstScriptParser.ASSIGNMENT)
			{
				Assignment a = new Assignment(script, stree);
				statementList.add(a);
				if (!script.hasVariableAssignment && a.isTargetVariable())
				{
					script.hasVariableAssignment = true;
				}
			} else if (stree.getType() == BirstScriptParser.DIM)
			{
				Definition def = new Definition(script, stree, new ArrayList<StagingTable>(scriptPath));
				statementList.add(def);
			} else if (stree.getType() == BirstScriptParser.WRITERECORD)
			{
				WriteRecord wr = new WriteRecord(script, stree.getLine(), stree.getCharPositionInLine(), stree.getCharPositionInLine()
						+ stree.getText().length());
				if (script.inCompleteBlock)
					script.hasCompleteBlockWithWriteRecord = true;
				else
					script.hasBodyWriteRecord = true;
				statementList.add(wr);
			} else if (stree.getType() == BirstScriptParser.PRINT)
			{
				Print pr = new Print(script, stree);
				statementList.add(pr);
			} else if (stree.getType() == BirstScriptParser.HALT)
			{
				Halt hr = new Halt(script, stree);
				statementList.add(hr);
			} else if (stree.getType() == BirstScriptParser.IF)
			{
				IfThenElse itr = new IfThenElse(script, stree, new ArrayList<StagingTable>(scriptPath));
				statementList.add(itr);
			} else if (stree.getType() == BirstScriptParser.WHILE)
			{
				WhileLoop wl = new WhileLoop(script, stree, new ArrayList<StagingTable>(scriptPath));
				statementList.add(wl);
			} else if (stree.getType() == BirstScriptParser.EXITWHILE)
			{
				// Make sure there's a containing while
				Tree pt = stree;
				while (!pt.isNil() && pt.getParent().getType() != BirstScriptParser.WHILE)
					pt = pt.getParent();
				if (pt.isNil())
					throw new ScriptException("Exit While not contained in a While loop", stree.getLine(), stree.getCharPositionInLine(),
							stree.getCharPositionInLine() + stree.getText().length());
				statementList.add(new ExitWhile());
			} else if (stree.getType() == BirstScriptParser.FOR)
			{
				ForLoop fl = new ForLoop(script, stree, new ArrayList<StagingTable>(scriptPath));
				statementList.add(fl);
			} else if (stree.getType() == BirstScriptParser.EXITFOR)
			{
				// Make sure there's a containing while
				Tree pt = stree;
				while (!pt.isNil() && pt.getParent().getType() != BirstScriptParser.FOR)
					pt = pt.getParent();
				if (pt.isNil())
					throw new ScriptException("Exit For not contained in a For loop", stree.getLine(), stree.getCharPositionInLine(),
							stree.getCharPositionInLine() + stree.getText().length());
				statementList.add(new ExitFor());
			} else if (stree.getType() == BirstScriptParser.COMPLETE)
			{
				script.inCompleteBlock = true;
				new OnComplete(script, stree, new ArrayList<StagingTable>(scriptPath));
				script.inCompleteBlock = false;
				script.hasCompleteBlock = true;
			} else if (stree.getType() == BirstScriptParser.ADD)
			{
				statementList.add(new Add(script, stree));
			} else if (stree.getType() == BirstScriptParser.REMOVEALL)
			{
				statementList.add(new RemoveAll(script, stree));
			} else if (stree.getType() == BirstScriptParser.REMOVEAT)
			{
				statementList.add(new RemoveAt(script, stree));
			} else if (stree.getType() == BirstScriptParser.REMOVEITEM)
			{
				statementList.add(new RemoveItem(script, stree));
			} else if (stree.getType() == BirstScriptParser.UPDATESFDC)
			{
				statementList.add(new UpdateSFDC(script, stree));
			} else if (stree.getType() == BirstScriptParser.ADDPARENTCHILD)
			{
				statementList.add(new AddParentChild(script, stree));
			} else if (stree.getType() == BirstScriptParser.DELETE)
			{
				statementList.add(new Delete(script, stree));
			} else if (stree.getType() == BirstScriptParser.FUNCTIONDEF)
			{
				Function f = new Function(script, stree, new ArrayList<StagingTable>(scriptPath));
				script.addFunction(f);
			} else if (stree.getType() == BirstScriptParser.FUNCTIONCALL)
			{
				Function f = script.getFunction(stree.getChild(0).getText());
				if (f == null)
					throw new ScriptException("Invalid function reference", stree.getLine(), stree.getCharPositionInLine(), stree.getCharPositionInLine()
							+ stree.getText().length());
				statementList.add(f);
			} else if (stree.getType() == BirstScriptParser.RETURN)
			{
				statementList.add(new Return(script, stree));
			} else if (stree.getType() == BirstScriptParser.RWRITE)
			{
				statementList.add(new RWrite(script, stree));
			} else
			{
				throw new ScriptException("Unknown statement at line " + stree.getLine(), stree.getLine(), stree.getCharPositionInLine(),
						stree.getCharPositionInLine() + stree.getText().length());
			}
		}
	}

	@Override
	public void execute() throws ScriptException
	{
		boolean checkMax = script.maxStatements > 0;
		for (Statement st : statementList)
		{
			try
			{
				st.execute();
			} catch (ScriptException sex)
			{
				sex.setLine(st.getLine());
				throw sex;
			}
			if (checkMax)
			{
				if (++sr.numStatements > script.maxStatements)
					throw new ScriptException("Script halted at line " + st.getLine() + ". Exceeded maximum allowed number of statements: " + sr.numStatements,
							st.getLine(), st.getStart(), st.getStop());
			}
		}
	}
}
