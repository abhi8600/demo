/**
 * 
 */
package com.successmetricsinc.transformation;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.antlr.runtime.tree.Tree;
import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.query.Database;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.Query;
import com.successmetricsinc.query.Database.DatabaseType;
import com.successmetricsinc.transformation.DataType.DType;
import com.successmetricsinc.transformation.operators.OpDateTime;
import com.successmetricsinc.transformation.operators.OpFloat;
import com.successmetricsinc.transformation.operators.OpInt;
import com.successmetricsinc.transformation.operators.OpVarchar;
import com.successmetricsinc.util.DateUtil;
import com.successmetricsinc.warehouse.StagingTable;

/**
 * @author bpeters
 * 
 */
public class Definition extends Statement
{
	private static Logger logger = Logger.getLogger(Definition.class);
	private VariableDataElement vde;
	private boolean initialized = false;
	private Operator compiledAssignment;
	public static final List<SimpleDateFormat> dateformats = DateUtil.getPossibleDateFormats();
	private SourceQuery sq;
	private Tree selectTree;
	private String physicalQuery;

	public Definition(TransformationScript script, Tree t, List<StagingTable> scriptPath) throws ScriptException
	{
		this.script = script;
		this.line = t.getLine();
		this.start = t.getCharPositionInLine();
		this.stop = t.getText().length() + this.start;
		vde = new VariableDataElement();
		vde.name = t.getChild(0).getText();
		vde.type = script.getType(t.getChild(1));
		DataElement curde = script.getElement(vde.name);
		if (curde != null)
			throw new ScriptException("Duplicate variable definition found", t.getChild(0).getLine(), t.getChild(0).getCharPositionInLine(), t.getChild(0)
					.getCharPositionInLine()
					+ t.getChild(0).getText().length());
		script.dataElements.add(vde);
		script.curMemory += vde.type.getInitializedSize();
		if (t.getChildCount() > 2)
		{
			// Initialize
			Tree etree = t.getChild(2);
			if (etree.getType() == BirstScriptParser.SELECT)
			{
				// Make sure there's only one projection list item
				List<Object> sourceTables = new ArrayList<Object>();
				List<String> sourceTableNames = new ArrayList<String>();
				TransformationScript sscript = new TransformationScript(script.getRepository(), script.getSession(), script.isForSourceTablesOnly());
				sscript.setBulkLoaded(script.getBulkLoaded());
				sscript.setLoadNecessaryStagingTables(script.isLoadNecessaryStagingTables());
				sscript.setStatus(script.getStatus());
				sscript.setPath(script.getPath());				
				sscript.setDatabasePath(script.getDatabasePath());
				sscript.setDatabaseConnection(script.getDatabaseConnection());
				sscript.setLoadGroup(script.getLoadGroup());
				sscript.setLoadGroups(script.getLoadGroups());
				List<DataElement> delements = new ArrayList<DataElement>();
				for (DataElement de : script.dataElements)
					if (VariableDataElement.class.isInstance(de))
						delements.add(de);
				sscript.dataElements = delements;
				selectTree = etree;
				sq = sscript.getSelectQuery(selectTree, script.getNumSourceRows(), sourceTables, sourceTableNames, true, scriptPath, false);
				if (vde.getType().type == DType.Map)
				{
					/*
					 * Need to allow more than 2 column for where clause if (selectTree.getChild(0).getChildCount() !=
					 * 2) throw new ScriptException("Query that assigns to a map must return 2 columns",
					 * t.getChild(1).getLine(), t.getChild(2) .getCharPositionInLine(),
					 * t.getChild(2).getCharPositionInLine() + t.getChild(2).getText().length());
					 */
					if (!Assignment.canAssignTo(vde.type.subtype, sscript.inputElements.get(1).type.type))
						throw new ScriptException("Incompatible types line " + t.getLine() + ", " + vde.getType().subtype
								+ " cannot be assigned a value of type " + sscript.inputElements.get(1).type.type, t.getChild(1).getLine(), t.getChild(2)
								.getCharPositionInLine(), t.getChild(2).getCharPositionInLine() + t.getChild(2).getText().length());
					if (!Assignment.canAssignTo(vde.type.lookuptype, sscript.inputElements.get(0).type.type))
						throw new ScriptException("Incompatible types line " + t.getLine() + ", " + vde.getType().lookuptype
								+ " cannot be assigned a value of type " + sscript.inputElements.get(0).type.type, t.getChild(1).getLine(), t.getChild(2)
								.getCharPositionInLine(), t.getChild(2).getCharPositionInLine() + t.getChild(2).getText().length());
				} else if (vde.getType().type == DType.List)
				{
					if (!Assignment.canAssignTo(vde.type.lookuptype, sscript.inputElements.get(0).type.type))
						throw new ScriptException("Incompatible types line " + t.getLine() + ", " + vde.getType().lookuptype
								+ " cannot be assigned a value of type " + sscript.inputElements.get(0).type.type, t.getChild(1).getLine(), t.getChild(2)
								.getCharPositionInLine(), t.getChild(2).getCharPositionInLine() + t.getChild(2).getText().length());
				} else
				{
					/*
					 * Need to allow more than 1 column for where clause if (selectTree.getChild(0).getChildCount() !=
					 * 1) throw new ScriptException("Query that assigns to a variable must return 1 column",
					 * t.getChild(1).getLine(), t.getChild(2) .getCharPositionInLine(),
					 * t.getChild(2).getCharPositionInLine() + t.getChild(2).getText().length());
					 */
					if (!Assignment.canAssignTo(vde.type.type, sscript.inputElements.get(0).type.type))
						throw new ScriptException("Incompatible types line " + t.getLine() + ", " + vde.getType().type + " cannot be assigned a value of type "
								+ sscript.inputElements.get(0).type.type, t.getChild(1).getLine(), t.getChild(2).getCharPositionInLine(), t.getChild(2)
								.getCharPositionInLine()
								+ t.getChild(2).getText().length());
				}
			} else
			{
				DataType type = Expression.getType(script, etree);
				if (type == null)
					throw new ScriptException("Data element " + etree.getText() + " not found: line " + t.getLine() + ", " + vde.getType().type, t.getChild(2)
							.getLine(), t.getChild(2).getCharPositionInLine(), t.getChild(2).getCharPositionInLine() + t.getChild(2).getText().length());
				if (!Assignment.canAssignTo(vde.type.type, type.type))
					throw new ScriptException("Incompatible types line " + t.getLine() + ", " + vde.getType().type + " cannot be assigned a value of type "
							+ type, t.getChild(1).getLine(), t.getChild(2).getCharPositionInLine(), t.getChild(2).getCharPositionInLine()
							+ t.getChild(2).getText().length());
				Expression ex = new Expression(script);
				compiledAssignment = ex.compile(etree);
			}
		} else
		{
			if (vde.type.type == DType.Integer)
			{
				vde.setValue(0l);
			} else if (vde.type.type == DType.Float)
			{
				vde.setValue(0);
			} else if (vde.type.type == DType.Date || vde.type.type == DType.DateTime)
			{
				vde.setValue(new Date(System.currentTimeMillis()));
			} else if (vde.type.type == DType.List)
			{
				vde.setValue(new ArrayList<Object>());
				script.curMemory += 10 - vde.type.getInitializedSize(); // Size of object
			} else if (vde.type.type == DType.Map)
			{
				vde.setValue(new HashMap<Object, Object>());
				script.curMemory += 10 - vde.type.getInitializedSize(); // Size of object
			}
		}
		if (script.curMemory > (script.maxMemory*0.9))
		{
			logger.warn("ETL Script data structures are consuming 90% of max limit " + (script.maxMemory) + ". Script execution will fail when max limit is reached.");
		}
		if (script.curMemory > script.maxMemory)
			throw new ScriptException("Memory usage exceeds allowed limits", line, start, stop);
	}

	public static synchronized java.util.Date getDate(String s, Repository r, Session session)
	{
		if (s.length() < 2)
			return new java.util.Date();
		s = s.substring(1, s.length() - 1);
		//replace GetVariable with variable value 
		boolean isGetVariable = Pattern.matches("GetVariable\\(\'.*?\'\\)", s);
		if (isGetVariable)
		{
			String vname = s.substring("GetVariable('".length(), s.length() - 2);
			s = r.replaceVariables(session, "V{" + vname + "}");
		}
		java.util.Date d = null;
		for (SimpleDateFormat sdf : dateformats)
		{
			try
			{
				sdf.setTimeZone(r.getServerParameters().getProcessingTimeZone());
				d = sdf.parse(s);
				break;
			} catch (ParseException ex2)
			{
			} catch (NumberFormatException ex3)
			{
			}			
		}
		return d;
	}
	
	private String getTopClause(DatabaseConnection dconn)
	{
		int count = 1; // default limit for non list/map queries
		if (vde.type.type == DType.Map || vde.type.type == DType.List)
		{
			if (sq.topN > 0)
			{
				count = sq.topN;	// map/list can have limits
			}
			else
			{
				return " "; // return the full query result set for the map/list
			}
		}
		if (dconn.supportsTop())
			return "TOP " + count + ' ';
		else if (dconn.supportsRowNum())
			return "ROWNUM <= " + count + ' ';
		else
			return "LIMIT " + count + ' ';
	}

	@Override
	public void execute() throws ScriptException
	{
		if (compiledAssignment == null && sq == null)
			return;
		if (initialized)
			return;
		DType type = vde.getType().type;
		Map<Object, Object> m = type == DType.Map ? new HashMap<Object, Object>() : null;
		List<Object> l = type == DType.List ? new ArrayList<Object>() : null;
		if (sq != null)
		{
			List<Object> sourceTables = new ArrayList<Object>();
			List<String> sourceTableNames = new ArrayList<String>();
			TransformationScript sscript = new TransformationScript(script.getRepository(), script.getSession());
			sscript.setBulkLoaded(script.getBulkLoaded());
			sscript.setLoadNecessaryStagingTables(script.isLoadNecessaryStagingTables());
			sscript.setStatus(script.getStatus());
			sscript.setPath(script.getPath());
			sscript.setDatabasePath(script.getDatabasePath());
			sscript.setDatabaseConnection(script.getDatabaseConnection());
			sscript.setLoadGroup(script.getLoadGroup());
			sscript.setLoadGroups(script.getLoadGroups());
			List<DataElement> delements = new ArrayList<DataElement>();
			for (DataElement de : script.dataElements)
				if (VariableDataElement.class.isInstance(de))
					delements.add(de);
			sscript.dataElements = delements;
			sq = sscript.getSelectQuery(selectTree, script.getNumSourceRows(), sourceTables, sourceTableNames, true, new ArrayList<StagingTable>(), false);
			try
			{
				DatabaseConnection dconn = script.getDatabaseConnection();
				physicalQuery = "SELECT " + (dconn.supportsTop() ? getTopClause(dconn) : "") + sq.projectionList + " FROM "
						+ sq.table + (sq.whereClause == null ? "" : " WHERE " + sq.whereClause)
						+ (sq.groupByClause != null && sq.groupByClause.length() > 0 ? " GROUP BY " + sq.groupByClause : "")
						+ (dconn.supportsLimit() ? " " + getTopClause(dconn) : "");
				if (dconn.supportsRowNum() && getTopClause(dconn).length() > 1)
				{
					physicalQuery = "SELECT * FROM (" + physicalQuery + ") WHERE " + getTopClause(dconn);
				}				
				boolean exists = true;
				if (!sq.isTime && !Database.tableExists(script.getRepository(), dconn, sq.physicalTableName, false))
				{
					exists = false;
				}
				Object result = null;
				if (exists)
				{
					Connection conn = dconn.ConnectionPool.getConnection();
					java.sql.Statement stmt = null;
					ResultSet rs = null;
					try
					{
						// http://docs.aws.amazon.com/redshift/latest/dg/jdbc-fetch-size-parameter.html
						// http://stackoverflow.com/questions/1468036/java-jdbc-ignores-setfetchsize
						// http://jdbc.postgresql.org/documentation/83/query.html#query-with-cursor
						if (dconn.DBType == DatabaseType.Redshift)
							conn.setAutoCommit(false);

						stmt = conn.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
						
						if (script.getRepository().getServerParameters().getScriptQueryTimeout() > 0 && dconn.DBType != DatabaseType.MonetDB)
							stmt.setQueryTimeout(script.getRepository().getServerParameters().getScriptQueryTimeout());
						stmt.setFetchSize(dconn.getInputScriptFetchSize());

						logger.debug(Query.getPrettyQuery(physicalQuery));
						rs = stmt.executeQuery(physicalQuery);
						int size = vde.getType().getInitializedSize();
						while (rs.next())
						{
							result = rs.getObject(1);
							if (type == DType.Map)
							{
								if (vde.getType().lookuptype ==  DType.Integer)
								{
									if (BigDecimal.class.isInstance(result))
										result = ((BigDecimal) result).longValue();
									else if (Long.class.isInstance(result))
										result = ((Long) result).longValue();
									else if (Integer.class.isInstance(result))
										result = ((Integer) result).longValue();
								}
								Object result2 = rs.getObject(2);
								m.put(result, result2);
								script.curMemory += size;
								if (script.curMemory > (script.maxMemory*0.9))
								{
									logger.warn("ETL Script data structures are consuming 90% of max limit " + (script.maxMemory) + ". Script execution will fail when max limit is reached.");
								}
								if (script.curMemory > script.maxMemory)
									throw new ScriptException("Attempt to add map element exceeded memory limits", line, start, stop);
							} else if (type == DType.List)
							{
								l.add(result);
								script.curMemory += size;
								if (script.curMemory > (script.maxMemory*0.9))
								{
									logger.warn("ETL Script data structures are consuming 90% of max limit " + (script.maxMemory) + ". Script execution will fail when max limit is reached.");
								}
								if (script.curMemory > script.maxMemory)
									throw new ScriptException("Attempt to add list element exceeded memory limits", line, start, stop);
							} else
							{
								if (Integer.class.isInstance(result))
									compiledAssignment = new OpInt(Long.valueOf(((Integer) result)).toString());
								else if (Long.class.isInstance(result))
									compiledAssignment = new OpInt(((Long) result).longValue());
								else if (Double.class.isInstance(result))
									compiledAssignment = new OpFloat((Double) result);
								else if (BigDecimal.class.isInstance(result))
								{
									if (type == DType.Integer)
										compiledAssignment = new OpInt(((BigDecimal) result).longValue());
									else
										compiledAssignment = new OpFloat(((BigDecimal) result).doubleValue());
								}
								else if (Float.class.isInstance(result))
									compiledAssignment = new OpFloat((Float) result);
								else if (Date.class.isInstance(result))
								{
									if (dconn.DBType == DatabaseType.Infobright || dconn.DBType == DatabaseType.MYSQL)
										compiledAssignment = new OpDateTime(rs.getDate(1));
									else
										compiledAssignment = new OpDateTime(rs.getDate(1, java.util.Calendar.getInstance(script.getRepository().getServerParameters().getProcessingTimeZone())));
								}
								else if (String.class.isInstance(result))
									compiledAssignment = new OpVarchar((String) result);
								else if (Timestamp.class.isInstance(result) || Time.class.isInstance(result))
								{
									if (dconn.DBType == DatabaseType.Infobright || dconn.DBType == DatabaseType.MYSQL)
										compiledAssignment = new OpDateTime(rs.getTimestamp(1));
									else
										compiledAssignment = new OpDateTime(rs.getTimestamp(1, java.util.Calendar.getInstance(script.getRepository().getServerParameters().getProcessingTimeZone())));
								}
								else if (result == null)
									compiledAssignment = null;
								else
									throw new ScriptException("Unknown datatype returned from initializer query", line, start, stop);
							}
						}
					} catch (SQLException ex)
					{
						if (ex.getMessage().indexOf("The query has timed out") >= 0)
						{
							throw new ScriptException("Script halted. Query timeout exceeded. Please simplify query or contact Birst support.", line, start,
									stop);
						}
						throw new ScriptException("Script halted. Bad query. Please examine your query and contact Birst support if needed.", line, start, stop);
					} finally
					{
						try
						{
							if (rs != null)
								rs.close();
							if (stmt != null)
								stmt.close();
						} catch (SQLException e)
						{
							logger.debug(e, e);
						}
					}
				} else
				{
					if (vde.type.type == DType.Varchar)
						compiledAssignment = new OpVarchar(null);
					else if (vde.type.type == DType.Integer)
						compiledAssignment = new OpInt(null);
					else if (vde.type.type == DType.Float)
						compiledAssignment = new OpFloat(null);
					else if (vde.type.type == DType.Date)
						compiledAssignment = new OpDateTime(new Date(System.currentTimeMillis()));
					else if (vde.type.type == DType.DateTime)
						compiledAssignment = new OpDateTime(new Date(System.currentTimeMillis()));
				}
			} catch (SQLException ex)
			{
				throw new ScriptException("Script halted. Contact Birst support if needed.", line, start, stop);
			}
		}
		Object o = null;
		if (type == DType.Map)
			o = m;
		else if (type == DType.List)
			o = l;
		else if (compiledAssignment == null)
			o = null;
		else
			o = compiledAssignment.evaluate();
		if (type == DType.Varchar && o != null)
		{
			String s = o.toString();
			if (s.length() > vde.getType().width)
				s = s.substring(0, vde.getType().width);
			vde.setValue(s);
		} else {
			vde.setValue(o);
		}
		initialized = true;
	}

	public boolean isInitialized()
	{
		return initialized;
	}

	public void setInitialized(boolean initialized)
	{
		this.initialized = initialized;
	}
}
