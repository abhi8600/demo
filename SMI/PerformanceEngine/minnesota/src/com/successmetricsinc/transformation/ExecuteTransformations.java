/**
 * 
 */
package com.successmetricsinc.transformation;

import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;

/**
 * @author bpeters
 * 
 */
public class ExecuteTransformations
{
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(ExecuteTransformations.class);
	@SuppressWarnings("unused")
	private Repository r;

	public ExecuteTransformations(Repository r)
	{
		this.r = r;
	}

	public void execute(int loadID)
	{
	}
}
