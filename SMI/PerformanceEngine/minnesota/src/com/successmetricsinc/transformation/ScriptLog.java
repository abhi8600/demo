package com.successmetricsinc.transformation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.query.Database;
import com.successmetricsinc.query.DatabaseConnection;

public class ScriptLog
{
	public enum StatusCode
	{
		None, Running, Complete, Failed
	};
	private static Logger logger = Logger.getLogger(ScriptLog.class);
	private Repository r;
	private String tableName;
	private DatabaseConnection dc;

	public ScriptLog(Repository r)
	{
		this.r = r;
		try
		{
			dc = r.getDefaultConnection();
			tableName = "TXN_SCRIPT_LOG";
			if (!Database.tableExists(r, dc, tableName, false))
			{
				List<String> outputColumns = new ArrayList<String>();
				List<String> outputColumnTypes = new ArrayList<String>();
				outputColumns.add("TM");
				outputColumnTypes.add(dc.getDateTimeType());
				outputColumns.add("SOURCE");
				outputColumnTypes.add("VARCHAR(80)");
				outputColumns.add("ITERATION");
				outputColumnTypes.add("VARCHAR(20)");
				outputColumns.add("STATUS");
				outputColumnTypes.add("INTEGER");
				outputColumns.add("NUMINPUTROWS");
				outputColumnTypes.add("BIGINT");
				outputColumns.add("NUMOUTPUTROWS");
				outputColumnTypes.add("BIGINT");
				outputColumns.add("MESSAGE");
				outputColumnTypes.add("VARCHAR(1000)");
				Database.createTable(r, dc, tableName, outputColumns, outputColumnTypes, false, false, null, null, null);
				Database.createIndex(dc, "ITERATION", tableName);
			}
		} catch (SQLException e)
		{
		}
	}

	private int mapStatusToInt(StatusCode status)
	{
		if (status == StatusCode.None)
			return 0;
		if (status == StatusCode.Running)
			return 1;
		if (status == StatusCode.Complete)
			return 2;
		if (status == StatusCode.Failed)
			return 3;
		return 0;
	}

	public void log(String source, String iteration, StatusCode status, long numInputRows, long numOutputRows, String message)
	{
		try
		{
			Connection conn = r.getDefaultConnection().ConnectionPool.getConnection();
			PreparedStatement pstmt = null;
			try
			{
				pstmt = conn.prepareStatement("INSERT INTO " + Database.getQualifiedTableName(dc, tableName)
						+ " (TM, SOURCE, ITERATION, STATUS, NUMINPUTROWS, NUMOUTPUTROWS, MESSAGE) VALUES (" + dc.getGetDate() + ",?,?,?,?,?,?)");
				pstmt.setString(1, source);
				pstmt.setString(2, iteration);
				pstmt.setInt(3, mapStatusToInt(status));
				if (numInputRows < 0)
					pstmt.setNull(4, Types.BIGINT);
				else
					pstmt.setLong(4, numInputRows);
				if (numOutputRows < 0)
					pstmt.setNull(5, Types.BIGINT);
				else
					pstmt.setLong(5, numOutputRows);
				if (message == null)
					pstmt.setNull(6, Types.VARCHAR);
				else
					pstmt.setString(6, message.substring(0, Math.min(message.length(), 1000)));
				pstmt.execute();
			}
			catch (SQLException e)
			{
				logger.warn("Could not save script status information in the database");
			}
			finally
			{
				try
				{
					if (pstmt != null)
						pstmt.close();
				}
				catch (SQLException e)
				{
					logger.debug(e, e);
				}
			}
			if (status == StatusCode.Running)
			{
				logger.info("Starting " + source);
			}
			if (status == StatusCode.Complete)
			{
				logger.info("Finished " + source);
			}
		} catch (SQLException e)
		{
			logger.warn("Could not save script status information in the database");
		}
	}
}
