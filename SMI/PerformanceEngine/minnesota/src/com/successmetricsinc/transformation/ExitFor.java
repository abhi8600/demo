/**
 * 
 */
package com.successmetricsinc.transformation;

/**
 * @author bpeters
 * 
 */
public class ExitFor extends Statement
{
	@Override
	public void execute() throws ScriptException
	{
		ScriptException se = new ScriptException();
		se.setExitFor(true);
		throw se;
	}
}
