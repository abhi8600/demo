/**
 * 
 */
package com.successmetricsinc.transformation;

import java.util.List;

import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.transformation.operators.OpBoolean;
import com.successmetricsinc.warehouse.StagingTable;

/**
 * @author bpeters
 * 
 */
public class WhileLoop extends Statement
{
	private Operator expression;
	private StatementBlock statementBlock;

	public WhileLoop(TransformationScript script, Tree t, List<StagingTable> scriptPath) throws ScriptException
	{
		this.script = script;
		Expression ex = new Expression(script);
		expression = ex.compile(t.getChild(0));
		statementBlock = new StatementBlock(script, t.getChild(1), scriptPath);
		this.line = t.getLine();
		this.start = t.getCharPositionInLine();
		this.stop = t.getText().length() + this.start;
	}

	@Override
	public void execute() throws ScriptException
	{
		if (expression != null)
		{
			Object o = expression.evaluate();
			boolean execute = OpBoolean.isTrue(o);
			while (execute)
			{
				try
				{
					statementBlock.execute();
				} catch (ScriptException se)
				{
					if (se.isExitWhile())
						break;
					throw se;
				}
				o = expression.evaluate();
				execute = OpBoolean.isTrue(o);
			}
		}
	}
}
