package com.successmetricsinc.transformation.varchar;

import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.transformation.Expression;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.Statement;
import com.successmetricsinc.transformation.TransformationScript;

public class Substring extends Operator
{
	private Operator stringValue;
	private Operator startIndex;
	private Operator endIndex;
	private Tree t;
	private TransformationScript script;

	public Substring(TransformationScript script, Tree t) throws ScriptException
	{
		super(script, t);
		this.t = t;
		this.script = script;
		Expression ex = new Expression(script);
		stringValue = ex.compile(t.getChild(0));
		if (stringValue.getType() != Operator.Type.Varchar)
			throw new ScriptException("Substring may only be applied to varchar data elements", t.getChild(0).getLine(), t.getChild(0).getCharPositionInLine(),
					t.getChild(0).getCharPositionInLine() + t.getChild(0).getText().length());
		startIndex = ex.compile(t.getChild(1));
		if (startIndex.getType() != Operator.Type.Integer)
			throw new ScriptException("Substring start index must be an integer", t.getChild(1).getLine(), t.getChild(1).getCharPositionInLine(), t.getChild(1)
					.getCharPositionInLine() + t.getChild(1).getText().length());
		if (t.getChildCount() == 3)
		{
			endIndex = ex.compile(t.getChild(2));
			if (endIndex.getType() != Operator.Type.Integer)
				throw new ScriptException("Substring end index must be an integer", t.getChild(2).getLine(), t.getChild(2).getCharPositionInLine(), t.getChild(
						2).getCharPositionInLine()
						+ t.getChild(2).getText().length());
		}
	}

	@Override
	public Operator clone()
	{
		try
		{
			return new Substring(script, t);
		} catch (ScriptException e)
		{
			return null;
		}
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		return evaluate().toString().compareTo(b.evaluate().toString());
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		return evaluate().toString().equals(b.evaluate().toString());
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		Object o = stringValue.evaluate();
		if (o == null)
			throw new ScriptException("Cannot evaluate a substring on a null string: " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(),
					t.getCharPositionInLine() + t.getText().length());
		String s = o.toString();
		int start = ((Long) startIndex.evaluate()).intValue();  
		if (endIndex == null)
		{
			try
			{
				return s.substring(start);
			} catch (IndexOutOfBoundsException ex)
			{
				throw new ScriptException("Varchar index out of bounds in substring (" + start + "): " + Statement.toString(t, null), t.getLine(),
						t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
			}
		} else
		{
			int end = ((Long) endIndex.evaluate()).intValue();
			if (end < start)
			{
				throw new ScriptException("Varchar index out of bounds in substring (" + end + " < " + start + "): " + Statement.toString(t, null),
						t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
			}
			try
			{
				return s.substring(start, Math.min(end, s.length()));
			} catch (IndexOutOfBoundsException ex)
			{
				throw new ScriptException("Varchar index out of bounds in substring (" + start + "," + end + "): " + Statement.toString(t, null), t.getLine(),
						t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
			}
		}
	}

	@Override
	public int getSize()
	{
		try
		{
			return evaluate().toString().length();
		} catch (ScriptException e)
		{
			return 0;
		}
	}

	@Override
	public Type getType()
	{
		return Operator.Type.Varchar;
	}
}
