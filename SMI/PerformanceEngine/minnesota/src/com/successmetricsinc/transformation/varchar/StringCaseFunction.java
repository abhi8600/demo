package com.successmetricsinc.transformation.varchar;

import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.transformation.BirstScriptParser;
import com.successmetricsinc.transformation.Expression;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.Statement;
import com.successmetricsinc.transformation.TransformationScript;

public class StringCaseFunction extends Operator
{
	private Tree t;
	private TransformationScript script;
	private Operator stringValue;

	public StringCaseFunction(TransformationScript script, Tree t) throws ScriptException
	{
		super(script, t);
		this.t = t;
		this.script = script;
		Expression ex = new Expression(script);
		stringValue = ex.compile(t.getChild(0));
		if (stringValue.getType() != Operator.Type.Varchar)
			throw new ScriptException(t.getText() + " may only be applied to varchar data elements: " + Statement.toString(t.getChild(0), null), t.getLine(),
					t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
	}

	@Override
	public Operator clone()
	{
		try
		{
			return new StringCaseFunction(script, t);
		} catch (ScriptException e)
		{
			return null;
		}
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		return evaluate().toString().compareTo(b.evaluate().toString());
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		return evaluate().toString().equals(b.evaluate().toString());
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		Object o = stringValue.evaluate();
		if (o == null)
			return null;
		String s = o.toString();
		if (t.getType() == BirstScriptParser.TOUPPER)
			return s.toUpperCase();
		else if (t.getType() == BirstScriptParser.TOLOWER)
			return s.toLowerCase();
		else if (t.getType() == BirstScriptParser.TRIM)
			return s.trim();
		throw new ScriptException("Unknown function: " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine()
				+ t.getText().length());
	}

	@Override
	public int getSize()
	{
		try
		{
			return evaluate().toString().length();
		} catch (ScriptException e)
		{
			return 0;
		}
	}

	@Override
	public Type getType()
	{
		return Operator.Type.Varchar;
	}
}
