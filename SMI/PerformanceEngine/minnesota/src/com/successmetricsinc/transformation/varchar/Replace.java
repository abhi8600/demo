package com.successmetricsinc.transformation.varchar;

import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.transformation.BirstScriptParser;
import com.successmetricsinc.transformation.Expression;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.Statement;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.util.Util;

public class Replace extends Operator
{
	private Operator stringValue;
	private Operator searchValue;
	private Operator replaceValue;
	private Tree t;
	private TransformationScript script;
	private boolean all = false;

	public Replace(TransformationScript script, Tree t) throws ScriptException
	{
		super(script, t);
		this.t = t;
		this.script = script;
		Expression ex = new Expression(script);
		stringValue = ex.compile(t.getChild(0));
		if (stringValue.getType() != Operator.Type.Varchar)
			throw new ScriptException("Replace may only be applied to varchar data elements", t.getChild(0).getLine(), t.getChild(0).getCharPositionInLine(), t
					.getChild(0).getCharPositionInLine() + t.getChild(0).getText().length());
		searchValue = ex.compile(t.getChild(1));
		if (searchValue.getType() != Operator.Type.Varchar)
			throw new ScriptException("Replace value must be a varchar", t.getChild(1).getLine(), t.getChild(1).getCharPositionInLine(), t.getChild(1)
					.getCharPositionInLine() + t.getChild(1).getText().length());
		if (t.getChildCount() == 3)
		{
			replaceValue = ex.compile(t.getChild(2));
			if (replaceValue.getType() != Operator.Type.Varchar)
				throw new ScriptException("Replace value must be a varchar", t.getChild(2).getLine(), t.getChild(2).getCharPositionInLine(), t.getChild(2)
						.getCharPositionInLine() + t.getChild(2).getText().length());
		}
		if (t.getType() == BirstScriptParser.REPLACEALL)
			all = true;
	}

	@Override
	public Operator clone()
	{
		try
		{
			return new Replace(script, t);
		} catch (ScriptException e)
		{
			return null;
		}
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		return evaluate().toString().compareTo(b.evaluate().toString());
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		return evaluate().toString().equals(b.evaluate().toString());
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		Object o = stringValue.evaluate();
		if (o == null)
			throw new ScriptException("Cannot evaluate a replace on a null string: " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(),
					t.getCharPositionInLine() + t.getText().length());
		String s = o.toString();
		o = searchValue.evaluate();
		if (o == null)
			return s;
		String lookup = o.toString();
		o = replaceValue.evaluate();
		if (o == null)
			return s;
		String replace = o.toString();
		if (all)
			return s.replaceAll(lookup, replace);
		else
			return Util.replaceStr(s, lookup, replace);
	}

	@Override
	public int getSize()
	{
		try
		{
			return evaluate().toString().length();
		} catch (ScriptException e)
		{
			return 0;
		}
	}

	@Override
	public Type getType()
	{
		return Operator.Type.Varchar;
	}
}
