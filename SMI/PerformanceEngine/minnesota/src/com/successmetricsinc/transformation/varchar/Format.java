package com.successmetricsinc.transformation.varchar;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.transformation.Expression;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.Statement;
import com.successmetricsinc.transformation.TransformationScript;

public class Format extends Operator
{
	private Operator formatValue;
	private Operator pattern;
	private TransformationScript script;
	private Tree t;

	public Format(TransformationScript script, Tree t) throws ScriptException
	{
		super(script, t);
		this.script = script;
		this.t = t;
		Expression ex = new Expression(script);
		formatValue = ex.compile(t.getChild(0));
		if (formatValue.getType() != Operator.Type.DateTime && formatValue.getType() != Operator.Type.Float && formatValue.getType() != Operator.Type.Integer)
			throw new ScriptException("Format may only be applied to date and number elements", t.getChild(0).getLine(), t.getChild(0).getCharPositionInLine(),
					t.getChild(0).getCharPositionInLine() + t.getChild(0).getText().length());
		pattern = ex.compile(t.getChild(1));
		if (pattern.getType() != Operator.Type.Varchar)
			throw new ScriptException("Format string must be a varchar", t.getChild(1).getLine(), t.getChild(1).getCharPositionInLine(), t.getChild(1)
					.getCharPositionInLine() + t.getChild(1).getText().length());
	}

	@Override
	public Operator clone()
	{
		try
		{
			return new Format(script, t);
		} catch (ScriptException e)
		{
			return null;
		}
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		return evaluate().toString().compareTo(b.evaluate().toString());
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		return evaluate().toString().equals(b.evaluate().toString());
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		Object fo = formatValue.evaluate();
		Object po = pattern.evaluate();
		if (fo == null || po == null)
			throw new ScriptException("Cannot use Format with null values: " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(),
					t.getCharPositionInLine() + t.getText().length());
		String ps = po.toString();
		try
		{
			if (formatValue.getType() == Operator.Type.DateTime)
			{
				if (java.sql.Date.class.isInstance(fo))
				{
					SimpleDateFormat sdf = new SimpleDateFormat(ps);
					return sdf.format((Date) fo);
				}
				if (Date.class.isInstance(fo))
				{
					SimpleDateFormat sdf = new SimpleDateFormat(ps);
					return sdf.format((Date) fo);
				}
				if (GregorianCalendar.class.isInstance(fo))
				{
					SimpleDateFormat sdf = new SimpleDateFormat(ps);
					return sdf.format(((GregorianCalendar) fo).getTime());
				}
				if (Calendar.class.isInstance(fo))
				{
					SimpleDateFormat sdf = new SimpleDateFormat(ps);
					return sdf.format(((Calendar) fo).getTime());
				}
				throw new ScriptException("Format expected a DateTime, but the value was not a Date or Calendar (" + fo + "): " + Statement.toString(t, null),
						t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
			} else
			{
				DecimalFormat df = new DecimalFormat(ps);
				if (Integer.class.isInstance(fo))
					return df.format((Integer) fo);
				if (Double.class.isInstance(fo))
					return df.format((Double) fo);
				if (Long.class.isInstance(fo))
					return df.format((Long) fo);
				if (BigDecimal.class.isInstance(fo))
					return df.format((BigDecimal) fo);
				throw new ScriptException("Format expected a number, but the value was not a number (" + fo + "): " + Statement.toString(t, null), t.getLine(),
						t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
			}
		} catch (IllegalArgumentException ex)
		{
			throw new ScriptException("Format string is not valid (" + ps + "): " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(),
					t.getCharPositionInLine() + t.getText().length());
		}
	}

	@Override
	public int getSize()
	{
		try
		{
			return evaluate().toString().length();
		} catch (ScriptException e)
		{
			return 0;
		}
	}

	@Override
	public Type getType()
	{
		return Operator.Type.Varchar;
	}
}
