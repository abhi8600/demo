package com.successmetricsinc.transformation.varchar;

import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.transformation.Expression;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.Statement;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.transformation.operators.OpInt;

public class Position extends Operator
{
	private Operator findValue;
	private Operator searchValue;
	private Operator startIndex;
	private Tree t;
	private TransformationScript script;

	public Position(TransformationScript script, Tree t) throws ScriptException
	{
		super(script, t);
		this.t = t;
		this.script = script;
		Expression ex = new Expression(script);
		findValue = ex.compile(t.getChild(0));
		if (findValue.getType() != Operator.Type.Varchar)
			throw new ScriptException("Position may only be applied to varchar data elements: " + Statement.toString(t.getChild(0), null), t.getChild(0)
					.getLine(), t.getChild(0).getCharPositionInLine(), t.getChild(0).getCharPositionInLine() + t.getChild(0).getText().length());
		searchValue = ex.compile(t.getChild(1));
		if (searchValue.getType() != Operator.Type.Varchar)
			throw new ScriptException("Position may only be applied to varchar data elements: " + Statement.toString(t.getChild(1), null), t.getChild(1)
					.getLine(), t.getChild(1).getCharPositionInLine(), t.getChild(1).getCharPositionInLine() + t.getChild(1).getText().length());
		if (t.getChildCount() == 3)
		{
			startIndex = ex.compile(t.getChild(2));
			if (startIndex.getType() != Operator.Type.Integer)
				throw new ScriptException("Position start index may only be used with integer data elements: " + Statement.toString(t.getChild(1), null), t
						.getChild(1).getLine(), t.getChild(1).getCharPositionInLine(), t.getChild(1).getCharPositionInLine() + t.getChild(1).getText().length());
		}
	}

	@Override
	public Operator clone()
	{
		try
		{
			return new Position(script, t);
		} catch (ScriptException e)
		{
			return null;
		}
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		long result = (Long) evaluate();
		if (OpInt.class.isInstance(b))
		{
			long br = (Long) b.evaluate();
			return result > br ? 1 : (result == br ? 0 : -1);
		}
		return 0;
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		if (OpInt.class.isInstance(b))
		{
			long br = (Long) b.evaluate();
			return (Long) evaluate() == br;
		}
		return false;
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		Object fo = findValue.evaluate();
		Object so = searchValue.evaluate();
		Object starto = null;
		if (startIndex != null)
			starto = startIndex.evaluate();
		if (fo == null)
			throw new ScriptException("Cannot use Position with a null find value: " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(),
					t.getCharPositionInLine() + t.getText().length());
		if (so == null)
			throw new ScriptException("Cannot use Position with a null search value: " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(),
					t.getCharPositionInLine() + t.getText().length());
		String fs = fo.toString().toLowerCase();
		String ss = so.toString().toLowerCase();
		if (starto != null && starto instanceof Integer)
			return ((Integer) ss.indexOf(fs, (Integer) starto)).longValue();
		else if (starto != null && starto instanceof Long)
			return ((Integer) ss.indexOf(fs, ((Long) starto).intValue())).longValue();
		else
			return ((Integer) (ss.indexOf(fs))).longValue();
	}

	@Override
	public int getSize()
	{
		return Integer.SIZE / 8;
	}

	@Override
	public Type getType()
	{
		return Operator.Type.Integer;
	}
}
