package com.successmetricsinc.transformation.varchar;

import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.transformation.Expression;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.TransformationScript;

public class ToString extends Operator
{
	private Operator value;
	private Tree t;
	private TransformationScript script;

	public ToString(TransformationScript script, Tree t) throws ScriptException
	{
		super(script, t);
		this.t = t;
		this.script = script;
		Expression ex = new Expression(script);
		value = ex.compile(t.getChild(0));
	}

	@Override
	public Operator clone()
	{
		try
		{
			return new ToString(script, t);
		} catch (ScriptException e)
		{
			return null;
		}
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		return evaluate().toString().compareTo(b.evaluate().toString());
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		return evaluate().toString().equals(b.evaluate().toString());
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		Object o = value.evaluate();
		if (o == null)
			return null;
		String s = o.toString();
		return s;
	}

	@Override
	public int getSize()
	{
		try
		{
			return evaluate().toString().length();
		} catch (ScriptException e)
		{
			return 0;
		}
	}

	@Override
	public Type getType()
	{
		return Operator.Type.Varchar;
	}
}
