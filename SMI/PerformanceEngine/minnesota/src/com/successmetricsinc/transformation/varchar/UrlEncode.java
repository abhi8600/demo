package com.successmetricsinc.transformation.varchar;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.transformation.Expression;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.Statement;
import com.successmetricsinc.transformation.TransformationScript;

public class UrlEncode extends Operator
{
	private Operator urlParameter;
	private Operator encoding;
	private Tree t;
	private TransformationScript script;

	public UrlEncode(TransformationScript script, Tree t) throws ScriptException
	{
		super(script, t);
		this.t = t;
		this.script = script;
		Expression ex = new Expression(script);
		urlParameter = ex.compile(t.getChild(0));
		if (urlParameter.getType() != Operator.Type.Varchar)
			throw new ScriptException("URLENCODE argument to be encoded must be a varchar: " + Statement.toString(t.getChild(0), null),
					t.getChild(0).getLine(), t.getChild(0).getCharPositionInLine(), t.getChild(0).getCharPositionInLine() + t.getChild(0).getText().length());
		encoding = ex.compile(t.getChild(1));
		if (encoding.getType() != Operator.Type.Varchar)
			throw new ScriptException("URLENCODE encoding value must be a varchar: " + Statement.toString(t.getChild(1), null), t.getChild(1).getLine(), t
					.getChild(1).getCharPositionInLine(), t.getChild(1).getCharPositionInLine() + t.getChild(1).getText().length());
	}

	@Override
	public Operator clone()
	{
		try
		{
			return new UrlEncode(script, t);
		} catch (ScriptException e)
		{
			return null;
		}
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		return evaluate().toString().compareTo(b.evaluate().toString());
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		return evaluate().toString().equals(b.evaluate().toString());
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		Object fo = urlParameter.evaluate();
		if (fo == null)
			return null;
		Object po = encoding.evaluate();
		if (po == null)
			throw new ScriptException("Cannot use URLENCODE with null encoding value: " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(),
					t.getCharPositionInLine() + t.getText().length());
		String fs = fo.toString();
		String ps = po.toString();
		try
		{
			return URLEncoder.encode(fs, ps);
		} catch (UnsupportedEncodingException ex)
		{
			throw new ScriptException("URLENCOED encoding value is not supported (" + ps + "): " + Statement.toString(t, null), t.getLine(),
					t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
	}

	@Override
	public int getSize()
	{
		try
		{
			return evaluate().toString().length();
		} catch (ScriptException e)
		{
			return 0;
		}
	}

	@Override
	public Type getType()
	{
		return Operator.Type.Varchar;
	}
}
