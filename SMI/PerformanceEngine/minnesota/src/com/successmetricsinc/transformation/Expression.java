package com.successmetricsinc.transformation;

import java.util.ArrayList;
import java.util.List;

import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.query.BadColumnNameException;
import com.successmetricsinc.query.DimensionColumn;
import com.successmetricsinc.query.DimensionExpressionColumn;
import com.successmetricsinc.query.DisplayExpression;
import com.successmetricsinc.query.LogicalQueryString;
import com.successmetricsinc.query.MeasureColumn;
import com.successmetricsinc.query.MeasureExpressionColumn;
import com.successmetricsinc.query.NavigationException;
import com.successmetricsinc.query.Query;
import com.successmetricsinc.query.QueryFilter;
import com.successmetricsinc.query.SyntaxErrorException;
import com.successmetricsinc.query.DisplayExpression.MeasureExpressionType;
import com.successmetricsinc.query.DisplayExpression.PositionType;
import com.successmetricsinc.query.olap.DimensionMemberSetNav;
import com.successmetricsinc.query.olap.OlapLogicalQueryString;
import com.successmetricsinc.query.olap.OlapMemberExpression;
import com.successmetricsinc.transformation.DataType.DType;
import com.successmetricsinc.transformation.agg.Aggregate;
import com.successmetricsinc.transformation.date.DateAdd;
import com.successmetricsinc.transformation.date.DateDiff;
import com.successmetricsinc.transformation.date.DatePart;
import com.successmetricsinc.transformation.integer.ToTime;
import com.successmetricsinc.transformation.math.Math;
import com.successmetricsinc.transformation.math.Random;
import com.successmetricsinc.transformation.object.FunctionCall;
import com.successmetricsinc.transformation.object.GetColumnValue;
import com.successmetricsinc.transformation.object.Groups;
import com.successmetricsinc.transformation.object.IIF;
import com.successmetricsinc.transformation.object.Ifnull;
import com.successmetricsinc.transformation.object.Length;
import com.successmetricsinc.transformation.object.Let;
import com.successmetricsinc.transformation.object.NumRows;
import com.successmetricsinc.transformation.object.RExpression;
import com.successmetricsinc.transformation.object.RowNumber;
import com.successmetricsinc.transformation.object.SavedExpression;
import com.successmetricsinc.transformation.object.SetLookup;
import com.successmetricsinc.transformation.object.Transform;
import com.successmetricsinc.transformation.object.Trend;
import com.successmetricsinc.transformation.operators.OpBinaryBoolean;
import com.successmetricsinc.transformation.operators.OpBinaryFloat;
import com.successmetricsinc.transformation.operators.OpBinaryInt;
import com.successmetricsinc.transformation.operators.OpBinaryVarchar;
import com.successmetricsinc.transformation.operators.OpBoolean;
import com.successmetricsinc.transformation.operators.OpDateTime;
import com.successmetricsinc.transformation.operators.OpFloat;
import com.successmetricsinc.transformation.operators.OpInt;
import com.successmetricsinc.transformation.operators.OpList;
import com.successmetricsinc.transformation.operators.OpUnaryBoolean;
import com.successmetricsinc.transformation.operators.OpUnaryDateTime;
import com.successmetricsinc.transformation.operators.OpUnaryFloat;
import com.successmetricsinc.transformation.operators.OpUnaryInt;
import com.successmetricsinc.transformation.operators.OpUnaryVarchar;
import com.successmetricsinc.transformation.operators.OpVarchar;
import com.successmetricsinc.transformation.transforms.GetLevelAttribute;
import com.successmetricsinc.transformation.transforms.GetLevelValue;
import com.successmetricsinc.transformation.transforms.NextChild;
import com.successmetricsinc.transformation.varchar.Format;
import com.successmetricsinc.transformation.varchar.Position;
import com.successmetricsinc.transformation.varchar.Replace;
import com.successmetricsinc.transformation.varchar.StringCaseFunction;
import com.successmetricsinc.transformation.varchar.Substring;
import com.successmetricsinc.transformation.varchar.ToString;
import com.successmetricsinc.transformation.varchar.UrlEncode;

public class Expression
{
	private TransformationScript script;
	private boolean logicalQuery;
	private DisplayExpression displayExpression;
	public boolean isRank;
	public boolean isPTile;
	public boolean isAggregate;
	public boolean isRSum;
	
	public Expression(TransformationScript script)
	{
		this.script = script;
		this.displayExpression = script.getDisplayExpression();
		this.logicalQuery = script.isLogicalQuery();
	}

	public Operator compile(Tree tree) throws ScriptException
	{
		Operator compiledAssignment = null;
		int type = tree.getType();
		switch (type)
		{
		case BirstScriptParser.PAREN:
			compiledAssignment = compile(tree.getChild(0));
			break;
		case BirstScriptParser.INTEGER:
			compiledAssignment = new OpInt(tree.getText());
			break;
		case BirstScriptParser.FLOAT:
			compiledAssignment = new OpFloat(tree.getText());
			break;
		case BirstScriptParser.STRING:
			String s = tree.getText();
			compiledAssignment = new OpVarchar(s.substring(1, s.length() - 1));
			break;
		case BirstScriptParser.DATETIME:
			compiledAssignment = new OpDateTime(tree.getText(), script);
			break;
		case BirstScriptParser.NULL:
			return null;
		case BirstScriptParser.BOOLEAN:
			compiledAssignment = new OpBoolean(tree.getText());
			break;
		case BirstScriptParser.EQUALS:
		case BirstScriptParser.LT:
		case BirstScriptParser.GT:
		case BirstScriptParser.LTEQ:
		case BirstScriptParser.GTEQ:
		case BirstScriptParser.NOTEQUALS:
		case BirstScriptParser.AND:
		case BirstScriptParser.OR:
		case BirstScriptParser.LIKE:
		case BirstScriptParser.NOTLIKE:
		case BirstScriptParser.IN:
		case BirstScriptParser.NOTIN:
			compiledAssignment = new OpBinaryBoolean(compile(tree.getChild(0)), compile(tree.getChild(1)), tree.getType());
			break;
		case BirstScriptParser.ISNULL:
		case BirstScriptParser.ISNOTNULL:
		case BirstScriptParser.ISNAN:
		case BirstScriptParser.ISINF:
			compiledAssignment = new OpUnaryBoolean(compile(tree.getChild(0)), tree.getType());
			break;
		case BirstScriptParser.VALUELIST: // list of constant values, such as ('major', 'minor') or (1, 2, 3) for use in
			// IN/NOT IN
			List<Operator> lst = new ArrayList<Operator>(tree.getChildCount());
			for (int i = 0; i < tree.getChildCount(); i++)
			{
				lst.add(compile(tree.getChild(i)));
			}
			compiledAssignment = new OpList(lst);
			break;
		case BirstScriptParser.PLUS:
		case BirstScriptParser.MINUS:
		case BirstScriptParser.DIV:
		case BirstScriptParser.MULT:
		case BirstScriptParser.MOD:
			Tree cl = tree.getChild(0);
			Tree cr = tree.getChild(1);
			/* this optimization doesn't work, the single quotes are not properly stripped, let the OpBinaryVarchart does this
			if (tree.getType() == BirstScriptParser.PLUS && cl.getType() == BirstScriptParser.STRING && cr.getType() == BirstScriptParser.STRING)
			{
				compiledAssignment = new OpVarchar(cl.getText() + cr.getText());
				break;
			}
			*/
			DataType dl = getType(script, cl, logicalQuery);
			DataType dr = getType(script, cr, logicalQuery);
			if (dl.type == DType.Integer && dr.type == DType.Integer)
				compiledAssignment = new OpBinaryInt(compile(tree.getChild(0)), compile(tree.getChild(1)), tree.getType());
			else if ((dl.type == DType.Integer || dl.type == DType.Float) && (dr.type == DType.Integer || dr.type == DType.Float))
				compiledAssignment = new OpBinaryFloat(compile(tree.getChild(0)), compile(tree.getChild(1)), tree.getType());
			else if (tree.getType() == BirstScriptParser.PLUS && dl.type == DType.Varchar || dr.type == DType.Varchar)
				compiledAssignment = new OpBinaryVarchar(compile(tree.getChild(0)), compile(tree.getChild(1)), tree.getType(), null);
			else
				throw new ScriptException("Invalid expression: line " + tree.getLine() + ", column " + tree.getCharPositionInLine(), tree.getLine(),
						tree.getCharPositionInLine(), tree.getCharPositionInLine() + tree.getText().length());
			break;
		case BirstScriptParser.BITAND:
		case BirstScriptParser.BITOR:
			cl = tree.getChild(0);
			cr = tree.getChild(1);
			dl = getType(script, cl, logicalQuery);
			dr = getType(script, cr, logicalQuery);
			if (dl.type == DType.Integer && dr.type == DType.Integer)
				compiledAssignment = new OpBinaryInt(compile(tree.getChild(0)), compile(tree.getChild(1)), tree.getType());
			else
				throw new ScriptException("Invalid expression: line " + tree.getLine() + ", column " + tree.getCharPositionInLine(), tree.getLine(),
						tree.getCharPositionInLine(), tree.getCharPositionInLine() + tree.getText().length());
			break;
		case BirstScriptParser.NEGATE:
			cl = tree.getChild(0);
			dl = getType(script, cl, logicalQuery);
			if (dl.type == DType.Integer)
				compiledAssignment = new OpUnaryInt(compile(tree.getChild(0)), tree.getType());
			else
				compiledAssignment = new OpUnaryFloat(compile(tree.getChild(0)), tree.getType());
			break;
		case BirstScriptParser.MDXEXPRESSION:
			String expression = tree.getChild(0).getText();
			String display = tree.getChildCount() > 1 ? tree.getChild(1).getChild(0).getText() : "M" + new java.util.Random().nextInt();
			OlapMemberExpression exp = new OlapMemberExpression();
			exp.expression = expression;
			exp.displayName = display; 
			List<OlapMemberExpression> olapMemberExpressionsList = displayExpression.geOlapMemberExpressions();
			LogicalQueryDataElement olaplqde = new LogicalQueryDataElement();
			olaplqde.index = olapMemberExpressionsList.size();
			olapMemberExpressionsList.add(exp);
			olaplqde.displayExpression = displayExpression;
			olaplqde.fieldType = DisplayExpression.OLAP_TYPE;
			olaplqde.type = new DataType(DType.Varchar, 0);
			return new OpVarchar(olaplqde, null);			
		case BirstScriptParser.AVG:
		case BirstScriptParser.SUM:
		case BirstScriptParser.COUNT:
		case BirstScriptParser.COUNTDISTINCT:
		case BirstScriptParser.MIN:
		case BirstScriptParser.MAX:
		case BirstScriptParser.COLUMN_NAME:
		case BirstScriptParser.IDENT_NAME:
		case BirstScriptParser.GRAIN_COLUMN:
		case BirstScriptParser.SPARSE:
		case BirstScriptParser.LEVEL_COLUMN:
			DataElement de = null;
			if (logicalQuery)
			{
				if (type == BirstScriptParser.IDENT_NAME || type == BirstScriptParser.COLUMN_NAME)
				{
					de = script.getNonOutputElement(tree.getText());
					if (de != null)
					{
						if (de.getType().type == DType.Varchar)
							return new OpVarchar(de, null);
						else if (de.getType().type == DType.Integer)
							return new OpInt(de, null);
						else if (de.getType().type == DType.Float)
							return new OpFloat(de, null);
						else if (de.getType().type == DType.DateTime || de.getType().type == DType.Date)
							return new OpDateTime(de, null);
						else if (de.getType().type == DType.List)
							throw new ScriptException("Invalid expression, cannot reference an array without an index", tree.getLine(),
									tree.getCharPositionInLine(), tree.getCharPositionInLine() + tree.getText().length());
					}
				}
				String name = tree.getText();
				if (type == BirstScriptParser.RANK || type == BirstScriptParser.DRANK || type == BirstScriptParser.PTILE)
				{
					Tree rchild = tree.getChild(0);
					if (rchild.getType() != BirstScriptParser.IDENT_NAME && tree.getParent() != null
							&& tree.getParent().getType() != BirstScriptParser.PROJECTIONLIST)
						throw new ScriptException("Rank may only be used on measures or as the top-most function in an expression", tree.getLine(),
								tree.getCharPositionInLine(), tree.getCharPositionInLine() + tree.getText().length());
					if (tree.getParent() != null && tree.getParent().getType() == BirstScriptParser.PROJECTIONLIST)
					{
						if (type == BirstScriptParser.RANK || type == BirstScriptParser.DRANK)
							isRank = true;
						else if (type == BirstScriptParser.PTILE)
							isPTile = true;
						return compile(rchild);
					}
					name = rchild.getText();
				}
				name = name.substring(1, name.length() - 1);
				if (type == BirstScriptParser.IDENT_NAME || type == BirstScriptParser.RANK || type == BirstScriptParser.DRANK
						|| type == BirstScriptParser.PTILE)
				{
					MeasureExpressionColumn mec = new MeasureExpressionColumn();
					mec.expression = tree.getText();
					MeasureColumn mc = script.getRepository().findMeasureColumn(name);
					if (mc == null)
						throw new ScriptException("Unknown measure name: " + tree.getText(), tree.getLine(), tree.getCharPositionInLine(),
								tree.getCharPositionInLine() + tree.getText().length());
					mec.displayName = name;
					mec.columnName = name;
					mec.setMeasureColumn(mc);
					// Rank and PTile are deprecated here - new ranking/agg functions to be used instead of
					// queryresultset-based ranking
					if (type == BirstScriptParser.RANK)
						mec.type = MeasureExpressionType.Rank;
					else if (type == BirstScriptParser.DRANK)
						mec.type = MeasureExpressionType.DenseRank;
					else if (type == BirstScriptParser.PTILE)
						mec.type = MeasureExpressionType.Percentile;
					else
						mec.type = MeasureExpressionType.Normal;
					if (mec.type != MeasureExpressionType.Normal)
						mec.displayName += mec.type == MeasureExpressionType.Rank ? "_RANK" : mec.type == MeasureExpressionType.DenseRank ? "_DRANK" : "_PTILE";
					// Add measure filters and groups
					for (int j = 0; j < tree.getChildCount(); j++)
					{
						if (tree.getChild(j).getType() == BirstScriptParser.WHERE)
						{
							QueryFilter qf;
							try
							{
								qf = LogicalQueryString.validateTreeFilter(tree.getChild(j).getChild(0), script.getRepository(), script.getSession(), true);
							} catch (SyntaxErrorException e)
							{
								throw new ScriptException("Unable to apply filter to measure", tree.getLine(), tree.getCharPositionInLine(),
										tree.getCharPositionInLine() + tree.getText().length());
							}
							mec.filter = qf;
							try
							{
								mec.displayName = mec.displayName
										+ (mec.filter != null ? java.lang.Math.abs(mec.filter.getQueryKeyStr(mec.query, null).hashCode()) : "");
							} catch (NavigationException e)
							{
								throw new ScriptException("Unnable to navigate expression: " + tree.getText(), tree.getLine(), tree.getCharPositionInLine(),
										tree.getCharPositionInLine() + tree.getText().length());
							} catch (CloneNotSupportedException e)
							{
								throw new ScriptException("Error: " + tree.getText(), tree.getLine(), tree.getCharPositionInLine(),
										tree.getCharPositionInLine() + tree.getText().length());
							} catch (BadColumnNameException e)
							{
								throw new ScriptException("Bad column name: " + tree.getText(), tree.getLine(), tree.getCharPositionInLine(),
										tree.getCharPositionInLine() + tree.getText().length());
							}
						} else if (tree.getChild(j).getType() == BirstScriptParser.BY)
						{
							mec.groups = new ArrayList<String>();
							for (int i = 0; i < tree.getChild(j).getChildCount(); i++)
							{
								String gname = tree.getChild(j).getChild(i).getText();
								gname = gname.substring(1, gname.length() - 1);
								mec.groups.add(gname);
							}
							if (mec.type != MeasureExpressionType.Normal)
								mec.displayName += mec.type == MeasureExpressionType.Rank ? "_GRANK" : mec.type == MeasureExpressionType.DenseRank ? "_GDRANK"
										: "_GPTILE";
						}
					}
					mec.positions = new ArrayList<com.successmetricsinc.query.Position>();
					for (int i = 0; i < tree.getChildCount(); i++)
					{
						Tree posChild = tree.getChild(i);
						if (posChild.getType() != BirstScriptParser.POSITION)
							continue;
						boolean isPositional = isPositionalCalc(posChild);
						com.successmetricsinc.query.Position pos = new com.successmetricsinc.query.Position();
						if (isPositional)
						{
							String ps = posChild.getChild(0).getText().substring(1); // get rid of leading {
							int index = ps.indexOf('.');
							pos.pDim = ps.substring(1, index); // get rid of leading [
							pos.pCol = ps.substring(index + 1, ps.length() - 1); // get rid of trailing ]
							if (posChild.getChildCount() == 3)
							{
								String operator = posChild.getChild(1).getText();
								if (operator.equals("="))
									pos.ptype = PositionType.Absolute;
								else if (operator.equals("+="))
									pos.ptype = PositionType.RelativePlus;
								else if (operator.equals("-="))
									pos.ptype = PositionType.RelativeMinus;
								Tree valChild = posChild.getChild(2);
								if (valChild.getType() == BirstScriptParser.STRING)
								{
									String pvs = valChild.getText();
									pos.pVal = pvs.substring(1, pvs.length()-1);
								}
								else if (valChild.getType() == BirstScriptParser.INTEGER || valChild.getType() == BirstScriptParser.FLOAT)
									pos.pVal = Double.valueOf(valChild.getText());
								else
									pos.pVal = valChild.getText();
							} else
								pos.ptype = PositionType.Absolute;
						} else
						{
							if (mec.getAggRule() == MeasureExpressionColumn.AggregationRule.CountDistinct || mec.getAggRule() == MeasureExpressionColumn.AggregationRule.Avg)
							{
								throw new ScriptException(
										"Dimension expressions unavailable for non-additive aggregations COUNT DISTINCT and AVG (Note: AVG may be decomposed into SUM and COUNT for this purpose): "
												+ tree.getText(), tree.getLine(), tree.getCharPositionInLine(), tree.getCharPositionInLine()
												+ tree.getText().length());
							}
							pos.ptype = PositionType.DimensionExpression;							
							pos.dimensionExpression = posChild.getChild(0);
							// Create a unique name for this column so it can be mapped
							mec.displayName += System.currentTimeMillis();
							// If there are any level specifiers, capture them
							pos.dimensionExpressionLevels = new String[posChild.getChildCount() - 1];
							for(int positionLevel = 0; positionLevel < pos.dimensionExpressionLevels.length; positionLevel++)
							{
								pos.dimensionExpressionLevels[positionLevel] = posChild.getChild(positionLevel + 1).getText();
							}
						}
						mec.positions.add(pos);
					}
					List<MeasureExpressionColumn> meclist = null;
					if (script.isCompilingParent())
						meclist = script.getParentDisplayExpression().getMeasureExpressionColumns();
					else
						meclist = displayExpression.getMeasureExpressionColumns();
					LogicalQueryDataElement lqde = new LogicalQueryDataElement();
					lqde.index = meclist.size();
					meclist.add(mec);
					mec.displayExpression = displayExpression;
					lqde.displayExpression = displayExpression;
					lqde.fieldType = DisplayExpression.MEASURE_TYPE;
					if (type == BirstScriptParser.RANK || type == BirstScriptParser.DRANK)
						lqde.type = new DataType(DType.Integer, 0);
					else if (type == BirstScriptParser.PTILE)
						lqde.type = new DataType(DType.Float, 0);
					else
					{
						if (mc.DataType.equals("Integer"))
							lqde.type = new DataType(DType.Integer, 0);
						else if (mc.DataType.equals("Number"))
							lqde.type = new DataType(DType.Float, 0);
						else if (mc.DataType.equals("Float"))
							lqde.type = new DataType(DType.Float, 0);
						else
						{
							if (mc.AggregationRule.equals("COUNT") || mc.AggregationRule.equals("COUNT DISTINCT"))
								lqde.type = new DataType(DType.Integer, 0);
							else if (mc.DataType.equals("DateTime"))
								lqde.type = new DataType(DType.DateTime, 0);
							else if (mc.DataType.equals("Date"))
								lqde.type = new DataType(DType.Date, 0);
							else if (mc.DataType.equals("Varchar"))
								lqde.type = new DataType(DType.Varchar, 0);
						}
					}
					de = lqde;
				} else if (type == BirstScriptParser.COLUMN_NAME || type == BirstScriptParser.SPARSE)
				{
					boolean sparse = false;
					if (type == BirstScriptParser.SPARSE)
					{
						if (tree.getChild(0).getType() != BirstScriptParser.COLUMN_NAME)
							throw new ScriptException("Invalid column name: " + tree.getText(), tree.getLine(), tree.getCharPositionInLine(),
									tree.getCharPositionInLine() + tree.getText().length());
						name = tree.getChild(0).getText();
						name = name.substring(1, name.length() - 1);
						sparse = true;
					}
					DimensionExpressionColumn dec = new DimensionExpressionColumn();
					int pos = name.indexOf('.');
					dec.dimName = name.substring(0, pos);
					dec.colName = name.substring(pos + 1, name.length());
					dec.sparse = sparse;
					DimensionColumn dc = script.getRepository().findDimensionColumn(dec.dimName, dec.colName);
					if (dc == null)
						throw new ScriptException("Unknown column name: " + tree.getText(), tree.getLine(), tree.getCharPositionInLine(),
								tree.getCharPositionInLine() + tree.getText().length());
					LogicalQueryDataElement lqde = new LogicalQueryDataElement();
					if (displayExpression != null)
					{
						List<DimensionExpressionColumn> declist = null;
						if (script.isCompilingParent())
							declist = script.getParentDisplayExpression().getDimensionExpressionColumns();
						else
							declist = displayExpression.getDimensionExpressionColumns();
						lqde.displayExpression = displayExpression;
						lqde.index = declist.size();
						declist.add(dec);
					}
					lqde.fieldType = DisplayExpression.DIMENSION_TYPE;
					if (type == BirstScriptParser.RANK || type == BirstScriptParser.DRANK)
						lqde.type = new DataType(DType.Integer, 0);
					else
					{
						if (dc.DataType.equals("Integer"))
							lqde.type = new DataType(DType.Integer, 0);
						else if (dc.DataType.equals("Number"))
							lqde.type = new DataType(DType.Float, 0);
						else if (dc.DataType.equals("Float"))
							lqde.type = new DataType(DType.Float, 0);
						else if (dc.DataType.equals("DateTime"))
							lqde.type = new DataType(DType.DateTime, 0);
						else if (dc.DataType.equals("Date"))
							lqde.type = new DataType(DType.Date, 0);
						else if (dc.DataType.equals("Varchar"))
							lqde.type = new DataType(DType.Varchar, 0);
					}
					de = lqde;
				} else
					throw new ScriptException("Unknown data element: " + tree.getText(), tree.getLine(), tree.getCharPositionInLine(),
							tree.getCharPositionInLine() + tree.getText().length());
			} else if (tree.getType() == BirstScriptParser.COLUMN_NAME || tree.getType() == BirstScriptParser.IDENT_NAME
					|| tree.getType() == BirstScriptParser.GRAIN_COLUMN || tree.getType() == BirstScriptParser.LEVEL_COLUMN)
			{
				de = script.getNonOutputElement(tree.getText());
				if (de == null)
					throw new ScriptException("Unknown data element: " + tree.getText(), tree.getLine(), tree.getCharPositionInLine(),
							tree.getCharPositionInLine() + tree.getText().length());
			} else
			{
				// Aggregation
				cl = tree.getChild(0);
				de = script.getNonOutputElement(tree.getText().toUpperCase() + "(" + cl.getText() + ")");
				if (de == null)
					throw new ScriptException("Unknown data element: " + tree.getText() + "(" + cl.getText() + ")", tree.getLine(),
							tree.getCharPositionInLine(), tree.getCharPositionInLine() + tree.getText().length());
			}
			if (de.getType().type == DType.Varchar)
				return new OpVarchar(de, null);
			else if (de.getType().type == DType.Integer)
				return new OpInt(de, null);
			else if (de.getType().type == DType.Float)
				return new OpFloat(de, null);
			else if (de.getType().type == DType.DateTime || de.getType().type == DType.Date)
				return new OpDateTime(de, null);
			else if (de.getType().type == DType.List)
				throw new ScriptException("Invalid expression, cannot reference an array without an index", tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			break;
		case BirstScriptParser.CURRENTVALUE:
			if (!script.isInDimensionExpression())
				throw new ScriptException("Invalid expression, cannot utilize CURRENTVALUE outside of a dimension expression", tree.getLine(),
						tree.getCharPositionInLine(), tree.getCharPositionInLine() + tree.getText().length());
			script.setCompilingParent(true);
			compiledAssignment = compile(tree.getChild(0));
			script.setCompilingParent(false);
			/*
			 * The fields for this are driven off of the parent query
			 */
			if (compiledAssignment instanceof OpVarchar)
			{
				LogicalQueryDataElement lqde = (LogicalQueryDataElement) ((OpVarchar) compiledAssignment).getDataElement();
				lqde.setParentType(true);
			} else if (compiledAssignment instanceof OpInt)
			{
				LogicalQueryDataElement lqde = (LogicalQueryDataElement) ((OpInt) compiledAssignment).getDataElement();
				lqde.setParentType(true);
			} else if (compiledAssignment instanceof OpFloat)
			{
				LogicalQueryDataElement lqde = (LogicalQueryDataElement) ((OpFloat) compiledAssignment).getDataElement();
				lqde.setParentType(true);
			} else if (compiledAssignment instanceof OpDateTime)
			{
				LogicalQueryDataElement lqde = (LogicalQueryDataElement) ((OpDateTime) compiledAssignment).getDataElement();
				lqde.setParentType(true);
			}
			break;
		case BirstScriptParser.PTILE:
		case BirstScriptParser.RANK:
		case BirstScriptParser.DRANK:
		case BirstScriptParser.STATMEDIAN:
		case BirstScriptParser.RSUM:
			if (!logicalQuery)
				throw new ScriptException("RANK/PTILE/DRANK/STATMEDIAN/RSUM can not be used in ETL scripts", tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			compiledAssignment = new Aggregate(script, tree);
			isAggregate = true;
			if (type == BirstScriptParser.RSUM)
				isRSum = true;
			break;
		case BirstScriptParser.FUNCTIONCALL:
			if (logicalQuery)
				throw new ScriptException(tree.getText() + " can not be used in ETL scripts", tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			// See if this is a function call or an array lookup
			de = script.getNonOutputElement(tree.getChild(0).getText());
			if (de == null)
			{
				Function f = script.getFunction(tree.getChild(0).getText());
				if (f != null)
				{
					compiledAssignment = new FunctionCall(script, tree);
					break;
				}
			}
			if (de == null)
				throw new ScriptException("Unknown data element: " + tree.getText(), tree.getLine(), tree.getCharPositionInLine(), tree.getCharPositionInLine()
						+ tree.getText().length());
			DataType targetType = de.getType();
			DType ttype = targetType.type;
			if (ttype != DType.List && ttype != DType.Map)
				throw new ScriptException("Attempt to use an array index on a non-array and non-map type", tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			cr = tree.getChild(1);
			DType ltype = getType(script, cr).type;
			if (ttype == DType.List && ltype != DType.Integer)
				throw new ScriptException("Array index must be an integer", tree.getLine(), tree.getCharPositionInLine(), tree.getCharPositionInLine()
						+ tree.getText().length());
			else if (ttype == DType.Map && ltype != targetType.lookuptype)
				throw new ScriptException("Lookup value must match declared data type of map", tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			if (de.getType().subtype == DType.Varchar)
				return new OpVarchar(de, compile(cr));
			else if (de.getType().subtype == DType.Integer)
				return new OpInt(de, compile(cr));
			else if (de.getType().subtype == DType.Float)
				return new OpFloat(de, compile(cr));
			else if (de.getType().subtype == DType.DateTime || de.getType().subtype == DType.Date)
				return new OpDateTime(de, compile(cr));
			break;
		case BirstScriptParser.SUBSTRING:
			compiledAssignment = new Substring(script, tree);
			break;
		case BirstScriptParser.LENGTH:
			compiledAssignment = new Length(script, tree);
			break;
		case BirstScriptParser.POSITION:
			compiledAssignment = new Position(script, tree);
			break;
		case BirstScriptParser.TOSTRING:
			compiledAssignment = new ToString(script, tree);
			break;
		case BirstScriptParser.TOUPPER:
		case BirstScriptParser.TOLOWER:
		case BirstScriptParser.TRIM:
			compiledAssignment = new StringCaseFunction(script, tree);
			break;
		case BirstScriptParser.REPLACE:
		case BirstScriptParser.REPLACEALL:
			compiledAssignment = new Replace(script, tree);
			break;
		case BirstScriptParser.FORMAT:
			compiledAssignment = new Format(script, tree);
			break;
		case BirstScriptParser.URLENCODE:
			compiledAssignment = new UrlEncode(script, tree);
			break;
		case BirstScriptParser.TOTIME:
			compiledAssignment = new ToTime(script, tree);
			break;
		case BirstScriptParser.DATEPART:
			compiledAssignment = new DatePart(script, tree);
			break;
		case BirstScriptParser.DATEADD:
			compiledAssignment = new DateAdd(script, tree);
			break;
		case BirstScriptParser.DATEDIFF:
			compiledAssignment = new DateDiff(script, tree);
			break;
		case BirstScriptParser.IIF:
			compiledAssignment = new IIF(script, tree);
			break;
		case BirstScriptParser.IFNULL:
			compiledAssignment = new Ifnull(script, tree);
			break;
		case BirstScriptParser.ROWNUMBER:
			if (!logicalQuery)
				throw new ScriptException("ROWNUMBER can not be used in ETL scripts", tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			compiledAssignment = new RowNumber(script, tree);
			break;
		case BirstScriptParser.NUMROWS:
			if (!logicalQuery)
				throw new ScriptException("NUMROWS can not be used in ETL scripts", tree.getLine(), tree.getCharPositionInLine(), tree.getCharPositionInLine()
						+ tree.getText().length());
			compiledAssignment = new NumRows(script, tree);
			break;
		case BirstScriptParser.SIN:
		case BirstScriptParser.COS:
		case BirstScriptParser.TAN:
		case BirstScriptParser.ARCSIN:
		case BirstScriptParser.ARCCOS:
		case BirstScriptParser.ARCTAN:
		case BirstScriptParser.ABS:
		case BirstScriptParser.CEILING:
		case BirstScriptParser.FLOOR:
		case BirstScriptParser.SQRT:
		case BirstScriptParser.LOG:
		case BirstScriptParser.LN:
		case BirstScriptParser.EXP:
		case BirstScriptParser.POWER:
		case BirstScriptParser.MATHMAX:
		case BirstScriptParser.MATHMIN:
			compiledAssignment = new Math(script, tree, type);
			break;
		case BirstScriptParser.RANDOM:
			compiledAssignment = new Random(script, tree);
			break;
		case BirstScriptParser.CAST:
			cl = tree.getChild(1);
			dl = getType(script, cl, logicalQuery);
			if (tree.getChild(0).getType() == BirstScriptParser.INTEGERTYPE)
			{
				if (dl.type == DType.Integer || dl.type == DType.Float || dl.type == DType.Varchar)
					compiledAssignment = new OpUnaryInt(compile(tree.getChild(1)), BirstScriptParser.INTEGERTYPE);
				else
					throw new ScriptException("Cannot cast to integer", tree.getLine(), tree.getCharPositionInLine(), tree.getCharPositionInLine()
							+ tree.getText().length());
			} else if (tree.getChild(0).getType() == BirstScriptParser.FLOATTYPE)
			{
				if (dl.type == DType.Integer || dl.type == DType.Float || dl.type == DType.Varchar)
					compiledAssignment = new OpUnaryFloat(compile(tree.getChild(1)), BirstScriptParser.FLOATTYPE);
				else
					throw new ScriptException("Cannot cast to float", tree.getLine(), tree.getCharPositionInLine(), tree.getCharPositionInLine()
							+ tree.getText().length());
			} else if (tree.getChild(0).getType() == BirstScriptParser.DATETIMETYPE)
			{
				if (dl.type == DType.Varchar || dl.type == DType.Date || dl.type == DType.DateTime || dl.type == DType.Float)
					compiledAssignment = new OpUnaryDateTime(compile(tree.getChild(1)), BirstScriptParser.DATETIMETYPE);
				else
					throw new ScriptException("Cannot cast to datetime", tree.getLine(), tree.getCharPositionInLine(), tree.getCharPositionInLine()
							+ tree.getText().length());
			} else if (tree.getChild(0).getType() == BirstScriptParser.VARCHARTYPE)
			{
				compiledAssignment = new OpUnaryVarchar(compile(tree.getChild(1)), BirstScriptParser.VARCHARTYPE, script);
			}
			break;
		case BirstScriptParser.GETVARIABLE:
			if (tree.getChildCount() == 1)
				compiledAssignment = new OpBinaryVarchar(compile(tree.getChild(0)), null, BirstScriptParser.GETVARIABLE, script);
			else
			{
				dr = getType(script, tree.getChild(1));
				if (dr.type != DType.Varchar)
					throw new ScriptException("Username must be a varchar", tree.getLine(), tree.getCharPositionInLine(), tree.getCharPositionInLine()
							+ tree.getText().length());
				compiledAssignment = new OpBinaryVarchar(compile(tree.getChild(0)), compile(tree.getChild(1)), BirstScriptParser.GETVARIABLE, script);
			}
			script.setContainsVariable(true);
			break;
		case BirstScriptParser.SAVEDEXPRESSION:
			compiledAssignment = new SavedExpression(script, tree);
			break;
		case BirstScriptParser.GETPROMPTVALUE:
			if (tree.getChildCount() == 1)
				compiledAssignment = new OpBinaryVarchar(compile(tree.getChild(0)), null, BirstScriptParser.GETPROMPTVALUE, script);
			else
			{
				dr = getType(script, tree.getChild(1));
				if (dr.type != DType.Varchar && dr.type != DType.Any)
					throw new ScriptException("GetPromptValue default value must be a varchar or null", tree.getLine(), tree.getCharPositionInLine(),
							tree.getCharPositionInLine() + tree.getText().length());
				compiledAssignment = new OpBinaryVarchar(compile(tree.getChild(0)), compile(tree.getChild(1)), BirstScriptParser.GETPROMPTVALUE, script);
			}
			break;
		case BirstScriptParser.GETCOLUMNVALUE:
			if (logicalQuery)
				throw new ScriptException("GETCOLUMNVALUE can only be used in ETL scripts", tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			compiledAssignment = new GetColumnValue(script, tree);
			break;
		case BirstScriptParser.NEXTCHILD:
			if (logicalQuery)
				throw new ScriptException("NEXTCHILD can only be used in ETL scripts", tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			compiledAssignment = new NextChild(script, tree);
			break;
		case BirstScriptParser.GETLEVELVALUE:
			if (logicalQuery)
				throw new ScriptException("GETLEVELVALUE can only be used in ETL scripts", tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			compiledAssignment = new GetLevelValue(script, tree);
			break;
		case BirstScriptParser.GETLEVELATTRIBUTE:
			if (logicalQuery)
				throw new ScriptException("GETLEVELATTRIBUTE can only be used in ETL scripts", tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			compiledAssignment = new GetLevelAttribute(script, tree);
			break;
		case BirstScriptParser.GETDAYID:
			dl = getType(script, tree.getChild(0));
			if (dl.type != DType.Date && dl.type != DType.DateTime)
				throw new ScriptException("Date type required for ID: line " + tree.getLine() + ", column " + tree.getCharPositionInLine(), tree.getLine(),
						tree.getCharPositionInLine(), tree.getCharPositionInLine() + tree.getText().length());
			compiledAssignment = new OpUnaryInt(compile(tree.getChild(0)), BirstScriptParser.GETDAYID, script.getRepository());
			break;
		case BirstScriptParser.GETWEEKID:
			dl = getType(script, tree.getChild(0));
			if (dl.type != DType.Date && dl.type != DType.DateTime)
				throw new ScriptException("Date type required for ID: line " + tree.getLine() + ", column " + tree.getCharPositionInLine(), tree.getLine(),
						tree.getCharPositionInLine(), tree.getCharPositionInLine() + tree.getText().length());
			compiledAssignment = new OpUnaryInt(compile(tree.getChild(0)), BirstScriptParser.GETWEEKID, script.getRepository());
			break;
		case BirstScriptParser.GETMONTHID:
			dl = getType(script, tree.getChild(0));
			if (dl.type != DType.Date && dl.type != DType.DateTime)
				throw new ScriptException("Date type required for ID: line " + tree.getLine() + ", column " + tree.getCharPositionInLine(), tree.getLine(),
						tree.getCharPositionInLine(), tree.getCharPositionInLine() + tree.getText().length());
			compiledAssignment = new OpUnaryInt(compile(tree.getChild(0)), BirstScriptParser.GETMONTHID, script.getRepository());
			break;
		case BirstScriptParser.SETLOOKUP:
			if (!logicalQuery)
				throw new ScriptException(tree.getText() + " can not be used in ETL scripts", tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			compiledAssignment = new SetLookup(script, tree, true, false, false, false);
			break;
		case BirstScriptParser.SETLOOKUPROW:
			if (!logicalQuery)
				throw new ScriptException(tree.getText() + " can not be used in ETL scripts", tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			compiledAssignment = new SetLookup(script, tree, true, true, false, false);
			break;
		case BirstScriptParser.SETFIND:
			if (!logicalQuery)
				throw new ScriptException(tree.getText() + " can not be used in ETL scripts", tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			compiledAssignment = new SetLookup(script, tree, false, false, false, false);
			break;
		case BirstScriptParser.SETSTAT:
			if (!logicalQuery)
				throw new ScriptException(tree.getText() + " can not be used in ETL scripts", tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			compiledAssignment = new SetLookup(script, tree, false, false, true, false);
			break;
		case BirstScriptParser.FUNCTION:
			if (!logicalQuery)
				throw new ScriptException("FUNCTION can not be used in ETL scripts", tree.getLine(), tree.getCharPositionInLine(), tree.getCharPositionInLine()
						+ tree.getText().length());
			compiledAssignment = new SetLookup(script, tree, false, false, false, true);
			break;
		case BirstScriptParser.FUNCTIONLOOKUP:
			if (!logicalQuery)
				throw new ScriptException("FUNCTIONLOOKUP can not be used in ETL scripts", tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			compiledAssignment = new SetLookup(script, tree, true, false, false, true);
			isAggregate = true;
			break;
		case BirstScriptParser.TRANSFORM:
			if (!logicalQuery)
				throw new ScriptException("TRANSFORM can not be used in ETL scripts", tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			Transform t = new Transform(script, tree);
			compiledAssignment = t;
			script.getDisplayExpression().setTransform(t);
			break;
		case BirstScriptParser.LET:
			if (!logicalQuery)
				throw new ScriptException("LET can not be used in ETL scripts", tree.getLine(), tree.getCharPositionInLine(), tree.getCharPositionInLine()
						+ tree.getText().length());
			Let let = new Let(script, tree);
			compiledAssignment = let;
			break;
		case BirstScriptParser.PARAMETER:
			throw new ScriptException("Parameters may only be used in lookups.", tree.getLine(), tree.getCharPositionInLine(), tree.getCharPositionInLine()
					+ tree.getText().length());
		case BirstScriptParser.GETPROMPTFILTER:
			throw new ScriptException("GetPromptFilter may only be used in filter expressions.", tree.getLine(), tree.getCharPositionInLine(),
					tree.getCharPositionInLine() + tree.getText().length());
		case BirstScriptParser.GROUPS:
			if (!logicalQuery)
				throw new ScriptException("GROUPS can only be used in ETL scripts", tree.getLine(), tree.getCharPositionInLine(), tree.getCharPositionInLine()
						+ tree.getText().length());
			compiledAssignment = new Groups(script, tree);
			break;
		case BirstScriptParser.TREND:
			if (!logicalQuery)
				throw new ScriptException("TREND can not be used in ETL scripts", tree.getLine(), tree.getCharPositionInLine(), tree.getCharPositionInLine()
						+ tree.getText().length());
			compiledAssignment = new Trend(script, tree);
			break;
		case BirstScriptParser.REXP:
			compiledAssignment = new RExpression(script, tree);
			break;
		default:
			break;
		}
		return compiledAssignment;
	}

	private static boolean isPositionalCalc(Tree tree)
	{
		int childCount = tree.getChildCount();
		if (childCount == 1 && tree.getChild(0).getType() == BirstScriptParser.POSITION_COLUMN_START)
			return true;
		if (childCount == 3)
		{
			if (tree.getChild(1).getText().equals("=") || tree.getChild(1).getText().equals("+=") || tree.getChild(1).getText().equals("-="))
				return true;
		}
		return false;
	}

	public static DataType getType(TransformationScript script, Tree tree) throws ScriptException
	{
		return getType(script, tree, false);
	}

	public static DataType getLogicalQueryType(TransformationScript script, Tree tree) throws ScriptException
	{
		return getType(script, tree, true);
	}

	public static DataType getType(TransformationScript script, Tree tree, boolean logicalQuery) throws ScriptException
	{
		switch (tree.getType())
		{
		case BirstScriptParser.PAREN:
			return (getType(script, tree.getChild(0), logicalQuery));
		case BirstScriptParser.INTEGER:
			return (new DataType(DType.Integer, 0));
		case BirstScriptParser.FLOAT:
			return (new DataType(DType.Float, 0));
		case BirstScriptParser.STRING:
			return (new DataType(DType.Varchar, tree.getText().length() - 2));
		case BirstScriptParser.DATETIME:
			return (new DataType(DType.DateTime, 0));
		case BirstScriptParser.NULL:
			return (new DataType(DType.Any, 0));
		case BirstScriptParser.BOOLEAN:
		case BirstScriptParser.EQUALS:
		case BirstScriptParser.LT:
		case BirstScriptParser.GT:
		case BirstScriptParser.LTEQ:
		case BirstScriptParser.GTEQ:
		case BirstScriptParser.NOTEQUALS:
		case BirstScriptParser.AND:
		case BirstScriptParser.OR:
		case BirstScriptParser.IN:
		case BirstScriptParser.NOTIN:
		case BirstScriptParser.LIKE:
		case BirstScriptParser.NOTLIKE:
		case BirstScriptParser.ISNULL:
		case BirstScriptParser.ISNOTNULL:
			return (new DataType("Boolean", 5));
		case BirstScriptParser.PLUS:
			Tree cl = tree.getChild(0);
			Tree cr = tree.getChild(1);
			DataType dtl = getType(script, cl, logicalQuery);
			DataType dtr = getType(script, cr, logicalQuery);
			if (dtl == null || dtr == null)
				throw new ScriptException("Invalid data type: line " + tree.getLine(), tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			if (dtl.type == DType.DateTime || dtr.type == DType.DateTime || dtl.type == DType.Date || dtr.type == DType.Date)
				throw new ScriptException("Invalid data type Date/DateTime: line " + tree.getLine(), tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			if (dtl.type == DType.Boolean || dtr.type == DType.Boolean)
				throw new ScriptException("Invalid data type Boolean: line " + tree.getLine(), tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			return (typeMath(dtl, dtr));
		case BirstScriptParser.MINUS:
			cl = tree.getChild(0);
			cr = tree.getChild(1);
			dtl = getType(script, cl, logicalQuery);
			dtr = getType(script, cr, logicalQuery);
			if (dtl == null || dtr == null)
				throw new ScriptException("Invalid data type: line " + tree.getLine(), tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			if (dtl.type == DType.Varchar || dtr.type == DType.Varchar)
				throw new ScriptException("Invalid data type Varchar: line " + tree.getLine(), tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			if (dtl.type == DType.DateTime || dtr.type == DType.DateTime || dtl.type == DType.Date || dtr.type == DType.Date)
				throw new ScriptException("Invalid data type Date/DateTime: line " + tree.getLine(), tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			if (dtl.type == DType.Boolean || dtr.type == DType.Boolean)
				throw new ScriptException("Invalid data type Boolean: line " + tree.getLine(), tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			return (typeMath(dtl, dtr));
		case BirstScriptParser.BITAND:
		case BirstScriptParser.BITOR:
			cl = tree.getChild(0);
			cr = tree.getChild(1);
			dtl = getType(script, cl, logicalQuery);
			dtr = getType(script, cr, logicalQuery);
			if (dtl == null || dtr == null)
				throw new ScriptException("Invalid data type for bit operations, must be Integer: line " + tree.getLine(), tree.getLine(),
						tree.getCharPositionInLine(), tree.getCharPositionInLine() + tree.getText().length());
			if (dtl.type != DType.Integer || dtr.type != DType.Integer)
				throw new ScriptException("Invalid data type for bit operations, must be Integer: line " + tree.getLine(), tree.getLine(),
						tree.getCharPositionInLine(), tree.getCharPositionInLine() + tree.getText().length());
			return (new DataType("Integer", 20));
		case BirstScriptParser.NEGATE:
			cl = tree.getChild(0);
			dtl = getType(script, cl, logicalQuery);
			if (dtl.type == DType.Varchar)
				throw new ScriptException("Invalid data type Varchar: line " + tree.getLine(), tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			if (dtl.type == DType.DateTime || dtl.type == DType.Date)
				throw new ScriptException("Invalid data type Date/DateTime: line " + tree.getLine(), tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			if (dtl.type == DType.Boolean)
				throw new ScriptException("Invalid data type Boolean: line " + tree.getLine(), tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			dtl = getType(script, cl, logicalQuery);
			return (dtl);
		case BirstScriptParser.DIV:
			cl = tree.getChild(0);
			cr = tree.getChild(1);
			dtl = getType(script, cl, logicalQuery);
			dtr = getType(script, cr, logicalQuery);
			if (dtl == null || dtr == null)
				throw new ScriptException("Invalid data type: line " + tree.getLine(), tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			if (dtl.type == DType.Varchar || dtr.type == DType.Varchar)
				throw new ScriptException("Invalid data type Varchar: line " + tree.getLine(), tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			if (dtl.type == DType.DateTime || dtr.type == DType.DateTime || dtl.type == DType.Date || dtr.type == DType.Date)
				throw new ScriptException("Invalid data type Date/DateTime: line " + tree.getLine(), tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			if (dtl.type == DType.Boolean || dtr.type == DType.Boolean)
				throw new ScriptException("Invalid data type Boolean: line " + tree.getLine(), tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			return (typeMath(dtl, dtr));
		case BirstScriptParser.MULT:
			cl = tree.getChild(0);
			cr = tree.getChild(1);
			dtl = getType(script, cl, logicalQuery);
			dtr = getType(script, cr, logicalQuery);
			if (dtl == null || dtr == null)
				throw new ScriptException("Invalid data type: line " + tree.getLine(), tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			if (dtl.type == DType.Varchar || dtr.type == DType.Varchar)
				throw new ScriptException("Invalid data type Varchar: line " + tree.getLine(), tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			if (dtl.type == DType.DateTime || dtr.type == DType.DateTime || dtl.type == DType.Date || dtr.type == DType.Date)
				throw new ScriptException("Invalid data type Date/DateTime: line " + tree.getLine(), tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			if (dtl.type == DType.Boolean || dtr.type == DType.Boolean)
				throw new ScriptException("Invalid data type Boolean: line " + tree.getLine(), tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			return (typeMath(dtl, dtr));
		case BirstScriptParser.MOD:
			cl = tree.getChild(0);
			cr = tree.getChild(1);
			dtl = getType(script, cl, logicalQuery);
			dtr = getType(script, cr, logicalQuery);
			if (dtl == null || dtr == null)
				throw new ScriptException("Invalid data type: line " + tree.getLine(), tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			if (dtl.type == DType.Varchar || dtr.type == DType.Varchar)
				throw new ScriptException("Invalid data type Varchar: line " + tree.getLine(), tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			if (dtl.type == DType.DateTime || dtr.type == DType.DateTime || dtl.type == DType.Date || dtr.type == DType.Date)
				throw new ScriptException("Invalid data type Date/DateTime: line " + tree.getLine(), tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			if (dtl.type == DType.Boolean || dtr.type == DType.Boolean)
				throw new ScriptException("Invalid data type Boolean: line " + tree.getLine(), tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			return (typeMath(dtl, dtr));
		case BirstScriptParser.AVG:
			return (new DataType(DType.Float, 0));
		case BirstScriptParser.COUNT:
			return (new DataType(DType.Integer, 0));
		case BirstScriptParser.COUNTDISTINCT:
			return (new DataType(DType.Integer, 0));
		case BirstScriptParser.SUM:
		case BirstScriptParser.MIN:
		case BirstScriptParser.MAX:
		case BirstScriptParser.COLUMN_NAME:
		case BirstScriptParser.IDENT_NAME:
		case BirstScriptParser.GRAIN_COLUMN:
		case BirstScriptParser.LEVEL_COLUMN:
			DataElement de = null;
			if (logicalQuery)
			{
				if (tree.getType() == BirstScriptParser.COLUMN_NAME || tree.getType() == BirstScriptParser.IDENT_NAME)
				{
					if (tree.getType() == BirstScriptParser.IDENT_NAME)
					{
						de = script.getElement(tree.getText());
						if (de != null)
						{
							return de.getType();
						}
						DataType dt = getIdentDataType(tree.getText(), script);
						if (dt != null)
							return dt;
					}
					else if(tree.getType() == BirstScriptParser.COLUMN_NAME)
					{
						String cname = tree.getText();
						if(cname != null)
						{
							cname = cname.replace("[", "").replace("]", "");
						}
						int index = cname.indexOf('.');
						if(index > 0)
						{
							String dimName = cname.substring(0,index);
							String colName = cname.substring(index+1);
							if((dimName != null) && (colName != null))
							{
								DimensionColumn dc = script.getRepository().findDimensionColumn(dimName, colName);
								if((dc != null) && (dc.DataType !=null))
								{
									if (dc.DataType.equals("Integer"))
									{
										return new DataType(DType.Integer, 0);
									}
									else if (dc.DataType.equals("Number") || dc.DataType.equals("Float"))
									{
										return new DataType(DType.Float, 0);
									}
									else if (dc.DataType.equals("DateTime"))
									{
										return new DataType(DType.DateTime, 0);
									}
									else if (dc.DataType.equals("Date"))
									{
										return new DataType(DType.Date, 0);
									}
									else if (dc.DataType.equals("Varchar"))
									{
										return new DataType(DType.Varchar, 0);
									}
								}
							}
						}
					}
					return new DataType(DType.Float, 0);
				} else
					throw new ScriptException("Unknown data element: " + tree.getText(), tree.getLine(), tree.getCharPositionInLine(),
							tree.getCharPositionInLine() + tree.getText().length());
			}
			if (tree.getType() == BirstScriptParser.COLUMN_NAME || tree.getType() == BirstScriptParser.IDENT_NAME
					|| tree.getType() == BirstScriptParser.GRAIN_COLUMN || tree.getType() == BirstScriptParser.LEVEL_COLUMN)
			{
				de = script.getElement(tree.getText());
				if (de == null)
				{
					// find in repository
					if (tree.getType() == BirstScriptParser.IDENT_NAME)
					{
						String name = tree.getText();
						DataType dt = getIdentDataType(name, script);
						if (dt != null)
							return dt;
					}
				}
				if (de == null)
					throw new ScriptException("Unknown data element: " + tree.getText(), tree.getLine(), tree.getCharPositionInLine(),
							tree.getCharPositionInLine() + tree.getText().length());
			} else
			{
				cl = tree.getChild(0);
				de = script.getElement(tree.getText().toUpperCase() + "(" + cl.getText() + ")");
				if (de == null)
					throw new ScriptException("Unknown data element: " + tree.getText() + "(" + cl.getText() + ")", tree.getLine(),
							tree.getCharPositionInLine(), tree.getCharPositionInLine() + tree.getText().length());
			}
			return de.getType();
		case BirstScriptParser.GETCOLUMNVALUE:
			int pos = Integer.valueOf(tree.getChild(0).getText());
			InputDataElement ide = null;
			if (pos >= 0 && pos < script.inputElements.size())
				ide = script.inputElements.get(pos);
			if (ide == null)
				throw new ScriptException("Unknown data element: " + tree.getText() + "(" + tree.getChild(0).getText() + ")", tree.getLine(),
						tree.getCharPositionInLine(), tree.getCharPositionInLine() + tree.getText().length());
			return ide.getType();
		case BirstScriptParser.CURRENTVALUE:
			return getType(script, tree.getChild(0), logicalQuery);
		case BirstScriptParser.SUBSTRING:
		case BirstScriptParser.TOUPPER:
		case BirstScriptParser.TOLOWER:
		case BirstScriptParser.TRIM:
		case BirstScriptParser.REPLACE:
		case BirstScriptParser.REPLACEALL:
		case BirstScriptParser.FORMAT:
		case BirstScriptParser.URLENCODE:
		case BirstScriptParser.TOSTRING:
		case BirstScriptParser.TOTIME:
		case BirstScriptParser.MDXEXPRESSION:
			return new DataType(DType.Varchar, 0);
		case BirstScriptParser.LENGTH:
		case BirstScriptParser.POSITION:
		case BirstScriptParser.DATEPART:
		case BirstScriptParser.DATEDIFF:
		case BirstScriptParser.NUMROWS:
		case BirstScriptParser.ROWNUMBER:
			return new DataType(DType.Integer, 0);
		case BirstScriptParser.DATEADD:
			return new DataType(DType.DateTime, 0);
		case BirstScriptParser.IIF:
			cl = tree.getChild(1);
			cr = tree.getChild(2);
			dtl = getType(script, cl, logicalQuery);
			dtr = getType(script, cr, logicalQuery);
			if (dtl == null || dtr == null)
				throw new ScriptException("Invalid data type: line " + tree.getLine(), tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			if (dtl.type == dtr.type)
				return dtl;
			if (dtl.type == DType.Any)
				return dtr;
			if (dtr.type == DType.Any)
				return dtl;
			if (dtl.type == DType.Integer && dtr.type == DType.Float)
				return dtr;
			if (dtl.type == DType.Float && dtr.type == DType.Integer)
				return dtl;
			if (dtl.type != dtr.type)
				throw new ScriptException("Both results of IIF must be of the same type: line " + tree.getLine(), tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			return dtl;
		case BirstScriptParser.IFNULL:
			cl = tree.getChild(0);
			cr = tree.getChild(1);
			dtl = getType(script, cl, logicalQuery);
			dtr = getType(script, cr, logicalQuery);
			if (dtl == null || dtr == null)
				throw new ScriptException("Invalid data type: line " + tree.getLine(), tree.getLine(), tree.getCharPositionInLine(),
						tree.getCharPositionInLine() + tree.getText().length());
			if (dtl.type == dtr.type)
				return dtl;
			if (dtl.type == DType.Any)
				return dtr;
			if (dtr.type == DType.Any)
				return dtl;
			if (dtl.type == DType.Integer && dtr.type == DType.Float)
				return dtr;
			if (dtl.type == DType.Float && dtr.type == DType.Integer)
				return dtl;
			if (dtl.type != dtr.type)
				throw new ScriptException("Both results of IFNULL must be of the same type: line " + tree.getLine(), tree.getLine(),
						tree.getCharPositionInLine(), tree.getCharPositionInLine() + tree.getText().length());
			return dtl;
		case BirstScriptParser.SAVEDEXPRESSION:
			return DataType.getType(new SavedExpression(script, tree).getType());
		case BirstScriptParser.RANK:
			return new DataType(DType.Integer, 0);
		case BirstScriptParser.DRANK:
			return new DataType(DType.Integer, 0);
		case BirstScriptParser.PTILE:
			return new DataType(DType.Float, 0);
		case BirstScriptParser.CAST:
			if (tree.getChild(0).getType() == BirstScriptParser.INTEGERTYPE)
				return new DataType(DType.Integer, 0);
			else if (tree.getChild(0).getType() == BirstScriptParser.FLOATTYPE)
				return new DataType(DType.Float, 0);
			else if (tree.getChild(0).getType() == BirstScriptParser.DATETIMETYPE)
				return new DataType(DType.DateTime, 0);
			else if (tree.getChild(0).getType() == BirstScriptParser.VARCHARTYPE)
				return new DataType(DType.Varchar, 0);
			break;
		case BirstScriptParser.GETPROMPTVALUE:
		case BirstScriptParser.GETVARIABLE:
		case BirstScriptParser.GETLEVELVALUE:
			return new DataType(DType.Varchar, 0);
		case BirstScriptParser.NEXTCHILD:
			return new DataType(DType.Boolean, 0);
		case BirstScriptParser.GETLEVELATTRIBUTE:
			return new DataType(DType.Integer, 0);
		case BirstScriptParser.GETDAYID:
		case BirstScriptParser.GETWEEKID:
		case BirstScriptParser.GETMONTHID:
			return new DataType(DType.Integer, 0);
		case BirstScriptParser.PARAMETER:
			throw new ScriptException("Parameters may only be used in lookups.", tree.getLine(), tree.getCharPositionInLine(), tree.getCharPositionInLine()
					+ tree.getText().length());
		case BirstScriptParser.GETPROMPTFILTER:
			throw new ScriptException("GetPromptFilter may only be used in filter expressions.", tree.getLine(), tree.getCharPositionInLine(),
					tree.getCharPositionInLine() + tree.getText().length());
		case BirstScriptParser.STATMEDIAN:
		case BirstScriptParser.RSUM:
			return Aggregate.getType(script, tree);
		case BirstScriptParser.SETLOOKUP:
		case BirstScriptParser.SETLOOKUPROW:
		case BirstScriptParser.SETFIND:
		case BirstScriptParser.SETSTAT:
		case BirstScriptParser.FUNCTION:
		case BirstScriptParser.FUNCTIONLOOKUP:
			return getSetDataType(script, tree);
		case BirstScriptParser.LET:
			return getType(script, tree.getChild(1), logicalQuery);
		case BirstScriptParser.TRANSFORM:
			return new DataType(DType.Integer, 0);
		case BirstScriptParser.GROUPS:
			cl = tree.getChild(0);
			return getType(script, cl, logicalQuery);
		case BirstScriptParser.SIN:
		case BirstScriptParser.COS:
		case BirstScriptParser.TAN:
		case BirstScriptParser.ARCSIN:
		case BirstScriptParser.ARCCOS:
		case BirstScriptParser.ARCTAN:
		case BirstScriptParser.ABS:
		case BirstScriptParser.CEILING:
		case BirstScriptParser.FLOOR:
		case BirstScriptParser.SQRT:
		case BirstScriptParser.LOG:
		case BirstScriptParser.LN:
		case BirstScriptParser.EXP:
		case BirstScriptParser.POWER:
		case BirstScriptParser.RANDOM:
			return new DataType(DType.Float, 0);
		case BirstScriptParser.ISNAN:
		case BirstScriptParser.ISINF:
			return new DataType(DType.Boolean, 0);
		case BirstScriptParser.TREND:
			return new Trend(script, tree).getDataType();
		case BirstScriptParser.FUNCTIONCALL:
			if (logicalQuery)
				throw new ScriptException("Syntax error", tree.getLine(), tree.getCharPositionInLine(), tree.getCharPositionInLine() + tree.getText().length());
			de = script.getNonOutputElement(tree.getChild(0).getText());
			if (de == null)
			{
				Function f = script.getFunction(tree.getChild(0).getText());
				if (f == null)
					throw new ScriptException("Unknown function or data element reference", tree.getLine(), tree.getCharPositionInLine(),
							tree.getCharPositionInLine() + tree.getText().length());
				return getOperatorType(f.getReturnType());
			}
			return new DataType(de.getType().subtype, de.getType().width);
		default:
			break;
		}
		return null;
	}

	private static DataType getIdentDataType(String name, TransformationScript script)
	{
		/*
		 * This breaks in cases where there is one integer being divided by another integer in that case, since both are
		 * integers the return type is an integer, but it should be a float, since its division. // strip off [ and ]
		 * name = name.substring(1); name = name.substring(0, name.length() - 1); MeasureColumn mc =
		 * script.getRepository().findMeasureColumn(name); if (mc != null) return new DataType(mc.DataType, mc.Width);
		 */
		return null;
	}

	private static DataType getSetDataType(TransformationScript script, Tree tree) throws ScriptException
	{
		boolean exact = false;
		boolean userow = false;
		boolean isstat = false;
		boolean isfunc = false;
		switch (tree.getType())
		{
		case BirstScriptParser.SETLOOKUP:
			exact = true;
			userow = false;
			isstat = false;
			isfunc = false;
			break;
		case BirstScriptParser.SETLOOKUPROW:
			exact = true;
			userow = true;
			isstat = false;
			isfunc = false;
			break;
		case BirstScriptParser.SETFIND:
			exact = false;
			userow = false;
			isstat = false;
			isfunc = false;
			break;
		case BirstScriptParser.SETSTAT:
			exact = false;
			userow = false;
			isstat = true;
			isfunc = false;
			break;
		case BirstScriptParser.FUNCTION:
			exact = false;
			userow = false;
			isstat = false;
			isfunc = true;
			break;
		case BirstScriptParser.FUNCTIONLOOKUP:
			exact = true;
			userow = false;
			isstat = false;
			isfunc = true;
			break;
		}
		Operator op = new SetLookup(script, tree, exact, userow, isstat, isfunc);
		Operator.Type type = op.getType();
		return getOperatorType(type);
	}

	public static DataType getOperatorType(Operator.Type type)
	{
		if (type == Operator.Type.Boolean)
			return new DataType(DType.Boolean, 0);
		else if (type == Operator.Type.Integer)
			return new DataType(DType.Integer, 0);
		else if (type == Operator.Type.Float)
			return new DataType(DType.Float, 0);
		else if (type == Operator.Type.Varchar)
			return new DataType(DType.Varchar, 0);
		else if (type == Operator.Type.DateTime)
			return new DataType(DType.DateTime, 0);
		return null;
	}

	private static DataType typeMath(DataType dtl, DataType dtr)
	{
		if (dtl.type == DType.Float && dtr.type == DType.Float)
			return (new DataType(DType.Float, 0));
		if (dtl.type == DType.Integer && dtr.type == DType.Integer)
			return (new DataType("Integer", 20));
		if ((dtl.type == DType.Float && dtr.type == DType.Integer) || (dtr.type == DType.Float && dtl.type == DType.Integer))
			return (new DataType(DType.Float, 0));
		return (new DataType(DType.Varchar, dtl.width + dtr.width));
	}

	public boolean isLogicalQuery()
	{
		return logicalQuery;
	}

	public void setLogicalQuery(boolean logicalQuery)
	{
		this.logicalQuery = logicalQuery;
	}
}
