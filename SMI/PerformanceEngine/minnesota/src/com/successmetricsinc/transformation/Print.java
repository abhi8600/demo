package com.successmetricsinc.transformation;

import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.util.logging.TraceLog;

public class Print extends Statement
{
	private TransformationScript script;
	private Tree t;
	private Operator compiledAssignment;
	
	public Print(TransformationScript script, Tree t) throws ScriptException
	{
		this.script = script;
		this.t = t;
		// XXX no type checking for now, knife in the teeth debugging
		Expression ex = new Expression(this.script);
		compiledAssignment = ex.compile(this.t.getChild(0));
	}
	
	@Override
	public void execute() throws ScriptException
	{
		Object o = compiledAssignment == null ? null : compiledAssignment.evaluate();
		TraceLog tlog = script.getTraceLog();
		if (tlog == null)
			return; // failsafe check
		synchronized (tlog)
		{
			tlog.log(o == null ? "null" : o.toString());
		}
	}
}