package com.successmetricsinc.transformation;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import org.antlr.runtime.tree.Tree;
import org.apache.log4j.Logger;

import com.successmetricsinc.transformation.DataType.DType;

public class Assignment extends Statement
{
	private Operator compiledAssignment;
	private String targetName;
	private DataElement targetElement;
	private Operator lookup;
	private DType targetType;
	private static Logger logger = Logger.getLogger(Assignment.class);

	public Assignment(TransformationScript script, Tree t) throws ScriptException
	{
		this.script = script;
		DataElement de;
		if (t.getChild(0).getType() == BirstScriptParser.FUNCTIONCALL)
		{
			targetName = t.getChild(0).getChild(0).getText();
			de = script.findElement(targetName);
			if (de == null)
				throw new ScriptException("Error line " + t.getChild(0).getLine() + ", " + targetName + " not found", t.getChild(0).getLine(), t.getChild(0)
						.getCharPositionInLine(), t.getChild(0).getCharPositionInLine() + t.getChild(0).getText().length());
			targetType = de.getType().type;
			DType ltype = Expression.getType(script, t.getChild(0).getChild(1)).type;
			if (ltype != DType.Integer && targetType == DType.List)
				throw new ScriptException("Array index must be an integer: " + Statement.toString(t, null), t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine()
						+ t.getText().length());
			else if (targetType == DType.Map)
			{
				if (de.getType().lookuptype != ltype)
					throw new ScriptException("Map lookup value must match declared data type for lookup", t.getLine(), t.getCharPositionInLine(), t
							.getCharPositionInLine()
							+ t.getText().length());
			}
			Expression ex = new Expression(script);
			lookup = ex.compile(t.getChild(0).getChild(1));
		} else
		{
			targetName = t.getChild(0).getText();
			de = script.findNonInputElement(targetName);
			if (de == null)
				throw new ScriptException("Error line " + t.getChild(0).getLine() + ", " + targetName + " not found", t.getChild(0).getLine(), t.getChild(0)
						.getCharPositionInLine(), t.getChild(0).getCharPositionInLine() + t.getChild(0).getText().length());
			targetType = de.getType().type;
			if (targetType == DType.None)
				throw new ScriptException("Cannot assign values to columns of datatype None", t.getChild(0).getLine(), t.getChild(0).getCharPositionInLine(), t
						.getChild(0).getCharPositionInLine()
						+ t.getChild(0).getText().length());
		}
		Tree etree = t.getChild(1);
		DataType type = Expression.getType(script, etree);
		if (type == null)
			throw new ScriptException("Data element " + etree.getText() + " not found: line " + t.getLine() + ", " + de.getType().type,
					t.getChild(1).getLine(), t.getChild(1).getCharPositionInLine(), t.getChild(1).getCharPositionInLine() + t.getChild(1).getText().length());
		DType ttype = targetType;
		if (targetType == DType.List || targetType == DType.Map)
			ttype = de.getType().subtype;
		DType stype = type.type;
		if (stype == DType.List || stype == DType.Map)
			stype = type.subtype;
		if (!canAssignTo(ttype, stype))
			throw new ScriptException("Incompatible types line " + t.getLine() + ", " + de.getType().type + " cannot be assigned a value of type " + stype, t
					.getChild(1).getLine(), t.getChild(1).getCharPositionInLine(), t.getChild(1).getCharPositionInLine() + t.getChild(1).getText().length());
		compiledAssignment = compile(etree);
		targetElement = de;
		this.line = t.getLine();
		this.start = t.getCharPositionInLine();
		this.stop = t.getText().length() + this.start;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void execute() throws ScriptException
	{
		Object o = compiledAssignment == null ? null : compiledAssignment.evaluate();
		if (lookup != null)
		{
			if (targetType == DType.List)
			{
				List<Object> list = (List<Object>) targetElement.getValue(null);
				list.set((Integer) lookup.evaluate(), clipToType(o, targetElement.getType()));
			} else if (targetType == DType.Map)
			{
				Map<Object, Object> map = (Map<Object, Object>) targetElement.getValue(null);
				Object prevVal = map.put(lookup.evaluate(), clipToType(o, targetElement.getType()));
				if (prevVal == null)
				{
					script.curMemory += targetElement.getType().getInitializedSize();
					if (script.curMemory > (script.maxMemory*0.9))
					{
						logger.warn("ETL Script data structures are consuming 90% of max limit " + (script.maxMemory) + ". Script execution will fail when max limit is reached.");
					}
					if (script.curMemory > script.maxMemory)
						throw new ScriptException("Attempt to add map element exceeded memory limits", line, start, stop);
				}
			}
		} else
		{
			if (o == null)
			{
				targetElement.setValue(null);
			} else
			{
				DType type = targetElement.getType().type;
				if (type == DType.Varchar)
				{
					String s = o.toString();
					if (s.length() > targetElement.getType().width)
						s = s.substring(0, targetElement.getType().width);
					targetElement.setValue(s);
				} else if (type == DType.Integer && compiledAssignment.getType() == Operator.Type.Boolean)
				{
					targetElement.setValue(((Boolean) o) ? 1 : 0);
				} else if (type == DType.Integer)
				{
					try
					{
						if (Long.class.isInstance(o))
							targetElement.setValue(((Long) o).longValue());
						else if (Integer.class.isInstance(o))
							targetElement.setValue(o);
						else if (Double.class.isInstance(o))
							targetElement.setValue(((Double) o).longValue());
						else if (Boolean.class.isInstance(o))
							targetElement.setValue(((Boolean) o) ? 1.0 : 0.0);
						else
							targetElement.setValue(Long.valueOf(o.toString()));
					} catch (Exception ex)
					{
						targetElement.setValue(0);
					}
				} else if (type == DType.Boolean)
				{
					try
					{
						if (Boolean.class.isInstance(o))
							targetElement.setValue(o);
						else if (Integer.class.isInstance(o))
							targetElement.setValue(Boolean.valueOf(((Integer) o).intValue() != 0));
						else if (Long.class.isInstance(o))
							targetElement.setValue(Boolean.valueOf(((Long) o).intValue() != 0));
						else
							targetElement.setValue(Boolean.valueOf(o.toString()));
					} catch (Exception ex)
					{
						targetElement.setValue(false);
					}
				} else if (type == DType.Float)
				{
					try
					{
						if (Double.class.isInstance(o))
							targetElement.setValue(o);
						else if (Integer.class.isInstance(o))
							targetElement.setValue(((Integer) o).doubleValue());
						else if (Long.class.isInstance(o))
							targetElement.setValue(((Long) o).doubleValue());
						else if (Boolean.class.isInstance(o))
							targetElement.setValue(((Boolean) o) ? 1.0 : 0.0);
						else
							targetElement.setValue(Double.valueOf(o.toString()));
					} catch (Exception ex)
					{
						targetElement.setValue(0.0);
					}
				} else if (type == DType.Date || type == DType.DateTime)
				{
					if (Date.class.isInstance(o))
						targetElement.setValue(o);
					else if (GregorianCalendar.class.isInstance(o))
						targetElement.setValue(((GregorianCalendar)o).getTime());
					else
					{
						targetElement.setValue(Definition.getDate(o.toString(), script.getRepository(), script.getSession()));
					}
				} else
				{
					targetElement.setValue(o);
				}
			}
		}
	}

	public static Object clipToType(Object o, DataType type)
	{
		if (type.subtype == DType.Varchar)
		{
			String s = o == null ? "" : o.toString();
			if (s.length() > type.width)
				s = s.substring(0, type.width);
			return s;
		} else
			return o;
	}

	public static boolean canAssignTo(DType targetType, DType sourceType)
	{
		if (targetType == DType.Varchar || targetType == DType.Any)
			return true;
		if (targetType == DType.Integer && (sourceType != DType.Integer && sourceType != DType.Boolean && sourceType != DType.Any && sourceType != DType.Float && sourceType != DType.Varchar))
			return false;
		if (targetType == DType.Date && (sourceType != DType.Date && sourceType != DType.DateTime && sourceType != DType.Any && sourceType != DType.Varchar))
			return false;
		if (targetType == DType.DateTime
				&& (sourceType != DType.Date && sourceType != DType.DateTime && sourceType != DType.Any && sourceType != DType.Varchar))
			return false;
		if (targetType == DType.Float && (sourceType != DType.Float && sourceType != DType.Integer && sourceType != DType.Boolean && sourceType != DType.Any && sourceType != DType.Varchar))
			return false;
		if (targetType == DType.Boolean && sourceType != DType.Boolean && sourceType != DType.Any && sourceType != DType.Integer && sourceType != DType.Varchar)
			return false;
		return true;
	}

	private Operator compile(Tree tree) throws ScriptException
	{
		Expression ex = new Expression(script);
		return (ex.compile(tree));
	}
	
	public boolean isTargetVariable()
	{
		if((targetElement != null) &&  (targetElement instanceof VariableDataElement))
		{
			return true;
		}
		return false;
	}
}
