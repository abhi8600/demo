package com.successmetricsinc.transformation;

import org.antlr.runtime.tree.Tree;

public class Halt extends Statement
{
	private TransformationScript script;
	private Tree t;
	private Operator compiledAssignment;

	public Halt(TransformationScript script, Tree t) throws ScriptException 
	{
		this.script = script;
		this.t = t;
		Expression ex = new Expression(this.script);
		compiledAssignment = ex.compile(this.t.getChild(0));
	}
	
	@Override
	public void execute() throws ScriptException
	{
		Object o = compiledAssignment == null ? null : compiledAssignment.evaluate();
		throw new ScriptException(o == null ? "null" : o.toString(), t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
	}
}
