package com.successmetricsinc.transformation;

import java.io.IOException;
import java.io.OutputStreamWriter;

import org.antlr.runtime.tree.Tree;

public class RWrite extends Statement
{
	private TransformationScript script;
	private Tree t;
	private Operator rPath;
	private Operator expression;

	public RWrite(TransformationScript script, Tree t) throws ScriptException
	{
		this.script = script;
		this.t = t;
		// XXX no type checking for now, knife in the teeth debugging
		Expression ex = new Expression(this.script);
		rPath = ex.compile(this.t.getChild(0));
		expression = ex.compile(this.t.getChild(1));
	}

	@Override
	public void execute() throws ScriptException
	{
		if (rPath.getType() != Operator.Type.Varchar)
			if (this.t.getChild(0).getType() != BirstScriptParser.STRING)
				throw new ScriptException("Invalid path specified for writing to R Server: ", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine()
						+ t.getText().length());
		String path = rPath.evaluate().toString();
		OutputStreamWriter writer = script.getRWriter(path);
		Object o = expression == null ? null : expression.evaluate();
		try
		{
			writer.write(o.toString());
		} catch (IOException e)
		{
			throw new ScriptException(e.getMessage(), t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
	}
}