/**
 * 
 */
package com.successmetricsinc.transformation;

import com.successmetricsinc.query.DisplayExpression;
import com.successmetricsinc.query.FieldProvider;
import com.successmetricsinc.transformation.DataType.DType;

/**
 * @author bpeters
 * 
 */
public class LogicalQueryDataElement implements DataElement
{
	public DisplayExpression displayExpression;
	public String name;
	public DataType type;
	public int index;
	public byte fieldType;
	private boolean parentType;
	private FieldProvider fp;

	@Override
	public boolean canGet()
	{
		return true;
	}

	@Override
	public boolean canSet()
	{
		return false;
	}

	@Override
	public String getName()
	{
		return name;
	}

	@Override
	public DataType getType()
	{
		return type;
	}

	@Override
	public Object getValue(Operator lookup) throws ScriptException
	{
		DisplayExpression de = displayExpression;
		if (de.ClonedChildren != null && de.ClonedChildren.containsKey(Thread.currentThread()))
		{
			de = de.ClonedChildren.get(Thread.currentThread());
		}
		if (parentType)
			fp = de.getParentFields();
		else
			fp = de.getFields();
		if (fp == null)
			return null;
		try
		{
			if (type.type == DType.Integer)
				return fp.getIntProperty(fieldType, index);
			else if (type.type == DType.Float)
				return fp.getDoubleProperty(fieldType, index);
			else if (type.type == DType.Varchar)
				return fp.getStringProperty(fieldType, index);
			else
				return fp.getObjectProperty(fieldType, index);
		} catch (Exception e)
		{
			return 0;
		}
	}

	@Override
	public void setValue(Object o)
	{
	}

	public boolean isParentType()
	{
		return parentType;
	}

	public void setParentType(boolean parentType)
	{
		this.parentType = parentType;
	}
}
