package com.successmetricsinc.transformation;

import org.antlr.runtime.tree.Tree;

public abstract class Operator
{
	public enum Type
	{
		Boolean, Integer, Float, Varchar, DateTime, List
	}
	protected int line;

	public Operator(TransformationScript script, Tree t)
	{
		if (t != null)
			this.line = t.getLine();
	}

	public abstract Operator clone();

	public Object evaluate() throws ScriptException
	{
		return null;
	}

	public Type getType()
	{
		return null;
	}

	public boolean equals(Operator b) throws ScriptException
	{
		return false;
	}

	public int compareTo(Operator b) throws ScriptException
	{
		return 0;
	}

	public int getSize()
	{
		return 0;
	}

	public int getLine()
	{
		return line;
	}
}
