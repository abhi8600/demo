package com.successmetricsinc.transformation.transforms;

import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.transformation.Expression;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.transformation.object.IIF;

public class GetLevelValue extends Operator
{
	private Operator levelNum;
	private Tree t;
	private TransformationScript script;

	public GetLevelValue(TransformationScript script, Tree t) throws ScriptException
	{
		super(script, t);
		this.t = t;
		this.script = script;
		Expression ex = new Expression(script);
		levelNum = ex.compile(t.getChild(0));
		if (levelNum.getType() != Operator.Type.Integer)
			throw new ScriptException("GetLevelValue must be supplied an integer value to specify a level", t.getLine(), t.getCharPositionInLine(),
					t.getCharPositionInLine() + t.getText().length());
	}

	@Override
	public Operator clone()
	{
		try
		{
			return new GetLevelValue(script, t);
		} catch (ScriptException e)
		{
			return null;
		}
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		Object value = evaluate();
		if (b == null)
			return value == null ? 0 : 1;
		else if (value == null)
			return -1;
		Object bo = b.evaluate();
		return value.toString().compareTo(bo.toString());
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		Object value = evaluate();
		if (b == null)
			return value == null;
		else if (value == null)
			return false;
		Object bo = b.evaluate();
		return value.equals(bo.toString());
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		FlattenParentChild fpc = script.getFlattenParentChild();
		if (fpc == null)
			return "";
		Object o = levelNum.evaluate();
		if (o instanceof Integer)
			return fpc.getLevelValue((Integer) o);
		else if (o instanceof Long)
			return fpc.getLevelValue(((Long) o).intValue());
		return null;
	}

	@Override
	public int getSize()
	{
		return 0;
	}

	@Override
	public Type getType()
	{
		return Operator.Type.Varchar;
	}
}
