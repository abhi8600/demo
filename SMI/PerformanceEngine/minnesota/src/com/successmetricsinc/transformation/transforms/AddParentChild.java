/**
 * 
 */
package com.successmetricsinc.transformation.transforms;

import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.transformation.Expression;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.Statement;
import com.successmetricsinc.transformation.TransformationScript;

/**
 * @author bpeters
 * 
 */
public class AddParentChild extends Statement
{
	private Operator parent;
	private Operator child;

	public AddParentChild(TransformationScript script, Tree t) throws ScriptException
	{
		this.script = script;
		Expression ex = new Expression(script);
		parent = ex.compile(t.getChild(0));
		child = ex.compile(t.getChild(1));
	}

	@Override
	public void execute() throws ScriptException
	{
		Object parentObj = parent.evaluate();
		Object childObj = child.evaluate();
		script.addParentChild(parentObj, childObj);
	}
}
