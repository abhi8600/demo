/**
 * 
 */
package com.successmetricsinc.transformation.transforms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * @author Brad
 * 
 */
public class FlattenParentChild
{
	private Map<Object, Object> childParentMap = new HashMap<Object, Object>();
	private Iterator<Entry<Object, Object>> iterator;
	private Object curObject;
	private Integer numLevels;

	public boolean nextChild()
	{
		if (childParentMap == null)
			return false;
		if (iterator == null)
			iterator = childParentMap.entrySet().iterator();
		if (iterator.hasNext())
		{
			curObject = iterator.next().getKey();
			return true;
		}
		return false;
	}

	public void addParentChild(Object parent, Object child)
	{
		iterator = null;
		childParentMap.put(child, parent);
	}

	public String getLevelValue(int level)
	{
		List<Object> traversed = new ArrayList<Object>(20);
		int depth = getDepth(curObject, traversed);
		if (level < depth)
		{
			Object o = traversed.get(traversed.size() - level - 1);
			return o == null ? null : o.toString();
		}
		return curObject == null ? null : curObject.toString();
	}

	private int getDepth(Object child, List<Object> traversed)
	{
		traversed.add(child);
		Object parent = childParentMap.get(child);
		if (parent == null || traversed.contains(parent))
			return 0;
		return 1 + getDepth(parent, traversed);
	}

	public int getNumLevels()
	{
		if (childParentMap == null)
			return 0;
		if (numLevels != null)
			return numLevels;
		int maxDepth = 0;
		for (Object child : childParentMap.keySet())
		{
			List<Object> traversed = new ArrayList<Object>(20);
			int depth = getDepth(child, traversed);
			if (depth > maxDepth)
				maxDepth = depth;
		}
		numLevels = maxDepth + 1;
		return numLevels;
	}

	public int getCurrentDepth()
	{
		if (childParentMap == null)
			return 0;
		if (curObject == null)
			return 0;
		List<Object> traversed = new ArrayList<Object>(20);
		int depth = getDepth(curObject, traversed);
		return depth;
	}
}
