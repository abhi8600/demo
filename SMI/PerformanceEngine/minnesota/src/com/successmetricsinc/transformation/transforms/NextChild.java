package com.successmetricsinc.transformation.transforms;

import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.transformation.operators.OpBoolean;

public class NextChild extends Operator
{
	private TransformationScript script;
	private Tree t;

	public NextChild(TransformationScript script, Tree t) throws ScriptException
	{
		super(script, t);
		this.t = t;
		this.script = script;
	}

	@Override
	public Operator clone()
	{
		try
		{
			return new NextChild(script, t);
		} catch (ScriptException e)
		{
			return null;
		}
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		return (new OpBoolean((Boolean) evaluate())).compareTo(b);
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		return (new OpBoolean((Boolean) evaluate())).equals(b);
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		return script.getFlattenParentChild().nextChild();
	}

	@Override
	public int getSize()
	{
		return 1;
	}

	@Override
	public Type getType()
	{
		return Operator.Type.Boolean;
	}
}
