package com.successmetricsinc.transformation.transforms;

import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.transformation.BirstScriptParser;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.transformation.operators.OpInt;

public class GetLevelAttribute extends Operator
{
	private enum AttributeType
	{
		NumLevels, CurrentDepth
	}
	private Tree t;
	private TransformationScript script;
	private AttributeType type;

	public GetLevelAttribute(TransformationScript script, Tree t) throws ScriptException
	{
		super(script, t);
		this.t = t;
		this.script = script;
		if (t.getChild(0).getType() != BirstScriptParser.STRING)
			throw new ScriptException("Attribute name must be a string", t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine()
					+ t.getText().length());
		String val = t.getChild(0).getText().toLowerCase();
		if (val.equals("'numlevels'"))
		{
			type = AttributeType.NumLevels;
		} else if (val.equals("'currentdepth'"))
		{
			type = AttributeType.CurrentDepth;
		}
	}

	@Override
	public Operator clone()
	{
		try
		{
			return new GetLevelAttribute(script, t);
		} catch (ScriptException e)
		{
			return null;
		}
	}

	@Override
	public int compareTo(Operator b) throws ScriptException
	{
		Object ob = this.evaluate();
		long value = 0l;
		if (ob instanceof Integer)
			value = ((Integer) ob).longValue();
		else if (ob instanceof Long)
			value = (Long) ob;
		return (new OpInt(value)).compareTo(b);		
	}

	@Override
	public boolean equals(Operator b) throws ScriptException
	{
		Object ob = this.evaluate();
		long value = 0l;
		if (ob instanceof Integer)
			value = ((Integer) ob).longValue();
		else if (ob instanceof Long)
			value = (Long) ob;
		return (new OpInt(value)).equals(b);		
	}

	@Override
	public Object evaluate() throws ScriptException
	{
		FlattenParentChild fpc = script.getFlattenParentChild();
		if (fpc == null)
			return null;
		if (type == AttributeType.NumLevels)
			return fpc.getNumLevels();
		else if (type == AttributeType.CurrentDepth)
			return fpc.getCurrentDepth();
		return null;
	}

	@Override
	public int getSize()
	{
		return Integer.SIZE / 8;
	}

	@Override
	public Type getType()
	{
		return Operator.Type.Integer;
	}
}
