package com.successmetricsinc.transformation;

import com.successmetricsinc.transformation.operators.OpDateTime;

public class DataType
{
	public enum DType
	{
		Varchar, Integer, Float, Date, DateTime, Boolean, Any, List, Map, None
	};
	public DType type;
	public DType subtype;
	public DType lookuptype;
	public int width;

	public DataType(DType type, int width)
	{
		this.type = type;
		this.width = width;
	}

	private DType getDType(String name)
	{
		if (name.equalsIgnoreCase("varchar"))
		{
			return DType.Varchar;
		} else if (name.equalsIgnoreCase("integer"))
		{
			return DType.Integer;
		} else if (name.equalsIgnoreCase("float"))
		{
			return DType.Float;
		} else if (name.equalsIgnoreCase("number"))
		{
			return DType.Float;
		} else if (name.equalsIgnoreCase("date"))
		{
			return DType.Date;
		} else if (name.equalsIgnoreCase("datetime"))
		{
			return DType.DateTime;
		} else if (name.equalsIgnoreCase("boolean"))
		{
			return DType.Boolean;
		} else if (name.equalsIgnoreCase("list"))
		{
			return DType.List;
		} else if (name.equalsIgnoreCase("map"))
		{
			return DType.Map;
		} else if (name.equalsIgnoreCase("none"))
		{
			return DType.None;
		}
		return null;
	}

	public DataType(String name, String subtypename, int width)
	{
		this.type = getDType(name);
		this.subtype = getDType(subtypename);
		this.width = width;
	}

	public DataType(String name, String subtypename, int width, String lookuptypename)
	{
		this.type = getDType(name);
		this.subtype = getDType(subtypename);
		this.width = width;
		this.lookuptype = getDType(lookuptypename);
	}

	public DataType(String name, int width)
	{
		this.type = getDType(name);
		this.width = width;
	}

	public String toString()
	{
		if (type == DType.Varchar)
			return "Varchar(" + width + ")";
		else if (type == DType.Integer)
			return "Integer";
		else if (type == DType.Float)
			return "Float";
		else if (type == DType.Date)
			return "Date";
		else if (type == DType.DateTime)
			return "DateTime";
		else if (type == DType.Boolean)
			return "Boolean";
		else if (type == DType.Map)
			return "Map";
		else if (type == DType.List)
			return "List";
		else if (type == DType.None)
			return "None";
		return null;
	}

	private int getSizeOfType(DType type)
	{
		if (type == DType.Varchar)
			return width;
		else if (type == DType.Integer)
			return Integer.SIZE / 8;
		else if (type == DType.Float)
			return Double.SIZE / 8;
		else if (type == DType.Date)
			return OpDateTime.DATE_SIZE;
		else if (type == DType.DateTime)
			return OpDateTime.DATE_SIZE;
		else if (type == DType.Boolean)
			return 1;
		return 0;
	}

	public int getInitializedSize()
	{
		if (type == DType.List)
			return getSizeOfType(subtype);
		else if (type == DType.Map)
			return getSizeOfType(subtype);
		return getSizeOfType(type);
	}
	
	public static DataType getType(Operator.Type t) {
		switch (t) {
		case Boolean:
			return new DataType(DType.Boolean, 0);
		case DateTime:
			return new DataType(DType.DateTime, 0);
		case Float:
			return new DataType(DType.Float, 0);
		case Integer:
			return new DataType(DType.Integer, 0);
		case List:
			return new DataType(DType.List, 0);
		case Varchar:
			default:
				return new DataType(DType.Varchar, 0);
		}
	}
}
