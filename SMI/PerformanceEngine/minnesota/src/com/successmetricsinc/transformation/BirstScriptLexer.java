// $ANTLR 3.4 C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g 2014-03-25 12:51:06
 package com.successmetricsinc.transformation; 

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class BirstScriptLexer extends Lexer {
    public static final int EOF=-1;
    public static final int T__230=230;
    public static final int T__231=231;
    public static final int T__232=232;
    public static final int T__233=233;
    public static final int T__234=234;
    public static final int ABS=4;
    public static final int ADD=5;
    public static final int ADDPARENTCHILD=6;
    public static final int ALIAS=7;
    public static final int ALLCHILDREN=8;
    public static final int AND=9;
    public static final int ARCCOS=10;
    public static final int ARCSIN=11;
    public static final int ARCTAN=12;
    public static final int AS=13;
    public static final int ASCENDING=14;
    public static final int ASSIGNMENT=15;
    public static final int AVG=16;
    public static final int BEGIN=17;
    public static final int BITAND=18;
    public static final int BITOR=19;
    public static final int BOOLEAN=20;
    public static final int BY=21;
    public static final int CAST=22;
    public static final int CEILING=23;
    public static final int CHILDREN=24;
    public static final int COLUMN=25;
    public static final int COLUMNS=26;
    public static final int COLUMN_NAME=27;
    public static final int COMMA=28;
    public static final int COMPLETE=29;
    public static final int COS=30;
    public static final int COUNT=31;
    public static final int COUNTDISTINCT=32;
    public static final int CUBE=33;
    public static final int CURRENTVALUE=34;
    public static final int DATEADD=35;
    public static final int DATEDIFF=36;
    public static final int DATEFIELD=37;
    public static final int DATEPART=38;
    public static final int DATETIME=39;
    public static final int DATETIMETYPE=40;
    public static final int DATETYPE=41;
    public static final int DELETE=42;
    public static final int DESCENDING=43;
    public static final int DIM=44;
    public static final int DISPLAYBY=45;
    public static final int DISPLAYWHERE=46;
    public static final int DISTINCT=47;
    public static final int DIV=48;
    public static final int DRANK=49;
    public static final int ELSE=50;
    public static final int ELSEIF=51;
    public static final int END=52;
    public static final int ENDCOMPLETE=53;
    public static final int ENDFUNCTION=54;
    public static final int ENDIF=55;
    public static final int ENDWHILE=56;
    public static final int EQUALS=57;
    public static final int EXITFOR=58;
    public static final int EXITWHILE=59;
    public static final int EXP=60;
    public static final int EscapeSequence=61;
    public static final int FLOAT=62;
    public static final int FLOATTYPE=63;
    public static final int FLOOR=64;
    public static final int FOR=65;
    public static final int FORMAT=66;
    public static final int FROM=67;
    public static final int FULLOUTERJOIN=68;
    public static final int FUNCTION=69;
    public static final int FUNCTIONCALL=70;
    public static final int FUNCTIONDEF=71;
    public static final int FUNCTIONLOOKUP=72;
    public static final int GETCOLUMNVALUE=73;
    public static final int GETDAYID=74;
    public static final int GETLEVELATTRIBUTE=75;
    public static final int GETLEVELVALUE=76;
    public static final int GETMONTHID=77;
    public static final int GETPROMPTFILTER=78;
    public static final int GETPROMPTVALUE=79;
    public static final int GETVARIABLE=80;
    public static final int GETWEEKID=81;
    public static final int GRAIN=82;
    public static final int GRAIN_COLUMN=83;
    public static final int GRAIN_TABLE=84;
    public static final int GROUPS=85;
    public static final int GT=86;
    public static final int GTEQ=87;
    public static final int HALT=88;
    public static final int HexDigit=89;
    public static final int IDENT=90;
    public static final int IDENT_NAME=91;
    public static final int IF=92;
    public static final int IFNULL=93;
    public static final int IFTHEN=94;
    public static final int IIF=95;
    public static final int IN=96;
    public static final int INNERJOIN=97;
    public static final int INPUT=98;
    public static final int INTEGER=99;
    public static final int INTEGERTYPE=100;
    public static final int ISINF=101;
    public static final int ISNAN=102;
    public static final int ISNOTNULL=103;
    public static final int ISNULL=104;
    public static final int JOIN=105;
    public static final int LEAVES=106;
    public static final int LEFTOUTERJOIN=107;
    public static final int LEFT_PAREN=108;
    public static final int LENGTH=109;
    public static final int LET=110;
    public static final int LEVEL=111;
    public static final int LEVEL_COLUMN=112;
    public static final int LEVEL_TABLE=113;
    public static final int LIKE=114;
    public static final int LINE_COMMENT=115;
    public static final int LIST=116;
    public static final int LN=117;
    public static final int LOG=118;
    public static final int LOGICALMEMBERSET=119;
    public static final int LOGICAL_MEMBER_SET_PREFIX=120;
    public static final int LT=121;
    public static final int LTEQ=122;
    public static final int MAP=123;
    public static final int MATHMAX=124;
    public static final int MATHMIN=125;
    public static final int MAX=126;
    public static final int MDXEXPRESSION=127;
    public static final int MEASURETABLE=128;
    public static final int MEMBEREXPRESSIONS=129;
    public static final int METHOD=130;
    public static final int MIN=131;
    public static final int MINUS=132;
    public static final int MINUSEQUALS=133;
    public static final int ML_COMMENT=134;
    public static final int MOD=135;
    public static final int MULT=136;
    public static final int NEGATE=137;
    public static final int NEXT=138;
    public static final int NEXTCHILD=139;
    public static final int NONETYPE=140;
    public static final int NOT=141;
    public static final int NOTEQUALS=142;
    public static final int NOTIN=143;
    public static final int NOTLIKE=144;
    public static final int NULL=145;
    public static final int NULLS_FIRST=146;
    public static final int NULLS_LAST=147;
    public static final int NUMBERTYPE=148;
    public static final int NUMROWS=149;
    public static final int OLAPCUBE=150;
    public static final int OLAPPROJECTIONLIST=151;
    public static final int ON=152;
    public static final int OR=153;
    public static final int ORDERBY=154;
    public static final int OUTPUT=155;
    public static final int OUTPUTCOLUMN=156;
    public static final int OUTPUTCOLUMNLIST=157;
    public static final int PARALLEL=158;
    public static final int PARAMETER=159;
    public static final int PAREN=160;
    public static final int PARENTS=161;
    public static final int PARTITION=162;
    public static final int PLUS=163;
    public static final int PLUSEQUALS=164;
    public static final int POSITION=165;
    public static final int POSITION_COLUMN_START=166;
    public static final int POW=167;
    public static final int POWER=168;
    public static final int PRINT=169;
    public static final int PROJECTIONLIST=170;
    public static final int PROJECTIONLISTS=171;
    public static final int PTILE=172;
    public static final int PUT=173;
    public static final int QUALIFIER=174;
    public static final int RANDOM=175;
    public static final int RANK=176;
    public static final int REMOVEALL=177;
    public static final int REMOVEAT=178;
    public static final int REMOVEITEM=179;
    public static final int REPLACE=180;
    public static final int REPLACEALL=181;
    public static final int RETURN=182;
    public static final int REXP=183;
    public static final int RIGHTOUTERJOIN=184;
    public static final int RIGHT_PAREN=185;
    public static final int ROWNUMBER=186;
    public static final int RSUM=187;
    public static final int RWRITE=188;
    public static final int SAVEDEXPRESSION=189;
    public static final int SCRIPT=190;
    public static final int SELECT=191;
    public static final int SELECTED=192;
    public static final int SETFIND=193;
    public static final int SETLOOKUP=194;
    public static final int SETLOOKUPROW=195;
    public static final int SETSTAT=196;
    public static final int SIN=197;
    public static final int SINGLEQUERY=198;
    public static final int SPARSE=199;
    public static final int SQRT=200;
    public static final int STATMEDIAN=201;
    public static final int STATSTDDEV=202;
    public static final int STEP=203;
    public static final int STRING=204;
    public static final int SUBSTRING=205;
    public static final int SUM=206;
    public static final int TAN=207;
    public static final int THEN=208;
    public static final int TO=209;
    public static final int TOLOWER=210;
    public static final int TOP=211;
    public static final int TOSTRING=212;
    public static final int TOTIME=213;
    public static final int TOUPPER=214;
    public static final int TRANSFORM=215;
    public static final int TREND=216;
    public static final int TRIM=217;
    public static final int UNION=218;
    public static final int UNIONALL=219;
    public static final int UPDATESFDC=220;
    public static final int URLENCODE=221;
    public static final int USINGOUTERJOIN=222;
    public static final int UnicodeEscape=223;
    public static final int VALUELIST=224;
    public static final int VARCHARTYPE=225;
    public static final int WHERE=226;
    public static final int WHILE=227;
    public static final int WRITERECORD=228;
    public static final int WS=229;

    	List<RecognitionException> exceptions = new ArrayList<RecognitionException>();

    	@Override
    	public void reportError(RecognitionException e)
    	{
    	    exceptions.add(e);
    	}

    	public boolean hasError()
    	{
    		return exceptions.size() > 0;
    	}

    	public RecognitionException errorMessage()
    	{
    		return exceptions.get(0);
    	}
    	
    	/*Hack to control max iterations in predict method for a dfa*/
    	class DFA extends org.antlr.runtime.DFA
    	{
    		@Override
            	public int predict(IntStream input)
            		throws RecognitionException
            	{
            		if ( debug ) {
            			System.err.println("Enter DFA.predict for decision "+decisionNumber);
            		}
            		int mark = input.mark(); // remember where decision started in input
            		int s = 0; // we always start at s0
            		int lastIndex = 0;
            		int currentIndex = 0;
            		int iterationCounter = 0;
            		final int MAX_ITERATIONS_ALLOWED = 1000;
            		try {
            			while ( true ) {
            				if ( debug ) System.err.println("DFA "+decisionNumber+" state "+s+" LA(1)="+(char)input.LA(1)+"("+input.LA(1)+
            												"), index="+input.index());
            				int specialState = special[s];
            				lastIndex = input.index();
            				if ( specialState>=0 ) {
            					if ( debug ) {
            						System.err.println("DFA "+decisionNumber+
            							" state "+s+" is special state "+specialState);
            					}
            					s = specialStateTransition(specialState,input);
            					if ( debug ) {
            						System.err.println("DFA "+decisionNumber+
            							" returns from special state "+specialState+" to "+s);
            					}
            					if ( s==-1 ) {
            						noViableAlt(s,input);
            						return 0;
            					}
            					input.consume();
            					continue;
            				}
            				if ( accept[s] >= 1 ) {
            					if ( debug ) System.err.println("accept; predict "+accept[s]+" from state "+s);
            					return accept[s];
            				}
            				// look for a normal char transition
            				char c = (char)input.LA(1); // -1 == \uFFFF, all tokens fit in 65000 space
            				if (c>=min[s] && c<=max[s]) {
            					int snext = transition[s][c-min[s]]; // move to next state
            					if ( snext < 0 ) {
            						// was in range but not a normal transition
            						// must check EOT, which is like the else clause.
            						// eot[s]>=0 indicates that an EOT edge goes to another
            						// state.
            						if ( eot[s]>=0 ) {  // EOT Transition to accept state?
            							if ( debug ) System.err.println("EOT transition");
            							s = eot[s];
            							input.consume();
            							// TODO: I had this as return accept[eot[s]]
            							// which assumed here that the EOT edge always
            							// went to an accept...faster to do this, but
            							// what about predicated edges coming from EOT
            							// target?
            							continue;
            						}
            						noViableAlt(s,input);
            						return 0;
            					}
            					s = snext;
            					input.consume();
            					/* Allow 1000 iterations on the same character then exit */
            					currentIndex = input.index();
            					if(currentIndex == lastIndex)
            					{
            						iterationCounter++;
            						if(iterationCounter > MAX_ITERATIONS_ALLOWED)
            						{
            							noViableAlt(s,input);
            							return 0;
            						}
            					}
            					else
            					{
            						iterationCounter = 0;
            					}
            					continue;
            					
            				}
            				if ( eot[s]>=0 ) {  // EOT Transition?
            					if ( debug ) System.err.println("EOT transition");
            					s = eot[s];
            					input.consume();
            					continue;
            				}
            				if ( c==(char)Token.EOF && eof[s]>=0 ) {  // EOF Transition to accept state?
            					if ( debug ) System.err.println("accept via EOF; predict "+accept[eof[s]]+" from "+eof[s]);
            					return accept[eof[s]];
            				}
            				// not in range and not EOF/EOT, must be invalid symbol
            				if ( debug ) {
            					System.err.println("min["+s+"]="+min[s]);
            					System.err.println("max["+s+"]="+max[s]);
            					System.err.println("eot["+s+"]="+eot[s]);
            					System.err.println("eof["+s+"]="+eof[s]);
            					for (int p=0; p<transition[s].length; p++) {
            						System.err.print(transition[s][p]+" ");
            					}
            					System.err.println();
            				}
            				noViableAlt(s,input);
            				return 0;
            			}
            		}
            		finally {
            			input.rewind(mark);
            		}
            	}
        	
    	    }
    		


    // delegates
    // delegators
    public Lexer[] getDelegates() {
        return new Lexer[] {};
    }

    public BirstScriptLexer() {} 
    public BirstScriptLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public BirstScriptLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);
    }
    public String getGrammarFileName() { return "C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g"; }

    // $ANTLR start "T__230"
    public final void mT__230() throws RecognitionException {
        try {
            int _type = T__230;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:142:8: ( ':' )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:142:10: ':'
            {
            match(':'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__230"

    // $ANTLR start "T__231"
    public final void mT__231() throws RecognitionException {
        try {
            int _type = T__231;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:143:8: ( '@' )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:143:10: '@'
            {
            match('@'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__231"

    // $ANTLR start "T__232"
    public final void mT__232() throws RecognitionException {
        try {
            int _type = T__232;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:144:8: ( ']' )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:144:10: ']'
            {
            match(']'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__232"

    // $ANTLR start "T__233"
    public final void mT__233() throws RecognitionException {
        try {
            int _type = T__233;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:145:8: ( '{' )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:145:10: '{'
            {
            match('{'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__233"

    // $ANTLR start "T__234"
    public final void mT__234() throws RecognitionException {
        try {
            int _type = T__234;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:146:8: ( '}' )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:146:10: '}'
            {
            match('}'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__234"

    // $ANTLR start "INPUT"
    public final void mINPUT() throws RecognitionException {
        try {
            int _type = INPUT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:198:7: ( ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'P' | 'p' ) ( 'U' | 'u' ) ( 'T' | 't' ) ':' )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:198:9: ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'P' | 'p' ) ( 'U' | 'u' ) ( 'T' | 't' ) ':'
            {
            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            match(':'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "INPUT"

    // $ANTLR start "OUTPUT"
    public final void mOUTPUT() throws RecognitionException {
        try {
            int _type = OUTPUT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:199:8: ( ( 'O' | 'o' ) ( 'U' | 'u' ) ( 'T' | 't' ) ( 'P' | 'p' ) ( 'U' | 'u' ) ( 'T' | 't' ) ':' )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:199:10: ( 'O' | 'o' ) ( 'U' | 'u' ) ( 'T' | 't' ) ( 'P' | 'p' ) ( 'U' | 'u' ) ( 'T' | 't' ) ':'
            {
            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            match(':'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "OUTPUT"

    // $ANTLR start "COLUMNS"
    public final void mCOLUMNS() throws RecognitionException {
        try {
            int _type = COLUMNS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:200:9: ( ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'L' | 'l' ) ( 'U' | 'u' ) ( 'M' | 'm' ) ( 'N' | 'n' ) ( 'S' | 's' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:200:11: ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'L' | 'l' ) ( 'U' | 'u' ) ( 'M' | 'm' ) ( 'N' | 'n' ) ( 'S' | 's' )
            {
            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "COLUMNS"

    // $ANTLR start "BEGIN"
    public final void mBEGIN() throws RecognitionException {
        try {
            int _type = BEGIN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:201:7: ( ( 'B' | 'b' ) ( 'E' | 'e' ) ( 'G' | 'g' ) ( 'I' | 'i' ) ( 'N' | 'n' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:201:9: ( 'B' | 'b' ) ( 'E' | 'e' ) ( 'G' | 'g' ) ( 'I' | 'i' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='B'||input.LA(1)=='b' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "BEGIN"

    // $ANTLR start "END"
    public final void mEND() throws RecognitionException {
        try {
            int _type = END;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:202:5: ( ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'D' | 'd' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:202:7: ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'D' | 'd' )
            {
            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "END"

    // $ANTLR start "SELECT"
    public final void mSELECT() throws RecognitionException {
        try {
            int _type = SELECT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:203:8: ( ( 'S' | 's' ) ( 'E' | 'e' ) ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'C' | 'c' ) ( 'T' | 't' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:203:10: ( 'S' | 's' ) ( 'E' | 'e' ) ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'C' | 'c' ) ( 'T' | 't' )
            {
            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "SELECT"

    // $ANTLR start "WRITERECORD"
    public final void mWRITERECORD() throws RecognitionException {
        try {
            int _type = WRITERECORD;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:206:2: ( ( 'W' | 'w' ) ( 'R' | 'r' ) ( 'I' | 'i' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'R' | 'r' ) ( 'D' | 'd' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:206:4: ( 'W' | 'w' ) ( 'R' | 'r' ) ( 'I' | 'i' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'R' | 'r' ) ( 'D' | 'd' )
            {
            if ( input.LA(1)=='W'||input.LA(1)=='w' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "WRITERECORD"

    // $ANTLR start "UPDATESFDC"
    public final void mUPDATESFDC() throws RecognitionException {
        try {
            int _type = UPDATESFDC;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:208:12: ( ( 'U' | 'u' ) ( 'P' | 'p' ) ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'S' | 's' ) ( 'F' | 'f' ) ( 'D' | 'd' ) ( 'C' | 'c' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:208:14: ( 'U' | 'u' ) ( 'P' | 'p' ) ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'S' | 's' ) ( 'F' | 'f' ) ( 'D' | 'd' ) ( 'C' | 'c' )
            {
            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "UPDATESFDC"

    // $ANTLR start "DIM"
    public final void mDIM() throws RecognitionException {
        try {
            int _type = DIM;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:210:5: ( ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'M' | 'm' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:210:7: ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'M' | 'm' )
            {
            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DIM"

    // $ANTLR start "AS"
    public final void mAS() throws RecognitionException {
        try {
            int _type = AS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:211:4: ( ( 'A' | 'a' ) ( 'S' | 's' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:211:6: ( 'A' | 'a' ) ( 'S' | 's' )
            {
            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "AS"

    // $ANTLR start "COMMA"
    public final void mCOMMA() throws RecognitionException {
        try {
            int _type = COMMA;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:213:7: ( ',' )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:213:9: ','
            {
            match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "COMMA"

    // $ANTLR start "LEFT_PAREN"
    public final void mLEFT_PAREN() throws RecognitionException {
        try {
            int _type = LEFT_PAREN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:214:11: ( '(' )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:214:14: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LEFT_PAREN"

    // $ANTLR start "RIGHT_PAREN"
    public final void mRIGHT_PAREN() throws RecognitionException {
        try {
            int _type = RIGHT_PAREN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:215:12: ( ')' )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:215:15: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "RIGHT_PAREN"

    // $ANTLR start "QUALIFIER"
    public final void mQUALIFIER() throws RecognitionException {
        try {
            int _type = QUALIFIER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:217:2: ( '.' )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:217:4: '.'
            {
            match('.'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "QUALIFIER"

    // $ANTLR start "FROM"
    public final void mFROM() throws RecognitionException {
        try {
            int _type = FROM;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:219:6: ( ( 'F' | 'f' ) ( 'R' | 'r' ) ( 'O' | 'o' ) ( 'M' | 'm' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:219:8: ( 'F' | 'f' ) ( 'R' | 'r' ) ( 'O' | 'o' ) ( 'M' | 'm' )
            {
            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "FROM"

    // $ANTLR start "SUM"
    public final void mSUM() throws RecognitionException {
        try {
            int _type = SUM;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:221:5: ( ( 'S' | 's' ) ( 'U' | 'u' ) ( 'M' | 'm' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:221:7: ( 'S' | 's' ) ( 'U' | 'u' ) ( 'M' | 'm' )
            {
            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "SUM"

    // $ANTLR start "AVG"
    public final void mAVG() throws RecognitionException {
        try {
            int _type = AVG;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:222:5: ( ( 'A' | 'a' ) ( 'V' | 'v' ) ( 'G' | 'g' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:222:7: ( 'A' | 'a' ) ( 'V' | 'v' ) ( 'G' | 'g' )
            {
            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='V'||input.LA(1)=='v' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "AVG"

    // $ANTLR start "COUNT"
    public final void mCOUNT() throws RecognitionException {
        try {
            int _type = COUNT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:223:7: ( ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'U' | 'u' ) ( 'N' | 'n' ) ( 'T' | 't' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:223:9: ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'U' | 'u' ) ( 'N' | 'n' ) ( 'T' | 't' )
            {
            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "COUNT"

    // $ANTLR start "COUNTDISTINCT"
    public final void mCOUNTDISTINCT() throws RecognitionException {
        try {
            int _type = COUNTDISTINCT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:225:2: ( ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'U' | 'u' ) ( 'N' | 'n' ) ( 'T' | 't' ) ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'S' | 's' ) ( 'T' | 't' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'C' | 'c' ) ( 'T' | 't' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:225:4: ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'U' | 'u' ) ( 'N' | 'n' ) ( 'T' | 't' ) ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'S' | 's' ) ( 'T' | 't' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'C' | 'c' ) ( 'T' | 't' )
            {
            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "COUNTDISTINCT"

    // $ANTLR start "MIN"
    public final void mMIN() throws RecognitionException {
        try {
            int _type = MIN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:226:5: ( ( 'M' | 'm' ) ( 'I' | 'i' ) ( 'N' | 'n' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:226:7: ( 'M' | 'm' ) ( 'I' | 'i' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "MIN"

    // $ANTLR start "MAX"
    public final void mMAX() throws RecognitionException {
        try {
            int _type = MAX;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:227:5: ( ( 'M' | 'm' ) ( 'A' | 'a' ) ( 'X' | 'x' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:227:7: ( 'M' | 'm' ) ( 'A' | 'a' ) ( 'X' | 'x' )
            {
            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='X'||input.LA(1)=='x' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "MAX"

    // $ANTLR start "DISTINCT"
    public final void mDISTINCT() throws RecognitionException {
        try {
            int _type = DISTINCT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:228:10: ( ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'S' | 's' ) ( 'T' | 't' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'C' | 'c' ) ( 'T' | 't' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:228:12: ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'S' | 's' ) ( 'T' | 't' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'C' | 'c' ) ( 'T' | 't' )
            {
            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DISTINCT"

    // $ANTLR start "COLUMN_NAME"
    public final void mCOLUMN_NAME() throws RecognitionException {
        try {
            int _type = COLUMN_NAME;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:232:2: ( '[' IDENT '.' IDENT ']' )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:232:4: '[' IDENT '.' IDENT ']'
            {
            match('['); 

            mIDENT(); 


            match('.'); 

            mIDENT(); 


            match(']'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "COLUMN_NAME"

    // $ANTLR start "GRAIN_COLUMN"
    public final void mGRAIN_COLUMN() throws RecognitionException {
        try {
            int _type = GRAIN_COLUMN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:235:2: ( '[' GRAIN '(' IDENT ')' '.' IDENT ']' | '[' MEASURETABLE '(' IDENT ')' '.' IDENT ']' )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0=='[') ) {
                int LA1_1 = input.LA(2);

                if ( (LA1_1=='G'||LA1_1=='g') ) {
                    alt1=1;
                }
                else if ( (LA1_1=='M'||LA1_1=='m') ) {
                    alt1=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 1, 1, input);

                    throw nvae;

                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;

            }
            switch (alt1) {
                case 1 :
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:235:4: '[' GRAIN '(' IDENT ')' '.' IDENT ']'
                    {
                    match('['); 

                    mGRAIN(); 


                    match('('); 

                    mIDENT(); 


                    match(')'); 

                    match('.'); 

                    mIDENT(); 


                    match(']'); 

                    }
                    break;
                case 2 :
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:236:3: '[' MEASURETABLE '(' IDENT ')' '.' IDENT ']'
                    {
                    match('['); 

                    mMEASURETABLE(); 


                    match('('); 

                    mIDENT(); 


                    match(')'); 

                    match('.'); 

                    mIDENT(); 


                    match(']'); 

                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "GRAIN_COLUMN"

    // $ANTLR start "LEVEL_COLUMN"
    public final void mLEVEL_COLUMN() throws RecognitionException {
        try {
            int _type = LEVEL_COLUMN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:239:2: ( '[' LEVEL '(' IDENT '.' IDENT ')' '.' IDENT ']' )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:239:4: '[' LEVEL '(' IDENT '.' IDENT ')' '.' IDENT ']'
            {
            match('['); 

            mLEVEL(); 


            match('('); 

            mIDENT(); 


            match('.'); 

            mIDENT(); 


            match(')'); 

            match('.'); 

            mIDENT(); 


            match(']'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LEVEL_COLUMN"

    // $ANTLR start "OLAPCUBE"
    public final void mOLAPCUBE() throws RecognitionException {
        try {
            int _type = OLAPCUBE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:242:2: ( '[' CUBE ']' )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:242:4: '[' CUBE ']'
            {
            match('['); 

            mCUBE(); 


            match(']'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "OLAPCUBE"

    // $ANTLR start "IDENT_NAME"
    public final void mIDENT_NAME() throws RecognitionException {
        try {
            int _type = IDENT_NAME;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:245:2: ( '[' IDENT ']' )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:245:4: '[' IDENT ']'
            {
            match('['); 

            mIDENT(); 


            match(']'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "IDENT_NAME"

    // $ANTLR start "GRAIN_TABLE"
    public final void mGRAIN_TABLE() throws RecognitionException {
        try {
            int _type = GRAIN_TABLE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:248:2: ( '[' GRAIN '(' IDENT ')' ']' | '[' MEASURETABLE '(' IDENT ')' ']' )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0=='[') ) {
                int LA2_1 = input.LA(2);

                if ( (LA2_1=='G'||LA2_1=='g') ) {
                    alt2=1;
                }
                else if ( (LA2_1=='M'||LA2_1=='m') ) {
                    alt2=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 2, 1, input);

                    throw nvae;

                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;

            }
            switch (alt2) {
                case 1 :
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:248:4: '[' GRAIN '(' IDENT ')' ']'
                    {
                    match('['); 

                    mGRAIN(); 


                    match('('); 

                    mIDENT(); 


                    match(')'); 

                    match(']'); 

                    }
                    break;
                case 2 :
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:249:3: '[' MEASURETABLE '(' IDENT ')' ']'
                    {
                    match('['); 

                    mMEASURETABLE(); 


                    match('('); 

                    mIDENT(); 


                    match(')'); 

                    match(']'); 

                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "GRAIN_TABLE"

    // $ANTLR start "LEVEL_TABLE"
    public final void mLEVEL_TABLE() throws RecognitionException {
        try {
            int _type = LEVEL_TABLE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:252:2: ( '[' LEVEL '(' IDENT '.' IDENT ')' ']' )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:252:4: '[' LEVEL '(' IDENT '.' IDENT ')' ']'
            {
            match('['); 

            mLEVEL(); 


            match('('); 

            mIDENT(); 


            match('.'); 

            mIDENT(); 


            match(')'); 

            match(']'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LEVEL_TABLE"

    // $ANTLR start "POSITION_COLUMN_START"
    public final void mPOSITION_COLUMN_START() throws RecognitionException {
        try {
            int _type = POSITION_COLUMN_START;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:255:2: ( '{' '[' IDENT '.' IDENT ']' )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:255:4: '{' '[' IDENT '.' IDENT ']'
            {
            match('{'); 

            match('['); 

            mIDENT(); 


            match('.'); 

            mIDENT(); 


            match(']'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "POSITION_COLUMN_START"

    // $ANTLR start "LOGICAL_MEMBER_SET_PREFIX"
    public final void mLOGICAL_MEMBER_SET_PREFIX() throws RecognitionException {
        try {
            int _type = LOGICAL_MEMBER_SET_PREFIX;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:258:2: ( '[' IDENT '.' '{' )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:258:4: '[' IDENT '.' '{'
            {
            match('['); 

            mIDENT(); 


            match('.'); 

            match('{'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LOGICAL_MEMBER_SET_PREFIX"

    // $ANTLR start "SPARSE"
    public final void mSPARSE() throws RecognitionException {
        try {
            int _type = SPARSE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:260:8: ( ( 'S' | 's' ) ( 'P' | 'p' ) ( 'A' | 'a' ) ( 'R' | 'r' ) ( 'S' | 's' ) ( 'E' | 'e' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:260:10: ( 'S' | 's' ) ( 'P' | 'p' ) ( 'A' | 'a' ) ( 'R' | 'r' ) ( 'S' | 's' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "SPARSE"

    // $ANTLR start "GRAIN"
    public final void mGRAIN() throws RecognitionException {
        try {
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:262:16: ( ( 'G' | 'g' ) ( 'R' | 'r' ) ( 'A' | 'a' ) ( 'I' | 'i' ) ( 'N' | 'n' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:262:18: ( 'G' | 'g' ) ( 'R' | 'r' ) ( 'A' | 'a' ) ( 'I' | 'i' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }


        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "GRAIN"

    // $ANTLR start "MEASURETABLE"
    public final void mMEASURETABLE() throws RecognitionException {
        try {
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:263:23: ( ( 'M' | 'm' ) ( 'E' | 'e' ) ( 'A' | 'a' ) ( 'S' | 's' ) ( 'U' | 'u' ) ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'A' | 'a' ) ( 'B' | 'b' ) ( 'L' | 'l' ) ( 'E' | 'e' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:264:4: ( 'M' | 'm' ) ( 'E' | 'e' ) ( 'A' | 'a' ) ( 'S' | 's' ) ( 'U' | 'u' ) ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'A' | 'a' ) ( 'B' | 'b' ) ( 'L' | 'l' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='B'||input.LA(1)=='b' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }


        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "MEASURETABLE"

    // $ANTLR start "LEVEL"
    public final void mLEVEL() throws RecognitionException {
        try {
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:265:16: ( ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'V' | 'v' ) ( 'E' | 'e' ) ( 'L' | 'l' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:265:18: ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'V' | 'v' ) ( 'E' | 'e' ) ( 'L' | 'l' )
            {
            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='V'||input.LA(1)=='v' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }


        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LEVEL"

    // $ANTLR start "VARCHARTYPE"
    public final void mVARCHARTYPE() throws RecognitionException {
        try {
            int _type = VARCHARTYPE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:267:13: ( ( 'V' | 'v' ) ( 'A' | 'a' ) ( 'R' | 'r' ) ( 'C' | 'c' ) ( 'H' | 'h' ) ( 'A' | 'a' ) ( 'R' | 'r' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:267:15: ( 'V' | 'v' ) ( 'A' | 'a' ) ( 'R' | 'r' ) ( 'C' | 'c' ) ( 'H' | 'h' ) ( 'A' | 'a' ) ( 'R' | 'r' )
            {
            if ( input.LA(1)=='V'||input.LA(1)=='v' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='H'||input.LA(1)=='h' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "VARCHARTYPE"

    // $ANTLR start "INTEGERTYPE"
    public final void mINTEGERTYPE() throws RecognitionException {
        try {
            int _type = INTEGERTYPE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:268:13: ( ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'G' | 'g' ) ( 'E' | 'e' ) ( 'R' | 'r' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:268:15: ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'G' | 'g' ) ( 'E' | 'e' ) ( 'R' | 'r' )
            {
            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "INTEGERTYPE"

    // $ANTLR start "FLOATTYPE"
    public final void mFLOATTYPE() throws RecognitionException {
        try {
            int _type = FLOATTYPE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:269:11: ( ( 'F' | 'f' ) ( 'L' | 'l' ) ( 'O' | 'o' ) ( 'A' | 'a' ) ( 'T' | 't' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:269:13: ( 'F' | 'f' ) ( 'L' | 'l' ) ( 'O' | 'o' ) ( 'A' | 'a' ) ( 'T' | 't' )
            {
            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "FLOATTYPE"

    // $ANTLR start "NUMBERTYPE"
    public final void mNUMBERTYPE() throws RecognitionException {
        try {
            int _type = NUMBERTYPE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:270:12: ( ( 'N' | 'n' ) ( 'U' | 'u' ) ( 'M' | 'm' ) ( 'B' | 'b' ) ( 'E' | 'e' ) ( 'R' | 'r' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:270:14: ( 'N' | 'n' ) ( 'U' | 'u' ) ( 'M' | 'm' ) ( 'B' | 'b' ) ( 'E' | 'e' ) ( 'R' | 'r' )
            {
            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='B'||input.LA(1)=='b' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "NUMBERTYPE"

    // $ANTLR start "DATETYPE"
    public final void mDATETYPE() throws RecognitionException {
        try {
            int _type = DATETYPE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:271:10: ( ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:271:12: ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DATETYPE"

    // $ANTLR start "DATETIMETYPE"
    public final void mDATETIMETYPE() throws RecognitionException {
        try {
            int _type = DATETIMETYPE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:272:14: ( ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'I' | 'i' ) ( 'M' | 'm' ) ( 'E' | 'e' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:272:16: ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'I' | 'i' ) ( 'M' | 'm' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DATETIMETYPE"

    // $ANTLR start "NONETYPE"
    public final void mNONETYPE() throws RecognitionException {
        try {
            int _type = NONETYPE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:273:10: ( ( 'N' | 'n' ) ( 'O' | 'o' ) ( 'N' | 'n' ) ( 'E' | 'e' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:273:12: ( 'N' | 'n' ) ( 'O' | 'o' ) ( 'N' | 'n' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "NONETYPE"

    // $ANTLR start "STRING"
    public final void mSTRING() throws RecognitionException {
        try {
            int _type = STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            int normal;

            StringBuilder lBuf = new StringBuilder();
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:276:6: ( '\\'' ( EscapeSequence | ( options {greedy=false; } :normal=~ ( '\\u0000' .. '\\u001f' | '\\\\' | '\\'' ) ) )* '\\'' )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:276:10: '\\'' ( EscapeSequence | ( options {greedy=false; } :normal=~ ( '\\u0000' .. '\\u001f' | '\\\\' | '\\'' ) ) )* '\\''
            {
            match('\''); 

            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:276:15: ( EscapeSequence | ( options {greedy=false; } :normal=~ ( '\\u0000' .. '\\u001f' | '\\\\' | '\\'' ) ) )*
            loop3:
            do {
                int alt3=3;
                int LA3_0 = input.LA(1);

                if ( (LA3_0=='\\') ) {
                    alt3=1;
                }
                else if ( ((LA3_0 >= ' ' && LA3_0 <= '&')||(LA3_0 >= '(' && LA3_0 <= '[')||(LA3_0 >= ']' && LA3_0 <= '\uFFFF')) ) {
                    alt3=2;
                }


                switch (alt3) {
            	case 1 :
            	    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:277:6: EscapeSequence
            	    {
            	    mEscapeSequence(); 


            	    lBuf.append(getText());

            	    }
            	    break;
            	case 2 :
            	    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:278:6: ( options {greedy=false; } :normal=~ ( '\\u0000' .. '\\u001f' | '\\\\' | '\\'' ) )
            	    {
            	    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:278:6: ( options {greedy=false; } :normal=~ ( '\\u0000' .. '\\u001f' | '\\\\' | '\\'' ) )
            	    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:278:33: normal=~ ( '\\u0000' .. '\\u001f' | '\\\\' | '\\'' )
            	    {
            	    normal= input.LA(1);

            	    if ( (input.LA(1) >= ' ' && input.LA(1) <= '&')||(input.LA(1) >= '(' && input.LA(1) <= '[')||(input.LA(1) >= ']' && input.LA(1) <= '\uFFFF') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    lBuf.appendCodePoint(normal);

            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            match('\''); 

            setText(lBuf.insert(0,'\'').append('\'').toString());

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "STRING"

    // $ANTLR start "INTEGER"
    public final void mINTEGER() throws RecognitionException {
        try {
            int _type = INTEGER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:283:2: ( ( '0' .. '9' )+ )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:283:4: ( '0' .. '9' )+
            {
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:283:4: ( '0' .. '9' )+
            int cnt4=0;
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( ((LA4_0 >= '0' && LA4_0 <= '9')) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:
            	    {
            	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt4 >= 1 ) break loop4;
                        EarlyExitException eee =
                            new EarlyExitException(4, input);
                        throw eee;
                }
                cnt4++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "INTEGER"

    // $ANTLR start "FLOAT"
    public final void mFLOAT() throws RecognitionException {
        try {
            int _type = FLOAT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:285:2: ( ( '0' .. '9' )* '.' ( '0' .. '9' )+ )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:285:4: ( '0' .. '9' )* '.' ( '0' .. '9' )+
            {
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:285:4: ( '0' .. '9' )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( ((LA5_0 >= '0' && LA5_0 <= '9')) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:
            	    {
            	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            match('.'); 

            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:285:20: ( '0' .. '9' )+
            int cnt6=0;
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( ((LA6_0 >= '0' && LA6_0 <= '9')) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:
            	    {
            	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt6 >= 1 ) break loop6;
                        EarlyExitException eee =
                            new EarlyExitException(6, input);
                        throw eee;
                }
                cnt6++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "FLOAT"

    // $ANTLR start "DATETIME"
    public final void mDATETIME() throws RecognitionException {
        try {
            int _type = DATETIME;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:287:3: ( '#' (~ '#' )* '#' | ( 'N' | 'n' ) ( 'O' | 'o' ) ( 'W' | 'w' ) | ( 'N' | 'n' ) ( 'O' | 'o' ) ( 'W' | 'w' ) ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' ) | ( 'N' | 'n' ) ( 'O' | 'o' ) ( 'W' | 'w' ) ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'I' | 'i' ) ( 'M' | 'm' ) ( 'E' | 'e' ) )
            int alt8=4;
            int LA8_0 = input.LA(1);

            if ( (LA8_0=='#') ) {
                alt8=1;
            }
            else if ( (LA8_0=='N'||LA8_0=='n') ) {
                int LA8_2 = input.LA(2);

                if ( (LA8_2=='O'||LA8_2=='o') ) {
                    int LA8_3 = input.LA(3);

                    if ( (LA8_3=='W'||LA8_3=='w') ) {
                        int LA8_4 = input.LA(4);

                        if ( (LA8_4=='D'||LA8_4=='d') ) {
                            int LA8_5 = input.LA(5);

                            if ( (LA8_5=='A'||LA8_5=='a') ) {
                                int LA8_7 = input.LA(6);

                                if ( (LA8_7=='T'||LA8_7=='t') ) {
                                    int LA8_8 = input.LA(7);

                                    if ( (LA8_8=='E'||LA8_8=='e') ) {
                                        int LA8_9 = input.LA(8);

                                        if ( (LA8_9=='T'||LA8_9=='t') ) {
                                            alt8=4;
                                        }
                                        else {
                                            alt8=3;
                                        }
                                    }
                                    else {
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 8, 8, input);

                                        throw nvae;

                                    }
                                }
                                else {
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 8, 7, input);

                                    throw nvae;

                                }
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 8, 5, input);

                                throw nvae;

                            }
                        }
                        else {
                            alt8=2;
                        }
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 8, 3, input);

                        throw nvae;

                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 8, 2, input);

                    throw nvae;

                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;

            }
            switch (alt8) {
                case 1 :
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:287:5: '#' (~ '#' )* '#'
                    {
                    match('#'); 

                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:287:9: (~ '#' )*
                    loop7:
                    do {
                        int alt7=2;
                        int LA7_0 = input.LA(1);

                        if ( ((LA7_0 >= '\u0000' && LA7_0 <= '\"')||(LA7_0 >= '$' && LA7_0 <= '\uFFFF')) ) {
                            alt7=1;
                        }


                        switch (alt7) {
                    	case 1 :
                    	    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:
                    	    {
                    	    if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\"')||(input.LA(1) >= '$' && input.LA(1) <= '\uFFFF') ) {
                    	        input.consume();
                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;
                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop7;
                        }
                    } while (true);


                    match('#'); 

                    }
                    break;
                case 2 :
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:287:23: ( 'N' | 'n' ) ( 'O' | 'o' ) ( 'W' | 'w' )
                    {
                    if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='W'||input.LA(1)=='w' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;
                case 3 :
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:288:6: ( 'N' | 'n' ) ( 'O' | 'o' ) ( 'W' | 'w' ) ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' )
                    {
                    if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='W'||input.LA(1)=='w' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;
                case 4 :
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:289:6: ( 'N' | 'n' ) ( 'O' | 'o' ) ( 'W' | 'w' ) ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'I' | 'i' ) ( 'M' | 'm' ) ( 'E' | 'e' )
                    {
                    if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='W'||input.LA(1)=='w' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DATETIME"

    // $ANTLR start "BOOLEAN"
    public final void mBOOLEAN() throws RecognitionException {
        try {
            int _type = BOOLEAN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:291:2: ( ( ( 'T' | 't' ) ( 'R' | 'r' ) ( 'U' | 'u' ) ( 'E' | 'e' ) ) | ( ( 'F' | 'f' ) ( 'A' | 'a' ) ( 'L' | 'l' ) ( 'S' | 's' ) ( 'E' | 'e' ) ) )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0=='T'||LA9_0=='t') ) {
                alt9=1;
            }
            else if ( (LA9_0=='F'||LA9_0=='f') ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;

            }
            switch (alt9) {
                case 1 :
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:291:4: ( ( 'T' | 't' ) ( 'R' | 'r' ) ( 'U' | 'u' ) ( 'E' | 'e' ) )
                    {
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:291:4: ( ( 'T' | 't' ) ( 'R' | 'r' ) ( 'U' | 'u' ) ( 'E' | 'e' ) )
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:291:5: ( 'T' | 't' ) ( 'R' | 'r' ) ( 'U' | 'u' ) ( 'E' | 'e' )
                    {
                    if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }


                    }
                    break;
                case 2 :
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:292:4: ( ( 'F' | 'f' ) ( 'A' | 'a' ) ( 'L' | 'l' ) ( 'S' | 's' ) ( 'E' | 'e' ) )
                    {
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:292:4: ( ( 'F' | 'f' ) ( 'A' | 'a' ) ( 'L' | 'l' ) ( 'S' | 's' ) ( 'E' | 'e' ) )
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:292:5: ( 'F' | 'f' ) ( 'A' | 'a' ) ( 'L' | 'l' ) ( 'S' | 's' ) ( 'E' | 'e' )
                    {
                    if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }


                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "BOOLEAN"

    // $ANTLR start "NULL"
    public final void mNULL() throws RecognitionException {
        try {
            int _type = NULL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:293:6: ( ( 'N' | 'n' ) ( 'U' | 'u' ) ( 'L' | 'l' ) ( 'L' | 'l' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:293:8: ( 'N' | 'n' ) ( 'U' | 'u' ) ( 'L' | 'l' ) ( 'L' | 'l' )
            {
            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "NULL"

    // $ANTLR start "PARAMETER"
    public final void mPARAMETER() throws RecognitionException {
        try {
            int _type = PARAMETER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:295:2: ( '%' INTEGER )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:295:4: '%' INTEGER
            {
            match('%'); 

            mINTEGER(); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "PARAMETER"

    // $ANTLR start "IDENT"
    public final void mIDENT() throws RecognitionException {
        try {
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:298:2: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' | ' ' | ':' | '$' | '#' | '%' | '-' | '/' | '&' | '!' | '^' | '@' | ',' | ';' | '>' | '<' | '0' .. '9' | '\\u0080' .. '\\uFFFF' )+ )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:298:4: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | ' ' | ':' | '$' | '#' | '%' | '-' | '/' | '&' | '!' | '^' | '@' | ',' | ';' | '>' | '<' | '0' .. '9' | '\\u0080' .. '\\uFFFF' )+
            {
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:298:4: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | ' ' | ':' | '$' | '#' | '%' | '-' | '/' | '&' | '!' | '^' | '@' | ',' | ';' | '>' | '<' | '0' .. '9' | '\\u0080' .. '\\uFFFF' )+
            int cnt10=0;
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( ((LA10_0 >= ' ' && LA10_0 <= '!')||(LA10_0 >= '#' && LA10_0 <= '&')||(LA10_0 >= ',' && LA10_0 <= '-')||(LA10_0 >= '/' && LA10_0 <= '<')||LA10_0=='>'||(LA10_0 >= '@' && LA10_0 <= 'Z')||(LA10_0 >= '^' && LA10_0 <= '_')||(LA10_0 >= 'a' && LA10_0 <= 'z')||(LA10_0 >= '\u0080' && LA10_0 <= '\uFFFF')) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:
            	    {
            	    if ( (input.LA(1) >= ' ' && input.LA(1) <= '!')||(input.LA(1) >= '#' && input.LA(1) <= '&')||(input.LA(1) >= ',' && input.LA(1) <= '-')||(input.LA(1) >= '/' && input.LA(1) <= '<')||input.LA(1)=='>'||(input.LA(1) >= '@' && input.LA(1) <= 'Z')||(input.LA(1) >= '^' && input.LA(1) <= '_')||(input.LA(1) >= 'a' && input.LA(1) <= 'z')||(input.LA(1) >= '\u0080' && input.LA(1) <= '\uFFFF') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt10 >= 1 ) break loop10;
                        EarlyExitException eee =
                            new EarlyExitException(10, input);
                        throw eee;
                }
                cnt10++;
            } while (true);


            }


        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "IDENT"

    // $ANTLR start "EscapeSequence"
    public final void mEscapeSequence() throws RecognitionException {
        try {
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:301:2: ( '\\\\' ( 'n' | 'r' | 't' | '\\'' | '\\\\' | UnicodeEscape ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:301:4: '\\\\' ( 'n' | 'r' | 't' | '\\'' | '\\\\' | UnicodeEscape )
            {
            match('\\'); 

            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:302:4: ( 'n' | 'r' | 't' | '\\'' | '\\\\' | UnicodeEscape )
            int alt11=6;
            switch ( input.LA(1) ) {
            case 'n':
                {
                alt11=1;
                }
                break;
            case 'r':
                {
                alt11=2;
                }
                break;
            case 't':
                {
                alt11=3;
                }
                break;
            case '\'':
                {
                alt11=4;
                }
                break;
            case '\\':
                {
                alt11=5;
                }
                break;
            case 'u':
                {
                alt11=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;

            }

            switch (alt11) {
                case 1 :
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:303:5: 'n'
                    {
                    match('n'); 

                    setText("\n");

                    }
                    break;
                case 2 :
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:304:4: 'r'
                    {
                    match('r'); 

                    setText("\r");

                    }
                    break;
                case 3 :
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:305:4: 't'
                    {
                    match('t'); 

                    setText("\t");

                    }
                    break;
                case 4 :
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:306:4: '\\''
                    {
                    match('\''); 

                    setText("\'");

                    }
                    break;
                case 5 :
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:307:4: '\\\\'
                    {
                    match('\\'); 

                    setText("\\");

                    }
                    break;
                case 6 :
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:308:4: UnicodeEscape
                    {
                    mUnicodeEscape(); 


                    }
                    break;

            }


            }


        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "EscapeSequence"

    // $ANTLR start "UnicodeEscape"
    public final void mUnicodeEscape() throws RecognitionException {
        try {
            CommonToken i=null;
            CommonToken j=null;
            CommonToken k=null;
            CommonToken l=null;

            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:312:6: ( 'u' i= HexDigit j= HexDigit k= HexDigit l= HexDigit )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:312:12: 'u' i= HexDigit j= HexDigit k= HexDigit l= HexDigit
            {
            match('u'); 

            int iStart2030 = getCharIndex();
            int iStartLine2030 = getLine();
            int iStartCharPos2030 = getCharPositionInLine();
            mHexDigit(); 
            i = new CommonToken(input, Token.INVALID_TOKEN_TYPE, Token.DEFAULT_CHANNEL, iStart2030, getCharIndex()-1);
            i.setLine(iStartLine2030);
            i.setCharPositionInLine(iStartCharPos2030);


            int jStart2034 = getCharIndex();
            int jStartLine2034 = getLine();
            int jStartCharPos2034 = getCharPositionInLine();
            mHexDigit(); 
            j = new CommonToken(input, Token.INVALID_TOKEN_TYPE, Token.DEFAULT_CHANNEL, jStart2034, getCharIndex()-1);
            j.setLine(jStartLine2034);
            j.setCharPositionInLine(jStartCharPos2034);


            int kStart2038 = getCharIndex();
            int kStartLine2038 = getLine();
            int kStartCharPos2038 = getCharPositionInLine();
            mHexDigit(); 
            k = new CommonToken(input, Token.INVALID_TOKEN_TYPE, Token.DEFAULT_CHANNEL, kStart2038, getCharIndex()-1);
            k.setLine(kStartLine2038);
            k.setCharPositionInLine(kStartCharPos2038);


            int lStart2042 = getCharIndex();
            int lStartLine2042 = getLine();
            int lStartCharPos2042 = getCharPositionInLine();
            mHexDigit(); 
            l = new CommonToken(input, Token.INVALID_TOKEN_TYPE, Token.DEFAULT_CHANNEL, lStart2042, getCharIndex()-1);
            l.setLine(lStartLine2042);
            l.setCharPositionInLine(lStartCharPos2042);


            setText(Character.toString((char)Integer.parseInt(i.getText()+j.getText()+k.getText()+l.getText(),16)));

            }


        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "UnicodeEscape"

    // $ANTLR start "HexDigit"
    public final void mHexDigit() throws RecognitionException {
        try {
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:316:2: ( ( '0' .. '9' | 'a' .. 'f' | 'A' .. 'F' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:
            {
            if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'F')||(input.LA(1) >= 'a' && input.LA(1) <= 'f') ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }


        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "HexDigit"

    // $ANTLR start "WS"
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:320:2: ( ( ' ' | '\\r' | '\\t' | '\\u000C' | '\\n' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:320:5: ( ' ' | '\\r' | '\\t' | '\\u000C' | '\\n' )
            {
            if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||(input.LA(1) >= '\f' && input.LA(1) <= '\r')||input.LA(1)==' ' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            _channel=HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "WS"

    // $ANTLR start "PLUS"
    public final void mPLUS() throws RecognitionException {
        try {
            int _type = PLUS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:322:6: ( '+' )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:322:8: '+'
            {
            match('+'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "PLUS"

    // $ANTLR start "MINUS"
    public final void mMINUS() throws RecognitionException {
        try {
            int _type = MINUS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:323:7: ( '-' )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:323:9: '-'
            {
            match('-'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "MINUS"

    // $ANTLR start "MULT"
    public final void mMULT() throws RecognitionException {
        try {
            int _type = MULT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:324:6: ( '*' )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:324:8: '*'
            {
            match('*'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "MULT"

    // $ANTLR start "DIV"
    public final void mDIV() throws RecognitionException {
        try {
            int _type = DIV;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:325:5: ( '/' )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:325:7: '/'
            {
            match('/'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DIV"

    // $ANTLR start "MOD"
    public final void mMOD() throws RecognitionException {
        try {
            int _type = MOD;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:326:5: ( '%' )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:326:7: '%'
            {
            match('%'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "MOD"

    // $ANTLR start "POW"
    public final void mPOW() throws RecognitionException {
        try {
            int _type = POW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:327:5: ( '^' )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:327:7: '^'
            {
            match('^'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "POW"

    // $ANTLR start "LT"
    public final void mLT() throws RecognitionException {
        try {
            int _type = LT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:328:4: ( '<' )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:328:6: '<'
            {
            match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LT"

    // $ANTLR start "LTEQ"
    public final void mLTEQ() throws RecognitionException {
        try {
            int _type = LTEQ;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:329:6: ( '<=' )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:329:8: '<='
            {
            match("<="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LTEQ"

    // $ANTLR start "GT"
    public final void mGT() throws RecognitionException {
        try {
            int _type = GT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:330:4: ( '>' )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:330:6: '>'
            {
            match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "GT"

    // $ANTLR start "GTEQ"
    public final void mGTEQ() throws RecognitionException {
        try {
            int _type = GTEQ;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:331:6: ( '>=' )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:331:8: '>='
            {
            match(">="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "GTEQ"

    // $ANTLR start "EQUALS"
    public final void mEQUALS() throws RecognitionException {
        try {
            int _type = EQUALS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:333:2: ( '=' )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:333:4: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "EQUALS"

    // $ANTLR start "NOTEQUALS"
    public final void mNOTEQUALS() throws RecognitionException {
        try {
            int _type = NOTEQUALS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:335:2: ( '!=' | '<>' )
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0=='!') ) {
                alt12=1;
            }
            else if ( (LA12_0=='<') ) {
                alt12=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;

            }
            switch (alt12) {
                case 1 :
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:335:4: '!='
                    {
                    match("!="); 



                    }
                    break;
                case 2 :
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:335:11: '<>'
                    {
                    match("<>"); 



                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "NOTEQUALS"

    // $ANTLR start "BITAND"
    public final void mBITAND() throws RecognitionException {
        try {
            int _type = BITAND;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:336:7: ( '&' )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:336:9: '&'
            {
            match('&'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "BITAND"

    // $ANTLR start "BITOR"
    public final void mBITOR() throws RecognitionException {
        try {
            int _type = BITOR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:337:7: ( '|' )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:337:9: '|'
            {
            match('|'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "BITOR"

    // $ANTLR start "IN"
    public final void mIN() throws RecognitionException {
        try {
            int _type = IN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:339:3: ( ( 'I' | 'i' ) ( 'N' | 'n' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:340:2: ( 'I' | 'i' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "IN"

    // $ANTLR start "NOTIN"
    public final void mNOTIN() throws RecognitionException {
        try {
            int _type = NOTIN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:341:6: ( ( 'N' | 'n' ) ( 'O' | 'o' ) ( 'T' | 't' ) WS ( 'I' | 'i' ) ( 'N' | 'n' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:342:2: ( 'N' | 'n' ) ( 'O' | 'o' ) ( 'T' | 't' ) WS ( 'I' | 'i' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "NOTIN"

    // $ANTLR start "LIKE"
    public final void mLIKE() throws RecognitionException {
        try {
            int _type = LIKE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:343:5: ( ( 'L' | 'l' ) ( 'I' | 'i' ) ( 'K' | 'k' ) ( 'E' | 'e' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:344:2: ( 'L' | 'l' ) ( 'I' | 'i' ) ( 'K' | 'k' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='K'||input.LA(1)=='k' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LIKE"

    // $ANTLR start "NOTLIKE"
    public final void mNOTLIKE() throws RecognitionException {
        try {
            int _type = NOTLIKE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:345:8: ( ( 'N' | 'n' ) ( 'O' | 'o' ) ( 'T' | 't' ) WS ( 'L' | 'l' ) ( 'I' | 'i' ) ( 'K' | 'k' ) ( 'E' | 'e' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:346:2: ( 'N' | 'n' ) ( 'O' | 'o' ) ( 'T' | 't' ) WS ( 'L' | 'l' ) ( 'I' | 'i' ) ( 'K' | 'k' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='K'||input.LA(1)=='k' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "NOTLIKE"

    // $ANTLR start "PLUSEQUALS"
    public final void mPLUSEQUALS() throws RecognitionException {
        try {
            int _type = PLUSEQUALS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:349:2: ( '+=' )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:349:4: '+='
            {
            match("+="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "PLUSEQUALS"

    // $ANTLR start "MINUSEQUALS"
    public final void mMINUSEQUALS() throws RecognitionException {
        try {
            int _type = MINUSEQUALS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:351:2: ( '-=' )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:351:4: '-='
            {
            match("-="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "MINUSEQUALS"

    // $ANTLR start "NOT"
    public final void mNOT() throws RecognitionException {
        try {
            int _type = NOT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:352:5: ( '!' | ( 'N' | 'n' ) ( 'O' | 'o' ) ( 'T' | 't' ) )
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0=='!') ) {
                alt13=1;
            }
            else if ( (LA13_0=='N'||LA13_0=='n') ) {
                alt13=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;

            }
            switch (alt13) {
                case 1 :
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:352:7: '!'
                    {
                    match('!'); 

                    }
                    break;
                case 2 :
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:352:13: ( 'N' | 'n' ) ( 'O' | 'o' ) ( 'T' | 't' )
                    {
                    if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "NOT"

    // $ANTLR start "OR"
    public final void mOR() throws RecognitionException {
        try {
            int _type = OR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:353:5: ( '||' | ( 'O' | 'o' ) ( 'R' | 'r' ) )
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0=='|') ) {
                alt14=1;
            }
            else if ( (LA14_0=='O'||LA14_0=='o') ) {
                alt14=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;

            }
            switch (alt14) {
                case 1 :
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:353:8: '||'
                    {
                    match("||"); 



                    }
                    break;
                case 2 :
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:353:15: ( 'O' | 'o' ) ( 'R' | 'r' )
                    {
                    if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "OR"

    // $ANTLR start "AND"
    public final void mAND() throws RecognitionException {
        try {
            int _type = AND;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:354:6: ( '&&' | ( 'A' | 'a' ) ( 'N' | 'n' ) ( 'D' | 'd' ) )
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0=='&') ) {
                alt15=1;
            }
            else if ( (LA15_0=='A'||LA15_0=='a') ) {
                alt15=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 15, 0, input);

                throw nvae;

            }
            switch (alt15) {
                case 1 :
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:354:9: '&&'
                    {
                    match("&&"); 



                    }
                    break;
                case 2 :
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:354:16: ( 'A' | 'a' ) ( 'N' | 'n' ) ( 'D' | 'd' )
                    {
                    if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "AND"

    // $ANTLR start "ISNULL"
    public final void mISNULL() throws RecognitionException {
        try {
            int _type = ISNULL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:356:7: ( ( 'I' | 'i' ) ( 'S' | 's' ) WS ( 'N' | 'n' ) ( 'U' | 'u' ) ( 'L' | 'l' ) ( 'L' | 'l' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:357:2: ( 'I' | 'i' ) ( 'S' | 's' ) WS ( 'N' | 'n' ) ( 'U' | 'u' ) ( 'L' | 'l' ) ( 'L' | 'l' )
            {
            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ISNULL"

    // $ANTLR start "ISNOTNULL"
    public final void mISNOTNULL() throws RecognitionException {
        try {
            int _type = ISNOTNULL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:359:2: ( ( 'I' | 'i' ) ( 'S' | 's' ) WS ( 'N' | 'n' ) ( 'O' | 'o' ) ( 'T' | 't' ) WS ( 'N' | 'n' ) ( 'U' | 'u' ) ( 'L' | 'l' ) ( 'L' | 'l' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:359:4: ( 'I' | 'i' ) ( 'S' | 's' ) WS ( 'N' | 'n' ) ( 'O' | 'o' ) ( 'T' | 't' ) WS ( 'N' | 'n' ) ( 'U' | 'u' ) ( 'L' | 'l' ) ( 'L' | 'l' )
            {
            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ISNOTNULL"

    // $ANTLR start "INNERJOIN"
    public final void mINNERJOIN() throws RecognitionException {
        try {
            int _type = INNERJOIN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:362:2: ( ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'N' | 'n' ) ( 'E' | 'e' ) ( 'R' | 'r' ) WS ( 'J' | 'j' ) ( 'O' | 'o' ) ( 'I' | 'i' ) ( 'N' | 'n' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:362:4: ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'N' | 'n' ) ( 'E' | 'e' ) ( 'R' | 'r' ) WS ( 'J' | 'j' ) ( 'O' | 'o' ) ( 'I' | 'i' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='J'||input.LA(1)=='j' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "INNERJOIN"

    // $ANTLR start "FULLOUTERJOIN"
    public final void mFULLOUTERJOIN() throws RecognitionException {
        try {
            int _type = FULLOUTERJOIN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:364:2: ( ( 'F' | 'f' ) ( 'U' | 'u' ) ( 'L' | 'l' ) ( 'L' | 'l' ) WS ( 'O' | 'o' ) ( 'U' | 'u' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'R' | 'r' ) WS ( 'J' | 'j' ) ( 'O' | 'o' ) ( 'I' | 'i' ) ( 'N' | 'n' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:364:4: ( 'F' | 'f' ) ( 'U' | 'u' ) ( 'L' | 'l' ) ( 'L' | 'l' ) WS ( 'O' | 'o' ) ( 'U' | 'u' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'R' | 'r' ) WS ( 'J' | 'j' ) ( 'O' | 'o' ) ( 'I' | 'i' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='J'||input.LA(1)=='j' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "FULLOUTERJOIN"

    // $ANTLR start "LEFTOUTERJOIN"
    public final void mLEFTOUTERJOIN() throws RecognitionException {
        try {
            int _type = LEFTOUTERJOIN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:366:2: ( ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'F' | 'f' ) ( 'T' | 't' ) WS ( 'O' | 'o' ) ( 'U' | 'u' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'R' | 'r' ) WS ( 'J' | 'j' ) ( 'O' | 'o' ) ( 'I' | 'i' ) ( 'N' | 'n' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:366:4: ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'F' | 'f' ) ( 'T' | 't' ) WS ( 'O' | 'o' ) ( 'U' | 'u' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'R' | 'r' ) WS ( 'J' | 'j' ) ( 'O' | 'o' ) ( 'I' | 'i' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='J'||input.LA(1)=='j' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LEFTOUTERJOIN"

    // $ANTLR start "RIGHTOUTERJOIN"
    public final void mRIGHTOUTERJOIN() throws RecognitionException {
        try {
            int _type = RIGHTOUTERJOIN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:368:2: ( ( 'R' | 'r' ) ( 'I' | 'i' ) ( 'G' | 'g' ) ( 'H' | 'h' ) ( 'T' | 't' ) WS ( 'O' | 'o' ) ( 'U' | 'u' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'R' | 'r' ) WS ( 'J' | 'j' ) ( 'O' | 'o' ) ( 'I' | 'i' ) ( 'N' | 'n' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:368:4: ( 'R' | 'r' ) ( 'I' | 'i' ) ( 'G' | 'g' ) ( 'H' | 'h' ) ( 'T' | 't' ) WS ( 'O' | 'o' ) ( 'U' | 'u' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'R' | 'r' ) WS ( 'J' | 'j' ) ( 'O' | 'o' ) ( 'I' | 'i' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='H'||input.LA(1)=='h' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='J'||input.LA(1)=='j' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "RIGHTOUTERJOIN"

    // $ANTLR start "USINGOUTERJOIN"
    public final void mUSINGOUTERJOIN() throws RecognitionException {
        try {
            int _type = USINGOUTERJOIN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:370:2: ( ( 'U' | 'u' ) ( 'S' | 's' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'G' | 'g' ) WS ( 'O' | 'o' ) ( 'U' | 'u' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'R' | 'r' ) WS ( 'J' | 'j' ) ( 'O' | 'o' ) ( 'I' | 'i' ) ( 'N' | 'n' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:370:4: ( 'U' | 'u' ) ( 'S' | 's' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'G' | 'g' ) WS ( 'O' | 'o' ) ( 'U' | 'u' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'R' | 'r' ) WS ( 'J' | 'j' ) ( 'O' | 'o' ) ( 'I' | 'i' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='J'||input.LA(1)=='j' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "USINGOUTERJOIN"

    // $ANTLR start "UNION"
    public final void mUNION() throws RecognitionException {
        try {
            int _type = UNION;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:372:7: ( ( 'U' | 'u' ) ( 'N' | 'n' ) ( 'I' | 'i' ) ( 'O' | 'o' ) ( 'N' | 'n' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:372:9: ( 'U' | 'u' ) ( 'N' | 'n' ) ( 'I' | 'i' ) ( 'O' | 'o' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "UNION"

    // $ANTLR start "UNIONALL"
    public final void mUNIONALL() throws RecognitionException {
        try {
            int _type = UNIONALL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:374:2: ( ( 'U' | 'u' ) ( 'N' | 'n' ) ( 'I' | 'i' ) ( 'O' | 'o' ) ( 'N' | 'n' ) ( 'A' | 'a' ) ( 'L' | 'l' ) ( 'L' | 'l' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:374:4: ( 'U' | 'u' ) ( 'N' | 'n' ) ( 'I' | 'i' ) ( 'O' | 'o' ) ( 'N' | 'n' ) ( 'A' | 'a' ) ( 'L' | 'l' ) ( 'L' | 'l' )
            {
            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "UNIONALL"

    // $ANTLR start "ON"
    public final void mON() throws RecognitionException {
        try {
            int _type = ON;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:376:4: ( ( 'O' | 'o' ) ( 'N' | 'n' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:376:6: ( 'O' | 'o' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ON"

    // $ANTLR start "WHERE"
    public final void mWHERE() throws RecognitionException {
        try {
            int _type = WHERE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:377:7: ( ( 'W' | 'w' ) ( 'H' | 'h' ) ( 'E' | 'e' ) ( 'R' | 'r' ) ( 'E' | 'e' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:377:9: ( 'W' | 'w' ) ( 'H' | 'h' ) ( 'E' | 'e' ) ( 'R' | 'r' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='W'||input.LA(1)=='w' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='H'||input.LA(1)=='h' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "WHERE"

    // $ANTLR start "ORDERBY"
    public final void mORDERBY() throws RecognitionException {
        try {
            int _type = ORDERBY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:378:9: ( ( 'O' | 'o' ) ( 'R' | 'r' ) ( 'D' | 'd' ) ( 'E' | 'e' ) ( 'R' | 'r' ) WS ( 'B' | 'b' ) ( 'Y' | 'y' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:378:11: ( 'O' | 'o' ) ( 'R' | 'r' ) ( 'D' | 'd' ) ( 'E' | 'e' ) ( 'R' | 'r' ) WS ( 'B' | 'b' ) ( 'Y' | 'y' )
            {
            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='B'||input.LA(1)=='b' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='Y'||input.LA(1)=='y' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ORDERBY"

    // $ANTLR start "ASCENDING"
    public final void mASCENDING() throws RecognitionException {
        try {
            int _type = ASCENDING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:380:2: ( ( 'A' | 'a' ) ( 'S' | 's' ) ( 'C' | 'c' ) ( ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'G' | 'g' ) )? )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:380:4: ( 'A' | 'a' ) ( 'S' | 's' ) ( 'C' | 'c' ) ( ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'G' | 'g' ) )?
            {
            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:380:34: ( ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'G' | 'g' ) )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0=='E'||LA16_0=='e') ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:380:35: ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'G' | 'g' )
                    {
                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ASCENDING"

    // $ANTLR start "DESCENDING"
    public final void mDESCENDING() throws RecognitionException {
        try {
            int _type = DESCENDING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:382:2: ( ( 'D' | 'd' ) ( 'E' | 'e' ) ( 'S' | 's' ) ( 'C' | 'c' ) ( ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'G' | 'g' ) )? )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:382:4: ( 'D' | 'd' ) ( 'E' | 'e' ) ( 'S' | 's' ) ( 'C' | 'c' ) ( ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'G' | 'g' ) )?
            {
            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:382:44: ( ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'G' | 'g' ) )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0=='E'||LA17_0=='e') ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:382:45: ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'G' | 'g' )
                    {
                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DESCENDING"

    // $ANTLR start "PARTITION"
    public final void mPARTITION() throws RecognitionException {
        try {
            int _type = PARTITION;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:384:2: ( ( 'P' | 'p' ) ( 'A' | 'a' ) ( 'R' | 'r' ) ( 'T' | 't' ) ( 'I' | 'i' ) ( 'T' | 't' ) ( 'I' | 'i' ) ( 'O' | 'o' ) ( 'N' | 'n' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:384:4: ( 'P' | 'p' ) ( 'A' | 'a' ) ( 'R' | 'r' ) ( 'T' | 't' ) ( 'I' | 'i' ) ( 'T' | 't' ) ( 'I' | 'i' ) ( 'O' | 'o' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "PARTITION"

    // $ANTLR start "PARALLEL"
    public final void mPARALLEL() throws RecognitionException {
        try {
            int _type = PARALLEL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:386:2: ( ( 'P' | 'p' ) ( 'A' | 'a' ) ( 'R' | 'r' ) ( 'A' | 'a' ) ( 'L' | 'l' ) ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'L' | 'l' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:386:4: ( 'P' | 'p' ) ( 'A' | 'a' ) ( 'R' | 'r' ) ( 'A' | 'a' ) ( 'L' | 'l' ) ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'L' | 'l' )
            {
            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "PARALLEL"

    // $ANTLR start "NULLS_LAST"
    public final void mNULLS_LAST() throws RecognitionException {
        try {
            int _type = NULLS_LAST;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:388:13: ( ( 'N' | 'n' ) ( 'U' | 'u' ) ( 'L' | 'l' ) ( 'L' | 'l' ) ( 'S' | 's' ) WS ( 'L' | 'l' ) ( 'A' | 'a' ) ( 'S' | 's' ) ( 'T' | 't' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:388:15: ( 'N' | 'n' ) ( 'U' | 'u' ) ( 'L' | 'l' ) ( 'L' | 'l' ) ( 'S' | 's' ) WS ( 'L' | 'l' ) ( 'A' | 'a' ) ( 'S' | 's' ) ( 'T' | 't' )
            {
            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "NULLS_LAST"

    // $ANTLR start "NULLS_FIRST"
    public final void mNULLS_FIRST() throws RecognitionException {
        try {
            int _type = NULLS_FIRST;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:389:13: ( ( 'N' | 'n' ) ( 'U' | 'u' ) ( 'L' | 'l' ) ( 'L' | 'l' ) ( 'S' | 's' ) WS ( 'F' | 'f' ) ( 'I' | 'i' ) ( 'R' | 'r' ) ( 'S' | 's' ) ( 'T' | 't' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:389:15: ( 'N' | 'n' ) ( 'U' | 'u' ) ( 'L' | 'l' ) ( 'L' | 'l' ) ( 'S' | 's' ) WS ( 'F' | 'f' ) ( 'I' | 'i' ) ( 'R' | 'r' ) ( 'S' | 's' ) ( 'T' | 't' )
            {
            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "NULLS_FIRST"

    // $ANTLR start "DISPLAYWHERE"
    public final void mDISPLAYWHERE() throws RecognitionException {
        try {
            int _type = DISPLAYWHERE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:392:2: ( ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'S' | 's' ) ( 'P' | 'p' ) ( 'L' | 'l' ) ( 'A' | 'a' ) ( 'Y' | 'y' ) WS ( 'W' | 'w' ) ( 'H' | 'h' ) ( 'E' | 'e' ) ( 'R' | 'r' ) ( 'E' | 'e' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:392:4: ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'S' | 's' ) ( 'P' | 'p' ) ( 'L' | 'l' ) ( 'A' | 'a' ) ( 'Y' | 'y' ) WS ( 'W' | 'w' ) ( 'H' | 'h' ) ( 'E' | 'e' ) ( 'R' | 'r' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='Y'||input.LA(1)=='y' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='W'||input.LA(1)=='w' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='H'||input.LA(1)=='h' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DISPLAYWHERE"

    // $ANTLR start "DISPLAYBY"
    public final void mDISPLAYBY() throws RecognitionException {
        try {
            int _type = DISPLAYBY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:394:2: ( ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'S' | 's' ) ( 'P' | 'p' ) ( 'L' | 'l' ) ( 'A' | 'a' ) ( 'Y' | 'y' ) WS ( 'B' | 'b' ) ( 'Y' | 'y' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:394:4: ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'S' | 's' ) ( 'P' | 'p' ) ( 'L' | 'l' ) ( 'A' | 'a' ) ( 'Y' | 'y' ) WS ( 'B' | 'b' ) ( 'Y' | 'y' )
            {
            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='Y'||input.LA(1)=='y' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='B'||input.LA(1)=='b' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='Y'||input.LA(1)=='y' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DISPLAYBY"

    // $ANTLR start "TOP"
    public final void mTOP() throws RecognitionException {
        try {
            int _type = TOP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:395:5: ( ( 'T' | 't' ) ( 'O' | 'o' ) ( 'P' | 'p' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:395:7: ( 'T' | 't' ) ( 'O' | 'o' ) ( 'P' | 'p' )
            {
            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "TOP"

    // $ANTLR start "IF"
    public final void mIF() throws RecognitionException {
        try {
            int _type = IF;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:397:4: ( ( 'I' | 'i' ) ( 'F' | 'f' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:397:6: ( 'I' | 'i' ) ( 'F' | 'f' )
            {
            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "IF"

    // $ANTLR start "THEN"
    public final void mTHEN() throws RecognitionException {
        try {
            int _type = THEN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:398:6: ( ( 'T' | 't' ) ( 'H' | 'h' ) ( 'E' | 'e' ) ( 'N' | 'n' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:398:8: ( 'T' | 't' ) ( 'H' | 'h' ) ( 'E' | 'e' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='H'||input.LA(1)=='h' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "THEN"

    // $ANTLR start "ELSE"
    public final void mELSE() throws RecognitionException {
        try {
            int _type = ELSE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:399:6: ( ( 'E' | 'e' ) ( 'L' | 'l' ) ( 'S' | 's' ) ( 'E' | 'e' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:399:8: ( 'E' | 'e' ) ( 'L' | 'l' ) ( 'S' | 's' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ELSE"

    // $ANTLR start "ELSEIF"
    public final void mELSEIF() throws RecognitionException {
        try {
            int _type = ELSEIF;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:400:8: ( ( 'E' | 'e' ) ( 'L' | 'l' ) ( 'S' | 's' ) ( 'E' | 'e' ) ( 'I' | 'i' ) ( 'F' | 'f' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:400:10: ( 'E' | 'e' ) ( 'L' | 'l' ) ( 'S' | 's' ) ( 'E' | 'e' ) ( 'I' | 'i' ) ( 'F' | 'f' )
            {
            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ELSEIF"

    // $ANTLR start "ENDIF"
    public final void mENDIF() throws RecognitionException {
        try {
            int _type = ENDIF;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:401:7: ( ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'D' | 'd' ) WS ( 'I' | 'i' ) ( 'F' | 'f' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:401:9: ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'D' | 'd' ) WS ( 'I' | 'i' ) ( 'F' | 'f' )
            {
            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ENDIF"

    // $ANTLR start "WHILE"
    public final void mWHILE() throws RecognitionException {
        try {
            int _type = WHILE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:403:8: ( ( 'W' | 'w' ) ( 'H' | 'h' ) ( 'I' | 'i' ) ( 'L' | 'l' ) ( 'E' | 'e' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:403:10: ( 'W' | 'w' ) ( 'H' | 'h' ) ( 'I' | 'i' ) ( 'L' | 'l' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='W'||input.LA(1)=='w' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='H'||input.LA(1)=='h' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "WHILE"

    // $ANTLR start "ENDWHILE"
    public final void mENDWHILE() throws RecognitionException {
        try {
            int _type = ENDWHILE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:404:9: ( ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'D' | 'd' ) WS ( 'W' | 'w' ) ( 'H' | 'h' ) ( 'I' | 'i' ) ( 'L' | 'l' ) ( 'E' | 'e' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:404:11: ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'D' | 'd' ) WS ( 'W' | 'w' ) ( 'H' | 'h' ) ( 'I' | 'i' ) ( 'L' | 'l' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='W'||input.LA(1)=='w' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='H'||input.LA(1)=='h' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ENDWHILE"

    // $ANTLR start "EXITWHILE"
    public final void mEXITWHILE() throws RecognitionException {
        try {
            int _type = EXITWHILE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:405:10: ( ( 'E' | 'e' ) ( 'X' | 'x' ) ( 'I' | 'i' ) ( 'T' | 't' ) WS ( 'W' | 'w' ) ( 'H' | 'h' ) ( 'I' | 'i' ) ( 'L' | 'l' ) ( 'E' | 'e' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:405:12: ( 'E' | 'e' ) ( 'X' | 'x' ) ( 'I' | 'i' ) ( 'T' | 't' ) WS ( 'W' | 'w' ) ( 'H' | 'h' ) ( 'I' | 'i' ) ( 'L' | 'l' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='X'||input.LA(1)=='x' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='W'||input.LA(1)=='w' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='H'||input.LA(1)=='h' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "EXITWHILE"

    // $ANTLR start "FOR"
    public final void mFOR() throws RecognitionException {
        try {
            int _type = FOR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:407:5: ( ( 'F' | 'f' ) ( 'O' | 'o' ) ( 'R' | 'r' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:407:7: ( 'F' | 'f' ) ( 'O' | 'o' ) ( 'R' | 'r' )
            {
            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "FOR"

    // $ANTLR start "TO"
    public final void mTO() throws RecognitionException {
        try {
            int _type = TO;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:408:4: ( ( 'T' | 't' ) ( 'O' | 'o' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:408:6: ( 'T' | 't' ) ( 'O' | 'o' )
            {
            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "TO"

    // $ANTLR start "NEXT"
    public final void mNEXT() throws RecognitionException {
        try {
            int _type = NEXT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:409:6: ( ( 'N' | 'n' ) ( 'E' | 'e' ) ( 'X' | 'x' ) ( 'T' | 't' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:409:8: ( 'N' | 'n' ) ( 'E' | 'e' ) ( 'X' | 'x' ) ( 'T' | 't' )
            {
            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='X'||input.LA(1)=='x' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "NEXT"

    // $ANTLR start "EXITFOR"
    public final void mEXITFOR() throws RecognitionException {
        try {
            int _type = EXITFOR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:410:9: ( ( 'E' | 'e' ) ( 'X' | 'x' ) ( 'I' | 'i' ) ( 'T' | 't' ) WS ( 'F' | 'f' ) ( 'O' | 'o' ) ( 'R' | 'r' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:410:11: ( 'E' | 'e' ) ( 'X' | 'x' ) ( 'I' | 'i' ) ( 'T' | 't' ) WS ( 'F' | 'f' ) ( 'O' | 'o' ) ( 'R' | 'r' )
            {
            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='X'||input.LA(1)=='x' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "EXITFOR"

    // $ANTLR start "STEP"
    public final void mSTEP() throws RecognitionException {
        try {
            int _type = STEP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:411:6: ( ( 'S' | 's' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'P' | 'p' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:411:8: ( 'S' | 's' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'P' | 'p' )
            {
            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "STEP"

    // $ANTLR start "COMPLETE"
    public final void mCOMPLETE() throws RecognitionException {
        try {
            int _type = COMPLETE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:414:2: ( ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'M' | 'm' ) ( 'P' | 'p' ) ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'E' | 'e' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:414:4: ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'M' | 'm' ) ( 'P' | 'p' ) ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "COMPLETE"

    // $ANTLR start "ENDCOMPLETE"
    public final void mENDCOMPLETE() throws RecognitionException {
        try {
            int _type = ENDCOMPLETE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:416:2: ( ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'D' | 'd' ) WS ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'M' | 'm' ) ( 'P' | 'p' ) ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'E' | 'e' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:416:4: ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'D' | 'd' ) WS ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'M' | 'm' ) ( 'P' | 'p' ) ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ENDCOMPLETE"

    // $ANTLR start "LIST"
    public final void mLIST() throws RecognitionException {
        try {
            int _type = LIST;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:418:6: ( ( 'L' | 'l' ) ( 'I' | 'i' ) ( 'S' | 's' ) ( 'T' | 't' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:418:8: ( 'L' | 'l' ) ( 'I' | 'i' ) ( 'S' | 's' ) ( 'T' | 't' )
            {
            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LIST"

    // $ANTLR start "ADD"
    public final void mADD() throws RecognitionException {
        try {
            int _type = ADD;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:419:5: ( ( 'A' | 'a' ) ( 'D' | 'd' ) ( 'D' | 'd' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:419:7: ( 'A' | 'a' ) ( 'D' | 'd' ) ( 'D' | 'd' )
            {
            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ADD"

    // $ANTLR start "REMOVEAT"
    public final void mREMOVEAT() throws RecognitionException {
        try {
            int _type = REMOVEAT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:421:2: ( ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'M' | 'm' ) ( 'O' | 'o' ) ( 'V' | 'v' ) ( 'E' | 'e' ) ( 'A' | 'a' ) ( 'T' | 't' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:421:4: ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'M' | 'm' ) ( 'O' | 'o' ) ( 'V' | 'v' ) ( 'E' | 'e' ) ( 'A' | 'a' ) ( 'T' | 't' )
            {
            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='V'||input.LA(1)=='v' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "REMOVEAT"

    // $ANTLR start "REMOVEALL"
    public final void mREMOVEALL() throws RecognitionException {
        try {
            int _type = REMOVEALL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:423:2: ( ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'M' | 'm' ) ( 'O' | 'o' ) ( 'V' | 'v' ) ( 'E' | 'e' ) ( 'A' | 'a' ) ( 'L' | 'l' ) ( 'L' | 'l' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:423:4: ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'M' | 'm' ) ( 'O' | 'o' ) ( 'V' | 'v' ) ( 'E' | 'e' ) ( 'A' | 'a' ) ( 'L' | 'l' ) ( 'L' | 'l' )
            {
            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='V'||input.LA(1)=='v' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "REMOVEALL"

    // $ANTLR start "MAP"
    public final void mMAP() throws RecognitionException {
        try {
            int _type = MAP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:425:5: ( ( 'M' | 'm' ) ( 'A' | 'a' ) ( 'P' | 'p' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:425:7: ( 'M' | 'm' ) ( 'A' | 'a' ) ( 'P' | 'p' )
            {
            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "MAP"

    // $ANTLR start "PUT"
    public final void mPUT() throws RecognitionException {
        try {
            int _type = PUT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:426:5: ( ( 'P' | 'p' ) ( 'U' | 'u' ) ( 'T' | 't' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:426:7: ( 'P' | 'p' ) ( 'U' | 'u' ) ( 'T' | 't' )
            {
            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "PUT"

    // $ANTLR start "REMOVEITEM"
    public final void mREMOVEITEM() throws RecognitionException {
        try {
            int _type = REMOVEITEM;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:428:2: ( ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'M' | 'm' ) ( 'O' | 'o' ) ( 'V' | 'v' ) ( 'E' | 'e' ) ( 'I' | 'i' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'M' | 'm' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:428:4: ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'M' | 'm' ) ( 'O' | 'o' ) ( 'V' | 'v' ) ( 'E' | 'e' ) ( 'I' | 'i' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'M' | 'm' )
            {
            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='V'||input.LA(1)=='v' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "REMOVEITEM"

    // $ANTLR start "DELETE"
    public final void mDELETE() throws RecognitionException {
        try {
            int _type = DELETE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:429:8: ( ( 'D' | 'd' ) ( 'E' | 'e' ) ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'E' | 'e' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:429:10: ( 'D' | 'd' ) ( 'E' | 'e' ) ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DELETE"

    // $ANTLR start "PRINT"
    public final void mPRINT() throws RecognitionException {
        try {
            int _type = PRINT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:431:7: ( ( 'P' | 'p' ) ( 'R' | 'r' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'T' | 't' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:431:9: ( 'P' | 'p' ) ( 'R' | 'r' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'T' | 't' )
            {
            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "PRINT"

    // $ANTLR start "HALT"
    public final void mHALT() throws RecognitionException {
        try {
            int _type = HALT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:432:6: ( ( 'H' | 'h' ) ( 'A' | 'a' ) ( 'L' | 'l' ) ( 'T' | 't' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:432:8: ( 'H' | 'h' ) ( 'A' | 'a' ) ( 'L' | 'l' ) ( 'T' | 't' )
            {
            if ( input.LA(1)=='H'||input.LA(1)=='h' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "HALT"

    // $ANTLR start "SUBSTRING"
    public final void mSUBSTRING() throws RecognitionException {
        try {
            int _type = SUBSTRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:436:2: ( ( 'S' | 's' ) ( 'U' | 'u' ) ( 'B' | 'b' ) ( 'S' | 's' ) ( 'T' | 't' ) ( 'R' | 'r' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'G' | 'g' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:436:4: ( 'S' | 's' ) ( 'U' | 'u' ) ( 'B' | 'b' ) ( 'S' | 's' ) ( 'T' | 't' ) ( 'R' | 'r' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'G' | 'g' )
            {
            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='B'||input.LA(1)=='b' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "SUBSTRING"

    // $ANTLR start "LENGTH"
    public final void mLENGTH() throws RecognitionException {
        try {
            int _type = LENGTH;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:437:8: ( ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'G' | 'g' ) ( 'T' | 't' ) ( 'H' | 'h' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:437:10: ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'G' | 'g' ) ( 'T' | 't' ) ( 'H' | 'h' )
            {
            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='H'||input.LA(1)=='h' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LENGTH"

    // $ANTLR start "POSITION"
    public final void mPOSITION() throws RecognitionException {
        try {
            int _type = POSITION;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:438:9: ( ( 'P' | 'p' ) ( 'O' | 'o' ) ( 'S' | 's' ) ( 'I' | 'i' ) ( 'T' | 't' ) ( 'I' | 'i' ) ( 'O' | 'o' ) ( 'N' | 'n' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:438:11: ( 'P' | 'p' ) ( 'O' | 'o' ) ( 'S' | 's' ) ( 'I' | 'i' ) ( 'T' | 't' ) ( 'I' | 'i' ) ( 'O' | 'o' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "POSITION"

    // $ANTLR start "TOUPPER"
    public final void mTOUPPER() throws RecognitionException {
        try {
            int _type = TOUPPER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:440:8: ( ( 'T' | 't' ) ( 'O' | 'o' ) ( 'U' | 'u' ) ( 'P' | 'p' ) ( 'P' | 'p' ) ( 'E' | 'e' ) ( 'R' | 'r' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:440:10: ( 'T' | 't' ) ( 'O' | 'o' ) ( 'U' | 'u' ) ( 'P' | 'p' ) ( 'P' | 'p' ) ( 'E' | 'e' ) ( 'R' | 'r' )
            {
            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "TOUPPER"

    // $ANTLR start "TOLOWER"
    public final void mTOLOWER() throws RecognitionException {
        try {
            int _type = TOLOWER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:441:8: ( ( 'T' | 't' ) ( 'O' | 'o' ) ( 'L' | 'l' ) ( 'O' | 'o' ) ( 'W' | 'w' ) ( 'E' | 'e' ) ( 'R' | 'r' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:441:10: ( 'T' | 't' ) ( 'O' | 'o' ) ( 'L' | 'l' ) ( 'O' | 'o' ) ( 'W' | 'w' ) ( 'E' | 'e' ) ( 'R' | 'r' )
            {
            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='W'||input.LA(1)=='w' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "TOLOWER"

    // $ANTLR start "TRIM"
    public final void mTRIM() throws RecognitionException {
        try {
            int _type = TRIM;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:442:6: ( ( 'T' | 't' ) ( 'R' | 'r' ) ( 'I' | 'i' ) ( 'M' | 'm' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:442:8: ( 'T' | 't' ) ( 'R' | 'r' ) ( 'I' | 'i' ) ( 'M' | 'm' )
            {
            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "TRIM"

    // $ANTLR start "FORMAT"
    public final void mFORMAT() throws RecognitionException {
        try {
            int _type = FORMAT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:443:8: ( ( 'F' | 'f' ) ( 'O' | 'o' ) ( 'R' | 'r' ) ( 'M' | 'm' ) ( 'A' | 'a' ) ( 'T' | 't' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:443:10: ( 'F' | 'f' ) ( 'O' | 'o' ) ( 'R' | 'r' ) ( 'M' | 'm' ) ( 'A' | 'a' ) ( 'T' | 't' )
            {
            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "FORMAT"

    // $ANTLR start "REPLACE"
    public final void mREPLACE() throws RecognitionException {
        try {
            int _type = REPLACE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:444:9: ( ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'P' | 'p' ) ( 'L' | 'l' ) ( 'A' | 'a' ) ( 'C' | 'c' ) ( 'E' | 'e' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:444:11: ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'P' | 'p' ) ( 'L' | 'l' ) ( 'A' | 'a' ) ( 'C' | 'c' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "REPLACE"

    // $ANTLR start "REPLACEALL"
    public final void mREPLACEALL() throws RecognitionException {
        try {
            int _type = REPLACEALL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:445:12: ( ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'P' | 'p' ) ( 'L' | 'l' ) ( 'A' | 'a' ) ( 'C' | 'c' ) ( 'E' | 'e' ) ( 'A' | 'a' ) ( 'L' | 'l' ) ( 'L' | 'l' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:446:3: ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'P' | 'p' ) ( 'L' | 'l' ) ( 'A' | 'a' ) ( 'C' | 'c' ) ( 'E' | 'e' ) ( 'A' | 'a' ) ( 'L' | 'l' ) ( 'L' | 'l' )
            {
            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "REPLACEALL"

    // $ANTLR start "URLENCODE"
    public final void mURLENCODE() throws RecognitionException {
        try {
            int _type = URLENCODE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:447:11: ( ( 'U' | 'u' ) ( 'R' | 'r' ) ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'D' | 'd' ) ( 'E' | 'e' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:448:3: ( 'U' | 'u' ) ( 'R' | 'r' ) ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'D' | 'd' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "URLENCODE"

    // $ANTLR start "TOTIME"
    public final void mTOTIME() throws RecognitionException {
        try {
            int _type = TOTIME;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:451:8: ( ( 'T' | 't' ) ( 'O' | 'o' ) ( 'T' | 't' ) ( 'I' | 'i' ) ( 'M' | 'm' ) ( 'E' | 'e' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:451:10: ( 'T' | 't' ) ( 'O' | 'o' ) ( 'T' | 't' ) ( 'I' | 'i' ) ( 'M' | 'm' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "TOTIME"

    // $ANTLR start "DATEPART"
    public final void mDATEPART() throws RecognitionException {
        try {
            int _type = DATEPART;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:454:9: ( ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'P' | 'p' ) ( 'A' | 'a' ) ( 'R' | 'r' ) ( 'T' | 't' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:454:11: ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'P' | 'p' ) ( 'A' | 'a' ) ( 'R' | 'r' ) ( 'T' | 't' )
            {
            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DATEPART"

    // $ANTLR start "DATEDIFF"
    public final void mDATEDIFF() throws RecognitionException {
        try {
            int _type = DATEDIFF;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:455:9: ( ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'F' | 'f' ) ( 'F' | 'f' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:455:11: ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'F' | 'f' ) ( 'F' | 'f' )
            {
            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DATEDIFF"

    // $ANTLR start "DATEADD"
    public final void mDATEADD() throws RecognitionException {
        try {
            int _type = DATEADD;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:456:8: ( ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'A' | 'a' ) ( 'D' | 'd' ) ( 'D' | 'd' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:456:10: ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'A' | 'a' ) ( 'D' | 'd' ) ( 'D' | 'd' )
            {
            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DATEADD"

    // $ANTLR start "IIF"
    public final void mIIF() throws RecognitionException {
        try {
            int _type = IIF;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:458:5: ( ( 'I' | 'i' ) ( 'I' | 'i' ) ( 'F' | 'f' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:458:7: ( 'I' | 'i' ) ( 'I' | 'i' ) ( 'F' | 'f' )
            {
            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "IIF"

    // $ANTLR start "IFNULL"
    public final void mIFNULL() throws RecognitionException {
        try {
            int _type = IFNULL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:459:7: ( ( 'I' | 'i' ) ( 'F' | 'f' ) ( 'N' | 'n' ) ( 'U' | 'u' ) ( 'L' | 'l' ) ( 'L' | 'l' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:459:9: ( 'I' | 'i' ) ( 'F' | 'f' ) ( 'N' | 'n' ) ( 'U' | 'u' ) ( 'L' | 'l' ) ( 'L' | 'l' )
            {
            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "IFNULL"

    // $ANTLR start "DATEFIELD"
    public final void mDATEFIELD() throws RecognitionException {
        try {
            int _type = DATEFIELD;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:462:2: ( ( 'Y' | 'y' ) ( 'E' | 'e' ) ( 'A' | 'a' ) ( 'R' | 'r' ) | ( 'Q' | 'q' ) ( 'U' | 'u' ) ( 'A' | 'a' ) ( 'R' | 'r' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'R' | 'r' ) | ( 'M' | 'm' ) ( 'O' | 'o' ) ( 'N' | 'n' ) ( 'T' | 't' ) ( 'H' | 'h' ) | ( 'W' | 'w' ) ( 'E' | 'e' ) ( 'E' | 'e' ) ( 'K' | 'k' ) | ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'Y' | 'y' ) | ( 'H' | 'h' ) ( 'O' | 'o' ) ( 'U' | 'u' ) ( 'R' | 'r' ) | ( 'M' | 'm' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'U' | 'u' ) ( 'T' | 't' ) ( 'E' | 'e' ) | ( 'S' | 's' ) ( 'E' | 'e' ) ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'N' | 'n' ) ( 'D' | 'd' ) )
            int alt18=8;
            switch ( input.LA(1) ) {
            case 'Y':
            case 'y':
                {
                alt18=1;
                }
                break;
            case 'Q':
            case 'q':
                {
                alt18=2;
                }
                break;
            case 'M':
            case 'm':
                {
                int LA18_3 = input.LA(2);

                if ( (LA18_3=='O'||LA18_3=='o') ) {
                    alt18=3;
                }
                else if ( (LA18_3=='I'||LA18_3=='i') ) {
                    alt18=7;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 18, 3, input);

                    throw nvae;

                }
                }
                break;
            case 'W':
            case 'w':
                {
                alt18=4;
                }
                break;
            case 'D':
            case 'd':
                {
                alt18=5;
                }
                break;
            case 'H':
            case 'h':
                {
                alt18=6;
                }
                break;
            case 'S':
            case 's':
                {
                alt18=8;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 18, 0, input);

                throw nvae;

            }

            switch (alt18) {
                case 1 :
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:462:4: ( 'Y' | 'y' ) ( 'E' | 'e' ) ( 'A' | 'a' ) ( 'R' | 'r' )
                    {
                    if ( input.LA(1)=='Y'||input.LA(1)=='y' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;
                case 2 :
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:463:3: ( 'Q' | 'q' ) ( 'U' | 'u' ) ( 'A' | 'a' ) ( 'R' | 'r' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'R' | 'r' )
                    {
                    if ( input.LA(1)=='Q'||input.LA(1)=='q' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;
                case 3 :
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:464:3: ( 'M' | 'm' ) ( 'O' | 'o' ) ( 'N' | 'n' ) ( 'T' | 't' ) ( 'H' | 'h' )
                    {
                    if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='H'||input.LA(1)=='h' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;
                case 4 :
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:465:3: ( 'W' | 'w' ) ( 'E' | 'e' ) ( 'E' | 'e' ) ( 'K' | 'k' )
                    {
                    if ( input.LA(1)=='W'||input.LA(1)=='w' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='K'||input.LA(1)=='k' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;
                case 5 :
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:466:3: ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'Y' | 'y' )
                    {
                    if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='Y'||input.LA(1)=='y' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;
                case 6 :
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:467:3: ( 'H' | 'h' ) ( 'O' | 'o' ) ( 'U' | 'u' ) ( 'R' | 'r' )
                    {
                    if ( input.LA(1)=='H'||input.LA(1)=='h' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;
                case 7 :
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:468:3: ( 'M' | 'm' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'U' | 'u' ) ( 'T' | 't' ) ( 'E' | 'e' )
                    {
                    if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;
                case 8 :
                    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:469:3: ( 'S' | 's' ) ( 'E' | 'e' ) ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'N' | 'n' ) ( 'D' | 'd' )
                    {
                    if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DATEFIELD"

    // $ANTLR start "GETCOLUMNVALUE"
    public final void mGETCOLUMNVALUE() throws RecognitionException {
        try {
            int _type = GETCOLUMNVALUE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:471:2: ( ( 'G' | 'g' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'L' | 'l' ) ( 'U' | 'u' ) ( 'M' | 'm' ) ( 'N' | 'n' ) ( 'V' | 'v' ) ( 'A' | 'a' ) ( 'L' | 'l' ) ( 'U' | 'u' ) ( 'E' | 'e' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:471:4: ( 'G' | 'g' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'L' | 'l' ) ( 'U' | 'u' ) ( 'M' | 'm' ) ( 'N' | 'n' ) ( 'V' | 'v' ) ( 'A' | 'a' ) ( 'L' | 'l' ) ( 'U' | 'u' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='V'||input.LA(1)=='v' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "GETCOLUMNVALUE"

    // $ANTLR start "GETVARIABLE"
    public final void mGETVARIABLE() throws RecognitionException {
        try {
            int _type = GETVARIABLE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:473:2: ( ( 'G' | 'g' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'V' | 'v' ) ( 'A' | 'a' ) ( 'R' | 'r' ) ( 'I' | 'i' ) ( 'A' | 'a' ) ( 'B' | 'b' ) ( 'L' | 'l' ) ( 'E' | 'e' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:473:4: ( 'G' | 'g' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'V' | 'v' ) ( 'A' | 'a' ) ( 'R' | 'r' ) ( 'I' | 'i' ) ( 'A' | 'a' ) ( 'B' | 'b' ) ( 'L' | 'l' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='V'||input.LA(1)=='v' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='B'||input.LA(1)=='b' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "GETVARIABLE"

    // $ANTLR start "SAVEDEXPRESSION"
    public final void mSAVEDEXPRESSION() throws RecognitionException {
        try {
            int _type = SAVEDEXPRESSION;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:475:2: ( ( 'S' | 's' ) ( 'A' | 'a' ) ( 'V' | 'v' ) ( 'E' | 'e' ) ( 'D' | 'd' ) ( 'E' | 'e' ) ( 'X' | 'x' ) ( 'P' | 'p' ) ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'S' | 's' ) ( 'S' | 's' ) ( 'I' | 'i' ) ( 'O' | 'o' ) ( 'N' | 'n' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:475:6: ( 'S' | 's' ) ( 'A' | 'a' ) ( 'V' | 'v' ) ( 'E' | 'e' ) ( 'D' | 'd' ) ( 'E' | 'e' ) ( 'X' | 'x' ) ( 'P' | 'p' ) ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'S' | 's' ) ( 'S' | 's' ) ( 'I' | 'i' ) ( 'O' | 'o' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='V'||input.LA(1)=='v' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='X'||input.LA(1)=='x' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "SAVEDEXPRESSION"

    // $ANTLR start "ADDPARENTCHILD"
    public final void mADDPARENTCHILD() throws RecognitionException {
        try {
            int _type = ADDPARENTCHILD;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:477:2: ( ( 'A' | 'a' ) ( 'D' | 'd' ) ( 'D' | 'd' ) ( 'P' | 'p' ) ( 'A' | 'a' ) ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'T' | 't' ) ( 'C' | 'c' ) ( 'H' | 'h' ) ( 'I' | 'i' ) ( 'L' | 'l' ) ( 'D' | 'd' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:477:4: ( 'A' | 'a' ) ( 'D' | 'd' ) ( 'D' | 'd' ) ( 'P' | 'p' ) ( 'A' | 'a' ) ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'T' | 't' ) ( 'C' | 'c' ) ( 'H' | 'h' ) ( 'I' | 'i' ) ( 'L' | 'l' ) ( 'D' | 'd' )
            {
            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='H'||input.LA(1)=='h' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ADDPARENTCHILD"

    // $ANTLR start "NEXTCHILD"
    public final void mNEXTCHILD() throws RecognitionException {
        try {
            int _type = NEXTCHILD;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:479:2: ( ( 'N' | 'n' ) ( 'E' | 'e' ) ( 'X' | 'x' ) ( 'T' | 't' ) ( 'C' | 'c' ) ( 'H' | 'h' ) ( 'I' | 'i' ) ( 'L' | 'l' ) ( 'D' | 'd' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:479:4: ( 'N' | 'n' ) ( 'E' | 'e' ) ( 'X' | 'x' ) ( 'T' | 't' ) ( 'C' | 'c' ) ( 'H' | 'h' ) ( 'I' | 'i' ) ( 'L' | 'l' ) ( 'D' | 'd' )
            {
            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='X'||input.LA(1)=='x' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='H'||input.LA(1)=='h' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "NEXTCHILD"

    // $ANTLR start "GETLEVELVALUE"
    public final void mGETLEVELVALUE() throws RecognitionException {
        try {
            int _type = GETLEVELVALUE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:481:2: ( ( 'G' | 'g' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'V' | 'v' ) ( 'E' | 'e' ) ( 'L' | 'l' ) ( 'V' | 'v' ) ( 'A' | 'a' ) ( 'L' | 'l' ) ( 'U' | 'u' ) ( 'E' | 'e' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:481:4: ( 'G' | 'g' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'V' | 'v' ) ( 'E' | 'e' ) ( 'L' | 'l' ) ( 'V' | 'v' ) ( 'A' | 'a' ) ( 'L' | 'l' ) ( 'U' | 'u' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='V'||input.LA(1)=='v' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='V'||input.LA(1)=='v' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "GETLEVELVALUE"

    // $ANTLR start "GETLEVELATTRIBUTE"
    public final void mGETLEVELATTRIBUTE() throws RecognitionException {
        try {
            int _type = GETLEVELATTRIBUTE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:483:2: ( ( 'G' | 'g' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'V' | 'v' ) ( 'E' | 'e' ) ( 'L' | 'l' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'T' | 't' ) ( 'R' | 'r' ) ( 'I' | 'i' ) ( 'B' | 'b' ) ( 'U' | 'u' ) ( 'T' | 't' ) ( 'E' | 'e' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:483:4: ( 'G' | 'g' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'V' | 'v' ) ( 'E' | 'e' ) ( 'L' | 'l' ) ( 'A' | 'a' ) ( 'T' | 't' ) ( 'T' | 't' ) ( 'R' | 'r' ) ( 'I' | 'i' ) ( 'B' | 'b' ) ( 'U' | 'u' ) ( 'T' | 't' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='V'||input.LA(1)=='v' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='B'||input.LA(1)=='b' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "GETLEVELATTRIBUTE"

    // $ANTLR start "GETDAYID"
    public final void mGETDAYID() throws RecognitionException {
        try {
            int _type = GETDAYID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:485:2: ( ( 'G' | 'g' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'Y' | 'y' ) ( 'I' | 'i' ) ( 'D' | 'd' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:485:4: ( 'G' | 'g' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'D' | 'd' ) ( 'A' | 'a' ) ( 'Y' | 'y' ) ( 'I' | 'i' ) ( 'D' | 'd' )
            {
            if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='Y'||input.LA(1)=='y' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "GETDAYID"

    // $ANTLR start "GETWEEKID"
    public final void mGETWEEKID() throws RecognitionException {
        try {
            int _type = GETWEEKID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:487:2: ( ( 'G' | 'g' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'W' | 'w' ) ( 'E' | 'e' ) ( 'E' | 'e' ) ( 'K' | 'k' ) ( 'I' | 'i' ) ( 'D' | 'd' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:487:4: ( 'G' | 'g' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'W' | 'w' ) ( 'E' | 'e' ) ( 'E' | 'e' ) ( 'K' | 'k' ) ( 'I' | 'i' ) ( 'D' | 'd' )
            {
            if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='W'||input.LA(1)=='w' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='K'||input.LA(1)=='k' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "GETWEEKID"

    // $ANTLR start "GETMONTHID"
    public final void mGETMONTHID() throws RecognitionException {
        try {
            int _type = GETMONTHID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:489:2: ( ( 'G' | 'g' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'M' | 'm' ) ( 'O' | 'o' ) ( 'N' | 'n' ) ( 'T' | 't' ) ( 'H' | 'h' ) ( 'I' | 'i' ) ( 'D' | 'd' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:489:4: ( 'G' | 'g' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'M' | 'm' ) ( 'O' | 'o' ) ( 'N' | 'n' ) ( 'T' | 't' ) ( 'H' | 'h' ) ( 'I' | 'i' ) ( 'D' | 'd' )
            {
            if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='H'||input.LA(1)=='h' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "GETMONTHID"

    // $ANTLR start "SETLOOKUP"
    public final void mSETLOOKUP() throws RecognitionException {
        try {
            int _type = SETLOOKUP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:490:11: ( ( 'L' | 'l' ) ( 'O' | 'o' ) ( 'O' | 'o' ) ( 'K' | 'k' ) ( 'U' | 'u' ) ( 'P' | 'p' ) ( 'V' | 'v' ) ( 'A' | 'a' ) ( 'L' | 'l' ) ( 'U' | 'u' ) ( 'E' | 'e' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:490:13: ( 'L' | 'l' ) ( 'O' | 'o' ) ( 'O' | 'o' ) ( 'K' | 'k' ) ( 'U' | 'u' ) ( 'P' | 'p' ) ( 'V' | 'v' ) ( 'A' | 'a' ) ( 'L' | 'l' ) ( 'U' | 'u' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='K'||input.LA(1)=='k' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='V'||input.LA(1)=='v' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "SETLOOKUP"

    // $ANTLR start "SETLOOKUPROW"
    public final void mSETLOOKUPROW() throws RecognitionException {
        try {
            int _type = SETLOOKUPROW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:491:14: ( ( 'L' | 'l' ) ( 'O' | 'o' ) ( 'O' | 'o' ) ( 'K' | 'k' ) ( 'U' | 'u' ) ( 'P' | 'p' ) ( 'R' | 'r' ) ( 'O' | 'o' ) ( 'W' | 'w' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:491:16: ( 'L' | 'l' ) ( 'O' | 'o' ) ( 'O' | 'o' ) ( 'K' | 'k' ) ( 'U' | 'u' ) ( 'P' | 'p' ) ( 'R' | 'r' ) ( 'O' | 'o' ) ( 'W' | 'w' )
            {
            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='K'||input.LA(1)=='k' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='W'||input.LA(1)=='w' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "SETLOOKUPROW"

    // $ANTLR start "SETFIND"
    public final void mSETFIND() throws RecognitionException {
        try {
            int _type = SETFIND;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:492:9: ( ( 'F' | 'f' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'D' | 'd' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:492:11: ( 'F' | 'f' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'D' | 'd' )
            {
            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "SETFIND"

    // $ANTLR start "SETSTAT"
    public final void mSETSTAT() throws RecognitionException {
        try {
            int _type = SETSTAT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:493:9: ( ( 'S' | 's' ) ( 'T' | 't' ) ( 'A' | 'a' ) ( 'T' | 't' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:493:11: ( 'S' | 's' ) ( 'T' | 't' ) ( 'A' | 'a' ) ( 'T' | 't' )
            {
            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "SETSTAT"

    // $ANTLR start "LET"
    public final void mLET() throws RecognitionException {
        try {
            int _type = LET;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:494:5: ( ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'T' | 't' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:494:7: ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'T' | 't' )
            {
            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LET"

    // $ANTLR start "FUNCTION"
    public final void mFUNCTION() throws RecognitionException {
        try {
            int _type = FUNCTION;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:496:2: ( ( 'F' | 'f' ) ( 'U' | 'u' ) ( 'N' | 'n' ) ( 'C' | 'c' ) ( 'T' | 't' ) ( 'I' | 'i' ) ( 'O' | 'o' ) ( 'N' | 'n' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:496:4: ( 'F' | 'f' ) ( 'U' | 'u' ) ( 'N' | 'n' ) ( 'C' | 'c' ) ( 'T' | 't' ) ( 'I' | 'i' ) ( 'O' | 'o' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "FUNCTION"

    // $ANTLR start "RETURN"
    public final void mRETURN() throws RecognitionException {
        try {
            int _type = RETURN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:497:8: ( ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'U' | 'u' ) ( 'R' | 'r' ) ( 'N' | 'n' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:497:10: ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'U' | 'u' ) ( 'R' | 'r' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "RETURN"

    // $ANTLR start "ENDFUNCTION"
    public final void mENDFUNCTION() throws RecognitionException {
        try {
            int _type = ENDFUNCTION;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:499:2: ( ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'D' | 'd' ) WS ( 'F' | 'f' ) ( 'U' | 'u' ) ( 'N' | 'n' ) ( 'C' | 'c' ) ( 'T' | 't' ) ( 'I' | 'i' ) ( 'O' | 'o' ) ( 'N' | 'n' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:499:4: ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'D' | 'd' ) WS ( 'F' | 'f' ) ( 'U' | 'u' ) ( 'N' | 'n' ) ( 'C' | 'c' ) ( 'T' | 't' ) ( 'I' | 'i' ) ( 'O' | 'o' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mWS(); 


            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ENDFUNCTION"

    // $ANTLR start "FUNCTIONLOOKUP"
    public final void mFUNCTIONLOOKUP() throws RecognitionException {
        try {
            int _type = FUNCTIONLOOKUP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:501:2: ( ( 'F' | 'f' ) ( 'U' | 'u' ) ( 'N' | 'n' ) ( 'C' | 'c' ) ( 'T' | 't' ) ( 'I' | 'i' ) ( 'O' | 'o' ) ( 'N' | 'n' ) ( 'L' | 'l' ) ( 'O' | 'o' ) ( 'O' | 'o' ) ( 'K' | 'k' ) ( 'U' | 'u' ) ( 'P' | 'p' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:501:4: ( 'F' | 'f' ) ( 'U' | 'u' ) ( 'N' | 'n' ) ( 'C' | 'c' ) ( 'T' | 't' ) ( 'I' | 'i' ) ( 'O' | 'o' ) ( 'N' | 'n' ) ( 'L' | 'l' ) ( 'O' | 'o' ) ( 'O' | 'o' ) ( 'K' | 'k' ) ( 'U' | 'u' ) ( 'P' | 'p' )
            {
            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='K'||input.LA(1)=='k' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "FUNCTIONLOOKUP"

    // $ANTLR start "TRANSFORM"
    public final void mTRANSFORM() throws RecognitionException {
        try {
            int _type = TRANSFORM;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:503:2: ( ( 'T' | 't' ) ( 'R' | 'r' ) ( 'A' | 'a' ) ( 'N' | 'n' ) ( 'S' | 's' ) ( 'F' | 'f' ) ( 'O' | 'o' ) ( 'R' | 'r' ) ( 'M' | 'm' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:503:4: ( 'T' | 't' ) ( 'R' | 'r' ) ( 'A' | 'a' ) ( 'N' | 'n' ) ( 'S' | 's' ) ( 'F' | 'f' ) ( 'O' | 'o' ) ( 'R' | 'r' ) ( 'M' | 'm' )
            {
            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "TRANSFORM"

    // $ANTLR start "TREND"
    public final void mTREND() throws RecognitionException {
        try {
            int _type = TREND;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:506:2: ( ( 'T' | 't' ) ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'D' | 'd' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:506:4: ( 'T' | 't' ) ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'D' | 'd' )
            {
            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "TREND"

    // $ANTLR start "MDXEXPRESSION"
    public final void mMDXEXPRESSION() throws RecognitionException {
        try {
            int _type = MDXEXPRESSION;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:509:2: ( ( 'M' | 'm' ) ( 'D' | 'd' ) ( 'X' | 'x' ) ( 'E' | 'e' ) ( 'X' | 'x' ) ( 'P' | 'p' ) ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'S' | 's' ) ( 'S' | 's' ) ( 'I' | 'i' ) ( 'O' | 'o' ) ( 'N' | 'n' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:509:4: ( 'M' | 'm' ) ( 'D' | 'd' ) ( 'X' | 'x' ) ( 'E' | 'e' ) ( 'X' | 'x' ) ( 'P' | 'p' ) ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'S' | 's' ) ( 'S' | 's' ) ( 'I' | 'i' ) ( 'O' | 'o' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='X'||input.LA(1)=='x' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='X'||input.LA(1)=='x' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "MDXEXPRESSION"

    // $ANTLR start "STATMEDIAN"
    public final void mSTATMEDIAN() throws RecognitionException {
        try {
            int _type = STATMEDIAN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:512:2: ( ( 'M' | 'm' ) ( 'E' | 'e' ) ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'A' | 'a' ) ( 'N' | 'n' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:512:4: ( 'M' | 'm' ) ( 'E' | 'e' ) ( 'D' | 'd' ) ( 'I' | 'i' ) ( 'A' | 'a' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "STATMEDIAN"

    // $ANTLR start "STATSTDDEV"
    public final void mSTATSTDDEV() throws RecognitionException {
        try {
            int _type = STATSTDDEV;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:514:2: ( ( 'S' | 's' ) ( 'T' | 't' ) ( 'D' | 'd' ) ( 'D' | 'd' ) ( 'E' | 'e' ) ( 'V' | 'v' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:514:4: ( 'S' | 's' ) ( 'T' | 't' ) ( 'D' | 'd' ) ( 'D' | 'd' ) ( 'E' | 'e' ) ( 'V' | 'v' )
            {
            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='V'||input.LA(1)=='v' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "STATSTDDEV"

    // $ANTLR start "NUMROWS"
    public final void mNUMROWS() throws RecognitionException {
        try {
            int _type = NUMROWS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:515:9: ( ( 'N' | 'n' ) ( 'U' | 'u' ) ( 'M' | 'm' ) ( 'R' | 'r' ) ( 'O' | 'o' ) ( 'W' | 'w' ) ( 'S' | 's' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:515:11: ( 'N' | 'n' ) ( 'U' | 'u' ) ( 'M' | 'm' ) ( 'R' | 'r' ) ( 'O' | 'o' ) ( 'W' | 'w' ) ( 'S' | 's' )
            {
            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='W'||input.LA(1)=='w' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "NUMROWS"

    // $ANTLR start "ROWNUMBER"
    public final void mROWNUMBER() throws RecognitionException {
        try {
            int _type = ROWNUMBER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:517:2: ( ( 'R' | 'r' ) ( 'O' | 'o' ) ( 'W' | 'w' ) ( 'N' | 'n' ) ( 'U' | 'u' ) ( 'M' | 'm' ) ( 'B' | 'b' ) ( 'E' | 'e' ) ( 'R' | 'r' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:517:4: ( 'R' | 'r' ) ( 'O' | 'o' ) ( 'W' | 'w' ) ( 'N' | 'n' ) ( 'U' | 'u' ) ( 'M' | 'm' ) ( 'B' | 'b' ) ( 'E' | 'e' ) ( 'R' | 'r' )
            {
            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='W'||input.LA(1)=='w' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='B'||input.LA(1)=='b' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ROWNUMBER"

    // $ANTLR start "GROUPS"
    public final void mGROUPS() throws RecognitionException {
        try {
            int _type = GROUPS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:518:8: ( ( 'G' | 'g' ) ( 'R' | 'r' ) ( 'O' | 'o' ) ( 'U' | 'u' ) ( 'P' | 'p' ) ( 'S' | 's' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:518:10: ( 'G' | 'g' ) ( 'R' | 'r' ) ( 'O' | 'o' ) ( 'U' | 'u' ) ( 'P' | 'p' ) ( 'S' | 's' )
            {
            if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "GROUPS"

    // $ANTLR start "GETPROMPTVALUE"
    public final void mGETPROMPTVALUE() throws RecognitionException {
        try {
            int _type = GETPROMPTVALUE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:520:2: ( ( 'G' | 'g' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'P' | 'p' ) ( 'R' | 'r' ) ( 'O' | 'o' ) ( 'M' | 'm' ) ( 'P' | 'p' ) ( 'T' | 't' ) ( 'V' | 'v' ) ( 'A' | 'a' ) ( 'L' | 'l' ) ( 'U' | 'u' ) ( 'E' | 'e' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:520:4: ( 'G' | 'g' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'P' | 'p' ) ( 'R' | 'r' ) ( 'O' | 'o' ) ( 'M' | 'm' ) ( 'P' | 'p' ) ( 'T' | 't' ) ( 'V' | 'v' ) ( 'A' | 'a' ) ( 'L' | 'l' ) ( 'U' | 'u' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='V'||input.LA(1)=='v' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "GETPROMPTVALUE"

    // $ANTLR start "GETPROMPTFILTER"
    public final void mGETPROMPTFILTER() throws RecognitionException {
        try {
            int _type = GETPROMPTFILTER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:522:2: ( ( 'G' | 'g' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'P' | 'p' ) ( 'R' | 'r' ) ( 'O' | 'o' ) ( 'M' | 'm' ) ( 'P' | 'p' ) ( 'T' | 't' ) ( 'F' | 'f' ) ( 'I' | 'i' ) ( 'L' | 'l' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'R' | 'r' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:522:4: ( 'G' | 'g' ) ( 'E' | 'e' ) ( 'T' | 't' ) ( 'P' | 'p' ) ( 'R' | 'r' ) ( 'O' | 'o' ) ( 'M' | 'm' ) ( 'P' | 'p' ) ( 'T' | 't' ) ( 'F' | 'f' ) ( 'I' | 'i' ) ( 'L' | 'l' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'R' | 'r' )
            {
            if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "GETPROMPTFILTER"

    // $ANTLR start "TOSTRING"
    public final void mTOSTRING() throws RecognitionException {
        try {
            int _type = TOSTRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:524:2: ( ( 'T' | 't' ) ( 'O' | 'o' ) ( 'S' | 's' ) ( 'T' | 't' ) ( 'R' | 'r' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'G' | 'g' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:524:4: ( 'T' | 't' ) ( 'O' | 'o' ) ( 'S' | 's' ) ( 'T' | 't' ) ( 'R' | 'r' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'G' | 'g' )
            {
            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "TOSTRING"

    // $ANTLR start "SELECTED"
    public final void mSELECTED() throws RecognitionException {
        try {
            int _type = SELECTED;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:527:2: ( ( 'S' | 's' ) ( 'E' | 'e' ) ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'C' | 'c' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'D' | 'd' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:527:4: ( 'S' | 's' ) ( 'E' | 'e' ) ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'C' | 'c' ) ( 'T' | 't' ) ( 'E' | 'e' ) ( 'D' | 'd' )
            {
            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "SELECTED"

    // $ANTLR start "CHILDREN"
    public final void mCHILDREN() throws RecognitionException {
        try {
            int _type = CHILDREN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:529:2: ( ( 'C' | 'c' ) ( 'H' | 'h' ) ( 'I' | 'i' ) ( 'L' | 'l' ) ( 'D' | 'd' ) ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'N' | 'n' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:529:4: ( 'C' | 'c' ) ( 'H' | 'h' ) ( 'I' | 'i' ) ( 'L' | 'l' ) ( 'D' | 'd' ) ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='H'||input.LA(1)=='h' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "CHILDREN"

    // $ANTLR start "ALLCHILDREN"
    public final void mALLCHILDREN() throws RecognitionException {
        try {
            int _type = ALLCHILDREN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:531:2: ( ( 'A' | 'a' ) ( 'L' | 'l' ) ( 'L' | 'l' ) ( 'C' | 'c' ) ( 'H' | 'h' ) ( 'I' | 'i' ) ( 'L' | 'l' ) ( 'D' | 'd' ) ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'N' | 'n' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:531:4: ( 'A' | 'a' ) ( 'L' | 'l' ) ( 'L' | 'l' ) ( 'C' | 'c' ) ( 'H' | 'h' ) ( 'I' | 'i' ) ( 'L' | 'l' ) ( 'D' | 'd' ) ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='H'||input.LA(1)=='h' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ALLCHILDREN"

    // $ANTLR start "LEAVES"
    public final void mLEAVES() throws RecognitionException {
        try {
            int _type = LEAVES;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:533:2: ( ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'A' | 'a' ) ( 'V' | 'v' ) ( 'E' | 'e' ) ( 'S' | 's' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:533:4: ( 'L' | 'l' ) ( 'E' | 'e' ) ( 'A' | 'a' ) ( 'V' | 'v' ) ( 'E' | 'e' ) ( 'S' | 's' )
            {
            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='V'||input.LA(1)=='v' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LEAVES"

    // $ANTLR start "PARENTS"
    public final void mPARENTS() throws RecognitionException {
        try {
            int _type = PARENTS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:535:2: ( ( 'P' | 'p' ) ( 'A' | 'a' ) ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'T' | 't' ) ( 'S' | 's' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:535:4: ( 'P' | 'p' ) ( 'A' | 'a' ) ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'T' | 't' ) ( 'S' | 's' )
            {
            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "PARENTS"

    // $ANTLR start "CUBE"
    public final void mCUBE() throws RecognitionException {
        try {
            int _type = CUBE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:537:2: ( ( 'C' | 'c' ) ( 'U' | 'u' ) ( 'B' | 'b' ) ( 'E' | 'e' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:537:4: ( 'C' | 'c' ) ( 'U' | 'u' ) ( 'B' | 'b' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='B'||input.LA(1)=='b' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "CUBE"

    // $ANTLR start "CURRENTVALUE"
    public final void mCURRENTVALUE() throws RecognitionException {
        try {
            int _type = CURRENTVALUE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:540:2: ( ( 'C' | 'c' ) ( 'U' | 'u' ) ( 'R' | 'r' ) ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'T' | 't' ) ( 'V' | 'v' ) ( 'A' | 'a' ) ( 'L' | 'l' ) ( 'U' | 'u' ) ( 'E' | 'e' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:540:4: ( 'C' | 'c' ) ( 'U' | 'u' ) ( 'R' | 'r' ) ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'N' | 'n' ) ( 'T' | 't' ) ( 'V' | 'v' ) ( 'A' | 'a' ) ( 'L' | 'l' ) ( 'U' | 'u' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='V'||input.LA(1)=='v' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "CURRENTVALUE"

    // $ANTLR start "SIN"
    public final void mSIN() throws RecognitionException {
        try {
            int _type = SIN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:543:5: ( ( 'S' | 's' ) ( 'I' | 'i' ) ( 'N' | 'n' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:543:7: ( 'S' | 's' ) ( 'I' | 'i' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "SIN"

    // $ANTLR start "COS"
    public final void mCOS() throws RecognitionException {
        try {
            int _type = COS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:544:5: ( ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'S' | 's' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:544:7: ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'S' | 's' )
            {
            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "COS"

    // $ANTLR start "TAN"
    public final void mTAN() throws RecognitionException {
        try {
            int _type = TAN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:545:5: ( ( 'T' | 't' ) ( 'A' | 'a' ) ( 'N' | 'n' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:545:7: ( 'T' | 't' ) ( 'A' | 'a' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "TAN"

    // $ANTLR start "ARCSIN"
    public final void mARCSIN() throws RecognitionException {
        try {
            int _type = ARCSIN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:546:8: ( ( 'A' | 'a' ) ( 'R' | 'r' ) ( 'C' | 'c' ) ( 'S' | 's' ) ( 'I' | 'i' ) ( 'N' | 'n' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:546:10: ( 'A' | 'a' ) ( 'R' | 'r' ) ( 'C' | 'c' ) ( 'S' | 's' ) ( 'I' | 'i' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ARCSIN"

    // $ANTLR start "ARCCOS"
    public final void mARCCOS() throws RecognitionException {
        try {
            int _type = ARCCOS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:547:8: ( ( 'A' | 'a' ) ( 'R' | 'r' ) ( 'C' | 'c' ) ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'S' | 's' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:547:10: ( 'A' | 'a' ) ( 'R' | 'r' ) ( 'C' | 'c' ) ( 'C' | 'c' ) ( 'O' | 'o' ) ( 'S' | 's' )
            {
            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ARCCOS"

    // $ANTLR start "ARCTAN"
    public final void mARCTAN() throws RecognitionException {
        try {
            int _type = ARCTAN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:548:8: ( ( 'A' | 'a' ) ( 'R' | 'r' ) ( 'C' | 'c' ) ( 'T' | 't' ) ( 'A' | 'a' ) ( 'N' | 'n' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:548:10: ( 'A' | 'a' ) ( 'R' | 'r' ) ( 'C' | 'c' ) ( 'T' | 't' ) ( 'A' | 'a' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ARCTAN"

    // $ANTLR start "ABS"
    public final void mABS() throws RecognitionException {
        try {
            int _type = ABS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:549:5: ( ( 'A' | 'a' ) ( 'B' | 'b' ) ( 'S' | 's' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:549:7: ( 'A' | 'a' ) ( 'B' | 'b' ) ( 'S' | 's' )
            {
            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='B'||input.LA(1)=='b' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ABS"

    // $ANTLR start "CEILING"
    public final void mCEILING() throws RecognitionException {
        try {
            int _type = CEILING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:550:9: ( ( 'C' | 'c' ) ( 'E' | 'e' ) ( 'I' | 'i' ) ( 'L' | 'l' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'G' | 'g' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:550:11: ( 'C' | 'c' ) ( 'E' | 'e' ) ( 'I' | 'i' ) ( 'L' | 'l' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'G' | 'g' )
            {
            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "CEILING"

    // $ANTLR start "FLOOR"
    public final void mFLOOR() throws RecognitionException {
        try {
            int _type = FLOOR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:551:7: ( ( 'F' | 'f' ) ( 'L' | 'l' ) ( 'O' | 'o' ) ( 'O' | 'o' ) ( 'R' | 'r' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:551:9: ( 'F' | 'f' ) ( 'L' | 'l' ) ( 'O' | 'o' ) ( 'O' | 'o' ) ( 'R' | 'r' )
            {
            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "FLOOR"

    // $ANTLR start "SQRT"
    public final void mSQRT() throws RecognitionException {
        try {
            int _type = SQRT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:552:6: ( ( 'S' | 's' ) ( 'Q' | 'q' ) ( 'R' | 'r' ) ( 'T' | 't' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:552:8: ( 'S' | 's' ) ( 'Q' | 'q' ) ( 'R' | 'r' ) ( 'T' | 't' )
            {
            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='Q'||input.LA(1)=='q' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "SQRT"

    // $ANTLR start "LN"
    public final void mLN() throws RecognitionException {
        try {
            int _type = LN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:553:4: ( ( 'L' | 'l' ) ( 'N' | 'n' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:553:6: ( 'L' | 'l' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LN"

    // $ANTLR start "LOG"
    public final void mLOG() throws RecognitionException {
        try {
            int _type = LOG;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:554:5: ( ( 'L' | 'l' ) ( 'O' | 'o' ) ( 'G' | 'g' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:554:7: ( 'L' | 'l' ) ( 'O' | 'o' ) ( 'G' | 'g' )
            {
            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='G'||input.LA(1)=='g' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LOG"

    // $ANTLR start "EXP"
    public final void mEXP() throws RecognitionException {
        try {
            int _type = EXP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:555:5: ( ( 'E' | 'e' ) ( 'X' | 'x' ) ( 'P' | 'p' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:555:7: ( 'E' | 'e' ) ( 'X' | 'x' ) ( 'P' | 'p' )
            {
            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='X'||input.LA(1)=='x' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "EXP"

    // $ANTLR start "POWER"
    public final void mPOWER() throws RecognitionException {
        try {
            int _type = POWER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:556:7: ( ( 'P' | 'p' ) ( 'O' | 'o' ) ( 'W' | 'w' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:556:9: ( 'P' | 'p' ) ( 'O' | 'o' ) ( 'W' | 'w' )
            {
            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='W'||input.LA(1)=='w' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "POWER"

    // $ANTLR start "RANDOM"
    public final void mRANDOM() throws RecognitionException {
        try {
            int _type = RANDOM;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:557:8: ( ( 'R' | 'r' ) ( 'A' | 'a' ) ( 'N' | 'n' ) ( 'D' | 'd' ) ( 'O' | 'o' ) ( 'M' | 'm' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:557:10: ( 'R' | 'r' ) ( 'A' | 'a' ) ( 'N' | 'n' ) ( 'D' | 'd' ) ( 'O' | 'o' ) ( 'M' | 'm' )
            {
            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "RANDOM"

    // $ANTLR start "ISNAN"
    public final void mISNAN() throws RecognitionException {
        try {
            int _type = ISNAN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:560:7: ( ( 'I' | 'i' ) ( 'S' | 's' ) ( 'N' | 'n' ) ( 'A' | 'a' ) ( 'N' | 'n' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:560:9: ( 'I' | 'i' ) ( 'S' | 's' ) ( 'N' | 'n' ) ( 'A' | 'a' ) ( 'N' | 'n' )
            {
            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ISNAN"

    // $ANTLR start "ISINF"
    public final void mISINF() throws RecognitionException {
        try {
            int _type = ISINF;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:561:7: ( ( 'I' | 'i' ) ( 'S' | 's' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'F' | 'f' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:561:9: ( 'I' | 'i' ) ( 'S' | 's' ) ( 'I' | 'i' ) ( 'N' | 'n' ) ( 'F' | 'f' )
            {
            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ISINF"

    // $ANTLR start "DRANK"
    public final void mDRANK() throws RecognitionException {
        try {
            int _type = DRANK;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:564:7: ( ( 'D' | 'd' ) ( 'R' | 'r' ) ( 'A' | 'a' ) ( 'N' | 'n' ) ( 'K' | 'k' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:564:9: ( 'D' | 'd' ) ( 'R' | 'r' ) ( 'A' | 'a' ) ( 'N' | 'n' ) ( 'K' | 'k' )
            {
            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='K'||input.LA(1)=='k' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DRANK"

    // $ANTLR start "RANK"
    public final void mRANK() throws RecognitionException {
        try {
            int _type = RANK;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:565:6: ( ( 'R' | 'r' ) ( 'A' | 'a' ) ( 'N' | 'n' ) ( 'K' | 'k' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:565:8: ( 'R' | 'r' ) ( 'A' | 'a' ) ( 'N' | 'n' ) ( 'K' | 'k' )
            {
            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='K'||input.LA(1)=='k' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "RANK"

    // $ANTLR start "PTILE"
    public final void mPTILE() throws RecognitionException {
        try {
            int _type = PTILE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:566:7: ( ( 'P' | 'p' ) ( 'T' | 't' ) ( 'I' | 'i' ) ( 'L' | 'l' ) ( 'E' | 'e' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:566:9: ( 'P' | 'p' ) ( 'T' | 't' ) ( 'I' | 'i' ) ( 'L' | 'l' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "PTILE"

    // $ANTLR start "RSUM"
    public final void mRSUM() throws RecognitionException {
        try {
            int _type = RSUM;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:567:6: ( ( 'R' | 'r' ) ( 'S' | 's' ) ( 'U' | 'u' ) ( 'M' | 'm' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:567:8: ( 'R' | 'r' ) ( 'S' | 's' ) ( 'U' | 'u' ) ( 'M' | 'm' )
            {
            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='U'||input.LA(1)=='u' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "RSUM"

    // $ANTLR start "BY"
    public final void mBY() throws RecognitionException {
        try {
            int _type = BY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:569:4: ( ( 'B' | 'b' ) ( 'Y' | 'y' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:569:6: ( 'B' | 'b' ) ( 'Y' | 'y' )
            {
            if ( input.LA(1)=='B'||input.LA(1)=='b' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='Y'||input.LA(1)=='y' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "BY"

    // $ANTLR start "REXP"
    public final void mREXP() throws RecognitionException {
        try {
            int _type = REXP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:571:6: ( ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'X' | 'x' ) ( 'P' | 'p' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:571:8: ( 'R' | 'r' ) ( 'E' | 'e' ) ( 'X' | 'x' ) ( 'P' | 'p' )
            {
            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='X'||input.LA(1)=='x' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "REXP"

    // $ANTLR start "RWRITE"
    public final void mRWRITE() throws RecognitionException {
        try {
            int _type = RWRITE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:572:8: ( ( 'R' | 'r' ) ( 'W' | 'w' ) ( 'R' | 'r' ) ( 'I' | 'i' ) ( 'T' | 't' ) ( 'E' | 'e' ) )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:572:10: ( 'R' | 'r' ) ( 'W' | 'w' ) ( 'R' | 'r' ) ( 'I' | 'i' ) ( 'T' | 't' ) ( 'E' | 'e' )
            {
            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='W'||input.LA(1)=='w' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "RWRITE"

    // $ANTLR start "LINE_COMMENT"
    public final void mLINE_COMMENT() throws RecognitionException {
        try {
            int _type = LINE_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:575:14: ( '//' ( . )* '\\n' )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:575:16: '//' ( . )* '\\n'
            {
            match("//"); 



            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:575:21: ( . )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( (LA19_0=='\n') ) {
                    alt19=2;
                }
                else if ( ((LA19_0 >= '\u0000' && LA19_0 <= '\t')||(LA19_0 >= '\u000B' && LA19_0 <= '\uFFFF')) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:575:21: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);


            match('\n'); 

            _channel=HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LINE_COMMENT"

    // $ANTLR start "ML_COMMENT"
    public final void mML_COMMENT() throws RecognitionException {
        try {
            int _type = ML_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:577:5: ( '/*' ( options {greedy=false; } : . )* '*/' )
            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:577:9: '/*' ( options {greedy=false; } : . )* '*/'
            {
            match("/*"); 



            // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:577:14: ( options {greedy=false; } : . )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( (LA20_0=='*') ) {
                    int LA20_1 = input.LA(2);

                    if ( (LA20_1=='/') ) {
                        alt20=2;
                    }
                    else if ( ((LA20_1 >= '\u0000' && LA20_1 <= '.')||(LA20_1 >= '0' && LA20_1 <= '\uFFFF')) ) {
                        alt20=1;
                    }


                }
                else if ( ((LA20_0 >= '\u0000' && LA20_0 <= ')')||(LA20_0 >= '+' && LA20_0 <= '\uFFFF')) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:577:41: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);


            match("*/"); 



            _channel=HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ML_COMMENT"

    public void mTokens() throws RecognitionException {
        // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:8: ( T__230 | T__231 | T__232 | T__233 | T__234 | INPUT | OUTPUT | COLUMNS | BEGIN | END | SELECT | WRITERECORD | UPDATESFDC | DIM | AS | COMMA | LEFT_PAREN | RIGHT_PAREN | QUALIFIER | FROM | SUM | AVG | COUNT | COUNTDISTINCT | MIN | MAX | DISTINCT | COLUMN_NAME | GRAIN_COLUMN | LEVEL_COLUMN | OLAPCUBE | IDENT_NAME | GRAIN_TABLE | LEVEL_TABLE | POSITION_COLUMN_START | LOGICAL_MEMBER_SET_PREFIX | SPARSE | VARCHARTYPE | INTEGERTYPE | FLOATTYPE | NUMBERTYPE | DATETYPE | DATETIMETYPE | NONETYPE | STRING | INTEGER | FLOAT | DATETIME | BOOLEAN | NULL | PARAMETER | WS | PLUS | MINUS | MULT | DIV | MOD | POW | LT | LTEQ | GT | GTEQ | EQUALS | NOTEQUALS | BITAND | BITOR | IN | NOTIN | LIKE | NOTLIKE | PLUSEQUALS | MINUSEQUALS | NOT | OR | AND | ISNULL | ISNOTNULL | INNERJOIN | FULLOUTERJOIN | LEFTOUTERJOIN | RIGHTOUTERJOIN | USINGOUTERJOIN | UNION | UNIONALL | ON | WHERE | ORDERBY | ASCENDING | DESCENDING | PARTITION | PARALLEL | NULLS_LAST | NULLS_FIRST | DISPLAYWHERE | DISPLAYBY | TOP | IF | THEN | ELSE | ELSEIF | ENDIF | WHILE | ENDWHILE | EXITWHILE | FOR | TO | NEXT | EXITFOR | STEP | COMPLETE | ENDCOMPLETE | LIST | ADD | REMOVEAT | REMOVEALL | MAP | PUT | REMOVEITEM | DELETE | PRINT | HALT | SUBSTRING | LENGTH | POSITION | TOUPPER | TOLOWER | TRIM | FORMAT | REPLACE | REPLACEALL | URLENCODE | TOTIME | DATEPART | DATEDIFF | DATEADD | IIF | IFNULL | DATEFIELD | GETCOLUMNVALUE | GETVARIABLE | SAVEDEXPRESSION | ADDPARENTCHILD | NEXTCHILD | GETLEVELVALUE | GETLEVELATTRIBUTE | GETDAYID | GETWEEKID | GETMONTHID | SETLOOKUP | SETLOOKUPROW | SETFIND | SETSTAT | LET | FUNCTION | RETURN | ENDFUNCTION | FUNCTIONLOOKUP | TRANSFORM | TREND | MDXEXPRESSION | STATMEDIAN | STATSTDDEV | NUMROWS | ROWNUMBER | GROUPS | GETPROMPTVALUE | GETPROMPTFILTER | TOSTRING | SELECTED | CHILDREN | ALLCHILDREN | LEAVES | PARENTS | CUBE | CURRENTVALUE | SIN | COS | TAN | ARCSIN | ARCCOS | ARCTAN | ABS | CEILING | FLOOR | SQRT | LN | LOG | EXP | POWER | RANDOM | ISNAN | ISINF | DRANK | RANK | PTILE | RSUM | BY | REXP | RWRITE | LINE_COMMENT | ML_COMMENT )
        int alt21=201;
        alt21 = dfa21.predict(input);
        switch (alt21) {
            case 1 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:10: T__230
                {
                mT__230(); 


                }
                break;
            case 2 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:17: T__231
                {
                mT__231(); 


                }
                break;
            case 3 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:24: T__232
                {
                mT__232(); 


                }
                break;
            case 4 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:31: T__233
                {
                mT__233(); 


                }
                break;
            case 5 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:38: T__234
                {
                mT__234(); 


                }
                break;
            case 6 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:45: INPUT
                {
                mINPUT(); 


                }
                break;
            case 7 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:51: OUTPUT
                {
                mOUTPUT(); 


                }
                break;
            case 8 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:58: COLUMNS
                {
                mCOLUMNS(); 


                }
                break;
            case 9 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:66: BEGIN
                {
                mBEGIN(); 


                }
                break;
            case 10 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:72: END
                {
                mEND(); 


                }
                break;
            case 11 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:76: SELECT
                {
                mSELECT(); 


                }
                break;
            case 12 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:83: WRITERECORD
                {
                mWRITERECORD(); 


                }
                break;
            case 13 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:95: UPDATESFDC
                {
                mUPDATESFDC(); 


                }
                break;
            case 14 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:106: DIM
                {
                mDIM(); 


                }
                break;
            case 15 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:110: AS
                {
                mAS(); 


                }
                break;
            case 16 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:113: COMMA
                {
                mCOMMA(); 


                }
                break;
            case 17 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:119: LEFT_PAREN
                {
                mLEFT_PAREN(); 


                }
                break;
            case 18 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:130: RIGHT_PAREN
                {
                mRIGHT_PAREN(); 


                }
                break;
            case 19 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:142: QUALIFIER
                {
                mQUALIFIER(); 


                }
                break;
            case 20 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:152: FROM
                {
                mFROM(); 


                }
                break;
            case 21 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:157: SUM
                {
                mSUM(); 


                }
                break;
            case 22 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:161: AVG
                {
                mAVG(); 


                }
                break;
            case 23 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:165: COUNT
                {
                mCOUNT(); 


                }
                break;
            case 24 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:171: COUNTDISTINCT
                {
                mCOUNTDISTINCT(); 


                }
                break;
            case 25 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:185: MIN
                {
                mMIN(); 


                }
                break;
            case 26 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:189: MAX
                {
                mMAX(); 


                }
                break;
            case 27 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:193: DISTINCT
                {
                mDISTINCT(); 


                }
                break;
            case 28 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:202: COLUMN_NAME
                {
                mCOLUMN_NAME(); 


                }
                break;
            case 29 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:214: GRAIN_COLUMN
                {
                mGRAIN_COLUMN(); 


                }
                break;
            case 30 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:227: LEVEL_COLUMN
                {
                mLEVEL_COLUMN(); 


                }
                break;
            case 31 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:240: OLAPCUBE
                {
                mOLAPCUBE(); 


                }
                break;
            case 32 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:249: IDENT_NAME
                {
                mIDENT_NAME(); 


                }
                break;
            case 33 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:260: GRAIN_TABLE
                {
                mGRAIN_TABLE(); 


                }
                break;
            case 34 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:272: LEVEL_TABLE
                {
                mLEVEL_TABLE(); 


                }
                break;
            case 35 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:284: POSITION_COLUMN_START
                {
                mPOSITION_COLUMN_START(); 


                }
                break;
            case 36 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:306: LOGICAL_MEMBER_SET_PREFIX
                {
                mLOGICAL_MEMBER_SET_PREFIX(); 


                }
                break;
            case 37 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:332: SPARSE
                {
                mSPARSE(); 


                }
                break;
            case 38 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:339: VARCHARTYPE
                {
                mVARCHARTYPE(); 


                }
                break;
            case 39 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:351: INTEGERTYPE
                {
                mINTEGERTYPE(); 


                }
                break;
            case 40 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:363: FLOATTYPE
                {
                mFLOATTYPE(); 


                }
                break;
            case 41 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:373: NUMBERTYPE
                {
                mNUMBERTYPE(); 


                }
                break;
            case 42 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:384: DATETYPE
                {
                mDATETYPE(); 


                }
                break;
            case 43 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:393: DATETIMETYPE
                {
                mDATETIMETYPE(); 


                }
                break;
            case 44 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:406: NONETYPE
                {
                mNONETYPE(); 


                }
                break;
            case 45 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:415: STRING
                {
                mSTRING(); 


                }
                break;
            case 46 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:422: INTEGER
                {
                mINTEGER(); 


                }
                break;
            case 47 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:430: FLOAT
                {
                mFLOAT(); 


                }
                break;
            case 48 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:436: DATETIME
                {
                mDATETIME(); 


                }
                break;
            case 49 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:445: BOOLEAN
                {
                mBOOLEAN(); 


                }
                break;
            case 50 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:453: NULL
                {
                mNULL(); 


                }
                break;
            case 51 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:458: PARAMETER
                {
                mPARAMETER(); 


                }
                break;
            case 52 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:468: WS
                {
                mWS(); 


                }
                break;
            case 53 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:471: PLUS
                {
                mPLUS(); 


                }
                break;
            case 54 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:476: MINUS
                {
                mMINUS(); 


                }
                break;
            case 55 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:482: MULT
                {
                mMULT(); 


                }
                break;
            case 56 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:487: DIV
                {
                mDIV(); 


                }
                break;
            case 57 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:491: MOD
                {
                mMOD(); 


                }
                break;
            case 58 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:495: POW
                {
                mPOW(); 


                }
                break;
            case 59 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:499: LT
                {
                mLT(); 


                }
                break;
            case 60 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:502: LTEQ
                {
                mLTEQ(); 


                }
                break;
            case 61 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:507: GT
                {
                mGT(); 


                }
                break;
            case 62 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:510: GTEQ
                {
                mGTEQ(); 


                }
                break;
            case 63 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:515: EQUALS
                {
                mEQUALS(); 


                }
                break;
            case 64 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:522: NOTEQUALS
                {
                mNOTEQUALS(); 


                }
                break;
            case 65 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:532: BITAND
                {
                mBITAND(); 


                }
                break;
            case 66 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:539: BITOR
                {
                mBITOR(); 


                }
                break;
            case 67 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:545: IN
                {
                mIN(); 


                }
                break;
            case 68 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:548: NOTIN
                {
                mNOTIN(); 


                }
                break;
            case 69 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:554: LIKE
                {
                mLIKE(); 


                }
                break;
            case 70 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:559: NOTLIKE
                {
                mNOTLIKE(); 


                }
                break;
            case 71 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:567: PLUSEQUALS
                {
                mPLUSEQUALS(); 


                }
                break;
            case 72 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:578: MINUSEQUALS
                {
                mMINUSEQUALS(); 


                }
                break;
            case 73 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:590: NOT
                {
                mNOT(); 


                }
                break;
            case 74 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:594: OR
                {
                mOR(); 


                }
                break;
            case 75 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:597: AND
                {
                mAND(); 


                }
                break;
            case 76 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:601: ISNULL
                {
                mISNULL(); 


                }
                break;
            case 77 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:608: ISNOTNULL
                {
                mISNOTNULL(); 


                }
                break;
            case 78 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:618: INNERJOIN
                {
                mINNERJOIN(); 


                }
                break;
            case 79 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:628: FULLOUTERJOIN
                {
                mFULLOUTERJOIN(); 


                }
                break;
            case 80 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:642: LEFTOUTERJOIN
                {
                mLEFTOUTERJOIN(); 


                }
                break;
            case 81 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:656: RIGHTOUTERJOIN
                {
                mRIGHTOUTERJOIN(); 


                }
                break;
            case 82 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:671: USINGOUTERJOIN
                {
                mUSINGOUTERJOIN(); 


                }
                break;
            case 83 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:686: UNION
                {
                mUNION(); 


                }
                break;
            case 84 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:692: UNIONALL
                {
                mUNIONALL(); 


                }
                break;
            case 85 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:701: ON
                {
                mON(); 


                }
                break;
            case 86 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:704: WHERE
                {
                mWHERE(); 


                }
                break;
            case 87 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:710: ORDERBY
                {
                mORDERBY(); 


                }
                break;
            case 88 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:718: ASCENDING
                {
                mASCENDING(); 


                }
                break;
            case 89 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:728: DESCENDING
                {
                mDESCENDING(); 


                }
                break;
            case 90 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:739: PARTITION
                {
                mPARTITION(); 


                }
                break;
            case 91 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:749: PARALLEL
                {
                mPARALLEL(); 


                }
                break;
            case 92 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:758: NULLS_LAST
                {
                mNULLS_LAST(); 


                }
                break;
            case 93 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:769: NULLS_FIRST
                {
                mNULLS_FIRST(); 


                }
                break;
            case 94 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:781: DISPLAYWHERE
                {
                mDISPLAYWHERE(); 


                }
                break;
            case 95 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:794: DISPLAYBY
                {
                mDISPLAYBY(); 


                }
                break;
            case 96 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:804: TOP
                {
                mTOP(); 


                }
                break;
            case 97 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:808: IF
                {
                mIF(); 


                }
                break;
            case 98 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:811: THEN
                {
                mTHEN(); 


                }
                break;
            case 99 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:816: ELSE
                {
                mELSE(); 


                }
                break;
            case 100 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:821: ELSEIF
                {
                mELSEIF(); 


                }
                break;
            case 101 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:828: ENDIF
                {
                mENDIF(); 


                }
                break;
            case 102 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:834: WHILE
                {
                mWHILE(); 


                }
                break;
            case 103 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:840: ENDWHILE
                {
                mENDWHILE(); 


                }
                break;
            case 104 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:849: EXITWHILE
                {
                mEXITWHILE(); 


                }
                break;
            case 105 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:859: FOR
                {
                mFOR(); 


                }
                break;
            case 106 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:863: TO
                {
                mTO(); 


                }
                break;
            case 107 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:866: NEXT
                {
                mNEXT(); 


                }
                break;
            case 108 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:871: EXITFOR
                {
                mEXITFOR(); 


                }
                break;
            case 109 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:879: STEP
                {
                mSTEP(); 


                }
                break;
            case 110 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:884: COMPLETE
                {
                mCOMPLETE(); 


                }
                break;
            case 111 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:893: ENDCOMPLETE
                {
                mENDCOMPLETE(); 


                }
                break;
            case 112 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:905: LIST
                {
                mLIST(); 


                }
                break;
            case 113 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:910: ADD
                {
                mADD(); 


                }
                break;
            case 114 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:914: REMOVEAT
                {
                mREMOVEAT(); 


                }
                break;
            case 115 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:923: REMOVEALL
                {
                mREMOVEALL(); 


                }
                break;
            case 116 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:933: MAP
                {
                mMAP(); 


                }
                break;
            case 117 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:937: PUT
                {
                mPUT(); 


                }
                break;
            case 118 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:941: REMOVEITEM
                {
                mREMOVEITEM(); 


                }
                break;
            case 119 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:952: DELETE
                {
                mDELETE(); 


                }
                break;
            case 120 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:959: PRINT
                {
                mPRINT(); 


                }
                break;
            case 121 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:965: HALT
                {
                mHALT(); 


                }
                break;
            case 122 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:970: SUBSTRING
                {
                mSUBSTRING(); 


                }
                break;
            case 123 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:980: LENGTH
                {
                mLENGTH(); 


                }
                break;
            case 124 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:987: POSITION
                {
                mPOSITION(); 


                }
                break;
            case 125 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:996: TOUPPER
                {
                mTOUPPER(); 


                }
                break;
            case 126 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1004: TOLOWER
                {
                mTOLOWER(); 


                }
                break;
            case 127 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1012: TRIM
                {
                mTRIM(); 


                }
                break;
            case 128 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1017: FORMAT
                {
                mFORMAT(); 


                }
                break;
            case 129 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1024: REPLACE
                {
                mREPLACE(); 


                }
                break;
            case 130 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1032: REPLACEALL
                {
                mREPLACEALL(); 


                }
                break;
            case 131 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1043: URLENCODE
                {
                mURLENCODE(); 


                }
                break;
            case 132 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1053: TOTIME
                {
                mTOTIME(); 


                }
                break;
            case 133 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1060: DATEPART
                {
                mDATEPART(); 


                }
                break;
            case 134 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1069: DATEDIFF
                {
                mDATEDIFF(); 


                }
                break;
            case 135 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1078: DATEADD
                {
                mDATEADD(); 


                }
                break;
            case 136 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1086: IIF
                {
                mIIF(); 


                }
                break;
            case 137 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1090: IFNULL
                {
                mIFNULL(); 


                }
                break;
            case 138 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1097: DATEFIELD
                {
                mDATEFIELD(); 


                }
                break;
            case 139 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1107: GETCOLUMNVALUE
                {
                mGETCOLUMNVALUE(); 


                }
                break;
            case 140 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1122: GETVARIABLE
                {
                mGETVARIABLE(); 


                }
                break;
            case 141 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1134: SAVEDEXPRESSION
                {
                mSAVEDEXPRESSION(); 


                }
                break;
            case 142 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1150: ADDPARENTCHILD
                {
                mADDPARENTCHILD(); 


                }
                break;
            case 143 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1165: NEXTCHILD
                {
                mNEXTCHILD(); 


                }
                break;
            case 144 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1175: GETLEVELVALUE
                {
                mGETLEVELVALUE(); 


                }
                break;
            case 145 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1189: GETLEVELATTRIBUTE
                {
                mGETLEVELATTRIBUTE(); 


                }
                break;
            case 146 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1207: GETDAYID
                {
                mGETDAYID(); 


                }
                break;
            case 147 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1216: GETWEEKID
                {
                mGETWEEKID(); 


                }
                break;
            case 148 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1226: GETMONTHID
                {
                mGETMONTHID(); 


                }
                break;
            case 149 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1237: SETLOOKUP
                {
                mSETLOOKUP(); 


                }
                break;
            case 150 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1247: SETLOOKUPROW
                {
                mSETLOOKUPROW(); 


                }
                break;
            case 151 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1260: SETFIND
                {
                mSETFIND(); 


                }
                break;
            case 152 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1268: SETSTAT
                {
                mSETSTAT(); 


                }
                break;
            case 153 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1276: LET
                {
                mLET(); 


                }
                break;
            case 154 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1280: FUNCTION
                {
                mFUNCTION(); 


                }
                break;
            case 155 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1289: RETURN
                {
                mRETURN(); 


                }
                break;
            case 156 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1296: ENDFUNCTION
                {
                mENDFUNCTION(); 


                }
                break;
            case 157 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1308: FUNCTIONLOOKUP
                {
                mFUNCTIONLOOKUP(); 


                }
                break;
            case 158 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1323: TRANSFORM
                {
                mTRANSFORM(); 


                }
                break;
            case 159 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1333: TREND
                {
                mTREND(); 


                }
                break;
            case 160 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1339: MDXEXPRESSION
                {
                mMDXEXPRESSION(); 


                }
                break;
            case 161 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1353: STATMEDIAN
                {
                mSTATMEDIAN(); 


                }
                break;
            case 162 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1364: STATSTDDEV
                {
                mSTATSTDDEV(); 


                }
                break;
            case 163 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1375: NUMROWS
                {
                mNUMROWS(); 


                }
                break;
            case 164 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1383: ROWNUMBER
                {
                mROWNUMBER(); 


                }
                break;
            case 165 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1393: GROUPS
                {
                mGROUPS(); 


                }
                break;
            case 166 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1400: GETPROMPTVALUE
                {
                mGETPROMPTVALUE(); 


                }
                break;
            case 167 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1415: GETPROMPTFILTER
                {
                mGETPROMPTFILTER(); 


                }
                break;
            case 168 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1431: TOSTRING
                {
                mTOSTRING(); 


                }
                break;
            case 169 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1440: SELECTED
                {
                mSELECTED(); 


                }
                break;
            case 170 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1449: CHILDREN
                {
                mCHILDREN(); 


                }
                break;
            case 171 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1458: ALLCHILDREN
                {
                mALLCHILDREN(); 


                }
                break;
            case 172 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1470: LEAVES
                {
                mLEAVES(); 


                }
                break;
            case 173 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1477: PARENTS
                {
                mPARENTS(); 


                }
                break;
            case 174 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1485: CUBE
                {
                mCUBE(); 


                }
                break;
            case 175 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1490: CURRENTVALUE
                {
                mCURRENTVALUE(); 


                }
                break;
            case 176 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1503: SIN
                {
                mSIN(); 


                }
                break;
            case 177 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1507: COS
                {
                mCOS(); 


                }
                break;
            case 178 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1511: TAN
                {
                mTAN(); 


                }
                break;
            case 179 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1515: ARCSIN
                {
                mARCSIN(); 


                }
                break;
            case 180 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1522: ARCCOS
                {
                mARCCOS(); 


                }
                break;
            case 181 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1529: ARCTAN
                {
                mARCTAN(); 


                }
                break;
            case 182 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1536: ABS
                {
                mABS(); 


                }
                break;
            case 183 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1540: CEILING
                {
                mCEILING(); 


                }
                break;
            case 184 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1548: FLOOR
                {
                mFLOOR(); 


                }
                break;
            case 185 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1554: SQRT
                {
                mSQRT(); 


                }
                break;
            case 186 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1559: LN
                {
                mLN(); 


                }
                break;
            case 187 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1562: LOG
                {
                mLOG(); 


                }
                break;
            case 188 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1566: EXP
                {
                mEXP(); 


                }
                break;
            case 189 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1570: POWER
                {
                mPOWER(); 


                }
                break;
            case 190 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1576: RANDOM
                {
                mRANDOM(); 


                }
                break;
            case 191 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1583: ISNAN
                {
                mISNAN(); 


                }
                break;
            case 192 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1589: ISINF
                {
                mISINF(); 


                }
                break;
            case 193 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1595: DRANK
                {
                mDRANK(); 


                }
                break;
            case 194 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1601: RANK
                {
                mRANK(); 


                }
                break;
            case 195 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1606: PTILE
                {
                mPTILE(); 


                }
                break;
            case 196 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1612: RSUM
                {
                mRSUM(); 


                }
                break;
            case 197 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1617: BY
                {
                mBY(); 


                }
                break;
            case 198 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1620: REXP
                {
                mREXP(); 


                }
                break;
            case 199 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1625: RWRITE
                {
                mRWRITE(); 


                }
                break;
            case 200 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1632: LINE_COMMENT
                {
                mLINE_COMMENT(); 


                }
                break;
            case 201 :
                // C:\\gitcurrent\\birst\\SMI\\PerformanceEngine\\minnesota\\src\\com\\successmetricsinc\\transformation\\BirstScript.g:1:1645: ML_COMMENT
                {
                mML_COMMENT(); 


                }
                break;

        }

    }


    protected DFA21 dfa21 = new DFA21(this);
    static final String DFA21_eotS =
        "\4\uffff\1\61\16\uffff\1\132\6\uffff\1\156\2\uffff\1\164\1\uffff"+
        "\1\166\1\170\1\uffff\1\173\1\uffff\1\176\1\u0080\1\uffff\1\u0081"+
        "\1\u0082\1\u0084\10\uffff\1\u009a\1\uffff\1\u009f\2\uffff\1\u0083"+
        "\33\uffff\1\u00ba\34\uffff\1\u00d7\66\uffff\1\u00eb\23\uffff\1\u00f5"+
        "\4\uffff\1\u00fd\1\u00fe\13\uffff\1\u0081\37\uffff\1\u0121\5\uffff"+
        "\1\u012a\23\uffff\1\u0131\1\uffff\1\u0135\21\uffff\1\u013c\10\uffff"+
        "\1\u0141\32\uffff\1\u014f\34\uffff\1\u0167\3\uffff\1\u016d\42\uffff";
    static final String DFA21_eofS =
        "\u0183\uffff";
    static final String DFA21_minS =
        "\1\11\3\uffff\1\133\1\uffff\1\106\1\116\2\105\1\114\1\101\1\105"+
        "\1\116\1\101\1\102\3\uffff\1\60\2\101\1\40\1\uffff\1\105\1\uffff"+
        "\1\56\1\uffff\1\101\1\60\1\uffff\2\75\1\uffff\1\52\1\uffff\2\75"+
        "\1\uffff\1\75\1\46\1\174\1\105\3\101\1\uffff\1\105\2\uffff\1\116"+
        "\1\11\1\116\2\uffff\1\104\1\uffff\1\114\1\uffff\1\102\3\uffff\1"+
        "\104\1\123\1\111\1\103\1\102\1\uffff\1\101\4\uffff\1\105\2\uffff"+
        "\1\111\1\uffff\1\115\1\124\1\114\1\uffff\1\103\2\uffff\1\104\1\uffff"+
        "\1\103\4\uffff\1\117\1\uffff\1\114\1\122\1\uffff\1\116\1\120\2\uffff"+
        "\5\40\1\114\1\116\1\130\1\uffff\1\101\1\114\24\uffff\1\113\1\101"+
        "\1\107\2\uffff\1\115\1\uffff\1\116\2\uffff\1\122\2\uffff\1\123\2"+
        "\uffff\1\124\7\uffff\1\116\4\uffff\1\116\4\uffff\1\11\1\105\1\124"+
        "\1\uffff\1\105\7\uffff\1\117\1\uffff\1\120\1\105\4\uffff\1\120\1"+
        "\103\1\101\1\uffff\1\103\1\115\1\125\2\uffff\2\40\1\uffff\3\40\1"+
        "\102\1\114\1\uffff\1\11\1\124\17\uffff\1\113\1\uffff\1\117\1\114"+
        "\2\uffff\1\104\1\101\2\uffff\1\103\1\117\1\124\1\uffff\1\103\1\111"+
        "\1\11\1\103\1\116\1\uffff\1\114\1\101\7\uffff\1\124\3\uffff\1\40"+
        "\2\uffff\3\40\2\uffff\1\123\1\111\1\103\1\125\1\126\1\101\7\uffff"+
        "\1\105\3\uffff\1\122\2\uffff\1\104\6\uffff\1\106\1\124\2\101\5\uffff"+
        "\1\111\4\40\1\11\5\uffff\1\120\1\105\1\103\1\126\1\117\4\uffff\1"+
        "\105\2\uffff\1\131\1\117\3\40\1\uffff\1\106\1\122\1\101\2\105\1"+
        "\115\2\uffff\1\11\1\116\3\40\5\uffff\1\114\1\uffff\1\101\1\114\1"+
        "\120\1\102\1\114\3\40\4\uffff\1\101\1\124\4\uffff\1\56\2\40\2\uffff"+
        "\1\106\2\uffff\2\40\2\uffff\1\40\1\56\1\40\2\uffff\3\40\1\56";
    static final String DFA21_maxS =
        "\1\175\3\uffff\1\133\1\uffff\1\163\2\165\1\171\1\170\1\165\1\162"+
        "\1\163\1\162\1\166\3\uffff\1\71\1\165\1\157\1\uffff\1\uffff\1\165"+
        "\1\uffff\1\71\1\uffff\1\162\1\71\1\uffff\2\75\1\uffff\1\57\1\uffff"+
        "\1\76\1\75\1\uffff\1\75\1\46\1\174\1\157\1\167\1\165\1\157\1\uffff"+
        "\1\162\2\uffff\1\164\2\156\2\uffff\1\144\1\uffff\1\165\1\uffff\1"+
        "\162\3\uffff\1\144\1\163\1\160\1\154\1\155\1\uffff\1\145\4\uffff"+
        "\1\151\2\uffff\1\151\1\uffff\1\163\1\171\1\163\1\uffff\1\143\2\uffff"+
        "\1\144\1\uffff\1\143\4\uffff\1\157\1\uffff\1\156\1\162\1\uffff\1"+
        "\156\1\170\2\uffff\5\uffff\1\155\1\167\1\170\1\uffff\2\165\24\uffff"+
        "\1\163\1\164\1\157\2\uffff\1\170\1\uffff\1\156\2\uffff\1\162\2\uffff"+
        "\1\167\2\uffff\1\164\7\uffff\1\156\4\uffff\1\156\4\uffff\1\40\1"+
        "\145\1\164\1\uffff\1\145\7\uffff\1\157\1\uffff\1\164\1\145\4\uffff"+
        "\1\160\1\164\1\157\1\uffff\1\143\1\155\1\165\2\uffff\2\uffff\1\uffff"+
        "\3\uffff\1\162\1\154\1\uffff\1\40\1\164\17\uffff\1\153\1\uffff\1"+
        "\157\1\154\2\uffff\1\153\1\164\2\uffff\1\167\1\165\1\164\1\uffff"+
        "\1\167\1\151\1\40\1\143\1\156\1\uffff\1\154\1\164\7\uffff\1\164"+
        "\3\uffff\1\uffff\2\uffff\3\uffff\2\uffff\1\163\1\154\1\143\1\165"+
        "\1\166\1\141\7\uffff\1\145\3\uffff\1\162\2\uffff\1\144\6\uffff\1"+
        "\167\1\164\2\141\5\uffff\1\151\4\uffff\1\40\5\uffff\1\160\1\145"+
        "\1\143\1\166\1\157\4\uffff\1\145\2\uffff\1\171\1\157\3\uffff\1\uffff"+
        "\1\154\1\166\1\151\2\145\1\155\2\uffff\1\40\1\156\3\uffff\5\uffff"+
        "\1\164\1\uffff\1\141\1\154\1\160\1\167\1\154\3\uffff\4\uffff\1\166"+
        "\1\164\4\uffff\1\135\2\uffff\2\uffff\1\166\2\uffff\2\uffff\2\uffff"+
        "\1\uffff\1\135\1\uffff\2\uffff\3\uffff\1\135";
    static final String DFA21_acceptS =
        "\1\uffff\1\1\1\2\1\3\1\uffff\1\5\12\uffff\1\20\1\21\1\22\4\uffff"+
        "\1\46\1\uffff\1\55\1\uffff\1\60\2\uffff\1\64\2\uffff\1\67\1\uffff"+
        "\1\72\2\uffff\1\77\7\uffff\1\u008a\1\uffff\1\43\1\4\3\uffff\1\u0088"+
        "\1\7\1\uffff\1\125\1\uffff\1\u00aa\1\uffff\1\u00b7\1\11\1\u00c5"+
        "\5\uffff\1\45\1\uffff\1\u008d\1\u00b0\1\u00b9\1\14\1\uffff\1\15"+
        "\1\122\1\uffff\1\u0083\3\uffff\1\u00c1\1\uffff\1\26\1\113\1\uffff"+
        "\1\u00ab\1\uffff\1\u00b6\1\23\1\57\1\24\1\uffff\1\61\2\uffff\1\u0097"+
        "\2\uffff\1\u00a0\1\u00a1\10\uffff\1\56\2\uffff\1\142\1\u00b2\1\63"+
        "\1\71\1\107\1\65\1\110\1\66\1\u00c8\1\u00c9\1\70\1\74\1\100\1\73"+
        "\1\76\1\75\1\111\1\101\1\112\1\102\3\uffff\1\u00ba\1\121\1\uffff"+
        "\1\u00a4\1\uffff\1\u00c4\1\u00c7\1\uffff\1\165\1\170\1\uffff\1\u00c3"+
        "\1\171\1\uffff\1\u00a5\1\6\1\47\1\116\1\103\1\u00bf\1\u00c0\1\uffff"+
        "\1\u0089\1\141\1\127\1\10\1\uffff\1\156\1\u00b1\1\u00ae\1\u00af"+
        "\3\uffff\1\u00bc\1\uffff\1\25\1\172\1\155\1\u0098\1\u00a2\1\126"+
        "\1\146\1\uffff\1\16\2\uffff\1\131\1\167\1\130\1\17\3\uffff\1\117"+
        "\3\uffff\1\32\1\164\2\uffff\1\40\5\uffff\1\54\2\uffff\1\177\1\u009e"+
        "\1\u009f\1\140\1\175\1\176\1\u0084\1\u00a8\1\152\1\105\1\160\1\120"+
        "\1\173\1\u0099\1\u00ac\1\uffff\1\u00bb\2\uffff\1\u009b\1\u00c6\2"+
        "\uffff\1\174\1\u00bd\3\uffff\1\12\5\uffff\1\33\2\uffff\1\u008e\1"+
        "\161\1\u00b3\1\u00b4\1\u00b5\1\50\1\u00b8\1\uffff\1\u0080\1\151"+
        "\1\31\1\uffff\1\44\1\34\3\uffff\1\51\1\u00a3\6\uffff\1\u00be\1\u00c2"+
        "\1\132\1\133\1\u00ad\1\u008b\1\u008c\1\uffff\1\u0092\1\u0093\1\u0094"+
        "\1\uffff\1\114\1\115\1\uffff\1\145\1\147\1\157\1\u009c\1\144\1\143"+
        "\4\uffff\1\53\1\u0085\1\u0086\1\u0087\1\52\6\uffff\1\62\1\104\1"+
        "\106\1\u008f\1\153\5\uffff\1\30\1\27\1\150\1\154\1\uffff\1\124\1"+
        "\123\5\uffff\1\37\6\uffff\1\u00a9\1\13\5\uffff\1\37\1\134\1\135"+
        "\1\u0095\1\u0096\1\uffff\1\166\10\uffff\1\162\1\163\1\u0082\1\u0081"+
        "\2\uffff\1\136\1\137\1\u009d\1\u009a\3\uffff\1\u0090\1\u0091\1\uffff"+
        "\1\35\1\41\2\uffff\1\u00a6\1\u00a7\3\uffff\1\36\1\42\4\uffff";
    static final String DFA21_specialS =
        "\u0183\uffff}>";
    static final String[] DFA21_transitionS = {
            "\2\36\1\uffff\2\36\22\uffff\1\36\1\47\1\uffff\1\33\1\uffff\1"+
            "\35\1\50\1\31\1\21\1\22\1\41\1\37\1\20\1\40\1\23\1\42\12\32"+
            "\1\1\1\uffff\1\44\1\46\1\45\1\uffff\1\2\1\17\1\11\1\10\1\16"+
            "\1\12\1\24\1\57\1\55\1\6\2\uffff\1\52\1\25\1\30\1\7\1\54\1\56"+
            "\1\53\1\13\1\34\1\15\1\27\1\14\1\uffff\1\56\1\uffff\1\26\1\uffff"+
            "\1\3\1\43\2\uffff\1\17\1\11\1\10\1\16\1\12\1\24\1\57\1\55\1"+
            "\6\2\uffff\1\52\1\25\1\30\1\7\1\54\1\56\1\53\1\13\1\34\1\15"+
            "\1\27\1\14\1\uffff\1\56\1\uffff\1\4\1\51\1\5",
            "",
            "",
            "",
            "\1\60",
            "",
            "\1\64\2\uffff\1\65\4\uffff\1\62\4\uffff\1\63\22\uffff\1\64"+
            "\2\uffff\1\65\4\uffff\1\62\4\uffff\1\63",
            "\1\70\3\uffff\1\67\2\uffff\1\66\30\uffff\1\70\3\uffff\1\67"+
            "\2\uffff\1\66",
            "\1\74\2\uffff\1\72\6\uffff\1\71\5\uffff\1\73\17\uffff\1\74"+
            "\2\uffff\1\72\6\uffff\1\71\5\uffff\1\73",
            "\1\75\23\uffff\1\76\13\uffff\1\75\23\uffff\1\76",
            "\1\100\1\uffff\1\77\11\uffff\1\101\23\uffff\1\100\1\uffff\1"+
            "\77\11\uffff\1\101",
            "\1\106\3\uffff\1\102\3\uffff\1\107\6\uffff\1\104\1\110\2\uffff"+
            "\1\105\1\103\13\uffff\1\106\3\uffff\1\102\3\uffff\1\107\6\uffff"+
            "\1\104\1\110\2\uffff\1\105\1\103",
            "\1\56\2\uffff\1\112\11\uffff\1\111\22\uffff\1\56\2\uffff\1"+
            "\112\11\uffff\1\111",
            "\1\115\1\uffff\1\113\1\uffff\1\116\1\114\32\uffff\1\115\1\uffff"+
            "\1\113\1\uffff\1\116\1\114",
            "\1\120\3\uffff\1\121\3\uffff\1\117\10\uffff\1\122\16\uffff"+
            "\1\120\3\uffff\1\121\3\uffff\1\117\10\uffff\1\122",
            "\1\131\1\uffff\1\126\7\uffff\1\127\1\uffff\1\125\3\uffff\1"+
            "\130\1\123\2\uffff\1\124\13\uffff\1\131\1\uffff\1\126\7\uffff"+
            "\1\127\1\uffff\1\125\3\uffff\1\130\1\123\2\uffff\1\124",
            "",
            "",
            "",
            "\12\133",
            "\1\136\7\uffff\1\141\2\uffff\1\135\2\uffff\1\140\2\uffff\1"+
            "\134\2\uffff\1\137\13\uffff\1\136\7\uffff\1\141\2\uffff\1\135"+
            "\2\uffff\1\140\2\uffff\1\134\2\uffff\1\137",
            "\1\143\2\uffff\1\144\1\145\3\uffff\1\142\5\uffff\1\56\21\uffff"+
            "\1\143\2\uffff\1\144\1\145\3\uffff\1\142\5\uffff\1\56",
            "\2\152\1\uffff\4\152\5\uffff\2\152\1\uffff\16\152\1\uffff\1"+
            "\152\1\uffff\3\152\1\151\3\152\1\146\4\152\1\150\1\147\15\152"+
            "\3\uffff\2\152\1\uffff\2\152\1\151\3\152\1\146\4\152\1\150\1"+
            "\147\15\152\5\uffff\uff80\152",
            "",
            "\1\155\11\uffff\1\154\5\uffff\1\153\17\uffff\1\155\11\uffff"+
            "\1\154\5\uffff\1\153",
            "",
            "\1\133\1\uffff\12\32",
            "",
            "\1\162\6\uffff\1\161\6\uffff\1\160\2\uffff\1\157\16\uffff\1"+
            "\162\6\uffff\1\161\6\uffff\1\160\2\uffff\1\157",
            "\12\163",
            "",
            "\1\165",
            "\1\167",
            "",
            "\1\172\4\uffff\1\171",
            "",
            "\1\174\1\175",
            "\1\177",
            "",
            "\1\175",
            "\1\125",
            "\1\u0083",
            "\1\u0086\3\uffff\1\u0085\4\uffff\1\u0088\1\u0087\25\uffff\1"+
            "\u0086\3\uffff\1\u0085\4\uffff\1\u0088\1\u0087",
            "\1\u008c\3\uffff\1\u008a\3\uffff\1\u0089\5\uffff\1\u008b\3"+
            "\uffff\1\u008d\3\uffff\1\u008e\11\uffff\1\u008c\3\uffff\1\u008a"+
            "\3\uffff\1\u0089\5\uffff\1\u008b\3\uffff\1\u008d\3\uffff\1\u008e",
            "\1\u008f\15\uffff\1\u0092\2\uffff\1\u0091\1\uffff\1\u0093\1"+
            "\u0090\13\uffff\1\u008f\15\uffff\1\u0092\2\uffff\1\u0091\1\uffff"+
            "\1\u0093\1\u0090",
            "\1\u0094\15\uffff\1\56\21\uffff\1\u0094\15\uffff\1\56",
            "",
            "\1\u0095\14\uffff\1\u0096\22\uffff\1\u0095\14\uffff\1\u0096",
            "",
            "",
            "\1\u0099\1\uffff\1\u0097\3\uffff\1\u0098\31\uffff\1\u0099\1"+
            "\uffff\1\u0097\3\uffff\1\u0098",
            "\2\u009d\1\uffff\2\u009d\22\uffff\1\u009d\50\uffff\1\u009c"+
            "\4\uffff\1\u009b\32\uffff\1\u009c\4\uffff\1\u009b",
            "\1\u009e\37\uffff\1\u009e",
            "",
            "",
            "\1\u00a0\37\uffff\1\u00a0",
            "",
            "\1\u00a1\1\u00a3\5\uffff\1\u00a4\1\uffff\1\u00a2\26\uffff\1"+
            "\u00a1\1\u00a3\5\uffff\1\u00a4\1\uffff\1\u00a2",
            "",
            "\1\u00a5\17\uffff\1\u00a6\17\uffff\1\u00a5\17\uffff\1\u00a6",
            "",
            "",
            "",
            "\1\u00a7\37\uffff\1\u00a7",
            "\1\u00a8\37\uffff\1\u00a8",
            "\1\u00a9\6\uffff\1\u00aa\30\uffff\1\u00a9\6\uffff\1\u00aa",
            "\1\56\10\uffff\1\u00ab\26\uffff\1\56\10\uffff\1\u00ab",
            "\1\u00ad\12\uffff\1\u00ac\24\uffff\1\u00ad\12\uffff\1\u00ac",
            "",
            "\1\u00af\2\uffff\1\u00b0\1\u00ae\33\uffff\1\u00af\2\uffff\1"+
            "\u00b0\1\u00ae",
            "",
            "",
            "",
            "",
            "\1\u00b1\3\uffff\1\u00b2\33\uffff\1\u00b1\3\uffff\1\u00b2",
            "",
            "",
            "\1\u00b3\37\uffff\1\u00b3",
            "",
            "\1\u00b4\5\uffff\1\u00b5\31\uffff\1\u00b4\5\uffff\1\u00b5",
            "\1\u00b6\4\uffff\1\56\32\uffff\1\u00b6\4\uffff\1\56",
            "\1\u00b8\6\uffff\1\u00b7\30\uffff\1\u00b8\6\uffff\1\u00b7",
            "",
            "\1\u00b9\37\uffff\1\u00b9",
            "",
            "",
            "\1\u00bb\37\uffff\1\u00bb",
            "",
            "\1\u00bc\37\uffff\1\u00bc",
            "",
            "",
            "",
            "",
            "\1\u00bd\37\uffff\1\u00bd",
            "",
            "\1\u00be\1\uffff\1\u00bf\35\uffff\1\u00be\1\uffff\1\u00bf",
            "\1\u00c0\37\uffff\1\u00c0",
            "",
            "\1\u00c1\37\uffff\1\u00c1",
            "\1\u00c3\7\uffff\1\u00c2\27\uffff\1\u00c3\7\uffff\1\u00c2",
            "",
            "",
            "\2\152\1\uffff\4\152\5\uffff\2\152\1\u00c5\16\152\1\uffff\1"+
            "\152\1\uffff\22\152\1\u00c4\10\152\2\uffff\1\u00c6\2\152\1\uffff"+
            "\21\152\1\u00c4\10\152\5\uffff\uff80\152",
            "\2\152\1\uffff\4\152\5\uffff\2\152\1\u00c5\16\152\1\uffff\1"+
            "\152\1\uffff\5\152\1\u00c7\25\152\2\uffff\1\u00c6\2\152\1\uffff"+
            "\4\152\1\u00c7\25\152\5\uffff\uff80\152",
            "\2\152\1\uffff\4\152\5\uffff\2\152\1\u00c5\16\152\1\uffff\1"+
            "\152\1\uffff\5\152\1\u00c8\25\152\2\uffff\1\u00c6\2\152\1\uffff"+
            "\4\152\1\u00c8\25\152\5\uffff\uff80\152",
            "\2\152\1\uffff\4\152\5\uffff\2\152\1\u00c5\16\152\1\uffff\1"+
            "\152\1\uffff\25\152\1\u00c9\5\152\2\uffff\1\u00c6\2\152\1\uffff"+
            "\24\152\1\u00c9\5\152\5\uffff\uff80\152",
            "\2\152\1\uffff\4\152\5\uffff\2\152\1\u00c5\16\152\1\uffff\1"+
            "\152\1\uffff\33\152\2\uffff\1\u00c6\2\152\1\uffff\32\152\5\uffff"+
            "\uff80\152",
            "\1\u00cb\1\u00ca\36\uffff\1\u00cb\1\u00ca",
            "\1\u00cc\5\uffff\1\u00cd\2\uffff\1\33\26\uffff\1\u00cc\5\uffff"+
            "\1\u00cd\2\uffff\1\33",
            "\1\u00ce\37\uffff\1\u00ce",
            "",
            "\1\u00d0\3\uffff\1\u00d1\3\uffff\1\u00cf\13\uffff\1\136\13"+
            "\uffff\1\u00d0\3\uffff\1\u00d1\3\uffff\1\u00cf\13\uffff\1\136",
            "\1\u00d4\3\uffff\1\u00d2\2\uffff\1\u00d6\1\u00d5\1\u00d3\26"+
            "\uffff\1\u00d4\3\uffff\1\u00d2\2\uffff\1\u00d6\1\u00d5\1\u00d3",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\u00d8\7\uffff\1\u00d9\27\uffff\1\u00d8\7\uffff\1\u00d9",
            "\1\u00dd\4\uffff\1\u00da\7\uffff\1\u00db\5\uffff\1\u00dc\14"+
            "\uffff\1\u00dd\4\uffff\1\u00da\7\uffff\1\u00db\5\uffff\1\u00dc",
            "\1\u00df\7\uffff\1\u00de\27\uffff\1\u00df\7\uffff\1\u00de",
            "",
            "",
            "\1\u00e0\2\uffff\1\u00e1\3\uffff\1\u00e2\3\uffff\1\u00e3\24"+
            "\uffff\1\u00e0\2\uffff\1\u00e1\3\uffff\1\u00e2\3\uffff\1\u00e3",
            "",
            "\1\u00e4\37\uffff\1\u00e4",
            "",
            "",
            "\1\u00e5\37\uffff\1\u00e5",
            "",
            "",
            "\1\u00e6\3\uffff\1\u00e7\33\uffff\1\u00e6\3\uffff\1\u00e7",
            "",
            "",
            "\1\u00e8\37\uffff\1\u00e8",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\u00e9\37\uffff\1\u00e9",
            "",
            "",
            "",
            "",
            "\1\u00ea\37\uffff\1\u00ea",
            "",
            "",
            "",
            "",
            "\2\u00ec\1\uffff\2\u00ec\22\uffff\1\u00ec",
            "\1\u00ed\37\uffff\1\u00ed",
            "\1\u00ee\37\uffff\1\u00ee",
            "",
            "\1\u00ef\37\uffff\1\u00ef",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\u00f0\37\uffff\1\u00f0",
            "",
            "\1\u00f2\3\uffff\1\u00f1\33\uffff\1\u00f2\3\uffff\1\u00f1",
            "\1\u00f3\37\uffff\1\u00f3",
            "",
            "",
            "",
            "",
            "\1\u00f4\37\uffff\1\u00f4",
            "\1\u00f7\17\uffff\1\u00f6\1\u00f8\16\uffff\1\u00f7\17\uffff"+
            "\1\u00f6\1\u00f8",
            "\1\u00f9\15\uffff\1\u00fa\21\uffff\1\u00f9\15\uffff\1\u00fa",
            "",
            "\1\u00fb\37\uffff\1\u00fb",
            "\1\u00fc\37\uffff\1\u00fc",
            "\1\56\37\uffff\1\56",
            "",
            "",
            "\2\152\1\uffff\4\152\5\uffff\2\152\1\u00c5\16\152\1\uffff\1"+
            "\152\1\uffff\1\152\1\u00ff\31\152\2\uffff\1\u00c6\2\152\1\uffff"+
            "\1\u00ff\31\152\5\uffff\uff80\152",
            "\2\u0101\1\uffff\4\u0101\5\uffff\2\u0101\1\uffff\16\u0101\1"+
            "\uffff\1\u0101\1\uffff\33\u0101\3\uffff\2\u0101\1\uffff\32\u0101"+
            "\1\u0100\4\uffff\uff80\u0101",
            "",
            "\2\152\1\uffff\4\152\5\uffff\2\152\1\u00c5\16\152\1\uffff\1"+
            "\152\1\uffff\1\152\1\u0102\31\152\2\uffff\1\u00c6\2\152\1\uffff"+
            "\1\u0102\31\152\5\uffff\uff80\152",
            "\2\152\1\uffff\4\152\5\uffff\2\152\1\u00c5\16\152\1\uffff\1"+
            "\152\1\uffff\26\152\1\u0103\4\152\2\uffff\1\u00c6\2\152\1\uffff"+
            "\25\152\1\u0103\4\152\5\uffff\uff80\152",
            "\2\152\1\uffff\4\152\5\uffff\2\152\1\u00c5\16\152\1\uffff\1"+
            "\152\1\uffff\2\152\1\u0104\30\152\2\uffff\1\u00c6\2\152\1\uffff"+
            "\1\152\1\u0104\30\152\5\uffff\uff80\152",
            "\1\u0105\17\uffff\1\u0106\17\uffff\1\u0105\17\uffff\1\u0106",
            "\1\u0107\37\uffff\1\u0107",
            "",
            "\2\u0108\1\uffff\2\u0108\22\uffff\1\u0108",
            "\1\u0109\37\uffff\1\u0109",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\u010a\37\uffff\1\u010a",
            "",
            "\1\u010b\37\uffff\1\u010b",
            "\1\u010c\37\uffff\1\u010c",
            "",
            "",
            "\1\u010d\6\uffff\1\u010e\30\uffff\1\u010d\6\uffff\1\u010e",
            "\1\u0110\3\uffff\1\u0111\16\uffff\1\u010f\14\uffff\1\u0110"+
            "\3\uffff\1\u0111\16\uffff\1\u010f",
            "",
            "",
            "\1\u0112\1\u0115\7\uffff\1\u0114\1\u0117\2\uffff\1\u0118\5"+
            "\uffff\1\u0113\1\u0116\13\uffff\1\u0112\1\u0115\7\uffff\1\u0114"+
            "\1\u0117\2\uffff\1\u0118\5\uffff\1\u0113\1\u0116",
            "\1\u011a\5\uffff\1\u0119\31\uffff\1\u011a\5\uffff\1\u0119",
            "\1\u011b\37\uffff\1\u011b",
            "",
            "\1\u011e\2\uffff\1\u011f\2\uffff\1\u011c\15\uffff\1\u011d\13"+
            "\uffff\1\u011e\2\uffff\1\u011f\2\uffff\1\u011c\15\uffff\1\u011d",
            "\1\u0120\37\uffff\1\u0120",
            "\2\u0122\1\uffff\2\u0122\22\uffff\1\u0122",
            "\1\u0123\37\uffff\1\u0123",
            "\1\u0124\37\uffff\1\u0124",
            "",
            "\1\u0125\37\uffff\1\u0125",
            "\1\u0129\2\uffff\1\u0128\13\uffff\1\u0127\3\uffff\1\u0126\14"+
            "\uffff\1\u0129\2\uffff\1\u0128\13\uffff\1\u0127\3\uffff\1\u0126",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\u012b\37\uffff\1\u012b",
            "",
            "",
            "",
            "\2\152\1\uffff\4\152\5\uffff\2\152\1\u00c5\16\152\1\uffff\1"+
            "\152\1\uffff\11\152\1\u012c\21\152\2\uffff\1\u00c6\2\152\1\uffff"+
            "\10\152\1\u012c\21\152\5\uffff\uff80\152",
            "",
            "",
            "\2\152\1\uffff\4\152\5\uffff\2\152\1\u00c5\16\152\1\uffff\1"+
            "\152\1\uffff\23\152\1\u012d\7\152\2\uffff\1\u00c6\2\152\1\uffff"+
            "\22\152\1\u012d\7\152\5\uffff\uff80\152",
            "\2\152\1\uffff\4\152\5\uffff\2\152\1\u00c5\16\152\1\uffff\1"+
            "\152\1\uffff\5\152\1\u012e\25\152\2\uffff\1\u00c6\2\152\1\uffff"+
            "\4\152\1\u012e\25\152\5\uffff\uff80\152",
            "\2\152\1\uffff\4\152\5\uffff\2\152\1\u00c5\16\152\1\uffff\1"+
            "\152\1\uffff\5\152\1\u012f\25\152\2\uffff\1\u00c6\2\152\1\uffff"+
            "\4\152\1\u012f\25\152\5\uffff\uff80\152",
            "",
            "",
            "\1\u0130\37\uffff\1\u0130",
            "\1\u0132\2\uffff\1\u0133\34\uffff\1\u0132\2\uffff\1\u0133",
            "\1\u0134\37\uffff\1\u0134",
            "\1\u0136\37\uffff\1\u0136",
            "\1\u0137\37\uffff\1\u0137",
            "\1\u0138\37\uffff\1\u0138",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\u0139\37\uffff\1\u0139",
            "",
            "",
            "",
            "\1\u013a\37\uffff\1\u013a",
            "",
            "",
            "\1\u013b\37\uffff\1\u013b",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\u013e\20\uffff\1\u013d\16\uffff\1\u013e\20\uffff\1\u013d",
            "\1\u013f\37\uffff\1\u013f",
            "\1\u0140\37\uffff\1\u0140",
            "\1\u0142\37\uffff\1\u0142",
            "",
            "",
            "",
            "",
            "",
            "\1\u0143\37\uffff\1\u0143",
            "\2\152\1\uffff\4\152\5\uffff\2\152\1\u00c5\16\152\1\uffff\1"+
            "\152\1\uffff\16\152\1\u0144\14\152\2\uffff\1\u00c6\2\152\1\uffff"+
            "\15\152\1\u0144\14\152\5\uffff\uff80\152",
            "\2\152\1\uffff\4\152\5\uffff\2\152\1\u00c5\16\152\1\uffff\1"+
            "\152\1\uffff\25\152\1\u0145\5\152\2\uffff\1\u00c6\2\152\1\uffff"+
            "\24\152\1\u0145\5\152\5\uffff\uff80\152",
            "\2\152\1\uffff\4\152\5\uffff\2\152\1\u00c5\16\152\1\uffff\1"+
            "\152\1\uffff\14\152\1\u0146\16\152\2\uffff\1\u00c6\2\152\1\uffff"+
            "\13\152\1\u0146\16\152\5\uffff\uff80\152",
            "\2\152\1\uffff\4\152\5\uffff\2\152\1\u00c5\16\152\1\uffff\1"+
            "\152\1\uffff\33\152\2\uffff\1\u0147\2\152\1\uffff\32\152\5\uffff"+
            "\uff80\152",
            "\2\u0148\1\uffff\2\u0148\22\uffff\1\u0148",
            "",
            "",
            "",
            "",
            "",
            "\1\u0149\37\uffff\1\u0149",
            "\1\u014a\37\uffff\1\u014a",
            "\1\u014b\37\uffff\1\u014b",
            "\1\u014c\37\uffff\1\u014c",
            "\1\u014d\37\uffff\1\u014d",
            "",
            "",
            "",
            "",
            "\1\u014e\37\uffff\1\u014e",
            "",
            "",
            "\1\u0150\37\uffff\1\u0150",
            "\1\u0151\37\uffff\1\u0151",
            "\2\152\1\uffff\4\152\1\uffff\1\u0152\3\uffff\2\152\1\u00c5"+
            "\16\152\1\uffff\1\152\1\uffff\33\152\2\uffff\1\u00c6\2\152\1"+
            "\uffff\32\152\5\uffff\uff80\152",
            "\2\152\1\uffff\4\152\5\uffff\2\152\1\u00c5\16\152\1\uffff\1"+
            "\152\1\uffff\22\152\1\u0153\10\152\2\uffff\1\u00c6\2\152\1\uffff"+
            "\21\152\1\u0153\10\152\5\uffff\uff80\152",
            "\2\152\1\uffff\4\152\1\uffff\1\u0154\3\uffff\2\152\1\u00c5"+
            "\16\152\1\uffff\1\152\1\uffff\33\152\2\uffff\1\u00c6\2\152\1"+
            "\uffff\32\152\5\uffff\uff80\152",
            "",
            "\1\u0157\5\uffff\1\u0156\31\uffff\1\u0157\5\uffff\1\u0156",
            "\1\u0159\3\uffff\1\u0158\33\uffff\1\u0159\3\uffff\1\u0158",
            "\1\u015a\7\uffff\1\u015b\27\uffff\1\u015a\7\uffff\1\u015b",
            "\1\u015c\37\uffff\1\u015c",
            "\1\u015d\37\uffff\1\u015d",
            "\1\u015e\37\uffff\1\u015e",
            "",
            "",
            "\2\u015f\1\uffff\2\u015f\22\uffff\1\u015f",
            "\1\u0160\37\uffff\1\u0160",
            "\2\u0161\1\uffff\4\u0161\5\uffff\2\u0161\1\uffff\16\u0161\1"+
            "\uffff\1\u0161\1\uffff\33\u0161\3\uffff\2\u0161\1\uffff\32\u0161"+
            "\5\uffff\uff80\u0161",
            "\2\152\1\uffff\4\152\5\uffff\2\152\1\u00c5\16\152\1\uffff\1"+
            "\152\1\uffff\5\152\1\u0162\25\152\2\uffff\1\u00c6\2\152\1\uffff"+
            "\4\152\1\u0162\25\152\5\uffff\uff80\152",
            "\2\u0163\1\uffff\4\u0163\5\uffff\2\u0163\1\uffff\16\u0163\1"+
            "\uffff\1\u0163\1\uffff\33\u0163\3\uffff\2\u0163\1\uffff\32\u0163"+
            "\5\uffff\uff80\u0163",
            "",
            "",
            "",
            "",
            "",
            "\1\u0165\7\uffff\1\u0164\27\uffff\1\u0165\7\uffff\1\u0164",
            "",
            "\1\u0166\37\uffff\1\u0166",
            "\1\u0168\37\uffff\1\u0168",
            "\1\u0169\37\uffff\1\u0169",
            "\1\u016b\24\uffff\1\u016a\12\uffff\1\u016b\24\uffff\1\u016a",
            "\1\u016c\37\uffff\1\u016c",
            "\2\u0161\1\uffff\4\u0161\2\uffff\1\u016e\2\uffff\2\u0161\1"+
            "\uffff\16\u0161\1\uffff\1\u0161\1\uffff\33\u0161\3\uffff\2\u0161"+
            "\1\uffff\32\u0161\5\uffff\uff80\u0161",
            "\2\152\1\uffff\4\152\5\uffff\2\152\1\u00c5\16\152\1\uffff\1"+
            "\152\1\uffff\24\152\1\u016f\6\152\2\uffff\1\u00c6\2\152\1\uffff"+
            "\23\152\1\u016f\6\152\5\uffff\uff80\152",
            "\2\u0163\1\uffff\4\u0163\5\uffff\2\u0163\1\u0170\16\u0163\1"+
            "\uffff\1\u0163\1\uffff\33\u0163\3\uffff\2\u0163\1\uffff\32\u0163"+
            "\5\uffff\uff80\u0163",
            "",
            "",
            "",
            "",
            "\1\u0172\24\uffff\1\u0171\12\uffff\1\u0172\24\uffff\1\u0171",
            "\1\u0173\37\uffff\1\u0173",
            "",
            "",
            "",
            "",
            "\1\u0174\56\uffff\1\u0175",
            "\2\152\1\uffff\4\152\5\uffff\2\152\1\u00c5\16\152\1\uffff\1"+
            "\152\1\uffff\1\152\1\u0176\31\152\2\uffff\1\u00c6\2\152\1\uffff"+
            "\1\u0176\31\152\5\uffff\uff80\152",
            "\2\u0177\1\uffff\4\u0177\5\uffff\2\u0177\1\uffff\16\u0177\1"+
            "\uffff\1\u0177\1\uffff\33\u0177\3\uffff\2\u0177\1\uffff\32\u0177"+
            "\5\uffff\uff80\u0177",
            "",
            "",
            "\1\u0179\17\uffff\1\u0178\17\uffff\1\u0179\17\uffff\1\u0178",
            "",
            "",
            "\2\152\1\uffff\4\152\5\uffff\2\152\1\u00c5\16\152\1\uffff\1"+
            "\152\1\uffff\2\152\1\u017a\30\152\2\uffff\1\u00c6\2\152\1\uffff"+
            "\1\152\1\u017a\30\152\5\uffff\uff80\152",
            "\2\u0177\1\uffff\4\u0177\2\uffff\1\u017b\2\uffff\2\u0177\1"+
            "\uffff\16\u0177\1\uffff\1\u0177\1\uffff\33\u0177\3\uffff\2\u0177"+
            "\1\uffff\32\u0177\5\uffff\uff80\u0177",
            "",
            "",
            "\2\152\1\uffff\4\152\5\uffff\2\152\1\u00c5\16\152\1\uffff\1"+
            "\152\1\uffff\14\152\1\u017c\16\152\2\uffff\1\u00c6\2\152\1\uffff"+
            "\13\152\1\u017c\16\152\5\uffff\uff80\152",
            "\1\u017d\56\uffff\1\u017e",
            "\2\152\1\uffff\4\152\5\uffff\2\152\1\u00c5\16\152\1\uffff\1"+
            "\152\1\uffff\5\152\1\u017f\25\152\2\uffff\1\u00c6\2\152\1\uffff"+
            "\4\152\1\u017f\25\152\5\uffff\uff80\152",
            "",
            "",
            "\2\152\1\uffff\4\152\1\uffff\1\u0180\3\uffff\2\152\1\u00c5"+
            "\16\152\1\uffff\1\152\1\uffff\33\152\2\uffff\1\u00c6\2\152\1"+
            "\uffff\32\152\5\uffff\uff80\152",
            "\2\u0181\1\uffff\4\u0181\5\uffff\2\u0181\1\uffff\16\u0181\1"+
            "\uffff\1\u0181\1\uffff\33\u0181\3\uffff\2\u0181\1\uffff\32\u0181"+
            "\5\uffff\uff80\u0181",
            "\2\u0181\1\uffff\4\u0181\2\uffff\1\u0182\2\uffff\2\u0181\1"+
            "\uffff\16\u0181\1\uffff\1\u0181\1\uffff\33\u0181\3\uffff\2\u0181"+
            "\1\uffff\32\u0181\5\uffff\uff80\u0181",
            "\1\u0174\56\uffff\1\u0175"
    };

    static final short[] DFA21_eot = DFA.unpackEncodedString(DFA21_eotS);
    static final short[] DFA21_eof = DFA.unpackEncodedString(DFA21_eofS);
    static final char[] DFA21_min = DFA.unpackEncodedStringToUnsignedChars(DFA21_minS);
    static final char[] DFA21_max = DFA.unpackEncodedStringToUnsignedChars(DFA21_maxS);
    static final short[] DFA21_accept = DFA.unpackEncodedString(DFA21_acceptS);
    static final short[] DFA21_special = DFA.unpackEncodedString(DFA21_specialS);
    static final short[][] DFA21_transition;

    static {
        int numStates = DFA21_transitionS.length;
        DFA21_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA21_transition[i] = DFA.unpackEncodedString(DFA21_transitionS[i]);
        }
    }

    class DFA21 extends DFA {

        public DFA21(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 21;
            this.eot = DFA21_eot;
            this.eof = DFA21_eof;
            this.min = DFA21_min;
            this.max = DFA21_max;
            this.accept = DFA21_accept;
            this.special = DFA21_special;
            this.transition = DFA21_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__230 | T__231 | T__232 | T__233 | T__234 | INPUT | OUTPUT | COLUMNS | BEGIN | END | SELECT | WRITERECORD | UPDATESFDC | DIM | AS | COMMA | LEFT_PAREN | RIGHT_PAREN | QUALIFIER | FROM | SUM | AVG | COUNT | COUNTDISTINCT | MIN | MAX | DISTINCT | COLUMN_NAME | GRAIN_COLUMN | LEVEL_COLUMN | OLAPCUBE | IDENT_NAME | GRAIN_TABLE | LEVEL_TABLE | POSITION_COLUMN_START | LOGICAL_MEMBER_SET_PREFIX | SPARSE | VARCHARTYPE | INTEGERTYPE | FLOATTYPE | NUMBERTYPE | DATETYPE | DATETIMETYPE | NONETYPE | STRING | INTEGER | FLOAT | DATETIME | BOOLEAN | NULL | PARAMETER | WS | PLUS | MINUS | MULT | DIV | MOD | POW | LT | LTEQ | GT | GTEQ | EQUALS | NOTEQUALS | BITAND | BITOR | IN | NOTIN | LIKE | NOTLIKE | PLUSEQUALS | MINUSEQUALS | NOT | OR | AND | ISNULL | ISNOTNULL | INNERJOIN | FULLOUTERJOIN | LEFTOUTERJOIN | RIGHTOUTERJOIN | USINGOUTERJOIN | UNION | UNIONALL | ON | WHERE | ORDERBY | ASCENDING | DESCENDING | PARTITION | PARALLEL | NULLS_LAST | NULLS_FIRST | DISPLAYWHERE | DISPLAYBY | TOP | IF | THEN | ELSE | ELSEIF | ENDIF | WHILE | ENDWHILE | EXITWHILE | FOR | TO | NEXT | EXITFOR | STEP | COMPLETE | ENDCOMPLETE | LIST | ADD | REMOVEAT | REMOVEALL | MAP | PUT | REMOVEITEM | DELETE | PRINT | HALT | SUBSTRING | LENGTH | POSITION | TOUPPER | TOLOWER | TRIM | FORMAT | REPLACE | REPLACEALL | URLENCODE | TOTIME | DATEPART | DATEDIFF | DATEADD | IIF | IFNULL | DATEFIELD | GETCOLUMNVALUE | GETVARIABLE | SAVEDEXPRESSION | ADDPARENTCHILD | NEXTCHILD | GETLEVELVALUE | GETLEVELATTRIBUTE | GETDAYID | GETWEEKID | GETMONTHID | SETLOOKUP | SETLOOKUPROW | SETFIND | SETSTAT | LET | FUNCTION | RETURN | ENDFUNCTION | FUNCTIONLOOKUP | TRANSFORM | TREND | MDXEXPRESSION | STATMEDIAN | STATSTDDEV | NUMROWS | ROWNUMBER | GROUPS | GETPROMPTVALUE | GETPROMPTFILTER | TOSTRING | SELECTED | CHILDREN | ALLCHILDREN | LEAVES | PARENTS | CUBE | CURRENTVALUE | SIN | COS | TAN | ARCSIN | ARCCOS | ARCTAN | ABS | CEILING | FLOOR | SQRT | LN | LOG | EXP | POWER | RANDOM | ISNAN | ISINF | DRANK | RANK | PTILE | RSUM | BY | REXP | RWRITE | LINE_COMMENT | ML_COMMENT );";
        }
    }
 

}