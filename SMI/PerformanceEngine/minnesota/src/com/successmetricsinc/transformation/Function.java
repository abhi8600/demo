/**
 * 
 */
package com.successmetricsinc.transformation;

import java.util.ArrayList;
import java.util.List;

import org.antlr.runtime.tree.Tree;
import org.apache.log4j.Logger;

import com.successmetricsinc.transformation.Operator.Type;
import com.successmetricsinc.warehouse.StagingTable;

/**
 * @author bpeters
 * 
 */
public class Function extends Statement
{
	private String name;
	private StatementBlock block;
	private TransformationScript functionScript;
	private List<VariableDataElement> parameters;
	private DataType type;
	private static Logger logger = Logger.getLogger(Function.class);

	public Function(TransformationScript script, Tree t, List<StagingTable> scriptPath) throws ScriptException
	{
		this.script = script;
		this.line = t.getLine();
		this.start = t.getCharPositionInLine();
		this.stop = t.getText().length() + this.start;
		functionScript = new TransformationScript(script.getRepository(), script.getSession());
		functionScript.initializeScriptResults();
		functionScript.setLogicalQuery(script.isLogicalQuery());
		functionScript.setDisplayExpression(script.getDisplayExpression());
		functionScript.setParentScript(script);
		name = t.getChild(0).getText();
		for (int i = 1; i < t.getChildCount(); i++)
		{
			Tree child = t.getChild(i);
			if (child.getType() == BirstScriptParser.IDENT_NAME)
			{
				VariableDataElement vde = new VariableDataElement();
				vde.name = child.getText();
				vde.type = script.getType(child.getChild(0));
				functionScript.dataElements.add(vde);
				if (parameters == null)
					parameters = new ArrayList<VariableDataElement>();
				parameters.add(vde);
				script.curMemory += vde.type.getInitializedSize();
			} else if (child.getType() == BirstScriptParser.SCRIPT)
			{
				block = new StatementBlock(functionScript, child, scriptPath);
			} else
			{
				if (!hasReturn(t))
				{
					throw new ScriptException("Function does not return a value", line, start, stop);
				}
				type = functionScript.getType(child);
			}
		}
		for (DataElement de : script.getNonOutputElements())
		{
			if (functionScript.getElement(de.getName()) != null)
				continue;
			if (de instanceof InputDataElement)
				functionScript.addInputDataElement((InputDataElement) de);
			else if (de instanceof OutputDataElement)
				functionScript.addOutputDataElement((OutputDataElement) de);
			else
				functionScript.addVariableDataElement((VariableDataElement) de);
		}
		if (script.curMemory > (script.maxMemory*0.9))
		{
			logger.warn("ETL Script data stuctures are consuming 90% of max limit " + (script.maxMemory) + ". Script execution will fail when max limit is reached.");
		}
		if (script.curMemory > script.maxMemory)
			throw new ScriptException("Memory usage exceeds allowed limits", line, start, stop);
	}

	public DataType getParameterType(int index)
	{
		if (index >= parameters.size())
			return null;
		return parameters.get(index).getType();
	}

	private boolean hasReturn(Tree t)
	{
		if (t.getType() == BirstScriptParser.RETURN)
			return true;
		for (int i = 0; i < t.getChildCount(); i++)
		{
			if (hasReturn(t.getChild(i)))
				return true;
		}
		return false;
	}

	public String getName()
	{
		return name;
	}

	public Type getReturnType()
	{
		return Type.Integer;
	}

	public int getReturnSize()
	{
		return Byte.SIZE;
	}

	public void setParameterValue(int index, Object value)
	{
		parameters.get(index).setValue(value);
	}

	public Object evaluate() throws ScriptException
	{
		try
		{
			block.execute();
		} catch (ScriptException ex)
		{
			if (!ex.isFunctionReturn())
				throw ex;
		}
		return functionScript.getFunctionResult();
	}

	@Override
	public void execute() throws ScriptException
	{
		block.execute();
	}
}
