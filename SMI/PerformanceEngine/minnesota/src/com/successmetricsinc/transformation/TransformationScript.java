/**
 * 
 */
package com.successmetricsinc.transformation;

import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import javax.crypto.SecretKey;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.MismatchedSetException;
import org.antlr.runtime.MismatchedTokenException;
import org.antlr.runtime.NoViableAltException;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.Tree;
import org.apache.log4j.Logger;
import org.rosuda.REngine.Rserve.RFileOutputStream;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.R.RServer;
import com.successmetricsinc.query.AbstractQueryString;
import com.successmetricsinc.query.Database;
import com.successmetricsinc.query.Database.DatabaseType;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.DimensionTable;
import com.successmetricsinc.query.DisplayExpression;
import com.successmetricsinc.query.MeasureTable;
import com.successmetricsinc.query.MeasureTableGrain;
import com.successmetricsinc.query.Query;
import com.successmetricsinc.query.QueryResultSet;
import com.successmetricsinc.query.ResultSetCache;
import com.successmetricsinc.query.TableSourceFilter;
import com.successmetricsinc.sfdc.SFDCUpdate;
import com.successmetricsinc.status.Status;
import com.successmetricsinc.transformation.BirstScriptParser.definition_return;
import com.successmetricsinc.transformation.BirstScriptParser.inputquery_return;
import com.successmetricsinc.transformation.BirstScriptParser.outputquery_return;
import com.successmetricsinc.transformation.BirstScriptParser.statementlist_return;
import com.successmetricsinc.transformation.DataType.DType;
import com.successmetricsinc.transformation.operators.OpBinaryVarchar;
import com.successmetricsinc.transformation.operators.OpDateTime;
import com.successmetricsinc.transformation.operators.OpVarchar;
import com.successmetricsinc.transformation.transforms.FlattenParentChild;
import com.successmetricsinc.util.AWSUtil;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.logging.TraceLog;
import com.successmetricsinc.warehouse.Bulkload;
import com.successmetricsinc.warehouse.Bulkload.WriteThread;
import com.successmetricsinc.warehouse.Line;
import com.successmetricsinc.warehouse.LineProcessor;
import com.successmetricsinc.warehouse.LineProcessorResult;
import com.successmetricsinc.warehouse.LoadTables;
import com.successmetricsinc.warehouse.SourceColumn;
import com.successmetricsinc.warehouse.SourceFile;
import com.successmetricsinc.warehouse.StagingColumn;
import com.successmetricsinc.warehouse.StagingTable;
import com.successmetricsinc.warehouse.StagingTableBulkLoadInfo;
import com.successmetricsinc.warehouse.TimeDefinition;

/**
 * @author bpeters
 * 
 */
public class TransformationScript
{
	public enum ScriptPart
	{
		ALL, INPUT, OUTPUT, SCRIPT
	};
	private static Logger logger = Logger.getLogger(TransformationScript.class);
	public static final long MINSIZEPERUNCOMPRESSEDSPLITFILE =    100000000L;	// min size (uncompressed) for split file (100MB)
	public static final long MAXSIZEPERUNCOMPRESSEDSPLITFILE = 50000000000L;	// max size (uncompressed) for split file (50GB)
	private static final int MAX_MEMORY_FOR_SCRIPT_DATA = 225000000; 			// If a single thread then set it to 75MB else 225 MB / number of threads
	private static final int MAX_MEMORY_FOR_THREAD = 75000000;              
	private static final int S3_MAX_READER_THREADS = 3;						// max # of S3 reader threads, too many and we start to get networking errors
	public static final int LINES_PROCESSED_LOGGING_BATCH = 1000000;
	public static final int WORK_QUEUE_SIZE = 100; 							// for parallel script execution
	private static final int S3_ETL_SCRIPT_READ_QUEUE_SIZE = 50000;			// number of results to queue up from the S3 readers
	public static final int EXECUTOR_TIMEOUT = 0;  							// in seconds, for parallel script execution - 0 is the correct value for the type of executor being used
	private Repository r;
	private List<Object> sourceTables = new ArrayList<Object>();
	private List<String> sourceTableNames = new ArrayList<String>();
	private String inputPhysicalQuery;
	private List<String> inputTables;
	private List<String> inputSchemas;
	private List<String> inputConnections;
	private String outputTableName;
	public LinkedBlockingQueue<String[]> writeQueue;	
	private String path;
	private String databasePath;
	private Status status;
	private String loadGroup;
	private boolean loadNecessaryStagingTables = false;
	private ScriptLog sl = null;
	private ScriptResult sr;
	private int maxOutputRows;
	private int maxInputRows;
	public long maxStatements;
	public long maxMemory = MAX_MEMORY_FOR_THREAD;
	public long curMemory;
	private FlattenParentChild flattenParentChild;
	private ResultSetCache rsc;
	private DatabaseConnection dconn;
	private int numSourceRows;
	private List<String> loadGroups = new ArrayList<String>();
	private List<StagingTable> bulkLoaded;
	private SimpleDateFormat outputDateFormat;
	private boolean logicalQuery;
	private DisplayExpression displayExpression;
	private Session session;
	// Data Elements
	public List<InputDataElement> inputElements = new ArrayList<InputDataElement>();
	private List<OutputDataElement> outputElements = new ArrayList<OutputDataElement>();
	public List<DataElement> dataElements = new ArrayList<DataElement>();
	private List<Integer> partitionPositions;
	// Compiled script
	private StatementBlock mainBlock;
	public Statement onComplete;
	// Preprocessing related declarations
	private LineProcessor lp = null;
	private SourceFile preprocessingSourceFile = null;
	private AtomicLong preprocessingWarningCount = null;
	private AtomicLong preprocessingErrorCount = null;
	private AtomicLong numFailedRecords = null;
	private AtomicBoolean skipRunning = null;
	private int iteration;
	private boolean incomplete;
	private int numResultsetRows;
	private TraceLog traceLog;
	private List<Definition> definitions;
	private QueryResultSet parentResultSet;
	private Map<Thread, Integer> parentRow = new HashMap<Thread, Integer>();
	private boolean loadAllBaseTables = false;
	public String processingGroup;
	private SFDCUpdate sforce;
	private List<String> joinTypes;
	private List<String> joinConditions;
	private TransformationScript parentScript;
	private Object functionResult;
	// Following are used for Infobright only when union or union all are present
	private String tempTableName = null;
	private String createTemptableSQL = null;
	private String dropTemptableSQL = null;
	private List<String> insertTemptableSQLList = null;
	private boolean doNotLoadVisulationSources = false; // set to true only from UI execute script calls
	// Parsing Tree
	protected Tree selectTree;
	protected Tree outputTree;
	protected Tree bodyTree;
	protected CommonTokenStream bodyTokenStream;
	// Parsing status
	public boolean inCompleteBlock;
	private boolean inDimensionExpression;
	private DisplayExpression parentDisplayExpression;
	private boolean compilingParent;
	// Script Properties
	public boolean containsVariable;
	public boolean hasCompleteBlock;
	public boolean hasCompleteBlockWithWriteRecord;
	public boolean hasBodyWriteRecord;
	public boolean hasOrderBy;
	public boolean hasVariableAssignment;
	public boolean hasUnion;
	public boolean hasTopn;
	private List<ColData> sortColumns;
	private Map<String, Function> functions;
	private AWSCredentials s3credentials;
	private AmazonS3 s3client;
	private SecretKey s3secretKey;
	public boolean forceRunParallel = false;
	private boolean requiresTempTableonUnload = false;
	private boolean isForSourceTablesOnly = false; // Used only from staging tables to determine parent source tables.
	// Streams for writing to R
	private Map<String, OutputStreamWriter> rOutputMap;

	public static class ScriptResult
	{
		public long numInputRows = -1;
		public long numOutputRows = -1;
		public long numStatements = -1;
	}

	public TransformationScript(Repository r, Session session)
	{
		this.r = r;
		this.session = session;
		if (r.getServerParameters() != null)
		{
			maxOutputRows = r.getServerParameters().getMaxOutputRows();
			maxInputRows = r.getServerParameters().getMaxInputRows();
			maxStatements = r.getServerParameters().getMaxScriptStatements();
		}
		outputDateFormat = r.getDefaultConnection().getOutputDateFormat();
	}
	
	public TransformationScript(Repository r, Session session, boolean isForSourceTablesOnly)
	{
		this(r,session);
		this.isForSourceTablesOnly = isForSourceTablesOnly;
	}
	
	public void setS3Credentials(AWSCredentials cred)
	{
		s3credentials = cred;
	}
	
	public void setS3Client(AmazonS3 s3client2)
	{
		s3client = s3client2;
	}
	
	public void setS3SecretKey(SecretKey symKey)
	{
		this.s3secretKey = symKey;
	}

	public void setLoadAllBaseTables(boolean value)
	{
		this.loadAllBaseTables = value;
	}

	public ParseResult parseScript(ScriptDefinition script, int numSourceRows, List<StagingTable> scriptPath)
	{
		this.numSourceRows = numSourceRows;
		ParseResult result = new ParseResult();
		// Input
		String inputQueryString = script.InputQuery.replace("\r", "\n");
		ANTLRStringStream stream = new ANTLRStringStream(inputQueryString);
		BirstScriptLexer lex = new BirstScriptLexer(stream);
		CommonTokenStream cts = new CommonTokenStream(lex);
		BirstScriptParser parser = new BirstScriptParser(cts);
		dconn = r == null || r.getConnections() == null ? null : r.getDefaultConnection();
		try
		{
			inputquery_return ret = parser.inputquery();
			if (lex.hasError())
				result.InputError = processLexError(lex);
			try
			{
				// Make sure that the entire string was processed
				if (ret.stop != cts.getTokens().get(cts.getTokens().size() - 1))
				{
					result.InputError = new ScriptException("Syntax error, unable to parse query", ret.stop.getLine(), ret.stop.getCharPositionInLine()
							+ ret.stop.getText().length(), inputQueryString.length());
				} else
				{
					List<StagingTable> spath = scriptPath == null ? new ArrayList<StagingTable>() : new ArrayList<StagingTable>(scriptPath);
					processSelect(ret.tree, numSourceRows, false, spath);
					selectTree = ret.tree;
					// Return load group information
					result.LoadGroups = new String[loadGroups.size()];
					loadGroups.toArray(result.LoadGroups);
				}
			} catch (ScriptException e)
			{
				result.InputError = e;
			}
		} catch (RecognitionException e)
		{
			try
			{
				processException(e, lex);
			} catch (ScriptException se)
			{
				result.InputError = se;
			}
		}
		// Output
		String output = script.Output.replace("\r", "\n");
		stream = new ANTLRStringStream(output);
		lex = new BirstScriptLexer(stream);
		cts = new CommonTokenStream(lex);
		parser = new BirstScriptParser(cts);
		try
		{
			outputquery_return ret = parser.outputquery();
			if (lex.hasError())
				result.OutputError = processLexError(lex);
			try
			{
				processOutput(ret.tree);
			} catch (ScriptException e)
			{
				result.OutputError = e;
			}
			outputTree = ret.tree;
		} catch (RecognitionException e)
		{
			try
			{
				logger.error("Unable to process output parameters for script: " + output);
				processException(e, lex);
			} catch (ScriptException se)
			{
				result.OutputError = se;
			}
		}
		if (result.InputError != null || result.OutputError != null)
			return result;
		/*
		 * Only parse script if input is valid
		 */
		String body = script.Script.replace("\r", "\n");
		stream = new ANTLRStringStream(body);
		lex = new BirstScriptLexer(stream);
		bodyTokenStream = new CommonTokenStream(lex);
		parser = new BirstScriptParser(bodyTokenStream);
		try
		{
			statementlist_return ret = parser.statementlist();
			if (lex.hasError())
				result.ScriptError = processLexError(lex);
			try
			{
				List<StagingTable> spath = scriptPath == null ? new ArrayList<StagingTable>() : new ArrayList<StagingTable>(scriptPath);
				bodyTree = ret.tree;
				processScript(ret.tree, spath);
			} catch (ScriptException e)
			{
				result.ScriptError = e;
			}
		} catch (RecognitionException e)
		{
			try
			{
				processException(e, lex);
			} catch (ScriptException se)
			{
				result.ScriptError = se;
			}
		}
		return result;
	}

	public BirstScriptParser parseScript(String script, int numSourceRows, List<StagingTable> scriptPath) throws ScriptException
	{
		this.numSourceRows = numSourceRows;
		if (script.indexOf('\n') < 0)
			script = script.replace("\r", "\n");
		ANTLRStringStream stream;
		stream = new ANTLRStringStream(script);
		BirstScriptLexer lex = new BirstScriptLexer(stream);
		CommonTokenStream cts = new CommonTokenStream(lex);
		BirstScriptParser parser = new BirstScriptParser(cts);
		dconn = r == null || r.getConnections() == null ? null : r.getDefaultConnection();
		try
		{
			definition_return ret = parser.definition();
			if (lex.hasError())
			{
				throw processLexError(lex);
			}
			List<StagingTable> spath = new ArrayList<StagingTable>(scriptPath);
			processSelect(ret.tree, numSourceRows, false, spath);
			processOutput(ret.tree);
			spath = new ArrayList<StagingTable>(scriptPath);
			selectTree = ret.tree;
			outputTree = ret.tree;
			bodyTree = ret.tree;
			processScript(ret.tree, spath);
		} catch (ScriptException e)
		{
			throw e;
		} catch (Exception e)
		{
			processException(e, lex);
		}
		return parser;
	}

	private void processException(Exception e, BirstScriptLexer lex) throws ScriptException
	{
		String error = null;
		int line = -1;
		int start = -1;
		int end = -1;
		if (MismatchedTokenException.class.isInstance(e))
		{
			if (lex != null && lex.hasError())
			{
				throw processLexError(lex);
			} else
			{
				MismatchedTokenException mte = (MismatchedTokenException) e;
				if (mte.charPositionInLine == -1)
					error = "Syntax error - unable to validate script";
				else
					error = "Syntax error at line " + mte.line + (mte.charPositionInLine >= 0 ? ", column " + mte.charPositionInLine : "")
							+ (mte.token.getText() != null ? ": " + mte.token.getText() : "");
				logger.error(error);
				line = mte.line;
				start = mte.charPositionInLine;
			}
		} else if (MismatchedSetException.class.isInstance(e))
		{
			MismatchedSetException mse = (MismatchedSetException) e;
			error = "Syntax error at line " + mse.line + (mse.charPositionInLine >= 0 ? ", column " + mse.charPositionInLine : "");
			line = mse.line;
			start = mse.charPositionInLine;
			logger.error(error);
		} else if (NoViableAltException.class.isInstance(e))
		{
			NoViableAltException nva = (NoViableAltException) e;
			line = nva.line;
			start = nva.charPositionInLine;
			error = "Syntax error at line " + nva.line + (nva.charPositionInLine >= 0 ? ", column " + nva.charPositionInLine : "");
			logger.error(error);
		} else
			error = e.getMessage();
		throw new ScriptException(error, line, start, end);
	}

	private ScriptException processLexError(BirstScriptLexer lex)
	{
		RecognitionException e = lex.errorMessage();
		String error = null;
		int line = -1;
		int start = -1;
		int end = -1;
		if (MismatchedTokenException.class.isInstance(e))
		{
			MismatchedTokenException mte = (MismatchedTokenException) e;
			error = "Syntax error at line " + mte.line + (mte.charPositionInLine >= 0 ? ", column " + mte.charPositionInLine : "");
			line = mte.line;
			start = mte.charPositionInLine;
			logger.error(error);
		} else if (MismatchedSetException.class.isInstance(e))
		{
			MismatchedSetException mse = (MismatchedSetException) e;
			error = "Syntax error at line " + mse.line + (mse.charPositionInLine >= 0 ? ", column " + mse.charPositionInLine : "");
			line = mse.line;
			start = mse.charPositionInLine;
			logger.error(error);
		} else if (NoViableAltException.class.isInstance(e))
		{
			NoViableAltException nva = (NoViableAltException) e;
			error = "Syntax error at line " + nva.line + (nva.charPositionInLine >= 0 ? ", column " + nva.charPositionInLine : "");
			line = nva.line;
			start = nva.charPositionInLine;
			logger.error(error);
		} else
			error = e.getMessage();
		return new ScriptException(error, line, start, end);
	}

	public List<Object> getSourceTables()
	{
		return sourceTables;
	}

	public void executeScript(Status status, String sourceName, ScriptDefinition script, String loadGroup, String path, String databasePath, int numSourceRows,
			List<StagingTable> bulkLoaded, StagingTableBulkLoadInfo stbl, SourceFile preprocessingSourceFile, List<StagingTable> scriptPath)
			throws ScriptException, SQLException, IOException
	{
		this.bulkLoaded = bulkLoaded;
		sl = new ScriptLog(r);
		ScriptResult sr = null;
		sl.log(sourceName, status.getIteration(), ScriptLog.StatusCode.Running, -1, -1, null);
		try
		{
			sr = executeScript(status, script.InputQuery, script.Output, script.Script, loadGroup, path, databasePath, numSourceRows, stbl,
					preprocessingSourceFile, scriptPath);
		} catch (ScriptException ex)
		{
			sl.log(sourceName, status.getIteration(), ScriptLog.StatusCode.Failed, -1, -1,
					ex.getMessage());
			throw ex;
		}
		sl.log(sourceName, status.getIteration(), ScriptLog.StatusCode.Complete, sr != null ? sr.numInputRows : -1, sr != null ? sr.numOutputRows : -1, null);
	}

	public ScriptResult executeScript(Status status, String script, String loadGroup, String path, String databasePath, int numSourceRows,
			StagingTableBulkLoadInfo stbl, SourceFile preprocessingSourceFile, List<StagingTable> scriptPath) throws ScriptException, SQLException, IOException
	{
		sr = new ScriptResult();
		this.path = path;
		this.databasePath = path;
		this.status = status;
		this.loadGroup = loadGroup;
		loadNecessaryStagingTables = true;
		parseScript(script, numSourceRows, scriptPath);
		return executeScript(status, loadGroup, path, databasePath, numSourceRows, stbl, preprocessingSourceFile);
	}

	public ScriptResult executeScript(Status status, String inputQuery, String output, String script, String loadGroup, String path, String databasePath,
			int numSourceRows, StagingTableBulkLoadInfo stbl, SourceFile preprocessingSourceFile, List<StagingTable> scriptPath) throws ScriptException, SQLException, IOException
	{
		sr = new ScriptResult();
		this.path = path;
		this.databasePath = databasePath;
		this.status = status;
		this.loadGroup = loadGroup;
		ScriptDefinition sd = new ScriptDefinition();
		sd.InputQuery = inputQuery;
		sd.Output = output;
		sd.Script = script;
		loadNecessaryStagingTables = true;
		if (this.bulkLoaded == null)
			bulkLoaded = Collections.synchronizedList(new ArrayList<StagingTable>());
		ParseResult pr = parseScript(sd, numSourceRows, scriptPath);
		if (pr.InputError != null)
			throw pr.InputError;
		if (pr.OutputError != null)
			throw pr.OutputError;
		if (pr.ScriptError != null)
			throw pr.ScriptError;
		return executeScript(status, loadGroup, path, databasePath, numSourceRows, stbl, preprocessingSourceFile);
	}

	@SuppressWarnings("resource")
	private ScriptResult executeScript(Status status, String loadGroup, String path, String databasePath, int numSourceRows, StagingTableBulkLoadInfo stbl,
			SourceFile preprocessingSourceFile) throws ScriptException, SQLException, IOException
	{
		sdfdate = dconn.getOutputDateFormat();
		sdfdatetime = dconn.getOutputDateTimeFormat();
		sdfdate.setTimeZone(r.getServerParameters().getProcessingTimeZone());
		sdfdatetime.setTimeZone(r.getServerParameters().getProcessingTimeZone());
		
		if (containsVariable)
		{
			try
			{
				r.processMetadata(true, false);
			} catch (Exception e)
			{
				throw new ScriptException("Unable to instantiate repository for space, please contact Birst", -1, -1, -1);
			}
		}
		if (preprocessingSourceFile != null && stbl != null)
		{
			/**
			 * set up date/datetime formats
			 */
			for (int i = 0; i < preprocessingSourceFile.Columns.length; i++)
			{
				SourceColumn sc = preprocessingSourceFile.Columns[i];
				if (sc.DataType == SourceColumn.Type.Date)
				{
					logger.debug("setting script column " + sc.Name + ", " + i + " to " + sdfdate.toPattern());
					stbl.formats[i] = (SimpleDateFormat) sdfdate.clone();
				} else if (sc.DataType == SourceColumn.Type.DateTime)
				{
					logger.debug("setting script column " + sc.Name + ", " + i + " to " + sdfdatetime.toPattern());
					stbl.formats[i] = (SimpleDateFormat) sdfdatetime.clone();
				}
			}
			preprocessingWarningCount = new AtomicLong();
			preprocessingErrorCount = new AtomicLong();
			numFailedRecords = new AtomicLong();
			skipRunning = new AtomicBoolean(false);
			lp = new LineProcessor(stbl, preprocessingWarningCount, preprocessingErrorCount, this.r, preprocessingSourceFile, stbl.st,
					stbl.st.getStagingColumnMap(r), null, null, preprocessingSourceFile.ReplaceWithNull, preprocessingSourceFile.SkipRecord,
					r.getNumThresholdFailedRecords(), numFailedRecords, skipRunning, this.r.getDefaultConnection());
			this.preprocessingSourceFile = preprocessingSourceFile;
		}
		DatabaseConnection dconn = null;
		if (inputConnections != null && inputConnections.size() > 0)
			dconn = r.findConnection(inputConnections.get(0));
		else
			dconn = r.getDefaultConnection(); // In the case connections are against the time tables
		
		// .gz extension to support newer version of ParAccel Bulk load
		// do not gzip compress Redshift files here, we do that during the splitting operation prior to upload
		String fnameSuffix = dconn.supportsZippedDataForBulkLoading() ? ((lp == null) ? "" : ".tmp.gz" ) : ((lp == null) ? "" : ".tmp");
		String fileName = r.getServerParameters().getApplicationPath() + "\\data\\" + outputTableName + ".txt" + fnameSuffix;
		if (DatabaseConnection.isDBTypeMySQL(dconn.DBType) && Util.containsNonAsciiCharacters(outputTableName))
			fileName = fileName.replace(outputTableName, stbl.st.Name);

		logger.debug("Script output file: " + fileName);

		AWSUtil.BirstTransferManager tm = null;
		if (dconn.DBType == DatabaseType.Redshift)
		{
			tm = new AWSUtil.BirstTransferManager(s3client, databasePath, Bulkload.S3_UPLOAD_THREADS, AWSUtil.getNumberOfSlices(dconn));
		}
		
		List<File> splitFiles = new ArrayList<File>();
		int numberOfSplits = 1;
		if (tm != null)
			numberOfSplits = tm.getNumberOfSplits();
		
		SourceFile sf = null; // sf is not needed when lp is null (executing from the UI) (sf is used for encoding for sampling), XXX just pass in encoding
		if (lp != null)
			sf = stbl.sf;

		/*
		 * Figure out parallelization strategy
		 */		
		boolean parallel = false;
		if (forceRunParallel)
			parallel = true;
		else if (!hasOrderBy && !hasCompleteBlockWithWriteRecord && !hasVariableAssignment)
			parallel = true;
		else if (partitionPositions != null)
			parallel = true;

		List<TransformationScript> scriptThreads = new ArrayList<TransformationScript>();
		scriptThreads.add(this);
		AtomicBoolean processing = new AtomicBoolean(true);
		AtomicBoolean stop = new AtomicBoolean(false);
		writeQueue =  new LinkedBlockingQueue<String[]>(Bulkload.WRITE_QUEUE_SIZE);
		AtomicLong writecount = new AtomicLong(0);
		long sizePerFile = Long.MAX_VALUE;
		if (dconn.DBType == DatabaseType.Redshift)
		{
			// estimate the bytes per row in the output file
			long bytesPerRow = 0;
			if (stbl != null && stbl.st != null)
			{
				StagingTable ost = stbl.st;
				logger.debug("output staging table: " + stbl.st.Name);
				if (ost != null)
				{
					for (StagingColumn sc : ost.Columns)
					{
						long width = sc.Width;
						if ("Integer".equals(sc.DataType) && sc.Width >= 10)
							width = width / 2; // average width in a column
						logger.debug("column name: " + sc.Name + ", original width: " + sc.Width + ", estimated width: " + width);
						bytesPerRow += (width + 1); // +1 for field/row terminator
					}
				}
			}
			logger.debug("output table bytes/row: " + bytesPerRow);
			
			// estimate the number of output rows, just use the max of the staging tables (dim/fact tables...)
			long rows = 0;
			if (this.inputTables != null)
			{
				Connection conn = dconn.ConnectionPool.getConnection();
				java.sql.Statement stmt = conn.createStatement();
				for (String stable : this.inputTables)
				{
					StagingTable st = r.findStagingTable(stable);
					if (st != null)
					{
						// force a size calculation
						st.setSize(dconn, stmt);
						if (st.Size == null)
								continue;
						logger.debug("input table: " + st.Name + ", rows: " + st.getSize());
						rows = Math.max(rows, st.getSize());
					}
				}
			}
			long totalSize = rows * bytesPerRow;
			sizePerFile = totalSize / numberOfSplits;
			logger.debug("output table bytes/file (pre-fenced): " + sizePerFile);
			
			// don't allow files that are too small or files that are too big (to minimize the cost of upload retries and allow more overlap)
			sizePerFile = Math.max(sizePerFile, TransformationScript.MINSIZEPERUNCOMPRESSEDSPLITFILE);
			sizePerFile = Math.min(sizePerFile, TransformationScript.MAXSIZEPERUNCOMPRESSEDSPLITFILE);
			logger.debug("Output table bytes per file (uncompressed): " + sizePerFile);
		}
		
		Bulkload.WriteThread wt = new Bulkload.WriteThread(dconn, r, writeQueue, fileName, numberOfSplits, sizePerFile, splitFiles, null, sf, processing, writecount, tm);
		String tempResultsTable = null;
		try
		{
			if (inputTables != null)
			{
				boolean exists = true;
				for (int i = 0; i < inputTables.size(); i++)
				{
					if (!Database.tableExists(r, dconn, inputSchemas.get(i), inputTables.get(i),false))
					{
						exists = false;
						break;
					}
				}
				sr.numInputRows = 0;
				sr.numOutputRows = 0;
				if (exists)
				{
					/*
					 * Get a new connection that needs to be closed manually. This is because if one query is being
					 * processed (this one) and the same connection is used for another query (e.g. log status) then the
					 * entire result set is flushed into memory - which doesn't work for big ones.
					 */
					Connection conn = null;
					java.sql.Statement stmt = null;
					BirstResultSet rs = null;
					traceLog = new TraceLog(r.getRepositoryRootPath(), r.getServerParameters().getMaxRecords() * 10);
					traceLog.enable();
					try
					{
						conn = dconn.ConnectionPool.getNewConnection();
						if ((!DatabaseConnection.isDBTypeInfoBright(dconn.DBType) && !DatabaseConnection.isDBTypeMemDB(dconn.DBType))
								|| (createTemptableSQL == null))
						{
							conn.setReadOnly(true);
						}
						
						// http://docs.aws.amazon.com/redshift/latest/dg/jdbc-fetch-size-parameter.html
						// http://stackoverflow.com/questions/1468036/java-jdbc-ignores-setfetchsize
						// http://jdbc.postgresql.org/documentation/83/query.html#query-with-cursor
						if (dconn.DBType == DatabaseType.Redshift)
							conn.setAutoCommit(false);
						
						stmt = conn.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
						stmt.setFetchSize(dconn.getInputScriptFetchSize());

						// Query timeout not supported in MonetDB and BirstDB
						if (r.getServerParameters().getScriptQueryTimeout() > 0 && dconn.DBType != DatabaseType.MonetDB
								&& !DatabaseConnection.isDBTypeMemDB(dconn.DBType))
							stmt.setQueryTimeout(r.getServerParameters().getScriptQueryTimeout() * 60);
						if (DatabaseConnection.isDBTypeInfoBright(dconn.DBType) || DatabaseConnection.isDBTypeMemDB(dconn.DBType))
						{
							if (createTemptableSQL != null)
							{
								Database.clearCacheForConnection(dconn);
								logger.debug("Create temp table: " + Query.getPrettyQuery(createTemptableSQL));
								stmt.executeUpdate(createTemptableSQL);
								if (!dconn.supportsLimit0())
								{
									logger.debug("Cleaning up 1 row inserted by LIMIT 1 query");
									StringBuilder sb = new StringBuilder("TRUNCATE TABLE " + tempTableName);
									logger.debug(Query.getPrettyQuery(sb.toString()));
									stmt.executeUpdate(sb.toString());
								}
							}
							if (insertTemptableSQLList != null && insertTemptableSQLList.size() > 0)
							{
								for (String insertSQL : insertTemptableSQLList)
								{
									logger.debug("Inserting into temp table: " + Query.getPrettyQuery(insertSQL));
									stmt.executeUpdate(insertSQL);
								}
							}
						}
						
						int numS3ReadThreads = 1;
						AWSUtil.BirstS3ReadThread[] s3ReadThread = null;
						AmazonS3 s3readclient[] = null;
						
						if (parallel) // even if not parallel, should still use the parallel code for simplicity XXX
						{
							Map<String, OutputStreamWriter> omap = Collections.synchronizedMap(new HashMap<String, OutputStreamWriter>());
							int threads = Math.max(1, Runtime.getRuntime().availableProcessors() - 1); // leave one for writing
							if(r.getServerParameters().getMaxThreadsInETL()!=0)
							{
								threads = Math.min(threads, r.getServerParameters().getMaxThreadsInETL());
								maxMemory = Math.round(MAX_MEMORY_FOR_SCRIPT_DATA/threads);//Allocate equal memory to all threads so that the combined Memory does not exceed 225MB
								logger.debug("MaxThreadsInETL = "+ r.getServerParameters().getMaxThreadsInETL()  + ". Threads to use in ETL: "+threads +". MaxMemory per thread: " + maxMemory);
							}

							for (int i = 1; i < threads; i++)
							{
								// Generate additional script threads
								TransformationScript newScript = new TransformationScript(r, session);
								newScript.sr = new ScriptResult();
								newScript.dconn = dconn;
								List<StagingTable> spath = new ArrayList<StagingTable>();
								newScript.processSelect(selectTree, -1, false, spath);
								newScript.processOutput(outputTree);
								//Reinitialize the script path - we don't want to use the same structure that we used 
								//for select. If we do so, a case where same table is referred in select as well as in
								//script block will fail.
								spath = new ArrayList<StagingTable>();
								newScript.processScript(bodyTree, spath);
								newScript.sdfdate = (SimpleDateFormat) sdfdate.clone();
								newScript.sdfdatetime = (SimpleDateFormat) sdfdatetime.clone();
								newScript.writeQueue = writeQueue;
								newScript.sr.numInputRows = 0;
								newScript.sr.numOutputRows = 0;
								newScript.lp = lp;
								newScript.setTraceLog(traceLog);
								newScript.preprocessingSourceFile = preprocessingSourceFile;
								newScript.rOutputMap = omap;
								newScript.maxMemory = maxMemory;
								scriptThreads.add(newScript);
							}
							logger.info("Running parallel scripts, number of threads: " + scriptThreads.size());
						}
						logger.debug("Input Script Query: " + Query.getPrettyQuery(inputPhysicalQuery));
						List<Exception> exceptionList = Collections.synchronizedList(new ArrayList<Exception>());

						String sourceFileName = outputTableName + ".txt";
						SourceFile sourceFile = r.findSourceFile(sourceFileName);
						if(sourceFile==null){
							logger.error("Unable to find Source File for " + outputTableName + ". File Parsing may fail.");
						}
						int colCount = 0;
						if (false && dconn.DBType == DatabaseType.Redshift) // XXX temporary due to network connection issues (even when in AWS)
						{
							
							// XXX due to connection limits, try getting a new s3 client for each ETL script
							// https://forums.aws.amazon.com/thread.jspa?messageID=133405&#133405 (2012)
							// http://aws.amazon.com/articles/1904/ (2008)
							
							List<SecretKey> keys = new ArrayList<SecretKey>();
							s3client = AWSUtil.getS3Client(s3credentials, keys);
							if (AWSUtil.isEncrypted(s3client))
								s3secretKey = keys.get(0);
							
							// XXX bug in UNLOAD, does not set all meta data? https://forums.aws.amazon.com/thread.jspa?threadID=135834&tstart=0
							String credentials = AWSUtil.getCopyCommandCredentials(s3credentials, null /* s3secretKey */);
							String dummycredentials = AWSUtil.getCopyCommandDummyCredentials(s3secretKey);
							
							String tempFilename = this.databasePath + "/etl_" + System.currentTimeMillis() + '_';
							String s3Location = AWSUtil.cleanupPath(tempFilename);
							
							// delete all objects matching the UNLOAD target
							String items[] = AWSUtil.splitS3Location(s3Location);
							String s3Bucket = items[0];
							String s3Key = items[1];
							AWSUtil.s3DeleteObjects(s3client, s3Bucket, s3Key);
							if(hasOrderBy && !requiresTempTableonUnload){
								inputPhysicalQuery ="SELECT * FROM (" + inputPhysicalQuery + ")";
							}
							//Redshift doesn't support LIMIT with Unload. Create a temptable with results and then unload
							if(requiresTempTableonUnload)
							{
								tempResultsTable = "temp" +System.currentTimeMillis();
								Database.createTableAs(r, dconn, tempResultsTable, inputPhysicalQuery, true);
								inputPhysicalQuery ="SELECT * FROM " + dconn.Schema+"."+tempResultsTable;
							}

							// unload the query (on Redshift this does a parallel unload across all of the slices, producing an object per slice)
							StringBuilder unloadQuery = new StringBuilder("UNLOAD ('");
							unloadQuery.append(Util.replaceStr(inputPhysicalQuery, "'", "\\'"));
							unloadQuery.append("')");
							unloadQuery.append(" TO '").append(s3Location).append("'");
							unloadQuery.append(" WITH");
							unloadQuery.append(" GZIP");
							unloadQuery.append(" DELIMITER AS '|'");
							unloadQuery.append(" ESCAPE");
							unloadQuery.append(" ADDQUOTES");
							unloadQuery.append(" ALLOWOVERWRITE");
							// XXX bug in UNLOAD, does not set all meta data? https://forums.aws.amazon.com/thread.jspa?threadID=135834&tstart=0
							if (false && AWSUtil.isEncrypted(s3client))
								unloadQuery.append(" ENCRYPTED");
							logger.debug(unloadQuery.toString() + dummycredentials);
							
							unloadQuery.append(credentials);
							
							colCount = inputElements.size();

							stmt.executeUpdate(unloadQuery.toString());
							
							// get a list of the S3 objects that were just UNLOADED to
							List<GetObjectRequest> objects = AWSUtil.getObjectList(s3client, s3Bucket, s3Key);
							
							// estimate size as the size of each split, pretty good
							if (objects.size() > 0)
							{
								S3Object s3Object = null;
								try
								{
									s3Object = s3client.getObject(objects.get(0));
									long estimatedUncompressedSize = s3Object.getObjectMetadata().getContentLength() * 3L;
									sizePerFile = Math.max(sizePerFile, estimatedUncompressedSize); // S3 unloaded objects are compressed
									sizePerFile = Math.min(sizePerFile, TransformationScript.MAXSIZEPERUNCOMPRESSEDSPLITFILE);
								}
								finally
								{
									if (s3Object != null)
										s3Object.close();
								}
							}
							
							BlockingQueue<Object[]> results = new ArrayBlockingQueue<Object[]>(S3_ETL_SCRIPT_READ_QUEUE_SIZE);
							
							numS3ReadThreads = Math.max(1, Runtime.getRuntime().availableProcessors() - 2); // limit to a value < # cores
							numS3ReadThreads = Math.min(numS3ReadThreads, objects.size());					// no reason to have more readers than objects
							numS3ReadThreads = Math.min(numS3ReadThreads, S3_MAX_READER_THREADS);				// set a max so we don't overload our S3 client
							numS3ReadThreads = Math.max(numS3ReadThreads, 1);								// just in case objects size is 0							
							logger.debug("Threads for reading from S3: " + numS3ReadThreads);
							s3ReadThread = new AWSUtil.BirstS3ReadThread[numS3ReadThreads];
							s3readclient = new AmazonS3[numS3ReadThreads];
							DType[] types = new DType[this.inputElements.size()];
							AtomicInteger index = new AtomicInteger(0);
							for (int i = 0; i < types.length; i++)
							{
								types[i] = ((InputDataElement) this.inputElements.get(i)).type.type;
							}

							// create threads that read the unloaded S3 objects and populate a result set queue
							for (int i = 0; i < numS3ReadThreads; i++)
							{
								s3readclient[i] = AWSUtil.getS3Client(s3credentials, keys); // allocate new client for each thread to deal with connection reset
								s3ReadThread[i] = new AWSUtil.BirstS3ReadThread(s3readclient[i], objects, results, types, index, exceptionList, sourceFile);
								s3ReadThread[i].start();
							}
							rs = new BirstResultSet(results, numS3ReadThreads);
						}
						else
						{
							ResultSet irs = stmt.executeQuery(inputPhysicalQuery);
							colCount = irs.getMetaData().getColumnCount();
							rs = new BirstResultSet(irs);
						}

						int threadNum = 0;
						BlockingQueue<Runnable> workQueue = new ArrayBlockingQueue<Runnable>(WORK_QUEUE_SIZE);
						// core and max thread count the same, 0 timeout since max == core, fixed size work queue to
						// stop memory exhaustion, caller runs policy to stop rejection execution
						ExecutorService tp = (ExecutorService) new ThreadPoolExecutor(scriptThreads.size(), scriptThreads.size(), EXECUTOR_TIMEOUT,
								TimeUnit.SECONDS, workQueue, new ThreadPoolExecutor.CallerRunsPolicy());
						Calendar tzCal = Calendar.getInstance(r.getServerParameters().getProcessingTimeZone());
						
						Thread monitor = new Bulkload.MonitorThread((ThreadPoolExecutor) tp, writeQueue, stop);
						monitor.start();	
						wt.start();
						
						if (lp == null) // for scripts run from the UI (all other script runs have a line processor)
						{
							StringBuilder sb = new StringBuilder();
							for (OutputDataElement ode : outputElements)
							{
								if (sb.length() > 0)
									sb.append('|');
								sb.append(ode.getName().substring(1, ode.getName().length() - 1));
							}
							String[] results = new String[2];
							results[0] = sb.toString();
							results[1] = null;
							WriteThread.offerQueue(writeQueue, results);
						}
						
						while (rs.next())
						{
							if (exceptionList.size() > 0)
								throw (exceptionList.get(0));

							sr.numInputRows++;							
							if (maxInputRows > 0 && sr.numInputRows > maxInputRows)
								throw new ScriptException("Script halted. Exceeded maximum allowed number of input rows: " + sr.numInputRows, -1, -1, -1);

							TransformationScript ts = null;
							if (!parallel)
								ts = this;
							else
							{
								if (partitionPositions != null)
								{
									// Explicit partitioning on positions
									String key = null;
									if (partitionPositions.size() == 1)
									{
										Object o = rs.getObject(partitionPositions.get(0));
										if (o == null)
											key = "";
										else
											key = o.toString();
									} else
									{
										StringBuilder sb = new StringBuilder();
										for (int i = 0; i <= partitionPositions.size(); i++)
										{
											Object o = rs.getObject(partitionPositions.get(0));
											if (o == null)
												key = "";
											else
												key = o.toString();
											sb.append(key);
											sb.append('|');
										}
										key = sb.toString();
									}
									int hash = key.hashCode();
									threadNum = hash % scriptThreads.size();
									ts = scriptThreads.get(threadNum);
								} else
								{
									// Round robin
									ts = scriptThreads.get(threadNum++);
									if (threadNum >= scriptThreads.size())
										threadNum = 0;
								}
							}
							List<Object> inputElementValues = new ArrayList<Object>();
							for (int i = 0; i < ts.inputElements.size() && i < colCount; i++)
							{
								try
								{
									Object o;
									DType type = ((InputDataElement) ts.inputElements.get(i)).type.type;
									
									if (type == DType.Date || type == DType.DateTime)
									{
										if (dconn.DBType == DatabaseType.Infobright || dconn.DBType == DatabaseType.MYSQL)
											o = rs.getTimestamp(i + 1);
										else
											o = rs.getTimestamp(i + 1, tzCal);
									} else
									{
										o = rs.getObject(i + 1);
										// Infobright can return longs instead of ints - correct
										if (type == DType.Integer)
										{
											if (Integer.class.isInstance(o))
												o = ((Integer) o).longValue();
											else if (Long.class.isInstance(o))
												o = ((Long) o).longValue();
											// Oracle can return BigDecimal instead of ints - correct
											else if (BigDecimal.class.isInstance(o))
												o = ((BigDecimal) o).longValue();
										}
									}
									inputElementValues.add(o);
								} catch (Exception e)
								{
									logger.error(e, e);
									inputElementValues.add(null);
								}
							}
							if (parallel)
							{
								tp.execute(new ExecuteScriptRow(ts, inputElementValues, colCount, exceptionList));
							}
							else
							{
								for (int i = 0; i < ts.inputElements.size() && i < colCount; i++)
								{
									ts.inputElements.get(i).setValue(inputElementValues.get(i));
								}
								ts.mainBlock.execute();
							}
							if (sr.numInputRows > 0 && sr.numInputRows % LINES_PROCESSED_LOGGING_BATCH == 0)
								logger.info(sr.numInputRows + " input rows processed");
						}
						
						if (exceptionList.size() > 0)
							throw (exceptionList.get(0));
						
						if (s3ReadThread != null)
						{
							for (int i = 0; i < numS3ReadThreads; i++)
							{
								try
								{
									s3ReadThread[i].join();	
								} catch (InterruptedException e)
								{
								}

							}
						}
						
						tp.shutdown();
						tp.awaitTermination(10, TimeUnit.MINUTES);
						if (parallel)
						{
							for (int i = 1; i < scriptThreads.size(); i++)
							{
								sr.numStatements += scriptThreads.get(i).sr.numStatements;
								sr.numOutputRows += scriptThreads.get(i).sr.numOutputRows;
							}
						}
						
					
					
						if (exceptionList.size() > 0)
							throw (exceptionList.get(0));
						
						if (lp != null)
							lp.clearMaps(true);
						logger.info(sr.numInputRows + " input rows and " + sr.numOutputRows + " output rows processed");
						if (DatabaseConnection.isDBTypeInfoBright(dconn.DBType))
						{
							if (dropTemptableSQL != null)
							{
								logger.debug("Dropping temp table: " + Query.getPrettyQuery(dropTemptableSQL));
								stmt.executeUpdate(dropTemptableSQL);
							}
						}						
					} catch (SQLException ex)
					{
						logger.error("Script halted. SQL Exception", ex);
						if (ex.getMessage() != null && ex.getMessage().indexOf("The query has timed out") >= 0)
						{
							throw new ScriptException("Script halted. Query timeout exceeded. Please simplify query or contact Birst support.", -1, -1, -1);
						}
						throw new ScriptException("Script halted. Bad query. Please examine your query and contact Birst support if needed.", -1, -1, -1);
					} catch (ScriptException ex)
					{
						logger.error("Script halted at record " + sr.numInputRows, ex);
						throw new ScriptException("Script halted at record " + sr.numInputRows + " (" + ex.getMessage() + ")", -1, -1, -1);
					} catch (Exception ex)
					{
						logger.error("Script halted at record " + sr.numInputRows, ex);
						throw new ScriptException("Script halted at record " + sr.numInputRows + " (" + ex.getMessage() + ")", -1, -1, -1);
					} finally
					{
						try
						{
							if(requiresTempTableonUnload && tempResultsTable!=null)
							{
								Database.dropTable(r, dconn, tempResultsTable, true);
							}
							if (rs != null)
								rs.close();
							if (stmt != null)
								stmt.close();
							if (conn != null)
								conn.close();
						} catch (SQLException ex)
						{
							logger.debug(ex, ex);
						}
					}
				}
			}
			else{
				BlockingQueue<Runnable> workQueue = new ArrayBlockingQueue<Runnable>(WORK_QUEUE_SIZE);
				ExecutorService tp = (ExecutorService) new ThreadPoolExecutor(scriptThreads.size(), scriptThreads.size(), EXECUTOR_TIMEOUT,
						TimeUnit.SECONDS, workQueue, new ThreadPoolExecutor.CallerRunsPolicy());
				Thread monitor = new Bulkload.MonitorThread((ThreadPoolExecutor) tp, writeQueue, stop);
				monitor.start();	
				wt.start();
			}
			if (onComplete != null)
			{
				if (traceLog != null)
					traceLog.log("Starting complete block");
				for (TransformationScript threadScript : scriptThreads)
					threadScript.onComplete.execute();
			}
			
			// set up the termination condition for the write threads
			try {
				writeQueue.put(new String[] {Bulkload.END_TOKEN, Bulkload.END_TOKEN});
			} catch (InterruptedException e1) {
				logger.error(e1);
			}
			processing.set(false);
			stop.set(true);
			// wait for the write thread to finish
			try
			{
				wt.join();	
			} catch (InterruptedException e)
			{
				logger.error(e);
			}	
			
			// finish up any SFDC processing
			if (sforce != null)
			{
				sforce.update();
				if (traceLog != null)
					traceLog.log("Updated " + sforce.getCount() + " objects in SFDC");
			}
			if (preprocessingSourceFile != null && lp != null)
			{
				preprocessingSourceFile.NumRowsWarning = lp.getWarningCount();
			}
			scriptThreads.clear(); // help the GC
		} catch (SQLException ex)
		{
			logger.error("Script halted. SQL Exception", ex);
			throw new ScriptException("Script halted. Bad query. Please examine your query and contact Birst support if needed.", -1, -1, -1);
		} catch (ScriptException ex)
		{
			logger.error("Script halted at record " + sr.numInputRows, ex);
			throw new ScriptException("Script halted at record " + sr.numInputRows + " (" + ex.getMessage() + ")", -1, -1, -1);
		} catch (Exception ex)
		{
			logger.error("Script halted at record " + sr.numInputRows, ex);
			throw new ScriptException("Script halted at record " + sr.numInputRows + " (" + ex.getMessage() + ")", -1, -1, -1);
		} finally
		{
			if (traceLog != null)
			{
				traceLog.log(sr.numInputRows + " input rows, " + sr.numOutputRows + " output rows, " + sr.numStatements + " statements executed");
				traceLog.disable();
			}
			if (rOutputMap != null)
			{
				for(Entry<String,OutputStreamWriter> en: rOutputMap.entrySet())
				{
					en.getValue().close();
				}
			}
		}
		return sr;
	}
	
	/**
	 * simple implementation of a result set that allows a separate set of threads to populate a queue for the result set
	 * - only implemented the get* methods needs for script processing
	 * @author ricks
	 *
	 */
	public static class BirstResultSet
	{
		BlockingQueue<Object[]> results;
		ResultSet rs;
		Object[] current;
		int count;
		int num;
		
		public BirstResultSet(BlockingQueue<Object[]> results, int count)
		{
			this.count = count;
			this.results = results;
			this.rs = null;
		}
		
		public BirstResultSet(ResultSet rs)
		{
			this.rs = rs;
			this.results = null;
		}
		
		public void close() throws SQLException
		{
			if (rs != null)
				rs.close();
			else
				results.clear();
		}
		
		public boolean next() throws SQLException
		{
			if (rs != null)
				return rs.next();
			else
			{
				do
				{
					try
					{
						current = results.take();
						if (current != null && Bulkload.END_TOKEN.equals(current[0]))
							count--;
						else
							return true;
					} catch (InterruptedException e1)
					{
						throw new SQLException(e1);
					}
				} while (count > 0);
				return false;
			}
		}
		
		public Object getObject(int index) throws SQLException
		{
			if (rs != null) 
				return rs.getObject(index);
			else if (current != null)
				return current[index - 1];
			return null;
		}
		
		public Object getTimestamp(int index) throws SQLException
		{
			if (rs != null) 
				return rs.getTimestamp(index);
			else if (current != null)
				return current[index - 1];
			return null;
		}
		
		public Object getTimestamp(int index, Calendar cal) throws SQLException
		{
			if (rs != null) 
				return rs.getTimestamp(index, cal);
			else if (current != null)
				return current[index - 1];
			return null;
		}
	}
	
	private static class ExecuteScriptRow implements Runnable
	{
		private TransformationScript ts;
		private List<Exception> exceptionList;
		List<Object> inputElementValues;
		int colCount;

		public ExecuteScriptRow(TransformationScript ts, List<Object> inputElementValues, int colCount, List<Exception> exceptionList)
		{
			this.ts = ts;
			this.inputElementValues = inputElementValues;
			this.colCount = colCount;
			this.exceptionList = exceptionList;
		}

		@Override
		public void run()
		{
			try
			{
				synchronized (ts)
				{
					for (int i = 0; i < ts.inputElements.size() && i < colCount; i++)
					{
						ts.inputElements.get(i).setValue(inputElementValues.get(i));
					}
					ts.mainBlock.execute();
					inputElementValues = null;
				}
			} catch (ScriptException e)
			{
				exceptionList.add(e);
			}
		}
	}
	private List<Object[]> outputRows;

	private void transformWriteRecord()
	{
		if (outputRows == null)
			outputRows = new ArrayList<Object[]>();
		Object[] row = new Object[outputElements.size() - 1];
		for (int i = 1; i < outputElements.size(); i++)
		{
			// First element is function result
			try
			{
				row[i - 1] = outputElements.get(i).getValue(null);
			} catch (ScriptException e)
			{
			}
		}
		outputRows.add(row);
	}

	public SFDCUpdate getSFDC()
	{
		return sforce;
	}

	public void setSFDC(SFDCUpdate _sforce)
	{
		sforce = _sforce;
	}

	public TraceLog getTraceLog()
	{
		return traceLog;
	}

	public void setTraceLog(TraceLog log)
	{
		traceLog = log;
	}
	private SimpleDateFormat sdfdatetime = null;
	private SimpleDateFormat sdfdate = null;

	public void writeRecord(WriteRecord wr) throws IOException, ScriptException
	{
		if (parentScript != null)
			parentScript.writeRecord(wr);
		if (displayExpression != null)
		{
			transformWriteRecord();
			return;
		}
		boolean first = true;
		StringBuilder record = new StringBuilder();
		for (OutputDataElement ode : outputElements)
		{
			if (first)
				first = false;
			else
				record.append('|');
			if (ode.getType().type == DType.None)
				continue;
			Object o = ode.getValue(null);
			if (o != null)
			{
				DType type = ode.getType().type;
				if (type == DType.DateTime)
					record.append(sdfdatetime.format(o));
				else if (type == DType.Date)
					record.append(sdfdate.format(o));
				else if (type == DType.Varchar)
				{
					// Quote if necessary
					String s = (String) o;
					boolean quote = false;
					for (char c : s.toCharArray())
					{
						if (c == '\r' || c == '\n' || c == '"' || c == '|' || c == '\\')
						{
							quote = true;
							break;
						}
					}
					if (quote)
						record.append('"' + s.replace("\\", "\\\\").replace("\"", "\\\"").replace("|", "\\|") + '"');
					else
						record.append(s);
				} else
					record.append(o.toString());
			}
		}
		boolean lineWritten = false;
		boolean rethrowException = false;
		/*
		 * Check if there is anything to write - the record string buffer can be empty if there are no assignment
		 * statements to scripted source columns or invalid assignments e.g. in case of "union", trying to assign a
		 * column from later script table.
		 */
		if (record.length() > 0)
		{
			if (lp == null)
			{
				String rs = record.toString();
				String[] results = new String[2];
				results[0] = rs;
				results[1] = null;
				WriteThread.offerQueue(writeQueue, results);
				lineWritten = true;
			} 
			else
			{
				Line l = new Line();
				l.line = record.toString();
				l.number = sr.numOutputRows + 1;
				LineProcessorResult lpr = null;
				try
				{
					if ((l.number % Bulkload.CACHE_CLEAR_CHECK_NUM_LINES) == 0)
					{
						lp.clearMaps();
					}
					lpr = lp.processLine(l);
					if (lp.skipRunning.get())
					{
						rethrowException = true;
						throw new ScriptException("Maximum no. of failed records allowed for source exceeded, aborting generating temp file for "
								+ outputTableName, -1, -1, -1);
					}
					if (lpr != null)
					{
						if (lpr.result != null)
						{
							String rs = lpr.result.toString();
							String[] results = new String[2];
							results[0] = rs;
							results[1] = l.line;
							WriteThread.offerQueue(writeQueue, results);
							lineWritten = true;
							preprocessingSourceFile.NumRowsWritten++;
						} else if (lpr.filtered)
						{
						} else
						{
							StringBuilder esb = new StringBuilder();
							for (int i = 0; i < lpr.pline.length; i++)
								esb.append('[' + lpr.pline[i] + ']');
							if (logger.isTraceEnabled())
								logger.trace(esb);
							preprocessingSourceFile.NumRowsError++;
						}
					} else
					{
						logger.error("No preprocessing result received from LinePreprocessor for line number " + l.number);
						preprocessingSourceFile.NumRowsError++;
					}
				} catch (IOException ie)
				{
					throw ie; // IO exceptions are generally fatal, no reason to keep going
				} catch (Exception e)
				{
					if (rethrowException && (e instanceof ScriptException))
					{
						throw (ScriptException) e;
					} else
					{
						logger.error("Error during preprocessing line number " + l.number);
						logger.error(e, e);
						preprocessingSourceFile.NumRowsError++;
					}
				}
			}
			if (sr != null && lineWritten)
			{
				sr.numOutputRows++;
				if (maxOutputRows > 0 && sr.numOutputRows > maxOutputRows)
					throw new ScriptException("Script halted at line " + wr.getLine() + ". Exceeded maximum allowed number of output rows: "
							+ sr.numOutputRows, wr.getLine(), wr.getStart(), wr.getStop());
			}
		}
	}

	private void checkLoadGroups(List<Object> tables, Tree node) throws ScriptException
	{
		for (Object o : tables)
		{
			if (StagingTable.class.isInstance(o))
			{
				if (loadGroups.isEmpty())
				{
					for (String s : ((StagingTable) o).LoadGroupNames)
						loadGroups.add(s);
				} else
				{
					boolean found = false;
					for (String s : ((StagingTable) o).LoadGroupNames)
						if (loadGroups.contains(s))
						{
							found = true;
							break;
						}
					if (!found)
						throw new ScriptException("Cannot create a script source based on two sources that may be loaded at different times", node.getLine(),
								node.getTokenStartIndex(), node.getTokenStopIndex());
				}
			}
		}
	}

	private static class SingleQuery
	{
		public String queryString;
		public List<DataType> dataTypes = new ArrayList<DataType>();
		public int topN = -1;
	}

	private SingleQuery getSingleQuery(Tree t, int numSourceRows, boolean definition, List<Object> stlist, List<Tree> vplist, List<StagingTable> scriptPath,
			boolean isIBorBirstandUnion) throws ScriptException
	{
		SingleQuery sq = new SingleQuery();
		Tree mainSelect = t.getType() == BirstScriptParser.SELECT ? t : t.getChild(0);
		List<StagingTable> originalScriptPath = scriptPath == null ? new ArrayList<StagingTable>() : new ArrayList<StagingTable>(scriptPath);
		List<StagingTable> drivingQueryScriptPath = new ArrayList<StagingTable>(originalScriptPath);
		SourceQuery drivingQuery = getSelectQuery(mainSelect, numSourceRows, sourceTables, sourceTableNames, false, drivingQueryScriptPath, isIBorBirstandUnion);
		if (drivingQueryScriptPath != null)
		{
			if (scriptPath == null)
				scriptPath = new ArrayList<StagingTable>(drivingQueryScriptPath);
			else
			{
				for (StagingTable st : drivingQueryScriptPath)
				{
					if (!scriptPath.contains(st))
						scriptPath.add(st);
				}
			}
		}
		sq.dataTypes.addAll(drivingQuery.dataTypes);
		// Verify that load groups match
		checkLoadGroups(drivingQuery.tables, mainSelect);
		inputTables = new ArrayList<String>();
		inputSchemas = new ArrayList<String>();
		inputConnections = new ArrayList<String>();
		stlist.addAll(drivingQuery.tables);
		// find the projection list
		for (int i = 0; i < mainSelect.getChildCount(); i++)
		{
			if (mainSelect.getChild(i).getType() == BirstScriptParser.PROJECTIONLIST)
			{
				vplist.add(mainSelect.getChild(i));
				break;
			}
		}
		List<SourceQuery> joinClauses = new ArrayList<SourceQuery>();
		List<String> joinRelations = new ArrayList<String>();
		for (int i = 1; i < t.getChildCount(); i++)
		{
			if (t.getChild(i).getType() == BirstScriptParser.INNERJOIN || t.getChild(i).getType() == BirstScriptParser.LEFTOUTERJOIN
					|| t.getChild(i).getType() == BirstScriptParser.RIGHTOUTERJOIN || t.getChild(i).getType() == BirstScriptParser.FULLOUTERJOIN)
			{
				Tree joinNode = t.getChild(i);
				if (joinNode.getType() == BirstScriptParser.FULLOUTERJOIN)
				{
					DatabaseConnection dc = r.getDefaultConnection();
					if (!dc.supportsFullOuterJoin())
					{
						throw new ScriptException(
								"FULL OUTER JOIN is not supported in MySQL/Infobright.  You can use the UNION of LEFT OUTER JOIN and RIGHT OUTER JOIN to accomplish the same thing. Contact Support for additional information.",
								joinNode.getLine(), joinNode.getCharPositionInLine(), joinNode.getCharPositionInLine() + joinNode.getText().length());
					}
				}
				vplist.add(joinNode.getChild(0).getChild(0));
				List<StagingTable> currentQueryScriptPath = new ArrayList<StagingTable>(originalScriptPath);
				SourceQuery joinQuery = getSelectQuery(joinNode.getChild(0), numSourceRows, sourceTables, sourceTableNames, false, currentQueryScriptPath,
						isIBorBirstandUnion);
				if (currentQueryScriptPath != null)
				{
					if (scriptPath == null)
						scriptPath = new ArrayList<StagingTable>(currentQueryScriptPath);
					else
					{
						for (StagingTable st : currentQueryScriptPath)
						{
							if (!scriptPath.contains(st))
								scriptPath.add(st);
						}
					}
				}
				sq.dataTypes.addAll(joinQuery.dataTypes);
				for (Object o : stlist)
				{
					if (joinQuery.tables.contains(o))
						throw new ScriptException("Cannot refer to the same source table more than once in an input query", joinNode.getLine(),
								joinNode.getCharPositionInLine(), joinNode.getCharPositionInLine() + joinNode.getText().length());
				}
				checkLoadGroups(joinQuery.tables, joinNode);
				stlist.addAll(joinQuery.tables);
				joinClauses.add(joinQuery);
				DataType clauseType = Expression.getType(this, joinNode.getChild(1).getChild(0));
				if (clauseType.type != DType.Boolean)
					throw new ScriptException("Join condition does not return a boolean result", joinNode.getLine(), joinNode.getCharPositionInLine(),
							joinNode.getCharPositionInLine() + joinNode.getText().length());
				String joinClause = getCondition(joinNode.getChild(1).getChild(0), definition, sourceTables, sourceTableNames);
				String joinRelation = null;
				if (joinNode.getType() == BirstScriptParser.INNERJOIN)
					joinRelation = " INNER JOIN ";
				else if (joinNode.getType() == BirstScriptParser.LEFTOUTERJOIN)
					joinRelation = " LEFT OUTER JOIN ";
				else if (joinNode.getType() == BirstScriptParser.RIGHTOUTERJOIN)
					joinRelation = " RIGHT OUTER JOIN ";
				else if (joinNode.getType() == BirstScriptParser.FULLOUTERJOIN)
					joinRelation = " FULL OUTER JOIN ";
				if (joinTypes == null)
					joinTypes = new ArrayList<String>();
				if (joinRelation!=null)
				{
					joinTypes.add(joinRelation.trim());
				}
				if (joinConditions == null)
					joinConditions = new ArrayList<String>();
				joinConditions.add(Statement.toString(joinNode.getChild(1).getChild(0), null));
				joinRelations.add(joinRelation + joinQuery.table + " ON " + joinClause);
			}
		}
		if (joinClauses.size() > 10)
		{
			throw new ScriptException(
					"Cannot have more than 10 join relationships in an input query.  Contact Support for ways to work around this restriction.", t.getLine(),
					t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
		StringBuilder q = new StringBuilder("SELECT ");
		if (drivingQuery.distinct)
			q.append("DISTINCT ");
		if (drivingQuery.topN >= 0)
		{
			if (!dconn.supportsLimit() && !dconn.supportsRowNum())
				q.append("TOP " + drivingQuery.topN + " ");
			hasTopn = true;
		}
		q.append(drivingQuery.projectionList);
		boolean hasWhere = drivingQuery.whereClause != null;
		boolean hasAgg = drivingQuery.hasAgg;
		// Don't add to input tables list (checking for existence) if in time
		if (!drivingQuery.isTime)
		{
			inputTables.add(drivingQuery.physicalTableName);
			inputSchemas.add(drivingQuery.physicalTableSchema);
			inputConnections.add(drivingQuery.physicalTableConnection);
		}
		Set<Integer> joinRelationsToIgnore = new HashSet<Integer>();
		for (int i = 0; i < joinClauses.size(); i++)
		{
			hasWhere = hasWhere || joinClauses.get(i).whereClause != null;
			hasAgg = hasAgg || joinClauses.get(i).hasAgg;
			if (!joinClauses.get(i).isTime)
			{
				/**
				If the same physical table appears multiple times in context of a given script, note down the index of
				the join relation so that it can be ignored later while trying to add the join condition. We still need
				to add columns from the join clause to physical query. It will end up pointing to the same physical table
				already present in the physical query.
				Example of such scenario is while using degenerate dimension:
				select Grain(measure-from-degen-dim) from degen-dim-measure-table
				inner join select level(degen-dim.degen-level) from degen-dim-dim-table <join condition>.
				It should result in physical sql as:
				select tab.measurecol, tab.dim-col from tab
				**/
				String tname = joinClauses.get(i).physicalTableName;
				String schema = joinClauses.get(i).physicalTableSchema;
				String conn = joinClauses.get(i).physicalTableConnection;
				if((tname != null) && (schema != null) && (conn != null) &&
					inputTables.contains(tname) && 
					inputSchemas.contains(schema) &&
					inputConnections.contains(conn))
				{
					joinRelationsToIgnore.add(i);
				}
				else
				{
					inputTables.add(tname);
					inputSchemas.add(schema);
					inputConnections.add(conn);
				}
			}
			q.append(',' + joinClauses.get(i).projectionList);
		}
		q.append(" FROM " + drivingQuery.table);
		for (int i = 0; i < joinRelations.size(); i++)
		{
			if(joinRelationsToIgnore.contains(i))
			{
				continue;
			}
			q.append(joinRelations.get(i));
		}
		if (hasWhere)
		{
			q.append(" WHERE (");
			boolean first = true;
			if (drivingQuery.whereClause != null)
			{
				q.append(drivingQuery.whereClause);
				first = false;
			}
			for (int i = 0; i < joinClauses.size(); i++)
			{
				if (joinClauses.get(i).whereClause != null)
				{
					if (!first)
						q.append(") AND (");
					else
						first = false;
					q.append(joinClauses.get(i).whereClause);
				}
			}
			q.append(')');
		}
		if (hasAgg)
		{
			boolean first = true;
			if (drivingQuery.groupByClause.length() > 0)
			{
				q.append(" GROUP BY ");
				q.append(drivingQuery.groupByClause);
				first = false;
			}
			for (int i = 0; i < joinClauses.size(); i++)
			{
				if (joinClauses.get(i).groupByClause != null && joinClauses.get(i).groupByClause.length() > 0)
				{
					if (!first)
						q.append(",");
					else
					{
						q.append(" GROUP BY ");
						first = false;
					}
					q.append(joinClauses.get(i).groupByClause);
				}
			}
		}
		if (drivingQuery.topN >= 0)
		{
			sq.topN = drivingQuery.topN;
		}
		sq.queryString = q.toString();
		return sq;
	}

	private void processSelect(Tree t, int numSourceRows, boolean definition, List<StagingTable> scriptPath) throws ScriptException
	{
		StringBuilder ob = null;
		StringBuilder q = new StringBuilder();
		List<Object> stlist = new ArrayList<Object>();
		List<Tree> vplist = new ArrayList<Tree>();
		SingleQuery firstsq = null;
		// Preserve the original copy of script path passed from earlier scripts
		List<StagingTable> originalScriptPath = new ArrayList<StagingTable>(scriptPath);
		boolean isIBorBirstandUnion = false;
		boolean isIB = DatabaseConnection.isDBTypeInfoBright(dconn.DBType);
		boolean isBirst = DatabaseConnection.isDBTypeMemDB(dconn.DBType);
		SingleQuery sq = null;
		if (isIB || isBirst)
		{
			for (int i = 0; i < t.getChildCount(); i++)
			{
				Tree qnode = t.getChild(i);
				if (qnode.getType() == BirstScriptParser.UNION || qnode.getType() == BirstScriptParser.UNIONALL)
				{
					isIBorBirstandUnion = true;
					break;
				}
			}
		}
		for (int i = 0; i < t.getChildCount(); i++)
		{
			Tree qnode = t.getChild(i);
			if (qnode.getType() == BirstScriptParser.ORDERBY)
			{
				if (sortColumns == null)
					sortColumns = new ArrayList<ColData>();
				ProjectionlistResult pr = new ProjectionlistResult(r, null, qnode.getChild(0), stlist, vplist, inputElements, dataElements,
						isIBorBirstandUnion, sortColumns, dconn);
				Set<String> physicalTableNames = new HashSet<String>();
				if (isIBorBirstandUnion && inputElements != null && inputElements.size() > 0)
				{
					for (InputDataElement ide : inputElements)
					{
						ColData cd = ide.getColdata();
						if (cd != null && cd.tname!=null && cd.tname.trim().length()>0)
						{
							physicalTableNames.add(cd.tname);
						}
					}
				}
				if (pr.sb.length() > 0)
				{
					if (isIBorBirstandUnion && physicalTableNames.size() > 0)
					{
						String obStr = pr.sb.toString();
						for (String physicalTable : physicalTableNames)
						{
							obStr = obStr.replace(physicalTable + ".", "");
						}
						ob = new StringBuilder();
						ob.append(obStr);
					} else
					{
						ob = pr.sb;
					}
				}
				hasOrderBy = true;
			} else if (qnode.getType() == BirstScriptParser.PARTITION)
			{
				ProjectionlistResult pr = new ProjectionlistResult(r, null, qnode, stlist, vplist, inputElements, dataElements, isIBorBirstandUnion, null, dconn);
				if (pr.inputPositions != null)
					partitionPositions = pr.inputPositions;
			} else if (qnode.getType() == BirstScriptParser.UNION || qnode.getType() == BirstScriptParser.UNIONALL)
			{
				hasUnion = true;
				List<Tree> newvplist = new ArrayList<Tree>();
				List<Object> newstlist = new ArrayList<Object>();
				/**
				 * Make a copy of original script path and use it for unions - this is needed because we want to allow
				 * addition of the same staging table within a given script across multiple unions. Any entries added to
				 * union script path are reconciled later with script path data structure that keeps track of dupes
				 * across different scripts.
				 */
				List<StagingTable> unionScriptPath = new ArrayList<StagingTable>(originalScriptPath);
				sq = getSingleQuery(qnode.getChild(0), numSourceRows, definition, newstlist, newvplist, unionScriptPath, isIBorBirstandUnion);
				// Make sure that any staging tables added to union script path are added to the script path that keeps
				// track of dupes
				// across different scripts.
				if (unionScriptPath != null)
				{
					if (scriptPath == null)
						scriptPath = new ArrayList<StagingTable>(unionScriptPath);
					else
					{
						for (StagingTable st : unionScriptPath)
						{
							if (!scriptPath.contains(st))
								scriptPath.add(st);
						}
					}
				}
				if (sq.dataTypes.size() != firstsq.dataTypes.size())
					throw new ScriptException("Cannot union queries of with different numbers of columns - " + firstsq.dataTypes.size() + ", " + sq.dataTypes.size(), qnode.getLine(), qnode.getCharPositionInLine(),
							qnode.getCharPositionInLine() + qnode.getText().length());
				for (int j = 0; j < sq.dataTypes.size(); j++)
					if (sq.dataTypes.get(j).type != firstsq.dataTypes.get(j).type && sq.dataTypes.get(j).type != DType.Any
							&& firstsq.dataTypes.get(j).type != DType.Any)
						throw new ScriptException("Cannot union queries of with different data types (position: " + j + ") - types: " 
							+ firstsq.dataTypes.get(j).type.name() + ", " + sq.dataTypes.get(j).type.name(),
							qnode.getLine(), qnode.getCharPositionInLine(), qnode.getCharPositionInLine() + qnode.getText().length());
				if (isIBorBirstandUnion)
				{
					if (insertTemptableSQLList == null)
					{
						insertTemptableSQLList = new ArrayList<String>();
					}
					StringBuilder insertTempTableSQL = new StringBuilder();
					insertTempTableSQL.append("INSERT INTO ");
					insertTempTableSQL.append(tempTableName);
					insertTempTableSQL.append(" ");
					insertTempTableSQL.append(sq.queryString);
					insertTemptableSQLList.add(insertTempTableSQL.toString());
					if (q.length() == 0)
					{
						if (qnode.getType() == BirstScriptParser.UNION)
						{
							q.append(" SELECT DISTINCT * FROM " + tempTableName);
						} else
						{
							q.append(" SELECT * FROM " + tempTableName);
						}
					}
				} else
				{
					if (qnode.getType() == BirstScriptParser.UNION)
					{
						q.append(" UNION " + sq.queryString);
					} else
					{
						q.append(" UNION ALL " + sq.queryString);
					}
				}
				stlist.addAll(newstlist);
				vplist.addAll(newvplist);
			} else if (t.getChild(i).getType() == BirstScriptParser.SELECT || t.getChild(i).getType() == BirstScriptParser.SINGLEQUERY)
			{
				Tree child = t.getType() == BirstScriptParser.SINGLEQUERY ? t : t.getChild(0);
				sq = getSingleQuery(child, numSourceRows, definition, stlist, vplist, scriptPath, isIBorBirstandUnion);
				firstsq = sq;
				if (isIBorBirstandUnion)
				{
					if (tempTableName == null)
					{
						String schemaName = ((dconn.Schema == null || dconn.Schema.trim().isEmpty()) ? "" : dconn.Schema);
						String ttName = "tempx_" + Thread.currentThread().getId() + "" + System.currentTimeMillis();
						tempTableName = (schemaName.equals("") ? "" : (schemaName + ".")) + ttName;
						List<String> colNames = new ArrayList<String>();
						List<String> colTypes = new ArrayList<String>();
						if (inputElements != null && inputElements.size() > 0)
						{
							for (InputDataElement ide : inputElements)
							{
								ColData cd = ide.getColdata();
								String originalPhysicalColName = cd.physicalColumnName;
								String physicalColName = cd.physicalColumnName;
								int dupRemovalIndex = 0;
								boolean containsCol = false;
								for (String curColName : colNames)
								{
									if (curColName.toLowerCase().equals(physicalColName.toLowerCase()))
									{
										containsCol = true;
										break;
									}
								}
								while (containsCol)
								{
									containsCol = false;
									physicalColName = originalPhysicalColName + dupRemovalIndex;
									dupRemovalIndex++;
									if (dupRemovalIndex == 100)
									{
										// In the unlikely situation of this happening, avoid infinite loop.
										break;
									}
									for (String curColName : colNames)
									{
										if (curColName.toLowerCase().equals(physicalColName.toLowerCase()))
										{
											containsCol = true;
											break;
										}
									}									
								}
								colNames.add(physicalColName);
								String colDataType = Util.getDBDataType(cd.datatype);
								if (colDataType != null && colDataType.equalsIgnoreCase("varchar"))
								{
									colDataType = colDataType + "(" + cd.width + ")";
								}
								else if (colDataType != null && colDataType.equalsIgnoreCase("float") && isIB)
								{
									colDataType = "DOUBLE";
								}
								if (cd.datatype != null && cd.datatype.equalsIgnoreCase("number") && isIB)
								{
									colDataType = dconn.getNumericType();
								}
								if (cd.datatype != null && cd.datatype.equalsIgnoreCase("date") && isIB)
								{
									colDataType = dconn.getDateType();
								}
								colTypes.add(colDataType);
							}
							StringBuilder sb = Database.getCreateTableSQL(r, dconn, ttName, colNames, colTypes, null, null, true, null, null, null);
							createTemptableSQL = sb.toString();													
						} else
						{
							if (isIB)
							{
								createTemptableSQL = "CREATE TABLE " + tempTableName + " engine=brighthouse AS " + sq.queryString;
								if (dconn.supportsLimit0())
									createTemptableSQL += " LIMIT 0";
								else
									createTemptableSQL += " LIMIT 1";
							} else if (isBirst)
							{
								createTemptableSQL = "CREATE TABLE " + tempTableName + " AS " + Util.replaceStr(sq.queryString, "SELECT ", "SELECT TOP 0 ");
							}
						}
						dropTemptableSQL = "DROP TABLE " + tempTableName;
					}
					if (insertTemptableSQLList == null)
					{
						insertTemptableSQLList = new ArrayList<String>();
					}
					StringBuilder insertTempTableSQL = new StringBuilder();
					insertTempTableSQL.append("INSERT INTO ");
					insertTempTableSQL.append(tempTableName);
					insertTempTableSQL.append(" ");
					insertTempTableSQL.append(sq.queryString);
					insertTemptableSQLList.add(insertTempTableSQL.toString());
				} else
				{
					q.append((q.length() > 0 ? " " : "") + sq.queryString);
				}
			}
		}
		if (ob != null && ob.length() > 0)
			q.append(" ORDER BY " + ob);

		if(sq.topN >= 0)
		{
			if (dconn.supportsLimit())
				q.append(" LIMIT " + sq.topN);
			if (dconn.supportsRowNum())
			{
				Query.wrapWithRownum(q, sq.topN);
			}
			if (dconn.DBType == DatabaseType.Redshift)
			{
				requiresTempTableonUnload = true;
			}
		}
		
		// Make sure all subqueries come from the same connection
		String lastConn = null;
		for (int i = 0; i < inputConnections.size(); i++)
		{
			if (lastConn != null && !inputConnections.get(i).equals(lastConn))
				throw new ScriptException("Cannot execute a script across two different connections", 0, 0, 0);
			lastConn = inputConnections.get(i);
		}
		inputPhysicalQuery = q.toString();
	}

	public StringBuilder getSingleNodeOrderBy(SourceQuery sq, Tree selectQuery, Tree orderByTree) throws ScriptException
	{
		StringBuilder ob = null;
		if (orderByTree.getType() == BirstScriptParser.ORDERBY)
		{
			List<Object> stlist = new ArrayList<Object>();
			stlist.addAll(sq.tables);
			List<Tree> vplist = new ArrayList<Tree>();
			vplist.add(selectQuery.getChild(0));
			ProjectionlistResult pr = new ProjectionlistResult(r, null, orderByTree.getChild(0), stlist, vplist, inputElements, dataElements, false, null, dconn);
			if (pr.sb.length() > 0)
				ob = pr.sb;
		}
		return ob;
	}

	public String getCondition(Tree tree, boolean definition, List<Object> sourceTables, List<String> sourceTableNames) throws ScriptException
	{
		String part;
		Tree date, date1, date2, number, cl, cr;
		switch (tree.getType())
		{
		case BirstScriptParser.DATEPART:
			part = tree.getChild(0).getText().toLowerCase();
			date = tree.getChild(1);
			if (DatabaseConnection.isDBTypeMySQL(dconn.DBType))
				return "EXTRACT(" + part + " FROM " + getCondition(date, definition, sourceTables, sourceTableNames) + ')';
			return "DATEPART(" + part + ',' + getCondition(date, definition, sourceTables, sourceTableNames) + ')';
		case BirstScriptParser.DATEADD:
			part = tree.getChild(0).getText().toLowerCase();
			number = tree.getChild(1);
			date = tree.getChild(2);
			if (DatabaseConnection.isDBTypeMySQL(dconn.DBType))
				return "TIMESTAMPADD(" + part + ',' + getCondition(number, definition, sourceTables, sourceTableNames) + ','
						+ getCondition(date, definition, sourceTables, sourceTableNames) + ')';
			return "DATEADD(" + part + ',' + getCondition(number, definition, sourceTables, sourceTableNames) + ','
					+ getCondition(date, definition, sourceTables, sourceTableNames) + ')';
		case BirstScriptParser.DATEDIFF:
			part = tree.getChild(0).getText().toLowerCase();
			date1 = tree.getChild(1);
			date2 = tree.getChild(2);
			if (DatabaseConnection.isDBTypeMySQL(dconn.DBType))
				return "TIMESTAMPDIFF(" + part + ',' + getCondition(date1, definition, sourceTables, sourceTableNames) + ','
						+ getCondition(date2, definition, sourceTables, sourceTableNames) + ')';
			return "DATEDIFF(" + part + ',' + getCondition(date1, definition, sourceTables, sourceTableNames) + ','
					+ getCondition(date2, definition, sourceTables, sourceTableNames) + ')';
		case BirstScriptParser.IFNULL:
			cl = tree.getChild(0);
			cr = tree.getChild(1);
			if (DatabaseConnection.isDBTypeMySQL(dconn.DBType))
				return "IFNULL(" + getCondition(cl, definition, sourceTables, sourceTableNames) + ','
						+ getCondition(cr, definition, sourceTables, sourceTableNames) + ')';
			else
				return "ISNULL(" + getCondition(cl, definition, sourceTables, sourceTableNames) + ','
						+ getCondition(cr, definition, sourceTables, sourceTableNames) + ')';
		case BirstScriptParser.SUBSTRING:
			// SUBSTRING follows Java semantics rather than SQL semantics
			// 0 based, arguments are starting position and position after the last character
			part = tree.getChild(0).getText().toLowerCase();
			cl = tree.getChild(1);
			if (tree.getChildCount() == 2)
			{
				return "SUBSTRING(" + part + ",(" + getCondition(cl, definition, sourceTables, sourceTableNames) + ")+1)";
			} else
			{
				cr = tree.getChild(2);
				return "SUBSTRING(" + part + ",(" + getCondition(cl, definition, sourceTables, sourceTableNames) + ")+1," + "("
						+ getCondition(cl, definition, sourceTables, sourceTableNames) + ")-(" + getCondition(cr, definition, sourceTables, sourceTableNames)
						+ "))";
			}
		case BirstScriptParser.GETVARIABLE:
			String s = tree.getChild(0).getText();
			OpVarchar vName = new OpVarchar(s.substring(1, s.length() - 1));
			OpVarchar uName = null;
			if (tree.getChildCount() > 1)
			{
				String s1 = tree.getChild(1).getText();
				uName = new OpVarchar(s1.substring(1, s1.length() - 1));
			}
			if(!isForSourceTablesOnly)
			{
				OpBinaryVarchar bv = new OpBinaryVarchar(vName, uName, tree.getType(), this);
				Object obj = bv.evaluate();
				return '\'' + obj.toString() + '\'';
			}
			else 
			{
				return "\'\'";
			}
		case BirstScriptParser.PAREN:
			return ('(' + getCondition(tree.getChild(0), definition, sourceTables, sourceTableNames) + ')');
		case BirstScriptParser.STRING:
			return DatabaseConnection.isDBTypeMSSQL(dconn.DBType)? 'N'+tree.getText() : tree.getText();			
		case BirstScriptParser.INTEGER:
		case BirstScriptParser.FLOAT:		
		case BirstScriptParser.BOOLEAN:
			return tree.getText();
		case BirstScriptParser.DATETIME:
			OpDateTime odt = new OpDateTime(tree.getText(), this);
			String outputdatetime = dconn.getOutputDateTimeFormat().format((Date) odt.evaluate());
			return dconn.getOutputDateTimeString(outputdatetime);
		case BirstScriptParser.NULL:
			return "NULL";
		case BirstScriptParser.PLUS:
			cl = tree.getChild(0);
			cr = tree.getChild(1);
			if (cl.getType() == BirstScriptParser.STRING && cr.getType() == BirstScriptParser.STRING)
				return (cl.getText().substring(0, cl.getText().length() - 1) + cr.getText().substring(1));
			DataType dl = getType(cl);
			DataType dr = getType(cr);
			String condl = getCondition(cl, definition, sourceTables, sourceTableNames);
			String condr = getCondition(cr, definition, sourceTables, sourceTableNames);
			/*
			 * For SQL Server 2005 need to surround varchars with ISNULLs because if one of the columns is null, then
			 * the query will return no rows otherwise
			 */
			if (dl.type == DType.Varchar && dr.type != DType.Varchar)
				return ("ISNULL(" + condl + ",'')+CAST(" + condr + " AS VARCHAR)");
			if (dl.type != DType.Varchar && dr.type == DType.Varchar)
				return ("CAST(" + condl + " AS VARCHAR)+ISNULL(" + condr + ",'')");
			if (dl.type == DType.Varchar && dr.type == DType.Varchar)
				return ("ISNULL(" + condl + ",'')+ISNULL(" + condr + ",'')");
			// if datetime, cast so that arithmetic can be used
			if (cl.getType() == BirstScriptParser.DATETIME)
			{
				condl = "CAST(" + condl + " AS DATETIME)";
			}
			if (cr.getType() == BirstScriptParser.DATETIME)
			{
				condr = "CAST(" + condr + " AS DATETIME)";
			}
			return (condl + "+" + condr);
		case BirstScriptParser.MINUS:
			cl = tree.getChild(0);
			condl = getCondition(cl, definition, sourceTables, sourceTableNames);
			cr = tree.getChild(1);
			condr = getCondition(cr, definition, sourceTables, sourceTableNames);
			// if datetime, cast so that arithmetic can be used
			if (cl.getType() == BirstScriptParser.DATETIME)
			{
				condl = "CAST(" + condl + " AS DATETIME)";
			}
			if (cr.getType() == BirstScriptParser.DATETIME)
			{
				condr = "CAST(" + condr + " AS DATETIME)";
			}
			return (condl + "-" + condr);
		case BirstScriptParser.NEGATE:
			cl = tree.getChild(0);
			return ("-" + getCondition(cl, definition, sourceTables, sourceTableNames));
		case BirstScriptParser.ISNULL:
			cl = tree.getChild(0);
			return (getCondition(cl, definition, sourceTables, sourceTableNames) + " IS NULL");
		case BirstScriptParser.ISNOTNULL:
			cl = tree.getChild(0);
			return (getCondition(cl, definition, sourceTables, sourceTableNames) + " IS NOT NULL");
		case BirstScriptParser.DIV:
			cl = tree.getChild(0);
			cr = tree.getChild(1);
			return (getCondition(cl, definition, sourceTables, sourceTableNames) + "/" + getCondition(cr, definition, sourceTables, sourceTableNames));
		case BirstScriptParser.MULT:
			cl = tree.getChild(0);
			cr = tree.getChild(1);
			return (getCondition(cl, definition, sourceTables, sourceTableNames) + "*" + getCondition(cr, definition, sourceTables, sourceTableNames));
		case BirstScriptParser.EQUALS:
			cl = tree.getChild(0);
			cr = tree.getChild(1);
			return (getCondition(cl, definition, sourceTables, sourceTableNames) + "=" + getCondition(cr, definition, sourceTables, sourceTableNames));
		case BirstScriptParser.LT:
			cl = tree.getChild(0);
			cr = tree.getChild(1);
			return (getCondition(cl, definition, sourceTables, sourceTableNames) + "<" + getCondition(cr, definition, sourceTables, sourceTableNames));
		case BirstScriptParser.GT:
			cl = tree.getChild(0);
			cr = tree.getChild(1);
			return (getCondition(cl, definition, sourceTables, sourceTableNames) + ">" + getCondition(cr, definition, sourceTables, sourceTableNames));
		case BirstScriptParser.LTEQ:
			cl = tree.getChild(0);
			cr = tree.getChild(1);
			return (getCondition(cl, definition, sourceTables, sourceTableNames) + "<=" + getCondition(cr, definition, sourceTables, sourceTableNames));
		case BirstScriptParser.GTEQ:
			cl = tree.getChild(0);
			cr = tree.getChild(1);
			return (getCondition(cl, definition, sourceTables, sourceTableNames) + ">=" + getCondition(cr, definition, sourceTables, sourceTableNames));
		case BirstScriptParser.NOTEQUALS:
			cl = tree.getChild(0);
			cr = tree.getChild(1);
			return (getCondition(cl, definition, sourceTables, sourceTableNames) + "<>" + getCondition(cr, definition, sourceTables, sourceTableNames));
		case BirstScriptParser.LIKE:
			cl = tree.getChild(0);
			cr = tree.getChild(1);
			return (getCondition(cl, definition, sourceTables, sourceTableNames) + " LIKE " + getCondition(cr, definition, sourceTables, sourceTableNames));
		case BirstScriptParser.NOTLIKE:
			cl = tree.getChild(0);
			cr = tree.getChild(1);
			return (getCondition(cl, definition, sourceTables, sourceTableNames) + " NOT LIKE " + getCondition(cr, definition, sourceTables, sourceTableNames));
		case BirstScriptParser.IN:
			cl = tree.getChild(0);
			cr = tree.getChild(1);
			return (getCondition(cl, definition, sourceTables, sourceTableNames) + " IN " + getCondition(cr, definition, sourceTables, sourceTableNames));
		case BirstScriptParser.NOTIN:
			cl = tree.getChild(0);
			cr = tree.getChild(1);
			return (getCondition(cl, definition, sourceTables, sourceTableNames) + " NOT IN " + getCondition(cr, definition, sourceTables, sourceTableNames));
		case BirstScriptParser.VALUELIST:
			return getValueList(tree);
		case BirstScriptParser.AND:
			cl = tree.getChild(0);
			cr = tree.getChild(1);
			return (getCondition(cl, definition, sourceTables, sourceTableNames) + " AND " + getCondition(cr, definition, sourceTables, sourceTableNames));
		case BirstScriptParser.OR:
			cl = tree.getChild(0);
			cr = tree.getChild(1);
			return (getCondition(cl, definition, sourceTables, sourceTableNames) + " OR " + getCondition(cr, definition, sourceTables, sourceTableNames));
		case BirstScriptParser.AVG:
			throw new ScriptException("Cannot use aggregated columns in condition", tree.getLine(), tree.getTokenStartIndex(), tree.getTokenStopIndex());
		case BirstScriptParser.COUNT:
			throw new ScriptException("Cannot use aggregated columns in condition", tree.getLine(), tree.getTokenStartIndex(), tree.getTokenStopIndex());
		case BirstScriptParser.COUNTDISTINCT:
			throw new ScriptException("Cannot use aggregated columns in condition", tree.getLine(), tree.getTokenStartIndex(), tree.getTokenStopIndex());
		case BirstScriptParser.SUM:
			throw new ScriptException("Cannot use aggregated columns in condition", tree.getLine(), tree.getTokenStartIndex(), tree.getTokenStopIndex());
		case BirstScriptParser.MIN:
			throw new ScriptException("Cannot use aggregated columns in condition", tree.getLine(), tree.getTokenStartIndex(), tree.getTokenStopIndex());
		case BirstScriptParser.MAX:
			throw new ScriptException("Cannot use aggregated columns in condition", tree.getLine(), tree.getTokenStartIndex(), tree.getTokenStopIndex());
		case BirstScriptParser.COLUMN_NAME:
		case BirstScriptParser.GRAIN_COLUMN:
		case BirstScriptParser.LEVEL_COLUMN:
			// use the same logic as used for the projection list
			ColData cd = ProjectionlistResult.getTableAndColumnNames(this, r, tree, sourceTables, false, dconn);
			if (cd != null)
			{
				return (cd.unqualified ? "" : cd.tname + '.') + cd.physicalColumnName;
			}
			throw new ScriptException("Bad column/grain/level name", tree.getLine(), tree.getTokenStartIndex(), tree.getTokenStopIndex());
		case BirstScriptParser.IDENT_NAME:
			if (definition)
			{
				// If referring to a variable in a select statement in a variable definition
				DataElement vde = getNonOutputElement(tree.getText());
				if (vde == null || !VariableDataElement.class.isInstance(vde))
					throw new ScriptException("Unknown data element: " + tree.getText(), tree.getLine(), tree.getTokenStartIndex(), tree.getTokenStopIndex());
				Object o = vde.getValue(null);
				if (vde.getType().type == DType.Integer || vde.getType().type == DType.Float)
					return o == null ? "0" : o.toString();
				else if (vde.getType().type == DType.Date)
					return "'" + (o == null ? "" : outputDateFormat.format((Date) o)) + "'";
				else if (vde.getType().type == DType.DateTime && DatabaseConnection.isDBTypeOracle(dconn.DBType))
				{
					if (o == null)
						return "''";
					return dconn.getOutputDateTimeString(dconn.getOutputDateTimeFormat().format(o));
				} else
					return "'" + (o == null ? "" : o.toString()) + "'";
			} else
				throw new ScriptException("Cannot refer to a non-input column in a condition", tree.getLine(), tree.getTokenStartIndex(),
						tree.getTokenStopIndex());
		default:
			throw new ScriptException("Invalid condition in where clause", tree.getLine(), tree.getTokenStartIndex(), tree.getTokenStopIndex());
		}
	}

	private String getValueList(Tree tree)
	{
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < tree.getChildCount(); i++)
		{
			if (sb.length() > 0)
				sb.append(',');
			sb.append(tree.getChild(i).getText());
		}
		return '(' + sb.toString() + ')';
	}

	public static String getScriptString(ScriptDefinition sd)
	{
		String script = "INPUT: " + sd.InputQuery + "\nOUTPUT: " + sd.Output + "\nBEGIN " + sd.Script + " END";
		return script;
	}

	public static StagingTable getStagingTable(String fromTable, Repository r)
	{
		for (StagingTable sst : r.getStagingTables())
		{
			String s = sst.SourceFile;
			int index = s.lastIndexOf('.');
			if (index > 0)
				s = s.substring(0, index);
			if (s.equals(fromTable))
			{
				return sst;
			}
		}
		return null;
	}

	public SourceQuery getSelectQuery(Tree t, int numSourceRows, List<Object> sourceTables, List<String> sourceTableNames, boolean definition,
			List<StagingTable> scriptPath, boolean isIBorBirstandUnion) throws ScriptException
	{
		// handle TOP and DISTINCT
		boolean distinct = false;
		int topN = -1;
		int offset = 0;
		int whereCount = 3;
		for (; offset < t.getChildCount(); offset++)
		{
			if (t.getChild(offset).getType() == BirstScriptParser.TOP)
			{
				topN = Integer.valueOf(t.getChild(offset).getChild(0).getText());
				whereCount++;
			}
			if (t.getChild(offset).getType() == BirstScriptParser.DISTINCT)
			{
				distinct = true;
				whereCount++;
			}
			if (t.getChild(offset).getType() == BirstScriptParser.PARALLEL)
			{
				this.forceRunParallel = true;
				whereCount++;
			}
			if (t.getChild(offset).getType() == BirstScriptParser.PROJECTIONLIST)
				break;
		}
		Tree projectionList = t.getChild(offset);
		Tree from = t.getChild(offset + 1);
		String fromTable = null;
		String fromSchema = r.getDefaultConnection().Schema;
		String fromConn = r.getDefaultConnection().Name;
		List<Object> tables = new ArrayList<Object>();
		List<String> tableNames = new ArrayList<String>();
		String sourceTableName = from.getChild(0).getText();
		sourceTableName = sourceTableName.substring(1, sourceTableName.length() - 1);
		boolean isdbo = false;
		List<TableSourceFilter> flist = new ArrayList<TableSourceFilter>();
		if (from.getChild(0).getType() == BirstScriptParser.IDENT_NAME)
		{
			fromTable = from.getChild(0).getText();
			fromTable = fromTable.substring(1, fromTable.length() - 1);
			StagingTable st = getStagingTable(fromTable, r);
			if (st == null)
				throw new ScriptException("Data source " + fromTable + " does not exist", from.getLine(), from.getTokenStartIndex(), from.getTokenStopIndex());
			if (scriptPath.contains(st) && st.Script != null && st.Script.InputQuery != null && st.Script.InputQuery.length() > 0)
				throw new ScriptException("Data source " + fromTable + " is contained in a circular reference", from.getLine(), from.getTokenStartIndex(),
						from.getTokenStopIndex());
			if (st.LiveAccess)
				throw new ScriptException("Cannot utilize live access sources for ETL", from.getLine(), from.getTokenStartIndex(), from.getTokenStopIndex());
			scriptPath.add(st);
			fromTable = Util.replaceWithPhysicalString(st.Name);
			String fromName = st.SourceFile;
			int index = fromName.lastIndexOf('.');
			if (index > 0)
				fromName = fromName.substring(0, index);
			if (st.Disabled)
				throw new ScriptException("Data source " + fromName + " is not enabled", from.getLine(), from.getTokenStartIndex(), from.getTokenStopIndex());
			sourceTables.add(st);
			sourceTableNames.add(sourceTableName);
			if (st.Imported)
			{
				DatabaseConnection dconn = st.r.getDefaultConnection();
				List<TableSourceFilter> tsflist = null;
				for (DatabaseConnection dc : r.getConnections())
				{
					if (dc.ConnectString.equals(dconn.ConnectString))
					{
						fromConn = dc.Name;
						fromSchema = dconn.Schema;
						tsflist = st.getSecurityFilters(fromTable, dc);
						break;
					}
				}
				if (tsflist != null)
				{
					flist.addAll(tsflist);
				}
			}
			/*
			 * Make sure there are no circular references
			 */
			if (!isForSourceTablesOnly && st.Script != null && st.Script.Script != null && st.Script.Script.length() > 0)
			{
				List<StagingTable> spath = new ArrayList<StagingTable>();
				if (scriptPath != null)
					spath.addAll(scriptPath);
				TransformationScript ts = new TransformationScript(r, session,isForSourceTablesOnly);
				ParseResult pr = ts.parseScript(st.Script, numSourceRows, spath);
				if (pr.InputError != null || pr.OutputError != null || pr.ScriptError != null)
				{
					String s = pr.InputError != null ? pr.InputError.getMessage() : pr.OutputError != null ? pr.OutputError.getMessage() : pr.ScriptError
							.getMessage();
					throw new ScriptException("Unable to parse script from source: " + fromName + ", [" + s + "]", from.getLine(), from.getTokenStartIndex(),
							from.getTokenStopIndex());
				}
			}
			tables.add(st);
			tableNames.add(sourceTableName);
		} else if (from.getChild(0).getType() == BirstScriptParser.GRAIN_TABLE)
		{
			fromTable = from.getChild(0).getText();
			MeasureTable smt = getGrainMeasureTable(fromTable.substring(1, fromTable.length() - 1), r);
			if (smt != null)
			{
				fromTable = smt.TableSource.Tables[0].PhysicalName;
				if (smt.TableSource.Schema != null)
					fromSchema = smt.TableSource.Schema;
				fromConn = smt.TableSource.ConnectionName;
				sourceTables.add(smt);
				sourceTableNames.add(sourceTableName);
				tables.add(smt);
				tableNames.add(sourceTableName);
				if (smt.TableSource.Filters != null && smt.TableSource.Filters.length > 0 && smt.Imported)
				{
					// Apply security filters
					for (TableSourceFilter tsf : smt.TableSource.Filters)
						flist.add(tsf);
				}
			} else
				throw new ScriptException("Data source " + fromTable + " does not exist", from.getLine(), from.getTokenStartIndex(), from.getTokenStopIndex());
		} else if (from.getChild(0).getType() == BirstScriptParser.LEVEL_TABLE)
		{
			fromTable = from.getChild(0).getText();
			fromTable = fromTable.substring(7, fromTable.length() - 2);
			int index = fromTable.indexOf('.');
			String dimension = fromTable.substring(0, index);
			TimeDefinition td = r.getTimeDefinition();
			// Time tables stored in dbo schema
			if (td != null && dimension.equals(td.Name))
				isdbo = true;
			String level = fromTable.substring(index + 1);
			DimensionTable sdt = null;
			for (DimensionTable dt : r.DimensionTables)
			{
				if (!dt.AutoGenerated || (dt.InheritTable != null && dt.InheritTable.length() > 0))
					continue;
				if (dt.DimensionName.equals(dimension) && dt.Level.equals(level))
				{
					fromTable = dt.TableSource.Tables[0].PhysicalName;
					if (dt.TableSource.Schema != null)
						fromSchema = dt.TableSource.Schema;
					fromConn = dt.TableSource.ConnectionName;
					sourceTables.add(dt);
					sourceTableNames.add(sourceTableName);
					tables.add(dt);
					tableNames.add(sourceTableName);
					sdt = dt;
					break;
				}
			}
			if (sdt == null)
				throw new ScriptException("Data source " + from.getChild(0).getText() + " does not exist", from.getLine(), from.getTokenStartIndex(),
						from.getTokenStopIndex());
			if (sdt.TableSource.Filters != null && sdt.TableSource.Filters.length > 0 && sdt.Imported)
			{
				// Apply security filters
				for (TableSourceFilter tsf : sdt.TableSource.Filters)
					flist.add(tsf);
			}
		}
		SourceQuery sq = new SourceQuery();
		ProjectionlistResult pr = new ProjectionlistResult(r, this, projectionList, tables, null, inputElements, dataElements, isIBorBirstandUnion, null, dconn);
		sq.projectionList = pr.sb.toString();
		sq.isTime = isdbo;
		if (fromSchema != null)
			sq.table = fromSchema + "." + fromTable + " " + fromTable;
		else
			sq.table = dconn == null ? fromTable : (isdbo ? "dbo." + fromTable : Database.getQualifiedTableName(dconn, fromTable)) + " " + fromTable;
		sq.tables = tables;
		sq.sourceTableNames = tableNames;
		sq.physicalTableName = fromTable;
		sq.physicalTableSchema = fromSchema;
		sq.physicalTableConnection = fromConn;
		sq.hasAgg = pr.hasAgg;
		sq.groupByClause = pr.gb.toString();
		sq.distinct = distinct;
		sq.topN = topN;
		sq.dataTypes = pr.dataTypes;
		if (t.getChildCount() == whereCount)
		{
			Tree where = t.getChild(offset + 2).getChild(0);
			sq.whereClause = getCondition(where, definition, tables, tableNames);
		}
		if (flist.size() > 0)
		{
			Object o = r.getRepositoryVariable("USER");
			String uname = o == null ? "USER" : o.toString();
			Session s = new Session(uname);
			rsc = ResultSetCache.getInstance(r.getApplicationIdentifier(), false);
			try
			{
				s.fillSessionVariables(r, rsc);
				for (TableSourceFilter tsf : flist)
				{
					String filter = r.replaceVariables(s, tsf.Filter);
					if (filter.indexOf("Q{") >= 0)
					{
						AbstractQueryString aqs = AbstractQueryString.getInstance(r, filter, r.isUseNewQueryLanguage());
						try
						{
							filter = aqs.replaceQueryString(filter, false, s, true, false, null, null, null);
						} catch (Exception e)
						{
							logger.error(e, e);
						}
					}
					if (sq.whereClause != null)
						sq.whereClause += " AND (" + filter + ")";
					else
						sq.whereClause = "(" + filter + ")";
				}
			} catch (SQLException e)
			{
				logger.error(e, e);
			}
		}
		if (loadNecessaryStagingTables && !isForSourceTablesOnly && r != null)
		{
			/*
			 * Load staging tables if needed
			 */
			LoadTables lt = new LoadTables(r);
			lt.setStagingTableName(fromTable);
			lt.setScriptPath(scriptPath);
			lt.setIgnoreCompleteStep(loadAllBaseTables);
			lt.setIgnoreVisualizationSourceLoad(doNotLoadVisulationSources);
			String[] subGroups = null;
			if (processingGroup != null)
			{
				subGroups = new String[]
				{ processingGroup };
			} 
			boolean result = lt.loadStaging(r.replaceVariables(null, path), r.replaceVariables(null, databasePath), status, false, false, loadGroup, subGroups,
					numSourceRows, false, bulkLoaded, s3client, s3credentials, s3secretKey, r);
			if (!result)
				throw new ScriptException("LoadStaging failed for " + fromTable + " while executing script", t.getLine(), t.getTokenStartIndex(),
						t.getTokenStopIndex());
		}
		// Return query
		return sq;
	}

	public static MeasureTable getGrainMeasureTable(String fromTable, Repository r)
	{
		StagingTable st = null;
		if (fromTable.toLowerCase().startsWith("grain("))
		{
			// Grain() form
			fromTable = fromTable.substring(6, fromTable.length() - 1);
			st = getStagingTable(fromTable, r);
		} else
			// MeasureTable() form
			fromTable = fromTable.substring(13, fromTable.length() - 1);
		for (MeasureTable mt : r.MeasureTables)
		{
			if (!mt.AutoGenerated || (mt.InheritTable != null && mt.InheritTable.length() > 0))
				continue;
			if (st != null && st.isAtMatchingGrain(mt.getGrainInfo(r)))
			{
				// Check to see if this is a generated measure table based on a staging table
				return mt;
			} else if (mt.DisplayName != null && mt.DisplayName.equals(fromTable))
			{
				// Check to see if this is an imported measure table
				return mt;
			}
		}
		return null;
	}

	private void processOutput(Tree t) throws ScriptException
	{
		Tree outputTree = t.getType() == BirstScriptParser.OUTPUT ? t : null;
		if (outputTree == null)
			for (int i = 0; i < t.getChildCount(); i++)
			{
				if (t.getChild(i).getType() == BirstScriptParser.OUTPUT)
				{
					outputTree = t.getChild(i);
					break;
				}
			}
		if (outputTree == null)
			throw new ScriptException("No output destination defined", t.getLine(), t.getTokenStartIndex(), t.getTokenStopIndex());
		outputTableName = outputTree.getChild(0).getText();
		outputTableName = outputTableName.substring(1, outputTableName.length() - 1);
		if (outputTree.getChildCount() > 1)
		{
			Tree outputColumnList = outputTree.getChild(1);
			for (int i = 0; i < outputColumnList.getChildCount(); i++)
			{
				Tree item = outputColumnList.getChild(i);
				OutputDataElement ode = new OutputDataElement();
				ode.setName(item.getChild(0).getText());
				Tree typeitem = item.getChild(1);
				ode.setType(getType(typeitem));
				addOutputDataElement(ode);
			}
		}
	}

	public void addOutputDataElement(OutputDataElement ode)
	{
		outputElements.add(ode);
		dataElements.add(ode);
	}

	public void addInputDataElement(InputDataElement ide)
	{
		inputElements.add(ide);
		dataElements.add(ide);
	}

	public void addVariableDataElement(VariableDataElement vde)
	{
		dataElements.add(vde);
	}

	public DataType getType(Tree typeitem)
	{
		if (typeitem.getChildCount() > 0)
		{
			String tname = typeitem.getText();
			if (tname.equalsIgnoreCase("list"))
			{
				if (typeitem.getChild(0).getChildCount() > 0)
				{
					int size = Integer.valueOf(typeitem.getChild(0).getChild(0).getText());
					return new DataType(tname, typeitem.getChild(0).getText(), size);
				} else
					return new DataType(tname, typeitem.getChild(0).getText(), 0);
			} else if (tname.equalsIgnoreCase("map"))
			{
				int size = 0;
				if (typeitem.getChild(1).getChildCount() > 0)
				{
					size = Integer.valueOf(typeitem.getChild(1).getChild(0).getText());
				}
				return new DataType(tname, typeitem.getChild(1).getText(), size, typeitem.getChild(0).getText());
			} else
			{
				int size = Integer.valueOf(typeitem.getChild(0).getText());
				return new DataType(DType.Varchar, size);
			}
		} else
			return new DataType(typeitem.getText(), 0);
	}

	public DataElement findElement(String name)
	{
		for (DataElement de : dataElements)
		{
			if (de.getName().equals(name))
				return de;
		}
		return null;
	}

	public DataElement findNonInputElement(String name)
	{
		for (DataElement de : dataElements)
		{
			if (de instanceof InputDataElement)
				continue;
			if (de.getName().equals(name))
				return de;
		}
		return null;
	}

	private void processScript(Tree st, List<StagingTable> scriptPath) throws ScriptException
	{
		Tree scriptTree = st.getType() == BirstScriptParser.SCRIPT ? st : null;
		if (scriptTree == null)
			for (int i = 0; i < st.getChildCount(); i++)
			{
				if (st.getChild(i).getType() == BirstScriptParser.SCRIPT)
				{
					scriptTree = st.getChild(i);
					break;
				}
			}
		if (scriptTree == null)
			throw new ScriptException("No script defined", st.getLine(), st.getTokenStartIndex(), st.getTokenStopIndex());
		mainBlock = new StatementBlock(this, scriptTree, scriptPath);
	}

	public int getElementIndex(String name)
	{
		int count = 0;
		for (DataElement de : dataElements)
		{
			if (de.getName().equals(name))
				return count;
			count++;
		}
		return -1;
	}

	public List<DataElement> getDataElements()
	{
		return dataElements;
	}

	public DataElement getNonOutputElement(String name)
	{
		for (DataElement de : dataElements)
		{
			if (OutputDataElement.class.isInstance(de))
				continue;
			if (de.getName().equals(name))
				return de;
		}
		return null;
	}

	public List<DataElement> getNonOutputElements()
	{
		List<DataElement> result = new ArrayList<DataElement>();
		for (DataElement de : dataElements)
		{
			if (OutputDataElement.class.isInstance(de))
				continue;
			result.add(de);
		}
		return result;
	}

	public DataElement getElement(String name)
	{
		for (DataElement de : dataElements)
		{
			if (de.getName().equals(name))
				return de;
		}
		return null;
	}

	public Repository getRepository()
	{
		return r;
	}

	public ScriptResult getScriptResults()
	{
		return sr;
	}

	public void initializeScriptResults()
	{
		this.sr = new ScriptResult();
	}

	public FlattenParentChild getFlattenParentChild()
	{
		if (flattenParentChild == null)
			flattenParentChild = new FlattenParentChild();
		return flattenParentChild;
	}

	public void addParentChild(Object parent, Object child)
	{
		if (flattenParentChild == null)
			flattenParentChild = new FlattenParentChild();
		flattenParentChild.addParentChild(parent, child);
	}

	public ResultSetCache getResultSetCache()
	{
		return rsc;
	}

	public void setResultSetCache(ResultSetCache rsc)
	{
		this.rsc = rsc;
	}

	public DatabaseConnection getDatabaseConnection()
	{
		return dconn;
	}

	public void setDatabaseConnection(DatabaseConnection dconn)
	{
		this.dconn = dconn;
	}

	public int getNumSourceRows()
	{
		return numSourceRows;
	}

	public boolean isLoadNecessaryStagingTables()
	{
		return loadNecessaryStagingTables;
	}

	public void setLoadNecessaryStagingTables(boolean loadNecessaryStagingTables)
	{
		this.loadNecessaryStagingTables = loadNecessaryStagingTables;
	}

	public List<StagingTable> getBulkLoaded()
	{
		return bulkLoaded;
	}

	public void setBulkLoaded(List<StagingTable> bulkLoaded)
	{
		this.bulkLoaded = bulkLoaded;
	}

	public Status getStatus()
	{
		return status;
	}

	public void setStatus(Status status)
	{
		this.status = status;
	}

	public String getDatabasePath()
	{
		return databasePath;
	}
	
	public void setLoadGroups(List<String> loadGroups)
	{
		this.loadGroups = loadGroups;
	}
	
	public List<String> getLoadGroups()
	{
		return loadGroups;
	}
	
	public void setLoadGroup(String loadGroup)
	{
		this.loadGroup = loadGroup;
	}
	
	public String getLoadGroup()
	{
		return loadGroup;
	}

	public void setDatabasePath(String databasePath)
	{
		this.databasePath = databasePath;
	}

	public String getPath()
	{
		return path;
	}

	public void setPath(String path)
	{
		this.path = path;
	}

	public boolean isContainsVariable()
	{
		return containsVariable;
	}

	public void setContainsVariable(boolean containsVariable)
	{
		this.containsVariable = containsVariable;
	}

	public List<String> getSourceTableNames()
	{
		return sourceTableNames;
	}

	public boolean isLogicalQuery()
	{
		return logicalQuery;
	}

	public void setLogicalQuery(boolean logicalQuery)
	{
		this.logicalQuery = logicalQuery;
	}

	public DisplayExpression getDisplayExpression()
	{
		return displayExpression;
	}

	public void setDisplayExpression(DisplayExpression displayExpression)
	{
		this.displayExpression = displayExpression;
	}

	public int getIteration()
	{
		return iteration;
	}

	public void setIteration(int iteration)
	{
		this.iteration = iteration;
	}

	public boolean isIncomplete()
	{
		return incomplete;
	}

	public void setIncomplete(boolean incomplete)
	{
		this.incomplete = incomplete;
	}

	public int getNumResultsetRows()
	{
		return numResultsetRows;
	}

	public void setNumResultsetRows(int numResultsetRows)
	{
		this.numResultsetRows = numResultsetRows;
	}

	public Session getSession()
	{
		return session;
	}

	public void setSession(Session session)
	{
		this.session = session;
	}

	public List<InputDataElement> getInputElements()
	{
		return this.inputElements;
	}

	public void addDefinition(Definition d)
	{
		if (definitions == null)
			definitions = new ArrayList<Definition>();
		definitions.add(d);
	}

	public void resetDefinitions()
	{
		if (definitions == null)
			return;
		for (Definition d : definitions)
			d.setInitialized(false);
	}

	public void resetMainBlock()
	{
		mainBlock = null;
	}

	public QueryResultSet getParentResultSet()
	{
		return parentResultSet;
	}

	public void setParentResultSet(QueryResultSet parentResultSet)
	{
		this.parentResultSet = parentResultSet;
	}

	public boolean hasParentRow()
	{
		return (parentRow.get(Thread.currentThread()) != null);
	}
	
	public int getParentRow()
	{
		return parentRow.get(Thread.currentThread());
	}

	public void setParentRow(int parentRow)
	{
		this.parentRow.put(Thread.currentThread(), parentRow);
	}

	public List<Object[]> getOutputRows()
	{
		return outputRows;
	}

	public void setOutputRows(List<Object[]> outputRows)
	{
		this.outputRows = outputRows;
	}

	public boolean getDoNotLoadVisulationSources()
	{
		return doNotLoadVisulationSources;
	}

	public void setDoNotLoadVisulationSources(boolean doNotLoadVisulationSources)
	{
		this.doNotLoadVisulationSources = doNotLoadVisulationSources;
	}

	public String[] getInputTables()
	{
		String[] sarr = new String[inputTables.size()];
		for (int i = 0; i < sarr.length; i++)
			sarr[i] = getInputTable(inputTables.get(i));
		return sarr;
	}

	private String getInputTable(String tname)
	{
		if (tname.startsWith("DW_DM_"))
		{
			for (DimensionTable dt : r.DimensionTables)
			{
				if (dt.TableSource != null && dt.TableSource.Tables != null && dt.TableSource.Tables.length == 1
						&& dt.TableSource.Tables[0].PhysicalName.equals(tname))
				{
					tname = dt.DimensionName + "." + dt.Level;
					break;
				}
			}
		} else if (tname.startsWith("DW_SF_"))
		{
			for (MeasureTable mt : r.MeasureTables)
			{
				if (mt.TableSource == null || mt.TableSource.Tables == null || mt.TableSource.Tables.length != 1)
					continue;
				if (!mt.TableSource.Tables[0].PhysicalName.equals(tname))
					continue;
				Collection<MeasureTableGrain> graininfo = mt.getGrainInfo(r);
				for (StagingTable st : r.getStagingTables())
				{
					if (st.isAtMatchingGrain(graininfo))
					{
						tname = "Measure Table: " + st.getDisplayName();
						break;
					}
				}
			}
		}
		return tname;
	}

	public String[] getInputColumns()
	{
		String[] result = new String[inputElements.size()];
		for (int i = 0; i < result.length; i++)
		{
			ColData cd = inputElements.get(i).getColdata();
			if (cd != null)
			{
				result[i] = getInputTable(cd.tname) + "." + cd.logicalColumnName;
			}
		}
		return result;
	}

	public String[] getSortColumns()
	{
		if (sortColumns == null)
			return null;
		List<String> resArr = new ArrayList<String>();
		for (int i = 0; i < sortColumns.size(); i++)
		{
			ColData cd = sortColumns.get(i);
			if (cd != null && cd.sortDirection != ColData.SORT_NONE)
			{
				resArr.add(getInputTable(cd.tname) + "." + cd.logicalColumnName);
			}
		}
		String[] result = new String[resArr.size()];
		resArr.toArray(result);
		return result;
	}

	public String[] getSortDirections()
	{
		if (sortColumns == null)
			return null;
		List<String> resArr = new ArrayList<String>();
		for (int i = 0; i < sortColumns.size(); i++)
		{
			ColData cd = sortColumns.get(i);
			if (cd != null && cd.sortDirection != ColData.SORT_NONE)
			{
				resArr.add(cd.sortDirection == ColData.SORT_ASCENDING ? "ASC" : "DESC");
			}
		}
		String[] result = new String[resArr.size()];
		resArr.toArray(result);
		return result;
	}

	public String[] getOutputColumns()
	{
		String[] result = new String[outputElements.size()];
		for (int i = 0; i < result.length; i++)
		{
			OutputDataElement ode = outputElements.get(i);
			result[i] = ode.getName();
		}
		return result;
	}

	public String[] getJoinTypes()
	{
		if (joinTypes == null)
			return null;
		List<String> resArr = new ArrayList<String>();
		for (int i = 0; i < joinTypes.size(); i++)
		{
			resArr.add(joinTypes.get(i));
		}
		String[] result = new String[resArr.size()];
		resArr.toArray(result);
		return result;
	}

	public String[] getJoinConditions()
	{
		if (joinConditions == null)
			return null;
		List<String> resArr = new ArrayList<String>();
		for (int i = 0; i < joinConditions.size(); i++)
		{
			resArr.add(joinConditions.get(i));
		}
		String[] result = new String[resArr.size()];
		resArr.toArray(result);
		return result;
	}

	public String getFormattedBody() throws ScriptException
	{
		if (bodyTree == null)
			return null;
		FormatContext fc = new FormatContext();
		fc.cts = bodyTokenStream;
		return Statement.toString(bodyTree, fc, -1);
	}

	public void addFunction(Function f)
	{
		if (functions == null)
			functions = new HashMap<String, Function>();
		functions.put(f.getName(), f);
	}

	public Function getFunction(String name)
	{
		return functions.get(name);
	}

	public TransformationScript getParentScript()
	{
		return parentScript;
	}

	public void setParentScript(TransformationScript parentScript)
	{
		this.parentScript = parentScript;
	}

	public Object getFunctionResult()
	{
		return functionResult;
	}

	public void setFunctionResult(Object functionResult)
	{
		this.functionResult = functionResult;
	}
	
	public String validForWizard()
	{
		if (hasUnion)
			return "Unable to utilize script wizard: script contains UNION/UNIONALL";
		if (hasTopn)
			return "Unable to utilize script wizard: script contains TOP n - please retry without";
		return null;
	}
	public DisplayExpression getParentDisplayExpression()
	{
		return parentDisplayExpression;
	}

	public void setParentDisplayExpression(DisplayExpression parentDisplayExpression)
	{
		this.parentDisplayExpression = parentDisplayExpression;
	}

	public boolean isCompilingParent()
	{
		return compilingParent;
	}

	public void setCompilingParent(boolean compilingParent)
	{
		this.compilingParent = compilingParent;
	}

	public boolean isInDimensionExpression()
	{
		return inDimensionExpression;
	}

	public void setInDimensionExpression(boolean inDimensionExpression)
	{
		this.inDimensionExpression = inDimensionExpression;
	}

	public int getMaxOutputRows() {
		return maxOutputRows;
	}

	public void setMaxOutputRows(int maxOutputRows) {
		this.maxOutputRows = maxOutputRows;
	}

	public long getMaxStatements() {
		return maxStatements;
	}

	public void setMaxStatements(long maxStatements) {
		this.maxStatements = maxStatements;
	}
	
	public boolean isForSourceTablesOnly() {
		return this.isForSourceTablesOnly;
	}
	
	public OutputStreamWriter getRWriter(String path) throws ScriptException
	{
		if (rOutputMap == null)
			rOutputMap = new HashMap<String, OutputStreamWriter>();
		OutputStreamWriter writer = rOutputMap.get(path);
		if (writer == null)
		{
			RServer rs = r.getRServer();
			if (rs == null)
				throw new ScriptException("R Server Unavailable", 0, 0, 0);
			try
			{
				RFileOutputStream rfos = rs.getOutputStream(path);
				writer = new OutputStreamWriter(rfos);
			} catch (Exception e)
			{
				throw new ScriptException("Unable to open R output path:" + path, 0, 0, 0);
			}
			rOutputMap.put(path, writer);
		}
		return writer;
	}

	
}
