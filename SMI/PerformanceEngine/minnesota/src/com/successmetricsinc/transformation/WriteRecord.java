package com.successmetricsinc.transformation;

import java.io.IOException;

public class WriteRecord extends Statement
{
	public WriteRecord(TransformationScript script, int line, int start, int stop)
	{
		this.script = script;
		this.line = line;
		this.start = start;
		this.stop = stop;
	}

	@Override
	public void execute() throws ScriptException
	{
		try
		{
			script.writeRecord(this);
		} catch (IOException e)
		{
			throw new ScriptException("Unable to write record to output - " + e.getMessage(), line, start, stop);
		}
	}
}
