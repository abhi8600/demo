/**
 * 
 */
package com.successmetricsinc.transformation;


/**
 * @author bpeters
 * 
 */
public class OutputDataElement implements DataElement
{
	private String name;
	private DataType type;
	private Object value;

	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public String getName()
	{
		return name;
	}

	@Override
	public boolean canGet()
	{
		return false;
	}

	@Override
	public boolean canSet()
	{
		return true;
	}

	public void setType(DataType type)
	{
		this.type = type;
	}

	@Override
	public DataType getType()
	{
		return type;
	}

	@Override
	public void setValue(Object o)
	{
		this.value = o;
	}

	@Override
	public Object getValue(Operator lookup) throws ScriptException
	{
		return value;
	}
}
