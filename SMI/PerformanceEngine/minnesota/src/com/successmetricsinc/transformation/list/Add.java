/**
 * 
 */
package com.successmetricsinc.transformation.list;

import java.util.List;

import org.antlr.runtime.tree.Tree;
import org.apache.log4j.Logger;

import com.successmetricsinc.transformation.Assignment;
import com.successmetricsinc.transformation.DataElement;
import com.successmetricsinc.transformation.Expression;
import com.successmetricsinc.transformation.Function;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.Statement;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.transformation.VariableDataElement;
import com.successmetricsinc.transformation.DataType.DType;

/**
 * @author bpeters
 * 
 */
public class Add extends Statement
{
	private VariableDataElement vde;
	private Operator expression;
	private static Logger logger = Logger.getLogger(Statement.class);

	public Add(TransformationScript script, Tree t) throws ScriptException
	{
		this.script = script;
		String vname = t.getChild(0).getText();
		DataElement de = script.findElement(vname);
		if (de == null)
			throw new ScriptException("Unknown variable " + vname + " at line " + t.getLine(), t.getLine(), t.getCharPositionInLine(), t
					.getCharPositionInLine()
					+ t.getText().length());
		if (!VariableDataElement.class.isInstance(de) || ((VariableDataElement) de).getType().type != DType.List)
			throw new ScriptException("Invalid data element " + vname + " at line " + t.getLine(), t.getLine(), t.getCharPositionInLine(), t
					.getCharPositionInLine()
					+ t.getText().length());
		vde = (VariableDataElement) de;
		Expression ex = new Expression(script);
		expression = ex.compile(t.getChild(1));
		if ((expression.getType() == Operator.Type.Boolean && vde.getType().subtype != DType.Boolean)
				|| (expression.getType() == Operator.Type.DateTime && vde.getType().subtype != DType.DateTime && vde.getType().subtype != DType.Date)
				|| (expression.getType() == Operator.Type.Float && vde.getType().subtype != DType.Float)
				|| (expression.getType() == Operator.Type.Integer && vde.getType().subtype != DType.Integer)
				|| (expression.getType() == Operator.Type.Varchar && vde.getType().subtype != DType.Varchar))
			throw new ScriptException("Cannot add incompatible data type to list " + vname + " at line " + t.getLine(), t.getLine(), t.getCharPositionInLine(),
					t.getCharPositionInLine() + t.getText().length());
	}

	@SuppressWarnings("unchecked")
	@Override
	public void execute() throws ScriptException
	{
		Object o = expression.evaluate();
		List<Object> olist = (List<Object>) vde.getValue(null);
		Object val = Assignment.clipToType(o, vde.getType());
		olist.add(val);
		script.curMemory += vde.getType().getInitializedSize();
		if (script.curMemory > (script.maxMemory*0.9))
		{
			logger.warn("ETL Script list is consuming 90% of max limit " + (script.maxMemory) + ". Script execution will fail when max limit is reached.");
		}
		if (script.curMemory > script.maxMemory)
			throw new ScriptException("Attempt to add list element exceeded memory limits", line, start, stop);
	}
}
