/**
 * 
 */
package com.successmetricsinc.transformation.list;

import java.util.List;

import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.transformation.DataElement;
import com.successmetricsinc.transformation.Expression;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.Statement;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.transformation.VariableDataElement;
import com.successmetricsinc.transformation.DataType.DType;

/**
 * @author bpeters
 * 
 */
public class RemoveAt extends Statement
{
	private VariableDataElement vde;
	private Operator expression;

	public RemoveAt(TransformationScript script, Tree t) throws ScriptException
	{
		this.script = script;
		String vname = t.getChild(0).getText();
		DataElement de = script.findElement(vname);
		if (de == null)
			throw new ScriptException("Unknown variable " + vname + " at line " + t.getLine(), t.getLine(), t.getCharPositionInLine(), t
					.getCharPositionInLine()
					+ t.getText().length());
		if (!VariableDataElement.class.isInstance(de) || ((VariableDataElement) de).getType().type != DType.List)
			throw new ScriptException("Invalid data element " + vname + " at line " + t.getLine(), t.getLine(), t.getCharPositionInLine(), t
					.getCharPositionInLine()
					+ t.getText().length());
		vde = (VariableDataElement) de;
		Expression ex = new Expression(script);
		expression = ex.compile(t.getChild(1));
		if (expression.getType() != Operator.Type.Integer)
			throw new ScriptException("RemoveAt requires integer position at line " + t.getLine(), t.getLine(), t.getCharPositionInLine(),
					t.getCharPositionInLine() + t.getText().length());
	}

	@SuppressWarnings("unchecked")
	@Override
	public void execute() throws ScriptException
	{
		Object o = expression.evaluate();
		int index = 0;
		try
		{
			if(o instanceof Integer)
			{
				index = (Integer)o;
			}
			else if(o instanceof Long)
			{
				index = ((Long)o).intValue();
			}
		} catch (Exception e)
		{
			index = 0;
		}
		List<Object> olist = (List<Object>) vde.getValue(null);
		if (index < olist.size())
		{
			olist.remove(index);
			script.curMemory -= vde.getType().getInitializedSize();
		}
	}
}
