package com.successmetricsinc.transformation;

import java.util.ArrayList;
import java.util.List;

import org.antlr.runtime.tree.Tree;

import com.successmetricsinc.transformation.DataType.DType;
import com.successmetricsinc.transformation.operators.OpBoolean;
import com.successmetricsinc.warehouse.StagingTable;

public class IfThenElse extends Statement
{
	private List<IfThen> conditions = new ArrayList<IfThen>();

	private static class IfThen
	{
		public Operator op;
		public StatementBlock sb;
	}

	public IfThenElse(TransformationScript script, Tree t, List<StagingTable> scriptPath) throws ScriptException
	{
		this.script = script;
		for (int i = 0; i < t.getChildCount(); i++)
		{
			IfThen it = new IfThen();
			Tree conditionalTree = t.getChild(i);
			if (conditionalTree.getType() == BirstScriptParser.IFTHEN)
			{
				Tree ifTree = conditionalTree.getChild(0);
				DataType type = Expression.getType(script, ifTree);
				if (type == null)
					throw new ScriptException("Unable to evaulate expression " + ifTree.getText() + ": line " + ifTree.getLine() + ", ", ifTree
							.getLine(), ifTree.getCharPositionInLine(), ifTree.getCharPositionInLine() + ifTree.getText().length());
				if (type.type != DType.Boolean && type.type != DType.Integer && type.type != DType.Float)
					throw new ScriptException("Expression does not return a valid data type: line " + ifTree.getLine() + ", ", ifTree.getLine(),
							ifTree.getCharPositionInLine(), ifTree.getCharPositionInLine() + ifTree.getText().length());
				Expression ex = new Expression(script);
				it.op = ex.compile(conditionalTree.getChild(0));
				it.sb = new StatementBlock(script, conditionalTree.getChild(1), scriptPath);
			} else {
				it.sb = new StatementBlock(script, conditionalTree.getChild(0), scriptPath);
			}
			conditions.add(it);
		}
		this.line = t.getLine();
		this.start = t.getCharPositionInLine();
		this.stop = t.getText().length() + this.start;
	}

	@Override
	public void execute() throws ScriptException
	{
		for (IfThen it : conditions)
		{
			boolean execute = true;
			if (it.op != null)
			{
				Object o = it.op.evaluate();
				execute = OpBoolean.isTrue(o);
			}
			if (execute)
			{
				it.sb.execute();
				break;
			}
		}
	}
}
