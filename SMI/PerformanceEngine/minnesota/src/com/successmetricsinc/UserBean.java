package com.successmetricsinc;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperPrint;

import org.apache.axis2.context.MessageContext;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.log4j.Logger;

import com.successmetricsinc.chart.ChartOptions;
import com.successmetricsinc.messaging.AsynchResult;
import com.successmetricsinc.messaging.AsynchResult.RenderStatus;
import com.successmetricsinc.util.DateUtil;

public class UserBean implements ISubreportState {
	public static Logger logger = Logger.getLogger(UserBean.class);

	protected TimeZone timeZone;
	protected Locale locale;
	protected SimpleDateFormat dateFormat;
	protected SimpleDateFormat dateTimeFormat;
	private Repository r;
	private Map<Object, Integer> subreportState = new HashMap<Object, Integer>();
	private List<JasperPrint> jasperPrintObjects = new ArrayList<JasperPrint>();
	private Map<String, String> asynchReportingKeyToId = new HashMap<String, String>();
	private Map<String, String> asynchReportingIdToResultsPath = new HashMap<String, String>();
	private Map<String, AsynchResult> asynchResults = new HashMap<String, AsynchResult>();
	private String asynchReportingDirectory;
	protected Session repSession;
	protected User user;
	private String id;
	
	public UserBean(){
		this.id = UUID.randomUUID().toString();
	}
	
	public String getId(){
		return id;
	}
	
	public SimpleDateFormat getDateFormat() {
		if(dateFormat == null)
		{
			SimpleDateFormat sdf = DateUtil.getShortDateFormat(getLocale());
			sdf.setTimeZone(getTimeZone());
			sdf.setLenient(false);
			return sdf;
		}
		return dateFormat;
	}
	
	public SimpleDateFormat getDateFormatNoTimeZone(String format) {
		SimpleDateFormat sdf = DateUtil.getShortDateFormat(getLocale());
		sdf.setLenient(false);
		
		if(format != null && !format.isEmpty()){
			sdf.applyPattern(format);
		}
		
		return (SimpleDateFormat)sdf.clone();
	}
	
	public SimpleDateFormat getDateFormatProcessingTimeZone(){
		SimpleDateFormat fmt = (SimpleDateFormat)getDateFormat().clone();
		fmt.setTimeZone(getProcessingTimeZone());
		return fmt;
	}
	public SimpleDateFormat getDisplayFormatDateOrDateTime(boolean isDateTime, String format){
		return isDateTime ? getDateTimeFormat(format) : getDateFormatProcessingTimeZone(format);
	}
	
	public SimpleDateFormat getDefaultFormatDateOrDateTime(boolean isDateTime){
		SimpleDateFormat fmt;
		
		if(isDateTime){
			fmt = (SimpleDateFormat)getDateTimeFormat(DateUtil.DEFAULT_DATETIME_FORMAT);
			fmt.setTimeZone(getProcessingTimeZone());
		}else{
			fmt = getDateFormatProcessingTimeZone(DateUtil.DEFAULT_DATE_FORMAT);
		}
		
		return fmt;
	}
	
	public SimpleDateFormat getDateFormatProcessingTimeZone(String format){
		if (format == null || format.isEmpty())
			return getDateFormatProcessingTimeZone();
		
		SimpleDateFormat sdf = ((SimpleDateFormat)getDateFormatProcessingTimeZone().clone());
		sdf.applyPattern(format);
		return sdf;
	}
	
	public SimpleDateFormat getDateFormat(String format) {
		if (format == null || format.isEmpty() || ChartOptions.DEFAULT_DIMENSION_FORMAT.equals(format))
			return getDateFormat();
		SimpleDateFormat sdf = ((SimpleDateFormat)getDateFormat().clone());
		sdf.applyPattern(format);
		return sdf;
	}

	public SimpleDateFormat getDateTimeFormat() {
		if(dateTimeFormat == null)
		{
			SimpleDateFormat sdf = DateUtil.getShortDateTimeFormat(getLocale());
			sdf.setTimeZone(getTimeZone());
			sdf.setLenient(false);
			return sdf;
		}			
		return dateTimeFormat;
	}
	
	public SimpleDateFormat getDateTimeFormat(String format) {
		// now all dimensions get a default format of "#"
		if (format == null || format.isEmpty() || ChartOptions.DEFAULT_DIMENSION_FORMAT.equals(format))
			return getDateTimeFormat();
		SimpleDateFormat sdf = ((SimpleDateFormat)getDateTimeFormat().clone());
		sdf.applyPattern(format);
		return sdf;
	}

	public Locale getLocale()
	{
		return locale;
	}
	
	public void setLocale(Locale locale)
	{
		this.locale = locale;
	}
	
	public int getDisplayTimeZoneOffset(){
		return getTimeZone().getOffset((new Date()).getTime());
	}
	
	public TimeZone getProcessingTimeZone()
	{
		return getRepository().getServerParameters().getProcessingTimeZone();
	}
	
	public int getProcessingTimeZoneOffset(){
		return getProcessingTimeZone().getOffset((new Date()).getTime());
	}
	
	/**
	 * sets user's display time zone to given tz
	 * @param TimeZone tz 
	 */
	public void setTimeZone(TimeZone tz) {
		this.timeZone = tz;
	}	
	
	/**
	 * @return users's display time zone
	 */
	public TimeZone getTimeZone() {
		if(this.timeZone == null)
			return getRepository().getServerParameters().getDisplayTimeZone();
		return this.timeZone;
	}
	
	public Repository getRepository() {
		return this.r;
	}
	
	public void setRepository(Repository r) {
		this.r = r;
	}
	
	private static Map<Long, UserBean> threadStuff = (Map<Long, UserBean>) Collections.synchronizedMap(new HashMap<Long, UserBean>());
	
	public static void addThreadRequest(Long id, UserBean r) {
		UserBean.threadStuff.put(id, r);
	}
	
	public static void removeThreadRequest(Long id) {
		UserBean.threadStuff.remove(id);
	}

	public static UserBean getParentThreadUserBean() {
		Long l = Long.valueOf(Thread.currentThread().getId());
		
		return UserBean.threadStuff.get(l);
	}

	public void addSubreport(Object key) {
		int counter = getSubreportCount(key);
		counter++;
		subreportState.put(key, Integer.valueOf(counter));
	}

	public int getSubreportCount(Object key) {
		Integer count = subreportState.get(key);
		int counter = count == null ? 0 : count.intValue();
		return counter;
	}

	public void removeSubreport(Object key) {
		Integer count = subreportState.get(key);
		if (count != null && count.intValue() > 1) {
			int counter = count.intValue() - 1;
			subreportState.put(key, Integer.valueOf(counter));
		}
		else {
			subreportState.remove(key);
		}
	}
	
	public void setJasperPrintObject(JasperPrint print) {
		jasperPrintObjects.add(print);
	}
	
	public void clearJasperPrintObject()
	{
		jasperPrintObjects.clear();
	}
	
	public List<JasperPrint> getJasperPrintObject() {
		return jasperPrintObjects;
	}
	
	public Session getRepSession() {
		return repSession;
	}

	public User getUser() {
		if (user == null) 
		{
			String userName = null;
			try {
				MessageContext context1 = MessageContext.getCurrentMessageContext();
				if (context1 != null) {
					HttpServletRequest request = (HttpServletRequest)context1.getProperty(
							HTTPConstants.MC_HTTP_SERVLETREQUEST);
					userName = request.getUserPrincipal().getName();
				}
			}
			catch (Exception e) {
				logger.warn(e, e);
			}
			
			if (userName == null) {
				// bootstrap this in, but don't save anything
				HttpServletRequest request = getHttpServletRequest();
				if (request != null) {
					userName = request.getUserPrincipal().getName();
				}
			}
			if (userName == null)
			{
				logger.debug("Could not find the username - returning null - this is an error");
				return null;
			}
			
			// validate user principal could be two formats: user or user,application
			int idx = userName.indexOf(',');
			if (idx != -1)
				userName = userName.substring(0, idx);
			Repository rep = getRepository();
			if (rep != null && rep.isRepositoryInitialized())
			{
				user = rep.findUser(userName);
				return user;
			}
			return null;
		}
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
		
	// infrastructure for jsp calling of this stuff
	private static ThreadLocal<HttpServletRequest> instance = new ThreadLocal<HttpServletRequest>() {
		protected HttpServletRequest initialValue() { return (null); }
	};
	
	public static HttpServletRequest getHttpServletRequest() {
		if (instance.get() != null) {
			return instance.get();
		}
		return null;
	}

	public static void setHttpServletRequest(HttpServletRequest request) {
		instance.set(request);
	}
	
	public static HttpSession getHttpSession() {
		HttpServletRequest request = getHttpServletRequest();
		if (request != null) {
			return request.getSession(false);
		}
		return null;
	}
	
	public static UserBean getUserBean() {
		UserBean userBean = com.successmetricsinc.UserBean.getParentThreadUserBean();
		
		if (userBean == null)
		{
			HttpSession session = getHttpSession();
			if (session == null) {
				return null;
			}
			userBean = (UserBean) session.getAttribute("userBean");			
		}
		
		if (userBean == null)
		{
			logger.debug("userBean attribute not found in the session");
			return null;
		}

		return userBean;
	}
	
	public void associateQueueIdWithResultsPath(String id, String path) {
		String oldPath = this.asynchReportingIdToResultsPath.put(id, path);
		if (oldPath != null) {
			File file = new File(oldPath);
			if (file.exists())
				file.delete();
			file = new File(path + "_print");
			if (file.exists())
				file.delete();
		}
	}
	
	public String getResultsPathFromQueueId(String id) {
		return this.asynchReportingIdToResultsPath.get(id);
	}
	
	public void cleanUpResults() {
		if (this.asynchReportingDirectory != null) {
			File file = new File(this.asynchReportingDirectory);
			File[] children = file.listFiles();
			for (File path : children) {
				if (path.exists())
					path.delete();
			}
			file.delete();
		}
	}
	
	public void associateReportRenderRequestWithQueueId(String request, String id) {
		this.asynchReportingKeyToId.put(request, id);
	}
	
	public String getQueueIdFromReportRenderRequest(String request) {
		return this.asynchReportingKeyToId.get(request);
	}
	
	public void configureAsynchResult(String id, AsynchResult aResult){
		this.asynchResults.put(id, aResult);
	}
	
	public boolean isAsynchReportCompleted(String id){
		if(asynchResults.containsKey(id)){
			return asynchResults.get(id).getStatus() == RenderStatus.COMPLETED;
		}
		return false;
	}
	
	public void cleanup() {
		cleanUpResults();		
	}
	
	public String getMessageDirectory(Repository r) {
		if (asynchReportingDirectory == null) {
			String path = r.getServerParameters().getApplicationPath();
			File file = new File(path, "temp"); 
			if (!file.exists())
				file.mkdir();
			
			HttpSession session = SessionHelper.getSession();
			if (session != null) {
				file = new File(file, session.getId());
			}
			else {
				try {
					file = File.createTempFile("tmp", "tmp", file);
					file.delete();
					file.mkdir();
				} catch (IOException e) {
					logger.warn(e, e);
				}
			}
			
			asynchReportingDirectory = file.getAbsolutePath();
		}
		return asynchReportingDirectory;
	}
}