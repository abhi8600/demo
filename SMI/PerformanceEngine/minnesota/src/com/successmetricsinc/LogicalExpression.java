package com.successmetricsinc;

import java.io.Serializable;
import java.util.List;

import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XmlUtils;

public class LogicalExpression implements Serializable
{
	public int Type;
    public String AggregationRule;
    public String[][] Levels;
    public String Expression;
    private String Name;
    private String Guid;
  	private String CreatedDate;
  	private String strLastModifiedDate;
  	private String CreatedUsername;
  	private String LastModifiedUsername;
    
    public LogicalExpression(Element e, Namespace ns)
    {
      Name = e.getChildText("Name",ns);
      Guid = e.getChildText("Guid",ns);
      CreatedDate = e.getChildText("CreatedDate",ns);
      strLastModifiedDate = e.getChildText("LastModifiedDate",ns);
      CreatedUsername = e.getChildText("CreatedUsername",ns);
      LastModifiedUsername = e.getChildText("LastModifiedUsername",ns);

      String s = e.getChildText("Type",ns);
      if (s!=null)
      {
        Type = Integer.parseInt(s);
      }
      AggregationRule = e.getChildText("AggregationRule",ns);
      Element lvls = e.getChild("Levels", ns);
      if (lvls != null)
      {
        List l2 = lvls.getChildren();
        Levels = new String[l2.size()][];
        for (int count2 = 0; count2 < l2.size(); count2++)
        {
          Element l3 = (Element) l2.get(count2);				
          List children = l3.getChildren();
          if (children!=null && children.size()>0)
          {
            Levels[count2] = new String[children.size()];
            for (int count3=0;count3<children.size();count3++)
            {
              Levels[count2][count3] = Util.intern(((Element) children.get(count3)).getValue());
            }
          }
        }
      }
      Expression = e.getChildText("Expression",ns);
    }
    
    public Element getLogicalExpressionElement(Namespace ns)
    {
      Element e = new Element("LogicalExpression",ns);

      Repository.addBaseAuditObjectAttributes(e, Guid, CreatedDate, strLastModifiedDate, CreatedUsername, LastModifiedUsername, ns);

      Element child = new Element("Name",ns);
      child.setText(Name);
      e.addContent(child);

      child = new Element("Type",ns);
      child.setText(String.valueOf(Type));
      e.addContent(child);

      if (AggregationRule!=null)
      {
        XmlUtils.addContent(e, "AggregationRule", AggregationRule,ns);
      }

      child = new Element("Levels",ns);
      if (Levels!=null)
      {
        for (String[] lvl1 : Levels)
        {
          Element arrayOfString = new Element("ArrayOfString",ns);
          for (String lvl2 : lvl1)
          {
            Element stringElement = new Element("string",ns);
            stringElement.setText(lvl2);
            arrayOfString.addContent(stringElement);
          }
          child.addContent(arrayOfString);
        }
      }
      e.addContent(child);

      XmlUtils.addContent(e, "Expression", Expression,ns);

      return e;
    }
    
    public String getName()
    {
    	return Name;
    }
}
