/**
 * $Id: WorkQueue.java,v 1.9 2011-02-19 00:32:15 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc;

import java.util.LinkedList;
import org.apache.log4j.Logger;

/**
 * Class to implement a general work queue for multi-threading applications.
 * 
 * @author bpeters
 * 
 */
public class WorkQueue
{
	private static Logger logger = Logger.getLogger(WorkQueue.class);
	
    private final int maxThreads;
    private final LinkedList<Object> queue;
    private final PoolWorker[] threads;
    private int runningThreads;

    public WorkQueue(int maxThreads)
    {
        this.maxThreads = maxThreads;
        queue = new LinkedList<Object>();
        threads = new PoolWorker[maxThreads];
        runningThreads = 0;
        for (int i = 0; i<maxThreads; i++)
        {
            threads[i] = new PoolWorker();
            threads[i].start();
        }
    }

    private synchronized void incrementRunning()
    {
        runningThreads++;
    }

    private synchronized void decrementRunning()
    {
        runningThreads--;
    }

    public void execute(Runnable r)
    {
        synchronized (queue)
        {
            incrementRunning(); 
            queue.addLast(r);
            queue.notify();
        }
    }

    public void kill()
    {
        synchronized (queue)
        {
            for (int i = 0; i<maxThreads; i++)
                queue.addLast(Boolean.FALSE);
            queue.notifyAll();
        }
    }

    public void completeCurrentWork()
    {
        while (runningThreads>0)
        {
            try
            {
                Thread.sleep(1000);
            } catch (InterruptedException ignored)
            {
            }
        }
    }

    private class PoolWorker extends Thread
    {
    	public PoolWorker()
    	{
			this.setName("Pool Worker - " + this.getId());
    	}
    	
        public void run()
        {
            Runnable r = null;
            while (true)
            {
                synchronized (queue)
                {
                    while (queue.isEmpty())
                    {
                        try
                        {
                            queue.wait();
                        } catch (InterruptedException ignored)
                        {
                        }
                    }
                    Object o = queue.removeFirst();
                    if (Boolean.class.equals(o.getClass()))
                    {
                    	decrementRunning();
                        return;
                    } else
                    {
                        r = (Runnable) o;
                    }
                }
                /*
                 * If we don't catch RuntimeException, the pool could leak threads
                 */
                try
                {
                    r.run();
                } catch (Exception e)
                {
                    logger.error("Thread pool runtime exception: "+e.toString(), e);
                } finally {
                    decrementRunning();
                }
            }
        }
    }
}
