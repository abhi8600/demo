/**
 * $Id: GenericFolderEntry.java,v 1.3 2008-03-28 16:08:57 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.successmetricsinc;

/**
 * @author bpeters
 * 
 */
public interface GenericFolderEntry
{
    public String getName();

    public int getNumTypes();

    public String getTypeParameter(int type);

    public String getTypeValue(int type);
}
