
package com.successmetricsinc.sfdc;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.antlr.runtime.tree.Tree;
import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;

import com.sforce.soap.partner.*;
import com.sforce.soap.partner.SforceServiceStub.Error;
import com.sforce.soap.partner.SforceServiceStub.*;
import com.successmetricsinc.Repository;
import com.successmetricsinc.sfdc.SalesforceSettings.SiteType;
import com.successmetricsinc.transformation.ScriptException;

public class SFDCUpdate extends SFDCOperation
{
	private static Logger logger = Logger.getLogger(SFDCUpdate.class);
	private SforceServiceStub stub = null;
	private static final int MAX_BATCH_SIZE = 200; // SFDC maximum
	private List<SObject> sObjects = new ArrayList<SObject>();
	private Set<String> sIds = new HashSet<String>();
	private int count = 0;
	private static final long timeoutInMilliseconds = 10 * 60 * 1000;// 10 minute time out, way too long, but that's okay for now

	private static final String DEFAULT_SFDC_CLIENT_ID = "Birst/";
	private static final String DEFAULT_SFDC_SANDBOX_URL = "https://test.salesforce.com/services/Soap/u/20.0";
	
	public SFDCUpdate(Tree t, Repository r) throws ScriptException
	{
		this.r = r;
		file = r.getRepositoryRootPath() + File.separator + "sforce.xml";
		try
		{
			settings = SalesforceSettings.getSalesforceSettings(file);
		}
		catch(Exception e)
		{
			throw new ScriptException(e.getMessage(), t.getLine(), t.getCharPositionInLine(), t.getCharPositionInLine() + t.getText().length());
		}
	}

	/**
	 * create the salesforce objects from the Map, batch update if needed
	 * 
	 * @param omap
	 * @throws ScriptException
	 */
	public void add(Map<Object, Object> omap) throws ScriptException
	{
		try
		{
			// set up a properly formatted salesforce SObject for Axis2
			SObject sforceObject = new SObject();
			Object t = omap.get("sfdc:type");
			if (t == null)
			{
				throw new ScriptException("UPDATESFDC failure, missing type", -1, -1, -1);
			}
			String type = t.toString();
			sforceObject.setType(type);
			ID id = new ID();
			t = omap.get("sfdc:id");
			if (t == null)
			{
				throw new ScriptException("UPDATESFDC failure, missing id", -1, -1, -1);
			}			
			// deal with 15 character IDs (SFDC IDs for web services are 18 character, but the ones you see in SFDC reports are 15 character)
			String tmp = fixUpSFDCID(t.toString());
			if(sIds.contains(tmp))
			{
				logger.warn("ID " + tmp + " has already been added to the update list for SFDC - will not be added again.");
				return;
			}
			sIds.add(tmp);
			try
			{
				id.setID(tmp);
			}
			catch (RuntimeException rex)
			{
				throw new ScriptException("UPDATESFDC failure, malformed SFDC id: " + tmp + ", " + rex.getMessage(), -1, -1, -1);
			}
			sforceObject.setId(id);
			OMFactory fac = OMAbstractFactory.getOMFactory();
			OMNamespace ns = fac.createOMNamespace("urn:sobject.partner.soap.sforce.com", "birst");		 // name space? urn:sobject.partner.soap.sforce.com	
			for (Object key : omap.keySet())
			{
				if (key.toString().startsWith("sfdc:")) // skip keys that are special to SFDC (id and type for now)
					continue;
				Object value = omap.get(key);
				if (value != null)
				{
					OMElement param = fac.createOMElement(key.toString(), ns);
					param.setText(value.toString());
					sforceObject.addExtraElement(param);
				}
				else
				{
					sforceObject.addFieldsToNull(key.toString());
				}
			}
			// add the object to the list, batch update if needed
			sObjects.add(sforceObject);
			if (sObjects.size() < MAX_BATCH_SIZE)
				return;
		} catch (Exception ex) {
			throw new ScriptException("UPDATESFDC failure: " + ex.getMessage(), -1, -1, -1);
		}
		// update the objects in the list
		update();
	}
	
	/**
	 * update the objects in salesforce
	 * 
	 * @throws ScriptException
	 */
	public void update() throws ScriptException
	{
		if (sObjects.isEmpty())
			return;
		
		UpdateResponse uresp = null;
		int retries = 0;
		do 
		{
			try
			{
				// login if not already logged in 
				if (stub == null)
				{
					// set the default value for client id and sandbox url
					setDefaultsIfRequired();
					login(timeoutInMilliseconds);
					stub = createSforceStub();
				}

				Update update = new Update();
				SObject[] arr = new SObject[sObjects.size()];
				count += sObjects.size();
				sObjects.toArray(arr);
				sObjects.clear(); // clear out the list so we can start the batch again
				update.setSObjects(arr);
				uresp = stub.update(update, sessionHeader, null, null, null, null, null, null, null, null, null, null);
				break;
			}
			catch (Exception ex)
			{
				logger.info("SFDC web service failure (" + retries + " ): " + ex.getMessage());
				if (++retries > 5)
					throw new ScriptException("SFDC web service failure: " + ex.getMessage(), -1, -1, -1);
			}
		} while (true);
		
		if (uresp != null)
		{
			SaveResult[] sr = uresp.getResult();
			if (sr != null)
			{
				boolean success = true;
				StringBuilder sb = null;
				for (int i = 0; i < sr.length; i++)
				{
					if (!sr[i].getSuccess())
					{
						success = false;
						Error[] er = sr[i].getErrors();
						if (er != null)
						{
							for (int j = 0; j < er.length; j++)
							{
								if (sb == null)
									sb = new StringBuilder();
								sb.append(er[j].getMessage());
								sb.append('\n');
							}
						}
					}
				}
				if (!success)
				{
					throw new ScriptException("SFDC web service failure: " + ((sb != null) ? sb.toString() : "No Message"), -1, -1, -1);
				}
			}
		}
	}
	
	private void setDefaultsIfRequired()
	{
		if(SFDCClientID == null || SFDCClientID.trim().length() == 0)
		{
			SFDCClientID = DEFAULT_SFDC_CLIENT_ID;
		}
		
		// if the site type is sandbox and sandbox url is not given
		// put in the default value
		if(settings != null && settings.sitetype != null 
				&& settings.sitetype == SiteType.Sandbox 
				&& sandboxURL == null)
		{
			sandboxURL = DEFAULT_SFDC_SANDBOX_URL;
		}
	}
	
	/**
	 * return the number of objects updated
	 * @return
	 */
	public int getCount()
	{
		return count;
	}

	/*
	 * SFDC IDs that users see are 15 characters, need to convert to 18 for web services api
	 *  1. Divide the 15 char into 3 chunks of 5 chars each
	 *	2. For each character give that position a value of 1 if uppercase, 0 otherwise (lowercase or number)
	 *	3. Combine the bits from each chunk into a 5 bit integer where the rightmost bit is the most significant bit.
	 *		This will yield a number between 0 and 31 for each chunk
	 *	4. Construct an array that contains the sequence of capital letters A-Z and 0-5
	 *	5. Use the integer from each chunk to choose a character from the array
	 *	6. Append the resulting 3 characters, in chunk order, to the end of the 15 char id
	 */
	private String fixUpSFDCID(String s) throws ScriptException
	{
		// a0BA0      00000 0L2ZC  => a0BA0000000L2ZCMA0
		// 01234      01234 01234
		// 2^3 + 2^2, 0,    2^4 + 2^3 + 2^1 => 12, 0, 26 =>  M, A, 0
		final String Base ="ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456";
		//                            1         2         3        
		//                  012345678901234567890123456789012

		String tmp = s.trim();
		if(tmp.length() == 18) {
			return tmp;
		}
		if (tmp.length() != 15)
		{
			throw new ScriptException("UPDATESFDC failure, bad id (incorrect length): " + tmp, -1, -1, -1);
		}
		StringBuilder sb = new StringBuilder(s);
		for(int i = 0; i < 3; i++){
			String chunk = tmp.substring(i*5, i*5+5);
			int value = 0;
			for (int j = 0; j < 5; j++)
			{
				Character ch = Character.valueOf(chunk.charAt(j));
				if (Character.isUpperCase(ch))
					value += (int) Math.pow(2, j);
			}
			sb.append(Base.charAt(value));
		}
		return sb.toString();
	}
}
