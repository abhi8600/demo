package com.successmetricsinc.sfdc;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// XXX need to move httpcomponents, httpclient is end of lifed
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.ByteArrayPartSource;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.StringPart;
import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;

public class Chatter extends SFDCOperation
{
	private static Logger logger = Logger.getLogger(Chatter.class);
	private static String baseURL = "/services/data/v25.0/chatter/feeds/news/me/feed-items"; // news was user-profile, externalize this?
	private static String sfdcURLPattern = "https://[^/?]+\\.(sales|visual\\.)force\\.com/.*"; // externalize this?
	
	private static String sessionId;
	private static String serverURL;
	
	public Chatter(Repository r, String _sessionId, String _serverURL) throws Exception
	{
		this.r = r;
		sessionId = _sessionId;
		serverURL = _serverURL;
	}
	
	private String fixupNewlines(String comment)
	{
		if (comment == null)
			return comment;
		return comment.replace('\r', '\n');
	}

	public void postStatus(String comment) throws Exception
	{
		Part[] parts = {
			new StringPart("text", fixupNewlines(comment))
		};
		postToFeed(parts);
	}
	
	public void postLink(String comment, String link, String urlDescription) throws Exception
	{
		Part[] parts = {
			new StringPart("url", link),
			new StringPart("urlName", urlDescription),
			new StringPart("text", fixupNewlines(comment))
		};
		postToFeed(parts);
	}

	public void postFile(String comment, String fileName, String fileDescription) throws Exception 
	{
		Part[] parts = {
				new StringPart("desc", fileDescription),
				new StringPart("fileName", fileName),
				new StringPart("text", fixupNewlines(comment)),
				new FilePart("feedItemFileUpload", new File(fileName))
		};
		postToFeed(parts);
	}
	
	public void postFile(String comment, String fileName) throws Exception 
	{
		Part[] parts = {
				new StringPart("fileName", fileName),
				new StringPart("text", fixupNewlines(comment)),
				new FilePart("feedItemFileUpload", new File(fileName))
		};
		postToFeed(parts);
	}
	

	public void postFile(String comment, String fileName, byte[] bytes) throws Exception
	{
		Part[] parts = {
				new StringPart("fileName", fileName),
				new StringPart("text", fixupNewlines(comment)),
				new FilePart("feedItemFileUpload", new ByteArrayPartSource(fileName, bytes))
		};
		postToFeed(parts);
	}	

	public void postFile(String comment, String fileName, byte[] bytes, String fileDescription) throws Exception
	{
		Part[] parts = {
				new StringPart("desc", fileDescription),
				new StringPart("fileName", fileName),
				new StringPart("text", fixupNewlines(comment)),
				new FilePart("feedItemFileUpload", new ByteArrayPartSource(fileDescription, bytes))
		};
		postToFeed(parts);
	}
	
	public void postToFeed(Part[] parts) throws Exception
	{
		if (!validSession(sessionId, serverURL))
			throw new Exception("Invalid Salesforce.com Session");
		
		String hostURL = this.stubServerUrl;
		String oauthToken = this.sessionHeader.getSessionId();

		// strip off everything from /services from hostUrl (comes from SOAP API serverURL)
		int index = hostURL.indexOf("/services");
		if (index > 0)
			hostURL = hostURL.substring(0, index);
		logger.info("Posting message to chatter feed");
		String url = hostURL + baseURL;
		internalPostToFeed(url, oauthToken, parts, false);
	}
	
	private void internalPostToFeed(String url, String oauthToken, Part[] parts, boolean redirected) throws Exception
	{
		logger.debug("Chatter:postToFeed arugments: url: " + url + ", OAuthToken: " + oauthToken);
		PostMethod postMethod = null;
		Pattern p = Pattern.compile(sfdcURLPattern);
		Matcher m = p.matcher(url);
	    if (!m.find())
	    	throw new Exception("Bad SFDC URL: " + url);			
		try 
		{
			logger.debug("Chatter:postToFeed arugments: url: " + url + ", OAuthToken: " + oauthToken);
			postMethod = new PostMethod(url);
			postMethod.setRequestEntity(new MultipartRequestEntity(parts, postMethod.getParams()));
			postMethod.setRequestHeader("Authorization", "OAuth " + oauthToken);
			postMethod.addRequestHeader("X-PrettyPrint", "1");
			HttpClient httpClient = new HttpClient();
			httpClient.getParams().setSoTimeout(60000);
			int returnCode = httpClient.executeMethod(postMethod);
			logger.debug("Chatter:postToFeed response: return code: " + returnCode + ", response body: " + postMethod.getResponseBodyAsString());
			if (returnCode == HttpStatus.SC_MOVED_TEMPORARILY)
			{
				// the problem is that visual force integration has a host like 'c.na9.visual.force.com', whereas the chatter feed has a host like 'na9.salesforce.com'
				// could probably just modify, but the redirection seems okay...
				Header hdr = postMethod.getResponseHeader("Location");
				if (hdr != null)
				{
					logger.debug("Chatter:postToFeed: redirecting to: " + hdr.getValue());
					if (!redirected)
						internalPostToFeed(hdr.getValue(), oauthToken, parts, true);
					else
						throw new Exception("attempt to redirect twice, aborting post to chatter feed");
				}
			}
			else if (returnCode != HttpStatus.SC_CREATED) 
			{
				throw new Exception("Chatter:postToFeed: bad return code: " + returnCode);
			}

		} 
		catch (Exception ex) 
		{
			logger.error(ex, ex);
		} 
		finally 
		{
			if (postMethod != null)
				postMethod.releaseConnection();
		}
	}
}