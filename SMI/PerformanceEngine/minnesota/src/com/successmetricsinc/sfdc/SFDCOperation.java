package com.successmetricsinc.sfdc;

import java.io.File;

import org.apache.axis2.client.Options;
import org.apache.axis2.context.ConfigurationContext;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.log4j.Logger;

import com.sforce.soap.partner.SforceServiceStub;
import com.sforce.soap.partner.SforceServiceStub.*;
import com.successmetricsinc.Repository;
import com.successmetricsinc.sfdc.SalesforceSettings.SiteType;
import com.successmetricsinc.util.Emailer;
import com.successmetricsinc.util.Util;

public abstract class SFDCOperation 
{
	protected String stubServerUrl;
	private long timeoutInMilliseconds;
	protected SessionHeader sessionHeader = null;
	protected String file;
	protected Repository r = null;
	protected String SFDCClientID;
	protected String sandboxURL;
	protected SalesforceSettings settings;
	protected CallOptions co = null;
	private Emailer emailer;
	private String uid;
	
	private static Logger logger = Logger.getLogger(SFDCOperation.class);
	
	// used for configuration axis client.
	private static final int MAX_CONNECTIONS_PER_HOST = 25;
	private static final int MAX_TOTAL_CONNECTIONS = 50;
	
	public boolean validSession(String sessionId, String serverURL)
	{
		if (sessionId == null || serverURL == null)
		{
			logger.info("SFDC: sessionId/serverURL null: " + sessionId + " / " + serverURL);
			return false;
		}
		logger.debug("SFDC: validation request: " + sessionId + " / " + serverURL);
		
		SforceServiceStub stub = null;
		// try 10 times
		for (int i = 0; i < 10; i++)
		{
			try
			{
				stub = new SforceServiceStub(serverURL);
				cleanUpIdleConnections(stub);
				if (SFDCClientID != null && SFDCClientID.trim().length() > 0)
				{
					co = new CallOptions();
					co.setClient(SFDCClientID);            
				}
				sessionHeader = new SessionHeader();
				sessionHeader.setSessionId(sessionId);
				GetUserInfo getUserInfo = new GetUserInfo();
				GetUserInfoResponse resp = stub.getUserInfo(getUserInfo, sessionHeader, co);
				if (resp == null || resp.getResult() == null)
				{
					logger.info("SFDC: bad response from getUserInfo: " + sessionId + " / " + serverURL);
					return false;
				}
				stubServerUrl = serverURL;
				logger.debug("SFDC: session is valid for: " + sessionId + " / " + serverURL);
				return true;
			}
			catch (Exception ex)
			{
				logger.warn("SFDC: exception: " + sessionId + " / " + serverURL);
				try
				{
					Thread.sleep(100); // sleep 100 ms and try again
				}
				catch (InterruptedException ex2)
				{}
			}
			finally{
				if(stub != null){
					Util.cleanUpServiceClient(stub._getServiceClient());
				}
			}
		}
		logger.info("SFDC: reached connection exception limit");
		return false;
	}
	
	private void cleanUpIdleConnections(SforceServiceStub stub) {
		if(stub != null && stub._getServiceClient() != null && stub._getServiceClient().getServiceContext() != null && 
				stub._getServiceClient().getServiceContext().getConfigurationContext() != null){
			Util.cleanUpIdleConnections(stub._getServiceClient().getServiceContext().getConfigurationContext());
		}
	}
	
	public void login(long timeoutInMilliseconds) throws Exception
	{
		this.timeoutInMilliseconds = timeoutInMilliseconds;
		SforceServiceStub stub = null;
		try {
			stub = settings.sitetype == SiteType.Sandbox ? new SforceServiceStub(sandboxURL) : new SforceServiceStub();
		} catch (Exception ex) {
			throw new Exception("SFDCOperation: SFDC web service failure: " + ex.getMessage());
		}
		Options options = stub._getServiceClient().getOptions();
		if (SFDCClientID != null && SFDCClientID.trim().length() > 0)
        {
            co = new CallOptions();
            co.setClient(SFDCClientID);            
        }
		options.setProperty(HTTPConstants.MC_ACCEPT_GZIP, Boolean.FALSE);
		options.setProperty(HTTPConstants.MC_GZIP_REQUEST, Boolean.FALSE);
		options.setTimeOutInMilliSeconds(timeoutInMilliseconds);
		String username = settings.getUsername();
		String password = settings.getPassword();
		if (username == null || password == null)
		{
			throw new Exception("SFDCOperation: no saved SFDC username/password");
		}
		Login login = new Login();
		login.setUsername(username);
		login.setPassword(password);
		LoginResponse lresp = null;
		try
		{
			lresp = stub.login(login, null, co);
		}
		catch(Exception ex)
		{
			logger.error(ex.getMessage(), ex);
			sendFailureEmail("Birst - Salesforce Login Failure", "Problem during Salesforce login for space " + r.getSpaceName() + ": " + ex.getMessage());
			throw new Exception("SFDCOperation: SFDC web service failure to login: " + ex.getMessage());
		}
		LoginResult lr	= null;
		lr = lresp.getResult();
		if (lr == null)
		{
			sendFailureEmail("Birst SFDC login failure", "SFDC login failure for user " + username + " for space " + r.getSpaceName());
			throw new Exception("SFDCOperation: SFDC web service login failure: null LoginResult");
		}
		if (lr.getPasswordExpired())
        {
			logger.warn("SFDC password expired for user " + settings.username + " for space " + r.getApplicationIdentifier());
			sendFailureEmail("Birst SFDC login failure", "SFDC login failure for user " + username + " for space " + r.getSpaceName() + ": password expired");
            throw new Exception("SFDCOperation: SFDC password expired for user " + settings.username + " for space " + r.getApplicationIdentifier());
        }
		try
		{
			// now that configContext is created as well as the underlying HttpConnectionManager
			// set the parameters for the connections -- NOTE previous call to login set up the httpConnectionManager
			configureParamsForConnectionManager(stub._getServiceClient().getServiceContext().getConfigurationContext());
			/*
             * Can't login using zip until after authentication - errors not caught correctly if zip is on
             */
			options = stub._getServiceClient().getOptions();
			options.setProperty(HTTPConstants.MC_ACCEPT_GZIP, Boolean.TRUE);
			options.setProperty(HTTPConstants.MC_GZIP_REQUEST, Boolean.TRUE);
			options.setTimeOutInMilliSeconds(timeoutInMilliseconds);
			lresp = stub.login(login, null, co);
			lr = lresp.getResult();
			if (lr == null)
			{
				throw new Exception("SFDCOperation: SFDC web service login failure: null LoginResult");
			}
			// set up the session information (id, endpoint, request options)
			sessionHeader = new SessionHeader();
			sessionHeader.setSessionId(lr.getSessionId());
			stubServerUrl = lr.getServerUrl();
		}
		catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			sendFailureEmail("Birst - Salesforce Extraction Failure", "Problem during Salesforce extraction for space " + r.getSpaceName() + ": " + ex.getMessage());
			throw new Exception("SFDCOperation: SFDC web service failure to login: " + ex.getMessage());
		}
	}
	
	/**
	 * values set on the configurationContext is used for all the 
	 * service clients that are generated for various requests by calling
	 * unless overriden at the serviceClient level 
	 * {@link #createSforceStub()}createSforceStub
	 * @param configurationContext
	 */
	private void configureParamsForConnectionManager( ConfigurationContext configContext) {
		
		if(configContext != null)
		{
			Object obj = configContext.getProperty(HTTPConstants.MULTITHREAD_HTTP_CONNECTION_MANAGER);
			if(obj != null)
			{
				HttpConnectionManagerParams params = new HttpConnectionManagerParams();
				params.setDefaultMaxConnectionsPerHost(MAX_CONNECTIONS_PER_HOST);
				params.setMaxTotalConnections(MAX_TOTAL_CONNECTIONS);
				MultiThreadedHttpConnectionManager connManager = (MultiThreadedHttpConnectionManager)obj;
				connManager.setParams(params);
			}
		}
	}

	/**
	 * create a new instance of SforceServiceStub
	 * @param timeoutInMilliseconds
	 * @throws Exception
	 */
	public synchronized SforceServiceStub createSforceStub() throws Exception
	{
		SforceServiceStub stub = new SforceServiceStub(stubServerUrl);
		Options options = stub._getServiceClient().getOptions();
		options.setProperty(HTTPConstants.MC_ACCEPT_GZIP, Boolean.TRUE);
		options.setProperty(HTTPConstants.MC_GZIP_REQUEST, Boolean.TRUE);
		options.setTimeOutInMilliSeconds(timeoutInMilliseconds);
		return stub;
	}
	
	public void setUid(String uniqueId)
	{
		this.uid = uniqueId;
	}
	
	public void setEmailer(Emailer emailer) {
		this.emailer = emailer;
	}
	
	protected void sendFailureEmail(String subject, String bodyText)
	{
		if(this.emailer != null)
		{	
			emailer.sendEmail(subject, bodyText);
		}
	}
	
	protected void writeSuccessFile()
	{
		String successFileName = null;
		try
		{
			if(uid != null)
			{
				successFileName = r.getRepositoryRootPath() + File.separator + "slogs" + File.separator + "success-extract-" + uid + ".txt";
				File file = new File(successFileName);
				if(!file.exists())
				{	
					file.createNewFile();
				}
			}
		}
		catch(Exception ex)
		{
			logger.warn("Error while creating out the success file file " +  successFileName);
		}
	}
}
