package com.successmetricsinc.sfdc;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import com.successmetricsinc.connectors.ExtractionObject;
import com.successmetricsinc.security.EncryptionService;

public class SalesforceSettings {
	public static int CurrentVersion = 2;
    public static String AMTime = "AM";
    public static String PMTime = "PM";
    public int version;
    public String username;
    public String password;
    public boolean saveAuthentication;
    public boolean automatic;
    public Date nextDate = new Date();
    public SalesforceObjectConnection[] objects;
    public enum SiteType { Production, Sandbox };
    public SiteType sitetype;
    private static EncryptionService svc = EncryptionService.getInstance();
    public SimpleDateFormat lastUpdatedDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    public SimpleDateFormat lastSystemModStampFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    public SimpleDateFormat dateTimeISOStandardFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    public Date startExtractionTime = new Date();
    public Date endExtractionTime = new Date();
    
    public String getUsername() {
    	if (username == null || username.isEmpty())
            return null;
    	return svc.decrypt(username);
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		if (password == null || password.isEmpty())
            return null;
		return svc.decrypt(password);
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isSaveAuthentication() {
		return saveAuthentication;
	}

	public void setSaveAuthentication(boolean saveAuthentication) {
		this.saveAuthentication = saveAuthentication;
	}

	public boolean isAutomatic() {
		return automatic;
	}

	public void setAutomatic(boolean automatic) {
		this.automatic = automatic;
	}

	public Date getStartExtractionTime() {
		return startExtractionTime;
	}

	public Date getEndExtractionTime() {
		return endExtractionTime;
	}

	public void setStartExtractionTime(Date startExtractionTime) {
		this.startExtractionTime = startExtractionTime;
	}

	public void setEndExtractionTime(Date endExtractionTime) {
		this.endExtractionTime = endExtractionTime;
	}

	public SalesforceObjectConnection[] getObjects() {
		return objects;
	}

	public boolean hasObjects() {
		return (objects != null && objects.length > 0);
	}
	
	public SalesforceObjectConnection getObjectConnection(String name)
    {
        SalesforceObjectConnection soc = null;
        lastUpdatedDateFormat.setLenient(false);
    	lastSystemModStampFormat.setLenient(false);
        if (objects == null)
        {
            objects = new SalesforceObjectConnection[1];
            soc = new SalesforceObjectConnection(name);
            objects[0] = soc;
            return soc;
        }
        for (SalesforceObjectConnection ssoc : objects)
        {
            if (ssoc.name.equals(name))
                return ssoc;
        }
        soc = new SalesforceObjectConnection(name);
        List<SalesforceObjectConnection> old = new ArrayList<SalesforceObjectConnection>(Arrays.asList(objects));
        old.add(soc);
        objects = old.toArray(new SalesforceObjectConnection[old.size()]);
        return soc;
    }
	
	public static SalesforceSettings getSalesforceSettings(String filename) throws Exception {
		SAXBuilder builder = new SAXBuilder();
		Document d = null;
		d = builder.build(new File(filename));
		SalesforceSettings settings = new SalesforceSettings();
		Element salesforceSettings = d.getRootElement();
		String temp = salesforceSettings.getChildText("version");
		if (temp != null && !temp.trim().isEmpty())
		{
			settings.version = Integer.valueOf(temp);
		}
		temp = salesforceSettings.getChildText("username");
		if (temp != null && !temp.trim().isEmpty())
		{
			settings.setUsername(temp);
		}
		temp = salesforceSettings.getChildText("password");
		if (temp != null && !temp.trim().isEmpty())
		{
			settings.setPassword(temp);
		}
		temp = salesforceSettings.getChildText("saveAuthentication");
		settings.setSaveAuthentication(Boolean.parseBoolean(temp));
		temp = salesforceSettings.getChildText("automatic");
		settings.setAutomatic(Boolean.parseBoolean(temp));
		temp = salesforceSettings.getChildText("sitetype");
		if (temp != null && !temp.trim().isEmpty())
		{
			settings.sitetype = temp.equals("Production") ? SiteType.Production : SiteType.Sandbox;
		}
		
		temp = salesforceSettings.getChildText("startExtractionTime");
		if(temp != null && !temp.trim().isEmpty())
		{
			settings.setStartExtractionTime(settings.dateTimeISOStandardFormat.parse(temp));
		}
		 
		temp = salesforceSettings.getChildText("endExtractionTime");
		if(temp != null && !temp.trim().isEmpty())
		{
			settings.setEndExtractionTime(settings.dateTimeISOStandardFormat.parse(temp));
		}    	
		Element objects = salesforceSettings.getChild("objects");
		if(objects != null)
		{
			List lstObjects = objects.getChildren("SalesforceObjectConnection");
			if(lstObjects != null)
			{
				settings.objects = new SalesforceObjectConnection[lstObjects.size()];
				for (int count = 0; count < lstObjects.size(); count++)
				{
					settings.objects[count] = settings.new SalesforceObjectConnection((Element) lstObjects.get(count));
				}
			}
		}
		return settings;
    }
	
	public void updateCredentials(String settingsFile) throws Exception
	{
		File sFile = new File(settingsFile);
		if (!sFile.exists())
		{
			createInitialSettingsFile(settingsFile);
		}
		SAXBuilder builder = new SAXBuilder();
		Document d = null;
		d = builder.build(new File(settingsFile));
		Element salesforceSettings = d.getRootElement();
		salesforceSettings.getChild("version").setText(String.valueOf(SalesforceSettings.CurrentVersion));
		if (this.saveAuthentication)
		{
			EncryptionService enc = EncryptionService.getInstance();
			salesforceSettings.getChild("username").setText(enc.encrypt(this.username));
			salesforceSettings.getChild("password").setText(enc.encrypt(this.password));
			salesforceSettings.getChild("sitetype").setText(this.sitetype.toString());
		}
		salesforceSettings.getChild("saveAuthentication").setText(String.valueOf(this.saveAuthentication));
		salesforceSettings.getChild("automatic").setText(String.valueOf(this.automatic));
	}
	
	public void createInitialSettingsFile(String settingsFile) throws Exception
	{
		File sFile = new File(settingsFile);
		if (!sFile.exists())
		{
			sFile.createNewFile();			
		}
		XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
		xmlOutputter.getFormat().setEncoding("utf-8");
		xmlOutputter.getFormat().setOmitDeclaration(false);
		xmlOutputter.getFormat().setOmitEncoding(false);
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(settingsFile)));
		bw.write("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
		bw.newLine();
		Element salesforceSettings = new Element("SalesforceSettings");
		salesforceSettings.addNamespaceDeclaration(Namespace.getNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance"));
		salesforceSettings.addNamespaceDeclaration(Namespace.getNamespace("xsd", "http://www.w3.org/2001/XMLSchema"));
		Element version = new Element("version");
		version.setText(String.valueOf(SalesforceSettings.CurrentVersion));
		salesforceSettings.addContent(version);
		if (this.saveAuthentication)
		{
			EncryptionService enc = EncryptionService.getInstance();
			Element username = new Element("username");
			username.setText(enc.encrypt(this.username));
			salesforceSettings.addContent(username);
			Element password = new Element("password");
			password.setText(enc.encrypt(this.password));
			salesforceSettings.addContent(password);
			Element sitetype = new Element("sitetype");
			sitetype.setText(this.sitetype.toString());
			salesforceSettings.addContent(sitetype);
		}
		Element saveAuthentication = new Element("saveAuthentication");
		saveAuthentication.setText(String.valueOf(this.saveAuthentication));
		salesforceSettings.addContent(saveAuthentication);
		Element automatic = new Element("automatic");
		automatic.setText(String.valueOf(this.automatic));
		salesforceSettings.addContent(automatic);
		Element objects = new Element("objects");
		salesforceSettings.addContent(objects);
		Element startExtractionTime = new Element("startExtractionTime");
		startExtractionTime.setText(dateTimeISOStandardFormat.format(this.startExtractionTime));
		salesforceSettings.addContent(startExtractionTime);
		Element endExtractionTime = new Element("endExtractionTime");
		endExtractionTime.setText(dateTimeISOStandardFormat.format(this.endExtractionTime));
		salesforceSettings.addContent(endExtractionTime);
		xmlOutputter.output(salesforceSettings, bw);
		bw.flush();
		bw.close();
	}

	public class SalesforceObjectConnection
    {
    	public String name;
        public String query;
        public String extractionGroupNames;
        public Date lastUpdatedDate;
        public Date lastSystemModStamp;
        public String[] columnNames;
        public Boolean includeAllRecords;
        public transient int noRetries = 0;
        public static final int SUCCESS = 1;
        public static final int FAILED = -1;
        public static final int NOT_EXTRACTED = 0;
        public transient int status = NOT_EXTRACTED;
        
        
        public SalesforceObjectConnection()
        {
        }

        public SalesforceObjectConnection(String name)
        {
            this.name = name;
        }
        
        public SalesforceObjectConnection(Element e) throws Exception
        {
        	name = e.getChildText("name");
        	query = e.getChildText("query");
        	includeAllRecords = e.getChildText("includeAllRecords") == null ? false : Boolean.valueOf(e.getChildText("includeAllRecords"));
        	String temp = e.getChildText("lastUpdatedDate");
        	if (temp != null)
        	{
        		lastUpdatedDate = lastUpdatedDateFormat.parse(temp);
        	}
        	temp = e.getChildText("lastSystemModStamp");
        	if (temp != null)
        	{
        		lastSystemModStamp = lastSystemModStampFormat.parse(temp);
        	}
        	temp = e.getChildText(ExtractionObject.EXT_GROUP_NAMES);
        	if (temp != null) {
        		setExtractionGroupNames(temp);
        	}
        	Element colNamesElement = e.getChild("columnNames");
        	if (colNamesElement != null)
        	{
	        	List colNames = colNamesElement.getChildren("string");
	        	columnNames = new String[colNames.size()];
	        	for (int count = 0; count < colNames.size(); count++)
	        	{
	        		columnNames[count] = ((Element) colNames.get(count)).getText();
	        	}
        	}
        }
        
        public void addColumnName(String s)
        {
            if (columnNames == null)
            {
                columnNames = new String[1];
                columnNames[0] = s;
            }
            else
            {
                boolean found = false;
                for (String cs : columnNames)
                {
                    if (s.equals(cs))
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    List<String> old = new ArrayList<String>(Arrays.asList(columnNames));
                    old.add(s);
                    columnNames = old.toArray(new String[old.size()]);
                }
            }
        }
        
        public String getExtractionGroupNames() {
        	return extractionGroupNames;
        }
        
        public void setExtractionGroupNames(String extGroupNames) {
        	extractionGroupNames = extGroupNames;
        }
    }
}