package com.successmetricsinc.sfdc;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.axiom.om.OMElement;
import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import com.sforce.soap.partner.SforceServiceStub;
import com.sforce.soap.partner.SforceServiceStub.*;
import com.successmetricsinc.CommandSession;
import com.successmetricsinc.ExecuteSteps;
import com.successmetricsinc.ExecuteSteps.Step;
import com.successmetricsinc.ExecuteSteps.StepCommand;
import com.successmetricsinc.ExecuteSteps.StepCommandType;
import com.successmetricsinc.Repository;
import com.successmetricsinc.sfdc.SalesforceSettings.SalesforceObjectConnection;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.warehouse.StagingTable;

public class SFDCDataExtractor extends SFDCOperation
{
	private static final Date minDateValue;
	private static final long timeoutInMilliseconds = 30 * 60 * 1000;
	private static final Logger logger = Logger.getLogger(SFDCDataExtractor.class);
	private BlockingQueue<SalesforceObjectConnection> queue;
	private List<SalesforceObjectConnection> statusList;
	private boolean allObjectsExtracted = false;
	private int noExtractionThreads;
	private ExtractionThread[] threads;
	private static final int maxRetries = 5;	
	private static final long retrySleepInMilliseconds = 10 * 1000; 
	
	static
	{
		Calendar c = Calendar.getInstance();
		c.set(1, 0, 1, 0, 0, 0);
		c.set(Calendar.MILLISECOND, 0);
		minDateValue = c.getTime();
	}
	
	public static void main(String[] args) throws Exception
	{
		CommandSession cmdsession = new CommandSession(true);
		List<Step> steps = new ArrayList<Step>();
		steps.add(new Step(StepCommand.Repository, StepCommandType.Init, args[0]));
		ExecuteSteps es = new ExecuteSteps(steps, cmdsession);
		es.start();
		es.join();
		if (es.isError())
		{
			throw new Exception("SFDCDataExtractor: Exception initializing repository");
		}
		Repository r = cmdsession.getRepository();
		if (r == null)
		{
			throw new Exception("SFDCDataExtractor: Repository invalid");
		}		
		
		SFDCDataExtractor sfExtractor = new SFDCDataExtractor(r, args[1], args[2].equalsIgnoreCase("null") ? null : args[2], Integer.parseInt(args[3]));
		if(args.length > 4)
		{
			Util.startCheckIn(args[4]);
		}
		sfExtractor.extractData();
	}
	
	public SFDCDataExtractor(Repository r, String SFDCClientID, String sandboxURL, int noExtractionThreads) throws Exception
	{
		this.r = r;
		file = r.getRepositoryRootPath() + File.separator + "sforce.xml";
		settings = SalesforceSettings.getSalesforceSettings(file);
		this.SFDCClientID = SFDCClientID;
		this.sandboxURL = sandboxURL;
		this.noExtractionThreads = noExtractionThreads;
		queue = new LinkedBlockingQueue<SalesforceObjectConnection>(settings.objects.length); 
		queue.addAll(Arrays.asList(settings.objects));
		statusList = new ArrayList<SalesforceObjectConnection>();
	}
	
	public void extractData() throws Exception
	{	
		clearOutKillLockFile();
		login(timeoutInMilliseconds);
		startExtraction();
		waitTillExtractionCompletes();
		saveSettings();
		if(allObjectsExtracted)
		{
			writeSuccessFile();
		}
	}
	
	private void startExtraction()
	{	
		resetSFDCQueryLog();
		if(settings != null)
		{
			settings.setStartExtractionTime(new Date());
		}
		threads = new ExtractionThread[noExtractionThreads];
		for (int i = 0; i < noExtractionThreads; i++)
        {
            threads[i] = new ExtractionThread();
            threads[i].start();
            logger.debug("Thread started " + threads[i].getName());
        }
	}
	
	private void waitTillExtractionCompletes()
	{
		logger.debug("Waiting for threads to join");
		for (int i = 0; i < noExtractionThreads; i++)
		{
			try
			{
				threads[i].join();
			} catch (InterruptedException e)
			{
				logger.error(e);
			}
			finally
			{
				if(settings != null)
				{
					settings.setEndExtractionTime(new Date());
				}
			}
		}
	}
	
	private class ExtractionThread extends Thread
	{
		public void run()
		{
			logger.debug("Starting Thread " + this.getName());
			SforceServiceStub stub = null;
			// The same stub can be used by the same thread over multiple objects
			try
			{		
				stub = createSforceStub();
			}
			catch(Exception ex)
			{
				logger.error("Exception while create sforce stub ", ex);
				return;
			}
			while(!queue.isEmpty())			
			{
				try
				{					
					if (logger.isTraceEnabled())
						logger.trace("Retreiving next object from queue");
					boolean result = false;
					SalesforceObjectConnection soc = queue.poll(); //queue.take();
					if(soc == null)
					{
						if (logger.isTraceEnabled())
							logger.trace("No object found in queue");
						return;
					}
					try
					{
						logger.info("Starting extract for object : " + soc.name);
						long startTime = System.currentTimeMillis();
						getObject(stub, soc);
						long timeTakenInSeconds = (System.currentTimeMillis() - startTime);
						logger.info("Successfully extracted object " + soc.name + " : Total Time taken : " + timeTakenInSeconds + "(milliseconds)");
						soc.status = SalesforceObjectConnection.SUCCESS;						
					}
					catch (RecoverableException ex)
					{	
						if (soc.noRetries < maxRetries)
						{
							logger.warn("Recoverable Exception encountered extracting object " + soc.name + " - "+ ex.getMessage() + " (retrying)");
							soc.noRetries++;
							logger.debug("Retrying extraction for object " + soc.name + ": number of retries: " + soc.noRetries);
							Thread.sleep(retrySleepInMilliseconds);
							queue.offer(soc);
							continue;
						}
						else
						{	
							logger.error("Recoverable Exception encountered extracting object  " + soc.name + " - maximum number of retries reached, marking extraction as failed : " + ex.getMessage(), ex);
							sendFailureEmail("Birst - Salesforce Extraction Failure", "Problem during Salesforce extraction for space " + r.getSpaceName() + ": " + ex.getMessage() + " (maximum number of retries reached)");
							soc.status = SalesforceObjectConnection.FAILED;
						}
					}
					catch (UnrecoverableException ex)
					{
						logger.error("Unrecoverable Exception encountered extracting object " + soc.name + " - "+ ex.getMessage(), ex);	    
						sendFailureEmail("Birst - Salesforce Extraction Failure", "Problem during Salesforce extraction for space " + r.getSpaceName() + ": " + ex.getMessage());
						soc.status = SalesforceObjectConnection.FAILED;
					}	                
					statusList.add(soc);
				}
				catch(Exception ex)
				{
					sendFailureEmail("Birst - Salesforce Extraction Failure", "Problem during Salesforce extraction for space " + r.getSpaceName() + ": " + ex.getMessage());
					logger.error(ex, ex);
				}
				finally
                {
					String killFileName = Util.getKillFileName(r.getRepositoryRootPath(), Util.KILL_EXTRACT_FILE); 
                	if(Util.isTerminated(killFileName))
                	{
                		Util.exitNow(killFileName);
                	}
                }
			}
		}
	}
	
	private void clearOutKillLockFile()
	{
		Util.deleteFile(Util.getKillFileName(r.getRepositoryRootPath(), Util.KILL_EXTRACT_FILE));
	}
	
	private class RecoverableException extends Exception
	{
		public RecoverableException(String message, Throwable cause)
		{
			super(message, cause);
		}
	}
		
	private class UnrecoverableException extends Exception
	{
		public UnrecoverableException(String message, Throwable cause)
		{
			super(message, cause);
		}
	}
	
	private void getObject(SforceServiceStub stub, SalesforceObjectConnection soc) throws RecoverableException, UnrecoverableException
	{
		logger.info("Space " + r.getApplicationIdentifier() + " - object retrieval process started for SFDC object: " + soc.name);
		BufferedWriter writer = null;
		Date maxLastModified = new Date(minDateValue.getTime());
		StringBuilder query = new StringBuilder();
		try
    	{
	        boolean first = true;
	        int lastModifiedField = -1;
	        StagingTable curst = null;
	        DescribeSObjectResult dresult = null;
	        for (StagingTable st : r.getStagingTables())
	        {
	            if (st.Name.equals("ST_" + soc.name))
	            {
	                curst = st;
	                break;
	            }
	        }        
	        if (soc.query == null || soc.query.trim().isEmpty())
	        {
	        	DescribeSObject dsObject = new DescribeSObject();
	        	dsObject.setSObjectType(soc.name);
	        	DescribeSObjectResponse dResponse = stub.describeSObject(dsObject, sessionHeader, co, null, null);
	        	if (dResponse != null)
	        	{
	        		dresult = dResponse.getResult();
	        		query.append("SELECT ");
	                for (Field f : dresult.getFields())
	                {
	                    if (first)
	                        first = false;
	                    else
	                        query.append(',');
	                    query.append(f.getName());
	                }
	                query.append(" FROM " + soc.name);
	                for (int i = 0; i < dresult.getFields().length; i++)
	                {
	                    Field f = dresult.getFields()[i];
	                    if (f.getName().equals("SystemModstamp"))
	                    {
	                        lastModifiedField = i;
	                        break;
	                    }
	                }
	                if (!soc.lastSystemModStamp.equals(minDateValue) && lastModifiedField >= 0 && curst != null && curst.Transactional)
	                {
	                    // Incremental load only
	                    query.append(" WHERE SystemModstamp > " + new SimpleDateFormat("yyyy-MM-ddThh:mm:sszzz").format(soc.lastSystemModStamp));
	                }
	                
	                writeMappingFile(soc.name, dresult);
	        	}                	
	        }
	        else
	        {
	            query.append(soc.query);
	        }	        
	        String soqlQuery = replaceGetVariableWithOldSyntax(query.toString());
	        soqlQuery = r.replaceVariables(null, soqlQuery);
	        writeSFDCQuery(soc.name, dresult, soqlQuery);
	        Query q = new Query();
	        q.setQueryString(soqlQuery);
	        QueryOptions qOptions = new QueryOptions();
	        qOptions.setBatchSize(2000);
	        soc.lastUpdatedDate = new Date();
	        QueryResponse qResp = stub.query(q, sessionHeader, co, qOptions, null, null);
	        QueryResult qr = null;
	        String fname = r.getRepositoryRootPath() + File.separator + "data" + File.separator + soc.name + ".txt";    		
	        if (qResp != null)
	        {
	        	qr = qResp.getResult();
	        }
	        if (qr != null && qr.getSize() > 0)
	        {	        	
        		writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fname, false), "UTF-8"));
        		first = true;
        		List<String> cnames = new ArrayList<String>();
        		for (OMElement e : qr.getRecords()[0].getExtraElement())
        		{
        			if (first)
                        first = false;
                    else
                        writer.write('|');
        			String n = getAllowable(e.getLocalName(), false);        			
                    int count = 0;
                    // Fix to deal with duplicate column names (which SFDC allows)
                    while (cnames.contains(count == 0 ? n : (n + count)))
                    {
                        count++;
                        if (count > 100)
                            break;
                    }
                    n = count == 0 ? n : (n + count);
                    cnames.add(n);
                    writer.write(n);
                    soc.addColumnName(n);
        		}
        		writer.newLine();
        		boolean done = false;
        		int numRecords = 0;
        		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                while (!done)
                {
                	for (int i = 0; i < qr.getRecords().length; i++)
                    {
                        numRecords++;
                        first = true;
                        for (int j = 0; j < qr.getRecords()[i].getExtraElement().length; j++)
                        {
                            if (first)
                                first = false;
                            else
                                writer.write('|');
                            String s = qr.getRecords()[i].getExtraElement()[j].getText();
                            if (s.contains("\r") || s.contains("\n") || s.contains("\"") || s.contains("|") || s.contains("\\"))
                            	writer.write('"' + s.replace("\\", "\\\\").replace("\"", "\\\"").replace("|", "\\|") + '"');
                            else
                                writer.write(s);
                        }
                        if (lastModifiedField >= 0)
                        {
                            try
                            {
                                Date dt = sdf.parse(qr.getRecords()[i].getExtraElement()[lastModifiedField].getText());
                                if (dt.after(maxLastModified))
                                    maxLastModified = dt;
                            }
                            catch (ParseException pe)
                            {
                            	logger.error("Cannot parse lastModifiedField : " + qr.getRecords()[i].getExtraElement()[lastModifiedField].getText());
                            }
                        }
                        writer.newLine();                        
                    }
                	done = qr.getDone();
                	if (!done)
                    {
                    	QueryMore qMore = new QueryMore();
                    	qMore.setQueryLocator(qr.getQueryLocator());
                    	QueryMoreResponse qmResp = stub.queryMore(qMore, sessionHeader, co, qOptions);
                    	if (qmResp != null)
                    	{
                    		qr = qmResp.getResult();
                    	}
                    }
                }
                soc.lastSystemModStamp = maxLastModified;
	        }
	        else
	        {
	        	File file = new File(fname);
	        	if(file.exists())
	        	{
	        		file.delete();
	        	}
	        	
	        	file.createNewFile();
	        	logger.info("No data returned for SFDC object " + soc.name + " - " + query + " (creating empty stub file)");
	        }
        }
    	catch (Exception e)
    	{
    		logger.error("Problem extracting SFDC object - " + soc.name + " - " + query);
    		if (e instanceof IOException || e instanceof com.sforce.soap.partner.InvalidQueryLocatorFault)
    		{
    			throw new RecoverableException(e.getMessage() + ": " + query, e);
    		}else
    		{
    			throw new UnrecoverableException(e.getMessage() + ": " + query, e);
    		}
    	}
    	finally
    	{
    		try
    		{
    			if (writer != null)
    				writer.close();
    		}
    		catch(IOException e)
    		{
    			logger.error("Problem closing file after extracting SFDC object - " + soc.name + " - " + query);
    			throw new RecoverableException(e.getMessage(), e);
    		}
    	}    	
	}
	
	private String replaceGetVariableWithOldSyntax(String str)
	{	
		String modifiedString = str;
		if (this.r != null && r.isUseNewQueryLanguage())
		{
			if (modifiedString != null && modifiedString.length() > 0)
			{
				boolean found = true;
				while (found)
				{
					String getVariableStringBegin = "GETVARIABLE('";
					String getVariableStringEnd = "')";
					int startIndex = modifiedString.toUpperCase().indexOf(getVariableStringBegin);
					if (startIndex == -1)
					{
						found = false;
						break;
					} else
					{
						int endIndex = modifiedString.toUpperCase().indexOf(getVariableStringEnd, startIndex);
						if (endIndex > 0)
						{
							String preGetVariable = modifiedString.substring(0, startIndex);
							String getVariable = modifiedString.substring(startIndex+ getVariableStringBegin.length(), endIndex);
							String postGetVariable = modifiedString.substring(endIndex + getVariableStringEnd.length());
							modifiedString = preGetVariable + "V{"	+ getVariable + "}" + postGetVariable;
						} else
						{
							found = false;
							break;
						}
					}
				}
			}
		}
		return modifiedString;
	}
	
	private void writeMappingFile(String objectName, DescribeSObjectResult response) throws IOException {
		if(response != null)
		{
			BufferedWriter writer = null;
			String objectMappingFileName = null;
			try
			{
				objectMappingFileName = getSFDCObjectMappingFilePath(objectName);
				writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(objectMappingFileName, false), "UTF-8"));
				boolean first = true;
				for (Field f : response.getFields())
				{
					if(!first)
					{
						writer.write(',');
					}
					
					writer.write(f.getName() + "=" + f.getLabel() + "=" + f.getType().getValue().equals("id"));
					if(first)
						first = false;
				}
				logger.debug("Successfully written mapping file : " + objectMappingFileName);
			}finally
			{
				try
				{
					if(writer != null)
						writer.close();
				}
				catch(Exception ex2)
				{
					logger.warn("Error while closing writer for " + objectMappingFileName);
				}
			}
		}

	}

	private void resetSFDCQueryLog()
	{
		File spaceQueryLogFileName = new File(getSFDCQueryLogFilePath());
		if (spaceQueryLogFileName.exists())
		{
			BufferedWriter bw = null;			
			try
			{
				bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(spaceQueryLogFileName), "UTF-8"));
				bw.write(r.getApplicationName() + " : " + r.getApplicationIdentifier());
			}
			catch (Exception ex)
            {
                logger.error("Exception in clearing file " + spaceQueryLogFileName, ex);
            }
			finally
			{
				if (bw != null)
				{
					try
					{
						bw.close();
					}
					catch (Exception ex2)
                    {
                        logger.error("Exception in releasing resource for " + spaceQueryLogFileName, ex2);
                    }
				}
			}
		}
	}
	
	private String getSFDCObjectMappingFilePath(String objectName)
	{
		String spaceLogsDirectory = r.getRepositoryRootPath() + File.separator + "mapping";
        File f = new File(spaceLogsDirectory);
        if (!f.exists() || !f.isDirectory())
        {
            f.mkdir();
        }
        return (spaceLogsDirectory + File.separator + objectName + "-mapping.txt");
	}
	
	private String getSFDCQueryLogFilePath()
	{
		String spaceLogsDirectory = r.getRepositoryRootPath() + File.separator + "logs";
        File f = new File(spaceLogsDirectory);
        if (!f.exists() || !f.isDirectory())
        {
            f.mkdir();
        }
        return (spaceLogsDirectory + File.separator + "sfdcQueryLog.txt");
	}
	
	synchronized private void writeSFDCQuery(String objectName, DescribeSObjectResult describeObjectResult, String query) throws Exception
    {
        BufferedWriter writer = null;
        String queryLogFileName = null;
        try
        {
            queryLogFileName = getSFDCQueryLogFilePath();                
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(queryLogFileName, true), "UTF-8"));

            writer.write("");
            writer.newLine();
            writer.write("Object Name : " +  objectName);
            writer.newLine();
            writer.write("Time : " + new Date().toString());
            writer.newLine();
            writer.write("Query :");
            writer.newLine();
            writer.write(query);                
            
            if(describeObjectResult != null)
            {
                writer.write("Field Descriptions : (Name, Label, Type, Length, Precision, Scale)");
                for (Field field : describeObjectResult.getFields())
                {                        
                    writer.write(field.getName());
                    writer.write(",");
                    writer.write(field.getLabel());
                    writer.write(",");
                    writer.write(field.getType().toString());                        
                    writer.write(",");
                    writer.write(field.getLength());
                    writer.write(",");
                    writer.write(field.getPrecision());
                    writer.write(",");
                    writer.write(field.getScale());
                    writer.newLine();
                }
            }            
        }
        catch (Exception ex)
        {
            logger.error("Exception in writing query information to the " + queryLogFileName, ex);
            throw new Exception("SFDCDataExtractor: Exception in writing query information to the " + queryLogFileName);
        }
        finally
        {
            if (writer != null)
            {
                try
                {
                    writer.close();
                }
                catch (Exception ex2)
                {
                    logger.warn("Unable to close writer for " + queryLogFileName, ex2);
                }
            }
        }
    }
	
	private void saveSettings() throws Exception
	{		
		if (statusList != null && statusList.size() == settings.objects.length)
		{
			for (SalesforceObjectConnection soc : statusList)
			{
				if (soc.status != SalesforceObjectConnection.SUCCESS)
				{
					logger.debug("Extraction failed for one or more objects - not saving settings");
					allObjectsExtracted = false;
					break;
				}
				allObjectsExtracted = true;
			}
		}
		else
		{
			allObjectsExtracted = false;
			logger.error("Cannot figure out status of extraction for one or more objects");
			throw new Exception("SFDCDataExtractor: Cannot figure out status of extraction for one or more objects");
		}
		BufferedWriter bw = null;
		try
		{
			SAXBuilder builder = new SAXBuilder();
			Document d = null;
			d = builder.build(new File(file));
			Element salesforceSettings = d.getRootElement();
			Namespace ns = salesforceSettings.getNamespace();
			Element startExtractionTimeElement = salesforceSettings.getChild("startExtractionTime");
			if(startExtractionTimeElement == null)
			{
				startExtractionTimeElement = new Element("startExtractionTime", ns);
			}
			startExtractionTimeElement.setText(settings.dateTimeISOStandardFormat.format(settings.getStartExtractionTime()));
			Element endExtractionTimeElement = salesforceSettings.getChild("endExtractionTime");
			if(endExtractionTimeElement == null)
			{
				endExtractionTimeElement = new Element("endExtractionTime", ns);
			}
			endExtractionTimeElement.setText(settings.dateTimeISOStandardFormat.format(settings.getEndExtractionTime()));

			Element objects = salesforceSettings.getChild("objects");
			List lstObjects = objects.getChildren("SalesforceObjectConnection");
			for (int count = 0; count < lstObjects.size(); count++)
			{
				Element e = ((Element)lstObjects.get(count));
				String name = e.getChildText("name");
				for (SalesforceObjectConnection soc : statusList)
				{
					if (soc.name.equals(name) && soc.status == SalesforceObjectConnection.SUCCESS)
					{
						for (Object ob : e.getChildren())
						{
							Element ce = (Element)ob;
							if (ce.getName().equals("lastUpdatedDate"))
							{
								ce.setText(settings.lastUpdatedDateFormat.format(soc.lastUpdatedDate));
							}
							else if (ce.getName().equals("lastSystemModStamp"))
							{
								ce.setText(settings.lastSystemModStampFormat.format(soc.lastSystemModStamp));
							}
							else if (ce.getName().equals("columnNames"))
							{
								ce.removeContent();
								for (String colName : soc.columnNames)
								{
									Element col = new Element("string", ns);
									col.setText(colName);
									ce.addContent(col);
								}							
							}
						}
						if (e.getChild("columnNames") == null && soc.columnNames != null)
						{
							//add columnNames node
							Element child = new Element("columnNames", ns);
							for (String colName : soc.columnNames)
							{
								Element col = new Element("string", ns);
								col.setText(colName);
								child.addContent(col);
							}
							e.addContent(child);
						}
					}
				}
			}
			XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
			xmlOutputter.getFormat().setEncoding("utf-8");
			xmlOutputter.getFormat().setOmitDeclaration(false);
			xmlOutputter.getFormat().setOmitEncoding(false);
			bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
			bw.write("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
			bw.newLine();
			xmlOutputter.output(salesforceSettings, bw);
		}
		catch (Exception ex)
		{
            logger.error("Exception in saving " + file, ex);
            throw new Exception("SFDCDataExtractor: Exception in saving " + file);
        }
		finally
		{
			if (bw != null)
				bw.close();
		}
		logger.debug("Successfully extracted " + settings.objects.length + " SFDC objects for Space: " + r.getApplicationIdentifier());
	}
	
	public static char[] new_lang_allowable = new char[] { 
        '_', ' ', ':', '$', '#', '%', '-', '/', '&', '!', 
        '^', '@', ',', ';', '>', '<'};
	public static char[] old_lang_allowable = new char[] { 
        '!', '@', '#', '$', '%', '^', '&', '*', '/', '<', 
        '>', ',', '|', '=', '+', '-', '_', ';',':'};
	char[] allowable = new_lang_allowable;
	
	private String getAllowable(String f, boolean allowSpaces)
    {
        StringBuilder sb = new StringBuilder();
        // Restrict the characters that are allowed
        for (int i = 0; i < f.length(); i++)
        {
            if ((f.charAt(i) >= 'a' && f.charAt(i) <= 'z') ||
                (f.charAt(i) >= 'A' && f.charAt(i) <= 'Z') ||
                (f.charAt(i) >= '0' && f.charAt(i) <= '9') ||
                (allowSpaces && f.charAt(i) == ' '))
            {
                sb.append(f.charAt(i));
                continue;
            }
            boolean ok = false;
            for (char c : allowable)
                if (f.charAt(i) == c)
                {
                    ok = true;
                    break;
                }
            if (!ok)
                sb.append('_');
            else
                sb.append(f.charAt(i));
        }
        return sb.toString();
    }
}
