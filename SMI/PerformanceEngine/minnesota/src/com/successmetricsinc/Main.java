/**
 * $Id: Main.java,v 1.258 2012-11-22 06:46:55 BIRST\mjani Exp $
 *
 * Copyright (C) 2007-2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.WriterAppender;
import org.apache.log4j.xml.DOMConfigurator;
import org.jdom.JDOMException;

import com.successmetricsinc.ExecuteSteps.Step;
import com.successmetricsinc.ExecuteSteps.StepCommand;
import com.successmetricsinc.ExecuteSteps.StepCommandType;
import com.successmetricsinc.chart.ChartRendererFactory;
import com.successmetricsinc.query.DataUnavailableException;
import com.successmetricsinc.query.WebServerConnectInfo;
import com.successmetricsinc.util.RepositoryException;
import com.successmetricsinc.util.Util;

/**
 * @author Brad Peters
 * 
 */
public class Main
{
	public static final String DEFAULT_LOG_FORMAT = "%n%d{yyyy-MM-dd HH:mm:ss,SSSZ} [%t] %-5p - %m";
	static Repository r = null;
	static Connection conn = null;
	public static String catalogDir = null;
	static int NUM_THREADS = 4;
	private static Logger logger = Logger.getLogger(Main.class);
	private static WriterAppender defaultLogfileAppender = null;
	private static FileAppender commandLineLogfileAppender = null;
	private static Level loglevel = Level.ALL;
	private static boolean foundProcessGroup = true;
	private static boolean restoreConsoleAppenderAtEnd = false;
	
	// Local ELT calling this method to set repository object
	public static void setRepository(Object repository) throws ClassCastException
	{
		r = (Repository)repository;
	}

	public static void main(String[] args)
	{
		List<String> argList = new ArrayList<String>(Arrays.asList(args));
		CommandSession cmdsession = null;
		String logFilename = null;
		PrintStream resultsWriter = null;
		String processGroup = null;
		String log4j_Override = System.getProperty("log4j.Override");
		if (log4j_Override != null)
		{
			DOMConfigurator.configure(log4j_Override);
		}
		defaultLogfileAppender = (WriterAppender) Logger.getRootLogger().getAppender("logfile");
		setupDefaultLogAppender();
		try
		{
			String serverString = null;
			if (argList != null && argList.size() > 0 && argList.contains("-version"))
			{
				System.out.println(Util.getBuildString());
				System.out.println("Engine Repository Version: " + Repository.RepositoryVersion);
				int index = argList.indexOf("-version");
				argList.remove(index);
			}
			/*
			 * Allow -s username/password@server option which will direct everything to a server
			 */
			if (argList != null && argList.size() > 0 && argList.get(0).equals("-s") && argList.size() > 2)
			{
				serverString = argList.get(1);
				argList.remove(0);
				argList.remove(0);
			}
			List<String> tokens = null;
			if (argList.contains("-variables"))
			{
				int index = argList.indexOf("-variables") + 1;
				if (argList.size() <= index)
				{
					throw new Exception("Option -variables must be followed by variable settings, -variables a=1,b=5");
				}
				String variable = argList.get(index);
				argList.remove(index - 1);
				argList.remove(index - 1);
				StringTokenizer st = new StringTokenizer(variable, ",");
				while (st.hasMoreTokens())
				{
					String[] split = st.nextToken().split("=");
					if (split != null && split.length == 2)
					{
						logger.debug("adding variable " + split[0] + " " + split[1]);
						System.setProperty("smi." + split[0], split[1]);
					}
				}
			}
			if (argList.contains("-loglevel") || argList.contains("loglevel"))
			{
				int index = argList.indexOf("-loglevel") + 1;
				if (index == 0)
					index = argList.indexOf("loglevel") + 1;
				if (argList.size() <= index)
				{
					throw new Exception("Option -loglevel must be followed by a level (ALL, TRACE, DEBUG, INFO, WARN, ERROR, FATAL, OFF)");
				}
				loglevel = Level.toLevel(argList.get(index), loglevel);
				argList.remove(index - 1);
				argList.remove(index - 1);
			}
			// Set logging to console
			if (argList.contains("-con") || argList.contains("-console"))
			{
				setupCommandLineConsoleAppender();
				int index = argList.indexOf("-con");
				if (index == -1)
					index = argList.indexOf("-console");
				argList.remove(index);
			}
			// Set logging to file
			if (argList.contains("-logfile"))
			{
				int index = argList.indexOf("-logfile") + 1;
				if (argList.size() <= index)
				{
					throw new Exception("Option -logfile must be followed by a log file name.");
				}
				logFilename = argList.get(index);
				logFilename = logFilename.replace("\"", "");
				File f = new File(logFilename);
				File dir = f.getParentFile();
				if ((dir != null) && (!dir.exists()))
				{
					dir.mkdir();
				}
				setupCommandLineLogfileAppender(logFilename);
				argList.remove(index - 1);
				argList.remove(index - 1);
			}
			int idx = argList.indexOf("-resout"); // see if there is a results out file
			if (idx != -1)
			{
				File resout = new File(argList.get(idx + 1));
				try
				{
					resultsWriter = new PrintStream(new FileOutputStream(resout, true),true,"UTF-8");
				} catch (Exception e)
				{
					// ignore;
				}
				argList.remove(idx - 1);
				argList.remove(idx - 1);
			}
			String home = System.getProperty("smi.home");
			if (home == null || home.isEmpty())
			{
				logger.error("smi.home not defined (java -Dsmi.home=...), stuff will probably not work");
			}
			/*
			 * If the first argument is -c then use a command file (form is -cCommandFilename)
			 */
			if (argList.size() > 1 && argList.get(0).equals("-c"))
			{
				String fname = argList.get(1);
				BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fname), "UTF-8"));
				StringBuilder buff = new StringBuilder();
				String line = null;
				if (argList.size() > 3 && (argList.get(2).equals("-g") || argList.get(2).equals("-group")) && (argList.get(3).length() > 0))
				{
					processGroup = argList.get(3);
					foundProcessGroup = false;
				}
				// Read command lines, // and -- signify lines that are comments
				while ((line = reader.readLine()) != null)
				{
					if (!line.startsWith("//") && !line.startsWith("--"))
						buff.append(line + "\n");
				}
				tokens = tokenize(buff.toString(), processGroup, true);
			}
			/*
			 * Otherwise, process list of command line tokens
			 */
			else
			{
				tokens = argList;
			}
			if (serverString != null)
			{
				int a = serverString.indexOf('/');
				int b = serverString.indexOf('@');
				if (a < 0 || b < 0)
				{
					System.out.println("Could not process server connect string. Use: username/password@server");
					return;
				}
				WebServerConnectInfo sc = new WebServerConnectInfo();
				sc.serverUsername = serverString.substring(0, a);
				sc.serverPassword = serverString.substring(a + 1, b);
				sc.serverURL = serverString.substring(b + 1);
				StringBuilder sb = new StringBuilder();
				for (String s : tokens)
				{
					if (s.contains(" "))
						sb.append("\"" + s + "\" ");
					else
						sb.append(s + " ");
				}
				// System.out.println(Remote.executeRemoteCommand(sc, sb.toString()));
				return;
			}
			// Create new command session
			cmdsession = new CommandSession(true);
			if (r != null) // bugzilla# 11590 wanted to skip repository command for local ETL  
				cmdsession.setRepository(r);
			cmdsession.setLogFile(logFilename);
			if (resultsWriter != null)
			{
				cmdsession.setResultsWriter(resultsWriter);
			}
			Logger.getRootLogger().setLevel(loglevel);
			logger.setLevel(loglevel);
			Util.logRuntimeInfo();
			processTokens(tokens, cmdsession, true);
			if (!foundProcessGroup)
			{
				logger.error("Did not find any commands for the process group: " + processGroup);
			}
		} catch (JDOMException e)
		{
			logger.info("Error loading the repository - " + e.getMessage());
			logger.error(e.getMessage(), e);
		} catch (IOException e)
		{
			logger.info(e.getMessage());
			logger.error(e.getMessage(), e);
		} catch (RepositoryException ex)
		{
			logger.info("Error loading the repository - " + ex.getMessage());
			logger.error(ex.getMessage(), ex);
		} catch (DataUnavailableException du)
		{
			if (du.getCause() != null)
			{
				logger.error(du.getCause().getMessage(), du);
			} else
			{
				logger.error(du.getMessage(), du);
			}
		} catch (Exception ex)
		{
			logger.info(ex.getMessage());
			logger.error(ex.getMessage(), ex);
		} finally
		{
			if (cmdsession != null && cmdsession.getResultsWriter() != cmdsession.getWriter())
			{
				cmdsession.getResultsWriter().close();
			}
			if (commandLineLogfileAppender != null)
			{
				Logger.getRootLogger().removeAppender(commandLineLogfileAppender);
				setupDefaultLogAppender();				
			}
			if (restoreConsoleAppenderAtEnd)
			{
				Logger.getRootLogger().addAppender(defaultLogfileAppender);
			}
			if (r != null)
				r.closeAllDBConnections();
			else if (cmdsession != null && cmdsession.getRepository() != null)
				cmdsession.getRepository().closeAllDBConnections();
		}
		// Workaround for strange Java bug
		try
		{
			Thread.sleep(100);
		} catch (Exception e)
		{
		}
		if(!Boolean.parseBoolean(System.getProperty(ExecuteSteps.DO_NOT_END_JVM_ON_COMPLETION)))
		{
			System.exit(0);
		}
	}

	private static void printHelp()
	{
		System.out.println("Birst Performance Engine\n");
		System.out.println("Repository Version: " + Repository.RepositoryVersion);
		System.out.println("Syntax (output version information):");
		System.out.println("   run -version");
		System.out.println("Syntax (retrieving commands from a command file):");
		System.out
				.println("   run [-version] -c <command file> -logfile <log file name> -loglevel <level> -variables \"a=1,b=2,...\" -g <group name> [-notify email] [console]");
		System.out.println("Syntax (commands explicitly listed on command line):");
		System.out.println("   run [command]* [console] [loglevel <level>]\n\n");
		System.out.println("Syntax (commands executed on a remote server):");
		System.out.println("   run [-version] -s username/password@server [other commands]\n\n");
		System.out.println("       where <level> is one of ALL, TRACE, DEBUG, INFO, WARN, ERROR, FATAL, OFF");
		System.out.println("Commands:");
		System.out
				.println("repository <Filename> [noaggregates]\n\tLoad the repository from a given filename. noaggregates will not process aggregates as part of repository initialization.");
		System.out.println("\nQuery commands");
		System.out.println("echo string\tEcho a string to the log file as well as console");
		System.out.println("runquery <Logical Query> [splitcacheon <Dimension.ColumnName>] [formatresults] [printcacheusage] [printutf8]\n\tRun a given logical query"
				+ " and output results to standard output, optionally split cache on a given dimension column. Another optional parameter"
				+ " formatresults can be used to display numbers using underlying measure formats e.g. display 1,223,456,789.33 instead of 1.22345678933E9.");
		System.out.println("explainnavigation <Logical Query>\n\tExplain the navigation for a given query");
		System.out.println("explainonfailure\n\tExplain the attempted navigation if failure");
		System.out.println("generatephysicalquery <Logical Query> [<abstractschema>]\n\tGenerate the physical query for a given logical query without acutally running the query. (AbstractSchema argument is optional to replace original schema with Connection name.)");
		System.out.println("explainnavigationagg <Aggregate Name>\n\tExplain the navigation for the logical query for a given repository aggregate");
		System.out.println("generatephysicalqueryagg <Aggregate Name>\n\tGenerate the physical query for the logical query for a given repository aggregate");
		System.out.println("translatequery <oldlogicalquery>\n\tTranslate an old logical query to a a new logical query (does not handle display operations (DO, DF), evaluation operations (EVAL), or constant operations (CONST)");
		System.out
				.println("exportquery <Logical Query> <file> [separator <separator>]\n\tRun a logical query and send it to a file, using an optional separator (default is TAB)");
		System.out.println("\nBusiness Modeling commands");
		System.out
				.println("getmodeldataset <Success Model Name> [createaggregates [persist [persistsubqueries]|remove]] [filter <Filter Name>]\n\tGenerates a dataset for a given success model\n\tOptionally, instead of getting the data, instantiate aggregates based on the success model and persist to or remove from the database if needed");
		System.out.println("writemodeldataset <Success Model Name> [start,stop]\n\tWrites the data for a given dataset to standard output (for range of rows)");
		System.out.println("summarizedataset <Success Model Name>\n\tDisplays key summary statistics for a given dataset");
		System.out.println("findrules <Success Model Name>\n\tGenerate a set of common rules for values within a dataset");
		System.out
				.println("rankfactors <Success Model Name> [mincorr=XX] [minlift=XX] [mindev=XX]\n\tRank the importance of values within a dataset. Optional filter for: minimum correlation, lift or standard deviation");
		System.out
				.println("analyzeoutcome <Outcome Definition Name> [iteration <iteration>] [writecsv] [writetable] [bulkload] [usedatasetsegments]\n\tProcesses a given outcome model (if necessary) and outputs scores");
		System.out
				.println("generateoutcomemodels <Outcome Definition Name> [iteration <comma separated list of iterations>]\n\tProcesses a given outcome model only");
		System.out
				.println("outputimpacts <Outcome Definition Name> [sample] [summary]\n\tAfter a value model is built, output projected impacts. Optionally do that for a sample and/or summarize");
		System.out.println("getoutcomemodelstats <Outcome Name>\n\tGenerate statistics and descriptions for models created for each segment of an outcome");
		System.out.println("createoutcomegroup <Outcome Group Name>\n\tCombines the output tables for several outcomes into one group table");
		System.out.println("findsegments <Segment Definition Name>\tFind segments based on a segment definition");
		System.out.println("scoresegments <Segment Definition Name>\tScore segments based on a segment definition");
		System.out.println("generatepeers <reference population> [writecsv] [writetable]\n\tProcess reference population definitions to create peer sets");
		System.out.println("getperformancemodel <modelname>\n\tGenerates the performance model dataset and builds the performance model");
		System.out.println("optimizeperformance <modelname>\n\tRun performance optimizations");
		System.out.println("optimizeperformancelist <modelname> <filename>\n\tRun performance optimizations on target ids found in a specified file");
		System.out
				.println("optimizeperformancedb <modelname> V{var}\n\tRun performance optimizations on target ids found in the results of the specified query");
		System.out.println("generateperformancetable <modelname>\n\tUtilizes the performance model to output performance results");
		System.out
				.println("generateperformancetablelist <modelname> <filename>\n\tUtilizes the performance model to output performance results for specified target ids");
		System.out
				.println("generatesegmentlists <Success Pattern Definition> [writecsv] [writetable]\n\tGenerate lists of targets that satisfy success pattern segments");
		System.out.println("searchpatterns <Pattern Search Definition>\tSearch for success patterns based on a repository search definition");
		System.out.println("\nAggregate commands");
		System.out
				.println("createqueryaggregate <Logical Query> <Name> [persist]\n\tCreate a new aggregate table with a given name based on a logical query, and [optional] persist it to the database");
		System.out
				.println("dropqueryaggregate <Name> [persist]\n\tDrops the query aggregate with a given name and [optional] drops the table associated with the query aggregate");
		System.out.println("updatequeryaggregate <Name>\n\tIncrementally updates the query aggregate");
		System.out
				.println("persistaggregate <Name> [persistsubqueries] [retaindqt]\n\tPhysically persist a repository-based aggregate table, optional parameter persistsubqueries to persist sub-queries for the main aggregate query for performance improvement. optional parameter retaindqt to retain derived query tables for debugging purposes.");
		System.out.println("persistaggregates [loadgroup=<loadgroup name> [subgroup=<subgroup name>]]\n\tpersist all aggregates (defined in the Aggregates section of the meta data)");
		System.out.println("aggregatelogicalquery <Name>\n\tOutput the aggregate logical query");
		System.out.println("aggregatephysicalquery <Name>\n\tOutput the aggregate physical query");
		System.out.println("instantiateaggregate <Name>\n\tInstantiate the meta data for a repository-based aggregate");
		System.out
				.println("updateaggregate <Name> [persistsubqueries] [loadgroup <loadgroup-name>]\n\tIncrementally update a repository-based aggregate table, , optional parameter persistsubqueries to persist sub-queries for the main aggregate query for performance improvement, optional parameter to apply given load group's filter condition");
		System.out
				.println("dropaggregate <Name> [persist]\n\tDrops the aggregate with a given name and [optional] drops the table associated with the aggregate");
		System.out.println("dropaggregates [persist]\n\tDrops the aggregates and [optional] drops the table associated with the aggregate");
		System.out.println("instantiateview <Table name> [persist]\n\tInstantiates an opaque view table source as a physical table and [optional] persists it to the database");
		System.out.println("dropview <Table name>\n\tDrops a physical instantiation of an opaque view");
		// repackaging of existing commands for better comprehension
		System.out.println("createmodelaggregates <name> [persistsubqueries]\n\tcreate aggregates for the model, persist if they do not exist");
		System.out.println("deletemodelaggregates <name>\n\tdelete aggregates for the model");
		System.out.println("\nCache commands");
		System.out.println("removecachefile <Filename> \n\tRemove a given cache file from cache");
		System.out.println("clearcache \n\tClear all cache entries from cache and reinitializes repository variables");
		System.out.println("profilecache [<Filename>|console]\n\tCreate a profile of current cache to a given output file or to console");
		System.out.println("compareprofiles <Filename1> <Filename2>\n\tCompare two cache profiles and identify differences");
		System.out.println("scriptcache [<Filename>|console]\n\tCreate a script that will populate current cache");
		System.out
				.println("removecacheforquery <QueryString> [exactMatchOnly] [reseed] \n\tRemove cache entries from cache for a given query - optional parameter is exactMatchOnly, optinal parameter to reseed the given query (does not regenerate all entries removed - just executes the given query again)");
		System.out
				.println("removecacheforlogicaltable <LogicalTableName> \n\tRemove cache entries from cache for all queries that refer to the given logical table name");
		System.out
				.println("removecacheforphysicaltable <PhysicalTableName> \n\tRemove cache entries from cache for all queries that refer to the given physical table name");
		System.out
				.println("generatecache <Output dir name> <Working dir name> <Output filename prefix> <Key parameter name> <Key file name> <Query file name>\n\tBegin cache seeding");
		System.out.println("runseedquery <Logical Query>\n\tRun a given logical query to seed the cache");
		System.out.println("viewcachefile <Filename>\n\tOutput the contents of a result set cache or row map cache file to standard output");
		System.out.println("\nParameter setting commands");
		System.out.println("setdefaultconnection <value>\n\tSets the name of the default connection to use");
		System.out.println("setrowlimit <value>\n\tSets the result set row size limit to <value>, overriding the value in the repository");
		System.out
				.println("setvariable <name> <value>\n\tSets the repository variable named <name> to the value <value>, overriding the value set during repository initialization.  Creates the variable if it does not exist.");
		System.out.println("\nFlow commands");
		System.out.println("thread\n\tBreak and spawn a new thread");
		System.out.println("group <Name>\n\tIdentifies a new thread group");
		System.out.println("completework\n\tFinish executing all current threads before proceeding in command file");
		System.out.println("exitonerrors\n\tExits after errors (the default)");
		System.out.println("continueonerrors\n\tContinues after errors");
		System.out.println("silent true|false\n\tTurn off/on query results\n");
		System.out.println("exec <system command>\n\tExecute a system command\n");
		System.out.println("touch <filename>\n\tTouches file specified - updates file modified time\n");
		System.out.println("errorcontinuepoint\n\tContinue point if there is an error\n");
		System.out.println("\nCleanup commands");
		System.out.println("removefiles\n\tRemoves the .mod, .dat, and .arff files from the 'path' directory");
		// meta-commands
		System.out.println("\nMeta commands");
		System.out.println("resetmodelrun\n\tremove all modeling files, modeling aggregates, and modeling opaque views");
		System.out.println("runpeers [writecsv] [writetable]\n\tProcess reference population definitions to create peer sets");
		System.out.println("runperformance <variable>\n\trun the performance models, variable is a repository variable that expands into a list of targets");
		System.out
				.println("runmodels [writecsv] [writetable] [bulkload]\n\trun all success models marked as 'runnable' (inherited models may not be 'runnable')");
		System.out.println("createoutcomegroups\n\tcreate all outcome groups");
		// Warehouse commands
		System.out.println("\nWarehouse commands");
		System.out.println("generateschema <Load ID> [notime] [skipmissingvalues] [execonchange=command] [databasepath <database-path>]\n\tVerify all generated tables; generate or update tables as needed using a given load id "
				+ "(must be int), optionally excludes time dimension and/or missing value records. Also, can execute a command if schema changes");
		System.out
				.println("addpartition <ending Load ID> <directory>\n\tCreate a new partition for new data loads from now through <ending Load ID>.  Put the partition in a file in <directory>.");
		System.out.println("generatetimeschema <Load ID>\n\tGenerate time dimension tables");
		System.out
				.println("loadstaging <Source file path> <Load ID> [loadgroup <loadgroup-name>] [subgroups <comma separated list of subgroups>] [databasepath <database-path>] [numrows <#rows to load>] [debug] [skiptransformations]\n\tLoad staging tables from source files "
						+ "(in designated directory) using a given load id (must be an integer), optional parameter loadgroup to process the staging tables for a given load group only.\n\t"
						+ "optional paramter to specify the path the database should use for data files (if remote), optional # rows to load, all if blank, optional skiptransformation \n"
						+ "parameter will skip execution of staging transformations");
		System.out
				.println("createsample <Source file path> <Dimension> <LevelName> <Staging Table Name> <percent>\n\tCreate a sample set for a given dimension level of a given percent using a source");
		System.out
				.println("derivesample <Source file path> <SourceDimension> <SourceLevelName> <TargetDimension> <TargetLevelName> <Staging Table Name>\n\tCreate a sample set for a given dimension level using another dimension\n\tlevel sample and a staging table as cross reference");
		System.out
				.println("applysample <Source file path> <Dimension> <LevelName> <Staging Table Name>\n\tApply sampling for a given level to a staging table source");
		System.out
				.println("removesample <Source file path> <Dimension> <LevelName> <Staging Table Name>\n\tRemove sampling for a given level from a staging table source");
		System.out.println("showsamples <Source file path>\n\tShow all samples present in a path");
		System.out.println("deletesamples <Source file path>\n\tDelete all created sample files in a path");
		System.out
			.println("loadwarehouse <Load ID> [loadgroup <loadgroup-name>] [subgroups <comma separated list of subgroups>] [scddate <SCD Date>] [nofactindices] [keepfacts] [onlymodifiedsources] [databasepath]\n\tLoad warehouse from staging tables using a given load id (must be integer)"
				+ ", optional parameter loadgroup to process the staging tables for a given load group only. Optional to load only from modified sources.");
		System.out.println("deletedata <Load ID>|ALL\n\tDelete loaded fact tables using a given load id (must be integer) or all data");
		System.out.println("deletealldata \n\tDelete all loaded tables");
		System.out.println("deletespace \n\tDelete all loaded tables and folders");
		System.out.println("enforcesnapshotpolicy\n\tEnforce snapshot policy");
		System.out
				.println("executetransformation <Script> <Source file path> <Load ID> [loadgroup <loadgroup-name>] [databasepath <database-path>]\n\tExecute a transformation script for a given load ID");
		System.out.println("executetransformations <Load ID>\n\tExcecute all transformations for a given load ID");
		System.out.println("executescriptgroup <Script Group>\n\tExecutes all scripts defined in a given script group");
		System.out.println("continueetlrun <LoadID>\n\tremove step completions from history for ETL run in order to continue all components of ETL");
		System.out.println("resetetlrun <LoadID> [onlymodifiedsources]\n\tremove command history for ETL run in order to rerun all components of ETL. Optionally, do not reset for unmodified staging tables.");
		System.out
				.println("exportwarehouse [<LoadID>|ALL] <Directory> [connection=<connection name>]\n\tExport warehouse tables to a directory, optionally include the schema the export will be meant to load,\n\tensures proper column ordering");
		System.out.println("loadinfobright <Directory>\n\tLoad InfoBright from an export directory");
		System.out
				.println("savedynamicrepository <dynamicrepositoryFilename>\n\tSerializes dynamic (in memomry version of) repository to view in admin tool in readonly mode for debugging purposes.");
		System.out.println("extractSFDCdata <SFDCClientID> <sandBoxURL> <noExtractionThreads> <unique id for the task>\n\textracts data from SFDC using configuratble parellel no. of threads");
		System.out.println("extractconnectordata <ConnectorName> <BIRST_Parameters> <sandBoxURL> <noExtractionThreads> <unique id for the task>\n\textracts data using connector using configuratble parellel no. of threads");
		System.out.println("emailer <mailHost> <from> <to>\n\tsetups up emailer for sending emails");
		System.out.println("createtestdatafiles <Data File Path> <Number of rows in each source file> \n\tgenerates test data files with specified number of rows containing random data");
		System.out.println("performanalyzeschema <Load ID> <LoadGroup>\n\tPerforms analyze on all schema tables after load is complete on paraccel");
		System.out.println("performvacuumschema <Load ID> <LoadGroup>\n\tPerforms vacuum on all schema tables after load is complete on paraccel");
		System.out.println("splitsourcefilesforpartitions <Source file path>\n\tSplits source files into multiple files based on partition definition with keys distributed across different partitions");
		System.out.println("setpartitionconnection <Connection Name>\n\tSets partition connection name so that warehouse is loaded only to that connection using corresponding data files");
		System.out.println("consolidatetxncmdhistory <Load ID> [loadgroup <loadgroup-name>] [subgroups <comma separated list of subgroups>] [runinpollingmode]\n\tConsolidates txn_command_history table from different partition connections to main connection");
		System.out.println("droptemptables <prefix> \n\tWhile running queries with IN filters for IB, we create temp tables that might linger around for performance optimization. This command drops all such tables starting with the given prefix.");
		System.out.println("updatestats \n\tupdate query optimization statistics on tables and/or index views of the space (sql server only)");
		System.out.println("copytable <fromSchema> <toSchema> <tableName> <databasedir>. Copies Tables from One Schema to Another. We use it for Paraccel and RedShift to copy schema");
		System.out.println("unload <fromSchema> <tableName> <targetDir> <keepFileNameasTableName> [<databaseloaddirpath>]. Unload tables to flat files for Supporting Databases. targetDir must be mounted on DB.keepFileNameasTablename  can be true or false. databaseloaddirpath absolute path of directory where data files are to be unloaded");
		System.out.println("load <toschema> <tableName> <sourcefile> [<databaseloaddirpath>]. Copy tables from flat filesfor Supporting databases. sourceFile must be accessible to DB. databaseloaddirpath absolute path of directory where data files are to be unloaded");
		System.out.println("creatediscoveryst <tableName>. Creates the Staging table in the database.");
		System.out.println("updatecompositekeys <physical tableName>. Update the composite keys in Fact tables.Used by MoveSpace for SQL Server to IB.");
		System.out.println("validatemetadata <ruleset> [<rulename>] [<groupname>] Validate the specified repository against the ruleset");
		System.out.println("fixmetadata <ruleset> [<rulename>] [<groupname>] Fix the specified repository against the ruleset");
		System.out.println("saverepository <File Path> Save the loaded repository to the specified file");
		System.out.println("unloadusingquery <scratchSchema> <sourceSchema> <tableName> <targetDir>. Unload by not considering any physical columns that are not present in repository.Used for MoveSpace");
		System.out.println("createscriptlog <tableName>. Create the TXN_SCRIPT_LOG table");
		System.out.println("getchecksum <columnname>. Used by MoveSpace to get a hashcode matching that generated by GenerateSchema");
		System.out.println("listindices. List all indices in a particular schema");
		System.out.println("listaggregatesphysical. List all physical names of aggregates");
		System.out.println("reinitloadvariables. Reinitialize all load variables existing in the repository");
		System.out.println("performasynctask asyncClass=<asyncClass> consumerIndex=<consumerIndex> msgID=<msgID> userName=<userName> spaceID=<spaceID> inputdata=<inputdata string from message> resultdata=<resultdata string from message>");
	}

	private static List<String> tokenize(String buff, String processGroup, boolean checkForRepositoryCommand)
	{
		
		/*
		 * Support grouping of commands - this allows the ability to separate commands and run them separately
		 */
		String curGroup = null;
		List<String> tokens = new ArrayList<String>();
		// Support the standard C/Java /* */ comments
		boolean comment = false;
		StringBuilder curToken = new StringBuilder();
		boolean quote = false;
		boolean superquote = false;
		boolean group = false;
		boolean repository = false;
		boolean catalogDirB = false;
		char[] delimiters = new char[]
		{ ' ', '\t', '\r', '\n' };
		for (int i = 0; i < buff.length(); i++)
		{
			boolean delim = false;
			for (char c : delimiters)
			{
				if (buff.charAt(i) == c)
				{
					delim = true;
					break;
				}
			}
			if (!delim && (i == (buff.length() - 1)))
			{
				curToken.append(buff.charAt(i));
			}
			if ((delim || i == (buff.length() - 1)) && !quote && !superquote && (curToken.length() > 0))
			{
				String s = curToken.toString();
				if (s.startsWith("/*"))
					comment = true;
				else if (comment && s.endsWith("*/"))
					comment = false;
				else if (s.equalsIgnoreCase("group"))
				{
					group = true;
				} else if (group)
				{
					group = false;
					curGroup = s;
				} else if (!comment)
				{
					/*
					 * Include a token if it references a repository, it's in the current group or there is no group
					 * specified
					 */
					if (s.equalsIgnoreCase("repository"))
					{
						tokens.add(s.trim());
						repository = true;
					} else if (repository || (!checkForRepositoryCommand))
					{
						tokens.add(s.trim());
						repository = false;
					} else if (s.equalsIgnoreCase("noaggregates"))
					{
						tokens.add(s.trim());
					} else if (s.equalsIgnoreCase("catalogdir"))
					{
						catalogDirB = true;
					} else if (catalogDirB == true)
					{
						catalogDir = s.trim();
						logger.debug("Setting Catalog Directory to " + catalogDir);
						// Pass catalogDir to exporter the hard way
						catalogDirB = false;
					} else if (processGroup == null)
					{
						tokens.add(s.trim());
					} else if (processGroup.equals(curGroup))
					{
						foundProcessGroup = true;
						tokens.add(s.trim());
					}
				}
				curToken = new StringBuilder();
			} else if (!quote && buff.charAt(i) == '\'' && (i == 0 || buff.charAt(i - 1) != '\\'))
			{
				superquote = !superquote;
			} else if (!superquote && buff.charAt(i) == '"' && (i == 0 || buff.charAt(i - 1) != '\\'))
			{
				quote = !quote;
			} else if (!delim || quote || superquote)
			{
				if (!superquote && !quote)
				{
					curToken.append(buff.charAt(i));
				} else if (superquote && !(buff.charAt(i) == '\\' && (i < buff.length() - 1) && buff.charAt(i + 1) == '\''))
				{
					curToken.append(buff.charAt(i));
				} else if (quote && !(buff.charAt(i) == '\\' && (i < buff.length() - 1) && buff.charAt(i + 1) == '"'))
				{
					curToken.append(buff.charAt(i));
				}
			}
		}
		return (tokens);
	}

	private static void processTokens(List<String> tokens, CommandSession cmdsession, boolean finish) throws Exception
	{
		List<Step> steps = new ArrayList<Step>();
		if (tokens.isEmpty())
		{
			printHelp();
		} else
		{
			WorkQueue wq = null;
			try
			{
				wq = new WorkQueue(NUM_THREADS);
				for (int i = 0; i < tokens.size(); i++)
				{
					String s = tokens.get(i);
					Step step = null;
					if (s.equals("getmodeldataset"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.GetModelDataSet, StepCommandType.Modeling, tokens.get(++i));
							if (i + 1 < tokens.size() && tokens.get(i + 1).equals("createaggregates"))
							{
								step.args.add("createaggregates");
								i++;
								if (i + 1 < tokens.size() && tokens.get(i + 1).equals("persist"))
								{
									step.args.add("persist");
									i++;
									if (i + 1 < tokens.size() && tokens.get(i + 1).equals("persistsubqueries"))
									{
										step.args.add("persistsubqueries");
										i++;
									}
								} else if (i + 1 < tokens.size() && tokens.get(i + 1).equals("remove"))
								{
									step.args.add("remove");
									i++;
								}
							}
							if (i + 2 < tokens.size() && tokens.get(i + 1).equals("filter"))
							{
								step.args.add("filter");
								i++;
								step.args.add(tokens.get(++i));
							}
						} else
							throw (new Exception("No Success Model Specified: " + s));
					} else if (s.equals("echo"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.Echo, StepCommandType.Query, tokens.get(++i));
						} else
							throw (new Exception("No String specified: " + s));
					} else if (s.equals("runmodels"))
					{
						step = new Step(StepCommand.RunModels, StepCommandType.Modeling, null);
						if (tokens.size() > i + 1
								&& (tokens.get(i + 1).equals("bulkload") || tokens.get(i + 1).equals("writecsv") || tokens.get(i + 1).equals("writetable")))
							step.args.add(tokens.get(++i));
						if (tokens.size() > i + 1
								&& (tokens.get(i + 1).equals("bulkload") || tokens.get(i + 1).equals("writecsv") || tokens.get(i + 1).equals("writetable")))
							step.args.add(tokens.get(++i));
						if (tokens.size() > i + 1
								&& (tokens.get(i + 1).equals("bulkload") || tokens.get(i + 1).equals("writecsv") || tokens.get(i + 1).equals("writetable")))
							step.args.add(tokens.get(++i));
					} else if (s.equals("createmodelaggregates"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.GetModelDataSet, StepCommandType.Modeling, tokens.get(++i));
							step.args.add("createaggregates");
							step.args.add("persist");
							if (i + 1 < tokens.size() && tokens.get(i + 1).equals("persistsubqueries"))
							{
								step.args.add("persistsubqueries");
								i++;
							}
						}
					} else if (s.equals("deletemodelaggregates"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.GetModelDataSet, StepCommandType.Modeling, tokens.get(++i));
							step.args.add("createaggregates");
							step.args.add("remove");
						}
					} else if (s.equals("writemodeldataset"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.WriteModelDataSet, StepCommandType.Modeling, tokens.get(++i));
							if (i + 1 < tokens.size() && tokens.get(i + 1).matches("[0-9]+[,][0-9]+"))
							{
								String[] range = tokens.get(i + 1).split(",");
								step.args.add(range[0]);
								step.args.add(range[1]);
								i++;
							}
						} else
							throw (new Exception("No Model Data Set Specified: " + s));
					} else if (s.equals("summarizedataset"))
					{
						if (i + 1 != tokens.size())
							step = new Step(StepCommand.SummarizeDataSet, StepCommandType.Modeling, tokens.get(++i));
						else
							throw (new Exception("No Model Data Set Specified: " + s));
					} else if (s.equals("findrules"))
					{
						if (i + 1 != tokens.size())
							step = new Step(StepCommand.FindModelDataSetRules, StepCommandType.Modeling, tokens.get(++i));
						else
							throw (new Exception("No Model Data Set Specified: " + s));
					} else if (s.equals("rankfactors"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.RankModelDataSetFactors, StepCommandType.Modeling, tokens.get(++i));
							boolean repeat = true;
							while (repeat && tokens.size() > i + 1)
							{
								if (tokens.get(i + 1).startsWith("mincorr=") || tokens.get(i + 1).startsWith("minlift=") || tokens.get(i + 1).startsWith("mindev="))
								{
									step.args.add(tokens.get(++i));
								} else
									repeat = false;
							}
						} else
							throw (new Exception("No Model Data Set Specified: " + s));
					} else if (s.equals("analyzeoutcome"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.AnalyzeOutcome, StepCommandType.Modeling, tokens.get(++i));
							if (tokens.size() > i + 2 && tokens.get(i + 1).equals("iteration"))
							{
								i += 2;
								step.args.add(tokens.get(i));
							}
							if (tokens.size() > i + 1
									&& (tokens.get(i + 1).equals("bulkload") || tokens.get(i + 1).equals("writecsv") || tokens.get(i + 1).equals("writetable") || tokens
											.get(i + 1).equals("usedatasetsegments")))
								step.args.add(tokens.get(++i));
							if (tokens.size() > i + 1
									&& (tokens.get(i + 1).equals("bulkload") || tokens.get(i + 1).equals("writecsv") || tokens.get(i + 1).equals("writetable") || tokens
											.get(i + 1).equals("usedatasetsegments")))
								step.args.add(tokens.get(++i));
							if (tokens.size() > i + 1
									&& (tokens.get(i + 1).equals("bulkload") || tokens.get(i + 1).equals("writecsv") || tokens.get(i + 1).equals("writetable") || tokens
											.get(i + 1).equals("usedatasetsegments")))
								step.args.add(tokens.get(++i));
							if (tokens.size() > i + 1
									&& (tokens.get(i + 1).equals("bulkload") || tokens.get(i + 1).equals("writecsv") || tokens.get(i + 1).equals("writetable") || tokens
											.get(i + 1).equals("usedatasetsegments")))
								step.args.add(tokens.get(++i));
						} else
							throw (new Exception("No Outcome Specified: " + s));
					} else if (s.equals("generateoutcomemodels"))
					{
						if (i + 1 != tokens.size())
						{
							if (i + 2 < tokens.size() && tokens.get(i + 2).equals("iteration"))
							{
								step = new Step(StepCommand.GenOutcomeModels, StepCommandType.Modeling, tokens.get(++i));
								++i;
								step.args.add(tokens.get(++i));
							} else
								step = new Step(StepCommand.GenOutcomeModels, StepCommandType.Modeling, tokens.get(++i));
						} else
							throw (new Exception("No Outcome Specified: " + s));
					} else if (s.equals("createoutcomegroup"))
					{
						if (i + 1 != tokens.size())
							step = new Step(StepCommand.CreateOutcomeGroup, StepCommandType.Modeling, tokens.get(++i));
						else
							throw (new Exception("No Outcome Group Specified: " + s));
					} else if (s.equals("createoutcomegroups"))
					{
						step = new Step(StepCommand.CreateOutcomeGroups, StepCommandType.Modeling, null);
					} else if (s.equals("searchpatterns"))
					{
						if (i + 1 != tokens.size())
							step = new Step(StepCommand.SearchPatterns, StepCommandType.Modeling, tokens.get(++i));
						else
							throw (new Exception("No Outcome Group Specified: " + s));
					} else if (s.equals("generatepeers"))
					{
						if (tokens.size() > i + 1)
						{
							step = new Step(StepCommand.GeneratePeers, StepCommandType.Modeling, tokens.get(++i));
							if (tokens.size() > i + 1 && (tokens.get(i + 1).equals("writecsv") || tokens.get(i + 1).equals("writetable")))
								step.args.add(tokens.get(++i));
							if (tokens.size() > i + 1 && (tokens.get(i + 1).equals("writecsv") || tokens.get(i + 1).equals("writetable")))
								step.args.add(tokens.get(++i));
						} else
							throw (new Exception("Missing population name"));
					} else if (s.equals("runpeers"))
					{
						step = new Step(StepCommand.RunPeers, StepCommandType.Modeling, null);
						if (tokens.size() > i + 1 && (tokens.get(i + 1).equals("writecsv") || tokens.get(i + 1).equals("writetable")))
							step.args.add(tokens.get(++i));
						if (tokens.size() > i + 1 && (tokens.get(i + 1).equals("writecsv") || tokens.get(i + 1).equals("writetable")))
							step.args.add(tokens.get(++i));
					} else if (s.equals("generateperformancetable"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.GeneratePerformanceTable, StepCommandType.Modeling, tokens.get(++i));
						} else
							throw (new Exception("Missing Performance Model Name"));
					} else if (s.equals("generateperformancetablelist"))
					{
						if (i + 1 != tokens.size())
						{
							if (i + 2 < tokens.size())
							{
								step = new Step(StepCommand.GeneratePerformanceTable, StepCommandType.Modeling, tokens.get(++i));
								step.args.add(tokens.get(++i));
							} else
								throw (new Exception("Missing Performance Model Name"));
						} else
							throw (new Exception("Missing filename: " + s));
					} else if (s.equals("generatesegmentlists"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.GenerateSegmentLists, StepCommandType.Modeling, tokens.get(++i));
							if (tokens.size() > i + 1 && (tokens.get(i + 1).equals("writecsv") || tokens.get(i + 1).equals("writetable")))
								step.args.add(tokens.get(++i));
							if (tokens.size() > i + 1 && (tokens.get(i + 1).equals("writecsv") || tokens.get(i + 1).equals("writetable")))
								step.args.add(tokens.get(++i));
						} else
						{
							throw (new Exception("No Success Pattern Segment Model Specified: " + s));
						}
					} else if (s.equals("outputimpacts"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.OutputImpacts, StepCommandType.Modeling, tokens.get(++i));
							if (tokens.size() > i + 1 && (tokens.get(i + 1).equals("sample") || tokens.get(i + 1).equals("summary")))
								step.args.add(tokens.get(++i));
							if (tokens.size() > i + 1 && (tokens.get(i + 1).equals("sample") || tokens.get(i + 1).equals("summary")))
								step.args.add(tokens.get(++i));
						} else
							throw (new Exception("No Success Model Specified: " + s));
					} else if (s.equals("getperformancemodel"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.GetPerformanceModel, StepCommandType.Modeling, tokens.get(++i));
						} else
							step = new Step(StepCommand.GetPerformanceModel, StepCommandType.Modeling, null);
					} else if (s.equals("getoutcomemodelstats"))
					{
						if (i + 1 != tokens.size())
							step = new Step(StepCommand.GetOutcomeModelStats, StepCommandType.Modeling, tokens.get(++i));
						else
							throw (new Exception("No Success Pattern Segment Model Specified: " + s));
					} else if (s.equals("optimizeperformance"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.OptimizePerformance, StepCommandType.Modeling, tokens.get(++i));
						} else
							step = new Step(StepCommand.OptimizePerformance, StepCommandType.Modeling, null);
					} else if (s.equals("optimizeperformancelist"))
					{
						if (i + 1 != tokens.size())
						{
							if (i + 2 < tokens.size())
							{
								step = new Step(StepCommand.OptimizePerformance, StepCommandType.Modeling, tokens.get(++i));
								step.args.add(tokens.get(++i));
							} else
								throw (new Exception("Missing Performance Model Name" + s));
						} else
							throw (new Exception("Missing filename: " + s));
					} else if (s.equals("optimizeperformancedb"))
					{
						if (i + 1 != tokens.size())
						{
							if (i + 2 < tokens.size())
							{
								step = new Step(StepCommand.OptimizePerformanceDB, StepCommandType.Modeling, tokens.get(++i));
								step.args.add(tokens.get(++i));
							} else
								throw (new Exception("Missing Performance Model Name" + s));
						} else
							throw (new Exception("Missing variable: " + s));
					} else if (s.equals("runperformance"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.RunPerformance, StepCommandType.Modeling, tokens.get(++i));
						} else
						{
							step = new Step(StepCommand.RunPerformance, StepCommandType.Modeling, null);
						}
					} else if (s.equals("findsegments"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.FindSegments, StepCommandType.Modeling, tokens.get(++i));
						} else
							throw (new Exception("No Segment Definition Specified: " + s));
					} else if (s.equals("scoresegments"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.ScoreSegments, StepCommandType.Modeling, tokens.get(++i));
						} else
							throw (new Exception("No Segment Definition Specified: " + s));
					} else if (s.equals("runquery"))
					{
						if (i + 1 < tokens.size())
						{
							step = new Step(StepCommand.RunQuery, StepCommandType.Query, tokens.get(++i));
							// Now look for optional parameters
							while (i + 1 < tokens.size()
									&& (tokens.get(i + 1).equalsIgnoreCase("splitcacheon") 
											|| tokens.get(i + 1).equalsIgnoreCase("formatresults") 
											|| tokens.get(i + 1).equalsIgnoreCase("printcacheusage")
											|| tokens.get(i + 1).equalsIgnoreCase("SimulateOuterJoinUsingPersistedDQTs")
											|| tokens.get(i + 1).equalsIgnoreCase("printutf8")))
							{
								if (i + 1 < tokens.size())
								{
									if (tokens.get(i + 1).equalsIgnoreCase("splitcacheon") && (i + 2 < tokens.size()))
									{
										++i;
										step.args.add("splitcacheon=" + tokens.get(++i));
									} else if (tokens.get(i + 1).equalsIgnoreCase("formatresults"))
									{
										++i;
										step.args.add("formatresults=true");
									} else if (tokens.get(i + 1).equalsIgnoreCase("printcacheusage"))
									{
										++i;
										step.args.add("printcacheusage=true");
									}else if (tokens.get(i + 1).equalsIgnoreCase("SimulateOuterJoinUsingPersistedDQTs"))
									{
										++i;
										step.args.add("SimulateOuterJoinUsingPersistedDQTs=true");
									} else if (tokens.get(i + 1).equalsIgnoreCase("printutf8"))
									{
										++i;
										step.args.add("printutf8=true");
									}
								} 
							}
						} else
							throw (new Exception("No Query Specified: " + s));
					} else if (s.equals("exportquery"))
					{
						if (i + 1 != tokens.size())
						{
							if (i + 2 < tokens.size())
							{
								step = new Step(StepCommand.ExportQuery, StepCommandType.Query, tokens.get(++i));
								step.args.add(tokens.get(++i)); // file name
								if (i + 1 < tokens.size() && tokens.get(i + 1).equals("separator"))
								{
									++i;
									step.args.add(tokens.get(++i)); // separator
								}
							} else
								throw (new Exception("No Filename Specificed: " + s));
						} else
							throw (new Exception("No Query Specified: " + s));
					} else if (s.equals("explainnavigation"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.ExplainNavigation, StepCommandType.Query, tokens.get(++i));
						} else
							throw (new Exception("No Query Specified: " + s));
					} else if (s.equals("explainonfailure"))
					{
						step = new Step(StepCommand.ExplainOnFailure, StepCommandType.Query, null);
					} else if (s.equals("explainnavigationagg"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.ExplainNavigationAgg, StepCommandType.Query, tokens.get(++i));
						} else
							throw (new Exception("No Aggregate Name Specified: " + s));
					} else if (s.equals("generatephysicalquery"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.GeneratePhysicalQuery, StepCommandType.Query, tokens.get(++i));
							while (i + 1 < tokens.size() && (tokens.get(i + 1).equalsIgnoreCase("abstractschema")))
							{
								String tkn = tokens.get(++i);
								step.args.add(tkn.toLowerCase());
							}
						} else
							throw (new Exception("No Query Specified: " + s));
						
					} else if (s.equals("translatequery"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.TranslateQuery, StepCommandType.Query, tokens.get(++i));
						} else
							throw (new Exception("No Query Specified: " + s));
					} else if (s.equals("decrypt"))
					{
						if (i + 1 != tokens.size())
						{
							if (i + 2 < tokens.size())
							{
								step = new Step(StepCommand.Decrypt, StepCommandType.Password, tokens.get(++i));
								step.args.add(tokens.get(++i));
							} else
								throw (new Exception("Missing key:" + s));
						} else
							throw (new Exception("Missing ciphertext: " + s));		
					} else if (s.equals("encrypt"))
					{
						if (i + 1 != tokens.size())
						{
							if (i + 2 < tokens.size())
							{
								step = new Step(StepCommand.Encrypt, StepCommandType.Password, tokens.get(++i));
								step.args.add(tokens.get(++i));
							} else
								throw (new Exception("Missing key:" + s));
						} else
							throw (new Exception("Missing plaintext: " + s));										
					} else if (s.equals("generatephysicalqueryagg"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.GeneratePhysicalQueryAgg, StepCommandType.Query, tokens.get(++i));
						} else
							throw (new Exception("No Aggregate Name Specified: " + s));
					
					} else if (s.equals("profilecache"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.ProfileCache, StepCommandType.Cache, tokens.get(++i));
						} else
							throw (new Exception("No destination specified"));
					} else if (s.equals("compareprofiles"))
					{
						if (i + 2 != tokens.size())
						{
							step = new Step(StepCommand.CompareProfiles, StepCommandType.Cache, tokens.get(++i));
							step.args.add(tokens.get(++i));
						} else
							throw (new Exception("No destination specified"));
					} else if (s.equals("scriptcache"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.ScriptCache, StepCommandType.Cache, tokens.get(++i));
						} else
							step = new Step(StepCommand.ScriptCache, StepCommandType.Cache, "console");
					} else if (s.equals("createqueryaggregate"))
					{
						if (i + 1 != tokens.size())
						{
							if (i + 2 < tokens.size())
							{
								step = new Step(StepCommand.CreateQueryAggregate, StepCommandType.Aggregates, tokens.get(++i));
								step.args.add(tokens.get(++i));
								if (i + 1 < tokens.size() && tokens.get(i + 1).equals("persist"))
								{
									step.args.add(tokens.get(++i));
								}
							} else
								throw (new Exception("No Name for Aggregate Specificed: " + s));
						} else
							throw (new Exception("No Query Specified: " + s));
					} else if (s.equals("dropqueryaggregate"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.DropQueryAggregate, StepCommandType.Aggregates, tokens.get(++i));
							if (i + 1 < tokens.size() && tokens.get(i + 1).equals("persist"))
							{
								step.args.add(tokens.get(++i));
							}
						} else
							throw (new Exception("No Query Aggregate Specified: " + s));
					} else if (s.equals("updatequeryaggregate"))
					{
						if (i + 1 != tokens.size())
						{
							if (i + 2 < tokens.size())
							{
								step = new Step(StepCommand.UpdateQueryAggregate, StepCommandType.Aggregates, tokens.get(++i));
								step.args.add(tokens.get(++i));
							} else
								throw (new Exception("No load_id specified: " + s));
						} else
							throw (new Exception("No Query Aggregate Specified: " + s));
					} else if (s.equals("persistaggregate"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.PersistAggregate, StepCommandType.Aggregates, tokens.get(++i));
							// Now look for optional parameters
							while (i + 1 < tokens.size()
									&& (tokens.get(i + 1).equalsIgnoreCase("persistsubqueries") || tokens.get(i + 1).equalsIgnoreCase("retaindqt")))
							{
								step.args.add(tokens.get(++i).toLowerCase());
							}
						} else
							throw (new Exception("No Aggregate Specified: " + s));
					} else if (s.equals("persistaggregates"))
					{
						step = new Step(StepCommand.PersistAggregates, StepCommandType.Aggregates, null);
						if (i + 1 != tokens.size())
						{
							if (tokens.get(i+1).toLowerCase().startsWith("loadgroup="))
							{
								step.args.add(tokens.get(++i));
							}
						}
						if (i + 1 != tokens.size())
						{
							if (tokens.get(i+1).toLowerCase().startsWith("subgroup="))
							{
								step.args.add(tokens.get(++i));
							}
						}
						if (i + 1 != tokens.size())
						{
							if (tokens.get(i+1).toLowerCase().startsWith("datadir="))
							{
								step.args.add(tokens.get(++i));
							}
						}
						if (i + 1 != tokens.size())
						{
							if (tokens.get(i+1).toLowerCase().startsWith("databasepath="))
							{
								step.args.add(tokens.get(++i));
							}
						}
					} else if (s.equals("aggregatelogicalquery"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.AggregateLogicalQuery, StepCommandType.Aggregates, tokens.get(++i));
						} else
							throw (new Exception("No Aggregate Specified: " + s));
					} else if (s.equals("aggregatephysicalquery"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.AggregatePhysicalQuery, StepCommandType.Aggregates, tokens.get(++i));
						} else
							throw (new Exception("No Aggregate Specified: " + s));
					} else if (s.equals("instantiateaggregate"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.InstantiateAggregate, StepCommandType.Aggregates, tokens.get(++i));
						} else
							throw (new Exception("No Aggregate Specified: " + s));
					} else if (s.equals("updateaggregate"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.UpdateAggregate, StepCommandType.Aggregates, tokens.get(++i));
							while (i + 1 < tokens.size()
									&& (tokens.get(i + 1).equalsIgnoreCase("loadgroup") || tokens.get(i + 1).equalsIgnoreCase("persistsubqueries")))
							{
								if (tokens.get(i + 1).equalsIgnoreCase("persistsubqueries"))
								{
									step.args.add(tokens.get(++i));
								} else if (tokens.get(i + 1).equalsIgnoreCase("loadgroup"))
								{
									i++;
									if (i + 1 < tokens.size())
									{
										String loadGroupName = tokens.get(++i);
										step.args.add("loadgroup=" + loadGroupName);
									} else
									{
										throw new Exception("No load group name specified after \"loadgroup\" option.");
									}
								}
							}
						} else
							throw (new Exception("No Aggregate Specified: " + s));
					} else if (s.equals("dropaggregate"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.DropAggregate, StepCommandType.Aggregates, tokens.get(++i));
							if (i + 1 < tokens.size() && tokens.get(i + 1).equals("persist"))
							{
								step.args.add(tokens.get(++i));
							}
						} else
							throw (new Exception("No Aggregate Specified: " + s));
					} else if (s.equals("dropaggregates"))
					{
						step = new Step(StepCommand.DropAggregates, StepCommandType.Aggregates, null);
						if (i + 1 < tokens.size() && tokens.get(i + 1).equals("persist"))
						{
							step.args.add(tokens.get(++i));
						}
					} else if (s.equals("removefiles"))
					{
						step = new Step(StepCommand.RemoveFiles, StepCommandType.Modeling, null);
					} else if (s.equals("resetmodelrun"))
					{
						step = new Step(StepCommand.ResetModelRun, StepCommandType.Modeling, null);
					} else if (s.equals("resetetlrun"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.ResetETLRun, StepCommandType.ETL, null);
							step.args.add(tokens.get(++i));
							if (i + 1 != tokens.size() && tokens.get(i + 1).toLowerCase().equals("onlymodifiedsources"))
							{
								step.args.add("onlymodifiedsources");
								i++;
							}
						} else
							throw (new Exception("No Load ID Specified: " + s));
					} else if (s.equals("continueetlrun"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.ContinueETLRun, StepCommandType.ETL, null);
							if (i + 1 != tokens.size())
							{
								step.args.add(tokens.get(++i));
							}
						} else
							throw (new Exception("No Load ID Specified: " + s));
					} else if (s.equals("runseedquery"))
					{
						if (i + 1 != tokens.size())
							step = new Step(StepCommand.RunSeedQuery, StepCommandType.Cache, tokens.get(++i));
						else
							throw (new Exception("No Query Specified: " + s));
					} else if (s.equals("generatecache"))
					{
						if (i + 5 < tokens.size())
						{
							/*
							 * Argument 0: Output directory name Argument 1: Working directory name Argument 2: Output file
							 * name prefix Argument 3: Key parameter name list Argument 4: Key parameter value list file
							 * name Argument 5: Query list file name
							 */
							step = new Step(StepCommand.InitCacheGenerator, StepCommandType.Cache, tokens.get(++i));
							step.args.add(tokens.get(++i));
							step.args.add(tokens.get(++i));
							step.args.add(tokens.get(++i));
							step.args.add(tokens.get(++i));
						} else if ((i + 1 < tokens.size()))
						{
							step = new Step(StepCommand.GenerateCache, StepCommandType.Cache, tokens.get(++i));
						} else
							throw (new Exception("Invalid arguments Specified: " + s));
					} else if (s.equals("thread"))
					{
						if (steps.size() > 0)
						{
							wq.execute(new ExecuteSteps(steps, cmdsession));
							steps = new ArrayList<Step>();
						}
					} else if (s.equals("viewcachefile"))
					{
						if (i + 1 != tokens.size())
							step = new Step(StepCommand.ViewCacheFile, StepCommandType.Cache, tokens.get(++i));
						else
							throw (new Exception("Result set filename not provided: " + s));
					} else if (s.equals("removecachefile"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.RemoveCacheFile, StepCommandType.Cache, tokens.get(++i));
						} else
							throw (new Exception("Cache filename not provided: " + s));
					} else if (s.equals("clearcache"))
					{
						step = new Step(StepCommand.ClearCache, StepCommandType.Cache, null);
					} else if (s.equals("removecacheforquery"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.RemoveCacheForQuery, StepCommandType.Cache, tokens.get(++i));
							if (i + 1 < tokens.size() && (tokens.get(i + 1).equals("true") || tokens.get(i + 1).equals("false")))
							{
								step.args.add(tokens.get(++i));
							}
							if (i + 1 < tokens.size() && (tokens.get(i + 1).equals("true") || tokens.get(i + 1).equals("false")))
							{
								step.args.add(tokens.get(++i));
							}
						} else
							throw (new Exception("Logical query not provided: " + s));
					} else if (s.equals("removecacheforlogicaltable"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.RemoveCacheForLogicalTable, StepCommandType.Cache, tokens.get(++i));
						} else
							throw (new Exception("Logical table name not provided: " + s));
					} else if (s.equals("removecacheforphysicaltable"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.RemoveCacheForPhysicalTable, StepCommandType.Cache, tokens.get(++i));
						} else
							throw (new Exception("Physical table name not provided: " + s));
					} else if (s.equals("completework"))
					{
						wq.completeCurrentWork();
					} else if (s.equals("repository"))
					{
						if (i + 1 != tokens.size())
							step = new Step(StepCommand.Repository, StepCommandType.Init, tokens.get(++i));
						else
							throw (new Exception("No Repository Filename Specified: " + s));
						// Now look for optional parameters
						while (i + 1 < tokens.size() && (tokens.get(i + 1).equalsIgnoreCase("noaggregates")))
						{
							step.args.add(tokens.get(++i).toLowerCase());
						}
						while (i + 1 < tokens.size() && (tokens.get(i + 1).equalsIgnoreCase("nometadata")))
						{
							step.args.add(tokens.get(++i).toLowerCase());
						}
						// Execute any current steps and completework
						steps.add(step);
						ExecuteSteps es = new ExecuteSteps(steps, cmdsession);
						wq.execute(es);
						wq.completeCurrentWork();
						if (es.isError())
						{
							break;
						}
						step.command = null;
						steps = new ArrayList<Step>();
					}
					else if (s.equals("saverepository"))
					{
						step = new Step(StepCommand.SaveRepository,StepCommandType.Init,tokens.get(++i));
					}
					else if (s.equals("savedynamicrepository"))
					{
						if (i + 1 < tokens.size())
						{
							step = new Step(StepCommand.SaveDynamicRepository, StepCommandType.Test, tokens.get(++i));
						} else
							throw (new Exception("No dynamic repository filename specified: " + s));
					} else if (s.equals("hashpassword"))
					{
						if (i + 1 != tokens.size())
							step = new Step(StepCommand.Hash, StepCommandType.Password, tokens.get(++i));
						else
							throw (new Exception("No Cleartext Password Specified: " + s));
					} else if (s.equals("instantiateview"))
					{
						if (i + 1 != tokens.size())
							step = new Step(StepCommand.InstantiateView, StepCommandType.Database, tokens.get(++i));
						else
							throw (new Exception("No Table Name Specified: " + s));
						if (i + 1 != tokens.size())
						{
							if (tokens.get(i + 1).equalsIgnoreCase("create") || tokens.get(i + 1).equalsIgnoreCase("persist"))
							{
								step.args.add("create");
								i++;
							}
						}
						// Execute any current steps and completework
						steps.add(step);
						ExecuteSteps es = new ExecuteSteps(steps, cmdsession);
						wq.execute(es);
						wq.completeCurrentWork();
						if (es.isError())
						{
							break;
						}
						step.command = null;
						steps = new ArrayList<Step>();
					} else if (s.equals("dropview"))
					{
						if (i + 1 != tokens.size())
							step = new Step(StepCommand.DropView, StepCommandType.Database, tokens.get(++i));
						else
							throw (new Exception("No Table Name Specified: " + s));
					} else if (s.equals("setrowlimit"))
					{
						if (i + 1 != tokens.size())
							step = new Step(StepCommand.SetRowLimit, StepCommandType.Database, tokens.get(++i));
						else
							throw (new Exception("No Row Limit Specified: " + s));
					} else if (s.equals("setvariable"))
					{
						if (i + 1 != tokens.size())
						{
							if (i + 2 < tokens.size())
							{
								step = new Step(StepCommand.SetVariable, StepCommandType.Database, tokens.get(++i));
								step.args.add(tokens.get(++i));
							} else
								throw (new Exception("No value specified: " + s));
						} else
							throw (new Exception("No variable name specified: " + s));
					} else if (s.equals("fillsessionvariables"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.FillSessionVariables, StepCommandType.Database, tokens.get(++i));
						} else
							throw (new Exception("No user name specified: " + s));
					} else if (s.equals("closesession"))
					{
						step = new Step(StepCommand.CloseSession, StepCommandType.Database, null);
					} else if (s.equals("createtimefiles"))		// create the time files (no database operations)
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.CreateTimeFiles, StepCommandType.Database, tokens.get(++i));
							while (i + 1 < tokens.size()
									&& (tokens.get(i + 1).toLowerCase().startsWith("folder=")))
							{
								String tkn = tokens.get(++i);
								step.args.add(tkn);
							}
						}
					} else if (s.equals("createtimeschema"))	// create the time schema, but do not load the tables
					{
						step = new Step(StepCommand.CreateTimeSchema, StepCommandType.Database, null);		
					} 
					else if (s.equals("uploadtimefiles"))	// upload the time files to S3
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.UploadTimeFilesToS3, StepCommandType.Database, tokens.get(++i));
							while (i + 1 < tokens.size()
									&& (tokens.get(i + 1).toLowerCase().startsWith("folder=")
											|| tokens.get(i + 1).toLowerCase().startsWith("s3folder=")
											|| tokens.get(i + 1).toLowerCase().startsWith("awsaccesskeyid=") 
											|| tokens.get(i + 1).toLowerCase().startsWith("awssecretkey=")
											|| tokens.get(i + 1).toLowerCase().startsWith("awstoken=")))
							{
								String tkn = tokens.get(++i);
								step.args.add(tkn);
							}
						}
					} else if (s.equals("loadtimeschema"))		// load the time schema assuming that the schema has been created and the files exist
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.LoadTimeSchema, StepCommandType.Database, tokens.get(++i));
							while (i + 1 < tokens.size()
									&& (tokens.get(i + 1).toLowerCase().startsWith("folder=")
											|| tokens.get(i + 1).toLowerCase().startsWith("awsaccesskeyid=") 
											|| tokens.get(i + 1).toLowerCase().startsWith("awssecretkey=")
											|| tokens.get(i + 1).toLowerCase().startsWith("awstoken=")))
							{
								String tkn = tokens.get(++i);
								step.args.add(tkn);
							}
						}
					} else if (s.equals("addpartition"))
					{
						if (i + 1 != tokens.size())
						{
							if (i + 2 < tokens.size())
							{
								step = new Step(StepCommand.AddPartition, StepCommandType.Database, tokens.get(++i));
								step.args.add(tokens.get(++i));
							} else
								throw (new Exception("No directory specified: " + s));
						} else
							throw (new Exception("No ending load ID specified: " + s));
					} else if (s.equals("silent"))
					{
						if (i + 1 != tokens.size())
							step = new Step(StepCommand.Silent, StepCommandType.Database, tokens.get(++i));
					} else if (s.equals("generateschema"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.GenerateSchema, StepCommandType.ETL, tokens.get(++i));
							while (i + 1 < tokens.size()
									&& (tokens.get(i + 1).equalsIgnoreCase("notime") 
											|| tokens.get(i + 1).equalsIgnoreCase("skipmissingvalues") 
											|| tokens.get(i + 1).toLowerCase().startsWith("execonchange=") 
											|| tokens.get(i + 1).toLowerCase().startsWith("databasepath=")
											|| tokens.get(i + 1).toLowerCase().startsWith("awsaccesskeyid=") 
											|| tokens.get(i + 1).toLowerCase().startsWith("awssecretkey=")
											|| tokens.get(i + 1).toLowerCase().startsWith("awstoken=")))
							{
								String tkn = tokens.get(++i);
								step.args.add(tkn);
							}
						} else
							throw (new Exception("No Load ID Specified: " + s));
					} else if (s.equals("exportwarehouse"))
					{
						if (i + 2 >= tokens.size())
						{
							throw new Exception("Invalid parameters for exportwarehouse");
						}
						step = new Step(StepCommand.ExportWarehouse, StepCommandType.ETL, tokens.get(++i));
						step.args.add(tokens.get(++i));
						if (tokens.get(i + 1).startsWith("connection="))
							step.args.add(tokens.get(++i));
					} else if (s.equals("loadinfobright"))
					{
						if (i + 1 >= tokens.size())
						{
							throw new Exception("Invalid parameters for loadinfobright");
						}
						step = new Step(StepCommand.LoadInfoBright, StepCommandType.ETL, tokens.get(++i));
					} else if (s.equals("setdefaultconnection"))
					{
						if (i + 1 >= tokens.size())
						{
							throw new Exception("Invalid parameters for setdefaultconnection");
						}
						step = new Step(StepCommand.SetDefaultConnection, StepCommandType.Init, tokens.get(++i));
					} else if (s.equals("generatetimeschema"))
					{
						step = new Step(StepCommand.GenerateTimeSchema, StepCommandType.ETL, null);
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.GenerateTimeSchema, StepCommandType.ETL, tokens.get(++i));
						} else
							throw (new Exception("No Load ID Specified: " + s));
					} else if (s.equals("executetransformation"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.ExecuteTransformation, StepCommandType.ETL, tokens.get(++i));
							if (i + 1 != tokens.size())
							{
								step.args.add(tokens.get(++i));
							} else
								throw (new Exception("No Path Specified: " + s));
							if (i + 1 != tokens.size())
							{
								step.args.add(tokens.get(++i));
							} else
								throw (new Exception("No Load ID Specified: " + s));
							if (i + 1 < tokens.size() && tokens.get(i + 1).equalsIgnoreCase("loadgroup"))
							{
								i++;
								if (i + 1 < tokens.size())
								{
									String loadGroupName = tokens.get(++i);
									step.args.add("loadgroup=" + loadGroupName);
								} else
								{
									throw new Exception("No load group name specified after \"loadgroup\" option.");
								}
							}
							if (((i + 1) < tokens.size()) && tokens.get(i + 1).equalsIgnoreCase("databasepath"))
							{
								i++;
								if (i + 1 < tokens.size())
								{
									String dbpath = tokens.get(++i);
									step.args.add("databasepath=" + dbpath);
								} else
								{
									throw new Exception("No database path specified after \"databasepath\" option.");
								}
							}
						} else
							throw (new Exception("No Script Specified: " + s));
					} else if (s.equals("loadstaging"))
					{
						if (i + 2 < tokens.size()) // make sure have dir and loadid
						{
							step = new Step(StepCommand.LoadStaging, StepCommandType.ETL, tokens.get(++i)); // data directory
							step.args.add(tokens.get(++i));	// load id

							while (++i < tokens.size())
							{
								String tkn = tokens.get(i);
								if (tkn.equalsIgnoreCase("debug") || tkn.equalsIgnoreCase("skiptransformations"))
								{
									step.args.add(tkn.toLowerCase());
								}
								else if (tkn.toLowerCase().startsWith("awsaccesskeyid=") || tkn.toLowerCase().startsWith("awssecretkey=") || tkn.toLowerCase().startsWith("awstoken="))
								{
									step.args.add(tkn);
								}
								else if (tkn.equalsIgnoreCase("loadgroup") || tkn.equalsIgnoreCase("subgroups") || tkn.equalsIgnoreCase("databasepath") || tkn.equalsIgnoreCase("numrows"))
								{
									String arg = tokens.get(++i);
									step.args.add(tkn + '=' + arg);
								}
								else
								{
									i--;
									break;
								}
							}
						} else
							throw (new Exception("No Path and/or Load ID Specified: " + s));
					} else if (s.equals("createsample"))
					{
						if (i + 5 < tokens.size())
						{
							step = new Step(StepCommand.CreateSample, StepCommandType.ETL, tokens.get(++i)); // path
							step.args.add(tokens.get(++i)); // dimension
							step.args.add(tokens.get(++i)); // level
							step.args.add(tokens.get(++i)); // staging table name
							step.args.add(tokens.get(++i)); // percent
						} else
							throw (new Exception("Invalid specification for create sample command: " + s));
					} else if (s.equals("derivesample"))
					{
						if (i + 6 < tokens.size())
						{
							step = new Step(StepCommand.DeriveSample, StepCommandType.ETL, tokens.get(++i)); // path
							step.args.add(tokens.get(++i)); // source dimension
							step.args.add(tokens.get(++i)); // source level
							step.args.add(tokens.get(++i)); // target dimension
							step.args.add(tokens.get(++i)); // target level
							step.args.add(tokens.get(++i)); // staging table name
						} else
							throw (new Exception("Invalid specification for create sample command: " + s));
					} else if (s.equals("applysample"))
					{
						if (i + 4 < tokens.size())
						{
							step = new Step(StepCommand.ApplySample, StepCommandType.ETL, tokens.get(++i)); // path
							step.args.add(tokens.get(++i)); // dimension
							step.args.add(tokens.get(++i)); // level
							step.args.add(tokens.get(++i)); // staging table name
						} else
							throw (new Exception("Invalid specification for apply sample command: " + s));
					} else if (s.equals("removesample"))
					{
						if (i + 4 < tokens.size())
						{
							step = new Step(StepCommand.RemoveSample, StepCommandType.ETL, tokens.get(++i)); //path
							step.args.add(tokens.get(++i)); // dimension
							step.args.add(tokens.get(++i)); // level
							step.args.add(tokens.get(++i)); // staging table name
						} else
							throw (new Exception("Invalid specification for remove sample command: " + s));
					} else if (s.equals("showsamples"))
					{
						if (i + 1 < tokens.size())
						{
							step = new Step(StepCommand.ShowSamples, StepCommandType.ETL, tokens.get(++i));
						} else
							throw (new Exception("Invalid specification for show samples command: " + s));
					} else if (s.equals("deletesamples"))
					{
						if (i + 1 < tokens.size())
						{
							step = new Step(StepCommand.DeleteSamples, StepCommandType.ETL, tokens.get(++i));
						} else
							throw (new Exception("Invalid specification for delete samples command: " + s));
					} else if (s.equals("loadwarehouse"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.LoadWarehouse, StepCommandType.ETL, tokens.get(++i));
							boolean repeat = true;
							while (repeat)
							{
								if (i + 1 < tokens.size() && tokens.get(i + 1).equalsIgnoreCase("loadgroup"))
								{
									i++;
									if (i + 1 < tokens.size())
									{
										String loadGroupName = tokens.get(++i);
										step.args.add("loadgroup=" + loadGroupName);
									} else
									{
										throw new Exception("No load group name specified after \"loadgroup\" option.");
									}
								} else if (i + 1 < tokens.size() && tokens.get(i + 1).equalsIgnoreCase("subgroups"))
								{
									i++;
									if (i + 1 < tokens.size())
									{
										String subgroups = tokens.get(++i);
										step.args.add("subgroup=" + subgroups);
									} else
									{
										throw new Exception("No subgroup names specified after \"subgroup\" option.");
									}
								} else if (i + 1 < tokens.size() && tokens.get(i + 1).equalsIgnoreCase("scddate"))
								{
									i++;
									if (i + 1 < tokens.size())
									{
										String scddate = tokens.get(++i);
										step.args.add("scddate=" + scddate);
									} else
									{
										throw new Exception("No date specified after \"scddate\" option.");
									}
								} else if (i + 1 < tokens.size() && tokens.get(i + 1).equalsIgnoreCase("nofactindices"))
								{
									i++;
									step.args.add("nofactindices");
								} else if (i + 1 < tokens.size() && tokens.get(i + 1).equalsIgnoreCase("keepfacts"))
								{
									i++;
									step.args.add("keepfacts");
								}else if (i + 1 < tokens.size() && tokens.get(i + 1).equalsIgnoreCase("onlymodifiedsources"))
								{
									i++;
									step.args.add("onlymodifiedsources");
								}
								else if (i + 1 < tokens.size() && (tokens.get(i + 1).toLowerCase().startsWith("databasepath=")
										|| tokens.get(i + 1).toLowerCase().startsWith("awsaccesskeyid=") || tokens.get(i + 1).toLowerCase().startsWith("awssecretkey=") || tokens.get(i + 1).toLowerCase().startsWith("awstoken=")))

								{
									step.args.add(tokens.get(++i));
								} else
									repeat = false;
							}
						} else
							throw (new Exception("No Load ID Specified: " + s));
					}
					else if (s.equals("copytable"))
					{
						if (i + 4 < tokens.size())
						{
							step = new Step(StepCommand.CopyTable, StepCommandType.ETL, tokens.get(++i)); // sourceSchema
							step.args.add(tokens.get(++i)); //  Destination Schema
							step.args.add(tokens.get(++i)); // tableName
							step.args.add(tokens.get(++i)); // databasePath
							//s3credential, require only for RedShift
							while (i + 1 < tokens.size()
									&& (tokens.get(i + 1).toLowerCase().startsWith("awsaccesskeyid=") 
											|| tokens.get(i + 1).toLowerCase().startsWith("awssecretkey=")
											|| tokens.get(i + 1).toLowerCase().startsWith("awstoken=")))
							{
								String tkn = tokens.get(++i);
								step.args.add(tkn);
							}
						} else
							throw (new Exception("Invalid arguments for command: " + s));
					}
					else if (s.equals("unload"))
					{
						if (i + 4 < tokens.size())
						{
							step = new Step(StepCommand.Unload, StepCommandType.ETL, tokens.get(++i)); // sourceSchema
							step.args.add(tokens.get(++i)); //  table
							step.args.add(tokens.get(++i)); // dir
							step.args.add(tokens.get(++i)); // keepFileNameasTableName

							if (tokens.size()>(i+1) && tokens.get(i+1)!=null && tokens.get(i+1).startsWith("targetschema"))
							{
								step.args.add(tokens.get(++i));
							}
							if (tokens.size()>(i+1) && tokens.get(i+1)!=null && tokens.get(i+1).startsWith("databaseloaddirpath"))
							{
								step.args.add(tokens.get(++i));
							}

							//s3credential, require only for RedShift
							while (i + 1 < tokens.size()
									&& (tokens.get(i + 1).toLowerCase().startsWith("awsaccesskeyid=") 
											|| tokens.get(i + 1).toLowerCase().startsWith("awssecretkey=")
											|| tokens.get(i + 1).toLowerCase().startsWith("awstoken=")))
							{
								String tkn = tokens.get(++i);
								step.args.add(tkn);
							}
						} else
							throw (new Exception("Invalid arguments for command: " + s));						
					}
					else if (s.equals("unloadusingquery"))
					{
						if (i + 4 < tokens.size())
						{
							step = new Step(StepCommand.UnloadUsingQuery, StepCommandType.ETL, tokens.get(++i));
							step.args.add(tokens.get(++i)); 
							step.args.add(tokens.get(++i));
							step.args.add(tokens.get(++i));
						} else
							throw (new Exception("Invalid arguments for command: " + s));
						
					}
					else if (s.equals("createscriptlog"))
					{
						if (i + 1 < tokens.size())
						{
							step = new Step(StepCommand.CreateScriptLog, StepCommandType.ETL, tokens.get(++i)); 
						} else
							throw (new Exception("Invalid arguments for command: " + s));
						
					}
					else if (s.equals("load"))
					{
						if (i + 3 < tokens.size())
						{
							step = new Step(StepCommand.Load, StepCommandType.ETL, tokens.get(++i)); // targetschema
							step.args.add(tokens.get(++i)); //  table
							step.args.add(tokens.get(++i)); // file

							if (tokens.size()>(i+1) && tokens.get(i+1)!=null && tokens.get(i+1).startsWith("databaseloaddirpath"))
							{
								step.args.add(tokens.get(++i));
							}

							if (tokens.size()>(i+1) && tokens.get(i+1)!=null && tokens.get(i+1).startsWith("sourceschema"))
							{
								step.args.add(tokens.get(++i));
							}

							//s3credential, require only for RedShift
							while (i + 1 < tokens.size()
									&& (tokens.get(i + 1).toLowerCase().startsWith("awsaccesskeyid=") 
											|| tokens.get(i + 1).toLowerCase().startsWith("awssecretkey=")
											|| tokens.get(i + 1).toLowerCase().startsWith("awstoken=")))
							{
								String tkn = tokens.get(++i);
								step.args.add(tkn);
							}
						} else
							throw (new Exception("Invalid arguments for command: " + s));
					}
					else if (s.equals("creatediscoveryst"))
					{
						if (i + 1 < tokens.size())
						{
							step = new Step(StepCommand.CreateDiscoveryST, StepCommandType.ETL, tokens.get(++i)); // tablename
						} else
							throw (new Exception("Invalid arguments for command: " + s));
					}
					else if (s.equals("updatecompositekeys"))
					{
						if (i + 1 < tokens.size())
						{
							step = new Step(StepCommand.UpdateCompositeKeys, StepCommandType.Database, tokens.get(++i)); // tablename
						} else
							throw (new Exception("Invalid arguments for command: " + s));
					}
					else if (s.equals("getchecksum"))
					{
						if (i + 1 < tokens.size())
						{
							step = new Step(StepCommand.GetCheckSum, StepCommandType.Database, tokens.get(++i)); // tablename
						} else
							throw (new Exception("Invalid arguments for command: " + s));
					}
					else if (s.equals("deletedata"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.DeleteData, StepCommandType.ETL, tokens.get(++i));
						} else
							throw (new Exception("No Load ID Specified: " + s));
					} else if (s.equals("deletealldata"))
					{
						step = new Step(StepCommand.DeleteAllData, StepCommandType.ETL, null);
					} else if (s.equals("deletespace"))
					{
						step = new Step(StepCommand.DeleteSpace, StepCommandType.ETL, null);
					} else if (s.equals("enforcesnapshotpolicy"))
					{
						step = new Step(StepCommand.EnforceSnapshotPolicy, StepCommandType.ETL, null);
					} else if (s.equals("executescriptgroup"))
					{
						if (i + 1 != tokens.size())
							step = new Step(StepCommand.ExecuteScriptGroup, StepCommandType.ETL, tokens.get(++i));
						else
							throw (new Exception("No Script Group Name Specified: " + s));
					} else if (s.equals("exec"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.Exec, StepCommandType.Init, tokens.get(++i));
						} else
							throw (new Exception("No Command Specified: " + s));
					} else if (s.equals("touch"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.Touch, StepCommandType.Init, tokens.get(++i));
						} else
							throw (new Exception("No Filename Specified: " + s));
					} else if (s.equals("errorcontinuepoint"))
					{
						step = new Step(StepCommand.ErrorContinuePoint, StepCommandType.Init, null);
					} else if (s.equals("console"))
					{
						setupCommandLineConsoleAppender();
					} else if (s.equals("continueonerrors"))
					{
						step = new Step(StepCommand.ContinueOnErrors, StepCommandType.Test, null);
					} else if (s.equals("exitonerrors"))
					{
						step = new Step(StepCommand.ExitOnErrors, StepCommandType.Test, null);
					} else if (s.equals("loadMarker")) // for testing MarkTheLastStep
					{
						if (i + 2 != tokens.size())
						{
							step = new Step(StepCommand.LoadMarker, StepCommandType.ETL, tokens.get(++i));
							step.args.add(tokens.get(++i));//loadgroup
							if(tokens.get(i+1).equalsIgnoreCase("status"))
							{
								i++;
								String statusCode = tokens.get(++i);
								int intStatusCodeValue = Integer.valueOf(statusCode);
								if(intStatusCodeValue == 1 || intStatusCodeValue == 2)
								{
									step.args.add(statusCode);
								}
								else
								{
									throw new Exception("Wrong status specified for markLoad : " + statusCode);
								}
							}
							else
							{
								throw new Exception("No status specified: " + s);
							}
						}
						else
							throw (new Exception("LoadNumber or Load Group Name not Specified: " + s));					
					} else if (s.equals("extractSFDCdata"))
					{
						if (i + 4 != tokens.size())
						{
							step = new Step(StepCommand.ExtractSFDCData, StepCommandType.ETL, tokens.get(++i));//SFDCClientID
							step.args.add(tokens.get(++i)); // sandBoxURL
							step.args.add(tokens.get(++i)); // noOfExtractionThreads
							step.args.add(tokens.get(++i)); // unique id for the task
						}
						else
							throw (new Exception("Not enough arguments supplied for extractSFDCdata: " + s));
					} else if (s.equals("extractconnectordata"))
					{
						if (i + 5 < tokens.size())
						{
							step = new Step(StepCommand.ExtractConnectorData, StepCommandType.ETL, tokens.get(++i));//ConnectorName
							step.args.add(tokens.get(++i)); // BIRST_Parameters (eg. SFDCClientID)
							step.args.add(tokens.get(++i)); // sandBoxURL
							step.args.add(tokens.get(++i)); // noOfExtractionThreads		
							step.args.add(tokens.get(++i)); // unique id for the task							
						}
						else
							throw (new Exception("Not enough arguments supplied for extractconnectordata: " + s));
					} else if (s.equals("emailer"))
					{
						step = new Step(StepCommand.EmailerClient, StepCommandType.ETL, tokens.get(++i)); // mailhost
						step.args.add(tokens.get(++i)); // from
						step.args.add(tokens.get(++i)); // to
					}else if (s.equals("checkIn"))
					{
						step = new Step(StepCommand.CheckIn, StepCommandType.ETL, tokens.get(++i)); // name of the task
					} else if (s.equals("testwrapper")) // for testing MarkTheLastStep
					{
						step = new Step(StepCommand.TestWrapper, StepCommandType.Test, null);
					} else if (s.equals("createtestdatafiles"))
					{
						if (i + 2 != tokens.size())
						{
							step = new Step(StepCommand.CreateTestDataFiles, StepCommandType.ETL, tokens.get(++i));//DataFilePath
							step.args.add(tokens.get(++i)); //Number of records in each file
						}
						else
							throw (new Exception("Not enough arguments supplied for createsampledatafiles: " + s));
					} else if (s.equals("performanalyzeschema"))
					{
						if (i + 2 != tokens.size())
						{
							step = new Step(StepCommand.PerformAnalyzeSchema, StepCommandType.ETL, tokens.get(++i));//LoadID
							step.args.add(tokens.get(++i)); //LoadGroup
						}
						else
							throw (new Exception("Not enough arguments supplied for performanalyseschema: " + s));
					} else if (s.equals("performvacuumschema"))
					{
						if (i + 2 != tokens.size())
						{
							step = new Step(StepCommand.PerformVacuumSchema, StepCommandType.ETL, tokens.get(++i));//LoadID
							step.args.add(tokens.get(++i)); //LoadGroup
						}
						else
							throw (new Exception("Not enough arguments supplied for performvacuumschema: " + s));
					} else if (s.equals("updatestats"))
					{
						step = new Step(StepCommand.UpdateStats,StepCommandType.Database,null);
						
					} else if (s.equals("setProperties"))
					{
						String startTag = tokens.get(++i);
						if(startTag != null && startTag.equalsIgnoreCase("start"))
						{
							boolean endTagDetected = false;
							step = new Step(StepCommand.SetProperties, StepCommandType.ETL, null);
							do
							{
								String property = tokens.get(++i);
								if(property != null && property.equalsIgnoreCase("end"))
								{
									endTagDetected = true;
									break;
								}
								else
								{
									step.args.add(property);
								}
							}while(!endTagDetected);
							
							if(!endTagDetected)
							{
								throw new Exception("No end tag detected in the setProperties command : " + s);
							}
						}
						else
						{
							throw new Exception("Invalid setProperties command format: " + s);
						}		
					} else if (s.equals("splitsourcefilesforpartitions"))
					{
						step = new Step(StepCommand.SplitSourceFilesForPartitions, StepCommandType.ETL, tokens.get(++i));
					} else if (s.equals("droptemptables"))
					{
						step = new Step(StepCommand.DropTempTables, StepCommandType.ETL, tokens.get(++i));
					} else if (s.equals("setpartitionconnection"))
					{
						step = new Step(StepCommand.SetPartitionConnection, StepCommandType.ETL, tokens.get(++i));
					} else if (s.equals("consolidatetxncmdhistory"))
					{
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.ConsolidateTxnCmdHistory, StepCommandType.ETL, tokens.get(++i));
							if (i + 1 != tokens.size())
							{
								if (i + 1 < tokens.size()
										&& (tokens.get(i + 1).equalsIgnoreCase("loadgroup") 
												|| tokens.get(i + 1).equalsIgnoreCase("subgroups")
												|| tokens.get(i + 1).equalsIgnoreCase("runinpollingmode")))
								{
									if (i + 1 < tokens.size() && tokens.get(i + 1).equalsIgnoreCase("loadgroup"))
									{
										i++;
										if (i + 1 < tokens.size())
										{
											String loadGroupName = tokens.get(++i);
											step.args.add("loadgroup=" + loadGroupName);
										} else
										{
											throw new Exception("No load group name specified after \"loadgroup\" option.");
										}
									}
									if (i + 1 < tokens.size() && tokens.get(i + 1).equalsIgnoreCase("subgroups"))
									{
										i++;
										if (i + 1 < tokens.size())
										{
											String subgroups = tokens.get(++i);
											step.args.add("subgroup=" + subgroups);
										} else
										{
											throw new Exception("No subgroup names specified after \"subgroup\" option.");
										}
									}
									if (i + 1 < tokens.size() && tokens.get(i + 1).equalsIgnoreCase("runinpollingmode"))
									{
										step.args.add("runinpollingmode");
										i++;
									}
								}
							} else
							{
								throw (new Exception("No Load ID Specified: " + s));
							}
						} else
							throw (new Exception("No Path Specified: " + s));
					}
					else if (s.equals("validatemetadata"))
					{
					  step = new Step(StepCommand.ValidateMetadata, StepCommandType.ETL, tokens.get(++i));
					  if (i < tokens.size()
					      && (tokens.get(i).equalsIgnoreCase("rule") 
					          || tokens.get(i).equalsIgnoreCase("rulegroup")))
					  {
					    if (i < tokens.size() && tokens.get(i).equalsIgnoreCase("rule"))
					    {					
					      if (i < tokens.size())
					      {
					        String ruleName = tokens.get(++i);
					        step.args.add(ruleName);
					      } else
					      {
					        throw new Exception("No rule name specified after \"rule\" option.");
					      }
					    }
					    else if (i < tokens.size() && tokens.get(i).equalsIgnoreCase("rulegroup"))
					    {								
					      if (i < tokens.size())
					      {
					        String ruleGroupName = tokens.get(++i);
					        step.args.add(ruleGroupName);
					      } else
					      {
					        throw new Exception("No rule group name specified after \"rulegroup\" option.");
					      }
					    }
					  }
					}
					else if (s.equals("fixmetadata"))
					{
					  step = new Step(StepCommand.FixMetadata, StepCommandType.ETL, tokens.get(++i));
					  if (i < tokens.size()
					      && (tokens.get(i).equalsIgnoreCase("rule") 
					          || tokens.get(i).equalsIgnoreCase("rulegroup")))
					  {
					    if (i < tokens.size() && tokens.get(i).equalsIgnoreCase("rule"))
					    {         
					      if (i < tokens.size())
					      {
					        String ruleName = tokens.get(++i);
					        step.args.add(ruleName);
					      } else
					      {
					        throw new Exception("No rule name specified after \"rule\" option.");
					      }
					    }
					    else if (i < tokens.size() && tokens.get(i).equalsIgnoreCase("rulegroup"))
					    {               
					      if (i < tokens.size())
					      {
					        String ruleGroupName = tokens.get(++i);
					        step.args.add(ruleGroupName);
					      } else
					      {
					        throw new Exception("No rule group name specified after \"rulegroup\" option.");
					      }
					    }
					  }            
					}
					else if (s.equals("copycatalog"))
					{						
						if(i+5 != tokens.size())
						{
							step = new Step(StepCommand.CopyCatalog, StepCommandType.ETL, tokens.get(++i)); // from
							step.args.add(tokens.get(++i));	//to
							step.args.add(tokens.get(++i));	//extension
							step.args.add(tokens.get(++i));	//allowed items
							step.args.add(tokens.get(++i));	//replicate
						}
						else
						{
							throw new Exception("Invalid syntax for comand: " + s);
						}
					}
					else if (s.equals("listindices"))
					{
					  step = new Step(StepCommand.ListIndices, StepCommandType.ETL, null);
					}
					else if (s.equals("listaggregatesphysical"))
					{
						step = new Step(StepCommand.ListAggregatesPhysical, StepCommandType.ETL, null);
					}
					else if (s.equals("reinitloadvariables"))
					{
						step = new Step(StepCommand.ReInitLoadVariables, StepCommandType.ETL, null);
					}
					else if (s.equals("performasynctask"))
					{						
						if (i + 1 != tokens.size())
						{
							step = new Step(StepCommand.PerformAsyncTask, StepCommandType.Async, tokens.get(++i));
							while (i + 1 < tokens.size()
									&& (tokens.get(i + 1).toLowerCase().startsWith("asyncclass=")
											|| tokens.get(i + 1).toLowerCase().startsWith("consumerindex=")
											|| tokens.get(i + 1).toLowerCase().startsWith("msgid=")											
											|| tokens.get(i + 1).toLowerCase().startsWith("username=")
											|| tokens.get(i + 1).toLowerCase().startsWith("spaceid=")
											|| tokens.get(i + 1).toLowerCase().startsWith("inputdata=")
											|| tokens.get(i + 1).toLowerCase().startsWith("resultdata=")))
							{
								String tkn = tokens.get(++i);
								step.args.add(tkn);
							}
						}
					}
					else
					{
						wq.kill();
						System.out.println("Unrecognized command: " + s);
						break;
					}
					if (step != null && step.command != null)
						steps.add(step);
				}
				wq.execute(new ExecuteSteps(steps, cmdsession));
			}
			catch (Exception ex)
			{
				logger.info(ex.getMessage());
				System.out.println(ex.getMessage());
			}
			finally
			{
				if (wq != null)
				{
					if (finish)
						wq.completeCurrentWork();
					wq.kill();
				}
			}
			ChartRendererFactory.destroyInstance();
		}
	}

	private static void setupCommandLineConsoleAppender()
	{
		ConsoleAppender commandLineConsoleAppender = null;
		if (defaultLogfileAppender != null)
			commandLineConsoleAppender = new ConsoleAppender(defaultLogfileAppender.getLayout());
		else
			commandLineConsoleAppender = new ConsoleAppender(new PatternLayout(DEFAULT_LOG_FORMAT));
		Logger.getRootLogger().addAppender(commandLineConsoleAppender);
		if (defaultLogfileAppender != null)
		{
			// Remove the default log file appender
			Logger.getRootLogger().removeAppender(defaultLogfileAppender);
			restoreConsoleAppenderAtEnd = true;
		}
	}

	private static void setupCommandLineLogfileAppender(String logFilename)
	{
		try
		{
			commandLineLogfileAppender = new FileAppender(new PatternLayout(DEFAULT_LOG_FORMAT), logFilename, true);
		} catch (IOException ioe)
		{
			logger.error(ioe.toString(), ioe);
			commandLineLogfileAppender = null;
		}
		if (commandLineLogfileAppender != null)
		{
			Logger.getRootLogger().addAppender(commandLineLogfileAppender);
			if (defaultLogfileAppender != null)
			{
				// Remove the default log file appender
				Logger.getRootLogger().removeAppender(defaultLogfileAppender);
				restoreConsoleAppenderAtEnd = true;
			}
		}
	}
	
	private static void setupDefaultLogAppender()
	{
		if (defaultLogfileAppender == null)
		{
			boolean containsConsoleAppender = false;
			@SuppressWarnings("rawtypes")
			Enumeration e = Logger.getRootLogger().getAllAppenders();
			while(e.hasMoreElements())
			{
				Object app = e.nextElement();
				if (app instanceof ConsoleAppender)
				{
					containsConsoleAppender = true;
					defaultLogfileAppender = (ConsoleAppender)app;
					break;
				}
			}
			if (!containsConsoleAppender)
			{
				PatternLayout ly = new PatternLayout(DEFAULT_LOG_FORMAT);
				defaultLogfileAppender = new ConsoleAppender(ly);
			}
		}
	}
}
