/**
 * $Id: Column.java,v 1.12 2011-09-24 01:02:16 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc;

import java.io.Serializable;
import java.util.Set;
import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.query.DimensionColumn;
import com.successmetricsinc.query.MeasureColumn;
import com.successmetricsinc.util.QuickReportDimension;
import com.successmetricsinc.util.Util;

@SuppressWarnings("rawtypes")
public class Column implements GenericFolderEntry, Comparable, Serializable
{
	private static final long serialVersionUID = 1L;
	private String dimension;
	private String hierarchy;
	private String name;
	private String displayName;
	private String memberName;
	private String clazz;
	private Set<Object> joinableTables;
	private String strName;

	public Column(Element e, Namespace ns)
	{
		dimension = e.getChildTextTrim("Dimension", ns);
		name = e.getChildTextTrim("Name", ns);
		strName = e.getChildText("Name", ns);
	}

	public Column()
	{
	}

	public Column(String dim, String column, String promptName) {
		dimension = dim;
		name = column;
		displayName = promptName;
	}

	public Column(QuickReportDimension dimensionColumn, String dName) {
		dimension = dimensionColumn.getDimensionName();
		name = dimensionColumn.getColumnName();
		displayName = dName;
	}

	public String toString()
	{
		return (this.name);
	}

	public String getName()
	{
		return (name);
	}

	public int getNumTypes()
	{
		return ((getDimension() == null) ? 1 : 2);
	}

	public String getTypeParameter(int type)
	{
		if (getDimension() == null)
			return ("mc");
		else
			switch (type)
			{
			case 0:
				return ("dn");
			case 1:
				return ("dc");
			}
		return (null);
	}

	public String getTypeValue(int type)
	{
		if (getDimension() == null)
			return (name);
		else
			switch (type)
			{
			case 0:
				return (getDimension());
			case 1:
				return (name);
			}
		return (null);
	}

	public void setJoinableTables(Set<Object> joinableTables)
	{
		this.joinableTables = joinableTables;
	}

	public Set<Object> getJoinableTables()
	{
		return joinableTables;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setDimension(String dimension)
	{
		this.dimension = dimension;
	}

	public String getDimension()
	{
		return dimension;
	}

	public String getDisplayName()
	{
		if (displayName == null)
			return getName();
		return displayName;
	}

	public void setDisplayName(String displayName)
	{
		this.displayName = displayName;
	}
	
	public String getMemberName() {
		return memberName != null ? memberName : name;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	@Override
	public int compareTo(Object o)
	{
		return (name.compareTo(((Column) o).name));
	}

	public String getClazz(Repository rep) {
		if (clazz == null) {
			if (hierarchy != null && hierarchy.trim().length() > 0) {
				clazz = "Varchar";
			}
			else if (rep != null) { 			// find class from repository
				if (dimension != null && Util.hasNonWhiteSpaceCharacters(dimension)) {
					DimensionColumn dc = rep.findDimensionColumn(dimension, name);
					if(dc != null){
						clazz = dc.DataType;
					}
				}
				else {
					MeasureColumn mc = rep.findMeasureColumn(name);
					if (mc != null)
						clazz = mc.DataType;
				}
			}
		}
		return clazz;
	}

	public void setClazz(String clazz) {
		this.clazz = clazz;
	}

	public String getHierarchy() {
		return hierarchy;
	}

	public void setHierarchy(String hierarchy) {
		this.hierarchy = hierarchy;
	}
	
	public Element getColumnElement(Namespace ns)
	{
		Element e = new Element("Column",ns);
		
		Element child = null;
		
		if (dimension!=null)
		{
			child = new Element("Dimension",ns);
			child.setText(dimension);
			e.addContent(child);
		}
		
		child = new Element("Name",ns);
		child.setText(strName);
		e.addContent(child);
				
		return e;
	}
}