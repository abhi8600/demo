package com.successmetricsinc;

import java.io.Serializable;

import org.apache.log4j.Logger;

/*
 * A placeholder object for various properties needed from Acorn Admin database
 * This allows us to not pass Acorn admin db information to SMIWeb and isolates the 2 components
 */
public class PackageSpaceProperties implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(PackageSpaceProperties.class);
	public static final String PACKAGE_ID = "packageID";
	public static final String PACKAGE_NAME = "packageName";
	public static final String SPACE_ID = "spaceID";
	public static final String SPACE_DIR = "spaceDirectory";
	public static final String SPACE_NAME = "spaceName";


	private String packageID;
	private String packageName;
	private String spaceID;
	private String spaceDirectory;
	private String spaceName;
	
	public String getPackageID() {
		return packageID;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageID(String packageID) {
		this.packageID = packageID;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	
	public String getSpaceID() {
		return spaceID;
	}
	public String getSpaceDirectory() {
		return spaceDirectory;
	}
	public void setSpaceID(String spaceID) {
		this.spaceID = spaceID;
	}
	public void setSpaceDirectory(String spaceDirectory) {
		this.spaceDirectory = spaceDirectory;
	}
	public String getSpaceName() {
		return spaceName;
	}
	public void setSpaceName(String spaceName) {
		this.spaceName = spaceName;
	}
	
	public boolean validateAllRequiredProperties(){
		if(packageID == null || packageID.trim().length() == 0){
			return false;
		}
		if(spaceID == null || spaceID.trim().length() == 0){
			return false;
		}
		if(spaceDirectory == null || spaceDirectory.trim().length() == 0){
			return false;
		}
		
		return true;
	}
	
	public void logProperties() {
		String logMessage = "packageID : " + packageID + " : packageName : " + packageName + " : spaceID : " + spaceID + " : spaceName : " + spaceName + " : spaceDirectory: " + spaceDirectory;
		logger.info(logMessage);
	}
	
	public boolean equals(Object o)
	{
		if (this == o)
			return true;
		if (!(o instanceof PackageSpaceProperties))
			return false;
		PackageSpaceProperties spaceProperties = (PackageSpaceProperties) o;
		return (spaceProperties.packageID.equals(packageID) &&
				spaceProperties.packageName.equals(packageName) &&
				spaceProperties.spaceID.equals(spaceID) && 
				spaceProperties.spaceName.equals(spaceName) &&
				spaceProperties.spaceDirectory.equals(spaceDirectory));
	}
}
