/**
 * $Id: HillClimber.java,v 1.6 2008-02-27 21:48:56 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import org.apache.log4j.Logger;
import java.util.ArrayList;
import java.util.List;
import weka.core.Instance;
import weka.core.Optimization;

/**
 * Class that implements a basic hill climbing solver. Can get caught in local minima, but is straightforward and efficient
 * 
 * @author bpeters
 * 
 */
public class HillClimber
{
	private static Logger logger = Logger.getLogger(HillClimber.class);
    private Instance inst;
    private int[] indices;
    private double[] maximums;
    private double[] minimums;
    private double[] means;
    private ObjectiveFunction objective;
    private double minError;
    private boolean solve;
    private static final double STARTING_RELATIVE_CHANGE = 0.2;
    private static final int MAX_ITERATIONS = 10000;
    private static final double ADJUSTMENT = .85;
    private static final int MAX_SAME = 40;
    private static final int MAX_SHRINK = 3;

    /**
     * Creates an instance of a hill climbing algorithm, with list of indices that can be changed and max/min values for those
     * indices Attempting to solve for values of indices that cause the objective function to get as close to zero as possible
     * (within minError) in the solve case, or maximize the objective function in the optimize case. Two methods: Greedy moves
     * each attribute in the direction of improvement, flipping signs and decreasing momentum on overshooting; Simplex is a more
     * sophisticated technique that uses multiple poings (a simplex) to approximate the solution - more robust.
     * 
     * @param inst
     *            Instance to solve
     * @param indices
     *            Indices to change
     * @param maximums
     *            Maximums for each index
     * @param minimums
     *            Minimums for each index
     * @param means
     *            Means for each index - used for calculating step sizes
     * @param objective
     *            Objective function to use
     * @param minError
     *            Minimum error (from solution when used when solving for result; for maximization, minimum amount of change
     *            considered meaningful between iterations)
     * @param solve
     *            Whether to solve for objective function equal to zero, or to maximize the objective function
     */
    public HillClimber(Instance inst, int[] indices, double[] maximums, double[] minimums, double[] means,
            ObjectiveFunction objective, double minError, boolean solve)
    {
        this.inst = inst;
        this.indices = indices;
        this.maximums = maximums;
        this.minimums = minimums;
        this.means = means;
        this.objective = objective;
        this.minError = minError;
        this.solve = solve;
    }

    /**
     * Finds a solution to the given hill climbing problem.
     * 
     * @return The objective function of the result
     * 
     * @throws Exception
     */
    public double findSolutionGreedy() throws Exception
    {
        double curObj = objective.getObjective(inst);
        double[] curIncrements = new double[indices.length];
        for (int i = 0; i<indices.length; i++)
        {
            curIncrements[i] = STARTING_RELATIVE_CHANGE*inst.value(indices[i]);
        }
        int countMax = 0;
        int iteration = 0;
        int countSame = 0;
        do
        {
            countMax = 0;
            for (int i = 0; i<indices.length; i++)
            {
                double val = inst.value(indices[i]);
                if (Double.isNaN(val))
                    val = 0;
                if (val+curIncrements[i]>maximums[indices[i]])
                {
                    countMax++;
                    inst.setValue(indices[i], maximums[indices[i]]);
                    curIncrements[i] = -curIncrements[i]*ADJUSTMENT;
                } else if (val+curIncrements[i]<minimums[indices[i]])
                {
                    countMax++;
                    inst.setValue(indices[i], minimums[indices[i]]);
                    curIncrements[i] = -curIncrements[i]*ADJUSTMENT;
                } else
                {
                    inst.setValue(indices[i], val+curIncrements[i]);
                }
                double newObj = objective.getObjective(inst);
                // If solving
                if (solve)
                {
                    // If signs have flipped, reverse direction and cut increment
                    if (newObj/curObj<0)
                    {
                        curIncrements[i] = -curIncrements[i]*ADJUSTMENT;
                        curObj = newObj;
                    }
                    // If not better, discard change
                    if (solve&&Math.abs(newObj)>Math.abs(curObj))
                    {
                        inst.setValue(indices[i], val);
                    }
                } else
                // Maximizing
                {
                    // If making worse, flip signs and divide by 2
                    if (newObj<=curObj)
                    {
                        curIncrements[i] = -curIncrements[i]*ADJUSTMENT;
                        inst.setValue(indices[i], val);
                        countSame++;
                    } else
                    {
                        curIncrements[i] = curIncrements[i]/ADJUSTMENT;
                        curObj = newObj;
                        countSame = 0;
                    }
                }
            }
        } while ((countMax<indices.length)&&(!solve||Math.abs(curObj)>minError)&&(iteration++<MAX_ITERATIONS)
                &&(countSame<MAX_SAME));
        return (curObj);
    }

    /**
     * Finds a solution to the given hill climbing problem using the Simplex method.
     * 
     * @return The objective function of the result
     * 
     * @throws Exception
     */
    public double findSolutionSimplex() throws Exception
    {
        List<double[]> faces = new ArrayList<double[]>();
        int numVertices = indices.length+1;
        // Establish simplex - n+1 vertices where n is the number of dimensions (or indices in this case), and m number
        // of offsets to current instance (where m is number of indices that can be changed)
        double[][] simplex = new double[numVertices][indices.length];
        for (int i = 0; i<numVertices; i++)
            for (int j = 0; j<indices.length; j++)
            {
                simplex[i][j] = inst.value(indices[j]);
                if (Double.isNaN(simplex[i][j]))
                    simplex[i][j] = 0;
            }
        double[] simplexValues = new double[numVertices];
        for (int i = 0; i<indices.length; i++)
        {
            double val = simplex[i][i];
            // Alter each vertex in a different direction - move closer to mean
            double mean = means[indices[i]];
            double adjust = (mean-simplex[i][i])/100;
            if (((simplex[i][i]+adjust)>maximums[indices[i]])||((simplex[i][i]+adjust)<minimums[indices[i]]))
                simplex[i][i] -= adjust;
            else
                simplex[i][i] += adjust;
            // Set objective value for each vertex
            inst.setValue(indices[i], simplex[i][i]);
            simplexValues[i] = objective.getObjective(inst);
            inst.setValue(indices[i], val);
        }
        simplexValues[indices.length] = objective.getObjective(inst);
        // Now iterate towards a solution
        int iteration = 0;
        int countSame = 0;
        int countShrink = 0;
        // Create test instance
        Instance test = (Instance) inst.copy();
        // Find best and worst vertices
        int worstVertex = 0;
        int bestVertex = 0;
        for (int i = 0; i<numVertices; i++)
        {
            if (solve ? (Math.abs(simplexValues[i])>Math.abs(simplexValues[worstVertex]))
                    : (simplexValues[i]<simplexValues[worstVertex]))
            {
                worstVertex = i;
            }
            if (solve ? (Math.abs(simplexValues[i])<Math.abs(simplexValues[bestVertex]))
                    : (simplexValues[i]>simplexValues[bestVertex]))
            {
                bestVertex = i;
            }
        }
        double lastBestObjective = simplexValues[bestVertex];
        do
        {
            boolean shrink = false;
            // Make room for a copy of the current vertix
            double[] copy = new double[indices.length];
            // Find center of face
            double[] face = new double[indices.length];
            for (int i = 0; i<numVertices; i++)
            {
                if (i!=worstVertex)
                {
                    for (int j = 0; j<indices.length; j++)
                    {
                        face[j] += simplex[i][j]/(numVertices-1);
                    }
                }
            }
            faces.add(face);
            // Reflect current worst vertex around the face
            for (int i = 0; i<indices.length; i++)
            {
                copy[i] = 2*face[i]-simplex[worstVertex][i];
                // Make sure new one is in bounds, if not clip
                if (copy[i]>maximums[indices[i]])
                {
                    copy[i] = maximums[indices[i]];
                } else if (copy[i]<minimums[indices[i]])
                {
                    copy[i] = minimums[indices[i]];
                }
                test.setValue(indices[i], copy[i]);
            }
            double newObj = objective.getObjective(test);
            // If better, try growing
            if (solve ? (Math.abs(newObj)<Math.abs(simplexValues[worstVertex])) : (newObj>simplexValues[worstVertex]))
            {
                // Try growing
                double[] growCopy = new double[indices.length];
                for (int i = 0; i<indices.length; i++)
                {
                    growCopy[i] = 3*face[i]-2*simplex[worstVertex][i];
                    // Make sure new one is in bounds, if not clip
                    if (growCopy[i]>maximums[indices[i]])
                    {
                        growCopy[i] = maximums[indices[i]];
                    } else if (growCopy[i]<minimums[indices[i]])
                    {
                        growCopy[i] = minimums[indices[i]];
                    }
                    test.setValue(indices[i], growCopy[i]);
                }
                double newgObj = objective.getObjective(test);
                // If better, keep, otherwise stick with first move
                if (solve ? (Math.abs(newgObj)<Math.abs(newObj)) : (newgObj>newObj))
                {
                    simplexValues[worstVertex] = newgObj;
                    for (int i = 0; i<indices.length; i++)
                    {
                        simplex[worstVertex][i] = growCopy[i];
                    }
                } else
                {
                    simplexValues[worstVertex] = newObj;
                    for (int i = 0; i<indices.length; i++)
                    {
                        simplex[worstVertex][i] = copy[i];
                    }
                }
            } else
            {
                // Try reflecting and shrinking
                for (int i = 0; i<indices.length; i++)
                {
                    copy[i] = (3*face[i]/2)-(simplex[worstVertex][i]/2);
                    // Make sure new one is in bounds, if not clip
                    if (copy[i]>maximums[indices[i]])
                    {
                        copy[i] = maximums[indices[i]];
                    } else if (copy[i]<minimums[indices[i]])
                    {
                        copy[i] = minimums[indices[i]];
                    }
                    test.setValue(indices[i], copy[i]);
                }
                newObj = objective.getObjective(test);
                // If better, keep
                if (solve ? (Math.abs(newObj)<Math.abs(simplexValues[worstVertex]))
                        : (newObj>simplexValues[worstVertex]))
                {
                    simplexValues[worstVertex] = newObj;
                    for (int i = 0; i<indices.length; i++)
                    {
                        simplex[worstVertex][i] = copy[i];
                    }
                } else
                {
                    // Try just shrinking
                    shrink = true;
                    for (int i = 0; i<indices.length; i++)
                    {
                        copy[i] = (3*face[i]+simplex[worstVertex][i])/4;
                        // Make sure new one is in bounds, if not clip
                        if (copy[i]>maximums[indices[i]])
                        {
                            copy[i] = maximums[indices[i]];
                        } else if (copy[i]<minimums[indices[i]])
                        {
                            copy[i] = minimums[indices[i]];
                        }
                        test.setValue(indices[i], copy[i]);
                    }
                    newObj = objective.getObjective(test);
                    if (solve ? (Math.abs(newObj)<Math.abs(simplexValues[worstVertex]))
                            : (newObj>simplexValues[worstVertex]))
                    {
                        simplexValues[worstVertex] = newObj;
                        for (int i = 0; i<indices.length; i++)
                        {
                            simplex[worstVertex][i] = copy[i];
                        }
                    } else
                    {
                        /*
                         * Otherwise, continuing to overshoot by making too big of moves, so give up and shrink all vertices
                         * towards the best one
                         */
                        for (int i = 0; i<numVertices; i++)
                        {
                            if (i!=bestVertex)
                            {
                                for (int j = 0; j<indices.length; j++)
                                {
                                    simplex[i][j] = (simplex[i][j]+3*simplex[bestVertex][j])/4;
                                    // Make sure new one is in bounds, if not clip
                                    if (simplex[i][j]>maximums[indices[j]])
                                    {
                                        simplex[i][j] = maximums[indices[j]];
                                    } else if (simplex[i][j]<minimums[indices[j]])
                                    {
                                        simplex[i][j] = minimums[indices[j]];
                                    }
                                    // Set objective value for each vertex
                                    test.setValue(indices[j], simplex[i][j]);
                                }
                                simplexValues[i] = objective.getObjective(test);
                            }
                        }
                    }
                }
            }
            // Find best and worst vertices
            for (int i = 0; i<numVertices; i++)
            {
                if (solve ? (Math.abs(simplexValues[i])>Math.abs(simplexValues[worstVertex]))
                        : (simplexValues[i]<simplexValues[worstVertex]))
                {
                    worstVertex = i;
                }
                if (solve ? (Math.abs(simplexValues[i])<Math.abs(simplexValues[bestVertex]))
                        : (simplexValues[i]>simplexValues[bestVertex]))
                {
                    bestVertex = i;
                }
            }
            if (solve)
            {
                if (Math.abs(lastBestObjective-simplexValues[bestVertex])<minError)
                {
                    countSame++;
                    if (shrink)
                        countShrink++;
                } else
                {
                    countSame = 0;
                    countShrink = 0;
                }
            } else
            {
                if (lastBestObjective==simplexValues[bestVertex])
                {
                    countSame++;
                    if (shrink)
                        countShrink++;
                } else
                {
                    countSame = 0;
                    countShrink = 0;
                }
            }
            lastBestObjective = simplexValues[bestVertex];
        } while ((!solve||Math.abs(simplexValues[bestVertex])>minError)&&(iteration++<MAX_ITERATIONS)
                &&(countShrink<MAX_SHRINK)&&(countSame<MAX_SAME));
        // Change instance to best value
        for (int j = 0; j<indices.length; j++)
            inst.setValue(indices[j], simplex[bestVertex][j]);
        return (simplexValues[bestVertex]);
    }

    class HCOpt extends Optimization
    {
        private Instance copy;

        // Provide the objective function
        protected double objectiveFunction(double[] x)
        {
            for (int i = 0; i<indices.length; i++)
            {
                copy.setValue(indices[i], x[i]);
            }
            try
            {
                return (-objective.getObjective(copy));
            } catch (Exception ex)
            {
                logger.error(ex.toString(), ex);
            }
            return (0);
        }

        // Provide the first derivatives
        protected double[] evaluateGradient(double[] x)
        {
            double[] g = new double[indices.length];
            try
            {
                for (int i = 0; i<indices.length; i++)
                {
                    copy.setValue(indices[i], x[i]);
                }
                double ob1 = -objective.getObjective(copy);
                for (int i = 0; i<indices.length; i++)
                {
                    double val = means[indices[i]];
                    if (val==0)
                    {
                        val = x[i];
                        if (val==0)
                        {
                            val = 1E-2;
                        }
                    }
                    double diff = val/(1E5);
                    copy.setValue(indices[i], x[i]+diff);
                    double ob2 = -objective.getObjective(copy);
                    if (diff==0)
                        g[i] = 0;
                    else
                        g[i] = (ob2-ob1)/diff;
                    copy.setValue(indices[i], x[i]);
                }
            } catch (Exception ex)
            {
                logger.error(ex.toString(), ex);
            }
            return (g);
        }
    }

    public double findSolutionBFGS()
    {
        try
        {
            // When it's the time to use it, in some routine(s) of other class...
            HCOpt opt = new HCOpt();
            // Set up initial variable values and bound constraints
            double[] x = new double[indices.length];
            // Lower and upper bounds: 1st row is lower bounds, 2nd is upper
            double[][] constraints = new double[2][indices.length];
            for (int i = 0; i<indices.length; i++)
            {
                constraints[0][i] = minimums[indices[i]];
                constraints[1][i] = maximums[indices[i]];
            }
            opt.copy = (Instance) inst.copy();
            opt.setMaxIteration(125);
            x = opt.findArgmin(x, constraints);
            int count = 0;
            while ((x==null)&&(count++<4))
            { // iterations are not enough
                x = opt.getVarbValues();
                x = opt.findArgmin(x, constraints);
            }
            // The minimal function value
            double minFunction = opt.getMinFunction();
            if (x==null)
            {
                x = opt.getVarbValues();
            }
            for (int i = 0; i<indices.length; i++)
            {
                inst.setValue(indices[i], x[i]);
            }
            return (minFunction);
        } catch (Exception ex)
        {
            logger.error(ex.toString(), ex);
        }
        return (0);
    }
}
