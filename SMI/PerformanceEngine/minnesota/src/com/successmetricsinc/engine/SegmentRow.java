/**
 * $Id: SegmentRow.java,v 1.8 2011-09-24 01:03:33 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
 * Class to encapsulate a single row in a given segment of a success model instance
 */
package com.successmetricsinc.engine;

import java.util.List;

import weka.classifiers.Classifier;
import weka.core.Instance;
import com.successmetricsinc.engine.Header.HeaderType;

public class SegmentRow
{
	/**
	 * 
	 */
	private Segment segment;
	private int rowIndex;
	private double[] row;
	private double[] baseRow;
	private SuccessModelInstance smi;
	private int targetIndex;

	public SegmentRow(SuccessModelInstance smi, Segment segment, int rowIndex, int targetIndex)
	{
		this.segment = segment;
		this.smi = smi;
		this.row = new double[smi.mds.numHeaders()];
		this.baseRow = new double[smi.mds.numHeaders()];
		this.targetIndex = targetIndex;
		this.rowIndex = rowIndex;
		for (int i = 0; i < smi.mds.numHeaders(); i++)
		{
			if (i == 0)
			{
				row[0] = smi.mds.getCell(rowIndex, targetIndex);
				baseRow[0] = row[0];
			}
			else
			{
				row[i] = smi.mds.getCell(rowIndex, i);
				baseRow[i] = row[i];
			}
		}
	}

	/**
	 * Create a copy of a row
	 * 
	 * @return
	 */
	public SegmentRow copy()
	{
		SegmentRow sr = new SegmentRow(this.smi, this.segment, rowIndex, targetIndex);
		return sr;
	}

	public double getColumn(int i)
	{
		return baseRow[i];
	}

	/**
	 * Find any cluster columns that are related to a given column index and update them.
	 * 
	 * @param columnIndex
	 *            Column index to use. If <0 then look at all columns
	 */
	public void setCentroids(int columnIndex)
	{
		for (int i = SuccessModelInstance.FIRSTDATAINDEX; i < smi.mds.numHeaders(); i++)
		{
			Header ch = smi.mds.getHeader(i);
			if (ch.Type == HeaderType.Cluster)
			{
				Pattern cp = smi.sm.Patterns[ch.PatternIndex];
				if (columnIndex < 0 || (cp.Startindex <= columnIndex && columnIndex < (cp.Startindex + cp.NumCategories)))
				{
					/*
					 * Now need to calculate the distance to the centroid for the base pattern measures. Need to find
					 * each measure in the list
					 */
					double[] values = new double[cp.NumCategories];
					for (int j = 0; j < cp.NumCategories; j++)
					{
						values[j] = row[cp.Startindex + j];
					}
					double[] distribution = this.segment.smi.getCentroidMemberships(cp.Centroids, values);
					for (int j = 0; j < cp.Centroids.length; j++)
						row[i + j] = distribution[j];
					i += cp.Centroids.length - 1;
				}
			}
		}
	}

	/**
	 * Update a column value - make sure that any chanes to a base measure are fully reflected in other slices of the
	 * same base measure
	 * 
	 * @param columnIndex
	 *            Index of column to change
	 * @param d
	 *            New value
	 */
	public void updateColumn(int columnIndex, double d)
	{
		row[columnIndex] = d;
		baseRow[columnIndex] = d;
		setCentroids(columnIndex);
	}

	/**
	 * Classify the segment row using a given classifier
	 * 
	 * @param cl
	 * @return
	 * @throws Exception
	 */
	public double classify(Classifier cl) throws Exception
	{
		List<Integer> selectedColumnIndexList = this.segment.selectedColumnIndexList;
		int sz = selectedColumnIndexList.size();
		double[] instRow = new double[sz + 1]; // target + selected columns

		instRow[0] = baseRow[0]; // target
		for (int j = 0; j < sz; j++)
		{
			int colIndex = selectedColumnIndexList.get(j);
			if (colIndex < 0)
				continue;
			double val = baseRow[colIndex];
			// round to prevent overflows
			instRow[j + 1] = (Math.rint(val * 1E5)) / 1E5;
		}
		Instance inst = new Instance(1, instRow);
		for (int j = 0; j < sz; j++)
		{
			if (selectedColumnIndexList.get(j) < 0)
				inst.setMissing(j + 1);
		}
		this.segment.setSampleInstances();
		inst.setDataset(this.segment.sampleInstances);
		double val = 0;
		boolean classified = false;
		int tries = 0;
		/*
		 * Loop in case there are stochastic irregularities in classification
		 */
		do
		{
			try
			{
				val = cl.classifyInstance(inst);
				classified = true;
			} catch (Exception ex)
			{
				classified = false;
			}
		} while (!classified && tries++ < 5);

		return (classified ? val : Double.NaN);
	}
}