/**
 * $Id: AttributeSearch.java,v 1.54 2011-11-03 17:37:39 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import weka.attributeSelection.BestFirst;
import weka.attributeSelection.CfsSubsetEval;
import weka.attributeSelection.ChiSquaredAttributeEval;
import weka.attributeSelection.ConsistencySubsetEval;
import weka.attributeSelection.GainRatioAttributeEval;
import weka.attributeSelection.InfoGainAttributeEval;
import weka.attributeSelection.Ranker;
import weka.attributeSelection.ReliefFAttributeEval;
import weka.attributeSelection.SVMAttributeEval;
import weka.attributeSelection.SymmetricalUncertAttributeEval;
import weka.classifiers.rules.Ridor;
import weka.classifiers.trees.J48;
import weka.core.Attribute;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Discretize;
import weka.filters.unsupervised.attribute.RemoveUseless;

import com.successmetricsinc.Repository;
import com.successmetricsinc.WorkQueue;
import com.successmetricsinc.engine.Header.HeaderType;
import com.successmetricsinc.query.DimensionColumn;

/**
 * Class to search for relevant attributes that drive the target variable
 * 
 * @author bpeters
 * 
 */
public class AttributeSearch
{
	private static Logger logger = Logger.getLogger(AttributeSearch.class);
	// Number of bins to use when turning target measure into a nominal measure
	private static final int NUM_MEASURE_BINS = 5;
	// Likely overlap between methods - need to boost screening levels
	private static final double OVERLAP_FACTOR = 0.45;
	/*
	 * Slack for finishing search - percent above the target number of attributes that requires another pass of
	 * attribute reduction
	 */
	private static double MAX_ATTRIBUTE_CEILING = 1.25; // If more than 25% beyond target, do another pass
	// Minimum sparsity below which no filtering of correlated columns will take place
	private static double MIN_SPARSITY = 0.025;
	// don't check for correlation if the number of 'good' (not NaN) values is less than this fraction of maxInstances
	private static double MIN_CORR_SIZE = 0.05;
	private SuccessModelInstance smi;
	private Repository r;
	private WorkQueue wq;
	private Map<Object, Segment> segmentMap;
	private String targetMeasure;
	private String classMeasure;
	private boolean failed = false;
	private String nameFilter = null;

	public AttributeSearch(SuccessModelInstance smi, Map<Object, Segment> segmentMap, String targetMeasure, String classMeasure)
	{
		this.smi = smi;
		this.r = smi.r;
		this.segmentMap = segmentMap;
		this.targetMeasure = targetMeasure;
		this.classMeasure = classMeasure;
	}

	/**
	 * Method to generate attribute lists for each segment in the success model instance
	 * 
	 * @throws Exception
	 */
	public boolean generateSegmentAttributeLists() throws Exception
	{
		long startTime = System.currentTimeMillis();
		segmentIterator = segmentMap.entrySet().iterator();
		wq = new WorkQueue(r.getModelParameters().MaxComputationThreads);
		Thread[] tlist = new Thread[r.getModelParameters().MaxComputationThreads];
		try
		{
			for (int i = 0; i < r.getModelParameters().MaxComputationThreads; i++)
			{
				tlist[i] = new GenerateSegmentAttributeList();
				tlist[i].start();
			}
		} catch (Exception e)
		{
			logger.error(e.toString());
		}
		try
		{
			for (int i = 0; i < r.getModelParameters().MaxComputationThreads; i++)
			{
				tlist[i].join();
			}
		} catch (InterruptedException e)
		{
			logger.error(e.toString());
		}
		wq.kill();
		long endTime = System.currentTimeMillis();
		logger.info("Total time for attribute selection: " + (endTime - startTime) + "ms");
		return !failed;
	}
	private static Iterator segmentIterator;

	private synchronized Segment getNextSegment()
	{
		if (segmentIterator.hasNext())
			return ((Segment) ((Map.Entry) segmentIterator.next()).getValue());
		else
			return (null);
	}

	/**
	 * Thread to search for attributes for a given segment
	 * 
	 * @author bpeters
	 * 
	 */
	private class GenerateSegmentAttributeList extends Thread
	{
		public GenerateSegmentAttributeList()
		{
			this.setName("Generate Segment Attribute List - " + this.getId());
		}

		public void run()
		{
			Segment s = getNextSegment();
			if (s == null)
				return;
			logger.info("Evaluating Attributes for Segment: " + s.getKey() + ", Total attributes to search: " + smi.mds.numHeaders() + ", TargetMeasure is "
					+ targetMeasure);
			/*
			 * Create instances for this segment. Need to filter out extremely highly correlated attributes - bad data
			 */
			/*
			 * Count the number of non-zero targets - check to see if sparse. Only filter correlated columns if not
			 * sparse
			 */
			int numNonzero = 0;
			int total = 0;
			int targetIndex = smi.getIndexForMeasure(targetMeasure);
			if (targetIndex < 0)
			{
				logger.error("Fatal error, can not find measure '" + targetMeasure + "'");
				failed = true;
				return;
			}
			List<Integer> rowIndexList = s.getRowIndexList();
			int sz = rowIndexList.size();
			for (int j = 0; j < sz; j++)
			{
				int row = rowIndexList.get(j);
				if (row < r.getModelParameters().MaxInstances)
				{
					double tar = smi.mds.getCell(row, targetIndex);
					if (!Double.isNaN(tar) && (tar != 0))
					{
						numNonzero++;
					}
					total++;
				}
			}
			boolean filterCorrelated = ((double) numNonzero) / ((double) total) > MIN_SPARSITY;
			List<Integer> completeColumnList = new ArrayList<Integer>();
			for (int i = 0; i < smi.mds.numHeaders(); i++)
			{
				if (i != targetIndex)
					completeColumnList.add(i);
			}
			// data set for attribute filtering (sampleDataSet is for model-based and weka removeuseless filtering)
			FilterResults fr = new FilterResults();
			Instances sampleDataSet = null;
			// Minimum number of samples for each attribute to be modeled - limits overfitting
			double MinSamplesPerAttribute = r.getModelParameters().MinSamplesPerAttribute;
			// Maximimum number of attributes to allow for model - depends on algorithm
			int MaxModelAttributes = r.getModelParameters().MaxAttributesPerModel;
			// Target rate to reduce attributes per reduction pass
			double TargetAttributeReductionRate = r.getModelParameters().TargetReductionRate;
			// Maximum attributes for current segment
			int MaxAttributes = (int) Math.round(Math.min(((double) s.getRowIndexList().size()) / ((double) MinSamplesPerAttribute), MaxModelAttributes));
			if ((smi.r.getModelParameters().UseWekaRemoveUseless || smi.r.getModelParameters().ModelBasedFiltering)
					&& (classMeasure != null && classMeasure.length() > 0))
			{
				try
				{
					s.smi = smi;
					sampleDataSet = getClassFilterSample(classMeasure, s, completeColumnList, smi.r.getModelParameters(), null);
				} catch (Exception ex)
				{
					logger.error("Could not build sample dataset for model-based filtering for classification: " + ex.toString());
				}
			}
			filterColumnList(smi, targetIndex, completeColumnList, s.getRowIndexList(), filterCorrelated, sampleDataSet, fr, nameFilter);
			logger.info("Filtered: " + fr.nullCategoriesFiltered + " null categories, " + fr.correlatedColumnsFiltered
					+ " highly correlated columns (to target), " + fr.crossCorrelatedColumnsFiltered + " cross correlated columns, " + fr.nondistinctFiltered
					+ " non distinct columns, " + fr.removeUselessFiltered + " Weka RemoveUseless columns, " + fr.modelFiltered + " columns via models, "
					+ fr.relatedColumnsFiltered + " columns that are related via meta data");
			// Number of passes required to reach this
			int NumPasses = (int) Math.round((Math.log(MaxAttributes) - Math.log(fr.columnList.size())) / Math.log(1 - TargetAttributeReductionRate));
			// Boost to reduction rate to reflect the fact that multiple methods are being
			// run and they don't fully overlap so more reduction is necessary to reach the target
			double OverlapFactor = OVERLAP_FACTOR;
			// After rounding the number of passes, target reduction rate per pass
			double AttributeReductionRate = 1 - (Math.exp((Math.log(MaxAttributes) - Math.log(fr.columnList.size())) / ((double) NumPasses)) * OverlapFactor);
			// Maximum number of attributes to be considered at a time
			int MaxAttributesPerFold = r.getModelParameters().MaxAttributesPerFold;
			// Conduct passes
			logger.info("Segment " + s.getKey() + ": " + NumPasses + " Passes");
			List<Integer> candidateList = new ArrayList<Integer>();
			candidateList.addAll(fr.columnList);
			int lastPassSize = -1;
			for (int pass = 0; pass < NumPasses; pass++)
			{
				boolean finalPass = (pass == NumPasses - 1);
				int numFolds = finalPass ? 1 : (int) Math.round(((double) candidateList.size()) / MaxAttributesPerFold);
				int numPerFold = (int) Math.ceil((double) candidateList.size() / (double) numFolds);
				List<Integer> resultList = Collections.synchronizedList(new ArrayList<Integer>());
				int curIndex = 0;
				double reductionRate = finalPass ? 1 - (((double) MaxAttributes) / ((double) candidateList.size())) * OverlapFactor : AttributeReductionRate;
				DecimalFormat df = new DecimalFormat("#0.0%");
				logger.info(s.getKey() + ": Reduction Pass: " + pass + " (" + candidateList.size() + " Attributes) " + numFolds + " Folds - "
						+ df.format(reductionRate) + " Reduction Rate");
				SelectAttributesThread[] pool = new SelectAttributesThread[numFolds];
				for (int fold = 0; fold < numFolds; fold++)
				{
					List<Integer> foldCandidates = new ArrayList<Integer>();
					for (int i = 0; (i < numPerFold) && (curIndex < candidateList.size()); i++)
						foldCandidates.add(candidateList.get(curIndex++));
					logger.info(s.getKey() + ": Pass " + pass + ", Fold " + fold + " (" + foldCandidates.size() + " Attributes)");
					pool[fold] = new SelectAttributesThread(s.getKey().toString() + "P" + pass + "F" + fold, foldCandidates, s.getRowIndexList(),
							reductionRate, resultList);
					wq.execute(pool[fold]);
				}
				for (int fold = 0; fold < numFolds; fold++)
				{
					while (!pool[fold].done())
					{
						try
						{
							Thread.sleep(1000);
						} catch (InterruptedException e)
						{
							logger.info("Attribute Search Interrupted: " + e.toString());
						}
					}
				}
				candidateList = resultList;
				// Check to see if close enough to target. If not, need one more pass
				if (finalPass && (candidateList.size() > (MaxAttributes * MAX_ATTRIBUTE_CEILING)))
				{
					/*
					 * If each pass is returning the same # of attributes, tighten the filter progressively
					 */
					if (candidateList.size() >= (((double) lastPassSize) * 0.95))
					{
						OverlapFactor /= 1.10;
					}
					pass = pass - 1;
					lastPassSize = candidateList.size();
				}
			}
			logger.info(s.getKey() + ": Total Attributes: " + candidateList.size());
			StringBuilder logBuf = new StringBuilder();
			// If reloading, clear current list
			s.getSelectedColumnIndexList().clear();
			for (int i = 0; i < candidateList.size(); i++)
			{
				s.getSelectedColumnIndexList().add(candidateList.get(i));
				logBuf.append(candidateList.get(i));
				if (i < candidateList.size() - 1)
					logBuf.append(",");
			}
			logger.info(s.getKey() + ": " + logBuf.toString());
		}
	}

	public static Instances getClassFilterSample(String classMeasure, Segment s, List<Integer> columns, ModelParameters params, List<Object> iterationValues)
			throws Exception
	{
		if (s.classSampleInstances != null)
			return (s.classSampleInstances);
		Instances sampleDataSet = null;
		if (params.SampleSize > 0)
			sampleDataSet = s.smi.constructInstances(s.getKey() + "_CLASS", s.getRowIndexList(), columns, true, true, classMeasure, false,
					s.getColumnHeaders(), iterationValues);
		else
		{
			sampleDataSet = s.smi.constructInstances(s.getKey() + "_CLASS", s.getRowIndexList(), columns, false, true, classMeasure, false, null, null, 0,
					params.MaxInstances);
			double pct = 100 * BestFitClassifier.getSampleSize(true, sampleDataSet.numAttributes(), params.Classifiers.MaxSampleInstanceValues,
					params.MinSamplesPerAttribute, BestFitClassifier.MAX_INSTANCES_PER_ATTRIBUTE);
			pct /= sampleDataSet.numInstances();
			sampleDataSet.setClassIndex(0);
			sampleDataSet = BestFitClassifier.getSample(sampleDataSet, pct, true, false, null);
			sampleDataSet.setClassIndex(0);
		}
		s.classSampleInstances = sampleDataSet;
		return (sampleDataSet);
	}

	/**
	 * Filter columns out of a column list that are likely data quality issues or not relevant predictors. Filter
	 * columns that are null category (i.e. don't contain any actionable information) or are very highly correlated (may
	 * be dupes or simply non-information columns). Also filter non-distinct columns (again, no information)
	 * 
	 * @param columns
	 *            List of columns to examine
	 * @param rowIndexList
	 *            Rows to evaluate for data
	 * @param filterCorrelated
	 *            Whether to filter highly correlated columns or not
	 * @param sampleSet
	 *            Sample data set for model-based filtering
	 * @return
	 */
	public static FilterResults filterColumnList(SuccessModelInstance smi, int targetIndex, List<Integer> columns, List<Integer> rowIndexList,
			boolean filterCorrelated, Instances sampleSet, FilterResults fr, String nameFilter)
	{
		List<Integer> cols = new ArrayList<Integer>();
		List<Integer> cols2 = new ArrayList<Integer>();
		cols.addAll(columns);
		cols2.addAll(columns);
		// filter out non-model patterns
		boolean nonModel = false;
		for (int i : cols2)
		{
			Header hd = smi.mds.getHeader(i);
			Pattern pat = smi.sm.Patterns[hd.PatternIndex];
			if (!pat.Model)
			{
				logger.info("Removing attribute because it is marked as non-model: " + hd.Name + " (" + pat.toString() + ")");
				cols.remove(Integer.valueOf(i));
				nonModel = true;
			}
		}
		if (nonModel)
		{
			cols2.clear();
			cols2.addAll(cols);
		}
		// filter out any patterns whose name matches designated substrings
		if (nameFilter != null && nameFilter.length() > 0)
		{
			logger.info("Using name filter: " + nameFilter);
			String[] substrings = nameFilter.split(",");
			for (int i : cols2)
			{
				Header hd = smi.mds.getHeader(i);
				boolean filter = false;
				for (String s : substrings)
				{
					// logger.debug("looking at '" + hd.Name + "' and '" + s + "'");
					if (hd.Name.indexOf(s) >= 0)
					{
						filter = true;
						break;
					}
				}
				if (filter)
				{
					logger.info("Removing attribute because its name matches the name filter: " + hd.Name);
					cols.remove(Integer.valueOf(i));
				}
			}
			cols2.clear();
			cols2.addAll(cols);
		}
		else
		{
			logger.info("No name filter being used");
		}
		if (!smi.r.getModelParameters().EnableAttributeSelection || (targetIndex < 0))
		{
			if (!smi.r.getModelParameters().EnableAttributeSelection)
			{
				logger.info("EnableAttributeSelection is false, skipping attribute selection");
			}
			if (targetIndex < 0)
			{
				logger.info("targetIndex is less than 0, skipping attribute selection");
			}
			// no target or no selection
			fr.columnList = cols;
			return (fr);
		}
		String targetName = smi.mds.getHeader(targetIndex).Name;
		double threshold = smi.r.getModelParameters().TargetCorrelationThreshold;
		// remove related measures
		if (smi.r.getModelParameters().UseRemoveRelated)
		{
			List<String> relatedMeasures = smi.r.getRelatedMeasures(targetName);
			if (relatedMeasures != null)
			{
				for (int i : cols2)
				{
					String name = smi.mds.getHeader(i).Name;
					if (!targetName.equals(name) && relatedMeasures.contains(name))
					{
						logger.info("Removing related measure: " + name);
						fr.relatedColumnsFiltered++;
						cols.remove(Integer.valueOf(i));
					}
				}
				cols2.clear();
				cols2.addAll(cols);
			}
		}
		else
		{
			logger.info("Not using UseRemoveRelated for attribute selection");
		}
		// do Weka RemoveUseless filtering if we have a sample data set
		String[] attributes = null;
		if (smi.r.getModelParameters().UseWekaRemoveUseless && sampleSet != null)
		{
			sampleSet.setClassIndex(0);
			attributes = removeUseless(sampleSet);
			// remove columns from the column list
			for (int i : cols2)
			{
				if (i == targetIndex)
					continue;
				if (matchesAttributes(attributes, smi.mds.getHeader(i).Name))
				{
					fr.removeUselessFiltered++;
					logger.info("Weka RemoveUseless Filtered column: " + smi.mds.getHeader(i).Name);
					cols.remove(Integer.valueOf(i));
				}
			}
			cols2.clear();
			cols2.addAll(cols);
		}
		else
		{
			logger.info("Not using UserWekaRemoveUseless for attribute selection");
		}
		// do decision tree filtering if we have a sample data set
		if (smi.r.getModelParameters().ModelBasedFiltering && sampleSet != null)
		{
			sampleSet.setClassIndex(0);
			// could imagine iterating over this many times to remove stuff...
			attributes = modelBasedFilter(sampleSet);
			// remove columns from the column list
			for (int i : cols2)
			{
				if (i == targetIndex)
					continue;
				if (matchesAttributes(attributes, smi.mds.getHeader(i).Name))
				{
					fr.modelFiltered++;
					logger.info("Model Filtered column: " + smi.mds.getHeader(i).Name);
					cols.remove(Integer.valueOf(i));
				}
			}
			cols2.clear();
			cols2.addAll(cols);
		}
		else
		{
			logger.info("Not using ModelBasedFiltering for attribute selection");
		}
		// just do one of the partitions, otherwise the inner loop will end up cycling through all partitions x # of
		// columns
		List<Partition> partitions = smi.mds.getPartitions();
		Partition part = partitions.get(0);
		// limit max instances for this to partition 0 size
		int maxInstances = smi.r.getModelParameters().MaxInstances;
		if (part.getNumRows() < maxInstances)
		{
			maxInstances = part.getNumRows();
			logger.warn("Using the size of the first partition (" + maxInstances + ") in AttributeSearch rather than MaxInstances ("
					+ smi.r.getModelParameters().MaxInstances + ")");
		}
		int arraySize = Math.min(rowIndexList.size(), maxInstances);
		/*
		 * build list of target values for each row - to test for correlation
		 */
		double[] targets = new double[arraySize];
		double[] values = new double[arraySize];
		for (int i : cols2)
		{
			if (i == targetIndex)
				continue;
			Header h = smi.mds.getHeader(i);
			if (smi.sm.FilterNullCategories && h.NullCategory)
			{
				fr.nullCategoriesFiltered++;
				logger.info("Filtered null category attribute: " + h.Name);
				cols.remove(Integer.valueOf(i));
				continue;
			}
			Double firstVal = null;
			boolean distinct = false;
			int count = 0;
			/*
			 * Get list of values for the column to test for excessive correlation
			 */
			for (int row : rowIndexList)
			{
				// limit the number of instances used in this calculation - for efficiency (don't go over a partition
				// boundary)
				if (count < maxInstances && row < maxInstances)
				{
					double tar = smi.mds.getCell(row, targetIndex);
					double val = smi.mds.getCell(row, i);
					if (!Double.isNaN(tar) && !Double.isNaN(val))
					{
						targets[count] = tar;
						values[count++] = val;
						if (!distinct)
						{
							if (firstVal == null)
							{
								firstVal = val;
							} else if (firstVal != val)
							{
								distinct = true;
							}
						}
					}
				} else
				{
					break;
				}
			}
			if ((smi.sm.Patterns[h.PatternIndex].NumCategories > 1) && !distinct)
			{
				fr.nondistinctFiltered++;
				logger.info("Filtered non-distinct category attribute: " + h.Name);
				cols.remove(Integer.valueOf(i));
			}
			double corr = correlation(targets, values, count);
			if (filterCorrelated && (Math.abs(corr) > threshold))
			{
				fr.correlatedColumnsFiltered++;
				logger.info("Filtered attribute too highly correlated to the target (" + targetName + "): " + h.Name + " (ABS(" + corr + ") > " + threshold
						+ ")");
				cols.remove(Integer.valueOf(i));
			}
		}
		if (smi.r.getModelParameters().UseCrossCorrelation)
		{
			int pre = cols.size();
			crossCorrelation(smi, cols, targetIndex, rowIndexList, maxInstances);
			fr.crossCorrelatedColumnsFiltered = (pre - cols.size());
			logger.info("Cross-correlated attributes removed: " + fr.crossCorrelatedColumnsFiltered);
		}
		else
		{
			logger.info("Not using UseCrossCorrelation for attribute selection");
		}
		fr.columnList = cols;
		return (fr);
	}

	/**
	 * check for cross-correlation between the attributes
	 * 
	 * @param smi
	 * @param columsn
	 *            column list
	 * @param targetIndex
	 *            target
	 * @param rowIndexList
	 *            rows to consider
	 * @param maxInstances
	 *            maximum number of instances to consider
	 */
	private static void crossCorrelation(SuccessModelInstance smi, List<Integer> columns, int targetIndex, List<Integer> rowIndexList, int maxInstances)
	{
		List<Integer> toBeRemoved = new ArrayList<Integer>();
		int size = Math.min(rowIndexList.size(), maxInstances);
		double threshold = smi.r.getModelParameters().CrossCorrelationThreshold;
		List<Integer> alreadySeen = new ArrayList<Integer>();
		for (int i : columns)
		{
			if ((i == targetIndex) || toBeRemoved.contains(i))
				continue;
			double[] attrArray1 = new double[size];
			alreadySeen.add(i);
			for (int j : columns)
			{
				if ((j == targetIndex) || (i == j) || toBeRemoved.contains(j))
					continue;
				if (alreadySeen.contains(j))
					continue;
				int count = 0;
				double[] attrArray2 = new double[size];
				for (int row : rowIndexList)
				{
					if ((count < maxInstances) && (row < maxInstances))
					{
						double val1 = smi.mds.getCell(row, i);
						double val2 = smi.mds.getCell(row, j);
						if (Double.isNaN(val1) || Double.isNaN(val2))
							continue;
						attrArray1[count] = val1;
						attrArray2[count++] = val2;
					} else
						break;
				}
				if (count < (MIN_CORR_SIZE * size))
				{
					// too few instances for meaningful correlation check, skip
					continue;
				}
				double corr = correlation(attrArray1, attrArray2, count);
				if (Math.abs(corr) > threshold)
				{
					Header h1 = smi.mds.getHeader(i);
					Header h2 = smi.mds.getHeader(j);
					boolean good = false;
					boolean swap = false;
					if (h1.Type == HeaderType.Attribute && h2.Type == HeaderType.Attribute)
					{
						int pos1 = h1.Name.indexOf('=');
						int pos2 = h2.Name.indexOf('=');
						if (pos1 > 0 && pos1 == pos2)
						{
							String attr1 = h1.Name.substring(0, pos1);
							String attr2 = h2.Name.substring(0, pos2);
							if (attr1.equals(attr2))
							{
								String val1 = h1.Name.substring(pos1 + 1);
								String val2 = h2.Name.substring(pos2 + 1);
								DimensionColumn dc = smi.r.findDimensionColumn(smi.sm.TargetDimension, attr1);
								List<String> desiredValues = dc.DesiredValues;
								logger.debug("Desired values: " + desiredValues);
								if (desiredValues != null)
								{
									boolean des1 = desiredValues.contains(val1);
									boolean des2 = desiredValues.contains(val2);
									if (des1 && !des2)
									{
										// order is good
										good = true;
									} else if (des1 && des2)
									{
										// use correlation
										good = false;
									} else if (!des1 && des2)
									{
										// swap
										swap = true;
										good = true;
									}
								}
							}
						}
					}
					if (!good)
					{
						// correlate each with target - more work then we need to do, we could get val1/tar1 once per i
						// loop, not per j loop
						double[] tar1 = new double[size];
						double[] tar2 = new double[size];
						double[] at1 = new double[size];
						double[] at2 = new double[size];
						int cnt1 = 0;
						int cnt2 = 0;
						for (int row : rowIndexList)
						{
							if (cnt1 < maxInstances && row < maxInstances)
							{
								double tar = smi.mds.getCell(row, targetIndex);
								double val1 = smi.mds.getCell(row, i);
								if (Double.isNaN(val1) || Double.isNaN(tar))
									continue;
								at1[cnt1] = val1;
								tar1[cnt1++] = tar;
							}
							if (cnt2 < maxInstances && row < maxInstances)
							{
								double tar = smi.mds.getCell(row, targetIndex);
								double val2 = smi.mds.getCell(row, j);
								if (Double.isNaN(val2) || Double.isNaN(tar))
									continue;
								at2[cnt2] = val2;
								tar2[cnt2++] = tar;
							}
							if (row >= maxInstances)
								break;
							if ((cnt1 >= maxInstances) && (cnt2 >= maxInstances))
								break;
						}
						double corr1 = correlation(tar1, at1, cnt1);
						double corr2 = correlation(tar2, at2, cnt2);
						if (Math.abs(corr2) > Math.abs(corr1))
						{
							// swap
							swap = true;
						}
					}
					if (swap)
					{
						logger.info("Attributes " + h1.Name + " and " + h2.Name + " are too highly correlated (ABS(" + corr + ") > " + threshold
								+ "), removing " + h1.Name);
						if (!toBeRemoved.contains(i))
						{
							toBeRemoved.add(i);
						}
						break;
					} else
					{
						logger.info("Attributes " + h1.Name + " and " + h2.Name + " are too highly correlated (ABS(" + corr + ") > " + threshold
								+ "), removing " + h2.Name);
						if (!toBeRemoved.contains(j))
						{
							toBeRemoved.add(j);
						}
					}
				}
			}
		}
		columns.removeAll(toBeRemoved);
	}

	/**
	 * correlation - more numerically stable than the Weka one that we were using
	 * 
	 * @param y1
	 *            - first attribute array
	 * @param y2
	 *            - second attribute array
	 * @param n
	 *            - number of values in the arrays
	 * @return correlation between the two arrays
	 */
	public static double correlation(double y1[], double y2[], int n)
	{
		if (n <= 1) // too few items to be meaningful, probably should be even larger, but...
			return 0;
		double sum_sq_x = 0;
		double sum_sq_y = 0;
		double sum_coproduct = 0;
		double mean_x = y1[0];
		double mean_y = y2[0];
		for (int i = 1; i < n; i++)
		{
			double sweep = (double) i / (double) (i + 1);
			double delta_x = y1[i] - mean_x;
			double delta_y = y2[i] - mean_y;
			sum_sq_x += delta_x * delta_x * sweep;
			sum_sq_y += delta_y * delta_y * sweep;
			sum_coproduct += delta_x * delta_y * sweep;
			mean_x += delta_x / i;
			mean_y += delta_y / i;
		}
		double pop_sd_x = Math.sqrt(sum_sq_x / n);
		double pop_sd_y = Math.sqrt(sum_sq_y / n);
		double cov_x_y = sum_coproduct / n;
		double corr = cov_x_y / (pop_sd_x * pop_sd_y);
		if (Double.isNaN(corr))
			return 0;
		return corr;
	}

	/**
	 * Filter attributes using weka removeUseless
	 * 
	 * @param sampleSet
	 * @return attribute selected
	 */
	private static String[] removeUseless(Instances sampleSet)
	{
		logger.debug("Doing Weka RemoveUseless filtering on a sample set of size = " + sampleSet.numInstances());
		List<String> atrs = new ArrayList<String>();
		// Use WEKA's RemoveUseless Filter to get rid of the easy stuff
		// - this should allow us to remove some of our code
		try
		{
			Enumeration iter = sampleSet.enumerateAttributes();
			HashSet<String> set = new HashSet<String>();
			while (iter.hasMoreElements())
			{
				String element = ((Attribute) (iter.nextElement())).name();
				set.add(element);
			}
			RemoveUseless m_AttrFilter = new RemoveUseless();
			m_AttrFilter.setInputFormat(sampleSet);
			sampleSet = Filter.useFilter(sampleSet, m_AttrFilter);
			iter = sampleSet.enumerateAttributes();
			while (iter.hasMoreElements())
			{
				set.remove(((Attribute) (iter.nextElement())).name());
			}
			for (String attrName : set)
			{
				atrs.add(attrName);
			}
		} catch (Exception e)
		{
			logger.error(e.getMessage(), e);
		}
		if (atrs.size() > 0)
		{
			String[] attributeNames = new String[atrs.size()];
			atrs.toArray(attributeNames);
			return attributeNames;
		}
		return null;
	}

	/**
	 * Filter attributes using simple models
	 * 
	 * @param sampleSet
	 * @return attribute selected
	 */
	private static String[] modelBasedFilter(Instances sampleSet)
	{
		logger.debug("Doing model-based filtering on a sample set of size = " + sampleSet.numInstances());
		List<String> atrs = new ArrayList<String>();
		try
		{
			boolean foundBadAttribute = false;
			do
			{
				foundBadAttribute = false;
				boolean foundBadJ48Attribute = false;
				// try out J48
				do
				{
					foundBadJ48Attribute = false;
					J48 j48 = new J48();
					j48.buildClassifier(sampleSet);
					Attribute attribute = j48.filterableAttribute();
					if (attribute != null)
					{
						// found a tree with 2 leaves, filter out the attribute
						atrs.add(attribute.name());
						logger.debug("Removing attribute from the sample set using J48: " + attribute.name());
						sampleSet.deleteAttributeAt(attribute.index());
						foundBadJ48Attribute = true;
						foundBadAttribute = true;
					}
				} while (foundBadJ48Attribute);
				boolean foundBadRidorAttribute = false;
				// do the same thing with Ridor
				do
				{
					foundBadRidorAttribute = false;
					Ridor ridor = new Ridor();
					ridor.buildClassifier(sampleSet);
					Attribute attribute = ridor.filterableAttribute();
					if (attribute != null)
					{
						// found a single attribute that can be filtered
						// - rule with 1 exception
						// - exception has 1 antecedent and the consequent is true
						atrs.add(attribute.name());
						logger.debug("Removing attribute from the sample set using Ridor: " + attribute.name());
						sampleSet.deleteAttributeAt(attribute.index());
						foundBadRidorAttribute = true;
						foundBadAttribute = true;
					}
				} while (foundBadRidorAttribute);
			} while (foundBadAttribute);
			if (atrs.size() > 0)
			{
				String[] attributeNames = new String[atrs.size()];
				atrs.toArray(attributeNames);
				return attributeNames;
			}
		} catch (Exception ex)
		{
			logger.error(ex.getMessage(), ex);
		}
		return null;
	}

	/**
	 * look for a match in the array of attributes returned by J48
	 * 
	 * @param attributes
	 * @param name
	 * @return
	 */
	private static boolean matchesAttributes(String[] attributes, String name)
	{
		if (attributes == null)
			return false;
		for (int i = 0; i < attributes.length; i++)
		{
			if (attributes[i].equals(name))
				return true;
		}
		return false;
	}

	/**
	 * Thread to do a single pass for a single fold of attribute selection
	 * 
	 * @author bpeters
	 * 
	 */
	private class SelectAttributesThread implements Runnable
	{
		private String passLabel;
		private List<Integer> candidateList;
		private List<Integer> rowIndexList;
		private double reductionRate;
		private List<Integer> resultList;
		private boolean done;

		public SelectAttributesThread(String passLabel, List<Integer> candidateList, List<Integer> rowIndexList, double reductionRate, List<Integer> resultList)
		{
			this.passLabel = passLabel;
			this.candidateList = candidateList;
			this.rowIndexList = rowIndexList;
			this.reductionRate = reductionRate;
			this.resultList = resultList;
			done = false;
		}

		public boolean done()
		{
			return (done);
		}

		public void run()
		{
			Instances dataSet = null;
			try
			{
				dataSet = smi.constructInstances(passLabel, rowIndexList, candidateList, true, false, null, false, null, null);
				dataSet.setClassIndex(0);
			} catch (Exception e)
			{
				logger.error("Selection Instance Exception: " + e.toString(), e);
			}
			logger.info("Searching Set: " + passLabel);
			// Setup set of attribute values
			Set<Integer> set = new HashSet<Integer>();
			BestFirst bf = new BestFirst();
			Ranker ranker = new Ranker();
			int[] result = null;
			int length = 0;
			int rankerCutoff = (int) Math.round((1 - reductionRate) * ((double) candidateList.size()));
			try
			{
				/*
				 * Do evaluations of numeric target first Cfs Subset Evaluation - also can't support non-numeric types
				 */
				CfsSubsetEval cfse = new CfsSubsetEval();
				cfse.setLocallyPredictive(true);
				cfse.setMissingSeperate(false);
				cfse.buildEvaluator(dataSet);
				result = bf.search(cfse, dataSet);
				// Only use locally predictive if it returns a modest number of attributes
				if (((double) result.length) / ((double) dataSet.numAttributes()) > (1 - reductionRate))
				{
					logger.info(passLabel + ": Cfs Subset Eval: locally predictive not sufficiently selective");
					cfse.setLocallyPredictive(false); // true will pick up a lot
					cfse.buildEvaluator(dataSet);
					result = bf.search(cfse, dataSet);
				}
				// Make sure that results are still in bounds
				if (((double) result.length) / ((double) dataSet.numAttributes()) < (1 - reductionRate))
				{
					logger.info(passLabel + ": Cfs Subset Eval: " + result.length);
					for (int i = 0; i < result.length; i++)
						set.add(Integer.valueOf(result[i]));
				} else
				{
					logger.info(passLabel + ": Cfs Subset Eval: not sufficiently selective");
				}
				/*
				 * Now, convert numeric target to nominal and evaluate using nominal methods Create class version of the
				 * attribute
				 */
				dataSet = discretizeNumericClass(dataSet, SuccessModelInstance.TARGETINDEX, NUM_MEASURE_BINS);
				double[][] rankedResult = null;
				// Infogain Evaluation
				if (false)
				{
					InfoGainAttributeEval ige = new InfoGainAttributeEval();
					ige.buildEvaluator(dataSet);
					ranker.search(ige, dataSet);
					rankedResult = ranker.rankedAttributes();
					for (length = 0; (length < rankerCutoff) && (length < rankedResult.length); length++)
						set.add(Integer.valueOf((int) ((double) rankedResult[length][0])));
					logger.info(passLabel + ": Infogain Eval: " + length);
				}
				// Gain Ratio Evaluation
				if (false)
				{
					GainRatioAttributeEval gre = new GainRatioAttributeEval();
					gre.buildEvaluator(dataSet);
					ranker.search(gre, dataSet);
					rankedResult = ranker.rankedAttributes();
					for (length = 0; (length < rankerCutoff) && (length < rankedResult.length); length++)
						set.add(Integer.valueOf((int) ((double) rankedResult[length][0])));
					logger.info(passLabel + ": Gain Ratio Eval: " + length);
				}
				// ChiSquare Evaluation
				if (false)
				{
					ChiSquaredAttributeEval cse = new ChiSquaredAttributeEval();
					cse.buildEvaluator(dataSet);
					ranker.search(cse, dataSet);
					rankedResult = ranker.rankedAttributes();
					for (length = 0; (length < rankerCutoff) && (length < rankedResult.length); length++)
						set.add(Integer.valueOf((int) ((double) rankedResult[length][0])));
					logger.info(passLabel + ": ChiSquare Eval: " + length);
				}
				// Relief Evaluation
				ReliefFAttributeEval re = new ReliefFAttributeEval();
				re.buildEvaluator(dataSet);
				ranker.search(re, dataSet);
				rankedResult = ranker.rankedAttributes();
				for (length = 0; (length < rankerCutoff) && (length < rankedResult.length); length++)
					set.add(Integer.valueOf((int) ((double) rankedResult[length][0])));
				logger.info(passLabel + ": Relief Eval: " + length);
				/*
				 * Symmetrical Evaluation - this, Info gain and Gain Ratio are all related to entropy gains. This
				 * corrects for certain biases (towards lots of values) in info gain
				 */
				if (true)
				{
					SymmetricalUncertAttributeEval sue = new SymmetricalUncertAttributeEval();
					sue.buildEvaluator(dataSet);
					ranker.search(sue, dataSet);
					rankedResult = ranker.rankedAttributes();
					for (length = 0; (length < rankerCutoff) && (length < rankedResult.length); length++)
						set.add(Integer.valueOf((int) ((double) rankedResult[length][0])));
					logger.info(passLabel + ": Symmetrical Uncertainty Eval: " + length);
				}
				/*
				 * Consistency Subset Evaluation - also can't support non-numeric types
				 */
				ConsistencySubsetEval csse = new ConsistencySubsetEval();
				csse.buildEvaluator(dataSet);
				result = bf.search(csse, dataSet);
				logger.info(passLabel + ": Consistency Subset Eval: " + result.length);
				for (int i = 0; i < result.length; i++)
					set.add(Integer.valueOf(result[i]));
				// SVM Subset Evaluation
				if (true)
				{
					SVMAttributeEval svme = new SVMAttributeEval();
					svme.setPercentToEliminatePerIteration((int) (reductionRate * 100));
					svme.setPercentThreshold(50);
					svme.setAttsToEliminatePerIteration(candidateList.size() / 2);
					svme.buildEvaluator(dataSet);
					ranker.search(svme, dataSet);
					rankedResult = ranker.rankedAttributes();
					for (length = 0; (length < rankerCutoff) && (length < rankedResult.length); length++)
						set.add(Integer.valueOf((int) ((double) rankedResult[length][0])));
					logger.info(passLabel + ": SVM Eval: " + length);
				}
			} catch (Exception e)
			{
				logger.error("Attribute Selection Exception: " + e.toString(), e);
			}
			// Translate indexes in set to items in the candidate list
			Iterator it = set.iterator();
			while (it.hasNext())
			{
				int index = ((Integer) it.next()).intValue() - 1;
				resultList.add(candidateList.get(index));
			}
			done = true;
		}
	}

	/**
	 * Discretize the target in a dataset
	 * 
	 * @param dataSet
	 * @param targetIndex
	 * @param numBins
	 * @return
	 * @throws Exception
	 */
	public static Instances discretizeNumericClass(Instances dataSet, int targetIndex, int numBins) throws Exception
	{
		/*
		 * Convert numeric target to nominal and evaluate using nominal methods Create class version of the attribute -
		 * binning into 10 bins
		 */
		Discretize dz = new Discretize();
		dz.setAttributeIndicesArray(new int[]
		{ targetIndex });
		dz.setBins(numBins);
		dz.setUseEqualFrequency(true);
		dataSet.setClassIndex(targetIndex < dataSet.numAttributes() - 1 ? targetIndex + 1 : targetIndex - 1);
		dz.setInputFormat(dataSet);
		dataSet = Filter.useFilter(dataSet, dz);
		dataSet.setClassIndex(targetIndex);
		return (dataSet);
	}

	public String getNameFilter()
	{
		return nameFilter;
	}

	public void setNameFilter(String nameFilter)
	{
		logger.debug("setting name filter to: " + nameFilter);
		this.nameFilter = nameFilter;
	}
}
