/**
 * $Id: InstanceValue.java,v 1.4 2007-08-30 16:26:47 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
/**
 * Utility class for ranking result sets
 */
package com.successmetricsinc.engine;

import java.util.List;

class InstanceValue implements Comparable<InstanceValue>
{
    int index;
    double value;

    public InstanceValue()
    {
    }

    public InstanceValue(int index, double value)
    {
        this.index = index;
        this.value = value;
    }

    public int compareTo(InstanceValue iv)
    {
        return (Double.compare(value, iv.value));
    }

    public String toString()
    {
        return (index+"="+value);
    }

    public static int getIndexLocation(List<InstanceValue> list, int index)
    {
        for (int i = 0; i<list.size(); i++)
        {
            if (list.get(i).index==index)
                return (i);
        }
        return (-1);
    }
}