/**
 * $Id: SuccessPatternAnalysis.java,v 1.63 2012-05-21 13:22:23 mpandit Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.query.Database;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.DimensionColumn;
import com.successmetricsinc.query.GroupBy;
import com.successmetricsinc.query.MeasureColumn;
import com.successmetricsinc.query.NavigationException;
import com.successmetricsinc.query.Query;
import com.successmetricsinc.query.SyntaxErrorException;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.RepositoryException;
import com.successmetricsinc.util.WekaUtil;

/**
 * Class to analyze a Success Model Instance for success patterns
 * 
 * @author bpeters
 * 
 */
public class SuccessPatternAnalysis implements Cloneable
{
	private static Logger logger = Logger.getLogger(SuccessPatternAnalysis.class);
	private SuccessModelInstance smi;
	private Repository r;
	private DatabaseConnection dc;
	private Connection conn;
	private PatternSearchDefinition psd;
	private static final int MAX_FIELD_WIDTH = 100;
	private static final int MAX_PATTERN_CATEGORY_FILTER_SIZE = 3000;
	// Amount to boost impact over just difference
	private static final double IMPACT_FACTOR = 100;
	// Amount ti discount attribute scores
	private static final double SCORE_FACTOR = 0.01;
	/*
	 * Power to raise probability difference to (only eliminate really similar ones). Probability is the probability
	 * that two distributions are different. Given that correlation takes into account degree of relationship, using the
	 * additional parameter of probability that distributions are different is not as critical. In order to prevent
	 * biasing just towards different distributions, regardless of whether that distribution is important, this number
	 * should be small. The default setting should be well below 1 and possibly 0 (no effect). 0.05 is a decent number
	 * to give a slight boost to strongly likely different distributions.
	 */
	private static final double PROB_DIFFERENCE_POWER = 0.05;
	/*
	 * Minimum difference between distributions. Below this threshold the algorithm will look for a different pattern
	 * that's more significant from within the same category. Above this threshold, the pattern is considered
	 * sufficiently valid and accepted
	 */
	private static final double MIN_DIFFERENCE = 0.1;
	/*
	 * Minimum number of exact matches required for a category before being able to replace
	 */
	private static final int MIN_EXACT_MATCHES = 1;
	private boolean FILTER_PEER_IMPROVEMENTS = true;
	/*
	 * When filtering for peer improvements, this is flag adds an additional predicate to the category filter that
	 * limits the output to items that are worse than the average peer value in that category (if applicable - i.e. not
	 * for proportions)
	 */
	private boolean ADD_PEER_LIMIT_TO_CATEGORY_FILTER = true;
	/*
	 * Flag to indicate whether it is valid to replace a pattern with another one outside the category if nothing
	 * interesting can be found within the same category
	 */
	private boolean REPLACE_OUTSIDE_CATEGORY = false;
	/*
	 * Flag indicating whether this pattern contains two dimensional patterns
	 */
	private boolean twoAttributes = false;
	private List<String> measuresToAnalyze;
	private List<String> categoriesToAnalyze;
	private List<String> listOfMeasures;
	private List<String> ignoredAnalysisMeasures;
	/*
	 * Need to have one name (usually a measure) for each category. Can have duplicate categories. Names don't have to
	 * be measures
	 */
	private List<String> namesToAssignToCategories;
	private Iterator<?> targetIterator;
	private Map<String, List<Peer>> peerMap;

	/**
	 * Create new success pattern analysis object
	 * 
	 * @param smi
	 *            Success Model Instance (load if null)
	 * @param r
	 *            Repository
	 * @param conn
	 *            Active database connection
	 * @param definitionName
	 *            Name of definition in repository
	 * @throws Exception
	 */
	public SuccessPatternAnalysis(SuccessModelInstance smi, Repository r, DatabaseConnection dbc, String definitionName) throws Exception
	{
		this.r = r;
		this.dc = dbc;
		logger.info("Retrieving Pattern Search Definition: " + definitionName);
		this.psd = r.findSearchDefinition(definitionName);
		if (this.psd == null)
			throw (new Exception("Unable to locate pattern search definition: " + definitionName));
		// Retrieve success model instance if necessary
		if (smi == null)
		{
			logger.info("Retrieving model (" + psd.SuccessModelName + ") associated with Pattern Search Definition");
			this.smi = WekaUtil.retrieveSMI(r.createApplicationPath(), psd.SuccessModelName, true);
			smi = this.smi;
		} else
		{
			this.smi = smi;
		}
		smi.r = r;
		/* Figure out whether space for 2 dimensional attributes is needed */
		for (Pattern p : smi.sm.Patterns)
		{
			if (p.Type == Pattern.TWOATTRIBUTE)
			{
				twoAttributes = true;
				break;
			}
		}
		// rebuild the Patterns based upon the header information
		List<Header> headers = smi.mds.getHeaders();
		for (int patIndex = 0; patIndex < smi.sm.Patterns.length; patIndex++)
		{
			smi.sm.Patterns[patIndex].NumCategories = 0;
			smi.sm.Patterns[patIndex].Startindex = 0;
		}

		for (int indx = 0; indx < headers.size(); indx++)
		{
			Header hdr = headers.get(indx);
			int patIndex = hdr.PatternIndex;
			smi.sm.Patterns[patIndex].NumCategories++;
			if (smi.sm.Patterns[patIndex].Startindex == 0)
				smi.sm.Patterns[patIndex].Startindex = indx; 
		}
		
		conn = dc.ConnectionPool.getConnection();
		List<String> columnNames = new ArrayList<String>();
		List<String> columnTypes = new ArrayList<String>();
		columnNames.add(r.getResource("Pattern Target ID").getValue());
		columnTypes.add("VARCHAR(30)");
		columnNames.add(r.getResource("Pattern Iteration ID").getValue());
		columnTypes.add("VARCHAR(30)");
		columnNames.add(r.getResource("Pattern Segment").getValue());
		columnTypes.add("VARCHAR(30)");
		columnNames.add(r.getResource("Pattern Measure Name").getValue());
		columnTypes.add("VARCHAR(60)");
		columnNames.add(r.getResource("Pattern Measure Used").getValue());
		columnTypes.add("VARCHAR(60)");
		columnNames.add(r.getResource("Pattern Segment Measure").getValue());
		columnTypes.add("VARCHAR(60)");
		columnNames.add(r.getResource("Pattern First Dimension").getValue());
		columnTypes.add("VARCHAR(60)");
		columnNames.add(r.getResource("Pattern First Attribute").getValue());
		columnTypes.add("VARCHAR(60)");
		if (twoAttributes)
		{
			columnNames.add(r.getResource("Pattern Second Dimension").getValue());
			columnTypes.add("VARCHAR(60)");
			columnNames.add(r.getResource("Pattern Second Attribute").getValue());
			columnTypes.add("VARCHAR(60)");
		}
		columnNames.add(r.getResource("Pattern Performance Category").getValue());
		columnTypes.add("VARCHAR(60)");
		columnNames.add(r.getResource("Pattern Format").getValue());
		columnTypes.add("VARCHAR(30)");
		columnNames.add(r.getResource("Pattern Rank").getValue());
		columnTypes.add("INTEGER");
		columnNames.add(r.getResource("Pattern Measure Rank").getValue());
		columnTypes.add("INTEGER");
		columnNames.add(r.getResource("Pattern Score").getValue());
		columnTypes.add("FLOAT");
		columnNames.add(r.getResource("Pattern Pattern").getValue());
		columnTypes.add("VARCHAR(" + MAX_FIELD_WIDTH + ")");
		columnNames.add(r.getResource("Pattern First Category").getValue());
		columnTypes.add("VARCHAR(" + MAX_FIELD_WIDTH + ")");
		if (twoAttributes)
		{
			columnNames.add(r.getResource("Pattern Second Category").getValue());
			columnTypes.add("VARCHAR(" + MAX_FIELD_WIDTH + ")");
		}
		columnNames.add(r.getResource("Pattern Value").getValue());
		columnTypes.add("FLOAT");
		columnNames.add(r.getResource("Pattern Reference Value").getValue());
		columnTypes.add("FLOAT");
		columnNames.add(r.getResource("Pattern Category Filter").getValue());
		columnTypes.add("VARCHAR(" + MAX_PATTERN_CATEGORY_FILTER_SIZE + ")");
		Database.createTable(r, dc, psd.OutputTableName, columnNames, columnTypes, psd.DropExistingTable, false, null, null, null);
		Map<String, String> valueByColumn = new HashMap<String, String>();
		valueByColumn.put(r.getResource("Pattern Iteration ID").getValue(), r.replaceVariables(null, psd.IterationAnalysisValue));
		Database.deleteOldData(dc, psd.OutputTableName, valueByColumn);
		Database.createIndex(dc, r.getResource("Pattern Target ID").getValue(), psd.OutputTableName);
		Database.createIndex(dc, r.getResource("Pattern Iteration ID").getValue(), psd.OutputTableName);
	}

	/**
	 * Empty constructor for cloning
	 */
	private SuccessPatternAnalysis()
	{
	}

	/**
	 * Generate a set of success patterns and store in the designated table - use two population filters
	 * 
	 * @param numThreads
	 *            Number of threads to use
	 * @throws Exception
	 */
	public void generateSuccessPatterns(int numThreads) throws Exception
	{
		Map<List<Object>, List<Object>> comparison = new HashMap<List<Object>, List<Object>>();
		if (psd.PatternSearchType == PatternSearchDefinition.TYPE_POPULATION_COMPARISON)
		{
			/*
			 * Get reference and comparison populations using filters
			 */
			List<Object> referenceList = getIDList(psd.referenceFilters);
			List<Object> comparisonList = getIDList(psd.comparisonFilters);
			comparison.put(referenceList, comparisonList);
		} else if (psd.PatternSearchType == PatternSearchDefinition.TYPE_POPULATION_SPLIT)
		{
			/*
			 * Get reference and comparison populations using splits for each segment
			 */
			for (Iterator<Entry<Object, Segment>> it = smi.getSegmentMap().entrySet().iterator(); it.hasNext();)
			{
				Segment seg = it.next().getValue();
				List<InstanceValue> list = new ArrayList<InstanceValue>();
				for (int i : seg.getRowIndexList())
				{
					InstanceValue iv = new InstanceValue();
					iv.index = i;
					iv.value = smi.mds.getCell(i, SuccessModelInstance.TARGETINDEX);
					list.add(iv);
				}
				Collections.sort(list);
				int cutoff = (int) (psd.SplitPercentage * ((double) list.size()));
				List<Object> referenceList = new ArrayList<Object>(cutoff);
				List<Object> comparisonList = new ArrayList<Object>(list.size() - cutoff);
				for (int i = 0; i < cutoff; i++)
				{
					referenceList.add(smi.mds.getTargetKey(list.get(i).index));
				}
				for (int i = cutoff; i < list.size(); i++)
				{
					comparisonList.add(smi.mds.getTargetKey(list.get(i).index));
				}
				comparison.put(referenceList, comparisonList);
			}
		}
		// Iterate through each target
		Thread[] tlist = new Thread[numThreads];
		targetIterator = comparison.entrySet().iterator();
		for (int i = 0; i < numThreads; i++)
		{
			tlist[i] = new EvaluateTarget();
			tlist[i].start();
		}
		for (int i = 0; i < numThreads; i++)
		{
			tlist[i].join();
		}
	}

	/**
	 * Get a list of ids based on a list of query filters
	 * 
	 * @param filterNames
	 * @return
	 * @throws NavigationException
	 * @throws SQLException
	 * @throws SyntaxErrorException 
	 * @throws RepositoryException 
	 * @throws ScriptException 
	 * @throws ArrayIndexOutOfBoundsException 
	 * @throws IOException 
	 */
	private List<Object> getIDList(String[] filterNames)
	throws BaseException, SQLException, CloneNotSupportedException, RepositoryException, ArrayIndexOutOfBoundsException, IOException
	{
		Query q = new Query(r);
		SuccessModel sm = r.findSuccessModel(psd.SuccessModelName);
		q.addDimensionColumn(sm.TargetDimension, sm.TargetLevel);
		q.addGroupBy(new GroupBy(sm.TargetDimension, sm.TargetLevel));
		q.addFilterList(filterNames);
		q.generateQuery(false);
		Connection conn = r.getDefaultConnection().ConnectionPool.getConnection();
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(q.getQuery());
		List<Object> idlist = new ArrayList<Object>();
		while (rs.next())
		{
			idlist.add(rs.getObject(1));
		}
		rs.close();
		stmt.close();
		return (idlist);
	}

	/**
	 * Generate a set of success patterns and store in the designated table
	 * 
	 * @param numThreads
	 *            Number of threads to use
	 * @param ID
	 *            ID to generate success patterns for - if null, generate all. For each ID, find peers and compare to
	 *            peers.
	 * @throws Exception
	 */
	public void generateSuccessPatterns(int numThreads, String ID) throws Exception
	{
		peerMap = (new Peers(r, psd.ReferencePopulationName)).retrievePeerMap(false, ID);
		// Iterate through each target
		Thread[] tlist = new Thread[numThreads];
		targetIterator = peerMap.entrySet().iterator();
		for (int i = 0; i < numThreads; i++)
		{
			tlist[i] = new EvaluateTarget();
			tlist[i].start();
		}
		for (int i = 0; i < numThreads; i++)
		{
			tlist[i].join();
		}
	}

	private synchronized Entry<?, List<Comparable>> getNextTarget()
	{
		if (!targetIterator.hasNext())
			return (null);
		Entry<?, List<Comparable>> me = (Entry<?, List<Comparable>>) targetIterator.next();
		return (me);
	}

	/*
	 * Thread for evaluating a target
	 */
	public class EvaluateTarget extends Thread
	{
		private Connection conn;
		private PreparedStatement pstmt;

		public EvaluateTarget() throws SQLException
		{
			this.setName("Evaluate Target - " + this.getId());
			// Create prepared statement for inserting in batch
			String insertQuery = "INSERT INTO " + psd.OutputTableName;
			insertQuery += " VALUES (?,?,?,?,?,?,?,?," + (twoAttributes ? "?,?,?," : "") + "?,?,?,?,?,?,?,?,?,?)";
			this.conn = dc.ConnectionPool.getConnection();
			this.pstmt = conn.prepareStatement(insertQuery);
		}

		@SuppressWarnings("unchecked")
		public void run()
		{
			Object iterationKey = r.replaceVariables(null, psd.IterationAnalysisValue);
			Object iterationKeyDB = iterationKey;
			if ((smi.sm.IterateDimension == null) || (smi.sm.IterateDimension.length() == 0))
			{
				iterationKey = "IT";
			}
			while (true)
			{
				try
				{
					Entry<?, List<Comparable>> entry = getNextTarget();
					if (entry == null)
					{
						pstmt.close();
						return;
					}
					Object target = entry.getKey();
					List<Comparable> list = entry.getValue();
					List<Object> targetList = null;
					Segment targetSegment = null;
					if (String.class.isInstance(target))
					{
						targetList = new ArrayList<Object>();
						targetList.add(target);
						targetSegment = smi.getSegment(target);
					} else if (List.class.isInstance(target))
					{
						targetList = (List<Object>) target;
						targetSegment = smi.getSegment(targetList.get(0));
					}
					if (targetSegment != null)
					{
						logger.info("Processing (SuccessPatternAnalysis) target(s): " + targetList);
						// Find top peers
						if (Peer.class.isInstance(list.get(0)))
						{
							Collections.sort(list);
						}
						// Generate sorted list of pattern differences
						List<PatternDifference> patternDifferences = rankDifferences(targetList, iterationKey, list);
						/*
						 * Pick the appropriate patterns to fit into the right categories Save to output table
						 */
						int numToSave = 0;
						if (psd.NumPatternsToSave < 0)
						{
							numToSave = patternDifferences.size();
						} else
						{
							numToSave = Math.min(patternDifferences.size(), psd.NumPatternsToSave);
						}
						List<PatternDifference> pdList = selectPatternsToSave(numToSave, patternDifferences);
						// Save all selected pattern differences
						for (int i = 0; i < pdList.size(); i++)
						{
							PatternDifference pd = (PatternDifference) pdList.get(i);
							if (Double.isNaN(pd.difference))
							{
								continue;
							}
							for (int j = 0; j < pd.values.length; j++)
							{
								if ((pd.values[j] != 0 || pd.peerValues[j] != 0)
										&& !(smi.mds.getHeader(pd.p.Startindex + j).NullCategory && smi.sm.FilterNullCategories))
								{
									String measureCat = pd.measureCat;
									if (listOfMeasures != null)
									{
										/*
										 * Replace Measure Assigned with the original name if an alaysis measure was
										 * used instead
										 */
										for (int k = 0; k < listOfMeasures.size(); k++)
										{
											MeasureColumn mc = r.findMeasureColumn((String) listOfMeasures.get(k));
											if (mc != null)
											{
												if ((mc.AnalysisMeasure != null) && (mc.AnalysisMeasure.length() > 0))
												{
													if (pd.measureCat.equals(mc.AnalysisMeasure) && !ignoredAnalysisMeasures.contains(mc.AnalysisMeasure))
													{
														measureCat = mc.ColumnName;
													}
												}
											}
										}
									}
									int curCol = 1;
									// Add row for target
									pstmt.setString(curCol++, targetList.size() == 1 ? targetList.get(0).toString() : psd.Name.substring(0, Math.min(20,
											psd.Name.length())));
									if (iterationKeyDB != null)
										pstmt.setString(curCol++, iterationKeyDB.toString());
									else
										pstmt.setNull(curCol++, Types.VARCHAR);
									pstmt.setString(curCol++, targetSegment.getKey().toString());
									pstmt.setString(curCol++, measureCat);
									pstmt.setString(curCol++, pd.p.Measure);
									// See if there's a segment measure
									MeasureColumn mc = r.findMeasureColumn(pd.p.Measure);
									String segmentMeasure = pd.p.Measure;
									boolean usingSegmentMeasure = false;
									if ((mc.SegmentMeasure != null) && (mc.SegmentMeasure.length() > 0))
									{
										segmentMeasure = mc.SegmentMeasure;
										usingSegmentMeasure = true;
									}
									pstmt.setString(curCol++, segmentMeasure);
									pstmt.setString(curCol++, pd.p.Dimension1);
									pstmt.setString(curCol++, pd.p.Attribute1);
									if (twoAttributes)
									{
										pstmt.setString(curCol++, pd.p.Dimension2);
										pstmt.setString(curCol++, pd.p.Attribute2);
									}
									pstmt.setString(curCol++, pd.performanceCategory);
									String formatStr = null;
									if (pd.p.Proportion)
									{
										formatStr = "#0%";
									} else
									{
										formatStr = pd.mc.Format;
									}
									pstmt.setString(curCol++, formatStr);
									pstmt.setInt(curCol++, i);
									pstmt.setInt(curCol++, pd.measureCount);
									if (Double.isNaN(pd.difference))
										pstmt.setNull(curCol++, Types.FLOAT);
									else
										pstmt.setFloat(curCol++, (float) pd.difference);
									String patternName = pd.p.Measure + " by " + pd.p.Attribute1;
									if (patternName.length() > MAX_FIELD_WIDTH)
									{
										patternName = patternName.substring(0, MAX_FIELD_WIDTH);
									}
									String cat1Name = smi.mds.getHeader(pd.p.Startindex + j).Attribute1Value;
									if (cat1Name.length() > MAX_FIELD_WIDTH)
									{
										cat1Name = cat1Name.substring(0, MAX_FIELD_WIDTH);
									}
									pstmt.setString(curCol++, patternName);
									pstmt.setString(curCol++, cat1Name);
									if (twoAttributes)
									{
										// XXX this is messed up, why no second pattern persistence?
										patternName += " by " + pd.p.Attribute2;
										String cat2Name = smi.mds.getHeader(pd.p.Startindex + j).Attribute2Value;
										if (cat2Name.length() > MAX_FIELD_WIDTH)
										{
											cat2Name = cat2Name.substring(0, MAX_FIELD_WIDTH);
										}
										pstmt.setString(curCol++, cat2Name);
									}
									// Add row for target
									if (Double.isNaN(pd.values[j]))
										pstmt.setNull(curCol++, Types.DOUBLE);
									else
										pstmt.setDouble(curCol++, pd.values[j]);
									// Add row for peers
									if (Double.isNaN(pd.peerValues[j]))
										pstmt.setNull(curCol++, Types.DOUBLE);
									else
										pstmt.setDouble(curCol++, pd.peerValues[j]);
									List<String> filterStringList = new ArrayList<String>();
									for (int k = 0; k < pd.category1List.length; k++)
									{
										if (pd.category1List[k] != null)
										{
											double weight = 1;
											if (pd.ba != null && psd.IgnoreZeroWeights)
											{
												/*
												 * Take the average weight of breakout attributes across the entire
												 * population of targets
												 */
												double keyWeight = 0;
												int count = 0;
												for (Object o : targetList)
												{
													keyWeight += pd.ba.getAttributeSetWeight(o, iterationKey, pd.category1List[k], pd.category2List[k]);
													count++;
												}
												if (count > 0)
												{
													weight = keyWeight / count;
												}
											}
											/*
											 * Don't put filters around categories with zero weights if the ignore
											 * option is being used
											 */
											if (weight != 0 || !psd.IgnoreZeroWeights)
											{
												String filter = "FDC{" + pd.p.Dimension1 + "." + pd.p.Attribute1 + "=" + pd.category1List[k] + "}";
												String op = (pd.mc.Improve > 0) ? "<" : ((pd.mc.Improve < 0) ? ">" : null);
												if (ADD_PEER_LIMIT_TO_CATEGORY_FILTER && (!pd.p.Proportion) && (op != null) && (!usingSegmentMeasure))
												{
													filter = "FAND{" + filter + ",FM{" + pd.p.Measure + op + pd.peerValues[k] + "}}";
												}
												if (pd.p.Type == Pattern.TWOATTRIBUTE)
												{
													filter = "FAND{" + filter + ",FDC{" + pd.p.Dimension2 + "." + pd.p.Attribute2 + "=" + pd.category2List[k]
															+ "}}";
												}
												filterStringList.add(filter);
											}
										}
									}
									StringBuilder categoryFilterString = new StringBuilder();
									for (int k = 0; k < filterStringList.size(); k++)
									{
										if (k > 0)
											categoryFilterString.append(',');
										categoryFilterString.append(filterStringList.get(k));
									}
									if (filterStringList.size() > 1)
									{
										categoryFilterString.insert(0, "FOR{");
										categoryFilterString.append("}");
									}
									pstmt.setString(curCol++, categoryFilterString.toString());
									pstmt.addBatch();
								}
							}
							pstmt.executeBatch();
						}
					}
				} catch (Exception ex)
				{
					logger.error(ex.toString(), ex);
				}
			}
		}
	}

	/**
	 * Add a given pattern difference to the list of categories to analyze (i.e. the ones to include in the weighting of
	 * the overall pattern).
	 * 
	 * @param pd
	 *            Pattern difference to add to
	 * @param index
	 *            Index of category within pattern
	 * @param categoriesToAnalyze
	 *            Structure indicating which categories are to be analyzed
	 * @param numCategoriesAnalyzed
	 *            Number of categories analyzed so far
	 * @return New number of categories analyzed
	 */
	private int addCategoryToAnalyze(PatternDifference pd, int index, boolean[] categoriesToAnalyze, int numCategoriesAnalyzed)
	{
		Pattern p = pd.p;
		Header h = smi.mds.getHeader(p.Startindex + index);
		if (p.Type == Pattern.ONEATTRIBUTE)
		{
			DimensionColumn dc = r.findDimensionColumn(p.Dimension1, p.Attribute1);
			if (dc != null)
			{
				if (((dc.DesiredValues.size() == 0) || dc.DesiredValues.contains(h.Attribute1Value))
						&& ((dc.IgnoredValues.size() == 0) || !dc.IgnoredValues.contains(h.Attribute1Value)))
				{
					categoriesToAnalyze[index] = true;
					pd.category1List[index] = h.Attribute1Value;
					numCategoriesAnalyzed++;
				}
			}
		} else if (p.Type == Pattern.TWOATTRIBUTE)
		{
			DimensionColumn dc1 = r.findDimensionColumn(p.Dimension1, p.Attribute1);
			DimensionColumn dc2 = r.findDimensionColumn(p.Dimension2, p.Attribute2);
			if ((dc1 != null) && (dc2 != null))
			{
				if ((((dc1.DesiredValues.size() == 0) || dc1.DesiredValues.contains(h.Attribute1Value)) && ((dc2.DesiredValues.size() == 0) || dc2.DesiredValues
						.contains(h.Attribute2Value)))
						&& (((dc1.IgnoredValues.size() == 0) || !dc1.IgnoredValues.contains(h.Attribute1Value)) && ((dc2.IgnoredValues.size() == 0) || !dc2.IgnoredValues
								.contains(h.Attribute2Value))))
				{
					categoriesToAnalyze[index] = true;
					pd.category1List[index] = h.Attribute1Value;
					pd.category2List[index] = h.Attribute2Value;
					numCategoriesAnalyzed++;
				}
			}
		}
		return (numCategoriesAnalyzed);
	}

	/**
	 * Return the value arrays for use in a given success pattern
	 * 
	 * @param p
	 *            Pattern to use
	 * @param iterationKey
	 *            Iteration key
	 * @param list
	 *            List of object keys or peers
	 * @param min
	 *            Minimum value to consider
	 * @param max
	 *            Maximum value to consider
	 * @return
	 */
	private double[] getValueArray(Pattern p, Object iterationKey, List<?> list, double min, double max)
	{
		double[] arr = new double[p.NumCategories];
		int count = 0;
		for (Object o : list)
		{
			Object key = null;
			if (Peer.class.isInstance(o))
			{
				if (count >= psd.NumPeersToCompare)
					break;
				key = ((Peer) o).key;
			} else
				key = o;
			int index = smi.mds.getRowIndex(key, iterationKey);
			if (index >= 0)
			{
				double total = 0;
				for (int i = 0; i < p.NumCategories; i++)
				{
					double val = WekaUtil.propVal(smi.mds.getCell(index, p.Startindex + i));
					if (val > max)
						val = max;
					else if (val < min)
						val = min;
					arr[i] += val;
					total += val;
				}
				if (!p.Proportion || (total != 0))
					count++;
			}
		}
		if (count > 0)
		{
			for (int i = 0; i < p.NumCategories; i++)
				arr[i] /= count;
			return (arr);
		} else
			return (null);
	}

	/**
	 * Generate a list of pattern differences ranked by significance
	 * 
	 * @param targetKey
	 * @param iterationKey
	 * @param peerList
	 * @return
	 */
	private List<PatternDifference> rankDifferences(List<Object> targetKeyList, Object iterationKey, List<Comparable> referenceList) throws Exception
	{
		// SegmentRow sr = new SegmentRow(smi, targetSegment, targetRowIndex);
		/*
		 * Iterate through each success pattern to find out which ones are the most different
		 */
		List<PatternDifference> patternDifferences = new ArrayList<PatternDifference>();
		for (int patternIndex = 0; patternIndex < smi.sm.Patterns.length; patternIndex++)
		{
			Pattern p = smi.sm.Patterns[patternIndex];
			MeasureColumn mc = r.findMeasureColumn(p.Measure);
			double max = Double.POSITIVE_INFINITY;
			double min = Double.NEGATIVE_INFINITY;
			try
			{
				max = Double.parseDouble(mc.Maximum);			
				min = Double.parseDouble(mc.Minimum);
			}
			catch (Exception ex)
			{
				logger.error("Invalid Minimum and/or Maximum value(s) for the measure " + p.Measure);
			}
			/*
			 * If only analyzing a subset of measures and categories (or a specific set), then make sure this pattern
			 * belongs to that set
			 */
			boolean analyze = false;
			if (measuresToAnalyze != null)
			{
				if (measuresToAnalyze.contains(p.Measure))
				{
					analyze = true;
				}
			}
			if ((!analyze) && (categoriesToAnalyze != null))
			{
				if (categoriesToAnalyze.contains(mc.PerformanceCategory))
				{
					analyze = true;
				}
			}
			if ((measuresToAnalyze == null) && (categoriesToAnalyze == null))
			{
				analyze = true;
			}
			Set<String> dim1Categories = new HashSet<String>(p.NumCategories);
			Set<String> dim2Categories = new HashSet<String>(p.NumCategories);
			for (int i = p.Startindex; i < p.Startindex + p.NumCategories; i++)
			{
				dim1Categories.add(smi.mds.getHeader(i).Attribute1Value);
				if (p.Type == Pattern.TWOATTRIBUTE)
					dim2Categories.add(smi.mds.getHeader(i).Attribute2Value);
			}
			if ((dim1Categories.size() > 1) && (p.Type != Pattern.TWOATTRIBUTE || dim2Categories.size() > 1) && analyze)
			{
				// Get the pattern for the target (take first from the list)
				Segment s = smi.getSegment(targetKeyList.get(0));
				double[] valArr = getValueArray(p, iterationKey, targetKeyList, min, max);
				// Setup average of peers
				double[] peerArr = getValueArray(p, iterationKey, referenceList, min, max);
				// If peers could be found, calc diff for pattern
				if (peerArr != null && valArr != null)
				{
					PatternDifference pd = new PatternDifference();
					pd.p = p;
					pd.values = valArr;
					pd.peerValues = peerArr;
					double valTotal = 0, peerTotal = 0;
					/*
					 * If only considering peer improvements, find the categories where peers show improvement
					 */
					boolean[] categoriesToAnalyze = new boolean[p.NumCategories];
					int numCategoriesAnalyzed = 0;
					pd.category1List = new String[p.NumCategories];
					pd.category2List = new String[p.NumCategories];
					if (FILTER_PEER_IMPROVEMENTS)
					{
						for (int i = 0; i < p.NumCategories; i++)
						{
							Header h = smi.mds.getHeader(p.Startindex + i);
							/*
							 * Don't count as zero columns where there is no data
							 */
							if (Double.isNaN(valArr[i]))
							{
								valArr[i] = 0;
							} else if ((valArr[i] != 0 || peerArr[i] != 0) && !(h.NullCategory && smi.sm.FilterNullCategories))
							{
								if ((mc.Improve == 0) || ((mc.Improve > 0) && (peerArr[i] > valArr[i])) || ((mc.Improve < 0) && (peerArr[i] < valArr[i])))
								{
									numCategoriesAnalyzed = addCategoryToAnalyze(pd, i, categoriesToAnalyze, numCategoriesAnalyzed);
								}
							}
						}
					} else
					{
						// If not filtering, compare all columns
						for (int i = 0; i < p.NumCategories; i++)
						{
							Header h = smi.mds.getHeader(p.Startindex + i);
							if ((valArr[i] != 0 || peerArr[i] != 0) && !(h.NullCategory && smi.sm.FilterNullCategories))
							{
								numCategoriesAnalyzed = addCategoryToAnalyze(pd, i, categoriesToAnalyze, numCategoriesAnalyzed);
							}
						}
					}
					// Calculate totals for each distribution
					for (int i = 0; i < p.NumCategories; i++)
					{
						valTotal += valArr[i];
						peerTotal += peerArr[i];
					}
					/*
					 * Calculate number of non-zero categories and attribute scores
					 */
					int numNonzero = 0;
					for (int i = 0; i < p.NumCategories; i++)
					{
						Header h = smi.mds.getHeader(p.Startindex + i);
						if (((valArr[i] != 0) || (peerArr[i] != 0)) && !(h.NullCategory && smi.sm.FilterNullCategories))
						{
							numNonzero++;
						}
					}
					BreakoutAttribute ba = smi.mds.findBreakoutAttributeWeights(mc.AttributeWeightMeasure, p.Dimension1, p.Attribute1, p.Dimension2,
							p.Attribute2);
					pd.ba = ba;
					if ((numCategoriesAnalyzed > 0) && (numNonzero > 1))
					{
						double totalChangeEstimate = 0;
						for (int i = 0; i < p.NumCategories; i++)
						{
							/*
							 * If it's a proportioned pattern - consider all columns, even ones that aren't showing
							 * peers as better. Because this is a proportion, it is a zero-sum game - any improvement
							 * MUST be accompanied by a loss somewhere else.
							 * 
							 */
							double changeEstimate = s.getLinearChangeEstimate(i + p.Startindex, peerArr[i] - valArr[i]);
							double weight = 1;
							if (ba != null)
							{
								Header h = smi.mds.getHeader(p.Startindex + i);
								int count = 0;
								double keyWeight = 0;
								/*
								 * Take the average weight of breakout attributes across the entire population of
								 * targets
								 */
								for (Object o : targetKeyList)
								{
									keyWeight += ba.getAttributeSetWeight(o, iterationKey, h.Attribute1Value, h.Attribute2Value);
									count++;
								}
								if (count > 0)
								{
									weight = keyWeight / ((double) count);
									changeEstimate *= weight;
								}
							}
							/*
							 * If ignoring categories that correspond to a weight of zero, then skip them. Otherwise,
							 * add the correlation effect of each category
							 */
							if (!psd.IgnoreZeroWeights || weight != 0)
							{
								if (p.Proportion)
								{
									/*
									 * Calculate the incremental correlation prediction from this change
									 */
									totalChangeEstimate += changeEstimate;
								} else
								{
									/*
									 * In the case where it's not a proportion, only consider improvements in columns
									 * where peers are better. It's possible to selectively change individual categories
									 */
									if (categoriesToAnalyze[i])
									{
										/*
										 * Calculate the incremental correlation prediction from this change
										 */
										totalChangeEstimate += changeEstimate;
									}
								}
							}
						}
						// Calculate impact
						pd.impact = totalChangeEstimate;
						// Record difference
						pd.probDifference = WekaUtil.calcDiff(valArr, peerArr);
						/*
						 * Difference is calculated to prioritize likely impact first, then pure differences
						 */
						pd.difference = (IMPACT_FACTOR * pd.impact) * (Math.pow(pd.probDifference, PROB_DIFFERENCE_POWER));
						/*
						 * Add attribute score (used to add preference to certain attributes)
						 */
						if (p.Type == Pattern.ONEATTRIBUTE)
						{
							DimensionColumn dc = r.findDimensionColumn(p.Dimension1, p.Attribute1);
							if (dc != null)
							{
								/*
								 * In general, give a boost to higer scored items
								 */
								pd.difference *= (1 + dc.Score);
								/*
								 * Use scoring as tie-breaker when difference is otherwise zero
								 */
								pd.difference += dc.Score * SCORE_FACTOR;
							}
						} else if (p.Type == Pattern.TWOATTRIBUTE)
						{
							DimensionColumn dc1 = r.findDimensionColumn(p.Dimension1, p.Attribute1);
							DimensionColumn dc2 = r.findDimensionColumn(p.Dimension2, p.Attribute2);
							if ((dc1 != null) && (dc2 != null))
							{
								/*
								 * In general, give a boost to higer scored items
								 */
								pd.difference *= (1 + dc1.Score) * (1 + dc2.Score);
								/*
								 * Use scoring as tie-breaker when difference is otherwise zero
								 */
								pd.difference += dc1.Score * SCORE_FACTOR + dc2.Score * SCORE_FACTOR;
							}
						}
					} else if (numNonzero == 0)
					{
						// If absolutely no data, then rank at bottom
						pd.impact = Double.NEGATIVE_INFINITY;
						pd.probDifference = 0;
						pd.difference = Double.NEGATIVE_INFINITY;
					} else
					{
						pd.impact = 0;
						pd.probDifference = WekaUtil.calcDiff(valArr, peerArr);
						pd.difference = 0;
					}
					if (!Double.isNaN(pd.difference))	
						patternDifferences.add(pd);
				}
			}
		}
		/*
		 * Sort list by differences, and take the ones with the greatest difference
		 */
		Collections.sort(patternDifferences);
		return (patternDifferences);
	}

	/**
	 * Return the selected list of pattern differences from the overall, sorted list
	 * 
	 * @param numToSave
	 * @param patternDifferences
	 * @return
	 */
	private List<PatternDifference> selectPatternsToSave(int numToSave, List<PatternDifference> patternDifferences)
	{
		int numSaved = 0;
		int minPerMeasureCategory = (psd.NumPatternsPerMeasure == 0 ? 3 : psd.NumPatternsPerMeasure);
		int numCategoriesFull = 0;
		int otherPatterns = 0;
		// Total number of measures/categories to assign patterns to
		int numMeasuresToAnalyze = (measuresToAnalyze == null ? 0 : measuresToAnalyze.size());
		int numCategoriesToAnalyze = (categoriesToAnalyze == null ? 0 : categoriesToAnalyze.size());
		int numMeasureCategoriesToAnalyze = numMeasuresToAnalyze + numCategoriesToAnalyze;
		// Counts of assignments
		int[] measureAssignmentCounts = new int[numMeasuresToAnalyze];
		int[] categoryAssignmentCounts = new int[numCategoriesToAnalyze];
		HashMap<String, Integer> measureCounts = new HashMap<String, Integer>();
		List<PatternDifference> topPDs = new ArrayList<PatternDifference>();
		List<PatternDifference> pdList = new ArrayList<PatternDifference>();
		List<PatternDifference> replaceList = new ArrayList<PatternDifference>();
		/*
		 * Generate a list of all the most relevant differences in the right categories. Go through the entire list of
		 * pattern differences first
		 */
		for (int i = 1; ((numSaved <= numToSave) || (numCategoriesFull < numMeasureCategoriesToAnalyze)) && (i <= patternDifferences.size()); i++)
		{
			PatternDifference pd = (PatternDifference) patternDifferences.get(patternDifferences.size() - i);
			Pattern p = pd.p;
			MeasureColumn mc = r.findMeasureColumn(p.Measure);
			pd.performanceCategory = mc.PerformanceCategory;
			pd.mc = mc;
			int measureCount = 1;
			if (measuresToAnalyze != null)
			{
				boolean assignToCategory = false;
				/*
				 * Check to see if this is measure was supposed to be analyzed, but can only analyze it's category
				 */
				if (!measuresToAnalyze.contains(p.Measure))
				{
					assignToCategory = true;
				} else
				{
					int index = measuresToAnalyze.indexOf(p.Measure);
					/*
					 * If this is one of the ones we're looking for, see if not full, and take it
					 */
					if (measureAssignmentCounts[index] < minPerMeasureCategory)
					{
						/*
						 * Make sure that this pattern isn't blocked by another one
						 */
						DimensionColumn dc = r.findDimensionColumn(p.Dimension1, p.Attribute1);
						if ((p.Type != Pattern.ONEATTRIBUTE) || !containsBlockingGroup(pdList, p.Measure, dc.BlockingGroup))
						{
							pd.measureCat = p.Measure;
							measureAssignmentCounts[index]++;
							measureCount = measureAssignmentCounts[index];
							if (measureCount >= minPerMeasureCategory)
								numCategoriesFull++;
						}
					} else
					{
						/*
						 * If this measure is full, allow this pattern to be assigned to another in the same category
						 */
						assignToCategory = true;
					}
				}
				if (assignToCategory)
				{
					/*
					 * If this is a pattern measure that was associated with a category, not a specific performance
					 * measure, then pick an analysis measure to assign it (and ensure it's in the right category)
					 */
					int pick = 0;
					for (; pick < numCategoriesToAnalyze; pick++)
					{
						String m = (String) namesToAssignToCategories.get(pick);
						MeasureColumn pmc = r.findMeasureColumn(m);
						if (mc.PerformanceCategory.equals(pmc.PerformanceCategory))
						{
							int pickCount = categoryAssignmentCounts[pick];
							// If this category isn't full, then assign
							if (pickCount < minPerMeasureCategory)
							{
								break;
							}
						}
					}
					// If not all assigned, then assign this one
					if (pick < numCategoriesToAnalyze)
					{
						/*
						 * Make sure that this pattern isn't blocked by another one
						 */
						DimensionColumn dc = r.findDimensionColumn(p.Dimension1, p.Attribute1);
						if ((p.Type != Pattern.ONEATTRIBUTE) || !containsBlockingGroup(pdList, (String) namesToAssignToCategories.get(pick), dc.BlockingGroup))
						{
							pd.measureCat = (String) namesToAssignToCategories.get(pick);
							categoryAssignmentCounts[pick]++;
							measureCount = categoryAssignmentCounts[pick];
							if (measureCount >= minPerMeasureCategory)
								numCategoriesFull++;
						}
					}
				}
			} else
			{
				/*
				 * If not looking for a specific set of measures, just take the current one. Get the count for that
				 * measure and increment it
				 */
				pd.measureCat = p.Measure;
				Integer cnt = (Integer) measureCounts.get(pd.measureCat);
				if (cnt == null)
				{
					measureCount = 1;
					measureCounts.put(pd.measureCat, Integer.valueOf(1));
				} else
				{
					measureCount = cnt.intValue();
					measureCount++;
					measureCounts.put(pd.measureCat, Integer.valueOf(measureCount));
				}
			}
			/*
			 * If this is an assigned measure, then put it on the save list, or we haven't saved enough yet
			 */
			if (((pd.measureCat != null) && (measureCount <= minPerMeasureCategory)) || (numSaved <= numToSave))
			{
				// If a slot was found for this pattern
				if (pd.measureCat != null)
				{
					/*
					 * See if need to substitute a better pattern. First, look for the top unused pattern that's in the
					 * same category
					 */
					PatternDifference topPD = null;
					for (int k = 0; k < topPDs.size(); k++)
					{
						topPD = (PatternDifference) topPDs.get(k);
						DimensionColumn dc = r.findDimensionColumn(topPD.p.Dimension1, topPD.p.Attribute1);
						if (topPD.performanceCategory.equals(pd.performanceCategory) && !containsBlockingGroup(pdList, topPD.p.Measure, dc.BlockingGroup))
							break;
						else
							topPD = null;
					}
					/*
					 * If this pattern isn't useful and there's another in the category that is, pull it off the top of
					 * the list and use it
					 */
					if (topPD != null)
					{
						if ((pd.difference < MIN_DIFFERENCE) && (topPD.difference > pd.difference) && (topPD.difference > MIN_DIFFERENCE))
						{
							if (measureCount > MIN_EXACT_MATCHES)
							{
								topPD.measureCat = pd.measureCat;
								topPDs.add(pd);
								pd = topPD;
								topPDs.remove(topPD);
							}
						}
					}
					/*
					 * If this is a relatively meaningless difference, then add it to the list to be added at the end of
					 * the process (want to make sure that the top ones are applied to the appropriate categories first,
					 * then at the end, take whatever is left and apply them to replacements
					 */
					if ((pd.difference < MIN_DIFFERENCE) && REPLACE_OUTSIDE_CATEGORY)
					{
						replaceList.add(pd);
						numSaved--;
					}
				}
				/*
				 * If a measure hasn't been assigned to this difference and each category is already full, then assign
				 * to Other
				 */
				if ((numCategoriesFull >= numMeasureCategoriesToAnalyze) && (pd.measureCat == null))
				{
					/*
					 * First, see if there are any patterns that need replacing. If so, pull them off of the top of the
					 * top list and replace the pattern
					 */
					while ((replaceList.size() > 0) && (topPDs.size() > 0))
					{
						/*
						 * If the current pattern is the one at the bottom of the list, then take it off the list
						 * because it will be substituted later
						 */
						if (replaceList.get(0) == pd)
						{
							replaceList.remove(0);
						} else
						{
							PatternDifference replacepd = (PatternDifference) topPDs.get(0);
							int index = 0;
							for (; index < pdList.size(); index++)
							{
								PatternDifference listpd = (PatternDifference) replaceList.get(0);
								if (pdList.get(index) == listpd)
								{
									replacepd.measureCat = listpd.measureCat;
									replacepd.measureCount = listpd.measureCount;
									pdList.set(index, replacepd);
									topPDs.remove(0);
									replaceList.remove(0);
									numSaved++;
									break;
								}
							}
						}
					}
					/*
					 * If there are still better differences on the list, add the current difference to the end of the
					 * list of top differences and get the top one from the list
					 */
					if (topPDs.size() > 0)
					{
						topPDs.add(pd);
						pd = (PatternDifference) topPDs.get(0);
						topPDs.remove(0);
					}
					pd.measureCat = "Other";
					measureCount = ++otherPatterns;
				}
				// If a measure has been picked, count it
				if (pd.measureCat != null)
				{
					pd.measureCount = measureCount;
					pdList.add(pd);
					numSaved++;
				} else
				{
					/*
					 * Could not assign this pattern, so save it
					 */
					topPDs.add(pd);
				}
			}
		}
		HashMap<String, Integer> pmap = new HashMap<String, Integer>();
		for (int i = 0; i < patternDifferences.size(); i++)
		{
			String category = ((PatternDifference) patternDifferences.get(i)).performanceCategory;
			Integer cnt = (Integer) pmap.get(category);
			if (cnt == null)
			{
				cnt = Integer.valueOf(1);
				pmap.put(category, cnt);
			} else
			{
				pmap.put(category, Integer.valueOf(cnt.intValue() + 1));
			}
		}
		HashMap<String, Integer> pdmap = new HashMap<String, Integer>();
		for (int i = 0; i < pdList.size(); i++)
		{
			String category = ((PatternDifference) pdList.get(i)).performanceCategory;
			Integer cnt = (Integer) pdmap.get(category);
			if (cnt == null)
			{
				cnt = Integer.valueOf(1);
				pdmap.put(category, cnt);
			} else
			{
				pdmap.put(category, Integer.valueOf(cnt.intValue() + 1));
			}
		}
		return (pdList);
	}

	/**
	 * Return whether the current list of patterns already contains a pattern for a given measure that is part of a
	 * given blocking group
	 * 
	 * @param list
	 * @param measure
	 * @param blockingGroup
	 * @return
	 */
	private boolean containsBlockingGroup(List<PatternDifference> list, String measure, String blockingGroup)
	{
		if ((blockingGroup == null) || (blockingGroup.length() == 0))
			return (false);
		for (PatternDifference pd : list)
		{
			if (pd.p.Type == Pattern.ONEATTRIBUTE)
			{
				DimensionColumn dc = r.findDimensionColumn(pd.p.Dimension1, pd.p.Attribute1);
				if ((dc.BlockingGroup != null) && (pd.measureCat != null) && (dc.BlockingGroup.length() > 0) && (pd.measureCat.length() > 0))
				{
					if (pd.mc.ColumnName.equals(measure) && dc.BlockingGroup.equals(blockingGroup))
					{
						return (true);
					}
				}
			}
		}
		return (false);
	}

	/**
	 * @return Returns the categoriesToAnalyze.
	 */
	public List<String> getCategoriesToAnalyze()
	{
		return categoriesToAnalyze;
	}

	/**
	 * @param categoriesToAnalyze
	 *            The categoriesToAnalyze to set.
	 */
	@SuppressWarnings("unchecked")
	public void setCategoriesToAnalyze(List<String> categoriesToAnalyze)
	{
		this.categoriesToAnalyze = new ArrayList<String>(categoriesToAnalyze);
	}

	/**
	 * @return Returns the measuresToAnalyze.
	 */
	public List<String> getMeasuresToAnalyze()
	{
		return measuresToAnalyze;
	}

	/**
	 * @param listOfMeasures
	 *            The measuresToAnalyze to set.
	 * @param addCategoriesIfNoMeasure
	 *            If a given measure doesn't exist in the success model, add it the measure's category to the categories
	 *            to analyze
	 */
	public void setMeasuresToAnalyze(List<String> listOfMeasures, boolean addCategoriesIfNoMeasure)
	{
		this.listOfMeasures = listOfMeasures;
		this.measuresToAnalyze = new ArrayList<String>();
		this.ignoredAnalysisMeasures = new ArrayList<String>();
		for (int i = 0; i < listOfMeasures.size(); i++)
		{
			MeasureColumn mc = r.findMeasureColumn((String) listOfMeasures.get(i));
			if (mc != null)
			{
				if ((mc.AnalysisMeasure != null) && (mc.AnalysisMeasure.length() > 0))
				{
					if (!listOfMeasures.contains(mc.AnalysisMeasure))
					{
						this.measuresToAnalyze.add(mc.AnalysisMeasure);
					} else
					{
						this.measuresToAnalyze.add(mc.ColumnName);
						this.ignoredAnalysisMeasures.add(mc.AnalysisMeasure);
					}
				} else
				{
					this.measuresToAnalyze.add(mc.ColumnName);
				}
			}
		}
		categoriesToAnalyze = null;
		if (addCategoriesIfNoMeasure)
		{
			for (int i = 0; i < this.measuresToAnalyze.size(); i++)
			{
				boolean found = false;
				for (int patternIndex = 0; patternIndex < smi.sm.Patterns.length; patternIndex++)
				{
					Pattern p = smi.sm.Patterns[patternIndex];
					if (p.Measure.equals(this.measuresToAnalyze.get(i)) && (p.NumCategories > 1))
					{
						found = true;
						break;
					}
				}
				if (!found)
				{
					MeasureColumn mc = r.findMeasureColumn((String) this.measuresToAnalyze.get(i));
					if (mc != null)
					{
						if (categoriesToAnalyze == null)
						{
							categoriesToAnalyze = new ArrayList<String>();
							namesToAssignToCategories = new ArrayList<String>();
						}
						this.measuresToAnalyze.remove(i--);
						namesToAssignToCategories.add(mc.ColumnName);
						categoriesToAnalyze.add(mc.PerformanceCategory);
					}
				}
			}
		}
	}

	/**
	 * Clones a copy of this structure
	 * 
	 * @see java.lang.Object#clone()
	 */
	public Object clone() throws CloneNotSupportedException
	{
		SuccessPatternAnalysis spc = (SuccessPatternAnalysis) super.clone();
		spc.r = this.r;
		spc.conn = this.conn;
		spc.dc = this.dc;
		spc.psd = this.psd;
		spc.smi = this.smi;
		spc.twoAttributes = this.twoAttributes;
		return (spc);
	}
}
