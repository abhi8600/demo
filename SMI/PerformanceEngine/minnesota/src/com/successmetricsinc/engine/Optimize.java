/**
 * $Id: Optimize.java,v 1.16 2011-02-24 00:30:31 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import org.apache.log4j.Logger;
import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.BitSet;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import weka.core.Instance;

/**
 * Class to choose a set of attributes to change that minimize an objective function and are feasible
 * 
 * @author bpeters
 * 
 */
public class Optimize implements Externalizable
{
	private static Logger logger = Logger.getLogger(Optimize.class);
	private static final long serialVersionUID = 1L;
	private String key;
	private Instance inst;
	private FeasibleSolution fs;
	private int[] indices;
	private int numToChange;
	private boolean validate;
	private List<Solution> population;
	private Map<BitSet,Solution> cache;
	private Random r;
	private double alterationRange;
	private static int populationSize = 125;
	private static int safePopulationSize = populationSize / 3;
	private static int maxGenerations = 100;
	private static double REPRODUCTION_RATE = .75;
	private static double MUTATION_RATE = 1.25;
	private static double MAX_SAME = 10;
	private static final int MAX_CROSSOVER_TRIES = 15;
	private static final int MAX_MUTATE_TRIES = 4000; // fail safe
	private static final int MAX_COLUMNS = 64;
	private Solution bestSolution;

	/**
	 * Deserialize object
	 * 
	 * @see java.io.Externalizable#readExternal(java.io.ObjectInput)
	 */
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException
	{
		inst = (Instance) in.readObject();
		bestSolution = (Solution) in.readObject();
	}

	/**
	 * Serialize the object
	 * 
	 * @see java.io.Externalizable#writeExternal(java.io.ObjectOutput)
	 */
	public void writeExternal(ObjectOutput out) throws IOException
	{
		out.writeObject(inst);
		out.writeObject(bestSolution);
	}

	/**
	 * Blank constructor for serialization
	 */
	public Optimize()
	{
	}

	/**
	 * Instantiates the optimizer - Uses a genetic algorithm to find the best set of indices to change
	 * 
	 * @param key
	 * 			  Target Key (for logging/debugging purposes)
	 * @param inst
	 *            Instance to optimize
	 * @param fs
	 *            Feasible solution function (true of feasible, false if not)
	 * @param of
	 *            Objective function to minimize
	 * @param indices
	 *            Allowable indices to change
	 * @param numToChange
	 *            Number of indices to change
	 * @param validate
	 *            Whether or not to validate a given instance when created (e.g. to make it locally optimal)
	 */
	public Optimize(String key, Instance inst, FeasibleSolution fs, int[] indices, int numToChange, boolean validate)
	{
		this.key = key;
		this.inst = inst;
		this.fs = fs;
		this.indices = indices;
		this.validate = validate;
		this.numToChange = numToChange;
		population = new ArrayList<Solution>();
		cache = new HashMap<BitSet,Solution>();
		r = new Random(System.currentTimeMillis());
		// Calculate range of values for number of mutations
		alterationRange = 0;
		for (int i = 0; i < indices.length; i++)
		{
			// Probability of a given number of mutations is proportional to the inverse power of 2 to the number of
			// mutations
			alterationRange += 1 / nPower(i + 1);
		}
	}

	/**
	 * Mutate a set of indices into a different set. Change one or more indices to a new, feasible option
	 * 
	 * @param currentIndices
	 *            Index list to start with
	 * @param allIndices
	 *            All possible indices that can be picked
	 * @return New index list (valid)
	 * @throws Exception
	 */
	private int[] mutate(int[] currentIndices, int[] allIndices) throws Exception
	{
		int[] newIndices = new int[currentIndices.length];
		System.arraycopy(currentIndices, 0, newIndices, 0, newIndices.length);
		boolean feasible = false;
		int tries = 0;
		do
		{
			// First, figure out how many indices to change. Bias towards smaller changes
			double muVal = r.nextDouble() * alterationRange;
			double muPos = 0;
			int numToMutate = 1;
			for (; numToMutate < newIndices.length; numToMutate++)
			{
				muPos += 1 / nPower(numToMutate);
				if (muPos >= muVal)
					break;
			}
			// Now, pick the values to change
			int numPicked = 0;
			int[] pickList = new int[newIndices.length];
			while (numPicked < numToMutate)
			{
				int index = r.nextInt(newIndices.length);
				boolean found = false;
				for (int i = 0; i < numPicked; i++)
				{
					if (pickList[i] == index)
					{
						found = true;
						break;
					}
				}
				if (!found)
				{
					pickList[numPicked++] = index;
				}
			}
			// Now, pick the new indices to change the picked ones into
			int numChanged = 0;
			while (numChanged < numToMutate)
			{
				// Get the index to change
				int index = pickList[numChanged];
				// Figure out which one of not currently picked indices to try
				int offset = r.nextInt(allIndices.length - newIndices.length);
				// Scan to find the appropriate offset into currently non-chosen indices
				int pos = 0;
				int count = 0;
				int indexTry;
				boolean current;
				do
				{
					current = false;
					indexTry = allIndices[pos++];
					for (int j = 0; j < newIndices.length; j++)
					{
						if (newIndices[j] == indexTry)
						{
							current = true;
							break;
						}
					}
					if (!current) // XXX conditional increment to termination condition
						count++;
				} while (count <= offset);
				newIndices[index] = indexTry;
				numChanged++;
			}
			tries++;
			feasible = fs.isFeasible(newIndices);
		} while (!feasible && (tries < MAX_MUTATE_TRIES));
		
		if (feasible)
		{
			return (newIndices);
		}
		else
		{
			logger.debug("mutate returned no feasible solution after " + MAX_MUTATE_TRIES + " iterations");
			return (null);
		}
	}

	/**
	 * Routine to return a probability spread number to a given integral power
	 * 
	 * @param i
	 *            Power to raise the spread number to
	 * @return Value of number raised to the indicated power
	 */
	private double nPower(int i)
	{
		double v = 1;
		for (int j = 1; j <= i; j++)
			v = v * 4;
		return (v);
	}

	/**
	 * Perform a crossover between two sets of instances and return an offspring. Both lists must be same size
	 * 
	 * @param list1
	 * @param list2
	 * @param indices
	 * @return
	 * @throws Exception
	 */
	private int[] crossOver(int[] list1, int[] list2) throws Exception
	{
		// First, figure out how many indices to crossover. Bias towards smaller changes
		double crVal = r.nextDouble() * alterationRange;
		double crPos = 0;
		int numToCrossover = 1;
		for (; numToCrossover < list1.length; numToCrossover++)
		{
			crPos += 1 / nPower(numToCrossover);
			if (crPos > crVal)
				break;
		}
		// Now, pick the values to change
		int numPicked = 0;
		int tries = 0;
		int[] pickList = new int[list1.length];
		boolean feasible = false;
		while (numPicked < numToCrossover)
		{
			int index = r.nextInt(list1.length);
			boolean found = false;
			for (int i = 0; i < numPicked; i++)
			{
				if (pickList[i] == index)
				{
					found = true;
					break;
				}
			}
			if (!found)
			{
				pickList[numPicked++] = index;
			}
		}
		int[] newList = new int[list1.length];
		do
		{
			// Pick dominant parent
			int parent = r.nextInt(2);
			for (int i = 0; i < list1.length; i++)
			{
				if (parent == 0)
				{
					newList[i] = list1[i];
				} else
				{
					newList[i] = list2[i];
				}
			}
			// Crossover picked ones - only if doesn't exist in parent already (do dupes)
			for (int i = 0; i < numPicked; i++)
			{
				int index = pickList[i];
				if (parent == 0)
				{
					boolean found = false;
					for (int j = 0; j < newList.length; j++)
					{
						if (newList[j] == list2[index])
						{
							found = true;
							break;
						}
					}
					if (!found)
						newList[index] = list2[index];
				} else
				{
					boolean found = false;
					for (int j = 0; j < newList.length; j++)
					{
						if (newList[j] == list1[index])
						{
							found = true;
							break;
						}
					}
					if (!found)
						newList[index] = list1[index];
				}
			}
			tries++;
			feasible = fs.isFeasible(newList);
		} while (!feasible && (tries < MAX_CROSSOVER_TRIES));
		if (feasible)
		{
			return (newList);
		} else
		{
			return (null);
		}
	}

	/**
	 * Get an index into the population biased based on health
	 * 
	 * @return Index
	 */
	private int getBiasedIndex(boolean includeSafe)
	{
		boolean done = false;
		int index = 0;
		int offset = includeSafe ? 0 : safePopulationSize;
		do
		{
			double val1 = r.nextDouble();
			double val2 = r.nextDouble();
			index = (int) (val1 * ((double) (population.size() - offset)));
			if (val2 <= val1)
			{
				done = true;
			}
		} while (!done);
		return (index + offset);
	}
	
	/**
	 * Find the optimized solution
	 * 
	 * @return Objective function value of solution
	 * @throws Exception
	 */
	public double findSolution() throws Exception
	{
		// Create initial population
		int[] curList = new int[numToChange];
		for (int i = 0; i < numToChange; i++)
			curList[i] = (i + 1);
		for (int i = 0; i < populationSize; i++)
		{
			curList = mutate(curList, indices);
			if (curList == null)
			{
				break;
			}
			Solution s = new Solution(curList);
			population.add(s);
		}
		
		// if population has no members, skip
		if (population.isEmpty())
		{
			logger.debug("Population size is 0, skipping");
			return Double.NaN;
		}
		
		// Cycle through generations
		int generation = 0;
		double curObjective = 0;
		int countSame = 0;
		int numToCrossover = (int) (population.size() * REPRODUCTION_RATE);
		int numToMutate = (int) (population.size() * MUTATION_RATE);
		do
		{
			// Generate offspring
			for (int i = 0; i < numToCrossover; i++)
			{
				// Pick an instance
				int index = getBiasedIndex(true);// r.nextInt(POPULATION_SIZE);
				// Pick a mate
				int mateIndex = r.nextInt(populationSize);
				// Generate an offspring
				int[] newList = crossOver(((Solution) population.get(index)).indices, ((Solution) population.get(mateIndex)).indices);
				if (newList != null)
				{
					Solution s = new Solution(newList);
					population.add(s);
				}
			}
			// Mutate some
			int sz = population.size(); // deal with weird mutate break above (normally would be POPULATION_SIZE)
			for (int i = 0; i < numToMutate; i++)
			{
				// Pick an instance
				int index = r.nextInt(sz);
				// Mutate
				int[] newList = mutate(((Solution) population.get(index)).indices, indices);
				if (newList != null)
				{
					Solution s = new Solution(newList);
					population.add(s);
				}
			}
			// Sort by fitness
			Collections.sort(population);
			// Kill off least fit
			if (generation < maxGenerations)
			{
				// Kill off randomly, but bias towards unhealthy
				while (population.size() > populationSize)
				{
					int index = getBiasedIndex(false);
					population.remove(index);
				}
			}
			
			// if population has no members, skip
			if (population.isEmpty())
			{
				logger.debug("Population size is 0, skipping");
				return Double.NaN;
			}
			
			// Save best solution
			bestSolution = (Solution) population.get(0);
			if (logger.isDebugEnabled())
			{
				Solution worstSolution = (Solution) population.get(population.size() - 1);
				logger.debug(key + " - " + generation + ": Best Solution: " + bestSolution.objective + ", Worst: "
						+ worstSolution.objective);
			}
			// Calculate improvement rate
			if (curObjective < bestSolution.objective)
			{
				countSame = 0;
			}
			curObjective = bestSolution.objective;
		} while ((generation++ < maxGenerations) && (countSame++ < MAX_SAME));
		// Clear population and cache
		population.clear();
		population = null;
		cache.clear();
		cache = null;
		return (bestSolution.objective);
	}

	/**
	 * Returns the instance for the solution found
	 * 
	 * @return The instance of the solution
	 */
	public Instance getSolutionInstance()
	{
		return (bestSolution.inst);
	}

	/**
	 * Returns the instance for the solution found
	 * 
	 * @return The instance of the solution
	 */
	public Instance getOriginalInstance()
	{
		return (inst);
	}

	/**
	 * Return a list of the indices changed during the optimization to find the optimal solution
	 * 
	 * @return List of changed indices
	 */
	public int[] getChangedIndices()
	{
		return (bestSolution.indices);
	}

	/**
	 * Class to hold an individual solution to the optimization problem
	 * 
	 * @author bpeters
	 */
	private class Solution implements Comparable<Solution>, Serializable
	{
		private static final long serialVersionUID = 1L;
		double objective;
		int[] indices;
		Instance inst;

		public Solution(int[] indices) throws Exception
		{
			this.indices = indices;
			BitSet bs = new BitSet(MAX_COLUMNS);
			for (int i = 0; i < indices.length; i++)
			{
				bs.set(indices[i]);
			}
			Solution exist = (Solution) cache.get(bs);
			if (exist != null)
			{
				this.inst = exist.inst;
				this.objective = exist.objective;
				return;
			}
			this.inst = (Instance) Optimize.this.inst.copy();
			// Make instance valid if necessary
			if (validate)
			{
				this.objective = fs.makeValidInstance(this.inst, indices);
			}
			cache.put(bs, this);
		}

		public int compareTo(Solution s)
		{
			return (-(new Double(objective).compareTo(new Double(s.objective))));
		}

		public String toString()
		{
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < indices.length; i++)
			{
				if (i > 0)
					sb.append(',');
				sb.append(indices[i]);
			}
			sb.append(": ");
			sb.append(objective);
			return sb.toString();
		}
	}

	/**
	 * @param max_generations
	 *            The mAX_GENERATIONS to set.
	 */
	public static void setMAX_GENERATIONS(int max_generations)
	{
		maxGenerations = max_generations;
	}

	/**
	 * @param max_same
	 *            The mAX_SAME to set.
	 */
	public static void setMAX_SAME(double max_same)
	{
		MAX_SAME = max_same;
	}

	/**
	 * @param mutation_rate
	 *            The mUTATION_RATE to set.
	 */
	public static void setMUTATION_RATE(double mutation_rate)
	{
		MUTATION_RATE = mutation_rate;
	}

	/**
	 * @param population_size
	 *            The pOPULATION_SIZE to set.
	 */
	public static void setPOPULATION_SIZE(int population_size)
	{
		populationSize = population_size;
		safePopulationSize = populationSize / 3;
	}

	/**
	 * @param reproduction_rate
	 *            The rEPRODUCTION_RATE to set.
	 */
	public static void setREPRODUCTION_RATE(double reproduction_rate)
	{
		REPRODUCTION_RATE = reproduction_rate;
	}
}
