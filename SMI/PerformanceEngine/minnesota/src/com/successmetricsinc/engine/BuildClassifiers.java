/**
 * $Id: BuildClassifiers.java,v 1.46 2010-08-30 00:32:08 bpeters Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.log4j.Logger;
import weka.core.Instances;
import com.successmetricsinc.query.DisplayFilter;
import com.successmetricsinc.util.WekaUtil;

/**
 * Build the classifiers for a given Success Model Instance
 * 
 * @author bpeters
 * 
 */
public class BuildClassifiers
{
	private static Logger logger = Logger.getLogger(BuildClassifiers.class);
	// private SuccessModelInstance smi;
	private static double FLATTEN_PCT = 0.05;
	private boolean biasToUniformClass = false;
	private boolean includeAllNonzero = false;
	private OutcomeFactor[] factors;
	private List<Object> iterationValues;
	private SuccessModelInstance smi;
	private ModelParameters mp;
	private Outcome outcome;
	private Map<Object, Segment> segmentMap;
	private String modelName;
	ClassifierSettings classifiers;
	private static Iterator<Entry<Object, Segment>> segmentIterator;
	
	public BuildClassifiers(SuccessModelInstance smi, Outcome outcome, ModelParameters mp, Map<Object, Segment> segmentMap)
	{
		this.smi = smi;
		this.segmentMap = segmentMap;
		this.outcome = outcome;
		modelName = outcome.Name;
		this.mp = mp;
		classifiers = outcome.Classifiers == null ? mp.Classifiers : outcome.Classifiers;
	}

	private synchronized Segment getNextSegment()
	{
		if (segmentIterator.hasNext())
			return (segmentIterator.next().getValue());
		else
			return (null);
	}

	/**
	 * Method to build classifiers
	 * 
	 * @param buildValue
	 *            Build a value classifier (use a numeric target)
	 * @param buildClass
	 *            Build a class classifier (use a nominal class attribute as target)
	 * @param buildValueForClass
	 *            Build a classifier to predict the numeric value if an instance is a member of the class (i.e. filter
	 *            out non-class members when training)
	 * @param classMeasureName
	 *            Measure to use to determine class membership (should be 1 (in class) or 0 (not in class))
	 */
	public void buildClassifiers(boolean buildValue, boolean buildClass, boolean buildValueForClass, String valueMeasureName, String classMeasureName)
	{
		segmentIterator = segmentMap.entrySet().iterator();
		Thread[] tlist = new Thread[mp.MaxComputationThreads];
		try
		{
			for (int i = 0; i < mp.MaxComputationThreads; i++)
			{
				tlist[i] = new BuildClassifierThread(smi, buildValue, buildClass, buildValueForClass, valueMeasureName, classMeasureName, outcome.TrainingFilter);
				tlist[i].start();
			}
		} catch (Exception e)
		{
			logger.error(e.getMessage(), e);
		}
		try
		{
			for (int i = 0; i < mp.MaxComputationThreads; i++)
			{
				tlist[i].join();
			}
		} catch (InterruptedException e)
		{
			logger.error(e.toString(), e);
		}
		logger.info("Classifiers Built");
	}

	private class BuildClassifierThread extends Thread
	{
		SuccessModelInstance smi;
		boolean buildValue;
		boolean buildClass;
		boolean buildValueForClass;
		String valueMeasureName;
		String classMeasureName;
		DisplayFilter trainingFilter;

		public BuildClassifierThread(SuccessModelInstance smi, boolean buildValue, boolean buildClass, boolean buildValueForClass, String valueMeasureName,
				String classMeasureName, DisplayFilter trainingFilter)
		{
			this.setName("Build Classifier - " + this.getId());
			this.smi = smi;
			this.buildValue = buildValue;
			this.buildClass = buildClass;
			this.buildValueForClass = buildValueForClass;
			this.valueMeasureName = valueMeasureName;
			this.classMeasureName = classMeasureName;
			this.trainingFilter = trainingFilter;
		}

		public void run()
		{
			Segment s = getNextSegment();
			if (s == null)
				return;
			logger.info(modelName + ": Building Classifiers for Segment: " + s.getKey());
			Instances valueDataSet = null;
			Instances classDataSet = null;
			Instances valueForClassDataSet = null;
			BestFitClassifier valuebfc = null;
			BestFitClassifier classbfc = null;
			BestFitClassifier valueforclassbfc = null;
			int numClassifierThreads = (segmentMap.size() < 2) ? mp.MaxComputationThreads : 1;
			try
			{
				if (buildClass)
				{
					/*
					 * For class prediction, filter out any columns that are exceptionally correlated with the class
					 * value (data quality). This duplicates the same functionality that's used in the attribute
					 * selection for the target measure, but was not applied to class value.
					 */
					setSegmentClassSelectedColumns(s, classMeasureName, smi, iterationValues, mp, outcome);
					// Now, generate instances for evaluating the models
					classDataSet = smi.constructInstances(s.getKey().toString(), s.getRowIndexList(), s.getSelectedClassColumnIndexList(), false, true,
							classMeasureName, false, s.getColumnHeaders(), iterationValues, trainingFilter);
					if (logger.isDebugEnabled())
						WekaUtil.writeArff(classDataSet, smi.instanceDir + File.separator + modelName + s.getKey().toString() + "_CLASS_RAW", null,
								smi.r.getModelParameters().MaxARFFInstances);
					if (classDataSet.numInstances() == 0)
					{
						logger.error("Unable to build classifier model - no instances were selected");
					} else
					{
						classDataSet.setClassIndex(0);
						classbfc = new BestFitClassifier(modelName + ":Class", classDataSet, true, true, false, true, numClassifierThreads,
								mp, classifiers);
						classbfc.setFlattenPercent(FLATTEN_PCT);
						classbfc.setWriteArff(true, smi.instanceDir + File.separator + modelName + s.getKey().toString() + "_CLASS");
						classbfc.setNonZeroFalsePositiveRate(true);
						classbfc.setBiasToUniformClass(biasToUniformClass);
						BestFitResult classbfr = classbfc.findBestFit();
						logger.info(modelName + s.getKey().toString() + " - " + classbfr.getErrorString());
						System.out.println(modelName + " - " + classbfr.getErrorString());
						s.setSegmentClassClassifier(classbfr);
					}
					s.setSmi(smi);
				}
				if (buildValue)
				{
					valueDataSet = smi.constructInstances(s.getKey().toString(), s.getRowIndexList(), s.getSelectedColumnIndexList(), false, false,
							valueMeasureName, false, s.getColumnHeaders(), iterationValues, trainingFilter);
					if (logger.isDebugEnabled())
						WekaUtil.writeArff(valueDataSet, smi.instanceDir + File.separator + modelName + s.getKey().toString() + "_VALUE_RAW", null,
								smi.r.getModelParameters().MaxARFFInstances);
					if (valueDataSet.numInstances() == 0)
					{
						logger.error("Unable to build value model - no instances were selected");
					} else
					{
						valueDataSet.setClassIndex(0);
						valuebfc = new BestFitClassifier(modelName + ":Value", valueDataSet, true, true, false, true, numClassifierThreads,
								mp, classifiers);
						valuebfc.setFlattenPercent(FLATTEN_PCT);
						valuebfc.setWriteArff(true, smi.instanceDir + File.separator + modelName + s.getKey().toString() + "_VALUE");
						valuebfc.setIncludeAllNonzero(includeAllNonzero);
						if (factors != null)
						{
							valuebfc.setFactors(factors);
						}
						BestFitResult valuebfr = valuebfc.findBestFit();
						logger.info(modelName + s.getKey().toString() + " - " + valuebfr.getErrorString());
						System.out.println(modelName + " - " + valuebfr.getErrorString());
						s.setSegmentValueClassifier(valuebfr);
					}
					s.setSmi(smi);
				}
				if (buildValueForClass)
				{
					valueForClassDataSet = smi.constructInstances(s.getKey().toString(), s.getRowIndexList(), s.getSelectedColumnIndexList(), false, false,
							valueMeasureName, true, s.getColumnHeaders(), iterationValues, trainingFilter);
					if (logger.isDebugEnabled())
						WekaUtil.writeArff(valueForClassDataSet, smi.instanceDir + File.separator + modelName + s.getKey().toString() + "_V4C_RAW", null,
								smi.r.getModelParameters().MaxARFFInstances);
					if (valueForClassDataSet.numInstances() == 0)
					{
						logger.error("Unable to build value for class model - no instances were selected");
					} else
					{
						valueForClassDataSet.setClassIndex(0);
						valueforclassbfc = new BestFitClassifier(modelName + ":VFC", valueForClassDataSet, true, true, false, true, numClassifierThreads,
								mp, classifiers);
						valueforclassbfc.setFlattenPercent(FLATTEN_PCT);
						valueforclassbfc.setWriteArff(true, smi.instanceDir + File.separator + modelName + s.getKey().toString() + "_V4C");
						BestFitResult valueforclassbfr = valueforclassbfc.findBestFit();
						logger.info(modelName + s.getKey().toString() + " - " + valueforclassbfr.getErrorString());
						System.out.println(modelName + " - " + valueforclassbfr.getErrorString());
						s.setSegmentValueForClassClassifier(valueforclassbfr);
					}
					s.setSmi(smi);
				}
			} catch (Exception e)
			{
				logger.error(modelName + s.getKey().toString() + "Classification Instance Exception: " + e.toString(), e);
			}
		}
	}

	public static void setSegmentClassSelectedColumns(Segment seg, String classMeasure, SuccessModelInstance smi, List<Object> iterationValues,
			ModelParameters params, Outcome outcome) throws Exception
	{
		// data set for column filtering
		Instances sampleDataSet = null;
		/*
		 * If a sample size is selectuse it. Otherwise work from the top of the list (first partition) and limit the
		 * results
		 */
		seg.smi = smi;
		logger.debug("Filtering attributes for target class measure: " + classMeasure);
		sampleDataSet = AttributeSearch.getClassFilterSample(classMeasure, seg, seg.getSelectedColumnIndexList(), params, iterationValues);
		FilterResults fr = new FilterResults();
		int classIndex = smi.getIndexForMeasure(classMeasure);
		if (classIndex < 0)
		{
			throw new Exception("Unable to find measure '" + classMeasure + "'");
		}
		AttributeSearch.filterColumnList(smi, classIndex, seg.getSelectedColumnIndexList(), seg.getRowIndexList(), true,
				sampleDataSet, fr, outcome != null ? outcome.NameFilter: null);
		List<Integer> list = seg.getSelectedClassColumnIndexList();
		if (list != null)
		{
			list.clear();
			seg.getSelectedClassColumnIndexList().addAll(fr.columnList);
		} else
			seg.setSelectedClassColumnIndexList(fr.columnList);
	}

	/**
	 * @param biasToUniformClass
	 *            The biasToUniformClass to set.
	 */
	public void setBiasToUniformClass(boolean biasToUniformClass)
	{
		this.biasToUniformClass = biasToUniformClass;
	}

	/**
	 * @param includeAllNonzero
	 *            The includeAllNonzero to set.
	 */
	public void setIncludeAllNonzero(boolean includeAllNonzero)
	{
		this.includeAllNonzero = includeAllNonzero;
	}

	/**
	 * @param factors
	 *            the factors to set
	 */
	public void setFactors(OutcomeFactor[] factors)
	{
		this.factors = factors;
	}

	/**
	 * @return Returns the iterationValues.
	 */
	public List<Object> getIterationValues()
	{
		return iterationValues;
	}

	/**
	 * @param iterationValues
	 *            The iterationValues to set.
	 */
	public void setIterationValues(List<Object> iterationValues)
	{
		this.iterationValues = iterationValues;
	}
}
