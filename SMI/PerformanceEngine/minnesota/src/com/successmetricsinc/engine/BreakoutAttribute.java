/**
 * $Id: BreakoutAttribute.java,v 1.6 2007-09-10 17:08:32 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.io.Serializable;
import java.util.List;

/**
 * Class to contain information on a given set of breakout attributes
 * 
 * @author bpeters
 * 
 */
public class BreakoutAttribute implements Serializable
{
	private static final long serialVersionUID = 1L;
	String dimension1;
	String attribute1;
	String dimension2;
	String attribute2;
	String weightMeasure;
	ValueSet[][] sets;
	Object[] keys;
	Object[] iterations;
	// Temporary structures
	List<List<ValueSet>> setList;
	List<Object> keyList;
	List<Object> iterationList;

	public static class ValueSet implements Serializable
	{
		private static final long serialVersionUID = 1L;
		String attribute1Value;
		String attribute2Value;
		Object iterationValue;
		double value;
	}

	private boolean valEq(String a, String b)
	{
		if (a == b)
			return true;		
		if (a == null || b == null)
			return false;
		return (a.equals(b));
	}

	/**
	 * Find the index of a given key/iteration combination
	 * 
	 * @param key
	 * @param iteration
	 * @return
	 */
	private int getKeyIndex(Object key, Object iteration)
	{
		int index = 0;
		for (; index < keys.length; index++)
		{
			if (keys[index].equals(key) && (iteration == null || iterations[index].equals(iteration)))
				break;
		}
		if (index < keys.length)
			return (index);
		return (-1);
	}

	/**
	 * Return the weight associated with a set of attribute values. Weights are normalized to total to 1 (percentage)
	 * 
	 * @param attribute1value
	 * @param attribute2value
	 * @return Weight assigned to a given set of attribute values (calculated using the proportion of the breakout
	 *         measure in that set)
	 */
	public double getAttributeSetWeight(Object key, Object iteration, String attribute1value, String attribute2value)
	{
		int index = getKeyIndex(key, iteration);
		if (index >= 0)
		{
			ValueSet[] curSets = sets[index];
			if (curSets != null)
			{
				double val = 0;
				double total = 0;
				for (int i = 0; i < curSets.length; i++)
				{
					if (valEq(curSets[i].attribute1Value, attribute1value) && valEq(curSets[i].attribute2Value, attribute2value))
					{
						val = curSets[i].value;
					}
					total += curSets[i].value;
				}
				if (total != 0)
					return (val / total);
			}
		}
		return (0);
	}

	/**
	 * Commit temporary filling structures into runtime ones (easier to store/more compact)
	 */
	public void commit()
	{
		keys = new Object[keyList.size()];
		iterations = new Object[iterationList.size()];
		sets = new ValueSet[setList.size()][];
		keyList.toArray(keys);
		iterationList.toArray(iterations);
		keyList = null;
		iterationList = null;
		for (int i = 0; i < setList.size(); i++)
		{
			List<ValueSet> list = setList.get(i);
			sets[i] = new ValueSet[list.size()];
			list.toArray(sets[i]);
		}
		setList = null;
	}
}
