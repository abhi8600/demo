/**
 * $Id: PerformanceModel.java,v 1.15 2011-09-24 01:03:33 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.List;
import org.jdom.Element;
import org.jdom.Namespace;
import com.successmetricsinc.Repository;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XmlUtils;

public class PerformanceModel implements Serializable
{
	private static final long serialVersionUID = 1L;
	public static final int TYPE_IMPROVEMENT = 0;
	public static final int TYPE_PROBABILITY = 1;
	public static final String DEFAULT_NAME = "Default Performance Model";
	public String Name;
	public String ReferencePopulationName;
	public String OptimizationGoal;
	public String ModelFileName;
	public String[] PerformanceFilters;
	public String PerformanceOutputTable;
	public boolean DropExistingTable;
	public double PerformanceImpactPercent = 5;
	public boolean PerformanceLogRelationship = false;
	public PerformanceMeasure[] Measures;
	public String PerformanceIterationDimension;
	public String PerformanceIterationDefaultAttribute;
	public String PerformanceIterationAnalysisValue;
	public String PerformanceIterationModelValue;
	public int NumMeasuresToOptimize;
	public int TargetType;
	public double TargetImprovement;
	public double MinTargetImprovement;
	public double TargetProbability;
	public double TargetMinimum;
	public double NumIterations;
	public double TargetAccuracy;
	public double DampeningFactor;
	public boolean Optimize;
	public boolean SearchPatterns;
	public String SearchPatternDefinition;
	public ClassifierSettings Classifiers;
	private String referencePopulation;
	private String modelFile;

	public PerformanceModel(Element e, Namespace ns)
	{
		Name = e.getChildText("Name", ns);
		ReferencePopulationName = e.getChildText("ReferencePopulation", ns);
		referencePopulation = e.getChildText("ReferencePopulation", ns);
		ModelFileName = e.getChildText("ModelFile", ns);
		modelFile = e.getChildText("ModelFile", ns);
		OptimizationGoal = e.getChildText("OptimizationGoal", ns);
		PerformanceFilters = Repository.getStringArrayChildren(e, "PerformanceFilters", ns);
		PerformanceOutputTable = e.getChildText("PerformanceOutputTable", ns);
		DropExistingTable = Boolean.parseBoolean(e.getChildTextTrim("DropExistingTable", ns));
		PerformanceImpactPercent = Double.parseDouble(e.getChildText("PerformanceImpactPercent", ns));
		PerformanceIterationDimension = e.getChildText("PerformanceIterationDimension", ns);
		PerformanceIterationDefaultAttribute = e.getChildText("PerformanceIterationDefaultAttribute", ns);
		PerformanceIterationAnalysisValue = e.getChildText("PerformanceIterationAnalysisValue", ns);
		PerformanceIterationModelValue = e.getChildText("PerformanceIterationModelValue", ns);
		PerformanceLogRelationship = Boolean.parseBoolean(e.getChildText("PerformanceLogRelationship", ns));
		@SuppressWarnings("rawtypes")
		List l2 = e.getChild("Measures", ns).getChildren();
		Measures = new PerformanceMeasure[l2.size()];
		for (int count2 = 0; count2 < l2.size(); count2++)
		{
			Element pme = (Element) l2.get(count2);
			Measures[count2] = new PerformanceMeasure(pme, ns);
		}
		NumMeasuresToOptimize = Integer.parseInt(e.getChildText("NumMeasuresToOptimize", ns));
		TargetType = Integer.parseInt(e.getChildText("TargetType", ns));
		TargetImprovement = Double.parseDouble(e.getChildText("TargetImprovement", ns));
		TargetProbability = Double.parseDouble(e.getChildText("TargetProbability", ns));
		TargetMinimum = Double.parseDouble(e.getChildText("TargetMinimum", ns));
		MinTargetImprovement = Double.parseDouble(e.getChildText("MinTargetImprovement", ns));
		NumIterations = Double.parseDouble(e.getChildText("NumIterations", ns));
		TargetAccuracy = Double.parseDouble(e.getChildText("TargetAccuracy", ns));
		DampeningFactor = Double.parseDouble(e.getChildText("DampeningFactor", ns));
		Optimize = Boolean.parseBoolean(e.getChildTextTrim("Optimize", ns));
		SearchPatterns = Boolean.parseBoolean(e.getChildTextTrim("SearchPatterns", ns));
		SearchPatternDefinition = e.getChildTextTrim("SearchPatternDefinition", ns);
		Element clSettings = e.getChild("Classifiers", ns);
		if (clSettings != null)
		{
			Classifiers = new ClassifierSettings();
			Classifiers = new ClassifierSettings();
			Classifiers.RegressionModels = Repository.getIntArrayChildren(clSettings, "RegressionModels", ns);
			Classifiers.ClassificationModels = Repository.getIntArrayChildren(clSettings, "ClassificationModels", ns);
			Classifiers.MaxSampleInstanceValues = Integer.parseInt(clSettings.getChildText("MaxSampleInstanceValues", ns));
			Classifiers.MaxInstanceValues = Integer.parseInt(clSettings.getChildText("MaxInstanceValues", ns));
		}
		
		// backwards compat
		if (Name == null || Name.length() == 0)
		{
			Name = DEFAULT_NAME;
		}
		if (ReferencePopulationName == null || ReferencePopulationName.length() == 0)
		{
			ReferencePopulationName = ReferencePopulation.DEFAULT_NAME;
		}
		if (ModelFileName == null || ModelFileName.length() == 0)
		{
			ModelFileName = "PERF.MOD";
		}
	}
	
	public Element getPerformanceModelElement(Namespace ns)
	{		
		Element e = new Element("PerformanceModel",ns);
	
		XmlUtils.addContent(e, "Name", this.Name, ns);
		XmlUtils.addContent(e, "ModelFile", this.modelFile, ns);
		XmlUtils.addContent(e, "ReferencePopulation", this.referencePopulation, ns);
		XmlUtils.addContent(e, "OptimizationGoal", this.OptimizationGoal, ns);
		
		if (this.PerformanceFilters!=null && this.PerformanceFilters.length>0)
		{
			e.addContent(Repository.getStringArrayElement("PerformanceFilters", PerformanceFilters, ns));
		}
		else
		{
		  e.addContent(new Element("PerformanceFilters",ns));
		}
		
		XmlUtils.addContent(e, "PerformanceOutputTable", this.PerformanceOutputTable, ns);
		XmlUtils.addContent(e, "DropExistingTable", this.DropExistingTable, ns);
		XmlUtils.addContent(e, "PerformanceImpactPercent", Util.getDecimalFormatForRepository().format(this.PerformanceImpactPercent), ns);
		XmlUtils.addContent(e, "PerformanceLogRelationship", this.PerformanceLogRelationship, ns);
		
		Element child = new Element("Measures",ns);
		if (this.Measures!=null && this.Measures.length>0)
		{
			for (PerformanceMeasure perfMeasure : Measures)
			{
				child.addContent(perfMeasure.getPerformanceMeasureElement(ns));
			}
		}
		e.addContent(child);
		
		XmlUtils.addContent(e, "PerformanceIterationDimension", this.PerformanceIterationDimension, ns);
		XmlUtils.addContent(e, "PerformanceIterationDefaultAttribute", this.PerformanceIterationDefaultAttribute, ns);
		XmlUtils.addContent(e, "PerformanceIterationModelValue", this.PerformanceIterationModelValue, ns);
		XmlUtils.addContent(e, "PerformanceIterationAnalysisValue", this.PerformanceIterationAnalysisValue, ns);
		XmlUtils.addContent(e, "NumMeasuresToOptimize", this.NumMeasuresToOptimize, ns);
		XmlUtils.addContent(e, "TargetType", this.TargetType, ns);
		XmlUtils.addContent(e, "TargetImprovement", Util.getDecimalFormatForRepository().format(this.TargetImprovement), ns);
		XmlUtils.addContent(e, "TargetProbability", Util.getDecimalFormatForRepository().format(this.TargetProbability), ns);
		XmlUtils.addContent(e, "TargetMinimum", Util.getDecimalFormatForRepository().format(this.TargetMinimum), ns);
		XmlUtils.addContent(e, "MinTargetImprovement", Util.getDecimalFormatForRepository().format(this.MinTargetImprovement), ns);
		XmlUtils.addContent(e, "NumIterations",Util.getDecimalFormatForRepository().format(this.NumIterations), ns);
		XmlUtils.addContent(e, "TargetAccuracy",Util.getDecimalFormatForRepository().format(this.TargetAccuracy), ns);
		XmlUtils.addContent(e, "DampeningFactor",Util.getDecimalFormatForRepository().format(this.DampeningFactor), ns);
		XmlUtils.addContent(e, "Optimize",this.Optimize, ns);
		XmlUtils.addContent(e, "SearchPatterns",this.SearchPatterns, ns);
		XmlUtils.addContent(e, "SearchPatternDefinition",this.SearchPatternDefinition, ns);
		
		if (Classifiers!=null)
		{
			e.addContent(Classifiers.getClassifierSettingsElement(ns));
		}
		
		return e;
	}
	
	public String getName()
	{
		return Name;
	}
}
