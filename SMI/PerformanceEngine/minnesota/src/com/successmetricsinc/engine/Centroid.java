/**
 * $Id: Centroid.java,v 1.2 2007-08-30 16:26:47 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.io.Serializable;

/**
 * Class to hold a clustering centroid
 * 
 * @author bpeters
 * 
 */
public class Centroid implements Serializable
{
    private static final long serialVersionUID = 1L;
    double Probability; // Prior probabilities of each cluster
    // first index is cluster, second is attribute
    double Means[];
    double StdDeviations[];
}
