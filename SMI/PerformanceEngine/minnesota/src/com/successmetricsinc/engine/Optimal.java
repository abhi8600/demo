/**
 * $Id: Optimal.java,v 1.11 2011-09-24 01:03:33 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import org.apache.log4j.Logger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import weka.classifiers.Classifier;
import weka.core.Instance;
import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.impl.DenseDoubleMatrix1D;
import cern.colt.matrix.linalg.Algebra;
import cern.colt.matrix.linalg.Blas;
import cern.colt.matrix.linalg.SeqBlas;
import com.successmetricsinc.Repository;

/**
 * Class to find an optimal instance given a model, set of peers and starting instance.
 * 
 * @author bpeters
 * 
 */
public class Optimal implements ObjectiveFunction
{
	private static Logger logger = Logger.getLogger(Optimal.class);
	protected static final double PROBABILITY_PENALTY_MULTIPLIER = 1E5;
	protected static final int MIN_ITERATIONS_FOR_SD = 4;
	protected Instance inst;
	protected double goal;
	protected double numIterations;
	protected Classifier c;
	protected AdjustedClassifier ac;
	protected DoubleMatrix2D covariance;
	protected DoubleMatrix2D invcovariance;
	protected Repository r;
	@SuppressWarnings("rawtypes")
	protected Map targetKeyMap;
	@SuppressWarnings("rawtypes")
	protected Map peerMap;
	protected double curPredVal;
	protected Performance perf;
	protected double det;
	protected double prob;

	/**
	 * @param inst
	 *            Current version of instance to optimize
	 * @param goal
	 *            Goal to seek (first attribute of instance)
	 * @param c
	 *            Classifier to use
	 * @param key
	 *            Target key
	 * @param r
	 *            Current repository
	 * @param iterateKeyList
	 *            List with iteration value of each dataset entry
	 * @param targetKeyMap
	 *            Target key map - each key is mapped to all instances of that key (each with a different iteration
	 *            value)
	 * @param peerMap
	 *            Peer map that contains a list of all peer keys for each key
	 * @param perf
	 *            Class of the performance data being used
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public Optimal(Instance inst, double goal, Classifier c, Object key, Repository r, List<String> iterateKeyList, Map targetKeyMap,
			Map peerMap, Performance perf) throws Exception
	{
		this.inst = inst;
		this.goal = goal;
		this.c = c;
		this.numIterations = Math.sqrt(perf.performance.NumIterations);
		this.r = r;
		this.targetKeyMap = targetKeyMap;
		this.peerMap = peerMap;
		this.perf = perf;
		// Create adjusted classifier (compensate for model error)
		ac = new AdjustedClassifier(c, inst);
		/*
		 * If using iterations, get average standard deviations of targets for each attribute, otherwise, use a local
		 * population to determine standard deviations
		 */
		HashSet<Object> keySet = new HashSet<Object>();
		if (iterateKeyList != null)
		{
			for (int i = 0; i < iterateKeyList.size(); i++)
			{
				keySet.add(iterateKeyList.get(i));
			}
		}
		/*
		 * If there are sufficient iterations to calculate individual standard deviations, use them to calculate
		 * relative standard deviations for each attribute, otherwise, use a local population (relative to target)
		 */
		if (keySet.size() >= MIN_ITERATIONS_FOR_SD)
		{
			covariance = perf.returnCovarianceMatrix(key, keySet.size());
		} else
		{
			covariance = perf.returnCovarianceMatrix(null, 0);
		}
		/*
		 * First, scale the covariance matrix by the number of iterations being considered (n instances with variance a
		 * added create a new instance with covarance sqrt(a))
		 */
		Blas b = SeqBlas.seqBlas;
		b.dscal(Math.sqrt(perf.performance.NumIterations), covariance);
		// Now, invert the covariance matrix for later use in the probability distribution function
		Algebra a = new Algebra();
		try
		{
			invcovariance = a.inverse(covariance);
		} catch (Exception ex)
		{
			// Singular matrix, look for all zero column and add "noise" - get a list of all that apply
			Random ra = new Random();
			List<Integer> zeroColumns = new ArrayList<Integer>();
			for (int i = 0; i < covariance.columns(); i++)
			{
				boolean nonzero = false;
				for (int j = 0; j < covariance.rows(); j++)
				{
					if (covariance.get(j, i) != 0)
					{
						nonzero = true;
						break;
					}
				}
				if (!nonzero)
					zeroColumns.add(Integer.valueOf(i));
			}
			for (Integer zindex : zeroColumns)
			{
				int i = zindex.intValue();
				/*
				 * If there doesn't exist a column that is nonzero, then need to add "noise" to both row and column
				 * (since it's symmetric)
				 */
				for (int j = 0; j < covariance.rows(); j++)
				{
					double val = 0;
					while (val == 0)
						val = Math.abs(ra.nextGaussian() * (1E-7));
					covariance.set(j, i, val);
					covariance.set(i, j, val);
				}
			}
			try
			{
				invcovariance = a.inverse(covariance);
			} catch (Exception cex)
			{
				logger.error(ex.toString());
				logger.error(covariance.toString());
			}
		}
	}

	/**
	 * Get the probability of a given instance
	 */
	public double getProb(Instance newInst) throws Exception
	{
		/*
		 * Adjust current value by amount model predicts value should change (need this to take into account model
		 * error)
		 */
		curPredVal = ac.classifyInstance(newInst);
		DoubleMatrix1D vector = new DenseDoubleMatrix1D(inst.numAttributes());
		for (int i = 1; i < inst.numAttributes(); i++)
		{
			if (newInst.value(i) != inst.value(i))
			{
				double a = newInst.value(i);
				if (Double.isNaN(a))
					a = 0;
				double b = inst.value(i);
				if (Double.isNaN(b))
					b = 0;
				vector.set(i, a - b);
			}
		}
		/*
		 * Calculate a factor proportional to the multivariate probability density function. No need to add scaling
		 * factor because we just want to maximize the quantity (for performance reasons). Since multivariate
		 * probability distribution function = 1/sqrt((2*PI)^n*det(S)) * exp(-transpose(x)*inv(S)*x/2) where S is the
		 * coveriance matrix and x is the current instance vector. As such, the term exp(-transpose(x)*inv(S)*x/2) is
		 * scaled version of the probability distribution function
		 */
		Algebra a = new Algebra();
		DoubleMatrix1D v2 = a.mult(invcovariance, vector);
		double val = a.mult(vector, v2);
		val = Math.max(0, val);
		prob = Math.exp(-val / 2);
		return (prob);
	}

	public double getObjective(Instance inst) throws Exception
	{
		return 0;
	}
}
