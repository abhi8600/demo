/**
 * $Id: OutcomeGroup.java,v 1.20 2012-05-21 13:22:23 mpandit Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import org.apache.log4j.Logger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import com.successmetricsinc.Repository;
import com.successmetricsinc.query.Database;
import com.successmetricsinc.query.Query;

/**
 * Class to encapsulate several outcomes into a group to provide a comprehensive outcome analysis for a target.
 * 
 * @author bpeters
 * 
 */
public class OutcomeGroup
{
	private static Logger logger = Logger.getLogger(OutcomeGroup.class);
	Repository r;
	Connection conn;
	String name;

	public OutcomeGroup(Repository r, Connection conn, String name)
	{
		this.r = r;
		this.conn = conn;
		this.name = name;
	}

	public void generateOutcomeGroupTable()
	{
		try
		{
			// Get list of outcomes in group
			List<Outcome> outcomeList = new ArrayList<Outcome>();
			for (int i = 0; i < r.getOutcomes().length; i++)
			{
				if (r.getOutcomes()[i].OutcomeGroupName.equals(name))
					outcomeList.add(r.getOutcomes()[i]);
			}
			String tableName = ((Outcome) outcomeList.get(0)).OutcomeGroupTableName;
			boolean drop = ((Outcome) outcomeList.get(0)).DropExistingTable;
			Statement stmt = conn.createStatement();
			List<String> outputColumns = new ArrayList<String>();
			List<String> outputColumnTypes = new ArrayList<String>();
			// Create target table
			// Generate column list
			List<String> outcomeNameList = new ArrayList<String>();
			
			outputColumns.add(r.getResource("Outcome Target ID").getValue());
			outputColumnTypes.add("Varchar(30)");
			outputColumns.add(r.getResource("Outcome Iteration ID").getValue());
			outputColumnTypes.add("Varchar(30)");
			
			for (int i = 0; i < outcomeList.size(); i++)
			{
				Outcome o = (Outcome) outcomeList.get(i);
				String oname = o.Name.replace(' ', '_').replaceAll("-", "").replaceAll("__", "_");
				outcomeNameList.add(oname);
				outputColumns.add(oname + "_" + r.getResource("Outcome Actual Value").getValue());
				outputColumnTypes.add("Float");
				outputColumns.add(oname + "_" + r.getResource("Outcome Class Probability").getValue());
				outputColumnTypes.add("Float");
				outputColumns.add(oname + "_" + r.getResource("Outcome Predicted Value").getValue());
				outputColumnTypes.add("Float");
				outputColumns.add(oname + "_" + r.getResource("Outcome Predicted Bucket").getValue());
				outputColumnTypes.add("Varchar(30)");
				for (int j = 0; j < o.NumFactorsToRank; j++)
				{
					outputColumns.add(oname + "_" + r.getResource("Outcome Factor").getValue() + j);
					outputColumnTypes.add("Varchar(30)");
				}
				for (int j = 0; j < o.NumFactorsToRank; j++)
				{
					outputColumns.add(oname + "_" + r.getResource("Outcome Factor Value").getValue() + j);
					outputColumnTypes.add("Float");
				}
				for (int j = 0; j < o.NumFactorsToRank; j++)
				{
					outputColumns.add(oname + "_" + r.getResource("Outcome Factor Base Value").getValue() + j);
					outputColumnTypes.add("Float");
				}
				for (int j = 0; j < o.NumFactorsToRank; j++)
				{
					outputColumns.add(oname + "_" + r.getResource("Outcome Factor Test Value").getValue() + j);
					outputColumnTypes.add("Float");
				}
			}
			outputColumns.add("SUM_" + r.getResource("Outcome Actual Value").getValue());
			outputColumnTypes.add("Float");
			outputColumns.add("COUNT_" + r.getResource("Outcome Predicted Class").getValue());
			outputColumnTypes.add("Integer");
			outputColumns.add("SUM_" + r.getResource("Outcome Predicted Value").getValue());
			outputColumnTypes.add("Float");
			
			// get the iteration column and value for filtering
			String iterationValue = null;
			String iterationColumn = r.getResource("Outcome Iteration ID").getValue();
			for (int i = 0; i < outcomeList.size(); i++)
			{
				Outcome o = (Outcome) outcomeList.get(i);
				iterationValue = r.replaceVariables(null, o.IterationAnalysisValue);
				if (iterationValue != null && iterationValue.length() > 0)
				{
					break;
				}
			}
			Database.createTable(r, r.getDefaultConnection(), tableName, outputColumns, outputColumnTypes, drop, false, null, null, null);
			Map<String, String> valueByColumn = new HashMap<String, String>();
			valueByColumn.put(iterationColumn, iterationValue);
			Database.deleteOldData(r.getDefaultConnection(), tableName, valueByColumn);
			Database.createIndex(r.getDefaultConnection(), r.getResource("Outcome Target ID").getValue(), tableName);
			Database.createIndex(r.getDefaultConnection(), r.getResource("Outcome Iteration ID").getValue(), tableName);
			
			String whereClause = iterationColumn + "='" + iterationValue + "'";;
	
			// Insert IDs and Iteration Values into table
			HashSet<String> set = new HashSet<String>();
			for (int i = 0; i < outcomeList.size(); i++)
			{
				Outcome o = (Outcome) outcomeList.get(i);
				String iq = "SELECT " + r.getResource("Outcome Target ID").getValue() + " FROM " + o.OutputTableName + " WHERE " + whereClause;
				logger.debug(Query.getPrettyQuery(iq));
				ResultSet rs = stmt.executeQuery(iq);
				int cnt = 0;
				while (rs.next())
				{
					set.add(rs.getString(1));
					cnt++;
				}
				logger.info("Found " + cnt + " IDs for " + o.OutputTableName);
			}
			
			logger.info("Found " + set.size() + " unique IDs for this iteration");
			
			String insert = "INSERT INTO " + tableName + " (" + r.getResource("Outcome Target ID").getValue() + "," + r.getResource("Outcome Iteration ID").getValue() + ") VALUES (?,'" + iterationValue + "')";
			logger.debug(Query.getPrettyQuery(insert));
			PreparedStatement pstmt = conn.prepareStatement(insert);
			int cnt = 0;
			for (String st : set)
			{
				pstmt.setString(1, st);
				pstmt.addBatch();
				if (++cnt % 100 == 0)
					pstmt.executeBatch();
			}
			pstmt.executeBatch();
			
			// Update Values In Table
			for (int i = 0; i < outcomeList.size(); i++)
			{
				Outcome o = (Outcome) outcomeList.get(i);
				String oname = (String) outcomeNameList.get(i);
				
				StringBuilder iq = new StringBuilder("UPDATE " + tableName + " SET " + oname + "_" + r.getResource("Outcome Actual Value").getValue() + "=B."
						+ r.getResource("Outcome Actual Value").getValue() + "," + oname + "_" + r.getResource("Outcome Class Probability").getValue() + "=B."
						+ r.getResource("Outcome Class Probability").getValue() + "," + oname + "_" + r.getResource("Outcome Predicted Value").getValue() + "=B."
						+ r.getResource("Outcome Predicted Value").getValue() + "," + oname + "_" + r.getResource("Outcome Predicted Bucket").getValue() + "=B."
						+ r.getResource("Outcome Predicted Bucket").getValue() + " ");
				if (o.RankFactors)
				{
					for (int j = 0; j < o.NumFactorsToRank; j++)
					{
						iq.append("," + oname + "_" + r.getResource("Outcome Factor").getValue() + j + "=B." + r.getResource("Outcome Factor").getValue() + j);
					}
					for (int j = 0; j < o.NumFactorsToRank; j++)
					{
						iq
								.append("," + oname + "_" + r.getResource("Outcome Factor Value").getValue() + j + "=B."
										+ r.getResource("Outcome Factor Value").getValue() + j);
					}
					for (int j = 0; j < o.NumFactorsToRank; j++)
					{
						iq.append("," + oname + "_" + r.getResource("Outcome Factor Base Value").getValue() + j + "=B."
								+ r.getResource("Outcome Factor Base Value").getValue() + j);
					}
					for (int j = 0; j < o.NumFactorsToRank; j++)
					{
						iq.append("," + oname + "_" + r.getResource("Outcome Factor Test Value").getValue() + j + "=B."
								+ r.getResource("Outcome Factor Test Value").getValue() + j);
					}
				}
				iq.append("FROM " + tableName + " A," + o.OutputTableName + " B WHERE A." + r.getResource("Outcome Target ID").getValue() + "=B."
						+ r.getResource("Outcome Target ID").getValue() + " AND A." + r.getResource("Outcome Iteration ID").getValue() + "=B."
						+ r.getResource("Outcome Iteration ID").getValue());
				iq.append(" AND A." + whereClause);
				
				String iqstr = r.replaceVariables(null, iq.toString());
				logger.info(Query.getPrettyQuery(iqstr));
				stmt.executeUpdate(iqstr);
			}
			
			// Update totals in table
			StringBuilder query = new StringBuilder("UPDATE " + tableName + " SET SUM_" + r.getResource("Outcome Actual Value").getValue() + "=");
			for (int i = 0; i < outcomeList.size(); i++)
			{
				String oname = (String) outcomeNameList.get(i);
				if (i > 0)
					query.append('+');
				query.append("(CASE WHEN " + oname + "_" + r.getResource("Outcome Actual Value").getValue() + " IS NULL THEN 0 ELSE " + oname + "_"
						+ r.getResource("Outcome Actual Value").getValue() + " END)");
			}
			query.append(" FROM " + tableName);
			query.append(" WHERE " + whereClause);
			logger.info(Query.getPrettyQuery(query));
			stmt.executeUpdate(query.toString());
			
			query = new StringBuilder("UPDATE " + tableName + " SET SUM_" + r.getResource("Outcome Predicted Value").getValue() + "=");
			for (int i = 0; i < outcomeList.size(); i++)
			{
				String oname = (String) outcomeNameList.get(i);
				if (i > 0)
					query.append('+');
				query.append("(CASE WHEN " + oname + "_" + r.getResource("Outcome Predicted Value").getValue() + " IS NULL THEN 0 ELSE " + oname + "_"
						+ r.getResource("Outcome Predicted Value").getValue() + " END)");
			}
			query.append(" FROM " + tableName);
			query.append(" WHERE " + whereClause);
			logger.info(Query.getPrettyQuery(query));
			stmt.executeUpdate(query.toString());
			
			query = new StringBuilder("UPDATE " + tableName + " SET COUNT_" + r.getResource("Outcome Predicted Class").getValue() + "=");
			for (int i = 0; i < outcomeList.size(); i++)
			{
				String oname = (String) outcomeNameList.get(i);
				if (i > 0)
					query.append('+');
				query.append("(CASE WHEN " + oname + "_" + r.getResource("Outcome Class Probability").getValue() + " IS NULL THEN 0 WHEN " + oname + "_"
						+ r.getResource("Outcome Class Probability").getValue() + ">0.5 THEN 1 ELSE 0 END)");
			}
			query.append(" FROM " + tableName);
			query.append(" WHERE " + whereClause);
			logger.info(Query.getPrettyQuery(query));
			stmt.executeUpdate(query.toString());
			
		} catch (SQLException se)
		{
			logger.error(se.toString(), se);
		}
	}
}
