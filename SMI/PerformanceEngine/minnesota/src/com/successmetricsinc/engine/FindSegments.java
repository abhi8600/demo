/**
 * $Id: FindSegments.java,v 1.18 2010-10-27 17:11:44 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import weka.attributeSelection.Ranker;
import weka.attributeSelection.SVMAttributeEval;
import weka.clusterers.Clusterer;
import weka.clusterers.EM;
import weka.core.Instance;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;
import com.successmetricsinc.Repository;
import com.successmetricsinc.util.WekaUtil;

/**
 * Class to automatically find segments
 * 
 * @author brad
 * 
 */
public class FindSegments
{
	Repository r;
	SegmentDefinition sd = new SegmentDefinition("AgentGroups", "T7D $ Cost To Serve");
	Connection conn;

	public FindSegments(Repository r, String SegmentDefinitionName) throws SQLException
	{
		this.r = r;
		sd.PhysicalTableName = "DW_DM_AGENT";
		// sd.PhysicalTableName = "DW_DM_CUSTOMER";
		sd.TargetKeyColumn = "ID";
		sd.SegmentColumn = "SEGMENT";
		sd.UpdateTable = true;
		this.conn = r.getDefaultConnection().ConnectionPool.getConnection();
	}

	public void generateSegments() throws Exception
	{
		SuccessModelInstance smi = WekaUtil.retrieveSMI(r.createApplicationPath(), sd.SuccessModel, true);
		smi.setRepository(r);
		List<Integer> colIndexList = new ArrayList<Integer>(smi.mds.numHeaders());
		for (int i = 1; i < smi.mds.numHeaders(); i++)
			colIndexList.add(i);
		for (Iterator<Entry<Object, Segment>> it = smi.getSegmentMap().entrySet().iterator(); it.hasNext();)
		{
			Segment seg = it.next().getValue();
			Instances dataset = smi.constructInstances(sd.SuccessModel, seg.getRowIndexList(), colIndexList, true, false, null, false, smi.mds.getHeaders(),
					null);
			/*
			 * Filter the columns to be used for clustering based on whether there's a target measure. If so, use that
			 * to prioritize which columns are most important
			 */
			if (colIndexList.size() > 20 && sd.TargetMeasure != null)
			{
				int targetIndex = 0;
				for (; targetIndex < dataset.numAttributes(); targetIndex++)
					if (dataset.attribute(targetIndex).name().equals(sd.TargetMeasure))
						break;
				if (targetIndex < dataset.numAttributes())
				{
					/*
					 * Pick only the most important attributes for modelling
					 */
					dataset = AttributeSearch.discretizeNumericClass(dataset, targetIndex, 3);
					dataset.setClassIndex(targetIndex);
					SVMAttributeEval svmeval = new SVMAttributeEval();
					double percent = 30.0 / ((double) dataset.numAttributes());
					double threshold = (percent + 0.1);
					svmeval.setPercentThreshold((int) threshold * 100);
					double removePerIteration = (((int) 100 * threshold - (int) 100 * percent) * dataset.numAttributes() / 100) + 1;
					svmeval.setAttsToEliminatePerIteration((int) removePerIteration);
					svmeval.setPercentToEliminatePerIteration(50);
					svmeval.buildEvaluator(dataset);
					Ranker ranker = new Ranker();
					int[] result = ranker.search(svmeval, dataset);
					Remove rf = new Remove();
					rf.setInvertSelection(true);
					List<Integer> indexList = new ArrayList<Integer>();
					for (int i = 0; i < 30; i++)
					{
						if (result[i] != 0)
							indexList.add(result[i]);
					}
					int[] indices = new int[indexList.size()];
					for (int i = 0; i < indices.length; i++)
						indices[i] = indexList.get(i);
					rf.setAttributeIndicesArray(indices);
					rf.setInputFormat(dataset);
					seg.setClusterIndices(indices);
					dataset = Filter.useFilter(dataset, rf);
				} else
				{
					int[] indices = new int[dataset.numAttributes()];
					for (int i = 0; i < indices.length; i++)
						indices[i] = i;
					seg.setClusterIndices(indices);
				}
				EM em = new EM();
				em.setNumClusters(10);
				em.setMaxIterations(250);
				Clusterer clusterer = em;
				clusterer.buildClusterer(dataset);
				seg.setClusterModel(clusterer);
				seg.setColumnHeaders(smi.mds.getHeaders());
				System.out.println(clusterer.toString());
			}
		}
		WekaUtil.saveSMI(smi);
	}

	public void scoreSegments(boolean writeTable) throws Exception
	{
		SuccessModelInstance smi = WekaUtil.retrieveSMI(r.createApplicationPath(), sd.SuccessModel, true);
		smi.setRepository(r);
		for (Iterator<Entry<Object, Segment>> it = smi.getSegmentMap().entrySet().iterator(); it.hasNext();)
		{
			Segment seg = it.next().getValue();
			int[] indices = seg.getClusterIndices();
			List<Integer> colIndexList = new ArrayList<Integer>(indices.length);
			for (int i = 0; i < indices.length; i++)
				colIndexList.add(indices[i]);
			Instances dataset = smi.constructInstances(sd.SuccessModel, seg.getRowIndexList(), colIndexList, false, false, null, false, seg.getColumnHeaders(),
					null);
			Clusterer clusterer = seg.getClusterModel();
			PreparedStatement stmt = null;
			if (writeTable)
			{
				String query = null;
				if (sd.UpdateTable)
				{
					query = "UPDATE " + sd.PhysicalTableName + " SET " + sd.SegmentColumn + "=? WHERE " + sd.TargetKeyColumn + "=?"
							+ (sd.IterationKeyColumn == null ? "" : " AND " + sd.IterationKeyColumn + "=?");
				} else
				{
					query = "INSERT INTO " + sd.PhysicalTableName + " (" + sd.TargetKeyColumn + ","
							+ (sd.IterationKeyColumn == null ? "" : sd.IterationKeyColumn + ",") + sd.SegmentColumn + ") VALUES (?,?"
							+ (sd.IterationKeyColumn == null ? "" : ",?") + ")";
				}
				stmt = conn.prepareStatement(query);
			}
			for (int i = 0; i < dataset.numInstances(); i++)
			{
				Instance inst = dataset.instance(i);
				int c = clusterer.clusterInstance(inst);
				Object targetKey = smi.mds.getTargetKey(seg.getRowIndexList().get(i));
				Object iteratorKey = smi.mds.getIteratorKey(seg.getRowIndexList().get(i));
				System.out.println(targetKey + "\t" + iteratorKey + "\t" + c);
				if (writeTable)
				{
					stmt.setInt(1, c);
					if (String.class.isInstance(targetKey))
						stmt.setString(2, (String) targetKey);
					else if (Integer.class.isInstance(targetKey))
						stmt.setInt(2, (Integer) targetKey);
					else if (Double.class.isInstance(targetKey))
						stmt.setDouble(2, (Double) targetKey);
					if (sd.IterationKeyColumn != null)
					{
						if (String.class.isInstance(iteratorKey))
							stmt.setString(3, (String) iteratorKey);
						else if (Integer.class.isInstance(iteratorKey))
							stmt.setInt(3, (Integer) iteratorKey);
						else if (Double.class.isInstance(iteratorKey))
							stmt.setDouble(3, (Double) iteratorKey);
					}
					stmt.addBatch();
				}
			}
			if (writeTable)
				stmt.executeBatch();
			if (stmt != null)
				stmt.close();
		}
	}
}
