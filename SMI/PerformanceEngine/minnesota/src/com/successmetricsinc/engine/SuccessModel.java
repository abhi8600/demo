/**
 * $Id: SuccessModel.java,v 1.12 2009-01-15 21:43:31 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jdom.Namespace;
import com.successmetricsinc.Repository;
import com.successmetricsinc.util.XmlUtils;

/**
 * Class to contain the definition of a success model
 * 
 * @author bpeters
 * 
 */
public class SuccessModel implements Serializable
{
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(SuccessModel.class);
	public String Name;
	public String Measure;
	public String TargetDimension;
	public String TargetLevel;
	public String IterateDimension;
	public String IterateLevel;
	public int BreakModel; // 0 - No Break, 1 - Break on Cluster, 2 - Break on
	// Attribute
	public String BreakAttribute;
	public String PartitionAttribute;
	public String[] Attributes;
	public Pattern[] Patterns;
	public String[] Filters;
	public String[] TempFilters; // filters with runtime filter, reset on each run
	public boolean FilterNullCategories;
	public boolean GroupMeasureOnly;
	public String InheritFromModel;
	public ClassifierSettings Classifiers;
	public boolean Runnable;
	public boolean CreateAggregates;

	public SuccessModel()
	{
	}

	public SuccessModel(Element sme, Namespace ns)
	{
		Name = sme.getChildTextTrim("Name", ns);
		Measure = sme.getChildTextTrim("Measure", ns);
		TargetDimension = sme.getChildTextTrim("TargetDimension", ns);
		TargetLevel = sme.getChildTextTrim("TargetLevel", ns);
		IterateDimension = sme.getChildTextTrim("IterateDimension", ns);
		IterateLevel = sme.getChildTextTrim("IterateLevel", ns);
		BreakModel = Integer.parseInt(sme.getChildTextTrim("BreakModel", ns));
		BreakAttribute = sme.getChildTextTrim("BreakAttribute", ns);
		PartitionAttribute = sme.getChildTextTrim("PartitionAttribute", ns);
		Attributes = Repository.getStringArrayChildren(sme, "Attributes", ns);
		Filters = Repository.getStringArrayChildren(sme, "Filters", ns);
		FilterNullCategories = Boolean.parseBoolean(sme.getChildText("FilterNullCategories", ns));
		GroupMeasureOnly = Boolean.parseBoolean(sme.getChildText("GroupMeasureOnly", ns));
		InheritFromModel = sme.getChildTextTrim("InheritFromModel", ns);
		Element spe = sme.getChild("Patterns", ns);
		if (spe != null)
		{
			List l2 = spe.getChildren();
			Patterns = new Pattern[l2.size()];
			for (int count2 = 0; count2 < l2.size(); count2++)
			{
				Pattern p = new Pattern();
				Element pe = (Element) l2.get(count2);
				p.Type = Integer.parseInt(pe.getChildTextTrim("Type", ns));
				p.Measure = pe.getChildTextTrim("Measure", ns);
				p.Dimension1 = pe.getChildTextTrim("Dimension1", ns);
				p.Attribute1 = pe.getChildTextTrim("Attribute1", ns);
				p.Dimension2 = pe.getChildTextTrim("Dimension2", ns);
				p.Attribute2 = pe.getChildTextTrim("Attribute2", ns);
				p.Proportion = Boolean.parseBoolean(pe.getChildTextTrim("Proportion", ns));
				p.Actionable = Boolean.parseBoolean(pe.getChildTextTrim("Actionable", ns));
				p.Model = Boolean.parseBoolean(pe.getChildTextTrim("Model", ns));
				p.MaxPatterns = Integer.parseInt(pe.getChildTextTrim("MaxPatterns", ns));
				Patterns[count2] = p;
			}
		}
		
		// for backwards compatibility
		Element clSettings = sme.getChild("Classifiers", ns);
		if (clSettings != null)
		{
			Classifiers = new ClassifierSettings();
			Classifiers.RegressionModels = Repository.getIntArrayChildren(clSettings, "RegressionModels", ns);
			Classifiers.ClassificationModels = Repository.getIntArrayChildren(clSettings, "ClassificationModels", ns);
			Classifiers.MaxSampleInstanceValues = Integer.parseInt(clSettings.getChildText("MaxSampleInstanceValues", ns));
			Classifiers.MaxInstanceValues = Integer.parseInt(clSettings.getChildText("MaxInstanceValues", ns));
			logger.warn("Found classifier settings in Success Model Dataset: " + Name + ".  Please move it to the Outcome(s).");
		}
		
		String run = sme.getChildText("Runnable", ns);
		if (run != null)
			Runnable = Boolean.parseBoolean(sme.getChildText("Runnable", ns));
		String agg = sme.getChildText("CreateAggregate", ns);
		if (run != null)
			CreateAggregates = Boolean.parseBoolean(sme.getChildText("CreateAggregates", ns));
	}
	
	public boolean isRunnable()
	{
		return Runnable;
	}
	
	public boolean hasAggreates()
	{
		return CreateAggregates;
	}

	public boolean isPartitioned()
	{
		return (PartitionAttribute != null && PartitionAttribute.length() > 0);
	}
	
	public Element getSuccessModelElement(Namespace ns)
	{
		Element e = new Element("SuccessModel",ns);
		
		Element child = new Element("Name",ns);
		child.setText(Name);
		e.addContent(child);
		
		XmlUtils.addContent(e, "Measure", Measure,ns);
	
		XmlUtils.addContent(e, "TargetDimension", TargetDimension,ns);
			
		XmlUtils.addContent(e, "TargetLevel", TargetLevel,ns);
		
		XmlUtils.addContent(e, "IterateDimension", IterateDimension,ns);
				
		XmlUtils.addContent(e, "IterateLevel", IterateLevel,ns);	
		
		child = new Element("BreakModel",ns);
		child.setText(String.valueOf(BreakModel));
		e.addContent(child);
		
		XmlUtils.addContent(e, "BreakAttribute", BreakAttribute,ns);
		
		XmlUtils.addContent(e, "PartitionAttribute", PartitionAttribute,ns);		
		
		if (Attributes!=null && Attributes.length>0)
		{
			e.addContent(Repository.getStringArrayElement("Attributes", Attributes, ns));
		}
		else
		{
			child = new Element("Attributes",ns);
			e.addContent(child);
		}
		
		child = new Element("Patterns",ns);
		if (Patterns!=null && Patterns.length>0)
		{
			for (Pattern pattern : Patterns)
			{
				child.addContent(pattern.getPatternElement(ns));
			}			
		}	
		e.addContent(child);
		
		if (Filters!=null && Filters.length>0)
		{
			e.addContent(Repository.getStringArrayElement("Filters", Filters, ns));
		}
		else
		{
			child = new Element("Filters",ns);
			e.addContent(child);
		}
		
		child = new Element("FilterNullCategories",ns);
		child.setText(String.valueOf(FilterNullCategories));
		e.addContent(child);
		
		XmlUtils.addContent(e, "InheritFromModel", InheritFromModel,ns);    
				
		child = new Element("GroupMeasureOnly",ns);
		child.setText(String.valueOf(GroupMeasureOnly));
		e.addContent(child);
		
		child = new Element("Runnable",ns);
		child.setText(String.valueOf(Runnable));
		e.addContent(child);
		
		child = new Element("CreateAggregates",ns);
		child.setText(String.valueOf(CreateAggregates));
		e.addContent(child);
		
		return e;
	}
}
