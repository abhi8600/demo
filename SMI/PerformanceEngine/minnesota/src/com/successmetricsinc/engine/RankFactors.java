/**
 * $Id: RankFactors.java,v 1.13 2011-09-07 21:19:11 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import weka.core.Utils;

/**
 * @author bpeters
 * 
 */
public class RankFactors
{
	/*
	 * Minimum amount of standard deviation as percentage of mean to be considered. Filters out metrics that are mostly
	 * constant or vary little
	 */
	private static double MIN_STDDEV = 0.025;
	/*
	 * Minimum correlation to consider
	 */
	private static double MIN_CORRELATION = 0.05;
	/*
	 * Minimum lift to consider
	 */
	private static double MIN_LIFT = 1.35;
	SuccessModelInstance smi;

	public RankFactors(SuccessModelInstance smi)
	{
		this.smi = smi;
	}

	private static class CorrelationResult implements Comparable<CorrelationResult>
	{
		int HeaderIndex;
		double Correlation;
		double CorrelationLift;

		public int compareTo(CorrelationResult cr)
		{
			if (Math.abs(Correlation) < Math.abs(cr.Correlation))
				return 1;
			if (Math.abs(Correlation) > Math.abs(cr.Correlation))
				return -1;
			return 0;
		}
	}

	public void rank(int targetIndex, Double minCorr, Double minLift, Double minDev) throws Exception
	{
		if (minCorr == null)
			minCorr = MIN_CORRELATION;
		if (minLift == null)
			minLift = MIN_LIFT;
		if (minDev == null)
			minDev = MIN_STDDEV;
		List<CorrelationResult> results = new ArrayList<CorrelationResult>();
		for (int index = 0; index < smi.mds.numHeaders(); index++)
		{
			if (smi.mds.getHeader(index).NullCategory != true)
			{
				/*
				 * build list of target values for each row - to test for correlation
				 */
				double[] targetList = new double[smi.mds.numRows()];
				double[] valueList = new double[smi.mds.numRows()];
				/*
				 * Get list of values for the column to test for excessive correlation
				 */
				int count = 0;
				double sum = 0;
				double sumsq = 0;
				for (int j = 0; j < smi.mds.numRows(); j++)
				{
					double tar = smi.mds.getCell(j, targetIndex);
					double val = smi.mds.getCell(j, index);
					if (!Double.isNaN(val) && !Double.isNaN(tar))
					{
						targetList[count] = tar;
						valueList[count++] = val;
						sum += val;
						sumsq += val * val;
					}
				}
				double corr = Utils.correlation(targetList, valueList, count);
				double avg = sum / count;
				double stdev = Math.sqrt((sumsq - ((sum * sum) / count)) / count);
				if (avg != 0 && Math.abs(stdev / avg) > minDev && Math.abs(corr) > minCorr && Math.abs(corr) != 1)
				{
					CorrelationResult cr = new CorrelationResult();
					cr.HeaderIndex = index;
					cr.Correlation = corr;
					results.add(cr);
				}
			}
		}
		/*
		 * Go throught and calculate correlation lift - i.e. how correlation for a given breakout metric compares to the
		 * other metrics in the same breakout pattern
		 */
		for (int i = 0; i < smi.sm.Patterns.length; i++)
		{
			Pattern p = smi.sm.Patterns[i];
			if (p.Type == Pattern.MEASUREONLY)
				for (CorrelationResult cr : results)
				{
					if (smi.mds.getHeader(cr.HeaderIndex).PatternIndex == i)
						cr.CorrelationLift = 1;
				}
			else
			{
				double total = 0;
				int count = 0;
				for (CorrelationResult cr : results)
				{
					if (smi.mds.getHeader(cr.HeaderIndex).PatternIndex == i)
					{
						total += cr.Correlation;
						count++;
					}
				}
				if (total != 0)
					for (CorrelationResult cr : results)
					{
						if (smi.mds.getHeader(cr.HeaderIndex).PatternIndex == i)
						{
							cr.CorrelationLift = cr.Correlation / (total / count);
						}
					}
			}
		}
		Collections.sort(results);
		System.out.println("Measure-only Metrics");
		System.out.println("Metric\tCorrelation");
		for (CorrelationResult cr : results)
		{
			if (smi.sm.Patterns[smi.mds.getHeader(cr.HeaderIndex).PatternIndex].Type == Pattern.MEASUREONLY)
				System.out.println(smi.mds.getHeader(cr.HeaderIndex) + "\t" + cr.Correlation);
		}
		System.out.println("\nOne Attribute Breakout Metrics");
		System.out.println("Metric\tAttribute\tValue\tCorrelation\tLift");
		for (CorrelationResult cr : results)
		{
			if (smi.sm.Patterns[smi.mds.getHeader(cr.HeaderIndex).PatternIndex].Type == Pattern.ONEATTRIBUTE && Math.abs(cr.CorrelationLift) > minLift)
			{
				Header h = smi.mds.getHeader(cr.HeaderIndex);
				Pattern p = smi.sm.Patterns[h.PatternIndex];
				System.out.println(p.Measure + "\t" + p.Attribute1 + "\t" + h.Attribute1Value + "\t" + cr.Correlation + "\t" + cr.CorrelationLift);
			}
		}
		System.out.println("\nTwo Attribute Breakout Metrics");
		System.out.println("Metric\tAttribute1\tValue1\tAttribute2\tValue2\tCorrelation\tLift");
		for (CorrelationResult cr : results)
		{
			if (smi.sm.Patterns[smi.mds.getHeader(cr.HeaderIndex).PatternIndex].Type == Pattern.TWOATTRIBUTE && Math.abs(cr.CorrelationLift) > minLift)
			{
				Header h = smi.mds.getHeader(cr.HeaderIndex);
				Pattern p = smi.sm.Patterns[h.PatternIndex];
				System.out.println(p.Measure + "\t" + p.Attribute1 + "\t" + h.Attribute1Value + "\t" + p.Attribute2 + "\t" + h.Attribute2Value + "\t"
						+ cr.Correlation + "\t" + cr.CorrelationLift);
			}
		}
	}
}
