/**
 * $Id: BaseCaseOptimal.java,v 1.5 2008-01-09 22:43:15 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import weka.classifiers.Classifier;
import weka.core.Instance;
import com.successmetricsinc.Repository;

/**
 * Class to solve for base case optimal solution - Assumption is that only base case measures change, and they change by the same
 * amount. Base case is logically equivalent to doing more of the same - hence no relative changes. Base case measures should be
 * ones that grow in proportion to performance, not ratios.
 * 
 * @author bpeters
 * 
 */
public class BaseCaseOptimal extends Optimal
{
    private double firstval;
    private int[] baseIndices;
    private int changeIndex;
    private double penaltyMultiplier;

    public BaseCaseOptimal(Instance inst, double goal, Classifier c, Object key, Repository r, List<String> iterateKeyList,
            Map targetKeyMap, Map peerMap, Performance perf, int changeIndex, int[] baseIndices) throws Exception
    {
        super(inst, goal, c, key, r, iterateKeyList, targetKeyMap, peerMap, perf);
        this.baseIndices = baseIndices;
        this.changeIndex = changeIndex;
        // Calculate penalty multiplier
        if (perf.performance.TargetType==PerformanceModel.TYPE_IMPROVEMENT)
            // Multiplier magnitude +2
            penaltyMultiplier = Math.pow(10, Math.round(Math.log10(inst.dataset().meanOrMode(0)))+2);
        else
            // Multiplier same magnitude as target
            penaltyMultiplier = Math.pow(10, Math.round(Math.log10(inst.dataset().meanOrMode(0))));
    }

    /**
     * Find change in base index and apply to all other base case indices (ensures all base case indices are changed
     * proportionately)
     * 
     * @param newInst
     */
    public void setInstance(Instance newInst)
    {
        if (firstval!=0)
        {
            double change = newInst.value(changeIndex)/firstval;
            for (int i = 0; i<baseIndices.length; i++)
            {
                if (baseIndices[i]!=changeIndex)
                {
                    newInst.setValue(baseIndices[i], change*inst.value(baseIndices[i]));
                }
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see successmetricsinc.com.Optimal#getObjective(weka.core.Instance)
     */
    public double getObjective(Instance newInst) throws Exception
    {
        prob = getProb(newInst);
        if (perf.performance.TargetType==PerformanceModel.TYPE_IMPROVEMENT)
        {
            // Only care if predicted value is close
            double diff = (Math.max(perf.performance.TargetAccuracy, Math.abs(curPredVal-goal)) - perf.performance.TargetAccuracy)/goal;
            double penalty = (diff*diff);
            /*
             * Need an objective function that weights probability highly, but not so highly that small changes swamp changes in
             * difference (utltimately difference should always get to 1, but you want probability to be maximized in the
             * process).
             */
            return (prob-(penaltyMultiplier*penalty));
        } else if (perf.performance.TargetType==PerformanceModel.TYPE_PROBABILITY)
        {
            // Only care if predicted value is close
            double diff = (Math.max(perf.performance.TargetAccuracy/100, Math.abs(prob-goal)) - perf.performance.TargetAccuracy/100)/goal;
            /*
             * Need penalty function that ensures we get the target probability only
             */
            double penalty = (diff*diff);
            /*
             * Objective function corrects difference using multiplier to bring order of magnitude down to normalized range.
             * Penalty for getting probability wrong is then applied
             */
            return ((curPredVal/penaltyMultiplier)-(PROBABILITY_PENALTY_MULTIPLIER*penalty));
        } else
        {
            return (0);
        }
    }
}
