/**
 * $Id: OptimizedResult.java,v 1.2 2007-08-30 16:26:47 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.io.Serializable;

/**
 * Class to contain the results of an optimization process.
 * 
 * @author bpeters
 * 
 */
public class OptimizedResult implements Serializable
{
    private static final long serialVersionUID = 1L;
    Optimize op;
    double goal;
    double predictedValue;
    double baseCasePredictedValue;
}
