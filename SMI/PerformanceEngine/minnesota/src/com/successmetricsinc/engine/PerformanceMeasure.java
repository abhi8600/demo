/**
 * $Id: PerformanceMeasure.java,v 1.4 2010-12-20 12:48:07 mpandit Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.io.Serializable;

import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.util.XmlUtils;

/**
 * @author Brad Peters
 * 
 */
public class PerformanceMeasure implements Serializable
{
	private static final long serialVersionUID = 1L;
	public String Name;
	public int Bucket;
	public String BlockGroup;
	public String CoverGroup;
	public int NumInCoverGroup;
	public boolean ModelFlag;
	public boolean BaseCaseMeasure;
	public boolean Optimize;

	public PerformanceMeasure(Element e, Namespace ns)
	{
		Name = e.getChildTextTrim("Name", ns);
		BaseCaseMeasure = Boolean.parseBoolean(e.getChildTextTrim("BaseCaseMeasure", ns));
		Optimize = Boolean.parseBoolean(e.getChildTextTrim("Optimize", ns));
		BlockGroup = e.getChildTextTrim("BlockGroup", ns);
		Bucket = Integer.parseInt(e.getChildTextTrim("Bucket", ns));
		CoverGroup = e.getChildTextTrim("CoverGroup", ns);
		ModelFlag = Boolean.parseBoolean(e.getChildTextTrim("ModelFlag", ns));
		NumInCoverGroup = Integer.parseInt(e.getChildTextTrim("NumInCoverGroup", ns));
	}
	
	public Element getPerformanceMeasureElement(Namespace ns)
	{
		Element e = new Element("PerformanceMeasure",ns);
		
		Element child = new Element("Name",ns);
		child.setText(Name);
		e.addContent(child);
		
		child = new Element("Bucket",ns);
		child.setText(String.valueOf(Bucket));
		e.addContent(child);
		
		XmlUtils.addContent(e, "BlockGroup", BlockGroup,ns);
		
		XmlUtils.addContent(e, "CoverGroup", CoverGroup,ns);
		
		child = new Element("NumInCoverGroup",ns);
		child.setText(String.valueOf(NumInCoverGroup));
		e.addContent(child);
		
		child = new Element("ModelFlag",ns);
		child.setText(String.valueOf(ModelFlag));
		e.addContent(child);
		
		child = new Element("BaseCaseMeasure",ns);
		child.setText(String.valueOf(BaseCaseMeasure));
		e.addContent(child);
		
		child = new Element("Optimize",ns);
		child.setText(String.valueOf(Optimize));
		e.addContent(child);
		
		return e;
	}

	public String toString()
	{
		return (this.Name);
	}
}