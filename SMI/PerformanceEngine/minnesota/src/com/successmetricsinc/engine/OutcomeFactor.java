/**
 * $Id: OutcomeFactor.java,v 1.6 2010-12-20 12:48:08 mpandit Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.io.Serializable;

import org.jdom.Element;
import org.jdom.Namespace;

/**
 * @author Brad Peters
 * 
 */
public class OutcomeFactor implements Serializable
{
	private static final long serialVersionUID = 1L;
	public enum SettingType
	{
		Zero, Average, Current, Value, RelativeValue
	}
	private String Measure;
	private String Dimension1;
	private String Attribute1;
	private String Attribute1Value;
	private String Dimension2;
	private String Attribute2;
	private String Attribute2Value;
	private SettingType Base;
	private double BaseValue;
	private SettingType Test;
	private double TestValue;
	
	private String strBase;
	private String strTest;

	public OutcomeFactor(String Measure, String Dimension1, String Attribute1, String Attribute1Value, String Dimension2, String Attribute2,
			String Attribute2Value, SettingType Base, double BaseValue, SettingType Test, double TestValue)
	{
		this.Measure = Measure;
		this.Dimension1 = Dimension1;
		this.Attribute1 = Attribute1;
		this.Attribute1Value = Attribute1Value;
		this.Dimension2 = Dimension2;
		this.Attribute2 = Attribute2;
		this.Attribute2Value = Attribute2Value;
		this.Base = Base;
		this.BaseValue = BaseValue;
		this.Test = Test;
		this.TestValue = TestValue;
	}

	public OutcomeFactor(Element e, Namespace ns)
	{
		this.Measure = e.getChildTextTrim("Measure", ns);
		this.Dimension1 = e.getChildTextTrim("Dimension1", ns);
		this.Attribute1 = e.getChildTextTrim("Attribute1", ns);
		this.Attribute1Value = e.getChildTextTrim("Attribute1Value", ns);
		this.Dimension2 = e.getChildTextTrim("Dimension2", ns);
		this.Attribute2 = e.getChildTextTrim("Attribute2", ns);
		this.Attribute2Value = e.getChildTextTrim("Attribute2Value", ns);
		String setting = e.getChildTextTrim("Base", ns);
		strBase = e.getChildText("Base", ns);
		if (setting.equals("Zero"))
			Base = SettingType.Zero;
		else if (setting.equals("Average"))
			Base = SettingType.Average;
		else if (setting.equals("Current"))
			Base = SettingType.Current;
		else if (setting.equals("Value"))
			Base = SettingType.Value;
		else if (setting.equals("RelativeValue"))
			Base = SettingType.RelativeValue;
		this.BaseValue = Double.parseDouble(e.getChildTextTrim("BaseValue", ns));
		setting = e.getChildTextTrim("Test", ns);
		strTest = e.getChildText("Test", ns);
		if (setting.equals("Zero"))
			Test = SettingType.Zero;
		else if (setting.equals("Average"))
			Test = SettingType.Average;
		else if (setting.equals("Current"))
			Test = SettingType.Current;
		else if (setting.equals("Value"))
			Test = SettingType.Value;
		else if (setting.equals("RelativeValue"))
			Test = SettingType.RelativeValue;
		this.TestValue = Double.parseDouble(e.getChildTextTrim("TestValue", ns));
	}
	
	public Element getOutcomeFactorElement(Namespace ns)
	{
		Element e = new Element("OutcomeFactor",ns);
		
		Element child = new Element("Measure",ns);
		child.setText(Measure);
		e.addContent(child);
		
		child = new Element("Dimension1",ns);
		child.setText(Dimension1);
		e.addContent(child);
		
		child = new Element("Attribute1",ns);
		child.setText(Attribute1);
		e.addContent(child);
		
		child = new Element("Attribute1Value",ns);
		child.setText(Attribute1Value);
		e.addContent(child);
		
		child = new Element("Dimension2",ns);
		child.setText(Dimension2);
		e.addContent(child);
		
		child = new Element("Attribute2",ns);
		child.setText(Attribute2);
		e.addContent(child);
		
		child = new Element("Attribute2",ns);
		child.setText(Attribute2Value);
		e.addContent(child);
		
		child = new Element("Base",ns);
		child.setText(strBase);
		e.addContent(child);
		
		child = new Element("Base",ns);
		child.setText(strBase);
		e.addContent(child);
		
		child = new Element("BaseValue",ns);
		child.setText(String.valueOf(BaseValue));
		e.addContent(child);
		
		child = new Element("Test",ns);
		child.setText(strTest);
		e.addContent(child);
		
		child = new Element("TestValue",ns);
		child.setText(String.valueOf(TestValue));
		e.addContent(child);
		
		return e;
	}

	/**
	 * @return the attribute1
	 */
	public String getAttribute1()
	{
		return Attribute1;
	}

	/**
	 * @param attribute1
	 *            the attribute1 to set
	 */
	public void setAttribute1(String attribute1)
	{
		Attribute1 = attribute1;
	}

	/**
	 * @return the attribute1Value
	 */
	public String getAttribute1Value()
	{
		return Attribute1Value;
	}

	/**
	 * @param attribute1Value
	 *            the attribute1Value to set
	 */
	public void setAttribute1Value(String attribute1Value)
	{
		Attribute1Value = attribute1Value;
	}

	/**
	 * @return the attribute2
	 */
	public String getAttribute2()
	{
		return Attribute2;
	}

	/**
	 * @param attribute2
	 *            the attribute2 to set
	 */
	public void setAttribute2(String attribute2)
	{
		Attribute2 = attribute2;
	}

	/**
	 * @return the attribute2Value
	 */
	public String getAttribute2Value()
	{
		return Attribute2Value;
	}

	/**
	 * @param attribute2Value
	 *            the attribute2Value to set
	 */
	public void setAttribute2Value(String attribute2Value)
	{
		Attribute2Value = attribute2Value;
	}

	/**
	 * @return the base
	 */
	public SettingType getBase()
	{
		return Base;
	}

	/**
	 * @param base
	 *            the base to set
	 */
	public void setBase(SettingType base)
	{
		Base = base;
	}

	/**
	 * @return the baseValue
	 */
	public double getBaseValue()
	{
		return BaseValue;
	}

	/**
	 * @param baseValue
	 *            the baseValue to set
	 */
	public void setBaseValue(double baseValue)
	{
		BaseValue = baseValue;
	}

	/**
	 * @return the dimension1
	 */
	public String getDimension1()
	{
		return Dimension1;
	}

	/**
	 * @param dimension1
	 *            the dimension1 to set
	 */
	public void setDimension1(String dimension1)
	{
		Dimension1 = dimension1;
	}

	/**
	 * @return the dimension2
	 */
	public String getDimension2()
	{
		return Dimension2;
	}

	/**
	 * @param dimension2
	 *            the dimension2 to set
	 */
	public void setDimension2(String dimension2)
	{
		Dimension2 = dimension2;
	}

	/**
	 * @return the measure
	 */
	public String getMeasure()
	{
		return Measure;
	}

	/**
	 * @param measure
	 *            the measure to set
	 */
	public void setMeasure(String measure)
	{
		Measure = measure;
	}

	/**
	 * @return the test
	 */
	public SettingType getTest()
	{
		return Test;
	}

	/**
	 * @param test
	 *            the test to set
	 */
	public void setTest(SettingType test)
	{
		Test = test;
	}

	/**
	 * @return the testValue
	 */
	public double getTestValue()
	{
		return TestValue;
	}

	/**
	 * @param testValue
	 *            the testValue to set
	 */
	public void setTestValue(double testValue)
	{
		TestValue = testValue;
	}

	/**
	 * Returns string equivalent
	 * 
	 * @param test
	 *            Whether to print test version or base version
	 * @return
	 */
	public String toString(boolean test)
	{
		if (test)
			return ((Attribute1Value != null ? Attribute1Value : "") + (Attribute2Value != null ? "," + Attribute2Value + "=" : "=") + TestValue);
		else
			return ((Attribute1Value != null ? Attribute1Value : "") + (Attribute2Value != null ? "," + Attribute2Value + "=" : "=") + BaseValue);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		if ((Attribute1 == null) && (Attribute2 == null))
		{
			// special case, don't want to get the '-' at the end
			return Measure;
		}
		return (Measure + '-' + (Attribute1 != null ? Attribute1 : "") + (Attribute2 != null ? ':' + Attribute2 : "")
				+ (Attribute1 != null ? '=' + Attribute1Value : "") + (Attribute2 != null ? ':' + Attribute2Value : ""));
	}
}
