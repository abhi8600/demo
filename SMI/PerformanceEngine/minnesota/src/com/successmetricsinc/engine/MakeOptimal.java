/**
 * $Id: MakeOptimal.java,v 1.7 2007-09-10 16:58:40 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.util.List;
import java.util.Map;
import weka.classifiers.Classifier;
import weka.core.Instance;
import com.successmetricsinc.Repository;

/**
 * @author bpeters
 * 
 */
public class MakeOptimal extends Optimal
{
	double penaltyMultiplier;

	public MakeOptimal(Instance inst, double goal, Classifier c, Object key, Repository r, List<String> iterateKeyList, Map targetKeyMap,
			Map peerMap, Performance perf) throws Exception
	{
		super(inst, goal, c, key, r, iterateKeyList, targetKeyMap, peerMap, perf);
		// Get order of magnitude of target
		double order = Math.round(Math.log10(inst.dataset().meanOrMode(0)));
		// Calculate penalty multiplier
		if (perf.performance.TargetType == PerformanceModel.TYPE_IMPROVEMENT)
			// Multiplier magnitude +4
			penaltyMultiplier = Math.pow(10, order + 4);
		else
			// Multiplier same magnitude as target
			penaltyMultiplier = Math.pow(10, order);
		// Make the change penalty multiplier 100 or smaller (if penalty multiplier is <1000)
	}

	/* (non-Javadoc)
	 * @see com.successmetricsinc.engine.Optimal#getObjective(weka.core.Instance)
	 */
	public double getObjective(Instance newInst) throws Exception
	{
		this.prob = getProb(newInst);
		if (perf.performance.TargetType == PerformanceModel.TYPE_IMPROVEMENT)
		{
			// Only care if predicted value is close
			double diff = (Math.max(perf.performance.TargetAccuracy, Math.abs(curPredVal - goal)) - perf.performance.TargetAccuracy) / goal;
			double penalty = (diff * diff);
			/*
			 * Need an objective function that weights probability highly, but not so highly that small changes swamp
			 * changes in difference (utltimately difference should always get to 1, but you want probability to be
			 * maximized in the process).
			 */
			return (prob - (penaltyMultiplier * penalty));
		} else if (perf.performance.TargetType == PerformanceModel.TYPE_PROBABILITY)
		{
			// Only care if predicted value is close
			double diff = (Math.max(perf.performance.TargetAccuracy / 100, Math.abs(prob - goal)) - perf.performance.TargetAccuracy / 100) / goal;
			/*
			 * Need penalty function that ensures we get the target probability only
			 */
			double penalty = (diff * diff);
			/*
			 * Objective function corrects difference using multiplier to bring order of magnitude down to normalized
			 * range. Penalty for getting probability wrong is then applied
			 */
			return ((curPredVal / penaltyMultiplier) - (PROBABILITY_PENALTY_MULTIPLIER * penalty));
		} else
		{
			return (0);
		}
	}
}
