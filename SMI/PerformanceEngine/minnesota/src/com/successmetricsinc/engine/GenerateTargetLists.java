/**
 * $Id: GenerateTargetLists.java,v 1.43 2012-05-21 13:22:23 mpandit Exp $
 *
 * Copyright (C) 2007-2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.query.Database;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.DimensionColumn;
import com.successmetricsinc.query.DisplayFilter;
import com.successmetricsinc.query.GroupBy;
import com.successmetricsinc.query.MeasureColumn;
import com.successmetricsinc.query.Query;
import com.successmetricsinc.query.QueryFilter;
import com.successmetricsinc.query.QueryResultSet;
import com.successmetricsinc.query.QueryString;
import com.successmetricsinc.query.SyntaxErrorException;
import com.successmetricsinc.query.Token;

/**
 * Class to generate target lists based on filters generated during success pattern analysis
 * 
 * @author bpeters
 * 
 */
public class GenerateTargetLists
{
	private static Logger logger = Logger.getLogger(GenerateTargetLists.class);
	// Metadata on internal schema generated from Pattern Searches
	private String PATTERN_DIMENSION = "Pattern";
	private String ITERATION_COLUMN_NAME = "Iteration";
	private String PATTERN_COLUMN_NAME = "Pattern";
	private String MEASURE_USED_COLUMN_NAME = "Measure Used";
	private String MEASURE_RANK_COLUMN_NAME = "Measure Rank";
	private String CATEGORY_FILTER_COLUMN_NAME = "Category Filter";
	private String PATTERN_SCORE_COLUMN_NAME = "Pattern Score";
	private int MAX_RECORDS = 1000000;
	private Repository r;
	private DatabaseConnection dc;
	private Connection conn;
	private PatternSearchDefinition psd;
	private SuccessModel sm;
	private PrintWriter writer = null;

	public GenerateTargetLists(Repository r, DatabaseConnection dc, String successPatternDefinition) throws SQLException
	{
		logger.info("Generate Target lists for " + successPatternDefinition);
		
		this.r = r;
		this.dc = dc;
		this.conn = dc.ConnectionPool.getConnection();
		
		psd = r.findSearchDefinition(successPatternDefinition);
		if (psd == null)
		{
			logger.error("Could not find success pattern: " + successPatternDefinition);
			return;
		}
		sm = r.findSuccessModel(psd.SuccessModelName);
		if (sm == null)
		{
			logger.error("Could not find success model: " + psd.SuccessModelName);
			return;
		}
		PATTERN_DIMENSION = r.getResource("Pattern Dimension").getValue();
		ITERATION_COLUMN_NAME = r.getResource("Iteration Column Name").getValue();
		PATTERN_COLUMN_NAME = r.getResource("Pattern Column Name").getValue();
		MEASURE_USED_COLUMN_NAME = r.getResource("Measure Used Column Name").getValue();
		MEASURE_RANK_COLUMN_NAME = r.getResource("Measure Rank Column Name").getValue();
		CATEGORY_FILTER_COLUMN_NAME = r.getResource("Category Filter Column Name").getValue();
		PATTERN_SCORE_COLUMN_NAME = r.getResource("Pattern Score Column Name").getValue();
	}

	public void generateTargetLists(boolean writeTable, boolean writeCSV, String path) throws Exception
	{
		Statement stmt = conn.createStatement();
		Query q = new Query(r);
		q.addDimensionColumn(sm.TargetDimension, sm.TargetLevel);
		q.addGroupBy(new GroupBy(sm.TargetDimension, sm.TargetLevel));
		q.addDimensionColumn(PATTERN_DIMENSION, ITERATION_COLUMN_NAME);
		q.addGroupBy(new GroupBy(PATTERN_DIMENSION, ITERATION_COLUMN_NAME));
		q.addDimensionColumn(PATTERN_DIMENSION, PATTERN_COLUMN_NAME);
		q.addGroupBy(new GroupBy(PATTERN_DIMENSION, PATTERN_COLUMN_NAME));
		q.addDimensionColumn(PATTERN_DIMENSION, MEASURE_USED_COLUMN_NAME);
		q.addGroupBy(new GroupBy(PATTERN_DIMENSION, MEASURE_USED_COLUMN_NAME));
		q.addDimensionColumn(PATTERN_DIMENSION, MEASURE_RANK_COLUMN_NAME);
		q.addGroupBy(new GroupBy(PATTERN_DIMENSION, MEASURE_RANK_COLUMN_NAME));
		q.addDimensionColumn(PATTERN_DIMENSION, CATEGORY_FILTER_COLUMN_NAME);
		q.addGroupBy(new GroupBy(PATTERN_DIMENSION, CATEGORY_FILTER_COLUMN_NAME));
		q.addMeasureColumn(PATTERN_SCORE_COLUMN_NAME, PATTERN_SCORE_COLUMN_NAME, false);
		if ((psd.IterationAnalysisValue != null) && (psd.IterationAnalysisValue.length() > 0))
		{
			q.addFilter(new QueryFilter(PATTERN_DIMENSION, ITERATION_COLUMN_NAME, "=", psd.IterationAnalysisValue));
		}
		String targetFilter = r.getResource("Target Filter").getValue();
		if (targetFilter != null)
			q.addFilterList(new String[]
			{ targetFilter });
		q.generateQuery(false);
		String query = q.getQuery();
		query = r.replaceVariables(null, query);
		logger.info("generateTargetLists");
		logger.debug(Query.getPrettyQuery(query));
		ResultSet rs = stmt.executeQuery(query);
		HashMap<String, List<TargetListDefinition>> map = new HashMap<String, List<TargetListDefinition>>();
		List<String> segmentMeasures = new ArrayList<String>();
		for (int i = 0; i < psd.SegmentMeasures.length; i++)
			segmentMeasures.add(psd.SegmentMeasures[i]);
		/*
		 * Get all target lists
		 */
		while (rs.next())
		{
			TargetListDefinition tld = new TargetListDefinition();
			tld.key = rs.getString(1);
			tld.iteration = rs.getString(2);
			tld.pattern = rs.getString(3);
			tld.measure = rs.getString(4);
			tld.rank = rs.getInt(5);
			tld.filter = rs.getString(6);
			tld.score = rs.getDouble(7);
			String key = tld.key;
			if ((tld.filter != null) && (tld.filter.length() > 0) && segmentMeasures.contains(tld.measure))
			{
				/*
				 * If not processing each target separately, just allocate to one threads
				 */
				if (!psd.ProcessTargetsSeparately)
				{
					key = "MasterThread";
				}
				List<TargetListDefinition> list = map.get(key);
				if (list == null)
				{
					list = new ArrayList<TargetListDefinition>();
					map.put(key, list);
				}
				list.add(tld);
			}
		}
		rs.close();
		stmt.close();

		if (writeTable)
		{
			// create the table, drop if necessary
			List<String> columnNames = new ArrayList<String>();
			List<String> columnTypes = new ArrayList<String>();
			columnNames.add(r.getResource("Target Key ID").getValue());
			columnTypes.add("VARCHAR(30)");
			columnNames.add(r.getResource("Target ID").getValue());
			columnTypes.add("VARCHAR(30)");
			columnNames.add(r.getResource("Target Iteration").getValue());
			columnTypes.add("VARCHAR(30)");
			columnNames.add(r.getResource("Target Pattern").getValue());
			columnTypes.add("VARCHAR(100)");
			columnNames.add(r.getResource("Target Message").getValue());
			columnTypes.add("VARCHAR(100)");
			columnNames.add(r.getResource("Target Score").getValue());
			columnTypes.add("FLOAT");
			
			Database.createTable(r, dc, psd.SegmentOutputTableName, columnNames, columnTypes, psd.DropExistingSegmentOutputTable, false, null, null, null);
			Map<String, String> valueByColumn = new HashMap<String, String>();
			valueByColumn.put(r.getResource("Target Iteration").getValue(), r.replaceVariables(null, psd.IterationAnalysisValue));
			Database.deleteOldData(dc, psd.SegmentOutputTableName, valueByColumn);
		}

		if (writeCSV)
		{
			writer = new PrintWriter(new FileWriter(path + File.separator + psd.SegmentOutputTableName + ".txt"));
			writer.println(r.getResource("Target Key ID").getValue() + "\t"
	                     + r.getResource("Target ID").getValue() + "\t"
	                     + r.getResource("Target Iteration").getValue() + "\t"
	                     + r.getResource("Target Pattern").getValue() + "\t"
	                     + r.getResource("Target Message").getValue() + "\t"
	                     + r.getResource("Target Score").getValue());
		}
		threadIterator = map.entrySet().iterator();
		int maxThreads = r.getDefaultConnection().MaxConnectionThreads;
		Thread[] tlist = new Thread[maxThreads];
		// Run threads to get lists of patterns for each client
		logger.info("Getting segment memberships");
		for (int i = 0; i < maxThreads; i++)
		{
			tlist[i] = new processSegmentThread(writeTable, writeCSV, writer);
			tlist[i].start();
		}
		for (int i = 0; i < maxThreads; i++)
		{
			tlist[i].join();
		}
		
		if (writeCSV)
		{
			writer.close();
		}
	}
	
	
	Iterator<Entry<String, List<TargetListDefinition>>> threadIterator;

	private synchronized List<TargetListDefinition> getNextList()
	{
		if (threadIterator.hasNext())
		{
			Entry<String, List<TargetListDefinition>> me = (Entry<String, List<TargetListDefinition>>) threadIterator.next();
			return ((List<TargetListDefinition>) me.getValue());
		}
		return (null);
	}

	private class processSegmentThread extends Thread
	{
		private boolean writeTable = false;
		private boolean writeCSV = false;
		PrintWriter writer = null;
		
		public processSegmentThread(boolean writeTable, boolean writeCSV, PrintWriter writer)
		{
			this.setName("Process Segment - " + this.getId());
			this.writeTable = writeTable;
			this.writeCSV = writeCSV;
			this.writer = writer;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Runnable#run()
		 */
		public void run()
		{
			while (true)
			{
				List<TargetListDefinition> list = getNextList();
				if (list == null)
					return;
				/*
				 * Need to go through each target list and see all combinations of dimensions. Create a set of unique
				 * combinations. For each set, include the dimension columns associated with that set. Also include the
				 * measures associated with that set. Then later will create a query for each set. That set of
				 * dimensions will bet attached to each target list so when filtering the result set, the right query
				 * result set can be identified/used. (to compare sets, use containsall and check that size is the same)
				 */
				/*
				 * Data structure to contain all unique sets of dimensions queried together
				 */
				List<HashSet<String>> dimensionSetList = new ArrayList<HashSet<String>>();
				/*
				 * Map of all measures that relate to each dimension set. Maps each HashSet of dimensions to a given
				 * list of measures.
				 */
				HashMap<HashSet<String>, HashSet<MeasureColumn>> measureMap = new HashMap<HashSet<String>, HashSet<MeasureColumn>>();
				/*
				 * Map of all dimensionColumns that relate to each dimension set. Maps each HashSet of dimensions to a
				 * given list of dimensionColumns.
				 */
				HashMap<HashSet<String>, HashSet<DimensionColumn>> dimensionColumnMap = new HashMap<HashSet<String>, HashSet<DimensionColumn>>();
				// Map to link each target with the appropriate target lists
				HashMap<Object, List<TargetListDefinition>> targetMap = new HashMap<Object, List<TargetListDefinition>>();
				// List to link query result sets with each dimension set
				HashMap<HashSet<String>, QueryResultSet> dimensionSetResults = new HashMap<HashSet<String>, QueryResultSet>();
				String key = null;
				String itvalue = r.replaceVariables(null, psd.IterationAnalysisValue);
				for (int i = 0; i < list.size(); i++)
				{
					TargetListDefinition tld = list.get(i);
					key = tld.key;
					// Parse the filter
					List<Token> tokens = Token.getTokens(tld.filter);
					// Take the first filter and return
					if ((tokens != null) && (tokens.size() > 0))
					{
						try
						{
							QueryFilter qf = QueryString.returnFilter((Token) tokens.get(0), r);
							tld.qf = qf;
							HashSet<Object> qfSet = new HashSet<Object>();
							qf.generateQueryObjectList(r, qfSet);
							// Set of dimensions for this target list
							HashSet<String> dimSet = new HashSet<String>();
							// Measures contained in list
							HashSet<MeasureColumn> measureSet = new HashSet<MeasureColumn>();
							measureSet.add(r.findMeasureColumn(tld.measure));
							// Dimensions contained in list
							HashSet<DimensionColumn> dimensionColumnSet = new HashSet<DimensionColumn>();
							/*
							 * Find all the dimensions in this target list. Record all the dimension columns present.
							 * Record all the measures present.
							 */
							for (Iterator<Object> it = qfSet.iterator(); it.hasNext();)
							{
								Object o = it.next();
								if (o.getClass() == DimensionColumn.class)
								{
									/*
									 * First, add this dimension to the unique set
									 */
									DimensionColumn dc = (DimensionColumn) o;
									dimSet.add(dc.DimensionTable.DimensionName);
									dimensionColumnSet.add(dc);
								} else if (o.getClass() == MeasureColumn.class)
								{
									MeasureColumn mc = (MeasureColumn) o;
									measureSet.add(mc);
								}
							}
							/*
							 * Find either the existing matching set of dimensions, or create a new one
							 */
							HashSet<String> foundSet = null;
							for (int j = 0; j < dimensionSetList.size(); j++)
							{
								HashSet<String> curSet = (HashSet<String>) dimensionSetList.get(j);
								if ((curSet.size() == dimSet.size()) && (curSet.containsAll(dimSet)))
								{
									foundSet = curSet;
									break;
								}
							}
							// If not found, add a new one to the list
							if (foundSet == null)
							{
								foundSet = dimSet;
								dimensionSetList.add(dimSet);
							}
							/*
							 * Record this set as associated with this target list (ensuring that the right query result
							 * set is used later)
							 */
							tld.dimensionSet = foundSet;
							/*
							 * Now, associate the set with the dimension columns and the measure columns
							 */
							HashSet<MeasureColumn> set = (HashSet<MeasureColumn>) measureMap.get(foundSet);
							if (set == null)
							{
								set = new HashSet<MeasureColumn>();
								measureMap.put(foundSet, set);
							}
							set.addAll(measureSet);
							HashSet<DimensionColumn> set2 = (HashSet<DimensionColumn>) dimensionColumnMap.get(foundSet);
							if (set2 == null)
							{
								set2 = new HashSet<DimensionColumn>();
								dimensionColumnMap.put(foundSet, set2);
							}
							set2.addAll(dimensionColumnSet);
						} catch (Exception ex)
						{
							logger.error(ex.toString(), ex);
						}
					}
				}
				// Generate one query for each dimension set
				for (int i = 0; i < dimensionSetList.size(); i++)
				{
					HashSet<String> dimSet = (HashSet<String>) dimensionSetList.get(i);
					// Generate a query using all objects in the set
					Query listq = new Query(r);
					try
					{
						listq.addDimensionColumn(psd.TargetDimension, psd.TargetLevel);
						listq.addGroupBy(new GroupBy(psd.TargetDimension, psd.TargetLevel));
						listq.addDimensionColumn(sm.TargetDimension, sm.TargetLevel);
						listq.addGroupBy(new GroupBy(sm.TargetDimension, sm.TargetLevel));
						// Add appropriate Dimension columns
						HashSet<DimensionColumn> dimensionColumns = dimensionColumnMap.get(dimSet);
						for (Iterator<DimensionColumn> it = dimensionColumns.iterator(); it.hasNext();)
						{
							DimensionColumn dc = it.next();
							listq.addDimensionColumn(dc.DimensionTable.DimensionName, dc.ColumnName);
							listq.addGroupBy(new GroupBy(dc.DimensionTable.DimensionName, dc.ColumnName));
						}
						// Add appropriate Measure columns
						HashSet<MeasureColumn> measureColumns = measureMap.get(dimSet);
						for (Iterator<MeasureColumn> it = measureColumns.iterator(); it.hasNext();)
						{
							MeasureColumn mc = it.next();
							listq.addMeasureColumn(mc.ColumnName, mc.ColumnName, false);
						}
						/*
						 * Add a join measure to ensure no cross-joins (i.e. when there is no measure in the filter -
						 * there will be a cross-join between the dimensions). The join measure should be slicable
						 * trivially by all dimensions
						 */
						listq.addMeasureColumn(psd.JoinMeasure, psd.JoinMeasure, true);
						/*
						 * If processing individual targets separately, add the key filter to the query
						 */
						if (psd.ProcessTargetsSeparately)
						{
							listq.addFilter(new QueryFilter(sm.TargetDimension, sm.TargetLevel, "=", key));
							logger.info("Processing key: " + key);
						}
						if ((psd.IterationAnalysisValue != null) && (psd.IterationAnalysisValue.length() > 0) && (sm.IterateLevel != null)
								&& (sm.IterateLevel.length() > 0) && (sm.IterateDimension != null) && (sm.IterateDimension.length() > 0))
						{
							listq.addFilter(new QueryFilter(sm.IterateDimension, sm.IterateLevel, "=", itvalue));
						}
						listq.addFilterList(sm.Filters);
					} 
					catch (Exception ex)
					{
						logger.error(ex.getMessage(), ex);
						return;
					}
					// Generate result set for all targets for given key
					QueryResultSet qrs = null;
					try
					{
						/*
						 * Use a full outer join to ensure results for all IDs in each query
						 */
						listq.setFullOuterJoin(true);
						listq.generateQuery(false);
						logger.info("Sending Query: " + listq.getPrettyQuery());
						qrs = new QueryResultSet(listq, MAX_RECORDS, 0, null);
						dimensionSetResults.put(dimSet, qrs);
					} catch (Exception ex)
					{
						logger.info(ex.toString(), ex);
						return;
					}
				}
				/*
				 * Now, repeatedly apply display filters to the result set to get the list of target IDs
				 */
				logger.info("Processing " + list.size() + " targets");
				for (int i = 0; i < list.size(); i++)
				{
					TargetListDefinition tld = (TargetListDefinition) list.get(i);
					key = tld.key;
					logger.info("Processing: " + tld.key + " (" + tld.filter + ")");
					QueryFilter qf = tld.qf;
					// Turn the query filter into a display filter
					DisplayFilter df = qf.returnDisplayFilter(r, null);
					if (df == null || !df.isValid())
					{
						logger.warn("Filter could not be converted or is invalid");
						return;
					}
					List<DisplayFilter> displayFilters = new ArrayList<DisplayFilter>();
					displayFilters.add(df);
					if (!psd.ProcessTargetsSeparately)
					{
						/*
						 * Apply key filter at the result set level if filtering in batch
						 */
						df = new DisplayFilter(sm.TargetLevel, "=", key);
						displayFilters.add(df);
					}
					QueryResultSet qrs = (QueryResultSet) dimensionSetResults.get(tld.dimensionSet);
					if (qrs != null)
					{
						QueryResultSet filteredResults = null;
						try
						{
							filteredResults = qrs.returnFilteredResultSet(displayFilters, r);
						} catch (SyntaxErrorException ex)
						{
							logger.error(ex.toString(), ex);
							return;
						}
						Object[][] rows = filteredResults.getRows();
						// Add targets to hashmap of lists
						for (int j = 0; j < rows.length; j++)
						{
							// First object in row is the target key
							List<TargetListDefinition> targetList = targetMap.get(rows[j][0]);
							if (targetList == null)
							{
								targetList = new ArrayList<TargetListDefinition>();
								targetMap.put(rows[j][0], targetList);
							}
							targetList.add(tld);
						}
					}
				}
				PreparedStatement pstmt = null;
				
				if (writeTable)
				{
					try
					{
						pstmt = conn.prepareStatement("INSERT INTO " + psd.SegmentOutputTableName + " VALUES(?,?,?,?,?,?)");
					} catch (SQLException ex)
					{
						logger.error(ex.toString(), ex);
						return;
					}
				}
				try
				{
					// Now save the top n segment memberships for each target
					logger.debug("Saving segment memberships: map size is " + targetMap.size());
					logger.debug("psd.ScreenDuplicateMeasures is " + psd.ScreenDuplicateMeasures);
					logger.debug("psd.NumTargetSegmentsToSave = " + psd.NumTargetSegmentsToSave);
					int count = 0;
					
					for (Map.Entry<Object, List<TargetListDefinition>> me : targetMap.entrySet())
					{
						String targetKey = me.getKey().toString();
						List<TargetListDefinition> targetList = me.getValue();
						// Sort the list
						Collections.sort(targetList);
						// Remove lower ranked duplicate measures if desired
						if (psd.ScreenDuplicateMeasures)
						{
							List<TargetListDefinition> toRemove = new ArrayList<TargetListDefinition>();
							for (int j = targetList.size() - 1; j >= 0; j--)
							{
								TargetListDefinition tld = (TargetListDefinition) targetList.get(j);
								for (int k = j - 1; k >= 0; k--)
								{
									TargetListDefinition tld2 = (TargetListDefinition) targetList.get(k);
									if (tld2.measure.equals(tld.measure))
									{
										toRemove.add(tld2);
									}
								}
							}
							targetList.removeAll(toRemove);
						}

						for (int j = 0; (j < targetList.size()) && (j < psd.NumTargetSegmentsToSave); j++)
						{
							TargetListDefinition tld = (TargetListDefinition) targetList.get(targetList.size() - j - 1);
							String val = tld.measure + " " + ((r.findMeasureColumn(tld.measure).Improve >= 0) ? "Low" : "High");
							if (writeTable)
							{
								pstmt.setString(1, tld.key);
								pstmt.setString(2, targetKey);
								pstmt.setString(3, itvalue);
								pstmt.setString(4, tld.pattern);
								pstmt.setString(5, val);
								pstmt.setDouble(6, tld.score);
								pstmt.addBatch();
							}
							if (writeCSV)
							{
								writer.println(tld.key + "\t" + targetKey + "\t" + itvalue + "\t" + tld.pattern + "\t"
											+ val + "\t" + tld.score);
							}
							if ((++count % 5000) == 0)
							{
								logger.info("Output " + count + " target segments");
							}
						}
						if (writeTable)
						{
							// once per targetMap iteration
							pstmt.executeBatch();
						}
					}
					logger.info("Total target segments: " + count);
				} catch (SQLException ex)
				{
					logger.error(ex.toString(), ex);
					break;
				}
			}
		}
	}

	private class TargetListDefinition implements Comparable<TargetListDefinition>
	{
		public String key;
		public String iteration;
		public String pattern;
		public String measure;
		public int rank;
		public String filter;
		public double score;
		public QueryFilter qf;
		public HashSet dimensionSet;

		public int compareTo(TargetListDefinition tld)
		{
			return (Double.compare(score, ((TargetListDefinition) tld).score));
		}

		public String toString()
		{
			return (key + "(" + iteration + "):" + pattern + "-" + filter);
		}
	}
}
