/**
 * $Id: Segment.java,v 1.48 2011-09-07 21:19:12 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;
import org.apache.log4j.Logger;
import weka.classifiers.Classifier;
import weka.clusterers.Clusterer;
import weka.core.Instance;
import weka.core.Instances;
import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.doublealgo.Statistic;
import cern.colt.matrix.impl.DenseDoubleMatrix2D;
import cern.jet.stat.Probability;
import com.successmetricsinc.engine.Header.HeaderType;
import com.successmetricsinc.util.WekaUtil;

/**
 * Segment definition for a given success model
 * 
 * @author Brad Peters
 */
public class Segment implements Externalizable, Cloneable
{
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(Segment.class);
	private static final double CHANGE_AMT = 0.5;
	private static final int MIN_SAMPLE_SIZE = 1000;
	private static final double MAX_SAMPLE_PCT = 0.05;
	private Object key;
	List<Integer> rowIndexList;
	List<Integer> selectedColumnIndexList;
	private List<Header> columnHeaders;
	BestFitResult segmentValueClassifier;
	private BestFitResult segmentClassClassifier;
	private BestFitResult segmentValueForClassClassifier;
	private List<Integer> baseColumnList;
	private List<Integer> columnIndexList;
	private List<Integer> classColumnIndexList;
	private List<double[]> impactRows;
	private int numTotalColumns;
	SuccessModelInstance smi;
	transient Instances sampleInstances;
	transient Instances classSampleInstances;
	private double[] correlations;
	private double[] standardDeviations;
	private Clusterer clusterModel = null;
	private int[] clusterIndices = null;
	
	private OutcomeAnalysis outcomeAnalysis;

	public Segment()
	{
	}

	public Segment(SuccessModelInstance smi)
	{
		this.smi = smi;
		selectedColumnIndexList = new ArrayList<Integer>();
		rowIndexList = new ArrayList<Integer>();
	}

	@SuppressWarnings("unchecked")
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException
	{
		key = in.readObject();
		rowIndexList = (List<Integer>) in.readObject();
		selectedColumnIndexList = (List<Integer>) in.readObject();
		columnHeaders = (List<Header>) in.readObject();
		segmentValueClassifier = (BestFitResult) in.readObject();
		segmentClassClassifier = (BestFitResult) in.readObject();
		segmentValueForClassClassifier = (BestFitResult) in.readObject();
		columnIndexList = (List<Integer>) in.readObject();
		numTotalColumns = (Integer) in.readObject();
		clusterModel = (Clusterer) in.readObject();
		clusterIndices = (int[]) in.readObject();
		classColumnIndexList = (List<Integer>) in.readObject();
	}

	public void writeExternal(ObjectOutput out) throws IOException
	{
		out.writeObject(key);
		out.writeObject(rowIndexList);
		out.writeObject(selectedColumnIndexList);
		out.writeObject(columnHeaders);
		out.writeObject(segmentValueClassifier);
		out.writeObject(segmentClassClassifier);
		out.writeObject(segmentValueForClassClassifier);
		out.writeObject(columnIndexList);
		out.writeObject(Integer.valueOf(numTotalColumns));
		out.writeObject(clusterModel);
		out.writeObject(clusterIndices);
		out.writeObject(classColumnIndexList);
	}

	public Object clone() throws CloneNotSupportedException
	{
		Segment s = (Segment) super.clone();
		s.key = key;
		s.rowIndexList = new ArrayList<Integer>(rowIndexList);
		s.selectedColumnIndexList = new ArrayList<Integer>(selectedColumnIndexList);
		s.columnHeaders = new ArrayList<Header>(columnHeaders);
		s.segmentValueClassifier = segmentValueClassifier;
		s.segmentClassClassifier = segmentClassClassifier;
		s.segmentValueForClassClassifier = segmentValueForClassClassifier;
		if (columnIndexList == null)
			s.columnIndexList = new ArrayList<Integer>();
		else
			s.columnIndexList = new ArrayList<Integer>(columnIndexList);
		if (classColumnIndexList == null)
			s.classColumnIndexList = new ArrayList<Integer>();
		else
			s.classColumnIndexList = new ArrayList<Integer>(classColumnIndexList);
		s.numTotalColumns = numTotalColumns;
		s.clusterModel = clusterModel;
		if (clusterIndices != null)
			s.clusterIndices = clusterIndices.clone();
		return (s);
	}

	public void outputModels(PrintStream stream) throws Exception
	{
		stream.println("Segment: " + key);
		if (segmentValueClassifier != null)
		{
			stream.println("Value Classifier");
			stream.println(segmentValueClassifier.getClassifier().toString());
			stream.println("\nValue Classifier Error");
			stream.println(segmentValueClassifier.getErrorString());
			stream.println();
			stream.println();
		}
		if (segmentClassClassifier != null)
		{
			stream.println("Class Classifier");
			stream.println(segmentClassClassifier.getClassifier().toString());
			stream.println("\nClass Classifier Error");
			stream.println(segmentClassClassifier.getErrorString());
			stream.println();
			stream.println();
		}
		if (segmentValueForClassClassifier != null)
		{
			stream.println("Value For Class Classifier");
			stream.println(segmentValueForClassClassifier.getClassifier().toString());
			stream.println("\nValue For Class Classifier Error");
			stream.println(segmentValueForClassClassifier.getErrorString());
			stream.println();
			stream.println();
		}
	}

	public synchronized double classifyInstanceValue(Instance inst) throws Exception
	{
		return (this.segmentValueClassifier.getClassifier().classifyInstance(inst));
	}

	public synchronized double classifyInstanceClass(Instance inst) throws Exception
	{
		return (this.segmentClassClassifier.getClassifier().classifyInstance(inst));
	}

	public synchronized double classifyInstanceValueForClass(Instance inst) throws Exception
	{
		return (this.segmentValueForClassClassifier.getClassifier().classifyInstance(inst));
	}

	/**
	 * Get the row index of a given target key. Return -1 if not found
	 */
	public int getTargetRowIndex(Object key)
	{
		for (int i = 0; i < rowIndexList.size(); i++)
		{
			int index = rowIndexList.get(i);
			if (smi.mds.getTargetKey(index).toString().equals(key.toString()))
			{
				return (index);
			}
		}
		return (-1);
	}

	/**
	 * Generate a matrix that indicates the individual impacts of each column in this segment against the target column
	 * 
	 * @param sample
	 *            Whether or not to generate a matrix based on a sample
	 * @throws Exception
	 */
	public void generateImpactMatrix(boolean sample) throws Exception
	{
		baseColumnList = new ArrayList<Integer>();
		// Generate list of base columns that underly success patterns
		for (int index : selectedColumnIndexList)
		{
			Header h = smi.mds.getHeader(index);
			if (h.Type == HeaderType.Cluster)
			{
				Pattern p = smi.sm.Patterns[h.PatternIndex];
				for (int j = 0; j < p.NumCategories; j++)
				{
					if (!selectedColumnIndexList.contains(p.Startindex + j) && !baseColumnList.contains(p.Startindex + j))
						baseColumnList.add(p.Startindex + j);
				}
			}
		}
		double[] deviations = this.getDeviations();
		rowIndex = 0;
		/*
		 * Sample rows if necessary
		 */
		if (!sample)
			impactRowList = new ArrayList<Integer>(rowIndexList);
		else
		{
			int sampleSize = Math.max(Math.min(MIN_SAMPLE_SIZE, rowIndexList.size()), (int) (MAX_SAMPLE_PCT * ((double) rowIndexList.size())));
			impactRowList = new ArrayList<Integer>(sampleSize);
			Random r = new Random();
			// sample within a partition
			int sampleRange = rowIndexList.size();
			if (smi.mds.getPartitionSize(0) > 2 * sampleSize)  // to make sure that the loop below eventually ends
				sampleRange = smi.mds.getPartitionSize(0);
				
			for (int i = 0; i < sampleSize; i++)
			{
				boolean done = false;
				do
				{
					int index = r.nextInt(sampleRange);
					if (!impactRowList.contains(index))
					{
						impactRowList.add(index);
						done = true;
					}
				} while (!done);
			}
			Collections.sort(impactRowList); // do this so we properly iterate through the partitions
		}
		// Instantiate impact rows
		if (Double.isNaN(standardDeviations[0]) || standardDeviations[0] == 0)
		{
			logger.warn("Standard Deviation of the target measure is " + standardDeviations[0] + ", no reason to calculate impacts");
			return;
		}
		String targetMeasure = (outcomeAnalysis == null || outcomeAnalysis.getOutcome().TargetMeasure == null) ? smi.sm.Measure : outcomeAnalysis.getOutcome().TargetMeasure;
		int targetIndex = smi.getIndexForMeasure(targetMeasure);
		if (targetIndex < 0)
		{
			throw new Exception("Unable to find measure '" + targetMeasure + "'");
		}
		
		impactRows = Collections.synchronizedList(new ArrayList<double[]>(impactRowList.size()));
		int numThreads = this.smi.r.getModelParameters().MaxComputationThreads;
		Thread[] tlist = new Thread[numThreads];
		
		logger.info("Calculating Row Impacts");
		for (int i = 0; i < numThreads; i++)
		{
			tlist[i] = new SegImpactClass(targetIndex, deviations, standardDeviations[0]);
			tlist[i].start();
		}
		for (int i = 0; i < numThreads; i++)
		{
			tlist[i].join();
		}
	}
	private int rowIndex = 0;
	private List<Integer> impactRowList;

	private synchronized int getNextRow()
	{
		if (rowIndex < impactRowList.size())
		{
			int result = impactRowList.get(rowIndex++);
			if (rowIndex % 1000 == 0)
				logger.info("[" + rowIndex + "]");
			return (result);
		} else
			return (-1);
	}
	private static double MIN_CORR = -2;
	private static double MAX_CORR = 2;

	private class SegImpactClass extends Thread
	{
		int targetIndex;
		double[] deviations;
		double targetsd;
		Classifier c;

		public SegImpactClass(int targetIndex, double[] deviations, double targetsd) throws Exception
		{
			this.setName("Segment Impact - " + this.getId());
			this.targetIndex = targetIndex;
			this.deviations = deviations;
			this.targetsd = targetsd;
			if (Segment.this.segmentValueClassifier == null)
			{
				logger.error("No value classifier for the segment");
				throw new Exception("No value classifier for the segment");
			}
			this.c = Segment.this.segmentValueClassifier.getClassifierCopy();
		}

		public void run()
		{
			while (true)
			{
				int rindex = getNextRow();
				if (rindex < 0)
					break;
				double[] impacts = new double[selectedColumnIndexList.size() + 1];
				impactRows.add(impacts);
				SegmentRow sr = new SegmentRow(smi, Segment.this, rindex, targetIndex);
				try
				{
					double pred = sr.classify(c);
					// Record column impacts
					impacts[0] = smi.mds.getCell(rindex, targetIndex);  // 0 is target value, not impact of column
					int curCol = 1;
					for (int colIndex : selectedColumnIndexList)
					{
						double oldval = sr.getColumn(colIndex);
						if (!Double.isNaN(oldval))
						{
							double sd = deviations[curCol - 1]; // deviations array does not have the target at index position 0
							if (sd != 0) // no need to test impact if oldval and newval are the same
							{
								double newval = oldval + (CHANGE_AMT * sd);
								sr.updateColumn(colIndex, newval);
								double newpred = sr.classify(c);
								double impact = (newpred - pred) / (CHANGE_AMT * targetsd);
								impacts[curCol] = Double.isNaN(impact) ? 0 : Math.max(MIN_CORR, Math.min(MAX_CORR, impact));
								sr.updateColumn(colIndex, oldval);
							}
						}
						curCol++;
					}
				} catch (Exception ex)
				{
					logger.error("Error: " + ex);
				}
			}
		}
	}

	private static class PatternMetricsComparator implements Comparator<Entry<String, double[]>>
	{
		public int compare(Entry<String, double[]> e1, Entry<String, double[]> e2)
		{
			double[] o1 = e1.getValue();
			double[] o2 = e2.getValue();
			return ((int) (o1[o1.length - 1] - o2[o2.length - 1]));
		}
	}

	/**
	 * Output the generated impact matrix
	 * 
	 */
	public void outputImpacts(boolean summary)
	{
		if (!summary)
		{
			System.out.println();
			System.out.println("Impacts for each selected column for all of the sampled rows");
			System.out.print("Target");
			for (int index : selectedColumnIndexList)
				System.out.print("\t" + smi.mds.getHeader(index));
			System.out.println();
			for (double[] row : impactRows)
			{
				System.out.print(row[0]);
				for (int i = 1; i < row.length; i++)
					System.out.print("\t" + row[i]);
				System.out.println();
			}
		} else
		{
			System.out.println("Summary of the impacts for the selected columns, using " + impactRows.size() + " sampled rows");
			System.out.println("Name\tPattern\tMin Impact\tAvg Impact\tMedian Impact\tMax Impact\tSD of Impact\tt-test(%)");
			List<Double> sortList = new ArrayList<Double>(impactRows.size());
			Map<String, double[]> patternMap = new HashMap<String, double[]>();
			for (int i = 0; i <= selectedColumnIndexList.size(); i++)
			{
				int count = 0;		// number of rows that had an impact for the column
				double sum = 0;		// sum of the impacts for the column
				double sumsq = 0;   // sum of the squares of the impacts for the column
				double min = Double.MAX_VALUE;  // min impact for the column
				double max = -Double.MAX_VALUE;  // max impact for the column
				sortList.clear();
				for (double[] row : impactRows)
				{
					if (!Double.isNaN(row[i]) && row[i] != 0)  // row[i] set to 0 when impact is NaN in classification method
					{
						if (row[i] > max)
							max = row[i];
						if (row[i] < min)
							min = row[i];
						sum += row[i];
						sumsq += row[i] * row[i];
						count++;
						sortList.add(row[i]);
					}
				}
				if (count > 1)  // only useful is more than 1 (see variance calculation below)
				{
					if (i == 0)
						System.out.print("Target\t");
					else
						System.out.print(smi.mds.getHeader(selectedColumnIndexList.get(i - 1)) + "\t");
					Collections.sort(sortList);
					double avg = sum / count;  // average impact for the column
					double var = (sumsq - (count * avg * avg)) / (count - 1);
					if (var < 0)
						var = 0;
					double sd = Math.sqrt(var);  // standard deviation for the column
					double t = 100 * Probability.studentT(impactRows.size() - selectedColumnIndexList.size() - 1, Math.abs(avg) / sd);
					if (i == 0)
						System.out.print('\t');
					else
					{
						Header h = smi.mds.getHeader(selectedColumnIndexList.get(i - 1));
						String pat = null;
						if (h.Type == HeaderType.Pattern)
						{
							Pattern p = smi.sm.Patterns[h.PatternIndex];
							if (p.Type == Pattern.MEASUREONLY)
								pat = p.Measure;
							else if (p.Type == Pattern.ONEATTRIBUTE)
								pat = p.Measure + " by " + p.Dimension1 + "." + p.Attribute1;
							else if (p.Type == Pattern.TWOATTRIBUTE)
								pat = p.Measure + " by " + p.Dimension1 + "." + p.Attribute1 + " by " + p.Dimension2 + "." + p.Attribute2;
						} else
						{
							int eq = h.Name.indexOf('=');
							if (eq < 0)
								pat = h.Name;
							else
								pat = h.Name.substring(0, eq);
						}
						System.out.print(pat + "\t");
						double[] mets = patternMap.get(pat);
						if (mets == null)
						{
							mets = new double[5];
							patternMap.put(pat, mets);
							mets[1] = Double.MAX_VALUE;
							mets[3] = -Double.MAX_VALUE;
						}
						mets[0]++;
						mets[1] = Math.min(mets[1], avg);
						mets[2] += avg;
						mets[3] = Math.max(mets[3], avg);
						mets[4] += t;
					}
					System.out.printf("%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.1f\n", min, avg, sortList.get(sortList.size() / 2), max, sd, t);
				}
			}
			PatternMetricsComparator pcomp = new PatternMetricsComparator();
			List<Entry<String, double[]>> plist = new ArrayList<Entry<String, double[]>>(patternMap.entrySet());
			Collections.sort(plist, pcomp);
			System.out.println();
			System.out.println();
			System.out.println("Pattern Summary");
			System.out.println("Name\tPattern count\tMin(Avg)\tAvg(Avg)\tMax(Avg)\tAvg(t-test)");
			for (Entry<String, double[]> entry : plist)
			{
				double[] mets = entry.getValue();
				System.out.printf(entry.getKey() + "\t%d\t%.3f\t%.3f\t%.3f\t%.3f\n", (int) mets[0], mets[1], mets[2] / mets[0], mets[3], mets[4] / mets[0]);
			}
		}
	}

	public synchronized void setSampleInstances() throws Exception
	{
		if (sampleInstances == null)
		{
			String targetMeasure = (outcomeAnalysis == null || outcomeAnalysis.getOutcome().TargetMeasure == null) ? smi.sm.Measure : outcomeAnalysis.getOutcome().TargetMeasure;
			sampleInstances = smi.constructInstances("SAMPLE", rowIndexList, selectedColumnIndexList, true, false, targetMeasure, false, null, null);
		}
	}

	public BestFitResult getSegmentValueClassifier()
	{
		return segmentValueClassifier;
	}

	public BestFitResult getSegmentClassClassifier()
	{
		return segmentClassClassifier;
	}

	public BestFitResult getSegmentValueForClassClassifier()
	{
		return segmentValueForClassClassifier;
	}

	public void setSegmentValueClassifier(BestFitResult segmentClassifier)
	{
		this.segmentValueClassifier = segmentClassifier;
	}

	public void setSegmentClassClassifier(BestFitResult segmentClassifier)
	{
		this.segmentClassClassifier = segmentClassifier;
	}

	public void setSegmentValueForClassClassifier(BestFitResult segmentClassifier)
	{
		this.segmentValueForClassClassifier = segmentClassifier;
	}

	public Object getKey()
	{
		return key;
	}

	public void setKey(Object key)
	{
		this.key = key;
	}

	public List<Integer> getRowIndexList()
	{
		return rowIndexList;
	}

	public void clearRowIndexList()
	{
		rowIndexList.clear();
	}

	public List<Integer> getSelectedColumnIndexList()
	{
		return selectedColumnIndexList;
	}

	public void setSelectedColumnIndexList(List<Integer> selectedColumnIndexList)
	{
		this.selectedColumnIndexList = selectedColumnIndexList;
	}

	public List<Integer> getSelectedClassColumnIndexList()
	{
		return classColumnIndexList;
	}

	public void setSelectedClassColumnIndexList(List<Integer> selectedColumnIndexList)
	{
		this.classColumnIndexList = selectedColumnIndexList;
	}
	
	public void setSmi(SuccessModelInstance smi)
	{
		this.smi = smi;
	}
	
	public void setOutcomeAnalysis(OutcomeAnalysis oa)
	{
		outcomeAnalysis = oa;
		setSmi(oa.getSuccessModelInstance());
	}

	public int getNumTotalColumns()
	{
		return numTotalColumns;
	}

	/**
	 * @return the columnHeaders
	 */
	public List<Header> getColumnHeaders()
	{
		return columnHeaders;
	}

	/**
	 * Get a list of the standard deviations for the selected columns
	 * 
	 * @return
	 */
	private double[] getDeviations() throws Exception
	{
		if (standardDeviations == null)
			calculateCorrelations();
		double[] deviations = new double[selectedColumnIndexList.size()];
		for (int i = 0; i < deviations.length; i++)
		{
			deviations[i] = standardDeviations[selectedColumnIndexList.get(i)];
		}
		return (deviations);
	}

	/**
	 * Calculate the covariance matrix for the selected columns
	 * XXX will need to be updated to play well with partitions, the loops are nested the wrong way
	 * 
	 * @return
	 */
	@SuppressWarnings("unused")
	private DoubleMatrix2D getCovarianceMatrix() throws Exception
	{
		DoubleMatrix2D matrix = new DenseDoubleMatrix2D(rowIndexList.size(), selectedColumnIndexList.size() + 1);
		double[] means = new double[selectedColumnIndexList.size() + 1];
		
		String targetMeasure = (outcomeAnalysis == null || outcomeAnalysis.getOutcome().TargetMeasure == null) ? smi.sm.Measure : outcomeAnalysis.getOutcome().TargetMeasure;
		int targetIndex = smi.getIndexForMeasure(targetMeasure);
		if (targetIndex < 0)
		{
			throw new Exception("Unable to find measure '" + targetMeasure + "'");
		}
		
		// Go through each column
		for (int i = 0; i <= selectedColumnIndexList.size(); i++)
		{
			// Go through each row
			int count = 0;
			double sum = 0;
			for (int curRow : rowIndexList)
			{
				if (i == 0)
				{
					double val = smi.mds.getCell(curRow, targetIndex);
					if (!Double.isNaN(val))
					{
						sum += val;
						count++;
					}
				} else
				{
					double val = smi.mds.getCell(curRow, selectedColumnIndexList.get(i - 1));
					if (!Double.isNaN(val))
					{
						sum += val;
						count++;
					}
				}
			}
			means[i] = count > 0 ? sum / count : 0;
		}
		// Go through each row
		for (int curRow : rowIndexList)
		{
			// Go through each column
			for (int i = 0; i <= selectedColumnIndexList.size(); i++)
			{
				double val = 0;
				if (i == 0)
				{
					val = smi.mds.getCell(curRow, targetIndex);
					if (Double.isNaN(val))
						val = means[i];
				} else
				{
					val = smi.mds.getCell(curRow, selectedColumnIndexList.get(i - 1));
					if (Double.isNaN(val))
						val = means[i];
				}
				// Remove mean to correct for different starting values
				matrix.set(curRow, i, val);
			}
		}
		// Now calculate covariance matrix
		return (Statistic.covariance(matrix));
	}
	private boolean calculatedCorrelations = false;

	/**
	 * Calculate the correlations of the columns in this segment with the target column
	 */
	private void calculateCorrelations() throws Exception
	{
		// Calculate correlations, means and standard deviations
		int numHeaders = smi.mds.numHeaders();
		correlations = new double[numHeaders];
		standardDeviations = new double[numHeaders];
		double[] means = new double[numHeaders];
		double[] sum = new double[numHeaders];
		double[] sumsq = new double[numHeaders];
		double[] min = new double[numHeaders];
		double[] max = new double[numHeaders];
		int[] count = new int[numHeaders];

		for (int i = 0; i < numHeaders; i++)
		{
			min[i] = Double.MAX_VALUE;
			max[i] = -Double.MIN_VALUE;
		}
		
		String targetMeasure = (outcomeAnalysis == null || outcomeAnalysis.getOutcome().TargetMeasure == null) ? smi.sm.Measure : outcomeAnalysis.getOutcome().TargetMeasure;
		int targetIndex = smi.getIndexForMeasure(targetMeasure);
		if (targetIndex < 0)
		{
			throw new Exception("Unable to find measure '" + targetMeasure + "'");
		}
		
		// calculate the sums and sum squared for each header
		for (int j : rowIndexList)
		{
			for (int i = 0; i < numHeaders; i++)
			{
				Header h = smi.mds.getHeader(i);
				if ((i == 0) || (h.Type == HeaderType.Pattern) || (h.Type == HeaderType.Attribute))
				{
					double val = 0;
					
					if (i == 0)
					{
						val = WekaUtil.propVal(smi.mds.getCell(j, targetIndex));
					}
					else
					{
						val = WekaUtil.propVal(smi.mds.getCell(j, i));
					}
					min[i] = Math.min(min[i], val);
					max[i] = Math.max(max[i], val);
					sum[i] += val;
					sumsq[i] += val * val;
					count[i]++;
				}
			}
		}
		
		// calculate means and standard deviations
		for (int i = 0; i < numHeaders; i++)
		{
			int n = count[i];
			double mean = sum[i] / n;
			double var = (sumsq[i] - (n * mean * mean)) / (n - 1);
			if (var < 0)
				var = 0;
			standardDeviations[i] = Math.sqrt(var);
			means[i] = mean;
		}
		
		// short circuit, no values to correlate
		if (rowIndexList.size() <= 1) {
			for (int i = 0; i < numHeaders; i++)
				correlations[i] = 1.0;
			calculatedCorrelations = true;
			return;
		}
		  
		// calculate the correlations
		double[] y11 = new double[numHeaders];
		double[] y12 = new double[numHeaders];
		double[] y22 = new double[numHeaders];
		double av1 = means[0];
		for (int i : rowIndexList)
		{
			double y1 = WekaUtil.propVal(smi.mds.getCell(i, targetIndex));
			double y11sq = (y1 - av1) * (y1 - av1);
			for (int j = 1; j < numHeaders; j++)
			{
				double y2 = WekaUtil.propVal(smi.mds.getCell(i, j));
				double av2 = means[j];
				y11[j] += y11sq;
				y22[j] += (y2 - av2) * (y2 - av2);
				y12[j] += (y1 - av1) * (y2 - av2);
		    }
		}
		
		for (int i = 1; i < numHeaders; i++)
		{
		    if (y11[i] * y22[i] == 0.0) {
		    	correlations[i] = 1.0;
		    }
		    else
		    {
		    	correlations[i] = y12[i] / Math.sqrt(Math.abs(y11[i] * y22[i]));
		    }
		}
		y11 = null;
		y12 = null;
		y22 = null;
		calculatedCorrelations = true;
		/*
		for (int i = 0; i < numHeaders; i++)
		{
			logger.debug(i + ": cnt=" + count[i] + ", min=" + min[i] + ", max=" + max[i] + ", cor=" + correlations[i] + ", mean=" + means[i] + ", stddev=" + standardDeviations[i]);
		}
		*/
		return;
	}

	/**
	 * Return the correlation with the target value for a given index;
	 * 
	 * @param index
	 * @return
	 */
	public double getCorrelation(int index) throws Exception
	{
		if (!calculatedCorrelations)
			calculateCorrelations();
		return (correlations[index]);
	}

	/**
	 * Return the standard deviation for the target column
	 * 
	 * @return
	 */
	public double getTargetStandardDeviation() throws Exception
	{
		if (!calculatedCorrelations)
			calculateCorrelations();
		return (standardDeviations[0]);
	}

	/**
	 * Return the standard deviation of a column of given index;
	 * 
	 * @param index
	 * @return
	 */
	public double getStandardDeviation(int index) throws Exception
	{
		if (!calculatedCorrelations)
			calculateCorrelations();
		return (standardDeviations[index]);
	}

	/**
	 * Estimate the change in the target column based on a change in a given column. This method uses the linear
	 * correlation between the column in question and the target column to create an estimate of change.
	 * 
	 * @param index
	 * @param diff
	 * @return
	 */
	public synchronized double getLinearChangeEstimate(int index, double diff) throws Exception
	{
		if (!calculatedCorrelations)
			calculateCorrelations();
		double correlation = correlations[index];
		double targetDeviation = standardDeviations[0];
		double colDeviation = standardDeviations[index];
		double correlationPrediction = 0;
		if (colDeviation != 0)
			correlationPrediction = diff * correlation * targetDeviation / colDeviation;
		return (correlationPrediction);
	}

	/**
	 * @param columnHeaders
	 *            the columnHeaders to set
	 */
	public void setColumnHeaders(List<Header> columnHeaders)
	{
		this.columnHeaders = columnHeaders;
	}

	/**
	 * @return the clusterModel
	 */
	public Clusterer getClusterModel()
	{
		return clusterModel;
	}

	/**
	 * @param clusterModel
	 *            the clusterModel to set
	 */
	public void setClusterModel(Clusterer clusterModel)
	{
		this.clusterModel = clusterModel;
	}

	/**
	 * @return the clusterIndices
	 */
	public int[] getClusterIndices()
	{
		return clusterIndices;
	}

	/**
	 * @param clusterIndices
	 *            the clusterIndices to set
	 */
	public void setClusterIndices(int[] clusterIndices)
	{
		this.clusterIndices = clusterIndices;
	}
}
