/**
 * $Id: InvalidateCache.java,v 1.19 2012-10-11 23:11:02 BIRST\ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine.operation;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.Types;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.User;
import com.successmetricsinc.query.CacheOperations;
import com.successmetricsinc.query.Filter;
import com.successmetricsinc.query.QueryString;
import com.successmetricsinc.query.ResultSetCache;

import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XmlUtils;

/**
 * Class to encapsulate an invalidate-cache action
 * 
 * @author Brad Peters
 * 
 */
public class InvalidateCache extends Action implements Serializable
{
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(InvalidateCache.class);
	public enum InvalidateType
	{
		LogicalQuery, LogicalTable
	};
	private InvalidateType Type;
	private String Query;
	private String TableName;
	private static Pattern pattern = Pattern.compile("GETPROMPTVALUE\\(\'.*?\'\\)", Pattern.CASE_INSENSITIVE);

	public InvalidateCache(Element e, Namespace ns)
	{
		ActionTypeName = e.getChildText("ActionTypeName",ns);
		
		String s = e.getChildText("ImageIndex",ns);
		if (s!=null)
		{
			ImageIndex = Integer.parseInt(s);
		}
		
		Name = e.getChildText("Name",ns);
		
		String typeStr = e.getChildText("Type", ns);
		if (typeStr.equals("LogicalQuery"))
			Type = InvalidateType.LogicalQuery;
		else if (typeStr.equals("LogicalTable"))
			Type = InvalidateType.LogicalTable;
		Query = e.getChildTextTrim("Query", ns);
		TableName = e.getChildTextTrim("TableName", ns);
	}

	@Override
	protected int execute(Connection conn, User user, Map<String, String> servletParameterMap, Repository r, Session session, ResultSetCache cache, Parameter[] parameters) throws Exception
	{
		String queryWithParamsReplaced = Query;
		for(Parameter p : parameters)
		{
			if(p.getType() != Parameter.ParameterType.SingleValue)
			{
				continue;
			}
			String name = p.getName();
			Object value = p.getValue();
			
			if((name != null) && (value != null))
			{
				queryWithParamsReplaced = queryWithParamsReplaced.replace("$P{" + name + "}", value.toString());
			}
		}
		Matcher matcher = pattern.matcher(queryWithParamsReplaced);
		if (r.isUseNewQueryLanguage())
		{
			while(matcher.find())
			{
				String key = matcher.group();
				key = key.substring("GETPROMPTVALUE('".length(), key.length() - 2);
				boolean isReplacedUsingParameters = false;
				for(Parameter p : parameters)
				{
					if(p.getType() != Parameter.ParameterType.SingleValue)
					{
						continue;
					}
					String name = p.getName();
					Object value = p.getValue();
					if (name != null && name.equals(key) && value != null)
					{
						//convert value to appropriate datatype
						String valString = value.toString();
						switch(p.getDatatype())
						{
						case Types.INTEGER	:						
						case Types.DOUBLE	:
							break;
						case Types.VARCHAR	:
							if (!valString.startsWith("'") && !valString.endsWith("'"))
								value = "'" + valString + "'";
							break;
						case Types.TIMESTAMP:	
							if (!valString.startsWith("#") && !valString.endsWith("#"))
								value = "#" + valString + "#";
							break;
						}
						queryWithParamsReplaced = Util.replaceStr(queryWithParamsReplaced, "GETPROMPTVALUE('" + key + "')", value.toString());
						isReplacedUsingParameters = true;
						break;
					}
				}
				if (!isReplacedUsingParameters && session != null)
				{
					Filter f = Session.getFilter(key);
					if (f != null)
					{
						List<String> list = f.getValues();
						if (list != null)
						{
							if (list.size() == 1)
							{
								if (QueryString.NO_FILTER_TEXT.equals(list.get(0)))
								{
									queryWithParamsReplaced = Util.replaceStr(queryWithParamsReplaced, "GETPROMPTVALUE('" + key + "')", "'" + QueryString.NO_FILTER_TEXT + "'");
								}
								if (f.getClazz() == null)
									queryWithParamsReplaced = Util.replaceStr(queryWithParamsReplaced, "GETPROMPTVALUE('" + key + "')", list.get(0));									
								else
								{
									String valString = list.get(0);
									switch(f.getClazz())
									{
									case "Integer"	:						
									case "Number"	:
									case "Float"	:
										break;
									case "Varchar"	:
									case "Boolean"	:
										if (!valString.startsWith("'") && !valString.endsWith("'"))
											valString = "'" + valString + "'";
										break;
									case "DateTime":
									case "Date":
										if (!valString.startsWith("#") && !valString.endsWith("#"))
											valString = "#" + valString + "#";
										break;
									}
									queryWithParamsReplaced = Util.replaceStr(queryWithParamsReplaced, "GETPROMPTVALUE('" + key + "')", valString);									
								}
							}							
						}
						else
						{
							logger.debug("Filter.getValues returned null for: " + key.toString());
						}
					}
					else
					{
						logger.debug("No prompt in map (via session.getFilter) for key: " + key.toString());
					}
				}
			}			
		}
		if(servletParameterMap == null)
		{
			if (Type == InvalidateType.LogicalQuery)
			{
				boolean foundMatch = cache.removeCacheForQuery(queryWithParamsReplaced, r, session, false, false);
				if(foundMatch)
				{
					logger.debug("Successfully removed cache entries for query "+queryWithParamsReplaced);
				}
				else
				{
					logger.debug("Could not find any cache entries for query "+queryWithParamsReplaced);
				}
			} else if (Type == InvalidateType.LogicalTable)
			{
				boolean foundMatch = cache.removeCacheForTable(TableName, r, session, true, false);
				if(foundMatch)
				{
					logger.debug("Successfully removed cache entries for table "+TableName);
				}
				else
				{
					logger.debug("Could not find any cache entries for table "+TableName);
				}
				
			}
		}
		else
		{
			String cmd, cmdParameter;
			if (Type == InvalidateType.LogicalQuery)
			{
				cmd = "ClearCacheForQuery";
				cmdParameter = queryWithParamsReplaced;
			} else if (Type == InvalidateType.LogicalTable)
			{
				cmd = "ClearCacheForLogicalTable";
				cmdParameter = TableName;
			}
			else
			{
				return ACTION_SUCCESS;
			}
			servletParameterMap.put("cmd", cmd);
			servletParameterMap.put("cmdParameter", cmdParameter);
			servletParameterMap.put("ExactMatchOnly", "true");
			CacheOperations co = new CacheOperations();
			co.performCacheOperation(servletParameterMap, r, session, cache);
		}
		return ACTION_SUCCESS;
	}
	
	public Element getInvalidateCacheElement(Namespace ns)
	{
		Element e = new Element("InvalidateCache",ns);
		
		Element child = new Element("Name",ns);
		child.setText(Name);
		e.addContent(child);
		
		XmlUtils.addContent(e, "ActionTypeName", ActionTypeName,ns);
		
		child = new Element("ImageIndex",ns);
		child.setText(String.valueOf(ImageIndex));
		e.addContent(child);
		
		child = new Element("Type",ns);
		if (Type.equals(InvalidateType.LogicalQuery))
			child.setText("LogicalQuery");
		else if (Type.equals(InvalidateType.LogicalTable))
			child.setText("LogicalTable");
		e.addContent(child);
		
		XmlUtils.addContent(e, "Query", Query,ns);
		
		XmlUtils.addContent(e, "TableName", TableName,ns);
		
		return e;
	}
}
