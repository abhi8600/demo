/**
 * $Id: Action.java,v 1.18 2011-09-26 19:03:48 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine.operation;

import java.util.List;
import java.util.Map;
import org.jdom.Element;
import org.jdom.Namespace;

import java.io.Serializable;
import java.sql.Connection;
import com.successmetricsinc.Repository;
import com.successmetricsinc.User;
import com.successmetricsinc.query.ResultSetCache;
import com.successmetricsinc.Session;

/**
 * Class to encapsulate individual transactions in an operation
 * 
 * @author Brad Peters
 * 
 */
public abstract class Action implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	 protected String ActionTypeName;
	 protected int ImageIndex;
	 protected String Name;
	
	//Used for return code from action/operation. < 0 means failure error code, >=0 is success
	public static final int ACTION_SUCCESS = 0;
	
	public static Action createAction(Element e, Namespace ns)
	{			
		
		if (e.getName().equals("UpdateStatement"))
		{
			return (new UpdateStatement(e, ns));
		} else if (e.getName().equals("InvalidateCache"))
		{
			return (new InvalidateCache(e, ns));
		}		
		else if (e.getName().equals("InvalidateDashboardCache"))
		{
			return (new InvalidateDashboardCache(e, ns));
		}
		else if (e.getName().equals("Email"))
		{
			return (new Email(e, ns));
		}				
		
		return (null);
	}
	
	public Element getActionElement(Namespace ns)
	{
		Element e = null;
		
		if (this instanceof UpdateStatement)
		{
			UpdateStatement updStmt = (UpdateStatement)this;
			return updStmt.getUpdateStatementElement(ns);
		} else if (this instanceof InvalidateCache)
		{
			InvalidateCache actionObj = (InvalidateCache)this;
			return actionObj.getInvalidateCacheElement(ns);
		}		
		else if (this instanceof InvalidateDashboardCache)
		{
			InvalidateDashboardCache actionObj = (InvalidateDashboardCache) this;
			return actionObj.getInvalidateDashboardCacheElement(ns);
		}
		else if (this instanceof Email)
		{
			Email actionObj = (Email) this;
			return actionObj.getEmailElement(ns);
		}
		
		return e;
	}

	protected abstract int execute(Connection conn, User user, Map<String, String> servletParameterMap, Repository r, Session session, ResultSetCache cache, Parameter[] parameters) throws Exception;
	
	protected String replaceVariables(Repository r, Session session, String str)
	{
		if(str == null || str.equals(""))
		{
			return str;
		}
		String strWithVariablesReplaced = str;
		
		strWithVariablesReplaced = r.replaceVariables(null, strWithVariablesReplaced);
		strWithVariablesReplaced = r.replaceVariables(session, strWithVariablesReplaced);
		
		return strWithVariablesReplaced;
	}
}
