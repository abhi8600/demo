package com.successmetricsinc.engine.operation;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Map;

import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.User;
import com.successmetricsinc.query.ResultSetCache;

public class InvalidateDashboardCache extends Action implements Serializable {

	public InvalidateDashboardCache(Element e,Namespace ns)
	{
		ActionTypeName = e.getChildText("ActionTypeName",ns);
		
		String s = e.getChildText("ImageIndex",ns);
		if (s!=null)
		{
			ImageIndex = Integer.parseInt(s);
		}
		
		Name = e.getChildText("Name",ns);
	}
	
	public Element getInvalidateDashboardCacheElement(Namespace ns)
	{
		Element e = new Element("InvalidateDashboardCache",ns);
		
		Element child = new Element("Name",ns);
		child.setText(Name);
		e.addContent(child);
		
		child = new Element("ActionTypeName",ns);
		child.setText(ActionTypeName);
		e.addContent(child);
		
		child = new Element("ImageIndex",ns);
		child.setText(String.valueOf(ImageIndex));
		e.addContent(child);		
		
		return e;
	}
	
	@Override
	protected int execute(Connection conn, User user,
			Map<String, String> servletParameterMap, Repository r,
			Session session, ResultSetCache cache, Parameter[] parameters)
			throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

}
