/**
 * $Id: UpdateStatement.java,v 1.20 2011-09-26 19:03:48 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine.operation;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.User;
import com.successmetricsinc.engine.operation.Parameter.ParameterType;
import com.successmetricsinc.query.ResultSetCache;

/**
 * Class to encapsulate a database update action
 * 
 * @author Brad Peters
 * 
 */
public class UpdateStatement extends Action implements Serializable
{
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(UpdateStatement.class);
	private String Statement;
	private boolean StoredProcedure;

	public UpdateStatement(Element e, Namespace ns)
	{
		ActionTypeName = e.getChildText("ActionTypeName",ns);
		
		String s = e.getChildText("ImageIndex",ns);
		if (s!=null)
		{
			ImageIndex = Integer.parseInt(s);
		}
		
		Name = e.getChildText("Name",ns);
		
		Statement = e.getChildTextTrim("Statement", ns);
		StoredProcedure = Boolean.parseBoolean(e.getChildTextTrim("StoredProcedure", ns));
	}

	@Override
	protected int execute(Connection conn, User user, Map<String, String> servletParameterMap, Repository r, Session session, ResultSetCache cache, Parameter[] parameters) throws SQLException
	{
		int returnCode = ACTION_SUCCESS; //Default success
		/*
		 * Do parameter replacement and build prepared statement
		 */
		
		// ('a'..'z' | 'A'..'Z' | '_' | ' ' | ':' | '$' | '#' | '%' | '-' | '/' | '&' | '!' | '^' | '@' | ',' | ';' | '>' | '<' | '0'..'9' | '\u0080'..'\uFFFF')
		// 'a' .. 'z' may be [a-zA-Z_....]+ 
		//\$P\{[a-zA-X_ :\$#%-/&!^@,;><0-9\u0080-\uFFFF]+\}
		Pattern pat = Pattern.compile("\\$P\\{[a-zA-X_ :\\$#%-/&!^@,;><0-9\u0080-\uFFFF]+\\}");
		Matcher m = pat.matcher(Statement);
		int lastPos = 0;
		StringBuilder sb = new StringBuilder();
		List<String> parameterSet = new ArrayList<String>();
		int numInParameters = 0;
		while (m.find())
		{
			int start = m.start();
			int end = m.end();
			if (start > 0)
				sb.append(Statement.substring(lastPos, start));
			sb.append('?');
			numInParameters++;
			lastPos = end;
			parameterSet.add(Statement.substring(start+3, end-1));
		}
		if (lastPos < Statement.length())
			sb.append(Statement.substring(lastPos));
		
		String statementWithParamsReplaced = sb.toString();
		statementWithParamsReplaced = replaceVariables(r, session, statementWithParamsReplaced);
		logger.debug("Prepared Statement = "+statementWithParamsReplaced);		
		if(StoredProcedure)
		{
			CallableStatement stmt = conn.prepareCall(statementWithParamsReplaced);
			//If it is a stored procedure, execute only once.
			for (int j = 0; j < parameterSet.size(); j++)
			{
				for (Parameter p : parameters)
				{
					if (p.getName().equals(parameterSet.get(j)))
					{
						if (p.getType() == ParameterType.SingleValue)
							setStatementValue(stmt, j+1, p.getDatatype(), p.getValue());
						else if (p.getType() == ParameterType.MultiValue)
						{
							String paramValueXML = p.getValueXML();
							logger.debug("Parameter Value XML: "+paramValueXML);
							setStatementValue(stmt, j+1, Types.LONGVARCHAR, paramValueXML);
						}
						break;
					}
				}
			}
			
			stmt.registerOutParameter(numInParameters+1, Types.INTEGER);
			stmt.executeUpdate();
			returnCode = stmt.getInt(numInParameters+1);
			stmt.close();
		}
		else //SQL statements
		{
			PreparedStatement stmt = conn.prepareStatement(statementWithParamsReplaced);
			// Figure out the number of times to execute
			int count = 1;
			for (Parameter p : parameters)
			{
				if (p.getType() == ParameterType.MultiValue)
				{
					if (count == 1 || count > p.getValueList().size())
						count = p.getValueList().size();
				}
			}
			for (int i = 0; i < count; i++)
			{
				// Set all the parameters
				for (int j = 0; j < parameterSet.size(); j++)
				{
					for (Parameter p : parameters)
					{
						if (p.getName().equals(parameterSet.get(j)))
						{
							if (p.getType() == ParameterType.SingleValue)
								setStatementValue(stmt, j+1, p.getDatatype(), p.getValue());
							else if (p.getType() == ParameterType.MultiValue)
								setStatementValue(stmt, j+1, p.getDatatype(), p.getValueList().get(i));
							break;
						}
					}
				}
				stmt.addBatch();
			}
			stmt.executeBatch();
			stmt.close();
		}
		return returnCode;
	}

	/**
	 * Set the appropriate parameter in a prepared statement based on data type
	 * 
	 * @param stmt
	 * @param index
	 * @param type
	 * @param value
	 * @throws SQLException
	 */
	private void setStatementValue(PreparedStatement stmt, int index, int type, Object value) throws SQLException
	{
		if (type == Types.VARCHAR || type == Types.LONGVARCHAR)
		{
			stmt.setString(index, value.toString());
		} else if (type == Types.DOUBLE)
		{
			if (Number.class.isInstance(value))
			{
				stmt.setDouble(index, ((Number) value).doubleValue());
			} else
			{
				try
				{
					double d = Double.parseDouble(value.toString());
					stmt.setDouble(index, d);
				} catch (NumberFormatException e)
				{
					stmt.setDouble(index, 0);
				}
			}
		} else if (type == Types.INTEGER)
		{
			if (Long.class.isInstance(value))
			{
				stmt.setLong(index, (Long) value);
			} else
			{
				try
				{
					long i = Long.parseLong(value.toString());
					stmt.setLong(index, i);
				} catch (NumberFormatException e)
				{
					stmt.setLong(index, 0);
				}
			}
		} else if (type == Types.TIMESTAMP)
		{
			if (Timestamp.class.isInstance(value))
			{
				stmt.setTimestamp(index, (Timestamp) value);
			} else if (java.sql.Date.class.isInstance(value))
			{
				stmt.setDate(index, (java.sql.Date) value);
			} else if (java.sql.Time.class.isInstance(value))
			{
				stmt.setTime(index, (java.sql.Time) value);
			} else
			{
				stmt.setTimestamp(index, null);
			}
		}
	}
	
	public Element getUpdateStatementElement(Namespace ns)
	{
		Element e = new Element("UpdateStatement",ns);
		
		Element child = new Element("Name",ns);
		child.setText(Name);
		e.addContent(child);
		
		child = new Element("ActionTypeName",ns);
		child.setText(ActionTypeName);
		e.addContent(child);
		
		child = new Element("ImageIndex",ns);
		child.setText(String.valueOf(ImageIndex));
		e.addContent(child);
		
		child = new Element("Statement",ns);
		child.setText(Statement);
		e.addContent(child);
		
		child = new Element("StoredProcedure",ns);
		child.setText(String.valueOf(StoredProcedure));
		e.addContent(child);
		
		return e;
	}
}
