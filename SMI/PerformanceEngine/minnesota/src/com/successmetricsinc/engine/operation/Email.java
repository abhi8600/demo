package com.successmetricsinc.engine.operation;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Map;

import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.User;
import com.successmetricsinc.query.ResultSetCache;

public class Email extends Action implements Serializable {
	
	public String From;
    public String To;
    public String Subject;
    public String Body;
    public String Report;
    public String MailHost;
	
	public Email(Element e,Namespace ns)
	{
		ActionTypeName = e.getChildText("ActionTypeName",ns);
		
		String s = e.getChildText("ImageIndex",ns);
		if (s!=null)
		{
			ImageIndex = Integer.parseInt(s);
		}
		
		Name = e.getChildText("Name",ns);
		
		From = e.getChildText("From",ns);
		To = e.getChildText("To",ns);
		Subject = e.getChildText("Subject",ns);
		Body = e.getChildText("Body",ns);
		Report = e.getChildText("Report",ns);
		MailHost = e.getChildText("MailHost",ns);
	}
	
	public Element getEmailElement(Namespace ns)
	{
		Element e = new Element("Email",ns);
		
		Element child = new Element("Name",ns);
		child.setText(Name);
		e.addContent(child);
		
		child = new Element("ActionTypeName",ns);
		child.setText(ActionTypeName);
		e.addContent(child);
		
		child = new Element("ImageIndex",ns);
		child.setText(String.valueOf(ImageIndex));
		e.addContent(child);	
		
		child = new Element("From",ns);
		child.setText(From);
		e.addContent(child);
		
		child = new Element("To",ns);
		child.setText(To);
		e.addContent(child);
		
		child = new Element("Subject",ns);
		child.setText(Subject);
		e.addContent(child);
		
		child = new Element("Body",ns);
		child.setText(Body);
		e.addContent(child);
		
		child = new Element("Report",ns);
		child.setText(Report);
		e.addContent(child);
		
		child = new Element("MailHost",ns);
		child.setText(MailHost);
		e.addContent(child);
		
		return e;
	}

	@Override
	protected int execute(Connection conn, User user,
			Map<String, String> servletParameterMap, Repository r,
			Session session, ResultSetCache cache, Parameter[] parameters)
			throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

}
