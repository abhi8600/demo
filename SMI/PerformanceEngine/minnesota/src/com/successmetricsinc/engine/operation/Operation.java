/**
 * $Id: Operation.java,v 1.27 2011-09-26 19:03:48 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine.operation;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.User;
import com.successmetricsinc.query.ResultSetCache;

/**
 * Class to encapsulate transactional operations
 * 
 * @author Brad Peters
 * 
 */
public class Operation implements Cloneable, Serializable
{
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(Operation.class);
	private String Name;
	private Parameter[] Parameters;
	private Action[] Actions;

	private Map<String, String> servletParameterMap;

	private User user = null;
	
	@SuppressWarnings("unused")
	private Operation()
	{
	}
	
	public Element getOperationElement(Namespace ns)
	{
		Element e = new Element("Operation",ns);
		
		Element child = new Element("Name",ns);
		child.setText(Name);
		e.addContent(child);
		
		if (Parameters!=null && Parameters.length>0)
		{
			child = new Element("Parameters",ns);
			for (Parameter param : Parameters)
			{
				child.addContent(param.getParameterType(ns));
			}
			e.addContent(child);
		}
		
		if (Actions!=null && Actions.length>0)
		{
			child = new Element("Actions",ns);
			for (Action action : Actions)
			{
				child.addContent(action.getActionElement(ns));
			}
			e.addContent(child);
		}
		
		return e;
	}

	@SuppressWarnings("rawtypes")
	public Operation(Element e, Namespace ns)
	{
		Name = e.getChildTextTrim("Name", ns);
		Element pe = e.getChild("Parameters", ns);
		if (pe != null)
		{
			List l2 = pe.getChildren();
			Parameters = new Parameter[l2.size()];
			for (int count2 = 0; count2 < l2.size(); count2++)
			{
				Parameters[count2] = new Parameter((Element) l2.get(count2), ns);
			}
		}
		Element ae = e.getChild("Actions", ns);
		if (ae != null)
		{
			List l2 = ae.getChildren();
			Actions = new Action[l2.size()];
			for (int count2 = 0; count2 < l2.size(); count2++)
			{
				Actions[count2] = Action.createAction((Element) l2.get(count2), ns);
			}
		}
		user = null;
		servletParameterMap = null;
	}

	public Object clone() throws CloneNotSupportedException
	{
		Operation op = (Operation) super.clone();
		op.Name = Name;
		op.Parameters = new Parameter[Parameters.length];
		for (int i = 0; i < Parameters.length; i++)
			op.Parameters[i] = (Parameter) Parameters[i].clone();
		op.Actions = Actions;
		op.user = user;
		op.servletParameterMap = servletParameterMap;
		return (op);
	}

	/**
	 * Set a parameter with a given name
	 * 
	 * @param name
	 *            Name of parameter
	 * @param value
	 *            Value to set parameter
	 * @return Wether a parameter with the same name and type was found and set
	 */
	public boolean setParameterValue(String name, Object value)
	{
		for (Parameter p : Parameters)
		{
			if (p.getName().equals(name) && p.getType() == Parameter.ParameterType.SingleValue)
			{
				p.setValue(value);
				return true;
			}
		}
		return false;
	}

	/**
	 * Set a parameter with a given name
	 * 
	 * @param name
	 *            Name of parameter to set
	 * @param valueList
	 *            List of parameter values
	 * @return Wether a parameter with the same name and type was found and set
	 */
	public boolean setParameterValue(String name, List<Object> valueList)
	{
		for (Parameter p : Parameters)
		{
			if (p.getName().equals(name) && p.getType() == Parameter.ParameterType.MultiValue)
			{
				p.setValueList(valueList);
				return true;
			}
		}
		return false;
	}

	/**
	 * Execute the operation with the current set of parameter values
	 * 
	 * @throws Exception
	 */
	public int execute(Repository r, ResultSetCache cache, Session session) throws Exception
	{
		int returnCode = Action.ACTION_SUCCESS;
		// Get a repository connection
		Connection conn = r.getDefaultConnection().ConnectionPool.getConnection();
		boolean auto = conn.getAutoCommit();
    	boolean committed = false;    	

    	conn.setAutoCommit(false);

    	try
    	{
			if (Actions != null)
			{
				for (Action a : Actions)
				{
					logger.debug("Excution action (" + a.toString() + ") for operation (" + this.Name + ")");
					returnCode = a.execute(conn, user, servletParameterMap, r, session, cache, Parameters);
					if(returnCode < 0)
					{
						break;
					}
				}
			}
			if(returnCode >= 0)
			{
				conn.commit();
				committed = true;
			}
    	}
    	catch(Exception e)
    	{
    		throw e;
    	}
    	finally
    	{
        	try
        	{
        		if (!committed)
        		{
        			conn.rollback();
        		}
        		conn.setAutoCommit(auto);
        	}
        	catch (Exception ex)
        	{
        		throw ex;
        	}
    	}
    	return returnCode;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return Name;
	}
	
	/*
	 * @return list of parameters used by this operation 
	 */
	public Parameter [] getParameters()
	{
		return Parameters;
	}

	public void setUser(User user)
	{
		this.user = user;
	}

	public void setServletParameterMap(Map<String, String> servletParameterMap)
	{
		this.servletParameterMap = servletParameterMap;
	}
}
