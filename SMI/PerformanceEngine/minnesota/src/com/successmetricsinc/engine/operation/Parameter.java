/**
 * $Id: Parameter.java,v 1.9 2010-12-20 12:48:04 mpandit Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine.operation;

import java.io.Serializable;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import org.jdom.Element;
import org.jdom.Namespace;
import com.thoughtworks.xstream.XStream;

/**
 * Class describing a parameter to a transactional operation
 * 
 * @author Brad Peters
 * 
 */
public class Parameter implements Cloneable, Serializable
{
	private static final long serialVersionUID = 1L;
	private String Name;
	private int Datatype;
	private Object Value;
	private List<Object> ValueList;
	
	private String dataTypeStr;
	private String parameterTypeStr;

	public enum ParameterType
	{
		SingleValue, MultiValue
	};
	private ParameterType Type;

	public Parameter()
	{
	}

	public Parameter(Element e, Namespace ns)
	{
		Name = e.getChildTextTrim("Name", ns);		
		dataTypeStr = e.getChildTextTrim("Datatype", ns);
		if (dataTypeStr.equals("Varchar"))
			Datatype = Types.VARCHAR;
		else if (dataTypeStr.equals("Number"))
			Datatype = Types.DOUBLE;
		else if (dataTypeStr.equals("Integer"))
			Datatype = Types.INTEGER;
		else if (dataTypeStr.equals("DateTime"))
			Datatype = Types.TIMESTAMP;
		parameterTypeStr = e.getChildTextTrim("Type", ns);
		if (parameterTypeStr.equals("SingleValue"))
			Type = ParameterType.SingleValue;
		else if (parameterTypeStr.equals("MultiValue"))
			Type = ParameterType.MultiValue;
	}
	
	public Element getParameterType(Namespace ns)
	{
		Element e = new Element("Parameter",ns);
		
		Element child = new Element("Name",ns);
		child.setText(Name);
		e.addContent(child);
		
		child = new Element("Datatype",ns);
		child.setText(dataTypeStr);
		e.addContent(child);
		
		child = new Element("Type",ns);
		child.setText(parameterTypeStr);
		e.addContent(child);
		
		return e;
	}

	public Object clone() throws CloneNotSupportedException
	{
		Parameter p = (Parameter) super.clone();
		p.Name = Name;
		p.Datatype = Datatype;
		p.Value = Value;
		if(ValueList == null)
		{
			p.ValueList = null;
		}
		else
		{
			p.ValueList = new ArrayList<Object>(ValueList);
		}
		p.Type = Type;
		return (p);
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return Name;
	}

	/**
	 * @return the value
	 */
	public Object getValue()
	{
		return Value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(Object value)
	{
		Value = value;
	}

	/**
	 * @return the valueList
	 */
	public List<Object> getValueList()
	{
		return ValueList;
	}

	/**
	 * @param valueList
	 *            the valueList to set
	 */
	public void setValueList(List<Object> valueList)
	{
		ValueList = valueList;
	}

	/**
	 * @return the type
	 */
	public ParameterType getType()
	{
		return Type;
	}

	/**
	 * @return the datatype
	 */
	public int getDatatype()
	{
		return Datatype;
	}
	
	public String getValueXML()
	{
        XStream xs = new XStream();
        xs.alias("value", String.class);        
        return xs.toXML(this.getValueList().toArray());
	}
}
