/**
 * $Id: MeasureBucket.java,v 1.7 2010-12-20 12:48:07 mpandit Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.io.Serializable;

import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.util.XmlUtils;

/**
 * @author Brad Peters
 * 
 */
public class MeasureBucket implements Serializable
{
	private static final long serialVersionUID = 1L;
	public String Name;
	public String Min;
	public Double dmin;
	public String Max;
	public Double dmax;

	public MeasureBucket(Element e, Namespace ns)
	{
		Name = e.getChildTextTrim("Name", ns);
		Min = e.getChildTextTrim("Min", ns);
		
		if (Min != null && !Min.equals("")) {
			try {
				dmin = Double.parseDouble(Min);
			} catch (NumberFormatException nex) {
				; //ignore ?
			}
		}
		Max = e.getChildTextTrim("Max", ns);
		if (Max != null && !Max.equals(""))
			dmax = Double.parseDouble(Max);
	}

	public String toString()
	{
		return (this.Name);
	}
	
	public Element getMeasureBucketElement(Namespace ns)
	{
		Element e = new Element("MeasureBucket",ns);
		
		XmlUtils.addContent(e, "Name", Name,ns);
		
		XmlUtils.addContent(e, "Min", Min,ns);
		
		XmlUtils.addContent(e, "Max", Max,ns);	
				
		return e;
	}
}