/**
 * $Id: FindRules.java,v 1.18 2009-10-07 22:33:50 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.HashSet;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.log4j.Logger;
import weka.associations.Apriori;
import weka.associations.ItemSet;
import weka.core.FastVector;
import weka.core.Instances;
import weka.core.Utils;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Discretize;
import weka.filters.unsupervised.instance.RemovePercentage;
import com.successmetricsinc.Repository;

/**
 * @author bpeters
 * 
 */
public class FindRules
{
	private static Logger logger = Logger.getLogger(FindRules.class);
	SuccessModelInstance smi;
	Apriori a;
	private static final String OFILE = "C:\\TEST.BIN";
	// Maximum number of instances that can process (depends on memory)
	private static final int MAX_INSTANCES = 20000;

	public FindRules(SuccessModelInstance smi)
	{
		this.smi = smi;
	}

	public void getRules() throws Exception
	{
		System.out.println("");
		List<Integer> rowList = new ArrayList<Integer>();
		for (int i = 0; i < smi.mds.numRows(); i++)
			rowList.add(Integer.valueOf(i));
		List<Integer> columnList = new ArrayList<Integer>();
		for (int i = 1; i < smi.mds.numHeaders(); i++)
			columnList.add(Integer.valueOf(i));
		Instances dataSet = smi.constructInstances("Rules", rowList, columnList, false, false, null, false, null, null);
		if (dataSet.numInstances() > MAX_INSTANCES)
		{
			RemovePercentage rp = new RemovePercentage();
			rp.setPercentage(100 - (100 * MAX_INSTANCES) / dataSet.numInstances());
			rp.setInputFormat(dataSet);
			dataSet = Filter.useFilter(dataSet, rp);
		}
		/* Discretize values */
		Discretize d = new Discretize();
		int[] attList = new int[dataSet.numAttributes() - 1];
		for (int i = 1; i < dataSet.numAttributes(); i++)
		{
			attList[i - 1] = i;
		}
		d.setAttributeIndicesArray(attList);
		d.setInputFormat(dataSet);
		d.setBins(3);
		d.setUseEqualFrequency(true);
		dataSet = Filter.useFilter(dataSet, d);
		for (int i = 1; i < dataSet.numAttributes(); i++)
		{
			if (dataSet.attribute(i).name().equals("# Repeat Work Orders"))
				dataSet.deleteAttributeAt(i);
		}
		/* Discretize Class if Needed */
		if (dataSet.attribute(0).isNumeric())
		{
			attList = new int[1];
			attList[0] = 0;
			d.setAttributeIndicesArray(attList);
			d.setInputFormat(dataSet);
			d.setBins(2);
			d.setMakeBinary(true);
			dataSet = Filter.useFilter(dataSet, d);
		}
		a = new Apriori();
		a.setCar(true);
		a.setClassIndex(0);
		a.setNumRules(Integer.MAX_VALUE);
		/* Only mine for class rules that aren't the base(first) class */
		a.setCarNonbase(true);
		/* Set upper bound on support - i.e. rules must be sufficiently rare/interesting */
		a.setUpperBoundMinSupport(0.2);
		/* Set lower bound on suppoert - i.e. rules must be common enough to be relevant */
		a.setLowerBoundMinSupport(0.0025);
		/* Set minimum significance - i.e. how correct are these rules */
		System.out.println("Building Rules for Model Dataset");
		/* Set the minimum confidence level (minMetric) for the rules */
		a.setMinMetric(0.45);
		/* Set the parallelization */
		a.setNumThreads(smi.r.getModelParameters().MaxComputationThreads);
		// Unload model data set - clear memory as not needed
		smi.mds = null;
		a.buildAssociations(dataSet);
		System.out.println("Dataset Size: " + dataSet.numInstances());
		System.out.println("Number of Rules: " + a.getAllTheRules()[0].size());
		System.out.println("\nBest rules found:\n\n");
		for (int i = 0; i < a.getAllTheRules()[0].size() && i < 1000; i++)
		{
			System.out.println(Utils.doubleToString((double) i + 1, (int) (Math.log(a.getNumRules()) / Math.log(10) + 1), 0) + ". "
					+ ((ItemSet) a.getAllTheRules()[0].elementAt(i)).toString(a.getInstancesNoClass()) + " ==> "
					+ ((ItemSet) a.getAllTheRules()[1].elementAt(i)).toString(a.getInstancesOnlyClass()) + "    conf:("
					+ Utils.doubleToString(((Double) a.getAllTheRules()[2].elementAt(i)).doubleValue(), 2) + ")");
		}
	}

	public void saveRules() throws IOException
	{
		File ofile = new File(OFILE);
		FileOutputStream fo = new FileOutputStream(ofile);
		ObjectOutput s = new ObjectOutputStream(fo);
		s.writeObject(a);
		s.close();
	}

	public void getSavedRules() throws IOException, ClassNotFoundException
	{
		FileInputStream in = new FileInputStream(OFILE);
		ObjectInputStream s = new ObjectInputStream(in);
		a = (Apriori) s.readObject();
		in.close();
	}

	public void countRuleFrequency() throws IOException, ClassNotFoundException
	{
		/* Count the number of rules that apply to each instance */
		int[] counts = new int[a.getAllTheRules()[0].size()];
		int maxCount = 0;
		for (int i = 0; i < a.getInstancesOnlyClass().numInstances(); i++)
		{
			int count = 0;
			for (int j = 0; j < a.getAllTheRules()[0].size(); j++)
			{
				if (((ItemSet) a.getAllTheRules()[0].elementAt(j)).containedBy(a.getInstancesNoClass().instance(i)))
				{
					count++;
				}
			}
			counts[count]++;
			if (count > maxCount)
				maxCount = count;
		}
		for (int i = 1; i <= maxCount; i++)
		{
			System.out.println(counts[i] + " instances satisfy " + i + " rule" + (i > 1 ? "s" : ""));
		}
	}
	private HashSet<Integer> pickedRules;
	private List<Integer>[] applyList;
	private FastVector rules;
	private FastVector confidences;
	private int[] ruleCounts;
	int numberClassItemsCovered = 0;
	AtomicInteger classCount;

	/**
	 * Prune rules to find the most interesting ones. In many cases an instance is satisfied by more than one rule and
	 * more rules than are really necessary are generated. So, to ensure coverage, while simplifying the process, select
	 * the best rule for each instance. First, pick the one with the highest confidence Then, pick the one that is the
	 * most common (i.e. applies to the most instances)
	 * 
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public void pruneRules(Repository r) throws IOException, ClassNotFoundException
	{
		this.rules = a.getAllTheRules()[0];
		this.confidences = a.getAllTheRules()[2];
		Thread[] tlist = new Thread[r.getModelParameters().MaxComputationThreads];
		/* First, count the number of instances that apply to each rule */
		this.ruleCounts = new int[rules.size()];
		this.applyList = new ArrayList[a.getInstancesNoClass().numInstances()];
		pickedRules = new HashSet<Integer>();
		decileSize = a.getInstancesNoClass().numInstances() / 10;
		System.out.println("Pruning Rules");
		curIndex = 0;
		curDecile = 1;
		maxIndex = a.getInstancesNoClass().numInstances();
		for (int i = 0; i < r.getModelParameters().MaxComputationThreads; i++)
		{
			tlist[i] = new pruneRulesThread(false);
		}
		for (int i = 0; i < r.getModelParameters().MaxComputationThreads; i++)
		{
			tlist[i].start();
		}
		for (int i = 0; i < r.getModelParameters().MaxComputationThreads; i++)
		{
			try
			{
				tlist[i].join();
			} catch (InterruptedException e)
			{
				logger.error(e, e);
			}
		}
		System.out.println();
		/* Now, go through each instance and pick the best rule - add to a set */
		curIndex = 0;
		curDecile = 1;
		classCount = new AtomicInteger(0);
		for (int i = 0; i < r.getModelParameters().MaxComputationThreads; i++)
		{
			tlist[i] = new pruneRulesThread(true);
		}
		for (int i = 0; i < r.getModelParameters().MaxComputationThreads; i++)
		{
			tlist[i].start();
		}
		for (int i = 0; i < r.getModelParameters().MaxComputationThreads; i++)
		{
			try
			{
				tlist[i].join();
			} catch (InterruptedException e)
			{
				logger.error(e, e);
			}
		}
		/* Now, list all the picked rules */
		System.out.println();
		System.out.println("Pruned Rule List: " + pickedRules.size() + " rules covering " + numberClassItemsCovered + "/" + classCount + " instances of class");
		for (int i = 0; i < rules.size(); i++)
		{
			if (pickedRules.contains(i))
				System.out.println(Utils.doubleToString((double) i + 1, (int) (Math.log(a.getNumRules()) / Math.log(10) + 1), 0) + ". "
						+ ((ItemSet) a.getAllTheRules()[0].elementAt(i)).toString(a.getInstancesNoClass()) + " ==> "
						+ ((ItemSet) a.getAllTheRules()[1].elementAt(i)).toString(a.getInstancesOnlyClass()) + "    conf:("
						+ Utils.doubleToString(((Double) a.getAllTheRules()[2].elementAt(i)).doubleValue(), 2) + ")");
		}
	}

	private synchronized void addRule(int i)
	{
		pickedRules.add(i);
	}

	private synchronized void addRuleApplication(int i, int j)
	{
		if (applyList[i] == null)
		{
			applyList[i] = new ArrayList<Integer>();
			numberClassItemsCovered++;
		}
		applyList[i].add(j);
	}
	int curIndex = 0;
	int maxIndex = 0;
	int curDecile = 0;
	int decileSize = 0;

	private synchronized int getNextIndex()
	{
		if (curIndex < maxIndex)
		{
			if (curIndex >= curDecile * decileSize)
			{
				System.out.print(curDecile + "0% ");
				curDecile++;
			}
			return (curIndex++);
		} else
			return (-1);
	}

	private class pruneRulesThread extends Thread
	{
		private boolean phaseTwo;

		public pruneRulesThread(boolean phaseTwo)
		{
			this.setName("Prune Rules - " + this.getId());
			this.phaseTwo = phaseTwo;
		}

		public void run()
		{
			if (!phaseTwo)
			{
				int i = 0;
				while ((i = getNextIndex()) > 0)
				{
					/* Only consider rules that actually apply to valid outcomes */
					if (a.getInstancesOnlyClass().instance(i).value(0) > 0)
					{
						for (int j = 0; j < rules.size(); j++)
						{
							if (((ItemSet) rules.elementAt(j)).containedBy(a.getInstancesNoClass().instance(i)))
							{
								ruleCounts[j]++;
								addRuleApplication(i, j);
							}
						}
					}
				}
			} else
			{
				int i = 0;
				while ((i = getNextIndex()) > 0)
				{
					if (a.getInstancesOnlyClass().instance(i).value(0) > 0)
					{
						classCount.incrementAndGet();
						List<Integer> pickedList = new ArrayList<Integer>();
						if (applyList[i] != null)
						{
							double maxConfidence = 0;
							for (int j : applyList[i])
							{
								double newConf = (Double) confidences.elementAt(j);
								if (newConf == maxConfidence)
								{
									pickedList.add(j);
								} else if (newConf > maxConfidence)
								{
									pickedList.clear();
									pickedList.add(j);
									maxConfidence = newConf;
								}
							}
							if (pickedList.size() > 1)
							{
								/* Now, find the shortest rule */
								int minSize = Integer.MAX_VALUE;
								List<Integer> newPicked = new ArrayList<Integer>();
								for (int j : pickedList)
								{
									ItemSet is = (ItemSet) rules.elementAt(j);
									int count = 0;
									for (int k = 0; k < is.items().length; k++)
										if (is.items()[k] > -1)
										{
											count++;
										}
									if (count < minSize)
									{
										newPicked.clear();
										minSize = count;
										newPicked.add(j);
									} else if (count == minSize)
										newPicked.add(j);
								}
								pickedList = newPicked;
							}
							if (pickedList.size() > 1)
							{
								/* Now find the one that's the most popular */
								List<Integer> newPicked = new ArrayList<Integer>();
								int maxCount = 0;
								for (int j : pickedList)
								{
									if (ruleCounts[j] > maxCount)
									{
										newPicked.clear();
										maxCount = ruleCounts[j];
										newPicked.add(j);
									} else if (ruleCounts[j] == maxCount)
										newPicked.add(j);
								}
								pickedList = newPicked;
							}
							if (pickedList.size() > 0)
							{
								addRule(pickedList.get(0));
							}
						}
					}
				}
			}
		}
	}
}
