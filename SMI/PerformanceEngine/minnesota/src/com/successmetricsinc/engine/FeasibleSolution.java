/**
 * $Id: FeasibleSolution.java,v 1.2 2007-08-30 16:26:47 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import weka.core.Instance;

/**
 * Interface to define whether a solution is feasible or not
 * 
 * @author bpeters
 * 
 */
public interface FeasibleSolution
{
    /**
     * Return returns whether a solution is feasible or not (i.e. whether the indices chosen are feasible options)
     * 
     * @param indices
     * @return True or False whether the indices are allowable
     * @throws Exception
     */
    public boolean isFeasible(int[] indices) throws Exception;

    /**
     * Interface to make a given instance a valid one during optimization. After an instance is mutated or has gone
     * through cross-over, this interface is used to ensure that the instance conforms to any requirements (e.g. is
     * locally optimal)
     * 
     * @param inst
     *            Instance to make valid (modifies original copy)
     * @returns objective function of instance
     * @throws Exception
     */
    public double makeValidInstance(Instance inst, int[] indices) throws Exception;
}
