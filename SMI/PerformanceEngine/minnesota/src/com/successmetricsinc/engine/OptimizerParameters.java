/**
 * $Id: OptimizerParameters.java,v 1.4 2010-12-20 00:36:05 bpeters Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.io.Serializable;
import java.text.DecimalFormat;

import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.util.Util;

/**
 * Class for storing parameters for the genetic optimizer
 * @author bpeters
 * 
 */
public class OptimizerParameters implements Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int SIMPLEX_METHOD = 0;
    public static final int QUASINEWTON_METHOD = 1;
    public int Method;
    public int PopulationSize;
    public int MaxGenerations;
    public int MaxNoChangeGenerations;
    public double MutationRate;
    public double ReproductionRate;
    
    public Element getOptimizerParameter(Namespace ns)
    {    	   
		Element e = new Element("OptimizerParameters",ns);
		
		Element child = new Element("Method",ns);
		child.setText(String.valueOf(Method));
		e.addContent(child);
		
		child = new Element("PopulationSize",ns);
		child.setText(String.valueOf(PopulationSize));
		e.addContent(child);
		
		child = new Element("MaxGenerations",ns);
		child.setText(String.valueOf(MaxGenerations));
		e.addContent(child);
		
		child = new Element("MaxNoChangeGenerations",ns);
		child.setText(String.valueOf(MaxNoChangeGenerations));
		e.addContent(child);
		
		child = new Element("MutationRate",ns);
		child.setText(Util.getDecimalFormatForRepository().format(MutationRate));
		e.addContent(child);
		
		child = new Element("ReproductionRate",ns);
		child.setText(Util.getDecimalFormatForRepository().format(ReproductionRate));
		e.addContent(child);
		
		return e;
	
    }
}
