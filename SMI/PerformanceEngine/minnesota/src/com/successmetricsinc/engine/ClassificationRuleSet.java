/**
 * $Id: ClassificationRuleSet.java,v 1.8 2012-02-13 11:51:28 mpandit Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.engine.SuccessModelInstance;

/**
 * Class for encapsulating a set of rules used for rule-based classification
 * @author ricks
 *
 */
public class ClassificationRuleSet implements Cloneable, Serializable
{
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(ClassificationRuleSet.class);
	List<ClassificationRule> rules;
	
	public ClassificationRuleSet()
	{
		rules = new ArrayList<ClassificationRule>();
	}
	
	/**
	 * add a classification rule to the ruleset
	 * @param rule	a classification rule (priority, filter, explanation)
	 */
	public void addRule(ClassificationRule rule)
	{
		rules.add(rule);
	}
	
	/**
	 * get the ruleset
	 * @return List<ClassificationRules>
	 */
	public List<ClassificationRule> getRules()
	{
		return rules;
	}
	
	/*
	 * evaluate a ruleset against a row (an instance converted to an array of objects)
	 * @param row	array of columns from the instance being classified
	 */
	public ClassificationRule evaluate(Object[] row, Repository r)
	{
		for (ClassificationRule rule : rules)
		{
			if (rule.evaluate(row, r))
			{
				return rule;
			}
		}
		return null;
	}
	
	/**
	 * bind the filters in the ruleset to the data, similar to bindToData in DisplayFilter
	 * 
	 * @param smi	Success Model Instance being used for the scoring
	 * @param colList	list of columns
	 * @return	true for success, false for failure
	 */
	public boolean bindToData(SuccessModelInstance smi, List<Integer> colList) throws CloneNotSupportedException
	{
		for (ClassificationRule rule : rules)
		{
			if (!rule.bindToData(smi, colList))
			{
				logger.warn("Removed rule from the ruleset: " + rule.toString());
			}
		}
		// sort the rules in priority order
		Collections.sort(rules);
		return true;
	}
	
	/**
	 * need a clone method because these objects are used in multiple threads and there is no reason to make them thread safe
	 */
	public Object clone() throws CloneNotSupportedException
	{
		ClassificationRuleSet crs = (ClassificationRuleSet) super.clone();
		crs.rules = new ArrayList<ClassificationRule>();
		for (ClassificationRule cr : this.rules)
		{
			crs.addRule((ClassificationRule) cr.clone());
		}
		return crs;
	}
}