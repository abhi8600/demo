/**
 * $Id: OutcomeAnalysis.java,v 1.135 2012-05-21 13:22:23 mpandit Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;

import weka.classifiers.Classifier;
import weka.classifiers.functions.Logistic;
import weka.classifiers.rules.Ridor;
import weka.classifiers.trees.J48;
import weka.core.Instance;
import weka.core.Instances;

import com.successmetricsinc.Repository;
import com.successmetricsinc.engine.Header.HeaderType;
import com.successmetricsinc.engine.OutcomeFactor.SettingType;
import com.successmetricsinc.query.Database;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.status.ModelingHistory;
import com.successmetricsinc.util.WekaUtil;

/**
 * $Id: OutcomeAnalysis.java,v 1.135 2012-05-21 13:22:23 mpandit Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
/**
 * Class to analyze a Success Model Instance against an outcome definition - used to predict outcomes.
 * 
 * @author bpeters
 * 
 */
public class OutcomeAnalysis
{
	/*
	 * Class probability threshold for predicting value
	 */
	private static final double PRED_VALUE_IN_CLASS_THRESHOLD = 0.5;
	private static final int MAX_BATCH_SIZE = 50000;
	private static Logger logger = Logger.getLogger(OutcomeAnalysis.class);
	private Outcome outcome;
	private PatternSearchDefinition psd;
	private SuccessModel sm;
	private Repository r;
	private SuccessModelInstance smi;
	private DatabaseConnection dc;
	private String mdsPath;
	private String csvPath;
	private String iterationAnalysis;
	private String iterationModel;
	private static int instanceBeingProcessed = 0;
	BlockingQueue<OutputData> outputDataList;
	Map<Object, Segment> segmentMap;
	private boolean treeDumped = false;
	private static boolean error = false;
	private ModelingHistory modelingHistory;
	/**
	 * return the next instance index
	 * 
	 * @return next instance index or -1 if no more
	 */
	private synchronized int getNextInstance(int numInstances)
	{
		int next = instanceBeingProcessed++;
		if (next >= numInstances)
			return -1;
		return next;
	}

	private void setError()
	{
		error = true;
	}

	/**
	 * Instantiate the Outcome Analysis class
	 * 
	 * @param r
	 *            Repository
	 * @param conn
	 *            Valid database connection to save results to
	 * @param outcomeName
	 *            Name of Outcome to analyze
	 * @param path
	 *            Path to store results and models
	 * @throws Exception
	 */
	public OutcomeAnalysis(Repository r, String outcomeName, String iterationModel, String iterationAnalysis, String mdsPath, String csvPath,
			ModelingHistory modelingHistory, boolean useDataSetSegments) throws Exception
	{
		logger.info("Outcome Analysis for " + outcomeName + ": model=" + iterationModel + ", analysis=" + iterationAnalysis);
		this.outcome = r.findOutcome(outcomeName);
		if (this.outcome == null)
		{
			// try success pattern - build dummy outcome model - for the old gensuccessmodel stuff
			this.psd = r.findSearchDefinition(outcomeName);
			if (this.psd == null)
				throw (new Exception("Unable to locate Outcome Definition: " + outcomeName));
			logger.info("Using Pattern Search Definition: " + psd.Name);
			this.outcome = new Outcome();
			this.outcome.Name = psd.Name;
			this.outcome.SuccessModelName = psd.SuccessModelName;
			this.outcome.EstimateValue = true;
		}
		this.sm = r.findSuccessModel(outcome.SuccessModelName);
		if (sm == null)
			throw (new Exception("Unable to locate Success Model: " + outcome.SuccessModelName));
		this.smi = WekaUtil.retrieveSMI(mdsPath, sm.Name, true);
		this.smi.sm = sm;
		this.r = r;
		this.smi.r = r;
		this.dc = r.getDefaultConnection();
		this.mdsPath = mdsPath;
		this.csvPath = csvPath;
		if (this.csvPath == null || this.csvPath.length() == 0)
			this.csvPath = this.mdsPath;
		this.modelingHistory = modelingHistory;
		if (iterationModel != null)
			this.iterationModel = iterationModel.equals("All") ? null : iterationModel;
		else
			this.iterationModel = (outcome.IterationModelValue == null || outcome.IterationModelValue.length() == 0 || sm.IterateLevel.length() == 0 || smi.sm.IterateDimension
					.equals("None")) ? null : outcome.IterationModelValue;
		if (this.iterationModel != null)
			this.iterationModel = r.replaceVariables(null, this.iterationModel);
		if (iterationAnalysis != null)
			this.iterationAnalysis = iterationAnalysis.equals("All") ? null : iterationAnalysis;
		else
			this.iterationAnalysis = (outcome.IterationAnalysisValue == null || outcome.IterationAnalysisValue.length() == 0 || sm.IterateLevel.length() == 0 || smi.sm.IterateDimension
					.equals("None")) ? null : outcome.IterationAnalysisValue;
		if (this.iterationAnalysis != null)
			this.iterationAnalysis = r.replaceVariables(null, this.iterationAnalysis);
		logger.info("Modeling with iteration '" + this.iterationModel + "' and scoring with iteration '" + this.iterationAnalysis + "'");
		segmentMap = new HashMap<Object, Segment>();
		if (!retrieve())
		{
			logger.debug("model corrupt or non-existent, resetting segment map");
			// model file does not exist or is corrupt, reset segment map
			for (Segment s : smi.getSegmentMap().values())
			{
				segmentMap.put(s.getKey(), s);
				s.smi = smi;
			}
		} else if (useDataSetSegments)
		{
			logger.debug("model file exists, but using MDS row index list for segments");
			// use the row index lists from the dataset, but the models from the model :)
			for (Segment s : smi.getSegmentMap().values())
			{
				segmentMap.get(s.getKey()).rowIndexList = s.getRowIndexList();
			}
		}
	}

	/*
	 * determine if any models have been generated for any segments in the map XXX not sure that this is sufficient
	 */
	public boolean generatedModels()
	{
		for (Segment s : segmentMap.values())
		{
			if (s.getSegmentValueClassifier() != null || s.getSegmentClassClassifier() != null || s.getSegmentValueForClassClassifier() != null)
				return (true);
		}
		return (false);
	}

	/**
	 * Generate models necessary for outcome analysis
	 * 
	 * @param overwrite
	 *            Overwrite existing models if they already exist
	 * @throws Exception
	 */
	public boolean generateModels(boolean overwrite) throws Exception
	{
		logger.debug("generateModels(" + overwrite + ")");
		String targetMeasure = outcome.TargetMeasure == null ? smi.sm.Measure : outcome.TargetMeasure;
		if (overwrite || !generatedModels())
		{
			// to be safe, reset the segments from the base files
			for (Segment s : smi.getSegmentMap().values())
			{
				segmentMap.put(s.getKey(), s);
				s.smi = smi;
			}
			AttributeSearch as = new AttributeSearch(smi, segmentMap, targetMeasure, outcome.ClassMeasure);
			as.setNameFilter(outcome.NameFilter);
			smi.mds.setPartition(0);
			if (!as.generateSegmentAttributeLists())
			{
				logger.error("Attribute selection failed");
				return false;
			}
		}
		Segment seg = segmentMap.values().iterator().next();
		boolean save = false;
		// If classifiers aren't built, build them
		ModelParameters mp = r.getModelParameters();
		if (outcome.EstimateClass && (overwrite || seg.getSegmentClassClassifier() == null))
		{
			logger.debug("generateModels: building classifiers for class");
			BuildClassifiers bc = new BuildClassifiers(smi, outcome, mp, segmentMap);
			bc.setBiasToUniformClass(outcome.BiasToUniformClass);
			if (iterationModel != null)
			{
				bc.setIterationValues(new ArrayList<Object>(Arrays.asList(iterationModel.split(","))));
			}
			bc.buildClassifiers(false, true, outcome.EstimateValueInClass && !outcome.EstimateValue, targetMeasure, outcome.ClassMeasure);
			if (modelingHistory != null)
			{
				modelingHistory.logResults(r, seg.getSegmentClassClassifier(), getAttributes(smi, seg.getSelectedClassColumnIndexList()));
				if (outcome.EstimateValueInClass && !outcome.EstimateValue)
				{
					modelingHistory.logResults(r, seg.getSegmentValueForClassClassifier(), getAttributes(smi, seg.getSelectedColumnIndexList()));
				}
			}
			seg.setColumnHeaders(smi.mds.getHeaders());
			save = true;
		}
		if (outcome.EstimateValue && (overwrite || seg.getSegmentValueClassifier() == null))
		{
			logger.debug("generateModels: building classifiers for value");
			BuildClassifiers bc = new BuildClassifiers(smi, outcome, mp, segmentMap);
			bc.setIncludeAllNonzero(outcome.IncludeAllNonzero);
			if (outcome.RankFactors)
				bc.setFactors(outcome.Factors);
			if (iterationModel != null)
			{
				bc.setIterationValues(new ArrayList<Object>(Arrays.asList(iterationModel.split(","))));
			}
			bc.buildClassifiers(true, false, false, targetMeasure, null);
			if (modelingHistory != null)
				modelingHistory.logResults(r, seg.getSegmentValueClassifier(), getAttributes(smi, seg.getSelectedColumnIndexList()));
			seg.setColumnHeaders(smi.mds.getHeaders());
			save = true;
		}
		if (save)
			save();
		logger.debug("Finished Generating Models");
		return true;
	}

	private List<String> getAttributes(SuccessModelInstance smi, List<Integer> list)
	{
		List<String> res = new ArrayList<String>(list.size());
		for (Integer attr : list)
		{
			res.add(smi.mds.getHeader(attr).Name);
		}
		return res;
	}

	private static String createOutcomeFilename(String smiFilename, String outcomeName)
	{
		int lastDot = smiFilename.lastIndexOf('.');
		String filePrefix = smiFilename.substring(0, lastDot);
		String fileSuffix = smiFilename.substring(lastDot + 1);
		String fileName = filePrefix + "_" + outcomeName + "." + fileSuffix;
		return fileName;
	}

	public SuccessModelInstance getSuccessModelInstance()
	{
		return smi;
	}

	public Outcome getOutcome()
	{
		return outcome;
	}

	private void save() throws IOException
	{
		if (this.psd != null) // backwards compat for gensuccessmodel
		{
			for (Segment s : segmentMap.values())
			{
				s.sampleInstances = null;
				s.classSampleInstances = null;
			}
			smi.segmentMap = segmentMap;
			WekaUtil.saveSMI(smi);
			return;
		}
		// Create file for saving data set
		String fileName = createOutcomeFilename(smi.getFileName(), outcome.Name);
		logger.info("Writing outcome segment models to file: " + fileName);
		for (Segment s : segmentMap.values())
		{
			s.sampleInstances = null;
			s.classSampleInstances = null;
		}
		File file = new File(fileName);
		File directory = file.getParentFile();
		File tempFile = File.createTempFile("OutcomeFile", null, directory);
		logger.info("Writing model to temporary file " + tempFile.getName());
		FileOutputStream fo = new FileOutputStream(tempFile);
		ObjectOutput s = new ObjectOutputStream(fo);
		s.writeObject(segmentMap);
		s.close();
		fo.close();
		logger.info("Renaming temporary file to " + fileName);
		File f2 = new File(fileName);
		f2.delete();
		if (tempFile.renameTo(f2))
		{
			tempFile.delete();
		} else
		{
			logger.error("Rename of temporary file failed, keeping temporary file around");
		}
	}

	@SuppressWarnings("unchecked")
	private boolean retrieve()
	{
		String fname = createOutcomeFilename(smi.getFileName(), outcome.Name);
		logger.info("Reading file " + fname);
		try
		{
			File f = new File(fname);
			if (!f.exists())
				return (false);
			FileInputStream in = new FileInputStream(fname);
			ObjectInputStream s = new ObjectInputStream(in);
			segmentMap = (Map<Object, Segment>) s.readObject();
			s.close();
			in.close();
			smi.setInstanceDir(mdsPath);
			return (true);
		} catch (Exception e)
		{
			logger.warn("Error in reading in the outcome file (clearing the segment map): " + fname + " - " + e.getMessage(), e);
			segmentMap = new HashMap<Object, Segment>();
			return false;
		}
	}

	/**
	 * Method to score outcomes
	 * 
	 * @param writeTable
	 *            Write results to database table
	 * @param writeCSVFile
	 *            Write results to a CSV file
	 * @throws Exception
	 */
	public void scoreOutcomes(boolean writeTable, boolean writeCSVFile, boolean bulkLoad) throws Exception
	{
		int numThreads = r.getModelParameters().MaxComputationThreads;
		if (numThreads > 2)
			numThreads--; // make sure we have room for the scoring output thread
		// fetch the model
		if (!generateModels(false))
		{
			logger.error("Model Generation failed");
			return;
		}
		String iterationString = r.replaceVariables(null, outcome.IterationAnalysisValue);
		for (Segment seg : segmentMap.values())
		{
			logger.info("Scoring Outcomes for " + outcome.Name + ": Success Model=" + smi.sm.Name + ", Segment=" + seg.getKey());
			outputDataList = new LinkedBlockingQueue<OutputData>();
			// create db/file thread
			OutputThread outputThread = new OutputThread(outputDataList, writeCSVFile, writeTable, bulkLoad, dc, outcome, csvPath, outcome.Name, seg.getKey()
					.toString(), iterationString, r);
			// start the output thread
			logger.info("Spawning 1 output thread for scoring the outcomes");
			outputThread.start();
			int start = 0;
			int stop = 0;
			boolean newPartition = true;
			while (stop < smi.mds.numRows())
			{
				// set start and stop
				start = stop;
				stop += MAX_BATCH_SIZE;
				// Don't go past the end of the dataset
				if (stop > smi.mds.numRows())
					stop = smi.mds.numRows();
				/*
				 * Stop at partition boundaries - otherwise you will see thrashing across the boundary. Get the first
				 * cell from the starting row - ensures that the proper partition is loaded and hence the appropriate
				 * last index is returned
				 */
				if (newPartition)
				{
					smi.mds.setPartition(start);
					newPartition = false;
				}
				int lastStop = smi.mds.getLastIndexOfCurrentPartition() + 1;
				if (lastStop > 1 && start != lastStop && stop > lastStop)
				{
					stop = lastStop;
					newPartition = true;
				}
				scoreOutcomesLoop(seg, start, stop - start, numThreads);
			}
			finishOutput(); // signal the output thread to commit the transaction/close the file
			// wait for the output to finish
			outputThread.join();
			// close down last partition file
			smi.mds.closeCurrentPartition();
			// handle exceptions down below
			if (error)
			{
				logger.error("Failure in the scoring");
				throw new Exception("Scoring failed");
			}
			logger.info("Outcome Scored for Success Model " + smi.sm.Name + ", Segment: " + seg.getKey());
		}
	}

	private void scoreOutcomesLoop(Segment seg, int start, int length, int numThreads) throws Exception
	{
		String targetMeasure = (outcome.TargetMeasure != null && outcome.TargetMeasure.length() > 0) ? outcome.TargetMeasure : smi.sm.Measure;
		logger.info("Scoring rows " + start + " through " + (start + length));
		// Data sets
		Instances classDataSet = null;
		Instances valueDataSet = null;
		Instances actualValuesDataSet = null;
		int numInstances = 0;
		List<Object> iterations = null;
		if (iterationAnalysis != null)
			iterations = new ArrayList<Object>(Arrays.asList(iterationAnalysis.split(",")));
		String segKey = seg.getKey().toString();
		List<Integer> rowIndexList = seg.getRowIndexList();
		List<Header> colHeaders = seg.getColumnHeaders();
		List<Integer> actualRowIndexList = null;
		if (outcome.EstimateValue)
		{
			actualRowIndexList = new ArrayList<Integer>(rowIndexList.size());
			valueDataSet = smi.getScoringDataSet(segKey, rowIndexList, seg.getSelectedColumnIndexList(), false, targetMeasure, colHeaders, iterations,
					outcome.ScoringFilter, start, length, actualRowIndexList);
			valueDataSet.setClassIndex(0);
			numInstances = valueDataSet.numInstances();
		}
		if (outcome.EstimateClass)
		{
			actualRowIndexList = new ArrayList<Integer>(rowIndexList.size());
			classDataSet = smi.getScoringDataSet(segKey, rowIndexList, seg.getSelectedClassColumnIndexList(), true, outcome.ClassMeasure, colHeaders,
					iterations, outcome.ScoringFilter, start, length, actualRowIndexList);
			classDataSet.setClassIndex(0);
			numInstances = classDataSet.numInstances();
			if (outcome.EstimateValueInClass && !outcome.EstimateValue)
			{
				/*
				 * Construct a value data set with no instances in it. Calling construct instances with equal start/stop
				 * returns a blank dataset.
				 */
				valueDataSet = smi.getScoringDataSet(segKey, rowIndexList, seg.getSelectedColumnIndexList(), false, targetMeasure, colHeaders, null,
						outcome.ScoringFilter, start, 0, null);
				valueDataSet.setClassIndex(0);
			}
			/*
			 * Create a dataset of the actual values - create column list with just the actual value column(0) in it
			 */
			List<Integer> valColList = new ArrayList<Integer>();
			actualValuesDataSet = smi.getScoringDataSet(segKey, rowIndexList, valColList, false, targetMeasure, colHeaders, iterations, outcome.ScoringFilter,
					start, length, null);
		}
		logger.info("Number of instances to score = " + numInstances);
		int[] factorIndices = null;
		double[] averages = null;
		if (outcome.EstimateValue && outcome.RankFactors)
		{
			outcome.NumFactorsToRank = outcome.NumFactorsToRank == 0 ? outcome.Factors.length : outcome.NumFactorsToRank;
			factorIndices = Outcome.getFactorIndices(valueDataSet, outcome.Factors);
			averages = new double[outcome.Factors.length];
			int cnt = 0;
			for (int j = 0; j < outcome.Factors.length; j++)
			{
				// ignore factors that have both base and test as current
				OutcomeFactor of = outcome.Factors[j];
				if ((of.getBase() == SettingType.Current) && (of.getTest() == SettingType.Current))
				{
					factorIndices[j] = -1;
				}
				if (factorIndices[j] >= 0)
				{
					cnt++;
					averages[j] = valueDataSet.meanOrMode(factorIndices[j]);
					logger.debug("average[" + outcome.Factors[j].toString() + "] = " + averages[j]);
				}
			}
			logger.info("Using " + cnt + " out of a possible " + outcome.Factors.length + " factors");
		}
		instanceBeingProcessed = 0;
		logger.info("Spawning " + numThreads + " processing threads");
		ScoringThread scoring[] = new ScoringThread[numThreads];
		for (int t = 0; t < numThreads; t++)
		{
			// classifiers are not reentrant, make copies for each thread
			Classifier cVal = null;
			BestFitResult bfVal = seg.getSegmentValueClassifier();
			if (bfVal != null)
			{
				cVal = bfVal.getClassifierCopy();
			}
			Classifier cCls = null;
			BestFitResult bfCls = seg.getSegmentClassClassifier();
			if (bfCls != null)
			{
				cCls = bfCls.getClassifierCopy();
			}
			Classifier cValForCls = null;
			BestFitResult bfValForCls = seg.getSegmentValueForClassClassifier();
			if (bfValForCls != null)
			{
				cValForCls = bfValForCls.getClassifierCopy();
			}
			scoring[t] = new ScoringThread(outputDataList, cVal, cCls, cValForCls, seg, numInstances, classDataSet, valueDataSet, actualValuesDataSet,
					actualRowIndexList, factorIndices, averages);
			scoring[t].start();
		}
		// wait for scoring threads to finish
		for (int t = 0; t < numThreads; t++)
		{
			scoring[t].join();
		}
		if (classDataSet != null)
			classDataSet.delete();
		if (valueDataSet != null)
			valueDataSet.delete();
		if (actualValuesDataSet != null)
			actualValuesDataSet.delete();
		classDataSet = null;
		valueDataSet = null;
		actualValuesDataSet = null;
	}

	private void finishOutput() throws Exception
	{
		OutputData data = new OutputData();
		data.done = true; // done signal
		outputDataList.put(data);
	}

	/**
	 * data structure used for passing data to the output thread from the scoring thread
	 * 
	 * @author Rick Spickelmier
	 */
	private static class OutputData
	{
		boolean done;
		String key;
		String iteratorKeyList;
		double actual;
		double predClass;
		double predVal;
		String bucketList;
		int[] factorRanks;
		double[] actualValues;
		double[] baseValues;
		double[] testValues;
		double[] impactValues;
		String explanation;
	}

	/**
	 * thread that handles writing the results to a table and/or file
	 * 
	 * @author Rick Spickelmier
	 */
	private class OutputThread extends Thread
	{
		private final BlockingQueue<OutputData> queue;
		private boolean writeCSVFile = true;
		private boolean writeTable = false;
		private boolean bulkLoad = false;
		private PrintWriter writer = null;
		private DatabaseConnection dc = null;
		private Connection dconn = null;
		private String pstmtText = null;
		private PreparedStatement pstmt = null;
		private Outcome outcome;
		private String name = null;
		private String csvFile = null;
		private String key = null;
		private String iterationString = null;
		private Repository r = null;
		private boolean initialized = false;
		private int count = 0;
		private int EXPLANATION_LENGTH = 200;

		public OutputThread(BlockingQueue<OutputData> q, boolean p_writeCSVFile, boolean p_writeTable, boolean p_bulkLoad, DatabaseConnection p_dc,
				Outcome p_outcome, String p_csvPath, String p_name, String p_key, String p_iterationString, Repository p_repository)
		{
			this.setName("Scoring Output - " + this.getId());
			queue = q;
			writeCSVFile = p_writeCSVFile;
			writeTable = p_writeTable;
			bulkLoad = p_bulkLoad;
			dc = p_dc;
			outcome = p_outcome;
			name = p_name;
			key = p_key;
			iterationString = p_iterationString;
			r = p_repository;
			csvFile = p_csvPath + File.separator + name + key + " - scoring.txt";
		}
		private int BATCH_SIZE = 1000;
		private List<OutputData> dlist = new ArrayList<OutputData>(BATCH_SIZE);

		public void run()
		{
			try
			{
				// loop until we get a done message
				while (true)
				{
					OutputData data = (OutputData) queue.take();
					if (!initialized)
					{
						// create the table/file and header information
						initOutput(data);
						initialized = true;
					}
					if (data.done == true) // no more data to process, close things up
					{
						finishOutput();
						logger.info(count + " rows scored and output");
						return;
					}
					addToOutput(data);
					dlist.add(data);
					if (writeTable && (count % BATCH_SIZE) == 0)
					{
						// execute batch every BATCH_SIZE rows
						try
						{
							pstmt.executeBatch();
							pstmt.clearBatch();
						} catch (SQLException ex)
						{
							/*
							 * Retry if lost the connection
							 */
							logger.warn("Lost connection to the database, reconnecting");
							logger.debug(ex.getMessage(), ex);
							dconn = dc.ConnectionPool.getConnection();
							dconn.setAutoCommit(false);
							pstmt = dconn.prepareStatement(pstmtText);
							for (OutputData retryData : dlist)
								addToOutput(retryData);
							pstmt.executeBatch();
							pstmt.clearBatch();
						}
						dlist.clear();
					}
					if (count % 50000 == 0)
						logger.trace(count + " rows processed");
				}
			} catch (Exception ex)
			{
				logger.error("Exception in scoring output thread: " + ex.toString(), ex);
				setError();
				return;
			}
		}

		/**
		 * commit the transaction and/or close the file
		 * 
		 * @throws Exception
		 */
		private void finishOutput() throws Exception
		{
			// close down the output file/table
			if (bulkLoad || writeCSVFile)
			{
				writer.close();
			}
			if (bulkLoad)
			{
				String bulkInsert = "BULK INSERT " + outcome.OutputTableName + " FROM '" + csvFile + "' WITH (FIRSTROW=2)";
				logger.debug(bulkInsert);
				Statement stmt = null;
				try
				{
					stmt = dconn.createStatement();
					stmt.executeUpdate(bulkInsert);
					logger.info("Bulk Insert Successful");
				} catch (Exception e)
				{
					logger.error(e.getMessage(), e);
				}
				finally
				{
					try
					{
						if (stmt != null)
							stmt.close();
					} catch (SQLException e)
					{
						logger.error(e);
					}
				}
			}
			if (writeTable)
			{
				// execute the remaining items in the batch and then commit
				pstmt.executeBatch();
				pstmt.close();
				dconn.commit();
				dconn.setAutoCommit(true);
			}
			if (bulkLoad || writeTable)
			{
				Database
						.createIndex(r.getDefaultConnection(), r.getResource("Outcome Target ID").getValue(), outcome.OutputTableName);
				Database.createIndex(r.getDefaultConnection(), r.getResource("Outcome Iteration ID").getValue(),
						outcome.OutputTableName);
			}
			return;
		}

		/**
		 * take a single result and output to the table and/or file
		 * 
		 * @param data
		 *            data to output
		 * @throws SQLException
		 * 
		 * @throws Exception
		 */
		private void addToOutput(OutputData data) throws SQLException
		{
			count++;
			if (bulkLoad || writeCSVFile)
			{
				writer.print(outcome.Name + "\t" + data.key + "\t" + data.iteratorKeyList);
				writer.print("\t");
				if (!Double.isNaN(data.actual))
					writer.print(data.actual);
				writer.print("\t");
				if (!Double.isNaN(data.predClass))
					writer.print(data.predClass);
				writer.print("\t");
				if (!Double.isNaN(data.predVal))
					writer.print(data.predVal);
				writer.print("\t");
				if (outcome.Buckets != null)
				{
					if (data.bucketList != null)
						writer.print(data.bucketList);
				}
				writer.print("\t");
				if (r.getModelParameters().UseOnlyExplanableClassificationModels)
				{
					if (data.explanation != null)
						writer.print(data.explanation);
				}
				if (data.factorRanks != null)
				{
					// factor name
					for (int j = 0; j < outcome.NumFactorsToRank; j++)
					{
						writer.print("\t");
						if (data.factorRanks[j] >= 0)
							writer.print(outcome.Factors[data.factorRanks[j]].toString());
					}
					// actual value of the factor
					if (outcome.OutputActuals)
						for (int j = 0; j < outcome.NumFactorsToRank; j++)
						{
							writer.print("\t");
							if (data.factorRanks[j] >= 0)
								writer.print(data.actualValues[data.factorRanks[j]]);
						}
					if (outcome.OutputBaseAndTestValues)
					{
						// base value used in the test
						for (int j = 0; j < outcome.NumFactorsToRank; j++)
						{
							writer.print("\t");
							if (data.factorRanks[j] >= 0)
								writer.print(data.baseValues[data.factorRanks[j]]);
						}
						// test value used in the test
						for (int j = 0; j < outcome.NumFactorsToRank; j++)
						{
							writer.print("\t");
							if (data.factorRanks[j] >= 0)
								writer.print(data.testValues[data.factorRanks[j]]);
						}
					}
					if (outcome.OutputPredictedImpacts)
						for (int j = 0; j < outcome.NumFactorsToRank; j++)
						{
							writer.print("\t");
							if (data.factorRanks[j] >= 0)
								if (!Double.isNaN(data.impactValues[data.factorRanks[j]]))
									writer.print(data.impactValues[data.factorRanks[j]]);
						}
				}
				writer.println();
			}
			if (writeTable)
			{
				pstmt.setString(1, data.key);
				if (Double.isNaN(data.actual))
					pstmt.setNull(2, Types.DOUBLE);
				else
					pstmt.setDouble(2, data.actual);
				if (Double.isNaN(data.predClass))
				{
					pstmt.setNull(3, Types.DOUBLE);
				} else
				{
					// Need to round very small numbers to zero to avoid SQL data type conversions. Predicted class
					// probabilities can get very small (like 1E-309 which cannot be converted)
					pstmt.setDouble(3, data.predClass < 1.0E-7 ? 0 : data.predClass);
				}
				if (Double.isNaN(data.predVal))
					pstmt.setNull(4, Types.DOUBLE);
				else
					pstmt.setDouble(4, data.predVal);
				if (outcome.OutputBuckets)
					pstmt.setString(5, data.bucketList);
				else
					pstmt.setNull(5, Types.VARCHAR);
				int curCol = 6;
				if (r.getModelParameters().UseOnlyExplanableClassificationModels)
				{
					if (data.explanation != null)
					{
						if (data.explanation.length() > EXPLANATION_LENGTH)
						{
							data.explanation = data.explanation.substring(0, EXPLANATION_LENGTH);
						}
						pstmt.setString(curCol, data.explanation);
					} else
					{
						pstmt.setNull(curCol, Types.VARCHAR);
					}
					curCol++;
				}
				// if we are ranking factors, output the results - all N names, then all N values, then all N base
				// values, and then all N test values - order of columns is rank order of impact
				if (data.factorRanks != null)
				{
					for (int j = 0; j < outcome.NumFactorsToRank; j++)
					{
						if (data.factorRanks[j] >= 0)
							pstmt.setString(curCol, outcome.Factors[data.factorRanks[j]].toString());
						else
							pstmt.setNull(curCol, Types.VARCHAR);
						curCol++;
					}
					if (outcome.OutputActuals)
						for (int j = 0; j < outcome.NumFactorsToRank; j++)
						{
							if (data.factorRanks[j] >= 0)
								pstmt.setDouble(curCol, data.actualValues[data.factorRanks[j]]);
							else
								pstmt.setNull(curCol, Types.DOUBLE);
							curCol++;
						}
					if (outcome.OutputBaseAndTestValues)
					{
						for (int j = 0; j < outcome.NumFactorsToRank; j++)
						{
							if (data.factorRanks[j] >= 0)
								pstmt.setDouble(curCol, data.baseValues[data.factorRanks[j]]);
							else
								pstmt.setNull(curCol, Types.DOUBLE);
							curCol++;
						}
						for (int j = 0; j < outcome.NumFactorsToRank; j++)
						{
							if (data.factorRanks[j] >= 0)
								pstmt.setDouble(curCol, data.testValues[data.factorRanks[j]]);
							else
								pstmt.setNull(curCol, Types.DOUBLE);
							curCol++;
						}
					}
					if (outcome.OutputPredictedImpacts)
						for (int j = 0; j < outcome.NumFactorsToRank; j++)
						{
							if ((data.factorRanks[j] >= 0) && !Double.isNaN(data.impactValues[data.factorRanks[j]]))
								pstmt.setDouble(curCol, data.impactValues[data.factorRanks[j]]);
							else
								pstmt.setNull(curCol, Types.DOUBLE);
							curCol++;
						}
				} else
				{
					for (int j = 0; j < outcome.NumFactorsToRank; j++)
					{
						pstmt.setNull(curCol++, Types.VARCHAR);
						if (outcome.OutputActuals)
							pstmt.setNull(curCol++, Types.DOUBLE);
						if (outcome.OutputBaseAndTestValues)
							pstmt.setNull(curCol++, Types.DOUBLE);
						pstmt.setNull(curCol++, Types.DOUBLE);
						if (outcome.OutputPredictedImpacts)
							pstmt.setNull(curCol++, Types.DOUBLE);
					}
				}
				pstmt.addBatch();
			}
			return;
		}

		/**
		 * initialize the output (create table and/or create file and headers)
		 * 
		 * @throws Exception
		 */
		private void initOutput(OutputData data) throws Exception
		{
			if (bulkLoad || writeCSVFile)
			{
				logger.info("Writing results to file " + csvFile);
				writer = new PrintWriter(new FileWriter(csvFile));
				writer.print("OUTCOME\tID\tITERATION\tVAL\tCLASSPROB\tPREDVAL\tBUCKET\tEXPLANATION");
				if (data.factorRanks != null)
				{
					for (int j = 0; j < outcome.NumFactorsToRank; j++)
					{
						writer.print("\tFACTOR " + j);
					}
					if (outcome.OutputActuals)
						for (int j = 0; j < outcome.NumFactorsToRank; j++)
						{
							writer.print("\tFACTOR " + j + " ACTUAL");
						}
					if (outcome.OutputBaseAndTestValues)
					{
						for (int j = 0; j < outcome.NumFactorsToRank; j++)
						{
							writer.print("\tFACTOR " + j + " BASE");
						}
						for (int j = 0; j < outcome.NumFactorsToRank; j++)
						{
							writer.print("\tFACTOR " + j + " TEST");
						}
					}
					if (outcome.OutputPredictedImpacts)
						for (int j = 0; j < outcome.NumFactorsToRank; j++)
						{
							writer.print("\tFACTOR " + j + " IMPACT");
						}
				}
				writer.println();
				writer.flush();
			}
			if ((bulkLoad || writeTable) && dc != null)
			{
				logger.info("Creating outcomes table " + outcome.OutputTableName);
				try
				{
					dconn = dc.ConnectionPool.getConnection();
					List<String> outputColumns = new ArrayList<String>();
					List<String> outputColumnTypes = new ArrayList<String>();
					outputColumns.add(r.getResource("Outcome Name").getValue());
					outputColumnTypes.add("Varchar(50)");
					outputColumns.add(r.getResource("Outcome Target ID").getValue());
					outputColumnTypes.add("Varchar(30)");
					outputColumns.add(r.getResource("Outcome Iteration ID").getValue());
					outputColumnTypes.add("Varchar(30)");
					outputColumns.add(r.getResource("Outcome Actual Value").getValue());
					outputColumnTypes.add("Float");
					outputColumns.add(r.getResource("Outcome Class Probability").getValue());
					outputColumnTypes.add("Float");
					outputColumns.add(r.getResource("Outcome Predicted Value").getValue());
					outputColumnTypes.add("Float");
					outputColumns.add(r.getResource("Outcome Predicted Bucket").getValue());
					outputColumnTypes.add("Varchar(30)");
					outputColumns.add("EXPLANATION");
					outputColumnTypes.add("Varchar(" + EXPLANATION_LENGTH + ")");
					if (data.factorRanks != null)
					{
						// handle the factors
						// - if there are N factors to rank (where N is from 0 to 3
						// normally), output N factor name columns,
						// N factor value columns, N factor base value columns, and N
						// factor test value columns
						for (int j = 0; j < outcome.NumFactorsToRank; j++)
						{
							outputColumns.add(r.getResource("Outcome Factor").getValue() + j);
							outputColumnTypes.add("Varchar(100)");
						}
						if (outcome.OutputActuals)
							for (int j = 0; j < outcome.NumFactorsToRank; j++)
							{
								outputColumns.add(r.getResource("Outcome Factor Value").getValue() + j);
								outputColumnTypes.add("Float");
							}
						if (outcome.OutputBaseAndTestValues)
						{
							for (int j = 0; j < outcome.NumFactorsToRank; j++)
							{
								outputColumns.add(r.getResource("Outcome Factor Base Value").getValue() + j);
								outputColumnTypes.add("Float");
							}
							for (int j = 0; j < outcome.NumFactorsToRank; j++)
							{
								outputColumns.add(r.getResource("Outcome Factor Test Value").getValue() + j);
								outputColumnTypes.add("Float");
							}
						}
						if (outcome.OutputPredictedImpacts)
							for (int j = 0; j < outcome.NumFactorsToRank; j++)
							{
								outputColumns.add(r.getResource("Outcome Factor Impact Value").getValue() + j);
								outputColumnTypes.add("Float");
							}
					}
					/*
					 * note that the number of columns can change based upon the value of NumFactors to rank -
					 * createTable will throw an exception of the column count does not match
					 */
					Database.createTable(r, r.getDefaultConnection(), outcome.OutputTableName, outputColumns, outputColumnTypes,
							outcome.DropExistingTable, false, null, null, null);
					Database.dropIndex(r.getDefaultConnection(), r.getResource("Outcome Target ID").getValue(),
							outcome.OutputTableName);
					Map<String, String> valueByColumn = new HashMap<String, String>();
					valueByColumn.put(r.getResource("Outcome Iteration ID").getValue(), iterationString);
					Database.deleteOldData(r.getDefaultConnection(), outcome.OutputTableName, valueByColumn);
					Database.dropIndex(r.getDefaultConnection(), r.getResource("Outcome Iteration ID").getValue(),
							outcome.OutputTableName);
				} catch (SQLException ex)
				{
					logger.info("Exception in creating/deleting tables/indices, reverting to file-based output: " + ex.toString(), ex);
					// revert to file based rather than lose the data
					writeTable = false;
					bulkLoad = false;
					writeCSVFile = true;
					initOutput(data);
					return;
				}
				if (!bulkLoad)
				{
					/*
					 * build an INSERT statement to handle the fixed set of arguments and the variable set of factor
					 * arguments - name, id, iteration, actual, classprob, predicted, bucket, explanation
					 */
					StringBuilder values = new StringBuilder();
					values.append('\'');
					values.append(outcome.Name);
					values.append("',?,'");
					values.append(iterationString);
					values.append("',?,?,?,?"); // initial fixed set of parameters
					if (r.getModelParameters().UseOnlyExplanableClassificationModels)
						values.append(",?");
					else
						values.append(",null");
					if (data.factorRanks != null)
					{
						for (int j = 0; j < outcome.NumFactorsToRank; j++)
						{
							values.append(",?");
							if (outcome.OutputActuals)
								values.append(",?");
							if (outcome.OutputBaseAndTestValues)
								values.append(",?,?");
							if (outcome.OutputPredictedImpacts)
								values.append(",?");
						}
					}
					pstmtText = "INSERT INTO " + Database.getQualifiedTableName(dc, outcome.OutputTableName) + " VALUES(" + values.toString() + ")";
					try
					{
						dconn.close();
						dconn = dc.ConnectionPool.getConnection();
						dconn.setAutoCommit(false);
						pstmt = dconn.prepareStatement(pstmtText);
					} catch (SQLException ex)
					{
						// Retry if necessary (including getting a connection)
						try
						{
							dconn = dc.ConnectionPool.getConnection();
							dconn.setAutoCommit(false);
							pstmt = dconn.prepareStatement(pstmtText);
						} catch (SQLException ex2)
						{
							logger.info("Exception on creating prepared statment: " + ex.toString(), ex2);
							return;
						}
					}
				}
			}
		}
	}

	/**
	 * handle the scoring of the instances
	 * 
	 * @author Rick Spickelmier
	 */
	private class ScoringThread extends Thread
	{
		private final BlockingQueue<OutputData> queue;
		private int numInstances;
		private Instances classDataSet;
		private Instances valueDataSet;
		private Instances actualValuesDataSet;
		private List<Integer> actualRowIndexList;
		private Segment seg;
		private int minIndex;
		private int maxIndex;
		private int[] factorIndices = null;
		private int[] factorRanks = null;
		private double[] averages = null;
		private double[] actualValues = null;
		private double[] baseValues = null;
		private double[] testValues = null;
		private double[] impactValues = null;
		private Classifier cVal = null;
		private Classifier cCls = null;
		private Classifier cValForCls = null;

		public ScoringThread(BlockingQueue<OutputData> q, Classifier p_cVal, Classifier p_cCls, Classifier p_cValForCls, Segment p_seg, int p_numInstances,
				Instances p_classDataSet, Instances p_valueDataSet, Instances p_actualValuesDataSet, List<Integer> p_actualRowIndexList, int[] p_factorIndices,
				double[] p_averages)
		{
			this.setName("Scoring - " + this.getId());
			queue = q;
			cVal = p_cVal;
			cCls = p_cCls;
			cValForCls = p_cValForCls;
			seg = p_seg;
			numInstances = p_numInstances;
			classDataSet = p_classDataSet;
			valueDataSet = p_valueDataSet;
			actualValuesDataSet = p_actualValuesDataSet;
			actualRowIndexList = p_actualRowIndexList;
			factorIndices = p_factorIndices;
			averages = p_averages;
		}

		/**
		 * add the results of a scoring pass to the table and/or file
		 * 
		 * @param inst
		 * @param keyList
		 * @param iteratorKeyList
		 * @param actual
		 * @param predClass
		 * @param predVal
		 * @param bucketList
		 * @param factorRanks
		 * @param factorIndices
		 * @param actualValues
		 * @param baseValues
		 * @param testValues
		 */
		private void addToOutput(String key, String iteratorKeyList, double actual, double predClass, double predVal, String bucketList, int[] factorRanks,
				double[] actualValues, double[] baseValues, double[] testValues, double[] impactValues, String explanation) throws Exception
		{
			OutputData data = new OutputData();
			data.done = false;
			data.key = key;
			data.iteratorKeyList = iteratorKeyList;
			data.actual = actual;
			data.predClass = predClass;
			data.predVal = predVal;
			data.bucketList = bucketList;
			data.factorRanks = factorRanks;
			data.actualValues = actualValues;
			data.baseValues = baseValues;
			data.testValues = testValues;
			data.impactValues = impactValues;
			data.explanation = explanation;
			queue.put(data);
		}

		/**
		 * initialize the data structures necessary for scoring
		 */
		private void setUpData() throws Exception
		{
			minIndex = 0;
			if (outcome.MinRelativeToMeasure)
			{
				// Get column index
				minIndex = smi.getIndexForMeasure(outcome.MinMeasure);
				if (minIndex < 0)
				{
					throw new Exception("Unable to find measure '" + outcome.MinMeasure + "' for outcome min measure");
				}
			}
			maxIndex = 0;
			if (outcome.MaxRelativeToMeasure)
			{
				// Get column index
				maxIndex = smi.getIndexForMeasure(outcome.MaxMeasure);
				if (maxIndex < 0)
				{
					throw new Exception("Unable to find measure '" + outcome.MinMeasure + "' for outcome max measure");
				}
			}
		}

		/**
		 * run the scoring until there are no more instances
		 */
		public void run()
		{
			try
			{
				int excludeRuleCount = 0;
				int cnt = 0;
				int valueIndex = 0;
				setUpData();
				if (outcome.EstimateValueInClass)
				{
					String measureName = outcome.TargetMeasure == null ? smi.sm.Measure : outcome.TargetMeasure;
					valueIndex = smi.getIndexForMeasure(measureName);
					if (valueIndex < 0)
					{
						throw new Exception("Unable to find measure '" + measureName + "'");
					}
				}
				// see if there are rulesets for classification and exclusion, if so, bind them to the data
				// (map attributes and measures to the attributes in the instance)
				ClassificationRuleSet ruleset = null;
				ClassificationRuleSet exclusionRuleset = null;
				if (outcome.EstimateClass)
				{
					if (outcome.getRuleset() != null)
					{
						// clone the ruleset so we can use it in multiple threads
						ruleset = (ClassificationRuleSet) outcome.getRuleset().clone();
						if (!ruleset.bindToData(smi, seg.getSelectedClassColumnIndexList()))
						{
							ruleset = null; // could not bind the ruleset, skipping
						}
					}
					if (outcome.getExclusionRuleset() != null)
					{
						// clone the ruleset so we can use it in multiple threads
						exclusionRuleset = (ClassificationRuleSet) outcome.getExclusionRuleset().clone();
						if (!exclusionRuleset.bindToData(smi, seg.getSelectedClassColumnIndexList()))
						{
							exclusionRuleset = null; // could not bind the ruleset, skipping
						}
					}
				}
				boolean excludeMin = outcome.ExcludeIfBelowMinValue;
				while (true)
				{
					int i = getNextInstance(numInstances);
					if (i == -1) // no more instances, close down the thread
					{
						logger.trace("Scoring Thread processed " + cnt + " instances (instances excluded by rules: " + excludeRuleCount + ")");
						return;
					}
					int index = actualRowIndexList.get(i);
					String key = smi.mds.getTargetKey(index).toString();
					String iteratorKey = smi.mds.getIteratorKey(index).toString();
					double predClass = 0;
					double predVal = 0;
					double actual = 0;
					String bucketList = null;
					String explanation = null;
					outcome.ExcludeIfBelowMinValue = excludeMin; // may have been set to false for rule-based
																	// classification
					if (outcome.EstimateClass)
					{
						Instance inst = classDataSet.instance(i);
						inst.setDataset(classDataSet);
						// map the instance to an array of objects for use with the rulesets
						Object[] row = null;
						if (exclusionRuleset != null || ruleset != null)
						{
							int numAttributes = inst.numAttributes();
							row = new Object[numAttributes];
							for (int j = 0; j < numAttributes; j++)
							{
								row[j] = inst.value(j);
							}
						}
						// check the negative ruleset to make sure we really want to classify it as this class
						boolean foundMatch = false;
						if (exclusionRuleset != null)
						{
							try
							{
								// see if any of the rules in the ruleset match the row
								ClassificationRule rule = exclusionRuleset.evaluate(row, r);
								if (rule != null)
								{
									predClass = 0.0; // if a rule matches, the predicted class is 0.0 (this is a
									// negative rule)
									foundMatch = true; // no need to further classify
									excludeRuleCount++;
								}
							} catch (Exception ex)
							{
								// don't let an exception stop the flow
								logger.error(ex.getMessage(), ex);
							}
						}
						// see if the positive ruleset matches the instance
						if (!foundMatch && (ruleset != null))
						{
							try
							{
								// see if any of the rules in the ruleset match the row
								ClassificationRule rule = ruleset.evaluate(row, r);
								if (rule != null)
								{
									predClass = 1.0; // if a rule matches, the predicted class is 1.0 (positive ruleset)
									explanation = rule.getExplanation();
									foundMatch = true;
								}
							} catch (Exception ex)
							{
								// don't let an exception stop the flow
								logger.error(ex.getMessage(), ex);
							}
						}
						row = null;
						// if no rules match (or if no rulesets available), use model-based classification
						if (!foundMatch)
						{
							// Predict Instance Class Probability
							double[] dist = cCls.distributionForInstance(inst);
							predClass = dist[1];
							if (predClass > PRED_VALUE_IN_CLASS_THRESHOLD)
							{
								if (cCls.isExplanable())
								{
									// dump the tree once for help in debugging
									if (!treeDumped)
									{
										treeDumped = true;
										System.out.println(cCls.toString());
										logger.debug(cCls.toString());
									}
									boolean[] binaries = new boolean[inst.numAttributes()];
									for (int j = 1; j < inst.numAttributes(); j++)
									{
										String name = inst.attribute(j).name();
										int colIndex = seg.getSelectedClassColumnIndexList().get(j - 1);
										Header h = smi.mds.getHeaders().get(colIndex);
										if ((h.Type == HeaderType.Attribute && name.indexOf('=') > 0) || name.indexOf("Has: ") >= 0)
											binaries[j] = true;
										else
											binaries[j] = false;
									}
									if (cCls instanceof J48)
									{
										J48 explanableClassifier = (J48) cCls;
										explanation = explanableClassifier.explainClassification(inst, binaries);
									}
									if (cCls instanceof Ridor)
									{
										Ridor explanableClassifier = (Ridor) cCls;
										explanation = explanableClassifier.explainClassification(inst, binaries);
									}
									if (cCls instanceof Logistic)
									{
										Logistic explanableClassifier = (Logistic) cCls;
										explanation = explanableClassifier.explainClassification(inst, binaries);
									}
									explanation = explanation.replace("&", "AND");
								}
							}
						}
						actual = actualValuesDataSet.instance(i).value(0);
						if (outcome.EstimateValueInClass && (!outcome.EstimateValue) && (predClass > PRED_VALUE_IN_CLASS_THRESHOLD))
						{
							/*
							 * Predict Instance Value (use same index as both datasets were created using the same row
							 * index list) construct instance on the fly
							 */
							if (foundMatch)
							{
								predVal = actual;
								outcome.ExcludeIfBelowMinValue = false; // actual == pred in the case of rule-based
								// classification
							} else
							{
								Instance vinst = smi.getInstance(index, seg.getSelectedColumnIndexList(), true, false, valueIndex, false);
								vinst.setDataset(valueDataSet);
								predVal = cValForCls.classifyInstance(vinst);
							}
							if (outcome.EstimateDifferenceFromActual)
								predVal = predVal - actual;
						}
					}
					if (outcome.EstimateValue)
					{
						Instance inst = valueDataSet.instance(i);
						inst.setDataset(valueDataSet);
						// Predict Instance Value
						predVal = cVal.classifyInstance(inst);
						actual = inst.value(0);
						if (outcome.EstimateDifferenceFromActual)
							predVal = predVal - actual;
					}
					// Make sure predicted value falls in the appropriate range
					double minVal = 0;
					if (outcome.UseMinEstimatedValue)
					{
						minVal = outcome.MinEstimatedValue;
						if (outcome.RelativeMinValue)
						{
							if (outcome.MinRelativeToMeasure)
							{
								if (minIndex >= 0)
								{
									minVal = smi.mds.getCell(index, minIndex) * minVal;
								} else
								{
									logger.info("Minimum Relative Measure Not Found");
									minVal = 0;
								}
							} else
							{
								minVal = minVal * actual;
							}
						}
						if (predVal < minVal)
						{
							predVal = minVal;
							if (outcome.ExcludeIfBelowMinValue)
							{
								predClass = 0;
								predVal = 0;
							}
						}
					}
					if (outcome.UseMaxEstimatedValue)
					{
						double val = outcome.MaxEstimatedValue;
						if (outcome.RelativeMaxValue)
						{
							if (outcome.MaxRelativeToMeasure)
							{
								if (maxIndex >= 0)
								{
									val = smi.mds.getCell(index, maxIndex) * val;
								} else
								{
									logger.info("Maximum Relative Measure Not Found");
									val = 0;
								}
							} else
							{
								val = val * actual;
							}
						}
						if (predVal > val)
						{
							if (outcome.UseMinEstimatedValue)
								predVal = Math.max(minVal, val);
							else
								predVal = val;
							if (outcome.ExcludeIfAboveMaxValue)
							{
								predClass = 0;
								predVal = 0;
							}
						}
					}
					if (outcome.OutputBuckets && (predClass > PRED_VALUE_IN_CLASS_THRESHOLD))
					{
						// Gross val up if it's only the difference
						double val = outcome.EstimateDifferenceFromActual ? (actual + predVal) : predVal;
						if (outcome.OutputBucketType == Outcome.BUCKET_TYPE_DIFFERENCE)
						{
							val = val - actual;
						} else if (outcome.OutputBucketType == Outcome.BUCKET_TYPE_PCT_DIFFERENCE)
						{
							/*
							 * Don't want to have a negative gain. If actual is zero, then this is huge relative growth
							 * (as long as the predicted value isn't zero as well)
							 */
							val = (actual == 0) ? ((val == 0) ? 0 : Double.POSITIVE_INFINITY) : (val - actual) / Math.max(actual, 0);
						}
						if (outcome.Buckets != null)
						{
							for (int j = 0; j < outcome.Buckets.length; j++)
							{
								if ((val >= outcome.Buckets[j].dmin) && (val <= outcome.Buckets[j].dmax))
								{
									bucketList = outcome.Buckets[j].Name;
									break;
								}
							}
						}
					}
					// rank factors that affect the result
					if (outcome.RankFactors && outcome.EstimateValue && (outcome.Factors.length > 0))
					{
						Instance inst = valueDataSet.instance(i); // instance under test
						inst.setDataset(valueDataSet);
						List<InstanceValue> factorList = new ArrayList<InstanceValue>();
						actualValues = new double[outcome.Factors.length];
						baseValues = new double[outcome.Factors.length];
						testValues = new double[outcome.Factors.length];
						factorRanks = new int[outcome.NumFactorsToRank];
						impactValues = new double[outcome.Factors.length];
						for (int j = 0; j < outcome.Factors.length; j++)
						{
							if (factorIndices[j] >= 0)
							{ // factorIndices[j] is the index of the attribute in the instance for the 'j'th factor
								OutcomeFactor of = outcome.Factors[j];
								Instance newBase = (Instance) inst.copy(); // base - 'average' value for factor
								newBase.setDataset(valueDataSet);
								Instance newTest = (Instance) inst.copy(); // test - 'current' value for factor
								newTest.setDataset(valueDataSet);
								for (int k = 0; k < outcome.Factors.length; k++)
								{
									if (factorIndices[k] >= 0)
									{ // factor exists within the instance
										if (k == j)
										{
											if (of.getTest() == SettingType.Zero)
											{
												newTest.setValue(factorIndices[k], 0);
											} else if (of.getTest() == SettingType.Average)
											{
												newTest.setValue(factorIndices[k], averages[k]);
											} else if (of.getTest() == SettingType.Value)
											{
												newTest.setValue(factorIndices[k], of.getTestValue());
											} else if (of.getTest() == SettingType.RelativeValue)
											{
												double curVal = newTest.value(factorIndices[k]);
												if (Double.isNaN(curVal))
													curVal = 0;
												newTest.setValue(factorIndices[k], curVal + of.getTestValue());
											}
										} else if (outcome.EvaluateFactorsAsGroup)
										{
											if (of.getBase() == SettingType.Zero)
											{
												newTest.setValue(factorIndices[k], 0);
											} else if (of.getBase() == SettingType.Average)
											{
												newTest.setValue(factorIndices[k], averages[k]);
											} else if (of.getBase() == SettingType.Value)
											{
												newTest.setValue(factorIndices[k], of.getBaseValue());
											} else if (of.getBase() == SettingType.RelativeValue)
											{
												double curVal = newTest.value(factorIndices[k]);
												if (Double.isNaN(curVal))
													curVal = 0;
												newTest.setValue(factorIndices[k], curVal + of.getBaseValue());
											}
										}
										if (outcome.EvaluateFactorsAsGroup || k == j)
										{
											if (of.getBase() == SettingType.Zero)
											{
												newBase.setValue(factorIndices[k], 0);
											} else if (of.getBase() == SettingType.Average)
											{
												newBase.setValue(factorIndices[k], averages[k]);
											} else if (of.getBase() == SettingType.Value)
											{
												newBase.setValue(factorIndices[k], of.getBaseValue());
											} else if (of.getBase() == SettingType.RelativeValue)
											{
												double curVal = newBase.value(factorIndices[k]);
												if (Double.isNaN(curVal))
													curVal = 0;
												newBase.setValue(factorIndices[k], curVal + of.getBaseValue());
											}
										}
									} // end if (factorIndices[k] >= 0)
								} // end for k
								// classify base and test, compute the delta (bigger delta, bigger effect)
								double basePred = cVal.classifyInstance(newBase);
								double testPred = cVal.classifyInstance(newTest);
								InstanceValue iv = new InstanceValue();
								iv.index = j;
								iv.value = testPred - basePred;
								factorList.add(iv);
								actualValues[j] = inst.value(factorIndices[j]);
								baseValues[j] = newBase.value(factorIndices[j]);
								testValues[j] = newTest.value(factorIndices[j]);
								impactValues[j] = iv.value;
							} // end if (factorIndices[j] >= 0)
						} // end for j
						// get top N factors (sorted in ascending order, so from the bottom)
						// getting the 'j' value above - i.e., the factor
						Collections.sort(factorList); // sort by 'value'
						for (int j = 0; j < outcome.NumFactorsToRank; j++)
						{
							factorRanks[j] = j < factorList.size() ? factorList.get(factorList.size() - j - 1).index : -1;
						}
					}
					if (!outcome.OutputOnlyClass || (predClass > PRED_VALUE_IN_CLASS_THRESHOLD))
					{
						addToOutput(key, iteratorKey, actual, predClass, predVal, bucketList, factorRanks, actualValues, baseValues, testValues, impactValues,
								explanation);
						if ((cnt > 0) && ((cnt % 20000) == 0))
							logger.trace("Scoring Thread has scored " + cnt + " instances");
					}
					if ((++cnt % 20000) == 0)
						logger.trace("Scoring Thread has processed " + cnt + " instances (in and out of classification)");
				}
			} catch (Exception ex)
			{
				logger.error("Exception in scoring thread: " + ex.toString(), ex);
				setError();
				return;
			}
		}
	}

	/**
	 * Output the key results of segment models
	 * 
	 * @param stream
	 * @throws Exception
	 */
	public void outputSegmentModels(PrintStream stream) throws Exception
	{
		for (Segment seg : segmentMap.values())
		{
			seg.outputModels(stream);
		}
	}

	/**
	 * @return the segmentMap
	 */
	public Map<Object, Segment> getSegmentMap()
	{
		return segmentMap;
	}
}
