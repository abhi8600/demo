/**
 * $Id: ModelParameters.java,v 1.20 2010-12-20 00:36:05 bpeters Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.io.Serializable;

import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jdom.Namespace;
import com.successmetricsinc.Repository;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author Brad Peters
 * 
 */
public class ModelParameters implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(ModelParameters.class);
	public int MaxComputationThreads;
	public int MaxComputationThreadsOrig;
	public int MaxClustersPerPattern;
	public int MaxOneAttributeCategories;
	public boolean ClusterDensity;
	public int SampleSize;
	public double MinSamplesPerAttribute;
	public int MaxAttributesPerModel;
	public double TargetReductionRate;
	public int MaxAttributesPerFold;
	public ClassifierSettings Classifiers;
	public boolean ModelBasedFiltering = true;
	public boolean UseOnlyExplanableClassificationModels = false;
	public boolean MapNullsToZero = true;
	public int MaxInstances = 500000;
	public String BulkLoadDirectory = null;
	public double TargetCorrelationThreshold = 0.96;
	public double CrossCorrelationThreshold = 0.92;
	public boolean UseCrossCorrelation = true;
	public boolean UseWekaRemoveUseless = true;
	public boolean UseRemoveRelated = true;
	public boolean EnableAttributeSelection = true;
	public int MaxARFFInstances = 5000;

	public ModelParameters(Element e, Namespace ns)
	{
		MaxComputationThreads = Integer.parseInt(e.getChildText("MaxComputationThreads", ns));
		MaxComputationThreadsOrig = Integer.parseInt(e.getChildText("MaxComputationThreads", ns));
		// put a ceiling on the number of computation threads to the number of avaliable processors
		int numberOfProcessors = Runtime.getRuntime().availableProcessors();
		// for a small number of processors, use all but 1
		if (numberOfProcessors > 1 && numberOfProcessors <= 4)
			numberOfProcessors -=1;
		// for a larger number of processors, use all but 2
		else if (numberOfProcessors >= 5)
			numberOfProcessors -= 2;
		if (MaxComputationThreads > numberOfProcessors)
		{
			if (System.getProperty("smi.dev.debug") == null) {
				logger.warn("Setting the number of computation threads to: " + numberOfProcessors + ", requested value was: " + MaxComputationThreads);
				MaxComputationThreads = numberOfProcessors;
			} else {
				// in a debug mode, so just use the number of therads irrespective of what are the number of processor on your machine.
				logger.warn("Setting the number of threads to more than the number of processors for debug purposes. Number of processors:" + numberOfProcessors + ", Computations threads requested in reposotory: " + MaxComputationThreads);
			}
		}
		MaxClustersPerPattern = Integer.parseInt(e.getChildText("MaxClustersPerPattern", ns));
		MaxOneAttributeCategories = Integer.parseInt(e.getChildText("MaxOneAttributeCategories", ns));
		ClusterDensity = Boolean.getBoolean(e.getChildText("ClusterDensity", ns));
		SampleSize = Integer.parseInt(e.getChildText("SampleSize", ns));
		MinSamplesPerAttribute = Double.parseDouble(e.getChildTextTrim("MinSamplesPerAttribute", ns));
		MaxAttributesPerModel = Integer.parseInt(e.getChildText("MaxAttributesPerModel", ns));
		TargetReductionRate = Double.parseDouble(e.getChildTextTrim("TargetReductionRate", ns));
		MaxAttributesPerFold = Integer.parseInt(e.getChildText("MaxAttributesPerFold", ns));
		String temp = e.getChildText("MaxARFFInstances", ns);
		if (temp != null)
		{
			MaxARFFInstances = Integer.parseInt(temp);
		}

		Element clSettings = e.getChild("Classifiers", ns);
		Classifiers = new ClassifierSettings();
		if (clSettings == null)
		{
			Classifiers.RegressionModels = new int[0];
			Classifiers.ClassificationModels = new int[0];
		} else
		{
			Classifiers.RegressionModels = Repository.getIntArrayChildren(clSettings, "RegressionModels", ns);
			Classifiers.ClassificationModels = Repository.getIntArrayChildren(clSettings, "ClassificationModels", ns);
			Classifiers.MaxSampleInstanceValues = Integer.parseInt(clSettings.getChildText("MaxSampleInstanceValues", ns));
			Classifiers.MaxInstanceValues = Integer.parseInt(clSettings.getChildText("MaxInstanceValues", ns));
		}

		// old name for backwards compatibility
		ModelBasedFiltering = Boolean.parseBoolean(e.getChildTextTrim("DecisionTreeFiltering", ns));
		// new name that more closely follows the actual meaning
		temp = e.getChildTextTrim("ModelBasedFiltering", ns);
		if (temp != null)
		{
			ModelBasedFiltering = Boolean.parseBoolean(temp);
		}

		MapNullsToZero = Boolean.parseBoolean(e.getChildText("MapNullsToZero", ns));
		temp = e.getChildText("MaxInstances", ns);
		if (temp != null)
			MaxInstances = Integer.parseInt(temp);
		
		temp = e.getChildText("TargetCorrelationThreshold", ns);
		if (temp != null)
			this.TargetCorrelationThreshold = Double.parseDouble(temp);
		temp = e.getChildText("CrossCorrelationThreshold", ns);
		if (temp != null)
			this.CrossCorrelationThreshold = Double.parseDouble(temp);
		temp = e.getChildText("UseCrossCorrelation", ns);
		if (temp != null)
			this.UseCrossCorrelation = Boolean.parseBoolean(temp);
		temp = e.getChildText("UseWekaRemoveUseless", ns);
		if (temp != null)
			this.UseWekaRemoveUseless = Boolean.parseBoolean(temp);
		temp = e.getChildText("UseRemoveRelated", ns);
		if (temp != null)
			this.UseRemoveRelated = Boolean.parseBoolean(temp);
		temp = e.getChildText("EnableAttributeSelection", ns);
		if (temp != null)
			this.EnableAttributeSelection = Boolean.parseBoolean(temp);
		temp = e.getChildTextTrim("UseOnlyExplanableClassificationModels", ns);
		if (temp != null)
			UseOnlyExplanableClassificationModels = Boolean.parseBoolean(temp);

		BulkLoadDirectory = e.getChildTextTrim("BulkLoadDirectory", ns);
	}
	
	public Element getModelParametersElement(Namespace ns)
	{
		Element e = new Element("ModelParameters",ns);
		
		Element child = new Element("MaxComputationThreads",ns);
		child.setText(String.valueOf(MaxComputationThreadsOrig));
		e.addContent(child);
		
		XmlUtils.addContent(e, "BulkLoadDirectory", BulkLoadDirectory,ns);
		
		child = new Element("MaxClustersPerPattern",ns);
		child.setText(String.valueOf(MaxClustersPerPattern));
		e.addContent(child);
		
		child = new Element("MaxOneAttributeCategories",ns);
		child.setText(String.valueOf(MaxOneAttributeCategories));
		e.addContent(child);
		
		child = new Element("ClusterDensity",ns);
		child.setText(String.valueOf(ClusterDensity));
		e.addContent(child);
		
		child = new Element("SampleSize",ns);
		child.setText(String.valueOf(SampleSize));
		e.addContent(child);
		
		child = new Element("MinSamplesPerAttribute",ns);
		child.setText(Util.getDecimalFormatForRepository().format(MinSamplesPerAttribute));
		e.addContent(child);
		
		child = new Element("MaxAttributesPerModel",ns);
		child.setText(String.valueOf(MaxAttributesPerModel));
		e.addContent(child);
		
		child = new Element("TargetReductionRate",ns);
		child.setText(String.valueOf(TargetReductionRate));
		e.addContent(child);
		
		child = new Element("MaxAttributesPerFold",ns);
		child.setText(String.valueOf(MaxAttributesPerFold));
		e.addContent(child);
		
		e.addContent(Classifiers.getClassifierSettingsElement(ns));
		
		child = new Element("ModelBasedFiltering",ns);
		child.setText(String.valueOf(ModelBasedFiltering));
		e.addContent(child);
		
		child = new Element("MapNullsToZero",ns);
		child.setText(String.valueOf(MapNullsToZero));
		e.addContent(child);
		
		child = new Element("MaxInstances",ns);
		child.setText(String.valueOf(MaxInstances));
		e.addContent(child);
		
		child = new Element("UseOnlyExplanableClassificationModels",ns);
		child.setText(String.valueOf(UseOnlyExplanableClassificationModels));
		e.addContent(child);
		
		child = new Element("TargetCorrelationThreshold",ns);
		child.setText(String.valueOf(TargetCorrelationThreshold));
		e.addContent(child);
		
		child = new Element("CrossCorrelationThreshold",ns);
		child.setText(String.valueOf(CrossCorrelationThreshold));
		e.addContent(child);
		
		child = new Element("UseCrossCorrelation",ns);
		child.setText(String.valueOf(UseCrossCorrelation));
		e.addContent(child);
		
		child = new Element("UseWekaRemoveUseless",ns);
		child.setText(String.valueOf(UseWekaRemoveUseless));
		e.addContent(child);
		
		child = new Element("EnableAttributeSelection",ns);
		child.setText(String.valueOf(EnableAttributeSelection));
		e.addContent(child);
		
		child = new Element("UseRemoveRelated",ns);
		child.setText(String.valueOf(UseRemoveRelated));
		e.addContent(child);
		
		child = new Element("MaxARFFInstances",ns);
		child.setText(String.valueOf(MaxARFFInstances));
		e.addContent(child);
		
		return e;
	}

	public int getMaxComputationThreads() {
		return MaxComputationThreads;
	}
}