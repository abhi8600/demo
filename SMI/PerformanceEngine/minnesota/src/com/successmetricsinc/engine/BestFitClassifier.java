/**
 * $Id: BestFitClassifier.java,v 1.54 2009-12-18 18:22:36 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.apache.log4j.Logger;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.functions.LinearRegression;
import weka.classifiers.functions.Logistic;
import weka.classifiers.functions.MultilayerPerceptron;
import weka.classifiers.functions.SMO;
import weka.classifiers.functions.SMOreg;
import weka.classifiers.functions.LibSVM;
import weka.classifiers.functions.SVMreg;
import weka.core.SelectedTag;
import weka.classifiers.functions.supportVector.NormalizedPolyKernel;
import weka.classifiers.functions.supportVector.PolyKernel;
import weka.classifiers.functions.supportVector.RBFKernel;
import weka.classifiers.meta.AdditiveRegression;
import weka.classifiers.rules.M5Rules;
import weka.classifiers.rules.Ridor;
import weka.classifiers.trees.J48;
import weka.classifiers.trees.REPTree;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.instance.Resample;
import weka.filters.unsupervised.attribute.ReplaceMissingValues;
import weka.core.Capabilities;

import com.successmetricsinc.util.WekaUtil;

/**
 * Class to find a classifier that best fits a given data set. Many are tried and tested.
 * 
 * @author bpeters
 */
public class BestFitClassifier
{
	private static Logger logger = Logger.getLogger(BestFitClassifier.class);
	// Number of crossfolds for final validation
	private static final int CROSS_VALIDATION_FOLDS = 5;
	// Weights used to compare measures of validation
	private static final double RAE_WEIGHT = 10; // Relative absolute error
	private static final double RRSE_WEIGHT = 0; // Root relative squared error
	private static final double RMSE_WEIGHT = 40; // Root mean squared error
	private static final double MAE_WEIGHT = 15; // Mean absolute error
	private static final double CORR_WEIGHT = 35; // Correlation
	private static final double ER_WEIGHT = 50; // Class error rate
	private static final double FP_WEIGHT = 30; // Class false positive rate
	private static final double FN_WEIGHT = 20; // Class false negative rate
	public static final double MAX_INSTANCES_PER_ATTRIBUTE = 150;
	private static final double MIN_INSTANCES_PER_ATTRIBUTE = 5;
	private int SAMPLE_INSTANCE_VALUES = 1000 * 85;
	private int MAX_INSTANCE_VALUES = 1000 * 225;
	private static final double MIN_BOOSTING_ERROR_REDUCTION = 0.95;
	private static final int NUM_TEST_FOLDS = 3;
	/*
	 * Amount to boost samples when doing classification only (not regression) - less information and faster time to
	 * process are two motivations for increasing sample size
	 */
	private static final double EXTRA_SAMPLES_FOR_CLASSIFICATION = 2;
	private static final double MAX_TEST_PASS_SQ_ERROR = 100;
	private static final double MAX_TEST_PASS_ERROR_RATE = 25;
	private static final int MAX_TEST_PASS_RETRIES = 2;
	private static final double MAX_ERROR_RATE_DIFF = 7;
	private static final double MAX_SQ_ERROR_DIFF = 15;
	private static final int MAX_SAMPLE_RETRIES = 3;
	private boolean ATTEMPT_BOOSTING = false;
	public final static int RIDOR_MODEL = 105;
	private static int[][] NON_LOG_VAL_MODELS =
	{
	{ 0, 1, 2, 3, 4, 5, 6, 10, 13, 15, 20, 21, 22, 23, 24, 25, 26 },
	{ 7, 8, 9, 11, 12, 27, 28 } };
	private static int[][] NON_LOG_CLASS_MODELS =
	{
	{ 103, 104, RIDOR_MODEL }, // explanable models
	{ 100, 101, 102, 150 } };
	private static int[][] LOG_MODELS =
	{
	{ 0, 1, 9, 10, 15, 20, 21 } };
	private static int[][] LOG_CLASS_MODELS =
	{
	{ 103, 104, RIDOR_MODEL }, // explanable models
	{ 100, 101, 102, 150 } };
	private final static int MODEL_REMOVED = 1024;
	private static final int MAX_MLP_ATTRIBUTES = 150;
	private boolean WRITE_ARFF = false;
	private String ARFF_NAME = null;
	private boolean CROSS_VALIDATE_TESTING = false;
	private boolean USE_SAMPLING = true;
	private Instances dataSet;
	private boolean LOGARITHMIC = false;
	private boolean FLATTEN_EXTREMES = false;
	private double FLATTEN_PCT = 0.05;
	private BestFitResult curBest = null;
	private int selected = 0;
	private int curModelIndex = 0;
	private int[] curModelList;
	private int maxThreads = 1;
	private boolean classModel = false;
	private int[][] classifierList = null;
	private String label;
	private boolean nonZeroFalsePositiveRate = false;
	private boolean biasToUniformClass = false;
	private boolean includeAllNonzero = false;
	private ModelParameters modelParams = null;
	private ClassifierSettings classifiers = null;
	private OutcomeFactor[] factors;

	/**
	 * Construct a BestFitClassifier instance
	 * 
	 * @param label
	 *            Label to use for classifier
	 * @param dataSet
	 *            Dataset to use to train classifier
	 * @param useSampling
	 *            Whether to sample the dataset or use all instances
	 * @param crossValidateTesting
	 *            Use cross-validation testing (vs. single-fold testing - faster) for testing modeling techniques
	 * @param logarithmic
	 *            Use logarithmic model - log values are taken of instances
	 * @param flattenExtremes
	 *            Flatten extreme attribute values to the ends of the ranges
	 * @param maxThreads
	 *            Maximum computation threads to use for training/testing
	 */
	public BestFitClassifier(String label, Instances dataSet, boolean useSampling, boolean crossValidateTesting, boolean logarithmic, boolean flattenExtremes,
			int maxThreads, ModelParameters modelParams, ClassifierSettings classifiers)
	{
		this.dataSet = dataSet;
		this.USE_SAMPLING = useSampling;
		this.CROSS_VALIDATE_TESTING = crossValidateTesting;
		this.LOGARITHMIC = logarithmic;
		this.FLATTEN_EXTREMES = flattenExtremes;
		this.maxThreads = maxThreads;
		this.label = label;
		this.modelParams = modelParams;
		this.classifiers = classifiers;
		this.MAX_INSTANCE_VALUES = classifiers.MaxInstanceValues;
		this.SAMPLE_INSTANCE_VALUES = classifiers.MaxSampleInstanceValues;
		if (dataSet.attribute(0).isNumeric())
		{
			classifierList = LOGARITHMIC ? LOG_MODELS : NON_LOG_VAL_MODELS;
		} else
		{
			classModel = true;
			classifierList = LOGARITHMIC ? LOG_CLASS_MODELS : NON_LOG_CLASS_MODELS;
		}
	}
	String arffComment = null;

	public void setArffComment(String comment)
	{
		this.arffComment = comment;
	}

	public void setFlattenPercent(double val)
	{
		this.FLATTEN_PCT = val;
	}

	public void setAttemptBoosting(boolean val)
	{
		ATTEMPT_BOOSTING = val;
	}

	public void setWriteArff(boolean wa, String name)
	{
		WRITE_ARFF = wa;
		ARFF_NAME = name;
	}

	public void setNonZeroFalsePositiveRate(boolean b)
	{
		nonZeroFalsePositiveRate = b;
	}
	List<InstanceValue> RRSEList = new ArrayList<InstanceValue>();
	List<InstanceValue> RAEList = new ArrayList<InstanceValue>();
	List<InstanceValue> RMSEList = new ArrayList<InstanceValue>();
	List<InstanceValue> MAEList = new ArrayList<InstanceValue>();
	List<InstanceValue> CorrList = new ArrayList<InstanceValue>();
	List<InstanceValue> Ranks = new ArrayList<InstanceValue>();
	List<InstanceValue> TimeList = new ArrayList<InstanceValue>();
	Map<Integer, BestFitResult> results = new HashMap<Integer, BestFitResult>();

	private void rankClassifiers()
	{
		Collections.sort(RRSEList);
		Collections.sort(RAEList);
		Collections.sort(RMSEList);
		Collections.sort(MAEList);
		Collections.sort(CorrList);
		Collections.sort(TimeList);
		/*
		 * Sort based on weighted ranking
		 */
		for (InstanceValue iv : Ranks)
		{
			iv.value = InstanceValue.getIndexLocation(RRSEList, iv.index) * RRSE_WEIGHT + InstanceValue.getIndexLocation(RAEList, iv.index) * RAE_WEIGHT
					+ InstanceValue.getIndexLocation(RMSEList, iv.index) * RMSE_WEIGHT + InstanceValue.getIndexLocation(MAEList, iv.index) * MAE_WEIGHT
					+ (CorrList.size() - InstanceValue.getIndexLocation(CorrList, iv.index) - 1) * CORR_WEIGHT;
			iv.value /= (RRSE_WEIGHT + RAE_WEIGHT + RMSE_WEIGHT + MAE_WEIGHT + CORR_WEIGHT);
		}
		Collections.sort(Ranks);
	}

	private synchronized void setCurBest(BestFitResult bfr, int classifier, boolean classModel) throws Exception
	{
		results.put(classifier, bfr);
		if (curBest == null)
		{
			curBest = bfr;
			selected = classifier;
		}
		if (classModel)
		{
			double bfrval = ((bfr.getErrorRate() * ER_WEIGHT) + (bfr.getFalsePositiveRate() * FP_WEIGHT) + (bfr.getFalseNegativeRate() * FN_WEIGHT))
					/ (ER_WEIGHT + FP_WEIGHT + FN_WEIGHT);
			double curval = ((curBest.getErrorRate() * ER_WEIGHT) + (curBest.getFalsePositiveRate() * FP_WEIGHT) + (curBest.getFalseNegativeRate() * FN_WEIGHT))
					/ (ER_WEIGHT + FP_WEIGHT + FN_WEIGHT);
			if (curval > bfrval)
			{
				// Make sure there's a non-zero false positive rate if desired
				if ((bfr.getFalsePositiveRate() > 0) || (!nonZeroFalsePositiveRate))
				{
					curBest = bfr;
					selected = classifier;
				}
			}
		} else
		{
			RRSEList.add(new InstanceValue(classifier, bfr.getRootRelativeSquaredError()));
			RAEList.add(new InstanceValue(classifier, bfr.getRelativeAbsoluteError()));
			RMSEList.add(new InstanceValue(classifier, bfr.getRootMeanSquaredError()));
			MAEList.add(new InstanceValue(classifier, bfr.getMeanAbsoluteError()));
			CorrList.add(new InstanceValue(classifier, bfr.getCorrelationCoefficient()));
			TimeList.add(new InstanceValue(classifier, (double) bfr.getTime()));
			Ranks.add(new InstanceValue(classifier, 0));
			rankClassifiers();
			selected = Ranks.get(0).index;
			curBest = results.get(selected);
		}
		logger.info(label + ": " + getModelString(classifier) + " - " + bfr.getErrorString());
	}

	private synchronized BestFitResult getCurBest()
	{
		return (curBest);
	}

	/**
	 * Get the index of the next valid model. If an acceptable list is provided, make sure that the model is in that
	 * list
	 * 
	 * @return
	 */
	private synchronized int getNextModel()
	{
		if (curModelIndex < curModelList.length)
		{
			boolean found = false;
			while (!found && (curModelIndex < curModelList.length))
			{
				int[] modelList = null;
				if (classModel)
					modelList = classifiers.ClassificationModels;
				else
					modelList = classifiers.RegressionModels;
				for (int i = 0; i < modelList.length; i++)
				{
					if (modelList[i] == curModelList[curModelIndex])
					{
						found = true;
						break;
					}
				}
				if (found)
					return (curModelList[curModelIndex++]);
				curModelIndex++;
			}
		}
		return (-1);
	}

	public static int getSampleSize(boolean classModel, int numAttributes, double maxInstanceValues, double minInstancesPerAttribute,
			double maxInstancesPerAttribute)
	{
		double maxInstVals = (classModel ? EXTRA_SAMPLES_FOR_CLASSIFICATION : 1) * maxInstanceValues;
		double maxInstPerAtt = (classModel ? EXTRA_SAMPLES_FOR_CLASSIFICATION : 1) * maxInstancesPerAttribute;
		int sampleSize1 = (int) Math.round(maxInstVals / ((double) numAttributes));
		int sampleSize2 = (int) (((double) numAttributes) * minInstancesPerAttribute);
		int sampleSize3 = (int) (maxInstPerAtt * numAttributes);
		return (Math.min(sampleSize3, Math.max(sampleSize1, sampleSize2)));
	}

	/**
	 * Method to try several different sets of model parameters to find the best fit
	 * 
	 * @return BestFitResult data structure containing both the classifier and evaluation of it
	 */
	public BestFitResult findBestFit()
	{
		// Create sub-sample for individual model type selection
		long curTime = System.currentTimeMillis();
		int numInstances = dataSet.numInstances();
		int numAttributes = dataSet.numAttributes();
		Random rand =  new Random(System.currentTimeMillis());
		try
		{
			// Repeat test phase if necessary - i.e. bad sample
			int testCount = 0;
			double testError = 0;
			double maxTestError = 0;
			Instances totalSample = null;
			Instances sample = null;
			Instances test = null;
			int sampleSize = 0;
			do
			{
				sampleSize = getSampleSize(classModel, numAttributes, SAMPLE_INSTANCE_VALUES, modelParams.MinSamplesPerAttribute, MAX_INSTANCES_PER_ATTRIBUTE);
				logger.info(label + ": Data Set Size: " + numInstances + ", Attributes: " + numAttributes + ", Test Count: " + testCount);
				if ((sampleSize > ((double) numInstances) * .75) || !USE_SAMPLING)
				{
					sampleSize = numInstances;
					totalSample = dataSet;
				} else
				{
					double samplePct = 100 * ((double) NUM_TEST_FOLDS / ((double) NUM_TEST_FOLDS - 1)) * ((double) sampleSize) / ((double) numInstances);
					totalSample = getSample(dataSet, samplePct, biasToUniformClass, includeAllNonzero, factors);
					sampleSize = (int) (totalSample.numInstances() * ((double) NUM_TEST_FOLDS - 1) / ((double) NUM_TEST_FOLDS));
				}
				if (FLATTEN_EXTREMES)
				{
					logger.info(label + ": Flattening Extreme Column Values, Test Count: " + testCount);
					if (classModel)
					{
						flattenExtremes(totalSample, FLATTEN_PCT, 1, numAttributes - 1);
					} else
					{
						flattenExtremes(totalSample, FLATTEN_PCT, 0, numAttributes - 1);
					}
				}
				if (WRITE_ARFF)
				{
					WekaUtil.writeArff(totalSample, ARFF_NAME, arffComment);
				}
				if (!CROSS_VALIDATE_TESTING)
				{
					// Split between training and evaluation samples
					sample = totalSample.trainCV(NUM_TEST_FOLDS, 0);
					test = totalSample.testCV(NUM_TEST_FOLDS, 0);
				}
				logger.info(label + ": Training Set Size for Tests: " + sampleSize + ", Test Count: " + testCount);
				for (int group = 0; group < classifierList.length; group++)
				{
					curModelList = classifierList[group];
					curModelIndex = 0;
					Thread[] tlist = new Thread[maxThreads];
					for (int i = 0; i < maxThreads; i++)
					{
						tlist[i] = new TryClassifierThread(totalSample, sample, test, classModel, modelParams.UseOnlyExplanableClassificationModels);
						tlist[i].start();
					}
					for (int i = 0; i < maxThreads; i++)
					{
						tlist[i].join();
					}
				}
				if (curBest == null)
				{
					throw (new Exception("No models built: Please ensure that at least one model type is selected in the repository"));
				}
				if (classModel)
				{
					testError = curBest.getErrorRate();
					maxTestError = MAX_TEST_PASS_ERROR_RATE;
				} else
				{
					testError = curBest.getRootRelativeSquaredError();
					maxTestError = MAX_TEST_PASS_SQ_ERROR;
				}
			} while ((testError > maxTestError) && ((++testCount) < MAX_TEST_PASS_RETRIES));
			
			if (ATTEMPT_BOOSTING && (!classModel))
			{
				logger.info(label + ": Selected and attempting to boost - " + getModelString(selected));
				AdditiveRegression ar = new AdditiveRegression();
				ar.setClassifier(curBest.getClassifier());
				Evaluation ev = null;
				if (CROSS_VALIDATE_TESTING)
				{
					// deal with missing values
					Instances newSample = fixUpMissingValues(totalSample, curBest.getClassifier());
					ev = new Evaluation(totalSample);
					ev.crossValidateModel(ar, newSample, NUM_TEST_FOLDS, rand, modelParams.MaxComputationThreads);
				} else
				{
					Instances newSample = fixUpMissingValues(sample, curBest.getClassifier());
					Instances newTest = fixUpMissingValues(test, curBest.getClassifier());
					ar.buildClassifier(newSample);
					ev = new Evaluation(newSample);
					ev.evaluateModel(ar, newTest);
				}
				// Accept if more than (1-MIN_BOOSTING_ERROR_REDUCTION) better
				if (ev.rootRelativeSquaredError() < curBest.getRootRelativeSquaredError() * MIN_BOOSTING_ERROR_REDUCTION)
				{
					curBest.setClassifier(ar);
					curBest.setEvaluation(classModel, ev);
					logger.info(label + ": Boosting Helpful");
				}
			} else
			{
				logger.info(label + ": Selected - " + getModelString(selected));
			}
			logger.info(label + ": Selected and conducting full training and " + CROSS_VALIDATION_FOLDS + "-fold cross-validation: " + getModelString(selected));
			// Sample if needed (only if 25% higher than threshold)
			boolean usesample = false;
			if (USE_SAMPLING)
			{
				sampleSize = getSampleSize(classModel, numAttributes, MAX_INSTANCE_VALUES, MIN_INSTANCES_PER_ATTRIBUTE, MAX_INSTANCES_PER_ATTRIBUTE);
				if (numInstances > sampleSize * 1.25)
				{
					usesample = true;
				}
			}
			if (usesample)
			{
				double samplePct = 100 * ((double) sampleSize) / ((double) numInstances);
				double errorRateDiff = 0;
				double maxErrorDiff;
				int retryCount = 0;
				Instances sampledSet = null;
				Evaluation eval = null;
				// Continue sampling until the error rate is consistent with testing (i.e. not a bad sample) or max
				// iterations have been reached
				do
				{
					sampledSet = getSample(dataSet, samplePct, biasToUniformClass, includeAllNonzero, factors);
					if (retryCount == 0)
					{
						logger.info(String.format(label + ": Sampling (retries: " + retryCount + ") for final fit/test (%.1f%%): " + sampledSet.numInstances(),
								samplePct));
					} else
					{
						logger.info(String.format(label + ": Resampling (retries: " + retryCount + ") for final fit/test (%.1f%%): "
								+ sampledSet.numInstances() + ", Difference from tests: " + errorRateDiff, samplePct));
					}
					if (FLATTEN_EXTREMES)
					{
						logger.info(label + ": Flattening Extreme Column Values, Retry Count: " + retryCount);
						if (classModel)
						{
							flattenExtremes(sampledSet, FLATTEN_PCT, 1, numAttributes - 1);
						} else
						{
							flattenExtremes(sampledSet, FLATTEN_PCT, 0, numAttributes - 1);
						}
					}
					// Conduct cross-validation
					Instances newSampledSet = fixUpMissingValues(sampledSet, curBest.getClassifier());
					eval = new Evaluation(newSampledSet);
					eval.crossValidateModel(curBest.getClassifier(), newSampledSet, CROSS_VALIDATION_FOLDS, rand, modelParams.MaxComputationThreads);
					if (classModel)
					{
						errorRateDiff = eval.errorRate() - curBest.getErrorRate();
						maxErrorDiff = MAX_ERROR_RATE_DIFF;
					} else
					{
						errorRateDiff = eval.rootRelativeSquaredError() - curBest.getRootRelativeSquaredError();
						maxErrorDiff = MAX_SQ_ERROR_DIFF;
					}
				} while ((errorRateDiff > maxErrorDiff) && ((++retryCount) < MAX_SAMPLE_RETRIES));
				
				curBest.setEvaluation(classModel, eval);
				logger.info(label + ": " + curBest.getErrorString());
				logger.info(label + ": Building full model");
				Instances newSampledSet = fixUpMissingValues(sampledSet, curBest.getClassifier());
				curBest.getClassifier().buildClassifier(newSampledSet);
			} else
			{
				// Conduct cross-validation
				Evaluation eval = new Evaluation(dataSet);
				logger.info(label + ": Using full data set " + dataSet.numInstances());
				if (FLATTEN_EXTREMES)
				{
					logger.info(label + ": Flattening Extreme Column Values");
					if (classModel)
					{
						flattenExtremes(dataSet, FLATTEN_PCT, 1, numAttributes - 1);
					} else
					{
						flattenExtremes(dataSet, FLATTEN_PCT, 0, numAttributes - 1);
					}
				}
				Instances newDataSet = fixUpMissingValues(dataSet, curBest.getClassifier());
				eval.crossValidateModel(curBest.getClassifier(), newDataSet, CROSS_VALIDATION_FOLDS, rand, modelParams.MaxComputationThreads);
				curBest.setEvaluation(classModel, eval);
				logger.info(label + ": " + curBest.getErrorString());
				logger.info(label + ": Building full model");
				curBest.getClassifier().buildClassifier(newDataSet);
			}
			
			// certain Ridor model results are bad, specifically if the class for the default rule is TRUE
			// need to detect for this and remove the model and retry
			Classifier cl = curBest.getClassifier();
			if (cl instanceof Ridor)
			{
				logger.debug(cl.toString());
				if (((Ridor) cl).isUnusable())
				{
					logger.warn("Removing unusable Ridor model - we only allow Excepts to be TRUE");
					// remove Ridor (105) from the classifierList
					for (int group = 0; group < classifierList.length; group++)
					{
						int[] modelList = classifierList[group];
						for (int model = 0; model < modelList.length; model++)
						{
							if (classifierList[group][model] == RIDOR_MODEL)
							{
								classifierList[group][model] = MODEL_REMOVED;
							}
						}
					}
					// call recursively with the Ridor model removed
					curBest = null;
					return findBestFit();
				}
			}
			curBest.setLabel(label);
			curBest.setTime(System.currentTimeMillis() - curTime);
			logger.info(label + ": Time taken: " + (System.currentTimeMillis() - curTime) + "ms");
			return (curBest);
		} catch (Exception e)
		{
			logger.error(label + ": Classification Exception: " + e.toString(), e);
			return (null);
		}
	}

	private String getModelString(int classifier)
	{
		switch (classifier)
		{
		case MODEL_REMOVED:
			return ("Model dynamically removed from the list");
		case 0:
			return ("1st Order SMO Regression");
		case 1:
			return ("2nd Order SMO Regression (lower terms)");
		case 2:
			return ("3rd Order SMO Regression (lower terms)");
		case 3:
			return ("2nd Order SMO Regression (no lower terms)");
		case 4:
			return ("3rd Order SMO Regression (no lower terms)");
		case 5:
			return ("4th Order SMO Regression (lower terms)");
		case 6:
			return ("4th Order SMO Regression (no lower terms)");
		case 7:
			return ("Low complexity boosting");
		case 8:
			return ("High complexity boosting");
		case 9:
			return ("Linear Regression");
		case 10:
			return ("Multi-layer Perceptron (Neural Net)");
		case 11:
			return ("M5 Rules");
		case 12:
			return ("Reduced Error Pruning Tree");
		case 13:
			return ("RBF SMO Regression");
		case 15:
			return ("LibSVM SVM Regression (nu-SVR with defaults)");
		case 20:
			return ("1st Order SVM Regression");
		case 21:
			return ("2nd Order SVM Regression (lower terms)");
		case 22:
			return ("3rd Order SVM Regression (lower terms)");
		case 23:
			return ("2nd Order SVM Regression (no lower terms)");
		case 24:
			return ("3rd Order SVM Regression (no lower terms)");
		case 25:
			return ("4th Order SVM Regression (lower terms)");
		case 26:
			return ("4th Order SVM Regression (no lower terms)");
		case 27:
			return ("Low complexity boosting - SVM");
		case 28:
			return ("High complexity boosting - SVM");
		case 100:
			return ("1st Order SMO Classification");
		case 101:
			return ("2nd Order SMO Classification (no lower terms)");
		case 102:
			return ("2nd Order SMO Classification (lower terms)");
		case 103:
			return ("Decision Tree (J48) Classifier (Explanable)");
		case 104:
			return ("Logistic Regression Classifier");
		case RIDOR_MODEL:
			return ("RIpple DOwn Rule Classifier (Explanable)");
		case 150:
			return ("LibSVM SVM Classification (C-SVC with defaults)");
		}
		return ("Unknown Model");
	}

	/**
	 * fix up data sets that have missing values for classifiers that can not handle them
	 */
	private Instances fixUpMissingValues(Instances dataSet, Classifier c)
	{
		if (dataSet == null)
			return null;
		try
		{
			Capabilities cap = new Capabilities(null);
			cap.enable(Capabilities.Capability.MISSING_VALUES);
			if (!c.getCapabilities().supports(cap))
			{
				// create sets with missing instances replaced (for possible use with LibSVM)
				logger.info(label + ": classifier does not support missing values, using datasets with the missing values replaced");
				Filter filter = new weka.filters.unsupervised.attribute.ReplaceMissingValues();
				if (!filter.setInputFormat(dataSet))
				{
					throw new Exception("ReplaceMissingValues setInputFormat failed");
				}
				return ReplaceMissingValues.useFilter(dataSet, filter);
			} else
			{
				return dataSet;
			}
		} catch (Exception ex)
		{
			logger.error(ex.getMessage(), ex);
			return dataSet;
		}
	}

	/**
	 * Thread to try a classifier on the given data set
	 * 
	 * @author bpeters
	 * 
	 */
	private class TryClassifierThread extends Thread
	{
		Instances totalSample;
		Instances sample;
		Instances test;
		boolean classModel;
		boolean useOnlyExplanable;

		public TryClassifierThread(Instances totalSample, Instances sample, Instances test, boolean classModel, boolean useOnlyExplanable)
		{
			this.setName("Try Classifier - " + this.getId());
			this.totalSample = totalSample;
			this.sample = sample;
			this.test = test;
			this.classModel = classModel;
			this.useOnlyExplanable = useOnlyExplanable;
		}

		public void run()
		{
			Random rand = new Random(System.currentTimeMillis());
					
			while (true)
			{
				int classifier = getNextModel();
				if (classifier < 0)
				{
					return;
				}
				Classifier c = null;
				SMOreg smor = null;
				SMO smo = null;
				LibSVM libsvm = null;
				SVMreg svmr = null;
				M5Rules m5 = null;
				NormalizedPolyKernel npk = null;
				PolyKernel pk = null;
				RBFKernel rk = null;
				J48 j48 = null;
				Ridor ridor = null;
				Logistic log = null;
				REPTree rep = null;
				try
				{
					switch (classifier)
					{
					// Regression Models
					case 0:
						smor = new SMOreg();
						pk = new PolyKernel();
						pk.setExponent(1);
						pk.setUseLowerOrder(true);
						smor.setKernel(pk);
						c = smor;
						break;
					case 1:
						smor = new SMOreg();
						npk = new NormalizedPolyKernel();
						npk.setExponent(2);
						npk.setUseLowerOrder(true);
						smor.setKernel(npk);
						c = smor;
						break;
					case 2:
						smor = new SMOreg();
						npk = new NormalizedPolyKernel();
						npk.setExponent(3);
						npk.setUseLowerOrder(true);
						smor.setKernel(npk);
						c = smor;
						break;
					case 3:
						smor = new SMOreg();
						npk = new NormalizedPolyKernel();
						npk.setExponent(2);
						npk.setUseLowerOrder(false);
						smor.setKernel(npk);
						c = smor;
						break;
					case 4:
						smor = new SMOreg();
						npk = new NormalizedPolyKernel();
						npk.setExponent(3);
						npk.setUseLowerOrder(false);
						smor.setKernel(npk);
						c = smor;
						break;
					case 5:
						smor = new SMOreg();
						npk = new NormalizedPolyKernel();
						npk.setExponent(4);
						npk.setUseLowerOrder(true);
						smor.setKernel(npk);
						c = smor;
						break;
					case 6:
						smor = new SMOreg();
						pk = new NormalizedPolyKernel();
						pk.setExponent(4);
						pk.setUseLowerOrder(true);
						smor.setKernel(pk);
						c = smor;
						break;
					case 7:
						if (getCurBest().getClassifier().getClass() == SMOreg.class)
						{
							smor = (SMOreg) Classifier.makeCopies(getCurBest().getClassifier(), 1)[0];
							smor.setC(0.7);
							c = smor;
						}
						break;
					case 8:
						if (getCurBest().getClassifier().getClass() == SMOreg.class)
						{
							smor = (SMOreg) Classifier.makeCopies(getCurBest().getClassifier(), 1)[0];
							smor.setC(1.5);
							c = smor;
						}
						break;
					case 9:
						LinearRegression lr = new LinearRegression();
						c = lr;
						break;
					case 10:
						// MLP performance is abysmal with too many attributes
						if (totalSample.numAttributes() <= MAX_MLP_ATTRIBUTES)
						{
							MultilayerPerceptron mlp = new MultilayerPerceptron();
							mlp.setRandomSeed(System.currentTimeMillis());
							mlp.setTrainingTime(750);
							c = mlp;
						} else
						{
							logger.info(label + ": Too many attributes (" + totalSample.numAttributes() + " >= " + MAX_MLP_ATTRIBUTES + ") - ignoring multi-layer perceptron (neural net)");
							c = null;
						}
						break;
					case 11:
						m5 = new M5Rules();
						m5.setBuildRegressionTree(false);
						m5.setUnpruned(false);
						c = m5;
						break;
					case 12:
						rep = new REPTree();
						c = rep;
						break;
					case 13:
						smor = new SMOreg();
						rk = new RBFKernel();
						smor.setKernel(rk);
						c = smor;
						break;
					case 15:
						// defaults to RBF kernel
						libsvm = new LibSVM();
						libsvm.setSVMType(new SelectedTag(LibSVM.SVMTYPE_NU_SVR, LibSVM.TAGS_SVMTYPE));
						libsvm.setNormalize(true);
						c = libsvm;
						break;
					case 20:
						svmr = new SVMreg();
						pk = new PolyKernel();
						pk.setExponent(1);
						pk.setUseLowerOrder(true);
						svmr.setKernel(pk);
						c = svmr;
						break;
					case 21:
						svmr = new SVMreg();
						npk = new NormalizedPolyKernel();
						npk.setExponent(2);
						npk.setUseLowerOrder(true);
						svmr.setKernel(npk);
						c = svmr;
						break;
					case 22:
						svmr = new SVMreg();
						npk = new NormalizedPolyKernel();
						npk.setExponent(3);
						npk.setUseLowerOrder(true);
						svmr.setKernel(npk);
						c = svmr;
						break;
					case 23:
						svmr = new SVMreg();
						npk = new NormalizedPolyKernel();
						npk.setExponent(2);
						npk.setUseLowerOrder(false);
						svmr.setKernel(npk);
						c = svmr;
						break;
					case 24:
						svmr = new SVMreg();
						npk = new NormalizedPolyKernel();
						npk.setExponent(3);
						npk.setUseLowerOrder(false);
						svmr.setKernel(npk);
						c = smor;
						break;
					case 25:
						svmr = new SVMreg();
						npk = new NormalizedPolyKernel();
						npk.setExponent(4);
						npk.setUseLowerOrder(true);
						svmr.setKernel(npk);
						c = svmr;
						break;
					case 26:
						svmr = new SVMreg();
						pk = new PolyKernel();
						pk.setExponent(4);
						pk.setUseLowerOrder(true);
						svmr.setKernel(pk);
						c = svmr;
						break;
					case 27:
						if (getCurBest().getClassifier().getClass() == SVMreg.class)
						{
							svmr = (SVMreg) Classifier.makeCopies(getCurBest().getClassifier(), 1)[0];
							svmr.setC(0.7);
							c = svmr;
						}
						break;
					case 28:
						if (getCurBest().getClassifier().getClass() == SVMreg.class)
						{
							svmr = (SVMreg) Classifier.makeCopies(getCurBest().getClassifier(), 1)[0];
							svmr.setC(1.5);
							c = svmr;
						}
						break;
						
					// Classification Models
					case 100:
						smo = new SMO();
						pk = new PolyKernel();
						pk.setExponent(1);
						pk.setUseLowerOrder(true);
						smo.setKernel(pk);
						c = smo;
						break;
					case 101:
						smo = new SMO();
						npk = new NormalizedPolyKernel();
						npk.setExponent(2);
						npk.setUseLowerOrder(true);
						smo.setKernel(npk);
						c = smo;
						break;
					case 102:
						smo = new SMO();
						npk = new NormalizedPolyKernel();
						npk.setExponent(3);
						npk.setUseLowerOrder(true);
						smo.setKernel(npk);
						c = smo;
						break;
					case 103:
						j48 = new J48();
						c = j48;
						break;
					case 104:
						log = new Logistic();
						c = log;
						break;
					case RIDOR_MODEL:
						ridor = new Ridor();
						c = ridor;
						break;
					case 150:
						libsvm = new LibSVM();
						libsvm.setSVMType(new SelectedTag(LibSVM.SVMTYPE_C_SVC, LibSVM.TAGS_SVMTYPE));
						libsvm.setNormalize(true);
						c = libsvm;
						break;
					case MODEL_REMOVED:
						logger.debug("Skipping removed model");
						c = null; // skip this model, was dynamically removed
						break;
					}
					Evaluation ev = null;
					if (c != null)
					{
						// skip models that are no explanable if the requirement is that they be explanable
						if (classifier >= 100 && useOnlyExplanable && !c.isExplanable())
						{
							logger.warn("Skipping model that is not explanable: " + getModelString(classifier));
							continue;
						}
						
						long t_start = System.currentTimeMillis();
						Instances total = fixUpMissingValues(totalSample, c);
						Instances smp = fixUpMissingValues(sample, c);
						Instances tst = fixUpMissingValues(test, c);
						if (CROSS_VALIDATE_TESTING)
						{
							ev = new Evaluation(total);
							ev.crossValidateModel(c, total, NUM_TEST_FOLDS, rand, modelParams.MaxComputationThreads);
						} else
						{
							ev = new Evaluation(smp);
							c.buildClassifier(smp);
							ev.evaluateModel(c, tst);
						}
						long t_delta = System.currentTimeMillis() - t_start; // only works if # threads < #
						// processors
						BestFitResult bfr = new BestFitResult();
						bfr.setClassifier(c);
						bfr.setEvaluation(classModel, ev);
						bfr.setTime(t_delta);
						setCurBest(bfr, classifier, classModel);
					}
				} catch (Exception ex)
				{
					logger.error(ex.toString(), ex);
				}
			}
		}
	}

	/**
	 * Use appropriate sampling method depending on size. If small, use sampling (faster), but if large percentage, use
	 * sample without replacement to ensure maximum coverage of data set
	 * 
	 * @param dataSet
	 * @param samplePct
	 * @return
	 * @throws Exception
	 */
	public static Instances getSample(Instances dataSet, double samplePct, boolean biasToUniform, boolean includeAllNonzero, OutcomeFactor[] factors)
			throws Exception
	{
		if (samplePct <= 15 || biasToUniform)
		{
			// If spread sampling
			if (biasToUniform)
			{
				// Add to sample size to include room for evaluation sample
				weka.filters.supervised.instance.Resample re = new weka.filters.supervised.instance.Resample();
				/* If biasing for a uniform distribution, add an extra class attribute */
				int cl = dataSet.classIndex();
				if (factors != null)
				{
					int numClasses = (int) Math.pow(2, factors.length);
					FastVector values = new FastVector(numClasses);
					for (int i = 0; i < numClasses; i++)
						values.addElement(Integer.toString(i));
					Attribute a = new Attribute("UNIFORMCLASS", values);
					dataSet.insertAttributeAt(a, dataSet.numAttributes());
					int[] indices = Outcome.getFactorIndices(dataSet, factors);
					dataSet.setClassIndex(dataSet.numAttributes() - 1);
					for (int i = 0; i < dataSet.numInstances(); i++)
					{
						int numClass = -1;
						for (int j = 0; j < indices.length; j++)
						{
							if (indices[j] >= 0)
							{
								double val = dataSet.instance(i).value(indices[j]);
								if (!(Double.isNaN(val) || val == 0))
								{
									int classAdd = (int) Math.pow(2, j);
									numClass = numClass < 0 ? classAdd : (numClass + classAdd);
								}
							}
						}
						if (numClass >= 0)
							dataSet.instance(i).setClassValue(numClass);
						else
							dataSet.instance(i).setClassMissing();
					}
				}
				re.setBiasToUniformClass(1);
				re.setSampleSizePercent(samplePct);
				re.setInputFormat(dataSet);
				re.setRandomSeed((int) System.currentTimeMillis());
				Instances result = Filter.useFilter(dataSet, re);
				if (factors != null)
				{
					dataSet.setClassIndex(cl);
					result.setClassIndex(cl);
					result.deleteAttributeAt(result.numAttributes() - 1);
					dataSet.deleteAttributeAt(dataSet.numAttributes() - 1);
				}
				return (result);
			} else
			{
				// Add to sample size to include room for evaluation sample
				Resample re = new Resample();
				re.setSampleSizePercent(samplePct);
				re.setInputFormat(dataSet);
				re.setRandomSeed((int) System.currentTimeMillis());
				Instances result = Filter.useFilter(dataSet, re);
				// If need to add sparsely non-zero instances
				if (dataSet.attribute(0).isNumeric() && includeAllNonzero)
				{
					// First, remove existing non-zeros (avoid duplicates - and minimize oversampling)
					for (int i = 0; i < result.numInstances(); i++)
					{
						if (result.instance(i).value(0) != 0)
						{
							result.delete(i--);
						}
					}
					// Now, add all non-zero instances
					for (int i = 0; i < dataSet.numInstances(); i++)
					{
						if (dataSet.instance(i).value(0) != 0)
						{
							result.add(dataSet.instance(i));
						}
					}
					// If too many non-zero, back off sample size target
					if ((samplePct / 100.0) * ((double) dataSet.numInstances()) * 1.25 < result.numInstances())
					{
						// Add to sample size to include room for evaluation sample
						return (WekaUtil.sampleWithoutReplacement(result, samplePct * 1.25 * ((double) dataSet.numInstances()) / ((double) result.numInstances()),
								System.currentTimeMillis()));
					}
				}
				return (result);
			}
		} else
		{
			// Add to sample size to include room for evaluation sample
			return (WekaUtil.sampleWithoutReplacement(dataSet, samplePct, System.currentTimeMillis()));
		}
	}

	/**
	 * Flatten extreme attribute values in a dataset
	 * 
	 * @param dataSet
	 * @param percentToRemove
	 * @param minIndex
	 * @param maxIndex
	 */
	public void flattenExtremes(Instances dataSet, double percentToRemove, int minIndex, int maxIndex)
	{
		int numInstances = dataSet.numInstances();
		int minRemove = (int) ((percentToRemove / 2) * numInstances);
		int maxRemove = (int) (numInstances - ((percentToRemove / 2) * numInstances));
		StringBuilder sb = new StringBuilder();
		
		for (int i = minIndex; i <= maxIndex; i++)
		{
			List<InstanceValue> list = new ArrayList<InstanceValue>(maxIndex - minIndex + 1);
			HashSet<Double> unique = new HashSet<Double>();
			for (int j = 0; j < numInstances; j++)
			{
				InstanceValue iv = new InstanceValue();
				iv.index = j;
				iv.value = dataSet.instance(j).value(i);
				list.add(iv);
				unique.add(iv.value);
			}
			
			if (unique.size() < 3)
			{
				if (logger.isDebugEnabled())
				{
					if (sb.length() > 0)
						sb.append(",");
					sb.append(dataSet.attribute(i).name());
				}
				continue;
			}
			
			Collections.sort(list);

			double minVal = dataSet.instance(list.get(minRemove + 1).index).value(i);
			double maxVal = dataSet.instance(list.get(maxRemove - 1).index).value(i);
			for (int j = minRemove; j >= 0; j--)
			{
				InstanceValue iv = (InstanceValue) list.get(j);
				dataSet.instance(iv.index).setValue(i, minVal);
			}
			for (int j = maxRemove; j < numInstances; j++)
			{
				InstanceValue iv = (InstanceValue) list.get(j);
				dataSet.instance(iv.index).setValue(i, maxVal);
			}
		}
		if (logger.isDebugEnabled() && sb.length() > 0)
			logger.debug("Not flattening the following binary attributes: " + sb.toString());
	}

	/**
	 * @param biasToUniformClass
	 *            The biasToUniformClass to set.
	 */
	public void setBiasToUniformClass(boolean biasToUniformClass)
	{
		this.biasToUniformClass = biasToUniformClass;
	}

	/**
	 * @param includeAllNonzero
	 *            The includeAllNonzero to set.
	 */
	public void setIncludeAllNonzero(boolean includeAllNonzero)
	{
		this.includeAllNonzero = includeAllNonzero;
	}

	/**
	 * @param factors
	 *            the factors to set
	 */
	public void setFactors(OutcomeFactor[] factors)
	{
		this.factors = factors;
	}
}
