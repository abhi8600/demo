/**
 * $Id: BestFitResult.java,v 1.10 2011-09-07 21:19:11 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.io.Serializable;

import org.apache.log4j.Logger;

import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;

/**
 * Class to hold the result of a best fit classifier run
 * 
 * @author bpeters
 * 
 */
public class BestFitResult implements Serializable
{
	private static Logger logger = Logger.getLogger(BestFitResult.class);
	private static final long serialVersionUID = 1L;
	private String label;
	private Classifier classifier;
	private double relativeAbsoluteError;
	private double rootRelativeSquaredError;
	private double rootMeanSquaredError;
	private double meanAbsoluteError;
	private double errorRate;
	private double falsePositiveRate;
	private double falseNegativeRate;
	private double correlationCoefficient;
	private boolean classModel;
	private long timeInMilliseconds;

	public void setEvaluation(boolean classModel, Evaluation eval)
	{
		this.classModel = classModel;
		if (!classModel)
		{
			try
			{
				this.relativeAbsoluteError = eval.relativeAbsoluteError();
				this.correlationCoefficient = eval.correlationCoefficient();
			} catch (Exception ex)
			{
				logger.error(ex.toString());
			}
		}
		
		this.rootRelativeSquaredError = eval.rootRelativeSquaredError();
		this.rootMeanSquaredError = eval.rootMeanSquaredError();
		this.meanAbsoluteError = eval.meanAbsoluteError();
		this.errorRate = eval.errorRate();
		if (classModel)
		{
			try
			{
				this.falsePositiveRate = eval.falsePositiveRate(1);
				this.falseNegativeRate = eval.falseNegativeRate(1);
			} catch (Exception ex)
			{
				logger.error(ex.toString());
			}
		}
	}

	/**
	 * @return Returns the classifier.
	 */
	public Classifier getClassifier()
	{
		return classifier;
	}

	public synchronized Classifier getClassifierCopy() throws Exception
	{
		synchronized (classifier)
		{
			return (Classifier.makeCopy(classifier));
		}
	}

	/**
	 * @param classifier
	 *            The classifier to set.
	 */
	public void setClassifier(Classifier classifier)
	{
		this.classifier = classifier;
	}
	
	public void setLabel(String label)
	{
		this.label = label;
	}
	
	public String getLabel()
	{
		return label;
	}
	
	/**
	 * set the time to build/eval the classifer
	 * @param tm
	 */
	public void setTime(long tm)
	{
		this.timeInMilliseconds = tm;
	}

	/**
	 * get the time to build/eval the classifer
	 * @return
	 */
	public long getTime()
	{
		return this.timeInMilliseconds;
	}
	
	/**
	 * @return Returns the errorRate.
	 */
	public double getErrorRate()
	{
		return errorRate;
	}

	/**
	 * @return Returns the falsePositiveRate.
	 */
	public double getFalsePositiveRate()
	{
		return falsePositiveRate;
	}

	/**
	 * @return Returns the falseNegativeRate.
	 */
	public double getFalseNegativeRate()
	{
		return falseNegativeRate;
	}

	/**
	 * @return Returns the relativeAbsoluteError.
	 */
	public double getRelativeAbsoluteError()
	{
		return relativeAbsoluteError;
	}

	/**
	 * @return Returns the rootRelativeSquaredError.
	 */
	public double getRootRelativeSquaredError()
	{
		return rootRelativeSquaredError;
	}

	/**
	 * @return Returns the correlationCoefficient.
	 */
	public double getCorrelationCoefficient()
	{
		return correlationCoefficient;
	}

	/**
	 * @return Returns the meanAbsoluteError.
	 */
	public double getMeanAbsoluteError()
	{
		return meanAbsoluteError;
	}

	/**
	 * @return Returns the rootMeanSquaredError.
	 */
	public double getRootMeanSquaredError()
	{
		return rootMeanSquaredError;
	}
	
	public boolean isClassModel()
	{
		return classModel;
	}

	public String getErrorString()
	{
		if (classModel)
		{
			return (String.format("ER: %.1f%%, FP Class ER: %.1f%%, FN Class ER: %.1f%%, Build/Eval Time: %dms", 100.0*this.errorRate, 100.0*falsePositiveRate,
					100.0*falseNegativeRate, timeInMilliseconds));
		} else
		{
			return (String.format("RAE: %.1f%%, RRSE: %.1f%%, RMSE: %.4f, MAE: %.4f, Corr: %.1f%%, Build/Eval Time: %dms", relativeAbsoluteError, rootRelativeSquaredError,
					rootMeanSquaredError, meanAbsoluteError, correlationCoefficient*100.0, timeInMilliseconds));
		}
	}
}
