/**
 * $Id: Peers.java,v 1.42 2012-05-21 13:22:23 mpandit Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import org.apache.log4j.Logger;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import com.successmetricsinc.Repository;
import com.successmetricsinc.query.Database;
import com.successmetricsinc.engine.ReferencePopulation;
import com.successmetricsinc.engine.ReferencePopulation.PeerItem;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.DimensionColumn;
import com.successmetricsinc.query.GroupBy;
import com.successmetricsinc.query.Level;
import com.successmetricsinc.query.Query;
import com.successmetricsinc.query.LevelKey;
import com.successmetricsinc.util.Util;

/**
 * Class to generate peer sets
 * 
 * @author Brad Peters
 * 
 */
public class Peers
{
	private static Logger logger = Logger.getLogger(Peers.class);
	private ReferencePopulation referencePopulation;
	private Repository r;
	private DatabaseConnection dconn;
	private Connection conn;
	private PrintWriter writer = null;
	private String targetDimension;
	private String level;

	public Peers(Repository r, String name) throws SQLException, Exception
	{
		this.r = r;
		dconn = r.getDefaultConnection();
		conn = dconn.ConnectionPool.getConnection();
		referencePopulation = r.findReferencePopulation(name);
		if (referencePopulation == null)
		{
			throw new Exception("Invalid reference population name: " + name);
		}
		targetDimension = (referencePopulation.TargetDimension != null) ? referencePopulation.TargetDimension : r.getTargetDimension();
		level = referencePopulation.Level;
	}

	/**
	 * Generate peer sets
	 * 
	 * @param performance
	 *            Whether to generate performance peers or success pattern peers
	 */
	public void generatePeers(boolean performance, boolean writeTable, boolean writeCSV, String path)
	{
		boolean auto = true;
		try
		{
			conn = dconn.ConnectionPool.getConnection();
			Statement stmt = conn.createStatement();
			List<String> outputColumns = new ArrayList<String>();
			List<String> outputColumnTypes = new ArrayList<String>();
			// Set output based on type of peer group
			outputColumns.add("ID1");
			outputColumnTypes.add("Varchar(30)");
			outputColumns.add("ID2");
			outputColumnTypes.add("Varchar(30)");
			outputColumns.add("DIST");
			outputColumnTypes.add("float");
			String outputTable = referencePopulation.getTableName(performance);
			if (!performance)
			{
				outputColumns.add("RANK");
				outputColumnTypes.add("float");
				outputColumns.add("VAL");
				outputColumnTypes.add("float");
			}
			if (writeCSV)
			{
				writer = new PrintWriter(new FileWriter(path + File.separator + outputTable + ".txt"));
				for (int i = 0; i < outputColumns.size(); i++)
				{
					if (i > 0)
						writer.print("\t");
					writer.print(outputColumns.get(i));
				}
				writer.println("");
			}
			if (writeTable)
			{
				// XXX should not drop if exists, that should signal that we can skip this step... or history...
				Database.createTable(r, dconn, outputTable, outputColumns, outputColumnTypes, true, false, null, null, null);
			}

			double totalWeight = 0;
			/*
			 * Generate the query to return all peering columns by target dimension key
			 */
			Query q = new Query(r);
			if (level == null || level.length() == 0)
			{
				LevelKey lk = r.getDimensionKey(targetDimension);
				if (lk == null)
					logger.fatal("No level key in peering for dimension " + targetDimension);
				if (lk.ColumnNames.length > 1)
					logger.error("In peering the dimension key contains more than one key column: " + targetDimension);
				logger.debug("adding (attribute - no ref pop level): " + targetDimension + ":" + lk.ColumnNames[0]);
				q.addDimensionColumn(targetDimension, lk.ColumnNames[0]);
				q.addGroupBy(new GroupBy(targetDimension, lk.ColumnNames[0]));
			} else
			{
				Level lvl = r.findDimensionLevel(targetDimension, level);
				if (lvl == null)
					logger.fatal("Invalid level for peering " + targetDimension + ":" + level);
				LevelKey col = lvl.getPrimaryKey();
				logger.debug("adding (attribute - at ref pop level (" + level + "): " + targetDimension + ":" + col.ColumnNames[0]);
				q.addDimensionColumn(targetDimension, col.ColumnNames[0]);
				q.addGroupBy(new GroupBy(targetDimension, col.ColumnNames[0]));
			}
			q.addFilterList(referencePopulation.getPeeringFilters());
			// Add attribute columns and sum all weights
			PeerItem[] itemArray = referencePopulation.getPeerColumns(performance);
			for (int count = 0; count < itemArray.length; count++)
			{
				if (itemArray[count].Type == 0)
				{
					logger.debug("adding (peer column - attribute): " + targetDimension + ":" + itemArray[count].Column);
					q.addDimensionColumn(targetDimension, itemArray[count].Column);
					q.addGroupBy(new GroupBy(targetDimension, itemArray[count].Column));
				} else
				{
					logger.debug("adding (peer column - measure): " + itemArray[count].Column);
					q.addMeasureColumn(itemArray[count].Column, itemArray[count].Column, false);
				}
				totalWeight += itemArray[count].Weight;
			}
			if (!performance)
			{
				logger.debug("adding (success measure): " + referencePopulation.getSuccessMeasure());
				q.addMeasureColumn(referencePopulation.getSuccessMeasure(), referencePopulation.getSuccessMeasure(), false);
			}
			logger.debug(q.getLogicalQueryString(false, null, false));			
			q.generateQuery(false);
			logger.debug(q.getPrettyQuery());
			// Generate the first SQL pass to get a list of all targets
			ResultSet mainRs = stmt.executeQuery(q.getQuery());
			// Generate type list
			int[] typeList = new int[itemArray.length];
			for (int count = 0; count < itemArray.length; count++)
			{
				// Get the dimension column record
				int type = 0;
				if (itemArray[count].Type == 0)
				{
					DimensionColumn dc = r.findDimensionColumn(targetDimension, itemArray[count].Column);
					if (dc == null)
						logger.fatal("Invalid attribute in peering: " + targetDimension + ":" + itemArray[count].Column);
					type = (dc.DataType.equals("Integer") || dc.DataType.equals("Number")) ? 0 : 1;
				} else
				{
					type = 0;
				}
				typeList[count] = type;
			}
			HashMap<String, List<Object>> rows = new HashMap<String, List<Object>>();
			double[] sumList = new double[itemArray.length];
			double[] sumsqList = new double[itemArray.length];
			double[] avgList = new double[itemArray.length];
			double[] stddevList = new double[itemArray.length];
			int cnt = 0;
			// Get result set
			while (mainRs.next())
			{
				List<Object> columns = new ArrayList<Object>();
				String curKey = mainRs.getString(1);
				for (int count = 0; count < itemArray.length; count++)
				{
					// Add the appropriate formula for numbers
					if (typeList[count] == 0) // Numeric
					{
						double val = mainRs.getDouble(count + 2);
						sumList[count] += val;
						sumsqList[count] += val * val;
						columns.add(new Double(val));
					} else
					{
						columns.add(mainRs.getString(count + 2));
					}
				}
				if (!performance)
				{
					columns.add(new Double(mainRs.getDouble(itemArray.length + 2)));
				}
				rows.put(curKey, columns);
				cnt++;
			}
			mainRs.close();
			stmt.close();
			logger.info("Processed " + cnt + " items from the query");
			// Generate standard deviations and means for numeric values
			for (int count = 0; count < itemArray.length; count++)
			{
				avgList[count] = sumList[count] / rows.size();
				stddevList[count] = Math.sqrt(sumsqList[count] / rows.size() - (avgList[count] * avgList[count]));
			}
			// Now generate a list of peers for each result
			Iterator<Entry<String, List<Object>>> it = rows.entrySet().iterator();
			String insertQuery = null;
			PreparedStatement pstmt = null;
			if (writeTable)
			{
				auto = conn.getAutoCommit();
				conn.setAutoCommit(false);
				insertQuery = "INSERT INTO " + Database.getQualifiedTableName(dconn, Util.replaceWithPhysicalString(outputTable));
				insertQuery += " VALUES (?,?,?" + (performance ? "" : ",?,?") + ")";
				logger.debug(insertQuery);
				pstmt = conn.prepareStatement(insertQuery);
			}
			int peers = 0;
			while (it.hasNext())
			{
				Entry<String, List<Object>>  me = it.next();
				List<ValRanker> list = new ArrayList<ValRanker>();
				List<ValRanker> plist = new ArrayList<ValRanker>();
				String ID = (String) me.getKey();
				List<Object> columns = me.getValue();
				/*
				 * Iterate through each row, calculating distance and adding to sorted tree map
				 */
				Iterator<Entry<String, List<Object>>> it2 = rows.entrySet().iterator();
				while (it2.hasNext())
				{
					Map.Entry<String, List<Object>> me2 = it2.next();
					String ID2 = me2.getKey();
					List<Object> columns2 = me2.getValue();
					double distance = 0;
					for (int i = 0; i < itemArray.length; i++)
					{
						double sqval = 0;
						if (typeList[i] == 0)
						{
							// Only calculate difference for non-deviating items
							if (stddevList[i] != 0)
							{
								double val1 = ((Double) columns.get(i)).doubleValue();
								double val2 = ((Double) columns2.get(i)).doubleValue();
								// Standardize
								val1 = (val1 - avgList[i]) / stddevList[i];
								val2 = (val2 - avgList[i]) / stddevList[i];
								sqval = Math.pow(val1 - val2, 2);
							}
						} else
						{
							if (columns.get(i) == null || columns2.get(i) == null)
							{
								if (columns.get(i) == null && columns2.get(i) == null)
									sqval = 1;
								else
									sqval = 0;
							} else
								sqval = ((String) columns.get(i)).equals((String) columns2.get(i)) ? 1 : 0;
						}
						distance += (itemArray[i].Weight / totalWeight) * sqval;
					}
					distance = Math.sqrt(distance);
					ValRanker vr = new ValRanker();
					vr.val = new Double(distance);
					vr.ID = ID2;
					list.add(vr);
					if (!performance)
					{
						vr = new ValRanker();
						vr.val = (Double) columns2.get(itemArray.length);
						vr.ID = ID2;
						plist.add(vr);
					}
				}
				Collections.sort(list);
				// Remove non-top items from plist
				int limit = referencePopulation.getNumPeers(performance);
				for (int count = limit; count < list.size(); count++)
				{
					ValRanker vr = (ValRanker) list.get(count);
					ValRanker vr2 = null;
					for (int i = 0; i < plist.size(); i++)
					{
						vr2 = (ValRanker) plist.get(i);
						if (vr2.ID.equals(vr.ID))
						{
							plist.remove(i);
							break;
						}
					}
				}
				if (!performance)
				{
					Collections.sort(plist);
				}
				for (int count = 0; (count < limit) && (count < list.size()); count++)
				{
					ValRanker vr = (ValRanker) list.get(count);
					if (writeTable)
					{
						pstmt.setString(1, ID);
						pstmt.setString(2, vr.ID);
						pstmt.setDouble(3, vr.val.doubleValue());
					}
					if (writeCSV)
					{
						writer.print(ID + "\t" + vr.ID + "\t" + vr.val.doubleValue());
					}
					if (!performance)
					{
						ValRanker vr2 = null;
						for (int i = 0; i < plist.size(); i++)
						{
							vr2 = (ValRanker) plist.get(i);
							if (vr2.ID.equals(vr.ID))
							{
								if (writeTable)
								{
									pstmt.setInt(4, plist.size() - i);
									pstmt.setDouble(5, vr2.val.doubleValue());
								}
								if (writeCSV)
								{
									writer.print("\t" + (plist.size() - i) + "\t" + vr2.val.doubleValue());
								}
								break;
							}
						}
					}
					if (writeTable)
					{
						pstmt.addBatch();
					}
					if (writeCSV)
					{
						writer.println("");
					}
				}
				peers++;
				if ((peers % 100) == 0)
				{
					logger.info("Output " + peers + " peers");
				}
				if (writeTable)
				{
					pstmt.executeBatch();
				}
			}
			if (writeTable)
			{
				pstmt.close();
				conn.commit();
				conn.setAutoCommit(auto);
				conn = null;
			}
			if (writeCSV)
			{
				writer.close();
			}
			logger.info("Dumped peer information for " + peers + " targets");
			// For Success Peers Populate Target Variable and Rank
			/*
			if (!performance && writeTable)
			{
				Query sq = new Query(r);
				LevelKey lk = r.getDimensionKey(targetDimension);
				if (lk.ColumnNames.length > 1)
					logger.error("Dimension key contains more than one key column: " + targetDimension);
				sq.addDimensionColumn(targetDimension, lk.ColumnNames[0], "ID$");
				sq.addGroupBy(new GroupBy(targetDimension, lk.ColumnNames[0]));
				sq.addMeasureColumn(referencePopulation.getSuccessMeasure(), "VAL", false);
				sq.addFilterList(referencePopulation.getPeeringFilters());
				sq.generateQuery(false);
				String subQuery = sq.getQuery();
				String query = null;
				if (r.getDefaultConnection().DBType == Database.DatabaseType.MSSQL || r.getDefaultConnection().DBType == Database.DatabaseType.MSSQL2005|| r.getDefaultConnection().DBType == Database.DatabaseType.MSSQL2008)
				{
					query = "UPDATE " + outputTable + " SET VAL=B.VAL FROM " + outputTable + " A, ";
				} else if (r.getDefaultConnection().DBType == Database.DatabaseType.MYSQL)
				{
					query = "UPDATE " + outputTable + " SET A.VAL=B.VAL " + outputTable + " FROM " + outputTable + " A, ";
				}
				query += "(" + subQuery + ") B WHERE A.ID2=B.ID$";
				logger.debug(Query.getPrettyQuery(query));
				stmt = conn.createStatement();
				stmt.executeUpdate(query);
				stmt.close();
			}
			*/
			if (writeTable)
			{
				Database.createIndex(r.getDefaultConnection(), "ID1", outputTable);
				Database.createIndex(r.getDefaultConnection(), "ID2", outputTable);
			}
		} catch (Exception ex)
		{
			logger.error(ex.toString(), ex);
		} finally
		{
			try
			{
				if (conn != null)
				{
					conn.rollback();
					conn.setAutoCommit(auto);
				}
			} catch (Exception ex)
			{
				logger.error(ex.toString(), ex);
			}
		}
	}

	private static class ValRanker implements Comparable<ValRanker>
	{
		Double val;
		String ID;

		public int compareTo(ValRanker vr)
		{
			return (val.compareTo(vr.val));
		}
	}

	/**
	 * Retrieve a data structure to contain the current peerset
	 * 
	 * @param performancePeers
	 *            Whether to obtain performance or success pattern peers
	 * @param ID
	 *            Id to retrive peers for. If null, retrieve map for all peers
	 * @return
	 * @throws SQLException
	 */
	public Map<String, List<Peer>> retrievePeerMap(boolean performancePeers, String ID) throws SQLException
	{
		String query = "SELECT ID1, ID2" + (performancePeers ? "" : ", VAL") + " FROM "
				+ Database.getQualifiedTableName(dconn, Util.replaceWithPhysicalString(referencePopulation.getTableName(performancePeers)));
		if (ID != null)
			query += " WHERE ID1='" + ID + "'";
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(query);
		HashMap<String, List<Peer>> map = new HashMap<String, List<Peer>>();
		while (rs.next())
		{
			String id1 = rs.getString(1);
			String id2 = rs.getString(2);
			double val = performancePeers ? 0 : rs.getDouble(3);
			List<Peer> list = (List<Peer>) map.get(id1);
			if (list == null)
			{
				list = new ArrayList<Peer>();
				map.put(id1, list);
			}
			Peer p = new Peer();
			p.key = id2;
			p.value = val;
			list.add(p);
		}
		rs.close();
		stmt.close();
		return (map);
	}
}