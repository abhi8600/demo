/**
 * $Id: AdjustedClassifier.java,v 1.2 2007-08-30 16:26:47 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import weka.classifiers.Classifier;
import weka.core.Instance;

/**
 * Class to adjust a classifier to compensate for prediction errors.
 * 
 * @author bpeters
 * 
 */
public class AdjustedClassifier
{
    private double curPredVal;
    private double curVal;
    private double multiplier;
    private double diffVal;
    private boolean diffAdjustMode;
    private Classifier c;

    public AdjustedClassifier(Classifier c, Instance inst) throws Exception
    {
        this.c = c;
        curPredVal = c.classifyInstance(inst);
        curVal = inst.value(0);
        // If the predicted value has a different sign than the real value or the difference is large, adjust using
        // differences, otherwise, adjust using percent differences
        if ((curVal/Math.abs(curPredVal)<0)||((curVal==0 ? 0 : (curVal-curPredVal)/curVal)>0.2))
        {
            diffAdjustMode = true;
            diffVal = curVal-curPredVal;
        } else
        {
            diffAdjustMode = false;
            multiplier = curVal/curPredVal;
        }
    }

    /**
     * Get the predicted value for an instance (adjusting for prediction errors)
     * 
     * @param newInst
     * @return
     * @throws Exception
     */
    public double classifyInstance(Instance newInst) throws Exception
    {
        double val = c.classifyInstance(newInst);
        if (diffAdjustMode)
        {
            val += diffVal;
        } else
        {
            val *= multiplier;
        }
        return (val);
    }
}
