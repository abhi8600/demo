/**
 * $Id: Outcome.java,v 1.30 2010-12-20 12:48:08 mpandit Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jdom.Namespace;

import weka.core.Instances;

import com.successmetricsinc.Repository;
import com.successmetricsinc.query.DisplayFilter;
import com.successmetricsinc.query.QueryFilter;
import com.successmetricsinc.util.RepositoryException;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XmlUtils;

/**
 * Class to define a given business outcome and outcome analysis process.
 * 
 * @author bpeters
 * 
 */
public class Outcome implements Serializable
{
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(Outcome.class);
	public static final int BUCKET_TYPE_VALUE = 0;
	public static final int BUCKET_TYPE_DIFFERENCE = 1;
	public static final int BUCKET_TYPE_PCT_DIFFERENCE = 2;
	public String Name;
	public String SuccessModelName;
	public String OutputTableName;
	public boolean DropExistingTable;
	public String OutcomeGroupName;
	public String OutcomeGroupTableName;
	public String IterationAnalysisValue;
	public String IterationModelValue;
	public boolean EstimateClass;
	public boolean EstimateValue;
	public boolean BiasToUniformClass;
	public boolean EstimateValueInClass;
	public boolean IncludeAllNonzero;
	public boolean EstimateDifferenceFromActual;
	public String TargetMeasure;
	public String ClassMeasure;
	public boolean OutputOnlyClass;
	public boolean UseMinEstimatedValue;
	public double MinEstimatedValue;
	public boolean ExcludeIfBelowMinValue;
	public boolean RelativeMinValue;
	public boolean UseMaxEstimatedValue;
	public double MaxEstimatedValue;
	public boolean ExcludeIfAboveMaxValue;
	public boolean RelativeMaxValue;
	public boolean MinRelativeToMeasure;
	public String MinMeasure;
	public boolean MaxRelativeToMeasure;
	public String MaxMeasure;
	public boolean OutputBuckets;
	public int OutputBucketType; // 0 - Value, 1 - Difference from Actual, 2 - %Difference from Actual
	public MeasureBucket[] Buckets;
	public boolean RankFactors;
	public int NumFactorsToRank;
	public ClassificationRuleSet Ruleset = null;
	public ClassificationRuleSet ExclusionRuleset = null;
	public ClassifierSettings Classifiers;
	public DisplayFilter TrainingFilter = null;
	public DisplayFilter ScoringFilter = null;
	public String NameFilter = null;
	
	/*
	 * Whether to evaluate factors as a group. When evaluating factors, for each instance, each a base and test instance
	 * is evaluated for each factor. If this setting is false, then the base and test instances only differ for the
	 * index of the currently evaluated factor. All other instance values are their current values. When set to true,
	 * all instance values are set to their base value except the factor currently being evaluated.
	 */
	public boolean EvaluateFactorsAsGroup = true;
	public OutcomeFactor[] Factors;
	public boolean OutputActuals = true;
	public boolean OutputBaseAndTestValues = true;
	public boolean OutputPredictedImpacts = true;
	private String trainingFilterName;
	private String scoringFilterName;

	public Outcome()
	{
	}

	public Outcome(Element oe, Namespace ns, Repository r) throws RepositoryException
	{
		Name = oe.getChildTextTrim("Name", ns);
		EstimateClass = Boolean.parseBoolean(oe.getChildTextTrim("EstimateClass", ns));
		EstimateValue = Boolean.parseBoolean(oe.getChildTextTrim("EstimateValue", ns));
		BiasToUniformClass = Boolean.parseBoolean(oe.getChildTextTrim("BiasToUniformClass", ns));
		EstimateValueInClass = Boolean.parseBoolean(oe.getChildTextTrim("EstimateValueInClass", ns));
		IncludeAllNonzero = Boolean.parseBoolean(oe.getChildTextTrim("IncludeAllNonzero", ns));
		EstimateDifferenceFromActual = Boolean.parseBoolean(oe.getChildTextTrim("EstimateDifferenceFromActual", ns));
		IterationAnalysisValue = oe.getChildTextTrim("IterationAnalysisValue", ns);
		IterationModelValue = oe.getChildTextTrim("IterationModelValue", ns);
		OutputTableName = oe.getChildTextTrim("OutputTableName", ns);
		OutcomeGroupName = oe.getChildTextTrim("OutcomeGroupName", ns);
		OutcomeGroupTableName = oe.getChildTextTrim("OutcomeGroupTableName", ns);
		SuccessModelName = oe.getChildTextTrim("SuccessModelName", ns);
		NameFilter = oe.getChildTextTrim("NameFilter", ns);
		if (SuccessModelName == null || SuccessModelName.length() == 0 || r.findSuccessModel(SuccessModelName) == null)
		{
			logger.error("Outcome " + Name + " has an invalid model dataset associated with it: " + SuccessModelName);
			throw new RepositoryException("Outcome " + Name + " has an invalid model dataset associated with it: " + SuccessModelName);
		}
		TargetMeasure = oe.getChildTextTrim("TargetMeasure", ns);
		ClassMeasure = oe.getChildTextTrim("ClassMeasure", ns);
		DropExistingTable = Boolean.parseBoolean(oe.getChildTextTrim("CreateTable", ns));
		OutputOnlyClass = Boolean.parseBoolean(oe.getChildTextTrim("OutputOnlyClass", ns));
		UseMinEstimatedValue = Boolean.parseBoolean(oe.getChildTextTrim("UseMinEstimatedValue", ns));
		MinEstimatedValue = Double.parseDouble(oe.getChildTextTrim("MinEstimatedValue", ns));
		ExcludeIfBelowMinValue = Boolean.parseBoolean(oe.getChildTextTrim("ExcludeIfBelowMinValue", ns));
		RelativeMinValue = Boolean.parseBoolean(oe.getChildTextTrim("RelativeMinValue", ns));
		UseMaxEstimatedValue = Boolean.parseBoolean(oe.getChildTextTrim("UseMaxEstimatedValue", ns));
		MaxEstimatedValue = Double.parseDouble(oe.getChildTextTrim("MaxEstimatedValue", ns));
		ExcludeIfAboveMaxValue = Boolean.parseBoolean(oe.getChildTextTrim("ExcludeIfAboveMaxValue", ns));
		RelativeMaxValue = Boolean.parseBoolean(oe.getChildTextTrim("RelativeMaxValue", ns));
		MinRelativeToMeasure = Boolean.parseBoolean(oe.getChildTextTrim("MinRelativeToMeasure", ns));
		MinMeasure = oe.getChildTextTrim("MinMeasure", ns);
		MaxRelativeToMeasure = Boolean.parseBoolean(oe.getChildTextTrim("MaxRelativeToMeasure", ns));
		MaxMeasure = oe.getChildTextTrim("MaxMeasure", ns);
		OutputBuckets = Boolean.parseBoolean(oe.getChildTextTrim("OutputBuckets", ns));
		OutputBucketType = Integer.parseInt(oe.getChildTextTrim("OutputBucketType", ns));
		String rankFactors = oe.getChildTextTrim("RankFactors", ns);
		if (rankFactors != null)
			RankFactors = Boolean.parseBoolean(rankFactors);
		String numFactors = oe.getChildTextTrim("NumFactorsToRank", ns);
		if (numFactors != null)
			NumFactorsToRank = Integer.parseInt(numFactors);
		Element be = oe.getChild("Buckets", ns);
		if (be != null)
		{
			List l2 = be.getChildren();
			Buckets = new MeasureBucket[l2.size()];
			for (int count2 = 0; count2 < l2.size(); count2++)
			{
				Buckets[count2] = new MeasureBucket((Element) l2.get(count2), ns);
			}
		}
		Element ofe = oe.getChild("Factors", ns);
		if (ofe != null)
		{
			List l2 = ofe.getChildren();
			Factors = new OutcomeFactor[l2.size()];
			for (int count2 = 0; count2 < l2.size(); count2++)
			{
				Factors[count2] = new OutcomeFactor((Element) l2.get(count2), ns);
			}
		}
		Element re = oe.getChild("RuleSet", ns);
		if (re != null)
		{
			List l2 = re.getChildren();
			Ruleset = new ClassificationRuleSet();
			for (int count2 = 0; count2 < l2.size(); count2++)
			{
				try
				{
					ClassificationRule rule = new ClassificationRule((Element) l2.get(count2), ns, r);
					Ruleset.addRule(rule);
				}
				catch (RepositoryException ex)
				{
					// bad rule, just skip it
				}
			}
		}
		Element nre = oe.getChild("ExclusionRuleSet", ns);
		if (nre != null)
		{
			List l2 = nre.getChildren();
			ExclusionRuleset = new ClassificationRuleSet();
			for (int count2 = 0; count2 < l2.size(); count2++)
			{
				try
				{
					ClassificationRule rule = new ClassificationRule((Element) l2.get(count2), ns, r);
					ExclusionRuleset.addRule(rule);
				}
				catch (RepositoryException ex)
				{
					// bad rule, just skip it
				}
			}
		}
		Element clSettings = oe.getChild("Classifiers", ns);
		if (clSettings != null)
		{
			Classifiers = new ClassifierSettings();
			Classifiers.RegressionModels = Repository.getIntArrayChildren(clSettings, "RegressionModels", ns);
			Classifiers.ClassificationModels = Repository.getIntArrayChildren(clSettings, "ClassificationModels", ns);
			Classifiers.MaxSampleInstanceValues = Integer.parseInt(clSettings.getChildText("MaxSampleInstanceValues", ns));
			Classifiers.MaxInstanceValues = Integer.parseInt(clSettings.getChildText("MaxInstanceValues", ns));
		}
		
		String filterName = oe.getChildTextTrim("TrainingFilter", ns);
		trainingFilterName = oe.getChildText("TrainingFilter", ns);
		if (filterName != null && filterName.length() > 0)
		{
			QueryFilter qf = r.findFilter(filterName);
			if (qf == null)
			{
				logger.error("Training filter '" + filterName + "' in outcome '" + Name + "' does not exist");
			}
			else
			{
				TrainingFilter = qf.returnDisplayFilter(r, null);
				if (TrainingFilter == null)
				{
					logger.error("Failure for training filter '" + filterName + "' in outcome '" + Name + "' - could not be converted or is invalid");
				}
			}
		}
		
		filterName = oe.getChildTextTrim("ScoringFilter", ns);
		scoringFilterName = oe.getChildText("ScoringFilter", ns);
		if (filterName != null && filterName.length() > 0)
		{
			QueryFilter qf = r.findFilter(filterName);
			if (qf == null)
			{
				logger.error("Scoring filter '" + filterName + "' in outcome '" + Name + "' does not exist");
			}
			else
			{
				ScoringFilter = qf.returnDisplayFilter(r, null);
				if (ScoringFilter == null)
				{
					logger.error("Failure for scoring filter '" + filterName + "' in outcome '" + Name + "' - could not be converted or is invalid");
				}
			}
		}
		
		// cleanup
		if (!EstimateClass)
			ClassMeasure = null;
		if (!EstimateClass)
			OutputOnlyClass = false;
		
		// for backwards compability
		if (Classifiers == null && SuccessModelName != null)
		{
			SuccessModel sm = r.findSuccessModel(SuccessModelName);
			if (sm.Classifiers != null)
			{
				Classifiers = sm.Classifiers;
				logger.warn("Using the Success Model Dataset defined classifiers for the Outcome: " + Name + ".  Please move your settings to the Outcome or use the defaults.");
			}
		}
		
		if (IterationModelValue == null || IterationModelValue.length() == 0)
			logger.warn("Outcome " + this.Name + " IterationModelValue is null, you probably want to set it using the Administration tool");
	}
	
	public Element getOutcomeElement(Namespace ns)
	{
		Element e = new Element("Outcome",ns);
		
		Element child = new Element("Name",ns);
		child.setText(Name);
		e.addContent(child);
		
		XmlUtils.addContent(e, "SuccessModelName", SuccessModelName,ns);		
		
		XmlUtils.addContent(e, "OutputTableName", OutputTableName,ns);
		
		XmlUtils.addContent(e, "OutcomeGroupName", OutcomeGroupName,ns);
		
		XmlUtils.addContent(e, "OutcomeGroupTableName", OutcomeGroupTableName,ns);
		
		XmlUtils.addContent(e, "IterationAnalysisValue", IterationAnalysisValue,ns);
	
		XmlUtils.addContent(e, "IterationModelValue", IterationModelValue,ns);
		
		child = new Element("EstimateClass",ns);
		child.setText(String.valueOf(EstimateClass));
		e.addContent(child);
		
		child = new Element("EstimateValue",ns);
		child.setText(String.valueOf(EstimateValue));
		e.addContent(child);
		
		child = new Element("EstimateValueInClass",ns);
		child.setText(String.valueOf(EstimateValueInClass));
		e.addContent(child);
		
		child = new Element("IncludeAllNonzero",ns);
		child.setText(String.valueOf(IncludeAllNonzero ));
		e.addContent(child);
		
		child = new Element("EstimateDifferenceFromActual",ns);
		child.setText(String.valueOf(EstimateDifferenceFromActual));
		e.addContent(child);
		
		if (TargetMeasure!=null)
		{
			child = new Element("TargetMeasure",ns);
			child.setText(TargetMeasure);
			e.addContent(child);
		}
		
		child = new Element("ClassMeasure",ns);
		child.setText(ClassMeasure);
		e.addContent(child);
		
		child = new Element("DropExistingTable",ns);
		child.setText(String.valueOf(DropExistingTable));
		e.addContent(child);
		
		child = new Element("OutputOnlyClass",ns);
		child.setText(String.valueOf(OutputOnlyClass));
		e.addContent(child);
		
		child = new Element("BiasToUniformClass",ns);
		child.setText(String.valueOf(BiasToUniformClass));
		e.addContent(child);
		
		child = new Element("UseMinEstimatedValue",ns);
		child.setText(String.valueOf(UseMinEstimatedValue));
		e.addContent(child);
		
		child = new Element("MinEstimatedValue",ns);
		child.setText(Util.getDecimalFormatForRepository().format(MinEstimatedValue));
		e.addContent(child);
		
		child = new Element("ExcludeIfBelowMinValue",ns);
		child.setText(String.valueOf(ExcludeIfBelowMinValue));
		e.addContent(child);
		
		child = new Element("RelativeMinValue",ns);
		child.setText(String.valueOf(RelativeMinValue));
		e.addContent(child);
		
		child = new Element("MinRelativeToMeasure",ns);
		child.setText(String.valueOf(MinRelativeToMeasure));
		e.addContent(child);
		
		XmlUtils.addContent(e, "MinMeasure", MinMeasure,ns);
				
		child = new Element("UseMaxEstimatedValue",ns);
		child.setText(String.valueOf(UseMaxEstimatedValue));
		e.addContent(child);
		
		child = new Element("MaxEstimatedValue",ns);
		child.setText(Util.getDecimalFormatForRepository().format(MaxEstimatedValue));
		e.addContent(child);
		
		child = new Element("ExcludeIfAboveMaxValue",ns);
		child.setText(String.valueOf(ExcludeIfAboveMaxValue));
		e.addContent(child);
		
		child = new Element("RelativeMaxValue",ns);
		child.setText(String.valueOf(RelativeMaxValue));
		e.addContent(child);
		
		child = new Element("MaxRelativeToMeasure",ns);
		child.setText(String.valueOf(MaxRelativeToMeasure));
		e.addContent(child);
		
		child = new Element("MaxMeasure",ns);
		child.setText(MaxMeasure);
		e.addContent(child);
		
		child = new Element("OutputBuckets",ns);
		child.setText(String.valueOf(OutputBuckets));
		e.addContent(child);
		
		child = new Element("OutputBucketType",ns);
		child.setText(String.valueOf(OutputBucketType));
		e.addContent(child);
		
		if (Buckets!=null && Buckets.length>0)
		{
			child = new Element("Buckets",ns);
			for (MeasureBucket mb : Buckets)
			{
				child.addContent(mb.getMeasureBucketElement(ns));
			}
			e.addContent(child);
		}
		
		child = new Element("RankFactors",ns);
		child.setText(String.valueOf(RankFactors));
		e.addContent(child);
		
		child = new Element("NumFactorsToRank",ns);
		child.setText(String.valueOf(NumFactorsToRank));
		e.addContent(child);
		
		if (Factors!=null && Factors.length>0)
		{
			child = new Element("Factors",ns);
			for (OutcomeFactor of : Factors)
			{
				child.addContent(of.getOutcomeFactorElement(ns));
			}
			e.addContent(child);
		}
		else
		{
			child = new Element("Factors",ns);
			e.addContent(child);
		}
		
		if (Ruleset!=null && !Ruleset.getRules().isEmpty())
		{
			child = new Element("RuleSet",ns);
			for (ClassificationRule  rule : Ruleset.getRules())
			{
				child.addContent(rule.getClassificationRuleElement(ns));
			}
			e.addContent(child);
		}
		else
		{
			child = new Element("RuleSet",ns);
			e.addContent(child);
		}
		
		if (ExclusionRuleset!=null && !ExclusionRuleset.getRules().isEmpty())
		{
			child = new Element("ExclusionRuleSet",ns);
			for (ClassificationRule  rule : ExclusionRuleset.getRules())
			{
				child.addContent(rule.getClassificationRuleElement(ns));
			}
			e.addContent(child);
		}
		else
		{
			child = new Element("ExclusionRuleSet",ns);
			e.addContent(child);
		}
		
		if (Classifiers!=null)
		{
			e.addContent(Classifiers.getClassifierSettingsElement(ns));
		}
		
		if (trainingFilterName!=null)
		{
		  XmlUtils.addContent(e, "TrainingFilter", trainingFilterName,ns);
		}
		
		if (scoringFilterName!=null)
		{
			child = new Element("ScoringFilter",ns);
			child.setText(scoringFilterName);
			e.addContent(child);
		}
		
		if (NameFilter!=null)
		{
			child = new Element("NameFilter",ns);
			child.setText(NameFilter);
			e.addContent(child);
		}
		
		return e;
	}

	/**
	 * Return an array of indices into a dataset that correspond to factors
	 * 
	 * @param dataSet
	 * @return
	 */
	public static int[] getFactorIndices(Instances dataSet, OutcomeFactor[] Factors)
	{
		int[] factorIndices = new int[Factors.length];
		for (int i = 0; i < Factors.length; i++)
		{
			int result = -1;
			for (int j = 0; j < dataSet.numAttributes(); j++)
			{
				if (dataSet.attribute(j).name().equals(Factors[i].toString()))
				{
					result = j;
					break;
				}
			}
			factorIndices[i] = result;
		}
		return (factorIndices);
	}
	
	/**
	 * return the classification ruleset
	 * @return	classification ruleset
	 */
	public ClassificationRuleSet getRuleset()
	{
		return Ruleset;
	}
	
	/**
	 * return the negative classification ruleset - find instances that should not be members of the class
	 * @return	negative classification ruleset
	 */
	public ClassificationRuleSet getExclusionRuleset()
	{
		return ExclusionRuleset;
	}
	
	/**
	 * get the training filter
	 */
	public DisplayFilter getTrainingFilter()
	{
		return TrainingFilter;
	}
}
