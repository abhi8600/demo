/**
 * $Id: SegmentDefinition.java,v 1.4 2007-08-30 16:26:47 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

/**
 * Class to capture the definition of segment search criteria. Drives the automatic generation of segments.
 * 
 * @author brad
 * 
 */
public class SegmentDefinition
{
	public String SuccessModel;
	public String TargetMeasure;
	public String PhysicalTableName;
	public String SegmentColumn;
	public String TargetKeyColumn;
	public String IterationKeyColumn;
	public boolean UpdateTable;

	public SegmentDefinition(String SuccessModelName, String TargetMeasure)
	{
		this.SuccessModel = SuccessModelName;
		this.TargetMeasure = TargetMeasure;
	}
}
