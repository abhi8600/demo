/**
 * $Id: MakeFeasible.java,v 1.9 2008-03-27 19:23:29 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.util.ArrayList;
import java.util.List;
import java.util.BitSet;
import weka.classifiers.Classifier;
import weka.core.Instance;

/**
 * Class to handle the feasibility of solutions (ensuring that only valid combinations of indexes are explored)
 * 
 * @author bpeters
 * 
 */
public class MakeFeasible implements FeasibleSolution
{
    double[] maximums;
    double[] minimums;
    private ObjectiveFunction of;
    BitSet[] blockGroups;
    BitSet[] coverGroups;
    BitSet badColumns;
    int[] numInCoverGroups;
    int numModeledColumns;
    Performance perf;
    private static double MIN_ERROR = 1E-5;

    public MakeFeasible(Instance inst, Instance uinst, ObjectiveFunction of, Classifier c, Performance perf)
            throws Exception
    {
        this.of = of;
        this.numModeledColumns = inst.numAttributes();
        maximums = new double[numModeledColumns];
        minimums = new double[numModeledColumns];
        this.perf = perf;
        // Create a bit vector for columns not allowed (i.e. where min and max are the same and there's a min improvement
        badColumns = new BitSet(numModeledColumns);
        for (int j = 1; j<numModeledColumns; j++)
        {
            double v = inst.value(j);
            if (Double.isNaN(v))
                v = 0;
            double max = perf.getMax(j, inst, uinst);
            double min = perf.getMin(j, inst, uinst);
            double maxImp = perf.getMaxImp(j, inst, uinst);
            double minImp = perf.getMinImp(j, inst, uinst);
            double valid = perf.getValid(j, inst, uinst);
            if (valid==0)
            {
                // If it is not valid, then restrict max and min to current value
                max = v;
                min = v;
            }
            double newmin = Math.max(Math.min(max, minImp+v), min);
            double newmax = Math.min(max, Math.max(min, v+maxImp));
            min = newmin;
            max = newmax;
            /*
             * Now, test directionality of model. If it matches the improve direction, then allow change. Otherwise, the optimizer
             * can make a measure worse
             */
            double curpred = c.classifyInstance(inst);
            double var = inst.dataset().variance(j);
            Instance newInst = (Instance) inst.copy();
            // Make a small change relative to the variance of the value
            newInst.setValue(j, v+var/10);
            double newpred = c.classifyInstance(newInst);
            int dir = (newpred>=curpred) ? 1 : -1;
            // If the directions are different, restrict max and min. If there's a required min improvement, mark column as bad
            if (perf.improveList[perf.getRepositoryIndex(j)]*dir<0)
            {
                max = v;
                min = v;
            }
            int improve = perf.improveList[perf.getRepositoryIndex(j)];
            if (improve>0)
            {
                minimums[j] = Math.max(min, v);
                maximums[j] = Math.max(max, v);
            } else if (improve<0)
            {
                maximums[j] = Math.min(v, max);
                minimums[j] = Math.min(v, min);
            } else
            {
                maximums[j] = Math.max(max, v);
                minimums[j] = Math.min(v, min);
            }
            /*
             * If the column can't change, mark it as bad
             */
            if ((maximums[j]==v)&&(minimums[j]==v))
            {
                badColumns.set(j);
            }
        }
        // Get list of blocking groups
        List<String> bgList = new ArrayList<String>();
        for (int i = 0; i< perf.performance.Measures.length; i++)
        {
            if ((perf.performance.Measures[i].BlockGroup!=null)
                    &&(perf.performance.Measures[i].BlockGroup.length()>0))
                if (!bgList.contains(perf.performance.Measures[i].BlockGroup))
                    bgList.add(perf.performance.Measures[i].BlockGroup);
        }
        // Create a bit vector for each group
        blockGroups = new BitSet[bgList.size()];
        for (int i = 0; i<bgList.size(); i++)
        {
            BitSet bs = new BitSet(numModeledColumns);
            blockGroups[i] = bs;
            for (int j = 0; j < perf.performance.Measures.length; j++)
            {
                if (perf.performance.Measures[j].BlockGroup.equals(bgList.get(i)))
                {
                    bs.set(perf.getDatasetIndex(j));
                }
            }
        }
        // Get list of cover groups
        List<String> cList = new ArrayList<String>();
        List<Integer> cnList = new ArrayList<Integer>();
        for (int i = 0; i < perf.performance.Measures.length; i++)
        {
            if ((perf.performance.Measures[i].CoverGroup!=null)
                    &&(perf.performance.Measures[i].CoverGroup.length()>0))
                if (!cList.contains(perf.performance.Measures[i].CoverGroup))
                {
                    cList.add(perf.performance.Measures[i].CoverGroup);
                    cnList.add(Integer.valueOf(perf.performance.Measures[i].NumInCoverGroup));
                }
        }
        // Create a bit vector for each group
        coverGroups = new BitSet[cList.size()];
        numInCoverGroups = new int[cList.size()];
        for (int i = 0; i<cList.size(); i++)
        {
            BitSet bs = new BitSet(numModeledColumns);
            coverGroups[i] = bs;
            numInCoverGroups[i] = ((Integer) cnList.get(i)).intValue();
            for (int j = 0; j<perf.performance.Measures.length; j++)
            {
                if (perf.performance.Measures[j].CoverGroup.equals(cList.get(i)))
                {
                    bs.set(perf.getDatasetIndex(j));
                }
            }
            // If all columns in a cover group are bad, remove the bad flags (need to pick one)
            BitSet bsc = (BitSet) bs.clone();
            bsc.and(badColumns);
            if (bsc.cardinality()==bs.cardinality())
            {
                badColumns.xor(bs);
            }
        }
    }

    public boolean isBadColumn(int i)
    {
        return (badColumns.get(i));
    }

    public boolean isFeasible(int[] indices) throws Exception
    {
        // Build vector
        BitSet bs = new BitSet(numModeledColumns);
        for (int i : indices)
        	bs.set(i);
        // See if any bad columns
        BitSet bsc = (BitSet) bs.clone();
        bsc.and(badColumns);
        if (bsc.cardinality() > 0)
            return (false);
        // See if satisfies blocking groups
        for (BitSet blockGroup : blockGroups)
        {
            bsc = (BitSet) bs.clone();
            bsc.and(blockGroup);
            // If more than one match, then not feasible
            if (bsc.cardinality() > 1)
                return (false);
        }
        // Now see if satisfies cardinality constraints
        for (int i = 0; i<coverGroups.length; i++)
        {
            bsc = (BitSet) bs.clone();
            bsc.and(coverGroups[i]);
            // If more than one match, then not feasible
            int gap = numInCoverGroups[i] - bsc.cardinality();
            if (gap != 0)
            {
                return (false);
            }
        }
        return true;
    }

    public double makeValidInstance(Instance inst, int[] indices) throws Exception
    {
        HillClimber hc = new HillClimber(inst, indices, maximums, minimums, perf.means, of, MIN_ERROR, false);
        double val = 0;
        if (perf.r.getOptimizerParameters().Method==OptimizerParameters.SIMPLEX_METHOD)
        {
            val = hc.findSolutionSimplex();
        } else if (perf.r.getOptimizerParameters().Method==OptimizerParameters.QUASINEWTON_METHOD)
        {
            val = hc.findSolutionBFGS();
        }
        return (val);
    }
}
