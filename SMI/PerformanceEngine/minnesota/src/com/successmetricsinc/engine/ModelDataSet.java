/**
 * $Id: ModelDataSet.java,v 1.68 2011-09-24 01:03:33 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.log4j.Logger;
import com.successmetricsinc.engine.Header.HeaderType;
import com.successmetricsinc.util.Util;

/**
 * Class to hold raw data for a success model instance. Class has little overhead and holds a large array of row values.
 * 
 * @author bpeters
 * 
 */
public class ModelDataSet
{
	private static Logger logger = Logger.getLogger(ModelDataSet.class);
	private ModelDatasetMetadata metaData;
	private List<Partition> partitions;
	private Map<Integer, Partition> partitionMap;
	private Partition currentPartition = null;
	private ByteBuffer buf = null;

	/**
	 * Save the model dataset to a file
	 * 
	 * @param fileName
	 * @throws IOException
	 */
	public void save(String fileName) throws IOException
	{
		File file = new File(fileName);
		File directory = file.getParentFile();
		File tempFile = File.createTempFile("modelDataFile", null, directory);
		logger.info("Writing model to temporary file " + tempFile.getName());
		FileOutputStream fo = new FileOutputStream(tempFile);
		ObjectOutputStream oo = new ObjectOutputStream(fo);
		oo.writeObject(metaData);
		oo.close();
		fo.close();
		logger.info("Renaming temporary file to " + fileName);
		File f2 = new File(fileName);
		f2.delete();
		if (tempFile.renameTo(f2))
		{
			tempFile.delete();
		}
		else
		{
			logger.error("Rename of temporary file failed, keeping temporary file around");
		}
		for (Partition p : partitions)
			p.saveMetadata();
	}

	/**
	 * Instantiate a model dataset from a saved file
	 * 
	 * @param fileName
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public ModelDataSet(String fileName) throws IOException, ClassNotFoundException
	{
		FileInputStream in = new FileInputStream(fileName);
		ObjectInputStream s = new ObjectInputStream(in);
		metaData = (ModelDatasetMetadata) s.readObject();
		in.close();
		partitions = new ArrayList<Partition>();
		partitionMap = new HashMap<Integer, Partition>();
		for (Object key : metaData.partitionKeys)
		{
			Partition p = new Partition(key, fileName, false);
			for (int i = p.getStartIndex(); i < p.getNumRows() + p.getStartIndex(); i++)
				partitionMap.put(i, p);
			partitions.add(p);
		}
	}

	/**
	 * Instantiate a brand new model dataset
	 */
	public ModelDataSet()
	{
		metaData = new ModelDatasetMetadata();
		// Setup key map
		metaData.keyMap = new HashMap<String, Integer>();
		partitionMap = new HashMap<Integer, Partition>();
		metaData.numRows = 0;
		partitions = new ArrayList<Partition>();
	}

	/**
	 * Add a partition to a model data set.
	 * 
	 * @param fileName
	 *            Modeldataset filename
	 * @param partitionKey
	 *            Key to new partition
	 * @param targetKeys
	 *            List of target keys
	 * @param iterateKeys
	 *            List if iteration keys
	 * @param segmentKeys
	 *            List of segment keys
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public void addPartition(String fileName, Object partitionKey, List<Object> targetKeys, List<Object> iterateKeys, List<Object> segmentKeys, int startIndex)
			throws IOException, ClassNotFoundException
	{
		Partition p = new Partition(partitionKey, fileName, true);
		
		partitions.add(p);
		if (metaData.partitionKeys == null)
			metaData.partitionKeys = new ArrayList<Object>();
		metaData.partitionKeys.add(partitionKey);
		p.targetKeys = new Object[targetKeys.size()];
		targetKeys.toArray(p.targetKeys);
		p.iteratorKeys = new Object[iterateKeys.size()];
		iterateKeys.toArray(p.iteratorKeys);
		p.segmentKeys = new Object[segmentKeys.size()];
		segmentKeys.toArray(p.segmentKeys);
		p.setStartIndex(startIndex);
		// Add to maps
		int sz = targetKeys.size();
		for (int i = 0; i < sz; i++)
		{
			int num = i + metaData.numRows;
			metaData.keyMap.put(getRowKey(targetKeys.get(i), iterateKeys.get(i)), num);
			partitionMap.put(num, p);
		}
		// Initialize blank partition
		metaData.numRows += targetKeys.size();
		if (numHeaders() > p.getNumColumns())
			p.allocateNewBlock(numHeaders() - p.getNumColumns());
		p.saveMetadata();
	}

	public static String getRowKey(Object targetKey, Object iterateKey)
	{
		return (targetKey.toString() + "_" + iterateKey.toString());
	}

	private void generateKeyMap()
	{
		int numRows = numRows();
		metaData.keyMap = new HashMap<String, Integer>(numRows);
		for (int i = 0; i < numRows; i++)
		{
			String key = getRowKey(getTargetKey(i), getIteratorKey(i));
			metaData.keyMap.put(key, i);
		}
	}

	public int getRowIndex(Object targetKey, Object iterateKey)
	{
		if (metaData.keyMap == null)
			generateKeyMap();
		Integer result = metaData.keyMap.get(getRowKey(targetKey, iterateKey));
		return (result == null ? -1 : result);
	}

	public List<Object> getPartitionKeys()
	{
		return (metaData.partitionKeys);
	}

	/**
	 * Add a new set of headers to the model data set. Headers are appended to the end of the current list of headers.
	 * If a Success Model is provided, set it to point to where each pattern starts in the list of headers
	 * 
	 * @param sm
	 * @param newHeaders
	 * @return Starting column number for headers
	 * @throws IOException
	 */
	public synchronized int addHeaders(SuccessModel sm, List<Header> newHeaders) throws IOException
	{
		int numColumns = this.numHeaders();
		if (newHeaders == null || newHeaders.isEmpty())
		{
			logger.error("Trying to add 0 new headers");
			return -1;
		}

		if (metaData.headers == null)
			metaData.headers = new ArrayList<Header>();
		/*
		 * Add all the headers if they are provided - if not, assume headers already exist
		 */
		int lastPI = -1;
		for (Header h : newHeaders)
		{
			if (sm != null && h.Type == HeaderType.Pattern && lastPI != h.PatternIndex)
				sm.Patterns[h.PatternIndex].Startindex = metaData.headers.size();
			lastPI = h.PatternIndex;
			metaData.headers.add(h);
		}
		int size = newHeaders.size();
		for (Partition p : partitions)
			p.allocateNewBlock(size);
		return (numColumns);
	}

	/**
	 * Add new data to a partition
	 * 
	 * @param partitionKey
	 * @param newRows
	 * @param newTargetKeys
	 * @param newIterateKeys
	 * @throws ClassNotFoundException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public synchronized int addData(Object partitionKey, int startCol, double[][] newRows, int newCols, List<Object> newTargetKeys,
			List<Object> newIterateKeys) throws IOException, ClassNotFoundException, InterruptedException
	{
		/*
		 * Resize rows to accomodate new data
		 */
		Set<Integer> used = new HashSet<Integer>(newRows.length);
		Partition p = partitions.get(metaData.partitionKeys.indexOf(partitionKey));
		p.openFileWrite(startCol, newCols);
		int unKnownKeys = 0;
		int startIndex = p.getStartIndex();
		for (int i = 0; i < newRows.length; i++)
		{
			Integer indexObject = metaData.keyMap.get(getRowKey(newTargetKeys.get(i), newIterateKeys.get(i)));
			// Only add row if it is in the target dimension set
			if (indexObject != null)
			{
				used.add(indexObject - startIndex);
				p.setRow(indexObject - startIndex, startCol, newRows[i]);
			} else
				unKnownKeys++;
		}
		// Add empties for rows that didn't have values
		int sz = p.targetKeys.length;
		for (int i = 0; i < sz; i++)
		{
			if (!used.contains(i))
			{
				for (int j = 0; j < newCols; j++)
					p.setCell(i, startCol + j, Double.NaN);
			}
		}
		used.clear();
		p.closeDataFile();
		return (unKnownKeys);
	}
	AtomicInteger curRow;

	/**
	 * Commit current values. Once committed, cannot add new data. Data is transormed from intermediate formats useful
	 * for adding, to more raw/efficient formats for fast storage and retrieval.
	 */
	public synchronized void commit()
	{
		logger.info("Committing Model Dataset");
		System.gc();
		logger.info("Committed Model Data Set");
	}

	/**
	 * Find the appropriate breakout attribute weights based on the supplied breakout
	 * 
	 * @param measure
	 * @param dimension1
	 * @param attribute1
	 * @param dimension2
	 * @param attribute2
	 * @return
	 */
	public BreakoutAttribute findBreakoutAttributeWeights(String measure, String dimension1, String attribute1, String dimension2, String attribute2)
	{
		if (metaData.breakoutAttributes != null)
		{
			for (int i = 0; i < metaData.breakoutAttributes.length; i++)
			{
				BreakoutAttribute ba = metaData.breakoutAttributes[i];
				if (ba.weightMeasure.equals(measure) && ba.dimension1.equals(dimension1) && ba.attribute1.equals(attribute1)
						&& ((attribute2 == null) || (ba.dimension2.equals(dimension2) && ba.attribute2.equals(attribute2))))
					return (ba);
			}
		}
		return (null);
	}

	public String toString(int start, int stop)
	{
		StringBuilder sb = new StringBuilder();
		sb.append("Target\tIteration\tSegement\t");
		for (Header h : metaData.headers)
			sb.append(h.Name + "\t");
		sb.append("\n");
		try
		{
			if (start == 0 && stop == 0)
				stop = metaData.numRows;
			for (int i = start; i < stop; i++)
			{
				Partition p = setPartition(i);
				sb.append(getTargetKey(i) + "\t" + getIteratorKey(i) + "\t" + getSegmentKey(i) + "\t");
				for (int j = 0; j < metaData.headers.size(); j++)
					sb.append(p.getCell(i - p.getStartIndex(), j) + "\t");
				sb.append("\n");
			}
			if (currentPartition != null)
			{
				currentPartition.closeDataFile();
				currentPartition = null;
			}
		} catch (Exception ex)
		{
			sb.append("Unable to load partition: " + ex.toString());
		}
		return (sb.toString());
	}

	public String toString()
	{
		return (toString(0, 0));
	}

	private static class SummaryStruct
	{
		double min;
		double max;
		double sum;
		double sumsq;
		int nullCount;
		Set<Double> uniqueSet;
		boolean hasNumericSummary;
	}

	private SummaryStruct[] getSummaryStruct()
	{
		SummaryStruct[] stats = new SummaryStruct[this.numHeaders()];
		for (int i = 0; i < this.numHeaders(); i++)
		{
			stats[i] = new SummaryStruct();
			stats[i].min = Double.NEGATIVE_INFINITY;
			stats[i].max = Double.NEGATIVE_INFINITY;
			stats[i].sum = Double.NEGATIVE_INFINITY;
			stats[i].sumsq = Double.NEGATIVE_INFINITY;
			stats[i].uniqueSet = Collections.synchronizedSet(new HashSet<Double>());
			Header h = metaData.headers.get(i);
			stats[i].hasNumericSummary = h.Type == HeaderType.Target || h.Type == HeaderType.Pattern || (h.Type == HeaderType.Attribute && h.Number);
		}
		return (stats);
	}

	public void summarizeDataset(PrintStream stream, int numThreads) throws InterruptedException, IOException, ClassNotFoundException
	{
		PrintWriter writer = new PrintWriter(stream);
		writer.println("Model Data Set Summary for " + Util.getBuildString());
		writer.format("%,d Instances, %,d Columns\n\n", this.numRows(), numHeaders());
		writer.println("Name\tType\tUnique\t% Null\tMin\tAvg\tMax\tSD");
		SummarizeThread[] statThreads = new SummarizeThread[numThreads];
		SummaryStruct[][] structs = new SummaryStruct[numThreads][];
		for (int i = 0; i < statThreads.length; i++)
		{
			structs[i] = getSummaryStruct();
		}
		SummaryStruct[] stats = getSummaryStruct();
		for (Partition p : partitions)
		{
			p.openDataFile();
			p.loadMemory();
			curRow = new AtomicInteger(p.getNumRows());
			for (int i = 0; i < statThreads.length; i++)
			{
				statThreads[i] = new SummarizeThread(p, structs[i], stats);
				statThreads[i].start();
			}
			for (int i = 0; i < statThreads.length; i++)
			{
				statThreads[i].join();
			}
			p.closeDataFile();
		}
		for (int j = 0; j < statThreads.length; j++)
		{
			for (int i = 0; i < stats.length; i++)
			{
				stats[i].nullCount += structs[j][i].nullCount;
				if (stats[i].hasNumericSummary)
				{
					if (stats[i].sum == Double.NEGATIVE_INFINITY)
					{
						stats[i].sum = structs[j][i].sum;
						stats[i].sumsq = structs[j][i].sumsq;
					} else
					{
						stats[i].sum += structs[j][i].sum == Double.NEGATIVE_INFINITY ? 0 : structs[j][i].sum;
						stats[i].sumsq += structs[j][i].sumsq == Double.NEGATIVE_INFINITY ? 0 : structs[j][i].sumsq;
					}
					if (stats[i].min == Double.NEGATIVE_INFINITY)
						stats[i].min = structs[j][i].min;
					else if (structs[j][i].min != Double.NEGATIVE_INFINITY && structs[j][i].min < stats[i].min)
						stats[i].min = structs[j][i].min;
					if (stats[i].max == Double.NEGATIVE_INFINITY)
						stats[i].max = structs[j][i].max;
					else if (structs[j][i].min != Double.NEGATIVE_INFINITY && structs[j][i].max > stats[i].max)
						stats[i].max = structs[j][i].max;
				}
			}
			structs[j] = null;
		}
		structs = null;
		for (int i = 0; i < this.numHeaders(); i++)
		{
			Header h = metaData.headers.get(i);
			writer.print(i + ": " + h.Name + "\t" + h.Type.toString());
			writer.format("\t%d\t%5.1f", stats[i].uniqueSet.size(), (100.0 * stats[i].nullCount) / numRows());
			if (stats[i].hasNumericSummary)
			{
				if (stats[i].sum != Double.NEGATIVE_INFINITY)
				{
					double avg = stats[i].sum / (numRows() - stats[i].nullCount);
					double var = stats[i].sumsq / (numRows() - stats[i].nullCount) - (avg * avg);
					double sd = Math.sqrt(var);
					writer.format("\t%.4f\t%.4f\t%.4f\t%.4f", stats[i].min, avg, stats[i].max, sd);
				}
			}
			writer.println();
			stats[i].uniqueSet.clear();
			stats[i].uniqueSet = null;
			stats[i] = null;
		}
		stats = null;
		writer.flush();
		memStats("summarize model data set");
	}

	/**
	 * GC and output debugging information about the memory
	 * @param label	prepended to the memory information in the log
	 */
	private void memStats(String label)
	{
		System.gc();
		System.gc();
		if (logger.isDebugEnabled())
		{
			long free = Runtime.getRuntime().freeMemory();
			long total = Runtime.getRuntime().totalMemory();
			logger.debug(label + ": used=" + (total - free) + ", total=" + total + ", free=" + free); 
		}
	}
	
	private class SummarizeThread extends Thread
	{
		SummaryStruct[] stats;
		SummaryStruct[] global;
		Partition p;

		public SummarizeThread(Partition p, SummaryStruct[] stats, SummaryStruct[] global)
		{
			this.setName("Summarize - " + this.getId());
			this.stats = stats;
			this.global = global;
			this.p = p;
		}

		public void run()
		{
			do
			{
				int j = curRow.decrementAndGet();
				if (j < 0)
					return;
				if (j > 0 && j % 10000 == 0)
					logger.info("Summarizing row: " + j);
				for (int i = 0; i < stats.length; i++)
				{
					double d = p.getCell(j, i);
					if (Double.isNaN(d))
						stats[i].nullCount++;
					else
					{
						global[i].uniqueSet.add(d);
						if (stats[i].hasNumericSummary)
						{
							if (stats[i].sum == Double.NEGATIVE_INFINITY)
							{
								stats[i].sum = d;
								stats[i].sumsq = d * d;
							} else
							{
								stats[i].sum += d;
								stats[i].sumsq += d * d;
							}
							if (stats[i].min == Double.NEGATIVE_INFINITY)
								stats[i].min = d;
							else if (d < stats[i].min)
								stats[i].min = d;
							if (stats[i].max == Double.NEGATIVE_INFINITY)
								stats[i].max = d;
							else if (d > stats[i].max)
								stats[i].max = d;
						}
					}
				}
			} while (true);
		}
	}

	public int numHeaders()
	{
		if (metaData.headers == null)
			return (0);
		return (metaData.headers.size());
	}

	public int numRows()
	{
		return (metaData.numRows);
	}

	/**
	 * Get data for a given cell. (Optimized for sequential access across partitions - preloads each partition such that
	 * intra partition operations are as fast as possible)
	 * 
	 * @param row
	 * @param column
	 * @return
	 */
	public synchronized double getCell(int row, int column)
	{
		try
		{
			return (currentPartition.getGlobalCell(row, column));
		} catch (Exception ex)
		{
			try
			{
				/*
				 * If a previous partition had been accessed, close it
				 */
				if (currentPartition != null)
				{
					currentPartition.closeDataFile();
					currentPartition = null;
				}
				if (buf == null)
				{
					/*
					 * If no buffer has been allocated, find the maximum sized buffer that is needed and allocate. Using
					 * one buffer across all partitions eliminates expensive garbage collection required to reallocate
					 * memory.
					 */
					int maxRows = 0;
					for (Partition p : partitions)
						if (p.getNumRows() > maxRows)
							maxRows = p.getNumRows();
					// try to keep this from overflowing the integer
					int bytes = Double.SIZE / Byte.SIZE;
					int size = maxRows * metaData.headers.size() * bytes;
					if (size <= 0)
					{
						logger.error("fatal error: ByteBuffer size has overflowed the integer range");
					}
					logger.debug("Allocating byte buffer of size " + size);
					buf = ByteBuffer.allocate(size);
				}
				currentPartition = partitionMap.get(row);
				if (currentPartition == null)
				{
					logger.error("No partition associated with row: " + row + ", max rows: " + this.metaData.numRows);
				}
				currentPartition.openDataFile(buf);
			} catch (IOException e)
			{
				return (Double.NaN);
			}
			try
			{
				return (currentPartition.getGlobalCell(row, column));
			} catch (Exception e)
			{
				logger.error(e);
				return (Double.NaN);
			}
		}
	}

	public synchronized void setCell(int row, int column, double d)
	{
		Partition p = partitionMap.get(row);
		p.setCell(row, column, d);
	}

	public Header getHeader(int column)
	{
		return (metaData.headers.get(column));
	}

	public List<Header> getHeaders()
	{
		return (metaData.headers);
	}

	public Object getTargetKey(int row)
	{
		Partition p = partitionMap.get(row);
		return (p.getTargetKey(row));
	}

	public Object getIteratorKey(int row)
	{
		Partition p = partitionMap.get(row);
		return (p.getIteratorKey(row));
	}

	public Object getSegmentKey(int row)
	{
		Partition p = partitionMap.get(row);
		return (p.getSegmentKey(row));
	}

	public void setBreakoutAttributes(List<BreakoutAttribute> baList)
	{
		if (baList == null)
			return;
		metaData.breakoutAttributes = new BreakoutAttribute[baList.size()];
		baList.toArray(metaData.breakoutAttributes);
	}

	public int getLastIndexOfPartition(int partition)
	{
		Partition p = partitions.get(partition);
		return (p.getStartIndex() + p.getNumRows() - 1);
	}

	public int getLastIndexOfCurrentPartition()
	{
		if (currentPartition == null)
			return 0;
		return (currentPartition.getStartIndex() + currentPartition.getNumRows() - 1);
	}

	/**
	 * Return the size of a partition
	 * 
	 * @param partitionNum
	 * @return
	 */
	public int getPartitionSize(int partitionNum)
	{
		return (partitions.get(partitionNum).getNumRows());
	}
	
	/**
	 * close down the current partition
	 */
	public void closeCurrentPartition() throws IOException
	{
		if (currentPartition != null)
		{
			currentPartition.closeDataFile();
			currentPartition = null;
		}
	}

	/**
	 * Ensure that the current partition is set for a given row. Also load memory with partition (assuming sequential
	 * access)
	 * 
	 * @param row
	 * @throws IOException
	 */
	public Partition setPartition(int row) throws IOException
	{
		Partition p = partitionMap.get(row);
		if (p != currentPartition)
		{
			if (currentPartition != null)
			{
				currentPartition.closeDataFile();
				currentPartition = null;
			}
			p.openDataFile();
			p.loadMemory();
			currentPartition = p;
		}
		return (p);
	}
	
	/**
	 * return the list of partitions
	 */
	public List<Partition> getPartitions()
	{
		return partitions;
	}
}
