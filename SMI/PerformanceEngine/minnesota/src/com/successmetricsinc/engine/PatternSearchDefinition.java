/**
 * $Id: PatternSearchDefinition.java,v 1.8 2010-12-20 12:48:08 mpandit Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.io.Serializable;

import org.jdom.Element;
import org.jdom.Namespace;
import com.successmetricsinc.Repository;
import com.successmetricsinc.util.XmlUtils;

public class PatternSearchDefinition implements Serializable
{
	private static final long serialVersionUID = 1L;
	public static final int TYPE_PEER_COMPARISON = 0;
	public static final int TYPE_POPULATION_COMPARISON = 1;
	public static final int TYPE_POPULATION_SPLIT = 2;
	public String Name;
	public String SuccessModelName;
	public String OutputTableName;
	public String IterationAnalysisValue;
	public int PatternSearchType;
	public int NumPatternsToSave;
	public int NumPatternsPerMeasure;
	public int NumPeersToCompare;
	public boolean DropExistingTable;
	public boolean IgnoreZeroWeights;
	public String[] SegmentMeasures;
	public boolean GenerateSegments;
	public String TargetDimension;
	public String TargetLevel;
	public String JoinMeasure;
	public int NumTargetSegmentsToSave;
	public boolean ScreenDuplicateMeasures;
	public boolean ProcessTargetsSeparately;
	public String SegmentOutputTableName;
	public boolean DropExistingSegmentOutputTable;
	public String[] referenceFilters;
	public String[] comparisonFilters;
	public double SplitPercentage;
	public String ReferencePopulationName;
	public String ReferencePopulationOrig;
	public boolean IngoreZeroWeights;

	public PatternSearchDefinition(Element psde, Namespace ns)
	{
		Name = psde.getChildTextTrim("Name", ns);
		IterationAnalysisValue = psde.getChildTextTrim("IterationAnalysisValue", ns);
		OutputTableName = psde.getChildTextTrim("OutputTableName", ns);
		SuccessModelName = psde.getChildTextTrim("SuccessModelName", ns);
		String typestr = null;
		if ((typestr = psde.getChildTextTrim("PatternSearchType", ns)) != null)
			PatternSearchType = Integer.parseInt(typestr);
		NumPatternsToSave = Integer.parseInt(psde.getChildTextTrim("NumPatternsToSave", ns));
		if (psde.getChild("NumPatternsPerMeasure", ns) != null)
		{
			NumPatternsPerMeasure = Integer.parseInt(psde.getChildTextTrim("NumPatternsPerMeasure", ns));
		}
		else
		{
			NumPatternsPerMeasure = 3; // default value
		}
		NumPeersToCompare = Integer.parseInt(psde.getChildTextTrim("NumPeersToCompare", ns));
		DropExistingTable = Boolean.parseBoolean(psde.getChildTextTrim("DropExistingTable", ns));
		IgnoreZeroWeights = Boolean.parseBoolean(psde.getChildTextTrim("IgnoreZeroWeights", ns));
		SegmentMeasures = Repository.getStringArrayChildren(psde, "SegmentMeasures", ns);
		GenerateSegments = Boolean.parseBoolean(psde.getChildTextTrim("GenerateSegments", ns));
		TargetDimension = psde.getChildTextTrim("TargetDimension", ns);
		TargetLevel = psde.getChildTextTrim("TargetLevel", ns);
		JoinMeasure = psde.getChildTextTrim("JoinMeasure", ns);
		NumTargetSegmentsToSave = Integer.parseInt(psde.getChildTextTrim("NumTargetSegmentsToSave", ns));
		ScreenDuplicateMeasures = Boolean.parseBoolean(psde.getChildTextTrim("ScreenDuplicateMeasures", ns));
		ProcessTargetsSeparately = Boolean.parseBoolean(psde.getChildTextTrim("ProcessTargetsSeparately", ns));
		SegmentOutputTableName = psde.getChildTextTrim("SegmentOutputTableName", ns);
		DropExistingSegmentOutputTable = Boolean.parseBoolean(psde.getChildTextTrim("DropExistingSegmentOutputTable", ns));
		if (psde.getChild("ReferenceFilters", ns) != null)
			referenceFilters = Repository.getStringArrayChildren(psde, "ReferenceFilters", ns);
		if (psde.getChild("ComparisonFilters", ns) != null)
			comparisonFilters = Repository.getStringArrayChildren(psde, "ComparisonFilters", ns);
		if (psde.getChild("SplitPercentage", ns) != null)
			SplitPercentage = Double.parseDouble(psde.getChildTextTrim("SplitPercentage", ns));
		ReferencePopulationName = psde.getChildTextTrim("ReferencePopulation", ns);
		ReferencePopulationOrig = psde.getChildText("ReferencePopulation", ns);
		if (ReferencePopulationName == null || ReferencePopulationName.length() == 0)
		{
			ReferencePopulationName = ReferencePopulation.DEFAULT_NAME;
		}
		if (psde.getChild("IngoreZeroWeights",ns)!=null)
			IngoreZeroWeights = Boolean.parseBoolean(psde.getChildText("IngoreZeroWeights",ns));
	}
	
	public Element getPatternSearchDefinitionElement(Namespace ns)
	{
		Element e = new Element("PatternSearchDefinition",ns);
		
		Element child = new Element("Name",ns);
		child.setText(Name);
		e.addContent(child);
		
		XmlUtils.addContent(e, "SuccessModelName", SuccessModelName,ns);
		
		XmlUtils.addContent(e, "OutputTableName", OutputTableName,ns);
					
		XmlUtils.addContent(e, "ReferencePopulation", ReferencePopulationOrig,ns);
		
		XmlUtils.addContent(e, "IterationAnalysisValue", IterationAnalysisValue,ns);		
		
		child = new Element("PatternSearchType",ns);
		child.setText(String.valueOf(PatternSearchType));
		e.addContent(child);
		
		child = new Element("NumPatternsToSave",ns);
		child.setText(String.valueOf(NumPatternsToSave));
		e.addContent(child);
		
		child = new Element("NumPatternsPerMeasure",ns);
		child.setText(String.valueOf(NumPatternsPerMeasure));
		e.addContent(child);
		
		child = new Element("NumPeersToCompare",ns);
		child.setText(String.valueOf(NumPeersToCompare));
		e.addContent(child);
		
		child = new Element("DropExistingTable",ns);
		child.setText(String.valueOf(DropExistingTable));
		e.addContent(child);
		
		child = new Element("IngoreZeroWeights",ns);
		child.setText(String.valueOf(IngoreZeroWeights));
		e.addContent(child);
		
		child = new Element("GenerateSegments",ns);
		child.setText(String.valueOf(GenerateSegments));
		e.addContent(child);
		
		if (SegmentMeasures!=null && SegmentMeasures.length>0)
		{
			e.addContent(Repository.getStringArrayElement("SegmentMeasures", SegmentMeasures, ns));
		}
		
		child = new Element("TargetDimension",ns);
		child.setText(TargetDimension);
		e.addContent(child);
		
		child = new Element("TargetLevel",ns);
		child.setText(TargetLevel);
		e.addContent(child);
		
		child = new Element("JoinMeasure",ns);
		child.setText(JoinMeasure);
		e.addContent(child);
		
		child = new Element("NumTargetSegmentsToSave",ns);
		child.setText(String.valueOf(NumTargetSegmentsToSave));
		e.addContent(child);
		
		child = new Element("ScreenDuplicateMeasures",ns);
		child.setText(String.valueOf(ScreenDuplicateMeasures));
		e.addContent(child);
		
		child = new Element("ProcessTargetsSeparately",ns);
		child.setText(String.valueOf(ProcessTargetsSeparately));
		e.addContent(child);
		
		child = new Element("SegmentOutputTableName",ns);
		child.setText(SegmentOutputTableName);
		e.addContent(child);
		
		child = new Element("DropExistingSegmentOutputTable",ns);
		child.setText(String.valueOf(DropExistingSegmentOutputTable));
		e.addContent(child);
		
		if (referenceFilters!=null && referenceFilters.length>0)
		{
			e.addContent(Repository.getStringArrayElement("ReferenceFilters", referenceFilters, ns));
		}
		else
		{
			child = new Element("ReferenceFilters",ns);
			e.addContent(child);
		}
		
		if (comparisonFilters!=null && comparisonFilters.length>0)
		{
			e.addContent(Repository.getStringArrayElement("ComparisonFilters", comparisonFilters, ns));
		}
		else
		{
			child = new Element("ComparisonFilters",ns);
			e.addContent(child);
		}
		
		child = new Element("SplitPercentage",ns);
		child.setText(String.valueOf(SplitPercentage));
		e.addContent(child);
		
		return e;
	}

	public String toString()
	{
		return (this.Name);
	}
}