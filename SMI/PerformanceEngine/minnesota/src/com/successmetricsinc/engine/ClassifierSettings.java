/**
 * $Id: ClassifierSettings.java,v 1.2 2007-08-30 16:26:47 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.io.Serializable;

import org.jdom.Element;
import org.jdom.Namespace;

public class ClassifierSettings implements Serializable
{
	private static final long serialVersionUID = 1L;
	public int[] RegressionModels;
	public int[] ClassificationModels;
	public int MaxSampleInstanceValues;
	public int MaxInstanceValues;
	
	public Element getClassifierSettingsElement(Namespace ns)
	{
		Element e = new Element("Classifiers",ns);
		
		Element child = new Element("RegressionModels",ns);
		if (RegressionModels!=null && RegressionModels.length>0)
		{
			for (int regressionModel : RegressionModels)
			{
				Element intElement = new Element("int",ns); //TODO Is this int or integer
				intElement.setText(String.valueOf(regressionModel));
				child.addContent(intElement);
			}
		}	
		e.addContent(child);
		
		child = new Element("ClassificationModels",ns);
		if (ClassificationModels!=null && ClassificationModels.length>0)
		{
			for (int classificationModel : ClassificationModels)
			{
				Element intElement = new Element("int",ns); //TODO Is this int or integer
				intElement.setText(String.valueOf(classificationModel));
				child.addContent(intElement);
			}
		}	
		e.addContent(child);
		
		child = new Element("MaxSampleInstanceValues",ns);
		child.setText(String.valueOf(MaxSampleInstanceValues));
		e.addContent(child);
		
		child = new Element("MaxInstanceValues",ns);
		child.setText(String.valueOf(MaxInstanceValues));
		e.addContent(child);
		
		return e;
	}
}