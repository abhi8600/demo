/**
 * $Id: Header.java,v 1.12 2011-10-13 20:31:21 ricks Exp $
 *
 * Copyright (C) 2007-2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.apache.log4j.Logger;

import com.successmetricsinc.query.DimensionColumn;

/**
 * Class to hold header information for each column in a success model instance
 * 
 * @author bpeters
 * 
 */
public class Header implements Externalizable
{
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(Header.class);
	private static final long serialVersionUID = 1L;

	public enum HeaderType
	{
		Target, Attribute, Pattern, Cluster
	};
	public HeaderType Type;
	public boolean Number = false;
	public boolean NullCategory = false;
	public String Name;
	public String Attribute1Value;
	public String Attribute2Value;
	public int PatternIndex; // Relevant pattern index
	public boolean MapNullToZero = false;
	public DimensionColumn dc;
	
	public Header()
	{
	}

	public Header(HeaderType Type, String Name)
	{
		this.Type = Type;
		this.Name = Name;
	}

	public Header(HeaderType Type, String Name, int PatternIndex)
	{
		this.Type = Type;
		this.Name = Name;
		this.PatternIndex = PatternIndex;
	}

	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException
	{
		Type = (HeaderType) in.readObject();
		Number = (Boolean) in.readObject();
		NullCategory = (Boolean) in.readObject();
		Name = (String) in.readObject();
		Attribute1Value = (String) in.readObject();
		Attribute2Value = (String) in.readObject();
		PatternIndex = (Integer) in.readObject();
		MapNullToZero = (Boolean) in.readObject();
	}

	public void writeExternal(ObjectOutput out) throws IOException
	{
		out.writeObject(Type);
		out.writeObject(Number);
		out.writeObject(NullCategory);
		out.writeObject(Name);
		out.writeObject(Attribute1Value);
		out.writeObject(Attribute2Value);
		out.writeObject(PatternIndex);
		out.writeObject(MapNullToZero);
	}

	public String toString()
	{
		return (Name);
	}

	public void setType(int t)
	{
		switch (t)
		{
		case Types.BIGINT:
		case Types.BOOLEAN:
		case Types.DECIMAL:
		case Types.DOUBLE:
		case Types.FLOAT:
		case Types.INTEGER:
		case Types.NUMERIC:
		case Types.REAL:
		case Types.SMALLINT:
		case Types.TINYINT:
			Number = true;
			break;
		default:
			Number = false;
			break;
		}
	}

	/**
	 * routines for determing if a null-valued column should be mapped to zero
	 */
	public void setMapNullToZero(boolean flag)
	{
		MapNullToZero = flag;
	}

	public boolean getMapNullToZero()
	{
		return MapNullToZero;
	}

	public Object getResultObject(ResultSet rs, int index) throws SQLException
	{
		if (Number)
			return (new Double(rs.getDouble(index)));
		else
			return (rs.getString(index));
	}

	/**
	 * @return Returns the attribute1Value.
	 */
	public String getAttribute1Value()
	{
		return Attribute1Value;
	}

	/**
	 * @param attribute1Value
	 *            The attribute1Value to set.
	 */
	public void setAttribute1Value(String attribute1Value)
	{
		Attribute1Value = attribute1Value;
	}

	/**
	 * @return Returns the attribute2Value.
	 */
	public String getAttribute2Value()
	{
		return Attribute2Value;
	}

	/**
	 * @param attribute2Value
	 *            The attribute2Value to set.
	 */
	public void setAttribute2Value(String attribute2Value)
	{
		Attribute2Value = attribute2Value;
	}

	public final boolean equals(Object o)
	{
		if (this == o)
			return true;
		if (!(o instanceof Header))
			return false;
		Header h = (Header) o;
		return (h.Type == this.Type && h.NullCategory == this.NullCategory && h.Number == this.Number && h.PatternIndex == this.PatternIndex && h.Name
				.equals(this.Name));
	}
	
	public int hashCode()
	{
		  assert false : "hashCode not designed";
		  return 42; // any arbitrary constant will do 
	}
}
