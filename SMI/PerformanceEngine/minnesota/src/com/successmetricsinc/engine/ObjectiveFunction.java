/**
 * $Id: ObjectiveFunction.java,v 1.3 2007-08-30 16:26:48 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import weka.core.Instance;

/**
 * Defines an interface for a generic objective function to be optimized
 * 
 * @author bpeters
 * 
 */
public interface ObjectiveFunction
{
    /**
     * 
     * Return objective given an instance
     * 
     * @param inst
     *            Instance to evaluate
     * @return Value of objective function
     * @throws Exception
     */
    public double getObjective(Instance inst) throws Exception;
}
