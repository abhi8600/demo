/**
 * $Id: ClassificationRule.java,v 1.13 2012-02-13 11:51:27 mpandit Exp $
 *
 * Copyright (C) 2007-2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.Repository;
import com.successmetricsinc.query.DisplayFilter;
import com.successmetricsinc.query.QueryFilter;
import com.successmetricsinc.util.RepositoryException;

/**
 * class for classification rules
 * 
 * @author ricks
 *
 */
public class ClassificationRule implements Comparable<ClassificationRule>, Cloneable, Serializable
{
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(ClassificationRule.class);
	DisplayFilter filter;	// filter use for classification
	int priority;			// priority of the rule, lower numbers are higher priority and evaluated first
	String explanation;		// explanation to use if the rule matches the data
	DisplayFilter working;	// filter that can be rewritten, used in binding and evaluation
	boolean invalid = false;	// could not be bound to the data
	String filterName;
	String strExplanation;

	public ClassificationRule(int priority, DisplayFilter filter, String explanation)
	{
		this.priority = priority;
		this.filter = filter;
		this.explanation = explanation + " (" + this.filter.toString() + ")";
	}
	
	public ClassificationRule(Element e, Namespace ns, Repository r) throws RepositoryException
	{
		this.priority = Integer.parseInt(e.getChildTextTrim("Priority", ns));
		filterName = e.getChildTextTrim("Filter", ns);
		QueryFilter qf = r.findFilter(filterName);
		if (qf == null)
		{
			logger.error("Bad filter (" + filterName + ") specified for a classification rule");
			throw new RepositoryException("Bad filter (" + filterName + ") specified for a classification rule");
		}
		this.filter = qf.returnDisplayFilter(r, null);
		if (this.filter == null || !this.filter.isValid())
		{
			throw new RepositoryException("Bad filter (" + filterName + ") specified for a classification rule - could not be converted or is invalid");
		}
		this.explanation = e.getChildTextTrim("Explanation", ns);
		strExplanation = e.getChildText("Explanation", ns);
		this.explanation = this.explanation + " (" + this.filter.toString() + ")";
	}
	
	public Element getClassificationRuleElement(Namespace ns)
	{
		Element e = new Element("ClassificationRule",ns);
		
		Element child = new Element("Priority",ns);
		child.setText(String.valueOf(priority));
		e.addContent(child);
		
		child = new Element("Filter",ns);
		child.setText(filterName);
		e.addContent(child);
		
		child = new Element("Explanation",ns);
		child.setText(strExplanation);
		e.addContent(child);
		
		return e;
	}
	
	/**
	 * used for sorting the rules based upon priority
	 */
	public int compareTo(ClassificationRule cr)
	{
		return priority - cr.priority;
	}
	
	public String getExplanation()
	{
		return explanation;
	}
	
	public DisplayFilter getFilter()
	{
		return filter;
	}
	
	/**
	 * compare the rule against the data
	 * 
	 * @param row	instance converted to array of objects
	 * @return
	 */
	public boolean evaluate(Object[] row, Repository r)
	{
		return !invalid && working.satisfiesFilter(row, r);
	}
	
	/**
	 * bind the filter in the rule to the data, similar to bindToData in DisplayFilter
	 * 
	 * @param smi	Success Model Instance being used for the scoring
	 * @param colList	list of columns
	 * @return	true for success, false for failure
	 */
	public boolean bindToData(SuccessModelInstance smi, List<Integer> colList) throws CloneNotSupportedException
	{
		working = (DisplayFilter) filter.clone();
		
		if (!working.bindToData(smi, colList, 0))
		{
			logger.error("Can not bind the filter to the data: " + toString());
			invalid = true;
			return false;
		}
		invalid = false;
		return true;
	}
	
	/**
	 * need a clone method because these objects are used in multiple threads and there is no reason to make them thread safe
	 */
	public Object clone() throws CloneNotSupportedException
	{
		ClassificationRule cr = (ClassificationRule) super.clone();
		cr.priority = this.priority;
		cr.filter = (DisplayFilter) this.filter.clone();
		cr.explanation = this.explanation;
		return cr;
	}
	
	public String toString()
	{
		return filter.toString() + " (" + priority + "), " + explanation; 
	}
}