/**
 * $Id: FilterResults.java,v 1.8 2010-06-29 17:07:57 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.util.List;

public class FilterResults
{
	List<Integer> columnList;
	int nondistinctFiltered;
	int nullCategoriesFiltered;
	int correlatedColumnsFiltered;
	int crossCorrelatedColumnsFiltered;
	int modelFiltered;
	int removeUselessFiltered;
	int relatedColumnsFiltered;
}