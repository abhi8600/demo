/**
 * $Id: Performance.java,v 1.76 2012-05-21 13:22:23 mpandit Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.log4j.Logger;
import weka.classifiers.Classifier;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Discretize;
import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.doublealgo.Statistic;
import cern.colt.matrix.impl.DenseDoubleMatrix2D;
import cern.jet.stat.Probability;
import com.successmetricsinc.Repository;
import com.successmetricsinc.query.Database;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.GroupBy;
import com.successmetricsinc.query.Level;
import com.successmetricsinc.query.LevelKey;
import com.successmetricsinc.query.MeasureColumn;
import com.successmetricsinc.query.Query;
import com.successmetricsinc.util.RepositoryException;
import com.successmetricsinc.util.WekaUtil;

/**
 * Class to analyze performance of target entities and generate optimizations of performance attributes
 * 
 * @author Brad Peters
 */
public class Performance
{
	private static Logger logger = Logger.getLogger(Performance.class);
	private static boolean DEBUG = false;
	protected Repository r;
	protected DatabaseConnection dc;
	private Connection conn;
	private int numModeledColumns = 0;
	private int numUnmodeled = 0;
	// Key Lists
	private List<String> targetKeyList = new ArrayList<String>();
	private Map<String, List<Integer>> targetKeyMap = new HashMap<String, List<Integer>>();
	private List<String> iterateKeyList = new ArrayList<String>();
	private Map<String, Instance> uniqueKeyMap = new HashMap<String, Instance>();
	private Map<String, Instance> unmodeledUniqueKeyMap = new HashMap<String, Instance>();
	private String singleKey = null;
	// Data
	private Instances dataSet = null;
	private Instances unmodeledData = null;
	// Metadata
	private double impactMatrix[][] = null;
	private double avgImpacts[] = null;
	int improveList[] = null;
	double minList[] = null;
	double maxList[] = null;
	double minImpList[] = null;
	double maxImpList[] = null;
	// Measures for metadata
	String minMeasures[] = null;
	String maxMeasures[] = null;
	String minImpMeasures[] = null;
	String maxImpMeasures[] = null;
	String validMeasures[] = null;
	// Model
	BestFitResult bfr = null;
	// Other
	String path = null;
	String perfModelFile = null;
	Map<String, OptimizedResult> optimizedResults;
	// Means
	double[] means = null;
	// Peers
	Map<String, List<Peer>> peerMap;
	// List of ids to optimize
	private List<String> optimizeList;
	private boolean analyzeOnlyOptimized;
	public PerformanceModel performance;
	private String targetDimension;
	private String level;
	private String optimizationGoal;

	/**
	 * Class to analyze the performance of target entities
	 * 
	 * @param r
	 *            Reference to current repository
	 * @param conn
	 *            Connection to use to read/write to database
	 * @param path
	 *            Path to store model files and output
	 * @throws SQLException
	 */
	public Performance(String name, Repository r, String path) throws SQLException, RepositoryException
	{
		performance = r.findPerformanceModel(name);
		if (performance == null)
		{
			logger.error("Invalid performance model: " + name);
			throw new RepositoryException("Invalid performance model: " + name);
		}
		ReferencePopulation refPop = r.findReferencePopulation(performance.ReferencePopulationName);
		targetDimension = (refPop.TargetDimension != null) ? refPop.TargetDimension : r.getTargetDimension();
		level = refPop.Level;
		optimizationGoal = (performance.OptimizationGoal != null) ? performance.OptimizationGoal : r.getOptimizationGoal();
		this.r = r;
		this.dc = r.getDefaultConnection();
		this.conn = dc.ConnectionPool.getConnection();
		this.path = path;
		// create a model name
		this.perfModelFile = path + File.separator + performance.ModelFileName;
		improveList = new int[performance.Measures.length];
		minList = new double[performance.Measures.length];
		maxList = new double[performance.Measures.length];
		minImpList = new double[performance.Measures.length];
		maxImpList = new double[performance.Measures.length];
		minMeasures = new String[performance.Measures.length];
		maxMeasures = new String[performance.Measures.length];
		minImpMeasures = new String[performance.Measures.length];
		maxImpMeasures = new String[performance.Measures.length];
		validMeasures = new String[performance.Measures.length];
		optimizeList = null;
		numModeledColumns = 0;
		numUnmodeled = 0;
		for (int i = 0; i < performance.Measures.length; i++)
		{
			if (performance.Measures[i].ModelFlag)
			{
				numModeledColumns++;
			} else
			{
				numUnmodeled++;
			}
		}
		performance.PerformanceIterationModelValue = r.replaceVariables(null, performance.PerformanceIterationModelValue);
		performance.PerformanceIterationAnalysisValue = r.replaceVariables(null, performance.PerformanceIterationAnalysisValue);
		numModeledColumns++; // Include target measure
		// Collect the appropriate measure metadata
		for (int count = 0; count < performance.Measures.length; count++)
		{
			MeasureColumn mc = r.findMeasureColumn(performance.Measures[count].Name);
			improveList[count] = mc.Improve;
			try
			{
				minList[count] = Double.parseDouble(mc.MinTarget);
			} catch (Exception ex)
			{
				minMeasures[count] = mc.MinTarget;
			}
			try
			{
				maxList[count] = Double.parseDouble(mc.MaxTarget);
			} catch (Exception ex)
			{
				maxMeasures[count] = mc.MaxTarget;
			}
			try
			{
				minImpList[count] = Double.parseDouble(mc.MinImprovement);
			} catch (Exception ex)
			{
				minImpMeasures[count] = mc.MinImprovement;
			}
			try
			{
				maxImpList[count] = Double.parseDouble(mc.MaxImprovement);
			} catch (Exception ex)
			{
				maxImpMeasures[count] = mc.MaxImprovement;
			}
			validMeasures[count] = mc.ValidMeasure;
		}
	}

	/**
	 * Generate the performance model - generate performance query first
	 */
	public void generateBestFitResult()
	{
		try
		{
			long queryStart = System.currentTimeMillis();
			if (performance.PerformanceLogRelationship)
			{
				logger.info("Using logarithmic model type");
			}
			// validate the necessary opaque views are in place
			r.validateModelingOpaqueViews(Repository.MODEL_PERFORMANCE);
			Statement stmt = conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			stmt.setFetchDirection(ResultSet.FETCH_FORWARD);
			/*
			 * Create queries for performance model If none, add a blank one
			 */
			boolean notIterating = false;
			// Setup data set
			FastVector attributes = new FastVector(numModeledColumns);
			FastVector unmodeledAttributes = new FastVector();
			attributes.addElement(new Attribute(optimizationGoal));
			// Create modeled and unmodeled attributes
			for (int count = 0; count < performance.Measures.length; count++)
			{
				if (performance.Measures[count].ModelFlag)
					attributes.addElement(new Attribute(performance.Measures[count].Name));
				else
					unmodeledAttributes.addElement(new Attribute(performance.Measures[count].Name));
			}
			// Now add any additional measures used in metadata
			HashSet<String> measureSet = new HashSet<String>();
			for (int count = 0; count < performance.Measures.length; count++)
			{
				if (minMeasures[count] != null)
				{
					if (!measureSet.contains(minMeasures[count]))
					{
						measureSet.add(minMeasures[count]);
						unmodeledAttributes.addElement(new Attribute(minMeasures[count]));
					}
				}
				if (maxMeasures[count] != null)
				{
					if (!measureSet.contains(maxMeasures[count]))
					{
						measureSet.add(maxMeasures[count]);
						unmodeledAttributes.addElement(new Attribute(maxMeasures[count]));
					}
				}
				if (minImpMeasures[count] != null)
				{
					if (!measureSet.contains(minImpMeasures[count]))
					{
						measureSet.add(minImpMeasures[count]);
						unmodeledAttributes.addElement(new Attribute(minImpMeasures[count]));
					}
				}
				if (maxImpMeasures[count] != null)
				{
					if (!measureSet.contains(maxImpMeasures[count]))
					{
						measureSet.add(maxImpMeasures[count]);
						unmodeledAttributes.addElement(new Attribute(maxImpMeasures[count]));
					}
				}
				if (validMeasures[count] != null)
				{
					if (!measureSet.contains(validMeasures[count]))
					{
						measureSet.add(validMeasures[count]);
						unmodeledAttributes.addElement(new Attribute(validMeasures[count]));
					}
				}
			}
			// Create dataset, first value is target (class)
			dataSet = new Instances("Performance", attributes, 1000);
			unmodeledData = new Instances("Unmodeled", unmodeledAttributes, 1000);
			String curIterationAttribute = performance.PerformanceIterationDefaultAttribute;
			if ((curIterationAttribute == null) || (curIterationAttribute.length() == 0))
			{
				notIterating = true;
			}
			Query q = new Query(r);
			q.setFullOuterJoin(true);
			if (level == null || level.length() == 0)
			{
				LevelKey lk = r.getDimensionKey(targetDimension);
				for (String s : lk.ColumnNames)
				{
					q.addDimensionColumn(targetDimension, s);
					q.addGroupBy(new GroupBy(targetDimension, s));
				}
			} else
			{
				Level lvl = r.findDimensionLevel(targetDimension, level);
				LevelKey col = lvl.getPrimaryKey();
				q.addDimensionColumn(targetDimension, col.ColumnNames[0]);
				q.addGroupBy(new GroupBy(targetDimension, col.ColumnNames[0]));
			}
			if (notIterating)
			{
				q.addConstantColumn("'ALL'", "ALL");
			} else
			{
				q.addDimensionColumn(performance.PerformanceIterationDimension, curIterationAttribute);
				q.addGroupBy(new GroupBy(performance.PerformanceIterationDimension, curIterationAttribute));
			}
			/*
			 * Add any filters that don't filter on other iteration attributes (want to be able to have custom filters
			 * for each iteration attribute)
			 */
			for (int i = 0; i < performance.PerformanceFilters.length; i++)
			{
				// See if it is on another iteration attribute
				q.addFilterList(new String[]
				{ performance.PerformanceFilters[i] });
			}
			if (notIterating || performance.PerformanceIterationDefaultAttribute.equals(curIterationAttribute))
			{
				q.addMeasureColumn(optimizationGoal, optimizationGoal, false);
			}
			// Add the modeled measures first
			for (int count = 0; count < performance.Measures.length; count++)
			{
				if (performance.Measures[count].ModelFlag)
					q.addMeasureColumn(performance.Measures[count].Name, performance.Measures[count].Name, false);
			}
			// Then add the unmodeled measures
			for (int count = 0; count < performance.Measures.length; count++)
			{
				if (!performance.Measures[count].ModelFlag)
					q.addMeasureColumn(performance.Measures[count].Name, performance.Measures[count].Name, false);
			}
			// Now add any additional measures used in metadata
			measureSet = new HashSet<String>();
			for (int count = 0; count < performance.Measures.length; count++)
			{
				if (minMeasures[count] != null)
				{
					if (!measureSet.contains(minMeasures[count]))
					{
						measureSet.add(minMeasures[count]);
						q.addMeasureColumn(minMeasures[count], minMeasures[count], false);
					}
				}
				if (maxMeasures[count] != null)
				{
					if (!measureSet.contains(maxMeasures[count]))
					{
						measureSet.add(maxMeasures[count]);
						q.addMeasureColumn(maxMeasures[count], maxMeasures[count], false);
					}
				}
				if (minImpMeasures[count] != null)
				{
					if (!measureSet.contains(minImpMeasures[count]))
					{
						measureSet.add(minImpMeasures[count]);
						q.addMeasureColumn(minImpMeasures[count], minImpMeasures[count], false);
					}
				}
				if (maxImpMeasures[count] != null)
				{
					if (!measureSet.contains(maxImpMeasures[count]))
					{
						measureSet.add(maxImpMeasures[count]);
						q.addMeasureColumn(maxImpMeasures[count], maxImpMeasures[count], false);
					}
				}
				if (validMeasures[count] != null)
				{
					if (!measureSet.contains(validMeasures[count]))
					{
						measureSet.add(validMeasures[count]);
						q.addMeasureColumn(validMeasures[count], validMeasures[count], false);
					}
				}
			}
			q.generateQuery(false);
			logger.debug(q.getPrettyQuery());
			ResultSet rs = stmt.executeQuery(q.getQuery());
			/*
			 * Get result set metadata and create a column name map (not in the order that columns were added to query) -
			 * also set up the null map
			 */
			ResultSetMetaData meta = rs.getMetaData();
			HashMap<String, Integer> rsMap = new HashMap<String, Integer>();
			HashSet<String> nullMap = new HashSet<String>();
			for (int i = 1; i <= meta.getColumnCount(); i++)
			{
				String label = meta.getColumnLabel(i);
				rsMap.put(label, i);
				List<MeasureColumn> mclist = r.findMeasureColumns(label);
				if (mclist != null && mclist.size() > 0)
				{
					MeasureColumn mc = mclist.get(0);
					if (mc.mapNullToZeroBasedUponAggregationRule())
					{
						nullMap.add(label);
						logger.info("Column " + mc.ColumnName + " will be mapped to 0.0 if null");
					}
				}
			}
			int cnt = 0;
			int nullMapped = 0;
			int nullNotMapped = 0;
			while (rs.next())
			{
				// Get the target key
				String targetKey = rs.getString(1);
				// Get the iteration key
				String iterateKey = rs.getString(2);
				String uniqueKey = targetKey + iterateKey;
				/*
				 * See if row in data set exists, if not, create it. If it exists, that means this query is a later part
				 * of an existing query set
				 */
				Instance inst = null;
				Instance uninst = null;
				if (uniqueKeyMap.containsKey(uniqueKey))
				{
					inst = (Instance) uniqueKeyMap.get(uniqueKey);
					uninst = (Instance) unmodeledUniqueKeyMap.get(uniqueKey);
				} else
				{
					inst = new Instance(numModeledColumns);
					uninst = new Instance(unmodeledData.numAttributes());
					inst.setDataset(dataSet);
					dataSet.add(inst);
					uninst.setDataset(unmodeledData);
					unmodeledData.add(uninst);
					/*
					 * Need to get the instance back from the data set because when you add one, a new copy is created
					 */
					inst = dataSet.instance(dataSet.numInstances() - 1);
					uniqueKeyMap.put(uniqueKey, inst);
					uninst = unmodeledData.instance(unmodeledData.numInstances() - 1);
					unmodeledUniqueKeyMap.put(uniqueKey, uninst);
					// Add target key to list
					targetKeyList.add(targetKey);
					iterateKeyList.add(iterateKey);
				}
				// Now set the attribute values of the columns to model
				for (int count = 0; count < numModeledColumns; count++)
				{
					String attrName = ((Attribute) attributes.elementAt(count)).name();
					Integer intOb = rsMap.get(attrName);
					/*
					 * If this query returned a result for that column, then set its value
					 */
					if (intOb != null)
					{
						Object o = rs.getObject(intOb);
						if (o == null)
						{
							if (r.getModelParameters().MapNullsToZero && nullMap.contains(attrName))
							{
								inst.setValue(count, 0.0);
								nullMapped++;
							} else
							{
								inst.setMissing(count);
								nullNotMapped++;
							}
						} else
						{
							double d = rs.getDouble(intOb);
							if (performance.PerformanceLogRelationship)
							{
								if (d > 0)
								{
									inst.setValue(count, Math.log(d));
								}
							} else
							{
								inst.setValue(count, d);
							}
						}
					}
				}
				for (int count = 0; count < unmodeledData.numAttributes(); count++)
				{
					String attrName = ((Attribute) unmodeledAttributes.elementAt(count)).name();
					Integer intOb = rsMap.get(attrName);
					/*
					 * If this query returned a result for that column, then set its value
					 */
					if (intOb != null)
					{
						Object o = rs.getObject(intOb);
						if (o == null)
						{
							if (r.getModelParameters().MapNullsToZero && nullMap.contains(attrName))
							{
								inst.setValue(count, 0.0);
								nullMapped++;
							} else
							{
								inst.setMissing(count);
								nullNotMapped++;
							}
						} else
						{
							double d = rs.getDouble(intOb);
							if (performance.PerformanceLogRelationship)
							{
								if (d > 0)
								{
									uninst.setValue(count, Math.log(d));
								}
							} else
							{
								uninst.setValue(count, d);
							}
						}
					}
				}
				if ((++cnt % 10000) == 0)
				{
					logger.debug("Processed row: " + cnt);
				}
			}
			logger.info("Processed rows: " + cnt);
			logger.debug("Mapped nulls to 0.0: " + nullMapped + ", left nulls alone: " + nullNotMapped);
			rs.close();
			rs = null;
			stmt.close();
			dataSet.setClassIndex(0);
			if (dataSet.numInstances() < dataSet.numAttributes())
				throw (new Exception("Insufficient data to build performance model: dataSet.numInstances() = " + dataSet.numInstances()
						+ ", dataSet.numAttributes() = " + dataSet.numAttributes()));
			/*
			 * Go throught the dataset and remove any entries with null targets
			 */
			List<Integer> nullInstances = new ArrayList<Integer>();
			for (int i = 0; i < dataSet.numInstances(); i++)
			{
				if (dataSet.instance(i).isMissing(0))
				{
					nullInstances.add(i);
				}
			}
			for (int nullIndex = 0; nullIndex < nullInstances.size(); nullIndex++)
			{
				Integer I = nullInstances.get(nullIndex);
				int i = I.intValue();
				targetKeyList.remove(i);
				iterateKeyList.remove(i);
				dataSet.delete(i);
				unmodeledData.delete(i);
				/*
				 * Go through list and decrement any ones that need to be moved down
				 */
				for (int nullIndex2 = nullIndex + 1; nullIndex2 < nullInstances.size(); nullIndex2++)
				{
					int i2 = nullInstances.get(nullIndex2).intValue();
					if (i2 > i)
						nullInstances.set(nullIndex2, i2 - 1);
				}
			}
			if (dataSet.numInstances() < dataSet.numAttributes())
				throw (new Exception("Insufficient data with non-null target values to build performance model: dataSet.numInstances() = "
						+ dataSet.numInstances() + ", dataSet.numAttributes() = " + dataSet.numAttributes()));
			/*
			 * Add it to a map that maps the key to all row numbers where it is present (due to iterating over the
			 * iteration key
			 */
			for (int i = 0; i < targetKeyList.size(); i++)
			{
				String targetKey = targetKeyList.get(i);
				List<Integer> list = (List<Integer>) targetKeyMap.get(targetKey);
				if (list != null)
				{
					list.add(Integer.valueOf(i));
				} else
				{
					list = new ArrayList<Integer>();
					list.add(Integer.valueOf(i));
					targetKeyMap.put(targetKey, list);
				}
			}
			// Discretize the appropriate columns
			int[] dlist = new int[performance.Measures.length];
			int dcount = 0;
			for (int i = 0; i < performance.Measures.length; i++)
			{
				if (performance.Measures[i].Bucket > 0)
				{
					dlist[dcount++] = i + 1;
				}
			}
			// Discretize if columns were found that should be discretized
			if (dcount > 0)
			{
				for (int i = 0; i < dcount; i++)
				{
					Discretize df = new Discretize();
					df.setAttributeIndicesArray(new int[]
					{ dlist[i] });
					df.setBins(performance.Measures[dlist[i] - 1].Bucket);
					df.setInputFormat(dataSet);
					df.setUseEqualFrequency(true);
					dataSet = Filter.useFilter(dataSet, df);
				}
			}
			Instances modelDataSet = dataSet;
			/*
			 * Pull model iteration instances if necessary (other values are utilized for measuring variance of values
			 * across iterations)
			 */
			if ((performance.PerformanceIterationModelValue != null) && (performance.PerformanceIterationModelValue.length() > 0))
			{
				// Copy dataset format
				modelDataSet = new Instances(dataSet, 0);
				// Now copy instances
				for (int i = 0; i < dataSet.numInstances(); i++)
				{
					if (iterateKeyList.get(i).equals(performance.PerformanceIterationAnalysisValue))
					{
						modelDataSet.add(dataSet.instance(i));
					}
				}
			}
			int delta = (int) (System.currentTimeMillis() - queryStart);
			logger.info(String.format("Query Returned: " + dataSet.numInstances() + " instances in " + delta + "ms"));
			ClassifierSettings classifiers = performance.Classifiers == null ? r.getModelParameters().Classifiers : performance.Classifiers;
			BestFitClassifier bfc = new BestFitClassifier("Performance", modelDataSet, true, true, false, true, r.getModelParameters().MaxComputationThreads,
					r.getModelParameters(), classifiers);
			bfr = bfc.findBestFit();
			saveData();
		} catch (Exception ex)
		{
			logger.error(ex.toString(), ex);
		}
	}

	/**
	 * Save dataset, keys and maps
	 */
	private void saveData() throws Exception
	{
		File directory = new File(path);
		File tempFile = File.createTempFile("performance", null, directory);
		logger.info("Writing to performance model to temporary file " + tempFile.getName());
		FileOutputStream fo = new FileOutputStream(tempFile);
		ObjectOutput s = new ObjectOutputStream(fo);
		s.writeObject(bfr.getClassifier());
		s.writeObject(dataSet);
		s.writeObject(unmodeledData);
		s.writeObject(Integer.valueOf(numModeledColumns));
		s.writeObject(targetKeyMap);
		s.writeObject(targetKeyList);
		s.writeObject(iterateKeyList);
		s.writeObject(optimizedResults);
		s.close();
		fo.close();
		logger.info("Renaming temporary file to " + perfModelFile);
		File f2 = new File(perfModelFile);
		f2.delete();
		if (tempFile.renameTo(f2))
		{
			tempFile.delete();
		} else
		{
			logger.error("Rename of temporary file failed, keeping temporary file around");
		}
	}

	/**
	 * Retrieve dataset, keys and maps
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private boolean getData() throws Exception
	{
		logger.info("Reading file " + perfModelFile);
		FileInputStream in = null;
		try
		{
			in = new FileInputStream(perfModelFile);
		} catch (FileNotFoundException fnf)
		{
			logger.error("Error: " + perfModelFile + " does not exist.  Must generate performance model before impact matrix");
			return false;
		}
		ObjectInputStream s = new ObjectInputStream(in);
		bfr = new BestFitResult();
		bfr.setClassifier((Classifier) s.readObject());
		dataSet = (Instances) s.readObject();
		unmodeledData = (Instances) s.readObject();
		numModeledColumns = ((Integer) s.readObject()).intValue();
		targetKeyMap = (HashMap) s.readObject();
		targetKeyList = (ArrayList) s.readObject();
		iterateKeyList = (ArrayList) s.readObject();
		optimizedResults = (Map) s.readObject();
		in.close();
		return true;
	}

	public void saveArff(String path) throws Exception
	{
		if (!getData())
			return;
		Instances modelDataSet = dataSet;
		if ((performance.PerformanceIterationModelValue != null) && (performance.PerformanceIterationModelValue.length() > 0))
		{
			// Copy dataset format
			modelDataSet = new Instances(dataSet, 0);
			// Now copy instances
			for (int i = 0; i < dataSet.numInstances(); i++)
			{
				if (iterateKeyList.get(i).equals(performance.PerformanceIterationModelValue))
				{
					modelDataSet.add(dataSet.instance(i));
				}
			}
		}
		logger.info("full dataset size is " + dataSet.numInstances() + ", dataset size filtered by iteration value for 'Model' (" + performance.PerformanceIterationModelValue + ") is "
				+ modelDataSet.numInstances());
		if (modelDataSet.numInstances() == 0)
		{
			logger.error("No instances in the dataset match the iteration value for 'Model': " + performance.PerformanceIterationModelValue);
		}
		WekaUtil.writeArff(modelDataSet, path + File.separator + "perf", null);
	}

	/**
	 * Routine to generate the performance impact table. Will pull performance data on each target, then build a model
	 * of performance. It will then pull performance peers for each target and build a table that documents top leverage
	 * areas and the predicted impacts.
	 */
	public void generateImpactTable()
	{
		try
		{
			if (!getData())
				return;
			generateImpactMatrix();
			try
			{
				List<String> outputColumns = new ArrayList<String>();
				List<String> outputColumnTypes = new ArrayList<String>();
				outputColumns.add(r.getResource("Performance ID Key").getValue());
				outputColumnTypes.add("Varchar(30)");
				outputColumns.add(r.getResource("Performance Iteration Key").getValue());
				outputColumnTypes.add("Varchar(30)");
				outputColumns.add(r.getResource("Performance Flag Column").getValue());
				outputColumnTypes.add("Char(1)");
				outputColumns.add(r.getResource("Performance Measure Name Column").getValue());
				outputColumnTypes.add("Varchar(60)");
				outputColumns.add(r.getResource("Performance Category Name Column").getValue());
				outputColumnTypes.add("Varchar(60)");
				outputColumns.add(r.getResource("Performance Measure Value Column").getValue());
				outputColumnTypes.add("Float");
				outputColumns.add(r.getResource("Performance Peer Value Column").getValue());
				outputColumnTypes.add("Float");
				outputColumns.add(r.getResource("Performance Percentile Column").getValue());
				outputColumnTypes.add("Float");
				outputColumns.add(r.getResource("Performance Measure Impact Column").getValue());
				outputColumnTypes.add("Float");
				outputColumns.add(r.getResource("Performance Peer Difference Impact Column").getValue());
				outputColumnTypes.add("Float");
				outputColumns.add(r.getResource("Performance Opportunity Column").getValue());
				outputColumnTypes.add("Varchar(150)");
				Database.createTable(r, dc, performance.PerformanceOutputTable, outputColumns, outputColumnTypes, performance.DropExistingTable, false, null, null, null);
				Map<String, String> valueByColumn = new HashMap<String, String>();
				valueByColumn.put(r.getResource("Performance Iteration Key").getValue(), r.replaceVariables(null, performance.PerformanceIterationAnalysisValue));
				Database.deleteOldData(dc, performance.PerformanceOutputTable, valueByColumn);
			} catch (SQLException ex)
			{
				logger.error("Exception: " + ex.toString());
				return;
			}
			logger.info("Retrieving Peers");
			peerMap = (new Peers(r, performance.ReferencePopulationName)).retrievePeerMap(true, null);
			// Current success pattern definition
			SuccessPatternAnalysis spa = null;
			// If need to analyze search patterns, setup
			if (performance.SearchPatterns)
			{
				try
				{
					spa = new SuccessPatternAnalysis(null, r, dc, performance.SearchPatternDefinition);
				} catch (Exception ex)
				{
					// If an error, don't search for patterns, but continue
					performance.SearchPatterns = false;
					logger.info("Unable to load success model: " + performance.SearchPatternDefinition + " - Not Analyzing Patterns");
				}
			}
			logger.info("Saving Performance Data");
			int maxThreads = r.getModelParameters().MaxComputationThreads;
			Thread[] tlist = new Thread[maxThreads];
			nextIndex = 0;
			for (int i = 0; i < maxThreads; i++)
			{
				/*
				 * Need to clone the success pattern structure if multi-threading (contains state)
				 */
				if (performance.SearchPatterns)
				{
					if (i > 0)
					{
						spa = (SuccessPatternAnalysis) spa.clone();
					}
				}
				tlist[i] = new impactTableThread(spa, bfr.getClassifier());
				tlist[i].start();
			}
			for (int i = 0; i < maxThreads; i++)
			{
				tlist[i].join();
			}
			// create indexes if they don't already exist
			Database.createIndex(dc, r.getResource("Performance ID Key").getValue(), performance.PerformanceOutputTable);
			Database.createIndex(dc, r.getResource("Performance Iteration Key").getValue(), performance.PerformanceOutputTable);
		} catch (Exception ex)
		{
			logger.error(ex.toString(), ex);
		}
	}

	/**
	 * Generates a matrix that has the incremental impact of improvement for each column of each instance. Also
	 * summarizes the average overall impact of a given measure in the avgImpacts structure. AvgImpacts records the
	 * average absolute impact of changes in a given measure. This is meant to capture the relative significance of a
	 * given measure.
	 * 
	 * @throws Exception
	 */
	private void generateImpactMatrix() throws Exception
	{
		// Get the covariance matrix for the entire population
		DoubleMatrix2D covariance = returnCovarianceMatrix(null, 0);
		// Calculate the standard deviation of the target variable
		double targetSD = Math.sqrt(covariance.get(0, 0));
		if (targetSD == 0)
		{
			throw new Exception("Cannot calculate performance matrix - target measure does not vary");
		}
		impactMatrix = new double[dataSet.numInstances()][numModeledColumns - 1];
		avgImpacts = new double[numModeledColumns]; // can be NaN
		/*
		 * Find max/min ranges for each attribute
		 */
		double[] minimums = new double[numModeledColumns];
		double[] maximums = new double[numModeledColumns];
		for (int i = 0; i < numModeledColumns; i++)
		{
			minimums[i] = Double.NaN;
			maximums[i] = Double.NaN;
		}
		int numInstances = dataSet.numInstances();
		for (int j = 0; j < numInstances; j++)
		{
			for (int i = 1; i < numModeledColumns; i++)
			{
				if (Double.isNaN(minimums[i]) && !dataSet.instance(j).isMissing(i))
				{
					minimums[i] = dataSet.instance(j).value(i);
					maximums[i] = dataSet.instance(j).value(i);
				} else
				{
					if (!dataSet.instance(j).isMissing(i))
					{
						double val = dataSet.instance(j).value(i);
						if (val > maximums[i])
							maximums[i] = val;
						if (val < minimums[i])
							minimums[i] = val;
					}
				}
				double mx = getMax(i, dataSet.instance(j), unmodeledData.instance(j));
				if (!Double.isNaN(mx))
					maximums[i] = Math.min(maximums[i], mx);
				double mn = getMin(i, dataSet.instance(j), unmodeledData.instance(j));
				if (!Double.isNaN(mn))
					minimums[i] = Math.max(minimums[i], mn);
			}
		}
		int[] count = new int[numModeledColumns];
		for (int j = 0; j < numInstances; j++)
		{
			// Get current predicted value of a given instance
			double pred = bfr.getClassifier().classifyInstance(dataSet.instance(j));
			/*
			 * Now, loop through all the columns, and test their incremental impact
			 */
			for (int i = 1; i < numModeledColumns; i++)
			{
				// Only test impact of non-discretized columns
				if (performance.Measures[getRepositoryIndex(i)].Bucket == 0)
				{
					double val = dataSet.instance(j).value(i);
					Instance newI = (Instance) dataSet.instance(j).copy();
					newI.setDataset(dataSet);
					/*
					 * Test the impact of an improvement. Use whether an improvement is a positive or negative change to
					 * the metric
					 */
					double sd = Math.sqrt(covariance.get(i, i));
					double weight = sd / targetSD;
					int repIndex = getRepositoryIndex(i);
					double newval = 0;
					if (performance.PerformanceLogRelationship)
					{
						double diff = (improveList[repIndex] >= 0 ? 1 : -1) * Math.log(performance.PerformanceImpactPercent / 100);
						newval = val + diff;
						newval = Math.min(maximums[i], newval);
						newval = Math.max(minimums[i], newval);
					} else
					{
						double diff = (improveList[repIndex] >= 0 ? 1 : -1) * (performance.PerformanceImpactPercent / 100);
						newval = val + (sd * diff);
						newval = Math.min(maximums[i], newval);
						newval = Math.max(minimums[i], newval);
					}
					// Make sure it's not worse than the original
					if (improveList[repIndex] >= 0)
					{
						newval = Math.max(val, newval);
					} else
					{
						newval = Math.min(val, newval);
					}
					newI.setValue(i, newval);
					double d;
					// If there's an allowable test, then calculate impact
					if (newI.value(i) - dataSet.instance(j).value(i) != 0)
					{
						double newVal = bfr.getClassifier().classifyInstance(newI);
						/*
						 * Calculate the correlation between the measure and the target. This is calculated as the slope
						 * of the relationship at that point scaled by the ratio of standard deviations (mutch as the
						 * single variable regression coefficient relates to the correlation coefficient). Also, might
						 * be considered a generalized version of partial correlation (although it's not derived from a
						 * traditional regression)
						 */
						if (performance.PerformanceLogRelationship)
						{
							d = (Math.exp(newVal) - Math.exp(pred)) / (Math.exp(newI.value(i)) - Math.exp(dataSet.instance(j).value(i)));
						} else
						{
							d = (newVal - pred) / (newI.value(i) - dataSet.instance(j).value(i));
						}
						d *= weight;
					} else
						d = 0;
					impactMatrix[j][i - 1] = d;
					if (!Double.isNaN(d))
					{
						avgImpacts[i] += Math.abs(d); // NaN filtered out
						count[i]++;
					}
				}
			}
		}
		// Normalize the weights
		double totalImpacts = 0; // can not be NaN
		for (int i = 1; i < numModeledColumns; i++)
		{
			avgImpacts[i] /= count[i]; // cound be NaN if count is 0
			if (!Double.isNaN(avgImpacts[i]))
				totalImpacts += avgImpacts[i];
		}
		for (int i = 1; i < numModeledColumns; i++)
		{
			avgImpacts[i] /= totalImpacts; // could be NaN if totalImpacts is 0 or NaN
		}
	}

	private class impactTableThread extends Thread
	{
		private SuccessPatternAnalysis spa;
		private Classifier c;

		public impactTableThread(SuccessPatternAnalysis spa, Classifier c) throws Exception
		{
			this.setName("Impact Table - " + this.getId());
			this.spa = spa;
			this.c = Classifier.makeCopy(c);
		}

		public void run()
		{
			try
			{
				// Prepare SQL statement
				Connection conn = dc.ConnectionPool.getConnection();
				PreparedStatement pstmt = conn.prepareStatement("INSERT INTO " + performance.PerformanceOutputTable + " VALUES(?,?,?,?,?,?,?,?,?,?,?)");
				// Generate list of categories
				List<String> categories = new ArrayList<String>();
				for (int i = 1; i < numModeledColumns; i++)
				{
					String category = r.getMeasureCategory(dataSet.attribute(i).name());
					if ((category != null) && (categories.indexOf(category) < 0))
					{
						categories.add(category);
					}
				}
				while (true)
				{
					int index = getNextIndex();
					if (index < 0)
					{
						return;
					}
					String targetKey = (String) targetKeyList.get(index);
					String iterateKey = (String) iterateKeyList.get(index);
					if (iterateKey.equals(performance.PerformanceIterationAnalysisValue))
					{
						logger.info("Processing (Performance) target: " + targetKey);
						PerformanceStatistics perfStats = getPerformanceStatistics(targetKey);
						if (perfStats.pavg != null)
						{
							/*
							 * Get total impact of all measures
							 */
							double impacts[] = new double[numModeledColumns];
							double totalMeasureImpacts = 0; // can not be NaN
							for (int i = 1; i < numModeledColumns; i++)
							{
								impacts[i] = impactMatrix[index][i - 1]; // can be NaN
								totalMeasureImpacts += Double.isNaN(impacts[i]) ? 0 : Math.abs(impacts[i]);
							}
							PeerDifferenceAnalysis pda = getPeerDiffImpacts(perfStats); // impacts can be NaN
							/*
							 * Prepare to save output Output one row per measure for modeled columns
							 */
							for (int i = 1; i < numModeledColumns; i++)
							{
								MeasureColumn mc = r.findMeasureColumn(dataSet.attribute(i).name());
								pstmt.setString(1, targetKey);
								pstmt.setString(2, iterateKey);
								pstmt.setString(3, "M");
								pstmt.setString(4, mc.ColumnName);
								pstmt.setString(5, mc.PerformanceCategory);
								// Actual value
								if (Double.isNaN(perfStats.values[i]))
									pstmt.setNull(6, Types.DOUBLE);
								else
									pstmt.setDouble(6, perfStats.values[i]);
								// Peer average value
								if (Double.isNaN(perfStats.pavg[i]))
									pstmt.setNull(7, Types.DOUBLE);
								else
									pstmt.setDouble(7, perfStats.pavg[i]);
								// Percentile ranking - set to 0 if NaN, this should never occur
								if (Double.isNaN(perfStats.ptile[i]))
									pstmt.setDouble(8, 0);
								else
									pstmt.setDouble(8, perfStats.ptile[i]);
								// Impact of x% change in value
								if (Double.isNaN(impacts[i]))
									pstmt.setNull(9, Types.DOUBLE);
								else
									pstmt.setDouble(9, impacts[i]);
								// Impact of differences from peer average
								if (Double.isNaN(pda.impacts[i]))
									pstmt.setNull(10, Types.DOUBLE);
								else
									pstmt.setDouble(10, pda.impacts[i]);
								pstmt.setString(11, "");
								pstmt.addBatch();
							}
							pstmt.executeBatch();
							// Output one row per measure for unmodeled columns
							for (int i = numModeledColumns; i < numModeledColumns + numUnmodeled; i++)
							{
								MeasureColumn mc = r.findMeasureColumn(unmodeledData.attribute(i - numModeledColumns).name());
								pstmt.setString(1, targetKey);
								pstmt.setString(2, iterateKey);
								pstmt.setString(3, "M");
								pstmt.setString(4, mc.ColumnName);
								pstmt.setString(5, mc.PerformanceCategory);
								// Actual value
								if (Double.isNaN(perfStats.values[i]))
									pstmt.setNull(6, Types.DOUBLE);
								else
									pstmt.setDouble(6, perfStats.values[i]);
								// Peer average value
								if (Double.isNaN(perfStats.pavg[i]))
									pstmt.setNull(7, Types.DOUBLE);
								else
									pstmt.setDouble(7, perfStats.pavg[i]);
								// Percentile ranking - set to 0 if NaN, this should never occur
								if (Double.isNaN(perfStats.ptile[i]))
									pstmt.setDouble(8, 0);
								else
									pstmt.setDouble(8, perfStats.ptile[i]);
								pstmt.setDouble(9, 0);
								pstmt.setDouble(10, 0);
								pstmt.setString(11, "");
								pstmt.addBatch();
							}
							pstmt.executeBatch();
							// Output one row per category, and one summary
							double sumAvgPtile = 0; // can not be NaN
							for (int i = 0; i < categories.size(); i++)
							{
								double totalWeights = 0; // can not be NaN
								double avgPtile = 0; // can not be NaN
								double totalCatImpact = 0; // can not be NaN
								String cat = (String) categories.get(i);
								for (int j = 1; j < numModeledColumns; j++)
								{
									if (cat.equals(r.getMeasureCategory(dataSet.attribute(j).name())))
									{
										if (!Double.isNaN(avgImpacts[j]))
										{
											totalWeights += avgImpacts[j];
											avgPtile += perfStats.ptile[j] * avgImpacts[j];
											sumAvgPtile += perfStats.ptile[j] * avgImpacts[j];
											totalCatImpact += Double.isNaN(impacts[j]) ? 0 : Math.abs(impacts[j]);
										}
									}
								}
								pstmt.setString(1, targetKey);
								pstmt.setString(2, iterateKey);
								pstmt.setString(3, "C");
								pstmt.setString(4, cat);
								pstmt.setString(5, cat);
								// No actual value
								pstmt.setDouble(6, 0);
								// No peer average value
								pstmt.setDouble(7, 0);
								/*
								 * Weighted average percentile ranking for category
								 */
								if (totalWeights == 0)
									pstmt.setDouble(8, 0);
								else
									pstmt.setDouble(8, avgPtile / totalWeights);
								// Weighted average impact of measures in category
								if (totalMeasureImpacts == 0)
									/*
									 * % of impact due to category - 0 if all measures are zero
									 */
									pstmt.setDouble(9, 0);
								else
									// % of impact due to category (totalCatImpact can not be NaN)
									pstmt.setDouble(9, totalCatImpact / totalMeasureImpacts);
								// No impact of difference from peers
								pstmt.setDouble(10, 0);
								// No opportunity message string
								pstmt.setString(11, "");
								pstmt.addBatch();
							}
							pstmt.executeBatch();
							// Generate optimization results, if necessary
							if (performance.Optimize)
							{
								OptimizedResult or = (OptimizedResult) optimizedResults.get(targetKey);
								if (or != null)
								{
									int[] indices = or.op.getChangedIndices();
									Instance newInst = or.op.getSolutionInstance();
									Instance originalInstance = (Instance) or.op.getOriginalInstance();
									// Calculate impacts first
									double[] oimpacts = new double[indices.length];
									double totalOImpact = 0;
									for (int j = 0; j < indices.length; j++)
									{
										Instance testInst = (Instance) originalInstance.copy();
										double curVal = c.classifyInstance(testInst);
										testInst.setValue(indices[j], newInst.value(indices[j]));
										double impVal = c.classifyInstance(testInst);
										oimpacts[j] = Math.abs(impVal - curVal);
										if (!Double.isNaN(oimpacts[j]))
											totalOImpact += oimpacts[j];
									}
									for (int j = 0; j < indices.length; j++)
									{
										MeasureColumn mc = r.findMeasureColumn(newInst.attribute(indices[j]).name());
										pstmt.setString(1, targetKey);
										pstmt.setString(2, iterateKey);
										pstmt.setString(3, "O");
										pstmt.setString(4, mc.ColumnName);
										pstmt.setString(5, mc.PerformanceCategory);
										// Actual value of optimized measure
										if (Double.isNaN(perfStats.values[indices[j]]))
											pstmt.setNull(6, Types.DOUBLE);
										else
											pstmt.setDouble(6, perfStats.values[indices[j]]);
										double tval = newInst.value(indices[j]);
										/*
										 * Make sure that the reported targets show at least a minimum target change
										 * (even thought the optimizer didn't find much value in it) - If you're going
										 * to show a target, it doesn't make sense to have it be the same as the current
										 * value
										 */
										if (mc.Improve >= 0)
										{
											/*
											 * First, make sure it changes by as much as it should
											 */
											tval = Math.max(tval, perfStats.values[indices[j]] * (1 + performance.MinTargetImprovement / 100));
										} else
										{
											tval = Math.min(tval, perfStats.values[indices[j]] * (1 - performance.MinTargetImprovement / 100));
										}
										/*
										 * If the current value is zero, then use the average to approximate what a
										 * reasonable improvement should be
										 */
										if (tval == 0)
										{
											tval = newInst.dataset().meanOrMode(indices[j]) * performance.MinTargetImprovement / 100;
											/*
											 * But make sure average doesn't make it worse
											 */
											if (mc.Improve < 0)
												tval = Math.min(perfStats.values[indices[j]], tval);
											else
												tval = Math.max(perfStats.values[indices[j]], tval);
										}
										if (totalOImpact != 0)
										{
											/*
											 * Dampen the impact if desired. Dampening replaces each raw value with a
											 * mix of that value and the average impact (regressing every one towards
											 * the mean). A value of 0 means 0% use of average. A value of 1 will make
											 * all values equal to the average.
											 */
											double dampen = Math.max(0, Math.min(performance.DampeningFactor, 1));
											double dampenedValue = (dampen / indices.length) + ((1 - dampen) * oimpacts[j] / totalOImpact);
											/*
											 * If the impact is reduced, reduce the recommended change
											 */
											if (oimpacts[j] / totalOImpact > dampenedValue)
												tval -= (tval - perfStats.values[indices[j]]) * dampen;
											// Target value of optimized measure
											if (Double.isNaN(tval))
												pstmt.setNull(7, Types.DOUBLE);
											else
												pstmt.setDouble(7, tval);
											/*
											 * Incremental impact in the base case
											 */
											if (Double.isNaN((or.baseCasePredictedValue - originalInstance.value(0)) * dampenedValue))
												pstmt.setDouble(8, 0);
											else
												pstmt.setDouble(8, (or.baseCasePredictedValue - originalInstance.value(0)) * dampenedValue);
											/*
											 * Incremental impact of change in optimized measure
											 */
											if (Double.isNaN((or.predictedValue - originalInstance.value(0)) * dampenedValue))
												pstmt.setNull(9, Types.DOUBLE);
											else
												pstmt.setDouble(9, (or.predictedValue - originalInstance.value(0)) * dampenedValue);
										} else
										{
											// Target value of optimized measure
											if (Double.isNaN(tval))
												pstmt.setNull(7, Types.DOUBLE);
											else
												pstmt.setDouble(7, tval);
											// No incremental impacts
											pstmt.setDouble(8, 0);
											pstmt.setDouble(9, 0);
										}
										// No value of difference from peers
										pstmt.setDouble(10, 0);
										// Opportunity message string
										pstmt.setString(11, mc.Opportunity);
										pstmt.addBatch();
									}
									pstmt.executeBatch();
									// If need to analyze search patterns, setup
									if (performance.SearchPatterns)
									{
										List<String> measuresToAnalyze = new ArrayList<String>();
										for (int j = 0; j < indices.length; j++)
										{
											measuresToAnalyze.add(newInst.attribute(indices[j]).name());
										}
										spa.setMeasuresToAnalyze(measuresToAnalyze, true);
										spa.generateSuccessPatterns(1, targetKey);
									}
								}
							}
							// Save summary record
							pstmt.setString(1, targetKey);
							pstmt.setString(2, iterateKey);
							pstmt.setString(3, "S");
							pstmt.setString(4, "Summary");
							pstmt.setString(5, "Summary");
							// Actual value of target
							if (Double.isNaN(perfStats.values[0]))
								pstmt.setNull(6, Types.DOUBLE);
							else
								pstmt.setDouble(6, perfStats.values[0]);
							// Peer average of target
							if (Double.isNaN(perfStats.pavg[0]))
								pstmt.setNull(7, Types.DOUBLE);
							else
								pstmt.setDouble(7, perfStats.pavg[0]);
							// Average percentile ranking - can be NaN
							if (Double.isNaN(sumAvgPtile))
								pstmt.setDouble(8, 0);
							else
								pstmt.setDouble(8, sumAvgPtile);
							// No average incremental impact
							pstmt.setDouble(9, 0);
							// Unexplained variance
							if (Double.isNaN(perfStats.values[0] - pda.predictedValue))
								pstmt.setNull(10, Types.DOUBLE);
							else
								pstmt.setDouble(10, perfStats.values[0] - pda.predictedValue);
							// No overall message
							pstmt.setString(11, "");
							pstmt.executeUpdate();
						}
					}
				}
			} catch (Exception ex)
			{
				logger.error(ex.toString(), ex);
			}
		}
	}

	private static class PeerDifferenceAnalysis
	{
		double[] impacts;
		double predictedValue;
		double predictedPeerAverageValue;
	}

	/**
	 * Generate an estimate of the value of differences from peer average values. Get the predicted incremental value of
	 * changing the current instance to the peer average. Also get the incremental value of changing the peer average to
	 * the current value. Take the average of the two as an estimate. Normalize the impacts such that they add to the
	 * total difference between the predicted current value and the predicted peer average value. Note: values are
	 * incremental from the peer average. So impacts are the impact that a change from peer average to the actual
	 * instance value would have.
	 * 
	 * @param perfStats
	 * @return
	 */
	private PeerDifferenceAnalysis getPeerDiffImpacts(PerformanceStatistics perfStats) throws Exception
	{
		Instance inst = new Instance(perfStats.values.length);
		Instance peerInst = new Instance(perfStats.values.length);
		inst.setDataset(dataSet);
		peerInst.setDataset(dataSet);
		for (int i = 0; i < perfStats.values.length; i++)
		{
			if (Double.isNaN(perfStats.values[i]))
				inst.setMissing(i);
			else
				inst.setValue(i, perfStats.values[i]);
			if (Double.isNaN(perfStats.pavg[i]))
				peerInst.setMissing(i);
			else
				peerInst.setValue(i, perfStats.pavg[i]);
		}
		PeerDifferenceAnalysis pda = new PeerDifferenceAnalysis();
		pda.predictedValue = bfr.getClassifier().classifyInstance(inst);
		pda.predictedPeerAverageValue = bfr.getClassifier().classifyInstance(peerInst);
		pda.impacts = new double[perfStats.values.length];
		double total = 0;
		for (int i = 1; i < perfStats.values.length; i++)
		{
			Instance newInst = (Instance) inst.copy();
			Instance newPeerInst = (Instance) peerInst.copy();
			/*
			 * Revert actual value to peer average to note loss of value of that change
			 */
			double val = peerInst.value(i);
			if (!Double.isNaN(val))
				newInst.setValue(i, val);
			else
				newInst.setMissing(i);
			/*
			 * Change peer average to actual value to test for additional value created by that change
			 */
			val = inst.value(i);
			if (!Double.isNaN(val))
				newPeerInst.setValue(i, val);
			else
				newPeerInst.setMissing(i);
			double newInstVal = bfr.getClassifier().classifyInstance(newInst);
			double newPeerInstVal = bfr.getClassifier().classifyInstance(newPeerInst);
			/*
			 * Measure incremental benefit of having a value over having the peer average value
			 */
			if (Double.isNaN(pda.predictedValue - newInstVal) && (!Double.isNaN(newPeerInstVal - pda.predictedPeerAverageValue)))
				pda.impacts[i] = newPeerInstVal - pda.predictedPeerAverageValue;
			else if ((!Double.isNaN(pda.predictedValue - newInstVal)) && Double.isNaN(newPeerInstVal - pda.predictedPeerAverageValue))
				pda.impacts[i] = pda.predictedValue - newInstVal;
			else
				pda.impacts[i] = ((pda.predictedValue - newInstVal) + (newPeerInstVal - pda.predictedPeerAverageValue)) / 2;
			if (!Double.isNaN(pda.impacts[i]))
				total += pda.impacts[i];
		}
		// Normalize results
		if (total != 0)
			for (int i = 1; i < perfStats.values.length; i++)
			{
				pda.impacts[i] *= (pda.predictedValue - pda.predictedPeerAverageValue) / total; // can be NaN
			}
		return (pda);
	}

	private static class PerformanceStatistics
	{
		double[] values;
		double[] pavg;
		double[] ptile;
	}

	/**
	 * Return the performance statistics for a given target
	 * 
	 * @param targetKey
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	protected PerformanceStatistics getPerformanceStatistics(String targetKey)
	{
		ArrayList indexList = (ArrayList) targetKeyMap.get(targetKey);
		int index = 0;
		boolean found = false;
		for (int i = 0; i < indexList.size(); i++)
		{
			index = ((Integer) indexList.get(i)).intValue();
			// Figure out whether this is the right iteration to calculate
			if (iterateKeyList.get(index).equals(performance.PerformanceIterationAnalysisValue))
			{
				found = true;
				break;
			}
		}
		if (!found)
			return (null);
		// Create array of values
		int numTotalColumns = numModeledColumns + numUnmodeled;
		PerformanceStatistics perfStats = new PerformanceStatistics();
		perfStats.values = new double[numTotalColumns];
		for (int i = 0; i < numModeledColumns; i++)
		{
			perfStats.values[i] = dataSet.instance(index).value(i);
		}
		for (int i = 0; i < numUnmodeled; i++)
		{
			perfStats.values[i + numModeledColumns] = unmodeledData.instance(index).value(i);
		}
		// Get peers
		ArrayList peerList = (ArrayList) peerMap.get(targetKey);
		if (peerList != null)
		{
			// Calculate %tiles
			perfStats.ptile = new double[numTotalColumns];
			perfStats.pavg = new double[numTotalColumns];
			int[] peerCount = new int[numTotalColumns];
			for (int j = 0; j < peerList.size(); j++) // Update for each peer
			{
				// Find each peer in dataset
				Peer pr = (Peer) peerList.get(j);
				int peerIndex = 0;
				indexList = (ArrayList) targetKeyMap.get(pr.key);
				if (indexList != null)
				{
					for (int i = 0; i < indexList.size(); i++)
					{
						peerIndex = ((Integer) indexList.get(i)).intValue();
						if (iterateKeyList.get(peerIndex).equals(performance.PerformanceIterationAnalysisValue))
							break;
					}
					if (peerIndex >= 0)
					{
						for (int i = 0; i < numTotalColumns; i++)
						{
							double val = 0;
							int improveDir = 0;
							if (i < numModeledColumns)
							{
								int ind = getRepositoryIndex(dataSet.instance(peerIndex).attribute(i).name());
								val = dataSet.instance(peerIndex).value(i);
								if (ind < improveList.length)
									improveDir = improveList[ind];
							} else
							{
								int ind = getRepositoryIndex(unmodeledData.instance(peerIndex).attribute(i - numModeledColumns).name());
								val = unmodeledData.instance(peerIndex).value(i - numModeledColumns);
								if (ind < improveList.length)
									improveDir = improveList[ind];
							}
							if (!Double.isNaN(val))
							{
								perfStats.pavg[i] += val;
								/*
								 * Make sure directionality of measure is taken into account - all comparisons with NaN
								 * return false, so ptile will be 0 if values is NaN
								 */
								if (improveDir >= 0)
									perfStats.ptile[i] += (perfStats.values[i] > val) ? 1 : 0;
								else
									perfStats.ptile[i] += (perfStats.values[i] < val) ? 1 : 0;
								peerCount[i]++;
							}
						}
					}
				}
			}
			/*
			 * Cleanup averaging and Calculate impact of each measure
			 */
			for (int i = 0; i < numTotalColumns; i++)
			{
				if (peerCount[i] > 0)
				{
					perfStats.pavg[i] /= peerCount[i];
					perfStats.ptile[i] /= peerCount[i];
				}
			}
		}
		return (perfStats);
	}
	// Index for threading
	private int nextIndex;

	/**
	 * Return the next index
	 * 
	 * @return
	 */
	private synchronized int getNextIndex()
	{
		if (singleKey != null)
		{
			if (nextIndex == 0)
			{
				nextIndex = 1;
				for (int i = 0; i < targetKeyList.size(); i++)
					if (((String) targetKeyList.get(i)).equals(singleKey)
							&& ((String) iterateKeyList.get(i)).equals(performance.PerformanceIterationAnalysisValue))
						return (i);
				return (-1);
			} else
				return (-1);
		} else
		{
			do
			{
				nextIndex++;
				if (nextIndex >= targetKeyList.size())
					return (-1);
			} while ((optimizeList != null) && (!optimizeList.contains((String) targetKeyList.get(nextIndex))));
			return (nextIndex);
		}
	}

	/**
	 * Optimize all target keys and save in optimizeResults map
	 * 
	 * @param singleKey
	 *            If not null, just optimize for a single key
	 * @throws Exception
	 */
	public void optimize(String singleKey) throws Exception
	{
		getData();
		if (dataSet == null)
			throw (new Exception("Error: Performance dataset has not been generated"));
		// Set optimizer pararmeters
		Optimize.setMAX_GENERATIONS(r.getOptimizerParameters().MaxGenerations);
		Optimize.setMAX_SAME(r.getOptimizerParameters().MaxNoChangeGenerations);
		Optimize.setMUTATION_RATE(r.getOptimizerParameters().MutationRate);
		Optimize.setPOPULATION_SIZE(r.getOptimizerParameters().PopulationSize);
		Optimize.setREPRODUCTION_RATE(r.getOptimizerParameters().ReproductionRate);
		// Retrieve peer map
		peerMap = (new Peers(r, performance.ReferencePopulationName)).retrievePeerMap(true, null);
		// Record means for later use
		means = new double[numModeledColumns];
		for (int j = 1; j < numModeledColumns; j++)
		{
			means[j] = dataSet.meanOrMode(j);
		}
		if (optimizedResults == null)
			optimizedResults = Collections.synchronizedMap(new HashMap<String, OptimizedResult>());
		int maxThreads = r.getModelParameters().MaxComputationThreads;
		// int maxThreads = 1;
		logger.info("Optimizing Performance");
		long curTime = System.currentTimeMillis();
		Thread[] tlist = new Thread[maxThreads];
		nextIndex = 0;
		this.singleKey = singleKey;
		// maxThreads = 1;
		for (int i = 0; i < maxThreads; i++)
		{
			/*
			 * For concurrency purposes, copy the classifier (classifiers aren't re-entrant)
			 */
			Classifier c = Classifier.makeCopy(bfr.getClassifier());
			tlist[i] = new optimizeThread(c);
			tlist[i].start();
		}
		for (int i = 0; i < maxThreads; i++)
		{
			tlist[i].join();
		}
		logger.info("Optimization Complete: " + (System.currentTimeMillis() - curTime) + "ms to complete");
		saveData();
	}

	/**
	 * Thread class for multithreading the optimization process
	 * 
	 * @author bpeters
	 * 
	 */
	private class optimizeThread extends Thread
	{
		private Classifier c;
		private int cnt; // safety check
		private int cnt2;

		public optimizeThread(Classifier c)
		{
			this.c = c;
		}

		public void run()
		{
			while (true)
			{
				int index = getNextIndex();
				// If no more keys, end
				if (index < 0)
				{
					if (cnt == 0)
					{
						logger.error("The optimization thread did not process any instances, the target list does not match the data");
					} else if (cnt2 == 0)
					{
						logger.error("The optimization thread did not process any instances, the data does not match the iteration value for 'Score': "
								+ performance.PerformanceIterationAnalysisValue);
					}
					return;
				}
				cnt++;
				String targetKey = (String) targetKeyList.get(index);
				String iterateKey = (String) iterateKeyList.get(index);
				// Pick only the iteration to analyze
				if (iterateKey.equals(performance.PerformanceIterationAnalysisValue))
				{
					cnt2++;
					OptimizedResult or = null;
					try
					{
						logger.info(index + "/" + targetKeyList.size() + " - Optimizing: " + targetKey);
						or = optimizePerformance(targetKey, 1 + (performance.TargetImprovement / 100), performance.NumMeasuresToOptimize,
								performance.TargetMinimum, c);
					} catch (Exception ex)
					{
						logger.error(ex.toString(), ex);
					}
					if (or != null)
					{
						optimizedResults.put(targetKey, or);
					}
				}
			}
		}
	}

	/**
	 * Method to return the repository index (index into list of performance measures) of an element using the name of
	 * the attribute
	 * 
	 * @param datasetIndex
	 * @return
	 */
	private int getRepositoryIndex(String name)
	{
		int index = 0;
		for (; index < performance.Measures.length; index++)
		{
			if (performance.Measures[index].Name.equals(name))
				break;
		}
		return (index);
	}

	/**
	 * Method to return the repository index (index into list of performance measures) of an element using the index
	 * relative to the current dataset
	 * 
	 * @param datasetIndex
	 * @return
	 */
	protected int getRepositoryIndex(int datasetIndex)
	{
		int index = 0;
		for (; index < performance.Measures.length; index++)
		{
			if (performance.Measures[index].Name.equals(dataSet.attribute(datasetIndex).name()))
				break;
		}
		return (index);
	}

	/**
	 * Method to return the dataset index of an element using the index relative to the current repository (index into
	 * list of performance measures)
	 * 
	 * @param repositoryIndex
	 * @return
	 */
	protected int getDatasetIndex(int repositoryIndex)
	{
		int index = 0;
		for (; index < dataSet.numAttributes(); index++)
		{
			if (performance.Measures[repositoryIndex].Name.equals(dataSet.attribute(index).name()))
				break;
		}
		return (index);
	}

	/**
	 * If a metadata value (such as max/min or max improvement or min improvement) are calculated dynamically using
	 * another measure, then get that value
	 * 
	 * @param inst
	 * @param index
	 * @return
	 */
	protected double getDynamicValue(String name, Instance inst, Instance uinst)
	{
		double max = 0;
		Attribute uat = uinst.dataset().attribute(name);
		Attribute at = inst.dataset().attribute(name);
		if (at != null)
		{
			int index = at.index();
			max = inst.value(index);
		} else if (uat != null)
		{
			int index = uat.index();
			max = uinst.value(index);
		}
		return (max);
	}

	/**
	 * Optimize the performance of a given target. Seek a given improvement (1-based: e.g. 1.5 is a 50% improvement)
	 * with a given number of changes to attributes
	 * 
	 * @param key
	 *            Key of target
	 * @param improvement
	 *            1-based Improvement
	 * @param numChanges
	 *            Number of items to change
	 * @return Optimization of performance
	 * @throws Exception
	 */
	public OptimizedResult optimizePerformance(String key, double improvement, int numChanges, double minGoal, Classifier c) throws Exception
	{
		long start = System.currentTimeMillis();
		int baseCaseIndex = -1;
		int[] baseCaseIndices = null;
		List<Integer> baseCaseIndexList = new ArrayList<Integer>();
		// Retrieve improvement directions and base case measure designations
		for (int count = 0; count < performance.Measures.length; count++)
		{
			improveList[count] = r.findMeasureColumn(performance.Measures[count].Name).Improve;
			if (performance.Measures[count].BaseCaseMeasure)
			{
				baseCaseIndexList.add(Integer.valueOf(count));
			}
		}
		// If there are base case indices, record them
		if (baseCaseIndexList.size() > 0)
		{
			baseCaseIndices = new int[baseCaseIndexList.size()];
			for (int i = 0; i < baseCaseIndexList.size(); i++)
			{
				baseCaseIndices[i] = getDatasetIndex(((Integer) baseCaseIndexList.get(i)).intValue());
			}
			baseCaseIndexList = null;
			baseCaseIndex = baseCaseIndices[0];
		}
		// Get list of indexes of instances for key
		List<Integer> indexList = (List<Integer>) targetKeyMap.get(key);
		for (int index : indexList)
		{
			// Figure out whether this is the right iteration to calculate
			boolean useForImpact = (iterateKeyList.get(index).equals(performance.PerformanceIterationAnalysisValue));
			if (useForImpact)
			{
				Instance inst = dataSet.instance(index);
				Instance uinst = unmodeledData.instance(index);
				Instance baseInst = (Instance) inst.copy();
				double goal = 0;
				if (performance.TargetType == PerformanceModel.TYPE_IMPROVEMENT)
					goal = Math.max(minGoal, inst.value(0) * improvement);
				else if (performance.TargetType == PerformanceModel.TYPE_PROBABILITY)
					goal = performance.TargetProbability / 100;
				// Create new structure to store results
				OptimizedResult or = new OptimizedResult();
				or.goal = goal;
				// Do base case optimization if specified
				BaseCaseOptimal bco = null;
				MakeFeasible bcmf = null;
				double baseval = 0;
				if (baseCaseIndex >= 0)
				{
					logger.info("Base Case");
					// Setup base case optimization
					bco = new BaseCaseOptimal(baseInst, goal, c, key, r, null, null, null, this, baseCaseIndex, baseCaseIndices);
					bcmf = new MakeFeasible(baseInst, uinst, bco, c, this);
					bcmf.makeValidInstance(baseInst, new int[]
					{ baseCaseIndex });
					bco.setInstance(baseInst);
					baseval = bco.ac.classifyInstance(baseInst);
					or.baseCasePredictedValue = baseval;
				}
				MakeOptimal mo = new MakeOptimal(inst, goal, c, key, r, iterateKeyList, targetKeyMap, peerMap, this);
				MakeFeasible mf = new MakeFeasible(inst, uinst, mo, c, this);
				/*
				 * Allowable list of indices to change (include only columns designated to be optimized and not bad)
				 */
				List<Integer> allowableIndexList = new ArrayList<Integer>();
				for (int k = 1; k < numModeledColumns; k++)
				{
					if (performance.Measures[getRepositoryIndex(k)].Optimize && !mf.isBadColumn(k))
					{
						allowableIndexList.add(k);
					}
				}
				int[] indices = new int[allowableIndexList.size()];
				for (int k = 0; k < allowableIndexList.size(); k++)
					indices[k] = allowableIndexList.get(k);
				Optimize opt = new Optimize(key, inst, mf, indices, numChanges, true);
				logger.info("Finding Solution for: " + key);
				if (Double.isNaN(opt.findSolution()))
				{
					logger.warn("Could not find a solution for: " + key);
					return null;
				}
				logger.info("Found Solution for: " + key);
				int[] changed = opt.getChangedIndices();
				Instance newInst = opt.getSolutionInstance();
				if (DEBUG && logger.isDebugEnabled())
				{
					if (baseCaseIndex >= 0)
					{
						for (int j = 0; j < baseCaseIndices.length; j++)
						{
							double base = c.classifyInstance(inst);
							Instance inst2 = (Instance) inst.copy();
							inst2.setValue(baseCaseIndices[j], baseInst.value(baseCaseIndices[j]));
							double basediff = c.classifyInstance(inst2) - base;
							int ind = getRepositoryIndex(baseCaseIndices[j]);
							StringBuilder logBuf = new StringBuilder();
							logBuf.append("BaseCase: " + baseInst.attribute(baseCaseIndices[j]).name() + ": " + baseInst.value(baseCaseIndices[j]));
							logBuf.append(" Cur: " + inst.value(baseCaseIndices[j]) + " Max: " + bcmf.maximums[baseCaseIndices[j]] + ", Min: "
									+ bcmf.minimums[baseCaseIndices[j]] + ", Dev: " + Math.sqrt(bco.covariance.get(baseCaseIndices[j], baseCaseIndices[j])));
							logBuf.append(", Improve: " + improveList[ind] + ", Imp: " + basediff);
							logger.info(logBuf.toString());
						}
						logger.debug("BaseCase: New Predicted Val: " + baseval + ", Current: " + inst.value(0) + ", Goal: " + goal);
					}
					logger.debug("Key: " + key);
					for (int j = 0; j < changed.length; j++)
					{
						StringBuilder logBuf = new StringBuilder();
						double base = c.classifyInstance(inst);
						Instance inst2 = (Instance) inst.copy();
						inst2.setValue(changed[j], newInst.value(changed[j]));
						double basediff = c.classifyInstance(inst2) - base;
						int ind = getRepositoryIndex(changed[j]);
						double absdeviation = Math.sqrt(mo.covariance.get(changed[j], changed[j])) * Math.sqrt(performance.NumIterations);
						double d = Math.abs(newInst.value(changed[j]) - inst.value(changed[j]));
						double prob = 2 * (1 - Probability.normal(0, absdeviation * absdeviation, d));
						logBuf.append(newInst.attribute(changed[j]).name() + ": " + newInst.value(changed[j]));
						logBuf.append(" Cur: " + inst.value(changed[j]) + " Max: " + mf.maximums[changed[j]] + ", Min: " + mf.minimums[changed[j]] + ", Dev: "
								+ Math.sqrt(mo.covariance.get(changed[j], changed[j])));
						logBuf.append(", Improve: " + improveList[ind] + ", Imp: " + basediff);
						logBuf.append(", Prob: " + prob);
						logger.debug(logBuf.toString());
					}
				}
				or.op = opt;
				or.predictedValue = mo.ac.classifyInstance(newInst);
				// Run get objective to set the probability
				mo.getObjective(newInst);
				logger.info("New Predicted Val (" + key + "): " + or.predictedValue + ", Current: " + inst.value(0) + ", Goal: " + goal + ", Time (ms): "
						+ (System.currentTimeMillis() - start));
				return (or);
			}
		}
		return (null);
	}

	/**
	 * Output a sensitivity analysis for a given key
	 * 
	 * @param key
	 *            Key to simulate changes in attributes for
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public void simulateForKey(String key) throws Exception
	{
		getData();
		StringBuilder logBuf = new StringBuilder();
		for (Iterator it = targetKeyMap.entrySet().iterator(); it.hasNext();)
		{
			// Get the target key
			Map.Entry me = (Map.Entry) it.next();
			String targetKey = (String) me.getKey();
			if (targetKey.equals(key))
			{
				// Get list of indexes of instances for key
				ArrayList indexList = (ArrayList) me.getValue();
				for (int i = 0; i < indexList.size(); i++)
				{
					int index = ((Integer) indexList.get(i)).intValue();
					/*
					 * Figure out whether this is the right iteration to calculate
					 */
					boolean useForImpact = (iterateKeyList.get(index).equals(performance.PerformanceIterationAnalysisValue));
					if (useForImpact)
					{
						Instance inst = dataSet.instance(index);
						// Break into 20 intervals
						for (int k = -1; k <= 20; k++)
						{
							for (int j = 1; j < numModeledColumns; j++)
							{
								double val = inst.value(j);
								if (val != 0)
								{
									if (k == -1)
									{
										logBuf.append(dataSet.attribute(j).name());
										logBuf.append(",VAL,");
									} else
									{
										double v = val - Math.abs(val) + (k * val / 10);
										inst.setValue(j, v);
										double newVal = bfr.getClassifier().classifyInstance(inst);
										logBuf.append(v);
										logBuf.append(',');
										logBuf.append(newVal);
										logBuf.append(',');
										inst.setValue(j, val);
									}
								}
							}
						}
					}
				}
			}
		}
		logger.info(logBuf.toString());
	}

	/**
	 * Return the covariance matrix for instances around a target. If target is given (numIterations must be supplied)
	 * then peers are used, otherwise, it's the full target list.
	 * 
	 * @param targetKey
	 * @param numIterations
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected DoubleMatrix2D returnCovarianceMatrix(Object targetKey, int numIterations)
	{
		List<Peer> peerList = null;
		if (targetKey != null)
		{
			// Retrieve peer list
			peerList = peerMap.get(targetKey);
			if (peerList == null)
				targetKey = null;
		}
		@SuppressWarnings("rawtypes")
		Iterator it = null;
		int numRows = numIterations > 0 ? numIterations : 1;
		if (targetKey == null)
		{
			numRows = 0;
			it = targetKeyMap.entrySet().iterator();
			// Count the number of usable lists
			for (; it.hasNext();)
			{
				Entry<String, List<Integer>> me = ((Iterator<Entry<String, List<Integer>>>) it).next();
				numRows += me.getValue() == null || me.getValue().size() <= 1 ? 0 : me.getValue().size();
			}
			it = targetKeyMap.entrySet().iterator();
		} else
		{
			it = peerList.iterator();
			numRows *= peerList.size();
		}
		int curRow = 0;
		DoubleMatrix2D matrix = new DenseDoubleMatrix2D(numRows, numModeledColumns);
		curRow = 0;
		for (; it.hasNext();)
		{
			List<Integer> idList = null;
			// Get entire population deviation if no key is specified
			if (targetKey == null)
			{
				Entry<String, List<Integer>> me = ((Iterator<Entry<String, List<Integer>>>) it).next();
				idList = me.getValue();
			} else
			{
				// If a key is specified, use only peers of that key
				Peer peer = (Peer) it.next();
				idList = targetKeyMap.get(peer.key);
			}
			if (idList != null && idList.size() > 1)
			{
				// First, figure out the means in each group
				double[] means = new double[numModeledColumns];
				int[] counts = new int[numModeledColumns];
				for (int j = 0; j < idList.size(); j++)
				{
					Instance curInst = dataSet.instance(idList.get(j));
					for (int i = 0; i < numModeledColumns; i++)
					{
						if (!curInst.isMissing(i))
						{
							means[i] += curInst.value(i);
							counts[i]++;
						}
					}
				}
				for (int i = 0; i < numModeledColumns; i++)
				{
					if (counts[i] > 0)
						means[i] /= counts[i];
				}
				/*
				 * Go through each iteration in the iteration id list for this target
				 */
				for (int j = 0; j < idList.size(); j++)
				{
					Instance curInst = dataSet.instance(idList.get(j));
					// Go through each column
					for (int i = 0; i < numModeledColumns; i++)
					{
						double val = curInst.value(i);
						if (curInst.isMissing(i))
						{
							val = means[i];
						}
						// Remove mean to correct for different starting values
						matrix.set(curRow, i, val - means[i]);
					}
					curRow++;
				}
			}
		}
		// Now calculate covariance matrix
		return (Statistic.covariance(matrix));
	}

	/**
	 * @param optimizeList
	 *            The optimizeList to set.
	 */
	public void setOptimizeList(List<String> optimizeList)
	{
		this.optimizeList = optimizeList;
		logger.info("Optimize List: " + this.optimizeList.toString());
	}

	/**
	 * Return minimum value of a measure
	 * 
	 * @param index -
	 *            Index of measure in the dataset
	 * @param inst -
	 *            Modeled instance
	 * @param uinst -
	 *            Unmodeled instance
	 * @return
	 */
	public double getMin(int index, Instance inst, Instance uinst)
	{
		double min = minList[getRepositoryIndex(index)];
		// See if there's a dynamic min
		if (minMeasures[getRepositoryIndex(index)] != null)
		{
			min = getDynamicValue(minMeasures[getRepositoryIndex(index)], inst, uinst);
		}
		return (min);
	}

	/**
	 * Return maximum value of a measure
	 * 
	 * @param index -
	 *            Index of measure in the dataset
	 * @param inst -
	 *            Modeled instance
	 * @param uinst -
	 *            Unmodeled instance
	 * @return
	 */
	public double getMax(int index, Instance inst, Instance uinst)
	{
		double max = maxList[getRepositoryIndex(index)];
		// See if there's a dynamic min
		if (maxMeasures[getRepositoryIndex(index)] != null)
		{
			max = getDynamicValue(maxMeasures[getRepositoryIndex(index)], inst, uinst);
		}
		return (max);
	}

	/**
	 * Return minimum improvement value of a measure
	 * 
	 * @param index -
	 *            Index of measure in the dataset
	 * @param inst -
	 *            Modeled instance
	 * @param uinst -
	 *            Unmodeled instance
	 * @return
	 */
	public double getMinImp(int index, Instance inst, Instance uinst)
	{
		double minImp = minImpList[getRepositoryIndex(index)];
		// See if there's a dynamic min
		if (minImpMeasures[getRepositoryIndex(index)] != null)
		{
			minImp = getDynamicValue(minImpMeasures[getRepositoryIndex(index)], inst, uinst);
		}
		return (minImp);
	}

	/**
	 * Return maximum improvement value of a measure
	 * 
	 * @param index -
	 *            Index of measure in the dataset
	 * @param inst -
	 *            Modeled instance
	 * @param uinst -
	 *            Unmodeled instance
	 * @return
	 */
	public double getMaxImp(int index, Instance inst, Instance uinst)
	{
		double maxImp = maxImpList[getRepositoryIndex(index)];
		// See if there's a dynamic max
		if (maxImpMeasures[getRepositoryIndex(index)] != null)
		{
			maxImp = getDynamicValue(maxImpMeasures[getRepositoryIndex(index)], inst, uinst);
		}
		return (maxImp);
	}

	/**
	 * Return valid measure value of a measure
	 * 
	 * @param index -
	 *            Index of measure in the dataset
	 * @param inst -
	 *            Modeled instance
	 * @param uinst -
	 *            Unmodeled instance
	 * @return
	 */
	public double getValid(int index, Instance inst, Instance uinst)
	{
		double valid = 1;
		// See if there is a dynamic valid value
		if (validMeasures[getRepositoryIndex(index)] != null)
		{
			valid = getDynamicValue(validMeasures[getRepositoryIndex(index)], inst, uinst);
		}
		return (valid);
	}

	public boolean isAnalyzeOnlyOptimized()
	{
		return analyzeOnlyOptimized;
	}

	public void setAnalyzeOnlyOptimized(boolean analyzeOnlyOptimized)
	{
		this.analyzeOnlyOptimized = analyzeOnlyOptimized;
	}
}
