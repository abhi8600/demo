/**
 * $Id: Partition.java,v 1.35 2010-04-26 21:34:52 ricks Exp $
 *
 * Copyright (C) 2007-2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.io.FileInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import org.apache.log4j.Logger;

/**
 * Class to encapsulate a model dataset partition
 * 
 * @author Brad Peters
 * 
 */
class Partition
{
	private static Logger logger = Logger.getLogger(Partition.class);
	public Object key;
	private int startIndex;
	public Object[] targetKeys;
	public Object[] iteratorKeys;
	public Object[] segmentKeys;
	private int numColumns;
	private long[] columnBlockPositions = new long[0];
	private int[] columnBlockColumnOffsets = new int[0];
	private int[] columnBlockNumColumns = new int[0];
	private String fileName;
	private String dataFileName;
	private RandomAccessFile ra;
	private FileChannel fc;
	private MappedByteBuffer mb;
	private DoubleBuffer db;
	private ByteBuffer buf;
	private int endIndex;
	private boolean writeMode = false;
	private long writeStartPosition;

	public Partition(Object key, String modelDatasetFilename, boolean reset) throws IOException, ClassNotFoundException
	{
		this.key = key;
		fileName = createPartitionFilename(modelDatasetFilename, "m");
		dataFileName = createPartitionFilename(modelDatasetFilename, "d");
		if (reset)
			deleteFiles();
		else
			loadMetadata();
	}

	private String createPartitionFilename(String modelDatasetFilename, String token)
	{
		int lastDot = modelDatasetFilename.lastIndexOf('.');
		String filePrefix = modelDatasetFilename.substring(0, lastDot);
		String fileSuffix = modelDatasetFilename.substring(lastDot + 1);
		String fileName = filePrefix + "_" + (key == null ? "0_" + token : key.toString()) + "_" + token + "." + fileSuffix;
		return fileName;
	}
	
	/**
	 * delete the partition files
	 */
	private void deleteFiles() throws IOException
	{
		try
		{
			File df = new File(dataFileName);
			logger.debug("deleting file " + dataFileName);
			df.delete();
		}
		catch (Exception e)
		{
			logger.debug(e, e);
		}
		try
		{
			File df = new File(fileName);
			logger.debug("deleting file " + fileName);
			df.delete();
		}
		catch (Exception e)
		{
			logger.debug(e, e);
		}
	}

	/**
	 * Open data file for read-write access
	 * 
	 * @throws IOException
	 */
	public void openDataFile() throws IOException
	{
		ra = new RandomAccessFile(dataFileName, "rw");
		fc = ra.getChannel();
		logger.debug("Mapping " + dataFileName + " (" + fc.size() + " bytes)");
		mb = fc.map(MapMode.READ_WRITE, 0, fc.size());
		mb.position(0);
		db = mb.asDoubleBuffer();
		writeMode = false;
	}

	/**
	 * Open data file for read-only access using an existing byte buffer and loading the entire partition into it
	 * 
	 * @throws IOException
	 */
	public void openDataFile(ByteBuffer buffer) throws IOException
	{
		FileInputStream fis = new FileInputStream(dataFileName);
		fc = fis.getChannel();
		logger.debug("Reading " + dataFileName + " (" + fc.size() + " bytes)");
		buffer.clear();
		fc.read(buffer);
		buffer.position(0);
		db = buffer.asDoubleBuffer();
		writeMode = false;
		mb = null;
		ra = null;
	}

	/**
	 * Opens file for writing to the last columns in a block (indicated by numCols)
	 * 
	 * @param startCol
	 *            starting column position
	 * @param numCols
	 *            number of new columns
	 */
	public void openFileWrite(int startCol, int numCols)
	{
		buf = ByteBuffer.allocate(targetKeys.length * numCols * (Double.SIZE / Byte.SIZE));
		writeMode = true;
		db = buf.asDoubleBuffer();
		writeStartPosition = columnBlockPositions[startCol];
	}

	/**
	 * Attempt to load the entire mapped buffer into memory (to increase performance during read operations)
	 */
	public void loadMemory()
	{
		mb.load();
	}

	/**
	 * Close data file
	 * 
	 * @throws IOException
	 */
	public void closeDataFile() throws IOException
	{
		if (writeMode)
		{
			ra = new RandomAccessFile(dataFileName,"rw");
			fc = ra.getChannel();
			fc.position(writeStartPosition * (Double.SIZE / Byte.SIZE));
			buf.position(0);
			fc.write(buf);
			writeMode = false;
		}
		if (fc != null)
			fc.close();
		if (ra != null)
			ra.close();
		buf = null;
		fc = null;
		ra = null;
		mb = null;
		db = null;
	}

	/**
	 * Set the row start index for this partition
	 * 
	 * @param startIndex
	 */
	public void setStartIndex(int startIndex)
	{
		this.startIndex = startIndex;
	}

	/**
	 * Return the row start index for this partition
	 * 
	 * @return
	 */
	public int getStartIndex()
	{
		return (startIndex);
	}

	/**
	 * Return the number of columns in this partition
	 * 
	 * @return
	 */
	public int getNumColumns()
	{
		return (numColumns);
	}

	/**
	 * Return the number of rows in this partition
	 * 
	 * @return
	 */
	public int getNumRows()
	{
		return (targetKeys.length);
	}

	/**
	 * Save metadata associated with the partition
	 * 
	 * @throws IOException
	 */
	public void saveMetadata() throws IOException
	{
		File file = new File(fileName);
		File directory = file.getParentFile();
		File tempFile = File.createTempFile("metaData", null, directory);
		logger.info("Writing model to temporary file " + tempFile.getName());
		FileOutputStream fo = new FileOutputStream(tempFile);
		ObjectOutputStream oo = new ObjectOutputStream(fo);
		oo.writeObject(key);
		oo.writeObject(startIndex);
		oo.writeObject(targetKeys);
		oo.writeObject(iteratorKeys);
		oo.writeObject(segmentKeys);
		oo.writeObject(numColumns);
		oo.writeObject(columnBlockPositions);
		oo.writeObject(columnBlockColumnOffsets);
		oo.writeObject(columnBlockNumColumns);
		oo.close();
		fo.close();
		logger.info("Renaming temporary file to " + fileName);
		File f2 = new File(fileName);
		f2.delete();
		if (tempFile.renameTo(f2))
		{
			tempFile.delete();
		}
		else
		{
			logger.error("Rename of temporary file failed, keeping temporary file around");
		}
		if (mb != null)
			mb.force();
	}

	public void allocateNewBlock(int numCols) throws IOException
	{
		// properly size the partition data file
		if (ra != null)
			ra.close();
		ra = new RandomAccessFile(dataFileName, "rw");
		long startPos = ra.length() / (Double.SIZE / Byte.SIZE);
		ra.setLength(ra.length() + (targetKeys.length * numCols * (Double.SIZE / Byte.SIZE)));
		ra.close();
		ra = null;
		fc = null;
		
		// update the column position information in the meta data
		long[] oldColBlockPositions = columnBlockPositions;
		columnBlockPositions = new long[oldColBlockPositions.length + numCols];
		System.arraycopy(oldColBlockPositions, 0, columnBlockPositions, 0, oldColBlockPositions.length);
		//
		int[] oldColumnBlockColumnOffsets = columnBlockColumnOffsets;
		columnBlockColumnOffsets = new int[oldColumnBlockColumnOffsets.length + numCols];
		System.arraycopy(oldColumnBlockColumnOffsets, 0, columnBlockColumnOffsets, 0, oldColumnBlockColumnOffsets.length);
		//
		int[] oldColumnBlockNumColumns = columnBlockNumColumns;
		columnBlockNumColumns = new int[oldColumnBlockNumColumns.length + numCols];
		System.arraycopy(oldColumnBlockNumColumns, 0, columnBlockNumColumns, 0, oldColumnBlockNumColumns.length);
		for (int i = 0; i < numCols; i++)
		{
			columnBlockPositions[numColumns + i] = startPos;
			columnBlockColumnOffsets[numColumns + i] = numColumns;
			columnBlockNumColumns[numColumns + i] = numCols;
		}
		numColumns += numCols;
	}

	/**
	 * Get value of a cell using local row index (index within the partition)
	 * 
	 * @param row
	 * @param column
	 * @return
	 */
	public double getCell(int row, int column)
	{
		return (db.get((int) (columnBlockPositions[column] + (row * columnBlockNumColumns[column]) + column - columnBlockColumnOffsets[column])));
	}

	/**
	 * Get the value of a cell using global row index (index across all partitions)
	 * 
	 * @param row
	 * @param column
	 * @return
	 * @throws Exception
	 */
	public double getGlobalCell(int row, int column) throws Exception
	{
		return (db.get((int) (columnBlockPositions[column] + ((row - startIndex) * columnBlockNumColumns[column]) + column - columnBlockColumnOffsets[column])));
	}

	/**
	 * Set value of a cell using local row index
	 * 
	 * @param row
	 * @param column
	 * @param d
	 */
	public synchronized void setCell(int row, int column, double d)
	{
		int position;
		// do calculations of position in long as much as possible to allow very large total datasets
		if (writeMode)
		{
			position = (int) ((columnBlockPositions[column] + (row * columnBlockNumColumns[column]) + column - columnBlockColumnOffsets[column]) - writeStartPosition);
		} else
		{
			position = (int) (columnBlockPositions[column] + (row * columnBlockNumColumns[column]) + column - columnBlockColumnOffsets[column]);
		}
		db.put(position, d);
	}

	/**
	 * Set an entire row of values using local row index
	 * 
	 * @param row
	 * @param column
	 * @param d
	 */
	public synchronized void setRow(int row, int column, double[] d) throws IllegalArgumentException
	{
		int position = (int) ((columnBlockPositions[column] + (row * columnBlockNumColumns[column]) + column - columnBlockColumnOffsets[column]) - writeStartPosition);
		if (position < 0)
		{
			logger.error("setRow: position less than zero, position=" + position + ", row=" + row + ", column=" + column);
		}
		try
		{
			db.position(position);
			db.put(d, 0, d.length);
		}
		catch (IllegalArgumentException e)
		{
			logger.error("setRow: exception for position=" + position + ", row=" + row + ", column=" + column + ", d.length=" + d.length + ", db.capacity=" + db.capacity(), e);
			throw new IllegalArgumentException(e);
		}
	}

	/**
	 * Set the value of a cell using global row indexing
	 * 
	 * @param row
	 * @param column
	 * @param d
	 */
	public synchronized void setGlobalCell(int row, int column, double d)
	{
		db.put((int) (columnBlockPositions[column] + ((row - startIndex) * columnBlockNumColumns[column]) + column - columnBlockColumnOffsets[column]), d);
	}

	public Object getTargetKey(int row)
	{
		return (targetKeys[row - startIndex]);
	}

	public Object getIteratorKey(int row)
	{
		return (iteratorKeys[row - startIndex]);
	}

	public Object getSegmentKey(int row)
	{
		return (segmentKeys[row - startIndex]);
	}

	/**
	 * Load the metadata associated with the partition
	 * 
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private void loadMetadata() throws IOException, ClassNotFoundException
	{
		FileInputStream fi = null;
		try
		{
			fi = new FileInputStream(fileName);
			ObjectInputStream oi = new ObjectInputStream(fi);
			key = oi.readObject();
			startIndex = (Integer) oi.readObject();
			targetKeys = (Object[]) oi.readObject();
			iteratorKeys = (Object[]) oi.readObject();
			segmentKeys = (Object[]) oi.readObject();
			numColumns = (Integer) oi.readObject();
			columnBlockPositions = (long[]) oi.readObject();
			columnBlockColumnOffsets = (int[]) oi.readObject();
			columnBlockNumColumns = (int[]) oi.readObject();
			oi.close();
			fi.close();
			endIndex = startIndex + targetKeys.length;
		}
		catch (Exception e)
		{
			// corrupt file, just null everything out and let it rebuild
			key = null;
			startIndex = -1;
			targetKeys = null;
			iteratorKeys = null;
			segmentKeys = null;
			numColumns = -1;
			columnBlockPositions = null;
			columnBlockColumnOffsets = null;
			columnBlockNumColumns = null;
			endIndex = -1;
		}
	}

	/**
	 * @return the endIndex
	 */
	public int getEndIndex()
	{
		return endIndex;
	}
}