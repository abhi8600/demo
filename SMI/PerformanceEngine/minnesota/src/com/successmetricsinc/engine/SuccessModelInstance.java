/**
 * $Id: SuccessModelInstance.java,v 1.188 2012-08-05 21:16:20 bpeters Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.io.Externalizable;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.sql.Types;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.Stack;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import weka.clusterers.EM;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;

import com.successmetricsinc.Repository;
import com.successmetricsinc.engine.BreakoutAttribute.ValueSet;
import com.successmetricsinc.engine.Header.HeaderType;
import com.successmetricsinc.query.ConnectionPool;
import com.successmetricsinc.query.Database;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.DimensionColumn;
import com.successmetricsinc.query.DimensionColumnNav;
import com.successmetricsinc.query.DisplayFilter;
import com.successmetricsinc.query.GroupBy;
import com.successmetricsinc.query.LevelKey;
import com.successmetricsinc.query.MeasureColumn;
import com.successmetricsinc.query.NavigationException;
import com.successmetricsinc.query.Query;
import com.successmetricsinc.query.QueryAggregate;
import com.successmetricsinc.query.QueryFilter;
import com.successmetricsinc.query.QueryResultSet;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.WekaUtil;

/**
 * Class to contain a Success Model with instantiated data, segments and models
 * 
 * @author bpeters
 * 
 */
public class SuccessModelInstance implements Externalizable
{
	private static Logger logger = Logger.getLogger(SuccessModelInstance.class);
	private static final long serialVersionUID = 1L;
	protected static final int TARGETINDEX = 0;
	protected static final int FIRSTDATAINDEX = 1;
	private static final int RESULTSET_ESTIMATE_LIST = 400000; // slightly larger than FMR partition and bigger than
																// RBC full dataset
	private static final int RESULTSET_ESTIMATE_MAP = (int) (RESULTSET_ESTIMATE_LIST / 0.75); // slightly larger than
																								// FMR partition and
																								// bigger than RBC full
																								// dataset (load factor
																								// for hashmaps is 0.75)
	private static final int SPINNING_LIMIT = 10000; // number of times to loop through without finding a good row in
														// the sampling (per sample iteration)
	private static final int LIMIT_NUM_ROWS = 0; // used for debugging, makes model data set query return only this
													// many rows
	private static final int PARTITION_LIMIT = 0; // used for debugging, makes model data set query only process this
													// many partitions
	protected Repository r;
	protected SuccessModel sm;
	protected String instanceDir;
	protected String instanceFile;
	protected String dataSetFile;
	protected int curList;
	protected int curPattern;
	protected boolean buildPatternClusters = false;
	protected ModelDataSet mds;
	protected Map<Object, Segment> segmentMap;
	private List<BreakoutAttribute> baList;
	private int numAggregates;
	private final static String DEFAULT_PARTITION = "default partition";
	private final static double ZERO = 0.0;
	private boolean failure = false;
	private String runtimeFilter;

	public String getRuntimeFilter()
	{
		return runtimeFilter;
	}

	public void setRuntimeFilter(String runtimeFilter)
	{
		this.runtimeFilter = runtimeFilter;
	}

	public SuccessModelInstance(Repository r, String modelName, String instanceDir, boolean reset) throws Exception
	{
		this.r = r;
		if (r == null)
			throw (new Exception("No repository specified"));
		this.sm = r.findSuccessModel(modelName);
		if (this.sm == null)
		{
			throw (new Exception("Unable to locate success model: " + modelName));
		}
		this.instanceDir = instanceDir;
		this.instanceFile = sm.Name + ".mod";
		this.dataSetFile = sm.Name + ".dat";
		this.numAggregates = 0;
		this.segmentMap = new HashMap<Object, Segment>();
		if (reset)
			deleteFiles();
	}

	public SuccessModelInstance(Repository r, SuccessModel model, String instanceDir, boolean reset) throws Exception
	{
		this.r = r;
		if (r == null)
			throw (new Exception("No repository specified"));
		this.sm = model;
		this.instanceDir = instanceDir;
		this.instanceFile = sm.Name + ".mod";
		this.dataSetFile = sm.Name + ".dat";
		this.numAggregates = 0;
		this.segmentMap = new HashMap<Object, Segment>();
		if (reset)
			deleteFiles();
	}

	public SuccessModelInstance()
	{
	}

	private void deleteFiles()
	{
		try
		{
			File df = new File(getFileName());
			logger.info("deleting file: " + df.getPath());
			df.delete();
		} catch (Exception e)
		{
			logger.debug(e.getMessage());
		}
		try
		{
			File df = new File(getDataSetFileName());
			logger.info("deleting file: " + df.getPath());
			df.delete();
		} catch (Exception e)
		{
			logger.debug(e.getMessage());
		}
	}

	public void setRepository(Repository r)
	{
		this.r = r;
	}

	@SuppressWarnings("unchecked")
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException
	{
		sm = (SuccessModel) in.readObject();
		instanceDir = (String) in.readObject();
		instanceFile = (String) in.readObject();
		dataSetFile = (String) in.readObject();
		segmentMap = (HashMap<Object, Segment>) in.readObject();
	}

	public void writeExternal(ObjectOutput out) throws IOException
	{
		out.writeObject(sm);
		out.writeObject(instanceDir);
		out.writeObject(instanceFile);
		out.writeObject(dataSetFile);
		out.writeObject(segmentMap);
	}

	public void saveMDS() throws IOException
	{
		// Create file for saving data set
		logger.info("Writing out file " + getDataSetFileName());
		// Serialize results
		mds.save(getDataSetFileName());
	}

	public void retrieveMDS() throws IOException, ClassNotFoundException
	{
		logger.info("Reading file " + getDataSetFileName());
		this.mds = new ModelDataSet(getDataSetFileName());
	}

	private synchronized List<Integer> getNextPatternList()
	{
		if (patternLists.size() == curList)
			return (null);
		return (patternLists.get(curList++));
	}

	private synchronized int getNextPatternIndex()
	{
		if (curPattern >= sm.Patterns.length)
			return (-1);
		return (curPattern++);
	}

	public String getFileDir()
	{
		return (instanceDir);
	}

	public String getFileName()
	{
		return (instanceDir + File.separator + instanceFile);
	}

	public String getDataSetFileName()
	{
		return (instanceDir + File.separator + dataSetFile);
	}

	/*
	 * Compares whether the target index of a rows is greater or less than another. Allows a rowset to be sorted
	 */
	@SuppressWarnings({ "unused", "rawtypes" })
	private static class CompareRows implements Comparator
	{
		public int compare(Object o1, Object o2)
		{
			ArrayList row1 = (ArrayList) o1;
			ArrayList row2 = (ArrayList) o2;
			double val1 = ((Double) row1.get(TARGETINDEX)).doubleValue();
			double val2 = ((Double) row2.get(TARGETINDEX)).doubleValue();
			if (val1 < val2)
				return -1;
			else if (val1 == val2)
				return 0;
			else
				return 1;
		}
	}

	private static class CategoryData
	{
		public String key;
		public String attribute1;
		public String attribute2;
		public int count = 1;
		public double value = 0;
		public boolean nullCategory;
	}

	/**
	 * get pattern list for the success model - also used for determining the number of aggregates
	 */
	private List<List<Integer>> getPatternLists()
	{
		// determine if sm.GroupMeasureOnly should be set
		boolean GroupMeasureOnlyShouldBeSet = true;
		for (int i = 0; i < sm.Patterns.length; i++)
		{
			if (sm.Patterns[i].Type != Pattern.MEASUREONLY)
			{
				GroupMeasureOnlyShouldBeSet = false;
				break;
			}
		}
		if (GroupMeasureOnlyShouldBeSet)
		{
			logger.warn("Setting GroupMeasureOnly to true for Model Data Set " + sm.Name + ", no non-measure patterns");
			sm.GroupMeasureOnly = true;
		}
		patternLists = new ArrayList<List<Integer>>();
		for (int i = 0; i < sm.Patterns.length; i++)
		{
			if (sm.Patterns[i].Type != Pattern.MEASUREONLY || (!sm.GroupMeasureOnly))
			{
				// Make sure that two attribute patterns have different attributes
				if ((sm.Patterns[i].Type != Pattern.TWOATTRIBUTE) || !sm.Patterns[i].Dimension1.equals(sm.Patterns[i].Dimension2)
						|| !sm.Patterns[i].Attribute1.equals(sm.Patterns[i].Attribute2))
				{
					boolean found = false;
					for (List<Integer> l : patternLists)
					{
						if (sm.Patterns[l.get(0)].sameAttributes(sm.Patterns[i]))
						{
							l.add(i);
							found = true;
							break;
						}
					}
					if (!found)
					{
						List<Integer> newlist = new ArrayList<Integer>();
						newlist.add(i);
						patternLists.add(newlist);
					}
				}
			}
		}
		return patternLists;
	}

	/**
	 * create the name of the aggregate
	 * 
	 * @param model
	 *            success model
	 * @param index
	 *            index of the aggregate
	 * @return constructed name for the aggregate
	 */
	private String createAggregateName(SuccessModel model, int index)
	{
		return model.Name + "_" + index;
	}

	/**
	 * Fill the success model instance
	 * 
	 * @param createAggregates
	 *            Whether to fill or create an aggregate
	 * @param persist
	 *            Whether to persist the aggregate
	 * @return
	 * @throws Exception
	 */
	public ModelDataSet fill(boolean createAggregates, boolean persist, boolean persistsubqueries) throws Exception
	{
		if (createAggregates)
			logger.info("Instantiating aggregate meta data for " + sm.Name);
		if (persist)
			logger.info("Creating aggregates in the database for " + sm.Name);
		if (persistsubqueries)
			logger.info("Persisting subqueries " + sm.Name);
		DatabaseConnection dc = r.getDefaultConnection();
		int numThreads = dc.MaxConnectionThreads;
		// Workaround for a threading issue where a list is updated in one thread while being iterated on in another
		if (createAggregates)
			numThreads = 1;
		// validate that the necessary aggregates are in place
		// - search the inheritance hierarchy for models with aggregates, create in reverse order
		if (!createAggregates)
		{
			Stack<SuccessModel> st = new Stack<SuccessModel>();
			// find parents with aggregates
			String parent = sm.InheritFromModel;
			while (parent != null && parent.length() > 0)
			{
				SuccessModel smparent = r.findSuccessModel(parent);
				if (smparent == null)
					break;
				if (smparent.hasAggreates())
				 	st.push(smparent);
				parent = smparent.InheritFromModel;
			}
			// create the aggregates
			String path = r.createApplicationPath();
			DatabaseConnection conn = r.getDefaultConnection();
			SuccessModel smParent = (st.size() == 0) ? null : st.pop();
			while (smParent != null)
			{
				SuccessModelInstance parentsmi = new SuccessModelInstance(r, smParent, path, false);
				// find the query aggregate(s) - loop through all, if some are not instantiated, do them all
				// - safe for persist, not yet sure for 'create' - naming is not deterministic
				// 0 is from getDimensionalAttributes
				// 1 through N is from FillThread over patternLists
				List<List<Integer>> patternLists = parentsmi.getPatternLists();
				boolean create = false;
				boolean persistDB = false;
				for (int i = 0; i < patternLists.size() + 1; i++)
				{
					String aggName = createAggregateName(smParent, i);
					QueryAggregate qa = r.findQueryAggregate(aggName);
					if (qa == null)
					{
						// has not been instantiated, needs to be
						create = true;
					}
					// build the default name, but see if the QueryAggregate built a different name
					String physicalName = Util.replaceWithPhysicalString(aggName);
					if (qa != null)
						physicalName = qa.getName();
					if (!Database.tableExists(r, conn, physicalName, false))
					{
						// has not been created in the DB, needs to be
						create = true;
						persistDB = true;
					}
					if (create && persistDB)
					{
						break;
					}
				}
				if (create || persistDB)
				{
					parentsmi.fill(create, persistDB, persistsubqueries); // create the aggregates for this model
				}
				smParent = (st.size() == 0) ? null : st.pop();
			}
		}
		// Create directories as necessary
		if (!createAggregates)
		{
			File dir = new File(instanceDir);
			if (!dir.exists())
				dir.mkdir();
		}
		// initialize a few things - segment rowlists, file sizes
		if (!createAggregates)
		{
			for (Entry<Object, Segment> entry : segmentMap.entrySet())
			{
				Segment seg = entry.getValue();
				if (seg != null)
				{
					seg.clearRowIndexList();
				}
			}
		}
		// First, get attribute weights if necessary
		logger.info("Getting Attribute Weights for " + sm.Name);
		/*
		 * Get all the dimensional attributes and measures that aren't being broken out
		 */
		patternLists = getPatternLists();

		// add the runtime filter
		List<String> flist = new ArrayList<String>();
		for(String s: sm.Filters)
		{
			flist.add(s);
		}
		if (runtimeFilter != null)
		{
			if (!flist.contains(runtimeFilter))
				flist.add(runtimeFilter);
		}
		sm.TempFilters = new String[flist.size()];  // new filter list, reset on each run
		flist.toArray(sm.TempFilters);			

		mds = getDimensionalAttributes(numThreads, dc, createAggregates, persist, persistsubqueries);
		// Spin threads to get the rest of the result sets
		Thread[] tlist = new Thread[numThreads];
		curList = 0;
		logger.info("Filling Pattern Data for " + sm.Name);
		/*
		 * Group patterns into those that are being sliced by the same dimensional attributes (i.e. can be run in the
		 * same query)
		 */
		if (patternLists != null)
		{
			logger.debug("# of Pattern Lists: " + patternLists.size());
		}
		baList = Collections.synchronizedList(new ArrayList<BreakoutAttribute>());
		for (int i = 0; i < numThreads; i++)
		{
			tlist[i] = new FillThread(dc, mds, createAggregates, persist, persistsubqueries);
			tlist[i].start();
		}
		for (int i = 0; i < numThreads; i++)
		{
			tlist[i].join();
		}
		if (failure)
		{
			throw new Exception("Pattern fill thread failed, please check the log for more information");
		}
		if (createAggregates)
		{
			return (null);
		}
		/*
		 * Add the break-out weightings to the data set
		 */
		mds.setBreakoutAttributes(baList);
		baList = null;
		// Calculate Proportions
		calculateProportions();
		for (Segment s : segmentMap.values())
		{
			s.setColumnHeaders(mds.getHeaders());
		}
		memStats("finished generating MDS");
		return (mds);
	}
	private List<List<Integer>> patternLists = null;

	private synchronized int getNumAggregate()
	{
		return (numAggregates++);
	}

	private ModelDataSet getDimensionalAttributes(int threads, DatabaseConnection dc, boolean createAggregates, boolean persist, boolean persistsubqueries)
			throws Exception
	{
		// Get the target entities
		logger.info("Getting Target Dimension Data for " + sm.Name);
		List<Header> headers = new ArrayList<Header>();
		List<Header> tempHeaders = new ArrayList<Header>();
		Query q = new Query(r);
		// First column is target key
		q.addDimensionColumn(sm.TargetDimension, sm.TargetLevel);
		q.addGroupBy(new GroupBy(sm.TargetDimension, sm.TargetLevel));
		// Next column is target key
		if ((sm.IterateLevel != null) && (!sm.IterateDimension.equals("None")) && (sm.IterateLevel.length() > 0))
		{
			q.addDimensionColumn(sm.IterateDimension, sm.IterateLevel);
			q.addGroupBy(new GroupBy(sm.IterateDimension, sm.IterateLevel));
		} else
		{
			q.addConstantColumn("'IT'", "IT");
		}
		/*
		 * Next column is segment - use break attribute if specified, otherwise, leave blank column for later filling in
		 */
		if (sm.BreakModel == 2)
		{
			q.addDimensionColumn(sm.TargetDimension, sm.BreakAttribute);
			q.addGroupBy(new GroupBy(sm.TargetDimension, sm.BreakAttribute));
		} else
		{
			q.addConstantColumn("'All'", "All");
		}
		// Next column is target measure
		if (sm.Measure != null && sm.Measure.length() > 0)
		{
			q.addMeasureColumn(sm.Measure, sm.Measure, false);
		} else
		{
			q.addConstantColumn("0", "Target");
		}
		tempHeaders.add(new Header(HeaderType.Target, sm.Measure));
		// Add the attributes
		StringBuilder sb = new StringBuilder("Columns that will be mapped to 0.0 if null: ");
		for (int i = 0; i < sm.Attributes.length; i++)
		{
			DimensionColumnNav dcn = q.addDimensionColumn(sm.TargetDimension, sm.Attributes[i]);
			if (dcn == null)
				throw (new Exception("Unable to process success model " + sm.Name + ": Attribute " + sm.Attributes[i] + " not found"));
			q.addGroupBy(new GroupBy(sm.TargetDimension, sm.Attributes[i]));
			Header h = new Header(HeaderType.Attribute, sm.Attributes[i]);
			h.dc = r.findDimensionColumn(sm.TargetDimension, sm.Attributes[i]);
			// set null mapping
			List<MeasureColumn> mclist = r.findMeasureColumns(sm.Attributes[i]);
			if (mclist != null && mclist.size() > 0)
			{
				MeasureColumn mc = mclist.get(0);
				if (mc.mapNullToZeroBasedUponAggregationRule())
				{
					sb.append(mc.ColumnName);
					sb.append(", ");
				}
				h.setMapNullToZero(mc.mapNullToZeroBasedUponAggregationRule());
			}
			tempHeaders.add(h);
		}
		int colCount = sm.Attributes.length;
		boolean foundMeasure = false;
		if (sm.GroupMeasureOnly)
		{
			// Make sure it's an outer join
			q.setFullOuterJoin(true);
			// Add measure-only patterns
			for (int i = 0; i < sm.Patterns.length; i++)
			{
				Pattern p = sm.Patterns[i];
				if (p.Type == Pattern.MEASUREONLY)
				{
					q.addMeasureColumn(p.Measure, p.Measure, false);
					Header h = new Header(HeaderType.Pattern, p.Measure, i);
					h.setType(Types.DOUBLE);
					h.NullCategory = false;
					// set null mapping
					List<MeasureColumn> mclist = r.findMeasureColumns(p.Measure);
					if (mclist != null && mclist.size() > 0)
					{
						MeasureColumn mc = mclist.get(0);
						if (mc.mapNullToZeroBasedUponAggregationRule())
						{
							sb.append(mc.ColumnName);
							sb.append(", ");
						}
						h.setMapNullToZero(mc.mapNullToZeroBasedUponAggregationRule());
						tempHeaders.add(h);
						p.NumCategories = 1;
						foundMeasure = true;
						colCount++;
					} else
					{
						logger.error("Bad measure (" + p.Measure + ") in pattern, quitting");
						throw new Exception("Bad measure (" + p.Measure + ") in pattern, quitting");
					}
				}
			}
		}
		logger.info(sb.toString());
		/*
		 * Add filters if measure exists
		 */
		for (String f : sm.TempFilters)
		{
			QueryFilter qf = r.findFilter(f);
			if (qf == null)
			{
				throw new Exception("Filter " + f + " does not exist, ignoring");
			}
			if ((sm.Measure != null && sm.Measure.length() > 0) || qf.containsOnlyDimension(sm.TargetDimension) || (sm.GroupMeasureOnly && foundMeasure))
				q.addFilter(qf);
		}
		/*
		 * If using partitions, first figure out the partitions
		 */
		List<Object> partitions = new ArrayList<Object>();
		if (!createAggregates)
		{
			if (sm.isPartitioned())
			{
				Query pq = new Query(r);
				pq.addDimensionColumn(sm.TargetDimension, sm.PartitionAttribute);
				pq.addGroupBy(new GroupBy(sm.TargetDimension, sm.PartitionAttribute));
				pq.generateQuery(false);
				QueryResultSet qrs = new QueryResultSet(pq, 1000, 0, null);
				Object[][] rows = qrs.getRows();
				for (Object[] row : rows)
				{
					if (row[0] == null)
					{
						logger.error("Found a NULL value for a partition column when enumerating the possible values, this is probably a fatal error");
					}
					else
					{
						partitions.add(row[0]);	
					}
					if (PARTITION_LIMIT > 0 && (partitions.size() >= PARTITION_LIMIT))
						break;
				}
			} else
			{
				partitions.add(DEFAULT_PARTITION);
			}
			logger.info("Using " + partitions.size() + " partition(s)");
		}
		/*
		 * If just creating an aggregate, do that
		 */
		if (createAggregates)
		{
			if (sm.isPartitioned())
			{
				q.addDimensionColumn(sm.TargetDimension, sm.PartitionAttribute);
				q.addGroupBy(new GroupBy(sm.TargetDimension, sm.PartitionAttribute));
			}
			QueryAggregate qa = new QueryAggregate(q, createAggregateName(this.sm, getNumAggregate()), null, !persistsubqueries, true, dc, null, null, null);
			r.addQueryAggregate(qa);
			if (persist)
			{
				// XXX warning: hack! we don't want to DB partition the modeling aggregates or tables
				boolean partFacts = r.getTimeDefinition().PartitionFacts;
				r.getTimeDefinition().PartitionFacts = false;
				boolean success = qa.persist(null, false, false, null);
				r.getTimeDefinition().PartitionFacts = partFacts;
				if (!success)
				{
					logger.error("Did not create aggregate " + qa.getName());
				}
				// If partitioned, create an index on the partition key
				if (sm.isPartitioned() && success)
				{
					DatabaseConnection conn = r.getDefaultConnection();
					LevelKey lk = Database.buildLevelKey(sm.TargetDimension, sm.PartitionAttribute);
					success = Database.createIndex(conn, lk, qa.getName(), false, true, null, null, true);
					if (!success)
					{
						logger.info("Did not create index on partition key for aggregate " + qa.getName());
					}
				}
			}
			return null;
		}
		ModelDataSet ds = null;
		List<List<CategoryData>> valueList = new ArrayList<List<CategoryData>>(colCount);
		List<Map<Object, CategoryData>> valueMaps = new ArrayList<Map<Object, CategoryData>>(sm.Attributes.length);
		for (int i = 0; i < sm.Attributes.length; i++)
		{
			valueMaps.add(new HashMap<Object, CategoryData>());
		}
		List<Object> targetKeys = new ArrayList<Object>(RESULTSET_ESTIMATE_LIST);
		List<Object> iterateKeys = new ArrayList<Object>(RESULTSET_ESTIMATE_LIST);
		List<Object> segmentKeys = new ArrayList<Object>(RESULTSET_ESTIMATE_LIST);
		List<double[]> tempRows = new ArrayList<double[]>(RESULTSET_ESTIMATE_LIST);
		Set<String> keySet = new HashSet<String>(RESULTSET_ESTIMATE_MAP);
		ConnectionPool cp = dc.ConnectionPool;
		Connection conn = cp.getConnection();
		if (conn == null)
			throw (new Exception("No connection available to retrieve model data"));
		Statement stmt = conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
		stmt.setFetchDirection(ResultSet.FETCH_FORWARD);
		int totalCount = 0; // total number processed in the result set
		int totalIn = 0; // total number put in the model data set
		int dupKey = 0; // number of duplicate target+iteration values
		int columnSize = 0; // number of columns in the dimensional dataset
		
		for (Object partition : partitions)
		{
			Query pq = (Query) q.clone();
			if (LIMIT_NUM_ROWS > 0)
				pq.addTopN(LIMIT_NUM_ROWS);
			if (partition != null && !isDefaultPartition(partition))
			{
				pq.addFilter(new QueryFilter(sm.TargetDimension, sm.PartitionAttribute, "=", partition.toString()));
			}
			pq.replaceVariables(null);
			// handle IB/MySQL (no support for FULL OUTER JOIN)
			if (pq.isFullOuterJoin() && !dc.supportsFullOuterJoin())
			{
				pq.setSimulateOuterJoinUsingPersistedDQTs(true);
				pq.setExpandDerivedQueryTables(false);
				pq.setDerivedQueryTableSuffix("_" + Long.valueOf(System.currentTimeMillis()).toString());
			}
			pq.generateQuery(false);
			if (pq.isFullOuterJoin() && !dc.supportsFullOuterJoin())
			{
				pq.persistDerivedQueryTables(dc);
			}
			// Get result set
			logger.debug(pq.getPrettyQuery());
			ResultSet rs = stmt.executeQuery(pq.getQuery());
			logger.info("Query Finished");
			/*
			 * First generate a temp result set to convert string/nominal attributes to binary ones
			 */
			if (ds == null)
			{
				ResultSetMetaData rsmd = rs.getMetaData();
				for (int i = 0; i < sm.Attributes.length; i++)
				{
					tempHeaders.get(FIRSTDATAINDEX + i).setType(rsmd.getColumnType(i + 5));
				}
			}
			int cnt = 0; // number of items added to the result set for this partition
			int cnt2 = 0; // number of items read from the result set
			int nullMapped = 0; // number of null values mapped to 0.0
			int nullNotMapped = 0; // number of null values left as null (NaN)
			while (rs.next())
			{
				totalCount++;
				if ((++cnt2 % 50000) == 0)
				{
					logger.trace("Retrieved row: " + cnt2);
				}
				Object targetkeyObj = rs.getString(1);
				if (targetkeyObj == null)
				{
					logger.error("Encountered null targetkey");
					continue;
				}
				String iteratekeyObj = rs.getString(2);
				if (iteratekeyObj == null)
				{
					logger.error("Encountered null iteratekey");
					continue;
				}
				// Get list of row data elements associated with this key
				String key = targetkeyObj.toString() + "_" + iteratekeyObj;
				if (!keySet.add(key))
				{
					dupKey++;
					continue;
				}
				targetKeys.add(targetkeyObj);
				iterateKeys.add(iteratekeyObj.intern());
				segmentKeys.add(rs.getString(3).intern());
				double[] row = new double[colCount + FIRSTDATAINDEX];
				row[0] = rs.getDouble(4);
				if (rs.wasNull())
					row[0] = Double.NaN;
				for (int i = 0; i < colCount; i++)
				{
					int rowIndex = i + FIRSTDATAINDEX;
					Header h = tempHeaders.get(rowIndex);
					if (!h.Number)
					{
						String val = rs.getString(i + 5);
						boolean isnull = (val == null);
						val = isnull ? "null" : val.trim();
						row[rowIndex] = (double) saveString(val);
						if (ds == null)
						{
							Map<Object, CategoryData> hm = valueMaps.get(i);
							CategoryData cd = (CategoryData) hm.get(val);
							if (cd == null)
							{
								cd = new CategoryData();
								cd.key = val;
								cd.nullCategory = isnull;
								hm.put(val, cd);
							} else
							{
								cd.count++;
								cd.nullCategory = cd.nullCategory || isnull;
							}
						}
					} else
					{
						double val = rs.getDouble(i + 5);
						if (rs.wasNull())
						{
							if (r.getModelParameters().MapNullsToZero && h.getMapNullToZero())
							{
								row[rowIndex] = ZERO;
								nullMapped++;
							} else
							{
								row[rowIndex] = Double.NaN;
								nullNotMapped++;
							}
						} else
						{
							row[rowIndex] = val;
						}
					}
				}
				totalIn++;
				tempRows.add(row);
				if ((++cnt % 50000) == 0)
				{
					logger.trace("Processed row: " + cnt);
				}
			}
			logger.info("Processed rows: " + cnt);
			logger.debug("Mapped nulls to 0.0: " + nullMapped + ", left nulls alone: " + nullNotMapped);
			rs.close();
			rs = null;
			if (pq.isFullOuterJoin() && !dc.supportsFullOuterJoin())
			{
				pq.dropDerivedQueryTables(dc);
			}
			keySet.clear();
			memStats("rows processed");
			// calculate the header information using the first partition
			if (ds == null)
			{
				if (logger.isDebugEnabled())
				{
					for (int i = 0; i < valueMaps.size(); i++)
					{
						Map<Object, CategoryData> hm = valueMaps.get(i);
						logger.debug("Column: " + i);
						for (Entry<Object, CategoryData> es : hm.entrySet())
						{
							CategoryData cd = es.getValue();
							logger.debug("Value: " + cd.key + " (" + cd.count + ")");
						}
					}
					logger.debug("String table size: " + StringToInt.size() + " (" + IntToString.size() + ", " + stringCount + ")");
				}
				// Create actual headers
				headers.add(tempHeaders.get(TARGETINDEX));
				for (int i = 0; i < colCount; i++)
				{
					Header h = (Header) tempHeaders.get(i + FIRSTDATAINDEX);
					valueList.add(null);
					if (h.Number)
						headers.add(h);
					else
					{
						Map<Object, CategoryData> hm = valueMaps.get(i);
						List<CategoryData> list = new ArrayList<CategoryData>(r.getModelParameters().MaxOneAttributeCategories);
						// First, find top categories by frequency
						TreeMap<Integer, CategoryData> sortMap = new TreeMap<Integer, CategoryData>();
						int count = 0;
						for (CategoryData cd : hm.values())
						{
							boolean add = true;
							if (h.dc.IgnoredValues != null && h.dc.IgnoredValues.contains(cd.key))
								add = false;
							if (add)
								sortMap.put(Integer.valueOf(cd.count * 100 + (count++)), cd);
						}
						/*
						 * Take the top by frequency if too many - replace the existing Hashmap with a new one
						 */
						for (int j = 0; (j < r.getModelParameters().MaxOneAttributeCategories) && (sortMap.size() > 0); j++)
						{
							CategoryData cd = sortMap.get(sortMap.lastKey());
							sortMap.remove(sortMap.lastKey());
							Header newh = new Header(h.Type, h.Name + "=" + cd.key);
							newh.NullCategory = cd.nullCategory;
							newh.Number = true;
							headers.add(newh);
							list.add(cd);
						}
						valueList.set(i, list);
					}
				}
				// now calculate the column size once we have figured out how many columns will be in the dimensional
				// dataset
				columnSize = 1;
				for (int j = 0; j < colCount; j++)
				{
					Header hdr = tempHeaders.get(j + FIRSTDATAINDEX);
					if (hdr.Number)
						columnSize++;
					else
						columnSize += valueList.get(j).size();
				}
				ds = new ModelDataSet();
				ds.addHeaders(sm, headers);
				logger.trace("Finished creating the headers");
				memStats("headers created");
			}
			// Create actual rows - converting nominal to binary
			int tempRowsSize = tempRows.size();
			double[][] rows = new double[tempRowsSize][];
			for (int i = 0; i < tempRowsSize; i++)
			{
				double[] newrow = new double[columnSize];
				int count = 0;
				newrow[count++] = tempRows.get(i)[TARGETINDEX];
				for (int j = 0; j < colCount; j++)
				{
					Header h = tempHeaders.get(j + FIRSTDATAINDEX);
					if (h.Number)
						newrow[count++] = tempRows.get(i)[j + FIRSTDATAINDEX];
					else
					{
						double dval = tempRows.get(i)[j + FIRSTDATAINDEX]; // index of the string in the string table
						String val = getString((int) dval);
						for (CategoryData cd : valueList.get(j))
						{
							if (cd.key.equals(val))
							{
								newrow[count++] = 1;
							} else
							{
								newrow[count++] = 0;
							}
						}
					}
				}
				tempRows.set(i, null); // free up the memory as soon as you're done with it
				rows[i] = newrow;
			}
			logger.trace("Finished creating the rows");
			tempRows.clear();
			memStats("final row creation");
			if (rows.length == 0)
			{
				logger.debug("No rows created, skipping partition " + partition);
				continue;
			}
			int oldLength = ds.numRows();
			ds.addPartition(getDataSetFileName(), partition, targetKeys, iterateKeys, segmentKeys, oldLength);
			memStats("added partition");
			int size = segmentKeys.size();
			for (int i = 0; i < size; i++)
			{
				Object key = segmentKeys.get(i);
				Segment s = segmentMap.get(key);
				if (s == null)
				{
					s = new Segment(this);
					s.setKey(key);
					segmentMap.put(key, s);
				}
				s.getRowIndexList().add(i + oldLength);
			}
			ds.addData(partition, 0, rows, rows[0].length, targetKeys, iterateKeys);
			targetKeys.clear();
			iterateKeys.clear();
			segmentKeys.clear();
			for (int i = 0; i < rows.length; i++)
				rows[i] = null;
			rows = null;
			memStats("added data");
		}
		stmt.close();
		valueList.clear();
		valueList = null;
		tempHeaders.clear();
		tempHeaders = null;
		targetKeys = null;
		iterateKeys = null;
		segmentKeys = null;
		memStats("return");
		logger.debug("total instances processed=" + totalCount + ", total in data set=" + totalIn + ", dup key count: " + dupKey);
		return (ds);
	}

	/**
	 * GC and output debugging information about the memory
	 * 
	 * @param label
	 *            prepended to the memory information in the log
	 */
	private void memStats(String label)
	{
		System.gc();
		System.gc();
		if (logger.isDebugEnabled())
		{
			long free  = Runtime.getRuntime().freeMemory()  / 1000000;
			long total = Runtime.getRuntime().totalMemory() / 1000000;
			logger.debug(label + ": used=" + (total - free) + "MB, total=" + total + "MB, free=" + free + "MB");
		}
	}
	/**
	 * routines and data structures for the string table
	 */
	private Map<String, Integer> StringToInt = new HashMap<String, Integer>();
	private Map<Integer, String> IntToString = new HashMap<Integer, String>();
	private int stringCount = 0;

	private int saveString(String val)
	{
		Integer stringNumber = StringToInt.get(val);
		if (stringNumber != null)
			return stringNumber.intValue();
		Integer newNumber = Integer.valueOf(stringCount);
		StringToInt.put(val, newNumber);
		IntToString.put(newNumber, val);
		return stringCount++;
	}

	private String getString(int key)
	{
		return IntToString.get(Integer.valueOf(key));
	}

	/**
	 * Thread fills all of the Patterns in the Success Model
	 * 
	 * @author bpeters
	 * 
	 */
	private class FillThread extends Thread
	{
		private DatabaseConnection dc;
		private Connection conn;
		private ModelDataSet mds;
		private boolean createAggregates;
		private boolean persist;
		private boolean persistsubqueries;

		FillThread(DatabaseConnection dc, ModelDataSet mds, boolean createAggregates, boolean persist, boolean persistsubqueries)
		{
			this.setName("Pattern Fill - " + this.getId());
			this.dc = dc;
			this.mds = mds;
			this.createAggregates = createAggregates;
			this.persist = persist;
			this.persistsubqueries = persistsubqueries;
		}

		private class RowData
		{
			Object targetkey;
			Object iteratekey;
			String category;
			String attribute1;
			String attribute2;
			double[] values;
		}

		public void run()
		{
			List<Integer> curList = null;
			do
			{
				int nullMapped = 0; // number of null values mapped to 0.0
				int nullNotMapped = 0; // number of null values left as null (NaN)
				curList = getNextPatternList();
				if (curList == null)
					break;
				Pattern firstp = sm.Patterns[curList.get(0)];
				try
				{
					// Create the query
					Query q = new Query(r);
					q.addDimensionColumn(sm.TargetDimension, sm.TargetLevel);
					q.addGroupBy(new GroupBy(sm.TargetDimension, sm.TargetLevel));
					// Next column is target key
					if (sm.IterateLevel != null && sm.IterateLevel.length() > 0)
					{
						q.addDimensionColumn(sm.IterateDimension, sm.IterateLevel);
						q.addGroupBy(new GroupBy(sm.IterateDimension, sm.IterateLevel));
					} else
					{
						q.addConstantColumn("'IT'", "IT");
					}
					if (firstp.Type >= Pattern.ONEATTRIBUTE)
					{
						q.addDimensionColumn(firstp.Dimension1, firstp.Attribute1);
						q.addGroupBy(new GroupBy(firstp.Dimension1, firstp.Attribute1));
					}
					if (firstp.Type >= Pattern.TWOATTRIBUTE)
					{
						q.addDimensionColumn(firstp.Dimension2, firstp.Attribute2);
						q.addGroupBy(new GroupBy(firstp.Dimension2, firstp.Attribute2));
					}
					logger.info("Patterns: ");
					// Add measures
					for (Integer i : curList)
					{
						logger.info(sm.Patterns[i].toString() + " (" + i + ")");
						q.addMeasureColumn(sm.Patterns[i].Measure, sm.Patterns[i].Measure, false);
						List<MeasureColumn> mclist = r.findMeasureColumns(sm.Patterns[i].Measure);
						if (mclist == null || mclist.size() == 0)
						{
							failure = true;
							logger.error("Bad measure (" + sm.Patterns[i].Measure + ") in pattern (" + sm.Patterns[i].toString() + "), quitting");
							throw new Exception("Bad measure (" + sm.Patterns[i].Measure + ") in pattern (" + sm.Patterns[i].toString() + "), quitting");
						}
					}
					// Add any attribute weights needed
					List<BreakoutAttribute> bList = new ArrayList<BreakoutAttribute>(curList.size());
					for (Integer i : curList)
					{
						Pattern p = sm.Patterns[i];
						boolean added = false;
						if ((p.Type == Pattern.ONEATTRIBUTE) || (p.Type == Pattern.TWOATTRIBUTE))
						{
							MeasureColumn mc = r.findMeasureColumn(p.Measure);
							if (mc == null)
								throw (new NavigationException("Unable to locate column: " + p.Measure));
							if ((mc.AttributeWeightMeasure != null) && (r.findMeasureColumn(mc.AttributeWeightMeasure) != null)
									&& !mc.AttributeWeightMeasure.equals(mc.ColumnName))
							{
								boolean found = false;
								for (BreakoutAttribute ba : bList)
								{
									if (ba != null)
										if (ba.weightMeasure.equals(mc.AttributeWeightMeasure))
										{
											found = true;
											break;
										}
								}
								if (!found)
								{
									BreakoutAttribute ba = new BreakoutAttribute();
									ba.dimension1 = p.Dimension1;
									ba.attribute1 = p.Attribute1;
									ba.dimension2 = p.Dimension2;
									ba.attribute2 = p.Attribute2;
									ba.weightMeasure = mc.AttributeWeightMeasure;
									ba.keyList = new ArrayList<Object>(RESULTSET_ESTIMATE_LIST);
									ba.iterationList = new ArrayList<Object>(RESULTSET_ESTIMATE_LIST);
									ba.setList = new ArrayList<List<ValueSet>>(RESULTSET_ESTIMATE_LIST);
									bList.add(ba);
									logger.debug("Adding breakout measure column: " + sm.Patterns[i].Measure);
									q.addMeasureColumn(sm.Patterns[i].Measure, sm.Patterns[i].Measure, false);
									added = true;
								}
							}
						}
						if (!added)
							bList.add(null);
					}
					/*
					 * Add any navigable filters
					 */
					for (String fs : sm.TempFilters)
					{
						QueryFilter qf = r.findFilter(fs);
						Query testq = (Query) q.clone();
						testq.addFilter(qf);
						boolean addFilter = true;
						try
						{
							testq.navigateQuery(true, true, null, false);
						} catch (NavigationException ex)
						{
							/*
							 * First, see if you can remove any offending measures in the filter. Since each section is
							 * basically outer joined, the filter will be applied implicitly through get dimensionsal
							 * attributes. Extra rows will be dropped.
							 */
							testq = (Query) q.clone();
							QueryFilter newqf = qf.getPushDownFilter(testq);
							if (newqf != null)
							{
								qf = newqf;
								testq.addFilter(qf);
								addFilter = true;
								try
								{
									testq.navigateQuery(true, true, null, false);
								} catch (NavigationException ex2)
								{
									/*
									 * If it still fails, don't add the filter
									 */
									addFilter = false;
								}
							} else
								addFilter = false;
						}
						if (addFilter)
							q.addFilter(qf);
					}
					if (curList.size() > 1)
						q.setFullOuterJoin(true);
					/*
					 * Create aggregates if appropriate
					 */
					if (createAggregates)
					{
						if (sm.isPartitioned())
						{
							q.addDimensionColumn(sm.TargetDimension, sm.PartitionAttribute);
							q.addGroupBy(new GroupBy(sm.TargetDimension, sm.PartitionAttribute));
						}
						QueryAggregate qa = new QueryAggregate(q, createAggregateName(sm, getNumAggregate()), null, !persistsubqueries, true, dc, null, null, null);
						if (persist)
						{
							// XXX warning: hack! we don't want to DB partition the modeling aggregates or tables
							boolean partFacts = r.getTimeDefinition().PartitionFacts;
							r.getTimeDefinition().PartitionFacts = false;
							boolean success = qa.persist(null, false, false, null);
							r.getTimeDefinition().PartitionFacts = partFacts;
							if (!success)
							{
								logger.error("Did not create aggregate " + qa.getName());
							}
							// If partitioned, create an index on the partition key
							if (sm.isPartitioned() && success)
							{
								LevelKey lk = Database.buildLevelKey(sm.TargetDimension, sm.PartitionAttribute);
								success = Database.createIndex(dc, lk, qa.getName(), false, true, null, null, true);
								if (!success)
								{
									logger.info("Did not create index on partition key for aggregate " + qa.getName());
								}
							}
						}
					} else
					{
						List<Object> targetKeys = new ArrayList<Object>(RESULTSET_ESTIMATE_LIST);
						List<Object> iterateKeys = new ArrayList<Object>(RESULTSET_ESTIMATE_LIST);
						List<Object> partitionKeys = mds.getPartitionKeys();
						List<List<String>> categoryLists = new ArrayList<List<String>>();
						int[] startCols = new int[curList.size()];
						conn = dc.ConnectionPool.getConnection();
						Statement stmt = conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
						stmt.setFetchDirection(ResultSet.FETCH_FORWARD);
						boolean badPattern = false;
						for (int partitionIndex = 0; partitionIndex < partitionKeys.size(); partitionIndex++)
						{
							Query pq = (Query) q.clone();
							if (LIMIT_NUM_ROWS > 0)
								pq.addTopN(LIMIT_NUM_ROWS);
							Object partition = partitionKeys.get(partitionIndex);
							if (partition != null && !isDefaultPartition(partition))
							{
								pq.addFilter(new QueryFilter(sm.TargetDimension, sm.PartitionAttribute, "=", partition.toString()));
							}
							pq.replaceVariables(null);
							pq.generateQuery(false);
							logger.debug(pq.getPrettyQuery());
							/*
							 * Go through the result set. Since the outer grouping is by the target level, can assume
							 * this is in order. Add a new row for each instance of the target level
							 */
							ResultSet rs = stmt.executeQuery(pq.getQuery());
							logger.trace("finished query");
							int sz = curList.size();
							Map<String, Integer> keys = new HashMap<String, Integer>(RESULTSET_ESTIMATE_MAP);
							List<List<RowData>> rows = new ArrayList<List<RowData>>(RESULTSET_ESTIMATE_LIST);
							List<Map<String, CategoryData>> categoryMap = new ArrayList<Map<String, CategoryData>>(sz);
							for (int i = 0; i < sz; i++)
								categoryMap.add(new HashMap<String, CategoryData>(20));
							// grab some stuff we'll need in the inner loops
							DimensionColumn dcA = r.findDimensionColumn(firstp.Dimension1, firstp.Attribute1);
							if (dcA == null || dcA.DataType == null)
							{
								logger.fatal("Pattern dimension column " + firstp.Dimension1 + ":" + firstp.Attribute1 + " does not exist");
							}
							DecimalFormat dfA = new DecimalFormat();
							boolean numberAP = dcA.DataType.equals("Number") || dcA.DataType.equals("Integer");
							if (numberAP)
							{
								if (dcA.Format != null)
									dfA = new DecimalFormat(dcA.Format);
								else
									logger.warn("No format for attribute " + firstp.Dimension1 + ":" + firstp.Attribute1 + ", using default");
							}
							DimensionColumn dcB = null;
							boolean numberBP = false;
							DecimalFormat dfB = new DecimalFormat();
							if (firstp.Type >= Pattern.TWOATTRIBUTE)
							{
								dcB = r.findDimensionColumn(firstp.Dimension2, firstp.Attribute2);
								if (dcB == null || dcB.DataType == null)
								{
									logger.fatal("Pattern dimension column " + firstp.Dimension2 + ":" + firstp.Attribute2 + " does not exist");
								}
								numberBP = dcB.DataType.equals("Number") || dcB.DataType.equals("Integer");
								if (numberBP)
								{
									if (dcB.Format != null)
										dfB = new DecimalFormat(dcB.Format);
									else
										logger.warn("No format for attribute " + firstp.Dimension2 + ":" + firstp.Attribute2 + ", using default");
								}
							}
							// counts for performance and debugging analysis
							int cnt = 0;
							int cnt2 = 0;
							while (rs.next())
							{
								if (++cnt2 % 50000 == 0)
								{
									logger.trace("read " + cnt2 + " rows");
								}
								Object targetkey = rs.getObject(1);
								String iteratekey = rs.getString(2).intern();
								// Get list of row data elements associated with this key
								String key = targetkey.toString() + "_" + iteratekey;
								List<RowData> row = null;
								Integer rowIndex = keys.get(key);
								if (rowIndex == null)
								{
									row = new ArrayList<RowData>(firstp.Type + 3 + sz);
									rowIndex = rows.size();
									keys.put(key, rowIndex);
									rows.add(row);
									for (BreakoutAttribute ba : bList)
									{
										if (ba != null)
										{
											ba.keyList.add(targetkey);
											ba.iterationList.add(iteratekey);
											ba.setList.add(new ArrayList<ValueSet>(firstp.Type + 3 + sz));
										}
									}
								} else
									row = rows.get(rowIndex);
								RowData rd = new RowData();
								rd.targetkey = targetkey;
								rd.iteratekey = iteratekey;
								String s = null;
								boolean nullCat = false;
								if (firstp.Type >= Pattern.ONEATTRIBUTE)
								{
									if (numberAP)
									{
										double d = rs.getDouble(3);
										if (!rs.wasNull())
										{
											try
											{
												s = dfA.format(d);
											} catch (Exception ex)
											{
												logger.fatal("Unable to format column: " + dcA.ColumnName);
											}
										}
									} else
									{
										s = rs.getString(3);
									}
									rd.category = (s == null) ? "null" : s.trim();
									rd.attribute1 = rd.category;
									nullCat = (s == null);
								}
								s = null;
								if (firstp.Type >= Pattern.TWOATTRIBUTE)
								{
									if (numberBP)
									{
										double d = rs.getDouble(4);
										if (!rs.wasNull())
										{
											try
											{
												s = dfB.format(d);
											} catch (Exception ex)
											{
												logger.fatal("Unable to format column: " + dcB.ColumnName);
											}
										}
									} else
									{
										s = rs.getString(4);
									}
									rd.category = rd.category.replace(':', '-');
									if (s != null)
										s = s.replace(':', '-');
									rd.attribute2 = ((s == null) ? "null" : s.trim());
									rd.category = rd.category + ":" + rd.attribute2;
									if (s == null)
										nullCat = true;
								}
								// Get measure values
								rd.values = new double[sz];
								for (int i = 0; i < sz; i++)
								{
									rd.values[i] = rs.getDouble(3 + firstp.Type + i);
									if (rs.wasNull())
									{
										int index = curList.get(i);
										String measureName = sm.Patterns[index].Measure;
										MeasureColumn mc = r.findMeasureColumn(measureName);
										if (r.getModelParameters().MapNullsToZero && (mc != null) && mc.mapNullToZeroBasedUponAggregationRule())
										{
											rd.values[i] = ZERO;
											nullMapped++;
										} else
										{
											rd.values[i] = Double.NaN;
											nullNotMapped++;
										}
									}
									/*
									 * Record total value for each category to limit if needed to the top categories
									 */
									if (firstp.Type != Pattern.MEASUREONLY)
									{
										CategoryData cd = categoryMap.get(i).get(rd.category);
										if (cd == null)
										{
											cd = new CategoryData();
											cd.key = rd.category;
											cd.attribute1 = rd.attribute1;
											cd.attribute2 = rd.attribute2;
											cd.nullCategory = nullCat;
											if (!Double.isNaN(rd.values[i]))
												cd.value = rd.values[i];
											categoryMap.get(i).put(cd.key, cd);
										} else
										{
											if (!Double.isNaN(rd.values[i]))
												cd.value += rd.values[i];
											if (nullCat)
												cd.nullCategory = true;
										}
									}
									// Get weight measure values (if needed)
									BreakoutAttribute ba = bList.get(i);
									if (ba != null)
									{
										ValueSet vs = new ValueSet();
										vs.attribute1Value = rd.attribute1;
										if (firstp.Type == Pattern.TWOATTRIBUTE)
										{
											vs.attribute2Value = rd.attribute2;
										}
										vs.value = rd.values[i];
										ba.setList.get(rowIndex).add(vs);
									}
								}
								row.add(rd);
								if (++cnt % 50000 == 0)
								{
									logger.trace("processed " + cnt + " rows");
								}
							}
							rs.close();
							keys.clear();
							logger.info("Processed " + cnt + " rows");
							logger.debug("Mapped nulls to 0.0: " + nullMapped + ", left nulls alone: " + nullNotMapped);
							logger.trace("Finished processing the rows");
							if (cnt == 0)
							{
								logger.warn("Pattern list " + curList.toString() + " did not return any rows for partition " + partition + ", skipping");
								badPattern = true;
								break;
							}
							for (int count = 0; count < sz; count++)
							{
								int index = curList.get(count);
								Pattern p = sm.Patterns[index];
								DimensionColumn dc1 = null;
								DimensionColumn dc2 = null;
								if (p.Type >= Pattern.ONEATTRIBUTE)
								{
									dc1 = r.findDimensionColumn(p.Dimension1, p.Attribute1);
								}
								if (p.Type >= Pattern.TWOATTRIBUTE)
								{
									dc2 = r.findDimensionColumn(p.Dimension2, p.Attribute2);
								}
								// Create headers
								List<String> categories = categoryLists.size() == curList.size() ? categoryLists.get(count) : null;
								/*
								 * Only process header data if this is the first partition being processed
								 */
								if (categories == null)
								{
									List<Header> headers = new ArrayList<Header>();
									categories = new ArrayList<String>();
									categoryLists.add(categories);
									if (firstp.Type == Pattern.MEASUREONLY)
									{
										Header h = new Header(HeaderType.Pattern, sm.Patterns[index].Measure, index);
										h.setType(Types.DOUBLE);
										h.NullCategory = false;
										headers.add(h);
										p.NumCategories = 1;
									} else
									{
										/*
										 * First, find top categories by value (not frequency - unlike dimension
										 * attributes)
										 */
										TreeMap<Double, CategoryData> sortMap = new TreeMap<Double, CategoryData>();
										for (CategoryData cd : categoryMap.get(count).values())
											if (((dc1 == null) || (dc1.IgnoredValues == null) || !dc1.IgnoredValues.contains(cd.attribute1))
											&&  ((dc2 == null) || (dc2.IgnoredValues == null) || !dc2.IgnoredValues.contains(cd.attribute2)))
												sortMap.put(cd.value, cd);
										p.NumCategories = 0;
										if (sortMap.size() == 0)
										{
											logger.warn("Pattern " + p.toString() + " of pattern list " + curList.toString()
													+ " did not have any valid category values, skipping the list (all patterns will have this problem)");
											if (dc1 != null)
												logger.warn(dc1.toString() + ", ignored values: " + dc1.IgnoredValues);
											if (dc2 != null)
												logger.warn(dc2.toString() + ", ignored values: " + dc2.IgnoredValues);
											for (CategoryData cd : categoryMap.get(count).values())
											{
												logger.warn("values from the database: " + cd.key);
											}
											badPattern = true;
											break;
										}
										int maxCategories = p.MaxPatterns;
										for (int j = 0; (j < maxCategories) && (sortMap.size() > 0); j++)
										{
											CategoryData cd = sortMap.get(sortMap.lastKey());
											sortMap.remove(sortMap.lastKey());
											Header h = new Header(HeaderType.Pattern, p.getName() + "=" + cd.key, index);
											h.setAttribute1Value(cd.attribute1);
											h.setAttribute2Value(cd.attribute2);
											h.setType(Types.DOUBLE);
											h.NullCategory = cd.nullCategory;
											headers.add(h);
											categories.add(cd.key);
											p.NumCategories++;
										}
									}
									if (p.NumCategories != 0) // only add if atleast 1 category
										startCols[count] = mds.addHeaders(sm, headers);
								}
								// Create rows
								double[][] newRows = new double[rows.size()][];
								int rowCount = 0;
								for (List<RowData> row : rows)
								{
									int rowSz = row.size();
									double[] newRow = firstp.Type != Pattern.MEASUREONLY ? new double[sm.Patterns[index].NumCategories] : new double[1];
									RowData rd = row.get(0);
									targetKeys.add(rd.targetkey);
									iterateKeys.add(rd.iteratekey);
									if (firstp.Type != Pattern.MEASUREONLY)
									{
										for (int i = 0; i < sm.Patterns[index].NumCategories; i++)
										{
											boolean found = false;
											for (int j = 0; j < rowSz; j++)
											{
												rd = row.get(j);
												if ((rd.category == null) || (categories.get(i) == null))
												{
													found = true;
													newRow[i] = rd.values[count];
													break;
												} else if (rd.category.equals(categories.get(i)))
												{
													found = true;
													newRow[i] = rd.values[count];
													break;
												}
											}
											if (!found)
												newRow[i] = Double.NaN;
										}
									} else
									{
										newRow[0] = rd.values[count];
									}
									newRows[rowCount++] = newRow;
								}
								// Add to the current data set
								mds.addData(partition, startCols[count], newRows, sm.Patterns[index].NumCategories, targetKeys, iterateKeys);
								targetKeys.clear();
								iterateKeys.clear();
								newRows = null;
							}
							if (badPattern)
							{
								logger.warn("The first partition did not return any valid data for this pattern list, skilling remaining partitions: "
										+ curList.toString());
								break;
							}
							for (BreakoutAttribute ba : bList)
							{
								if (ba != null)
								{
									ba.commit();
									baList.add(ba);
								}
							}
						}
						stmt.close();
					}
				} catch (NavigationException ne)
				{
					logger.error("Navigation Exception (" + firstp.toString() + ")", ne);
				} catch (Exception ex)
				{
					logger.error("Error:" + ex.toString(), ex);
				}
			} while (curList != null);
		}
	}

	/**
	 * Convert any patterns that are tagged as proportions
	 */
	private void calculateProportions()
	{
		logger.info("Calculating Pattern Proportions");
		// For designated patterns - calculate the proportion
		boolean anyProportions = false;
		for (int i = 0; i < sm.Patterns.length; i++)
		{
			if (sm.Patterns[i].Proportion)
			{
				anyProportions = true;
				break;
			}
		}
		if (!anyProportions) // if no proportions, skip processing the entire result set
			return;
		logger.info("Calculating Pattern Proportions");
		// For designated patterns - calculate the proportion
		for (int k = 0; k < mds.numRows(); k++)
		{
			for (int i = 0; i < sm.Patterns.length; i++)
			{
				if (sm.Patterns[i].Proportion)
				{
					double total = 0;
					for (int j = 0; j < sm.Patterns[i].NumCategories; j++)
					{
						double val = mds.getCell(k, sm.Patterns[i].Startindex + j);
						if (!Double.isNaN(val))
							total += val;
					}
					for (int j = 0; j < sm.Patterns[i].NumCategories; j++)
					{
						int index = sm.Patterns[i].Startindex + j;
						double val = mds.getCell(k, index);
						if ((total != 0) && !Double.isNaN(val))
						{
							mds.setCell(k, index, (double) (val / total));
						} else
						{
							mds.setCell(k, index, 0.0);
						}
					}
				}
			}
		}
	}

	public void buildClusters(Repository r) throws Exception
	{
		this.r = r;
		logger.info("Generating Pattern Clusters");
		// Generate clusters for success patters
		Thread[] tlist = new Thread[r.getModelParameters().MaxComputationThreads];
		curPattern = 0;
		for (int i = 0; i < r.getModelParameters().MaxComputationThreads; i++)
		{
			tlist[i] = new ClusterPatternThread(mds);
			tlist[i].start();
		}
		for (int i = 0; i < r.getModelParameters().MaxComputationThreads; i++)
		{
			tlist[i].join();
		}
		logger.info("");
	}

	/**
	 * Return the map of segment keys to rows
	 * 
	 * @return
	 */
	public Map<Object, Segment> getSegmentMap()
	{
		return (segmentMap);
	}

	protected Attribute createAttribute(int index)
	{
		if (index < 0)
		{
			return (new Attribute("Empty"));
		}
		Attribute a;
		Header h;
		h = mds.getHeader(index);
		a = new Attribute(h.Name);
		return (a);
	}

	// Commit data set
	public void commit()
	{
		mds.commit();
	}

	public double[] getCentroidMemberships(Centroid[] centroids, double[] values)
	{
		double[] distribution = new double[centroids.length];
		double total = 0;
		int match = -1;
		for (int j = 0; j < centroids.length; j++)
		{
			double dist = 0;
			for (int k = 0; k < centroids[j].Means.length; k++)
			{
				if (Double.isNaN(values[k]))
					values[k] = 0;
				double val = values[k] - centroids[j].Means[k];
				dist += val * val;
			}
			if (dist != 0)
			{
				distribution[j] = 1 / Math.sqrt(dist);
				total += distribution[j];
			} else
			{
				// Note if exact match
				match = j;
			}
		}
		// If exact match, set to 1
		if (match >= 0)
		{
			for (int j = 0; j < centroids.length; j++)
			{
				distribution[j] = (j == match) ? 1 : 0;
			}
		} else
		// Otherwise, normalize the data
		{
			for (int j = 0; j < centroids.length; j++)
			{
				distribution[j] /= total;
			}
		}
		return (distribution);
	}

	// Thread fills all of the Patterns in the Success Model
	private class ClusterPatternThread extends Thread
	{
		private ModelDataSet mds;

		public ClusterPatternThread(ModelDataSet mds)
		{
			this.mds = mds;
		}

		private Instance constructClusterInstance(int rowNum, int start, int length)
		{
			// Create a new instance row
			Instance inst = new Instance(length);
			// Set the values
			for (int j = 0; j < length; j++)
			{
				Double d = mds.getCell(rowNum, start + j);
				if (d != null)
					inst.setValue(j, d.doubleValue());
				else
					inst.setValue(j, 0);
			}
			return (inst);
		}

		private Instances constructClusterInstances(int start, int length)
		{
			FastVector attributes = new FastVector(length);
			// Add all attributes
			for (int i = start; i < start + length; i++)
			{
				attributes.addElement(createAttribute(i));
			}
			Instances dataSet = new Instances("Cluster", attributes, mds.numRows());
			// Setup for sampling of rows
			int sampleSize = (r.getModelParameters().SampleSize < 0) ? mds.numRows() : Math.min(r.getModelParameters().SampleSize, mds.numRows());
			List<Object> rows = new ArrayList<Object>();
			for (int i = 0; i < mds.numRows(); i++)
				rows.add(Integer.valueOf(i));
			Random r = new Random(System.currentTimeMillis());
			for (int i = 0; i < sampleSize; i++)
			{
				// Get the row number of the next sample
				int index = r.nextInt(rows.size());
				int rowNum = ((Integer) rows.get(index)).intValue();
				dataSet.add(constructClusterInstance(rowNum, start, length));
				// Remove the row number from the sampling pool
				rows.remove(index);
			}
			return (dataSet);
		}

		public void run()
		{
			int index = 0;
			do
			{
				Pattern p = null;
				do
				{
					index = getNextPatternIndex();
					if (index < 0)
						break;
					p = sm.Patterns[index];
				} while ((p.Type == Pattern.MEASUREONLY) || (p.NumCategories < 2));
				if (index < 0)
					break;
				logger.info("[" + index + "]");
				// Cluster the data
				Instances dataSet = constructClusterInstances(p.Startindex, p.NumCategories);
				// Use Expectations Maximization to cluster
				EM em = new EM();
				double[][][] info = null;
				try
				{
					em.buildClusterer(dataSet);
					// Record the centroids of each cluster
					info = em.getClusterModelsNumericAtts();
					// Recluster if too many clusters
					if (info.length > r.getModelParameters().MaxClustersPerPattern)
					{
						em.setNumClusters(r.getModelParameters().MaxClustersPerPattern);
						em.buildClusterer(dataSet);
						info = em.getClusterModelsNumericAtts();
					}
					p.ClusterModel = em;
				} catch (Exception e)
				{
					logger.error("Pattern Cluster Exception: " + e.toString());
					break;
				}
				p.Centroids = new Centroid[info.length];
				for (int i = 0; i < info.length; i++)
				{
					Centroid c = new Centroid();
					p.Centroids[i] = c;
					// Probabilities are same for all elements cluster
					c.Probability = info[i][0][2];
					c.Means = new double[p.NumCategories];
					c.StdDeviations = new double[p.NumCategories];
					for (int j = 0; j < info[i].length; j++)
					{
						c.Means[j] = info[i][j][0];
						c.StdDeviations[j] = info[i][j][1];
					}
				}
				// Add an attribute for each centroid
				List<Header> newList = new ArrayList<Header>();
				for (int i = 0; i < p.Centroids.length; i++)
				{
					Header h = null;
					if (p.Type == Pattern.ONEATTRIBUTE)
					{
						h = new Header(HeaderType.Cluster, "P[" + p.Measure + ":" + p.Attribute1 + "]C" + i, index);
					} else
					{
						h = new Header(HeaderType.Cluster, "P" + index + "[" + p.Measure + ":" + p.Attribute1 + ":" + p.Attribute2 + "]C" + i, index);
					}
					h.setType(Types.DOUBLE);
					newList.add(h);
				}
				int startCol = 0;
				try
				{
					startCol = mds.addHeaders(sm, newList);
				} catch (Exception ex)
				{
					return;
				}
				// Create probability for each centroid for each row
				for (int i = 0; i < mds.numRows(); i++)
				{
					double[] distribution = null;
					try
					{
						/*
						 * If assigning clusters based on probability density - use EM model to calculate probability
						 */
						if (r.getModelParameters().ClusterDensity)
						{
							Instance inst = new Instance(p.NumCategories);
							distribution = em.distributionForInstance(inst);
						} else
						{
							// Remove em model - no need in this case
							p.ClusterModel = null;
							/*
							 * Otherwise, calculate the inverse of distance to cluster to calculate how close an
							 * instance is to each cluster
							 */
							double[] values = new double[p.NumCategories];
							Instance inst = constructClusterInstance(i, p.Startindex, p.NumCategories);
							for (int j = 0; j < p.NumCategories; j++)
								values[j] = inst.value(j);
							distribution = getCentroidMemberships(p.Centroids, values);
						}
					} catch (Exception e)
					{
						logger.error("Instance Cluster Exception: " + e.toString());
					}
					for (int j = 0; j < p.Centroids.length; j++)
					{
						mds.setCell(i, startCol + j, distribution[j]);
					}
				}
			} while (index >= 0);
		}
	}

	/**
	 * Create a blank datset using a set of selected columns
	 * 
	 * @param name
	 *            Name of dataset
	 * @param sampleSize
	 *            Size of sample (used for allocating instances)
	 * @param columnIndexList
	 *            List of columns to use (may be changed if columns need to be remapped - headers is not null)
	 * @param useClass
	 *            Whether or not to use a class
	 * @param measureName
	 *            Name of class measure
	 * @param headers
	 *            Headers that need to be satisfied (used when a model is created on a previous dataset with perhaps
	 *            some columns missing on the new run)
	 * @return
	 * @throws Exception
	 */
	public Instances createDataSet(String name, int sampleSize, List<Integer> columnIndexList, boolean useClass, String measureName, List<Header> headers)
			throws Exception
	{
		// Remap column indices if needed
		if (headers != null)
		{
			List<Integer> colIndexList = new ArrayList<Integer>(columnIndexList);
			columnIndexList.clear();
			for (int index : colIndexList)
			{
				boolean found = false;
				for (int i = 0; i < mds.numHeaders(); i++)
				{
					if (mds.getHeader(i).equals(headers.get(index)))
					{
						columnIndexList.add(i);
						found = true;
						break;
					}
				}
				if (!found)
				{
					columnIndexList.add(-1);
				}
			}
		}
		FastVector attributes = new FastVector(columnIndexList.size() + 1);
		// if building one for a class attribute, find the index
		int classIndex = 0;
		if (measureName != null)
		{
			classIndex = getIndexForMeasure(measureName);
			if (classIndex < 0)
			{
				throw new Exception("Unable to find measure '" + measureName + "'");
			}
			// Make sure the column index list does not contain the class index
			int indexPos = columnIndexList.indexOf(Integer.valueOf(classIndex));
			if (indexPos > 0)
			{
				columnIndexList.remove(indexPos);
			}
		}
		// Initialize class attribute if necessary
		FastVector classValues = new FastVector();
		if (useClass)
		{
			classValues.addElement("False");
			classValues.addElement("True");
			if (measureName != null)
			{
				attributes.addElement(new Attribute(measureName, classValues));
			} else
			{
				attributes.addElement(new Attribute(sm.Measure, classValues));
			}
		} else
		{
			attributes.addElement(new Attribute(measureName == null ? sm.Measure : measureName));
		}
		// Add all attributes
		for (int index : columnIndexList)
			attributes.addElement(createAttribute(index));
		// Setup for sampling of rows
		Instances dataSet = new Instances(name, attributes, sampleSize);
		return (dataSet);
	}

	/**
	 * Create an individual instance
	 * 
	 * @param rowIndex
	 *            Index of row
	 * @param columnIndexList
	 *            Column indices
	 * @param isClass
	 *            Whether this instance is a class instance or value instance
	 * @param targetIndex
	 *            Index of target measure (whether class or value)
	 * @param returnNonzeroTargetInstances
	 *            Whether to only return an instance if the target is nonzero (if value or in class if class)
	 * @return
	 */
	public Instance getInstance(int rowIndex, List<Integer> columnIndexList, boolean includeTarget, boolean isClass, int targetIndex,
			boolean returnNonzeroTargetInstances)
	{
		Double targetVal = mds.getCell(rowIndex, targetIndex);
		// If appropriate, make sure only class instances are returned (non-zero values)
		if (!returnNonzeroTargetInstances || (targetVal > 0))
		{
			int offset = includeTarget ? 1 : 0;
			int sz = columnIndexList.size();
			double[] row = new double[sz + offset];
			if (includeTarget)
			{
				if (isClass)
				{
					// Set the target value first
					if (Double.isNaN(targetVal) || targetVal == 0)
						row[0] = 0;
					else
						row[0] = 1;
				} else
				{
					// Set the target value first
					row[0] = targetVal;
				}
			}
			for (int j = 0; j < sz; j++)
			{
				int colIndex = columnIndexList.get(j);
				double val = colIndex < 0 ? 0 : (mds.getCell(rowIndex, colIndex));
				// Round to prevent overvlows
				row[j + offset] = (Math.rint(val * 1E5)) / 1E5;
			}
			Instance inst = new Instance(1, row);
			for (int j = 0; j < sz; j++)
			{
				if (columnIndexList.get(j) < 0)
					inst.setMissing(j + offset);
			}
			return (inst);
		}
		return (null);
	}

	/**
	 * Construct a set of instances from the current model data set
	 * 
	 * @param name
	 *            Name to call new dataset
	 * @param rowIndexList
	 *            List of rows to select from
	 * @param colIndexList
	 *            List of valid columns
	 * @param sample
	 *            Use sampling
	 * @param includeTarget
	 *            Whether to include the target in the dataset
	 * @param isClass
	 *            Whether the target is a class measure
	 * @param measureName
	 *            If used, name of measure to use as the target (value or class) assumes default target if null
	 * @param returnOnlyNonzeroTargets
	 *            Return only instances that are members of a class (if class) or are nonzero (if value). Removes
	 *            indices from the row index list if they are not used (so clone it if it should not be changed)
	 * @param headers
	 *            Headers that need to be satisfied (used when a model is created on a previous dataset with perhaps
	 *            some columns missing on the new run)
	 * @param iterations
	 *            List of iteration values to use (ignored if null)
	 * @return
	 * @throws Exception
	 */
	protected Instances constructInstances(String name, List<Integer> rowIndexList, List<Integer> colIndexList, boolean sample, boolean isClass,
			String measureName, boolean returnOnlyNonzeroTargets, List<Header> headers, List<Object> iterations, int start, int length, DisplayFilter filter)
			throws Exception
	{
		logger.debug("constructInstances for attributes (" + colIndexList.size() + "): " + colIndexList.toString());
		List<Integer> columnIndexList = new ArrayList<Integer>(colIndexList);
		int numRows = rowIndexList == null ? mds.numRows() : rowIndexList.size();
		int sampleSize = (!sample || r.getModelParameters().SampleSize < 0) ? numRows : Math.min(r.getModelParameters().SampleSize, numRows);
		sampleSize = Math.min(sampleSize, length);
		Instances dataSet = createDataSet(name, sampleSize, columnIndexList, isClass, measureName, headers);
		if (sampleSize == 0)
			return dataSet;
		Random rn = null;
		BitSet rows = null;
		int sz = 0;
		int classIndex = 0;
		if (measureName != null)
		{
			classIndex = getIndexForMeasure(measureName);
			if (classIndex < 0)
			{
				throw new Exception("Unable to find measure '" + measureName + "'");
			}
		} else
			classIndex = SuccessModelInstance.TARGETINDEX;
		DisplayFilter fltr = null;
		List<Integer> completeColumnList = null;
		if (filter != null)
		{
			fltr = (DisplayFilter) filter.clone();
			completeColumnList = new ArrayList<Integer>();
			for (int i = 0; i < this.mds.numHeaders(); i++)
			{
				if (i != classIndex)
					completeColumnList.add(i);
			}
			logger.debug("complete column list size = " + completeColumnList.size() + ", classIndex is " + classIndex);
			if (!fltr.bindToData(this, completeColumnList, classIndex))
			{
				logger.warn("Could not bind the training filter to the dataset, filter being ignored: " + filter.toString());
				fltr = null;
			} else
			{
				logger.debug(fltr.toString());
			}
		}
		/*
		 * Deal with very large datasets (for now) - should only affect that times that we build the full dataset for
		 * Weka to sample for us. Need to make sure that this only applies if there isn't a start and stop.
		 * 
		 * limit this to the current range, otherwise this could get very expensive - basically the max is
		 * r.ModelParameters.MaxInstances
		 */
		int maxSize = Math.min(r.getModelParameters().MaxInstances, mds.getPartitionSize(0));
		if (start == 0 && (sampleSize > maxSize))
		{
			numRows = mds.getPartitionSize(0);
			logger.debug("Sample size is larger than the smaller of the repository MaxInstances or the size of the first partition, reducing the size");
			logger.debug("Original sample size=" + sampleSize + ", MaxInstances=" + r.getModelParameters().MaxInstances + ", partition size="
					+ mds.getPartitionSize(0));
			length = numRows;
			sampleSize = maxSize;
			if (sampleSize < numRows)
				sample = true;
		}
		if (sample)
		{
			// build a set of all possible rows to make sure we don't include the same value more than once in the
			// sample set
			int end = numRows;
			if (length > 0 && length != Integer.MAX_VALUE)
				end = start + length;
			rows = new BitSet(end - start);
			rows.clear();
			sz = end - start; // rows.size() is rounded to a multiple of 32 (it is a bit set)
			rn = new Random(System.currentTimeMillis());
		}
		List<Integer> rowIndices = new ArrayList<Integer>(sampleSize);
		boolean allIterations = true;
		if (length > 0)
			allIterations = (iterations == null) || iterations.contains("ALL") || iterations.contains("all");
		StringBuilder debugLog = new StringBuilder();
		int filtered = 0;
		boolean noMore = false; // hit the sampling limit for a single partition...
		do
		{
			if (sample)
			{
				int spinning = 0;
				do
				{
					int index = rn.nextInt(sz);
					// verify that it matches the iteration(s)
					if (!rows.get(index))
					{
						int rowIndex = index + start;
						if (allIterations || iterations.contains(mds.getIteratorKey(rowIndex).toString()))
						{
							rowIndices.add(rowIndex);
						} else
						{
							spinning++;
						}
					} else
					{
						spinning++;
					}
					rows.set(index); // mark as looked at
				} while ((rowIndices.size() < sampleSize) && (spinning < SPINNING_LIMIT));
				if (spinning >= SPINNING_LIMIT)
				{
					logger.warn("Could not find a sufficient number of instances that matched the required iterations in the current partition: current size="
							+ rowIndices.size() + ", size requested=" + sampleSize);
					noMore = true;
				}
			}
			if (!sample)
			{
				for (int i = 0; i < sampleSize; i++)
				{
					// verify that it matches the iteration
					int rowIndex = rowIndexList == null ? start + i : rowIndexList.get(start + i);
					boolean valid = allIterations || iterations.contains(mds.getIteratorKey(rowIndex).toString());
					if (!valid)
						continue;
					rowIndices.add(rowIndex);
				}
			}
			Collections.sort(rowIndices); // XXX if we never sample/process across partitions, this is not needed
			int debugCount = 0;
			for (int rowIndex : rowIndices)
			{
				if (fltr != null)
				{
					// validate that the instance matches the training filter, pull the entire instance
					Instance inst = getInstance(rowIndex, completeColumnList, true, isClass, classIndex, returnOnlyNonzeroTargets);
					if (inst == null)
					{
						filtered++;
						continue;
					}
					int numAttributes = inst.numAttributes();
					Object[] row = new Object[numAttributes];
					for (int j = 0; j < numAttributes; j++)
					{
						row[j] = inst.value(j);
					}
					if (!fltr.satisfiesFilter(row, r))
					{
						filtered++; // XXX if sampled, will need to grab some more
						continue;
					}
				}
				Instance inst = getInstance(rowIndex, columnIndexList, true, isClass, classIndex, returnOnlyNonzeroTargets);
				if (inst != null)
				{
					inst.setDataset(dataSet);
					dataSet.add(inst);
					if (logger.isDebugEnabled() && (debugCount++) < r.getModelParameters().MaxARFFInstances)
					{
						debugLog.append(rowIndex);
						debugLog.append(',');
					}
				}
			}
			rowIndices.clear();
		} while (!noMore && sample && dataSet.numInstances() < sampleSize);
		if (fltr != null)
			logger.debug("training filter removed " + filtered + " instances");
		logger.debug("constructInstances: segment = " + name + ", dataSet Size = " + dataSet.numInstances() + ", [" + start + "," + (start + length) + "]");
		if (dataSet.numAttributes() == 0)
			logger.error("Data set size is 0, this is going to cause failures later on in the modeling run; please check your data, iteration values, and training filter");		
		// logger.debug("First " + r.getModelParameters().MaxARFFInstances + " row indices: " + debugLog);
		return (dataSet);
	}

	protected Instances constructInstances(String name, List<Integer> rowIndexList, List<Integer> colIndexList, boolean sample, boolean isClass,
			String classMeasureName, boolean returnOnlyClassInstances, List<Header> headers, List<Object> iterations, int start, int length) throws Exception
	{
		return (constructInstances(name, rowIndexList, colIndexList, sample, isClass, classMeasureName, returnOnlyClassInstances, headers, iterations, start,
				length, null));
	}

	protected Instances constructInstances(String name, List<Integer> rowIndexList, List<Integer> colIndexList, boolean sample, boolean isClass,
			String measureName, boolean returnOnlyClassInstances, List<Header> headers, List<Object> iterations) throws Exception
	{
		return (constructInstances(name, rowIndexList, colIndexList, sample, isClass, measureName, returnOnlyClassInstances, headers, iterations, 0,
				Integer.MAX_VALUE, null));
	}

	protected Instances constructInstances(String name, List<Integer> rowIndexList, List<Integer> colIndexList, boolean sample, boolean isClass,
			String measureName, boolean returnOnlyClassInstances, List<Header> headers, List<Object> iterations, DisplayFilter filter) throws Exception
	{
		return (constructInstances(name, rowIndexList, colIndexList, sample, isClass, measureName, returnOnlyClassInstances, headers, iterations, 0,
				Integer.MAX_VALUE, filter));
	}

	protected Instances getScoringDataSet(String name, List<Integer> rowIndexList, List<Integer> colIndexList, boolean isClass, String measureName,
			List<Header> headers, List<Object> iterations, DisplayFilter filter, int start, int length, List<Integer> actualRowIndexList) throws Exception
	{
		List<Integer> columnIndexList = new ArrayList<Integer>(colIndexList);
		Instances dataSet = createDataSet(name, length, columnIndexList, isClass, measureName, headers);
		if (length == 0)
			return dataSet;
		int classIndex = getIndexForMeasure(measureName);
		;
		if (classIndex < 0)
		{
			throw new Exception("Unable to find measure '" + measureName + "'");
		}
		int filtered = 0;
		DisplayFilter fltr = null;
		List<Integer> completeColumnList = null;
		if (filter != null)
		{
			fltr = (DisplayFilter) filter.clone();
			completeColumnList = new ArrayList<Integer>();
			for (int i = 0; i < this.mds.numHeaders(); i++)
			{
				if (i != classIndex)
					completeColumnList.add(i);
			}
			logger.debug("complete column list size = " + completeColumnList.size() + ", classIndex is " + classIndex);
			if (!fltr.bindToData(this, completeColumnList, classIndex))
			{
				logger.warn("Could not bind the scoring filter to the dataset, filter being ignored: " + filter.toString());
				fltr = null;
			} else
			{
				logger.debug(fltr.toString());
			}
		}
		boolean allIterations = true;
		if (length > 0)
			allIterations = (iterations == null) || iterations.contains("ALL") || iterations.contains("all");
		for (int i = start; i < start + length; i++)
		{
			// verify that it matches the iteration
			int rowIndex = rowIndexList == null ? i : rowIndexList.get(i);
			boolean valid = allIterations || iterations.contains(mds.getIteratorKey(rowIndex).toString());
			if (!valid)
				continue;
			if (fltr != null)
			{
				// validate that the instance matches the training filter, pull the entire instance
				Instance inst = getInstance(rowIndex, completeColumnList, true, isClass, classIndex, false);
				if (inst == null)
				{
					filtered++;
					continue;
				}
				int numAttributes = inst.numAttributes();
				Object[] row = new Object[numAttributes];
				for (int j = 0; j < numAttributes; j++)
				{
					row[j] = inst.value(j);
				}
				if (!fltr.satisfiesFilter(row, r))
				{
					filtered++; // XXX if sampled, will need to grab some more
					continue;
				}
			}
			Instance inst = getInstance(rowIndex, columnIndexList, true, isClass, classIndex, false);
			if (inst != null)
			{
				inst.setDataset(dataSet);
				dataSet.add(inst);
				if (actualRowIndexList != null)
					actualRowIndexList.add(rowIndex);
			}
		}
		logger.debug("Scoring " + dataSet.numInstances() + " instances, " + filtered + " removed by the scoring filter");
		return (dataSet);
	}

	/**
	 * Output the key results of segment models
	 * 
	 * @param stream
	 * @throws Exception
	 */
	public void outputSegmentModels(PrintStream stream) throws Exception
	{
		for (Segment seg : segmentMap.values())
		{
			seg.outputModels(stream);
		}
	}

	/**
	 * Return the column index of a given measure-only success pattern
	 * 
	 * @param mname
	 * @return
	 */
	public int getIndexForMeasure(String mname)
	{
		for (int i = 0; i < mds.numHeaders(); i++)
		{
			Header h = mds.getHeader(i);
			if (h.Type == HeaderType.Pattern)
			{
				Pattern p = sm.Patterns[h.PatternIndex];
				if ((p.Type == Pattern.MEASUREONLY) && p.Measure.equals(mname))
				{
					return (i);
				}
			} else if (h.Type == HeaderType.Target && h.Name.equals(mname))
				return (0);
		}
		logger.error("Could not find the measure '" + mname + "' in the dataset, this is probably not good.");
		return (-1);
	}

	/**
	 * Find the segment of a given target key. Return null if not found.
	 * 
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public Segment getSegment(Object key) throws Exception
	{
		if (segmentMap == null)
			throw (new Exception("No segments have been created for success model"));
		for (Iterator<Entry<Object, Segment>> it = segmentMap.entrySet().iterator(); it.hasNext();)
		{
			Entry<Object, Segment> me = it.next();
			Segment s = me.getValue();
			s.setSmi(this);
			if (s.getTargetRowIndex(key) >= 0)
				return (s);
		}
		return (null);
	}

	/**
	 * Write instances to an arff file of a given name (in the instance directory)
	 * 
	 * @param i
	 * @param name
	 * @throws IOException
	 */
	protected void writeArff(Instances i, String name) throws IOException
	{
		WekaUtil.writeArff(i, instanceDir + File.separator + name, null);
	}

	/**
	 * @return Returns the mds.
	 */
	public ModelDataSet getMds()
	{
		return mds;
	}

	/**
	 * @return Returns the buildPatternClusters.
	 */
	public boolean isBuildPatternClusters()
	{
		return buildPatternClusters;
	}

	/**
	 * @param buildPatternClusters
	 *            The buildPatternClusters to set.
	 */
	public void setBuildPatternClusters(boolean buildPatternClusters)
	{
		this.buildPatternClusters = buildPatternClusters;
	}

	/**
	 * @return the sm
	 */
	public SuccessModel getSuccessModel()
	{
		return sm;
	}

	/**
	 * @param sm
	 *            the sm to set
	 */
	public void setSuccessModel(SuccessModel sm)
	{
		this.sm = sm;
	}

	/**
	 * @return Returns the instanceDir.
	 */
	public String getInstanceDir()
	{
		return instanceDir;
	}

	/**
	 * @param instanceDir
	 *            The instanceDir to set.
	 */
	public void setInstanceDir(String instanceDir)
	{
		this.instanceDir = instanceDir;
	}

	private boolean isDefaultPartition(Object key)
	{
		if (key != null && key.equals(DEFAULT_PARTITION))
		{
			return true;
		}
		return false;
	}
}
