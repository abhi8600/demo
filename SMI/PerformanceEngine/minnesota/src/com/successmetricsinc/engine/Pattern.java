/**
 * $Id: Pattern.java,v 1.6 2007-09-18 00:27:13 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.io.Serializable;

import org.jdom.Element;
import org.jdom.Namespace;

import weka.clusterers.EM;

/**
 * Class to contain a success pattern definition
 * 
 * @author bpeters
 * 
 */
public class Pattern implements Serializable
{
	private static final long serialVersionUID = 1L;
	public static final int MEASUREONLY = 0;
	public static final int ONEATTRIBUTE = 1;
	public static final int TWOATTRIBUTE = 2;
	public int Type;
	public String Measure;
	public String Dimension1;
	public String Attribute1;
	public String Dimension2;
	public String Attribute2;
	public boolean Proportion;
	public boolean Actionable;
	public boolean Model;
	public int MaxPatterns;
	public int Startindex;
	public int NumCategories;
	public Centroid[] Centroids;
	public EM ClusterModel = null;

	public String toString()
	{
		if (Type == MEASUREONLY)
		{
			return (Measure);
		} else if (Type == ONEATTRIBUTE)
		{
			return (Measure + "x" + Dimension1 + "." + Attribute1);
		} else
		{
			return (Measure + "x" + Dimension1 + "." + Attribute1 + "x" + Dimension2 + "." + Attribute2);
		}
	}
	
	public Element getPatternElement(Namespace ns)
	{
		Element e = new Element("Pattern",ns);
		
		Element child = new Element("Type",ns);
		child.setText(String.valueOf(Type));
		e.addContent(child);
		
		child = new Element("Measure",ns);
		child.setText(Measure);
		e.addContent(child);
		
		if (Dimension1!=null)
		{
			child = new Element("Dimension1",ns);
			child.setText(Dimension1);
			e.addContent(child);
		}
		
		if (Attribute1!=null)
		{
			child = new Element("Attribute1",ns);
			child.setText(Attribute1);
			e.addContent(child);
		}
		
		if (Dimension2!=null)
		{
			child = new Element("Dimension2",ns);
			child.setText(Dimension2);
			e.addContent(child);
		}
		
		if (Attribute2!=null)
		{
			child = new Element("Attribute2",ns);
			child.setText(Attribute2);
			e.addContent(child);
		}
		
		child = new Element("Proportion",ns);
		child.setText(String.valueOf(Proportion));
		e.addContent(child);
		
		child = new Element("Actionable",ns);
		child.setText(String.valueOf(Actionable));
		e.addContent(child);
		
		child = new Element("Model",ns);
		child.setText(String.valueOf(Model));
		e.addContent(child);
		
		child = new Element("MaxPatterns",ns);
		child.setText(String.valueOf(MaxPatterns));
		e.addContent(child);
		
		return e;
	}

	public String getName()
	{
		if (Type == MEASUREONLY)
			return (Measure);
		if (Type == ONEATTRIBUTE)
			return (Measure + '-' + Attribute1);
		else
			return (Measure + '-' + Attribute1 + ':' + Attribute2);
	}

	public boolean equals(Object o)
	{
		if (this == o)
			return true;
		if (!(o instanceof Pattern))
			return false;
		Pattern p = (Pattern) o;
		if (p.Type != Type)
			return (false);
		switch (Type)
		{
		case MEASUREONLY:
			return (p.Measure.equals(Measure));
		case ONEATTRIBUTE:
			return (p.Measure.equals(Measure) && p.Dimension1.equals(Dimension1) && p.Attribute1.equals(Attribute1));
		case TWOATTRIBUTE:
			return (p.Measure.equals(Measure) && p.Dimension1.equals(Dimension1) && p.Attribute1.equals(Attribute1) && p.Dimension2.equals(Dimension2) && p.Attribute2
					.equals(Attribute2));
		}
		return (false);
	}
	
	public int hashCode()
	{
		  assert false : "hashCode not designed";
		  return 42; // any arbitrary constant will do 
	}

	public boolean sameAttributes(Pattern p)
	{
		if (p.Type != Type)
			return (false);
		switch (Type)
		{
		case MEASUREONLY:
			return (true);
		case ONEATTRIBUTE:
			return (p.Dimension1.equals(Dimension1) && p.Attribute1.equals(Attribute1));
		case TWOATTRIBUTE:
			return (p.Dimension1.equals(Dimension1) && p.Attribute1.equals(Attribute1) && p.Dimension2.equals(Dimension2) && p.Attribute2.equals(Attribute2));
		}
		return (false);
	}
}
