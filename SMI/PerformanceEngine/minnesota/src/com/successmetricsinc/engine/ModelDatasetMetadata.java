/**
 * $Id: ModelDatasetMetadata.java,v 1.9 2007-08-30 16:26:48 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.List;
import java.util.Map;

/**
 * Class to encapsulate metadata for a model dataset
 * 
 * @author Brad Peters
 * 
 */
class ModelDatasetMetadata implements Externalizable
{
	private static final long serialVersionUID = 1L;
	List<Header> headers;
	Map<String, Integer> keyMap;
	List<Object> partitionKeys;
	int numRows;
	BreakoutAttribute[] breakoutAttributes;

	public ModelDatasetMetadata()
	{
	}

	@SuppressWarnings("unchecked")
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException
	{
		numRows = (Integer) in.readObject();
		headers = (List<Header>) in.readObject();
		partitionKeys = (List<Object>) in.readObject();
		breakoutAttributes = (BreakoutAttribute[]) in.readObject();
	}

	public void writeExternal(ObjectOutput out) throws IOException
	{
		out.writeObject(numRows);
		out.writeObject(headers);
		out.writeObject(partitionKeys);
		out.writeObject(breakoutAttributes);
	}
}