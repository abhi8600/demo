/**
 * $Id: ReferencePopulation.java,v 1.16 2011-09-24 01:03:33 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.List;
import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jdom.Namespace;
import com.successmetricsinc.Repository;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XmlUtils;

public class ReferencePopulation implements Serializable
{
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(ReferencePopulation.class);
	public static final String DEFAULT_NAME = "Default Reference Population";
	private String Name = DEFAULT_NAME;
	public String TargetDimension;
	public String Level;
	private PeerItem[] PerformancePeerColumns;
	private PeerItem[] SuccessPatternPeerColumns;
	private int NumPerformancePeers = 100;
	private int NumSuccessPatternPeers = 100;
	private String SuccessMeasure;
	private String[] PeeringFilters;
	private String PerformanceTableName;
	private String SuccessPatternTableName;
	private boolean isDefault = false;
	
	public String PopulationName;
    public String PerformanceTable;
    public String SuccessPatternTable;
	
	public ReferencePopulation(Element sme, Namespace ns)
	{
		Name = sme.getChildTextTrim("PopulationName", ns);
		PopulationName = sme.getChildTextTrim("PopulationName", ns);
		if (Name == null || Name.length() == 0 || Name.equalsIgnoreCase(DEFAULT_NAME))
		{
			Name = DEFAULT_NAME;
			isDefault = true;
		}
		NumPerformancePeers = Integer.parseInt(sme.getChildText("NumPerformancePeers", ns));
		NumSuccessPatternPeers = Integer.parseInt(sme.getChildText("NumSuccessPatternPeers", ns));
		PeeringFilters = Repository.getStringArrayChildren(sme, "PeeringFilters", ns);
		SuccessMeasure = sme.getChildText("SuccessMeasure", ns);
		TargetDimension = sme.getChildText("TargetDimension", ns);
		Level = sme.getChildText("Level", ns);
		
		Element spe = sme.getChild("PerformancePeerColumns", ns);
		if (spe != null)
		{
			setPerformancePeerColumns(spe, ns);
		}
		
		spe = sme.getChild("SuccessPatternPeerColumns", ns);
		if (spe != null)
		{
			setSuccessPatternsPeerColumns(spe, ns);
		}
		
		PerformanceTableName = sme.getChildText("PerformanceTable", ns);
		PerformanceTable  = sme.getChildText("PerformanceTable", ns);
		if (PerformanceTableName == null || PerformanceTableName.length() == 0)
		{
			if (isDefault())
			{
				PerformanceTableName = "P_PEERS"; // backwards compatibility
			}
			else
			{
				PerformanceTableName = Util.replaceWithPhysicalString("P_PEERS_" + Name);
			}
		}
		SuccessPatternTableName  = sme.getChildText("SuccessPatternTable", ns);
		SuccessPatternTable = sme.getChildText("SuccessPatternTable", ns);
		if (SuccessPatternTableName == null || SuccessPatternTableName.length() == 0)
		{
			if (isDefault())
			{
				SuccessPatternTableName = "S_PEERS"; // backwards compatibility
			}
			else
			{
				SuccessPatternTableName = Util.replaceWithPhysicalString("S_PEERS_" + Name);
			}
		}
	}
	
	public Element getReferencePopulationElement(Namespace ns)
	{
		Element e = new Element("ReferencePopulation",ns);
		
		Element child = null;
		
		XmlUtils.addContent(e, "TargetDimension", TargetDimension,ns);
		
		if (Level!=null)
		{
			child = new Element("Level",ns);
			child.setText(Level);
			e.addContent(child);
		}
		
		child = new Element("PerformancePeerColumns",ns);
		if (PerformancePeerColumns!=null && PerformancePeerColumns.length>0)
		{
			for (PeerItem peerItem : PerformancePeerColumns)
			{
				child.addContent(peerItem.getPeerItemElement(ns));
			}
		}
		e.addContent(child);
		
		child = new Element("SuccessPatternPeerColumns",ns);
		if (SuccessPatternPeerColumns!=null && SuccessPatternPeerColumns.length>0)
		{
			for (PeerItem peerItem : SuccessPatternPeerColumns)
			{
				child.addContent(peerItem.getPeerItemElement(ns));
			}
		}
		e.addContent(child);
		
		child = new Element("NumPerformancePeers",ns);
		child.setText(String.valueOf(NumPerformancePeers));
		e.addContent(child);
		
		child = new Element("NumSuccessPatternPeers",ns);
		child.setText(String.valueOf(NumSuccessPatternPeers));
		e.addContent(child);
		
		XmlUtils.addContent(e, "SuccessMeasure", SuccessMeasure,ns);
		
		e.addContent(Repository.getStringArrayElement("PeeringFilters", PeeringFilters, ns));
		
		XmlUtils.addContent(e, "PopulationName", PopulationName,ns);
		
		XmlUtils.addContent(e, "PerformanceTable", PerformanceTable,ns);
		
		XmlUtils.addContent(e, "SuccessPatternTable", SuccessPatternTable,ns);	
		
		return e;
	}
	
	private void setPerformancePeerColumns(Element spe, Namespace ns)
	{
		@SuppressWarnings("rawtypes")
		List l = spe.getChildren();
		PerformancePeerColumns = new PeerItem[l.size()];
		for (int count = 0; count < l.size(); count++)
		{
			Element pie = (Element) l.get(count);
			PeerItem pi = new PeerItem();
			pi.Column = pie.getChildTextTrim("Column", ns);
			pi.Type = Integer.parseInt(pie.getChildTextTrim("Type", ns));
			pi.Weight = Double.parseDouble(pie.getChildTextTrim("Weight", ns));
			PerformancePeerColumns[count] = pi;
		}
	}
	
	private void setSuccessPatternsPeerColumns(Element spe, Namespace ns)
	{
		@SuppressWarnings("rawtypes")
		List l = spe.getChildren();
		SuccessPatternPeerColumns = new PeerItem[l.size()];
		for (int count = 0; count < l.size(); count++)
		{
			Element pie = (Element) l.get(count);
			PeerItem pi = new PeerItem();
			pi.Column = pie.getChildTextTrim("Column", ns);
			pi.Type = Integer.parseInt(pie.getChildTextTrim("Type", ns));
			pi.Weight = Double.parseDouble(pie.getChildTextTrim("Weight", ns));
			SuccessPatternPeerColumns[count] = pi;
		}
	}
	
	public String getName()
	{
		return Name;
	}
	
	public boolean isDefault()
	{
		return isDefault;
	}
	
	public String getSuccessMeasure()
	{
		return SuccessMeasure;
	}
	
	public PeerItem[] getPeerColumns(boolean performance)
	{
		return (performance) ? PerformancePeerColumns : SuccessPatternPeerColumns;
	}
	
	public int getNumPeers(boolean performance)
	{
		return (performance ? NumPerformancePeers : NumSuccessPatternPeers);
	}
	
	public String getTableName(boolean performance)
	{
		return (performance ? PerformanceTableName : SuccessPatternTableName);
	}
	
	public String[] getPeeringFilters()
	{
		return PeeringFilters;
	}
	
	public String toString()
	{
		return Name + ": " + PerformanceTableName + ", " + SuccessPatternTableName + ", " + SuccessMeasure + ", " + NumPerformancePeers + ", " + NumSuccessPatternPeers;
	}
	
	public static class PeerItem implements Serializable
	{
		private static final long serialVersionUID = 1L;
		public String Column;
		public int Type;
		public double Weight;
		
		public Element getPeerItemElement(Namespace ns)
		{
			DecimalFormat format = new DecimalFormat();
	        format.setDecimalSeparatorAlwaysShown(false);
	        format.setGroupingUsed(false);
	    	
			Element e = new Element("PeerItem",ns);
			
			Element child = new Element("Column",ns);
			child.setText(Column);
			e.addContent(child);
			
			child = new Element("Type",ns);
			child.setText(String.valueOf(Type));
			e.addContent(child);
			
			child = new Element("Weight",ns);
			child.setText(format.format(Weight));
			e.addContent(child);
			
			return e;
		}
	}
}
	
