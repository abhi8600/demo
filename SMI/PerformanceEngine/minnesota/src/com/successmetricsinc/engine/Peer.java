/**
 * $Id: Peer.java,v 1.3 2007-09-20 21:46:51 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.engine;

import java.io.Serializable;

/**
 * Class to contain a peer object for peer comparisons
 * 
 * @author bpeters
 * 
 */
public class Peer implements Comparable<Peer>, Serializable
{
    private static final long serialVersionUID = 1L;
    public Object key;
    public double value;

    public int compareTo(Peer p)
    {
    	return Double.compare(value, p.value);
    }
}
