/**
 * $Id: PatternDifference.java,v 1.3 2011-09-07 21:19:11 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
/**
 * Class to encapsulate the differences associated with to results for a given success pattern
 */
package com.successmetricsinc.engine;

import com.successmetricsinc.query.MeasureColumn;

public class PatternDifference implements Comparable<PatternDifference>
{
	/*
	 * Difference is a score that is proportional to the difference between two different break-outs. The larger the value
	 * the more significant (and meaningful) the difference.
	 */
	public double difference;
	/*
	 * Estimate of the probability of this difference occurring
	 */
	public double probDifference;
	/*
	 * Estimated impact of this difference on the target column
	 */
	public double impact;
	public double[] values;
	public double[] peerValues;
	public String measureCat;
	public String performanceCategory;
	public int measureCount;
	public MeasureColumn mc;
	public Pattern p;
	public String[] category1List;
	public String[] category2List;
	public BreakoutAttribute ba;

	public int compareTo(PatternDifference pd)
	{
		return (new Double(difference).compareTo(new Double(pd.difference)));
	}

	public String toString()
	{
		return (p.toString() + ":" + difference + " (" + probDifference + ")");
	}
}