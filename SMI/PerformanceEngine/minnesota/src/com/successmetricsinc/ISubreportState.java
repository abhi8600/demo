package com.successmetricsinc;

public interface ISubreportState {

	public int getSubreportCount(Object key);
	public void addSubreport(Object key);
	public void removeSubreport(Object key);
}
