/**
 * $Id: CommandSession.java,v 1.25 2012-09-11 22:23:24 BIRST\gsingh Exp $
 *
 * Copyright (C) 2007-2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc;

import java.io.PrintStream;
import java.sql.Connection;
import java.sql.SQLException;

import org.jdom.Document;

import com.successmetricsinc.query.ResultSetCache;
import com.successmetricsinc.status.ModelingHistory;
import com.successmetricsinc.status.Status;

/**
 * @author bpeters
 * 
 */
public class CommandSession
{
	private Repository r;
	private ResultSetCache rsc = null;
	private String path;
	private Connection conn;
	private boolean offline;
	private PrintStream writer = System.out;
	private PrintStream resultsWriter = System.out; // where to write the results of 'runquery'
	private int maxRecords;
	private Session repSession = null;
	private String logFile = null;
	private Status status = null;
	private ModelingHistory modelingHistory = null;
	private Document d = null;

	public Document getDocument() {
		return d;
	}

	public void setDocument(Document d) {
		this.d = d;
	}

	public CommandSession(boolean offline)
	{
		this.offline = offline;
		this.maxRecords = -1;
	}

	public synchronized Repository getRepository()
	{
		return (r);
	}

	public synchronized void setRepository(Repository r)
	{
		this.r = r;
		this.maxRecords = r.getServerParameters().getMaxRecords();
	}

	public synchronized ResultSetCache getResultSetCache()
	{
		return (rsc);
	}

	public synchronized void setResultSetCache(ResultSetCache rsc)
	{
		this.rsc = rsc;
	}

	public synchronized int getMaxRecords()
	{
		return (maxRecords);
	}

	public synchronized void setMaxRecords(int maxRecords)
	{
		this.maxRecords = maxRecords;
	}

	/**
	 * @return Returns the Status object.
	 */
	public synchronized Status getStatus()
	{
		return status;
	}

	/**
	 * @param path
	 *            The Status object to set.
	 */
	public synchronized void setStatus(Status status)
	{
		this.status = status;
	}
	
	/**
	 * @return Returns the ModelingHistory object.
	 */
	public synchronized ModelingHistory getModelingHistory()
	{
		return modelingHistory;
	}

	/**
	 * @param path
	 *            The Status object to set.
	 */
	public synchronized void setModelingHistory(ModelingHistory modelingHistory)
	{
		this.modelingHistory = modelingHistory;
	}
	
	/**
	 * @return Returns the path.
	 */
	public synchronized String getPath()
	{
		return path;
	}

	/**
	 * @param path
	 *            The path to set.
	 */
	public synchronized void setPath(String path)
	{
		this.path = path;
	}

	/**
	 * @return Returns the connection.
	 * @throws SQLException
	 */
	public Connection getConnection() throws SQLException
	{
		if (conn == null)
		{
			if (r == null || r.getConnections() == null || r.getConnections().isEmpty())
				return (null);
			conn = r.getDefaultConnection().ConnectionPool.getConnection();
		}
		return conn;
	}

	/**
	 * @param conn
	 *            The connection to set.
	 */
	public void setConnection(Connection conn)
	{
		this.conn = conn;
	}

	/**
	 * @return the offline
	 */
	public boolean isOffline()
	{
		return offline;
	}

	/**
	 * @return the writer
	 */
	public PrintStream getWriter()
	{
		return writer;
	}
	
	public Session getRepSession()
	{
		return repSession;
	}
	
	public void setRepSession(Session repSession)
	{
		this.repSession = repSession;
	}
	
	public String getLogFile()
	{
		return logFile;
	}
	
	public void setLogFile(String lf)
	{
		logFile = lf;
	}

	public PrintStream getResultsWriter() {
		return resultsWriter;
	}

	public void setResultsWriter(PrintStream resultsWriter) {
		this.resultsWriter = resultsWriter;
	}
}
