/**
 * 
 */
package com.successmetricsinc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.axis2.context.MessageContext;
import org.apache.axis2.transport.http.HTTPConstants;

/**
 * @author agarrison
 *
 */
public class SessionHelper {
	public static MessageContext getMessageContext() {
		return MessageContext.getCurrentMessageContext();
	}

	/**
	 * Gets the Session object for this web service request
	 * @return	the Session; null if there is no session because a user is not logged in.
	 */
	public static HttpSession getSession() {
		HttpServletRequest request = getHttpServletRequest();
		if (request != null) {
			return request.getSession(false);
		}

		return null;
	}
	
	public static HttpServletRequest getHttpServletRequest() {
		MessageContext context = getMessageContext();
		if (context != null) {
			return (HttpServletRequest) context.getProperty(HTTPConstants.MC_HTTP_SERVLETREQUEST);
		}
		return null;
	}
	

}
