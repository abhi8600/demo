/**
 * $Id: Variable.java,v 1.17 2012-07-29 01:32:34 bpeters Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.util.XmlUtils;

public class Variable implements Cloneable, Comparable<Variable>, Serializable
{
	private static final long serialVersionUID = 1L;

	public enum VariableType
	{
		Session, Repository, Warehouse
	};
	private VariableType Type;
	private boolean MultiValue;
	private boolean Constant;
	private String Name;
	private String Query;
	private boolean LogicalQuery;
	private boolean PhysicalQuery;	
	private String ConnectionName;
	private boolean MultipleColumns;
	private boolean Cacheable;
	private String DefaultIfInvalid;
	private boolean evaluateOnDemand;
	private Repository fillRepository;
	private static Pattern getVariablePattern = Pattern.compile("GetVariable\\('.+'\\)");
	private static Pattern oldVariablePattern = Pattern.compile("V\\{.+\\}");
	
	private String Guid;
	private String CreatedDate;
	private String LastModifiedDate;
	private String vtype;
	private boolean Session;
	private String connection;
	private boolean BirstCanModify;
	
	private String CreatedUsername;
	private String LastModifiedUsername;

	public Variable()
	{
	}

	public Variable(Element ve, Namespace ns)
	{
		setName(ve.getChildTextTrim("Name", ns));
		vtype = ve.getChildTextTrim("Type", ns);
		CreatedUsername = ve.getChildText("CreatedUsername",ns);
		LastModifiedUsername = ve.getChildText("LastModifiedUsername",ns);
		if (vtype != null)
		{
			if (vtype.equals("Session"))
				setType(VariableType.Session);
			else if (vtype.equals("Repository"))
				setType(VariableType.Repository);
			else if (vtype.equals("Warehouse"))
				setType(VariableType.Warehouse);
		} else
		{
			String SessionStr = ve.getChildTextTrim("Session", ns);
			boolean Session = SessionStr == null ? false : Boolean.parseBoolean(SessionStr);
			setType(Session ? VariableType.Session : VariableType.Repository);
		}
		setMultiValue(Boolean.parseBoolean(ve.getChildTextTrim("MultiValue", ns)));
		setMultipleColumns(Boolean.parseBoolean(ve.getChildTextTrim("MultipleColumns", ns)));
		setConstant(Boolean.parseBoolean(ve.getChildTextTrim("Constant", ns)));
		setQuery(ve.getChildText("Query", ns));
		setConnectionName(ve.getChildText("Connection", ns));
		setLogicalQuery(Boolean.parseBoolean(ve.getChildTextTrim("LogicalQuery", ns)));
		setPhysicalQuery(Boolean.parseBoolean(ve.getChildTextTrim("PhysicalQuery", ns)));
		String temp = ve.getChildTextTrim("Cacheable", ns);
		setCacheable(temp == null ? false : Boolean.parseBoolean(temp));
		setDefaultIfInvalid(ve.getChildText("DefaultIfInvalid", ns));
		temp = ve.getChildTextTrim("EvaluateOnDemand", ns);
		setEvaluateOnDemand(temp == null ? false : Boolean.parseBoolean(temp));
		
		temp = ve.getChildTextTrim("Session", ns);
		Session = Boolean.parseBoolean(temp);
		
		temp = ve.getChildTextTrim("BirstCanModify", ns);
		BirstCanModify = Boolean.parseBoolean(temp);
		
		Guid = ve.getChildText("Guid", ns);
		CreatedDate = ve.getChildText("CreatedDate", ns);
		LastModifiedDate = ve.getChildText("LastModifiedDate", ns);
		connection = ve.getChildText("Connection", ns);
	}
	
	public Element getVariableElement(Namespace ns)
	{
		Element e = new Element("Variable",ns);
		
		Repository.addBaseAuditObjectAttributes(e, Guid, CreatedDate, LastModifiedDate, CreatedUsername, LastModifiedUsername, ns);
		
		Element child = new Element("Name",ns);
		child.setText(Name);
		e.addContent(child);
		
		child = new Element("Type",ns);
		child.setText(vtype);
		e.addContent(child);
		
		child = new Element("Session",ns);
		child.setText(String.valueOf(Session));
		e.addContent(child);
		
		child = new Element("MultiValue",ns);
		child.setText(String.valueOf(MultiValue));
		e.addContent(child);
		
		child = new Element("Constant",ns);
		child.setText(String.valueOf(Constant));
		e.addContent(child);
		
		child = new Element("MultipleColumns",ns);
		child.setText(String.valueOf(MultipleColumns));
		e.addContent(child);
		
		child = new Element("LogicalQuery",ns);
		child.setText(String.valueOf(LogicalQuery));
		e.addContent(child);
		
		child = new Element("PhysicalQuery",ns);
		child.setText(String.valueOf(PhysicalQuery));
		e.addContent(child);
		
		child = new Element("Query",ns);
		child.setText(Query);
		e.addContent(child);
		
		if (connection!=null)
		{
			XmlUtils.addContent(e, "Connection", connection,ns);
		}
		
		if (DefaultIfInvalid!=null)
		{
		  XmlUtils.addContent(e, "DefaultIfInvalid", DefaultIfInvalid,ns);
		}
		
		child = new Element("Cacheable",ns);
		child.setText(String.valueOf(Cacheable));
		e.addContent(child);
		
		child = new Element("EvaluateOnDemand",ns);
		child.setText(String.valueOf(evaluateOnDemand));
		e.addContent(child);
		
		if (BirstCanModify)
		{
			child = new Element("BirstCanModify",ns);
			child.setText(String.valueOf(BirstCanModify));
			e.addContent(child);
		}
		
		return e;
	}

	public Object clone() throws CloneNotSupportedException
	{
		// No object references requiring deep copy - so just call the super's clone method.
		Variable v = (Variable) super.clone();
		return v;
	}

	public int compareTo(Variable o)
	{
		return getName().compareTo(o.getName());
	}

	public void setType(VariableType type)
	{
		Type = type;		
	}

	public VariableType getType()
	{
		return Type;
	}

	public void setMultiValue(boolean multiValue)
	{
		MultiValue = multiValue;
	}

	public boolean isMultiValue()
	{
		return MultiValue;
	}

	public void setConstant(boolean constant)
	{
		Constant = constant;
	}

	public boolean isConstant()
	{
		return Constant;
	}

	public void setName(String name)
	{
		Name = name;
	}

	public String getName()
	{
		return Name;
	}

	public void setQuery(String query)
	{
		Query = query;
	}

	public String getQuery()
	{
		return Query;
	}

	public void setLogicalQuery(boolean logicalQuery)
	{
		LogicalQuery = logicalQuery;
	}

	public boolean isLogicalQuery()
	{
		return LogicalQuery;
	}

	public void setPhysicalQuery(boolean physicalQuery)
	{
		PhysicalQuery = physicalQuery;
	}

	public boolean isPhysicalQuery()
	{
		return PhysicalQuery;
	}

	public void setConnectionName(String connectionName)
	{
		ConnectionName = connectionName;
	}

	public String getConnectionName()
	{
		return ConnectionName;
	}

	public void setMultipleColumns(boolean multipleColumns)
	{
		MultipleColumns = multipleColumns;
	}

	public boolean isMultipleColumns()
	{
		return MultipleColumns;
	}

	public void setCacheable(boolean cacheable)
	{
		Cacheable = cacheable;
	}

	public boolean isCacheable()
	{
		return Cacheable;
	}

	public void setDefaultIfInvalid(String defaultIfInvalid)
	{
		DefaultIfInvalid = defaultIfInvalid;
	}

	public String getDefaultIfInvalid()
	{
		return DefaultIfInvalid;
	}

	public boolean isEvaluateOnDemand()
	{
		return evaluateOnDemand;
	}

	public void setEvaluateOnDemand(boolean evaluateOnDemand)
	{
		this.evaluateOnDemand = evaluateOnDemand;
	}

	public Repository getFillRepository()
	{
		return fillRepository;
	}

	public void setFillRepository(Repository fillRepository)
	{
		this.fillRepository = fillRepository;
	}

	public Set<Variable> getDependencies(Repository r)
	{
		Set<Variable> result = new HashSet<Variable>();
		if (LogicalQuery && r.isUseNewQueryLanguage())
		{
			Matcher m = getVariablePattern.matcher(Query);
			while (m.find())
			{
				String vname = Query.substring(m.start() + 13, m.end() - 2);
				Variable v = r.findVariable(vname);
				if (v != null)
					result.add(v);
			}
		} else
		{
			Matcher m = oldVariablePattern.matcher(Query);
			while (m.find())
			{
				String vname = Query.substring(m.start() + 2, m.end() - 1);
				Variable v = r.findVariable(vname);
				if (v != null)
					result.add(v);
			}
		}
		return result;
	}
	
	public String toString()
	{
		return Name;
	}
}