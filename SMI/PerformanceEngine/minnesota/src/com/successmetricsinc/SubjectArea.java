/**
 * $Id: SubjectArea.java,v 1.51 2012-11-11 20:06:37 birst\bpeters Exp $
 *
 * Copyright (C) 2007-2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.Folder.FolderType;
import com.successmetricsinc.query.DimensionColumn;
import com.successmetricsinc.query.DimensionTable;
import com.successmetricsinc.query.Hierarchy;
import com.successmetricsinc.query.Level;
import com.successmetricsinc.query.MeasureColumn;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.warehouse.StagingColumn;
import com.successmetricsinc.warehouse.StagingTable;
import com.successmetricsinc.warehouse.TimeDefinition;
import com.successmetricsinc.warehouse.TimePeriod;

public class SubjectArea implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(SubjectArea.class);
	private String name;
	private Folder[] folders;
	private String[] groups;
	private static final String DefaultSubjectAreaName = "Available Columns";
	public static final String AttributeFolderName = "Attributes";
	public static final String MeasureFolderName = "Measures";
	public static final String TimeMeasureFolderName = "Time Series Measures";
	public static String[] KEY_FOLDERS = {
		AttributeFolderName,
		MeasureFolderName,
		TimeMeasureFolderName,
	};
	private static final String LoadDateName = "Load Date";
	private static final String SubjectAreaBy = "By";
	private static boolean GROUP_HIERARCHY_LEVELS = false;
	private Repository r;
	private boolean created;
	// don't change without changing SubjectAreaTreeDataDescriptor.as
	public static final String NODE_SEPARATOR = "|~%#";

	public SubjectArea(Repository r)
	{
		this.r = r;
	}

	public SubjectArea(Element e, Namespace ns)
	{
		name = e.getChildTextTrim("Name", ns);
		groups = Repository.getStringArrayChildren(e, "Groups", ns);
		Element fe = e.getChild("Folders", ns);
		if (fe != null)
		{
			@SuppressWarnings("rawtypes")
			List l2 = fe.getChildren();
			folders = new Folder[l2.size()];
			for (int count2 = 0; count2 < l2.size(); count2++)
			{
				Element cfe = (Element) l2.get(count2);
				folders[count2] = new Folder(cfe, ns, null);
			}
		}
		created = true; // if explicit, it is already created
	}
	
	public Element getSubjectAreaElement(Namespace ns)
	{
		Element e = new Element("SubjectArea",ns);
		
		Element child = new Element("Name",ns);
		child.setText(name);
		e.addContent(child);
		
		if (folders!=null && folders.length>0)
		{
			child = new Element("Folders",ns);
			for (Folder folder : folders)
			{
				child.addContent(folder.getFolderElement(ns));
			}
			e.addContent(child);
		}
		
		if (groups!=null && groups.length>0)
		{
			e.addContent(Repository.getStringArrayElement("Groups", groups, ns));
		}
				
		return e;
	}

	private void create()
	{
		created = true;
		name = DefaultSubjectAreaName;
		List<Folder> flist = new ArrayList<Folder>();
		Folder attribFolder = new Folder();
		attribFolder.setGroups(new String[0]);
		flist.add(attribFolder);
		attribFolder.setName(AttributeFolderName);
		List<Folder> attribFolders = new ArrayList<Folder>();
		attribFolder.setColumns(new Column[0]);
		groups = new String[0];
		//if (r.getDimensionalStructure() == Repository.RELATIONAL_DIMENSIONAL_STRUCTURE)
		{
			List<String> hlist = new ArrayList<String>();
			if (r.getTimeDefinition() != null)
				hlist.add(r.getTimeDefinition().Name);
			for (Hierarchy h : r.getHierarchies())
			{
				//Preserve the hidden hierarchy check for backward compatibility - the flag is deprecated in 5.2
				if (!h.Hidden)
					if (!hlist.contains(h.DimensionName))
						hlist.add(h.DimensionName);
			}
			for (int i = 0; i < hlist.size(); i++)
			{
				Hierarchy h = r.findDimensionHierarchy(hlist.get(i));
				if (h == null)
				{
					logger.warn("Could not find hierarchy entry for: " + hlist.get(i));
					continue;
				}
				Folder af = new Folder();
				af.setGroups(new String[0]);
				af.setName(hlist.get(i));
				List<DimensionColumn> dclist = r.getDimensionColumns(hlist.get(i));
				Map<String, List<DimensionColumn>> dcmap = new HashMap<String, List<DimensionColumn>>();
				for (DimensionColumn dc : dclist)
				{
					if (dc.Key || dc.DimensionTable.packageHide)
						continue;
					if(dc.DimensionTable.snowflake)
					{
						continue;
					}
					// Don't show surrogate keys
					if (h.isSurrogateKeyColumn(dc.ColumnName))
						continue;
					String levelName = null;
					// Find the lowest level staging table it is in
					StagingTable[] stlist = r.getStagingTables();
					if (stlist != null)
					{
						Level curLevel = null;
						for (StagingTable st : stlist)
						{
							boolean inst = false;
							String lname = null;
							for (String[] stlevel : st.Levels)
							{
								if (stlevel[0].equals(h.Name))
								{
									inst = true;
									lname = stlevel[1];
									break;
								}
							}
							if (inst)
							{
								for (StagingColumn sc : st.Columns)
								{
									if (sc.Name.equals(dc.ColumnName) && sc.TargetTypes.contains(h.DimensionName))
									{
										Level cl = h.findLevel(lname);
										if (cl != null)
										{
											if (curLevel == null)
												curLevel = cl;
											else if (curLevel != cl && curLevel.findLevel(cl) != null)
												curLevel = cl;
										}
										break;
									}
								}
							}
						}
						if (curLevel != null && curLevel != h.DimensionKeyLevel)
							levelName = curLevel.Name;
					}
					if (levelName == null)
					{
						// Find level of table if didn't find in staging tables
						if (dc.DimensionTable.InheritTable != null && !dc.DimensionTable.InheritTable.isEmpty())
						{
							/*
							 * Only rename, however, if the hierarchy we are inheriting from is hidden (as happens in
							 * Automatic Mode). Otherwise this is an explicit inerhitance, so use the real name
							 */
							DimensionTable dt = r.findDimensionTable(dc.DimensionTable.InheritTable);
							Hierarchy ih = r.findDimensionHierarchy(dt.DimensionName);
							if (ih != null && ih.Hidden)
							{
								levelName = dt.Level != null && !dt.Level.isEmpty() ? dt.Level : null;
								if (dc.DimensionTable.InheritPrefix != null && (!dc.DimensionTable.InheritPrefix.isEmpty()) && dt.DimensionName != null
										&& dt.DimensionName.equals(dc.DimensionTable.DimensionName))
								{
									levelName = dc.DimensionTable.InheritPrefix + levelName;
								} else if (levelName.equals(h.DimensionKeyLevel.Name))
									levelName = null;
							} else
							{
								levelName = dc.DimensionTable.Level != null && !dc.DimensionTable.Level.isEmpty() ? dc.DimensionTable.Level : null; 
							}
						}
					}
					List<DimensionColumn> dedupedclist = dcmap.get(levelName);
					if (dedupedclist == null)
					{
						dedupedclist = new ArrayList<DimensionColumn>();
						dcmap.put(levelName, dedupedclist);
					}
					boolean found = false;
					for (Entry<String, List<DimensionColumn>> e : dcmap.entrySet())
						for (DimensionColumn dc2 : e.getValue())
							if (dc.ColumnName.equals(dc2.ColumnName))
							{
								found = true;
								break;
							}
					if (!found)
						dedupedclist.add(dc);
				}
				List<Folder> subfolderlist = new ArrayList<Folder>();
				for (Entry<String, List<DimensionColumn>> e : dcmap.entrySet())
				{
					Column[] clist = new Column[e.getValue().size()];
					for (int j = 0; j < e.getValue().size(); j++)
					{
						Column c = new Column();
						c.setDimension(af.getName());
						c.setName(e.getValue().get(j).ColumnName);
						clist[j] = c;
					}
					if (e.getKey() == null)
					{
						af.setColumns(clist);
					} else if (clist.length > 0)
					{
						Folder sf = new Folder();
						sf.setGroups(new String[0]);
						sf.setName(e.getKey());
						sf.setColumns(clist);
						sf.setSubfolders(new Folder[0]);
						subfolderlist.add(sf);
					}
				}
				if (GROUP_HIERARCHY_LEVELS)
				{
					Folder[] subfolders = new Folder[subfolderlist.size()];
					subfolderlist.toArray(subfolders);
					af.setSubfolders(subfolders);
					if ((af.getColumns() != null && af.getColumns().length > 0) || (af.getSubfolders() != null && af.getSubfolders().length > 0))
						attribFolders.add(af);
				} else
				{
					for (Folder sf : subfolderlist)
					{
						if ((sf.getColumns() != null && sf.getColumns().length > 0) || (sf.getSubfolders() != null && sf.getSubfolders().length > 0))
							attribFolders.add(sf);
					}
					if ((af.getColumns() != null && af.getColumns().length > 0) || (af.getSubfolders() != null && af.getSubfolders().length > 0))
					{
						if (!subfolderlist.isEmpty())
							af.setName(h.DimensionKeyLevel.Name);
						attribFolders.add(af);
					}
				}
			}
		}
		//else if (r.getDimensionalStructure() == Repository.OLAP_DIMENSIONAL_STRUCTURE)
		{
			List<String> olapDimensions = new ArrayList<String>();
			Map<String, List<String>> olapDimHierarchies = new HashMap<String, List<String>>();
			for (Hierarchy hr : r.getHierarchies())
			{
				String hDim = hr.OlapDimensionName;
				if (hDim != null && hr.DimensionName.startsWith(hDim + "-"))
				{
					if (!olapDimensions.contains(hDim))
						 olapDimensions.add(hDim);
					String hName = hr.DimensionName.substring((hDim + "-").length());
					if (olapDimHierarchies.get(hDim) == null)
					{
						olapDimHierarchies.put(hDim, new ArrayList<String>());
					}
					olapDimHierarchies.get(hDim).add(hName);
				}
			}
			for (String dim : olapDimensions)
			{
				Folder dimFolder = new Folder();
				dimFolder.setGroups(new String[0]);
				dimFolder.setName(dim);
				dimFolder.setType(FolderType.olapDimension);
				attribFolders.add(dimFolder);
				List<Folder> dimSubFolders = new ArrayList<Folder>();
				if (olapDimHierarchies.get(dim) != null && !olapDimHierarchies.get(dim).isEmpty())
				{
					for (String hr : olapDimHierarchies.get(dim))
					{
						Folder dimHrFolder = new Folder();
						dimHrFolder.setGroups(new String[0]);
						dimHrFolder.setName(hr);
						dimHrFolder.setType(FolderType.olapHierarchy);
						dimSubFolders.add(dimHrFolder);												
					}
				}
				Folder[] dimHirFolders = dimSubFolders.toArray(new Folder[]{});
				dimFolder.setSubfolders(dimHirFolders);
			}
		}
		Folder[] attribFolderArr = new Folder[attribFolders.size()];
		attribFolders.toArray(attribFolderArr);
		attribFolder.setSubfolders(attribFolderArr);
		// Setup measure folder
		Folder measureFolder = new Folder();
		measureFolder.setGroups(new String[0]);
		flist.add(measureFolder);
		measureFolder.setName(MeasureFolderName);
		// Setup time series measure folder
		Folder timeMeasureFolder = new Folder();
		timeMeasureFolder.setGroups(new String[0]);
		timeMeasureFolder.setName(TimeMeasureFolderName);
		// Add measures to folders
		Set<MeasureColumn> measures = r.getMeasureColumns();
		// Get a list of time period prefixes
		Map<String, TimePeriod> periodSet = new HashMap<String, TimePeriod>();
		Map<TimePeriod, Map<String, List<MeasureColumn>>> periodMap = new HashMap<TimePeriod, Map<String, List<MeasureColumn>>>();
		TimeDefinition td = r.getTimeDefinition();
		if (td != null && td.Periods != null)
		{
			for (TimePeriod p : td.Periods)
			{
				periodSet.put(p.Prefix, p);
			}
		}
		// Get a list of measure columns organized by root measure
		// Bucket by time period
		for (MeasureColumn mc : measures)
		{
			// if it is to be hidden for imported package
			if(mc.MeasureTable.packageHide)
			{
				continue;
			}
			String key = mc.RootMeasure == null ? mc.ColumnName : mc.RootMeasure;
			// use the last prefix to see if it's a prefix for a time period
			TimePeriod tp = mc.Prefixes != null && mc.Prefixes.size() > 0 ? periodSet.get(mc.Prefixes.get(mc.Prefixes.size() - 1).trim()) : null;
			Map<String, List<MeasureColumn>> folderList = periodMap.get(tp);
			if (folderList == null)
			{
				folderList = new TreeMap<String, List<MeasureColumn>>();
				periodMap.put(tp, folderList);
			}
			List<MeasureColumn> list = folderList.get(key);
			if (list == null)
			{
				list = new ArrayList<MeasureColumn>();
				folderList.put(key, list);
			}
			list.add(mc);
		}
		List<Folder> measureFolderList = new ArrayList<Folder>();
		// Create one folder per time period
		for (Entry<TimePeriod, Map<String, List<MeasureColumn>>> period : periodMap.entrySet())
		{
			TimePeriod tp = period.getKey();
			Folder pf = null;
			if (tp == null)
			{
				pf = measureFolder;
			} else
			{
				pf = new Folder();
				pf.setGroups(new String[0]);
				pf.setName(period.getKey().toString());
				measureFolderList.add(pf);
			}
			List<Folder> periodFolderList = new ArrayList<Folder>();
			for (Entry<String, List<MeasureColumn>> en : period.getValue().entrySet())
			{
				Folder newFolder = getMeasureFolder(en.getKey(), en.getValue(), periodSet.keySet(), tp == null ? "" : tp.Prefix);
				periodFolderList.add(newFolder);
			}
			Folder[] periodSubfolders = new Folder[periodFolderList.size()];
			periodFolderList.toArray(periodSubfolders);
			pf.setSubfolders(periodSubfolders);
		}
		// Setup time measure folders
		Folder[] subfolders = new Folder[measureFolderList.size()];
		measureFolderList.toArray(subfolders);
		if (subfolders.length > 0)
		{
			timeMeasureFolder.setSubfolders(subfolders);
			flist.add(timeMeasureFolder);
		}
		folders = new Folder[flist.size()];
		flist.toArray(folders);
	}

	private Folder getMeasureFolder(String rootMeasure, List<MeasureColumn> mcList, Set<String> prefixesToExclude, String displayPrefix)
	{
		if (displayPrefix == null)
			displayPrefix = "";
		else
			displayPrefix = displayPrefix + " ";
		Folder f = new Folder();
		f.setName(rootMeasure);
		f.setGroups(new String[0]);
		Map<String, Folder> subfolders = new HashMap<String, Folder>();
		for (MeasureColumn mc : mcList)
		{
			String prefix = null;
			if (mc.Prefixes == null)
			{
				if (mc.MeasureTable.Folder != null)
					prefix = mc.MeasureTable.Folder;
				else
					prefix = LoadDateName;
			} else if (mc.MeasureTable.Folder != null)
			{
				prefix = mc.MeasureTable.Folder;
			} else
			{
				StringBuilder sbpf = new StringBuilder();
				for (String s : mc.Prefixes)
					if (!prefixesToExclude.contains(s.trim()))
						sbpf.append(s);
				prefix = sbpf.toString();
			}
			if (prefix.isEmpty())
				prefix = LoadDateName;
			if (mc.MeasureTable.Folder != null && mc.MeasureTable.Folder.equals("SUBJECTAREA_EXCLUDE"))
				continue;
			boolean noBy = false;
			if (prefix.equals("NO_TIME") && mc.Prefixes != null && mc.Prefixes.size() > 0)
			{
				StringBuilder sbpf = new StringBuilder();
				for (String s : mc.Prefixes)
					if (!prefixesToExclude.contains(s.trim()))
						sbpf.append(s);
				prefix = sbpf.toString();
				noBy = true;
			}
			if (prefix.isEmpty()) {
				// BPD-17759 and BPD-17677
				continue;
			}
			
			Folder sf = subfolders.get(prefix);
			if (sf == null)
			{
				sf = new Folder();
				sf.setGroups(new String[0]);
				String name = (noBy ? "" : SubjectAreaBy + " ") + prefix;
				if (name.endsWith(": "))
					name = name.substring(0, name.length() - 2);
				sf.setName(name);
				subfolders.put(prefix, sf);
			}
			Column[] cols = sf.getColumns();
			if (cols == null)
			{
				cols = new Column[]
				{ new Column() };
				cols[0].setName(mc.ColumnName);
				cols[0].setDisplayName(displayPrefix + Util.firstCap(mc.AggregationRule));
			} else
			{
				Set<Column> newList = new TreeSet<Column>();
				// Make sure it's not a dupe (might be mapped in multiple facts)
				boolean dupe = false;
				for (int i = 0; i < cols.length; i++)
				{
					if (cols[i].getName().equals(mc.ColumnName))
					{
						dupe = true;
						break;
					}
					newList.add(cols[i]);
				}
				if (!dupe)
				{
					Column c = new Column();
					c.setName(mc.ColumnName);
					c.setDisplayName(displayPrefix + Util.firstCap(mc.AggregationRule));
					newList.add(c);
					cols = new Column[newList.size()];
					newList.toArray(cols);
				}
			}
			sf.setColumns(cols);
		}
		if (subfolders.size() > 1)
			subfolders.remove("NO_TIME");
		if (subfolders.size() == 1)
		{
			Folder sf = subfolders.values().iterator().next();
			f.setColumns(sf.getColumns());
			f.setSubfolders(sf.getSubfolders());
		} else
		{
			Folder[] subfolderarr = new Folder[subfolders.size()];
			int count = 0;
			for (Folder sf : subfolders.values())
			{
				subfolderarr[count++] = sf;
			}
			f.setSubfolders(subfolderarr);
		}
		return (f);
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getName()
	{
		if (!created)
			create();
		return name;
	}

	public void setFolders(Folder[] folders)
	{
		this.folders = folders;
	}

	public Folder[] getFolders()
	{
		if (!created)
			create();
		return folders;
	}

	public void setGroups(String[] groups)
	{
		this.groups = groups;
	}

	public String[] getGroups()
	{
		if (!created)
			create();
		return groups;
	}

	public String toString()
	{
		if (!created)
			create();
		return (this.getName());
	}
}