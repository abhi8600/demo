package com.successmetricsinc;

public class VersionInfo {

	private static final int EngineVersion = 5;
	//1 - datadir and databasepath arguments for persistaggregates for inmemory aggregates (since 5.2.3)
	//1 - setProperties command for DBA (since 5.2.3)
	//2 - touch command to set date modified on file(since 5.2.3)
	//3 - temp table drop command for IB (5.5)
	//4 - using unload/copy to copytable for Paraccel(5.6.3)
	//4 - 19355 Need to be able to process just changed data sources
	//5 - 24258 Implement a new command for copying space catalog in Java
	
	public static void main(String[] args) {
		System.out.print(EngineVersion);
	}

}
