/**
 * $Id: Group.java,v 1.10 2010-02-19 01:24:07 pconnolly Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.jdom.Element;
import org.jdom.Namespace;

/**
 * @author bpeters
 *
 */
public class Group implements Serializable
{
	public static final String GROUP_ADMINISTRATORS = "Administrators";
	public static final String GROUP_OWNER$ = "OWNER$";
	public static final String GROUP_USER$ = "USER$";
	public enum INTERNAL_GROUPS {
		ADMINISTRATORS,
		OWNER$,
		USER$,
	};
	public static final String GROUP_ALL = "All";
	
    private static final long serialVersionUID = 1L;
    private String name;
    private List<String> userNames;
    private List<String> filters;
    private boolean internalGroup = false;
    private String id;
    private boolean isFiltersTag;

    public Group() {
    }
    
    public Group(Element e,Namespace ns)
    {
    	id = e.getChildText("Id", ns);
    	name = e.getChildText("Name", ns);
    	userNames = Repository.getStringArrayListChildren(e, "Usernames", ns);
    	filters = Repository.getStringArrayListChildren(e, "Filters", ns);
    	isFiltersTag = e.getChild("Filters",ns)!=null?true:false;
    	String temp = e.getChildText("InternalGroup",ns);
    	if (temp!=null)
    	{
    		internalGroup = Boolean.parseBoolean(temp);
    	}
    }
    
    public Element getGroupElement(Namespace ns)
	{
		Element e = new Element("Group",ns);
		
		Element child = null;
		
		if (id!=null)
		{
			child = new Element("Id",ns);
			child.setText(id);
			e.addContent(child);
		}
		
		child = new Element("Name",ns);
		child.setText(name);
		e.addContent(child);
		
		if (userNames!=null && !userNames.isEmpty())
		{
			e.addContent(Repository.getStringArrayElement("Usernames", userNames, ns));
		}
		
		if (filters!=null && !filters.isEmpty())
		{
			e.addContent(Repository.getStringArrayElement("Filters", filters, ns));
		}
		else if (isFiltersTag) 
		{
			child = new Element("Filters",ns);
			e.addContent(child);
		}
		
		child = new Element("InternalGroup",ns);
		child.setText(String.valueOf(internalGroup));
		e.addContent(child);
				
		return e;
	}
    
    public Group(final String name) {
    	this.name = name;
    }
    
    public String toString() {
        return name + " " + id;
    }
    
    @Override
    public boolean equals(Object o) {
    	if (o instanceof Group) {
    		if (name.equals(((Group) o).getName())) {
    			return true;
    		} else {
    			return false;
    		}
    	} else {
    		throw new IllegalArgumentException("'equals()' requires an input of type 'Group'.");
    	}
    }
    
    public boolean equals(String s) {
    	return(s.equals(name));
    }
    
	public void setName(String name) {
		this.name = name;
	}
	
	public void setId(String id){
		this.id = id;
	}
	
    public String getName() {
    	return name;
    }
    
    public String getId() {
    	return id;
    }
    
	public void setInternalGroup(boolean internalGroup) {
		this.internalGroup = internalGroup;
	}

	public boolean isInternalGroup() {
		return internalGroup;
	}

	public void setUserNames(List<String> userNames) {
		this.userNames = userNames;
	}

    public void addUser(String userName) {
    	if (getUserNames() == null) {
    		setUserNames(new ArrayList<String>());
    	}
    	getUserNames().add(userName);
    }

	public List<String> getUserNames() {
		return userNames;
	}

	public static List<String> getGroupNamesList(final List<Group> groups) {
		List<String> groupNames = new ArrayList<String>();
		for (Group group : groups) {
			groupNames.add(group.getName());
		}
		return groupNames;
	}
	
	public void setFilters(List<String> filters){
		this.filters = filters;
	}
	
	public List<String> getFilters()
	{
		return filters;
	}
}
