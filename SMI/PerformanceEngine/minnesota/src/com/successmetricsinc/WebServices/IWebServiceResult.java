/**
 * $Id: IWebServiceResult.java,v 1.1 2011-08-17 21:35:13 agarrison Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */


package com.successmetricsinc.WebServices;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

/**
 * Each result of a web service call must implement this interface to serialize the data to clients
 * @author agarrison
 *
 */
public interface IWebServiceResult {

	/**
	 * Adds the content of this result object to the result object
	 * @param parent	the parent XML node
	 * @param factory	the OMFactory to use while adding content
	 * @param ns	the namespace to use
	 */
	public void addContent(OMElement parent, OMFactory factory, OMNamespace ns);
	
}
