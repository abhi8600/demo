/**
 * $Id: ServerParameters.java,v 1.43 2012-11-13 05:25:21 birst\bpeters Exp $
 *
 * Copyright (C) 2007-2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.TimeZone;

import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.util.TimeZoneUtil;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author Brad Peters
 * 
 */
public class ServerParameters implements Serializable
{
	private static final long serialVersionUID = 1L;
	private static final int DEFAULT_MAX_RECORDS = 1000000;
	private static final int DEFAULT_MAX_QUERYTIME = 1200; // will need different values when modeling engine uses query result set
	private String ApplicationPath;
	private String CatalogPath;
	private int MaxRecords = DEFAULT_MAX_RECORDS;
	private int MaxQueryTime = DEFAULT_MAX_QUERYTIME;
	private boolean MapNullToZero = false; // for backwards compatibility - set to true if you want pre 2.2 null semantics
	private int MaxSnowflakeSize;
	private boolean AllowDimensionalCrossjoins;
	// Whether to pick a measure table to complete a query that only has dimensions (overrides AllowDimensionCrossjoins)
	private boolean ForceDimensionCrossJoinsThroughFacts = true;
	private boolean ClearDimensionsOnReload = false;
	private TimeZone ProcessingTimeZone;
    private TimeZone DisplayTimeZone;
    private long MaxScriptStatements;
    private int MaxInputRows;
    private int MaxOutputRows;
    private int ScriptQueryTimeout; // Seconds
    private String ProxyDBConnectionString;
    private String ProxyDBUserName;
    private String ProxyDBPassword;
    private String Collation;
    
    private String strApplicationPath;
    private String CachePath;
    private int MaxCacheEntries;
    private String CustomerMailHost;
    private String StagingEncryptionKey;
    private boolean GenerateUniqueKeys;
    private String strProcessingTimeZone;
    private String strDisplayTimeZone;
    private boolean DisableCachingForGeneratedMeasureTables;
    private boolean DisableCachingForGeneratedDimensionTables;
    private long BulkDeleteBatchSize=1000000;
	private int MaxThreadsInETL=0;
    
	public ServerParameters(Element e, Namespace ns) throws IOException
	{
		String smiInstallDir = System.getProperty("smi.home");
		setApplicationPath(e.getChildText("ApplicationPath", ns));
		strApplicationPath = e.getChildText("ApplicationPath", ns);
		CachePath = e.getChildText("CachePath", ns);
		if (smiInstallDir != null)
		{
			setApplicationPath(getApplicationPath().replace("V{SMI_INSTALL_DIR}", smiInstallDir));
		}
		setApplicationPath(getApplicationPath().replace('\\', File.separatorChar));
		setApplicationPath(getApplicationPath().replace('/', File.separatorChar));
		if (getApplicationPath().endsWith(File.separator))
			setApplicationPath(getApplicationPath().substring(0, getApplicationPath().length() - 1));
		String temp = e.getChildText("MaxRecords", ns);
		if (temp != null && temp.length() > 0)
		{
			setMaxRecords(Integer.parseInt(temp.trim()));
		}
		temp = e.getChildText("MaxCacheEntries", ns);
		if (temp != null && temp.length() > 0)
		{
			MaxCacheEntries = Integer.parseInt(temp.trim());
		}		
		CustomerMailHost = e.getChildText("CustomerMailHost", ns);		
		temp = e.getChildText("MaxQueryTime", ns);
		if (temp != null && temp.length() > 0)
		{
			setMaxQueryTime(Integer.parseInt(temp.trim()));
		}
		temp = e.getChildText("MapNullToZero", ns);
		if (temp != null && temp.length() > 0)
		{
			setMapNullToZero(Boolean.parseBoolean(temp.trim()));
		}
		temp = e.getChildText("MaxSnowflakeSize", ns);
		if (temp != null && temp.length() > 0)
		{
			this.setMaxSnowflakeSize(Integer.parseInt(temp.trim()));
		}
		temp = e.getChildText("AllowDimensionalCrossjoins", ns);
		if (temp != null && temp.length() > 0)
		{
			this.setAllowDimensionalCrossjoins(Boolean.parseBoolean(temp.trim()));
		}
		temp = e.getChildText("MaxScriptStatements", ns);
		if (temp != null && temp.length() > 0)
		{
			this.setMaxScriptStatements(Long.parseLong(temp.trim()));
		}
		temp = e.getChildText("MaxInputRows", ns);
		if (temp != null && temp.length() > 0)
		{
			this.setMaxInputRows(Integer.parseInt(temp.trim()));
		}
		temp = e.getChildText("MaxOutputRows", ns);
		if (temp != null && temp.length() > 0)
		{
			this.setMaxOutputRows(Integer.parseInt(temp.trim()));
		}
		temp = e.getChildText("ScriptQueryTimeout", ns);
		if (temp != null && temp.length() > 0)
		{
			this.setScriptQueryTimeout(Integer.parseInt(temp.trim()));
		}
		setProcessingTimeZone(e.getChildText("ProcessingTimeZone", ns));
		strProcessingTimeZone = e.getChildText("ProcessingTimeZone", ns);
		setDisplayTimeZone(e.getChildText("DisplayTimeZone", ns));
		strDisplayTimeZone = e.getChildText("DisplayTimeZone", ns);
		temp = e.getChildText("ProxyDBConnectionString", ns);
		if (temp != null && temp.length() > 0)
		{
			this.ProxyDBConnectionString = temp;
		}
		temp = e.getChildText("ProxyDBUserName", ns);
		if (temp != null && temp.length() > 0)
		{
			this.ProxyDBUserName = temp;
		}
		temp = e.getChildText("ProxyDBPassword", ns);
		if (temp != null)
		{
			this.ProxyDBPassword = temp;
		}
		temp = e.getChildText("Collation",ns);
		if (temp!=null)
		{
			this.Collation = temp;
		}
		StagingEncryptionKey = e.getChildText("StagingEncryptionKey",ns);
		temp = e.getChildText("GenerateUniqueKeys", ns);
		if (temp != null && temp.length() > 0)
		{
			GenerateUniqueKeys = Boolean.parseBoolean(temp);
		}
		temp = e.getChildText("DisableCachingForGeneratedMeasureTables", ns);
		if (temp != null && temp.length() > 0)
		{
			DisableCachingForGeneratedMeasureTables = Boolean.parseBoolean(temp);
		}
		temp = e.getChildText("DisableCachingForGeneratedDimensionTables", ns);
		if (temp != null && temp.length() > 0)
		{
			DisableCachingForGeneratedDimensionTables = Boolean.parseBoolean(temp);
		}		
		temp = e.getChildText("BulkDeleteBatchSize", ns);
	    if (temp != null && temp.length() > 0)
	    {
	      BulkDeleteBatchSize = Long.parseLong(temp);
	    }
 		
		temp = e.getChildText("MaxThreadsInETL", ns);
	    if (temp != null && temp.length() > 0)
	    {
	    	MaxThreadsInETL = Integer.parseInt(temp);
	    }
	}
	
	public Element getServerParametersElement(Namespace ns)
	{
		Element e = new Element("ServerParameters",ns);
		
		Element child = new Element("ApplicationPath",ns);
		child.setText(strApplicationPath);
		e.addContent(child);
		
		XmlUtils.addContent(e, "CachePath", CachePath,ns);	
		
		child = new Element("MaxCacheEntries",ns);
		child.setText(String.valueOf(MaxCacheEntries));
		e.addContent(child);
		
		child = new Element("MaxRecords",ns);
		child.setText(String.valueOf(MaxRecords));
		e.addContent(child);
		
		XmlUtils.addContent(e, "CustomerMailHost", CustomerMailHost,ns);
		
		child = new Element("MapNullToZero",ns);
		child.setText(String.valueOf(MapNullToZero));
		e.addContent(child);
		
		child = new Element("MaxQueryTime",ns);
		child.setText(String.valueOf(MaxQueryTime));
		e.addContent(child);
		
		child = new Element("StagingEncryptionKey",ns);
		child.setText(String.valueOf(StagingEncryptionKey));
		e.addContent(child);
		
		child = new Element("GenerateUniqueKeys",ns);
		child.setText(String.valueOf(GenerateUniqueKeys));
		e.addContent(child);
		
		child = new Element("MaxSnowflakeSize",ns);
		child.setText(String.valueOf(MaxSnowflakeSize));
		e.addContent(child);
		
		child = new Element("AllowDimensionalCrossjoins",ns);
		child.setText(String.valueOf(AllowDimensionalCrossjoins));
		e.addContent(child);
		
		XmlUtils.addContent(e, "ProcessingTimeZone", strProcessingTimeZone,ns);
		
		XmlUtils.addContent(e, "DisplayTimeZone", strDisplayTimeZone,ns);
		
		child = new Element("MaxScriptStatements",ns);
		child.setText(String.valueOf(MaxScriptStatements));
		e.addContent(child);
		
		child = new Element("MaxInputRows",ns);
		child.setText(String.valueOf(MaxInputRows));
		e.addContent(child);
		
		child = new Element("MaxOutputRows",ns);
		child.setText(String.valueOf(MaxOutputRows));
		e.addContent(child);
		
		child = new Element("ScriptQueryTimeout",ns);
		child.setText(String.valueOf(ScriptQueryTimeout));
		e.addContent(child);
		
		XmlUtils.addContent(e, "ProxyDBConnectionString", ProxyDBConnectionString,ns);
		
		XmlUtils.addContent(e, "ProxyDBUserName", ProxyDBUserName,ns);
		
		XmlUtils.addContent(e, "ProxyDBPassword", ProxyDBPassword,ns);
		
		if (Collation!=null)
		{
		  XmlUtils.addContent(e, "Collation", Collation,ns);
		}
		
		if (DisableCachingForGeneratedMeasureTables)
		{
			child = new Element("DisableCachingForGeneratedMeasureTables",ns);
			child.setText(String.valueOf(DisableCachingForGeneratedMeasureTables));
			e.addContent(child);
		}
		
		if (DisableCachingForGeneratedDimensionTables)
		{
			child = new Element("DisableCachingForGeneratedDimensionTables",ns);
			child.setText(String.valueOf(DisableCachingForGeneratedDimensionTables));
			e.addContent(child);
		}
		
		if (ForceDimensionCrossJoinsThroughFacts)
		{
			child = new Element("ForceDimensionCrossJoinsThroughFacts",ns);
			child.setText(String.valueOf(ForceDimensionCrossJoinsThroughFacts));
			e.addContent(child);
		}	
	
		if (BulkDeleteBatchSize != 1000000)
		{
			child = new Element("BulkDeleteBatchSize",ns);
			child.setText(String.valueOf(BulkDeleteBatchSize));
			e.addContent(child);
		}
		if (MaxThreadsInETL != 0)
		{
			child = new Element("MaxThreadsInETL",ns);
			child.setText(String.valueOf(MaxThreadsInETL));
			e.addContent(child);
		}
		return e;
	}

	public TimeZone getProcessingTimeZone() {
		return ProcessingTimeZone;
	}

	public void setProcessingTimeZone(String windowsID) {
		if(windowsID == null || windowsID.equals("") || windowsID.equals("Server Local Time"))
			ProcessingTimeZone = TimeZone.getDefault();
		else
			ProcessingTimeZone = TimeZoneUtil.getTimeZoneForWindowsTZID(windowsID);
	}

	public TimeZone getDisplayTimeZone() {
		return DisplayTimeZone;
	}

	public void setDisplayTimeZone(String windowsID) {
		if(windowsID == null || windowsID.equals("") || windowsID.equals("Server Local Time") || windowsID.equals("Same As Processing Time Zone"))
			DisplayTimeZone = ProcessingTimeZone;
		else
			DisplayTimeZone = TimeZoneUtil.getTimeZoneForWindowsTZID(windowsID);
	}

	public String getCatalogPath()
	{
		return CatalogPath;
	}

	public void setApplicationPath(String applicationPath)
	{
		ApplicationPath = applicationPath;
	}

	public String getApplicationPath()
	{
		return ApplicationPath;
	}

	public void setCatalogPath(String catalogPath)
	{
		CatalogPath = catalogPath;
	}

	private void setMapNullToZero(boolean mapNullToZero)
	{
		MapNullToZero = mapNullToZero;
	}

	public boolean isMapNullToZero()
	{
		return MapNullToZero;
	}

	private void setMaxQueryTime(int maxQueryTime)
	{
		if (maxQueryTime == -1)
		{
			maxQueryTime = ServerParameters.DEFAULT_MAX_QUERYTIME;
		}
		MaxQueryTime = maxQueryTime;
	}

	public int getMaxQueryTime()
	{
		return MaxQueryTime;
	}

	public void setMaxRecords(int maxRecords)
	{
		if (maxRecords == -1)
		{
			maxRecords = ServerParameters.DEFAULT_MAX_RECORDS;
		}
		MaxRecords = maxRecords;
	}

	public int getMaxRecords()
	{
		int sessionMaxRecords = Session.getMaxRowCount();
		if (sessionMaxRecords > 0)
			return sessionMaxRecords;
		return MaxRecords;
	}

	public int getMaxSnowflakeSize()
	{
		return MaxSnowflakeSize;
	}

	public void setMaxSnowflakeSize(int maxSnowflakeSize)
	{
		MaxSnowflakeSize = maxSnowflakeSize;
	}

	public boolean isClearDimensionsOnReload()
	{
		return ClearDimensionsOnReload;
	}

	public void setClearDimensionsOnReload(boolean clearDimensionsOnReload)
	{
		ClearDimensionsOnReload = clearDimensionsOnReload;
	}

	public boolean isAllowDimensionalCrossjoins()
	{
		return AllowDimensionalCrossjoins;
	}

	public void setAllowDimensionalCrossjoins(boolean allowDimensionalCrossjoins)
	{
		AllowDimensionalCrossjoins = allowDimensionalCrossjoins;
	}

	public long getMaxScriptStatements()
	{
		return MaxScriptStatements;
	}

	public void setMaxScriptStatements(long maxScriptStatements)
	{
		MaxScriptStatements = maxScriptStatements;
	}

	public int getMaxInputRows()
	{
		return MaxInputRows;
	}

	public void setMaxInputRows(int maxInputRows)
	{
		MaxInputRows = maxInputRows;
	}

	public int getMaxOutputRows()
	{
		return MaxOutputRows;
	}

	public void setMaxOutputRows(int maxOutputRows)
	{
		MaxOutputRows = maxOutputRows;
	}

	public int getScriptQueryTimeout()
	{
		return ScriptQueryTimeout;
	}

	public void setScriptQueryTimeout(int scriptQueryTimeout)
	{
		ScriptQueryTimeout = scriptQueryTimeout;
	}
	
	public String getProxyDBConnectionString()
	{
		return ProxyDBConnectionString;
	}
	
	public void setProxyDBConnectionString(String proxyDBConnectionString)
	{
		this.ProxyDBConnectionString = proxyDBConnectionString;
	}

	public String getProxyDBUserName()
	{
		return ProxyDBUserName;
	}
	
	public void setProxyDBUserName(String proxyDBUserName)
	{
		this.ProxyDBUserName = proxyDBUserName;
	}

	public String getProxyDBPassword()
	{
		return ProxyDBPassword;
	}
	
	public void setProxyDBPassword(String proxyDBPassword)
	{
		this.ProxyDBPassword = proxyDBPassword;
	}

	public boolean isForceDimensionCrossJoinsThroughFacts()
	{
		return ForceDimensionCrossJoinsThroughFacts;
	}

	public void setForceDimensionCrossJoinsThroughFacts(boolean forceDimensionCrossJoinsThroughFacts)
	{
		ForceDimensionCrossJoinsThroughFacts = forceDimensionCrossJoinsThroughFacts;
	}

	public String getCollation() 
	{
		return Collation;
	}

	public void setCollation(String collation) 
	{
		Collation = collation;
	}

	public long getBulkDeleteBatchSize() {
		return BulkDeleteBatchSize;
	}

	public void setBulkDeleteBatchSize(long bulkDeleteBatchSize) {
		BulkDeleteBatchSize = bulkDeleteBatchSize;
	}

	public int getMaxThreadsInETL()
	{
			return MaxThreadsInETL;
	}

	public void setMaxThreadsInETL(int maxThreadsInETL)
	{
			MaxThreadsInETL = maxThreadsInETL;
	}
}