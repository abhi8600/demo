/**
 * $Id: Session.java,v 1.102 2012-09-11 21:16:38 BIRST\agarrison Exp $
 *
 * Copyright (C) 2007-2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc;

import java.io.File;
import java.io.Serializable;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.fill.JRBaseFiller;

import org.apache.log4j.Logger;

import com.successmetricsinc.Variable.VariableType;
import com.successmetricsinc.query.Database;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.DatabaseConnectionStatement;
import com.successmetricsinc.query.DimensionColumn;
import com.successmetricsinc.query.Filter;
import com.successmetricsinc.query.JasperDataSourceProvider;
import com.successmetricsinc.query.ResultSetCache;
import com.successmetricsinc.query.TempTable;
import com.successmetricsinc.query.DatabaseConnectionStatement.TYPE;
import com.successmetricsinc.transformation.object.SavedExpressions;
import com.successmetricsinc.util.RepositoryExpression;
import com.successmetricsinc.util.SimpleMRUCache;
import com.successmetricsinc.util.Util;

/**
 * Class to instantiate a user session
 */
public class Session implements Serializable
{
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(Session.class);
	private static boolean fillVariablesParallely = false;
	private Map<String, Object> variables;
	private transient List<DatabaseConnectionStatement> currentStatementList = null;
	private static final String DEMOMODE = "DemoMode";
	private boolean isCancelled = false;
	private String catalogDir;
	private transient Map<DatabaseConnection, List<TempTable>> tempTableMap = null;
	private transient Map<String, AtomicInteger> querySynchronization;
	private transient ReapTempTableThread rttt = null;
	private transient SavedExpressions privateSavedExpressions;
	private transient Map<String, JasperReport> jasperReportMap = new HashMap<String, JasperReport>();
	private transient Map<Long, JasperPrint> designerPageCache = Collections.synchronizedMap(new SimpleMRUCache<Long, JasperPrint>(4));
	private transient Repository repository;
	private Object defaultReportProperties;
	private boolean isLoadingDefaultReportProperties = false;	
	public enum Renderer
	{
		Dashboard, Adhoc
	};

	private static class ThreadSpecificData
	{
		private boolean isPdf;
		private boolean isPrint;
		private String dashletPrompts;
		// filter params from prompts
		private List<Filter> params = null;
		private Map<String, Filter> paramMap = null;
		// List of fillers currently executing
		private List<JRBaseFiller> currentFillerList = null;
		private Renderer renderer = Renderer.Adhoc;
		private String dashboardParams;	
		private String dashboardParamsSeperator;
		private String sessionVars;
		private boolean olapUseIndentation = true;
		private int maxPageCount = -1;
		private int maxRowCount = -1;
	}
	private static Map<Long, ThreadSpecificData> threadSpecificDataMap = (Map<Long, ThreadSpecificData>) Collections
			.synchronizedMap(new HashMap<Long, ThreadSpecificData>());
	private static Map<Long, Long> threadAssociationMap = (Map<Long, Long>) Collections.synchronizedMap(new HashMap<Long, Long>());

	public static void associateThreadWithParentThread(Long l, Long callingThreadId)
	{
		threadAssociationMap.put(l, callingThreadId);
	}

	public static void unAssociateThreadWithParentThread(Long l)
	{
		threadAssociationMap.remove(l);
	}

	private static ThreadSpecificData getThreadSpecificData()
	{
		Long threadId = getThreadID();
		ThreadSpecificData tsd = threadSpecificDataMap.get(threadId);
		if (tsd == null)
		{
			tsd = new ThreadSpecificData();
			threadSpecificDataMap.put(threadId, tsd);
		}
		return tsd;
	}

	private static Long getThreadID()
	{
		Long id = Long.valueOf(Thread.currentThread().getId());
		Long parentThread = threadAssociationMap.get(id);
		while (parentThread != null)
		{
			id = parentThread;
			parentThread = threadAssociationMap.get(id);
		}
		return id;
	}

	public String getCatalogDir()
	{
		return catalogDir;
	}

	public void setCatalogDir(String catalogDir)
	{
		this.catalogDir = catalogDir;
	}
	private static Map<Long, Session> threadStuff = (Map<Long, Session>) Collections.synchronizedMap(new HashMap<Long, Session>());

	public static void addThreadRequest(Long id, Session r)
	{
		Session.threadStuff.put(id, r);
	}

	public static void removeThreadRequest(Long id)
	{
		Session.threadStuff.remove(id);
	}

	public static Session getCurrentThreadSession()
	{
		Long l = Long.valueOf(Thread.currentThread().getId());
		return Session.threadStuff.get(l);
	}

	public Session(String username)
	{
		variables = new HashMap<String, Object>();
		variables.put("USER", username);
	}

	public Session()
	{
		variables = new HashMap<String, Object>();
	}

	public Map<String, Object> getSessionVariables()
	{
		return variables;
	}

	public Object getSessionVariableValue(String name, Repository r)
	{
		if (name.startsWith("P['") && name.endsWith("']"))
		{
			List<Filter> params = getParams();
			if (params != null)
			{
				Filter f = getFilter(name.substring(3, name.length() - 2));
				if (f != null)
				{
					List<String> list = f.getValues();
					if (list != null && !list.isEmpty())
					{
						String cname = f.getColumnName();
						int index = cname.indexOf('.');
						boolean charcol = true;
						if (index > 0)
						{
							DimensionColumn dc = r.findDimensionColumn(cname.substring(0, index), cname.substring(index + 1));
							if (dc != null && (dc.DataType.equals("Integer") || dc.DataType.equals("Float") || dc.DataType.equals("Number")))
							{
								charcol = false;
							}
						} else
						{
							charcol = false;
							if (list.size() == 1)
							{
								String s = list.get(0);
								try
								{
									Double.parseDouble(s);
								} catch (NumberFormatException ex)
								{
									charcol = true;
								}
							}
						}
						if (charcol)
						{
							if (list.size() == 1)
								return "'" + list.get(0) + "'";
							else
								return "'" + list.toString() + "'";
						} else
						{
							if (list.size() == 1)
								return list.get(0);
							else
								return list.toString();
						}
					} else
						return "NULL";
				}
			}
		} else if (name.startsWith("="))
		{
			RepositoryExpression re = new RepositoryExpression(r, this, name.substring(1));
			return re.evaluate();
		}
		return variables.get(name);
	}

	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		boolean first = true;
		for (String key : variables.keySet())
		{
			Object val = variables.get(key);
			if (!first)
			{
				sb.append(", ");
			}
			sb.append(key);
			sb.append("=");
			sb.append(val);
			if (first)
				first = false;
		}
		sb.append("}");
		return sb.toString();
	}

	private static List<JRBaseFiller> getCurrentFillers()
	{
		ThreadSpecificData tsd = getThreadSpecificData();
		if (tsd.currentFillerList == null)
		{
			tsd.currentFillerList = Collections.synchronizedList(new LinkedList<JRBaseFiller>());
		}
		return tsd.currentFillerList;
	}

	public static void removeFromCurrentFillers(JRBaseFiller filler)
	{
		getCurrentFillers().remove(filler);
	}

	public static void addToCurrentFillers(JRBaseFiller filler)
	{
		getCurrentFillers().add(filler);
	}
	
	public void fillSessionVariables(Repository r, ResultSetCache rsc) throws SQLException
	{
		fillSessionVariables(r, rsc, null);
	}

	/**
	 *  When searchVarsSet argument is provided by caller, lightweight session is created with only those session variables that are requested by caller.
	 *  This parameter should be used with caution and only be used in cases when caller only requires to replace selective variables and does not require 
	 *  to initialize all session variables for user.
	 *  Currently only BirstConnect and Connectors provide specific list of variables that need to be replaced when initializing userbean.
	 * 
	 */
	public void fillSessionVariables(Repository r, ResultSetCache rsc, Set<String> searchVarsSet) throws SQLException
	{
		Variable[] vars = r.getVariables();
		if (searchVarsSet != null && !searchVarsSet.isEmpty())
		{
			Set<Variable> searchVariablesSet = new HashSet<Variable>();
			for (Variable v : r.getVariables())
			{
				if (searchVarsSet.contains(v.getName()) || "USER".equals(v.getName()))  // always allow special session variable "USER"
					searchVariablesSet.add(v);
			}
			vars = searchVariablesSet.toArray(new Variable[0]);
		}
		if (fillVariablesParallely == false) {
			if (vars != null)
			{
				JasperDataSourceProvider jdsp = new JasperDataSourceProvider(r, rsc);
				jdsp.setSession(this);
				for (int i = 0; i < vars.length; i++)
				{
					if ("USER".equals(vars[i].getName())) // special session variable
						continue;
					if (vars[i].getType() == Variable.VariableType.Session)
					{
						if (vars[i].isConstant())
						{
							variables.put(vars[i].getName(), r.replaceVariables(this, vars[i].getQuery()));
						} else
						{
							Object existingValue = variables.get(vars[i].getName());
							if (existingValue == null)
							{
								fillVariable(r, null, jdsp, vars[i]);
							}
						}
					}
				}
				instantiateDynamicGroupVariable();
			}
			return;
		}
		if (vars != null)
		{
			Map<Variable, Set<Variable>> dependencyMap = new HashMap<Variable, Set<Variable>>();
			for (int i = 0; i < vars.length; i++)
			{
				if ("USER".equals(vars[i].getName())) // special session variable
					continue;
				if (vars[i].getType() == Variable.VariableType.Session)
				{
					if (vars[i].isConstant())
					{
						variables.put(vars[i].getName(), r.replaceVariables(this, vars[i].getQuery()));
					} else
					{
						Object existingValue = variables.get(vars[i].getName());
						if (existingValue == null)
						{
							dependencyMap.put(vars[i], vars[i].getDependencies(r));
						}
					}
				}
			}
			ExecutorService ltp = null;
			if (dependencyMap.size() > 0)
			{
				/*
				 * Fill variables in parallel
				 */
				while (dependencyMap.size() > 0)
				{
					Map<Variable, Set<Variable>> newMap = new HashMap<Variable, Set<Variable>>();
					List<Variable> fillList = new ArrayList<Variable>();
					for (Entry<Variable, Set<Variable>> e : dependencyMap.entrySet())
					{
						boolean fillable = true;
						for (Variable depv : e.getValue())
						{
							if (dependencyMap.containsKey(depv))
							{
								fillable = false;
								break;
							}
						}
						if (fillable)
						{
							fillList.add(e.getKey());
						} else
						{
							newMap.put(e.getKey(), e.getValue());
						}
					}
					if (fillList.size() == 0)
					{
						logger.info("Unable to fill session variables because of circular dependencies");
						break;
					}
					if (fillList.size() == 1)
					{
						RunFillThread rft = new RunFillThread(r, fillList.get(0), rsc, this);
						rft.run();
					} else if (fillList.size() > 1)
					{
						/*
						 * If can fill more than one in parallel - spawn a threadpool to do so
						 */
						ltp = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 2);
						for (Variable v : fillList)
						{
							RunFillThread rft = new RunFillThread(r, v, rsc, this);
							ltp.execute(rft);
						}
						try
						{
							ltp.shutdown();
							ltp.awaitTermination(10, TimeUnit.MINUTES);
						} catch (InterruptedException e)
						{
							logger.error(e);
						}
					}
					dependencyMap = newMap;
				}
			}
		}
		return;
	}

	private class RunFillThread implements Runnable
	{
		private Variable v;
		private Repository r;
		private ResultSetCache rsc;
		private Session s;

		public RunFillThread(Repository r, Variable v, ResultSetCache rsc, Session s)
		{
			this.r = r;
			this.v = v;
			this.rsc = rsc;
			this.s = s;
		}

		@Override
		public void run()
		{
			try
			{
				JasperDataSourceProvider jdsp = new JasperDataSourceProvider(r, rsc);
				jdsp.setSession(s);
				fillVariable(r, null, jdsp, v);
			} catch (Exception e)
			{
				logger.error("Unable to fill variable [" + v.getName() + "]:" + e);
			}
		}
	}

	// This method is to ensure backward compatibility when dynamic groups
	// is represented through Birst$Groups and NOT old GROUPS
	private void instantiateDynamicGroupVariable()
	{
		Object newReserved = variables.get("Birst$Groups");
		Object oldReserved = variables.get("GROUPS");
		if (newReserved == null && oldReserved != null)
		{
			// copy the object to the new reserved variable if it does NOT exist
			variables.put("Birst$Groups", oldReserved);
		}
		// if Birst$Groups is already populated, we are all good.
	}

	/**
	 * Fill a single session variable
	 * 
	 * @param r
	 * @param stmt
	 * @param jdsp
	 * @param v
	 */
	@SuppressWarnings("rawtypes")
	public void fillVariable(Repository r, Statement stmt, JasperDataSourceProvider jdsp, Variable v)
	{
		if ("USER".equals(v.getName())) // special case, USER is dynamically set
			return;
		Object o = r.fillVariable(stmt, v, this, jdsp);
		if (o == null)
		{
			// Check if there is a defaultValueifInvalid
			if (v.getType() == Variable.VariableType.Session && v.getDefaultIfInvalid() != null)
			{
				String defaultIfInvalidValue = v.getDefaultIfInvalid().trim();
				if (defaultIfInvalidValue.length() > 0)
				{
					logger.info("Using defaultIfInvalid value for variable = " + v.getName() + " with value = " + defaultIfInvalidValue);
					o = defaultIfInvalidValue;
				}
			}
		}
		if (o != null)
		{
			// make sure that the arraylist is not empty
			if (ArrayList.class.isInstance(o) && ((ArrayList) o).isEmpty())
				return;
			// GS bugfix 7494 - adding a check for object type to be an integer and putting the object directly -
			// No need for string conversion
			if (ArrayList.class.isInstance(o) || Util.isNumber(o))
				variables.put(v.getName(), o);
			else
				variables.put(v.getName(), Util.convertToString(o, false, r, false));
		}
	}

	/**
	 * Returns the value of a session variable
	 * 
	 * @param s
	 * @return
	 */
	public Object getVariable(String s)
	{
		return (variables.get(s));
	}

	/**
	 * the variable is updated after validation
	 * 
	 * @param variableName
	 * @param value
	 * @return true, if variable is updated. false, otherwise (validation problem)
	 */
	public boolean updateVariable(Repository r, String variableName, String value, boolean doValidation)
	{
		// see if the existing object is an ArrayList. We instantiate ArrayList object for
		// multi value query session variables
		if ("USER".equals(variableName)) // special session variable
			return false;
		boolean updated = false;
		Object obj = variables.get(variableName);
		if (obj == null)
		{
			// cannot overwrite if it is not existing
			return false;
		}
		// Find the variable type it is constant or not
		Variable variable = r.findVariable(variableName);
		if (variable != null && variable.isConstant())
		{
			String variableConstantValue = updateVariableConstant(r, variableName, value);
			if (variableConstantValue != null)
			{
				variables.put(variableName, variableConstantValue);
				updated = true;
			} else
			{
				logger.warn("Variable returned after updating is null for " + variableName);
				updated = false;
			}
			return updated;
		}
		if (ArrayList.class.isInstance(obj))
		{
			@SuppressWarnings("rawtypes")
			Object o = ((ArrayList) obj).get(0);
			boolean validateQuotes = (o.getClass() == String.class);
			boolean validateHashQuoted = (o.getClass() == Date.class);
			List<Object> newValues = getMultiValueList(value, doValidation, validateQuotes, validateHashQuoted);
			if (newValues == null || newValues.isEmpty())
			{
				logger.warn("Multi value list is null or empty for value - " + value + " Skipping population of variable - " + variableName);
				updated = false;
			} else
			{
				variables.put(variableName, newValues);
				updated = true;
			}
		} else
		{
			Object formattedValue = getFormattedValue(value);
			if (formattedValue == null)
			{
				logger.warn("Formatted Value is null for - " + value + " Skipping population of variable - " + variableName);
				updated = false;
			} else
			{
				variables.put(variableName, formattedValue);
				updated = true;
			}
		}
		return updated;
	}

	private String updateVariableConstant(Repository r, String variableName, String value)
	{
		String constant = null;
		// for constant, always create multi value list. Takes care of
		// single value case also.
		// guess based on the first element
		String[] multiValueArray = value.split(",");
		String firstValue = multiValueArray[0].trim();
		boolean validateQuotes = firstValue.startsWith("'") && firstValue.endsWith("'");
		boolean validateHashQuoted = firstValue.startsWith("#") && firstValue.endsWith("#");
		List<Object> newValues = getMultiValueList(value, true, validateQuotes, validateHashQuoted);
		if (newValues == null || newValues.isEmpty())
		{
			logger.warn("Multi value constant list is null or empty for value - " + value + " Skipping population of variable constant - " + variableName);
		} else
		{
			StringBuilder builder = new StringBuilder();
			for (int i = 0; i < newValues.size(); i++)
			{
				if (i > 0)
				{
					builder.append(",");
				}
				builder.append(Util.convertToString(newValues.get(i), true, r, true));
			}
			constant = builder.toString();
		}
		return constant;
	}

	private List<Object> getMultiValueList(String value, boolean doValidation, boolean validateQuotes, boolean validateHashQuoted)
	{
		String[] multiValueArray = value.split(",");
		List<Object> newValues = new ArrayList<Object>();
		// get the type of the filled variable values
		boolean valid = true;
		for (String str : multiValueArray)
		{
			boolean isQuoted = str.startsWith("'") && str.endsWith("'");
			boolean ishashQuoted = str.startsWith("#") && str.endsWith("#");
			if (doValidation)
			{
				if (!isQuoted && validateQuotes)
				{
					valid = false;
					break;
				} else if (!ishashQuoted && validateHashQuoted)
				{
					valid = false;
					break;
				}
			}
			// quotes are put during replaceVariables later
			Object formattedValue = getFormattedValue(str);
			if (formattedValue == null)
			{
				logger.warn("Formatted Value is null for value - " + str + " Skipping this value");
			} else
			{
				newValues.add(formattedValue);
			}
		}
		if (!valid)
		{
			logger.warn("Validation error while overwriting session variables with value : " + value);
			return null;
		}
		return newValues;
	}

	private Object getFormattedValue(String str)
	{
		boolean isQuoted = str.startsWith("'") && str.endsWith("'");
		boolean ishashQuoted = str.startsWith("#") && str.endsWith("#");
		// quotes are put during replaceVariables later
		Object modValue = null;
		if (isQuoted)
		{
			modValue = str.substring(1, str.length() - 1);
		} else if (ishashQuoted)
		{
			try
			{
				modValue = Date.valueOf(str.substring(1, str.length() - 1));
			} catch (IllegalArgumentException ex)
			{
				logger.warn("Date passed is not in the expected format yyyy-mm-dd : " + str, ex);
			}
		} else
		{
			try
			{
				modValue = Integer.valueOf(str);
			} catch (NumberFormatException ex)
			{
				logger.warn("Number provided cannot be parsed as Integer. Trying as Double" + str);
				// if we cannot parse it as integer, try double
				try
				{
					modValue = Double.valueOf(str);
				} catch (NumberFormatException ex2)
				{
					// we can't do anything now. logout and contine
					logger.warn("Number provided cannot be parsed as Double" + str, ex2);
				}
			}
		}
		return modValue;
	}

	/**
	 * Create a New variable
	 * 
	 * @param createMultiValue
	 *            - If true, always create list object for session variable. Otherwise, guess from comma separated
	 *            values
	 * @param variableName
	 *            - session variable name to be created
	 * @param value
	 *            - session variable value to be formatted and filled
	 */
	public boolean createNewVariable(boolean createMultiValue, String variableName, String value)
	{
		boolean updated = false;
		if ("USER".equals(variableName)) // special session variable
			return false;
		String[] multiValueArray = value.split(",");
		if (multiValueArray.length > 1 || createMultiValue)
		{
			// guess based on the first element
			String firstValue = multiValueArray[0].trim();
			boolean validateQuotes = firstValue.startsWith("'") && firstValue.endsWith("'");
			boolean validateHashQuoted = firstValue.startsWith("#") && firstValue.endsWith("#");
			List<Object> newValues = getMultiValueList(value, true, validateQuotes, validateHashQuoted);
			if (newValues == null || newValues.isEmpty())
			{
				logger.warn("Multi value list is null or empty for value - " + value + " Skipping population of variable - " + variableName);
				updated = false;
			} else
			{
				variables.put(variableName, newValues);
				updated = true;
			}
		} else
		{
			Object formattedValue = getFormattedValue(value);
			if (formattedValue == null)
			{
				logger.warn("Formatted Value is null for - " + value + " Skipping population of variable - " + variableName);
				updated = false;
			} else
			{
				variables.put(variableName, formattedValue);
				updated = true;
			}
		}
		return updated;
	}

	/**
	 * Returns list containing all the remaining session variables
	 * 
	 * @param repository
	 * @param modifiedVariableNameList
	 *            - variables to be excluded from the returned list
	 * @return
	 */
	public List<Variable> getRemainingSessionVariables(Repository repository, List<String> modifiedVariableNameList)
	{
		Variable[] vars = repository.getVariables();
		// if nothing is modified, the entire list is unmodified
		if (modifiedVariableNameList == null || modifiedVariableNameList.isEmpty())
		{
			return Arrays.asList(vars);
		}
		List<Variable> remainingVariableList = new ArrayList<Variable>();
		for (Variable v : vars)
		{
			if (modifiedVariableNameList.contains(v.getName()))
			{
				continue;
			}
			if (v.getType() == VariableType.Session && !Util.containsVariable(remainingVariableList, v))
			{
				remainingVariableList.add(v);
			}
		}
		return remainingVariableList;
	}

	/**
	 * Instantiates "NOT" of the list given. Meaning, it will instantiate all the session variables except those given
	 * in the list
	 * 
	 * @param repository
	 * @param variableNamesList
	 */
	public void instantiateAllSessionVarsNotInList(Repository repository, List<String> variableNamesList)
	{
		if (variableNamesList == null || variableNamesList.isEmpty())
		{
			return;
		}
		List<Variable> varListToInstantiate = this.getRemainingSessionVariables(repository, variableNamesList);
		if (varListToInstantiate != null && !varListToInstantiate.isEmpty())
		{
			instantiateSessionVariables(repository, varListToInstantiate);
		}
	}

	/**
	 * Fill the current session with variables after executing them
	 * 
	 * @param repository
	 * @param varListToInstantiate
	 *            List of Session Variables to Instantiate
	 */
	public void instantiateSessionVariables(Repository repository, List<Variable> varListToInstantiate)
	{
		// Map of the variables to be updated. Taking a backup in case the re-instantiation fails.
		// Revert to the old value
		Map<String, Object> previousVariableValues = new HashMap<String, Object>();
		for (Variable var : varListToInstantiate)
		{
			if ("USER".equals(var.getName()))
				continue;
			String key = var.getName();
			Object existingValue = getVariable(var.getName());
			previousVariableValues.put(key, existingValue);
			variables.remove(key);
		}
		JasperDataSourceProvider jdsp = new JasperDataSourceProvider(repository);
		jdsp.setSession(this);
		for (Variable var : varListToInstantiate)
		{
			if ("USER".equals(var.getName()))
				continue;
			fillVariable(repository, null, jdsp, var);
			// should have filled. If not revert to old value
			String key = var.getName();
			if (variables.get(key) == null && previousVariableValues.get(key) != null)
			{
				logger.warn("Reverting to old variable value for " + key);
				variables.put(var.getName(), previousVariableValues.get(key));
			}
		}
	}

	public void setDemoMode(boolean mode)
	{
		if (mode)
			variables.put(DEMOMODE, "true");
		else
			variables.remove(DEMOMODE);
	}

	public boolean isDemoMode()
	{
		return variables.containsKey(DEMOMODE);
	}

	public void addCurrentStatement(DatabaseConnectionStatement currentStatement)
	{
		if (currentStatementList == null)
		{
			currentStatementList = Collections.synchronizedList(new LinkedList<DatabaseConnectionStatement>());
		}
		currentStatementList.add(currentStatement);
	}

	public void resetState()
	{
		this.setCancelled(true);
		try
		{
			cancelCurrentRequest();
		} catch (Exception ex)
		{
			logger.warn("Error in cancelling request ", ex);
		}
	}

	private void cancelQueries()
	{
		if (currentStatementList != null)
		{
			List<DatabaseConnectionStatement> list = currentStatementList;
			currentStatementList = null;
			Iterator<DatabaseConnectionStatement> iter = list.iterator();
			while (iter.hasNext())
			{
				try
				{
					DatabaseConnectionStatement stmt = iter.next();
					if (logger.isDebugEnabled())
						logger.debug("Canceling SQL statement " + stmt);
					if (stmt.isDatabaseType())
						stmt.cancel();
					else if (stmt.getType() == TYPE.Realtime)
						stmt.CancelQueriesForLiveAccess();
				} catch (Exception sqle)
				{
					if (logger.isDebugEnabled())
						logger.debug("Exception while trying to cancel/close a statement:" + sqle.toString());
				}
			}
		}
	}

	private void cancelCurrentRequest()
	{
		cancelQueries();
		List<JRBaseFiller> fillers = getCurrentFillers();
		int numFillers = fillers.size();
		for (int i = 0; i < numFillers; i++)
		{
			JRBaseFiller f = fillers.get(i);
			try
			{				
				f.cancelFill();
			} catch (Exception e)
			{
				if (logger.isDebugEnabled())
					logger.debug("Request filler cancelled: " + e.getMessage());
			}
		}
	}

	public void removeStatement(DatabaseConnectionStatement stmt)
	{
		if (currentStatementList != null)
		{			
			currentStatementList.remove(stmt);
		}
	}

	public boolean isCancelled()
	{
		return isCancelled;
	}

	public void setCancelled(boolean isCancelled)
	{
		this.isCancelled = isCancelled;
	}

	/**
	 * 
	 * @return the username of the logged in user
	 */
	public String getUserName()
	{
		Object name = getVariable("USER");
		if (name != null)
		{
			return (String) name;
		}
		return null;
	}

	public static void clear() {
		Long threadId = getThreadID();
		threadSpecificDataMap.remove(threadId);
	}
	
	public static List<Filter> getParams()
	{
		ThreadSpecificData tsd = getThreadSpecificData();
		return tsd.params;		
	}

	public static void setParams(List<Filter> params)
	{
		ThreadSpecificData tsd = getThreadSpecificData();
		tsd.params = params;
		tsd.paramMap = new HashMap<String, Filter>();
		if (params != null)
		{
			for (Filter f : params)
			{
				boolean cloneFilter = false;
				List<String> filterValues = f.getValues();
				if (filterValues!=null && !filterValues.isEmpty() && f.getPromptType()!=null && f.getPromptType().equals("Query") && f.getClazz()!=null && f.getClazz().equals("Varchar"))
				{					
					for (String filterValue : filterValues)
					{
						if (filterValue.indexOf("'")>=0)
						{
							cloneFilter = true;
							break;
						}
					}
				}
				Filter newFilter = null;
				if (cloneFilter)
				{
					newFilter = cloneAndUpdateFilter(f);
				}
				tsd.paramMap.put(f.getColumnName(), newFilter!=null?newFilter:f);
			}
		}
	}

	public static Filter getFilter(String name)
	{
		ThreadSpecificData tsd = getThreadSpecificData();
		if (tsd.paramMap != null)
			return tsd.paramMap.get(name);
		return null;
	}
	
	/**
	 * clone and return filter object with escaped parameter values
	 * @param filter
	 */
	private static Filter cloneAndUpdateFilter(Filter filter)
	{
		Filter clonedFilter=null;
		try 
		{
			clonedFilter = filter.clone();
			List<String> filterValues = clonedFilter.getValues();
			if (filterValues!=null && !filterValues.isEmpty())
			{
				List<String> newValues = new ArrayList<String>();
				for (String filterValue : filterValues)
				{
					filterValue = Util.escapeQuotesIfExists(filterValue);
					newValues.add(filterValue);
				}
				clonedFilter.setValues(newValues);
			}
		} 
		catch (CloneNotSupportedException e) 
		{
			clonedFilter = filter;
		}		
		return clonedFilter;
	}

	public static void setFilter(Filter filter)
	{
		ThreadSpecificData tsd = getThreadSpecificData();
		boolean cloneFilter = false;
		List<String> filterValues = filter.getValues();
		if (filterValues!=null && !filterValues.isEmpty() && filter.getPromptType()!=null && filter.getPromptType().equals("Query") && filter.getClazz()!=null && filter.getClazz().equals("Varchar"))
		{					
			for (String filterValue : filterValues)
			{
				if (filterValue.indexOf("'")>=0)
				{
					cloneFilter = true;
					break;
				}
			}
		}
		Filter newFilter = null;
		if (cloneFilter)
		{
			newFilter = cloneAndUpdateFilter(filter);
		}
		Filter f = newFilter!=null?newFilter : filter;
		String cname = f.getColumnName();
		if (tsd.params != null)
		{
			Filter existingFilter = getFilter(cname);
			if (existingFilter != null)
			{
				tsd.params.remove(existingFilter);
				tsd.paramMap.remove(cname);
			}
			tsd.params.add(f);
			tsd.paramMap.put(cname, f);
		} else
		{
			tsd.params = new ArrayList<Filter>();
			tsd.params.add(f);
			tsd.paramMap = new HashMap<String, Filter>();
			tsd.paramMap.put(cname, f);
		}
	}

	public Map<DatabaseConnection, List<TempTable>> getTempTableMap()
	{
		return tempTableMap;
	}

	public void setTempTableMap(Map<DatabaseConnection, List<TempTable>> tempTableMap)
	{
		this.tempTableMap = tempTableMap;
		if (rttt == null)
		{
			// Start a reaper thread
			rttt = new ReapTempTableThread();
			rttt.start();
		}
	}

	private class ReapTempTableThread extends Thread
	{
		public ReapTempTableThread()
		{
			this.setName("ReapTempTableThread - " + this.getId());
			setDaemon(true); // if the JVM wants to exit due to errors, let it
		}

		public void run()
		{
			long lastChange = System.currentTimeMillis();
			while (!isCancelled)
			{
				// exit after a while so we don't keep these threads around
				if (System.currentTimeMillis() - lastChange > 10 * 60 * 1000)
				{
					return;
				}
				try
				{
					Thread.sleep(1000 * 30 * 1); // sleep for a bit before checking again
				} catch (InterruptedException e1)
				{
					return;
				}
				if (getTempTableMap() == null || getTempTableMap().isEmpty())
				{
					continue;
				}
				for (DatabaseConnection dbc : getTempTableMap().keySet())
				{
					List<TempTable> toRemove = new ArrayList<TempTable>();
					List<TempTable> ttlist = getTempTableMap().get(dbc);
					synchronized (ttlist)
					{
						for (TempTable tt : ttlist)
						{
							int count = tt.referenceCount.get();
							if (count > 1 || (System.currentTimeMillis() - tt.lastAccessed < 3 * 60 * 1000) || tt.liveAccessTmpTable)
								continue;
							toRemove.add(tt);
						}
						ttlist.removeAll(toRemove);
					}
					if (toRemove.isEmpty())
					{
						continue;
					}
					for (TempTable tt : toRemove)
					{
						try
						{
							int index = tt.tableName.indexOf('.');
							String s = tt.tableName;
							if (index >= 0)
								s = s.substring(index + 1);
							Database.dropTable(getRepository(), dbc, s, true);
						} catch (Exception e)
						{
							logger.error(e);
						}
					}
					lastChange = System.currentTimeMillis();
				}
			}
		}
	}

	public synchronized AtomicInteger getQuerySynchronization(String key)
	{
		if (querySynchronization == null)
		{
			querySynchronization = new java.util.concurrent.ConcurrentHashMap<String, AtomicInteger>();
			AtomicInteger s = new AtomicInteger(1);
			querySynchronization.put(key, s);
			return s;
		}
		AtomicInteger s = querySynchronization.get(key);
		if (s == null)
		{
			s = new AtomicInteger(1);
			querySynchronization.put(key, s);
		} else
		{
			s.incrementAndGet();
		}
		return s;
	}

	public synchronized void removeSynchronization(String key)
	{
		if (querySynchronization == null)
		{
			return;
		}
		AtomicInteger s = querySynchronization.get(key);
		if (s == null)
		{
			return;
		} else
		{
			int result = s.decrementAndGet();
			if (result == 0)
				querySynchronization.remove(key);
		}
	}

	public SavedExpressions getPrivateSavedExpressions()
	{
		if (catalogDir != null)
		{
			// fill it
			File file = new File(catalogDir);
			file = new File(file, "private");
			file = new File(file, getUserName());
			file = new File(file, SavedExpressions.fileName);
			if (privateSavedExpressions == null || privateSavedExpressions.isOlder(file))
				privateSavedExpressions = new SavedExpressions(file);
		}
		return privateSavedExpressions;
	}

	public JasperReport getJasperReport(String key)
	{
		return jasperReportMap.get(key.toUpperCase());
	}

	public JasperReport setJasperReport(String key, JasperReport jasperReport)
	{
		return jasperReportMap.put(key.toUpperCase(), jasperReport);
	}

	public void clearJasperReport(String key)
	{
		jasperReportMap.remove(key.toUpperCase());
	}

	public Repository getRepository()
	{
		return repository;
	}

	public Repository setRepository(Repository repository)
	{
		Repository oldRepository = this.repository;
		this.repository = repository;
		return oldRepository;
	}

	public static void setDashletPrompts(String dashletPrompts)
	{
		ThreadSpecificData tsd = getThreadSpecificData();
		tsd.dashletPrompts = dashletPrompts;
	}

	public static String getDashletPrompts()
	{
		ThreadSpecificData tsd = getThreadSpecificData();
		return tsd.dashletPrompts;
	}

	public static void setPdf(boolean isPdf)
	{
		ThreadSpecificData tsd = getThreadSpecificData();
		tsd.isPdf = isPdf;
	}

	public static boolean isPdf()
	{
		ThreadSpecificData tsd = getThreadSpecificData();
		return tsd.isPdf;
	}

	public static void setPrint(boolean isPrint)
	{
		ThreadSpecificData tsd = getThreadSpecificData();
		tsd.isPrint = isPrint;
	}

	public static boolean isPrint()
	{
		ThreadSpecificData tsd = getThreadSpecificData();
		return tsd.isPrint;
	}

	public static void setRenderer(Renderer r)
	{
		ThreadSpecificData tsd = getThreadSpecificData();
		tsd.renderer = r;
	}

	public static Renderer getRenderer()
	{
		ThreadSpecificData tsd = getThreadSpecificData();
		return tsd.renderer;
	}
	
	public static void setMaxRowCount(int count) {
		ThreadSpecificData tsd = getThreadSpecificData();
		tsd.maxRowCount = count;
	}
	
	public static int getMaxRowCount() {
		ThreadSpecificData tsd = getThreadSpecificData();
		return tsd.maxRowCount;
	}

	public static void setMaxPageCount(int count) {
		ThreadSpecificData tsd = getThreadSpecificData();
		tsd.maxPageCount = count;
	}
	
	public static int getMaxPageCount() {
		ThreadSpecificData tsd = getThreadSpecificData();
		return tsd.maxPageCount;
	}

	public Map<Long, JasperPrint> getDesignerPageCache()
	{
		return designerPageCache;
	}

	public Object getDefaultReportProperties()
	{
		return defaultReportProperties;
	}

	public void setDefaultReportProperties(Object defaultReportProperties)
	{
		this.defaultReportProperties = defaultReportProperties;
	}

	public boolean isLoadingDefaultReportProperties()
	{
		return isLoadingDefaultReportProperties;
	}

	public void setLoadingDefaultReportProperties(boolean isLoadingDefaultReportProperties)
	{
		this.isLoadingDefaultReportProperties = isLoadingDefaultReportProperties;
	}
	
	public static String getDashboardParams()
	{
		ThreadSpecificData tsd = getThreadSpecificData();
		return tsd.dashboardParams;
	}
	public static void setDashboardParams(String dashboardParams)
	{
		ThreadSpecificData tsd = getThreadSpecificData();
		tsd.dashboardParams = dashboardParams;
	}
	public static String getDashboardParamsSeperator()
	{
		ThreadSpecificData tsd = getThreadSpecificData();
		return tsd.dashboardParamsSeperator;
	}
	public static void setDashboardParamsSeperator(String dashboardParamsSeperator)
	{
		ThreadSpecificData tsd = getThreadSpecificData();
		tsd.dashboardParamsSeperator = dashboardParamsSeperator;
	}
	
	public static String getSessionVars()
	{
		ThreadSpecificData tsd = getThreadSpecificData();
		return tsd.sessionVars;
	}
	public static void setSessionVars(String sessionVars)
	{
		ThreadSpecificData tsd = getThreadSpecificData();
		tsd.sessionVars = sessionVars;
	}
	
	public static boolean isUseOlapIndentation() {
		ThreadSpecificData tsd = getThreadSpecificData();
		return tsd.olapUseIndentation;
	}
	
	public static void setUseOlapIndentation(boolean f) {
		ThreadSpecificData tsd = getThreadSpecificData();
		tsd.olapUseIndentation = f;
	}
	
}
