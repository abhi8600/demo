/**
 * $Id: Procedure.java,v 1.33 2010-12-20 12:48:03 mpandit Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.query.AbstractQueryString;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.JasperDataSourceProvider;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.util.BaseException;

/**
 * @author Brad Peters
 * 
 */
public class Procedure extends Transformation implements Serializable
{
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(Procedure.class);
	public String Name;
	public String Body;

	public Procedure(Element e, Namespace ns)
	{
		Name = e.getChildText("Name", ns);
		Body = e.getChildText("Body", ns);
	}

	public Procedure(String name, String body)
	{
		Name = name;
		Body = body;
	}
	
	public Element getProcedureElement(Namespace ns)
	{
		Element e = new Element("Procedure",ns);
			
		Element child = new Element("ExecuteAfter",ns);
		child.setText(String.valueOf(ExecuteAfter));
		e.addContent(child);
		
		child = new Element("Name",ns);
		child.setText(Name);
		e.addContent(child);
		
		child = new Element("Body",ns);
		child.setText(Body);
		e.addContent(child);		
		
		return e;
	}

	/**
	 * Execute procedure
	 * 
	 * @param r
	 * @param dc
	 * @param s
	 * @param jdsp
	 * @return
	 * @throws ScriptException 
	 */
	public void execute(Repository r, DatabaseConnection dc, Session s, JasperDataSourceProvider jdsp) throws BaseException, SQLException, IOException, CloneNotSupportedException
	{
		Connection conn = dc.ConnectionPool.getConnection();
		Statement stmt = conn.createStatement();
		String pcode = getPhysicalString(r, s, jdsp, stmt);
		pcode = r.replaceVariables(null, pcode);
		logger.debug(TableName + " - Executing procedure code (" + Name + "):\n" + pcode);
		stmt.executeUpdate(pcode);
		int rowsAffected = stmt.getUpdateCount();
		do
		{
			try
			{
				stmt.getMoreResults();
			} catch (SQLException e)
			{
				if (e.getCause() != null)
				{
					logger.error(e.getCause().toString(), e);
				} else
				{
					logger.error(e.toString(), e);
				}
				break;
			}
			rowsAffected = stmt.getUpdateCount();
		} while (-1 != rowsAffected);
		stmt.close();
	}

	public String convertToPhysical(Repository r, Connection conn) throws SQLException, IOException, BaseException, CloneNotSupportedException
	{
		Statement stmt = conn.createStatement();
		String res = getPhysicalString(r, null, null, stmt);
		stmt.close();
		return res;
	}

	private String getPhysicalString(Repository r, Session s, JasperDataSourceProvider jdsp, Statement stmt)
	throws IOException, BaseException, SQLException, CloneNotSupportedException
	{
		String pcode = processReplacements(r, s, stmt, jdsp, Body);
		AbstractQueryString aqs = AbstractQueryString.getInstance(r, pcode, r.isUseNewQueryLanguage());
		pcode = aqs.replaceQueryString(pcode, true, null, false, false, null, null, null);
		pcode = processReplacements(r, s, stmt, jdsp, pcode);
		return pcode;
	}

	private String processReplacements(Repository r, Session s, Statement stmt, JasperDataSourceProvider jdsp, String pcode)
	{
		for (StagingTable st : r.getStagingTables())
		{
			pcode = st.getReplacedString(pcode, null);
		}
		pcode = r.replaceVariables(s, pcode, s != null, stmt, jdsp);
		if (s != null)
		{
			pcode = r.replaceVariables(null, pcode, false, stmt, jdsp);
		}
		return pcode;
	}
}
