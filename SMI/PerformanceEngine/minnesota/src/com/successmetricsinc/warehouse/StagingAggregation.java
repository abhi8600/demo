/**
 * $Id: StagingAggregation.java,v 1.3 2010-12-20 00:36:15 bpeters Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse;

import java.io.Serializable;

import org.jdom.Element;
import org.jdom.Namespace;
import com.successmetricsinc.Repository;

/**
 * @author Brad Peters
 * 
 */
public class StagingAggregation implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String Type;
	public String Default;
	public String[] ValueList;
	public String[][] SetList;

	public StagingAggregation(Element e, Namespace ns)
	{
		Type = e.getChildTextTrim("Type", ns);
		Default = e.getChildTextTrim("Default", ns);
		ValueList = Repository.getStringArrayChildren(e, "ValueList", ns);
		SetList = Repository.getDoubleStringArrayChildren(e, "SetList", ns);
	}
	
	public Element getStagingAggregationElement(Namespace ns)
	{
		Element e = new Element("StagingAggregation",ns);
		
		Element child = new Element("Type",ns);
		child.setText(Type);
		e.addContent(child);
		
		child = new Element("Default",ns);
		child.setText(Default);
		e.addContent(child);
		
		if (ValueList!=null && ValueList.length>0)
		{
			e.addContent(Repository.getStringArrayElement("ValueList", ValueList, ns));
		}
		
		if (SetList!=null && SetList.length>0)
		{
			child = new Element("SetList",ns);
			for (String[] lvl1 : SetList)
			{
				Element arrayOfString = new Element("ArrayOfString",ns);
				for (String lvl2 : lvl1)
				{
					Element stringElement = new Element("string",ns);
					stringElement.setText(lvl2);
					arrayOfString.addContent(stringElement);
				}
				child.addContent(arrayOfString);
			}
			e.addContent(child);
		}
				
		return e;
	}
}
