/**
 * $Id: ScriptGroup.java,v 1.5 2011-09-24 01:10:50 ricks Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse;

import java.io.Serializable;
import java.util.List;

import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.util.RepositoryException;

public class ScriptGroup implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String Name;
	public Script [] Scripts;
	private boolean isScriptTag;
	
	public ScriptGroup(Element e, Namespace ns) throws RepositoryException
	{
		Name = e.getChildTextTrim("Name", ns);
		Element scriptsElem = e.getChild("Scripts", ns);
		isScriptTag = e.getChild("Scripts",ns)!=null?true:false;
		if (scriptsElem != null)
		{
			@SuppressWarnings("rawtypes")
			List l = scriptsElem.getChildren();
			Scripts = new Script[l.size()];
			for (int count = 0; count < l.size(); count++)
			{
				Script scr = new Script(this, (Element) l.get(count), ns);
				Scripts[count] = scr;
			}
		}
	}
	
	public Element getScriptGroupElement(Namespace ns)
	{
		Element e = new Element("ScriptGroup",ns);
		
		Element child = new Element("Name",ns);
		child.setText(Name);
		e.addContent(child);
		
		if (Scripts!=null && Scripts.length>0)
		{
			child = new Element("Scripts",ns);
			for (Script script : Scripts)
			{
				child.addContent(script.getScriptElement(ns));
			}
			e.addContent(child);
		}
		else if (isScriptTag)
		{
			child = new Element("Scripts",ns);
			e.addContent(child);
		}
				
		return e;
	}
	
	public String toString()
	{
		return Name;
	}
}
