package com.successmetricsinc.warehouse;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.DimensionTable;
import com.successmetricsinc.query.Level;
import com.successmetricsinc.query.LevelKey;
import com.successmetricsinc.util.UnterminatedQuoteException;
import com.successmetricsinc.warehouse.SourceFile.FileFormatType;

public class Sample
{
	private static Logger logger = Logger.getLogger(Sample.class);
	private Repository r;
	private String path;
	private List<StagingTable> stlist = new ArrayList<StagingTable>();
	private Level l;
	private LevelKey lk;
	private Set<String> sampleSet;

	public static class SampleMetadata
	{
		public List<String> dimensions;
		public List<String> levels;
		public List<Integer> numSamples;
		public List<Integer> fileSize;
		public List<String> mapDimensions;
		public List<String> mapLevels;
		public List<String> mapSources;

		public SampleMetadata()
		{
			dimensions = new ArrayList<String>();
			levels = new ArrayList<String>();
			numSamples = new ArrayList<Integer>();
			fileSize = new ArrayList<Integer>();
			mapDimensions = new ArrayList<String>();
			mapLevels = new ArrayList<String>();
			mapSources = new ArrayList<String>();
		}
	}

	public static void deleteSamples(String path)
	{
		File fi = new File(path);
		if (fi.exists() && fi.isDirectory())
		{
			for (File sf : fi.listFiles())
			{
				if (sf.getName().endsWith(".sample"))
				{
					sf.delete();
				} else if (sf.getName().endsWith(".samplecache"))
				{
					sf.delete();
				} else if (sf.getName().endsWith(".cnf"))
				{
					sf.delete();
				}
			}
		}
	}

	public static SampleMetadata getSampleMetadata(String repositoryPath, Repository r)
	{
		SampleMetadata smd = new SampleMetadata();
		File fi = new File(repositoryPath+File.separator+"data");
		if (fi.exists() && fi.isDirectory())
		{
			SampleSettings ss = getSettings(fi.getPath());
			for (SampleMap sm : ss.map)
			{
				smd.mapDimensions.add(sm.Dimension);
				smd.mapLevels.add(sm.Level);
				smd.mapSources.add(sm.StagingTableName);
			}
			for (File sf : fi.listFiles())
			{
				String name = sf.getName();
				if (name.endsWith(".sample"))
				{
					name = name.substring(0, name.length() - 7);
					int index = name.indexOf('.');
					if (index > 0)
					{
						String dimension = name.substring(0, index);
						String level = name.substring(index + 1);
						BufferedReader reader = null;
						try
						{
							reader = new BufferedReader(new InputStreamReader(new FileInputStream(sf), r.getDefaultConnection().getBulkLoadCharacterEncoding()));
							String line = reader.readLine();							
							smd.dimensions.add(dimension);
							smd.levels.add(level);
							int cnumSamples = 0;
							int cFileSize = 0;
							if(line != null)
							{
								String[] values = line.split("[|]");
								try {
									cnumSamples = Integer.parseInt(values[0]);
									cFileSize = Integer.parseInt(values[1]);
								} catch (NumberFormatException e) {
									logger.error(e);
								}
							}
							smd.numSamples.add(cnumSamples);
							smd.fileSize.add(cFileSize);
						} catch (IOException e)
						{
							logger.error(e, e);
						} finally
						{
							if (reader != null)
								try
								{
									reader.close();
								} catch (IOException e)
								{
									logger.debug(e, e);
								}
						}
					}
				}
			}
		}
		return smd;
	}

	public static void showSamples(String path, Repository r)
	{
		File fi = new File(path);
		if (!fi.exists() || !fi.isDirectory())
			return;
		SampleMetadata smd = getSampleMetadata(path, r);
		for (int i = 0; i < smd.mapDimensions.size(); i++)
		{
			String s = smd.mapSources.get(i);
			if (s.startsWith("ST_"))
			{
				StagingTable st = r.findStagingTable(s);
				if (st != null)
					s = st.getDisplayName();
			}
			System.out.println("Dimension: " + smd.mapDimensions.get(i) + ", Level: " + smd.mapLevels.get(i) + ", Source: " + s);
		}
		for (int i = 0; i < smd.dimensions.size(); i++)
		{
			System.out.println("Dimension: " + smd.dimensions.get(i) + ", Level: " + smd.levels.get(i) + ", Samples: " + smd.numSamples.get(i)
					+ ", Source File Size: " + smd.fileSize.get(i));
		}
	}

	public Sample(Repository r, String path, String dimension, String level)
	{
		this.r = r;
		this.path = path;
		sampleSet = new HashSet<String>(10000);
		l = r.findLevel(dimension, level);
		if (l == null)
		{
			logger.error("Level not found: " + dimension + "." + level);
			return;
		}
		lk = l.getNaturalKey();
		if (lk == null)
		{
			logger.error("Natural key not found for level: " + dimension + "." + level);
			return;
		}
		for (StagingTable st : r.getStagingTables())
		{
			if (st.Disabled)
				continue;
			if (st.SourceType != StagingTable.SOURCE_FILE)
				continue;
			boolean foundlk = true;
			// Make sure level key exists in this table
			for (String cname : lk.ColumnNames)
			{
				boolean foundcname = false;
				for (StagingColumn sc : st.Columns)
				{
					if (sc.Name.equals(cname) && sc.SourceFileColumn != null && sc.SourceFileColumn.length() > 0)
					{
						foundcname = true;
						break;
					}
				}
				if (!foundcname)
				{
					foundlk = false;
					break;
				}
			}
			if (!foundlk)
				continue;
			st.setRepository(r);
			stlist.add(st);
		}
	}

	public static List<Integer> getIndexes(LevelKey lk, StagingTable st, SourceFile sf)
	{
		List<StagingColumn> sclist = new ArrayList<StagingColumn>();
		for (String cname : lk.ColumnNames)
		{
			for (StagingColumn sc : st.Columns)
			{
				if (sc.Name.equals(cname) && sc.SourceFileColumn != null && sc.SourceFileColumn.length() > 0)
				{
					sclist.add(sc);
					break;
				}
			}
		}
		List<Integer> indexes = new ArrayList<Integer>();
		for (StagingColumn sc : sclist)
		{
			for (int i = 0; i < sf.Columns.length; i++)
			{
				if (sc.SourceFileColumn.equals(sf.Columns[i].Name))
				{
					indexes.add(i);
					break;
				}
			}
		}
		if (indexes.size() < sclist.size())
			return null;
		return indexes;
	}

	public boolean createSample(String stagingTableName, double percent) throws Exception
	{
		DatabaseConnection dc = r.getDefaultConnection();
		percent /= 100;
		boolean result = true;
		int linecount = 0;
		StringBuilder sb = new StringBuilder(2000);
		for (StagingTable st : stlist)
		{
			if (!st.Name.equals(stagingTableName))
				continue;
			SourceFile sf = r.findSourceFile(st.SourceFile);
			char[] separr = sf.Separator.toCharArray();
			StagingTableBulkLoadInfo stbl = st.getStagingTableBulkLoadInfo(sf, dc, r);
			List<SourceFile.BirstDataSource> sourceFiles = sf.getPhysicalSourceFiles(path, false, r, false, dc, null);
			Map<DimensionTable, List<StagingColumn>> scmap = st.getStagingColumnMap(r);
			for (SourceFile.BirstDataSource file : sourceFiles)
			{
				// Find the column indexes of all the level key columns
				List<Integer> indexes = getIndexes(lk, st, sf);
				if (indexes == null)
					continue;
				String name = file.getAbsolutePath();
				try
				{
					BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(name), sf.getEncoding()));
					StringBuilder line = null;
					for (int i = 0; i < sf.IgnoredRows + (sf.HasHeaders ? 1 : 0); i++)
						line = Bulkload.readLine(sb, sf, separr, br, sf.Type != FileFormatType.FixedWidth, sf.ForceNumColumns ? sf.Columns.length - 1 : 0,
								sf.OnlyQuoteAfterSeparator);
					int lineno = 1;
					LinkedBlockingQueue<Line> last = null;
					if (sf.IgnoredLastRows > 0)
						last = new LinkedBlockingQueue<Line>(sf.IgnoredLastRows);
					AtomicLong warningCount = new AtomicLong(0);
					AtomicLong errorCount = new AtomicLong(0);
					AtomicLong numFailedRecords = new AtomicLong();
					AtomicBoolean skipRunning = new AtomicBoolean(false);
					LineProcessor lp = new LineProcessor(stbl, warningCount, errorCount, r, sf, st, scmap, null, null, sf.ReplaceWithNull, sf.SkipRecord,
							r.getNumThresholdFailedRecords(), numFailedRecords, skipRunning, dc);
					Random rand = new Random();
					while ((line = Bulkload.readLine(sb, sf, separr, br, sf.Type != FileFormatType.FixedWidth, sf.ForceNumColumns ? sf.Columns.length - 1 : 0,
							sf.OnlyQuoteAfterSeparator)) != null)
					{
						linecount++;
						double d = rand.nextDouble();
						if (d > percent)
							continue;
						Line l = new Line();
						l.number = lineno++;
						l.line = line.toString();
						LineProcessorResult lpr = null;
						try
						{
							if (last == null)
							{
								lpr = lp.processLine(l);
							} else
							{
								if (!last.offer(l)) // handle ignoring the last N rows
								{
									lpr = lp.processLine(last.remove());
									last.put(l);
								}
							}
						} catch (Exception ex)
						{
							logger.debug(ex.getMessage(), ex);
						}
						if (lpr != null)
						{
							StringBuilder key = new StringBuilder();
							for (Integer index : indexes)
							{
								if (key.length() > 0)
									key.append('|');
								key.append(lpr.pline[index]);
							}
							String s = key.toString().trim();
							if (s.length() > 0)
								sampleSet.add(s);
						}
					}
				} catch (UnsupportedEncodingException e)
				{
					logger.error(e, e);
				} catch (FileNotFoundException e)
				{
					logger.error(e, e);
				} catch (IOException e)
				{
					logger.error(e, e);
				} catch (UnterminatedQuoteException e)
				{
					logger.error(e, e);
				}
			}
		}
		if (l == null || l.Hierarchy == null || l.Name == null)
		{
			logger.error("Bad level name specified");
			return false;
		}
		File outputFile = new File(path + "\\" + l.Hierarchy.DimensionName + "." + l.Name + ".sample");
		if (sampleSet.size() > 0)
		{
			try
			{
				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile, false), r.getDefaultConnection()
						.getBulkLoadCharacterEncoding()));
				bw.write(sampleSet.size() + "|" + linecount);
				bw.newLine();
				for (String s : sampleSet)
				{
					bw.write(s);
					bw.newLine();
				}
				bw.close();
			} catch (IOException e)
			{
				logger.error(e, e);
				result = false;
			}
		}
		if (!result && outputFile.exists())
			outputFile.delete();
		return result;
	}

	public boolean deriveSample(String sourceDimension, String sourceLevel, String referenceStagingTableName) throws Exception
	{
		Sample sourceSample = new Sample(r, path, sourceDimension, sourceLevel);
		Set<String> sourceSet = sourceSample.getSampleSet();
		if (sourceSet == null || sourceSet.size() == 0)
			return false;
		Level sLevel = r.findDimensionLevel(sourceDimension, sourceLevel);
		if (sLevel == null)
			return false;
		LevelKey skey = sLevel.getNaturalKey();
		if (skey == null)
			return false;
		StagingTable sourceStagingTable = r.findStagingTable(referenceStagingTableName);
		if (sourceStagingTable == null)
			return false;
		SourceFile sourceFile = r.findSourceFile(sourceStagingTable.SourceFile);
		if (sourceFile == null)
			return false;
		List<Integer> sourceIndexes = getIndexes(skey, sourceStagingTable, sourceFile);
		if (sourceIndexes == null)
			return false;
		DatabaseConnection dc = r.getDefaultConnection();
		boolean result = true;
		int linecount = 0;
		StringBuilder sb = new StringBuilder(2000);
		for (StagingTable st : stlist)
		{
			if (!st.Name.equals(referenceStagingTableName))
				continue;
			SourceFile sf = r.findSourceFile(st.SourceFile);
			char[] separr = sf.Separator.toCharArray();
			StagingTableBulkLoadInfo stbl = st.getStagingTableBulkLoadInfo(sf, dc, r);
			List<SourceFile.BirstDataSource> sourceFiles = sf.getPhysicalSourceFiles(path, false, r, false, dc, null);
			Map<DimensionTable, List<StagingColumn>> scmap = st.getStagingColumnMap(r);
			for (SourceFile.BirstDataSource file : sourceFiles)
			{
				// Find the column indexes of all the level key columns
				List<Integer> indexes = getIndexes(lk, st, sf);
				if (indexes == null)
					continue;
				String name = file.getAbsolutePath();
				try
				{
					BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(name), sf.getEncoding()));
					StringBuilder line = null;
					for (int i = 0; i < sf.IgnoredRows + (sf.HasHeaders ? 1 : 0); i++)
						line = Bulkload.readLine(sb, sf, separr, br, sf.Type != FileFormatType.FixedWidth, sf.ForceNumColumns ? sf.Columns.length - 1 : 0,
								sf.OnlyQuoteAfterSeparator);
					int lineno = 1;
					LinkedBlockingQueue<Line> last = null;
					if (sf.IgnoredLastRows > 0)
						last = new LinkedBlockingQueue<Line>(sf.IgnoredLastRows);
					AtomicLong warningCount = new AtomicLong(0);
					AtomicLong errorCount = new AtomicLong(0);
					AtomicLong numFailedRecords = new AtomicLong();
					AtomicBoolean skipRunning = new AtomicBoolean(false);
					LineProcessor lp = new LineProcessor(stbl, warningCount, errorCount, r, sf, st, scmap, null, null, sf.ReplaceWithNull, sf.SkipRecord,
							r.getNumThresholdFailedRecords(), numFailedRecords, skipRunning, dc);
					while ((line = Bulkload.readLine(sb, sf, separr, br, sf.Type != FileFormatType.FixedWidth, sf.ForceNumColumns ? sf.Columns.length - 1 : 0,
							sf.OnlyQuoteAfterSeparator)) != null)
					{
						linecount++;
						Line l = new Line();
						l.number = lineno++;
						l.line = line.toString();
						LineProcessorResult lpr = null;
						try
						{
							if (last == null)
							{
								lpr = lp.processLine(l);
							} else
							{
								if (!last.offer(l)) // handle ignoring the last N rows
								{
									lpr = lp.processLine(last.remove());
									last.put(l);
								}
							}
						} catch (Exception ex)
						{
							logger.debug(ex.getMessage(), ex);
						}
						if (lpr != null)
						{
							StringBuilder sourceKey = new StringBuilder();
							StringBuilder key = new StringBuilder();
							for (Integer sourceIndex : sourceIndexes)
							{
								if (sourceKey.length() > 0)
									sourceKey.append('|');
								sourceKey.append(lpr.pline[sourceIndex]);
							}
							if (sourceSet.contains(sourceKey.toString()))
							{
								for (Integer index : indexes)
								{
									if (key.length() > 0)
										key.append('|');
									key.append(lpr.pline[index]);
								}
								String s = key.toString().trim();
								if (s.length() > 0)
								{
									sampleSet.add(s);
								}
							}
						}
					}
				} catch (UnsupportedEncodingException e)
				{
					logger.error(e, e);
				} catch (FileNotFoundException e)
				{
					logger.error(e, e);
				} catch (IOException e)
				{
					logger.error(e, e);
				} catch (UnterminatedQuoteException e)
				{
					logger.error(e, e);
				}
			}
		}
		File outputFile = new File(path + "\\" + l.Hierarchy.DimensionName + "." + l.Name + ".sample");
		if (sampleSet.size() > 0)
		{
			try
			{
				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile, false), r.getDefaultConnection()
						.getBulkLoadCharacterEncoding()));
				bw.write(sampleSet.size() + "|" + linecount);
				bw.newLine();
				for (String s : sampleSet)
				{
					bw.write(s);
					bw.newLine();
				}
				bw.close();
			} catch (IOException e)
			{
				logger.error(e, e);
				result = false;
			}
		}
		if (!result && outputFile.exists())
			outputFile.delete();
		return result;
	}

	public Set<String> getSampleSet()
	{
		try
		{
			File f = new File(path + File.separator + l.Hierarchy.DimensionName + "." + l.Name + ".sample");
			if (!f.exists())
				return new HashSet<String>(0);
			sampleSet = new HashSet<String>(10000);
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(f), r.getDefaultConnection().getBulkLoadCharacterEncoding()));
			// ignore first line (with stats)
			String line = reader.readLine();
			while ((line = reader.readLine()) != null)
				sampleSet.add(line.trim());
			reader.close();
			return sampleSet;
		} catch (Exception e)
		{
			logger.error(e, e);
		}
		return new HashSet<String>(0);
	}

	public static SampleSettings getSettings(String path)
	{
		File f = new File(path + File.separator + "samples.cnf");
		SampleSettings ss = new SampleSettings();
		if (f.exists())
		{
			try
			{
				FileInputStream in = new FileInputStream(f);
				ObjectInputStream ois = new ObjectInputStream(in);
				ss = (SampleSettings) ois.readObject();
				ois.close();
				in.close();
			} catch (IOException ex)
			{
				logger.error(ex);
			} catch (ClassNotFoundException ex)
			{
				logger.error(ex, ex);
			}
		}
		return ss;
	}

	public static void saveSettings(String path, SampleSettings ss)
	{
		File f = new File(path + File.separator + "samples.cnf");
		try
		{
			FileOutputStream out = new FileOutputStream(f, false);
			ObjectOutputStream oos = new ObjectOutputStream(out);
			oos.writeObject(ss);
			oos.close();
			out.close();
		} catch (IOException ex)
		{
			logger.error(ex, ex);
		}
	}

	public void applySample(String stagingTableName)
	{
		SampleSettings ss = getSettings(path);
		ss.addMap(l.Hierarchy.DimensionName, l.Name, stagingTableName);
		saveSettings(path, ss);
	}

	public void removeSample(String stagingTableName)
	{
		SampleSettings ss = getSettings(path);
		ss.removeMap(l.Hierarchy.DimensionName, l.Name, stagingTableName);
		saveSettings(path, ss);
	}
}
