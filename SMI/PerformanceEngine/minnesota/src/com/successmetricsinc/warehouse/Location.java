package com.successmetricsinc.warehouse;

import java.io.Serializable;

import org.jdom.Element;
import org.jdom.Namespace;

public class Location implements Serializable {

	private String LoadGroupName;
	private int x;
	private int y;
	
	public Location(Element e,Namespace ns)
	{
		LoadGroupName = e.getChildText("LoadGroupName",ns);
		String s  = e.getChildText("x",ns);
		if (s!=null)
			x = Integer.parseInt(s);
		s  = e.getChildText("y",ns);
		if (s!=null)
			y = Integer.parseInt(s);
	} 
	
	public Element getLocationElement(Namespace ns)
	{
		Element e = new Element("Location",ns);
		
		Element child = new Element("LoadGroupName",ns);
		child.setText(LoadGroupName);
		e.addContent(child);
		
		child = new Element("x",ns);
		child.setText(String.valueOf(x));
		e.addContent(child);
		
		child = new Element("y",ns);
		child.setText(String.valueOf(y));
		e.addContent(child);
				
		return e;
	}
	
}
