/**
 *
 * Copyright (C) 2007-2009 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.ExecuteSteps.Step;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.MeasureTable;

public class RedshiftFactLoader extends FactLoader{
	private static Logger logger = Logger.getLogger(RedshiftFactLoader.class);

	public RedshiftFactLoader(Repository r, DatabaseConnection dconn,
			LoadWarehouse lw, Step step, List<StagingTable> stList) {
		super(r, dconn, lw, step, stList);
 	}

	@Override
	protected void processForIncrementalSnapshotFact(Connection conn, MeasureTable mt, Set<String> mtKeys, Statement stmt, int loadID, StagingTable st,
			Set<String> logicalTableKeys,String processingGroup) throws SQLException
	{
		super.processForIncrementalSnapshotFact(conn, mt, mtKeys, stmt, loadID, st, logicalTableKeys, processingGroup);
	}
}
