/**
 * $Id: StagingFilter.java,v 1.7 2010-12-20 12:48:03 mpandit Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse;

import java.io.Serializable;

import org.jdom.Element;
import org.jdom.Namespace;

public class StagingFilter implements Serializable
{
	private static final long serialVersionUID = 1L;
	public String Name;
	public String Condition;
	public static final StagingFilter NoFilter = new StagingFilter("NO_FILTER", null);

	public StagingFilter(String name, String Condition)
	{
	}

	public StagingFilter(Element e, Namespace ns)
	{
		Name = e.getChildTextTrim("Name", ns);
		Condition = e.getChildTextTrim("Condition", ns);
	}
	
	public Element getStagingFilterElement(Namespace ns)
	{
		Element e = new Element("StagingFilter",ns);
		
		Element child = new Element("Name",ns);
		child.setText(Name);
		e.addContent(child);
		
		child = new Element("Condition",ns);
		child.setText(Condition);
		e.addContent(child);
				
		return e;
	}
}
