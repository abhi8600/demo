package com.successmetricsinc.warehouse.partition;

import java.util.ArrayList;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import com.successmetricsinc.Repository;
import com.successmetricsinc.warehouse.SourceColumn;
import com.successmetricsinc.warehouse.SourceFile;

public class SourceFileSplitter
{
	private static Logger logger = Logger.getLogger(SourceFileSplitter.class);

	private Repository r;
	private String sourceFilePath;
	private Map<SourceFile, List<SourceColumn>> sourceFileToPartitionColumnsMap;
	private Map<SourceFile, List<SourceColumn>> sourceFileToChildColumnsMap;
	private Map<SourceFile, String []> sourceFileToPartitionPrefixesMap;
	private Map<SourceFile, PartitionConnectionLoadInfo []> sourceFileToPartitionConnectionLoadInfoMap;
	private Map<WarehousePartition, List<SourceFile>> warehousePartitionToSourceFileMap;
	private Map<SourceFile, List<WarehousePartition>> sourceFileToWarehousePartitionMap;
	
	public SourceFileSplitter(Repository r, String sourceFilePath)
	{
		this.r = r;
		this.sourceFilePath = sourceFilePath;
		init();
	}
	
	private void init()
	{
		sourceFileToPartitionColumnsMap = new HashMap<SourceFile, List<SourceColumn>>();
		sourceFileToChildColumnsMap = new HashMap<SourceFile, List<SourceColumn>>();
		sourceFileToPartitionPrefixesMap = new HashMap<SourceFile, String []>();
		sourceFileToPartitionConnectionLoadInfoMap = new HashMap<SourceFile, PartitionConnectionLoadInfo []> ();
		warehousePartitionToSourceFileMap = new HashMap<WarehousePartition, List<SourceFile>>();
		sourceFileToWarehousePartitionMap = new HashMap<SourceFile, List<WarehousePartition>>();
		
		List <WarehousePartition> wpList = r.getWarehousePartitions();
		if(wpList == null)
		{
			logger.debug("No warehouse partitions defined in repository.");
			return;
		}
		
		//Find child partitions and establish parent-child relationships
		for(WarehousePartition wp : wpList)
		{
			if(wp.PartitionColumnNames == null || wp.PartitionColumnNames.length == 0)
			{
				logger.error("No partition column names provided for warehouse partition " + wp.Name);
				wp.IsInvalid = true;
				continue;
			}
			if((wp.ChildPartitionColumnNames == null) || (wp.ChildPartitionColumnNames.length == 0))
			{
				continue;
			}
			for(WarehousePartition wp2 : wpList)
			{
				if(wp == wp2)
				{
					continue;
				}
				if(wp2.PartitionColumnNames == null || wp2.PartitionColumnNames.length == 0)
				{
					logger.error("No partition column names provided for warehouse partition " + wp2.Name);
					wp.IsInvalid = true;
					continue;
				}
				boolean found = false;				
				for(String childColumn : wp.ChildPartitionColumnNames)
				{
					found = false;
					for(String partitionColumn : wp2.PartitionColumnNames)
					{
						if((childColumn != null) && (partitionColumn != null) && (childColumn.equals(partitionColumn)))
						{
							found = true;
							break;
						}
					}
					if(!found)
					{
						break;
					}
				}
				if(found)
				{
					if(wp2.Parent != null)
					{
						logger.error("Multiple parents detected for warehouse partition " + wp2.Name);
						wp2.IsInvalid = true;
					}
					else
					{
						if(wp.Children == null)
						{
							wp.Children = new ArrayList<WarehousePartition>();
						}
						wp.Children.add(wp2);
						wp2.Parent = wp;
					}
				}
			}
		}		
		
		for(WarehousePartition parentwp : wpList)
		{
			if(parentwp.Parent != null)
			{
				continue;
			}
			List<WarehousePartition> hierWpList = new ArrayList<WarehousePartition>();
			try
			{
				parentwp.getAllChildren(hierWpList);
			}
			catch(Exception e)
			{
				logger.error(e.toString(), e);
				continue;
			}
			hierWpList.add(0, parentwp);
			//Add to dependency map only if there are interdependent warehouse partitions.
			boolean addToMap = hierWpList.size() > 1; 
			for(WarehousePartition wp : hierWpList)
			{
				String wpName = wp.getName();
				String sfName = wp.getSourceName();
				String [] columnNames  = wp.getPartitionColumnNames();
				String [] childColumnNames = wp.getChildPartitionColumnNames();
				if(columnNames == null || columnNames.length == 0)
				{
					logger.error("No column names provided for warehouse partition " + wpName);
					continue;
				}
				if(sfName == null || sfName.isEmpty())
				{
					//This warehouse partition definition applies to all source files.
					for(SourceFile sf : r.getSourceFiles())
					{
						List<SourceColumn> scList = getSourceColumnList(wpName, sf, columnNames, false);
						if(scList == null)
						{
							continue;
						}
						List<SourceColumn> childscList = getSourceColumnList(wpName, sf, childColumnNames, false);
						if(childscList == null)
						{
							childscList = new ArrayList<SourceColumn>();
						}
						if(sourceFileToChildColumnsMap.containsKey(sf))
						{
							List<SourceColumn> tempChildscList = sourceFileToChildColumnsMap.get(sf);
							if((tempChildscList != null) && (tempChildscList.size() > 0))
							{
								logger.warn("Not overriding valid child column list available for source file " + sf.FileName);
								continue;
							}
							else
							{
								logger.warn("Overriding child column list for source file " + sf.FileName);
							}
						}

						if(sourceFileToPartitionColumnsMap.containsKey(sf))
						{
							logger.warn("Overriding column list for source file " + sf.FileName);
						}

						String [] ppArr = wp.getPartitionPrefixes(r);
						if(ppArr == null || ppArr.length == 0)
						{
							logger.error("No partition prefixes found for source file: " + sfName + ". Warehouse partition will not be processed: " + wpName);
							continue;
						}

						PartitionConnectionLoadInfo [] pcliArr = new PartitionConnectionLoadInfo[ppArr.length];
						for(int idx = 0; idx < ppArr.length; idx++)
						{
							pcliArr[idx] = r.getPartitionConnectionLoadInfo(ppArr[idx]);
						}
						
						sourceFileToPartitionColumnsMap.put(sf, scList);
						sourceFileToChildColumnsMap.put(sf, childscList);
						sourceFileToPartitionPrefixesMap.put(sf, ppArr);
						sourceFileToPartitionConnectionLoadInfoMap.put(sf, pcliArr);
						if(addToMap)
						{
							List<SourceFile> sfList = warehousePartitionToSourceFileMap.get(parentwp);
							if(sfList == null)
							{
								sfList = new ArrayList<SourceFile>();
								warehousePartitionToSourceFileMap.put(parentwp, sfList);
							}
							if(!sfList.contains(sf))
							{
								//If this source file is the primary source for mapping in warehouse partition, add it at index 0 so that it
								//gets processed first and automatically gets used for mapping creation.
								if((wp.PrimarySourceName != null) && (sf.FileName != null) && (wp.PrimarySourceName.equalsIgnoreCase(sf.FileName)))
								{
									sfList.add(0, sf);
								}
								else
								{
									sfList.add(sf);
								}
							}
							
							List<WarehousePartition> pList = sourceFileToWarehousePartitionMap.get(sf);
							if(pList == null)
							{
								pList = new ArrayList<WarehousePartition>();
								sourceFileToWarehousePartitionMap.put(sf, pList);
							}
							if(!pList.contains(parentwp))
							{
								pList.add(parentwp);
							}
						}
					}
				}
				else
				{
					SourceFile sf = r.findSourceFile(sfName);
					if(sf == null)
					{
						logger.error("Source file not found: " + sfName + ". Warehouse partition will not be processed: " + wpName);
						continue;
					}
					List<SourceColumn> scList = getSourceColumnList(wpName, sf, columnNames, true);
					if(scList == null)
					{
						continue;
					}
					List<SourceColumn> childscList = getSourceColumnList(wpName, sf, childColumnNames, false);
					if(childscList == null)
					{
						childscList = new ArrayList<SourceColumn>();
					}
					if(sourceFileToPartitionColumnsMap.containsKey(sf))
					{
						logger.warn("Overriding column list for source file " + sf.FileName);
					}
					if(sourceFileToChildColumnsMap.containsKey(sf))
					{
						logger.warn("Overriding child column list for source file " + sf.FileName);
					}
					String [] ppArr = wp.getPartitionPrefixes(r);
					if(ppArr == null || ppArr.length == 0)
					{
						logger.error("No partition prefixes found for source file: " + sfName + ". Warehouse partition will not be processed: " + wpName);
						continue;
					}
					PartitionConnectionLoadInfo [] pcliArr = new PartitionConnectionLoadInfo[ppArr.length];
					for(int idx = 0; idx < ppArr.length; idx++)
					{
						pcliArr[idx] = r.getPartitionConnectionLoadInfo(ppArr[idx]);
					}
					
					sourceFileToPartitionColumnsMap.put(sf, scList);
					sourceFileToChildColumnsMap.put(sf, childscList);
					sourceFileToPartitionPrefixesMap.put(sf, ppArr);
					sourceFileToPartitionConnectionLoadInfoMap.put(sf, pcliArr);

					if(addToMap)
					{
						List<SourceFile> sfList = warehousePartitionToSourceFileMap.get(parentwp);
						if(sfList == null)
						{
							sfList = new ArrayList<SourceFile>();
							warehousePartitionToSourceFileMap.put(parentwp, sfList);
						}
						if(!sfList.contains(sf))
						{
							//If this source file is the primary source for mapping in warehouse partition, add it at index 0 so that it
							//gets processed first and automatically gets used for mapping creation.
							if((wp.PrimarySourceName != null) && (sf.FileName != null) && (wp.PrimarySourceName.equalsIgnoreCase(sf.FileName)))
							{
								sfList.add(0, sf);
							}
							else
							{
								sfList.add(sf);
							}
						}
						
						List<WarehousePartition> pList = sourceFileToWarehousePartitionMap.get(sf);
						if(pList == null)
						{
							pList = new ArrayList<WarehousePartition>();
							sourceFileToWarehousePartitionMap.put(sf, pList);
						}
						if(!pList.contains(parentwp))
						{
							pList.add(parentwp);
						}
					}
				}
			}
		}
		
		//Validate for referential integrity
		Set<SourceFile> sfToRemove = new HashSet<SourceFile>();
		for (Iterator<Entry<SourceFile, List<WarehousePartition>>> it = sourceFileToWarehousePartitionMap.entrySet().iterator(); it.hasNext();)
		{
			Entry<SourceFile, List<WarehousePartition>> ve = it.next();
			SourceFile sf = ve.getKey();
			wpList = sourceFileToWarehousePartitionMap.get(sf);
			if(wpList.size() > 1)
			{
				StringBuilder errorMsg = new StringBuilder();
				errorMsg.append("Source file '" + sf.FileName + "' is being loaded in multiple partitions:");
				boolean first = true;
				for(WarehousePartition wp : wpList)
				{
					if(first)
					{
						first = false;
					}
					else
					{
						errorMsg.append(", ");
					}
					errorMsg.append(wp.Name);
					List<SourceFile> sfList = warehousePartitionToSourceFileMap.get(wp);
					sfList.remove(sf);
				}
				errorMsg.append(". These partitions will not be processed for source file: '" + sf.FileName + "'.");
				logger.error(errorMsg.toString());

				sfToRemove.add(sf);
				//Do the necessary cleanup from the maps
			}
		}
		
		if(sfToRemove.size() > 0)
		{
			for(SourceFile sf : sfToRemove)
			{
				sourceFileToPartitionColumnsMap.remove(sf);
				sourceFileToChildColumnsMap.remove(sf);
				sourceFileToPartitionPrefixesMap.remove(sf);
				sourceFileToPartitionConnectionLoadInfoMap.remove(sf);
				sourceFileToWarehousePartitionMap.remove(sf);
			}
		}
		//Print all the maps
		logger.debug("====== Source file to source columns map ======");
		logger.debug(printSourceFileToPartitionColumnsMap());
		logger.debug("====== Source file to source columns map ======");

		logger.debug("====== Source file to partition prefixes map ======");
		logger.debug(printSourceFileToPartitionPrefixesMap());
		logger.debug("====== Source file to partition prefixes map ======");
		
		logger.debug("====== Source file to partition connection load info map ======");
		logger.debug(printSourceFileToPartitionConnectionLoadInfoMap());
		logger.debug("====== Source file to partition connection load info map ======");

		logger.debug("====== Warehouse partition to source file map ======");
		logger.debug(printWarehousePartitionToSourceFileMap());
		logger.debug("====== Warehouse partition to source file map ======");

		logger.debug("====== Source file to warehouse partition map ======");
		logger.debug(printSourceFileToWarehousePartitionMap());
		logger.debug("====== Source file to warehouse partition map ======");

	}


	
	private List<SourceColumn> getSourceColumnList(String wpName, SourceFile sf, String [] columnNames, boolean logErrorForColumnNotFound)
	{
		List<SourceColumn> scList  = new ArrayList<SourceColumn>();
		for(String cname : columnNames)
		{
			SourceColumn sc = sf.findColumn(cname);
			if(sc == null)
			{
				if(logErrorForColumnNotFound)
				{
					logger.error("Column name " + cname +" not found in source file " + sf.FileName + ". Partition " + wpName
							+ " will not be processed for source file splitting.");
				}
				return null;
			}
			scList.add(sc);
		}
		return scList;
	}
	
	public String printSourceFileToPartitionColumnsMap()
	{
		StringBuilder sb = new StringBuilder();
		if(sourceFileToPartitionColumnsMap == null || sourceFileToPartitionColumnsMap.isEmpty())
		{
			sb.append("<No entries in map>");
			return sb.toString();
		}
		
		boolean firstFile = true;
		for (Iterator<Entry<SourceFile, List<SourceColumn>>> it = sourceFileToPartitionColumnsMap.entrySet().iterator(); it.hasNext();)
		{
			Entry<SourceFile, List<SourceColumn>> ve = it.next();
			SourceFile sf = ve.getKey();
			List<SourceColumn> scList = ve.getValue();
			if(firstFile)
			{
				firstFile = false;
			}
			else
			{
				sb.append(", ");
			}
			sb.append("{" + sf.FileName + "=");
			boolean firstCol = true;
			for(SourceColumn sc : scList)
			{
				if(firstCol)
				{
					firstCol = false;
				}
				else
				{
					sb.append(",");
				}
				sb.append(sc.Name);
			}
			sb.append("}");
		}

		return sb.toString();
	}

	public String printSourceFileToPartitionPrefixesMap()
	{
		StringBuilder sb = new StringBuilder();
		if(sourceFileToPartitionPrefixesMap == null || sourceFileToPartitionPrefixesMap.isEmpty())
		{
			sb.append("<No entries in map>");
			return sb.toString();
		}
		
		boolean firstFile = true;
		for (Iterator<Entry<SourceFile, String []>> it = sourceFileToPartitionPrefixesMap.entrySet().iterator(); it.hasNext();)
		{
			Entry<SourceFile, String[] > ve = it.next();
			SourceFile sf = ve.getKey();
			String [] ppArr = ve.getValue();
			if(firstFile)
			{
				firstFile = false;
			}
			else
			{
				sb.append(", ");
			}
			sb.append("{" + sf.FileName + "=");
			if(ppArr != null && ppArr.length > 0)
			{
				boolean firstPrefix = true;
				for(String pp : ppArr)
				{
					if(firstPrefix)
					{
						firstPrefix = false;
					}
					else
					{
						sb.append(",");
					}
					sb.append(pp);
				}
			}
			sb.append("}");
		}

		return sb.toString();
	}

	public String printSourceFileToPartitionConnectionLoadInfoMap()
	{
		StringBuilder sb = new StringBuilder();
		if(sourceFileToPartitionConnectionLoadInfoMap == null || sourceFileToPartitionConnectionLoadInfoMap.isEmpty())
		{
			sb.append("<No entries in map>");
			return sb.toString();
		}
		
		boolean firstFile = true;
		for (Iterator<Entry<SourceFile, PartitionConnectionLoadInfo []>> it = sourceFileToPartitionConnectionLoadInfoMap.entrySet().iterator(); it.hasNext();)
		{
			Entry<SourceFile, PartitionConnectionLoadInfo[] > ve = it.next();
			SourceFile sf = ve.getKey();
			PartitionConnectionLoadInfo [] pcliArr = ve.getValue();
			if(firstFile)
			{
				firstFile = false;
			}
			else
			{
				sb.append(", ");
			}
			sb.append("{" + sf.FileName + "=");
			if(pcliArr != null && pcliArr.length > 0)
			{
				boolean firstPcli = true;
				for(PartitionConnectionLoadInfo pcli : pcliArr)
				{
					if(firstPcli)
					{
						firstPcli = false;
					}
					else
					{
						sb.append(",");
					}
					if(pcli == null)
					{
						sb.append("NULL");
					}
					else
					{
						sb.append(pcli.toString());
					}
				}
			}
			sb.append("}");
		}

		return sb.toString();
	}

	public String printWarehousePartitionToSourceFileMap()
	{
		StringBuilder sb = new StringBuilder();
		if(warehousePartitionToSourceFileMap == null || warehousePartitionToSourceFileMap.isEmpty())
		{
			sb.append("<No entries in map>");
			return sb.toString();
		}
		
		boolean firstPartition = true;
		for (Iterator<Entry<WarehousePartition, List<SourceFile>>> it = warehousePartitionToSourceFileMap.entrySet().iterator(); it.hasNext();)
		{
			Entry<WarehousePartition, List<SourceFile>> ve = it.next();
			WarehousePartition wp = ve.getKey();
			List<SourceFile> sfList = ve.getValue();
			if(firstPartition)
			{
				firstPartition = false;
			}
			else
			{
				sb.append(", ");
			}
			sb.append("{" + wp.Name + "=");
			boolean firstFile = true;
			for(SourceFile sf : sfList)
			{
				if(firstFile)
				{
					firstFile = false;
				}
				else
				{
					sb.append(",");
				}
				sb.append(sf.FileName);
			}
			sb.append("}");
		}

		return sb.toString();
	}

	public String printSourceFileToWarehousePartitionMap()
	{
		StringBuilder sb = new StringBuilder();
		if(sourceFileToWarehousePartitionMap == null || sourceFileToWarehousePartitionMap.isEmpty())
		{
			sb.append("<No entries in map>");
			return sb.toString();
		}
		
		boolean firstFile = true;
		for (Iterator<Entry<SourceFile, List<WarehousePartition>>> it = sourceFileToWarehousePartitionMap.entrySet().iterator(); it.hasNext();)
		{
			Entry<SourceFile, List<WarehousePartition>> ve = it.next();
			SourceFile sf = ve.getKey();
			List<WarehousePartition> wpList = ve.getValue();
			if(firstFile)
			{
				firstFile = false;
			}
			else
			{
				sb.append(", ");
			}
			sb.append("{" + sf.FileName + "=");
			boolean firstPartition = true;
			for(WarehousePartition wp : wpList)
			{
				if(firstPartition)
				{
					firstPartition = false;
				}
				else
				{
					sb.append(",");
				}
				sb.append(wp.Name);
			}
			sb.append("}");
		}

		return sb.toString();
	}
	
	public boolean split()
	{
		if(sourceFileToPartitionColumnsMap.isEmpty())
		{
			logger.warn("Returning from source file splitter, nothing to split");
			return true;
		}
		
		int numSourceFiles = sourceFileToPartitionColumnsMap.size();
		logger.info("Number of source files to be split: " + numSourceFiles);

		List<Exception> exceptionList = Collections.synchronizedList(new ArrayList<Exception>());
		List<SourceFileSplitterThread> threads = new ArrayList<SourceFileSplitterThread>();
		Set<SourceFile> processedSourceFiles = new HashSet<SourceFile>();
		List<SourceFile> sfList = new ArrayList<SourceFile>();
		Map<SourceFile, List<SourceColumn>> sourceFileToColumnsMap = new HashMap<SourceFile, List<SourceColumn>>();
		Map<SourceFile, List<SourceColumn>> sourceFileToCColumnsMap = new HashMap<SourceFile, List<SourceColumn>>();
		
		for (Iterator<Entry<WarehousePartition, List<SourceFile>>> it = warehousePartitionToSourceFileMap.entrySet().iterator(); it.hasNext();)
		{
			Entry<WarehousePartition, List<SourceFile>> ve = it.next();
			WarehousePartition wp = ve.getKey();
			sfList = warehousePartitionToSourceFileMap.get(wp);
			for(SourceFile sf : sfList)
			{
				List<SourceColumn> scList = sourceFileToPartitionColumnsMap.get(sf);
				sourceFileToColumnsMap.put(sf, scList);
				List<SourceColumn> childscList = sourceFileToChildColumnsMap.get(sf);
				sourceFileToCColumnsMap.put(sf, childscList);
				processedSourceFiles.add(sf);
			}
			SourceFileSplitterThread thread = new SourceFileSplitterThread(r, sfList, sourceFileToColumnsMap, sourceFileToCColumnsMap, sourceFileToPartitionPrefixesMap, sourceFileToPartitionConnectionLoadInfoMap, exceptionList, sourceFilePath);
			threads.add(thread);
		}
		
		for (Iterator<Entry<SourceFile, List<SourceColumn>>> it = sourceFileToPartitionColumnsMap.entrySet().iterator(); it.hasNext();)
		{
			Entry<SourceFile, List<SourceColumn>> ve = it.next();
			SourceFile sf = ve.getKey();
			if(processedSourceFiles.contains(sf))
			{
				continue;
			}
			List<SourceFile> sourceFileList = new ArrayList<SourceFile>();
			sourceFileList.add(sf);
			List<SourceColumn> scList = ve.getValue();
			sourceFileToColumnsMap = new HashMap<SourceFile, List<SourceColumn>>();
			sourceFileToColumnsMap.put(sf, scList);
			List<SourceColumn> childscList = sourceFileToChildColumnsMap.get(sf);
			sourceFileToCColumnsMap.put(sf, childscList);
			processedSourceFiles.add(sf);
			SourceFileSplitterThread thread = new SourceFileSplitterThread(r, sourceFileList, sourceFileToColumnsMap, sourceFileToCColumnsMap, sourceFileToPartitionPrefixesMap, sourceFileToPartitionConnectionLoadInfoMap, exceptionList, sourceFilePath);
			threads.add(thread);
		}
		
		int numThreads = Math.max(1, Runtime.getRuntime().availableProcessors() - 1);
		//We want a thread assigned to each source file. So, if we have more number of threads available, reduce the number of threads to match
		//with number of source files to be split.
		if(numThreads > threads.size())
		{
			numThreads = threads.size();
		}
		logger.info("Running parallel file splitting, number of threads: " + numThreads);
		ExecutorService tp = Executors.newFixedThreadPool(numThreads);
		for(SourceFileSplitterThread t : threads)
		{
			tp.execute(t);
			if(t.exceptionList != null)
			{
				for(Exception e : t.exceptionList)
				{
					logger.error(e.toString(), e);
				}
			}
		}
		
		tp.shutdown();
		try 
		{
			tp.awaitTermination(12, TimeUnit.HOURS);
		} catch (InterruptedException e) 
		{
			logger.error(e.toString(), e);
		}
		
		return true;
	}
	
	public boolean isSourceFileSplit(SourceFile sf)
	{
		return sourceFileToPartitionPrefixesMap.containsKey(sf) && sourceFileToPartitionConnectionLoadInfoMap.containsKey(sf); 
	}
}