/**
 * $Id: WarehousePartition.java,v 1.3 2012-11-12 14:47:06 BIRST\mjani Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse.partition;

import java.io.Serializable;

import java.util.List;

import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.Repository;
import com.successmetricsinc.query.DatabaseConnection;

public class PartitionConnectionLoadInfo implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;
    public String PartitionConnectionName;
    public String StagingLoadDir;
    public String DatabaseLoadDir;
    public String LoadMachine;
	
	/**
	 * Create a load group structure based on repository XML
	 * 
	 * @param ae
	 */
	public PartitionConnectionLoadInfo(Element e, Namespace ns)
	{
		PartitionConnectionName = e.getChildTextTrim("PartitionConnectionName", ns);
		StagingLoadDir = e.getChildTextTrim("StagingLoadDir", ns);
		DatabaseLoadDir = e.getChildTextTrim("DatabaseLoadDir", ns);
		LoadMachine = e.getChildTextTrim("LoadMachine", ns);		
	}
	
	public Element getWarehousePartitionElement(Namespace ns)
	{
	    Element e = new Element("WarehousePartition",ns);
	    
	    Element child = new Element("PartitionConnectionName",ns);
	    child.setText(PartitionConnectionName);
	    e.addContent(child);    
	    
	    child = new Element("StagingLoadDir",ns);
	    child.setText(StagingLoadDir);
	    e.addContent(child);
	    
	    child = new Element("DatabaseLoadDir",ns);
	    child.setText(DatabaseLoadDir);
	    e.addContent(child);
	    
	    child = new Element("LoadMachine",ns);
	    child.setText(LoadMachine);
	    e.addContent(child);
	    
	    return e;
	}
	
	public String getPartitionConnectionName()
	{
		return PartitionConnectionName;
	}
	
	public void setPartitionConnectionName(String PartitionConnectionName)
	{
		this.PartitionConnectionName = PartitionConnectionName;
	}

	public String getStagingLoadDir()
	{
		return StagingLoadDir;
	}
	
	public void setStagingLoadDir(String StagingLoadDir)
	{
		this.StagingLoadDir = StagingLoadDir;
	}

	public String getDatabaseLoadDir()
	{
		return DatabaseLoadDir;
	}
	
	public void setDatabaseLoadDir(String DatabaseLoadDir)
	{
		this.DatabaseLoadDir = DatabaseLoadDir;
	}

	public String getLoadMachine()
	{
		return LoadMachine;
	}
	
	public void setLoadMachine(String LoadMachine)
	{
		this.LoadMachine = LoadMachine;
	}
	
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("PartitionConnectionName = " + PartitionConnectionName);
		sb.append(", ");
		sb.append("StagingLoadDir = " + StagingLoadDir);
		sb.append(", ");
		sb.append("DatabaseLoadDir = " + DatabaseLoadDir);
		sb.append(", ");
		sb.append("LoadMachine = " + LoadMachine);
		
		return sb.toString();
	}
}
