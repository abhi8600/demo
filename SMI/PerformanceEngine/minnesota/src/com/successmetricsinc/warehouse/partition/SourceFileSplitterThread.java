package com.successmetricsinc.warehouse.partition;

import java.io.BufferedReader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.log4j.Logger;
import com.successmetricsinc.Repository;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.warehouse.Bulkload;
import com.successmetricsinc.warehouse.SourceColumn;
import com.successmetricsinc.warehouse.SourceFile;
import com.successmetricsinc.warehouse.SourceFile.FileFormatType;

public class SourceFileSplitterThread implements Runnable
{
	private static Logger logger = Logger.getLogger(SourceFileSplitterThread.class);
	private static final String COMPOUND_KEY_SEPARATOR = "~";
	private static final String PARTITION_KEY_TO_PREFIX_MAP_FILENAME_PREFIX = "PartitionKeyToPrefixMap";
	Repository r;
	List<SourceFile> sfList;
	Map<SourceFile, List<SourceColumn>> sourceFileToPartitionColumnsMap;
	Map<SourceFile, List<SourceColumn>> sourceFileToChildColumnsMap;
	Map<SourceFile, String []> sourceFileToPartitionPrefixesMap;
	Map<SourceFile, PartitionConnectionLoadInfo []> sourceFileToPartitionConnectionLoadInfoMap;	
	List<Exception> exceptionList;
	String sourceFilePath;
	Map<String, Map<String, String>> columnNameTopartitionKeyToPrefixMap;
	Map<String, Integer> partitionPrefixToIndexMap;
	int previousPartitionIndexUsed = -1;
	
	public SourceFileSplitterThread(Repository r, List<SourceFile> sfList, Map<SourceFile, List<SourceColumn>> sourceFileToPartitionColumnsMap, 
			Map<SourceFile, List<SourceColumn>> sourceFileToChildColumnsMap, Map<SourceFile, String []> sourceFileToPartitionPrefixesMap, 
			Map<SourceFile, PartitionConnectionLoadInfo []> sourceFileToPartitionConnectionLoadInfoMap, List<Exception> exceptionList, String sourceFilePath)
	{
		this.r = r;
		this.sfList = sfList;
		this.sourceFileToPartitionColumnsMap = sourceFileToPartitionColumnsMap;
		this.sourceFileToChildColumnsMap = sourceFileToChildColumnsMap;
		this.sourceFileToPartitionPrefixesMap = sourceFileToPartitionPrefixesMap;
		this.sourceFileToPartitionConnectionLoadInfoMap = sourceFileToPartitionConnectionLoadInfoMap;
		this.exceptionList = exceptionList;
		this.sourceFilePath = sourceFilePath;
	}
	
	@Override
	public void run()
	{
		for(SourceFile sf: sfList)
		{
			List<SourceColumn> scList = sourceFileToPartitionColumnsMap.get(sf);
			List<SourceColumn> childscList = sourceFileToChildColumnsMap.get(sf);
			String [] partitionPrefixes = sourceFileToPartitionPrefixesMap.get(sf);
			PartitionConnectionLoadInfo [] pcliArr = sourceFileToPartitionConnectionLoadInfoMap.get(sf);
			printStartMessage(sf, scList);			
			List<SourceFile.BirstDataSource> bdsList = null;
			try
			{
				bdsList = sf.getPhysicalSourceFiles(sourceFilePath, false, r, false,null, null);
			}
			catch(Exception e)
			{
				logger.error("Error getting source file list: " + e.toString(), e);
				continue;
			}
			List<File> sourceFiles = new ArrayList<File>();
			for(SourceFile.BirstDataSource bds : bdsList)
			{
				if(bds instanceof SourceFile.BirstFile)
				{
					sourceFiles.add((File)bds);
				}
			}
			if (sourceFiles == null || sourceFiles.isEmpty())
			{
				logger.error("Nothing to split as no data files were found for source file: " + sf.FileName);
				return;
			}
	
			int [] partitionKeyIndices;
			try
			{
				partitionKeyIndices = populatePartitionKeyIndices(sf, scList);
			}
			catch(Exception e)
			{
				logger.error(e.toString() + "::Incorrect partition source column names provided as partition keys for source file: " 
						+ sf.FileName + ". Splitting process failed.", e);
				return;
			}
			
			int [] childPartitionKeyIndices = null;
			if((childscList != null) && (childscList.size() > 0))
			{
				try
				{
					childPartitionKeyIndices = populatePartitionKeyIndices(sf, childscList);
				}
				catch(Exception e)
				{
					logger.error(e.toString() + "::Incorrect child partition source column names provided as partition keys for source file: " 
							+ sf.FileName + ". Splitting process failed.", e);
					return;
				}
			}
			
			columnNameTopartitionKeyToPrefixMap = new HashMap<String, Map<String, String>>();
			initializePartitionKeyToPrefixMap(scList);
			if((childscList != null) && (childscList.size() > 0))
			{
				initializePartitionKeyToPrefixMap(childscList);
			}
			String colKeyName = getColumnNameKey(scList);
			Map<String, String> partitionKeyToPrefixMap = columnNameTopartitionKeyToPrefixMap.get(colKeyName);
			
			Map<String, String> childPartitionKeyToPrefixMap = new HashMap<String, String>();
			if((childscList != null) && (childscList.size() > 0))
			{
				String childColKeyName = getColumnNameKey(childscList);
				childPartitionKeyToPrefixMap = columnNameTopartitionKeyToPrefixMap.get(childColKeyName);
			}			
			partitionPrefixToIndexMap = new HashMap<String, Integer>();
			//Map that points the partition prefix position in the array
			for(int i = 0; i < partitionPrefixes.length; i++)
			{
				partitionPrefixToIndexMap.put(partitionPrefixes[i], i);
			}
			BufferedReader br = null;
			InputStreamReader is = null;
			FileInputStream fis = null;
	
			FileOutputStream [] fosArr = null;
			OutputStreamWriter [] osArr = null;
			BufferedWriter [] bwArr = null;
			
			for(File f : sourceFiles)
			{
				try
				{
					AtomicLong errorCount = new AtomicLong(0);
					fis = new FileInputStream(f);
					is = new InputStreamReader(fis, sf.getEncoding());
					br = new BufferedReader(is);
					
					int numPrefixes = partitionPrefixes.length;
					fosArr = new FileOutputStream[numPrefixes];
					osArr = new OutputStreamWriter[numPrefixes];
					bwArr = new BufferedWriter[numPrefixes];
					for(int ppIdx = 0; ppIdx < partitionPrefixes.length; ppIdx++)
					{
						String pp = partitionPrefixes[ppIdx];
						String outFilename = pp + "_" + f.getName();
	
						String outFilePath = sourceFilePath;
						if((pcliArr[ppIdx] != null) && (pcliArr[ppIdx].StagingLoadDir != null))
						{
							outFilePath = pcliArr[ppIdx].StagingLoadDir;
						}
						File outFile = new File(outFilePath, outFilename);
						fosArr[ppIdx] = new FileOutputStream(outFile);
						osArr[ppIdx] = new OutputStreamWriter(fosArr[ppIdx], sf.getEncoding());
						bwArr[ppIdx] = new BufferedWriter(osArr[ppIdx]);						
					}
					
					StringBuilder sb = new StringBuilder();
					StringBuilder line;
					//Copy over headers to all output files
					char [] sepArr = null;
					if(sf.Separator != null)
					{
						sf.Separator.toCharArray();
					}
					if(sf.HasHeaders)
					{
						line = Bulkload.readLine(sb, sf, sepArr, br, sf.Type != FileFormatType.FixedWidth, 
								sf.ForceNumColumns ? sf.Columns.length - 1 : 0, sf.OnlyQuoteAfterSeparator);
						writeLine(bwArr, -1, line.toString());
					}
					
					while ((line = Bulkload.readLine(sb, sf, sepArr, br, sf.Type != FileFormatType.FixedWidth, sf.ForceNumColumns ? sf.Columns.length - 1 : 0, sf.OnlyQuoteAfterSeparator)) != null)
					{
						String lineStr = line.toString();
						String [] lineArr = Util.splitLine(lineStr, sf, 0, errorCount);
						int index = getWritePartitionIndex(sf, lineArr, partitionPrefixes, partitionKeyToPrefixMap, partitionKeyIndices, true);
						int indexBasedOnChild = -1;
						if(childPartitionKeyIndices != null && childPartitionKeyIndices.length > 0)
						{
							indexBasedOnChild = getWritePartitionIndex(sf, lineArr, partitionPrefixes, childPartitionKeyToPrefixMap, childPartitionKeyIndices, false);
						}
						if((indexBasedOnChild != -1) && (index != indexBasedOnChild))
						{
							logger.error("Unable to process line [" + line + "]: Different partition derived based on parent index and child index. index = " +
									index + ", indexBasedOnChild = " + indexBasedOnChild);
							errorCount.incrementAndGet();
							continue;
						}
						writeLine(bwArr, index, line.toString());
						if(childPartitionKeyIndices != null)
						{
							saveWritePartitionIndex(index, childPartitionKeyIndices, lineArr, childPartitionKeyToPrefixMap, partitionPrefixes);
						}
					}
					if(errorCount.get() != 0)
					{
						exceptionList.add(new Exception("There were errors processing source file: " + f.getName() 
								+ ". Please check log file for more details."));
					}
					savePartitionKeyToPrefixMap(scList);
					if((childscList != null) && (childscList.size() > 0))
					{
						savePartitionKeyToPrefixMap(childscList);
					}
				}
				catch(Exception e)
				{
					exceptionList.add(e);
				}
				finally
				{
					try
					{
						if(br != null)
							br.close();
						if(is != null)
							is.close();
						if(fis != null)
							fis.close();
						
						for(int ppIdx = 0; ppIdx < partitionPrefixes.length; ppIdx++)
						{
							if(bwArr[ppIdx] != null)
								bwArr[ppIdx].close();
							if(osArr[ppIdx] != null)
								osArr[ppIdx].close();
							if(fosArr[ppIdx] != null)
								fosArr[ppIdx].close();
						}
					}
					catch(IOException ioe)
					{
						//Do nothing
					}
				}
			}
		}
	}
	
	private int getWritePartitionIndex(SourceFile sf, String [] lineArr, String [] partitionPrefixes, 
			Map<String, String> partitionKeyToPrefixMap, int[] pkIndices, boolean processRoundRobin)
	{
		int index = -1;
		
		String partitionKey = getPartitionKey(pkIndices, lineArr);
		String partitionPrefix = partitionKeyToPrefixMap.get(partitionKey);
		
		if(partitionPrefix != null)
		{
			//Given partition key has been assigned a partition prefix use that.
			Integer indexObj = partitionPrefixToIndexMap.get(partitionPrefix);
			if(indexObj == null)
			{
				exceptionList.add(new Exception("Error looking up partition key in the map: " + partitionKey + " for source file: " + sf.FileName));
				index = 0;
			}
			else
			{
				index = indexObj.intValue();
			}
		}
		else if(processRoundRobin)
		{
			//Given partition key has not been assigned - pick the next one using round robin
			previousPartitionIndexUsed++;
			if(previousPartitionIndexUsed < partitionPrefixes.length)
			{
				index = previousPartitionIndexUsed;
			}
			else
			{
				index = 0;
			}
			previousPartitionIndexUsed = index;
			partitionKeyToPrefixMap.put(partitionKey, partitionPrefixes[index]);
		}
		return index;
	}
	
	private void saveWritePartitionIndex(int index, int[] pkIndices, String [] lineArr, Map<String, String> partitionKeyToPrefixMap, String [] partitionPrefixes)
	{
		String partitionKey = getPartitionKey(pkIndices, lineArr);
		partitionKeyToPrefixMap.put(partitionKey, partitionPrefixes[index]);
		
	}
	private void writeLine(BufferedWriter [] bwArr, int index, String line) throws IOException
	{
		if(index < 0)
		{
			for(int i = 0; i < bwArr.length; i++)
			{
				bwArr[i].write(line);
				bwArr[i].write((byte) '\r');
				bwArr[i].write((byte) '\n');
			}
		}
		else
		{
			bwArr[index].write(line);
			bwArr[index].write((byte) '\r');
			bwArr[index].write((byte) '\n');
		}
	}
	
	private void printStartMessage(SourceFile sf, List<SourceColumn> scList)
	{
		StringBuilder logMsg = new StringBuilder();
		logMsg.append("Splitting source file: " + sf.FileName + " based on partition column(s): ");
		boolean firstCol = true;
		for(SourceColumn sc : scList)
		{
			if(firstCol)
			{
				firstCol = false;
			}
			else
			{
				logMsg.append(", ");
			}
			logMsg.append(sc.Name);
		}
		logger.info(logMsg.toString());
	}
	
	private int [] populatePartitionKeyIndices(SourceFile sf, List<SourceColumn> scList) throws Exception
	{
		int scListSize = scList.size();
		int [] pkIndices = new int[scListSize];
	
		for(int scIdx = 0; scIdx < scListSize; scIdx++)
		{
			SourceColumn sc = scList.get(scIdx);
			int index = sf.findColumnIndex(sc.Name);
			if(index == -1)
			{
				throw new Exception("Source column not found: " + sc.Name + ". Source file: " + sf.FileName);
			}
			pkIndices[scIdx] = index;
		}

		return pkIndices;
	}

	@SuppressWarnings("unchecked")
	private void initializePartitionKeyToPrefixMap(List<SourceColumn> scList)
	{
		if(scList == null || scList.size() == 0)
		{
			logger.error("initializePartitionKeyToPrefixMap: Null or empty column list provided. Nothing to initialize.");
			return;
		}
		String colNameKey = getColumnNameKey(scList);
		String fname = PARTITION_KEY_TO_PREFIX_MAP_FILENAME_PREFIX + "." + colNameKey + ".ser";
		File f = new File(sourceFilePath, fname);
		boolean buildNewMap = true;
		Map<String, String> partitionKeyToPrefixMap = null;
		if(f.exists() && f.isFile())
		{
			//Deserialize the map saved from earlier runs.
			FileInputStream fis = null;
	        ObjectInputStream ois = null;
	        try
	        {
	        	fis = new FileInputStream(f);
	        	ois = new ObjectInputStream(fis);
	        	partitionKeyToPrefixMap = (Map<String, String>) ois.readObject();
	        	buildNewMap = false;
	        }
	        catch(Exception e)
	        {
	        	logger.error(e.toString(), e);
	        }
	        finally
	        {
	        	try
	        	{
	        		if(fis != null)
	        			fis.close();
		        	if(ois != null)
	        			ois.close();
	        	}
	        	catch(IOException ioe)
	        	{
	        		//Do nothing
	        	}
	        }
		}
		
		if(buildNewMap)
		{
			logger.info("No map of partition key to prefix was available - instantiating a new one.");
			partitionKeyToPrefixMap = new HashMap<String, String> ();
		}
		columnNameTopartitionKeyToPrefixMap.put(colNameKey, partitionKeyToPrefixMap);
	}

	private void savePartitionKeyToPrefixMap(List<SourceColumn> scList)
	{
		if(scList == null || scList.size() == 0)
		{
			logger.error("savePartitionKeyToPrefixMap: Null or empty column list provided. Nothing to save.");
			return;
		}
		
		String colNameKey = getColumnNameKey(scList);
		String fname = PARTITION_KEY_TO_PREFIX_MAP_FILENAME_PREFIX + "." + colNameKey + ".ser";

		Map<String, String> partitionKeyToPrefixMap = columnNameTopartitionKeyToPrefixMap.get(colNameKey);
		
		File f = new File(sourceFilePath, fname);
		if(f.exists() && f.isFile())
		{
			//Delete the file saved earlier
			f.delete();
		}
		
		//Serialize the map to disk.
		FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try
        {
        	fos = new FileOutputStream(f);
        	oos = new ObjectOutputStream(fos);
        	oos.writeObject(partitionKeyToPrefixMap);
        }
        catch(Exception e)
        {
        	logger.error(e.toString(), e);
        }
        finally
        {
        	try
        	{
	        	if(fos != null)
	        		fos.close();
	        	if(oos != null)
	        		oos.close();
        	}
        	catch(IOException ioe)
        	{
        		//Do nothing
        	}
        }
	}

	private String getColumnNameKey(List<SourceColumn> scList)
	{
		if(scList == null || scList.size() == 0)
		{
			return null;
		}
		StringBuilder colNameBuf = new StringBuilder();
		boolean first = true;
		for(int i = 0; i < scList.size(); i++)
		{
			String colName = scList.get(i).Name;
			if(first)
			{
				first = false;
			}
			else
			{
				colNameBuf.append(COMPOUND_KEY_SEPARATOR);
			}
			colNameBuf.append(colName);
		}

		return colNameBuf.toString();
	}
	
	private String getPartitionKey(int [] pkIndices, String [] lineArr)
	{
		StringBuilder partitionKeyBuf = new StringBuilder();
		boolean first = true;
		for(int i = 0; i < pkIndices.length; i++)
		{
			int colIdx = pkIndices[i];
			if(first)
			{
				first = false;
			}
			else
			{
				partitionKeyBuf.append(COMPOUND_KEY_SEPARATOR);
			}
			partitionKeyBuf.append(lineArr[colIdx]);
		}
		
		return partitionKeyBuf.toString();
	}
}