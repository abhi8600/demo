/**
 * $Id: WarehousePartition.java,v 1.3 2012-11-12 14:47:06 BIRST\mjani Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse.partition;

import java.io.Serializable;

import java.util.List;

import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.Repository;
import com.successmetricsinc.query.DatabaseConnection;

public class WarehousePartition implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;
	public String Name;
	public String DatabaseConnectionName;
	public String SourceName;
	public String [] PartitionColumnNames;
	public String [] ChildPartitionColumnNames;
	public String PrimarySourceName;
	public List<WarehousePartition> Children;
	public WarehousePartition Parent;
	public boolean IsInvalid = false;
	
	/**
	 * Create a load group structure based on repository XML
	 * 
	 * @param ae
	 */
	public WarehousePartition(Element e, Namespace ns)
	{
		Name = e.getChildTextTrim("Name", ns);
		DatabaseConnectionName = e.getChildTextTrim("DatabaseConnectionName", ns);
		SourceName = e.getChildTextTrim("SourceName", ns);
		PartitionColumnNames = Repository.getStringArrayChildren(e, "PartitionColumnNames", ns);		
		ChildPartitionColumnNames = Repository.getStringArrayChildren(e, "ChildPartitionColumnNames", ns);
		PrimarySourceName = e.getChildTextTrim("PrimarySourceName", ns);		
	}
	
	public Element getWarehousePartitionElement(Namespace ns)
	{
	    Element e = new Element("WarehousePartition",ns);
	    
	    Element child = new Element("Name",ns);
	    child.setText(Name);
	    e.addContent(child);    
	    
	    child = new Element("DatabaseConnectionName",ns);
	    child.setText(DatabaseConnectionName);
	    e.addContent(child);
	    
	    child = new Element("SourceName",ns);
	    child.setText(SourceName);
	    e.addContent(child);
	    
	    if (PartitionColumnNames!=null && PartitionColumnNames.length>0)
	    {
	      child = Repository.getStringArrayElement("PartitionColumnNames", PartitionColumnNames, ns);
	      e.addContent(child);
	    }

	    if (ChildPartitionColumnNames!=null && ChildPartitionColumnNames.length>0)
	    {
	      child = Repository.getStringArrayElement("ChildPartitionColumnNames", ChildPartitionColumnNames, ns);
	      e.addContent(child);
	    }

	    child = new Element("PrimarySourceName",ns);
	    child.setText(PrimarySourceName);
	    e.addContent(child);

	    return e;
	}
	
	public String getName()
	{
		return Name;
	}
	
	public void setName(String Name)
	{
		this.Name = Name;
	}

	public String getDatabaseConnectionName()
	{
		return DatabaseConnectionName;
	}
	
	public void setDatabaseConnectionName(String DatabaseConnectionName)
	{
		this.DatabaseConnectionName = DatabaseConnectionName;
	}

	public String getSourceName()
	{
		return SourceName;
	}
	
	public void setSourceName(String SourceName)
	{
		this.SourceName = SourceName;
	}

	public String [] getPartitionColumnNames()
	{
		return PartitionColumnNames;
	}
	
	public void setPartitionColumnNames(String [] ParitionColumnNames)
	{
		this.PartitionColumnNames = ParitionColumnNames;
	}

	public String [] getChildPartitionColumnNames()
	{
		return ChildPartitionColumnNames;
	}
	
	public void setChildPartitionColumnNames(String [] ChildParitionColumnNames)
	{
		this.ChildPartitionColumnNames = ChildParitionColumnNames;
	}

	public String getPrimarySourceName()
	{
		return PrimarySourceName;
	}
	
	public void setPrimarySourceName(String PrimarySourceName)
	{
		this.PrimarySourceName = PrimarySourceName;
	}
	
	public String [] getPartitionPrefixes(Repository r)
	{
		//Do not use the r.findConnection(...) since we might have overridden the default connection
		for(DatabaseConnection dc : r.getConnections())
		{
			if(dc.Name.equals(DatabaseConnectionName))
			{
				return dc.partitionConnectionNames;
			}
		}
		return null;
	}
	
	public void getAllChildren(List<WarehousePartition> allChildren) throws Exception
	{
		if((Children != null) && (Children.size() > 0))
		{
			for(int i = 0; i < Children.size(); i++)
			{
				WarehousePartition child = Children.get(i);
				if(allChildren.contains(child))
				{
					throw new Exception("Circular reference detected for warehouse partition: " + Name);
				}
				allChildren.add(child);
			}
			for(int i = 0; i < Children.size(); i++)
			{
				WarehousePartition child = Children.get(i);
				child.getAllChildren(allChildren);
			}
		}
	}
}
