/**
 * $Id: WarehousePartition.java,v 1.3 2012-11-12 14:47:06 BIRST\mjani Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse.partition;

import java.util.ArrayList;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.successmetricsinc.Repository;
import com.successmetricsinc.query.DimensionTable;
import com.successmetricsinc.query.MeasureTable;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.warehouse.SourceFile;
import com.successmetricsinc.warehouse.StagingColumn;
import com.successmetricsinc.warehouse.StagingTable;

public class PartitionMetadataProcessor
{
	Repository r;
	SourceFileSplitter sfs;
	Set<StagingTable> partitionedStagingTables = null;
	Set<MeasureTable> partitionedMeasureTables = null;
	Set<DimensionTable> partitionedDimensionTables = null;
	
	public PartitionMetadataProcessor(Repository r)
	{
		this.r = r;
		sfs = new SourceFileSplitter(r, r.getServerParameters().getApplicationPath());
		init();
	}
	
	private void init()
	{
		partitionedStagingTables = getSplitStagingTables(sfs);
		partitionedMeasureTables = new HashSet<MeasureTable>();
		partitionedDimensionTables = new HashSet<DimensionTable>();
		
		getPartitionedMeasureAndDimensionTables(partitionedStagingTables, partitionedMeasureTables, partitionedDimensionTables);
	}
	
	private Set<StagingTable> getSplitStagingTables(SourceFileSplitter sfs)
	{
		Set<StagingTable> splitstSet = new HashSet<StagingTable>();
		StagingTable [] stArr = r.getStagingTables();
		
		if((stArr == null) || (stArr.length == 0))
		{
			return splitstSet;
		}
		
		for(int stIdx = 0; stIdx < stArr.length; stIdx++)
		{
			String sfName = stArr[stIdx].SourceFile;
			if(sfName != null && sfName.length() > 0)
			{
				SourceFile sf = r.findSourceFile(sfName);
				if(sf != null && (sfs.isSourceFileSplit(sf)))
				{
					splitstSet.add(stArr[stIdx]);
				}
			}
		}

		for(int stIdx = 0; stIdx < stArr.length; stIdx++)
		{
			StagingTable st = stArr[stIdx];
			if((st.Script != null) && (st.Script.InputQuery != null) && (st.Script.InputQuery.trim().length() > 0))
			{
				//This is a scripted source - now check to see if this scripted source depends on any of the staging
				//tables in the split staging tables list
				List<StagingTable> scriptPath = new ArrayList<StagingTable> ();
				TransformationScript ts = new TransformationScript(r, null);
				ts.parseScript(st.Script, 100, scriptPath);
				if(scriptPath.size() > 0)
				{
					for(StagingTable scriptst : scriptPath)
					{
						splitstSet.add(scriptst);
					}
				}
			}
		}
		return splitstSet;
	}
	
	private void getPartitionedMeasureAndDimensionTables(Set<StagingTable> splitstSet, 
			Set<MeasureTable> splitmtSet, 
			Set<DimensionTable> splitdtSet)
	{
		for (Iterator<StagingTable> it = splitstSet.iterator(); it.hasNext();)
		{
			StagingTable st = it.next();
			List<MeasureTable> mtList = st.getAllMeasureTablesAtGrain();
			splitmtSet.addAll(mtList);
			
			Map<DimensionTable, List<StagingColumn>> scmap = st.getStagingColumnMap(r);
			splitdtSet.addAll(scmap.keySet());
		}
	}
	
	public boolean isMeasureTablePartitioned(MeasureTable mt)
	{
		return partitionedMeasureTables.contains(mt);
	}
	
	public boolean isDimensionTablePartitioned(DimensionTable dt)
	{
		return partitionedDimensionTables.contains(dt);
	}
}
