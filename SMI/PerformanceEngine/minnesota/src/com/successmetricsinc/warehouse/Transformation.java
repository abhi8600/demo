/**
 * $Id: Transformation.java,v 1.25 2010-12-20 00:36:15 bpeters Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.JasperDataSourceProvider;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.warehouse.transforms.JavaTransform;

/**
 * @author Brad Peters
 * 
 */
public class Transformation implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(Transformation.class);
	protected String TableName;
	private StringReplacement sr;
	protected int ExecuteAfter;
	protected int iExecuteAfter;
	public static final int AFTER_FILES_STAGED = 1;
	public static final int AFTER_QUERY_BASED_TABLES_GENERATED = 2;
	public static final int AFTER_UNKNOWN_KEYS_REPLACED = 3;
	public static final int AFTER_RANKS_SET = 4;
	public static final int AFTER_CHECKSUMS_UPDATED = 5;
	public static final int DO_NOT_EXECUTE = 6;

	public static Transformation createStagingTransformation(String tableName, StringReplacement sr, Element e, Namespace ns)
	{
		Transformation stran = null;
		if (e.getName().equals("Expression"))
		{
			stran = new Expression(e, ns);
		} else if (e.getName().equals("LogicalColumnDefinition"))
		{
			stran = new LogicalColumnDefinition(e, ns);
		} else if (e.getName().equals("TableLookup"))
		{
			stran = new TableLookup(e, ns);
		} else if (e.getName().equals("Procedure"))
		{
			stran = new Procedure(e, ns);
		} else if (e.getName().equals("DateID"))
		{
			stran = new StaticDateID(e, ns);
		} else if (e.getName().equals("Rank"))
		{
			stran = new Rank(e, ns);
		} else if (e.getName().equals("Household"))
		{
			stran = new Household(e, ns);
		} else if (e.getName().equals("JavaTransform"))
		{
			stran = new JavaTransform(e, ns);
		} else
		{
			logger.error("Bad transformation: " + tableName);
			return null;
		}
		stran.TableName = tableName;
		stran.sr = sr;
		String temp = e.getChildText("ExecuteAfter", ns);
		stran.ExecuteAfter = temp == null ? AFTER_UNKNOWN_KEYS_REPLACED : setTransformationStage(temp);
		if (temp!=null)
		{
			stran.iExecuteAfter = Integer.parseInt(temp);
		}
		return stran;
	}
	
	public Element getTransformationElement(Namespace ns)
	{
		Element e = null;
		if (this instanceof Expression)
		{
			e = ((Expression)this).getExpressionElement(ns);
		}
		if (this instanceof LogicalColumnDefinition)
		{
			e = ((LogicalColumnDefinition)this).getLogicalTableElement(ns);
		}
		if (this instanceof TableLookup)
		{
			e = ((TableLookup)this).getTableLookupElement(ns);
		}
		if (this instanceof Procedure)
		{
			e = ((Procedure)this).getProcedureElement(ns);
		}
		if (this instanceof Rank)
		{
			e = ((Rank)this).getRankElement(ns);
		}
		if (this instanceof Household)
		{
			e = ((Household)this).getHouseholdElement(ns);
		}
		if (this instanceof JavaTransform)
		{
			e = ((JavaTransform)this).getJavaTransformElement(ns);
		}
		if (this instanceof StaticDateID)
		{
			e = ((StaticDateID)this).getDateIDElement(ns);
		}
		return e;
	}

	private static int setTransformationStage(String stage)
	{
		int STAGE = AFTER_UNKNOWN_KEYS_REPLACED;
		try
		{
			STAGE = Integer.parseInt(stage);
		} catch (NumberFormatException e)
		{
			if (logger.isTraceEnabled())
				logger.trace("Invalid value for Transformation ExecuteAfter : " + stage + ", using default value : " + AFTER_UNKNOWN_KEYS_REPLACED + ".");
			return STAGE;
		}
		switch (STAGE)
		{
		case AFTER_FILES_STAGED:
		case AFTER_QUERY_BASED_TABLES_GENERATED:
		case AFTER_UNKNOWN_KEYS_REPLACED:
		case AFTER_RANKS_SET:
		case AFTER_CHECKSUMS_UPDATED:
		case DO_NOT_EXECUTE:
			return STAGE;
		default:
			if (logger.isTraceEnabled())
				logger.trace("Invalid value for Transformation ExecuteAfter : " + stage + ", using default value : " + AFTER_UNKNOWN_KEYS_REPLACED + ".");
			return AFTER_UNKNOWN_KEYS_REPLACED;
		}
	}

	public String getTransformationStage(int stage)
	{
		switch (stage)
		{
		case AFTER_FILES_STAGED:
			return ("After Files Staged");
		case AFTER_QUERY_BASED_TABLES_GENERATED:
			return ("After Query-Based Tables Generated");
		case AFTER_RANKS_SET:
			return ("After Ranks Set");
		case AFTER_CHECKSUMS_UPDATED:
			return ("After Checksums Updated");
		case DO_NOT_EXECUTE:
			return ("Do Not Execute");
		default:
			return ("After Unknown Keys Replaced");
		}
	}

	/**
	 * Replace any qualified logical column names from the staging table with their physical equivalents
	 * 
	 * @param result
	 * @return
	 */
	protected String getReplacedString(String result)
	{
		return (sr.getReplacedString(result));
	}

	public void executeTransformation(Repository r, DatabaseConnection dc, Session s, JasperDataSourceProvider jdsp)
	throws BaseException, SQLException, IOException, CloneNotSupportedException
	{
		if (Procedure.class.isInstance(this))
		{
			((Procedure) this).execute(r, dc, s, jdsp);
		} else if (Household.class.isInstance(this))
		{
			((Household) this).execute(r, dc, s, jdsp);
		} else if (JavaTransform.class.isInstance(this))
		{
			((JavaTransform) this).execute(r, dc, s, jdsp);
		}
	}

	public void setStringReplacement(StringReplacement sr)
	{
		this.sr = sr;
	}
}
