/**
 * $Id: StagingTable.java,v 1.122 2012-11-18 21:22:15 birst\bpeters Exp $
 *
 * Copyright (C) 2007-2009 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.Repository;
import com.successmetricsinc.query.AbstractQueryString;
import com.successmetricsinc.query.Database;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.DimensionColumn;
import com.successmetricsinc.query.DimensionTable;
import com.successmetricsinc.query.Hierarchy;
import com.successmetricsinc.query.Level;
import com.successmetricsinc.query.LevelKey;
import com.successmetricsinc.query.MeasureTable;
import com.successmetricsinc.query.MeasureTableGrain;
import com.successmetricsinc.query.TableSource;
import com.successmetricsinc.query.Database.DatabaseType;
import com.successmetricsinc.query.TableSourceFilter;
import com.successmetricsinc.transformation.ScriptDefinition;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.util.DateUtil;
import com.successmetricsinc.util.RepositoryException;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author Brad Peters
 * 
 */
public class StagingTable implements Comparable<StagingTable>, StringReplacement, Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(StagingTable.class);
	public static final int SOURCE_FILE = 0;
	public static final int SOURCE_QUERY = 1;
	public String Name;
	public int ID;
	public int SourceType;
	public String SourceFile;
	public String Query;
	public boolean PersistQuery;
	public String[][] Levels;
	public String[][] LevelFilters;
	public StagingColumn[] Columns;
	public Transformation[] Transformations;
	public Long Size;
	public boolean Disabled;
	public boolean TruncateOnLoad;
	public String[] LoadGroupNames;
	public String[] SubGroupNames;
	public String[] SourceGroups;
	public String ChecksumGroup;
	public boolean Transactional;
	public boolean IncrementalSnapshotFact;
	public boolean ExcludeFromModel;
	public boolean DiscoveryTable;
	public transient Repository r;
	public Set<String> DayIDColumnsMappedToLoadDate = null;
	private String SCDDateName;
	public SnapshotPolicy Snapshots;
	public ScriptDefinition Script;
	public String[] SnapshotDeleteKeys;
	public Map<String, StagingColumn> nameToStagingColumnMap;
	public Map<String, StagingColumn> sfColNameToStagingColumnMap;
	public String[] SourceKeys;
	public String[] ForeignKeySources;
	public boolean Imported;
	public boolean LiveAccess;
	public Date LastModifiedDate;
	
	private String Guid;
	private String CreatedDate;
	private String strLastModifiedDate;
	private String CreatedUsername;
	private String LastModifiedUsername;
	private String LogicalName;
	
	private boolean WebOptional;
	private boolean WebReadOnly;
	private boolean Consolidation;
	
	private String SourceGroupStr;
	private String cksumGroupStr;
	
	private String HierarchyName;
	private String LevelName;
	
	private boolean UseBirstLocalForSource;
	private String PreviousLoadGroup;
	
	private String CustomTimeColumn;
	private String CustomTimePrefix;
	private String NewColumnTarget;
	
	private Location[] locations;
	
	private String ParentForeignKeySource;
	
	private boolean Autoplace;
	private String MeasureNamePrefix;
	private boolean UnCached;
	private int Cardinality;
	
	private OverrideLevelKey[] OverrideLevelKeys;
	private CustomTimeShift[] CustomTimeShifts;
	
	public ForeignKey[] ForeignKeys;
  public ForeignKey[] FactForeignKeys;
  public String[] FactForeignKeySources;
  
  private TableSource TableSource;
  
  private InheritTable[] InheritTables;
	
	public StagingTable()
	{
	}

	@SuppressWarnings("rawtypes")
	public StagingTable(Element e, Namespace ns, Repository r) throws RepositoryException
	{
		this.r = r;
		
		Guid = e.getChildText("Guid",ns);
		CreatedDate = e.getChildText("CreatedDate",ns);
		strLastModifiedDate = e.getChildText("LastModifiedDate",ns);
		CreatedUsername = e.getChildText("CreatedUsername",ns);
		LastModifiedUsername = e.getChildText("LastModifiedUsername",ns);
		LogicalName = e.getChildText("LogicalName",ns);
		PreviousLoadGroup = e.getChildText("PreviousLoadGroup",ns);
		
		CustomTimeColumn= e.getChildText("CustomTimeColumn",ns);
		CustomTimePrefix= e.getChildText("CustomTimePrefix",ns);
		NewColumnTarget= e.getChildText("NewColumnTarget",ns);
		
		HierarchyName = e.getChildText("HierarchyName",ns);
		LevelName = e.getChildText("LevelName",ns);
		
		ParentForeignKeySource = e.getChildText("ParentForeignKeySource",ns);
		
		Name = e.getChildTextTrim("Name", ns);
		String s = e.getChildText("SourceType", ns);
		if (s != null)
		{
			SourceType = Integer.valueOf(s);
		}
		s = e.getChildText("ID", ns);
		if (s != null)
		{
			ID = Integer.valueOf(s);
		}
		s = e.getChildText("PersistQuery", ns);
		if (s != null)
		{
			PersistQuery = Boolean.valueOf(s);
		}
		s = e.getChildText("Disabled", ns);
		if (s != null)
		{
			Disabled = Boolean.valueOf(s);
		}
		s = e.getChildText("TruncateOnLoad", ns);
		if (s != null)
		{
			TruncateOnLoad = Boolean.valueOf(s);
		}
		s = e.getChildText("DiscoveryTable", ns);
		if (s != null)
		{
			DiscoveryTable = Boolean.valueOf(s);
		}
		s = e.getChildText("ExcludeFromModel", ns);
		if (s != null)
		{
			ExcludeFromModel = Boolean.valueOf(s);
		}
		s = e.getChildText("Transactional", ns);
		if (s != null)
		{
			Transactional = Boolean.valueOf(s);
		}
		s = e.getChildText("LiveAccess", ns);
		if (s != null)
		{
			LiveAccess = Boolean.valueOf(s);
		}
		s = e.getChildText("LastModifiedDate", ns);
		if (s != null && !s.isEmpty())
		{
			LastModifiedDate = DateUtil.convertStringToStandardDateTimeAlt(r.getServerParameters().getProcessingTimeZone(), s);
		}
		s = e.getChildText("IncrementalSnapshotFact", ns);
		if (s != null)
		{
			IncrementalSnapshotFact = Boolean.valueOf(s);
		}
		s = e.getChildText("SCDDateName", ns);
		if (s != null)
		{
			SCDDateName = s;
		}
		s = e.getChildText("WebOptional", ns);
		if (s != null)
		{
			WebOptional = Boolean.parseBoolean(s);
		}
		s = e.getChildText("WebReadOnly", ns);
		if (s != null)
		{
			WebReadOnly = Boolean.parseBoolean(s);
		}
		s = e.getChildText("Consolidation", ns);
		if (s != null)
		{
			Consolidation = Boolean.parseBoolean(s);
		}	
		s = e.getChildText("UseBirstLocalForSource", ns);
		if (s != null)
		{
			UseBirstLocalForSource = Boolean.parseBoolean(s);
		}	
		SourceFile = e.getChildTextTrim("SourceFile", ns);
		Query = e.getChildTextTrim("Query", ns);
		SourceGroupStr = e.getChildTextTrim("SourceGroups", ns);
		if (SourceGroupStr != null && SourceGroupStr.length() > 0)
		{
			SourceGroups = SourceGroupStr.split(",");
		}
		cksumGroupStr = e.getChildTextTrim("ChecksumGroup", ns);
		if (cksumGroupStr != null && cksumGroupStr.length() > 0)
		{
			ChecksumGroup = cksumGroupStr;
		}
		Element lvls = e.getChild("Levels", ns);
		if (lvls != null)
		{
			List l2 = lvls.getChildren();
			Levels = new String[l2.size()][];
			for (int count2 = 0; count2 < l2.size(); count2++)
			{
				Element l3 = (Element) l2.get(count2);
				Levels[count2] = new String[2];
				List children = l3.getChildren();
				Levels[count2][0] = Util.intern(((Element) children.get(0)).getValue());
				Levels[count2][1] = Util.intern(((Element) children.get(1)).getValue());
			}
		}
		lvls = e.getChild("LevelFilters", ns);
		if (lvls != null)
		{
			List l2 = lvls.getChildren();
			LevelFilters = new String[l2.size()][];
			for (int count2 = 0; count2 < l2.size(); count2++)
			{
				Element l3 = (Element) l2.get(count2);
				LevelFilters[count2] = new String[3];
				List children = l3.getChildren();
				LevelFilters[count2][0] = ((Element) children.get(0)).getValue();
				LevelFilters[count2][1] = ((Element) children.get(1)).getValue();
				LevelFilters[count2][2] = ((Element) children.get(2)).getValue();
			}
		}
		Element cols = e.getChild("Columns", ns);
		if (cols != null)
		{
			List l2 = cols.getChildren();
			Columns = new StagingColumn[l2.size()];
			for (int count2 = 0; count2 < l2.size(); count2++)
			{
				StagingColumn sc = new StagingColumn(this, (Element) l2.get(count2), ns);
				Columns[count2] = sc;
			}
		}
		Element overrideLevelKs = e.getChild("OverrideLevelKeys",ns);
		if (overrideLevelKs!=null)
		{
		  List l2 = overrideLevelKs.getChildren();
      OverrideLevelKeys = new OverrideLevelKey[l2.size()];
      for (int count2 = 0; count2 < l2.size(); count2++)
      {
        OverrideLevelKey olk = new OverrideLevelKey((Element) l2.get(count2), ns);
        OverrideLevelKeys[count2] = olk;
      }
		}
		
		Element customTimeShiftElems = e.getChild("CustomTimeShifts",ns);
    if (customTimeShiftElems!=null)
    {
      List l2 = customTimeShiftElems.getChildren();
      CustomTimeShifts = new CustomTimeShift[l2.size()];
      for (int count2 = 0; count2 < l2.size(); count2++)
      {
        CustomTimeShift cts = new CustomTimeShift((Element) l2.get(count2), ns);
        CustomTimeShifts[count2] = cts;
      }
    }
    
    Element elemFKeys = e.getChild("ForeignKeys",ns);
    if (elemFKeys!=null)
    {
      List l2 = elemFKeys.getChildren();
      ForeignKeys = new ForeignKey[l2.size()];
      for (int count2 = 0; count2 < l2.size(); count2++)
      {
        ForeignKey fk = new ForeignKey((Element) l2.get(count2), ns);
        ForeignKeys[count2] = fk;
      }
    }
    
    elemFKeys = e.getChild("FactForeignKeys",ns);
    if (elemFKeys!=null)
    {
      List l2 = elemFKeys.getChildren();
      FactForeignKeys = new ForeignKey[l2.size()];
      for (int count2 = 0; count2 < l2.size(); count2++)
      {
        ForeignKey fk = new ForeignKey((Element) l2.get(count2), ns);
        FactForeignKeys[count2] = fk;
      }
    }
    
    Element elemInheritTables = e.getChild("InheritTables",ns);
    if (elemInheritTables!=null)
    {
      List l2 = elemInheritTables.getChildren();
      InheritTables = new InheritTable[l2.size()];
      for (int count2 = 0; count2 < l2.size(); count2++)
      {
        InheritTable iT = new InheritTable((Element) l2.get(count2), ns);
        InheritTables[count2] = iT;
      }
    }
		
		Element te = e.getChild("Transformations", ns);
		if (te != null)
		{
			List l2 = te.getChildren();
			Transformations = new Transformation[l2.size()];
			for (int count2 = 0; count2 < l2.size(); count2++)
			{
				Transformations[count2] = Transformation.createStagingTransformation(Util.replaceWithPhysicalString(this.Name), this, (Element) l2.get(count2),
						ns);
			}
		}
		LoadGroupNames = Repository.getStringArrayChildren(e, "LoadGroups", ns);
		SubGroupNames = Repository.getStringArrayChildren(e, "SubGroups", ns);
		if (SubGroupNames.length == 0)
			SubGroupNames = null;
		DayIDColumnsMappedToLoadDate = new HashSet<String>();
		nameToStagingColumnMap = new HashMap<String, StagingColumn>();
		sfColNameToStagingColumnMap = new HashMap<String, StagingColumn>();
		if (Columns != null && Columns.length > 0)
		{
			for (StagingColumn sc : Columns)
			{
				nameToStagingColumnMap.put(sc.Name, sc);
				if (sc.SourceFileColumn != null && (sc.Transformations == null || sc.Transformations.length == 0) && !sc.DataType.startsWith(DateID.Prefix))
				{
					sfColNameToStagingColumnMap.put(sc.SourceFileColumn, sc);
				}
				if (sc.Transformations != null && sc.Transformations.length > 0)
				{
					for (Transformation t : sc.Transformations)
					{
						if (t.getClass().equals(StaticDateID.class))
						{
							StaticDateID dateID = (StaticDateID) t;
							if (dateID.DateType == TimePeriod.PERIOD_DAY && dateID.Expression != null && dateID.Expression.equals("V{LoadDate}"))
							{
								DayIDColumnsMappedToLoadDate.add(sc.Name);
							}
						}
					}
				}
			}
		}
		Element ce = e.getChild("Script", ns);
		if (ce != null)
		{
			Script = new ScriptDefinition(ce, ns);
		}
		ce = e.getChild("Snapshots", ns);
		if (ce != null)
			Snapshots = new SnapshotPolicy(ce, ns);
		SnapshotDeleteKeys = Repository.getStringArrayChildren(e, "SnapshotDeleteKeys", ns);
		SourceKeys = Repository.getStringArrayChildren(e, "SourceKey", ns);
		ForeignKeySources = Repository.getStringArrayChildren(e, "ForeignKeySources", ns);
		FactForeignKeySources = Repository.getStringArrayChildren(e, "ForeignKeySources", ns);
		
		Element elemLocations = e.getChild("Locations",ns);
		if (elemLocations!=null)
		{
			List lList = elemLocations.getChildren();
			locations = new Location[lList.size()];
			for (int count=0;count<lList.size();count++)
			{
				Element ele = (Element) lList.get(count);
				locations[count] = new Location(ele,ns);
			}
		}
		s = e.getChildText("Autoplace", ns);
    if (s != null)
    {
      Autoplace = Boolean.parseBoolean(s);
    } 
    MeasureNamePrefix = e.getChildText("MeasureNamePrefix", ns);
    s = e.getChildText("UnCached", ns);
    if (s != null)
    {
      UnCached = Boolean.parseBoolean(s);
    }
    s = e.getChildText("Cardinality", ns);
    if (s != null)
    {
      Cardinality = Integer.parseInt(s);
    }     
    if (e.getChild("TableSource",ns)!=null)
    {
      TableSource = new TableSource(e.getChild("TableSource",ns), ns, true);
    }
	}
	
	public Element getStagingTableElement(Namespace ns)
	{
		Element e = new Element("StagingTable",ns);
		
		Repository.addBaseAuditObjectAttributes(e, Guid, CreatedDate, strLastModifiedDate, CreatedUsername, LastModifiedUsername, ns);
		
		Element child = new Element("Name",ns);
		child.setText(Name);
		e.addContent(child);
		
		if (LogicalName!=null)
		{
			child = new Element("LogicalName",ns);
			child.setText(LogicalName);
			e.addContent(child);
		}
		
		child = new Element("ID",ns);
		child.setText(String.valueOf(ID));
		e.addContent(child);
		
		child = new Element("SourceType",ns);
		child.setText(String.valueOf(SourceType));
		e.addContent(child);
		
		XmlUtils.addContent(e, "SourceFile", SourceFile,ns);
		
		if (Query!=null)
		{
			child = new Element("Query",ns);
			child.setText(Query);
			e.addContent(child);
		}
		
		child = new Element("PersistQuery",ns);
		child.setText(String.valueOf(PersistQuery));
		e.addContent(child);
		
		child = new Element("Levels",ns);
		if (Levels!=null)
		{
			for (String[] lvl1 : Levels)
			{
				Element arrayOfString = new Element("ArrayOfString",ns);
				for (String lvl2 : lvl1)
				{
					Element stringElement = new Element("string",ns);
					stringElement.setText(lvl2);
					arrayOfString.addContent(stringElement);
				}
				child.addContent(arrayOfString);
			}
		}
		e.addContent(child);
		
		child = new Element("LevelFilters",ns);
		if (LevelFilters!=null)
		{
			for (String[] lvl1 : LevelFilters)
			{
				Element arrayOfString = new Element("ArrayOfString",ns);
				for (String lvl2 : lvl1)
				{
					Element stringElement = new Element("string",ns);
					stringElement.setText(lvl2);
					arrayOfString.addContent(stringElement);
				}
				child.addContent(arrayOfString);
			}
		}
		e.addContent(child);
		
		child = new Element("Columns",ns);
		if (Columns!=null && Columns.length>0)
		{
			for (StagingColumn sc : Columns)
			{
				child.addContent(sc.getStagingColumnElement(ns));
			}
		}
		e.addContent(child);
		
		child = new Element("Disabled",ns);
		child.setText(String.valueOf(Disabled));
		e.addContent(child);
		
		child = new Element("Transformations",ns);
		if (Transformations!=null && Transformations.length>0)
		{
			for (Transformation ts : Transformations)
			{
				child.addContent(ts.getTransformationElement(ns));
			}
		}
		e.addContent(child);
		
		child = new Element("LoadGroups",ns);
		if (LoadGroupNames!=null)
		{
			for (String loadGrpName : LoadGroupNames)
			{
				Element stringElement = new Element("string",ns);
				stringElement.setText(loadGrpName);
				child.addContent(stringElement);
			}
		}
		e.addContent(child);
		
		if (SubGroupNames!=null && SubGroupNames.length>0)
		{
			e.addContent(Repository.getStringArrayElement("SubGroups", SubGroupNames, ns));
		}
		
		child = new Element("WebOptional",ns);
		child.setText(String.valueOf(WebOptional));
		e.addContent(child);
		
		child = new Element("WebReadOnly",ns);
		child.setText(String.valueOf(WebReadOnly));
		e.addContent(child);
		
		child = new Element("Consolidation",ns);
		child.setText(String.valueOf(Consolidation));
		e.addContent(child);
		
		child = new Element("Transactional",ns);
		child.setText(String.valueOf(Transactional));
		e.addContent(child);
		
		child = new Element("TruncateOnLoad",ns);
		child.setText(String.valueOf(TruncateOnLoad));
		e.addContent(child);
		
		if (SourceGroupStr!=null)
		{
		  XmlUtils.addContent(e, "SourceGroups", SourceGroupStr,ns);
		}
		
		if (cksumGroupStr!=null)
		{
			child = new Element("ChecksumGroup",ns);
			child.setText(cksumGroupStr);
			e.addContent(child);
		}
		
		child = new Element("IncrementalSnapshotFact",ns);
		child.setText(String.valueOf(IncrementalSnapshotFact));
		e.addContent(child);
		
		if (SnapshotDeleteKeys!=null && SnapshotDeleteKeys.length>0)
		{
		  e.addContent(Repository.getStringArrayElement("SnapshotDeleteKeys", SnapshotDeleteKeys, ns));
		}
		
		if (OverrideLevelKeys!=null && OverrideLevelKeys.length>0)
		{
		  child = new Element("OverrideLevelKeys",ns);
	    for (OverrideLevelKey olk : OverrideLevelKeys)
	    {
	      child.addContent(olk.getOverrideLevelKeyElement(ns));
	    }
	    e.addContent(child);
		}
		
		if (SCDDateName!=null)
		{
		  child = new Element("SCDDateName",ns);
	    child.setText(SCDDateName);
	    e.addContent(child);
		}
		
		if (Script!=null)
		{
			e.addContent(Script.getScriptDefinitionElement(ns));	
		}
		
		if (CustomTimeColumn!=null)
		{
		  XmlUtils.addContent(e, "CustomTimeColumn", CustomTimeColumn,ns);
		}
		
		if (CustomTimePrefix!=null)
		{
		  XmlUtils.addContent(e, "CustomTimePrefix", CustomTimePrefix,ns);
		}
		
		if (CustomTimeShifts!=null && CustomTimeShifts.length>0)
		{
		  child = new Element("CustomTimeShifts",ns);
      for (CustomTimeShift cts : CustomTimeShifts)
      {
        child.addContent(cts.getCustomTimeShiftElement(ns));
      }
      e.addContent(child);
		}
		
		if (Snapshots!=null)
		{
		  e.addContent(Snapshots.getSnapshotPolicyElement("Snapshots", ns));
		}
		
		if (NewColumnTarget!=null)
		{
		  XmlUtils.addContent(e, "NewColumnTarget", NewColumnTarget,ns);
		}
				
		if (SourceKeys!=null && SourceKeys.length>0)
		{
			child = new Element("SourceKey",ns);
			for (String sourceKeyName : SourceKeys)
			{
				Element stringElement = new Element("string",ns);
				stringElement.setText(sourceKeyName);
				child.addContent(stringElement);
			}
			e.addContent(child);
		}		
		
		if (ForeignKeys!=null && ForeignKeys.length>0)
		{
		  child = new Element("ForeignKeys",ns);
      for (ForeignKey fk : ForeignKeys)
      {
        child.addContent(fk.getForeignKeyElement(ns));
      }
      e.addContent(child);
		}
		
		if (FactForeignKeys!=null && FactForeignKeys.length>0)
    {
      child = new Element("FactForeignKeys",ns);
      for (ForeignKey fk : FactForeignKeys)
      {
        child.addContent(fk.getForeignKeyElement(ns));
      }
      e.addContent(child);
    }
		
		if (ForeignKeySources!=null && ForeignKeySources.length>0)
		{
		  e.addContent(Repository.getStringArrayElement("ForeignKeySources", ForeignKeySources, ns));
		}
		
		if (FactForeignKeySources!=null && FactForeignKeySources.length>0)
    {
      e.addContent(Repository.getStringArrayElement("FactForeignKeySources", ForeignKeySources, ns));
    }
		
		if (ParentForeignKeySource!=null)
		{
		  child = new Element("ParentForeignKeySource",ns);
      child.setText(ParentForeignKeySource);
      e.addContent(child);
		}
		
		if (HierarchyName!=null)
		{
			child = new Element("HierarchyName",ns);
			child.setText(HierarchyName);
			e.addContent(child);
		}
		
		if (LevelName!=null)
		{
			child = new Element("LevelName",ns);
			child.setText(LevelName);
			e.addContent(child);
		}
		
		if (ExcludeFromModel)
		{
			child = new Element("ExcludeFromModel",ns);
			child.setText(String.valueOf(ExcludeFromModel));
			e.addContent(child);
		}
		
		if (DiscoveryTable)
		{
			child = new Element("DiscoveryTable",ns);
			child.setText(String.valueOf(DiscoveryTable));
			e.addContent(child);
		}
		
		if (UseBirstLocalForSource)
		{
			child = new Element("UseBirstLocalForSource",ns);
			child.setText(String.valueOf(UseBirstLocalForSource));
			e.addContent(child);
		}
		
		if (PreviousLoadGroup!=null)
		{
			child = new Element("PreviousLoadGroup",ns);
			child.setText(PreviousLoadGroup);
			e.addContent(child);
		}
		
		if (locations!=null && locations.length>0)
		{
			child = new Element("Locations",ns);
			for (Location l : locations)
			{
				child.addContent(l.getLocationElement(ns));
			}
			e.addContent(child);
		}
		
		if (Autoplace)
		{
		  child = new Element("Autoplace",ns);
      child.setText(String.valueOf(Autoplace));
      e.addContent(child);
		}
		
		if (MeasureNamePrefix!=null &&  !("").equals(MeasureNamePrefix))
		{
		  child = new Element("MeasureNamePrefix",ns);
      child.setText(MeasureNamePrefix);
      e.addContent(child);
		}
		
		if (LiveAccess)
		{
		  child = new Element("LiveAccess",ns);
      child.setText(String.valueOf(LiveAccess));
      e.addContent(child);
		}
		
		if (UnCached)
    {
      child = new Element("UnCached",ns);
      child.setText(String.valueOf(UnCached));
      e.addContent(child);
    }
		
		if (TableSource!=null)
		{
		  e.addContent(TableSource.getTableSourceElement(ns));
		}
		
		if (InheritTables!=null && InheritTables.length>0)
		{
		  child = new Element("InheritTables",ns);
      for (InheritTable it : InheritTables)
      {
        child.addContent(it.getInheritTableElement(ns));
      }
      e.addContent(child);
		}
		
		if (Cardinality!=0)
		{
		  child = new Element("Cardinality",ns);
      child.setText(String.valueOf(Cardinality));
      e.addContent(child);
		}
		
		return e;
	}

	public void updateStagingTableQuery(Repository r)
	{
		String query = r.replaceVariables(null, this.Query);
		AbstractQueryString aqs = AbstractQueryString.getInstance(r, query, r.isUseNewQueryLanguage());
		try
		{
			query = aqs.replaceQueryString(query, true, null, false, false, null, null, null); // replace any Q{...}
																								// inside of a physical
																								// query
		} catch (Exception e)
		{
			logger.error("Failed to do logical query replacement for staging table : " + this.Name);
		}
		this.Query = query;
	}

	/**
	 * Get the columns to create a table
	 * 
	 * @return
	 */
	public void getCreateColumns(DatabaseConnection dbc, Repository r, List<String> colNames, List<String> colTypes, boolean unicodeDatabase,
			List<String> ckcolNames, List<String> ckcolTypes, List<String> columnCompressionEncodings, List<String> distributionKeys, Set<String> sortKeys)
	{
		// Make sure that the staging columns that are not mapped to any source file columns are pushed to the
		// end so that the BCP does the right thing. If they appear somewhere in the middle, since BCP is based
		// on the order of columns, it will perform bulk insert incorrectly.
		List<String> noMapColNames = new ArrayList<String>();
		List<String> noMapColTypes = new ArrayList<String>();
		boolean isPADB = DatabaseConnection.isDBTypeParAccel(dbc.DBType);
		List<String> noMapColEncodings = (isPADB ? new ArrayList<String>() : null);
		for (StagingColumn sc : Columns)
		{
			boolean add = sc.isPersistable();
			if (add)
			{
				String type = sc.DataType;
				if (type.equals("Varchar"))
				{
					int width = sc.Width;
					if (!dbc.hasVarcharMax() && width > dbc.getMaxVarcharWidth())
					{
						width = dbc.getMaxVarcharWidth();
					}
					type = dbc.getVarcharType(width, dbc.DBType != DatabaseType.MonetDB);					
				}
				String physicalName = Util.replaceWithNewPhysicalString(sc.getPhysicalName(), dbc,r);
				if (type.equals("Integer") && sc.NaturalKey)
					type = dbc.getDimensionalKeyType(r);
				else if(type.equals("Integer"))
					type = dbc.getIntegerType(r);
				
				if (SequenceNumber.getSequenceNumber(r, this, sc, false) == null && (sc.SourceFileColumn == null || sc.SourceFileColumn.trim().equals("")))
				{
					noMapColNames.add(physicalName);
					noMapColTypes.add(type);
					if (dbc.supportsColumnCompression() && noMapColEncodings != null)
					{
						noMapColEncodings.add(Database.getColumnCompressionEncoding(dbc, null, sc.DataType, sc.Width));
					}
				} else
				{
					colNames.add(physicalName);
					colTypes.add(type);
					if (dbc.supportsColumnCompression() && columnCompressionEncodings != null)
					{
						columnCompressionEncodings.add(Database.getColumnCompressionEncoding(dbc, null, sc.DataType, sc.Width));
					}
					if (isPADB && DiscoveryTable)
					{
						if (SourceKeys != null && SourceKeys.length > 0)
						{
							for (String sk : SourceKeys)
							{
								if (sk.equals(sc.Name))
								{
									if (distributionKeys != null && distributionKeys.size() == 0)
									{
										distributionKeys.add(physicalName);
									}
									else
									{
										if (sortKeys != null)
											sortKeys.add(physicalName);
									}
									break;
								}
							}
						}
						if (ForeignKeySources != null && ForeignKeySources.length > 0)
						{
							for (String fk : ForeignKeySources)
							{
								StagingTable fkSTable = r.findStagingTable(fk);
								if (fkSTable != null && fkSTable.SourceKeys != null && fkSTable.SourceKeys.length > 0)
								{
									for (String fkey : fkSTable.SourceKeys)
									{
										if (fkey.equals(sc.Name))
										{
											if (sortKeys != null)
												sortKeys.add(physicalName);
											break;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		if (ckcolNames != null && ckcolNames.size() > 0)
		{
			if (DatabaseConnection.isDBTypeMemDB(dbc.DBType))
			{
				for (int i = 0; i < ckcolNames.size(); i++)
				{
					colNames.add(ckcolNames.get(i));
					colTypes.add(ckcolTypes.get(i) + " NOINDEX");
				}
			} else
			{
				colNames.addAll(ckcolNames);
				colTypes.addAll(ckcolTypes);
				if (isPADB && columnCompressionEncodings != null)
				{
					for (int i = 0; i < ckcolTypes.size(); i++)
					{
						columnCompressionEncodings.add(Database.getChecksumColumnCompression());
					}
				}
			}
		}
		colNames.addAll(noMapColNames);
		colTypes.addAll(noMapColTypes);
		if (dbc.supportsColumnCompression() && noMapColEncodings != null && columnCompressionEncodings != null)
		{
			columnCompressionEncodings.addAll(noMapColEncodings);
		}
	}

	/**
	 * Replace any qualified logical column names from the staging table with their physical equivalents
	 * 
	 * @param result
	 * @return
	 */
	public String getReplacedString(String result, String alias)
	{
		for (StagingColumn sc : Columns)
		{
			result = Util.replaceStr(result, Name + ".\"" + sc.Name + "\"",
					(alias == null ? Name : alias) + "." + Util.replaceWithNewPhysicalString(sc.getPhysicalName(), r.getDefaultConnection(),r));
		}
		return (result);
	}

	public String getReplacedString(String result)
	{
		return getReplacedString(result, null);
	}

	public void setSize(DatabaseConnection dc, Statement stmt) throws SQLException
	{
		if (Size != null)
			return;
		if (SourceType == StagingTable.SOURCE_QUERY && !PersistQuery)
		{
			Size = 0L;
			return;
		}
		String query = "SELECT COUNT(*) FROM " + Database.getQualifiedTableName(dc, Util.replaceWithPhysicalString(Name));
		logger.debug(query);
		ResultSet rs = null;
		try
		{
			rs = stmt.executeQuery(query);
			if (rs.next())
			{
				Size = rs.getLong(1);
			}
		} finally
		{
			try
			{
				if (rs != null)
					rs.close();
			} catch (Exception e)
			{
				logger.debug(e, e);
			}
		}
	}

	public long getSize()
	{
		return Size;
	}

	public int compareTo(StagingTable other)
	{
		
		// See if at the same grain
		boolean same = true;
		if (Levels.length != other.Levels.length)
			same = false;
		if (same)
		{
			boolean match = true;
			for (String[] level : Levels)
			{
				boolean found = false;
				for (String[] level2 : other.Levels)
				{
					if (level[0].equals(level2[0]) && level[1].equals(level2[1]))
					{
						found = true;
						break;
					}
				}
				if (!found)
				{
					match = false;
					break;
				}
			}
			if (!match)
				same = false;
		}
		if (!same)
		{
			// Compare grains
			Set<MeasureTableGrain> grainset = new HashSet<MeasureTableGrain>();
			for (String[] level : other.Levels)
			{
				MeasureTableGrain mtg = new MeasureTableGrain();
				mtg.DimensionName = level[0];
				mtg.DimensionLevel = level[1];
				grainset.add(mtg);
			}
			if (isAtHigherGrain(r, grainset) != null)
			{
				return (-1);
			} else
			{
				grainset = new HashSet<MeasureTableGrain>();
				for (String[] level : Levels)
				{
					MeasureTableGrain mtg = new MeasureTableGrain();
					mtg.DimensionName = level[0];
					mtg.DimensionLevel = level[1];
					grainset.add(mtg);
				}
				if (other.isAtHigherGrain(r, grainset) != null)
				{
					return (1);
				}
			}
		}
		boolean isTreeSort =false;
		try{
			isTreeSort=	System.getProperty("UsingTreeSort") != null ? Boolean.parseBoolean(System.getProperty("UsingTreeSort")) : false;
		}catch(Exception ex){
		}
		if(isTreeSort)
		{
			return (Size.compareTo(other.Size)==0) ? 1 : Size.compareTo(other.Size);
		}else{
			return (Size.compareTo(other.Size));
		}
	}

	

	public Level getDimensionLevel(Repository r, String dimName)
	{
		Hierarchy h = r.findDimensionHierarchy(dimName);
		if (h == null)
			return (null);
		for (String[] level : Levels)
		{
			if (level[0].equals(dimName))
			{
				return (h.findLevel(level[1]));
			}
		}
		return (null);
	}

	/**
	 * Given a dimension column name, find the corresponding staging column, consider column prefixes in the search
	 * 
	 * @param dimColName
	 * @return staging column
	 */
	public StagingColumn findStagingColumn(String dimColName)
	{
		StagingColumn stagingColumn = null;
		for (StagingColumn sc : Columns)
		{
			if (sc.ExpandedColumnNames != null && sc.ExpandedColumnNames.size() > 0)
			{
				for (String ecName : sc.ExpandedColumnNames)
				{
					if (ecName.equals(dimColName))
					{
						stagingColumn = sc;
						break;
					}
				}
				if (stagingColumn != null)
				{
					break;
				}
			} else if (sc.OriginalName.equals(dimColName))
			{
				stagingColumn = sc;
				break;
			}
		}
		return stagingColumn;
	}

	/**
	 * Check to see if this staging table is part of the load group. If load group is null a value true is returned.
	 * 
	 * @param loadGroup
	 * @return
	 */
	public boolean belongsToLoadGroup(String loadGroup)
	{
		boolean belongs = false;
		if (loadGroup != null)
		{
			if (LoadGroupNames != null && LoadGroupNames.length > 0)
			{
				for (String lgName : LoadGroupNames)
				{
					if (loadGroup.equals(lgName))
					{
						belongs = true;
						break;
					}
				}
			}
		} else
		{
			belongs = true;
		}
		return belongs;
	}

	/**
	 * Check to see if this staging table is part of a set of sub groups. If null a value true is returned.
	 * 
	 * @param loadGroup
	 * @return
	 */
	public boolean belongsToSubGroups(String[] subGroups)
	{
		if (subGroups == null)
			return true;
		if (SubGroupNames != null)
		{
			if (subGroups.length == 0 && SubGroupNames.length == 0)
				return true;
			for (String sgName : subGroups)
			{
				if (Arrays.asList(SubGroupNames).indexOf(sgName) >= 0)
				{
					return true;
				}
			}
		} else if (subGroups.length == 0)
			return true;
		return false;
	}

	public String toString()
	{
		return (Name);
	}

	/**
	 * Verifies whether a staging table's grain is higher than a given grain set (and therefore can be used to load
	 * added foreign keys in the lower table). Returns the higher grain.
	 * 
	 * @param grainSet
	 * @return
	 */
	public Collection<MeasureTableGrain> isAtHigherGrain(Repository r, Collection<MeasureTableGrain> initialGrainSet)
	{
		List<MeasureTableGrain> grainSet = new ArrayList<MeasureTableGrain>();
		for (String[] level : Levels)
		{
			MeasureTableGrain mtg = new MeasureTableGrain();
			mtg.DimensionName = level[0];
			mtg.DimensionLevel = level[1];
			grainSet.add(mtg);
		}
		return MeasureTable.isAtHigherGrain(r, grainSet, initialGrainSet);
	}

	public void setRepository(Repository r)
	{
		this.r = r;
	}

	public boolean requiresTransformationAtStage(int executionStage)
	{
		if (this.Transformations != null)
		{
			for (Transformation tr : this.Transformations)
			{
				if (tr.ExecuteAfter == executionStage)
					return true;
			}
		}
		return false;
	}

	public String getChecksumName(boolean logical, DatabaseConnection dc)
	{
		if (logical)
		{
			if (ChecksumGroup != null)
				return ChecksumGroup + "_CKSUM";
			return Name + "_CKSUM";
		}
		String name = null;
		if (ChecksumGroup != null)
			name = Util.replaceWithPhysicalString(ChecksumGroup + "_CKSUM") + dc.getPhysicalSuffix();
		else
			name = Util.replaceWithPhysicalString(Name) + "_CKSUM" + dc.getPhysicalSuffix();
		name = dc.getChecksumName(name);
		return name;
	}

	/**
	 * Get the natural key for a given dimension and level
	 * 
	 * @param r
	 * @param dimensionName
	 * @param levelName
	 * @return
	 */
	public LevelKey getNaturalKey(Repository r, String dimensionName, String levelName)
	{
		Level l = r.findDimensionLevel(dimensionName, levelName);
		if(l != null && l.Keys != null)
		{
			for (LevelKey lk : l.Keys)
			{
				boolean match = true;
				for (String col : lk.ColumnNames)
				{
					boolean found = false;
					for (StagingColumn sc : Columns)
					{
						if (sc.columnNameMatchesDimensionColumn(col) && sc.NaturalKey)
						{
							found = true;
							break;
						}
					}
					if (!found)
					{
						match = false;
						break;
					}
				}
				if (match)
					return (lk);
			}
		}
		return (null);
	}

	/**
	 * Returns a list of all the staging columns from a given staging table that apply to a given dimension table
	 * 
	 * @param l
	 *            Level of dimension table
	 * @param dt
	 *            Dimension table
	 * @param stLevel
	 *            Level of the staging table
	 * @param includeNaturalKeys
	 *            Whether to include natural keys in this list or not
	 * @return
	 */
	public List<StagingColumn> getStagingColumnList(Repository r, Level l, DimensionTable dt, Level stLevel, boolean includeNaturalKeys)
	{
		List<StagingColumn> sclist = new ArrayList<StagingColumn>();
		for (DimensionColumn dc : dt.DimensionColumns)
		{
			if (!dc.SurrogateKey)
			{
				StagingColumn foundsc = null;
				for (StagingColumn sc : Columns)
				{
					/*
					 * Make sure all the natural level keys are added for the level of the table
					 */
					boolean isLevelKey = false;
					for (LevelKey lk : l.Keys)
					{
						if (!lk.SurrogateKey)
						{
							for (String s : lk.ColumnNames)
							{
								if (sc.Name.equals(s))
								{
									isLevelKey = true;
									break;
								}
							}
						}
					}
					if (sc.columnNameMatchesDimensionColumn(dc.ColumnName) && sc.isMatchingDataType(dc.DataType)
							&& (isLevelKey || (sc.TargetTypes != null && sc.TargetTypes.contains(dt.DimensionName))))
					{
						foundsc = sc;
						/*
						 * Prune all natural keys that can be found in staging tables at lower levels than this one
						 */
						if (foundsc.NaturalKey && !includeNaturalKeys)
						{
							if (StagingTable.keyExistsInStagingTableLowerThanLevel(r, stLevel, foundsc.Name))
								foundsc = null;
						} else if (!foundsc.NaturalKey && (sc.TargetTypes == null || !sc.TargetTypes.contains(dt.DimensionName)))
						{
							// If it's not a natural key and not targeted, then it won't exist so prune
							foundsc = null;
						}
						break;
					}
				}
				// If found, process
				if (foundsc != null)
				{
					sclist.add(foundsc);
				}
			}
		}
		return (sclist);
	}

	/**
	 * Determine whether a key exists in a staging table at a level lower than the one specified
	 * 
	 * @param r
	 * @param l
	 * @param name
	 * @return
	 */
	static boolean keyExistsInStagingTableLowerThanLevel(Repository r, Level l, String name)
	{
		for (StagingTable nkst : r.getStagingTables())
		{
			if (nkst.Disabled)
				continue;
			Level nklevel = nkst.getDimensionLevel(r, l.Hierarchy.DimensionName);
			if (nklevel == null)
				continue;
			if (l != nklevel && l.findLevel(nklevel) != null)
			{
				for (StagingColumn sc : nkst.Columns)
				{
					if (sc.Name.equals(name))
					{
						return (true);
					}
				}
			}
		}
		return (false);
	}
	
	public boolean isAtLowerGrain(Collection<MeasureTableGrain> grainSet)
	{
		boolean match = grainSet.size() < Levels.length;
		if (match)
		{
			int found = 0;
			for (MeasureTableGrain mtg : grainSet)
			{				
				for (String[] level : Levels)
				{
					if (level[0].equals(mtg.DimensionName) && level[1].equals(mtg.DimensionLevel))
					{
						found++;
					}
				}
			}
			match = grainSet.size() == found;
		}
		return (match);
	}

	/**
	 * Verifies whether a staging table's grain matches that of a given grain set
	 * 
	 * @param grainSet
	 * @return
	 */
	public boolean isAtMatchingGrain(Collection<MeasureTableGrain> grainSet)
	{
		// Make sure grains match
		boolean match = grainSet.size() == Levels.length;
		if (match)
		{
			for (MeasureTableGrain mtg : grainSet)
			{
				boolean found = false;
				for (String[] level : Levels)
				{
					if (level[0].equals(mtg.DimensionName) && level[1].equals(mtg.DimensionLevel))
					{
						found = true;
						break;
					}
				}
				if (!found)
				{
					match = false;
					break;
				}
			}
		}
		return (match);
	}

	/**
	 * Find the measure table at the grain of the staging table
	 * 
	 * @return
	 */
	public MeasureTable getMeasureTableAtGrain()
	{
		for (MeasureTable mt : r.MeasureTables)
		{
			Collection<MeasureTableGrain> mtg = mt.getGrainInfo(r);
			if (isAtMatchingGrain(mtg))
			{
				return mt;
			}
		}
		return null;
	}

	/**
	 * Find all the measure tables at the grain of the staging table
	 * 
	 * @return
	 */
	public List<MeasureTable> getAllMeasureTablesAtGrain()
	{
		List<MeasureTable> mtList = new ArrayList<MeasureTable>();
		for (MeasureTable mt : r.MeasureTables)
		{
			Collection<MeasureTableGrain> mtg = mt.getGrainInfo(r);
			if (isAtMatchingGrain(mtg))
			{
				mtList.add(mt);
			}
		}
		return mtList;
	}
	
	/**
	 * Find all dimension key columns that are associated with the grain or are in the same dimension and higher
	 * 
	 * @param grainSet
	 * @return
	 */
	public List<LevelKey> getLevelKeyList(Collection<MeasureTableGrain> grainSet, TimeDefinition td, boolean includeTime)
	{
		List<LevelKey> levelList = new ArrayList<LevelKey>();
		for (MeasureTableGrain mtg : grainSet)
		{
			Hierarchy h = r.findDimensionHierarchy(mtg.DimensionName);
			if (h == null)
				continue;
			Level l = h.findLevel(mtg.DimensionLevel);
			if (l == null)
				continue;
			List<Level> llist = new ArrayList<Level>();
			llist.add(l);
			l.getAllParents(llist);
			for (Level l2 : llist)
			{
				if (l2.Keys.length == 0)
					continue;
				/*
				 * Make sure all the columns in a key for this level are in the staging table. If so, add them to the
				 * level column list
				 */
				int count = 0;
				LevelKey lk = null;
				boolean found = false;
				do
				{
					boolean isTime = td != null && td.Name != null && td.Name.equals(h.DimensionName);
					if (!l2.Keys[count].SurrogateKey || isTime)
					{
						found = true;
						for (String s : l2.Keys[count].ColumnNames)
						{
							boolean foundsc = false;
							for (StagingColumn sc : Columns)
							{
								if (sc.NaturalKey && sc.columnNameMatchesDimensionColumn(s))
								{
									// Make sure this staging table is at this level
									boolean atLevel = false;
									for (String[] level : Levels)
									{
										if (level[0].equals(h.DimensionName) && level[1].equals(l2.Name))
											atLevel = true;
									}
									if (!atLevel)
										continue;
									/*
									 * If we are including time keys, make sure that the column is a valid date
									 * transformation
									 */
									if (!isTime || sc.DataType.startsWith(DateID.Prefix))
										foundsc = true;
									else if (isTime && sc.Transformations != null)
										for (Transformation t : sc.Transformations)
											if (StaticDateID.class.isInstance(t))
											{
												foundsc = true;
												break;
											}
									break;
								}
							}
							if (!foundsc)
							{
								found = false;
								break;
							}
						}
						if (found)
							lk = l2.Keys[count];
					}
				} while (!found && ++count < l2.Keys.length);
				if (found)
				{
					levelList.add(lk);
				}
			}
		}
		return (levelList);
	}

	/**
	 * Generate a map for a given staging table between each dimension table that is loaded from it and which columns
	 * are to be loaded from it.
	 * 
	 * @param r
	 * @return
	 */
	public Map<DimensionTable, List<StagingColumn>> getStagingColumnMap(Repository r)
	{
		Map<DimensionTable, List<StagingColumn>> scmap = new LinkedHashMap<DimensionTable, List<StagingColumn>>();
		for (DimensionTable dt : r.DimensionTables)
		{
			if (dt.AutoGenerated && !dt.Calculated && dt.TableSource.ConnectionName.equals(r.getDefaultConnectionName()))
			{
				Level stLevel = getDimensionLevel(r, dt.DimensionName);
				Level dtLevel = r.findDimensionLevel(dt.DimensionName, dt.Level);
				LevelKey lk = getNaturalKey(r, dt.DimensionName, dt.Level);
				if (dtLevel == null || stLevel == null)
				{
					continue;
				}
				if (lk == null)
				{
					lk = getNaturalKey(r, dt.DimensionName, stLevel.Name);
					if (lk == null || stLevel.findLevel(dtLevel) == null)
					{
						continue;
					}
				}
				boolean includeNaturalKeys = dt.isIncludeNaturalKeys(r, this, dtLevel, stLevel);
				List<StagingColumn> sclist = getStagingColumnList(r, dtLevel, dt, stLevel, includeNaturalKeys);
				if (sclist.size() > 0)
				{
					if (!includeNaturalKeys)
					{
						/*
						 * If there are columns other than natural keys that can be updated, make sure to include the
						 * natural keys as well so that the checksum on the staging table is calculated correctly.
						 */
						sclist = getStagingColumnList(r, dtLevel, dt, stLevel, true);
					}
					scmap.put(dt, sclist);
				}
			}
		}
		return (scmap);
	}

	public StagingColumn getSCDDateColumn()
	{
		if (SCDDateName != null && SCDDateName.length() > 0)
		{
			for (StagingColumn sc : Columns)
			{
				if (sc.Name.equals(SCDDateName))
				{
					return sc;
				}
			}
		}
		return null;
	}

	public Map<Level, String> getCompoundKeys()
	{
		Map<Level, String> m = new HashMap<Level, String>();
		// Add any compound key columns
		for (String[] level : Levels)
		{
			Hierarchy h = r.findDimensionHierarchy(level[0]);
			if (h == null)
				continue;
			Level l = h.findLevel(level[1]);
			if (l == null)
				continue;
			List<Level> llist = new ArrayList<Level>();
			llist.add(l);
			l.getAllParents(llist);
			for (Level cl : llist)
			{
				LevelKey lk = cl.getNaturalKey();
				if (lk == null)
					continue;
				if (lk.ColumnNames.length > 1 && lk.isCompoundKeyRequired(r))
				{
					int foundCount = 0;
					for (StagingColumn sc : Columns)
					{
						for (String s : lk.ColumnNames)
						{
							if (s.equals(sc.Name))
							{
								foundCount++;
							}
						}
					}
					if (foundCount < lk.ColumnNames.length)
						continue;
					int width = 0;
					for (StagingColumn sc : Columns)
					{
						boolean found = false;
						for (String s : lk.ColumnNames)
						{
							if (s.equals(sc.Name))
							{
								found = true;
								break;
							}
						}
						if (found)
						{
							if (width > 0)
								width++; // Separator
							if (sc.DataType.equals("Varchar"))
								width += sc.Width + 1;
							else if (sc.DataType.equals("Integer"))
								width += (r.getCurrentBuilderVersion() < Repository.IntToLongBuilderVersion) ? 11 : 25;
							else if (sc.DataType.equals("Float"))
								width += (r.getCurrentBuilderVersion() < Repository.IntToLongBuilderVersion) ? 15 : 25;
							else if (sc.DataType.equals("Number"))
								width += (r.getCurrentBuilderVersion() < Repository.IntToLongBuilderVersion) ? 20 : 25;
							else if (sc.DataType.equals("Date"))
								width += 20;
							else if (sc.DataType.equals("DateTime"))
								width += 20;
							else
								width += sc.Width + 1;
						}
					}
					m.put(cl, "VARCHAR(" + width + ")");
				}
			}
		}
		return m;
	}

	public List<String> getGrainKey()
	{
		Map<Level, String> m = new HashMap<Level, String>();
		List<String> result = new ArrayList<String>();
		// Add any compound key columns
		for (String[] level : Levels)
		{
			Hierarchy h = r.findDimensionHierarchy(level[0]);
			if (h == null)
				continue;
			Level l = h.findLevel(level[1]);
			if (l == null)
				continue;
			List<Level> llist = new ArrayList<Level>();
			llist.add(l);
			l.getAllParents(llist);
			for (Level cl : llist)
			{
				LevelKey lk = cl.getNaturalKey();
				if (lk == null)
					continue;
				int foundCount = 0;
				for (StagingColumn sc : Columns)
				{
					for (String s : lk.ColumnNames)
					{
						if (s.equals(sc.Name))
						{
							foundCount++;
						}
					}
				}
				if (foundCount < lk.ColumnNames.length)
					continue;
				int width = 0;
				for (StagingColumn sc : Columns)
				{
					boolean found = false;
					for (String s : lk.ColumnNames)
					{
						if (s.equals(sc.Name))
						{
							found = true;
							break;
						}
					}
					if (found)
					{
						if (width > 0)
							width++; // Separator
						if (sc.DataType.equals("Varchar"))
							width += sc.Width + 1;
						else if (sc.DataType.equals("Integer"))
							width += 11;
						else if (sc.DataType.equals("Float"))
							width += 15;
						else if (sc.DataType.equals("Number"))
							width += 20;
						else if (sc.DataType.equals("Date"))
							width += 20;
						else if (sc.DataType.equals("DateTime"))
							width += 20;
						else
							width += sc.Width + 1;
					}
				}
				m.put(cl, "VARCHAR(" + width + ")");
			}
		}
		return result;
	}

	/**
	 * Return all date id columns defined for the given staging column
	 * 
	 * @param sc
	 * @return list of date id staging columns
	 */
	public List<StagingColumn> getDateIDColumns(StagingColumn sc)
	{
		// The incoming staging column have to be of data types of datetime or date so that we can figure out the
		// associated
		// date id columns
		if (!(sc.DataType.equals("DateTime") || sc.DataType.equals("Date")))
		{
			return null;
		}
		List<StagingColumn> dateIDCols = new ArrayList<StagingColumn>();
		for (int i = 0; i < Columns.length; i++)
		{
			// Ignore self
			if (sc.Name.equals(Columns[i].Name))
			{
				continue;
			}
			// Look for date id columns only
			if (!(Columns[i].DataType.startsWith(DateID.Prefix)))
			{
				continue;
			}
			if (sc.SourceFileColumn != null && Columns[i].SourceFileColumn != null && sc.SourceFileColumn.equals(Columns[i].SourceFileColumn))
			{
				dateIDCols.add(Columns[i]);
			}
		}
		return dateIDCols;
	}

	/**
	 * Create and return a staging table bulk load info object. This object holds information on staging table for bulk
	 * load like date id columns, sequence numbers, expressions etc. The purpose is for scripting engine and warehouse
	 * loading process to use this same underlying object while trying to preprocess the source files during script
	 * execution and load staging process respectively.
	 * 
	 * @param sf
	 *            Source file
	 * @param dc
	 *            Database Connection
	 * @param r
	 *            Repository
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public StagingTableBulkLoadInfo getStagingTableBulkLoadInfo(SourceFile sf, DatabaseConnection dc, Repository r)
	{
		StagingTableBulkLoadInfo stbl = new StagingTableBulkLoadInfo(this, sf, dc, r);
		// First figure out the number of date id, expressions and sequence number columns in the staging table.
		stbl.dateIDCount = 0;
		stbl.scExpressionCount = 0;
		stbl.sequenceNumberCount = 0;
		for (StagingColumn stc : Columns)
		{
			if (stc.SourceFileColumn == null || stc.SourceFileColumn.length() == 0)
			{
				if (SequenceNumber.getSequenceNumber(r, this, stc, false) != null)
					stbl.sequenceNumberCount++;
				else
					continue;
			}
			if (stc.DataType.startsWith(DateID.Prefix))
			{
				stbl.dateIDCount++;
			} else if (sf.isValidColumnExpression(stc.SourceFileColumn))
			{
				stbl.scExpressionCount++;
			}
		}
		// loop over the records - source file data fields
		int length = sf.Columns.length + stbl.dateIDCount + stbl.scExpressionCount + stbl.sequenceNumberCount;
		for (int i = 0; i < length; i++)
		{
			SourceColumn sc = i < sf.Columns.length ? sf.Columns[i] : null;
			int index = i + 1;
			// assumes that they are sorted by position
			if (sc != null)
				stbl.sourceFileColumnsMap.put(sc.Name, index);
		}
		int numExtraColumns = 0;
		for (StagingColumn stc : Columns)
		{
			if (!stc.isPersistable())
				continue;
			String type = stc.DataType;
			String SourceFileColumnName = stc.SourceFileColumn;
			if (sf.isValidColumnExpression(SourceFileColumnName))
			{
				SourceFileColumnName = sf.replaceColumnNamesWithIndices(SourceFileColumnName);
				if (!type.startsWith(DateID.Prefix))
				{
					stbl.scExpressions.add(SourceFileColumnName);
					numExtraColumns++;
				}
			}
			DateID di = null;
			if (type.startsWith(DateID.Prefix))
			{
				if (SourceFileColumnName == null || SourceFileColumnName.length() == 0)
					continue; // don't include ID columns that are not mapped
				di = new DateID();
				di.SourceFileColumnName = SourceFileColumnName;
				di.setType(type);
				di.Index = stbl.dateIDColumns.size();
				// Set unknown value
				for (StagingColumn ssc : Columns)
				{
					if (ssc == stc)
						continue;
					if (ssc.UnknownValue != null && ssc.UnknownValue.length() > 0 && ssc.SourceFileColumn != null
							&& ssc.SourceFileColumn.equals(stc.SourceFileColumn) && (ssc.DataType.equals("Date") || ssc.DataType.equals("DateTime")))
					{
						Calendar cal = Calendar.getInstance();
						DateID di2 = new DateID();
						di2.SourceFileColumnName = ssc.SourceFileColumn;
						di2.setType(stc.DataType);
						Date unknownDateVal = DateUtil.parseUsingPossibleFormats(ssc.UnknownValue);
						cal.setTimeInMillis(unknownDateVal.getTime());
						di.UnknownValue = di2.getDateID(r.getTimeDefinition(), cal);
						break;
					}
				}
				stbl.dateIDColumns.add(di);
				type = "SQLINT";
				numExtraColumns++;
			} else if (type.equals("None"))
			{
				// Smallest datatype implicitly convertable into anything
				type = "SQLTINYINT";
			}
			Integer index = stbl.sourceFileColumnsMap.get(SourceFileColumnName);
			SequenceNumber sn = null;
			if (index == null && !sf.isValidColumnExpression(SourceFileColumnName))
			{
				/*
				 * See if this column is a natural key column and is a single column level key - if so, create a
				 * sequence number for this column to create a natural key from scratch
				 */
				sn = SequenceNumber.getSequenceNumber(r, this, stc, true);
				if (sn != null)
				{
					stbl.sequenceNumberMap.put(stc.Name, sn);
					numExtraColumns++;
				} else
					// skip column not in source file column list
					// (transformation column?) and is not a column
					// expression
					continue;
			}
			if (di != null || sf.isValidColumnExpression(SourceFileColumnName) || sn != null)
			{
				index = sf.Columns.length + numExtraColumns;
				if (di != null)
					stbl.extraColumnMap.put(di, index - 1);
				else if (sn != null)
					stbl.extraColumnMap.put(sn, index - 1);
				else
					stbl.extraColumnMap.put(SourceFileColumnName, index - 1);
			}
		}
		stbl.idColumns = new List[sf.Columns.length];
		stbl.formats = new Format[sf.Columns.length];
		stbl.replaceColumns = new boolean[stbl.dateIDColumns.size()];
		for (int i = 0; i < sf.Columns.length; i++)
		{
			if (sf.Columns[i].Format != null && !sf.Columns[i].isSpecialFormat())
			{
				if (sf.Columns[i].DataType == SourceColumn.Type.DateTime || sf.Columns[i].DataType == SourceColumn.Type.Date)
				{
					stbl.formats[i] = new SimpleDateFormat(sf.Columns[i].Format);
				} else
				{
					stbl.formats[i] = new DecimalFormat(sf.Columns[i].Format);
				}
			}
			for (int j = 0; j < stbl.dateIDColumns.size(); j++)
			{
				if (sf.isValidColumnExpression(stbl.dateIDColumns.get(j).SourceFileColumnName))
				{
					stbl.replaceColumns[j] = true;
				}
				if (stbl.dateIDColumns.get(j).SourceFileColumnName.equals(sf.Columns[i].Name))
				{
					if (stbl.idColumns[i] == null)
						stbl.idColumns[i] = new ArrayList<DateID>();
					stbl.idColumns[i].add(stbl.dateIDColumns.get(j));
				}
			}
			if (sf.FilterColumn != null && sf.FilterColumn.length() > 0 && sf.Columns[i].Name.equals(sf.FilterColumn))
			{
				// Varchar, Integer, Number, DateTime, Float, Date, None, BadDataType
				if (sf.Columns[i].DataType == SourceColumn.Type.Varchar)
				{
					stbl.filterValue = sf.FilterValue;
				} else if (sf.Columns[i].DataType == SourceColumn.Type.Integer)
				{
					try
					{
						stbl.filterValue = Integer.valueOf(sf.FilterValue);
					} catch (NumberFormatException nfe)
					{
						logger.error("Invalid filter value\"" + sf.FilterValue + "\" specified for source file: " + sf.FileName
								+ " - needs to be a parsable integer." + "Filter will not be applied during preprocessing.");
						stbl.filterValue = null;
					}
				} else if (sf.Columns[i].DataType == SourceColumn.Type.Number || sf.Columns[i].DataType == SourceColumn.Type.Float)
				{
					try
					{
						stbl.filterValue = Double.valueOf(sf.FilterValue);
					} catch (NumberFormatException nfe)
					{
						logger.error("Invalid filter value\"" + sf.FilterValue + "\" specified for source file: " + sf.FileName
								+ " - needs to be a parsable number." + "Filter will not be applied during preprocessing.");
						stbl.filterValue = null;
					}
				} else if (sf.Columns[i].DataType == SourceColumn.Type.DateTime || sf.Columns[i].DataType == SourceColumn.Type.Date)
				{
					if (stbl.formats[i] == null)
					{
						logger.error("No format is specified for source file column \"" + sf.FileName + sf.Columns[i].Name + "\"."
								+ "You must specify a valid date format for filtering on date/datetime columns."
								+ "Filter will not be applied during preprocessing.");
						stbl.filterValue = null;
					} else
					{
						try
						{
							((SimpleDateFormat) stbl.formats[i]).setTimeZone(sf.getInputTimeZone());
							stbl.filterValue = Calendar.getInstance();
							((Calendar) stbl.filterValue).setTimeInMillis(((SimpleDateFormat) stbl.formats[i]).parse(sf.FilterValue).getTime());
						} catch (ParseException pe)
						{
							logger.error("Invalid filter value\"" + sf.FilterValue + "\" specified for source file: " + sf.FileName
									+ " - needs to be a parsable date." + "Filter will not be applied during preprocessing.");
							stbl.filterValue = null;
						}
					}
				} else if (sf.Columns[i].DataType == SourceColumn.Type.None || sf.Columns[i].DataType == SourceColumn.Type.BadDataType)
				{
					logger.error("Filter value\"" + sf.FilterValue + "\" specified for source file: " + sf.FileName + " on column with none or bad data type."
							+ "Filter will not be applied during preprocessing.");
					stbl.filterValue = null;
				}
				if (stbl.filterValue != null)
				{
					stbl.filterColumn = i;
					stbl.filterOperator = sf.FilterOperator;
					logger.debug("Filter Column " + sf.FilterColumn + " is at index " + stbl.filterColumn + ", operator is '" + stbl.filterOperator
							+ "' and value is '" + stbl.filterValue + "'");
				} else
				{
					logger.warn("Filter Column " + sf.FilterColumn + " is specified, but not being used");
				}
			}
		}
		if (dc.DBType == DatabaseType.Infobright)
		{
			// Add compound keys
			Map<Level, String> ckeys = this.getCompoundKeys();
			if (ckeys != null && ckeys.size() > 0)
				stbl.compoundKeys = ckeys;
			// Add grain keys
			stbl.setupGrainKeys(sf);
		}
		if (stbl.compoundKeys != null)
		{
			/*
			 * If there is a need to concatenate compound keys, then setup indices
			 */
			stbl.compoundKeyIndices = new ArrayList<int[]>();
			for (Entry<Level, String> en : stbl.compoundKeys.entrySet())
			{
				LevelKey lk = en.getKey().getNaturalKey();
				List<Integer> ilist = new ArrayList<Integer>();
				if(lk.isCompoundKeyRequired(r))
				{
					for (String s : lk.ColumnNames)
					{
						for (StagingColumn sc : this.Columns)
						{
							if (!sc.Name.equals(s) || sc.SourceFileColumn == null || sc.SourceFileColumn.length() == 0)
								continue;
							int index = 0;
							for (SourceColumn sfc : sf.Columns)
							{
								if (sc.SourceFileColumn.equals(sfc.Name))
								{
									ilist.add(index);
									break;
								}
								index++;
							}
						}
					}
					int[] iarr = new int[ilist.size()];
					for (int i = 0; i < iarr.length; i++)
						iarr[i] = ilist.get(i);
					stbl.compoundKeyIndices.add(iarr);
				}
			}
		}
		return stbl;
	}

	public StagingColumn getStagingColumn(String scname)
	{
		return nameToStagingColumnMap.get(scname);
	}

	public StagingColumn getStagingColumnForSourceFileColumn(String sourceFileColname)
	{
		return sfColNameToStagingColumnMap.get(sourceFileColname);
	}

	public void replaceNameWithHashedIdentifier(DatabaseConnection dconn)
	{
		if (Columns == null || Columns.length == 0)
			return;
		for (StagingColumn sc : Columns)
		{
			String pname = Util.replaceWithNewPhysicalString(sc.getPhysicalName(), dconn,r);
			String hashedPName = dconn.getHashedIdentifier(pname);
			if (!pname.equals(hashedPName))
				sc.PhysicalName = hashedPName;
		}
	}
	
	public List<TableSourceFilter> getSecurityFilters(String tableName, DatabaseConnection dc)
	{
		List<TableSourceFilter> result = new ArrayList<TableSourceFilter>();
		for (StagingColumn sc : Columns)
		{
			if (sc.SecFilter == null || !sc.SecFilter.Enabled)
				continue;
			String filterPart = null;
			if (sc.SecFilter.Type == SecurityFilter.TYPE_VARIABLE)
			{
				filterPart = " IN (V{" + sc.SecFilter.SessionVariable + "})";
			} else
			{
				// set based filter. Put logical query directly and wrap it with Q construct
				// Q construct will be expanded while building where clause for table source filters
				filterPart = " IN (Q{" + sc.SecFilter.SessionVariable + "})";
			}
			TableSourceFilter tsf = new TableSourceFilter();
			tsf.Filter = sc.getQualifiedPhysicalName(tableName, dc) + filterPart;
			tsf.SecurityFilter = true;
			if (sc.SecFilter.FilterGroups != null)
			{
				tsf.FilterGroups = sc.SecFilter.FilterGroups;
			}
			result.add(tsf);
		}
		return result;
	}
	
	public String getDisplayName()
	{
            String fname = null;
            int extindex = SourceFile.lastIndexOf('.');
            if (extindex >= 0)
                fname = SourceFile.substring(0, extindex);
            else
                fname = SourceFile;
            return fname;
	}
	
	public boolean isScript()
	{
		return Script != null && Script.InputQuery != null && Script.Script != null && Script.Script.length() > 0;
	}
	
	public List<StagingTable> getAncestors()
	{
		List<StagingTable> ancestors = new ArrayList<StagingTable>();
		if (!isScript())
			return ancestors;
		TransformationScript sc = new TransformationScript(r, null,true);
		try
		{
			sc.parseScript(Script, 0, null);
			for (Object source : sc.getSourceTables())
			{
				if (source instanceof StagingTable)
				{
					StagingTable ancestor = ((StagingTable) source);
					ancestors.add(ancestor);
					List<StagingTable> ancestorAncestors = ancestor.getAncestors();
					if (ancestorAncestors == null)
						return null;
					ancestors.addAll(ancestorAncestors);
				} else if (source instanceof MeasureTable)
				{
					// Can't tell if a measure table is unmodified
					return null;
				} else if (source instanceof DimensionTable)
				{
					// Can't tell if a dimension table is unmodified
					return null;
				}
			}
		} catch (Exception ex)
		{
			logger.info(ex.getMessage());
		}
		return ancestors;
	}

}
