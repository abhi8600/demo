/**
 * $Id: MelissaDataMatchInMemory.java,v 1.13 2010-10-23 01:11:56 ricks Exp $
 *
 * Copyright (C) 2007-2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse.transforms;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import DtApi.DtApi;

import com.successmetricsinc.security.EncryptionService;

/**
 * Java transform class for matching staging table data against existing master customer record data
 */
class MelissaDataMatchInMemory implements IJavaTransform
{
	private static Logger logger = Logger.getLogger(MelissaDataMatchInMemory.class);
	private static String[] arguments = 
	{
		"melissadata.library",			// Dt3Api (Dt3Api.dll needs to be in the PATH)
		"melissadata.registration",		// license information
		"melissadata.dtloc",			// location of the MatchUpAPI directory (normally c:/program files/matchupapi)
		"melissadata.cassloc",			// location of the MatchUPAPI CASS directory (normally c:/program files/matchupapi/cassmate)
										// CASS is used for address correction
		"melissadata.matchcode",		// name of the matchcode (from the MatchCode editor)
		
		"database.driverstring",		// driver string (normally com.microsoft.sqlserver.jdbc.SQLServerDriver)
		"database.connectionstring",	// connection string (normally jdbc:sqlserver://<host>:<port>;databaseName=<databasename>
		"database.username",			// username in cleartext
		"database.password",			// encrypted password, encrypted using the repository db password encryption
		
		"dimensiontable.name",			// name of the dimension table
		"dimensiontable.id",			// id column, value to be used in the staging table, INTEGER
		"dimensiontable.matchkey",		// match key column, varchar(128)
		"dimensiontable.clusterkey",	// cluster key column, varchar(128), non-unique index
		
		"stagingtable.name",			// name of the staging table
		"stagingtable.id",				// id column, INTEGER
		"stagingtable.dimensionid",		// id column for dimension id, INTEGER, non-unique index
		"stagingtable.matchkey",		// match key column, varchar(128)
		"stagingtable.clusterkey",		// cluster key column, varchar(128)
	};
	
	// melissa data variables
	private long hDeduper;						// opaque handle
    private int keySize; 						// size of the key used for matching
    private int blockSize;						// size of the cluster key used for storage and performance

    // database variables
	private Connection conn;					// database connection

	// prepared statements
	private PreparedStatement stagingTableQueryStmt;
	private PreparedStatement stagingTableUpdateStmt;
	private PreparedStatement dimensionTableQueryStmt;
	
	private Map<String, Map<String, Integer>> dimMap;
	private Map<String,List<Object>> stagingMap = new LinkedHashMap<String,List<Object>>(); // matchKey-1:stagingColumn,type (zip5, etc)
	
	public boolean init(Map<String,String> params)
	{		
		// process the arguments
		if (!validateParameters(params))
			return false;
		
		// process the melissa data arguments
		String library = params.get("melissadata.library");
		String registration = params.get("melissadata.registration");
		String dtloc = params.get("melissadata.dtloc");
		String cassloc = params.get("melissadata.cassloc");
		String matchcode = params.get("melissadata.matchcode");

		logger.info("melissadata.library = " + library);
		logger.info("melissadata.registration = " + registration);
		logger.info("melissadata.dtloc = " + dtloc);
		logger.info("melissadata.cassloc = " + cassloc);
		logger.info("melissadata.matchcode = " + matchcode);
		
		// process the database parameters
		String driverString = params.get("database.driverstring");
		String connectionString = params.get("database.connectionstring");
		String username = params.get("database.username");
		String password = params.get("database.password");
		
		// dimension table
		String dimensionTable = params.get("dimensiontable.name");
		String dimensionTableId = params.get("dimensiontable.id");
		String dimensionTableMatchKey = params.get("dimensiontable.matchkey");
		String dimensionTableClusterKey = params.get("dimensiontable.clusterkey");

		// query to build up in memory data structure
		String dimensionTableQuery = "SELECT " + dimensionTableId + ", " + dimensionTableMatchKey + ", " + dimensionTableClusterKey
										+ " FROM " + dimensionTable + " WHERE " + dimensionTableMatchKey + " IS NOT NULL";
		logger.debug(dimensionTableQuery);
		
		// staging table
		String stagingTable = params.get("stagingtable.name");
		String stagingTableId = params.get("stagingtable.id");
		String stagingTableDimensionId = params.get("stagingtable.dimensionid");
		String stagingTableMatchKey = params.get("stagingtable.matchkey");
		String stagingTableClusterKey = params.get("stagingtable.clusterkey");
		
		// process the fields used for matching
		for (int i = 0; i < params.size(); i++)
		{
			String key = "stagingTable.field-" + i;
			if (params.containsKey(key))
			{
				String value = params.get(key);
				if (value != null)
				{
					int index = value.lastIndexOf(',');
					if (index >= 0)
					{
						String column = value.substring(0, index);
						String typeName = value.substring(index+1);
						// convert from a string, such as MP_ZIP5, to a constant
						int type = 0;
						if (typeName.equals("MP_ZIP5"))
							type = DtApi.MP_ZIP5;
						else if (typeName.equals("MP_ZIP9"))
							type = DtApi.MP_ZIP9;
						else if (typeName.equals("MP_ADDR"))
							type = DtApi.MP_ADDR;
						else if (typeName.equals("MP_FIRST"))
							type = DtApi.MP_FIRST;	
						else if (typeName.equals("MP_LAST"))
							type = DtApi.MP_LAST;	
                        else if (typeName.equals("MP_GENERAL"))
                            type = DtApi.MP_GENERAL;               						
						else 
						{
							logger.warn("Invalid match code type for parameter " + key + ", " + value);
							continue;
						}
						List<Object> lst = new ArrayList<Object>();
						lst.add(column);
						lst.add(type);
						stagingMap.put(key, lst);
						logger.info("Matchkey: column=" + column + ", type=" + type + " (" + typeName + ")");
					}
					else
					{
						logger.warn("Invalid value for parameter (should be of the form: key,value) " + key + ", " + value);
					}
				}
				else
				{
					logger.warn("No value for parameter " + key);
				}
			}
		}
		
		// build a query for iterating over the staging table, returning the required fields
		StringBuilder stagingTableProjectionList = new StringBuilder();
		for (String key : stagingMap.keySet())
		{
			if (stagingTableProjectionList.length() > 0)
				stagingTableProjectionList.append(',');
			List<Object> lst = stagingMap.get(key);
			stagingTableProjectionList.append((String) lst.get(0));
		}
		String stagingTableQuery = "SELECT " + stagingTableId + ", " + stagingTableProjectionList.toString() + ", "
								+ stagingTableMatchKey + ", " + stagingTableClusterKey + ", " + stagingTableDimensionId 
								+ " FROM " + stagingTable;
		logger.debug(stagingTableQuery);
		
		// update the staging table entries with match key, cluster key, and dim key (if found)
		String stagingTableUpdate = "UPDATE " + stagingTable + " SET " 
										+ stagingTableDimensionId + " = ?, "
										+ stagingTableMatchKey + " = ?, "
										+ stagingTableClusterKey + " = ? "
										+ " WHERE " + stagingTableId + " = ?";
		logger.debug(stagingTableUpdate);

		// load the melissadata DLL
		try
		{
			System.loadLibrary(library);
		}
		catch (Exception ex)
		{
			logger.fatal(ex.getMessage(), ex);
			return false;
		}
		
		// turn off debugging messages
		DtApi.DTDebugMessages(false);
		
		// check the license
		DtApi.DTRegister(registration);
		/*
		if (!DtApi.DTRegister(registration))
		{
			logger.fatal("MelissaData registration failed, probably bad license - '" + registration + "'");
			return false;
		}
		*/

		// initialize the matching engine
		hDeduper = DtApi.DTInitHybrid(matchcode, dtloc, cassloc);

		if (hDeduper == 0)
		{
			switch (DtApi.DTLastError())
			{
			case DtApi.DTE_DTLOC:
				logger.fatal("DTLoc parameter (melissadata.dtloc) is incorrect or lookup table(s) missing - '" + dtloc + "'");
				return false;
			case DtApi.DTE_CASSLOC:
				logger.fatal("CassLoc parameter (melissadata.cassloc) is incorrect or CASS table(s) missing - '" + cassloc + "'");
				return false;
			case DtApi.DTE_MATCHCODE:
				logger.fatal("Matchcode parameter (melissadata.matchcode) is incorrect - '" + matchcode + "'");
				return false;
			case DtApi.DTE_NOCASS:
				logger.fatal("Matchcode uses CASS, but CASS is not activated");
				return false;
			default:
				logger.fatal("Unknown error");
				return false;
			}
		}

		// output a bunch of debugging information
		logger.debug("Match Code: " + DtApi.DTGetMatchcodeName(hDeduper));
		for (int i = 1; i <= DtApi.DTGetComponentCount(hDeduper); i++) { 
			logger.debug("Match Code " + i);
			logger.debug("Type: " + DtApi.DTGetComponentType(hDeduper, i));
			logger.debug("Size: " + DtApi.DTGetComponentSize(hDeduper, i));
			logger.debug("Label: " + DtApi.DTGetComponentLabel(hDeduper, i));
			logger.debug("Words: " + DtApi.DTGetComponentWords(hDeduper, i));
			logger.debug("Start: " + DtApi.DTGetComponentStart(hDeduper, i));
			logger.debug("Trim: " + DtApi.DTGetComponentTrim(hDeduper, i));
			logger.debug("Fuzzy: " + DtApi.DTGetComponentFuzzy(hDeduper, i));
			logger.debug("Short: " + DtApi.DTGetComponentShort(hDeduper, i));
			logger.debug("Swap: " + DtApi.DTGetComponentSwap(hDeduper, i));
			logger.debug("Combinations: " + Long.toBinaryString(DtApi.DTGetComponentCombinations(hDeduper, i))); 
		}
		
        keySize = DtApi.DTKeySize(hDeduper); 		// size of the key used for matching
        blockSize = DtApi.DTBlockSize(hDeduper);	// size of the cluster key used for storage and performance
        
        logger.debug("Keysize: " + keySize + ", blocksize: " + blockSize);
		
        // verify that the key fields are satisfied by the ruleset
		boolean bMappingOk = true;
		int count = 1;
		for (String key : stagingMap.keySet())
		{
			List<Object> lst = stagingMap.get(key);
			bMappingOk &= DtApi.DTMapComponentEx(hDeduper, count, (Integer) lst.get(1));
			if (!bMappingOk)
			{
				logger.debug("Incorrect Matchcode mapping for " + key + ", " + stagingMap.get(key) + " at position " + count);
				return false;
			}
			count++;
		}
		
		// dump out the map components
		logger.debug("Map Count: " + DtApi.DTGetMapCount(hDeduper));
		for (int i = 1; i <= DtApi.DTGetMapCount(hDeduper); i++)
		{
			logger.debug("Component Type: " + DtApi.DTGetMapComponentType(hDeduper, i));
			logger.debug("Component Label: " + DtApi.DTGetMapComponentLabel(hDeduper, i));
		}

		// load the JDBC driver
		try
		{
			Class.forName(driverString);
		}
		catch (ClassNotFoundException cnfex)
		{
			logger.fatal(cnfex.getMessage(), cnfex);
			return false;
		}

		try
		{
			// connect to the database
			conn = DriverManager.getConnection(connectionString, username, EncryptionService.getInstance().decrypt(password));

			// build the prepared statements
			stagingTableQueryStmt = conn.prepareStatement(stagingTableQuery);
			stagingTableUpdateStmt = conn.prepareStatement(stagingTableUpdate);
			dimensionTableQueryStmt = conn.prepareStatement(dimensionTableQuery);
		}
		catch (SQLException sex)
		{
			logger.fatal(sex.getMessage(), sex);
			return false;
		}
		return true;
	}
	
	public boolean execute()
	{
		int count = 0;		// number of items processed
		int matched = 0;	// number of items matched
		int existing = 0;	// prematched
		int noupdate = 0;	// have dimId and correct match and cluster keys
		
		try
		{
			// fill up the dimension hash table
			logger.info("Loading the dimension table keys into memory");
			dimMap = new HashMap<String, Map<String,Integer>>(8000000);
			ResultSet rs2 = dimensionTableQueryStmt.executeQuery();
			int cnt = 0;
			while (rs2.next())
			{
				int id = rs2.getInt(1);
				String matchKey = rs2.getString(2);
				String clusterKey = rs2.getString(3);
				Map<String,Integer> map = dimMap.get(clusterKey);
				if (map == null)
				{
					map = new HashMap<String,Integer>(1);
					dimMap.put(clusterKey, map);
				}
				map.put(matchKey, id);
				cnt++;
				if ((cnt % 100000) == 0)
					logger.debug("processed " + cnt + " records");
			}
			logger.info("Dimension table size: " + dimMap.size());
			// iterate over the staging table
			logger.info("Processing the staging table");
			ResultSet rs = stagingTableQueryStmt.executeQuery();
			while (rs.next())
			{
				StringBuilder fields = new StringBuilder();
				boolean first = true;
				
				int id = rs.getInt(1);
				int index = 2;
				// build the match and cluster keys
				for (String key : stagingMap.keySet())
				{
					if (first)
						first = false;
					else
						fields.append('\t');
					String value = rs.getString(index);
					if (value != null)
					{
						value = value.trim();
						fields.append(value);
					}
					index++;
				}
				
				String matchKeyFromStagingTable = rs.getString(index++);
				if (matchKeyFromStagingTable != null)
					matchKeyFromStagingTable = matchKeyFromStagingTable.trim();
				String clusterKeyFromStagingTable = rs.getString(index++);
				if (clusterKeyFromStagingTable != null)
					clusterKeyFromStagingTable = clusterKeyFromStagingTable.trim();

				String key = DtApi.DTBuildKey(hDeduper, fields.toString());
				String clusterKey = key.substring(0, blockSize);
				
				Integer dimId = rs.getInt(index++);
				if (logger.isTraceEnabled())
					logger.trace("Staging Id: " + id + ", dimId: " + dimId + ", key: '" + key + "', clusterKey: " + clusterKey + ", fields: " + fields.toString());
				count++;

				// if dimId is null that means there was no 'prematch', so do a full match
				if (rs.wasNull())
				{
					boolean foundMatch = false;
					// fetch the master keys that match the cluster key and do a more detailed comparison using the matching key
					Map<String,Integer> map = dimMap.get(clusterKey);
					if (map != null)
					{
						for (Entry<String,Integer> entry : map.entrySet())
						{
							String dimMatchKeyValue = entry.getKey();
							long result = DtApi.DTCompare(hDeduper, key, dimMatchKeyValue, 0);
							if (result != 0)
							{
								int dimensionMatchId = entry.getValue();
								if (logger.isTraceEnabled())
									logger.trace("matches - dimensionMatchId: " + dimensionMatchId + ", result: " + Long.toHexString(result));
								stagingTableUpdateStmt.setInt(1, dimensionMatchId);
								matched++;
								foundMatch = true;
								break;
							}
						}
					}
					if (!foundMatch)
					{
						stagingTableUpdateStmt.setNull(1, Types.INTEGER);
						if (logger.isTraceEnabled())
							logger.trace("no match");
					}
				}
				else
				{
					// see if the item has already been fully pre-matched
					if (matchKeyFromStagingTable != null && matchKeyFromStagingTable.equals(key)
							&& clusterKeyFromStagingTable != null && clusterKeyFromStagingTable.equals(clusterKey))
					{
						noupdate++;
						continue;
					}
					stagingTableUpdateStmt.setInt(1, dimId);	// doing work we don't need to do, but easier than having two prepared statements and addBatch
					existing++;
				}
				stagingTableUpdateStmt.setString(2, key);
				stagingTableUpdateStmt.setString(3, clusterKey);
				stagingTableUpdateStmt.setInt(4, id);
				stagingTableUpdateStmt.addBatch();
				if ((count % 5000) == 0)
				{
					long tm = System.currentTimeMillis();
					int[] results = stagingTableUpdateStmt.executeBatch();
					logger.debug("Updated " + results.length + " records in " + (System.currentTimeMillis() - tm) + "ms");
					for (int i = 0; i < results.length; i++)
					{
						if (results[i] != 1)
						{
							logger.warn("Record " + i + " was not properly updated (code: " + results[i] + ")");
						}
					}
				}
			}
			rs.close();
			// make sure that we have finished the entire batch
			if ((count % 5000) != 0)
			{
				long tm = System.currentTimeMillis();
				int[] results = stagingTableUpdateStmt.executeBatch();
				logger.debug("Updated " + results.length + " records in " + (System.currentTimeMillis() - tm) + "ms");
				for (int i = 0; i < results.length; i++)
				{
					if (results[i] != 1)
					{
						logger.warn("Record " + i + " was not properly updated (code: " + results[i] + ")");
					}
				}
			}
		}
		catch (SQLException sex)
		{
			logger.fatal(sex.getMessage(), sex);
			return false;
		}
		
		// close down melissadata
		DtApi.DTClose(hDeduper);
		logger.info("Processed " + count + " items, " + existing + " items already in the dimension table via dimension key, " + noupdate + " not updated due to keys being the same, " + matched + " matched items via melissa, " + (count - existing - matched - noupdate) + " new items");
		return true;
	}
	
	/**
	 * determine if a string is empty
	 * @param str
	 * @return
	 */
	private boolean emptyString(String str)
	{
		return (str == null || str.trim().length() == 0);
	}
	
	/**
	 * validate a set of name/value pairs
	 * @param params
	 * @return
	 */
	private boolean validateParameters(Map<String,String> params)
	{
		boolean okay = true;
		
		if (params == null)
		{
			logger.fatal("No parameters for " + this.getClass().getSimpleName());
			return false;
		}
		
		for (String key : arguments)
		{
			if (params.containsKey(key))
			{
				if (emptyString(params.get(key)))
				{
					logger.fatal("Missing value for " + key);
					okay = false;
				}
			}
			else
			{
				logger.fatal("Missing key " + key);
				okay = false;
			}
		}
		return okay;
	}
}

