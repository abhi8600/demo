/**
 * $Id: MelissaDataMatch.java,v 1.13 2010-10-23 01:11:56 ricks Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse.transforms;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Map;

import org.apache.log4j.Logger;

import com.successmetricsinc.security.EncryptionService;

import DtApi.DtApi;

/**
 * Java transform class for matching staging table data against existing master customer record data
 */
class MelissaDataMatch implements IJavaTransform
{
	private static Logger logger = Logger.getLogger(MelissaDataMatch.class);
	private static String[] arguments = 
	{
		"melissadata.library",			// Dt3Api (Dt3Api.dll needs to be in the PATH)
		"melissadata.registration",		// license information
		"melissadata.dtloc",			// location of the MatchUpAPI directory (normally c:/program files/matchupapi)
		"melissadata.cassloc",			// location of the MatchUPAPI CASS directory (normally c:/program files/matchupapi/cassmate)
										// CASS is used for address correction
		"melissadata.matchcode",		// name of the matchcode (from the MatchCode editor)
		
		"database.driverstring",		// driver string (normally com.microsoft.sqlserver.jdbc.SQLServerDriver)
		"database.connectionstring",	// connection string (normally jdbc:sqlserver://<host>:<port>;databaseName=<databasename>
		"database.username",			// username in cleartext
		"database.password",			// encrypted password, encrypted using the repository db password encryption
		
		"dimensiontable.name",			// name of the dimension table
		"dimensiontable.id",			// id column, value to be used in the staging table, INTEGER
		"dimensiontable.matchkey",		// match key column, varchar(128)
		"dimensiontable.clusterkey",	// cluster key column, varchar(128), non-unique index
		
		"stagingtable.name",			// name of the staging table
		"stagingtable.id",				// id column, INTEGER
		"stagingtable.dimensionid",		// id column for dimension id, INTEGER, non-unique index
		"stagingtable.matchkey",		// match key column, varchar(128)
		"stagingtable.clusterkey",		// cluster key column, varchar(128)
		"stagingtable.zip5",			// zip5 column
		"stagingtable.addr1",			// addr1 column
		"stagingtable.firstname",		// firstname column
		"stagingtable.lastname"			// lastname column
	};
	
	// melissa data variables
	private long hDeduper;						// opaque handle
    private int keySize; 						// size of the key used for matching
    private int blockSize;						// size of the cluster key used for storage and performance

    // database variables
	private Connection conn;					// database connection

	// prepared statements
	private PreparedStatement stagingTableQueryStmt;
	private PreparedStatement stagingTableUpdateStmt;
	private PreparedStatement dimensionTableQueryStmt;

	public boolean init(Map<String,String> params)
	{		
		// process the arguments
		if (!validateParameters(params))
			return false;
		
		// process the melissa data arguments
		String library = params.get("melissadata.library");
		String registration = params.get("melissadata.registration");
		String dtloc = params.get("melissadata.dtloc");
		String cassloc = params.get("melissadata.cassloc");
		String matchcode = params.get("melissadata.matchcode");

		logger.info("melissadata.library = " + library);
		logger.info("melissadata.registration = " + registration);
		logger.info("melissadata.dtloc = " + dtloc);
		logger.info("melissadata.cassloc = " + cassloc);
		logger.info("melissadata.matchcode = " + matchcode);
		
		// process the database parameters
		String driverString = params.get("database.driverstring");
		String connectionString = params.get("database.connectionstring");
		String username = params.get("database.username");
		String password = params.get("database.password");
		
		// dimension table
		String dimensionTable = params.get("dimensiontable.name");
		String dimensionTableId = params.get("dimensiontable.id");
		String dimensionTableMatchKey = params.get("dimensiontable.matchkey");
		String dimensionTableClusterKey = params.get("dimensiontable.clusterkey");
		
		// for overriding the automatically generated query
		String dimensionTableQuery = "SELECT " + dimensionTableId + ", " + dimensionTableMatchKey + " FROM " + dimensionTable
								+ " WHERE " + dimensionTableClusterKey + " = ?";
		logger.debug(dimensionTableQuery);
		
		// staging table
		String stagingTable = params.get("stagingtable.name");
		String stagingTableId = params.get("stagingtable.id");
		String stagingTableDimensionId = params.get("stagingtable.dimensionid");
		String stagingTableMatchKey = params.get("stagingtable.matchkey");
		String stagingTableClusterKey = params.get("stagingtable.clusterkey");
		String stagingTableZip5 = params.get("stagingtable.zip5");
		String stagingTableAddr1 = params.get("stagingtable.addr1");
		String stagingTableFirstName = params.get("stagingtable.firstname");
		String stagingTableLastName = params.get("stagingtable.lastname");
		
		String stagingTableQuery = "SELECT " + stagingTableId + ", " + stagingTableZip5 + ", " + stagingTableAddr1 + ", "
		+ stagingTableFirstName + ", " + stagingTableLastName + ", " 
		+ stagingTableMatchKey + ", " + stagingTableClusterKey + ", " + stagingTableDimensionId 
		+ " FROM " + stagingTable;

		logger.debug(stagingTableQuery);
		String stagingTableUpdate = "UPDATE " + stagingTable + " SET " 
										+ stagingTableDimensionId + " = ?, "
										+ stagingTableMatchKey + " = ?, "
										+ stagingTableClusterKey + " = ? "
										+ " WHERE " + stagingTableId + " = ?";
		logger.debug(stagingTableUpdate);

		// load the melissadata DLL
		try
		{
			System.loadLibrary(library);
		}
		catch (Exception ex)
		{
			logger.fatal(ex.getMessage(), ex);
			return false;
		}
		
		// turn off debugging messages
		DtApi.DTDebugMessages(false);
		
		// check the license
		DtApi.DTRegister(registration);
		/*
		if (!DtApi.DTRegister(registration))
		{
			logger.fatal("MelissaData registration failed, probably bad license - '" + registration + "'");
			return false;
		}
		*/

		// initialize the matching engine
		hDeduper = DtApi.DTInitHybrid(matchcode, dtloc, cassloc);

		if (hDeduper == 0)
		{
			switch (DtApi.DTLastError())
			{
			case DtApi.DTE_DTLOC:
				logger.fatal("DTLoc parameter (melissadata.dtloc) is incorrect or lookup table(s) missing - '" + dtloc + "'");
				return false;
			case DtApi.DTE_CASSLOC:
				logger.fatal("CassLoc parameter (melissadata.cassloc) is incorrect or CASS table(s) missing - '" + cassloc + "'");
				return false;
			case DtApi.DTE_MATCHCODE:
				logger.fatal("Matchcode parameter (melissadata.matchcode) is incorrect - '" + matchcode + "'");
				return false;
			case DtApi.DTE_NOCASS:
				logger.fatal("Matchcode uses CASS, but CASS is not activated");
				return false;
			default:
				logger.fatal("Unknown error");
				return false;
			}
		}

		// output a bunch of debugging information
		logger.debug("Match Code: " + DtApi.DTGetMatchcodeName(hDeduper));
		for (int i = 1; i <= DtApi.DTGetComponentCount(hDeduper); i++) { 
			logger.debug("Match Code " + i);
			logger.debug("Type: " + DtApi.DTGetComponentType(hDeduper, i));
			logger.debug("Size: " + DtApi.DTGetComponentSize(hDeduper, i));
			logger.debug("Label: " + DtApi.DTGetComponentLabel(hDeduper, i));
			logger.debug("Words: " + DtApi.DTGetComponentWords(hDeduper, i));
			logger.debug("Start: " + DtApi.DTGetComponentStart(hDeduper, i));
			logger.debug("Trim: " + DtApi.DTGetComponentTrim(hDeduper, i));
			logger.debug("Fuzzy: " + DtApi.DTGetComponentFuzzy(hDeduper, i));
			logger.debug("Short: " + DtApi.DTGetComponentShort(hDeduper, i));
			logger.debug("Swap: " + DtApi.DTGetComponentSwap(hDeduper, i));
			logger.debug("Combinations: " + Long.toBinaryString(DtApi.DTGetComponentCombinations(hDeduper, i))); 
		}
		
        keySize = DtApi.DTKeySize(hDeduper); 		// size of the key used for matching
        blockSize = DtApi.DTBlockSize(hDeduper);	// size of the cluster key used for storage and performance
        
        logger.debug("Keysize: " + keySize + ", blocksize: " + blockSize);
		
        // verify that the key fields are satisfied by the ruleset
		boolean bMappingOk = DtApi.DTMapComponentEx(hDeduper, 1, DtApi.MP_ZIP5);
		bMappingOk &= DtApi.DTMapComponentEx(hDeduper, 2, DtApi.MP_LAST);
		bMappingOk &= DtApi.DTMapComponentEx(hDeduper, 3, DtApi.MP_FIRST);
		bMappingOk &= DtApi.DTMapComponentEx(hDeduper, 4, DtApi.MP_ADDR);
		if (!bMappingOk)
		{
			logger.debug("Incorrect Matchcode mapping");
			return false;
		}
		
		// dump out the map components
		logger.debug("Map Count: " + DtApi.DTGetMapCount(hDeduper));
		for (int i = 1; i <= DtApi.DTGetMapCount(hDeduper); i++)
		{
			logger.debug("Component Type: " + DtApi.DTGetMapComponentType(hDeduper, i));
			logger.debug("Component Label: " + DtApi.DTGetMapComponentLabel(hDeduper, i));
		}

		// load the JDBC driver
		try
		{
			Class.forName(driverString);
		}
		catch (ClassNotFoundException cnfex)
		{
			logger.fatal(cnfex.getMessage(), cnfex);
			return false;
		}

		try
		{
			// connect to the database
			conn = DriverManager.getConnection(connectionString, username, EncryptionService.getInstance().decrypt(password));

			// build the prepared statements
			stagingTableQueryStmt = conn.prepareStatement(stagingTableQuery);
			stagingTableUpdateStmt = conn.prepareStatement(stagingTableUpdate);
			dimensionTableQueryStmt = conn.prepareStatement(dimensionTableQuery);
		}
		catch (SQLException sex)
		{
			logger.fatal(sex.getMessage(), sex);
			return false;
		}
		return true;
	}
	
	public boolean execute()
	{
		int count = 0;		// number of items processed
		int matched = 0;	// number of items matched
		int existing = 0;	// prematched
		int noupdate = 0;	// fully prematched
		
		try
		{
			// iterate over the staging table
			ResultSet rs = stagingTableQueryStmt.executeQuery();
			while (rs.next())
			{
				int id = rs.getInt(1);

				// build the match and cluster keys
				String zip5 = rs.getString(2);
				if (zip5 != null)
					zip5 = zip5.trim();
				String addr1 = rs.getString(3);
				if (addr1 != null)
					addr1 = addr1.trim();
				String firstname = rs.getString(4);
				if (firstname != null)
					firstname = firstname.trim();
				String lastname = rs.getString(5);
				if (lastname != null)
					lastname = lastname.trim();
				String matchKeyFromStagingTable = rs.getString(6);
				if (matchKeyFromStagingTable != null)
					matchKeyFromStagingTable = matchKeyFromStagingTable.trim();
				String clusterKeyFromStagingTable = rs.getString(7);
				if (clusterKeyFromStagingTable != null)
					clusterKeyFromStagingTable = clusterKeyFromStagingTable.trim();

				String fields = zip5 + '\t' + lastname + '\t' + firstname + '\t' + addr1;
				String key = DtApi.DTBuildKey(hDeduper, fields);
				String clusterKey = key.substring(0, blockSize);
				
				Integer dimId = rs.getInt(8);
				if (logger.isTraceEnabled())
					logger.trace("Staging Id: " + id + ", dimId: " + dimId + ", key: '" + key + "', clusterKey: " + clusterKey + ", fields: " + fields);
				count++;

				// if dimId is null that means there was no 'prematch', so do a full match
				if (rs.wasNull())
				{
					// fetch the master keys that match the cluster key and do a more detailed comparison using the matching key
					dimensionTableQueryStmt.setString(1, clusterKey);
					long tmstart = System.currentTimeMillis();
					ResultSet rs2 = dimensionTableQueryStmt.executeQuery();
					if (logger.isTraceEnabled())
						logger.trace("dm query time: " + (System.currentTimeMillis() - tmstart));
					boolean foundMatch = false;
					int cnt = 0;
					while (rs2.next())
					{
						cnt++;
						String dimMatchKeyValue = rs2.getString(2);
						long result = DtApi.DTCompare(hDeduper, key, dimMatchKeyValue, 0);
						if (result != 0)
						{
							int dimensionMatchId = rs2.getInt(1);
							if (logger.isTraceEnabled())
								logger.trace("matches - dimensionMatchId: " + dimensionMatchId + ", cnt: " + cnt + ", result: " + Long.toHexString(result));
							stagingTableUpdateStmt.setInt(1, dimensionMatchId);
							matched++;
							foundMatch = true;
							break;
						}
					}
					rs2.close();
					if (!foundMatch)
					{
						stagingTableUpdateStmt.setNull(1, Types.INTEGER);
						if (logger.isTraceEnabled())
							logger.trace("no match - cnt: " + cnt);
					}
					if (logger.isTraceEnabled())
						logger.trace("full match time: " + (System.currentTimeMillis() - tmstart));
				}
				else
				{
					// see if the item has already been fully pre-matched
					if (matchKeyFromStagingTable != null && matchKeyFromStagingTable.equals(key)
							&& clusterKeyFromStagingTable != null && clusterKeyFromStagingTable.equals(clusterKey))
					{
						noupdate++;
						continue;
					}
					stagingTableUpdateStmt.setInt(1, dimId);	// doing work we don't need to do, but easier than having two prepared statements and addBatch
					existing++;
				}
				stagingTableUpdateStmt.setString(2, key);
				stagingTableUpdateStmt.setString(3, clusterKey);
				stagingTableUpdateStmt.setInt(4, id);
				stagingTableUpdateStmt.addBatch();
				if ((count % 1000) == 0)
				{
					long tm = System.currentTimeMillis();
					int[] results = stagingTableUpdateStmt.executeBatch();
					logger.debug("Updated " + results.length + " records in " + (System.currentTimeMillis() - tm) + "ms");
					for (int i = 0; i < results.length; i++)
					{
						if (results[i] != 1)
						{
							logger.warn("Record " + i + " was not properly updated (code: " + results[i] + ")");
						}
					}
				}
			}
			rs.close();
			// make sure that we have finished the entire batch
			if ((count % 1000) != 0)
			{
				long tm = System.currentTimeMillis();
				int[] results = stagingTableUpdateStmt.executeBatch();
				logger.debug("Updated " + results.length + " records in " + (System.currentTimeMillis() - tm) + "ms");
				for (int i = 0; i < results.length; i++)
				{
					if (results[i] != 1)
					{
						logger.warn("Record " + i + " was not properly updated (code: " + results[i] + ")");
					}
				}
			}
		}
		catch (SQLException sex)
		{
			logger.fatal(sex.getMessage(), sex);
			return false;
		}
		
		// close down melissadata
		DtApi.DTClose(hDeduper);
		logger.info("Processed " + count + " items, " + existing + " items already in the dimension table via dimension key, " + noupdate + " not updated due to keys being the same, " + matched + " matched items via melissa, " + (count - existing - matched - noupdate) + " new items");
		return true;
	}
	
	/**
	 * determine if a string is empty
	 * @param str
	 * @return
	 */
	private boolean emptyString(String str)
	{
		return (str == null || str.trim().length() == 0);
	}
	
	/**
	 * validate a set of name/value pairs
	 * @param params
	 * @return
	 */
	private boolean validateParameters(Map<String,String> params)
	{
		boolean okay = true;
		
		if (params == null)
		{
			logger.fatal("No parameters for " + this.getClass().getSimpleName());
			return false;
		}
		
		for (String key : arguments)
		{
			if (params.containsKey(key))
			{
				if (emptyString(params.get(key)))
				{
					logger.fatal("Missing value for " + key);
					okay = false;
				}
			}
			else
			{
				logger.fatal("Missing key " + key);
				okay = false;
			}
		}
		return okay;
	}
}

