/**
 * $Id: JavaTransform.java,v 1.6 2011-09-24 01:08:10 ricks Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse.transforms;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.JasperDataSourceProvider;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.warehouse.Transformation;

public class JavaTransform extends Transformation implements Serializable
{
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(JavaTransform.class);
	private String name;
	private String className;
	private Map<String,String> parameters;

	@SuppressWarnings("rawtypes")
	public JavaTransform(Element e, Namespace ns)
	{
		name = e.getChildTextTrim("Name", ns);
		className = e.getChildTextTrim("ClassName", ns);
		Element p1 = e.getChild("Parameters", ns);
		List pairs = p1.getChildren("NameValuePair", ns);
		parameters = new LinkedHashMap<String,String>(pairs.size());
		Iterator iter = pairs.iterator();
		while (iter.hasNext())
		{
			Element item = (Element) iter.next();
			String name = item.getChildTextTrim("Name", ns);
			String value = item.getChildTextTrim("Value", ns);
			parameters.put(name, value);
		}
	}
	
	public Element getJavaTransformElement(Namespace ns)
	{
		Element e = new Element("JavaTransform",ns);
		
		Element child = new Element("ExecuteAfter",ns);
		child.setText(String.valueOf(iExecuteAfter));
		e.addContent(child);
		
		child = new Element("Name",ns);
		child.setText(name);
		e.addContent(child);
		
		child = new Element("ClassName",ns);
		child.setText(className);
		e.addContent(child);
		
		child = new Element("Parameters",ns);
		if (parameters!=null && parameters.size()>0)
		{
			for (String key : parameters.keySet())
			{
				Element nameValPair = new Element("NameValuePair",ns);
				Element nameElement = new Element("Name",ns);
				nameElement.setText(key);
				nameValPair.addContent(nameElement);
				Element valueElement = new Element("Value",ns);
				valueElement.setText(parameters.get(key));
				nameValPair.addContent(valueElement);
				child.addContent(nameValPair);
			}
		}
		e.addContent(child);
		
		return e;
	}

	/**
	 * Execute the java transform
	 * 
	 * @param r
	 * @param dc
	 * @param s
	 * @param jdsp
	 * @return
	 */
	public void execute(Repository r, DatabaseConnection dc, Session s, JasperDataSourceProvider jdsp) throws BaseException, SQLException, IOException, CloneNotSupportedException
	{
		try
		{
			// lookup class (load by name), defined interface
			Class clz = Class.forName(className);
			IJavaTransform xformClz = (IJavaTransform) clz.newInstance();

			// variable substitution of the parameter values
			for (String key : parameters.keySet())
			{
				parameters.put(key, r.replaceVariables(s, parameters.get(key)));
			}

			// grab the DB information
			String driverString = dc.Driver;
			String connectionString = dc.ConnectString;
			String username = dc.UserName;
			String password = dc.Password;
			
			parameters.put("database.driverstring", driverString);
			parameters.put("database.connectionstring",	connectionString);
			parameters.put("database.username",	username);
			parameters.put("database.password",	password);
			
			logger.info("Initializing Java Transform: " + name + ", " + className);
			if (!xformClz.init(parameters))
				return;
			logger.info("Executing Java Tranform: " + name + ", " + className);
			xformClz.execute();
		}
		catch (Exception ex)
		{
			logger.error(ex.getMessage(), ex);
		}
	}
}
