/**
 * $Id: GenerateMatchKeys.java,v 1.3 2010-05-27 18:14:41 ricks Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse.transforms;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.WriterAppender;

import DtApi.DtApi;

import com.successmetricsinc.security.EncryptionService;

/**
 * Java transform class for matching staging table data against existing master customer record data
 */
class GenerateMatchKeys
{
	private static Logger logger = Logger.getLogger(GenerateMatchKeys.class);
	private static WriterAppender defaultLogfileAppender = null;
	private static Level loglevel = Level.TRACE;

	private static String[] arguments = 
	{
		"melissadata.library",			// Dt3Api (Dt3Api.dll needs to be in the PATH)
		"melissadata.registration",		// license information
		"melissadata.dtloc",			// location of the MatchUpAPI directory (normally c:/program files/matchupapi)
		"melissadata.cassloc",			// location of the MatchUPAPI CASS directory (normally c:/program files/matchupapi/cassmate)
										// CASS is used for address correction
		"melissadata.matchcode",		// name of the matchcode (from the MatchCode editor)
		
		"database.driverstring",		// driver string (normally com.microsoft.sqlserver.jdbc.SQLServerDriver)
		"database.connectionstring",	// connection string (normally jdbc:sqlserver://<host>:<port>;databaseName=<databasename>
		"database.username",			// username in cleartext
		"database.password",			// encrypted password, encrypted using the repository db password encryption
		
		"dimensiontable.name",			// name of the dimension table
		"dimensiontable.id",			// id column, INTEGER, must be unique
		"dimensiontable.matchkey",		// match key column, varchar(128)
		"dimensiontable.clusterkey",	// cluster key column, varchar(128), non-unique index
		"dimensiontable.zip5",			// zip5 column
		"dimensiontable.addr1",			// addr1 column
		"dimensiontable.firstname",		// firstname column
		"dimensiontable.lastname"		// lastname column
	};
	
	// melissa data variables
	private long hDeduper;						// opaque handle
    private int keySize; 						// size of the key used for matching
    private int blockSize;						// size of the cluster key used for storage and performance

    // database variables
	private Connection conn;					// database connection

	// prepared statements
	private PreparedStatement dimensionTableQueryStmt;
	private PreparedStatement dimensionTableUpdateStmt;
	
	public static void main(String[] args)
	{
		PatternLayout ly = new PatternLayout("%n%d{yyyy-MM-dd HH:mm:ss,SSSZ} [%t] %-5p - %m");
		defaultLogfileAppender = new ConsoleAppender(ly);
		Logger.getRootLogger().addAppender(defaultLogfileAppender);
		Logger.getRootLogger().setLevel(loglevel);
		logger.setLevel(loglevel);

		Properties result = new Properties();
		try
		{
			result.load(new FileInputStream(args[0]));
		} catch (Exception ex)
		{
			logger.fatal("Could not load properties file - " + args[0]);
		}
		GenerateMatchKeys gmk = new GenerateMatchKeys();
		if (gmk.init(result))
			gmk.execute();
	}
	
	public boolean init(Properties props)
	{		
		// process the arguments
		if (!validateParameters(props))
			return false;
		
		// process the melissa data arguments
		String library = props.getProperty("melissadata.library").trim();
		String registration = props.getProperty("melissadata.registration").trim();
		String dtloc = props.getProperty("melissadata.dtloc").trim();
		String cassloc = props.getProperty("melissadata.cassloc").trim();
		String matchcode = props.getProperty("melissadata.matchcode").trim();

		logger.info("melissadata.library = " + library);
		logger.info("melissadata.registration = " + registration);
		logger.info("melissadata.dtloc = " + dtloc);
		logger.info("melissadata.cassloc = " + cassloc);
		logger.info("melissadata.matchcode = " + matchcode);
		
		// process the database parameters
		String driverString = props.getProperty("database.driverstring").trim();
		String connectionString = props.getProperty("database.connectionstring").trim();
		String username = props.getProperty("database.username").trim();
		String password = props.getProperty("database.password").trim();
		
		// dimension table
		String dimensionTable = props.getProperty("dimensiontable.name").trim();
		String dimensionTableId = props.getProperty("dimensiontable.id").trim();
		String dimensionTableMatchKey = props.getProperty("dimensiontable.matchkey").trim();
		String dimensionTableClusterKey = props.getProperty("dimensiontable.clusterkey").trim();
		String dimensionTableZip5 = props.getProperty("dimensiontable.zip5").trim();
		String dimensionTableAddr1 = props.getProperty("dimensiontable.addr1").trim();
		String dimensionTableFirstName = props.getProperty("dimensiontable.firstname").trim();
		String dimensionTableLastName = props.getProperty("dimensiontable.lastname").trim();
		
		String dimensionTableQuery = "SELECT " + dimensionTableId + ", " + dimensionTableZip5 + ", " + dimensionTableAddr1 + ", "
								+ dimensionTableFirstName + ", " + dimensionTableLastName + " FROM " + dimensionTable;
		logger.debug(dimensionTableQuery);
		String dimensionTableUpdate = "UPDATE " + dimensionTable
										+ " SET "
										+ dimensionTableMatchKey + " = ?, "
										+ dimensionTableClusterKey + " = ? "
										+ " WHERE " + dimensionTableId + " = ?";
		logger.debug(dimensionTableUpdate);

		// load the melissadata DLL
		try
		{
			System.loadLibrary(library);
		}
		catch (Exception ex)
		{
			logger.fatal(ex.getMessage(), ex);
			return false;
		}
		
		// turn on debugging messages
		DtApi.DTDebugMessages(true);
		
		// check the license
		DtApi.DTRegister(registration);
		/*
		if (!DtApi.DTRegister(registration))
		{
			logger.fatal("MelissaData registration failed, probably bad license - '" + registration + "'");
			return false;
		}
		*/

		// initialize the matching engine
		hDeduper = DtApi.DTInitHybrid(matchcode, dtloc, cassloc);

		if (hDeduper == 0)
		{
			switch (DtApi.DTLastError())
			{
			case DtApi.DTE_DTLOC:
				logger.fatal("DTLoc parameter (melissadata.dtloc) is incorrect or lookup table(s) missing - '" + dtloc + "'");
				return false;
			case DtApi.DTE_CASSLOC:
				logger.fatal("CassLoc parameter (melissadata.cassloc) is incorrect or CASS table(s) missing - '" + cassloc + "'");
				return false;
			case DtApi.DTE_MATCHCODE:
				logger.fatal("Matchcode parameter (melissadata.matchcode) is incorrect - '" + matchcode + "'");
				return false;
			case DtApi.DTE_NOCASS:
				logger.fatal("Matchcode uses CASS, but CASS is not activated");
				return false;
			default:
				logger.fatal("Unknown error");
				return false;
			}
		}

		// output a bunch of debugging information
		logger.debug("Match Code: " + DtApi.DTGetMatchcodeName(hDeduper));
		for (int i = 1; i <= DtApi.DTGetComponentCount(hDeduper); i++) { 
			logger.debug("Match Code " + i);
			logger.debug("Type: " + DtApi.DTGetComponentType(hDeduper, i));
			logger.debug("Size: " + DtApi.DTGetComponentSize(hDeduper, i));
			logger.debug("Label: " + DtApi.DTGetComponentLabel(hDeduper, i));
			logger.debug("Words: " + DtApi.DTGetComponentWords(hDeduper, i));
			logger.debug("Start: " + DtApi.DTGetComponentStart(hDeduper, i));
			logger.debug("Trim: " + DtApi.DTGetComponentTrim(hDeduper, i));
			logger.debug("Fuzzy: " + DtApi.DTGetComponentFuzzy(hDeduper, i));
			logger.debug("Short: " + DtApi.DTGetComponentShort(hDeduper, i));
			logger.debug("Swap: " + DtApi.DTGetComponentSwap(hDeduper, i));
			logger.debug("Combinations: " + Long.toBinaryString(DtApi.DTGetComponentCombinations(hDeduper, i))); 
		}
		
        keySize = DtApi.DTKeySize(hDeduper); 		// size of the key used for matching
        blockSize = DtApi.DTBlockSize(hDeduper);	// size of the cluster key used for storage and performance
        
        logger.debug("Keysize: " + keySize + ", blocksize: " + blockSize);
		
        // verify that the key fields are satisfied by the ruleset
		boolean bMappingOk = DtApi.DTMapComponentEx(hDeduper, 1, DtApi.MP_ZIP5);
		bMappingOk &= DtApi.DTMapComponentEx(hDeduper, 2, DtApi.MP_LAST);
		bMappingOk &= DtApi.DTMapComponentEx(hDeduper, 3, DtApi.MP_FIRST);
		bMappingOk &= DtApi.DTMapComponentEx(hDeduper, 4, DtApi.MP_ADDR);
		if (!bMappingOk)
		{
			logger.debug("Incorrect Matchcode mapping");
			return false;
		}
		
		// dump out the map components
		logger.debug("Map Count: " + DtApi.DTGetMapCount(hDeduper));
		for (int i = 1; i <= DtApi.DTGetMapCount(hDeduper); i++)
		{
			logger.debug("Component Type: " + DtApi.DTGetMapComponentType(hDeduper, i));
			logger.debug("Component Label: " + DtApi.DTGetMapComponentLabel(hDeduper, i));
		}

		// load the JDBC driver
		try
		{
			Class.forName(driverString);
		}
		catch (ClassNotFoundException cnfex)
		{
			logger.fatal(cnfex.getMessage(), cnfex);
			return false;
		}

		try
		{
			// connect to the database
			conn = DriverManager.getConnection(connectionString, username, EncryptionService.getInstance().decrypt(password));

			// build the prepared statements
			dimensionTableUpdateStmt = conn.prepareStatement(dimensionTableUpdate);
			dimensionTableQueryStmt = conn.prepareStatement(dimensionTableQuery);
		}
		catch (SQLException sex)
		{
			logger.fatal(sex.getMessage(), sex);
			return false;
		}
		return true;
	}
	
	public boolean execute()
	{
		int count = 0;		// number of items processed

		try
		{
			// iterate over the staging table
			ResultSet rs = dimensionTableQueryStmt.executeQuery();
			while (rs.next())
			{
				int id = rs.getInt(1);

				// build the match and cluster keys
				String zip5 = rs.getString(2);
				String addr1 = rs.getString(3);
				String firstname = rs.getString(4);
				String lastname = rs.getString(5);
				String fields = zip5 + "\t" + lastname + "\t" + firstname + "\t" + addr1;
				
				String matchKey = DtApi.DTBuildKey(hDeduper, fields);
				String clusterKey = matchKey.substring(0, blockSize);
				logger.debug("Id: " + id + ", key: '" + matchKey + "', clusterKey: " + clusterKey + ", fields: " + fields);
				
				dimensionTableUpdateStmt.setString(1, matchKey);
				dimensionTableUpdateStmt.setString(2, clusterKey);
				dimensionTableUpdateStmt.setInt(3, id);
				dimensionTableUpdateStmt.addBatch();
				count++;
				if ((count % 1000) == 0)
				{
					int[] results = dimensionTableUpdateStmt.executeBatch();
					for (int i = 0; i < results.length; i++)
					{
						if (results[i] != 0)
						{
							logger.warn("Record " + i + " was not properly updated");
						}
					}
				}
			}
			// make sure that we have finished the entire batch
			if ((count % 1000) != 0)
			{
				dimensionTableUpdateStmt.executeBatch();
			}
		}
		catch (SQLException sex)
		{
			logger.fatal(sex.getMessage(), sex);
			return false;
		}
		
		// close down melissadata
		DtApi.DTClose(hDeduper);
		logger.info("Processed " + count + " items");
		return true;
	}
	
	/**
	 * determine if a string is empty
	 * @param str
	 * @return
	 */
	private boolean emptyString(String str)
	{
		return (str == null || str.trim().length() == 0);
	}
	
	/**
	 * validate a set of name/value pairs
	 * @param params
	 * @return
	 */
	private boolean validateParameters(Properties props)
	{
		boolean okay = true;
		
		if (props == null)
		{
			logger.fatal("No parameters for " + this.getClass().getSimpleName());
			return false;
		}
		
		for (String key : arguments)
		{
			if (props.containsKey(key))
			{
				if (emptyString(props.getProperty(key)))
				{
					logger.fatal("Missing value for " + key);
					okay = false;
				}
			}
			else
			{
				logger.fatal("Missing key " + key);
				okay = false;
			}
		}
		return okay;
	}
}

