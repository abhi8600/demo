/**
 * $Id: SetMatchKeys.java,v 1.5 2010-05-27 18:14:41 ricks Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse.transforms;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.apache.log4j.Logger;

import DtApi.DtApi;

import com.successmetricsinc.security.EncryptionService;

/**
 * Java transform class for matching staging table records against themselves
 */
class SetMatchKeys implements IJavaTransform
{
	private static Logger logger = Logger.getLogger(SetMatchKeys.class);
	private static String[] arguments = 
	{
		"melissadata.library",			// Dt3Api (Dt3Api.dll needs to be in the PATH)
		"melissadata.registration",		// license information
		"melissadata.dtloc",			// location of the MatchUpAPI directory (normally c:/program files/matchupapi)
		"melissadata.cassloc",			// location of the MatchUPAPI CASS directory (normally c:/program files/matchupapi/cassmate)
										// CASS is used for address correction
		"melissadata.matchcode",		// name of the matchcode (from the MatchCode editor)
		
		"database.driverstring",		// driver string (normally com.microsoft.sqlserver.jdbc.SQLServerDriver)
		"database.connectionstring",	// connection string (normally jdbc:sqlserver://<host>:<port>;databaseName=<databasename>
		"database.username",			// username in cleartext
		"database.password",			// encrypted password, encrypted using the repository db password encryption
				
		"stagingtable.name",			// name of the staging table
		"stagingtable.matchkey",		// match key column, varchar(128)
		"stagingtable.clusterkey",		// cluster key column, varchar(128)
		"stagingtable.zip5",			// zip5 column
		"stagingtable.addr1",			// addr1 column
		"stagingtable.firstname",		// firstname column
		"stagingtable.lastname",		// lastname column
	};
	
	// melissa data variables
	private long hDeduper;						// opaque handle
    private int keySize; 						// size of the key used for matching
    private int blockSize;						// size of the cluster key used for storage and performance

    // database variables
	private Connection conn;					// database connection

	// prepared statements
	private PreparedStatement stagingTableQueryStmt;

	public boolean init(Map<String,String> params)
	{		
		// process the arguments
		if (!validateParameters(params))
			return false;
		
		// process the melissa data arguments
		String library = params.get("melissadata.library");
		String registration = params.get("melissadata.registration");
		String dtloc = params.get("melissadata.dtloc");
		String cassloc = params.get("melissadata.cassloc");
		String matchcode = params.get("melissadata.matchcode");

		logger.info("melissadata.library = " + library);
		logger.info("melissadata.registration = " + registration);
		logger.info("melissadata.dtloc = " + dtloc);
		logger.info("melissadata.cassloc = " + cassloc);
		logger.info("melissadata.matchcode = " + matchcode);
		
		// process the database parameters
		String driverString = params.get("database.driverstring");
		String connectionString = params.get("database.connectionstring");
		String username = params.get("database.username");
		String password = params.get("database.password");
				
		// staging table
		String stagingTable = params.get("stagingtable.name");
		String stagingTableMatchKey = params.get("stagingtable.matchkey");
		String stagingTableClusterKey = params.get("stagingtable.clusterkey");
		String stagingTableZip5 = params.get("stagingtable.zip5");
		String stagingTableAddr1 = params.get("stagingtable.addr1");
		String stagingTableFirstName = params.get("stagingtable.firstname");
		String stagingTableLastName = params.get("stagingtable.lastname");
		
		String stagingTableQuery = "SELECT " + stagingTableZip5 + ", " + stagingTableAddr1 + ", "
								+ stagingTableFirstName + ", " + stagingTableLastName + ", "
								+ stagingTableMatchKey + ", " +  stagingTableClusterKey 
								+ " FROM " + stagingTable;
		logger.debug(stagingTableQuery);

		// load the melissadata DLL
		try
		{
			System.loadLibrary(library);
		}
		catch (Exception ex)
		{
			logger.fatal(ex.getMessage(), ex);
			return false;
		}
		
		// turn off debugging messages
		DtApi.DTDebugMessages(false);
		
		// check the license
		DtApi.DTRegister(registration);
		/*
		if (!DtApi.DTRegister(registration))
		{
			logger.fatal("MelissaData registration failed, probably bad license - '" + registration + "'");
			return false;
		}
		*/

		// initialize the matching engine
		hDeduper = DtApi.DTInitHybrid(matchcode, dtloc, cassloc);

		if (hDeduper == 0)
		{
			switch (DtApi.DTLastError())
			{
			case DtApi.DTE_DTLOC:
				logger.fatal("DTLoc parameter (melissadata.dtloc) is incorrect or lookup table(s) missing - '" + dtloc + "'");
				return false;
			case DtApi.DTE_CASSLOC:
				logger.fatal("CassLoc parameter (melissadata.cassloc) is incorrect or CASS table(s) missing - '" + cassloc + "'");
				return false;
			case DtApi.DTE_MATCHCODE:
				logger.fatal("Matchcode parameter (melissadata.matchcode) is incorrect - '" + matchcode + "'");
				return false;
			case DtApi.DTE_NOCASS:
				logger.fatal("Matchcode uses CASS, but CASS is not activated");
				return false;
			default:
				logger.fatal("Unknown error");
				return false;
			}
		}

		// output a bunch of debugging information
		logger.debug("Match Code: " + DtApi.DTGetMatchcodeName(hDeduper));
		for (int i = 1; i <= DtApi.DTGetComponentCount(hDeduper); i++) { 
			logger.debug("Match Code " + i);
			logger.debug("Type: " + DtApi.DTGetComponentType(hDeduper, i));
			logger.debug("Size: " + DtApi.DTGetComponentSize(hDeduper, i));
			logger.debug("Label: " + DtApi.DTGetComponentLabel(hDeduper, i));
			logger.debug("Words: " + DtApi.DTGetComponentWords(hDeduper, i));
			logger.debug("Start: " + DtApi.DTGetComponentStart(hDeduper, i));
			logger.debug("Trim: " + DtApi.DTGetComponentTrim(hDeduper, i));
			logger.debug("Fuzzy: " + DtApi.DTGetComponentFuzzy(hDeduper, i));
			logger.debug("Short: " + DtApi.DTGetComponentShort(hDeduper, i));
			logger.debug("Swap: " + DtApi.DTGetComponentSwap(hDeduper, i));
			logger.debug("Combinations: " + Long.toBinaryString(DtApi.DTGetComponentCombinations(hDeduper, i))); 
		}
		
        keySize = DtApi.DTKeySize(hDeduper); 		// size of the key used for matching
        blockSize = DtApi.DTBlockSize(hDeduper);	// size of the cluster key used for storage and performance
        
        logger.debug("Keysize: " + keySize + ", blocksize: " + blockSize);
		
        // verify that the key fields are satisfied by the ruleset
		boolean bMappingOk = DtApi.DTMapComponentEx(hDeduper, 1, DtApi.MP_ZIP5);
		bMappingOk &= DtApi.DTMapComponentEx(hDeduper, 2, DtApi.MP_LAST);
		bMappingOk &= DtApi.DTMapComponentEx(hDeduper, 3, DtApi.MP_FIRST);
		bMappingOk &= DtApi.DTMapComponentEx(hDeduper, 4, DtApi.MP_ADDR);
		if (!bMappingOk)
		{
			logger.debug("Incorrect Matchcode mapping");
			return false;
		}
		
		// dump out the map components
		logger.debug("Map Count: " + DtApi.DTGetMapCount(hDeduper));
		for (int i = 1; i <= DtApi.DTGetMapCount(hDeduper); i++)
		{
			logger.debug("Component Type: " + DtApi.DTGetMapComponentType(hDeduper, i));
			logger.debug("Component Label: " + DtApi.DTGetMapComponentLabel(hDeduper, i));
		}

		// load the JDBC driver
		try
		{
			Class.forName(driverString);
		}
		catch (ClassNotFoundException cnfex)
		{
			logger.fatal(cnfex.getMessage(), cnfex);
			return false;
		}

		try
		{
			// connect to the database
			conn = DriverManager.getConnection(connectionString, username, EncryptionService.getInstance().decrypt(password));

			// build the prepared statements
			stagingTableQueryStmt = conn.prepareStatement(stagingTableQuery, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
		}
		catch (SQLException sex)
		{
			logger.fatal(sex.getMessage(), sex);
			return false;
		}
		return true;
	}
	
	public boolean execute()
	{
		int count = 0;	// number of items processed
		
		try
		{
			// iterate over the staging table
			ResultSet rs = stagingTableQueryStmt.executeQuery();
			while (rs.next())
			{
				// build the match and cluster keys
				String zip5 = rs.getString(1);
				if (zip5 != null)
					zip5 = zip5.trim();
				String addr1 = rs.getString(2);
				if (addr1 != null)
					addr1 = addr1.trim();
				String firstname = rs.getString(3);
				if (firstname != null)
					firstname = firstname.trim();
				String lastname = rs.getString(4);
				if (lastname != null)
					lastname = lastname.trim();
				
				String fields = zip5 + '\t' + lastname + '\t' + firstname + '\t' + addr1;
				String key = DtApi.DTBuildKey(hDeduper, fields);
				String clusterKey = key.substring(0, blockSize);
				logger.debug("Key: '" + key + "', clusterKey: " + clusterKey + ", fields: " + fields);
				
				rs.updateString(5, key);
				rs.updateString(6, clusterKey);
				rs.updateRow();
				count++;
			}
			rs.close();
		}
		catch (SQLException sex)
		{
			logger.fatal(sex.getMessage(), sex);
			return false;
		}
		
		// close down melissadata
		DtApi.DTClose(hDeduper);
		logger.info("Processed " + count + " items");
		return true;
	}
	
	/**
	 * determine if a string is empty
	 * @param str
	 * @return
	 */
	private boolean emptyString(String str)
	{
		return (str == null || str.trim().length() == 0);
	}
	
	/**
	 * validate a set of name/value pairs
	 * @param params
	 * @return
	 */
	private boolean validateParameters(Map<String,String> params)
	{
		boolean okay = true;
		
		if (params == null)
		{
			logger.fatal("No parameters for " + this.getClass().getSimpleName());
			return false;
		}
		
		for (String key : arguments)
		{
			if (params.containsKey(key))
			{
				if (emptyString(params.get(key)))
				{
					logger.fatal("Missing value for " + key);
					okay = false;
				}
			}
			else
			{
				logger.fatal("Missing key " + key);
				okay = false;
			}
		}
		return okay;
	}
}

