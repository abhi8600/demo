/**
 * $Id: IJavaTransform.java,v 1.1 2008-07-07 23:32:36 ricks Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse.transforms;

import java.util.Map;

/**
 * Interface for classes that are used as Java transformations for staging tables
 */
public interface IJavaTransform
{
	public abstract boolean init(Map<String,String> parameters);
	public abstract boolean execute();
}
