/**
 * $Id: SourceFile.java,v 1.47 2012-11-22 12:04:45 BIRST\mjani Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jdom.Namespace;

import com.amazonaws.services.s3.model.GetObjectRequest;
import com.successmetricsinc.Repository;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.util.RepositoryException;
import com.successmetricsinc.util.SMIFilenameFilter;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author Brad Peters
 * 
 */
public class SourceFile implements Serializable
{

	private static final long serialVersionUID = 2L;
	private static Logger logger = Logger.getLogger(SourceFile.class);
	private static final String DEFAULT_ENCODING = "iso-8859-1"; // ISO Latin-1

	public enum FileFormatType
	{
		FixedWidth, Delimited
	}
	public String FileName;	// if S3Bucket and S3Folder are set, this is the Name component s3://<S3Bucket>/<S3Folder>/<FileName>
	private String encoding; // iso-8859-1 or Unicode
	public FileFormatType Type;
	public String Separator;
	public int IgnoredRows;
	public int IgnoredLastRows;
	public int NumRows;
	public boolean HasHeaders;
	public SourceColumn[] Columns;
	public SourceColumn[] ImportColumns;
	public String SearchPattern;  // only on the FileName (or S3 name portion) portion
	public String S3Bucket;
	public String S3Folder;
	public char Quote;
	public String FilterColumn;
	public String FilterOperator;
	private String FilterOperatorRepository;
	public String FilterValue;
	public String[][] Escapes;
	public int[] KeyIndices;
	public static final int ESCAPE_SUBSTRING = 0; // Escape anywhere in the field
	public static final int ESCAPE_ALL = 1; // Escape if field exactly matches
	public static final int ESCAPE_START = 2; // Escape if field starts with match
	public static final int ESCAPE_END = 3; // Escape if field ends with match
	public int[] EscapeTypes;
	private boolean ignoreBackslash = false;
	public String InputTimeZoneID;
	private TimeZone InputTimeZone;
	public boolean ContinueIfMissing = false;
	public int MinRows;
	public boolean ReplaceWithNull;
	public boolean SkipRecord;	
	//To be used for tracking number of rows written, errors, warnings etc. for scripted sources
	public long NumRowsWritten = 0;
	public long NumRowsError = 0;
	public long NumRowsWarning = 0;
	private String ftext;
	private long FileLength; 
	private String LastUploadDate;
	
	/*
	 * Force the number of columns to match - this will ignore carriage returns and line feeds in every field except the
	 * last one
	 */
	public boolean ForceNumColumns;
	/*
	 * Only consider a quote to start if it occurs right after a separator
	 */
	public boolean OnlyQuoteAfterSeparator;
	// do not treat carriage return as a newline character
	public boolean DoNotTreatCarriageReturnAsNewline = false;
	
	public boolean LockFormat;
	public boolean SchemaLock;
	public boolean AllowAddColumns;
	public boolean AllowNullBindingRemovedColumns;
	public boolean AllowUpcast;
	public boolean AllowVarcharExpansion;
	public boolean AllowValueTruncationOnLoad;
	public boolean FailUploadOnVarcharExpansion;
	public boolean CustomUpload;
	public String BadColumnValueAction;
	
	private String Guid;
	private String CreatedDate;
	private String LastModifiedDate;
	private String CreatedUsername;
	private String LastModifiedUsername;
	public String[][] EscapesSF;
	private boolean isEscapesTag;

	public SourceFile(Element e, Namespace ns) throws RepositoryException
	{
		Guid = e.getChildText("Guid",ns);
		CreatedDate = e.getChildText("CreatedDate",ns);
		LastModifiedDate = e.getChildText("LastModifiedDate",ns);
		CreatedUsername = e.getChildText("CreatedUsername",ns);
		LastModifiedUsername = e.getChildText("LastModifiedUsername",ns);
		
		FileName = e.getChildTextTrim("FileName", ns);
		encoding = e.getChildTextTrim("Encoding", ns);
		if (encoding == null || encoding.length() == 0)
		{
			encoding = DEFAULT_ENCODING;
		}
		else
		{
			encoding = Util.intern(encoding);
		}
		ftext = e.getChildText("FormatType", ns);
		if (ftext != null && ftext.equals("FixedWidth"))
			Type = FileFormatType.FixedWidth;
		else
			Type = FileFormatType.Delimited;
		Separator = e.getChildText("Separator", ns);
		String qtext = e.getChildText("Quote", ns);
		if (qtext == null || qtext.length() == 0)
			Quote = 0;
		else
			Quote = qtext.charAt(0);
		IgnoredRows = Integer.valueOf(e.getChildText("IgnoredRows", ns));
		String temp = e.getChildText("IgnoredLastRows", ns);
		if (temp != null && temp.length() > 0)
			IgnoredLastRows = Integer.valueOf(temp);
		HasHeaders = Boolean.valueOf(e.getChildText("HasHeaders", ns));
		NumRows = Integer.valueOf(e.getChildText("NumRows", ns));
		Element cols = e.getChild("Columns", ns);
		if (cols != null)
		{
			@SuppressWarnings("rawtypes")
			List l2 = cols.getChildren();
			Columns = new SourceColumn[l2.size()];
			for (int count2 = 0; count2 < l2.size(); count2++)
			{
				SourceColumn sc = new SourceColumn((Element) l2.get(count2), ns);
				Columns[count2] = sc;
			}
		}
		cols = e.getChild("ImportColumns", ns);
		if (cols != null)
		{
			@SuppressWarnings("rawtypes")
			List l2 = cols.getChildren();
			ImportColumns = new SourceColumn[l2.size()];
			for (int count2 = 0; count2 < l2.size(); count2++)
			{
				SourceColumn sc = new SourceColumn((Element) l2.get(count2), ns);
				ImportColumns[count2] = sc;
			}
		}
		SearchPattern = Util.intern(e.getChildText("SearchPattern", ns));
		S3Bucket = Util.intern(e.getChildText("S3Bucket", ns));
		S3Folder = Util.intern(e.getChildText("S3Folder", ns));
		FilterColumn = Util.intern(e.getChildText("FilterColumn", ns));
		FilterValue = Util.intern(e.getChildText("FilterValue", ns));
		FilterOperator = Util.intern(e.getChildText("FilterOperator", ns));
		FilterOperatorRepository = Util.intern(e.getChildText("FilterOperator", ns));
		//Backward compatibility - filter column is specified but filter operator is not specified, set it to "=".
		if(FilterColumn != null && FilterColumn.length() > 0 && (FilterOperator == null || FilterOperator.length() == 0))
		{
			FilterOperator = "=";
		}
		if (e.getChild("Escapes", ns) != null)
		{
		  isEscapesTag = true;
			Escapes = Repository.getDoubleStringArrayChildren(e, "Escapes", ns);
			EscapesSF = Repository.getDoubleStringArrayChildren(e, "Escapes", ns);
			EscapeTypes = new int[Escapes.length];
			for (int i = 0; i < Escapes.length; i++)
			{
				// If the escape starts and ends with a '^', escape only the entire field
				if (Escapes[i][0].charAt(0) == '^' && Escapes[i][0].charAt(Escapes[i][0].length() - 1) == '^')
				{
					EscapeTypes[i] = ESCAPE_ALL;
					Escapes[i][0] = Escapes[i][0].substring(1, Escapes[i][0].length() - 1);
				}
				// If the escape starts with a '^', escape only the start
				else if (Escapes[i][0].charAt(0) == '^')
				{
					EscapeTypes[i] = ESCAPE_START;
					Escapes[i][0] = Escapes[i][0].substring(1);
				}
				// If the escape ends with a '^', escape only the end
				else if (Escapes[i][0].charAt(Escapes[i][0].length() - 1) == '^')
				{
					EscapeTypes[i] = ESCAPE_END;
					Escapes[i][0] = Escapes[i][0].substring(0, Escapes[i][0].length() - 1);
				}
				// Escape anywhere in the field
				else
					EscapeTypes[i] = ESCAPE_SUBSTRING;
			}
		}
		if (e.getChild("KeyIndices",ns)!=null)
		{
			KeyIndices = Repository.getIntArrayChildren(e, "KeyIndices", ns);
		}
		String text = e.getChildText("ForceNumColumns", ns);
		if (text != null)
			ForceNumColumns = Boolean.valueOf(text);
		text = e.getChildText("OnlyQuoteAfterSeparator", ns);
		if (text != null)
			OnlyQuoteAfterSeparator = Boolean.valueOf(text);
		text = e.getChildText("DoNotTreatCarriageReturnAsNewline", ns);
		if (text != null)
			DoNotTreatCarriageReturnAsNewline = Boolean.valueOf(text);
		text = e.getChildText("IgnoreBackslash", ns);
		if (text != null)
			ignoreBackslash = Boolean.valueOf(text);
		
		text = e.getChildText("ContinueIfMissing", ns);
		if (text != null)
			ContinueIfMissing = Boolean.valueOf(text);
		
		String tempMinRows = e.getChildText("MinRows", ns);
		if (tempMinRows != null && tempMinRows.length() > 0)
		{
			try
			{
				MinRows = Integer.valueOf(tempMinRows);
			}
			catch(NumberFormatException nfe)
			{
				throw new RepositoryException("Invalid value of MinRows: \"" + tempMinRows + "\" in source file "+FileName);
			}
		}
		
		this.InputTimeZoneID = Util.intern(e.getChildText("InputTimeZone", ns));
		
		text = e.getChildText("BadColumnValueAction", ns);
		BadColumnValueAction = e.getChildText("BadColumnValueAction", ns);
		if (text != null && text.equals("SkipRecord"))
			SkipRecord = true;
		else
			ReplaceWithNull = true;
		
		text = e.getChildText("FileLength", ns);
		if (text!=null)
		{
			FileLength = Long.parseLong(text);
		}
		
		LastUploadDate = e.getChildText("LastUploadDate", ns);
		
		text = e.getChildText("LockFormat", ns);
		if (text != null)
			LockFormat = Boolean.valueOf(text);
		
		text = e.getChildText("SchemaLock", ns);
		if (text != null)
			SchemaLock = Boolean.valueOf(text);
		
		text = e.getChildText("AllowAddColumns", ns);
		if (text != null)
			AllowAddColumns = Boolean.valueOf(text);
		
		text = e.getChildText("AllowNullBindingRemovedColumns", ns);
		if (text != null)
			AllowNullBindingRemovedColumns = Boolean.valueOf(text);
		
		text = e.getChildText("AllowUpcast", ns);
		if (text != null)
			AllowUpcast = Boolean.valueOf(text);
		
		text = e.getChildText("AllowVarcharExpansion", ns);
		if (text != null)
			AllowVarcharExpansion = Boolean.valueOf(text);
		
		text = e.getChildText("AllowValueTruncationOnLoad", ns);
		if (text != null)
			AllowValueTruncationOnLoad = Boolean.valueOf(text);
		
		text = e.getChildText("FailUploadOnVarcharExpansion", ns);
		if (text != null)
			FailUploadOnVarcharExpansion = Boolean.valueOf(text);
		
		text = e.getChildText("CustomUpload", ns);
		if (text != null)
			CustomUpload = Boolean.valueOf(text);	
		
		// logger.debug("File: " + FileName + ", Encoding: " + encoding + ", Separator: " + cleanup(Separator) + ", Quote: " + cleanup(Quote) + ", IgnoredRows: " + IgnoredRows + ", IgnoredLastRows: " + IgnoredLastRows + ", HasHeaders: " + HasHeaders + ", IgnoreBackslash: " + ignoreBackslash + ", InputTimeZone: " + InputTimeZoneID);
	}
	
	public Element getSourceFileElement(Namespace ns)
	{
		Element e = new Element("SourceFile",ns);
		
		Repository.addBaseAuditObjectAttributes(e, Guid, CreatedDate, LastModifiedDate, CreatedUsername, LastModifiedUsername, ns);
		
		Element child = new Element("FileName",ns);
		child.setText(FileName);
		e.addContent(child);
		
		XmlUtils.addContent(e, "Encoding", encoding,ns);
		
		child = new Element("Separator",ns);
		child.setText(Separator);
		e.addContent(child);
		
		child = new Element("IgnoredRows",ns);
		child.setText(String.valueOf(IgnoredRows));
		e.addContent(child);
		
		child = new Element("HasHeaders",ns);
		child.setText(String.valueOf(HasHeaders));
		e.addContent(child);
		
		child = new Element("IgnoredLastRows",ns);
		child.setText(String.valueOf(IgnoredLastRows));
		e.addContent(child);
		
		if (InputTimeZoneID!=null)
		{
			child = new Element("InputTimeZone",ns);
			child.setText(InputTimeZoneID);
			e.addContent(child);
		}
		
		child = new Element("Columns",ns);
		if (Columns!=null && Columns.length>0)
		{
			for (SourceColumn sc : Columns)
			{
				child.addContent(sc.getSourceColumnElement(ns));
			}
		}
		e.addContent(child);
				
		if (ImportColumns!=null && ImportColumns.length>0)
		{
			child = new Element("ImportColumns",ns);
			for (SourceColumn sc : ImportColumns)
			{
				child.addContent(sc.getSourceColumnElement(ns));
			}
			e.addContent(child);
		}		
		
		child = new Element("FormatType",ns);
		child.setText(ftext);
		e.addContent(child);
		
		if (SearchPattern!=null)
		{
			XmlUtils.addContent(e, "SearchPattern", SearchPattern,ns);
		}
		
		if (Quote!=0x0)
		{
			child = new Element("Quote",ns);
			child.setText(String.valueOf(Quote));
			e.addContent(child);
		}
		else
		{
			child = new Element("Quote",ns);
			e.addContent(child);
		}
		
		child = new Element("NumRows",ns);
		child.setText(String.valueOf(NumRows));
		e.addContent(child);	
		
		child = new Element("FileLength",ns);
		child.setText(String.valueOf(FileLength));
		e.addContent(child);
		
		child = new Element("LastUploadDate",ns);
		child.setText(LastUploadDate);
		e.addContent(child);
		
		if (FilterColumn!=null)
		{
			child = new Element("FilterColumn",ns);
			child.setText(FilterColumn);
			e.addContent(child);
		}
		
		if (FilterOperatorRepository!=null)
		{
			child = new Element("FilterOperator",ns);
			child.setText(FilterOperatorRepository);
			e.addContent(child);
		}
		
		if (FilterValue!=null)
		{
			child = new Element("FilterValue",ns);
			child.setText(FilterValue);
			e.addContent(child);
		}
		
		if (EscapesSF!=null && EscapesSF.length>0)
		{
			child = new Element("Escapes",ns);
			for (String[] lvl1 : EscapesSF)
			{
				Element superElement = new Element("ArrayOfString",ns);
				for (String lvl2 : lvl1)
				{
					Element stringElement = new Element("string",ns);
					if (lvl2!=null && !lvl2.isEmpty())
					{
					stringElement.addContent(lvl2);
					}
					superElement.addContent(stringElement);
				}
				child.addContent(superElement);
			}
			e.addContent(child);
		}
		else if (isEscapesTag)
		{
		  child = new Element("Escapes",ns);
		  e.addContent(child);
		}
				
		if (KeyIndices!=null && KeyIndices.length>0)
		{
			child = new Element("KeyIndices",ns);
			for (int keyIndice : KeyIndices)
			{
				Element intElement = new Element("int",ns); //TODO Is this int or integer
				intElement.setText(String.valueOf(keyIndice));
				child.addContent(intElement);
			}
			e.addContent(child);
		}			
		
		child = new Element("ForceNumColumns",ns);
		child.setText(String.valueOf(ForceNumColumns));
		e.addContent(child);
		
		child = new Element("OnlyQuoteAfterSeparator",ns);
		child.setText(String.valueOf(OnlyQuoteAfterSeparator));
		e.addContent(child);
		
		child = new Element("DoNotTreatCarriageReturnAsNewline",ns);
		child.setText(String.valueOf(DoNotTreatCarriageReturnAsNewline));
		e.addContent(child);
		
		child = new Element("IgnoreBackslash",ns);
		child.setText(String.valueOf(ignoreBackslash));
		e.addContent(child);
		
		child = new Element("LockFormat",ns);
		child.setText(String.valueOf(LockFormat));
		e.addContent(child);
		
		child = new Element("SchemaLock",ns);
		child.setText(String.valueOf(SchemaLock));
		e.addContent(child);
		
		if (AllowAddColumns)
		{
			child = new Element("AllowAddColumns",ns);
			child.setText(String.valueOf(AllowAddColumns));
			e.addContent(child);
		}
		
		if (AllowNullBindingRemovedColumns)
		{
			child = new Element("AllowNullBindingRemovedColumns",ns);
			child.setText(String.valueOf(AllowNullBindingRemovedColumns));
			e.addContent(child);
		}
		
		if (AllowUpcast)
		{
			child = new Element("AllowUpcast",ns);
			child.setText(String.valueOf(AllowUpcast));
			e.addContent(child);
		}
		
		if (AllowVarcharExpansion)
		{
			child = new Element("AllowVarcharExpansion",ns);
			child.setText(String.valueOf(AllowVarcharExpansion));
			e.addContent(child);
		}
		
		if (AllowValueTruncationOnLoad)
		{
			child = new Element("AllowValueTruncationOnLoad",ns);
			child.setText(String.valueOf(AllowValueTruncationOnLoad));
			e.addContent(child);
		}
		
		if (FailUploadOnVarcharExpansion)
		{
			child = new Element("AllowValueTruncationOnLoad",ns);
			child.setText(String.valueOf(FailUploadOnVarcharExpansion));
			e.addContent(child);
		}
		
		child = new Element("ContinueIfMissing",ns);
		child.setText(String.valueOf(ContinueIfMissing));
		e.addContent(child);
		
		child = new Element("MinRows",ns);
		child.setText(String.valueOf(MinRows));
		e.addContent(child);
		
		if (CustomUpload)
		{
			child = new Element("CustomUpload",ns);
			child.setText(String.valueOf(CustomUpload));
			e.addContent(child);
		}
		
		if (BadColumnValueAction!=null)
		{
			child = new Element("BadColumnValueAction",ns);
			child.setText(BadColumnValueAction);
			e.addContent(child);
		}
		
		return e;
	}
	
	public TimeZone getInputTimeZone() {
		return InputTimeZone;
	}

	public void setInputTimeZone(TimeZone timezone) {
		this.InputTimeZone = timezone;
	}
	
	public String getEncoding()
	{
		if (encoding.equals("Unicode") || encoding.equals("UTF-16(Little Endian)"))
			return "UTF-16LE";
		if (encoding.equals("UTF-16(Big Endian)"))
			return "UTF-16BE";
		return encoding;
	}

	public void setEncoding(String encoding)
	{
		this.encoding = encoding;
	}

	public boolean isValidColumnExpression(String expression)
	{
		if (expression == null || expression.length() == 0)
		{
			return false;
		}
		for (SourceColumn sc : Columns)
		{
			if (expression.equals(sc.Name))
			{
				return false;
			}
		}
		return ((expression.indexOf('{') != -1) && (expression.indexOf('}') != -1));
	}

	public String replaceColumnNamesWithIndices(String expression)
	{
		String replacedExpression = expression;
		for (int i = 0; i < Columns.length; i++)
		{
			SourceColumn sc = Columns[i];
			replacedExpression = Util.replaceStr(replacedExpression, "{" + sc.Name + "}", "{" + i + "}");
		}
		return replacedExpression;
	}

	public boolean isIgnoreBackslash() {
		return ignoreBackslash;
	}

	public void setIgnoreBackslash(boolean ignoreBackslash) {
		this.ignoreBackslash = ignoreBackslash;
	}
	
		
	public List<BirstDataSource> getPhysicalSourceFiles(String path, boolean addTempSuffix, Repository r, boolean lookForPartitionFiles, DatabaseConnection dc, String nonASCIIFileName) throws Exception
	{
		File pathf = new File(path);

		if (!pathf.exists() || !pathf.isDirectory()) // data directory does not exist, needed for .tmp/.format files
			return null;
		
		List<BirstDataSource> sfLst = new ArrayList<BirstDataSource>();
		String connectionPrefix = null;
		if(lookForPartitionFiles && r.getSourceFileSplitter(path).isSourceFileSplit(this))
		{
			connectionPrefix = r.partitionConnection;
		}
	
		if ((SearchPattern != null) && (!SearchPattern.trim().equals("")))
		{
			String sp = SearchPattern;
			if(connectionPrefix != null)
			{
				if(sp.startsWith("*"))
				{
					sp = "*" + connectionPrefix + "_" +sp.substring(1);
				}
				else
				{
					sp = connectionPrefix + "_" + sp;
				}
			}
			// XXX no search pattern support for S3 right now
			SMIFilenameFilter ff = new SMIFilenameFilter();
			ff.setSearchPattern(sp);
			String[] fnames = pathf.list(ff);
			if (fnames != null)
			{
				logger.debug("fnames: " + fnames.length);
				for(String fname : fnames)
				{
					// adding gz extension to support bulkupload for new paraccel version  
				 	String name = path + File.separator + fname + getSuffix(dc, addTempSuffix);
					BirstDataSource f = new BirstFile(name);
					
					if (f.exists() && f.isFile())
						sfLst.add(f);
				}
			//	Collections.sort(sfLst); XXX need a real sorter here
			}			
		}
		else
		{
		// XXX no partitions for S3 right now
			if (S3Bucket != null)
			{
				BirstS3Object f = new BirstS3Object(S3Bucket, S3Folder, FileName);
				if (f.exists() && f.isFile()) // XXX need S3Client for exists check
					sfLst.add(f);
			}
			else
			{
				String name = path + File.separator + ((connectionPrefix != null) ? (connectionPrefix + "_") : "") 
						+ (nonASCIIFileName != null ? nonASCIIFileName : FileName) + getSuffix(dc, addTempSuffix);
				// in the case of Redshift, the files have been split into name.#, need to look for the existence of those
				List<File> files = Util.getFileList(name);
				BirstFile f = new BirstFile(name);
				if (files != null && files.size() > 0)
					sfLst.add(f);
			}
		}
		return sfLst;
	}
	
	private String getSuffix(DatabaseConnection dc, boolean addTempSuffix)
	{
		return (dc!= null && dc.supportsZippedDataForBulkLoading() ? (addTempSuffix ? ".tmp.gz" : "") : (addTempSuffix ? ".tmp" : ""));
	}
	public SourceColumn findColumn(String scName)
	{
		if(scName == null)
		{
			return null;
		}
		
		for(SourceColumn cursc : Columns)
		{
			if(scName.equals(cursc.Name))
			{
				return cursc;
			}
		}
		
		return null;
	}
	
	public int findColumnIndex(String scName)
	{
		int index = -1;
		if(scName == null)
		{
			return index;
		}
		
		for(int idx = 0; idx < Columns.length; idx++)
		{
			SourceColumn cursc = Columns[idx];
			if(scName.equals(cursc.Name))
			{
				return idx;
			}
		}
		
		return -1;
	}

	public String toString()
	{
		return (FileName);
	}
		/**
	 * interface and classes to encapsulate Files and S3Objects
	 * @author ricks
	 *
	 */
	public abstract static interface BirstDataSource
	{
		public String getLocalAbsolutePath(String path); // path on the local host (for S3)
		long lastModified();
		String getName();
		String getAbsolutePath();
		boolean isFile();
		boolean exists();
		String getLogName();
	}
	
	@SuppressWarnings("serial")
	public static class BirstFile extends File implements BirstDataSource
	{
		public BirstFile(String pathname) {
			super(pathname);
		}
		@Override
		public String getLocalAbsolutePath(String path)
		{
			return getAbsolutePath();
		}
		@Override
		public String getLogName()
		{
			return getName();
		}
	}
	
	public static class BirstS3Object implements BirstDataSource 
	{
		GetObjectRequest req;
		String bucket;
		String folder;
		String name;
		
		private String getKey()
		{
			return (folder != null ? (folder + '/') : "") + name;
		}
		
		public BirstS3Object(String _bucket, String _folder, String _name) 
		{
			bucket = _bucket;
			folder = _folder;
			name = _name;
			req = new GetObjectRequest(bucket, getKey());
		}
		@Override
		public boolean exists()
		{
			return true; // XXX needs S3Client to check for existence
		}
		@Override
		public boolean isFile()
		{
			return (name != null && name.length() > 0);
		}
		@Override
		public String getName()
		{
			return name;
		}
		@Override
		public String getAbsolutePath()
		{
			return "s3://" + bucket + '/' + getKey();
		}		
		@Override
		public String getLocalAbsolutePath(String path)
		{
			return path + '/' + getName();
		}
		@Override
		public long lastModified()
		{
			return Long.MAX_VALUE; // can not use the actual last modified value for the bucket since the time is not synchronized with the Birst data center time source
		}
		@Override
		public String getLogName()
		{
			return getAbsolutePath();
		}
		public GetObjectRequest getObjectRequest()
		{
			return req;
		}
	}
}
