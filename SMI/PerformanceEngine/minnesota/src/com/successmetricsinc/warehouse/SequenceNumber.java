/**
 * $Id: SequenceNumber.java,v 1.11 2012-01-09 14:45:50 rchandarana Exp $
 *
 * Copyright (C) 2007-2009 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import com.successmetricsinc.Main;
import com.successmetricsinc.Repository;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.Hierarchy;
import com.successmetricsinc.query.Level;
import com.successmetricsinc.query.OrderBy;
import com.successmetricsinc.query.Query;

/**
 * @author bpeters
 * 
 */
public class SequenceNumber
{
	private static Logger logger = Logger.getLogger(Main.class);
	public String Dimension;
	public String Column;
	private int curValue;

	public static SequenceNumber getSequenceNumber(Repository r, StagingTable st, StagingColumn sc, boolean initializeValue)
	{
		if (sc.SourceFileColumn != null || !sc.NaturalKey || (sc.Transformations != null && sc.Transformations.length > 0) || !sc.DataType.equals("Integer"))
			return (null);
		String dim = getDimension(sc, r);
		
		if (dim != null)
		{
			SequenceNumber sn = new SequenceNumber();
			sn.Dimension = dim;
			sn.Column = sc.Name;
			int start = 0;
			if (initializeValue)
			{
				/* If initializing the sequence number, try to get the current max value and increment it */
				Statement stmt = null;
				ResultSet rs = null;
				try
				{
					Query q = new Query(r);
					q.addDimensionColumn(dim, sc.OriginalName);
					q.addOrderBy(new OrderBy(dim, sc.OriginalName, false));
					q.addTopN(1);
					q.generateQuery(false);
					DatabaseConnection dc = r.getDefaultConnection();
					stmt = dc.ConnectionPool.getConnection().createStatement();
					rs = stmt.executeQuery(q.getQuery());
					if (rs.next())
						start = rs.getInt(1) + 1;
				} catch (Exception e)
				{
					logger.error(e.getMessage(), e);
				}
				finally
				{
					try
					{
						if (rs != null)
							rs.close();
						if (stmt != null)
							stmt.close();
					}
					catch (SQLException e)
					{
						logger.debug(e, e);
					}
				}
			}
			sn.curValue = start;
			return (sn);
		}
		return (null);
	}
	
	public static String getDimension(StagingColumn sc, Repository r)
	{
		String dim = null;
		Hierarchy[] hlist = r.getHierarchies();
		if (sc.TargetTypes != null)
		{
			for (String d : sc.TargetTypes)
			{
				if (d.equals("Measure"))
					continue;
				for (Hierarchy h : hlist)
				{
					if (h.DimensionName.equals(d))
					{
						Level l = h.findLevelKeyColumn(sc.OriginalName);
						if (l != null && l.isKey(sc.OriginalName))
						{
							dim = d;
							break;
						}
					}
				}
				if (dim != null)
					break;
			}
		}
		
		return dim;
	}
	
	public synchronized int getAndIncrement()
	{
		return this.curValue++;
	}
}
