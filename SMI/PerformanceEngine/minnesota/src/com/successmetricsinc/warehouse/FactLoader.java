/**
 * Copyright (C) 2007-2009 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;

import com.amazonaws.auth.AWSCredentials;
import com.successmetricsinc.ExecuteSteps.Step;
import com.successmetricsinc.ExecuteSteps.StepCommand;
import com.successmetricsinc.Repository;
import com.successmetricsinc.query.Database;
import com.successmetricsinc.query.Database.DatabaseType;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.DatabaseIndex;
import com.successmetricsinc.query.DimensionColumn;
import com.successmetricsinc.query.DimensionTable;
import com.successmetricsinc.query.Hierarchy;
import com.successmetricsinc.query.Level;
import com.successmetricsinc.query.LevelKey;
import com.successmetricsinc.query.MeasureColumn;
import com.successmetricsinc.query.MeasureTable;
import com.successmetricsinc.query.MeasureTableGrain;
import com.successmetricsinc.query.Query;
import com.successmetricsinc.query.QueryMap;
import com.successmetricsinc.status.Status;
import com.successmetricsinc.util.Util;

public class FactLoader {
	
	private static Logger logger = Logger.getLogger(FactLoader.class);
	protected Repository r;
	protected DatabaseConnection dconn;
	protected LoadWarehouse lw;
	protected Step step;
	protected List<StagingTable> stList;
	public static final int FLOAT_WIDTH = 10; // width of varchar to convert float to
	// Number of rows to decide to drop indexes before inserts
	public static final long DROP_INDEX_LIMIT = 5000000;
	private LoadWarehouseHelper lwHelper ;
	private static Map<String,List<String>> factToStagingMap = new HashMap<String,List<String>>();
	
	public FactLoader(Repository r, DatabaseConnection dconn, LoadWarehouse lw,  Step step,	List<StagingTable> stList)
	{
		this.r = r;
		this.dconn = dconn;
		this.lw = lw;
		this.step = step;
		this.stList = stList;
		lwHelper = new LoadWarehouseHelper(r,dconn);
	}
	
	/**
	 * Load all fact tables
	 * 
	 * @param stmt
	 * @param loadID
	 * @param status
	 * @throws Exception 
	 */
	void loadFacts(Connection conn, Statement stmt, int loadID, Status status, String loadGroup, boolean noFactIndices, boolean keepFacts,
			String processingGroup, String databasePath, AWSCredentials s3credentials) throws Exception
	{
		List<MeasureTable> generatedTables = LoadWarehouseHelper.getGeneratedMeasureTables(r);

		// Order fact table loading by grains - load higher grains first
		CompareGrains cg = new CompareGrains(r);
		orderTablesByGrain(generatedTables, cg);
		for (MeasureTable mt : generatedTables)
		{
			String substep = LoadWarehouseHelper.getSubstep(mt, loadGroup);
			if (status.isComplete(StepCommand.LoadWarehouse, substep, processingGroup, true, Util.getKillFileName(r.getRepositoryRootPath(), Util.KILL_PROCESS_FILE)))
			{
				logger.info("Measure table " + mt.TableName + " already loaded successfully for load id =" + status.getIteration()
						+ (loadGroup == null ? "" : (", for load group " + loadGroup))
						+ (processingGroup == null ? "" : (", for processingGroup " + processingGroup)) + ". This step will not be repeated.");
				continue;
			}
			status.logStatus(step, substep, processingGroup, Status.StatusCode.Running);

			String mtname = mt.TableSource.Tables[0].PhysicalName;
			if (dconn.DBType == DatabaseType.MSSQL2012ColumnStore)
			{
				Database.dropColumnStoreIndex(dconn, mtname);
			}
			
			long numRows = 0;
			boolean firstst = true;
			boolean matched = false;
			boolean invalidSnapshotDeleteKeysFound = false;
			StagingTable incrementalSnapshotFactSt = null;
			// For each staging table
			Collection<MeasureTableGrain> grainSet = mt.getGrainInfo(r);
			boolean incrementalSnapshotFact = false;
			Set<String> dayIDColumnsMappedToLoadDate = new HashSet<String>();
			Set<String> tableKeys = null;
			Set<String> logicalTableKeys = null;
			for (StagingTable st : stList)
			{
				if (st.isAtMatchingGrain(grainSet))
				{
					matched = true;
					// Build clustered indices
					lwHelper.generateIndices(mt, mtname, true);
					boolean truncate = false;
					if (firstst)
					{
						for (StagingTable st2 : stList)
						{
							if (st2.isAtMatchingGrain(grainSet))
							{
								if (st2.TruncateOnLoad)
								{
									truncate = true;
								}
								if (st2.IncrementalSnapshotFact)
								{
									incrementalSnapshotFact = true;
								}
							}
						}
					}
					Set<String> loadedSet = new HashSet<String>();
					if (r.getCurrentBuilderVersion() >= 18 && !r.AllowNonUniqueGrains)
					{
						/*
						 * Check to see if the grain for this measure table is unique in this staging table. If not,
						 * there can be issues - especially if there are more than one staging table loading this fact
						 * table. That would create a cartesian join - bad.
						 */
						if (!isUniqueGrainKey(grainSet, st, stmt))
						{
							logger.warn("The grain key for source " + st.Name + " (i.e. the combination of the level keys for each level in its grain) is not unique. This can result in cartesian joins.");							
						}
					}
					numRows += loadMeasureTableFromStagingAtGrain(conn, loadID, grainSet, mt, st, stmt, loadedSet, firstst, truncate, noFactIndices,
							generatedTables, keepFacts, processingGroup, databasePath, s3credentials);
					if (incrementalSnapshotFact)
					{
						dayIDColumnsMappedToLoadDate.addAll(st.DayIDColumnsMappedToLoadDate);
						// If delete keys are specified for the staging table, use those.
						if (st.SnapshotDeleteKeys != null && st.SnapshotDeleteKeys.length > 0)
						{
							tableKeys = new HashSet<String>();
							logicalTableKeys = new HashSet<String>();
							for (int i = 0; i < st.SnapshotDeleteKeys.length; i++)
							{
								StagingColumn sc = st.getStagingColumn(st.SnapshotDeleteKeys[i]);
								if (sc == null)
								{
									logger.warn("Invalid snapshot delete key encountered: " + st.SnapshotDeleteKeys[i] + " ,on staging table: " + st.Name);
									invalidSnapshotDeleteKeysFound = true;
								} else
								{
									String ms = sc.getMatchingMeasureColumnName(mt, r,true);
									boolean found = false;
									for (MeasureColumn mc : mt.MeasureColumns)
									{
										if (mc.ColumnName.equals(ms))
										{
											tableKeys.add(mc.PhysicalName);
											logicalTableKeys.add(sc.Name);
											found = true;
											break;
										}
									}
									if (!found)
									{
										logger.warn("Invalid snapshot delete key encountered: " + st.SnapshotDeleteKeys[i] + " ,on staging table: " + st.Name
												+ " - No corresponding measure column found on measure table " + mt.TableName);
										invalidSnapshotDeleteKeysFound = true;
									} else
									{
										incrementalSnapshotFactSt = st;
									}
								}
							}
						}
					}
					if (firstst)
						firstst = false;
				}
			}
			if (matched)
			{
				if (incrementalSnapshotFact)
				{
					if (invalidSnapshotDeleteKeysFound)
					{
						logger.warn("Invalid snapshot delete keys encountered - incremental snapshot will not be processed" + " for measure table: "
								+ mt.TableName);
					} else
					{
						// See if there are table keys
						Set<String> mtKeys = null;
						if (tableKeys != null && tableKeys.size() > 0)
						{
							mtKeys = tableKeys;
						}
						if (mtKeys != null && mtKeys.size() > 0)
						{
							if (loadID <= 1)
								logger.debug("Incremental snapshot will not be processed" + " for measure table: " + mt.TableName + " for LOAD_ID " + loadID);
							else
								processForIncrementalSnapshotFact(conn, mt, mtKeys, stmt, loadID, incrementalSnapshotFactSt, logicalTableKeys, processingGroup);
						} else
						{
							logger.warn("No snapshot delete keys specified - incremental snapshot will not be processed" + " for measure table: "
									+ mt.TableName);
						}
					}
				}
				// Build other indices
				if (!noFactIndices)
				{
					lwHelper.generateIndices(mt, mtname, false);
				}
				if (dconn.DBType == DatabaseType.MSSQL2012ColumnStore)
				{
					List<String> colNames = new ArrayList<String>();
					for (MeasureColumn mc : mt.MeasureColumns)
					{
						// string off qualification and CAST
						String col = Util.unQualifyPhysicalName(mc.PhysicalName);
						int idx = col.indexOf(" AS "); // hack, how does create fact table do this?
						if (idx > 0)
							col = col.substring(0, idx);
						if (!colNames.contains(col))
							colNames.add(col);
					}
					Database.createColumnStoreIndex(dconn, mtname, colNames);
				}
			} else
			{
				logger.warn("No matching staging tables found for measure table: " + mt.TableName);
				numRows = -1;
			}
			status.logStatus(step, substep, processingGroup, Status.StatusCode.Complete, numRows);
		}
	}
	
	private static class CompareGrains implements Comparator<MeasureTable>
	{
		Repository r;

		public CompareGrains(Repository r)
		{
			this.r = r;
		}

		@Override
		public int compare(MeasureTable mt1, MeasureTable mt2)
		{
			Collection<MeasureTableGrain> grainSet1 = mt1.getGrainInfo(r);
			Collection<MeasureTableGrain> grainSet2 = mt2.getGrainInfo(r);
			if (MeasureTableGrain.isAtHigherGrain(r, grainSet1, grainSet2))
				return -1;
			else if (MeasureTableGrain.isAtHigherGrain(r, grainSet2, grainSet1))
				return 1;
			return 0;
		}
	}

	private void orderTablesByGrain(List<MeasureTable> mtables, CompareGrains cg)
	{
		boolean swap;
		do
		{
			swap = false;
			for (int i = 0; i < mtables.size(); i++)
			{
				for (int j = i + 1; j < mtables.size(); j++)
				{
					if (cg.compare(mtables.get(i), mtables.get(j)) > 0)
					{
						MeasureTable mt = mtables.get(j);
						mtables.set(j, mtables.get(i));
						mtables.set(i, mt);
						swap = true;
					}
				}
				if (swap)
					break;
			}
		} while (swap);
	}
	
	private boolean isUniqueGrainKey(Collection<MeasureTableGrain> grainSet, StagingTable st, Statement stmt) throws SQLException
	{
		/*
		 * Check to see if the grain for this measure table is unique in this staging table. If not,
		 * there can be issues - especially if there are more than one staging table loading this fact
		 * table. That would create a cartesian join - bad.
		 */
		StringBuilder uniqueCheck = new StringBuilder("SELECT COUNT(*) FROM (SELECT COUNT(*) CNT FROM " + Database.getQualifiedTableName(dconn, st.Name)
				+ " GROUP BY ");
		boolean isNoColumnInGroupBy = true;
		List<LevelKey> levelList = st.getLevelKeyList(grainSet, r.getTimeDefinition(), false);
		for (LevelKey lk : levelList)
		{
			if (lk.Level.Hierarchy.DimensionName.equals("Time"))
				continue;
			for (String keyName : lk.ColumnNames)
			{
				if (!isNoColumnInGroupBy)
					uniqueCheck.append(",");
				else
					isNoColumnInGroupBy = false;
				StagingColumn sc = st.findStagingColumn(keyName);
				if (sc == null)
					return false;
				uniqueCheck.append(sc.getQualifiedPhysicalFormula(r, null, null));
			}
		}
		uniqueCheck.append(" HAVING COUNT(*)>1) ");
		if (DatabaseConnection.isDBTypeOracle(dconn.DBType))
		{
			uniqueCheck.append("T");
		}
		else
		{
			uniqueCheck.append("AS T");
		}
		if(isNoColumnInGroupBy)
		{
			logger.warn("Could not find any level keys for performing a uniqueness check on source " + st.Name);
			return false;
		}
		logger.info("[QUERY:" + Query.getPrettyQuery(uniqueCheck.toString()) + "]");
		ResultSet rs = null;
		int result = -1;
		try
		{
			rs = stmt.executeQuery(uniqueCheck.toString());
			if (rs.next())
			{
				result = rs.getInt(1);
			}
			logger.debug("Count = " + result);
			if (result > 0)
				return false;
			else
				return true;
		} catch (SQLException sqlEx)
		{
			logger.warn("Error while checking uniqueness of grain key for " + st.Name + ": " + sqlEx.toString());
			throw sqlEx;
		} finally
		{
			try
			{
				if (rs != null)
					rs.close();
			} catch (SQLException ex)
			{
				// do nothing
			}
		}

	}
	
	/**
	 * Load a measure table from a staging table that is at the same grain, returns a set containing natural keys used
	 * for loading this measure table.
	 * 
	 * @param loadID
	 * @param grainSet
	 * @param mt
	 * @param st
	 * @param stmt
	 * @param loadedSet
	 * @param firstst
	 * @throws Exception 
	 */
	private long loadMeasureTableFromStagingAtGrain(Connection conn, int loadID, Collection<MeasureTableGrain> grainSet, MeasureTable mt, StagingTable st,
			Statement stmt, Set<String> loadedSet, boolean firstst, boolean truncate, boolean noFactIndices, List<MeasureTable> mtlist, boolean keepFacts,
			String processingGroup, String databasePath, AWSCredentials s3credentials) throws Exception
	{
		long numRows = 0;
		if (isIgnoreStagingTableForFact(st, mt))
		{
			return numRows;
		}
		/*
		 * Find all dimension key columns that are associated with the grain or are in the same dimension and higher
		 */
		List<LevelKey> levelList = st.getLevelKeyList(grainSet, r.getTimeDefinition(), false);
		Set<DimensionTable> degenerateDimensions = new HashSet<DimensionTable>();
		List<Level> degenerateLevels = new ArrayList<Level>();
		QueryMap qm = new QueryMap();
		String mtPhysicalName = mt.getPhysicalName();
		if (dconn != null)
			qm.setDBType(dconn.DBType);
		// Determine if this measure table is degenerate
		for (MeasureTableGrain mtg : grainSet)
		{
			Hierarchy h = r.findDimensionHierarchy(mtg.DimensionName);
			if (h == null)
			{
				logger.warn("Staging table " + st.Name + " is using hierarchy name " + mtg.DimensionName
						+ ". This hierarchy does not exist in the repository - all references to non-existent"
						+ " hierarchy should be removed from the repository.");
				continue;
			}
			Level l = h.findLevel(mtg.DimensionLevel);
			if (l != null && l.Degenerate)
			{
				degenerateLevels.add(l);
				for (DimensionTable dt : r.DimensionTables)
				{
					String dtPhysicalName = dt.getPhysicalName();
					if (dt.AutoGenerated && dt.DimensionName.equals(mtg.DimensionName) && dt.Level.equals(l.Name) && mtPhysicalName.equals(dtPhysicalName))
						degenerateDimensions.add(dt);
				}
			}
		}
		// Get natural keys
		Map<String, String> nkeys = getNaturalKeys(r, st, mt, qm);
		StringBuilder insertFilter = new StringBuilder();
		StringBuilder createAsTempTableForInsertFilter = new StringBuilder();
		StringBuilder dropTempTableForInsertFilter = new StringBuilder();
		
		String mtname = mt.TableSource.Tables[0].PhysicalName;
		long id = 0;
		/*
		 * If this is the first staging table that's being used to load a fact, either delete any previous records with
		 * this load ID or, if truncating, truncate the table. If this is a subgroup, the fact table may have been
		 * loaded in a previous subgroup or the main group. So, if it's not a truncation, leave the facts from that
		 * group in the table and do an update.
		 */
		if (firstst && (!keepFacts || truncate || !factToStagingMap.containsKey(mtname)))
		{
			if (!truncate)
			{
				/*
				 * should not drop the indexes - bad performance as the table grows in size
				 */
				logger.info("Deleting any records from table " + mt.TableName + " with same load ID");
				String dq = "DELETE FROM " + Database.getQualifiedTableName(dconn, mtname) + " WHERE LOAD_ID=" + loadID;
				id = System.currentTimeMillis();
				logger.info("[QUERY:" + id + ":" + Query.getPrettyQuery(dq) + "]");
				int rows = stmt.executeUpdate(dq);
				if (!conn.getAutoCommit())
					conn.commit();
				logger.info("[DELMT:" + id + ":" + mtname + ":" + rows + "]");
			} else
			{
				logger.info("Truncating table " + mt.TableName);
				String dq = "TRUNCATE TABLE " + Database.getQualifiedTableName(dconn, mtname);
				id = System.currentTimeMillis();
				logger.info("[QUERY:" + id + ":" + Query.getPrettyQuery(dq) + "]");
				stmt.executeUpdate(dq);
				if (!conn.getAutoCommit())
					conn.commit();
				logger.info("[TRUNCMT:" + id + ":" + mtname + "]");
			}
		} else if (!ignoreUpdateForTable(stmt, mtname, loadID)) // bugzilla# 13784 For a given load id If data is loading for the first time (i.e. first PG) then do not execute update query and ignore exists clause while inserting data
		{
			numRows += processUpdatePass(stmt, conn, mtname, mt, st, loadID, degenerateDimensions, loadedSet, nkeys, qm, numRows, insertFilter,
					createAsTempTableForInsertFilter, dropTempTableForInsertFilter, processingGroup);
		}
		int numNonKeys = 0;
		List<String> cnames = new ArrayList<String>();
		Set<String> logicalnames = new HashSet<String>();
		List<String> pnames = new ArrayList<String>();
		List<StagingColumn> scols = new ArrayList<StagingColumn>();
		List<String[]> lookupTables = new ArrayList<String[]>();
		List<String> lookupJoins = new ArrayList<String>();
		/*
		 * Process measure columns
		 */
		for (MeasureColumn mc : mt.MeasureColumns)
		{
			if (cnames.contains(mc.PhysicalName))
			{
				continue;
			}
			StagingColumn foundsc = null;
			for (StagingColumn sc : st.Columns)
			{
				if (sc.columnNameMatchesMeasure(mc.ColumnName, r) && sc.TargetTypes != null)
				{
					if (sc.TargetTypes.contains("Measure"))
					{
						foundsc = sc;
						break;
					} else
					{
						if (sc.NaturalKey)
							foundsc = sc;
						else
							for (LevelKey lk : levelList)
							{
								String measureColumnName = sc.getMatchingMeasureColumnName(mt, r,false);
								if (Arrays.asList(lk.ColumnNames).indexOf(measureColumnName) >= 0)
								{
									foundsc = sc;
									break;
								}
							}
					}
				}
			}
			if ((mc.Qualify) && (foundsc != null))
			{
				String pname = foundsc.getQualifiedPhysicalFormula(r, "B", qm);
				pname = r.replaceVariables(null, pname);
				if (!foundsc.NaturalKey || foundsc.TargetTypes.contains("Measure"))
					numNonKeys++;
				// Make sure no overflow converting floats to chars
				if (mc != null && mc.DataType.equals("Varchar") && foundsc.DataType.equals("Float"))
				{
					if (DatabaseConnection.isDBTypeMySQL(dconn.DBType))
						pname = "CAST(" + pname + " AS CHAR(" + LoadWarehouse.FLOAT_WIDTH + "))";
					else if (DatabaseConnection.isDBTypeMemDB(dconn.DBType))
						pname = "CAST(" + pname + " AS VARCHAR)";
					else
						pname = "STR(" + pname + "," + FLOAT_WIDTH + ")";
				}
				pnames.add(pname);
				scols.add(foundsc);
				loadedSet.add(mc.ColumnName);
				cnames.add(mc.PhysicalName);
				if (r.getTimeDefinition() == null || foundsc.TargetTypes.size() != 1 || !foundsc.TargetTypes.get(0).equals(r.getTimeDefinition().Name))
					logicalnames.add(foundsc.Name);
				lookupTables.addAll(foundsc.getLookupTables(qm, dconn,r));
				lookupJoins.addAll(foundsc.getLookupJoins(st, "B", qm));
			}
		}
		/*
		 * Process any degenerate dimension column names
		 */
		for (DimensionTable dt : degenerateDimensions)
		{
			for (DimensionColumn dc : dt.DimensionColumns)
			{
				if (cnames.contains(dc.PhysicalName))
				{
					continue;
				}
				StagingColumn foundsc = null;
				for (StagingColumn sc : st.Columns)
				{
					if (sc.columnNameMatchesDimensionColumn(dc.ColumnName) && sc.TargetTypes != null)
					{
						if (sc.TargetTypes.contains(dt.DimensionName))
							foundsc = sc;
					}
				}
				if ((dc.Qualify) && (foundsc != null))
				{
					String pname = foundsc.getQualifiedPhysicalFormula(r, "B", qm);
					pname = r.replaceVariables(null, pname);
					if (!foundsc.NaturalKey || foundsc.TargetTypes.contains("Measure"))
						numNonKeys++;
					pnames.add(pname);
					scols.add(foundsc);
					loadedSet.add(dc.ColumnName);
					cnames.add(dc.PhysicalName);
					if (r.getTimeDefinition() == null || foundsc.TargetTypes.size() != 1 || !foundsc.TargetTypes.get(0).equals(r.getTimeDefinition().Name))
						logicalnames.add(foundsc.Name);
					/**
					 * A column with lookup expression can potentially be measure column as well dimension column on
					 * degenerate dimension so make sure that the lookup is included only once.
					 */
					List<String[]> degenLookupTables = foundsc.getLookupTables(qm, dconn,r);
					List<String> degenLookupJoins = foundsc.getLookupJoins(st, "B", qm);
					if (degenLookupTables != null && degenLookupTables.size() > 0)
					{
						for (int i = 0; i < degenLookupTables.size(); i++)
						{
							if (!lookupTables.contains(degenLookupTables.get(i)))
							{
								lookupTables.add(degenLookupTables.get(i));
								lookupJoins.add(degenLookupJoins.get(i));
							}
						}
					}
				}
			}
		}
		if (numNonKeys > 0)
		{
			numRows += processInsertPass(stmt, conn, mtname, cnames, mt, st, mtlist, grainSet, loadID, loadedSet, degenerateDimensions, scols,
					lookupTables, lookupJoins, pnames, numRows, insertFilter, createAsTempTableForInsertFilter, dropTempTableForInsertFilter,
					processingGroup, databasePath, logicalnames, s3credentials);
		} else
		{
			logger.info("No non-key columns found for table " + mt.TableName + " from staging table " + st.Name);
		}
		for (Level degenerateLevel : degenerateLevels)
		{
			for (DimensionTable dt : degenerateDimensions)
			{
				if (!noFactIndices)
				{
					lwHelper.generateIndices(dt, dt.TableSource.Tables[0].PhysicalName);
				}
				LoadWarehouseHelper.updateSurrogateKeysForDimensionTable(loadID, dt, degenerateLevel, stmt, loadedSet, lw, r);
			}
		}
		List<String> stNameList = factToStagingMap.get(mtname);
		if (stNameList==null)
		{
			stNameList = new ArrayList<String>();				
		}	
		stNameList.add(st.Name);
		factToStagingMap.put(mtname, stNameList);
		logger.debug("factToStagingMap list for "+ mtname + " " +stNameList);
		return numRows;
	}
	
	/**
	 * Go through update pass for this fact. For fact records that were orginally loaded from another staging table, we
	 * need to update those records with columns that may be coming from this staging table. Then we can insert new
	 * records from this staging table. This method updates the insert filter as well which is used by subsequent methods.
	 * @throws SQLException 
	 * 
	 */
	private long processUpdatePass(Statement stmt, Connection conn, String mtname, MeasureTable mt, StagingTable st, int loadID, Set<DimensionTable> degenerateDimensions,
			Set<String> loadedSet, Map<String, String> nkeys, QueryMap qm, long numRows, StringBuilder insertFilter, 
			StringBuilder createAsTempTableForInsertFilter, StringBuilder dropTempTableForInsertFilter, String processingGroup) throws SQLException
	{
		StringBuilder sb = new StringBuilder();
		StringBuilder oracleDupCheckQuery = new StringBuilder();
		StringBuilder oracleDupCheckGroupBy = new StringBuilder();
		boolean isOracle = DatabaseConnection.isDBTypeOracle(dconn.DBType);
		boolean isPADB = DatabaseConnection.isDBTypeParAccel(dconn.DBType);
		boolean isSAPHana = DatabaseConnection.isDBTypeSAPHana(dconn.DBType);
		sb.append("UPDATE " + Database.getQualifiedTableName(dconn, mtname));
		if (isOracle)
		{
			sb.append(" A");
		}
		StringBuilder set = new StringBuilder(" SET ");
		StringBuilder oracleSelect = new StringBuilder();
		StringBuilder oracleExists = new StringBuilder();
		if (isOracle)
		{
			set.append("(");
			oracleSelect.append(" = ( SELECT ");
			oracleExists.append(" AND EXISTS ( SELECT 1 FROM ");
		}
		boolean first = true;
		int numSet = 0;
		// Get column names
		Map<String, String> colNames = new HashMap<String, String>();
		Set<String> pcolNames = new HashSet<String>();
		Set<String> logicalnames = new HashSet<String>();
		for (MeasureColumn mc : mt.MeasureColumns)
		{
			if (mc.Qualify)
				colNames.put(mc.ColumnName, mc.DataType);
		}
		// Look for any degenerate column names
		for (DimensionTable dt : degenerateDimensions)
		{
			for (DimensionColumn dc : dt.DimensionColumns)
			{
				colNames.put(dc.ColumnName, dc.DataType);
			}
		}
		// Add any date IDs
		for (StagingColumn sc : st.Columns)
		{
			if (sc.DataType.startsWith("Date ID: ") && sc.NaturalKey && sc.TargetTypes != null && sc.TargetTypes.size() == 1 && sc.TargetTypes.contains("Time"))
			{
				colNames.put(sc.Name, "Integer");
			}
		}
		List<String> pnames = new ArrayList<String>();
		List<String> cnames = new ArrayList<String>();
		for (Entry<String, String> col : colNames.entrySet())
		{
			StagingColumn foundsc = null;
			for (StagingColumn sc : st.Columns)
			{
				if (sc.columnNameMatchesMeasure(col.getKey(), r) && sc.DataType.equals(col.getValue()) && sc.TargetTypes != null)
				{
					if (sc.TargetTypes.contains("Measure") || sc.NaturalKey)
						foundsc = sc;
				} else if (sc.Name.equals(col.getKey()) && sc.DataType.startsWith("Date ID: ") && sc.NaturalKey && sc.TargetTypes != null
						&& sc.TargetTypes.size() == 1 && sc.TargetTypes.contains("Time"))
					foundsc = sc;
			}
			if (foundsc != null)
			{
				String pname = foundsc.getQualifiedPhysicalFormula(r, "B", qm);
				pname = r.replaceVariables(null, pname);
				pnames.add(pname);
				MeasureColumn mc = null;
				DimensionColumn dc = null;
				for (MeasureColumn fmc : mt.MeasureColumns)
				{
					if (col.getKey().equals(fmc.ColumnName))
					{
						mc = fmc;
						break;
					}
				}
				/*
				 * Could not find the corresponding measure column from the measure table. Check to see if it is part a
				 * dimension column on one of the degenerate dimension tables.
				 */
				if (mc == null)
				{
					for (DimensionTable dt : degenerateDimensions)
					{
						for (DimensionColumn fdc : dt.DimensionColumns)
							if (col.getKey().equals(fdc.ColumnName) && foundsc.TargetTypes.contains(dt.DimensionName))
							{
								dc = fdc;
								break;
							}
					}
				}
				String cname = null;
				if (mc != null || dc != null)
					cname = mc != null ? mc.PhysicalName : dc.PhysicalName;
				else
				{
					if (foundsc.DataType.startsWith("Date ID: ") && foundsc.NaturalKey && foundsc.TargetTypes != null && foundsc.TargetTypes.size() == 1
							&& foundsc.TargetTypes.contains("Time"))
					{
						//For Oracle and IB, physical measure column name could be hashed so proceed differently.
						if(DatabaseConnection.isDBTypeOracle(dconn.DBType) || DatabaseConnection.isDBTypeMySQL(dconn.DBType))
						{
							String logicalColName = "Time."+foundsc.Name;
							MeasureColumn foundmc = mt.findColumn(logicalColName);
							if(foundmc != null)
							{
								cname = foundmc.PhysicalName;
								//Dont remove physical table name for IB since the method convertInfobrightDeletes() expects it.
								if(DatabaseConnection.isDBTypeOracle(dconn.DBType))
								{
									cname = cname.replace(foundmc.MeasureTable.PhysicalName + ".", "");
								}
							}
						}
						else
						{
							cname = "Time"+dconn.getPhysicalSuffix()+Util.replaceWithNewPhysicalString(foundsc.Name, dconn,r);
						}
					} else
					{
						String mcolname = foundsc.getMatchingMeasureColumnName(mt, r,false);
						if(mcolname != null)
						{
							MeasureColumn foundmc = mt.findColumn(mcolname);
							if(foundmc != null)
							{
								cname = foundmc.PhysicalName;
							}
							else
							{
								logger.warn("Column not found in measure table:" + mt.TableName + "." + mcolname);
								cname = Util.replaceWithNewPhysicalString(mcolname, dconn, r);
							}
						}
						else
						{
							logger.warn("Column not found in measure table for staging column:" + st.Name + "." + foundsc.Name);
							cname = Util.replaceWithNewPhysicalString(foundsc.Name, dconn, r);
						}
					}
				}
				cnames.add(cname);
				// Should not update join keys
				boolean found = false;
				for (String s : nkeys.keySet())
				{
					if ((mt.TableSource.Tables[0].PhysicalName + "." + s).equals(cname))
					{
						found = true;
						break;
					}
				}
				// If Infobright or MySQL, need to use alias
				if (dconn.DBType == DatabaseType.Infobright || dconn.DBType == DatabaseType.MYSQL || isOracle)
				{
					cname = Util.replaceStr(cname, mt.PhysicalName + ".", "A.");
				} else if (isPADB)// no need to qualify columns for target table for padb
				{
					cname = Util.replaceStr(cname, mt.PhysicalName + ".", "");
				}
				// Make sure no overflow converting floats to chars
				if (mc != null && mc.DataType.equals("Varchar") && foundsc.DataType.equals("Float"))
				{
					if (DatabaseConnection.isDBTypeMySQL(dconn.DBType) || isOracle)
						pname = "CAST(" + pname + " AS CHAR(" + LoadWarehouse.FLOAT_WIDTH + "))";
					else if (DatabaseConnection.isDBTypeMemDB(dconn.DBType))
						pname = "CAST(" + pname + " AS VARCHAR)";
					else
						pname = "STR(" + pname + "," + FLOAT_WIDTH + ")";
				}
				if (mc == null && dc != null && dc.DataType.equals("Varchar") && foundsc.DataType.equals("Float"))
				{
					if (DatabaseConnection.isDBTypeMySQL(dconn.DBType) || isOracle)
						pname = "CAST(" + pname + " AS CHAR(" + LoadWarehouse.FLOAT_WIDTH + "))";
					else if (DatabaseConnection.isDBTypeMemDB(dconn.DBType))
						pname = "CAST(" + pname + " AS VARCHAR)";
					else
						pname = "STR(" + pname + "," + FLOAT_WIDTH + ")";
				}
				if (!found && !pcolNames.contains(cname))
				{
					if (!first)
					{
						set.append(',');
						if (isOracle)
						{
							oracleSelect.append(",");
							oracleDupCheckQuery.append(",");
							oracleDupCheckGroupBy.append(",");
						}
					} else
						first = false;
					if (isOracle)
					{
						set.append(cname);
						oracleDupCheckQuery.append(cname);
						oracleSelect.append(pname);
						oracleDupCheckGroupBy.append(cname);
					} else
					{
						set.append(cname + "=" + pname);
					}
					numSet++;
					loadedSet.add(foundsc.Name);
					logicalnames.add(foundsc.Name);
					pcolNames.add(cname);
				}
			}
		}
		if (isOracle)
		{
			set.append(")");
			oracleDupCheckQuery.append(", ").append(" COUNT(*) CNT ").append(" FROM " ).append(Database.getQualifiedTableName(dconn, mtname)).append(" A ");
		}
		StagingTableBulkLoadInfo stbl = null;
		if (dconn.requiresTransactionID())
		{
			List<String> grainKeys = new ArrayList<String>();
			stbl = new StagingTableBulkLoadInfo(st, null, dconn, r);
			stbl.setupGrainKeys(r.findSourceFile(st.SourceFile));
			if (stbl.grainKeyMap.containsKey(mt))
			{
				int index = 0;
				for (MeasureTable gmt : stbl.grainKeyMap.keySet())
				{
					if (gmt == mt)
						break;
					index++;
				}
				if (index < stbl.grainKeyMap.size())
				{
					grainKeys.add(dconn.getHashedIdentifier(stbl.grainKeyNames.get(index)));
				}
			}
			if (grainKeys.size() > 0)
			{
				for (String s : grainKeys)
				{
					if (!first)
						set.append(',');
					else
						first = false;
					set.append("A." + s + "=B." + s);
				}
			}
		}
		if (!dconn.useWatcomOracleUpdateSyntax())
		{
			sb.append(set);
			if (!isOracle)
			{
				sb.append(" FROM " + Database.getQualifiedTableName(dconn, mtname));
			}
		}
		if (!isOracle)
		{
			if(isSAPHana)
			{
				sb.append(" INNER JOIN ");
			}
			else
			{
				sb.append(" A INNER JOIN ");
			}
		}
		if (DatabaseConnection.isDBTypeMemDB(dconn.DBType))
			sb.append("MAXONE ");
		if (isOracle)
		{
			oracleSelect.append(" FROM ");
			oracleDupCheckQuery.append(" INNER JOIN ");
			if (st.SourceType == StagingTable.SOURCE_QUERY && !st.PersistQuery)
			{
				st.updateStagingTableQuery(r);
				oracleSelect.append("(" + st.Query + ")");
				oracleExists.append("(" + st.Query + ")");
			} else
			{
				oracleSelect.append(Database.getQualifiedTableName(dconn, Util.replaceWithPhysicalString(st.Name)));
				oracleExists.append(Database.getQualifiedTableName(dconn, Util.replaceWithPhysicalString(st.Name)));
				oracleDupCheckQuery.append(Database.getQualifiedTableName(dconn, Util.replaceWithPhysicalString(st.Name)));
			}
			oracleSelect.append(" B WHERE ");
			oracleExists.append(" B WHERE ");
			oracleDupCheckQuery.append(" B ON ");
		} else
		{
			if (st.SourceType == StagingTable.SOURCE_QUERY && !st.PersistQuery)
			{
				st.updateStagingTableQuery(r);
				sb.append("(" + st.Query + ")");
			} else
				sb.append(Database.getQualifiedTableName(dconn, Util.replaceWithPhysicalString(st.Name)));
			sb.append(" B ON ");
		}
		StringBuilder keyfilter = new StringBuilder();
		boolean firstKey = true;
		String grainKeyName = null;
		if (dconn.requiresTransactionID())
		{
			if (stbl.grainKeyMap.containsKey(mt))
			{
				int index = 0;
				for (MeasureTable gmt : stbl.grainKeyMap.keySet())
				{
					if (gmt == mt)
						break;
					index++;
				}
				if (index < stbl.grainKeyMap.size())
				{
					String s = dconn.getHashedIdentifier(stbl.grainKeyNames.get(index));
					grainKeyName = s;
					keyfilter.append("A." + s + "=B." + s);
					firstKey = false;
				}
			}
		}
		StringBuilder keywherefilter = new StringBuilder();
		if (firstKey)
		{
			for (String s : nkeys.keySet())
			{
				if (firstKey)
					firstKey = false;
				else
				{
					keyfilter.append(" AND ");
					keywherefilter.append(" AND ");
				}
				keyfilter.append( (isSAPHana ?  Database.getQualifiedTableName(dconn, mtname) : "A") + "." + s + "=" + nkeys.get(s));
				keywherefilter.append((isSAPHana ?  Database.getQualifiedTableName(dconn, mtname) : "A") + "." + s + " IS NULL");
			}
		}
		keyfilter = new StringBuilder(r.replaceVariables(null, keyfilter.toString()));
		if (isOracle)
		{
			oracleSelect.append(keyfilter);
			oracleSelect.append(")");
			oracleExists.append(keyfilter);
			oracleExists.append(")");
			sb.append(oracleSelect);
			oracleDupCheckQuery.append(keyfilter);
		} else
		{
			sb.append(keyfilter);
		}
		boolean skipUpdate = false;
		if(isOracle && r.getCurrentBuilderVersion()>=20)
		{
			String query = "SELECT COUNT(*) CNT FROM ( SELECT " +oracleDupCheckQuery.toString() + " GROUP BY " + oracleDupCheckGroupBy + " HAVING COUNT(*) > 1 )";
			long dupRecordCount = executeQueryAndReturnCount(stmt,query);
			if(dupRecordCount!=0)
			{
				logger.error("Cannot update fact table " + mt.TableName  +" from " + st.Name + " as the grain is not unique.");
				skipUpdate = true;
			}
		}
		
		if (dconn.useWatcomOracleUpdateSyntax())
			sb.append(set);
		String loadIDFilter = " WHERE " + (isSAPHana ?  Database.getQualifiedTableName(dconn, mtname) : "A") + ".LOAD_ID=" + loadID;
		sb.append(loadIDFilter);
		if (isOracle)
		{
			sb.append(oracleExists);
		}
		if (isPADB)
		{
			String whereAppend = Util.replaceStr(keyfilter.toString(), "A.", Database.getQualifiedTableName(dconn, mtname) + ".");
			if (whereAppend.length() > 0)
				sb.append(" AND ").append(whereAppend);
		}
		if (DatabaseConnection.isDBTypeInfoBright(dconn.DBType))
		{
			convertInfobrightDeletes(stmt, conn, mtname, mt, st, cnames, pnames, loadID, keyfilter, grainKeyName);
			numSet = 0;
		}
		if ((numSet > 0) && (!skipUpdate))
		{
			logger.info("Updating records in table " + mt.TableName + " from staging table " + st.Name);
			logger.debug(Query.getPrettyQuery(sb));
			long id = System.currentTimeMillis();
			logger.info("[QUERY:" + id + ":" + Query.getPrettyQuery(sb.toString()) + "]");
			int rows = stmt.executeUpdate(sb.toString());
			long finish = System.currentTimeMillis();
			logger.info("[UPDATEF:" + id + ":" + loadID + ":" + mt.TableName + ":" + st.Name + ":" + rows + ":" + logicalnames + ":" + (finish - id) + ":"
					+ processingGroup + "] " + rows + " rows updated");
			numRows += rows;
		}
		if (DatabaseConnection.isDBTypeInfoBright(dconn.DBType) && grainKeyName != null)
		{
			// Add a filter to only insert records not in other staging tables - for IB, do a left outer join with a temp table that
			// just has keys. NOT IN or self-joins just don't work with large volumes of data in IB.
			String tempKeysTableName = Database.getQualifiedTableName(dconn, "tempKeys");
			String fullmtName = Database.getQualifiedTableName(dconn, mtname);

			dropTempTableForInsertFilter.append("DROP TABLE IF EXISTS " + tempKeysTableName);
			createAsTempTableForInsertFilter.append("SELECT DISTINCT " + grainKeyName + " FROM " + fullmtName 
					+ " WHERE LOAD_ID=" + loadID + " AND " + grainKeyName + " IS NOT NULL");
			
			insertFilter.append(" LEFT OUTER JOIN " + tempKeysTableName + " ON " + "B." + grainKeyName 
					+ "=" + tempKeysTableName + "." + grainKeyName);
			insertFilter.append(" WHERE " + tempKeysTableName + "." + grainKeyName + " IS NULL");
		} else if (DatabaseConnection.isDBTypeMemDB(dconn.DBType))
		{
			// Add a filter to only insert records not in other staging tables
			insertFilter.append(" LEFT OUTER JOIN MAXONE " + Database.getQualifiedTableName(dconn, mtname) + " A ON ");
			insertFilter.append(keyfilter + " AND A.LOAD_ID=" + loadID + " WHERE " + keywherefilter);
		} else if (isPADB)
		{
			// Add a filter to only insert records not in other staging tables
			insertFilter.append(" LEFT OUTER JOIN " + Database.getQualifiedTableName(dconn, mtname) + " A ON ");
			insertFilter.append(keyfilter + " AND A.LOAD_ID=" + loadID + " WHERE " + keywherefilter);
		} else
		{
			// Add a filter to only insert records not in other staging tables
			//Since we are using the same keyfilter in insert and update facts, using qualified tablenames instead of alias
			insertFilter.append(" WHERE NOT EXISTS (SELECT * FROM " + Database.getQualifiedTableName(dconn, mtname) + (isSAPHana ? "" : " A" ) +"  WHERE " );
			insertFilter.append(keyfilter);
			String insertLoadIDFilter = " AND "  + (isSAPHana ?  Database.getQualifiedTableName(dconn, mtname) : "A") + ".LOAD_ID=" + loadID + " )";
			insertFilter.append(insertLoadIDFilter);
		}
		return numRows;
	}
	
	/**
	 * If there are non-key columns, then go ahead and load the fact
	 * 
	 * @param stmt
	 * @param conn
	 * @param mtname
	 * @param cnames
	 * @param mt
	 * @param st
	 * @param sb
	 * @param mtlist
	 * @param grainSet
	 * @param loadID
	 * @param loadedSet
	 * @param degenerateDimensions
	 * @param scols
	 * @param lookupTables
	 * @param lookupJoins
	 * @param pnames
	 * @param numRows
	 * @param insertFilter
	 * @param processingGroup
	 * @param databasePath
	 * @param logicalnames
	 * @return
	 * @throws Exception 
	 */
	private long processInsertPass(Statement stmt, Connection conn, String mtname, List<String> cnames, MeasureTable mt, StagingTable st,
			List<MeasureTable> mtlist, Collection<MeasureTableGrain> grainSet, int loadID, Set<String> loadedSet,
			Set<DimensionTable> degenerateDimensions, List<StagingColumn> scols, List<String[]> lookupTables, List<String> lookupJoins, List<String> pnames,
			long numRows, StringBuilder insertFilter, StringBuilder createAsTempTableForInsertFilter, StringBuilder dropTempTableForInsertFilter, 
			String processingGroup, String databasePath, Set<String> logicalnames, AWSCredentials s3credentials) throws Exception
	{					
		StringBuilder sb = new StringBuilder();
		/*
		 * Get any added foreign keys (i.e. keys that are not explicitly part of the grain hierarchy - for example, date
		 * keys, or keys inherited from a higher level grain fact table). Sort from the lowest grain up.
		 */
		List<CompareMeasureTableMatches> matchList = new ArrayList<CompareMeasureTableMatches>();
		for (int i = mtlist.size() - 1; i >= 0; i--)
		{
			MeasureTable mt2 = mtlist.get(i);
			boolean isAtMatching = mt2.isAtMatchingGrain(r, grainSet);
			Collection<MeasureTableGrain> higherGrain = mt2.isAtHigherGrain(r, grainSet);
			if (!isAtMatching && higherGrain != null)
			{
				CompareMeasureTableMatches cm = new CompareMeasureTableMatches(mtlist);
				cm.grainCount = higherGrain.size();
				cm.higherGrain = higherGrain;
				cm.mt = mt2;
				matchList.add(cm);
			}
		}
		Collections.sort(matchList);
		List<ForeignKeyUpdate> fkulist = new ArrayList<ForeignKeyUpdate>();
		List<CompareMeasureTableMatches> fkeyMatches = new ArrayList<CompareMeasureTableMatches>();
		Map<Level, SurrogateKeyUpdate> mappedLevels = null;
		boolean repeat = false;
		Set<String> loadedBeforeForeignKeyUpdates = new HashSet<String>(loadedSet);
		Set<String> surrogateKeys = null;
		Set<MeasureColumn> requiredKeySet = new HashSet<MeasureColumn>();
		do
		{
			repeat = false;
			Set<MeasureTable> foreignKeyMeasureTables = new HashSet<MeasureTable>();
			for (CompareMeasureTableMatches cm : matchList)
			{
				if (fkeyMatches.contains(cm))
					continue;
				Set<String> fkLoadedSet = new HashSet<String>(loadedBeforeForeignKeyUpdates);
				ForeignKeyUpdate fku = ForeignKeyUpdate.getForeignKeyUpdate(stmt, r, dconn, mt, st, cm.mt, cm.higherGrain, grainSet, fkLoadedSet,
						foreignKeyMeasureTables, loadID, requiredKeySet, fkulist);
				if (fku != null)
				{
					// Coalesce columns if necessary
					for (int i = 0; i < fku.logicalNames.size(); i++)
					{
						boolean found = false;
						for (ForeignKeyUpdate fku2 : fkulist)
						{
							for (int i2 = 0; i2 < fku2.logicalNames.size(); i2++)
							{
								if (fku.logicalNames.get(i).equals(fku2.logicalNames.get(i2)))
								{
									/*
									 * If two foreign key updates are occurring for the same logical column, use a
									 * coalesce to bring them together
									 */
									fku2.sourceColumnNames.set(i2, "COALESCE(" + fku2.sourceColumnNames.get(i2) + "," + fku.sourceColumnNames.get(i) + ")");
									fku.columnNames.remove(i);
									fku.sourceColumnNames.remove(i);
									fku.logicalNames.remove(i);
									fku.hasCoalescedColumns = true;
									i--;
									found = true;
									break;
								}
							}
							if (found)
								break;
						}
					}
					fkulist.add(fku);
					fkeyMatches.add(cm);
				}
			}
			/*
			 * First, find all the columns that would otherwise be retrieved through surrogate key lookups
			 */
			Set<String> baseSurrogateKeys = new HashSet<String>(loadedSet);
			getSurrogateKeyUpdatesForMeasureTable(loadID, mt, stmt, baseSurrogateKeys, st, degenerateDimensions);
			Set<String> availableForSurrogateKeys = new HashSet<String>(loadedSet);
			/*
			 * Now, include all foreign key columns that wouldn't have been added anyway in the list of columns that
			 * have been loaded - adding these columns to the list may allow other surrogate keys to get looked up (ones
			 * that need to because foreign keys brought them down)
			 */
			for (ForeignKeyUpdate fku : fkulist)
			{
				for (String s : fku.logicalNames)
				{
					if (!baseSurrogateKeys.contains(s))
						availableForSurrogateKeys.add(s);
				}
			}
			Set<String> beforeSurrogateKeys = new HashSet<String>(availableForSurrogateKeys);
			/*
			 * Now, get the real list of surrogate key lookups
			 */
			Map<Level, SurrogateKeyUpdate> newLevels = getSurrogateKeyUpdatesForMeasureTable(loadID, mt, stmt, availableForSurrogateKeys, st,
					degenerateDimensions);
			availableForSurrogateKeys.removeAll(beforeSurrogateKeys);
			surrogateKeys = availableForSurrogateKeys;
			
			/** Prune any columns that are already being loaded. We should not prune columns being updated as surrogate
			    key here. Keeping surrogate keys for the time being is necessary to support degenerate dimension case and 
			    pruning happens later anyway while trying to consolidate SKU and FKU. Look for comment:
			    "Remove any foreign key updates that are also updated by surrogate key updates" to see where it is being 
			    done later.
			 **/
			
			for (ForeignKeyUpdate fku : fkulist)
			{
				List<String> removeColumnNamesList = new ArrayList<String>();
				List<String> removeLogicalNamesList = new ArrayList<String>();
				List<String> removeSourceColumnNamesList = new ArrayList<String>();
				for (int i = 0; i < fku.logicalNames.size(); i++)
				{
					if (loadedSet.contains(fku.logicalNames.get(i)))
					{
						removeColumnNamesList.add(fku.columnNames.get(i));
						removeLogicalNamesList.add(fku.logicalNames.get(i));
						removeSourceColumnNamesList.add(fku.sourceColumnNames.get(i));
					}
				}
				if(removeColumnNamesList.size() > 0)
				{
					fku.columnNames.removeAll(removeColumnNamesList);
				}
				if(removeLogicalNamesList.size() > 0)
				{
					fku.logicalNames.removeAll(removeLogicalNamesList);
				}
				if(removeSourceColumnNamesList.size() > 0)
				{
					fku.sourceColumnNames.removeAll(removeSourceColumnNamesList);
				}
			}
			if (mappedLevels == null || mappedLevels.size() != newLevels.size())
			{
				mappedLevels = newLevels;
				repeat = true;
			}
		} while (repeat);
		/*
		 * Update the list of columns that are being loaded from each of the two lookup sources
		 */
		for (ForeignKeyUpdate fku : fkulist)
		{
			for (String s : fku.logicalNames)
				loadedSet.add(s);
		}
		if (surrogateKeys != null)
			for (String s : surrogateKeys)
				loadedSet.add(s);
		
		
		sb.append("INSERT INTO " + Database.getQualifiedTableName(dconn, mtname) + " ( ");
		
		boolean firstT = true;
		for (String colName : cnames)
		{
			if (firstT)
			{
				firstT = false;
			} else
			{
				sb.append(",");
			}
			if (dconn.stripTableNameFromInsertColumnList())
				sb.append(Util.replaceStr(colName, mtname + ".", ""));
			else
				sb.append(colName);
		}
		/*
		 * Only update surrogate keys where the natural key is in the insert
		 */
		List<Level> orderedLevels = new ArrayList<Level>();
		List<SurrogateKeyUpdate> validUpdateSet = new ArrayList<SurrogateKeyUpdate>();
		/*
		 * Order updates by level (so that lower levels are used first - allows joining to higher levels as snowflakes
		 */
		for (Level l : mappedLevels.keySet())
		{
			boolean end = true;
			for (Level l2 : orderedLevels)
			{
				if (l2.Hierarchy.DimensionName.equals(l.Hierarchy.DimensionName))
				{
					if (l2.findLevel(l) != null)
						end = false;
					break;
				}
			}
			if (end)
				orderedLevels.add(l);
			else
				orderedLevels.add(0, l);
		}
		// Consolidate levels if possible
		for (int i = 0; i < orderedLevels.size(); i++)
		{
			for (int j = i + 1; j < orderedLevels.size(); j++)
			{
				Level l1 = orderedLevels.get(i);
				Level l2 = orderedLevels.get(j);
				if (l2.Hierarchy.DimensionName.equals(l1.Hierarchy.DimensionName))
				{
					SurrogateKeyUpdate sku1 = mappedLevels.get(l1);
					SurrogateKeyUpdate sku2 = mappedLevels.get(l2);
					if (sku2.leveldt == null || sku1.leveldt == null)
						continue;
					for (int k = 0; k < sku2.insertColumns.size(); k++)
					{
						String cname = sku2.insertSourceColumns.get(k);
						DimensionColumn dc2 = null;
						for (DimensionColumn sdc : sku1.leveldt.DimensionColumns)
						{
							if (!sdc.AutoGenerated || !sdc.Qualify)
								continue;
							int index1 = sdc.PhysicalName.indexOf('.');
							int index2 = cname.indexOf('.');
							if (index1 >= 0 && index2 >= 0 && sdc.PhysicalName.substring(index1).equals(cname.substring(index2)))
							{
								dc2 = sdc;
								break;
							}
						}
						if (dc2 != null)
						{
							sku1.insertColumns.add(sku2.insertColumns.get(k));
							sku1.insertSourceColumns.add(dc2.PhysicalName);
							sku2.insertColumns.remove(k);
							sku2.insertSourceColumns.remove(k);
						}
					}
					/*
					 * Snowflake the join if possible (and the second source cannot be fully eliminated) - replace
					 * reference to target fact table with lower level dimension column
					 */
					if (sku2.insertColumns.size() > 0)
					{
						for (DimensionColumn sdc : sku1.leveldt.DimensionColumns)
						{
							if (!sdc.AutoGenerated || !sdc.Qualify)
								continue;
							int index1 = sdc.PhysicalName.indexOf('.');
							if (index1 < 0)
								continue;
							String cname = Util.replaceWithNewPhysicalString(sku1.leveldt.DimensionName, dconn,r) + sdc.PhysicalName.substring(index1 + 1);
							sku2.joinCondition = Util.replaceStr(sku2.joinCondition, mt.PhysicalName + "." + cname, sku1.leveldt.PhysicalName + "."
									+ sdc.PhysicalName.substring(index1 + 1));
							int nkindex = sku2.naturalKeyColumns.indexOf(mt.PhysicalName + "." + cname);
							if (nkindex >= 0)
								sku2.naturalKeyColumns.set(nkindex, sku1.leveldt.PhysicalName + "." + sdc.PhysicalName.substring(index1 + 1));
						}
					}
				}
			}
		}
		// Surrogate key updates
		for (Level l : orderedLevels)
		{
			SurrogateKeyUpdate sku = mappedLevels.get(l);
			boolean valid = true;
			if (sku.naturalKeyColumns == null || sku.insertColumns.isEmpty())
				continue;
			for (String keyCol : sku.naturalKeyColumns)
			{
				boolean colFound = false;
				/*
				 * Make sure that the key columns used are being loaded
				 */
				for (String colName : cnames)
				{
					if (keyCol.equals(colName))
					{
						colFound = true;
						break;
					}
				}
				// If not found, see if it's an added foreign key
				if (!colFound)
				{
					for (ForeignKeyUpdate fku : fkulist)
					{
						if(fku.isMeasureTableContainsColumns(r, sku.naturalKeyColumns))
						{
							for (String c : fku.columnNames)
							{
								if (keyCol.equals(mt.PhysicalName + "." + c))
								{
									colFound = true;
									fku.requiredForSurrogateKeyUpdate = true;
									sku.joinCondition = Util.replaceStr(sku.joinCondition, mt.PhysicalName + ".", fku.tableName + ".");
									break;
								}
							}
							if (colFound)
								break;
						}
					}
				}
				if (!colFound)
				{
					valid = false;
					break;
				}
			}
			if (valid)
			{
				boolean match = true;
				// Make sure datatypes match
				for (DimensionColumn dc : sku.matchingDimensionColumns)
				{
					for (StagingColumn sc : scols)
					{
						if (sc.columnNameMatchesDimensionColumn(dc.ColumnName))
						{
							if (!sc.DataType.equals(dc.DataType))
							{
								if (!((sc.DataType.equals("Integer") && dc.DataType.equals("Float"))
										|| (dc.DataType.equals("Integer") && sc.DataType.equals("Float")) || (sc.DataType.startsWith("Date ID:") && dc.DataType
										.equals("Integer"))))
								{
									match = false;
									logger.warn("Key datatype mismatch: staging column [" + sc.Name + "] (" + sc.DataType + ") in table " + st.Name
											+ " cannot be joined to dimension column [" + dc.ColumnName + "] (" + dc.DataType + ") on table "
											+ dc.DimensionTable.TableName + " - cannot update keys " + sku.insertSourceColumns);
								}
							}
						}
					}
				}
				if (match)
				{
					validUpdateSet.add(sku);
				}
			}
		}
		List<ForeignKeyUpdate> fkuRemove = new ArrayList<ForeignKeyUpdate>();
		// Remove any foreign key updates that are also updated by surrogate key updates
		for (ForeignKeyUpdate fku : fkulist)
		{
			List<String> toRemove = new ArrayList<String>();
			for (String s : fku.columnNames)
			{
				boolean found = false;
				for (SurrogateKeyUpdate sku : validUpdateSet)
				{
					for (String s2 : sku.insertColumns)
					{
						if (s.equals(s2))
						{
							found = true;
							break;
						}
					}
					if (found)
						break;
				}
				if (found)
					toRemove.add(s);
			}
			for (String s : toRemove)
			{
				int index = fku.columnNames.indexOf(s);
				if (index >= 0)
				{
					fku.logicalNames.remove(index);
					fku.columnNames.remove(index);
					fku.sourceColumnNames.remove(index);
				}
			}
			if (fku.columnNames.isEmpty() && !fku.hasCoalescedColumns && !fku.requiredForSurrogateKeyUpdate)
				fkuRemove.add(fku);
		}
		// Only remove tables that aren't joined through by other Foreign Key Updates
		for (ForeignKeyUpdate fku : fkuRemove)
		{
			boolean found = false;
			for (ForeignKeyUpdate fku2 : fkulist)
			{
				if (fku2.dependencyTables.contains(fku.tableName))
				{
					found = true;
					break;
				}
			}
			if (!found)
				fkulist.remove(fku);
		}
		/**
		 * Remove any surrogate key updates that use the same underlying physical tables as one of the foreign key
		 * updates. This scenario occurs for degenerate dimensions - foreign key update uses measure version of the
		 * physical table vs. surrogate key update uses dimension version of the same physical table. In these cases, if
		 * we let both updates as they are, it results in SQL error saying "Correlation name exposed in SQL query
		 * twice". So, remove surrogate key update and transfer the columns to the corresponding foreign key update.
		 */
		List<SurrogateKeyUpdate> skuToRemove = new ArrayList<SurrogateKeyUpdate>();
		for (SurrogateKeyUpdate sku : validUpdateSet)
		{
			String schemaQual = sku.leveldt.TableSource.Schema != null ? sku.leveldt.TableSource.Schema : dconn.Schema != null ? dconn.Schema : null;
			schemaQual = schemaQual + ".";
			for (ForeignKeyUpdate fku : fkulist)
			{
				String fkuPhysicalTableName = fku.tableName;
				String skuPhysicalTableName = sku.leveldt.TableSource.Tables[0].PhysicalName;
				if (fkuPhysicalTableName != null && skuPhysicalTableName != null && fkuPhysicalTableName.equals(skuPhysicalTableName))
				{
					for (int i = 0; i < sku.insertColumns.size(); i++)
					{
						String skColName = sku.insertColumns.get(i);
						// Make sure there are no dupes
						if (!fku.columnNames.contains(skColName))
						{
							fku.columnNames.add(skColName);
							String skSourceColName = sku.insertSourceColumns.get(i);
							// Foreign key source columns have no schema qualifier since it uses alias where as
							// surrogate key source columns do - take care of this while transferring from SKU to
							// FKU
							int indexSchemaQual = skSourceColName.indexOf(schemaQual);
							if (indexSchemaQual >= 0)
							{
								skSourceColName = Util.replaceStr(skSourceColName, schemaQual, "");
							}
							fku.sourceColumnNames.add(skSourceColName);
						}
					}
					skuToRemove.add(sku);
					break;
				}
			}
		}
		if (skuToRemove.size() > 0)
		{
			validUpdateSet.removeAll(skuToRemove);
		}
		for (SurrogateKeyUpdate sku : validUpdateSet)
		{
			for (String isc : sku.insertColumns)
			{
				String skname = mtname + '.' + isc;
				if (firstT)
				{
					firstT = false;
				} else
				{
					sb.append(",");
				}
				if (dconn.stripTableNameFromInsertColumnList())
					sb.append(Util.replaceStr(skname, mtname + ".", ""));
				else
					sb.append(skname);
			}
		}
		// Updated added foreign keys
		for (ForeignKeyUpdate fku : fkulist)
		{
			for (String s : fku.columnNames)
			{
				if (firstT)
					firstT = false;
				else
					sb.append(",");
				if (dconn.stripTableNameFromInsertColumnList())
					sb.append(Util.replaceStr(s, mtname + ".", ""));
				else
					sb.append(s);
			}
		}
		sb.append(", LOAD_ID");
		List<String> grainKeys = new ArrayList<String>();
		Set<String> grainKeyLogicalNames = new HashSet<String>();
		if (dconn.requiresTransactionID())
		{
			for (StagingTable st2 : r.getStagingTables())
			{
				if (st2.isAtMatchingGrain(grainSet))
				{
					SourceFile sf2 = r.findSourceFile(st2.SourceFile);
					StagingTableBulkLoadInfo stbl = new StagingTableBulkLoadInfo(st2, sf2, mt.TableSource.connection, r);
					stbl.setupGrainKeys(sf2);
					if (stbl.grainKeyMap.containsKey(mt))
					{
						int index = 0;
						for (MeasureTable gmt : stbl.grainKeyMap.keySet())
						{
							if (gmt == mt)
								break;
							index++;
						}
						if (index < stbl.grainKeyMap.size() && index < stbl.grainKeyNames.size())
						{
							String s = dconn.getHashedIdentifier(stbl.grainKeyNames.get(index));
							String sphysical = "B." + s;
							boolean stagingColumnExists = false;
							if(st != st2)
							{
								//Make sure column exists on current staging table
								if(st.findStagingColumn(s) != null)
								{
									sphysical = "B." + Util.replaceWithNewPhysicalString(stbl.grainKeyNames.get(index), dconn,r);
									stagingColumnExists = true;
								}
							}
							else
							{
								stagingColumnExists = true;
							}
							//Check for dupes otherwise physical SQL will fail.
							if(stagingColumnExists && (!grainKeys.contains(sphysical)) && (!grainKeyLogicalNames.contains(s)))
							{
								if (firstT)
									firstT = false;
								else
									sb.append(",");
								sb.append(s);
								grainKeys.add(sphysical);
								grainKeyLogicalNames.add(s);
							}
						}
					}
				}
			}
		}
		String insertColumns = sb.toString().substring(sb.toString().indexOf("(") + 1);
		sb.append(" ) ");
		sb.append(" SELECT ");
		firstT = true;
		for (String pname : pnames)
		{
			if (firstT)
			{
				firstT = false;
			} else
			{
				sb.append(",");
			}
			sb.append(pname);
		}
		// Surrogate key updates
		for (SurrogateKeyUpdate sku : validUpdateSet)
		{
			for (String isc : sku.insertSourceColumns)
			{
				if(sku.degenerateInlineJoin)
				{
					isc = Util.replaceStr(isc,dconn.Schema+"."+ sku.leveldt.TableSource.Tables[0].PhysicalName, sku.leveldt.TableSource.Tables[0].PhysicalName);
				}
				if (firstT)
				{
					firstT = false;
				} else
				{
					sb.append(",");
				}
				if (dconn.DBType == DatabaseType.MonetDB)
				{
					isc = Util.replaceStr(isc, dconn.Schema + ".", "");
					isc = Util.replaceStr(isc, "dbo.", "");
				}
				sb.append(isc);
			}
		}
		// Added foreign key updates
		for (ForeignKeyUpdate fku : fkulist)
		{
			for (String s : fku.sourceColumnNames)
			{
				if (firstT)
					firstT = false;
				else
					sb.append(",");
				if (dconn.DBType == DatabaseType.MonetDB)
				{
					s = Util.replaceStr(s, dconn.Schema + ".", "");
					s = Util.replaceStr(s, "dbo.", "");
				}
				sb.append(s);
			}
		}
		logger.info("Inserting new records into table " + mt.TableName + " from staging table " + st.Name);
		sb.append("," + loadID);
		if (grainKeys.size() > 0)
		{
			for (String s : grainKeys)
			{
				if (firstT)
					firstT = false;
				else
					sb.append(",");
				sb.append(s);
			}
		}
		sb.append(" FROM ");
		if (st.SourceType == StagingTable.SOURCE_QUERY && !st.PersistQuery)
		{
			st.updateStagingTableQuery(r);
			sb.append("(" + st.Query + ")");
		} else
			sb.append(Database.getQualifiedTableName(dconn, Util.replaceWithPhysicalString(st.Name)));
		sb.append(" B");
		// Foreign key update joins
		for (ForeignKeyUpdate fku : fkulist)
		{
			String joinClause = fku.joinClause.toString();
			sb.append(joinClause);
		}
		if (lookupTables != null && lookupTables.size() > 0)
		{
			for (int i = 0; i < lookupTables.size(); i++)
			{
				sb.append(" LEFT OUTER JOIN " + (DatabaseConnection.isDBTypeMemDB(dconn.DBType) ? "MAXONE " : "")
						+ Database.getQualifiedTableName(dconn, lookupTables.get(i)[0]) + " " + lookupTables.get(i)[1] + " ON " + lookupJoins.get(i));
			}
		}
		// Surrogate key joins
		for (SurrogateKeyUpdate sku : validUpdateSet)
		{
			String joinClause = sku.joinCondition;
			// Substitute for any inserted data
			for (int i = 0; i < pnames.size(); i++)
			{
				joinClause = Util.replaceStr(joinClause, cnames.get(i), pnames.get(i));
			}
			for (ForeignKeyUpdate fku : fkulist)
				for (int i = 0; i < fku.columnNames.size(); i++)
				{
					joinClause = Util.replaceStr(joinClause, mt.PhysicalName + "." + fku.columnNames.get(i), fku.tableName + "." + fku.columnNames.get(i));
				}
			if (dconn.DBType == DatabaseType.MonetDB)
			{
				joinClause = Util.replaceStr(joinClause, dconn.Schema + ".", "");
				joinClause = Util.replaceStr(joinClause, "dbo.", "");
			}
			if(!sku.degenerateInlineJoin ) 
			{
				sb.append(sku.joinTable + " " + joinClause);
			}
			else
			{
				sb.append(" LEFT OUTER JOIN " + joinClause);
			}
		}
		sb.append(insertFilter);
		
		if((createAsTempTableForInsertFilter != null) && (createAsTempTableForInsertFilter.length() > 0) 
				&& (dropTempTableForInsertFilter != null) && (dropTempTableForInsertFilter.length() > 0))
		{
			Database.createTableAs(r, dconn, "tempKeys", createAsTempTableForInsertFilter.toString(), true);
		}
		/*
		 * Drop indexes if it's a big insert
		 */
		List<DatabaseIndex> dilist = null;
		if (st.Size > DROP_INDEX_LIMIT)
		{
			dilist = Database.getIndices(dconn, stmt, dconn.Schema, mtname);
			for (DatabaseIndex di : dilist)
			{
				logger.info("Dropping index " + di.name + " for insert performance");
				Database.dropIndex(dconn, dconn.ConnectionPool.getConnection(), di.name, Database.getQualifiedTableName(dconn, mtname));
			}
		}
		long id = System.currentTimeMillis();
		logger.info("[QUERY:" + id + ":" + Query.getPrettyQuery(sb) + "]");
		int rows = 0;
		if (dconn.requiresPersistQueryToFileAndLoadIntoTable(r))
		{
			String sbSelect = sb.toString().substring(sb.toString().indexOf("SELECT"));
			rows = Database.persistQueryToFileAndLoadIntoTable(sbSelect, mtname, insertColumns, databasePath, false, dconn, stmt, s3credentials,null,null,r);
		} else
		{
			rows = stmt.executeUpdate(sb.toString());
		}
		if (!conn.getAutoCommit())
			conn.commit();
		long finish = System.currentTimeMillis();
		logger.info("[INSERTF:" + id + ":" + loadID + ":" + mt.TableName + ":" + st.Name + ":" + rows + ":" + logicalnames + ":" + (finish - id) + ":"
				+ processingGroup + "] " + rows + " rows inserted");
		numRows += rows;
		Database.setIdentityColumns(dconn, stmt, mtname);

		if((dropTempTableForInsertFilter != null) && (dropTempTableForInsertFilter.length() > 0))
		{
			logger.info(Query.getPrettyQuery(dropTempTableForInsertFilter.toString()));
			stmt.executeUpdate(dropTempTableForInsertFilter.toString());
		}
		
		if(degenerateDimensions != null && degenerateDimensions.size() > 0)
		{
			updateSurrogateFactKeysForDegenerateDimensions(degenerateDimensions, mt, loadID, stmt, conn, processingGroup);
		}
		
		if (dilist != null)
		{
			for (DatabaseIndex di : dilist)
			{
				logger.info("Recreating index " + di.name + " for insert performance");
				Database.createIndex(dconn, stmt, di);
			}
		}
		
		return numRows;
	}
	
	protected long executeQueryAndReturnCount(Statement stmt,String qry) throws SQLException
	{
	  long rowCount = 0;
	  ResultSet rs  = null;
		try {
			long id = System.currentTimeMillis();
			logger.info("[COUNTQUERY:" + id + ":" + Query.getPrettyQuery(qry) + "]");
			rs = stmt.executeQuery(qry);
			if (rs.next()) {
				rowCount = rs.getLong(1);
			}
			logger.info("[COUNT:" + id + ":" + rowCount + "]");
		} catch (Exception e) {
			logger.error("Exception while executing query ", e);
			throw e;
		}finally{
			try{
				if(rs!=null)
				{
					rs.close();
					rs= null;
				}
			}catch(Exception e)
			{
			}
		}
		return rowCount;
	}
	


	/**
	 * Inrecemental snapshot fact processing - delete the rows loaded from previous loads matching natural keys in a
	 * self-join
	 * 
	 * @param mt
	 *            Measure table
	 * @param mtKeys
	 *            Measure table keys used for loading from staging table
	 * @param stmt
	 *            Statement
	 * @param loadID
	 *            Load ID
	 * @param st
	 *            Staging table that triggered this incremental snapshot fact processing
	 * @param logicalTableKeys
	 *            Logical names of snapshot delete keys used
	 * @throws SQLException
	 */
	protected void processForIncrementalSnapshotFact(Connection conn, MeasureTable mt, Set<String> mtKeys, Statement stmt, int loadID, StagingTable st,
			Set<String> logicalTableKeys, String processingGroup) throws SQLException
	{
		boolean isOracle = DatabaseConnection.isDBTypeOracle(dconn.DBType);
		boolean isPADB = DatabaseConnection.isDBTypeParAccel(dconn.DBType);
		String qualifiedMeasureTableName = Database.getQualifiedTableName(dconn, mt.TableSource.Tables[0].PhysicalName);
		StringBuilder deleteBuf = new StringBuilder();
		if (isOracle || isPADB)
		{
			deleteBuf.append("DELETE " + " FROM " + qualifiedMeasureTableName);
			deleteBuf.append(" WHERE EXISTS ( SELECT 1 FROM " + qualifiedMeasureTableName + " B WHERE ");
		}
		else
		{
			deleteBuf.append("DELETE " + qualifiedMeasureTableName + " FROM " + qualifiedMeasureTableName);
			deleteBuf.append(" INNER JOIN " + qualifiedMeasureTableName + " B ON ");
		}
		boolean firstKey = true;
		StringBuilder keyFilter = new StringBuilder();
		for (String mtk : mtKeys)
		{
			if (firstKey)
			{
				firstKey = false;
			} else
			{
				keyFilter.append(" AND ");
			}
			if (mtk.contains("."))
			{
				mtk = mtk.substring(mtk.lastIndexOf(".") + 1);
			}
			keyFilter.append(qualifiedMeasureTableName + "." + mtk + "=B." + mtk);
		}
		deleteBuf.append(r.replaceVariables(null, keyFilter.toString()));
		deleteBuf.append(" AND " + qualifiedMeasureTableName + ".LOAD_ID < " + loadID);
		deleteBuf.append(" AND " + "B.LOAD_ID = " + loadID);
		if (isOracle || isPADB)
		{
			deleteBuf.append(")");
		}
	
		boolean areOptimizingIndicesCreated = false;
		
		LevelKey levelKey = new LevelKey();
		List<String> levelKeyColumnsList = new ArrayList<String>();
		levelKeyColumnsList.addAll(logicalTableKeys);
		
		Collections.sort(levelKeyColumnsList);
		
		levelKey.ColumnNames = levelKeyColumnsList.toArray(new String[levelKeyColumnsList.size()]);
		
		if (dconn.supportsIndexes()) {
			boolean isIndexPresent = Database.indexExistsOnKey(dconn, mt.TableSource.Tables[0].PhysicalName, levelKey, false, true,r);
			logger.debug("Checking for any index on "+ mt.TableSource.Tables[0].PhysicalName +" : " + (isIndexPresent ? " Index Present " : "Index Not Present"));
			if(!isIndexPresent)
			{
				logger.debug("Optimizing delete by Creating a "+ (levelKeyColumnsList.size()>1 ? "Compound Index"  : "Index" ) + " on " + mt.TableSource.Tables[0].PhysicalName + " on Columns " + mtKeys.toString());
				areOptimizingIndicesCreated = Database.createIndex(dconn,levelKey, mt.TableSource.Tables[0].PhysicalName, false, false, null, null,true,r);
			}
		}
		
		logger.debug(" Processing for incremental snapshot fact: delete rows loaded from previous runs for the same key combination present in current run");
		long id = System.currentTimeMillis();
		logger.info("[QUERY:" + id + ":" + Query.getPrettyQuery(deleteBuf) + "]");
		int rows = stmt.executeUpdate(deleteBuf.toString());
		if (!conn.getAutoCommit())
			conn.commit();
		logger.info("[DELETEF:" + id + ":" + loadID + ":" + mt.TableName + ":" + (st != null ? st.Name : "") + ":" + rows + ":" + logicalTableKeys + ":" + ":"
				+ processingGroup + "] " + rows + " rows deleted");
		
		if(dconn.supportsIndexes()){
			if(areOptimizingIndicesCreated){
				logger.debug("Dropping Compound Index created for Optimization " +  mt.TableSource.Tables[0].PhysicalName + " on Columns " + mtKeys.toString());
				Database.dropIndex(dconn, levelKey, mt.TableSource.Tables[0].PhysicalName, false,true,r);
				
			}
			
		}
	}
	
	public static Map<String, String> getNaturalKeys(Repository r, StagingTable st, MeasureTable mt, QueryMap qm)
	{
		// Get natural keys
		Map<String, String> nkeys = new HashMap<String, String>();
		if (r.getCurrentBuilderVersion() < 20)
		{
			// Use lowest level keys only
			List<StagingColumn> nkeylist = new ArrayList<StagingColumn>();
			for (StagingColumn sc : st.Columns)
			{
				if (sc.NaturalKey)
					nkeylist.add(sc);
			}
			/*
			 * This method of using all columns where the natural key flag is selected and then pruning columns that are
			 * at higher levels than others works, but isn't technically correct. We should, instead, get the grain
			 * levels and find, for each, a key where the most columns are present.
			 */
			for (StagingColumn sc : nkeylist)
			{
				// Make sure there aren't lower level staging columns
				boolean OK = true;
				if (sc.TargetTypes == null)
					continue;
				for (String target : sc.TargetTypes)
				{
					Hierarchy h = r.findDimensionHierarchy(target);
					if (h == null)
						continue;
					// See if another column is at a lower level
					boolean lowerLevelExists = false;
					for (StagingColumn sc2 : nkeylist)
					{
						if (sc == sc2 || sc2.TargetTypes == null)
							continue;
						for (String target2 : sc2.TargetTypes)
						{
							Hierarchy h2 = r.findDimensionHierarchy(target2);
							if (h2 == null || h2 != h)
								continue;
							Level l = h.findLevelKeyColumn(sc.OriginalName);
							Level l2 = h.findLevelKeyColumn(sc2.OriginalName);
							if (l != null && l2 != null && l != l2 && l.findLevel(l2) != null)
							{
								// Make sure it's not in the key of the lower level
								boolean found = false;
								for (LevelKey lk : l2.Keys)
								{
									for (String cname : lk.ColumnNames)
									{
										if (cname.equals(sc.Name))
										{
											found = true;
											break;
										}
									}
									if (found)
										break;
								}
								lowerLevelExists = !found;
								break;
							}
						}
						if (lowerLevelExists)
							break;
					}
					if (lowerLevelExists)
					{
						OK = false;
						break;
					}
				}
				if (qm == null)
					qm = new QueryMap();
				if (OK)
					addQualifiedKeyToMap(r, mt, sc, nkeys, "B", qm);
			}
		} else
		{
			for (String[] level : st.Levels)
			{
				Hierarchy h = r.findDimensionHierarchy(level[0]);
				if (h == null)
					continue;
				Level l = h.findLevel(level[1]);
				if (l == null)
					continue;
				// Find the best level key
				LevelKey selectedKey = null;
				int numColumnsMatched = 0;
				for (LevelKey lk : l.Keys)
				{
					int numMatched = 0;
					for (String col : lk.ColumnNames)
					{
						for (StagingColumn sc : st.Columns)
						{
							if (sc.columnNameMatchesDimensionColumn(col))
							{
								numMatched++;
								break;
							}
						}
					}
					if (numMatched > numColumnsMatched)
					{
						selectedKey = lk;
						numColumnsMatched = numMatched;
					}
				}
				if (selectedKey == null)
					continue;
				if (qm == null)
					qm = new QueryMap();
				for (String col : selectedKey.ColumnNames)
				{
					for (StagingColumn sc : st.Columns)
					{
						if (sc.columnNameMatchesDimensionColumn(col))
						{
							addQualifiedKeyToMap(r, mt, sc, nkeys, "B", qm);
							break;
						}
					}
				}
			}			
		}
		return nkeys;
	}
	
	/**
	 * Add an entry to the key map for a given staging column that is a key for a measure table
	 * 
	 * @param mt
	 * @param sc
	 * @param nkeys
	 */
	public static void addQualifiedKeyToMap(Repository r, MeasureTable mt, StagingColumn sc, Map<String, String> nkeys, String alias, QueryMap qm)
	{
		String measureColumnName = sc.getMatchingMeasureColumnName(mt, r,false);
		/*
		 * Make sure that the measure column is present in the measure table being updated. If a natural key is part of
		 * a level that does not have the corresponding dimension table generated, natural key is not made part of the
		 * measure table definition
		 */
		MeasureColumn fmc = null;
		for (MeasureColumn mc : mt.MeasureColumns)
		{
			if (mc.ColumnName.equals(measureColumnName))
			{
				fmc = mc;
				break;
			}
		}
		if (fmc != null)
		{
			if (fmc.Qualify)
			{
				int index = fmc.PhysicalName.indexOf('.');
				if (index > 0)
					nkeys.put(fmc.PhysicalName.substring(index + 1), sc.getQualifiedPhysicalFormula(r, alias, qm));
				else
					nkeys.put(fmc.PhysicalName, sc.getQualifiedPhysicalFormula(r, alias, qm));
			} else
			{
				DatabaseConnection dc = r.getDefaultConnection();
				nkeys.put(Util.replaceWithNewPhysicalString(fmc.ColumnName, dc,r), sc.getQualifiedPhysicalFormula(r, alias, qm));
			}
		}
	}

	
	private boolean ignoreUpdateForTable(Statement stmt, String tableName, int loadID)
	{
		String query = "SELECT COUNT(1) FROM " + Database.getQualifiedTableName(dconn, tableName) + 
				 " WHERE LOAD_ID = " + loadID;
		logger.info("[QUERY:" + Query.getPrettyQuery(query) + "]");
		ResultSet rs = null;
		int result = -1;
		try
		{
			rs = stmt.executeQuery(query);
			if (rs.next())
			{
				result = rs.getInt(1);
			}
			logger.debug("Count = " + result);
			return result <= 0;
		} catch (SQLException sqlEx)
		{
			logger.warn("Error while checking data loaded for table " + tableName + ": " + sqlEx.toString());
			return false;
		} finally
		{
			try
			{
				if (rs != null)
					rs.close();
			} catch (SQLException ex)
			{
				//do nothing
			}
		}
		
	}
	

	private void updateSurrogateFactKeysForDegenerateDimensions(Set<DimensionTable> degenerateDimensions, MeasureTable mt, 
			int loadID, Statement stmt, Connection conn, String processingGroup) throws SQLException
	{
		//Update fact surrogate keys for degenerate dimensions
		List<String> sourceColumns = new ArrayList<String>();
		List<String> targetColumns = new ArrayList<String>();
		if(degenerateDimensions != null && (!degenerateDimensions.isEmpty()))
		{
			for (MeasureColumn mc : mt.MeasureColumns)
			{
				int index = mc.ColumnName.indexOf('.');
				String dimName = null;
				String colName = null;
				if (index > 0)
				{
					dimName = mc.ColumnName.substring(0, index);
					colName = mc.ColumnName.substring(index + 1);
					DimensionColumn dc = r.findSurrogateKey(dimName, colName);
					if (dc != null && degenerateDimensions.contains(dc.DimensionTable))
					{
						String srcColName = dc.PhysicalName;
						if(dc.PhysicalName.indexOf(".") > 0)
						{
							srcColName = dc.PhysicalName.substring(dc.PhysicalName.indexOf(".") + 1);
						}
						String targetColName = mc.PhysicalName;
						if(mc.PhysicalName.indexOf(".") > 0)
						{
							targetColName = mc.PhysicalName.substring(mc.PhysicalName.indexOf(".") + 1);
						}
						sourceColumns.add(srcColName);
						targetColumns.add(targetColName);
					}
				}
			}
			if(sourceColumns.size() > 0 && targetColumns.size() > 0)
			{
				String mtname = mt.PhysicalName;
				StringBuilder updatesb = new StringBuilder();
				updatesb.append("UPDATE " + Database.getQualifiedTableName(dconn, mtname) + " SET ");
				boolean isFirstCol = true;
				for(int srcIdx = 0; srcIdx < sourceColumns.size(); srcIdx++)
				{
					if(!isFirstCol)
					{
						updatesb.append(", ");
					}
					else
					{
						isFirstCol = false;
					}
					updatesb.append(targetColumns.get(srcIdx));
					updatesb.append(" = ");
					updatesb.append(sourceColumns.get(srcIdx));
				}
				updatesb.append(" WHERE LOAD_ID = " + loadID);
				long updateStartTime = System.currentTimeMillis();
				logger.info("[QUERY:" + updateStartTime + ":" + Query.getPrettyQuery(updatesb) + "]");
				int updateRows = 0;
				updateRows = stmt.executeUpdate(updatesb.toString());
				if (!conn.getAutoCommit())
					conn.commit();
				long updateFinishTime = System.currentTimeMillis();
				logger.info("[UPDATEF:" + updateStartTime + ":" + loadID + ":" + mt.TableName + ":" + (updateFinishTime - updateStartTime) + ":"
						+ processingGroup + "] " + updateRows + " rows updated");
			}
		}

	}

	private Map<Level, SurrogateKeyUpdate> getSurrogateKeyUpdatesForMeasureTable(int loadID, MeasureTable mt, Statement stmt, Set<String> loadedSet,
			StagingTable st, Set<DimensionTable> degenerateDimensions) throws SQLException
	{
		String mtname = mt.TableSource.Tables[0].PhysicalName;
		/*
		 * Generate an update clause for every level where keys exist
		 */
		Map<Level, SurrogateKeyUpdate> mappedLevels = new LinkedHashMap<Level, SurrogateKeyUpdate>();
		Map<Level, List<String[]>> colNames = new HashMap<Level, List<String[]>>();
		List<Level> llist = new ArrayList<Level>();
		/*
		 * Order key updates by level so that the lowest levels are loaded first (then can snowflake higher levels if
		 * needed)
		 */
		for (MeasureColumn mc : mt.MeasureColumns)
		{
			int index = mc.ColumnName.indexOf('.');
			String dimName = null;
			String colName = null;
			if (index > 0)
			{
				dimName = mc.ColumnName.substring(0, index);
				colName = mc.ColumnName.substring(index + 1);
				DimensionColumn dc = r.findSurrogateKey(dimName, colName);
				if (dc != null)
				{
					String[] col = new String[3];
					col[0] = dimName;
					col[1] = colName;
					col[2] = mc.PhysicalName;
					Level rl = r.findDimensionLevelColumn(dc.DimensionTable.DimensionName, dc.ColumnName);
					if (rl == null)
						continue;
					int lindex = 0;
					for (; lindex < llist.size(); lindex++)
					{
						Level l2 = llist.get(lindex);
						if (l2 == rl || l2.Hierarchy.DimensionName != rl.Hierarchy.DimensionName)
							continue;
						if (l2.findLevel(rl) != null)
							break;
					}
					if (lindex < llist.size())
						llist.add(lindex, rl);
					else
						llist.add(rl);
					List<String[]> collist = colNames.get(rl);
					if (collist == null)
					{
						collist = new ArrayList<String[]>();
						colNames.put(rl, collist);
					}
					collist.add(col);
				}
			}
		}
		for (Level l : llist)
		{
			for (String[] col : colNames.get(l))
			{
				lwHelper.getSurrogateUpdates(mtname, mt, null, col[0], col[1], col[2], loadedSet, mappedLevels, true, st, degenerateDimensions, loadID, r);
			}
		}
		return (mappedLevels);
	}

	/**
	 * Deletes are terribly slow in Infobright - so replace with inserts
	 * 
	 * @param stmt
	 * @param conn
	 * @param mtname
	 * @throws SQLException 
	 */
	protected void convertInfobrightDeletes(Statement stmt, Connection conn, String mtname, MeasureTable mt, StagingTable st, List<String> cnames,
			List<String> pnames, int loadID, StringBuilder keyfilter, String grainKeyName) throws SQLException
	{
		/*Implementation in IBFactLoader.java */
	}
	
	private boolean isIgnoreStagingTableForFact(StagingTable st,MeasureTable mt)
	{		
		String mtname = mt.TableSource.Tables[0].PhysicalName;	
		if (factToStagingMap != null)
		{
			List<String> stNameList = factToStagingMap.get(mtname);		
			if (stNameList!=null && !stNameList.isEmpty())
			{
				int listsize = stNameList.size();				
				String lastStTableName = stNameList.get(listsize-1);
				if (lastStTableName.equals(st.Name))
				{
					/*
					 * Skip this staging table since we already loaded from this in the previous pass
					 */
					logger.info("Skipping inserting new records into " + mt.TableName + " from staging table "+ st.Name +" since it has already been loaded in a previous pass");
					return true;				
				}
			}		
		}
		return false;
	}
}
