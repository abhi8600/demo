/**
 * $Id: StagingColumn.java,v 1.57 2012-11-12 18:25:19 BIRST\ricks Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.jdom.Element;
import org.jdom.Namespace;
import com.successmetricsinc.Repository;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.DimensionColumn;
import com.successmetricsinc.query.DimensionTable;
import com.successmetricsinc.query.Hierarchy;
import com.successmetricsinc.query.Level;
import com.successmetricsinc.query.LevelKey;
import com.successmetricsinc.query.MeasureColumn;
import com.successmetricsinc.query.MeasureTable;
import com.successmetricsinc.query.QueryMap;
import com.successmetricsinc.util.RepositoryException;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XmlUtils;
import com.successmetricsinc.warehouse.StagingColumnGrainInfo.NameType;

/**
 * @author Brad Peters
 * 
 */
public class StagingColumn implements Serializable
{
	private static final long serialVersionUID = 2L;
	public String Name;
	public String PhysicalName;
	public String DataType;
	public int Width;
	public String SourceFileColumn;
	public List<String> TargetTypes;
	public boolean NaturalKey;
	public boolean Index;
	public Transformation[] Transformations;
	public StagingTable Table;
	public StagingColumnGrainInfo[] GrainInfo;
	public List<String> ExpandedColumnNames;
	public List<String> ExpandedMeasureColumnNames;
	public String UnknownValue;
	public String[] TargetAggregations;
	public boolean ExcludeFromChecksum;
	public SecurityFilter SecFilter;
	public String OriginalName;
	public boolean GenerateTimeDimension;
	public boolean TableKey;
	public boolean AnalyzeMeasure;
	
	private boolean isTargetTypesTag;
	private boolean isGrainInfoTag;

	public StagingColumn()
	{
	}

	public StagingColumn(StagingTable table, Element e, Namespace ns) throws RepositoryException
	{
		Table = table;
		Name = e.getChildTextTrim("Name", ns);
		String sPhysicalNameTemp = e.getChildTextTrim("PhysicalName", ns);
		if(sPhysicalNameTemp!=null && sPhysicalNameTemp.length()>0)
		{
			PhysicalName = sPhysicalNameTemp;
		}
		OriginalName = Name;
		DataType = Util.intern(e.getChildTextTrim("DataType", ns));
		Width = Integer.parseInt(e.getChildTextTrim("Width", ns));
		SourceFileColumn = Util.intern(e.getChildTextTrim("SourceFileColumn", ns));
		TargetTypes = Repository.getInternedStringArrayListChildren(e, "TargetTypes", ns);
		isTargetTypesTag = e.getChild("TargetTypes",ns) != null? true:false;
		String s = e.getChildTextTrim("NaturalKey", ns);
		NaturalKey = s == null ? false : Boolean.valueOf(s).booleanValue();
		s = e.getChildTextTrim("Index", ns);
		Index = s == null ? false : Boolean.valueOf(s).booleanValue();
		UnknownValue = Util.intern(e.getChildTextTrim("UnknownValue", ns));
		TargetAggregations = Repository.getInternedStringArrayChildren(e, "TargetAggregations", ns);
		Element fe = e.getChild("GrainInfo", ns);
		isGrainInfoTag = e.getChild("GrainInfo", ns)!=null?true:false;
		if (fe != null)
		{
			@SuppressWarnings("rawtypes")
			List l = fe.getChildren();
			GrainInfo = new StagingColumnGrainInfo[l.size()];
			for (int count = 0; count < l.size(); count++)
			{
				GrainInfo[count] = new StagingColumnGrainInfo((Element) l.get(count), ns);
			}
		}
		Element te = e.getChild("Transformations", ns);
		if (te != null)
		{
			@SuppressWarnings("rawtypes")
			List l2 = te.getChildren();
			Transformations = new Transformation[l2.size()];
			for (int count2 = 0; count2 < l2.size(); count2++)
			{
				Transformations[count2] = Transformation.createStagingTransformation(Util.replaceWithPhysicalString(table.Name), table, (Element) l2
						.get(count2), ns);
			}
		}
		Element se = e.getChild("SecFilter", ns);
		if (se != null)
		{
			SecFilter = new SecurityFilter();
			s = se.getChildTextTrim("Type", ns);
			SecFilter.Type = s == null ? SecurityFilter.TYPE_VARIABLE : Integer.valueOf(s).intValue();
			Element sen = se.getChild("Enabled", ns);
			if (sen != null)
			{
				SecFilter.Enabled = Boolean.valueOf(se.getChildTextTrim("Enabled", ns)).booleanValue();
			} else
			{
				SecFilter.Enabled = false;
			}
			SecFilter.SessionVariable = se.getChildTextTrim("SessionVariable", ns);
			SecFilter.FilterGroups = Repository.getStringArrayChildren(se, "FilterGroups", ns);
		}
		Element ic = e.getChild("ExcludeFromChecksum", ns);
		if (ic != null)
		{
			ExcludeFromChecksum = Boolean.valueOf(e.getChildTextTrim("ExcludeFromChecksum", ns)).booleanValue();
		} else
		{
			ExcludeFromChecksum = false;
		}
		ExpandedColumnNames = null;
		if (GrainInfo != null && GrainInfo.length > 0)
		{
			for (int i = 0; i < GrainInfo.length; i++)
			{
				if (GrainInfo[i].NewName != null && GrainInfo[i].NewName.trim().length() > 0)
				{
					if (ExpandedColumnNames == null)
					{
						ExpandedColumnNames = new ArrayList<String>();
					}
					if (GrainInfo[i].NewNameType == NameType.Prefix)
						ExpandedColumnNames.add(GrainInfo[i].NewName + Name);
					else if (GrainInfo[i].NewNameType == NameType.Rename)
						ExpandedColumnNames.add(GrainInfo[i].NewName);
				}
			}
		}
		if (TargetAggregations != null)
		{
			ExpandedMeasureColumnNames = new ArrayList<String>();
			for (String ts : TargetAggregations)
			{
				ExpandedMeasureColumnNames.add(getTargetAggregationName(ts, Name));
			}
		}
		String temp = e.getChildText("GenerateTimeDimension",ns);
		if (temp!=null)
		{
			GenerateTimeDimension = Boolean.parseBoolean(temp);
		}
		temp = e.getChildText("TableKey",ns);
		if (temp!=null)
		{
			TableKey = Boolean.parseBoolean(temp);
		}
		temp = e.getChildText("AnalyzeMeasure",ns);
		if (temp!=null)
		{
			AnalyzeMeasure = Boolean.parseBoolean(temp);
		}
	}
	
	public Element getStagingColumnElement(Namespace ns)
	{
		Element e = new Element("StagingColumn",ns);
		
		Element child = new Element("Name",ns);
		child.setText(Name);
		e.addContent(child);
		
		child = new Element("DataType",ns);
		child.setText(DataType);
		e.addContent(child);
		
		child = new Element("Width",ns);
		child.setText(String.valueOf(Width));
		e.addContent(child);
		

		if (PhysicalName!=null && PhysicalName.length()>0)
		{
			child = new Element("PhysicalName",ns);
			child.setText(PhysicalName);
			e.addContent(child);
		}
		
		if (SourceFileColumn!=null)
		{
			child = new Element("SourceFileColumn",ns);
			child.setText(SourceFileColumn);
			e.addContent(child);
		}
				
		if (Transformations!=null && Transformations.length>0)
		{
			child = new Element("Transformations",ns);
			for (Transformation ts : Transformations)
			{
				child.addContent(ts.getTransformationElement(ns));
			}
			e.addContent(child);
		}		
						
		if (TargetTypes!=null && !TargetTypes.isEmpty())
		{
			child = new Element("TargetTypes",ns);
			for (String targetType : TargetTypes)
			{
				Element stringElement = new Element("string",ns);
				if (targetType!=null && !targetType.isEmpty())
				{
					stringElement.setText(targetType);
				}
				child.addContent(stringElement);
			}
			e.addContent(child);
		}
		else if (isTargetTypesTag)
		{
			child = new Element("TargetTypes",ns);
			e.addContent(child);
		}
		
		if (NaturalKey)
		{
			child = new Element("NaturalKey",ns);
			child.setText(String.valueOf(NaturalKey));
			e.addContent(child);
		}
		
		if (GenerateTimeDimension)
		{
			child = new Element("GenerateTimeDimension",ns);
			child.setText(String.valueOf(GenerateTimeDimension));
			e.addContent(child);
		}
		
		if (Index)
		{
			child = new Element("Index",ns);
			child.setText(String.valueOf(Index));
			e.addContent(child);
		}
		
		if (GrainInfo!=null && GrainInfo.length>0)
		{
			child = new Element("GrainInfo",ns);
			for (StagingColumnGrainInfo gi : GrainInfo)
			{
				child.addContent(gi.getStagingColumnGrainInfo(ns));
			}
			e.addContent(child);
		}
		else if (isGrainInfoTag)
		{
			child = new Element("GrainInfo",ns);
			e.addContent(child);
		}

		if (UnknownValue!=null)
		{
		  XmlUtils.addContent(e, "UnknownValue", UnknownValue,ns);
		}
				
		if (TargetAggregations!=null && TargetAggregations.length>0)
		{
			child = new Element("TargetAggregations",ns);
			for (String targetAggregation : TargetAggregations)
			{
				Element stringElement = new Element("string",ns);
				stringElement.setText(targetAggregation);
				child.addContent(stringElement);
			}
			e.addContent(child);
		}		
		
		if (ExcludeFromChecksum)
		{
			child = new Element("ExcludeFromChecksum",ns);
			child.setText(String.valueOf(ExcludeFromChecksum));
			e.addContent(child);
		}
		
		if (TableKey)
		{
			child = new Element("TableKey",ns);
			child.setText(String.valueOf(TableKey));
			e.addContent(child);
		}
		
		if (SecFilter!=null)
		{
			e.addContent(SecFilter.getSecurityFilterElement("SecFilter", ns));
		}
		
		if (AnalyzeMeasure)
		{
			child = new Element("AnalyzeMeasure",ns);
			child.setText(String.valueOf(AnalyzeMeasure));
			e.addContent(child);
		}
		
		return e;
	}

	private String getTargetAggregationName(String aggRule, String baseName)
	{
		if (aggRule.equals("SUM"))
			return ("Sum: " + baseName);
		else if (aggRule.equals("AVG"))
			return ("Avg: " + baseName);
		else if (aggRule.equals("COUNT"))
			return ("# " + baseName);
		else if (aggRule.equals("COUNT DISTINCT"))
			return ("# Distinct " + baseName);
		else if (aggRule.equals("MIN"))
			return ("Min: " + baseName);
		else if (aggRule.equals("MAX"))
			return ("Max: " + baseName);
		return (baseName);
	}
	
	public String getPhysicalName()
	{
		return ((this.PhysicalName!=null && this.PhysicalName.length()>0 ) ? this.PhysicalName : this.Name);
	}

	public String getQualifiedPhysicalFormula(Repository r, String tableQualifier, QueryMap qm)
	{
		return (getQualifiedPhysicalFormula(r, tableQualifier, false, null, qm));
	}

	public String getQualifiedPhysicalFormula(Repository r, String tableQualifier, boolean wrapped, StagingColumnGrainInfo scg, QueryMap qm)
	{
		DatabaseConnection dc = r.getDefaultConnection();
		if (scg == null || scg.Aggregation == null)
		{
			if (Transformations != null)
			{
				for (Transformation stran : Transformations)
				{
					if (Expression.class.isInstance(stran))
					{
						String expression = (((Expression) stran).getPhysicalFormula());
						expression = Table.getReplacedString(expression);
						if (tableQualifier != null)
						{
							expression = Util.replaceStr(expression, Table.Name + ".", tableQualifier + ".");
						}
						return expression;
					} else if (TableLookup.class.isInstance(stran))
					{
						String lookup = (((TableLookup) stran).getPhysicalFormula());
						lookup = Table.getReplacedString(lookup, qm.getMap(this));
						lookup = Util.replaceStr(lookup, ((TableLookup) stran).LookupTable + ".", qm.getMap(this) + ".");
						return lookup;
					} else if (StaticDateID.class.isInstance(stran))
					{
						String val = Integer.toString(((StaticDateID) stran).getPhysicalFormula(r));
						return val;
					}
				}
			} else
			{
				String result = getQualifiedPhysicalName(tableQualifier, dc,r);
				return result;
			}
		}
		if (scg == null || scg.NewName == null || scg.NewName.length() == 0)
			return (getQualifiedPhysicalName(tableQualifier, getPhysicalName(), dc));
		return (getQualifiedPhysicalName(tableQualifier, scg.NewNameType == NameType.Rename ? Util.replaceWithNewPhysicalString(scg.NewName, dc,r) : Util
				.replaceWithNewPhysicalString(scg.NewName + getPhysicalName(), dc,r), dc));
	}

	public String getQualifiedPhysicalName(String tableQualifier, DatabaseConnection dc)
	{
		return (getQualifiedPhysicalName(tableQualifier, getPhysicalName(), dc));
	}
	
	public String getQualifiedPhysicalName(String tableQualifier, DatabaseConnection dc,Repository r)
	{
		return (getQualifiedPhysicalName(tableQualifier, getPhysicalName(), dc,r));
	}

	public String getQualifiedPhysicalName(String tableQualifier, String columnName, DatabaseConnection dc)
	{
		return getQualifiedPhysicalName(tableQualifier, columnName, dc,null);
		
	}
	public String getQualifiedPhysicalName(String tableQualifier, String columnName, DatabaseConnection dc,Repository r)
	{
		String result = Util.replaceWithNewPhysicalString(columnName, dc,r);
		return (tableQualifier != null ? tableQualifier + "." + result : result);
	}

	public List<String[]> getLookupTables(QueryMap qm, DatabaseConnection dc,Repository r)
	{
		if (Transformations != null)
		{
			for (Transformation stran : Transformations)
			{
				if (TableLookup.class.isInstance(stran))
				{
					List<String> list = ((TableLookup) stran).getLookupTables();
					List<String[]> rlist = new ArrayList<String[]>();
					for (String s : list)
						rlist.add(new String[]
						{ s, qm.getMap(this), Util.replaceWithNewPhysicalString(getPhysicalName(),dc,r)});
					return (list == null ? new ArrayList<String[]>(0) : rlist);
				}
			}
		}
		return (new ArrayList<String[]>(0));
	}

	public List<String> getLookupJoins(StagingTable st, String stagingTableQualifier, QueryMap qm)
	{
		if (Transformations != null)
		{
			for (Transformation stran : Transformations)
			{
				if (TableLookup.class.isInstance(stran))
				{
					List<String> list = ((TableLookup) stran).getLookupJoins(stagingTableQualifier, qm.getMap(this));
					return (list == null ? new ArrayList<String>(0) : list);
				}
			}
		}
		return (new ArrayList<String>(0));
	}

	public boolean matchNaturalKeyName(String colName, Repository r)
	{
		for (String[] s : Table.Levels)
		{
			// Make sure the table grain is in the dimension of this column
			if ((s[0] + "." + OriginalName).equals(colName))
			{
				/*
				 * If this column is targeted to a dimension, make sure it matches, otherwise, assume that it is a key
				 * column that is not to be used as a source
				 */
				int count = 0;
				for (String tt : TargetTypes)
				{
					if (!tt.equals("Measure"))
					{
						count++;
						if ((tt + "." + OriginalName).equals(colName))
							return (true);
					}
				}
				if (count == 0)
					return (true);
				/*
				 * If not, see if the column is in a compound level key, if so, then assume it matches
				 */
				Hierarchy h = r.findDimensionHierarchy(s[0]);
				if (h != null)
				{
					Level l = h.findLevel(s[1]);
					for (LevelKey lk : l.Keys)
					{
						if (!lk.SurrogateKey)
							for (String key : lk.ColumnNames)
							{
								if (key.equals(OriginalName))
									return (true);
							}
					}
				}
			}
		}
		return (false);
	}

	/**
	 * Whether the staging column (expanded or the actual name) matches the measure column name.
	 * 
	 * @param colName
	 *            Dimension Column name
	 * @return true of false
	 */
	public boolean columnNameMatchesMeasure(String colName, Repository r)
	{
		if (ExpandedMeasureColumnNames != null && ExpandedMeasureColumnNames.size() > 0)
		{
			if (ExpandedMeasureColumnNames.contains(colName))
				return (true);
		}
		if (NaturalKey)
		{
			return (matchNaturalKeyName(colName, r) || OriginalName.equals(colName));
		} else
			return (OriginalName.equals(colName));
	}

	/**
	 * Whether the staging column (expanded or the actual name) matches the dimension column name.
	 * 
	 * @param colName
	 *            Dimension Column name
	 * @return true of false
	 */
	public boolean columnNameMatchesDimensionColumn(String colName)
	{
		boolean match = false;
		if ((ExpandedColumnNames != null) && (ExpandedColumnNames.size() > 0))
			match = ExpandedColumnNames.contains(colName);
		else
			match = OriginalName.equals(colName);
		return match;
	}

	/**
	 * Iterates over all dimension columns to see if a staging column name (generated with or without prefix) matches a
	 * given dimension column name and returns the appropriate dimension column
	 * 
	 * @param dt
	 * @return
	 */
	public DimensionColumn getMatchingDimensionColumn(DimensionTable dt)
	{
		if ((ExpandedColumnNames != null) && (ExpandedColumnNames.size() > 0))
		{
			for (DimensionColumn dc : dt.DimensionColumns)
			{
				if (ExpandedColumnNames.contains(dc.ColumnName))
				{
					return (dc);
				}
			}
		} else
		{
			for (DimensionColumn dc : dt.DimensionColumns)
			{
				if (OriginalName.equals(dc.ColumnName))
				{
					return (dc);
				}
			}
		}
		return null;
	}

	/**
	 * Iterates over all measure columns to see if a staging column name (generated with or without prefix) matches a
	 * given measure column name and returns the appropriate measure column name
	 * 
	 * Added isCheckKeyFirst. If true and if NaturalKey then it will first look for a matching measure column on this 
	 * basis. We need to do this because when calling while processing incremental snapshot fact, we always want to use a
	 * key column if available to delete and if not then we can move on to the other options.
	 * @return
	 */
	public String getMatchingMeasureColumnName(MeasureTable mt, Repository r, boolean isCheckKeyFirst)
	{
		if(isCheckKeyFirst && NaturalKey)
		{
			for (MeasureColumn mc : mt.MeasureColumns)
			{
				if (matchNaturalKeyName(mc.ColumnName, r))
					return (mc.ColumnName);
			}
		}
		if (ExpandedMeasureColumnNames != null && ExpandedMeasureColumnNames.size() > 0)
		{
			for (MeasureColumn mc : mt.MeasureColumns)
			{
				if (ExpandedMeasureColumnNames.contains(mc.ColumnName))
					return (mc.ColumnName);
			}
		}
		if (NaturalKey && !isCheckKeyFirst)
		{
			for (MeasureColumn mc : mt.MeasureColumns)
			{
				if (matchNaturalKeyName(mc.ColumnName, r))
					return (mc.ColumnName);
			}
		} else
			return (OriginalName);
		return null;
	}

	public String toString()
	{
		return (Name);
	}

	public StagingColumnGrainInfo getGrainInfoForLevel(String dimension, String levelName)
	{
		if (GrainInfo == null)
			return (null);
		for (StagingColumnGrainInfo scg : GrainInfo)
		{
			if ((scg.Dimension.equals(dimension)) && (scg.Level.equals(levelName)))
			{
				return (scg);
			}
		}
		return (null);
	}

	public boolean isPersistable()
	{
		boolean persist = true;
		if (Transformations != null && Transformations.length > 0)
		{
			for (Transformation tr : Transformations)
			{
				if (Expression.class.isInstance(tr) || TableLookup.class.isInstance(tr) || StaticDateID.class.isInstance(tr))
				{
					persist = false;
					break;
				}
			}
		}
		return persist;
	}

	/**
	 * Decide if the staging column data type matches with the data type provided. If they match exactly, return true.
	 * If the staging column is a date id column and the data type provided is integer, also return true.
	 * 
	 * @param dataType
	 * @return boolean to indicate if the data types match
	 */
	public boolean isMatchingDataType(String dataType)
	{
		if (dataType == null)
		{
			return false;
		}
		boolean dataTypeMatches = false;
		if (DataType.equals(dataType))
		{
			dataTypeMatches = true;
		} else if (DataType.startsWith("Date ID:") && dataType.equals("Integer"))
		{
			dataTypeMatches = true;
		}
		return dataTypeMatches;
	}
}
