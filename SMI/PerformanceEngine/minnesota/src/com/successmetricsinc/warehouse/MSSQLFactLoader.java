/**
 *
 * Copyright (C) 2007-2009 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.successmetricsinc.ExecuteSteps.Step;
import com.successmetricsinc.Repository;
import com.successmetricsinc.query.Database;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.DatabaseIndex;
import com.successmetricsinc.query.LevelKey;
import com.successmetricsinc.query.MeasureTable;
import com.successmetricsinc.query.Query;
import com.successmetricsinc.util.Util;

public class MSSQLFactLoader extends FactLoader{
	
	private static Logger logger = Logger.getLogger(MSSQLFactLoader.class);

	public MSSQLFactLoader(Repository r, DatabaseConnection dconn,
			LoadWarehouse lw, Step step, List<StagingTable> stList) {
		super(r, dconn, lw, step, stList);
	}

	@Override
	protected void processForIncrementalSnapshotFact(Connection conn, MeasureTable mt, Set<String> mtKeys, Statement stmt, int loadID, StagingTable st,
			Set<String> logicalTableKeys,String processingGroup) throws SQLException
	{
	    String qualifiedMeasureTableName = Database.getQualifiedTableName(dconn, mt.TableSource.Tables[0].PhysicalName);
	    StringBuilder deleteBuf = new StringBuilder();
	    StringBuilder countBuf = new StringBuilder();
	    StringBuilder joinBuff = new StringBuilder();	    
	    StringBuilder tempTableBuff = new StringBuilder();
	    	    
	    boolean firstKey = true;
	    StringBuilder keyFilter = new StringBuilder();
	    StringBuilder keySelector = new StringBuilder();
	    for (String mtk : mtKeys)
	    {
	      if (firstKey)
	      {
	        firstKey = false;
	      } else
	      {
	        keyFilter.append(" AND ");
	        keySelector.append(",");
	      }
	      if (mtk.contains("."))
	      {
	        mtk = mtk.substring(mtk.lastIndexOf(".") + 1);
	      }
	      keyFilter.append(qualifiedMeasureTableName + "." + mtk + "=B." + mtk);
	      keySelector.append(mtk);
	    }
	    
	    String tempTableName = Database.getQualifiedTableName(dconn,"MTKEYS_"+System.currentTimeMillis()); 
	    tempTableBuff.append("SELECT ");
	    tempTableBuff.append(keySelector);	
	    tempTableBuff.append(" INTO "+tempTableName);
	    tempTableBuff.append(" FROM ").append(qualifiedMeasureTableName);
	    tempTableBuff.append(" WHERE LOAD_ID=").append(loadID);
	    tempTableBuff.append(" GROUP BY ").append(keySelector);
	    
	    
	    try {
	    	logger.info("[QUERY:" + ":" + Query.getPrettyQuery(tempTableBuff) + "]");
	    	stmt.executeUpdate(tempTableBuff.toString());
	    	
			joinBuff.append(qualifiedMeasureTableName + " INNER JOIN " + tempTableName + " B ON ");
			joinBuff.append(r.replaceVariables(null, keyFilter.toString()));
			joinBuff.append(" AND " + qualifiedMeasureTableName + ".LOAD_ID < " + loadID);		
			
			countBuf.append("SELECT COUNT_BIG(*) FROM ");
			countBuf.append(joinBuff);
			
			LevelKey levelKey = new LevelKey();
			List<String> levelKeyColumnsList = new ArrayList<String>();
			levelKeyColumnsList.addAll(logicalTableKeys);
			levelKeyColumnsList.add("LOAD_ID");
			
			Collections.sort(levelKeyColumnsList);
			
			levelKey.ColumnNames = levelKeyColumnsList.toArray(new String[levelKeyColumnsList.size()]);
			
			String[] indexColumns = new String[mtKeys.size()+1];
			int index = 0;
			for (String mtKey : mtKeys)
			{
				indexColumns[index++] = Util.unQualifyPhysicalName(mtKey);
			}
			indexColumns[index] = "LOAD_ID";	 
			
			String indexName = Database.getIndexName(dconn, mt.TableSource.Tables[0].PhysicalName, levelKey, false, 
			    Database.getMaxIndexNameLength(dconn.DBType),r);
			boolean isIndexWithSameNamePresent = Database.indexExists(dconn, mt.TableSource.Tables[0].PhysicalName, indexName);
			boolean isIndexPresent = Database.indexExistsOnColumns(dconn, mt.TableSource.Tables[0].PhysicalName, indexColumns, r, indexName);
			logger.debug("Checking for any index on "+ mt.TableSource.Tables[0].PhysicalName +" : " + (isIndexWithSameNamePresent ? " Index With Same Name Present " : " Index With Same Name Not Present"));    
			logger.debug("Checking for any index on "+ mt.TableSource.Tables[0].PhysicalName +" : " + (isIndexPresent ? " Index With Same Columns Present " : " Index With Same Columns Not Present"));
			if(!isIndexPresent && !isIndexWithSameNamePresent)
			{
			  logger.debug("Optimizing delete by Creating a "+ (levelKeyColumnsList.size()>1 ? "Compound Index"  : "Index" ) + " on " + mt.TableSource.Tables[0].PhysicalName + " on Columns " + levelKeyColumnsList.toString());     
			  Database.createIndex(dconn, stmt, dconn.Schema, mt.TableSource.Tables[0].PhysicalName, indexName, indexColumns, null, false);
			}
			
			/*
			 * Delete existing indexes to optimize performance
			 */
			logger.debug("Dropping all indexes on fact to optimize delete ");
			List<DatabaseIndex> lstIndexes = Database.getIndices(dconn, stmt, dconn.Schema, mt.TableSource.Tables[0].PhysicalName);
			if (lstIndexes != null && !lstIndexes.isEmpty())
			{
				for (DatabaseIndex di : lstIndexes)
				{
					if (!di.name.equals(indexName))
					{
						Database.dropIndex(dconn, conn, di.name, qualifiedMeasureTableName);
					}
					else
					{
						logger.debug("Index "+di.name+" not being dropped as it is going to be used for delete");
					}
				}
			}
			
			long rowCount = executeQueryAndReturnCount(stmt,countBuf.toString());
			if (rowCount > 0)
			{
				long batchSize = 1000000;
				if (r.getServerParameters()!=null && r.getServerParameters().getBulkDeleteBatchSize()!=batchSize && r.getServerParameters().getBulkDeleteBatchSize()>0)
				{
					batchSize = r.getServerParameters().getBulkDeleteBatchSize();
				}
				if (rowCount>batchSize)
				{
					logger.debug(" Processing for incremental snapshot fact: delete rows loaded from previous runs for the same key combination present in current run");
					logger.debug("Deleting rows more than batch size "+batchSize+" "+rowCount);
					long id = System.currentTimeMillis();
					int rows = 0;
					deleteBuf.append("DELETE TOP(" + batchSize +")" + qualifiedMeasureTableName + " FROM ");
					deleteBuf.append(joinBuff);
					logger.info("[QUERY:" + id + ":" + Query.getPrettyQuery(deleteBuf) + "]");
					long deletedRows = 0;
					do
					{
						deletedRows = stmt.executeUpdate(deleteBuf.toString());
						logger.debug("Deleted in batch "+deletedRows);
						rows += deletedRows;
					}while (deletedRows>0);
					if (!conn.getAutoCommit())
						conn.commit();
					logger.info("[DELETEF:" + id + ":" + loadID + ":" + mt.TableName + ":" + (st != null ? st.Name : "") + ":" + rows + ":" + logicalTableKeys + ":" + ":"
							+ processingGroup + "] " + rows + " rows deleted");
				}
				else
				{
					logger.debug(" Processing for incremental snapshot fact: delete rows loaded from previous runs for the same key combination present in current run");
					long id = System.currentTimeMillis();
					deleteBuf.append("DELETE " + qualifiedMeasureTableName + " FROM ");
					deleteBuf.append(joinBuff);
					logger.info("[QUERY:" + id + ":" + Query.getPrettyQuery(deleteBuf) + "]");
					int rows = stmt.executeUpdate(deleteBuf.toString());
					if (!conn.getAutoCommit())
						conn.commit();
					logger.info("[DELETEF:" + id + ":" + loadID + ":" + mt.TableName + ":" + (st != null ? st.Name : "") + ":" + rows + ":" + logicalTableKeys + ":" + ":"
							+ processingGroup + "] " + rows + " rows deleted");
				}
			}
			else
			{
				logger.info("No rows for deletion while processing incremental snapshot fact ");
			}
		} 
	    finally {
	    	try
	    	{
	    		String dropTableQuery = "DROP TABLE "+tempTableName;
	    		logger.info("[QUERY:" + ":" + Query.getPrettyQuery(dropTableQuery) + "]");
	    		stmt.execute(dropTableQuery);
	    	}
	    	catch (Exception e)
	    	{
	    		logger.error("Drop temp table "+tempTableName+" failed. ",e);
	    	}
		}
	        
	  }
}

