package com.successmetricsinc.warehouse;

public class LineProcessorResult 
{
	public boolean error;
	public boolean filtered;
	public StringBuilder result;
	public String [] pline;
}
