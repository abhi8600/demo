/**
 * $Id: DimensionLoader.java,v 1.142 2012-11-12 14:47:06 BIRST\mjani Exp $
 *
 * Copyright (C) 2009 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;

import com.amazonaws.auth.AWSCredentials;
import com.successmetricsinc.ExecuteSteps.Step;
import com.successmetricsinc.ExecuteSteps.StepCommand;
import com.successmetricsinc.Repository;
import com.successmetricsinc.query.Database;
import com.successmetricsinc.query.Database.DatabaseType;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.DatabaseIndex;
import com.successmetricsinc.query.DimensionColumn;
import com.successmetricsinc.query.DimensionTable;
import com.successmetricsinc.query.Hierarchy;
import com.successmetricsinc.query.Level;
import com.successmetricsinc.query.LevelKey;
import com.successmetricsinc.query.MeasureColumn;
import com.successmetricsinc.query.MeasureTable;
import com.successmetricsinc.query.Query;
import com.successmetricsinc.query.QueryMap;
import com.successmetricsinc.status.Status;
import com.successmetricsinc.util.Util;

public class DimensionLoader
{
	private static Logger logger = Logger.getLogger(DimensionLoader.class);
	private Repository r;
	private DatabaseConnection dconn;
	private LoadWarehouse lw;
	private Calendar scdStartDate;
	private Calendar scdEndDate;
	private Step step;
	private List<StagingTable> stList;
	private List<DistinctResult> distinctResults = new ArrayList<DistinctResult>();
	private Map<String, String> tempIndices;
	private int numTempTables;
	private String[] subGroups;
	private static boolean USE_TEMP_TABLES_FOR_DISTINCT = true;
	private static boolean USE_SEQUENTIAL_TEMP_TABLES = true;
	private String processingGroup;
	private LoadWarehouseHelper lwHelper ;
	private DimensionUpdater dimUpdater;

	public DimensionLoader(Repository r, DatabaseConnection dconn, LoadWarehouse lw, Calendar scdStartDate, Calendar scdEndDate, Step step,
			List<StagingTable> stList, Map<String, String> tempIndices, String[] subGroups, String processingGroup)
	{
		this.r = r;
		this.dconn = dconn;
		this.lw = lw;
		this.scdStartDate = scdStartDate;
		this.scdEndDate = scdEndDate;
		this.step = step;
		this.stList = stList;
		this.tempIndices = tempIndices;
		this.subGroups = subGroups;
		this.processingGroup = processingGroup;
		lwHelper = new LoadWarehouseHelper(r,dconn);
		dimUpdater = new DimensionUpdater(r, dconn, stList, tempIndices);
	}

	static class DistinctResult
	{
		public StagingTable st;
		public LevelKey lk;
		public boolean distinct;
	}

	static class DimensionUpdateColumn
	{
		String columnName;
		String column;
		String expression;
		boolean constant;
		boolean levelKey;
		boolean checksum;
		@SuppressWarnings("unused")
		boolean surrogateKey;

		public DimensionUpdateColumn(String columnName, String column, String expression)
		{
			this.columnName = columnName;
			this.column = column;
			this.expression = expression;
		}
	}

	static class DimensionUpdateJoin
	{
		JoinType type;
		String tableName;
		String alias;
		String condition;
		int tempTableNumber;

		public DimensionUpdateJoin clone()
		{
			DimensionUpdateJoin duj = new DimensionUpdateJoin();
			duj.type = type;
			duj.tableName = tableName;
			duj.alias = alias;
			duj.condition = condition;
			duj.tempTableNumber = tempTableNumber;
			return duj;
		}
	}

	static class DimensionUpdate
	{
		boolean update; // insert vs. update
		String sourceTable;
		List<DimensionUpdateColumn> columns = new ArrayList<DimensionUpdateColumn>();
		List<String> targetJoinColumns = new ArrayList<String>();
		// StringBuilder joinCondition = new StringBuilder();
		List<DimensionUpdateJoin> joinConditions = new ArrayList<DimensionUpdateJoin>();
		String checksumExpression = null;
		StringBuilder where = new StringBuilder();
		StagingTable st;
		Set<String> logicalNames;
		List<DimensionUpdateIndex> indices = new ArrayList<DimensionUpdateIndex>();
		boolean notdistinct;
		Level sourceTableLevel;
		LevelKey lk;
		int scdType = 1;
		List<String[]> lookupTables;
		String levelFilterCondition;
		List<String> uniqueTempCreateList = new ArrayList<String>();
		List<String> uniqueTempDropList = new ArrayList<String>();
		String uniqueFilter;
		String distinctKeyFilter;
		String typeIIClear;
	}

	static class DimensionUpdateIndex
	{
		LevelKey lk;
		String tableName;
	}

	
	enum JoinType
	{
		INNER, LEFT_OUTER
	};
	
	
	/**
	 * Get a list of auto-generated dimension tables. We need to ensure here that the dimension tables at a higher level
	 * appear first in the list so that they are loaded first and lower level table's surrogate keys can be updated from
	 * the higher level tables. This requires re-ordering of the list containing auto-generated dimension tables.
	 * 
	 * @return List of auto-generated dimension tables.
	 */
	private List<DimensionTable> getGeneratedTables()
	{
		List<DimensionTable> generatedTables = new ArrayList<DimensionTable>();
		for (DimensionTable dt : r.DimensionTables)
		{
			if(dt.Imported)
			{
				continue;
			}
			if (dt.AutoGenerated && !dt.Calculated && dt.InheritTable == null && dt.TableSource.ConnectionName.equals(r.getDefaultConnectionName()))
				generatedTables.add(dt);
		}
		/*
		 * Make sure that the higher level dimension tables are inserted before the lower level dimension tables. This
		 * applies to direct as well as shared child levels.
		 */
		List<DimensionTable> orderedGeneratedTables = new ArrayList<DimensionTable>(generatedTables.size());
		for (DimensionTable dt : generatedTables)
		{
			Hierarchy h = r.findDimensionHierarchy(dt.DimensionName);
			if (h == null)
			{
				if (!orderedGeneratedTables.contains(dt))
				{
					orderedGeneratedTables.add(dt);
				}
			} else
			{
				// Position in the ordered list that this particular dimension
				// table needs to be inserted at.
				int insertPos = -1;
				if (dt.Level != null)
				{
					Level l = h.findLevel(dt.Level);
					List<Level> decendents = new ArrayList<Level>();
					l.getAllDecendents(decendents);
					if (decendents.size() > 0)
					{
						for (Level decendentLevel : decendents)
						{
							for (int i = 0; i < orderedGeneratedTables.size(); i++)
							{
								String genLevelName = orderedGeneratedTables.get(i).Level;
								if (genLevelName != null && decendentLevel.Name.equals(genLevelName) && ((insertPos == -1) || (i < insertPos)))
								{
									insertPos = i;
								}
							}
						}
					}
				}
				if (insertPos != -1)
				{
					orderedGeneratedTables.add(insertPos, dt);
				} else
				{
					orderedGeneratedTables.add(dt);
				}
			}
		}
		logger.info("Order of loading dimension tables: " + orderedGeneratedTables);
		return orderedGeneratedTables;
	}

	private String getSubstep(DimensionTable dt, String loadGroup)
	{
		return (loadGroup == null ? "" : loadGroup + ": ") + dt.TableName;
	}

			
	private boolean addHigherLevelSurrogateKeyUpdate(String dtname, Level hl, SurrogateKeyUpdate sku, DimensionUpdate du)
	{
		for (LevelKey lk : hl.Keys)
		{
			if (lk.SurrogateKey)
				continue;
			boolean foundkey = true;
			for (String s : lk.ColumnNames)
			{
				boolean foundcol = false;
				for (DimensionUpdateColumn duc : du.columns)
				{
					if (duc.columnName != null && duc.columnName.equals(s))
					{
						foundcol = true;
						break;
					}
				}
				if (!foundcol)
				{
					foundkey = false;
					break;
				}
			}
			if (foundkey)
			{
				for (int i = 0; i < sku.insertColumns.size(); i++)
				{
					DimensionUpdateColumn duc = new DimensionUpdateColumn(null, sku.insertColumns.get(i), null);
					duc.expression = sku.insertSourceColumns.get(i);
					duc.surrogateKey = true;
					du.columns.add(duc);
				}
				DimensionUpdateJoin duj = new DimensionUpdateJoin();
				duj.type = JoinType.LEFT_OUTER;
				duj.tableName = Util.replaceStr(sku.joinTable, dtname + ".", "B.");
				duj.tableName = Util.replaceStr(duj.tableName, "INNER JOIN ", "");
				duj.tableName = Util.replaceStr(duj.tableName, " ON", "").trim();
				String joinClause = Util.replaceStr(sku.joinCondition, dtname + ".", "B.");
				if (du.lookupTables != null)
				{
					for (String[] ls : du.lookupTables)
					{
						/*
						 * If the driving table or any other higher level update table is a lookup use them
						 */
						if (ls[0].equalsIgnoreCase(dtname))
							joinClause = Util.replaceStr(joinClause, "B.", ls[1] + ".");
						else
							joinClause = Util.replaceStr(joinClause, "B." + ls[2], ls[1] + "." + ls[2]);
						if (!duj.tableName.endsWith("." + ls[0]))
							joinClause = Util.replaceStr(joinClause, ls[0] + ".", ls[1] + ".");
					}
				}
				duj.condition = joinClause;
				du.joinConditions.add(duj);
				return true;
			}
		}
		return false;
	}
		
	
	private StringBuilder buildJoins(String dtname, DimensionUpdate du, List<DimensionUpdateJoin> conditions)
	{
		StringBuilder sbu = new StringBuilder();
		sbu.append(Database.getQualifiedTableName(dconn, dtname) + " A INNER JOIN " + du.sourceTable + " B ON");
		if (conditions.size() > 0)
		{
			// must output the conditions in this order
			for (DimensionUpdateJoin duj : conditions)
			{
				if (duj.tableName == null)
				{
					sbu.append(duj.condition);
				}
			}
			for (DimensionUpdateJoin duj : conditions)
			{
				if (duj.tableName != null)
				{
					sbu.append(duj.type == JoinType.INNER ? " INNER JOIN " : " LEFT OUTER JOIN ");
					if (DatabaseConnection.isDBTypeMemDB(dconn.DBType))
						sbu.append("MAXONE ");
					sbu.append(duj.tableName + (duj.alias != null ? " " + duj.alias : "") + " ON ");
					if (dconn.DBType == DatabaseType.MonetDB)
					{
						String condition = Util.replaceStr(duj.condition, dconn.Schema + ".", "");
						sbu.append(condition);
					} else
						sbu.append(duj.condition);
				}
			}
		}
		return sbu;
	}

	/**
	 * Load all dimension tables from all staging tables. Load each dimension table and staging table combination
	 * individually.
	 * 
	 * @param stmt
	 * @param loadID
	 * @param status
	 * @param loadGroup
	 * @throws Exception 
	 */
	public void loadDimensions(Connection conn, Statement stmt, int loadID, Status status, String loadGroup, Set<String> ttnames, String databasePath, AWSCredentials s3credentials,Repository r) throws Exception
	{
		List<DimensionTable> generatedTables = getGeneratedTables();
		Long currTime = System.currentTimeMillis();
		if (scdStartDate == null)
		{
			scdStartDate = Calendar.getInstance();
			scdStartDate.setTimeInMillis(currTime);
		}
		if (scdEndDate == null)
		{
			scdEndDate = Calendar.getInstance();
			scdEndDate.setTimeInMillis(currTime-2);
		}
		
		String scdStartDateStr = dconn.getOutputDateTimeFormat().format(scdStartDate.getTime());
		String scdEndDateStr = dconn.getOutputDateTimeFormat().format(scdEndDate.getTime());
		
		boolean isOracle = DatabaseConnection.isDBTypeOracle(dconn.DBType);
		boolean isPADB = DatabaseConnection.isDBTypeParAccel(dconn.DBType);
		boolean isIB = dconn.DBType == DatabaseType.Infobright;
		boolean isMonetDB = dconn.DBType == DatabaseType.MonetDB;

		HashSet<String> stagingTablesProcessed = null;
		for (DimensionTable dt : generatedTables)
		{
		  stagingTablesProcessed = new HashSet<String>();
			// Get the hierarchy
			Hierarchy h = r.findDimensionHierarchy(dt.DimensionName);
			if (h == null)
			{
				logger.info("No hierarchy for " + dt.DimensionName + " / " + dt.TableName + ", skipping");
				continue;
			}
			// Get the grain level for this dimension table
			Level l = h.findLevel(dt.Level);
			if (l == null)
			{
				logger.info("No level for " + dt.Level + ", skipping");
				continue;
			}
			// Make sure this level is not degenerate
			if (l.Degenerate)
			{
				logger.info(dt.Level + " is degenerate for " + dt.DimensionName + " / " + dt.TableName + ", skipping");
				continue;
			}
			String substep = getSubstep(dt, loadGroup);
			if (status.isComplete(StepCommand.LoadWarehouse, substep, processingGroup, true, Util.getKillFileName(r.getRepositoryRootPath(), Util.KILL_PROCESS_FILE)))
			{
				logger.info("Dimension table " + dt.TableName + " already loaded successfully for load id =" + status.getIteration()
						+ (loadGroup == null ? "" : (", for load group " + loadGroup)) 
						+ (processingGroup == null ? "" : (", for processingGroup " + processingGroup))
						+ ". This step will not be repeated.");
				continue;				
			}
			status.logStatus(step, substep, processingGroup, Status.StatusCode.Running);
			long numRows = 0;
			String dtname = dt.TableSource.Tables[0].PhysicalName;
			if (dconn.DBType == DatabaseType.MSSQL2012ColumnStore)
			{
				Database.dropColumnStoreIndex(dconn,  dtname);
			}
			Boolean firstst = true;
			Set<String> loadedSet = new HashSet<String>();
			List<Level> levelList = new ArrayList<Level>();
			h.getBottomsUpLevels(levelList);
			List<DimensionUpdate> updateList = new ArrayList<DimensionUpdate>();
			List<Level> processLevelList = new ArrayList<Level>();
			processLevelList.add(l);
			for (Level sl : levelList)
				if (!processLevelList.contains(sl))
					processLevelList.add(sl);
			QueryMap qm = new QueryMap();
			if (dconn != null)
				qm.setDBType(dconn.DBType);
			for (Level processLevel : processLevelList)
			{
				dimUpdater.getDimensionUpdates(conn, updateList, dt, h, l, processLevel, stmt, dtname, loadID, qm, scdStartDateStr, scdEndDateStr, loadedSet, firstst, ttnames);
			}
			if (updateList.isEmpty())
			{
				StringBuilder msg = new StringBuilder();
				msg.append("No matching staging tables available for loading dimension table ");
				msg.append(dt.TableName);
				if (subGroups != null && subGroups.length > 0)
				{
					msg.append(" for subgroup list [");
					boolean first = true;
					for (String s : subGroups)
					{
						if (first)
						{
							first = false;
						} else
						{
							msg.append(',');
						}
						msg.append(s);
					}
					msg.append("]");
				}
				logger.info(msg.toString());
				numRows = -1;
				status.logStatus(step, substep, processingGroup, Status.StatusCode.Complete, numRows);
				continue;
			}
			Map<Level, SurrogateKeyUpdate> keyUpdates = LoadWarehouseHelper.updateSurrogateKeysForDimensionTable(loadID, dt, l, stmt, loadedSet, lw, r);
			List<Level> updatedLevelList = new ArrayList<Level>();
			// Account for higher levels that have been updated
			for (DimensionUpdate du : updateList)
			{
				// See if higher level keys can be filled
				for (Level hl : keyUpdates.keySet())
				{
					SurrogateKeyUpdate sku = keyUpdates.get(hl);
					if (addHigherLevelSurrogateKeyUpdate(dtname, hl, sku, du))
					{
						updatedLevelList.add(hl);
					}
				}
			}
			for (DimensionUpdate du : updateList)
			{
				/*
				 * Process each insert
				 */
				if (!du.update)
				{
					StringBuilder insertColumns = new StringBuilder();
					StringBuilder expressionColumns = new StringBuilder();
					List<DimensionUpdateJoin> joinCondition = new ArrayList<DimensionUpdateJoin>();
					StringBuilder stagingTables = new StringBuilder();
					List<String> logicalNames = new ArrayList<String>();
					Set<String> insertSet = new HashSet<String>();
					String from = null;
					String where = null;
					stagingTablesProcessed.add(dconn.Schema+"."+du.st.Name);
					logger.info("Inserting new records into table " + dt.TableName + " from staging table " + du.st.Name);
					from = " FROM " + du.sourceTable + " B";
					if (du.distinctKeyFilter != null)
						from += " " + du.distinctKeyFilter;
					if (du.joinConditions.size() > 0)
					{
						for (DimensionUpdateJoin duj : du.joinConditions)
							joinCondition.add((DimensionUpdateJoin) duj.clone());
					}
					/*
					 * If there are other inserts for the same level, include those tables in this insert so that all
					 * columns are included in all inserts
					 */
					for (DimensionUpdate du2 : updateList)
					{
						// First, count the number of inserts
						if (du2 == du || du2.update)
							continue;
					}
					boolean notdistinct = du.notdistinct;
					/*
					 * Create a map of all colums that could be inserted. For non-level key columns that have multiple
					 * sources, use a COALESCE
					 */
					Map<String, List<DimensionUpdateColumn>> expressionMap = new HashMap<String, List<DimensionUpdateColumn>>();
					List<String> groupBy = new ArrayList<String>();
					List<DimensionUpdateColumn> dlist = new ArrayList<DimensionUpdateColumn>();
					Map<String, String> subqueryTables = new HashMap<String, String>();
					for (DimensionUpdate du2 : updateList)
					{
						if (du2 == du || du2.update)
							continue;
						else if (isIB && stagingTablesProcessed.contains(du2.sourceTable))
						{
						  logger.info(du2.sourceTable+ "will not be added in the join as records from it have already been loaded");
						  continue;
						}
						/*
						 * Make sure that this join isn't a duplicate of one in the main update - possibly because it
						 * was already included by way of a lookup
						 */
						boolean dupeJoin = false;
						for (DimensionUpdateJoin sduj : du.joinConditions)
						{
							if (sduj.tableName.equals(du2.sourceTable))
							{
								dupeJoin = true;
								break;
							}
						}
						if (dupeJoin)
							continue;
						DimensionTable dt2 = r.findDimensionTableAtLevel(du2.lk.Level.Hierarchy.DimensionName, du2.lk.Level.Name);
						/*
						 * When adding columns from other levels in this dimension, use the dimension table if these
						 * columns are at a higher level. This way, you can access all previous loads. If at the same
						 * level, you might be loading the dimension from multiple tables at the same time. If so, then
						 * use the staging table.
						 */
						Level sourceTableLevel = null;
						for (String[] l2 : du2.st.Levels)
						{
							if (l2[0].equals(du.lk.Level.Hierarchy.DimensionName))
							{
								sourceTableLevel = r.findDimensionLevel(l2[0], l2[1]);
								break;
							}
						}
						boolean higherLevel = dt.Level.equals(sourceTableLevel) && du2.lk.Level != sourceTableLevel
								&& du2.lk.Level.findLevel(sourceTableLevel) != null;
						String sourceName = higherLevel ? Database.getQualifiedTableName(dconn, dt2.TableSource.Tables[0].PhysicalName) : du2.sourceTable;
						int count = 0;
						Map<String, List<DimensionUpdateColumn>> tempMap = new HashMap<String, List<DimensionUpdateColumn>>();
						for (int i = 0; i < du2.columns.size(); i++)
						{
							boolean isBeingInsertedByDrivingTable = false;
							for (int j = 0; j < du.columns.size(); j++)
							{
								if (du.columns.get(j).column.equals(du2.columns.get(i).column))
								{
									isBeingInsertedByDrivingTable = true;
									break;
								}
							}
							if (isBeingInsertedByDrivingTable && !du2.columns.get(i).checksum)
								continue;
							List<DimensionUpdateColumn> expList = tempMap.get(du2.columns.get(i).column);
							boolean exists = expList != null;
							// Don't coalesce level keys or constants (unless the constant is in a checksum column)
							if (exists && (expList.get(0).levelKey || du2.columns.get(i).constant))
								continue;
							String expression = null;
							if (du2.columns.get(i).checksum)
							{
								if (higherLevel)
									continue;
								if (exists && expList.get(0).constant)
									expList.remove(0);
								if (isMonetDB)
								{
									int index = du.sourceTable.indexOf('.');
									expression = Util.replaceStr(du2.columns.get(i).expression, "B.", du2.sourceTable.substring(index + 1) + ".");
								} else
								{
									if(Util.startsWithAnAlias(du2.columns.get(i).expression,"B.")){
										expression = du2.columns.get(i).expression.replaceFirst("B.", du2.sourceTable + ".");
									}else{
										expression = du2.columns.get(i).expression;
									}
									expression = Util.replaceStr(expression, " B.", " " +sourceName + ".");
									expression = Util.replaceStr(expression, "=B.", "=" +sourceName + ".");
								}
							} else
							{
								if (isMonetDB)
								{
									int index = sourceName.indexOf('.');
									expression = Util.replaceStr(du2.columns.get(i).expression, "B.", sourceName.substring(index + 1) + ".");
								} else
								{
									if(Util.startsWithAnAlias(du2.columns.get(i).expression,"B.")){
										expression = du2.columns.get(i).expression.replaceFirst("B.", sourceName + ".");
									}else{
										expression = du2.columns.get(i).expression;
									}
									expression = Util.replaceStr(expression, " B.", " " +sourceName + ".");
									expression = Util.replaceStr(expression, "=B.", "=" +sourceName + ".");
								}
								
							}
							/*
							 * Replace any references to tables that will be masked by wrapping if not distinct
							 */
							if (du2.notdistinct)
							{
								for (DimensionUpdateJoin duj : du2.joinConditions)
								{
									expression = Util.replaceStr(expression, duj.tableName + ".", sourceName + ".");
									expression = Util.replaceStr(expression, duj.alias + ".", sourceName + ".");
								}
							}
							if (!exists)
							{
								expList = new ArrayList<DimensionUpdateColumn>();
								tempMap.put(du2.columns.get(i).column, expList);
							}
							DimensionUpdateColumn newduc = new DimensionUpdateColumn(du2.columns.get(i).columnName, du2.columns.get(i).column, expression);
							newduc.constant = du2.columns.get(i).constant;
							newduc.checksum = du2.columns.get(i).checksum;
							expList.add(newduc);
							dlist.add(du2.columns.get(i));
							// Don't count checksum columns
							if (!newduc.checksum)
								count++;
						}
						if (count > 0)
						{
							/*
							 * Copy over columns that should be added
							 */
							for (Entry<String, List<DimensionUpdateColumn>> en : tempMap.entrySet())
							{
								List<DimensionUpdateColumn> expList = expressionMap.get(en.getKey());
								if (expList == null)
								{
									expList = new ArrayList<DimensionUpdateColumn>();
									expressionMap.put(en.getKey(), expList);
								}
								for (DimensionUpdateColumn duc : en.getValue())
									expList.add(duc);
							}
							DimensionUpdateJoin duj = new DimensionUpdateJoin();
							duj.type = JoinType.LEFT_OUTER;
							duj.tableName = sourceName;
							if (du2.notdistinct && !DatabaseConnection.isDBTypeMemDB(dconn.DBType))
							{
								/*
								 * If it's not a distinct source, need to make it that way to prevent cross join
								 * explosion
								 */
								StringBuilder dusb = new StringBuilder("SELECT ");
								StringBuilder dugb = new StringBuilder();
								boolean first = true;
								boolean firstgb = true;
								for (int i = 0; i < du2.columns.size(); i++)
								{
									if (du2.columns.get(i).constant)
										continue;
									if (first)
										first = false;
									else
										dusb.append(',');
									boolean useAs = isMonetDB;
									String expression;
									if (du2.columns.get(i).checksum)
									{
										/*
										 * If using the dimension table, adjust checksum
										 */
										if (higherLevel)
											expression = sourceName + "." + du.st.getChecksumName(false, dconn);
										else
											expression = Util.replaceStr(du2.columns.get(i).expression, "B.", du2.sourceTable + ".");
									} else
									{
										expression = Util.replaceStr(du2.columns.get(i).expression, "B.", sourceName + ".");
									}
									if (du2.columns.get(i).levelKey)
									{
										dusb.append(expression + " " + (useAs ? "AS " : "") + du2.columns.get(i).column);
										if (firstgb)
											firstgb = false;
										else
											dugb.append(',');
										dugb.append(expression);
									} else if (du2.columns.get(i).checksum)
										dusb.append("MAX(" + expression + ") " + (useAs ? "AS " : "")
												+ (higherLevel ? du2.st.getChecksumName(false, dconn) : dt.getChecksumColumnName(dconn)));
									else
										dusb.append("MAX(" + expression + ") " + (useAs ? "AS " : "") + du2.columns.get(i).column);
								}
								String tname = sourceName;
								int index = sourceName.indexOf('.');
								if (index > 0)
									tname = sourceName.substring(index + 1);
								dusb.append(" FROM " + sourceName);
								if (du2.joinConditions != null && du2.joinConditions.size() > 0)
								{
									for (DimensionUpdateJoin duj2 : du2.joinConditions)
									{
										if (isMonetDB)
										{
											int pos = sourceName.indexOf('.');
											duj.condition = Util.replaceStr(duj2.condition, sourceName + ".", sourceName.substring(pos + 1) + ".");
										} else
											duj.condition = Util.replaceStr(duj2.condition, "B.", sourceName + ".");
										duj.tableName = Util.replaceStr(duj2.tableName, "B.", sourceName + ".");
										if (duj.type == JoinType.INNER)
											dusb.append(" INNER JOIN ");
										else if (duj.type == JoinType.LEFT_OUTER)
											dusb.append(" LEFT OUTER JOIN ");
										if (DatabaseConnection.isDBTypeMemDB(dconn.DBType))
											dusb.append("MAXONE ");
										if (duj2.alias != null)
										{
											dusb.append(duj2.tableName + " " + duj2.alias + " ON " + duj.condition);
										} else
										{
											dusb.append(duj.tableName + " ON " + duj.condition);
										}
									}
								}
								dusb.append(" GROUP BY " + dugb);
								subqueryTables.put(sourceName, tname);
								sourceName = tname;
								if (dconn.DBType == DatabaseType.MonetDB)
								{
									duj.tempTableNumber = numTempTables++;
									if (Database.tableExists(r, dconn, "temp" + duj.tempTableNumber, false))
									{
										stmt.executeUpdate("DROP TABLE " + dconn.Schema + ".temp" + duj.tempTableNumber);
									}
									String create = "CREATE TABLE " + dconn.Schema + ".temp" + duj.tempTableNumber + " AS " + dusb + " WITH DATA";
									int schindex = duj.tableName.indexOf('.');
									String sname = duj.tableName;
									if (schindex >= 0)
										sname = sname.substring(schindex + 1);
									create = Util.replaceStr(create, duj.tableName + ".", sname + ".");
									logger.debug(Query.getPrettyQuery(create));
									stmt.executeUpdate(create);
									duj.tableName = dconn.Schema + ".temp" + duj.tempTableNumber + " " + tname;
								} else
									duj.tableName = "(" + dusb.toString() + ")" + tname;
							}
							boolean first = true;
							StringBuilder condition = new StringBuilder();
							for (String s : du2.lk.ColumnNames)
							{
								if (!first)
									condition.append(" AND ");
								else
									first = false;
								StagingColumn sc2 = du2.st.findStagingColumn(s);
								String scName2 = sc2.getQualifiedPhysicalFormula(r, sourceName, du2.notdistinct, null, qm);
								StagingColumn sc = du.st.findStagingColumn(s);
								String scName = sc.getQualifiedPhysicalFormula(r, "B", du.notdistinct, null, qm);
								condition.append(scName + "=" + scName2);
							}
							if (du2.levelFilterCondition != null)
							{
								condition.append(" AND ");
								condition.append(du2.levelFilterCondition);
							}
							if (isMonetDB)
							{
								int pos = sourceName.indexOf('.');
								duj.condition = Util.replaceStr(condition.toString(), sourceName + ".", sourceName.substring(pos + 1) + ".");
							} else
								duj.condition = condition.toString();
							joinCondition.add(duj);
							if (!du2.notdistinct && du2.joinConditions != null && du2.joinConditions.size() > 0)
							{
								for (DimensionUpdateJoin duj2 : du2.joinConditions)
								{
									duj = new DimensionUpdateJoin();
									duj.type = duj2.type;
									if (isMonetDB)
									{
										int pos = sourceName.indexOf('.');
										duj.condition = Util.replaceStr(duj2.condition, "B.", sourceName.substring(pos + 1) + ".");
									} else
									{
										duj.condition = Util.replaceStr(duj2.condition, " B.", " " +sourceName + ".");
										duj.condition = Util.replaceStr(duj.condition, "=B.", "=" +sourceName + ".");
										if(Util.startsWithAnAlias(duj2.condition,"B.")){
											duj.condition = duj.condition.replaceFirst("B.", sourceName + ".");
										}
									}
									duj.tableName = Util.replaceStr(duj2.tableName, "B.", sourceName + ".");
									joinCondition.add(duj);
								}
							}
						}
					}
					if (notdistinct)
					{
						for (DimensionUpdateColumn duc : dlist)
							if (duc.levelKey && !groupBy.contains(duc.column))
								groupBy.add(duc.column);
					}
					for (int i = 0; i < du.columns.size(); i++)
					{
						List<DimensionUpdateColumn> expList = expressionMap.get(du.columns.get(i).column);
						if (expList == null)
						{
							expList = new ArrayList<DimensionUpdateColumn>();
							expressionMap.put(du.columns.get(i).column, expList);
						}
						if (du.columns.get(i).checksum && expList.size() > 0)
						{
							if (du.columns.get(i).constant)
								continue;
							if (expList.get(0).constant)
								expList.remove(0);
						}
						expList.add(du.columns.get(i));
						if (notdistinct && du.columns.get(i).levelKey)
							groupBy.add(du.columns.get(i).column);
					}
					Map<String, String> cmap = new HashMap<String, String>();
					int cksumColFromOtherStagingTableCount = 0;
					for (Entry<String, List<DimensionUpdateColumn>> en : expressionMap.entrySet())
					{
						if (insertColumns.length() > 0)
						{
							insertColumns.append(',');
							expressionColumns.append(',');
						}
						insertColumns.append(en.getKey());
						String expression = null;
						List<DimensionUpdateColumn> duclist = en.getValue();
						/*
						 * If updating a checksum in a type 2, use the retired version if current is zero
						 */
						if (en.getKey().endsWith("_CKSUM$") && duclist.size() == 2)
						{
							if (duclist.get(0).expression.equals("0") && duclist.get(1).expression.startsWith("R."))
							{
								// Swap values, still need a zero in case R value is null
								duclist.add(duclist.get(0));
								duclist.remove(0);
							}
						}
						if (duclist.size() > 1)
						{
							StringBuilder sb = new StringBuilder();
							sb.append("COALESCE(");
							for (int i = 0; i < duclist.size(); i++)
							{
								if (i > 0)
									sb.append(',');
								if ((isPADB || dconn.DBType == DatabaseType.Oracle) && i == duclist.size() - 1 && duclist.get(0).expression != null
										&& duclist.get(0).expression.equals("0") && duclist.get(i).expression != null && duclist.get(i).expression.equals("0"))
								{
									sb.append(String.valueOf(cksumColFromOtherStagingTableCount++));
								} else
								{
									sb.append(duclist.get(i).expression);
								}
							}
							sb.append(')');
							expression = sb.toString();
							cmap.put(en.getKey(), sb.toString());
						} else
						{
							if (isMonetDB)
								expression = Util.replaceStr(duclist.get(0).expression, dconn.Schema + ".", "");
							else
								expression = duclist.get(0).expression;
							cmap.put(en.getKey(), expression);
						}
						// XXX MAX(IFNULL(expression,0)) works in mysql (not supported in IB)
						// XXX IFNULL(MAX(expression), 0) does not work mysql, group issue
						if (notdistinct && !duclist.get(0).levelKey && !(duclist.size() == 1 && duclist.get(0).constant))
							expression = "MAX(" + expression + ")";
						if (dconn.DBType != DatabaseType.Infobright && dconn.DBType != DatabaseType.MonetDB && duclist.get(0).checksum)
							expression = dconn.getIsNullScalarFunction() + "(" + expression + "," + 
									((isPADB || dconn.DBType == DatabaseType.Oracle) && expression.equals("0") ? String.valueOf(cksumColFromOtherStagingTableCount++) : "0") + ")";
						/*
						 * Replace any table references where subqueries were used
						 */
						for (Entry<String, String> replacement : subqueryTables.entrySet())
						{
							expression = Util.replaceStr(expression, replacement.getKey() + ".", replacement.getValue() + ".");
						}
						expressionColumns.append(expression);
						insertSet.add(en.getKey());
					}
					if (du.where.length() > 0)
						where = " WHERE " + du.where;
					if (stagingTables.length() > 0)
						stagingTables.append('/');
					stagingTables.append(du.st.Name);
					logicalNames.addAll(du.logicalNames);
					dimUpdater.processUpdates(updateList, du, insertColumns, expressionColumns, insertSet, joinCondition, dt, notdistinct);
					List<List<DimensionUpdateJoin>> joinSet = new ArrayList<List<DimensionUpdateJoin>>();
					for (DimensionUpdateJoin duj : joinCondition)
					{
						// Prune any redundant joins
						boolean found = false;
						List<DimensionUpdateJoin> list = null;
						for (List<DimensionUpdateJoin> joinList : joinSet)
						{
							for (DimensionUpdateJoin duj2 : joinList)
							{
								if (duj.alias == null && duj2.alias == null && duj.tableName.equals(duj2.tableName))
								{
									found = true;
									break;
								}
								if (duj.alias != null && duj2.alias != null && duj.alias.equals(duj2.alias))
								{
									found = true;
									break;
								}
							}
							if (found)
							{
								list = joinList;
								break;
							}
						}
						if (list == null)
						{
							list = new ArrayList<DimensionUpdateJoin>();
							joinSet.add(list);
						}
						// Make sure condition is not a dupe
						boolean dupe = false;
						for (DimensionUpdateJoin sduj : list)
						{
							if (sduj.condition.equalsIgnoreCase(duj.condition))
							{
								dupe = true;
								break;
							}
						}
						if (!dupe)
							list.add(duj);
					}
					String lastTname = du.sourceTable;
					List<String> jcols = new ArrayList<String>();
					List<String> lastjcols = new ArrayList<String>();
					List<String> jnames = new ArrayList<String>();
					Map<String, String> namealiasmap = new HashMap<String, String>();
					int tempIndex = 0;
					if (isIB && USE_SEQUENTIAL_TEMP_TABLES)
					{
						/*
						 * Code added to handle the fact that Infobright doesn't deal well with multiple left-outer
						 * joins. Seems to only be able to handle one. This restructures the inserts into a series of
						 * temp tables. This code may be completely deleted once Infobright fixes the underlying issue
						 */
						boolean first = true;
						String tempName = dconn.Schema + ".temp" + tempIndex;
						ttnames.add("temp" + tempIndex);
						
						StringBuilder ljq = new StringBuilder("CREATE TABLE " + tempName + " " + (dconn.isIBLoadWarehouseUsingUTF8() ? "CHARACTER SET UTF8 " : "") + " SELECT ");
						for (List<DimensionUpdateColumn> duclist : expressionMap.values())
							for (DimensionUpdateColumn duc : duclist)
							{
								if (duc.expression.startsWith("B."))
								{
									String cname = "C" + tempIndex + "$" + duc.expression.substring(2);
									jcols.add("B." + cname);
									lastjcols.add("B." + cname);
									if (first)
										first = false;
									else
										ljq.append(',');
									ljq.append(duc.expression + " " + cname);
									jnames.add(cname);
									namealiasmap.put(duc.expression.substring(2), cname);
								}
							}
						ljq.append(" FROM " + lastTname + " B " + du.uniqueFilter);
						if (du.where != null && du.where.length() > 0)
							ljq.append(" WHERE " + du.where);
						lastTname = tempName;
						
						from = " FROM " + tempName + " B";
						
						String expressionString = new StringBuilder(Util.replaceStr(expressionColumns.toString(), ",B.", ",B.C" + tempIndex + "$")).toString();
						expressionString = Util.replaceStr(expressionString, "(B.", "(B.C" + tempIndex + "$");
						if(Util.startsWithAnAlias(expressionString, "B."))
						{
							expressionColumns = new StringBuilder(expressionString.replaceFirst("B.", "B.C" + tempIndex + "\\$"));
						}else{
							expressionColumns = new StringBuilder(expressionString);
						}
						
						where = Util.replaceStr(where, "B.", "B.C" + tempIndex + "$");
						du.uniqueFilter = Util.replaceStr(du.uniqueFilter, "B.", "B.C" + tempIndex + "$");
						for (Entry<String, String> en : cmap.entrySet())
						{
							en.setValue(Util.replaceStr(en.getValue(), "B.", "B.C" + tempIndex + "$"));
						}
						if ((du.uniqueTempDropList != null) && (du.uniqueTempDropList.size() > 0))
						{
							for(String utd : du.uniqueTempDropList)
							{
								stmt.executeUpdate(utd);
							}
						}
						if ((du.uniqueTempCreateList != null) && (du.uniqueTempCreateList.size() > 0))
						{
							for(String utc : du.uniqueTempCreateList)
							{
								logger.debug(Query.getPrettyQuery(utc));
								stmt.executeUpdate(utc);
							}
						}
						stmt.executeUpdate("DROP TABLE IF EXISTS " + tempName);
						// Create initial select clause
						long id = System.currentTimeMillis();
						logger.info("[QUERY:" + id + ":" + Query.getPrettyQuery(ljq) + "]");
						stmt.executeUpdate(ljq.toString());
						tempIndex++;
					}
					
					StringBuilder joinsb = new StringBuilder();
					Map<String, String> tempNameReplacements = new HashMap<String, String>();
					for (DimensionUpdateJoin duj : joinCondition)
					{
						// Use each join in the position of the last duplicate
						List<DimensionUpdateJoin> list = null;
						for (List<DimensionUpdateJoin> slist : joinSet)
						{
							if (slist.get(slist.size() - 1) == duj)
							{
								list = slist;
								break;
							}
						}
						if (list == null || list.isEmpty())
							continue;
						if (isIB && USE_SEQUENTIAL_TEMP_TABLES)
						{
							/*
							 * Code added to handle the fact that Infobright doesn't deal well with multiple left-outer
							 * joins. Seems to only be able to handle one. This restructures the inserts into a series
							 * of temp tables. This code may be completely deleted once Infobright fixes the underlying
							 * issue
							 */
							// Progressively create temp tables
							String tempName = dconn.Schema + ".temp" + tempIndex;
							ttnames.add("temp" + tempIndex);
							StringBuilder ljq = new StringBuilder("CREATE TABLE " + tempName + " "+ (dconn.isIBLoadWarehouseUsingUTF8() ? "CHARACTER SET UTF8 " : "") + " SELECT ");
							boolean first = true;
							stmt.executeUpdate("DROP TABLE IF EXISTS " + tempName);
							if(duj.alias != null && duj.alias.length() > 0 && duj.alias.equals("R"))
							{
								for (List<DimensionUpdateColumn> duclist : expressionMap.values())
								{
									for (DimensionUpdateColumn duc : duclist)
									{
										if (duc.expression.startsWith(duj.alias + "."))
										{
											String cname = "C" + tempIndex + "$" + duc.expression.substring(2);
											jcols.add(duc.expression);
											jnames.add(cname);
											namealiasmap.put(duc.expression.substring(2), cname);
										}
									}
								}
								expressionColumns = new StringBuilder(Util.replaceStr(expressionColumns.toString(), "R.", "B.C" + tempIndex + "$"));								
							}
							
							for (int c = 0; c < jcols.size(); c++)
							{
								if (first)
									first = false;
								else
									ljq.append(',');								
								String idx = "C" + (tempIndex) + "$";
								String idxWithAlias = "B.C" + (tempIndex) + "$";
								String cnameToLookup = jcols.get(c);
								if(cnameToLookup.startsWith(idx))
								{
									cnameToLookup = "C" + (tempIndex -1) + "$" + cnameToLookup.substring(idx.length());									
								}
								else if(cnameToLookup.startsWith(idxWithAlias))
								{
									cnameToLookup = "C" + (tempIndex -1) + "$" + cnameToLookup.substring(idxWithAlias.length());
								}
 								ljq.append((lastjcols.contains(cnameToLookup)) ?  jnames.get(c) :(jcols.get(c) + " " + jnames.get(c)));
							}
														
							for (Entry<String, String> mapEntry : namealiasmap.entrySet())
							{
								String key = mapEntry.getKey();
								if (duj.condition.startsWith("B."+key))
								{
									duj.condition = Util.replaceFirstStr(duj.condition, "B."+key, "B."+mapEntry.getValue());
								}
								if (duj.condition.indexOf(" B."+key)>=0)
								{
									duj.condition = Util.replaceStr(duj.condition, " B."+key, " B."+mapEntry.getValue());
								}
								if (duj.condition.indexOf("=B."+key)>=0)
								{
									duj.condition = Util.replaceStr(duj.condition, "=B."+key, "=B."+mapEntry.getValue());
								}								
							}							
						
							String dujtname = duj.tableName;
							String sstdujname = duj.tableName;
							int subqindex = dujtname.lastIndexOf(')');
							if (subqindex > 0)
							{
								// Due to Infobright bug which crashes server, persist subquery
								String tname = dconn.Schema + ".temp";
								ttnames.add("temp");
								StringBuilder subq = new StringBuilder("CREATE TABLE " + tname + " " + dujtname.substring(1, subqindex));
								if (lastTname!=null && lastTname.equals(dconn.Schema+".temp"+(tempIndex-1)))
								{
								  StringBuilder strInnerJoin = new StringBuilder();
								  String stName =dujtname.substring(subqindex + 1);
								  strInnerJoin.append(" INNER JOIN ");
								  strInnerJoin.append(lastTname).append(" B ON ");
								  strInnerJoin.append(duj.condition.replace(stName, dconn.Schema+"."+stName)).append(" ");
								  String subStrSearch = "FROM "+dconn.Schema+"."+stName;
								  int fromTableIndex = subq.indexOf(subStrSearch)+subStrSearch.length();
								  String mainQuery = subq.substring(0, fromTableIndex);
								  String restOfQuery = subq.substring(fromTableIndex+1);
								  subq = new StringBuilder();
								  subq.append(mainQuery);
								  subq.append(strInnerJoin);
								  subq.append(restOfQuery);
								}
								stmt.executeUpdate("DROP TABLE IF EXISTS " + tname);
								long id = System.currentTimeMillis();
								logger.info("[QUERY:" + id + ":" + Query.getPrettyQuery(subq) + "]");
								int rows = stmt.executeUpdate(subq.toString());
								logger.info("[ROWS:" + id + ":" + rows + "]");
								// Take into account subqueries
								dujtname = dujtname.substring(subqindex + 1);
								sstdujname = Database.getQualifiedTableName(dconn, dujtname);
								duj.condition = Util.replaceStr(duj.condition, dujtname, tname);
								duj.tableName = tname;
							}
							for (List<DimensionUpdateColumn> duclist : expressionMap.values())
								for (DimensionUpdateColumn duc : duclist)
								{
									String duname = sstdujname + ".";
									int subqindex2 = duname.lastIndexOf(')');
									if (subqindex2 > 0)
										duname = duname.substring(subqindex2 + 1);
									int index = duc.expression.indexOf(duname);
									if (index >= 0)
									{
										String cname = null;
										String colname = null;
										
										if (index == 0)
										{
											cname = "C" + tempIndex + "$" + duc.expression.substring(duname.length());
											colname = duc.expression.substring(duname.length());
										}
										else
										{
											cname = "C" + tempIndex + "$" + duc.column;
											colname = duc.column;
										}
										
										if (!jnames.contains(cname))
										{
										jcols.add("B." + cname);
										jnames.add(cname);
										
										namealiasmap.put(colname, cname);
										
										if (first)
											first = false;
										else
											ljq.append(',');
										ljq.append(Util.replaceStr(duc.expression, sstdujname, duj.tableName) + " " + cname);
									}
								}
								}
							// Make sure that any columns needed in the future for joins are added (i.e. level keys)
							StagingTable st = null;
							for (StagingTable sst : r.getStagingTables())
							{
								if (Database.getQualifiedTableName(dconn, sst.Name).equals(sstdujname))
								{
									st = sst;
									break;
								}
							}
							DimensionTable sdt = null;
							for (DimensionTable jdt : r.DimensionTables)
							{
								if (!dt.AutoGenerated)
									continue;
								if (Database.getQualifiedTableName(dconn, jdt.TableSource.Tables[0].PhysicalName).equals(sstdujname))
								{
									sdt = jdt;
									break;
								}
							}
							if (sdt != null || st != null)
							{
								List<String> pnamelist = new ArrayList<String>();
								if (st != null)
								{
									pnamelist.add(dt.getChecksumColumnName(dconn));
									List<StagingColumn> targetedStagingColumns = st.getStagingColumnList(r, l, dt, l, true);
									for (StagingColumn sc : targetedStagingColumns)
									{
										String pname = sc.getQualifiedPhysicalName(null, dconn);
										pnamelist.add(pname);
									}
								}
								if (sdt != null)
								{
									for (DimensionColumn dc : sdt.DimensionColumns)
									{
										if (!dc.Qualify)
											continue;
										String pname = dc.PhysicalName;
										int index = pname.indexOf('.');
										if (index > 0)
											pnamelist.add(pname.substring(index + 1));
									}
								}
								for (String pname : pnamelist)
								{
									for (DimensionUpdateJoin sduj : joinCondition)
									{
										boolean addcol = expressionColumns.indexOf("." + pname) >= 0;
										if (!addcol && sduj == duj)
											continue;
										addcol = addcol || sduj.condition.indexOf("." + pname) >= 0;
										if (addcol)
										{
											String cname = "C" + tempIndex + "$" + pname;
											if (jnames.contains(cname))
												continue;
											jcols.add("B." + cname);
											namealiasmap.put(pname, cname);
											jnames.add(cname);
											if (first)
												first = false;
											else
												ljq.append(',');
											ljq.append(pname + " " + cname);
										}
									}
								}
							}
							ljq.append(" FROM " + lastTname + " B");
							// Update for any other replacements previously made
							for (Entry<String, String> en : tempNameReplacements.entrySet())
							{
								expressionColumns = new StringBuilder(Util.replaceStr(expressionColumns.toString(), en.getKey(), en.getValue()));
								duj.condition = Util.replaceStr(duj.condition, en.getKey(), en.getValue());
							}
							boolean replaceTableNameWithAlias = false;
							if (sstdujname.startsWith(dconn.Schema + "."))
							{
								sstdujname = sstdujname.substring(dconn.Schema.length() + 1);
								replaceTableNameWithAlias = true;
							}
							tempNameReplacements.put(sstdujname + ".", "B.C" + tempIndex + "$");
							ljq.append((duj.type == JoinType.INNER ? " INNER JOIN " : " LEFT OUTER JOIN "
									+ (DatabaseConnection.isDBTypeMemDB(dconn.DBType) ? "MAXONE " : ""))
									+ duj.tableName + (duj.alias != null ? " " + duj.alias : "") + " ON " + duj.condition);
							lastTname = tempName;
							// Create initial select clause
							long id = System.currentTimeMillis();
							logger.info("[QUERY:" + id + ":" + Query.getPrettyQuery(ljq) + "]");
							try
							{
								stmt.executeUpdate(ljq.toString());
							} catch (Exception ex)
							{
								conn.close();
								conn = dconn.ConnectionPool.getConnection();
								stmt = conn.createStatement();
								stmt.executeUpdate("DROP TABLE IF EXISTS " + tempName);
								stmt.executeUpdate(ljq.toString());
							}
							String expressionColumnsStr = Util.replaceStr(expressionColumns.toString(), dujtname + ".", "B.C" + tempIndex + "$");
							
							
							if (replaceTableNameWithAlias)
								expressionColumnsStr = Util.replaceStr(expressionColumnsStr, sstdujname + ".", "B.C" + tempIndex + "$");
							expressionColumns = new StringBuilder(expressionColumnsStr);
							from = " FROM " + tempName + " B";
							tempIndex++;
							lastjcols = jcols;
						} else
						{
							if (duj.tableName != null)
								joinsb.append((duj.type == JoinType.INNER ? " INNER JOIN " : " LEFT OUTER JOIN ")
										+ (DatabaseConnection.isDBTypeMemDB(dconn.DBType) ? "MAXONE " : "") + duj.tableName
										+ (duj.alias != null ? " " + duj.alias : "") + " ON ");
							boolean first = true;
							for (DimensionUpdateJoin condition : list)
							{
								if (first)
									first = false;
								else
									joinsb.append(" OR ");
								if (isMonetDB)
									joinsb.append(Util.replaceStr(condition.condition, dconn.Schema + ".", ""));
								else
									joinsb.append(condition.condition);
							}
						}
					}
					StringBuilder sb = new StringBuilder();
					
					String tempSequenceName = null;
					if (dconn.useSequenceForIdentity())
					{
						/** Get Max Number for Sequence START WITH  **/
						String col = Database.getIdentityColumn(dtname);
						String setVariable = "SELECT " + dconn.getIsNullScalarFunction() + "(MAX(" + col + "), 0) FROM " + Database.getQualifiedTableName(dconn, dtname);
						logger.debug(setVariable);

						ResultSet rs = null;
						long maxValue = 0;
						try
						{
							rs = stmt.executeQuery(setVariable);
							if (rs.next())
							{
								maxValue = rs.getLong(1);
							}
						} finally
						{
							if (rs != null)
								rs.close();
						}
						
						/** Create Temporary Sequence **/
						tempSequenceName = Database.getQualifiedTableName(dconn, "SEQUENCE_" + System.currentTimeMillis());
						String strSequence = "CREATE SEQUENCE " + tempSequenceName  + " START WITH "+ (maxValue + 1);
						if (dconn.DBType == DatabaseType.Oracle)
							strSequence += " MINVALUE -1";
						logger.debug(strSequence);
						stmt.executeUpdate(strSequence);
						
						/** Execute INSERT Statement With sequence.nextval **/
						/* the S. triggers a SQL parsing error in Hana */
						sb.append("INSERT INTO " + Database.getQualifiedTableName(dconn, dtname) + " (" + insertColumns + "," + col + ") " +
								"SELECT " + ((dconn.DBType != DatabaseType.Hana) ? "S.*" : "*") +","+ tempSequenceName +".nextval FROM (SELECT " + expressionColumns);
					}
					else
					{
						sb.append("INSERT INTO " + Database.getQualifiedTableName(dconn, dtname) + " (" + insertColumns + ") SELECT " + expressionColumns);
					}
					
					
					sb.append(from + joinsb);
					
					if ((du.uniqueTempDropList != null) && (du.uniqueTempDropList.size() > 0) && !isIB)
					{
						for(String utd : du.uniqueTempDropList)
						{
							stmt.executeUpdate(utd);
						}
					}
					if ((du.uniqueTempCreateList != null) && (du.uniqueTempCreateList.size() > 0) && !isIB)
					{
						for(String utc : du.uniqueTempCreateList)
						{
							logger.debug(Query.getPrettyQuery(utc));
							stmt.executeUpdate(utc);
						}
					}
					if (du.uniqueFilter != null)
						sb.append(du.uniqueFilter);
					if (where != null)
						sb.append(where);
					if (notdistinct && groupBy.size() > 0)
					{
						/*
						 * Optimization to ensure fast loading of non-distinct aggregates on SQL Server
						 */
						if (du.st != null && du.st.SourceFile != null && DatabaseConnection.isDBTypeMSSQL(dconn.DBType))
						{
							SourceFile sf = r.findSourceFile(du.st.SourceFile);
							StringBuilder indexQuery = null;
							if (sf != null && sf.NumRows > LoadWarehouse.INDEX_OPTIMIZATION_ROW_LIMIT && groupBy.size() < 16)
							{									
								StringBuilder temp = new StringBuilder("");							
								for (String s : groupBy)
								{
									temp.append('_');
									temp.append(s);
								}
								String idxname = Database.getIndexName(dconn, "IDX_UPD_" + du.st.Name, temp.toString(),  
										 Database.MAX_SQL_SERVER_INDEX_NAME_LENGTH);
																								
								String qstname = Database.getQualifiedTableName(dconn, du.st.Name);
								if (Database.indexExists(dconn, du.st.Name, idxname))
								{
									logger.info("Performance index " + idxname + " for table " + du.st.Name + " already exists.");
									/*
									 * Put the entry in the temporary indices map so that it gets dropped at the end of
									 * load warehouse
									 */
									tempIndices.put(idxname, qstname);
								} else
								{
									indexQuery = new StringBuilder("CREATE NONCLUSTERED INDEX " + idxname + " ON " + qstname + " (");
									for (int i = 0; i < groupBy.size(); i++)
									{
										if (i > 0)
											indexQuery.append(',');
										indexQuery.append(groupBy.get(i));
									}
									indexQuery.append(')');
									logger.info("Creating performance index for table " + du.st.Name);
									logger.debug(Query.getPrettyQuery(indexQuery));
									try
									{
										stmt.executeUpdate(indexQuery.toString());
										tempIndices.put(idxname, qstname);
									}catch (Exception e)
									{
										logger.debug("Unable to create index " + idxname + " on " + du.st.Name);
										logger.error(e, e);
									} 									
								}
							}
						}
						sb.append(" GROUP BY ");
						for (int i = 0; i < groupBy.size(); i++)
						{
							if (i > 0)
								sb.append(',');
							sb.append(cmap.get(groupBy.get(i)));
						}
					}
					if (isMonetDB)
					{
						// Remove schema references (except in FROM clause)
						int index = du.sourceTable.indexOf('.');
						if (index > 0)
						{
							String s = Util.replaceStr(sb.toString(), du.sourceTable, du.sourceTable.substring(index + 1));
							s = Util.replaceStr(s, du.sourceTable.substring(index + 1) + " B", du.sourceTable + " B");
							sb = new StringBuilder(s);
						}
					}
					/*
					 * Drop indexes if it's a big insert
					 */
					List<DatabaseIndex> dilist = null;
					DatabaseIndex keyIndex = null;
					if (du.st.Size > LoadWarehouse.DROP_INDEX_LIMIT)
					{
						dilist = Database.getIndices(dconn, stmt, dconn.Schema, dtname);
						for (DatabaseIndex di : dilist)
						{
							/*
							 * If it's an index on the where not exists key - keep
							 */
							boolean isWhereNotExistsKey = false;
							if (du.lk.ColumnNames.length == di.columns.length)
							{
								boolean match = true;
								for (String s : du.lk.ColumnNames)
								{
									boolean exists = false;
									DimensionColumn dc = r.findDimensionColumn(h.DimensionName, s);
									if (dc != null && dc.Qualify)
									{
										for (String s2 : di.columns)
										{
											if (dc.PhysicalName.equals(dc.DimensionTable.TableSource.Tables[0].PhysicalName + "." + s2))
											{
												exists = true;
												break;
											}
										}
									}
									if (!exists)
									{
										match = false;
										break;
									}
								}
								if (match)
									isWhereNotExistsKey = true;
							}
							if (!isWhereNotExistsKey)
							{
								logger.info("Dropping index " + di.name + " for insert performance");
								Database.dropIndex(dconn, dconn.ConnectionPool.getConnection(), di.name, Database.getQualifiedTableName(dconn, dtname));
							} else
								keyIndex = di;
						}
					}
					// If MySQL create index on primary key
					if (dconn.DBType == DatabaseType.MYSQL)
					{
						LevelKey pkey = null;
						for (LevelKey slk : l.Keys)
							if (slk.SurrogateKey)
								continue;
							else
							{
								pkey = slk;
								break;
							}
						if (pkey != null)
							Database.createIndex(dconn, pkey, dt.TableSource.Tables[0].PhysicalName, false, false, dt, l, true);
					}
					
					if (dconn.useSequenceForIdentity())
					{
						if (dconn.DBType != DatabaseType.Hana)
							sb.append(") S");
						else
							sb.append(")");
					}
					long id = System.currentTimeMillis();
					logger.info("[QUERY:" + id + ":" + Query.getPrettyQuery(sb) + "]");
					int insertRows = 0;
					if (dconn.requiresPersistQueryToFileAndLoadIntoTable(r))
					{
						String sbSelect = sb.toString().substring(sb.toString().indexOf("SELECT"));
						insertRows = Database.persistQueryToFileAndLoadIntoTable(sbSelect, dt.TableSource.Tables[0].PhysicalName, 
								insertColumns.toString(), databasePath, false, dconn, stmt, s3credentials,null,null,r);						
					}
					else
					{
						insertRows = stmt.executeUpdate(sb.toString());												
					}
					long finish = System.currentTimeMillis();
					logger.info("[INSERTDT:" + id + ":" + loadID + ":" + dt.TableName + ":" + stagingTables + ":" + insertRows + ":" + logicalNames + ":"
							+ (finish - id) + ":" + processingGroup + "] " + insertRows + " rows inserted");
					numRows += insertRows;
					
					if (dconn.useSequenceForIdentity())
					{
						/** Drop Temporary Sequence **/
						String sqlDropSequence = "DROP SEQUENCE " + tempSequenceName;
						logger.debug(sqlDropSequence);
						stmt.executeUpdate(sqlDropSequence);	
					}
					else
					{
						Database.setIdentityColumns(dconn, stmt, dtname);
					}
					
					if (l.SCDType == 2)
					{
						if (r.getCurrentBuilderVersion() >= 20)
						{
							/*
							 * If we just retired some records for a type II, we need to fixup any records retired
							 * within the load (i.e. end date is before the start date)
							 */
							/*
							 * We will need to update any facts that contain surrogate keys pointing to the deleted
							 * records
							 */
							List<MeasureTable> mtlist = new ArrayList<MeasureTable>();
							for (MeasureTable mt : LoadWarehouseHelper.getGeneratedMeasureTables(r))
							{
								for (MeasureColumn mc : mt.MeasureColumns)
								{
									if (mc.ColumnName.equals(h.DimensionName + "." + l.getSurrogateKey().ColumnNames[0]))
									{
										mtlist.add(mt);
									}
								}
							}
							String pname = Util.replaceWithPhysicalString(l.getSurrogateKey().ColumnNames[0]) + dconn.getPhysicalSuffix();
							String mpname = Util.replaceWithPhysicalString(h.DimensionName) + dconn.getPhysicalSuffix()
									+ Util.replaceWithPhysicalString(l.getSurrogateKey().ColumnNames[0]) + dconn.getPhysicalSuffix();
							String mpnameForMeasureTable = Util.replaceWithPhysicalString(h.DimensionName)+"."+Util.replaceWithPhysicalString(l.getSurrogateKey().ColumnNames[0]);
							String tempName = null;
							long numRecs = 0;
							if (mtlist.size() > 0)
							{
								/*
								 * Create a temp table that maps old to new surrogate keys
								 */
								tempName = "temp" + System.currentTimeMillis();
								StringBuilder tempCreate = new StringBuilder("SELECT A." + pname + " OLDKEY,B." + pname + " NEWKEY FROM "
										+ Database.getQualifiedTableName(dconn, dtname) + " A INNER JOIN " + Database.getQualifiedTableName(dconn, dtname)
										+ " B ON ");
								LevelKey lk = l.getNaturalKey();
								boolean first = true;
								for(String s: lk.ColumnNames)
								{
									if (!first)
										tempCreate.append(" AND ");
									else
										first = false;
									tempCreate.append("A." + Util.replaceWithNewPhysicalString(s, dconn,r) + "=B." + Util.replaceWithNewPhysicalString(s, dconn,r));
								}
								tempCreate.append(" WHERE A.LOAD_ID="+ loadID +" AND  A.TYPEIIRETIRED=1 AND B.SCD_END_DATE IS NULL");
								Database.createTableAs(r, dconn, tempName, tempCreate.toString(), true);
								String countq = "SELECT COUNT(*) FROM " + Database.getQualifiedTableName(dconn, tempName);
								logger.info("[QUERY:" + System.currentTimeMillis() + ":" + Query.getPrettyQuery(countq) + "]");
								ResultSet rs = stmt.executeQuery(countq);
								if (rs.next())
									numRecs = rs.getLong(1);
							}
							if (numRecs > 0)
							{
								for (MeasureTable mt : mtlist)
								{
									StringBuilder factUpdate = new StringBuilder();
									if(dconn.useWatcomOracleUpdateSyntax())
									{
										String fullmtname = Database.getQualifiedTableName(dconn, mt.TableSource.Tables[0].PhysicalName);
										String fulltempname = Database.getQualifiedTableName(dconn, tempName);
										factUpdate.append("UPDATE "+ fullmtname);
										factUpdate.append(" INNER JOIN " + fulltempname + " ON " + fullmtname + "." + mpname + "=" + fulltempname + ".OLDKEY");
										factUpdate.append(" SET " + mpname + "=" + fulltempname + ".NEWKEY");
									}
									else if(isOracle)
									{
										MeasureColumn mc = mt.findColumn(mpnameForMeasureTable);
										if(mc!=null)
										{
											mpname = mc.PhysicalName;
											if(mpname.indexOf(".") >= 0)
											{
												mpname = mpname.substring(mpname.indexOf(".")+1);
											}
										}
										factUpdate.append("UPDATE "
												+ Database.getQualifiedTableName(dconn, mt.TableSource.Tables[0].PhysicalName)).append(  " A ");
										factUpdate.append(" SET " + mpname + "= ( SELECT B.NEWKEY");
										factUpdate.append(" FROM " ).append(Database.getQualifiedTableName(dconn, tempName) + " B WHERE A." + mpname + "=B.OLDKEY ) ");
										factUpdate.append(" WHERE EXISTS ( SELECT 1 FROM ").append(Database.getQualifiedTableName(dconn, tempName) + " B WHERE A." + mpname + "=B.OLDKEY ) ");
									}
									else
									{
										factUpdate.append("UPDATE "
												+ Database.getQualifiedTableName(dconn, mt.TableSource.Tables[0].PhysicalName));
										factUpdate.append(" SET " + mpname + "=B.NEWKEY ");
										factUpdate.append(" FROM " + Database.getQualifiedTableName(dconn, mt.TableSource.Tables[0].PhysicalName)
												+ " A INNER JOIN " + Database.getQualifiedTableName(dconn, tempName) + " B ON A." + mpname + "=B.OLDKEY");
									}
									logger.info("[QUERY:" + System.currentTimeMillis() + ":" + Query.getPrettyQuery(factUpdate) + "]");
									stmt.executeUpdate(factUpdate.toString());
								}
							}
							if (mtlist.size() > 0)
							{
								Database.clearCacheForConnection(dconn);
								Database.dropTable(r, dconn, tempName, true);
							}
							/*
							 * Remove any invalid SCD records that were retired in the middle of the load for prior
							 * loads
							 */
							String deleteq = "DELETE FROM " + Database.getQualifiedTableName(dconn, dtname) + " WHERE SCD_END_DATE<SCD_START_DATE AND LOAD_ID="
									+ loadID;
							logger.info("[QUERY:" + System.currentTimeMillis() + ":" + Query.getPrettyQuery(deleteq) + "]");
							stmt.executeUpdate(deleteq);
						}
						/*
						 * If this is a type II SCD where there is a specific SDC Date column, then we need to retire
						 * any records that we may have inserted where the start date is actually before that of other
						 * records (we may get historical records at a later date)
						 */
						StagingColumn scdDateColumn = du.st.getSCDDateColumn();
						if (scdDateColumn != null)
						{
							StringBuilder keyCols = new StringBuilder();
							StringBuilder joinCondition1 = new StringBuilder();
							StringBuilder joinCondition2 = new StringBuilder();
							/*
							 * Get surrogate key
							 */
							LevelKey joinKey = null;
							for (LevelKey slk : du.lk.Level.Keys)
							{
								if (slk.SurrogateKey)
								{
									joinKey = slk;
									break;
								}
							}
							if (joinKey == null)
								joinKey = du.lk;
							boolean first = true;
							for (String s : joinKey.ColumnNames)
							{
								if (!first)
								{
									keyCols.append(",");
									joinCondition2.append(" AND ");
								} else
									first = false;
								String col = Util.replaceWithNewPhysicalString(s, dconn,r);
								joinCondition2.append("D." + col + "=C." + col);
								keyCols.append("A." + col);
							}
							first = true;
							for (String s : du.lk.ColumnNames)
							{
								if (!first)
								{
									joinCondition1.append(" AND ");
								} else
									first = false;
								String col = Util.replaceWithNewPhysicalString(s, dconn,r);
								joinCondition1.append("A." + col + "=B." + col);
							}
							sb = new StringBuilder("UPDATE " + Database.getQualifiedTableName(dconn, dtname) + " SET SCD_END_DATE = C.New_End_Date"
									+ " FROM " + Database.getQualifiedTableName(dconn, dtname) + " D INNER JOIN (SELECT " + keyCols
									+ ",MIN(DATEADD(ms,-2,B.SCD_START_DATE)) New_End_Date FROM " + Database.getQualifiedTableName(dconn, dtname)
									+ " A INNER JOIN " + Database.getQualifiedTableName(dconn, dtname) + " B ON " + joinCondition1
									+ " AND B.SCD_START_DATE>A.SCD_START_DATE GROUP BY " + keyCols + ") C ");
							sb.append("ON " + joinCondition2 + " WHERE D.LOAD_ID=" + loadID + " AND D.SCD_END_DATE IS NULL");
							id = System.currentTimeMillis();
							logger.info("[QUERY:" + id + ":" + Query.getPrettyQuery(sb) + "]");
							int rows = stmt.executeUpdate(sb.toString());
							logger.info("[UPDSCDIIRETIRE:" + id + ":" + loadID + ":" + dt.TableName + ":" + rows + ":" + "]");
						}
					}
					if (dilist != null)
					{
						for (DatabaseIndex di : dilist)
						{
							if (di != keyIndex)
							{
								logger.info("Recreating index " + di.name + " for insert performance");
								Database.createIndex(dconn, stmt, di);
							}
						}
					}
				} else
				{
					/*
					 * Update if there is an insert from the same source table
					 */
					boolean found = false;
					for (DimensionUpdate sdu : updateList)
					{
						if (sdu != du && !sdu.update && sdu.sourceTable.equals(du.sourceTable))
						{
							if ((isIB && USE_SEQUENTIAL_TEMP_TABLES) || isOracle)
							{
								du.notdistinct = sdu.notdistinct;
							}
							found = true;
						}
					}
					/*
					 * If not found, see if this is a higher level table. In that case, it is possible that we have
					 * updated records at a higher level and changed some key relationships at that level. If we didn't
					 * have any inserts or updates at the lower level, then we would miss these. Say, for example,
					 * someone just uploaded an updated list of orders with customer IDs. Same order id and order
					 * details as before. The customer ID would not get updated on the order details table unless order
					 * details records were added as well - which is not guaranteed.
					 * Don't do this for Infobright since the compound grain key column won't exist on the dimension table.
					 */
					if (!isIB && !found && !du.sourceTableLevel.Name.equals(dt.Level) && du.sourceTableLevel.findLevel(dt.Level) != null)
					{
						found = true;
					}
					if (found)
					{
						for (DimensionUpdateIndex dui : du.indices)
						{
							Database.createIndex(dconn, dui.lk, dui.tableName, false, false, null, null, true,r);
						}
						logger.info("Updating table " + dt.PhysicalName + " from staging table " + du.st.Name);
						StringBuilder expressionColumns = new StringBuilder();
						Set<String> insertSet = new HashSet<String>();
						List<DimensionUpdateJoin> conditions = new ArrayList<DimensionUpdateJoin>();
						StringBuilder sbu = new StringBuilder("UPDATE ");
						ttnames.add("temp");
						StringBuilder select = new StringBuilder("CREATE TABLE " + dconn.Schema + ".temp SELECT ");
						if (isIB)
						{
							stmt.executeUpdate("DROP TABLE IF EXISTS " + dconn.Schema + ".temp");
						}
						if (isMonetDB)
						{
							if (Database.tableExists(r, dconn, "temp", false))
							{
								stmt.executeUpdate("DROP TABLE " + dconn.Schema + ".temp");
							}
						}
						StringBuilder wheresb = new StringBuilder();
						StringBuilder ibwheresb = null;
						StringBuilder ijoinsb = null;
						String dtCheckSumColumnName = dt.getChecksumColumnName(dconn);
						String stgCheckSumColumnName = du.st.getChecksumName(false, dconn);
						if (isIB && USE_TEMP_TABLES_FOR_DISTINCT)
						{
							select = new StringBuilder();
							ibwheresb = new StringBuilder();
							ijoinsb = new StringBuilder();
							LevelKey lk = null;
							for (LevelKey slk : du.sourceTableLevel.Keys)
							{
								if (!slk.SurrogateKey)
									lk = slk;
							}
							LevelKey sklk = null;
							for (LevelKey slk : du.sourceTableLevel.Keys)
							{
								if (slk.SurrogateKey)
									sklk = slk;
							}
							if (lk != null && sklk != null)
							{
								/*
								 * Create a temp table that has all the keys from staging table that also exist in dimension table but
								 * checksum values in staging vs. dim tables are different. This is done in 3 different steps below:
								 * 1. tempStgKeys - contains all distinct keys+chceksum, necessary to do distinct on keys+checksum since 
								 * if we are populating the dimension from a fact based data source, there could be multiple rows in the 
								 * staging table for each combination of key+checksum.
								 * 2. tempDimKeys - contains all distinct keys from staging table that also exist in dimension table. 
								 * This temp table is a subset of the temp table created in step 1. This table also has the checksum value
								 * from dimension table.
								 * 3. temp - Using a combination of left outer join and is null condition on the above two temp tables, we 
								 * figure out the distinct keys that exist both in staging table and in dimension table but the checksums
								 * are not matching for the same keys. This is the set of keys that we want to update from staging table to
								 * dimension table.
								 */
								String kc = "";
								String dimKc = "";
								String kcWhere = "";								
								String joincDimKeys = "";

								for (String s : lk.ColumnNames)
								{
									if (kc.length() > 0)
									{
										kc += ",";
										dimKc += ","; 
									}
									String kcol = Util.replaceWithNewPhysicalString(s, dconn,r);
									kc += "A." + kcol;
									dimKc += "B." + kcol;
									DimensionColumn dc = r.findDimensionColumn(lk.Level.Hierarchy.DimensionName, s);
									if (dc != null)
									{
										if (dc.UnknownValue != null && dc.UnknownValue.length() > 0)
										{
											if (kcWhere.length() > 0)
											{
												kcWhere += " AND ";
											} else
											{
												kcWhere += " WHERE ";
											}
											kcWhere += "B." + kcol + " <> ";
											if (dc.DataType.equals("Varchar") || dc.DataType.equals("Date") || dc.DataType.equals("DateTime"))
												kcWhere += "'" + dc.UnknownValue + "'";
											else
												kcWhere += dc.UnknownValue;
										}
									}				
									if (joincDimKeys.length() > 0)
									{
										joincDimKeys += " AND ";
									}
									joincDimKeys += "A." + kcol + "=B." + kcol;
								}
								if(l.SCDType == 2)
								{
									if (kcWhere.length() > 0)
									{
										kcWhere += " AND ";
									} else
									{
										kcWhere += " WHERE ";
									}
									kcWhere += "B.SCD_END_DATE IS NULL";
								}
								if (lk.ColumnNames.length > 1 && du.st.getCompoundKeys().size() > 0 && lk.isCompoundKeyRequired(r))
								{
									// Use a compound key if necessary
									if (kc.length() > 0)
									{
										kc += ",";
										dimKc += ",";
									}
									String ccol = lk.getCompoundKeyName(r, dconn);
									kc += "A." + ccol;
									dimKc += "B." + ccol;
									joincDimKeys = "A." + ccol + "=B." + ccol;
								}
								String joincKeysDiff = joincDimKeys + " AND A." + stgCheckSumColumnName +"=B." + dtCheckSumColumnName;
								String skcol = null;
								if (Util.containsNonAsciiCharacters(sklk.Level.Hierarchy.DimensionName) && r.getCurrentBuilderVersion()>=22)
								{
									skcol = Util.getSurrogateKeyColumnPhysicalName(sklk.ColumnNames[0], dt, false, dconn,r,DatabaseConnection.getIdentifierLength(dconn.DBType));									
								}
								else
								{
									skcol = Util.replaceWithNewPhysicalString(sklk.ColumnNames[0], dconn,r);
								}								
								String whereNull = "B." + dtCheckSumColumnName + " IS NULL";
								
								String drop = null;
								String createAs = null;
							
								drop = "DROP TABLE IF EXISTS " + dconn.Schema + ".tempStgKeys";
								logger.debug(Query.getPrettyQuery(drop));
								stmt.executeUpdate(drop);
								
								createAs = "CREATE TABLE " + dconn.Schema + ".tempStgKeys"  + " " + (dconn.isIBLoadWarehouseUsingUTF8() ? "CHARACTER SET UTF8 " : "") +" AS SELECT DISTINCT " + kc 
										+ ", A." + dtCheckSumColumnName + " FROM " + du.sourceTable + " A";
								logger.debug(Query.getPrettyQuery(createAs));
								stmt.executeUpdate(createAs);
								ttnames.add("tempStgKeys");
								drop = "DROP TABLE IF EXISTS " + dconn.Schema + ".tempDimKeys";
								logger.debug(Query.getPrettyQuery(drop));
								stmt.executeUpdate(drop);
								ttnames.add("tempDimKeys");
								
								createAs = "CREATE TABLE " + dconn.Schema + ".tempDimKeys" + " " + (dconn.isIBLoadWarehouseUsingUTF8() ? "CHARACTER SET UTF8 " : "") +" AS SELECT DISTINCT " + dimKc   
										+ ", B." + stgCheckSumColumnName + ", B." + skcol  
										+ " FROM " + dconn.Schema + ".tempStgKeys A INNER JOIN " + Database.getQualifiedTableName(dconn, dtname)
										+ " B ON " + joincDimKeys + (kcWhere.length() > 0 ? kcWhere : "");
								logger.debug(Query.getPrettyQuery(createAs));
								stmt.executeUpdate(createAs);
								drop = "DROP TABLE IF EXISTS " + dconn.Schema + ".temp";
								logger.debug(Query.getPrettyQuery(drop));
								stmt.executeUpdate(drop);
								
								createAs = "CREATE TABLE " + dconn.Schema + ".temp"+  " " + (dconn.isIBLoadWarehouseUsingUTF8() ? "CHARACTER SET UTF8 " : "") +" AS SELECT A.* "  
										+ " FROM " + dconn.Schema + ".tempDimKeys A LEFT OUTER JOIN " + dconn.Schema + ".tempStgKeys B"
										+ " ON " + joincKeysDiff + " WHERE " + whereNull;
								logger.debug(Query.getPrettyQuery(createAs));
								stmt.executeUpdate(createAs);
								ibwheresb.append("A." + skcol + " IN (SELECT " + skcol + " from " + dconn.Schema + ".temp)");
								ttnames.add("tempA");
								ijoinsb.append("CREATE TABLE " + dconn.Schema + ".tempA" + " " + (dconn.isIBLoadWarehouseUsingUTF8() ? "CHARACTER SET UTF8 " : "") +" AS SELECT " + (du.notdistinct ? "DISTINCT" : "") + " A.* FROM "
										+ Database.getQualifiedTableName(dconn, dtname) + " A INNER JOIN " + dconn.Schema + ".temp T ON A." + skcol + "=T."
										+ skcol);
								if (du.joinConditions.size() > 0)
									ijoinsb.append(" INNER JOIN " + dconn.Schema + "." + du.st.Name + " B ON " + du.joinConditions.get(0).condition);
								if(l.SCDType == 2){
									ijoinsb.append(" WHERE SCD_END_DATE IS NULL");
								}
								
							} else
								isIB = false;
						}
						if (!dconn.useWatcomOracleUpdateSyntax() && !isOracle && !isPADB)
						{
							sbu.append("A SET ");
						}
						if (isPADB)
						{
							sbu.append(Database.getQualifiedTableName(dconn, dtname)).append(" SET ");
						}
						String alias = dconn.useWatcomOracleUpdateSyntax() ? "A." : "";
						for (int i = 0; i < du.columns.size(); i++)
						{
							if (expressionColumns.length() > 0)
								expressionColumns.append(',');
							expressionColumns.append(alias + du.columns.get(i).column + "=" + du.columns.get(i).expression);
							insertSet.add(du.columns.get(i).column);
						}
						/**
						 * Include higher level keys to this update only if it is not a type 2 update. Type 2 updates
						 * are used for retiring old records only and we should not try to join with higher level keys
						 */
						if (du.scdType != 2)
						{
							dimUpdater.processUpdates(updateList, du, null, expressionColumns, insertSet, conditions, dt, du.notdistinct);
						}
						
						if (du.joinConditions.size() > 0)
						{
							for (DimensionUpdateJoin duj : du.joinConditions)
							{
								boolean dupe = false;
								/* Prevent duplicates across dimension updates - from multiple higher level tables that have the same keys*/
								for(DimensionUpdateJoin cond : conditions)
								{
									if ((duj.tableName != null) && (duj.tableName.equals(cond.tableName)) 
										&& ((duj.alias == null && cond.alias == null) || ((duj.alias != null) && (cond.alias != null) && (duj.alias.equals(cond.alias)))))
									{
										dupe = true;
										break;
									}
								}
								if(!dupe)
								{
									conditions.add(duj);
								}
							}
						}
						StringBuilder joins = buildJoins(dtname, du, conditions);
						if (du.checksumExpression != null)
						{
							if (du.checksumExpression.startsWith(" AND "))
								wheresb.append(du.checksumExpression.substring(5));
							else
								wheresb.append(du.checksumExpression);
						}
						if (du.where.length() > 0)
						{
							if (wheresb.length() > 0)
								wheresb.append(" AND ");
							wheresb.append(du.where);
						}
						if (du.typeIIClear != null)
						{
							// Clear any retired flags
							stmt.executeUpdate(du.typeIIClear);
							logger.debug(Query.getPrettyQuery(du.typeIIClear));
						}
						if (isMonetDB || isIB)
						{
							// MonetDB update
							select = new StringBuilder();
							StringBuilder create = new StringBuilder();
							List<String> tempCols = new ArrayList<String>();
							
							if (isMonetDB)
							{
								select.append("CREATE TABLE " + Database.getQualifiedTableName(dconn, "temp1") + " AS SELECT ");
							} else if (isIB)
							{
								ttnames.add("temp1");
								create = new StringBuilder("CREATE TABLE " + dconn.Schema + ".temp1 (");
								select = new StringBuilder("INSERT INTO " + dconn.Schema + ".temp1 SELECT ");
							}
							List<Object[]> collist = Database.getTableSchema(dconn, conn, null, dconn.Schema, dtname);
							StringBuilder groupByBuf = new StringBuilder();
							for (int i = 0; i < collist.size(); i++)
							{
								DimensionColumn dc = null;
								String dimColumnName = (String) collist.get(i)[0];
								for (DimensionColumn sdc : dt.DimensionColumns)
								{
									if (sdc.Qualify && sdc.PhysicalName.equalsIgnoreCase(dtname + "." + collist.get(i)[0]))
									{
										dc = sdc;
										break;
									}
								}
								DimensionUpdateColumn uc = null;
								if (dc != null)
								{
									for (DimensionUpdateColumn duc : du.columns)
									{
										if (duc.columnName != null && duc.columnName.equals(dc.ColumnName))
										{
											uc = duc;
											break;
										}
									}
								} else if (dimColumnName != null && dimColumnName.equalsIgnoreCase(stgCheckSumColumnName))
								{
									for (DimensionUpdateColumn duc : du.columns)
									{
										if (duc.checksum && dimColumnName.equals(duc.column))
										{
											uc = duc;
											break;
										}
									}
								}
								if (i > 0)
								{
									select.append(',');
									create.append(',');
								}
								if (uc != null)
								{
									if (isIB && USE_SEQUENTIAL_TEMP_TABLES && du.notdistinct)
									{
										select.append("MAX(");
									}
									select.append(uc.expression);
									if (isIB && USE_SEQUENTIAL_TEMP_TABLES && du.notdistinct)
									{
										select.append(")");
									}
								} else
								{
									select.append("A." + collist.get(i)[0]);
									if (isIB && USE_SEQUENTIAL_TEMP_TABLES && du.notdistinct)
									{
										if (groupByBuf.length() == 0)
										{
											groupByBuf.append(" GROUP BY ");
										} else
										{
											groupByBuf.append(", ");
										}
										groupByBuf.append("A." + collist.get(i)[0]);
									}
								}
								String type = collist.get(i)[2].toString();
								if (type.equalsIgnoreCase("varchar"))
								{
									type += ("(" + collist.get(i)[3].toString() + ") comment 'for_insert'");
								}
								else if (type.equalsIgnoreCase("biginit"))
								{
									type += (" comment 'for_insert'");
								}
								create.append(collist.get(i)[0] + " " + type);
								tempCols.add((String)collist.get(i)[0]);
							}
							select.append(" FROM " + joins);
							if (isIB && ijoinsb != null && ijoinsb.length() > 0)
							{
								select = new StringBuilder(Util.replaceStr(select.toString(), Database.getQualifiedTableName(dconn, dtname) + " A",
										dconn.Schema + ".tempA A"));
								stmt.executeUpdate("DROP TABLE IF EXISTS " + dconn.Schema + ".tempA");
								logger.debug(Query.getPrettyQuery(ijoinsb));
								stmt.executeUpdate(ijoinsb.toString());
							}
							if (l.SCDType == 2 && isIB)
							{
								select.append(" WHERE A.SCD_END_DATE IS NULL ");
							}
							if (groupByBuf.length() > 0)
							{
								select.append(groupByBuf);
							}
							if (!isIB && wheresb != null && wheresb.length() > 0)
								select.append(" WHERE " + wheresb);
							if (isMonetDB)
							{
								select.append(" WITH DATA");
								if (Database.tableExists(r, dconn, "temp1", false))
								{
									Database.dropTable(r, dconn, "temp1",true);
								}
							} else if (isIB)
							{
								stmt.executeUpdate("DROP TABLE IF EXISTS " + dconn.Schema + ".temp1");
								create.append(')');
								if(dconn.isIBLoadWarehouseUsingUTF8()) 
								{
									create.append(" CHARSET=utf8");
								}
								logger.debug(Query.getPrettyQuery(create));
								stmt.executeUpdate(create.toString());
							}
							// Remove schema references (except in FROM clause)
							int index = du.sourceTable.indexOf('.');
							if (index > 0)
							{
								String s = Util.replaceStr(select.toString(), du.sourceTable, du.sourceTable.substring(index + 1));
								s = Util.replaceStr(s, " JOIN " + du.sourceTable.substring(index + 1), " JOIN " + du.sourceTable);
								select = new StringBuilder(s);
							}
							logger.debug(Query.getPrettyQuery(select));
							stmt.executeUpdate(select.toString());
							
							LevelKey sk = du.sourceTableLevel.getSurrogateKey();
							if (sk != null)
							{
								int rows = -1;
								if (l.SCDType == 2 && isIB)
								{
									String updateQuery = "UPDATE " + dconn.Schema + ".temp1" + " SET SCD_END_DATE='" + scdEndDateStr + "'" 
											+ ((r.getCurrentBuilderVersion() >= 20) ? ", TYPEIIRETIRED=1" : "" ) + " WHERE SCD_END_DATE IS NULL";
									logger.debug(Query.getPrettyQuery(updateQuery));
									rows = stmt.executeUpdate(updateQuery);
									logger.debug(rows + " rows updated");
								}
								String pkey = null;
								if (Util.containsNonAsciiCharacters(sk.Level.Hierarchy.DimensionName) && r.getCurrentBuilderVersion() >= 22)
								{
									pkey = Util.getSurrogateKeyColumnPhysicalName(sk.ColumnNames[0], dt, false, dconn,r,DatabaseConnection.getIdentifierLength(dconn.DBType));
								}
								else
								{
									pkey = Util.replaceWithNewPhysicalString(sk.ColumnNames[0], dconn,r);
								}
								String delete = "DELETE FROM " + Database.getQualifiedTableName(dconn, dtname) + " WHERE " + pkey + " IN (SELECT " + pkey
										+ " FROM " + (isIB ? dconn.Schema + ".temp1" : Database.getQualifiedTableName(dconn, "temp1")) + ")" + ((l.SCDType == 2 && isIB) ? " AND SCD_END_DATE IS NULL":"");
								logger.debug(Query.getPrettyQuery(delete));
								rows = stmt.executeUpdate(delete);
								logger.debug(rows + " rows deleted");
								if (l.SCDType == 2 && isIB)
								{
									boolean first = true;
									StringBuilder levelKeyJoins = new StringBuilder();
									Set<String> levelKeyColumns = new HashSet<String>();
									String skColName = null;
									for (LevelKey lk : du.sourceTableLevel.Keys)
									{
										if (lk.SurrogateKey)
										{
											if(lk.ColumnNames.length == 1)
											{
												skColName = Util.replaceWithNewPhysicalString(lk.ColumnNames[0], dconn,r);
											}
											continue;
										}
										for (String s : lk.ColumnNames)
										{
											s = Util.replaceWithNewPhysicalString(s, dconn,r);
											if (first)
												first = false;
											else
												levelKeyJoins.append(" AND "); 
											levelKeyJoins.append("A." + s + "=" + "B." + s);
											levelKeyColumns.add(s);
										}
									}
									StringBuilder tCols = new StringBuilder();
									StringBuilder stCols = new StringBuilder();
									StringBuilder groupBy = new StringBuilder();
									first = true;
									List<String> colNames= new ArrayList<String>();
									List<String> colTypes= new ArrayList<String>();
									Database.getStagingTableMetaData(r, dconn, du.st,du.st.getStagingColumnMap(r), colNames, colTypes, null, null, null);
									Set<String> tColsSet = new HashSet<String>();
									for (String sc : colNames)
									{
										String cname = sc;
										if (sc.equalsIgnoreCase(dtCheckSumColumnName))
										{
											cname = du.st.getChecksumName(false, dconn);
										}
										if (tempCols.contains(cname))
										{
											if (first)
											{
												first = false;
											}
											else
											{
												tCols.append(", ");
												stCols.append(", ");
											}
											if(skColName != null && skColName.equals(sc))
											{
												stCols.append("NULL");
											}
											else if(du.notdistinct)
											{
												if(levelKeyColumns.contains(sc))
												{
													if (groupBy.length() == 0)
													{
														groupBy.append(" GROUP BY ");
													} else
													{
														groupBy.append(", ");
													}
													groupBy.append("A." + sc);
													stCols.append("A." + sc);
												}
												else
												{
													stCols.append("MAX(A." + sc + ")");
												}
											}
											else
											{
												stCols.append("A." + sc);
											}
											tCols.append(cname);
											tColsSet.add(cname);
										}
									}

									//Preserve column values from temp1 for the columns that are not targeted to this particular staging table
									if(collist != null && collist.size() > 0)
									{
										for(int colIdx = 0; colIdx < collist.size(); colIdx++)
										{
											Object cobj = collist.get(colIdx)[0];
											String cname = null;
											if(cobj != null)
											{
												cname = cobj.toString();
											}
											if(cname == null || cname.equals("SCD_START_DATE") || cname.equals("SCD_END_DATE") 
													|| cname.equals("TYPEIIRETIRED") || cname.equals("LOAD_ID") 
													|| tColsSet.contains(cname))
											{
												continue;
											}
											if (first)
											{
												first = false;
											}
											else
											{
												tCols.append(", ");
												stCols.append(", ");
											}
											
											if(skColName != null && skColName.equals(cname))
											{
												stCols.append("NULL");
											}
											else if(du.notdistinct)
											{
												if(levelKeyColumns.contains(cname))
												{
													if (groupBy.length() == 0)
													{
														groupBy.append(" GROUP BY ");
													} else
													{
														groupBy.append(", ");
													}
													groupBy.append("B." + cname);
													stCols.append("B." + cname);
												}
												else
												{
													stCols.append("MAX(B." + cname + ")");
												}
											}
											else
											{
												stCols.append("B." + cname);
											}
											tCols.append(cname);
										}
									}
									String insertQuery = "INSERT INTO " + dconn.Schema + ".temp1" + " (" + tCols + ", SCD_START_DATE, LOAD_ID) SELECT " + stCols + ", " 
									        + "'" + scdStartDateStr + "', " + loadID + " FROM " + du.sourceTable + " A " + " INNER JOIN " + dconn.Schema + ".temp1 B ON "
											+ levelKeyJoins.toString() + groupBy.toString();
									logger.debug(Query.getPrettyQuery(insertQuery));
									rows = stmt.executeUpdate(insertQuery);
									logger.debug(rows + " rows updated");
								}
							}
							String insert = "INSERT INTO " + Database.getQualifiedTableName(dconn, dtname) + " SELECT * FROM "
									+ (isIB ? dconn.Schema + ".temp1" : Database.getQualifiedTableName(dconn, "temp1"));
							logger.debug(Query.getPrettyQuery(insert));
							int rows = stmt.executeUpdate(insert);
							logger.debug(rows + " rows inserted");
							//TODO
							if (isIB)
							{
								if(l.SCDType == 2)
								{
									Database.setIdentityColumns(dconn, stmt, dtname);
								}																							
							}
						} else if (isOracle)
						{
							sbu.append(Database.getQualifiedTableName(dconn, dtname)).append(" A SET (");
							boolean first = true;
							for (DimensionUpdateColumn duc : du.columns)
							{
								if (!first)
									sbu.append(", ");
								first = false;
								sbu.append(duc.column);
							}
							StringBuilder existsSB = new StringBuilder();
							sbu.append(") = (SELECT ");
							existsSB.append("WHERE EXISTS (SELECT 1");
							first = true;
							for (DimensionUpdateColumn duc : du.columns)
							{
								if (!first)
									sbu.append(", ");
								
								first = false;
								sbu.append(duc.expression);
							}
							sbu.append(" FROM ").append(du.sourceTable).append(" B ");
							existsSB.append(" FROM ").append(du.sourceTable).append(" B ");
							for (DimensionUpdateJoin duj : conditions)
							{
								StringBuilder conditionSB = new StringBuilder();
								if (duj.tableName != null)
								{
									conditionSB.append(duj.type == JoinType.INNER ? "INNER JOIN " : "LEFT OUTER JOIN "
											+ (DatabaseConnection.isDBTypeMemDB(dconn.DBType) ? "MAXONE " : ""));
									conditionSB.append(duj.tableName + (duj.alias != null ? " " + duj.alias : "") + " ON ");
									conditionSB.append(duj.condition).append(" ");
								}
								sbu.append(conditionSB);
								existsSB.append(conditionSB);
							}
							for (DimensionUpdateJoin duj : conditions)
							{
								if (duj.tableName != null)
									continue;
								if (wheresb.length() > 0)
									wheresb.append(" AND ");
								wheresb.append(duj.condition);
							}
							if (wheresb.length() > 0)
							{
								sbu.append("WHERE").append(wheresb);
								if (du.notdistinct)
									sbu.append(" AND ROWNUM = 1");
								existsSB.append("WHERE").append(wheresb);
							}
							sbu.append(")");
							existsSB.append(")");
							sbu.append(existsSB);
							long id = System.currentTimeMillis();
							int rows = 0;
							logger.debug("[QUERY:" + id + ":" + Query.getPrettyQuery(sbu) + "]");
							rows = stmt.executeUpdate(sbu.toString());
							if (!conn.getAutoCommit())
								conn.commit();
							long finish = System.currentTimeMillis();
							logger.info("[UPDATEDT:" + id + ":" + loadID + ":" + dt.TableName + ":" + du.st.Name + ":" + rows + ":" + du.logicalNames + ":"
									+ (finish - id) + ":" + processingGroup + "] " + rows + " rows affected");
							numRows += rows;
						} else
						{
							// Non-MonetDB update
							if (dconn.useWatcomOracleUpdateSyntax())
								sbu.append(" SET ");
							sbu.append(expressionColumns);
							if (!dconn.useWatcomOracleUpdateSyntax())
							{
								sbu.append(" FROM ");
								sbu.append(joins);
							} else
							{
								sbu.insert("UPDATE ".length(), joins);
							}
							if (isIB && ibwheresb != null && ibwheresb.length() > 0)
								sbu.append(" WHERE " + ibwheresb);
							if (isPADB)
							{
								StringBuilder sb = new StringBuilder();
								boolean first = true;
								for (DimensionUpdateJoin duj : conditions)
								{
									// check if it is an inner join to target table, 
									// if so, append the join condition to where clause without table alias for target table
									if (!((duj.type == null || duj.type == JoinType.INNER) && duj.condition.contains("A.")))
										continue;
									if (first)
										first = false;
									else
										sb.append(" AND ");
									sb.append(duj.condition.replace("A.", Database.getQualifiedTableName(dconn, dtname) + "."));
								}
								if (sb.length() > 0)
								{
									if (wheresb != null && wheresb.length() > 0)
										wheresb.append(" AND ");
									wheresb.append(sb);
								}
							}
							if (!isIB && wheresb != null && wheresb.length() > 0)
								sbu.append(" WHERE " + wheresb);
							long id = System.currentTimeMillis();
							int rows = 0;
							logger.debug("[QUERY:" + id + ":" + Query.getPrettyQuery(sbu) + "]");
							rows = stmt.executeUpdate(sbu.toString());
							if (!conn.getAutoCommit())
								conn.commit();
							long finish = System.currentTimeMillis();
							logger.info("[UPDATEDT:" + id + ":" + loadID + ":" + dt.TableName + ":" + du.st.Name + ":" + rows + ":" + du.logicalNames + ":"
									+ (finish - id) + ":" + processingGroup + "] " + rows + " rows affected");
							numRows += rows;
						}
					}
				}
			}			
			for (Level ul : updatedLevelList)
				keyUpdates.remove(ul);
			lw.updateSurrogateKeys(dconn, conn, stmt, dtname, keyUpdates, loadID);
			// Build indices
			lwHelper.generateIndices(dt, dtname);
			if (dconn.DBType == DatabaseType.MSSQL2012ColumnStore)
			{
				List<String> colNames = new ArrayList<String>();
				for (DimensionColumn dc : dt.DimensionColumns)
				{
					if (!colNames.contains(Util.unQualifyPhysicalName(dc.PhysicalName)))
						colNames.add(Util.unQualifyPhysicalName(dc.PhysicalName));
				}
				Database.createColumnStoreIndex(dconn, dtname, colNames);
			}
			status.logStatus(step, substep, processingGroup, Status.StatusCode.Complete, numRows);
		}
	}
}
