/**
 * $Id: TimePeriod.java,v 1.4 2010-12-20 00:36:15 bpeters Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse;

import java.io.Serializable;

import org.jdom.Element;
import org.jdom.Namespace;

/**
 * @author Brad Peters
 * 
 */
public class TimePeriod implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int AGG_TYPE_NONE = 0;
	public static final int AGG_TYPE_TRAILING = 1;
	public static final int AGG_TYPE_TO_DATE = 2;
	public static final int PERIOD_DAY = 0;
	public static final int PERIOD_WEEK = 1;
	public static final int PERIOD_MONTH = 2;
	public static final int PERIOD_QUARTER = 3;
	public static final int PERIOD_HALF_YEAR = 4;
	public static final int PERIOD_YEAR = 5;
	public int AggregationType;
	public int AggregationPeriod;
	public int AggregationNumPeriods;
	public int ShiftAmount;
	public int ShiftPeriod;
	public boolean ShiftAgo;
	public String Prefix;

	public TimePeriod(Element e, Namespace ns)
	{
		AggregationType = Integer.valueOf(e.getChildText("AggregationType", ns));
		AggregationPeriod = Integer.valueOf(e.getChildText("AggregationPeriod", ns));
		AggregationNumPeriods = Integer.valueOf(e.getChildText("AggregationNumPeriods", ns));
		ShiftAmount = Integer.valueOf(e.getChildText("ShiftAmount", ns));
		ShiftPeriod = Integer.valueOf(e.getChildText("ShiftPeriod", ns));
		ShiftAgo = Boolean.valueOf(e.getChildText("ShiftAgo", ns));
		Prefix = e.getChildTextTrim("Prefix", ns);
	}
	
	public Element getTimePeriodElement(Namespace ns)
	{
		Element e = new Element("TimePeriod",ns);
		
		Element child = new Element("AggregationType",ns);
		child.setText(String.valueOf(AggregationType));
		e.addContent(child);
		
		child = new Element("AggregationPeriod",ns);
		child.setText(String.valueOf(AggregationPeriod));
		e.addContent(child);
		
		child = new Element("AggregationNumPeriods",ns);
		child.setText(String.valueOf(AggregationNumPeriods));
		e.addContent(child);
		
		child = new Element("ShiftAmount",ns);
		child.setText(String.valueOf(ShiftAmount));
		e.addContent(child);
		
		child = new Element("ShiftPeriod",ns);
		child.setText(String.valueOf(ShiftPeriod));
		e.addContent(child);
		
		child = new Element("ShiftAgo",ns);
		child.setText(String.valueOf(ShiftAgo));
		e.addContent(child);
		
		child = new Element("Prefix",ns);
		child.setText(Prefix);
		e.addContent(child);
		
		return e;
	}

	public String getPeriodString(int period)
	{
		switch (period)
		{
		case PERIOD_DAY:
			return ("Day");
		case PERIOD_WEEK:
			return ("Week");
		case PERIOD_MONTH:
			return ("Month");
		case PERIOD_QUARTER:
			return ("Quarter");
		case PERIOD_HALF_YEAR:
			return ("Half-year");
		case PERIOD_YEAR:
			return ("Year");
		}
		return (null);
	}

	public int getPeriod(String periodString)
	{
		if (periodString.startsWith("Day"))
			return PERIOD_DAY;
		else if (periodString.startsWith("Week"))
			return PERIOD_WEEK;
		else if (periodString.startsWith("Year"))
			return PERIOD_YEAR;
		else if (periodString.startsWith("Quarter"))
			return PERIOD_QUARTER;
		else if (periodString.startsWith("Half-year"))
			return PERIOD_HALF_YEAR;
		return (PERIOD_MONTH);
	}

	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		if (ShiftAmount != 0)
		{
			if (ShiftAmount > 1)
				sb.append(ShiftAmount + " " + getPeriodString(ShiftPeriod) + "s " + (ShiftAgo ? "Ago" : "Ahead"));
			else
				sb.append(getPeriodString(ShiftPeriod) + " " + (ShiftAgo ? "Ago" : "Ahead"));
		}
		if (AggregationType != AGG_TYPE_NONE)
		{
			if (sb.length() > 0)
				sb.append(' ');
			if (AggregationType == AGG_TYPE_TRAILING)
			{
				sb.append("Trailing " + AggregationNumPeriods + " " + getPeriodString(AggregationPeriod) + (AggregationNumPeriods > 1 ? "s" : ""));
			} else if (AggregationType == AGG_TYPE_TO_DATE)
			{
				sb.append(getPeriodString(AggregationPeriod) + " To-date");
			}
		}
		return (sb.toString());
	}
}
