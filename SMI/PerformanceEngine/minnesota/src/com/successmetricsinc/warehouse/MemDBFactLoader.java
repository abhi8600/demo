/**
 *
 * Copyright (C) 2007-2009 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.successmetricsinc.ExecuteSteps.Step;
import com.successmetricsinc.Repository;
import com.successmetricsinc.query.Database;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.MeasureTable;
import com.successmetricsinc.query.Query;
import com.successmetricsinc.query.Database.DatabaseType;

public class MemDBFactLoader extends FactLoader{

	private static Logger logger = Logger.getLogger(MemDBFactLoader.class);

	public MemDBFactLoader(Repository r, DatabaseConnection dconn,
			LoadWarehouse lw, Step step, List<StagingTable> stList) {
		super(r, dconn, lw, step, stList);
	}

	/*
	 * processes IncrementalSnapshotFact for Infobright Connection Infobright hangs to delete from table if it uses
	 * joins with itself. The workaround suggested by Infobright support is to create a temp table with all the
	 * deletekeys and use the same in subquery (IN clause)
	 */
	@Override
	protected void processForIncrementalSnapshotFact(Connection conn, MeasureTable mt, Set<String> mtKeys, Statement stmt, int loadID, StagingTable st,
			Set<String> logicalTableKeys,String processingGroup) throws SQLException
	{
		String qualifiedMeasureTableName = Database.getQualifiedTableName(dconn, mt.TableSource.Tables[0].PhysicalName);
		// first create temp table and populate it with the deletekeys for current load id
		StringBuilder keyCols = new StringBuilder();
		List<String> keyColsList = new ArrayList<String>();
		boolean firstKey = true;
		for (String mtk : mtKeys)
		{
			if (firstKey)
				firstKey = false;
			else
				keyCols.append(", ");
			if (mtk.contains("."))
				mtk = mtk.substring(mtk.lastIndexOf(".") + 1);
			keyCols.append(mtk);
			keyColsList.add(mtk);
		}
		boolean isSingleDeleteKey = false;
		if (mtKeys.size() == 1)
			isSingleDeleteKey = true;
		StringBuilder createTableSQL = new StringBuilder();
		long id = System.currentTimeMillis();
		String tempKeysTableName = dconn.Schema + ".temp" + id;
		createTableSQL.append("CREATE TABLE ").append(tempKeysTableName);
		createTableSQL.append(" AS SELECT ").append(keyCols.toString()).append(", MAX(LOAD_ID) AS 'LOAD_ID' FROM ").append(qualifiedMeasureTableName);
		createTableSQL.append(" GROUP BY ").append(keyCols.toString());
		Database.clearCacheForConnection(dconn);
		logger.debug(" Processing for incremental snapshot fact: delete rows loaded from previous runs for the same key combination present in current run");
		logger.info("Temp Table Query:" + id + ":" + Query.getPrettyQuery(createTableSQL));
		int rows = Database.retryExecuteUpdate(dconn, stmt, createTableSQL.toString());		
		logger.info(rows + " rows inserted into temp table " + tempKeysTableName);
		StringBuilder createTempFactTableSQL = new StringBuilder();
		long id2 = System.currentTimeMillis() + 1;
		String tempFactTableName = dconn.Schema + ".temp" + id2;
		createTempFactTableSQL.append("CREATE TABLE ").append(tempFactTableName);
		createTempFactTableSQL.append(" AS SELECT A.* FROM ").append(qualifiedMeasureTableName + " A ").append(" INNER JOIN ");
		createTempFactTableSQL.append(tempKeysTableName + " B").append(" ON ");
		if (isSingleDeleteKey)
			createTempFactTableSQL.append("A." + keyCols.toString() + "=B." + keyCols.toString());
		else
		{
			boolean first = true;
			for (String mtk : mtKeys)
			{
				if (first)
					first = false;
				else
					createTempFactTableSQL.append(" AND ");
				if (mtk.contains("."))
					mtk = mtk.substring(mtk.lastIndexOf(".") + 1);
				createTempFactTableSQL.append("A." + mtk + "=B." + mtk);
			}
		}
		createTempFactTableSQL.append(" AND A.LOAD_ID=B.LOAD_ID");
		Database.clearCacheForConnection(dconn);
		logger.info("Temp Fact Table Query:" + id2 + ":" + Query.getPrettyQuery(createTempFactTableSQL));
		rows = Database.retryExecuteUpdate(dconn, stmt, createTempFactTableSQL.toString());
		logger.info(rows + " rows inserted into temp fact table " + createTempFactTableSQL);
		StringBuilder insertTempFactTableSQL = new StringBuilder();
		insertTempFactTableSQL.append("INSERT INTO ").append(tempFactTableName);
		insertTempFactTableSQL.append(" SELECT * FROM ").append(qualifiedMeasureTableName);
		insertTempFactTableSQL.append(" WHERE ");
		if (isSingleDeleteKey)
			insertTempFactTableSQL.append(keyCols.toString()).append(" IS NULL");
		else
		{
			if(dconn.DBType == DatabaseType.Infobright)
			{
				insertTempFactTableSQL.append("concat(").append(keyCols.toString()).append(")").append(" IS NULL");
			}
			else if(dconn.DBType == DatabaseType.MemDB)
			{
				boolean firstKeyCol = true;
				for(String kc : keyColsList)
				{
					if(firstKeyCol)
					{
						firstKeyCol = false;
					}
					else
					{
						insertTempFactTableSQL.append(" OR ");
					}
					insertTempFactTableSQL.append("(" + kc + " IS NULL)");
				}
			}
		}
		logger.info("Insert Temp Fact Table Query:" + id2 + ":" + Query.getPrettyQuery(insertTempFactTableSQL));
		rows = stmt.executeUpdate(insertTempFactTableSQL.toString());
		logger.info(rows + " rows inserted for null keys into temp fact table " + insertTempFactTableSQL);
		if(dconn.DBType == DatabaseType.Infobright)
		{
			logger.debug("Dropping temp table " + tempKeysTableName);
			stmt.executeUpdate("DROP TABLE " + tempKeysTableName);
			logger.debug("Dropping fact table " + qualifiedMeasureTableName);
			stmt.executeUpdate("DROP TABLE " + qualifiedMeasureTableName);
			logger.debug("Renaming " + tempFactTableName + " TO " + qualifiedMeasureTableName);
			stmt.executeUpdate("RENAME TABLE " + tempFactTableName + " TO " + qualifiedMeasureTableName);
		}
		else if(dconn.DBType == DatabaseType.MemDB)
		{
			//Since rename table is not available in memdb (unlike IB), we will delete everything from the
			//current fact table, insert everything from temp fact table and then drop temp fact table.
			String deleteSQL = "DELETE FROM " + qualifiedMeasureTableName;
			logger.debug("Deleting from fact table " + qualifiedMeasureTableName);
			logger.debug(Query.getPrettyQuery(deleteSQL));
			stmt.executeUpdate(deleteSQL);
			
			String insertSQL = "INSERT INTO " + qualifiedMeasureTableName + " SELECT * FROM " + tempFactTableName;
			logger.debug("Inserting into fact table " + qualifiedMeasureTableName + " from temp table " + tempFactTableName);
			logger.debug(Query.getPrettyQuery(insertSQL));
			stmt.executeUpdate(insertSQL);
			
			logger.debug("Dropping temp table " + tempFactTableName);
			stmt.executeUpdate("DROP TABLE " + tempFactTableName);
		}
		if (!conn.getAutoCommit())
			conn.commit();
	}
	
}
