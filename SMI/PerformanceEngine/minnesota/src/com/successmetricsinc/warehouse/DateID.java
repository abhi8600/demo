/**
 * $Id: DateID.java,v 1.3 2010-11-30 02:25:35 bpeters Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
/**
 * Class to encapsulate type information for Date IDs
 */
package com.successmetricsinc.warehouse;

import java.util.Calendar;

public class DateID
{
	public static final String Prefix = "Date ID:";
	public static final int Day = 0;
	public static final int Week = 1;
	public static final int Month = 2;
	public static final int Quarter = 3;
	public static final int Halfyear = 4;
	public static final int Year = 5;
	public String SourceFileColumnName;
	public int Type;
	public int Index;
	public Integer UnknownValue;

	/**
	 * Set the type of this date ID based on the given staging column type
	 * 
	 * @param name
	 */
	public void setType(String columnType)
	{
		Type = getType(columnType);
	}

	/**
	 * Return the type of a column
	 * 
	 * @param columnType
	 * @return
	 */
	public static int getType(String columnType)
	{
		if (columnType.endsWith("Day"))
			return (Day);
		else if (columnType.endsWith("Week"))
			return (Week);
		else if (columnType.endsWith("Month"))
			return (Month);
		else if (columnType.endsWith("Quarter"))
			return (Quarter);
		else if (columnType.endsWith("Half-year"))
			return (Halfyear);
		else if (columnType.endsWith("Year"))
			return (Year);
		return (Month);
	}

	/**
	 * Return the date id for a given date value based on a time definition
	 * 
	 * @param td
	 * @param val
	 * @return
	 */
	public int getDateID(TimeDefinition td, Calendar dateVal)
	{
		switch (Type)
		{
		case Month:
			return (td.getMonthID(dateVal));
		case Day:
			return (td.getDayID(dateVal));
		case Week:
			return (td.getWeekID(dateVal));
		case Quarter:
			return (td.getQuarterID(dateVal));
		case Halfyear:
			return (td.getHalfYearID(dateVal));
		case Year:
			return (td.getYearID(dateVal));
		}
		return (td.getMonthID(dateVal));
	}
}