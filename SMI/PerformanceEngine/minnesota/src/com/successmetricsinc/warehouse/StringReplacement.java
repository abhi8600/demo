/**
 * $Id: StringReplacement.java,v 1.2 2008-02-01 22:21:19 ricks Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse;

/**
 * @author Brad Peters
 * 
 */
public interface StringReplacement
{
	public String getReplacedString(String result);
}
