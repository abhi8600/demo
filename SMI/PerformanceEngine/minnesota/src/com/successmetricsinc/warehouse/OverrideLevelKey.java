package com.successmetricsinc.warehouse;

import java.io.Serializable;

import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.Repository;

public class OverrideLevelKey implements Serializable 
{
  public String Dimension;
  public String Level;
  public String[] KeyColumns;

  public OverrideLevelKey(Element e, Namespace ns)
  {
    Dimension= e.getChildText("Dimension",ns);
    Level= e.getChildText("Level",ns);
    KeyColumns= Repository.getStringArrayChildren(e, "KeyColumns", ns);
  }
  
  public Element getOverrideLevelKeyElement(Namespace ns)
  {
    Element e = new Element("OverrideLevelKey",ns);
    
    Element child = new Element("Dimension",ns);
    child.setText(Dimension);
    e.addContent(child);
    
    child = new Element("Level",ns);
    child.setText(Level);
    e.addContent(child);
    
    if (KeyColumns!=null)
    {
      e.addContent(Repository.getStringArrayElement("KeyColumns", KeyColumns, ns));
    }
    
    return e;
  }
  
}
