/**
 * $Id: Bulkload.java,v 1.173 2012-11-22 12:04:46 BIRST\mjani Exp $
 *
 * Copyright (C) 2007-2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.zip.GZIPOutputStream;

import javax.crypto.SecretKey;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.successmetricsinc.ExecuteSteps;
import com.successmetricsinc.Repository;
import com.successmetricsinc.query.Database;
import com.successmetricsinc.query.Database.DatabaseType;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.DimensionTable;
import com.successmetricsinc.query.Level;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.util.AWSUtil;
import com.successmetricsinc.util.UnterminatedQuoteException;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.warehouse.SourceFile.FileFormatType;

/**
 * Class to handle bulk loading of data into staging tables
 */
public class Bulkload
{
	private static Logger logger = Logger.getLogger(Bulkload.class);
	private static final int DEBUG_LINES_PER_THREAD = 10;
	public static final int WRITE_QUEUE_SIZE = 10000;
	public static final int S3_UPLOAD_THREADS = 4;
	private static final long WARN_QUOTED_STRING_LENGTH =   200000;	// 200k, warn about potential unterminated string
	private static final long ERROR_QUOTED_STRING_LENGTH = 2000000;	// 2m, fail with unterminated string error
	private static final long ERROR_LINE_LENGTH = 10000000;	// 10m, fail with line too long
	public static final String END_TOKEN = "***birst end token***";
	public static final int HEARTBEAT_NUM_LINES = 1000000;			// log the count every HEARTBEAT_NUM_LINES lines
	public static final int CACHE_CLEAR_CHECK_NUM_LINES = 50000;	// check if we need to clear the cache every CACHE_CLEAR_CHECK_NUM_LINES lines
	private Repository r;
	private AtomicLong writecount = new AtomicLong(0);
	private AtomicLong warningCount = new AtomicLong(0);
	private AtomicLong errorCount = new AtomicLong(0);
	private boolean replaceWithNullOnFailureToParseValue;
	private boolean skipRecordOnFailureToParseValue;
	private AtomicLong numFailedRecords;
	private int numThresholdFailedRecords;
	private AtomicBoolean skipRunning;

	public static class ProcessResult
	{
		List<File> splitFiles;
		long numRowsProcessed;
		long numErrors;
		long numWarnings;
		long duration;
		long size;
	}	

	/*
	 * Need to put dates that don't include time after the ones that do - because otherwise it will match and lose the
	 * time piece
	 */
	/**
	 * Bulk load the staging tables from the given source file(s). This method does not update the checksums
	 * 
	 * @param r
	 * @param c
	 * @param path
	 * @param sf
	 * @param st
	 * @param scmap
	 * @param debug
	 * @return
	 * @throws Exception 
	 */
	public ProcessResult load(Repository r, DatabaseConnection dc, String path, String databasePath, SourceFile sf, StagingTable st,
			Map<DimensionTable, List<StagingColumn>> scmap, boolean debug, int numRows, StagingTableBulkLoadInfo stbl,
			AmazonS3 s3client, AWSCredentials s3credentials, SecretKey s3secretKey) throws Exception
	{
		this.r = r;
		if (dc.supportsBulkLoad())
		{
			boolean isPADB = DatabaseConnection.isDBTypeParAccel(dc.DBType);

			AWSUtil.BirstTransferManager tm = null;

			boolean loadedDirectlyFromScriptOutput = st.Script != null && st.Script.InputQuery != null && st.Script.InputQuery.trim().length() > 0;
			File pathf = new File(path);
			if (!pathf.exists() || !pathf.isDirectory())
			{
				logger.error("Directory \"" + path + "\" does not exist - staging table \"" + st.Name + "\" will not be loaded.");
				return null;
			}
			/*
			 * Delete all files with .format or .tmp extension that might have been lingering from previous run - this
			 * can happen if the load staging process was killed manually
			 */
			String deleteFormatFileSearchPattern = null;
			String deleteTmpFileSearchPattern = null;
			String sourcefileName = sf.FileName;
			List<SourceFile.BirstDataSource> sourceFiles = null;
			if (sourcefileName != null && DatabaseConnection.isDBTypeMySQL(dc.DBType) && loadedDirectlyFromScriptOutput && Util.containsNonAsciiCharacters(sourcefileName))
			{
				String cleanSourceFileName = sourcefileName;
				if (cleanSourceFileName.indexOf(".") >= 0)
				{
					cleanSourceFileName = cleanSourceFileName.substring(0, sourcefileName.indexOf("."));
				}
				cleanSourceFileName = sourcefileName.replace(cleanSourceFileName, st.Name);
				sourceFiles = sf.getPhysicalSourceFiles(path, loadedDirectlyFromScriptOutput, r, true, dc, cleanSourceFileName);
			}
			else
			{
				sourceFiles = sf.getPhysicalSourceFiles(path, loadedDirectlyFromScriptOutput, r, true, dc, null);
			}
			// Only delete files that have been updated (cache the tmp files for fast development)
			long sfmodified = sourceFiles.size() == 1 ? (sourceFiles.get(0)).lastModified() : Long.MAX_VALUE;
			if ((sf.SearchPattern != null) && (!sf.SearchPattern.trim().equals("")))
			{
				deleteFormatFileSearchPattern = sf.SearchPattern + ".format";
				deleteTmpFileSearchPattern = dc.getUploadFilename(sf.SearchPattern);
			} else
			{
				deleteFormatFileSearchPattern = sourcefileName + ".format";
				deleteTmpFileSearchPattern = dc.getUploadFilename(sourcefileName);
			}
			if(r.isPartitioned() && r.partitionConnection != null)
			{
				deleteFormatFileSearchPattern = r.partitionConnection + "_" + deleteFormatFileSearchPattern;
				deleteTmpFileSearchPattern = r.partitionConnection + "_" + deleteTmpFileSearchPattern;
			}
			logger.debug("Deleting old format files - " + deleteFormatFileSearchPattern);
			Util.deleteFiles(path, deleteFormatFileSearchPattern);
			if (!loadedDirectlyFromScriptOutput)
			{
				logger.debug("Deleting old tmp files - " + deleteTmpFileSearchPattern);
				Util.deleteFiles(path, deleteTmpFileSearchPattern);
			}
			if (sourceFiles == null || sourceFiles.isEmpty())
			{
				if (sf.ContinueIfMissing)
				{
					logger.info("Physical file for SourceFile \"" + sf.FileName + "\" does not exist - staging table \"" + st.Name
							+ "\" will not be loaded from this sourcefile.");
					logger.info("SourceFile \"" + sf.FileName + "\" is optional and therefore being ignored - staging table \"" + st.Name
							+ "\" will be skipped from loadstaging.");
				} else
				{
					logger.info("SourceFile \"" + sf.FileName + "\" does not exist - staging table \"" + st.Name
							+ "\" will not be loaded from this sourcefile.");
					return null;
				}
			}
			// Create the table
			boolean successCreatePhysicalStagingTable = Database.createPhysicalStagingTable(r, dc, st, scmap);
			if (!successCreatePhysicalStagingTable || sourceFiles == null || sourceFiles.isEmpty())
			{
				return null;
			}
			long numRowsProcessed = 0;
			long numErrors = 0;
			long numWarnings = 0;
			for (SourceFile.BirstDataSource file : sourceFiles)
			{
				String baseName = file.getName();							// source.txt
				String bcpFullFilename = file.getLocalAbsolutePath(path);	// local entity .../spaceId/data/source.txt{.format}
				String tempFullFilename = file.getLocalAbsolutePath(path);	// local entity .../spaceId/data/source.txt{.tmp{.gz}}
				if(r.isPartitioned() && r.partitionConnection != null)
				{
					String fname = file.getName();
					bcpFullFilename = bcpFullFilename.replace(fname, r.partitionConnection + "_" + fname);
					tempFullFilename = tempFullFilename.replace(fname, r.partitionConnection + "_" + fname);
				}
				if (DatabaseConnection.isDBTypeMySQL(dc.DBType) && Util.containsNonAsciiCharacters(baseName))
				{
					String filename = baseName;
					if (filename.indexOf(".") >= 0)
					{
						filename = filename.substring(0, filename.indexOf("."));
					}
					String tname = st.Name;
					bcpFullFilename = bcpFullFilename.replace(filename, tname);
					tempFullFilename = tempFullFilename.replace(filename, tname);
				}
				// Generate bcp file
				File bcpfile = new File(bcpFullFilename + ".format");

				// XXX should split out code that updates the maps/lists from this)
				int numbcpFileCols = bcpFileGen(bcpfile, sf, st, dc.isUnicodeDatabase(), stbl.dateIDColumns, stbl.scExpressions, stbl.sequenceNumberMap, stbl.dateIDCount,
						stbl.scExpressionCount, stbl.sequenceNumberCount, stbl.extraColumnMap, (r.getCurrentBuilderVersion() >= 10) ? scmap : new HashMap<DimensionTable, List<StagingColumn>>(), dc);
				ProcessResult pr = null;
				if (loadedDirectlyFromScriptOutput)
				{
					logger.info("Loading directly from script output - " + tempFullFilename);
					pr = new ProcessResult();
					pr.numRowsProcessed = sf.NumRowsWritten;
					pr.numErrors = sf.NumRowsError;
					pr.numWarnings = sf.NumRowsWarning;
					pr.splitFiles = new ArrayList<File>();
					pr.splitFiles = Util.getFileList(tempFullFilename); // all that match this root name.*, return List<File> // XXX only do the new stuff for redshift
				} else
				{
					String filename = dc.getUploadFilename(tempFullFilename); // adds on the .tmp and .tmp.gz (for ParAccel)
					File f = new File(filename);
					if (f.lastModified() < sfmodified) // tmp file vs potential s3
					{
						logger.info("Processing source file: " + sf.FileName + " (" + file.getLogName() + ")");

						if (file instanceof SourceFile.BirstS3Object || dc.DBType == DatabaseType.Redshift)
						{
							// XXX due to connection limits, try getting a new s3 client for each ETL script
							// https://forums.aws.amazon.com/thread.jspa?messageID=133405&#133405 (2012)
							// http://aws.amazon.com/articles/1904/ (2008)
							List<SecretKey> keys = new ArrayList<SecretKey>();
							s3client = AWSUtil.getS3Client(s3credentials, keys);							
							boolean encrypted = AWSUtil.isEncrypted(s3client);
							if (encrypted)
								s3secretKey = keys.get(0);
							if (dc.DBType == DatabaseType.Redshift)
								tm = new AWSUtil.BirstTransferManager(s3client, databasePath, S3_UPLOAD_THREADS, AWSUtil.getNumberOfSlices(dc));
							else
								
							tm = new AWSUtil.BirstTransferManager(s3client);
						}

						pr = processTempFile(r, file, tempFullFilename, pathf, sf, stbl, debug, numRows, sf.ForceNumColumns, sf.OnlyQuoteAfterSeparator, dc, scmap, warningCount, tm);
						if (pr == null)
						{
							logger.error("Preprocesessing problem, cannot generate tempfile for Bulk Loading " + sf.FileName);
							return (null);
						}
						if (pr.numRowsProcessed == 0)
						{
							logger.info("Source file \"" + sf.FileName
									+ "\" is empty - this is OK only if you are using a placeholder source file with no real data.");
						} else
						{
							logger.info("Successfully preprocessed source file: " + sf.FileName);
						}
					} else
					{
						// XXX does not work for Redshift, need the split files... might already be here.. need to enumerate
						logger.info("Not preprocessing source file (recent preprocessed version exists, development mode only): " + sf.FileName);
						pr = new ProcessResult();
						pr.numRowsProcessed = sf.NumRowsWritten;
						pr.numErrors = sf.NumRowsError;
						pr.numWarnings = sf.NumRowsWarning;
						pr.splitFiles = new ArrayList<File>();
						pr.splitFiles.add(f);
					}
				}
				numRowsProcessed += pr.numRowsProcessed;
				numErrors += pr.numErrors;
				numWarnings += pr.numWarnings;
				// Do a bulk insert
				try
				{
					String bulkInsert = null;
					String outputBulkInsert = null;
					String oracleExtDataDir = null;
					String oracleExtLogDir = null;
					String oracleExtTableName = null;
					File rootFile = null;
					if (pr.splitFiles.size() == 1)
						rootFile = pr.splitFiles.get(0);
					else
					{
						// strip off the last .#
						File first = pr.splitFiles.get(0);
						String name = first.getAbsolutePath();
						int pos = name.lastIndexOf('.');
						if (pos > -1)
							name = name.substring(0, pos);
						rootFile = new File(name);
					}
					String tempFileLocation = null;
					if (databasePath == null || (DatabaseConnection.isDBTypeMySQL(dc.DBType)))
						tempFileLocation = rootFile.getAbsolutePath();
					else
						tempFileLocation = databasePath + "\\" + rootFile.getName();
					tempFileLocation = Util.replaceStr(tempFileLocation, "'", "''");
					logger.debug("Bulk loading staging table " + st.Name + " from source file " + tempFileLocation);
					if (DatabaseConnection.isDBTypeMSSQL(dc.DBType))
					{
						String formatFileLocation = databasePath == null ? bcpfile.getAbsoluteFile().toString() : (databasePath + "\\" + bcpfile.getName());
						formatFileLocation = Util.replaceStr(formatFileLocation, "'", "''");
						bulkInsert = "BULK INSERT " + Database.getQualifiedTableName(dc, Util.replaceWithPhysicalString(st.Name)) + " FROM '"
								+ tempFileLocation + "' WITH (FORMATFILE='" + formatFileLocation + "',FIRSTROW=1,ROWS_PER_BATCH=" + numRowsProcessed
								+ (dc.isUnicodeDatabase() ? ",DATAFILETYPE='widechar'" : "") + ")";
					} else if (DatabaseConnection.isDBTypeMySQL(dc.DBType) || dc.DBType == Database.DatabaseType.MemDB || isPADB || dc.DBType == Database.DatabaseType.Oracle || dc.DBType == DatabaseType.Hana)
					{
						//Take care of any out of order columns appearing in the list. e.g. columns 
						//1,2,3,5,6,7,4 appearing in format file in that order.
						//We have to make sure that the values from temp file end up in the correct database 
						//column. So, build a map of table column names by source file column number for use later.
						Map<Integer, String> tableColsBySourceFieldNumber = new HashMap<Integer, String>();
						for(int i = 1; i <= numbcpFileCols; i++)
						{
							tableColsBySourceFieldNumber.put(i, "@var" + i);
						}
						SAXBuilder builder = new SAXBuilder();
						Document d = null;
						try
						{
							d = builder.build(bcpfile);
							Element root = d.getRootElement();
							@SuppressWarnings("rawtypes")
							List children = root.getChildren();
							@SuppressWarnings("rawtypes")
							Iterator iterator = children.iterator();
							while (iterator.hasNext())
							{
								Element child = (Element) iterator.next();
								String childName = child.getName();
								if ("ROW".equals(childName))
								{
									@SuppressWarnings("rawtypes")
									List l = child.getChildren();
									@SuppressWarnings("rawtypes")
									Iterator rowIterator = l.iterator();
									while (rowIterator.hasNext())
									{
										Element column = (Element) rowIterator.next();
										String sourceNumStr = column.getAttributeValue("SOURCE");
										int sourceFieldNum = -1;
										try
										{
											sourceFieldNum = Integer.parseInt(sourceNumStr);
										}
										catch (NumberFormatException nfe)
										{
											logger.debug("Invalid source column number in format file: "+ bcpfile.getName());
										}
										String col = column.getAttributeValue("NAME");
										StagingColumn sc = st.getStagingColumnForSourceFileColumn(col);
										String colName = null;
										if(sc != null)
										{
											colName = Util.replaceWithNewPhysicalString(sc.getPhysicalName(), dc,r);
										}
										else
										{
											if (col.endsWith("_CKSUM$"))
												colName = dc.getChecksumName(col);
											else
												colName = Util.replaceWithNewPhysicalString(col, dc,r);
										}
										if(colName != null && sourceFieldNum != -1)
										{
											if ((dc.DBType == DatabaseType.Infobright || dc.DBType == DatabaseType.MYSQL) && (colName.length() > 61)) 
												colName = "x" + Integer.toHexString(colName.hashCode());
											tableColsBySourceFieldNumber.put(sourceFieldNum, colName);
										}
									}
									break;
								}
							}
						} catch (JDOMException e)
						{
							logger.fatal(e.getMessage(), e);
						}
						StringBuilder cols = new StringBuilder();
						for(int i = 1; i <= numbcpFileCols; i ++)
						{
							String col = tableColsBySourceFieldNumber.get(i);
							if (cols.length() > 0)
								cols.append(',');
							cols.append(col);
						}
						if (dc.requiresTransactionID())
						{
							if (stbl.compoundKeys != null)
							{
								for (Level l : stbl.compoundKeys.keySet())
								{
									if (cols.length() > 0)
										cols.append(',');
									cols.append(l.getNaturalKey().getCompoundKeyName(r, dc));
								}
							}
							if (stbl.grainKeyNames != null && stbl.grainKeyNames.size() > 0)
							{
								for (String s : stbl.grainKeyNames)
								{
									if (cols.length() > 0)
										cols.append(',');
									cols.append(dc.getHashedIdentifier(s));
								}
							}
						}
						
						if (dc.DBType == Database.DatabaseType.MemDB)
							bulkInsert = "LOAD DATA INFILE '"
									+ tempFileLocation.replace('\\', '/')
									+ "' INTO TABLE "
									+ Database.getQualifiedTableName(dc, Util.replaceWithPhysicalString(st.Name) + " DELIMITED BY '|' "
											+ "TERMINATED BY '\\r\\n' "
											+ "ENCLOSED BY '\"'"  + " (" + cols + ")");
						else if (isPADB)
						{
							if (dc.DBType == Database.DatabaseType.ParAccel)
							{
								// not doing parallel load with ParAccel - the architecture only works well with direct attached storage, not with SAN
								String rejectLogDir = System.getProperty(ExecuteSteps.PADB_REJECT_LOG_DIR);
								bulkInsert = "COPY " + Database.getQualifiedTableName(dc, Util.replaceWithPhysicalString(st.Name)) + " (" + cols + ")"
										+ " FROM '"	+ Util.replaceStr(tempFileLocation, "\\", "/") + "'" 
										+ " WITH"
										+ (dc.supportsZippedDataForBulkLoading() ? " GZIP" : "")
										+ " DELIMITER AS '|'"
										+ " REMOVEQUOTES" 
										+ " DATEFORMAT AS 'YYYY-MM-DD'"
										+ " TIMEFORMAT AS 'YYYY-MM-DD HH24:MI:SS'"
										+ " ACCEPTANYDATE" 
										+ " IGNOREBLANKLINES"
										+ (rejectLogDir == null || rejectLogDir.isEmpty() ? "" : (" REJECTLOG '" + rejectLogDir + "'"))
										+ " ESCAPE";
							}
							else if (dc.DBType == Database.DatabaseType.Redshift)
							{
								// http://docs.aws.amazon.com/redshift/latest/dg/r_COPY.html
								// http://docs.aws.amazon.com/redshift/latest/dg/t_Loading_tables_with_the_COPY_command.html
								// http://docs.aws.amazon.com/redshift/latest/dg/t_Loading-data-from-S3.html
								// http://docs.aws.amazon.com/redshift/latest/dg/t_preparing-input-data.html
								//
								// note that S3 COPY time can be long and for small files that time for S3 setup can be larger than the time to actually do the COPY
								// - https://forums.aws.amazon.com/thread.jspa?threadID=128491&tstart=0
								//
								// note that the S3 bucket must be in the same region as the Redshift cluster
								//
								// dbloaddir in spaces.config file/table will be s3://bucketName (e.g., s3://customer_birst_data)
								// dbloaddir in space table will be s3://bucketName/spaceId
								// tempFileLocation will be s3://bucketName/spaceId/data/file.txt.tmp								
								String S3Location = AWSUtil.cleanupPath(tempFileLocation);
								
								String baseBulkInsert = AWSUtil.getCopyCommand(Database.getQualifiedTableName(dc, Util.replaceWithPhysicalString(st.Name)), cols.toString(), S3Location, s3client, false);
								String credentials = AWSUtil.getCopyCommandCredentials(s3credentials, s3secretKey);
								String dummyCredentials = AWSUtil.getCopyCommandDummyCredentials(s3secretKey);
								bulkInsert = baseBulkInsert + credentials;
								outputBulkInsert = baseBulkInsert + dummyCredentials;
							}
						}
						else if (dc.DBType == Database.DatabaseType.Oracle)
						{
							Connection conn = dc.ConnectionPool.getConnection();
							Statement stmt = null;
							try
							{
								stmt = conn.createStatement();
								//create external directory and grant user right to read from directory
								File tempFile = new File(tempFileLocation);
								String timeInMillis = String.valueOf(System.currentTimeMillis());
								oracleExtDataDir = "EXT_DATA_DIR_" + timeInMillis;
								oracleExtLogDir = "EXT_LOG_DIR_" + timeInMillis;
								String dataPath = tempFile.getParent().replace('\\', '/');
						        String logPath = tempFile.getParentFile().getParentFile().getPath() + File.separator + "logs";
							    logPath = logPath. replace('\\', '/');
								String createDirSQL = "CREATE OR REPLACE DIRECTORY " + oracleExtDataDir + " AS '" + dataPath + "'";
								logger.debug(createDirSQL);
								stmt.executeUpdate(createDirSQL);
								createDirSQL = "CREATE OR REPLACE DIRECTORY " + oracleExtLogDir + " AS '" + logPath + "'";
								logger.debug(createDirSQL);
								stmt.executeUpdate(createDirSQL);
								//create external table
								List<String> stagingColNames = new ArrayList<String>();
								List<String> stagingColTypes = new ArrayList<String>();
								Database.getStagingTableMetaData(r, dc, st, scmap, stagingColNames, stagingColTypes, null, null, null);
								String[] columnsInTempFile = cols.toString().split(",");
								List<String> colNames = new ArrayList<String>();
								List<String> colTypes = new ArrayList<String>();
								for (String col : columnsInTempFile)
								{
									if (stagingColNames.contains(col))
									{
										colNames.add(col);
										colTypes.add(stagingColTypes.get(stagingColNames.indexOf(col)));
									}
								}
								oracleExtTableName = "EXT_TABLE_" + timeInMillis;
								StringBuilder createExtTableSQL = Database.getCreateTableSQL(r, dc, oracleExtTableName, colNames, colTypes, null, null, false, null, null, null).append("\n");
								createExtTableSQL.append("ORGANIZATION EXTERNAL (").append("\n");
								createExtTableSQL.append("TYPE ORACLE_LOADER").append("\n");
								createExtTableSQL.append("DEFAULT DIRECTORY ").append(oracleExtDataDir).append("\n");
								createExtTableSQL.append("ACCESS PARAMETERS (").append("\n");
								createExtTableSQL.append("RECORDS DELIMITED BY '\\r\\r\\n' CHARACTERSET 'UTF16'").append("\n");						
								createExtTableSQL.append("BADFILE ").append(oracleExtLogDir).append(":'BAD_").append(sf.FileName).append(".bad'").append("\n");
								createExtTableSQL.append("LOGFILE ").append(oracleExtLogDir).append(":'LOG_").append(sf.FileName).append(".log'").append("\n");
								createExtTableSQL.append("FIELDS TERMINATED BY '|' OPTIONALLY ENCLOSED BY '\"'").append("\n");
								createExtTableSQL.append("MISSING FIELD VALUES ARE NULL").append(")").append("\n");
								createExtTableSQL.append("LOCATION ('").append(tempFile.getName()).append("')").append("\n");
								createExtTableSQL.append(")REJECT LIMIT 0").append("\n");
								logger.debug(createExtTableSQL.toString());
								stmt.executeUpdate(createExtTableSQL.toString());
								StringBuilder columnsSB = new StringBuilder();
								boolean first = true;
								for (String colName : colNames)
								{
									if (!first)
										columnsSB.append(", ");
									first = false;
									columnsSB.append(colName);
								}
								bulkInsert = "INSERT INTO " + Database.getQualifiedTableName(dc, Util.replaceWithPhysicalString(st.Name)) + "(" +
									columnsSB.toString() + ") (SELECT " + columnsSB.toString() + " FROM " + dc.Schema + "." + oracleExtTableName + ")";
							}
							finally
							{						
								if (stmt != null)
									stmt.close();
							}
						}
						else if (dc.DBType == Database.DatabaseType.Hana)
						{
							String errorLogFile = null; // XXX
							bulkInsert = "IMPORT FROM CSV FILE '"	+ Util.replaceStr(tempFileLocation, "\\", "/") + "' INTO " 
									+ Database.getQualifiedTableName(dc, Util.replaceWithPhysicalString(st.Name))
									+ " WITH"
									+ " COLUMN LIST (" + cols + ")"
									+ " RECORD DELIMITED BY '\\r\\n'"
									+ " FIELD DELIMITED BY '|'"
									+ " OPTIONALLY ENCLOSED BY '\"'"
									+ " DATE FORMAT 'YYYY-MM-DD'"
									+ " TIMESTAMP FORMAT 'YYYY-MM-DD HH24:MI:SS'"
									+ " THREADS 10"		// XXX initial recommendation from the documentation
									+ " BATCH 10000"	// XXX initial recommendation from the documentation
									+ " TABLE LOCK"		// faster loading
									+ (errorLogFile == null || errorLogFile.isEmpty() ? "" : (" ERROR LOG '" + errorLogFile + "'"));
						}
						else if (DatabaseConnection.isDBTypeMySQL(dc.DBType))
						{
							bulkInsert = "LOAD DATA LOCAL INFILE '"
									+ tempFileLocation.replace('\\', '/')
									+ "' INTO TABLE "
									+ Database.getQualifiedTableName(dc, Util.replaceWithPhysicalString(st.Name)) + " " +
									(dc.isIBLoadWarehouseUsingUTF8() ? "CHARACTER SET UTF8 " : "") +  
									"FIELDS TERMINATED BY '|' "
											+ (dc.DBType == DatabaseType.MYSQL ? "LINES TERMINATED BY '\\r\\n' " : "")
											+ (dc.DBType == DatabaseType.Infobright ? "ENCLOSED BY '\"'" : "") + " (" + cols + ")";
						}
					} else if (dc.DBType == DatabaseType.MonetDB) // XXX we really should remove MonetDB support, it hasn't been exercised in years, same for Excel and Siebel Analytics
					{
						bulkInsert = "COPY INTO " + Database.getQualifiedTableName(dc, Util.replaceWithPhysicalString(st.Name)) + " FROM '"
								+ tempFileLocation.replace('\\', '/') + "' USING DELIMITERS '|','\\r\\n','\"' NULL AS ''";
					}
					Connection conn = dc.ConnectionPool.getConnection();
					Statement stmt = null;
					int rows;
					ResultSet rs = null;
					try
					{
						
						stmt = conn.createStatement();
						if (dc.DBType == Database.DatabaseType.Infobright)
						{
							// force use of the MySQL loader - it supports column lists
							stmt.execute("set @bh_dataformat='mysql'");
						}
						logger.debug(outputBulkInsert != null ? outputBulkInsert : bulkInsert);
						rows = stmt.executeUpdate(bulkInsert);
					}
					catch(SQLException sqle)
					{
						if (!isPADB || sqle.getMessage() == null || 
								!sqle.getMessage().contains("Check 'stl_load_errors' system table for details."))
							throw sqle;

						// Extract load errors from stl_load_errors table into logfile for PADB and Redshift
						String sql = null;
						
						if (dc.DBType == Database.DatabaseType.ParAccel)
						{
							sql = "SELECT filename, starttime, line_number, field, position, err_reason, raw_line " +
									"FROM stl_load_errors " +
									"WHERE filename = '" + Util.replaceStr(tempFileLocation, "\\", "/") + "' " +
									"ORDER BY starttime DESC LIMIT 20";
							rs = stmt.executeQuery(sql);
							while (rs.next())
							{
								logger.error("Failed to bulk load " + Database.getQualifiedTableName(dc, Util.replaceWithPhysicalString(st.Name))); 
								logger.error(" - S3 Object: " + rs.getString("filename"));
								logger.error(" - Start Time: " + rs.getString("starttime"));
								logger.error(" - Line Number: " + rs.getString("line_number"));
								logger.error(" - Field: " + rs.getString("field"));
								logger.error(" - Position: " + rs.getString("position"));
								logger.error(" - Reason: " + rs.getString("err_reason"));
								logger.error(" - Raw Line: " + rs.getString("raw_line"));
							}
						}
						else if (dc.DBType == Database.DatabaseType.Redshift)
						{
							AWSUtil.dumpErrors(st.Name.toLowerCase(), Database.getQualifiedTableName(dc, Util.replaceWithPhysicalString(st.Name)), sqle, stmt);
						}
						throw sqle;
					}
					finally
					{
						// delete any S3 objects and split files that we created
						if (tm != null)
						{
							try
							{
								tm.cleanup();
							}
							catch (Exception ex)
							{
								logger.debug(ex, ex);
							}
						}
						//drop external table and remove external directory
						if (oracleExtTableName != null)
						{
							String dropTableSQL = "DROP TABLE " + dc.Schema + "." + oracleExtTableName;
							logger.debug(dropTableSQL);
							stmt.executeUpdate(dropTableSQL);
						}
						if (oracleExtDataDir != null)
						{
							String dropDirSQL = "DROP DIRECTORY " + oracleExtDataDir;
							logger.debug(dropDirSQL);
							stmt.executeUpdate(dropDirSQL);
						}
						if (oracleExtLogDir != null)
						{
							String dropDirSQL = "DROP DIRECTORY " + oracleExtLogDir;
							logger.debug(dropDirSQL);
							stmt.executeUpdate(dropDirSQL);
						}
						// close down the result set and statement
						if (rs != null)
							rs.close();
						if (stmt != null)
							stmt.close();
					}
					logger.info("Successfully loaded " + st.Name + (dc.DBType == Database.DatabaseType.Oracle ? " : Rows inserted=" + rows : ""));
					for (File fl : pr.splitFiles)
						Util.secureDelete(fl);
					if (bcpfile != null)
						Util.secureDelete(bcpfile);
				} catch (SQLException ex)
				{
					logger.error(ex.getMessage(), ex);
					logger.error(ex.getCause());
					return null;
				}
			}
			ProcessResult pr = new ProcessResult();
			pr.numRowsProcessed = numRowsProcessed;
			pr.numErrors = numErrors;
			pr.numWarnings = numWarnings;
			return pr;
		} else
		{
			logger.error("BULKLOAD is not supported for this database type");
			return null;
		}
	}

	/**
	 * Read a line from a buffered reader taking quotation marks into effect (allows newlines in fields that are quoted)
	 * 
	 * @return
	 * @throws UnterminatedQuoteException 
	 * @throws IOException 
	 */
	public static StringBuilder readLine(StringBuilder sb, SourceFile sf, char[] separr, BufferedReader br, boolean recognizeQuotes, int minSeparators, boolean onlyQuoteAfterSeparator)
			throws UnterminatedQuoteException, IOException
	{
		sb.setLength(0);
		int i = 0;
		boolean quote = false;
		boolean lastEscape = false;
		long startQuote = -1;
		boolean isPreviousCharQuote = false;
		long charCount = 0;
		int sepchar = 0;
		int numSeparators = 0;
		boolean lastSep = true;
		boolean isIgnoreBackslash = sf.isIgnoreBackslash();
		// char[] separr = sf.Separator.toCharArray();
		while ((i = br.read()) > -1)
		{
			char c = (char) i;
			charCount++;
			if (charCount > ERROR_LINE_LENGTH)
			{
				throw new UnterminatedQuoteException("too long line found in in file " + sf.FileName + " at line that begins with: " + sb.substring(0, 100));
			}
			if ((startQuote != -1) && quote && ((charCount - startQuote) > ERROR_QUOTED_STRING_LENGTH))
			{
				throw new UnterminatedQuoteException("unterminated quoted string in file " + sf.FileName + " (starting quote at position: " + startQuote + ") at line that begins with: " + sb.substring(0, 100));
			}
			if (isIgnoreBackslash && c == '\\')
				continue;
			if ((startQuote != -1) && quote && onlyQuoteAfterSeparator)
			{
				if (isPreviousCharQuote && (c == separr[sepchar]))//found separator after quote
				{
					startQuote = -1;
					quote = false;
				}
				else if (!sf.DoNotTreatCarriageReturnAsNewline && c == '\r')
				{
					if (sb.length() > 0)
					{
						if (minSeparators <= 0 || numSeparators >= minSeparators)
							return sb;
					}
				}
			}
			if (recognizeQuotes && (c == sf.Quote) && (!onlyQuoteAfterSeparator || lastSep || quote))
			{
				if (!lastEscape)
				{
					if (quote)
					{
						if ((charCount - startQuote) > WARN_QUOTED_STRING_LENGTH)
						{
							logger.warn("potentially unterminated quoted string in file " + sf.FileName + " (quoted string length: " + (charCount - startQuote) + ", starting quote at position: " + startQuote + ") at line that begins with: " + sb.substring(0, 100));
						}
						startQuote = -1;
					}
					else
					{
						startQuote = charCount;
					}
					quote = !quote;
				}
				sb.append(c);
			} else if (!quote)
			{
				if ((startQuote == -1) && (c == sf.Quote) && onlyQuoteAfterSeparator)
				{
					startQuote = charCount;
					quote = true;
				}
				if (minSeparators > 0 && c == separr[sepchar])
				{
					sepchar++;
					if (sepchar == separr.length)
					{
						sepchar = 0;
						lastSep = true;
						numSeparators++;
					}
				} else
				{
					sepchar = 0;
					lastSep = false;
				}
				if ((!sf.DoNotTreatCarriageReturnAsNewline && c == '\r') || c == '\n')
				{
					if (sb.length() > 0)
					{
						if (minSeparators <= 0 || numSeparators >= minSeparators)
							return sb;
						else
							sb.append(c);
					}
				} else
					sb.append(c);
			} else
				sb.append(c);
			if (c == '\\' && !lastEscape)
				lastEscape = true;
			else
				lastEscape = false;
			if (c == sf.Quote)
			{
				isPreviousCharQuote = true;					
			}
			else
			{
				isPreviousCharQuote = false;
			}
		}
		if (sb.length() > 0)
			return sb;
		return null;
	}
	
	/**
	 * required to keep a handle on the S3Client (AWS SDK bug)
	 * - see https://forums.aws.amazon.com/thread.jspa?messageID=438171
	 */
	public static class S3ObjectInputStreamWrapper extends FilterInputStream {
		 
	    @SuppressWarnings("unused")
	    private AmazonS3 client;
	 
	    // don't call this
	    protected S3ObjectInputStreamWrapper(InputStream inputStream) {
	        super(inputStream);
	    }
	    
	    public S3ObjectInputStreamWrapper(S3ObjectInputStream inputStream, AmazonS3 client) {
	        super(inputStream);
	        this.client = client;
	    }	    
	    // S3ObjectInputStream also implements abort() and getHttpRequest(). Override and delegate if needed.
	}

	/**
	 * Create a temp file based on a source file that is pre-processed to ensure proper delimiting and number/date
	 * formats
	 * 
	 * @param fileName
	 * @param pathFile
	 * @param sf
	 * @param dateIDColumns
	 * @param debug
	 * @return
	 * @throws Exception 
	 */
	private ProcessResult processTempFile(Repository r, SourceFile.BirstDataSource dataSource, String tempFileName, File pathFile, SourceFile sf, StagingTableBulkLoadInfo stbl, 
			boolean debug, int numRows, boolean forceMinColumns, boolean onlyQuoteAfterSeparator, DatabaseConnection dc, Map<DimensionTable, 
			List<StagingColumn>> scmap,	AtomicLong warningCount, AWSUtil.BirstTransferManager tm) throws Exception
	{
		File tmpfile = null;  
		String filename = dc.getUploadFilename(tempFileName);
		tmpfile= new File(filename);
		
		List<File> splitFiles = null;
		
		File sampleFile = null;
		long readcount = 0;
		AtomicLong filteredCount = null;
		// read the file in the proper character encoding, normally iso-8859-1
		// (US/Western Europe) or Unicode
		BufferedReader br = null;
		InputStreamReader is = null;
		S3ObjectInputStream sis = null;
		FileInputStream fis = null;
		S3Object s3Object = null;
		try
		{
			char[] seps = sf.Separator.toCharArray();
			boolean usingSample = false;
			long size = 0;
			
			if (dataSource instanceof SourceFile.BirstFile)
				sampleFile = new File(((SourceFile.BirstFile) dataSource).getAbsolutePath() + ".samplecache");
				
			if (sampleFile != null && sampleFile.exists())
			{
				fis = new FileInputStream(sampleFile.getAbsoluteFile());
				is = new InputStreamReader(fis, sf.getEncoding());
				br = new BufferedReader(is);
				usingSample = true;
			} 
			else
			{
				
				if (dataSource instanceof SourceFile.BirstS3Object)
				{
					// note that server-side encryption can be used - http://docs.aws.amazon.com/AmazonS3/latest/dev/UsingServerSideEncryption.html
					s3Object = tm.getClient().getObject(((SourceFile.BirstS3Object) dataSource).getObjectRequest());
					logger.debug("S3 object name: " + s3Object.getBucketName() + "/" + s3Object.getKey());
					String encoding = s3Object.getObjectMetadata().getContentEncoding();
					String type = s3Object.getObjectMetadata().getContentType();
					logger.debug("S3 object encoding: " + encoding);
					logger.debug("S3 object type: " + type);
					size = s3Object.getObjectMetadata().getContentLength();
					logger.debug("S3 object size: " + size);
					// XXX should look for .gz and do a gzip stream
					sis = s3Object.getObjectContent();
					if (encoding != null)
						is = new InputStreamReader(new S3ObjectInputStreamWrapper(sis, tm.getClient()), encoding);
					else
						is = new InputStreamReader(new S3ObjectInputStreamWrapper(sis, tm.getClient()));
				}
				else
				{
					size = ((SourceFile.BirstFile) dataSource).length();
					fis = new FileInputStream((SourceFile.BirstFile) dataSource);
					is = new InputStreamReader(fis, sf.getEncoding());
				}
				br = new BufferedReader(is);	
			}
			
			StringBuilder line = null;
			int numberOfProcessingThreads =  Math.max(1, Runtime.getRuntime().availableProcessors() - 1); // need one for writing, the read thread will be used for the processing
			StringBuilder sb = new StringBuilder(2000);
			/*
			 * read the lines at the top of the file that are to be ignored (and ignore them)
			 */
			for (int i = 0; i < sf.IgnoredRows + (sf.HasHeaders ? 1 : 0); i++)
				readLine(sb, sf, seps, br, sf.Type != FileFormatType.FixedWidth, forceMinColumns ? sf.Columns.length - 1 : 0, onlyQuoteAfterSeparator);
			/*
			 * create and start the thread the handles writing out the transformed source data
			 */
			if (!usingSample)
			{
				sampleFile = null;
				SampleSettings ss = Sample.getSettings(pathFile.getAbsolutePath());
				if (ss != null)
				{
					boolean found = false;
					for (SampleMap sm : ss.map)
					{
						if (!sm.StagingTableName.equals(stbl.st.Name))
							continue;
						found = true;
						break;
					}
					if (found)
					{
						if (dataSource instanceof SourceFile.BirstFile)
							sampleFile = new File(((SourceFile.BirstFile) dataSource).getAbsolutePath() + ".samplecache");
					}
				}
			}

			AtomicBoolean processing = new AtomicBoolean(true);
			LinkedBlockingQueue<String[]> writeQueue =  new LinkedBlockingQueue<String[]>(WRITE_QUEUE_SIZE);
			splitFiles = new ArrayList<File>();
			String fileName = tmpfile.getAbsolutePath();
			// should really be number of lines, but this is a good proxy, keep them atleast a minimum size
			long MinSizeOfSplitFile = TransformationScript.MINSIZEPERUNCOMPRESSEDSPLITFILE;
			int splits = 1;
			long sizePerFile = Long.MAX_VALUE;
			if (tm != null)
			{
				splits = tm.getNumberOfSplits();
				sizePerFile = Math.max(MinSizeOfSplitFile, (long) Math.ceil((double) size / (double) splits));
				sizePerFile = Math.min(sizePerFile, TransformationScript.MAXSIZEPERUNCOMPRESSEDSPLITFILE);
				logger.debug("Size per split file (uncompressed) " + sizePerFile);
			}
			writecount = new AtomicLong(0);

			BlockingQueue<Runnable> workQueue = new ArrayBlockingQueue<Runnable>(TransformationScript.WORK_QUEUE_SIZE);
			// core and max thread count the same, 0 timeout since max == core, fixed size work queue to
			// stop memory exhaustion, caller runs policy to stop rejection execution
			ExecutorService tp = (ExecutorService) new ThreadPoolExecutor(numberOfProcessingThreads, numberOfProcessingThreads, TransformationScript.EXECUTOR_TIMEOUT,
					TimeUnit.SECONDS, workQueue, new ThreadPoolExecutor.CallerRunsPolicy());

			AtomicBoolean stop = new AtomicBoolean(false);
			Thread monitor = new MonitorThread((ThreadPoolExecutor) tp, writeQueue, stop);
			monitor.start();

			WriteThread wt = new WriteThread(dc, r, writeQueue, fileName, splits, sizePerFile, splitFiles, sampleFile, sf, processing, writecount, tm);
			wt.start();

			Map<String, Set<String>> sampleMap = new HashMap<String, Set<String>>();
			filteredCount = new AtomicLong(0);
			replaceWithNullOnFailureToParseValue = sf.ReplaceWithNull;
			skipRecordOnFailureToParseValue = sf.SkipRecord;
			numThresholdFailedRecords = r.getNumThresholdFailedRecords();
			numFailedRecords = new AtomicLong(0);
			skipRunning = new AtomicBoolean(false);

			logger.info("Processing input records, number of threads: " + numberOfProcessingThreads + ", work queue size: " + TransformationScript.WORK_QUEUE_SIZE + ", write queue size: " + WRITE_QUEUE_SIZE);
			
			List<Exception> exceptionList = Collections.synchronizedList(new ArrayList<Exception>());
			LineProcessor lp = new LineProcessor(stbl, warningCount, errorCount, r, sf, stbl.st, scmap, pathFile, sampleMap, 
						replaceWithNullOnFailureToParseValue, skipRecordOnFailureToParseValue, numThresholdFailedRecords, numFailedRecords, skipRunning, dc);
			
			LinkedBlockingQueue<Line> last = null;
			/*
			 * set up a queue to handle processing rows at the end of the file that are to be ignored
			 */
			if (sf.IgnoredLastRows > 0)
			{
				last = new LinkedBlockingQueue<Line>(sf.IgnoredLastRows);
			}
			int lineno = 1;
			// read in the file and process it
			while ((line = readLine(sb, sf, seps, br, sf.Type != FileFormatType.FixedWidth, forceMinColumns ? sf.Columns.length - 1 : 0, onlyQuoteAfterSeparator)) != null)
			{
				if (exceptionList.size() > 0)
					throw (exceptionList.get(0));
				Line l = new Line();
				l.number = lineno++;
				
				if (numRows > 0 && l.number > numRows)
					break;
				
				readcount++;

				if ((l.number % CACHE_CLEAR_CHECK_NUM_LINES) == 0)
					lp.clearMaps();

				l.line = line.toString();
				
				if(last == null)
				{
					// XXX make a degenerate copy script? everything is a script?
					tp.execute(new ExecuteProcessRow(lp, l, exceptionList, writeQueue, sf, filteredCount, false));
				}else
				{
					if (!last.offer(l)) // handle ignoring the last N rows
					{						
						tp.execute(new ExecuteProcessRow(lp, last.remove(), exceptionList, writeQueue, sf, filteredCount, false));						
						last.put(l);
					}
				}
			}
			tp.shutdown();
			tp.awaitTermination(10, TimeUnit.MINUTES);

			// set up the termination condition for the write thread
			try {
				writeQueue.put(new String[] {END_TOKEN, END_TOKEN});
			} catch (InterruptedException e1) {
				logger.error(e1);
			}
			processing.set(false);
			stop.set(true);
			
			// wait for the write thread to finish
			try {
				wt.join();	
			} catch (InterruptedException e)
			{
				logger.error(e);
			}
			boolean filtered = sf.FilterColumn != null && sf.FilterColumn.length() > 0;
			if (readcount != writecount.get() && !filtered)
				logger.info("Read " + readcount + " lines, wrote " + writecount.get() + " lines");
			else
			{
				if (filtered)
					logger.info("Read " + readcount + " lines, wrote " + writecount.get() + " lines (column \"" + sf.FilterColumn + "\" filtered to value \""
							+ sf.FilterValue + "\")");
				else
					logger.info("Read " + readcount + " lines, wrote " + writecount.get() + " lines");
			}

		}
		catch (Exception e)
		{
			logger.debug(e, e);
			throw e;
		}
		finally
		{
			try
			{
				if (br != null)
					br.close();
				if (is != null)
					is.close();
				if (sis != null)
				{
					sis.abort();
					sis.close();
				}
				if (s3Object != null)
					s3Object.close();
				if (fis != null)
					fis.close();
			}
			catch(Exception e)
			{
				logger.error(e.getMessage(), e);
			}
		}
		if (skipRunning.get())
		{
			//Abort Bulk Loading
			logger.error("Maximum no. of failed records allowed for source exceeded, Bulk Load for " + sf.FileName + " aborted.");
			return null;
		}
		ProcessResult pr = new ProcessResult();
		pr.splitFiles = splitFiles;
		pr.numRowsProcessed = writecount.get();
		pr.numErrors = readcount - writecount.get() - filteredCount.get();
		pr.numWarnings = warningCount.get();
		return pr;
	}
	
	public static class MonitorThread extends Thread
	{
		final int MONITORTIME = 5 * 60 * 1000; // 5 minutes per logging message
		ThreadPoolExecutor executor;
		LinkedBlockingQueue<String[]> writeQueue;
		AtomicBoolean stop;
		
		public MonitorThread(ThreadPoolExecutor tp, LinkedBlockingQueue<String[]> writeQueue, AtomicBoolean stop)
		{
			this.executor = tp;
			this.writeQueue = writeQueue;
			this.stop = stop;
			this.setName("Monitor - " + this.getId());
			this.setDaemon(true);
		}
		
		public void run()
		{
	        try
	        {
	        	while (!stop.get())
	            {
	                logger.info(
	                    String.format("[%d/%d] Active (threads): %d, Completed: %d, Task: %d, Work Queue: %d/%d, Write Queue: %d/%d",
	                        this.executor.getPoolSize(),
	                        this.executor.getCorePoolSize(),
	                        this.executor.getActiveCount(),
	                        this.executor.getCompletedTaskCount(),
	                        this.executor.getTaskCount(),
	                        this.executor.getQueue().size(),
	                        this.executor.getQueue().size() + this.executor.getQueue().remainingCapacity(),
	                        this.writeQueue.size(),
	                        this.writeQueue.size() + this.writeQueue.remainingCapacity()));
	                Thread.sleep(MONITORTIME);
	            }
	        }
	        catch (Exception e)
	        {
	        }
		}
	}
	
	private static class ExecuteProcessRow implements Runnable
	{
		private List<Exception> exceptionList;
		private Line l;
		private LineProcessor lp;
		private LinkedBlockingQueue<String[]> writeQueue;	 
		private SourceFile sf;
		private AtomicLong filteredCount;
		
		boolean debug = false;
		int debugLines = 100;

		public ExecuteProcessRow(LineProcessor lp, Line l, List<Exception> exceptionList, LinkedBlockingQueue<String[]>  writeQueue, SourceFile sf, AtomicLong filteredCount, boolean debug)
		{
			this.lp = lp;
			this.l = l;
			this.exceptionList = exceptionList;
			this.writeQueue = writeQueue;
			this.sf = sf;
			this.filteredCount = filteredCount;
			this.debug = debug;
		}

		@Override
		public void run()
		{
			try
			{
				if (l != null && l.number == -1)
					return;
				
				LineProcessorResult lpr = null;
				lpr = lp.processLine(l);
				if (lp.skipRunning.get())
					throw new ScriptException("Maximum number of failed records allowed for source exceeded, aborting generating temp file for " + sf.FileName, -1, -1, -1);
				if (lpr != null && lpr.result != null)
				{
					/*
					 * Only debug unfiltered lines
					 */
					if (debug && debugLines < DEBUG_LINES_PER_THREAD)
					{
						for (int i = 0; i < lpr.pline.length; i++)
						{
							if (i < sf.Columns.length)
							{
								logger.debug(sf.Columns[i].Name + ": [" + lpr.pline[i] + "]");
							} else
							{
								logger.debug(lpr.pline[i]);
							}
						}
						debugLines++;
					}
					String rs = lpr.result.toString();
					String[] results = new String[2];
					results[0] = rs;
					results[1] = l.line;
					WriteThread.offerQueue(writeQueue, results);
				} else if (lpr != null && lpr.filtered)
				{
					filteredCount.incrementAndGet();
				} else if (lpr != null)
				{
					StringBuilder esb = new StringBuilder();
					for (int i = 0; i < lpr.pline.length; i++)
						esb.append('[' + lpr.pline[i] + ']');
					if (logger.isTraceEnabled())
						logger.trace(esb);
				}
			} catch (Exception e)
			{
				exceptionList.add(e);
			}
		}
	}
	
	public static class WriteThread extends Thread
	{
		static Map<String,Long> writeMap = new HashMap<String,Long>();
		static public boolean debug = false;

		LinkedBlockingQueue<String[]> writeQueue = null;
		AtomicBoolean running;
		FileOutputStream fos;
		GZIPOutputStream zos;
		OutputStreamWriter os;
		BufferedWriter bw;
		FileOutputStream sampleFos;
		OutputStreamWriter sampleOs;
		BufferedWriter sampleWriter;
		public boolean extraCarraigeReturn = false;
		private DatabaseConnection dc;
		String fileName;
		List<File> splitFiles;
		int numberOfSplits;
		long sizePerFile;
		AWSUtil.BirstTransferManager tm = null;
		Repository r;
		AtomicLong writecount;

		int count = 0;
		long currentFileSize = 0;
		
		public static boolean offerQueue(LinkedBlockingQueue<String[]> writeQueue, String[] results)
		{
			int retryCount = 0;
			
			if (debug)
			{
				String key = Thread.currentThread().getName();
				if (writeMap.containsKey(key))
				{
					long val = writeMap.get(key);
					writeMap.put(key, ++val);
				}
				else
					writeMap.put(key,  1L);
			}
			while (!writeQueue.offer(results))
			{
				try
				{
					Thread.sleep(500);
				}
				catch (InterruptedException ex)
				{}
				if (++retryCount > 1000)
				{
					logger.debug("write queue offer fail retry limit (1000 with 500ms sleep)");
					return false;
				}
			}
			return true;
		}

		public WriteThread(DatabaseConnection dc, Repository r, LinkedBlockingQueue<String[]> writeQueue, String fileName, int numberOfSplits, long sizePerFile, List<File> splitFiles, File sampleFile, SourceFile sf, AtomicBoolean running, AtomicLong writecount, AWSUtil.BirstTransferManager tm) throws IOException
		{
			if (debug)
				writeMap = new HashMap<String,Long>();

			this.setName("WriteThread - " + this.getId());
			this.r = r;
			this.writeQueue = writeQueue;
			this.running = running;
			this.dc = dc;
			this.fileName = fileName;
			this.splitFiles = splitFiles;
			this.numberOfSplits = numberOfSplits;
			this.sizePerFile = sizePerFile;
			this.writecount = writecount;
			setDaemon(true); // if the process wants to exit due to errors, let it
			if (dc.DBType == DatabaseType.Oracle)
				extraCarraigeReturn = true;
			
			// do not allow sample and multiple threads
			if (sampleFile != null && sf != null)
			{
				sampleFos = new FileOutputStream(sampleFile);
				sampleOs = new OutputStreamWriter(sampleFos, sf.getEncoding());
				sampleWriter = new BufferedWriter(sampleOs);						
			}

			// clean up old files
			List<File> files = Util.getFileList(fileName);
			if (files != null)
			{
				for (File fl : files)
				{
					if (fl.exists())
						fl.delete();
				}
			}
			
			this.tm = tm;
		}
		
		private void resetFile(boolean last) throws Exception
		{
			// close down the old file and upload (if it exists)
			if (bw != null)
				bw.close();
			if (os != null)
				os.close();
			if (zos != null)
				zos.close();
			if (fos != null)
				fos.close();
			
			int index = splitFiles.size() - 1;
			if (index >= 0 && tm != null)
			{
				File lastFile = splitFiles.get(index);
				tm.uploadFile(lastFile);
			}
			
			if (last)
			{
				if (tm != null)
					tm.waitForUploads();
				return;
			}

			currentFileSize = 0;
			String name = fileName;
			if (numberOfSplits > 1)
				name += "." + String.valueOf(++count);
			File fl = new File(name);
			if (fl.exists())
				fl.delete();
			splitFiles.add(fl);
			fos = new FileOutputStream(fl);
			logger.debug("writing: " + fl.getName());
			
			// compress if the DB supports it
			if (dc.supportsZippedDataForBulkLoading())
				zos = new GZIPOutputStream(fos);
			os = new OutputStreamWriter(((dc.supportsZippedDataForBulkLoading()) ? zos : fos), r.getDefaultConnection().getBulkLoadCharacterEncoding());
			bw = new BufferedWriter(os);
		}

		public void run()
		{
			try
			{
				resetFile(false);

				while (!writeQueue.isEmpty() || running.get())
				{
					String[] s = null;
					try
					{
						s = writeQueue.take();
					} catch (InterruptedException e1)
					{
						logger.error(e1);
					}
					if (s != null)
					{
						if (END_TOKEN.equals(s[0]))
						{
							break;
						}
						try
						{
							bw.write(s[0]);
							currentFileSize += s[0].length();
							if (extraCarraigeReturn)
							{
								bw.write((byte) '\r');
								currentFileSize++;
							}
							if (dc.DBType != DatabaseType.Infobright && !DatabaseConnection.isDBTypeParAccel(dc.DBType))
							{
								bw.write((byte) '\r');
								currentFileSize++;
							}
							bw.write((byte) '\n');
							currentFileSize++;
							writecount.incrementAndGet();
							
							long cnt = writecount.get();
							if (cnt % TransformationScript.LINES_PROCESSED_LOGGING_BATCH == 0)
								logger.info(cnt + " rows output");
							
							if (numberOfSplits > 1 && currentFileSize > sizePerFile)
								resetFile(false);
						} catch (IOException e)
						{
							logger.error(e);
							break;
						}
						if (sampleWriter != null)
						{
							try
							{
								sampleWriter.write(s[1]);
								sampleWriter.write((byte) '\r');
								sampleWriter.write((byte) '\n');
							} catch (IOException e)
							{
								logger.error(e);
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				logger.debug(ex, ex);
			}
			finally
			{
				try
				{
					resetFile(true);
					if (sampleWriter != null)
						sampleWriter.close();
					if (sampleOs != null)
						sampleOs.close();
					if (sampleFos != null)
						sampleFos.close();
				}
				catch (Exception ex)
				{
					logger.debug(ex, ex);
				}
			}
		}
	}

	/**
	 * Generate a bcp format file given a source file and staging table target
	 * 
	 * @param file
	 * @param sf
	 * @param st
	 * @throws FileNotFoundException
	 */
	private int bcpFileGen(File file, SourceFile sf, StagingTable st, boolean unicodeDatabase, List<DateID> dateIDColumns, List<String> scExpressions,
			Map<String, SequenceNumber> sequenceNumberMap, int dateIDCount, int scExpressionCount, int sequenceNumberCount,
			Map<Object, Integer> extraColumnMap, Map<DimensionTable, List<StagingColumn>> scmap, DatabaseConnection dc)			throws FileNotFoundException
	{
		String END_OF_LINE = "\\r\\n";
		String END_OF_LINE_UNICODE = "\\r\\0\\n\\0";
		String CHARTYPE = "CharTerm";
		String CHARTYPE_UNICODE = "NCharTerm";
		String sep = "|";
		// loop over the records - source file data fields
		Map<String, Integer> sourceFileColumns = new HashMap<String, Integer>();
		int length = sf.Columns.length + dateIDCount + scExpressionCount + sequenceNumberCount + scmap.size();

		PrintWriter writer = null;
		try
		{
			writer = new PrintWriter(new FileOutputStream(file));

			// write the preamble
			writer.write("<?xml version=\"1.0\"?>\n");
			writer.write("<BCPFORMAT");
			writer.write(" xmlns=\"http://schemas.microsoft.com/sqlserver/2004/bulkload/format\"");
			writer.write(" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n");
			writer.write("\t<RECORD>\n");
			for (int i = 0; i < length; i++)
			{
				SourceColumn sc = i < sf.Columns.length ? sf.Columns[i] : null;
				int index = i + 1;
				// assumes that they are sorted by position
				if (sc != null)
					sourceFileColumns.put(sc.Name, index);
				/*
				 * type is always 'CharTerm' for character files (NCharTerm for Unicode files)
				 */
				writer.write("\t\t<FIELD ID=\"" + index + "\" xsi:type=\"" + (unicodeDatabase ? CHARTYPE_UNICODE : CHARTYPE) + "\"");
				if (sc != null && sc.Width > 0)
				{
					writer.write(" MAX_LENGTH=\"" + (sc.Width * (unicodeDatabase ? 2 : 1)) + "\""); // length
					/*
					 * is measured in bytes here
					 */
				}
				// end of line
				if (i == length - 1)
				{
					writer.write(" TERMINATOR=\"" + (unicodeDatabase ? END_OF_LINE_UNICODE : END_OF_LINE) + "\"");
				} else
				{
					writer.write(" TERMINATOR=\"" + sep + (unicodeDatabase ? "\\0" : "") + "\"");
				}
				writer.write("/>\n");
			}
			writer.write("\t</RECORD>\n");
			// loop over the rows - staging table columns
			writer.write("\t<ROW>\n");
			int dateIDIndex = 0;
			int maxIndex = -1;
			for (StagingColumn stc : st.Columns)
			{
				if (!stc.isPersistable())
					continue;
				String precisionScale = "";
				String type = stc.DataType;
				String SourceFileColumnName = stc.SourceFileColumn;
				if (sf.isValidColumnExpression(SourceFileColumnName))
				{
					SourceFileColumnName = sf.replaceColumnNamesWithIndices(SourceFileColumnName);
					if (!type.startsWith(DateID.Prefix))
					{
						scExpressions.add(SourceFileColumnName);
					}
				}
				DateID di = null;
				// map to BCP type names
				if (type.equals("Varchar"))
				{
					type = (unicodeDatabase ? "SQLNVARCHAR" : "SQLVARYCHAR");
				} else if (type.equals("Integer"))
				{
					type = "SQLBIGINT";
				} else if (type.equals("Number"))
				{
					type = "SQLNUMERIC";
					precisionScale = " PRECISION=\"19\" SCALE=\"6\"";
				} else if (type.equals("DateTime") || type.equals("Date"))
				{
					type = "SQLDATETIM8";
				} else if (type.equals("Float"))
				{
					type = "SQLFLT8";
				} else if (type.startsWith(DateID.Prefix))
				{
					if (SourceFileColumnName == null || SourceFileColumnName.length() == 0)
						continue; // don't include ID columns that are not mapped
					if (dateIDIndex < dateIDColumns.size())
					{
						di = dateIDColumns.get(dateIDIndex);
						dateIDIndex++;
						type = "SQLINT";
					}
				} else if (type.equals("None"))
				{
					// Smallest datatype implicitly convertable into anything
					type = "SQLTINYINT";
				}
				Integer index = sourceFileColumns.get(SourceFileColumnName);
				SequenceNumber sn = null;
				if (index == null && !sf.isValidColumnExpression(SourceFileColumnName))
				{
					/*
					 * See if a sequence number is already available for this column
					 */
					sn = sequenceNumberMap.get(stc.Name);
					if (sn == null)
					{
						if (stc.SourceFileColumn != null)
						{
							/*
							 * Fix for 2071: if the physical column that is mentioned in the staging table then error out.
							 * If it is a column built using transformation, then the physical column would be null
							 */
							String msg = "Bulk load failed: Unable to find the source column:" + SourceFileColumnName + " in source file:" + sf.FileName
									+ ", which is used in staging table:" + st.Name;
							logger.error(msg);
							throw new UnsupportedOperationException(msg);
						} else
						{
							// skip column not in source file column list
							// (transformation column?) and is not a column
							// expression
							continue;
						}
					}
				}
				if (di != null || sf.isValidColumnExpression(SourceFileColumnName) || sn != null)
				{
					if (di != null)
						index = extraColumnMap.get(di);
					else if (sn != null)
						index = extraColumnMap.get(sn);
					else
						index = extraColumnMap.get(SourceFileColumnName);
					// Add 1 to index because format file column number starts from 1 as opposed to 0 in java data
					// structures.
					index++;
				}
				writer.write("\t\t<COLUMN SOURCE=\"" + index + "\" NAME=\"" + Util.replaceWithPhysicalString(stc.getPhysicalName(),r) + "\" xsi:type=\"" + type + "\""
						+ precisionScale + "/>\n");
				if (index > maxIndex)
					maxIndex = index;
			}
			if (r.getCurrentBuilderVersion() >= 10)
			{
				// Create checksums
				int index = maxIndex + 1;
				for (Entry<DimensionTable, List<StagingColumn>> en : scmap.entrySet())
				{
					String physicalName = Util.replaceWithNewPhysicalString(en.getKey().TableSource.Tables[0].PhysicalName + "_CKSUM", dc,r);
					writer.write("\t\t<COLUMN SOURCE=\"" + index + "\" NAME=\"" + physicalName + "\" xsi:type=\"SQLINT\"/>\n");
					index++;
				}
			}
			writer.write("\t</ROW>\n");
			writer.write("</BCPFORMAT>\n");
		}
		finally
		{
			if (writer != null)
				writer.close();
		}
		return length;
	}
}
