/**
 * $Id: TimeDefinition.java,v 1.69 2012-05-21 13:22:23 mpandit Exp $
 *
 * Copyright (C) 2007-2013 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.WeakHashMap;

import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jdom.Namespace;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.successmetricsinc.Repository;
import com.successmetricsinc.query.Database;
import com.successmetricsinc.query.Database.DatabaseType;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.Hierarchy;
import com.successmetricsinc.query.Level;
import com.successmetricsinc.util.AWSUtil;
import com.successmetricsinc.util.Util;

/**
 * @author Brad Peters
 * 
 */
public class TimeDefinition implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(TimeDefinition.class);
	
	private static String[] DWM_PREFIXES = new String[] { "DY", "WK", "MN", "QT", "HY" };
	private static String[] LEVEL_NAMES = new String[] { "Day", "Week", "Month", "Quarter", "Half" };

	public static final Map<String, Integer> weekdayConstantsByName = new HashMap<String, Integer>();
	static
	{
		weekdayConstantsByName.put("Sunday", Calendar.SUNDAY);	// XXX ugly non-internationalized date setting
		weekdayConstantsByName.put("Monday", Calendar.MONDAY);
		weekdayConstantsByName.put("Tuesday", Calendar.TUESDAY);
		weekdayConstantsByName.put("Wednesday", Calendar.WEDNESDAY);
		weekdayConstantsByName.put("Thursday", Calendar.THURSDAY);
		weekdayConstantsByName.put("Friday", Calendar.FRIDAY);
		weekdayConstantsByName.put("Saturday", Calendar.SATURDAY);
	}
	// Whether to generate a time dimension automatically
	public boolean GenerateTimeDimension;
	// Name of dimension
	public String Name;
	// Levels to instantiate
	public boolean Day;
	public boolean Week;
	public boolean Month;
	public boolean Quarter;
	public boolean Halfyear;
	public boolean Year;
	// Array of selected time periods to analyze
	public TimePeriod[] Periods;
	// Other time parameters
	public Calendar StartDate; // Beginning of time, defaults to 1900-01-01
	public TimeZone StartDateTimeZone;
	public Calendar EndDate; // End of time, defaults to 2050-11-31
	// Partition parameters
	public boolean PartitionFacts;
	public String PartitionPeriod; // String representing periodicity (e.g. Month)
	public Calendar PartitionStart; // Date to start partitioning
	public Calendar PartitionEnd; // Date to end partitioning
	public int FirstDayOfWeek = Calendar.SUNDAY;	// XXX ugly non-internationalized date setting
	public int WeekDeterminantDay = Calendar.SUNDAY;
	public int FirstDayOfWeekOffset = 0;
	public int WeekDeterminantDayOffset = 0;
	private String strWeekDeterminantDay;
	private String strFirstDayOfWeek;
	
	private String strPartitionStart;
	private String strPartitionEnd;
	private String strStartDate;
	private String strEndDate;
	
	public TimeDefinition(Element e, Namespace ns)
	{
		Name = e.getChildTextTrim("Name", ns);
		GenerateTimeDimension = Boolean.valueOf(e.getChildText("GenerateTimeDimension", ns)).booleanValue();
		Day = Boolean.valueOf(e.getChildText("Day", ns)).booleanValue();
		Week = Boolean.valueOf(e.getChildText("Week", ns)).booleanValue();
		Month = Boolean.valueOf(e.getChildText("Month", ns)).booleanValue();
		Quarter = Boolean.valueOf(e.getChildText("Quarter", ns)).booleanValue();
		Halfyear = Boolean.valueOf(e.getChildText("Halfyear", ns)).booleanValue();
		Year = Boolean.valueOf(e.getChildText("Year", ns)).booleanValue();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		PartitionFacts = Boolean.valueOf(e.getChildText("PartitionFacts", ns)).booleanValue();
		PartitionPeriod = e.getChildText("PartitionPeriod", ns);
		String s = e.getChildText("WeekDeterminantDay", ns);
		strWeekDeterminantDay = e.getChildText("WeekDeterminantDay", ns);
		if (s != null)
		{
			Integer determinantDayObj = weekdayConstantsByName.get(s);
			if (determinantDayObj != null)
			{
				WeekDeterminantDay = determinantDayObj.intValue();
			}
		}
		WeekDeterminantDayOffset = WeekDeterminantDay - 1;
		s = e.getChildText("FirstDayOfWeek", ns);
		strFirstDayOfWeek = e.getChildText("FirstDayOfWeek", ns);
		if (s != null)
		{
			Integer firstDayObj = weekdayConstantsByName.get(s);
			if (firstDayObj != null)
			{
				FirstDayOfWeek = firstDayObj.intValue();
			}
		}
		FirstDayOfWeekOffset = FirstDayOfWeek - 1;
		s = e.getChildText("StartDate", ns);
		strStartDate = e.getChildText("StartDate", ns);
		if (s != null)
		{
			try
			{
				Date d = df.parse(s);
				if (d != null)
				{
					StartDate = Calendar.getInstance();
					StartDate.setFirstDayOfWeek(FirstDayOfWeek);
					StartDate.setTime(d);
				}
			} catch (ParseException pe)
			{
				StartDate = null;
			}
		}
		if (StartDate == null)
		{
			StartDate = Calendar.getInstance();
			StartDate.setFirstDayOfWeek(FirstDayOfWeek);
			StartDate.set(1900, 1, 1);
		}
		StartDate.setMinimalDaysInFirstWeek(7);
		// Always start from the first day of week so that entries in the week definition table are consistent
		while (StartDate.get(Calendar.DAY_OF_WEEK) != FirstDayOfWeek)
		{
			StartDate.add(Calendar.DATE, 1);
		}
		StartDateTimeZone = StartDate.getTimeZone();
		s = e.getChildText("EndDate", ns);
		strEndDate = e.getChildText("EndDate", ns);
		if (s != null)
		{
			try
			{
				Date d = df.parse(s);
				if (d != null)
				{
					EndDate = Calendar.getInstance();
					EndDate.setTime(d);
				}
			} catch (ParseException pe)
			{
				EndDate = null;
			}
		}
		if (EndDate == null)
		{
			EndDate = Calendar.getInstance();
			EndDate.set(2050, 11, 31);
		}
		s = e.getChildText("PartitionStart", ns);
		strPartitionStart = e.getChildText("PartitionStart", ns);
		if (s != null)
		{
			try
			{
				Date d = df.parse(s);
				if (d != null)
				{
					PartitionStart = Calendar.getInstance();
					PartitionStart.setTime(d);
				}
			} catch (ParseException pe)
			{
				PartitionStart = null;
			}
		}
		if (PartitionStart == null)
		{
			PartitionStart = Calendar.getInstance();
			PartitionStart.set(2000, 1, 1);
		}
		s = e.getChildText("PartitionEnd", ns);
		strPartitionEnd = e.getChildText("PartitionEnd", ns);
		if (s != null)
		{
			try
			{
				Date d = df.parse(s);
				if (d != null)
				{
					PartitionEnd = Calendar.getInstance();
					PartitionEnd.setTime(d);
				}
			} catch (ParseException pe)
			{
				PartitionEnd = null;
			}
		}
		if (PartitionEnd == null)
		{
			PartitionEnd = Calendar.getInstance();
			PartitionEnd.set(2050, 11, 31);
		}
		Element periods = e.getChild("Periods", ns);
		if (periods != null)
		{
			@SuppressWarnings("rawtypes")
			List p2 = periods.getChildren();
			Periods = new TimePeriod[p2.size()];
			for (int count2 = 0; count2 < p2.size(); count2++)
			{
				TimePeriod tp = new TimePeriod((Element) p2.get(count2), ns);
				Periods[count2] = tp;
			}
		}
	}
	
	public Element getTimeDefinitionElement(Namespace ns)
	{
		Element e = new Element("TimeDefinition",ns);
		
		Element child = new Element("GenerateTimeDimension",ns);
		child.setText(String.valueOf(GenerateTimeDimension));
		e.addContent(child);
		
		child = new Element("Name",ns);
		child.setText(Name);
		e.addContent(child);
		
		child = new Element("Day",ns);
		child.setText(String.valueOf(Day));
		e.addContent(child);
		
		child = new Element("Week",ns);
		child.setText(String.valueOf(Week));
		e.addContent(child);
		
		child = new Element("Month",ns);
		child.setText(String.valueOf(Month));
		e.addContent(child);
		
		child = new Element("Quarter",ns);
		child.setText(String.valueOf(Quarter));
		e.addContent(child);
		
		child = new Element("Halfyear",ns);
		child.setText(String.valueOf(Halfyear));
		e.addContent(child);
		
		child = new Element("Year",ns);
		child.setText(String.valueOf(Year));
		e.addContent(child);
		
		if (Periods!=null && Periods.length>0)
		{
			child = new Element("Periods",ns);
			for (TimePeriod period : Periods)
			{
				child.addContent(period.getTimePeriodElement(ns));
			}
			e.addContent(child);
		}
		
		if (strWeekDeterminantDay!=null)
		{
			child = new Element("WeekDeterminantDay",ns);
			child.setText(strWeekDeterminantDay);
			e.addContent(child);
		}
		
		if (strFirstDayOfWeek!=null)
		{
			child = new Element("FirstDayOfWeek",ns);
			child.setText(strFirstDayOfWeek);
			e.addContent(child);
		}
		
		child = new Element("StartDate",ns);
		child.setText(strStartDate);
		e.addContent(child);
		
		child = new Element("EndDate",ns);
		child.setText(strEndDate);
		e.addContent(child);
		
		child = new Element("PartitionFacts",ns);
		child.setText(String.valueOf(PartitionFacts));
		e.addContent(child);
		
		if (PartitionPeriod!=null)
		{
			child = new Element("PartitionPeriod",ns);
			child.setText(PartitionPeriod);
			e.addContent(child);
		}
		
		child = new Element("PartitionStart",ns);
		child.setText(strPartitionStart);
		e.addContent(child);
		
		child = new Element("PartitionEnd",ns);
		child.setText(strPartitionEnd);
		e.addContent(child);
		
		return e;
	}
	
	private boolean[] getDWM()
	{
		return new boolean[] { Day, Week, Month, Quarter, Halfyear };
	}

	/**
	 * create the time files (do not touch the database)
	 * @param folder	file system folder where the time files are created
	 * @throws Exception
	 */
	public void createTimeFiles(String folder) throws Exception
	{
		boolean[] dwm = getDWM();
		createTimeFiles(dwm, null, folder);
		if (Periods != null)
		{
			for (TimePeriod tp : Periods)
			{
				createTimeFiles(dwm, tp, folder);				
			}
		}
	}
	
	/**
	 * create the time schema, but do not load the tables
	 * @param r
	 * @throws Exception
	 */
	public void createTimeSchema(Repository r) throws Exception
	{
		DatabaseConnection dconn = r.getDefaultConnection();
		boolean[] dwm = getDWM();
		if (!Database.schemaExists(dconn))
			Database.createSchema(dconn);
		createTimeSchema(dconn, dwm, null, r);
		if (Periods != null)
			for (TimePeriod tp : Periods)
			{
				createTimeSchema(dconn, dwm, tp, r);
			}
	}
	
	/**
	 * upload time files to S3
	 * @param r
	 * @param folder	file system location for the time files
	 * @param s3Folder	S3 bucket plus folder where the time files should be stored in S3
	 * @param s3credentials	AWS credentials for accessing S3
	 * @throws IOException
	 */
	public void uploadTimeFilesToS3(Repository r, String folder, String s3Folder, AWSCredentials s3credentials) throws Exception
	{
		DatabaseConnection dconn = r.getDefaultConnection();
		boolean[] dwm = getDWM();		
		uploadTimeFilesToS3(dconn, dwm, null, folder, s3Folder, s3credentials);
		if (Periods != null)
		{
			for (TimePeriod tp : Periods)
			{
				uploadTimeFilesToS3(dconn, dwm, tp, folder, s3Folder, s3credentials);
			}
		}
	}

	/**
	 * load the time tables
	 * @param r
	 * @param folder	location of the time objects (could be file system folder or S3 folder)
	 * @param s3credentials	credentials for AWS if using Redshift
	 * @throws Exception
	 */
	public void loadTimeSchema(Repository r, String folder, AWSCredentials s3credentials) throws Exception
	{
		DatabaseConnection dconn = r.getDefaultConnection();
		boolean[] dwm = getDWM();
		loadTimeSchema(dconn, dwm, null, r, folder, s3credentials);
		if (Periods != null)
		{
			for (TimePeriod tp : Periods)
			{
				loadTimeSchema(dconn, dwm, tp, r, folder, s3credentials);
			}
		}
	}
	
	/**
	 * generate the time tables - create the schema and insert the data
	 * - will be removed once we have bulk load of time tables working across the databases
	 * @param r
	 * @throws Exception
	 * @deprecated
	 */
	public void generate(Repository r) throws Exception
	{
		DatabaseConnection dconn = r.getDefaultConnection();
		boolean[] dwm = getDWM();
		generateTable(dconn, dwm, null, r);
		if (Periods != null)
		{
			for (TimePeriod tp : Periods)
			{
				generateTable(dconn, dwm, tp, r);
			}
		}
	}

	/**
	 * Return the year ID for a given date
	 * 
	 * @param date
	 * @return
	 */
	public int getYearID(Calendar date)
	{
		return (date.get(Calendar.YEAR) - StartDate.get(Calendar.YEAR)) + 1;
	}

	/**
	 * Return the half-year ID for a given date
	 * 
	 * @param date
	 * @return
	 */
	public int getHalfYearID(Calendar date)
	{
		return (date.get(Calendar.YEAR) - StartDate.get(Calendar.YEAR)) * 2 + (date.get(Calendar.MONTH) / 6) + 1;
	}

	/**
	 * Return the quarter ID for a given date
	 * 
	 * @param date
	 * @return
	 */
	public int getQuarterID(Calendar date)
	{
		return (date.get(Calendar.YEAR) - StartDate.get(Calendar.YEAR)) * 4 + (date.get(Calendar.MONTH) / 3) + 1;
	}

	/**
	 * Return the month ID for a given date
	 * 
	 * @param date
	 * @return
	 */
	public int getMonthID(Calendar date)
	{
		if (date == null)
			return (Calendar.getInstance().get(Calendar.YEAR) - StartDate.get(Calendar.YEAR)) * 12 + Calendar.getInstance().get(Calendar.MONTH) + 1;
		return (date.get(Calendar.YEAR) - StartDate.get(Calendar.YEAR)) * 12 + date.get(Calendar.MONTH) + 1;
	}

	/**
	 * Return the week ID for a given date
	 * 
	 * @param date
	 * @return
	 */
	public int getWeekID(Calendar date)
	{
		if (date == null)
		{
			long dt = System.currentTimeMillis();
			return (int) Math.floor(((double) ((dt + TimeZone.getDefault().getOffset(dt)) - (StartDate.getTimeInMillis()) + StartDate.getTimeZone().getOffset(
					StartDate.getTimeInMillis())) / ((double) 1000 * 60 * 60 * 24 * 7))) + 1;
		}
		/*
		 * To prevent rounding errors, create a temporary date object below and make sure we borrow just the year, month
		 * and day parts from the date. This makes sure that when division is performed, the remainder is exactly 0.
		 */
		Calendar tempDate = Calendar.getInstance();
		tempDate.set(date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DATE));
		return (int) Math.floor((double) ((double) ((tempDate.getTimeInMillis() + tempDate.getTimeZone().getOffset(tempDate.getTimeInMillis())) - (StartDate
				.getTimeInMillis() + StartDateTimeZone.getOffset(StartDate.getTimeInMillis()))) / ((double) 1000 * 60 * 60 * 24 * 7))) + 1;
	}

	Calendar tempDate = Calendar.getInstance();
	TimeZone tempDateTimeZone = tempDate.getTimeZone();
	private transient Map<Calendar, Integer> dayIDCache = Collections.synchronizedMap(new WeakHashMap<Calendar, Integer>());
	
	/**
	 * Initializes the dayIDCache. 
	 * dayIDCache is a transient variable meaning it won't be initialized properly during bin repository loading
	 * Hence an explicit need to initialize properly.
	 */
	private synchronized void initializeDayIDCache(){
		if (dayIDCache == null)
			dayIDCache = Collections.synchronizedMap(new WeakHashMap<Calendar, Integer>());
	}
	
	/**
	 * Return the day ID for a given date
	 * 
	 * @param date
	 * @return
	 */
	public int getDayID(Calendar date)
	{
		if (dayIDCache == null)
			initializeDayIDCache();
		Integer result = dayIDCache.get(date);
		if (result != null)
			return result;
		if (date == null)
		{
			long dt = System.currentTimeMillis();
			return (int) Math.floor((double) ((double) ((dt + TimeZone.getDefault().getOffset(dt)) - (StartDate.getTimeInMillis()) + StartDateTimeZone
					.getOffset(StartDate.getTimeInMillis())) / ((double) 1000 * 60 * 60 * 24))) + 1;
		}
		/*
		 * To prevent rounding errors, create a temporary date object below and make sure we borrow just the year, month
		 * and day parts from the date. This makes sure that when division is performed, the remainder is exactly 0.
		 */
		synchronized (tempDate)
		{
			tempDate.set(date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DATE));
			result = (int) Math.floor((double) ((double) ((tempDate.getTimeInMillis() + tempDateTimeZone.getOffset(tempDate.getTimeInMillis())) - (StartDate
					.getTimeInMillis() + StartDateTimeZone.getOffset(StartDate.getTimeInMillis()))) / ((double) 1000 * 60 * 60 * 24))) + 1;
		}
		dayIDCache.put(date, result);
		return result;
	}
	
	/**
	 * Return the day ID column name
	 * 
	 * @return
	 */
	public String getDayIDColumnName()
	{
		return ("Day ID");
	}	

	/**
	 * Return the week ID column name
	 * 
	 * @return
	 */
	private String getWeekIDColumnName()
	{
		return ("Week ID");
	}

	/**
	 * Return the month ID column name
	 * 
	 * @return
	 */
	private String getMonthIDColumnName()
	{
		return ("Month ID");
	}

	/**
	 * Return the month ID column name
	 * 
	 * @return
	 */
	private String getQuarterIDColumnName()
	{
		return ("Quarter ID");
	}

	/**
	 * Return the month ID column name
	 * 
	 * @return
	 */
	private String getHalfyearIDColumnName()
	{
		return ("Half Year ID");
	}

	/**
	 * Return the year ID column name
	 * 
	 * @return
	 */
	private String getYearIDColumnName()
	{
		return ("Year ID");
	}

	/**
	 * Return the number of weeks the month of a specific date
	 * 
	 * @param date
	 * @return
	 */
	private int getNumWeeksInMonth(Calendar date)
	{
		Calendar dateNumWeeks = (Calendar) date.clone();
		int maxDay = dateNumWeeks.getActualMaximum(Calendar.DAY_OF_MONTH);
		// Set to the first day
		dateNumWeeks.add(Calendar.DAY_OF_MONTH, (-dateNumWeeks.get(Calendar.DAY_OF_MONTH) + maxDay));
		int weeksInMonth = dateNumWeeks.get(Calendar.WEEK_OF_MONTH);
		return weeksInMonth;
	}

	/**
	 * return the day of the week for a specific date
	 * @param date
	 * @return
	 */
	private int getDayOfWeek(Calendar date)
	{
		int dayOfWeek = date.get(Calendar.DAY_OF_WEEK);
		dayOfWeek = dayOfWeek - FirstDayOfWeekOffset;
		if (dayOfWeek < 1)
		{
			dayOfWeek += 7;
		}
		return dayOfWeek;
	}

	/**
	 * Return the ID column for the partition period
	 * 
	 * @return
	 */
	public String getPartitionIDColumnName()
	{
		if (PartitionPeriod == null)
			return (null);
		String colName = null;
		if (PartitionPeriod.equalsIgnoreCase("month"))
			colName = getMonthIDColumnName();
		else if (PartitionPeriod.equalsIgnoreCase("week"))
			colName = getWeekIDColumnName();
		else if (PartitionPeriod.equalsIgnoreCase("year"))
			colName = getYearIDColumnName();
		return (colName);
	}

	/**
	 * Return the relevant partition scheme name
	 * 
	 * @param r
	 * @return
	 */
	public String getPartitionSchemeName(Repository r)
	{
		return (r.getApplicationName() == null || r.getApplicationName().length() == 0 ? "PS_DEFAULT" : Util.replaceWithPhysicalString(r.getApplicationName()));
	}

	/**
	 * create the time files, do not load them into the database
	 * @param dwm
	 * @param tp
	 * @throws Exception
	 */
	private void createTimeFiles(boolean[] dwm, TimePeriod tp, String location) throws Exception
	{
		String[] dwmPrefix = new String[]
				{ "DY", "WK", "MN", "QT", "HY" };
		String[] levelNames = new String[]
				{ "Day", "Week", "Month", "Quarter", "Half" };
		Map<String, Integer> periodConstantByPrefix = new HashMap<String, Integer>();
		periodConstantByPrefix.put("DY", Calendar.DAY_OF_YEAR);
		periodConstantByPrefix.put("WK", Calendar.WEEK_OF_YEAR);
		periodConstantByPrefix.put("MN", Calendar.MONTH);
		periodConstantByPrefix.put("QT", Calendar.MONTH);
		periodConstantByPrefix.put("HY", Calendar.MONTH);
		
		File fl = new File(location);
		if (!fl.exists())
			fl.mkdirs();

		for (int i = 0; i < 5; i++)
		{						
			if (dwm[i])
			{
				String name = getTableName(tp, levelNames[i], dwmPrefix[i]);
				logger.info("Creating file for: " + name);

				// XXX not internationalized, should just be indexes into message catalogs
				String[] days = new String[]
						{ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
				String[] months = new String[]
						{ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
				Calendar curDate = (Calendar) StartDate.clone();

				FileStatement stmt = null;
				FileOutputStream fos = null;
				OutputStreamWriter os = null;

				try
				{
					fos = new FileOutputStream(location + '/' + name + ".txt");
					os = new OutputStreamWriter(fos, "UTF-8");
					stmt = new FileStatement(os);

					int num = 0;
					while (curDate.getTimeInMillis() <= EndDate.getTimeInMillis())
					{
						Calendar idDate = null;
						Calendar stop = null;
						if (tp == null)
						{
							idDate = (Calendar) curDate.clone();
							stop = (Calendar) curDate.clone();
						} else
						{
							idDate = (Calendar) curDate.clone();
							if (tp.ShiftAmount != 0)
							{
								if (tp.ShiftPeriod == TimePeriod.PERIOD_WEEK)
									idDate.add(Calendar.WEEK_OF_YEAR, (tp.ShiftAgo ? -1 : 1) * tp.ShiftAmount);
								else if (tp.ShiftPeriod == TimePeriod.PERIOD_MONTH)
									idDate.add(Calendar.MONTH, (tp.ShiftAgo ? -1 : 1) * tp.ShiftAmount);
								else if (tp.ShiftPeriod == TimePeriod.PERIOD_QUARTER)
									idDate.add(Calendar.MONTH, (tp.ShiftAgo ? -1 : 1) * tp.ShiftAmount * 3);
								else if (tp.ShiftPeriod == TimePeriod.PERIOD_HALF_YEAR)
									idDate.add(Calendar.MONTH, (tp.ShiftAgo ? -1 : 1) * tp.ShiftAmount * 6);
								else if (tp.ShiftPeriod == TimePeriod.PERIOD_YEAR)
									idDate.add(Calendar.MONTH, (tp.ShiftAgo ? -1 : 1) * tp.ShiftAmount * 12);
							}
							stop = (Calendar) idDate.clone();
							if (tp.AggregationType == TimePeriod.AGG_TYPE_TRAILING)
							{
								if (tp.AggregationPeriod == TimePeriod.PERIOD_DAY)
								{
									idDate.add(Calendar.DAY_OF_YEAR, -tp.AggregationNumPeriods);
									idDate.add(periodConstantByPrefix.get(dwmPrefix[i]), 1);
								} else if (tp.AggregationPeriod == TimePeriod.PERIOD_WEEK)
								{
									idDate.add(Calendar.WEEK_OF_YEAR, -tp.AggregationNumPeriods);
									idDate.add(periodConstantByPrefix.get(dwmPrefix[i]), 1);
								} else if (tp.AggregationPeriod == TimePeriod.PERIOD_MONTH)
								{
									idDate.add(Calendar.MONTH, -tp.AggregationNumPeriods);
									idDate.add(periodConstantByPrefix.get(dwmPrefix[i]), 1);
								}
							} else if (tp.AggregationType == TimePeriod.AGG_TYPE_TO_DATE)
							{
								if (tp.AggregationPeriod == TimePeriod.PERIOD_WEEK)
									idDate.set(Calendar.DAY_OF_WEEK, 1);
								else if (tp.AggregationPeriod == TimePeriod.PERIOD_MONTH)
									idDate.set(Calendar.DAY_OF_MONTH, 1);
								else if (tp.AggregationPeriod == TimePeriod.PERIOD_QUARTER)
								{
									idDate.set(Calendar.DAY_OF_MONTH, 1);
									idDate.add(Calendar.MONTH, -idDate.get(Calendar.MONTH) % 3);
								} else if (tp.AggregationPeriod == TimePeriod.PERIOD_HALF_YEAR)
								{
									idDate.set(Calendar.DAY_OF_MONTH, 1);
									idDate.add(Calendar.MONTH, -idDate.get(Calendar.MONTH) % 6);
								} else if (tp.AggregationPeriod == TimePeriod.PERIOD_YEAR)
								{
									idDate.set(Calendar.DAY_OF_YEAR, 1);
								}
								/**
								 * if the time definition period is week, make sure that the ID date is set to the first day of week
								 */
								if (i == 1)
								{
									while (idDate.get(Calendar.DAY_OF_WEEK) != WeekDeterminantDay)
									{
										idDate.add(Calendar.DATE, 1);
									}
								}
							}
						}
						while (idDate.getTimeInMillis() <= stop.getTimeInMillis())
						{
							int pos = 1;
							if (i == 1 && (curDate.get(Calendar.DAY_OF_WEEK) != WeekDeterminantDay))
							{
								Calendar weekDeterminantDate = (Calendar) curDate.clone();
								while (weekDeterminantDate.get(Calendar.DAY_OF_WEEK) != WeekDeterminantDay)
								{
									weekDeterminantDate.add(Calendar.DATE, 1);
								}
								stmt.setDate(pos++, new java.sql.Date(weekDeterminantDate.getTimeInMillis()));
							} else
							{
								stmt.setDate(pos++, new java.sql.Date(curDate.getTimeInMillis()));
							}
							if (i == 0)
							{
								int dayNumber = (curDate.get(Calendar.YEAR) * 10000) + ((curDate.get(Calendar.MONTH) + 1) * 100)
										+ curDate.get(Calendar.DAY_OF_MONTH);
								stmt.setInt(pos++, dayNumber);		
								stmt.setInt(pos++, getDayID(curDate));
								stmt.setString(pos++, days[curDate.get(Calendar.DAY_OF_WEEK) - 1]);
								stmt.setInt(pos++, getDayOfWeek(curDate));
								stmt.setInt(pos++, curDate.get(Calendar.DAY_OF_WEEK_IN_MONTH));
								stmt.setInt(pos++, curDate.get(Calendar.DAY_OF_MONTH));
								stmt.setInt(pos++, curDate.get(Calendar.DAY_OF_YEAR));
								if (tp != null)
								{
									// Day_ID and TTM_Day_ID
									stmt.setInt(pos++, getDayID(curDate));
									stmt.setInt(pos++, getDayID(idDate));
								} else
								{
									// Just Day_ID
									stmt.setInt(pos++, getDayID(idDate));
								}
							}
							if (i <= 1)
							{
								Calendar weekStartDate = (Calendar) curDate.clone();
								while (weekStartDate.get(Calendar.DAY_OF_WEEK) != FirstDayOfWeek)
								{
									weekStartDate.add(Calendar.DATE, -1);
								}
								Calendar weekEndDate = (Calendar) weekStartDate.clone();
								weekEndDate.add(Calendar.DATE, 6);
								int weekOfMonth = curDate.get(Calendar.WEEK_OF_MONTH);
								int weekNumber = 0;
								if (weekOfMonth == 0)
								{
									weekOfMonth = weekStartDate.get(Calendar.WEEK_OF_MONTH);
									weekNumber = (weekStartDate.get(Calendar.YEAR) * 10000) + ((weekStartDate.get(Calendar.MONTH) + 1) * 100) + weekOfMonth;
								} else
								{
									weekNumber = (curDate.get(Calendar.YEAR) * 10000) + ((curDate.get(Calendar.MONTH) + 1) * 100)
											+ curDate.get(Calendar.WEEK_OF_MONTH);
								}
								stmt.setInt(pos++, weekNumber);
								stmt.setInt(pos++, getWeekID(curDate));
								stmt.setInt(pos++, weekOfMonth);
								stmt.setInt(pos++, curDate.get(Calendar.WEEK_OF_YEAR));
								stmt.setDate(pos++, new java.sql.Date(weekStartDate.getTimeInMillis()));	
								stmt.setDate(pos++, new java.sql.Date(weekEndDate.getTimeInMillis()));
								if (tp != null)
								{
									// Week_ID and TTM_Week_ID
									stmt.setInt(pos++, getWeekID(curDate));
									stmt.setInt(pos++, getWeekID(idDate));
								} else
								{
									// Just Week_ID
									stmt.setInt(pos++, getWeekID(idDate));
								}
							}
							if (i <= 2)
							{
								int monthNumber = (curDate.get(Calendar.YEAR) * 100) + curDate.get(Calendar.MONTH) + 1;
								stmt.setInt(pos++, monthNumber);
								stmt.setInt(pos++, getMonthID(curDate));
								stmt.setString(pos++, months[curDate.get(Calendar.MONTH)]);
								int mo = curDate.get(Calendar.MONTH) + 1;
								stmt.setString(pos++, String.format("%4d/%02d", curDate.get(Calendar.YEAR), mo));
								stmt.setInt(pos++, mo);
								Calendar qagoDate = (Calendar) curDate.clone();
								qagoDate.add(Calendar.MONTH, -2);
								int qagomo = qagoDate.get(Calendar.MONTH) + 1;
								int yearnum = curDate.get(Calendar.YEAR) % 100;
								int qagoyearnum = qagoDate.get(Calendar.YEAR) % 100;
								StringBuilder trailing3MonthNameBuf = new StringBuilder();
								trailing3MonthNameBuf.append(String.format("%02d", qagomo));
								trailing3MonthNameBuf.append("/");
								trailing3MonthNameBuf.append(String.format("%02d", qagoyearnum));
								trailing3MonthNameBuf.append(" - ");
								trailing3MonthNameBuf.append(String.format("%02d", mo));
								trailing3MonthNameBuf.append("/");
								trailing3MonthNameBuf.append(String.format("%02d", yearnum));
								stmt.setString(pos++, trailing3MonthNameBuf.toString());
								int numWeeksInMonth = getNumWeeksInMonth(curDate);
								stmt.setInt(pos++, numWeeksInMonth);
								if (tp != null)
								{
									// Month_ID and TTM_Month_ID
									stmt.setInt(pos++, getMonthID(curDate));
									stmt.setInt(pos++, getMonthID(idDate));
								} else
								{
									// Just Month_ID
									stmt.setInt(pos++, getMonthID(idDate));
								}
							}
							if (i <= 3)
							{
								int q = (curDate.get(Calendar.MONTH) / 3) + 1;
								int quarterNumber = (curDate.get(Calendar.YEAR) * 10) + q;
								stmt.setInt(pos++, quarterNumber);
								stmt.setInt(pos++, getQuarterID(curDate));
								stmt.setString(pos++, "Q" + q);
								stmt.setString(pos++, curDate.get(Calendar.YEAR) + "/Q" + q);
								if (tp != null)
								{
									// Quarter_ID and TTM_Quarter_ID
									stmt.setInt(pos++, getQuarterID(curDate));
									stmt.setInt(pos++, getQuarterID(idDate));
								} else
								{
									// Just Quarter_ID
									stmt.setInt(pos++, getQuarterID(idDate));
								}
							}
							if (i <= 4)
							{
								int h = (curDate.get(Calendar.MONTH) / 6) + 1;
								int halfYearNumber = (curDate.get(Calendar.YEAR) * 100) + h;
								stmt.setInt(pos++, halfYearNumber);
								stmt.setInt(pos++, getHalfYearID(curDate));
								stmt.setString(pos++, "H" + h);
								stmt.setString(pos++, curDate.get(Calendar.YEAR) + "/H" + h);
								if (tp != null)
								{
									// Half_Year_ID and TTM_Half_Year_ID
									stmt.setInt(pos++, getHalfYearID(curDate));
									stmt.setInt(pos++, getHalfYearID(idDate));
								} else
								{
									// Just Half_Year_ID
									stmt.setInt(pos++, getHalfYearID(idDate));
								}
							}
							if (tp != null)
							{
								stmt.setDate(pos++, new java.sql.Date(idDate.getTimeInMillis()));
							}
							int yearNumber = curDate.get(Calendar.YEAR);
							stmt.setInt(pos++, yearNumber);
							stmt.setInt(pos++, getYearID(curDate));
							stmt.setInt(pos++, getYearID(curDate)); // XXX idDate?
							stmt.addBatch();

							if (++num % 500000 == 0)
							{
								logger.debug("wrote " + num + " records");
							}	

							if (i == 0)
								idDate.add(Calendar.DATE, 1);
							else if (i == 1)
								idDate.add(Calendar.WEEK_OF_YEAR, 1);
							else if (i == 2)
								idDate.add(Calendar.MONTH, 1);
							else if (i == 3)
								idDate.add(Calendar.MONTH, 3);
							else if (i == 4)
								idDate.add(Calendar.MONTH, 6);
						}
						if (i == 0)
						{
							curDate.add(Calendar.DATE, 1);
						} else if (i == 1)
						{
							curDate.add(Calendar.WEEK_OF_YEAR, 1);
						} else if (i == 2)
						{
							curDate.add(Calendar.MONTH, 1);
						} else if (i == 3)
						{
							curDate.add(Calendar.MONTH, 3);
						} else if (i == 4)
						{
							curDate.add(Calendar.MONTH, 6);
						}
					}
				}
				finally
				{
					if (stmt != null)
						stmt.close();
					if (os != null)
						os.close();
					if (fos != null)
						fos.close();
				}
			}
		}		
	}
	
	/**
	 * create the time schema, do not load the tables
	 * @param dc
	 * @param dwm
	 * @param tp
	 * @param r
	 * @throws SQLException
	 */
	private void createTimeSchema(DatabaseConnection dc, boolean[] dwm, TimePeriod tp, Repository r) throws SQLException
	{		
		for (int i = 0; i < 5; i++)
		{
			if (dwm[i])
			{
				String name = getTableName(tp, LEVEL_NAMES[i], DWM_PREFIXES[i]);
				logger.info("Creating: " + name);

				List<String> colNames = new ArrayList<String>();
				List<String> colTypes = new ArrayList<String>();
				List<String> distkeys = new ArrayList<String>();
				Set<String> sortkeys = new HashSet<String>();
				
				generateColumns(dc, i, tp, colNames, colTypes, distkeys,r);
				if (distkeys.size() == 1)
					sortkeys.add(distkeys.get(0));
				Database.createTable(r, dc, name, colNames, colTypes, true, false, null, distkeys, sortkeys); // no compression encodings for RedShift, calculate on the load
			}
		}		
	}	
	
	/**
	 * based upon the time period and grain, generate the columns for the time table
	 * @param dc
	 * @param i	grain index (day, week, month, quarter, half year)
	 * @param tp	time period (YTD, TTM, etc)
	 * @param colNames
	 * @param colTypes
	 * @param distkeys
	 */
	private void generateColumns(DatabaseConnection dc, int i, TimePeriod tp, List<String> colNames, List<String> colTypes, List<String> distkeys,Repository r)
	{
		colNames.add(Util.replaceWithNewPhysicalString("Date", dc,null));
		colTypes.add(dc.getDateType());
		
		if (i == 0)
		{
			colNames.add(Util.replaceWithNewPhysicalString("Day Number", dc,r));
			colTypes.add("INTEGER");
			colNames.add(Util.replaceWithNewPhysicalString("Day Seq Number", dc,r));
			colTypes.add("INTEGER");
			colNames.add(Util.replaceWithNewPhysicalString("Day", dc,r));
			colTypes.add("VARCHAR(10)");
			colNames.add(Util.replaceWithNewPhysicalString("Day of Week", dc,r));
			colTypes.add("INTEGER");
			colNames.add(Util.replaceWithNewPhysicalString("Day of Week in Month", dc,r));
			colTypes.add("INTEGER");
			colNames.add(Util.replaceWithNewPhysicalString("Day of Month", dc,r));
			colTypes.add("INTEGER");
			colNames.add(Util.replaceWithNewPhysicalString("Day of Year", dc,r));
			colTypes.add("INTEGER");
			colNames.add(Util.replaceWithNewPhysicalString(getDayIDColumnName(), dc,r));
			colTypes.add(dc.getDimensionalKeyType());
			if (tp != null)
			{
				colNames.add(Util.replaceWithNewPhysicalString(tp.Prefix + "_" + getDayIDColumnName(), dc,r));
				colTypes.add(dc.getDimensionalKeyType());
				distkeys.add(Util.replaceWithNewPhysicalString(tp.Prefix + "_" + getDayIDColumnName(), dc,r));
			}
			else
			{
				distkeys.add(Util.replaceWithNewPhysicalString(getDayIDColumnName(), dc,r));
			}
		}
		if (i <= 1)
		{
			colNames.add(Util.replaceWithNewPhysicalString("Week Number", dc, r));
			colTypes.add("INTEGER");
			colNames.add(Util.replaceWithNewPhysicalString("Week Seq Number", dc, r));
			colTypes.add("INTEGER");
			colNames.add(Util.replaceWithNewPhysicalString("Week of Month", dc, r));
			colTypes.add("INTEGER");
			colNames.add(Util.replaceWithNewPhysicalString("Week of Year", dc, r));
			colTypes.add("INTEGER");
			colNames.add(Util.replaceWithNewPhysicalString("Week Start Date", dc, r));
			colTypes.add(dc.getDateType());
			colNames.add(Util.replaceWithNewPhysicalString("Week End Date", dc, r));
			colTypes.add(dc.getDateType());
			colNames.add(Util.replaceWithNewPhysicalString(getWeekIDColumnName(), dc, r));
			colTypes.add(dc.getDimensionalKeyType());
			if (tp != null)
			{
				colNames.add(Util.replaceWithNewPhysicalString(tp.Prefix + "_" + getWeekIDColumnName(), dc, r));
				colTypes.add(dc.getDimensionalKeyType());
				if (i == 1)
					distkeys.add(Util.replaceWithNewPhysicalString(tp.Prefix + "_" + getWeekIDColumnName(), dc, r));
			}
			else
			{
				if (i == 1)
					distkeys.add(Util.replaceWithNewPhysicalString(getWeekIDColumnName(), dc, r));
			}
		}
		if (i <= 2)
		{
			colNames.add(Util.replaceWithNewPhysicalString("Month Number", dc, r));
			colTypes.add("INTEGER");
			colNames.add(Util.replaceWithNewPhysicalString("Month Seq Number", dc, r));
			colTypes.add("INTEGER");
			colNames.add(Util.replaceWithNewPhysicalString("Month", dc, r));
			colTypes.add("VARCHAR(10)");
			colNames.add(Util.replaceWithNewPhysicalString("Year_Month", dc, r));
			colTypes.add("VARCHAR(15)");
			colNames.add(Util.replaceWithNewPhysicalString("Month of Year", dc, r));
			colTypes.add("INTEGER");
			colNames.add(Util.replaceWithNewPhysicalString("Trailing 3 Month Name", dc, r));
			colTypes.add("VARCHAR(15)");
			colNames.add(Util.replaceWithNewPhysicalString("Num Weeks In Month", dc, r));
			colTypes.add("INTEGER");
			colNames.add(Util.replaceWithNewPhysicalString(getMonthIDColumnName(), dc, r));
			colTypes.add(dc.getDimensionalKeyType());
			if (tp != null)
			{
				colNames.add(Util.replaceWithNewPhysicalString(tp.Prefix + "_" + getMonthIDColumnName(), dc, r));
				colTypes.add(dc.getDimensionalKeyType());
				if (i == 2)
					distkeys.add(Util.replaceWithNewPhysicalString(tp.Prefix + "_" + getMonthIDColumnName(), dc, r));
			}
			else
			{
				if (i == 2)
					distkeys.add(Util.replaceWithNewPhysicalString(getMonthIDColumnName(), dc, r));
			}
		}
		if (i <= 3)
		{
			colNames.add(Util.replaceWithNewPhysicalString("Quarter Number", dc, r));
			colTypes.add("INTEGER");
			colNames.add(Util.replaceWithNewPhysicalString("Quarter Seq Number", dc, r));
			colTypes.add("INTEGER");
			colNames.add(Util.replaceWithNewPhysicalString("Quarter", dc, r));
			colTypes.add("VARCHAR(2)");
			colNames.add(Util.replaceWithNewPhysicalString("Year_Quarter", dc, r));
			colTypes.add("VARCHAR(7)");
			colNames.add(Util.replaceWithNewPhysicalString(getQuarterIDColumnName(), dc, r));
			colTypes.add(dc.getDimensionalKeyType());
			if (tp != null)
			{
				colNames.add(Util.replaceWithNewPhysicalString(tp.Prefix + "_" + getQuarterIDColumnName(), dc, r));
				colTypes.add(dc.getDimensionalKeyType());
				if (i == 3)
					distkeys.add(Util.replaceWithNewPhysicalString(tp.Prefix + "_" + getQuarterIDColumnName(), dc, r));
			}
			else
			{
				if (i == 3)
					distkeys.add(Util.replaceWithNewPhysicalString(getQuarterIDColumnName(), dc, r));
			}
		}
		if (i <= 4)
		{
			colNames.add(Util.replaceWithNewPhysicalString("Half Year Number", dc, r));
			colTypes.add("INTEGER");
			colNames.add(Util.replaceWithNewPhysicalString("Half Year Seq Number", dc, r));
			colTypes.add("INTEGER");
			colNames.add(Util.replaceWithNewPhysicalString("Half", dc, r));
			colTypes.add("VARCHAR(2)");
			colNames.add(Util.replaceWithNewPhysicalString("Year_Half", dc, r));
			colTypes.add("VARCHAR(7)");
			colNames.add(Util.replaceWithNewPhysicalString(getHalfyearIDColumnName(), dc, r));
			colTypes.add(dc.getDimensionalKeyType());
			if (tp != null)
			{
				colNames.add(Util.replaceWithNewPhysicalString(tp.Prefix + "_" + getHalfyearIDColumnName(), dc, r));
				colTypes.add(dc.getDimensionalKeyType());
				if (i == 4)
					distkeys.add(Util.replaceWithNewPhysicalString(tp.Prefix + "_" + getHalfyearIDColumnName(), dc, r));
			}
			else
			{
				if (i == 4)
					distkeys.add(Util.replaceWithNewPhysicalString(getHalfyearIDColumnName(), dc, r));
			}
		}
		if (tp != null)
		{
			colNames.add(Util.replaceWithNewPhysicalString(tp.Prefix + "_Date", dc, r));
			colTypes.add(dc.getDateType());
		}

		colNames.add(Util.replaceWithNewPhysicalString("Year", dc, r));
		colTypes.add("INTEGER");
		colNames.add(Util.replaceWithNewPhysicalString("Year Seq Number", dc, r));
		colTypes.add("INTEGER");
		colNames.add(Util.replaceWithNewPhysicalString(getYearIDColumnName(), dc, r));
		colTypes.add(dc.getDimensionalKeyType());
	}
	
	/**
	 * generate the appropriate name for the time table
	 * - based upon the time period and level (day, week, month, quarter, half year)
	 * @param tp
	 * @param levelName
	 * @param dwmPrefix
	 * @return
	 */
	private String getTableName(TimePeriod tp, String levelName, String dwmPrefix)
	{
		String name = tp == null ? "DW_DM_" + Name + "_" + levelName : "DW_DM_" + tp.Prefix + "_" + dwmPrefix;
		return Util.replaceWithPhysicalString(name.toUpperCase());
	}
	
	/**
	 * upload the time files to S3, but do not load them
	 * @param dwm
	 * @param tp
	 * @param folder	directory containing the file
	 * @param s3Location	location of the S3 folder
	 * @throws IOException
	 */
	private void uploadTimeFilesToS3(DatabaseConnection dc, boolean[] dwm, TimePeriod tp, String folder, String s3Location, AWSCredentials s3credentials) throws Exception
	{
		if (s3credentials == null)
			throw new IOException("AWS Credentials are null");

		AmazonS3Client s3 = new AmazonS3Client(s3credentials);
		
		for (int i = 0; i < 5; i++)
		{
			if (dwm[i])
			{
				String object = getTableName(tp, LEVEL_NAMES[i], DWM_PREFIXES[i]);
				String name = folder + '/' + object;
				File file = new File(name + ".txt");				
				if (!file.exists())
				{
					logger.warn("Load file does not exist, skipping: " + file.getName());
					continue;
				}
				logger.info("Uploading " + file.getAbsolutePath() + " to " + s3Location);
				int slices = AWSUtil.getNumberOfSlices(dc);
				AWSUtil.s3FileCopy(slices, file, s3Location, s3);
			}
		}
	}
	
	/**
	 * bulk load the time schema
	 * - assumes that the time schema tables already exist (createtimeschema)
	 * - assumes that the time table files (or S3 objects for Redshift) already exist (createtimefiles, uploadtimefiles)
	 * @param dc
	 * @param conn
	 * @param dwm
	 * @param tp
	 * @param r
	 * @throws SQLException
	 * @throws Exception
	 */
	private void loadTimeSchema(DatabaseConnection dc, boolean[] dwm, TimePeriod tp, Repository r, String location, AWSCredentials s3credentials) throws SQLException, Exception
	{		
		if (!dc.supportsBulkLoad())
		{
			throw new Exception("Database type (" + dc.Type + ") does not support bulk loading");
		}
		Connection conn = dc.ConnectionPool.getConnection();
		for (int i = 0; i < 5; i++)
		{
			if (dwm[i])
			{
				String name = getTableName(tp, LEVEL_NAMES[i], DWM_PREFIXES[i]);
				logger.info("Populating: " + name);

				List<String> colNames = new ArrayList<String>();
				List<String> colTypes = new ArrayList<String>();
				List<String> distkeys = new ArrayList<String>();
				
				generateColumns(dc, i, tp, colNames, colTypes, distkeys,r);

				if (dc.DBType == DatabaseType.Redshift)
				{
					if (s3credentials == null)
						throw new SQLException("AWS Credentials are null");

					// no encryption for time dimension in S3, would need to have a global key
					AmazonS3 s3client = AWSUtil.getS3Client(s3credentials, null);

					String tableName = dc.Schema + '.' + name;
					String s3Location = location + '/' + name + ".txt";
					String baseBulkInsert = AWSUtil.getCopyCommand(tableName, null, s3Location, s3client, false);						
					String credentials = AWSUtil.getCopyCommandCredentials(s3credentials, null);
					String dummyCredentials = AWSUtil.getCopyCommandDummyCredentials(null);
					
					String bulkInsert = baseBulkInsert + credentials;
					String outputBulkInsert = baseBulkInsert + dummyCredentials;
					
					logger.info(outputBulkInsert);
					Statement bulkInsertStmt = conn.createStatement();
					bulkInsertStmt.execute(bulkInsert);
				}
				else if (dc.DBType == DatabaseType.Hana)
				{
					String tableName = dc.Schema + '.' + name;
					String fileName = location + '/' + name + ".txt";
					String errorLogFile = null; // XXX
					String bulkInsert = "IMPORT FROM CSV FILE '" + fileName + "' INTO " 
							+ tableName
							+ " WITH"							
							+ " FIELD DELIMITED BY '|'" 
							+ " DATE FORMAT 'YYYY-MM-DD'"
							+ " TIMESTAMP FORMAT 'YYYY-MM-DD HH24:MI:SS'"
							+ " THREADS 10"		// XXX initial recommendation from the documentation
							+ " BATCH 10000"	// XXX initial recommendation from the documentation
							+ " TABLE LOCK"		// faster loading
							+ (errorLogFile == null || errorLogFile.isEmpty() ? "" : (" ERROR LOG '" + errorLogFile + "'"));										
					
					logger.info(bulkInsert);
					Statement copyStmt = conn.createStatement();
					copyStmt.execute(bulkInsert);
				}
				else if (dc.DBType == DatabaseType.ParAccel)	// no compression or parallel loading of data for ParAccel in this case (for now)
				{
					String tableName = dc.Schema + '.' + name;
					StringBuilder copy = new StringBuilder("COPY " + tableName + " FROM ");
					String fileName = location + '/' + name + ".txt";
					copy.append('\'' + fileName + "' ");
					copy.append("WITH ");
					copy.append("DELIMITER AS '|' ");
					copy.append("DATEFORMAT AS 'YYYY-MM-DD'");
					Statement copyStmt = conn.createStatement();
					copyStmt.execute(copy.toString());
				}
				else if (DatabaseConnection.isDBTypeMySQL(dc.DBType))	// MySQL and Infobright
				{
					String qualifiedTable = dc.Schema + '.' + name;
					String fileName = location + '/' + name + ".txt";
					StringBuilder copy = new StringBuilder("LOAD DATA INFILE '"  + fileName + "' INTO TABLE " + qualifiedTable);
					copy.append(" FIELDS TERMINATED BY '|'");
					logger.debug(copy.toString());
					Statement copyStmt = conn.createStatement();
					copyStmt.execute(copy.toString());
				}				
				else if (DatabaseConnection.isDBTypeMSSQL(dc.DBType))
				{
					// XXX not working, may need a format file (yuck!)
					String qualifiedTable = dc.Schema + '.' + name;
					String fileName = location + '/' + name + ".txt";
					StringBuilder copy = new StringBuilder("BULK INSERT "  + qualifiedTable + " FROM '" + fileName);
					copy.append(" WITH (CODEPAGE='RAW', FIELDTERMINATOR='|', ROWTERMINATOR='\n')");
					logger.debug(copy.toString());
					Statement copyStmt = conn.createStatement();
					copyStmt.execute(copy.toString());
				}
				// XXX missing support for memdb, oracle
				else
				{
					throw new Exception("Database type (" + dc.Type + ") does not have an implementation for bulk loading of time dimensions");
				}

				Hierarchy h = r.findDimensionHierarchy(Name);
				if (i == 0)
				{
					Level l = h.findLevel("Day");
					// Create index - include other surrogate keys if not a time period (for lookups)
					Database.createIndex(dc, l.Keys[0], name, false, false, tp == null ? r.findDimensionTableAtLevel(Name, l.Name) : null, tp == null ? l
							: null, true);
				}
				if (i <= 1)
				{
					Level l = h.findLevel("Week");
					// Create index - include other surrogate keys if not a time period (for lookups)
					if (l != null)
					{
						Database.createIndex(dc, l.Keys[0], name, false, false, tp == null ? r.findDimensionTableAtLevel(Name, l.Name) : null, tp == null ? l
								: null, true);
					}
				}
				if (i <= 2)
					Database.createIndex(dc, "Month_ID", name, false, true);
				if (i <= 3)
					Database.createIndex(dc, "Quarter_ID", name, false, true);
				Database.createIndex(dc, "Half_Year_ID", name, false, true);
				Database.createIndex(dc, "Year_ID", name, false, true);
			}
		}		
	}
	
	
	/**
	 * generate the time schema
	 * - will soon be deprecated once we get the bulk loading working across all DBs
	 * @param dc
	 * @param conn
	 * @param dwm
	 * @param tp
	 * @param r
	 * @throws SQLException
	 * @deprecated
	 */
	private void generateTable(DatabaseConnection dc, boolean[] dwm, TimePeriod tp, Repository r) throws SQLException
	{
		Connection conn = dc.ConnectionPool.getConnection();
		
		String[] dwmPrefix = new String[]
		{ "DY", "WK", "MN", "QT", "HY" };
		String[] levelNames = new String[]
		{ "Day", "Week", "Month", "Quarter", "Half" };
		Map<String, Integer> periodConstantByPrefix = new HashMap<String, Integer>();
		periodConstantByPrefix.put("DY", Calendar.DAY_OF_YEAR);
		periodConstantByPrefix.put("WK", Calendar.WEEK_OF_YEAR);
		periodConstantByPrefix.put("MN", Calendar.MONTH);
		periodConstantByPrefix.put("QT", Calendar.MONTH);
		periodConstantByPrefix.put("HY", Calendar.MONTH);
		for (int i = 0; i < 5; i++)
		{
			if (dwm[i])
			{
				String name = getTableName(tp, levelNames[i], dwmPrefix[i]);

				List<String> colNames = new ArrayList<String>();
				List<String> colTypes = new ArrayList<String>();
				List<String> distkeys = new ArrayList<String>();
				
				generateColumns(dc, i, tp, colNames, colTypes, distkeys,r);

				logger.info("Generating time dimension table " + name);
				boolean created = Database.createTable(r, dc, name, colNames, colTypes, false, false, null, null, null);
				if (!created)
					continue;
				String[] days = new String[]
				{ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
				String[] months = new String[]
				{ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
				Calendar curDate = (Calendar) StartDate.clone();
				StringBuilder sb = new StringBuilder("INSERT INTO " + Database.getQualifiedTableName(dc, name) + " VALUES (?,");
				if (i == 0)
				{
					sb.append("?,?,?,?,?,?,?,?,");
					if (tp != null)
					{
						sb.append("?,");
					}
				}
				if (i <= 1)
				{
					sb.append("?,?,?,?,?,?,?,");
					if (tp != null)
					{
						sb.append("?,");
					}
				}
				if (i <= 2)
				{
					sb.append("?,?,?,?,?,?,?,?,");
					if (tp != null)
					{
						sb.append("?,");
					}
				}
				if (i <= 3)
				{
					sb.append("?,?,?,?,?,");
					if (tp != null)
					{
						sb.append("?,");
					}
				}
				if (i <= 4)
				{
					sb.append("?,?,?,?,?,");
					if (tp != null)
					{
						sb.append("?,");
					}
				}
				if (tp != null)
				{
					sb.append("?,");
				}
				sb.append("?,?,?)");
				logger.debug("Populating " + name);
				logger.debug(sb.toString());
				PreparedStatement stmt = conn.prepareStatement(sb.toString());
				conn.setAutoCommit(false);
				int num = 0;
				while (curDate.getTimeInMillis() <= EndDate.getTimeInMillis())
				{
					Calendar idDate = null;
					Calendar stop = null;
					if (tp == null)
					{
						idDate = (Calendar) curDate.clone();
						stop = (Calendar) curDate.clone();
					} else
					{
						idDate = (Calendar) curDate.clone();
						if (tp.ShiftAmount != 0)
						{
							if (tp.ShiftPeriod == TimePeriod.PERIOD_WEEK)
								idDate.add(Calendar.WEEK_OF_YEAR, (tp.ShiftAgo ? -1 : 1) * tp.ShiftAmount);
							else if (tp.ShiftPeriod == TimePeriod.PERIOD_MONTH)
								idDate.add(Calendar.MONTH, (tp.ShiftAgo ? -1 : 1) * tp.ShiftAmount);
							else if (tp.ShiftPeriod == TimePeriod.PERIOD_QUARTER)
								idDate.add(Calendar.MONTH, (tp.ShiftAgo ? -1 : 1) * tp.ShiftAmount * 3);
							else if (tp.ShiftPeriod == TimePeriod.PERIOD_HALF_YEAR)
								idDate.add(Calendar.MONTH, (tp.ShiftAgo ? -1 : 1) * tp.ShiftAmount * 6);
							else if (tp.ShiftPeriod == TimePeriod.PERIOD_YEAR)
								idDate.add(Calendar.MONTH, (tp.ShiftAgo ? -1 : 1) * tp.ShiftAmount * 12);
						}
						stop = (Calendar) idDate.clone();
						if (tp.AggregationType == TimePeriod.AGG_TYPE_TRAILING)
						{
							if (tp.AggregationPeriod == TimePeriod.PERIOD_DAY)
							{
								idDate.add(Calendar.DAY_OF_YEAR, -tp.AggregationNumPeriods);
								idDate.add(periodConstantByPrefix.get(dwmPrefix[i]), 1);
							} else if (tp.AggregationPeriod == TimePeriod.PERIOD_WEEK)
							{
								idDate.add(Calendar.WEEK_OF_YEAR, -tp.AggregationNumPeriods);
								idDate.add(periodConstantByPrefix.get(dwmPrefix[i]), 1);
							} else if (tp.AggregationPeriod == TimePeriod.PERIOD_MONTH)
							{
								idDate.add(Calendar.MONTH, -tp.AggregationNumPeriods);
								idDate.add(periodConstantByPrefix.get(dwmPrefix[i]), 1);
							}
						} else if (tp.AggregationType == TimePeriod.AGG_TYPE_TO_DATE)
						{
							if (tp.AggregationPeriod == TimePeriod.PERIOD_WEEK)
								idDate.set(Calendar.DAY_OF_WEEK, 1);
							else if (tp.AggregationPeriod == TimePeriod.PERIOD_MONTH)
								idDate.set(Calendar.DAY_OF_MONTH, 1);
							else if (tp.AggregationPeriod == TimePeriod.PERIOD_QUARTER)
							{
								idDate.set(Calendar.DAY_OF_MONTH, 1);
								idDate.add(Calendar.MONTH, -idDate.get(Calendar.MONTH) % 3);
							} else if (tp.AggregationPeriod == TimePeriod.PERIOD_HALF_YEAR)
							{
								idDate.set(Calendar.DAY_OF_MONTH, 1);
								idDate.add(Calendar.MONTH, -idDate.get(Calendar.MONTH) % 6);
							} else if (tp.AggregationPeriod == TimePeriod.PERIOD_YEAR)
							{
								idDate.set(Calendar.DAY_OF_YEAR, 1);
							}
							/**
							 * If the time definition period is week, make sure that the ID date is set to the first day
							 * of week
							 */
							if (i == 1)
							{
								while (idDate.get(Calendar.DAY_OF_WEEK) != WeekDeterminantDay)
								{
									idDate.add(Calendar.DATE, 1);
								}
							}
						}
					}
					while (idDate.getTimeInMillis() <= stop.getTimeInMillis())
					{
						int pos = 1;
						if (i == 1 && (curDate.get(Calendar.DAY_OF_WEEK) != WeekDeterminantDay))
						{
							Calendar weekDeterminantDate = (Calendar) curDate.clone();
							while (weekDeterminantDate.get(Calendar.DAY_OF_WEEK) != WeekDeterminantDay)
							{
								weekDeterminantDate.add(Calendar.DATE, 1);
							}
							stmt.setDate(pos++, new java.sql.Date(weekDeterminantDate.getTimeInMillis()));
						} else
						{
							stmt.setDate(pos++, new java.sql.Date(curDate.getTimeInMillis()));
						}
						if (i == 0)
						{
							int dayNumber = (curDate.get(Calendar.YEAR) * 10000) + ((curDate.get(Calendar.MONTH) + 1) * 100)
									+ curDate.get(Calendar.DAY_OF_MONTH);
							stmt.setInt(pos++, dayNumber);
							stmt.setInt(pos++, getDayID(curDate));
							stmt.setString(pos++, days[curDate.get(Calendar.DAY_OF_WEEK) - 1]);
							stmt.setInt(pos++, getDayOfWeek(curDate));
							stmt.setInt(pos++, curDate.get(Calendar.DAY_OF_WEEK_IN_MONTH));
							stmt.setInt(pos++, curDate.get(Calendar.DAY_OF_MONTH));
							stmt.setInt(pos++, curDate.get(Calendar.DAY_OF_YEAR));
							if (tp != null)
							{
								// Day_ID and TTM_Day_ID
								stmt.setInt(pos++, getDayID(curDate));
								stmt.setInt(pos++, getDayID(idDate));
							} else
							{
								// Just Day_ID
								stmt.setInt(pos++, getDayID(idDate));
							}
						}
						if (i <= 1)
						{
							Calendar weekStartDate = (Calendar) curDate.clone();
							while (weekStartDate.get(Calendar.DAY_OF_WEEK) != FirstDayOfWeek)
							{
								weekStartDate.add(Calendar.DATE, -1);
							}
							Calendar weekEndDate = (Calendar) weekStartDate.clone();
							weekEndDate.add(Calendar.DATE, 6);
							int weekOfMonth = curDate.get(Calendar.WEEK_OF_MONTH);
							int weekNumber = 0;
							if (weekOfMonth == 0)
							{
								weekOfMonth = weekStartDate.get(Calendar.WEEK_OF_MONTH);
								weekNumber = (weekStartDate.get(Calendar.YEAR) * 10000) + ((weekStartDate.get(Calendar.MONTH) + 1) * 100) + weekOfMonth;
							} else
							{
								weekNumber = (curDate.get(Calendar.YEAR) * 10000) + ((curDate.get(Calendar.MONTH) + 1) * 100)
										+ curDate.get(Calendar.WEEK_OF_MONTH);
							}
							stmt.setInt(pos++, weekNumber);
							stmt.setInt(pos++, getWeekID(curDate));
							stmt.setInt(pos++, weekOfMonth);
							stmt.setInt(pos++, curDate.get(Calendar.WEEK_OF_YEAR));
							stmt.setDate(pos++, new java.sql.Date(weekStartDate.getTimeInMillis()));
							stmt.setDate(pos++, new java.sql.Date(weekEndDate.getTimeInMillis()));
							if (tp != null)
							{
								// Week_ID and TTM_Week_ID
								stmt.setInt(pos++, getWeekID(curDate));
								stmt.setInt(pos++, getWeekID(idDate));
							} else
							{
								// Just Week_ID
								stmt.setInt(pos++, getWeekID(idDate));
							}
						}
						if (i <= 2)
						{
							int monthNumber = (curDate.get(Calendar.YEAR) * 100) + curDate.get(Calendar.MONTH) + 1;
							stmt.setInt(pos++, monthNumber);
							stmt.setInt(pos++, getMonthID(curDate));
							stmt.setString(pos++, months[curDate.get(Calendar.MONTH)]);
							int mo = curDate.get(Calendar.MONTH) + 1;
							stmt.setString(pos++, String.format("%4d/%02d", curDate.get(Calendar.YEAR), mo));
							stmt.setInt(pos++, mo);
							Calendar qagoDate = (Calendar) curDate.clone();
							qagoDate.add(Calendar.MONTH, -2);
							int qagomo = qagoDate.get(Calendar.MONTH) + 1;
							int yearnum = curDate.get(Calendar.YEAR) % 100;
							int qagoyearnum = qagoDate.get(Calendar.YEAR) % 100;
							StringBuilder trailing3MonthNameBuf = new StringBuilder();
							trailing3MonthNameBuf.append(String.format("%02d", qagomo));
							trailing3MonthNameBuf.append("/");
							trailing3MonthNameBuf.append(String.format("%02d", qagoyearnum));
							trailing3MonthNameBuf.append(" - ");
							trailing3MonthNameBuf.append(String.format("%02d", mo));
							trailing3MonthNameBuf.append("/");
							trailing3MonthNameBuf.append(String.format("%02d", yearnum));
							stmt.setString(pos++, trailing3MonthNameBuf.toString());
							int numWeeksInMonth = getNumWeeksInMonth(curDate);
							stmt.setInt(pos++, numWeeksInMonth);
							if (tp != null)
							{
								// Month_ID and TTM_Month_ID
								stmt.setInt(pos++, getMonthID(curDate));
								stmt.setInt(pos++, getMonthID(idDate));
							} else
							{
								// Just Month_ID
								stmt.setInt(pos++, getMonthID(idDate));
							}
						}
						if (i <= 3)
						{
							int q = (curDate.get(Calendar.MONTH) / 3) + 1;
							int quarterNumber = (curDate.get(Calendar.YEAR) * 10) + q;
							stmt.setInt(pos++, quarterNumber);
							stmt.setInt(pos++, getQuarterID(curDate));
							stmt.setString(pos++, "Q" + q);
							stmt.setString(pos++, curDate.get(Calendar.YEAR) + "/Q" + q);
							if (tp != null)
							{
								// Quarter_ID and TTM_Quarter_ID
								stmt.setInt(pos++, getQuarterID(curDate));
								stmt.setInt(pos++, getQuarterID(idDate));
							} else
							{
								// Just Quarter_ID
								stmt.setInt(pos++, getQuarterID(idDate));
							}
						}
						if (i <= 4)
						{
							int h = (curDate.get(Calendar.MONTH) / 6) + 1;
							int halfYearNumber = (curDate.get(Calendar.YEAR) * 100) + h;
							stmt.setInt(pos++, halfYearNumber);
							stmt.setInt(pos++, getHalfYearID(curDate));
							stmt.setString(pos++, "H" + h);
							stmt.setString(pos++, curDate.get(Calendar.YEAR) + "/H" + h);
							if (tp != null)
							{
								// Quarter_ID and TTM_Half_Year_ID
								stmt.setInt(pos++, getHalfYearID(curDate));
								stmt.setInt(pos++, getHalfYearID(idDate));
							} else
							{
								// Just Half_Year_ID
								stmt.setInt(pos++, getHalfYearID(curDate));
							}
						}
						if (tp != null)
						{
							stmt.setDate(pos++, new java.sql.Date(idDate.getTimeInMillis()));
						}
						int yearNumber = curDate.get(Calendar.YEAR);
						stmt.setInt(pos++, yearNumber);
						stmt.setInt(pos++, getYearID(curDate));
						stmt.setInt(pos++, getYearID(curDate));
						if (dc.DBType == Database.DatabaseType.MonetDB)
						{
							stmt.execute();
							if (++num % 5000 == 0)
							{
								stmt.close();
								stmt = conn.prepareStatement(sb.toString());
								logger.debug("inserted " + num + " records");
							}
						} else
						{
							stmt.addBatch();
							if (++num % 25000 == 0)
							{
								stmt.executeBatch();
								conn.commit();
								logger.debug("inserted " + num + " records");
							}
						}
						if (i == 0)
							idDate.add(Calendar.DATE, 1);
						else if (i == 1)
							idDate.add(Calendar.WEEK_OF_YEAR, 1);
						else if (i == 2)
							idDate.add(Calendar.MONTH, 1);
						else if (i == 3)
							idDate.add(Calendar.MONTH, 3);
						else if (i == 4)
							idDate.add(Calendar.MONTH, 6);
					}
					if (i == 0)
					{
						curDate.add(Calendar.DATE, 1);
					} else if (i == 1)
					{
						curDate.add(Calendar.WEEK_OF_YEAR, 1);
					} else if (i == 2)
					{
						curDate.add(Calendar.MONTH, 1);
					} else if (i == 3)
					{
						curDate.add(Calendar.MONTH, 3);
					} else if (i == 4)
					{
						curDate.add(Calendar.MONTH, 6);
					}
				}
				stmt.executeBatch();
				conn.commit();
				stmt.close();
				conn.setAutoCommit(true);
				logger.debug("finished populating " + name + ", inserted " + num + " records");
				Hierarchy h = r.findDimensionHierarchy(Name);
				if (i == 0)
				{
					Level l = h.findLevel("Day");
					// Create index - include other surrogate keys if not a time period (for lookups)
					Database.createIndex(dc, l.Keys[0], name, false, false, tp == null ? r.findDimensionTableAtLevel(Name, l.Name) : null, tp == null ? l
							: null, true);
				}
				if (i <= 1)
				{
					Level l = h.findLevel("Week");
					// Create index - include other surrogate keys if not a time period (for lookups)
					if (l != null)
						Database.createIndex(dc, l.Keys[0], name, false, false, tp == null ? r.findDimensionTableAtLevel(Name, l.Name) : null, tp == null ? l
								: null, true);
				}
				if (i <= 2)
					Database.createIndex(dc, "Month_ID", name, false, true);
				if (i <= 3)
					Database.createIndex(dc, "Quarter_ID", name, false, true);
				Database.createIndex(dc, "Half_Year_ID", name, false, true);
				Database.createIndex(dc, "Year_ID", name, false, true);
			}
		}
	}
	
	/**
	 * helper class to make it easier to generate the time files from what was originally code to load the database
	 */
	public class FileStatement
	{
		private BufferedWriter bw = null;
		private SimpleDateFormat sdf = null;
		
		FileStatement(OutputStreamWriter os)
		{
			bw = new BufferedWriter(os);
			sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
		}
		
		public void addBatch() throws IOException
		{
			bw.write('\n');
		}
		
		public void close() throws IOException
		{
			bw.close();
		}
		
		public void setInt(int pos, int value) throws IOException
		{
			if (pos > 1)
				bw.write('|');
			bw.write(String.valueOf(value));
		}
		
		public void setString(int pos, String value) throws IOException
		{
			if (pos > 1)
				bw.write('|');
			bw.write(value);
		}
		
		public void setDate(int pos, java.sql.Date value) throws IOException
		{
			if (pos > 1)
				bw.write('|');
			bw.write(sdf.format(value));
		}
	}
}
