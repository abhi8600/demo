/**
 * $Id: TableLookup.java,v 1.5 2010-12-20 12:48:03 mpandit Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.util.Util;

/**
 * @author Brad Peters
 * 
 */
public class TableLookup extends Transformation implements Serializable
{
	private static final long serialVersionUID = 1L;
	public String Formula;
	public String LookupTable;
	public String JoinCondition;

	public TableLookup(Element e, Namespace ns)
	{
		Formula = e.getChildTextTrim("Formula", ns);
		LookupTable = e.getChildTextTrim("LookupTable", ns);
		JoinCondition = e.getChildTextTrim("JoinCondition", ns);
	}
	
	public Element getTableLookupElement(Namespace ns)
	{
		Element e = new Element("TableLookup",ns);
		
		Element child = new Element("ExecuteAfter",ns);
		child.setText(String.valueOf(iExecuteAfter));
		e.addContent(child);
		
		child = new Element("Formula",ns);
		child.setText(Formula);
		e.addContent(child);
		
		child = new Element("LookupTable",ns);
		child.setText(LookupTable);
		e.addContent(child);
		
		child = new Element("JoinCondition",ns);
		child.setText(JoinCondition);
		e.addContent(child);
		
		return e;
	}

	public String getPhysicalFormula()
	{
		return (getReplacedString(Formula));
	}

	public List<String> getLookupJoins(String stagingTableQualifier, String alias)
	{
		List<String> list = new ArrayList<String>(1);
		String s = getReplacedString(JoinCondition);
		if (stagingTableQualifier != null)
			s = Util.replaceStr(s, TableName + ".", stagingTableQualifier + ".");
		if (alias != null)
			s = Util.replaceStr(s, LookupTable + ".", alias + ".");
		list.add(s);
		return (list);
	}

	public List<String> getLookupTables()
	{
		List<String> list = new ArrayList<String>(1);
		list.add(LookupTable);
		return (list);
	}
}
