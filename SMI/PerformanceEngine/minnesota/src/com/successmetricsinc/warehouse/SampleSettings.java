/**
 * 
 */
package com.successmetricsinc.warehouse;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author bpeters
 * 
 */
public class SampleSettings implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public List<SampleMap> map = new ArrayList<SampleMap>();

	public void addMap(String dimension, String level, String stagingTableName)
	{
		boolean found = false;
		for (SampleMap sm : map)
		{
			if (sm.Dimension.equals(dimension) && sm.Level.equals(level) && sm.StagingTableName.equals(stagingTableName))
			{
				found = true;
				break;
			}
		}
		if (!found)
		{
			SampleMap sm = new SampleMap();
			sm.Dimension = dimension;
			sm.Level = level;
			sm.StagingTableName = stagingTableName;
			map.add(sm);
		}
	}

	public void removeMap(String dimension, String level, String stagingTableName)
	{
		SampleMap foundsm = null;
		for (SampleMap sm : map)
		{
			if (sm.Dimension.equals(dimension) && sm.Level.equals(level) && sm.StagingTableName.equals(stagingTableName))
			{
				foundsm = sm;
				break;
			}
		}
		if (foundsm != null)
		{
			map.remove(foundsm);
		}
	}
}
