/**
 * $Id: StaticDateID.java,v 1.10 2010-12-20 12:48:04 mpandit Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.Repository;
import com.successmetricsinc.util.DateUtil;

/**
 * @author Brad Peters
 * 
 */
public class StaticDateID extends Transformation implements Serializable
{
	private static final long serialVersionUID = 1L;
	public Calendar DateValue;
	public String Expression;
	public int DateType;

	public StaticDateID(Element e, Namespace ns)
	{
		Expression = e.getChildTextTrim("DateExpression", ns);
		setFormat(Expression);
		DateType = Integer.valueOf(e.getChildText("DateType", ns));
	}
	
	public Element getDateIDElement(Namespace ns)
	{
		Element e = new Element("DateID",ns);
		
		Element child = new Element("ExecuteAfter",ns);
		child.setText(String.valueOf(iExecuteAfter));
		e.addContent(child);
		
		child = new Element("DateExpression",ns);
		child.setText(Expression);
		e.addContent(child);
		
		child = new Element("DateType",ns);
		child.setText(String.valueOf(DateType));
		e.addContent(child);
		
		return e;
	}

	public int getPhysicalFormula(Repository r)
	{
		if (DateValue == null)
		{
			Expression = r.replaceVariables(null, Expression);
			setFormat(Expression);
		}
		if (DateType == TimePeriod.PERIOD_DAY)
			return (r.getTimeDefinition().getDayID(DateValue));
		else if (DateType == TimePeriod.PERIOD_WEEK)
			return (r.getTimeDefinition().getWeekID(DateValue));
		return (r.getTimeDefinition().getMonthID(DateValue));
	}

	private void setFormat(String expression)
	{
		if (expression == null || expression.length() == 0)
		{
			DateValue = Calendar.getInstance();
			DateValue.setTimeInMillis(System.currentTimeMillis());
			return;
		}
		if (expression.charAt(0) == '\'' && expression.endsWith("\'"))
			expression = expression.substring(1, expression.length() - 1);
		Date d = DateUtil.parseUsingPossibleFormats(expression);
		if (d != null)
		{
			DateValue = Calendar.getInstance();
			DateValue.setTime(d);
		}
	}
}
