package com.successmetricsinc.warehouse;

import java.io.Serializable;

import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.Repository;

public class CustomTimeShift implements Serializable 
{
  public String BaseKey;
  public String RelativeKey;
  public String Prefix;
  public String[] Columns;
  
  public CustomTimeShift(Element e, Namespace ns)
  {
    BaseKey= e.getChildText("BaseKey",ns);
    RelativeKey= e.getChildText("RelativeKey",ns);
    Prefix= e.getChildText("Prefix",ns);
    Columns= Repository.getStringArrayChildren(e, "Columns", ns);
  }
  
  public Element getCustomTimeShiftElement(Namespace ns)
  {
    Element e = new Element("CustomTimeShift",ns);
    
    Element child = new Element("BaseKey",ns);
    if (BaseKey!=null && !BaseKey.isEmpty())
    {
    	child.setText(BaseKey);
    }
    e.addContent(child);
    
    child = new Element("RelativeKey",ns);
    if (RelativeKey!=null && !RelativeKey.isEmpty())
    {
    	child.setText(RelativeKey);
    }
    e.addContent(child);
    
    child = new Element("Prefix",ns);
    if (Prefix!=null && !Prefix.isEmpty())
    {
    	child.setText(Prefix);
    }
    e.addContent(child);
    
    if (Columns!=null && Columns.length>0)
    {
      e.addContent(Repository.getStringArrayElement("Columns", Columns, ns));
    }
    
    return e;
  }
}
