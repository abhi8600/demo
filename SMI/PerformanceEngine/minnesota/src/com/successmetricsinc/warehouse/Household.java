/**
 * $Id: Household.java,v 1.14 2010-12-20 12:48:03 mpandit Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.JasperDataSourceProvider;
import com.successmetricsinc.query.NavigationException;
import com.successmetricsinc.query.Query;
import com.successmetricsinc.query.SyntaxErrorException;

/**
 * @author Brad Peters
 * 
 */
public class Household extends Transformation implements Serializable
{
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(Household.class);
	public String KeyColumn;
	public String HHIDColumn;
	public String Address;
	public String City;
	public String State;
	public String Zip;
	public String TargetTable;
	public String TargetKeyColumn;
	public String TargetHHIDColumn;
	public String TargetAddress;
	public String TargetCity;
	public String TargetState;
	public String TargetZip;
	/**
	 * long and short values for common address strings
	 */
	private String[][] AddressSynonyms =
	{
	{ "avenue", "ave" },
	{ "boulevard", "blvd" },
	{ "road", "rd" },
	{ "street", "st" } };
	/**
	 * state names and abbreviations
	 */
	private String[][] States =
	{
	{ "alabama", "al" },
	{ "alaska", "al" },
	{ "arizona", "az" },
	{ "california", "ca" },
	{ "colorado", "co" },
	{ "connecticut", "ct" },
	{ "delaware", "de" },
	{ "florida", "fl" },
	{ "georgia", "ga" },
	{ "hawaii", "hi" },
	{ "idaho", "id" },
	{ "illinois", "il" },
	{ "indiana", "in" },
	{ "iowa", "ia" },
	{ "kansas", "ka" },
	{ "kentucky", "ky" },
	{ "louisiana", "la" },
	{ "maine", "me" },
	{ "maryland", "md" },
	{ "massachussetts", "ma" },
	{ "michigan", "mi" },
	{ "minnesota", "mn" },
	{ "mississippi", "ms" },
	{ "missouri", "mo" },
	{ "montana", "mt" },
	{ "nebraska", "ne" },
	{ "nevada", "nv" },
	{ "new hampshire", "nh" },
	{ "new jersey", "nj" },
	{ "new mexico", "nm" },
	{ "new york", "ny" },
	{ "north carolina", "nc" },
	{ "north dakota", "nd" },
	{ "ohio", "oh" },
	{ "oklahoma", "ok" },
	{ "oregon", "or" },
	{ "pennsylvania", "pa" },
	{ "rhode island", "ri" },
	{ "south carolina", "sc" },
	{ "south dakota", "sd" },
	{ "tennessee", "tn" },
	{ "texas", "tx" },
	{ "utah", "ut" },
	{ "vermont", "vt" },
	{ "virginia", "va" },
	{ "washington", "wa" },
	{ "west virginia", "wv" },
	{ "wisconsin", "wi" },
	{ "wyoming", "wy" } };

	public Household(Element e, Namespace ns)
	{
		KeyColumn = e.getChildTextTrim("KeyColumn", ns);
		HHIDColumn = e.getChildTextTrim("HHIDColumn", ns);
		Address = e.getChildTextTrim("Address", ns);
		City = e.getChildTextTrim("City", ns);
		State = e.getChildTextTrim("State", ns);
		Zip = e.getChildTextTrim("Zip", ns);
		TargetTable = e.getChildTextTrim("TargetTable", ns);
		TargetKeyColumn = e.getChildTextTrim("TargetKeyColumn", ns);
		TargetHHIDColumn = e.getChildTextTrim("HHIDColumn", ns);
		TargetAddress = e.getChildTextTrim("TargetAddress", ns);
		TargetCity = e.getChildTextTrim("TargetCity", ns);
		TargetState = e.getChildTextTrim("TargetState", ns);
		TargetZip = e.getChildTextTrim("TargetZip", ns);
	}
	
	public Element getHouseholdElement(Namespace ns)
	{
		Element e = new Element("Household",ns);
		
		Element child = new Element("ExecuteAfter",ns);
		child.setText(String.valueOf(iExecuteAfter));
		e.addContent(child);
		
		child = new Element("KeyColumn",ns);
		child.setText(KeyColumn);
		e.addContent(child);
		
		child = new Element("HHIDColumn",ns);
		child.setText(HHIDColumn);
		e.addContent(child);
		
		child = new Element("Address",ns);
		child.setText(Address);
		e.addContent(child);
		
		child = new Element("City",ns);
		child.setText(City);
		e.addContent(child);
		
		child = new Element("State",ns);
		child.setText(State);
		e.addContent(child);
		
		child = new Element("Zip",ns);
		child.setText(Zip);
		e.addContent(child);
		
		child = new Element("TargetTable",ns);
		child.setText(TargetTable);
		e.addContent(child);
		
		child = new Element("TargetKeyColumn",ns);
		child.setText(TargetKeyColumn);
		e.addContent(child);
		
		child = new Element("TargetHHIDColumn",ns);
		child.setText(TargetHHIDColumn);
		e.addContent(child);
		
		child = new Element("TargetAddress",ns);
		child.setText(TargetAddress);
		e.addContent(child);
		
		child = new Element("TargetCity",ns);
		child.setText(TargetCity);
		e.addContent(child);
		
		child = new Element("TargetState",ns);
		child.setText(TargetState);
		e.addContent(child);
		
		child = new Element("TargetZip",ns);
		child.setText(TargetZip);
		e.addContent(child);
		
		return e;
	}

	/**
	 * Execute householding process
	 * 
	 * @param r
	 * @param dc
	 * @param s
	 * @param jdsp
	 * @return
	 */
	public void execute(Repository r, DatabaseConnection dc, Session s, JasperDataSourceProvider jdsp) throws NavigationException, SQLException, IOException,
			SyntaxErrorException, CloneNotSupportedException
	{
		processReplacements();
		Connection conn = dc.ConnectionPool.getConnection();
		Statement stmt = conn.createStatement();
		// First, update any already assigned household IDs
		String query = "UPDATE " + TableName + " SET " + HHIDColumn + "=B." + TargetHHIDColumn + " FROM " + TableName + " A, " + TargetTable + " B WHERE A."
				+ HHIDColumn + " IS NULL AND B." + TargetHHIDColumn + " IS NOT NULL";
		logger.info("Updating previously assigned household IDs");
		logger.debug(Query.getPrettyQuery(query));
		stmt.executeUpdate(query);
		query = "SELECT " + TargetHHIDColumn + "," + TargetAddress + "," + TargetCity + "," + TargetState + "," + TargetZip + " FROM " + TargetTable;
		logger.info("Retrieving existing householding assignments");
		logger.debug(Query.getPrettyQuery(query));
		ResultSet rs = stmt.executeQuery(query);
		Map<String, Integer> mappings = new HashMap<String, Integer>();
		// Get existing HHID mappings
		int maxID = -1;
		while (rs.next())
		{
			int HHID = rs.getInt(1);
			maxID = Math.max(maxID, HHID);
			List<String> addrKeys = getAddressKeys(rs.getString(2).toLowerCase(), rs.getString(3).toLowerCase(), rs.getString(4).toLowerCase(), rs.getString(5));
			for (String addrKey : addrKeys)
				mappings.put(addrKey, HHID);
		}
		// Select all rows where HHID is still null
		query = "SELECT " + Address + "," + City + "," + State + "," + Zip + " FROM " + TableName + " WHERE " + HHIDColumn + " IS NULL GROUP BY " + Address
				+ "," + City + "," + State + "," + Zip;
		logger.info("Setting householding assignments");
		logger.debug(Query.getPrettyQuery(query));
		// get all househoulds with a null household ID
		rs = stmt.executeQuery(query);
		// Update those rows with the appropriately assigned new HHID
		PreparedStatement updstmt = conn.prepareStatement("UPDATE " + TableName + " SET " + HHIDColumn + "=%1 WHERE " + Address + "=%2 AND " + City + "=%3 "
				+ State + "=%4 AND " + Zip + "=%5");
		// Get existing mappings
		int count = 0;
		while (rs.next())
		{
			String addr = rs.getString(1);
			String city = rs.getString(2);
			String state = rs.getString(3);
			String zip = rs.getString(4);
			// find all possible address key values for the address
			List<String> addrKeys = getAddressKeys(addr.toLowerCase(), city.toLowerCase(), state.toLowerCase(), zip);
			Integer ID = null;
			// match the possible address key values against existing address keys
			for (String key : addrKeys)
			{
				Integer ID2 = mappings.get(key);
				if (ID2 != null)
				{
					ID = ID2;
					break;
				}
			}
			// did not find a match, new household, add this to the list of existing address keys
			if (ID == null)
			{
				ID = ++maxID;
				for (String key : addrKeys)
					mappings.put(key, ID);
			}
			// update the household ID
			updstmt.setInt(1, ID);
			updstmt.setString(2, addr); // isn't there a sequence # or something else we can match on? using these keys
										// is inefficient
			updstmt.setString(3, city); // we could also just use a cursor and update the id of the record we are
										// currently on
			updstmt.setString(4, state);
			updstmt.setString(5, zip);
			updstmt.addBatch();
			if (count % 1000 == 0)
				updstmt.executeBatch();
		}
		if (count % 1000 != 0)
			updstmt.executeBatch();
		updstmt.close();
		stmt.close();
	}

	/**
	 * remove extra spaces and common address punctuation marks XXX probably should remove tabs
	 * 
	 * @param s
	 * @return
	 */
	private String cleanStr(String s)
	{
		char[] newArr = new char[s.length()];
		int count = 0;
		boolean lastSpace = false;
		for (char c : s.toCharArray())
		{
			if (c != '.' && c != ',' && c != '\r' & c != '\n' && !(lastSpace && c == ' '))
			{
				newArr[count++] = c;
				lastSpace = c == ' ';
			}
		}
		s = String.valueOf(newArr, 0, count);
		return (s);
	}

	/**
	 * build up a set of canonicalized address keys (not canonicalized addresses) that can be used for matching
	 * 
	 * @param address
	 * @param city
	 * @param state
	 * @param zip
	 * @return
	 */
	private List<String> getAddressKeys(String address, String city, String state, String zip)
	{
		List<String> results = new ArrayList<String>();
		zip = zip == null ? null : (zip.length() <= 5 ? zip : zip.substring(0, 5));
		if (zip != null && zip.length() > 0 && state != null && state.length() > 0)
			results.add(cleanStr("[" + getShort(address, AddressSynonyms) + "][" + city + "][" + getShort(state, States) + "][" + zip + "]"));
		if (zip != null && zip.length() > 0)
			results.add(cleanStr("[" + getShort(address, AddressSynonyms) + "][" + city + "][" + zip + "]"));
		if (state != null && state.length() > 0)
			results.add(cleanStr("[" + getShort(address, AddressSynonyms) + "][" + city + "][" + getShort(state, States) + "]"));
		return (results);
	}

	/**
	 * replace all occurances of long address names in the string with short ones, not trying to build a proper address,
	 * but a key fragment
	 * 
	 * @param s
	 * @param Synonyms
	 * @return
	 */
	private String getShort(String s, String[][] Synonyms)
	{
		if (s == null)
			return ("");
		for (String[] arr : Synonyms)
		{
			s = s.replace(arr[0], arr[1]);
		}
		return (s);
	}

	/**
	 * get the meta data defined physical column names
	 */
	private void processReplacements()
	{
		HHIDColumn = getReplacedString("HHIDColumn");
		Address = getReplacedString("Address");
		City = getReplacedString("City");
		State = getReplacedString("State");
		Zip = getReplacedString("Zip");
		TargetTable = getReplacedString("TargetTable");
		TargetHHIDColumn = getReplacedString("HHIDColumn");
		TargetAddress = getReplacedString("TargetAddress");
		TargetCity = getReplacedString("TargetCity");
		TargetState = getReplacedString("TargetState");
		TargetZip = getReplacedString("TargetZip");
	}
}
