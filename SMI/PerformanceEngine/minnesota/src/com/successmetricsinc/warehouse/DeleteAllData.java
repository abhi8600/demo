package com.successmetricsinc.warehouse;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.query.Database;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.Query;

/**
 * Delete All previously loaded data
 * 
 * @author Rohan
 * 
 */
public class DeleteAllData
{
	private static Logger logger = Logger.getLogger(DeleteAllData.class);
	DatabaseConnection dconn;
	Repository r;
	
	public DeleteAllData(Repository r)
	{
		this.r = r;
	}

	public void delete() throws SQLException
	{
		dconn = r.getDefaultConnection();
		Connection conn = dconn.ConnectionPool.getConnection();
		Statement stmt = null;
		ResultSet rs = null;
		try
		{
			String schema = dconn.Schema;
			if (schema == null || schema.trim().isEmpty())
			{
				logger.info("Can not initiate delete all published data for application: " + r.getApplicationName());
				return;
			}
			stmt = conn.createStatement();
			logger.debug("Get schema tables");
			String selectTablesQuery = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='" + schema + "'";
			logger.debug(Query.getPrettyQuery(selectTablesQuery));
			rs = stmt.executeQuery(selectTablesQuery);
			Set<String> tnames = new HashSet<String> ();
			while (rs.next())
			{
				tnames.add(rs.getString(1));
			}
			if (tnames.size() > 0)
			{
				for (String tableName : tnames) 
				{
					dropTable(stmt, tableName);
				}
			}
		}
		catch (SQLException sqle)
		{
			logger.error(sqle.getMessage(), sqle);
			throw new SQLException("SQLException during DeleteAllData delete method ", sqle);
		}
		finally
		{
			try
			{
				if (rs != null)
					rs.close();
				if (stmt != null)
					stmt.close();
			}
			catch (SQLException e)
			{
				throw new SQLException(e);
			}
		}		
	}

	private void dropTable(Statement stmt, String tableName) throws SQLException
	{
		String dropTableStr = "DROP TABLE ";
		try
		{
			String qualifiedTableName = Database.getQualifiedTableName(dconn, tableName);
			String dropTableQuery = dropTableStr + qualifiedTableName;
			logger.debug("Dropping table: " + qualifiedTableName);
			logger.debug(Query.getPrettyQuery(dropTableQuery));
			stmt.executeUpdate(dropTableQuery);
		}
		catch (SQLException sqle)
		{
			logger.error(sqle.getMessage(), sqle);
			throw sqle;
		}
	}
}
