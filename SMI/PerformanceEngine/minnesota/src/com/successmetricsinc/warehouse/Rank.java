/**
 * $Id: Rank.java,v 1.17 2012-06-18 17:24:14 ricks Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.Repository;
import com.successmetricsinc.query.Database.DatabaseType;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.Query;

/**
 * @author Brad Peters
 * 
 */
public class Rank extends Transformation implements Serializable
{
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(Query.class);

	public enum RankType
	{
		Rank, NTile
	};
	public RankType Type;
	public int NTileNum;
	public String[] PartitionByColumns;
	public String[] OrderByColumns;
	public boolean[] OrderByAscending;
	private int x;

	public Rank(Element e, Namespace ns)
	{
		x = Integer.valueOf(e.getChildTextTrim("Type", ns));
		Type = x == 0 ? RankType.Rank : RankType.NTile;
		NTileNum = Integer.valueOf(e.getChildTextTrim("NTileNum", ns));
		PartitionByColumns = Repository.getStringArrayChildren(e, "PartitionByColumns", ns);
		OrderByColumns = Repository.getStringArrayChildren(e, "OrderByColumns", ns);
		OrderByAscending = Repository.getBooleanArrayChildren(e, "OrderByAscending", ns);
	}

	public Rank(String name, String body)
	{
	}
	
	public Element getRankElement(Namespace ns)
	{
		Element e = new Element("Rank",ns);
		
		Element child = new Element("ExecuteAfter",ns);
		child.setText(String.valueOf(iExecuteAfter));
		e.addContent(child);
		
		child = new Element("Type",ns);
		child.setText(String.valueOf(x));
		e.addContent(child);
		
		child = new Element("NTileNum",ns);
		child.setText(String.valueOf(NTileNum));
		e.addContent(child);
		
		child = new Element("PartitionByColumns",ns);
		if (PartitionByColumns!=null && PartitionByColumns.length>0)
		{
			for (String partitionByColumn : PartitionByColumns)
			{
				Element stringElement = new Element("string",ns);
				stringElement.setText(partitionByColumn);
				child.addContent(stringElement);
			}
		}
		e.addContent(child);
		
		child = new Element("OrderByColumns",ns);
		if (OrderByColumns!=null && OrderByColumns.length>0)
		{
			for (String strOrderByColumn : OrderByColumns)
			{
				Element stringElement = new Element("string",ns);
				stringElement.setText(strOrderByColumn);
				child.addContent(stringElement);
			}
		}
		e.addContent(child);
		
		child = new Element("OrderByAscending",ns);
		if (OrderByAscending!=null && OrderByAscending.length>0)
		{
			for (Boolean orderByAsc : OrderByAscending)
			{
				Element stringElement = new Element("boolean",ns);
				stringElement.setText(String.valueOf(orderByAsc));
				child.addContent(stringElement);
			}
		}
		e.addContent(child);
		
		return e;
	}

	/**
	 * Process a set of ranks in one pass
	 * 
	 * @param r
	 *            Repository
	 * @param dc
	 *            Database connection to use
	 * @param name
	 *            Name of table to process
	 * @param tmap
	 *            Map of column names to transformations
	 * @param keys
	 *            List of keys for the table to process
	 * @throws SQLException
	 */
	public static void processRanks(Repository r, DatabaseConnection dc, String name, Map<String, Transformation> tmap, List<String> keys) throws SQLException
	{
		Statement stmt = dc.ConnectionPool.getConnection().createStatement();
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE " + name + " SET ");
		int count = 0;
		for (Entry<String, Transformation> en : tmap.entrySet())
		{
			if (count > 0)
				sb.append(',');
			sb.append(en.getKey() + "=S.R" + count);
			count++;
		}
		sb.append(" FROM " + name + " INNER JOIN (SELECT ");
		boolean first = true;
		for (String s : keys)
		{
			if (first)
				first = false;
			else
				sb.append(',');
			sb.append(name + "." + s);
		}
		count = 0;
		for (Entry<String, Transformation> en : tmap.entrySet())
		{
			if (first)
				first = false;
			else
				sb.append(',');
			sb.append(((Rank) en.getValue()).getPredicate(r, dc, "R" + (count++)));
		}
		sb.append(" FROM " + name + ") S ON ");
		first = true;
		for (String s : keys)
		{
			if (first)
				first = false;
			else
				sb.append(" AND ");
			sb.append(name + "." + s + "=S." + s);
		}
		logger.info("Updating Ranks");
		logger.debug(Query.getPrettyQuery(sb.toString()));
		int cnt = stmt.executeUpdate(sb.toString());
		logger.info("Update record count: " + cnt);
		stmt.close();
	}

	/**
	 * @param r
	 * @param dc
	 * @param name
	 * @return
	 */
	public String getPredicate(Repository r, DatabaseConnection dc, String name)
	{
		if (DatabaseConnection.isDBTypeMSSQL(dc.DBType))
		{
			StringBuilder sb = new StringBuilder();
			if (Type == RankType.Rank)
				sb.append("RANK()");
			else
				sb.append("NTILE(" + NTileNum + ")");
			if (PartitionByColumns.length + OrderByColumns.length > 0)
			{
				sb.append(" OVER (");
				boolean firstpb = true;
				for (String s : PartitionByColumns)
				{
					if (firstpb)
					{
						sb.append("PARTITION BY " + getReplacedString(s));
						firstpb = false;
					} else
						sb.append("," + getReplacedString(s));
				}
				boolean firstob = true;
				for (int i = 0; i < OrderByColumns.length; i++)
				{
					if (firstob)
					{
						if (!firstpb)
							sb.append(' ');
						sb.append("ORDER BY ");
						firstob = false;
					} else
					{
						sb.append(",");
					}
					sb.append(getReplacedString(OrderByColumns[i]));
					sb.append(" ");
					sb.append(OrderByAscending[i] ? "ASC" : "DESC");
				}
				sb.append(") " + name);
			}
			return (sb.toString());
		}
		return (null);
	}
}
