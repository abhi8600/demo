/**
 * 
 */
package com.successmetricsinc.warehouse;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.Query;
import com.successmetricsinc.util.Util;

/**
 * @author bpeters
 * 
 */
public class LoadInfoBright
{
	private static Logger logger = Logger.getLogger(Bulkload.class);
	private Repository r;

	public LoadInfoBright(Repository r)
	{
		this.r = r;
	}

	public void load(String directory)
	{
		DatabaseConnection dc = r.findConnection("IB Connection");
		Statement stmt = null;
		try
		{
			Connection conn = dc.ConnectionPool.getConnection();
			stmt = conn.createStatement();
			stmt.executeUpdate("use " + dc.Schema);

			File df = new File(directory);
			for (File f : df.listFiles())
			{
				String fname = f.getName();
				if (!fname.endsWith(".dat"))
					continue;
				int index = fname.lastIndexOf('.');
				String tname = fname.substring(0, index);
				String command;
				command = "LOAD DATA INFILE '" + Util.replaceStr(f.getPath(), "\\", "\\\\") + "' INTO TABLE " + tname
						+ " FIELDS TERMINATED BY '|' ENCLOSED BY '\"' ESCAPED BY '\\\\' LINES TERMINATED BY '\\r\\n'";
				logger.debug(Query.getPrettyQuery(command));
				try
				{
					stmt.executeUpdate(command);
				} catch (SQLException e)
				{
					logger.error(e, e);
				}
			}
		} 
		catch (SQLException e)
		{
			logger.error(e, e);
		}
		finally
		{
			if (stmt != null)
			{
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.debug(e, e);
				}
			}
		}
	}
}
