/**
 * $Id: LoadGroup.java,v 1.3 2010-12-20 00:36:15 bpeters Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse;

import java.io.Serializable;
import java.util.List;

import org.jdom.Element;
import org.jdom.Namespace;

public class LoadGroup implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String Name;
	private LoadGroupCondition [] Conditions;
	private boolean isConditionsTag;

	/**
	 * Create a load group structure based on repository XML
	 * 
	 * @param ae
	 */
	public LoadGroup(Element ae, Namespace ns)
	{
		Name = ae.getChildTextTrim("Name", ns);
		Element cae = ae.getChild("Conditions", ns);
		isConditionsTag = cae!=null?true:false;
		if (cae != null)
		{
			List l2 = cae.getChildren();
			Conditions = new LoadGroupCondition[l2.size()];
			for (int count = 0; count < l2.size(); count++)
			{
				Element ale = (Element) l2.get(count);
				Conditions[count] = new LoadGroupCondition(ale, ns);
			}
		}
	}
	
	public Element getLoadGroupElement(Namespace ns)
	{
		Element e = new Element("LoadGroup",ns);
		
		Element child = new Element("Name",ns);
		child.setText(Name);
		e.addContent(child);
		
		if (Conditions!=null && Conditions.length>0)
		{
			child = new Element("Conditions",ns);
			for (LoadGroupCondition condition : Conditions)
			{
				child.addContent(condition.getLoadGroupConditionElement(ns));
			}
			e.addContent(child);
		}
		else if (isConditionsTag)
		{
			child = new Element("Conditions",ns);
			e.addContent(child);
		}
				
		return e;
	}
	
	public String getName()
	{
		return Name;
	}
	
	public void setName(String Name)
	{
		this.Name = Name;
	}
	
	public LoadGroupCondition [] getConditions()
	{
		return Conditions;
	}
	
	public void setConditions(LoadGroupCondition [] Conditions)
	{
		this.Conditions = Conditions;
	}
}
