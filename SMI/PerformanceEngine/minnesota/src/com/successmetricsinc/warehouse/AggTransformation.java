/**
 * $Id: AggTransformation.java,v 1.6 2010-12-20 00:36:15 bpeters Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse;

import java.io.Serializable;

import org.jdom.Element;
import org.jdom.Namespace;
import com.successmetricsinc.query.Aggregate;
import com.successmetricsinc.util.Util;

/**
 * @author Brad Peters
 * 
 */
public class AggTransformation implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String Dimension;
	public String ColumnName;
	public Transformation[] Transformations;

	public AggTransformation(Aggregate agg, Element e, Namespace ns)
	{
		Dimension = e.getChildTextTrim("Dimension", ns);
		ColumnName = e.getChildTextTrim("ColumnName", ns);
		Element child = e.getChild("Transformations", ns);
		if (child != null)
			Transformations = new Transformation[]
			{ com.successmetricsinc.warehouse.Transformation.createStagingTransformation(Util.replaceWithPhysicalString(agg
					.getName()), (StringReplacement) agg, (Element) child.getChildren().get(0), ns) };
	}
	
	public Element getAggTransformationElement(Namespace ns)
	{
		Element e = new Element("AggTransformation",ns);	
		
		Element child = new Element("Dimension",ns);
		child.setText(Dimension);
		e.addContent(child);
		
		child = new Element("ColumnName",ns);
		child.setText(ColumnName);
		e.addContent(child);
						
		if (Transformations!=null && Transformations.length>0)
		{
			child = new Element("Transformations",ns);
			for (Transformation t : Transformations)
			{
				child.addContent(t.getTransformationElement(ns));
			}
			e.addContent(child);
		}
		
		return e;
	}
}
