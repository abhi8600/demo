package com.successmetricsinc.warehouse;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.Query;

/**
 * Delete All previously loaded data
 * 
 * @author Rohan
 * 
 */
public class DeleteSpace
{
	private static Logger logger = Logger.getLogger(DeleteSpace.class);
	DatabaseConnection dconn;
	Repository r;
	public static final String TXN_COMMAND_HISTORY_TABLE_NAME = "TXN_COMMAND_HISTORY";
	
	public DeleteSpace(Repository r)
	{
		this.r = r;
	}

	public void delete() throws SQLException
	{
		dconn = r.getDefaultConnection();
		Statement stmt = null;
		try
		{
			String schema = dconn.Schema;
			if (schema == null || schema.trim().isEmpty())
			{
				logger.info("Can not initiate delete space for application: " + r.getApplicationName());
				return;
			}
			Connection conn = dconn.ConnectionPool.getConnection();
			stmt = conn.createStatement();
			//drop schema tables
			DeleteAllData delAllData = new DeleteAllData(r);
			delAllData.delete(); 
			//drop schema
			dropSchema(stmt, schema);
		}
		catch (SQLException sqle)
		{
			logger.error(sqle.getMessage(), sqle);
			throw new SQLException("SQLException during DeleteSpace delete method ", sqle);
		}
		catch (Exception ex)
		{
			logger.error(ex.getMessage(), ex);
		}
		finally
		{
			try
			{
				if (stmt != null)
					stmt.close();
			}
			catch (SQLException e)
			{
				throw new SQLException(e);
			}
		}		
	}
	
	public void dropSchema(Statement stmt, String schema) throws SQLException
    {
		try
		{
			boolean isIB = DatabaseConnection.isDBTypeInfoBright(dconn.DBType);
	        String dropSchemaSQL = null;
	        if (isIB)
	        {
	            dropSchemaSQL = "DROP DATABASE IF EXISTS " + dconn.Schema;
	        }
	        else
	        {
	            dropSchemaSQL = "DROP SCHEMA [" + dconn.Schema + "]";
	        }
	        logger.debug(Query.getPrettyQuery(dropSchemaSQL));
	        stmt.executeUpdate(dropSchemaSQL);
	        logger.debug("Database schema dropped " + dconn.Schema);
	        
		} 
		catch (SQLException sqex)
		{
			logger.error(sqex.getMessage(), sqex);
			throw new SQLException("SQLException while dropping schema " + schema,  sqex);
		}
    }
}
