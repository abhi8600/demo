/**
 * $Id: SourceColumn.java,v 1.18 2012-11-12 18:25:19 BIRST\ricks Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse;

import java.io.Serializable;

import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.RepositoryException;

/**
 * @author Brad Peters
 * 
 */
public class SourceColumn implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public enum Type
	{
		Varchar, Integer, Number, DateTime, Float, Date, None, BadDataType
	};
	public String Name;
	public Type DataType;
	public String DataTypeString;
	public int Width;
	public String Format;
	public double Multiplier;
	private boolean encrypted = false;
	public int NumDistinctValues;
	public boolean PreventUpdate;
	public boolean formatTag;

	public SourceColumn(Element e, Namespace ns) throws RepositoryException
	{
		Name = Util.intern(e.getChildTextTrim("Name", ns));
		DataTypeString = Util.intern(e.getChildTextTrim("DataType", ns));
		if (DataTypeString == null || DataTypeString.length() == 0)
			DataType = Type.BadDataType;
		else if (DataTypeString.equals("Varchar"))
			DataType = Type.Varchar;
		else if (DataTypeString.equals("Integer"))
			DataType = Type.Integer;
		else if (DataTypeString.equals("Number"))
			DataType = Type.Number;
		else if (DataTypeString.equals("Date"))
			DataType = Type.Date;
		else if (DataTypeString.equals("DateTime"))
			DataType = Type.DateTime;
		else if (DataTypeString.equals("Float"))
			DataType = Type.Float;
		else if (DataTypeString.equals("None"))
			DataType = Type.None;
		else
			DataType = Type.BadDataType;
		String s = e.getChildTextTrim("Width", ns);
		if (s != null)
			Width = Integer.parseInt(e.getChildTextTrim("Width", ns));
		Format = e.getChildText("Format", ns);
		formatTag = e.getChildText("Format", ns)!=null?true:false;
		if (Format != null && Format.trim().isEmpty()) // handle case of <Format />
			Format = null;
		Format = Util.intern(Format);
		s = e.getChildText("Multiplier", ns);
		if (s != null)
			Multiplier = Double.valueOf(s);
		s = e.getChildText("Encrypted", ns);
		if (s != null)
			encrypted = Boolean.parseBoolean(s);
		s = e.getChildText("NumDistinctValues", ns);
		if (s != null)
			NumDistinctValues = Integer.parseInt(s);
		s = e.getChildText("PreventUpdate", ns);
		if (s != null)
			PreventUpdate = Boolean.parseBoolean(s);
	}
	
	public Element getSourceColumnElement(Namespace ns)
	{
		Element e = new Element("SourceColumn",ns);
		
		Element child = new Element("Name",ns);
		child.setText(Name);
		e.addContent(child);
		
		child = new Element("DataType",ns);
		child.setText(DataTypeString);
		e.addContent(child);
		
		child = new Element("Width",ns);
		child.setText(String.valueOf(Width));
		e.addContent(child);
		
		if (Format!=null)
		{
			child = new Element("Format",ns);
			child.setText(String.valueOf(Format));
			e.addContent(child);
		}
		else if (formatTag)
		{
			child = new Element("Format",ns);
			e.addContent(child);
		}
		
		if (Multiplier!=0)
		{
			child = new Element("Multiplier",ns);
			child.setText(Util.getDecimalFormatForRepository().format(Multiplier));
			e.addContent(child);
		}
		
		if (NumDistinctValues!=0)
		{
			child = new Element("NumDistinctValues",ns);
			child.setText(String.valueOf(NumDistinctValues));
			e.addContent(child);
		}
		
		if (encrypted)
		{
			child = new Element("Encrypted",ns);
			child.setText(String.valueOf(encrypted));
			e.addContent(child);
		}
		
		if (PreventUpdate)
		{
			child = new Element("PreventUpdate",ns);
			child.setText(String.valueOf(PreventUpdate));
			e.addContent(child);
		}
		
		return e;
	}

	public boolean isEncrypted() {
		return encrypted;
	}

	public void setEncrypted(boolean encrypted) {
		this.encrypted = encrypted;
	}
	
	public boolean isTrim()
	{
		return (DataType == SourceColumn.Type.Varchar)  && "trim".equalsIgnoreCase(Format);
	}
	
	public boolean isEBCDIC()
	{
		return (DataType == SourceColumn.Type.Integer) && "EBCDIC".equalsIgnoreCase(Format);
	}
	
	public boolean isUnixTime()
	{
		return (DataType == SourceColumn.Type.Integer) && "Unix".equalsIgnoreCase(Format);
	}
	
	public boolean isExcelDate()
	{
		return (DataType == SourceColumn.Type.Integer) && "Excel".equalsIgnoreCase(Format);
	}
	
	public boolean isExcelDatePlusOne()
	{
		return (DataType == SourceColumn.Type.Integer) && "Excel+1".equalsIgnoreCase(Format);
	}
	
	public boolean isSpecialFormat()
	{
		return isTrim() || isEBCDIC() || isExcelDate() || isUnixTime();
	}
	
	public String toString()
	{
		return (Name);
	}
}
