/**
 * $Id: LoadGroupCondition.java,v 1.4 2010-12-20 00:36:15 bpeters Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse;

import java.io.Serializable;

import org.jdom.Element;
import org.jdom.Namespace;

public class LoadGroupCondition implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String Dimension;
	private String Column;
	private String Operator;
	private String Value;
	private String strOperator;

	/**
	 * Create a load group condition structure based on repository XML
	 * 
	 * @param ae
	 */
	public LoadGroupCondition(Element ae, Namespace ns)
	{
		Dimension = ae.getChildTextTrim("DimensionName", ns);
		Column = ae.getChildTextTrim("DimensionColumn", ns);
		Value = ae.getChildTextTrim("Value", ns);
		String temp = ae.getChildTextTrim("Operator", ns);
		strOperator = ae.getChildTextTrim("Operator", ns);
		Operator = temp == null ? "=" : temp; 
	}
	
	public Element getLoadGroupConditionElement(Namespace ns)
	{
		Element e = new Element("LoadGroupCondition",ns);
		
		Element child = new Element("DimensionName",ns);
		child.setText(Dimension);
		e.addContent(child);
		
		child = new Element("DimensionColumn",ns);
		child.setText(Column);
		e.addContent(child);
		
		child = new Element("Operator",ns);
		child.setText(strOperator);
		e.addContent(child);
		
		child = new Element("Value",ns);
		child.setText(Value);
		e.addContent(child);
				
		return e;
	}

	public String getDimension()
	{
		return Dimension;
	}
	
	public void setDimension(String Dimension)
	{
		this.Dimension = Dimension;
	}
	
	public String getColumn()
	{
		return Column;
	}
	
	public void setColumn(String Column)
	{
		this.Column = Column;
	}
	
	public String getOperator() {
		return Operator;
	}

	public void setOperator(String Operator) {
		this.Operator = Operator;
	}

	public String getValue()
	{
		return Value;
	}
	
	public void setValue(String Value)
	{
		this.Value = Value;
	}
}
