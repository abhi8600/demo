/**
 * $Id: Expression.java,v 1.3 2010-12-20 12:48:03 mpandit Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse;

import java.io.Serializable;

import org.jdom.Element;
import org.jdom.Namespace;

/**
 * @author Brad Peters
 * 
 */
public class Expression extends Transformation implements Serializable
{
	private static final long serialVersionUID = 1L;
	public String Formula;

	public Expression(Element e, Namespace ns)
	{
		Formula = e.getChildTextTrim("Formula", ns);
	}
	
	public Element getExpressionElement(Namespace ns)
	{
		Element e = new Element("Expression",ns);
		
		Element child = new Element("ExecuteAfter",ns);
		child.setText(String.valueOf(iExecuteAfter));
		e.addContent(child);
		
		child = new Element("Formula",ns);
		child.setText(Formula);
		e.addContent(child);
		
		return e;
	}

	public String getPhysicalFormula()
	{
		return (getReplacedString(Formula));
	}
}
