package com.successmetricsinc.warehouse;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimeZone;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.zip.CRC32;

import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.query.Database.DatabaseType;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.DimensionTable;
import com.successmetricsinc.query.Level;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.warehouse.SourceColumn.Type;

public class LineProcessor
{
	private static Logger logger = Logger.getLogger(LineProcessor.class);
	private static String DATA_ERROR_MARKER = "DATAERROR";
	private static String DATA_WARNING_MARKER = "DATAWARNING";
	private static final int MAX_ERRORS_AND_WARNINGS = 500;
	private static TimeZone GMT_TZ = TimeZone.getTimeZone("GMT");
	private static final DecimalFormat OUTPUT_DECIMAL_FORMAT = new DecimalFormat("###################.######");
	private static final int MAX_CACHE_MAP_SIZE = 50000;
	private Repository r;
	private DatabaseConnection dbc;
	private AtomicLong warningCount;
	private AtomicLong errorCount;
	private StagingTableBulkLoadInfo stbl;
	private SimpleDateFormat[] setDateFormats;
	private SourceFile sf;
	private String[] unknowns;
	private int[][] cksumlist;
	private int colNameHashCode;
	private Map<String, List<Integer>> sampleIndexes;
	private Map<String, Set<String>> sampleMap;
	NumberFormat df = DecimalFormat.getInstance();
	TimeZone tz;
	// Calendar calendar;
	private boolean replaceWithNullOnFailureToParseValue;
	private boolean skipRecordOnFailureToParseValue;
	private AtomicLong numFailedRecords;
	private int numThresholdFailedRecords;
	public AtomicBoolean skipRunning;

	private Map<Double, String> doubleMap = Collections.synchronizedMap(new WeakHashMap<Double, String>());
	private Map<Calendar, String> formattedDateMap = Collections.synchronizedMap(new WeakHashMap<Calendar, String>());
	private Map<Calendar, String> formattedDateTimeMap = Collections.synchronizedMap(new WeakHashMap<Calendar, String>());
	private Map<String, Calendar> dateCache = Collections.synchronizedMap(new WeakHashMap<String, Calendar>(1000));
	
	public LineProcessor(StagingTableBulkLoadInfo stbl, AtomicLong warningCount, AtomicLong errorCount, Repository r, SourceFile sf, StagingTable st,
			Map<DimensionTable, List<StagingColumn>> scmap, File pathFile, Map<String, Set<String>> sampleMap, 
			boolean replaceWithNullOnFailureToParseValue, boolean skipRecordOnFailureToParseValue, int numThresholdFailedRecords,
			AtomicLong numFailedRecords, AtomicBoolean skipRunning, DatabaseConnection dbc)
	{
		this.warningCount = warningCount;
		this.errorCount = errorCount;
		this.stbl = stbl;
		this.r = r;
		this.setDateFormats = new SimpleDateFormat[sf.Columns.length + stbl.dateIDColumns.size()];
		this.sf = sf;
		this.replaceWithNullOnFailureToParseValue = replaceWithNullOnFailureToParseValue;
		this.skipRecordOnFailureToParseValue = skipRecordOnFailureToParseValue;
		this.numThresholdFailedRecords = numThresholdFailedRecords;
		this.numFailedRecords = numFailedRecords;
		this.skipRunning = skipRunning;
		this.dbc = dbc;
		tz = sf.getInputTimeZone();
		unknowns = new String[sf.Columns.length];
		// Setup unknown value replacement strings
		if (st != null)
		{
			for (int i = 0; i < sf.Columns.length; i++)
			{
				for(StagingColumn sc: st.Columns)
				{
					if (sc.SourceFileColumn != null && sc.SourceFileColumn.equals(sf.Columns[i].Name)
							&& sc.UnknownValue != null && sc.UnknownValue.length() > 0)
					{
						unknowns[i] = sc.UnknownValue;
					}
				}
			}
		}
		// Create checksums
		StringBuilder colNamesSB = new StringBuilder();
		if (r.getCurrentBuilderVersion() >= 10)
		{
			cksumlist = new int[scmap.entrySet().size()][];
			int cki = 0;
			for (Entry<DimensionTable, List<StagingColumn>> en : scmap.entrySet())
			{
				cksumlist[cki] = new int[en.getValue().size()];
				int cki2 = 0;
				for (StagingColumn sc : en.getValue())
				{
					cksumlist[cki][cki2] = -1;
					if (sc.SourceFileColumn == null || sc.SourceFileColumn.length() == 0)
						continue;
					int index = -1;
					for (int i = 0; i < sf.Columns.length; i++)
					{
						if (sc.SourceFileColumn.equals(sf.Columns[i].Name))
						{
							index = i;
							colNamesSB.append(sc.Name);
							break;
						}
					}
					if (index >= 0)
						cksumlist[cki][cki2] = index;
					cki2++;
				}
				cki++;
			}
		} else
			cksumlist = new int[0][];	
		this.colNameHashCode = colNamesSB.toString().hashCode();
		if (pathFile != null && sampleMap != null)
		{
			SampleSettings ss = Sample.getSettings(pathFile.getAbsolutePath());
			this.sampleMap = sampleMap;
			if (ss != null)
			{
				for (SampleMap sm: ss.map)
				{
					if (!sm.StagingTableName.equals(st.Name))
						continue;
					Level l = r.findDimensionLevel(sm.Dimension, sm.Level);
					if (l != null)
					{
						List<Integer> indexes = Sample.getIndexes(l.getNaturalKey(), st, sf);
						if (indexes != null)
						{
							synchronized(sampleMap)
							{
								if (!sampleMap.containsKey(sm.Dimension+"."+sm.Level))
								{
									Sample s = new Sample(r, pathFile.getAbsolutePath(), sm.Dimension, sm.Level);
									Set<String> sampleSet = s.getSampleSet();
									if (sampleSet != null && sampleSet.size() > 0)
									{
										sampleMap.put(sm.Dimension + "." + sm.Level, sampleSet);
									}
								}
							}
							if (sampleIndexes == null)
								sampleIndexes = new HashMap<String, List<Integer>>();
							sampleIndexes.put(sm.Dimension+"."+sm.Level, indexes);
						}
					}
				}
			}
		}
	}

	/**
	 * Process a line (array of strings) vs. the expected columns in a source file and sees if each column can be parsed
	 * and put into a known neutral format that will bulk load
	 * 
	 * @param sf
	 * @param line
	 * @param lineno
	 * @return
	 */
	public LineProcessorResult processLine(Line l) throws Exception
	{
		LineProcessorResult lpr = null;
		if (l != null)
		{
			String[] pline = Util.splitLine(l.line, sf, MAX_ERRORS_AND_WARNINGS, errorCount);
			if (pline == null)
				return null;
			/*
			 * Process any escapes necessary
			 */
			if (sf.Escapes != null)
			{
				for (int i = 0; i < pline.length; i++)
				{
					for (int j = 0; j < sf.Escapes.length; j++)
					{
						String[] esc = sf.Escapes[j];
						if (sf.EscapeTypes[j] == SourceFile.ESCAPE_SUBSTRING)
						{
							//replacement of pipe character i.e. esc[0] = pipe(|) and pline[i] contains  backslash pipe (\|)
							if (esc[0].equals("|"))
							{
								pline[i] = Util.replaceStr(pline[i], "\\|", esc[1]);
							}
							//replacement of new line char
							else if (esc[0].equals("\\r\\n"))
							{
								pline[i] = Util.replaceStr(pline[i], "\r\n", esc[1]);
							}
							pline[i] = Util.replaceStr(pline[i], esc[0], esc[1]);
						}
						else if (sf.EscapeTypes[j] == SourceFile.ESCAPE_ALL)
						{
							if (pline[i].equals(esc[0]))
								pline[i] = esc[1];
						} else if (sf.EscapeTypes[j] == SourceFile.ESCAPE_START)
						{
							if (pline[i].startsWith(esc[0]))
								pline[i] = esc[1] + pline[i].substring(esc[0].length());
						} else if (sf.EscapeTypes[j] == SourceFile.ESCAPE_END)
						{
							if (pline[i].endsWith(esc[0]))
								pline[i] = pline[i].substring(0, pline[i].length() - esc[0].length()) + esc[1];
						}
					}
				}
			}
			lpr = processLine(pline, l.number);
			lpr.pline = pline;
			if (sampleIndexes != null)
			{
				// Apply sampling if appropriate
				for (String lname: sampleIndexes.keySet())
				{
					List<Integer> indexes = sampleIndexes.get(lname);
					Set<String> samples = sampleMap.get(lname);
					StringBuilder sb = new StringBuilder();
					for(Integer index: indexes)
					{
						if (sb.length() > 0)
							sb.append('|');
						try
						{
							sb.append(pline[index]);
						} catch (Exception ex)
						{
						}
					}
					String sampleKey = sb.toString();
					// If the sample isn't present - ignore line
					if (!samples.contains(sampleKey))
					{
						lpr.filtered = true;
						lpr.result = null;
					}
				}
			}
		}
		return lpr;
	}

	/**
	 * Process a line (array of strings) vs. the expected columns in a source file and sees if each column can be parsed
	 * and put into a known neutral format that will bulk load
	 * - XXX this could be made alot more efficient...
	 * 
	 * @param sf
	 * @param line
	 * @param lineno
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	private LineProcessorResult processLine(String[] line, long lineno) throws UnsupportedEncodingException
	{
		StringBuilder sb = new StringBuilder();
		boolean error = false;
		Integer[] dateIDs = new Integer[stbl.dateIDColumns.size()];
		LineProcessorResult lpr = new LineProcessorResult();
		if (tz == null)
			tz = TimeZone.getDefault();
		for (int i = 0; i < sf.Columns.length; i++)
		{
			SourceColumn sc = sf.Columns[i];
			String s = line[i];
			if (!Util.hasNonWhiteSpaceCharacters(s))
			{
				// if there is an equals filter on this column and column is empty, it must be filtered
				if (i == stbl.filterColumn && stbl.filterOperator != null && stbl.filterOperator.equals("=") && stbl.filterValue != null)
				{
					lpr.filtered = true;
					return (lpr);
				}
				if (unknowns[i] != null)
					sb.append(unknowns[i]);
				else
				{
					sb.append(stbl.bulkLoadNull);
				}
				if (i < sf.Columns.length - 1)
					sb.append('|');
				continue;
			}
			// expand tabs
			if (s.indexOf("\\t") >= 0)
			{
				s = s.replace("\\t", "\t");
			}
			// Replace separator character if necessary
			if (s.indexOf('|') >= 0)
				s = s.replace('|', '/');
			switch (sc.DataType)
			{
			case Varchar:
				if (sc.isTrim())
					s = s.trim();
				if (i == stbl.filterColumn && stbl.filterOperator != null && stbl.filterValue != null)
				{
					if(stbl.filterOperator.equals("=") && (s.compareTo((String)stbl.filterValue) != 0) || 
						stbl.filterOperator.equals("<") && (s.compareTo((String)stbl.filterValue) >= 0) ||
						stbl.filterOperator.equals(">") && (s.compareTo((String)stbl.filterValue) <= 0) ||
						stbl.filterOperator.equals("<>") && (s.compareTo((String)stbl.filterValue) == 0))
					{
						lpr.filtered = true;
						return (lpr);
					}
				}
				// make sure that the string is less than the UTF-8 limit for writeUTF - XXX shoudn't this be for the entire line???
				s = utfTruncator(s);
				if ((sc.Width < stbl.maxVarCharWidth || !stbl.hasvarcharmax) && (s.length() > Math.min(sc.Width, stbl.maxVarCharWidth)))
				{
					if (warningCount.get() < MAX_ERRORS_AND_WARNINGS)
						logger.warn("[" + DATA_WARNING_MARKER + ":" + sf.FileName + "] Line processing problem: " + lineno + ", column: " + sc.Name + " - ["
								+ s + "] - does not fit Varchar(" + Math.min(sc.Width, stbl.maxVarCharWidth) + ") - truncating");
					warningCount.incrementAndGet();
					s = s.substring(0, Math.min(sc.Width, stbl.maxVarCharWidth));
				}
				// XXX hack
				if (s.length() > 4000)
					s = s.substring(0, 4000); // XXX hack
				if (i == sf.Columns.length - 1 && s.contains("\n"))
				{
					// Replace any terminator strings - will cause load to fail
					s = Util.replaceStr(s, "\r\n", "\r");
				}
				if (dbc.DBType == DatabaseType.Redshift || dbc.DBType == DatabaseType.Hana) // redshift doesn't like embedded \r in files, even when quoted
					s = Util.replaceStr(s, "\r", "");
				/* no longer needed
				 * https://forums.aws.amazon.com/ann.jspa?annID=2036
				 *
				if (DatabaseConnection.isDBTypeParAccel(dbc.DBType))
				{
					// redshift and paraccel can't handle 4 byte UTF-8
					// - http://docs.aws.amazon.com/redshift/latest/dg/t_preparing-input-data.html
					// - http://docs.aws.amazon.com/redshift/latest/dg/t_loading_unicode_data.html
					s = strip4ByteUTF8(s);
				}
				*/
				/* MonetDB seems like must remove backslashes and quotes */
				if (stbl.quoteAllStrings)
					s = '"' + s.replace("\\", "").replace("\"", "").replace("\u001a", "") + '"';
				/* In Infobright must turn double quotes into single quotes as "ESCAPED BY" doesn't seem to work */
				else if (stbl.quoteStrings)
					s = '"' + s.replace("\"", "'") + '"';
				if (stbl.isescapedandnotquoted)
					s = s.replace("\\", "\\\\");
				if (stbl.escapeNewLine)
					s = s.replace("\n", "\\\n");
				sb.append(s);
				break;
			case Integer:
				s = s.trim();
				try
				{
					if (s.length() > 0)
					{
						/*
						 * check for integer special formats (EpochExcel and EpochUnix)
						 */
						if (sc.isExcelDate())
						{
							// January 1, 2008 is 39448
							int delta = Integer.parseInt(s) - 39448;
							Calendar cal = GregorianCalendar.getInstance();
							cal.set(2008, 0, 1, 0, 0, 0); // January 1, 2008
							cal.add(Calendar.DATE, delta);
							synchronized (stbl.outputDateTimeFormat)
							{
								sb.append(stbl.outputDateTimeFormat.format(new Date(cal.getTimeInMillis())));
							}
						} else if (sc.isExcelDatePlusOne())
						{
							/*
							 * January 1, 2008 is 39447 (because PNK has slightly different dates than Excel... epoch
							 * issue)
							 */
							int delta = Integer.parseInt(s) - 39447;
							Calendar cal = GregorianCalendar.getInstance();
							cal.set(2008, 0, 1, 0, 0, 0); // January 1, 2008
							cal.add(Calendar.DATE, delta);
							synchronized (stbl.outputDateTimeFormat)
							{
								sb.append(stbl.outputDateTimeFormat.format(new Date(cal.getTimeInMillis())));
							}
						} else if (sc.isUnixTime())
						{
							// time since January 1, 1970, 00:00:00 GMT
							long delta = Long.parseLong(s);
							Date dt = new Date(delta);
							synchronized (stbl.outputDateTimeFormat)
							{
								sb.append(stbl.outputDateTimeFormat.format(dt));
							}
						} else
						{
							// Leading plus signs
							if (s.charAt(0) == '+' && s.length() > 1)
								s = s.substring(1);
							Long si = Long.parseLong(s);
							if (i == stbl.filterColumn && stbl.filterOperator != null && stbl.filterValue != null)
							{
								if(stbl.filterOperator.equals("=") && si != ((Integer)stbl.filterValue).intValue() ||
									stbl.filterOperator.equals("<") && si >= ((Integer)stbl.filterValue).intValue() ||
									stbl.filterOperator.equals(">") && si <= ((Integer)stbl.filterValue).intValue() ||
									stbl.filterOperator.equals("<>") && si == ((Integer)stbl.filterValue).intValue())
								{
									lpr.filtered = true;
									return (lpr);
								}
							}
							sb.append(si);
						}
					}
				} catch (NumberFormatException ex)
				{
					try
					{
						if (s.length() > 0)
						{
							double d;
							DecimalFormat fmt = (DecimalFormat) stbl.formats[i];
							if (fmt != null)
							{
								synchronized (fmt)
								{
									d = fmt.parse(s).doubleValue();
								}
							}
							else if (sc.isEBCDIC())
								d = getEBCDIC(s);
							else
							{
								synchronized (df)
								{
									d = df.parse(s).doubleValue();
								}
							}
							if (sc.Multiplier != 0)
								d *= sc.Multiplier;
							if (d > (double) Long.MAX_VALUE  || d < (double) Long.MIN_VALUE)
								throw new NumberFormatException("Value exceeds range");
							sb.append((long) d);
						}
					} catch (NumberFormatException ex2)
					{
						if (replaceWithNullOnFailureToParseValue)
						{
							if (warningCount.get() < MAX_ERRORS_AND_WARNINGS)
								logger.warn("[" + DATA_WARNING_MARKER + ":" + sf.FileName + "] Line processing problem: " + lineno + ", column: " + sc.Name + " - ["
										+ s + "] - Number Format Exception (Integer) - Value will be replaced with NULL");
							sb.append(stbl.bulkLoadNull);
							warningCount.incrementAndGet();
						}
						else if (skipRecordOnFailureToParseValue)
						{
							if (errorCount.get() < MAX_ERRORS_AND_WARNINGS)
								logger.error("[" + DATA_ERROR_MARKER + ":" + sf.FileName + "] Line processing problem: " + lineno + ", column: " + sc.Name + " - ["
										+ s + "] - Number Format Exception (Integer) - Record will be skipped");
							error = true;
							errorCount.incrementAndGet();
						}												
					} catch (ParseException e)
					{
						if (replaceWithNullOnFailureToParseValue)
						{
							if (warningCount.get() < MAX_ERRORS_AND_WARNINGS)
								logger.warn("[" + DATA_WARNING_MARKER + ":" + sf.FileName + "] Line processing problem: " + lineno + ", column: " + sc.Name + " - ["
										+ s + "] - Parse Exception (Integer) - Value will be replaced with NULL");
							sb.append(stbl.bulkLoadNull);
							warningCount.incrementAndGet();
						}
						else if (skipRecordOnFailureToParseValue)
						{
							if (errorCount.get() < MAX_ERRORS_AND_WARNINGS)
								logger.error("[" + DATA_ERROR_MARKER + ":" + sf.FileName + "] Line processing problem: " + lineno + ", column: " + sc.Name + " - ["
										+ s + "] - Number Format Exception (Integer) - Record will be skipped");
							error = true;
							errorCount.incrementAndGet();
						}						
					}
				}
				break;
			case Number:
			case Float:
				s = s.trim();
				try
				{
					if (s.length() > 0)
					{
						double d;
						DecimalFormat fmt = (DecimalFormat) stbl.formats[i];
						if (fmt != null)
						{
							synchronized (fmt)
							{
								d = fmt.parse(s).doubleValue();
							}
						}
						else if (sc.isEBCDIC())
							d = getEBCDIC(s);
						else
						{
							synchronized (df)
							{
								d = df.parse(s).doubleValue();
							}
						}
						if (i == stbl.filterColumn && stbl.filterOperator != null && stbl.filterValue != null)
						{
							if(stbl.filterOperator.equals("=") && d != ((Double)stbl.filterValue).doubleValue() ||
								stbl.filterOperator.equals("<") && d >= ((Double)stbl.filterValue).doubleValue() ||
								stbl.filterOperator.equals(">") && d <= ((Double)stbl.filterValue).doubleValue() ||
								stbl.filterOperator.equals("<>") && d == ((Double)stbl.filterValue).doubleValue())
							{
								lpr.filtered = true;
								return (lpr);
							}
						}
						if (sc.Multiplier != 0)
							d *= sc.Multiplier;
						String ds = null;
						// use output format for numbers, bulk import cannot handle scientific notation
						if (sc.DataType == Type.Number)
							ds = OUTPUT_DECIMAL_FORMAT.format(d);
						else 
						{
							ds = doubleMap.get(d);
							if (ds == null)
							{
								ds = Double.toString(d);
								doubleMap.put(d, ds);
							}
						}
						if(ds != null && (ds.equalsIgnoreCase("-Infinity") || ds.equalsIgnoreCase("Infinity")))
						{
							if (replaceWithNullOnFailureToParseValue)
							{
								if (warningCount.get() < MAX_ERRORS_AND_WARNINGS)
									logger.warn("[" + DATA_WARNING_MARKER + ":" + sf.FileName + "] Line processing problem: " + lineno + ", column: " + sc.Name + " - ["
											+ s + "] - Invalid Number - Value will be replaced with NULL");
								sb.append(stbl.bulkLoadNull);
								warningCount.incrementAndGet();
							}
							else if (skipRecordOnFailureToParseValue)
							{
								if (errorCount.get() < MAX_ERRORS_AND_WARNINGS)
									logger.error("[" + DATA_ERROR_MARKER + ":" + sf.FileName + "] Line processing problem: " + lineno + ", column: " + sc.Name + " - ["
											+ s + "] - Invalid Number - Record will be skipped");
								error = true;
								errorCount.incrementAndGet();
							}						

						}
						else
						{
							sb.append(ds);
						}
					}
				} catch (NumberFormatException ex)
				{
					if (!s.equalsIgnoreCase("nan")) // Not a number
					{
						if (replaceWithNullOnFailureToParseValue)
						{
							if (warningCount.get() < MAX_ERRORS_AND_WARNINGS)
								logger.warn("[" + DATA_WARNING_MARKER + ":" + sf.FileName + "] Line processing problem: " + lineno + ", column: " + sc.Name + " - ["
										+ s + "] - Number Format Exception (Number/Float) - Value will be replaced with NULL");
							sb.append(stbl.bulkLoadNull);
							warningCount.incrementAndGet();
						}
						else if (skipRecordOnFailureToParseValue)
						{
							if (errorCount.get() < MAX_ERRORS_AND_WARNINGS)
								logger.error("[" + DATA_ERROR_MARKER + ":" + sf.FileName + "] Line processing problem: " + lineno + ", column: " + sc.Name + " - ["
										+ s + "] - Number Format Exception (Number/Float) - Record will be skipped");
							error = true;
							errorCount.incrementAndGet();
						}						
					}
				} catch (ParseException e)
				{
					if (!s.equalsIgnoreCase("nan")) // Not a number
					{
						if (replaceWithNullOnFailureToParseValue)
						{
							if (warningCount.get() < MAX_ERRORS_AND_WARNINGS)
								logger.warn("[" + DATA_WARNING_MARKER + ":" + sf.FileName + "] Line processing problem: " + lineno + ", column: " + sc.Name + " - ["
										+ s + "] - Parse Exception (Number/Float) - Value will be replaced with NULL");
							sb.append(stbl.bulkLoadNull);
							warningCount.incrementAndGet();
						}
						else if (skipRecordOnFailureToParseValue)
						{
							if (errorCount.get() < MAX_ERRORS_AND_WARNINGS)
								logger.error("[" + DATA_ERROR_MARKER + ":" + sf.FileName + "] Line processing problem: " + lineno + ", column: " + sc.Name + " - ["
										+ s + "] - Number Format Exception (Number/Float) - Record will be skipped");
							error = true;
							errorCount.incrementAndGet();
						}						
					}
				}
				break;
			case DateTime:
			case Date:
				s = s.trim();
				if (s.length() > 0)
				{
					try
					{
						Calendar cal = (tz == null ? Calendar.getInstance() : Calendar.getInstance(tz));
						{
							if (stbl.formats[i] != null)
							{
								synchronized (stbl.formats[i])
								{
									((SimpleDateFormat) stbl.formats[i]).setTimeZone(tz);
									cal.setTimeInMillis(((SimpleDateFormat) stbl.formats[i]).parse(s).getTime());
									setDateFormats[i] = (SimpleDateFormat) stbl.formats[i];
								}
							} else
							{
								// detect the format and parse using it
								cal = parseDate(sf, s, i, lineno, sc.Name);
							}
							if (cal != null)
							{
								if (i == stbl.filterColumn && stbl.filterOperator != null && stbl.filterValue != null)
								{
									if(stbl.filterOperator.equals("=") && !cal.equals((Calendar)stbl.filterValue) ||
										stbl.filterOperator.equals("<") && cal.after((Calendar)stbl.filterValue) ||
										stbl.filterOperator.equals(">") && cal.before((Calendar)stbl.filterValue) ||
										stbl.filterOperator.equals("<>") && cal.equals((Calendar)stbl.filterValue))
									{
										lpr.filtered = true;
										return (lpr);
									}
								}
								if (cal.before(dbc.getMinDatetime()) || cal.after(dbc.getMaxDatetime()))
								{
									if (replaceWithNullOnFailureToParseValue)
									{
										if (warningCount.get() < MAX_ERRORS_AND_WARNINGS)
											logger.warn("[" + DATA_WARNING_MARKER + ":" + sf.FileName + "] Line processing problem: " + lineno + ", column: " + sc.Name
													+ " - [" + s + " / Year=" + cal.get(Calendar.YEAR)
													+ "] (Couldn't process Date/DateTime (year outside of accepted range) - Value will be replaced with NULL)");
										sb.append(stbl.bulkLoadNull);
										warningCount.incrementAndGet();
									}
									else if (skipRecordOnFailureToParseValue)
									{
										if (errorCount.get() < MAX_ERRORS_AND_WARNINGS)
											logger.error("[" + DATA_ERROR_MARKER + ":" + sf.FileName + "] Line processing problem: " + lineno + ", column: " + sc.Name
													+ " - [" + s + " / Year=" + cal.get(Calendar.YEAR)
													+ "] (Couldn't process Date/DateTime (year outside of accepted range) - Record will be skipped)");
										error = true;
										errorCount.incrementAndGet();
									}
									cal = null;
								} else
								{
									if (sc.DataType == Type.Date)
									{
										String ds = formattedDateMap.get(cal);
										if (ds == null)
										{
											synchronized (stbl.outputDateFormat)
											{
												stbl.outputDateFormat.setTimeZone(tz);
												ds = stbl.outputDateFormat.format(cal.getTime());
											}
											formattedDateMap.put(cal, ds);
										}
										sb.append(ds);
									} else if (sc.DataType == Type.DateTime)
									{
										String ds = formattedDateTimeMap.get(cal);
										if (ds == null)
										{
											synchronized (stbl.outputDateTimeFormat)
											{
												ds = stbl.outputDateTimeFormat.format(cal.getTime());
											}
											int len = sb.length();
											// Some random formatting error - hack for now
											if (len >= 6 && sb.substring(len - 6, len - 1).equals("/00 AM"))
											{
												if (replaceWithNullOnFailureToParseValue)
												{
													if (warningCount.get() < MAX_ERRORS_AND_WARNINGS)
														logger.warn("[" + DATA_WARNING_MARKER + ":" + sf.FileName + "] Line processing problem: " + lineno
																+ ", column: " + sc.Name + " - [" + s
																+ "] (Couldn't process Date/DateTime (special case) - Value will be replaced with NULL)");
													ds = stbl.bulkLoadNull;
													warningCount.incrementAndGet();
												}
												else if (skipRecordOnFailureToParseValue)
												{
													if (errorCount.get() < MAX_ERRORS_AND_WARNINGS)
														logger.error("[" + DATA_ERROR_MARKER + ":" + sf.FileName + "] Line processing problem: " + lineno + ", column: " + sc.Name
																+ " - [" + s
																+ "] (Couldn't process Date/DateTime (special case) - Record will be skipped)");
													error = true;
													errorCount.incrementAndGet();
												}
												cal = null;
											}
											formattedDateTimeMap.put(cal, ds);
										}
										sb.append(ds);
									}
								}
							} else
							{
								if (replaceWithNullOnFailureToParseValue)
								{
									if (warningCount.get() < MAX_ERRORS_AND_WARNINGS)
										logger.warn("[" + DATA_WARNING_MARKER + ":" + sf.FileName + "] Line processing problem: " + lineno
												+ ", column: " + sc.Name + " - [" + s
												+ "] (Couldn't process Date/DateTime - Value will be replaced with NULL)");
									sb.append(stbl.bulkLoadNull);
									warningCount.incrementAndGet();
								}
								else if (skipRecordOnFailureToParseValue)
								{
									if (errorCount.get() < MAX_ERRORS_AND_WARNINGS)
										logger.error("[" + DATA_ERROR_MARKER + ":" + sf.FileName + "] Line processing problem: " + lineno + ", column: " + sc.Name
												+ " - [" + s
												+ "] (Couldn't process Date/DateTime - Record will be skipped)");
									error = true;
									errorCount.incrementAndGet();
								}
							}
						}
						if (stbl.idColumns[i] != null)
						{
							for (DateID di : stbl.idColumns[i])
							{
								if (cal == null)
								{
									/*
									 * Mark the associated date id with a value -1 so that attempts to parse the same
									 * date are not repeated as part of date id logic later
									 */
									dateIDs[di.Index] = -1;
								} else
								{
									if (tz != null && !tz.hasSameRules(r.getServerParameters().getProcessingTimeZone()))
									{
										Calendar dateIDcal = Calendar.getInstance(r.getServerParameters().getProcessingTimeZone());
										dateIDcal.setTimeInMillis(cal.getTimeInMillis());
										dateIDs[di.Index] = di.getDateID(stbl.td, dateIDcal);
									} else
										dateIDs[di.Index] = di.getDateID(stbl.td, cal);
								}
							}
						}
					} catch (Exception ex)
					{
						logger.debug(ex, ex);
						/*
						 * Mark the associated date ids with a value -1 so that attempts to parse the same date are not
						 * repeated as part of date id logic later
						 */
						if (stbl.idColumns[i] != null)
						{
							for (DateID di : stbl.idColumns[i])
								dateIDs[di.Index] = -1;
						}
						if (replaceWithNullOnFailureToParseValue)
						{
							if (warningCount.get() < MAX_ERRORS_AND_WARNINGS)
								logger.warn("[" + DATA_WARNING_MARKER + ":" + sf.FileName + "] Error processing line: " + lineno + ", column: " + sc.Name + " - ["
										+ s + "] (Date/DateTime) - Value will be replaced with NULL");
							sb.append(stbl.bulkLoadNull);
							warningCount.incrementAndGet();
						}
						else if (skipRecordOnFailureToParseValue)
						{
							if (errorCount.get() < MAX_ERRORS_AND_WARNINGS)
								logger.error("[" + DATA_ERROR_MARKER + ":" + sf.FileName + "] Error processing line: " + lineno + ", column: " + sc.Name + " - ["
										+ s + "] (Date/DateTime) - Record will be skipped");
							error = true;
							errorCount.incrementAndGet();
						}						
					}
				} else
				{
					/*
					 * Mark the associated date ids with a value -1 so that attempts to parse the same date are not
					 * repeated as part of date id logic later
					 */
					if (stbl.idColumns[i] != null)
					{
						for (DateID di : stbl.idColumns[i])
							dateIDs[di.Index] = -1;
					}
				}
				break;
			case BadDataType:
				if (replaceWithNullOnFailureToParseValue)
				{
					if (warningCount.get() < MAX_ERRORS_AND_WARNINGS)
						logger.warn("[" + DATA_WARNING_MARKER + ":" + sf.FileName + "] Error processing line: " + lineno + ", column: " + sc.Name + " - [" + s
								+ "] (Invalid DataType found for sourcecolumn) - Value will be replaced with NULL");
					sb.append(stbl.bulkLoadNull);
					warningCount.incrementAndGet();
				}
				else if (skipRecordOnFailureToParseValue)
				{
					if (errorCount.get() < MAX_ERRORS_AND_WARNINGS)
						logger.error("[" + DATA_ERROR_MARKER + ":" + sf.FileName + "] Error processing line: " + lineno + ", column: " + sc.Name + " - [" + s
								+ "] (Invalid DataType found for sourcecolumn) - Record will be skipped");
					error = true;
					errorCount.incrementAndGet();
				}				
			}
			if (error)
				break;
			if (i < sf.Columns.length - 1)
				sb.append('|');
		}
		// skip the line if it is empty
		if (isEmptyLine(sf, sb.toString()))
		{
			lpr.filtered = true;
			return (lpr);
		}
		if (!error)
		{
			CRC32 crc = new CRC32();
			for (int index = sf.Columns.length; index < sf.Columns.length + stbl.extraColumnMap.size(); index++)
			{
				for (int idx = 0; idx < dateIDs.length; idx++)
				{
					DateID did = stbl.dateIDColumns.get(idx);
					if (did == null || stbl.extraColumnMap.get(did) == null)
						continue;
					if (stbl.extraColumnMap.get(did) != index)
						continue;
					Integer id = dateIDs[idx];
					int colIdx = sf.Columns.length + idx;
					if (colIdx > 0)
						sb.append('|');
					if (id == null && stbl.replaceColumns[idx])
					{
						String s = stbl.dateIDColumns.get(idx).SourceFileColumnName;
						s = replaceColumnIndexWithValue(s, line);
						s = s.trim();
						if (s.length() > 0)
						{
							try
							{
								Calendar cal = parseDate(sf, s, colIdx, lineno, "\"date id column: " + idx + "\"");
								if (cal != null)
								{
									if (tz != null && !tz.hasSameRules(r.getServerParameters().getProcessingTimeZone()))
									{
										Calendar dateIDcal = Calendar.getInstance(r.getServerParameters().getProcessingTimeZone());
										dateIDcal.setTimeInMillis(cal.getTimeInMillis());
										dateIDs[idx] = stbl.dateIDColumns.get(idx).getDateID(stbl.td, dateIDcal);
									} else
										dateIDs[idx] = stbl.dateIDColumns.get(idx).getDateID(stbl.td, cal);
									sb.append(dateIDs[idx]);
								} else
								{
									sb.append(stbl.bulkLoadNull);
								}
							} catch (Exception ex)
							{
								if (errorCount.get() < MAX_ERRORS_AND_WARNINGS)
									logger.error("[" + DATA_ERROR_MARKER + ":" + sf.FileName + "] Error processing line: " + lineno
											+ ", column: \"date id column: " + idx + "\"" + " - [" + s + "] (DateTime)");
								errorCount.incrementAndGet();
								error = true;
							}
						}
					} else if (id == null && stbl.dateIDColumns.get(idx) != null)
					{
						Integer result = stbl.dateIDColumns.get(idx).UnknownValue;
						sb.append(result == null ? stbl.bulkLoadNull : result.toString());
						
					} else if (id == null || id != -1)
					/*
					 * -1 indicates that earlier attempts to parse this particular date failed, append nothing
					 */
					{
						sb.append(id == null ? stbl.bulkLoadNull : id);
					}
					if (error)
						break;
				}
				for (String expr : stbl.scExpressions)
				{
					if (stbl.extraColumnMap.get(expr) != index)
						continue;
					if (sf.Columns.length > 0)
						sb.append('|');
					sb.append(replaceColumnIndexWithValue(expr, line));
				}
				for (SequenceNumber sn : stbl.sequenceNumberMap.values())
				{
					if (stbl.extraColumnMap.get(sn) != index)
						continue;
					if (sf.Columns.length > 0)
						sb.append('|');
					sb.append(sn.getAndIncrement());
				}
			}
			// Create checksum
			for(int i = 0; i < cksumlist.length; i++)
			{
				StringBuilder cksb = new StringBuilder();
				if (r.getCurrentBuilderVersion() >= 11)
				{
					cksb.append(colNameHashCode);//includes columnheaders to calculate checksum of row - bug #10550
				}
				for(int j = 0; j < cksumlist[i].length; j++)
				{
					if (cksumlist[i][j] >= 0)
					{
						String cksbItem = line[cksumlist[i][j]];
						if(cksbItem == null || cksbItem.length() == 0 && unknowns[cksumlist[i][j]] != null)
						{
							cksbItem = unknowns[cksumlist[i][j]];
						}
						if(cksbItem == null)
						{
							cksb.append(Util.BIRST_NULL);
						}
						else if(cksbItem.length() == 0)
						{
							cksb.append(Util.BIRST_EMPTY);
						}
						else
						{
							cksb.append(cksbItem);
						}
					}
				}
				if (sb.length() > 0)
					sb.append('|');
				String cksbstr = cksb.toString();
				if (r.getCurrentBuilderVersion() < 21)
				{
					sb.append(cksbstr.hashCode());
				}
				else
				{
					crc.reset();
					crc.update(cksbstr.getBytes("UTF-8"));
					sb.append((int) crc.getValue());
				}
			}
			if (stbl.compoundKeys != null)
			{
				// Add compound keys if necessary
				for (int[] iarr : stbl.compoundKeyIndices)
				{
					if (sb.length() > 0)
						sb.append('|');
					for (int i = 0; i < iarr.length; i++)
					{
						if (i > 0)
							sb.append((char)254);
						sb.append(line[iarr[i]]);
					}
				}
			}
			if (stbl.grainKeyIndices != null)
			{
				// Add grain keys if necessary
				for (int[] iarr : stbl.grainKeyIndices)
				{
					if (sb.length() > 0)
						sb.append('|');
					for (int i = 0; i < iarr.length; i++)
					{
						if (i > 0)
							sb.append((char)254);
						sb.append(line[iarr[i]]);
					}
				}
			}
		}
		if (skipRunning.get())
		{
			logger.error("Another thread already encountered Maximum no. of failed records, giving up line processing at line: " + lineno);
			lpr.error = true;
			return (lpr);
		}
		if (error)
		{
			lpr.error = true;
			long curNoFailedRecords = numFailedRecords.incrementAndGet();
			if (numThresholdFailedRecords > 0 && curNoFailedRecords >= numThresholdFailedRecords)
			{
				logger.error("Maximum no. of failed records allowed for source exceeded, aborting Line Processing at line: " + lineno);
				skipRunning.set(true);			
			}
			return (lpr);
		} else
		{
			lpr.result = sb;
			return (lpr);
		}
	}

	private Calendar parseDate(SourceFile sf, String s, int dateFormatIndex, long lineno, String scName) throws Exception
	{
		Calendar cacheResult = dateCache.get(s);
		if (cacheResult != null)
			return cacheResult;
		TimeZone tz = sf.getInputTimeZone();
		Calendar cal = tz == null ? Calendar.getInstance() : Calendar.getInstance(tz);
		boolean good = false;
		boolean setNull = false;
		try
		{
			if (setDateFormats[dateFormatIndex] == null)
				setFormats(sf, s, dateFormatIndex, scName);
			if (setDateFormats[dateFormatIndex] != null)
			{
				SimpleDateFormat sdfWithTZ = (SimpleDateFormat) setDateFormats[dateFormatIndex];
				synchronized (sdfWithTZ)
				{
					if (sdfWithTZ.toPattern().endsWith("'Z'"))
						tz = GMT_TZ;
					if (tz != null)
						sdfWithTZ.setTimeZone(tz);
					cal.setTimeInMillis(sdfWithTZ.parse(s).getTime());
					if (cal.before(dbc.getMinDatetime()) || cal.after(dbc.getMaxDatetime())
							|| (cal.getTimeInMillis() == ZERO_DATE && !sdfWithTZ.format(cal.getTime()).equals(s)))
					{
						setNull = true;
					} else
					{
						good = true;
					}
				}
			}
		} catch (ParseException ex)
		{
			logger.debug("Parse Failure: " + s + ", " + ex.getMessage());
			setFormats(sf, s, dateFormatIndex, scName);
			if (setDateFormats[dateFormatIndex] != null)
			{
				try
				{
					synchronized (setDateFormats[dateFormatIndex])
					{
						SimpleDateFormat sdfWithTZ = (SimpleDateFormat) setDateFormats[dateFormatIndex];
						if (tz != null)
							sdfWithTZ.setTimeZone(tz);
						cal.setTimeInMillis(sdfWithTZ.parse(s).getTime());

						if (cal.before(dbc.getMinDatetime()) || cal.after(dbc.getMaxDatetime())
								|| (cal.getTimeInMillis() == ZERO_DATE && !setDateFormats[dateFormatIndex].format(cal.getTime()).equals(s)))
						{
							setNull = true;
						} else
						{
							good = true;
						}
					}
				} catch (ParseException e)
				{
					logger.warn(e);
					good = false;
				}
			}
		}
		if (!good)
		{
			if (setDateFormats[dateFormatIndex] == null || setNull)
			{
				// error message handled at an upper level
				cal = null;
			} else
			{
				throw new Exception("Line processing problem: " + lineno + ", column: " + scName + " - [" + s + "] (Date/DateTime)");
			}
		}
		dateCache.put(s, cal);
		return cal;
	}

	private String replaceColumnIndexWithValue(String expression, String[] line)
	{
		String replacedExpression = expression;
		for (int i = 0; i < line.length; i++)
		{
			replacedExpression = Util.replaceStr(replacedExpression, "{" + i + "}", line[i]);
		}
		return replacedExpression;
	}

	private void setFormats(SourceFile sf, String s, int i, String name)
	{
		if (logger.isTraceEnabled())
			logger.trace("Trying to pick a date format for input string: " + s + ", column: " + name + ", position: " + i);
		for (SimpleDateFormat sdf : stbl.dateformats)
		{
			try
			{
				TimeZone tz = sf.getInputTimeZone();
				synchronized (sdf)
				{
					if (tz != null)
						sdf.setTimeZone(tz);
					sdf.parse(s);
					setDateFormats[i] = sdf;
					if (logger.isDebugEnabled())
						logger.debug("Date format picked for: " + s + ", column: " + name + ", position: " + i + " is: " + sdf.toPattern());
				}
				return;
			} catch (ParseException ex2)
			{
			}
		}
		if (logger.isDebugEnabled())
			logger.debug("Unable to pick a date format for input string:" + s + ", column: " + name + ", position: " + i);
	}
	
	/**
	 * Redshift and ParAccel can't handle 4 byte UTF-8 sequences
	 *  - http://docs.aws.amazon.com/redshift/latest/dg/t_preparing-input-data.html
	 *  - http://docs.aws.amazon.com/redshift/latest/dg/t_loading_unicode_data.html
	 * - http://stackoverflow.com/questions/14374018/removing-4-byte-utf-characters-from-a-large-file
	 * 
	 * no longer the case: https://forums.aws.amazon.com/ann.jspa?annID=2036
	 */
	@SuppressWarnings("unused")
	private String strip4ByteUTF8(String str)
	{
		StringBuilder sb = new StringBuilder();
		int len = str.length();
		for (int i = 0; i < len; i++)
		{
			char c = str.charAt(i);
			if (Character.isSurrogate(c))
				i++; // skip the current and next character
			else
				sb.append(c);
		}
		return sb.toString();
	}

	/**
	 * Java limits UTF-8 writes (writeUTF) to be limited to 64k, this routine enforces that
	 * 
	 * @return
	 */
	private String utfTruncator(String str)
	{
		/**
		 * basic algorithm taken from writeUTF
		 */
		int strlen = str.length();
		int utflen = 0;
		// worst case strings are increased by a factor of 3
		if (strlen * 3 < 65535)
			return str;
		/* use charAt instead of copying String to char array */
		for (int i = 0; i < strlen; i++)
		{
			int c = str.charAt(i);
			if ((c >= 0x0001) && (c <= 0x007F))
			{
				utflen++;
			} else if (c > 0x07FF)
			{
				utflen += 3;
			} else
			{
				utflen += 2;
			}
			if (utflen > 65535)
			{
				return str.substring(0, i); // truncate before the character that caused the overflow
			}
		}
		return str;
	}

	/**
	 * process EBCDIC encoded numbers
	 * 
	 * @param s
	 *            EBCDIC encoded number
	 * @return number
	 */
	private double getEBCDIC(String s)
	{
		double d;
		char[] arr = s.toCharArray();
		char c = arr[arr.length - 1];
		boolean negative = false;
		if (c == '{')
		{
			arr[arr.length - 1] = 0;
		} else if (c == '}')
		{
			arr[arr.length - 1] = 0;
			negative = true;
		} else if (c >= 'A' && c <= 'I')
		{
			arr[arr.length - 1] = (char) ((byte) c - (byte) 'A' + (byte) '1');
		} else if (c >= 'J' && c <= 'R')
		{
			arr[arr.length - 1] = (char) ((byte) c - (byte) 'J' + (byte) '1');
			negative = true;
		}
		d = Double.parseDouble(String.copyValueOf(arr));
		if (negative)
			d *= -1;
		return (d);
	}
	private long ZERO_DATE = (long) -62170128000000.0;

	private boolean isEmptyLine(SourceFile sf, String line)
	{
		for (int i = 0; i < sf.Columns.length; i++)
		{
			if (i < sf.Columns.length - 1)
			{
				if (line.startsWith(stbl.bulkLoadNull + "|"))
					line = line.substring((stbl.bulkLoadNull + "|").length());
				else
					return false;
			} else
			{
				if (line.equals(stbl.bulkLoadNull))
					return true;
				else
					return false;
			}
		}
		return false;
	}
	
	public long getWarningCount()
	{
		return warningCount.get();
	}
	
	public void clearMaps()
	{
		clearMaps(false);
	}
	
	public void clearMaps(boolean full)
	{
		if(full || doubleMap.size() > MAX_CACHE_MAP_SIZE)
		{
			doubleMap.clear();
		}
		if(full || formattedDateMap.size() > MAX_CACHE_MAP_SIZE)
		{
			formattedDateMap.clear();
		}
		if(full || formattedDateTimeMap.size() > MAX_CACHE_MAP_SIZE)
		{
			formattedDateTimeMap.clear();
		}
		if(full || dateCache.size() > MAX_CACHE_MAP_SIZE)
		{
			dateCache.clear();
		}
	}	
}
