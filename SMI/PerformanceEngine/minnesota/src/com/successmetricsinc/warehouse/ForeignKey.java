package com.successmetricsinc.warehouse;

import java.io.Serializable;

import org.jdom.Element;
import org.jdom.Namespace;

public class ForeignKey implements Serializable 
{
  public String Source;
  public int Type;
  public String Condition;
  public boolean Invalid;
  public boolean Redundant;
  
  public ForeignKey(Element e,Namespace ns)
  {
    Source = e.getChildText("Source",ns);
    String s = e.getChildText("Type",ns);
    if (s!=null)
      Type = Integer.parseInt(s);
    Condition = e.getChildText("Condition",ns);
    s = e.getChildText("Invalid",ns);
    if (s!=null)
      Invalid = Boolean.parseBoolean(s);
    s = e.getChildText("Redundant",ns);
    if (s!=null)
      Redundant = Boolean.parseBoolean(s);
  }
  
  public Element getForeignKeyElement(Namespace ns)
  {
    Element e = new Element("ForeignKey",ns);
    
    Element child = new Element("Source",ns);
    child.setText(Source);
    e.addContent(child);
    
    child = new Element("Type",ns);
    child.setText(String.valueOf(Type));
    e.addContent(child);
    
    if (Condition!=null)
    {
      child = new Element("Condition",ns);
      if (!Condition.isEmpty())
    	  child.setText(Condition);
      e.addContent(child);
    }
    
    child = new Element("Invalid",ns);
    child.setText(String.valueOf(Invalid));
    e.addContent(child);
    
    child = new Element("Redundant",ns);
    child.setText(String.valueOf(Redundant));
    e.addContent(child);
        
    return e;
  }
}
