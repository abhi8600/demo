/**
 * 
 */
package com.successmetricsinc.warehouse;

import java.io.Serializable;

/**
 * @author bpeters
 * 
 */
public class SampleMap implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String Dimension;
	public String Level;
	public String StagingTableName;
}
