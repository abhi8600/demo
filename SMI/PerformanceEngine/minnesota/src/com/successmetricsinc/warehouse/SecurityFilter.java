/**
 * 
 */
package com.successmetricsinc.warehouse;

import java.io.Serializable;

import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.Repository;

/**
 * @author bpeters
 * 
 */
public class SecurityFilter implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static int TYPE_VARIABLE = 0;
	public static int TYPE_SET_BASED = 1;
	public int Type;
	// Name of the Session Variable OR Direct Logical Query String for set based filter report
	public String SessionVariable;
	public boolean Enabled;
    public String[] FilterGroups;
    public boolean addIsNullJoinCondition;
    
    public SecurityFilter()
    {
    	
    }
    
    public SecurityFilter(Element e,Namespace ns)
    {
    	if (e!=null)
    	{
    		String temp = e.getChildText("Type",ns);
    		if (temp!=null)
    		{
    			Type = Integer.parseInt(temp);
    		}
    		SessionVariable = e.getChildText("SessionVariable",ns);
    		temp = e.getChildText("Enabled",ns);
    		if (temp!=null)
    		{
    			Enabled = Boolean.parseBoolean(temp);
    		}
    		FilterGroups = Repository.getInternedStringArrayChildren(e, "FilterGroups", ns);
    		temp = e.getChildText("addIsNullJoinCondition",ns);
    		if (temp!=null)
    		{
    			addIsNullJoinCondition = Boolean.parseBoolean(temp);
    		}
    	}
    }
    
    public Element getSecurityFilterElement(String elementName,Namespace ns)
    {
    	Element e = new Element(elementName,ns);    	    
		
		Element child = new Element("Type",ns);
		child.setText(String.valueOf(Type));
		e.addContent(child);
		
		child = new Element("SessionVariable",ns);
		child.setText(SessionVariable);
		e.addContent(child);
		
		child = new Element("Enabled",ns);
		child.setText(String.valueOf(Enabled));
		e.addContent(child);
				
		if (FilterGroups!=null && FilterGroups.length>0)
		{
			child = Repository.getStringArrayElement("FilterGroups", FilterGroups, ns);
			e.addContent(child);
		}
		
		if (addIsNullJoinCondition)
		{
			child = new Element("addIsNullJoinCondition",ns);
			child.setText(String.valueOf(addIsNullJoinCondition));
			e.addContent(child);
		}
				
    	return e;
    }
    
}
