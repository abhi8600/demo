/**
 * $Id: Script.java,v 1.6 2010-12-20 00:36:15 bpeters Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse;

import java.io.Serializable;

import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.JasperDataSourceProvider;
import com.successmetricsinc.util.RepositoryException;

public class Script implements StringReplacement, Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(Script.class);
	public String Name;
	public String Text;
	public ScriptGroup Group;

	public Script(ScriptGroup group, Element e, Namespace ns) throws RepositoryException
	{
		Group = group;
		Name = e.getChildText("Name", ns);
		Text = e.getChildText("Text", ns);
	}
	
	public Element getScriptElement(Namespace ns)
	{
		Element e = new Element("Script",ns);
		
		Element child = new Element("Name",ns);
		child.setText(Name);
		e.addContent(child);
		
		child = new Element("Text",ns);
		child.setText(Text);
		e.addContent(child);
				
		return e;
	}

	public void execute(Repository r, DatabaseConnection dc, Session s, JasperDataSourceProvider jdsp) throws Exception
	{
		logger.info("Executing script " + Name);
		Procedure proc = new Procedure(Name, Text);
		proc.setStringReplacement(this);
		proc.execute(r, dc, s, jdsp);
	}

	public String toString()
	{
		return Name;
	}

	public String getReplacedString(String result)
	{
		return result;
	}
}
