package com.successmetricsinc.warehouse;

import java.io.Serializable;

import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.Repository;

public class InheritTable implements Serializable 
{
  public int Type;
  public String Name;
  public String Prefix;
  
  public InheritTable(Element e, Namespace ns)
  {
    String s = e.getChildText("Type",ns);
    if (s!=null)
    {
      Type= Integer.parseInt(s);
    }
    Name= e.getChildText("Name",ns);
    Prefix= e.getChildText("Prefix",ns);
  }
  
  public Element getInheritTableElement(Namespace ns)
  {
    Element e = new Element("InheritTable",ns);
    
    Element child = new Element("Type",ns);
    child.setText(String.valueOf(Type));
    e.addContent(child);
    
    child = new Element("Name",ns);
    child.setText(Name);
    e.addContent(child);
    
    child = new Element("Prefix",ns);
    if (Prefix!=null && !Prefix.isEmpty())
    {
    	child.setText(Prefix);
    }
    e.addContent(child);
    
    return e;
  }
}
