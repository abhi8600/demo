package com.successmetricsinc.warehouse;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.query.Database;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.DimensionTable;
import com.successmetricsinc.query.MeasureTable;
import com.successmetricsinc.security.EncryptionService;
import com.successmetricsinc.util.Util;

public class ExportWarehouse
{
	private static Logger logger = Logger.getLogger(Bulkload.class);
	private Repository r;

	public ExportWarehouse(Repository r)
	{
		this.r = r;
	}

	public void export(int loadNum, String directory, String verifySchemaConnection)
	{
		DatabaseConnection dc = r.getDefaultConnection();
		DatabaseMetaData dmd = null;
		try
		{
			Connection conn = dc.ConnectionPool.getConnection();
			dmd = conn.getMetaData();
		} catch (SQLException e)
		{
			logger.error(e);
			return;
		}
		DatabaseMetaData verifydmd = null;
		if (verifySchemaConnection != null)
		{
			DatabaseConnection verifydc = r.findConnection(verifySchemaConnection);
			if (verifydc == null)
			{
				logger.error("Unable to find connection: " + verifySchemaConnection);
				return;
			}
			try
			{
				Connection qconn = verifydc.ConnectionPool.getConnection();
				verifydmd = qconn.getMetaData();
			} catch (SQLException e)
			{
				logger.error(e);
				return;
			}
		}
		logger.debug("Default Connection String: " + dc.ConnectString.toLowerCase());
		int sindex = dc.ConnectString.toLowerCase().indexOf("jdbc:sqlserver://");
		if (!DatabaseConnection.isDBTypeMSSQL(dc.DBType) || sindex < 0)
		{
			logger.error("Birst only supports MS SQL Server for this operation and connection strings must start with 'jdbc:sqlserver://'");
			return;
		}
		int eindex = dc.ConnectString.indexOf(';', sindex);
		String sname = eindex > 0 ? dc.ConnectString.substring(sindex + 17, eindex) : dc.ConnectString.substring(sindex);
		logger.debug("Server name: " + sname);
		String password = dc.Password;
		if (password != null && password.length() > 0)
			password = EncryptionService.getInstance().decrypt(password);
		String database = dc.decodeDatabase();
		List<String> tnames = new ArrayList<String>();
		for (DimensionTable dt : r.DimensionTables)
		{
			if (!dt.AutoGenerated || (dt.InheritTable != null && dt.InheritTable.length() > 0))
				continue;
			if (dt.TableSource.Tables.length != 1)
				continue;
			if (dt.TableSource.Schema != null && dt.TableSource.Schema.equals("dbo"))
				continue;
			tnames.add(dt.TableSource.Tables[0].PhysicalName);
		}
		for (MeasureTable mt : r.MeasureTables)
		{
			if (!mt.AutoGenerated || (mt.InheritTable != null && mt.InheritTable.length() > 0))
				continue;
			if (mt.TableSource.Tables.length != 1)
				continue;
			tnames.add(mt.TableSource.Tables[0].PhysicalName);
		}
		for (String tname : tnames)
		{
			String fname = directory + "\\" + tname + ".dat";
			try
			{
				List<Column> clist = getColumnList(dmd, dc.Schema, tname);
				List<Column> colList = new ArrayList<Column>();
				StringBuilder query = new StringBuilder("SELECT ");
				if (verifydmd != null)
				{
					List<Column> vlist = getColumnList(verifydmd, dc.Schema, tname);
					for (Column c : vlist)
					{
						boolean found = false;
						for (Column c2 : clist)
						{
							if (c2.name.equals(c.name))
							{
								found = true;
								break;
							}
						}
						if (found)
							colList.add(c);
					}
				} else
				{
					for (Column c : clist)
						colList.add(c);
				}
				boolean first = true;
				for (Column c : colList)
				{
					if (first)
						first = false;
					else
						query.append(',');
					query.append(c.name);
				}
				query.append(" FROM " + database + "." + dc.Schema + "." + tname);
				String command = "bcp \"" + query + "\" queryout " + directory + "\\" + tname + ".dat -S" + sname + " -U" + dc.UserName + " -P" + password
						+ " \"-t|\" -r\\n -c -C1252";
				logger.debug(command);
				logger.info("Exporting: " + database + "." + dc.Schema + "." + tname + " to " + directory + "\\" + tname + ".dat");
				Process p = Runtime.getRuntime().exec(command);
				BufferedReader stdOut = new BufferedReader(new InputStreamReader(p.getInputStream(),"UTF-8"));
				String line = null;
				while ((line = stdOut.readLine()) != null)
				{
					// logger.info(line);
				}
				stdOut.close();
				try
				{
					p.waitFor();
				} catch (InterruptedException e)
				{
					logger.error(e);
				}
				int exitvalue = p.exitValue();
				if (exitvalue != 0)
				{
					BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream(),"UTF-8"));
					StringBuilder sb = new StringBuilder();
					String s = null;
					while ((s = stdError.readLine()) != null)
					{
						sb.append(s);
					}
					if (sb.length() > 0)
						logger.error(sb.toString());
					else
						logger.error("General error bulk unloading table");
					stdError.close();
				}
				p.destroy();
				processDataFile(fname, colList);
			} catch (IOException e)
			{
				logger.error(e);
			} catch (SQLException e)
			{
				logger.error(e);
			}
		}
	}

	private void processDataFile(String fname, List<Column> clist) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(fname));
		BufferedWriter writer = new BufferedWriter(new FileWriter(fname + ".tmp"));
		int c = 0;
		int sep = (int) '|';
		int field = 0;
		boolean newline = true;
		char[] escapedquote = new char[2];
		escapedquote[0] = '\\';
		escapedquote[1] = '"';
		char[] carriagereturnlinefeed = new char[2];
		carriagereturnlinefeed[0] = '\r';
		carriagereturnlinefeed[1] = '\n';
		while ((c = reader.read()) >= 0)
		{
			if (newline)
			{
				if (c == '\n' || c == '\r')
					continue;
				field = 0;
				int t = clist.get(field).dtype;
				if (t == Types.VARCHAR || t == Types.NVARCHAR)
				{
					writer.write('"');
				}
				newline = false;
			}
			if (c == sep)
			{
				int t = clist.get(field).dtype;
				if (t == Types.VARCHAR || t == Types.NVARCHAR)
				{
					writer.write('"');
				}
				writer.write(sep);
				field++;
				t = clist.get(field).dtype;
				if (t == Types.VARCHAR || t == Types.NVARCHAR)
				{
					writer.write('"');
				}
			} else if (c == '"')
			{
				writer.write(escapedquote);
			} else if (field == clist.size() - 1 && (c == '\r' || c == '\n'))
			{
				int t = clist.get(field).dtype;
				if (t == Types.VARCHAR || t == Types.NVARCHAR)
				{
					writer.write('"');
				}
				newline = true;
				writer.write(carriagereturnlinefeed);
			} else
				writer.write(c);
		}
		reader.close();
		writer.close();
		File f = new File(fname);
		Util.secureDelete(f);
		f = new File(fname + ".tmp");
		f.renameTo(new File(fname));
	}

	private static class Column
	{
		String name;
		int dtype;
		int width;
	}

	private List<Column> getColumnList(DatabaseMetaData dmd, String schema, String tname) throws SQLException
	{
		List<Column> clist = new ArrayList<Column>();
		String driver = dmd.getDriverName();
		ResultSet rs = null;
		if (driver.toLowerCase().startsWith("mysql"))
			rs = dmd.getColumns(schema, null, tname, null);
		else
			rs = dmd.getColumns(null, schema, tname, null);
		while (rs.next())
		{
			Column c = new Column();
			c.name = rs.getString(4);
			c.dtype = rs.getInt(5);
			c.width = rs.getInt(7);
			clist.add(c);
		}
		return clist;
	}

	@SuppressWarnings("unused")
	private String buildFormatFile(DatabaseMetaData dmd, String schema, String tname, String directory) throws FileNotFoundException, SQLException
	{
		List<Column> clist = getColumnList(dmd, schema, tname);
		return buildFormatFile(clist, directory + "\\" + tname + ".fmt");
	}

	private String buildFormatFile(List<Column> clist, String fname) throws FileNotFoundException
	{
		String END_OF_LINE = "\\r\\n";
		String CHARTYPE = "CharTerm";
		PrintWriter writer = new PrintWriter(new FileOutputStream(fname));
		// write the preamble
		writer.write("<?xml version=\"1.0\"?>\n");
		writer.write("<BCPFORMAT");
		writer.write(" xmlns=\"http://schemas.microsoft.com/sqlserver/2004/bulkload/format\"");
		writer.write(" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n");
		writer.write("\t<RECORD>\n");
		String sep = "|";
		String quotesep = "\\\"|\\\"";
		int index = 1;
		for (Column c : clist)
		{
			if (index == 1 && (c.dtype == Types.VARCHAR || c.dtype == Types.NVARCHAR))
			{
				// If first column, add field with quote
				if (index == clist.size())
					writer.write("\t\t<FIELD ID=\"1\" xsi:type=\"" + CHARTYPE + "\" TERMINATOR=\"" + END_OF_LINE + "\"/>");
				else
					writer.write("\t\t<FIELD ID=\"1\" xsi:type=\"" + CHARTYPE + "\" TERMINATOR=\"\\\"\"/>");
				index++;
			}
			writer.write("\t\t<FIELD ID=\"" + index + "\" xsi:type=\"" + CHARTYPE + "\"");
			if (c != null && c.width > 0)
			{
				writer.write(" MAX_LENGTH=\"" + c.width + "\""); // length
			}
			if (index == clist.size())
			{
				writer.write(" TERMINATOR=\"" + END_OF_LINE + "\"");
			} else
			{
				String s = sep;
				if (c.dtype == Types.VARCHAR || c.dtype == Types.NVARCHAR)
					s = quotesep;
				if (index < clist.size() - 1 && (clist.get(index).dtype == Types.VARCHAR || clist.get(index).dtype == Types.NVARCHAR))
					s += "\\\"";
				writer.write(" TERMINATOR=\"" + s + "\"");
			}
			writer.write("/>\n");
			index++;
		}
		writer.write("\t</RECORD>\n");
		writer.write("\t<ROW>\n");
		index = 1;
		for (Column c : clist)
		{
			// map to BCP type names
			String type = null;
			String precisionScale = "";
			if (c.dtype == Types.VARCHAR || c.dtype == Types.NVARCHAR)
			{
				type = "SQLVARYCHAR";
			} else if (c.dtype == Types.INTEGER)
			{
				type = "SQLINT";
			} else if (c.dtype == Types.NUMERIC)
			{
				type = "SQLNUMERIC";
				precisionScale = " PRECISION=\"19\" SCALE=\"6\"";
			} else if (c.dtype == Types.TIMESTAMP || c.dtype == Types.DATE)
			{
				type = "SQLDATETIM8";
			} else if (c.dtype == Types.FLOAT || c.dtype == Types.DOUBLE)
			{
				type = "SQLFLT8";
			} else if (c.dtype == Types.TINYINT)
			{
				// Smallest datatype implicitly convertable into anything
				type = "SQLTINYINT";
			} else
				continue;
			writer.write("\t\t<COLUMN SOURCE=\"" + index + "\" NAME=\"" + c.name + "\" xsi:type=\"" + type + "\"" + precisionScale + "/>\n");
			index++;
		}
		writer.write("\t</ROW>\n");
		writer.write("</BCPFORMAT>\n");
		writer.close();
		return fname;
	}
}
