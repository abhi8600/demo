package com.successmetricsinc.warehouse;

import java.sql.SQLException;

import org.apache.log4j.Logger;
import com.successmetricsinc.Repository;
import com.successmetricsinc.ExecuteSteps.Step;
import com.successmetricsinc.ExecuteSteps.StepCommand;
import com.successmetricsinc.ExecuteSteps.StepCommandType;
import com.successmetricsinc.query.Database;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.status.Status;

public class TxnCommandHistoryConsolidator
{
	private static Logger logger = Logger.getLogger(TxnCommandHistoryConsolidator.class);
	private static final String TXN_COMMAND_HISTORY_TABLE_NAME = "TXN_COMMAND_HISTORY";
	private static final int UPDATE_INTERVAL = 10000; 
	private Repository r;
	private boolean isRunInPollingMode;
	private DatabaseConnection mainConnection;
	private Status mainStatus;
	private Status [] partitionStatus;
	private String iteration;
	private String processingGroup;
	private String loadGroup;
	
	public TxnCommandHistoryConsolidator(Repository r, DatabaseConnection dconn, String iteration, String loadGroup, 
			String[] subGroups, boolean isRunInPollingMode)
	{
		this.r = r;
		this.mainConnection = dconn;
		this.isRunInPollingMode = isRunInPollingMode;
		this.iteration = iteration;
		this.processingGroup = (subGroups != null && subGroups.length > 0)? subGroups[0] : null;
		this.loadGroup = loadGroup;
		init();
	}
	
	private void init()
	{
		try
		{
			if(mainConnection != null)
			{
				mainStatus = new Status(r, iteration, mainConnection);				
			}
			
			//Make sure txn_command_history table exists on all database connections and wait if not
			boolean txnCmdHistoryTableExists = false;
			do
			{
				if (mainConnection!=null)
				{
					txnCmdHistoryTableExists = Database.tableExists(r, mainConnection, TXN_COMMAND_HISTORY_TABLE_NAME, false);
				}
				if(!txnCmdHistoryTableExists)
				{
					logger.debug("TXN_COMMAND_HISTORY table doesn't exist on main connection, will check again after 10 seconds.");
				}
				else
				{
					for(int pcIdx = 0; pcIdx < mainConnection.partitionConnections.length; pcIdx++)
					{
						DatabaseConnection pdc = mainConnection.partitionConnections[pcIdx];
						txnCmdHistoryTableExists = Database.tableExists(r, pdc, TXN_COMMAND_HISTORY_TABLE_NAME, false);
						if(!txnCmdHistoryTableExists)
						{
							logger.debug("TXN_COMMAND_HISTORY table doesn't exist on connection: "+ pdc.Name+ ", will check again after 10 seconds.'");
							break;
						}
					}
					if(!txnCmdHistoryTableExists)
					{
						//Do consolidation every few seconds in polling mode
						try
						{
							Thread.sleep(UPDATE_INTERVAL);
						}
						catch(InterruptedException ie)
						{
							//Waking up early shouldn't be too bad - so do nothing.
						}
					}
				}
			}
			while(!txnCmdHistoryTableExists);
			
			if((mainConnection != null) && (mainConnection.partitionConnections != null) && (mainConnection.partitionConnections.length > 0))
			{
				partitionStatus = new Status[mainConnection.partitionConnections.length];
				for(int pcIdx = 0; pcIdx < mainConnection.partitionConnections.length; pcIdx++)
				{
					DatabaseConnection pdc = mainConnection.partitionConnections[pcIdx];
					partitionStatus[pcIdx] = new Status(r, iteration, pdc);
				}
			}
		}
		catch(SQLException sqle)
		{
			logger.error("TxnCommandHistoryConsolidator Init Failed: "+ sqle.toString(), sqle);
		}
	}
	
	public void consolidate()
	{
		if((mainConnection == null) || (mainStatus == null) || (partitionStatus == null) || (partitionStatus.length == 0))
		{
			logger.error("Nothing to consolidate, returning.");
			return;
		}
		boolean done = false;
		do
		{
			try
			{
				done = consolidateOnce();
			}
			catch(Exception e)
			{
				logger.error(e.toString(), e);
				//Break the loop
				isRunInPollingMode = false;
			}
			if(isRunInPollingMode && !done)
			{
				//Do consolidation every few seconds in polling mode
				try
				{
					Thread.sleep(UPDATE_INTERVAL);
				}
				catch(InterruptedException ie)
				{
					//Waking up early shouldn't be too bad - so do nothing.
				}
			}
		}
		while (isRunInPollingMode && !done);
	}
	
	/**
	 * Consolidates status across partitioned databases
	 * @return true if all step entries (e.g. generate schema, load staging, load warehouse and so no) are consolidated, 
	 * false otherwise
	 */
	private boolean consolidateOnce()
	{
		boolean done = false;
		boolean jumpToLoadMarker = false;
		Status.StatusCode curStatus = null;
		curStatus = consolidateStatusForStep(StepCommand.GenerateSchema, null);
		if(curStatus != null && (curStatus == Status.StatusCode.Complete || curStatus == Status.StatusCode.Failed))
		{
			if(curStatus == Status.StatusCode.Failed)
			{
				jumpToLoadMarker = true;
			}
			done = true;
		}
		if(!done)
		{
			return done;
		}

		done = false;
		if(!jumpToLoadMarker)
		{
			curStatus = consolidateStatusForStep(StepCommand.LoadStaging, loadGroup);
			if(curStatus != null && (curStatus == Status.StatusCode.Complete || curStatus == Status.StatusCode.Failed))
			{
				if(curStatus == Status.StatusCode.Failed)
				{
					jumpToLoadMarker = true;
				}
				done = true;
			}
			if(!done)
			{
				return done;
			}
		}

		done = false;
		if(!jumpToLoadMarker)
		{
			curStatus = consolidateStatusForStep(StepCommand.LoadWarehouse, loadGroup);
			if(curStatus != null && (curStatus == Status.StatusCode.Complete || curStatus == Status.StatusCode.Failed))
			{
				if(curStatus == Status.StatusCode.Failed)
				{
					jumpToLoadMarker = true;
				}
				done = true;
			}
			if(!done)
			{
				return done;
			}
		}

		done = false;
		if(!jumpToLoadMarker)
		{
			curStatus = consolidateStatusForStep(StepCommand.ExecuteScriptGroup, loadGroup);
			if(curStatus != null && (curStatus == Status.StatusCode.Complete || curStatus == Status.StatusCode.Failed))
			{
				if(curStatus == Status.StatusCode.Failed)
				{
					jumpToLoadMarker = true;
				}
				done = true;
			}
			if(!done)
			{
				return done;
			}
		}

		done = false;
		curStatus = consolidateStatusForStep(StepCommand.LoadMarker, loadGroup);
		if(curStatus != null && (curStatus == Status.StatusCode.Complete || curStatus == Status.StatusCode.Failed))
		{
			done = true;
		}
		if(!done)
		{
			return done;
		}
		
		return done;
	}
	
	private Status.StatusCode consolidateStatusForStep(StepCommand step, String substep)
	{
		Status.StatusCode sc = mainStatus.getMaxStatus(step, substep, processingGroup, true);

		if(sc == Status.StatusCode.Complete || sc == Status.StatusCode.Failed)
		{
			//This step is already done previously - so return true.
			return sc;
		}
		
		if(sc == null || sc == Status.StatusCode.Running)
		{
			Status.StatusCode lcdscFromPartitions = getLcdStatusFromPartitions(step, substep);
			if((sc == null) && (lcdscFromPartitions == Status.StatusCode.Running))
			{
				mainStatus.logStatus(new Step(step, StepCommandType.ETL, null), substep, processingGroup, Status.StatusCode.Running);
			}
			else if((sc == null) && (lcdscFromPartitions == Status.StatusCode.Complete))
			{
				mainStatus.logStatus(new Step(step, StepCommandType.ETL, null), substep, processingGroup, Status.StatusCode.Running);
				mainStatus.logStatus(new Step(step, StepCommandType.ETL, null), substep, processingGroup, Status.StatusCode.Complete);
				return lcdscFromPartitions;
			}
			else if((sc == null) && (lcdscFromPartitions == Status.StatusCode.Failed))
			{
				mainStatus.logStatus(new Step(step, StepCommandType.ETL, null), substep, processingGroup, Status.StatusCode.Running);
				mainStatus.logStatus(new Step(step, StepCommandType.ETL, null), substep, processingGroup, Status.StatusCode.Failed);
				return lcdscFromPartitions;
			}
			else if(sc == Status.StatusCode.Running)
			{
				if(lcdscFromPartitions == Status.StatusCode.Complete)
				{
					//To Do: Copy over entire content of all partition histories into main txn command history
					mainStatus.logStatus(new Step(step, StepCommandType.ETL, null), substep, processingGroup, Status.StatusCode.Complete);
					return lcdscFromPartitions;
				}
				else if(lcdscFromPartitions == Status.StatusCode.Failed)
				{
					//To Do: Copy over entire content of all partition histories into main txn command history
					mainStatus.logStatus(new Step(step, StepCommandType.ETL, null), substep, processingGroup, Status.StatusCode.Failed);
					return lcdscFromPartitions;					
				}
			}
		}

		return null;
	}
	
	/**
	 * Get least common denominator status from all partition statuses
	 * @param step
	 * @return
	 */
	private Status.StatusCode getLcdStatusFromPartitions(StepCommand step, String substep)
	{
		Status.StatusCode lcdsc = null;
		
		Status.StatusCode [] partitionsc = new Status.StatusCode[partitionStatus.length];
		
		int numRunning = 0;
		int numFailed = 0;
		int numComplete = 0;
		
		for(int i = 0; i < partitionStatus.length; i++)
		{
			partitionsc[i] = partitionStatus[i].getMaxStatus(step, substep, processingGroup, true);
			if(partitionsc[i] == Status.StatusCode.Running)
			{
				numRunning++;
			}
			else if(partitionsc[i] == Status.StatusCode.Complete)
			{
				numComplete++;
			}
			else if(partitionsc[i] == Status.StatusCode.Failed)
			{
				numFailed++;
			}
		}
		
		int totalPartitions = partitionsc.length;
		if(numRunning > 0)
		{
			//Any of the processes are running
			lcdsc = Status.StatusCode.Running;
		}
		else if(numComplete == totalPartitions)
		{
			//Everything completed successfully.
			lcdsc = Status.StatusCode.Complete; 
		}
		else if((numFailed > 0) && ((numFailed == totalPartitions) || ((numComplete + numFailed) == totalPartitions)))
		{
			//Either everything failed or some completed and some failed.
			lcdsc = Status.StatusCode.Failed;
		}
		
		return lcdsc;
	}
}