/**
 * $Id: StagingColumnGrainInfo.java,v 1.5 2010-12-20 00:36:15 bpeters Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse;

import java.io.Serializable;

import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.util.XmlUtils;

public class StagingColumnGrainInfo implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public enum NameType
	{
		Prefix, Rename
	};
	String Dimension;
	String Level;
	String StagingFilterName;
	String NewName;
	NameType NewNameType;
	String newNameType;
	public StagingAggregation Aggregation;

	public StagingColumnGrainInfo(Element e, Namespace ns)
	{
		Dimension = e.getChildText("Dimension", ns);
		Level = e.getChildText("Level", ns);
		StagingFilterName = e.getChildText("StagingFilterName", ns);
		NewName = e.getChildText("NewName", ns);
		newNameType = e.getChildText("NewNameType", ns);
		String s = e.getChildText("NewNameType", ns);
		NewNameType = s == null ? NameType.Prefix : Integer.valueOf(s) == 0 ? NameType.Prefix : NameType.Rename;
		Element age = e.getChild("Aggregation", ns);
		if (age != null)
			Aggregation = new StagingAggregation(age, ns);
	}
	
	public Element getStagingColumnGrainInfo(Namespace ns)
	{
		Element e = new Element("StagingColumnGrainInfo",ns);
		
		Element child = new Element("Dimension",ns);
		child.setText(Dimension);
		e.addContent(child);
		
		child = new Element("Level",ns);
		child.setText(Level);
		e.addContent(child);
		
		if (StagingFilterName!=null)
		{
			child = new Element("StagingFilterName",ns);
			child.setText(StagingFilterName);
			e.addContent(child);
		}
		
		XmlUtils.addContent(e, "NewName", NewName,ns);
	
		XmlUtils.addContent(e, "NewNameType", newNameType,ns);
		
		if (Aggregation!=null)
		{
			e.addContent(Aggregation.getStagingAggregationElement(ns));
		}
		
		return e;
	}
}
