/**
 * $Id: LoadWarehouse.java,v 1.402 2012-11-12 14:47:08 BIRST\asharma Exp $
 *
 * Copyright (C) 2007-2009 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.successmetricsinc.ExecuteSteps.Step;
import com.successmetricsinc.Repository;
import com.successmetricsinc.query.Database;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.MeasureColumn;
import com.successmetricsinc.query.MeasureTable;
import com.successmetricsinc.query.Query;
import com.successmetricsinc.util.Util;

public class IBFactLoader extends FactLoader{
	
	private static Logger logger = Logger.getLogger(IBFactLoader.class);

	public IBFactLoader(Repository r, DatabaseConnection dconn,	LoadWarehouse lw, Step step, List<StagingTable> stList) {
		super(r, dconn, lw, step, stList);
	}


	/*
	 * processes IncrementalSnapshotFact for Infobright Connection Infobright hangs to delete from table if it uses
	 * joins with itself. The workaround suggested by Infobright support is to create a temp table with all the
	 * deletekeys and use the same in subquery (IN clause)
	 */
	@Override
	protected void processForIncrementalSnapshotFact(Connection conn, MeasureTable mt, Set<String> mtKeys, Statement stmt, int loadID, StagingTable st,
			Set<String> logicalTableKeys,String processingGroup) throws SQLException
	{
		logger.debug(" Processing for incremental snapshot fact: delete rows loaded from previous runs for the same key combination present in current run");

		String qualifiedMeasureTableName = Database.getQualifiedTableName(dconn, mt.TableSource.Tables[0].PhysicalName);
		// first create temp table and populate it with the deletekeys for current load id
		StringBuilder keyCols = new StringBuilder();
		List<String> keyColsList = new ArrayList<String>();
		boolean firstKey = true;
		for (String mtk : mtKeys)
		{
			if (firstKey)
				firstKey = false;
			else
				keyCols.append(", ");
			if (mtk.contains("."))
				mtk = mtk.substring(mtk.lastIndexOf(".") + 1);
			keyCols.append(mtk);
			keyColsList.add(mtk);
		}
		boolean isSingleDeleteKey = false;
		if (mtKeys.size() == 1)
			isSingleDeleteKey = true;
		StringBuilder createTableSQL = new StringBuilder();
		long id = System.currentTimeMillis();
		String tempKeysTableName = dconn.Schema + ".temp" + id;
		createTableSQL.append("CREATE TABLE ").append(tempKeysTableName);
		createTableSQL.append(dconn.isIBLoadWarehouseUsingUTF8() ? " CHARACTER SET UTF8 " : ""); 
		createTableSQL.append(" AS SELECT ").append(keyCols.toString()).append(", MAX(LOAD_ID) AS 'LOAD_ID' FROM ").append(qualifiedMeasureTableName);
		createTableSQL.append(" GROUP BY ").append(keyCols.toString());
		Database.clearCacheForConnection(dconn);			
		logger.info("Temp Table Query:" + id + ":" + Query.getPrettyQuery(createTableSQL));
		int rows = Database.retryExecuteUpdate(dconn, stmt, createTableSQL.toString());		
		logger.info(rows + " rows inserted into temp table " + tempKeysTableName);
		StringBuilder createTempFactTableSQL = new StringBuilder();
		long id2 = System.currentTimeMillis() + 1;
		String tempFactTableName = dconn.Schema + ".temp" + id2;
		createTempFactTableSQL.append("CREATE TABLE ").append(tempFactTableName);
		createTempFactTableSQL.append(dconn.isIBLoadWarehouseUsingUTF8() ? " CHARACTER SET UTF8 " : "");
		createTempFactTableSQL.append(" AS SELECT A.* FROM ").append(qualifiedMeasureTableName + " A ").append(" INNER JOIN ");
		createTempFactTableSQL.append(tempKeysTableName + " B").append(" ON ");
		if (isSingleDeleteKey)
			createTempFactTableSQL.append("A." + keyCols.toString() + "=B." + keyCols.toString());
		else
		{
			boolean first = true;
			for (String mtk : mtKeys)
			{
				if (first)
					first = false;
				else
					createTempFactTableSQL.append(" AND ");
				if (mtk.contains("."))
					mtk = mtk.substring(mtk.lastIndexOf(".") + 1);
				createTempFactTableSQL.append("A." + mtk + "=B." + mtk);
			}
		}
		createTempFactTableSQL.append(" AND A.LOAD_ID=B.LOAD_ID");
		Database.clearCacheForConnection(dconn);
		logger.info("Temp Fact Table Query:" + id2 + ":" + Query.getPrettyQuery(createTempFactTableSQL));
		rows = Database.retryExecuteUpdate(dconn, stmt, createTempFactTableSQL.toString());
		logger.info(rows + " rows inserted into temp fact table " + createTempFactTableSQL);
		StringBuilder insertTempFactTableSQL = new StringBuilder();
		insertTempFactTableSQL.append("INSERT INTO ").append(tempFactTableName);
		insertTempFactTableSQL.append(" SELECT * FROM ").append(qualifiedMeasureTableName);
		insertTempFactTableSQL.append(" WHERE ");
		if (isSingleDeleteKey)
			insertTempFactTableSQL.append(keyCols.toString()).append(" IS NULL");
		else
		{
			insertTempFactTableSQL.append("concat(").append(keyCols.toString()).append(")").append(" IS NULL");
		}
		logger.info("Insert Temp Fact Table Query:" + id2 + ":" + Query.getPrettyQuery(insertTempFactTableSQL));
		rows = stmt.executeUpdate(insertTempFactTableSQL.toString());
		logger.info(rows + " rows inserted for null keys into temp fact table " + insertTempFactTableSQL);
		logger.debug("Dropping temp table " + tempKeysTableName);
		stmt.executeUpdate("DROP TABLE " + tempKeysTableName);
		logger.debug("Dropping fact table " + qualifiedMeasureTableName);
		stmt.executeUpdate("DROP TABLE " + qualifiedMeasureTableName);
		logger.debug("Renaming " + tempFactTableName + " TO " + qualifiedMeasureTableName);
		stmt.executeUpdate("RENAME TABLE " + tempFactTableName + " TO " + qualifiedMeasureTableName);
		if (!conn.getAutoCommit())
			conn.commit();		
	}
	
	/**
	 * Deletes are terribly slow in Infobright - so replace with inserts
	 * 
	 * @param stmt
	 * @param conn
	 * @param mtname
	 * @throws SQLException 
	 */
	@Override
	protected void convertInfobrightDeletes(Statement stmt, Connection conn, String mtname, MeasureTable mt, StagingTable st, List<String> cnames,
			List<String> pnames, int loadID, StringBuilder keyfilter, String grainKeyName) throws SQLException
	{
		StringBuilder create = new StringBuilder();
		create = new StringBuilder("CREATE TABLE " + dconn.Schema + ".temp1 (");
		List<Object[]> collist = Database.getTableSchema(dconn, conn, null, dconn.Schema, mtname);
		StringBuilder insert = new StringBuilder("INSERT INTO " + dconn.Schema + ".temp1 SELECT ");
		StringBuilder insertCols = new StringBuilder();
		StringBuilder selectCols = new StringBuilder();
		boolean columnUpdatePresent = false;
		int rows = 0;
		for (int i = 0; i < collist.size(); i++)
		{
			MeasureColumn mc = null;
			for (MeasureColumn smc : mt.MeasureColumns)
			{
				if (smc.Qualify && smc.PhysicalName.equalsIgnoreCase(mtname + "." + collist.get(i)[0]))
				{
					mc = smc;
					break;
				}
			}
			if (i > 0)
			{
				create.append(',');
				insertCols.append(',');
				insert.append(',');
				selectCols.append(",");
			}
			if (mc != null && cnames.contains(mc.PhysicalName))
			{
				String projname = pnames.get(cnames.indexOf(mc.PhysicalName));
				insert.append(projname + " " + collist.get(i)[0]);
				if(projname.startsWith("B."))
				{
					columnUpdatePresent = true;
				}
			} else
			{
				insert.append("A." + collist.get(i)[0]);
			}
			String type = collist.get(i)[2].toString();
			if (type.equalsIgnoreCase("varchar") || type.toLowerCase().contains("varchar"))
			{
				type += ("(" + collist.get(i)[3].toString() + ") comment 'for_insert'");
			}
			else if (type.equalsIgnoreCase("biginit"))
			{
				type += (" comment 'for_insert'");
			}
			create.append(collist.get(i)[0] + " " + type);
			insertCols.append(collist.get(i)[0]);
			selectCols.append("A." + collist.get(i)[0]);
		}
		insert.append(" FROM " + Database.getQualifiedTableName(dconn, mtname));
		insert.append(" A INNER JOIN ");
		if (st.SourceType == StagingTable.SOURCE_QUERY && !st.PersistQuery)
		{
			st.updateStagingTableQuery(r);
			insert.append("(" + st.Query + ")");
		} else
			insert.append(Database.getQualifiedTableName(dconn, Util.replaceWithPhysicalString(st.Name)));
		insert.append(" B ON ");
		insert.append(keyfilter);
		insert.append(" WHERE A.LOAD_ID=" + loadID);
		create.append(")");
		if(dconn.isIBLoadWarehouseUsingUTF8()) 
		{
			create.append(" CHARSET=utf8");
		}
		Database.clearCacheForConnection(dconn);
		stmt.executeUpdate("DROP TABLE IF EXISTS " + dconn.Schema + ".temp1");
		logger.debug(Query.getPrettyQuery(create));
		Database.retryExecuteUpdate(dconn, stmt, create.toString());
		logger.debug(Query.getPrettyQuery(insert));
		
		int rowsInserted = stmt.executeUpdate(insert.toString());
		if(rowsInserted <= 0)
		{
			logger.debug("Nothing to update from staging table:" + st.Name);
			stmt.executeUpdate("DROP TABLE IF EXISTS " + dconn.Schema + ".temp1");
			return;
		}
		else
		{
			logger.debug("["+rowsInserted+" rows inserted into temp1]");
		}
		stmt.executeUpdate("DROP TABLE IF EXISTS " + dconn.Schema + ".temp2");
	
		int width = 255;
		for (int i = 0; i < collist.size(); i++)
		{
			if (collist.get(i)[0].equals(grainKeyName))
				width = Integer.valueOf(collist.get(i)[3].toString());
		}
		Database.clearCacheForConnection(dconn);
		stmt.executeUpdate("DROP TABLE IF EXISTS " + dconn.Schema + ".temp2");
		StringBuilder tempInsert = new StringBuilder("CREATE TABLE " + dconn.Schema + ".temp2 (" + grainKeyName + ' ' + dconn.getVarcharType(width, true) + ")");
		if(dconn.isIBLoadWarehouseUsingUTF8()) 
		{
			tempInsert.append(" CHARSET=utf8");
		}
		logger.debug(Query.getPrettyQuery(tempInsert));
		Database.retryExecuteUpdate(dconn, stmt, tempInsert.toString());
		tempInsert = new StringBuilder("INSERT INTO " + dconn.Schema + ".temp2");
		tempInsert.append(" SELECT A."+grainKeyName+" FROM ");
		tempInsert.append(Database.getQualifiedTableName(dconn, mtname)+" A");
		tempInsert.append(" LEFT OUTER JOIN "+ dconn.Schema + ".temp1 B ON");
		tempInsert.append(" A." + grainKeyName + "=" + "B." + grainKeyName);
		tempInsert.append(" WHERE A.LOAD_ID=" + loadID + " AND B." + grainKeyName + " IS NULL");
		logger.debug(Query.getPrettyQuery(tempInsert));
		rows = stmt.executeUpdate(tempInsert.toString());
		logger.debug(rows + " rows inserted");
		
		insert = new StringBuilder("INSERT INTO " + dconn.Schema + ".temp1");
		insert.append(" (" + insertCols + ") SELECT " + selectCols);
		insert.append(" FROM " + Database.getQualifiedTableName(dconn, mtname) + " A");
		StringBuilder insert2 = new StringBuilder(insert);
		insert.append(" INNER JOIN " + dconn.Schema + ".temp2 B ON");
		insert.append(" A." + grainKeyName + "=" + "B." + grainKeyName);
		insert.append(" WHERE LOAD_ID=" + loadID);
		logger.debug(Query.getPrettyQuery(insert));
		rows = stmt.executeUpdate(insert.toString());
		logger.debug(rows + " rows inserted");
		
		if ((rows > 0) || columnUpdatePresent)
		{
			insert2.append(" WHERE A.LOAD_ID<>" + loadID);
			logger.debug(Query.getPrettyQuery(insert2));
			int rows2 = stmt.executeUpdate(insert2.toString());
			logger.debug(rows2 + " rows inserted");

			String dropTemp2 = "DROP TABLE IF EXISTS " + dconn.Schema + ".temp2";
			logger.debug(Query.getPrettyQuery(dropTemp2));
			stmt.executeUpdate(dropTemp2);
			
			String renameQuery = "RENAME TABLE " + Database.getQualifiedTableName(dconn, mtname) + " TO " + dconn.Schema + ".temp2";
			logger.debug(Query.getPrettyQuery(renameQuery));
			stmt.executeUpdate(renameQuery);

			renameQuery = "RENAME TABLE " + dconn.Schema + ".temp1 TO " + Database.getQualifiedTableName(dconn, mtname);
			logger.debug(Query.getPrettyQuery(renameQuery));
			stmt.executeUpdate(renameQuery);
			
			logger.debug(Query.getPrettyQuery(dropTemp2));
			stmt.executeUpdate(dropTemp2);
		}
		stmt.executeUpdate("DROP TABLE IF EXISTS " + dconn.Schema + ".temp1");
		stmt.executeUpdate("DROP TABLE IF EXISTS " + dconn.Schema + ".temp2");
	}
	
}
