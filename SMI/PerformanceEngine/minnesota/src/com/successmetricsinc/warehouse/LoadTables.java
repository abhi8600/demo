/**
 * $Id: LoadTables.java,v 1.124 2012-11-12 14:47:13 BIRST\mjani Exp $
 *
 * Copyright (C) 2007-2009 Success Metrics, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.crypto.SecretKey;

import org.apache.log4j.Logger;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.successmetricsinc.ExecuteSteps.Step;
import com.successmetricsinc.ExecuteSteps.StepCommand;
import com.successmetricsinc.ExecuteSteps.StepCommandType;
import com.successmetricsinc.Repository;
import com.successmetricsinc.query.Database;
import com.successmetricsinc.query.Database.DatabaseType;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.DimensionTable;
import com.successmetricsinc.query.Hierarchy;
import com.successmetricsinc.query.Level;
import com.successmetricsinc.query.Query;
import com.successmetricsinc.status.Status;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.util.DateUtil;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.warehouse.Bulkload.ProcessResult;

/**
 * @author Brad Peters
 * 
 */
public class LoadTables
{
	private static Logger logger = Logger.getLogger(LoadTables.class);
	Repository r;
	private Map<String, Boolean> statusMap = null;
	private Map<StagingTable, Boolean> missingOptionalSF = null;
	private boolean failed;
	private String stagingTableName;
	private boolean continueOnScriptFailure = false;
	private List<StagingTable> scriptPath;
	private boolean ignoreCompleteStep = false;
	private boolean ignoreVisualizationSourceLoad = false;

	public LoadTables(Repository r)
	{
		this.r = r;
	}

	public void setStagingTableName(String stagingTableName)
	{
		this.stagingTableName = stagingTableName;
	}

	private boolean isScriptedSource(StagingTable st)
	{
		return (st.SourceType != StagingTable.SOURCE_FILE || 
				(st.Script != null && st.Script.InputQuery != null && st.Script.Script != null && st.Script.Script.length() > 0));
	}
	
	public void setIgnoreCompleteStep(boolean value)
	{
		this.ignoreCompleteStep = value;
	}
	
	public void setIgnoreVisualizationSourceLoad(boolean value)
	{
		this.ignoreVisualizationSourceLoad = value;
	}
	
	private boolean isSourceLoaded(StagingTable st, Status status, String substep, String loadGroup, String processingGroup) throws SQLException
	{
		boolean isLoaded = false;
		// first check if it is from UI -- visualization sources should not be loaded. This will mess up the warehouse
		// since Staging tables are the final warehouse tables
		if(ignoreVisualizationSourceLoad)
		{
			if(st.DiscoveryTable)
			{
				return true;
			}
		}
		
		if (!ignoreCompleteStep)//do not check for completion, load the tables anyway - required for executing scripts from UI
		{
			if (isScriptedSource(st))
			{
				//check if loadstaging is performed for this staging table for CURRENT processing group
				if (status.isComplete(StepCommand.LoadStaging, substep, processingGroup, true))
				{
					logger.info("Staging table " + st.Name + " already loaded successfully for load id =" + status.getIteration()
							+ (loadGroup == null ? "" : (", for load group " + loadGroup)) 
							+ (processingGroup == null ? "" : (", for processingGroup " + processingGroup))
							+ ". This step will not be repeated.");
					isLoaded = true;
				}
			}
			else
			{
				//check if loadstaging is performed for this staging table for ANY processing group
				if (status.isComplete(StepCommand.LoadStaging, substep))
				{
					logger.info("Staging table " + st.Name + " already loaded successfully for load id =" + status.getIteration()
							+ (loadGroup == null ? "" : (", for load group " + loadGroup)) 
							+ ". This step will not be repeated.");
					isLoaded = true;
				}
			}
		}
		return isLoaded;
	}

	public boolean loadStaging(String path, String databasePath, Status status, boolean debug, boolean ignoreErrors, String loadGroup, String[] subGroups,
			int numRows, boolean skipTransformations, List<StagingTable> bulkLoaded, AmazonS3 s3client, AWSCredentials s3credentials, SecretKey s3secretKey, Repository r)
	{
		Step step = new Step(StepCommand.LoadStaging, StepCommandType.ETL, null);
		failed = false;
		statusMap = new HashMap<String, Boolean>();
		HashMap<StagingTable, ProcessResult> rowsProcessed = new HashMap<StagingTable, ProcessResult>();
		Set<StagingTable> nonPersistantQueryBased = new HashSet<StagingTable>();
		missingOptionalSF = new HashMap<StagingTable, Boolean>();
		String processingGroup = (subGroups != null && subGroups.length > 0)? subGroups[0] : null; 
		DatabaseConnection dc = r.getDefaultConnection();
		try
		{
			if (!ignoreCompleteStep)
			{
				if (status.isComplete(StepCommand.LoadStaging, loadGroup, processingGroup, true))
				{
					logger.info("Staging tables already loaded successfully for load id =" + status.getIteration()
							+ (loadGroup == null ? "" : (", for load group " + loadGroup)) 
							+ (processingGroup == null ? "" : (", for processingGroup " + processingGroup))
							+ ". This step will not be repeated.");
					return true;
				}
			}
			status.logStatus(step, loadGroup, processingGroup, Status.StatusCode.Running);
			if(r.getCurrentBuilderVersion() < 10  && DatabaseConnection.isDBTypeOracle(dc.DBType))
			{
				logger.error("Oracle database load not supported for builder version less than 10. Please fix repository and try again.");
				throw new Exception("Oracle database load not supported for builder version less than 10. Please fix repository and try again.");
			}
			for (StagingTable st : r.getStagingTables())
			{
				if (st.SourceType == StagingTable.SOURCE_QUERY && !st.PersistQuery)
					nonPersistantQueryBased.add(st);
			}
			List<StagingTable> stlist = new ArrayList<StagingTable>();
			List<String> unmodifiedSources = status.getUnmodifiedSources(true);
			List<String> sampledStagingTables = status.getSampledStagingTable(path);
			for (StagingTable st : r.getStagingTables())
			{
				if (st.Disabled || st.Imported || st.LiveAccess)
					continue;
				if (stagingTableName != null && !st.Name.equals(stagingTableName))
					continue;
				if (bulkLoaded != null && bulkLoaded.contains(st))
					continue;
				// Don't load tables that have already been loaded and for which sampling has not been configured
				if (unmodifiedSources != null && unmodifiedSources.contains(st.Name))
				{
					if(sampledStagingTables.contains(st.Name)){
						unmodifiedSources.remove(st.Name);
					}else{
						continue;
					}
				}
				boolean belongs = st.belongsToLoadGroup(loadGroup);
				if (!belongs)
				{
					logger.info("Staging table " + st.Name + " does not belong to load group " + loadGroup + ". The staging table will not be loaded.");
					continue;
				}
				belongs = st.belongsToSubGroups(subGroups);
				if (!belongs && (stagingTableName == null || !st.Name.equals(stagingTableName)))
				{
					StringBuilder sb = new StringBuilder();
					for (String s : subGroups)
					{
						if (sb.length() > 0)
							sb.append(',');
						sb.append(s);
					}
					logger.info("Staging table " + st.Name + " does not belong to sub group from list [" + sb
							+ "]. The staging table will not be used for loadstaging.");
					continue;
				}
				String substep = getSubstep(st, loadGroup);
				if (isSourceLoaded(st, status, substep, loadGroup, processingGroup))
				{
					statusMap.put(st.Name, true);
					continue;
				}
				st.setRepository(r);
				stlist.add(st);
			}
			Set<StagingTable> skippedStagingTables = new HashSet<StagingTable>();
			
			// drop column store indexes
			if (dc.DBType == DatabaseType.MSSQL2012ColumnStore)
			{
				for (StagingTable st: stlist)
				{
					Database.dropColumnStoreIndex(dc, Util.replaceWithPhysicalString(st.Name));
				}
			}
			
			// iterate over the tables, doing the bulk load
			for (StagingTable st : stlist)
			{
				if (isSourceLoaded(st, status, getSubstep(st, loadGroup), loadGroup, processingGroup))
				{
					skippedStagingTables.add(st);
					statusMap.put(st.Name, true);
					continue;
				}
				if (st.SourceType == StagingTable.SOURCE_FILE)
				{
					String substep = getSubstep(st, loadGroup);
					try
					{
						status.logStatus(step, substep, processingGroup, Status.StatusCode.Running);
						SourceFile sf = r.findSourceFile(st.SourceFile);
						StagingTableBulkLoadInfo stbl = st.getStagingTableBulkLoadInfo(sf, dc, r);
						if (st.isScript())
						{
							TransformationScript sc = new TransformationScript(r, null);
							sc.setS3Credentials(s3credentials);
							sc.setS3Client(s3client);
							sc.setS3SecretKey(s3secretKey);
							sc.processingGroup = processingGroup;
							sc.setLoadAllBaseTables(ignoreCompleteStep);//pass the value received from caller
							sc.setDoNotLoadVisulationSources(ignoreVisualizationSourceLoad);
							try
							{
								sc.executeScript(status, st.Name, st.Script, loadGroup, path, databasePath, numRows, bulkLoaded, stbl, sf, scriptPath);
							} catch (ScriptException se)
							{
								if (continueOnScriptFailure)
								{
									// The processing has failed
									st.Disabled = true;
									logger.error("Disabling source " + st.Name + ", failed to execute script: " + se.getMessage());
									continue;
								} else
									throw se;
							}
						}
						Map<DimensionTable, List<StagingColumn>> scmap = st.getStagingColumnMap(r);
						if (sf != null)
						{
							Bulkload bl = new Bulkload();
							long startTime = System.currentTimeMillis();
							ProcessResult pr = bl.load(r, dc, path, databasePath, sf, st, scmap, debug, numRows, stbl, s3client, s3credentials, s3secretKey);
							// if this failed, there is no need to continue on with processing the staging table
							if (pr == null || pr.numRowsProcessed < 0)
							{
								List<SourceFile.BirstDataSource> sfLst = sf.getPhysicalSourceFiles(path,
										st.Script != null && st.Script.InputQuery != null, r, true, dc, null);
								if (sf.ContinueIfMissing && (sfLst == null || sfLst.isEmpty()))
								{
									missingOptionalSF.put(st, Boolean.TRUE);
									continue;
								}
								// The processing has failed and not because optional sourcefile is missing
								throw new Exception("Failed to bulkload " + st.Name);
							}
							if (pr.numRowsProcessed < sf.MinRows)
								throw new Exception("Failed to bulkload. For \"" + sf.FileName + "\" number of rows processed are \"" + pr.numRowsProcessed
										+ "\" lesser than minimum rows to be processed \"" + sf.MinRows + "\"");
							long endTime = System.currentTimeMillis();
							pr.duration = endTime - startTime;
							rowsProcessed.put(st, pr);
							if (bulkLoaded != null)
								bulkLoaded.add(st);
						} else
						{
							logger.error("No source file matches " + st.SourceFile);
						}
					} catch (Exception ex)
					{
						logger.error(ex, ex);
						failed = true;
						status.logStatus(step, substep, processingGroup, ScriptException.class.isInstance(ex) ? Status.StatusCode.ScriptFailure : Status.StatusCode.Failed, -1,
								-1, -1, -1, ex.getMessage());
						if (ignoreErrors)
							continue;
						else
							throw new Exception(ex.getMessage());
					}
					statusMap.put(st.Name, Boolean.TRUE);
				}
			}
			if (!skipTransformations)
			{
				for (StagingTable st : getStagingTablesForTransformationAtStage(Transformation.AFTER_FILES_STAGED, stlist))
				{
					performTransformation(st, loadGroup, processingGroup, dc, step, status, ignoreErrors, Transformation.AFTER_FILES_STAGED);
				}
			}
			// iterate over the tables, doing query sources
			for (StagingTable st : stlist)
			{
				if (nonPersistantQueryBased.contains(st))
					continue;
				if (st.SourceType == StagingTable.SOURCE_QUERY && st.PersistQuery)
				{
					String substep = getSubstep(st, loadGroup);
					try
					{
						if (status.isComplete(StepCommand.LoadStaging, substep, processingGroup, true))
						{
							logger.info("Staging table " + st.Name + " already loaded successfully for load id =" + status.getIteration()
									+ (loadGroup == null ? "" : (", for load group " + loadGroup)) 
									+ (processingGroup == null ? "" : (", for processingGroup " + processingGroup))
									+ ". This step will not be repeated.");
							continue;
						}
						status.logStatus(step, substep, processingGroup, Status.StatusCode.Running);
						st.updateStagingTableQuery(r);
						Map<DimensionTable, List<StagingColumn>> scmap = st.getStagingColumnMap(r);
						// Create the physical staging table in the database
						Database.createPhysicalStagingTable(r, dc, st, scmap);
						// Now insert records into staging table using the query specified in the repository.
						StringBuilder insertBuf = new StringBuilder();
						insertBuf.append("INSERT INTO " + Database.getQualifiedTableName(dc, Util.replaceWithPhysicalString(st.Name)) + " ( ");
						List<String> colNames = new ArrayList<String>();
						List<String> colTypes = new ArrayList<String>();
						st.getCreateColumns(dc, r, colNames, colTypes, dc.isUnicodeDatabase(), new ArrayList<String>(), new ArrayList<String>(), null, null, null);
						boolean firstCol = true;
						for (String cname : colNames)
						{
							if (firstCol)
							{
								insertBuf.append(cname);
							} else
							{
								insertBuf.append(", " + cname);
							}
							firstCol = false;
						}
						insertBuf.append(" ) ");
						insertBuf.append(st.Query);
						logger.info("Inserting new records into staging table " + st.Name);
						logger.debug(Query.getPrettyQuery(insertBuf));
						Connection conn = dc.ConnectionPool.getConnection();
						Statement stmt = conn.createStatement();
						int rows = stmt.executeUpdate(insertBuf.toString());
						logger.info(rows + " rows inserted");
					} catch (Exception ex)
					{
						logger.error(ex.getMessage());
						failed = true;
						status.logStatus(step, substep, processingGroup, Status.StatusCode.Failed);
						if (ignoreErrors)
							continue;
						else
							throw new Exception(ex.getMessage());
					}
					statusMap.put(st.Name, Boolean.TRUE);
				}
			}
			if (!skipTransformations)
			{
				for (StagingTable st : getStagingTablesForTransformationAtStage(Transformation.AFTER_QUERY_BASED_TABLES_GENERATED, stlist))
				{
					performTransformation(st, loadGroup, processingGroup, dc, step, status, ignoreErrors, Transformation.AFTER_QUERY_BASED_TABLES_GENERATED);
				}
			}
			// iterate over the tables, replacing the keys
			for (StagingTable st : stlist)
			{
				// in new version, replace keys in pre-processing
				if (r.getCurrentBuilderVersion() >= 10)
					continue;
				if (nonPersistantQueryBased.contains(st))
					continue;
				String substep = getSubstep(st, loadGroup);
				try
				{
					if (!statusMap.containsKey(st.Name))
					{
						if (missingOptionalSF.containsKey(st) && missingOptionalSF.get(st).equals(Boolean.TRUE))
						{
							logger.debug("Optional Sourcefile " + st.SourceFile + " is missing, " + "skipping unknown keys replacement for staging table "
									+ st.Name + ".");
							continue;
						}
						throw new Exception(st.Name + ": previous steps failed, no reason to replace unknown keys");
					}
					// validate that the table exists
					if (!Database.tableExists(r, dc, Util.replaceWithPhysicalString(st.Name), false))
					{
						throw new Exception(st.Name + " does not exist, no reason to replace unknown keys");
					}
					// Update any natural keys with their unknown values
					logger.info("Replacing unknown keys in " + st.Name);
					for (StagingColumn sc : st.Columns)
					{
						if (sc.UnknownValue != null && sc.UnknownValue.length() > 0)
						{
							String scname = Util.replaceWithNewPhysicalString(sc.getPhysicalName(), dc,r);
							String query = null;
							String from = DatabaseConnection.isDBTypeMemDB(dc.DBType) ? " FROM "
									+ Database.getQualifiedTableName(dc, Util.replaceWithPhysicalString(st.Name)) : "";
							if (sc.DataType.equals("Varchar"))
							{
								query = "UPDATE " + Database.getQualifiedTableName(dc, Util.replaceWithPhysicalString(st.Name)) + " SET " + scname + "='"
										+ sc.UnknownValue + "'" + from + " WHERE " + scname + " IS NULL";
							} else if (sc.DataType.equals("DateTime") || sc.DataType.equals("Date"))
							{
								StringBuilder querysb = new StringBuilder();
								querysb.append("UPDATE " + Database.getQualifiedTableName(dc, Util.replaceWithPhysicalString(st.Name)) + " SET " + scname
										+ "='" + sc.UnknownValue + "'");
								Date unknownDateVal = DateUtil.parseUsingPossibleFormats(sc.UnknownValue);
								if (unknownDateVal == null)
								{
									logger.error("Unknown value provided for staging column " + sc.Name + " on staging table " + st.Name
											+ " is not in parsable date format. The corresponding date id columns "
											+ "will not be updated and may result in incorrect reporting of unknown facts.");
								} else
								{
									Calendar cal = Calendar.getInstance();
									cal.setTimeInMillis(unknownDateVal.getTime());
									List<StagingColumn> dateIDCols = st.getDateIDColumns(sc);
									if (dateIDCols != null && dateIDCols.size() > 0)
									{
										for (StagingColumn dateidsc : dateIDCols)
										{
											DateID di = new DateID();
											di.SourceFileColumnName = dateidsc.SourceFileColumn;
											di.setType(dateidsc.DataType);
											int dateID = di.getDateID(r.getTimeDefinition(), cal);
											querysb.append(", " + Util.replaceWithNewPhysicalString(dateidsc.Name, dc,r) + "=" + dateID);
										}
									}
								}
								querysb.append(from + " WHERE " + scname + " IS NULL");
								query = querysb.toString();
							} else
								query = "UPDATE " + Database.getQualifiedTableName(dc, Util.replaceWithPhysicalString(st.Name)) + " SET " + scname + "="
										+ sc.UnknownValue + from + " WHERE " + scname + " IS NULL";
							logger.debug(Query.getPrettyQuery(query));
							dc.ConnectionPool.getConnection().createStatement().executeUpdate(query);
						}
					}
				} catch (Exception ex)
				{
					logger.error(ex.getMessage());
					failed = true;
					status.logStatus(step, substep, processingGroup, Status.StatusCode.Failed);
					if (ignoreErrors)
						continue;
					else
						throw new Exception(ex.getMessage());
				}
			}
			if (!skipTransformations)
			{
				for (StagingTable st : getStagingTablesForTransformationAtStage(Transformation.AFTER_UNKNOWN_KEYS_REPLACED, stlist))
				{
					performTransformation(st, loadGroup, processingGroup, dc, step, status, ignoreErrors, Transformation.AFTER_UNKNOWN_KEYS_REPLACED);
				}
				for (StagingTable st : getStagingTablesForTransformationAtStage(Transformation.DO_NOT_EXECUTE, stlist))
				{
					for (Transformation tr : st.Transformations)
					{
						if (tr.ExecuteAfter == Transformation.DO_NOT_EXECUTE && Procedure.class == tr.getClass())
							logger.debug(st.Name + " - Not executing procedure code (" + ((Procedure) tr).Name + ")");
					}
				}
			}
			/*
			 * Execute transformations, rank transformations and update checksums if skip transformations flag is set to
			 * false
			 */
			if (!skipTransformations)
			{
				// process the ranks
				for (StagingTable st : stlist)
				{
					if (nonPersistantQueryBased.contains(st))
						continue;
					String substep = getSubstep(st, loadGroup);
					try
					{
						if (!statusMap.containsKey(st.Name))
						{
							if (missingOptionalSF.containsKey(st) && missingOptionalSF.get(st).equals(Boolean.TRUE))
							{
								logger.debug("Optional Sourcefile " + st.SourceFile + " is missing, " + "skipping processing the ranks for staging table "
										+ st.Name + ".");
								continue;
							}
							throw new Exception(st.Name + ": previous steps failed, no reason to process the ranks");
						}
						// validate that the table exists
						if (!Database.tableExists(r, dc, Util.replaceWithPhysicalString(st.Name), false))
						{
							throw new Exception(st.Name + " does not exist, no reason to process the ranks");
						}
						// Process any rank transformations
						Map<String, Transformation> rankTransformations = new HashMap<String, Transformation>();
						for (StagingColumn sc : st.Columns)
						{
							if (sc.Transformations != null)
							{
								for (Transformation t : sc.Transformations)
								{
									if (Rank.class.isInstance(t))
										rankTransformations.put(Util.replaceWithNewPhysicalString(sc.Name, dc,r), t);
								}
							}
						}
						if (rankTransformations.size() > 0)
						{
							List<String> keys = new ArrayList<String>();
							// If there's a sequence number - use it
							for (StagingColumn sc : st.Columns)
							{
								if (SequenceNumber.getSequenceNumber(r, st, sc, false) != null)
									keys.add(Util.replaceWithNewPhysicalString(sc.Name, dc,r));
							}
							// Otherwise, join by all natural keys
							if (keys.isEmpty())
								for (String[] level : st.Levels)
								{
									Hierarchy h = r.findDimensionHierarchy(level[0]);
									if (h != null)
									{
										Level l = h.findLevel(level[1]);
										if (l != null)
										{
											// Join by natural keys
											for (StagingColumn sc : st.Columns)
											{
												if (sc.NaturalKey && l.isKey(sc.Name))
												{
													// Make sure we dont include the ones with DateID transformation
													if (sc.Transformations != null && sc.Transformations.length > 0
															&& sc.Transformations[0].getClass().equals(StaticDateID.class))
													{
														continue;
													}
													keys.add(Util.replaceWithNewPhysicalString(sc.Name, dc,r));
												}
											}
										}
									}
								}
							Rank.processRanks(r, dc, Database.getQualifiedTableName(dc, Util.replaceWithPhysicalString(st.Name)), rankTransformations, keys);
						}
					} catch (Exception ex)
					{
						logger.error(ex.getMessage());
						failed = true;
						status.logStatus(step, substep, processingGroup, Status.StatusCode.Failed);
						if (ignoreErrors)
							continue;
						else
							throw new Exception(ex.getMessage());
					}
				}
				for (StagingTable st : getStagingTablesForTransformationAtStage(Transformation.AFTER_RANKS_SET, stlist))
				{
					performTransformation(st, loadGroup, processingGroup, dc, step, status, ignoreErrors, Transformation.AFTER_RANKS_SET);
				}
				// do the checksums after all tables have been loaded and transformed
				for (StagingTable st : stlist)
				{
					// in new version, replace checksums in pre-processing
					if (r.getCurrentBuilderVersion() >= 10)
						continue;
					if (nonPersistantQueryBased.contains(st))
						continue;
					String substep = getSubstep(st, loadGroup);
					try
					{
						if (!statusMap.containsKey(st.Name))
						{
							if (missingOptionalSF.containsKey(st) && missingOptionalSF.get(st).equals(Boolean.TRUE))
							{
								logger.debug("Optional Sourcefile " + st.SourceFile + " is missing, " + "skipping updateing checksums for staging table "
										+ st.Name + ".");
								status.logStatus(step, substep, processingGroup, Status.StatusCode.Complete, 0, 0, 0, 0, "Sourcefile " + st.SourceFile
										+ " is optional and is missing, " + "skipping staging table " + st.Name + " for loadstaging.");
								continue;
							}
							throw new Exception(st.Name + ": previous steps failed, no reason to calculate the checksums");
						}
						// validate that the table exists
						if (!Database.tableExists(r, dc, Util.replaceWithPhysicalString(st.Name), false))
						{
							throw new Exception(st.Name + " does not exist, no reason to process the ranks");
						}
						// validate that the table exists
						if (!Database.tableExists(r, dc, Util.replaceWithPhysicalString(st.Name), false))
						{
							logger.debug(st.Name + " does not exist, no reason to update checksums");
							failed = true;
							status.logStatus(step, substep, processingGroup, Status.StatusCode.Failed);
							continue;
						}
						Database db = new Database();
						Map<DimensionTable, List<StagingColumn>> scmap = st.getStagingColumnMap(r);
						db.updateChecksums(dc, st, scmap,r);
					} catch (Exception ex)
					{
						logger.error(ex.getMessage());
						failed = true;
						status.logStatus(step, substep, processingGroup, Status.StatusCode.Failed);
						if (ignoreErrors)
							continue;
						else
							throw new Exception(ex.getMessage());
					}
				}
				for (StagingTable st : getStagingTablesForTransformationAtStage(Transformation.AFTER_CHECKSUMS_UPDATED, stlist))
				{
					performTransformation(st, loadGroup, processingGroup, dc, step, status, ignoreErrors, Transformation.AFTER_CHECKSUMS_UPDATED);
				}
				// logging status
				for (StagingTable st : stlist)
				{
					if (nonPersistantQueryBased.contains(st))
						continue;
					if (skippedStagingTables.contains(st))
						continue;
					String substep = getSubstep(st, loadGroup);
					try
					{
						ProcessResult pr = rowsProcessed.get(st);
						status.logStatus(step, substep, processingGroup, Status.StatusCode.Complete, pr == null ? 0 : pr.numRowsProcessed, pr == null ? 0 : pr.numErrors,
									pr == null ? 0 : pr.numWarnings, pr == null ? 0 : pr.duration, null);						
					} catch (Exception ex)
					{
						logger.error(ex.getMessage());
						failed = true;
						status.logStatus(step, substep, processingGroup, Status.StatusCode.Failed);
						if (ignoreErrors)
							continue;
						else
							throw new Exception(ex.getMessage());
					}
				}
			} else
			{
				logger.info("Skipped execution of transformations, rank transformations and update checksum because skiptransformation parameter is set.");
			}
			
			if (dc.DBType == DatabaseType.MSSQL2012ColumnStore)
			{
				for (StagingTable st : stlist)
				{
					List<String> colNames = new ArrayList<String>();
					List<String> colTypes = new ArrayList<String>();
					st.getCreateColumns(dc, r, colNames, colTypes, dc.isUnicodeDatabase(), new ArrayList<String>(), new ArrayList<String>(), null, null, null);
					Database.createColumnStoreIndex(dc, Util.replaceWithPhysicalString(st.Name), colNames);
				}
			}
		} catch (Exception ex)
		{
			logger.error(ex.getMessage(),ex);
			status.logStatus(step, loadGroup, processingGroup, Status.StatusCode.Failed, -1, -1, -1, -1, ex.getMessage());
			return false;
		} finally
		{
			if (DatabaseConnection.isDBTypeMemDB(dc.DBType))
			{
				Connection conn;
				try
				{
					conn = dc.ConnectionPool.getConnection();
					Statement stmt = conn.createStatement();
					stmt.executeUpdate("FLUSH");
					logger.debug("Database flushed to disk");
				} catch (SQLException e)
				{
					logger.error(e.getMessage());
				}
			}
		}
		if (stagingTableName == null)
			status.logStatus(step, loadGroup, processingGroup, failed ? Status.StatusCode.Failed : Status.StatusCode.Complete);
		return true;
	}

	private String getSubstep(StagingTable st, String loadGroup)
	{
		return (loadGroup == null ? "" : loadGroup + ": ") + st.Name;
	}

	private List<StagingTable> getStagingTablesForTransformationAtStage(int executionStage, List<StagingTable> stlist)
	{
		List<StagingTable> lstST = new ArrayList<StagingTable>();
		for (StagingTable st : stlist)
			if (st.requiresTransformationAtStage(executionStage))
				lstST.add(st);
		return lstST;
	}

	private void performTransformation(StagingTable st, String loadGroup, String processingGroup, DatabaseConnection dc, Step step, Status status, boolean ignoreErrors,
			int executionStage) throws Exception
	{
		if (st.Disabled)
			return;
		boolean belongs = st.belongsToLoadGroup(loadGroup);
		if (!belongs)
		{
			return;
		}
		String substep = getSubstep(st, loadGroup);
		try
		{
			// validate that the table exists
			if (!Database.tableExists(r, dc, Util.replaceWithPhysicalString(st.Name), false))
			{
				throw new Exception(st.Name + " does not exist, no reason to apply the transformations");
			}
			if (!statusMap.containsKey(st.Name))
			{
				if (missingOptionalSF.containsKey(st) && missingOptionalSF.get(st).equals(Boolean.TRUE))
				{
					logger.debug("Optional Sourcefile " + st.SourceFile + " is missing, " + "skipping transformations for staging table " + st.Name + ".");
					return;
				}
				throw new Exception(st.Name + ": previous steps failed, no reason to perform the transformations");
			}
			if (st.Transformations != null)
			{
				boolean isLogged = false;
				for (Transformation stran : st.Transformations)
				{
					if (stran.ExecuteAfter == executionStage)
					{
						if (!isLogged)
						{
							logger.info("Performing Transformations for Staging Table " + st.Name + " " + stran.getTransformationStage(executionStage) + ".");
							isLogged = true;
						}
						stran.executeTransformation(r, dc, null, null);
					}
				}
			}
		} catch (Exception ex)
		{
			logger.error(ex.getMessage(), ex);
			failed = true;
			status.logStatus(step, substep, processingGroup, Status.StatusCode.Failed);
			if (!ignoreErrors)
				throw new Exception(ex.getMessage());
		}
	}

	public boolean isContinueOnScriptFailure()
	{
		return continueOnScriptFailure;
	}

	public void setContinueOnScriptFailure(boolean continueOnScriptFailure)
	{
		this.continueOnScriptFailure = continueOnScriptFailure;
	}

	public List<StagingTable> getScriptPath()
	{
		return scriptPath;
	}

	public void setScriptPath(List<StagingTable> scriptPath)
	{
		this.scriptPath = scriptPath;
	}
}
