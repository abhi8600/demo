/**
 * $Id: ForeignKeyUpdate.java,v 1.28 2012-11-05 11:47:52 BIRST\mjani Exp $
 *
 * Copyright (C) 2009 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.query.Database;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.DimensionColumn;
import com.successmetricsinc.query.DimensionTable;
import com.successmetricsinc.query.Level;
import com.successmetricsinc.query.LevelKey;
import com.successmetricsinc.query.MeasureColumn;
import com.successmetricsinc.query.MeasureTable;
import com.successmetricsinc.query.MeasureTableGrain;
import com.successmetricsinc.query.Query;
import com.successmetricsinc.query.Database.DatabaseType;
import com.successmetricsinc.util.Util;

class ForeignKeyUpdate
{
	private static Logger logger = Logger.getLogger(ForeignKeyUpdate.class);
	public String logicalTableName;
	public String tableName;
	public List<String> columnNames;
	public boolean hasCoalescedColumns;
	public boolean requiredForSurrogateKeyUpdate;
	public List<String> dependencyTables;
	public List<String> sourceColumnNames;
	public List<String> logicalNames;
	public StringBuilder joinClause;
	public boolean inlineView;
	/*
	 * Columns that save the input parameters of getForeignKeyUpdate so that a second pass can be done if necessary to
	 * include keys needed to join to lower level tables
	 */
	public MeasureTable smt;
	public StagingTable sgrainst;
	public MeasureTable ssourceMeasureTable;
	public Collection<MeasureTableGrain> sgrainSet;
	public Collection<MeasureTableGrain> sbaseGrainSet;

	public static ForeignKeyUpdate getForeignKeyUpdate(Statement stmt, Repository r, DatabaseConnection dc, MeasureTable mt, StagingTable grainst,
			MeasureTable sourceMeasureTable, Collection<MeasureTableGrain> grainSet, Collection<MeasureTableGrain> baseGrainSet, Set<String> loadedSet,
			Set<MeasureTable> foreignKeyMeasureTables, int loadID, Set<MeasureColumn> requiredKeySet, List<ForeignKeyUpdate> fkulist) throws SQLException
	{
		Set<MeasureColumn> mclist = new HashSet<MeasureColumn>();
		Set<MeasureColumn> smclist = new HashSet<MeasureColumn>();
		/*
		 * Identify all columns in this measure table that have not been populated already by another staging table that
		 * matches the grain of the target measure table. Since the staging table is at a higher grain than the measure
		 * table, these columns can be carried down
		 */
		ForeignKeyUpdate fku = null;
		List<String> dependencyTables = new ArrayList<String>();
		List<StagingTable> stagingTablesAtLevel = new ArrayList<StagingTable>();
		for (StagingTable lst : r.getStagingTables())
		{
			if (lst.isAtMatchingGrain(grainSet))
			{
				stagingTablesAtLevel.add(lst);
			}
		}
		for (MeasureColumn smc : sourceMeasureTable.MeasureColumns)
		{
			if (!smc.Key || loadedSet.contains(smc.ColumnName))
				continue;
			for (StagingTable lst : stagingTablesAtLevel)
			{
				for (StagingColumn sc : lst.Columns)
				{
					if (sc.NaturalKey && sc.matchNaturalKeyName(smc.ColumnName, r))
					{
						/*
						 * Make sure it's in the target table
						 */
						for (MeasureColumn tmc : mt.MeasureColumns)
						{
							if (tmc.ColumnName.equals(smc.ColumnName))
							{
								mclist.add(tmc);
								smclist.add(smc);
								break;
							}
						}
					}
				}
			}
		}
		if (mclist.size() > 0)
		{
			String sourcename = Database.getQualifiedTableName(dc, sourceMeasureTable.TableSource.Tables[0].PhysicalName);
			List<LevelKey> levelList = sourceMeasureTable.getLevelKeyList(r, grainSet, baseGrainSet, r.getTimeDefinition(), true);
			Map<String, String> naturalKeysToUpdate = new HashMap<String, String>();
			String ptname = sourceMeasureTable.TableSource.Tables[0].PhysicalName;
			List<String> newlyLoaded = new ArrayList<String>();
			Map<MeasureColumn, String> nkeyCols = new HashMap<MeasureColumn, String>();
			for (MeasureColumn mc : mclist)
			{
				addQualifiedKeyToMap(r, mt, mc, naturalKeysToUpdate, ptname);
				nkeyCols.put(mc, mc.getQualifiedPhysicalFormula(ptname));
				newlyLoaded.add(mc.ColumnName);
			}
			Map<String, List<String>> joinKeys = new HashMap<String, List<String>>();
			List<MeasureColumn> keyList = new ArrayList<MeasureColumn>();
			for (MeasureColumn mc : sourceMeasureTable.MeasureColumns)
			{
				if (mc.Key && !smclist.contains(mc))
				{
					for (LevelKey lk : levelList)
					{
						/*
						 * Make sure entire level key is present
						 */
						boolean hasKey = true;
						for (String keyCol : lk.ColumnNames)
						{
							boolean found = false;
							for (MeasureColumn mc2 : sourceMeasureTable.MeasureColumns)
							{
								if (mc2.ColumnName.equals(lk.Level.Hierarchy.DimensionName + "." + keyCol))
								{
									found = true;
									break;
								}
							}
							if (!found)
							{
								hasKey = false;
								break;
							}
						}
						if (!hasKey)
							continue;
						/*
						 * Make sure the column exists in the staging table to join on
						 */
						StagingColumn grainsc = null;
						boolean isTimeKey = false;
						for (StagingColumn sc2 : grainst.Columns)
						{
							if (mc.ColumnName.equals(lk.Level.Hierarchy.DimensionName + "." + sc2.Name) && sc2.TargetTypes != null && sc2.NaturalKey
									&& sc2.SourceFileColumn != null && !sc2.DataType.startsWith("Date ID: "))
							{
								boolean isColumnAtLevel = false;
								if(lk.ColumnNames != null && lk.ColumnNames.length > 0)
								{
									for(String cname : lk.ColumnNames)
									{
										if(sc2.Name.equals(cname))
										{
											isColumnAtLevel = true;
										}
									}
								}
								if(isColumnAtLevel)
								{
									grainsc = sc2;
									break;
								}
							}
							else if (mc.ColumnName.equals(lk.Level.Hierarchy.DimensionName + "." + sc2.Name) && sc2.TargetTypes != null 
									&& sc2.NaturalKey && sc2.SourceFileColumn == null && !sc2.DataType.startsWith("Date ID: "))
							{
								isTimeKey = true;
								break;
							}
						}
						String targetAlias = "B";
						MeasureColumn grainmc = null;
						if (grainsc == null && (!isTimeKey))
						{
							/*
							 * If the join key column cannot be found, then maybe it's in another foreign key update in
							 * this same update. If so, we can join through that table.
							 */
							for (MeasureTable mt2 : foreignKeyMeasureTables)
							{
								if (mt2.isAtHigherGrain(r, grainSet) == null)
									continue;
								for (MeasureColumn mc2 : mt2.MeasureColumns)
								{
									if (mc2.Key && mc2.ColumnName.equals(mc.ColumnName))
									{
										boolean inlk = false;
										for (String lkcol : lk.ColumnNames)
										{
											if (mc.ColumnName.equals(lk.Level.Hierarchy.DimensionName + "." + lkcol))
											{
												inlk = true;
												break;
											}
										}
										if (inlk)
										{
											grainmc = mc2;
											/*
											 * We don't need to use schema qualifier for targetAlias since we are adding
											 * the measure table to dependency tables. If the table doesn't exist on any
											 * other foreign key updates, this fku instance will be removed anyway by loadwarehouse.
											 * If we use the schema qualifier, in a customer data scenario, it causes column
											 * could not be bound exception.
											 * Example:
											 * LEFT OUTER JOIN <schema_qual>.PARENT_FACT PARENT FACT ON CONDITION
											 * LEFT OUTER JOIN <schema_qual>.THIS_FACT THIS ON <CONDITION THAT INCLUDES PARENT_FACT>
											 * If we use schema qualifier on targetAlias, it will cause the <CONDITION THAT INCLUDES PARENT_FACT>
											 * to include the schema qualifier, which is incorrect sql and would fail.
											 */
											targetAlias = mt2.TableSource.Tables[0].PhysicalName;
											dependencyTables.add(mt2.TableSource.Tables[0].PhysicalName);
											/*
											 * Make a second pass to recreate the parent foreign key update to ensure it
											 * has this key. It's possible that the key was not needed to join to a
											 * higher level table or the staging table and so may not be there if it's
											 * an inline view
											 */
											for (int i = 0; i < fkulist.size(); i++)
											{
												ForeignKeyUpdate pfku = fkulist.get(i);
												// Don't bother if it's not an inline view
												if (!pfku.inlineView)
													continue;
												// If it is, make sure that this key is included in the select list
												if (pfku.tableName.equals(mt2.TableSource.Tables[0].PhysicalName))
												{
													requiredKeySet.add(mc2);													
													ForeignKeyUpdate newfku = ForeignKeyUpdate.getForeignKeyUpdate(stmt, r, dc, pfku.smt, pfku.sgrainst,
															pfku.ssourceMeasureTable, pfku.sgrainSet, pfku.sbaseGrainSet, loadedSet, foreignKeyMeasureTables,
															loadID, requiredKeySet, fkulist);
													//It could not be recreated which means the key should already be part of inline view
													if(newfku == null)
													{
														continue;
													}
													fkulist.set(i, newfku);
													break;
												}
											}
											break;
										}
									}
								}
								if (grainsc != null)
									break;
							}
						}
						/*
						 * Make sure that this key should be used. If we are loading from an incremental snapshot fact
						 * and this key isn't in the table key, then it cannot be relied upon. Need to only use columns
						 * in the table key.
						 */
						if (grainsc != null)
						{
							boolean foundMatchingKeyCol = false;
							for (StagingTable gst : stagingTablesAtLevel)
							{
								if (gst.IncrementalSnapshotFact && (gst.SnapshotDeleteKeys != null) && (gst.SnapshotDeleteKeys.length > 0))
								{
									foundMatchingKeyCol = false;
									for (StagingColumn sc : gst.Columns)
									{
										if (sc.matchNaturalKeyName(mc.ColumnName, r))
										{
											for(String sdk : gst.SnapshotDeleteKeys)
											{
												if(sdk.equals(sc.Name))
												{
													foundMatchingKeyCol = true;
													break;
												}
											}
											if(foundMatchingKeyCol)
											{
												break;
											}
										}
									}
								}
								else
								{
									//Found a staging table that is not an incremental snapshot fact.
									foundMatchingKeyCol = true;
									break;
								}
							}
							if(!foundMatchingKeyCol)
							{
								grainsc = null;
							}
						}
						if (grainsc == null && grainmc == null && requiredKeySet.contains(mc))
						{
							grainmc = mc;
						}
						if (grainsc != null || grainmc != null)
						{
							String a = grainsc != null ? grainsc.getQualifiedPhysicalName(targetAlias, dc ,r) : grainmc.getQualifiedPhysicalFormula(targetAlias);
							String b = mc.getQualifiedPhysicalFormula(ptname);
							if (!a.equals(b) && !keyList.contains(mc))
							{
								List<String> blist = joinKeys.get(a);
								if(blist == null)
								{
									blist = new ArrayList<String>();
									joinKeys.put(a, blist);										
								}
								blist.add(b);
								keyList.add(mc);
							}
						}
					}
				}
			}
			if (joinKeys.size() > 0 && naturalKeysToUpdate.size() > 0)
			{
				loadedSet.addAll(newlyLoaded);
				fku = new ForeignKeyUpdate();
				fku.sbaseGrainSet = baseGrainSet;
				fku.sgrainSet = grainSet;
				fku.sgrainst = grainst;
				fku.smt = mt;
				fku.ssourceMeasureTable = sourceMeasureTable;
				fku.dependencyTables = dependencyTables;
				for (String s : naturalKeysToUpdate.keySet())
				{
					if (fku.columnNames == null)
					{
						fku.logicalNames = new ArrayList<String>();
						fku.joinClause = new StringBuilder();
						fku.columnNames = new ArrayList<String>();
						fku.sourceColumnNames = new ArrayList<String>();
					}
					fku.columnNames.add(s);
					fku.sourceColumnNames.add(naturalKeysToUpdate.get(s));
					for (MeasureColumn mc : nkeyCols.keySet())
					{
						String name = mc.getQualifiedPhysicalFormula(ptname);
						if (name.equals(naturalKeysToUpdate.get(s)))
						{
							fku.logicalNames.add(mc.ColumnName);
							break;
						}
					}
				}
				// Ensure that lookup is distinct, if not, make it distinct to prevent pathological joins
				LevelKey lk = new LevelKey();
				lk.ColumnNames = new String[keyList.size()];
				int count = 0;
				for (MeasureColumn mc : keyList)
				{
					lk.ColumnNames[count++] = mc.ColumnName;
				}
				boolean transactional = false;
				boolean incrementalSnapshot = false;
				for (StagingTable gst : stagingTablesAtLevel)
				{
					if (gst.IncrementalSnapshotFact)
						incrementalSnapshot = true;
					if (gst.Transactional)
						transactional = true;
				}
				boolean isSnapshot = !transactional && !incrementalSnapshot;
				boolean distinct = getDistinct(dc, stmt, sourceMeasureTable, lk, loadID, isSnapshot);
				fku.tableName = sourceMeasureTable.TableSource.Tables[0].PhysicalName;
				fku.logicalTableName = sourceMeasureTable.TableName;
				fku.joinClause.append(" LEFT OUTER JOIN "+ (DatabaseConnection.isDBTypeMemDB(dc.DBType) ? "MAXONE " : ""));
				if (distinct)
					fku.joinClause.append(sourcename);
				else
				{
					Set<String> joinColumns = new HashSet<String>();
					fku.joinClause.append("(SELECT ");
					boolean first = true;
					for (MeasureColumn mc : keyList)
					{
						if (first)
							first = false;
						else
							fku.joinClause.append(',');
						joinColumns.add(mc.getQualifiedPhysicalFormula("F"));
						fku.joinClause.append(mc.getQualifiedPhysicalFormula("F"));
					}
					for (MeasureColumn mc : nkeyCols.keySet())
					{
						if (first)
							first = false;
						else
							fku.joinClause.append(',');
						String cs = mc.getQualifiedPhysicalFormula("F");
						String cs2 = mc.getQualifiedPhysicalFormula(null);
						fku.joinClause.append("MAX(" + cs + ") " + cs2);
						joinColumns.add(cs);
					}
					DimensionTable degenDimTable = null;
					for(MeasureTableGrain mtg : grainSet)
					{
						Level l = mtg.getLevel(r);
						if((l != null) && (l.Degenerate))
						{
							degenDimTable = r.findDimensionTableAtLevel(mtg.DimensionName, mtg.DimensionLevel);
						}
					}

					/**
					 * If the source measure table is also a degenerate dimension table, we need to include key columns from dimension
					 * table so that if at later stage in load warehouse process, a surrogate key from degenerate dimension table is
					 * with this foreign key update, the key columns from dimension table are available for inclusion in projection list. 
					 */
					if(degenDimTable != null)
					{
						String degenPhysicalName = degenDimTable.TableSource.Tables[0].PhysicalName;
						String sourceMeasureTablePhysicalName = sourceMeasureTable.TableSource.Tables[0].PhysicalName;
						if(degenPhysicalName != null && sourceMeasureTablePhysicalName != null && degenPhysicalName.equals(sourceMeasureTablePhysicalName))
						{
							for(DimensionColumn ddc : degenDimTable.DimensionColumns)
							{
								String cs = ddc.getQualifiedPhysicalFormula("F");
								if((ddc.Key) && (!joinColumns.contains(cs)))
								{
									fku.joinClause.append(", MAX(" + cs + ") " + ddc.getQualifiedPhysicalFormula(null));
									joinColumns.add(cs);
								}
							}
						}
					}
					fku.joinClause.append(",MAX(LOAD_ID) LOAD_ID");
					fku.joinClause.append(" FROM " + Database.getQualifiedTableName(dc, ptname) + " F GROUP BY ");
					fku.inlineView = true;
					first = true;
					for (MeasureColumn mc : keyList)
					{
						if (first)
							first = false;
						else
							fku.joinClause.append(',');
						fku.joinClause.append(mc.getQualifiedPhysicalFormula("F"));
					}
					fku.joinClause.append(')');
				}
				fku.joinClause.append(" " + ptname + " ON ");

				if (distinct && DatabaseConnection.isDBTypeInfoBright(dc.DBType) && r.getCurrentBuilderVersion() >= Repository.JoinCompoundColumnBuilderVersion 
						&& joinKeys.size() > 1	&& !isMultipleTableQualifier(joinKeys))
				{
					/*
					 * If this is Infobright and there are more than one column to join by, create compound columns if
					 * necessary. Joining on two or more columns in IB is very slow.
					 */
					fku.joinClause.append(createCompoundKeyColumns(r, dc, stmt, joinKeys, grainst, sourceMeasureTable));
				}
				else
				{
					boolean firstKey = true;
					for (String s : joinKeys.keySet())
					{
						if (firstKey)
							firstKey = false;
						else
							fku.joinClause.append(" AND ");
						List<String> joinValues = joinKeys.get(s);
						boolean firstJV = true;
						for(String jv : joinValues)
						{
							if(firstJV)
							{
								firstJV = false;
							}
							else
							{
								fku.joinClause.append(" AND ");
							}
							fku.joinClause.append(s + "=" + jv);
						}
					}
				}
				/*
				 * Add a load ID filter if a snapshot fact
				 */
				if (isSnapshot)
				{
					fku.joinClause.append(" AND " + sourceMeasureTable.TableSource.Tables[0].PhysicalName + ".LOAD_ID=" + loadID);
				}
				foreignKeyMeasureTables.add(sourceMeasureTable);
			}
		}
		return (fku);
	}

	/**
	 * It checks in the keySet whether multiple table qualifier exists.
	 * Ex, 
	 * KeySet = a.x, a.y, b.z
	 * Here, qualifiers=a,b (string before last '.')
	 * @param joinKeys 
	 * @return 
	 * 		- True, if multiple table qualifier exists in keySet 
	 * 		- False, otherwise
	 */
	private static boolean isMultipleTableQualifier(Map<String, List<String>> joinKeys) {
		boolean isMultipleTableQualifier = false;		
		Set<String> qualifiers = new HashSet<String>();
		for (String s : joinKeys.keySet())
		{
			int index = s.lastIndexOf(".");
			if (index > 0)
			{
				String qualifier = s.substring(0, index);
				qualifiers.add(qualifier);
				if(qualifiers.size() > 1)
				{
					isMultipleTableQualifier = true;
					break;
				}
			}						
		}	
		return isMultipleTableQualifier;
	}

	/**
	 * If this is Infobright and there are more than one column to join by, create compound columns if necessary.
	 * Joining on two or more columns in IB is very slow.
	 * 
	 * @param r
	 * @param dc
	 * @param stmt
	 * @param joinKeys
	 * @param grainst
	 * @param sourceMeasureTable
	 * @return
	 * @throws SQLException
	 */
	private static String createCompoundKeyColumns(Repository r, DatabaseConnection dc, Statement stmt, Map<String, List<String>> joinKeys, StagingTable grainst,
			MeasureTable sourceMeasureTable) throws SQLException
	{
		// First, create physical key column name
		Map<String, String> keys = new TreeMap<String, String>();
		int width = 0;
		String sourceQualifier = "";
		String stagingQualifier = "";
		for (String s : joinKeys.keySet())
		{
			List<String> sourceKeyList = joinKeys.get(s);
			if(sourceKeyList != null && sourceKeyList.size() > 0)
			{
				for(String sourcekey : sourceKeyList)
				{
					int index = sourcekey.lastIndexOf(".");
					if (index > 0)
					{
						sourceQualifier = sourcekey.substring(0, index);
						sourcekey = sourcekey.substring(index + 1);
					}
					for (StagingColumn sc : grainst.Columns)
					{
						if (sc.getQualifiedPhysicalName("B", dc).equals(s))
						{
							width += getVarcharWidth(sc.DataType, sc.Width);
							break;
						}
					}
					String stagingkey = s;
					index = stagingkey.lastIndexOf(".");
					if (index > 0)
					{
						stagingQualifier = stagingkey.substring(0, index);
						stagingkey = stagingkey.substring(index + 1);
					}
					keys.put(sourcekey, stagingkey);
				}
			}
		}
		String skeyName = "";
		String sformula = "";
		String stagingkeyName = "";
		String stagingFormula = "";
		for (String s : keys.keySet())
		{
			skeyName += s;
			stagingkeyName += keys.get(s);
			String f = "CAST(" + s + " AS " + dc.getCastableCharType() + ")";
			if (sformula.length() > 0)
				sformula = "CONCAT(" + f + "," + sformula + ")";
			else
				sformula += f;
			f = "CAST(" + keys.get(s) + " AS " + dc.getCastableCharType() + ")";
			if (stagingFormula.length() > 0)
				stagingFormula = "CONCAT(" + f + "," + stagingFormula + ")";
			else
				stagingFormula += f;
		}
		List<Object[]> sourceColumns = Database
				.getTableSchema(dc, stmt.getConnection(), null, dc.Schema, sourceMeasureTable.TableSource.Tables[0].PhysicalName);
		List<Object[]> stagingColumns = Database.getTableSchema(dc, stmt.getConnection(), null, dc.Schema, grainst.Name);
		
		skeyName = getHashIfLenExcdMaxLen(skeyName, dc);		
		// Add column to source measure table if needed
		boolean found = false;
		for (Object[] column : sourceColumns)
		{
			if (skeyName.equals(column[0]))
			{
				found = true;
				break;
			}
		}
		if (!found)
		{
			// Add the column if needed
			List<String> columnNames = new ArrayList<String>();
			List<String> columnTypes = new ArrayList<String>();
			List<Integer> actions = new ArrayList<Integer>();
			columnNames.add(skeyName);
			columnTypes.add(dc.getVarcharType(width, false));
			actions.add(Database.ALTER_ADD);
			Database.alterTable(r, dc, sourceMeasureTable.TableSource.Tables[0].PhysicalName, columnNames, columnTypes, null, actions);
		}
		// Update column with proper values
		StringBuilder updateQuery = new StringBuilder("UPDATE " + Database.getQualifiedTableName(dc, sourceMeasureTable.TableSource.Tables[0].PhysicalName)
				+ " SET " + skeyName + "=" + sformula + " WHERE " + skeyName + " IS NULL");
		logger.debug(Query.getPrettyQuery(updateQuery.toString()));
		stmt.executeUpdate(updateQuery.toString());
				
		stagingkeyName = getHashIfLenExcdMaxLen(stagingkeyName, dc);
		// Add column to staging table if needed
		found = false;
		for (Object[] column : stagingColumns)
		{
			if (stagingkeyName.equals(column[0]))
			{
				found = true;
				break;
			}
		}
		if (!found)
		{
			// Add the column if needed
			List<String> columnNames = new ArrayList<String>();
			List<String> columnTypes = new ArrayList<String>();
			List<Integer> actions = new ArrayList<Integer>();
			columnNames.add(stagingkeyName);
			columnTypes.add(dc.getVarcharType(width, false));
			actions.add(Database.ALTER_ADD);
			Database.alterTable(r, dc, grainst.Name, columnNames, columnTypes, null, actions);
		}
		// Update staging table if needed
		updateQuery = new StringBuilder("UPDATE " + Database.getQualifiedTableName(dc, grainst.Name) + " SET " + stagingkeyName + "=" + stagingFormula
				+ " WHERE " + stagingkeyName + " IS NULL");
		logger.debug(Query.getPrettyQuery(updateQuery.toString()));
		stmt.executeUpdate(updateQuery.toString());
		return stagingQualifier + "." + stagingkeyName + "=" + sourceQualifier + "." + skeyName;
	}

	/**
	 * It returns hash of given columnName if it exceeds max length supported by given database.
	 *   
	 * @param columnName - column name
	 * @param dc - database connection
	 * @param r - repository
	 * @return - HashCode of columnName/ ColumnName itself 
	 */
	private static String getHashIfLenExcdMaxLen(String columnName,	DatabaseConnection dc) {		
		if(columnName.length() >= dc.getIdentifierLength(dc.DBType)){				
			return dc.getHashedIdentifier(columnName);
		}
		
		return columnName;
	}

	private static int getVarcharWidth(String datatype, int width)
	{
		if (datatype.equals("Varchar"))
			return width;
		return 20;
	}
	
	/**
	 * Probe to see if a distinct is necessary. Distinct may be needed if the staging table has duplicates at this level
	 * (i.e. this staging table may be at a lower level than this set of columns which may have been denormalized into
	 * this table and therefore repeat
	 * 
	 * @param stmt
	 * @param st
	 * @param dt
	 * @param lk
	 * @return
	 * @throws SQLException
	 */
	private static boolean getDistinct(DatabaseConnection dconn, Statement stmt, MeasureTable mt, LevelKey lk, int loadID, boolean isSnapshot)
			throws SQLException
	{
		if (DatabaseConnection.isDBTypeMemDB(dconn.DBType))
			return true;
		StringBuilder distinctq = new StringBuilder("SELECT ");
		if (dconn.supportsRowNum())
			distinctq.append("* FROM (SELECT ");
		else if (!dconn.supportsLimit() && !dconn.supportsRowNum())
			distinctq.append("TOP 1 ");
		boolean distfirst = true;
		StringBuilder distinctcols = new StringBuilder();
		for (String s : lk.ColumnNames)
		{
			if (distfirst)
				distfirst = false;
			else
				distinctcols.append(',');
			MeasureColumn mc = mt.findColumn(s);
			distinctcols.append(mc.getQualifiedPhysicalFormula("B"));
		}
		distinctq.append(distinctcols + ",COUNT(*) FROM "
				+ Database.getQualifiedTableName(dconn, Util.replaceWithPhysicalString(mt.TableSource.Tables[0].PhysicalName)) + " B");
		if (isSnapshot)
			distinctq.append(" WHERE LOAD_ID=" + loadID);
		distinctq.append(" GROUP BY " + distinctcols + " HAVING COUNT(*)>1");
		if (dconn.supportsLimit())
			distinctq.append(" LIMIT 1 ");
		if (dconn.supportsRowNum())
		{
			distinctq.append(") WHERE ROWNUM = 1");
		}
		logger.info("Probing staging table " + mt.TableName + " for DISTINCT for level key: " + Arrays.toString(lk.ColumnNames));
		logger.debug(Query.getPrettyQuery(distinctq));
		ResultSet rs = null;
		boolean distinct = true;
		try
		{
			rs = stmt.executeQuery(distinctq.toString());
			if (rs.next())
			{	
				logger.info("Not distinct");
				distinct = false;
			}
		}
		finally
		{
			if (rs != null)
				rs.close();
		}
		return distinct;
	}

	public boolean isMeasureTableContainsColumns(Repository r, List<String> physicalColNames)
	{
		if(r == null || physicalColNames == null || physicalColNames.size() == 0)
		{
			return false;
		}
		MeasureTable fkmt = r.findMeasureTableName(logicalTableName);
		if(fkmt == null || fkmt.MeasureColumns == null || fkmt.MeasureColumns.length == 0)
		{
			return false;
		}
		
		for(String physicalName : physicalColNames)
		{
			MeasureColumn mc = fkmt.findColumnPhysical(physicalName);
			if(mc == null)
			{
				return false;
			}
		}
		return true;
	}
	
	public static void addQualifiedKeyToMap(Repository r, MeasureTable mt, MeasureColumn fmc, Map<String, String> nkeys, String alias)
	{
		if (fmc != null)
		{
			if (fmc.Qualify)
			{
				int index = fmc.PhysicalName.indexOf('.');
				if (index > 0)
					nkeys.put(fmc.PhysicalName.substring(index + 1), fmc.getQualifiedPhysicalFormula(alias));
				else
					nkeys.put(fmc.PhysicalName, fmc.getQualifiedPhysicalFormula(alias));
			} else
			{
				DatabaseConnection dc = r.getDefaultConnection();
				nkeys.put(Util.replaceWithNewPhysicalString(fmc.ColumnName, dc,r), fmc.getQualifiedPhysicalFormula(alias));
			}
		}
	}
}