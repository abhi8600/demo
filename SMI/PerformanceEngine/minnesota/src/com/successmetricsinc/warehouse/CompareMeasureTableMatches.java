/**
 * 
 */
package com.successmetricsinc.warehouse;

import java.util.Collection;
import java.util.List;

import com.successmetricsinc.query.MeasureTable;
import com.successmetricsinc.query.MeasureTableGrain;

class CompareMeasureTableMatches implements Comparable<CompareMeasureTableMatches>
{
	public MeasureTable mt;
	public int grainCount;
	public Collection<MeasureTableGrain> higherGrain;
	/**
	 * 
	 */
	private List<MeasureTable> mtList;

	/**
	 * @param loadWarehouse
	 */
	CompareMeasureTableMatches(List<MeasureTable> mtList)
	{
		this.mtList = mtList;
	}

	public int compareTo(CompareMeasureTableMatches o)
	{
		if (grainCount == o.grainCount)
			return (mtList.indexOf(mt) - mtList.indexOf(o.mt));
		return (o.grainCount - grainCount);
	}
}