/**
 * $Id: LoadWarehouse.java,v 1.402 2012-11-12 14:47:08 BIRST\mjani Exp $
 *
 * Copyright (C) 2007-2009 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;

import com.amazonaws.auth.AWSCredentials;
import com.successmetricsinc.ExecuteSteps.Step;
import com.successmetricsinc.ExecuteSteps.StepCommand;
import com.successmetricsinc.ExecuteSteps.StepCommandType;
import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.query.Database;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.Level;
import com.successmetricsinc.query.MeasureColumn;
import com.successmetricsinc.query.MeasureTable;
import com.successmetricsinc.query.Query;
import com.successmetricsinc.status.Status;
import com.successmetricsinc.util.Util;

/**
 * @author Brad Peters
 * 
 */
public class LoadWarehouse
{
	private static Logger logger = Logger.getLogger(LoadWarehouse.class);
	Repository r;
	DatabaseConnection dconn;
	List<StagingTable> stList = new ArrayList<StagingTable>();
	Map<String, String> tempIndices = new HashMap<String, String>();
	Step step;
	Session s;
	private Calendar scdStartDate;
	private Calendar scdEndDate;
	// Maximum width of an index key (SQL Server limitation) - in unicode characters (2 bytes each)
	public static final int MAX_INDEX_KEY_LENGTH = 450;
	// Lower bound of source file size to use index optimization
	public static final int INDEX_OPTIMIZATION_ROW_LIMIT = 250000;
	// Number of rows to decide to drop indexes before inserts
	public static final long DROP_INDEX_LIMIT = 5000000;
	public static final int FLOAT_WIDTH = 10; // width of varchar to convert float to
	private static boolean USE_INNER_JOIN_FOR_INFOBRIGHT_MEASURES = false;

	public LoadWarehouse(Repository r)
	{
		this.r = r;
		step = new Step(StepCommand.LoadWarehouse, StepCommandType.ETL, null);
	}

	public void load(int loadID, Status status, String loadGroup, String[] subGroups, boolean noFactIndices, boolean keepFacts,  boolean onlymodifiedsources, String databasePath, AWSCredentials s3credentials,Repository r) throws Exception
	{
		String processingGroup = (subGroups != null && subGroups.length > 0) ? subGroups[0] : null;
		// set of temporary tables used for loading data
		Set<String> ttnames = new HashSet<String>();
		dconn = r.getDefaultConnection();
		try
		{
			if (status.isComplete(StepCommand.LoadWarehouse, loadGroup, processingGroup, true, Util.getKillFileName(r.getRepositoryRootPath(), Util.KILL_PROCESS_FILE)))
			{
				logger.info("Warehouse already loaded successfully for load id =" + status.getIteration()
						+ (loadGroup == null ? "" : (", for load group " + loadGroup))
						+ (processingGroup == null ? "" : (", for processingGroup " + processingGroup)) + ". This step will not be repeated.");
				return;
			}
			status.logStatus(step, loadGroup, processingGroup, Status.StatusCode.Running);
			Connection conn = dconn.ConnectionPool.getConnection();
			if (dconn.turnOffAutoCommit())
				conn.setAutoCommit(false);
			Statement stmt = conn.createStatement();
			List<String> unmodifiedSources = null;
			if (onlymodifiedsources)
				unmodifiedSources = status.getUnmodifiedSources(true);
			// Order staging tables (largest to smallest)			
			stList = sortStagingTables(unmodifiedSources,subGroups,loadGroup,stmt,loadID,conn);
			// Create session
			s = new Session();
			// Load dimension tables
			DimensionLoader dl = new DimensionLoader(r, dconn, this, scdStartDate, scdEndDate, step, stList, tempIndices, subGroups, processingGroup);
			dl.loadDimensions(conn, stmt, loadID, status, loadGroup, ttnames, databasePath, s3credentials,r);			
			// Load fact tables
			FactLoader fl = FactLoaderFactory.getFactLoaderInstance(r, dconn, this, step, stList);
			fl.loadFacts(conn, stmt, loadID, status, loadGroup, noFactIndices, keepFacts, processingGroup, databasePath, s3credentials);
			if (dconn.turnOffAutoCommit())
			{
				conn.commit();
				conn.setAutoCommit(true);
			}
			if (tempIndices.size() > 0)
			{
				logger.info("Dropping temporary indices");
				for (String idxname : tempIndices.keySet())
				{
					Database.dropIndex(dconn, conn, idxname, tempIndices.get(idxname));
				}
			}
			status.logStatus(step, loadGroup, processingGroup, Status.StatusCode.Complete);
			r.setLoadWarehouseExecuted(true);
		} catch (SQLException ex)
		{
			logger.error("SQLException during LoadWarehouse load method", ex);
			status.logStatus(step, loadGroup, processingGroup, Status.StatusCode.Failed);
			throw new SQLException("SQLException during LoadWarehouse load method ", ex);
		} finally
		{
			if (DatabaseConnection.isDBTypeMemDB(dconn.DBType))
			{
				Connection conn;
				try
				{
					conn = dconn.ConnectionPool.getConnection();
					Statement stmt = conn.createStatement();
					stmt.executeUpdate("FLUSH");
					logger.debug("Database flushed to disk");
				} catch (SQLException e)
				{
					logger.error(e.getMessage());
				}
			}
			
			if (dconn != null && DatabaseConnection.isDBTypeInfoBright(dconn.DBType) && ttnames.size() > 0) 
			{
				logger.debug("Dropping temporary tables used for loading data."); 
				Database.dropTables(r, dconn, ttnames);  
			}
		}		
	}
				
			
	/**
	 * Deletes are terribly slow in Infobright - so replace with inserts
	 * 
	 * @param stmt
	 * @param conn
	 * @param mtname
	 * @throws SQLException 
	 */
	private void convertInfobrightDeletes(Statement stmt, Connection conn, String mtname, MeasureTable mt, StagingTable st, List<String> cnames,
			List<String> pnames, int loadID, StringBuilder keyfilter, String grainKeyName) throws SQLException
	{
		StringBuilder create = new StringBuilder();
		create = new StringBuilder("CREATE TABLE " + dconn.Schema + ".temp1 (");
		List<Object[]> collist = Database.getTableSchema(dconn, conn, null, dconn.Schema, mtname);
		StringBuilder insert = new StringBuilder("INSERT INTO " + dconn.Schema + ".temp1 SELECT ");
		StringBuilder insertCols = new StringBuilder();
		StringBuilder selectCols = new StringBuilder();
		boolean columnUpdatePresent = false;
		int rows = 0;
		for (int i = 0; i < collist.size(); i++)
		{
			MeasureColumn mc = null;
			for (MeasureColumn smc : mt.MeasureColumns)
			{
				if (smc.Qualify && smc.PhysicalName.equalsIgnoreCase(mtname + "." + collist.get(i)[0]))
				{
					mc = smc;
					break;
				}
			}
			if (i > 0)
			{
				create.append(',');
				insertCols.append(',');
				insert.append(',');
				selectCols.append(",");
			}
			if (mc != null && cnames.contains(mc.PhysicalName))
			{
				String projname = pnames.get(cnames.indexOf(mc.PhysicalName));
				insert.append(projname + " " + collist.get(i)[0]);
				if(projname.startsWith("B."))
				{
					columnUpdatePresent = true;
				}
			} else
			{
				insert.append("A." + collist.get(i)[0]);
			}
			String type = collist.get(i)[2].toString();
			if (type.equalsIgnoreCase("varchar") || type.toLowerCase().contains("varchar"))
			{
				type += ('(' + collist.get(i)[3].toString() + ')');
			}
			create.append(collist.get(i)[0] + " " + type);
			insertCols.append(collist.get(i)[0]);
			selectCols.append("A." + collist.get(i)[0]);
		}
		insert.append(" FROM " + Database.getQualifiedTableName(dconn, mtname));
		insert.append(" A INNER JOIN ");
		if (st.SourceType == StagingTable.SOURCE_QUERY && !st.PersistQuery)
		{
			st.updateStagingTableQuery(r);
			insert.append("(" + st.Query + ")");
		} else
			insert.append(Database.getQualifiedTableName(dconn, Util.replaceWithPhysicalString(st.Name)));
		insert.append(" B ON ");
		insert.append(keyfilter);
		insert.append(" WHERE A.LOAD_ID=" + loadID);
		create.append(")");
		Database.clearCacheForConnection(dconn);
		stmt.executeUpdate("DROP TABLE IF EXISTS " + dconn.Schema + ".temp1");
		logger.debug(Query.getPrettyQuery(create));
		Database.retryExecuteUpdate(dconn, stmt, create.toString());
		logger.debug(Query.getPrettyQuery(insert));
		
		stmt.executeUpdate(insert.toString());
		stmt.executeUpdate("DROP TABLE IF EXISTS " + dconn.Schema + ".temp2");
	
		int width = 255;
		for (int i = 0; i < collist.size(); i++)
		{
			if (collist.get(i)[0].equals(grainKeyName))
				width = Integer.valueOf(collist.get(i)[3].toString());
		}
		Database.clearCacheForConnection(dconn);
		stmt.executeUpdate("DROP TABLE IF EXISTS " + dconn.Schema + ".temp2");
		StringBuilder tempInsert = new StringBuilder("CREATE TABLE " + dconn.Schema + ".temp2 (" + grainKeyName + ' ' + dconn.getVarcharType(width, true) + ")");
		logger.debug(Query.getPrettyQuery(tempInsert));
		Database.retryExecuteUpdate(dconn, stmt, tempInsert.toString());
		tempInsert = new StringBuilder("INSERT INTO " + dconn.Schema + ".temp2 SELECT " + grainKeyName + " FROM " + dconn.Schema + ".temp1");
		logger.debug(Query.getPrettyQuery(tempInsert));
		rows = stmt.executeUpdate(tempInsert.toString());
		logger.debug(rows + " rows inserted");
		
		insert = new StringBuilder("INSERT INTO " + dconn.Schema + ".temp1");
		insert.append(" (" + insertCols + ") SELECT " + selectCols);
		insert.append(" FROM " + Database.getQualifiedTableName(dconn, mtname) + " A");
		StringBuilder insert2 = new StringBuilder(insert);
		insert.append(" LEFT OUTER JOIN " + dconn.Schema + ".temp2 B ON");
		insert.append(" A." + grainKeyName + "=" + "B." + grainKeyName);
		insert.append(" WHERE LOAD_ID=" + loadID + " AND B." + grainKeyName + " IS NULL");
		logger.debug(Query.getPrettyQuery(insert));
		rows = stmt.executeUpdate(insert.toString());
		logger.debug(rows + " rows inserted");
		
		if ((rows > 0) || columnUpdatePresent)
		{
			insert2.append(" WHERE A.LOAD_ID<>" + loadID);
			logger.debug(Query.getPrettyQuery(insert2));
			int rows2 = stmt.executeUpdate(insert2.toString());
			logger.debug(rows2 + " rows inserted");

			String dropTemp2 = "DROP TABLE IF EXISTS " + dconn.Schema + ".temp2";
			logger.debug(Query.getPrettyQuery(dropTemp2));
			stmt.executeUpdate(dropTemp2);
			
			String renameQuery = "RENAME TABLE " + Database.getQualifiedTableName(dconn, mtname) + " TO " + dconn.Schema + ".temp2";
			logger.debug(Query.getPrettyQuery(renameQuery));
			stmt.executeUpdate(renameQuery);

			renameQuery = "RENAME TABLE " + dconn.Schema + ".temp1 TO " + Database.getQualifiedTableName(dconn, mtname);
			logger.debug(Query.getPrettyQuery(renameQuery));
			stmt.executeUpdate(renameQuery);
			
			logger.debug(Query.getPrettyQuery(dropTemp2));
			stmt.executeUpdate(dropTemp2);
		}
		stmt.executeUpdate("DROP TABLE IF EXISTS " + dconn.Schema + ".temp1");
		stmt.executeUpdate("DROP TABLE IF EXISTS " + dconn.Schema + ".temp2");
	}		
				
	private long executeQueryAndReturnCount(Statement stmt,String qry) throws SQLException
	{
	  long rowCount = 0;
    try
    {
      ResultSet rs = stmt.executeQuery(qry);
      if (rs.next())
      {
        rowCount = rs.getLong(1);
      }
    }
    catch (Exception e)
    {
      logger.error("Exception while executing query ",e);
      throw e;
    }
    return rowCount;
	}
		
	
	/* Sort Staging Tables before Loading Dimensions and Facts. Sort by Level if possible then by Grain else by Staging table Size.
	 * @param unmodifiedSources
	 * @param subGroups
	 * @param loadGroup
	 * @param stmt
	 * @param loadID
	 * @param conn
	 * @return
	 * @throws SQLException
	 */
	private ArrayList<StagingTable> sortStagingTables(List<String> unmodifiedSources,String[] subGroups,String loadGroup,Statement stmt, 
			int loadID,Connection conn) throws SQLException
	{
		// Order staging tables (largest to smallest)
		ArrayList<StagingTable> stList = new ArrayList<StagingTable>();
		for (StagingTable st : r.getStagingTables())
		{
			if (st.Disabled)
				continue;
			if (st.ExcludeFromModel)
				continue;
			if (st.Imported) // do not allow tables from the parent to be targeted to the warehouse
				continue;
			if (unmodifiedSources != null && unmodifiedSources.contains(st.Name))
				continue;
			// Don't load if only targeting time
			if (st.Levels == null || st.Levels.length == 0
					|| (st.Levels.length == 1 && r.getTimeDefinition() != null && st.Levels[0][0].equals(r.getTimeDefinition().Name)))
			{
				logger.info("Staging table " + st.Name + " ignored as it only has time in its grain.");
				continue;
			}
			boolean belongs = st.belongsToLoadGroup(loadGroup);
			if (!belongs)
			{
				logger.info("Staging table " + st.Name + " does not belong to load group " + loadGroup
						+ ". The staging table will not be used for loading warehouse.");
				continue;
			}
			belongs = st.belongsToSubGroups(subGroups);
			if (!belongs)
			{
				StringBuilder sb = new StringBuilder();
				for (String s : subGroups)
				{
					if (sb.length() > 0)
						sb.append(',');
					sb.append(s);
				}
				logger.info("Staging table " + st.Name + " does not belong to sub group from list [" + sb
						+ "]. The staging table will not be used for loading warehouse.");
				continue;
			}
			if (st.Snapshots != null)
			{
				if (!st.Snapshots.isValidSnapshot(st.Name, loadID, dconn, conn))
				{
					logger.info("Staging table " + st.Name + " has a snapshot policy that does not apply to this load id(" + loadID
							+ "). The staging table will not be used for loading warehouse.");
					continue;
				}
			}
			st.setSize(dconn, stmt);
			st.setRepository(r);
			stList.add(st);
		}
		try
        {
              Collections.sort(stList, Collections.reverseOrder());
        }
        catch (IllegalArgumentException ex)
        {
        	  int stListSize = stList.size();
        	  System.setProperty("UsingTreeSort", "true");
              logger.warn("Collections.sort() failed. Using TreeSet for sorting.");
              TreeSet<StagingTable> t = new TreeSet<StagingTable>(Collections.reverseOrder());
              t.addAll(stList);
              stList = new ArrayList<StagingTable>(t);
              System.setProperty("UsingTreeSort", "false");
              if(stListSize!=stList.size()){
            	  logger.error("Sorting Not Valid for TreeSet as well. Before =" + stListSize  + " and After =" + stList.size());
            	  throw ex;
              }
            
        }
		return stList;
	}
	
	/**
	 * Update all surrogate keys from a given table
	 * 
	 * @param stmt
	 * @param tableName
	 * @param mappedLevels
	 * @param loadID
	 * @throws SQLException
	 */
	public void updateSurrogateKeys(DatabaseConnection dconn, Connection conn, Statement stmt, String tableName, Map<Level, SurrogateKeyUpdate> mappedLevels,
			int loadID) throws SQLException
	{
		StringBuilder skq = null;
		boolean isOracle = DatabaseConnection.isDBTypeOracle(dconn.DBType);
		if(isOracle)
		{
			for (Level l : mappedLevels.keySet())
			{
				skq = new StringBuilder("UPDATE " + Database.getQualifiedTableName(dconn, tableName) + " " + tableName + " SET ( ");
				StringBuilder existssb = new StringBuilder(" WHERE EXISTS (SELECT 1 ");
				SurrogateKeyUpdate sku = mappedLevels.get(l);
				if(sku.updateColumns != null && sku.updateColumns.size() > 0)
				{
					boolean firstCol = true;
					for(String uc : sku.updateColumns)
					{
						if(!firstCol)
						{
							skq.append(", ");
						}
						skq.append(uc);
						if(firstCol)
						{
							firstCol = false;
						}
					}
				}
				skq.append(") = (SELECT ");
				if(sku.updateSourceColumns != null && sku.updateSourceColumns.size() > 0)
				{
					boolean firstCol = true;
					for(String usc : sku.updateSourceColumns)
					{
						if(!firstCol)
						{
							skq.append(", ");
						}
						skq.append(usc);
						if(firstCol)
						{
							firstCol = false;
						}
					}
				}
				String jc = sku.joinCondition;
				if(sku.leftOuterJoin)
				{
					jc = Util.replaceStr(jc, "=", "+=");
				}
				skq.append(" FROM ");
				skq.append(sku.joinTable);
				skq.append(" WHERE ");
				skq.append(jc);
				if(l.SCDType != 2)
				{
					if(sku.joinCondition.length() > 0)
					{
						skq.append(" AND ");
					}
					skq.append(Database.getQualifiedTableName(dconn, tableName) + ".LOAD_ID=" + loadID);					
				}
				skq.append(" ) ");
				
				existssb.append(" FROM ");
				existssb.append(sku.joinTable);
				existssb.append(" WHERE ");
				existssb.append(jc);
				if(l.SCDType != 2)
				{
					if(sku.joinCondition.length() > 0)
					{
						existssb.append(" AND ");
					}
					existssb.append(Database.getQualifiedTableName(dconn, tableName) + ".LOAD_ID=" + loadID);					
				}
				existssb.append(" ) ");
				
				skq.append(existssb);

				String q = skq.toString();
				long id = System.currentTimeMillis();
				logger.info("[QUERY:" + id + ":" + Query.getPrettyQuery(q) + "]");
				int rows = stmt.executeUpdate(q);
				if (!conn.getAutoCommit())
					conn.commit();
				logger.info("[UPDSKEY:" + id + ":" + tableName + ":" + rows + "]");
			}
			return;
		}

		if (DatabaseConnection.isDBTypeMySQL(dconn.DBType))
		{
			skq = new StringBuilder("UPDATE ");
		} else
		{
			skq = new StringBuilder("UPDATE " + Database.getQualifiedTableName(dconn, tableName) + " SET ");
		}
		/*
		 * Send each update clause
		 */
		for (Level l : mappedLevels.keySet())
		{
			SurrogateKeyUpdate sku = mappedLevels.get(l);
			/*
			 * If the mapped level is a slowly changing dimension, update all surrogate keys for the target table. If
			 * not, just update the ones with current load id
			 */
			String q = null;
			if (DatabaseConnection.isDBTypeMySQL(dconn.DBType))
			{
				q = skq + " " + Database.getQualifiedTableName(dconn, tableName) + sku.joinTable + " " + sku.joinCondition + " SET " + sku.updateClause
						+ ((l.SCDType == 2) ? "" : (" WHERE " + Database.getQualifiedTableName(dconn, tableName) + ".LOAD_ID=" + loadID));
			}else if (DatabaseConnection.isDBTypeParAccel(dconn.DBType))
			{
				StringBuilder wheresb = new StringBuilder((l.SCDType == 2) ? "" : (" WHERE B.LOAD_ID=" + loadID));
				if (wheresb.length() > 0)
					wheresb.append(" AND ");
				else
					wheresb.append(" WHERE ");
				wheresb.append(Util.replaceStr(sku.joinCondition, tableName + ".", "B."));				
				q = skq + sku.updateClause + " FROM " + Database.getQualifiedTableName(dconn, tableName) + " B " 
						+ sku.joinTable + " " + Util.replaceStr(sku.joinCondition, tableName + ".", "B.")
						+ wheresb.toString();
			}else
			{
				q = skq + sku.updateClause + " FROM " + Database.getQualifiedTableName(dconn, tableName) + sku.joinTable + " " + sku.joinCondition
						+ ((l.SCDType == 2) ? "" : (" WHERE " + Database.getQualifiedTableName(dconn, tableName) + ".LOAD_ID=" + loadID));
			}
			long id = System.currentTimeMillis();
			logger.info("[QUERY:" + id + ":" + Query.getPrettyQuery(q) + "]");
			int rows = stmt.executeUpdate(q);
			if (!conn.getAutoCommit())
				conn.commit();
			logger.info("[UPDSKEY:" + id + ":" + tableName + ":" + rows + "]");
		}
	}

	public Calendar getScdStartDate()
	{
		return scdStartDate;
	}

	public void setScdStartDate(Calendar scdStartDate)
	{
		this.scdStartDate = scdStartDate;
	}

	public Calendar getScdEndDate()
	{
		return scdEndDate;
	}

	public void setScdEndDate(Calendar scdEndDate)
	{
		this.scdEndDate = scdEndDate;
	}		
}

