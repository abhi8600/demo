/** 
 * Copyright (C) 2009 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.query.Database;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.DimensionColumn;
import com.successmetricsinc.query.DimensionTable;
import com.successmetricsinc.query.Hierarchy;
import com.successmetricsinc.query.Level;
import com.successmetricsinc.query.LevelKey;
import com.successmetricsinc.query.Query;
import com.successmetricsinc.query.QueryMap;
import com.successmetricsinc.query.Database.DatabaseType;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.warehouse.DimensionLoader.DimensionUpdate;
import com.successmetricsinc.warehouse.DimensionLoader.DimensionUpdateColumn;
import com.successmetricsinc.warehouse.DimensionLoader.DimensionUpdateIndex;
import com.successmetricsinc.warehouse.DimensionLoader.DimensionUpdateJoin;
import com.successmetricsinc.warehouse.DimensionLoader.DistinctResult;
import com.successmetricsinc.warehouse.DimensionLoader.JoinType;
import com.successmetricsinc.warehouse.StagingColumnGrainInfo.NameType;

public class DimensionUpdater {

	private static Logger logger = Logger.getLogger(DimensionUpdater.class);
	private Repository r;
	private DatabaseConnection dconn;
	private List<StagingTable> stList;	
	private List<DistinctResult> distinctResults = new ArrayList<DistinctResult>();
	private Map<String, String> tempIndices;
	private static boolean USE_TEMP_TABLES_FOR_DISTINCT = true;

	
	public DimensionUpdater(Repository r, DatabaseConnection dconn, 
			List<StagingTable> stList, Map<String, String> tempIndices)
	{
		this.r = r;
		this.dconn = dconn;
		this.stList = stList;
		this.tempIndices = tempIndices;
	}
	
	void getDimensionUpdates(Connection conn, List<DimensionUpdate> updateList, DimensionTable dt, Hierarchy h, Level l, Level processLevel,
			Statement stmt, String dtname, int loadID, QueryMap qm, String scdStartDateStr, String scdEndDateStr, Set<String> loadedSet, Boolean firstst, Set<String> ttnames)
			throws SQLException
	{
		boolean isIB = dconn.DBType == DatabaseType.Infobright;
		boolean isBirstDB = dconn.DBType == DatabaseType.MemDB;
		boolean isPADB = DatabaseConnection.isDBTypeParAccel(dconn.DBType);
		boolean isOracle = DatabaseConnection.isDBTypeOracle(dconn.DBType);
		List<StagingTable> stProcessList = new ArrayList<StagingTable>();
		/*
		 * If there is a staging table that is at the level of this dimension table, make it first. You are most likely
		 * to get useful columns out of the table that is at the level you are loading into
		 */
		for (StagingTable st : stList)
		{
			boolean atLevel = false;
			for (String[] level : st.Levels)
			{
				if (level[0].equals(dt.DimensionName) && level[1].equals(dt.Level))
				{
					atLevel = true;
					break;
				}
			}
			if (atLevel)
				stProcessList.add(0, st);
			else
				stProcessList.add(st);
		}
		// For each staging table
		for (StagingTable st : stProcessList)
		{
			/*
			 * If there are source groups, make sure they match
			 */
			if (h.SourceGroups != null)
			{
				StringBuilder sb = new StringBuilder();
				boolean first = true;
				for (String sg : h.SourceGroups)
				{
					if (first)
						first = false;
					else
						sb.append(",");
					sb.append(sg);
				}
				if (st.SourceGroups == null)
				{
					logger.debug("Skipping Staging Table " + st.Name + " for loading dimension " + h.Name + " as it does not belong to SourceGroups " + sb.toString());
					continue;
				}
				boolean found = false;
				for (String hs : h.SourceGroups)
				{
					for (String s : st.SourceGroups)
					{
						if (s.equals(hs))
						{
							found = true;
							break;
						}
					}
					if (found)
						break;
				}
				if (!found)
				{
					logger.debug("Skipping Staging Table " + st.Name + " for loading dimension " + h.Name + " as it does not belong to SourceGroups " + sb.toString());
					continue;
				}
			}
			/*
			 * Ensure that the staging table applies to this dimension table. Make sure that the grain includes the
			 * dimension and is at the level of this dimension table definition or lower (and the level key for this
			 * table is present in the staging table)
			 */
			Level stLevel = st.getDimensionLevel(r, dt.DimensionName);
			if (stLevel == null || stLevel != processLevel)
				continue;
			/*
			 * It's OK to insert if the staging table is below the dimension table level
			 */
			boolean insertOK = l.findLevel(stLevel) != null;
			/*
			 * See if this staging table contains a level key for this dimension table. If so, the staging table can
			 * directly satisfy the dimension table with the correct keys
			 */
			LevelKey lk = st.getNaturalKey(r, dt.DimensionName, dt.Level);
			if (lk == null)
			{
				/*
				 * If not, see if the staging table is at a higher level and has all the keys it needs for that
				 */
				lk = st.getNaturalKey(r, dt.DimensionName, stLevel.Name);
				if (lk == null || stLevel.findLevel(l) == null)
				{
					logger.info("Unable to load dimension table [" + dt.TableName + "] from staging table [" + st.Name
							+ "]: natural keys not available at level " + dt.Level);
					continue;
				}
			}
			/*
			 * Make sure that the keys in both tables are of the same type
			 */
			boolean match = true;
			for (String s : lk.ColumnNames)
			{
				StagingColumn ssc = null;
				for (StagingColumn sc : st.Columns)
				{
					if (sc.Name.equals(s))
					{
						ssc = sc;
						break;
					}
				}
				if (ssc != null)
				{
					for (DimensionColumn dc : dt.DimensionColumns)
					{
						if (ssc.columnNameMatchesDimensionColumn(dc.ColumnName) && !ssc.isMatchingDataType(dc.DataType))
						{
							match = false;
							break;
						}
					}
				}
				if (!match)
				{
					break;
				}
			}
			if (!match)
			{
				logger.info("Unable to load dimension table [" + dt.TableName + "] from staging table [" + st.Name + "]: datatype(s) of level key do not match");
				continue;
			}
			/*
			 * Determine which columns should be loaded into this dimension table from this staging table
			 */
			boolean includeNaturalKeys = dt.isIncludeNaturalKeys(r, st, l, stLevel);
			List<StagingColumn> scList = st.getStagingColumnList(r, l, dt, stLevel, includeNaturalKeys);
			if ((scList == null) || scList.isEmpty())
			{
				logger.info("Unable to load dimension table [" + dt.TableName + "] from staging table [" + st.Name
						+ "]: could not find columns in staging table that map to any dimension columns"
						+ (includeNaturalKeys ? "" : " except for any potential references to natural keys"));
				continue;
			}
			StringBuilder whereForInsert = new StringBuilder();
			// Determine the default level filter
			StagingFilter levelFilter = null;
			if (st.LevelFilters != null)
				levelFilter = getLevelFilter(r, st, h.DimensionName, l.Name);
			levelFilter = levelFilter == null ? StagingFilter.NoFilter : levelFilter;
			if (levelFilter != null && levelFilter != StagingFilter.NoFilter)
			{
				whereForInsert.append(Util.replaceStr(st.getReplacedString(levelFilter.Condition, null), Util.replaceWithPhysicalString(st.Name) + ".", "B."));
			}
			int rows = 0;
			/*
			 * If this is the first staging table for this dimension table for this load, then clear out any old records
			 * that may have been loaded in a previously failed load. This step is unnecessary if this is the first time
			 * this load has been attempted, but, without checking logging to make sure no attempt with this load ID has
			 * been done before, this is the safest thing to do
			 */
			if (firstst && r.getServerParameters().isClearDimensionsOnReload())
			{
				logger.info("Deleting any records " + dt.TableName + " with same load ID");
				String dq = "DELETE FROM " + Database.getQualifiedTableName(dconn, dtname) + " WHERE LOAD_ID=" + loadID;
				long id = System.currentTimeMillis();
				logger.info("[QUERY:" + id + ":" + Query.getPrettyQuery(dq) + "]");
				rows = stmt.executeUpdate(dq);
				if (!conn.getAutoCommit())
					conn.commit();
				firstst = false;
				logger.info("[DELDT:" + id + ":" + rows + "]");
			}
			/*
			 * Probe to see if a distinct is necessary. Distinct may be needed if the staging table has duplicates at
			 * this level (i.e. this staging table may be at a lower level than this set of columns which may have been
			 * denormalized into this table and therefore repeat. If there is a level filter for this insert, then make
			 * the assumption that that filter will ensure distinctness (because distinct has some expense, don't use it
			 * if you don't need to)
			 */
			boolean distinct = whereForInsert.length() > 0 || getDistinct(dconn, stmt, st, dt, lk, qm,r);
			/*
			 * Get a list of update queries for this dimension table from this staging table. There will be one update
			 * for the default level filter case, and one update for every other unique level filter present in the
			 * staging table
			 */
			List<DimensionUpdate> updates = getUpdates(loadID, l, st, dt, levelFilter, scList, scdStartDateStr, scdEndDateStr, loadedSet, lk);
			/*
			 * Process update queries
			 */
			for (DimensionUpdate dku : updates)
				updateList.add(dku);
			/*
			 * If the staging table is at the same level as the dimension table or there is a level filter (which
			 * presumably filters the staging table to the appropriate records in this level), add any level keys from
			 * the staging table that are missing from the dimension table
			 */
			List<String[]> lookupTables = new ArrayList<String[]>();
			List<String> lookupJoins = new ArrayList<String>();
			DimensionUpdate du = new DimensionUpdate();
			List<String> columns = new ArrayList<String>();
			Set<String> logicalnames = new HashSet<String>();
			// For inserts, always include all available natural keys
			scList = st.getStagingColumnList(r, l, dt, stLevel, true);
			List<StagingColumn> aggColumns = new ArrayList<StagingColumn>();
			for (StagingColumn foundsc : scList)
			{
				// Only insert columns who's level filter matches
				StagingColumnGrainInfo scg = foundsc.getGrainInfoForLevel(h.DimensionName, l.Name);
				StagingFilter filter = scg == null ? levelFilter : scg.StagingFilterName == null ? StagingFilter.NoFilter : r
						.getStagingFilter(scg.StagingFilterName);
				if (filter == levelFilter)
				{
					String tableAlias = "B";
					DimensionColumn dc = foundsc.getMatchingDimensionColumn(dt);
					String pname = foundsc.getQualifiedPhysicalFormula(r, tableAlias, false, scg, qm);
					/*
					 * Process any aggregations
					 */
					if (scg != null && scg.Aggregation != null)
					{
						tableAlias = "AGG";
						aggColumns.add(foundsc);
						if (scg.NewName == null || scg.NewName.length() == 0)
						{
							pname = foundsc.getQualifiedPhysicalName(tableAlias, dconn);
						} else
						{
							pname = foundsc.getQualifiedPhysicalName(tableAlias, scg.NewNameType == NameType.Rename ? scg.NewName : scg.NewName + foundsc.Name,
									dconn);
						}
					}
					// Make sure no overflow converting floats to chars
					if (dc != null && dc.DataType.equals("Varchar") && foundsc.DataType.equals("Float"))
					{
						if (DatabaseConnection.isDBTypeMySQL(dconn.DBType))
							pname = "CAST(" + pname + " AS CHAR(" + LoadWarehouse.FLOAT_WIDTH + "))";
						else
							pname = "STR(" + pname + "," + LoadWarehouse.FLOAT_WIDTH + ")";
					}
					lookupTables.addAll(foundsc.getLookupTables(qm, dconn,r));
					lookupJoins.addAll(foundsc.getLookupJoins(st, tableAlias, qm));
					if (r.getTimeDefinition() == null || foundsc.TargetTypes.size() != 1 || !foundsc.TargetTypes.get(0).equals(r.getTimeDefinition().Name))
						logicalnames.add(foundsc.Name);
					String col = dc.PhysicalName != null ? dc.PhysicalName.substring(dc.PhysicalName.lastIndexOf('.') + 1) : Util.replaceWithNewPhysicalString(dc.ColumnName, dconn,r);
					DimensionUpdateColumn duc = new DimensionUpdateColumn(foundsc.Name, col, null);
					du.columns.add(duc);
					if (foundsc.Transformations == null || foundsc.Transformations.length == 0)
					{
						columns.add(foundsc.getQualifiedPhysicalName(null, dconn,r));
					}
					if (!dc.DataType.equals(foundsc.DataType))
					{
						if (!(dc.DataType.equals("Integer") && foundsc.DataType.startsWith("Date ID: ")))
							pname = "CAST(" + pname + " AS " + Database.getDataType(dconn, dc.DataType) + ")";
					}
					/*
					 * If the staging table does not have distinct rows for each level key being inserted into the
					 * dimension table, then we need to correct this. We should not have duplicate keys. To handle this,
					 * we group by the key in the staging table and simply take the MAX of all other columns. This is as
					 * good a rule as any, but clearly not perfect. However, it is fast. If we are loading to a higher
					 * level and there are dupes, correct data would have the dupes all be the same so MAX or MIN would
					 * give you the same result. Only for poorly modeled data (where dupes are likely accidental) might
					 * MIN and MAX give different results - but that's OK. If we don't do this, pathalogical queries can
					 * result later on in both the updates of the dimension table and the surrogate key lookups. This is
					 * because both of these types of queries use this key as the join condition and if the key is not
					 * unique (let's say there's only one value for the entire table) then you can end up with
					 * cross-product queries.
					 */
					duc.expression = pname;
					boolean isLevelKey = false;
					for (String lkCol : lk.ColumnNames)
					{
						if (lkCol.equals(foundsc.Name))
						{
							isLevelKey = true;
						}
					}
					if (isLevelKey)
						duc.levelKey = true;
				}
			}
			/*
			 * Substitute 0 for any checksum columns not loaded (this allows avoiding the null check and an or condition
			 * - which slows updates down)
			 */
			Map<StagingTable, List<StagingColumn>> scmap = dt.getStagingColumnMap(r);
			DimensionUpdateColumn duc = null;
			for (StagingTable ckst : scmap.keySet())
			{
				if (ckst == st)
					continue;
				String ckname = ckst.getChecksumName(false, dconn);
				duc = new DimensionUpdateColumn(null, ckname, "0");
				duc.checksum = true;
				duc.constant = true;
				du.columns.add(duc);
			}
			if (insertOK)
			{
				du.update = false;
				du.lk = lk;
				duc = new DimensionUpdateColumn(null, "LOAD_ID", Integer.toString(loadID));
				duc.constant = true;
				du.columns.add(duc);
				String ckname = "B." + dt.getChecksumColumnName(dconn);
				du.notdistinct = !distinct;
				duc = new DimensionUpdateColumn(null, st.getChecksumName(false, dconn), ckname);
				duc.checksum = true;
				du.columns.add(duc);
				if (isIB && lk.ColumnNames.length > 1 && lk.isCompoundKeyRequired(r))
				{
					duc = new DimensionUpdateColumn(null, lk.getCompoundKeyName(r, dconn), "B." + lk.getCompoundKeyName(r, dconn));
					duc.checksum = false;
					du.columns.add(duc);
				}
				String levelFilterCondition = null;
				if (levelFilter != null && levelFilter != StagingFilter.NoFilter)
				{
					levelFilterCondition = st.getReplacedString(levelFilter.Condition, null);
				}
				du.levelFilterCondition = levelFilterCondition;
				/*
				 * If this is a type II slowly changing dimension, then add the additional start and end date columns
				 */
				if (l.SCDType == 2)
				{
					StagingColumn scdDateColumn = st.getSCDDateColumn();
					if (scdDateColumn != null)
					{
						String scdStartDateExpr = scdDateColumn.getQualifiedPhysicalName("B", dconn);
						if (du.notdistinct)
						{
							scdStartDateExpr = "MAX(" + scdStartDateExpr + ")";
						}
						duc = new DimensionUpdateColumn(null, "SCD_START_DATE", scdStartDateExpr);
					} else
					{
						duc = new DimensionUpdateColumn(null, "SCD_START_DATE", "'" + scdStartDateStr + "'");
					}
					duc.constant = true;
					du.columns.add(duc);
					duc = new DimensionUpdateColumn(null, "SCD_END_DATE", "NULL");
					duc.constant = true;
					du.columns.add(duc);
					if (r.getCurrentBuilderVersion() >= 20)
					{
						List<DimensionColumn> lookupFromRetired = new ArrayList<DimensionColumn>();
						/*
						 * Add lookup to dimension for any retired values not in this update. These are values in the
						 * retired record that came from other sources
						 */
						for(DimensionColumn dc: dt.DimensionColumns)
						{
							boolean found = false;
							for(DimensionUpdateColumn sduc: du.columns)
							{
								if (sduc.columnName != null && sduc.columnName.equals(dc.ColumnName))
								{
									found = true;
									break;
								}
							}
							if (!found && !dc.SurrogateKey && dc.Qualify && dc.PhysicalName.indexOf("SCD_START_DATE") < 0
									&& dc.PhysicalName.indexOf("SCD_END_DATE") < 0)
							{
								lookupFromRetired.add(dc);
							}
						}
						if (lookupFromRetired.size() > 0)
						{
							DimensionUpdateJoin duj = new DimensionUpdateJoin();
							duj.alias = "R";
							duj.tableName =  Database.getQualifiedTableName(dconn, dt.TableSource.Tables[0].PhysicalName);
							duj.type = JoinType.LEFT_OUTER;
							for (String cname : lk.ColumnNames)
							{
								String kcol = Util.replaceWithNewPhysicalString(cname, dconn,r);
								String s = "R." + kcol + "=B." + kcol;
								if (duj.condition == null)
									duj.condition = s;
								else
									duj.condition += " AND " + s;
							}
							if (duj.condition == null)
								duj.condition += " R.TYPEIIRETIRED=1";
							else
								duj.condition += " AND R.TYPEIIRETIRED=1";
							for(DimensionColumn dc: lookupFromRetired)
							{
								String s = dc.PhysicalName;
								int index = s.indexOf('.');
								if (index > 0)
									s = s.substring(index + 1);
								DimensionUpdateColumn lduc = new DimensionUpdateColumn(null, s, "R." + s);
								du.columns.add(lduc);
							}
							// Get checksum columns
							for (StagingTable st2 : scmap.keySet())
							{
								String ckname2 = st2.getChecksumName(false, dconn);
								DimensionUpdateColumn lduc = new DimensionUpdateColumn(null, ckname2, "R." + ckname2);
								du.columns.add(lduc);
							}
							du.joinConditions.add(duj);
						}
					}
				}
				String sttablename = Database.getQualifiedTableName(dconn, Util.replaceWithPhysicalString(st.Name));
				if (st.SourceType == StagingTable.SOURCE_QUERY && !st.PersistQuery)
				{
					st.updateStagingTableQuery(r);
					du.sourceTable = "(" + st.Query + ")";
				} else
					du.sourceTable = sttablename;
				for (int i = 0; i < lookupTables.size(); i++)
				{
					DimensionUpdateJoin duj = new DimensionUpdateJoin();
					duj.type = JoinType.LEFT_OUTER;
					duj.tableName = Database.getQualifiedTableName(dconn, lookupTables.get(i)[0]) + " " + lookupTables.get(i)[1];
					duj.condition = lookupJoins.get(i);
					boolean duplicate = false;
					for (DimensionUpdateJoin duj2 : du.joinConditions)
					{
						if (duj2.tableName.equals(duj.tableName))
						{
							duplicate = true;
							break;
						}
					}
					if (!duplicate)
						du.joinConditions.add(duj);
				}
				// Add subquery for aggregations
				if (aggColumns.size() > 0)
				{
					du.joinConditions.add(getAggregationSubquery(aggColumns, lk, st, h.DimensionName, l.Name, lookupTables, lookupJoins, qm, null));
				}
				StringBuilder filterStr = new StringBuilder();
				StringBuilder nullStr = new StringBuilder();
				StringBuilder unknownStr = new StringBuilder();
				StringBuilder indexQuery = null;
				String idxname = null;
				// Index optimization for SQL Server (only do it for large files)
				if (st.SourceType == StagingTable.SOURCE_FILE && DatabaseConnection.isDBTypeMSSQL(dconn.DBType))
				{
					for (SourceFile sf : r.getSourceFiles())
					{
						if (st.SourceFile != null && st.SourceFile.equals(sf.FileName))
						{
							if (sf.NumRows > LoadWarehouse.INDEX_OPTIMIZATION_ROW_LIMIT)
							{
								idxname = Database.getIndexName(dconn, "IDX_UPD_" + st.Name + "_", dtname, dconn.DBType == DatabaseType.MYSQL
										? Database.MAX_MYSQL_INDEX_NAME_LENGTH : Database.MAX_SQL_SERVER_INDEX_NAME_LENGTH);
								if (Database.indexExists(dconn, st.Name, idxname))
								{
									logger.info("Performance index " + idxname + " for table " + st.Name + " already exists.");
									/*
									 * Put the entry in the temporary indices map so that it gets dropped at the end of
									 * load warehouse
									 */
									tempIndices.put(idxname, sttablename);
								} else
								{
									indexQuery = new StringBuilder("CREATE NONCLUSTERED INDEX " + idxname + " ON " + sttablename + " (");
								}
							}
							break;
						}
					}
				}
				int indexCount = 0;
				for (String s : lk.ColumnNames)
				{
					if (filterStr.length() > 0 && !isIB)
					{
						filterStr.append(" AND ");
					}
					StagingColumn sc = st.findStagingColumn(s);
					String scName = sc.getQualifiedPhysicalFormula(r, "B", qm);
					if (isIB && lk.ColumnNames.length > 1 && lk.isCompoundKeyRequired(r))
					{
						if (filterStr.length() == 0)
							filterStr.append("C." + lk.getCompoundKeyName(r, dconn) + "=B." + lk.getCompoundKeyName(r, dconn));
					} else
					{
						if(isOracle)
						{
							DimensionColumn curdc = dt.findColumn(s);
							if(curdc == null)
							{
								s = Util.replaceWithNewPhysicalString(s, dconn,r);
							}
							else
							{
								s = curdc.PhysicalName;
								if(s.indexOf(".") >= 0)
								{
									s = s.substring(s.indexOf(".")+1);
								}
							}
						}
						else
						{
							s = Util.replaceWithNewPhysicalString(s, dconn,r);
						}
						filterStr.append("C." + s + "=" + scName);
						//SAP Hana gives inconsistent results with empty dimension tables. Putting a not null condition for level key helps fix this
						if(DatabaseConnection.isDBTypeSAPHana(dconn.DBType))
						{
							filterStr.append(" AND " + "C." + s + " IS NOT NULL ");
						}
					}
					if (sc.UnknownValue != null && sc.UnknownValue.length() > 0)
					{
						if (unknownStr.length() > 0)
							unknownStr.append(" AND ");
						if (sc.DataType.equals("Varchar") || sc.DataType.equals("DateTime") || sc.DataType.equals("Date"))
							unknownStr.append(scName + "<>'" + sc.UnknownValue + "'");
						else
							unknownStr.append(scName + "<>" + sc.UnknownValue);
					}
					if (indexQuery != null)
					{
						if (indexCount++ > 0)
							indexQuery.append(',');
						indexQuery.append(s);
						columns.remove(s);
					}
				}
				LevelKey slk = l.getSurrogateKey();
				nullStr.append("C." + Util.replaceWithNewPhysicalString(slk.ColumnNames[0], dconn,r) + " IS NULL");
				if (indexQuery != null && columns.size() > 0)
				{
					indexQuery.append(')');
					indexQuery.append(" INCLUDE (");
					for (int i = 0; i < columns.size(); i++)
					{
						if (i > 0)
							indexQuery.append(',');
						indexQuery.append(columns.get(i));
					}
					indexQuery.append("," + dt.getChecksumColumnName(dconn)
							+ ") WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]");
					logger.info("Creating performance index for table " + st.Name);
					logger.debug(Query.getPrettyQuery(indexQuery));
					stmt.executeUpdate(indexQuery.toString());
					tempIndices.put(idxname, sttablename);
				}
				boolean useTempTablesForDistinct = isIB && USE_TEMP_TABLES_FOR_DISTINCT;
				boolean useWhereNotExists = !isIB && !isBirstDB && !isPADB;
				boolean useLeftOuter = !useTempTablesForDistinct && !useWhereNotExists;
				if (useTempTablesForDistinct)
				{
					// Use a series of temp table joins for Infobright instead of a correlated sub-query
					if (isIB)
					{
						if (unknownStr.length() > 0)
							du.where.append(" " + unknownStr);
						/*
						 * Create a temp table that has all the keys from staging table that do not exist in dimension table. This is done
						 * in 3 different steps below:
						 * 1. tempStgKeys - contains all distinct keys, necessary to do distinct on keys since if we are populating the 
						 * dimension from a fact based data source, there could be multiple rows in the staging table for each value of key.
						 * 2. tempDimKeys - contains all distinct keys from staging table that also exist in dimension table. This temp table
						 * is a subset of the temp table created in step 1.
						 * 3. temp - Using a combination of left outer join and is null condition on the above two temp tables, we figure out
						 * the distinct keys that exist in staging table but not in dimension table. These are the keys that we actually want
						 * to insert into the dimension table.
						 */
						String kc = "";
						String joinc = "";
						String joincDimKeys = "";
						String whereNullKeys = "";
						for (String s : lk.ColumnNames)
						{
							if (kc.length() > 0)
							{
								kc += ",";
								joinc += " AND ";
							}
							String kcol = Util.replaceWithNewPhysicalString(s, dconn,r);
							kc += kcol;
							joinc += "T." + kcol + "=B." + kcol;
							if(joincDimKeys.length()>0)
							{
								joincDimKeys += " AND ";
							}
							joincDimKeys += "A." + kcol + "=B." + kcol;
							if(whereNullKeys.length()>0)
							{
								whereNullKeys += " AND ";
							}
							whereNullKeys += "B." + kcol + " IS NULL";
						}
						if (lk.ColumnNames.length > 1 && st.getCompoundKeys().size() > 0 && lk.isCompoundKeyRequired(r))
						{
							// Use a compound key if necessary
							if (kc.length() > 0)
							{
								kc += ",";
							}
							String ccol = lk.getCompoundKeyName(r, dconn);
							kc += ccol;
							joinc = "T." + ccol + "=B." + ccol;
							joincDimKeys = "A." + ccol + "=B." + ccol;
							whereNullKeys = "B." + ccol + " IS NULL";
						}
						String joincKeysDelta = joincDimKeys;
						du.uniqueTempDropList.add("DROP TABLE IF EXISTS " + dconn.Schema + ".tempStgKeys");
						du.uniqueTempCreateList.add("CREATE TABLE " + dconn.Schema + ".tempStgKeys"  + " " + (dconn.isIBLoadWarehouseUsingUTF8() ? "CHARACTER SET UTF8 " : "") +"AS SELECT DISTINCT " + kc 
								+ " FROM " + du.sourceTable);
						ttnames.add("tempStgKeys");
						
						du.uniqueTempDropList.add("DROP TABLE IF EXISTS " + dconn.Schema + ".tempDimKeys");
						du.uniqueTempCreateList.add("CREATE TABLE " + dconn.Schema + ".tempDimKeys"  + " " + (dconn.isIBLoadWarehouseUsingUTF8() ? "CHARACTER SET UTF8 " : "") +"AS SELECT A.* "  
								+ " FROM " + dconn.Schema + ".tempStgKeys A INNER JOIN " + Database.getQualifiedTableName(dconn, dtname)
								+ " B ON " + joincDimKeys);
						ttnames.add("tempDimKeys");
						
						du.uniqueTempDropList.add("DROP TABLE IF EXISTS " + dconn.Schema + ".temp");
						du.uniqueTempCreateList.add("CREATE TABLE " + dconn.Schema + ".temp" + " " + (dconn.isIBLoadWarehouseUsingUTF8() ? "CHARACTER SET UTF8 " : "") + "AS SELECT A.* "  
								+ " FROM " + dconn.Schema + ".tempStgKeys A LEFT OUTER JOIN " + dconn.Schema + ".tempDimKeys B"
								+ " ON " + joincKeysDelta + " WHERE " + whereNullKeys);
						ttnames.add("temp");
						
						du.uniqueFilter = " INNER JOIN " + dconn.Schema + ".temp T ON " + joinc;
					}
				}
				StagingColumn scdStagingColumn = st.getSCDDateColumn();
				if (useWhereNotExists)
				{
					if (unknownStr.length() > 0)
						du.where.append(" " + unknownStr + " AND ");
					du.where.append("NOT EXISTS (SELECT * FROM " + Database.getQualifiedTableName(dconn, dtname) + " C");
					du.where.append(" WHERE " + filterStr);
					if (l.SCDType == 2)
					{
						du.where.append(" AND "
								+ (scdStagingColumn != null ? scdStagingColumn.getQualifiedPhysicalName("B", dconn) + ">= C.SCD_START_DATE AND ("
										+ scdStagingColumn.getQualifiedPhysicalName("B", dconn) + " <= C.SCD_END_DATE OR C.SCD_END_DATE IS NULL)"
										: "C.SCD_END_DATE IS NULL") + " AND (C." + st.getChecksumName(false, dconn) + "=0 OR C."
								+ st.getChecksumName(false, dconn) + "=B." + dt.getChecksumColumnName(dconn) + ")");
					}
					du.where.append(")");
				} else if (useLeftOuter)
				{
					du.uniqueFilter = " LEFT OUTER JOIN " + (DatabaseConnection.isDBTypeMemDB(dconn.DBType) ? "MAXONE " : "")
							+ Database.getQualifiedTableName(dconn, dtname) + " C ON " + filterStr;
					if (l.SCDType == 2)
					{
						
					du.uniqueFilter += (" AND "
							+ (scdStagingColumn != null ? scdStagingColumn.getQualifiedPhysicalName("B", dconn) + ">= C.SCD_START_DATE AND ("
									+ scdStagingColumn.getQualifiedPhysicalName("B", dconn) + " <= C.SCD_END_DATE OR C.SCD_END_DATE IS NULL)"
										: "C.SCD_END_DATE IS NULL") + " AND (C." + st.getChecksumName(false, dconn) + "=0 OR C."
								+ st.getChecksumName(false, dconn) + "=B." + dt.getChecksumColumnName(dconn) + ")");
						
					}
					if (unknownStr.length() > 0)
						du.where.append(unknownStr + " AND ");
					du.where.append(nullStr);
					if (DatabaseConnection.isDBTypeMemDB(dconn.DBType))
					{
						String distinctKeyFilter = "DISTINCTFORKEY(";
						// Use distinct key filter if BirstDB
						int count = 0;
						for (String s : lk.ColumnNames)
						{
							if (count++ > 0)
							{
								distinctKeyFilter += ',';
							}
							String kcol = Util.replaceWithNewPhysicalString(s, dconn,r);
							distinctKeyFilter += kcol;
						}
						distinctKeyFilter += ")";
						du.distinctKeyFilter = distinctKeyFilter;
					}
				}
				if (whereForInsert.length() > 0)
				{
					du.where.append(du.where.length() > 0 ? " AND " + whereForInsert : whereForInsert);
				}
				du.st = st;
				du.logicalNames = logicalnames;
				du.lookupTables = lookupTables;
				for (DimensionUpdateColumn sduc : du.columns)
				{
					loadedSet.add(sduc.columnName);
				}
				updateList.add(du);
			}
		}
	}
	
	/**
	 * Get all the update queries for a given dimension table from a given staging table. There will be one default
	 * update (based on the default level filter), and one update for each unique level filter present in the staging
	 * table
	 * 
	 * @param loadID
	 * @param l
	 * @param st
	 * @param dt
	 * @param levelFilter
	 * @param scList
	 * @param curDateStr
	 * @param loadedSet
	 * @param lk
	 * @return
	 */
	private List<DimensionUpdate> getUpdates(int loadID, Level l, StagingTable st, DimensionTable dt, StagingFilter levelFilter, List<StagingColumn> scList,
			String scdStartDateStr, String scdEndDateStr, Set<String> loadedSet, LevelKey lk)
	{
		String dtname = dt.TableSource.Tables[0].PhysicalName;
		/*
		 * Determine the default level filter to apply (null if no level filter applies)
		 */
		Hierarchy h = l.getHierarchy();
		/*
		 * Now, determine which level filter overrides exist
		 */
		Map<StagingFilter, List<StagingColumn>> stagingColumnsByLevelFilter = new HashMap<StagingFilter, List<StagingColumn>>();
		// Determine which filter should be applied for each column
		for (StagingColumn sc : scList)
		{
			// Don't update level keys
			boolean isKey = false;
			for (String s : lk.ColumnNames)
			{
				if (st.findStagingColumn(s) == sc)
				{
					isKey = true;
					break;
				}
			}
			if (isKey)
				continue;
			StagingFilter filterOverride = null;
			StagingColumnGrainInfo scg = sc.getGrainInfoForLevel(h.DimensionName, l.Name);
			if (scg != null)
			{
				if (scg.StagingFilterName != null && scg.StagingFilterName.trim().length() > 0)
					filterOverride = r.getStagingFilter(scg.StagingFilterName);
				else if (scg.Aggregation != null)
					filterOverride = StagingFilter.NoFilter;
			}
			StagingFilter currentFilter = (filterOverride == null) ? levelFilter : filterOverride;
			List<StagingColumn> scListForFilter = stagingColumnsByLevelFilter.get(currentFilter);
			if (scListForFilter == null)
			{
				scListForFilter = new ArrayList<StagingColumn>();
				stagingColumnsByLevelFilter.put(currentFilter, scListForFilter);
			}
			scListForFilter.add(sc);
		}
		List<DimensionUpdate> updates = new ArrayList<DimensionUpdate>();
		/*
		 * Generate an update for this table for each filter required
		 */
		for (StagingFilter filter : stagingColumnsByLevelFilter.keySet())
		{
			DimensionUpdate dku = getDimensionUpdate(st, dt, filter, stagingColumnsByLevelFilter, h, l, scdEndDateStr, loadedSet, lk, levelFilter, loadID,
					dtname, false);
			updates.add(dku);
		}
		return (updates);
	}

	
	private DimensionUpdate getDimensionUpdate(StagingTable st, DimensionTable dt, StagingFilter filter,
			Map<StagingFilter, List<StagingColumn>> stagingColumnsByLevelFilter, Hierarchy h, Level l, String scdEndDateStr, Set<String> loadedSet,
			LevelKey lk, StagingFilter levelFilter, int loadID, String dtname, boolean typeIIHistoricalUpdate)
	{
		Set<String> logicalnames = new HashSet<String>();
		List<String[]> lookupTables = new ArrayList<String[]>();
		List<String> lookupJoins = new ArrayList<String>();
		QueryMap qm = new QueryMap();
		if (dconn != null)
			qm.setDBType(dconn.DBType);
		List<StagingColumn> filterScList = stagingColumnsByLevelFilter.get(filter);
		/*
		 * First, update the dimension table for any instances of the level key that exist in both the dimension table
		 * and the staging table
		 */
		DimensionUpdate dku = new DimensionUpdate();
		dku.update = true;
		List<StagingColumn> aggColumns = new ArrayList<StagingColumn>();
		DimensionUpdateColumn duc = null;
		/*
		 * Update load id
		 */
		duc = new DimensionUpdateColumn(null, "LOAD_ID", Integer.toString(loadID));
		duc.constant = true;
		boolean typeII = l.SCDType == 2 && !typeIIHistoricalUpdate;
		if (!typeII || (typeII && r.getCurrentBuilderVersion() < 14))//do not update LOAD_ID if type2 and build version >= 14
			dku.columns.add(duc);
		boolean firstset = true;
		boolean isOracle = DatabaseConnection.isDBTypeOracle(dconn.DBType);
		boolean isPADB = DatabaseConnection.isDBTypeParAccel(dconn.DBType);
		List<String> pnames = new ArrayList<String>();
		if (typeII)
		{
			/*
			 * If it's a type II SCD, just invalidate these records
			 */
			StagingColumn scdDateColumn = st.getSCDDateColumn();
			if (scdDateColumn != null)
			{
				dku.where.append(scdDateColumn.getQualifiedPhysicalName("B", dconn) + ">=" + (isPADB ? "A." : "") + "SCD_START_DATE AND ");
				duc = new DimensionUpdateColumn(null, "SCD_END_DATE", "DATEADD(ms,-2," + scdDateColumn.getQualifiedPhysicalName("B", dconn) + ")");
			} else
			{
				duc = new DimensionUpdateColumn(null, "SCD_END_DATE", "'" + scdEndDateStr + "'");
			}
			duc.constant = true;
			dku.columns.add(duc);
			if (r.getCurrentBuilderVersion() >= 20)
			{
				// Clear and set the retired flag
				dku.typeIIClear = "UPDATE " + Database.getQualifiedTableName(dconn, dtname) + " SET TYPEIIRETIRED=0";
				duc = new DimensionUpdateColumn(null, "TYPEIIRETIRED", "1");
				dku.columns.add(duc);
			}
			if (isPADB)
			{
				dku.where.append("A.SCD_END_DATE IS NULL");				
			}
			else
			{
				dku.where.append("SCD_END_DATE IS NULL");
			}
			dku.scdType = 2;
		} else
		{
			for (StagingColumn foundsc : filterScList)
			{
				String tableAlias = "B";
				/*
				 * Process any aggregations
				 */
				StagingColumnGrainInfo scg = foundsc.getGrainInfoForLevel(h.DimensionName, l.Name);
				String pname = foundsc.getQualifiedPhysicalFormula(r, tableAlias, false, scg, qm);
				if (scg != null && scg.Aggregation != null)
				{
					tableAlias = "AGG";
					aggColumns.add(foundsc);
					if (scg.NewName == null || scg.NewName.length() == 0)
					{
						pname = foundsc.getQualifiedPhysicalName(tableAlias, dconn);
					} else
					{
						pname = foundsc.getQualifiedPhysicalName(tableAlias, scg.NewNameType == NameType.Rename ? scg.NewName : (scg.NewName + foundsc.getPhysicalName()),
								dconn);
					}
				}
				// Process the column
				pname = r.replaceVariables(null, pname);
				lookupTables.addAll(foundsc.getLookupTables(qm, dconn,r));
				lookupJoins.addAll(foundsc.getLookupJoins(st, tableAlias, qm));
				DimensionColumn dc = foundsc.getMatchingDimensionColumn(dt);
				// Make sure no overflow converting floats to chars
				if (dc != null && dc.DataType.equals("Varchar") && foundsc.DataType.equals("Float"))
				{
					if (DatabaseConnection.isDBTypeMySQL(dconn.DBType))
						pname = "CAST(" + pname + " AS CHAR(" + LoadWarehouse.FLOAT_WIDTH + "))";
					else if (DatabaseConnection.isDBTypeMemDB(dconn.DBType))
						pname = "CAST(" + pname + " AS VARCHAR)";
					else
						pname = "STR(" + pname + "," + LoadWarehouse.FLOAT_WIDTH + ")";
				} else if (!dc.DataType.equals(foundsc.DataType))
				{
					if (!(dc.DataType.equals("Integer") && foundsc.DataType.startsWith("Date ID: ")))
						pname = "CAST(" + pname + " AS " + Database.getDataType(dconn, dc.DataType) + ")";
				}
				duc = new DimensionUpdateColumn(dc.ColumnName, dc.PhysicalName != null ? dc.PhysicalName.substring(dc.PhysicalName.lastIndexOf('.') + 1) : Util.replaceWithNewPhysicalString(dc.ColumnName, dconn,r), pname);
				dku.columns.add(duc);
				pnames.add(pname);
				loadedSet.add(dc.ColumnName);
				if (r.getTimeDefinition() == null || foundsc.TargetTypes.size() != 1 || !foundsc.TargetTypes.get(0).equals(r.getTimeDefinition().Name))
					logicalnames.add(foundsc.Name);
				firstset = false;
			}
		}
		if (firstset && !typeII)
			return dku;
		if (filter == levelFilter && !typeII)
		{
			duc = new DimensionUpdateColumn(null, st.getChecksumName(false, dconn), "B." + dt.getChecksumColumnName(dconn));
			duc.checksum = true;
			dku.columns.add(duc);
			pnames.add("B." + dt.getChecksumColumnName(dconn));
		}
		if (st.SourceType == StagingTable.SOURCE_QUERY && !st.PersistQuery)
		{
			st.updateStagingTableQuery(r);
			dku.sourceTable = "(" + st.Query + ")";
		} else
		{
			dku.sourceTable = Database.getQualifiedTableName(dconn, Util.replaceWithPhysicalString(st.Name));
		}
		dku.sourceTableLevel = lk.Level;
		boolean first = true;
		/*
		 * If a lookup is used on a natural key, need to defer the clause until the lookup table is in the query
		 */
		Map<String, String> deferredJoinClauses = new HashMap<String, String>();
		StringBuilder joinCondition = new StringBuilder();
		DimensionUpdateJoin duj = new DimensionUpdateJoin();
		dku.joinConditions.add(duj);
		for (String s : lk.ColumnNames)
		{
			StagingColumn sc = st.findStagingColumn(s);
			if (!filterScList.contains(sc))
			{
				lookupTables.addAll(sc.getLookupTables(qm, dconn,r));
				lookupJoins.addAll(sc.getLookupJoins(st, "B", qm));
			}
			String scName = sc.getQualifiedPhysicalFormula(r, "B", qm);
			if(isOracle || (r.getCurrentBuilderVersion() >= 22))
			{
				//For oracle, find the physical name from dimension table since it can be hashed for more than 26 characters
				DimensionColumn curdc = dt.findColumn(s);
				if(curdc == null)
				{
					s = Util.replaceWithNewPhysicalString(s, dconn, r);
				}
				else
				{
					s = curdc.PhysicalName;
					if(s.indexOf(".") >= 0)
					{
						s = s.substring(s.indexOf(".")+1);
					}
				}
			}
			else
			{
				s = Util.replaceWithNewPhysicalString(s, dconn, r);
			}
			if (sc.Transformations != null && sc.Transformations.length > 0 && TableLookup.class.isInstance(sc.Transformations[0]))
			{
				deferredJoinClauses.put(sc.getLookupTables(qm, dconn,r).get(0)[0], " A." + s + "=" + scName);
				continue;
			}
			dku.targetJoinColumns.add(sc.Name);
			if (dconn.DBType == DatabaseType.Infobright && lk.ColumnNames.length > 1 && lk.isCompoundKeyRequired(r))
			{
				if (joinCondition.length() == 0)
					joinCondition.append(" A." + lk.getCompoundKeyName(r, dconn) + "=B." + lk.getCompoundKeyName(r, dconn));
			} else
			{
				if (first)
					first = false;
				else
					joinCondition.append(" AND");
				joinCondition.append(" A." + s + "=" + scName);
			}
		}
		duj.condition = joinCondition.toString();
		if (filter == levelFilter)
		{
			if (r.getCurrentBuilderVersion() >=20)
			{
				dku.checksumExpression = " A." + st.getChecksumName(false, dconn) + "<>B."
						+ dt.getChecksumColumnName(dconn);
			} else
			{
				dku.checksumExpression = (typeII ? " A." + st.getChecksumName(false, dconn) + "<>0 AND" : "") + " A." + st.getChecksumName(false, dconn) + "<>B."
						+ dt.getChecksumColumnName(dconn);
			}
			// Setup load indices
			LevelKey cklk1 = new LevelKey();
			cklk1.Level = lk.Level;
			DatabaseConnection dconn = r.getDefaultConnection();
			if (dconn.DBType == DatabaseType.MYSQL)
			{
				cklk1.ColumnNames = new String[lk.ColumnNames.length];
				for (int i = 0; i < lk.ColumnNames.length; i++)
					cklk1.ColumnNames[i] = lk.ColumnNames[i];
			} else
			{
				cklk1.ColumnNames = new String[lk.ColumnNames.length + 1];
				for (int i = 0; i < lk.ColumnNames.length; i++)
					cklk1.ColumnNames[i] = lk.ColumnNames[i];
				cklk1.ColumnNames[lk.ColumnNames.length] = st.getChecksumName(true, dconn);
			}
			try
			{
				int width = 0;
				for (String s : cklk1.ColumnNames)
				{
					DimensionColumn dc = null;
					for (DimensionColumn sdc : dt.DimensionColumns)
					{
						if (sdc.ColumnName.equals(s))
						{
							dc = sdc;
							break;
						}
					}
					if (dc != null)
						width += dc.Width;
				}
				if (width < LoadWarehouse.MAX_INDEX_KEY_LENGTH)
				{
					boolean used = false;
					for (String s : lk.ColumnNames)
					{
						if (LoadWarehouseHelper.usedInJoin(dt, s, dconn, r))
						{
							used = true;
							break;
						}
					}
					if (used)
					{
						DimensionUpdateIndex dui = new DimensionUpdateIndex();
						dui.lk = cklk1;
						dui.tableName = dtname;
						dku.indices.add(dui);
					}
				}
			} catch (Exception e)
			{
				logger.error("Failed to create index: " + e);
			}
			if (st.SourceType != StagingTable.SOURCE_QUERY || st.PersistQuery)
			{
				LevelKey cklk2 = new LevelKey();
				cklk2.Level = lk.Level;
				cklk2.ColumnNames = new String[lk.ColumnNames.length + 1];
				for (int i = 0; i < lk.ColumnNames.length; i++)
					cklk2.ColumnNames[i] = lk.ColumnNames[i];
				cklk2.ColumnNames[lk.ColumnNames.length] = dt.getLogicalChecksumColumnName();
				try
				{
					DimensionUpdateIndex dui = new DimensionUpdateIndex();
					dui.lk = cklk2;
					dui.tableName = Util.replaceWithPhysicalString(st.Name);
					dku.indices.add(dui);
				} catch (Exception e)
				{
					logger.error("Failed to create index: " + e);
				}
			}
		} else
		{
			dku.where.append(" AND LOAD_ID=" + loadID);
		}
		// Add subquery for aggregations
		if (aggColumns.size() > 0)
		{
			dku.joinConditions.add(getAggregationSubquery(aggColumns, lk, st, h.DimensionName, l.Name, lookupTables, lookupJoins, qm, deferredJoinClauses));
		}
		// Add any lookups
		if (!typeII)
			for (int i = 0; i < lookupTables.size(); i++)
			{
				/*
				 * If a join clause has been deferred from a lookup table for a natural key, then use an inner join
				 * (otherwise the previous join will not be complete)
				 */
				String lookupAlias = lookupTables.get(i)[1];
				if (deferredJoinClauses.containsKey(lookupTables.get(i)))
				{
					String jc = lookupJoins.get(i) + " AND " + deferredJoinClauses.get(lookupTables.get(i)[0]);
					jc = Util.replaceStr(jc, lookupTables.get(i)[0], lookupAlias);
					duj = new DimensionUpdateJoin();
					duj.type = JoinType.INNER;
					duj.tableName = Database.getQualifiedTableName(dconn, lookupTables.get(i)[0]);
					duj.alias = lookupAlias;
					duj.condition = jc;
					dku.joinConditions.add(duj);
				} else
				{
					String jc = lookupJoins.get(i);
					jc = Util.replaceStr(jc, lookupTables.get(i)[0], lookupAlias);
					duj = new DimensionUpdateJoin();
					duj.type = JoinType.LEFT_OUTER;
					duj.tableName = Database.getQualifiedTableName(dconn, lookupTables.get(i)[0]);
					duj.alias = lookupAlias;
					duj.condition = jc;
					dku.joinConditions.add(duj);
				}
				for (DimensionUpdateColumn lkduc : dku.columns)
				{
					lkduc.expression = Util.replaceStr(lkduc.expression, lookupTables.get(i)[0] + ".", lookupAlias + ".");
				}
			}
		/*
		 * Add any level filters
		 */
		StringBuilder where = new StringBuilder();
		if (filter != null && filter != StagingFilter.NoFilter)
		{
			where.append(" " + Util.replaceStr(st.getReplacedString(filter.Condition, null), Util.replaceWithPhysicalString(st.Name) + ".", "B."));
		}
		if (where.length() > 0)
		{
			dku.where.append((dku.where.length() > 0 ? " AND " : "") + where);
		}
		dku.logicalNames = logicalnames;
		dku.st = st;
		dku.lookupTables = lookupTables;
		return dku;
	}
	
	
	/**
	 * Add updates to a base query - updating higher level keys, surrogate keys, etc.
	 * 
	 * @param updateList
	 * @param du
	 * @param insertColumns
	 * @param expressionColumns
	 * @param insertSet
	 * @param joinCondition
	 * @param dt
	 * @throws SQLException
	 */
	void processUpdates(List<DimensionUpdate> updateList, DimensionUpdate du, StringBuilder insertColumns, StringBuilder expressionColumns,
			Set<String> insertSet, List<DimensionUpdateJoin> joinCondition, DimensionTable dt, boolean notdistinct) throws SQLException
	{
		int count = 0;
		for (DimensionUpdate udu : updateList)
		{
			if (udu.update)
			{
				/*
				 * Don't update if the source is also being used for an insert and this isn't that insert
				 */
				boolean found = false;
				for (DimensionUpdate sdu : updateList)
				{
					if (sdu != udu && !sdu.update && sdu != du && sdu.sourceTable.equals(udu.sourceTable))
						found = true;
				}
				if (found)
					continue;
				/*
				 * Make sure join columns are present
				 */
				for (String s : udu.targetJoinColumns)
				{
					found = false;
					for (StagingColumn sc : du.st.Columns)
					{
						if (sc.Name.equals(s))
						{
							found = true;
							break;
						}
					}
					if (!found)
						break;
				}
				if (!found)
					continue;
				if (!udu.sourceTable.equals(du.sourceTable))
				{
					for (DimensionUpdateIndex dui : udu.indices)
					{
						Database.createIndex(dconn, dui.lk, dui.tableName, false, false, null, null, true,r);
					}
					String updateAlias = "UP" + (count++);
					/*
					 * If the level of the source table is above that of the target, then use the dimension table
					 */
					String sourceTableName = udu.sourceTable;
					boolean replaced = false;
					boolean noJoin = false;
					if (!udu.sourceTableLevel.Name.equals(dt.Level) && udu.sourceTableLevel.findLevel(dt.Level) != null)
					{
						DimensionTable sdt = r.findDimensionTableAtLevel(udu.sourceTableLevel.Hierarchy.DimensionName, udu.sourceTableLevel.Name);
						if (sdt != null && sdt.AutoGenerated && (sdt.InheritTable == null || sdt.InheritTable.length() == 0))
						{
							sourceTableName = sdt.TableSource.Tables[0].PhysicalName;
							replaced = true;
							/*
							 * If adding a join that already exists, prune
							 */
							for (DimensionUpdateJoin duj : joinCondition)
							{
								if (duj.tableName != null && duj.type == JoinType.LEFT_OUTER
										&& duj.tableName.equals(Database.getQualifiedTableName(dconn, sourceTableName)))
								{
									updateAlias = duj.alias != null ? duj.alias : sourceTableName;
									noJoin = true;
									break;
								}
							}
						}
					}
					/*
					 * If adding a join that already exists, prune (remove the join and join condition)
					 */
					if (!noJoin)
					{
						boolean first = true;
						for (DimensionUpdateJoin uduj : udu.joinConditions)
						{
							DimensionUpdateJoin duj = new DimensionUpdateJoin();
							if (first)
							{
								duj.type = JoinType.LEFT_OUTER;
								duj.tableName = sourceTableName.indexOf('.') < 0 ? Database.getQualifiedTableName(dconn, sourceTableName) : sourceTableName;
								duj.alias = updateAlias;
							} else
							{
								duj.type = uduj.type;
								duj.tableName = Util.replaceStr(Util.replaceStr(uduj.tableName, "B.", updateAlias + "."), "A.", "B.");
								duj.alias = uduj.alias;
							}
							if (uduj.condition != null && (!uduj.condition.startsWith(" ")))
							{
								duj.condition = " " + uduj.condition;
							} else
							{
								duj.condition = uduj.condition;
							}
							duj.condition = Util.replaceStr(duj.condition, " B.", " " + updateAlias + ".");
							duj.condition = Util.replaceStr(duj.condition, "=B.", "=" + updateAlias + ".");
							duj.condition = Util.replaceStr(duj.condition, " A.", " B.");
							duj.condition = Util.replaceStr(duj.condition, "=A.", "=B.");
							boolean dupe = false;
							/* Prevent duplicates - from multiple higher level tables that have the same keys */
							for (DimensionUpdateJoin sduj : joinCondition)
							{
								if (sduj.tableName != null && sduj.tableName.equals(duj.tableName))
									dupe = true;
							}
							if (!dupe)
							{
								joinCondition.add(duj);
								if (first)
									first = false;
							}
						}
					} else
					{
						boolean first = true;
						for (DimensionUpdateJoin uduj : udu.joinConditions)
						{
							/*
							 * Always eliminate the first table of a duplicate
							 */
							if (first)
							{
								first = false;
								continue;
							}
							/*
							 * However, if lookups were used in the update, make sure they are not redundant too
							 */
							noJoin = false;
							DimensionUpdateJoin duj = new DimensionUpdateJoin();
							duj.type = uduj.type;
							duj.tableName = Util.replaceStr(Util.replaceStr(uduj.tableName, "B.", updateAlias + "."), "A.", "B.");
							if (uduj.condition != null && (!uduj.condition.startsWith(" ")))
							{
								duj.condition = " " + uduj.condition;
							} else
							{
								duj.condition = uduj.condition;
							}
							duj.condition = Util.replaceStr(duj.condition, " B.", " " + updateAlias + ".");
							duj.condition = Util.replaceStr(duj.condition, "=B.", "=" + updateAlias + ".");
							duj.condition = Util.replaceStr(duj.condition, " A.", " B.");
							duj.condition = Util.replaceStr(duj.condition, "=A.", "=B.");
							for (DimensionUpdateJoin cduj : joinCondition)
							{
								if (cduj.tableName != null && cduj.type == JoinType.LEFT_OUTER && cduj.tableName.equals(uduj.tableName))
								{
									noJoin = true;
									break;
								}
							}
							boolean dupe = false;
							/* Prevent duplicates - from multiple higher level tables that have the same keys */
							for (DimensionUpdateJoin sduj : joinCondition)
							{
								if (sduj.tableName != null && sduj.tableName.equals(duj.tableName))
									dupe = true;
							}
							if (!dupe)
							{
								joinCondition.add(duj);
							}
						}
					}
					String alias = dconn.useWatcomOracleUpdateSyntax() ? "A." : "";
					for (int i = 0; i < udu.columns.size(); i++)
					{
						if (insertSet.contains(udu.columns.get(i).column))
							continue;
						if (expressionColumns.length() > 0)
						{
							if (insertColumns != null)
								insertColumns.append(",");
							expressionColumns.append(",");
						}
						if (insertColumns != null)
						{
							insertColumns.append(udu.columns.get(i).column);
							String expr = notdistinct ? "MAX(" + udu.columns.get(i).expression + ")" : udu.columns.get(i).expression;
							expressionColumns.append(Util.replaceStr(expr, "B.", updateAlias + "."));
						} else
						{
							if (replaced && udu.columns.get(i).column.equals(udu.st.getChecksumName(false, dconn)))
								expressionColumns.append(alias + udu.columns.get(i).column + "=" + updateAlias + "." + udu.columns.get(i).column);
							else
								expressionColumns.append(alias + udu.columns.get(i).column + "="
										+ Util.replaceStr(udu.columns.get(i).expression, "B.", updateAlias + "."));
						}
						insertSet.add(udu.columns.get(i).column);
					}
				}
			}
		}
	}
	
	
	/**
	 * Get the aggregation subquery
	 * 
	 * @param aggColumns
	 * @param lk
	 * @param st
	 * @param dimension
	 * @param levelName
	 * @return
	 */
	private DimensionUpdateJoin getAggregationSubquery(List<StagingColumn> aggColumns, LevelKey lk, StagingTable st, String dimension, String levelName,
			List<String[]> lookupTables, List<String> lookupJoins, QueryMap qm, Map<String, String> deferredJoinClauses)
	{
		StringBuilder keyStr = new StringBuilder();
		StringBuilder wrapKeyStr = new StringBuilder();
		StringBuilder gbStr = new StringBuilder();
		StringBuilder joinStr = new StringBuilder();
		boolean first = true;
		for (String s : lk.ColumnNames)
		{
			String pstr = Util.replaceWithNewPhysicalString(s, dconn,r);
			StagingColumn sc = st.findStagingColumn(s);
			String qualStr = sc.getQualifiedPhysicalFormula(r, "B", qm);
			boolean deferredJoin = false;
			if (deferredJoinClauses != null && sc.Transformations != null && sc.Transformations.length > 0
					&& TableLookup.class.isInstance(sc.Transformations[0]))
			{
				deferredJoinClauses.put((sc.getLookupTables(qm, dconn,r).get(0)[0]), qualStr + "=AGG." + pstr);
				deferredJoin = true;
			}
			if (first)
				first = false;
			else
			{
				keyStr.append(",");
				wrapKeyStr.append(",");
				gbStr.append(",");
			}
			if (joinStr.length() > 0)
				joinStr.append(" AND ");
			gbStr.append(qualStr);
			if (!qualStr.equals(s))
				keyStr.append(qualStr + " " + pstr);
			else
				keyStr.append(qualStr);
			wrapKeyStr.append(pstr);
			if (!deferredJoin)
				joinStr.append(qualStr + "=AGG." + pstr);
		}
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append(keyStr);
		boolean wrap = false;
		Map<StagingColumn, Set<String>> setMap = new HashMap<StagingColumn, Set<String>>();
		for (StagingColumn sc : aggColumns)
		{
			StagingColumnGrainInfo scg = sc.getGrainInfoForLevel(dimension, levelName);
			StagingAggregation sa = scg.Aggregation;
			String scName = sc.getQualifiedPhysicalFormula(r, "B", qm);
			String cname = null;
			if (scg.NewName != null && scg.NewName.length() > 0)
				cname = Util.replaceWithNewPhysicalString(scg.NewNameType == NameType.Rename ? scg.NewName : (scg.NewName + sc.getPhysicalName()), dconn,r);
			else
				cname = Util.replaceWithNewPhysicalString(sc.getPhysicalName(), dconn,r);
			if (sa.Type.equals("Ordered List"))
			{
				sb.append(",MIN(CASE");
				for (int i = 0; i < sa.ValueList.length; i++)
				{
					if (sa.ValueList[i].equalsIgnoreCase("NULL"))
					{
						sb.append(" WHEN " + scName + " IS NULL THEN " + i);
					} else
					{
						sb.append(" WHEN " + scName + "='" + sa.ValueList[i] + "' THEN " + i);
					}
				}
				if (sa.Default != null && sa.Default.length() > 0)
				{
					sb.append(" ELSE " + sa.ValueList.length);
				}
				sb.append(" END) " + cname);
				wrap = true;
			} else if (sa.Type.equals("Map"))
			{
				Set<String> valueSet = new HashSet<String>();
				for (String[] set : sa.SetList)
				{
					for (String s : set)
						valueSet.add(s);
				}
				setMap.put(sc, valueSet);
				for (String s : valueSet)
				{
					if (s.equalsIgnoreCase("NULL"))
					{
						sb.append(",MAX(CASE WHEN " + scName + " IS NULL THEN 1 ELSE 0 END) " + cname + "_" + s);
					} else
					{
						sb.append(",MAX(CASE WHEN " + scName + "='" + s + "' THEN 1 ELSE 0 END) " + cname + "_" + s);
					}
				}
				wrap = true;
			} else
			{
				sb.append("," + sa.Type + "(" + scName + ") " + cname);
			}
		}
		sb.append(" FROM " + Database.getQualifiedTableName(dconn, Util.replaceWithPhysicalString(st.Name)) + " B");
		for (int i = 0; i < lookupTables.size(); i++)
		{
			sb.append(" LEFT OUTER JOIN " + (DatabaseConnection.isDBTypeMemDB(dconn.DBType) ? "MAXONE " : "")
					+ Database.getQualifiedTableName(dconn, lookupTables.get(i)[0]) + " " + lookupTables.get(i)[1] + " ON " + lookupJoins.get(i));
		}
		sb.append(" GROUP BY " + gbStr);
		if (wrap)
		{
			StringBuilder newsb = new StringBuilder();
			newsb.append("SELECT " + wrapKeyStr);
			for (StagingColumn sc : aggColumns)
			{
				StagingColumnGrainInfo scg = sc.getGrainInfoForLevel(dimension, levelName);
				StagingAggregation sa = scg.Aggregation;
				String cname = null;
				if (scg.NewName != null && scg.NewName.length() > 0)
					cname = Util.replaceWithNewPhysicalString(scg.NewNameType == NameType.Rename ? scg.NewName : (scg.NewName + sc.Name), dconn,r);
				else
					cname = Util.replaceWithNewPhysicalString(sc.Name, dconn,r);
				if (sa.Type.equals("Ordered List"))
				{
					newsb.append(",CASE");
					for (int i = 0; i < sa.ValueList.length; i++)
					{
						newsb.append(" WHEN " + cname + "=" + i + " THEN " + Database.getQuotedString(sa.ValueList[i]));
					}
					if (sa.Default != null && sa.Default.length() > 0)
						newsb.append(" ELSE " + Database.getQuotedString(sa.Default));
					newsb.append(" END " + cname);
				} else if (sa.Type.equals("Map"))
				{
					newsb.append(",CASE");
					for (int i = 0; i < sa.SetList.length; i++)
					{
						newsb.append(" WHEN ");
						for (int j = 0; j < sa.SetList[i].length; j++)
						{
							if (j > 0)
								newsb.append(" AND ");
							newsb.append(cname + "_" + sa.SetList[i][j] + ">0");
						}
						newsb.append(" THEN " + Database.getQuotedString(sa.ValueList[i]));
					}
					if (sa.Default != null && sa.Default.length() > 0)
						newsb.append(" ELSE " + Database.getQuotedString(sa.Default));
					newsb.append(" END " + cname);
				} else
					newsb.append("," + cname);
			}
			newsb.append(" FROM (" + sb + ") AGG2");
			sb = newsb;
		}
		DimensionUpdateJoin duj = new DimensionUpdateJoin();
		duj.tableName = "(" + sb + ")";
		duj.alias = "AGG";
		duj.type = JoinType.INNER;
		duj.condition = joinStr.toString();
		return duj;
	}
	
	/**
	 * Probe to see if a distinct is necessary. Distinct may be needed if the staging table has duplicates at this level
	 * (i.e. this staging table may be at a lower level than this set of columns which may have been denormalized into
	 * this table and therefore repeat
	 * 
	 * @param stmt
	 * @param st
	 * @param dt
	 * @param lk
	 * @return
	 * @throws SQLException
	 */

	private boolean getDistinct(DatabaseConnection dconn, Statement stmt, StagingTable st, DimensionTable dt, LevelKey lk, QueryMap qm,Repository r) throws SQLException
	{
		if (DatabaseConnection.isDBTypeMemDB(dconn.DBType))
			return true;
		for (DistinctResult dr : distinctResults)
		{
			if (dr.st == st && dr.lk.ColumnNames.length == lk.ColumnNames.length)
			{
				boolean same = true;
				for (int i = 0; i < lk.ColumnNames.length; i++)
				{
					if (!dr.lk.ColumnNames[i].equals(lk.ColumnNames[i]))
					{
						same = false;
						break;
					}
				}
				if (same)
				{
					return (dr.distinct);
				}
			}
		}
		StringBuilder distinctq = new StringBuilder("SELECT ");
		if (dconn.supportsRowNum())
		{
			distinctq.append("* FROM (SELECT ");
		}
		if (!dconn.supportsLimit() && !dconn.supportsRowNum())
		{
			distinctq.append("TOP 1 ");
		}
		boolean distfirst = true;
		StringBuilder distinctcols = new StringBuilder();
		List<String[]> lookupTables = new ArrayList<String[]>();
		List<String> lookupJoins = new ArrayList<String>();
		StringBuilder distinctFilter = new StringBuilder();
		for (String s : lk.ColumnNames)
		{
			if (distfirst)
				distfirst = false;
			else
				distinctcols.append(',');
			StagingColumn sc = st.findStagingColumn(s);
			lookupTables.addAll(sc.getLookupTables(qm, dconn,r));
			lookupJoins.addAll(sc.getLookupJoins(st, "B", qm));
			distinctcols.append(Util.replaceWithNewPhysicalString(sc.getPhysicalName(), dconn,r));
			if (sc.UnknownValue != null && sc.UnknownValue.length() > 0)
			{
				String val = null;
				if (sc.DataType.equals("Varchar") || sc.DataType.equals("Date") || sc.DataType.equals("DateTime"))
					val = "'" + sc.UnknownValue + "'";
				else
					val = sc.UnknownValue;
				distinctFilter.append((distinctFilter.length() > 0 ? " AND " : "") + Util.replaceWithNewPhysicalString(sc.getPhysicalName(), dconn,r) + "<>" + val);
			}
		}
		distinctq.append(distinctcols + ",COUNT(*) FROM " + Database.getQualifiedTableName(dconn, Util.replaceWithPhysicalString(st.Name)) + " B");
		for (int i = 0; i < lookupTables.size(); i++)
		{
			distinctq.append(" INNER JOIN " + Database.getQualifiedTableName(dconn, lookupTables.get(i)[0]) + " " + lookupTables.get(i)[1] + " ON "
					+ lookupJoins.get(i));
		}
		if (distinctFilter.length() > 0)
			distinctq.append(" WHERE " + distinctFilter);
		distinctq.append(" GROUP BY " + distinctcols);
		distinctq.append(" HAVING COUNT(*)>1");
		if (dconn.supportsLimit())
		{
			distinctq.append(" LIMIT 1");
		}
		if (dconn.supportsRowNum())
		{
			distinctq.append(") WHERE ROWNUM = 1");
		}
		logger.info("Probing staging table " + st.Name + " for DISTINCT" + (dt != null ? " to load " + dt.TableName : ""));
		logger.debug(Query.getPrettyQuery(distinctq));
		boolean distinct = true;
		ResultSet rs = null;
		try
		{
			rs = stmt.executeQuery(distinctq.toString());
			if (rs.next())
			{
				logger.info("Not distinct");
				distinct = false;
			}
		} finally
		{
			if (rs != null)
				rs.close();
		}
		DistinctResult dr = new DistinctResult();
		dr.st = st;
		dr.lk = lk;
		dr.distinct = distinct;
		distinctResults.add(dr);
		return (distinct);
	}

	
	/**
	 * Determine the name of the default level filter for a given staging table
	 * 
	 * @param r
	 * @param st
	 * @param dimension
	 * @param levelName
	 * @return
	 */
	private StagingFilter getLevelFilter(Repository r, StagingTable st, String dimension, String levelName)
	{
		StagingFilter levelFilter = null;
		for (String[] filter : st.LevelFilters)
		{
			if (filter[0].equals(dimension) && filter[1].equals(levelName))
			{
				levelFilter = r.getStagingFilter(filter[2]);
				if (levelFilter == null)
				{
					logger.error("Staging filter " + filter[2] + " referenced in staging table " + st.Name
							+ " not found in repository - filter will be ignored.");
					continue;
				}
				// Found staging filter for the dimension and level
				break;
			}
		}
		return (levelFilter);
	}
}
