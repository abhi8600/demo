/**
 * Copyright (C) 2007-2009 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse;

import java.util.List;

import com.successmetricsinc.Repository;
import com.successmetricsinc.ExecuteSteps.Step;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.Database.DatabaseType;

public class FactLoaderFactory {	
	
	/**
	 * Returns a FactLoader class for Corresponding Database
	 * @param r
	 * @param dconn
	 * @param lw
	 * @param step
	 * @param stList
	 * @return
	 */
	public static FactLoader getFactLoaderInstance(Repository r, DatabaseConnection dconn, LoadWarehouse lw,  Step step, List<StagingTable> stList)
	{
		FactLoader factLoader = null;
		
		if(dconn.DBType ==  DatabaseType.Infobright)
		{
			factLoader = new IBFactLoader(r,dconn,lw,step,stList);
		}
		else if(dconn.DBType ==  DatabaseType.MemDB)
		{
			factLoader = new MemDBFactLoader(r,dconn,lw,step,stList);
		}
		else if(dconn.DBType ==  DatabaseType.Oracle)
		{
			factLoader = new OracleFactLoader(r,dconn,lw,step,stList);
		}
		else if(dconn.DBType ==  DatabaseType.ParAccel)
		{
			factLoader = new PADBFactLoader(r,dconn,lw,step,stList);
		}
		else if(dconn.DBType ==  DatabaseType.Redshift)
		{
			factLoader = new RedshiftFactLoader(r, dconn, lw, step, stList);
		}
		else if(dconn.DBType == DatabaseType.MSSQL || dconn.DBType == DatabaseType.MSSQL2005 || dconn.DBType == DatabaseType.MSSQL2008 || dconn.DBType == DatabaseType.MSSQL2012 || dconn.DBType == DatabaseType.MSSQL2014)
		{
			factLoader = new MSSQLFactLoader(r, dconn, lw, step, stList);
		}
		else
		{
			factLoader = new FactLoader(r,dconn,lw,step,stList);
		}
		return factLoader;
	}

}
