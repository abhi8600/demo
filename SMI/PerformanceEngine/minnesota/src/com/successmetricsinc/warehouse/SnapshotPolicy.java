/**
 * $Id: SnapshotPolicy.java,v 1.8 2011-09-15 16:02:48 ricks Exp $
 *
 * Copyright (C) 2009-2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.warehouse;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jdom.Namespace;

import com.successmetricsinc.Repository;
import com.successmetricsinc.query.Database;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.DimensionTable;
import com.successmetricsinc.query.Hierarchy;
import com.successmetricsinc.query.MeasureTable;
import com.successmetricsinc.query.MeasureTableGrain;
import com.successmetricsinc.query.Query;

/**
 * @author bpeters
 * 
 */
public class SnapshotPolicy implements Serializable
{
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(SnapshotPolicy.class);
	public static final String TABLENAME = "TXN_SNAPSHOT_POLICY";
	public static final int INTERVAL_WEEK = 1;
	public static final int INTERVAL_MONTH = 2;
	public static final int DAY_OF_WEEK_SUNDAY = 1;
	public static final int DAY_OF_WEEK_MONDAY = 2;
	public static final int DAY_OF_WEEK_TUESDAY = 4;
	public static final int DAY_OF_WEEK_WEDNESDAY = 8;
	public static final int DAY_OF_WEEK_THURSDAY = 16;
	public static final int DAY_OF_WEEK_FRIDAY = 32;
	public static final int DAY_OF_WEEK_SATURDAY = 64;
	public boolean CurrentDay;
	public int Type; // 1 - Weekly, 2 - Monthly
	public int DaysOfWeek;
	public int DayOfMonth;
	public boolean firstDayOfMonth;
  public boolean lastDayOfMonth;

	public SnapshotPolicy(Element e, Namespace ns)
	{
		String s = e.getChildText("CurrentDay", ns);
		if (s != null)
			CurrentDay = Boolean.valueOf(s);
		s = e.getChildText("Type", ns);
		if (s != null)
			Type = Integer.valueOf(s);
		s = e.getChildText("DaysOfWeek", ns);
		if (s != null)
			DaysOfWeek = Integer.valueOf(s);
		s = e.getChildText("DayOfMonth", ns);
		if (s != null)
			DayOfMonth = Integer.valueOf(s);
		s = e.getChildText("firstDayOfMonth", ns);
		if (s != null)
			firstDayOfMonth = Boolean.valueOf(s);
		s = e.getChildText("lastDayOfMonth", ns);
		if (s != null)
			lastDayOfMonth = Boolean.valueOf(s);
	}
	
	public Element getSnapshotPolicyElement(String elementName,Namespace ns)
	{
		Element e = new Element(elementName,ns);

		Element child = new Element("CurrentDay",ns);
		child.setText(String.valueOf(CurrentDay));
		e.addContent(child);

		child = new Element("Type",ns);
		child.setText(String.valueOf(Type));
		e.addContent(child);

		child = new Element("DaysOfWeek",ns);
		child.setText(String.valueOf(DaysOfWeek));
		e.addContent(child);

		child = new Element("DayOfMonth",ns);
		child.setText(String.valueOf(DayOfMonth));
		e.addContent(child);

		child = new Element("firstDayOfMonth",ns);
		child.setText(String.valueOf(firstDayOfMonth));
		e.addContent(child);

		child = new Element("lastDayOfMonth",ns);
		child.setText(String.valueOf(lastDayOfMonth));
		e.addContent(child);

		return e;
	}

	public boolean isValidSnapshot(String policyName, int loadID, DatabaseConnection dc, Connection conn)
	{
		String query = "SELECT COUNT(*) FROM " + Database.getQualifiedTableName(dc, TABLENAME) + " WHERE Policy=? AND LoadID=?";		
		PreparedStatement pstmt = null;
		ResultSet res = null;
		try
		{
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, policyName);
			pstmt.setInt(2, loadID);
			logger.debug("Checking snapshot (" + policyName + "," + loadID + "): " + Query.getPrettyQuery(query));
			res = pstmt.executeQuery();
			if (!res.next())
			{
				return false;
			}
			int result = res.getInt(1);
			return result > 0;
		} catch (SQLException e)
		{
			return false;
		}
		finally
		{
			try
			{
			if (res != null)
				res.close();
			if (pstmt != null)
				pstmt.close();
			}
			catch (SQLException e)
			{
				return false;
			}
		}
	}

	public static void enforce(Repository r)
	{
		List<MeasureTable> generatedmTables = new ArrayList<MeasureTable>();
		for (MeasureTable mt : r.MeasureTables)
		{
			if (mt.AutoGenerated && mt.InheritTable == null)
				generatedmTables.add(mt);
		}
		DatabaseConnection dc = r.getDefaultConnection();
		Connection conn = null;
		try
		{
			conn = dc.ConnectionPool.getConnection();
		} catch (SQLException e)
		{
			logger.error("Unable to enforce snapshot polciy", e);
		}
		for (StagingTable st : r.getStagingTables())
		{
			if (st.Snapshots != null)
			{
				for (MeasureTable mt : generatedmTables)
				{
					Collection<MeasureTableGrain> grainSet = mt.getGrainInfo(r);
					if (st.isAtMatchingGrain(grainSet))
					{
						String query = "DELETE FROM " + Database.getQualifiedTableName(dc, mt.TableSource.Tables[0].PhysicalName)
								+ " WHERE LOAD_ID NOT IN (SELECT LOADID FROM " + Database.getQualifiedTableName(dc, TABLENAME) + " WHERE Policy=?)";
						PreparedStatement pstmt = null;
						try
						{
							pstmt = conn.prepareStatement(query);
							pstmt.setString(1, st.Name);
							logger.info("Enforcing snapshot policy (" + st.Name + "): " + Query.getPrettyQuery(query));
							pstmt.executeUpdate();
						} catch (SQLException e)
						{
							logger.error(e, e);
						}
						finally
						{
							try
							{
								if (pstmt != null)
									pstmt.close();
							}
							catch (SQLException e)
							{
								logger.debug(e, e);
							}
						}
					}
				}
			}
		}
		List<DimensionTable> generateddTables = new ArrayList<DimensionTable>();
		for (DimensionTable dt : r.DimensionTables)
		{
			if (dt.AutoGenerated && !dt.Calculated && dt.InheritTable == null)
				generateddTables.add(dt);
		}
		for (StagingTable st : r.getStagingTables())
		{
			if (st.Snapshots != null)
			{
				for (DimensionTable dt : generateddTables)
				{
					boolean isAtLevel = false;
					for (String[] level : st.Levels)
					{
						if (dt.DimensionName.equals(level[0]) && dt.Level.equals(level[1]))
						{
							isAtLevel = true;
							break;
						}
					}
					if (!isAtLevel)
						continue;
					isAtLevel = false;
					for (StagingColumn sc : st.Columns)
					{
						if (sc.TargetTypes != null && sc.TargetTypes.indexOf(dt.DimensionName) >= 0)
						{
							Hierarchy h = r.findDimensionHierarchy(dt.DimensionName);
							if ((h.DimensionKeyLevel != null && h.DimensionKeyLevel.Name != null && h.DimensionKeyLevel.Name.equals(dt.Level)) || h.findLevelColumn(sc.Name) != null)
							{
								isAtLevel = true;
								break;
							}
						}
					}
					if (!isAtLevel)
						continue;
					String query = "DELETE FROM " + Database.getQualifiedTableName(dc, dt.TableSource.Tables[0].PhysicalName)
							+ " WHERE LOAD_ID NOT IN (SELECT LOADID FROM " + Database.getQualifiedTableName(dc, TABLENAME) + " WHERE Policy=?)";
					PreparedStatement pstmt = null;
					try
					{
						pstmt = conn.prepareStatement(query);
						pstmt.setString(1, st.Name);
						logger.info("Enforcing snapshot policy (" + st.Name + "): " + Query.getPrettyQuery(query));
						pstmt.executeUpdate();
					} catch (SQLException e)
					{
						logger.error(e);
					}
					finally
					{
						try
						{
							if (pstmt != null)
								pstmt.close();
						}
						catch (SQLException e)
						{
							logger.debug(e, e);
						}
					}
				}
			}
		}
	}
}
