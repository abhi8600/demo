/**
 * 
 */
package com.successmetricsinc.warehouse;

import java.util.List;

import com.successmetricsinc.query.DimensionColumn;
import com.successmetricsinc.query.DimensionTable;

class SurrogateKeyUpdate
{
	List<String> insertColumns;
	List<String> insertSourceColumns;
	String updateClause;
	String joinTable;
	String joinCondition;
	List<String> naturalKeyColumns;
	List<DimensionColumn> matchingDimensionColumns;
	DimensionTable leveldt;
	//Following data structures are used by Oracle loading only
	List<String> updateColumns;
	List<String> updateSourceColumns;
	boolean leftOuterJoin;
	boolean degenerateInlineJoin;
}