package com.successmetricsinc.messaging;


public class AsynchResult {
	public enum RenderStatus {
		RUNNING, COMPLETED
	}
	

	RenderStatus status;
	
	public AsynchResult(RenderStatus status){
		this.status = status;
	}
	
	public RenderStatus getStatus(){
		return status;
	}
		
}
