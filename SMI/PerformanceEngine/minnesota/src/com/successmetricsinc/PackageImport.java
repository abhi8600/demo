package com.successmetricsinc;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;

import com.successmetricsinc.packages.PObject;
import com.successmetricsinc.packages.PackageDefinition;
import com.successmetricsinc.query.Aggregate;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.DimensionColumn;
import com.successmetricsinc.query.DimensionTable;
import com.successmetricsinc.query.Hierarchy;
import com.successmetricsinc.query.Join;
import com.successmetricsinc.query.MeasureColumn;
import com.successmetricsinc.query.MeasureTable;
import com.successmetricsinc.query.TableDefinition;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.RepositoryException;
import com.successmetricsinc.util.SimpleMRUCache;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.warehouse.ForeignKey;
import com.successmetricsinc.warehouse.StagingTable;

public class PackageImport implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(PackageImport.class);
	public String SpaceID;
	public String PackageID;
	public String PackageName;
	public ImportedItem[] Items;

	public PackageImport(Element e, Namespace ns) throws IOException
	{
		SpaceID = e.getChildTextTrim("SpaceID", ns);
		PackageName = e.getChildTextTrim("PackageName", ns);
		PackageID = e.getChildText("PackageID", ns);
		
		Element iItems = e.getChild("Items",ns);
    if (iItems!=null)
    {
      List children = iItems.getChildren();
      if (children!=null && !children.isEmpty())
      {
        Items = new ImportedItem[children.size()];
        for (int count =0;count<children.size();count++)
        {
          ImportedItem ii = new ImportedItem((Element) children.get(count), ns);
          Items[count] = ii;
        }
      }
    }
	}
	
	public Element getPackageImportElement(Namespace ns)
	{
	  Element e = new Element("PackageImport",ns);
	  
	  Element child = new Element("SpaceID",ns);
    child.setText(SpaceID);
    e.addContent(child);
    
    child = new Element("PackageID",ns);
    child.setText(PackageID);
    e.addContent(child);
    
    child = new Element("PackageName",ns);
    child.setText(PackageName);
    e.addContent(child);
    
    if (Items!=null && Items.length>0)
    {
      child = new Element("Items",ns);
      for (ImportedItem ii : Items)
      {
        child.addContent(ii.getImportedItemElement(ns));
      }
      e.addContent(child);
    }
	  
	  return e;
	}

	@SuppressWarnings("static-access")
	public static void processImports(Repository r)
	{
		int nameCount = 0;

		Set<String> timePrefixes = new HashSet<String>();
		timePrefixes.add("");
		for(DimensionTable dt : r.DimensionTables)
		{
			if(dt.DimensionName != null && dt.DimensionName.equals("Time") && dt.VirtualMeasuresPrefix != null && (!dt.VirtualMeasuresPrefix.isEmpty()))
			{
				timePrefixes.add(dt.VirtualMeasuresPrefix);
			}
		}
		
		try
		{
			HashSet<String> importedCSANames = new HashSet<String>();
			List<PackageDefinition> importedPackageDefinitionList = new ArrayList<PackageDefinition>();
			Map<String, RepositoryPackage> importedRepositoryPackageCache = new SimpleMRUCache<String, RepositoryPackage>(15);			
			for (PackageImport pi : r.Imports)
			{
				PackageDefinition pDefinition = null;
				PackageSpaceProperties spaceProperties = r.getPackageSpaceProps(pi.PackageID, pi.SpaceID);
				if (spaceProperties == null || spaceProperties.getSpaceDirectory() == null)
				{
					logger.error("No package space information found for " + pi.SpaceID + ", package name: " + pi.PackageName);
					continue;
				}
				
				String directory = spaceProperties.getSpaceDirectory();
				String cacheKey = directory;
				File pfile = new File(directory + "\\Packages.xml");
				if (!pfile.exists())
				{
					logger.error("No package definition file found in directory: " + directory + ", space: " + pi.SpaceID + ", package name: " + pi.PackageName);
					continue;
				}
				SAXBuilder builder = new SAXBuilder();
				Document d = null;
				Namespace ns = Namespace.getNamespace("http://www.successmetricsinc.com");
				PObject[] stagingTables = null;
				PObject[] dimensionTables = null;
				PObject[] measureTables = null;
				PObject[] variables = null;
				PObject[] aggregates = null;
				PObject[] customSubjectAreas = null;
				RepositoryPackage existingRP = null;
				Element el = null;
				boolean newFormat = false;
				try
				{
					existingRP = importedRepositoryPackageCache.get(cacheKey);
					if(existingRP != null)
					{
						el = existingRP.packagesElement;
						newFormat = existingRP.newFormat;
					}
					
					if(el == null)
					{
						d = builder.build(pfile);
						el = d.getRootElement();
						if (el == null)
							continue;
						String rootName = el.getName();
						newFormat = "PackageList2".equals(rootName);

						el = el.getChild("Packages", ns);
						if (el == null)
							continue;
						RepositoryPackage iRPackage = new RepositoryPackage();
						iRPackage.packagesElement = el;
						iRPackage.newFormat = newFormat;						
						importedRepositoryPackageCache.put(cacheKey, iRPackage);
					}

					boolean foundPackage = false;
					for (Object o : el.getChildren())
					{
						Element pe = (Element) o;
						String packageID = pe.getChildTextTrim("ID", ns);
						if (!packageID.equals(pi.PackageID))
						{
							continue;
						}
						foundPackage = true;
						pDefinition = new PackageDefinition(pe, ns, newFormat);
						pDefinition.setPackageSpaceProperties(spaceProperties);
						stagingTables = pDefinition.getStagingTables();
						dimensionTables = pDefinition.getDimensionTables();
						measureTables = pDefinition.getMeasureTables();
						variables = pDefinition.getVariables();
						aggregates = pDefinition.getAggregates();
						customSubjectAreas = pDefinition.getCustomSubjectAreas();
						break;
					}
					if (!foundPackage)
					{
						logger.warn("Package not found : Package ID" + pi.PackageID + " : Package Name : " + pi.PackageName + " : Package Space ID : "
								+ pi.SpaceID);
					}
				} catch (JDOMException e)
				{
					logger.error(e.getMessage(), e);
				} catch (IOException e)
				{
					logger.error(e.getMessage(), e);
				}
				// Don't import an empty package
				if (stagingTables == null && dimensionTables == null && measureTables == null && variables == null && aggregates == null && customSubjectAreas == null)
					continue;
				File tf = new File(directory + "\\repository.xml");
				try
				{					
					d = builder.build(tf);
					Repository ir = new Repository(false, d, false, null, tf.lastModified(), false);

					if(!validatePackageBeforeImport(stagingTables, dimensionTables, measureTables, variables, aggregates, ir, r) || 
							!validatePackageForCustomSubjectAreas(importedCSANames, customSubjectAreas, r))
					{
						logger.error("Skipping importing remaining packages : Collision detected while importing package : Package ID" 
								+ pi.PackageID + " : Package Name : " + pi.PackageName + " : Package Space ID : " + pi.SpaceID);
						r.setCollisionFlag(true);
						break;
					}
					if(customSubjectAreas != null && customSubjectAreas.length > 0)
					{
						importedCSANames.addAll(Arrays.asList(getPackageObjectNames(customSubjectAreas)));
					}
					boolean defaultConflict = false;
					if(!r.SkipDBAConnectionMerge)
					{
						/*
						 * Merge the database connections
						 */
						List<DatabaseConnection> newConnections = new ArrayList<DatabaseConnection>();
						for (DatabaseConnection dconn : r.getConnections())
							newConnections.add(dconn);
						String irDefaultConnectionNewName = null; 
						for (DatabaseConnection dconn : ir.getConnections())
						{	
							if (dconn == null)
								continue;

							// See if an existing one has the same connection info
							DatabaseConnection founddc = null;
							for (DatabaseConnection dc : newConnections)
							{
								if (dc.ConnectString.equals(dconn.ConnectString) && dc.UserName.equals(dconn.UserName) && 
										dc.Password.equals(dconn.Password) && dc.Schema.equals(dconn.Schema))
								{
									founddc = dc;
									break;
								}
							}
							String oldName = null;
							String newName = null;
							if (founddc != null)
							{
								// Merge if necessary
								if (!founddc.Name.equals(dconn.Name))
								{
									oldName = dconn.Name;
									newName = founddc.Name;
								}
							} else
							{
								// Make sure there's no naming conflict
								for (DatabaseConnection dc : newConnections)
								{
									if (dc.Name.equals(dconn.Name))
									{
										oldName = dconn.Name;
										newName = dconn.Name + System.currentTimeMillis() + nameCount++;
										dconn.Name = newName;
										/*
										 * The default connection in the imported repository does not match the default in
										 * the main repository. Therefore things like staging tables that exclusively use
										 * the default connection (i.e. they have no explicit connection defined) cannot be
										 * imported
										 * 
										 * OK, this is a bad idea. We can handle staging tables across connections (BP)
										 */
										//if (ir.getDefaultConnection() == dconn)
										//										defaultConflict = true;
										break;
									}
								}

								newConnections.add(dconn);
								r.instantiateConnectionPool(dconn);
							}
							if (oldName != null)
							{
								if(oldName.equals(Repository.DEFAULT_DB_CONNECTION))
								{
									irDefaultConnectionNewName = newName;
								}
								/*
								 * Rename connections in imported repository as needed
								 */
								for (DimensionTable dt : ir.DimensionTables)
								{
									if (dt.TableSource != null && dt.TableSource.ConnectionName != null && dt.TableSource.ConnectionName.equals(oldName))
										dt.TableSource.ConnectionName = newName;
								}
								for (MeasureTable mt : ir.MeasureTables)
								{
									if (mt.TableSource != null && mt.TableSource.ConnectionName != null && mt.TableSource.ConnectionName.equals(oldName))
										mt.TableSource.ConnectionName = newName;
								}

								Aggregate[] iAggregates = ir.getAggregates();
								if(iAggregates != null && iAggregates.length > 0)
								{
									for(Aggregate agg : iAggregates)
									{
										String aggConnectionName = agg.getConnectionName();
										// if null connection meaning the default connection name which is modified in the child repository
										String newConnectionName = aggConnectionName != null && aggConnectionName.equals(oldName) ? newName : irDefaultConnectionNewName;
										agg.setConnectionName(newConnectionName);
									}
								}
							}
						}
						// Update connection list
						r.setConnections(newConnections);
					}
					// Collect tables
					List<StagingTable> stables = new ArrayList<StagingTable>();
					List<DimensionTable> dtables = new ArrayList<DimensionTable>();
					List<MeasureTable> mtables = new ArrayList<MeasureTable>();
					List<Variable> newvars = new ArrayList<Variable>();
					if (stagingTables != null)
					{
						for (PObject stPObject : stagingTables)
						{
							String stname = stPObject.getName();
							StagingTable st = ir.findStagingTable(stname);
							if (st != null)
							{
								st.Imported = true;
								st.r = ir;
								stables.add(st);
								if (st.DiscoveryTable)
								{
									/*
									 * If this is a discovery source, import the associated dimension and measure tables
									 */
									String baseName = getDisplayName(st.SourceFile);
									DimensionTable dt = ir.findDimensionTable(baseName);
									if (dt != null)
									{
										dt.Imported = true;
										dt.packageHide = stPObject.isHide();
										dtables.add(dt);
									}
									MeasureTable mt = ir.findMeasureTableName(baseName + " Fact");
									if (mt != null)
									{
										mt.Imported = true;
										mt.packageHide = stPObject.isHide();
										mtables.add(mt);
									}
								}
							}
						}
					}
					if(aggregates != null && aggregates.length > 0)
					{
						List<Aggregate> allAggs = new ArrayList<Aggregate>();
						for(PObject aggregatePObject : aggregates)
						{	
							Aggregate iAgg = ir.findAggregate(aggregatePObject.getName());
							if(iAgg != null)
							{
								iAgg.setImported(true);
								allAggs.add(iAgg);
							}
						}
						
						Aggregate[] existingAggs = r.getAggregates();
						if(existingAggs != null && existingAggs.length > 0)
						{
							for(Aggregate agg : existingAggs)
							{
								allAggs.add(agg);
							}
						}
						if(allAggs != null && allAggs.size() > 0)
						{
							r.setAggregates(allAggs.toArray(new Aggregate[allAggs.size()]));
						}
					}
					if (dimensionTables != null)
					{
						for (PObject dimTablePObject : dimensionTables)
						{
							String dimTable = dimTablePObject.getName();
							DimensionTable dt = ir.findDimensionTable(dimTable);
							if (dt != null)
							{
								dt.Imported = true;
								dt.packageHide = dimTablePObject.isHide();
								dtables.add(dt);
							}
						}
					}
					if (measureTables != null)
					{
						Set<String> mtPhysicalNames = new HashSet<String>();
						for (int i = 0; i < measureTables.length; i++)
						{
							PObject mTablePObject = measureTables[i];
							String mTable = mTablePObject.getName();
							MeasureTable mt = ir.findMeasureTableName(mTable);
							if (mt != null)
							{
								mt.Imported = true;
								mt.packageHide = mTablePObject.isHide();
								mt.DisplayName = mTablePObject.getDisplayName();
								mtables.add(mt);
								if(mt.TableSource != null && mt.TableSource.Tables != null && mt.TableSource.Tables.length == 1 && mt.TableSource.Tables[0] != null)
								{
									mtPhysicalNames.add(mt.TableSource.Tables[0].PhysicalName);
								}
							}							
						}

						List<MeasureTable> comboFactTables = new ArrayList<MeasureTable>();
						pullInComboFactTables(mtPhysicalNames, comboFactTables, ir);
						if(comboFactTables.size() > 0)
						{
							mtables.addAll(comboFactTables);
						}
						
						List<MeasureTable> inheritedMeasureTables = new ArrayList<MeasureTable>();
						for (MeasureTable mt : mtables)
						{
							pullInInheritedTables(mt, inheritedMeasureTables, ir);
						}
						if(inheritedMeasureTables.size() > 0)
						{
							mtables.addAll(inheritedMeasureTables);
						}
					}
					if (variables != null)
					{
						for (PObject variablePObject : variables)
						{
							String vname = variablePObject.getName();
							Variable v = ir.findVariable(vname);
							if (v == null)
								continue;
							v.setFillRepository(ir);
							newvars.add(v);
						}
					}
					if (newvars.size() > 0)
					{
						Variable[] nativeVariables = r.getVariables();
						if(nativeVariables != null && nativeVariables.length > 0)
						{
							for (Variable v : r.getVariables())
								// XXX duplicates issues for all tables and vars and hiers and dimensions? // XXX package at
								// table level
								newvars.add(v);
						}
						Variable[] newvarr = new Variable[newvars.size()];
						newvars.toArray(newvarr);
						r.setVariables(newvarr);
					}
					if (stables.size() > 0)
					{
						if (defaultConflict)
							logger.error("Unable to import staging tables from repository [" + ir.getApplicationName()
									+ "] - the default connections do not match");
						else
						{
							for (StagingTable oldst : r.getStagingTables())
								stables.add(oldst);
							StagingTable[] newlist = new StagingTable[stables.size()];
							stables.toArray(newlist);
							r.setStagingTables(newlist);
						}
					}
					List<Join> newJoins = new ArrayList<Join>();
					if (dtables.size() > 0)
					{
						for (DimensionTable dt : dtables)
						{
							List<Join> jlist = ir.getJoinList(dt.TableName);
							if(jlist != null && jlist.size() > 0)
							{
								for (Join j : jlist)
								{
									String t2 = null;
									if (j.Table1Str.equals(dt.TableName))
										t2 = j.Table2Str;
									else
										t2 = j.Table1Str;
									boolean foundJoiningmt = false;
									for (MeasureTable mt : mtables)
									{
										if (mt.TableName.equals(t2))
										{
											newJoins.add(j);
											foundJoiningmt = true; 
											break;
										}
									}
									//Joining measure table not found so this could be a snowflake join
									if(!foundJoiningmt)
									{
										for(DimensionTable joiningdt : dtables)
										{
											//Ignore the same dimension table
											if(joiningdt == dt)
											{
												continue;
											}
											if(joiningdt.TableName.equals(t2))
											{
												newJoins.add(j);
												break;
											}
										}
									}
								}
							}
						}
					}
					if (dtables.size() > 0)
					{
						/*
						 * Gather a list of hierarchies and dimensions that need to be imported as well
						 */
						List<Hierarchy> hlist = new ArrayList<Hierarchy>();
						for (DimensionTable dt : dtables)
						{
							String dimName = dt.DimensionName;
							for (Hierarchy h : ir.getHierarchies())
								if (h.DimensionName.equals(dimName))
									hlist.add(h);
							for (DimensionColumn dc : dt.DimensionColumns)
							{
								r.addDimensionColumnToMap(dc);
							}
							// Update the schema
							DatabaseConnection dconn = ir.findConnection(dt.TableSource.ConnectionName);
							if ((dt.TableSource.Schema == null || dt.TableSource.Schema.length() == 0) && dconn != null && dconn.Schema != null
									&& dconn.Schema.length() > 0)
								dt.TableSource.Schema = dconn.Schema;
						}
						if (r.DimensionTables != null)
							for (DimensionTable olddt : r.DimensionTables)
								dtables.add(olddt);
						DimensionTable[] newlist = new DimensionTable[dtables.size()];
						dtables.toArray(newlist);
						r.DimensionTables = newlist;
						for (Hierarchy oldh : r.getHierarchies())
							hlist.add(oldh);
						Hierarchy[] newhlist = new Hierarchy[hlist.size()];
						hlist.toArray(newhlist);
						r.setHierarchies(newhlist);
					}
					List<Variable> variablesToAdd = new ArrayList<Variable>();
					if (mtables.size() > 0)
					{
						for (MeasureTable mt : mtables)
						{
							for (MeasureColumn mc : mt.MeasureColumns)
							{
								r.addMeasureColumnToMap(mc);
							}
							// Update the schema
							DatabaseConnection dconn = ir.findConnection(mt.TableSource.ConnectionName);
							if ((mt.TableSource.Schema == null || mt.TableSource.Schema.length() == 0) && dconn != null && dconn.Schema != null
									&& dconn.Schema.length() > 0)
								mt.TableSource.Schema = dconn.Schema;
							// Import any necessary load variables
							Variable[] irVariables = ir.getVariables();
							if(irVariables != null && irVariables.length > 0)
							{
								for (Variable v : ir.getVariables())
								{
									if (v.getName().equals("Load:" + mt.TableName))
									{
										variablesToAdd.add(v);
									}
								}
							}
							// Process any time-related joins, including time series measures 							
							for(String tp : timePrefixes)
							{
								String mtableName = tp + mt.TableName;
								List<Join> jlist = ir.getJoinList(mtableName);
								if(jlist != null && jlist.size() > 0)
								{
									for (Join j : jlist)
									{
										String otherTableName = j.Table1Str;
										if (otherTableName.equals(mtableName))
											otherTableName = j.Table2Str;
										DimensionTable dt = ir.findDimensionTable(otherTableName);
										if (dt != null && dt.AutoGenerated && dt.DimensionName.equals("Time"))
											newJoins.add(j);
									}
								}
							}
						}
						if (r.MeasureTables != null)
							for (MeasureTable oldmt : r.MeasureTables)
								mtables.add(oldmt);
						MeasureTable[] newlist = new MeasureTable[mtables.size()];
						mtables.toArray(newlist);
						r.MeasureTables = newlist;
					}
					if (newJoins.size() > 0)
					{
						Join[] curJoins = r.getJoins();
						for (Join j : curJoins)
							newJoins.add(j);
						Join[] newJArr = new Join[newJoins.size()];
						newJoins.toArray(newJArr);
						r.setJoins(newJArr);
					}
					if (variablesToAdd.size() > 0)
					{
						Variable[] curList = r.getVariables();
						if (curList == null)
							curList = new Variable[0];
						Variable[] newList = new Variable[curList.length + variablesToAdd.size()];
						for (int i = 0; i < curList.length; i++)
							newList[i] = curList[i];
						for (int i = 0; i < variablesToAdd.size(); i++)
							newList[i + curList.length] = variablesToAdd.get(i);
						r.setVariables(newList);
					}
					
					importedPackageDefinitionList.add(pDefinition);
				} catch (JDOMException e)
				{
					logger.error(e.getMessage(), e);
				} catch (IOException e)
				{
					logger.error(e.getMessage(), e);
				} catch (RepositoryException e)
				{
					logger.error(e.getMessage(), e);
				} catch (CloneNotSupportedException e)
				{
					logger.error(e.getMessage(), e);
				} catch (BaseException e)
				{
					logger.error(e.getMessage(), e);
				}
			}
			
			r.setImportedPackageDefinitionList(importedPackageDefinitionList);
		} catch (SQLException e)
		{
			logger.error("Unable to connect to admin database to process imports: " + e.getMessage());
			return;
		} finally
		{
		}
	}

	private static String[] getPackageObjectNames(PObject[] pObjects)
	{
		if(pObjects != null && pObjects.length > 0)
		{
			String[] response = new String[pObjects.length];
			for(int i=0; i < pObjects.length; i++)
			{
				response[i] = pObjects[i].getName();
			}
			return response;
		}
		return null;
	}
	
	private static boolean validatePackageBeforeImport(PObject[] stagingTables, PObject[] dimensionTables, PObject[] measureTables,
			PObject[] variables, PObject[] aggregates, Repository importedRepository, Repository r) 
	{
		return validatePackageBeforeImport(getPackageObjectNames(stagingTables), getPackageObjectNames(dimensionTables), getPackageObjectNames(measureTables), 
				getPackageObjectNames(variables), getPackageObjectNames(aggregates), importedRepository, r);
	}
	
	private static boolean validatePackageBeforeImport(String[] stagingTables, String[] dimensionTables, String[] measureTables,
			String[] variables, String[] aggregates, Repository importedRepository, Repository r) 
	{	
		if(stagingTables != null && stagingTables.length > 0)
		{
			StagingTable[] existingStagingTables = r.getStagingTables();
			if(existingStagingTables != null && existingStagingTables.length > 0)
			{
				List<String> existingStagingTableNames = new ArrayList<String>();
				for(StagingTable st : existingStagingTables)
				{
					existingStagingTableNames.add(st.Name);
				}
				
				String stagingTableMatch = Util.anyMatch(stagingTables, 
						(String[]) existingStagingTableNames.toArray(new String[existingStagingTableNames.size()])); 
				if(stagingTableMatch != null && stagingTableMatch.trim().length() > 0)
				{
					logger.error("Collision detected on staging table match : " + stagingTableMatch);
					return false;
				}
			}
		}
		
		
		if(dimensionTables != null && dimensionTables.length > 0)
		{
			DimensionTable[] existingDimTables = r.DimensionTables;
			if(existingDimTables != null && existingDimTables.length > 0)
			{
				List<String> existingDimTableNames = new ArrayList<String>();
				for(DimensionTable dt : existingDimTables)
				{
					existingDimTableNames.add(dt.TableName);
				}
				
				String dimTableMatch = Util.anyMatch(dimensionTables, 
						(String[]) existingDimTableNames.toArray(new String[existingDimTableNames.size()])); 
				if(dimTableMatch != null && dimTableMatch.trim().length() > 0)
				{
					logger.error("Collision detected on dimension table match : " + dimTableMatch);
					return false;
				}
				
				List<String> existingDimList = r.getDimensionList();
				List<String> toImportDimList = getDimensions(importedRepository, (String[]) dimensionTables);
				if(toImportDimList != null && toImportDimList.size() > 0)
				{
					for(String existingDimension : existingDimList)
					{
						for(String toImportDim : toImportDimList)
						{
							if(existingDimension.equals(toImportDim))
							{
								logger.error("Collision detected on dimension match : " + toImportDim);
								return false;
							}
						}
					}
				}
			}
		}
		
		if(measureTables != null && measureTables.length > 0)
		{
			MeasureTable[] existingMeasureTables = r.MeasureTables;
			if(existingMeasureTables != null && existingMeasureTables.length > 0)
			{
				List<String> existingMeasureTableNames = new ArrayList<String>();
				for(MeasureTable mt : existingMeasureTables)
				{
					existingMeasureTableNames.add(mt.TableName);
				}

				String measureTableMatch = Util.anyMatch(measureTables, 
						(String[]) existingMeasureTableNames.toArray(new String[existingMeasureTableNames.size()])); 
				if(measureTableMatch != null && measureTableMatch.trim().length() > 0)
				{
					logger.error("Collision detected on measure table match : " + measureTableMatch);
					return false;
				}
			}
		}
		
		if(variables != null && variables.length > 0)
		{
			Variable[] existingVariables = r.getVariables();
			if(existingVariables != null && existingVariables.length > 0)
			{
				List<String> existingVariableNames = new ArrayList<String>();
				for(Variable variable : existingVariables)
				{
					existingVariableNames.add(variable.getName());
				}
				String variableNameMatch = Util.anyMatch(variables, 
						(String[]) existingVariableNames.toArray(new String[existingVariableNames.size()]));
				if(variableNameMatch != null && variableNameMatch.trim().length() > 0)
				{
					logger.error("Collision detected on variable name match : " + variableNameMatch);
					return false;
				}
			}
		}
		
		if(aggregates != null && aggregates.length > 0)
		{
			Aggregate[] existingAggregates = r.getAggregates();
			if(existingAggregates != null && existingAggregates.length > 0)
			{
				List<String> existingAggNames = new ArrayList<String>();
				for(Aggregate aggregate : existingAggregates)
				{
					existingAggNames.add(aggregate.getName());
				}
				String aggNameMatch = Util.anyMatch(variables, 
						(String[]) existingAggNames.toArray(new String[existingAggNames.size()]));
				if(aggNameMatch != null && aggNameMatch.trim().length() > 0)
				{
					logger.error("Collision detected on aggregate name match : " + aggNameMatch);
					return false;
				}
			}
		}
		
		return true; 
	}
	

	private static boolean validatePackageForCustomSubjectAreas( HashSet<String> importedCSANames, PObject[] toImportCustomSubjectAreas, Repository r) 
	{
		if(toImportCustomSubjectAreas != null && toImportCustomSubjectAreas.length > 0)
		{
			String[] toImportCSANames = PackageImport.getPackageObjectNames(toImportCustomSubjectAreas);
			if(toImportCSANames != null && toImportCSANames.length > 0)
			{
				String[] alreadyImported = importedCSANames != null && importedCSANames.size() > 0 ? importedCSANames.toArray(new String[importedCSANames.size()]) : null;
				String csaNameMatch = Util.anyMatch(toImportCSANames, alreadyImported);
				//Compare with other imported first
				if(csaNameMatch != null && csaNameMatch.trim().length() > 0)
				{
					logger.error("Collision detected on custom subject area name match : " + csaNameMatch);
					return false;
				}

				final File repositoryRootDir = r.getRepositoryRoot();
				final String subjectAreaPath = repositoryRootDir + File.separator + "custom-subject-areas";
				final File subjectAreaDir = new File(subjectAreaPath);
				if(subjectAreaDir.exists())
				{
					final HashSet<String> toImportCSAFileNames = new HashSet<String>();
					for(String name : toImportCSANames)
					{	
						toImportCSAFileNames.add("subjectarea_" + name + ".xml");
					}
					// see if there are any file names that matches with the toImport csa names
					String[] matchingNames = subjectAreaDir.list( new FilenameFilter() {
						@Override
						public boolean accept(File dir, String name) {
							if(toImportCSAFileNames.contains(name)){
								return true;
							}
							return false;
						}
					}
					);

					if(matchingNames != null && matchingNames.length > 0)
					{
						logger.error("Collision detected on custom subject area name matches");
						return false;
					}
				}
			}
		}
		return true;
	}
	
	private static List<String> getDimensions(Repository repository, String[] dimTableNames)
    {
        List<String> dimensions = new ArrayList<String>();
        if (dimTableNames != null && dimTableNames.length > 0)
        {
            for(String dimTableName : dimTableNames)
            {
                DimensionTable dt = findDimTable(repository, dimTableName);
                if(dt != null)
                {
                	String dimName = dt.DimensionName;
                	if (dimName != null && !dimensions.contains(dimName))
                	{
                		dimensions.add(dimName);
                	}
                }
            }
        }
        return dimensions;
    }

	private static DimensionTable findDimTable(Repository repository, String dimTableName)
    {   
        if (repository != null && repository.DimensionTables != null && repository.DimensionTables.length > 0)
        {
            for(DimensionTable dt : repository.DimensionTables)
            {
                if (dt.TableName == dimTableName)
                {
                    return dt;
                }
            }
        }
        return null;
    }
	
	private static void pullInInheritedTables(MeasureTable mt, List<MeasureTable> tbls, Repository ir)
	{
		String name = mt.TableName;
		if (mt.InheritPrefix != null && (!mt.InheritPrefix.isEmpty())) // only interested in base tables and combo fact tables
			return;
		for (MeasureTable imt : ir.MeasureTables)
		{
			if (name.equals(imt.InheritTable))
			{
				imt.Imported = true;
				tbls.add(imt);
			}
		}
	}

	private static void pullInComboFactTables(Set<String> mtPhysicalNames, List<MeasureTable> tbls, Repository ir)
	{
		for (MeasureTable imt : ir.MeasureTables)
		{
			//Ignore the inherited tables - they will be processed as part of separate method to pullInInheritedTables
			if (imt.InheritPrefix != null && (!imt.InheritPrefix.isEmpty()))
			{
				continue;
			}
			if(imt.TableSource != null && imt.TableSource.Tables != null && imt.TableSource.Tables.length > 1)
			{
				boolean include = true;
				for(TableDefinition td : imt.TableSource.Tables)
				{
					//Make sure for a combo fact table, all underlying base fact tables are actually imported as part of the package.
					if(td.PhysicalName == null || !mtPhysicalNames.contains(td.PhysicalName))
					{
						include = false;
						break;
					}
				}
				if(include)
				{
					imt.Imported = true;
					tbls.add(imt);
				}
			}
		}
	}
	
	private static String getDisplayName(String sourceFileName)
	{
		String fname = null;
		int extindex = sourceFileName.lastIndexOf('.');
		if (extindex >= 0)
			fname = sourceFileName.substring(0, extindex);
		else
			fname = sourceFileName;
		return fname;
	}
	
	private static class RepositoryPackage
	{		
		Element packagesElement;
		boolean newFormat;
	}
}
