/**
o * $Id: Repository.java,v 1.569 2012-11-23 12:43:26 BIRST\mpandit Exp $
 *
 * Copyright (C) 2007-2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.apache.log4j.Logger;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import com.successmetricsinc.Variable.VariableType;
import com.successmetricsinc.R.RServer;
import com.successmetricsinc.chart.CustomGeoMap;
import com.successmetricsinc.engine.ModelParameters;
import com.successmetricsinc.engine.OptimizerParameters;
import com.successmetricsinc.engine.Outcome;
import com.successmetricsinc.engine.Pattern;
import com.successmetricsinc.engine.PatternSearchDefinition;
import com.successmetricsinc.engine.PerformanceMeasure;
import com.successmetricsinc.engine.PerformanceModel;
import com.successmetricsinc.engine.ReferencePopulation;
import com.successmetricsinc.engine.ReferencePopulation.PeerItem;
import com.successmetricsinc.engine.SuccessModel;
import com.successmetricsinc.engine.SuccessModelInstance;
import com.successmetricsinc.engine.operation.Operation;
import com.successmetricsinc.packages.PackageDefinition;
import com.successmetricsinc.query.AbstractQueryString;
import com.successmetricsinc.query.Aggregate;
import com.successmetricsinc.query.BadColumnNameException;
import com.successmetricsinc.query.ColumnSubstitution;
import com.successmetricsinc.query.ConnectionPool;
import com.successmetricsinc.query.DataUnavailableException;
import com.successmetricsinc.query.Database;
import com.successmetricsinc.query.Database.DatabaseType;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.DatabaseConnectionStatement;
import com.successmetricsinc.query.Dimension;
import com.successmetricsinc.query.DimensionColumn;
import com.successmetricsinc.query.DimensionTable;
import com.successmetricsinc.query.GenericColumn;
import com.successmetricsinc.query.GroupBy;
import com.successmetricsinc.query.Hierarchy;
import com.successmetricsinc.query.HierarchyLevel;
import com.successmetricsinc.query.JasperDataSourceProvider;
import com.successmetricsinc.query.Join;
import com.successmetricsinc.query.JoinResult;
import com.successmetricsinc.query.JoinType;
import com.successmetricsinc.query.Level;
import com.successmetricsinc.query.LevelKey;
import com.successmetricsinc.query.MeasureColumn;
import com.successmetricsinc.query.MeasureGrain;
import com.successmetricsinc.query.MeasureTable;
import com.successmetricsinc.query.NavigationException;
import com.successmetricsinc.query.PublishingException;
import com.successmetricsinc.query.Query;
import com.successmetricsinc.query.QueryAggregate;
import com.successmetricsinc.query.QueryColumn;
import com.successmetricsinc.query.QueryFilter;
import com.successmetricsinc.query.QueryMap;
import com.successmetricsinc.query.QueryResultSet;
import com.successmetricsinc.query.SyntaxErrorException;
import com.successmetricsinc.query.TableDefinition;
import com.successmetricsinc.query.TableSource;
import com.successmetricsinc.query.TableSourceFilter;
import com.successmetricsinc.query.VirtualColumn;
import com.successmetricsinc.query.FederatedJoinQueryHelper;
import com.successmetricsinc.security.EncryptionService;
import com.successmetricsinc.security.IRealm;
import com.successmetricsinc.security.SMIRealm;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.object.SavedExpressions;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.QuickDashboard;
import com.successmetricsinc.util.RepositoryException;
import com.successmetricsinc.util.RepositoryExpression;
import com.successmetricsinc.util.TimeZoneUtil;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XmlUtils;
import com.successmetricsinc.warehouse.LoadGroup;
import com.successmetricsinc.warehouse.ScriptGroup;
import com.successmetricsinc.warehouse.SourceFile;
import com.successmetricsinc.warehouse.StagingFilter;
import com.successmetricsinc.warehouse.StagingTable;
import com.successmetricsinc.warehouse.TimeDefinition;
import com.successmetricsinc.warehouse.partition.PartitionMetadataProcessor;
import com.successmetricsinc.warehouse.partition.SourceFileSplitter;
import com.successmetricsinc.warehouse.partition.WarehousePartition;
import com.successmetricsinc.warehouse.partition.PartitionConnectionLoadInfo;

/**
 * Logical metadata repository
 * 
 * @author Brad Peters
 */
public class Repository implements Serializable
{
	// REMEMBER TO INCREMENT THIS VALUE ON EVERY SINGLE RELEASE, EVEN MAINTENANCE RELEASES, EVEN IF REPOSITORY.JAVA DOES NOT CHANGE
	// 1 for 4.4.1, 2 for 4.4.2, 3 for 5.0.X, 4 for 5.1/5.1.1, 5 for 5.1.2, 6 for 5.2, 7 for 5.2.1, 8 for 5.2.2, 9 for 5.3
	// 10 for 5.3.1.1, 11 for 5.4.0.0 (introduction of synonyms hashtable)
	// 13 For new classes added for saving repository via PerfEngine command
	private static final long serialVersionUID = 13L; 
	private static final int RepositoryCompatVersion = 5; // oldest version this engine can read
	public static final int RepositoryVersion = 8; // current engine version
	private static Logger logger = Logger.getLogger(Repository.class);
	
	// General Info
	private Integer Version = null;
	private long lastModified;
	private String applicationName;
	private String sourceCodeControlVersion;
	private int currentBuilderVersion;
	private String lastEdit;
	private String targetDimension;
	private String optimizationGoal;
	private Integer CurrentReferenceID;
	// Whether or not to auto-generate a subject area
	private boolean GenerateSubjectArea;
	// Reference Populations - peering
	private ReferencePopulation[] referencePopulations;
	// Performance Models (use Reference Populations)
	private PerformanceModel[] performanceModels;
	// Success Models
	private SuccessModel[] successModels;
	// Filters
	private QueryFilter[] filters;
	// Success Patterns (use Reference Populations)
	private PatternSearchDefinition[] searchDefinitions;
	// Outcomes (use Success Models)
	private Outcome[] outcomes;
	// Data Modeling
	public MeasureTable[] MeasureTables;
	public DimensionTable[] DimensionTables;
	public DimensionTable[] allDimensionTables;
	public Dimension[] Dimensions;
	private Join[] joins;
	private VirtualColumn[] virtualColumns;
	private Hierarchy[] hierarchies;
	private Aggregate[] aggregates;
	private SubjectArea[] subjectAreas;
	private Operation[] operations;
	private Map<String, GenericColumn> generics = new HashMap<String, GenericColumn>();
	// Email
	private Broadcast[] broadcasts;
	// Warehouse
	private SourceFile[] sourceFiles;
	private StagingTable[] stagingTables;
	// Server parameters
	private ServerParameters serverParameters;
	// Connection Info
	private List<DatabaseConnection> connections;
	private ModelParameters modelParameters;
	private OptimizerParameters optimizerParameters;
	// Runtime Info
	private Group[] groups;
	public Group[] repositoryGroups;
	private LoadGroup[] loadGroups;
	private List<CustomDrill> customDrills = new ArrayList<CustomDrill>();
	private List<User> users;
	public User[] repositoryUsers;
	public ACLItem[] repositoryACLItems;
	private Variable[] variables;
	private Map<String, List<ACLItem>> accessControlLists;
	private Map<String, Resource> resources;
	public Resource[] rResources; 
	private TimeDefinition timeDefinition;
	// Synonyms to allow internal physical renaming of objects
	private Map<String, String> synonyms = new HashMap<String, String>();
	// Runtime Data
	private Map<String, Object> repositoryVariables;
	// Hashed structures for finding objects
	private static final int DEFAULT_INITIAL_CAPACITY = 5000;
	private Map<String, List<MeasureColumn>> measureColumnMap = new HashMap<String, List<MeasureColumn>>(DEFAULT_INITIAL_CAPACITY);
	private Map<String, List<DimensionColumn>> dimensionColumnMap = new HashMap<String, List<DimensionColumn>>(DEFAULT_INITIAL_CAPACITY);
	private Map<Object, List<Object>> joinTableMap = new HashMap<Object, List<Object>>(DEFAULT_INITIAL_CAPACITY / 4);
	private Map<Object, List<Join>> joinMap = new HashMap<Object, List<Join>>(DEFAULT_INITIAL_CAPACITY / 4);
	private Set<String> volatileColumns = new HashSet<String>();
	private IRealm realm = null;
	private HashSet<String> variableSet = new HashSet<String>();
	private String orOperatorForMultiValueFilter = null;
	private String andOperatorForMultiValueFilter = null;
	private String separatorForListPromptOptions = null;
	public static final String DEFAULT_OR_OPERATOR = "||";
	public static final String DEFAULT_AND_OPERATOR = "&&";
	public static final String DEFAULT_SEPARATOR_FOR_LIST_PROMPT_OPTIONS = ",";
	public static final String DEFAULT_DB_CONNECTION = "Default Connection";
	public static final int MODEL_NONE = 0;
	public static final int MODEL_REFERENCE_POPULATIONS = 1;
	public static final int MODEL_PERFORMANCE = 2;
	public static final int MODEL_SUCCESS_MODELS = 3;
	// Query aggregates
	private List<QueryAggregate> queryAggregates = new ArrayList<QueryAggregate>();
	private boolean repositoryInitialized = false;
	private Properties overrides = null;
	private Map<String, StagingFilter> stagingFilterMap = new HashMap<String, StagingFilter>();
	public StagingFilter[] stFilters;
	private ScriptGroup[] ScriptGroups;
	private Map<String, ScriptGroup> scriptGroupMap = new HashMap<String, ScriptGroup>();
	private Set<String> possibleUnmappedMeasures = new HashSet<String>();
	private HierarchyLevel[][] dependencies;
	private List<QuickDashboard> quickDashboards = new ArrayList<QuickDashboard>();
	private List<QueryFilter> mdxSourceFilters = new ArrayList<QueryFilter>();
	
	private static final int REPOSITORY_SIZE_WARN =   10000000; // 10MB
	private static final int REPOSITORY_SIZE_ERROR = 100000000; // 100MB
	// Constants for WarehouseDataTypes
	public static final String DOUBLE = "Double";
	public static final String INTEGER = "Integer";
	public static final String VARCHAR = "Varchar";
	public static final String DATETIME = "DateTime";
	public static final String BOOLEAN = "Boolean";
	private boolean noAggregates = false;
	private boolean isDynamic = false;
	private boolean useNewQueryLanguage = false;
	private int queryLanguageVersion = 0;
	private boolean UseCacheForRepositoryInitialization = true;
	private int NumThresholdFailedRecords;
	private int MinYear = -1; // for limiting the initial year query in prompts
	private int MaxYear = -1;
	public boolean AllowNonUniqueGrains;
	public static final String USERS_GROUPS_FILE_NAME = "UsersAndGroups.xml";
	public List<PackageImport> Imports;
	private boolean UseNewETLSchemeForIB;
	public static final int RELATIONAL_DIMENSIONAL_STRUCTURE = 0;
	public static final int OLAP_DIMENSIONAL_STRUCTURE = 1;
	private int DimensionalStructure = RELATIONAL_DIMENSIONAL_STRUCTURE;
	
	private static int VARIABLE_FILL_TIMEOUT = 300;
	public static final String ALLOW_DYNAMIC_DATABASE_CONNECTIONS = "AllowDynamicDatabaseConnections";
	private List<PackageSpaceProperties> packageSpacesPropertiesList;
	private boolean collision;
	private List<PackageDefinition> importedPackageDefinitionList;
	public boolean SkipDBAConnectionMerge;

	// Whether or not to partition the warehouse storage across different database instances
	private boolean IsPartitioned;
	private List<WarehousePartition> WarehousePartitions;
	private List<PartitionConnectionLoadInfo> PartitionConnectionLoadInfoList;
	private Map<String, PartitionConnectionLoadInfo> dcToPartitionConnectionLoadInfoMap = new HashMap<String, PartitionConnectionLoadInfo>();
	public String partitionConnection;
	private SourceFileSplitter sfs;
	public static final int IntToLongBuilderVersion = 19;
	public static final int PushDownMeasureFilters = 23;
	public static final int JoinCompoundColumnBuilderVersion = 23;
	
	private static boolean useNewConnectorFramework = false;
	// Whether or not to allow java integrations
	private boolean AllowExternalIntegrations;
	private int ExternalIntegrationTimeout = -1;
	// Whether or not to show the cache key in the log
	private boolean logCacheKey = false;
	private boolean liveAccessSecurityFilter = false;
	private int MaxPhysicalTableNameLength;
	private int MaxPhysicalColumnNameLength;
	private int NumPerformancePeers;
	private int NumSuccessPatternPeers;
	
	private boolean RequiresPublish;
	private boolean RequiresRebuild;
	private boolean Hybrid;
	private boolean NoBirstApplicationRebuild;

	private boolean DisablePublishLock;
	private boolean EnableLiveAccessToDefaultConnection;	
	private boolean DisableImpliedGrainsProcessing;
	
	private BuildProperties buildProperties;
	
	public LogicalExpression[] logicalExpressions;
	public String[][] DynamicGroups;
	
	public String SuccessMeasure;
	public String[] PeeringFilters;
	
	public String repoDefaultConn;
	public MeasureGrain[] UnmappedMeasureGrains;	
	private transient boolean isLoadWarehouseExecuted;
	
	/**
	 * Performance optimization: During profiler run, it was found that the method findDimensionLevel() was called very
	 * frequently and consumed a lot of time during processing. So, now we maintain a map with key dimension/level
	 * string and value as Level object. The method findDimensionLevel() now uses this map instead of iterating over
	 * each hierarchy and levels within a given hierarchy to find a given dimension level.
	 */
	private Map<String, Level> dimensionLevelMap = new HashMap<String, Level>();
	
	private List<CustomGeoMap> customGeoMaps;
	
	// saved expressions that can be used with the SavedExpression('Bob') expression syntax
	private SavedExpressions savedExpressions;

	public static String googleAnalyticsAPIKey = null;
	
	public boolean joinContainsVariable = false;
	
	// R Connect Information
	private RServer RServer;
	
	public RServer getRServer()
	{
		return RServer;
	}

	public void setRServer(RServer rServer)
	{
		RServer = rServer;
	}

	public Repository()
	{
	}
	
	public static boolean isUseNewConnectorFramework() {
		return useNewConnectorFramework;
	}

	public static void setUseNewConnectorFramework(boolean useNewConnectorFramework) {
		Repository.useNewConnectorFramework = useNewConnectorFramework;
	}

	public boolean isLiveAccessSecurityFilter() {
		return liveAccessSecurityFilter;
	}	

	public int getDimensionalStructure()
	{
		return DimensionalStructure;
	}
	
	public int getNumThresholdFailedRecords()
	{
		return NumThresholdFailedRecords;
	}
	
	public boolean isUseNewETLSchemeForIB()
	{
		return UseNewETLSchemeForIB;
	}

	public boolean isUseNewQueryLanguage()
	{
		return useNewQueryLanguage;
	}
	
	public int getMinYear()
	{
		return MinYear;
	}
	
	public int getMaxYear()
	{
		return MaxYear;
	}

	public static long getSerializationVersion()
	{
		return serialVersionUID;
	}

	public static boolean checkRepositorySize(File fi)
	{
		if (fi.length() > REPOSITORY_SIZE_ERROR)
		{
			logger.error("Repository " + fi.getAbsolutePath() + " is too large, will not load it (" + fi.length() + " >= " + REPOSITORY_SIZE_ERROR + ")");
			return false;
		} else if (fi.length() > REPOSITORY_SIZE_WARN)
		{
			logger.warn("Repository " + fi.getAbsolutePath() + " is large (" + fi.length() + " >= " + REPOSITORY_SIZE_WARN + ")");
			return true;
		}
		// logger.debug("Repository " + fi.getAbsolutePath() + " size is " + fi.length());
		return true;
	}

	/**
	 * Save the dynamic (in memory) version of repository with the meta data generated to view in admin tool for
	 * debugging purposes.
	 * 
	 * This method uses the actual xml structure of the original repository and replaces the DimensionTables,
	 * MeasureTables and Joins nodes with the in memory structures so that the meta data generated when loading
	 * repository as well as initializing aggregates becomes part of the dynamic repository that is to be saved and this
	 * repository can be viewed in Admin Tool in readonly mode to view the meta data generated. When dynamic repository
	 * is serialized with in memory data structures, it also sets IsDynamic flag to true in repository and
	 * reinitializing such a dynamic repository will be reported as fatal error for both Performance Engine and SMIWeb
	 * and also the Admin Tool will open such a repository in read only mode and will not permit the user to save the
	 * changes made to it.
	 * 
	 * @param d
	 *            XML document structure for the current command session
	 * @param dynamicRepFileName
	 *            Name of file which is to be saved as dynamic repository
	 * 
	 * @author mpandit
	 */
	public void saveDynamicRep(Document d, String dynamicRepFileName)
	{
		try
		{
			XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
			xmlOutputter.getFormat().setEncoding("utf-8");
			xmlOutputter.getFormat().setOmitDeclaration(false);
			xmlOutputter.getFormat().setOmitEncoding(false);
			PrintWriter pw = new PrintWriter(new File(dynamicRepFileName));
			Element e = d.getRootElement();
			Namespace ns = e.getNamespace();
			@SuppressWarnings("rawtypes")
			Iterator it = e.getChildren().iterator();
			while (it.hasNext())
			{		
				Element child = (Element) it.next();
				if (child.getName().equalsIgnoreCase("DimensionTables"))
				{
					child.removeChildren("DimensionTable", ns);
					for (DimensionTable dt : DimensionTables)
					{
						child.addContent(dt.getDimensionTableElement(ns, this));
					}
				} else if (child.getName().equalsIgnoreCase("MeasureTables"))
				{
					child.removeChildren("MeasureTable", ns);
					for (MeasureTable mt : MeasureTables)
					{
						child.addContent(mt.getMeasureTableElement(ns, this));
					}
				} else if (child.getName().equalsIgnoreCase("Joins"))
				{
					child.removeChildren("Join", ns);
					for (Join j : joins)
					{
						child.addContent(j.getJoinElement(ns, this));
					}
				}
			}
			Element child = new Element("IsDynamic", ns);
			child.setText("true");
			e.addContent(child);
			xmlOutputter.output(e, pw);
			pw.close();
		} catch (FileNotFoundException e)
		{
			logger.error(e.getMessage(), e);
		} catch (IOException e)
		{
			logger.error(e.getMessage(), e);
		}
	}

	/**
	 * Initialize the repository structure using an xml document source
	 * 
	 * @param d
	 *            XML document
	 * @param createAggregates
	 *            Whether to create aggregate tables while instantiating them
	 * @param lastModified
	 *            Time when the repository file was last modified (not used)
	 * @throws ScriptException 
	 * @throws Exception
	 */
	public Repository(Document d, boolean createAggregates, long lastModified) throws SQLException, IOException, RepositoryException,
			CloneNotSupportedException, BaseException
	{
		initialize(d, createAggregates, null, lastModified, false, true);
	}
	
	/**
   * Initialize the repository structure using an xml document source
   * 
   * @param d
   *            XML document
   * @param createAggregates
   *            Whether to create aggregate tables while instantiating them
   * @param lastModified
   *            Time when the repository file was last modified (not used)
   * @param processMetadata
   *            Whether to generate repository in-memory metadata
   * @throws ScriptException 
   * @throws Exception
   */
  public Repository(Document d, boolean createAggregates, long lastModified,boolean processMetadata) throws SQLException, IOException, RepositoryException,
      CloneNotSupportedException, BaseException
  {
    initialize(d, createAggregates, null, lastModified, false, processMetadata);
  }

	/**
	 * Initialize the repository structure using an xml document source
	 * 
	 * @param d
	 *            XML document
	 * @param createAggregates
	 *            Whether to create aggregate tables while instantiating them
	 * @param overrides
	 *            Repository values that are overriden by customer.properties (normally the DB connection)
	 * @param lastModified
	 *            Time when the repository file was last modified (not used)
	 * @param noAggregates
	 *            Whether aggregates are not to be initialized (disables all aggregates)
	 * @throws ScriptException 
	 * @throws Exception
	 */
	public Repository(Document d, boolean createAggregates, Properties overrides, long lastModified, boolean noAggregates, 
			List<PackageSpaceProperties> spacePropertiesList, String partitionConnection) throws SQLException, IOException,
			RepositoryException, CloneNotSupportedException, BaseException
	{
		this.noAggregates = noAggregates;
		this.partitionConnection = partitionConnection;
		createSpacePropertiesMap(spacePropertiesList);
		initialize(d, createAggregates, overrides, lastModified, false, true);
	}
	
	/**
	 * Initialize the repository structure using an xml document source
	 * 
	 * @param d
	 *            XML document
	 * @param createAggregates
	 *            Whether to create aggregate tables while instantiating them
	 * @param overrides
	 *            Repository values that are overriden by customer.properties (normally the DB connection)
	 * @param lastModified
	 *            Time when the repository file was last modified (not used)
	 * @param noAggregates
	 *            Whether aggregates are not to be initialized (disables all aggregates)
	 * @param noMetadata
	 * 			 Whether to generate in-memory metadata
	 * @throws ScriptException 
	 * @throws Exception
	 */
	public Repository(Document d, boolean createAggregates, Properties overrides, long lastModified, boolean noAggregates, 
			List<PackageSpaceProperties> spacePropertiesList, String partitionConnection,boolean processmetadata) throws SQLException, IOException,
			RepositoryException, CloneNotSupportedException, BaseException
	{
		this.noAggregates = noAggregates;
		this.partitionConnection = partitionConnection;
		createSpacePropertiesMap(spacePropertiesList);
		initialize(d, createAggregates, overrides, lastModified, false, processmetadata);
	}
	
	private void createSpacePropertiesMap(List<PackageSpaceProperties> packageSpacePropertiesList)
	{
		if(packageSpacePropertiesList != null && packageSpacePropertiesList.size() > 0)
		{
			for(PackageSpaceProperties spaceProperties : packageSpacePropertiesList)
			{
				addPackageSpaceInfo(spaceProperties);
			}
		}
	}
	
	/**
	 * Initialize the repository structure using an xml document source
	 * 
	 * @param d
	 *            XML document
	 * @param createAggregates
	 *            Whether to create aggregate tables while instantiating them
	 * @param overrides
	 *            Repository values that are overriden by customer.properties (normally the DB connection)
	 * @param lastModified
	 *            Time when the repository file was last modified (not used)
	 * @param repOnlyAuthenticationAndAuthorization
	 *            Only use the repository for authentication and authorization, not the DB - mainly for Multitenant
	 * @throws ScriptException
	 * @throws Exception
	 */
	public Repository(Document d, boolean createAggregates, Properties overrides, boolean repOnlyAuthenticationAndAuthorization, long lastModified)
			throws SQLException, IOException, RepositoryException, CloneNotSupportedException, BaseException
	{
		initialize(d, createAggregates, overrides, lastModified, repOnlyAuthenticationAndAuthorization, true);
	}

	public Repository(boolean processMetadata, Document d, boolean createAggregates, Properties overrides, long lastModified, boolean repOnlyAuthenticationAndAuthorization) throws SQLException, IOException, RepositoryException, CloneNotSupportedException, BaseException
	{
		initialize(d, createAggregates, overrides, lastModified, repOnlyAuthenticationAndAuthorization, processMetadata);
	}

	private void initialize(Document d, boolean createAggregates, Properties overrides, long lastModified, boolean repOnlyAuthenticationAndAuthorization,
			boolean processMetadata) throws SQLException, IOException, RepositoryException, CloneNotSupportedException, BaseException
	{
		setCollisionFlag(false);
		this.lastModified = lastModified;
		if (repositoryInitialized)
		{
			logger.error("trying to construct the repository twice");
			return;
		}
		this.overrides = overrides;
		parseXMLDoc(d);
		if (processMetadata)
			processUninitializedMetadata(createAggregates, repOnlyAuthenticationAndAuthorization);
		else
			setupDBConnections();
	}

	public void processUninitializedMetadata(boolean createAggregates, boolean repOnlyAuthenticationAndAuthorization) throws RepositoryException,
			NavigationException, CloneNotSupportedException, SQLException, IOException, BaseException
	{
		if (metaDataProcessed)
			return;
		setupDBConnections();
		buildPhysicalNamesforAggregates();
		processMetadata(createAggregates, repOnlyAuthenticationAndAuthorization);
		CustomGeoMap.initialize(this);
	}
	public boolean metaDataProcessed = false;

	public void processMetadata(boolean createAggregates, boolean repOnlyAuthenticationAndAuthorization) throws RepositoryException,
			CloneNotSupportedException, SQLException, NavigationException, IOException, BaseException
	{
		if (metaDataProcessed)
			return;
		buildDimensionLevelMap();
		metaDataProcessed = true;
		Statement stmt = null;
		Resource resource = getResource("Multi Value Filter OR Operator");
		if (resource != null)
		{
			orOperatorForMultiValueFilter = resource.getValue();
		}
		resource = getResource("Multi Value Filter AND Operator");
		if (resource != null)
		{
			andOperatorForMultiValueFilter = resource.getValue();
		}
		resource = getResource("List Prompt Options Separator");
		if (resource != null)
		{
			separatorForListPromptOptions = resource.getValue();
		}
		if (orOperatorForMultiValueFilter == null)
		{
			orOperatorForMultiValueFilter = DEFAULT_OR_OPERATOR;
		}
		if (andOperatorForMultiValueFilter == null)
		{
			andOperatorForMultiValueFilter = DEFAULT_AND_OPERATOR;
		}
		if (separatorForListPromptOptions == null)
		{
			separatorForListPromptOptions = DEFAULT_SEPARATOR_FOR_LIST_PROMPT_OPTIONS;
		}
		DimensionColumn.setSurrogateKeyFlags(this);
		processImports();
		setupConnectionsAndFilters();
		addDummySessionVariable("USER"); // force USER into the list of session variables so it can be found in processVariables
		processVariables(false);
		processInheritedColumns();
		processColumnQualifications();
		processJoinLevels();
		setJoins();
		expandDerivedMeasures();
		updateBaseMeasuresListForDerivedMeasures();
		processVirtualColumns(stmt);
		/*
		 * Don't log queries processed in opaque views
		 */
		org.apache.log4j.Level currentLogLevel = Logger.getRootLogger().getLevel();
		Logger.getRootLogger().setLevel(org.apache.log4j.Level.ERROR);
		processOpaqueViews();
		Logger.getRootLogger().setLevel(currentLogLevel);
		processSnowFlakes();
		// Generate a subject area if needed
		if (GenerateSubjectArea)
		{
			if (this.subjectAreas == null || this.subjectAreas.length < 1)
				this.subjectAreas = new SubjectArea[1];
			this.subjectAreas[0] = new SubjectArea(this);
		}
		processJoinRelationships();
		processVariables(true);
		instantiateRepositoryVariables(stmt, UseCacheForRepositoryInitialization ? new JasperDataSourceProvider(this) : null);
		processAggregateTables(stmt, createAggregates);
		// Process measure tables that inherit from aggregate tables and virtual measures for aggregates
		processInheritedMeasureTables(true);
		processInheritedVirtualMeasures(true);
		if(isPartitioned())
		{
			processPartitionMetadata();
		}
		// second argument is where it is rep only - if mt or excel, then rep only
		DatabaseConnection conn = findConnection(Repository.DEFAULT_DB_CONNECTION);
		if (conn.DBType == Database.DatabaseType.ODBCExcel || DatabaseConnection.isDBTypeODBC(conn.DBType))
			repOnlyAuthenticationAndAuthorization = true;
		realm = new SMIRealm(this, repOnlyAuthenticationAndAuthorization, overrides);
		repositoryInitialized = true;
		modelingDerivedMeasures();
		// Setup subject area
		//processSubjectAreaJoins();
		// check for unmapped measures
		if (getPossibleUnmappedMeasures().size() > 0)
		{
			for (Aggregate agg : aggregates)
			{
				if (agg.getMeasures() != null)
					for (String meas : agg.getMeasures())
					{
						getPossibleUnmappedMeasures().remove(meas);
					}
			}
			if (getPossibleUnmappedMeasures().size() > 0)
			{
				StringBuilder sb = new StringBuilder();
				for (String meas : getPossibleUnmappedMeasures())
				{
					if (sb.length() > 0)
						sb.append(',');
					sb.append(meas);
				}
				logger.warn("The following measures are UNMAPPED: " + sb.toString());
			}
		}
		// validate model data sets
		validateModelingMetadata();
		// dump custom drill
		if (logger.isDebugEnabled())
			dumpCustomDrills();
	}
	
	/**
	 * Generate Physical Names for Aggregates which can control physical table name 
	 */
	public void buildPhysicalNamesforAggregates()
	{
		if(this.getCurrentBuilderVersion()>=26)
		{
			for(Aggregate a : this.getAggregates())
			{
				DatabaseConnection dc = null;
				dc = a.getConnectionName() != null ? this.findConnection(a.getConnectionName()) : this.getDefaultConnection();			
				if(a.getName()!=null && a.getName().length() >= DatabaseConnection.getIdentifierLength(dc.DBType)-4)
				{
					a.setName(dc.getHashedIdentifier(a.getName()));
				}
			}
		}
	}

	/**
	 * Build a map of dimension levels so that given a dimension/level string, we can readily look up the associated
	 * level object.
	 */
	public void buildDimensionLevelMap()
	{
		Hierarchy[] hiers = getHierarchies();
		if (hiers != null && hiers.length > 0)
		{
			for (int i = 0; i < hiers.length; i++)
			{
				List<Level> decendents = new ArrayList<Level>();
				hiers[i].getAllDecendents(decendents);
				if (!decendents.isEmpty())
				{
					for (Level l : decendents)
					{
						dimensionLevelMap.put(hiers[i].DimensionName + "." + l.Name, l);
					}
				}
			}
		}
	}

	private void setInputTimeZonesForSourceFiles(TimeZone processingTZ)
	{
		if (this.sourceFiles != null)
		{
			for (SourceFile sf : this.sourceFiles)
			{
				if (sf.InputTimeZoneID == null || sf.InputTimeZoneID.equals("") || sf.InputTimeZoneID.equals("Same As Processing Time Zone"))
					sf.setInputTimeZone(processingTZ);
				else
					sf.setInputTimeZone(TimeZoneUtil.getTimeZoneForWindowsTZID(sf.InputTimeZoneID));
			}
		}
	}

	private void dumpCustomDrills()
	{
		List<CustomDrill> cds = getCustomDrills();
		if (cds != null)
		{
			for (CustomDrill cd : cds)
			{
				logger.debug(cd.toString());
			}
		}
	}

	/**
	 * load repository for the purposes of repository-based authentication (used for multitenant)
	 * 
	 * @param d
	 * @throws IOException
	 * @throws RepositoryException
	 */
	public Repository(Document d) throws IOException, RepositoryException
	{
		parseXMLDoc(d);
		realm = new SMIRealm(this, true, null);
		setCollisionFlag(false);
	}

	/**
	 * validate the metadata associated with modeling
	 */
	private void validateModelingMetadata()
	{
		validateReferencePopulations();
		validatePerformanceModels();
		validateModelDatasets();
	}

	/**
	 * validate the metadata associated with reference populations
	 */
	private void validateReferencePopulations()
	{
		if (referencePopulations == null || referencePopulations.length == 0)
			return;
		for (int j = 0; j < referencePopulations.length; j++)
		{
			ReferencePopulation population = referencePopulations[j];
			if (population.TargetDimension == null || population.TargetDimension.isEmpty())
			{
				logger.warn("Reference Population: " + population.getName() + ": missing target dimension");
				continue;
			}
			if (population.Level != null && population.Level.length() > 0)
			{
				if (findDimensionLevel(population.TargetDimension, population.Level) == null)
				{
					logger.error("Reference Population: " + population.getName() + ": dimension level does not exist: " + population.TargetDimension + ":"
							+ population.Level);
				}
			} else
			{
				LevelKey lk = getDimensionKey(population.TargetDimension);
				if (lk == null)
					logger.error("Reference Population: " + population.getName() + ": No level key for dimension: " + population.TargetDimension);
				if (lk.ColumnNames.length > 1)
					logger.error("Reference Population: " + population.getName() + ": dimension key contains more than one key column: "
							+ population.TargetDimension);
				if (findDimensionColumn(population.TargetDimension, lk.ColumnNames[0]) == null)
				{
					logger.error("Reference Population: " + population.getName() + ": dimension column does not exist: " + population.TargetDimension + ":"
							+ lk.ColumnNames[0]);
				}
			}
			validatePeers(population, true);
			validatePeers(population, false);
			String measure = population.getSuccessMeasure();
			if (measure == null || measure.isEmpty())
			{
				logger.error("Reference Population: " + population.getName() + ": missing success measure");
			} else
			{
				validateMeasure("Reference Population", population.getName(), measure);
			}
			validateFilters("Reference Population", population.getName(), population.getPeeringFilters());
		}
	}

	/**
	 * validate a measure
	 */
	private void validateMeasure(String label, String name, String measureName)
	{
		List<MeasureColumn> mclist = findMeasureColumns(measureName);
		if (mclist == null || mclist.isEmpty())
		{
			logger.error(label + ": " + name + ": measure does not exist: " + measureName);
		}
	}

	/**
	 * validate the peer sets for a reference population
	 * 
	 * @param population
	 * @param performance
	 *            true for performance, false for success
	 */
	private void validatePeers(ReferencePopulation population, boolean performance)
	{
		if (population.getPeerColumns(performance) != null)
		{
			PeerItem[] itemArray = population.getPeerColumns(true);
			for (int count = 0; count < itemArray.length; count++)
			{
				if (itemArray[count].Type == 0)
				{
					if (findDimensionColumn(population.TargetDimension, itemArray[count].Column) == null)
					{
						logger.error("Reference Population: " + population.getName() + ": peer dimension column does not exist: " + population.TargetDimension
								+ ":" + itemArray[count].Column);
					}
				} else
				{
					validateMeasure("Reference Population", population.getName(), itemArray[count].Column);
				}
			}
		}
	}

	private void validateFilters(String label, String name, String[] filterNames)
	{
		if (filterNames != null)
		{
			for (int i = 0; i < filterNames.length; i++)
			{
				String filterName = filterNames[i];
				if (findFilter(filterName) == null)
				{
					logger.error(label + ": " + name + ": filter does not exist: " + filterName);
				}
			}
		}
	}

	/**
	 * validate performance models
	 */
	private void validatePerformanceModels()
	{
		if (performanceModels == null || performanceModels.length == 0)
			return;
		for (int j = 0; j < performanceModels.length; j++)
		{
			PerformanceModel perf = performanceModels[j];
			@SuppressWarnings("unused")
			ReferencePopulation population = findReferencePopulation(perf.ReferencePopulationName);
			validateFilters("Performance Model", perf.getName(), perf.PerformanceFilters);
			if (perf.Measures != null)
			{
				for (int i = 0; i < perf.Measures.length; i++)
				{
					PerformanceMeasure measure = perf.Measures[i];
					validateMeasure("Performance Model", perf.getName(), measure.Name);
				}
			}
		}
	}

	/**
	 * validate the metadata associated with model datasets
	 * 
	 * @return
	 */
	private void validateModelDatasets()
	{
		if (successModels == null || successModels.length == 0)
			return;
		for (int j = 0; j < successModels.length; j++)
		{
			SuccessModel sm = successModels[j];
			if (findDimensionColumn(sm.TargetDimension, sm.TargetLevel) == null)
			{
				logger.error("Model Dataset: " + sm.Name + ": bad target attribute " + sm.TargetDimension + ":" + sm.TargetLevel);
			}
			if (sm.BreakModel == 2)
			{
				if (findDimensionColumn(sm.TargetDimension, sm.BreakAttribute) == null)
				{
					logger.error("Model Dataset: " + sm.Name + ": bad break attribute " + sm.TargetDimension + ":" + sm.BreakAttribute);
				}
			}
			if (sm.Measure != null && sm.Measure.length() > 0)
			{
				validateMeasure("Model Dataset", sm.Name, sm.Measure);
			}
			for (int i = 0; i < sm.Attributes.length; i++)
			{
				if (findDimensionColumn(sm.TargetDimension, sm.Attributes[i]) == null)
				{
					logger.error("Model Dataset: " + sm.Name + ": bad attribute " + sm.TargetDimension + ":" + sm.Attributes[i]);
				}
			}
			for (int i = 0; i < sm.Patterns.length; i++)
			{
				Pattern p = sm.Patterns[i];
				if (p.Type >= Pattern.MEASUREONLY)
				{
					List<MeasureColumn> mclist = findMeasureColumns(p.Measure);
					if (mclist == null || mclist.isEmpty())
					{
						logger.error("Model Dataset: " + sm.Name + ": bad pattern (measure) " + p.toString());
					}
				}
				if (p.Type >= Pattern.ONEATTRIBUTE)
				{
					if (findDimensionColumn(p.Dimension1, p.Attribute1) == null)
					{
						logger.error("Model Dataset: " + sm.Name + ": bad pattern (attribute1) " + p.toString());
					}
				}
				if (p.Type >= Pattern.TWOATTRIBUTE)
				{
					if (findDimensionColumn(p.Dimension2, p.Attribute2) == null)
					{
						logger.error("Model Dataset: " + sm.Name + ": bad pattern (attribute2) " + p.toString());
					}
				}
			}
		}
	}

	public boolean isRepositoryInitialized()
	{
		return repositoryInitialized;
	}
	Map<String, List<String>> modelingMeasures = new HashMap<String, List<String>>();

	public List<String> getRelatedMeasures(String measure)
	{
		return modelingMeasures.get(measure);
	}

	private void modelingDerivedMeasures()
	{
		Map<String, List<String>> internal = new HashMap<String, List<String>>();
		for (MeasureTable mt : MeasureTables)
		{
			if (mt.Type == MeasureTable.DERIVED)
			{
				for (MeasureColumn mc : mt.MeasureColumns)
				{
					List<String> items = new ArrayList<String>();
					findRelatedMeasures(mc.PhysicalName, items);
					internal.put(mc.ColumnName, items);
				}
			}
		}
		for (Outcome out : getOutcomes())
		{
			String classMeasure = out.ClassMeasure;
			if (classMeasure != null)
			{
				List<String> classMeasureList = internal.get(classMeasure);
				if (classMeasureList == null)
					continue;
				// see if the class measure list contains any of the other measures
				for (String key : internal.keySet())
				{
					// match keys to class measure list, add key list
					if (classMeasureList.contains(key))
					{
						classMeasureList.addAll(internal.get(key));
					}
					// match key list to class measure list, add key
					List<String> items = internal.get(key);
					for (String item : items)
					{
						if (classMeasureList.contains(item))
						{
							classMeasureList.add(key);
							break;
						}
					}
				}
				modelingMeasures.put(classMeasure, classMeasureList);
			}
			String valueMeasure = out.TargetMeasure;
			if (valueMeasure != null)
			{
				List<String> valueMeasureList = internal.get(valueMeasure);
				if (valueMeasureList == null)
					continue;
				// see if the value measure list contains any of the other measures
				for (String key : internal.keySet())
				{
					// match keys to value measure list, add key list
					if (valueMeasureList.contains(key))
					{
						valueMeasureList.addAll(internal.get(key));
					}
					// match key list to value measure list, add key
					List<String> items = internal.get(key);
					for (String item : items)
					{
						if (valueMeasureList.contains(item))
						{
							valueMeasureList.add(key);
							break;
						}
					}
				}
				modelingMeasures.put(valueMeasure, valueMeasureList);
			}
		}
		for (String key : modelingMeasures.keySet())
		{
			logger.debug("Modeling outcome related measures - " + key + ": " + getRelatedMeasures(key));
		}
	}

	private void findRelatedMeasures(String physicalName, List<String> list)
	{
		int start = physicalName.indexOf("M{", 0);
		while (start >= 0)
		{
			int end = physicalName.indexOf('}', start + 1);
			if (end > start)
			{
				String measure = physicalName.substring(start + 2, end);
				list.add(measure);
				start = physicalName.indexOf("M{", end + 1);
			} else
				break;
		}
		return;
	}
	/**
	 * Go through the derived measures and expand any derived measures that they include
	 */
	Map<String, MeasureColumn> dmeasures;

	private void expandDerivedMeasures()
	{
		dmeasures = new HashMap<String, MeasureColumn>();
		/*
		 * Collect all derived measures
		 */
		for (MeasureTable mt : MeasureTables)
		{
			if (mt.Type == MeasureTable.DERIVED)
			{
				for (MeasureColumn mc : mt.MeasureColumns)
					dmeasures.put(mc.ColumnName, mc);
			}
		}
		/*
		 * Go through each derived measure and expand
		 */
		for (Entry<String, MeasureColumn> entry : dmeasures.entrySet())
		{
			/*
			 * Look for every measure in the list and expand it's formula
			 */
			MeasureColumn mc = entry.getValue();
			mc.PhysicalName = Util.intern(expandDerivedMeasures(mc.PhysicalName));
		}
	}

	public String expandDerivedMeasures(String formula)
	{
		boolean changed;
		do
		{
			changed = false;
			for (Entry<String, MeasureColumn> entry2 : dmeasures.entrySet())
			{
				MeasureColumn mc = entry2.getValue();
				String result = formula.replace("M{" + mc.ColumnName + "}", "(" + mc.PhysicalName + ")");
				if (!formula.equals(result))
				{
					changed = true;
					formula = result;
				}
			}
		} while (changed);
		return (formula);
	}

	private void updateBaseMeasuresListForDerivedMeasures()
	{
		for (Entry<String, MeasureColumn> entry : dmeasures.entrySet())
		{
			MeasureColumn mc = entry.getValue();
			List<String> baseMeasures = new ArrayList<String>();
			findRelatedMeasures(mc.PhysicalName, baseMeasures);
			mc.setBaseMeasures(baseMeasures);
		}
	}

	/**
	 * create connection pools for all connections defined in the repository
	 * 
	 * @throws RepositoryException
	 */
	public void setupDBConnections() throws RepositoryException
	{
		for (DatabaseConnection c : getConnections())
		{
			if (c.DBType != DatabaseType.GoogleAnalytics && !c.isRealTimeConnection() && !c.Disabled)
			{
				if((partitionConnection != null) && (!partitionConnection.isEmpty()) && (!partitionConnection.equals(c.Name)))
				{
					continue;
				}
				instantiateConnectionPool(c);
				if((!c.isRealTimeConnection()) && DatabaseConnection.isDBTypeInfoBright(c.DBType))
				{
					if(Database.isUtf8CollationPossible(c))
					{
						c.setIBLoadWarehouseUsingUTF8(true);
					}
				}
				if (partitionConnection != null && !partitionConnection.isEmpty() && partitionConnection.equals(c.Name))
				{
					//create schema if not exists
					try
					{
						if (!Database.schemaExists(c))
						{
							Database.createSchema(c);
							logger.info("Schema created successfully on partition connnection - " + partitionConnection);
						}
					}
					catch (Exception ex)
					{
						logger.error("Problem encountered creating schema on partition connection  - " + partitionConnection);
					}
				}
			}
		}
		try
		{
			//Make sure SQL Server driver is loaded - this is required to obtain Proxy DB Registration to BirstAdmin database for LiveAccess connections
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		}
		catch(ClassNotFoundException ex)
		{
			logger.error(ex.getMessage(), ex);
		}
	}
	
	public void instantiateConnectionPool(DatabaseConnection c) throws RepositoryException
	{
		try
		{
			ConnectionPool cp = ConnectionPool.getInstance(c, c.Name, c.Driver, c.ConnectString, c.UserName, c.Password, c.ConnectionPoolSize,
					serverParameters.getProcessingTimeZone());
			c.ConnectionPool = cp;
		} catch (Exception ex)
		{
			logger.error("failed to create connection pool");
			throw (new RepositoryException("Unable to create connection pool for connection: " + c.Name + " - " + ex.toString()));
		}
	}
	
	public void closeAllDBConnections()
	{
		for (DatabaseConnection c : getConnections())
		{
			if (c.DBType != DatabaseType.GoogleAnalytics && !c.isRealTimeConnection() && !c.Disabled)
			{
				if (c.ConnectionPool != null)
					c.ConnectionPool.closeAllConnections();
			}
		}
		
	}

	/**
	 * Link up all table sources to their appropriate connections and populate their filter structures
	 */
	public void setupConnectionsAndFilters() throws RepositoryException
	{
		for (DimensionTable dt : DimensionTables)
		{
			if (dt.TableSource != null)
			{
				dt.TableSource.connection = findConnection(dt.TableSource.ConnectionName);
				// If no datasource has been selected, then use the default (first)
				if (dt.TableSource.connection == null)
				{
					dt.TableSource.ConnectionName = findConnection(Repository.DEFAULT_DB_CONNECTION).Name;
					dt.TableSource.connection = findConnection(Repository.DEFAULT_DB_CONNECTION);
				}
				dt.TableSource.setFilters(Arrays.asList(this.filters));
				if(dt.TableSource.connection.Realtime && DatabaseConnection.isDBTypeXMLA(dt.TableSource.connection.DBType))
				{
					if(dt.TableSource.Filters != null && dt.TableSource.Filters.length > 0)
					{
						for(TableSourceFilter tsf : dt.TableSource.Filters)
						{
							if(tsf.Filter != null && !tsf.Filter.isEmpty())
							{
								QueryFilter qf = QueryFilter.createFilterUsingNQL(tsf.Filter, this);
								if(qf != null && !mdxSourceFilters.contains(qf))
								{
									mdxSourceFilters.add(qf);
								}
							}
						}
					}
				}
			}
		}
		for (MeasureTable mt : MeasureTables)
		{
			// Don't qualify derived tables
			if (mt.Type != MeasureTable.DERIVED)
			{
				if (mt.TableSource != null)
				{
					mt.TableSource.connection = findConnection(mt.TableSource.ConnectionName);
					// If no datasource has been selected, then use the default (first)
					if (mt.TableSource.connection == null)
					{
						mt.TableSource.ConnectionName = findConnection(Repository.DEFAULT_DB_CONNECTION).Name;
						mt.TableSource.connection = findConnection(Repository.DEFAULT_DB_CONNECTION);
					}
					mt.TableSource.setFilters(Arrays.asList(this.filters));
				}
			}
		}
	}

	/**
	 * Lookup all levels for each join
	 */
	private void processJoinLevels()
	{
		for (Join j : joins)
			j.setLevel(this);
	}

	/**
	 * 
	 * @throws RepositoryException
	 */
	private void processColumnQualifications() throws RepositoryException
	{
		for (DimensionTable dt : DimensionTables)
		{
			if (dt.ProcessedQualifications)
				continue;
			dt.ProcessedQualifications = true;
			/* Qualify any columns that need to be qualified */
			for (DimensionColumn dc : dt.DimensionColumns)
			{
				if (dc.Qualify)
				{
					if (dt.Type == DimensionTable.OPAQUE_VIEW)
					{
						dc.PhysicalName = dt.PhysicalName + "." + dc.PhysicalName;
					} else
					{
						if (dt.TableSource.Tables.length == 0)
							throw (new RepositoryException("No table definitions defined for logical table " + dt.TableName));
						dc.PhysicalName = dt.TableSource.connection.getQualifiedColumn(dt.TableSource.Tables[0].PhysicalName, dc.PhysicalName);
					}
					if (dt.ColumnSubstitutions != null && (!isPrimaryLevelKey(dc)))
						dc.PhysicalName = ColumnSubstitution.makeSubstitutions(dc.PhysicalName, dt.ColumnSubstitutions);
				}
			}
		}
		for (MeasureTable mt : MeasureTables)
		{
			if (mt.ProcessedQualifications)
				continue;
			mt.ProcessedQualifications = true;
			// Don't qualify derived tables
			if (mt.Type != MeasureTable.DERIVED)
			{
				for (MeasureColumn mc : mt.MeasureColumns)
				{
					if (mc.Qualify)
					{
						if (mt.Type == MeasureTable.OPAQUE_VIEW)
						{
							mc.PhysicalName = Util.intern(mt.PhysicalName + "." + mc.PhysicalName);
						} else
						{
							if (mt.TableSource.Tables.length == 0)
								throw (new RepositoryException("No table definitions defined for logical table " + mt.TableName));
							mc.PhysicalName = Util.intern(mt.TableSource.connection.getQualifiedColumn(mt.TableSource.Tables[0].PhysicalName, mc.PhysicalName));
						}
					}
					if (mt.ColumnSubstitutions != null)
						mc.PhysicalName = Util.intern(ColumnSubstitution.makeSubstitutions(mc.PhysicalName, mt.ColumnSubstitutions));
				}
			}
		}
	}

	/**
	 * Set all joins with the appropriate tables (or delete)
	 */
	private void setJoins()
	{
		List<Join> dynamicJoins = new ArrayList<Join>();
		for (Join j : joins)
		{
			if (j.Table1 == null)
			{
				Object o1 = findDimensionTable(j.Table1Str);
				if (o1 == null)
					o1 = findMeasureTableName(j.Table1Str);
				j.Table1 = o1;
			}
			if (j.Table2 == null)
			{
				Object o2 = findDimensionTable(j.Table2Str);
				if (o2 == null)
					o2 = findMeasureTableName(j.Table2Str);
				j.Table2 = o2;
			}
			if(j.Federated && (j.FederatedJoinKeys != null) && (j.FederatedJoinKeys.length > 0))
			{
				MeasureTable mt = ((j.Table1 != null) && (j.Table1 instanceof MeasureTable)) ? (MeasureTable)j.Table1 : (((j.Table2 != null) && (j.Table2 instanceof MeasureTable)) ? (MeasureTable)j.Table2 : null);
				DimensionTable dt = ((j.Table1 != null) && (j.Table1 instanceof DimensionTable)) ? (DimensionTable)j.Table1 : (((j.Table2 != null) && (j.Table2 instanceof DimensionTable)) ? (DimensionTable)j.Table2 : null);  

				Join djoin = FederatedJoinQueryHelper.addDynamicDimensionAndReturnJoin(mt, dt, j.FederatedJoinKeys, this);
				if(djoin != null)
				{
					dynamicJoins.add(djoin);
				}
			}
		}
		
		if(dynamicJoins.size() > 0)
		{
			addJoins(dynamicJoins);
		}
	}

	public void addMeasureColumnToMap(MeasureColumn mc)
	{
		List<MeasureColumn> mclist = measureColumnMap.get(mc.ColumnName);
		if (mclist == null)
		{
			mclist = new ArrayList<MeasureColumn>(1);
			measureColumnMap.put(mc.ColumnName, mclist);
		}
		mclist.add(mc);
	}

	private void removeMeasureColumnFromMap(MeasureColumn mc)
	{
		List<MeasureColumn> mclist = measureColumnMap.get(mc.ColumnName);
		if (mclist != null)
		{
			mclist.remove(mc);
			if (mclist.isEmpty())
			{
				// For an empty list, remove reference from the measure column map
				measureColumnMap.remove(mc.ColumnName);
			}
		}
	}

	/**
	 * Add a dimension column to the mapping structure
	 * 
	 * @param dc
	 */
	public void addDimensionColumnToMap(DimensionColumn dc)
	{
		if (dc.overrideDimensionName != null && dc.overrideDimensionName.length() > 0)
			addDimensionColumnToMap(dc, dc.overrideDimensionName);
		else
			addDimensionColumnToMap(dc, dc.DimensionTable.DimensionName);
	}

	private void addDimensionColumnToMap(DimensionColumn dc, String dimensionName)
	{
		List<DimensionColumn> dclist = dimensionColumnMap.get(dimensionName + "." + dc.ColumnName);
		if (dclist == null)
		{
			dclist = new ArrayList<DimensionColumn>(1);
			dimensionColumnMap.put(dimensionName + "." + dc.ColumnName, dclist);
		}
		if (dc.isVolatile())
		{
			volatileColumns.add(dimensionName + "." + dc.ColumnName);
		}
		dclist.add(dc);
	}
	
	/**
	 * Remove a dimension column from mapping structures
	 * 
	 * @param dc
	 */
	private void removeDimensionColumnFromMap(DimensionColumn dc)
	{
		removeDimensionColumnFromMap(dc, dc.DimensionTable.DimensionName);
		if(dc.overrideDimensionName != null && dc.overrideDimensionName.length() > 0)
		{
			removeDimensionColumnFromMap(dc, dc.overrideDimensionName);
		}
	}

	private void removeDimensionColumnFromMap(DimensionColumn dc, String dimensionName)
	{
		String dimColName = dimensionName + "." + dc.ColumnName;
		List<DimensionColumn> dclist = dimensionColumnMap.get(dimColName);
		if (dclist != null)
		{
			dclist.remove(dc);
			if (dclist.isEmpty())
			{
				// For an empty list, remove reference from the dimension column map
				dimensionColumnMap.remove(dimColName);
			}
		}
	}
	/**
	 * Returns whether a given dimension column is volatile
	 * 
	 * @param dimension
	 * @param column
	 * @return
	 */
	public boolean isVolatile(String dimension, String column)
	{
		return (volatileColumns.contains(dimension + "." + column));
	}

	/**
	 * Return a list of dimension columns that match a given dimension and column name
	 * 
	 * @param dimensionName
	 * @param columnName
	 * @return
	 */
	public List<DimensionColumn> getDimensionColumnList(String dimensionName, String columnName)
	{
		return (dimensionColumnMap.get(dimensionName + "." + columnName));
	}

	/**
	 * Parse the XML repository document
	 * 
	 * @param d
	 * @throws RepositoryException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void parseXMLDoc(Document d) throws IOException, RepositoryException
	{
		Element root = d.getRootElement();
		List children = root.getChildren();
		Namespace ns = Namespace.getNamespace("http://www.successmetricsinc.com");
		// processing the version must be the first thing done
		String ver = root.getChildText("Version", ns);
		if (ver != null)
		{
			this.Version = Integer.valueOf(ver);
			logger.debug("Repository Version: " + this.Version);
		}
		String currentRefId = root.getChildText("CurrentReferenceID",ns);
		if (currentRefId!=null && !currentRefId.isEmpty())
		{
			this.CurrentReferenceID = Integer.parseInt(currentRefId);
		}
		if ((this.Version == null) || (this.Version.intValue() < Repository.RepositoryCompatVersion || this.Version.intValue() > Repository.RepositoryVersion))
		{
			System.out.println("FATAL ERROR: Engine Repository Version range (" + Repository.RepositoryCompatVersion + ".." + Repository.RepositoryVersion
					+ ") is not compatible with the Version in the Repository (" + this.Version + ")");
			logger.fatal("FATAL ERROR: Engine Repository Version range (" + Repository.RepositoryCompatVersion + ".." + Repository.RepositoryVersion
					+ ") is not compatible with the Version in the Repository (" + this.Version + ")");
			throw new RepositoryException("FATAL ERROR: Engine Repository Version range (" + Repository.RepositoryCompatVersion + ".."
					+ Repository.RepositoryVersion + ") is not compatible with the Version in the Repository (" + this.Version + ")");
		}
		// Check if the repository is ReadOnly and terminate if so.
		String dynamic = root.getChildText("IsDynamic", ns);
		if (dynamic != null)
		{
			this.isDynamic = Boolean.parseBoolean(dynamic);
			if (isDynamic)
			{
				System.out.println("FATAL ERROR: Cannot initialize readonly dynamic repository.");
				logger.fatal("Cannot initialize readonly dynamic repository.");
				throw new RepositoryException("Cannot initialize readonly dynamic repository.");
			}
		}
		String dimensionalStructureIndex = root.getChildText("DimensionalStructure", ns);
		if (dimensionalStructureIndex != null)
		{
			try
			{
				DimensionalStructure = Integer.parseInt(dimensionalStructureIndex);
			}
			catch(NumberFormatException nfe){}
		}
		this.sourceCodeControlVersion = root.getChildText("SourceCodeControlVersion", ns);
		this.lastEdit = root.getChildText("LastEdit", ns);
		this.applicationName = root.getChildText("ApplicationName", ns);
		String liveAccessSecurityFlag = root.getChildText("LiveAccessSecurityFilter", ns);
		if (liveAccessSecurityFlag!=null)
			this.liveAccessSecurityFilter = Boolean.parseBoolean(liveAccessSecurityFlag);
		String temp = root.getChildText("UseNewQueryLanguage", ns); // for backwards compatibility
		if (temp != null)
			this.useNewQueryLanguage = Boolean.valueOf(temp);
		temp = root.getChildText("QueryLanguageVersion", ns);
		if (temp != null)
		{
			this.queryLanguageVersion = Integer.valueOf(temp);
			if (this.queryLanguageVersion > 0)
			{
				this.useNewQueryLanguage = true;
			} else
			{
				this.useNewQueryLanguage = false;
			}
		}
		temp = root.getChildText("AllowNonUniqueGrains", ns); 
		if (temp != null)
			this.AllowNonUniqueGrains = Boolean.valueOf(temp);
		temp = root.getChildText("AllowExternalIntegrations", ns); 
		if (temp != null)
			this.AllowExternalIntegrations = Boolean.valueOf(temp);
		temp = root.getChildText("ExternalIntegrationTimeout", ns); 
		if (temp != null)
			this.ExternalIntegrationTimeout = Integer.valueOf(temp);
		temp = root.getChildText("LogCacheKey", ns); 
		if (temp != null)
			this.logCacheKey = Boolean.valueOf(temp);
		temp = root.getChildText("BuilderVersion", ns);
		if (temp != null)
		{
			currentBuilderVersion = Integer.valueOf(temp);
			logger.debug("Repository Builder Version: " + this.currentBuilderVersion);
		}
		temp = root.getChildText("MaxPhysicalTableNameLength", ns);
		if (temp!=null)
		{
			this.MaxPhysicalTableNameLength = Integer.valueOf(temp);			
		}
		temp = root.getChildText("MaxPhysicalColumnNameLength", ns);
		if (temp!=null)
		{
			this.MaxPhysicalColumnNameLength = Integer.valueOf(temp);			
		}
		temp = root.getChildText("MinYear", ns);
		if (temp != null)
		{
			MinYear = Integer.valueOf(temp);
			logger.debug("Minimum Year: " + this.MinYear);
		}
		temp = root.getChildText("MaxYear", ns);
		if (temp != null)
		{
			MaxYear = Integer.valueOf(temp);
			logger.debug("Maximum Year: " + this.MaxYear);
		}
		SuccessMeasure = root.getChildText("SuccessMeasure", ns);
		PeeringFilters = Repository.getStringArrayChildren(root, "PeeringFilters", ns);
		
		temp = root.getChildText("RequiresPublish", ns);
		if (temp != null)
		{
			RequiresPublish = Boolean.parseBoolean(temp);
		}
		
		temp = root.getChildText("RequiresRebuild", ns);
		if (temp != null)
		{
			RequiresRebuild = Boolean.parseBoolean(temp);
		}	
		
		temp = root.getChildText("Hybrid", ns);
		if (temp != null)
		{
			Hybrid = Boolean.parseBoolean(temp);
		}
		
		temp = root.getChildText("NoBirstApplicationRebuild", ns);
		if (temp != null)
		{
			NoBirstApplicationRebuild = Boolean.parseBoolean(temp);
		}
		
		temp = root.getChildText("DisablePublishLock", ns);
		if (temp != null)
		{
			DisablePublishLock = Boolean.parseBoolean(temp);
		}
		
		temp = root.getChildText("EnableLiveAccessToDefaultConnection", ns);
		if (temp != null)
		{
			EnableLiveAccessToDefaultConnection = Boolean.parseBoolean(temp);
		}
		
		temp = root.getChildText("DisableImpliedGrainsProcessing", ns);
		if (temp != null)
		{
			DisableImpliedGrainsProcessing = Boolean.parseBoolean(temp);
		}
		
		String defcon = root.getChildText("DefaultConnection", ns);
		repoDefaultConn = root.getChildText("DefaultConnection", ns);
		if (defcon != null)
		{
			this.defaultConnection = defcon;
			logger.debug("Setting the Default Connection to: " + this.defaultConnection);
		}
		
		Element xmlBuildProperties = root.getChild("BuildProperties",ns);
		if (xmlBuildProperties!=null)
		{
			this.buildProperties = new BuildProperties();
			this.buildProperties.postBuildReminder = xmlBuildProperties.getChildText("postBuildReminder", ns);
		}
				
		this.setDependencies(getHierarchyLevelDoubleArrayChildren(root, "Dependencies", ns));
		String genstr = root.getChildText("GenerateSubjectArea", ns);
		this.GenerateSubjectArea = genstr == null ? false : Boolean.parseBoolean(genstr);
		
		String ispartitionedstr = root.getChildText("IsPartitioned", ns);
		this.IsPartitioned = ispartitionedstr == null ? false : Boolean.parseBoolean(ispartitionedstr);
		
		// default settings
		this.targetDimension = root.getChildText("TargetDimension", ns);
		this.optimizationGoal = root.getChildText("OptimizationGoal", ns);
		// handle backwards compatibility (single reference population at the top level)
		String peers = root.getChildText("NumPerformancePeers", ns);
		if (peers != null)
		{
			this.NumPerformancePeers = Integer.valueOf(peers);
			this.referencePopulations = new ReferencePopulation[1];
			this.referencePopulations[0] = new ReferencePopulation(root, ns);
			if (this.referencePopulations[0].TargetDimension == null || this.referencePopulations[0].TargetDimension.isEmpty())
				this.referencePopulations[0].TargetDimension = this.targetDimension;
		}
		else
		{
			this.NumPerformancePeers = 0;
		}
		temp = root.getChildText("NumSuccessPatternPeers", ns);
		if (temp != null)
			NumSuccessPatternPeers = Integer.valueOf(temp);
		else
			NumSuccessPatternPeers = 0;
		temp = root.getChildText("NumThresholdFailedRecords", ns);
		if (temp != null)
			NumThresholdFailedRecords = Integer.valueOf(temp);
		else
			NumThresholdFailedRecords = 0;
		temp = root.getChildText("UseNewETLSchemeForIB", ns);
		UseNewETLSchemeForIB = Boolean.parseBoolean(temp);
		String skipDBAConnectionMergeString = root.getChildText("SkipDBAConnectionMerge", ns);
		if(skipDBAConnectionMergeString != null)
		{
			SkipDBAConnectionMerge = Boolean.parseBoolean(skipDBAConnectionMergeString);
		}
		// Get server parameters first to ensure we have the time zone for objets modified date
		Iterator iterator = children.iterator();
		while (iterator.hasNext())
		{
			Element child = (Element) iterator.next();
			String name = child.getName();
			if (name == "ServerParameters")
			{
				this.setServerParameters(new ServerParameters(child, ns));
				// logger.info("Engine Repository Version: " + Repository.RepositoryVersion + ", Repository Version: " +
				// this.Version);
				overrideServerParameters();
			}
		}
		iterator = children.iterator();
		while (iterator.hasNext())
		{
			Element child = (Element) iterator.next();
			String name = child.getName();
			if (name == "PerformancePeerColumns")
			{
				// ignore, already processed
			} else if (name == "SuccessPatternPeerColumns")
			{
				// ignore, already processed
			} else if (name == "ReferencePopulations")
			{
				List l = child.getChildren();
				this.referencePopulations = new ReferencePopulation[l.size()];
				for (int count = 0; count < l.size(); count++)
				{
					this.referencePopulations[count] = new ReferencePopulation((Element) l.get(count), ns);
				}
			} else if (name == "Connections")
			{
				List l = child.getChildren();
				this.setConnections(new ArrayList<DatabaseConnection>(l.size()));
				for (int count = 0; count < l.size(); count++)
				{
					DatabaseConnection dbc = new DatabaseConnection((Element) l.get(count), ns, this);
					// see if there are overrides in customer.properties (only connect string, username, and password)
					overrideDatabaseConnection(dbc);
					getConnections().add(dbc);
				}
				overrideDefaultConnection();
				boolean partitionConnectionFound = false;
				for(DatabaseConnection dc : connections)
				{
					if(dc.isPartitioned)
					{
						dc.initPartitionConnections(this);
						/**
						 * Make sure that there is only one connection in repository that is partitioned. If we want to allow multiple
						 * partitioned connections in a single repository, changes have to be made in app builder to associate dimension
						 * and facts to a given partitioned connection. Also, there have to be multiple serialized maps that contain
						 * partition key to prefix mapping in source file splitting process. 
						**/ 
						if(partitionConnectionFound)
						{
							throw new RepositoryException("Multiple connections with partitioned flag set to true are not supported.");
						}
						else
						{
							partitionConnectionFound = true;
						}
					}
				}
			} else if (name == "WarehousePartitions")
			{
				List l = child.getChildren();
				WarehousePartitions = new ArrayList<WarehousePartition>(l.size());
				for (int count = 0; count < l.size(); count++)
				{
					WarehousePartition wp = new WarehousePartition((Element) l.get(count), ns);
					WarehousePartitions.add(wp);
				}
				//Validate warehouse partitions - currently, we support partitions on default connection only.
				if(WarehousePartitions != null)
				{
					for(WarehousePartition wp : WarehousePartitions)
					{
						String dcName = wp.getDatabaseConnectionName();
						if(dcName == null)
						{
							throw new RepositoryException("No database connection name provided for partition \"" + wp.getName() + "\"");
						}
						//At some point, we will remove this check for default connection. When we do that, we want to introduce a new check
						//to make sure that all partitions in a given repository point to one single partitioned connection.
						if(!dcName.equals(DEFAULT_DB_CONNECTION))
						{
							throw new RepositoryException("Non-default connection name provided for partition \"" + wp.getName() 
									+ "\". Currently, only partitions on default connections are supported.");
						}
						String [] cnames = wp.getPartitionColumnNames();
						if(cnames == null || cnames.length == 0)
						{
							throw new RepositoryException("No partition column names provided for partition \"" + wp.getName() 
									+ "\".");
						}
					}
				}
			} else if (name == "PartitionConnectionLoadInfoList")
			{
				List l = child.getChildren();
				PartitionConnectionLoadInfoList = new ArrayList<PartitionConnectionLoadInfo>(l.size());
				for (int count = 0; count < l.size(); count++)
				{
					PartitionConnectionLoadInfo pcli = new PartitionConnectionLoadInfo((Element) l.get(count), ns);
					PartitionConnectionLoadInfoList.add(pcli);
					dcToPartitionConnectionLoadInfoMap.put(pcli.getPartitionConnectionName(), pcli);
				}
				//Validate warehouse partitions - currently, we support partitions on default connection only.
				if(WarehousePartitions != null)
				{
					for(WarehousePartition wp : WarehousePartitions)
					{
						String dcName = wp.getDatabaseConnectionName();
						if(dcName == null)
						{
							throw new RepositoryException("No database connection name provided for partition \"" + wp.getName() + "\"");
						}
						//At some point, we will remove this check for default connection. When we do that, we want to introduce a new check
						//to make sure that all partitions in a given repository point to one single partitioned connection.
						if(!dcName.equals(DEFAULT_DB_CONNECTION))
						{
							throw new RepositoryException("Non-default connection name provided for partition \"" + wp.getName() 
									+ "\". Currently, only partitions on default connections are supported.");
						}
						String [] cnames = wp.getPartitionColumnNames();
						if(cnames == null || cnames.length == 0)
						{
							throw new RepositoryException("No partition column names provided for partition \"" + wp.getName() 
									+ "\".");
						}
					}
				}
			} else if (name == "Performance")
			{
				this.performanceModels = new PerformanceModel[1];
				this.performanceModels[0] = new PerformanceModel(child, ns);
			} else if (name == "PerformanceModels")
			{
				List l = child.getChildren();
				this.performanceModels = new PerformanceModel[l.size()];
				for (int count = 0; count < l.size(); count++)
				{
					this.performanceModels[count] = new PerformanceModel((Element) l.get(count), ns);
				}
			} else if (name == "ModelParameters")
			{
				this.setModelParameters(new ModelParameters(child, ns));
			} else if (name == "OptimizerParameters")
			{
				this.setOptimizerParameters(new OptimizerParameters());
				this.getOptimizerParameters().MaxGenerations = Integer.parseInt(child.getChildText("MaxGenerations", ns));
				this.getOptimizerParameters().MaxNoChangeGenerations = Integer.parseInt(child.getChildText("MaxNoChangeGenerations", ns));
				this.getOptimizerParameters().Method = Integer.parseInt(child.getChildText("Method", ns));
				this.getOptimizerParameters().MutationRate = Double.parseDouble(child.getChildText("MutationRate", ns));
				this.getOptimizerParameters().PopulationSize = Integer.parseInt(child.getChildText("PopulationSize", ns));
				this.getOptimizerParameters().ReproductionRate = Double.parseDouble(child.getChildText("ReproductionRate", ns));
			} else if (name == "MeasureTables")
			{
				List l = child.getChildren();
				List<MeasureTable> mlist = new ArrayList<MeasureTable>();
				for (int count = 0; count < l.size(); count++)
				{
					MeasureTable mt = new MeasureTable(this, (Element) l.get(count), ns);
					if (mt.TableName != null && mt.MeasureColumns != null)
						mlist.add(mt);
				}
				this.MeasureTables = new MeasureTable[mlist.size()];
				invalidateMeasureTableNameMap();
				
				mlist.toArray(this.MeasureTables);
			} else if (name == "DimensionTables")
			{
				List l = child.getChildren();
				List<DimensionTable> dlist = new ArrayList<DimensionTable>();
				allDimensionTables = new DimensionTable[l.size()];
				for (int count = 0; count < l.size(); count++)
				{
					DimensionTable dim = new DimensionTable(this, (Element) l.get(count), ns);
					allDimensionTables[count] = dim;
					if (dim.TableName != null && dim.DimensionColumns != null && !dim.unmapped)
						dlist.add(dim);
				}
				this.DimensionTables = new DimensionTable[dlist.size()];
				dlist.toArray(this.DimensionTables);
			}
			else if (name == "Dimensions")
			{
				List l = child.getChildren();
				this.Dimensions = new Dimension[l.size()];
				for (int count=0;count<l.size();count++)
				{
					Dimension dm = new Dimension((Element)l.get(count),ns);
					this.Dimensions[count] = dm;
				}
			}
			else if (name == "Joins")
			{
				List l = child.getChildren();
				this.joins = new Join[l.size()];
				joinContainsVariable = false;
				for (int count = 0; count < l.size(); count++)
				{
					Join jn = new Join((Element) l.get(count), ns);
					if(!joinContainsVariable && containsVariable(jn.JoinCondition))
					{
						joinContainsVariable = true;
					}
					this.joins[count] = jn;
				}
			} else if (name == "Groups")
			{
				List l = child.getChildren();
				this.setGroups(new Group[l.size()]);
				repositoryGroups = new Group[l.size()];
				for (int count = 0; count < l.size(); count++)
				{
					Group g = new Group();
					Element ge = (Element) l.get(count);
					g.setName(ge.getChildTextTrim("Name", ns));
					g.setUserNames(getStringArrayListChildren(ge, "Usernames", ns));
					g.setFilters(getStringArrayListChildren(ge, "Filters", ns));
					this.getGroups()[count] = g;
					String internal = ge.getChildTextTrim("InternalGroup", ns);
					if (internal != null)
					{
						g.setInternalGroup(Boolean.parseBoolean(internal));
					}
					Group repoGroup = new Group(ge,ns);
					repositoryGroups[count] = repoGroup;
				}
			} else if (name == "Users")
			{
				List l = child.getChildren();
				this.setUsers(new ArrayList<User>());
				repositoryUsers = new User[l.size()];
				for (int count = 0; count < l.size(); count++)
				{
					User u = new User();
					Element ue = (Element) l.get(count);
					u.setUserName(ue.getChildTextTrim("Username", ns));
					String pass = ue.getChildTextTrim("Password", ns);
					// support the new format once the admin tool starts using it
					if ((pass == null) || pass.startsWith("$sha-1$"))
					{
						u.setHashedPassword(pass);
					} else
					{
						// decrypt the old format and hash the cleartext result
						// - this is for backwards compatibility
						EncryptionService ps = EncryptionService.getInstance();
						String decryptedpass = ps.decrypt(pass);
						u.setHashedPassword(ps.createDigest(ps.genSalt(), decryptedpass));
					}
					this.getUsers().add(u);
					User repUser = new User(ue,ns);
					repositoryUsers[count] = repUser;
				}
			} else if (name == "Variables")
			{
				List l = child.getChildren();
				this.variables = new Variable[l.size()];
				for (int count = 0; count < l.size(); count++)
				{
					this.variables[count] = new Variable((Element) l.get(count), ns);
				}
			} else if (name == "Resources")
			{
				List l = child.getChildren();
				this.resources = new HashMap<String, Resource>(DEFAULT_INITIAL_CAPACITY);
				rResources = new Resource[l.size()];
				for (int count = 0; count < l.size(); count++)
				{
					Resource re = new Resource();
					Element rre = (Element) l.get(count);
					re.setName(rre.getChildTextTrim("Name", ns));
					re.setType(rre.getChildTextTrim("Type", ns));
					re.setArea(rre.getChildTextTrim("Area", ns));
					re.setValue(rre.getChildTextTrim("Value", ns));
					re.setDescription(rre.getChildTextTrim("Description", ns));
					resources.put(re.getName(), re);
					rResources[count] = re;
				}
			} else if (name == "Operations")
			{
				List l = child.getChildren();
				this.operations = new Operation[l.size()];
				for (int count = 0; count < l.size(); count++)
				{
					Operation op = new Operation((Element) l.get(count), ns);
					this.operations[count] = op;
				}
			} else if (name == "ACL")
			{
				List l = child.getChildren();
				accessControlLists = new HashMap<String, List<ACLItem>>();
				repositoryACLItems = new ACLItem[l.size()];
				for (int count = 0; count < l.size(); count++)
				{
					ACLItem item = new ACLItem();
					Element ae = (Element) l.get(count);
					item.setGroup(ae.getChildTextTrim("Group", ns));
					item.setTag(ae.getChildTextTrim("Tag", ns));
					item.setAccess(Boolean.parseBoolean(ae.getChildTextTrim("Access", ns)));
					List<ACLItem> list = accessControlLists.get(item.getTag());
					if (list == null)
					{
						list = new ArrayList<ACLItem>();
						accessControlLists.put(item.getTag(), list);
					}
					list.add(item);
					repositoryACLItems[count] = item;
				}
			} else if (name == "SearchDefinitions")
			{
				List l = child.getChildren();
				this.searchDefinitions = new PatternSearchDefinition[l.size()];
				for (int count = 0; count < l.size(); count++)
				{
					this.searchDefinitions[count] = new PatternSearchDefinition((Element) l.get(count), ns);
				}
			} else if (name == "Filters")
			{
				List l = child.getChildren();
				this.filters = new QueryFilter[l.size()];
				for (int count = 0; count < l.size(); count++)
				{
					Element de = (Element) l.get(count);
					QueryFilter f = new QueryFilter(de, ns);
					this.filters[count] = f;
				}
			} else if (name == "Outcomes")
			{
				List l = child.getChildren();
				this.setOutcomes(new Outcome[l.size()]);
				for (int count = 0; count < l.size(); count++)
				{
					this.getOutcomes()[count] = new Outcome((Element) l.get(count), ns, this);
				}
			} else if (name == "SuccessModels")
			{
				List l = child.getChildren();
				this.successModels = new SuccessModel[l.size()];
				for (int count = 0; count < l.size(); count++)
				{
					this.successModels[count] = new SuccessModel((Element) l.get(count), ns);
				}
			} else if (name == "SubjectAreas")
			{
				List l = child.getChildren();
				this.setSubjectAreas(new SubjectArea[l.size() + (GenerateSubjectArea ? 1 : 0)]);
				for (int count = 0; count < l.size(); count++)
				{
					Element sae = (Element) l.get(count);
					SubjectArea sa = new SubjectArea(sae, ns);
					subjectAreas[count + (GenerateSubjectArea ? 1 : 0)] = sa;
				}
			} else if (name == "VirtualColumns")
			{
				List l = child.getChildren();
				this.virtualColumns = new VirtualColumn[l.size()];
				for (int count = 0; count < l.size(); count++)
				{
					this.virtualColumns[count] = new VirtualColumn((Element) l.get(count), ns);
				}
			} else if (name == "Hierarchies")
			{
				List l = child.getChildren();
				this.setHierarchies(new Hierarchy[l.size()]);
				boolean firstT = true;
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < l.size(); i++)
				{
					this.getHierarchies()[i] = new Hierarchy((Element) l.get(i), ns, this);
					if (this.getHierarchies()[i].DimensionKeyLevel == null || this.getHierarchies()[i].DimensionKeyLevel.equals(""))
					{
						if (firstT)
						{
							firstT = false;
							sb.append(this.getHierarchies()[i].DimensionName);
						} else
						{
							sb.append(", " + this.getHierarchies()[i].DimensionName);
						}
					}
				}
				String missingDimKeyLevels = sb.toString();
				if (!missingDimKeyLevels.equals(""))
				{
					logger.error("Dimension key level missing for the following dimensional hierarchies : " + missingDimKeyLevels
							+ "  - Please fix the repository.");
					throw new RepositoryException("Dimension key level missing for the following dimensional hierarchies : " + missingDimKeyLevels
							+ "  - Please fix the repository.");
				}
			} else if (name == "Aggregates")
			{
				List l = child.getChildren();
				this.aggregates = new Aggregate[l.size()];
				for (int i = 0; i < l.size(); i++)
				{
					Aggregate a = new Aggregate((Element) l.get(i), ns);
					if (a != null)
						this.aggregates[i] = a;
				}
			} else if (name == "StagingTables")
			{
				List l = child.getChildren();
				this.setStagingTables(new StagingTable[l.size()]);
				for (int count = 0; count < l.size(); count++)
				{
					StagingTable st = new StagingTable((Element) l.get(count), ns, this);
					st.r = this;
					this.getStagingTables()[count] = st;
				}
			} else if (name == "StagingFilters")
			{
				List l = child.getChildren();
				stFilters = new StagingFilter[l.size()];
				for (int count = 0; count < l.size(); count++)
				{
					StagingFilter sf = new StagingFilter((Element) l.get(count), ns);
					stFilters[count] = sf;
					stagingFilterMap.put(sf.Name, sf);
				}
			}
			else if (name == "UnmappedMeasureGrains")
      {
        List l = child.getChildren();
        UnmappedMeasureGrains = new MeasureGrain[l.size()];
        try
        {
          for (int count = 0;count < l.size() ; count++)
          {
            MeasureGrain mg = new MeasureGrain((Element) l.get(count), ns);
            UnmappedMeasureGrains[count] = mg;
          }
        }
        catch (Exception e)
        {           
          logger.error("Could not parse UnmappedMeasureGrains ");
        }          
      }
			else if (name == "ScriptGroups")
			{
				List l = child.getChildren();
				this.ScriptGroups = new ScriptGroup[l.size()];
				for (int count = 0; count < l.size(); count++)
				{
					ScriptGroup sg = new ScriptGroup((Element) l.get(count), ns);
					this.ScriptGroups[count] = sg;
					this.scriptGroupMap.put(sg.Name, sg);
				}
			} else if (name == "SourceFiles")
			{
				List l = child.getChildren();
				this.setSourceFiles(new SourceFile[l.size()]);
				for (int count = 0; count < l.size(); count++)
				{
					SourceFile sf = new SourceFile((Element) l.get(count), ns);
					this.getSourceFiles()[count] = sf;
				}
			} else if (name == "TimeDefinition")
			{
				this.setTimeDefinition(new TimeDefinition(child, ns));
			} else if (name == "Broadcasts")
			{
				List l = child.getChildren();
				this.broadcasts = new Broadcast[l.size()];
				for (int count = 0; count < l.size(); count++)
				{
					Broadcast bc = new Broadcast((Element) l.get(count), ns);
					this.broadcasts[count] = bc;
				}
			} else if (name == "LoadGroups")
			{
				List l = child.getChildren();
				this.loadGroups = new LoadGroup[l.size()];
				for (int i = 0; i < l.size(); i++)
				{
					LoadGroup lg = new LoadGroup((Element) l.get(i), ns);
					if (lg != null)
						this.loadGroups[i] = lg;
				}
			} else if (name == "CustomDrills")
			{
				List<Element> cds = child.getChildren("CustomDrill", ns);
				for (Element el : cds)
				{
					CustomDrill cd = new CustomDrill(el, ns);
					this.customDrills.add(cd);
				}
			} else if (name == "Imports")
			{
				List<Element> ids = child.getChildren("PackageImport", ns);
				for (Element el : ids)
				{
					if (Imports == null)
						Imports = new ArrayList<PackageImport>();
					Imports.add(new PackageImport(el, ns));
				}
			} else if (name == "QuickDashboards")
			{
				List<Element> qds = child.getChildren("QuickDashboard", ns);
				for (Element el : qds)
				{
					QuickDashboard dash = new QuickDashboard(el, ns);
					this.quickDashboards.add(dash);
				}
				/*
				 * Dashboard dash = new Dashboard(); Adhoc adhoc = Adhoc.getCurrentAdhoc(); UserBean userBean =
				 * UserBean.getUserBean(); // call this to be able to get the shared directory below.
				 * userBean.getRootDirectories(); String dashboardName = XmlUtils.getStringContent(child,
				 * "DashboardName", ns); String pageName = XmlUtils.getStringContent(child, "PageName", ns);
				 * dash.setName(dashboardName); dash.setPage(pageName); dash.setPath(userBean.getSharedRootDirectory());
				 * String fileName = dash.getFullName();
				 * 
				 * AdhocReport topReport = new AdhocReport();
				 * 
				 * List reports = child.getChildren("Reports", ns); for (Iterator<Element> reportIter =
				 * reports.iterator(); reportIter.hasNext(); ) { Element quickReport = reportIter.next(); AdhocReport
				 * report = new AdhocReport(quickReport, ns); adhoc.setReport(report);
				 * adhoc.setSaveReportPath(userBean.getSharedRootDirectory()); String reportName = report.getTitle();
				 * adhoc.setSaveReportName(reportName); try { adhoc.saveReport(); } catch (Exception e) { // TODO
				 * Auto-generated catch block e.printStackTrace(); } topReport.addSubreport(); List<AdhocEntity>
				 * entities = topReport.getEntities(); AdhocSubreport subreport =
				 * (AdhocSubreport)entities.get(entities.size() - 1); subreport.setSubreport(reportName); }
				 * 
				 * adhoc.setSaveReportName(fileName); adhoc.setReport(topReport); try { adhoc.saveReport(); } catch
				 * (Exception e) { // TODO Auto-generated catch block e.printStackTrace(); }
				 */
			}
			else if (name == "Expressions")
			{
				List<Element> lstExpressions = child.getChildren();
				logicalExpressions = new LogicalExpression[lstExpressions.size()];
				for (int count=0;count<lstExpressions.size();count++)
				{
					LogicalExpression logicalExp = new LogicalExpression(lstExpressions.get(count), ns);
					logicalExpressions[count] = logicalExp;
				}
			}
			else if (name == "DynamicGroups")
			{
				List l2 = child.getChildren();
				DynamicGroups = new String[l2.size()][];
				for (int count2 = 0; count2 < l2.size(); count2++)
				{
					Element l3 = (Element) l2.get(count2);
					DynamicGroups[count2] = new String[2];
					List dynamicGrpChildren = l3.getChildren();
					DynamicGroups[count2][0] = Util.intern(((Element) dynamicGrpChildren.get(0)).getValue());
					DynamicGroups[count2][1] = Util.intern(((Element) dynamicGrpChildren.get(1)).getValue());
				}
			}
			else if (name == "RServer")
			{
				RServer RServer = new RServer();
				temp = child.getChildText("URL", ns);
				if (temp != null)
					RServer.URL = temp;
				temp = child.getChildText("Port", ns);
				if (temp != null)
				{
					try
					{
						RServer.Port = Integer.parseInt(temp);
					} catch (NumberFormatException e)
					{
					}
				}
				EncryptionService es = EncryptionService.getInstance();
				temp = child.getChildText("Username", ns);
				if (temp != null)
					RServer.Username = es.decrypt(temp);
				temp = child.getChildText("Password", ns);
				if (temp != null)
					RServer.Password = es.decrypt(temp);
				this.RServer = RServer;
			}
		}
		initializeUsersAndGroups();
		setupStagingTables();
		// set input timezone for each sourcefiles
		setInputTimeZonesForSourceFiles(this.serverParameters.getProcessingTimeZone());
	}

	/**
	 * Initialize and fill data structure for users and groups from a separate file Per 4.3, user and group information
	 * is stored in a file called UsersAndGroups.xml A file listener on this file (defined in AC) will reload this data
	 * structure
	 */
	public void initializeUsersAndGroups()
	{
		String path = getServerParameters().getApplicationPath();
		String userAndGroupFileName = path + File.separator + Repository.USERS_GROUPS_FILE_NAME;
		File file = new File(userAndGroupFileName);
		if (!file.exists())
		{
			logger.warn(Repository.USERS_GROUPS_FILE_NAME + " file not found for the space path " + path + " . It is probably not created");
			return;
		}
		try
		{
			SAXBuilder builder = new SAXBuilder();
			Document d = builder.build(file);
			parseUsersAndGroups(d);
		} catch (Exception ex)
		{
			logger.error("Exception in loading user group file " + userAndGroupFileName, ex);
		}
	}
	
	public void setupStagingTables()
	{
		if (stagingTables == null || stagingTables.length == 0)
			return;
		DatabaseConnection dconn = this.getDefaultConnection();
		if (dconn == null || (!dconn.requiresHashedIdentifier()))
			return;
		for(StagingTable st : stagingTables)
		{
			st.replaceNameWithHashedIdentifier(dconn);
		}
	}

	@SuppressWarnings("rawtypes")
	public void parseUsersAndGroups(Document d)
	{
		Element root = d.getRootElement();
		List children = root.getChildren();
		Iterator iterator = children.iterator();
		Namespace ns = Namespace.getNamespace("http://www.successmetricsinc.com");
		while (iterator.hasNext())
		{
			Element child = (Element) iterator.next();
			String name = child.getName();
			if (name == "Groups")
			{
				List l = child.getChildren();
				this.setGroups(new Group[l.size()]);
				for (int count = 0; count < l.size(); count++)
				{
					Group g = new Group();
					Element ge = (Element) l.get(count);
					g.setName(ge.getChildTextTrim("Name", ns));
					g.setId(ge.getChildTextTrim("Id", ns));
					g.setUserNames(getStringArrayListChildren(ge, "Usernames", ns));
					this.getGroups()[count] = g;
					String internal = ge.getChildTextTrim("InternalGroup", ns);
					if (internal != null)
					{
						g.setInternalGroup(Boolean.parseBoolean(internal));
					}
				}
			} else if (name == "Users")
			{
				List l = child.getChildren();
				List<User> ulist = this.getUsers();
				if (ulist == null)
					this.setUsers(new ArrayList<User>());
				for (int count = 0; count < l.size(); count++)
				{
					User u = new User();
					Element ue = (Element) l.get(count);
					u.setUserName(ue.getChildTextTrim("Username", ns));
					String pass = ue.getChildTextTrim("Password", ns);
					// support the new format once the admin tool starts using
					// it
					if ((pass == null) || pass.startsWith("$sha-1$"))
					{
						u.setHashedPassword(pass);
					} else
					{
						// decrypt the old format and hash the cleartext result
						// - this is for backwards compatibility
						EncryptionService ps = EncryptionService.getInstance();
						String decryptedpass = ps.decrypt(pass);
						u.setHashedPassword(ps.createDigest(ps.genSalt(), decryptedpass));
					}
					this.getUsers().add(u);
				}
			} else if (name == "ACL")
			{
				List l = child.getChildren();
				accessControlLists = new HashMap<String, List<ACLItem>>();
				for (int count = 0; count < l.size(); count++)
				{
					ACLItem item = new ACLItem();
					Element ae = (Element) l.get(count);
					item.setGroup(ae.getChildTextTrim("Group", ns));
					item.setTag(ae.getChildTextTrim("Tag", ns));
					item.setAccess(Boolean.parseBoolean(ae.getChildTextTrim("Access", ns)));
					List<ACLItem> list = accessControlLists.get(item.getTag());
					if (list == null)
					{
						list = new ArrayList<ACLItem>();
						accessControlLists.put(item.getTag(), list);
					}
					list.add(item);
				}
			}
		}
	}

	private void overrideDatabaseConnection(DatabaseConnection dbc)
	{
		String ConnectString = null;
		String UserName = null;
		String Password = null;
		String Schema = null;
		String Realtime = null;
		String Disabled = null;
		if (overrides == null)
			return;
		
		if(dbc.Realtime)
		{
			String bypassLiveAccessStr = overrides.getProperty("UseDirectDBConnectionForLiveAccessQueries");
			if(bypassLiveAccessStr != null)
			{
				boolean bypassLiveAccess = Boolean.valueOf(bypassLiveAccessStr);
				if(bypassLiveAccess && dbc.useDirectConnection)
				{
					logger.info("'UseDirectDBConnectionForLiveAccessQueries' property is set to true. Will use direct db connection for live access queries for connection '" + dbc.Name + "'");					
				}
				else if (!bypassLiveAccess)
				{
					dbc.useDirectConnection = false;					
				}
			}
		}
		try
		{
			String name = java.net.URLEncoder.encode("DatabaseConnection." + dbc.Name + ".ConnectString", "UTF-8");
			ConnectString = overrides.getProperty(name);
			name = java.net.URLEncoder.encode("DatabaseConnection." + dbc.Name + ".UserName", "UTF-8");
			UserName = overrides.getProperty(name);
			name = java.net.URLEncoder.encode("DatabaseConnection." + dbc.Name + ".Password", "UTF-8");
			Password = overrides.getProperty(name);
			name = java.net.URLEncoder.encode("DatabaseConnection." + dbc.Name + ".Schema", "UTF-8");
			Schema = overrides.getProperty(name);
			name = java.net.URLEncoder.encode("DatabaseConnection." + dbc.Name + ".Realtime", "UTF-8");
			Realtime = overrides.getProperty(name);
			name = java.net.URLEncoder.encode("DatabaseConnection." + dbc.Name + ".Disabled", "UTF-8");
			Disabled = overrides.getProperty(name);
		} catch (Exception ex)
		{
			logger.error("DatabaseConnection override for '" + dbc.Name + "' failed, using repository.xml values");
			return;
		}
		if (ConnectString != null)
		{
			dbc.ConnectString = ConnectString.trim();
			logger.debug("Overriding '" + dbc.Name + "' ConnectString with: '" + dbc.ConnectString + "'");
		}
		if (UserName != null)
		{
			dbc.UserName = UserName.trim();
			logger.debug("Overriding '" + dbc.Name + "' UserName with: '" + dbc.UserName + "'");
		}
		if (Password != null)
		{
			dbc.Password = Password.trim();
			logger.debug("Overriding '" + dbc.Name + "' Password with: '" + dbc.Password + "'");
		}
		if (Schema != null)
		{
			dbc.Schema = Schema.trim();
			logger.debug("Overriding '" + dbc.Name + "' Schema with: '" + dbc.Schema + "'");
		}
		if (Realtime != null)
		{
			dbc.Realtime = Boolean.valueOf(Realtime);
			logger.debug("Overriding '" + dbc.Name + "' Realtime with: '" + dbc.Realtime + "'");
		}
		if (Disabled != null)
		{
			dbc.Disabled = Boolean.valueOf(Disabled);
			logger.debug("Overriding '" + dbc.Name + "' Disabled with: '" + dbc.Disabled + "'");
		}
	}

	private void overrideDefaultConnection()
	{
		String DefaultConnection = null;
		if (overrides == null)
			return;
		try
		{
			String name = java.net.URLEncoder.encode("DefaultConnection", "UTF-8");
			DefaultConnection = overrides.getProperty(name);
		} catch (Exception ex)
		{
			logger.error("DatabaseConnection override for DefaultConnection failed, using repository.xml values");
			return;
		}
		if (DefaultConnection != null)
		{
			this.setDefaultConnection(DefaultConnection);
			logger.debug("Overriding default connection with: '" + DefaultConnection + "'");
		}
	}
	
	private void overrideServerParameters()
	{
		if (overrides == null)
			return;
		String temp = overrides.getProperty("ApplicationPath");
		if (temp != null && temp.length() > 0)
		{
			this.getServerParameters().setApplicationPath(temp.trim());
			logger.debug("Overriding ApplicationPath with: '" + this.getServerParameters().getApplicationPath() + "'");
		}
		temp = overrides.getProperty("CatalogPath");
		if (temp != null && temp.length() > 0)
		{
			this.getServerParameters().setCatalogPath(temp.trim());
			logger.debug("Overriding CatalogPath with: '" + this.getServerParameters().getCatalogPath() + "'");
		}
		/*
		 * now driven by the SPACES table temp = overrides.getProperty("MaxRecords"); if (temp != null && temp.length()
		 * > 0) { this.getServerParameters().setMaxRecords(Integer.parseInt(temp.trim()));
		 * logger.info("Overriding MaxRecords with: '" + this.getServerParameters().getMaxRecords() + "'"); } temp =
		 * overrides.getProperty("MaxQueryTime"); if (temp != null && temp.length() > 0) {
		 * this.getServerParameters().setMaxQueryTime(Integer.parseInt(temp.trim()));
		 * logger.info("Overriding MaxQueryTime with: '" + this.getServerParameters().getMaxQueryTime() + "'"); }
		 */
		temp = overrides.getProperty("ProxyDBConnectionString");
		if (temp != null && temp.length() > 0)
		{
			this.getServerParameters().setProxyDBConnectionString(temp.trim());
			logger.debug("Overriding ProxyDBConnectionString with: '" + this.getServerParameters().getProxyDBConnectionString() + "'");
		}
		temp = overrides.getProperty("ProxyDBUserName");
		if (temp != null && temp.length() > 0)
		{
			this.getServerParameters().setProxyDBUserName(temp.trim());
			logger.debug("Overriding ProxyDBUserName with: '" + this.getServerParameters().getProxyDBUserName() + "'");
		}
		temp = overrides.getProperty("ProxyDBPassword");
		if (temp != null)
		{
			this.getServerParameters().setProxyDBPassword(temp);
			logger.debug("Overriding ProxyDBPassword with new value...");
		}
		temp = overrides.getProperty("UseCacheForRepositoryInitialization");
		if(temp != null && temp.toLowerCase().equals("false"))
		{
			UseCacheForRepositoryInitialization = false;
			logger.debug("Overriding UseCacheForRepositoryInitialization to false - will not instantiate cache for repository intialization.");
		}
	}

	/**
	 * Process all inherited columns
	 */
	private void processInheritedColumns() throws CloneNotSupportedException
	{
		processInheritedMeasureTables(false);
		processInheritedDimensionTables();
		processInheritedVirtualMeasures(false);
		processInheritedSuccessModels();
	}

	/**
	 * Create compound table sources to reflect snowflake relationships
	 */
	private void processSnowFlakes() throws CloneNotSupportedException
	{
		/*
		 * First, identify all sets of dimension tables that are joined together
		 */
		List<DimensionTable[]> setList = new ArrayList<DimensionTable[]>();
		for (Join j : joins)
		{
			if ((!j.Invalid) && j.Table1 != null && j.Table2 != null)
			{
				if (DimensionTable.class.isInstance(j.Table1) && DimensionTable.class.isInstance(j.Table2))
				{
					DimensionTable[] s = new DimensionTable[2];
					s[0] = (DimensionTable) j.Table1;
					s[1] = (DimensionTable) j.Table2;

					//Allow for manually defined joins across different dimensions to cause snowflakes
					if((currentBuilderVersion >= 15) && (!j.AutoGenerated) && (!s[0].DimensionName.equals(s[1].DimensionName)))
					{
						s[0].SnowflakeAllDimensions = true;
						s[1].SnowflakeAllDimensions = true;
					}

					//Either both tables should be on the same dimension or app builder has instructed to create snowflakes on all dimensions
					if (s[0].DimensionName.equals(s[1].DimensionName) || ((DimensionTable) j.Table1).SnowflakeAllDimensions
							|| ((DimensionTable) j.Table2).SnowflakeAllDimensions)
					{
						setList.add(s);
					}
				}
			}
		}
		/*
		 * For each set of dimension tables that are joined, generate all possible join combinations in a sorted set.
		 * The set is sorted from smallest to largest. This way table sources with 2 physical tables will be chosen
		 * ahead of table sources with 3 or more
		 */
		Set<Set<DimensionTable>> combinations = new LinkedHashSet<Set<DimensionTable>>();
		genTableCombinations(setList, combinations);
		List<Set<DimensionTable>> comboList = new ArrayList<Set<DimensionTable>>();
		comboList.addAll(combinations);
		Collections.sort(comboList, new CompareCombinations());
		/*
		 * Generate a compound table source for each combination
		 */
		for (Set<DimensionTable> set : comboList)
		{
			createSnowflake(set);
		}
	}

	/**
	 * Create a snowflake dimension table given a set of dimension tables that are joined together
	 * 
	 * @param set
	 * @return The new DimensionTable snowflake object
	 */
	public List<DimensionTable> createSnowflake(Set<DimensionTable> set) throws CloneNotSupportedException
	{
		List<DimensionTable> result = new ArrayList<DimensionTable>();
		if (set.size() < 2)
			return (null);
		int maxSnowflakeSize = serverParameters.getMaxSnowflakeSize();
		if (maxSnowflakeSize > 0 && set.size() > maxSnowflakeSize)
			return (null);
		String name = "";
		
		// Moving the isImported logic out of later iteration
		boolean isImported = false;
		for(DimensionTable dimTable : set){
			if(dimTable.Imported)
			{
				isImported = true;
				break;
			}
		}
		List<String> dnameList = null;
		boolean validSet = true;
		boolean cacheable = true;
		int ttl = -1;
		DatabaseConnection dcon = null;
		Level l = null;
		int tableCount = 0;
		Set<String> dimensions = new HashSet<String>();
		for (DimensionTable dt : set)
		{
			name += dt.TableName + "_";
			dimensions.add(dt.DimensionName);
			if (dnameList == null)
			{
				dnameList = new ArrayList<String>();
				dnameList.add(dt.DimensionName);
			} else if (dt.SnowflakeAllDimensions && (!dnameList.contains(dt.DimensionName)))
			{
				dnameList.add(dt.DimensionName);
			} else if (!dnameList.contains(dt.DimensionName) && !dt.SnowflakeAllDimensions)
			{
				// now that we only put tables in the same dimension into the sets, this should never happen
				validSet = false;
				logger.fatal("Cannot create snowflake for table set: " + set + " - not in same dimension");
				break;
			}
			if (!dt.Cacheable)
				cacheable = false;
			if (dt.TTL != -1)
			{
				if (ttl == -1)
					ttl = dt.TTL;
				else
					ttl = Math.min(ttl, dt.TTL);
			}
			if (dcon == null)
				dcon = dt.TableSource.connection;
			else if(!matchDBConnectString(dt.TableSource.connection, dcon, isImported))
			{
				validSet = false;
				logger.error("Cannot create snowflake for table set: " + set + " - not using same connection");
				break;
			}
			Level dl = findDimensionLevel(dt.DimensionName, dt.Level);
			if (l == null)
				l = dl;
			else
			/* If level is below the level of the previous tables, choose it (lowest level chosen) */
			if (l.findLevel(dl) != null && l != dl)
				l = dl;
			tableCount += dt.Type == DimensionTable.OPAQUE_VIEW ? 1 : dt.TableSource.Tables.length;
		}
		if (validSet)
		{
			String dn = dnameList.get(0);
			logger.debug("Creating snowflake: " + set);
			// Create a new dimension table
			DimensionTable newdt = new DimensionTable(this);
			newdt.TableName = name;
			newdt.Imported = isImported;
			newdt.DimensionName = dn;
			newdt.Type = 0;
			newdt.Cacheable = cacheable;
			newdt.TTL = ttl;
			newdt.snowflake = true;
			newdt.NumSnowflakeDimensions = dimensions.size();
			if (l != null)
				newdt.Level = l.Name;
			/*
			 * Create the compound table source
			 */
			newdt.TableSource = new TableSource();
			newdt.TableSource.connection = dcon;
			newdt.TableSource.ConnectionName = dcon.Name;
			newdt.TableSource.Tables = new TableDefinition[tableCount];
			List<DimensionTable> processed = new ArrayList<DimensionTable>();
			int count = 0;
			List<DimensionColumn> colList = new ArrayList<DimensionColumn>();
			List<String[]> joinReplacements = new ArrayList<String[]>();
			List<TableDefinition> bridgeTables = new ArrayList<TableDefinition>();
			int sz = set.size();
			for (int loop = 0; loop < sz; loop++)
			{
				/*
				 * Need to pick a dimension table that joins with one of the already processed tables in this set
				 */
				DimensionTable dt = null;
				DimensionTable joineddt = null;
				Join j = null;
				String synonym = null;
				for (DimensionTable setdt : set)
				{
					// If none have been processed, take the first one
					if (processed.isEmpty())
					{
						dt = setdt;
						break;
					}
					if (!processed.contains(setdt))
					{
						/*
						 * Find the first join to any previously processed dimension table and add to this
						 * definition (no need to look at all joins because they would be redundant)
						 */
						for (DimensionTable processeddt : processed)
						{
							j = getJoin(setdt.TableName, processeddt.TableName);
							if (j != null)
							{
								joineddt = processeddt;
								/*
								 * If the specified join is not symmetric (i.e. LEFT or RIGHT OUTER JOIN) and the
								 * order doesn't match the existing order here, then need to flip the join
								 */
								if (j.Type == JoinType.LeftOuter || j.Type == JoinType.RightOuter)
								{
									if (j.Table1 != processeddt)
									{
										j = (Join) j.clone();
										if (j.Type == JoinType.LeftOuter)
											j.Type = JoinType.RightOuter;
										else
											j.Type = JoinType.LeftOuter;
									}
								}
								/*
								 * If this is a self-join based on single tables, use a synonym to rename
								 */
								if (setdt.TableSource.Tables.length == 1 && joineddt.TableSource.Tables.length == 1 
										&& setdt.TableSource.Tables[0].PhysicalName.equals(joineddt.TableSource.Tables[0].PhysicalName)
										&& setdt.Type == DimensionTable.NORMAL && joineddt.Type == DimensionTable.NORMAL)
								{
									synonym = createSynonym(setdt.TableSource.Tables[0].PhysicalName);
								}
								break;
							}
						}
						if (j != null)
						{
							dt = setdt;
							break;
						}
					}
				}
				
				for (DimensionColumn dc : dt.DimensionColumns)
				{
					DimensionColumn newdc = (DimensionColumn) dc.clone();
					newdc.TableName = newdt.TableName;
					newdc.DimensionTable = newdt;
					if (synonym != null)
						newdc.PhysicalName = Util.replaceStr(newdc.PhysicalName, dt.TableSource.Tables[0].PhysicalName, synonym);
					if (dt.SnowflakeAllDimensions && (!dt.DimensionName.equals(newdt.DimensionName)))
					{
						newdc.overrideDimensionName = dt.DimensionName;
					}
					DimensionColumn founddc = null;
					// Don't add duplicates
					for (DimensionColumn testdc : colList)
					{
						if (testdc.ColumnName.equals(newdc.ColumnName))
						{
							founddc = testdc;
							break;
						}
					}
					/* Don't add virtual columns if they are not virtual in another table in the snoowflake */
					if (founddc == null && dc.VC != null)
					{
						for (DimensionTable vcdt : set)
						{
							if (vcdt != dt)
							{
								for (DimensionColumn vcdc : vcdt.DimensionColumns)
								{
									if (vcdc.ColumnName.equals(dc.ColumnName) && vcdc.VC == null)
									{
										founddc = vcdc;
										break;
									}
								}
							}
							if (founddc != null)
								break;
						}
					}
					if (founddc == null)
					{
						colList.add(newdc);
					}
					else if(currentBuilderVersion >= 15)
					{
						if(!founddc.matchesDimensionName(newdc))
						{
							colList.add(newdc);
						}
					}
				}
				/*
				 * If this is an opaque view, setup as such
				 */
				if (dt.Type == DimensionTable.OPAQUE_VIEW)
				{
					TableDefinition newtd = new TableDefinition();
					newtd.OpaqueView = dt.OpaqueView;
					newtd.PhysicalName = dt.getPhysicalName();
					if (dt.TableSource.Schema != null && dt.TableSource.Schema.length() > 0)
						newtd.Schema = dt.TableSource.Schema;
					newdt.TableSource.Tables[count++] = newtd;
					processJoin(newtd, dt, joineddt, j, synonym);
					String[] replacement = null;
					if (count == 0)
						replacement = new String[]
						{ "\"" + dt.TableName + "\"", "\"" + newdt.TableName + "\"" };
					else
						replacement = new String[]
						{ "\"" + dt.TableName + "\"", "\"" + newdt.TableName + "\"." + dt.getPhysicalName() };
					joinReplacements.add(replacement);
				} else
				{
					// copy all the security filters from dt to the compound table
					if (dt.TableSource != null && dt.TableSource.Filters != null)
					{
						List<TableSourceFilter> filtersList = new ArrayList<TableSourceFilter>();
						for (TableSourceFilter tsf : dt.TableSource.Filters)
						{
							// if (tsf.SecurityFilter)
							filtersList.add(tsf);
						}
						if (newdt.TableSource.Filters != null)
						{
							for (TableSourceFilter tsf : newdt.TableSource.Filters)
							{
								filtersList.add(tsf);
							}
						}
						if (!filtersList.isEmpty())
						{
							newdt.TableSource.Filters = new TableSourceFilter[filtersList.size()];
							filtersList.toArray(newdt.TableSource.Filters);
						}
					}
					for (TableDefinition td : dt.TableSource.Tables)
					{
						TableDefinition newtd = (TableDefinition) td.clone();
						newtd.OverrideDimension = dt.DimensionName;
						newtd.TableName= dt.TableName;
						if (newtd.Schema == null && dt.TableSource.Schema != null)
							newtd.Schema = dt.TableSource.Schema;
						if (synonym != null)
							newtd.PhysicalName = synonym;
						/*
						 * If this is the first table in anything beyond the first table source, then create a new
						 * join relationship
						 */
						if (!processed.isEmpty() && td == dt.TableSource.Tables[0])
						{
							String[] bridges = processJoin(newtd, dt, joineddt, j, synonym);
							/*
							 * Create table definitions for any bridge tables
							 */
							if (bridges != null)
							{
								for (String s : bridges)
								{
									TableDefinition btd = new TableDefinition();
									btd.JoinType = JoinType.Inner;
									btd.PhysicalName = s;
									btd.TableName = dt.TableName;
									bridgeTables.add(btd);
								}
							}
						}
						String[] replacement = null;
						if (count == 0)
						{
							replacement = new String[]
							{ "\"" + dt.TableName + "\"", "\"" + newdt.TableName + "\"" };
							joinReplacements.add(replacement);
							for (int i = 0; i < dt.TableSource.Tables.length; i++)
							{
								replacement = new String[]
								{ "\"" + dt.TableName + "\"." + dt.TableSource.Tables[i].PhysicalName,
										"\"" + newdt.TableName + "\"." + dt.TableSource.Tables[i].PhysicalName };
								joinReplacements.add(replacement);
							}
						} else
						{
							replacement = new String[]
							{ "\"" + dt.TableName + "\"", "\"" + newdt.TableName + "\"." + dt.TableSource.Tables[0].PhysicalName };
							joinReplacements.add(replacement);
							for (int i = 0; i < dt.TableSource.Tables.length; i++)
							{
								replacement = new String[]
								{ "\"" + dt.TableName + "\"." + dt.TableSource.Tables[i].PhysicalName,
										"\"" + newdt.TableName + "\"." + dt.TableSource.Tables[i].PhysicalName };
								joinReplacements.add(replacement);
							}
						}
						if (dt.TableSource.Schema != null && dt.TableSource.Schema.length() > 0)
							newtd.Schema = dt.TableSource.Schema;
						
						if(isImported && newtd.Schema == null && dt.TableSource.connection != null && dt.TableSource.connection.Schema != null)
							newtd.Schema = dt.TableSource.connection.Schema;
						newdt.TableSource.Tables[count++] = newtd;
					}
				}
				processed.add(dt);
			}
			/*
			 * Process any bridge tables
			 */
			if (!bridgeTables.isEmpty())
			{
				TableDefinition[] newTables = new TableDefinition[newdt.TableSource.Tables.length + bridgeTables.size()];
				for (int i = 0; i < bridgeTables.size(); i++)
					newTables[i] = bridgeTables.get(i);
				for (int i = 0; i < newdt.TableSource.Tables.length; i++)
					newTables[bridgeTables.size() + i] = newdt.TableSource.Tables[i];
				newdt.TableSource.Tables = newTables;
			}
			/*
			 * Create new lists of dimension tables and joins (in order to expand them)
			 */
			List<Join> jlist = new ArrayList<Join>();
			/*
			 * Now, duplicate any joins to measures that need to be added to this new dimension table. Go through
			 * the list of all measure tables and find the first join to any of the dimension tables in this set
			 * (others would be redundant) and add it as a new join to this new compound table. Make sure that if
			 * there is a non-redundant join, it is used. If one of the dimension tables has a redundant join and
			 * that is used, it will not join properly when columns from other dimension tables in the set are
			 * included that do not have redundant joins with the measure table.
			 * 
			 * In the case where the different tables in the set are at different levels, take only joins to the
			 * lowest level tables.
			 */
			Level joinLevel = null;
			Hierarchy h = findDimensionHierarchy(newdt.DimensionName);
			if (h != null)
				for (DimensionTable dt : set)
				{
					if (dt.Level != null)
					{
						Level jl = h.findLevel(dt.Level);
						if (joinLevel == null)
							joinLevel = jl;
						else if (joinLevel != jl && joinLevel.findLevel(jl) != null)
							joinLevel = jl;
					}
				}
			for (MeasureTable mt : MeasureTables)
			{
				/*
				 * See if it's OK to have a redundant join
				 */
				boolean redundantOK = true;
				for (DimensionTable dt : set)
				{
					Join j = getJoin(mt.TableName, dt.TableName);
					if (j != null)
					{
						if (!j.Redundant)
						{
							redundantOK = false;
							break;
						}
					} else
					{
						/*
						 * If no redundant join is found for one of the dimension tables, then a redundant join
						 * cannot be used
						 */
						redundantOK = false;
						break;
					}
				}
				for (DimensionTable dt : set)
				{
					if (joinLevel == null || joinLevel.Name.equals(dt.Level) || dt.SnowflakeAllDimensions)
					{
						Join j = getJoin(mt.TableName, dt.TableName);
						if (j != null && (!j.Redundant || redundantOK))
						{
							Join newJoin = (Join) j.clone();
							/*
							 * Replace any references to the original dimension table with ones to the new compound
							 * table
							 */
							for (int i = joinReplacements.size() - 1; i >= 0; i--)
							{
								if (newJoin.JoinCondition != null)
								{
									newJoin.JoinCondition = newJoin.JoinCondition.replace(joinReplacements.get(i)[0], joinReplacements.get(i)[1]);
								}
							}
							if (newJoin.Table1 == dt)
							{
								newJoin.Table1 = newdt;
								newJoin.Table1Str = newdt.TableName;
							} else if (newJoin.Table2 == dt)
							{
								newJoin.Table2 = newdt;
								newJoin.Table2Str = newdt.TableName;
							}
							jlist.add(newJoin);
							break;
						}
					}
				}
			}
			/*
			 * Add the new dimension table to the list of dimension tables
			 */
			newdt.DimensionColumns = new DimensionColumn[colList.size()];
			colList.toArray(newdt.DimensionColumns);
			addDimensionTable(newdt);
			addJoins(jlist);
			result.add(newdt);				
		}
		return result;
	}
	
	// This was re-factored to accommodate the difference in database connection for imported tables
	// checkForConnectString is true for imported dba child space. In that case we check for the actual connection properties
	// to see if they are effectively same db. 
	// If checkForConnectString is false, use the old logic of comparing direct connection objects -- Minimal change
	private boolean matchDBConnectString(DatabaseConnection dc, DatabaseConnection dconn, boolean checkForConnectString)
	{
		// return true if it is the same object
		if(dc == dconn)
			return true;
		
		if (checkForConnectString && 
				dc != null && dconn != null && 
				dc.ConnectString != null && dconn.ConnectString != null &&
				dc.UserName != null && dconn.UserName != null && 
				dc.Password != null && dconn.Password != null &&
				dc.ConnectString.equals(dconn.ConnectString) && dc.UserName.equals(dconn.UserName) && 
				dc.Password.equals(dconn.Password))
		{
				return true;
		}
		
		return false;
	}

	/**
	 * Process a join in a table definition. Return any additional bridge tables present.
	 * 
	 * @param newtd
	 * @param dt
	 * @param joineddt
	 * @param j
	 * @return
	 */
	private String[] processJoin(TableDefinition newtd, DimensionTable dt, DimensionTable joineddt, Join j, String synonym)
	{
		if (j != null)
		{
			newtd.JoinType = j.Type;
			newtd.JoinClause = j.JoinCondition;
			/*
			 * Replace the logical table names with the appropriate physical ones
			 */
			for (TableDefinition jtd : dt.TableSource.Tables)
			{
				newtd.JoinClause = newtd.JoinClause.replace("\"" + dt.TableName + "\"." + jtd.PhysicalName + ".", jtd.PhysicalName + ".");
			}
			for (TableDefinition jtd : joineddt.TableSource.Tables)
			{
				newtd.JoinClause = newtd.JoinClause.replace("\"" + joineddt.TableName + "\"." + jtd.PhysicalName + ".", jtd.PhysicalName + ".");
			}
			String pname1 = synonym != null ? synonym : dt.getPhysicalName();
			String pname2 = joineddt.getPhysicalName();
			newtd.JoinClause = newtd.JoinClause.replace("\"" + dt.TableName + "\"", pname1);
			newtd.JoinClause = newtd.JoinClause.replace("\"" + joineddt.TableName + "\"", pname2);
			return (j.BridgeTables);
		}
		return (null);
	}
	
	private String createSynonym(String name)
	{
		String value = "|SYN" + (synonyms == null ? 0 : synonyms.size()) + System.currentTimeMillis();
		if (synonyms!=null)
		{
			synonyms.put(name, value);
		}
		return value;
	}
	
	public String replaceSynonyms(String s)
	{
		if (synonyms != null) {
			for(Entry<String,String> en: synonyms.entrySet())
			{
				s = Util.replaceStr(s, en.getValue(), en.getKey());
			}
		}
		return s;
	}

	private static class CompareCombinations implements Comparator<Set<DimensionTable>>
	{
		public int compare(Set<DimensionTable> set1, Set<DimensionTable> set2)
		{
			return (set1.size() - set2.size());
		}
	}
	/**
	 * build and maintain a temporary hash map for quick join lookups
	 */
	private Map<String, Set<Join>> tempJoinMap = null;
	private Map<String, List<Join>> tempJoinHashMap;
	private Map<String, Join> tempJoinedTables = null; // hash set keys by tableA / tableB, for quick 'is tableA

	// joined to tableB'
	private void invalidateTempStructures()
	{
		tempJoinHashMap = null;
		tempJoinMap = null;
	}

	public List<Join> getJoinList(String table)
	{
		buildTempJoinHashMap();
		return tempJoinHashMap.get(table);
	}
	
	public Join[] getJoins()
	{
		return joins;
	}

	public void setJoins(Join[] joins)
	{
		this.joins = joins;
	}

	private void addToTempJoinHashMap(Join j)
	{
		buildTempJoinHashMap();
		String key1 = j.Table1Str;
		List<Join> val1 = tempJoinHashMap.get(key1);
		if (val1 == null)
		{
			val1 = new ArrayList<Join>(10);
			tempJoinHashMap.put(key1, val1);
		}
		val1.add(j);
		String key2 = j.Table2Str;
		List<Join> val2 = tempJoinHashMap.get(key2);
		if (val2 == null)
		{
			val2 = new ArrayList<Join>(10);
			tempJoinHashMap.put(key2, val2);
		}
		val2.add(j);
	}

	private void buildTempJoinHashMap()
	{
		if (tempJoinHashMap == null)
		{
			tempJoinHashMap = new HashMap<String, List<Join>>(joins.length);
			for (Join j : joins)
			{
				addToTempJoinHashMap(j);
			}
		}
	}

	private void updateTempStructures(Join j)
	{
		addToTempJoinHashMap(j);
		addToOther(j);
	}

	private void addToOther(Join j)
	{
		buildOther();
		Set<Join> jlist = tempJoinMap.get(j.Table1Str);
		if (jlist == null)
		{
			jlist = new LinkedHashSet<Join>(10);
			tempJoinMap.put(j.Table1Str, jlist);
		}
		if (!jlist.contains(j))
			jlist.add(j);
		jlist = tempJoinMap.get(j.Table2Str);
		if (jlist == null)
		{
			jlist = new LinkedHashSet<Join>(10);
			tempJoinMap.put(j.Table2Str, jlist);
		}
		if (!jlist.contains(j))
			jlist.add(j);
		tempJoinedTables.put(buildSuperKey(j.Table1Str, j.Table2Str), j);
	}

	private void buildOther()
	{
		if (tempJoinMap == null)
		{
			tempJoinMap = new HashMap<String, Set<Join>>(joins.length);
			tempJoinedTables = new HashMap<String, Join>(joins.length);
			for (Join j : joins)
			{
				addToOther(j);
			}
		}
	}

	/**
	 * key used for quick determination of two tables are joined
	 * 
	 * @param table1
	 * @param table2
	 * @return
	 */
	private String buildSuperKey(String table1, String table2)
	{
		if (table1.compareTo(table2) < 0)
		{
			return table1 + '/' + table2;
		} else
		{
			return table2 + '/' + table1;
		}
	}

	/**
	 * Find the join between two tables
	 * 
	 * @param table1
	 * @param table2
	 * @return
	 */
	private Join getJoin(String table1, String table2)
	{
		buildOther();
		String key = buildSuperKey(table1, table2);
		if (!tempJoinedTables.containsKey(key))
			return null;
		else
			return tempJoinedTables.get(key);
	}

	private void genTableCombinations(List<DimensionTable[]> list, Set<Set<DimensionTable>> combinations)
	{
		int maxSnowflakeSize = serverParameters.getMaxSnowflakeSize();
		/*
		 * If there are more than 50 snowflake joins, then limit to a depth of 2
		 */
		if (list.size() > 50)
		{
			logger.info("More than 50 snow flakes to process, so limiting the maximum allowed depth of each snow flake to 2. Repository specified depth is:"
					+ maxSnowflakeSize);
			maxSnowflakeSize = 2;
		}
		/*
		 * Start with a list of pairs
		 */
		for (DimensionTable[] dtarr : list)
		{
			Set<DimensionTable> s = new LinkedHashSet<DimensionTable>();
			s.addAll(Arrays.asList(dtarr));
			combinations.add(s);
		}
		/*
		 * Now, try to enlarge each set
		 */
		for (int i = 2; i <= list.size(); i++)
		{
			List<Set<DimensionTable>> newSets = new ArrayList<Set<DimensionTable>>();
			for (Set<DimensionTable> s : combinations)
			{
				if (maxSnowflakeSize > 0 && s.size() >= maxSnowflakeSize)
				{
					continue;
				}
				if (s.size() == i)
				{
					// get the dimension name
					DimensionTable[] dimTable = new DimensionTable[i];
					s.toArray(dimTable);
					String dimName = dimTable[0].DimensionName;
					for (Set<DimensionTable> s2 : combinations)
					{
						if (s2.size() == 2)
						{
							DimensionTable[] dtarr = new DimensionTable[2];
							s2.toArray(dtarr);
							if (!dimName.equals(dtarr[0].DimensionName) && !dtarr[0].SnowflakeAllDimensions)
								continue; // snowflakes are only within dimensions
							if (s.contains(dtarr[0]))
							{
								Set<DimensionTable> newSet = new LinkedHashSet<DimensionTable>(s);
								newSet.add(dtarr[1]);
								newSets.add(newSet);
							} else if (s.contains(dtarr[1]))
							{
								Set<DimensionTable> newSet = new LinkedHashSet<DimensionTable>(s);
								newSet.add(dtarr[0]);
								newSets.add(newSet);
							}
						}
					}
				}
			}
			combinations.addAll(newSets);
		}
	}

	/**
	 * Add a join to the hashed joinmap
	 * 
	 * @param j
	 */
	private void addJoinToMaps(Join j)
	{
		if ((j.Table1 != null) && (j.Table2 != null))
		{
			List<Object> olist = joinTableMap.get(j.Table1);
			List<Join> jlist = joinMap.get(j.Table1);
			if (olist == null)
			{
				olist = new ArrayList<Object>();
				joinTableMap.put(j.Table1, olist);
				jlist = new ArrayList<Join>();
				joinMap.put(j.Table1, jlist);
			}
			olist.add(j.Table2);
			jlist.add(j);
			olist = joinTableMap.get(j.Table2);
			jlist = joinMap.get(j.Table2);
			if (olist == null)
			{
				olist = new ArrayList<Object>();
				joinTableMap.put(j.Table2, olist);
				jlist = new ArrayList<Join>();
				joinMap.put(j.Table2, jlist);
			}
			olist.add(j.Table1);
			jlist.add(j);
		}
	}

	private void removeJoinFromMaps(Join j)
	{
		if ((j.Table1 != null) && (j.Table2 != null))
		{
			List<Object> olist = joinTableMap.get(j.Table1);
			List<Join> jlist = joinMap.get(j.Table1);
			if (olist != null)
				olist.remove(j.Table2);
			if (jlist != null)
				jlist.remove(j);
			olist = joinTableMap.get(j.Table2);
			jlist = joinMap.get(j.Table2);
			if (olist != null)
				olist.remove(j.Table1);
			if (jlist != null)
				jlist.remove(j);
		}
	}

	/**
	 * Evaluate all columns in all subject areas to determine allowable join relationships
	 */
	private void processJoinRelationships()
	{
		/*
		 * First, create a map of all join relationships
		 */
		for (Join j : joins)
		{
			addJoinToMaps(j);
		}
	}

	/**
	 * Calculate possible join relationships in subject areas
	 */
	private void processSubjectAreaJoins()
	{
		synchronized (this)
		{
			if (this.processedSAJoins)
				return;
			this.processedSAJoins = true;
			/*
			 * Now add join relationships to the subject areas
			 */
			for (SubjectArea sa : getSubjectAreas())
			{
				for (Folder f : sa.getFolders())
				{
					processSubjectAreaFolderJoins(f);
				}
			}
		}
	}

	private void processSubjectAreaFolderJoins(Folder f)
	{
		boolean forceCrossJoins = serverParameters.isForceDimensionCrossJoinsThroughFacts();
		if (f.getColumns() != null)
		{
			for (Column c : f.getColumns())
			{
				if (c.getJoinableTables() == null)
					c.setJoinableTables(new LinkedHashSet<Object>(5));
				if (c.getDimension() == null)
				{
					List<MeasureColumn> mclist = measureColumnMap.get(c.getName());
					if (mclist != null)
					{
						for (MeasureColumn mc : mclist)
						{
							if (mc.MeasureTable.Type == MeasureTable.DERIVED)
							{
								if (mc.BaseMeasures != null && mc.BaseMeasures.size() > 0)
								{
									Set<Object> joinableMeasureTables = new HashSet<Object>();
									boolean firstT = true;
									for (String bmcName : mc.BaseMeasures)
									{
										Set<Object> joinableDimTables = new HashSet<Object>();
										List<MeasureColumn> bmcList = measureColumnMap.get(bmcName);
										if (bmcList != null)
										{
											for (MeasureColumn bmc : bmcList)
											{
												joinableMeasureTables.add(bmc.MeasureTable);
												List<Object> olist = joinTableMap.get(bmc.MeasureTable);
												if (olist != null)
												{
													joinableDimTables.addAll(olist);
												}
											}
										}
										if (firstT)
										{
											c.getJoinableTables().addAll(joinableDimTables);
											firstT = false;
										} else
										{
											c.getJoinableTables().retainAll(joinableDimTables);
										}
									}
									c.getJoinableTables().addAll(joinableMeasureTables);
								}
							} else
							{
								c.getJoinableTables().add(mc.MeasureTable);
								List<Object> olist = joinTableMap.get(mc.MeasureTable);
								if (olist != null)
									c.getJoinableTables().addAll(olist);
							}
						}
					}
				} else
				{
					List<DimensionColumn> dclist = dimensionColumnMap.get(c.getDimension() + "." + c.getName());
					if (dclist != null)
					{
						for (DimensionColumn dc : dclist)
						{
							c.getJoinableTables().add(dc.DimensionTable);
							List<Object> olist = joinTableMap.get(dc.DimensionTable);
							if (olist != null)
							{
								c.getJoinableTables().addAll(olist);
								if (forceCrossJoins)
								{
									for (Object o : olist)
									{
										if (!(o instanceof MeasureTable))
											continue;
										List<Object> olist2 = joinTableMap.get((MeasureTable) o);
										if (olist2 != null)
											c.getJoinableTables().addAll(olist2);
									}
								}
							}
						}
					}
				}
			}
		}
		if (f.getSubfolders() != null)
		{
			for (Folder cf : f.getSubfolders())
			{
				processSubjectAreaFolderJoins(cf);
			}
		}
	}

	private void processInheritedSuccessModels()
	{
		if (successModels != null)
		{
			for (int i = 0; i < successModels.length; i++)
			{
				SuccessModel sm = successModels[i];
				if ((sm.InheritFromModel != null) && (sm.InheritFromModel.length() > 0))
				{
					SuccessModel ism = findSuccessModel(sm.InheritFromModel);
					if (ism != null)
					{
						// Copy flags
						if (ism.FilterNullCategories)
							sm.FilterNullCategories = true;
						if (ism.GroupMeasureOnly)
							sm.GroupMeasureOnly = true;
						// First, copy attributes
						List<String> newlist = new ArrayList<String>();
						for (int j = 0; j < sm.Attributes.length; j++)
						{
							if (!newlist.contains(sm.Attributes[j]))
								newlist.add(sm.Attributes[j]);
						}
						/*
						 * If it's inherited, add it. If it's overridden (i.e. it's in both places, then remove it -
						 * having it overridden means do the opposite)
						 */
						for (int j = 0; j < ism.Attributes.length; j++)
						{
							if (!newlist.contains(ism.Attributes[j]))
								newlist.add(ism.Attributes[j]);
							else
								newlist.remove(ism.Attributes[j]);
						}
						sm.Attributes = new String[newlist.size()];
						newlist.toArray(sm.Attributes);
						// Now, copy all success patterns
						List<Pattern> patList = new ArrayList<Pattern>();
						for (int j = 0; j < sm.Patterns.length; j++)
						{
							if (!patList.contains(sm.Patterns[j]))
								patList.add(sm.Patterns[j]);
						}
						for (int j = 0; j < ism.Patterns.length; j++)
						{
							boolean found = false;
							for (int k = 0; k < patList.size(); k++)
							{
								if (patList.get(k).equals(ism.Patterns[j]))
								{
									found = true;
									break;
								}
							}
							if (!found)
								patList.add(ism.Patterns[j]);
						}
						sm.Patterns = new Pattern[patList.size()];
						patList.toArray(sm.Patterns);
						// Copy all filters
						newlist = new ArrayList<String>();
						for (int j = 0; j < sm.Filters.length; j++)
						{
							if (!newlist.contains(sm.Filters[j]))
								newlist.add(sm.Filters[j]);
						}
						for (int j = 0; j < ism.Filters.length; j++)
						{
							if (!newlist.contains(ism.Filters[j]))
								newlist.add(ism.Filters[j]);
						}
						sm.Filters = new String[newlist.size()];
						newlist.toArray(sm.Filters);
						// Copy any additional unfilled in fields
						if ((sm.TargetDimension == null) || (sm.TargetDimension.isEmpty()))
							sm.TargetDimension = ism.TargetDimension;
						if ((sm.TargetLevel == null) || (sm.TargetLevel.isEmpty()))
							sm.TargetLevel = ism.TargetLevel;
						if ((sm.IterateDimension == null) || (sm.IterateDimension.isEmpty()))
							sm.IterateDimension = ism.IterateDimension;
						if ((sm.IterateLevel == null) || (sm.IterateLevel.isEmpty()))
							sm.IterateLevel = ism.IterateLevel;
						if ((sm.BreakAttribute == null) || (sm.BreakAttribute.isEmpty()))
							sm.BreakAttribute = ism.BreakAttribute;
						if ((sm.PartitionAttribute == null) || (sm.PartitionAttribute.isEmpty()))
							sm.PartitionAttribute = ism.PartitionAttribute;
						if ((sm.Measure == null) || (sm.Measure.isEmpty()))
							sm.Measure = ism.Measure;
					}
				}
			}
		}
	}

	private void processInheritedDimensionTables() throws CloneNotSupportedException
	{
		for (int i = 0; i < DimensionTables.length; i++)
		{
			DimensionTable dt = DimensionTables[i];
			if (dt.InheritTable != null)
			{
				// Create a new list
				List<DimensionColumn> list = new ArrayList<DimensionColumn>();
				for (int j = 0; j < dt.DimensionColumns.length; j++)
				{
					list.add(dt.DimensionColumns[j]);
				}
				// Add inherited columns
				DimensionTable idt = findDimensionTable(dt.InheritTable);
				if (idt != null)
				{
					if (!dt.DontInheritColumns)
					{
						for (int j = 0; j < idt.DimensionColumns.length; j++)
						{
							DimensionColumn newdc = (DimensionColumn) idt.DimensionColumns[j].clone();
							newdc.ColumnName = (dt.InheritPrefix == null ? "" : dt.InheritPrefix) + idt.DimensionColumns[j].ColumnName;
							newdc.DimensionTable = dt;
							newdc.TableName = dt.TableName;
							/*
							 * For opaque views, use new physical name
							 */
							if (dt.Type == 1)
							{
								for (TableDefinition td : idt.TableSource.Tables)
								{
									newdc.PhysicalName = Util.replaceStr(newdc.PhysicalName, td.PhysicalName + ".", dt.PhysicalName + ".");
								}
							}
							// Make any necessary substitutions
							if (!isPrimaryLevelKey(newdc))
							{
								newdc.PhysicalName = ColumnSubstitution.makeSubstitutions(newdc.PhysicalName, dt.ColumnSubstitutions);
							}
							list.add(newdc);
							addDimensionColumnToMap(newdc);
						}
					}
					/*
					 * First get all measure tables that join to current table and inherited table
					 */
					if (!dt.DontInheritJoins)
					{
						// Add inherited joins
						HashMap<String, Join> curMap = new HashMap<String, Join>();
						HashMap<String, Join> iMap = new HashMap<String, Join>();
						int joinLength = joins.length;
						for (int j = 0; j < joinLength; j++)
						{
							if (joins[j].Table1Str.equals(dt.TableName) && (findMeasureTableName(joins[j].Table2Str) != null))
							{
								curMap.put(joins[j].Table2Str, joins[j]);
							} else if (joins[j].Table2Str.equals(dt.TableName) && (findMeasureTableName(joins[j].Table1Str) != null))
							{
								curMap.put(joins[j].Table1Str, joins[j]);
							}
							if (joins[j].Table1Str.equals(idt.TableName) && (findMeasureTableName(joins[j].Table2Str) != null))
							{
								iMap.put(joins[j].Table2Str, joins[j]);
							} else if (joins[j].Table2Str.equals(idt.TableName) && (findMeasureTableName(joins[j].Table1Str) != null))
							{
								iMap.put(joins[j].Table1Str, joins[j]);
							}
						}
						/*
						 * Now go through joins of inherited table and add any that fit to the inheriting table (if not
						 * overridden)
						 */
						List<Join> joinList = new ArrayList<Join>();
						for (Iterator<Entry<String, Join>> it = iMap.entrySet().iterator(); it.hasNext();)
						{
							Entry<String, Join> me = (Entry<String, Join>) it.next();
							if (!curMap.containsKey(me.getKey()))
							{
								Join jn = (Join) me.getValue();
								boolean ok = true;
								if (dt.IgnoreJoinsForTables != null)
								{
									for (String s : dt.IgnoreJoinsForTables)
									{
										if (s.equals(jn.Table1Str) || s.equals(jn.Table2Str))
										{
											ok = false;
											break;
										}
									}
								}
								if (!ok)
									continue;
								Join newJoin = (Join) jn.clone();
								if (jn.Table1Str.equals(idt.TableName))
									newJoin.Table1Str = dt.TableName;
								else
									newJoin.Table1Str = jn.Table1Str;
								if (jn.Table2Str.equals(idt.TableName))
									newJoin.Table2Str = dt.TableName;
								else
									newJoin.Table2Str = jn.Table2Str;
								if (jn.JoinCondition != null)
								{
									newJoin.JoinCondition = jn.JoinCondition.replace("\"" + idt.TableName + "\"", "\"" + dt.TableName + "\"");
								}
								// Make any necessary substitutions
								newJoin.JoinCondition = ColumnSubstitution.makeJoinSubstitutions(dt.TableName, dt.TableSource, newJoin.JoinCondition,
										dt.ColumnSubstitutions);
								joinList.add(newJoin);
							}
						}
						if (!joinList.isEmpty())
							addJoins(joinList);
					}
				}
				DimensionColumn[] newArr = new DimensionColumn[list.size()];
				list.toArray(newArr);
				dt.DimensionColumns = newArr;
				if (idt != null && idt.TableSource != null && idt.TableSource.Filters != null)
				{
					// Copy any security filters
					List<TableSourceFilter> flist = new ArrayList<TableSourceFilter>();
					for (TableSourceFilter tsf : idt.TableSource.Filters)
					{
						if (tsf.SecurityFilter)
							flist.add(tsf);
					}
					if (dt.TableSource.Filters != null)
						for (TableSourceFilter tsf : dt.TableSource.Filters)
						{
							flist.add(tsf);
						}
					if (!flist.isEmpty())
					{
						dt.TableSource.Filters = new TableSourceFilter[flist.size()];
						flist.toArray(dt.TableSource.Filters);
					}
				}
			}
		}
	}

	public void processInheritedMeasureTables(boolean aggregatesOnly) throws CloneNotSupportedException
	{
		/*
		 * First of all, make sure we have an ordered list of inherited measure tables that takes care of the
		 * dependencies. e.g. the parent table should always be processed before its children. So, in a situation when A
		 * inherits from B, and B inherits from C, make sure that B is processed before A irrespective of the order of
		 * their appearance in the repository definition.
		 */
		List<MeasureTable> inheritedMeasureTableList = new ArrayList<MeasureTable>();
		List<String> inheritTableNames = new ArrayList<String>();
		for (int i = 0; i < MeasureTables.length; i++)
		{
			MeasureTable mt = MeasureTables[i];
			if (mt.InheritTable != null)
			{
				/*
				 * This table is a parent of another table already added to the list. Make sure that we add this table
				 * before the child table in the list.
				 */
				if (inheritTableNames.contains(mt.TableName))
				{
					int index = inheritTableNames.indexOf(mt.TableName);
					inheritedMeasureTableList.add(index, mt);
					inheritTableNames.add(index, mt.InheritTable);
				} else
				{
					inheritedMeasureTableList.add(mt);
					inheritTableNames.add(mt.InheritTable);
				}
			}
		}
		if (!inheritedMeasureTableList.isEmpty())
		{
			for (MeasureTable mt : inheritedMeasureTableList)
			{
				if (mt.InheritTable != null)
				{
					// See if it is an inheritance from an aggregate table
					Aggregate agg = findAggregate(mt.InheritTable);
					if (((agg == null || !agg.isProcessed()) && (aggregatesOnly)) || ((agg != null) && (!aggregatesOnly)))
					{
						continue;
					}
					// Check for circular reference
					if (aggregatesOnly && agg.usesMeasureTable(mt.TableName))
					{
						logger.error("Circular reference detected for measure table inheritance: Aggregate " + agg.getName() + " depends on measure table "
								+ mt.TableName + " - inheritance will not be processed.");
						continue;
					}
					// Create a new list
					List<MeasureColumn> mclist = new ArrayList<MeasureColumn>();
					for (int j = 0; j < mt.MeasureColumns.length; j++)
					{
						mclist.add(mt.MeasureColumns[j]);
					}
					List<String> tableNames = new ArrayList<String>();
					if (aggregatesOnly)
						tableNames.add(mt.InheritTable);
					else
					{
						// Get inherit tables
						String[] tlist = mt.InheritTable.split(",");
						for (String s : tlist)
							tableNames.add(s);
					}
					for (String tableName : tableNames)
					{
						// Add inherited columns
						String measureTableName = tableName + (aggregatesOnly ? "_AGG" : "");
						MeasureTable imt = findMeasureTableName(measureTableName);
						if (imt != null)
						{
							List<Join> joinList = new ArrayList<Join>();
							addInheritedMeasureColumnsAndJoins(mt, imt, mclist, joinList, tableNames.size() > 1, tableNames.size() > 1
									&& mt.TableSource.Tables.length != 1 && imt.TableSource.Tables.length == 1 ? imt.TableSource.Tables[0].PhysicalName : null);
							addJoins(joinList);
						}
						MeasureColumn[] newArr = new MeasureColumn[mclist.size()];
						mclist.toArray(newArr);
						mt.MeasureColumns = newArr;
					}
				}
			}
		}
	}

	private void addInheritedMeasureColumnsAndJoins(MeasureTable mt, MeasureTable imt, List<MeasureColumn> mclist, List<Join> joinList,
			boolean qualifyFormulas, String physicalTableName) throws CloneNotSupportedException
	{
		if (!mt.DontInheritColumns)
			for (int j = 0; j < imt.MeasureColumns.length; j++)
			{
				MeasureColumn newmc = (MeasureColumn) imt.MeasureColumns[j].clone();
				String prefix = mt.InheritPrefix == null ? "" : mt.InheritPrefix;
				newmc.ColumnName = prefix + imt.MeasureColumns[j].ColumnName;
				if (newmc.Prefixes == null)
					newmc.Prefixes = new ArrayList<String>(1);
				newmc.Prefixes.add(prefix);
				newmc.MeasureTable = mt;
				/*
				 * See if another measure currently exists with same name, and if so, borrow it's properties
				 */
				MeasureColumn existingmc = findMeasureColumn(newmc.ColumnName);
				if (existingmc != null)
				{
					newmc.Format = existingmc.Format;
					newmc.EnforceLimits = existingmc.EnforceLimits;
					newmc.Improve = existingmc.Improve;
					newmc.MaxImprovement = existingmc.MaxImprovement;
					newmc.MinImprovement = existingmc.MinImprovement;
					newmc.Maximum = existingmc.Maximum;
					newmc.Minimum = existingmc.Minimum;
					newmc.Opportunity = existingmc.Opportunity;
					newmc.MaxTarget = existingmc.MaxTarget;
					newmc.MinTarget = existingmc.MinTarget;
					newmc.PerformanceCategory = existingmc.PerformanceCategory;
					newmc.ValidMeasure = existingmc.ValidMeasure;
					newmc.AttributeWeightMeasure = existingmc.AttributeWeightMeasure;
					newmc.AnalysisMeasure = existingmc.AnalysisMeasure;
					newmc.SegmentMeasure = existingmc.SegmentMeasure;
				}
				/*
				 * For opaque views, use new physical name
				 */
				if (mt.Type == MeasureTable.OPAQUE_VIEW)
				{
					for (TableDefinition td : imt.TableSource.Tables)
					{
						newmc.PhysicalName = Util.intern(Util.replaceStr(newmc.PhysicalName, td.PhysicalName + ".", mt.PhysicalName + "."));
						newmc.PhysicalFilter = Util.intern(Util.replaceStr(newmc.PhysicalFilter, td.PhysicalName + ".", mt.PhysicalName + "."));
					}
				}
				if (qualifyFormulas && newmc.Qualify)
				{
					newmc.Qualify = false;
					newmc.PhysicalName = Util.intern(imt.TableSource.Tables[0].PhysicalName + "." + newmc.PhysicalName);
				}
				// Make any necessary substitutions
				newmc.PhysicalName = Util.intern(ColumnSubstitution.makeSubstitutions(newmc.PhysicalName, mt.ColumnSubstitutions));
				newmc.PhysicalFilter = Util.intern(ColumnSubstitution.makeSubstitutions(newmc.PhysicalFilter, mt.ColumnSubstitutions));
				newmc.TableName = mt.TableName;
				mclist.add(newmc);
				addMeasureColumnToMap(newmc);
			}
		if (!mt.DontInheritJoins)
		{
			// Add inherited joins
			HashMap<String, Join> curMap = new HashMap<String, Join>();
			HashMap<String, Join> iMap = new HashMap<String, Join>();
			// First get all dimension tables that join to current table
			// and inherited table
			for (int j = 0; j < joins.length; j++)
			{
				if (joins[j].Table1Str.equals(mt.TableName))
				{
					curMap.put(joins[j].Table2Str, joins[j]);
				} else if (joins[j].Table2Str.equals(mt.TableName))
				{
					curMap.put(joins[j].Table1Str, joins[j]);
				}
				if (joins[j].Table1Str.equals(imt.TableName))
				{
					iMap.put(joins[j].Table2Str, joins[j]);
				} else if (joins[j].Table2Str.equals(imt.TableName))
				{
					iMap.put(joins[j].Table1Str, joins[j]);
				}
			}
			/*
			 * Now go through joins of inherited table and add any that fit to the inheriting table (if not overridden)
			 */
			for (Iterator<Entry<String, Join>> it = iMap.entrySet().iterator(); it.hasNext();)
			{
				Entry<String, Join> me = (Entry<String, Join>) it.next();
				if (!curMap.containsKey(me.getKey()))
				{
					Join jn = (Join) me.getValue();
					Join newJoin = (Join) jn.clone();
					if (jn.Table1Str.equals(imt.TableName))
						newJoin.Table1Str = mt.TableName;
					else
						newJoin.Table1Str = jn.Table1Str;
					if (jn.Table2Str.equals(imt.TableName))
						newJoin.Table2Str = mt.TableName;
					else
						newJoin.Table2Str = jn.Table2Str;
					// If we're inheriting from multiple (simple - i.e. 1 table definition) tables, then fully specify
					if (physicalTableName != null)
					{
						if (jn.JoinCondition != null)
						{
							newJoin.JoinCondition = jn.JoinCondition.replace("\"" + imt.TableName + "\".", "\"" + mt.TableName + "\"."
								+ physicalTableName + ".");
						}
					} else
					{
						if (jn.JoinCondition != null) {
							newJoin.JoinCondition = jn.JoinCondition.replace(
									imt.TableName, mt.TableName);
						}
					}
					// Make any necessary substitutions
					if (mt.ColumnSubstitutions != null)
						newJoin.JoinCondition = ColumnSubstitution.makeJoinSubstitutions(mt.TableName, mt.TableSource, newJoin.JoinCondition,
								mt.ColumnSubstitutions);
					joinList.add(newJoin);
				}
			}
		}
		if (imt.TableSource.Filters != null)
		{
			// Copy any security filters
			List<TableSourceFilter> flist = new ArrayList<TableSourceFilter>();
			for (TableSourceFilter tsf : imt.TableSource.Filters)
			{
				if (tsf.SecurityFilter)
					flist.add(tsf);
			}
			if (mt.TableSource.Filters != null)
				for (TableSourceFilter tsf : mt.TableSource.Filters)
				{
					flist.add(tsf);
				}
			if (flist.size() > 0)
			{
				mt.TableSource.Filters = new TableSourceFilter[flist.size()];
				flist.toArray(mt.TableSource.Filters);
			}
		}
	}

	public void processInheritedVirtualMeasures(boolean aggregatesOnly) throws CloneNotSupportedException
	{
		for (int i = 0; i < DimensionTables.length; i++)
		{
			DimensionTable dt = DimensionTables[i];
			boolean isTime = (timeDefinition != null && timeDefinition.Name != null && timeDefinition.Name.equals(dt.DimensionName)); 
			if (dt.VirtualMeasuresInheritTable != null)
			{
				// Create a new list
				List<DimensionColumn> list = new ArrayList<DimensionColumn>();
				for (int j = 0; j < dt.DimensionColumns.length; j++)
				{
					list.add(dt.DimensionColumns[j]);
				}
				// Add inherited columns
				DimensionTable idt = findDimensionTable(dt.VirtualMeasuresInheritTable);
				if (idt == null)
				{
					logger.warn("Unable to locate inherted dimension table " + dt.VirtualMeasuresInheritTable + " referenced in table " + dt.TableName);
					continue;
				}
				/*
				 * Dimension column definitions have already been created in the first run when this method is called
				 * for base tables, so do not create the definitions again when called for aggregates.
				 */
				if (!aggregatesOnly && !dt.DontInheritColumns)
				{
					for (int j = 0; j < idt.DimensionColumns.length; j++)
					{
						DimensionColumn newdc = (DimensionColumn) idt.DimensionColumns[j].clone();
						newdc.ColumnName = idt.DimensionColumns[j].ColumnName;
						newdc.DimensionTable = dt;
						newdc.TableName = dt.TableName;
						for (int k = 0; k < dt.TableSource.Tables.length; k++)
						{
							if (idt.TableSource != null && idt.TableSource.Tables.length > k)
								if (!dt.TableSource.Tables[k].PhysicalName.equals(idt.TableSource.Tables[k].PhysicalName))
								{
									newdc.PhysicalName = Util.replaceStr(newdc.PhysicalName, idt.TableSource.Tables[k].PhysicalName,
											dt.TableSource.Tables[k].PhysicalName);
								}
						}
						// Make any necessary substitutions
						if (!isPrimaryLevelKey(newdc))
						{
							newdc.PhysicalName = ColumnSubstitution.makeSubstitutions(newdc.PhysicalName, dt.ColumnSubstitutions);
						}
						list.add(newdc);
						addDimensionColumnToMap(newdc);
					}
				}
				/*
				 * Find all measure tables that join to the inherited dimension table
				 */
				List<MeasureTable> mtList = new ArrayList<MeasureTable>();
				for (int j = 0; j < MeasureTables.length; j++)
				{
					MeasureTable mt = MeasureTables[j];
					// See if this is an aggregate table
					Aggregate agg = null;
					if (mt.QueryAgg != null)
					{
						agg = findAggregate(mt.QueryAgg.getName());
					}
					if (((agg == null || !agg.isProcessed() || agg.hasFilters()) && (aggregatesOnly)) || ((agg != null) && (!aggregatesOnly)))
					{
						continue;
					}
					boolean containsMeasureWithoutVirtualPrefix = false;
					if (aggregatesOnly)
					{
						/*
						 * If this is a measure table that was auto-generated during aggregate processing, check to see
						 * if there are any measure columns that are not already prefixed
						 */
						for (MeasureColumn mc : mt.MeasureColumns)
						{
							if (!mc.ColumnName.startsWith(dt.VirtualMeasuresPrefix))
							{
								containsMeasureWithoutVirtualPrefix = true;
								break;
							}
						}
					} else
					{
						containsMeasureWithoutVirtualPrefix = true;
					}
					if (containsMeasureWithoutVirtualPrefix && joinedTables(mt, idt))
					{
						mtList.add(mt);
					}
				}
				if ((idt != null) && (mtList.size() > 0))
				{
					MeasureTable[] newMeasureTables = new MeasureTable[MeasureTables.length + mtList.size()];
					for (int j = 0; j < MeasureTables.length; j++)
					{
						newMeasureTables[j] = MeasureTables[j];
					}
					for (int j = 0; j < mtList.size(); j++)
					{
						// First, create the new measure table
						MeasureTable mt = (MeasureTable) mtList.get(j);
						MeasureTable newmt = (MeasureTable) mt.clone();
						newMeasureTables[MeasureTables.length + j] = newmt;
						newmt.TableName = dt.VirtualMeasuresPrefix + mt.TableName;
						newmt.BaseTable = mt.TableName;
						List<MeasureColumn> mcList = new ArrayList<MeasureColumn>();
						for (int k = 0; k < newmt.MeasureColumns.length; k++)
						{
							if (aggregatesOnly && newmt.MeasureColumns[k].ColumnName.startsWith(dt.VirtualMeasuresPrefix))
							{
								continue;
							}
							MeasureColumn newmc = (MeasureColumn) newmt.MeasureColumns[k].clone();
							String prefix = dt.VirtualMeasuresPrefix == null ? "" : dt.VirtualMeasuresPrefix;
							newmc.ColumnName = prefix + newmt.MeasureColumns[k].ColumnName;
							if (newmc.Prefixes == null)
								newmc.Prefixes = new ArrayList<String>(1);
							newmc.Prefixes.add(prefix);
							newmc.TableName = newmt.TableName;
							newmc.MeasureTable = newmt;
							newmc.NonAdditive = isTime;
							/*
							 * See if another measure currently exists with same name, and if so, borrow it's properties
							 */
							MeasureColumn existingmc = findMeasureColumn(newmc.ColumnName);
							if (existingmc != null)
							{
								newmc.Format = existingmc.Format;
								newmc.EnforceLimits = existingmc.EnforceLimits;
								newmc.Improve = existingmc.Improve;
								newmc.MaxImprovement = existingmc.MaxImprovement;
								newmc.MinImprovement = existingmc.MinImprovement;
								newmc.Maximum = existingmc.Maximum;
								newmc.Minimum = existingmc.Minimum;
								newmc.Opportunity = existingmc.Opportunity;
								newmc.MaxTarget = existingmc.MaxTarget;
								newmc.MinTarget = existingmc.MinTarget;
								newmc.PerformanceCategory = existingmc.PerformanceCategory;
								newmc.ValidMeasure = existingmc.ValidMeasure;
								newmc.AttributeWeightMeasure = existingmc.AttributeWeightMeasure;
								newmc.AnalysisMeasure = existingmc.AnalysisMeasure;
								newmc.SegmentMeasure = existingmc.SegmentMeasure;
							}
							mcList.add(newmc);
							addMeasureColumnToMap(newmc);
						}
						newmt.MeasureColumns = new MeasureColumn[mcList.size()];
						for (int k = 0; k < mcList.size(); k++)
						{
							newmt.MeasureColumns[k] = mcList.get(k);
						}
						// Now, find all joins between parents and copy them
						List<Join> newJoinList = new ArrayList<Join>();
						List<Join> joinList = new ArrayList<Join>();
						/*
						 * First get all dimension tables that join to current measure table
						 */
						List<Join> jList = getJoinList(mt.TableName);
						if (jList != null)
						{
							for (Join j3 : jList)
							{
								if (j3.Invalid)
									continue;
								/*
								 * Add an inherited join only if the measure table is not being overridden (also, make
								 * sure any dimension table is not in the same dimension)
								 */
								if (j3.Table1Str.equals(mt.TableName) || j3.Table2Str.equals(mt.TableName))
								{
									String tname = j3.Table1Str.equals(mt.TableName) ? j3.Table2Str : j3.Table1Str;
									boolean ok = true;
									if (dt.IgnoreJoinsForTables != null)
									{
										for (String s : dt.IgnoreJoinsForTables)
										{
											if (s.equals(tname))
											{
												ok = false;
												break;
											}
										}
									}
									if (!ok)
										continue;
									for (DimensionTable odt : DimensionTables)
									{
										if (odt.TableName.equals(tname) && odt.DimensionName.equals(dt.DimensionName)
												&& !(odt.TableName.equals(idt.TableName) || odt.TableName.equals(dt.TableName)))
										{
											ok = false;
											break;
										}
									}
									if (ok)
										newJoinList.add(j3);
								}
							}
						}
						/*
						 * Now go through joins of inherited table and add any that fit to the inheriting table (if not
						 * overridden - to overwrite, just create a join between the new measure table (add the prefix)
						 * and the new dimension table. This will be first in the list of joins and will be the join
						 * used. Joins between a measure table and the inherited table if there is a join defined
						 * between the inheriting table and that same measure table)
						 */
						for (Join oldJoin : newJoinList)
						{
							Join newJoin = (Join) oldJoin.clone();
							if (oldJoin.Table1Str.equals(idt.TableName))
							{
								newJoin.Table1Str = dt.TableName;
								newJoin.Table1 = dt;
							} else
							{
								newJoin.Table1Str = oldJoin.Table1Str;
								newJoin.Table1 = oldJoin.Table1;
							}
							if (oldJoin.Table2Str.equals(idt.TableName))
							{
								newJoin.Table2Str = dt.TableName;
								newJoin.Table2 = dt;
							} else
							{
								newJoin.Table2Str = oldJoin.Table2Str;
								newJoin.Table2 = oldJoin.Table2;
							}
							if (newJoin.Table1Str.equals(mt.TableName))
							{
								newJoin.Table1Str = newmt.TableName;
								newJoin.Table1 = newmt;
							}
							if (newJoin.Table2Str.equals(mt.TableName))
							{
								newJoin.Table2Str = newmt.TableName;
								newJoin.Table2 = newmt;
							}
							if (aggregatesOnly)
							{
								/*
								 * For aggregates the physical table name qualification is always present in the
								 * generated joins So, in order for it to work with column substitutions later, make
								 * sure that the physical name is removed.
								 */
								if (oldJoin.JoinCondition != null)
								{
									newJoin.JoinCondition = oldJoin.JoinCondition.replace("." + idt.PhysicalName + ".", ".");
								}
							} else
							{
								newJoin.JoinCondition = oldJoin.JoinCondition;
							}
							if (newJoin.JoinCondition != null)
							{
								newJoin.JoinCondition = newJoin.JoinCondition.replace(idt.TableName, dt.TableName);
								newJoin.JoinCondition = newJoin.JoinCondition.replace(mt.TableName, newmt.TableName);
							}							
							// Make any necessary substitutions
							newJoin.JoinCondition = ColumnSubstitution.makeJoinSubstitutions(dt.TableName, dt.TableSource, newJoin.JoinCondition,
									dt.ColumnSubstitutions);
							newJoin.JoinCondition = ColumnSubstitution.makeJoinSubstitutions(mt.TableName, mt.TableSource, newJoin.JoinCondition,
									mt.ColumnSubstitutions);
							for (TableDefinition td : idt.TableSource.Tables)
							{
								boolean found = false;
								for (TableDefinition td2 : dt.TableSource.Tables)
								{
									if (td2.PhysicalName.equals(td.PhysicalName))
									{
										found = true;
										break;
									}
								}
								if (!found && newJoin.JoinCondition != null)
									newJoin.JoinCondition = newJoin.JoinCondition.replace(td.PhysicalName, dt.TableSource.Tables[0].PhysicalName);
							}
							for (TableDefinition td : mt.TableSource.Tables)
							{
								boolean found = false;
								for (TableDefinition td2 : newmt.TableSource.Tables)
								{
									if (td2.PhysicalName.equals(td.PhysicalName))
									{
										found = true;
										break;
									}
								}
								if (!found && newJoin.JoinCondition != null)
									newJoin.JoinCondition = newJoin.JoinCondition.replace(td.PhysicalName, newmt.TableSource.Tables[0].PhysicalName);
							}
							/*
							 * Make sure it isn't overridden
							 */
							boolean found = isExactSameJoin(newJoin);
							if (!found)
							{
								joinList.add(newJoin);
								/*
								 * Add join to the map only if this method is called for aggregates. For base tables,
								 * processJoinRelationships() method will add all generated joins to the map.
								 */
								if (aggregatesOnly)
								{
									addJoinToMaps(newJoin);
								}
							}
						}
						addJoins(joinList);
					}
					MeasureTables = newMeasureTables;
					invalidateMeasureTableNameMap();
				}
				DimensionColumn[] newArr = new DimensionColumn[list.size()];
				list.toArray(newArr);
				dt.DimensionColumns = newArr;
			}
		}
	}

	private void processPartitionMetadata()
	{
		PartitionMetadataProcessor pmp = new PartitionMetadataProcessor(this);
		if(DimensionTables != null && DimensionTables.length > 0)
		{
			for(DimensionTable dt : DimensionTables)
			{
				if(pmp.isDimensionTablePartitioned(dt))
				{
					dt.IsPartitioned = true;
				}
			}
		}
		if(MeasureTables != null && MeasureTables.length > 0)
		{
			for(MeasureTable mt : MeasureTables)
			{
				if(pmp.isMeasureTablePartitioned(mt))
				{
					mt.IsPartitioned = true;
				}
			}
		}
	}
	
	/**
	 * Replace all logical query strings in opaque views
	 * 
	 * @param stmt
	 * @throws SQLException
	 * @throws NavigationException
	 * @throws IOException
	 * @throws RepositoryException
	 */
	private void processOpaqueViews() throws BaseException, IOException, SQLException, RepositoryException,
			CloneNotSupportedException
	{
		DatabaseConnection dc = findConnection(Repository.DEFAULT_DB_CONNECTION);
		List<String> dbTableNameList = null;
		for (int i = 0; i < DimensionTables.length; i++)
		{
			DimensionTable dt = DimensionTables[i];
			if (dt.Type == DimensionTable.OPAQUE_VIEW)
			{
				if (!dt.AutoGenerated && dt.PhysicalName != null && dt.PhysicalName.length() > 0 && dbTableNameList == null)
					dbTableNameList = Database.getTables(dc, null, dc.Schema, null);
				if (!dt.AutoGenerated && dt.PhysicalName != null && dt.PhysicalName.length() > 0 && dbTableNameList.contains(dt.PhysicalName.toLowerCase()))
					instantiateOpaqueView(dt.TableName, false);
				else
				{
					AbstractQueryString aqs = AbstractQueryString.getInstance(this, dt.OpaqueView, useNewQueryLanguage);
					dt.OpaqueView = aqs.replaceQueryString(dt.OpaqueView, true, null, false, false, null, null, null);
				}
			}
		}
		for (int i = 0; i < MeasureTables.length; i++)
		{
			MeasureTable mt = MeasureTables[i];
			if (mt.Type == MeasureTable.OPAQUE_VIEW)
			{
				if (!mt.AutoGenerated && mt.PhysicalName != null && mt.PhysicalName.length() > 0 && dbTableNameList == null)
					dbTableNameList = Database.getTables(dc, null, dc.Schema, null);
				if (!mt.AutoGenerated && mt.PhysicalName != null && mt.PhysicalName.length() > 0 && dbTableNameList.contains(mt.PhysicalName.toLowerCase()))
					instantiateOpaqueView(mt.TableName, false);
				else
				{
					AbstractQueryString aqs = AbstractQueryString.getInstance(this, mt.OpaqueView, useNewQueryLanguage);
					mt.OpaqueView = aqs.replaceQueryString(mt.OpaqueView, true, null, false, false, null, null, null);
				}
			}
		}
	}

	/**
	 * instantiate and create the opaque views for the various macro modeling steps
	 * 
	 * @param step
	 *            step in the modeling flow: Repository.MODEL_*
	 * @return
	 */
	public void validateModelingOpaqueViews(int step) throws SQLException
	{
		// iterate over all opaque views, looking for ones that this step depends upon
		for (DimensionTable dt : DimensionTables)
		{
			if (dt.Type == DimensionTable.OPAQUE_VIEW && dt.modelingStep == step)
			{
				logger.info("Instantiating opaque view " + dt.TableName);
				instantiateOpaqueView(dt.TableName, true);
			}
		}
		for (MeasureTable mt : MeasureTables)
		{
			if (mt.Type == MeasureTable.OPAQUE_VIEW && mt.modelingStep == step)
			{
				logger.info("Instantiating opaque view " + mt.TableName);
				instantiateOpaqueView(mt.TableName, true);
			}
		}
	}

	/**
	 * remove any opaque views marked as for modeling
	 * 
	 * @return
	 */
	public void removeModelingOpaqueViews() throws SQLException
	{
		// iterate over all opaque views, looking for ones that this step depends upon
		for (DimensionTable dt : DimensionTables)
		{
			if (dt.Type == DimensionTable.OPAQUE_VIEW && dt.modelingStep > 0)
			{
				dropOpaqueView(dt.TableName);
			}
		}
		for (MeasureTable mt : MeasureTables)
		{
			if (mt.Type == MeasureTable.OPAQUE_VIEW && mt.modelingStep > 0)
			{
				dropOpaqueView(mt.TableName);
			}
		}
	}

	/**
	 * delete data in the tables associated with reference populations
	 */
	public void removeReferencePopulationData() throws SQLException
	{
		DatabaseConnection dc = findConnection(Repository.DEFAULT_DB_CONNECTION);
		for (ReferencePopulation refpop : referencePopulations)
		{
			Database.deleteOldData(dc, refpop.getTableName(false), null);
			Database.deleteOldData(dc, refpop.getTableName(true), null);
		}
	}

	/**
	 * remove any aggregates created by the models
	 * 
	 * @return
	 */
	public void removeModelingAggregates() throws Exception
	{
		for (SuccessModel sm : successModels)
		{
			if (sm.hasAggreates())
			{
				SuccessModelInstance smi = new SuccessModelInstance(this, sm, null, false);
				smi.fill(true, false, false);
				dropQueryAggregates(sm.Name, true);
			}
		}
	}

	private Set<String> sessionVariables; // session variables used in the expressions in the repository
	
	private void addDummySessionVariable(String name)
	{
		Variable v = new Variable();
		v.setName(name);
		v.setCacheable(false);
		v.setConstant(true);
		v.setType(VariableType.Session);
		v.setQuery("''");
		v.setEvaluateOnDemand(true);
		List<Variable> newlist = new ArrayList<Variable>();
		if (variables != null)
		{
			for (Variable v2 : variables)
			{
				newlist.add(v2);
			}
		}
		newlist.add(v);
		variables = new Variable[newlist.size()];
		newlist.toArray(variables);
	}
	
	/**
	 * get the session variables referenced in the expression
	 * @param expression
	 * @return
	 */
	public List<Variable> getSessionVariableList(String expression)
	{
		List<Variable> vlist = null;
		if (expression == null  || !containsVariableString(expression))
		{
			return vlist;
		}
		for (Variable v : variables)
		{
			if (v.getType() == Variable.VariableType.Session &&
					(expression.contains("V{" + v.getName() + "}") || expression.contains("GETVARIABLE('" + v.getName() + "')") || expression.contains("GetVariable('" + v.getName() + "')")))
			{
				if (vlist == null)
					vlist = new ArrayList<Variable>();
				vlist.add(v);
			}
		}
		if (sessionVariables == null)
		{
			sessionVariables = new HashSet<String>();
			for (Variable v : variables)
			{
				if (v.getType() == Variable.VariableType.Session)
					sessionVariables.add(v.getName());
			}
		}
		boolean found;
		int count = 0;
		do
		{
			int pos = 0;
			found = false;
			while ((pos = expression.indexOf("V{P['", pos)) >= 0)
			{
				found = true;
				int epos = expression.indexOf("']}", pos);
				if (epos > pos)
				{
					String name = expression.substring(pos + 2, epos + 2);
					if (!sessionVariables.contains(name))
					{
						Variable v = new Variable();
						v.setName(name);
						v.setCacheable(false);
						v.setConstant(true);
						v.setType(VariableType.Session);
						v.setQuery("''");
						v.setEvaluateOnDemand(true);
						List<Variable> newlist = new ArrayList<Variable>();
						if (variables != null)
						{
							for (Variable v2 : variables)
							{
								newlist.add(v2);
							}
						}
						newlist.add(v);
						variables = new Variable[newlist.size()];
						newlist.toArray(variables);
						if (vlist == null)
							vlist = new ArrayList<Variable>();
						vlist.add(v);
						sessionVariables.add(name);
					}
					pos = epos+1;
				} else
				{
					// If didn't find the end, just ignore this prefix
					pos += 2;
				}
			}
		} while (found && (count++ < 15));
		dumpSessionVariables(expression, vlist);
		return (vlist != null && !vlist.isEmpty() ? vlist : null);
	}
	
	/**
	 * output the session variables associated with the expression
	 * @param expression
	 * @param vlist
	 */
	private void dumpSessionVariables(String expression, List<Variable> vlist)
	{
		if (logger.isTraceEnabled())
		{
			if (vlist != null && !vlist.isEmpty())
			{
				StringBuilder vars = new StringBuilder();
				for (Variable var : vlist)
				{
					if (vars.length() > 0)
						vars.append(',');
					vars.append(var.getName());
				}
				logger.trace(expression + ": " + vars.toString());
			}
		}
	}
	
	/**
	 * quick check to see if the expression contains a variable reference
	 * @param expression
	 * @return
	 */
	private boolean containsVariableString(String expression)
	{
		return expression != null && (expression.contains("V{") || expression.contains("GETVARIABLE('") || expression.contains("GetVariable('"));
	}
	
	/**
	 * get the variables associated with the table source
	 * @param ts
	 * @return
	 */
	private List<Variable> getTableSourceVariableList(TableSource ts)
	{
		List<Variable> filtersvlist = null;
		if (ts != null)
		{
			if (ts.Filters != null)
			{
				for (TableSourceFilter filter : ts.Filters)
				{
					if(filter.SecurityFilter)
					{						
						continue;
					}
					List<Variable> filtervlist = getSessionVariableList(filter.Filter);
					if (filtervlist != null)
					{
						if (filtersvlist == null)
							filtersvlist = new ArrayList<Variable>();
						filtersvlist.addAll(filtervlist);
					}
				}
			}
		}
		return filtersvlist;
	}
	
	/**
	 * Process various places in the repository that may contain variables. Mark presence of session variables in columns.
	 * @param inheritedTablesOnly
	 */
	private void processVariables(boolean inheritedTablesOnly)
	{
		for (MeasureTable mt : MeasureTables)
		{
			if(inheritedTablesOnly && (mt.InheritTable == null || mt.InheritTable.trim().isEmpty()))
			{
				continue;
			}
			List<Variable> tablevlist = getSessionVariableList(mt.OpaqueView);
			List<Variable> filtersvlist = getTableSourceVariableList(mt.TableSource);
			if (filtersvlist != null)
			{
				if (tablevlist == null)
					tablevlist = new ArrayList<Variable>();
				tablevlist.addAll(filtersvlist);
			}
			for (MeasureColumn mc : mt.MeasureColumns)
			{				
				List<Variable> vlist = getSessionVariableList(mc.PhysicalName);		
				List<Variable> pfvlist = getSessionVariableList(mc.PhysicalFilter);
				if (pfvlist != null)
				{
					if (vlist == null)
						vlist = new ArrayList<Variable>();
					vlist.addAll(pfvlist);
				}
				if (tablevlist != null)
				{
					if (vlist == null)
						vlist = new ArrayList<Variable>();
					vlist.addAll(tablevlist);
				}
				if (vlist != null)
					variableSet.add(mc.ColumnName);
				mc.SessionVariables = vlist;
			}
		}
		for (DimensionTable dt : DimensionTables)
		{
			if(inheritedTablesOnly && (dt.InheritTable == null || dt.InheritTable.trim().isEmpty()))
			{
				continue;
			}
			List<Variable> tablevlist = getSessionVariableList(dt.OpaqueView);
			List<Variable> filtersvlist = getTableSourceVariableList(dt.TableSource);
			if (filtersvlist != null)
			{
				if (tablevlist == null)
					tablevlist = new ArrayList<Variable>();
				tablevlist.addAll(filtersvlist);
			}
			for (DimensionColumn dc : dt.DimensionColumns)
			{
				List<Variable> vlist = getSessionVariableList(dc.PhysicalName);				
				if (tablevlist != null)
				{
					if (vlist == null)
						vlist = new ArrayList<Variable>();
					vlist.addAll(tablevlist);
				}
				if (vlist != null)
				{
					variableSet.add(dc.DimensionTable.DimensionName + "." + dc.ColumnName);
				}
				dc.SessionVariables = vlist;
			}
		}
	}

	/**
	 * Look to see if a logical column has a session variable in one of its physical incarnations
	 * 
	 * @param dimension
	 * @param column
	 * @return
	 */
	public boolean hasVariables(String dimension, String column)
	{
		if (dimension == null)
			return (variableSet.contains(column));
		else
			return (variableSet.contains(dimension + "." + column));
	}

	/**
	 * Return a list of Custom Drill objects in the Repository
	 * 
	 * @return List<CustomDrill>
	 */
	public List<CustomDrill> getCustomDrills()
	{
		return customDrills;
	}

	/**
	 * Return the data to fill a given variable
	 * 
	 * @param stmt
	 * @param v
	 * @return
	 */
	public Object fillVariable(Statement stmt, Variable v, Session session, JasperDataSourceProvider jdsp)
	{
		if (v.isConstant())
		{
			String s = v.getQuery();
			return s;
		}
		String vConnName = v.getConnectionName();
		DatabaseConnection dc = null;
		Repository fillRepository = v.getFillRepository();
		if (vConnName != null && !vConnName.isEmpty())
		{
			if (fillRepository != null)
			{
				try
				{
					fillRepository.processUninitializedMetadata(false,  false);
					dc = fillRepository.findConnection(vConnName);
				} catch (Exception e)
				{
					logger.error(e);
				}
			}
			else
				dc = findConnection(vConnName);
		}
		boolean isRealtime = dc != null && dc.isRealTimeConnection();
		if (jdsp == null && 
				(isRealtime || (dc != null && DatabaseConnection.isDBTypeXMLA(dc.DBType) && dc.useDirectConnection)) 
				&& v.isCacheable()) 
		{
			jdsp = new JasperDataSourceProvider(fillRepository == null ? this : fillRepository);
		}
		if ((v.isLogicalQuery()) && (jdsp != null) && (v.isCacheable()))
		{
			try
			{
				QueryResultSet qrs = (QueryResultSet) jdsp.create(v.getQuery(), session);
				if (qrs != null)
				{
					if (v.isMultipleColumns())
					{
						return (Object) qrs;
					} else if (v.isMultiValue())
					{
						List<Object> values = new ArrayList<Object>();
						for (Object[] rows : qrs.getRows())
						{
							if (rows.length > 0)
							{
								if (rows[0] != null)
								{
									values.add(rows[0]);
								}
							}
						}
						return (values);
					} else
					{
						return qrs.getValue(0, 0);
					}
				}
			} catch (PublishingException pe)
			{
				logger.info(pe.getMessage()); // no need for stack trace here
			} catch (Exception e)
			{
				if (v.getDefaultIfInvalid() != null && v.getDefaultIfInvalid().length() > 0)
				{
					logger.warn("Variable [" + v.getName() + "] query invalid (cannot execute) - using default value: " + v.getDefaultIfInvalid());
					return (v.getDefaultIfInvalid());
				} else
				{
					logger.error(e);
				}
			}
		} else
		// Process physical query here or convert logical query into physical query and execute it.
		{
			String q = replaceVariableQuery(session, v);
			logger.debug(Query.getPrettyQuery(q));
			if (isRealtime)
			{
				return fillLiveAccessVariable(dc, v, q);
			}
			else
			{
				boolean stmtAllocatedHere = false;
				if (stmt == null)
				{
					if (dc == null && (vConnName == null || vConnName.trim().equals("")))
					{
						logger.debug("Database connection for variable " + v.getName() + " not provided. Using default connection to populate variable.");
						if (fillRepository != null)
						{
							try
							{
								fillRepository.processUninitializedMetadata(false,  false);
								dc = fillRepository.findConnection(DEFAULT_DB_CONNECTION);
							} catch (Exception e)
							{
								logger.error(e);
							}
						}
						else
							dc = findConnection(DEFAULT_DB_CONNECTION);
					}
					if (dc != null)
					{
						try
						{
							Connection connection = dc.ConnectionPool.getConnection();
							if (connection != null)
							{
								stmt = connection.createStatement();
								stmtAllocatedHere = true;
							}
						} catch (SQLException e)
						{
							logger.error(e.toString(), e);
						}
					}
				}
				if (stmt != null)
				{
					ResultSet rs = null;
					try
					{
						// use some limits to make sure we don't trash the SQL Server or the web application server
						stmt.setQueryTimeout(VARIABLE_FILL_TIMEOUT);
					}
					catch (Exception e)
					{
						logger.debug(e.getMessage(), e);
					}
					try
					{
						rs = stmt.executeQuery(q);
					} catch (SQLException e)
					{
						if (v.getDefaultIfInvalid() != null && v.getDefaultIfInvalid().length() > 0)
						{
							logger.warn("Variable [" + v.getName() + "] query invalid (cannot execute) - using default value: " + v.getDefaultIfInvalid());
							return (v.getDefaultIfInvalid());
						} else
						{
							logger.error(e.toString(), e);
						}
						return (null);
					}
					try
					{
						if (v.isMultiValue())
						{
							List<Object> values = new ArrayList<Object>();
							while (rs.next())
							{
								// Take the first column
								values.add(rs.getObject(1));
							}
							return (values);
						} else
						{
							if (rs.next())
							{
								if (v.isMultipleColumns())
								{
									return (Object) rs;
								}
								return (rs.getObject(1));
							}
						}
					} catch (SQLException e)
					{
						logger.error(e);
					} finally
					{
						try
						{
							if (rs != null)
								rs.close();
							if (stmt != null && stmtAllocatedHere)
								stmt.close();
						} catch (SQLException e)
						{
							logger.error(e);
						}
					}
				}
			}
		}
		return (null);
	}
	
	public Object fillLiveAccessVariable(DatabaseConnection dc, Variable v, String query)
	{
		if (dc != null && dc.isRealTimeConnection())
		{
			try
			{
				List<Object> values = new ArrayList<Object>();
				DatabaseConnectionStatement statement = new DatabaseConnectionStatement(this, dc);
				List<Object[]> result = statement.executeLiveAccessQuery(query, false);
				if (v.isMultiValue())
				{
					for (Object[] row: result)
					{
						// Take the first column
						values.add(row[0]);
					}
					return (values);
				} else
				{
					if (result.size() > 0)
					{
						if (v.isMultipleColumns())
						{
							return (Object) result;
						}
						Object[] row = result.get(0);
						return (row[0]);
					}
				}
				
			} catch (Exception e)
			{
				if (e instanceof DataUnavailableException || e instanceof SQLException)
				{
					if (v.getDefaultIfInvalid() != null && v.getDefaultIfInvalid().length() > 0)
					{
						logger.warn("Variable [" + v.getName() + "] query cannot be executed - using default value: " + v.getDefaultIfInvalid());
						return (v.getDefaultIfInvalid());
					} else
					{
						logger.error(e.toString(), e);
					}
				}
			}
		}
		return (null);
	}
	
	private String replaceVariableQuery(Session session, Variable v)
	{
		String q = null;
		try
		{
			// do variable replacement (done in the upper section via 'create')
			String query = v.getQuery();
			Repository fillRepository = v.getFillRepository();
			AbstractQueryString aqs = null;
			if (fillRepository == null)
			{
				query = replaceVariables(session, query);
				aqs = AbstractQueryString.getInstance(this, query, useNewQueryLanguage);
			} else
			{
				query = fillRepository.replaceVariables(session, query);
				fillRepository.processUninitializedMetadata(false, false);
				aqs = AbstractQueryString.getInstance(fillRepository, query, useNewQueryLanguage);
			}
			if (v.isLogicalQuery())
			{
				q = aqs.replaceQueryString("Q{" + query + "}", true, session, false, false, null, null, null);
			} else
			{
				q = aqs.replaceQueryString(query, true, session, false, false, null, null, null);
			}
		} catch (SyntaxErrorException ex)
		{
			logger.warn(ex.toString());
			q = v.getQuery();
		} catch (BadColumnNameException ex)
		{
			logger.warn(ex.toString());
			q = v.getQuery();
		} catch (NavigationException ex)
		{
			logger.warn(ex.toString());
			q = v.getQuery();
		} catch (Exception ex)
		{
			logger.error(ex.toString(), ex);
			q = v.getQuery();
		}
		return q;
	} 
	
	/**
	 * Instantiate all repository variables
	 * 
	 * @param stmt
	 */
	public void instantiateRepositoryVariables(Statement stmt, JasperDataSourceProvider jdsp) throws RepositoryException
	{
		repositoryVariables = new HashMap<String, Object>();
		// set SCHEMA first so it can be used in physical query repository variables
		if (getConnections().size() > 0)
		{
			DatabaseConnection dc = getConnections().get(0);
			repositoryVariables.put("SCHEMA", dc.Schema);
		}		
		if (variables != null)
		{
			for (int i = 0; i < variables.length; i++)
			{
				// check for a property setting first smi.Variable=
				String key = "smi." + variables[i].getName();
				String val = System.getProperty(key);
				if (val != null)
				{
					repositoryVariables.put(variables[i].getName(), val);
					logger.info("Setting repository variable " + variables[i].getName() + " from Java System properties");
					continue;
				}
				// don't evaluate these until they are needed
				if (variables[i].isEvaluateOnDemand())
					continue;
				if (variables[i].getType() == Variable.VariableType.Repository)
				{
					// do variable replacement - not perfect, requires ordering
					variables[i].setQuery(replaceVariables(null, variables[i].getQuery()));
					if (variables[i].isConstant())
					{
						repositoryVariables.put(variables[i].getName(), variables[i].getQuery());
					} else
					{
						Object o = fillVariable(stmt, variables[i], null, jdsp);
						if (o != null)
						{
							if (o instanceof java.sql.Date)
								repositoryVariables.put(variables[i].getName(), o.toString());
							else if (o.getClass() == ArrayList.class)
								repositoryVariables.put(variables[i].getName(), o);
							else
								repositoryVariables.put(variables[i].getName(), Util.convertToString(o, true, this, false));
						} else if (variables[i].getDefaultIfInvalid() != null && variables[i].getDefaultIfInvalid().length() > 0)
						{
							repositoryVariables.put(variables[i].getName(), Util.convertToString(variables[i].getDefaultIfInvalid(), true, this, false));
						} else
						{
							throw new RepositoryException("Bad Variable: " + variables[i].getName());
						}
					}
				}
			}
		}
		for (Iterator<Entry<String, Object>> iter = repositoryVariables.entrySet().iterator(); iter.hasNext();)
		{
			try
			{
				Entry<String, Object> entry = (Entry<String, Object>) iter.next();
				Object val = entry.getValue();
				logger.info("Variable: " + entry.getKey() + " = " + (val == null ? "" : val.toString()));
			}
			catch (Exception ex)
			{
				logger.debug("Exception during the output of repository variables - " + ex.getMessage());
			}
		}
		if (logger.isTraceEnabled())
			logger.trace("Repository Variable Initialization Complete.");
	}

	public void setRepositoryVariable(String name, String value)
	{
		repositoryVariables.put(name, value);
		logger.info("Set repository variable '" + name + "' to value '" + replaceVariables(null, "V{" + name + "}") + "'");
	}

	public Object getRepositoryVariable(String name)
	{
		return repositoryVariables.get(name);
	}

	public String replaceVariables(Session session, String s)
	{
		return (replaceVariables(session, s, false, null, null));
	}

	public String replaceVariables(Session session, String s, boolean processWarehouseVariables, Statement stmt, JasperDataSourceProvider jdsp)
	{
		return replaceVariables(session, s, processWarehouseVariables, stmt, jdsp, false, null);
	}

	/**
	 * Replace all instances of variables (denoted by V{NAME}) in a string with their values. Use repository variables
	 * if session variable map is null. Otherwise, replace with session variables If processForSecurityFilter is true,
	 * variable values are single-quoted appropriately
	 * 
	 * @param session
	 * @param s
	 * @param processForSecurityFilter
	 * @return
	 */
	public String replaceVariables(Session session, String s, boolean processWarehouseVariables, Statement stmt, JasperDataSourceProvider jdsp,
			boolean processForSecurityFilter, DatabaseConnection dbc)
	{
		if (s == null)
		{
			return s;
		}
		/*
		 * Replace all repository and session variable references
		 */
		StringBuilder str = new StringBuilder(s);
		boolean found = false;
		int count = 0;
		do
		{
			int pos = 0;
			found = false;
			while ((pos = str.indexOf("V{", pos)) >= 0)
			{
				found = true;
				int epos = str.indexOf("}", pos);
				if (epos > pos)
				{
					String name = str.substring(pos + 2, epos);
					Object o = null;
					if (repositoryVariables != null)
					{
						Variable var = findVariable(name);
						if (var != null && var.isEvaluateOnDemand() && (var.getType() == Variable.VariableType.Repository))
						{
							o = fillVariable(stmt, var, session, jdsp);
							if (o != null && o.getClass().getName().equals("oracle.sql.TIMESTAMP"))
							{
								try
								{
									//convert to java.sql.Timestamp
									Class tzClass = Class.forName("oracle.sql.TIMESTAMP");
									Method timestampValue = tzClass.getMethod("timestampValue", new Class[]{ });
									o = timestampValue.invoke(o, new Object[] { });
								}
								catch (Exception e) {}
							}
							if (o != null)
							{
								if(var.isConstant() || o instanceof java.sql.Date)
								{
									repositoryVariables.put(name, o.toString());
								}
								else
								{
									if (var.isMultiValue())
										repositoryVariables.put(name,o);
									else
										repositoryVariables.put(name, Util.convertToString(o, true, this, false));
								}
								var.setEvaluateOnDemand(false); // fill it from the hash table from this point on
							}
						}
						o = repositoryVariables.get(name);
					}
					if (session != null && o == null)
					{
						o = session.getSessionVariableValue(name, this);
						if (o == null && processWarehouseVariables)
						{
							Variable v = findVariable(name);
							if (v != null && v.getType() == Variable.VariableType.Warehouse)
							{
								session.fillVariable(this, stmt, jdsp, v);
								o = session.getSessionVariableValue(name, this);
							}
						}
					} else if (name.startsWith("="))
					{
						RepositoryExpression re = new RepositoryExpression(this, null, name.substring(1));
						o = re.evaluate();
					}
					if (o != null)
					{
						StringBuilder val = new StringBuilder();
						String valStr = "";
						// If this is a multi-value variable
						if (o.getClass() == ArrayList.class)
						{
							@SuppressWarnings("rawtypes")
							List list = (ArrayList) o;
							// Substitute with comma-separated list of values
							if (!list.isEmpty())
							{
								for (int i = 0; i < list.size(); i++)
								{
									if (i > 0)
										val.append(',');
									if (processForSecurityFilter)
									{
										Variable var = findVariable(name);
										if (var != null && var.isConstant())
										{
											val.append(list.get(i).toString());
										} else
										{
											val.append(Util.convertToString(list.get(i), true, this, true, dbc));
										}
									} else
									{
										val.append(Util.convertToString(list.get(i), false, this, true));
									}
								}
								valStr = val.toString();
							}
						} else
						{
							// This is a single value variable
							if (processForSecurityFilter)
							{
								Variable var = findVariable(name);
								if (var.isConstant())
								{
									valStr = o.toString();
								} else
								{
									valStr = Util.convertToString(o, true, this, true, dbc);
								}
							} else
							{
								valStr = o.toString();
							}
						}
						// look at the context. If the incoming V{} is already in single quotes ('V{name}') and the replaced value is also in
						// single quotes, then remove one of them
						if(isConstantVariable(name) && pos > 0 && epos < str.length() - 1 && valStr.length() > 0 &&
								str.charAt(pos-1) == '\'' && str.charAt(epos + 1) == '\'' &&
								valStr.charAt(0) == '\'' && valStr.charAt(valStr.length()-1) == '\'')
						{
							valStr = valStr.substring(1, valStr.length()-1);
						}
						str.replace(pos, epos + 1, valStr);
					} else
					{
						// logger.warn("Variable [" + name +
						// "] query returned null value. Cannot replace variable value in " + s);
						found = false;
						pos += 2;
					}
					// adding debugging to print out instance ids
					if(o != null)
					{
						if (logger.isTraceEnabled())
							logger.trace("Replaced variable " + name + " with object id " + o.hashCode() + " for repository " + this.applicationName + " with object id " + this.hashCode() );
					}
				} else
				{
					// If didn't find the end, just ignore this prefix
					pos += 2;
				}
			}
		} while (found && (count++ < 15));
		return (str.toString());
	}
	
	private boolean isConstantVariable(String name)
	{
		Variable var = findVariable(name);
		return var != null && var.isConstant() ? true : false;
	}
	/*
	 *  Returns pipe delimited variables(name/value pair)
	 */
	public String getVariablesStr(Session session, Set<String> searchVariables, String spaceID)
	{
		StringBuilder vars = new StringBuilder();
		vars.append("|");
		if (session != null)
		{
			//searchVariables is null then instantiate all variables. Used for old (pre 5.8.3) birst connect.
			boolean includeAll =  (searchVariables == null);
			if (includeAll)
			{
				logger.info("Instantiating all the variables for space: " + spaceID);
			}
			Set<String> replacedVariables = new HashSet<String>();
			for (Variable v : variables)
			{
				if (!includeAll && searchVariables != null && !searchVariables.contains(v.getName()))
				{
					continue;
				}
				String name = "V{" + v.getName() + "}";
				vars.append(name);
				vars.append('=');
				JasperDataSourceProvider jdsp = new JasperDataSourceProvider(this);
				//need to replace char/string data-type values with single quotes so passing true as security filter 
				String value = replaceVariables(session, name, false, null, jdsp, true, null);				
				//returned result for query type variable can be null. Substitute those variables with blank("")
				if (value == null)
					vars.append("");
				else
					vars.append(value);
				vars.append('|');
				replacedVariables.add(v.getName());
			}
			if (!includeAll)
			{
				if (replacedVariables.isEmpty())
				{
					for (String v : searchVariables)
					{
						logger.error("Cannot find variable: " + v);
					}
				}
				else
				{
					replacedVariables.removeAll(searchVariables);
					if (replacedVariables.size() > 0)
					{
						for (String v : replacedVariables)
						{
							logger.error("Cannot find variable: " + v);
						}
					}
				}
			}
		}
		
		return vars.toString();
	}
	/**
	 * Find a given group
	 * 
	 * @param name
	 * @return
	 */
	public Group findGroup(String name)
	{
		return realm.findGroup(name);
	}

	/**
	 * Return a list of groups for the current space/repository.
	 * 
	 * @param filterInternal
	 *            true if internal groups should be filtered out of final result; false to return all groups including
	 *            internal.
	 */
	public List<Group> getGroups(boolean filterInternal)
	{
		return realm.getGroups(filterInternal);
	}

	/**
	 * Return a list of the {@link Group}s that a user belongs to.
	 * 
	 * @param user
	 *            is the user whose GROUPS should be retrieved.
	 * @param session
	 *            is the source of the GROUPS list.
	 * @return the list of {@link Group}s that the user belongs to.
	 */
	public List<Group> getUserGroups(User user, Session session)
	{
		List<Group> glist = realm.getUserGroups(user);
		List<Group> dynamicGroups = getDynamicGroups(session);
		if (dynamicGroups != null && !dynamicGroups.isEmpty())
		{
			for (Group group : dynamicGroups)
			{
				glist.add(group);
			}
		}
		return glist;
	}

	/**
	 * Returns the list of {@link Group}s for user associated via dynamic user group mapping
	 * 
	 * @param session
	 *            - Session object containing populated dynamic groups during login *
	 * 
	 * @return the list of {@link Group}s for user associated via dynamic user group mapping
	 */
	@SuppressWarnings("unchecked")
	public List<Group> getDynamicGroups(Session session)
	{
		List<Group> dynamicGroupList = new ArrayList<Group>();
		if (session != null)
		{
			Object sessionGroups = session.getVariable("Birst$Groups");
			if (sessionGroups != null && sessionGroups instanceof List)
			{
				for (Object sessionGroup : (List<Object>) sessionGroups)
				{
					// Group names can be numeric (bug 6759)
					final String groupName = sessionGroup.toString();
					for (Group group : groups)
					{
						if (group.getName().equals(groupName))
							dynamicGroupList.add(group);
					}
				}
			}
		}
		return dynamicGroupList;
	}

	/**
	 * Return a list of the {@link Group}s that a user belongs to.
	 * 
	 * 
	 * @param session
	 *            contains the information about the logged in user as well as group information
	 * @return the list of {@link Group}s that the user belongs to.
	 */
	public List<Group> getUserGroups(Session session)
	{
		// get the user object of the logged in username from SMIRealm
		// using username from the Session object
		if (session != null)
		{
			String username = session.getUserName();
			if (username != null)
			{
				User u = realm.findUser(username);
				if (u != null)
				{
					return getUserGroups(u, session);
				}
			}
		}
		return null;
	}

	/**
	 * Return whether a user has access to this tag (users in the Administrator group have access to everything)
	 * 
	 * @param u
	 * @return
	 */
	public boolean hasRole(User user, String tag)
	{
		return realm.hasRole(user, tag);
	}

	/**
	 * return whether a user is a 'super user' administrator
	 */
	public boolean isAdministrator(User user)
	{
		return realm.isAdministrator(user);
	}

	/**
	 * find a given username and return the user
	 * 
	 * @param username
	 * @return
	 */
	public User findUser(String username)
	{
		return realm.findUser(username);
	}

	/**
	 * Find dimension table based on name
	 * 
	 * @param name
	 * @return Dimension Table structure
	 */
	public DimensionTable findDimensionTable(String name)
	{
		DimensionTable dim = null;
		for (int count = 0; count < this.DimensionTables.length; count++)
		{
			if (this.DimensionTables[count].TableName.equals(name))
			{
				dim = this.DimensionTables[count];
				break;
			}
		}
		return (dim);
	}

	/**
	 * Find dimension column based on name (first one of it is overloaded)
	 * 
	 * @param dimension
	 * @param cname
	 * @return DimensionColumn structure
	 */
	public DimensionColumn findDimensionColumn(String dimension, String cname)
	{
		List<DimensionColumn> dclist = dimensionColumnMap.get(dimension + "." + cname);
		if (dclist == null)
			return (null);
		return (dclist.get(0));
	}

	/**
	 * Find dimension column that matches a surrogate key name
	 * 
	 * @param skey
	 * @return DimensionColumn structure (null if not found)
	 */
	public DimensionColumn findSurrogateKey(String dimName, String skey)
	{
		for (DimensionTable dt : DimensionTables)
		{
			if (dt.InheritTable != null && dt.InheritTable.length() > 0)
				continue;
			if (dt.VirtualMeasuresInheritTable != null && dt.VirtualMeasuresInheritTable.length() > 0)
				continue;
			if (dimName == null || dimName.equals(dt.DimensionName))
			{
				for (DimensionColumn dc : dt.DimensionColumns)
				{
					if (dc.SurrogateKey && dc.ColumnName.equals(skey))
						return (dc);
				}
			}
		}
		return (null);
	}

	/**
	 * Find a dimension table at a given level
	 * 
	 * @param dimension
	 * @param level
	 * @return
	 */
	public DimensionTable findDimensionTableAtLevel(String dimension, String level)
	{
		return findDimensionTableAtLevel(dimension, level, false);
	}

	/**
	 * Find a dimension table at a given level
	 * 
	 * @param dimension
	 * @param level
	 * @return
	 */
	public DimensionTable findDimensionTableAtLevel(String dimension, String level, boolean isAutoGenerated)
	{
		for (DimensionTable dt : DimensionTables)
		{
			if (dt.InheritTable != null && dt.InheritTable.length() > 0)
				continue;
			if (dt.VirtualMeasuresInheritTable != null && dt.VirtualMeasuresInheritTable.length() > 0)
				continue;
			if (isAutoGenerated && !dt.AutoGenerated)
				continue;
			if (dt.DimensionName.equals(dimension) && dt.Level.equals(level))
				return (dt);
		}
		return (null);
	}

	/**
	 * Find dimension column based on name (all if it is overloaded)
	 * 
	 * @param dimension
	 * @param cname
	 * @return DimensionColumn structure
	 */
	public List<DimensionColumn> findDimensionColumns(String dimension, String cname)
	{
		List<DimensionColumn> dclist = dimensionColumnMap.get(dimension + "." + cname);
		if (dclist == null)
			return (null);
		return (dclist);
	}

	private Map<String, MeasureTable> measureTableNameMap;
	
	/**
	 * Find measure table based on measure table name (first one if it is overloaded)
	 * 
	 * @param measureTableName
	 * @return
	 */
	public MeasureTable findMeasureTableName(String measureTableName)
	{
		if (measureTableNameMap == null || (measureTableNameMap != null && measureTableNameMap.size() != MeasureTables.length))
		{
			measureTableNameMap = new HashMap<String, MeasureTable>();
			for (int count = 0; count < this.MeasureTables.length; count++)
			{
				measureTableNameMap.put(this.MeasureTables[count].TableName, this.MeasureTables[count]);
			}
		}
		return measureTableNameMap.get(measureTableName);
	}
	
	private void invalidateMeasureTableNameMap(){
		measureTableNameMap = null;
	}

	/**
	 * Find measure table based on measure name
	 * 
	 * @param measureName
	 * @return
	 */
	public MeasureTable findMeasureTable(String measureName)
	{
		MeasureColumn mc = findMeasureColumn(measureName);
		if (mc != null)
			return mc.MeasureTable;
		else
			return null;
	}

	/**
	 * Find measure table based on measure name (first if overloaded)
	 * 
	 * @param measureName
	 * @return
	 */
	public MeasureColumn findMeasureColumn(String measureName)
	{
		List<MeasureColumn> mclist = findMeasureColumns(measureName);
		if (mclist == null)
			return null;
		return mclist.get(0);
	}

	/**
	 * Find measure table based on measure name (all if overloaded)
	 * 
	 * @param measureName
	 * @return
	 */
	public List<MeasureColumn> findMeasureColumns(String measureName)
	{
		List<MeasureColumn> mclist = measureColumnMap.get(measureName);
		return mclist;
	}

	/**
	 * Find a filter given a name
	 * 
	 * @param name
	 * @return
	 */
	public QueryFilter findFilter(String name)
	{
		for (int i = 0; i < filters.length; i++)
		{
			if (filters[i].getName().equals(name))
				return (filters[i]);
		}
		return (null);
	}

	/**
	 * Find a success model given a name
	 * 
	 * @param name
	 * @return
	 */
	public SuccessModel findSuccessModel(String name)
	{
		for (int i = 0; i < successModels.length; i++)
		{
			if (successModels[i].Name.equals(name))
				return (successModels[i]);
		}
		return (null);
	}

	/**
	 * Find a search pattern definition given a name
	 * 
	 * @param name
	 * @return
	 */
	public PatternSearchDefinition findSearchDefinition(String name)
	{
		for (int i = 0; i < searchDefinitions.length; i++)
		{
			if (searchDefinitions[i].Name.equals(name))
				return (searchDefinitions[i]);
		}
		return (null);
	}

	/**
	 * Find an outcome definition
	 * 
	 * @param name
	 * @return
	 */
	public Outcome findOutcome(String name)
	{
		for (int i = 0; i < getOutcomes().length; i++)
		{
			if (getOutcomes()[i].Name.equals(name))
				return (getOutcomes()[i]);
		}
		return (null);
	}

	/**
	 * Find a load group given a name
	 * 
	 * @param name
	 * @return
	 */
	public LoadGroup findLoadGroup(String name)
	{
		if ((loadGroups != null) && (loadGroups.length > 0))
		{
			for (int i = 0; i < loadGroups.length; i++)
			{
				if (loadGroups[i].getName().equals(name))
					return (loadGroups[i]);
			}
		}
		return (null);
	}

	/**
	 * Get children of an XML element that form a string array
	 * 
	 * @param e
	 *            Element
	 * @param name
	 *            Name of child array element
	 * @param ns
	 *            Namespace
	 * @return String array result
	 */
	public static String[] getStringArrayChildren(Element e, String name, Namespace ns)
	{
		@SuppressWarnings("rawtypes")
		List l = e.getChildren(name, ns);
		if (l == null)
			return null;
		if (l.isEmpty())
			return (new String[0]); // XXX this is just plain WRONG, but sadly we now have code the depends upon it...
		l = ((Element) l.get(0)).getChildren();
		String[] result = new String[l.size()];
		for (int count = 0; count < l.size(); count++)
		{
			result[count] = ((Element) l.get(count)).getTextTrim();
		}
		return (result);
	}
	
	public static String[] getInternedStringArrayChildren(Element e, String name, Namespace ns)
	{
		@SuppressWarnings("rawtypes")
		List l = e.getChildren(name, ns);
		if (l == null)
			return null;
		if (l.isEmpty())
			return (new String[0]); // XXX this is just plain WRONG, but sadly we now have code the depends upon it...
		l = ((Element) l.get(0)).getChildren();
		String[] result = new String[l.size()];
		for (int count = 0; count < l.size(); count++)
		{
			result[count] = Util.intern(((Element) l.get(count)).getTextTrim());
		}
		return (result);
	}

	/**
	 * Get children of an XML element that form a double string array
	 * 
	 * @param e
	 *            Element
	 * @param name
	 *            Name of child array element
	 * @param ns
	 *            Namespace
	 * @return String array result
	 */
	@SuppressWarnings("rawtypes")
	public static String[][] getDoubleStringArrayChildren(Element e, String name, Namespace ns)
	{
		List l = e.getChildren(name, ns);
		if (l.isEmpty())
			return (new String[0][]);
		l = ((Element) l.get(0)).getChildren();
		String[][] result = new String[l.size()][];
		for (int count = 0; count < l.size(); count++)
		{
			List l2 = ((Element) l.get(count)).getChildren();
			String[] arr = new String[l2.size()];
			for (int count2 = 0; count2 < l2.size(); count2++)
			{
				arr[count2] = ((Element) l2.get(count2)).getTextTrim();
			}
			result[count] = arr;
		}
		return (result);
	}

	/**
	 * Get children of an XML element that form a double string array
	 * 
	 * @param e
	 *            Element
	 * @param name
	 *            Name of child array element
	 * @param ns
	 *            Namespace
	 * @return String array result
	 */
	@SuppressWarnings("rawtypes")
	public static double[][] getDoubleDoubleArrayChildren(Element e, String name, Namespace ns)
	{
		List l = e.getChildren(name, ns);
		if (l.isEmpty())
			return (new double[0][]);
		l = ((Element) l.get(0)).getChildren();
		double[][] result = new double[l.size()][];
		for (int count = 0; count < l.size(); count++)
		{
			List l2 = ((Element) l.get(count)).getChildren();
			double[] arr = new double[l2.size()];
			for (int count2 = 0; count2 < l2.size(); count2++)
			{
				arr[count2] = Double.valueOf(((Element) l2.get(count2)).getTextTrim());
			}
			result[count] = arr;
		}
		return (result);
	}

	/**
	 * Get children of an XML element that form a double HierarchyLevel array
	 * 
	 * @param e
	 *            Element
	 * @param name
	 *            Name of child array element
	 * @param ns
	 *            Namespace
	 * @return String array result
	 */
	@SuppressWarnings("rawtypes")
	public static HierarchyLevel[][] getHierarchyLevelDoubleArrayChildren(Element e, String name, Namespace ns)
	{
		List l = e.getChildren(name, ns);
		if (l.isEmpty())
			return (null);
		l = ((Element) l.get(0)).getChildren();
		HierarchyLevel[][] result = new HierarchyLevel[l.size()][];
		for (int count = 0; count < l.size(); count++)
		{
			List l2 = ((Element) l.get(count)).getChildren();
			HierarchyLevel[] arr = new HierarchyLevel[l2.size()];
			for (int count2 = 0; count2 < l2.size(); count2++)
			{
				arr[count2] = new HierarchyLevel((Element) l2.get(count2), ns);
			}
			result[count] = arr;
		}
		return (result);
	}

	/**
	 * Get children of an XML element that form a ArrayList<String>
	 * 
	 * @param e
	 *            Element
	 * @param name
	 *            Name of child array element
	 * @param ns
	 *            Namespace
	 * @return String array result
	 */
	public static List<String> getStringArrayListChildren(Element e, String name, Namespace ns)
	{
		@SuppressWarnings("rawtypes")
		List l = e.getChildren(name, ns);
		if (l.isEmpty())
			return (new ArrayList<String>(0));
		l = ((Element) l.get(0)).getChildren();
		List<String> result = new ArrayList<String>(l.size());
		for (int count = 0; count < l.size(); count++)
		{
			result.add(((Element) l.get(count)).getTextTrim());
		}
		return (result);
	}
	
	public static List<String> getInternedStringArrayListChildren(Element e, String name, Namespace ns)
	{
		@SuppressWarnings("rawtypes")
		List l = e.getChildren(name, ns);
		if (l.isEmpty())
			return (new ArrayList<String>(0));
		l = ((Element) l.get(0)).getChildren();
		List<String> result = new ArrayList<String>(l.size());
		for (int count = 0; count < l.size(); count++)
		{
			result.add(Util.intern(((Element) l.get(count)).getTextTrim()));
		}
		return (result);
	}

	/**
	 * Get children of an XML document that form an integer array
	 * 
	 * @param e
	 *            Element node
	 * @param name
	 *            Name of child array element
	 * @param ns
	 *            Namespace
	 * @return Integer array result
	 */
	public static int[] getIntArrayChildren(Element e, String name, Namespace ns)
	{
		@SuppressWarnings("rawtypes")
		List l = e.getChildren(name, ns);
		if (l.isEmpty())
			return (new int[0]);
		l = ((Element) l.get(0)).getChildren();
		int[] result = new int[l.size()];
		for (int count = 0; count < l.size(); count++)
		{
			try
			{
				result[count] = Integer.parseInt(((Element) l.get(count)).getTextTrim());
			} catch (NumberFormatException nfe)
			{
				result[count] = 0;
			}
		}
		return (result);
	}

	/**
	 * Get children of an XML document that form a boolean array
	 * 
	 * @param e
	 *            Element node
	 * @param name
	 *            Name of child array element
	 * @param ns
	 *            Namespace
	 * @return Boolean array result
	 */
	public static boolean[] getBooleanArrayChildren(Element e, String name, Namespace ns)
	{
		@SuppressWarnings("rawtypes")
		List l = e.getChildren(name, ns);
		if (l.isEmpty())
			return (new boolean[0]);
		l = ((Element) l.get(0)).getChildren();
		boolean[] result = new boolean[l.size()];
		for (int count = 0; count < l.size(); count++)
		{
			try
			{
				result[count] = Boolean.parseBoolean(((Element) l.get(count)).getTextTrim());
			} catch (NumberFormatException nfe)
			{
				result[count] = false;
			}
		}
		return (result);
	}

	/**
	 * Find a sublevel within a hierarchy
	 * 
	 * @param hierarchy
	 * @param level
	 * @return
	 */
	public Level findLevel(String hierarchy, String level)
	{
		for (int i = 0; i < getHierarchies().length; i++)
		{
			if (getHierarchies()[i].Name.equals(hierarchy))
				return (getHierarchies()[i].findLevel(level));
		}
		return (null);
	}

	/**
	 * Find a level for a given dimension (using a level name)
	 * 
	 * @param hierarchy
	 * @param level
	 * @return
	 */
	public Level findDimensionLevel(String dimension, String level)
	{
		return (dimensionLevelMap.get(dimension + "." + level));
	}

	/**
	 * Find a level for a given dimension (using a column name to search for)
	 * 
	 * @param hierarchy
	 * @param columnName
	 * @return
	 */
	public Level findDimensionLevelColumn(String dimension, String columnName)
	{
		for (int i = 0; i < getHierarchies().length; i++)
		{
			if (getHierarchies()[i].DimensionName.equals(dimension))
				return (getHierarchies()[i].findLevelColumn(columnName));
		}
		return (null);
	}

	/**
	 * Return a hierarchy of a given name
	 * 
	 * @param hierarchy
	 * @return
	 */
	public Hierarchy findHierarchy(String hierarchy)
	{
		for (int i = 0; i < getHierarchies().length; i++)
		{
			if (getHierarchies()[i].Name.equals(hierarchy))
				return (getHierarchies()[i]);
		}
		return (null);
	}

	/**
	 * Return a hierarchy for a given dimension
	 * 
	 * @param dimensionName
	 * @return
	 */
	public Hierarchy findDimensionHierarchy(String dimensionName)
	{
		for (int i = 0; i < getHierarchies().length; i++)
		{
			if (getHierarchies()[i].DimensionName.equals(dimensionName))
				return (getHierarchies()[i]);
		}
		return (null);
	}

	/**
	 * Return full path for application and create directory if necessary
	 * 
	 * @return
	 * @throws SecurityException
	 */
	public String createApplicationPath() throws SecurityException
	{
		String[] pathComponents = getServerParameters().getApplicationPath().split(File.separator.replace("\\", "\\\\"));
		StringBuilder sb = new StringBuilder();
		for (String s : pathComponents)
		{
			if (sb.length() > 0)
				sb.append(File.separator);
			sb.append(s);
			File f = new File(sb.toString());
			if (!f.exists())
			{
				f.mkdir();
			}
		}
		String path = getServerParameters().getApplicationPath() + File.separator + Util.replaceWithPhysicalString(applicationName);
		File f = new File(path);
		if (!f.exists())
		{
			f.mkdir();
		}
		return (path);
	}

	/**
	 * Return list of dimensions
	 * 
	 * @return
	 */
	public List<String> getDimensionList()
	{
		List<String> list = new ArrayList<String>();
		for (int i = 0; i < DimensionTables.length; i++)
		{
			if (!list.contains(DimensionTables[i].DimensionName))
				list.add(DimensionTables[i].DimensionName);
		}
		return (list);
	}

	/**
	 * Return a dimension's key (primary key of dimension key level)
	 * 
	 * @param dname
	 * @return
	 */
	public LevelKey getDimensionKey(String dname)
	{
		// Get hierarchy for dimension
		for (int i = 0; i < getHierarchies().length; i++)
		{
			if (getHierarchies()[i].DimensionName.equals(dname))
			{
				return (getHierarchies()[i].DimensionKeyLevel.Keys[0]);
			}
		}
		return (null);
	}

	/**
	 * Return list of measures
	 * 
	 * @return
	 */
	public List<String> getMeasureList()
	{
		List<String> list = new ArrayList<String>();
		// Include pivot measures
		for (int i = 0; i < virtualColumns.length; i++)
		{
			if (virtualColumns[i].Type == VirtualColumn.PIVOTED)
			{
				list.add(virtualColumns[i].Name);
				for (int j = 0; j < virtualColumns[i].PivotColumns.length; j++)
				{
					list.add(virtualColumns[i].PivotColumns[j]);
				}
			}
		}
		// Normal measures
		for (int i = 0; i < MeasureTables.length; i++)
		{
			for (int j = 0; j < MeasureTables[i].MeasureColumns.length; j++)
			{
				if (!MeasureTables[i].MeasureColumns[j].Key)
				{
					if (!list.contains(MeasureTables[i].MeasureColumns[j].ColumnName))
						list.add(MeasureTables[i].MeasureColumns[j].ColumnName);
				}
			}
		}
		return (list);
	}

	/**
	 * Return list of measures
	 * 
	 * @return
	 */
	public Set<MeasureColumn> getMeasureColumns()
	{
		Set<MeasureColumn> list = new LinkedHashSet<MeasureColumn>();
		// Normal measures
		for (int i = 0; i < MeasureTables.length; i++)
		{
			for (int j = 0; j < MeasureTables[i].MeasureColumns.length; j++)
			{
				MeasureColumn mc = MeasureTables[i].MeasureColumns[j];
				/*
				 * Don't include any columns that are dimensional foreign keys - they include two $ - the first to
				 * separate the dimension from the column name
				 */
				if (mc.ColumnName.contains(".") && MeasureTables[i].TableSource != null && MeasureTables[i].TableSource.connection != null && 
						DatabaseConnection.isDBTypeOracle(MeasureTables[i].TableSource.connection.DBType))
					continue;
				if (!list.contains(mc) && mc.PhysicalName != null
						&& ((!mc.Qualify && !mc.Key) || mc.PhysicalName.indexOf('$') == mc.PhysicalName.lastIndexOf('$')))
					list.add(MeasureTables[i].MeasureColumns[j]);
			}
		}
		return (list);
	}

	/**
	 * Return modeling category for a measure
	 * 
	 * @param mname
	 * @return
	 */
	public String getMeasureCategory(String mname)
	{
		MeasureColumn mc = findMeasureColumn(mname);
		if (mc != null)
			return (mc.PerformanceCategory);
		return (null);
	}

	/**
	 * Return array list of dimension columns
	 * 
	 * @param dname
	 * @return
	 */
	public List<String> getDimensionColumnList(String dname)
	{
		List<String> list = new ArrayList<String>();
		for (int i = 0; i < DimensionTables.length; i++)
		{
			if (DimensionTables[i].DimensionName.equals(dname))
			{
				for (int j = 0; j < DimensionTables[i].DimensionColumns.length; j++)
				{
					if (!list.contains(DimensionTables[i].DimensionColumns[j].ColumnName) && !DimensionTables[i].DimensionColumns[j].Key)
						list.add(DimensionTables[i].DimensionColumns[j].ColumnName);
				}
			}
		}
		return (list);
	}

	/**
	 * Return array list of dimension columns
	 * 
	 * @param dname
	 * @return
	 */
	public List<DimensionColumn> getDimensionColumns(String dname)
	{
		List<DimensionColumn> list = new ArrayList<DimensionColumn>();
		Set<Object> set = new HashSet<Object>();
		for (int i = 0; i < DimensionTables.length; i++)
		{
			if (DimensionTables[i].DimensionName.equals(dname))
			{
				for (int j = 0; j < DimensionTables[i].DimensionColumns.length; j++)
				{
					if (!DimensionTables[i].DimensionColumns[j].SurrogateKey && !set.contains(DimensionTables[i].DimensionColumns[j]))
					{
						list.add(DimensionTables[i].DimensionColumns[j]);
						set.add(DimensionTables[i].DimensionColumns[j]);
					}
				}
			}
		}
		return (list);
	}

	private boolean isExactSameJoin(Join newJoin)
	{
		List<Join> jList = getJoinList(newJoin.Table1Str);
		if (jList != null)
		{
			for (Join j2 : jList)
			{
				/*
				 * See if the exact same join is defined in the inheriting table (i.e. it is overridden)
				 */
				if (((j2.LevelStr == null && newJoin.LevelStr == null) || (j2.LevelStr != null && newJoin.LevelStr != null && j2.LevelStr
						.equals(newJoin.LevelStr)))
						&& ((j2.Table1Str.equals(newJoin.Table1Str) && j2.Table2Str.equals(newJoin.Table2Str)) || (j2.Table2Str.equals(newJoin.Table1Str) && j2.Table1Str
								.equals(newJoin.Table2Str))))
				{
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Return whether a given measure table and dimension table are joined
	 * 
	 * @param mt
	 *            Measure table
	 * @param dt
	 *            Dimension table
	 * @param checkRedundant
	 *            Whether to check if there's a redundant join (vs. any join)
	 * @param checkLevelBased
	 *            Whether to check if there's a level-based join (vs. any join)
	 * @param level
	 *            Level of join to be sought - if null ignore, otherwise only use a join that is specifically at that
	 *            level
	 * @return True/False if they are joined
	 */
	public boolean joinedTables(MeasureTable mt, DimensionTable dt, boolean checkRedundant, boolean checkLevelBased, Level level)
	{
		Join tj = getJoin(mt, dt, level);
		if (tj != null)
		{
			boolean result = checkRedundant ? tj.Redundant : true;
			result = result && !tj.Invalid;
			if (!result)
				return result;
			result = result && ((checkLevelBased && tj.LevelStr != null && level != null) ? tj.LevelStr.equals(level.Name) : true);
			return result;
		}
		return false;
	}

	/**
	 * Get the join between a measure table and a dimension table at a given level
	 * 
	 * @param mt
	 * @param dt
	 * @param level
	 * @return
	 */
	public Join getJoin(MeasureTable mt, DimensionTable dt, Level level)
	{
		buildOther();
		String key = buildSuperKey(mt.TableName, dt.TableName);
		if (!tempJoinedTables.containsKey(key))
			return null;
		else
		{
			Join j = tempJoinedTables.get(key);
			if (j.isAtLevel(level))
				return j;
		}
		Set<Join> a = tempJoinMap.get(mt.TableName);
		if (a != null)
		{
			Set<Join> b = tempJoinMap.get(dt.TableName);
			if (b != null)
			{
				for (Iterator<Join> it = a.iterator(); it.hasNext();)
				{
					Join j = it.next();
					if (b.contains(j) && j.isAtLevel(level))
						return j;
				}
			}
		}
		return null;
	}

	/**
	 * Return whether a given measure table and dimension table are joined
	 * 
	 * @param mt
	 *            Measure table
	 * @param dt
	 *            Dimension table
	 * @return True/False if they are joined
	 */
	public boolean joinedTables(MeasureTable mt, DimensionTable dt)
	{
		return (joinedTables(mt, dt, false, false, null));
	}

	/**
	 * Return whether a given measure table and dimension are joined by a given column
	 * 
	 * @param mt
	 * @param dim
	 * @return True/False if they are joined
	 * @throws NavigationException
	 */
	public boolean joinedToDimensionColumn(MeasureTable mt, String dim, String column) throws NavigationException
	{
		if (joinTableMap.isEmpty())
		{
			for (int i = 0; i < DimensionTables.length; i++)
			{
				if (DimensionTables[i].DimensionName.equals(dim))
				{
					if (joinedTables(mt, DimensionTables[i]))
					{
						for (int j = 0; j < DimensionTables[i].DimensionColumns.length; j++)
						{
							if (DimensionTables[i].DimensionColumns[j].ColumnName.equals(column))
								return (true);
						}
					}
				}
			}
		} else
		{
			List<Object> olist = joinTableMap.get(mt);
			if (olist == null)
			{
				logger.error("No joins defined for measure table: " + mt.TableName);
				throw new NavigationException("Unable to navigate request");
			}
			for (Object o : olist)
			{
				if (DimensionTable.class.isInstance(o))
				{
					DimensionTable dt = (DimensionTable) o;
					if (dt.DimensionName.equals(dim))
						for (DimensionColumn dc : dt.DimensionColumns)
						{
							if (dc.ColumnName.equals(column))
								return (true);
						}
				}
			}
		}
		return (false);
	}

	/**
	 * Return the join between a measure table and a dimension table. Replace logical names with identifiers for tables.
	 * 
	 * @param mt
	 *            Measure table
	 * @param dt
	 *            Dimension table
	 * @param tableMap
	 *            Tablemap mapping table objects to renamed strings
	 * @param dtVirtual
	 *            Whether the dimension table has virtual columns and therefore should be treated like an opaque view
	 * @param level
	 *            Lowest level in the dimension of the dimension table - determines which join to use if level-specific.
	 *            If null, ignored
	 * @return
	 */
	public JoinResult getJoin(MeasureTable mt, DimensionTable dt, QueryMap tableMap, boolean dtVirtual, Level level, Session session)
	{
		List<Join> joinList = getJoinList(mt.TableName);
		for (Join j : joinList)
		{
			/*
			 * Measure table on left
			 */
			boolean process = false;
			boolean reverse = false;
			if (mt.TableName.equals(j.Table1Str) && dt.TableName.equals(j.Table2Str))
			{
				process = true;
			}
			/*
			 * Measure table on right
			 */
			if (mt.TableName.equals(j.Table2Str) && dt.TableName.equals(j.Table1Str))
			{
				process = true;
				reverse = true;
			}
			if (process && !j.isAtLevel(level))
				process = false;
			if (process)
			{
				JoinResult jr = new JoinResult();
				String join = j.JoinCondition;
				if (joinContainsVariable)
				{
					String replacedjoin = replaceVariables(session, join);
					if (join!=null && !join.equals(replacedjoin))
					{
						//replacements for snowflake table  
					 	if (dt.snowflake)  
					 	{  
					 		if (dt.TableSource.Tables.length > 0)  
					 		{  
					 			replacedjoin = Util.replaceStr(replacedjoin,   
					 					"\"" + dt.TableSource.Tables[0].TableName + "\"",   
					 					"\"" + dt.TableName + "\"." + dt.TableSource.Tables[0].PhysicalName);  
					 		}  
					 		for (int i=0; i<dt.TableSource.Tables.length; i++)  
					 		{  
					 			replacedjoin = Util.replaceStr(replacedjoin,   
					 					"\"" + dt.TableSource.Tables[i].TableName + "\"." + dt.TableSource.Tables[i].PhysicalName,  
					 					"\"" + dt.TableName + "\"." + dt.TableSource.Tables[i].PhysicalName);  
					 		}  
					 	}  
					 	join = replacedjoin;  
					 	try  
					 	{  
					 		j = (Join) j.clone();  
					 		j.parseBridgeTables(replacedjoin);  
					 	}  
					 	catch (CloneNotSupportedException ex)  
					 	{}
					}
				}
				if (mt.Type == MeasureTable.OPAQUE_VIEW)
				{
					join = Util.replaceStr(join, "\"" + mt.TableName + "\".", tableMap.getMap(mt) + ".");
				} else
				{
					/*
					 * First, process replacements for all explicit references to tables sources, e.g.: "Table
					 * One".TAB2.ID
					 */
					for (TableDefinition mtd : mt.TableSource.Tables)
						join = Util.replaceStr(join, "\"" + mt.TableName + "\"." + mtd.PhysicalName + ".", tableMap.getMap(mtd) + ".");
					/*
					 * Now, process default replaces just for the logical table names (default to first table source) -
					 * unless it's an opaque view, then use the derived table name
					 */
					join = Util.replaceStr(join, "\"" + mt.TableName + "\".", tableMap.getMap(mt.TableSource.Tables[0]) + ".");
					
					boolean snowflakeJoinClauseContainsVariable = false;  
				 	if (dt.snowflake && dt.TableSource != null && dt.TableSource.Tables != null && dt.TableSource.Tables.length > 1)  
				 	{  
				 		//check if snowflake join condition contains variables  
				 		for (TableDefinition td : dt.TableSource.Tables)  
				 		{  
				 			if (td.JoinClause != null && !td.JoinClause.trim().isEmpty())  
				 			{  
				 				if (Repository.containsVariable(td.JoinClause))  
				 				{  
				 					String replacedJoinClause = replaceVariables(session, td.JoinClause);  
				 					if (!replacedJoinClause.equals(td.JoinClause))  
				 					{  
				 						snowflakeJoinClauseContainsVariable = true;  
				 						break;  
				 					}  
				 				}  
				 			}  
				 		}						  
				 	}  
				 	if (snowflakeJoinClauseContainsVariable)  
				 	{  
				 		for (TableDefinition td : dt.TableSource.Tables)  
				 		{  
				 			if (td.TableName != null && join != null)  
				 				join = join.replace("\"" + td.TableName + "\"." + td.PhysicalName + ".", td.PhysicalName + ".");							  
				 		}  
				 		for (TableDefinition td : dt.TableSource.Tables)  
				 		{  
				 			if (td.TableName != null && join != null)  
				 				join = Util.replaceStr(join, "\"" + td.TableName + "\".", tableMap.getMap(td) + ".");  
				 		}							  
				 	} 
				}
				if (dt.Type == DimensionTable.OPAQUE_VIEW || dtVirtual)
				{
					join = Util.replaceStr(join, "\"" + dt.TableName + "\".", tableMap.getMap(dt) + ".");
					/*
					 * Remove any table source qualifiers - "Table One".TAB1.ID. These don't work in opaque views and
					 * virtual columns. To use them correctly, any column which appears in the join condition should
					 * also be labeled as a key column. This will ensure that they are also pulled into the virtual
					 * dimension table and can be referenced in the join condition. If that has been done, then all we
					 * need to do is remove any qualifiers
					 */
					for (TableDefinition td : dt.TableSource.Tables)
						join = Util.replaceStr(join, "." + td.PhysicalName + ".", ".");
				} else
				{
					/*
					 * First, process replacements for all explicit references to tables sources, e.g.: "Table
					 * One".TAB2.ID
					 */
					for (TableDefinition dtd : dt.TableSource.Tables)
					{
						join = Util.replaceStr(join, "\"" + dt.TableName + "\"." + dtd.PhysicalName + ".", tableMap.getMap(dtd) + ".");
					}
					/*
					 * Now, process default replaces just for the logical table names (default to first table source) -
					 * unless it's an opaque view, then use the derived table name
					 */
					join = Util.replaceStr(join, "\"" + dt.TableName + "\".", tableMap.getMap(dt.TableSource.Tables[0]) + ".");
				}
				jr.joinCondition = join;
				/*
				 * Reverse the direction of the join - because the measure table is always first in the query. So if the
				 * measure table is second and the query is supposed to be a left outer join, then when produced, it
				 * should be a right outer join
				 */
				jr.type = j.Type;
				if (reverse)
				{
					if (jr.type == JoinType.LeftOuter)
						jr.type = JoinType.RightOuter;
					else if (jr.type == JoinType.RightOuter)
						jr.type = JoinType.LeftOuter;
				}
				jr.redundant = j.Redundant;
				jr.setBridgeTables(j.BridgeTables);
				/*
				 * Replace any bridge table names in the join condition
				 */
				if (jr.bridgeTables != null)
				{
					for (JoinResult.BridgeTable bt : jr.bridgeTables)
					{
						jr.joinCondition = Util.replaceStr(jr.joinCondition, bt.toString(), tableMap.getMap(bt));
					}
				}
				if (j.LevelStr != null)
					jr.levelSpecificJoin = true;
				return (jr);
			}
		}
		return (null);
	}

	/**
	 * Process all virtual columns. Go through the virtual column list and either create new measure tables or change
	 * existing dimension tables to include virtual columns (replace tables with opaque views)
	 * 
	 * @param stmt
	 * @param useTemps
	 * @param createTemps
	 * @throws NavigationException
	 * @throws SyntaxErrorException
	 * @throws RepositoryException
	 * @throws IOException
	 * @throws ScriptException 
	 * @throws ArrayIndexOutOfBoundsException 
	 * @throws Exception
	 */
	private void processVirtualColumns(Statement stmt) throws SQLException, BaseException, CloneNotSupportedException,
			IOException, RepositoryException, ArrayIndexOutOfBoundsException
	{
		/*
		 * Process all dimension tables first
		 */
		for (int i = 0; i < DimensionTables.length; i++)
		{
			DimensionTables[i].processVirtualColumns(this);
		}
		for (int i = 0; i < virtualColumns.length; i++)
		{
			// Pivoted Derived Attribute
			if (virtualColumns[i].Type == 2)
			{
				processPivotMeasure(virtualColumns[i], stmt);
			}
		}
	}

	private void processPivotMeasure(VirtualColumn vc, Statement stmt) throws SQLException, BaseException, CloneNotSupportedException,
			ArrayIndexOutOfBoundsException, IOException, RepositoryException
	{
		// Get the measure and dimensionTables
		MeasureColumn mc = findMeasureColumn(vc.Measure);
		DimensionColumn pivdc = findDimensionColumn(vc.PivotDimension, vc.PivotLevel);
		DimensionTable pivdim = pivdc.DimensionTable;
		/*
		 * Add grouping for all keys to all dimensions that join to the base measure table
		 */
		List<DimensionColumn> groupingKeys = new ArrayList<DimensionColumn>();
		List<String> groupingDimensions = new ArrayList<String>();
		if (mc.MeasureTable.Type == MeasureTable.NORMAL)
			for (int j = 0; j < DimensionTables.length; j++)
			{
				if (joinedTables(mc.MeasureTable, DimensionTables[j]))
				{
					if (!groupingDimensions.contains(DimensionTables[j].DimensionName))
					{
						if (!DimensionTables[j].DimensionName.equals(pivdim.DimensionName))
						{
							Hierarchy h = findHierarchy(DimensionTables[j].DimensionName);
							DimensionColumn dc = null;
							if (h == null)
							{
								dc = DimensionTables[j].getFirstKeyColumn();
							} else
							{
								LevelKey lk = h.DimensionKeyLevel.Keys[0];
								dc = DimensionTables[j].findColumn(lk.ColumnNames[0]);
							}
							if (dc == null)
							{
								logger.error("No Dimension Key for Dimension: " + DimensionTables[j].DimensionName);
								throw (new NavigationException("No Dimension Key for Dimension"));
							}
							groupingKeys.add(dc);
							groupingDimensions.add(DimensionTables[j].DimensionName);
						}
					}
				}
			}
		/*
		 * Get all of the distinct column values for pivot dimension column
		 */
		StringBuilder query = new StringBuilder("SELECT DISTINCT ");
		query.append(pivdc.PhysicalName);
		query.append(" FROM");
		for (int i = 0; i < pivdim.TableSource.Tables.length; i++)
		{
			if (i > 0)
			{
				query.append(' ');
				query.append(Join.getJoinTypeStr(pivdim.TableSource.Tables[i].JoinType, null));
				query.append(' ');
			}
			query.append(pivdim.TableSource.Tables[i].PhysicalName);
			if (i > 0 && pivdim.TableSource.Tables[i].JoinClause.length() > 0)
			{
				query.append(" ON ");
				query.append(pivdim.TableSource.Tables[i].JoinClause);
			}
		}
		query.append(" ORDER BY ");
		ResultSet rs = null;
		List<String> attValList = new ArrayList<String>();
		try
		{
			rs = stmt.executeQuery(query.toString());
			while (rs.next())
			{
				attValList.add(rs.getString(1));
			}
		} catch (Exception ex)
		{
			logger.error("Pivot Query Error: " + ex.toString());
			return;
		} finally
		{
			try
			{
				if (rs != null)
					rs.close();
			} catch (SQLException e)
			{
				logger.debug(e, e);
			}
		}
		// If no results, skip
		if (!attValList.isEmpty())
		{
			// Create appropriate subquery
			Query q = new Query(this);
			// Add the key columns
			for (int k = 0; k < groupingKeys.size(); k++)
			{
				DimensionColumn dc = (DimensionColumn) groupingKeys.get(k);
				q.addDimensionColumn(dc.DimensionTable.DimensionName, dc.ColumnName, dc.DimensionTable.DimensionName.replace(" ", "") + "KEY");
				q.addGroupBy(new GroupBy(dc.DimensionTable.DimensionName, dc.ColumnName));
			}
			q.addFilterList(vc.Filters);
			q.addHiddenDimensionColumn(pivdim.DimensionName, vc.PivotLevel);
			vc.PivotColumns = new String[attValList.size()];
			/*
			 * Generate the query first to navigate to the right dimension table
			 */
			q.addMeasureColumn(mc.ColumnName, mc.ColumnName, false);
			q.generateQuery(false);
			pivdc = q.getNavigatedDimensionColumn(pivdim.DimensionName, pivdc.ColumnName);
			mc = (MeasureColumn) ((QueryColumn) q.getReturnProjectionListColumn(q.getReturnProjectionListSize() - 1)).o;
			// Add each of the individual pivot measures
			for (int j = 0; j < attValList.size(); j++)
			{
				String pstring = null;
				String aggRule = mc.AggregationRule.toUpperCase();
				if (aggRule.equals("COUNT DISTINCT"))
				{
					// pstring = "COUNT(DISTINCT CASE WHEN "+q.getTableMap(pivdc.DimensionTable)+"."+pivdc.PhysicalName
					// +"='"+attValList.get(j)+"' THEN "+q.getTableMap(mc.MeasureTable)+"."+mc.PhysicalName
					// +" ELSE "+nonVal+" END)";
					// if (vc.PivotPercent)
					// {
					// pstring = pstring+"/COUNT(DISTINCT "+q.getTableMap(mc.MeasureTable)+"."+mc.PhysicalName+")";
					// }
					// } else
					// {
					// pstring = mc.AggregationRule+"(CASE WHEN "+q.getTableMap(pivdc.DimensionTable)+"."
					// +pivdc.PhysicalName+"='"+attValList.get(j)+"' THEN "+q.getTableMap(mc.MeasureTable)+"."
					// +mc.PhysicalName+" ELSE "+nonVal+" END)";
					// if (vc.PivotPercent)
					// {
					// pstring = pstring+"/"+mc.AggregationRule+"("+q.getTableMap(mc.MeasureTable)+"."+mc.PhysicalName
					// +")";
					// }
				}
				q.addConstantColumn(pstring, mc.PhysicalName + j);
			}
			q.generateQuery(false);
			// Create a new measure table
			MeasureTable newmt = new MeasureTable();
			// newmt.PhysicalTableName = "MT"+vc.Name+"$";
			newmt.OpaqueView = q.getQuery();
			newmt.Cardinality = mc.MeasureTable.Cardinality;
			newmt.TableName = vc.TableName;
			newmt.Type = 1;
			newmt.MeasureColumns = new MeasureColumn[groupingKeys.size() + attValList.size()];
			newmt.TTL = -1;
			/*
			 * Add key columns for all the dimension keys First, expand join list
			 */
			List<Join> joinList = new ArrayList<Join>(groupingKeys.size());
			for (int j = 0; j < groupingKeys.size(); j++)
			{
				MeasureColumn newmc = new MeasureColumn();
				DimensionColumn kdc = (DimensionColumn) ((QueryColumn) q.getReturnProjectionListColumn(j)).o;
				newmc.ColumnName = Util.intern(kdc.DimensionTable.DimensionName.replace(" ", "") + "KEY");
				newmc.DataType = "Integer";
				newmc.Key = true;
				newmc.AggregationRule = "";
				newmc.MeasureTable = newmt;
				newmc.PhysicalName = Util.intern(kdc.DimensionTable.DimensionName.replace(" ", "") + "KEY");
				newmc.TableName = newmt.TableName;
				newmc.PerformanceCategory = "";
				newmt.MeasureColumns[j] = newmc;
				addMeasureColumnToMap(newmc);
				// Need to add join to join list
				Join jn = new Join();
				jn.Table1 = findMeasureTableName(newmt.TableName);
				jn.Table2 = findDimensionTable(kdc.TableName);
				jn.JoinCondition = "\"" + newmt.TableName + "\"." + newmc.PhysicalName + "=\"" + kdc.DimensionTable.TableName + "\"." + kdc.PhysicalName;
				joinList.add(jn);
			}
			addJoins(joinList);
			// Add the new measure columns
			for (int j = 0; j < attValList.size(); j++)
			{
				MeasureColumn newmc = new MeasureColumn();
				newmc.ColumnName = mc.ColumnName + ": " + attValList.get(j);
				newmc.DataType = "Number";
				newmc.Key = false;
				newmc.AggregationRule = mc.AggregationRule;
				newmc.PerformanceCategory = "";
				newmc.MeasureTable = newmt;
				newmc.PhysicalName = mc.PhysicalName + j;
				newmc.TableName = newmt.TableName;
				if (vc.PivotPercent)
				{
					newmc.Format = "##.0%";
					newmc.AggregationRule = "AVG";
				} else
				{
					newmc.Format = mc.Format;
				}
				newmt.MeasureColumns[j + groupingKeys.size()] = newmc;
				// Save pivot column
				addMeasureColumnToMap(newmc);
				vc.PivotColumns[j] = newmc.ColumnName;
			}
			/*
			 * Add new measure table to the current measure table list
			 */
			MeasureTable[] mtables = new MeasureTable[MeasureTables.length + 1];
			for (int j = 0; j < MeasureTables.length; j++)
			{
				mtables[j] = MeasureTables[j];
			}
			mtables[mtables.length - 1] = newmt;
			MeasureTables = mtables;
			invalidateMeasureTableNameMap();
		}
	}

	/**
	 * Process all aggregate tables, create structures for navigation and create database tables if specified
	 * 
	 * @param stmt
	 *            Statement object to use
	 * @param persist
	 *            Whether to persist the aggregate tables to the database
	 * @throws RepositoryException
	 * @throws SyntaxErrorException
	 * @throws IOException
	 * @throws ScriptException 
	 * @throws Exception
	 */
	private void processAggregateTables(Statement stmt, boolean persist) throws SQLException, BaseException, IOException,
			RepositoryException, CloneNotSupportedException
	{
		if (!noAggregates)
		{
			// Process each aggregate sequentially
			for (int i = 0; i < aggregates.length; i++)
			{
				Aggregate a = aggregates[i];
				if (a.isDisabled())
				{
					logger.info("Aggregate " + a.getName() + " is disabled. Aggregate will not be processed.");
				} else
				{
					
					String connectionName = null;
					if (connectionName == null)
						connectionName = (a.getConnectionName() == null || a.getConnectionName().isEmpty()) ? Repository.DEFAULT_DB_CONNECTION : a
								.getConnectionName();
					DatabaseConnection dc = findConnection(connectionName);
					String aggregateTableName = this.getCurrentBuilderVersion() >=26 ? Util.replaceWithPhysicalString(a.getName()) : Util.replaceWithPhysicalString(dc,a.getName(),this);
					if(this.getCurrentBuilderVersion() >=26)
					{
						if(aggregateTableName!=null && aggregateTableName.length() >= DatabaseConnection.getIdentifierLength(dc.DBType)-4)
						{
							aggregateTableName = dc.getHashedIdentifier(aggregateTableName);
						}
					}
					boolean tableExists = Database.tableExists(this, dc, aggregateTableName, true);
					if (!persist && !tableExists)
					{
						logger.warn("Table " + aggregateTableName + " for aggregate " + a.getName()
								+ " does not exist in the database. Aggregate will not be processed.");
					} else
					{
						Session s = null;
						try
						{
							s = new Session();
							a.process(this, persist, s);
						}
						catch (Exception ex)
						{
							logger.warn("Table " + aggregateTableName + " for aggregate " + a.getName()
									+ "  encountered an exception when processin. Aggregate will not be processed. " + ex.getMessage(), ex);
						}
						finally
						{
							if (s != null)
								s.setCancelled(true);
						}
					}
				}
			}
		} else
		{
			logger
					.info("No Aggregates will be processed while repository initialization because repository is being intialized with noaggregates flag set to true.");
		}
	}

	/**
	 * return a reference population based upon a name
	 */
	public ReferencePopulation findReferencePopulation(String name)
	{
		if (referencePopulations == null)
			return null;
		for (ReferencePopulation refpop : referencePopulations)
		{
			if (refpop.getName().equals(name))
				return refpop;
		}
		return null;
	}

	/**
	 * return a performance model based upon a name
	 */
	public PerformanceModel findPerformanceModel(String name)
	{
		if (performanceModels == null)
			return null;
		for (PerformanceModel pmodel : performanceModels)
		{
			if (pmodel.getName().equals(name))
				return pmodel;
		}
		return null;
	}

	/**
	 * Return an aggregate based on a name
	 * 
	 * @param name
	 * @return
	 */
	public Aggregate findAggregate(String name)
	{
		if (aggregates == null)
			return (null);
		for (Aggregate a : aggregates)
		{
			if (a.getName().equals(name))
				return (a);
		}
		return (null);
	}

	public Aggregate[] getAggregates()
	{
		return aggregates;
	}

	public void setAggregates(Aggregate[] aggregates)
	{
		this.aggregates = aggregates;;
	}

	
	/**
	 * Return a resource with a given name
	 * 
	 * @param name
	 * @return
	 */
	public Resource getResource(String name)
	{
		return (resources.get(name));
	}

	/**
	 * Return a list of resources with a given type and area. If either is null, it is ignored
	 * 
	 * @param type
	 * @param area
	 * @return
	 */
	public List<Resource> getResourcesByType(String type, String area)
	{
		List<Resource> results = new ArrayList<Resource>();
		for (Iterator<Entry<String, Resource>> it = resources.entrySet().iterator(); it.hasNext();)
		{
			Resource r = it.next().getValue();
			if ((type == null || r.getType().equals(type)) && (area == null || r.getArea().equals(area)))
			{
				results.add(r);
			}
		}
		return (results);
	}

	public static class Resource implements Serializable
	{
		private static final long serialVersionUID = 1L;
		private String name;
		private String type;
		private String area;
		private String value;
		private String description;

		public void setValue(String value)
		{
			this.value = value;
		}

		public String getValue()
		{
			return value;
		}

		public void setArea(String area)
		{
			this.area = area;
		}

		public String getArea()
		{
			return area;
		}

		public void setType(String type)
		{
			this.type = type;
		}

		public String getType()
		{
			return type;
		}

		public String getName()
		{
			return name;
		}

		public void setName(String name)
		{
			this.name = name;
		}
		
		public Element getResourceElement(Namespace ns)
		{
			Element e = new Element("Resource",ns);
			
			XmlUtils.addContent(e, "Name", name,ns);
			
			XmlUtils.addContent(e, "Type", type,ns);
			
			XmlUtils.addContent(e, "Area", area,ns);
			
			XmlUtils.addContent(e, "Description", description,ns);
			
			XmlUtils.addContent(e, "Value", value,ns);					
			
			return e;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}
	}

	public static class ACLItem implements Serializable
	{
		private static final long serialVersionUID = 1L;
		private String group;
		private String tag;
		private boolean access;

		public void setGroup(String group)
		{
			this.group = group;
		}

		public String getGroup()
		{
			return group;
		}

		public void setTag(String tag)
		{
			this.tag = tag;
		}

		public String getTag()
		{
			return tag;
		}

		public void setAccess(boolean access)
		{
			this.access = access;
		}

		public boolean isAccess()
		{
			return access;
		}
		
		public Element getACLItemElement(Namespace ns)
		{
			Element e = new Element("ACLItem",ns);
			
			Element child = new Element("Group",ns);
			child.setText(group);
			e.addContent(child);
			
			child = new Element("Tag",ns);
			child.setText(tag);
			e.addContent(child);
			
			child = new Element("Access",ns);
			child.setText(String.valueOf(access));
			e.addContent(child);
					
			return e;
		}
	}

	/**
	 * Instantiate an opaque for a table
	 * 
	 * @param tableName
	 *            name of the opaque view
	 * @param create
	 *            create the table in the database rather than just mark it as NORMAL
	 */
	public void instantiateOpaqueView(String tableName, boolean create) throws SQLException
	{
		String opaqueView = null;
		String physicalName = null;
		List<String> indexColumns = new ArrayList<String>();
		MeasureTable mt = findMeasureTableName(tableName);
		if (mt == null)
		{
			DimensionTable dt = findDimensionTable(tableName);
			if (dt == null)
				return;
			dt.Type = DimensionTable.NORMAL;
			if (dt.TableSource == null || dt.TableSource.Tables == null || dt.TableSource.Tables.length == 0)
			{
				dt.TableSource = new TableSource();
				dt.TableSource.connection = findConnection(Repository.DEFAULT_DB_CONNECTION);
				dt.TableSource.Tables = new TableDefinition[1];
				dt.TableSource.Tables[0] = new TableDefinition();
				dt.TableSource.Tables[0].PhysicalName = dt.PhysicalName;
			}
			if (!create)
				return;
			opaqueView = dt.OpaqueView;
			if (dt.PhysicalName != null)
				physicalName = dt.PhysicalName;
			else
			{
				physicalName = dt.TableName.replace(' ', '_').replace('$', 'D').replace('#', 'N');
				dt.PhysicalName = physicalName;
			}
			for (int i = 0; i < dt.DimensionColumns.length; i++)
			{
				if (dt.DimensionColumns[i].Key)
					indexColumns.add(Util.unQualifyPhysicalName(dt.DimensionColumns[i].PhysicalName));
			}
		} else
		{
			mt.Type = MeasureTable.NORMAL;
			if (mt.TableSource == null || mt.TableSource.Tables == null || mt.TableSource.Tables.length == 0)
			{
				mt.TableSource = new TableSource();
				mt.TableSource.connection = findConnection(Repository.DEFAULT_DB_CONNECTION);
				mt.TableSource.Tables = new TableDefinition[1];
				mt.TableSource.Tables[0] = new TableDefinition();
				mt.TableSource.Tables[0].PhysicalName = mt.PhysicalName;
			}
			if (!create)
				return;
			opaqueView = mt.OpaqueView;
			if (mt.PhysicalName != null)
				physicalName = mt.PhysicalName;
			else
			{
				physicalName = mt.TableName.replace(' ', '_').replace('$', 'D').replace('#', 'N');
				mt.PhysicalName = physicalName;
			}
			/*
			 * Go through all measure tables and make sure those that are virtual tables derived from this one are also
			 * instantiated
			 */
			for (MeasureTable imt : MeasureTables)
			{
				if ((imt.BaseTable != null) && imt.BaseTable.equals(tableName))
				{
					imt.Type = 0;
					imt.PhysicalName = mt.PhysicalName;
				}
			}
			for (int i = 0; i < mt.MeasureColumns.length; i++)
			{
				if (mt.MeasureColumns[i].Key)
					indexColumns.add(Util.unQualifyPhysicalName(mt.MeasureColumns[i].PhysicalName));
			}
		}
		if ((opaqueView == null) || (opaqueView.isEmpty()) || (physicalName == null) || (physicalName.isEmpty()))
		{
			return;
		}
		// replace variables, such as V{CurrentMonthID}
		opaqueView = replaceVariables(null, opaqueView);
		boolean created = Database.createTableAs(this, findConnection(Repository.DEFAULT_DB_CONNECTION), physicalName, opaqueView, false);
		// Create the indexes
		if (created)
		{
			for (String colName : indexColumns)
			{
				Database.createIndex(findConnection(Repository.DEFAULT_DB_CONNECTION), colName, physicalName);
			}
		}
	}

	/**
	 * Drop an opaque for for a table
	 * 
	 * @param table
	 */
	public void dropOpaqueView(String tableName) throws SQLException
	{
		String physicalName = null;
		MeasureTable mt = findMeasureTableName(tableName);
		if (mt == null)
		{
			DimensionTable dt = findDimensionTable(tableName);
			if (dt == null)
				return;
			dt.Type = DimensionTable.OPAQUE_VIEW;
			if (dt.PhysicalName != null)
			{
				physicalName = dt.PhysicalName;
			} else
			{
				physicalName = dt.TableName.replace(' ', '_').replace('$', 'D').replace('#', 'N');
			}
		} else
		{
			mt.Type = MeasureTable.OPAQUE_VIEW;
			if (mt.PhysicalName != null)
			{
				physicalName = mt.PhysicalName;
			} else
			{
				physicalName = mt.TableName.replace(' ', '_').replace('$', 'D').replace('#', 'N');
			}
			// Turn all inherited tables back as well
			for (MeasureTable imt : MeasureTables)
			{
				if ((imt.BaseTable != null) && imt.BaseTable.equals(tableName))
				{
					imt.Type = MeasureTable.OPAQUE_VIEW;
				}
			}
		}
		if (physicalName == null || physicalName.isEmpty())
			return;
		Database.dropTable(this, findConnection(Repository.DEFAULT_DB_CONNECTION), physicalName, true);
	}
	private String defaultConnection = DEFAULT_DB_CONNECTION;

	public DatabaseConnection getDefaultConnection()
	{
		return findConnection(defaultConnection);
	}

	public void setDefaultConnection(String name)
	{
		defaultConnection = name;
	}

	/**
	 * Find a database connection based on its name
	 * 
	 * @param name
	 * @return
	 */
	public DatabaseConnection findConnection(String name)
	{
		if (name == null)
		{
			if (this.defaultConnection != null)
				return findConnection(this.defaultConnection);
			logger.warn("Requested connection with 'null' connection name, returning the first one; you probably want to fix this");
			return getConnections().get(0);
		}
		for (DatabaseConnection conn : getConnections())
		{
			if (name.equals(DEFAULT_DB_CONNECTION) && conn.Name.equals(this.defaultConnection))
				return (conn);
		}
		for (DatabaseConnection conn : getConnections())
		{
			if (conn.Name.equals(name))
				return (conn);
		}
		// fall back mode, only 1 connection
		if (getConnections().size() > 0 && name.equals(Repository.DEFAULT_DB_CONNECTION))
		{
			logger.warn("Requested 'Default Connection', but it does not exist, returning the first one; you probably want to fix this");
			return getConnections().get(0);
		}
		return (null);
	}

	public String getOrOperatorForMultiValueFilter()
	{
		return orOperatorForMultiValueFilter;
	}

	public String getAndOperatorForMultiValueFilter()
	{
		return andOperatorForMultiValueFilter;
	}

	public String getSeparatorForListPromptOptions()
	{
		return separatorForListPromptOptions;
	}

	/**
	 * Add measure tables to the repository
	 * 
	 * @param tables
	 *            List of tables to add
	 */
	public synchronized void addMeasureTables(List<MeasureTable> tables)
	{
		MeasureTable[] curTables = this.MeasureTables;
		MeasureTable[] newTables = new MeasureTable[curTables.length + tables.size()];
		System.arraycopy(curTables, 0, newTables, 0, curTables.length);
		for (int i = 0; i < tables.size(); i++)
		{
			MeasureTable mt = tables.get(i);
			newTables[i + curTables.length] = mt;
			for (MeasureColumn mc : mt.MeasureColumns)
				addMeasureColumnToMap(mc);
		}
		this.MeasureTables = newTables;
		invalidateMeasureTableNameMap();
	}

	/**
	 * Add a single measure table to the repository
	 * 
	 * @param mt
	 */
	public synchronized void addMeasureTable(MeasureTable mt)
	{
		List<MeasureTable> list = new ArrayList<MeasureTable>(1);
		list.add(mt);
		addMeasureTables(list);
	}

	/**
	 * Remove measure tables from the repository
	 * 
	 * @param tables
	 *            List of tables to remove
	 */
	public synchronized void removeMeasureTables(List<MeasureTable> tables)
	{
		for (MeasureTable mt : tables)
		{
			for (MeasureColumn mc : mt.MeasureColumns)
				removeMeasureColumnFromMap(mc);
		}
		List<MeasureTable> newList = new ArrayList<MeasureTable>(Arrays.asList(this.MeasureTables));
		newList.removeAll(tables);
		MeasureTable[] newTables = new MeasureTable[newList.size()];
		newList.toArray(newTables);
		this.MeasureTables = newTables;
		invalidateMeasureTableNameMap();
	}

	/**
	 * Add dimension tables to the repository
	 * 
	 * @param tables
	 *            List of tables to add
	 */
	public synchronized void addDimensionTables(List<DimensionTable> tables)
	{
		DimensionTable[] curTables = this.DimensionTables;
		DimensionTable[] newTables = new DimensionTable[curTables.length + tables.size()];
		for (int i = 0; i < curTables.length; i++)
			newTables[i] = curTables[i];
		for (int i = 0; i < tables.size(); i++)
		{
			DimensionTable dt = tables.get(i);
			newTables[i + curTables.length] = dt;
			for (DimensionColumn dc : dt.DimensionColumns)
				addDimensionColumnToMap(dc);
		}
		this.DimensionTables = newTables;
	}

	/**
	 * Add a single dimension table to the repository
	 * 
	 * @param dt
	 */
	public synchronized void addDimensionTable(DimensionTable dt)
	{
		List<DimensionTable> list = new ArrayList<DimensionTable>(1);
		list.add(dt);
		addDimensionTables(list);
	}

	/**
	 * Remove dimension tables from the repository
	 * 
	 * @param tables
	 *            List of tables to remove
	 */
	public synchronized void removeDimensionTables(List<DimensionTable> tables)
	{
		if (tables == null || tables.isEmpty())
			return;
		for (DimensionTable dt : tables)
		{
			for (DimensionColumn dc : dt.DimensionColumns)
				this.removeDimensionColumnFromMap(dc);
		}
		List<DimensionTable> newList = new ArrayList<DimensionTable>(Arrays.asList(this.DimensionTables));
		newList.removeAll(tables);
		DimensionTable[] newTables = new DimensionTable[newList.size()];
		newList.toArray(newTables);
		this.DimensionTables = newTables;
	}

	/**
	 * Add joins to the repository
	 * 
	 * @param tables
	 */
	public synchronized void addJoins(List<Join> jlist)
	{
		Join[] curJoins = this.joins;
		Join[] newJoins = new Join[curJoins.length + jlist.size()];
		System.arraycopy(curJoins, 0, newJoins, 0, curJoins.length);
		for (int i = 0; i < jlist.size(); i++)
		{
			Join j = jlist.get(i);
			addJoinToMaps(j);
			newJoins[i + curJoins.length] = j;
			updateTempStructures(j);
		}
		this.joins = newJoins;
	}

	/**
	 * Add a single join to the repository
	 * 
	 * @param j
	 */
	public synchronized void addJoin(Join j)
	{
		List<Join> list = new ArrayList<Join>(1);
		list.add(j);
		addJoins(list);
	}

	/**
	 * Remove all joins related to measure tables in a list
	 * 
	 * @param tables
	 *            List of measure tables
	 */
	public synchronized void removeJoins(List<MeasureTable> tables)
	{
		if (tables == null || tables.isEmpty())
			return;
		List<Join> newList = new ArrayList<Join>(Arrays.asList(this.joins));
		List<Join> removeList = new ArrayList<Join>();
		for (Join j : newList)
		{
			boolean found = false;
			for (MeasureTable mt : tables)
			{
				if (j.Table1 == mt || j.Table2 == mt)
				{
					found = true;
					break;
				}
			}
			if (found)
				removeList.add(j);
		}
		for (Join j : removeList)
			removeJoinFromMaps(j);
		newList.removeAll(removeList);
		Join[] newJoins = new Join[newList.size()];
		newList.toArray(newJoins);
		this.joins = newJoins;
		invalidateTempStructures();
	}

	/**
	 * Remove all joins related to dimension tables in a list
	 * 
	 * @param tables
	 *            List of dimension tables
	 */
	public synchronized void removeDimensionTableJoins(List<DimensionTable> tables)
	{
		if (tables == null || tables.isEmpty())
			return;
		List<Join> newList = new ArrayList<Join>(Arrays.asList(this.joins));
		List<Join> removeList = new ArrayList<Join>();
		for (Join j : newList)
		{
			boolean found = false;
			for (DimensionTable dt : tables)
			{
				if (j.Table1 == dt || j.Table2 == dt)
				{
					found = true;
					break;
				}
			}
			if (found)
				removeList.add(j);
		}
		for (Join j : removeList)
			removeJoinFromMaps(j);
		newList.removeAll(removeList);
		Join[] newJoins = new Join[newList.size()];
		newList.toArray(newJoins);
		this.joins = newJoins;
		invalidateTempStructures();
	}

	/**
	 * Add a query aggregate to the repository
	 * 
	 * @param qa
	 */
	public synchronized void addQueryAggregate(QueryAggregate qa)
	{
		queryAggregates.add(qa);
	}

	/**
	 * Remove a query aggregate from the repository
	 * 
	 * @param qa
	 */
	public synchronized void removeQueryAggregate(QueryAggregate qa)
	{
		queryAggregates.remove(qa);
	}

	/**
	 * Search for a query aggregate with a given name
	 * 
	 * @param name
	 * @return
	 */
	public QueryAggregate findQueryAggregate(String name)
	{
		name = Util.replaceWithPhysicalString(name);
		for (QueryAggregate qa : queryAggregates)
		{
			if (qa.getName().equals(name))
				return (qa);
		}
		return (null);
	}

	/**
	 * Drop an aggregate table associated with a query aggregate
	 * 
	 * @param nameStart
	 *            Common beginning of query aggregate names - useful for removing multiple query aggregates that begin
	 *            with the same prefix (as in the case when query aggregates are created for success models)
	 * @throws SQLException
	 */
	public void dropQueryAggregates(String nameStart, boolean persist) throws SQLException
	{
		List<QueryAggregate> removeList = new ArrayList<QueryAggregate>();
		nameStart = Util.replaceWithPhysicalString(nameStart);
		for (QueryAggregate qa : queryAggregates)
		{
			if (qa.getName().startsWith(nameStart))
			{
				removeList.add(qa);
				qa.removeAggregate(persist);
			}
		}
		if (removeList.size() > 0)
		{
			for (QueryAggregate qa : removeList)
			{
				removeQueryAggregate(qa);
			}
		}
	}

	/**
	 * Find a given operation
	 * 
	 * @param name
	 * @return
	 */
	public Operation getOperation(String name) throws CloneNotSupportedException
	{
		if (operations == null)
			return null;
		for (Operation op : operations)
		{
			if (op.getName().equals(name))
				return ((Operation) op.clone());
		}
		return (null);
	}
	
	public Operation[] getOperations() {
		return operations;
	}

	/**
	 * find a variable in the repository
	 * 
	 * @param variableName
	 * @return variable
	 */
	public Variable findVariable(String variableName)
	{
		if (variableName == null)
		{
			return null;
		}
		// strip off V{...} if it exists
		if (variableName.startsWith("V{") && variableName.endsWith("}"))
		{
			variableName = variableName.substring(2, variableName.length() - 1);
		}
		Variable v = null;
		if ((variables != null) && (variables.length > 0))
		{
			for (int i = 0; i < variables.length; i++)
			{
				if (variableName.equals(variables[i].getName()))
				{
					v = variables[i];
					break;
				}
			}
		}
		return v;
	}

	public File getRepositoryRoot()
	{
		final String repositoryRootPath = serverParameters.getApplicationPath();
		final File repositoryRoot = new File(repositoryRootPath);
		return repositoryRoot;
	}

	public String getRepositoryRootPath()
	{
		final File repositoryRoot = getRepositoryRoot();
		String repositoryRootPath = "";
		try
		{
			repositoryRootPath = repositoryRoot.getCanonicalPath();
		} catch (IOException e)
		{
			logger.error("Could not get canonical path for file: " + repositoryRoot.getAbsolutePath());
		}
		return repositoryRootPath;
	}

	/**
	 * @return the ACL
	 */
	public Map<String, List<ACLItem>> getAccessControlLists()
	{
		return accessControlLists;
	}

	/**
	 * @return the variables
	 */
	public Variable[] getVariables()
	{
		return variables;
	}

	/**
	 * @return the virtualColumns
	 */
	public VirtualColumn[] getVirtualColumns()
	{
		return virtualColumns;
	}

	/**
	 * Find a warehouse source file by name
	 * 
	 * @param name
	 * @return
	 */
	public SourceFile findSourceFile(String name)
	{
		if (getSourceFiles() == null)
			return null;
		for (SourceFile sf : getSourceFiles())
		{
			if (sf.FileName.equalsIgnoreCase(name))
				return (sf);
		}
		return (null);
	}

	public Broadcast findBroadcast(String name)
	{
		if (broadcasts == null)
			return null;
		for (Broadcast bc : broadcasts)
		{
			if (bc.getName().equalsIgnoreCase(name))
				return bc;
		}
		return null;
	}

	public StagingFilter getStagingFilter(String name)
	{
		return stagingFilterMap.get(name);
	}

	public ScriptGroup getScriptGroup(String name)
	{
		return scriptGroupMap.get(name);
	}

	/**
	 * Find a generic column with a given name or return null
	 * 
	 * @param dimension
	 * @param name
	 * @return
	 */
	public GenericColumn findGenericColumn(String dimension, String name)
	{
		return (generics.get(dimension + "." + name));
	}

	/**
	 * Add a new generic column
	 * 
	 * @param gc
	 */
	public void addGenericColumn(GenericColumn gc)
	{
		generics.put(gc.Dimension + "." + gc.Name, gc);
	}

	/**
	 * Returns true if a given dimension column is used as a primary level key in any of the associated dimension levels
	 * in the hierarchy.
	 * 
	 * @param dc
	 * @return
	 */
	private boolean isPrimaryLevelKey(DimensionColumn dc)
	{
		String dimName = dc.DimensionTable.DimensionName;
		Hierarchy h = findHierarchy(dimName);
		if (h == null)
			return (false);
		Level l = h.findPrimaryLevelKey(dc.ColumnName);
		return (l != null);
	}

	public long getLastModified()
	{
		return lastModified;
	}

	public void setLastModified(long lastModified)
	{
		this.lastModified = lastModified;
	}
	
	public String getSpaceName()
	{
		String spaceName = null;
		
		// dervied from application name which is spacename/ownername
		if(applicationName != null)
		{
			int lastIndex = applicationName.lastIndexOf("/");
			spaceName = applicationName.substring(0, lastIndex);
		}
		return spaceName;
	}
	
	public String getApplicationName()
	{
		return applicationName;
	}

	public String getSourceCodeControlVersion()
	{
		return sourceCodeControlVersion;
	}

	public String getTargetDimension()
	{
		return targetDimension;
	}

	public String getOptimizationGoal()
	{
		return optimizationGoal;
	}

	public String getLastEdit()
	{
		return lastEdit;
	}

	public ReferencePopulation[] getReferencePopulations()
	{
		return referencePopulations;
	}

	public PerformanceModel[] getPerformanceModels()
	{
		return performanceModels;
	}

	public SuccessModel[] getSuccessModels()
	{
		return successModels;
	}

	public PatternSearchDefinition[] getSearchDefinitions()
	{
		return searchDefinitions;
	}

	public void setUsers(List<User> users)
	{
		this.users = users;
	}

	public List<User> getUsers()
	{
		return users;
	}

	public void setPossibleUnmappedMeasures(Set<String> possibleUnmappedMeasures)
	{
		this.possibleUnmappedMeasures = possibleUnmappedMeasures;
	}

	public Set<String> getPossibleUnmappedMeasures()
	{
		return possibleUnmappedMeasures;
	}

	public void setDependencies(HierarchyLevel[][] dependencies)
	{
		this.dependencies = dependencies;
	}

	public HierarchyLevel[][] getDependencies()
	{
		return dependencies;
	}

	public void setConnections(List<DatabaseConnection> connections)
	{
		this.connections = connections;
	}

	public List<DatabaseConnection> getConnections()
	{
		return connections;
	}

	public void setServerParameters(ServerParameters serverParameters)
	{
		this.serverParameters = serverParameters;
	}

	public ServerParameters getServerParameters()
	{
		return serverParameters;
	}

	public void setModelParameters(ModelParameters modelParameters)
	{
		this.modelParameters = modelParameters;
	}

	public ModelParameters getModelParameters()
	{
		return modelParameters;
	}

	public void setOptimizerParameters(OptimizerParameters optimizerParameters)
	{
		this.optimizerParameters = optimizerParameters;
	}

	public OptimizerParameters getOptimizerParameters()
	{
		return optimizerParameters;
	}

	public void setGroups(Group[] groups)
	{
		this.groups = groups;
	}

	public Group[] getGroups()
	{
		return groups;
	}

	public void setOutcomes(Outcome[] outcomes)
	{
		this.outcomes = outcomes;
	}

	public Outcome[] getOutcomes()
	{
		return outcomes;
	}

	public void setSubjectAreas(SubjectArea[] subjectAreas)
	{
		this.subjectAreas = subjectAreas;
	}

	private boolean processedSAJoins = false;
	
	
	public SubjectArea[] getSubjectAreas()
	{
		synchronized (this)
		{
			if (!processedSAJoins)
				processSubjectAreaJoins();
			return subjectAreas;
		}
	}

	public void setStagingTables(StagingTable[] stagingTables)
	{
		this.stagingTables = stagingTables;
	}

	public StagingTable[] getStagingTables()
	{
		return stagingTables;
	}

	private void setSourceFiles(SourceFile[] sourceFiles)
	{
		this.sourceFiles = sourceFiles;
	}

	public SourceFile[] getSourceFiles()
	{
		return sourceFiles;
	}

	public void setHierarchies(Hierarchy[] hierarchies)
	{
		this.hierarchies = hierarchies;
	}

	public Hierarchy[] getHierarchies()
	{
		return hierarchies;
	}

	public void setTimeDefinition(TimeDefinition timeDefinition)
	{
		this.timeDefinition = timeDefinition;
	}

	public TimeDefinition getTimeDefinition()
	{
		return timeDefinition;
	}

	public Level getDimensionTableLevel(DimensionTable dt)
	{
		if (dt.Level == null || dt.Level.isEmpty())
			return (null);
		Hierarchy h = findDimensionHierarchy(dt.DimensionName);
		if (h == null)
			return (null);
		return (h.findLevel(dt.Level));
	}

	public List<QuickDashboard> getQuickDashboards()
	{
		return quickDashboards;
	}

	public void setQuickDashboards(List<QuickDashboard> quickDashboards)
	{
		this.quickDashboards = quickDashboards;
	}

	/**
	 * @param name
	 * @return
	 */
	public StagingTable findStagingTable(String name)
	{
		for (StagingTable t : stagingTables)
		{
			if (t.Name.equals(name))
				return t;
		}
		return null;
	}
	
	public StagingTable findStagingTableForSourceFile(String sfname)
	{
		for (StagingTable t : stagingTables)
		{
			if (t.SourceFile.equals(sfname))
				return t;
		}
		return null;
	}

	public boolean isProxyDatabaseConnectionAvailable()
	{
		return this.serverParameters.getProxyDBConnectionString() != null;
	}

	public String getProxyDatabaseConnectString()
	{
		return this.serverParameters.getProxyDBConnectionString();
	}

	public String getProxyDatabaseUsername()
	{
		return this.serverParameters.getProxyDBUserName();
	}

	public String getProxyDatabasePassword()
	{
		return this.serverParameters.getProxyDBPassword();
	}

	public Integer getDefaultAdhocNumberWidth()
	{
		Resource res = getResource("DefaultAdhocNumberWidth");
		if (res != null)
		{
			try
			{
				return Integer.valueOf(res.getValue());
			} catch (Exception e)
			{
				;
			}
		}
		return null;
	}

	/**
	 * Returns true if the repository contains dynamic groups variable (Birst$Groups)
	 * 
	 * @return
	 */
	public boolean hasDynamicGroups()
	{
		boolean hasDg = false;
		Variable v = findVariable("Birst$Groups");
		if (v != null)
		{
			hasDg = true;
		}
		return hasDg;
	}

	/**
	 * @return the customGeoMaps
	 */
	public List<CustomGeoMap> getCustomGeoMaps() {
		return customGeoMaps;
	}

	/**
	 * @param customGeoMaps the customGeoMaps to set
	 */
	public void setCustomGeoMaps(List<CustomGeoMap> customGeoMaps) {
		this.customGeoMaps = customGeoMaps;
	}
	
	/*
	 * Returns the directory name of the ApplicationPath, for example 069a52f6-8750-41b6-9b99-5f759c6b8959 of 
	 * c:\SMI\Data\069a52f6-8750-41b6-9b99-5f759c6b8959
	 */
	public String getApplicationIdentifier(){
		File dir = new File(getServerParameters().getApplicationPath());
		return dir.getName();
	}

	public int getCurrentBuilderVersion()
	{
		return currentBuilderVersion;
	}
	
	public List<QueryFilter> getMdxSourceFilters()
	{
		return mdxSourceFilters;
	}

	public SavedExpressions getSavedExpressions() {
		File file = new File(getServerParameters().getApplicationPath());
		file = new File(file, SavedExpressions.fileName);
		if (savedExpressions == null || savedExpressions.isOlder(file))
			savedExpressions = new SavedExpressions(file);
		return savedExpressions;
	}
	
	public void clearSavedExpressions() {
		this.savedExpressions = null;
	}
	
	public String getCharacterEncodingForDefaultConnection()
	{
		DatabaseConnection dc = getDefaultConnection();
		if (dc == null)
			return null;
		return dc.getBulkLoadCharacterEncoding();
	}
	
	public void processImports()
	{
		if (Imports == null || Imports.size() == 0)
			return;
		PackageImport.processImports(this);
	}

	public void setVariables(Variable[] variables)
	{
		this.variables = variables;
	}

	private void addPackageSpaceInfo(PackageSpaceProperties spaceProperties) {
		if(packageSpacesPropertiesList == null)
		{
			packageSpacesPropertiesList = new ArrayList<PackageSpaceProperties>();
		}
		packageSpacesPropertiesList.add(spaceProperties);
	}
	
	public PackageSpaceProperties getPackageSpaceProps(String packageID, String spaceID)
	{	
		if(packageID != null && packageID.trim().length() > 0 
				&& spaceID != null && spaceID.trim().length() > 0 
				&& packageSpacesPropertiesList != null && packageSpacesPropertiesList.size() > 0)
		{
			for(PackageSpaceProperties packageSpaceProperties : packageSpacesPropertiesList)
			{
				if(spaceID.equalsIgnoreCase(packageSpaceProperties.getSpaceID()) 
						&& packageID.equalsIgnoreCase(packageSpaceProperties.getPackageID()))
				{
					return packageSpaceProperties;
				}
			}
		}
		return null;
	}
	
	public List<PackageSpaceProperties> getPackagesSpacePropertiesList()
	{
		return packageSpacesPropertiesList;
	}
	
	public PackageSpaceProperties getPackageSpaceProps(String packageID)
	{
		if(packageID != null && packageID.trim().length() > 0 
				&& packageSpacesPropertiesList != null && packageSpacesPropertiesList.size() > 0)
		{
			for(PackageSpaceProperties packageSpaceProperties : packageSpacesPropertiesList)
			{
				if(packageID.equalsIgnoreCase(packageSpaceProperties.getPackageID()))
				{
					return packageSpaceProperties;
				}
			}
		}
		return null;
	}

	public void setCollisionFlag(boolean flag) {
		this.collision = flag;
	}
	
	public boolean anyCollisions()
	{
		return collision;
	}
	
	public void setImportedPackageDefinitionList(List<PackageDefinition> packageDefinitionList)
	{
		this.importedPackageDefinitionList = packageDefinitionList;
	}
	
	public List<PackageDefinition> getImportedPackageDefinitionList()
	{
		return importedPackageDefinitionList;
	}
	
	public List<WarehousePartition> getWarehousePartitions()
	{
		return WarehousePartitions;
	}
	
	public SourceFileSplitter getSourceFileSplitter(String sourceFilePath)
	{
		if(sfs == null)
		{
			sfs = new SourceFileSplitter(this, sourceFilePath);
		}
		return sfs;
	}

	public String getDefaultConnectionName()
	{
		String defaultConnectionName = getDefaultConnection().Name;
		if(partitionConnection != null)
		{
			defaultConnectionName = Repository.DEFAULT_DB_CONNECTION;
		}
		return defaultConnectionName;
	}

	public boolean isPartitioned()
	{
		return IsPartitioned;
	}
	
	public static boolean containsVariable(String joinCondition)
	{
		boolean containsVariable = false;
		if (joinCondition != null)
		{
			int variableStartIndex = joinCondition.indexOf("V{");
			if (variableStartIndex >= 0)
			{
				int variableEndIndex = joinCondition.indexOf("}", variableStartIndex);
				if (variableEndIndex > variableStartIndex)
				{
					containsVariable = true;
				}
			}
		}
		return containsVariable;
	}

	public boolean isAllowExternalIntegrations()
	{
		return AllowExternalIntegrations;
	}

	public int getExternalIntegrationTimeout()
	{
		return ExternalIntegrationTimeout;
	}

	public boolean isLogCacheKey()
	{
		return logCacheKey;
	}
	
	public int getCurrentReferenceID()
	{
		return this.CurrentReferenceID;
	}
	
	public boolean getGenerateSubectArea()
	{
		return this.GenerateSubjectArea;
	}
	
	public boolean isDynamic()
	{
		return this.isDynamic;
	}
	
	public int getQueryLanguageVersion()
	{
		return this.queryLanguageVersion;
	}
	
	public int getMaxPhysicalTableNameLength()
	{
		return this.MaxPhysicalTableNameLength;
	}
	
	public int getMaxPhysicalColumnNameLength()
	{
		return this.MaxPhysicalColumnNameLength;
	}
	
	public int getNumPerformancePeers()
	{
		return this.NumPerformancePeers;
	}
	
	public int getNumSuccessPatternPeers()
	{
		return this.NumSuccessPatternPeers;
	}
	
	public QueryFilter[] getFilters()
	{
		return this.filters;
	}
	
	public Broadcast[] getBroadcasts()
	{
		return this.broadcasts;
	}
	
	public LoadGroup[] getLoadGroups()
	{
		return this.loadGroups;
	}
	
	public static Element getStringArrayElement(String name,String[] values,Namespace ns)
	{
		Element child = new Element(name,ns);
		
		if (values!=null && values.length>0)
		{					
			for (String strValue : values)
			{
				Element stringElement = new Element("string",ns);
				stringElement.setText(strValue);
				child.addContent(stringElement);
			}			
		}	
		
		return child;
	}
	
	public static Element getStringArrayElement(String name,List<String> values,Namespace ns)
	{
		Element child = new Element(name,ns);
		
		if (values!=null && !values.isEmpty())
		{					
			for (String strValue : values)
			{
				Element stringElement = new Element("string",ns);
				stringElement.setText(strValue);
				child.addContent(stringElement);
			}			
		}	
		
		return child;
	}
	
	public ScriptGroup[] getScriptGroups()
	{
		return this.ScriptGroups;
	}

	public boolean isRequiresPublish() {
		return RequiresPublish;
	}

	public boolean isRequiresRebuild() {
		return RequiresRebuild;
	}

	public boolean isHybrid() {
		return Hybrid;
	}

	public boolean isDisablePublishLock() {
		return DisablePublishLock;
	}
	
	public boolean isNoBirstApplicationRebuild() {
		return NoBirstApplicationRebuild;
	}
	
	public boolean isEnableLiveAccessToDefaultConnection() {
		return EnableLiveAccessToDefaultConnection;
	}
	
	public boolean isDisableImpliedGrainsProcessing()
	{
		return DisableImpliedGrainsProcessing;
	}		
	
	public static void addBaseAuditObjectAttributes(Element e,String Guid,String CreatedDate,String LastModifiedDate,String CreatedUsername,String LastModifiedUsername,Namespace ns)
	{
		Element child = new Element("Guid",ns);
		child.setText(Guid);
		e.addContent(child);
		
		child = new Element("CreatedDate",ns);
		if (CreatedDate!=null && !CreatedDate.isEmpty())
		{
		child.setText(CreatedDate);
		}
		e.addContent(child);
		if (CreatedDate==null || CreatedDate.isEmpty())
		{
			makeNilAttribute(child, ns);
		}
		
		child = new Element("LastModifiedDate",ns);
		if (LastModifiedDate!=null && !LastModifiedDate.isEmpty())
		{
		child.setText(LastModifiedDate);
		}
		e.addContent(child);
		if (LastModifiedDate==null || LastModifiedDate.isEmpty())
		{
			makeNilAttribute(child, ns);
		}
		
		if (CreatedUsername!=null)
		{
		  XmlUtils.addContent(e, "CreatedUsername", CreatedUsername,ns);
		}
		
		if (LastModifiedUsername!=null)
		{
		  XmlUtils.addContent(e, "LastModifiedUsername", LastModifiedUsername,ns);
		}
	}
	
	public static void makeNilAttribute(Element e, Namespace ns)
	{
		Attribute attr = new Attribute("nil", "true");
		attr.setNamespace(Namespace.getNamespace("xsi","http://www.w3.org/2001/XMLSchema-instance"));
		e.setAttribute(attr);
	}
	
	public BuildProperties getBuildProperties()
	{
		return buildProperties;
	}
	
	public SubjectArea[] getAllSubjectAreas()
	{
		return this.subjectAreas;
	}
	
	public Object findTableByPhysicalName(String physicalTableName,DatabaseConnection dconn)
	{
		Object tableObject = null;
		if (aggregates!=null && !physicalTableName.toLowerCase().startsWith("dw_"))
		{
			for (Aggregate aa : aggregates)
			{
				String name = aa.getName();
				name =  getCurrentBuilderVersion()>=26 ? Util.replaceWithPhysicalString(dconn,name,this) :  Util.replaceWithPhysicalString(name);
				if(getCurrentBuilderVersion()>=26)
				{
					if(name!=null && name.length() >= DatabaseConnection.getIdentifierLength(dconn.DBType)-4){
						name = dconn.getHashedIdentifier(name);
					}
				}
				if (name!=null && name.equalsIgnoreCase(physicalTableName))
				{
					return null;
				}
			}
		}
		
		for (DimensionTable dt : DimensionTables)
		{
			if (dt.PhysicalName!=null && dt.PhysicalName.equalsIgnoreCase(physicalTableName))
			{
				return dt;
			}
		}
		for (MeasureTable mt : MeasureTables)
		{
			if (mt.PhysicalName!=null && mt.PhysicalName.equalsIgnoreCase(physicalTableName))
			{
				return mt;
			}
		}
		for (StagingTable st : stagingTables)
		{
			if (st.Name!=null && st.Name.equalsIgnoreCase(physicalTableName))
			{
				return st;
			}
		}
		return tableObject;
	}
	
	public boolean isLoadWarehouseExecuted() 
	{
		return isLoadWarehouseExecuted;
	}

	public void setLoadWarehouseExecuted(boolean isLoadWarehouseExecuted) 
	{
		this.isLoadWarehouseExecuted = isLoadWarehouseExecuted;
	}
	
	/*
	 * Load variables exist in the repository when the repository contains subgroups. Since the load variables are initialized
	 * along with the repository initialization and then the load number increases during load warehouse, the load number is 
	 * not correct at the time of persisting of aggregates. This method will loop through all the repository variables, figure out
	 * if it is a load variable and set the EvaluateOnDemand flag to true so it is re-evaluated during its subsitution
	 */
	public void reInitLoadVariables()
	{
		try
		{
			if (variables!=null)
			{
				for (Variable var : variables)
				{
					if (var.getType().equals(VariableType.Repository))
					{
						if (var.getName().startsWith("Load:"))
						{
							String factName = var.getName().substring(5);
							if (factName.endsWith("Fact Fact"))
							{
								factName = factName.substring(0, factName.lastIndexOf(" Fact"));										
							}
							MeasureTable mt = findMeasureTableName(factName);
							if (mt != null)
							{								
								var.setEvaluateOnDemand(true);
							}
						}
					}
				}
			}
		}
		catch (Exception ex)
		{
			logger.error("Exception in reinit load variables ",ex);
		}
	}
	
	public PartitionConnectionLoadInfo getPartitionConnectionLoadInfo(String connName)
	{
		PartitionConnectionLoadInfo pcli = null;
		if(dcToPartitionConnectionLoadInfoMap != null)
		{
			pcli = dcToPartitionConnectionLoadInfoMap.get(connName);
		}
		
		return pcli;
	}
}
