/**
 * $Id: DrillPath.java,v 1.7 2008-03-27 19:23:29 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc;

import java.io.UnsupportedEncodingException;
import com.successmetricsinc.query.DimensionColumn;
import com.successmetricsinc.query.Level;

/**
 * Structure containing a drill path for a given dimension column
 * 
 * @author bpeters
 * 
 */
public class DrillPath
{
    Repository r;
    DimensionColumn dc;
    Level[] levels;

    private void initialize()
    {
        Level l = null;
        if (dc==null)
            return;
        for (int i = 0; i<r.getHierarchies().length; i++)
        {
            if (r.getHierarchies()[i].DimensionName.equals(dc.DimensionTable.DimensionName))
            {
                l = findLevel(r.getHierarchies()[i].Children, dc.ColumnName);
                break;
            }
        }
        if (l==null)
            levels = null;
        else
            levels = l.Children;
    }

    /**
     * Instantiate a drill path
     * 
     * @param dc
     *            Dimension column to instantiate drill for
     * @return
     */
    public DrillPath(Repository r, DimensionColumn dc)
    {
        this.r = r;
        this.dc = dc;
        initialize();
    }

    /**
     * Instantiate a drill path
     * 
     * @param dimension
     * @param column
     */
    public DrillPath(Repository r, String dimension, String column)
    {
        this.r = r;
        dc = r.findDimensionColumn(dimension, column);
        initialize();
    }

    /**
     * Return whether this is a valid drill path or not
     * 
     * @return
     */
    public boolean isValid()
    {
        return (dc!=null);
    }

    /**
     * Return the drill path string that contains a filter on the drilled dimension column
     * 
     * @param drillURL
     *            URL target (a null value indicates not to specify a URL target)
     * @param filterValue
     *            Filter value
     * @return
     * @throws UnsupportedEncodingException
     */
    public String getDrillPathString(String drillURL, String filterValue) throws UnsupportedEncodingException
    {
        if (dc==null)
            return (null);
        if (levels==null)
            return "";
        StringBuilder ref = new StringBuilder();
        ref.append("cr=");
        ref.append(dc.ColumnName);
        ref.append('&');

        for (int j = 0; j<levels.length; j++)
        {
            boolean first = true;
            for (int k = 0; k<levels[j].ColumnNames.length; k++)
            {
                if (!levels[j].HiddenColumns[k])
                {
                    if (first)
                        first = false;
                    else
                        ref.append('&');
                    String colname = java.net.URLEncoder.encode(levels[j].ColumnNames[k], "UTF-8");
                    ref.append("dn");
                    ref.append(j*levels.length+k);
                    ref.append('=');
                    ref.append(java.net.URLEncoder.encode(dc.DimensionTable.DimensionName, "UTF-8"));
                    ref.append("&dc");
                    ref.append(j*levels.length+k);
                    ref.append('=');
                    ref.append(colname);
                    ref.append("&cac=");
                    ref.append(colname);
                }
            }
        }
        ref.append("&dft=");
        ref.append(dc.DimensionTable.DimensionName);
        ref.append("&dfc=");
        ref.append(dc.ColumnName);
        ref.append("&df=");
        ref.append(filterValue);

        if (drillURL != null)
        {
        	StringBuilder sb = new StringBuilder(drillURL);
        	if (drillURL.indexOf('?') < 0)
        		sb.append('?');
        	else if (drillURL.length() > 0)
        		sb.append('&');
        	sb.append(ref);
        	return sb.toString();
        }
        else
        	return ref.toString();
    }

    private static Level findLevel(Level[] list, String s)
    {
        for (int i = 0; i<list.length; i++)
        {
            for (int k = 0; k<list[i].ColumnNames.length; k++)
            {
                if (list[i].ColumnNames[k].equals(s))
                    return (list[i]);
            }
            Level l = findLevel(list[i].Children, s);
            if (l!=null)
                return (l);
        }
        return (null);
    }

	public DimensionColumn getDc() {
		return dc;
	}

	public Level[] getLevels() {
		return levels;
	}
}
