package com.successmetricsinc.util;

import org.junit.Test;

import junit.framework.Assert;

public class FileUtilsTest {
	
	private static final String PATH_1 = "/private/user@domain/Daily Reports/Sales Projects for 2009-07-05.AdhocReport";
	private static final String PATH_1_BASENAME = "Sales Projects for 2009-07-05.AdhocReport";
	private static final String PATH_1_BASENAME_NO_EXT = "Sales Projects for 2009-07-05";
	private static final String PATH_1_EXT = "AdhocReport";

	@Test
	public void testGetFileBasename() throws Exception {
		// Run test
		final String result = FileUtils.getFileBasename(PATH_1);
		
		// Check results
		Assert.assertEquals(PATH_1_BASENAME, result);
	}

	@Test
	public void testGetFileBasenameWithoutExtension() throws Exception {
		// Run test
		final String result = FileUtils.getFileBasenameWithoutExtension(PATH_1);
		
		// Check results
		Assert.assertEquals(PATH_1_BASENAME_NO_EXT, result);
	}

	@Test
	public void testGetFileExtension() throws Exception {
		// Run test
		final String result = FileUtils.getFileExtension(PATH_1);
		
		// Check results
		Assert.assertEquals(PATH_1_EXT, result);
	}

	@Test
	public void testGetFileNameNoStripDefault() throws Exception {
		// Run test
		final String result = FileUtils.getFileName(PATH_1);
		
		// Check results
		Assert.assertEquals(PATH_1_BASENAME, result);
	}

	@Test
	public void testGetFileNameNoStripOverride() throws Exception {
		// Run test
		final String result = FileUtils.getFileName(PATH_1, false);
		
		// Check results
		Assert.assertEquals(PATH_1_BASENAME, result);
	}

	@Test
	public void testGetFileStripOverride() throws Exception {
		// Run test
		final String result = FileUtils.getFileName(PATH_1, true);
		
		// Check results
		Assert.assertEquals(PATH_1_BASENAME_NO_EXT, result);
	}
}
