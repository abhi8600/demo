package com.successmetricsinc.query;

import java.sql.Statement;

public class WrapperDatabaseStatementTestSubClass extends
		WrapperDatabaseStatement {

	private int testRetries = 0;
	private int testRetryCount; 
	private String testString;
	public WrapperDatabaseStatementTestSubClass(DatabaseConnection _dc, Statement _stmt) {		
		super(_dc, _stmt);		
		testRetryCount = 3;
	}
	
	public boolean getIsTesting(){
		
		if(testString !=null && testRetries < testRetryCount + 1){
			testRetries++;
			return true;
		}
		return false;
	}
   
	public void setTestRetryCount(int _testRetryCount){
		testRetryCount = _testRetryCount;
	}
	
	public void resetTestRetries(){
		testRetries = 0;
	}
	
	public void setTestString(String _testString){
		this.testString = _testString;
	}
	
	public String getTestString(){
		return this.testString;
	}
}
