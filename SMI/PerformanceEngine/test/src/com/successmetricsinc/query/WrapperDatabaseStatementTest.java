package com.successmetricsinc.query;

import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.apache.log4j.BasicConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.successmetricsinc.CommandSession;
import com.successmetricsinc.command.CommandStepAction;
import com.successmetricsinc.command.CommandStepActionFactory;


public class WrapperDatabaseStatementTest {

	private static CommandSession cmdSession = null;
	private static String TABLE_NAME = "myTestTable";
    private static String CREATE_QUERY = "CREATE TABLE tableName" +
    		" ( USERNAME VARCHAR(200)," +
    		"  COL2 INT not null," +
    		"  COL3 INT not null," +
    		"  PRIMARY KEY(USERNAME)" +
    		")";
    private static String INSERT_QUERY = "INSERT INTO tableName values('test1',1,2);";
    private static String SELECT_QUERY = "SELECT count(*) FROM tableName;";
    private static String DELETE_QUERY = "DELETE FROM tableName";    
    private static String UPDATE_QUERY_1 = "UPDATE tableName set COL2=20 where COL2=1";
    private static String UPDATE_QUERY_2 = "UPDATE tableName set COL2=40 where COL2=20";
    private String CONNECTION_RESET_ERROR = "Connection reset";
    private String CONNECTION_OTHER_ERROR = "Connection other";
    
    private static String qualifiedName;
    
    
	@BeforeClass
	public static void setUpCommandSession() throws Exception {
		System.setProperty("smi.sc.home","c:\\Workspace\\SMI\\");
		cmdSession = new CommandSession(true);
		CommandStepAction repoAction = CommandStepActionFactory.createCommandStepAction(cmdSession, "repository "+ getTestRoot().getAbsolutePath()+"/repository/testRepository.xml", -1);
		repoAction.execute();
		BasicConfigurator.configure();		
	}
	
	@Before
	public void setUpTestTables() throws SQLException{
		DatabaseConnection dc = cmdSession.getRepository().getDefaultConnection();
		// Native Sql Statement
	    Database.dropTable(dc, TABLE_NAME);
	    qualifiedName = Database.getQualifiedTableName(dc, TABLE_NAME);	 
	    SELECT_QUERY = SELECT_QUERY.replace("tableName", qualifiedName);
	    CREATE_QUERY = CREATE_QUERY.replace("tableName", qualifiedName);
	    INSERT_QUERY = INSERT_QUERY.replace("tableName", qualifiedName);
	    DELETE_QUERY = DELETE_QUERY.replace("tableName", qualifiedName);
	    UPDATE_QUERY_1 = UPDATE_QUERY_1.replace("tableName", qualifiedName);
	    UPDATE_QUERY_2 = UPDATE_QUERY_2.replace("tableName", qualifiedName);
		Statement stmt = dc.ConnectionPool.getConnection().createStatement();		
		stmt.executeUpdate(CREATE_QUERY);	
		stmt.executeUpdate(INSERT_QUERY);
		stmt.close();		
		
	}
	
	//@After 
	public void deleteTestTable() throws SQLException{
		DatabaseConnection dc = cmdSession.getRepository().getDefaultConnection();
		// Native Sql Statement
	    Database.dropTable(dc, TABLE_NAME);	      
	}
	
	@Test
	public void testWrapperStatementExecuteQuery() throws SQLException {
		
		DatabaseConnection dc = cmdSession.getRepository().getDefaultConnection();
		Connection conn = dc.ConnectionPool.getConnection();
		Statement stmt = conn.createStatement();
		
		WrapperDatabaseStatement wr = new WrapperDatabaseStatement(dc, stmt);
		long nativeRowCount = 0;
		long wrapperRowCount = 1;
		
		ResultSet rs1 = stmt.executeQuery(SELECT_QUERY);		
		
		while (rs1.next())
		{
			nativeRowCount = rs1.getLong(1);
			Assert.assertTrue("Unexpected result set", nativeRowCount > 0);
			System.out.println("result1="+nativeRowCount);			
		}
		
		ResultSet rs2 = wr.executeQuery(SELECT_QUERY);
		while (rs2.next())
		{
			wrapperRowCount = rs2.getLong(1);
			Assert.assertTrue("Unexpected result set", wrapperRowCount > 0);
			System.out.println("result2="+ wrapperRowCount);
		}
		Assert.assertEquals("Native and Wrapper Statements do not return the same results", nativeRowCount, wrapperRowCount);	
		wr.close();
		
	}
	
	@Test
	public void testWrapperStatementExecuteQueryException(){
		
		DatabaseConnection dc = cmdSession.getRepository().getDefaultConnection();
		// Native Sql Statement
		Statement stmt = null;		
		WrapperDatabaseStatementTestSubClass wr = null;
		
		long nativeRowCount = 0;
		try {
			stmt = dc.ConnectionPool.getConnection().createStatement();			
			wr = new WrapperDatabaseStatementTestSubClass(dc, stmt);			
			ResultSet rs1 = stmt.executeQuery(SELECT_QUERY);
			while (rs1.next()) {
				nativeRowCount = rs1.getLong(1);
				Assert.assertTrue("Unexpected result set", nativeRowCount > 0);
				System.out.println("result1=" + nativeRowCount);
			}			
			
			// setTesting flag to thrown SQL Exception
			wr.setTestString(CONNECTION_RESET_ERROR); // message of SQL Exception
			wr.resetTestRetries();

			ResultSet rs2 = wr.executeQuery(SELECT_QUERY);
			Assert.fail("WrapperStatement should have thrown exception on max retries");
		} catch (SQLException e) {
			Assert.assertNotNull(e.getMessage());			
		}
		
		// Setting exception message other than connection reset
		wr.setTestString(CONNECTION_OTHER_ERROR);
		wr.resetTestRetries();
		try {
			wr.executeQuery(SELECT_QUERY);
			Assert.fail("Exception should have been thrown by now");
		} catch (SQLException e) {
			Assert.assertNotNull(e.getMessage());
		}
		
		// Setting exception message - simulating connection working after 2 retries
		wr.setTestString(CONNECTION_RESET_ERROR);
		wr.setTestRetryCount(2);
		wr.resetTestRetries();
		
		ResultSet rs3 = null;
		long wrapperRowCount = 0;
		try {
			rs3 = wr.executeQuery(SELECT_QUERY);
			while (rs3.next())
			{
				wrapperRowCount = rs3.getLong(1);
				Assert.assertTrue("Unexpected result set", wrapperRowCount > 0);
				System.out.println("result2="+ wrapperRowCount);
			}
			Assert.assertEquals("Native and Wrapper Statements do not return the same results", nativeRowCount, wrapperRowCount);
		} catch (SQLException e) {
			Assert.fail("Unwanted Exception "+ e);
		}	
		
		wr.close();
	}
	

	@Test
	public void testWrapperStatementExecuteUpdate(){
		
		DatabaseConnection dc = cmdSession.getRepository().getDefaultConnection();
		
		Statement stmt = null;
		WrapperDatabaseStatement wr = null;
		try {
			stmt = dc.ConnectionPool.getConnection().createStatement();			
			wr = new WrapperDatabaseStatement(dc, stmt);
			int nativeRowCount = stmt.executeUpdate(UPDATE_QUERY_1);			
			System.out.println("update count="+nativeRowCount);
			Assert.assertTrue("Native statement not working", nativeRowCount==1);			
			
			
			int wrapperRowCount = wr.executeUpdate(UPDATE_QUERY_2);			
			Assert.assertTrue("WrapperStatement does not handle executeUpdate exception as expected",wrapperRowCount==1);
		} catch (SQLException e) {
			Assert.fail("Unwanted Exception");
		}
		
		wr.close();		
	}
	
		
	@Test
	public void testWrapperStatementExecuteUpdateException(){
		DatabaseConnection dc = cmdSession.getRepository().getDefaultConnection();
		// Native Sql Statement
		Statement stmt = null;		
		WrapperDatabaseStatementTestSubClass wr = null;
		try {
			stmt = dc.ConnectionPool.getConnection().createStatement();			
			wr = new WrapperDatabaseStatementTestSubClass(dc, stmt);
			int nativeRowCount = stmt.executeUpdate(UPDATE_QUERY_1);
			System.out.println("update count="+nativeRowCount);
			Assert.assertTrue("Native statement not working", nativeRowCount==1);						
			
			// setTesting flag to thrown SQL Exception
			wr.setTestString(CONNECTION_RESET_ERROR); // message of SQL Exception
			wr.resetTestRetries();
			int wrapperRowCount = wr.executeUpdate(UPDATE_QUERY_2);		
			// update should not happen and instead should return 0.
			Assert.fail("WrapperStatement should have thrown exception by now");
		} catch (SQLException e) {
			Assert.assertNotNull(e.getMessage());
		}
		// Setting exception message other than connection reset
		wr.setTestString(CONNECTION_OTHER_ERROR);
		wr.resetTestRetries();
		
		try {
			wr.executeUpdate(UPDATE_QUERY_2);
			Assert.fail("Exception should have been thrown by now");
		} catch (SQLException e) {
			Assert.assertNotNull(e.getMessage());
		}
		
		// Setting exception message - simulating connection working after 2 retries
		wr.setTestString(CONNECTION_RESET_ERROR);
		wr.setTestRetryCount(2);
		wr.resetTestRetries();
		int wrapperRowCount=0;
		try {
			wrapperRowCount = wr.executeUpdate(UPDATE_QUERY_2);
		} catch (SQLException e) {
			Assert.fail("Exception thrown = " + e);
		}			
		Assert.assertTrue("WrapperStatement does not handle executeUpdate exception as expected",wrapperRowCount==1);		
		
		wr.close();
	}
	
	
	public static File getTestRoot() {
		String sourceControlHome = System.getenv("SMI_SC_HOME");		
		sourceControlHome = System.getProperty("smi.sc.home", sourceControlHome);
		if (sourceControlHome == null) {
			// assume current directory as the root.
			return new File("PerformanceEngine/test");
		}
		return new File(sourceControlHome+"/PerformanceEngine/test");
	}
}
