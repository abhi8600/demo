package com.successmetricsinc;


import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.junit.*;

public class RepUsersAndGroupsTest {

	Logger logger = Logger.getLogger(RepUsersAndGroupsTest.class);
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {		
	}
	
	@Test
	public void parseUserGroupFileTest() throws Exception
	{		
		String fileName = "C:\\Guri\\Test\\testFiles\\userAndGroup.xml";
		File file = new File(fileName);
		SAXBuilder builder = new SAXBuilder();
		Document d = builder.build(file);
		
		Repository r = new Repository();
		r.parseUsersAndGroups(d);
		
		for(Group group : r.getGroups())
		{	
			Assert.assertNotNull(group);
			Assert.assertNotNull(group.getName());
			Assert.assertNotNull(r.getUsers());
			logger.info("Group Name " + group.getName() + " type internalGroup = " + group.isInternalGroup() +  " with users " + r.getUsers());			
		}
	}
		

}
