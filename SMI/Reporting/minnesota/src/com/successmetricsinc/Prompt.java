/**
 * $Id: Prompt.java,v 1.205 2012-12-07 23:33:23 BIRST\sfoo Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.successmetricsinc;

import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.Map.Entry;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;

import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.WebServices.IWebServiceResult;
import com.successmetricsinc.adhoc.AdhocColumn;
import com.successmetricsinc.adhoc.AdhocColumn.OlapColumn;
import com.successmetricsinc.query.DisplayFilter;
import com.successmetricsinc.query.Filter;
import com.successmetricsinc.query.JasperDataSourceProvider;
import com.successmetricsinc.query.NavigationException;
import com.successmetricsinc.query.Query;
import com.successmetricsinc.query.QueryFilter;
import com.successmetricsinc.query.QueryResultSet;
import com.successmetricsinc.query.QueryString;
import com.successmetricsinc.query.TopFilter;
import com.successmetricsinc.query.Filter.FilterType;
import com.successmetricsinc.query.olap.BirstOlapField;
import com.successmetricsinc.query.olap.DimensionMemberSetNav;
import com.successmetricsinc.query.olap.MemberSelectionExpression;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.util.DateUtil;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XmlUtils;
import com.successmetricsinc.util.QuickDashboard.QuickPrompt;

/**
 * @author Brad Peters
 */
public class Prompt implements Cloneable, IWebServiceResult, Comparable<Object>
{
	private static final String ENABLED_COL_NAME = "EnabledColName";
	private static final String MULTILINE = "Multiline";
	private static final String USE_DRILL_PATH = "UseDrillPath";
	private static final String TREE_CONTROL = "TreeControl";
	private static final String EXPAND_INTO_BOXES = "ExpandIntoButtons";
	public static final String DATE_TIME = "DateTime";
	public static final String DATE = "Date";
	public static final String BOOLEAN = "Boolean";
	public static final String VARCHAR = "Varchar";
	public static final String FLOAT = "Float";
	public static final String NUMBER = "Number";
	public static final String INTEGER = "Integer";
	private static final String IS_EXPRESSION = "IsExpression";
	private static final String COLUMN_NAME = "ColumnName";
	private static final String VERSION = "Version";
	private static final String FILTER_TYPE = "FilterType";
	private static final String CLAZZ = "Class";
	private static final String SELECTED_VALUES = "selectedValues";
	private static final String LIST_VALUE = "ListValue";
	private static final String LABEL = "Label";
	private static final String VALUE = "Value";
	private static final String LIST_VALUES = "ListValues";
	private static final String SELECTED_VALUE = "selectedValue";
	private static final String SELECTED_DATE = "selectedDate";
	private static final String HIDE_LABEL = "hideLabel";
	private static final String IMMEDIATE_PARENT_FILTER_PARAM = "immediateParentFilterParam";
	private static final String PARENTS = "Parents";
	private static final String PARENT = "Parent";
	private static final String DO_NOT_FILL_IF_PARENT_NO_SELECTION = "doNotFillIfParentNoSelection";
	private static final String VARIABLE_TO_UPDATE = "variableToUpdate";
	private static final String VALIDATION_MESSAGE = "validationMessage";
	private static final String IMAGE_PATH = "imagePath";
	private static final String OPERATION_NAME = "operationName";
	private static final String HIDE_IF_ONLY_ONE_ROW = "hideIfOnlyOneRow";
	private static final String MULTI_SELECT_TYPE = "multiSelectType";
	private static final String FILTER = "filter";
	private static final String PARENT_FILTER_PARAMS = "parentFilterParams";
	private static final String TARGET_DASHBOARD_NAME = "targetDashboardName";
	private static final String TEXT = "text";
	private static final String INCLUDED = "Included";
	private static final String INCLUDED_PROMPT_INDICES = "IncludedPromptIndices";
	private static final String ADD_NO_SELECTION_ENTRY = "addNoSelectionEntry";
	private static final String ROW = "row";
	private static final String QUERY = "Query";
	private static final String DEFAULT_OPTION = "DefaultOption";
	private static final String OPERATOR = "Operator";
	private static final String OPERATORS = "Operators";
	private static final String OPTION_VALUE = "OptionValue";
	private static final String OPTION_VALUES = "OptionValues";
	private static final String OPTION = "Option";
	private static final String OPTIONS = "Options";
	private static final String VISIBLE_NAME = "VisibleName";
	private static final String OPERATOR_PARAMETER_NAME = "OperatorParameterName";
	private static final String PARAMETER_NAME = "ParameterName";
	private static final String PROMPT_TYPE = "PromptType";
	private static final String PROMPT = "Prompt";
	//ReportPath is used by the Dashboard UI during edits
	private static final String REPORT_PATH = "ReportPath"; 
	//Tooltip is used by the Dashboard UI for tooltip display
	private static final String TOOLTIP = "ToolTip";
	private static final String QUERY_COL_NAME = "QueryColName";
	private static final String COLUMN_FORMAT = "ColumnFormat";
	private static final String COLLECTION = "Collection";
	private static final String READ_ONLY = "ReadOnly";
	
	//Sliders
	private static final String SLIDER_TYPE = "SliderType";
	private static final String SLIDER_WIDTH = "SliderWidth";
	private static final String SLIDER_ENDPOINTS = "SliderEndPts";
	private static final String SLIDER_DISPLAY_LABELS = "SliderDispLabels";
	private static final String SLIDER_START_PT = "SliderStartPt";
	private static final String SLIDER_END_PT = "SliderEndPt";
	private static final String SLIDER_INCREMENT = "SliderIncrement";
	private static final String SLIDER_DEFAULT_START = "SliderDefaultStart";
	private static final String SLIDER_DEFAULT_END = "SliderDefaultEnd";
	
	//initial implementation
	private static final int UNKNOWN_VERSION = 0;
	
	/*
	 * version - changes
	 * 1 - set filter type to data instead of display,  filterType = FilterType.DATA;
	 * 2 - detect parameter name != column name, set it as an expression prompt	(isExpression = true)		
	 */
	private static final int CURRENT_VERSION = 2;
	
	private static Logger logger = Logger.getLogger(Prompt.class);
	
	private static Map<PromptType, String> promptTypes;
	private static Map<MultipleSelectionType, String> multipleSelectionTypes;
	
	
	public static final String DEFAULT_DISPLAY_DATE_FORMAT = "MM/dd/yy";
	public static final String DEFAULT_DISPLAY_DATETIME_FORMAT = "MM/dd/yyyy HH:mm:ss";
	
	public enum PromptType
	{
		Value, List, Query, OperatorValue, TextOnly, PromptGroup, CheckBox, Date, Slider, OlapHierarchy
	};

	public enum MultipleSelectionType
	{
		NONE, AND, OR
	};
	
	private PromptType Type;
	private String ParameterName;
	private String OperatorParameterName;
	private String VisibleName;
	private List<String> Options;
	private List<String> OptionValues;
	private transient List<String> FilledOptions;
	private transient List<String> FilledOptionValues;
	private List<Boolean> FilledEnabledValues;
	private List<String> Operators;
	private String DefaultOption;
	private String Query, QueryWithHierarchicalFilters;
	private int row;
	private boolean addNoSelectionEntry;
	private List<Integer> includedPromptIndices;
	private String text;
	private String targetDashboardName;
	private List<String> parentFilterParams;
	private MultipleSelectionType multiSelectType;
	private boolean hideIfOnlyOneRow;
	private String operationName;
	private String imagePath;
	private String validationMessage;
	private String variableToUpdate;
	private boolean doNotFillIfParentNoSelection;
	private String immediateParentFilterParam;
	private ArrayList<String> parentFilterPrompts;
	private boolean hideLabel;
	private String selectedDate;
	private List<String> selectedValues;
	private List<ListValue> listValues;
	private String Operator;
	private String reportPath;
	private String toolTip;
	// not really transient, but need to fool XStream for dashboard migration
	private transient String clazz;
	private static boolean SORT_PROMPTS = false;
	private String queryColName;
	private String enabledColName;
	private FilterType filterType = FilterType.DATA; 
	private int version = CURRENT_VERSION;
	private String columnFormat;
	private String columnName;
	private boolean isExpression = false;
	private String collection;
	private boolean readOnly;

	//sliders
	private String sliderType;
	private int sliderWidth;
	private String sliderEndPtsType;
	private String sliderDisplayLabels;
	private String sliderStartPt;
	private String sliderEndPt;
	private String sliderIncrement;
	private String sliderDefaultStart;
	private String sliderDefaultEnd;
	
	private boolean expandIntoBoxes = false;
	private boolean expandIntoTree = false;
	private boolean useDrillPath = false;
	private boolean mutliline = false;
	private String dimensionName;
	private List<OlapColumn> additionalOlapColumns;
	
	//logging for 4.3 parsing null column name
	private String dashboardPage;
	
	public void setDashboardPage(String dbPage){
		dashboardPage = dbPage;
	}
	
	public List<ListValue> getListValues() {
		return listValues;
	}
	
	public String getColumnFormat() {
		return columnFormat;
	}

	public void setColumnFormat(String columnFormat) {
		this.columnFormat = columnFormat;
	}

	static {
		promptTypes = new HashMap<PromptType, String>();
		promptTypes.put(PromptType.CheckBox, "CheckBox");
		promptTypes.put(PromptType.Date, DATE);
		promptTypes.put(PromptType.List, "List");
		promptTypes.put(PromptType.OperatorValue, "OperatorValue");
		promptTypes.put(PromptType.PromptGroup, "PromptGroup");
		promptTypes.put(PromptType.Query, QUERY);
		promptTypes.put(PromptType.TextOnly, "TextOnly");
		promptTypes.put(PromptType.Value, "Value");
		promptTypes.put(PromptType.Slider, "Slider");
		promptTypes.put(PromptType.OlapHierarchy, "OlapHierarchy");
		
		multipleSelectionTypes = new HashMap<MultipleSelectionType, String>();
		multipleSelectionTypes.put(MultipleSelectionType.AND, "AND");
		multipleSelectionTypes.put(MultipleSelectionType.OR, "OR");
		multipleSelectionTypes.put(MultipleSelectionType.NONE, "None");
		
	}
	
	public Prompt() 
	{
		Options = new ArrayList<String>();
		row = 0;
		addNoSelectionEntry = false;
		hideIfOnlyOneRow = false;
		includedPromptIndices = new ArrayList<Integer>();
		parentFilterParams = new ArrayList<String>();
		text = "";
		operationName = "";
		imagePath = "";
		validationMessage = "";
		variableToUpdate = "";
		doNotFillIfParentNoSelection = false;
		immediateParentFilterParam = "";
		parentFilterPrompts = new ArrayList<String>();
		hideLabel = false;
		readOnly = false;
		
		//sliders
		sliderType = "Point";
		sliderWidth = 100;
		sliderEndPtsType = "Query-Based";
		sliderDisplayLabels = "Ends";
		sliderStartPt = "0";
		sliderEndPt = "10";
		sliderIncrement = "1";
		sliderDefaultStart = "";
		sliderDefaultEnd = "";
	}
	
	public Prompt(Column col) {
		this();
		initPromptFromColumn(col, col.getName());
	}
	
	public Prompt(Column col, String label) {
		this();
		initPromptFromColumn(col, label);
	}
	
	private void initPromptFromColumn(Column col, String label) {
		this.Type = PromptType.Query;
		this.VisibleName = col.getDisplayName();	

		StringBuilder sb = new StringBuilder();
		String colName = col.getDimension();
		String hierarchy = col.getHierarchy();
		
		//SavedExpression ?
		boolean isSavedExpression = this.VisibleName.startsWith("SavedExpression(");
		if(isSavedExpression){
			colName = "EXPR." + this.VisibleName;
			 this.VisibleName = this.VisibleName.substring("SavedExpression(".length() + 1, this.VisibleName.length() - 2);
		}else{
			this.clazz = col.getClazz(ServerContext.getRepository());
		}
		
		if (colName == null || !Util.hasNonWhiteSpaceCharacters(colName) || colName.equals("EXPR")) {
			// measure
			this.Type = PromptType.Value;
			this.ParameterName = col.getName();
		}else if (isSavedExpression){
			this.ParameterName = colName;
			sb.append("SELECT ");
			sb.append(col.getDisplayName());
			sb.append(" FROM [ALL]");			
			this.Query = sb.toString();
		} else if (hierarchy != null && hierarchy.trim().length() > 0) {
			this.Type = PromptType.OlapHierarchy;
			this.dimensionName = col.getDimension() + '-' + col.getHierarchy();
			
			additionalOlapColumns = new ArrayList<OlapColumn>();
			String techName = col.getName();
			additionalOlapColumns.add(new OlapColumn(label, techName, AdhocColumn.SELECTED));
			
			this.ParameterName = col.getDimension() + "-" + col.getHierarchy();
		}else {
			// ugly hack for year limiting
			int minYear = ServerContext.getRepository().getMinYear();
			int maxYear = ServerContext.getRepository().getMaxYear();

			boolean isNewQueryLang = ServerContext.getRepository().isUseNewQueryLanguage();
			if (isNewQueryLang)
			{
				sb.append("SELECT [");
				sb.append(col.getDimension());
				sb.append('.');
				sb.append(col.getName());
				sb.append("] FROM [ALL]");

				if (col.getDimension().equals("Time") && (minYear > 0 || maxYear > 0))
				{
					sb.append(" WHERE ");
					if (minYear > 0)
					{
						sb.append("[Time.Year]>=");
						sb.append(minYear);
					}
					if (maxYear > 0)
					{
						if (minYear > 0)
							sb.append(" AND ");
						sb.append("[Time.Year]<=");
						sb.append(maxYear);
					}
					sb.append(" ORDER BY [");
					sb.append(col.getDimension());
					sb.append('.');
					sb.append(col.getName());
					sb.append("] ASC");
				}else{
					autoAddSort(col, sb, isNewQueryLang);
				}
			}
			else
			{
				sb.append("DC{");
				sb.append(col.getDimension());
				sb.append('.');
				sb.append(col.getName());
				sb.append("}");

				if (col.getDimension().equals("Time") && (minYear > 0 || maxYear > 0))
				{
					sb.append(",FAND{");
					if (minYear > 0)
					{
						sb.append("FDC{Time.Year>=");
						sb.append(minYear);
						sb.append('}');
					}
					if (maxYear > 0)
					{
						if (minYear > 0)
							sb.append(",");
						sb.append("FDC{Time.Year<=");
						sb.append(maxYear);
						sb.append('}');
					}
					sb.append('}');
					sb.append(",ODC{");
					sb.append(col.getDimension());
					sb.append('.');
					sb.append(col.getName());
					sb.append(",ASC}");
				}else{
					autoAddSort(col, sb, isNewQueryLang);
				}
			}
			
			this.ParameterName = col.getDimension() + "." + col.getName();
			this.Query = sb.toString();
		}
		this.addNoSelectionEntry = true;
		this.Operator = "=";
		this.multiSelectType = Prompt.MultipleSelectionType.OR;
		this.columnName = ParameterName; //isSavedExpression ? colName : col.getDimension() == null ? col.getName() : col.getDimension() + "." + col.getName();
	}

	private void autoAddSort(Column col, StringBuilder sb, boolean isNewQueryLanguage){
		if(isNewQueryLanguage){
			sb.append(" ORDER BY [");
			sb.append(col.getDimension());
			sb.append('.');
			sb.append(col.getName());
			sb.append("] ");
		}else{
			sb.append(",ODC{");
			sb.append(col.getDimension());
			sb.append('.');
			sb.append(col.getName());
			sb.append(",");
		}
	
		if (this.clazz != null && ((this.clazz.equals("Integer") || 
		   this.clazz.equals("Float") || 
		   this.clazz.equals("Double") || 
		   this.clazz.equals("Date") || 
		   this.clazz.equals("DateTime")))){
			sb.append("DESC");
		}else{
			sb.append("ASC");
		}
		
		if(!isNewQueryLanguage){
			sb.append("}");
		}
	}
	
	public Prompt(QueryFilter qf) {
		this(new Column(qf.getDimension(), qf.getColumn(), qf.getPromptName()));
		this.Operator = qf.getOperator();
		if (qf.isPrompted()) {
			this.ParameterName = qf.getPromptName();
			this.filterType = FilterType.DATA;
			setIsExpressionIfColNameNotEqParamName();
		}		
	}

	public Prompt(DisplayFilter df) {
		this(new Column(df.getTableName(), df.getColumn(), df.getPromptName()));
		this.Operator = df.getOperatorString();
		if (df.isPrompted()) {
			this.ParameterName = df.getPromptName();
			this.filterType = FilterType.DISPLAY;
			setIsExpressionIfColNameNotEqParamName();
		}		
	}
	
	public void setAsSavedExpression(String expression){
		if(expression != null && expression.startsWith("SavedExpression(")){
			this.Type = Type.Query;
			this.columnName = "EXPR." + expression;
			this.isExpression = true;
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT ");
			sb.append(expression);
			sb.append(" FROM [ALL]");			
			this.Query = sb.toString();
		}
	}
	
	/**
	 * Bug 9951. We need to set the checkbox for 'Change parameter name (for report expression prompts)'
	 */
	private void setIsExpressionIfColNameNotEqParamName(){
		if(columnName != null && ParameterName != null && !columnName.equals(ParameterName)){
			isExpression = true;
		}
	}

	public Prompt(QuickPrompt qp) {
		this(new Column(qp.getDimensionColumn(), qp.getVisibleName()));
	}

	public Prompt(TopFilter topFilter) {
		this();
		this.Type = PromptType.Value;
		this.VisibleName = this.ParameterName = topFilter.getPromptName();
		this.Operator = "=";
		this.multiSelectType = Prompt.MultipleSelectionType.NONE;
	}

	/**
	 * @override 
	 */
	public int compareTo(Object o1) {
    	if (this.VisibleName == ((Prompt) o1).VisibleName)
            return 0;
         else
            return(this.VisibleName.compareTo(((Prompt)o1).VisibleName));
    }

	public Object clone() throws CloneNotSupportedException
	{
		Prompt p = (Prompt) super.clone();
		return p;
	}

	/**
	 * 
	 * @return a list of prompt group prompts
	 */
	public static List<Prompt> getPromptGroups(List<Prompt> prompts)
	{
		List<Prompt> promptGroups = new ArrayList<Prompt>();
		
		if (SORT_PROMPTS) Collections.sort(prompts);
		
		for (Prompt p : prompts)
		{
			if (p.getType() == Prompt.PromptType.PromptGroup)
			{
				promptGroups.add(p);
			}
		}
		if (SORT_PROMPTS) Collections.sort(promptGroups);
		return promptGroups;
	}

	/**
	 * @return String version of this prompt
	 */
	public String toString()
	{
		switch (Type)
		{
		case Value:
			return (ParameterName + "=Value" + (getDefaultOption() != null ? " (default=" + getDefaultOption() + ")" : ""));
		case Date:
			return (ParameterName + "=Date" + (getDefaultOption() != null ? " (default=" + getDefaultOption() + ")" : ""));
		case List:
		case Query:
			StringBuilder sb = new StringBuilder();
			sb.append(ParameterName + " in [");
			if (Query != null)
			{
				sb.append(QUERY);
				if (getDefaultOption() != null)
					sb.append("] (default=" + getDefaultOption() + ")");
			} else
			{
				boolean first = true;
				if (FilledOptions != null)
					for (int i = 0; i < FilledOptions.size(); i++)
					{
						String s = FilledOptions.get(i);
						String val = s;
						if (FilledOptionValues != null)
						{
							if (i < FilledOptionValues.size())
								val = FilledOptionValues.get(i);
						}
						if (first)
							first = false;
						else
							sb.append(',');
						if (val.equals(getDefaultOption()))
						{
							sb.append(s + "(default)");
						} else
						{
							sb.append(s);
						}
					}
				else if (Options != null)
					for (int i = 0; i < Options.size(); i++)
					{
						String s = Options.get(i);
						String val = s;
						if (OptionValues != null)
						{
							if (i < OptionValues.size())
								val = OptionValues.get(i);
						}
						if (first)
							first = false;
						else
							sb.append(',');
						if (val.equals(getDefaultOption()))
						{
							sb.append(s + "(default)");
						} else
						{
							sb.append(s);
						}
					}
				sb.append("]");
			}
			return (sb.toString());
		case OperatorValue:
			return (ParameterName + " Operator Value" + (getDefaultOption() != null ? " (default=" + getDefaultOption() + ")" : ""));
		case TextOnly:
			return (VisibleName + " Text Only");
		}
		return ("");
	}

	/**
	 * @return Returns the options.
	 */
	public List<String> getOptions()
	{
		return Options;
	}

	/**
	 * @return Returns the parameterName.
	 */
	public String getParameterName()
	{
		return ParameterName;
	}

	/**
	 * @return Returns the query.
	 */
	private String getQuery()
	{
		return Query;
	}

	public List<String> getParents()
	{
		return parentFilterPrompts;
	}
	
	
	public void addParent(String s) {
		if (parentFilterPrompts == null)
			parentFilterPrompts = new ArrayList<String>();
		parentFilterPrompts.add(s);
	}
	
	public boolean hasParents()
	{
		return parentFilterPrompts.size() > 0;
	}
	
	/**
	 * @param query
	 *            The query to set.
	 * @param jdsp
	 *            JasperDataSourceProvider to use to create options from query
	 * @throws Exception
	 */
	private void setQueryWithHierarchicalFilters(String queryWithHierarchicalFilters)
	{
		QueryWithHierarchicalFilters = queryWithHierarchicalFilters;
		FilledOptions = null;
		FilledOptionValues = null;
		FilledEnabledValues = null;
	}

	private String getQueryToBeUsed(Map<String,String> hierarchicalFilterMap, Map<String,Object> parameterMap) {
		String queryToBeUsed = Query;
		if (getType() == PromptType.OlapHierarchy) {
			queryToBeUsed = this.generateOlapQuery(hierarchicalFilterMap, parameterMap);
		}
		if ((hierarchicalFilterMap != null) && (QueryWithHierarchicalFilters != null) && (!hierarchicalFilterMap.isEmpty()))
		{
			queryToBeUsed = QueryWithHierarchicalFilters;
			Iterator<String> iter = hierarchicalFilterMap.keySet().iterator();
			while (iter.hasNext())
			{
				String parentFilterParamName = (String) iter.next();
				String parentFilterParamVar = "V{" + parentFilterParamName + "}";
				Object v = hierarchicalFilterMap.get(parentFilterParamName);
				if (!(v instanceof String)) {
					continue;
				}
				String parentFilterParamValue = (String) v;
				queryToBeUsed = queryToBeUsed.replace(parentFilterParamVar, parentFilterParamValue);
			}
		}
		return queryToBeUsed;
	}
	
	/**
	 * Fill prompt options using query
	 * 
	 * @param jdsp
	 * @return 
	 */
	public String fillQuery(JasperDataSourceProvider jdsp, 
							Map<String, String> hierarchicalMap, 
							Map<String, Object> parameterMap)
	       throws Exception
	{
		if (((Query == null) || !isQueryBase()) && getType() != PromptType.OlapHierarchy)
			return null;
	
		String queryToBeUsed = getQueryToBeUsed(hierarchicalMap, parameterMap);
		boolean parentHasNoSelection = true;
		if(parentFilterPrompts.size() > 0) {
			for(int i = 0; i < parentFilterPrompts.size(); i++){
				Object parentFilterParamValue = hierarchicalMap.get(parentFilterPrompts.get(i));
				if (parentFilterParamValue != null && !parentFilterParamValue.equals(QueryString.NO_FILTER_TEXT)){
					parentHasNoSelection = false;
					break;
				}
			}
		}else{
			parentHasNoSelection = false;
		}

		if (FilledOptions == null)
			FilledOptions = new ArrayList<String>();
		else
			FilledOptions.clear();
		if (FilledOptionValues == null)
			FilledOptionValues = new ArrayList<String>();
		else
			FilledOptionValues.clear();
		if (FilledEnabledValues == null)
			FilledEnabledValues = new ArrayList<Boolean>();
		else
			FilledEnabledValues.clear();
		
		if(parentHasNoSelection && doNotFillIfParentNoSelection)
		{
			return null;
		}
		
		com.successmetricsinc.UserBean ub = SMIWebUserBean.getUserBean();
		Session session = null;
		if (ub != null)
			session = ub.getRepSession();
		QueryResultSet qrs = (QueryResultSet) jdsp.create(queryToBeUsed, session);	
		if (qrs == null)
		{
			// bug fix for 647.  If qrs is null, the query was canceled.
			return null;
		}
		
		String matchedValue = null;
		String resolvedDefaultOpt = getResolvedDefaultOption(); 
		
		int valueColumn = getPromptValueColumn(qrs);
		int enabledColumn = getPromptEnabledColumn(qrs);
		if (qrs != null)
		{
			for (Object[] row : qrs.getRows())
			{
				if (row.length > 0)
				{
					if (row[0] == null)
						FilledOptions.add("null");
					else
					{
						Object value = row[0];
						if (value instanceof java.util.Calendar)
						{					
							SimpleDateFormat sdf = null;
							String []warehouseColumnsDataTypes = qrs.getWarehouseColumnsDataTypes();
							if (warehouseColumnsDataTypes[0] != null && warehouseColumnsDataTypes[0].equals(DATE_TIME))
							{
								sdf = ub.getDateTimeFormat(this.columnFormat); 
							}
							else if (warehouseColumnsDataTypes[0] != null && warehouseColumnsDataTypes[0].equals(DATE))
							{
								sdf = ub.getDateFormatProcessingTimeZone(this.columnFormat);
							}
							if (sdf != null)
								value = sdf.format(((java.util.Calendar) value).getTime());
						}
						FilledOptions.add(value.toString());
					}
					if (row[valueColumn] == null)
						FilledOptionValues.add("null");
					else
					{
						Object value = row[valueColumn];
						if (value instanceof java.util.Calendar)
						{
							if (value instanceof java.util.Calendar)
							{
								String []warehouseColumnsDataTypes = qrs.getWarehouseColumnsDataTypes();
								if (warehouseColumnsDataTypes[valueColumn] != null && 
										warehouseColumnsDataTypes[valueColumn].equals(DATE_TIME))
								{
//									value = DateUtil.convertDateTimeToStandardFormat((java.util.Calendar)value, ub.getProcessingTimeZone());
									value = DateUtil.convertDateTimeToStandardFormat((java.util.Calendar)value, TimeZone.getTimeZone("GMT"));
								}							
								else if (warehouseColumnsDataTypes[valueColumn] != null && 
										warehouseColumnsDataTypes[valueColumn].equals(DATE))
								{
									value = DateUtil.convertDateToStandardFormat((java.util.Calendar)value, ub.getProcessingTimeZone());
								}							
							}
						}
						
						String v = value.toString();
						FilledOptionValues.add(v);
						
						if(!addNoSelectionEntry && matchedValue == null && resolvedDefaultOpt != null && !resolvedDefaultOpt.isEmpty()){
							if(resolvedDefaultOpt.equals(v)){
								matchedValue = v;
							}
						}
					}
					
					if (enabledColumn >= 0) {
						if (row[enabledColumn] == null)
							this.FilledEnabledValues.add(Boolean.TRUE);
						else {
							Object value = row[enabledColumn];
							if (value instanceof Boolean)
								this.FilledEnabledValues.add((Boolean)value);
							else
								this.FilledEnabledValues.add(Boolean.parseBoolean(value.toString()));
						}
					}
				}
			}
			//Handle the situation when the parameter map has a value that is not in the filled options
			if(parameterMap != null)
			{
				String value = (String)parameterMap.get(ParameterName);
				// handle the case of multiple selections
		        Repository r = ServerContext.getRepository();
				String orSeparator = r.getOrOperatorForMultiValueFilter();
				String andSeparator = r.getAndOperatorForMultiValueFilter();
				if (value != null && (value.indexOf(orSeparator) > 0 || value.indexOf(andSeparator) > 0)) {
					String sep = orSeparator;
					while (sep != null) {
						String workingValue = value;
						List<String> actualValues = new ArrayList<String>();
						int numberOfStrings = 0;
						while (workingValue != null) {
							int index = workingValue.indexOf(sep);
							String currentValue;
							if (index < 0) {
								currentValue = workingValue;
								workingValue = null;
							}
							else {
								currentValue = workingValue.substring(0, index);
								workingValue = workingValue.substring(index + 2);
							}
							if (FilledOptionValues.contains(currentValue) || FilledOptions.contains(currentValue)) {
								actualValues.add(currentValue);
								numberOfStrings++;
							}
						}
						if (actualValues.size() < numberOfStrings) {
							if (actualValues.isEmpty()) {
								if(addNoSelectionEntry)
								{
									workingValue = QueryString.NO_FILTER_TEXT;
								}
								else
								{
									if (FilledOptionValues.isEmpty())
									{
										logger.error("Prompt query returned zero values: " + queryToBeUsed);
										throw new Exception("Prompt query returned zero values: " + queryToBeUsed);
									}
									else
									{
										workingValue = FilledOptionValues.get(0);
									}
								}
							}
							else {
								workingValue = null;
								for (String singleValue : actualValues) {
									if (workingValue == null) {
										workingValue = singleValue;
									}
									else {
										workingValue = workingValue.concat(sep).concat(singleValue);
									}
								}
							}
							parameterMap.put(ParameterName, workingValue);
						}
						if (sep == orSeparator) {
							sep = andSeparator;
						}
						else {
							sep = null;
							// skip the next part since we've already handled it.
							value = null;
						}
					}
				}
				
				if(value != null && !FilledOptionValues.contains(value) && !FilledOptions.contains(value))
				{
					if (qrs.getRows().length == 1 && QueryString.NO_FILTER_TEXT.equals(value)) {
						parameterMap.put(ParameterName, FilledOptionValues.get(0));
					}
					else if(addNoSelectionEntry)
					{
						parameterMap.put(ParameterName, QueryString.NO_FILTER_TEXT);
					}
					else
					{
						if (FilledOptionValues.isEmpty())
						{
							logger.error("Prompt query returned zero values: " + queryToBeUsed);
							throw new Exception("Prompt query returned zero values: " + queryToBeUsed);
						}
						else
						{
							parameterMap.put(ParameterName, FilledOptionValues.get(0));
						}
					}
				}
			}
		}
		return matchedValue;
	}

	private String generateOlapQuery(Map<String, String> hierarchicalMap,
			Map<String, Object> parameterMap) {
		
		SMIWebUserBean ub = SMIWebUserBean.getUserBean();
		Repository r = ub.getRepository();
		Query q = new Query(r);
		List<MemberSelectionExpression> memberExpressions = OlapColumn.getOlapMemberExpressionMap(additionalOlapColumns);
		String dname = this.getColumnName();
		String fieldName = this.getParameterName();
		String dataType = "Varchar"; //rc.getDataType();
		String format = null; //rc.getSourceFormat();
		DimensionMemberSetNav dms;
		try {
			dms = q.addDimensionMemberSet(dname, memberExpressions, fieldName, dataType, format);
			// add parent name and ordinal now
			q.addDimMemberSetParent(dms, fieldName + BirstOlapField.PARENT);
			q.addDimMemberSetOrdinal(dms, fieldName + BirstOlapField.ORDINAL);
			q.addDimMemberSetUniqueName(dms, fieldName + BirstOlapField.UNIQUENAME);
			
			return q.getNewLogicalQueryString(false, null);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Error filling prompt", e);
		}
		return null;
	}

	private int getPromptValueColumn(QueryResultSet qrs) {
		int valueColumn = 0;
		if (this.Type == Prompt.PromptType.Query && this.queryColName == null && this.OptionValues != null && this.OptionValues.size() > 0) {
			// convert OptionValue to queryColName;
			this.queryColName = this.OptionValues.get(0);
		}
		
		if (queryColName != null && Util.hasNonWhiteSpaceCharacters(queryColName))
		{
			boolean foundValueColumn = false;
			String dimensionColumnName = queryColName.trim();
			String columnName = parsePromptValueColumn(dimensionColumnName);
			
			for (int i = 0; i < qrs.getColumnNames().length; i++)
			{
				if (qrs.getColumnNames()[i] == null)
				{
					logger.error(qrs);
				}
				if (qrs.getColumnNames()[i].equals(columnName))
				{
					valueColumn = i;
					foundValueColumn = true;
					break;
				}
			}
			if (!foundValueColumn)
			{
				logger.error("Unable to find query column name \"" + dimensionColumnName + "\"" + " for prompt: " + getVisibleName());
			}
		}
		return valueColumn;
	}
	
	private int getPromptEnabledColumn(QueryResultSet qrs) {
		int col = -1;
		if (enabledColName != null && Util.hasNonWhiteSpaceCharacters(enabledColName)) {
			boolean foundEnabledCol = false;
			String dimensionColumnName = enabledColName.trim();
			String columnName = parsePromptValueColumn(dimensionColumnName);
			for (int i = 0; i < qrs.getColumnNames().length; i++)
			{
				if (qrs.getColumnNames()[i] == null)
				{
					logger.error(qrs);
				}
				if (qrs.getColumnNames()[i].equals(columnName) || columnName.equals(qrs.getDisplayNames()[i]))
				{
					col = i;
					foundEnabledCol = true;
					break;
				}
			}
			if (!foundEnabledCol)
			{
				logger.error("Unable to find enabled column name \"" + dimensionColumnName + "\"" + " for prompt: " + getVisibleName());
			}
		}
		return col;
	}
	
	/**
	 * Parse and returns the column name, eg - 
	 * DC{Time.Year} => Year
	 * [Time.Year] => Year
	 * Time.Year => Year
	 * @param dimensionColumnName
	 * @return
	 */
	private String parsePromptValueColumn(String dimensionColumnName)
	{
		String colName = extractDimName('[', ']', dimensionColumnName);
		
		if(colName == null){
			colName = extractDimName('{', '}', dimensionColumnName);
		}
		
		if(colName == null){
			int dotIdx = dimensionColumnName.indexOf('.');
			if(dotIdx != -1){
				colName = dimensionColumnName.substring(dotIdx + 1);
			}
		}
		
		return (colName == null) ? dimensionColumnName : colName;
	}
	
	/**
	 * Extracts B from 'prefix'A.B'suffix'
	 * @param prefix
	 * @param suffix
	 * @param dimensionColumnName
	 * @return
	 */
	private String extractDimName(char prefix, char suffix, String dimensionColumnName){
		if ((dimensionColumnName.indexOf(prefix) != -1) && (dimensionColumnName.indexOf(suffix) != -1) && (dimensionColumnName.indexOf('.') != -1))
		{
			return dimensionColumnName.substring(dimensionColumnName.indexOf('.') + 1, dimensionColumnName.indexOf(suffix));
		}
		
		return null;
	}
	
	/**
	 * @return Returns the type.
	 */
	public PromptType getType()
	{
		return Type;
	}
	
	/**
	 * @return Returns the type string
	 */
	public String getTypeAsString()
	{
		if (Type!=null)
		{
			if (Type.equals(PromptType.CheckBox))
			{
				return "CheckBox";				
			}
			if (Type.equals(PromptType.List))
			{
				return "List";
			}
			if (Type.equals(PromptType.OperatorValue))
			{
				return "OperatorValue";
			}
			if (Type.equals(PromptType.PromptGroup))
			{
				return "PromptGroup";
			}
			if (Type.equals(PromptType.TextOnly))
			{
				return "TextOnly";
			}
			if (Type.equals(PromptType.Value))
			{
				return "Value";
			}
			if (Type.equals(PromptType.Slider))
			{
				return "Slider";
			}
			if (Type.equals(PromptType.Date))
			{
				return DATE;
			}
			if (Type.equals(PromptType.Query))
			{
				return QUERY;
			}
		}
		return null;
	}

	/**
	 * @return Returns the visibleName.
	 */
	public String getVisibleName()
	{
		return VisibleName;
	}

	/**
	 * @return Returns the defaultOption.
	 */
	public String getDefaultOption()
	{
		return DefaultOption;
	}

	/**
	 * @returns the resolved value of the defaultOption
	 */
	public String getResolvedDefaultOption(){
		SMIWebUserBean ub = SMIWebUserBean.getUserBean();
		String defaultOpt = getDefaultOption();
		if(defaultOpt != null && !defaultOpt.isEmpty() && defaultOpt.startsWith("V{")){
			Repository r = ub.getRepository();
			defaultOpt = r.replaceVariables(null, defaultOpt);
			defaultOpt = r.replaceVariables(ub.getRepSession(), defaultOpt);
		}
		return defaultOpt;
	}
	
	/**
	 * @return Returns the optionValues.
	 */
	public List<String> getOptionValues()
	{
		return OptionValues;
	}

	/**
	 * Return whether this prompt needs to be filled
	 * 
	 * @return
	 */
	public boolean needsFilling(boolean checkForParentFilterParams, Map<String, Object> parameterMap)
	{
		if ((Type == PromptType.Query) && (FilledOptions == null))
		{
			return (true);
		}
		boolean retVal = false;
		if (checkForParentFilterParams)
		{
			if ((Type == PromptType.Query) && (parentFilterParams != null) && (parentFilterParams.size() > 0))
			{
				if (parameterMap != null)
				{
					// See if the selected value is already part of the current prompt
					String selectedValue = (String) parameterMap.get(getParameterName());
					if (selectedValue != null)
					{
						if (FilledOptionValues != null && FilledOptionValues.contains(selectedValue))
						{
							retVal = false;
						} else if (FilledOptions.contains(selectedValue))
						{
							retVal = false;
						}
					}
					//Consider the flag do not fill if parent no selection
					if(doNotFillIfParentNoSelection)
					{
						if(parentFilterPrompts.size() > 0)
						{
							retVal = true;
						}
					}
				}
			}
		}
		return (retVal);
	}

	public boolean isAddNoSelectionEntry()
	{
		return addNoSelectionEntry;
	}

	public MultipleSelectionType getMultiSelectType()
	{
		if (multiSelectType == null) {
			multiSelectType = MultipleSelectionType.NONE;
		}
		return multiSelectType;
	}

	public List<String> getParentFilterParams()
	{
		return parentFilterParams;
	}

	public String getImmediateParentFilterParam()
	{
		return immediateParentFilterParam;
	}

	/**
	 * 
	 * @param prompts
	 * @param checkForOperationPromptGroupOnly
	 * @return boolean true if prompts are in any prompt group
	 */
	public boolean isInAnyPromptGroup(List<Prompt> prompts, 
									  boolean checkForOperationPromptGroupOnly)
	{
		boolean ingroup = false;
		for (Prompt p : prompts)
		{
			if (p.getType() != Prompt.PromptType.PromptGroup)
			{
				continue;
			}
			List<String> plist = p.getOptions();
			String operationName = p.getOperationName();
			if (plist.contains(getParameterName()) && (!checkForOperationPromptGroupOnly || ((operationName != null) && (!operationName.equals("")))))
			{
				ingroup = true;
				break;
			}
		}
		return ingroup;
	}

	public boolean isInPromptGroup(String name, List<Prompt> prompts)
	{
		boolean ingroup = false;
		for (Prompt p : prompts)
		{
			if (name == null || name.equals(p.getVisibleName()))
			{
				if (p.getType() != Prompt.PromptType.PromptGroup)
				{
					continue;
				}
				List<String> plist = p.getOptions();
				if (plist.contains(getParameterName()))
				{
					ingroup = true;
					break;
				}
			}
		}
		return ingroup;
	}

	/**
	 * @return Returns the operationName.
	 */
	public String getOperationName()
	{
		return operationName;
	}

    public void createQueryWithHierarchicalFilters(Repository r, List<Prompt> prompts)
    {
    	StringBuilder sb = new StringBuilder();
    	String query = getQuery();
    	if (SORT_PROMPTS) Collections.sort(prompts);
    	if(Type != PromptType.Query || query == null)
    	{
    		return;
    	}
    	sb.append(query);
    	if((prompts != null) 
    		&& (prompts.size() > 0) 
    		&& parentFilterPrompts.size() > 0)
    	{
    		for(Prompt parentPrompt : prompts)
    		{
    			if(parentPrompt == this) //Exclude self
    			{
    				continue;
    			}
    			
    			if (parentPrompt.isAncestorPrompt(this, prompts)) {
    				if (r.isUseNewQueryLanguage())
    				{
    					// for the new query language, add [WHERE|AND] firstDimension=GETPROMPTVALUE('parentColumnName')
    					String value = QueryFilter.getPromptValueString(parentPrompt.getParameterName(), parentPrompt.getClazz());
    					if (value == null || value.equals(QueryString.NO_FILTER_TEXT))
    						continue;
    			    	int whereIndex = sb.indexOf(" WHERE "); // XXX ugly hack, assumes no measure filters
    					if (whereIndex < 0)
    						sb.append(" WHERE ");
    					StringBuilder clause = new StringBuilder();
    					clause.append('[');
    					
    					String colName = parentPrompt.getColumnName();
    					if(colName == null || colName.indexOf('.') < 0){
    						colName = parentPrompt.columnName;
    					}
    					clause.append(colName);
    					
    					clause.append("]");
    					clause.append(validParentQueryOperator(parentPrompt.getOperator()));
    					clause.append(value);
    					if (whereIndex >= 0)
    					{
    						clause.append(" AND ");
    						sb.insert(whereIndex + 7, clause);
    					}
    					else
    					{
    						sb.append(clause);
    					}
    				}
    				else
    				{
    					String value = null;
    					List<String> parentOptionValues = parentPrompt.getSelectedValues();
    					if(parentOptionValues != null)
    					{
    						StringBuilder sb1 = new StringBuilder();
    						for (String s : parentOptionValues) {
    							if (sb1.length() > 0) {
    								sb1.append("||");
    							}
    							sb1.append(s);
    						}
    						value = sb1.toString();
    					}
    					if (value == null || !Util.hasNonWhiteSpaceCharacters(value) || QueryString.NO_FILTER_TEXT.equals(value)) {
    						continue;
    					}

    					String firstDimension = parentPrompt.getColumnName();
    					
    					if(firstDimension == null || firstDimension.indexOf('.') < 0){
    						firstDimension = parentPrompt.getQueryColumnName();
    					}
    					
    					if(firstDimension == null || !Util.hasNonWhiteSpaceCharacters(firstDimension))
    					{
    						String parentQuery = parentPrompt.getQuery();
    						if(parentQuery != null)
    						{
    							if(parentQuery.indexOf(',') != -1)
    							{
    								firstDimension = parentQuery.substring(0, parentQuery.indexOf(','));
    							}
    							else
    							{
    								firstDimension = parentQuery;
    							}
    						}
    					}

    					if((firstDimension.indexOf('{') != -1) && (firstDimension.indexOf('}') != -1))
    					{
    						firstDimension = firstDimension.substring(firstDimension.indexOf('{')+1, firstDimension.indexOf('}'));
    					}
    					sb.append(",");
    					sb.append("FDC{");
    					sb.append(firstDimension);
    					sb.append(validParentQueryOperator(parentPrompt.getOperator()));
    					sb.append(value);
    					sb.append("}");
    				}
    			}
    		}
        	String queryWithHierarchicalFilters = sb.toString();
            if(!query.equals(queryWithHierarchicalFilters))
            {
            	setQueryWithHierarchicalFilters(queryWithHierarchicalFilters);
            }
            else
            {
            	setQueryWithHierarchicalFilters(null);
            }
    	}
    }
    
    //Bug 14815
    private String validParentQueryOperator(String operator){
    	if(operator != null){
			String op = operator.toLowerCase();
			if(op.equals("range") || op.indexOf("like") >= 0){
				return "=";
			}
		}else{
			return "=";
		}
    	return operator;
    }
    
    private boolean isAncestorPrompt(Prompt p, List<Prompt> list) {
    	return isAncestorPrompt(p, list, new ArrayList<Prompt>());
    }
    
    private boolean isAncestorPrompt(Prompt p, List<Prompt> list, List<Prompt> alreadyChecked) {
    	if (alreadyChecked.contains(p)) {
    		// throw an exception here?
    		logger.error("Prompt " + p.getParameterName() + " is configured wrong.  The parent prompts are recursive.");
    		return false;
    	}
    
    	alreadyChecked.add(p);
    	
    	List<String> parents = p.parentFilterPrompts;
    	int numParents = parents.size();
    	if(numParents == 0){
    		return false;
    	}
    	
    	for(int i = 0; i < numParents; i++){
    		String promptParent = parents.get(i);
    		if(promptParent.equals(getParameterName())){
    			return true;
    		}
    	}
    	
    	for (Prompt prompt: list) {
    	    for(int i = 0; i < numParents; i++){
    			String parent = parents.get(i);
    			if (parent.equals(prompt.getParameterName())) {
    				return isAncestorPrompt(prompt, list, alreadyChecked);
    			}
    		}
    	}
    	
    	return false;
    }
    /**
     * 
     * @param parameters
     * @param jdsp
     * @param hierarchicalMap
     */
    public void fillIfNecessary(Map<String, Object> parameters, 
    						    JasperDataSourceProvider jdsp,
    						    Map<String, String> hierarchicalMap)
    {
		if (getType() == Prompt.PromptType.Query) {
			if (needsFilling(true, parameters)) {
				try {
					fillQuery(jdsp, hierarchicalMap, parameters);
				} catch (Exception e) {
					// logger.error(e.getMessage(), e); // don't log this stack trace, not needed
				}
			}
		}
    }
    
    public final List<String> getFilledOptions() {
    	return this.FilledOptions;
    }
    public final List<String> getFilledOptionValues() {
    	return this.FilledOptionValues;
    }
    
    public final List<Boolean> getFilledEnabledValues() {
    	return this.FilledEnabledValues;
    }
	/**
	 * 
	 * @return String
	 */
	
	public String getQueryColumnName() {
		if (this.getType() == PromptType.Query) {
			if (queryColName != null && Util.hasNonWhiteSpaceCharacters(queryColName)) {
				return queryColName;
			}
			List<String> list = getOptionValues();
			if (list != null && !list.isEmpty() && list.get(0) != null && Util.hasNonWhiteSpaceCharacters(list.get(0))) {
				return list.get(0);
			}
			String q = this.getQuery();
			if (q != null && Util.hasNonWhiteSpaceCharacters(q)) {
				int openCurly = q.indexOf("DC{");	// hopefully this is only needed for the old query language
				int closeCurly = q.indexOf('}', openCurly + 3);
				if (openCurly >=0 && closeCurly > openCurly) {
					return q.substring(openCurly + 3, closeCurly);
				}
			}
		}
		
		return null;
	}

	public List<String> getSelectedValues() {
		return this.selectedValues;
	}
	
	public void setSelectedValues(List<String> s) {
		this.selectedValues = s;
	}

	public void setSelectedValue(String selectedValue) {
		//Bug 12850
		//Check that the date value doesn't include 00:00:00
		//Remove it otherwise because the client doesn't expects it
		if(selectedValue != null && clazz != null && clazz.equals(DATE)){
			int idx = selectedValue.indexOf(" 00:00:00");
			if(idx > 0){
				selectedValue = selectedValue.substring(0, idx);
			}
		}
		
		this.selectedValues = new ArrayList<String>();
		selectedValues.add(selectedValue);
	}

	@SuppressWarnings("unchecked")
	public boolean parseElement(OMElement parent) throws SQLException {
		boolean hasType = false;
		boolean versionSet = false;
		
		selectedValues = new ArrayList<String>();
		for (Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();
			if (PROMPT_TYPE.equals(elName)) {
				String promptValue = XmlUtils.getStringContent(el);
				for (Entry<PromptType, String> entry : promptTypes.entrySet()) {
					if (entry.getValue().equals(promptValue)) {
						this.Type = entry.getKey();
					}
				}
			}
			else if (PARAMETER_NAME.equals(elName)) {
				ParameterName = XmlUtils.getStringContent(el);
			}
			else if (OPERATOR_PARAMETER_NAME.equals(elName)) {
				OperatorParameterName = XmlUtils.getStringContent(el);
			}
			else if (VISIBLE_NAME.equals(elName)) {
				VisibleName = XmlUtils.getStringContent(el);
			}
			else if (VERSION.equals(elName)) {
				versionSet = true;
				version = XmlUtils.getIntContent(el);
			}
			else if (OPTIONS.equals(elName)) {
				Options = new ArrayList<String>();
				for (Iterator<OMElement> children = el.getChildElements(); children.hasNext(); ) {
					OMElement child = children.next();
					String childName = child.getLocalName();
					if (OPTION.equals(childName)) {
						Options.add(XmlUtils.getStringContent(child));
					}
				}
			}
			else if (OPTION_VALUES.equals(elName)) {
				OptionValues = new ArrayList<String>();
				for (Iterator<OMElement> children = el.getChildElements(); children.hasNext(); ) {
					OMElement child = children.next();
					String childName = child.getLocalName();
					if (OPTION_VALUE.equals(childName)) {
						OptionValues.add(XmlUtils.getStringContent(child));
					}
				}
			}
			else if (OPERATORS.equals(elName)) {
				Operators = new ArrayList<String>();
				for (Iterator<OMElement> children = el.getChildElements(); children.hasNext(); ) {
					OMElement child = children.next();
					String childName = child.getLocalName();
					if (OPERATOR.equals(childName)) {
						Operators.add(XmlUtils.getStringContent(child));
					}
				}
			}
			else if (DEFAULT_OPTION.equals(elName)) {
				DefaultOption = XmlUtils.getStringContent(el);
			}
			else if (QUERY.equals(elName)) {
				Query = XmlUtils.getStringContent(el);
			}
			else if (QUERY_COL_NAME.equals(elName)) {
				queryColName = XmlUtils.getStringContent(el);
			}
			else if (Prompt.ENABLED_COL_NAME.equals(elName)) {
				enabledColName = XmlUtils.getStringContent(el);
			}
			else if (ROW.equals(elName)) {
				row = XmlUtils.getIntContent(el);
			}
			else if (ADD_NO_SELECTION_ENTRY.equals(elName)) {
				addNoSelectionEntry = XmlUtils.getBooleanContent(el);
			}
			else if (INCLUDED_PROMPT_INDICES.equals(elName)) {
				includedPromptIndices = new ArrayList<Integer>();
				for (Iterator<OMElement> children = el.getChildElements(); children.hasNext(); ) {
					OMElement child = children.next();
					String childName = child.getLocalName();
					if (INCLUDED.equals(childName)) {
						includedPromptIndices.add(XmlUtils.getIntContent(child));
					}
				}
			}
			else if (TEXT.equals(elName)) {
				text = XmlUtils.getStringContent(el);
			}
			else if (TARGET_DASHBOARD_NAME.equals(elName)) {
				targetDashboardName = XmlUtils.getStringContent(el);
			}
			else if (PARENT_FILTER_PARAMS.equals(elName)) {
				parentFilterParams = new ArrayList<String>();
				for (Iterator<OMElement> children = el.getChildElements(); children.hasNext(); ) {
					OMElement child = children.next();
					String childName = child.getLocalName();
					if (FILTER.equals(childName)) {
						parentFilterParams.add(XmlUtils.getStringContent(child));
					}
				}
			}
			if (MULTI_SELECT_TYPE.equals(elName)) {
				String promptValue = XmlUtils.getStringContent(el);
				for (Entry<MultipleSelectionType, String> entry : multipleSelectionTypes.entrySet()) {
					if (entry.getValue().equals(promptValue)) {
						this.multiSelectType = entry.getKey();
					}
				}
			}
			else if (HIDE_IF_ONLY_ONE_ROW.equals(elName)) {
				hideIfOnlyOneRow = XmlUtils.getBooleanContent(el);
			}
			else if (OPERATION_NAME.equals(elName)) {
				operationName = XmlUtils.getStringContent(el);
			}
			else if (IMAGE_PATH.equals(elName)) {
				imagePath = XmlUtils.getStringContent(el);
			}
			else if (VALIDATION_MESSAGE.equals(elName)) {
				validationMessage = XmlUtils.getStringContent(el);
			}
			else if (VARIABLE_TO_UPDATE.equals(elName)) {
				variableToUpdate = XmlUtils.getStringContent(el);
			}
			else if (DO_NOT_FILL_IF_PARENT_NO_SELECTION.equals(elName)) {
				doNotFillIfParentNoSelection = XmlUtils.getBooleanContent(el);
			}
			else if (IMMEDIATE_PARENT_FILTER_PARAM.equals(elName)) {
				immediateParentFilterParam = XmlUtils.getStringContent(el);
				String filterParam = XmlUtils.getStringContent(el);
				if(filterParam != null){
					parentFilterPrompts.add(filterParam);
				}
			}else if (PARENTS.equals(elName)){
				for (Iterator<OMElement> children = el.getChildrenWithLocalName(PARENT); children.hasNext(); ) {
					OMElement child = children.next();
					String p = XmlUtils.getStringContent(child);
					if(p != null){
						parentFilterPrompts.add(p);
					}
				}
			}else if (HIDE_LABEL.equals(elName)) {
				hideLabel = XmlUtils.getBooleanContent(el);
			}
			else if (SELECTED_DATE.equals(elName)) {
				selectedDate = XmlUtils.getStringContent(el);
			}
			else if (LIST_VALUES.equals(elName)) {
				listValues = new ArrayList<ListValue>();
				for (Iterator<OMElement> children = el.getChildrenWithLocalName(LIST_VALUE); children.hasNext(); ) {
					OMElement child = children.next();
					String childName = child.getLocalName();
					if (LIST_VALUE.equals(childName)) {
						ListValue listValue = new ListValue();
						listValue.parseElement(child);
						listValues.add(listValue);
					}
				}
			}
			else if (SELECTED_VALUES.equals(elName)) {
				for (Iterator<OMElement> children = el.getChildrenWithLocalName(SELECTED_VALUE); children.hasNext(); ) {
					OMElement child = children.next();
					String childName = child.getLocalName();
					if (SELECTED_VALUE.equals(childName)) {
						selectedValues.add(XmlUtils.getStringContent(child));
					}
				}
			}
			else if (OPERATOR.equals(elName)) {
				Operator = XmlUtils.getStringContent(el);
			}
			else if (CLAZZ.equals(elName)) {
				clazz = XmlUtils.getStringContent(el);
			}else if (REPORT_PATH.equals(elName)){
				reportPath = XmlUtils.getStringContent(el);
			}else if (TOOLTIP.equals(elName)){
				toolTip = XmlUtils.getStringContent(el);
			} else if (FILTER_TYPE.equals(elName)) {
				String rf = XmlUtils.getStringContent(el);
				if (Filter.DATA.equals(rf)) {
					filterType = FilterType.DATA;
				}
				else {
					filterType = FilterType.DISPLAY;
				}
			}else if (COLUMN_FORMAT.equals(elName)){
				columnFormat = XmlUtils.getStringContent(el);
			}
			else if (COLUMN_NAME.equals(elName)) {
				columnName = XmlUtils.getStringContent(el);
			}
			else if (IS_EXPRESSION.equals(elName)) {
				isExpression = XmlUtils.getBooleanContent(el);
			}else if (COLLECTION.equals(elName)){
				collection = XmlUtils.getStringContent(el);
			}else if (READ_ONLY.equals(elName)){
				readOnly = XmlUtils.getBooleanContent(el);
			}else if(SLIDER_TYPE.equals(elName)){
				sliderType = XmlUtils.getStringContent(el);
			}else if(SLIDER_WIDTH.equals(elName)){
				sliderWidth = XmlUtils.getIntContent(el);
			}else if(SLIDER_ENDPOINTS.equals(elName)){
				sliderEndPtsType = XmlUtils.getStringContent(el);
			}else if(SLIDER_DISPLAY_LABELS.equals(elName)){
				sliderDisplayLabels = XmlUtils.getStringContent(el);
			}else if(SLIDER_START_PT.equals(elName)){ 
				sliderStartPt = XmlUtils.getStringContent(el);
			}else if(SLIDER_END_PT.equals(elName)){
				sliderEndPt = XmlUtils.getStringContent(el);
			}else if(SLIDER_INCREMENT.equals(elName)){
				sliderIncrement = XmlUtils.getStringContent(el);
			}else if(SLIDER_DEFAULT_START.equals(elName)){
				sliderDefaultStart = XmlUtils.getStringContent(el);
			}else if(SLIDER_DEFAULT_END.equals(elName)){
				sliderDefaultEnd = XmlUtils.getStringContent(el);
			}else if (EXPAND_INTO_BOXES.equals(elName)) {
				expandIntoBoxes = XmlUtils.getBooleanContent(el);
			}else if (TREE_CONTROL.equals(elName)) {
				expandIntoTree = XmlUtils.getBooleanContent(el);
			} else if (USE_DRILL_PATH.equals(elName)) {
				useDrillPath = XmlUtils.getBooleanContent(el);
			}
			else if (MULTILINE.equals(elName)) {
				this.mutliline = XmlUtils.getBooleanContent(el);
			}
			else if (AdhocColumn.ADDITIONAL_OLAP_COLUMNS.equals(elName)) {
				additionalOlapColumns = OlapColumn.parseOlapColumns(el);
			}
		}
		
		// handle backwards compatibility - may not have clazz set
		if (clazz == null) {
			// default to Varchar
			clazz = VARCHAR;
			
			if (this.Type == PromptType.Value) {
				clazz = FLOAT;
			}
			else if (this.Type == PromptType.Query && this.Query != null) {
				SMIWebUserBean ub = SMIWebUserBean.getUserBean();
				JasperDataSourceProvider jdsp = ub.getJasperDataSourceProvider();
				try {
					// XXX this is a hack, should generate the query object and force in a top 0
					String queryToUse = null;
					if (Query.toLowerCase().startsWith("select"))
					{
						int end1 = 6;
						int end2 = 7;
						int index = Query.toLowerCase().indexOf(" top ");
						if (index > 0)
						{
							end1 = index + 5;
							end2 = Query.indexOf(end1, ' ');
						}
						queryToUse = Query.substring(0, end1) + " TOP 0 " + Query.substring(end2); 
					}
					else
					{
						queryToUse = Query + ",TOP{0}";
					}
					logger.debug("modified: " + queryToUse);
					QueryResultSet qrs = (QueryResultSet) jdsp.create(queryToUse);
					int valueColumn = getPromptValueColumn(qrs);
					int[] cdts = qrs.getColumnDataTypes();
					if (cdts != null && cdts.length > valueColumn) {
						switch (cdts[valueColumn]) {
						case Types.INTEGER: clazz = INTEGER; break;
						case Types.DOUBLE:  clazz = FLOAT; break;
						case Types.VARCHAR: clazz = VARCHAR; break;
						case Types.BOOLEAN: clazz = BOOLEAN; break;
						case Types.DATE: clazz = DATE; break;
						case Types.TIMESTAMP: clazz = DATE_TIME; break;
						}
					}
				} catch (Exception e) {
					logger.error(e, e);
					
					//Uh oh. Bad query. Default to Varchar
					clazz = VARCHAR;
				}	
			}
		}else{
			hasType = true;
		}

		//check if we're date related and didn't have a display format
		//if we don't, then we give it the old default. New ones should have it.
		if (clazz.equals(DATE) || clazz.equals(DATE_TIME)){
			if (columnFormat == null){
				try{
					//Parse default option
					DefaultOption = parseOldDateTimeFormat2New(DefaultOption);
					
					//Flex default date time format
					columnFormat = clazz.equals(DATE) ? DateUtil.DEFAULT_DATE_FORMAT : DateUtil.DEFAULT_DATETIME_FORMAT;		
					
					//Write back to disk our column format
					hasType = false;
				}catch(Exception e){
					logger.error("Unable to parse old prompt date default / selection values: " + e.toString());
				}
			}
		}
		
		if (columnName == null) {
			
			if(queryColName != null){
				String queryColumn = queryColName;
				//Try to validate that this column name exists.
				//Strip of DC{} or [] from DC{Dim.Col} or [Dim.Col]
				int newQueryBracketIdx = queryColName.indexOf('[');
				int oldQueryBracketIdx = queryColName.indexOf("DC{");
				
				int startIdx = -1;
				char endBracket = '*';
				if (oldQueryBracketIdx >= 0){
					startIdx = oldQueryBracketIdx + 2;
					endBracket = '}';
				} else if(newQueryBracketIdx >= 0){
					startIdx = newQueryBracketIdx;
					endBracket = ']';
				}
				
				if(startIdx > 0){
					int endIdx = queryColName.indexOf(endBracket, startIdx + 1);
					if(endIdx > 0){
						queryColumn = queryColName.substring(startIdx + 1, endIdx);
					}
				}
				
				//Validate if it exists. Save it.
				String[] dimCol = queryColumn.split("\\.");
				if(dimCol.length == 2){
					if(ServerContext.getRepository().findDimensionColumn(dimCol[0], dimCol[1]) != null){
						columnName = queryColumn;
						logNullColumnName("QueryColumnName");
					}
				}
			}
			
			// set queryColName if possible
			if (columnName == null && queryColName == null && Query != null) {
				// get it from the query
				String q = Query;
				int firstIndex = 0;
				int secondIndex = 0;
				if (q.toLowerCase().startsWith("select")) {
					firstIndex = q.indexOf('[');
					secondIndex = q.indexOf(']');
				}
				else {
					firstIndex = q.indexOf("DC{");
					secondIndex = q.indexOf('}', firstIndex + 3);
				}
				if (firstIndex >= 0 && secondIndex > firstIndex) {
					queryColName = q.substring(firstIndex + 3, secondIndex);
					columnName = queryColName;
					logNullColumnName("Query");
				}
			}
			
			if(columnName == null && queryColName != null){
				columnName = queryColName;
				logNullColumnName("QueryColumnName");
			}
		}
			
		// set immediateParentFilterParam if parentFilterParams is set but immediateParentFilterParams is not set.
		if (immediateParentFilterParam == null || !Util.hasNonWhiteSpaceCharacters(immediateParentFilterParam)) {
			List<String> parents = this.getParentFilterParams();
			if (parents != null && parents.size() > 0) {
				immediateParentFilterParam = parents.get(0); 
			}
		}

		if (this.Type == Prompt.PromptType.Query && this.queryColName == null && this.OptionValues != null && this.OptionValues.size() > 0) {
			// convert OptionValue to queryColName;
			this.queryColName = this.OptionValues.get(0);
		}
		
		if (versionSet == false || version != CURRENT_VERSION) {
			// update version
			if (versionSet == false || version < 1) {
				// upgrade to version 1
				// set filter type to data instead of display
				filterType = FilterType.DATA;
				version = UNKNOWN_VERSION;
			}
		}
		
		//version 2 upgrade change - detect parameter not the same as column name, assume it is an expression prompt
		if(version < 2){
			setIsExpressionIfColNameNotEqParamName();
		}
			
		version = CURRENT_VERSION;
		return hasType;
	}
	
	private void logNullColumnName(String substitution){
		if(dashboardPage != null){
			logger.info("NULL Column Name, Page [" + dashboardPage + "], Prompt [" + VisibleName + "], Column Name [" + columnName + "], used " + substitution);
		}
	}
	
	private String parseOldDateTimeFormat2New(String oldDateStr) throws java.text.ParseException{
		if (oldDateStr != null && !oldDateStr.equals("") && (oldDateStr.indexOf("V{") == -1)){
			boolean isDate = clazz.equals(DATE);
			
			SimpleDateFormat oldFmt = 
					isDate ? 
					new SimpleDateFormat(DEFAULT_DISPLAY_DATE_FORMAT) :
					new SimpleDateFormat(DEFAULT_DISPLAY_DATETIME_FORMAT);
			
			SimpleDateFormat newFmt =
					isDate ? 
					new SimpleDateFormat(DateUtil.DEFAULT_DATE_FORMAT) :
					new SimpleDateFormat(DateUtil.DEFAULT_DATETIME_FORMAT);
					
			// check to see if already in the new format
			try
			{
				@SuppressWarnings("unused")
				Date dt1 = newFmt.parse(oldDateStr);
				return oldDateStr;
			}
			catch (Exception ex)
			{
			}
			
			Date dt = oldFmt.parse(oldDateStr);
			return newFmt.format(dt);
		}
		
		//No formatting required
		return oldDateStr;
	}
	
	@Override
	public void addContent(OMElement parent, OMFactory factory, OMNamespace ns) {
		OMElement el = factory.createOMElement(PROMPT, ns);
		XmlUtils.addContent(factory, el, VERSION, version, ns);
		XmlUtils.addContent(factory, el, PROMPT_TYPE, promptTypes.get(Type), ns);
		XmlUtils.addContent(factory, el, PARAMETER_NAME, ParameterName, ns);
		XmlUtils.addContent(factory, el, OPERATOR_PARAMETER_NAME, OperatorParameterName, ns);
		XmlUtils.addContent(factory, el, VISIBLE_NAME, VisibleName, ns);
		if (Options != null) {
			OMElement ops = factory.createOMElement(OPTIONS, ns);
			for (String s : Options) {
				XmlUtils.addContent(factory, ops, OPTION, s, ns);
			}
			el.addChild(ops);
		}
		if (OptionValues != null) {
			OMElement opVals = factory.createOMElement(OPTION_VALUES, ns);
			for (String s : OptionValues) {
				XmlUtils.addContent(factory, opVals, OPTION_VALUE, s, ns);
			}
			el.addChild(opVals);
		}
		if (Operators != null) {
			OMElement ops = factory.createOMElement(OPERATORS, ns);
			for (String s : Operators) {
				XmlUtils.addContent(factory, ops, OPERATOR, s, ns);
			}
			el.addChild(ops);
		}
		XmlUtils.addContent(factory, el, DEFAULT_OPTION, DefaultOption, ns);
		XmlUtils.addContent(factory, el, QUERY, Query, ns);
		if (this.Type == Prompt.PromptType.Query && this.queryColName == null && this.OptionValues != null && this.OptionValues.size() > 0) {
			// convert OptionValue to queryColName;
			this.queryColName = this.OptionValues.get(0);
		}
		XmlUtils.addContent(factory, el, QUERY_COL_NAME, queryColName, ns);
		
		if (this.Type == Prompt.PromptType.Query) {
			XmlUtils.addContent(factory, el, ENABLED_COL_NAME, this.enabledColName == null ? "" : this.enabledColName, ns);
		}
		// I think this should be transient as it is calculated
//		XmlUtils.addContent(factory, el, "QueryWithHierarchicalFilters", QueryWithHierarchicalFilters, ns);
		XmlUtils.addContent(factory, el, ROW, row, ns);
		XmlUtils.addContent(factory, el, ADD_NO_SELECTION_ENTRY, addNoSelectionEntry, ns);
		if (includedPromptIndices != null && includedPromptIndices.size() > 0) {
			OMElement included = factory.createOMElement(INCLUDED_PROMPT_INDICES, ns);
			for (Integer i : includedPromptIndices) {
				XmlUtils.addContent(factory, included, INCLUDED, i, ns);
			}
			el.addChild(included);
		}
		XmlUtils.addContent(factory, el, TEXT, text, ns);
		XmlUtils.addContent(factory, el, TARGET_DASHBOARD_NAME, targetDashboardName, ns);
		if (parentFilterParams != null && parentFilterParams.size() > 0) {
			OMElement parentFilters = factory.createOMElement(PARENT_FILTER_PARAMS, ns);
			for (String s : parentFilterParams) {
				XmlUtils.addContent(factory, parentFilters, FILTER, s, ns);
			}
			el.addChild(parentFilters);
		}
		XmlUtils.addContent(factory, el, MULTI_SELECT_TYPE, multipleSelectionTypes.get(multiSelectType), ns);
		XmlUtils.addContent(factory, el, HIDE_IF_ONLY_ONE_ROW, hideIfOnlyOneRow, ns);
		XmlUtils.addContent(factory, el, OPERATION_NAME, operationName, ns);
		XmlUtils.addContent(factory, el, IMAGE_PATH, imagePath, ns);
		XmlUtils.addContent(factory, el, VALIDATION_MESSAGE, validationMessage, ns);
		XmlUtils.addContent(factory, el, VARIABLE_TO_UPDATE, variableToUpdate, ns);
		XmlUtils.addContent(factory, el, DO_NOT_FILL_IF_PARENT_NO_SELECTION, doNotFillIfParentNoSelection, ns);
		
		XmlUtils.addContent(factory, el, IMMEDIATE_PARENT_FILTER_PARAM, immediateParentFilterParam, ns);
		
		if (parentFilterPrompts != null && parentFilterPrompts.size() > 0) {
			OMElement parents = factory.createOMElement(PARENTS, ns);
			for (String s : parentFilterPrompts)
				XmlUtils.addContent(factory, parents, PARENT, s, ns);
			el.addChild(parents);
		}
		
		XmlUtils.addContent(factory, el, HIDE_LABEL, hideLabel, ns);
		XmlUtils.addContent(factory, el, SELECTED_DATE, selectedDate, ns);
		if (selectedValues != null && selectedValues.size() > 0) {
			OMElement values = factory.createOMElement(SELECTED_VALUES, ns);
			for (String v : selectedValues) {
				XmlUtils.addContent(factory, values, SELECTED_VALUE, v, ns);
			}
			el.addChild(values);
		}
		
		if (this.Type == Prompt.PromptType.List && (this.listValues == null || this.listValues.isEmpty())) {
			// need to convert from old style Options and OptionValues to List
			if (this.Options != null && !this.Options.isEmpty()) {
				this.listValues = new ArrayList<ListValue>();
				for (int i = 0; i < this.Options.size(); i++) {
					String option = this.Options.get(i);
					ListValue l = new ListValue();
					l.setLabel(option);
					if (this.OptionValues != null && this.OptionValues.size() > i) {
						String v = this.OptionValues.get(i);
						if (v != null && Util.hasNonWhiteSpaceCharacters(v)) {
							option = v;
						}
					}
					l.setValue(option);
					this.listValues.add(l);
				}
			}
		}
		
		if (this.listValues != null && !this.listValues.isEmpty()) {
			OMElement included = factory.createOMElement(LIST_VALUES, ns);
			for (ListValue lv : listValues) {
				lv.addContent(included, factory, ns);
			}
			el.addChild(included);
		}
		if (Operator == null || !Util.hasNonWhiteSpaceCharacters(Operator)) {
			Operator = "=";
		}
		XmlUtils.addContent(factory, el, OPERATOR, Operator, ns);
		XmlUtils.addContent(factory, el, CLAZZ, clazz, ns);
		XmlUtils.addContent(factory, el, FILTER_TYPE, filterType == FilterType.DATA ? Filter.DATA : Filter.DISPLAY, ns);
		if(reportPath != null){
			XmlUtils.addContent(factory, el, REPORT_PATH, reportPath, ns);
		}
		String tip = (toolTip != null) ? toolTip : "";
		XmlUtils.addContent(factory, el, TOOLTIP, tip, ns);
		XmlUtils.addContent(factory, el, COLUMN_FORMAT, columnFormat == null ? "" : columnFormat, ns);
		
		if (columnName != null){
			XmlUtils.addContent(factory, el, COLUMN_NAME, columnName, ns);
		}
		XmlUtils.addContent(factory, el, IS_EXPRESSION, isExpression, ns);
		
		if(collection != null){
			XmlUtils.addContent(factory, el, COLLECTION, collection, ns);
		}
		
		XmlUtils.addContent(factory, el, READ_ONLY, readOnly, ns);
		
		//Sliders
		XmlUtils.addContent(factory, el, SLIDER_TYPE, sliderType, ns);
		XmlUtils.addContent(factory, el, SLIDER_WIDTH, sliderWidth, ns);
		XmlUtils.addContent(factory, el, SLIDER_DISPLAY_LABELS, sliderDisplayLabels, ns);
		XmlUtils.addContent(factory, el, SLIDER_ENDPOINTS, sliderEndPtsType, ns);
		XmlUtils.addContent(factory, el, SLIDER_START_PT, sliderStartPt, ns);
		XmlUtils.addContent(factory, el, SLIDER_END_PT, sliderEndPt, ns);
		XmlUtils.addContent(factory, el, SLIDER_INCREMENT, sliderIncrement, ns);
		XmlUtils.addContent(factory, el, SLIDER_DEFAULT_START, sliderDefaultStart, ns);
		XmlUtils.addContent(factory, el, SLIDER_DEFAULT_END, sliderDefaultEnd, ns);
		
		XmlUtils.addContent(factory, el, EXPAND_INTO_BOXES, expandIntoBoxes, ns);
		XmlUtils.addContent(factory, el, TREE_CONTROL, expandIntoTree, ns);
		XmlUtils.addContent(factory, el, USE_DRILL_PATH, useDrillPath, ns);
		XmlUtils.addContent(factory, el, MULTILINE, this.mutliline, ns);
		
		if (this.additionalOlapColumns != null && additionalOlapColumns.size() > 0) {
			OlapColumn.addContent(factory, el, additionalOlapColumns, ns);
		}
		
		parent.addChild(el);
	}
	
	public String getValidationMessage() {
		return validationMessage;
	}

	public List<Integer> getIncludedPromptIndices() {
		return includedPromptIndices;
	}

	public String getImagePath() {
		return imagePath;
	}

	public String getTargetDashboardName() {
		return targetDashboardName;
	}

	public static class ListValue {
		private String label;
		private String value;
		
		public String getLabel() {
			return label;
		}
		public void setLabel(String label) {
			this.label = label;
		}
		public String getValue() {
			return value;
		}
		public void setValue(String value) {
			this.value = value;
		}
		
		public void parseElement(OMElement parent) {
			for (@SuppressWarnings("unchecked")
			Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
				OMElement el = iter.next();
				String elName = el.getLocalName();
				if (LABEL.equals(elName)) {
					label = XmlUtils.getStringContent(el);
				}
				else if (VALUE.equals(elName)) {
					value = XmlUtils.getStringContent(el);
				}
				
			}
		}
		public void addContent(OMElement parent, OMFactory factory, OMNamespace ns) {
			OMElement el = factory.createOMElement(LIST_VALUE, ns);
			XmlUtils.addContent(factory, el, LABEL, label, ns);
			XmlUtils.addContent(factory, el, VALUE, value, ns);
			parent.addChild(el);
		}
	}

	public String getOperator() {
		return Operator;
	}

	public void setOperator(String operator) {
		Operator = operator;
	}

	/**
	 * @param promptClass the promptClass to set
	 */
	public void setClazz(String promptClass) {
		this.clazz = promptClass;
	}

	/**
	 * @return the promptClass
	 */
	public String getClazz() {
		return clazz;
	}

	public FilterType getFilterType() {
		return filterType;
	}
	
	public void setFilterType(FilterType ft) {
		this.filterType = ft;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (! (obj instanceof Prompt))
			return false;
		
		return ParameterName.equals(((Prompt)obj).getParameterName());
	}

	/**
	 * @return the columnName
	 */
	public String getColumnName() {
		return isExpression() ? ParameterName : ((columnName != null) ? columnName : ParameterName);
	}
	
	/**
	 * Does this prompt requires query values 
	 * @return
	 */
	public boolean isQueryBase(){
		return 
			(Type == Prompt.PromptType.Query) ||
			(Type == Prompt.PromptType.Slider && sliderEndPtsType.equals("Query-Based")) ||
			(Type == Prompt.PromptType.OlapHierarchy);
	}

	/**
	 * @return the isExpression
	 */
	public boolean isExpression() {
		return isExpression || (columnName == null);
	}
}
