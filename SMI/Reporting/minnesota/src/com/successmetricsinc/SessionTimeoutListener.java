/**
 * $Id: SessionTimeoutListener.java,v 1.24 2011-10-11 22:48:20 ricks Exp $
 *
 * Copyright (C) 2007-2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.successmetricsinc;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.apache.log4j.Logger;

import com.successmetricsinc.UI.ApplicationContext;

/**
 * Class to listen for session timeouts
 */
public class SessionTimeoutListener implements HttpSessionBindingListener
{
	private static Logger logger = Logger.getLogger(SessionTimeoutListener.class);
	private String loginName = null;
	private String httpSessionId = null;
	private long sessionStartTime;
	private ApplicationContext userServerContext = null; // will be null in the single tenant case
	private Session session;
	
	public SessionTimeoutListener(String name, String httpSessionId, ApplicationContext userServerContext, Session session)
	{
		loginName = name;
		this.httpSessionId = httpSessionId;
		this.userServerContext = userServerContext;
		this.sessionStartTime = System.currentTimeMillis();
		this.session = session;
	}

	public void valueBound(HttpSessionBindingEvent event)
	{
		if (logger.isTraceEnabled())
			logger.trace("Successful session start: " + loginName);
	}

	public void valueUnbound(HttpSessionBindingEvent event)
	{
		if (session != null)
			session.resetState();
		
		if (userServerContext != null) // null if running single tenant
			userServerContext.dispose();
		if (logger.isTraceEnabled())
			logger.trace("Successful session end: " + loginName + ", duration of the session: " + (System.currentTimeMillis() - sessionStartTime)/1000 + "s");
		userServerContext = null;
	}
	
	public String toString()
	{
		return loginName + ", " + httpSessionId;
	}
}
