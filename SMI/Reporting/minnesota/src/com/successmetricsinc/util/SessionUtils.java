/**
 * 
 */
package com.successmetricsinc.util;

import javax.servlet.http.HttpSession;

import com.successmetricsinc.SessionHelper;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.adhoc.AdhocReport;

/**
 * @author agarrison
 *
 */
public class SessionUtils {
    protected static final String ADHOC_REPORT = "adhocReport";
	
	/**
	 * Gets the user bean object for this logged in user
	 * @return	the user bean; null if there is no user bean because there is no session.
	 */
	public static SMIWebUserBean getUserBean() {
		SMIWebUserBean ub = (SMIWebUserBean)getSessionObject("userBean");
		if (ub == null && SessionHelper.getSession() != null) {
			ub = new SMIWebUserBean();
			setSessionObject("userBean", ub);
		}
		else if (ub == null) {
			HttpSession httpSession = SMIWebUserBean.getHttpSession();
			if (httpSession != null)
				ub = (SMIWebUserBean) httpSession.getAttribute("userBean");
		}
		return ub;
	}
	
	public static AdhocReport getReport() {
		AdhocReport a = (AdhocReport)getSessionObject(ADHOC_REPORT);
		if (a == null && SessionHelper.getSession() != null) {
			a = new AdhocReport();
			setSessionObject(ADHOC_REPORT, a);
		}
		return a;
	}
	
	public static void setReport(AdhocReport r) {
		setSessionObject(ADHOC_REPORT, r);
	}
	
	public static Object getSessionObject(String name) {
		HttpSession session = SessionHelper.getSession();
		if (session != null) {
			return session.getAttribute(name);
		}
		return null;
	}
	
	public static void setSessionObject(String name, Object o) {
		HttpSession session = SessionHelper.getSession();
		if (session != null) {
			session.setAttribute(name, o);
		}
	}

}
