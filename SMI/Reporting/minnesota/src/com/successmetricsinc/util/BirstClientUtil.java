package com.successmetricsinc.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMAttribute;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.util.AXIOMUtil;
import org.apache.axis2.AxisFault;
import org.apache.axis2.Constants;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.log4j.Logger;

import com.successmetricsinc.PackageSpaceProperties;
import com.successmetricsinc.ServerContext;
import com.successmetricsinc.util.Util;

public class BirstClientUtil {

	private static final Logger logger = Logger.getLogger(BirstClientUtil.class);
	public static final String BIRST_ADMIN_SERVICE_NAMESPACE = "http://www.birst.com/";
	public static final String BIRST_ADMIN_SERVICE_ENDPOINT = "/InterServerWebService.asmx";
	
	
	public static OMElement createPayLoad(String namespace, String operationName, String paramName, Object value) throws Exception
	{			
		LinkedHashMap<String,Object> map = new LinkedHashMap<String, Object>();
		map.put(paramName, value);		
		return createPayLoad(namespace, operationName, map);
	}
	
	public static OMElement createPayLoad(String namespace, String operationName, LinkedHashMap<String, Object> paramValuesMap)
	{
		
		if(paramValuesMap.size() != paramValuesMap.size()){
			return null;
		}
		
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace omNs = fac.createOMNamespace(namespace, "ns1");		
		OMElement method = fac.createOMElement(operationName, omNs);
		
		for (String key : paramValuesMap.keySet())
		{
			Object paramValue = paramValuesMap.get(key);		
			if (paramValue != null && paramValue instanceof ArrayList)
			{
				@SuppressWarnings("unchecked")
				ArrayList<String> list = (ArrayList<String>) paramValue;
				for (String val : list)
				{
					OMElement listElement = fac.createOMElement(key, omNs);
					listElement.setText(val.toString());
					method.addChild(listElement);
				}
			} 
			else if(paramValue != null && paramValue.toString().length() > 0)
			{				
				OMElement value = fac.createOMElement(key, omNs);
				value.setText(paramValue.toString());
				method.addChild(value);
			}
			else
			{
				OMElement nullElement = fac.createOMElement(key, omNs);
				OMNamespace xsi = fac.createOMNamespace("http://www.w3.org/2001/XMLSchema-instance", "xsi");
				OMAttribute nil = fac.createOMAttribute("nil", xsi, "true");
				nullElement.addAttribute(nil);
				method.addChild(nullElement);			
			}
		}		
		return method;
	}
	
	@SuppressWarnings("unchecked")
	public static List<PackageSpaceProperties> getPackageSpaceProps(String childSpaceDirectory) throws Exception
	{
		List<PackageSpaceProperties> list = new ArrayList<PackageSpaceProperties>();	
		ServiceClient acornServiceClient	= null;
		try
		{		
			acornServiceClient = getServiceClient(ServerContext.getBirstAdminUrl() + BIRST_ADMIN_SERVICE_ENDPOINT);
			acornServiceClient.getOptions().setAction(BIRST_ADMIN_SERVICE_NAMESPACE + "getPackagesDetails");
			acornServiceClient.getOptions().setProperty(HTTPConstants.CHUNKED, Constants.VALUE_FALSE);
			LinkedHashMap<String, Object> inputValuesMap = new LinkedHashMap<String, Object>();
			inputValuesMap.put("childSpaceDirectory", childSpaceDirectory);
			
			OMElement acornServicePayLoad = createPayLoad(BIRST_ADMIN_SERVICE_NAMESPACE, "getPackagesDetails", inputValuesMap);
			OMElement result = acornServiceClient.sendReceive(acornServicePayLoad);
			BirstWebServiceResponse response  = parseBirstWebServiceResponse(BIRST_ADMIN_SERVICE_NAMESPACE, "getPackagesDetailsResult", "PackageDetails", result);
	
			if(isSuccessful(response))
			{
				OMElement getPackageAndSpaceDetailsElement = response.resultNode;
				if(getPackageAndSpaceDetailsElement != null)
				{	
					Iterator<OMElement> pspaceArrayIterator = getPackageAndSpaceDetailsElement.getChildrenWithName(new QName("PackageSpace"));
					if(pspaceArrayIterator != null)
					{
						while(pspaceArrayIterator.hasNext())
						{
							OMElement packageSpaceElement = pspaceArrayIterator.next();
							PackageSpaceProperties packageSpaceProps = parseAndGetPackageSpaceProps(packageSpaceElement);
							if(packageSpaceProps != null ){
								if(packageSpaceProps.validateAllRequiredProperties()){
								list.add(packageSpaceProps);
								}
								else
								{
									logger.warn("Not all package space properties present : " + packageSpaceProps);
								}
							}
						}
					}

				}
			}
			else
			{
				logger.warn("Not a successful response returned from birst for getPackageDetails() for child space directory: " + childSpaceDirectory);
			}
		}
		finally
		{
			if(acornServiceClient != null)
			{
				try
				{
				acornServiceClient.cleanup();
				acornServiceClient.cleanupTransport();
				}catch(Exception ex){
					logger.warn("Error in cleaning up birstClient resources", ex);
				}
			}
		}
		return list;
	}	
	
	private static PackageSpaceProperties parseAndGetPackageSpaceProps(OMElement packageSpaceElement) {
		
		PackageSpaceProperties response = new PackageSpaceProperties();
		OMElement packageID = packageSpaceElement.getFirstChildWithName(new QName("PackageID"));
		if (packageID != null)
		{
			response.setPackageID(packageID.getText());					
		}
		OMElement packageName = packageSpaceElement.getFirstChildWithName(new QName("Name"));
		if (packageName != null)
		{
			response.setPackageName(packageName.getText());
		}
		OMElement parentSpaceID = packageSpaceElement.getFirstChildWithName(new QName("SpaceID"));

		if (parentSpaceID != null)
		{
			response.setSpaceID(parentSpaceID.getText());					
		}
		OMElement parentSpaceName = packageSpaceElement.getFirstChildWithName(new QName("SpaceName"));
		if (parentSpaceName != null)
		{
			response.setSpaceName(parentSpaceName.getText());
		}
		OMElement parentSpaceDirectory = packageSpaceElement.getFirstChildWithName(new QName("SpaceDirectory"));
		if (parentSpaceDirectory != null)
		{
			response.setSpaceDirectory(parentSpaceDirectory.getText());					
		}
		
		return response;
	}

	public static BirstWebServiceResponse parseBirstWebServiceResponse(String namespace, String rootResponseNode, String resultNodeName, OMElement responseElement)
	{		
		BirstWebServiceResponse response = new BirstWebServiceResponse();
		try
		{
			OMElement returnElement = responseElement.getFirstChildWithName(new QName(BIRST_ADMIN_SERVICE_NAMESPACE, rootResponseNode));	
			String resultString = returnElement.getText();
			if(resultString != null){
				OMElement rootElement = AXIOMUtil.stringToOM(resultString);
				if(rootElement != null)
				{			
					OMElement resultNodeElement = rootElement.getFirstChildWithName(new QName(resultNodeName));		
					response.resultNode = resultNodeElement;
					OMElement error = rootElement.getFirstChildWithName(new QName("Error"));
					if (error != null)
					{				
						OMElement errorCode = error.getFirstChildWithName(new QName("ErrorCode"));
						if (errorCode != null)
						{
							response.ErrorCode = Integer.parseInt(errorCode.getText());					
						}
						OMElement errorMessage = error.getFirstChildWithName(new QName("ErrorMessage"));
						if (errorMessage != null)
						{
							response.ErrorMessage = errorMessage.getText();
						}
					}		
				}
			}
		}catch(Exception ex){
			logger.error("Error in parsing birst web response", ex);
		}
		return response;
	}

	public static boolean isSuccessful(BirstWebServiceResponse response)
	{
		boolean success = false;
		if(response != null)
		{
			success = response.ErrorCode == 0 ? true : false;
		}
		return success;
	}	 	
	
	public static ServiceClient getServiceClient(String serviceEndpoint) throws AxisFault
	{
		ServiceClient serviceClient = new ServiceClient(null, null);
		configureConnectionParams(serviceClient);
		Options opts = new Options();
		opts.setTo(new EndpointReference(serviceEndpoint));		
		serviceClient.setOptions(opts);
		return serviceClient;
	}	
	
	public static void configureConnectionParams(ServiceClient serviceClient)
	{
		Util.configureConnectionParams(serviceClient.getServiceContext().getConfigurationContext());		
	}
}
