package com.successmetricsinc.util;

import org.apache.axiom.om.OMElement;

public class BirstWebServiceResponse {
	
	public OMElement resultNode;
	// 0 is used for success. Should not be a default value
	public int ErrorCode = -1;
	public String ErrorMessage;

}
