/**
 * 
 */
package com.successmetricsinc.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.axiom.om.OMElement;
import org.apache.log4j.Logger;

import com.successmetricsinc.queueing.Message;
import com.successmetricsinc.queuing.AsynchReportGeneration.AsynchReportMessage;


/**
 * @author agarrison
 *
 */
public class MessageFactory {

	private static final Logger logger = Logger.getLogger(MessageFactory.class);		
	
	public static AsynchReportMessage createMessage(String path, OMElement data) throws IOException {
		AsynchReportMessage m = new AsynchReportMessage();
		File tempDir = new File(path);
		if (! tempDir.exists())
			tempDir.mkdir();
		File dataFile = File.createTempFile("AsynchReportData", ".xml", tempDir);
		dataFile.delete();
		m.setMessageDataFileName(dataFile.getAbsolutePath());
		FileOutputStream fos = null;		
		try {
			if (! dataFile.exists()) {
				dataFile.createNewFile();
			}
			fos = new FileOutputStream(dataFile);
			XMLStreamWriter writer = XMLOutputFactory.newInstance().createXMLStreamWriter(fos, "UTF-8");
			data.serialize(writer);
			writer.flush();
		} catch (IOException e) {
			logger.error("Error saving adhoc report: (with attempted file name: " + dataFile.toString() +")", e);
			throw (e);
		} catch (XMLStreamException e) {
			logger.error(e, e);
		} catch (FactoryConfigurationError e) {
			logger.error(e, e);
		}
		finally {
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
				}
			}
		}
		return m;
	}
}
