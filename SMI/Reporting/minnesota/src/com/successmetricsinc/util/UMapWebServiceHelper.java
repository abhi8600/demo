package com.successmetricsinc.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.jdom.Element;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.successmetricsinc.Repository;
import com.successmetricsinc.ServerContext;
import com.successmetricsinc.UserBean;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.query.MongoMgr;

import de.micromata.opengis.kml.v_2_2_0.Boundary;
import de.micromata.opengis.kml.v_2_2_0.Coordinate;
import de.micromata.opengis.kml.v_2_2_0.Document;
import de.micromata.opengis.kml.v_2_2_0.Feature;
import de.micromata.opengis.kml.v_2_2_0.Folder;
import de.micromata.opengis.kml.v_2_2_0.Geometry;
import de.micromata.opengis.kml.v_2_2_0.Kml;
import de.micromata.opengis.kml.v_2_2_0.LinearRing;
import de.micromata.opengis.kml.v_2_2_0.MultiGeometry;
import de.micromata.opengis.kml.v_2_2_0.Placemark;
import de.micromata.opengis.kml.v_2_2_0.Point;
import de.micromata.opengis.kml.v_2_2_0.Polygon;

public class UMapWebServiceHelper {
	private static final String DBNAME = "BirstKmls";
	private static Logger logger = Logger.getLogger(UMapWebServiceHelper.class);
	
	public static Element getPolygons(String kmlName, List regionKeys){
		UserBean ub = SessionUtils.getUserBean();
		if(ub == null){
			ub = UserBean.getParentThreadUserBean();
		}
		if(!kmlName.toLowerCase().endsWith(".kml")){
			kmlName += ".kml";
		}
		
		File kmlFile = new File(getAcornRootDir(ServerContext.getAcornRoot()) + File.separator + kmlName);
		String globalFileName = kmlFile.getAbsolutePath();
		String spaceFileName = "";
		if(!kmlFile.exists()){
			kmlFile = new File(getKmlDir() + File.separator + kmlName);
			spaceFileName = kmlFile.getAbsolutePath();
		}
		
		if(kmlFile.exists()){
			Element response = null;
			try{
				response = findPlacemarkFromDB(ub, kmlName, kmlFile, regionKeys);
			}catch(Exception ex){
				logger.error("Cannot find placemarks from MongoDB", ex);
				
				//Try to read from file
				Kml kml = Kml.unmarshal(kmlFile);
				if(kml != null){
					//Build the list of placemarks to search for
					response = parseFileToResponse(kml, regionKeys);
				}else{
					logger.error("", new RuntimeException("Cannot parse kml file: " + kmlFile.getAbsolutePath()));
					return createErrorXml(-201, kmlFile.getName());
				}
			}
			return response;
		}else{
			logger.error("", new RuntimeException("File not found at " + globalFileName + " or " + spaceFileName));
			return createErrorXml(-200, kmlFile.getName());
		}
	}
	
	private static Element createErrorXml(Integer code, String message){
		Element root = new Element("placemarks");
		Element error = new Element("error");
		error.setAttribute("code", code.toString());
		error.setText(message);
		root.addContent(error);
		return root;
	}
	
	@SuppressWarnings("unchecked")
	public static Element findPlacemarkFromDB(UserBean ub, String kmlName,File kmlFile, List<String> regionKeys) throws UnknownHostException{
		Element response = new Element("placemarks");
		
		BasicDBList keys = new BasicDBList();
		for(int i = 0; i < regionKeys.size(); i++){
			keys.add(regionKeys.get(i));
		}
		
		if(keys.size() > 0){
			DB db = getDB();
			DBCollection coll = getPageCollection(db, ub);
			db.requestStart();
			
			//Check to see if the file was parsed and cached before
			BasicDBObject checkQuery = new BasicDBObject("file", kmlName);
			BasicDBObject one = (BasicDBObject)coll.findOne(checkQuery);
			if(one == null){
				String kmlData = fixupEntity(readFile(kmlFile));
				Kml kml = Kml.unmarshal(kmlData);
				writeKmlToMongoDB(kmlName, kml, ub);
			}
			
			BasicDBObject query = new BasicDBObject("file", kmlName);
			query.append("name", new BasicDBObject("$in", keys));
			
			DBCursor cursor = coll.find(query);
			Iterator<DBObject> dbItr = cursor.iterator();
			while(dbItr.hasNext()){
				BasicDBObject o = (BasicDBObject)dbItr.next();
				BasicDBObject geom = (BasicDBObject)o.get("geometry");
				String type = geom.getString("type");
				String key = o.getString("name");
				
				Element polyEl = new Element("pm");
				polyEl.setAttribute("id", key);
				polyEl.setAttribute("type", type);
				if(type.equals("multipolygons")){
					BasicDBList polygons = (BasicDBList)geom.get("polygons");
					if(polygons != null){
						for(int i = 0; i < polygons.size(); i++){
							Element coords = convertToCoordsElements((BasicDBList)polygons.get(i));
							polyEl.addContent(coords);
						}
					}
				}else if(type.equals("polygon")){
					BasicDBList polygon = (BasicDBList)geom.get("coords");
					Element coords = convertToCoordsElements((BasicDBList)polygon);
					polyEl.addContent(coords);
				}
				
				response.addContent(polyEl);
			}
			db.requestDone();
		}
		return response;
	}
	
	public static String fixupEntity(String data){
		//We do some fixup here. Our kml parser only likes <kml xmlns="http://www.opengis.net/kml/2.2">
		int idx = data.indexOf("<kml");
		if(idx >= 0){
			char end = data.charAt(idx + "<kml".length());
			if(end == '>'){
				data = data.replaceFirst("<kml>", "<kml xmlns=\"http://www.opengis.net/kml/2.2\">");
			}else{
				//KML downloaded from Google Maps
				data = data.replaceFirst("http://earth.google.com/kml/2.2", "http://www.opengis.net/kml/2.2");
			}
		}
		return data;
	}
	
	private static String readFile(File kmlFile){
		StringBuffer sb = new StringBuffer();
		FileInputStream fis = null;
		InputStreamReader is = null;
		BufferedReader br = null;
		
		try {
			fis = new FileInputStream(kmlFile);
			is = new InputStreamReader(fis, "UTF-8");
			br = new BufferedReader(is);
			String nextLine = "";
		
			while ((nextLine = br.readLine()) != null) {
				sb.append(nextLine);
			}
		} catch (FileNotFoundException e) {
			logger.error("Kml file not found: " + e.getMessage());
		} catch (IOException e) {
			logger.error("Kml file io exception: " + e.getMessage());
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch(Exception e){}
			}
			if (is != null) {
				try {
					is.close();
				} catch(Exception e){}
			}
			if (fis != null) {
				try {
					fis.close();
				} catch(Exception e){}
			}
		}
		
		return sb.toString();
	}
	
	@SuppressWarnings("unchecked")
	private static Element parseFileToResponse(Kml kml, List columnKeys){
		HashMap<String, List<Polygon>> placemarkPolys = parseGetPolygons(kml);

		Element response = new Element("placemarks");
		Iterator itr = columnKeys.iterator();
		while(itr.hasNext()){
			String key = (String)itr.next();
			List<Polygon> polys = placemarkPolys.get(key);
			//Match
			if(polys != null){
				Element polyEl = new Element("pm");
				polyEl.setAttribute("id", key);

				for(int i = 0; i < polys.size(); i++){
					Polygon p = polys.get(i);
					Boundary bound = p.getOuterBoundaryIs();
					if(bound != null){
						LinearRing ring = bound.getLinearRing();
						if(ring != null){
							Element coordsEl = new Element("coords");
							StringBuffer coordsBuf = new StringBuffer();
							List<Coordinate> coords = ring.getCoordinates();
							for(int y = 0; y < coords.size(); y++){
								Coordinate cord = coords.get(y);
								coordsBuf.append(cord.getLatitude()).append(",").append(cord.getLongitude());
								if(y < coords.size() - 1){
									coordsBuf.append(" ");
								}
							}
							coordsEl.setText(coordsBuf.toString());
							polyEl.addContent(coordsEl);
						}
					}
				}
				response.addContent(polyEl);
			}
		}
		return response;
	}
	
	private static HashMap<String, List<Polygon>> parseGetPolygons(Kml kml){
		Document doc = (Document)kml.getFeature();
		List<Feature> features = doc.getFeature();
		
		HashMap<String, List<Polygon>> placemarkPolys = new HashMap<String, List<Polygon>>();
		
		for(int i = 0; i < features.size(); i++){
			Feature f = features.get(i);
			if(f instanceof Placemark ){
				//Find its geometry
				Placemark p = (Placemark)f;
				ArrayList<Polygon> polygons = new ArrayList<Polygon>();
				recurseAddPolygons(p.getGeometry(), polygons);
				if(polygons.size() > 0){
					placemarkPolys.put(p.getName(), polygons);
				}
			}else if (f instanceof Folder){
				Folder folder = (Folder)f;
				List<Feature>folderFeatures = folder.getFeature();
				for(int y = 0; y < folderFeatures.size(); y++){
					Feature featureFolder = folderFeatures.get(y);
					if(featureFolder instanceof Placemark){
						Placemark p = (Placemark)featureFolder;
						ArrayList<Polygon> polygons = new ArrayList<Polygon>();
						recurseAddPolygons(p.getGeometry(), polygons);
						if(polygons.size() > 0){
							placemarkPolys.put(p.getName(), polygons);
						}
					}
				}
			}
		}
		
		return placemarkPolys;
	}
	
	private static void recurseAddPolygons(Geometry geometry, List<Polygon> polys){
		if(geometry instanceof MultiGeometry){
			MultiGeometry multiGeom = (MultiGeometry) geometry;
			List<Geometry> geoms = multiGeom.getGeometry();
			for(int i = 0; i < geoms.size(); i++){
				Geometry geo = geoms.get(i);
				recurseAddPolygons(geo, polys);
			}
		}else if (geometry instanceof Polygon){
			polys.add((Polygon)geometry);
		}
		
	}
	
	private static String getKmlDir(){
		SMIWebUserBean ub = SessionUtils.getUserBean();
		String catalogPath;
		if(ub == null){
			UserBean userBean = UserBean.getParentThreadUserBean();
			Repository rep = userBean.getRepository();
            catalogPath = rep.getServerParameters().getApplicationPath() + "\\catalog";
		}else{
			catalogPath = ub.getCatalogPath();
		}
		return catalogPath + File.separator + "Kml";
	}
	
	private static String getAcornRootDir(String acornRoot){
		String rootDir = acornRoot;
		if(rootDir == null){
			rootDir = "C:/SMI/Acorn/";
		}
		return rootDir + "/config/Kml";
	}
	
	/**
	 * Write / cache the parsed kml structure into MongoDB 
	 * @param kmlName
	 * @param kml
	 * @param ub
	 * @throws UnknownHostException
	 */
	private static void writeKmlToMongoDB(String kmlName, Kml kml, UserBean ub) throws UnknownHostException{
		DB db = getDB();
		DBCollection coll = getPageCollection(db, ub);
		db.requestStart();
		
		//Remove previous entries
		coll.remove(new BasicDBObject("file", kmlName));
		
		ArrayList<BasicDBObject> placemarkObjs = new ArrayList<BasicDBObject>();
		
		Document doc = (Document)kml.getFeature();
		List<Feature> features = doc.getFeature();
		for(int i = 0; i < features.size(); i++){
			Feature f = features.get(i);
			if(f instanceof Placemark ){
				parsePlacemark((Placemark)f, kmlName, placemarkObjs);
			}else if (f instanceof Folder){
				Folder folder = (Folder)f;
				List<Feature>folderFeatures = folder.getFeature();
				for(int y = 0; y < folderFeatures.size(); y++){
					Feature featureFolder = folderFeatures.get(y);
					if(featureFolder instanceof Placemark){
						parsePlacemark((Placemark) featureFolder, kmlName, placemarkObjs);
					}
				}
			}
		}
		
		coll.ensureIndex("name");
		for(int i = 0; i < placemarkObjs.size(); i++){
			BasicDBObject o = placemarkObjs.get(i);
			coll.insert(o);
		}
		db.requestDone();
	}
	
	/**
	 * 
	 * @param geometry
	 * @param dblist
	 */
	private static void recurseAddPolygons(Geometry geometry, BasicDBList dblist){
		if(geometry instanceof MultiGeometry){
			MultiGeometry multiGeom = (MultiGeometry) geometry;
			List<Geometry> geoms = multiGeom.getGeometry();
			for(int i = 0; i < geoms.size(); i++){
				Geometry geo = geoms.get(i);
				recurseAddPolygons(geo, dblist);
			}
		}else if (geometry instanceof Polygon){
			BasicDBList l = createPolygonDBList(geometry);
			dblist.add(l);
		}
	}
	
	/** 
	 * Create a mongodb basicdblist from the polygon object, eg-
	 * [[-123.23, 33.5], [-123.11, 33.0], ...]]
	 * 
	 * @param geom
	 */
	private static BasicDBList createPolygonDBList(Geometry geom){
		Polygon p = (Polygon)geom;
		
		BasicDBList polyList = new BasicDBList();
		Boundary bound = p.getOuterBoundaryIs();
		if(bound != null){
			LinearRing ring = bound.getLinearRing();
			if(ring != null){
				List<Coordinate> coords = ring.getCoordinates();
				for(int y = 0; y < coords.size(); y++){
					Coordinate cord = coords.get(y);
					BasicDBList point = new BasicDBList();
					point.add(cord.getLatitude());
					point.add(cord.getLongitude());
					polyList.add(point);
				}
			}
		}
		return polyList;
	}
	
	private static Element convertToCoordsElements(BasicDBList coordsList){
		Element coordsEl = new Element("coords");
		StringBuffer coordsBuf = new StringBuffer();
		for(int y = 0; y < coordsList.size(); y++){
			BasicDBList cord =(BasicDBList)coordsList.get(y);
			double latitude = (Double)cord.get(0);
			double longitude = (Double)cord.get(1);
			coordsBuf.append(latitude).append(",").append(longitude);
			if(y < coordsList.size() - 1){
				coordsBuf.append(" ");
			}
		}
		coordsEl.setText(coordsBuf.toString());
		return coordsEl;
	}
	
	private static void parsePlacemark(Placemark p, String kmlName, ArrayList<BasicDBObject> placemarkObjs){
		BasicDBObject pObj = new BasicDBObject("name", p.getName());
		pObj.append("file", kmlName);
		placemarkObjs.add(pObj);
		
		Geometry geometry = p.getGeometry();
		if(geometry instanceof MultiGeometry){
			BasicDBList list = new BasicDBList();
			MultiGeometry multiGeom = (MultiGeometry) geometry;
			List<Geometry> geoms = multiGeom.getGeometry();
			for(int y = 0; y < geoms.size(); y++){
				Geometry geo = geoms.get(y);
				recurseAddPolygons(geo, list);
			}
			BasicDBObject multiObj = new BasicDBObject("type", "multipolygons");
			multiObj.append("polygons", list);
			pObj.append("geometry", multiObj);
		}else if (geometry instanceof Polygon){
			BasicDBObject polyObj = new BasicDBObject("type", "polygon");
			polyObj.append("coords", createPolygonDBList(geometry));
			pObj.append("geometry", polyObj);
		}else if (geometry instanceof Point){
			Point pt = (Point)geometry;
			List<Coordinate>coords = pt.getCoordinates();
			
			BasicDBObject ptObj = new BasicDBObject("type", "point");
			BasicDBList ptList = new BasicDBList();
			
			Coordinate c = coords.get(0);
			ptList.add(c.getLatitude());
			ptList.add(c.getLongitude());
			
			ptObj.append("coords", ptList);
			pObj.append("geometry", ptObj);
		}
	}
	
	private static DBCollection getPageCollection(DB db, UserBean ub){
		return db.getCollection(ub.getRepository().getApplicationIdentifier() + ".kml");
	}
	
	private static DB getDB() throws UnknownHostException{
		return MongoMgr.getMongo().getDB(DBNAME);
	}
	
}
