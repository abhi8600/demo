/**
 * $Id: ServerContext.java,v 1.181 2012-10-12 08:49:38 BIRST\cpatel Exp $
 *
 * Copyright (C) 2007-2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.successmetricsinc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;

import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.adhoc.AdhocReport;
import com.successmetricsinc.adhoc.AdhocReportHelper;
import com.successmetricsinc.catalog.CatalogManager;
import com.successmetricsinc.chart.ChartOptions;
import com.successmetricsinc.chart.ChartServerContext;
import com.successmetricsinc.query.MongoMgr;
import com.successmetricsinc.query.QueryResultSet;
import com.successmetricsinc.queueing.producer.QueuePublishManager;
import com.successmetricsinc.queueing.qClient.BrokerCredentials;
import com.successmetricsinc.queueing.qClient.Consumer;
import com.successmetricsinc.queueing.qClient.MessengerFactory;
import com.successmetricsinc.queueing.qClient.MessengerFactory.Queue;
import com.successmetricsinc.queueing.qClient.MessengerFactory.Topic;
import com.successmetricsinc.queueing.qClient.QClientConnectionFactory;
import com.successmetricsinc.queueing.qClient.QueueConnectionManager;
import com.successmetricsinc.queueing.qClient.Exceptions.MessengerException;
import com.successmetricsinc.queuing.AsynchReportGeneration.AsynchReportResultSubscriber;
import com.successmetricsinc.util.Util;

/**
 * Singleton class to maintain server context
 * 
 * @author bpeters
 * 
 */
public class ServerContext
{
	private static final String SPECIAL_RELEASE = "SpecialRelease";
	private static final String ENABLE_ASYNC_REPORTING = "EnableAsyncReporting";

	private static Logger logger = Logger.getLogger(ServerContext.class);
	
	private static boolean seedQueryInMultitenant = false;
	
	
	private static String seedDirName = "seed";
	private static String seedQuerySpaceIDFilePath;
	
	// server-specific
	private static final String SMI_INSTALL_DIR_TAG = "V{SMI_INSTALL_DIR}";
	private static final String TOMCAT_INSTALL_DIR_TAG = "V{TOMCAT_INSTALL_DIR}";
	private static final String CHART_SERVER_USE_STANDALONE = "UseStandaloneChartServer";
	private static final String CHART_SERVER_NAME_KEY = "ChartServerHostName";
	private static final String CHART_SERVER_PORT_KEY = "ChartServerPortNumber";
	private static final String CHART_SERVER_TIMEOUT_KEY = "ChartServerTimeOut";
	private static final String CHART_SERVER_SWF_FILEPATH = "ChartServerSwfFilepath";

	private static final String ACORN_ROOT = "AcornRoot";
	private static final String CHART_ANIMATION = "EnableChartAnimation";
	private static final String KEY_BIRST_ADMIN_URL = "BirstAdminURL";
	private static final String LOGIN_SERVICE_IPS = "SMIWebLogin.AllowedIPs";
	private static List<String> loginServiceAllowedIPs;
	private static String tomcatInstallDir;
	private static String smiInstallDir;
	private static Double defaultScaleFactor;
	private static Properties overrides = null;
	private static String customerPropertyFileName = Util.CUSTOMER_PROPERTIES_FILENAME;

	private static String applicationRoot;	// root directory for the application

	private static String acornPreferencesEndpoint = "https://app.birst.com/PreferencesService.asmx";
	
	public static boolean useStandaloneChartServer;
	public static String chartServerHostName;
	public static int chartServerPortNumber;
	public static int chartServerTimeOut;
	public static String chartServerSwfFilepath;

	private static String customerPropertiesFile;
	private static boolean useChartAnimation = true;
	
	private static boolean processExpressionsSerially = false;
	
	private static String DB_WAIT_TIMEOUT_KEY = "DBConnectionWaitTimeout";
	private static final int DB_WAIT_TIMEOUT_DEFAULT = 3600;
	private static int dbWaitTimeout;
	
	private static String acornRoot;
	private static String birstAdminUrl;
	private static final String BIRST_ADMIN_URL_DEFAULT = "http://localhost:6105";
	
	public static final String USENEWCONNECTORFRAMEWORK = "UseNewConnectorFramework";
	private static String CONNECTOR_CONTROLLER_URI = "ConnectorControllerURI";
	private static String connectorControllerURI = null;
	
	private static Map<Topic,List<Consumer>> subscribedConsumers = new HashMap<Topic, List<Consumer>>();
	private static Set<Queue> initializedQueues = new HashSet<Queue>();
	private static Set<Topic> initializedTopics = new HashSet<Topic>();
	private static final String QUEUE_PUBLISHER = "QueuePublisher";
	private static final String QUEUE_BROKER_HOST = "BrokerHost";
	private static final String QUEUE_BROKER_PORT = "BrokerPort";
	private static final String asyncReportSysPropertiesFileName = "asyncreport.system.properties";
	private static Map<String, String> asyncReportSysProperties = new HashMap<>();
	
	private static String spacialRelease = null;
	private static HealthChecker healthChecker;
	private static QueuePublishManager queuePublishManager;
	
	private ServerContext()
	{
	}

	public synchronized static void initContext(ServletContext context) throws Exception
	{
		tomcatInstallDir = System.getProperty("catalina.home");
		smiInstallDir = System.getProperty("smi.home");
		
		// get the override file name and properties
		customerPropertiesFile = context.getInitParameter("CustomerProperties");
		customerPropertiesFile = replacePathVariables(customerPropertiesFile);
		if (customerPropertiesFile == null || customerPropertiesFile.isEmpty())
			customerPropertiesFile = customerPropertyFileName;
		overrides = Util.getProperties(customerPropertiesFile, true);	

		applicationRoot = getParameter("ApplicationRoot", context); // only used in seed queries
		applicationRoot = replacePathVariables(applicationRoot);

		String isSeedQuery = getParameter("SeedQueryInMultitenant", context);
		if (isSeedQuery != null && isSeedQuery.equalsIgnoreCase("true"))
		{
			seedQueryInMultitenant = true;
			String temp = getParameter("SeedDirName", context);
			{
				if (temp != null && !temp.isEmpty())
				{
					seedDirName = temp;
				}
			}
			seedQuerySpaceIDFilePath = getParameter("SeedQuerySpaceIDFilePath", context);
			seedQuerySpaceIDFilePath = replacePathVariables(seedQuerySpaceIDFilePath);
		}
		
		/** Get Google Analytics API Key from Context **/
		String googleAnalyticsAPIKey = getParameter("GoogleAnalyticsAPIKey", context);
		Repository.googleAnalyticsAPIKey = googleAnalyticsAPIKey;

		// get the server-specific settings
		Double defaultScreenWidth = null;
		Double defaultScreenHeight = null;
		String defaultScreenWidthStr = getParameter("DefaultScreenWidth", context);
		String defaultScreenHeightStr = getParameter("DefaultScreenHeight", context);
		if (defaultScreenWidthStr == null || defaultScreenHeightStr == null)
		{
			logger.info("DefaultScreenWidth and DefaultScreenHeight parameters not provided. Assuming resolution 1024 X 768.");
		} else
		{
			try
			{
				defaultScreenWidth = Double.parseDouble(defaultScreenWidthStr);
			} catch (NumberFormatException nfe)
			{
				logger.warn("Invalid value of parameter DefaultScreenWidth provided:" + defaultScreenWidthStr);
			}
			try
			{
				defaultScreenHeight = Double.parseDouble(defaultScreenHeightStr);
			} catch (NumberFormatException nfe)
			{
				logger.warn("Invalid value of parameter DefaultScreenHeight provided: " + defaultScreenHeightStr);
			}
		}
		defaultScaleFactor = calculateScaleFactor(defaultScreenWidth, defaultScreenHeight);

		String enableAnimationStr = getParameter(CHART_ANIMATION, context);
		if(enableAnimationStr != null){
			boolean enableAnimation;
			try{
				enableAnimation = Boolean.parseBoolean(enableAnimationStr);
				ServerContext.useChartAnimation = enableAnimation;
			}catch(Exception e){}
		}
		logger.info("Chart Animation enabled: " + ServerContext.useChartAnimation);

		String processExpressionsSeriallyStr = getParameter("ProcessExpressionsSerially", context);
		if (processExpressionsSeriallyStr != null) {
			boolean processSerially; 
			try {
				processSerially = Boolean.parseBoolean(processExpressionsSeriallyStr);
				ServerContext.processExpressionsSerially = processSerially;
			}
			catch (Exception e) {}
		}
		logger.info("Expressions will be processed " + (isProcessExpressionsSerially() ? "serially" : "in parallel"));
		QueryResultSet.setProcessSerially(ServerContext.processExpressionsSerially);
		
		//MongoDB parameters
		initializeMongoDBSettings(context);
				
		// Chart Server parameters
		final String use = getParameter(CHART_SERVER_USE_STANDALONE, context);
		useStandaloneChartServer = (use != null && use.equalsIgnoreCase("true")) ? true : false;
		chartServerHostName = getParameter(CHART_SERVER_NAME_KEY, context);
		final String port = getParameter(CHART_SERVER_PORT_KEY, context);
		if (port != null && !port.isEmpty())
			chartServerPortNumber = Integer.valueOf(port);
		else
			logger.error("Chart server port number (" + CHART_SERVER_PORT_KEY + ") not set");
		final String timeout = getParameter(CHART_SERVER_TIMEOUT_KEY, context);
		if (timeout != null && !timeout.isEmpty())
			chartServerTimeOut = Integer.valueOf(timeout);
		else
			logger.error("Chart server timeout (" + CHART_SERVER_TIMEOUT_KEY + ") not set");
		final String swfFilepath = getParameter(CHART_SERVER_SWF_FILEPATH, context);
		if (swfFilepath != null && !swfFilepath.isEmpty())
		{
			final File swfFile = new File(swfFilepath);
			if (swfFile.exists()) {
				chartServerSwfFilepath = swfFilepath;
			} else {
				logger.error("ChartRenderer.swf file path (" + CHART_SERVER_SWF_FILEPATH + "): " + swfFilepath + " does not exist. AnyChart PDF chart rendering will not work");
			}
		}
		else
		{
			logger.error("ChartRenderer.swf file path (" + CHART_SERVER_SWF_FILEPATH + ") has not been set.  AnyChart PDF chart rendering will not work");
		}
		// Add values back to PerformanceEngine bridge class - ChartServerContext
		ChartServerContext.useStandaloneChartServer = useStandaloneChartServer;
		ChartServerContext.chartServerHostName = chartServerHostName;
		ChartServerContext.chartServerPortNumber = chartServerPortNumber;
		ChartServerContext.chartServerTimeOut = chartServerTimeOut;
		ChartServerContext.chartServerSwfFilepath = chartServerSwfFilepath;
		
		//acornRoot (eg - C:\smi\Acorn ) required for listing of kml files, etc
		acornRoot = getParameter(ACORN_ROOT, context);

		// allowedIPs for SMIWebLogin Service. 
		String ips = getParameter(LOGIN_SERVICE_IPS, context);
		setLoginServiceAllowedIPs(ips);
		
		final String allowDynamicConnections = getParameter(Repository.ALLOW_DYNAMIC_DATABASE_CONNECTIONS, context);
		if (allowDynamicConnections != null && !allowDynamicConnections.isEmpty())
			System.setProperty(Repository.ALLOW_DYNAMIC_DATABASE_CONNECTIONS, String.valueOf(Boolean.parseBoolean(allowDynamicConnections)));			
		
		dbWaitTimeout = DB_WAIT_TIMEOUT_DEFAULT;
		final String dbWaitTimeoutString = getParameter(DB_WAIT_TIMEOUT_KEY, context);		
		if(dbWaitTimeoutString != null && dbWaitTimeoutString.trim().length() > 0)
		{
			try
			{
				int timeoutValue = Integer.parseInt(dbWaitTimeoutString.trim());
				if(timeoutValue > 0)
				{
					dbWaitTimeout = timeoutValue;
				}
			}
			catch(Exception ex)
			{
					logger.warn("Using default value. Error while retreiving db wait timeout " + ex.getMessage());
			}
		}
		
		Util.setDBWaitTimeout(dbWaitTimeout);
		birstAdminUrl = getParameter(KEY_BIRST_ADMIN_URL, context);
		if(birstAdminUrl == null){
			logger.debug("Cannot find BirstAdminURL property. Using Default Value : " + BIRST_ADMIN_URL_DEFAULT);
			birstAdminUrl = BIRST_ADMIN_URL_DEFAULT;
		}
		
		boolean isUseNewConnectorFramework = Boolean.parseBoolean(getParameter (USENEWCONNECTORFRAMEWORK, context));
		Repository.setUseNewConnectorFramework(isUseNewConnectorFramework);
		if (isUseNewConnectorFramework)
		{
			connectorControllerURI = getParameter(CONNECTOR_CONTROLLER_URI, context);
			if (connectorControllerURI == null)
			{
				logger.error("ConnectorControllerURI not found, connectors may not work as expected");
			}
		}
		boolean isAsyncReportingEnabled = Boolean.parseBoolean(getParameter(ServerContext.ENABLE_ASYNC_REPORTING, context));
		AdhocReportHelper.setAsynchReportGenerationEnabled(isAsyncReportingEnabled);
		startupAsyncProcessing(context);
	}
	
	public static void startupAsyncProcessing(ServletContext context) {		
		if (AdhocReportHelper.isAsynchReportGenerationEnabled()) {
			queuePublishManager = new QueuePublishManager();
			initialQueueConsumersAndPublishers(context);
			initializeAsyncReportSysProperties(context);
			startQueueHealthCheck();
		}
		
		spacialRelease = getParameter(ServerContext.SPECIAL_RELEASE, context);
	}
	
	private static void startQueueHealthCheck(){
		try{
			if(healthChecker == null){
				healthChecker = new HealthChecker();
			}
			healthChecker.init();
			healthChecker.run();
		}catch(Exception ex){
			logger.fatal("Unable to start health check for queue");
		}
	}
	
	public static String getSpecialRelease()
	{
		return spacialRelease;
	}
	
	private static void initializeAsyncReportSysProperties(ServletContext context)
	{
		InputStream fis = null;
		try {	
			String filePath = context.getRealPath("/WEB-INF/" + asyncReportSysPropertiesFileName);
			if (filePath != null)
			{
				fis = new FileInputStream(filePath);
				if (fis != null)
				{
					BufferedReader br = new BufferedReader(new InputStreamReader(fis));
					String name = null;
					while ((name = br.readLine()) != null) {
						if (name.startsWith("#"))
							continue;
						String value = System.getProperty(name);
						if (value != null)
						{
							asyncReportSysProperties.put(name, value);
						}
					}
				}
			}
		}
		catch (Exception e) {
			logger.error(e, e);
		}
		finally {
			try {
				if (fis != null) 
					fis.close();
			}
			catch (Exception e) {}
		}
	}
	
	public static Map<String, String> getAsyncReportSysPropertiesMap()
	{
		return asyncReportSysProperties;
	}
	
	private static void initialQueueConsumersAndPublishers(ServletContext context){
		try{
			String brokerHost = getParameter(QUEUE_BROKER_HOST, context);		
			String brokerPortStr = getParameter(QUEUE_BROKER_PORT, context);
			if(brokerHost != null && brokerPortStr != null){
				int brokerPort = Integer.parseInt(brokerPortStr);
				BrokerCredentials brokerCredentials = new BrokerCredentials(brokerHost, brokerPort);
				QClientConnectionFactory.setUpDefaultCredentials(brokerCredentials);
			}

			// result topic
			queuePublishManager.configurePublishersForTopic(Topic.NOW_REPORTGEN_RESULT, 1);			
			initializedTopics.add(Topic.NOW_REPORTGEN_RESULT);

			// queue publishers
			QueuePublishManager qpm = new QueuePublishManager();
			String queuePublisher = getParameter(QUEUE_PUBLISHER, context);
			if (queuePublisher != null && !queuePublisher.isEmpty())
			{
				String[] publishConfig = queuePublisher.split(",");
				List<Queue> queues = Arrays.asList(Queue.values());
				for (String keyVal : publishConfig)
				{
					String[] cconfig = keyVal.split(":");
					if (cconfig.length == 2)
					{
						Queue queue = Queue.valueOf(cconfig[0]);
						int numThreads = Integer.parseInt(cconfig[1]);
						if (queues.contains(queue))
						{
							queuePublishManager.configurePublishersForQueue(queue, numThreads);							
							initializedQueues.add(queue);
						}
					}
				}
			}
			
			initializeSubscribers(Topic.NOW_REPORTGEN_RESULT);			
			startInitializedPublishersAndSubscribers();			
		}catch(Exception ex){
			logger.fatal("Unable to initialize consumers/publishers " , ex);
		}
	}
	
	private static void initializeSubscribers(Topic topic){
		subscribedConsumers.put(topic, new LinkedList<Consumer>());
	}
	
	public static void addReportSubscribers() throws MessengerException{
		Consumer consumer = MessengerFactory.getConsumer(Topic.NOW_REPORTGEN_RESULT);
		consumer.subscribeToTopic(AsynchReportResultSubscriber.getInstance());		
		addSubscriberConsumer(Topic.NOW_REPORTGEN_RESULT, consumer);
	}
	
	private static void addSubscriberConsumer(Topic topic, Consumer consumer){
		if(!subscribedConsumers.containsKey(topic)){
			subscribedConsumers.put(topic, new LinkedList<Consumer>());
		}
		subscribedConsumers.get(topic).add(consumer);
	}
	
	public static String getConnectorControllerURI() {
		return connectorControllerURI;
	}

	/**
	 * Base AcornRoot directory - 
	 * @return
	 */
	public static String getAcornRoot(){
		return acornRoot;
	}
	
	/**
	 * Set up MongoDB parameters
	 * @param context
	 */
	private static void initializeMongoDBSettings(ServletContext context){
		//Set the system properties required for MongoDB
		setSystemPropertyValue(MongoMgr.PR_HOSTNAME, context, MongoMgr.DEFAULT_HOST);
		setSystemPropertyValue(MongoMgr.PR_PORT, context, Integer.toString(MongoMgr.DEFAULT_PORT));
		setSystemPropertyValue(MongoMgr.PR_POOL_SZ, context, Integer.toString(MongoMgr.DEFAULT_POOLSIZE));
		
		//Create the shared query file cache root directory
		String sharedCacheDir = setSystemPropertyValue(MongoMgr.PR_FS_ROOTDIR, context, MongoMgr.DEFAULT_ROOT_FILE_CACHE_DIR);
		if(sharedCacheDir != null){
			File dir = new File(sharedCacheDir);
			if(!dir.exists()){
				if(!dir.mkdir()){
					logger.error("!!! Cannot create shared cache directory : " + sharedCacheDir);
				}
			}else{
				if(!dir.isDirectory()){
					logger.error("!!! Shared cache directory : " + sharedCacheDir + " is NOT a directory");
				}
			}
		}
	}
	
	/**
	 * Sets the java system property for the parameter with the web.xml parameter value
	 * or default value if the system property is not set.
	 * @param parameter
	 * @param context
	 * @param defaultValue
	 */
	private static String setSystemPropertyValue(String parameter, ServletContext context, String defaultValue){
		String value = System.getProperty(parameter);
		if(value == null){
			value = getParameter(parameter, context);
			if(value == null){
				value = defaultValue;
			}
			System.setProperty(parameter, value);
		}
	
		logger.info("System property " + parameter + " = " + value);
		return value;
	}
	
	public static boolean useChartAnimation()
	{
		return ServerContext.useChartAnimation;
	}
	
	public synchronized static String getParameter(String name, ServletContext context)
	{
		// property file always overrides the web.xml setting
		String value = null;
		if (overrides != null)
		{
			value = overrides.getProperty(name);
		}
		if (value == null)
		{
			value = context.getInitParameter(name);
		}
		else
		{
			logger.debug("Using override for " + name + ": " + value);
		}
		return value;
	}

	public static Double getDefaultScaleFactor()
	{
		return defaultScaleFactor;
	}

	/**
	 * get the repository
	 * @return Returns the repository
	 */
	public static Repository getRepository()
	{
		com.successmetricsinc.UserBean ub = (com.successmetricsinc.UserBean) SMIWebUserBean.getUserBean();
		if (ub != null)
			return ub.getRepository();
		else {
			com.successmetricsinc.UserBean userBean = com.successmetricsinc.UserBean.getParentThreadUserBean();
			if (userBean != null)
				return userBean.getRepository();
		}
		logger.debug("No repository found");
		return null;
	}

	// XXX only use is in multi-tenant seed queries, should find another way to do this
	public static String getApplicationRoot()
	{
		return applicationRoot;
	}
	
	public static JasperReport getReport(String fileName) throws Exception {
		com.successmetricsinc.UserBean userBean = com.successmetricsinc.UserBean.getParentThreadUserBean();
		Session session = userBean.getRepSession();
		String fullFileName = CatalogManager.getNormalizedPath(fileName, session.getCatalogDir());
		return getReport(fullFileName, userBean.getRepository(), session.getSessionVariables(), Session.isPrint(), Session.isPdf(), 
				Session.getDashletPrompts(), session, true);
	}
	
	public static JasperReport getReport(String fileName, Repository repository, Map<String, Object> ps, 
			boolean isPrint, boolean isPdf, String dashletPrompts, Session session, boolean isSubreport) throws Exception{
		long start = System.currentTimeMillis();
		try {
			File reportFile = new File(fileName);
			
			String baseName = reportFile.getName();
			int index = baseName.lastIndexOf('.');
			if (index > 0) {
				baseName = baseName.substring(0, index);
			}
			File adhocReportFile = new File(reportFile.getParentFile(), baseName + ".AdhocReport");
			if (adhocReportFile.exists()) {
				JasperReport jr = session == null ? null : session.getJasperReport(adhocReportFile.getCanonicalPath() + dashletPrompts);
				if (jr == null) {
					AdhocReport ar = AdhocReport.read(adhocReportFile);
					boolean selectorViewNoChart = ps == null ? false : ps.containsKey(ChartOptions.CHART_TYPE) && ps.get(ChartOptions.CHART_TYPE) == null;
					jr = ar.getDesign(repository, isPrint, false, true, true, ps, 
							isPdf, selectorViewNoChart, dashletPrompts, isSubreport);
					if (session != null && ar.isHidedataFlagSet() == false)
						session.setJasperReport(adhocReportFile.getCanonicalPath() + dashletPrompts, jr);
				}
				return jr;
			}
			else {
				File jrxmlFile = new File(reportFile.getParentFile(), baseName + ".JRXML");
				if (jrxmlFile.exists()) {
					JasperReport jr = session == null ? null : session.getJasperReport(jrxmlFile.getCanonicalPath());
					if (jr == null) {
						JasperDesign jdesign = JRXmlLoader.load(jrxmlFile);
						jr = JasperCompileManager.compileReport(jdesign);
						if (session != null)
							session.setJasperReport(jrxmlFile.getCanonicalPath(), jr);
					}
					return jr;
				}
				logger.debug(jrxmlFile.getAbsolutePath() + " does not exist");
			}
			logger.debug(adhocReportFile.getAbsolutePath() + " does not exist");
		}
		catch (Exception e) {
			logger.error(e, e);
		}
		finally
		{
			logger.debug("+++ get subreport (" + fileName + "): " + (System.currentTimeMillis() - start));			
		}
		logger.debug("could not getReport for " + fileName);
		return null;
	}
	
	/*
	 * old method used in jrxml files
	 * @deprecated
	 */
	public static String getCatalogObject(String name) throws Exception
	{
		logger.debug("getCatalogObject: " + name);
		String upperCaseName = name.toUpperCase();
		if (upperCaseName.endsWith(".JRXML") || 
				upperCaseName.endsWith(".ADHOCREPORT") || 
				upperCaseName.endsWith(".JASPER") || 
				upperCaseName.endsWith(".JASPER440")) {
			logger.error("Use of ServerContext.getCatalogObject (which no longer works) for " + name);
			return null; // jasper will be catching this, so no need to throw an exception
		}
		Session session = Session.getCurrentThreadSession();
		if (session != null) {
			String catDir = session.getCatalogDir();
			
			String fileName = CatalogManager.getNormalizedPath(name, catDir);
			File f = CatalogManager.staticGetReportFile(fileName);
			if (f != null && f.exists()) {
				logger.info("getCatalogObject: returning file " + f.getAbsolutePath());
				return f.getAbsolutePath();
			}
			
			f = new File(catDir + "/" + name);
			if (f.exists()) {
				logger.info("getCatalogObject: returning file " + f.getAbsolutePath());
				return f.getAbsolutePath();
			}
		}
		
		SMIWebUserBean userBean = SMIWebUserBean.getUserBean();
		if (userBean != null) {
			CatalogManager manager = userBean.getCatalogManager();
			File file = manager.getReportFile(userBean, name);
			if (file != null && file.exists()) {
				logger.info("getCatalogObject: returning file " + file.getAbsolutePath());
				return file.getAbsolutePath();
			}
		}

		FileNotFoundException f = new FileNotFoundException(name + " does not exist in the catalog."); 
		logger.warn("Could not find obect " + name, f);
		throw f;
	}

	public static String getCustomerPropertiesFilePath()
	{
		return customerPropertiesFile;
	}
	
	private static Double calculateScaleFactor(Double screenWidth, Double screenHeight)
	{
		if (screenWidth == null)
		{
			screenWidth = 1024.0;
		}
		if (screenHeight == null)
		{
			screenHeight = 768.0;
		}
		// Cap the resolution at 1024 X 768.
		if ((screenWidth.doubleValue() > 1024) || (screenHeight.doubleValue() > 768))
		{
			screenWidth = 1024.0;
			screenHeight = 768.0;
		}
		Double availableScreenWidth = calculateAvailableScreenWidth(screenWidth);
		Double scaleFactor = Double.valueOf((availableScreenWidth.doubleValue() / 612));
		return scaleFactor;
	}

	private static Double calculateAvailableScreenWidth(Double screenWidth)
	{
		if (screenWidth == null)
		{
			screenWidth = 1024.0;
		}
		double availableScreenWidth = screenWidth.doubleValue() * 0.975; // 97.5% of width is available.
		return Double.valueOf(availableScreenWidth);
	}

	public static String replacePathVariables(String str)
	{
		if (str == null || str.equals(""))
		{
			return str;
		}
		if (smiInstallDir != null)
		{
			str = str.replace(SMI_INSTALL_DIR_TAG, smiInstallDir);
		}
		if (tomcatInstallDir != null)
		{
			str = str.replace(TOMCAT_INSTALL_DIR_TAG, tomcatInstallDir);
		}
		return str;
	}
	
	public static boolean isSeedQueryInMultitenant() {
		return seedQueryInMultitenant;
	}
	
	public static String getSeedQuerySpaceIDFilePath()
	{
		return seedQuerySpaceIDFilePath;
	}
	
	public static String getSeedDirName() {
		return seedDirName;
	}
	
	public static Properties getServerOverrides() {
		return overrides;
	}	
	
	public static List<String> getLoginServiceAllowedIPs()
	{
		return loginServiceAllowedIPs;
	}
	private static void setLoginServiceAllowedIPs(String ips){		
		
		if (ips != null)
		{
			StringTokenizer st = new StringTokenizer(ips, ", ");
			loginServiceAllowedIPs = new LinkedList<String>();
			while (st.hasMoreTokens())
			{
				String ip = st.nextToken();
				loginServiceAllowedIPs.add(ip);
			}
		}
	}

	public static void setAcornPreferencesEndpoint(String value) {
		acornPreferencesEndpoint = value;
	}

	public static String getAcornPreferencesEndpoint() {
		return acornPreferencesEndpoint;
	}

	public static void errorMessage(Exception ex, String message, HttpServletResponse response)
	{
		if (ex != null)
			logger.debug(ex.getMessage(), ex);
	
		response.setContentType("text/html");
		try
		{
			PrintWriter writer = response.getWriter();
			writer.println("<html>");
			writer.println("<head>");
			writer.println("<title>");
			writer.println("Birst Error");
			writer.println("</title>");
			writer.println("</head>");
			writer.println("<body>");
			writer.println("<div class=\"error\">");
			writer.println("<BR>");
			writer.println(StringEscapeUtils.escapeHtml4(message));
			writer.println("</div>");
			writer.println("</body>");
			writer.println("</html>");
			writer.flush();
		}
		catch (Exception ex2)
		{
			logger.debug(ex2.getMessage(), ex2);
		}
	}

	public static boolean isProcessExpressionsSerially() {
		return processExpressionsSerially;
	}

	public static String getBirstAdminUrl() {
		
		return birstAdminUrl;
	}

	public static void closeAllPublishersAndSubscribers() {
		try {
			queuePublishManager.closeAllPublishers();	
			closeAllSubscribers();
		} catch (MessengerException ex) {
			logger.warn(ex.getMessage());
		}
	}	

	public static void closeAllSubscribers(){		
		if(subscribedConsumers != null && subscribedConsumers.size() > 0){
			for(Topic topic : subscribedConsumers.keySet()){
				Iterator<Consumer> iter = subscribedConsumers.get(topic).iterator();
				while(iter.hasNext()){		
					Consumer consumer = iter.next();
					try{					
						consumer.close();					
					}catch(Exception ex){
						logger.warn(ex.getMessage());
					}
					iter.remove();
				}
				QueueConnectionManager.closeConnectionForTopic(topic);
			}			
		}
	}
	
	public static void stopQueueHealthCheck(){
		if(healthChecker != null){
			healthChecker.stop();
		}
	}	
	
	public static void startInitializedPublishersAndSubscribers() throws MessengerException{
		if (AdhocReportHelper.isAsynchReportGenerationEnabled()) {
			if(initializedQueues != null && initializedQueues.size() > 0){
				for(Queue queue : initializedQueues){
					queuePublishManager.initializePublishers(queue);
				}
			}
			
			if(initializedTopics != null && initializedTopics.size() > 0){
				for(Topic topic : initializedTopics){
					queuePublishManager.initializePublishers(topic);
				}
			}			
			addReportSubscribers();
		}
	}
	
	public static QueuePublishManager getQueuePublishManager(){
		return queuePublishManager;
	}
}
