/**
 * $Id: DashboardFolders.java,v 1.2 2009-09-17 16:10:09 pconnolly Exp $
 *
 * Copyright (C) 2007-2009 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.catalog;

import java.util.List;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.successmetricsinc.WebServices.IWebServiceResult;
import com.successmetricsinc.util.XmlUtils;

/**
 * To encapsulate the List<String> of dashboard folders' file paths for Axis2.
 * @author pconnolly
 */
public class DashboardFolders implements IWebServiceResult {
	
	private static final String DASHBOARD_FOLDERS = "DashboardFolders";
	private static final String DASHBOARD = "Dashboard";
	
	private List<String> folders;

	/**
	 * 	
	 * @param fileNode
	 */
	public DashboardFolders(List<String> folders) {
		this.folders = folders;
	}

	public void addContent(OMElement parent, OMFactory factory, OMNamespace ns) {
		OMElement element = factory.createOMElement(DASHBOARD_FOLDERS, ns);
		for (String folder : folders) {
			XmlUtils.addContent(factory, element, DASHBOARD, folder, ns);
		}
		parent.addChild(element);
	}

	public void setFolders(List<String> folders) {
		this.folders = folders;
	}
	
	public List<String> getFolders() {
		return folders;
	}
}
