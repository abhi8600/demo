/**
 * $Id: FileNode.java,v 1.12 2012-01-12 01:25:26 agarrison Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.catalog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.WebServices.IWebServiceResult;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 */
public class FileNode implements IWebServiceResult {
	private static final String CREATED_BY = "CreatedBy";
	public static final String CHILDREN = "ch";
	private static final String IS_DIRECTORY = "dir";
	private static final String IS_DASHBOARD = "dash";
	private static final String LABEL = "l";
	public static final String NAME = "n";
	private static final String MODIFIED_DATE = "m";
	private static final String LOGIN_TYPE = "lt";
	private static final String SUPERUSER = "superuser";
	private static final String OWNER = "owner";
	public static final String REGULAR = "regular";
	public static final String PRIVATE = "private";
	public static final String WRITABLE = "w";

	public static final String NODE_SEPARATOR = "/";
	
	private String label;
	private String name;
	private boolean isDirectory;
	private boolean isDashboard;
	private Date modifiedDate;
	private boolean isWritable;
	private String createdBy;
	
	protected List<FileNode> children;

	
	/**
	 * Full constructor for a file.
	 * @param name is the full filepath of the file.
	 * @param label is the short file name.
	 * @param isDirectory true if it's a directory; false otherwise.
	 * @param createdDate is the date/time that the file was originally created.
	 * @param modifiedDate is the date/time that the file was last modified.
	 */
	public FileNode(String name, String label, boolean isDirectory, Date modifiedDate, boolean canModify) {
		this.name = name;
		this.label = label;
		this.isDirectory = isDirectory;
		this.modifiedDate = modifiedDate;
		this.isWritable = canModify;
	}
	
	public FileNode(String name, String label, boolean isDirectory, long lastModified, boolean canModify) {
		this(name, label, isDirectory, new Date(lastModified), canModify);
	}

	/**
	 * 
	 * @param catalogFile
	 */
	public FileNode(CatalogManager.CatalogFile catalogFile) {
		this.setLabel(catalogFile.getName());
		this.setName(catalogFile.getName());
		this.setIsDirectory(catalogFile.getFile().isDirectory());
		this.setModifiedDate(catalogFile.getModifiedDate());
		this.isWritable = catalogFile.canModify();
	}
	

	/**
	 * @return list of file nodes
	 */
	public List<FileNode> getChildren() {
		return children;
	}

	/**
	 * 
	 * @param children
	 */
	public void setChildren(List<FileNode> children) {
		if (this.getName() != null && this.getName().trim().length() > 0) {
			for (FileNode child: children) {
				child.setName(this.getName().concat(NODE_SEPARATOR).concat(child.getName()));
			}
		}
		this.children = children;
	}

	/**
	 * 
	 * @param child
	 */
	public void addChild(FileNode child) {
		if (this.children == null) {
			this.children = new ArrayList<FileNode>();
		}
		this.children.add(child);
	}

	/**
	 *  @param parent
	 *  @param ns
	 */
	public OMElement addChildContent(OMFactory factory, OMElement parent, OMNamespace ns, FileNode node) {
		OMElement el = factory.createOMElement("FileNode", ns);
		// Add 'superUser' attribute to 'private' node 
		if (node.getLabel().equals(PRIVATE)) {
			if (SMIWebUserBean.getUserBean().isSuperUser()) {
				el.addAttribute(LOGIN_TYPE, SUPERUSER, ns);
			} else {
				if (SMIWebUserBean.getUserBean().isOwner()) {
					el.addAttribute(LOGIN_TYPE, OWNER, ns);
				} else {
					el.addAttribute(LOGIN_TYPE, REGULAR, ns);
				}
			}
		}  
		el.addAttribute(NAME, node.getName(), ns);
		el.addAttribute(LABEL, node.getLabel(), ns);
		if (node.isDirectory())
			el.addAttribute(IS_DIRECTORY, Boolean.toString(true), ns);
		if (node.isDashboardDirectory()) {
			el.addAttribute(IS_DASHBOARD, Boolean.toString(true), ns);
		}
		if (node.isWritable())
			el.addAttribute(WRITABLE, Boolean.toString(true), ns);
		
		if (node.getCreatedBy() != null) {
			el.addAttribute(CREATED_BY, node.getCreatedBy(), ns);
		}
		
		Date dt = node.getModifiedDate();
		if (dt != null)
		{
			SimpleDateFormat sdf = new SimpleDateFormat(XmlUtils.dateFormatPattern); // not multithread safe, moved to here from static
			el.addAttribute(MODIFIED_DATE, sdf.format(dt), ns);
		}
		
		List<FileNode> children = node.getChildren();
		if (children != null) {
			OMElement childrenEl = factory.createOMElement(CHILDREN, ns);
			for (FileNode nd : children) {
				addChildContent(factory, childrenEl, ns, nd);
			}
			el.addChild(childrenEl);
		}
		parent.addChild(el);
		return el;
	}

	public void addContent(OMElement parent, OMFactory factory, OMNamespace ns) {
		parent = addChildContent(factory, parent, ns, this);
/*		for (FileNode node : this.children) {
			addChildContent(factory, parent, ns, node);
		}
		*/
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isDirectory()
	{
		return isDirectory;
	}
	
	public void setIsDirectory(boolean newval)
	{
		isDirectory = newval;
	}

	public boolean isDashboardDirectory()
	{
		return isDashboard;
	}
	
	public void setIsDashboardDirectory(boolean newval)
	{
		isDashboard = newval;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	public boolean isWritable() {
		return isWritable;
	}

	public void setWritable(boolean isWritable) {
		this.isWritable = isWritable;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedBy() {
		return createdBy;
	}

}
