/**
 * $Id: CatalogPermission.java,v 1.38 2011-09-24 01:11:12 ricks Exp $
 *
 * Copyright (C) 2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.successmetricsinc.catalog;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import org.apache.axiom.om.OMAttribute;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.xml.sax.InputSource;

import com.successmetricsinc.Group;
import com.successmetricsinc.Repository;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.WebServices.IWebServiceResult;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 * @author pconnolly
 */
public class CatalogPermission implements IWebServiceResult {

	private static Logger logger = Logger.getLogger(CatalogPermission.class);
	
	private static final String PERMISSION_FILE_NAME = "permission.xml";
	
	private static final String GROUP = "Group";
	private static final String GROUP_NAME = "GroupName";
	private static final String NAME = "Name";
	
	private static final String VIEW = "View";
	private static final String MODIFY = "Modify";
	private static final String UPLOAD = "Upload";
	private static final String DOWNLOAD = "Download";
	private static final String ADMIN = "Admin";

    /** Is the list of all possible permission types. */
	public static final String[] PERMISSION_TYPES = {
		VIEW,
		MODIFY,
	};
	
	/** The full file path of the folder which this instance of CatalogPermission represents. */
	private String folderPath;
	
	/** 
	 * Lists of group names, each keyed by the permission type (e.g., View, Modify,...) that reflects
     * the permissions for the current instance.  The set of keys used is provided by {@link #PERMISSION_TYPES}.
	 */
	private Map<String, Set<String>> groupMap;
	
	/**
	 * No trespassing constructor.
	 */
	@SuppressWarnings("unused")
	private CatalogPermission() {
	}
	
	/**
	 * Gets the complete tree of permissions for the specified folder path.  
	 * It recurses up the tree to the 'root' folder (which returns "all" permissions)
     * with each folder's permissions reflecting the permissions found at that level.
	 * If a 'permission.xml' file is encountered in any of the children folders, those
     * permissions are read in and form the contents of the groups lists for each
     * permission type (i.e., View, Modify,...)
	 * <p/>
	 * After retrieving the entire tree of permissions, a final filter can be <b>optionally</b>
	 * performed -- {@link #applyParentalPermissionsOverrides()} -- that walks up the newly
	 * created permissions instance's folder tree and removes less-restrictive permissions
	 * (i.e., allowed) for any group and type combination (e.g., USER$/Modify) if there is a more
     * restrictive permission (i.e., prohibited) above it in the tree.  For example, in the following
	 * example the final 'a' is overridden by the 'prohibited' entry above it.
 	 * <table>
 	 * <tr><th width="67%"></th><th>USER$/Modify</th></tr>                                                    
	 * <tr><td>/ (root) - all permissions</td><td>allowed</td>
	 * <tr><td>&nbsp;&nbsp;/a</td><td>-</td>
	 * <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;/b</td><td>prohibited</td>
	 * <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/c</td><td>-</td>
	 * <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/d</td><td>allowed</td>
	 * </table>           
     * <p/>
     * (The '-' indicates that that level of the directories had no permission.xml override file.)
     * <p/>
	 * This scenario can occur because, although there are edits on {@link #savePermissions(String)} to
	 * disallow the creation of the 'd' directory's 'allowed' entry, the 'b' directory's overrides 
	 * can be created <b>after</b> the 'd' directory's overrides.  In that case, navigating the all
	 * the subdirectory trees to avoid (or correct!) this occurrence during the 'b' directory's 
	 * permissions save processing would be overly complex and can be more easily handled 
	 * with the final filtering process provided by {@link #applyParentalPermissionsOverrides()}.
	 * <p/>
	 * When the 'spaceAdminUser' flag is set to 'true' all permissions and groups are used
	 * in the formulation, rather than just those groups to whom the logged-in user is a member.
	 * This is because the space administrator has access to all groups when setting a 
	 * folder's permissions.
	 * @param rootFolder is the path of the root folder ('catalog') of the current space.
	 * @param folderPath is the path whose permissions should be derived.
	 */
	public CatalogPermission(final String rootFolder, 
			                 final String folderPath) {
		// Save the current folder path
		this.folderPath = folderPath;
		// At the root folder, add all permissions and return
		File rootFile = new File(rootFolder);
		File folderFile = new File(folderPath);

		while (folderFile != null && ! rootFile.equals(folderFile)) {
			
			// Is there a permission.xml file?
			File permissionsFile = new File(folderFile, PERMISSION_FILE_NAME);				
	
			if (permissionsFile.exists()) {
	            try { 
	            	 
				    parseFileToSetPermissions(permissionsFile);
				    break;
	            } catch (IOException e) {
	                // IO error; log error and let user reset the values
	                logger.error("Could not read permission file", e);
	                initializeGroupMap();
	            } catch (JDOMException e) {
	                // Corrupt XML file; log error and let user reset the values.
	                logger.error("Could not parse permission file", e);
	                initializeGroupMap();
	            }
			}
			else {
				folderFile = folderFile.getParentFile();
			}
		}

		if (folderFile == null || folderFile.equals(rootFile)) {
			// Otherwise initialize all permission types with 'null' group lists
			initializeRootPermissions();
		}
	}
	

    /**
     * Creates a single-level permissions instance when given a Flex XMLList representation
     * of the user's modified permissions.  No parentPermission links are formed.
     * <p/>
     * This constructor adds a <Groups> root element to the XMLList structure to produce a
     * well-formed XML document which can then be converted to an OMElement.  The OMElement
     * is then used to populate the group lists.
     * <p/>
     * The instance created by this constructor is only meant to be used in the 
     * {@link CatalogManager#savePermissions(String, String)} and {@link #savePermissions(String)}
     * methods since it does not have any parent permissions.  Since it has no parent
     * permissions it cannot be used to derive a permissions instance that includes
     * parental prohibitions.  (See {@link #CatalogPermission(String, String, boolean)}
     * for more information.
     * @param xmlDocument is the String-ified (toXmlString()) XMLList input from the
     * Flex CatalogManagement.ContextMenuEventManager.savePermissions() method.
     */
    public CatalogPermission(final String xmlDocument) {
    	// Receive XML string from Axis2 with bizarre encoding: &lt;Group>&lt;Name>...
//        final String wellFormedXml = "<Groups>\n" + xmlDocument.replaceAll("&lt;", "<") + "\n</Groups>";
//        logger.trace("Creating CatalogPermission instance using:\n" + wellFormedXml);
        final OMElement element = XmlUtils.convertToOMElement(xmlDocument);
        // Parse OMElement to populate permission group lists
        initialize(element);
    }

    /**
	 * Read in the 'permission.xml' file and populate the various permissions groups.
	 * @param file is the 'permission.xml' file instance.
     * @throws IOException if there is a problem reading the file.
     * @throws JDOMException if there is a problem parsing the file.
	 */
	protected void parseFileToSetPermissions(final File file) throws IOException, JDOMException {
		
        SAXBuilder builder = new SAXBuilder();       
        Document doc = builder.build(file);
        Element root = doc.getRootElement();      
        groupMap = new HashMap<String, Set<String>>();
        for (String permissionType : PERMISSION_TYPES) {
            groupMap.put(permissionType, getGroupNames(root, permissionType));
        }
	}
	
	public void savePermissions(String fullyQualifiedDirectoryPath) {
		// Nothing to save. Exit.
		boolean nothingToSave = true;
		for (Set<String> group : groupMap.values()) {
			if (group != null) {
				nothingToSave = false;
				break;
			}
		}
		if (nothingToSave) {
			return;
		}
		String pathName = fullyQualifiedDirectoryPath + File.separator + PERMISSION_FILE_NAME;
		File file = new File(pathName);
		if (! file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				logger.error("Can not create permission file.\n", e);
			}
		}
		if (! file.canWrite()) {
			logger.error("Can not write catalog permission file " + pathName);
			return;
		}
		Element root = new Element(this.getClass().getName());
		Document doc = new Document(root);
		for (String permissionType : PERMISSION_TYPES) {
			if (ADMIN.equals(permissionType))
				continue;
			
			addContent(root, permissionType, groupMap.get(permissionType));
		}	
		XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat());
		outputter.getFormat().setEncoding("UTF-8" );		
		FileOutputStream writer = null;
		try {
			writer = new FileOutputStream(file);
			outputter.output(doc, writer);
		} catch (IOException e) {
			logger.error("Could not save catalog permission file " + pathName, e);
		}
		finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e) {
					logger.error(e);
				}
			}
		}
	}
	
	private void addContent(Element parent, String elementName, Set<String> groupContent) {
		Element element = new Element(elementName);
		Element groups = new Element(GROUP_NAME);
		if (groupContent != null) {
			for (String s : groupContent) {
				Element content = new Element("String");
				content.setText(s);
				groups.addContent(content);
			}
		}
		element.addContent(groups);
		parent.addContent(element);
	}
	
	/**
	 * Being the superuser 'admin' (as set in the Repository instance) 
	 * trumps all other settings.  The superuser 'admin' differs from the
	 * regular "Admin" group.
	 * Otherwise there are three situations that should be checked:
	 * <dl>
	 * <dt>null list of groups</dt>
	 * <dd>means that no permission overrides were set for this folder.  
	 * In that case:
	 * <ul>
	 * <li>if the folder is the root folder (i.e., there is no 'parentPermission' 
	 * instance defined, assume that the user can view.<br/>
	 * (Root folder gives 'all' permissions.)</li>
	 * <li>if not the root folder then recurse up the tree to see if there is
	 * a parental override.</li>
	 * </ul>
	 * </dd>
	 * <dt>list of groups is not null but is empty</dt>
	 * <dd>
	 * <ul>
	 * <li>If the list of groups is empty, then the view permission has been 
	 * explicitly denied.</li>
	 * </ul>
	 * </dd>
	 * <dt>list of groups has entries</dt>
	 * <dd>
	 * <li>If there is a non-empty list, then see if that list intersects with
	 * the complete list of groups for this user.  If so, then the user has
	 * the 'view' permission.</li>
	 * </ul>
	 * </dd>
	 * </dl>
	 * @param permissionType is the type of permission (i.e., "View", "Modify",...) to
     * query and must be one of the ones contained in the {@link #PERMISSION_TYPES} array..
	 * @return true if the user can view this folder; false otherwise.
	 */
	public boolean hasPermission(final String permissionType, List<Group> groupList, boolean isSuperUserOwnerOrAdmin) {
        if (!isValidPermissionType(permissionType)) {
            throw new IllegalArgumentException("'permissionType' :" + permissionType + " is invalid.");
        }
		// Superuser or space admin (OWNER$) trumps the other settings
		// 'getIsAdmin()' not very useful: only Administrator user is a member of the Administrators group...
		if (isSuperUserOwnerOrAdmin) { 
			return true;
		}
		final Set<String> groups = groupMap.get(permissionType);
		if (groups == null || groups.isEmpty()) {
			return false;
		}
		return groupsIntersect(groupList, groups);
	}
	
	/**
	 * Convenience method for 'viewability' of folder.
	 * @return true if the user can view this folder; false otherwise.
	 */
	public boolean canView(List<Group> groupList, boolean isSuperUserOwnerOrAdmin) {
		return hasPermission(VIEW, groupList, isSuperUserOwnerOrAdmin);
	}
	
	/**
	 * Convenience method for 'modifiability' of folder.
	 * @return true if the user can modify this folder; false otherwise.
	 */
	public boolean canModify(List<Group> groupList, boolean isSuperUserOwnerOrAdmin) {
		return hasPermission(MODIFY, groupList, isSuperUserOwnerOrAdmin);
	}


	/**
	 * Builds OM element in the form:
	 * <ul style="list-style: none;">
	 * <li>&lt;Groups&gt;</li>
	 * <ul style="list-style: none;">
	 * <li>&lt;Group&gt;</li>
	 * <ul style="list-style: none;">
	 * <li>&lt;Name&gt;USER$&lt;/Name&gt;</li>
	 * <li>&lt;View&gt;true&lt;/View&gt;</li>
	 * <li>&lt;Modify&gt;false&lt;/Modify&gt;</li>
	 * <li>&lt;Upload&gt;false&lt;/Upload&gt;</li>
	 * <li>&lt;Download&gt;true&lt;/Download&gt;</li>
	 * <li>&lt;Administrator&gt;false&lt;/Administrator&gt;</li>
	 * </ul>
	 * <li>&lt;/Group&gt;</li>
	 * <li>...</li>
	 * </ul>
	 * <li>&lt;Groups&gt;</li>
	 * </ul>
	 * Since groups are stored by permission type in this class and the XML needed
	 * for the Flex app has permissions by group, this method has to first cull a 
	 * complete list of all the groups, then build the permissions for each group.
	 */
	public void addContent(OMElement parent, OMFactory factory, OMNamespace ns) {
		// Get distinct list of all the groups (i.e., USER$, OWNER$, Administrator,...)
        final List<Group> allGroups = SMIWebUserBean.getUserBean().getRepository().getGroups(true);
		OMElement groupsElement = factory.createOMElement("Groups", ns);
		// Create XML subelement "Group" for each group in this CatalogPermission instance
		for (Group group : allGroups) {
			OMElement groupElement = factory.createOMElement("Group", ns);
			groupElement.addAttribute("Name", group.getName(), ns);
			// Add XML subelements for each permission within this "Group"
			for (String permissionType : PERMISSION_TYPES) {
				if (ADMIN.equals(permissionType))
					continue;
				
				Set<String> groups = groupMap.get(permissionType);
				String trueFalse; 
				if (groups == null) {
					trueFalse = "false";
				} else {
					trueFalse = groups.contains(group.getName()) ? "true" : "false";
				}
				groupElement.addAttribute(permissionType, trueFalse, ns);
			}
			groupsElement.addChild(groupElement);
		}
		parent.addChild(groupsElement);
	}
	
	/**
	 * Reads the OMElement XML created from the Flex client's XMLList to 
	 * populate the current CatalogPermission instance.
	 * @param document is the OMElement generated from the String-ified version 
	 * of the Flex savePermissions XMLList.
	 */
	@SuppressWarnings("unchecked")
	public void initialize(final OMElement document) {
		document.build();
		//document.detach();
		
		// 'groupMap' contains the list of groups belonging to each permission type 
		initializeGroupMap();
		
		// 'elementIterator'/'element' at the <Group> child level (i.e., <Name>, <View>,...)
		for (Iterator<OMElement> elementIterator = document.getChildElements(); elementIterator.hasNext(); ) {
			OMElement element = elementIterator.next();
			final String elementName = element.getLocalName();
			if (GROUP.equals(elementName)) {
				String groupName = null;
				for (Iterator<OMAttribute> childIterator = element.getAllAttributes(); childIterator.hasNext(); ) {
					OMAttribute childElement = childIterator.next();
					final String childName = childElement.getLocalName();
					if (NAME.equals(childName)) {
						groupName = childElement.getAttributeValue();
						continue;
					}
					if (VIEW.equals(childName)) {
						if ("true".equals(childElement.getAttributeValue())) {
							addGroup(VIEW, groupName);
						} 
						continue;
					}
					if (MODIFY.equals(childName)) {
						if ("true".equals(childElement.getAttributeValue())) {
							addGroup(MODIFY, groupName);
						}
						continue;
					}
					if (UPLOAD.equals(childName)) {
						if ("true".equals(childElement.getAttributeValue())) {
							addGroup(UPLOAD, groupName);
						} 
						continue;
					}
					if (DOWNLOAD.equals(childName)) {
						if ("true".equals(childElement.getAttributeValue())) {
							addGroup(DOWNLOAD, groupName);
						} 
						continue;
					}
				}
			} else {
				// Logic error.  Should be encountering <Group> elements at this level
				throw new IllegalStateException("Incorrect element type encountered");
			}
		}
		// Replace any null lists with empty lists to avoid problems with list compares
		for (String permissionType : PERMISSION_TYPES) {
			if (groupMap.get(permissionType) == null) {
				groupMap.put(permissionType, new HashSet<String>());
			}
		}
	}
	
	/* ----------------------------------------------------------------------------------
	 * -- Helper methods                                                               --
	 * ---------------------------------------------------------------------------------- */
	
	
    private Set<String> getGroupNames(Element parent, String childName) {
        return getNames(parent, childName, GROUP_NAME);
    }

    @SuppressWarnings("rawtypes")
	private Set<String> getNames(Element parent, String childName, String grandchildName) {
		
		Element child = parent.getChild(childName);
		if (child != null) {
			List groups = child.getChildren(grandchildName);
			if (groups != null && groups.size() == 1) {
				Element element = (Element) groups.get(0);
				List names = element.getChildren("String");
				if (names != null) {
					int size = names.size();
					Set<String> list = new HashSet<String>();
					for (int i = 0; i < size; i++) {
						Element el = (Element) names.get(i);
						if (el != null) {
							list.add(el.getTextTrim());
						}
					}
					return list;
				}
			}
		}
		return Collections.emptySet();
	}
	

	/**
	 * Applies the updates in the 'newPermissions' instance to this current instance, 
	 * returning the updated {@link CatalogPermission} instance.
	 * @param newPermissions is the {@link CatalogPermission} instance representing the new permissions.
	 * @return the updated {@link CatalogPermission} instance.
	 */
	public CatalogPermission applyUpdates(final CatalogPermission newPermissions) {
		logger.trace("'currentPermissions' before 'applyUpdates()':\n" + this);
		logger.trace("'newPermissions' before 'applyUpdates()':\n" + newPermissions);
		// Process each permission type
		for (String permissionType : CatalogPermission.PERMISSION_TYPES) {
			Set<String> currentGroups = groupMap.get(permissionType);
			final Set<String> newGroups = newPermissions.groupMap.get(permissionType);
			// Should groups be taken out of currentGroups?
			for (String currentGroup : currentGroups) {
				if (!newGroups.contains(currentGroup)) {
					currentGroups = removeGroupFromPermissionType(currentGroups, currentGroup, permissionType);
				}
			}
			// Does newGroups include group(s) not in current group?
			for (String newGroup : newGroups) {
				currentGroups.add(newGroup);
			}
			groupMap.put(permissionType, currentGroups);
		}
		logger.trace("'currentPermissions' after 'applyUpdates()':\n" + this);
		return this;
	}
	
	/**
	 * Removes the current group from the permission type, effectively taking away that
	 * permission for that group.  
	 * @param groupNames is the set of group names to examine.
	 * @param groupName is the name of the group to be removed from the permission type.
	 * @param permissionType is, well, the permission type (i.e., View, Modify,...).
	 * @return the updated (if necessary) subset of group names.
	 */
	private Set<String> removeGroupFromPermissionType(final Set<String> groupNames, 
			                                          final String groupName, 
			                                          final String permissionType) {
		if (groupNames.contains(groupName)) {
			Set<String> filteredGroups = new HashSet<String>();
			for (String aGroup : groupNames) {
				if (!aGroup.equals(groupName)) {
					filteredGroups.add(aGroup);
				}
			}
			// Replace 'groupNames' with new subset of groups
			return filteredGroups;
		} else {
			return groupNames;
		}
	}
	
	/**
	 * Initializes the root level folder in which every group has every privilege.
	 */
	private void initializeRootPermissions() {
		// Get the repository for this space
		final com.successmetricsinc.UserBean userBean = SMIWebUserBean.getUserBean();
		if (userBean == null) {
			throw new IllegalStateException("Could not retrieve user information.");
		}
		final Repository repository = userBean.getRepository();
		if (repository == null) {
			throw new IllegalStateException("Could not retrieve repository information.");
		}
		// Get all available groups for this space
		final List<Group> groups = repository.getGroups(true);
		groupMap = new HashMap<String, Set<String>>(PERMISSION_TYPES.length);
		for (String permissionType : PERMISSION_TYPES) {
			groupMap.put(permissionType, new HashSet<String>(groups.size()));
			for (Group group : groups) {
				groupMap.get(permissionType).add(group.getName());
			}
		}
	}
	
	private void addGroup(final String permissionType, final String group) {
		if (groupMap.get(permissionType) == null) {
			groupMap.put(permissionType, new HashSet<String>());
		}
		// Set: can't add the same group twice...
		groupMap.get(permissionType).add(group);
	}
	
	
    /**
     * Initializes the map of group lists as 'null' for each permission type.
     * The resulting {@link #groupMap} represents a folder with no 'permission.xml'
     * file overrides.
     */
	private void initializeGroupMap() {
		groupMap = new HashMap<String, Set<String>>(PERMISSION_TYPES.length);
		for (String permissionType : PERMISSION_TYPES) {
			groupMap.put(permissionType, null);
		}
	}

	public String getFolderPath() {
		return folderPath;
	}
	
	public void setFolderPath(final String folderPath) {
		this.folderPath = folderPath;
	}

    /**
     * Checks that the permission type is one of the valid values.
     * @param permissionType is the value to check.
     * @return true if the 'permissionType' is a valid one; false otherwise;
     */
    private boolean isValidPermissionType(final String permissionType) {
        boolean isValid = true;
        if (permissionType == null || permissionType.equals("")) {
            isValid = false;
        }
        if (!contains(Arrays.asList(PERMISSION_TYPES), permissionType)) {
            isValid = false;
        }
        return isValid;
    }
    
	private boolean contains(List<String> list, String searchItem) {
		boolean found = false;
		for (String item : list) {
			if (item.equals(searchItem)) {
				found = true;
				break;
			}
		}
		return found;
	}

	/**
	 * Just like it sounds. Checks 'groups' to see if any of its members is contained in 'actionGroup'.   
	 * @param groups is the first set of groups.
	 * @param actionGroup is the second set of groups.
	 * @return the intersection of those two sets.
	 */
	private boolean groupsIntersect(List<Group> groups, Set<String> actionGroup) {
		if (groups == null)
			return false;
		
		for (Group g : groups) {
			if (actionGroup.contains(g.getName())) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public String toString() {
		// Observe and Report
		StringBuilder sb = new StringBuilder();
		sb.append("Permissions for: '").append(folderPath).append("' are:\n");
		for (String permissionType : groupMap.keySet()) {
			sb.append("Permission type: '").append(permissionType).append("' has groups: '");
			if (groupMap.get(permissionType) != null) {
				for (String group : groupMap.get(permissionType)) {
					sb.append(group).append("', '");
				}
				// Trim off last \n
				if (groupMap.get(permissionType).size() > 0) {
					int lastComma = sb.lastIndexOf(",");
					sb.setLength(lastComma);
				} else {
					sb.append("'");
				}
				sb.append("\n");
			} else {
				sb.append("'");
			}
		}
		// Trim off last \n
		sb.setLength(sb.length() - 1);
		return sb.toString();
	}
}
