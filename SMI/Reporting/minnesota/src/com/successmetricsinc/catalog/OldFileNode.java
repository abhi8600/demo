/**
 * 
 */
package com.successmetricsinc.catalog;

import java.util.Date;
import java.util.List;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class OldFileNode extends FileNode {
	private static final String WRITEABLE = "Writeable";
	private static final String CHILDREN = "children";
	private static final String IS_DIRECTORY = "isDirectory";
	private static final String IS_DASHBOARD = "isDashboard";
	private static final String LABEL = "label";
	private static final String NAME = "name";
	private static final String MODIFIED_DATE = "modifiedDate";
	private static final String LOGIN_TYPE = "loginType";
	private static final String SUPERUSER = "superuser";
	private static final String OWNER = "owner";
	private static final String REGULAR = "regular";
	private static final String PRIVATE = "private";

	
	/**
	 * Full constructor for a file.
	 * @param name is the full filepath of the file.
	 * @param label is the short file name.
	 * @param isDirectory true if it's a directory; false otherwise.
	 * @param createdDate is the date/time that the file was originally created.
	 * @param modifiedDate is the date/time that the file was last modified.
	 */
	public OldFileNode(String name, String label, boolean isDirectory, Date modifiedDate, boolean canModify) {
		super(name, label, isDirectory, modifiedDate, canModify);
	}
	public OldFileNode(String name, String label, boolean isDirectory, long l, boolean canModify) {
		super(name, label, isDirectory, l, canModify);
	}

	public void addContent(OMElement parent, OMFactory factory, OMNamespace ns) {
		for (FileNode node : this.children) {
			addChildContent(factory, parent, ns, node);
		}
	}
	
	public OMElement addChildContent(OMFactory factory, OMElement parent, OMNamespace ns, FileNode node) {
		OMElement el = factory.createOMElement("FileNode", ns);
		// Add 'superUser' attribute to 'private' node 
		if (node.getLabel().equals(PRIVATE)) {
			if (SMIWebUserBean.getUserBean().isSuperUser()) {
				el.addAttribute(LOGIN_TYPE, SUPERUSER, ns);
			} else {
				if (SMIWebUserBean.getUserBean().isOwner()) {
					el.addAttribute(LOGIN_TYPE, OWNER, ns);
				} else {
					el.addAttribute(LOGIN_TYPE, REGULAR, ns);
				}
			}
		}  
		XmlUtils.addContent(factory, el, NAME, node.getName(), ns);
		XmlUtils.addContent(factory, el, LABEL, node.getLabel(), ns);
		XmlUtils.addContent(factory, el, IS_DIRECTORY, node.isDirectory(), ns);
		if (node.isDashboardDirectory()) {
			XmlUtils.addContent(factory, el, IS_DASHBOARD, node.isDashboardDirectory(), ns);
		}
		XmlUtils.addContent(factory, el, MODIFIED_DATE, node.getModifiedDate(), ns);
		
		if (node.isWritable())
			XmlUtils.addContent(factory, el, WRITEABLE, true, ns);
		
		List<FileNode> children = node.getChildren();
		if (children != null) {
			OMElement childrenEl = factory.createOMElement(CHILDREN, ns);
			for (FileNode nd : children) {
				addChildContent(factory, childrenEl, ns, nd);
			}
			el.addChild(childrenEl);
		}
		parent.addChild(el);
		
		return el;
	}
}
