/**
 * 
 */
package com.successmetricsinc.catalog;

import java.util.ArrayList;
import java.util.List;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.successmetricsinc.WebServices.IWebServiceResult;

/**
 * @author agarrison
 *
 */
public class DirectoryNode implements IWebServiceResult {

	private static final String FULL_PATH = "p";
	private String name;
	private String fullPath = "";
	
	protected List<DirectoryNode> children;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<DirectoryNode> getChildren() {
		return children;
	}

	/**
	 * 
	 */
	public DirectoryNode(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.successmetricsinc.WebServices.IWebServiceResult#addContent(org.apache.axiom.om.OMElement, org.apache.axiom.om.OMFactory, org.apache.axiom.om.OMNamespace)
	 */
	@Override
	public void addContent(OMElement parent, OMFactory factory, OMNamespace ns) {
		addContent(parent, factory, ns, this);
	}
	
	
	public static void addContent(OMElement parent, OMFactory factory, OMNamespace ns, DirectoryNode node) {
		OMElement el = factory.createOMElement("DirectoryNode", ns);
		el.addAttribute(FileNode.NAME, node.getName(), ns);
		el.addAttribute(FULL_PATH, node.fullPath, ns);
		
		List<DirectoryNode> children = node.getChildren();
		if (children != null) {
			OMElement childrenEl = factory.createOMElement(FileNode.CHILDREN, ns);
			for (DirectoryNode nd : children) {
				addContent(childrenEl, factory, ns, nd);
			}
			el.addChild(childrenEl);
		}
		parent.addChild(el);
	}

	public void addChild(DirectoryNode child) {
		if (this.children == null) {
			this.children = new ArrayList<DirectoryNode>();
		}
		child.fullPath = (fullPath == null ? "" : fullPath + FileNode.NODE_SEPARATOR) + child.getName();
		this.children.add(child);
	}

	public String getFullPath() {
		return fullPath;
	}

	public void setFullPath(String fullPath) {
		this.fullPath = fullPath;
	}
}
