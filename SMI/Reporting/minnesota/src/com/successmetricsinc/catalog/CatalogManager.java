/**
 * $Id: CatalogManager.java,v 1.161 2012-12-11 17:54:50 BIRST\ricks Exp $
 *
 * Copyright (C) 2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.successmetricsinc.catalog;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;

import com.successmetricsinc.Group;
import com.successmetricsinc.UserBean;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.adhoc.AdhocReport;
import com.successmetricsinc.catalog.CatalogAccessException.Reason;
import com.successmetricsinc.util.SwapSpaceInProgressException;

/**
 * <b>Note:</b> The new (4.0) Flex-related methods accept both relative or absolute file paths.
 * Each method calls the {@link #getNormalizedPath(String)} which converts any relative
 * paths into normalized absolute paths.
 * <b>Note:</b> PAGE files will replace DASHBOARD files with 4.0 release.  
 * TODO: Support for dashboard filetype can be removed after the 4.0 file conversion is complete.
 * TODO: Mark pre-4.0 methods as deprecated.
 * @author agarrison
 * @author pconnolly
 */
public class CatalogManager {
	private static final int MAX_NUMBER_TEMP_DIR_NAMES = 100;
	public static final String NEW_FOLDER_NAME = "New folder";

	public enum ACTION {
		MOVE,
		COPY,
		RENAME,
		DELETE,
		ADD_FOLDER,
		GET_FILESYSTEM
	}
	
	public static final String PRIVATE_DIR = "private";
	public static final String SHARED_DIR = "shared";
	public static final String DASHBOARDS = "Dashboards";
	public static final String DASHBOARD_MARKER_FILE = "dashboard.xml";
	
	private static Logger logger = Logger.getLogger(CatalogManager.class);
	private static final String SLASH_REGEX = "\\" + File.separator;

	private static final String UPLOAD_PROHIBITED = "User %s does not have sufficient privileges to upload %s filetypes.";
	private static final String DOWNLOAD_PROHIBITED = "User %s does not have sufficient privileges to download %s filetypes.";
	private static final String MODIFY_PERMISSIONS_REQUIRED = "Must have 'modify' permissions to perform this request.";

	/**
	 * Allowed file types in the Birst user's catalog file system.
	 * PDFs can be downloaded but not uploaded.  All others can be uploaded or downloaded.
	 */
	private static final String[] ALLOWED_EXTENSIONS = {
		"PNG",
		"GIF",
		"JPG",
		"PDF",
		"ADHOCREPORT",
		"DASHBOARD",
		"PAGE",
		"JRXML",
		"DASHLET",
		"VIZ"
	};
	
	private static final String[] ADMIN_UPLOAD_DOWNLOAD_FILETYPES = {
		"ADHOCREPORT", 
		"PAGE",
		"JRXML",
		"DASHLET"
	};
	
	private static final String[] INTERNAL_DIRECTORIES = {
		".bookmarks",
	};
	
	private static final String COPY_OF = "Copy of ";
	
	private File catalogDirectory;
	private String catalogDirectoryPath;
	private boolean bypassServletContext;
	private File[] testRootDirs;
	
	public CatalogManager(String catalogDirectory) {
		// Root directory should be in canonical form
		catalogDirectory = com.successmetricsinc.util.FileUtils.normalizePath(catalogDirectory);
		this.catalogDirectory = new File(catalogDirectory);
	}
	
	public CatalogManager(String catalogDirectory, boolean writeable) {
		this(catalogDirectory);
	}
	
	/**
	 * Handles the renaming of files or directories.  Assumes that the target and source
	 * directories are the same.  In other words there is no overlap with the move functionality.
	 * <b>Note:</b> this method accepts either relative or absolute file paths.
	 * @param newName is the new file/directory name.
	 * @param originalFilePath is the full file path of the file/directory that is to be renamed.
	 * @return an empty errorMessage String if the operation was successful. A non-zero length
	 * errorMessage String if the processing encountered an error.
	 * @throws CatalogAccessException if the permissions on the target folder are insufficient.
	 */
	public String renameFile(String newName, String _originalFilePath, List<Group> groupList, boolean isSuperUserOwnerOrAdmin) throws CatalogAccessException {
		String errorMessage = "";
		
		// Normalize path
		String originalFilePath = getNormalizedPath(_originalFilePath);
		if (originalFilePath == null) {
			throw new IllegalArgumentException("'renameFile()' received an invalid original file path");
		}
		
		if (!(errorMessage = validateFilePathInput(originalFilePath)).equals("")) {
			return errorMessage;
		}
		
		final File originalFile = new File(originalFilePath);
		if (!(errorMessage = validateNewFileName(newName, originalFile.isFile())).equals("")) {
			return errorMessage;
		}
		final File newFile = new File(originalFile.getParent() + File.separator + newName);
		// Is rename permitted?
		CatalogPermission oldPermission;
		if (originalFile.isDirectory()) {
			oldPermission = getPermission(originalFile.getAbsolutePath());
		} else {
			oldPermission = getPermission(originalFile.getParent());
		}
		if (oldPermission.canModify(groupList, isSuperUserOwnerOrAdmin) == false) {
			throw new CatalogAccessException(CatalogAccessException.Reason.NoPermissions);
		}
		// Do the rename
		final boolean renameSuccessful = originalFile.renameTo(newFile);
		
		if (renameSuccessful == true) {
			// Rename related files (.Dashboard ==> .smijasper or .AdhocReport or .jrxml ==> .jasper)
			if (newFile.isFile()) {
				String lower = originalFile.getName().toLowerCase();
				String relatedSuffix = null;
				if (lower.endsWith(".adhocreport") || lower.endsWith(".jrxml")) {
					relatedSuffix = com.successmetricsinc.util.FileUtils.COMPILED_REPORT_EXTENSION;
				}
				if (relatedSuffix != null) {
					// Derive compiled file
					String originalFileName = originalFile.getName();
					int index1 = originalFileName.lastIndexOf('.');
					relatedSuffix = com.successmetricsinc.util.FileUtils.COMPILED_REPORT_EXTENSION;
					String compiledFileName = originalFileName.substring(0, index1).concat(relatedSuffix);
					
					File compiledFile = new File(originalFile.getParent(), compiledFileName);
					if (!compiledFile.exists()) {
						// try old suffix
						relatedSuffix = com.successmetricsinc.util.FileUtils.OLD_COMPILED_REPORT_EXTENSION;
						compiledFileName = originalFileName.substring(0, index1).concat(relatedSuffix);
						compiledFile = new File(originalFile.getParent(), compiledFileName);
					}
					if (compiledFile.exists()) {
						// Derive new compiled file
						String newFileName = newFile.getName();
						int index2 = newFileName.lastIndexOf('.');
						String compiledNewFileName = newFile.getName().substring(0, index2).concat(relatedSuffix);
						File relatedNewFile = new File(newFile.getParent() + File.separator + compiledNewFileName);
						// Rename
						compiledFile.renameTo(relatedNewFile);
					}
				}
			}
		} else {
			errorMessage = "There was a problem renaming the file: " + originalFile.getName() + " to: " + newFile.getName();
		}
		return errorMessage;
	}

	/**
	 * Moves or copies a file or directory to the designated target directory.
	 * Integrates file and directory move and copy operations to reduce code at the
	 * expense of a little extra complexity.
	 * <b>Note:</b> this method accepts either relative or absolute file paths.
	 * @param action can be MOVE or COPY.
	 * @param sourcePath is the full filepath of the source file or directory to be moved or copied.
	 * @param targetDirectoryPath is the full file path of the target directory.
	 * @return an errorMessage which is empty ("") if there were no problems with the file operation;
	 * or contains a descriptive error message otherwise.
	 * @throws CatalogAccessException if the permissions are not acceptable for this user.
	 */
	public String moveCopyFile(ACTION action, String _sourcePath, String _targetDirectoryPath, List<Group> groupList, boolean isSuperUserOwnerOrAdmin) 
			throws CatalogAccessException {
		// Normalize paths
		String sourcePath = getNormalizedPath(_sourcePath);
		if (sourcePath == null) {
			throw new IllegalArgumentException("'moveCopyFile()' could not normalize the source path");
		}
		String targetDirectoryPath = getNormalizedPath(_targetDirectoryPath);
		if (targetDirectoryPath == null) {
			throw new IllegalArgumentException("'moveCopyFile()' could not normalize the target directory path");
		}
		
		String errorMessage = "";
		// Validate target directory
		if (!(errorMessage = validateFilePathInput(targetDirectoryPath)).equals("")) {
			return errorMessage;
		}
		// Validate source file or directory
		if (!(errorMessage = validateFilePathInput(sourcePath)).equals("")) {
			return errorMessage;
		}
		// Observe and report
		logger.debug("Requesting a " + action + " of: " + sourcePath + " to: " + targetDirectoryPath);
		// Lock and load
		final File source = new File(sourcePath);
		final File targetDirectory = new File(targetDirectoryPath);
		// Validate move copy combination
		if (!(errorMessage = validateMoveCopyOperation(action, source, targetDirectory)).equals("")) {
			return errorMessage;
		}
		// Is copy/move permitted? (Throws CatalogAccessException if not)
		checkMoveCopyPermissions(action, source, targetDirectory, groupList, isSuperUserOwnerOrAdmin);
		// Do the copy or move
		try {
			if (source.isDirectory()) {
				copyDirectory(source, targetDirectory);
			} else {
				copyFile(source, targetDirectory);
			}
			if (action.equals(ACTION.MOVE)) {
				deleteFile(sourcePath, groupList, isSuperUserOwnerOrAdmin);
			}
		} catch (IOException e) {
			errorMessage = "File or folder could not be moved/copied.";
			logger.error(errorMessage);
			logger.error("ACTION: " + action + "; source: " + sourcePath + "; target: " + targetDirectoryPath, e);
		}
		return errorMessage;
	}

	/**
	 * Copy a directory to a directory.
	 * <p/>
	 * If the source folder's parent directory is the same as the target directory
	 * make sure that the source folder is renamed to "Copy of ". 
	 * @param source is the directory to copy.
	 * @param targetDirectory is the directory to copy to.
	 * @throws IOException if there is a problem creating, copying or moving files.
	 */
	private void copyDirectory(final File source, final File targetDirectory)
			throws IOException {
		// Create "Copy of " if copying to the same directory
		final String sourceParentPath = source.getParent();
		final String targetPath = targetDirectory.getCanonicalPath();
		if (sourceParentPath.equals(targetPath)) {
			// Copy to temp, rename, then move to target
			final File tmpDir = com.successmetricsinc.util.FileUtils.createTmpSubdir();
			FileUtils.copyDirectoryToDirectory(source, tmpDir);
			final File newDirCopy = new File(tmpDir, source.getName());
			final File copyOfDirectory = new File(tmpDir, COPY_OF + source.getName());
			newDirCopy.renameTo(copyOfDirectory);
			FileUtils.moveDirectoryToDirectory(copyOfDirectory, targetDirectory, false);
		} else {
			// Otherwise just do the regular copy
			FileUtils.copyDirectoryToDirectory(source, targetDirectory);
		}
	}

	/**
	 * Copies a file to the specified directory.
	 * <p/>
	 * If the source file's parent folder is identical to the target directory
	 * or if there is already a target file by that name, rename the copied file
	 * to the "Copy of " variant during the copy.
	 * Otherwise, just perform a normal copy.
	 * <p/>
	 * If there is a related file, make sure that that file is copied too,
	 * including any "Copy of " renaming.
	 * @param source is the file to copy.
	 * @param targetDirectory is the target directory.
	 * @throws IOException if there is a problem creating, copying or moving files.
	 */
	private void copyFile(final File source, final File targetDirectory)
			throws IOException {
		// Is this copy happening in the same folder?
		final String sourceParentPath = source.getParent();
		// Or is there already a target file with that name?
		final File targetFile = new File(targetDirectory, source.getName());
		// Create "Copy of " version of target file?
		if (sourceParentPath.equals(targetDirectory.getCanonicalPath()) || targetFile.exists()) {
			// Make a copy of the file
			final File fileCopy = new File(targetDirectory, COPY_OF + source.getName());
			FileUtils.copyFile(source, fileCopy, true);
		} else {
			FileUtils.copyFileToDirectory(source, targetDirectory);
		}
	}
	
	/**
	 * Checks to see that a valid operation has been requested.  Returns an error message if not.
	 * Basically, the invalid combinations are:
	 * <ul>
	 * <li>Cannot copy or move a folder to itself</li>
	 * </ul>
	 * @param action can be MOVE or COPY.
	 * @param source is the source file or directory to be moved or copied.
	 * @param targetDirectory is the target directory.
	 * @return an error message if there is a problem; otherwise returns an empty String.
	 */
	private String validateMoveCopyOperation(final ACTION action, final File source, final File targetDirectory) {
		final String sourcePath = source.getAbsolutePath();
		final String sourceParentPath = source.getParent();
		final String targetPath = targetDirectory.getAbsolutePath();
		if (!targetDirectory.isDirectory()) {
			return "Target of a move/copy operation must be a folder.";
		}
		if (source.isDirectory()) {
			if (sourcePath.equals(targetPath)) {
				return "Cannot move or copy a folder to itself.";
			}
			if (targetPath.indexOf(sourcePath) == 0 && sourcePath.length() < targetPath.length()) {
				return "Cannot move or copy a folder into one of its children.";
			}
			if (action.equals(ACTION.MOVE) && sourceParentPath.equals(targetPath)) {
				return "Cannot move a folder within the same parent folder.";
			}
		}
		return "";
	}
	
	/**
	 * Checks the permissions on the move or copy operation.
	 * @param action can be MOVE or COPY.
	 * @param source is the source file or directory to be moved or copied.
	 * @param targetDirectory is the target directory.
	 * @throws CatalogAccessException if the permissions are not acceptable for this user.
	 */
	private void checkMoveCopyPermissions(final ACTION action, final File source, final File targetDirectory, List<Group> groupList, boolean isSuperUserOwnerOrAdmin) 
			throws CatalogAccessException {
		CatalogPermission sourcePermission;
		if (source.isDirectory()) {
			sourcePermission = getPermission(source.getAbsolutePath());
		} else {
			sourcePermission = getPermission(source.getParent());
		}
		CatalogPermission targetPermission;
		if (targetDirectory.isDirectory()) {
			targetPermission = getPermission(targetDirectory.getAbsolutePath());
		} else {
			targetPermission = getPermission(targetDirectory.getParent());
		}
		// Move modifies the source so check its permission for moves
		if (action.equals(ACTION.MOVE)) {
			if (!sourcePermission.canModify(groupList, isSuperUserOwnerOrAdmin)) {
				throw new CatalogAccessException(CatalogAccessException.Reason.NoPermissions);
			}
		}
		// Always modifies the target, so always check the target
		if (!targetPermission.canModify(groupList, isSuperUserOwnerOrAdmin)) {
			throw new CatalogAccessException(CatalogAccessException.Reason.NoPermissions);
		}
	}
	
	/**
	 * Handles the deletion of files and directories.  If the file represents a pair of files (i.e., 
	 * .AdhocReport or .Dashboard) then the related, compiled file is also deleted.  If the deletion
	 * of the compiled file fails, the delete operation will still be considered a success.
	 * <b>Note:</b> this method accepts either relative or absolute file paths.
	 * @param filePath is the relative or full filepath of the file/directory that is to be deleted.
	 * @return an empty errorMessage String if the operation was successful. A non-zero length
	 * errorMessage String if the processing encountered an error.
	 * @throws CatalogAccessException if the permissions on the target folder are insufficient.
	 */
	public String deleteFile(String _filePath, List<Group> groupList, boolean isSuperUserOwnerOrAdmin) throws CatalogAccessException {
		String filePath = getNormalizedPath(_filePath);
		if (filePath == null) {
			throw new IllegalArgumentException("'deleteFile()' could not normalize the file path");
		}
		// Does file exist?
		String errorMessage = "";
		if (!(errorMessage = validateFilePathInput(filePath)).equals("")) {
			return errorMessage;
		}
		
		final File fileToDelete = new File(filePath);
		// Is rename permitted? Adequate permissions and not restricted folders
		CatalogPermission permission;
		boolean isDirectory = fileToDelete.isDirectory();
		if (isDirectory) {
			// Deal breaker if folder is restricted (i.e., 'shared', 'private',...)
			if (!(errorMessage = isRestrictedFolder(filePath)).equals("")) {
				logger.error(errorMessage);
				logger.error("...attempted by: " + SMIWebUserBean.getUserBean().getUserName());
				return errorMessage;
			} else {
				permission = getPermission(fileToDelete.getAbsolutePath());
			}
		} else {
			permission = getPermission(fileToDelete.getParent());
		}
		if (permission.canModify(groupList, isSuperUserOwnerOrAdmin) == false) {
			throw new CatalogAccessException(CatalogAccessException.Reason.NoPermissions);
		}
		// Do the rename
		if (isDirectory) {
			try {
				FileUtils.deleteDirectory(fileToDelete);
			} catch (IOException e) {
				errorMessage = "Could not delete directory: " + fileToDelete.getName(); 
				logger.error(errorMessage, e);
				return errorMessage;
			}
		} else {
			boolean deleteEmptyDashboardDir = false;
			File parent = null;
			//Check do we delete empty dashboards directory
			if(_filePath.toLowerCase().endsWith(".page")){
				parent = fileToDelete.getParentFile();
				if(parent.isDirectory()){
					File[] fs = parent.listFiles();
					if(fs.length == 2){
						for (File f : fs){
							if(f.getName().toLowerCase().equals(DASHBOARD_MARKER_FILE)){
								f.delete();
								deleteEmptyDashboardDir = true;
							}
						}
					}
				}
			}
			
			if (!fileToDelete.delete()) {
				errorMessage = "Could not delete file: " + fileToDelete.getName();
				logger.error(errorMessage);
				return errorMessage;
			}else{
				//Delete empty dashboard directory
				if(deleteEmptyDashboardDir && parent != null){
					try {
						FileUtils.deleteDirectory(parent);
						return "Empty dashboard deleted";
					} catch (IOException e) {
						errorMessage = "Could not delete directory: " + parent.getName(); 
						logger.error(errorMessage, e);
						return errorMessage;
					}
				}
			}
		}

		return errorMessage;
	}

	/**
	 * This method deletes the following files from the specified 'dashboardFolderPath' directory:
	 * <ul>
	 * <li>*.page
	 * <li>dashboard.xml
	 * </ul>
	 * This method is used when the dashboard directory will be used for other purposes (e.g., 
	 * the user has decided to save reports (.page) files in it).
	 * If after deleting those files the directory is empty, the directory will be deleted.
	 * <p/>
	 * If any of the files in the directory cannot be deleted, the method returns an 'errorMessage'
	 * containing the problematic files.
	 * If the directory cannot be deleted, the method will return an 'errorMessage'.
	 * <b>Note:</b> this method accepts either relative or absolute file paths.
	 * @param dashboardFolderPath is the directory path that should be deactivated.
	 * @return an empty String if the request was successful; descriptive error messages if there
	 * were problems with the request.
	 * @throws CatalogAccessException if the user does not have 'modify' permissions on this folder.
	 */
	public String deactivateDashboardFolder(String _dashboardFolderPath, List<Group> groupList, boolean isSuperUserOwnerOrAdmin) 
			throws CatalogAccessException {
		if (logger.isTraceEnabled())
			logger.trace("'deactivateDashboardFolder()' for: " + _dashboardFolderPath);
		// Normalize path
		String dashboardFolderPath = getNormalizedPath(_dashboardFolderPath);
		if (dashboardFolderPath == null) {
			throw new IllegalArgumentException("'deactivateDashboardFolder()' received an invalid original file path");
		}
		StringBuilder errorMessage = new StringBuilder();
		// Validate existence of path
		errorMessage.append(validateFilePathInput(dashboardFolderPath));
		if (!errorMessage.toString().equals("")) {
			logger.warn("'deactivateDashboardFolder()' 'dashboardFolderPath' had validation error: " + errorMessage.toString());
			return errorMessage.toString();
		}
		// Check that user has 'modify' access to this folder
		final CatalogPermission catalogPermission = getPermission(dashboardFolderPath);
		if (!catalogPermission.canModify(groupList, isSuperUserOwnerOrAdmin)) {
			logger.warn("'deactivateDashboardFolder()': " + MODIFY_PERMISSIONS_REQUIRED); 
			logger.warn("'deactivateDashboardFolder()' permissions for: " + dashboardFolderPath 
					+ "\n" + catalogPermission.toString());
			throw new CatalogAccessException(MODIFY_PERMISSIONS_REQUIRED, CatalogAccessException.Reason.NoPermissions);
		}
		// Delete any *.page & dashboard.xml files
		final File dashboardFolder = new File(dashboardFolderPath);
		final File[] files = dashboardFolder.listFiles(DeactivateDashboardFilenameFilter.getInstance());
		for (File file : files) {
			if (!file.delete()) {
				errorMessage.append("Could not delete: " + file.getName()).append("\n");
			}
			if (errorMessage.length() > 0) {
				logger.warn("'deactivateDashboardFolder()': " + errorMessage.toString());
			}
		}
		// If the directory is now empty, delete it
		if (errorMessage.toString().equals("")) {
		    // Directory must be empty in order for the delete() to succeed
			if (dashboardFolder.delete()) {
				logger.debug("directory was empty, so deleted it: " + dashboardFolderPath);
			} else {
				// Don't issue an error message if the folder couldn't be deleted.
				logger.warn("'deleteDirectory()' dashboard files deleted but could not delete folder: " + dashboardFolderPath + ". There are probably other files in the folder.");
			}
		}
		// Clip off last \n
		if (!errorMessage.toString().equals("") 
				&& errorMessage.lastIndexOf("\n") == errorMessage.length() - 1) {
			errorMessage.substring(errorMessage.length() - 1);
		}
		return errorMessage.toString();
	}
	
	/**
	 * Takes the byte array contents of an uploaded file and writes it to the catalog area. 
	 * <b>Note:</b> this method accepts either relative or absolute file paths.
	 * @param uploadFolder is the directory where the file will be written.
	 * @param filename is the filename under which the file contents will be stored.
	 * @param decodedContents is the contents of the file to be written.
	 * @param isSuperUserOwnerOrAdmin 
	 * @param groupList 
	 * @return an error message, which is an empty string if there were no errors.
	 * @throws CatalogAccessException if the user does not have sufficient permissions.
	 */
	public String writeFile(final String uploadFolder, final String filename, final byte[] decodedContents, List<Group> groupList, boolean isSuperUserOwnerOrAdmin) 
			throws CatalogAccessException {
		String errorMessage = "";
		// Get the full file path
		final String normalizedUploadFolder;
		if ((normalizedUploadFolder = getNormalizedPath(uploadFolder)) == null) {
			throw new IllegalArgumentException("Could not normalize the target upload folder");
		}
		final File uploadFile = new File(normalizedUploadFolder + File.separator + filename);
		// Only an admin user can download .page, .AdhocReport and .jrxml files
		final String fileExtension = com.successmetricsinc.util.FileUtils.getFileExtension(filename).toUpperCase();
		if (Arrays.asList(ADMIN_UPLOAD_DOWNLOAD_FILETYPES).contains(fileExtension)) {
			final SMIWebUserBean userBean = SMIWebUserBean.getUserBean();
			if (!userBean.isSuperUser()) {
				throw new CatalogAccessException("Sorry. You must be an \"Admin\"-level user to upload .jrxml, .AdhocReport, .Dashlet or .page files.",
				                                 CatalogAccessException.Reason.Error);
			}
		}
		// Check permissions
		CatalogPermission permissions = getPermission(normalizedUploadFolder);
		if (!permissions.canModify(groupList, isSuperUserOwnerOrAdmin)) {
			throw new CatalogAccessException(String.format(UPLOAD_PROHIBITED, SMIWebUserBean.getUserBean().getUser(), fileExtension),
					                         CatalogAccessException.Reason.NoPermissions);
		}
		// Write the file
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(uploadFile);
			fos.write(decodedContents);
			fos.flush();
		} catch (FileNotFoundException e) {
			errorMessage = "Problem creating file: " + e.getMessage();
		} catch (IOException e) {
			errorMessage = "Could not write file: " + e.getMessage();
		}
		finally
		{
			try
			{
				if (fos != null)
					fos.close();
			}
			catch (IOException e)
			{
				logger.debug(e, e);
			}
		}
		return errorMessage;
	}

	/**
	 * Reads and returns the byte array contents of the requested file.
	 * <b>Note:</b> this method accepts either relative or absolute file paths.
	 * @param filepath is the relative or absolute file path of the file to be read.
	 * @return the byte array contents of the requested file.
	 * @throws IOException if there is problem reading the file.
	 * @throws CatalogAccessException if there is a problem with file permissions.
	 */
	public byte[] readFile(final String filepath) throws IOException, CatalogAccessException {
		// Normalize the filepath
		String downloadFilePath;
		if ((downloadFilePath = getNormalizedPath(filepath)) == null) {
			throw new IllegalArgumentException("'readFile()' cannot normalize the filepath");
		}
		// Check for its existence
		final File downloadFile = new File(downloadFilePath);
		if (!downloadFile.exists()) {
			throw new IllegalArgumentException("'readFile()' - filepath does not exist");
		}
		final String fileExtension = com.successmetricsinc.util.FileUtils.getFileExtension(downloadFilePath).toUpperCase();
		final SMIWebUserBean userBean = SMIWebUserBean.getUserBean();	
		// Get containing folder of download file
		final String folder = com.successmetricsinc.util.FileUtils.getFileParent(downloadFile.getAbsolutePath());
		// Check permissions
		CatalogPermission permissions = getPermission(folder);
		if (permissions.canView(userBean.getUserGroups(), userBean.isSuperUserOwnerOrAdmin()) == false) {
			throw new CatalogAccessException(String.format(DOWNLOAD_PROHIBITED, SMIWebUserBean.getUserBean().getUser(), fileExtension),
					                         CatalogAccessException.Reason.NoPermissions);
		}

		// Read the file contents and return contents 
		byte[] byteArray = new byte[(int) downloadFile.length()];
		if (downloadFile.exists())
		{
			FileInputStream fis = null;
			try
			{
				fis = new FileInputStream(downloadFile);
				int bytesRead = fis.read(byteArray, 0, (int) downloadFile.length());
				if (bytesRead != downloadFile.length()) {
					logger.error("File length read - " + bytesRead + " does not match file handle: " + downloadFile.length());
				}
			}
			finally
			{
				if (fis != null)
					fis.close();
			}
		}
		return byteArray;
	}

	/**
	 * A convenience/bridge method that lets pre-4.0 clients call the old method.
	 * Basically forwards the request to {@link #createDirectory(String, String)}.  
	 * @param folderPath is the full folder path including the new folder. 
	 * @return true if created successfully; false otherwise.
	 * @throws CatalogAccessException if the caller has insufficient authority.
	 */
	public boolean createFolder(String _folderPath) throws CatalogAccessException {
		String folderPath = getNormalizedPath(_folderPath);
		if (folderPath == null) {
			throw new IllegalArgumentException("'createFolder()' cannot normalize the filepath");
		}
		SMIWebUserBean ub = SMIWebUserBean.getUserBean();
		String parentFolder = com.successmetricsinc.util.FileUtils.getFileParent(folderPath);
		final String newFolder = com.successmetricsinc.util.FileUtils.getFileLeaf(folderPath);
		if (parentFolder != null && newFolder != null) {
			String errorMessage = createDirectory(newFolder, parentFolder, ub.getUserGroups(), ub.isSuperUserOwnerOrAdmin());
			if (errorMessage.equals("")) {
				return true;
			} else {
				return false;
			}
		} else {
			logger.error("'createFolder()' was passed an invalid 'folderPath': " + folderPath);
			return false;
		}
	}
	
	/**
	 * Handles the creation of subdirectories.
	 * <b>Note:</b> this method accepts either relative or absolute file paths.
	 * @param dirName is the new subdirectory name.
	 * @param fullFilePath is the full filepath of the directory in which the new subdirectory will
	 * be created.
	 * @return an empty errorMessage String if the operation was successful. A non-zero length
	 * errorMessage String if the processing encountered an error.
	 * @throws CatalogAccessException if the permissions on the target folder are insufficient.
	 */
	public String createDirectory(String dirName, String _fullFilePath, List<Group> groupList, boolean isSuperUserOwnerOrAdmin) throws CatalogAccessException {
		if (_fullFilePath == null || _fullFilePath.trim().length() == 0) {
			return "Invalid file path: " + _fullFilePath;
		}
		String errorMessage = "";
		// Normalize path
		String fullFilePath = getNormalizedPath(_fullFilePath);
		if (fullFilePath == null) {
			throw new IllegalArgumentException("'createDirectory()' received an invalid original file path");
		}
		if (!(new File(fullFilePath).isDirectory())) {
			errorMessage = "Parent folder must be a folder, not a file.";
			return errorMessage;
		}
		final File parentDirectory = new File(fullFilePath);
		final File newSubdirectory = new File(parentDirectory, dirName);
		
		// if already exist, just return
		if (newSubdirectory.isDirectory())
			return errorMessage;
		
		if (!(errorMessage = validateNewFolderName(dirName, fullFilePath)).equals("")) {
			return errorMessage;
		}
		// Is rename permitted?
		CatalogPermission permission = getPermission(parentDirectory.getAbsolutePath());
		if (permission != null && permission.canModify(groupList, isSuperUserOwnerOrAdmin) == false) {
			throw new CatalogAccessException(CatalogAccessException.Reason.NoPermissions);
		}
		// Create the directory		
		final boolean createSuccessful = newSubdirectory.mkdirs();
		// Cache the permission
		if (createSuccessful == false) {
			errorMessage = "There was a problem creating the new folder: " + newSubdirectory.getName();
		}
		return errorMessage;
	}

	public String createNewDirectory(final String _fullFilePath, List<Group> groupList, boolean isSuperUserOwnerOrAdmin) throws CatalogAccessException {
		if (_fullFilePath == null || _fullFilePath.trim().length() == 0) {
			throw new IllegalArgumentException("Invalid file path: " + _fullFilePath);
		}
		String errorMessage = "";
		// Normalize path
		String fullFilePath = getNormalizedPath(_fullFilePath);
		if (fullFilePath == null) {
			throw new IllegalArgumentException("'createDirectory()' received an invalid original file path");
		}
		if (!(new File(fullFilePath).isDirectory())) {
			throw new IllegalArgumentException("Parent folder must be a folder, not a file.");
		}
		final File parentDirectory = new File(fullFilePath);
		
		String dirName = NEW_FOLDER_NAME;
		File newSubdirectory = null;
		int i = 2;
		for (; i < MAX_NUMBER_TEMP_DIR_NAMES; i++) {
			newSubdirectory = new File(parentDirectory, dirName);
			
			// if already exist, just return
			if (! newSubdirectory.exists() )
				break;
			
			dirName = NEW_FOLDER_NAME + " (" + i + ")";
		}
		
		if (i == MAX_NUMBER_TEMP_DIR_NAMES) {
			throw new CatalogAccessException("Too many temporary directories already exist in " + _fullFilePath, CatalogAccessException.Reason.Error);
		}
		
		if (!(errorMessage = validateNewFolderName(dirName, fullFilePath)).equals("")) {
			throw new CatalogAccessException(errorMessage, Reason.Error);
		}
		// Is rename permitted?
		CatalogPermission permission = getPermission(parentDirectory.getAbsolutePath());
		if (permission != null && permission.canModify(groupList, isSuperUserOwnerOrAdmin) == false) {
			throw new CatalogAccessException(CatalogAccessException.Reason.NoPermissions);
		}
		// Create the directory		
		final boolean createSuccessful = newSubdirectory.mkdirs();
		// Cache the permission
		if (createSuccessful == false) {
			throw new CatalogAccessException("There was a problem creating the new folder: " + newSubdirectory.getName(), Reason.Error);
		}
		return dirName;
	}

	/**
	 * Main method for non-"Manage Report Catalog" (Catalog Management) users.
	 * Calls the full method: {@link #getPermission(String, boolean)} and applies
	 * the parental overrides using the current user's group membership.
	 * See the full method for more information.
	 * <p/>
	 * <b>Note:</b> this method accepts either relative or absolute file paths.
	 * @param folderPath is the folder path to examine for defaulted or overridden
	 * group permissions.
	 * @return a fully-populated CatalogPermission instance including all the current
	 * folder's parents' permissions.
	 */
	public CatalogPermission getPermission(String folderPath) {
		return getPermission(folderPath, false);
	}
	
	/**
	 * Gets the complete tree of permissions for a specified folder path.  
	 * Recurses up the tree until the 'root' folder returns "all" permissions.
	 * Then all subsequent children are built, specifying the preceding parent
	 * in the tree.  If a 'permission.xml' file is encountered in any of the 
	 * children folders, those permissions are read in and form the contents
	 * of the groups lists for each permission type (i.e., View, Modify,...)
	 * <p/>
	 * After retrieving the entire tree of permissions, a final filter is performed --
	 * {@link CatalogPermission#applyParentalPermissionsOverrides()} -- that walks up
	 * the newly created permission instance and removes less-restrictive permissions 
	 * (i.e., true) for a group type (e.g., USER$/modify) if there is a more restrictive 
	 * permission (i.e., false) above it in the tree.  For example, in the following 
	 * example the final 'true' is overridden by the 'false' entry above it.  
	 * (The '-' indicates that that level of the directories had no permission.xml override file.)
 	 * <table>
 	 * <tr><th width="67%"></th><th>USER$/Modify</th></tr>                                                    
	 * <tr><td>/ (root) - all permissions</td><td>true</td>
	 * <tr><td>&nbsp;&nbsp;/a</td><td>-</td>
	 * <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;/b</td><td>false</td>
	 * <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/c</td><td>-</td>
	 * <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/d</td><td>true</td>
	 * </table>           
	 * This scenario can occur because, although there are edits on
     * {@link CatalogPermission#savePermissions(String)} to disallow the creation of the 'd'
     * directory's 'true' entry, the 'b' directory's overrides can be created <b>after</b> the
     * 'd' directory's overrides.  In that case, navigating the all the subdirectory trees to
     * avoid (or correct!) this occurrence during the 'b' directory's permissions save processing
     * would be overly complex and can be more easily handled with the final filtering process
     * provided by {@link CatalogPermission#applyParentalPermissionsOverrides()}.
	 * <p/>
	 * <b>Note:</b> this method accepts either relative or absolute file paths.
	 * @param folderPath is the folder path to examine for defaulted or overridden
	 * group permissions.
	 * @return a fully-populated CatalogPermission instance including all the current
	 * folder's parents' permissions.
	 */
	public CatalogPermission getPermission(String _folderPath,
			                               final boolean spaceAdminUser) {
		logger.trace("'getPermission()' for 'folderPath': " + _folderPath);
		// Normalize 'folderPath' to canonical path
		String folderPath = getNormalizedPath(_folderPath);
		if (folderPath == null) {
			throw new IllegalArgumentException("'getPermission()' couldn't normalize path");
		}
		// Validate existence of path
		if (!new File(folderPath).exists()) {
			throw new IllegalArgumentException("'getPermission()' encountered non-existent path");
		}
		// Are folder's permissions already cached?
			// Get parent folder's permissions via recursive call
		final CatalogPermission permission = new CatalogPermission(getCatalogRootPath(), folderPath);
		// Observe and report
		logger.trace(permission.toString());
		
		return permission;
	}
	
	public List<String> getViewFolders(String parentPath, List<Group> groupList, boolean isSuperUserOwnerOrAdmin) {
		if (parentPath == null) {
			parentPath = this.catalogDirectory.getAbsolutePath();
		}
		File parent = new File(parentPath);
		if (parent.isDirectory() == false) {
			return null;
		}
		if (getPermission(parentPath).canView(groupList, isSuperUserOwnerOrAdmin) == false) {
			return null;
		}
		List<String> ret = new ArrayList<String>();
		File[] children = parent.listFiles(BirstOnlyFilenameFilter.getInstance());
		for (File child : children) {
			if (child.isDirectory() == false) {
				continue;
			}
			if (getPermission(child.getAbsolutePath()).canView(groupList, isSuperUserOwnerOrAdmin) == false) {
				continue;
			}
			ret.add(child.getAbsolutePath());
		}
		return ret;
	}

	public List<File> getFolderFiles(String parentPath) {
		if (parentPath == null || parentPath.equals("") || parentPath.equals("/")) {
			parentPath = catalogDirectory.getAbsolutePath();
		}
		final File parent = new File(parentPath);
		if (!parent.isDirectory()) {
			return null;
		}
		// Iterate through folder entries and add directories
		final List<File> directories = new ArrayList<File>();
		for (File child : parent.listFiles(BirstOnlyFilenameFilter.getInstance())) {
			if (child.isDirectory()) {
				directories.add(child);
			}
		}
		return directories;
	}

	/**
	 * Persists the newly updated permissions.
	 * <p/>
	 * The 'permissions' String is actually the Flex XML representation of the permissions
	 * Model.  For example, it has a structure like:<br/>
	 * <code>
	 * &lt;Group&gt;<br/>
	 * &nbsp;&nbsp;&lt;Name&gt;Administrators&lt;/Name&gt;<br/>
     * &nbsp;&nbsp;&lt;View&gt;true&lt;/View&gt;<br/>
     * &nbsp;&nbsp;&lt;Modify&gt;true&lt;/Modify&gt;<br/>
     * &nbsp;&nbsp;&lt;Upload&gt;true&lt;/Upload&gt;<br/>
     * &nbsp;&nbsp;&lt;Download&gt;true&lt;/Download&gt;<br/>
     * &nbsp;&nbsp;&lt;Admin&gt;true&lt;/Admin&gt;<br/>
     * &lt;/Group&gt;<br/>
	 * &lt;Group&gt;<br/>
	 * &nbsp;&nbsp;&lt;Name&gt;OWNER$&lt;/Name&gt;<br/>
     * &nbsp;&nbsp;&lt;View&gt;true&lt;/View&gt;<br/>
     * &nbsp;&nbsp;&lt;Modify&gt;true&lt;/Modify&gt;<br/>
     * &nbsp;&nbsp;&lt;Upload&gt;true&lt;/Upload&gt;<br/>
     * &nbsp;&nbsp;&lt;Download&gt;true&lt;/Download&gt;<br/>
     * &nbsp;&nbsp;&lt;Admin&gt;true&lt;/Admin&gt;<br/>
     * &lt;/Group&gt;<br/>
	 * &lt;Group&gt;<br/>
	 * &nbsp;&nbsp;&lt;Name&gt;USER$&lt;/Name&gt;<br/>
     * &nbsp;&nbsp;&lt;View&gt;true&lt;/View&gt;<br/>
     * &nbsp;&nbsp;&lt;Modify&gt;true&lt;/Modify&gt;<br/>
     * &nbsp;&nbsp;&lt;Upload&gt;true&lt;/Upload&gt;<br/>
     * &nbsp;&nbsp;&lt;Download&gt;true&lt;/Download&gt;<br/>
     * &nbsp;&nbsp;&lt;Admin&gt;true&lt;/Admin&gt;<br/>
     * &lt;/Group&gt;<br/>
	 * &lt;Group&gt;<br/>
	 * &nbsp;&nbsp;&lt;Name&gt;GroupA&lt;/Name&gt;<br/>
     * &nbsp;&nbsp;&lt;View&gt;false&lt;/View&gt;<br/>
     * &nbsp;&nbsp;&lt;Modify&gt;true&lt;/Modify&gt;<br/>
     * &nbsp;&nbsp;&lt;Upload&gt;false&lt;/Upload&gt;<br/>
     * &nbsp;&nbsp;&lt;Download&gt;false&lt;/Download&gt;<br/>
     * &nbsp;&nbsp;&lt;Admin&gt;true&lt;/Admin&gt;<br/>
     * &lt;/Group&gt;
     * </code>
     * <p/>
	 * (Although they appear in the XML, the 'Administrators' and 'OWNER$' groups are not 
	 * displayed and cannot be updated by the user.  Likewise, the 'Admin' permission is
	 * not displayed and cannot be updated by the user.)
	 * <p/>
	 * Please note that although these permissions are received by groups, they are stored 
	 * by permission type (i.e., View, Modify, Upload, Download).  See the data store:
	 * 'groupMap' in {@link CatalogPermission} for the permissions format.
	 * <b>Note:</b> this method accepts either relative or absolute file paths.
	 * @param folderPath is the relative or full file path for the folder.
	 * If it's relative then the parameter will be modified to reflect the full path. 
	 * @param permissions is the string-ified XML of the folder's new permissions.
	 */
	public void savePermissions(String _folderPath, final String permissions) {
		if (logger.isTraceEnabled())
		{
			logger.trace("'savePermissions()' entered with 'folderPath':  " + _folderPath);
			logger.trace("'savePermissions()' entered with 'permissions': " + permissions);
		}
		// Normalize folder path and allocate file
		String folderPath = getNormalizedPath(_folderPath);
		if (folderPath == null) {
			throw new IllegalArgumentException("'savePermissions()' could not normalize 'folderPath'");
		}
		File folder = new File(folderPath);
		// Validate existence of path
		if (!folder.exists()) {
			throw new IllegalArgumentException("'savePermissions()' encountered non-existent path");
		}
		// Get current folder permissions with parental prohibitions included
		final CatalogPermission currentPermissions = new CatalogPermission(getCatalogRootPath(), folderPath);
		final CatalogPermission newPermissions = new CatalogPermission(permissions);
		final CatalogPermission updatedPermissions = currentPermissions.applyUpdates(newPermissions);
		if (updatedPermissions != null) {
			updatedPermissions.savePermissions(folderPath);
		} else {
			throw new IllegalStateException("Could not create CatalogPermission instance from permissions: " + permissions);
		}
	}
	
	/**
	 * Convenience method will only return Birst-specific files using 
	 * the {@link BirstOnlyFilenameFilter}.
	 * @param folder is the directory to examine for files and folders.
	 * @return a list of Birst-specific {@link CatalogFile}s that reside in the 'folder'.
	 */
	public List<CatalogFile> getBirstFiles(final String folder, final SMIWebUserBean ub) {
		return getFiles(folder, BirstOnlyFilenameFilter.getInstance(), ub);
	}
	
	/**
	 * Convenience method returns all files under the specified 'folder'.
	 * @param folder is the directory to examine for files and subfolders. 
	 * @return a list of all {@link CatalogFile}s that reside in the 'folder'.
	 */
	public List<CatalogFile> getAllFiles(final String folder, final SMIWebUserBean ub) {
		return getFiles(folder, null, ub);
	}
	
	/**
	 * Gets all the files and folders residing in the specified 'folder'.
	 * @param folder is the directory to examine for 
	 * @param birstOnly if true only return Birst-specific files; otherwise return all files.
	 * @return null if there is a problem, otherwise return the list of files and 
	 * directories in the specified 'folder'.
	 */
	public List<CatalogFile> getFiles(final String folder, final FilenameFilter filter, final SMIWebUserBean ub) {
		if (folder == null || folder.trim().length() == 0) { 
			return null;
		}
		
		File folderFile = new File(folder);
		if (folderFile == null || !folderFile.isDirectory()) {
			return null;
		}

		List<CatalogFile> list = new ArrayList<CatalogFile>();
		File[] files;
		if (filter != null) {
			files = folderFile.listFiles(filter);
		} else {
			files = folderFile.listFiles();
		}
		if (files != null) {
			for (File file : files) {
				if (file.isDirectory()) {
					continue;
				}
				CatalogPermission perm = getPermission(file.getAbsolutePath());
				list.add(new CatalogFile(file, perm.canModify(ub.getUserGroups(), ub.isSuperUserOwnerOrAdmin())));
			}
		}
		return list;
	}
	
	/* --------------------------------------------------------------------------------------------
	 * -- Inner class: CatalogFile                                                               --
	 * -------------------------------------------------------------------------------------------- */

	public static class CatalogFile {
		File file;
		boolean _canModify;
		
		public CatalogFile(File file, boolean canModify) {
			this.file = file;
			_canModify = canModify;
		}
		
		public File getFile() {
			return file;
		}
		
		public String getName() { 
			return file.getName();
		}
				
		public Date getModifiedDate() {
			return new Date(file.lastModified());
		}
		
		public boolean canModify() {
			return _canModify;
		}
	}

	/* --------------------------------------------------------------------------------------------
	 * -- End, inner class: CatalogFile                                                          --
	 * -------------------------------------------------------------------------------------------- */

	
	/**
	 * Gets the canonical path of the space's catalog's root directory.
	 * <p/>
	 * Uses canonical path because it may have to resolve relative paths given from
	 * 'smi.home' used in configuration files.  For example, a relative path like: 
	 * C:\SMI\SMIWeb\tomcat\..\..\Data\1b2cf631-..." should be resolved to 
	 * C:\SMI\Data\1b2cf631-...".  This keeps the paths consistent so that cached
	 * maps that rely on the path (e.g., CatalogManager's use of a cached hashmap
	 * of {@link CatalogPermission}s) won't have multiple entries for the same path.
	 * @return the canonical path of the space's catalog's root directory.
	 */
	public String getCatalogRootPath() {
		if (catalogDirectoryPath == null) {
			try {
				catalogDirectoryPath = catalogDirectory.getCanonicalPath();
			} catch (IOException e) {
				throw new IllegalStateException("Failed to get canonical path for root folder");
			}
		}
		return catalogDirectoryPath;
	}
	
	/**
	 * Not-so-lazy initialization and retrieval of the list of user's root directories.
	 * If the user is a regular Birst user, then the list will be something like:
	 * <ul>
	 * <li>private/user@domain
	 * <li>shared
	 * </ul>
	 * and creates them if they don't exist.
	 * If the user is a Birst Service "superuser", then the list will be something like:
	 * <ul>
	 * <li>private (which should include all user@domain dirs underneath)
	 * <li>shared
	 * </ul>
	 * If the user is a regular user on an Enterprise app, then the list will be something like:
	 * <ul>
	 * <li>private/user@domain
	 * <li>Fidelity (although it's not clear that this is used)
	 * <li>shared
	 * </ul>
	 * In this case the app folder is not created if it does not exist.
	 * @param userBean contains the current user's information.
	 * @return a list of root directories (as above) for this user.
	 * @throws SwapSpaceInProgressException 
	 */
	public List<File> getRootDirectories(SMIWebUserBean userBean) throws SwapSpaceInProgressException {
		return getRootDirectories(userBean, false);
	}
	
	public List<File> getRootDirectories(SMIWebUserBean userBean, boolean allAccessToAll) throws SwapSpaceInProgressException {
		List<File> rootDirectories = userBean.getRootDirectories(); 
		if (rootDirectories == null) {
			rootDirectories = new ArrayList<File>();
			String rootFolder = getCatalogRootPath();
			File catalogRoot = new File(rootFolder);
			if (! catalogRoot.isDirectory())
				catalogRoot.mkdirs();
			
			// Get 'private' root directories
			if (userBean.getUser() == null || userBean.getUser().getUserName() == null) {
				logger.error("Unknown user logged in.  Can not create private directory.");
			} else {
				// Regular user has access to his/her own private dir only - do this first so that the directory is created
				try {
					String userName = normalizeName(userBean.getUser().getUserName());
					
					File privateDir = new File(catalogRoot.getCanonicalFile(), PRIVATE_DIR);
					createDirectory(userName, privateDir.getCanonicalPath(), null, true);
					final File privateUserFolder = new File(privateDir, userName);
					rootDirectories.add(privateUserFolder);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					logger.error(e, e);
				}
				
				// Superuser (Birst Service) has access to all 'private' dirs
				if (userBean.isSuperUser() || allAccessToAll) {
					rootDirectories.clear();
					try {
						createDirectory(PRIVATE_DIR, catalogRoot.getCanonicalPath(), null, true);
						final File privateFolder = new File(rootFolder, PRIVATE_DIR);
						rootDirectories.add(privateFolder);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						logger.error(e, e);
					}
				}
			}
			// Get an enterprise folder (e.g., Fidelity)  TODO: Still needed?
			String appName = userBean.getRepository().getApplicationName();
			if (appName != null && appName.trim().length() > 0) {
				String customerFolder = rootFolder + "/" + userBean.getRepository().getApplicationName();
				File file = new File(customerFolder);
				if (file.exists()) {
					rootDirectories.add(file);
				}
			}
			try {
				createDirectory(SHARED_DIR, catalogRoot.getCanonicalPath(), null, true);
				final File sharedDirectory = new File(catalogRoot, SHARED_DIR);
				rootDirectories.add(sharedDirectory);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error(e, e);
			}
			// Refresh the user bean's list
			userBean.setRootDirectories(rootDirectories);
			// Observe and report
			if (logger.isTraceEnabled())
			{
				logger.trace("For user: " + userBean.getUserName() + ", retrieving root dirs:\n");
				for (File directory : userBean.getRootDirectories())
				{
					logger.trace("\t" + directory.getAbsolutePath());
				}
			}
		}
		return rootDirectories;
	}
    
	public File getPrivateDirectory(SMIWebUserBean userBean) throws IOException{
		File catalogRoot = new File(getCatalogRootPath());
		String userName = normalizeName(userBean.getUser().getUserName());
		File privateDir = new File(catalogRoot.getCanonicalFile(), PRIVATE_DIR);
		
		return new File(privateDir, userName);
	}
	
    private String normalizeName(String userName) {
    	StringBuffer newName = new StringBuffer();
    	
    	for (int i = 0; i < userName.length(); i++) {
    		char c = userName.charAt(i);
    		if (c == '\\' || c == '/' || c == ':' || c == '?' || c == '"' || c == '<' || c == '>' || c == '|')
    			c = '_';
    		
    		newName.append(c);
    	}
    	
    	return newName.toString();
	}

	public List<File> getRootDirs(SMIWebUserBean userBean) throws SwapSpaceInProgressException {
    	List<File> rootDirs = null;
    	
    	// For unit testing only. Avoids full ServletContext setup.
    	if (bypassServletContext) {
    		return Arrays.asList(testRootDirs);
    	}
    	
    	rootDirs = this.getRootDirectories(userBean);    	
    	return rootDirs;
    }

    /**
     * Returns a list of directories that have been tagged via the presence of
     * the "dashboard.xml" file in those directories.
     * <b>Note:</b> This method deals exclusively with relative file paths.
     * @param userBean sets the context of all file queries to that user's catalog area.
     * @return a list of file paths that represent dashboard directories.  The list will
     * be empty if there aren't any.
     * @throws SwapSpaceInProgressException 
     */
    public List<String> getDashboardFolders(final SMIWebUserBean userBean) throws SwapSpaceInProgressException {
    	 return getDashboardFolders(userBean, false);
    }
    
	/**
     * Returns a list of directories that have been tagged via the presence of
     * the "dashboard.xml" file in those directories.
     * <b>Note:</b> This method deals exclusively with relative file paths.
     * @param userBean sets the context of all file queries to that user's catalog area.
     * @param filterChildren will exclude child dashboard folders found under a parent
     * dashboard folder; otherwise all dashboard folders will be returned if this 
     * flag is false.
     * @return a list of file paths that represent dashboard directories.  The list will
     * be empty if there aren't any.
	 * @throws SwapSpaceInProgressException 
     */
    public List<String> getDashboardFolders(final SMIWebUserBean userBean, final boolean filterChildren) throws SwapSpaceInProgressException {
    	// logger.debug("'getDashboardFolders()' - 'userName':" + userBean.getUserName() 
    	//		+ "; filterChildren': " + filterChildren);
    	// Get the complete file system
    	final FileNode fileNode = getUserFileNode(userBean);

    	// Recurse into 'fileNode' filesystem hierarchy
    	final List<String> dashboardDirs = new ArrayList<String>();
    	findDashboardFolders(dashboardDirs, fileNode);
    	Collections.sort(dashboardDirs);
    	
    	// Remove any "dashboard" subdirectories of higher-level "dashboard" directories
    	if (filterChildren) {
	    	List<String> filteredPaths = new ArrayList<String>();
	    	for (int i = 0; i < dashboardDirs.size(); i++) {
				if (i > 0) {
		    		final String currentPath = dashboardDirs.get(i);
		    		final String previousPath = dashboardDirs.get(i - 1);
		    		if (currentPath.indexOf(previousPath) != -1) {
		    			continue;
		    		}
				}
	    		filteredPaths.add(dashboardDirs.get(i));
	    	}
	    	/*
	    	for (String filteredPath : filteredPaths) {
	    		logger.debug("'getDashboardFolders()' - 'filteredPath': " + filteredPath);
	    	}
	    	*/
	    	return filteredPaths;
    	} else {
    		// Otherwise return full set of dashboard directories
    		/*
    		for (String dashboardDir : dashboardDirs) {
    			logger.debug("'getDashboardFolders()' - 'dashboardDir': " + dashboardDir);
    		}
    		*/
    		return dashboardDirs;
    	}
    }

    /**
     * Recursive dive through FileNode hierarchy looking for 'dashboard.xml'
     * marker files.  Adds the parent folder of any encountered files to the list.
     * <b>Note:</b> This method deals exclusively with relative file paths.
     * @param dashboardDirs is the accumulator of dashboard directory paths.
     * @param fileNode is the FileNode hierarchy to be examined.
     */
    private void findDashboardFolders(List<String> dashboardDirs, FileNode fileNode) {
    	final List<FileNode> fileNodes = fileNode.getChildren();
    	// Stop recursion at leaf
    	if (fileNodes == null || fileNodes.isEmpty()) {
    		return;
    	}
    	// Descend thru tree looking for marker file
    	for (FileNode childFileNode : fileNodes) {
    		if (childFileNode.isDashboardDirectory()) {
	    		// Found a dashboard directory. Store it and return.
	    		dashboardDirs.add(childFileNode.getName());
    		} 
			if (childFileNode.getChildren() != null && childFileNode.getChildren().size() > 0) {
				findDashboardFolders(dashboardDirs, childFileNode);
			}
    	}
    }
    
	/**
	 * Returns a nested tree of the file system visible for a specific user.
	 * <p/>
	 * <b>Background:</b>
	 * This method was moved from {@link BaseFileWS}.  (It really belongs here.)  It was
	 * moved here as part of the effort to provide dashboard directories for
	 * the Flex Adhoc application. 
	 * <p/> 
	 * <b>Note:</b>
	 * This method uses getAllFiles() which retrieves ALL files in the 
	 * directory structure since it needs to find and report folders that 
	 * contain the Dashboard marker file: dashboard.xml.  The original version in 
	 * BaseFileWS uses getFiles() which includes the BirstOnlyFilenameFilter
	 * and restricts the files returned to those that have Birst-specific
	 * file extensions.
     * <b>Note:</b> This method deals exclusively with relative file paths.
	 * @param userBean is the user whose file system tree is to be returned.
	 * @throws SwapSpaceInProgressException 
	 */
	public FileNode getUserFileNode(SMIWebUserBean userBean) throws SwapSpaceInProgressException {
		// Create the root FileNode
		FileNode rootFileNode = new FileNode("", "RootDirs", true, 0, false);
		// Get the root directories for this user
		List<File> dirNames = getRootDirs(userBean);
		// Determine the 'private' directories
		for (File dirName : dirNames) {
			
			CatalogPermission perm = getPermission(dirName.getAbsolutePath());
			
			// Change label field from 'private/user@domain' to 'private'
			final String filteredLabel = filterDirectoryLabel(dirName);
			final String filteredDirName = getRelativeFilepath(dirName.getAbsolutePath());
			// Create directory FileNode
			FileNode child = new FileNode(filteredDirName, filteredLabel, true, dirName.lastModified(), 
					perm.canModify(userBean.getUserGroups(), userBean.isSuperUserOwnerOrAdmin()));
			// Starting with private/shared nodes, populate all the descendant FileNodes
			if (child != null) {
				// Add children recursively. When finished add to 'root' FileNode.
				addAllChildren(child, userBean);
				rootFileNode.addChild(child);
			}
		}
		return rootFileNode;
	}
	
    public static File staticGetReportFile(String fullPath) throws Exception {
    	if (fullPath.toUpperCase().endsWith(com.successmetricsinc.util.FileUtils.COMPILED_REPORT_EXTENSION) || fullPath.toUpperCase().endsWith(com.successmetricsinc.util.FileUtils.OLD_COMPILED_REPORT_EXTENSION)) {
    		// look for .jrxml first, then look for .AdhocReport
    		String jrxmlFile = fullPath;
    		int index = jrxmlFile.lastIndexOf('.');
    		jrxmlFile = jrxmlFile.substring(0, index).concat(".jrxml");
    		File jFile = staticGetReportFile(jrxmlFile);
    		if (jFile != null) {
    			// see if this file needs to be compiled
    			String path = jFile.getAbsolutePath();
    			int i = path.lastIndexOf('.');
    			path = path.substring(0, i).concat(com.successmetricsinc.util.FileUtils.COMPILED_REPORT_EXTENSION);
    			File f = new File(path);
    			if (f.exists() == false) {
    				// try old extension
    				path = path.substring(0,i).concat(com.successmetricsinc.util.FileUtils.OLD_COMPILED_REPORT_EXTENSION);
    			}
    			if (f.exists() == false || f.lastModified() < jFile.lastModified()) {
    				// compile and return f
//    				compileReport(jFile);
    			}
				return f;
    		}
    		
    		File aFile = getAdhocReportFile(fullPath, "");
    		if (aFile != null) {
    			String path = aFile.getAbsolutePath();
    			int i = path.lastIndexOf('.');
    			path = path.substring(0, i).concat(com.successmetricsinc.util.FileUtils.COMPILED_REPORT_EXTENSION);
    			File f = new File(path);
    			if (f.exists() == false) {
    				// try old extension
    				path = path.substring(0, i).concat(com.successmetricsinc.util.FileUtils.OLD_COMPILED_REPORT_EXTENSION);
    				f = new File(path);
    			}
    			if (f.exists() == false || f.lastModified() < aFile.lastModified()) {
	    			AdhocReport report = AdhocReport.read(aFile);
	    			try {
						report.compileAndSaveReport(aFile);
						// its going to be always new extension
						path = path.substring(0, i).concat(com.successmetricsinc.util.FileUtils.COMPILED_REPORT_EXTENSION);
						f = new File(path);
					} catch (Exception e) {
						logger.error(e, e);
						f = null;
					}
    			}
    			return f;
    		}
    	}
    	
    	// see if its a full pathname now
    	File file = new File(fullPath);
    	if (file.exists())
    		return file;
    	
    	return null;
    }
    
    public  File getReportFile(SMIWebUserBean userBean, String fullPath) throws Exception {
    	String fileName = getNormalizedPath(fullPath);
    	File file = staticGetReportFile(fileName);
    	if (file != null && file.exists())
    		return file;
    		
    	// try to see if just adding the root directory will give us a file
    	file = new File(getCatalogRootPath(), fullPath);
    	if (file.exists()) {
    		return file;
    	}
    	
    	if (userBean != null) {
	    	List<File> rootDirs = getRootDirs(userBean);
	    	logger.debug("getReportFile: fullPath: " + fullPath + ", rootDirs: " + rootDirs);
	    	
	    	for (File s: rootDirs) {
	    		String path = s.getAbsolutePath() + File.separator + fullPath;
	    		file = new File(path);
	    		if (file.exists())
	    			return file;
	    	}
    	}
    	return null;
    }
    
    public static File getAdhocReportFile(String p, String catalogRootPath) throws Exception {
    	p = getNormalizedPath(p, catalogRootPath);
		int index = p.lastIndexOf('.');
		if (index < 0)
			return null;
		
		String adhocFile = p.substring(0, index).concat(".AdhocReport");
		return staticGetReportFile(adhocFile);
    }
    
    public static String getJRXMLFile(String p, String catalogRootPath) throws Exception {
    	p = getNormalizedPath(p, catalogRootPath);
		int index = p.lastIndexOf('.');
		if (index < 0)
			return null;
		
		String jrxmlFile = p.substring(0, index).concat(".jrxml");
		return jrxmlFile;
    }
    
    /**
     * Checks to see if the specified 'folderPath' is viewable by the current user
     * (via his/her CatalogManager instance).  Called by BaseFileWS.addAllChildrenExtended.
     * TODO: Could refactor addAllChildren to use this approach too.
     * @param folderPath is the path to check. Should be absolute path.
     * @return true if this user can view the folder; false otherwise.
     */
    public boolean isViewableFolder(final String folderPath) {
    	// If you're the Birst Service "superuser" you can always view folders
    	final SMIWebUserBean userBean = SMIWebUserBean.getUserBean();
    	if (userBean.isSuperUser() || isSpaceAdmin()) {
    		return true;
    	}
    	
    	return isViewableFolder(folderPath, userBean);
    }
    
    public boolean isViewableFolder(final String folderPath, final SMIWebUserBean userBean) {
    	final CatalogPermission permission = getPermission(folderPath);
    	return permission.canView(userBean.getUserGroups(), userBean.isSuperUserOwnerOrAdmin());
    	
    }
    
	/**
	 * @param writeable the writeable to set
	 */
	public void setWriteable(boolean writeable) {
	}
	
	/* ---------------------------------------------------------------------------- */
	/* The set of methods that return just name and path in the FileNode instances. */
	/* (used in Adhoc file dialogs)                                                 */
	/* ---------------------------------------------------------------------------- */

	/**
	 * Returns a nested tree of the file system visible for a specific user.
	 * Gates between the viewable or writeable versions of the method depending
	 * on the 'writeable' setting.
	 * <p/>
	 * <b>Background:</b>
	 * These methods were moved from BaseFileWS.  (And they really belongs here.)  
	 * They were moved here as part of the effort to provide dashboard directories 
	 * for the Flex Adhoc application. 
	 * <p/> 
	 * <b>Note:</b>
	 * These methods use getAllFiles() which retrieves ALL files in the 
	 * directory structure since it needs to find and report folders that 
	 * contain the Dashboard marker file: dashboard.xml.  The original version in 
	 * BaseFileWS uses getFiles() which includes the BirstOnlyFilenameFilter
	 * and restricts the files returned to those that have Birst-specific
	 * file extensions.
	 */
	private void addAllChildren(final FileNode child, SMIWebUserBean ub) {
		// Recreate full directory filepath
		String fullDirPath = getCatalogRootPath() + File.separator + child.getName();
		// Get list of viewable/modifiable folders under that 'dirName'
		List<File> subdirectories = getFolderFiles(fullDirPath);
		if (subdirectories != null) {
			for (File subdirectory : subdirectories) {
				// Skip if this folder is not viewable
				CatalogPermission perm = getPermission(subdirectory.getAbsolutePath());
				if (! perm.canView(ub.getUserGroups(), ub.isSuperUserOwnerOrAdmin()))
					continue;

				// Create FileNode instance for this 'subdirectory'
				String label = subdirectory.getName();
				final String relativePath = getRelativeFilepath(subdirectory.getAbsolutePath());
				boolean isDirectory = true;
				FileNode grandchildFileNode = new FileNode(relativePath, 
                                                           label, 
                                                           isDirectory, 
                                                           subdirectory.lastModified(),
                                                           perm.canModify(ub.getUserGroups(), ub.isSuperUserOwnerOrAdmin()));
				// Recurse through children of this most current child
				addAllChildren(grandchildFileNode, ub);
				child.addChild(grandchildFileNode);
			}
		}
		// Now process all files under the child folder
		List<CatalogFile> catalogFiles = getAllFiles(fullDirPath, ub);
		if (catalogFiles != null) {
			boolean found1stDashboard = false;
			for (CatalogFile catalogFile : catalogFiles) {
				String label = catalogFile.getName();
				if (CatalogManager.isBirstFileType(catalogFile.getFile())) {
					String name = getRelativeFilepath(catalogFile.getFile().getAbsolutePath());
					Date modifiedDate = catalogFile.getModifiedDate();
					FileNode fn = new FileNode(name, label, false, modifiedDate, false);
					child.addChild(fn);
				} else {
					// If the 'child' contains dashboard marker file, set 'child's flag
					if (label.equalsIgnoreCase(CatalogManager.DASHBOARD_MARKER_FILE)) {
						if (!found1stDashboard) {
							child.setIsDashboardDirectory(true);
							found1stDashboard = true;
						}
					}
				}
			}
		}
	}
	
	public static boolean isBirstFileType(final File file) {
		boolean birstFileType = false;
		final String fileType = com.successmetricsinc.util.FileUtils.getFileExtension(file.getName());
		for (String birstFileExtension : ALLOWED_EXTENSIONS) {
			if (fileType.equalsIgnoreCase(birstFileExtension)) {
				birstFileType = true;
				break;
			}
		}
		return birstFileType;
	}
	
	/* ---------------------------------------------------------------------------- 
	   -- Filename Filters                                                       --
	   ---------------------------------------------------------------------------- */

	/**
	 * A FilenameFilter that allows only files that have file extensions that
	 * are relevant to Birst.  See the ALLOWED_EXTENSIONS array for the current
	 * list of allowed filenames. 
	 */
	public static class BirstOnlyFilenameFilter implements FilenameFilter {
		
		private static BirstOnlyFilenameFilter instance = new BirstOnlyFilenameFilter(); 

		public static BirstOnlyFilenameFilter getInstance() { return instance; }
		
		public boolean accept(File dir, String name) {
			boolean isVisibleToEndUsers = true;
			final File file = new File(dir, name);
			if (file.isFile()) {
				// Get the file extension
				final int dotIndex = name.lastIndexOf('.');
				if (dotIndex == -1) {
					isVisibleToEndUsers = false;
				}
				final String fileExtension = name.substring(dotIndex + 1);
				isVisibleToEndUsers = contains(fileExtension.toUpperCase(), ALLOWED_EXTENSIONS);
			} else {
				for (String birstOnlyDirectoryName : INTERNAL_DIRECTORIES) {
					if (name.equalsIgnoreCase(birstOnlyDirectoryName)) {
						isVisibleToEndUsers = false;
						break;
					}
				}
			}
			return isVisibleToEndUsers;
		}
	}
	
	/**
	 * A FilenameFilter that allows only files that have file extensions that
	 * are relevant to Birst.  See the ALLOWED_EXTENSIONS array for the current
	 * list of allowed filenames. 
	 */
	public static class DeactivateDashboardFilenameFilter implements FilenameFilter {
		
		private static DeactivateDashboardFilenameFilter instance = new DeactivateDashboardFilenameFilter(); 

		public static DeactivateDashboardFilenameFilter getInstance() { return instance; }
		
		public boolean accept(File dir, String name) {
			// Check for 'dashboard.xml' marker file
			if (name.equalsIgnoreCase(DASHBOARD_MARKER_FILE)) {
				return true;
			}
			// Accept .Dashboard & .smijasper files
			final int dotIndex = name.lastIndexOf('.');
			if (dotIndex == -1) {
				return false;
			}
			final String fileExtension = name.substring(dotIndex + 1);
			return fileExtension.equalsIgnoreCase("page");
		}
	}
	
	public static class UserDefinedFilenameFilter implements FilenameFilter {
		
		private String[] _allowedExtensions;
		
		public UserDefinedFilenameFilter(String[] allowedExtensions) {
			_allowedExtensions = allowedExtensions;
			for (int i = 0; i < _allowedExtensions.length; i++) {
				if (_allowedExtensions[i] == null)
					_allowedExtensions[i] = "";
				_allowedExtensions[i] = _allowedExtensions[i].toUpperCase();
			}
		}
		
		public boolean accept(File dir, String name) {
			boolean isVisibleToEndUsers = true;
			final File file = new File(dir, name);
			if (file.isFile()) {
				// Get the file extension
				final int dotIndex = name.lastIndexOf('.');
				if (dotIndex == -1) {
					isVisibleToEndUsers = false;
				}
				final String fileExtension = name.substring(dotIndex + 1);
				isVisibleToEndUsers = contains(fileExtension.toUpperCase(), _allowedExtensions);
			} else {
				for (String birstOnlyDirectoryName : INTERNAL_DIRECTORIES) {
					if (name.equalsIgnoreCase(birstOnlyDirectoryName)) {
						isVisibleToEndUsers = false;
						break;
					}
				}
			}
			return isVisibleToEndUsers;
		}
	}
	
	/* --------------------------------------------------------------------------------------------
	 * -- Helper (private) methods                                                               --
	 * -------------------------------------------------------------------------------------------- */
	
	/**
	 * This method takes a relative or absolute path and converts it to an absolute path in canonical form.
	 * Examples of relative and absolute are:
	 * <ul>
	 * <li>relative - private/pconnolly@birst.com/Dashboards, or /private/pconnolly@birst.com/Dashboards
	 * <li>absolute - C:\SMI\Data\a73a3a35-6923-495f-85f4-0b6357577478\catalog\private\pconnolly@birst.com\Dashboards
	 * </ul>
	 * If possible it will return the absolute path in canonical Windows format; it throws an 
	 * IllegalArgumentException if it cannot.
	 * @param path is the relative or absolute path to be normalized.
	 * @return the absolute path in canonical Windows (barf) format.
	 */
	public String getNormalizedPath(final String path) {
		return getNormalizedPath(path, getCatalogRootPath());
	}
	
	public static String getNormalizedPath(final String path, final String catalogRootPath) {
		logger.trace("'getNormalizedPath()' entered with 'path': " + path);
		// Convert to all Windoze-format path separators
		final String slashifiedPath = FilenameUtils.separatorsToSystem(path);
		// Normalize the 'path'
		final String normalizedPath = FilenameUtils.normalize(slashifiedPath);
		// See if the 'path' begins with the root folder already
		String normalizedAbsolutePath;
		if (normalizedPath.toUpperCase().indexOf(catalogRootPath.toUpperCase()) != 0) {
			// Add the root catalog folder if this is a relative path
			normalizedAbsolutePath = catalogRootPath + File.separator + normalizedPath;
		} else {
			normalizedAbsolutePath = normalizedPath;
		}
		// Determine the path's canonical form for this platform
		final File file = new File(normalizedAbsolutePath);
		String canonicalPath = null;
		try {
			canonicalPath = file.getCanonicalPath();
		} catch (IOException e) {
			logger.error("Could not get canonical path for: " + path);
		}
		logger.trace("'getNormalizedPath()' exited with 'canonicalPath': " + canonicalPath);
		return canonicalPath;
	}
	
	/**
	 * Searches the provided 'targetArray' array of Strings for the first occurrence
	 * of 'searchString'.  Assumes that the strings in the 'targetArray' are all
	 * capitalized and capitalizes the 'searchString' before doing the comparison.
	 * TODO: Generalize this and move it to an ArrayUtils class.
	 * @param searchString is the string to test for inclusion in the 'targetArray'.
	 * @param targetArray is the array of possible values.
	 * @return true if the 'searchString' is included in the 'targetArray'; false otherwise.
	 */
	private static boolean contains(final String searchString, final String[] targetArray) {
		final String upperCaseSearchString = searchString.toUpperCase();
		boolean containsString = false;
		for (String target : targetArray) {
			if (upperCaseSearchString.equals(target)) {
				containsString = true;
				break;
			}
		}
		return containsString;
	}
	
	/**
	 * Sends back the last subdir in the path, unless the parent is 'private' in which case the 
	 * 'private' portion is sent back too. For example, if path is .../private/user@domain then 
	 * the returned string will be 'private/user@domain'.
	 * 
	 * @param dirName is the full directory name to be filtered.
	 * @param catalogManager is the user's catalog manager instance.
	 * @return is the filtered (or not) label for the directory.
	 */
	private String filterDirectoryLabel(final File f) {
		String dirName = f.getAbsolutePath();
		String[] parts = dirName.split(SLASH_REGEX);
		// If penultimate subdir is 'private', send back 'private/user@domain'
		if (parts[parts.length - 2].equals(CatalogManager.PRIVATE_DIR)) {
			return parts[parts.length - 2] + "/" + parts[parts.length - 1];
		} else {
			// Otherwise just send the last subdir in the path
			return parts[parts.length - 1];
		}
	}

	/**
	 * Converts a directory filepath to one that is relative to the user's catalog root and
	 * convert any backslashes to forward slashes.
	 * 
	 * @param directoryPath is the full directory filepath.
	 * @param catalogManager is the user's catalog manager instance.
	 * @return a directory filepath that is relative to the user's catalog root
	 * with all forward slashes, no backslashes.
	 */
	private String getRelativeFilepath(final String directoryPath) {
		// Get full filepath for the user's catalog
		String rootFilePath = getCatalogRootPath();
		// Normalize (resolve) the current directory name
		String normalizedDirectoryPath = null;
		try {
			normalizedDirectoryPath = new File(directoryPath).getCanonicalPath();
		} catch (IOException e) {
			logger.error("Could not get the canonical path of the directory: " + directoryPath, e);
			
		}
		String name = "";
		if (normalizedDirectoryPath != null) {
			// Strip off the user catalog path from the beginning of the directoryPath
			name = directoryPath.substring(rootFilePath.length() + 1);
			name = name.replace('\\', '/');
		}
		// Convert any backslashes to forward slashes and return
		return name;
	}
	
	/* ---------------------------------------------------------------------------- 
	   -- Helper methods for testing.                                            --
	   ---------------------------------------------------------------------------- */

	/**
	 * For testing only. Used to run unit tests without the full servlet context.
	 * @param testRootDirs is the array of test roots to use.
	 */
	protected void bypassServletContext(File[] testRootDirs) {
		this.bypassServletContext = true;
		this.testRootDirs = testRootDirs;
	}
	
	private String validateNewFileName(String newName, boolean isFile) {
		String errorMessage = "";
		// Check that newName exists and that it is valid
		if (newName == null || newName.trim().length() == 0) {
			errorMessage = "New file name is null or zero-length.";
		} else {
			// Only check extensions for files, not directories
			if (isFile) {
				newName = newName.trim();
				final int lastDot = newName.lastIndexOf(".");
				if (lastDot > 0) {
					final String fileExtension = newName.substring(newName.lastIndexOf("."));
					if (fileExtension == null || fileExtension.length() == 0) {
						errorMessage = "The new file name needs a file extension of .png, .gif, .jpg, .pdf, .AdhocReport, .jrxml or .page";
					}
					if (!fileExtension.equalsIgnoreCase(".png") 
						&& !fileExtension.equalsIgnoreCase(".gif")
						&& !fileExtension.equalsIgnoreCase(".jpg")
						&& !fileExtension.equalsIgnoreCase(".pdf")
						&& !fileExtension.equalsIgnoreCase(".adhocreport")
						&& !fileExtension.equalsIgnoreCase(".jrxml")
						&& !fileExtension.equalsIgnoreCase(".dashboard")
						&& !fileExtension.equalsIgnoreCase(".dashlet")
						&& !fileExtension.equalsIgnoreCase(".page")) {
						errorMessage = "Only file extensions of .png, .gif, .jpg, .pdf, .AdhocReport, .dashlet, .jrxml, .Dashboard and .page are allowed.";
					}
				} else {
					errorMessage = "The new file name must have a valid extension: .png, .gif, .jpg, .pdf, .AdhocReport, .dashlet, .jrxml, .Dashboard or .page";
				}
			}
		}
		return errorMessage;
	}
	
	private String validateNewFolderName(String newName, String parentDirectoryPath) {
		String errorMessage = "";
		// Check that newName exists and that it is valid
		if (newName == null || newName.trim().length() == 0) {
			errorMessage = "New folder name is null or zero-length.";
		} else {
			if (!com.successmetricsinc.util.FileUtils.isValidWindowsFilename(newName)) {
				errorMessage = "New folder name contains one or more illegal characters: \\/:*?\"<>|"; 
			} else {
				final String newDirectoryPath = parentDirectoryPath + File.separator + newName;
				final File newDirectory = new File(newDirectoryPath);
				if (newDirectory.exists()) {
					errorMessage = "Cannot create new folder. New name already exists.";
				}
			}
		}
		return errorMessage;
	}
	
	/**
	 * Validates that the input fullFilePath is not null and not an empty String and
	 * that the file or directory actually exists.
	 * @param fullFilePath is the full filepath to be examined.
	 * @return an empty String ("") is the input fullFilePath represents a valid
	 * file or directory; a non-empty error message if there is a problem.
	 */
	private String validateFilePathInput(final String fullFilePath) {
		return com.successmetricsinc.util.FileUtils.isValidPathInput(fullFilePath);
	}
	
	/**
	 * Indicates whether or not the specified file path is a restricted folder.
	 * @param filePath expects a folder, not files.
	 * @return Empty String if there is no restriction; otherwise return a non-blank error message.
	 */
	protected String isRestrictedFolder(String filePath) {
		final String normalizedPath = getNormalizedPath(filePath);		
		final String relativePath = getRelativeFilepath(normalizedPath);
		
		// Is specified folder 'shared'?
		if (relativePath.equalsIgnoreCase(SHARED_DIR)) {
			return "Cannot delete the 'shared' directory";
		}
		// Is specified folder 'private'?
		if (relativePath.equalsIgnoreCase(PRIVATE_DIR)) {
			return "Cannot delete the 'private' directory";
		}
		// Can't delete /private/<user@domain> unless it's a superuser
		String[] folders = relativePath.split("/");
		if (folders.length == 2 && folders[0].equalsIgnoreCase(PRIVATE_DIR)) {
			final SMIWebUserBean userBean = SMIWebUserBean.getUserBean();
			// Only superusers can delete 'private/user@domain' directories
			if (!userBean.isSuperUser()) {
				return "Cannot delete a 'private/user@domain' directory without special privileges";
			}
			// Can't delete the user's own directory
			if (userBean.getUserName().equalsIgnoreCase(folders[1].toLowerCase())) {
				return "Cannot delete the current user's directory";
			}
		}
		return "";
	}
	
	/**
	 * If a path containing $root/private is entered, add the user's
	 * folder to the path.
	 * @param folderPath is the path under which might have $root/private
	 * @return the new folderPath if required with the user's folder added to private
	 */
	public String addUserFolderToPrivate(String folderPath) {
		String parentFolder = com.successmetricsinc.util.FileUtils.getFileParent(folderPath);
		final String newFolder = com.successmetricsinc.util.FileUtils.getFileLeaf(folderPath);
		if (parentFolder != null && newFolder != null) {
			File privateDir = new File(getCatalogRootPath() + File.separator + "private");
			if (privateDir.equals(new File(parentFolder))) {
				final UserBean userBean = SMIWebUserBean.getUserBean();
				folderPath = parentFolder + File.separator + userBean.getUser() + File.separator + newFolder; 
			}
		}
		return folderPath;
	}
	
	public static void checkForSwapSpaceOperation(CatalogManager cm) throws SwapSpaceInProgressException
	{	
		if (cm != null
				&& cm.getCatalogRootPath() != null
				&& com.successmetricsinc.util.FileUtils.isSwapSpaceInProgress(cm.getCatalogRootPath() + File.separator + "..")) {
			throw new SwapSpaceInProgressException();
		}
	}
	
	/**
	 * A convenience method to indicate whether the current user is a Birst Service superuser.  
	 * Intentionally avoids the 'admin' term used throughout Acorn and SMI. 
	 * @return true if the user is a Birst Service ("superuser") administrator.
	 */
	public boolean isSpaceAdmin() {
    	final SMIWebUserBean userBean = SMIWebUserBean.getUserBean();
    	// superuser trumps regular checks
    	if (userBean.isSuperUser()) {
    		return true;
    	}
    	// adminUserBean is for setup and should not be the current user
    	if (userBean.getIsAdmin() && !userBean.equals(SMIWebUserBean.getAdminUserBean())) {
    		return true;
    	} else {
    		return false;
    	}
	}

}
