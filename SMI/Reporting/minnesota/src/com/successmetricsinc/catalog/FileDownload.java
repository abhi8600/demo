/**
 * $Id: FileDownload.java,v 1.3 2011-09-24 01:11:12 ricks Exp $
 *
 * Copyright (C) 2009 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.catalog;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;

import com.successmetricsinc.WebServices.IWebServiceResult;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author pconnolly
 */
public class FileDownload implements IWebServiceResult {
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(FileDownload.class);
	
	private String filename;
	private String encodedFileContents;

	/**
	 * No trespassing constructor.
	 */
	@SuppressWarnings("unused")
	private FileDownload() {
		
	}
	
	public FileDownload(final String filename, final String encodedFileContents) {
		this.filename = filename;
		this.encodedFileContents = encodedFileContents;
	}
	
	/**
	 * Builds OM element in the form:
	 * <ul style="list-style: none;">
	 * <li>&lt;FileDownload&gt;</li>
	 * <ul style="list-style: none;">
	 * <li>&lt;FileName&gt;Your File Name Here.txt&lt;/FileName&gt;</li>
	 * <li>&lt;EncodedFileContents&gt;01234567890ABCDEF&lt;/EncodedFileContents&gt;</li>
	 * </ul>
	 * <li>&lt;/FileDownload&gt;</li>
	 * </ul>
	 * Adds the filename and the encoded file contents to an OMElement for Axis2.
	 */
	public void addContent(OMElement parent, OMFactory factory, OMNamespace ns) {
		// Create root element "FileDownload"
		OMElement fileDownloadElement = factory.createOMElement("FileDownload", ns);
		// Add XML subelement "FileName"
		XmlUtils.addContent(factory, fileDownloadElement, "FileName", filename, ns);
		// Add XML subelement "EncodedFileContents"
		XmlUtils.addContent(factory, fileDownloadElement, "EncodedFileContents", encodedFileContents, ns);
		// Add "FileDownload" element to parent
		parent.addChild(fileDownloadElement);
	}
	
	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getEncodedFileContents() {
		return encodedFileContents;
	}

	public void setEncodedFileContents(String encodedFileContents) {
		this.encodedFileContents = encodedFileContents;
	}
}
