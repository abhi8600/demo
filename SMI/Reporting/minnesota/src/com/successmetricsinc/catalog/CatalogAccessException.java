/**
 * $Id: CatalogAccessException.java,v 1.5 2009-12-17 01:19:25 pconnolly Exp $
 *
 * Copyright (C) 2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.catalog;
/**
 * Exception thrown if error on accessing catalog.
 * @author Brad Peters
 */
public class CatalogAccessException extends Exception
{
	public enum Reason
	{
		NoPermissions, 
		Error,
	}
	private static final long serialVersionUID = 1L;
	private Reason reason;

	public CatalogAccessException() {
		super();
	}

	public CatalogAccessException(String message) {
		super(message);
	}

	public CatalogAccessException(Reason reason) {
		this.reason = reason;
	}

	public CatalogAccessException(String message, Reason reason) {
		super(message);
		this.reason = reason;
	}

	public CatalogAccessException(String message, Throwable e) {
		super(message, e);
	}

	public Reason getReason() {
		return reason;
	}

	public void setReason(Reason reason) {
		this.reason = reason;
	}
}
