package com.successmetricsinc.queuing;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.util.JRSaver;

import org.apache.axiom.om.OMElement;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.UserBean;
import com.successmetricsinc.UI.ApplicationContext;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.adhoc.AdhocReportHelper;
import com.successmetricsinc.queueing.Message;
import com.successmetricsinc.queueing.MessageResult;
import com.successmetricsinc.queueing.consumer.QueueConsumer;
import com.successmetricsinc.queuing.AsynchReportGeneration.AsyncReportGenerationMessageFactory;
import com.successmetricsinc.queuing.AsynchReportGeneration.AsynchReportMessage;

public class AsyncReportGenerator extends QueueConsumer {

	private static final Logger logger = Logger.getLogger(AsyncReportGenerator.class);
	
	public AsyncReportGenerator(Message message, int consumerIndex)
	{
		super(message, consumerIndex);
	}
	
	private AsynchReportMessage asynchMessage;
	
	@Override
	public MessageResult consume() throws Exception {
		logger.info("consuming message: " + message.getId());
		result = new AsyncReportResult();
		ApplicationContext ac = null;
		try
		{
			MDC.put("username", message.getUserName());
			MDC.put("spaceID", message.getSpaceId());
			MDC.put("messageID", message.getId());
			asynchMessage = AsynchReportMessage.parseMessage(message);
			AsyncReportGenerationMessageFactory.MessageData msgData = new AsyncReportGenerationMessageFactory.MessageData(asynchMessage.getMessageDataFileName());
			Map<String, String> systemPropertiesMap = msgData.getSystemPropertiesMap();
			if (systemPropertiesMap != null)
			{
				for (String propName : systemPropertiesMap.keySet())
				{
					logger.debug("Overriding async report system property: " + propName);
					System.setProperty(propName, systemPropertiesMap.get(propName));
				}
			}
			ac = ApplicationContext.getInstance(msgData.getSpaceAppPath(), null);
			Repository r = ac.getRepository();
			if (r == null)
			{
				throw new Exception("Could not find repository in ApplicationContext.");
			}
			SMIWebUserBean userBean = new SMIWebUserBean();
			userBean.setApplicationContext(ac);
			userBean.setUser(r.findUser(message.getUserName()));
			userBean.setRepository(r);
			userBean.setLocale(msgData.getLocale());
			userBean.setTimeZone(msgData.getTimezone());
			Session repSession = userBean.getRepSession(true);
			Map<String, String> sessionVarsMap = msgData.getSessionVariablesMap();
			if (sessionVarsMap != null)
			{
				for (String variableName : sessionVarsMap.keySet())
				{
					if(repSession.getVariable(variableName) != null)
					{
						if (logger.isTraceEnabled())
							logger.trace("Overwriting existing session variable " + variableName);
						repSession.updateVariable(userBean.getRepository(), variableName, sessionVarsMap.get(variableName), true);						
					}
					else
					{
						if (logger.isTraceEnabled())
							logger.trace("Creating new session variable " + variableName);
						repSession.createNewVariable(true, variableName, sessionVarsMap.get(variableName));
					}
					logger.debug("Updated session variables: " + repSession.toString());
				}
			}
			UserBean.addThreadRequest(Thread.currentThread().getId(), userBean);
			
			AsyncReportGenerationMessageFactory.ReportGenerationData repData = new AsyncReportGenerationMessageFactory.ReportGenerationData(asynchMessage.getMessageDataFileName());
			JasperPrint prt = AdhocReportHelper.getSynchDashboardJasperPrint(repData, r, userBean);
			if (prt == null)
			{
				throw new Exception("Could not generate JasperPrint Object for report.");
			}
			File msgDataFile = new File(asynchMessage.getMessageDataFileName());
			if (msgDataFile.exists())
			{
				msgDataFile.delete();
			}
			File printFile = new File(asynchMessage.getResultsFileName() + "_print");
			if (!printFile.createNewFile())
			{
				throw new Exception("File " + printFile.getName() + " already exists.");
			}
			JRSaver.saveObject(prt, printFile);
			result.setSuccess(true);
			((AsyncReportResult)result).setJasperPrintObject(prt);
			((AsyncReportResult)result).setJasperPrintFileName(printFile.getAbsolutePath());
			logger.info("Successfully consumed message - " + message.getId());
		} catch (Exception ex) {
			logger.error(ex.toString(), ex);
			result.setSuccess(false);
			result.setErrorMessage(ex.getMessage());
			result.setErrorCode(1);//XXX need to set specific error code
		} finally
		{
			UserBean.removeThreadRequest(Thread.currentThread().getId());
			MDC.remove("username");
			MDC.remove("spaceID");
			MDC.remove("messageID");
			if (ac != null)
				ac.dispose();
		}	
		return ((AsyncReportResult)result);		
	}

	@Override
	public OMElement getMessageResultElement() throws Exception {
		return ((AsyncReportResult)result).getMessageResultElement();
	}

	@Override
	public void postConsumption() throws Exception {
		try
		{
			MDC.put("username", message.getUserName());
			MDC.put("spaceID", message.getSpaceId());
			MDC.put("messageID", message.getId());			
		File tempFile = new File(asynchMessage.getResultsFileName() + "_temp");
		FileOutputStream fos = null;		
		try {
			if (! tempFile.exists()) {
				tempFile.createNewFile();
			}
			fos = new FileOutputStream(tempFile);
			XMLStreamWriter writer = XMLOutputFactory.newInstance().createXMLStreamWriter(fos, "UTF-8");
			getMessageResultElement().serialize(writer);
			writer.flush();
		} catch (IOException e) {
			logger.error("Error saving message result: (with attempted file name: " + tempFile.toString() +")", e);
			throw (e);
		} catch (XMLStreamException e) {
			logger.error(e, e);
		} catch (FactoryConfigurationError e) {
			logger.error(e, e);
		}
		finally {
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
				}
			}
		}
		File resultFile = new File(asynchMessage.getResultsFileName());
		if (!tempFile.renameTo(resultFile))
		{
			throw new Exception("Renaming file " + tempFile.getName() + " to " + resultFile.getName() + " failed.");
		}		
		}
		finally
		{
			MDC.remove("username");
			MDC.remove("spaceID");
			MDC.remove("messageID");
		}
	}
}
