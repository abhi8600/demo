package com.successmetricsinc.queuing.AsynchReportGeneration;

import com.successmetricsinc.queueing.Message;

public class AsynchReportMessage{

	private String messageDataFileName;
	private String resultsFileName;		
	private transient String reportKey;
	
	public AsynchReportMessage(){
		
	}
	
	public static AsynchReportMessage parseMessage(Message message) {
		AsynchReportMessage asynchMsg = new AsynchReportMessage();
		asynchMsg.setMessageDataFileName(message.getInputData());
		asynchMsg.setResultsFileName(message.getResultData());
		return asynchMsg;
	}

	public String getMessageDataFileName(){
		return messageDataFileName;
	}
	
	public void setMessageDataFileName(String messageDataFileName){
		this.messageDataFileName = messageDataFileName;
	}
	
	public String getResultsFileName(){
		return resultsFileName;
	}
	
	public void setResultsFileName(String resultsFileName){
		this.resultsFileName = resultsFileName;
	}

	public void setReportKey(String reportKey) {
		this.reportKey = reportKey;
	}
	
	public String getReportKey() {
		return reportKey;
	}		
}
