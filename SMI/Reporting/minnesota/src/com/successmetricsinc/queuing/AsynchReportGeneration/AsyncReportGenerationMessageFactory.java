/**
 * 
 */
package com.successmetricsinc.queuing.AsynchReportGeneration;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.ServerContext;
import com.successmetricsinc.Session;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.adhoc.AdhocReport;
import com.successmetricsinc.queueing.Message;
import com.successmetricsinc.util.MessageFactory;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class AsyncReportGenerationMessageFactory {
	static Logger logger = Logger.getLogger(AsyncReportGenerationMessageFactory.class);
	final static Properties properties = Util.getProperties(Util.CUSTOMER_PROPERTIES_FILENAME, true);

	private static final String IS_PRINT = "isPrint";
	private static final String IS_PDF = "isPdf";
	private static final String APPLY_ALL_PROMPTS = "applyAllPrompts";
	private static final String IGNORE_PAGINATION = "ignorePagination";
	private static final String DASHLET_PROMPTS = "dashletPrompts";
	private static final String PROMPTS = "prompts";
	private static final String REPORT = "report";
	private static final String SPACE_APP_PATH = "spaceAppPath";
	private static final String LOCALE = "locale";
	private static final String TIMEZONE = "timezone";
	private static final String SESSION_VARIABLES = "sessionVars";
	private static final String VARIABLE = "variable";
	private static final String VARIABLE_NAME = "variableName";
	private static final String VARIABLE_VALUE = "variableValue";
	private static final String SYSTEM_PROPERTIES = "systemProperties";
	private static final String PROPERTY = "property";
	private static final String PROPERTY_NAME = "propertyName";
	private static final String PROPERTY_VALUE = "propertyValue";
	private static final String MAX_NUMBER_OF_PAGES = "asyncMaxNumberOfPages";
	private static final String MAX_NUMBER_OF_ROWS = "asyncMaxNumberOfRows";
	
	public static class MessageData {
		private String spaceAppPath;
		private Locale locale;
		private TimeZone timezone;
		private Map<String, String> sessionVariablesMap;
		private Map<String, String> systemPropertiesMap;
		
		public String getSpaceAppPath() {
			return spaceAppPath;
		}
		public Locale getLocale() {
			return locale;
		}
		public TimeZone getTimezone() {
			return timezone;
		}
		public Map<String, String> getSessionVariablesMap() {
			return sessionVariablesMap;
		}
		public Map<String, String> getSystemPropertiesMap() {
			return systemPropertiesMap;
		}
		
		public MessageData(String messageDataFilePath) {
			FileInputStream fis = null;
			XMLStreamReader parser;
			try {
				fis = new FileInputStream(new File(messageDataFilePath));
				XMLInputFactory inputFactory = XmlUtils.getSecureXMLInputFactory();
				parser = inputFactory.createXMLStreamReader(fis, "UTF-8");
				
				for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
					String name = parser.getLocalName();
					if (name.equals(Message.MESSAGE_DATA)) {
						// now parse the elements in here
						for (XMLStreamIterator msgChildIter = new XMLStreamIterator(parser); msgChildIter.hasNext(); ) {
							String elName = parser.getLocalName();
							if (SPACE_APP_PATH.equals(elName)) {
								this.spaceAppPath = XmlUtils.getStringContent(parser);
							}
							else if (LOCALE.equals(elName)) {
								String localeString = XmlUtils.getStringContent(parser);
								if (localeString != null)
								{
									final String[] parts = localeString.split("-");
									String languageCode = parts[0];
									String countryCode = parts.length == 2 ? parts[1] : null;
									this.locale = new Locale(languageCode, countryCode);
								}
								else
								{
									this.locale = Locale.getDefault();
								}
							}
							else if (TIMEZONE.equals(elName)) {
								String tzString = XmlUtils.getStringContent(parser);
								if (tzString != null)
								{
									this.timezone = TimeZone.getTimeZone(tzString);
								}
								else
								{
									this.timezone = TimeZone.getDefault();
								}
							}
							else if (SESSION_VARIABLES.equals(elName)) {
								sessionVariablesMap = new HashMap<>();
								for (XMLStreamIterator varIter = new XMLStreamIterator(parser); varIter.hasNext(); ) {
									String varEl = parser.getLocalName();
									if (varEl.equals(VARIABLE))
									{
										String varName = null, varValue = null;
										for (XMLStreamIterator varChildIter = new XMLStreamIterator(parser); varChildIter.hasNext(); ) {
											String varChildEl = parser.getLocalName();
											if (varChildEl.equals(VARIABLE_NAME))
												varName = XmlUtils.getStringContent(parser);
											else if (varChildEl.equals(VARIABLE_VALUE))
												varValue = XmlUtils.getStringContent(parser);											
										}
										sessionVariablesMap.put(varName, varValue);
									}
								}
							}
							else if (SYSTEM_PROPERTIES.equals(elName)) {
								systemPropertiesMap = new HashMap<>();
								for (XMLStreamIterator varIter = new XMLStreamIterator(parser); varIter.hasNext(); ) {
									String propEl = parser.getLocalName();
									if (propEl.equals(PROPERTY))
									{
										String propName = null, propValue = null;
										for (XMLStreamIterator varChildIter = new XMLStreamIterator(parser); varChildIter.hasNext(); ) {
											String varChildEl = parser.getLocalName();
											if (varChildEl.equals(PROPERTY_NAME))
												propName = XmlUtils.getStringContent(parser);
											else if (varChildEl.equals(PROPERTY_VALUE))
												propValue = XmlUtils.getStringContent(parser);											
										}
										systemPropertiesMap.put(propName, propValue);
									}
								}
							}
						}
					}					
				}

			} catch (Exception e) {
				logger.error(e, e);
			}
			finally {
				if (fis != null)
				{
					try
					{
						fis.close();
					}
					catch(Exception e)
					{
						logger.error(e, e);
					}
				}
					
			}
		}
	}

	public static class ReportGenerationData {
		private AdhocReport report;
		private String prompts;
		private String dashletPrompts;
		private boolean ignorePagination;
		private boolean applyAllPrompts;
		private boolean isPdf;
		private boolean isPrint;
		
		
		public AdhocReport getReport() {
			return report;
		}
		public String getPrompts() {
			return prompts;
		}
		public String getDashletPrompts() {
			return dashletPrompts;
		}
		public boolean isIgnorePagination() {
			return ignorePagination;
		}
		public boolean isApplyAllPrompts() {
			return applyAllPrompts;
		}
		public boolean isPdf() {
			return isPdf;
		}
		public boolean isPrint() {
			return isPrint;
		}
		
		public ReportGenerationData(String messageDataFilePath) {
			FileInputStream fis = null;
			XMLStreamReader parser;
			try {
				fis = new FileInputStream(new File(messageDataFilePath));
				XMLInputFactory inputFactory = XmlUtils.getSecureXMLInputFactory();
				parser = inputFactory.createXMLStreamReader(fis, "UTF-8");
				
				for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
					String name = parser.getLocalName();
					if (name.equals(Message.MESSAGE_DATA)) {
						// now parse the elements in here
						for (XMLStreamIterator msgChildIter = new XMLStreamIterator(parser); msgChildIter.hasNext(); ) {
							String elName = parser.getLocalName();
							if (elName.equals(Message.ASYNC_REPORT_GENERATION_DATA)) {
								int numAttributes = parser.getAttributeCount();
								for (int i = 0; i < numAttributes; i++) {
									String attribName = parser.getAttributeLocalName(i);
									if (IGNORE_PAGINATION.equals(attribName)) {
										Boolean val = Boolean.parseBoolean(parser.getAttributeValue(i));
										this.ignorePagination = val;
									} else if (APPLY_ALL_PROMPTS.equals(attribName)) {
										Boolean val = Boolean.parseBoolean(parser.getAttributeValue(i));
										this.applyAllPrompts = val;
									} else if (IS_PDF.equals(attribName)) {
										Boolean val = Boolean.parseBoolean(parser.getAttributeValue(i));
										this.isPdf = val;
									} else if (IS_PRINT.equals(attribName)) {
										Boolean val = Boolean.parseBoolean(parser.getAttributeValue(i));
										this.isPrint = val;
									} else if (MAX_NUMBER_OF_PAGES.equals(attribName)) {
										try {
											int val = Integer.parseInt(parser.getAttributeValue(i));
											Session.setMaxPageCount(val);
											logger.error("Setting max number of pages to " + val);
										}
										catch (Exception e) {
											logger.warn("Bad max number of pages in message", e);
										}
									} else if (MAX_NUMBER_OF_ROWS.equals(attribName)) {
										try {
											int val = Integer.parseInt(parser.getAttributeValue(i));
											Session.setMaxRowCount(val);
										}
										catch (Exception e) {
											logger.warn("Bad max number of rows in message", e);
										}
									}
								}
								// now parse the elements in here
								for (XMLStreamIterator childIter = new XMLStreamIterator(parser); childIter.hasNext(); ) {
									elName = parser.getLocalName();
									if (REPORT.equals(elName)) {
										this.report = AdhocReport.convert(parser);
									}
									else if (PROMPTS.equals(elName)) {
										this.prompts = XmlUtils.getStringContent(parser);
									}
									else if (DASHLET_PROMPTS.equals(elName)) {
										this.dashletPrompts = XmlUtils.getStringContent(parser);
									}							
								}
							}
						}
					}					
				}

			} catch (Exception e) {
				logger.error(e, e);
			}
			finally {
				if (fis != null)
				{
					try
					{
						fis.close();
					}
					catch(Exception e)
					{
						logger.error(e, e);
					}
				}
					
			}			
		}
		
	}
	public static AsynchReportMessage createAsyncReportGenerationMessage(AdhocReport report, 
													  String prompts, 
													  String dashletPrompts, 
													  Repository r, 
													  boolean ignorePagination, 
													  SMIWebUserBean ub, 
													  boolean applyAllPrompts,
													  boolean isPdf,
													  boolean isPrint) throws XMLStreamException, IOException {

		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace ns = fac.createOMNamespace("", "");
		OMElement messageData = fac.createOMElement(Message.MESSAGE_DATA, ns);
		XmlUtils.addContent(fac, messageData, AsyncReportGenerationMessageFactory.SPACE_APP_PATH, r.getServerParameters().getApplicationPath(), ns);
		Locale locale = ub.getLocale();
		if (locale != null) {
			String localeString = locale.getLanguage() + (locale.getCountry() == null ? "" : "-" + locale.getCountry());
			XmlUtils.addContent(fac, messageData, AsyncReportGenerationMessageFactory.LOCALE, localeString, ns);
		}
		TimeZone timeZone = ub.getTimeZone();
		if (timeZone != null) {
			XmlUtils.addContent(fac, messageData, AsyncReportGenerationMessageFactory.TIMEZONE, timeZone.getID(), ns);
		}
		Session session = ub.getRepSession();
		if (session != null && session.getSessionVariables() != null)
		{
			OMElement sessionVarsEl = XmlUtils.addContent(fac, messageData, AsyncReportGenerationMessageFactory.SESSION_VARIABLES, ns);
			for (String varName : session.getSessionVariables().keySet())
			{
				if(varName.equalsIgnoreCase("user"))
				{
					continue;
				}
				OMElement varEl = XmlUtils.addContent(fac, sessionVarsEl, AsyncReportGenerationMessageFactory.VARIABLE, ns);
				XmlUtils.addContent(fac, varEl, AsyncReportGenerationMessageFactory.VARIABLE_NAME, varName, ns);
				XmlUtils.addContent(fac, varEl, AsyncReportGenerationMessageFactory.VARIABLE_VALUE, Util.convertToString(session.getSessionVariables().get(varName), false, r, true), ns);
			}
		}
		OMElement systemVarEl = XmlUtils.addContent(fac, messageData, AsyncReportGenerationMessageFactory.SYSTEM_PROPERTIES, ns);
		Map<String, String> asyncReportSysPropertiesMap = ServerContext.getAsyncReportSysPropertiesMap();
		if (asyncReportSysPropertiesMap != null)
		{
			for (String key : asyncReportSysPropertiesMap.keySet())
			{
				OMElement propEl = XmlUtils.addContent(fac, systemVarEl, AsyncReportGenerationMessageFactory.PROPERTY, ns);
				XmlUtils.addContent(fac, propEl, AsyncReportGenerationMessageFactory.PROPERTY_NAME, key, ns);
				XmlUtils.addContent(fac, propEl, AsyncReportGenerationMessageFactory.PROPERTY_VALUE, asyncReportSysPropertiesMap.get(key), ns);
			}
		}		
		OMElement data = XmlUtils.addContent(fac, messageData, Message.ASYNC_REPORT_GENERATION_DATA, ns);
		OMElement rep = fac.createOMElement(AsyncReportGenerationMessageFactory.REPORT, ns, data);
		report.addContent(rep, fac, ns);
		if (prompts != null) {
			XmlUtils.addContent(fac, data, AsyncReportGenerationMessageFactory.PROMPTS, prompts, ns);
		}
		if (dashletPrompts != null) {
			XmlUtils.addContent(fac, data, AsyncReportGenerationMessageFactory.DASHLET_PROMPTS, dashletPrompts, ns);
		}
		XmlUtils.addAttribute(fac, data, AsyncReportGenerationMessageFactory.IGNORE_PAGINATION, Boolean.toString(ignorePagination), ns);
		XmlUtils.addAttribute(fac, data, AsyncReportGenerationMessageFactory.APPLY_ALL_PROMPTS, Boolean.toString(applyAllPrompts), ns);
		XmlUtils.addAttribute(fac, data, AsyncReportGenerationMessageFactory.IS_PDF, Boolean.toString(isPdf), ns);
		XmlUtils.addAttribute(fac, data, AsyncReportGenerationMessageFactory.IS_PRINT, Boolean.toString(isPrint), ns);
		if (AsyncReportGenerationMessageFactory.properties != null) {
			String strMaxNumberOfPages = properties.getProperty(AsyncReportGenerationMessageFactory.MAX_NUMBER_OF_PAGES);
			if (strMaxNumberOfPages != null) {
				try {
					int maxNumberOfPages = Integer.valueOf(strMaxNumberOfPages);
					XmlUtils.addAttribute(fac, data, AsyncReportGenerationMessageFactory.MAX_NUMBER_OF_PAGES, String.valueOf(maxNumberOfPages), ns);
				} catch (Exception e) {
					
				}
			}
			
			String strMaxNumberOfRows = properties.getProperty(AsyncReportGenerationMessageFactory.MAX_NUMBER_OF_ROWS);
			if (strMaxNumberOfRows != null) {
				try {
					int maxNumberOfRows = Integer.valueOf(strMaxNumberOfRows);
					XmlUtils.addAttribute(fac, data, AsyncReportGenerationMessageFactory.MAX_NUMBER_OF_ROWS, String.valueOf(maxNumberOfRows), ns);
				} catch (Exception e) {
					
				}
			}
			
		}
		String reportKey = data.toStringWithConsume();
		
		AsynchReportMessage ret = MessageFactory.createMessage(ub.getMessageDirectory(r), messageData);
		ret.setReportKey(reportKey);
		return ret;
	}
	
	
}
