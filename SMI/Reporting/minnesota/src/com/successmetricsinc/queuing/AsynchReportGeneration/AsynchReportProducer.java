/**
 * 
 */
package com.successmetricsinc.queuing.AsynchReportGeneration;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.successmetricsinc.ServerContext;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.messaging.AsynchResult;
import com.successmetricsinc.messaging.AsynchResult.RenderStatus;
import com.successmetricsinc.queueing.Message;
import com.successmetricsinc.queueing.producer.QueuePublishManager;
import com.successmetricsinc.queueing.qClient.MessengerFactory.Queue;
import com.successmetricsinc.queueing.qClient.Exceptions.MessengerException;
import com.successmetricsinc.util.Util;

/**
 * @author agarrison
 *
 */
public class AsynchReportProducer {

	private static final Logger logger = Logger.getLogger(AsynchReportProducer.class);
	
	public static String postMessage(AsynchReportMessage asynchReportMessage, SMIWebUserBean ub) throws IOException, MessengerException {
		// add results file location to message
		String dir = ub.getMessageDirectory(ub.getRepository());
		File tempDir = new File(dir);
		if (! tempDir.exists())
			tempDir.mkdir();
		File resultsFile = File.createTempFile("AsynchReport", ".xml", tempDir);
		resultsFile.delete();
		asynchReportMessage.setResultsFileName(resultsFile.getAbsolutePath());
		
		String userBeanId = ub.getId();
		String reportId = UUID.randomUUID().toString();
		
		String id = userBeanId + ":" + reportId;
		Message m = Message.createMessage(ub.getUserName(), ub.getSpaceID());
		m.setId(id);
		m.setInputData(asynchReportMessage.getMessageDataFileName());
		m.setResultData(asynchReportMessage.getResultsFileName());
		
		String releaseNumber = ServerContext.getSpecialRelease();//check if running special release eg. MRC or SDE
		if (releaseNumber == null)
		{
			String buildString = Util.getBuildString();
			if (buildString != null && !buildString.isEmpty() && buildString.contains("Release: ") && buildString.contains(", Build: "))
	        {
	            releaseNumber = buildString.substring("Release: ".length(), buildString.indexOf(", Build: ")).trim();
	            if (releaseNumber.contains(" "))
	            {
	                releaseNumber = releaseNumber.substring(0, releaseNumber.indexOf(" "));                
	            }
	        }
		}
		m.setReleaseNumber(releaseNumber);
		ServerContext.getQueuePublishManager().publishMessage(Queue.NOW_REPORT, m, 2);
		logger.info("Published message for async report generation using " + releaseNumber + " reporting engine :" + m.getId());
		// now keep track of this message
		// need data -> id map and id -> results file name map
		ub.associateQueueIdWithResultsPath(reportId, resultsFile.getAbsolutePath());
		ub.associateReportRenderRequestWithQueueId(asynchReportMessage.getReportKey(), reportId);
		ub.configureAsynchResult(reportId, new AsynchResult(RenderStatus.RUNNING));
		
		AsynchReportResultSubscriber.registerResult(userBeanId, ub);
		return reportId;
	}
	
	public static boolean isReportRendered(SMIWebUserBean ub, String id) {
		return ub.isAsynchReportCompleted(id);
		/*
		String reportPath = ub.getResultsPathFromQueueId(id);
		if (reportPath == null)
			throw new IllegalArgumentException("The id " + id + " was not found as a valid report generation id.");
		
		File file = new File(reportPath);
		if (file.exists() && file.canWrite()) {
			return true;
		}
		
		return false;*/
	}	
}
