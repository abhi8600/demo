package com.successmetricsinc.queuing.AsynchReportGeneration;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.successmetricsinc.UserBean;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.messaging.AsynchResult;
import com.successmetricsinc.messaging.AsynchResult.RenderStatus;
import com.successmetricsinc.queueing.Message;
import com.successmetricsinc.queueing.qClient.Subscriber;

public class AsynchReportResultSubscriber implements Subscriber {

	private static AsynchReportResultSubscriber subscriber = new AsynchReportResultSubscriber();
	private Map<String, SMIWebUserBean> asyncReportMap = Collections.synchronizedMap(new HashMap<String, SMIWebUserBean>());
	

	public static AsynchReportResultSubscriber getInstance(){
		return subscriber;
	}
	
	public static void registerResult(String id, SMIWebUserBean ub) {
		Map<String, SMIWebUserBean> reportMap = subscriber.asyncReportMap;
		synchronized(reportMap){
			if(!reportMap.containsKey(id))
				reportMap.put(id, ub);
		}
	}
	
	public static void unRegister(UserBean ub){
		String userBeanId = ub.getId();
		Map<String, SMIWebUserBean> reportMap = subscriber.asyncReportMap;
		synchronized(reportMap){
			if(reportMap.containsKey(userBeanId))
				reportMap.remove(userBeanId);
		}
	}
	
	@Override
	public void onRecieve(Message message) {		
		if(message != null && message.getId() != null && message.getId().trim().length() > 0){
			synchronized(asyncReportMap){
				String messageId = message.getId(); // of the format userBeanId:reportId
				String[] ids = messageId.split(":");
				String userBeanId = null;
				String reportId = null;
				if(ids.length > 1){
					userBeanId = ids[0];
					reportId = ids[1];
				}
				else{
					userBeanId = reportId = ids[0];
				}
				
				if(asyncReportMap.containsKey(userBeanId)){
					AsynchResult aResult = new AsynchResult(RenderStatus.COMPLETED);
					asyncReportMap.get(userBeanId).configureAsynchResult(reportId, aResult);					
				}
			}
		}
	}		
}
