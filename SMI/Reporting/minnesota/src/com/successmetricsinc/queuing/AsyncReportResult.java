package com.successmetricsinc.queuing;

import java.io.File;
import java.io.FileInputStream;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;

import net.sf.jasperreports.engine.JasperPrint;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;

import com.successmetricsinc.queueing.MessageResult;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

public class AsyncReportResult extends MessageResult {

	private JasperPrint print;
	private String jasperPrintFileName;
	public static final String JASPER_PRINT_FILE_NAME = "JasperPrintFileName";
	private static final Logger logger = Logger.getLogger(AsyncReportResult.class);
	
	public void setJasperPrintObject(JasperPrint print) {
		this.print = print;
	}
	
	public JasperPrint getJasperPrintObject() {
		return this.print;
	}
	
	public String getJasperPrintFileName() {
		return jasperPrintFileName;
	}

	public void setJasperPrintFileName(String jasperPrintFileName) {
		this.jasperPrintFileName = jasperPrintFileName;
	}

	@Override
	public OMElement getMessageResultElement()
	{
		OMElement root = super.getMessageResultElement();
		OMFactory fac = root.getOMFactory();
		OMNamespace ns = root.getNamespace();
		if (print != null && jasperPrintFileName != null)
		{
			XmlUtils.addContent(fac, root, JASPER_PRINT_FILE_NAME, jasperPrintFileName, ns);
		}
		return root;
	}
	
	public static AsyncReportResult parseAsyncReportResult(String resultFile) throws Exception
	{
		MessageResult messageResult = MessageResult.parseAsyncResult(resultFile);
		AsyncReportResult result = new AsyncReportResult();
		result.setSuccess(messageResult.isSuccess());
		result.setErrorCode(messageResult.getErrorCode());
		result.setErrorMessage(messageResult.getErrorMessage());
		if (result.isSuccess())
		{
			FileInputStream fis = null;
			XMLStreamReader parser;
			try {
				fis = new FileInputStream(new File(resultFile));
				XMLInputFactory inputFactory = XmlUtils.getSecureXMLInputFactory();
				parser = inputFactory.createXMLStreamReader(fis, "UTF-8");
				
				for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
					String name = parser.getLocalName();
					if (name.equals(MESSAGE_RESULT)) {
						for (XMLStreamIterator msgChildIter = new XMLStreamIterator(parser); msgChildIter.hasNext(); ) {
							String elName = parser.getLocalName();
							if (JASPER_PRINT_FILE_NAME.equals(elName)) {
								result.jasperPrintFileName = XmlUtils.getStringContent(parser);
							}
							
						}
					}
				}
			}
			catch (Exception e) {
				logger.error(e, e);
			}
			finally {
				if (fis != null)
				{
					try
					{
						fis.close();
					}
					catch(Exception e)
					{
						logger.error(e, e);
					}
				}
					
			}
		}
		return result;
	}
}
