/**
 * $Id: SMISubreportRunnerFactory.java,v 1.2 2009-07-01 00:02:23 agarrison Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */


package com.successmetricsinc.jasperReports;

import net.sf.jasperreports.engine.fill.JRBaseFiller;
import net.sf.jasperreports.engine.fill.JRFillSubreport;
import net.sf.jasperreports.engine.fill.JRSubreportRunner;
import net.sf.jasperreports.engine.fill.JRThreadSubreportRunnerFactory;

/**
 * @author agarrison
 *
 */
public class SMISubreportRunnerFactory extends JRThreadSubreportRunnerFactory {
	
	public JRSubreportRunner createSubreportRunner(JRFillSubreport fillSubreport, JRBaseFiller subreportFiller)
	{
		return new SMISubreportRunner(fillSubreport, subreportFiller);
	}

}
