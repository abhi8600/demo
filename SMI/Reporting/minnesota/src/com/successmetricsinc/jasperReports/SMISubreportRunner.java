/**
 * $Id: SMISubreportRunner.java,v 1.16 2012-06-25 18:29:45 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */


package com.successmetricsinc.jasperReports;

import net.sf.jasperreports.engine.JRExpression;
import net.sf.jasperreports.engine.JRRuntimeException;
import net.sf.jasperreports.engine.fill.JRBaseFiller;
import net.sf.jasperreports.engine.fill.JRFillSubreport;
import net.sf.jasperreports.engine.fill.JRSubreportRunResult;
import net.sf.jasperreports.engine.fill.JRSubreportRunner;
import net.sf.jasperreports.engine.fill.JRThreadSubreportRunner;

import com.successmetricsinc.ISubreportState;
import com.successmetricsinc.Session;
import com.successmetricsinc.UserBean;
import com.successmetricsinc.util.JRBandOverflowException;

/**
 * @author agarrison
 *
 */
public class SMISubreportRunner extends JRThreadSubreportRunner implements JRSubreportRunner{

	private JRFillSubreport fillSub;
	private static final int MAX_SUBREPORTS = 5;
	// changing to 1000 for PDFs from dashboards that are really long.
	private static final int MAX_RESUMES = 1000;
	private int resumes = 0;
	private UserBean request;
	private Session session;
	private Long callingThreadId;
	
	public SMISubreportRunner(JRFillSubreport fillSub, JRBaseFiller subreportFiller)
	{
		super(fillSub, subreportFiller);
		callingThreadId = Long.valueOf(Thread.currentThread().getId());
		this.fillSub = fillSub;
		request = UserBean.getParentThreadUserBean();
		if (request == null) {
			request = com.successmetricsinc.UI.SMIWebUserBean.getUserBean();
		}
		request.removeSubreport(getKey());
		session = Session.getCurrentThreadSession();
	}
	
	@Override
	public void run() {
		Long l = Long.valueOf(Thread.currentThread().getId());
		try {
			Session.associateThreadWithParentThread(l, callingThreadId);
			UserBean.addThreadRequest(l, request);
			Session.addThreadRequest(l, session);
			super.run();
		}
		finally {
			UserBean.removeThreadRequest(l);
			Session.removeThreadRequest(l);
			Session.unAssociateThreadWithParentThread(l);
		}
	}

	private ISubreportState getSubreportStateHolder() {
		return request;
	}
	private Object getKey() {
		if (fillSub != null) {
			JRExpression expression = fillSub.getExpression();
			return expression.getText();
		}
		return null;
	}
	
	/* (non-Javadoc)
	 * @see net.sf.jasperreports.engine.fill.JRSubreportRunner#reset()
	 */
	public void reset() {
		ISubreportState iss = getSubreportStateHolder();
		if (iss != null) {
			iss.removeSubreport(getKey());
		}
		super.reset();

	}
	/* (non-Javadoc)
	 * @see net.sf.jasperreports.engine.fill.JRSubreportRunner#start()
	 */
	public JRSubreportRunResult start() {
		ISubreportState iss = getSubreportStateHolder();
		if (iss != null) {
			if (iss.getSubreportCount(getKey()) >= MAX_SUBREPORTS) {
				throw new JRRuntimeException(new JRSubreportRecursionException("Subreport Recusion Problem: A subreport references itself."));
			}
			iss.addSubreport(getKey());
		}
		return super.start();
	}

	@Override
	public JRSubreportRunResult resume()  {
		resumes++;
		if (resumes > SMISubreportRunner.MAX_RESUMES) {
			throw new JRRuntimeException(new JRBandOverflowException("Either the summary section or the header section data does not fit on a page (resumes)."));
		}
		return super.resume();
	}
}
