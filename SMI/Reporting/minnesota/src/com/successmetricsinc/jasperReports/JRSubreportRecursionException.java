/**
 * $Id: JRSubreportRecursionException.java,v 1.2 2011-09-15 17:31:53 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */


package com.successmetricsinc.jasperReports;

import net.sf.jasperreports.engine.JRException;

/**
 * @author agarrison
 *
 */
public class JRSubreportRecursionException extends JRException {

	private static final long serialVersionUID = 1L;

	/**
	 * @param message
	 */
	public JRSubreportRecursionException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param t
	 */
	public JRSubreportRecursionException(Throwable t) {
		super(t);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param t
	 */
	public JRSubreportRecursionException(String message, Throwable t) {
		super(message, t);
		// TODO Auto-generated constructor stub
	}

}
