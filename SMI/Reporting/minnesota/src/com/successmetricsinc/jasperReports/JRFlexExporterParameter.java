/**
 * 
 */
package com.successmetricsinc.jasperReports;

import net.sf.jasperreports.engine.JRExporterParameter;

/**
 * @author agarrison
 *
 */
public class JRFlexExporterParameter extends JRExporterParameter {

	protected JRFlexExporterParameter(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	/**
	 * A map containing all the images that were used for generating the report. The JasperReports engine stores all the
	 * images in this map, and uses the map keys for referencing images throughout the export process.
	 */
	public static final JRFlexExporterParameter ADHOC_REPORT = new JRFlexExporterParameter("Adhoc Report");
	public static final JRFlexExporterParameter BACKGROUND_COLOR = new JRFlexExporterParameter("BackgroundColor");
	public static final JRFlexExporterParameter SMI_SERVLET_PATH = new JRFlexExporterParameter("SMI_SERVLET_PATH");
}
