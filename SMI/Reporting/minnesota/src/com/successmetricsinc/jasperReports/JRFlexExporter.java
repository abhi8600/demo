/*
 * ============================================================================
 * GNU Lesser General Public License
 * ============================================================================
 *
 * JasperReports - Free Java report-generating library.
 * Copyright (C) 2001-2006 JasperSoft Corporation http://www.jaspersoft.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 * JasperSoft Corporation
 * 303 Second Street, Suite 450 North
 * San Francisco, CA 94107
 * http://www.jaspersoft.com
 */
/*
 * Contributors:
 * Alex Parfenov - aparfeno@users.sourceforge.net
 * Adrian Jackson - iapetus@users.sourceforge.net
 * David Taylor - exodussystems@users.sourceforge.net
 * Lars Kristensen - llk@users.sourceforge.net
 */
package com.successmetricsinc.jasperReports;

import java.awt.Color;
import java.awt.Dimension;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import net.sf.jasperreports.engine.JRAbstractExporter;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRImageRenderer;
import net.sf.jasperreports.engine.JRLineBox;
import net.sf.jasperreports.engine.JRPen;
import net.sf.jasperreports.engine.JRPrintElement;
import net.sf.jasperreports.engine.JRPrintElementIndex;
import net.sf.jasperreports.engine.JRPrintEllipse;
import net.sf.jasperreports.engine.JRPrintFrame;
import net.sf.jasperreports.engine.JRPrintHyperlink;
import net.sf.jasperreports.engine.JRPrintImage;
import net.sf.jasperreports.engine.JRPrintLine;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JRPrintRectangle;
import net.sf.jasperreports.engine.JRPrintText;
import net.sf.jasperreports.engine.JRPropertiesMap;
import net.sf.jasperreports.engine.JRRenderable;
import net.sf.jasperreports.engine.JRRuntimeException;
import net.sf.jasperreports.engine.JRStyle;
import net.sf.jasperreports.engine.JRWrappingSvgRenderer;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.base.JRBoxPen;
import net.sf.jasperreports.engine.export.JRExportProgressMonitor;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.engine.export.JRHyperlinkProducer;
import net.sf.jasperreports.engine.export.JRHyperlinkProducerFactory;
import net.sf.jasperreports.engine.export.JROriginExporterFilter;
import net.sf.jasperreports.engine.export.XMLExporter;
import net.sf.jasperreports.engine.fill.JRTemplatePrintImage;
import net.sf.jasperreports.engine.type.HorizontalAlignEnum;
import net.sf.jasperreports.engine.type.HyperlinkTargetEnum;
import net.sf.jasperreports.engine.type.HyperlinkTypeEnum;
import net.sf.jasperreports.engine.type.LineDirectionEnum;
import net.sf.jasperreports.engine.type.LineStyleEnum;
import net.sf.jasperreports.engine.type.ModeEnum;
import net.sf.jasperreports.engine.type.ScaleImageEnum;
import net.sf.jasperreports.engine.util.JRColorUtil;
import net.sf.jasperreports.engine.util.JRProperties;

import org.apache.log4j.Logger;

import com.successmetricsinc.Prompt;
import com.successmetricsinc.Session;
import com.successmetricsinc.Prompt.PromptType;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.adhoc.AdhocChart;
import com.successmetricsinc.adhoc.AdhocReport;
import com.successmetricsinc.adhoc.AnyChartRenderer;
import com.successmetricsinc.chart.ChartOptions;
import com.successmetricsinc.chart.FlashRenderer;
import com.successmetricsinc.chart.AnyChart.AnyChartNode.ChartType;
import com.successmetricsinc.query.JasperDataSourceProvider;
import com.successmetricsinc.util.DateUtil;
import com.successmetricsinc.util.XmlUtils;


/**
 * Exports a JasperReports document to HTML format. It has character output type and exports the document to a
 * grid-based layout.
 * <p>
 * Since classic AWT fonts can be sometimes very different from HTML fonts, a font mapping feature was added. By using
 * the {@link JRExporterParameter#FONT_MAP} parameter, a logical font like "sansserif" can be mapped to a list of HTML
 * specific fonts, like "Arial, Verdana, Tahoma". Both map keys and values are strings.
 * 
 * @author Teodor Danciu (teodord@users.sourceforge.net)
 * @version $Id: JRFlexExporter.java,v 1.53 2012-10-12 09:20:43 BIRST\mpandit Exp $
 */
/*
 * CHANGES FOR SMI 1. style caching 2. handle prompts, checkbox (PROMPT_GROUP_, CHECKBOX_, etc styles), etc 3. scaling,
 * 4. ENCRYPTED_ styles for decryption
 */
public class JRFlexExporter extends JRAbstractExporter
{
	private static Logger logger = Logger.getLogger(JRFlexExporter.class);
	
	public static final String FLEX_EXPORTER_KEY = JRProperties.PROPERTY_PREFIX + "flex";
	public static final String DETAIL_MENU_STYLE = "DETAIL_MENU";
	public static final String HEADER_MENU_STYLE = "HEADER_MENU";
	public static final String COLUMN_STYLE = "COLUMN_STYLE";
	public static final String ALT_HEADER_STYLE = "ALT";
	public static final String STYLE_SPLIT_SEPARATOR = "-";
	// Setting for whether AnyChart is used
	private static final String HTML_ORIGIN_EXPORTER_FILTER_PREFIX = JRProperties.PROPERTY_PREFIX + "export.html.exclude.origin.";
	/**
	 * @deprecated Replaced by {@link JRHtmlExporterParameter#PROPERTY_FRAMES_AS_NESTED_TABLES}.
	 */
	public static final String PROPERTY_FRAMES_AS_NESTED_TABLES = JRProperties.PROPERTY_PREFIX + "export.html.frames.as.nested.tables";
	/*
	 * Constant indicating no scaling of image (SMI specific)
	 */
	public static final byte SMI_NO_SCALE_IMAGE = (byte) 0xFF;
	/**
	 * 
	 */
	protected static final String JR_PAGE_ANCHOR_PREFIX = "JR_PAGE_ANCHOR_";
	/**
	 * 
	 */
	protected static final String CSS_TEXT_ALIGN_LEFT = "left";
	protected static final String CSS_TEXT_ALIGN_RIGHT = "right";
	protected static final String CSS_TEXT_ALIGN_CENTER = "center";
	protected static final String CSS_TEXT_ALIGN_JUSTIFY = "justify";
	/**
	 * 
	 */
	protected static final String HTML_VERTICAL_ALIGN_TOP = "top";
	protected static final String HTML_VERTICAL_ALIGN_MIDDLE = "middle";
	protected static final String HTML_VERTICAL_ALIGN_BOTTOM = "bottom";
	public static final String IMAGE_NAME_PREFIX = "img_";
	protected static final int IMAGE_NAME_PREFIX_LEGTH = IMAGE_NAME_PREFIX.length();
	/**
	 * 
	 */
	protected StringBuilder builder;
	protected JRExportProgressMonitor progressMonitor = null;
	protected Map<String, String> rendererToImagePathMap = null;
	protected Map<String, byte[]> imageNameToImageDataMap = null;
	protected List<JRPrintElementIndex> imagesToProcess = null;
	protected boolean isPxImageLoaded = false;
	protected int reportIndex = 0;
	protected int pageIndex = 0;
	/**
	 * 
	 */
	protected File imagesDir = null;
	protected String imagesURI = null;
	protected String servletContext = null;
	protected boolean isOutputImagesToDir = false;
	protected String encoding;
	protected String sizeUnit = null;
	protected boolean isUsingImagesToAlign;
	/**
	 * 
	 */
	protected StringProvider emptyCellStringProvider = null;
	/**
	 * 
	 */
	private LinkedList<Color> backcolorStack;
	private Color backcolor;
	private float scaleFactor;
	/**
	 * Prompts - SMI
	 */
	Map<String, Prompt> promptMap;
	/*
	 * Cached Styles - SMI
	 */
	Map<String, String> styleMap = new HashMap<String, String>();

	/*
	 * TextTypes - SMI
	 */
	public enum TextType
	{
		Normal, CheckBox, PromptGroup, DropDown, Encrypted, EditableTextBox, Button, HTML
	};

	// protected JRHyperlinkProducerFactory hyperlinkProducerFactory;
	protected boolean deepGrid;

	public JRFlexExporter()
	{
		backcolorStack = new LinkedList<Color>();
		backcolor = null;
	}

	/**
	 * Scale a pixel coordinate by scaleFactor for HTML output - SMI
	 * 
	 * @param px
	 * @return
	 */
	public long scalePx(int px)
	{
		return Math.round((scaleFactor * ((double) px)));
	}

	public void setScaleFactor(float scaleFactor)
	{
		this.scaleFactor = scaleFactor;
	}

	public float getScaleFactor()
	{
		return scaleFactor;
	}

	/*
	 * interface for exportReport with prompt information - SMI
	 */
	public void exportReport(Map<String, Prompt> promptMap) throws JRException
	{
		this.promptMap = promptMap;
		exportReport();
	}

	/**
	 * 
	 */
	public void exportReport() throws JRException
	{
		progressMonitor = (JRExportProgressMonitor) parameters.get(JRExporterParameter.PROGRESS_MONITOR);
		/*   */
		setOffset();
		try
		{
			/*   */
			setExportContext();
			/*   */
			setInput();
			if (!parameters.containsKey(JRExporterParameter.FILTER))
			{
				filter = JROriginExporterFilter.getFilter(jasperPrint.getPropertiesMap(), HTML_ORIGIN_EXPORTER_FILTER_PREFIX);
			}
			/*   */
			if (!isModeBatch)
			{
				setPageRange();
			}
			imagesDir = (File) parameters.get(JRHtmlExporterParameter.IMAGES_DIR);
			servletContext = (String) parameters.get("SMI_SERVLET_PATH");

			Boolean isOutputImagesToDirParameter = (Boolean) parameters.get(JRHtmlExporterParameter.IS_OUTPUT_IMAGES_TO_DIR);
			if (isOutputImagesToDirParameter != null)
			{
				isOutputImagesToDir = isOutputImagesToDirParameter.booleanValue();
			}
			String uri = (String) parameters.get(JRHtmlExporterParameter.IMAGES_URI);
			if (uri != null)
			{
				imagesURI = uri;
			}
			encoding = getStringParameterOrDefault(JRExporterParameter.CHARACTER_ENCODING, JRExporterParameter.PROPERTY_CHARACTER_ENCODING);
			rendererToImagePathMap = new HashMap<String, String>();
			imagesToProcess = new ArrayList<JRPrintElementIndex>();
			isPxImageLoaded = false;
			// backward compatibility with the IMAGE_MAP parameter
			imageNameToImageDataMap = getImageMap();
			sizeUnit = getStringParameterOrDefault(JRHtmlExporterParameter.SIZE_UNIT, JRHtmlExporterParameter.PROPERTY_SIZE_UNIT);
			isUsingImagesToAlign = getBooleanParameter(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN,
					JRHtmlExporterParameter.PROPERTY_USING_IMAGES_TO_ALIGN, true);
			if (isUsingImagesToAlign)
			{
				emptyCellStringProvider = new StringProvider()
				{
					public String getStringForCollapsedTD(Object value)
					{
						return "><img src=\"" + value + "px\"";
					}

					public String getStringForEmptyTD(Object value)
					{
						return "<img alt=\"\" src=\"" + value + "px\" border=\"0\"/>";
					}
				};
				loadPxImage();
			} else
			{
				emptyCellStringProvider = new StringProvider()
				{
					public String getStringForCollapsedTD(Object value)
					{
						return "";
					}

					public String getStringForEmptyTD(Object value)
					{
						return "";
					}
				};
			}
			setHyperlinkProducerFactory();
			StringBuffer sb = (StringBuffer) parameters.get(JRExporterParameter.OUTPUT_STRING_BUFFER);
			if (sb != null)
			{
				exportReportToBuilder();
				sb.append(builder);
			} else
			{
				Writer writer = (Writer) parameters.get(JRExporterParameter.OUTPUT_WRITER);
				if (writer != null)
				{
					exportReportToBuilder();
					try
					{
						writer.write(builder.toString());
					} catch (IOException e)
					{
						throw new JRException("Error writing to StringBuffer writer : " + jasperPrint.getName(), e);
					}
				} else
				{
					OutputStream os = (OutputStream) parameters.get(JRExporterParameter.OUTPUT_STREAM);
					if (os != null)
					{
						exportReportToBuilder();
						try
						{
							writer = new OutputStreamWriter(os, encoding);
							writer.write(builder.toString());
						} catch (IOException e)
						{
							throw new JRException("Error writing to OutputStream writer : " + jasperPrint.getName(), e);
						}
					} else
					{
						File destFile = (File) parameters.get(JRExporterParameter.OUTPUT_FILE);
						if (destFile == null)
						{
							String fileName = (String) parameters.get(JRExporterParameter.OUTPUT_FILE_NAME);
							if (fileName != null)
							{
								destFile = new File(fileName);
							} else
							{
								throw new JRException("No output specified for the exporter.");
							}
						}
						try
						{
							os = new FileOutputStream(destFile);
							writer = new OutputStreamWriter(os, encoding);
						} catch (IOException e)
						{
							throw new JRException("Error creating to file writer : " + jasperPrint.getName(), e);
						}
						if (imagesDir == null)
						{
							imagesDir = new File(destFile.getParent(), destFile.getName() + "_files");
						}
						if (isOutputImagesToDirParameter == null)
						{
							isOutputImagesToDir = true;
						}
						if (imagesURI == null)
						{
							imagesURI = imagesDir.getName() + "/";
						}
						exportReportToBuilder();
						try
						{
							writer.write(builder.toString());
						} catch (IOException e)
						{
							throw new JRException("Error writing to file writer : " + jasperPrint.getName(), e);
						} finally
						{
							if (writer != null)
							{
								try
								{
									writer.close();
								} catch (IOException e)
								{
								}
							}
						}
					}
				}
			}
			if (isOutputImagesToDir)
			{
				if (imagesDir == null)
				{
					throw new JRException("The images directory was not specified for the exporter.");
				}
				if (isPxImageLoaded || (imagesToProcess != null && imagesToProcess.size() > 0))
				{
					if (!imagesDir.exists())
					{
						imagesDir.mkdir();
					}
					if (isPxImageLoaded)
					{
						JRRenderable pxRenderer = JRImageRenderer.getInstance("net/sf/jasperreports/engine/images/pixel.GIF");
						byte[] imageData = pxRenderer.getImageData();
						File imageFile = new File(imagesDir, "px");
						FileOutputStream fos = null;
						try
						{
							fos = new FileOutputStream(imageFile);
							fos.write(imageData, 0, imageData.length);
						} catch (IOException e)
						{
							throw new JRException("Error writing to image file : " + imageFile, e);
						} finally
						{
							if (fos != null)
							{
								try
								{
									fos.close();
								} catch (IOException e)
								{
								}
							}
						}
					}
					for (Iterator<JRPrintElementIndex> it = imagesToProcess.iterator(); it.hasNext();)
					{
						JRPrintElementIndex imageIndex = it.next();
						JRPrintImage image = getImage(jasperPrintList, imageIndex);
						JRRenderable renderer = image.getRenderer();
						if (renderer.getType() == JRRenderable.TYPE_SVG)
						{
							renderer = new JRWrappingSvgRenderer(renderer, new Dimension(image.getWidth(), image.getHeight()), 
									ModeEnum.OPAQUE == image.getModeValue() ? image.getBackcolor() : null);
						}
						byte[] imageData = renderer.getImageData();
						File imageFile = new File(imagesDir, getImageName(imageIndex));
						FileOutputStream fos = null;
						try
						{
							fos = new FileOutputStream(imageFile);
							fos.write(imageData, 0, imageData.length);
						} catch (IOException e)
						{
							throw new JRException("Error writing to image file : " + imageFile, e);
						} finally
						{
							if (fos != null)
							{
								try
								{
									fos.close();
								} catch (IOException e)
								{
								}
							}
						}
					}
				}
			}
		} finally
		{
			resetExportContext();
		}
	}

	@SuppressWarnings("unchecked")
	private Map<String, byte[]> getImageMap() {
		return (Map<String, byte[]>) parameters.get(JRHtmlExporterParameter.IMAGES_MAP);
	}

	protected void setHyperlinkProducerFactory()
	{
		hyperlinkProducerFactory = (JRHyperlinkProducerFactory) parameters.get(JRExporterParameter.HYPERLINK_PRODUCER_FACTORY);
	}

	public static JRPrintImage getImage(List<JasperPrint> jasperPrintList, String imageName)
	{
		return getImage(jasperPrintList, getPrintElementIndex(imageName));
	}

	public static JRPrintImage getImage(List<JasperPrint> jasperPrintList, JRPrintElementIndex imageIndex)
	{
		JasperPrint report = (JasperPrint) jasperPrintList.get(imageIndex.getReportIndex());
		JRPrintPage page = (JRPrintPage) report.getPages().get(imageIndex.getPageIndex());
		Integer[] elementIndexes = imageIndex.getAddressArray();
		Object element = page.getElements().get(elementIndexes[0].intValue());
		for (int i = 1; i < elementIndexes.length; ++i)
		{
			JRPrintFrame frame = (JRPrintFrame) element;
			element = frame.getElements().get(elementIndexes[i].intValue());
		}
		return (JRPrintImage) element;
	}
	

	/**
	 * changed exportReportToWriter to full up a String (StringBuilder) - SMI
	 */
	protected void exportReportToBuilder() throws JRException
	{
		builder = new StringBuilder();
//		builder.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
		builder.append("<report width=\"" + Math.round(((JasperPrint) jasperPrintList.get(0)).getPageWidth() * scaleFactor) + "\" height=\""
				+ Math.round(((JasperPrint) jasperPrintList.get(0)).getPageHeight() * scaleFactor));
		Object o = getParameter(JRFlexExporterParameter.BACKGROUND_COLOR);
		if (o != null && o instanceof Color) {
			builder.append("\" color=\"");
			builder.append(JRColorUtil.getColorHexa((Color)o));
		}
		builder.append("\">");
		for (reportIndex = 0; reportIndex < jasperPrintList.size(); reportIndex++)
		{
			jasperPrint = (JasperPrint) jasperPrintList.get(reportIndex);
			addProperties(jasperPrint.getPropertiesMap());
			
			List<JRPrintPage> pages = jasperPrint.getPages();
			if (pages != null && pages.size() > 0)
			{
				if (isModeBatch)
				{
					startPageIndex = 0;
					endPageIndex = pages.size() - 1;
				}
				JRPrintPage page = null;
				for (pageIndex = startPageIndex; pageIndex <= endPageIndex; pageIndex++)
				{
					if (Thread.currentThread().isInterrupted())
					{
						throw new JRException("Current thread interrupted.");
					}
					page = (JRPrintPage) pages.get(pageIndex);
					exportPage(page);
				}
			}
		}
		builder.append("</report>");
	}

	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	protected void exportPage(JRPrintPage page) throws JRException
	{
		for (Iterator it = page.getElements().iterator(); it.hasNext();)
		{
			JRPrintElement element = ((JRPrintElement) it.next());
			exportElement(0, 0, element, 0, 0);
		}
		if (progressMonitor != null)
		{
			progressMonitor.afterPageExport();
		}
	}
	
	protected void exportElement(int xOrigin, int yOrigin, JRPrintElement element, int parentWidth, int parentHeight) throws JRException {
		if (element instanceof JRPrintText)
		{
			JRPrintText pt = (JRPrintText) element;
			TextType type = TextType.Normal;
			JRStyle textStyle = pt.getStyle();
			if (textStyle != null && promptMap != null)
			{
				String sname = textStyle.getName();
				if (sname.startsWith("PROMPT_GROUP_"))
				{
					type = TextType.PromptGroup;
				}
				else if (sname.startsWith("HTML_"))
				{
					type = TextType.HTML;
				}					
				else if (sname.startsWith("CHECKBOX_"))
				{
					type = TextType.CheckBox;
				}
				else if (sname.startsWith("DROPDOWN_"))
				{
					type = TextType.DropDown;
				}
				else if (sname.startsWith("TEXTBOX_")) {
					type = TextType.EditableTextBox;
				}
				else if (sname.startsWith("BUTTON_"))
					type = TextType.Button;
				/*else if (sname.startsWith("ENCRYPTED_"))
				{
					type = TextType.Encrypted;
				}*/
			}
			
			switch (type)
			{
				case 	Normal			:	exportTextField(0, 0, pt, parentWidth, parentHeight);
											break;
				case 	HTML			:	exportHTMLField(0, 0, pt, parentWidth, parentHeight);
											break;											
				case	CheckBox		:	exportPromptGroupCheckBox(0, 0, pt, parentWidth, parentHeight);
											break;
				case	PromptGroup		:	exportPromptGroupButton(0, 0, pt, parentWidth, parentHeight);
											break;
				case	DropDown		:	exportPromptGroupDropDown(0, 0, pt, parentWidth, parentHeight);
											break;
				case	EditableTextBox	:	exportPromptGroupEditableTextBox(0, 0, pt, parentWidth, parentHeight);
											break;
				case	Button			:   exportButton(0, 0, pt, parentWidth, parentHeight);
											break;
				/*case	Encrypted		:	exportEncryptedField(0, 0, pt, parentWidth, parentHeight);
											break;*/
			}
							
		} else if (element instanceof JRTemplatePrintImage)
		{
			JRTemplatePrintImage tpi = (JRTemplatePrintImage) element;
			exportImage(0, 0, tpi, parentWidth, parentHeight);
		}
		else if (element instanceof JRPrintFrame) {
			exportFrame(0, 0, (JRPrintFrame)element, parentWidth, parentHeight);
		}
		else if (element instanceof JRPrintLine) {
			exportLine(0, 0, (JRPrintLine)element, parentWidth, parentHeight);
		}
		else if (element instanceof JRPrintEllipse) {
			exportDataGrid(0,0, (JRPrintEllipse)element, parentWidth, parentHeight);
		}
		else if (element instanceof JRPrintRectangle) {
			exportRectangle(0, 0, (JRPrintRectangle)element, parentWidth, parentHeight);
		}
		/*
		else {
			logger.debug("Unknown object type being exported <" + element.toString() + ">");
		}
		*/
	}

	private void exportRectangle(int xOrigin, int yOrigin, JRPrintRectangle element,
			int parentWidth, int parentHeight) {

		builder.append("<rectangle ");
		addDefaultProperties(xOrigin, yOrigin, element, parentWidth, parentHeight);
		builder.append(" lineWidth=\"");
		builder.append(element.getLinePen().getLineWidth());
		builder.append("\" lineColor=\"#" + JRColorUtil.getColorHexa(element.getLinePen().getLineColor()) + "\"");
		builder.append(">");
		JRPropertiesMap properties = element.getPropertiesMap();
		addProperties(properties);
		builder.append("</rectangle>");
	}

	private void exportDataGrid(int xOrigin, int yOrigin, JRPrintEllipse element,
			int parentWidth, int parentHeight) {
		builder.append("<dataGrid "); 
		addDefaultProperties(xOrigin, yOrigin, element, parentWidth, parentHeight);
		builder.append(">"); 
		JRPropertiesMap properties = element.getPropertiesMap();
		addProperties(properties);
		builder.append("</dataGrid>");
	}

	private void exportLine(int xOrigin, int yOrigin, JRPrintLine element, int parentWidth, int parentHeight) {
		builder.append("<line ");
		addDefaultProperties(xOrigin, yOrigin, element, parentWidth, parentHeight);
		JRPen pen = element.getLinePen();
		float lineWidth = pen.getLineWidth().floatValue();
		builder.append(" lineWidth=\"" + Math.round(lineWidth * scaleFactor) + "\"");
		LineStyleEnum lineStyle = pen.getLineStyleValue(); 
		builder.append(" lineStyle=\"");
		builder.append(lineStyle == LineStyleEnum.DASHED ? "Dashed" : 
			(lineStyle == LineStyleEnum.DOTTED ? "Dotted" : 
				(lineStyle == LineStyleEnum.DOUBLE ? "Double" : "Solid")));
		builder.append("\" direction=\"");
		builder.append(element.getDirectionValue() == LineDirectionEnum.BOTTOM_UP ? "BottomUp" : "TopDown");
		builder.append("\">");
		JRPropertiesMap properties = element.getPropertiesMap();
		addProperties(properties);
		builder.append("</line>");
		
	}

	protected void exportFrame(int xOrigin, int yOrigin, JRPrintFrame element, int parentWidth, int parentHeight) throws JRException {
		builder.append("<frame ");
		addDefaultProperties(xOrigin, yOrigin, element, parentWidth, parentHeight);
		builder.append(">");
		JRPropertiesMap properties = element.getPropertiesMap();
		addProperties(properties);
		int x = getScaledX(element, xOrigin);
		int y = getScaledY(element, yOrigin);
		int width = getScaledWidth(element, xOrigin, x);
		int height = getScaledHeight(element, yOrigin, y);
		for (Iterator<JRPrintElement> iter = element.getElements().iterator(); iter.hasNext(); ) {
			JRPrintElement child = iter.next();
			exportElement(0, 0, child, width, height);
		}
		builder.append("</frame>");
	}
	
	private int getScaledHeight(JRPrintElement element, int yOrigin, int y) {
		int height = Math.round(element.getHeight() * scaleFactor);
		if ((element.getY() + element.getHeight() + yOrigin) * scaleFactor > y + height) {
			height = Math.round((element.getY() + element.getHeight() + yOrigin) * scaleFactor);
			height = height - y;
		}
		return height;
	}
	private int getScaledWidth(JRPrintElement element, int xOrigin, int x) {
		int width = Math.round(element.getWidth() * scaleFactor);
		if ((element.getX() + element.getWidth() + xOrigin) * scaleFactor > x + width) {
			width = Math.round((element.getX() + element.getWidth() + xOrigin) * scaleFactor);
			width = width - x;
		}
		return width;
	}
	private int getScaledX(JRPrintElement element, int xOrigin) {
		return Math.round((element.getX() + xOrigin) * scaleFactor);
	}
	private int getScaledY(JRPrintElement element, int yOrigin) {
		return Math.round((element.getY() + yOrigin) * scaleFactor); 
	}
	protected void addDefaultProperties(int xOrigin, int yOrigin, JRPrintElement element, int parentWidth, int parentHeight) {
		// handle space between elements due to scaling problems.
		int y = getScaledY(element, yOrigin);
		int height = getScaledHeight(element, yOrigin, y);
		int x = getScaledX(element, xOrigin);
		int width = getScaledWidth(element, xOrigin, x);
		
		// handle rounding in a frame
		if (parentHeight > 0) {
			if (Math.abs(parentHeight - height) < (int)scaleFactor + 1) 
				height = parentHeight;
		}
		if (parentWidth > 0) {
			if (Math.abs(parentWidth - width) < (int)scaleFactor + 1)
				width = parentWidth;
		}
		builder.append("x=\"" + x + "\" y=\"" + y + "\" width=\"" + width
				+ "\" height=\"" + height + "\" color=\"#" + JRColorUtil.getColorHexa(element.getForecolor()) 
				+ "\" backcolor=\"#" + JRColorUtil.getColorHexa(element.getBackcolor()) + "\" ");
		if (element.getModeValue() == ModeEnum.TRANSPARENT) {
			builder.append("transparent=\"true\" ");
		}
	}

	protected void addProperties(JRPropertiesMap properties) {
		addProperties(properties, null);
	}
	
	
	protected void addProperties(JRPropertiesMap properties, String v) {
		if (properties != null) {
			String[] names = properties.getPropertyNames();
			if (names != null) {
				String datetimeFormat = null;
				Boolean isDateTime = Boolean.FALSE; 
				for (String name: names) {
					String value = properties.getProperty(name);
					if (name.startsWith("net.sf.jasperreports.export")) {
						name = name.substring("net.sf.jasperreports.export".length());
					}
					// <.xxx is invalid XML
					if (name.startsWith("."))
						continue;
					if (name.equalsIgnoreCase("datetimeFormat") && v != null) {
						datetimeFormat = value;
					}
					else if (name.equalsIgnoreCase("IsDateTime")) {
						isDateTime = Boolean.parseBoolean(value);
					}
					builder.append("<");
					builder.append(name);
					builder.append(">");
					builder.append(value);
					builder.append("</");
					builder.append(name);
					builder.append(">");
				}
				
				if (datetimeFormat != null) {
					if (v != null && v.length() > 0) {
						try {
							SimpleDateFormat sdf = new SimpleDateFormat(datetimeFormat);
							Date dt = sdf.parse(v);
							Calendar cal = new GregorianCalendar();
							cal.setTime(dt);
							String newValue = null;
							if (isDateTime) {
	//							SMIWebUserBean userBean = SMIWebUserBean.getUserBean();
	//							newValue = DateUtil.convertDateTimeToStandardFormat(cal, userBean.getTimeZone());
								newValue = DateUtil.convertDateTimeToStandardFormat(cal, TimeZone.getTimeZone("GMT"));
							}
							else { 
								newValue = DateUtil.convertDateToStandardFormat(cal);
							}
							
							builder.append("<valueToUse>");
							if (newValue != null) {
								builder.append(newValue);
							}
							else {
								builder.append("null");
							}
							builder.append("</valueToUse>");
						}
						catch (Exception e) {
	//						logger.warn("Error converting date to standard format " + v, e);
						}
					}
					else {
						builder.append("<valueToUse>");
						builder.append("null");
						builder.append("</valueToUse>");
					}
				}
			}
		}
	}
	protected void exportTextField(int xOrigin, int yOrigin, JRPrintText pt, int parentWidth, int parentHeight) {
		builder.append("<text ");
		addDefaultProperties(xOrigin, yOrigin, pt, parentWidth, parentHeight);
		builder.append(" bold=\"" + pt.isBold() + "\" italic=\"" + pt.isItalic() + "\" face=\""
				+ pt.getFontName() + "\" size=\"" + (Math.round(pt.getFontSize() * scaleFactor) - 1)
				+ "\" value=\"" + XmlUtils.encode(pt.getText()) + "\" underline=\"");
		builder.append(pt.isUnderline());
		builder.append("\" alignment=\"");
		builder.append(pt.getHorizontalAlignmentValue() == HorizontalAlignEnum.LEFT ? "left" : 
			pt.getHorizontalAlignmentValue() == HorizontalAlignEnum.RIGHT ? "right" : "center");
		builder.append("\" ");
		JRLineBox lineBox = pt.getLineBox();
		if (lineBox != null) {
			addBorderStyle("topBorder", lineBox.getTopPen());
			addBorderStyle("bottomBorder", lineBox.getBottomPen());
			addBorderStyle("leftBorder", lineBox.getLeftPen());
			addBorderStyle("rightBorder", lineBox.getRightPen());
		}
		
		if (pt.getText() != null && !pt.getText().isEmpty() && 
			pt.getHyperlinkTypeValue() != HyperlinkTypeEnum.NULL && pt.getHyperlinkTypeValue() != HyperlinkTypeEnum.NONE)
		{
			String target = getHyperlinkTarget(pt);
			String url = getHyperlinkURL(pt, 0);
			if (url != null) {
				boolean javascript = pt.getLinkType() != null ? pt.getLinkType().equals("javascript") : false;
				builder.append(" link=\"" + (javascript ? "javascript:" : "") + XmlUtils.encode(url) + "\""
					+ (target != null ? " target=\"" + target + "\"" : ""));
			}
		}
		builder.append(">");
		addProperties(pt.getPropertiesMap(), pt.getText());
		builder.append("</text>");
	}
	
	protected void exportHTMLField(int xOrigin, int yOrigin, JRPrintText pt, int parentWidth, int parentHeight) {
		builder.append("<html ");
		addDefaultProperties(xOrigin, yOrigin, pt, parentWidth, parentHeight);
		builder.append(" value=\"" + XmlUtils.encode(pt.getText()) + "\"");
		builder.append(">");
		addProperties(pt.getPropertiesMap(), pt.getText());
		builder.append("</html>");
	}
	
	protected void exportPromptGroupCheckBox(int xOrigin, int yOrigin, JRPrintText pt, int parentWidth, int parentHeight)
	{
		String parameterName = null;
		Prompt p = promptMap.get(pt.getStyle().getName().substring("CHECKBOX_".length()));
		if (p != null && p.getType() == PromptType.CheckBox)
		{
			parameterName = p.getParameterName();			
		}
		else if (p == null)
		{
			//write back defined using desinger
			parameterName = pt.getStyle().getName().substring("CHECKBOX_".length());			
		}
		if (parameterName != null)
		{
			builder.append("<checkbox ");
			addDefaultProperties(xOrigin, yOrigin, pt, parentWidth, parentHeight);
			/*builder.append(" bold=\"" + pt.isBold() + "\" italic=\"" + pt.isItalic() + "\" face=\""
					+ pt.getFontName() + "\" size=\"" + (Math.round(pt.getFontSize() * scaleFactor) - 1)
					+ "\" value=\"" + XmlUtils.encode(pt.getText()) + "\" ");*/
			builder.append("id=\"").append(XmlUtils.encode(pt.getText())).append("\" ");
			builder.append("name=\"").append(parameterName).append("\" ");
			builder.append("value=\"").append(XmlUtils.encode(pt.getText())).append("\" ");
			builder.append("alignment=\"");
			builder.append(pt.getHorizontalAlignmentValue() == HorizontalAlignEnum.LEFT ? "left" : 
				pt.getHorizontalAlignmentValue() == HorizontalAlignEnum.RIGHT ? "right" : "center");
			builder.append("\" ");
			JRLineBox lineBox = pt.getLineBox();
			if (lineBox != null) {
				addBorderStyle("topBorder", lineBox.getTopPen());
				addBorderStyle("bottomBorder", lineBox.getBottomPen());
				addBorderStyle("leftBorder", lineBox.getLeftPen());
				addBorderStyle("rightBorder", lineBox.getRightPen());
			}
			builder.append(">");
			addProperties(pt.getPropertiesMap());
			builder.append("</checkbox>");
		}
	}
	
	protected void exportButton(int xOrigin, int yOrigin, JRPrintText pt, int parentWidth, int parentHeight) {
		builder.append("<adhocButton ");
		addDefaultProperties(xOrigin, yOrigin, pt, parentWidth, parentHeight);
		builder.append(" bold=\"" + pt.isBold() + "\" italic=\"" + pt.isItalic() + "\" face=\""
				+ pt.getFontName() + "\" size=\"" + (Math.round(pt.getFontSize() * scaleFactor) - 1)
				+ "\" value=\"" + XmlUtils.encode(pt.getText()) + "\" ");
		builder.append(" alignment=\"");
		builder.append(pt.getHorizontalAlignmentValue() == HorizontalAlignEnum.LEFT ? "left" : 
			pt.getHorizontalAlignmentValue() == HorizontalAlignEnum.RIGHT ? "right" : "center");
		builder.append("\" ");
		if (pt.getText() != null && !pt.getText().isEmpty() && pt.getHyperlinkTypeValue() != HyperlinkTypeEnum.NULL && 
				pt.getHyperlinkTypeValue() != HyperlinkTypeEnum.NONE)
		{
			String target = getHyperlinkTarget(pt);
			String url = getHyperlinkURL(pt, 0);
			if (url != null) {
				boolean javascript = pt.getLinkType() != null ? pt.getLinkType().equals("javascript") : false;
				builder.append(" link=\"" + (javascript ? "javascript:" : "") + XmlUtils.encode(url) + "\""
					+ (target != null ? " target=\"" + target + "\"" : ""));
			}
		}
		builder.append(">");
		JRPropertiesMap properties = pt.getPropertiesMap();
		addProperties(properties);
		builder.append("</adhocButton>");
}
	protected void exportPromptGroupDropDown(int xOrigin, int yOrigin, JRPrintText pt, int parentWidth, int parentHeight)
	{
		Prompt p = promptMap.get(pt.getStyle().getName().substring("DROPDOWN_".length()));
		if (p != null && (p.getType() == PromptType.Query || p.getType() == PromptType.List))
		{
			builder.append("<dropdown ");
			addDefaultProperties(xOrigin, yOrigin, pt, parentWidth, parentHeight);
			/*builder.append(" bold=\"" + pt.isBold() + "\" italic=\"" + pt.isItalic() + "\" face=\""
			+ pt.getFontName() + "\" size=\"" + (Math.round(pt.getFontSize() * scaleFactor) - 1)
			+ "\" value=\"" + XmlUtils.encode(pt.getText()) + "\" ");*/
			builder.append("id=\"").append(XmlUtils.encode(pt.getText())).append("\" ");
			builder.append("name=\"").append(p.getParameterName()).append("\" ");
			builder.append("value=\"").append(XmlUtils.encode(pt.getText())).append("\" ");
			builder.append("alignment=\"");
			builder.append(pt.getHorizontalAlignmentValue() == HorizontalAlignEnum.LEFT ? "left" : 
				pt.getHorizontalAlignmentValue() == HorizontalAlignEnum.RIGHT ? "right" : "center");
			builder.append("\" ");
			JRLineBox lineBox = pt.getLineBox();
			if (lineBox != null) {
				addBorderStyle("topBorder", lineBox.getTopPen());
				addBorderStyle("bottomBorder", lineBox.getBottomPen());
				addBorderStyle("leftBorder", lineBox.getLeftPen());
				addBorderStyle("rightBorder", lineBox.getRightPen());
			}
			builder.append(">");
			
			if (p.getType() == PromptType.Query)
			{
				try
				{
					SMIWebUserBean userBean = SMIWebUserBean.getUserBean();
					Session session = userBean.getRepSession();
					session.setCancelled(false);
					JasperDataSourceProvider jdsp = userBean.getJasperDataSourceProvider();
					jdsp = new JasperDataSourceProvider(jdsp);					
					HashMap<String, String> hierarchicalMap = new HashMap<String, String>();
					HashMap<String, Object> parameters = new HashMap<String, Object>();
					p.fillQuery(jdsp, hierarchicalMap, parameters);
				}
				catch(Exception e)
				{
					logger.error("Unable to fill values for prompt.", e);
				}
			
				if (p.getFilledOptions() != null && p.getFilledOptionValues() != null && p.getFilledOptions().size() == p.getFilledOptionValues().size())
				{
					builder.append("<Options>");
					for (int i = 0; i < p.getFilledOptions().size(); i++) {
						builder.append("<Option>");
							builder.append("<Label>");
								builder.append(p.getFilledOptions().get(i));
							builder.append("</Label>");
							builder.append("<Value>");
								builder.append(p.getFilledOptionValues().get(i));
							builder.append("</Value>");
						builder.append("</Option>");
					}
					builder.append("</Options>");
				}
			}
			else if (p.getType() == PromptType.List)
			{
				if (p.getListValues() != null)
				{
					builder.append("<Options>");
					for (int i = 0; i < p.getListValues().size(); i++) {
						Prompt.ListValue lv = p.getListValues().get(i);
						builder.append("<Option>");
							builder.append("<Label>");
								builder.append(lv.getLabel());
							builder.append("</Label>");
							builder.append("<Value>");
								builder.append(lv.getValue());
							builder.append("</Value>");
						builder.append("</Option>");
					}
					builder.append("</Options>");
				}
			}
			addProperties(pt.getPropertiesMap());
			builder.append("</dropdown>");
		}
	}
	
	protected void exportPromptGroupEditableTextBox(int xOrigin, int yOrigin, JRPrintText pt, int parentWidth, int parentHeight)
	{
		Prompt p = promptMap.get(pt.getStyle().getName().substring("TEXTBOX_".length()));
		if (p != null && p.getType() == PromptType.Value)
		{
			builder.append("<editabletextbox ");
			addDefaultProperties(xOrigin, yOrigin, pt, parentWidth, parentHeight);
			/*builder.append(" bold=\"" + pt.isBold() + "\" italic=\"" + pt.isItalic() + "\" face=\""
			+ pt.getFontName() + "\" size=\"" + (Math.round(pt.getFontSize() * scaleFactor) - 1)
			+ "\" value=\"" + XmlUtils.encode(pt.getText()) + "\" ");*/
			builder.append("id=\"").append(XmlUtils.encode(pt.getText())).append("\" ");
			builder.append("name=\"").append(p.getParameterName()).append("\" ");
			builder.append("value=\"").append(XmlUtils.encode(pt.getText())).append("\" ");
			builder.append("alignment=\"");
			builder.append(pt.getHorizontalAlignmentValue() == HorizontalAlignEnum.LEFT ? "left" : 
				pt.getHorizontalAlignmentValue() == HorizontalAlignEnum.RIGHT ? "right" : "center");
			builder.append("\" ");
			JRLineBox lineBox = pt.getLineBox();
			if (lineBox != null) {
				addBorderStyle("topBorder", lineBox.getTopPen());
				addBorderStyle("bottomBorder", lineBox.getBottomPen());
				addBorderStyle("leftBorder", lineBox.getLeftPen());
				addBorderStyle("rightBorder", lineBox.getRightPen());
			}
			builder.append(">");
			addProperties(pt.getPropertiesMap());
			builder.append("</editabletextbox>");
		}
	}
	
	protected void exportPromptGroupButton(int xOrigin, int yOrigin, JRPrintText pt, int parentWidth, int parentHeight)
	{
		String operationName = null, id = null, name = null, visibleName = null, targetDashboardName = null, targetDashboardPage = null, imagePath = null, validationMessage = null, OptionsXML = null;
		Prompt p = promptMap.get(pt.getStyle().getName().substring("PROMPT_GROUP_".length()));
		if (p != null && p.getType() == PromptType.PromptGroup)
		{
			operationName = p.getOperationName();
			id = p.getParameterName();
			name = p.getParameterName();
			visibleName = p.getVisibleName();
			imagePath = p.getImagePath();
			targetDashboardName = p.getTargetDashboardName();
			validationMessage = p.getValidationMessage();
			StringBuilder optionsBuilder = new StringBuilder();
			if (p.getOptions() != null)
			{
				optionsBuilder.append("<Options>");
				for (String option : p.getOptions()) {
					optionsBuilder.append("<Option>");
					optionsBuilder.append(option);
					optionsBuilder.append("</Option>");
				}
				optionsBuilder.append("</Options>");
				OptionsXML = optionsBuilder.toString();
			}
		}
		else if (p == null && pt.getPropertiesMap() != null)
		{
			JRPropertiesMap map = pt.getPropertiesMap();
			operationName = map.getProperty(AdhocReport.JASPERREPORTS_EXPORT_PREFIX + "operationName");
			id = operationName;
			name = operationName;
			visibleName = operationName;
			imagePath = map.getProperty(AdhocReport.JASPERREPORTS_EXPORT_PREFIX + "imagePath");
			targetDashboardName = map.getProperty(AdhocReport.JASPERREPORTS_EXPORT_PREFIX + "targetDashboardName");
			targetDashboardPage = map.getProperty(AdhocReport.JASPERREPORTS_EXPORT_PREFIX + "targetDashboardPage");
			validationMessage = map.getProperty(AdhocReport.JASPERREPORTS_EXPORT_PREFIX + "validationMessage");
			OptionsXML = map.getProperty(AdhocReport.JASPERREPORTS_EXPORT_PREFIX + "OptionsXML");
		}
		if (operationName != null)
		{
			builder.append("<button ");
			addDefaultProperties(xOrigin, yOrigin, pt, parentWidth, parentHeight);
			/*builder.append(" bold=\"" + pt.isBold() + "\" italic=\"" + pt.isItalic() + "\" face=\""
			+ pt.getFontName() + "\" size=\"" + (Math.round(pt.getFontSize() * scaleFactor) - 1)
			+ "\" value=\"" + XmlUtils.encode(pt.getText()) + "\" ");*/
			builder.append("id=\"").append(id).append("\" ");
			builder.append("name=\"").append(name).append("\" ");
			builder.append("VisibleName=\"").append(visibleName).append("\" ");
			builder.append("targetDashboardName=\"").append(targetDashboardName).append("\" ");
			if (targetDashboardPage != null)
				builder.append("targetDashboardPage=\"").append(targetDashboardPage).append("\" ");
			builder.append("operationName=\"").append(operationName).append("\" ");
			builder.append("imagePath=\"").append(imagePath).append("\" ");
			builder.append("validationMessage=\"").append(validationMessage).append("\" ");
			builder.append(">");
			addProperties(pt.getPropertiesMap());
			if (OptionsXML != null)
			{
				builder.append(OptionsXML);				
			}
			if (p != null && p.getIncludedPromptIndices() != null)
			{
				builder.append("<IncludedPromptIndices>");
				for (Integer i : p.getIncludedPromptIndices()) {
					builder.append("<Included>");
					builder.append(i);
					builder.append("</Included>");
				}
				builder.append("</IncludedPromptIndices>");
			}
			builder.append("</button>");
		}
	}
	
	protected void addBorderStyle(String borderPrefix, JRBoxPen pen) {
		if (pen != null && !pen.getLineColor().equals(Color.white)) {
			Float width = pen.getLineWidth();
			if (width != null && width.floatValue()  > 0.0f) {
				builder.append(" " + borderPrefix + "Color=\"#");
				builder.append(JRColorUtil.getColorHexa(pen.getLineColor()));
				builder.append("\" " + borderPrefix + "Width=\"");
				builder.append(width);
				builder.append("\" " + borderPrefix + "Style=\"");
				builder.append(pen.getLineStyleValue() == LineStyleEnum.DOUBLE ? "double" :
					pen.getLineStyleValue() == LineStyleEnum.DASHED ? "dashed" :
						pen.getLineStyleValue() == LineStyleEnum.DOTTED ? "dotted" : "solid"); 
				builder.append("\" ");
			}
		}
	}
	
	protected void exportImage(int xOrigin, int yOrigin, JRTemplatePrintImage tpi, int parentWidth, int parentHeight) throws JRException {
		JRRenderable renderer = tpi.getRenderer();
		if (renderer == null)
		{
			// error text
			int fontSize = 14;
			builder.append("<text ");
			builder.append(" alignment=\"center\" ");
			addDefaultProperties(xOrigin, yOrigin, tpi, parentWidth, parentHeight);
			builder.append(" size=\"" + Math.round(fontSize * scaleFactor) + "\" value=\"Error\" tooltip=\"No chart or image created - no data, invalid data, or invalid configuration\" >");
			builder.append("</text>");
		} else if (renderer.getImageType() == (byte) 255)
		{
			String xml = ((XMLExporter) renderer).getXML(scaleFactor, false, false);
			if (xml == null)
				return;
			
			String type = "chart";
			if (renderer instanceof FlashRenderer) {
				ChartOptions co = ((FlashRenderer)renderer).getChartOptions();
				if (co.type == ChartOptions.ChartType.MeterPlot) {
					type = "gauge";
				}
			}
			else if (renderer instanceof AnyChartRenderer) {
				AdhocChart ac = ((AnyChartRenderer)renderer).getAdhocChart();
				if (ac.isTrellis()){ //Trellis chart will generate its own <chart/>..
					type = "frame";
				}else{
					List<ChartType> chartTypes = ac.getChartTypes();
					if (chartTypes.contains(ChartType.Gauge) || chartTypes.contains(ChartType.NewGauge))
						type = "gauge";
				}
			}
			
			builder.append("<" + type + " ");
			addDefaultProperties(xOrigin, yOrigin, tpi, parentWidth, parentHeight);
			JRLineBox lineBox = tpi.getLineBox();
			if (lineBox != null) {
				addBorderStyle("topBorder", lineBox.getTopPen());
				addBorderStyle("bottomBorder", lineBox.getBottomPen());
				addBorderStyle("leftBorder", lineBox.getLeftPen());
				addBorderStyle("rightBorder", lineBox.getRightPen());
			}
			
			if (renderer instanceof AnyChartRenderer) {
				AdhocChart ac = ((AnyChartRenderer)renderer).getAdhocChart();
				builder.append(" reportIndex=\"" + ac.getReportIndex() + "\"");
			}
			builder.append(">");
			builder.append(xml);
			addProperties(tpi.getPropertiesMap());
			builder.append("</" + type + ">");
		} else if (renderer instanceof JRRenderable)
		{
			exportImage((JRPrintImage) tpi);
		}
	}
	

	protected String getHyperlinkTarget(JRPrintHyperlink h)
	{
		String target = null;
		if (h.getHyperlinkTargetValue() == HyperlinkTargetEnum.BLANK)
		{
			target = "_blank";
		}
		return target;
	}

	protected String getHyperlinkURL(JRPrintHyperlink h, int page)
	{
		String href = null;
		if (h.getHyperlinkTypeValue() == HyperlinkTypeEnum.CUSTOM)
		{
			if (h.getHyperlinkReference() != null)
				return h.getHyperlinkReference().trim();
			else if (h.getHyperlinkAnchor() != null)
				return h.getHyperlinkAnchor().trim();
		}
		else if (h.getHyperlinkTypeValue() == HyperlinkTypeEnum.REFERENCE)
		{
			if (h.getHyperlinkReference() != null)
			{
				href = h.getHyperlinkReference().trim();
			}
		}
		else if (h.getHyperlinkTypeValue() == HyperlinkTypeEnum.LOCAL_ANCHOR)
		{
			if (h.getHyperlinkAnchor() != null)
			{
				href = "#" + h.getHyperlinkAnchor().trim();
			}
		}
		else if (h.getHyperlinkTypeValue() == HyperlinkTypeEnum.LOCAL_PAGE)
		{
			if (page > 0)
			{
				href = "#" + JR_PAGE_ANCHOR_PREFIX + reportIndex + "_" + page;
			}
		}
		else if (h.getHyperlinkTypeValue() == HyperlinkTypeEnum.REMOTE_ANCHOR)
		{
			if (h.getHyperlinkReference() != null && h.getHyperlinkAnchor() != null)
			{
				href = h.getHyperlinkReference().trim() + "#" + h.getHyperlinkAnchor().trim();
			}
		}
		else if (h.getHyperlinkTypeValue() == HyperlinkTypeEnum.REMOTE_PAGE)
		{
			if (h.getHyperlinkReference() != null && page > 0)
			{
				href = h.getHyperlinkReference().trim() + "#" + JR_PAGE_ANCHOR_PREFIX + "0_" + page;
			}
		}
		return href;
	}

	protected JRHyperlinkProducer getCustomHandler(JRPrintHyperlink link)
	{
		return hyperlinkProducerFactory == null ? null : hyperlinkProducerFactory.getHandler(link.getLinkType());
	}

	protected boolean appendBorderStyle(JRLineBox box, StringBuilder styleBuffer)
	{
		boolean addedToStyle = false;
		if (box != null)
		{
			addedToStyle |= appendPen(styleBuffer, box.getTopPen(), "top");
			addedToStyle |= appendPadding(styleBuffer, box.getTopPadding(), "top");
			addedToStyle |= appendPen(styleBuffer, box.getLeftPen(), "left");
			addedToStyle |= appendPadding(styleBuffer, box.getLeftPadding(), "left");
			addedToStyle |= appendPen(styleBuffer, box.getBottomPen(), "bottom");
			addedToStyle |= appendPadding(styleBuffer, box.getBottomPadding(), "bottom");
			addedToStyle |= appendPen(styleBuffer, box.getRightPen(), "right");
			addedToStyle |= appendPadding(styleBuffer, box.getRightPadding(), "right");
		}
		return addedToStyle;
	}

	/**
	 * 
	 */
	int imagecount = 0;

	@SuppressWarnings("unchecked")
	protected void exportImage(JRPrintImage image) throws JRException
	{
		String anchor = image.getAnchorName();
		JRStyle istyle = image.getStyle();
		JRRenderable renderer = image.getRenderer();
		boolean useAlt = false;
		if (anchor != null && istyle != null && istyle.getName().equals(JRFlexExporter.ALT_HEADER_STYLE))
			useAlt = true;
		byte scaleImage = image.getScaleImageValue().getValue();
		if (renderer != null)
		{
			builder.append("<image x=\"" + scalePx(image.getX()) + "\" y=\"" + scalePx(image.getY()) + "\"");
			if (image.getHyperlinkTypeValue() != HyperlinkTypeEnum.NULL && image.getHyperlinkTypeValue() != HyperlinkTypeEnum.NONE)
			{
				String target = getHyperlinkTarget(image);
				String url = getHyperlinkURL(image, 0);
				if (url != null)
				{
					boolean javascript = image.getLinkType() != null ? image.getLinkType().equals("javascript") : false;
					builder.append(" link=\"" + (javascript ? "javascript:" : "") + XmlUtils.encode(url) + "\""
						+ (target != null ? " target=\"" + target + "\"" : ""));
				}
			}			builder.append(" backcolor=\"#" + JRColorUtil.getColorHexa(image.getBackcolor()) + "\"");
			if (useAlt)
			{
				builder.append(" alt=\"" + anchor + "\"");
			}
			int imageWidth = image.getWidth();
			int imageHeight = image.getHeight();
			if (scaleImage == ScaleImageEnum.FILL_FRAME.getValue())
			{
				builder.append(" width=\"");
				builder.append(scalePx(imageWidth));
				builder.append("\" height=\"");
				builder.append(scalePx(imageHeight));
				builder.append("\"");
			}
			else if (scaleImage == JRFlexExporter.SMI_NO_SCALE_IMAGE) {
			} else 
			{
				if (!image.isLazy())
				{
					// Image load might fail.
					JRRenderable tmpRenderer = JRImageRenderer.getOnErrorRendererForDimension(renderer, image.getOnErrorTypeValue());
					tmpRenderer.getDimension();
					// If renderer was replaced, ignore image dimension.
				}
				if (imageHeight > 0)
				{
					//Bug 11570 Embedded images are not scaled correctly
					builder.append(" width=\"");
					builder.append(imageWidth);
					builder.append("\"");
					builder.append(" height=\"");
					builder.append(imageHeight);
					builder.append("\"");
				}
			}
			builder.append(">");
			String imagePath = null;
			if (renderer != null)
			{
				if (renderer.getType() == JRRenderable.TYPE_IMAGE && rendererToImagePathMap.containsKey(renderer.getId()))
				{
					imagePath = (String) rendererToImagePathMap.get(renderer.getId());
				} else
				{
					if (image.isLazy())
					{
						imagePath = ((JRImageRenderer) renderer).getImageLocation();
						// add on the servlet context
						imagePath = servletContext + imagePath;
					} else
					{
						//
						JRPrintPage page = (JRPrintPage)this.jasperPrint.getPages().get(this.pageIndex);
						List list = page.getElements();
						int index = 0;
						for (; index < list.size(); index++) {
							if (list.get(index) == image) {
								break;
							}
						}
						List<JasperPrint> jasperPrintList = SMIWebUserBean.getJasperPrintObject(null);
						JRPrintElementIndex imageIndex = new JRPrintElementIndex(jasperPrintList.indexOf(this.jasperPrint), this.pageIndex, String.valueOf(index));
						imagesToProcess.add(imageIndex);
						String imageName = getImageName(imageIndex);
						imagePath = imagesURI + imageName;
						// backward compatibility with the IMAGE_MAP parameter
						if (imageNameToImageDataMap != null)
						{
							if (renderer.getType() == JRRenderable.TYPE_SVG)
							{
								renderer = new JRWrappingSvgRenderer(renderer, new Dimension(image.getWidth(), image.getHeight()),
										ModeEnum.OPAQUE == image.getModeValue() ? image.getBackcolor() : null);
							}
							imageNameToImageDataMap.put(imageName, renderer.getImageData());
						}
						// END - backward compatibility with the IMAGE_MAP parameter
					}
					rendererToImagePathMap.put(renderer.getId(), imagePath);
				}
			}
			if (imagePath != null)
				builder.append(imagePath);
			builder.append("</image>");
		}
	}

	/**
	 * 
	 */
	protected void loadPxImage() throws JRException
	{
		isPxImageLoaded = true;
		// backward compatibility with the IMAGE_MAP parameter
		if (imageNameToImageDataMap != null && !imageNameToImageDataMap.containsKey("px"))
		{
			JRRenderable pxRenderer = JRImageRenderer.getInstance("net/sf/jasperreports/engine/images/pixel.GIF");
			rendererToImagePathMap.put(pxRenderer.getId(), imagesURI + "px");
			imageNameToImageDataMap.put("px", pxRenderer.getImageData());
		}
		// END - backward compatibility with the IMAGE_MAP parameter
	}

	/**
	 * 
	 */
	protected static interface StringProvider
	{
		/**
		 * 
		 */
		public String getStringForCollapsedTD(Object valuet);

		/**
		 * 
		 */
		public String getStringForEmptyTD(Object value);
	}

	/**
	 * 
	 */
	private boolean appendPadding(StringBuilder sb, Integer padding, String side)
	{
		boolean addedToStyle = false;
		if (padding.intValue() > 0)
		{
			sb.append("padding");
			if (side != null)
			{
				sb.append("-");
				sb.append(side);
			}
			sb.append(": ");
			sb.append(padding);
			sb.append(sizeUnit);
			sb.append("; ");
			addedToStyle = true;
		}
		return addedToStyle;
	}

	/**
 	 *
 	 */
	private boolean appendPen(StringBuilder sb, JRPen pen, String side)
	{
		boolean addedToStyle = false;
		float borderWidth = pen.getLineWidth().floatValue();
		if (0f < borderWidth && borderWidth < 1f)
		{
			borderWidth = 1f;
		}
		String borderStyle = null;
		if (pen.getLineStyleValue() == LineStyleEnum.DOUBLE)
		{
			borderStyle = "double";
		}
		else if (pen.getLineStyleValue() == LineStyleEnum.DOTTED)
		{
			borderStyle = "dotted";
		}
		else if (pen.getLineStyleValue() == LineStyleEnum.DASHED)
		{
			borderStyle = "dashed";
		}
		else
		{
			borderStyle = "solid";
		}
		if (borderWidth > 0f)
		{
			sb.append("border");
			if (side != null)
			{
				sb.append("-");
				sb.append(side);
			}
			sb.append("-style: ");
			sb.append(borderStyle);
			sb.append("; ");
			sb.append("border");
			if (side != null)
			{
				sb.append("-");
				sb.append(side);
			}
			sb.append("-width: ");
			sb.append((int) borderWidth);
			sb.append(sizeUnit);
			sb.append("; ");
			sb.append("border");
			if (side != null)
			{
				sb.append("-");
				sb.append(side);
			}
			sb.append("-color: #");
			sb.append(JRColorUtil.getColorHexa(pen.getLineColor()));
			sb.append("; ");
			addedToStyle = true;
		}
		return addedToStyle;
	}

	/**
	 * 
	 */
	public static String getImageName(JRPrintElementIndex printElementIndex)
	{
		return IMAGE_NAME_PREFIX + printElementIndex.toString();
	}

	/**
	 * 
	 */
	public static JRPrintElementIndex getPrintElementIndex(String imageName)
	{
		if (!imageName.startsWith(IMAGE_NAME_PREFIX))
		{
			throw new JRRuntimeException("Invalid image name: " + imageName);
		}
		return JRPrintElementIndex.parsePrintElementIndex(imageName.substring(IMAGE_NAME_PREFIX_LEGTH));
	}


	protected void setBackcolor(Color color)
	{
		backcolorStack.addLast(backcolor);
		backcolor = color;
	}

	protected void restoreBackcolor()
	{
		backcolor = backcolorStack.removeLast();
	}

	public static String encodeForXhtml(String text)
	{
		return XmlUtils.encode(text);
		/*
		if (text != null)
		{
			int length = text.length();
			if (length > 0)
			{
				StringBuilder ret = new StringBuilder(length * 12 / 10);
				int last = 0;
				for (int i = 0; i < length; i++)
				{
					char c = text.charAt(i);
					switch (c)
					{
					case '&':
						if (text.substring(i).startsWith("&amp;"))
						{
							continue;
						}
						if (last < i)
						{
							ret.append(text.substring(last, i));
						}
						last = i + 1;
						ret.append("&amp;");
						break;
					}
				}
				if (last < length)
				{
					ret.append(text.substring(last));
				}
				return ret.toString();
			}
		}
		return text;
		*/
	}

	@Override
	public String getExporterKey() {
		return FLEX_EXPORTER_KEY;
	}
}
