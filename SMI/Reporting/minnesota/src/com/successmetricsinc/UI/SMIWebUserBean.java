/**
 * $Id: SMIWebUserBean.java,v 1.16 2012-10-03 01:15:13 BIRST\ricks Exp $
 *
 * Copyright (C) 2007-2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.successmetricsinc.UI;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperPrint;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.input.SAXBuilder;

import com.successmetricsinc.Group;
import com.successmetricsinc.Repository;
import com.successmetricsinc.Repository.ACLItem;
import com.successmetricsinc.Session;
import com.successmetricsinc.SessionHelper;
import com.successmetricsinc.SessionTimeoutListener;
import com.successmetricsinc.PackageSpaceProperties;
import com.successmetricsinc.User;
import com.successmetricsinc.UserBean;
import com.successmetricsinc.adhoc.AdhocReportHelper;
import com.successmetricsinc.adhoc.DrillMapList;
import com.successmetricsinc.catalog.CatalogManager;
import com.successmetricsinc.query.JasperDataSourceProvider;
import com.successmetricsinc.query.ResultSetCache;
import com.successmetricsinc.queuing.AsynchReportGeneration.AsynchReportResultSubscriber;
import com.successmetricsinc.util.SessionUtils;

/**
 * @author agarrison
 *
 */
public class SMIWebUserBean extends com.successmetricsinc.UserBean {
	public static final String PERM_ADHOC = "Adhoc";
	public static final String PERM_EDIT_DASHBOARD = "EditDashboard";
	public static final String PERM_ENABLE_DOWNLOAD = "EnableDownload";
	public static final String PERM_ENABLE_SELF_SCHEDULE = "SelfScheduleAllowed";
	public static final String PERM_ENABLE_ASYNC_REPORT_GENERATION = "EnableAsyncReportGeneration";
	public static final String PERM_IS_DISCOVER_FREE_TRIAL = "IsDiscoverFreeTrial";
	public static final String PERM_IS_OLAP_CUBE = "IsOlapCube";
	public static final String PERM_EDIT_REPORT_CATALOG = "EditReportCatalog";
	public static final String PERM_MODIFY_SAVED_EXPRESSIONS = "ModifySavedExpressionsAllowed";
	
	public static Logger logger = Logger.getLogger(SMIWebUserBean.class);
	public static final String AUTO_REPORT_DIR      = "/Auto-generated";

	private static String SESSION_END_ATTRIBUTE = "SessionEndAttribute";
	private List<Group> groupList;
    private List<Group> testGroupList;
    private Repository testRepository;
	private Boolean isAdmin; // TODO: deprecate this, use isAdministrator
	private Map<String,Boolean> roles = null; // new HashMap<String,Boolean>();
	private ApplicationContext applicationContext;
	private List<File> rootDirectories;
	private static UserBean adminUserBean = createAdminUserBean(); 
	private boolean superUser;
	private boolean embedded = false;
	private String pageTitle;
	private boolean isAdministrator = false;
	private String userID;
	private String spaceID;	
	// overriden session variables 
	private List<String> overridenSessionVariables;
	private DrillMapList drillMaps;
	private boolean discoverySpace = false;
	private boolean freeTrial = false;
	private boolean discoveryFreeTrial = false;
	private List<PackageSpaceProperties> spacesPropertiesList;
	private String SFDC_sessionId;
	private String SFDC_serverURL;
	private long userBeanContextReloadTime;
	private String aspSessionId;
	private String jSessionId;
	private boolean visualizerEnabled = false;
	private boolean asyncReportGenerationEnabled = false;
	private Map<String, JasperPrint> exportPrintList = new HashMap<String, JasperPrint>();
	
	public SMIWebUserBean(){
		super();
	}
	
	public String getPageTitle() {
		return pageTitle;
	}
	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public static Locale CURRENT_LOCALE = Locale.getDefault();
	
	//TODO: deprecate this
	private static UserBean createAdminUserBean() {
		SMIWebUserBean bean = new SMIWebUserBean();
		bean.isAdmin = Boolean.TRUE;
		return bean;
	}
	public boolean isEmbedded() {
		return embedded;
	}

	public void setEmbedded(boolean embedded) {
		this.embedded = embedded;
	}

	public static SMIWebUserBean getUserBean() {
		SMIWebUserBean userBean = null;
		try {
			userBean = SessionUtils.getUserBean();			
		}
		catch (Exception e) {
			logger.debug(e, e);
		}

		if (userBean == null) {
			com.successmetricsinc.UserBean ub = com.successmetricsinc.UserBean.getParentThreadUserBean();
			if (ub != null && ub instanceof SMIWebUserBean) {
				userBean = (SMIWebUserBean)ub;
			}
		}
		
		if (userBean == null)
		{
			HttpSession session = getHttpSession();
			if (session == null) {
				return null;
			}
			userBean = (SMIWebUserBean) session.getAttribute("userBean");			
		}
		
		if (userBean == null)
		{
			logger.debug("userBean attribute not found in the session");
			return null;
		}

		createApplicationContext(userBean);
		if (userBean.getApplicationContext() == null || !userBean.getApplicationContext().isValid())
			return null;

		return userBean;
	}

	private static void createApplicationContext(SMIWebUserBean ub)
	{
		// see if we need to get user specific server context information
		if (ub.applicationContext == null)
		{
			// get application name from the user principal
			String principal;
			principal = getHttpServletRequest().getUserPrincipal().getName();
			if (principal == null)
			{
				logger.warn("Null principal specification");
				return;
			}
			String[] parts = principal.split(",");
			if (parts == null || parts.length != 3)
			{
				logger.warn("Invalid principal specification - " + principal );
				return;
			}
			ub.applicationContext = ApplicationContext.getInstance(parts[1], ub.spacesPropertiesList);
		}
	}

	public String getUserName()
	{
		return getUser().getUserName();
	}
	
	/**
	 * Gets the list of all external (non-internal) groups for the current space.
	 * Allows for a tweak to include the USER$ internal group since this is 
	 * visible to certain sub-applications of Birst (i.e., CatMan & Custom Subject
	 * Areas).
	 * @param includeUserGroup if true then include the internal group -- "USER$" --
	 * in the final list to be returned; otherwise return purely external groups.
	 * @return the list of all non-internal groups for the current space, optionally
	 * including the "USER$" group if requested via the 'includeUserGroup' parameter.
	 */
	public List<Group> getExternalGroupList(final boolean includeUserGroup) {
		final List<Group> externalGroups = new ArrayList<Group>();
		for (Group group : getGroupList(false)) {
			if (includeUserGroup && group.getName().equals(Group.GROUP_USER$)) {
				externalGroups.add(group);
			}
			if (!group.isInternalGroup()) { 
				externalGroups.add(group);
			}
		}
		return externalGroups;
	}
	
	public Session getRepSession() {
		return getRepSession(true);
	}
	
	public Session getRepSession(boolean createIfNull) {
		return getRepSession(createIfNull, null);
	}
	
	/**
	 *  When searchVarsSet argument is provided by caller, lightweight session is created with only those session variables that are requested by caller.
	 *  This parameter should be used with caution and only be used in cases when caller only requires to replace selective variables and does not require 
	 *  to initialize all session variables for user.
	 *  Currently only BirstConnect and Connectors provide specific list of variables that need to be replaced when initializing userbean.
	 * 
	 */
	public Session getRepSession(boolean createIfNull, Set<String> searchVarsSet) {
		if (repSession == null && createIfNull) {
			User user = getUser();
			if (user == null)
				return null;
			repSession = new Session(user.getUserName());
			repSession.setCatalogDir(this.getCatalogPath());
			try {
				repSession.fillSessionVariables(getRepository(), getCache(), searchVarsSet);
			} catch (SQLException e) {
				logger.warn("Can not fill session variables", e);
			}
			setSessionTimeOutListener(repSession);
			if (!getIsAdmin())
			{
				HttpServletRequest req = getHttpServletRequest();
				if (req != null && req.isUserInRole("DemoMode"))
				{
					repSession.setDemoMode(true);
					logger.info("Setting demo mode for " + getUser().getUserName());
				}
			}
			logger.info("Session variables (UserBean:getRepSession): " + repSession.toString());
		}
		return repSession;
	}
	
	/**
	 * Returns a list of groups that the current user belongs to.
	 * @param fillSession is used to indicate whether to fill session variables or not. 
	 * Used by the Catalog Management for faster session creation.
	 * @return List of Groups for the User
	 */
	public List<Group> getGroupList(boolean fillSession) {
        if (testGroupList != null) {
            return testGroupList;
        }
		User user = getUser();
		Session repSession = getRepSession(fillSession);
		groupList = getRepository().getUserGroups(user, repSession);
		return groupList;
	}
	
	/**
	 * This is an "enhanced" version of the idiom: UserBean.getUserBean().getGroupList()
	 * in that checks to see if the user is a superuser and grants them access to all
	 * the groups in that space, regardless of what their specific group assignments are.
	 * Otherwise it retrieves the regular set of groups for that user.
	 * @return the set of all groups that this user is a member of.
	 */
	public final List<Group> getUserGroups() {
		// If superuser (Birst Service) set to all groups in space
		if (isSuperUser()) {
			return getRepository().getGroups(true);
		}
		
		// Otherwise just get the list of groups for the user
		// pass the flag to not fill session variables - not required - bugfix 7463
        return getGroupList(false);
	}
	
	// set the session timeout listener
	public void setSessionTimeOutListener(Session session)
	{
		Object httpSession = null;
		httpSession = getHttpSession();
		if (httpSession != null && httpSession instanceof HttpSession) {
			HttpSession ses = (HttpSession) httpSession;
			String id = ses.getId();
			if (ses.getAttribute(SESSION_END_ATTRIBUTE) == null)
			{
				ses.setAttribute(SESSION_END_ATTRIBUTE, new SessionTimeoutListener(getUser().getUserName(), id, applicationContext, session));
			}
		}
		else
		{
			if (httpSession == null)
				logger.warn("Session is null");
			else
				logger.warn("Session from external context is not an HttpSession");
		}
	}
	
	//TODO: deprecate this
	public Boolean getIsAdmin() {
		if (isAdmin == null) {
			isAdmin = Boolean.valueOf(getRepository().isAdministrator(getUser()));
		}
		return isAdmin;
	}
	
	public boolean getCanDownload() {
		return hasRole("EnableDownload");
	}
	
	public boolean hasAdhocPermission()
	{
		return hasRole(PERM_ADHOC);
	}
	
	public boolean hasEditDashboardPermission()
	{
		return hasRole(PERM_EDIT_DASHBOARD);
	}
	
	public boolean hasRole(String role) {
		if (roles != null && roles.containsKey(role))
		{
			return roles.get(role);
		}
		boolean res = false;
		HttpServletRequest request = SessionHelper.getHttpServletRequest();
		if (request == null) {
			request = SMIWebUserBean.getHttpServletRequest();
		}
		res = request.isUserInRole(role);
		if(!res)
		{	
			// if we cannot find role based on groups/acls in the repository.
			// try it with dynamic groups. This should already be populated during login session
			res = checkHasRoleWithDynamicGroups(role);
		}
		if (roles != null)
			roles.put(role, res);
		return res;
	}

	/**
	 * Check for the existence of Dynamic Group Variable in repository. 
	 * Dynamic Group Variable is in the form of a logical query which if found is executed.
	 * An alternative is to pass group variable in SSO request session variables.
	 * We first check for Dynamic Group Variable Name (Birst$Groups) in the SSO passed session variables
	 * and use them instead of any repository based variable. 
	 * @param role - ACL tag name in the repository
	 * @return
	 */
	private boolean checkHasRoleWithDynamicGroups(String role)
	{
		boolean hasRole = false;	
		try
		{
			Repository r = getRepository();
			boolean userHasDynamicGroups = r.hasDynamicGroups();
			if(!userHasDynamicGroups)
			{
				List<Group> gList = r.getDynamicGroups(getRepSession());
				if(gList != null && gList.size() > 0)
				{
					userHasDynamicGroups = true;
				}
			}
			
			if(role != null && userHasDynamicGroups && r.getAccessControlLists() != null)
			{
				List<ACLItem> list = r.getAccessControlLists().get(role);
				if (list != null && list.size() > 0)
				{
					List<Group> userDynamicGroups = r.getDynamicGroups(getRepSession());
					for (ACLItem item : list)
					{
						if (item.isAccess() && item.getTag().equals(role))
						{
							for (Group g : userDynamicGroups)
							{
								if (g.getName().equals(item.getGroup()))
								{
									hasRole = true;
									break;
								}
							}
							
							if(hasRole)
							{
								break;
							}
						}
					}
				}
			}
		}
		catch(Exception ex)
		{
			logger.error("Exception while finding role : " + role, ex);
		}
		return hasRole;
	}
	
	public JasperDataSourceProvider getJasperDataSourceProvider() throws SQLException {
		return new JasperDataSourceProvider(getRepository());
	}

	public CatalogManager getCatalogManager() {
		return getApplicationContext().getCatalogManager();
	}
	
	private Locale _sessionLocale = null;
	private String acornRoot;
	private String schedulerRoot;
	private boolean hasBingMaps;
	private String gaTrackingAccount;
	private String aspxFormsAuth;	
	
	public Locale getSessionLocale()
	{
		if (_sessionLocale == null){
			_sessionLocale = getHttpServletRequest().getLocale();
			CURRENT_LOCALE = _sessionLocale;
		}
		return _sessionLocale;
	}
	
	public void setSessionLocale(Locale newval)
	{
		_sessionLocale = newval;
	}

	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		if (user != null)
		{
			sb.append("User: " + user.toString() + "; ");
		}
		if (repSession != null)
		{
			sb.append("RepSession: " + repSession.toString() + "; ");
		}
		if (groupList != null)
		{
			sb.append("Groups: " + groupList.toString() + "; ");
		}
		if (roles != null)
		{
			boolean first = true;
			sb.append("Roles: [");
			for (String role : roles.keySet())
			{
				if (roles.get(role))
				{
					if (!first)
						sb.append(", ");
					sb.append(role);
					first = false;
				}
			}
			sb.append("]; ");
		}
		return sb.toString();
	}

	// methods that access ServerContext
	public Repository getRepository() {
        if (testRepository != null) {
            return testRepository;
        }
		return getApplicationContext().getRepository();
	}

	public String getCatalogPath() {
		return this.getCatalogManager().getCatalogRootPath();
	}

	public ResultSetCache getCache() {
		return getApplicationContext().getCache();
	}

	public ApplicationContext getApplicationContext() {
		if (applicationContext == null)
			createApplicationContext(this);
		return applicationContext;
	}
	
	public void setApplicationContext(ApplicationContext ac) {
		this.applicationContext = ac;
	}

	public List<File> getRootDirectories() {
		return rootDirectories;
	}
	public void setRootDirectories(List<File> rootDirectories) {
		this.rootDirectories = rootDirectories;
	}
	public static UserBean getAdminUserBean() {
		return adminUserBean;
	}

	/**
	 * Looks to see if user passed from Birst (Acorn) is superuser.
	 * The superuser flag is set in TrustedLoginWS. 
	 * @return true if user logged in as a superuser; false otherwise.
	 */
	public boolean isSuperUser() {
		return superUser;	
	}
	
	public void setSuperUser(boolean userType) {
		superUser = userType;
	}

	/**
	 * Determines if the current user is an OWNER$ of the current space.
	 * @return true if the current user is a member of the OWNER$ group for the
	 * current space; false otherwise.
	 */
	public boolean isOwner() {
		final UserBean userBean = SMIWebUserBean.getUserBean();
		final User user = userBean.getUser();
		final List<Group> groups = userBean.getRepository().getUserGroups(user, null);
		boolean isOwner = false;
		if (groups != null) {
			for (Group group : groups) {
				if (group.getName().equals(Group.GROUP_OWNER$)) {
					isOwner = true;
					break;
				}
			}
		}
		return isOwner;
	}
	
	public boolean isSuperUserOwnerOrAdmin() {
		return (isSuperUser() || isOwner() || getIsAdmin() || isAdministrator());
	}
	
	public boolean hasEditReportCatalog() {
		return hasRole(PERM_EDIT_REPORT_CATALOG);
	}
	
    /**
     * Like it sounds... this is for unit testing only.  Tries to avoid the
     * lengthy setup required for full webapp/webservices testing.
     * @param testGroupList the mocked-up version of the 'groupList' variable.
     */
    public void setTestGroupList(List<Group> testGroupList) {
        this.testGroupList = testGroupList;
    }

    /**
     * Allows a unit test version of the repository to be loaded into the user bean.
     * @param testRepository is the mocked-up repository instance.
     */
    public void setTestRepository(Repository testRepository) {
        this.testRepository = testRepository;
    }
    
    public static List<JasperPrint> getJasperPrintObject(HttpServletRequest request) {
    	if (request != null) {
	    	HttpSession session = request.getSession();
	    	if (session != null) {
	    		com.successmetricsinc.UserBean ub = (com.successmetricsinc.UserBean) session.getAttribute("userBean");
	    		if (ub != null)
	    			return ub.getJasperPrintObject();
	    	}
    	}
		UserBean userBean = SMIWebUserBean.getUserBean();
		if (userBean != null)
			return userBean.getJasperPrintObject();

		com.successmetricsinc.UserBean ub = com.successmetricsinc.UserBean.getParentThreadUserBean();
		if (ub != null)
			return ub.getJasperPrintObject();

		return null;
    }
    
    /**
     * To be set by Acorn
     * @param isAdministrator
     */
    public void setIsAdministrator(boolean isAdministrator) {
    	this.isAdministrator = isAdministrator;
    }
    
    /**
     * Does the user belong to the Administrator group? This is set by Acorn.
     * Used by dashboards to show Global Properties to Administrators only.
     * Default is false.
     * @return
     */
    public boolean isAdministrator() {
    	return isAdministrator;
    }
    
	public String getSpaceID() {
		return spaceID;
	}
	public void setSpaceID(String spaceID) {
		this.spaceID = spaceID;
	}
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	public void setOverridenSessionVariables(List<String> overridenSessionVariables) {
		this.overridenSessionVariables = overridenSessionVariables;
	}
	
	public List<String> getOverridenSessionVariables()
	{
		return overridenSessionVariables;
	}
	
	public DrillMapList getDrillMaps() {
		return drillMaps ;
	}
	public void setDrillMaps(DrillMapList drillMaps) {
		this.drillMaps = drillMaps;
	}
	
	public void initializeDrillMapsList()
	{
		DrillMapList drillMapList = null;
		String drillMapsFileName = null;
		try
		{	
			String path = getRepository().getServerParameters().getApplicationPath();
			String drillMapsFile = DrillMapList.getDrillMapsFileName(path);
			File file = new File(drillMapsFile);
			if (file.exists())
			{
				drillMapList = DrillMapList.read(file);
				drillMapList.validate(getRepository());
				drillMapList.populateDrillDownMap(this);
			}
		}
		catch (Exception ex)
		{
			logger.error("Exception in loading drill maps file " + drillMapsFileName, ex);
		}

		finally
		{
			// create a blanket list
			if(drillMapList == null)
			{
				drillMapList = new DrillMapList();
			}
			setDrillMaps(drillMapList);
		}
	}
	public void setDiscoverySpace(boolean discoverySpace)
	{
		this.discoverySpace = discoverySpace;
	}
	
	public boolean isDiscoverySpace() 
	{
		return discoverySpace;
	}

	public void setFreeTrial(boolean freeTrial)
	{
		this.freeTrial = freeTrial;
	}
	
	public boolean isFreeTrial() 
	{

		return freeTrial;
	}
	
	public void setDiscoveryFreeTrial(boolean freeTrial) {
		this.discoveryFreeTrial = freeTrial;
	}
	
	public boolean isDiscoveryFreeTrial() {
		return this.discoveryFreeTrial;
	}
	
	public void setVisualizerFlag(boolean enable){
		this.visualizerEnabled = enable;
	}
	
	public boolean isVisualizerEnabled(){
		return this.visualizerEnabled;
	}
	
	public void addSpacePropertiesToList(PackageSpaceProperties spaceProperties)
	{
		if(spaceProperties != null)
		{
			if(spacesPropertiesList == null)
			{
				spacesPropertiesList = new ArrayList<PackageSpaceProperties>();
			}			
			spacesPropertiesList.add(spaceProperties);			
		}
	}
	
	public List<PackageSpaceProperties> getSpacesPropertiesList() {
		return spacesPropertiesList;
	}
	
	public void setSpacePropertiesList(List<PackageSpaceProperties> spacesPropertiesList) {
		this.spacesPropertiesList = spacesPropertiesList;
	}
	public String getSFDC_sessionId() {
		return SFDC_sessionId;
	}
	public void setSFDC_sessionId(String sFDC_sessionId) {
		SFDC_sessionId = sFDC_sessionId;
	}
	public String getSFDC_serverURL() {
		return SFDC_serverURL;
	}
	public void setSFDC_serverURL(String sFDC_serverURL) {
		SFDC_serverURL = sFDC_serverURL;
	}
	
	public boolean isUserBeanAppContextModified(){
		long appContextReloadTime = getApplicationContext().getReloadContextTime();
		if(userBeanContextReloadTime > 0 && appContextReloadTime > 0 && appContextReloadTime > userBeanContextReloadTime){
			return true;
		}
		
		return false;
	}
	
	public void syncUpUserBeanAppContextTime()
	{
		userBeanContextReloadTime = getApplicationContext().getReloadContextTime(); 
	}
	public void setFlashVars(String asp_net_sessionId, String id, String acornRoot, String schedulerRoot, boolean hasBingMaps, String gaTrackingAccount, String aspxformsauth) {
		aspSessionId = asp_net_sessionId;
		jSessionId = id;
		this.acornRoot = acornRoot;
		this.schedulerRoot = schedulerRoot;
		this.hasBingMaps = hasBingMaps;
		this.gaTrackingAccount = gaTrackingAccount;
		this.aspxFormsAuth = aspxformsauth;
	}
	
	public String getAcornRoot() {
		return acornRoot;
	}
	public String getSchedulerRoot() {
		return schedulerRoot;
	}
	public boolean isHasBingMaps() {
		return hasBingMaps;
	}
	public String getGaTrackingAccount() {
		return gaTrackingAccount;
	}
	public String getAspSessionId() {
		return aspSessionId;
	}
	public String getJSessionId() {
		return jSessionId;
	}
	
	public String getAspxFormsAuth() {
		return aspxFormsAuth;
	}
	
	public void unregisterSubscribers(){
		AsynchReportResultSubscriber.unRegister(this);
	}

	public boolean isAsyncReportGenerationEnabled() {
		return asyncReportGenerationEnabled;
	}

	public void setAsyncReportGenerationEnabled(boolean asyncReportGenerationEnabled) {
		this.asyncReportGenerationEnabled = asyncReportGenerationEnabled;
	}
	
	private String generateKey() {
		UUID uuid = UUID.randomUUID();
		return uuid.toString();
	}
	
	public String addExportJasperPrintObject(JasperPrint jp) {
		String key = generateKey();
		exportPrintList.put(key, jp);
		return key;
	}
	
	public JasperPrint getExportJasperPrintObject(String key) throws Exception {
		JasperPrint jp = exportPrintList.remove(key);
		if (jp == null) {
			jp = AdhocReportHelper.getAsynchronouslyGeneratedJasperPrintObject(this, key);
		}
		return jp;
	}
}
