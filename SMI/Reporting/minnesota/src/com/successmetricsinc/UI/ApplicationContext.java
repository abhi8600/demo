/**
 * $Id: ApplicationContext.java,v 1.105 2012-12-11 01:42:28 BIRST\gsingh Exp $
 *
 * Copyright (C) 2007-2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.UI;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.jci.listeners.FileChangeListener;
import org.apache.commons.jci.monitor.FilesystemAlterationListener;
import org.apache.commons.jci.monitor.FilesystemAlterationMonitor;
import org.apache.commons.jci.monitor.FilesystemAlterationObserver;
import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.input.SAXBuilder;

import com.successmetricsinc.PackageSpaceProperties;
import com.successmetricsinc.Repository;
import com.successmetricsinc.ServerContext;
import com.successmetricsinc.catalog.CatalogManager;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.JasperDataSourceProvider;
import com.successmetricsinc.query.ResultSetCache;
import com.successmetricsinc.query.Database.DatabaseType;
import com.successmetricsinc.security.EncryptionService;
import com.successmetricsinc.util.BirstClientUtil;
import com.successmetricsinc.util.RepositoryException;
import com.successmetricsinc.util.Util;

/**
 * Contains Application specific information - repository, cache, and catalog
 * 
 * @author ricks
 */
public class ApplicationContext
{
	private static Logger logger = Logger.getLogger(ApplicationContext.class);
	private static Map<String, ApplicationContext> contexts = Collections.synchronizedMap(new HashMap<String, ApplicationContext>());
	private static final String PUBLISH_LOCK_FILE = "publish.lock";
	private static final String DELETED_LOCK_FILE = "deleted.lock";	

	private String applicationPath;
	private String repositoryFilename;
	private Repository repository;
	private ResultSetCache cache;
	private String catalogDir;
	private CatalogManager catalogManager;
	private boolean isValid;
	private int refCount = 0;
	private FileMonitor fsm = new FileMonitor();
	private boolean repositoryChanged = false;
	private long mappingFileModifiedDate;
	private boolean cacheBeingSeed = false;
	private boolean amIBeingFilled = false;
	private List<PackageSpaceProperties> packageSpacesPropertiesList;
	private long reloadContextTime = System.currentTimeMillis();
	
	/*
	 * dummy constructor to get an object to synchronize on
	 */
	private ApplicationContext()
	{
		refCount = 1;
	}

	/**
	 * factory method to return an instance of ApplicationContext for 'path' (path to the space)
	 * @param path
	 * @return
	 */
	public static ApplicationContext getInstance(String path, List<PackageSpaceProperties> spacePropertiesList)
	{
		ApplicationContext ac = null;
		synchronized (contexts)
		{
			ac = contexts.get(path);
			if (ac == null)
			{
				ac = new ApplicationContext();
				contexts.put(path, ac);
				ac.amIBeingFilled = true;
			}
		}
		synchronized (ac)
		{
			if (ac.amIBeingFilled)
			{
				// no AC exists for this path, create one and fill it
				try
				{
					ac.fill(path, spacePropertiesList);
					logger.debug("AC " + path + " ref count is " + ac.refCount + ", total # of ACs is " + contexts.size());
				}
				finally{
					ac.amIBeingFilled = false;
				}			
			} 
			else
			{
				// an AC exists, increment the reference count and validate that the repository and user/group information up to date
				ac.refCount++;
				boolean repositoryReloaded = false;
				// check for a valid date
				String repositoryFilename = ac.applicationPath + File.separator + "repository.xml";
				File repositoryFile = new File(repositoryFilename);
				// This ensures that if there is a mismatch in the cached repository with the updated packages list, it refreshes
				boolean refreshRepForPackages = ac.refreshOnPackagesMismatch(spacePropertiesList);
				if ((ac.getRepository() == null) || (repositoryFile.lastModified() != ac.getRepository().getLastModified()) 
						|| refreshRepForPackages)
				{				
					logger.debug("cached repository, but out of date or invalid, reloading");
					if (ac.getRepository() == null)
						logger.debug("cached repository is null.");
					else if (refreshRepForPackages) {
						logger.debug("refreshOnPackageMismatch is true");
					}
					else {
						logger.debug("repositoryFile.lastModified is " + repositoryFile.lastModified());
						logger.debug("repository last modified is    " + ac.getRepository().getLastModified());
					}
					ac.repositoryChanged = true;
					if(refreshRepForPackages)
					{
						ac.packageSpacesPropertiesList = spacePropertiesList;
					}	
					ac.reloadContext();
					if(refreshRepForPackages && ac.fsm != null){
						ac.fsm.resetImportedSpaceListener();
					}
					repositoryReloaded = true;
				}
			
				if (!repositoryReloaded)
				{
					// If the repository is already reloaded, mapping file gets reloaded as part of it.
					// reload the user and group mapping file. 
					String userGroupMappingFileName = ac.applicationPath + File.separator + Repository.USERS_GROUPS_FILE_NAME;
					File userGroupMappingFile = new File(userGroupMappingFileName);
					if(ac.getMappingFileModifiedDate() != userGroupMappingFile.lastModified())
					{					
						logger.debug(("reloading " + Repository.USERS_GROUPS_FILE_NAME));
						ac.getRepository().initializeUsersAndGroups();
						ac.setMappingFileModifiedDate(userGroupMappingFile.lastModified());
					}
				}
				logger.debug("AC " + path + " ref count is " + ac.refCount + ", total # of ACs is " + contexts.size());							
			}
		}
		return ac;
	}
	
	/**
	 * Checks for the use case: 
	 * where the cached repository has no packages while application context has. Could happen 
	 * if the bin repository creation happen via call with {@link #getInstance(String, List)} and List as null 
	 * In this case caller should refresh repository 
	 * @return
	 */
	private boolean refreshOnPackagesMismatch(List<PackageSpaceProperties> pSpacesPropertiesList)
	{
		if(pSpacesPropertiesList != null && pSpacesPropertiesList.size() > 0 && repository != null)
		{
			List<PackageSpaceProperties> repositoryStoredList = repository.getPackagesSpacePropertiesList();
			if(repositoryStoredList == null || repositoryStoredList.size() == 0 
					|| repositoryStoredList.size() != pSpacesPropertiesList.size()){
				return true;
			}
		}
		return false;
	}
	
	public static ApplicationContext getApplicationContextIfAvailable(String path)
	{
		ApplicationContext ac = contexts.get(path);
		if(ac != null && !ac.amIBeingFilled)
		{
			return ac;
		}
		return null;
	}
	
	public void updatePackageSpacesProperties(List<PackageSpaceProperties> updatedPackageSpacesProperties)
	{
		List<PackageSpaceProperties> existingSpacesProperties = packageSpacesPropertiesList;
		if(updatedPackageSpacesProperties == null && existingSpacesProperties == null)
		{
			return;
		}
		
		boolean changed = false;
		if(updatedPackageSpacesProperties != null && existingSpacesProperties == null)
		{
			changed = true;
		}
		else if(existingSpacesProperties != null && updatedPackageSpacesProperties == null)
		{
			changed = true;
		}
		else if(updatedPackageSpacesProperties != null && existingSpacesProperties != null && updatedPackageSpacesProperties.size() != existingSpacesProperties.size())
		{
			changed = true;
		}
		else 
		{
			if(updatedPackageSpacesProperties != null)
			{
				for(PackageSpaceProperties spaceProperties : updatedPackageSpacesProperties)
				{	
					boolean found = false;
					for(PackageSpaceProperties existing : existingSpacesProperties)
					{
						if(existing != null && existing.equals(spaceProperties))
						{
							found = true;
							break;
						}
					}
				
					if(!found)
					{
						changed = true;
						break;
					}
				}
			}
		}

		if(changed && fsm != null)
		{
			logger.debug("Detected change of package space properties for child space " + applicationPath);
			packageSpacesPropertiesList = updatedPackageSpacesProperties;
			fsm.resetImportedSpaceListener();
		}
	}

	
	public boolean isCacheBeingSeed() {
		return cacheBeingSeed;
	}

	public void setCacheBeingSeed(boolean cacheBeingSeed) {
		this.cacheBeingSeed = cacheBeingSeed;
	}

	/**
	 * always run using a synchronization block on the AC
	 * @param _applicationPath	path to the space
	 */
	private void fill(String _applicationPath, List<PackageSpaceProperties> _packageSpacePropertiesList)
	{
		if (_applicationPath == null)
			return;
		applicationPath = _applicationPath;
		if(_packageSpacePropertiesList != null)
		{
			packageSpacesPropertiesList = _packageSpacePropertiesList;
		}
		logger.debug("Initializing Context '" + applicationPath.toLowerCase() + "'");
		loadContext();
		fsm.init();
		fsm.run();
	}

	/**
	 * load a serialized repository (a performance improvement for repositories that take a long time to initialize)
	 * @param repositoryFilename
	 * @param applicationPath
	 * @param repositoryFile
	 * @param usersAndGroupsFile
	 * @return
	 * @throws IOException
	 */
	private static Repository loadBinRepository(String repositoryFilename, String applicationPath, File repositoryFile, 
			File usersAndGroupsFile, File customerPropertyFile, List<PackageSpaceProperties> packageSpacePropertiesList) throws IOException
	{
		File fibin = new File(repositoryFilename + ".bin" + Repository.getSerializationVersion());
		long maxModifiedDate = getMaxModifiedDateOfParentRepositories(packageSpacePropertiesList);
		Repository r = null;
		long modifiedDateToCompare = repositoryFile.lastModified() > maxModifiedDate ? repositoryFile.lastModified() : maxModifiedDate;
		if (fibin.exists() && fibin.lastModified() > modifiedDateToCompare)
		{
			// making customer.properties & usersAndGroups.xml as optional checks for using .bin files
			//if they exists and recent then do not use .bin file. if not exist then continue using .bin file
			if (customerPropertyFile != null && customerPropertyFile.exists() && customerPropertyFile.lastModified() > fibin.lastModified())
			{
				logger.debug("Ignoring deserializing the Repository object for " + applicationPath + " because customer.properties file is more recent.");
				return null;
			}
			if (usersAndGroupsFile.exists() && usersAndGroupsFile.lastModified() > fibin.lastModified())
			{
				logger.debug("Ignoring deserializing the Repository object for " + applicationPath + " because usersAndGroups file is more recent.");
				return null;
			}
			BufferedInputStream fis = null;
			ObjectInputStream ois = null;
			try
			{
				fis = new BufferedInputStream(new FileInputStream(fibin));
				ois = new ObjectInputStream(fis);
				r = (Repository) ois.readObject();
				logger.debug("Loaded repository from serialized file");
			} catch (Exception ex)
			{
				// if this fails, we're dead
				logger.debug("Problem in deserializing the Repository object for '"
						+ applicationPath + "'. Deleting the serialized file and reading the XML version. " + ex.getMessage());
				if (ois != null)
					ois.close();
				if (fis != null)
					fis.close();
				if(fibin != null && fibin.exists())
				{
					Util.secureDelete(fibin);
				}
			} finally
			{
				if (ois != null)
					ois.close();
				if (fis != null)
					fis.close();
			}
		}
		else
		{
			logger.debug("Skipping load of serialized repository due to missing file or more recent modification dates");
			if (fibin.exists())
			{
				if (fibin.lastModified() <= repositoryFile.lastModified())
				{
					logger.debug("XML file is newer than serialized file");
				}
			}
			else
			{
				logger.debug("Serialized file does not exist");
			}
		}
		return r;
	}
	
	private static long getMaxModifiedDateOfParentRepositories(List<PackageSpaceProperties> packageSpacePropertiesList)
	{
		long maxModifiedDate = -1;
		if(packageSpacePropertiesList != null && packageSpacePropertiesList.size() > 0)
		{
			try
			{	 
				for(PackageSpaceProperties packageSpaceProperties : packageSpacePropertiesList)
				{
					String importedSpaceDirectory = packageSpaceProperties.getSpaceDirectory();
					File importedRepositoryFile = new File(importedSpaceDirectory + File.separator + "repository.xml");
					if(importedRepositoryFile != null && importedRepositoryFile.exists())
					{
						long importedFileModifiedDate = importedRepositoryFile.lastModified();
						if(importedFileModifiedDate > maxModifiedDate)
						{
							maxModifiedDate = importedFileModifiedDate;
						}
					}
				}
			}
			catch(Exception ex)
			{
				logger.warn("Error in getting max modifidate date of imported repositories ", ex);
				return -1;
			}
		}
		
		return maxModifiedDate;
	}
	/**
	 * save a repository object to a serialized file (a performance improvement for repositories that take a long time to initialize)
	 * @param repositoryFilename
	 * @param repository
	 * @param applicationPath
	 * @throws IOException
	 */
	private static void saveBinRepository(String repositoryFilename, Repository repository, String applicationPath) throws IOException
	{
		File fibin = new File(repositoryFilename + ".bin" + Repository.getSerializationVersion());
		BufferedOutputStream fos = null;
		ObjectOutputStream oos = null;
		try
		{	
			fos = new BufferedOutputStream(new FileOutputStream(fibin));
			oos = new ObjectOutputStream(fos);
			//try decrypting the username if its encrypted for Redshift
			if(repository.getDefaultConnection().DBType == DatabaseType.Redshift)
			{
				repository.getDefaultConnection().UserName = EncryptionService.getInstance().decrypt(repository.getDefaultConnection().UserName);
			}
			oos.writeObject(repository);
			logger.debug("Saved repository as a serialized file");			
		} catch (Exception ex)
		{
			// if this fails, we're not using the optimization to use serialized version of repository
			logger.debug("Problem in serializing repository object for '" + applicationPath + "'. " + ex.getMessage());
			if (oos != null)
				oos.close();
			if (fos != null)
				fos.close();
			if(fibin != null && fibin.exists())
			{
				Util.secureDelete(fibin);
			}						
		} finally
		{
			if (oos != null)
				oos.close();
			if (fos != null)
				fos.close();
		}
	}
	
	/**
	 * load the repository, cache, and catalog
	 * - used by fill and reloadContext, always under a synchronization block on the AC
	 */	
	private void loadContext()
	{
		isValid = false;
		// look for deleted lock file
		String deletedLockFileName = applicationPath + File.separator + DELETED_LOCK_FILE;
		File deletedLockFile = new File(deletedLockFileName);
		if (deletedLockFile.exists())
		{
			logger.error("The application has been deleted.");
			return;
		}
		Properties serverOverrides = ServerContext.getServerOverrides();
		try
		{
			repositoryFilename = applicationPath + File.separator + "repository.xml";
			repository = null;
			File repositoryFile = new File(repositoryFilename);
			if (!Repository.checkRepositorySize(repositoryFile))
			{
				return;
			}
			
			String usersAndGroupsFileName = applicationPath + File.separator + Repository.USERS_GROUPS_FILE_NAME;
			final File usersAndGroupsFile = new File(usersAndGroupsFileName);
			String customerPropFileName = ServerContext.getCustomerPropertiesFilePath();
			final File customerPropertyFile = (customerPropFileName == null ? null : new File(customerPropFileName));
			try
			{
				
				repository = loadBinRepository(repositoryFilename, applicationPath, repositoryFile, usersAndGroupsFile, customerPropertyFile, packageSpacesPropertiesList);
				boolean forceRefreshBinaryFile = repository != null && repository.getLastModified() != repositoryFile.lastModified();
				if(forceRefreshBinaryFile){
					logger.debug("Refreshing binary file because of mismatch between reference repository last modified property and repository file modified date");
				}
				if (forceRefreshBinaryFile || repository == null || refreshOnPackagesMismatch(packageSpacesPropertiesList))
				{
					SAXBuilder builder = new SAXBuilder();					
					Document d = builder.build(repositoryFile);
					repository = new Repository(d, false, serverOverrides, repositoryFile.lastModified(), false, packageSpacesPropertiesList, null);					
					saveBinRepository(repositoryFilename, repository, applicationPath);
				}
			} catch (Exception ex)
			{
				// if this fails, we're dead
				logger.error("Exception in loading the Repository for '" + applicationPath + "'. Fix this before going further.", ex);
				return;
			}
			if (repository == null || !repository.isRepositoryInitialized())
			{
				// if this fails, we're dead
				logger.error("Repository did not initialize for '" + applicationPath + "'. Fix this before going further.");
				return;
			}
						
			if(usersAndGroupsFile.exists())
			{
				setMappingFileModifiedDate(usersAndGroupsFile.lastModified());
			}
	
			// see if the repository is newer than the cacheMaps file, if so reset the cache
			boolean resetCache = isCacheOlderThanRepository(repository.getApplicationIdentifier(), repositoryFile);
			resetCache = resetCache || isCacheOlderThanImportedRepositories();
			cache = ResultSetCache.getInstance(repository.getApplicationIdentifier(), resetCache);
			cache.setPublishLockPath(applicationPath + File.separator + PUBLISH_LOCK_FILE);
			cache.setImportedSpacePublishingOrHasPublished(false);
			updateCacheFlagsForImportedSpaces();
			catalogDir = applicationPath + File.separator + "catalog";
			logger.debug("catalogDir: " + catalogDir);

			// Rep variables should have been instantiated already as part of rep initialization - do it again only if the cache was reset.
			if(resetCache)
			{
				repository.instantiateRepositoryVariables(null, new JasperDataSourceProvider(repository));
				logger.debug("Instantiated repository variables for " + applicationPath + " with repository object id - " + repository.hashCode());
			}
		} catch (Exception e)
		{
			logger.fatal("Initialization Exception for '" + applicationPath + "'", e);
			return;
		}
		repositoryChanged = false;
		isValid = true;
	}
	
	/**
	 * Check whether the cache is older than our repository xml file
	 * @param repositoryId
	 * @param cacheDir
	 * @param repositoryFile
	 * @return
	 */
	private boolean isCacheOlderThanRepository(String repositoryId, File repositoryFile)
	{
		// see if the repository is newer than the result cache repository sub directory of the file caches.
		File sharedCacheDir = new File(ResultSetCache.getSharedDirectory(repositoryId));
		if(sharedCacheDir.exists() && (repositoryFile.lastModified() > sharedCacheDir.lastModified())){
			logger.debug("Repository file is newer than the shared cache dir " + sharedCacheDir.getAbsolutePath());
			return true;
		}
		return false;
	}
	
	
	private boolean isCacheOlderThanImportedRepositories()
	{
		if(repository != null && packageSpacesPropertiesList != null && packageSpacesPropertiesList.size() > 0){
			String childRepositoryId = repository.getApplicationIdentifier();
			File sharedCacheDir = new File(ResultSetCache.getSharedDirectory(childRepositoryId));
			if(sharedCacheDir.exists()){
				// see if the any imported repository is newer than the child space result cache repository sub directory of the file caches. 
				for(PackageSpaceProperties importedPackgeSpaceProperty : packageSpacesPropertiesList){

					String importedSpaceDirectory = importedPackgeSpaceProperty.getSpaceDirectory();
					if(importedSpaceDirectory != null && importedSpaceDirectory.trim().length() > 0)
					{
						File importedRepositoryFile = new File(importedSpaceDirectory + File.separator + "repository.xml");
						if(importedRepositoryFile.exists() && (importedRepositoryFile.lastModified() > sharedCacheDir.lastModified())){
							{
								logger.debug("Imported Package repository : " + importedRepositoryFile.getAbsolutePath() + " is newer than the child shared cache dir " + sharedCacheDir.getAbsolutePath());
								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}
	
	public static Repository loadRepositoryIfMoreRecent(String applicationPath, String type, Repository r, List<PackageSpaceProperties> spacePropertiesList)
	{
		String repositoryFilename = null;
		if (type.equals("dev"))
			repositoryFilename = applicationPath + File.separator + "repository_dev.xml";
		else
			repositoryFilename = applicationPath + File.separator + "repository.xml";
		File fi = new File(repositoryFilename);
		if (!fi.exists())
		{
			logger.fatal("The repository file does not exist - " + repositoryFilename);
			return null;
		}
		if (fi.lastModified() > r.getLastModified())
			return loadRepository(applicationPath, type, spacePropertiesList);
		return r;
	}
	
	/**
	 *  
	 * @param applicationPath	path to the space
	 * @param type	type of repository (dev for development repository)
	 * @return
	 */
	public static Repository loadRepository(String applicationPath, String type, List<PackageSpaceProperties> spacePropertiesList)
	{
		String repositoryFilename = null;
		if (type.equals("dev"))
			repositoryFilename = applicationPath + File.separator + "repository_dev.xml";
		else
			repositoryFilename = applicationPath + File.separator + "repository.xml";
		logger.debug("loadRepository: " + repositoryFilename);
		File fi = new File(repositoryFilename);
		if (!fi.exists())
		{
			logger.fatal("The repository file does not exist - " + repositoryFilename);
			return null;
		}
		String usersAndGroupsFileName = applicationPath + File.separator + Repository.USERS_GROUPS_FILE_NAME;
		final File usersAndGroupsFile = new File(usersAndGroupsFileName);
		String customerPropFileName = ServerContext.getCustomerPropertiesFilePath();
		final File customerPropertyFile = new File(customerPropFileName);
		Repository r;
		try
		{
			r = loadBinRepository(repositoryFilename, applicationPath, fi, usersAndGroupsFile, customerPropertyFile, spacePropertiesList);
		} catch (IOException e)
		{
			r = null;
		}
		if (r != null)
			return r;
		if (!Repository.checkRepositorySize(fi))
		{
			return null;
		}
		String key = applicationPath + type;
		ApplicationContext ac = contexts.get(key);
		if (ac != null)
		{
			// make sure the repository is up to date
			if (fi.lastModified() == ac.getRepository().getLastModified())
			{
				return ac.getRepository();
			}
		}
		try
		{
			SAXBuilder builder = new SAXBuilder();			
			Document d = builder.build(fi);
			r = new Repository(d, false, ServerContext.getServerOverrides(), fi.lastModified(), false, spacePropertiesList, null);
			saveBinRepository(repositoryFilename, r, applicationPath);
			return r;
		} catch (Exception ex)
		{
			// if this fails, we're dead
			logger.error("Exception in loading the Repository for '" + applicationPath + "'. The problem must be addressed before the space can be used.", ex);
		}
		return null;
	}

	/**
	 * @return Returns the cache.
	 */
	public ResultSetCache getCache()
	{
		return cache;
	}

	/**
	 * @return Returns the catalog manager
	 */
	public CatalogManager getCatalogManager()
	{
		if (catalogManager == null)
		{
			catalogManager = new CatalogManager(getCatalogPath());
		}
		return catalogManager;
	}

	/**
	 * @return Returns the repository.
	 */
	public Repository getRepository()
	{
		return repository;
	}

	private String getCatalogPath()
	{
		return catalogDir;
	}

	/**
	 * clean up the context information - connections are shared across applications/users on this server
	 */
	public void dispose()
	{
		synchronized (this)
		{
			refCount--;
			logger.debug("AC " + applicationPath + " ref count is " + refCount);
			// cleaning up memory
			if (refCount <= 0)
			{
				try
				{
					logger.debug("Removing application " + applicationPath + " from memory");
					// try to shutdown and remove from memory as much as is possible to minimize memory leaks
					if (fsm != null)
						fsm.stop();
					fsm = null;
					if (repository != null && repository.getConnections() != null)
					{
						for (DatabaseConnection dc: repository.getConnections())
						{
							if (dc.ConnectionPool != null && explictlyCloseDBConnections(dc))
							{
								dc.ConnectionPool.closeAllConnections();
							}
						}
					}
					ResultSetCache.dispose(cache.getRepositoryId()); // remove from the rscCache
					repository = null;
					cache = null;
					catalogManager = null;
				}
				catch (Exception ex)
				{
					logger.error("Failed to fully dispose of the application context");
				}
				contexts.remove(applicationPath); // remove the application data from memory
			}
		}
	}
	
	/**
	 * Signals if the database connections needs to be explictly closed based on the db type	 * 
	 * @param dc
	 * @return
	 */
	private boolean explictlyCloseDBConnections(DatabaseConnection dc)
	{
		if(DatabaseConnection.isDBTypeMySQL(dc.DBType))
		{
			return false;
		}
		return true;
	}

	private void retrieveUpdatedPackageSpaceProperties()
	{
		try{
			// only attempt to retrieve the list from Acorn for DBA space
			if(repository != null && repository.Imports != null && repository.Imports.size() > 0){
				List<PackageSpaceProperties> retreivedList = BirstClientUtil.getPackageSpaceProps(applicationPath);
				if(retreivedList != null && retreivedList.size() > 0){
					updatePackageSpacesProperties(retreivedList);
				}
			}
		}catch(Exception ex){
			logger.error("Error in retreiving the updated list of package space info for child space : " + applicationPath, ex);
		}
	}
	/**
	 * update the application context due to new repository - clear out cache - reload repository
	 */
	public void reloadContext()
	{
		synchronized (this)
		{
			if (!repositoryChanged) // see if this has already occurred
			{
				logger.debug("reloadContext already done");
				return;
			}
			logger.debug("Reinitializing application (delete and recreate cache, read repository and catalog) - " + applicationPath);
			if (cache != null)
				cache.resetCache();
			loadContext(); // read in the repository, cache, and catalog
			setReloadContextTime();
		}
	}
	
	private void setReloadContextTime()
	{
		reloadContextTime = System.currentTimeMillis();
	}
	
	public long getReloadContextTime(){
		return reloadContextTime;
	}

	private void updateCacheFlagsForImportedSpaces()
	{
		if(cache == null)
		{
			return;
		}
		boolean previousImportedSpacePublishing = cache.isImportedSpacePublishingOrHasPublished();
		boolean anyImportedSpacePublishing = false;
		if(packageSpacesPropertiesList != null && packageSpacesPropertiesList.size() > 0)
		{
			for(PackageSpaceProperties importedSpaceProperties : packageSpacesPropertiesList)
			{
				String importedSpaceDirectory = importedSpaceProperties.getSpaceDirectory();
				if(importedSpaceDirectory != null && importedSpaceDirectory.trim().length() > 0)
				{	
					File importedSpacePublishLockFile = new File(importedSpaceDirectory + File.separator + PUBLISH_LOCK_FILE);
					if(importedSpacePublishLockFile.exists())
					{
						anyImportedSpacePublishing = true;
						break;
					}
				}
			}
		}
		cache.setImportedSpacePublishingOrHasPublished(anyImportedSpacePublishing);
		if(previousImportedSpacePublishing && !anyImportedSpacePublishing)
		{
			logger.debug("Reevaluating variables after publishing of the imported space - "+ applicationPath);
			try
			{
				repository.instantiateRepositoryVariables(null, new JasperDataSourceProvider(repository));
				logger.debug("Instantiated repository variables for " + applicationPath + " with repository object id - " + repository.hashCode());
			}
			catch(RepositoryException re)
			{
				logger.error("Exception while reevaluating variables - " + applicationPath, re);
			}
		}
	}

	/**
	 * encapsulate the monitoring of the publish lock and repository files for changes
	 */
	private class FileMonitor
	{
		private FilesystemAlterationMonitor fam = new FilesystemAlterationMonitor();
		private boolean firstTime = true;
		private boolean firstTimeUsersAndGroups = true;
		private List<String> firstTimeImportedRepositoryChanged = new ArrayList<String>();
		private List<FilesystemAlterationListener> importedSpacesListeners = new ArrayList<FilesystemAlterationListener>();
		
		/**
		 * initialize the file monitors
		 */
		public void init()
		{
			final File repositoryFile = new File(repositoryFilename);
			FilesystemAlterationListener repositoryListener = new FileChangeListener()
			{
				public void onStop(FilesystemAlterationObserver pObserver)
				{
					super.onStop(pObserver);
					if (hasChanged())
					{
						if (firstTime) // for some odd reason, we get a repository changed event on the first read...
						{
							firstTime = false;
							return;
						}
						logger.debug("repository changed: " + applicationPath);
						repositoryChanged = true;
						retrieveUpdatedPackageSpaceProperties();
						reloadContext();
					}
				}
			};
			
			String usersAndGroupsFileName = applicationPath + File.separator + Repository.USERS_GROUPS_FILE_NAME;
			final File usersAndGroupsFile = new File(usersAndGroupsFileName);
			
			FilesystemAlterationListener usersAndGroupsFileListener = new FileChangeListener()
			{
				public void onStop(FilesystemAlterationObserver pObserver)
				{
					super.onStop(pObserver);
					if (hasChanged())
					{
						if (firstTimeUsersAndGroups) // for some odd reason, we get a usersAndGroups File changed event on the first read...
						{
							firstTimeUsersAndGroups = false;
							return;
						}
						
						logger.debug((Repository.USERS_GROUPS_FILE_NAME + " file changed for application " + applicationPath));
						if(repository != null)
						{
							try
							{
								repository.initializeUsersAndGroups();
							}
							catch(Exception ex)
							{
								logger.error("Error while reloading " + Repository.USERS_GROUPS_FILE_NAME + " for " + applicationPath, ex);
							}
						}
						else
						{
							logger.warn("Cannot initialize UserAndGroups. Repository object is null for " + applicationPath);						
						}
					}
				}
			};
			fam.addListener(repositoryFile, repositoryListener);
			fam.addListener(usersAndGroupsFile, usersAndGroupsFileListener);
			createRepositoryAndPublishLockFileListenerOnImportedSpaces();
		}
		
		private synchronized void updateImportedSpacesListeners()
		{
			firstTimeImportedRepositoryChanged.clear();
			if(importedSpacesListeners != null && importedSpacesListeners.size() > 0)
			{
				for(FilesystemAlterationListener importedSpaceListener : importedSpacesListeners)
				{
					fam.removeListener(importedSpaceListener);
				}
			}
			createRepositoryAndPublishLockFileListenerOnImportedSpaces();
		}
		
		private void createRepositoryAndPublishLockFileListenerOnImportedSpaces()
		{	
			if(packageSpacesPropertiesList != null && packageSpacesPropertiesList.size() > 0)
			{
				FilesystemAlterationListener importedRepositoryFileListener = new FileChangeListener()
				{
					public void onStop(FilesystemAlterationObserver pObserver)
					{
						super.onStop(pObserver);
						if (hasChanged())
						{
							File changedFile = pObserver.getRootDirectory();
							String changedImportedFileName = null;
							try
							{
								changedImportedFileName = changedFile != null ? changedFile.getAbsolutePath() : null;
							}
							catch(Exception ex)
							{
								logger.warn("Error in retrieving imported file name on file listener", ex);
								return;
							}
							
							if (!firstTimeImportedRepositoryChanged.contains(changedImportedFileName)) // for some odd reason, we get a repository changed event on the first read...
							{
								firstTimeImportedRepositoryChanged.add(changedImportedFileName);
								return;
							}
							
							logger.debug("Reloading the child repository since imported repository changed: " + changedImportedFileName);
							repositoryChanged = true;
							retrieveUpdatedPackageSpaceProperties();
							reloadContext();
						}
					}
				};
				
				FilesystemAlterationListener importedSpacePublishLockListener = new FileChangeListener()				
				{
					public void onStop(FilesystemAlterationObserver pObserver)
					{
						super.onStop(pObserver);
						if (hasChanged())
						{	
							if(cache != null)
							{
								cache.setImportedSpacePublishingOrHasPublished(true);
								updateCacheFlagsForImportedSpaces();
							}
						}
					}
				};
				
				for(PackageSpaceProperties importedSpaceProperties : packageSpacesPropertiesList)
				{
					String importedSpaceDirectory = importedSpaceProperties.getSpaceDirectory();
					if(importedSpaceDirectory != null && importedSpaceDirectory.trim().length() > 0)
					{
						File importedRepositoryFile = new File(importedSpaceDirectory + File.separator + "repository.xml");
						File importedSpacePublishLockFile = new File(importedSpaceDirectory + File.separator + PUBLISH_LOCK_FILE);
						if(importedRepositoryFile.exists())
						{
							fam.addListener(importedRepositoryFile, importedRepositoryFileListener);
							fam.addListener(importedSpacePublishLockFile, importedSpacePublishLockListener);
						}
					}
				}
				importedSpacesListeners.add(importedRepositoryFileListener);
				importedSpacesListeners.add(importedSpacePublishLockListener);
			}
		}
		/**
		 * start the file monitors
		 */
		public void run()
		{
			fam.start();
		}

		public void stop()
		{
			fam.stop();
		}
		
		public void resetImportedSpaceListener()
		{		
			updateImportedSpacesListeners();
		}
	}

	public boolean isValid()
	{
		synchronized (this)
		{
			return isValid;
		}
	}

	/*
	 * always called inside of a synchronized block
	 */
	private void setMappingFileModifiedDate(long mappingFileModifiedDate)
	{
		this.mappingFileModifiedDate = mappingFileModifiedDate;
	}

	/*
	 * always called inside of a synchronized block
	 */
	private long getMappingFileModifiedDate()
	{
		return mappingFileModifiedDate;
	}
}