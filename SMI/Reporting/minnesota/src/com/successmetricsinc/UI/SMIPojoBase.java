/**
 * $Id: SMIPojoBase.java,v 1.34 2011-09-28 16:05:14 ricks Exp $
 *
 * Copyright (C) 2007-2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.successmetricsinc.UI;

import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.User;
import com.successmetricsinc.query.ResultSetCache;

public class SMIPojoBase {

	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(SMIPojoBase.class);
	
	public SMIPojoBase() {
		super();
	}

	public static SMIWebUserBean getUserBean() {
		return SMIWebUserBean.getUserBean();
	}
	
	public static User getLoggedInUser() {
		return getUserBean().getUser();
	}

	public static Session getSession() {
		return getUserBean().getRepSession();
	}

	public static Repository getRepository() {
		return getUserBean().getRepository();
	}
	
	public static ResultSetCache getCache() {
		return getUserBean().getCache();
	}
}