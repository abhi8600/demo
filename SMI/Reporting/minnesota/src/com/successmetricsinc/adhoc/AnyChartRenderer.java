/**
 * 
 */
package com.successmetricsinc.adhoc;

import java.awt.Dimension;
import java.awt.geom.Dimension2D;
import java.util.List;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.export.XMLExporter;

import org.jdom.Element;

import com.successmetricsinc.chart.JCommonDrawableRenderer;
import com.successmetricsinc.util.XmlUtils;

import org.apache.log4j.Logger;
import com.successmetricsinc.util.TrellisTooLargeException;
import com.successmetricsinc.chart.AnyChart.NoDataException;

/**
 * @author agarrison
 *
 */
public class AnyChartRenderer extends JCommonDrawableRenderer implements XMLExporter {

	private static final long serialVersionUID = 1L;
	private AdhocChart adhocChart;
	private JRDataSource dataSource;
	private ChartFilters chartFilters;
	private String selectorXml;
	
	private static Logger logger = Logger.getLogger(AnyChartRenderer.class);
	
	public AnyChartRenderer() {
		
	}
	
	public AnyChartRenderer(AdhocChart chart, JRDataSource ds, ChartFilters cf, String selectors) {
		adhocChart = chart;
		dataSource = ds;
		chartFilters = cf;
		selectorXml = selectors;
	}

	@Override
	public String getXML(double scaleFactor, boolean animation,
			boolean isPrinted) { 
		Element root = null;
		double scaleFact = scaleFactor;
		try
		{
			if(adhocChart.isTrellis()){
				List<Element> charts = adhocChart.getTrellisXml(dataSource, chartFilters, selectorXml, scaleFactor, isPrinted);
				if(charts == null){
					return null;
				}
				StringBuffer buf = new StringBuffer();
				for(int i = 0; i < charts.size(); i++){
					Element c = charts.get(i);
					buf.append(XmlUtils.convertElementToString(c));
				}
				return buf.toString();
			}else{
				root = adhocChart.getXML(dataSource, chartFilters, selectorXml, scaleFactor, isPrinted);
				if (root == null)
					return null;
			}
		}
	
		catch(NoDataException nde)
		{
			// Generate a "No data chart" when query returns no data
			root = adhocChart.generateNoDataChart(nde, scaleFact);
		}
		
		catch(TrellisTooLargeException tle)
		{
			logger.error("*** AnyChartRenderer.getXml Caught TrellisTooLargeException ***" , tle);
		}
		if (root == null)
			return null;
	    
		
		return XmlUtils.convertElementToString(root);
	}

	public byte getImageType() {
		return ((byte) 255);
	}

	/**
	 * @return the adhocChart
	 */
	public AdhocChart getAdhocChart() {
		return adhocChart;
	}
	
	public Dimension2D getDimension() {
		if (adhocChart != null) {
			return new Dimension(adhocChart.getWidth() * 2, adhocChart.getHeight() * 2);
		}
		
		return new Dimension(AdhocChart.DEFAULT_CHART_WIDTH,AdhocChart.DEFAULT_CHART_WIDTH);
	}
}
