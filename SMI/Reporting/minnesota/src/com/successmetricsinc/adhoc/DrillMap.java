package com.successmetricsinc.adhoc;

import java.io.File;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;

import com.successmetricsinc.Group;
import com.successmetricsinc.Repository;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

public class DrillMap {
	
	private static final Logger logger = Logger.getLogger(DrillMap.class);
	public static final String DRILL_ROWS = "DrillRows";
	public static final String DRILL_ROW = "DrillRow";
	public static final String DRILL_MAP_ID = "Id";
	public static final String DRILL_MAP_NAME = "Name";
	public static final String DRILL_MAP_ENABLED = "Enabled";
	public static final String DRILL_MAP_CREATED_DATE = "CreatedDate";
	public static final String DRILL_MAP_CREATED_BY = "CreatedBy";
	public static final String DRILL_MAP_MODIFIED_DATE = "ModifiedDate";
	public static final String DRILL_MAP_MODIFIED_BY = "ModifiedBy";
	public static final String DRILL_MAP_VALID = "Valid";
	public static final String DRILL_MAP_GROUPS = "Groups";
	public static final String DRILL_MAP_GROUP = "Group";
	public static final String DRILL_MAP_GROUP_ID = "Id";
	
	private String id;
	private String name;
	private boolean enabled;
	private boolean valid = true;
	private List<DrillRow> drillRows;
	private Date createdDate;
	private Date modifiedDate;
	private String createdBy;
	private String modifiedBy;
	private List<Group> drillMapGroups;
	
	// create a unique identifer the presence of which means drill map is not allowed for any group
	// this is used to have a backward compatibility as the previous drill maps do not have any groups
	// but they should be available to everybody
	private static final String DRILL_MAP_NO_GROUP = "DB54E80E-3B35-412E-B160-63202F7FD198";
			
	public DrillMap()
	{	
	}
	
	public static DrillMap convert(String xml,OMNamespace ns) throws Exception {
		
		XMLStreamReader parser;
		try {
			XMLInputFactory inputFactory = XmlUtils.getSecureXMLInputFactory();
			parser = inputFactory.createXMLStreamReader(new StringReader(xml));

			for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) 
			{
				String elName = parser.getLocalName();
				if (DrillMapList.DRILL_MAP_ROOT.equals(elName))
				{
					DrillMap drillMap = new DrillMap();
					drillMap.parseElement(parser);
					return drillMap;
				}
			}
			
		} catch (XMLStreamException e) {
			logger.error(e, e);
		} catch (FactoryConfigurationError e) {
			logger.error(e, e);
		}
		return null;
	}
	
	public void parseElement(XMLStreamReader parser) throws Exception {
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
			
			String elName = parser.getLocalName();
			if (DRILL_MAP_ID.equals(elName))
			{
				setId(XmlUtils.getStringContent(parser));
			}
			if (DRILL_MAP_NAME.equals(elName))
			{
				setName(XmlUtils.getStringContent(parser));
			}
			else if (DRILL_MAP_ENABLED.equals(elName))
			{
				setEnabled(XmlUtils.getBooleanContent(parser));
			}
			else if (DRILL_ROWS.equals(elName))
			{
				parseDrillRows(parser);
			}
			else if (DRILL_MAP_CREATED_DATE.equals(elName))
			{
				Date cDate = XmlUtils.getDateContent(parser, true);
				if(cDate != null)
				{
					setCreatedDate(cDate);
				}
			}
			else if (DRILL_MAP_CREATED_BY.equals(elName))
			{
				setCreatedBy(XmlUtils.getStringContent(parser));
			}
			else if (DRILL_MAP_MODIFIED_DATE.equals(elName))
			{
				Date mDate = XmlUtils.getDateContent(parser, true);
				if(mDate != null)
				{
					setModifiedDate(mDate);
				}
			}
			else if (DRILL_MAP_MODIFIED_BY.equals(elName))
			{
				setModifiedBy(XmlUtils.getStringContent(parser));
			}
			else if(DRILL_MAP_GROUPS.equals(elName)){
				parseDrillMapGroups(parser);
			}
		}
	}
	
	private void parseDrillRows(XMLStreamReader parser) throws Exception
	{	
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) 
		{
			String elName = parser.getLocalName();
			if (DRILL_ROW.equals(elName))
			{
				DrillRow drillRow = new DrillRow();
				drillRow.parseElement(parser);
				addDrillRow(drillRow);
			}
		}
	}
	
	private void parseDrillMapGroups(XMLStreamReader parser) throws Exception
	{	
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) 
		{
			String elName = parser.getLocalName();
			if (DRILL_MAP_GROUP.equals(elName))
			{
				parseDrillMapGroup(parser);
			}
		}
	}
	
	private void parseDrillMapGroup(XMLStreamReader parser) throws Exception{
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) 
		{
			String elName = parser.getLocalName();
			if (DRILL_MAP_GROUP_ID.equals(elName))
			{
				if(drillMapGroups == null){
					drillMapGroups = new ArrayList<Group>();
				}
				
				Group group = new Group();
				group.setId(XmlUtils.getStringContent(parser));
				drillMapGroups.add(group);
			}
		}
	}

	private void addDrillRow(DrillRow drillRow)
	{
		if(drillRows == null)
		{
			drillRows = new ArrayList<DrillRow>();
		}
		drillRows.add(drillRow);
	}

	public String getName() {
		return name;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public List<DrillRow> getDrillRows() {
		return drillRows;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public void setDrillRows(List<DrillRow> drillRows) {
		this.drillRows = drillRows;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}
	
	public void addContent(OMFactory fac, OMElement content, OMNamespace ns, boolean deepCopy, boolean includeValidFlag)
	{	
		XmlUtils.addContent(fac, content, DRILL_MAP_ID, getId(), ns);
		XmlUtils.addContent(fac, content, DRILL_MAP_NAME, getName(), ns);
		XmlUtils.addContent(fac, content, DRILL_MAP_ENABLED, isEnabled(), ns);
		XmlUtils.addContent(fac, content, DRILL_MAP_CREATED_DATE, getCreatedDate(), true, ns);
		XmlUtils.addContent(fac, content, DRILL_MAP_CREATED_BY, getCreatedBy(), ns);
		XmlUtils.addContent(fac, content, DRILL_MAP_MODIFIED_DATE, getModifiedDate(), true, ns);
		XmlUtils.addContent(fac, content, DRILL_MAP_MODIFIED_BY, getModifiedBy(), ns);
		if(includeValidFlag)
		{
			XmlUtils.addContent(fac, content, DRILL_MAP_VALID, isValid(), ns);
		}
		// if not shallow
		if(deepCopy)
		{
			addDrillRowsContent(fac, content, DRILL_ROWS, getDrillRows(), ns, includeValidFlag);
			addDrillGroupsContent(fac, content, ns);
		}
	}
	
	protected void addDrillRowsContent(OMFactory fac, OMElement parent, String name, List<DrillRow> drillRows, OMNamespace ns, boolean includeValidFlag) {
		OMElement element = fac.createOMElement(name, ns);
		for (DrillRow drillRow : drillRows) {
			OMElement content = fac.createOMElement(DRILL_ROW, ns);
			drillRow.addContent(fac, content, ns, includeValidFlag);
			element.addChild(content);
		}
		parent.addChild(element);
	}
	
	private void addDrillGroupsContent(OMFactory fac, OMElement parent, OMNamespace ns){
		OMElement element = fac.createOMElement(DRILL_MAP_GROUPS, ns);
		if(drillMapGroups != null && drillMapGroups.size() > 0){
			for(Group drillMapGroup: drillMapGroups){
				OMElement groupElement = fac.createOMElement(DRILL_MAP_GROUP, ns);
				XmlUtils.addContent(fac, groupElement, DRILL_MAP_GROUP_ID, drillMapGroup.getId(), ns);
				element.addChild(groupElement);
			}
		}
		parent.addChild(element);
	}

	public void validate(Repository repository) 
	{
		boolean valid = true;
		List<DrillRow> drillRows = this.getDrillRows();
		if(drillRows != null && drillRows.size() > 0)
		{
			for(DrillRow drillRow : drillRows)
			{	
				drillRow.validate(repository);
				if(!drillRow.isValid())
				{	
					valid = false;
					// still go through all the drill rows so that the corresponding valid flag is set
				}
			}
		}
		setValid(valid);
	}

	public boolean accessAllowed(List<Group> userGroups) {
		if(drillMapGroups == null || drillMapGroups.size() == 0){
			// potentially old drill maps. Allow for all
			return true;
		}
		
		// if no groups are allowed then look for a specific no_group id
		// to ensure the backward compatibility of having "empty groups" means all
		if(drillMapGroups.size() == 1 && DrillMap.DRILL_MAP_NO_GROUP.equalsIgnoreCase(drillMapGroups.get(0).getId())){
			return false;
		}
		
		for(Group drillMapGroup : drillMapGroups){
			if(drillMapGroup.isInternalGroup()){
				continue;
			}
			if(isDrillMapUserGroupMember(drillMapGroup, userGroups)){
				return true;
			}
		}
		return false;
	}

	private boolean isDrillMapUserGroupMember(Group drillMapGroup,
			List<Group> userGroups) {
		if(drillMapGroup != null && userGroups != null && userGroups.size() > 0){
			for(Group userGroup : userGroups){
				if(userGroup.getId().equalsIgnoreCase(drillMapGroup.getId())){
					return true;
				}	
			}
		}
		return false;
	}

}
