/**
 * 
 */
package com.successmetricsinc.adhoc;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.successmetricsinc.chart.AnyChart.AnyChartNode;
import com.successmetricsinc.chart.AnyChart.AnyChartNode.ChartType;

/**
 * @author agarrison
 *
 */
public class VisualizationChartConfig {

	public static final String columns = "c";
	public static final String rows    = "r";
	public static final String details = "d";
	public static final String either  = "e";
	
	public static final String measure   = "m";
	public static final String time      = "t";
	public static final String location  = "l";
	public static final String attribute = "a";
	
	private static List<VisualizationChartConfig> _config = null;
	private static Logger logger = Logger.getLogger(VisualizationChartConfig.class);
	
	private List<AnyChartNode.ChartType> chartTypes = new ArrayList<AnyChartNode.ChartType>();
	private int _rm = 0;
	private int _cm = 0;
	private int _em = 0;
	private int _dm = 0;
	private int _rt = 0;
	private int _ct = 0;
	private int _et = 0;
	private int _dt = 0;
	private int _rl = 0;
	private int _cl = 0;
	private int _el = 0;
	private int _dl = 0;
	private int _ra = 0;
	private int _ca = 0;
	private int _ea = 0;
	private int _da = 0;
	
	private VisualizationChartConfig() {
		chartTypes.add(ChartType.Column);
	}
	
	private static final List<VisualizationChartConfig> getConfig() {
		if (_config == null) {
			InputStream fis = null;
			String resource = "visualizationCharts.config";
			try {
				ClassLoader classLoader = Thread.currentThread().getContextClassLoader();		
				if (classLoader != null)
				{
					fis = classLoader.getResourceAsStream(resource);
				}
				if (fis == null)
					fis = ClassLoader.getSystemResourceAsStream(resource);
				BufferedReader br = new BufferedReader(new InputStreamReader(fis));
				String line;
				_config = new ArrayList<VisualizationChartConfig>();
				while ((line = br.readLine()) != null) {
					if (line.startsWith("#"))
						continue;
					// ignore anything after ;
					String [] parts = line.split(";");
					parts = parts[0].split(":");
					if (parts.length > 1) {
						VisualizationChartConfig vcc = new VisualizationChartConfig();
						vcc.chartTypes = decodeChartType(parts[1]);
						String[] attributes = parts[0].split(",");
						for (int i = 0; i < attributes.length; i++) {
							String attrib = attributes[i].trim().toLowerCase();
							int add = 1;
							if (attrib.length() > 2) {
								try {
									add = Integer.valueOf(attrib.substring(2));
								}
								catch (Exception e) {
									add = 1;
								}
							}
							if (attrib.startsWith("cm")) 
								vcc._cm += add;
							else if (attrib.startsWith("ct"))
								vcc._ct += add;
							else if (attrib.startsWith("cl"))
								vcc._cl += add;
							else if (attrib.startsWith("ca"))
								vcc._ca += add;
							else if (attrib.startsWith("rm"))
								vcc._rm += add;
							else if (attrib.startsWith("rt"))
								vcc._rt += add;
							else if (attrib.startsWith("rl"))
								vcc._rl += add;
							else if (attrib.startsWith("ra"))
								vcc._ra += add;
							else if (attrib.startsWith("dm"))
								vcc._dm += add;
							else if (attrib.startsWith("dt"))
								vcc._dt += add;
							else if (attrib.startsWith("dl"))
								vcc._dl += add;
							else if (attrib.startsWith("da"))
								vcc._da += add;
							else if (attrib.startsWith("em"))
								vcc._em += add;
							else if (attrib.startsWith("et"))
								vcc._et += add;
							else if (attrib.startsWith("el"))
								vcc._el += add;
							else if (attrib.startsWith("ea"))
								vcc._ea += add;
						}
						_config.add(vcc);
					}
				}
			}
			catch (Exception e) {
				logger.error(e, e);
			}
			finally {
				try {
					if (fis != null) 
						fis.close();
				}
				catch (Exception e) {}
			}
		}
		
		return _config;
	}
	
	public static VisualizationChartConfig findChartType(int numRm, int numCm, int numDm, int numRt, int numCt, int numDt,
			int numRl, int numCl, int numDl, int numRa, int numCa, int numDa) {
		for (VisualizationChartConfig vcc : getConfig()) {
			if (vcc.matches(numRm, numCm, numCm + numRm, numDm, numRt, numCt, numRt + numCt, numDt, 
					numRl, numCl, numRl + numCl, numDl, numRa, numCa, numRa + numCa, numDa))
				return vcc;
		}
		return null;
	}
	
	public AnyChartNode.ChartType getDefaultChartType() {
		return chartTypes.get(0);
	}

	private boolean matches(int numRm, int numCm, int numEm, int numDm,
			int numRt, int numCt, int numEt, int numDt, int numRl, int numCl,
			int numEl, int numDl, int numRa, int numCa, int numEa, int numDa) {
		boolean numMeasuresOk = (numRm >= _rm && numCm >= _cm && numEm >= _em && numDm >= _dm);
		boolean numTimeDimsOk = (numRt >= _rt && numCt >= _ct && numEt >= _et && numDt >= _dt);
		boolean numLocationsOk = (numRl >= _rl && numCl >= _cl && numEl >= _el && numDl >= _dl);
		boolean numDimsOk = (numRa >= _ra && numCa >= _ca && numEa >= _ea && numDa >= _da); 
		return  numMeasuresOk && numTimeDimsOk && numDimsOk;
	}
	
	private static List<ChartType> decodeChartType(String charts) {
		ArrayList<ChartType> types = new ArrayList<ChartType>();
		if (charts != null && charts.length() > 0) {
			String[] parts = charts.split(",");
			for (int i = 0; i < parts.length; i++){
				String chart = parts[i];
				if ("column".equalsIgnoreCase(chart))
					types.add(ChartType.Column);
				if ("bar".equalsIgnoreCase(chart))
					types.add(ChartType.Bar);
				if ("rangecolumn".equalsIgnoreCase(chart))
					types.add(ChartType.RangeColumn);
				if ("rangebar".equalsIgnoreCase(chart))
					types.add(ChartType.RangeBar);
				if ("waterfall".equalsIgnoreCase(chart))
					types.add(ChartType.Waterfall);
				if ("line".equalsIgnoreCase(chart))
					types.add(ChartType.Line);
				if ("pie".equalsIgnoreCase(chart))
					types.add(ChartType.Pie);
				if ("doughnut".equalsIgnoreCase(chart))
					types.add(ChartType.Doughnut);
				if ("stackedcolumn".equalsIgnoreCase(chart))
					types.add(ChartType.StackedColumn);
				if ("stackedbar".equalsIgnoreCase(chart))
					types.add(ChartType.StackedBar);
				if ("percentstackedcolumn".equalsIgnoreCase(chart))
					types.add(ChartType.PercentStackedColumn);
				if ("percentstackedbar".equalsIgnoreCase(chart))
					types.add(ChartType.PercentStackedBar);				
				if ("area".equalsIgnoreCase(chart))
					types.add(ChartType.Area);
				if ("stackedarea".equalsIgnoreCase(chart))
					types.add(ChartType.StackedArea);
				if ("scatter".equalsIgnoreCase(chart))
					types.add(ChartType.Scatter);
				if ("bubble".equalsIgnoreCase(chart))
					types.add(ChartType.Bubble);
				if ("meter".equalsIgnoreCase(chart))
					types.add(ChartType.Gauge);
				if ("gauge".equalsIgnoreCase(chart))
					types.add(ChartType.NewGauge);
				if ("treemap".equalsIgnoreCase(chart))
					types.add(ChartType.Treemap);
				if ("heatmap".equalsIgnoreCase(chart))
					types.add(ChartType.Heatmap);
				if ("pyramid".equalsIgnoreCase(chart))
					types.add(ChartType.Pyramid);
				if ("funnel".equalsIgnoreCase(chart))
					types.add(ChartType.Funnel);
				if ("geomap".equalsIgnoreCase(chart))
					types.add(ChartType.GeoMap);
				if ("radarchart".equalsIgnoreCase(chart))
					types.add(ChartType.Radar);
				if ("table".equalsIgnoreCase(chart))
					types.add(null);
			}
		}
		else {
			types.add(ChartType.Column);
		}
		
		return types;
	}

	public List<AnyChartNode.ChartType> getChartTypes() {
		return chartTypes;
	}

	public int getNumRowMeasures() {
		return _rm;
	}

	public int getNumColumnMeasures() {
		return _cm;
	}

	public int getNumRowOrColumnMeasures() {
		return _em;
	}

	public int getNumDetailMeasures() {
		return _dm;
	}

	public int getNumRowTimeAttributes() {
		return _rt;
	}

	public int getNumColumnTimeAttributes() {
		return _ct;
	}

	public int getNumRowOrColumnTimeAttributes() {
		return _et;
	}

	public int getNumDetailTimeAttributes() {
		return _dt;
	}

	public int getNumRowLocationAttributes() {
		return _rl;
	}

	public int getNumColumnLocationAttributes() {
		return _cl;
	}

	public int getNumRowOrColumnLocationAttributes() {
		return _el;
	}

	public int getNumDetailLocationAttributes() {
		return _dl;
	}

	public int getNumRowAttributes() {
		return _ra;
	}

	public int getNumColumnAttributes() {
		return _ca;
	}

	public int getNumRowOrColumnAttributes() {
		return _ea;
	}

	public int getNumDetailAttributes() {
		return _da;
	}

}
