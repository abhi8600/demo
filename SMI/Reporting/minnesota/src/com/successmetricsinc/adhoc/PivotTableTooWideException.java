package com.successmetricsinc.adhoc;

import com.successmetricsinc.util.BaseException;

public class PivotTableTooWideException extends BaseException {
	
	private static final long serialVersionUID = 1L;

	public PivotTableTooWideException(String s, Throwable cause)
	{
		super(s, cause);
	}

    public PivotTableTooWideException(String s)
    {
        super(s);
    }
    
	@Override
	public int getErrorCode() {
		return ERROR_PIVOT_TABLE_TOO_WIDE;
	}
}
