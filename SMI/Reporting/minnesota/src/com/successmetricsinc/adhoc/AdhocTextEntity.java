/**
 * $Id: AdhocTextEntity.java,v 1.23 2012-09-11 00:55:01 BIRST\agarrison Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */


package com.successmetricsinc.adhoc;

import java.awt.Font;

import javax.xml.stream.XMLStreamReader;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRStyle;
import net.sf.jasperreports.engine.design.JRDesignExpression;
import net.sf.jasperreports.engine.design.JRDesignStyle;
import net.sf.jasperreports.engine.design.JRDesignTextField;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.type.HorizontalAlignEnum;
import net.sf.jasperreports.engine.type.HyperlinkTypeEnum;
import net.sf.jasperreports.engine.type.PenEnum;
import net.sf.jasperreports.engine.type.VerticalAlignEnum;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;

import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public abstract class AdhocTextEntity extends AdhocEntity {

	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(AdhocTextEntity.class);
	private static final String HYPERLINK = "Hyperlink";
	private static final String RENDERHTML = "RenderHTML";
	public static final int DEFAULT_PADDING = 2;
	public static final int DEFAULT_VERTICAL_PADDING = 1;
	private String hyperlink;
	protected boolean renderHTML = false;
	
	protected void setDefaultEntityProperties(JRDesignTextField textField, boolean showNull_Zero) {
		super.setDefaultEntityProperties(textField);
		PenEnum border = getBorderLeft();
		BorderProperties.setPenAttributes(textField.getLineBox().getLeftPen(), border, this.getBorderColor());
		border = getBorderRight();
		BorderProperties.setPenAttributes(textField.getLineBox().getRightPen(), border, this.getBorderColor());
		border = getBorderTop();
		BorderProperties.setPenAttributes(textField.getLineBox().getTopPen(), border, this.getBorderColor());
		border = getBorderBottom();
		BorderProperties.setPenAttributes(textField.getLineBox().getBottomPen(), border, this.getBorderColor());
		
		Font hf = this.getFont();
		if (hf != null) {
			textField.setFontName(hf.getName());
			textField.setBold(hf.isBold());
			textField.setItalic(hf.isItalic());
			textField.setFontSize(hf.getSize());
			textField.setPdfEmbedded(Boolean.TRUE);
			textField.setPdfEncoding("Identity-H");
		} else
		{
			textField.setFontName(PageInfo.DEFAULT_FONT_NAME);
			textField.setFontSize(PageInfo.DEFAULT_FONT_SIZE);
		}
		
		textField.setHorizontalAlignment(getJRAlignment(this.getAlignment()));
		textField.setStretchWithOverflow(true);
		textField.getLineBox().setLeftPadding(DEFAULT_PADDING);
		textField.getLineBox().setRightPadding(DEFAULT_PADDING);
		textField.getLineBox().setTopPadding(DEFAULT_VERTICAL_PADDING);
		textField.getLineBox().setBottomPadding(DEFAULT_VERTICAL_PADDING);
		if(showNull_Zero)
			textField.setBlankWhenNull(false);
		else
			textField.setBlankWhenNull(true); // default behavior, show blank for null values
		
		String link = getHyperlink();
		if (link != null && Util.hasNonWhiteSpaceCharacters(link)) {
			textField.setHyperlinkType(HyperlinkTypeEnum.REFERENCE);
			JRDesignExpression hyperlinkReferenceExpression = new JRDesignExpression();
			hyperlinkReferenceExpression.addTextChunk("\"" + link + "\"");
			textField.setHyperlinkReferenceExpression(hyperlinkReferenceExpression);
		}
		
		textField.setVerticalAlignment(VerticalAlignEnum.MIDDLE);	
	}
	
	public void setHTMLStyle(AdhocReport report, JasperDesign jasperDesign, JRDesignTextField textField)
	{
		
		JRDesignStyle htmlStyle = getHTMLStyle(report, jasperDesign);
		if (htmlStyle != null)
			textField.setStyle(htmlStyle);
	}
	
	private JRDesignStyle getHTMLStyle(AdhocReport report, JasperDesign jasperDesign)
	{
		JRDesignStyle htmlStyle = null;
		String htmlStyleName = null;
		for (AdhocEntity entity : report.getEntities()) {
			if (entity instanceof AdhocTextEntity) {
				htmlStyleName = "HTML_" + this.getReportIndex();
			}
		}
		if (htmlStyleName != null)
		{
			try
			{
				boolean found = false;
				for (JRStyle style : jasperDesign.getStyles())
				{
					if (style.getName().equals(htmlStyleName))
					{
						found = true;
						htmlStyle = (JRDesignStyle) style;
						break;
					}
				}
				if (!found)
				{
					htmlStyle = new JRDesignStyle();
					htmlStyle.setName(htmlStyleName);
					jasperDesign.addStyle(htmlStyle);
				}
			} 
			catch (JRException e) {
				logger.error(e,e);
			}
		}
		return htmlStyle;
	}

	public static HorizontalAlignEnum getJRAlignment(Align align)
	{
		if (align == Align.Center)
			return HorizontalAlignEnum.CENTER;
		else if (align == Align.Right)
			return HorizontalAlignEnum.RIGHT;
		else if (align == Align.Justified)
			return HorizontalAlignEnum.JUSTIFIED;
		return HorizontalAlignEnum.LEFT;
	}

	private String getHyperlink() {
		return hyperlink;
	}

	private void setHyperlink(String hyperlink) {
		this.hyperlink = hyperlink;
	}
	
	public boolean parseAttribute(OMElement el, String elName) {
		if (HYPERLINK.equals(elName)) {
			setHyperlink(XmlUtils.getStringContent(el));
			return true;
		}
		else if (RENDERHTML.equals(elName)) {
			renderHTML = XmlUtils.getBooleanContent(el);
			return true;
		}
		return super.parseAttribute(el, elName);
	}
	
	public boolean parseAttribute(XMLStreamReader parser, String elName) throws Exception {
		if (HYPERLINK.equals(elName)) {
			setHyperlink(XmlUtils.getStringContent(parser));
			return true;
		}
		else if (RENDERHTML.equals(elName)) {
			renderHTML = XmlUtils.getBooleanContent(parser);
		}
		return super.parseAttribute(parser, elName);
	}
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		super.addContent(fac, parent, ns);
		XmlUtils.addContent(fac, parent, HYPERLINK, this.getHyperlink(), ns);
		XmlUtils.addContent(fac, parent, RENDERHTML, renderHTML, ns);
	}
	
	public abstract String getLabel();
	public abstract void setLabel(String l);
	public abstract void setLabelField(AdhocTextEntity t);
}
