/**
 * $Id: DrillParam.java,v 1.16 2011-09-27 19:07:21 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */


package com.successmetricsinc.adhoc;

import java.util.Iterator;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.ServerContext;
import com.successmetricsinc.query.DisplayExpression;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class DrillParam {
	private static Logger logger = Logger.getLogger(DrillParam.class);

    @SuppressWarnings("unused")
	private static final String ELEMENT_NAME = "Id";
	@SuppressWarnings("unused")
	private static final String NAME = "Name";
	private static final String EXPRESSION = "Expression";
	private static final String DRILL_PARAM = "DrillParam";
	private String Expression;
	private String parameterName;
    private transient DisplayExpression DE;
    @SuppressWarnings("unchecked")
	private transient Class clazz;

    public DrillParam() {
    }
    
	@SuppressWarnings("unchecked")
	public void parseElement(OMElement parent) {
		String expression = null;
		for (Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();
			if (EXPRESSION.equals(elName)) {
				expression = XmlUtils.getStringContent(el);
			}
			else if (AdhocColumn.DRILL_TO_PARAM_NAME.equals(elName))
				parameterName = XmlUtils.getStringContent(el);
			/*
			else if (NAME.equals(elName)) {
				// ignore
				name = XmlUtils.getStringContent(el);
			}
			else if (ELEMENT_NAME.equals(elName)) {
				// ignore
				this.ID = XmlUtils.getLongContent(el);
			}
			*/
		}
		try {
			this.setExpression(expression, expression);
		} catch (Exception e) {
			logger.error(e, e);
		}
	}
	public void parseElement(XMLStreamReader parser) throws Exception {
		String expression = null;
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
			iter.next();
			String elName = parser.getLocalName();
			if (EXPRESSION.equals(elName)) {
				expression = XmlUtils.getStringContent(parser);
			}
			else if (AdhocColumn.DRILL_TO_PARAM_NAME.equals(elName))
				parameterName = XmlUtils.getStringContent(parser);
			/*
			else if (NAME.equals(elName)) {
				// ignore
				name = XmlUtils.getStringContent(el);
			}
			else if (ELEMENT_NAME.equals(elName)) {
				// ignore
				this.ID = XmlUtils.getLongContent(el);
			}
			*/
		}
		try {
			this.setExpression(expression, expression);
		} catch (Exception e) {
			logger.error(e, e);
		}
	}
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		OMElement formatEl = fac.createOMElement(DRILL_PARAM, ns);
		XmlUtils.addContent(fac, formatEl, EXPRESSION, this.getExpression(), ns);
		XmlUtils.addContent(fac, formatEl, AdhocColumn.DRILL_TO_PARAM_NAME, getParameterName(), ns);
		parent.addChild(formatEl);
	}

	public String getExpression() {
		return Expression;
	}

	public void setExpression(String expression, String name)
	throws NullPointerException, BaseException, CloneNotSupportedException
	{
    	name = Util.replaceWithPhysicalString(name);
        name = name.replaceAll("\\}", "_");
        name = name.replaceAll("\\{", "_");
        name = name.replaceAll("\\.", "_");
        name = name.replaceAll("\"", "_");
        Expression = expression;
        Repository r = ServerContext.getRepository();
        DisplayExpression de = new DisplayExpression(r, expression, null);
        de.setName("CFEXPR_" + name);
        de.setPosition(-1);
        this.clazz = de.getCompiledClass();
        this.DE = de;
	}

    /**
     * Return the format expression's name
     * 
     * @return
     */
    public String getName()
    {
        if (DE==null)
            return (null);
        String name = DE.getName();
        return name; 
    }

    /**
     * Set the format expression's name
     * 
     * @return
     */
    public void setName(String name)
    {
    	name = Util.replaceWithPhysicalString(name);
        name = name.replaceAll("\\}", "_");
        name = name.replaceAll("\\{", "_");
        name = name.replaceAll("\\.", "_");
        if (DE!=null)
            DE.setName(name);
        return;
    }
	public DisplayExpression getDE() {
		return DE;
	}

	public void setDE(DisplayExpression de) {
		DE = de;
	}

	@SuppressWarnings("unchecked")
	public Class getClazz() {
		return clazz;
	}

	public void setClazz(@SuppressWarnings("unchecked") Class clazz) {
		this.clazz = clazz;
	}

	public String getParameterName() {
		return parameterName;
	}

	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}
    
}
