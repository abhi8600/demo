package com.successmetricsinc.adhoc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

import jxl.common.Logger;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.successmetricsinc.Group;
import com.successmetricsinc.Repository;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.adhoc.DrillRow.Type;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

public class DrillMapList {

	private static Logger logger = Logger.getLogger(DrillMapList.class);
	// possibly use in future
	//public static HashMap<MeasureColumn, String> measureColumnDrillMap = new HashMap<DimensionColumn, List<DrillColumnList>>();
	
	private HashMap<String, List<DrillColumnList>> dimensionColumnDrillDownMap = null;
	
	private List<DrillMap> drillMapList; 
	public static final String DRILL_MAP_LIST_ROOT = "DrillMaps";
	public static final String DRILL_MAP_ROOT = "DrillMap";
	public static final String DRILL_MAPS_FILE_NAME = "DrillMaps.xml";
	
	public DrillMapList(){		
	}
	
	public void populateDrillDownMap(SMIWebUserBean userBean) {
		
		if(dimensionColumnDrillDownMap == null)
		{
			dimensionColumnDrillDownMap = new HashMap<String, List<DrillColumnList>>();
		}
		
		dimensionColumnDrillDownMap.clear();
		List<Group> userGroups = userBean.getGroupList(true);
		boolean isOwner = userBean.isOwner();
		if(drillMapList != null && drillMapList.size() > 0)
		{
			for(DrillMap drillMap : drillMapList)
			{
				// if it is disabled do not add for consideration
				if(!drillMap.isEnabled())
				{
					continue;
				}
				
				
				if(!isOwner && !drillMap.accessAllowed(userGroups)){
					continue;
				}
				List<DrillRow> drillRows = drillMap.getDrillRows();
				if(drillRows != null && drillRows.size() > 0)
				{
					for(DrillRow drillRow : drillRows)
					{	
						// just for dimension for now
						if(drillRow.getType() == Type.Dimension && drillRow.isValid())
						{
							String dimensionColumnName = getDimensionColumnName(drillRow.getDimension(), drillRow.getColumn());
							List<DrillColumnList> drillColumnsList = dimensionColumnDrillDownMap.get(dimensionColumnName);
							if(drillColumnsList == null)
							{
								drillColumnsList = new ArrayList<DrillColumnList>();
								dimensionColumnDrillDownMap.put(dimensionColumnName, drillColumnsList);
							}
							drillColumnsList.add(drillRow.getDrillToColumn());
						}
					}
				}
			}
		}
	}
	
	public List<DrillColumnList> getDrillDownColumns(String dimension, String name)
	{
		if(dimensionColumnDrillDownMap != null){
			String dimensionColumnName = getDimensionColumnName(dimension, name);
			return dimensionColumnDrillDownMap.get(dimensionColumnName);
		}
		return null;
	}
	
	public static String getDimensionColumnName(String dimension, String name)
	{
		return dimension + "." + name;
	}
	
	public List<DrillMap> getDrillMapList() {
		return drillMapList;
	}

	public void setDrillMapList(List<DrillMap> drillMapList) {
		this.drillMapList = drillMapList;
	}
	
	public static DrillMapList read(File file) throws Exception {
		if(!file.exists())
		{
			return null;
		}
		XMLStreamReader parser = null;
		FileInputStream fis = null;
		InputStreamReader is = null;
		try {
			fis = new FileInputStream(file);
			is = new InputStreamReader(fis, "UTF-8");
			XMLInputFactory inputFactory = XmlUtils.getSecureXMLInputFactory();
			parser = inputFactory.createXMLStreamReader(is);
			DrillMapList drillMapList = new DrillMapList();
			return drillMapList.convert(parser, file, null);
		} catch (FileNotFoundException e) {
			logger.info("DrillMapList:read: " + e.getMessage(), e);
		} catch (XMLStreamException | FactoryConfigurationError e) {
			logger.error(e, e);
		} catch (UnsupportedEncodingException e) {
			logger.error(e, e);
		}finally{
			try	{
				if(parser != null){
					parser.close();
				}
			}catch(Exception ex){
				logger.warn("DrillMapList: Error in closing parser object ", ex);
			}
			try	{
				if(is != null){
					is.close();
				}
			}catch(Exception ex){
				logger.warn("DrillMapList: Error in closing inputstream object ", ex);
			}
			try {
				if(fis != null)	{
					fis.close();
				}
			}catch(Exception ex){
				logger.warn("DrillMapList: Error in closing fileinputstream object ", ex);
			}
		}		
		return null;
	}
	
	public DrillMapList convert(XMLStreamReader parser, File file, OMNamespace ns) throws Exception {
		if (parser != null) {
			for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
				
				String elName = parser.getLocalName();
				if (DRILL_MAP_ROOT.equals(elName))
					parseAndAddDrillMap(parser);
			}
			return this;
		}
		return null;
	}
	
	private void parseAndAddDrillMap(XMLStreamReader parser) throws Exception
	{
		DrillMap drillMap = new DrillMap();
		drillMap.parseElement(parser);
		if(this.drillMapList == null)
		{
			drillMapList = new ArrayList<DrillMap>();
		}
		drillMapList.add(drillMap);
	}
	
	public void save(File file) throws IOException 
	{	
		OMElement doc = getDrillMapsXMLDocument();
		FileOutputStream fos = null;
		
		try {
			if (! file.exists()) {
				file.createNewFile();
			}
			fos = new FileOutputStream(file);
			XMLStreamWriter writer = XMLOutputFactory.newInstance().createXMLStreamWriter(fos);
			doc.serialize(writer);
			writer.flush();
		} catch (IOException e) {
			logger.error("Error saving drill maps : (with attempted file name: " + file.toString() +")", e);
			throw (e);
		} catch (XMLStreamException e) {
			logger.error(e, e);
		} catch (FactoryConfigurationError e) {
			logger.error(e, e);
		}
		finally {
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
				}
			}
		}		
	}
	
	private OMElement getDrillMapsXMLDocument()
	{
		OMFactory fac = OMAbstractFactory.getOMFactory();
		return getDrillMapsXMLDocument(null, fac, true, false);
	}
	
	public OMElement getDrillMapsXMLDocument(OMNamespace ns, OMFactory fac, boolean deepCopy, boolean includeValidFlag)
	{
		OMElement root = fac.createOMElement(DRILL_MAP_LIST_ROOT, ns);
		if(drillMapList != null && drillMapList.size() > 0)
		{
			for (DrillMap drillMap : drillMapList) 
			{
				OMElement content = fac.createOMElement(DRILL_MAP_ROOT, ns);
				drillMap.addContent(fac, content, ns, deepCopy, includeValidFlag);
				root.addChild(content);
			}
		}
		return root;
	}

	public void addDrillMap(DrillMap  drillMap)
	{	
		if(this.drillMapList == null)
		{
			drillMapList = new ArrayList<DrillMap>();
		}
		
		DrillMap existingDrillMap = getExistingDrillMap(drillMap);
		if(existingDrillMap == null)
		{
			drillMapList.add(drillMap);
		}
	}
	
	public void deleteDrillMap(DrillMap drillMap)
	{
		DrillMap existingDrillMap = getExistingDrillMap(drillMap);
		if(existingDrillMap != null)
		{
			drillMapList.remove(existingDrillMap);
		}		
	}
	
	public void modifyDrillMap(DrillMap drillMap) 
	{
		if(drillMapList == null)
		{
			drillMapList = new ArrayList<DrillMap>();
		}

		DrillMap existingDrillMap = getExistingDrillMap(drillMap);
		if(existingDrillMap != null)
		{	
			// copy the created by and created date
			drillMap.setCreatedBy(existingDrillMap.getCreatedBy());
			drillMap.setCreatedDate(existingDrillMap.getCreatedDate());
			drillMapList.remove(existingDrillMap);
			drillMapList.add(drillMap);
		}
	}
	
	public DrillMap getExistingDrillMap(DrillMap drillMap)
	{
		DrillMap existingMap = null;
		for(DrillMap dmap : drillMapList)
		{
			String existingId = dmap.getId();
			if(existingId != null && drillMap.getId() != null 
					&& existingId.equalsIgnoreCase(drillMap.getId()))
			{
				existingMap = dmap;
				break;
			}
		}
		return existingMap;
	}
	
	public void save(String applicationPath) throws IOException
	{
		File file = new File(getDrillMapsFileName(applicationPath));
		save(file);
	}
	
	public static String getDrillMapsFileName(String applicationPath)
	{	
		return applicationPath + File.separator + DrillMapList.DRILL_MAPS_FILE_NAME;
	}

	// Go over all drill maps and validate the maps
	public void validate(Repository repository) 
	{
		if(drillMapList != null && drillMapList.size() > 0)
		{
			for(DrillMap drillMap : drillMapList)
			{
				drillMap.validate(repository);
			}
		}
	}
	

	
	/***
	 * /**
	 * Used to get Drill Map containing the drill from column 
	 * with the given dimension and column columns.
	 * skipThisId is the id of the incoming drillRow containing dimension and column
	 * @param drillFromDimension
	 * @param drillFromColumn
	 * @param skipThisId - can be null also
	 * @return
	 */
	public DrillMap getDrillMapWithDrillFrom(String drillFromDimension, String drillFromColumn, String skipThisId) 
	{		
		if(drillMapList != null && drillMapList.size() > 0)
		{
			for(DrillMap dMap : drillMapList)
			{
				List<DrillRow> drillRows = dMap.getDrillRows();
				if(drillRows != null && drillRows.size() > 0)
				{
					for(DrillRow drillRow : drillRows)
					{
						
						String id = drillRow.getId();
						if(id != null && skipThisId != null 
								&& id.equalsIgnoreCase(skipThisId))
								{	
									continue;
								}
						// going with this 2 level check to accomodate from measure column also						
						String dimension = drillRow.getDimension();
						String column = drillRow.getColumn();
						boolean matchColumn = false;
						if(drillFromColumn != null && column != null 
								&& drillFromColumn.equalsIgnoreCase(column))
						{
							matchColumn = true;
						}
							
						if(matchColumn)
						{
							// check for dimension
							boolean checkForDimension = drillFromDimension != null;
							if(checkForDimension)
							{
								if(dimension != null && dimension.equalsIgnoreCase(drillFromDimension))
								{
									return dMap;
								}
							}
							else
							{
								return dMap;
							}
						}
					}
				}
			}
		}
		return null;
	}

	public DrillMap getDrillMap(String drillMapName) 
	{
		if(drillMapName != null && drillMapList != null && drillMapList.size() > 0)
		{
			for(DrillMap dMap : drillMapList)
			{
				String name = dMap.getName();
				if(name != null && name.equalsIgnoreCase(drillMapName))
				{
					return dMap;
				}
			}
		}
		return null;
	}
}
