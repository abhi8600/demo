/**
 * 
 */
package com.successmetricsinc.adhoc;

import com.successmetricsinc.queueing.MessageResult;

/**
 * @author agarrison
 *
 */
public class ReportGenerationException extends Exception {
	private int ErrorCode;
	private String ErrorMessage;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ReportGenerationException(MessageResult mr) {
		super(mr.getErrorMessage());
		this.ErrorCode = mr.ErrorCode;
		this.ErrorMessage = mr.getErrorMessage();
	}
	
	public ReportGenerationException(String error) {
		super(error);
	}

	public int getErrorCode() {
		return ErrorCode;
	}

	public String getErrorMessage() {
		return ErrorMessage;
	}
}
