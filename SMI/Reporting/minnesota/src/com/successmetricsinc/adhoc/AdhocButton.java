/**
 * 
 */
package com.successmetricsinc.adhoc;

import java.awt.Color;
import java.util.List;
import java.util.Map;

import javax.xml.stream.XMLStreamReader;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRPropertiesMap;
import net.sf.jasperreports.engine.design.JRDesignElement;
import net.sf.jasperreports.engine.design.JRDesignExpression;
import net.sf.jasperreports.engine.design.JRDesignStyle;
import net.sf.jasperreports.engine.design.JRDesignTextField;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.type.HyperlinkTargetEnum;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.chart.Chart;
import com.successmetricsinc.util.ColorUtils;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class AdhocButton extends AdhocLabel {

	private static final String WRITE_BACK_VALIDATION_MESSAGE = "WriteBackValidationMessage";
	private static final String WRITE_BACK_FIELD = "WriteBackField";
	private static final String WRITE_BACK_NAME = "WriteBackName";
	private static final String WRITE_BACK = "WriteBack";
	private static final String USE_GRADIENT = "UseGradient";
	private static final String CORNER_RADIUS = "CornerRadius";
	private static final String GRADIENT_END_COLOR = "GradientEndColor";
	private static final String LINE_COLOR = "LineColor";

	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(AdhocButton.class);
	private int cornerRadius = 20;
	private Color lineColor;
	private boolean useGradient = false;
	private Color gradientEndColor;
	private float lineWidth = 1.0f;
	private List<AdhocColumn.AdditionalDrillThruColumn> additionalDrillThuColumns;
	private String additionalDrillThruString;
	private String drillTo;
	private String drillToDash;
	private String drillToDashPage;
	private boolean writeBack;
	public boolean isWriteBack() {
		return writeBack;
	}
	public void setWriteBack(boolean writeBack) {
		this.writeBack = writeBack;
	}
	public String getWriteBackName() {
		return writeBackName;
	}
	public void setWriteBackName(String writeBackName) {
		this.writeBackName = writeBackName;
	}
	public Integer getWriteBackField() {
		return writeBackField;
	}
	public void setWriteBackField(Integer writeBackField) {
		this.writeBackField = writeBackField;
	}

	private String writeBackName;
	private Integer writeBackField;
	private String validationMessage;
	
	public AdhocButton() {
		this.label = "Button";
	}
	/* (non-Javadoc)
	 * @see com.successmetricsinc.adhoc.AdhocEntity#getElement(com.successmetricsinc.adhoc.AdhocReport, net.sf.jasperreports.engine.design.JasperDesign, com.successmetricsinc.adhoc.PageInfo, boolean)
	 */
	@Override
	public JRDesignElement getElement(AdhocReport report, Repository r,
			JasperDesign jasperDesign, PageInfo pi, boolean isPdf, Map<String, ColumnSelector> selectors) {
		JRDesignTextField ret = (JRDesignTextField)super.getElement(report, r, jasperDesign, pi, isPdf, selectors);
		JRDesignStyle style = new JRDesignStyle();
		if (writeBack)
			style.setName("PROMPT_GROUP_" + reportIndex);
		else	
			style.setName("BUTTON_" + reportIndex);
		ret.setStyle(style);
		try {
			jasperDesign.addStyle(style);
		} catch (JRException e) {
			logger.error(e, e);
		}
		JRPropertiesMap props = ret.getPropertiesMap();
		props.setProperty(AdhocReport.JASPERREPORTS_EXPORT_PREFIX + CORNER_RADIUS, String.valueOf(cornerRadius));
		if (gradientEndColor != null && useGradient == true)
			props.setProperty(AdhocReport.JASPERREPORTS_EXPORT_PREFIX + "GradientEndColor", ColorUtils.colorToString(gradientEndColor));
		
		if (writeBack && writeBackField != null)
		{
			AdhocEntity ae = report.getEntityById(writeBackField);
			if (ae != null && ae instanceof AdhocColumn) {
				//add all writeback properties
				props.setProperty(AdhocReport.JASPERREPORTS_EXPORT_PREFIX + "operationName", writeBackName);
				String wbFieldName = ((AdhocColumn)ae).getName();
				props.setProperty(AdhocReport.JASPERREPORTS_EXPORT_PREFIX + "OptionsXML", "<Options><Option>" + wbFieldName + "</Option></Options>");
				props.setProperty(AdhocReport.JASPERREPORTS_EXPORT_PREFIX + "imagePath", label);
				props.setProperty(AdhocReport.JASPERREPORTS_EXPORT_PREFIX + "validationMessage", validationMessage);
				props.setProperty(AdhocReport.JASPERREPORTS_EXPORT_PREFIX + "targetDashboardName", getDrillToDash());
				props.setProperty(AdhocReport.JASPERREPORTS_EXPORT_PREFIX + "targetDashboardPage", getDrillToDashPage());
			}
		}
		
		StringBuilder filter = new StringBuilder();
		if (additionalDrillThruString != null && additionalDrillThruString.length() > 0) {
			filter.append(additionalDrillThruString);
		}
		String drillThruText = AdhocColumn.getDrillThruString(getDrillTo(), r, Chart.DRILL_TYPE_NAVIGATE, 
				filter, additionalDrillThuColumns, report, null, false, false);
		
		JRDesignExpression refExp = new JRDesignExpression();
		refExp.setText(drillThruText);

		ret.setHyperlinkReferenceExpression(refExp);
		ret.setHyperlinkTarget(HyperlinkTargetEnum.SELF);
		ret.setLinkType("javascript");
		if (! report.isDoNotStyleHyperlinks()) {
			ret.setUnderline(true);
			ret.setForecolor(Color.BLUE);
		}
		return ret;
	}
	@Override
	public boolean parseAttribute(OMElement el, String elName) {
		if (CORNER_RADIUS.equals(elName)) {
			cornerRadius = XmlUtils.getIntContent(el);
			return true;
		}
		else if (LINE_COLOR.equals(elName)) {
			lineColor = XmlUtils.getColorContent(el);
			return true;
		}
		else if (GRADIENT_END_COLOR.equals(elName)) {
			gradientEndColor = XmlUtils.getColorContent(el);
			return true;
		}
		else if (USE_GRADIENT.equals(elName)) {
			useGradient = XmlUtils.getBooleanContent(el);
			return true;
		}
		else if (AdhocColumn.DRILL_TO.equals(elName)) {
			drillTo = XmlUtils.getStringContent(el);
			return true;
		}
		else if (AdhocColumn.DRILL_TO_DASH.equals(elName)) {
			drillToDash = XmlUtils.getStringContent(el);
			return true;
		}
		else if (AdhocColumn.DRILL_TO_DASH_PAGE.equals(elName)) {
			drillToDashPage = XmlUtils.getStringContent(el);
			return true;
		}
		else if (AdhocColumn.ADDITIONAL_DRILL_THRU_COLUMNS.equals(elName)) {
			additionalDrillThuColumns = AdhocColumn.AdditionalDrillThruColumn.parseAdditionalDrillThruColumn(el);
			return true;
		}
		else if (AdhocColumn.ADDITIONAL_DRILL_THRU_STRING.equals(elName)) {
			additionalDrillThruString = XmlUtils.getStringContent(el);
			return true;
		}
		else if (WRITE_BACK.equals(elName)) {
			writeBack = XmlUtils.getBooleanContent(el);
			return true;
		}
		else if (WRITE_BACK_NAME.equals(elName)) {
			writeBackName = XmlUtils.getStringContent(el);
			return true;
		}
		else if (WRITE_BACK_FIELD.equals(elName)) {
			writeBackField = XmlUtils.getIntContent(el);
			return true;
		}
		else if (WRITE_BACK_VALIDATION_MESSAGE.equals(elName)) {
			validationMessage = XmlUtils.getStringContent(el);
			return true;
		}
		return super.parseAttribute(el, elName);
	}
	
	@Override
	public boolean parseAttribute(XMLStreamReader el, String elName)
			throws Exception {
		if (CORNER_RADIUS.equals(elName)) {
			cornerRadius = XmlUtils.getIntContent(el);
			return true;
		}
		else if (LINE_COLOR.equals(elName)) {
			lineColor = XmlUtils.getColorContent(el);
			return true;
		}
		else if (GRADIENT_END_COLOR.equals(elName)) {
			gradientEndColor = XmlUtils.getColorContent(el);
			return true;
		}
		else if (USE_GRADIENT.equals(elName)) {
			useGradient = XmlUtils.getBooleanContent(el);
			return true;
		}
		else if (AdhocColumn.DRILL_TO.equals(elName)) {
			drillTo = XmlUtils.getStringContent(el);
			return true;
		}
		else if (AdhocColumn.DRILL_TO_DASH.equals(elName)) {
			drillToDash = XmlUtils.getStringContent(el);
			return true;
		}
		else if (AdhocColumn.DRILL_TO_DASH_PAGE.equals(elName)) {
			drillToDashPage = XmlUtils.getStringContent(el);
			return true;
		}
		else if (AdhocColumn.ADDITIONAL_DRILL_THRU_COLUMNS.equals(elName)) {
			additionalDrillThuColumns = AdhocColumn.AdditionalDrillThruColumn.parseAdditionalDrillThruColumn(el);
			return true;
		}
		else if (AdhocColumn.ADDITIONAL_DRILL_THRU_STRING.equals(elName)) {
			additionalDrillThruString = XmlUtils.getStringContent(el);
			return true;
		}
		else if (WRITE_BACK.equals(elName)) {
			writeBack = XmlUtils.getBooleanContent(el);
			return true;
		}
		else if (WRITE_BACK_NAME.equals(elName)) {
			writeBackName = XmlUtils.getStringContent(el);
			return true;
		}
		else if (WRITE_BACK_FIELD.equals(elName)) {
			writeBackField = XmlUtils.getIntContent(el);
			return true;
		}
		else if (WRITE_BACK_VALIDATION_MESSAGE.equals(elName)) {
			validationMessage = XmlUtils.getStringContent(el);
			return true;
		}
		return super.parseAttribute(el, elName);
	}
	
	@Override
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, CORNER_RADIUS, cornerRadius, ns);
		XmlUtils.addContent(fac, parent, LINE_COLOR, lineColor, ns);
		XmlUtils.addContent(fac, parent, USE_GRADIENT, useGradient, ns);
		XmlUtils.addContent(fac, parent, GRADIENT_END_COLOR, gradientEndColor, ns);
		XmlUtils.addContent(fac, parent, AdhocColumn.DRILL_TO, drillTo, ns);
		XmlUtils.addContent(fac, parent, AdhocColumn.DRILL_TO_DASH, drillToDash, ns);
		XmlUtils.addContent(fac, parent, AdhocColumn.DRILL_TO_DASH_PAGE, drillToDashPage, ns);
		if (this.additionalDrillThuColumns != null && additionalDrillThuColumns.size() > 0) {
			AdhocColumn.AdditionalDrillThruColumn.addContent(fac, parent, additionalDrillThuColumns, ns);
		}
		if (additionalDrillThruString != null && additionalDrillThruString.length() > 0) 
			XmlUtils.addContent(fac, parent, AdhocColumn.ADDITIONAL_DRILL_THRU_STRING, additionalDrillThruString, ns);
		XmlUtils.addContent(fac, parent, WRITE_BACK, writeBack, ns);
		if (writeBackName != null && writeBackName.length() > 0)
			XmlUtils.addContent(fac, parent, WRITE_BACK_NAME, writeBackName, ns);
		if (writeBackField != null)
			XmlUtils.addContent(fac, parent, WRITE_BACK_FIELD, writeBackField, ns);
		if (validationMessage != null && validationMessage.length() > 0)
			XmlUtils.addContent(fac, parent, WRITE_BACK_VALIDATION_MESSAGE, validationMessage, ns);
		super.addContent(fac, parent, ns);
	}

	public Color getLineColor() {
		return lineColor;
	}

	public void setLineColor(Color lineColor) {
		this.lineColor = lineColor;
	}

	public Color getGradientEndColor() {
		return gradientEndColor;
	}

	public void setGradientEndColor(Color gradientEndColor) {
		this.gradientEndColor = gradientEndColor;
	}

	public void setLineWidth(float lineWidth) {
		this.lineWidth = lineWidth;
	}

	public float getLineWidth() {
		return lineWidth;
	}
	public String getDrillToDash() {
		if (drillToDash == null && drillTo != null) {
			if (drillTo.indexOf('_') > 0) {
				String[] drillToParts = drillTo.split("_");
				drillToDash = drillToParts[0];
				drillToDashPage = drillToParts[1];
			}
		}
		return drillToDash;
	}
	public String getDrillToDashPage() {
		if (drillToDashPage == null && drillTo != null) {
			if (drillTo.indexOf('_') > 0) {
				String[] drillToParts = drillTo.split("_");
				drillToDash = drillToParts[0];
				drillToDashPage = drillToParts[1];
			}
		}
		return drillToDashPage;
	}

	private String getDrillTo() {
		if (getDrillToDash() == null || getDrillToDashPage() == null)
			return drillTo;
		return getDrillToDash() + "','" + getDrillToDashPage();
	}
	public List<AdhocColumn.AdditionalDrillThruColumn> getAdditionalDrillThuColumns() {
		return additionalDrillThuColumns;
	}

}
