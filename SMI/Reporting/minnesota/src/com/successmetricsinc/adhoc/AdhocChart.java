/**
 * 
 */
package com.successmetricsinc.adhoc;

import java.awt.Color;
import java.awt.Font;
import java.awt.Paint;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.namespace.QName;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRExpressionChunk;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignElement;
import net.sf.jasperreports.engine.design.JRDesignExpression;
import net.sf.jasperreports.engine.design.JRDesignExpressionChunk;
import net.sf.jasperreports.engine.design.JRDesignFrame;
import net.sf.jasperreports.engine.design.JRDesignImage;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.type.PositionTypeEnum;
import net.sf.jasperreports.engine.JRException;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.apache.axiom.om.util.AXIOMUtil;
import org.apache.log4j.Logger;
import org.jdom.Element;

import com.successmetricsinc.Repository;
import com.successmetricsinc.ServerContext;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.adhoc.AdhocReportHelper;
import com.successmetricsinc.adhoc.AdhocColumn.ColumnType;
import com.successmetricsinc.adhoc.AdhocReport.Band;
import com.successmetricsinc.chart.ChartColors;
import com.successmetricsinc.chart.ChartGradientPaint;
import com.successmetricsinc.chart.ChartOptions;
import com.successmetricsinc.chart.ChartServerContext;	
import com.successmetricsinc.chart.AnyChart.AnyChartNode;
import com.successmetricsinc.chart.AnyChart.AnyChartNode.ChartType;
import com.successmetricsinc.chart.AnyChart.AnyChartNode.DataRows.DataRow;
import com.successmetricsinc.chart.AnyChart.Axes;
import com.successmetricsinc.chart.AnyChart.Background;
import com.successmetricsinc.chart.AnyChart.BarSeries;
import com.successmetricsinc.chart.AnyChart.BubbleSeries;
import com.successmetricsinc.chart.AnyChart.ChartSettings;
import com.successmetricsinc.chart.AnyChart.InvalidDataException;
import com.successmetricsinc.chart.AnyChart.LegendSettings;
import com.successmetricsinc.chart.AnyChart.NewGaugeSeries;
import com.successmetricsinc.chart.AnyChart.NoDataException;
import com.successmetricsinc.chart.AnyChart.ThreeD;
import com.successmetricsinc.chart.AnyChart.XAxis;
import com.successmetricsinc.query.DisplayFilter;
import com.successmetricsinc.query.DisplayFilter.Operator;
import com.successmetricsinc.query.QueryFilter;
import com.successmetricsinc.query.QueryResultSet;
import com.successmetricsinc.query.SyntaxErrorException;
import com.successmetricsinc.util.BirstXMLSerializable;
import com.successmetricsinc.util.TrellisTooLargeException;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class AdhocChart extends AdhocEntity  implements Cloneable {
	private static final String THREE_D_SETTINGS = "ThreeDSettings";
	private static final long serialVersionUID = 1L;
	private static final String FOOTER_COLOR = "FooterColor";
	private static final String SUBTITLE_COLOR = "SubtitleColor";
	private static final String TITLE_COLOR = "TitleColor";
	private static final String XAXIS = "XAxis";
	private static final String SHOW_BACKGROUND = "ShowBackground";
	private static final String COLORS = "Colors";
	private static final String THREE_D = "ThreeD";
	private static final String ANIMATION = "Animation";
	private static final String ORIENATION = "Orientation";
	private static final String LEGEND_SETTINGS = "LegendSettings";
	private static final String SHOW_LEGEND = "ShowLegend";
	private static final String FOOTER = "Footer";
	private static final String TITLE = "Title";
	private static final String SUBTITLE = "Subtitle";
	private static final String FOOTER_FONT = "FooterFont";
	private static final String TITLE_FONT = "TitleFont";
	private static final String SUBTITLE_FONT = "SubtitleFont";
	private static final String SUB_TITLE_ALIGN = "SubTitleAlign";
	private static final String FOOTER_ALIGN = "FooterAlign";
	private static final String TITLE_ALIGN = "TitleAlign";
	private static final String BACKGROUND = "Background";
	public static final String ANYCHART_CHART_BACKGROUND = "chart_background";
	public static final String ANYCHART_DATA_PLOT_BACKGROUND = "data_plot_background";
	public static final String ANYCHART_FALSE = "False";
	public static final String ANYCHART_TRUE = "True";
	public static final String ANYCHART_ENABLED = "enabled";
	public static final String ANYCHART_BORDER = "border";
	private static final String MARGIN_BOTTOM = "MarginBottom";
	private static final String MARGIN_RIGHT = "MarginRight";
	private static final String MARGIN_TOP = "MarginTop";
	private static final String MARGIN_LEFT = "MarginLeft";
	private static final String CHART_SETTINGS = "ChartSettings";
	private static final String SHOW_IN_REPORT = "showInReport";
	public static final String CHARTS = "Charts";
	private static final String SHOW_BORDER = "ShowBorder";
	public static final String BIRST_SELECTORS = "BIRST_SELECTORS";
	public static final String APPLY_ALL_PROMPTS = "APPLY_ALL_PROMPTS"; 
	public static final String IS_TRELLIS = "IsTrellis";
	public static final String TRELLIS_COLUMN = "TrellisColumn";
	public static final String TRELLIS_COLUMN_IDX = "TrellisColIndex";
	public static final String TRELLIS_COLUMN2 = "TrellisColumn2";
	public static final String TRELLIS_COLUMN2_IDX = "TrellisColIndex2";
	
		
	//We're using this dimensions in the flex client side as well
	public static final int DEFAULT_CHART_WIDTH = 400;
	public static final int DEFAULT_CHART_HEIGHT = 300;
	
	private static Logger logger = Logger.getLogger(AdhocChart.class);
	
	public enum PlotType { CategorizedVertical, CategorizedHorizontal, CategorizedBySeriesVertical,
		CategorizedBySeriesHorizontal, Scatter, Pie, Doughnut, Map, HeatMap, TreeMap, Funnel, Radar, UMap
	}
	
	private static final PlotType DEFAULT_PLOT_TYPE = PlotType.CategorizedVertical;
	private static final Orientation DEFAULT_ORIENTATION = Orientation.Horizontal;
	private static Map<PlotType, String> plotTypes = new HashMap<PlotType, String>();
	private transient PlotType plotType = DEFAULT_PLOT_TYPE;
	
	// <charts> node
	private List<AnyChartNode> charts;
	
	// <margin> node
	private int margin_left = 0;
	private int margin_top = 0;
	private int margin_right = 0;
	private int margin_bottom = 0;
	private boolean showBorder = false;
	private String footer = null;
	private Font footerFont;
	private String footerAlign;
	private String title = null;
	private Font titleFont;
	private String titleAlign;
	private String subTitle = null;
	private Font subTitleFont;
	private String subTitleAlign;
	private Color footerColor;
	private Color titleColor;
	private Color subTitleColor;
	
	private boolean showLegend = false;
	private LegendSettings legendSettings;
	private XAxis xAxis;
	
	private Background background;
	private boolean showBackground;
	// <chart_settings>
	private ChartSettings chartSettings;
	
	public enum Orientation { Horizontal, Vertical };
	private Orientation orientation = DEFAULT_ORIENTATION;
	private boolean threeD;
	private ThreeD threeDSettings = new ThreeD();
	
	private boolean animation;
	
	private String adhocReportXML;
	private AdhocReport parentReport;
	
	private List<Color> colorList;
	private boolean applyAllPrompts = false;
	private boolean showInReport = true;
	private boolean queryHasNoData = false;
	@SuppressWarnings("rawtypes")
	private Map parameterMap;
	
	private static final String DUMMY = "*** BIRST DUMMY VALUE ***";
	private static final int TrellisChartWidth = 150; // XXX make this controllable
	private static final double FudgeFactor = 0.25; // % larger for the left hand trellis chart
	private static final int MaxTrellisCharts = 64; // XXX make this controllable
	private static final int MaxFreeTrialTrellisCharts = 40; // XXX make this controllable
	private static final int SideFudge = 20; // XXX make this controllable
	private static final int BottomFudge = 10; // XXX make this controllable
	
	// trellis settings for the report
	private boolean isTrellis = false;
	private String trellisColumn = null;
	private int trellisColumnIdx = -1;
	private String trellisColumn2 = null;
	private int trellisColumn2Idx = -1;
	
	// values specific to trellis charts and are not persisted
	private int numChartsX;	// number of charts going across
	private int numChartsY; // number of charts going down (only useful for two attribute case, 1 in the case of a single attribute)
	private int chartsAcrossOnPage; 	// actual number across when doing a grid, used for page sizing
	private int chartsDownOnPage;		// actual number down when doing a grid, used for page sizing
	Set<String> uniqueValuesInFirstAttribute; 	// set of unique (and ordered) attribute values for the first trellis attribute/column
	Set<String> uniqueValuesInSecondAttribute; // set of unique (and ordered) attribute values for the second trellis attribute/column
	Map<String, String> firstAttributeLabelMap; 	// set of unique (and ordered) attribute values for the first trellis attribute/column
	Map<String, String> secondAttributeLabelMap; // set of unique (and ordered) attribute values for the second trellis attribute/column
	AdhocColumn targetColumn; // first trellis attribute column
	AdhocColumn targetColumn2; // second trellis attribute column
	
	int xtrellisColumnIdx = -1;
	int xtrellisColumn2Idx = -1;
	
	private static Map<Orientation, String> orientationMap = new HashMap<Orientation, String>();
	
	static {
		plotTypes.put(PlotType.CategorizedVertical, "CategorizedVertical");
		plotTypes.put(PlotType.CategorizedHorizontal, "CategorizedHorizontal");
		plotTypes.put(PlotType.CategorizedBySeriesVertical, "CategorizedBySeriesVertical");
		plotTypes.put(PlotType.CategorizedBySeriesHorizontal, "CategorizedBySeriesHorizontal");
		plotTypes.put(PlotType.Scatter, "Scatter");
		plotTypes.put(PlotType.Pie, "Pie");
		plotTypes.put(PlotType.Doughnut, "Doughnut");
		plotTypes.put(PlotType.Map, "Map");
		plotTypes.put(PlotType.HeatMap, "HeatMap");
		plotTypes.put(PlotType.TreeMap, "TreeMap");
		plotTypes.put(PlotType.Funnel, "Funnel");
		plotTypes.put(PlotType.Radar, "Radar");
		plotTypes.put(PlotType.UMap, "UMap");

		orientationMap.put(Orientation.Horizontal, "Horizontal");
		orientationMap.put(Orientation.Vertical, "Vertical");
	}
	
	public AdhocChart() {
		setWidth(DEFAULT_CHART_WIDTH);
		setHeight(DEFAULT_CHART_HEIGHT);
		parseDefaults();
	}
	
	public OMElement parseDefaults() {
		OMElement parent = getDefaults();
		if (parent != null)
			try {
				this.parseElement(parent);
			} catch (Exception e) {
				logger.error(e, e);
			}
		
		return parent;
	}
	
	public static OMElement getDefaults() {
		AdhocReport defaultReport = AdhocReport.getDefaultReportProperties();
		if (defaultReport != null) {
			AdhocChart defaultChart = defaultReport.getDefaultChart();
			if (defaultChart != null) {
				try {
					String xml = defaultChart.getEntityXML();
					OMElement parent = AXIOMUtil.stringToOM(xml);
					return parent;
				}
				catch (Exception e) {
					// do nothing
					logger.error(e, e);
				}
			}
		}
		return null;
	}
	
	public AdhocChart(ChartOptions co, AdhocReport report) {
		OMElement defaults = parseDefaults();
		this.setPositionType(PositionTypeEnum.FLOAT);
		this.setLeft(0);
		this.setTop(0);
		this.setWidth(co.width);
		this.setHeight(co.height);
		margin_left = co.leftOutsideMargin;
		margin_top = co.topOutsideMargin;
		margin_right = co.rightOutsideMargin;
		margin_bottom = co.bottomOutsideMargin;
		showBorder = ! co.noborder;
		
		//footer = co.footerLabel;
		
		showLegend = co.legend;
		showBackground = (co.chartBackgroundColor != null);
		colorList = co.chartColors;
		threeD = co.threeD;
		
		legendSettings = new LegendSettings(co);
		xAxis = new XAxis(co);
		background = new Background(co);
		chartSettings = new ChartSettings(co);
		
		if (co.type == ChartOptions.ChartType.Column || co.type == ChartOptions.ChartType.BarLine || 
				co.type == ChartOptions.ChartType.Line || co.type == ChartOptions.ChartType.StackedColumn ||
				co.type == ChartOptions.ChartType.StackedColumnLine || co.type == ChartOptions.ChartType.StackedArea)
			orientation = Orientation.Vertical;
		else
			orientation = Orientation.Horizontal;
		
		OMElement chartsEl = null;
		if (defaults != null) {
			chartsEl = defaults.getFirstChildWithName(new QName(CHARTS));
		}
		charts = AnyChartNode.createChartList(co, report, chartsEl == null ? null : chartsEl.getFirstElement());
	}
	
	/* (non-Javadoc)
	 * @see com.successmetricsinc.adhoc.AdhocEntity#getElement(com.successmetricsinc.query.QueryResultSet, com.successmetricsinc.adhoc.AdhocReport, net.sf.jasperreports.engine.design.JasperDesign, com.successmetricsinc.adhoc.PageInfo, boolean)
	 */
	@Override
	public JRDesignElement getElement(AdhocReport report, Repository r,
			JasperDesign jasperDesign, PageInfo pi, boolean isPdf, Map<String, ColumnSelector> selectors) {
		
		if (showInReport == false)
			return null;
		
		if(isTrellis){
			String name = AdhocReport.JASPERREPORTS_EXPORT_PREFIX + getFieldName();
			jasperDesign.setProperty(name, "![CDATA[" + getEntityXML() + "]]");
			
			JRDesignFrame frame = new JRDesignFrame(jasperDesign);
			frame.setX(getLeft());
			frame.setY(getTop());

			// BPD-12200 Want to see error msg if no data
			if(queryHasNoData)
			{	
				frame.setWidth(Math.max(DEFAULT_CHART_WIDTH, frame.getWidth()));
				frame.setHeight(Math.max(DEFAULT_CHART_HEIGHT, frame.getHeight()));
			}
			else
			{
				frame.setWidth(getWidth());
				frame.setHeight(getHeight());
			}
			
			JRDesignImage image = (JRDesignImage) getElementOld(report, r, jasperDesign, pi, isPdf, selectors);
			image.setX(getLeft() + 10);
			image.setY(getTop() + 10);

			// BPD-12200 Want to see error msg if no data
			if(queryHasNoData)
			{	
				image.setWidth(Math.max(DEFAULT_CHART_WIDTH - 20, image.getWidth() - 20));
				image.setHeight(Math.max(DEFAULT_CHART_HEIGHT - 10 , image.getHeight() - 20 ));			
			}
			else
			{
				image.setWidth(getWidth() - 20);
				image.setHeight(getHeight() - 10);
			}
			frame.addElement(image);

			return frame;
		}else{
			return getElementOld(report, r, jasperDesign, pi, isPdf, selectors);
		}
		
	}
	
	private  JRDesignElement getElementOld(AdhocReport report, Repository r,
			JasperDesign jasperDesign, PageInfo pi, boolean isPdf, Map<String, ColumnSelector> selectors) {
		// validate that the columns in categories, series and series2 still exist in the report
		
		
		// create a variable to contain the XML of this element
		String name = AdhocReport.JASPERREPORTS_EXPORT_PREFIX + getFieldName();
		jasperDesign.setProperty(name, "![CDATA[" + getEntityXML() + "]]");
/*		JRDesignVariable var = new JRDesignVariable();
		var.setCalculation(JRVariable.CALCULATION_NOTHING);
		var.setValueClass(String.class);
		var.setName(name);
		JRDesignExpression expression = new JRDesignExpression();
		expression.setValueClass(String.class);
		expression.setText("\"" + getEntityXML() + "\"");

		var.setInitialValueExpression(expression);
		var.setExpression(expression);
		try {
			jasperDesign.addVariable(var);
		} catch (JRException e) {
			logger.error(e,e);
			return null;
		}
		*/
		
		JRDesignImage image = new JRDesignImage(jasperDesign);
		image.setX(getLeft());
		image.setY(getTop());
		image.setWidth(getWidth());
		image.setHeight(getHeight());
		
		// add filters on both the report and the line and whatever is in the jdsp already.
		ChartFilters chartFilters = new ChartFilters();
		List<QueryFilter> qfs = report.getFilters();
//		Map parametersMap = jasperDesign.getParametersMap();
		if (qfs != null) {
			for (QueryFilter qf : qfs) {
				// don't know what to do with logical stuff
				// if prompted will be handled by jdsp parameters
				/*
				if (qf.isPrompted() && parametersMap.containsKey(qf.getPromptName())) {
					try {
						qf = (QueryFilter) qf.clone();
						qf.setOperand("$P{" + qf.getPromptName() + "}");
					} catch (CloneNotSupportedException e) {
						logger.error(e,e);
					}
					
				}
				*/
				if (qf.getType() != QueryFilter.TYPE_PREDICATE || (qf.isPrompted() && qf.getOperand() == null))
					continue;
				
				chartFilters.addFilter(qf);
			}
		}
		List<DisplayFilter> dfs = report.getDisplayFilters();
		if (dfs != null) {
			for (DisplayFilter df : dfs) {
				if (df.getType() != DisplayFilter.Type.Predicate || (df.isPrompted() && df.getOperand() == null))
					continue;
				
				chartFilters.addFilter(df);
			}
		}
		
		// add band specific filters now
		ArrayList<AdhocColumn> columns = new ArrayList<AdhocColumn>();
		for (AdhocColumn col : report.getColumns()) {
			if (col.getType() == ColumnType.Dimension) {
				if (getBand() == Band.Detail) {
					columns.add(col);
				}
				else if (getBand() == Band.GroupHeader || getBand() == Band.GroupFooter) {
					if ((col.getBand() == Band.GroupHeader || col.getBand() == Band.GroupFooter)
							&& col.getGroupNumber() <= getGroupNumber())
						columns.add(col);
				}
			}
		}
		for (AdhocColumn col : columns) {
			chartFilters.addFilter(col.getDimension(), col.getColumn(), "=", "\" + $F{" + col.getFieldName() + "} + \"");
		}
		
		JRDesignExpression parser = new JRDesignExpression();
		String chartFilterXmlText = chartFilters.getXmlText();
		if (chartFilterXmlText != null) {
			chartFilterXmlText = chartFilterXmlText.replace("\\", "\\\\");
		}
		parser.setText(chartFilterXmlText);
		
		JRDesignExpression imgExpr = new JRDesignExpression();
		imgExpr.addTextChunk("com.successmetricsinc.adhoc.AdhocChart.getRenderer(\"");
		imgExpr.addTextChunk(name);
		imgExpr.addTextChunk("\", ");
		imgExpr.addParameterChunk(JRParameter.JASPER_REPORT);
		imgExpr.addTextChunk(", ");
		imgExpr.addParameterChunk(JRParameter.REPORT_DATA_SOURCE);
		imgExpr.addTextChunk(", ");
		imgExpr.addParameterChunk(BIRST_SELECTORS);
		imgExpr.addTextChunk(", ");
		imgExpr.addParameterChunk(APPLY_ALL_PROMPTS);
		imgExpr.addTextChunk(", ");
		imgExpr.addParameterChunk(JRParameter.REPORT_PARAMETERS_MAP);
		imgExpr.addTextChunk(", \"");
		for (JRExpressionChunk chunk : parser.getChunks()) {
			imgExpr.addChunk((JRDesignExpressionChunk) chunk);
		}
		imgExpr.addTextChunk("\")");
		image.setExpression(imgExpr);
	
		return image;
		
	}
	
	private ChartFilters getChartFilters(AdhocReport report){
		ChartFilters chartFilters = new ChartFilters();
		List<QueryFilter> qfs = report.getFilters();
//		Map parametersMap = jasperDesign.getParametersMap();
		if (qfs != null) {
			for (QueryFilter qf : qfs) {
				// don't know what to do with logical stuff
				// if prompted will be handled by jdsp parameters
				/*
				if (qf.isPrompted() && parametersMap.containsKey(qf.getPromptName())) {
					try {
						qf = (QueryFilter) qf.clone();
						qf.setOperand("$P{" + qf.getPromptName() + "}");
					} catch (CloneNotSupportedException e) {
						logger.error(e,e);
					}
					
				}
				*/
				if (qf.getType() != QueryFilter.TYPE_PREDICATE || (qf.isPrompted() && qf.getOperand() == null))
					continue;
				
				if(isTrellis && qf.isTrellisChart())
					continue;
						
				chartFilters.addFilter(qf);
				
			}
		}
		List<DisplayFilter> dfs = report.getDisplayFilters();
		if (dfs != null) {
			for (DisplayFilter df : dfs) {
				if (df.getType() != DisplayFilter.Type.Predicate || (df.isPrompted() && df.getOperand() == null))
					continue;
				
				chartFilters.addFilter(df);
			}
		}
		
		// add band specific filters now
		ArrayList<AdhocColumn> columns = new ArrayList<AdhocColumn>();
		for (AdhocColumn col : report.getColumns()) {
			if (col.getType() == ColumnType.Dimension) {
				if (getBand() == Band.Detail) {
					columns.add(col);
				}
				else if (getBand() == Band.GroupHeader || getBand() == Band.GroupFooter) {
					if ((col.getBand() == Band.GroupHeader || col.getBand() == Band.GroupFooter)
							&& col.getGroupNumber() <= getGroupNumber())
						columns.add(col);
				}
			}
		}
		for (AdhocColumn col : columns) {
			chartFilters.addFilter(col.getDimension(), col.getColumn(), "=", "\" + $F{" + col.getFieldName() + "} + \"");
		}
		
		return chartFilters;
	}
	
	private String getFieldName()
	{
		StringBuilder fieldName = new StringBuilder();
		fieldName.append('F').append(reportIndex).append("_Chart");

		return Util.replaceWithPhysicalString(fieldName.toString());
	}

	/* (non-Javadoc)
	 * @see com.successmetricsinc.adhoc.AdhocEntity#parseAttribute(org.apache.axiom.om.OMElement)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean parseAttribute(OMElement el, String elName) {
		if (MARGIN_LEFT.equals(elName)) {
			margin_left = XmlUtils.getIntContent(el);
		}
		else if (MARGIN_TOP.equals(elName)) {
			margin_top = XmlUtils.getIntContent(el);
		}
		else if (MARGIN_RIGHT.equals(elName)) {
			margin_right = XmlUtils.getIntContent(el);
		}
		else if (MARGIN_BOTTOM.equals(elName)) {
			margin_bottom = XmlUtils.getIntContent(el);
		}
		else if (CHART_SETTINGS.equals(elName)) {
			chartSettings = (ChartSettings) XmlUtils.parseFirstChild(el, new ChartSettings());
		}
		else if (SHOW_BORDER.equals(elName)) {
			showBorder = XmlUtils.getBooleanContent(el);
		}
		else if (SHOW_LEGEND.equals(elName)) {
			showLegend = XmlUtils.getBooleanContent(el);
		}
		else if (LEGEND_SETTINGS.equals(elName)) {
			legendSettings = (LegendSettings)XmlUtils.parseFirstChild(el, new LegendSettings());
		}
		else if (ORIENATION.equals(elName)) {
			orientation = parseOrientation(XmlUtils.getStringContent(el));
		}
		else if (THREE_D.equals(elName)) {
			threeD = XmlUtils.getBooleanContent(el);
		}
		else if (THREE_D_SETTINGS.equals(elName))
			threeDSettings = (ThreeD)XmlUtils.parseFirstChild(el, new ThreeD());
		else if (ANIMATION.equals(elName)) {
			animation = XmlUtils.getBooleanContent(el);
		}
		else if (CHARTS.equals(elName)) {
			charts = XmlUtils.parseAllChildren(el, new AnyChartNode(), new ArrayList<AnyChartNode>());
			for (int i = 0; i < charts.size(); i++) {
				AnyChartNode node = charts.get(i);
				node.setLabel(String.valueOf(i));
			}
		}
		else if (BACKGROUND.equals(elName)) {
			background = (Background) XmlUtils.parseFirstChild(el, new Background());
		}
		else if (SHOW_BACKGROUND.equals(elName))
			showBackground = XmlUtils.getBooleanContent(el);
		else if (FOOTER.equals(elName))
			footer = XmlUtils.getStringContent(el);
		else if (TITLE.equals(elName))
			title = XmlUtils.getStringContent(el);
		else if (SUBTITLE.equals(elName))
			subTitle = XmlUtils.getStringContent(el);
		else if (FOOTER_FONT.equals(elName))
			footerFont = XmlUtils.getFontContent(el);
		else if (TITLE_FONT.equals(elName))
			titleFont = XmlUtils.getFontContent(el);
		else if (SUBTITLE_FONT.equals(elName))
			subTitleFont = XmlUtils.getFontContent(el);
		else if (TITLE_ALIGN.equals(elName))
			titleAlign = XmlUtils.getStringContent(el);
		else if (FOOTER_ALIGN.equals(elName))
			footerAlign = XmlUtils.getStringContent(el);
		else if (SUB_TITLE_ALIGN.equals(elName))
			subTitleAlign = XmlUtils.getStringContent(el);
		else if (TITLE_COLOR.equals(elName))
			titleColor = XmlUtils.getColorContent(el);
		else if (SUBTITLE_COLOR.equals(elName))
			subTitleColor = XmlUtils.getColorContent(el);
		else if (FOOTER_COLOR.equals(elName))
			footerColor = XmlUtils.getColorContent(el);
		else if (XAXIS.equals(elName))
			xAxis = (XAxis)XmlUtils.parseFirstChild(el, new XAxis());
		else if (COLORS.equals(elName)) {
			String clrs = XmlUtils.getStringContent(el);
			if (clrs != null) {
				String[] colors = clrs.split(";");
				colorList = new ArrayList<Color>();
				for (String s: colors) {
					String[] color = s.split("=");
					if (color.length > 1) {
						Color c = new Color(Integer.valueOf(color[1]));
						colorList.add(c);
					}
				}
			}
		}else if(IS_TRELLIS.equals(elName)){
			isTrellis = XmlUtils.getBooleanContent(el);
		}else if (TRELLIS_COLUMN.equals(elName)){
			trellisColumn = XmlUtils.getStringContent(el);
		}else if (TRELLIS_COLUMN_IDX.equals(elName)){
			trellisColumnIdx = XmlUtils.getIntContent(el);
		}else if (TRELLIS_COLUMN2.equals(elName)){
			trellisColumn2 = XmlUtils.getStringContent(el);
		}else if (TRELLIS_COLUMN2_IDX.equals(elName)){
			trellisColumn2Idx = XmlUtils.getIntContent(el);			
		}else if (SHOW_IN_REPORT.equals(elName)) {
			showInReport = XmlUtils.getBooleanContent(el);
			return true;
		}else {
			return super.parseAttribute(el, elName);
		}
		
		return true;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean parseAttribute(XMLStreamReader parser, String elName) throws Exception {
		if (MARGIN_LEFT.equals(elName)) {
			margin_left = XmlUtils.getIntContent(parser);
		}
		else if (MARGIN_TOP.equals(elName)) {
			margin_top = XmlUtils.getIntContent(parser);
		}
		else if (MARGIN_RIGHT.equals(elName)) {
			margin_right = XmlUtils.getIntContent(parser);
		}
		else if (MARGIN_BOTTOM.equals(elName)) {
			margin_bottom = XmlUtils.getIntContent(parser);
		}
		else if (CHART_SETTINGS.equals(elName)) {
			chartSettings = (ChartSettings) XmlUtils.parseFirstChild(parser, chartSettings == null ? new ChartSettings() : chartSettings);
		}
		else if (SHOW_BORDER.equals(elName)) {
			showBorder = XmlUtils.getBooleanContent(parser);
		}
		else if (SHOW_LEGEND.equals(elName)) {
			showLegend = XmlUtils.getBooleanContent(parser);
		}
		else if (LEGEND_SETTINGS.equals(elName)) {
			legendSettings = (LegendSettings)XmlUtils.parseFirstChild(parser, legendSettings == null ? new LegendSettings() : legendSettings);
		}
		else if (ORIENATION.equals(elName)) {
			orientation = parseOrientation(XmlUtils.getStringContent(parser));
		}
		else if (THREE_D.equals(elName)) {
			threeD = XmlUtils.getBooleanContent(parser);
		}
		else if (THREE_D_SETTINGS.equals(elName))
			threeDSettings = (ThreeD)XmlUtils.parseFirstChild(parser, threeDSettings == null ? new ThreeD() : threeDSettings);
		else if (ANIMATION.equals(elName)) {
			animation = XmlUtils.getBooleanContent(parser);
		}		
		else if (CHARTS.equals(elName)) {
			charts = XmlUtils.parseAllChildren(parser, new AnyChartNode(), new ArrayList<AnyChartNode>());
			for (int i = 0; i < charts.size(); i++) {
				AnyChartNode node = charts.get(i);
				node.setLabel(String.valueOf(i));
			}
		}
		else if (BACKGROUND.equals(elName)) {
			background = (Background) XmlUtils.parseFirstChild(parser, background == null ? new Background() : background);
		}
		else if (SHOW_BACKGROUND.equals(elName))
			showBackground = XmlUtils.getBooleanContent(parser);
		else if (FOOTER.equals(elName))
			footer = XmlUtils.getStringContent(parser);
		else if (TITLE.equals(elName))
			title = XmlUtils.getStringContent(parser);
		else if (SUBTITLE.equals(elName))
			subTitle = XmlUtils.getStringContent(parser);
		else if (FOOTER_FONT.equals(elName))
			footerFont = XmlUtils.getFontContent(parser);
		else if (TITLE_FONT.equals(elName))
			titleFont = XmlUtils.getFontContent(parser);
		else if (SUBTITLE_FONT.equals(elName))
			subTitleFont = XmlUtils.getFontContent(parser);
		else if (TITLE_ALIGN.equals(elName))
			titleAlign = XmlUtils.getStringContent(parser);
		else if (FOOTER_ALIGN.equals(elName))
			footerAlign = XmlUtils.getStringContent(parser);
		else if (SUB_TITLE_ALIGN.equals(elName))
			subTitleAlign = XmlUtils.getStringContent(parser);
		else if (TITLE_COLOR.equals(elName))
			titleColor = XmlUtils.getColorContent(parser);
		else if (SUBTITLE_COLOR.equals(elName))
			subTitleColor = XmlUtils.getColorContent(parser);
		else if (FOOTER_COLOR.equals(elName))
			footerColor = XmlUtils.getColorContent(parser);
		else if (XAXIS.equals(elName))
			xAxis = (XAxis)XmlUtils.parseFirstChild(parser, xAxis == null ? new XAxis() : xAxis);
		else if (COLORS.equals(elName)) {
			String clrs = XmlUtils.getStringContent(parser);
			if (clrs != null) {
				String[] colors = clrs.split(";");
				colorList = new ArrayList<Color>();
				for (String s: colors) {
					String[] color = s.split("=");
					if (color.length > 1) {
						Color c = new Color(Integer.valueOf(color[1]));
						colorList.add(c);
					}
				}
			}
		}else if (IS_TRELLIS.equals(elName)){
			isTrellis = XmlUtils.getBooleanContent(parser);
		}else if (TRELLIS_COLUMN.equals(elName)){
			trellisColumn = XmlUtils.getStringContent(parser);
		}else if (TRELLIS_COLUMN_IDX.equals(elName)){
			trellisColumnIdx = XmlUtils.getIntContent(parser);
		}else if (TRELLIS_COLUMN2.equals(elName)){
			trellisColumn2 = XmlUtils.getStringContent(parser);
		}else if (TRELLIS_COLUMN2_IDX.equals(elName)){
			trellisColumn2Idx = XmlUtils.getIntContent(parser);			
		}else if (SHOW_IN_REPORT.equals(elName)) {
			showInReport = XmlUtils.getBooleanContent(parser);
			return true;
		}
		else {
			return super.parseAttribute(parser, elName);
		}
		
		return true;
	}
	

	private Orientation parseOrientation(String o) {
		for (Orientation key: orientationMap.keySet()) {
			String s = orientationMap.get(key);
			if (s.equals(o))
				return key;
		}
		
		return DEFAULT_ORIENTATION;
	}

	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		super.addContent(fac, parent, ns);
		XmlUtils.addContent(fac, parent, MARGIN_LEFT, margin_left, ns);
		XmlUtils.addContent(fac, parent, MARGIN_TOP, margin_top, ns);
		XmlUtils.addContent(fac, parent, MARGIN_RIGHT, margin_right, ns);
		XmlUtils.addContent(fac, parent, MARGIN_BOTTOM, margin_bottom, ns);
		XmlUtils.addContent(fac, parent, CHART_SETTINGS, chartSettings, ns);
		XmlUtils.addContent(fac, parent, SHOW_BORDER, showBorder, ns);
		XmlUtils.addContent(fac, parent, BACKGROUND, background, ns);
		XmlUtils.addContent(fac, parent, SHOW_BACKGROUND, showBackground, ns);
		XmlUtils.addContent(fac, parent, SHOW_LEGEND, showLegend, ns);
		XmlUtils.addContent(fac, parent, FOOTER, footer, ns);
		XmlUtils.addContent(fac, parent, TITLE, title, ns);
		XmlUtils.addContent(fac, parent, SUBTITLE, subTitle, ns);
		XmlUtils.addContent(fac, parent, FOOTER_FONT, footerFont, ns);
		XmlUtils.addContent(fac, parent, TITLE_FONT, titleFont, ns);
		XmlUtils.addContent(fac, parent, SUBTITLE_FONT, subTitleFont, ns);
		XmlUtils.addContent(fac, parent, FOOTER_ALIGN, footerAlign, ns);
		XmlUtils.addContent(fac, parent, TITLE_ALIGN, titleAlign, ns);
		XmlUtils.addContent(fac, parent, SUB_TITLE_ALIGN, subTitleAlign, ns);
		XmlUtils.addContent(fac, parent, FOOTER_COLOR, footerColor, ns);
		XmlUtils.addContent(fac, parent, TITLE_COLOR, titleColor, ns);
		XmlUtils.addContent(fac, parent, SUBTITLE_COLOR, subTitleColor, ns);
		XmlUtils.addContent(fac, parent, LEGEND_SETTINGS, legendSettings, ns);
		XmlUtils.addContent(fac, parent, ORIENATION, orientationMap.get(orientation), ns);
		XmlUtils.addContent(fac, parent, THREE_D, threeD, ns);
		XmlUtils.addContent(fac, parent, THREE_D_SETTINGS, threeDSettings, ns);
		XmlUtils.addContent(fac, parent, ANIMATION, animation, ns);
		XmlUtils.addContent(fac, parent, XAXIS, xAxis, ns);
		XmlUtils.addContent(fac, parent, IS_TRELLIS, isTrellis, ns);
		XmlUtils.addContent(fac, parent, TRELLIS_COLUMN, trellisColumn, ns);
		XmlUtils.addContent(fac, parent, TRELLIS_COLUMN_IDX, trellisColumnIdx, ns);
		XmlUtils.addContent(fac, parent, TRELLIS_COLUMN2, trellisColumn2, ns);
		XmlUtils.addContent(fac, parent, TRELLIS_COLUMN2_IDX, trellisColumn2Idx, ns);		
		
		XmlUtils.addContent(fac, parent, SHOW_IN_REPORT, showInReport, ns);
		
		if (colorList != null && colorList.size() > 0) {
			StringBuilder color = new StringBuilder();
			for (Color c: colorList) {
				int clr = c.getRGB() & 0xFFFFFF; 
				if (color.length() > 0)
					color.append(';');
				color.append("color=");
				color.append(clr);
			}
			
			XmlUtils.addContent(fac, parent, COLORS, color.toString(), ns);
		}
		
		if (charts != null) {
			OMElement cs = fac.createOMElement(CHARTS, ns);
			for (AnyChartNode node: charts) {
				XmlUtils.addContent(fac, cs, "Chart", node, ns);
			}
			parent.addChild(cs);
		}
	}

	private AdhocReport getParentReport() {
		if (parentReport == null && adhocReportXML != null) {
			try {
				parentReport = AdhocReport.convert(adhocReportXML, null, null);
			} catch (Exception e) {
				logger.error(e,e);
			}
		}
		return parentReport;
	}
	
	public static AnyChartRenderer getRenderer(String parameterName, JasperReport jreport, JRDataSource dataSource, String selectors, String filters)
	{
		return getRenderer(parameterName, jreport, dataSource, selectors, true, filters);
	}

	@SuppressWarnings("rawtypes")
	public static AnyChartRenderer getRenderer(String parameterName, JasperReport jreport, JRDataSource dataSource, String selectors, Boolean applyAllPrompts, String filters) {
		return getRenderer(parameterName, jreport, dataSource, selectors, applyAllPrompts, new HashMap(), filters);
	}
	
	public static AnyChartRenderer getRenderer(String parameterName, JasperReport jreport, JRDataSource dataSource, String selectors, Boolean applyAllPrompts, @SuppressWarnings("rawtypes") Map parameterMap, String filters) {
		AdhocChart chart = new AdhocChart();
		ChartFilters chartFilters = new ChartFilters();
		String xml = jreport.getProperty(parameterName);
		// now xml has ![CDATA[...]]
		xml = xml.substring(8);
		xml = xml.substring(0, xml.length() - 2);
		
		chart.adhocReportXML = jreport.getProperty(AdhocReport.JASPERREPORTS_EXPORT_PREFIX + AdhocReport.ADHOC_REPORT);
		chart.applyAllPrompts  = applyAllPrompts;
		chart.parameterMap = parameterMap;
		
		XMLStreamReader parser;
		try {
			XMLInputFactory inputFactory = XmlUtils.getSecureXMLInputFactory();
			parser = inputFactory.createXMLStreamReader(new StringReader(xml));

			StAXOMBuilder builder = new StAXOMBuilder(parser);
			OMElement doc = null;
			doc = builder.getDocumentElement();
			chart.parseElement(doc);
			
			inputFactory = XmlUtils.getSecureXMLInputFactory();
			parser = inputFactory.createXMLStreamReader(new StringReader(filters));
			
			builder = new StAXOMBuilder(parser);
			doc = builder.getDocumentElement();
			chartFilters.parseElement(doc);
		} catch (XMLStreamException e) {
			logger.error(e, e);
		} catch (FactoryConfigurationError e) {
			logger.error(e, e);
		} catch (Exception e) {
			logger.error(e, e);
		}
		
		return new AnyChartRenderer(chart, dataSource, chartFilters, selectors);
		
	}

	/* (non-Javadoc)
	 * @see com.successmetricsinc.adhoc.AdhocEntity#getEntityXML()
	 */
	@Override
	public String getEntityXML() {
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement parent = factory.createOMElement(this.getClass().getName(), null);
		this.addContent(factory, parent, null);
		
		try {
			return parent.toStringWithConsume();
		} catch (XMLStreamException e) {
			logger.error(e, e);
		}
		return null;
	}

	/**
	 * @return the charts
	 */
	public List<AnyChartNode> getCharts() {
		return charts;
	}
	
	public Font getTitleFont() {
		return titleFont;
	}
	
	public void setTitleFont(String str)
	{
		titleFont = XmlUtils.getFontContent(str);
	}
	
	public void setTitleFontSize(float newSize){
		if (titleFont == null) {
			setTitleFont("Arial-BOLD-12");
		}
		titleFont = titleFont.deriveFont(newSize);
	}

	/**
	 * @return the showBorder
	 */
	public boolean isShowBorder() {
		return showBorder;
	}

	public void setMarginAttributes(Element margin) {
		margin.setAttribute("left", String.valueOf(margin_left));
		margin.setAttribute("top", String.valueOf(margin_top));
		margin.setAttribute("right", String.valueOf(margin_right));
		margin.setAttribute("bottom", String.valueOf(margin_bottom));
	}

	public List<ChartType> getChartTypes() {
		List<ChartType> chartTypes = new ArrayList<ChartType>();
		if (charts != null) {
			for (AnyChartNode node : charts) {
				chartTypes.add(node.getChartType());
				if (node.getDataSourcePath() == null || node.getDataSourcePath().trim().length() == 0) {
					node.setReport(getParentReport());
				}
			}
		}
		return chartTypes;
	}
	
	/**
	 * return the result set index for a column in the report
	 * @param report
	 * @param displayNames
	 * @param reportIndex
	 * @return
	 */
	private int getResultSetIndex(AdhocReport report, String[] displayNames, Integer reportIndex)
	{
		AdhocEntity entNT = report.getEntityById(reportIndex);
		if (entNT != null && entNT instanceof AdhocColumn)
		{
			AdhocColumn col = (AdhocColumn) entNT;
			return getResultSetIndex(col, displayNames);
		}
		return -1;
	}
	
	/**
	 * return the result set index for a column in the report
	 * @param col
	 * @param displayNames
	 * @return
	 */
	private int getResultSetIndex(AdhocColumn col, String[] displayNames)
	{
		String fieldName = col.getFieldName();
		for (int i = 0; i < displayNames.length; i++)
		{
			if (displayNames[i].equals(fieldName))
			{
				return i;
			}
		}
		return -1;
	}
	
	/**
	 * calculate the size of the trellis chart based upon the trellis attributes and the page size
	 * @param report
	 * @param dataSource
	 * @return
	 * @throws TrellisTooLargeException 
	 */
	public boolean adjustChartSizeForTrellis(AdhocReport report, JRDataSource dataSource) throws TrellisTooLargeException
	{
		if (isTrellis && (dataSource instanceof QueryResultSet))
		{
			QueryResultSet qrs = (QueryResultSet) dataSource;
			String[] displayNames = qrs.getDisplayNames();

			if(displayNames != null && displayNames.length > 0){
				// find the index of the first trellis column in the data row's column
				AdhocEntity ent = report.getEntityById(trellisColumnIdx);
				targetColumn = null;
				if (ent != null && ent instanceof AdhocColumn)
				{
					targetColumn = (AdhocColumn) ent;
					xtrellisColumnIdx = this.getResultSetIndex(targetColumn, displayNames);
				}
				
				if (xtrellisColumnIdx == -1)
					return false;

				// find the index of the second trellis column in the data row's column
				targetColumn2 = null;
				if (trellisColumn2Idx != -1)
				{
					AdhocEntity ent2 = report.getEntityById(trellisColumn2Idx);
					if (ent2 != null && ent2 instanceof AdhocColumn)
					{
						targetColumn2 = (AdhocColumn) ent2;
						xtrellisColumn2Idx = this.getResultSetIndex(targetColumn2, displayNames);
					}
				}

				// need a two dimensional array of filters, possibly just a list of lists
				// top level list is column 1, second level is 2
				uniqueValuesInFirstAttribute = new LinkedHashSet<String>();
				uniqueValuesInSecondAttribute = new LinkedHashSet<String>();
				firstAttributeLabelMap = new HashMap<String, String>();
				secondAttributeLabelMap = new HashMap<String, String>();
				SMIWebUserBean ub = SMIWebUserBean.getUserBean();
				for (int j = 0; j < qrs.numRows(); j++)
				{
					Object v = qrs.getRows()[j][xtrellisColumnIdx];
					if (v != null) {
						uniqueValuesInFirstAttribute.add(v.toString());
						if (! firstAttributeLabelMap.containsKey(v)) {
							// 
							if (v instanceof java.util.Calendar)
							{					
								SimpleDateFormat sdf = null;
								String []warehouseColumnsDataTypes = qrs.getWarehouseColumnsDataTypes();
								if (warehouseColumnsDataTypes[xtrellisColumnIdx] != null && warehouseColumnsDataTypes[xtrellisColumnIdx].equals(Repository.DATETIME))
								{
									sdf = ub.getDateTimeFormat(qrs.getColumnFormats()[xtrellisColumnIdx]); 
								}
								else if (warehouseColumnsDataTypes[xtrellisColumnIdx] != null && warehouseColumnsDataTypes[xtrellisColumnIdx].equals("Date"))
								{
									sdf = ub.getDateFormatProcessingTimeZone(qrs.getColumnFormats()[xtrellisColumnIdx]);
									
								}
								if (sdf != null)
									firstAttributeLabelMap.put(v.toString(), sdf.format(((java.util.Calendar) v).getTime()));
							}
							else
								firstAttributeLabelMap.put(v.toString(), v.toString());
						}
					}
					if (xtrellisColumn2Idx != -1)
					{
						Object v2 = qrs.getRows()[j][xtrellisColumn2Idx];
						if (v2 != null) {
							uniqueValuesInSecondAttribute.add(v2.toString());
							if (! secondAttributeLabelMap.containsKey(v2)) {
								// 
								if (v2 instanceof java.util.Calendar)
								{					
									SimpleDateFormat sdf = null;
									String []warehouseColumnsDataTypes = qrs.getWarehouseColumnsDataTypes();
									if (warehouseColumnsDataTypes[xtrellisColumn2Idx] != null && warehouseColumnsDataTypes[xtrellisColumn2Idx].equals(Repository.DATETIME))
									{
										sdf = ub.getDateTimeFormat(qrs.getColumnFormats()[xtrellisColumn2Idx]); 
									}
									else if (warehouseColumnsDataTypes[xtrellisColumn2Idx] != null && warehouseColumnsDataTypes[xtrellisColumn2Idx].equals("Date"))
									{
										sdf = ub.getDateFormatProcessingTimeZone(qrs.getColumnFormats()[xtrellisColumn2Idx]);
										
									}
									if (sdf != null)
										secondAttributeLabelMap.put(v2.toString(), sdf.format(((java.util.Calendar) v2).getTime()));
								}
								else
									secondAttributeLabelMap.put(v2.toString(), v2.toString());
							}
						}
					}
				}
				
				// for a single attribute trellis chart, add a dummy value to the second set so we can easily iterate over both sets
				boolean twoAttribute = true;
				if (uniqueValuesInSecondAttribute.isEmpty())
				{
					twoAttribute = false;
					uniqueValuesInSecondAttribute.add(DUMMY);
				}
				
				if (ub.isFreeTrial() && (uniqueValuesInFirstAttribute.size() * uniqueValuesInSecondAttribute.size() > MaxFreeTrialTrellisCharts))
					throw new TrellisTooLargeException("Your last change resulted in a trellis chart with too many chart instances (> 40)");
						
				// calculate the chart size
				int cwidth = TrellisChartWidth;
				int cheight = cwidth; // make them square

				// number of charts in each direction
				numChartsX = uniqueValuesInFirstAttribute.size();
				numChartsY = uniqueValuesInSecondAttribute.size();
				
				// number of charts for page sizing
				chartsAcrossOnPage = numChartsX;
				chartsDownOnPage = numChartsY;

				if (!twoAttribute)
				{
					// if one attribute, do a square root approximation
					chartsAcrossOnPage = (int) Math.ceil(Math.sqrt(numChartsX));
					chartsDownOnPage = chartsAcrossOnPage;
				}
                
                // BPD-12200  - wamt to see error msg if no data
				if(numChartsX != 0) 
				{	
					// don't include the table for trellis charts
					report.setIncludeTable(false);
					// adjust the page size to be as big as is necessary to contain the charts
					report.setPageSize(PageInfo.PAGE_SIZE_CUSTOM);
					report.setPageWidth((int) ((cwidth * chartsAcrossOnPage + cwidth * FudgeFactor) + report.getLeftMargin() + report.getRightMargin()) + SideFudge);
					report.setPageHeight((int) (cheight * chartsDownOnPage + report.getTopMargin() + report.getBottomMargin()) + BottomFudge);
	
					// set the chart size
					int height = report.getPageHeight() - report.getTopMargin() - report.getBottomMargin();
					int width = report.getPageWidth() - report.getLeftMargin() - report.getRightMargin();
					setHeight(height);
					setWidth(width);
	
					// need to also set the band height for the title band (where the chart resides)
					report.setTitleBandHeight(height);
               }
			   return true;
			}
		}
		return false;
	}
	
	/**
	 * Transform a regular chart to a Trellis chart
	 * @param report
	 * @param column
	 * @param columnIdx
	 */
	public void transformToTrellis(AdhocReport report, String column, int columnIdx){
		
		// make sure the column is an attribute
		AdhocEntity ent = report.getEntityById(columnIdx);
		if (!(ent instanceof AdhocColumn))
			return;
		AdhocColumn col = (AdhocColumn) ent;
		if (col.getType() != ColumnType.Dimension)
			return;
			
		// deal with swapping out categories
		boolean removeOldTrellis = true;
		if (isTrellis)
		{
			//Add /swap second trellis attribute
			this.trellisColumn2 = column;
			this.trellisColumn2Idx = columnIdx;
			
			for (AnyChartNode node : charts)
			{
				List<Integer> categories = node.getCategories();
				if (categories != null && categories.contains(columnIdx))
				{
					int index = categories.indexOf(columnIdx);
					categories.set(index, trellisColumn2Idx);
					removeOldTrellis = false;
				}
			}
		}
		
		// remove the previous trellis column if not a swap out
		if (removeOldTrellis && trellisColumn2Idx > 0 && trellisColumn2Idx != columnIdx){
			AdhocEntity entity = report.getEntityById(trellisColumn2Idx);
			if(entity != null){
				report.removeEntity(entity);
			}
		}
		
		if(!isTrellis){
			trellisColumn = column;
			trellisColumnIdx = columnIdx;
			isTrellis = true;
		}
	}
	
	private static class PrototypeSetComparator implements Comparator<List<Object>>
	{
		@Override
		public int compare(List<Object> o1, List<Object> o2) {
			// objects are List<Object>
			if (o1 == null && o2 == null)
				return 0;
			if (o1 == null)
				return -1;
			if (o2 == null)
				return 1;
			for (int i = 0; i < o1.size(); i++)
			{
				int res = compareWithNull(o1.get(i), o2.get(i));
				if (res != 0)
					return res;
			}
			return 0;
		}
		
		// comparison with null checks
		private static int compareWithNull(Object arg0, Object arg1)
		{
			if (arg0 == null && arg1 == null)
				return 0;
			if (arg0 == null)
				return -1;
			if (arg1 == null)
				return 1;
			if (arg0.equals(arg1))
				return 0;
			if (arg0 instanceof Integer)
				return ((Integer) arg0).compareTo((Integer) arg1);
			if (arg0 instanceof Long)
				return ((Long) arg0).compareTo((Long) arg1);
			if (arg0 instanceof Double)
				return ((Double) arg0).compareTo((Double) arg1);
			if (arg0 instanceof String)
				return ((String) arg0).compareTo((String) arg1);
			if (arg0 instanceof Date)
				return ((Date) arg0).compareTo((Date) arg1);
			if (arg0 instanceof Calendar)
				return ((Calendar) arg0).compareTo((Calendar) arg1);		
			return 0;
		}
	}
	
	/**
	 * build set rows that contain all non trellis attribute values in the original result set
	 * -- assume ordered left to right for sorting purposes
	 * @param nonTrellisAttributes
	 * @return
	 */
	private Object[][] buildPrototypeSet(QueryResultSet qrs, Set<Integer> nonTrellisAttributes, Set<Integer> resultSeriesSet)
	{
		Object[][] rows = qrs.getRows();
		Set<List<Object>> set = new TreeSet<List<Object>>(new PrototypeSetComparator());
		for (int i = 0; i < rows.length; i++)
		{
			// build a composite key for the row
			List<Object> keys = new ArrayList<Object>();
			for (Integer index : nonTrellisAttributes)
			{
				keys.add(rows[i][index]);
			}
			set.add(keys); // sorts and gets rid of duplicates
		}
		
		// build up a set of rows based upon the non trellis attributes and series
		Object[][] newRows = new Object[set.size()][];
		int i = 0;
		int columnCount = qrs.getColumnNames().length;
		for (List<Object> lst : set)
		{
			Object[] newRow = new Object[columnCount];
			for (int k = 0; k < columnCount; k++) // null out the columns
				newRow[k] = null;
			for (Integer index : resultSeriesSet) // set series columns to zero
			{
				newRow[index] = null; // XXX 0.0 or null?
			}
			int j = 0;
			for (Integer index : nonTrellisAttributes)	// fill in the non-trellis attribute values
			{
				newRow[index] = lst.get(j++);
			}
			newRows[i++] = newRow;
		}
		return newRows;
	}
	
	/**
	 * add missing rows (attribute values that were dropped due to filtering)
	 * -- O(n^2) performance, not good but probably okay for chart result sets
	 */
	private void addMissingRows(QueryResultSet qrs, Object[][] prototypeSet, Set<Integer> nonTrellisAttributes, Object trellis1Value, Object trellis2Value)
	{
		Object[][] newRows = new Object[prototypeSet.length][];
		
		for (int i = 0; i < newRows.length; i++)
		{
			// see if the non-trellis attribute row already exists in the result set, just copy the row over
			Object[] existingRow = findRow(qrs, prototypeSet[i], nonTrellisAttributes);
			if (existingRow != null)
			{
				newRows[i] = existingRow;
			}
			else
			{
				// the row does not exist, set the values of the trellis attributes, leave the rest alone
				newRows[i] = prototypeSet[i];
				if (xtrellisColumnIdx >= 0)
					newRows[i][xtrellisColumnIdx] = trellis1Value;
				if (xtrellisColumn2Idx >= 0)
					newRows[i][xtrellisColumn2Idx] = trellis2Value;
			}
		}
		qrs.setRows(newRows);
		return;
	}
	
	/**
	 * find the row in the result set that matches the nonTrellisAttributes
	 * -- assumes only one row matches (aggregation?)
	 * @param qrs
	 * @param prototypeRow
	 * @param nonTrellisAttributes
	 * @return
	 */
	private Object[] findRow(QueryResultSet qrs, Object[] prototypeRow, Set<Integer> nonTrellisAttributes)
	{
		Object[][] rows = qrs.getRows();
		for (int i = 0; i < rows.length; i++)
		{
			boolean found = true;
			Object[] row = rows[i];
			for (Integer index : nonTrellisAttributes)
			{
				if (!equalWithNull(row[index], prototypeRow[index]))
				{
					found = false;
					break;
				}
			}
			if (found)
				return row;
		}
		return null;
	}
	
	// equality with null checks
	private static boolean equalWithNull(Object obj1, Object obj2)
	{
		if (obj1 != null)
			return obj1.equals(obj2);
		if (obj2 == null)
			return true;
		return false;
	}
	
	/**
	 * build a good result set data array and find the non-trellis attribute indices
	 * @param qrs
	 * @param report
	 * @param nonTrellisAttributes
	 * @return
	 */
	private Object[][] buildPrototypeSet(QueryResultSet qrs, AdhocReport report, Set<Integer> nonTrellisAttributes)
	{
		// build up a list of all chart series in the result set so we can zero them out when adding dummy rows to deal with sparsity
		Set<Integer> chartSet = new HashSet<Integer>();
		Set<Integer> nonTrellisAttributesSet = new HashSet<Integer>();
		for (int k = 0; k < charts.size(); k++)
		{
			nonTrellisAttributesSet.addAll(charts.get(k).getCategories());
			chartSet.addAll(charts.get(k).getSeries());
			if (charts.get(k).getSeries2() != null)
				chartSet.addAll(charts.get(k).getSeries2());
			if (charts.get(k).getSeries3() != null)
				chartSet.addAll(charts.get(k).getSeries3());
		}
		
		Set<Integer> resultSeriesSet = new HashSet<Integer>();
		for (Integer reportIndex : chartSet)
		{
			int resultSetIndex = getResultSetIndex(report, qrs.getDisplayNames(), reportIndex);
			if (resultSetIndex >= 0)
				resultSeriesSet.add(resultSetIndex);
		}		
		for (Integer reportIndex : nonTrellisAttributesSet)
		{
			int resultSetIndex = getResultSetIndex(report, qrs.getDisplayNames(), reportIndex);
			if (resultSetIndex >= 0)
				nonTrellisAttributes.add(resultSetIndex);
		}
		return buildPrototypeSet(qrs, nonTrellisAttributes, resultSeriesSet);
	}
	
	/**
	 * generate the XML for the trellis chart
	 * @param dataSource
	 * @param chartFilters
	 * @param selectorXml
	 * @param scaleFactor
	 * @param isPrinted
	 * @return
	 * @throws NoDataException 
	 * @throws TrellisTooLargeException 
	 */
	public List<Element> getTrellisXml(JRDataSource dataSource, ChartFilters chartFilters, String selectorXml, double scaleFactor, boolean isPrinted) throws NoDataException, TrellisTooLargeException
	{
		//Check if there is a chart defined
		List<ChartType> chartTypes = getChartTypes();
		if (chartTypes == null || chartTypes.isEmpty())
			return null;
		
		ArrayList<Element> elements = new ArrayList<Element>();

		AdhocReport report = this.getParentReport();
		boolean calculate = false;
		try
		{
			calculate = adjustChartSizeForTrellis(report, dataSource);
		}
		catch (Exception ex)
		{
			
		}
		
		if (calculate)
		{
			QueryResultSet qrs = (QueryResultSet) dataSource;
			
			// BPD-12200 Want to see error msg if no data returned on trellis chart queries
			// AnyChartRenderer is catching nde
			if(numChartsX == 0) {
		    	queryHasNoData = true;
			    throw new NoDataException("Query returned no data.\nTry removing some filters.");
			}
			
			Set<Integer> nonTrellisAttributes = new TreeSet<Integer>();
			Object[][] prototypeSet = buildPrototypeSet(qrs, report, nonTrellisAttributes);
			
			int cwidth = TrellisChartWidth;
			int cheight = cwidth; // make them square

			// create the filtered result sets
			QueryResultSet[][] filteredResultSet = new QueryResultSet[numChartsX][numChartsY];
			int i = 0;
			int count = 0;
			 
			for (String keyX : uniqueValuesInFirstAttribute)
			{
				// stop after MaxTrellisCharts
				if (count > MaxTrellisCharts)
					break;

				DisplayFilter filterX = new DisplayFilter(targetColumn.getFieldName(), "=", keyX);
				int j = 0;
				for (String keyY : uniqueValuesInSecondAttribute)
				{
					if (count > MaxTrellisCharts)
						break;

					ArrayList<DisplayFilter> fList = new ArrayList<DisplayFilter>();
					if (!keyY.equals(DUMMY))
					{
						// two attributes, build an AND of the two filters
						DisplayFilter filterY = new DisplayFilter(targetColumn2.getFieldName(), "=", keyY);
						DisplayFilter filter = new DisplayFilter(filterX, Operator.And, filterY);
						fList.add(filter);
					}
					else
					{
						// one attribute
						fList.add(filterX);
					}
					try
					{
						// get result set based upon the one or two filters
						filteredResultSet[i][j] = qrs.returnFilteredResultSet(fList, qrs.getJasperDataSourceProvider().getRepository());
						addMissingRows(filteredResultSet[i][j], prototypeSet, nonTrellisAttributes, keyX, keyY);
					} 
					catch (SyntaxErrorException e) 
					{
						logger.error("Unable to add display filter on result set", e);
						return null;
					}
					j++;
					count++;
				}
				i++;
			}

			// calculate the min/max ranges for series, series2, and series3 so we can properly autoscale across all trellis charts
			Set<Integer> seriesSet = new HashSet<Integer>();
			Set<Integer> series2Set = new HashSet<Integer>();
			Set<Integer> series3Set = new HashSet<Integer>();
			for (int k = 0; k < charts.size(); k++)
			{
				seriesSet.addAll(charts.get(k).getSeries());
				if (charts.get(k).getSeries2() != null)
					series2Set.addAll(charts.get(k).getSeries2());
				if (charts.get(k).getSeries3() != null)
					series3Set.addAll(charts.get(k).getSeries3());
			}

			List<AdhocColumn> seriesColumns = new ArrayList<AdhocColumn>();
			for (Integer series : seriesSet)
			{
				AdhocEntity e = report.getEntityById(series);
				if (e != null && e instanceof AdhocColumn) {
					seriesColumns.add((AdhocColumn) e);
				}
			}
			List<AdhocColumn> series2Columns = new ArrayList<AdhocColumn>();
			for (Integer series : series2Set)
			{
				AdhocEntity e = report.getEntityById(series);
				if (e != null && e instanceof AdhocColumn) {
					series2Columns.add((AdhocColumn) e);
				}
			}
			List<AdhocColumn> series3Columns = new ArrayList<AdhocColumn>();
			for (Integer series : series3Set)
			{
				AdhocEntity e = report.getEntityById(series);
				if (e != null && e instanceof AdhocColumn) {
					series3Columns.add((AdhocColumn) e);
				}
			}

			Double trellisMin = null;
			Double trellisMax = null;
			Double trellis2Min = null;
			Double trellis2Max = null;	
			Double[][] trellis3Max = new Double[numChartsX][numChartsY];
			Double bubbleMax = null;

			count = 0;
			for (i = 0; i < uniqueValuesInFirstAttribute.size(); i++)
			{
				if (count > MaxTrellisCharts)
					break;
				for (int j = 0; j < uniqueValuesInSecondAttribute.size(); j++)
				{
					if (count > MaxTrellisCharts)
						break;
					AnyChartNode dummy = new AnyChartNode();
					AnyChartNode.DataRows dataRows = dummy.new DataRows(filteredResultSet[i][j]);
					for (DataRow dataRow : dataRows)
					{					
						for (AdhocColumn seriesCol: seriesColumns)
						{
							try
							{
								if (seriesCol.isMeasureExpression())
								{
									Number val = (Number) dataRow.getFieldValue(seriesCol.getFieldName(), seriesCol);
									if (val == null)
										continue;
									double v = val.doubleValue();
									if (trellisMin == null || trellisMin > v)
										trellisMin = v;
									if (trellisMax == null || trellisMax < v)
										trellisMax = v;
								}
							}
							catch (InvalidDataException e)
							{}
						}
						for (AdhocColumn seriesCol: series2Columns)
						{
							try
							{
								if (seriesCol.isMeasureExpression())
								{
									Number val = (Number) dataRow.getFieldValue(seriesCol.getFieldName(), seriesCol);
									if (val == null)
										continue;
									double v = val.doubleValue();
									if (trellis2Min == null ||trellis2Min > v)
										trellis2Min = v;
									if (trellis2Max == null || trellis2Max < v)
										trellis2Max = v;
								}
							}
							catch (InvalidDataException e)
							{}
						}
						for (AdhocColumn seriesCol: series3Columns)
						{
							try
							{
								if (seriesCol.isMeasureExpression())
								{
									Number val = (Number) dataRow.getFieldValue(seriesCol.getFieldName(), seriesCol);
									if (val == null)
										continue;
									double v = val.doubleValue();
									if (trellis3Max[i][j] == null || trellis3Max[i][j] < v)
										trellis3Max[i][j] = v;
									if (bubbleMax == null || bubbleMax < v)
										bubbleMax = v;
								}
							}
							catch (InvalidDataException e)
							{}
						}							
					}
					count++;
				}
			}

			boolean resize = true; // for pie/funnel/pyramid
			boolean swapped = false; // true if bar chart
			
			// adjust min max to add some slop
			if (trellisMin != null && trellisMax != null)
			{
				double slop = 0.0;
				double range = Math.abs(trellisMax - trellisMin);
				if (range == 0)
				{
					// no range, use absolute value of max
					double max = Math.max(Math.abs(trellisMax), Math.abs(trellisMin));
					slop = 0.10 * max;
				}
				else
					slop = 0.10 * range; // 10% slop
				trellisMin = trellisMin - slop;
				trellisMax = trellisMax + slop;
			}
			
			// start modifying chart, need a copy so that we can recover
			String saveXML = this.getEntityXML();
			
			// set the min and max, so the ranges are the same across the charts
			for (int k = 0; k < charts.size(); k++)
			{
				AnyChartNode ch = charts.get(k);
				Axes axes = ch.getAxes();
				if (ch.getChartType() == ChartType.Scatter || ch.getChartType() == ChartType.Bubble)
				{
					if (trellisMin != null)
						xAxis.setxMinimum(trellisMin.toString());
					if (trellisMax != null)
						xAxis.setxMaximum(trellisMax.toString());
					if (trellis2Min != null)
						axes.setyMinimum(trellis2Min.toString());
					if (trellis2Max != null)
						axes.setyMaximum(trellis2Max.toString());
				}
				else
				{
					if (trellisMin != null)
						axes.setyMinimum(trellisMin.toString());
					if (trellisMax != null)
						axes.setyMaximum(trellisMax.toString());
				}
				if (ch.getChartType() == ChartType.Pie || ch.getChartType() == ChartType.Funnel || ch.getChartType() == ChartType.Pyramid)
				{
					resize = false;
				}
				if (ch.getChartType() == ChartType.Bar || ch.getChartType() == ChartType.StackedBar)
				{
					swapped = true;
				}
			}

			// generate the charts
			i = 0;
			count = 0;
			if (this.colorList == null || colorList.size() == 0) {
				ChartColors chartColors = new ChartColors((plotType == PlotType.Pie || plotType == PlotType.Doughnut) ? 1 : 0, 1);
				int size = chartColors.getNumColors();
				for (int index = 0; index < size; index++) {
					Paint pnt = chartColors.getNextPaint();
					if (pnt instanceof Color)
						colorList.add((Color) pnt);
					else if (pnt instanceof ChartGradientPaint)
					{
						ChartGradientPaint cgp = (ChartGradientPaint) pnt;
						colorList.add(cgp.getBaseColor());
					}
				}
			}
			Font f = getTitleFont();
			setTitleFontSize(6); // also sets 'titleFont' as a side effect

			titleFont = titleFont.deriveFont(Font.BOLD, getFont().getSize2D());
			for (String keyX : uniqueValuesInFirstAttribute)
			{
				if (count > MaxTrellisCharts)
					break;
				int j = 0;
				for (String keyY : uniqueValuesInSecondAttribute)
				{	
					if (count > MaxTrellisCharts)
						break;
					
					// calculate the actual x and y indexes (dealing with two attributes and single attribute with grid)
					boolean twoAttribute = true;
					if (keyY.equals(DUMMY))
					{
						twoAttribute = false; // only one attribute
					}
					int xindex = (twoAttribute ? i : (i % chartsAcrossOnPage));
					int yindex = j + (twoAttribute ? 0 : (i / chartsAcrossOnPage));
					
					// set up the axes and chart specific settings
					for (int k = 0; k < charts.size(); k++)
					{
						AnyChartNode ch = charts.get(k);
						if (ch.getChartType() == ChartType.Bubble)
						{
							// scale the bubble size across all of the charts
							BubbleSeries bs = ch.getDataPlotSettings().getBubbleSeries();
							bs.setBubbleSizeMultiplier(String.valueOf(((double) BubbleSeries.DEFAULT_MAX_BUBBLE_SIZE / bubbleMax) * (trellis3Max[i][j]/bubbleMax)));
						}
						if (ch.getChartType() == ChartType.Bar || ch.getChartType() == ChartType.Column || ch.getChartType() == ChartType.StackedBar || ch.getChartType() == ChartType.StackedColumn 
								|| ch.getChartType() == ChartType.PercentStackedBar || ch.getChartType() == ChartType.PercentStackedColumn)
						{
							// put the bars/columns a little closer together
							BarSeries bs = ch.getDataPlotSettings().getBarSeries();
							bs.setPointPadding(0.1);
							bs.setGroupPadding(0.25);
						}
						Axes ax = ch.getAxes();
						ax.setyShowLabels(swapped ? true : xindex == 0);
						ax.setyShowTickmarkLabels(swapped ? true : xindex == 0);
						ax.setyShowGroupLabels(swapped ? true : xindex == 0);
						
						ch.setShowDisplayValues(false);
						ax.setLabelFontSize(6);
						ax.setTitleFontSize(6);
					}

					if (swapped && xAxis != null) // swapped is true for bar charts (domain axis on the left rather than at the bottom)
					{
						xAxis.setxShowLabels(xindex == 0);
						xAxis.setxShowTickmarkLabels(xindex == 0);
						xAxis.setxShowGroupLabels(xindex == 0);
					}
					/* XXX until we have better control over the plot area
					else if (resize)  // resize is not true for pie/funnel/pyramid charts
					{
						xAxis.setxShowLabels(twoAttribute ? (j == set2.size() - 1) : true);
						xAxis.setxShowTickmarkLabels(twoAttribute ? (j == set2.size() - 1) : true);
						xAxis.setxShowGroupLabels(twoAttribute ? (j == set2.size() - 1) : true);
					}
					*/

					// determine the top and left titles
					String titleX = firstAttributeLabelMap.get(keyX);
					String titleY = secondAttributeLabelMap.get(keyY);
					if (twoAttribute)
					{
						if (j != 0)
							titleX = null; // only need axis title on left hand side
					}
					else
					{
						titleY = null; // no title needed in this case (keyY is the dummy value)
					}
					
					// change font sizes before going in
					// set chart values to make it better suited for a smaller area
					if(xAxis != null)
					{
						xAxis.setLabelFontSize(6);
						xAxis.setTitleFontSize(6);
						//If we have Pie chart, set domain labels off
						for (AnyChartNode node : charts)
						{
							AnyChartNode.ChartType type = node.getChartType();
							if(type == AnyChartNode.ChartType.Pie || type == AnyChartNode.ChartType.Funnel || type == AnyChartNode.ChartType.Pyramid)
							{
								// xAxis.setShowDomainLabels(false);
								xAxis.setMaxChar("5");
								break;
							}
						}
					}
					
					// Generate the xml for each chart
					Element anychart = getXMLImpl(filteredResultSet[i][j], chartFilters, selectorXml, scaleFactor, isPrinted, titleX, titleY);

					// calculate where the place the chart, taking into account if we need to include a left hand axis
					long fudgedWidth = (int) ((double) cwidth * (1.0 + FudgeFactor));
					long firstWidth = resize ? fudgedWidth : cwidth;
					long actualWidth = (xindex == 0) ? firstWidth : cwidth;
					long x = (xindex == 0) ? 0 : (firstWidth + (xindex - 1) * cwidth);
					long y = yindex * cheight;
					long height = cheight;
					
					// scale the sizes and locations
					actualWidth = (long) ((double) actualWidth * scaleFactor);
					height = (long) ((double) height * scaleFactor);
					x = (long) ((double) x * scaleFactor);
					y = (long) ((double) y * scaleFactor);

					Element chartXml = new Element("chart");
					chartXml.setAttribute("x", String.valueOf(x));
					chartXml.setAttribute("y", String.valueOf(y));
					chartXml.setAttribute("width", String.valueOf(actualWidth));
					chartXml.setAttribute("height", String.valueOf(height));
					chartXml.addContent(anychart);
					elements.add(chartXml);
					j++;
					count++;
				}
				i++;
			}
			titleFont = f;
			
			// recover the old XML for the client
			try
			{
				StringReader reader = new StringReader(saveXML);
				XMLInputFactory inputFactory = XmlUtils.getSecureXMLInputFactory();
				XMLStreamReader parser = inputFactory.createXMLStreamReader(reader);		
				this.parseElement(parser);
			}
			catch (Exception ex)
			{
				logger.debug(ex, ex);
			}
		}
		else
		{
			elements.add(getXMLImpl(dataSource, chartFilters, selectorXml, scaleFactor, isPrinted, null, null));
		}
		return elements;
	}

   /************************************************************************************************************
    * Method to generate a "No Data Chart" especially in the case of trellis charts filtered to return no data.
    * We want to see the message "Query returned no data - try removing some filters. Returns the xml needed to
    * generate this.            
    *   
    * @return root
    * 
    *************************************************************************************************************/
	public Element generateNoDataChart(NoDataException nde, double scaleFactor)
	{
		Element root = new Element("chart");
		root.setAttribute("x", "0");
		root.setAttribute("y", "0");
		root.setAttribute("width", String.valueOf(DEFAULT_CHART_WIDTH));
		root.setAttribute("height", String.valueOf(DEFAULT_CHART_HEIGHT));
		
		Element chartAny = XmlUtils.findOrCreateChild(root, "anychart");
		Element settingsEl = XmlUtils.findOrCreateChild(chartAny, "settings");
		
		Element chartsEl = XmlUtils.findOrCreateChild(chartAny, "charts");
		Element chartEl = XmlUtils.findOrCreateChild(chartsEl, "chart");
		
		chartEl.setAttribute("plot_type", plotTypes.get(PlotType.CategorizedHorizontal));
		chartEl.removeChild("data");
		
		Element noDataEl = XmlUtils.findOrCreateChild(settingsEl, "no_data");
		Element labelEl = XmlUtils.findOrCreateChild(noDataEl, "label");
		Element textEl = XmlUtils.findOrCreateChild(labelEl, "text");
		
		textEl.setText(nde.getMessage());
		Font tahomaTen = Font.decode("Tahoma-PLAIN-8");
		AnyChartNode.addFontElement(labelEl, tahomaTen, scaleFactor);
		
		return (root);
	}
	
	public Element getXML(JRDataSource dataSource, ChartFilters chartFilters, String selectorXml, double scaleFactor, boolean isPrinted){
		return getXMLImpl(dataSource, chartFilters, selectorXml, scaleFactor, isPrinted, null, null);
	}
	
	public Element getXMLImpl(JRDataSource dataSource, ChartFilters chartFilters, String selectorXml, double scaleFactor, boolean isPrinted, String titleX, String titleY) {
		// determine plot type
		List<ChartType> chartTypes = getChartTypes();
		if (chartTypes == null || chartTypes.isEmpty())
			return null;
		
		Element root = new Element("anychart");
		// set up the context menu
		Element top_settings = XmlUtils.findOrCreateChild(root, "settings");
		Element menu = XmlUtils.findOrCreateChild(top_settings, "context_menu");
		String[] names = { "print_chart", "about_anychart", "save_as_pdf" };
		for (int i = 0; i < names.length; i++)
		{
			menu.setAttribute(names[i], ANYCHART_FALSE);
		}
		
		Element animationEl = XmlUtils.findOrCreateChild(top_settings, "animation");
		animationEl.setAttribute("enabled", animation && ServerContext.useChartAnimation() && !isPrinted ? ANYCHART_TRUE : ANYCHART_FALSE);
		
		Element margin = XmlUtils.findOrCreateChild(root, "margin");
		setMarginAttributes(margin);
		addDefaultSettings(root, isPrinted);
		
		try {
			if (chartTypes.contains(ChartType.Gauge)) {
				return getGaugeChart(root, dataSource, selectorXml, chartFilters, scaleFactor);
			} else if (chartTypes.contains(ChartType.NewGauge)) {
				return getNewGaugeChart(root, dataSource, selectorXml, chartFilters, scaleFactor);
			}
			Element chartsEl = XmlUtils.findOrCreateChild(root, "charts");
			
			boolean flipYAxis = false;
	
			plotType = PlotType.CategorizedVertical;
			if (chartTypes.contains(ChartType.Bar) || chartTypes.contains(ChartType.StackedBar) || chartTypes.contains(ChartType.PercentStackedBar) || chartTypes.contains(ChartType.RangeBar)) {
				plotType = PlotType.CategorizedHorizontal;
				flipYAxis = true;
			}
			else if (chartTypes.contains(ChartType.Column) || chartTypes.contains(ChartType.StackedColumn) || chartTypes.contains(ChartType.PercentStackedColumn) || chartTypes.contains(ChartType.RangeColumn) || chartTypes.contains(ChartType.Waterfall))
				plotType = PlotType.CategorizedVertical;
			else if (chartTypes.contains(ChartType.GeoMap)) 
				plotType = PlotType.Map;
			else if (chartTypes.contains(ChartType.UMap)) 
				plotType = PlotType.UMap;
			else if (chartTypes.contains(ChartType.Treemap))
				plotType = PlotType.TreeMap;
			else if (chartTypes.contains(ChartType.Heatmap))
				plotType = PlotType.HeatMap;
			else if (chartTypes.contains(ChartType.Pie))
				plotType = PlotType.Pie;
			else if (chartTypes.contains(ChartType.Doughnut))
				plotType = PlotType.Doughnut;			
			else if (chartTypes.contains(ChartType.Funnel) || chartTypes.contains(ChartType.Pyramid)) {
				if (background != null) {
					background.setFillColor(null);
				}
				plotType = PlotType.Funnel;
			}
			else if (chartTypes.contains(ChartType.Scatter) || chartTypes.contains(ChartType.Bubble)) {
				plotType = PlotType.Scatter;
				animationEl.setAttribute("enabled", ANYCHART_FALSE);
			}
			else if (chartTypes.contains(ChartType.Radar))
				plotType = PlotType.Radar;
			else if (orientation == Orientation.Horizontal) {
				plotType = PlotType.CategorizedHorizontal;
				flipYAxis = true;
			}
			
			if (selectorXml != null && selectorXml.indexOf("<ChartType>Line</ChartType>") > 0) {
				plotType = PlotType.CategorizedVertical;
				flipYAxis = false;
			}
			
			ChartColors chartColors = new ChartColors((plotType == PlotType.Pie || plotType == PlotType.Doughnut) ? 1 : 0, 1);
			if (this.colorList != null && colorList.size() > 0) {
				Color[] list = new Color[colorList.size()];
				for (int i = 0; i < list.length; i++) {
					list[i] = colorList.get(i);
				}
				chartColors.setColors(list);
			}
			
			Element chartEl = XmlUtils.findOrCreateChild(chartsEl, "chart");
			chartEl.setAttribute("plot_type", plotTypes.get(plotType));
			
			// set the title based on the chart filter if we're a trellis chart
			if (titleX != null)
			{
				this.title = titleX;
			}
			
			if (titleY != null)
			{
				for (AnyChartNode node : charts)
				{
					Axes axes = node.getAxes();
					axes.setyUseDefaultLabel(false);
					axes.setyLabel(titleY);
				}
			}
			
			if ((!queryHasNoData) && isTrellis)
			{
				xAxis.setxUseDefaultLabel(false);
				xAxis.setxLabel(" ");
			}
			
			Element settings = XmlUtils.findOrCreateChild(chartEl, "chart_settings");
			setChartSettings(settings, scaleFactor);
			
			if (background != null)
				background.getXML(settings, showBackground, showBorder);
			
			Element dataPlotSettingsEl = XmlUtils.findOrCreateChild(chartEl, "data_plot_settings"); 
			dataPlotSettingsEl.setAttribute("enable_3d_mode", threeD ? "True" : "False");
			if (threeD)
				threeDSettings.getXML(dataPlotSettingsEl);
			
			Element interactivityEl = XmlUtils.findOrCreateChild(dataPlotSettingsEl, "interactivity");
			interactivityEl.setAttribute("allow_multiple_select", "true");
			interactivityEl.setAttribute("allow_select", "true");
			Element selectRectangelEl = XmlUtils.findOrCreateChild(interactivityEl, "select_rectangle");
			selectRectangelEl.setAttribute("enabled", "True");
			selectRectangelEl.setAttribute("line_series_selection", "MarkersOnly");
			Element fillEl = XmlUtils.findOrCreateChild(selectRectangelEl, "fill");
			fillEl.setAttribute("enabled", "True");
			fillEl.setAttribute("color", "#067EE3");
			fillEl.setAttribute("opacity", ".3");
			Element borderEl = XmlUtils.findOrCreateChild(selectRectangelEl, "border");
			borderEl.setAttribute("enabled", "True");
			borderEl.setAttribute("color", "#057DE3");
			borderEl.setAttribute("thickness", "1");
			borderEl.setAttribute("opacity", "1");
			addAllChartLayers(dataSource, selectorXml, chartEl, chartFilters, chartColors, scaleFactor, flipYAxis, isPrinted, settings);
			
			determineScaling(chartEl, settings, dataPlotSettingsEl);
		}
		catch (Exception e) {
			// clear out all data
			Element chartsEl = XmlUtils.findOrCreateChild(root, "charts");
			Element chartEl = XmlUtils.findOrCreateChild(chartsEl, "chart");
			chartEl.setAttribute("plot_type", plotTypes.get(PlotType.CategorizedHorizontal));
			chartEl.removeChild("data");
			
			Element settingsEl = XmlUtils.findOrCreateChild(root, "settings");
			Element noDataEl = XmlUtils.findOrCreateChild(settingsEl, "no_data");
			Element labelEl = XmlUtils.findOrCreateChild(noDataEl, "label");
			Element textEl = XmlUtils.findOrCreateChild(labelEl, "text");
			textEl.setText(e.getMessage());
			Font tahomaTen = Font.decode("Tahoma-PLAIN-8");
			AnyChartNode.addFontElement(labelEl, tahomaTen, scaleFactor);
		}
		return root;
	}
	
	private void addAllChartLayers(JRDataSource dataSource, String selectorXml, Element chartEl, 
			ChartFilters chartFilters, ChartColors chartColors, double scaleFactor, boolean flipYAxis, 
			boolean isPrinted, Element settings) throws Exception {
		boolean hasData = false;
		NoDataException nde = null;
		for (AnyChartNode node : charts) {
			try {
				node.getXML(dataSource, selectorXml, chartEl, chartFilters, chartColors, scaleFactor, flipYAxis, xAxis, legendSettings, plotType, isPrinted, getParentReport(), applyAllPrompts, parameterMap, isTrellis, this);
				hasData = true;
			}
			catch (NoDataException e) {
				nde = e;
			}
		}
		
		if (hasData == false && nde != null) {
			throw nde;
		}
		
		if (plotType == PlotType.Map) {
			Element axesEl = settings.getChild("axes");
			if (axesEl != null) 
				axesEl.detach();
		}
		
	}
	
	@SuppressWarnings("rawtypes")		
	private void determineScaling(Element chartEl, Element settings, Element dataPlotSettingsEl) {
		// now start scaling axes, in the case of multiple charts
		Element axesEl = settings.getChild("axes");
		if (charts.size() >= 1 && axesEl != null) {
			
			List<String> axesNames = new ArrayList<String>();
			List<String> stackedAxesNames = new ArrayList<String>();
			List<Element> axesList = new ArrayList<Element>();
			boolean yAxisStacked = false;
			Element yAxis = axesEl.getChild("y_axis");
			if (yAxis != null) {
				axesList.add(yAxis);
				Element scaleEl = yAxis.getChild("scale");
				if (scaleEl != null) {
					String mode = scaleEl.getAttributeValue("mode");
					if (mode != null && mode.equalsIgnoreCase("stacked")) {
						yAxisStacked = true;
						stackedAxesNames.add("y_axis");
					}
					else
						axesNames.add("y_axis");
				}
				else {
					axesNames.add("y_axis");
				}
			}
			
			Element extraEl = axesEl.getChild("extra");
			if (extraEl != null) {
				List extraYs = extraEl.getChildren("y_axis");
				if (extraYs != null) {
					for (int i = 0; i < extraYs.size(); i++) {
						Element extraY = (Element) extraYs.get(i);
						if (extraY != null) {
							String enabled = extraY.getAttributeValue("enabled");
							if (enabled == null || enabled.equalsIgnoreCase("false")) {
								axesList.add(extraY);
								boolean addRegular = true;
								Element scaleEl = extraY.getChild("scale");
								if (scaleEl != null) {
									String mode = scaleEl.getAttributeValue("mode");
									if (mode != null && mode.equalsIgnoreCase("stacked"))
										addRegular = false;
								}
								if (addRegular)
									axesNames.add(extraY.getAttributeValue("name"));
								else
									stackedAxesNames.add(extraY.getAttributeValue("name"));
							}
						}
					}
				}
			}
			
			// now have a list of names of axes - find all series using those names
			// include any axis not using a y_axis name at all
			List<Element> seriesToRange = new ArrayList<Element>();
			List<Element> stackedSeriesToRange = new ArrayList<Element>();
			
			Element dataEl = chartEl.getChild("data");
			if (dataEl != null) {
				List seriesList = dataEl.getChildren("series");
				if (seriesList != null) {
					for (int i = 0; i < seriesList.size(); i++) {
						Element seriesEl = (Element)seriesList.get(i);
						String axisName = seriesEl.getAttributeValue("y_axis");
						if ((axisName == null && yAxisStacked == false) 
								|| axesNames.contains(axisName)) {
							seriesToRange.add(seriesEl);
						}
						else if ((axisName == null && yAxisStacked == true)
								|| stackedAxesNames.contains(axisName))
							stackedSeriesToRange.add(seriesEl);
					}
				}
			}
			
			if (seriesToRange.size() > 1 || stackedSeriesToRange.size() > 0) {
				// only do this if there are multiple series
				autoRangeSeries(seriesToRange, stackedSeriesToRange, axesList);
			}
		}
	}

	private Element getNewGaugeChart(Element root, JRDataSource dataSource, String selectorXml, ChartFilters chartFilters, double scaleFactor) throws Exception {
		if (charts == null || charts.isEmpty())
			return null;
		AnyChartNode node = charts.get(0);
		Element gauges = XmlUtils.findOrCreateChild(root, "gauges"); 
		Element gaugeEl = XmlUtils.findOrCreateChild(gauges, "gauge");
		Element settings = XmlUtils.findOrCreateChild(gaugeEl, "chart_settings");
		//HTML5 gauge charts do not like legends. Gauge charts do not show legend in html5 or flash
		this.showLegend = false;
		setChartSettings(settings, scaleFactor);
		if (background != null)
			background.getXML(settings, showBackground, showBorder);
		
		return node.getNewGaugeChart(root, dataSource, selectorXml, chartFilters, getParentReport(), applyAllPrompts, parameterMap, isTrellis);
	}

	private Element getGaugeChart(Element root, JRDataSource dataSource, String selectorXml, ChartFilters chartFilters, double scaleFactor) throws Exception {
		if (charts == null || charts.isEmpty())
			return null;
		AnyChartNode node = charts.get(0);
		Element gauges = XmlUtils.findOrCreateChild(root, "gauges"); 
		Element gaugeEl = XmlUtils.findOrCreateChild(gauges, "gauge");
		Element settings = XmlUtils.findOrCreateChild(gaugeEl, "chart_settings");
		
		//Bug DASH-313 Gauge / Meter charts fail in HTML5 with legend
		//Gauge charts do not show legends regardless anyway.
		this.showLegend = false;
		
		setChartSettings(settings, scaleFactor);
		if (background != null)
			background.getXML(settings, showBackground, showBorder);
		
		return node.getGaugeChart(root, dataSource, selectorXml, chartFilters, getParentReport(), applyAllPrompts, parameterMap, isTrellis);
	}
	private void addDefaultSettings(Element root, boolean isPrinted) {
		Element settingsEl = XmlUtils.findOrCreateChild(root, "settings");
		Element noDataEl = XmlUtils.findOrCreateChild(settingsEl, "no_data");
		noDataEl.setAttribute("show_waiting_animation", "False");
		Element labelEl = XmlUtils.findOrCreateChild(noDataEl, "label");
		labelEl.setAttribute("enabled", "True");
		Element textEl = XmlUtils.findOrCreateChild(labelEl, "text");
		textEl.setText("No data or too much data, please check your query.");
		Element mapsEl = XmlUtils.findOrCreateChild(settingsEl, "maps");
		
		if (isPrinted) {
			mapsEl.setAttribute("path_type", "Absolute");
			if (ChartServerContext.chartServerAmapFilepath == null)
			{
				logger.error("ChartServerContext.chartServerAmapFilepath is not specified in customer.properties");
			}
			else
			{
				mapsEl.setAttribute("path", ChartServerContext.chartServerAmapFilepath);
			}
		}
		else 
			mapsEl.setAttribute("path_type", "RelativeToSWF");
	}

	private void setChartSettings(Element settings, double scaleFactor) {

		final String ALIGN_BY = "Chart";
		if (this.footer != null && this.footer.length() > 0) {
			Element footer = XmlUtils.findOrCreateChild(settings, "footer");
			footer.setAttribute("enabled", "True");
			Element footerText = new Element("text");
			String f = this.footer;
			footerText.setText(f);
			footer.addContent(footerText);
			footer.setAttribute("align", 
					"Left".equals(footerAlign) ? "Near" : ("Right".equals(footerAlign) ? "Far" : "Center"));
			footer.setAttribute("align_by", ALIGN_BY);
			Element backgroundEl = XmlUtils.findOrCreateChild(footer, "background");
			backgroundEl.setAttribute("enabled", "False");
			if (footerFont != null) {
				Element font = BirstXMLSerializable.getFontElement(footerFont, scaleFactor);
				footer.addContent(font);
				AnyChartNode.addColorAttribute(font, footerColor);
			}
		}
		
		Element titleEl = XmlUtils.findOrCreateChild(settings, "title");
		if (title != null && title.length() > 0) {
			titleEl.setAttribute("enabled", "True");
			Element titleText = new Element("text");
			titleText.setText(title);
			titleEl.addContent(titleText);
			titleEl.setAttribute("align", 
					"Left".equals(titleAlign) ? "Near" : ("Right".equals(titleAlign) ? "Far" : "Center"));
			titleEl.setAttribute("align_by", ALIGN_BY);
			if (titleFont != null) {
				Element font = BirstXMLSerializable.getFontElement(titleFont, scaleFactor);
				titleEl.addContent(font);
				AnyChartNode.addColorAttribute(font, titleColor);
			}
		}
		else {
			titleEl.setAttribute(ANYCHART_ENABLED, ANYCHART_FALSE);
		}
		
		if (this.subTitle != null && subTitle.length() > 0) {
			Element footer = XmlUtils.findOrCreateChild(settings, "subtitle");
			footer.setAttribute("enabled", "True");
			Element footerText = new Element("text");
			String f = this.subTitle;
			footerText.setText(f);
			footer.addContent(footerText);
			footer.setAttribute("align", 
					"Left".equals(subTitleAlign) ? "Near" : ("Right".equals(subTitleAlign) ? "Far" : "Center"));
			footer.setAttribute("align_by", ALIGN_BY);
			Element backgroundEl = XmlUtils.findOrCreateChild(footer, "background");
			backgroundEl.setAttribute("enabled", "False");
			if (subTitleFont != null) {
				Element font = BirstXMLSerializable.getFontElement(subTitleFont, scaleFactor);
				footer.addContent(font);
				AnyChartNode.addColorAttribute(font, subTitleColor);
			}
		}
		
		addLegend(settings, scaleFactor);
		if (chartSettings != null) {
			chartSettings.getXML(settings);
		}
		
	}

	private void addLegend(Element settings, double scaleFactor) {
		if (this.showLegend) {
			LegendSettings.addLegendSettings(settings, legendSettings, scaleFactor);
		}
	}

	public void drillDown(AdhocReport report, Drill drill) {
		for (AnyChartNode node: charts) {
			node.drillDown(report, drill);
		}
		
	}

	public boolean isShowInReport() {
		return showInReport;
	}

	public void setShowInReport(boolean showInReport) {
		this.showInReport = showInReport;
	}

	@SuppressWarnings("rawtypes")
	private void autoRangeSeries(List<Element> seriesToRange, List<Element> stackedSeriesToRange, List<Element> axesList) {
		// for the stacked series
		// need to sum up each point with the same name across the list of series
		// to determine the stacked scale
		double max = Double.NEGATIVE_INFINITY;
		double min = Double.POSITIVE_INFINITY;
		if (stackedSeriesToRange != null) {
			Map<String, Double[]> pointSums = new HashMap<String, Double[]>();
			for (Element seriesEl : stackedSeriesToRange) {
				// for each series - get list of points
				String seriesName = seriesEl.getAttributeValue("y_axis");
				if (seriesName == null)
					seriesName = "y_axis";
				List points = seriesEl.getChildren("point");
				if (points != null) {
					for (int i = 0; i < points.size(); i++) {
						Element pointEl = (Element)points.get(i);
						String name = seriesName + "|" + pointEl.getAttributeValue("name");
						try {
							Double d = Double.valueOf(pointEl.getAttributeValue("y"));
							if (d == null || d.isNaN())
								continue;
							
							Double[] o = pointSums.get(name);
							if (o == null) {
								o = new Double[2];
								pointSums.put(name, o);
							}
							if (d.doubleValue() < 0) {
								if (o[0] == null || o[0].isNaN()) {
									o[0] = d;
								}
								else if (d != null && ! d.isNaN()) {
									o[0] = Double.valueOf(d.doubleValue() + o[0].doubleValue());
								}
							}
							else {
								if (o[1] == null || o[1].isNaN()) {
									o[1] = d;
								}
								else if (d != null && ! d.isNaN()) {
									o[1] = Double.valueOf(d.doubleValue() + o[1].doubleValue());
								}
							}
						}
						catch (Exception e) { // ignore 
							
						}
					}
				}
			}
			// always make sure that stacks cross 0
			min = 0;
			max = 0;
			for (Double[] d: pointSums.values()) {
				if (d[0] != null && d[1] != null) {
					max = Math.max(max, d[1].doubleValue());
					min = Math.min(min, d[0].doubleValue());
				}
				else if (d[0] != null) {
					max = Math.max(max, d[0].doubleValue());
					min = Math.min(min, d[0].doubleValue());
				}
				else if (d[1] != null) {
					max = Math.max(max, d[1].doubleValue());
					min = Math.min(min, d[1].doubleValue());
				}
			}
		}
		
		// now determine each individual scale across the rest of the series
		// by the highest and lowest point in each individual series
		if (seriesToRange != null) {
			for (Element seriesEl: seriesToRange) {
				List points = seriesEl.getChildren("point");
				if (points != null) {
					for (int i = 0; i < points.size(); i++) {
						Element pointEl = (Element)points.get(i);
						// need to get the various values in order to properly autorange
						String yString = pointEl.getAttributeValue("y");
						if (yString != null) {
							try {
								double d = Double.valueOf(yString);
								min = Math.min(min, d);
								max = Math.max(max, d);
							}
							catch (NumberFormatException nfe) { // who cares
								
							}
						}
						yString = pointEl.getAttributeValue("start");
						if (yString != null) {
							try {
								double d = Double.valueOf(yString);
								min = Math.min(min, d);
								max = Math.max(max, d);
							}
							catch (NumberFormatException nfe) { // who cares
								
							}
						}
						yString = pointEl.getAttributeValue("end");
						if (yString != null) {
							try {
								double d = Double.valueOf(yString);
								min = Math.min(min, d);
								max = Math.max(max, d);
							}
							catch (NumberFormatException nfe) { // who cares
								
							}
						}
					}
				}
			}
		}
		
		// add a slight margin to make sure the markers appear
		double margin = 0.075;
		min = (min > 0 ? min * (1.0 - margin) : min * (1.0 + margin));
		max = (max > 0 ? max * (1.0 + margin) : max * (1.0 - margin));
		
		if (min != max)
		{
			// okay, now i have min and max - set the range on each 
			for (Element yAxis: axesList) {
				Element scale = XmlUtils.findOrCreateChild(yAxis, "scale");
				if (scale.getAttribute("minimum") == null && scale.getAttribute("maximum") == null) {
					scale.setAttribute("minimum", String.valueOf(min));
					scale.setAttribute("maximum", String.valueOf(max));
				}
			}
		}
	}
	
	public void applyStyle(AdhocChart style) {
		margin_left = style.margin_left;
		margin_top = style.margin_top;
		margin_right = style.margin_right;
		margin_bottom = style.margin_bottom;
		showBorder = style.showBorder;
		footerFont = style.footerFont;
		footerAlign = style.footerAlign;
		titleFont = style.titleFont;
		titleAlign = style.titleAlign;
		subTitleFont = style.subTitleFont;
		subTitleAlign = style.subTitleAlign;
		footerColor = style.footerColor;
		titleColor = style.titleColor;
		subTitleColor = style.subTitleColor;
		showLegend = style.showLegend;
		showBackground = style.showBackground;
		orientation = style.orientation;
		threeD = style.threeD;
		animation = style.animation;
		colorList = new ArrayList<Color>();
		if (style.colorList != null && style.colorList.size() > 0)
			colorList.addAll(style.colorList);
		
		legendSettings.applyStyle(style.legendSettings);
		xAxis.applyStyle(style.xAxis);
		background.applyStyle(style.background);
		chartSettings.applyStyle(style.chartSettings);
		threeDSettings.applyStyle(style.threeDSettings);
		
		if (style.charts != null && style.charts.size() > 0) {
			AnyChartNode styleNode = style.charts.get(0);
			for (AnyChartNode node : charts) {
				node.applyStyle(styleNode);
			}
		}
	}
	
	public void isTrellis(boolean isTrellis){
		this.isTrellis = isTrellis;
	}
	
	public boolean isTrellis(){
		return this.isTrellis;
	}
	
	public void setTrellisColumn(String column){
		this.trellisColumn = column;
	}

	public void setupVisualizationChart(VisualizationChartConfig vcc, AdhocReport report) {
		if (charts == null || charts.size() == 0) {
			charts = new ArrayList<AnyChartNode>();
			charts.add(new AnyChartNode());
		}
		List<String> timeDims = AdhocReportHelper.getTimeDimensions();
		ChartType chartType = vcc.getDefaultChartType();
		List<AdhocColumn> columns = new ArrayList<AdhocColumn>();
		List<AdhocColumn> rows = new ArrayList<AdhocColumn>();
		List<AdhocColumn> details = new ArrayList<AdhocColumn>();
		int firstMeasureType = -1;
		for (AdhocColumn ac : report.getColumns()) {
			if (AdhocColumn.VISUALIZATION_PANEL_ROWS.equals(ac.getVisualizationPanel())) {
				rows.add(ac);
				if (firstMeasureType < 0 && ac.isMeasure()) {
					firstMeasureType = 0;
				}
			}
			else if (AdhocColumn.VISUALIZATION_PANEL_MEASURE.equals(ac.getVisualizationPanel())) {
				details.add(ac);
			}
			else {
				columns.add(ac);
				if (firstMeasureType < 0 && ac.isMeasure())
					firstMeasureType = 1;
			}
		}
		AnyChartNode node = charts.get(0);
		node.setChartType(chartType);
		node.getCategories().clear();
		node.getSeries().clear();
		
		// these happen regardless of the type of chart
		if (vcc.getNumColumnTimeAttributes() > 0) {
			// add that many time attributes to categories
			int numTimeDims = vcc.getNumColumnTimeAttributes();
			int numFound = 0;
			for (int i = 0; i < columns.size() && numFound < numTimeDims;) {
				AdhocColumn ac = columns.get(i);
				if (ac.isDimension() && timeDims.contains(ac.getDimension())) {
					node.getCategories().add(ac.reportIndex);
					columns.remove(i);
					numFound++;
				}
				else
					i++;
			}
		}
		if (vcc.getNumColumnAttributes() > 0) {
			// add the number of column attributes to 
			int numDims = vcc.getNumColumnAttributes();
			int numFound = 0;
			for (int i = 0; i < columns.size() && numFound < numDims; ) {
				AdhocColumn ac = columns.get(i);
				if (ac.isDimension()) {
					node.getCategories().add(ac.reportIndex);
					columns.remove(i);
					numFound++;
				}
				else
					i++;
			}
		}
		if (vcc.getNumRowTimeAttributes() > 0) {
			// add that many time attributes to categories
			int numTimeDims = vcc.getNumRowTimeAttributes();
			int numFound = 0;
			for (int i = 0; i < rows.size() && numFound < numTimeDims; ) {
				AdhocColumn ac = rows.get(i);
				if (ac.isDimension() && timeDims.contains(ac.getDimension())) {
					node.getCategories().add(ac.reportIndex);
					rows.remove(i);
					numFound++;
				}
				else
					i++;
			}
		}
		if (vcc.getNumRowAttributes() > 0) {
			// add the number of column attributes to 
			int numDims = vcc.getNumRowAttributes();
			int numFound = 0;
			for (int i = 0; i < rows.size() && numFound < numDims; ) {
				AdhocColumn ac = rows.get(i);
				if (ac.isDimension()) {
					node.getCategories().add(ac.reportIndex);
					rows.remove(i);
					numFound++;
				}
				else
					i++;
			}
		}
		
		switch (chartType) {
		case Column: // 1,1
		case Bar:  // 1,1
		case Line: //1,1
		case Pie: //1,1
		case Doughnut: //1,1
		case Area: //1,1
		case Treemap: //1,1
		case Pyramid: //1,1
		case Funnel: //1,1
		case GeoMap: //1,1
		case Radar: //1,1
		case Heatmap: //2,1
		case Waterfall: //2, 1
		case StackedBar: //1,2
		case StackedColumn: // 1,2
		case PercentStackedBar:
		case PercentStackedColumn:
			// need at least 1 dimension and 1 measure
			// which ones?
			
			// add all row and dimension measures
			for (int i = 0; i < rows.size(); ) {
				AdhocColumn ac = rows.get(i);
				if (ac.isMeasure()) {
					node.getSeries().add(ac.reportIndex);
					rows.remove(i);
				}
				else
					i++;
			}
			for (int i = 0; i < columns.size(); ) {
				AdhocColumn ac = columns.get(i);
				if (ac.isMeasure()) {
					node.getSeries().add(ac.reportIndex);
					columns.remove(i);
				}
				else
					i++;
			}
			
			if (node.getSeries().size() == 0) {
				if (details.size() > 0) {
					for (int i = 0; i < details.size(); ) {
						AdhocColumn ac = details.get(i);
						if (ac.isMeasure()) {
							node.getSeries().add(ac.reportIndex);
							details.remove(i);
							break;
						}
					}
				}
			}
			if (chartType == ChartType.StackedBar || chartType == ChartType.StackedColumn || chartType == ChartType.PercentStackedBar || chartType == ChartType.PercentStackedColumn) {
				if (details.size() > 0) {
					for (int i = 0; i < details.size(); i++) {
						AdhocColumn ac = details.get(i);
						if (ac.isDimension()) {
							node.getSeries().add(ac.reportIndex);
							details.remove(i);
							break;
						}
					}
				}
			}
			else {
				if (vcc.getNumDetailTimeAttributes() > 0) {
					// add that many time attributes to categories
					int numTimeDims = vcc.getNumDetailTimeAttributes();
					int numFound = 0;
					for (int i = 0; i < details.size() && numFound < numTimeDims; ) {
						AdhocColumn ac = details.get(i);
						if (ac.isDimension() && timeDims.contains(ac.getDimension())) {
							node.getCategories().add(ac.reportIndex);
							details.remove(i);
							numFound++;
						}
						else
							i++;
					}
				}
				if (vcc.getNumDetailAttributes() > 0) {
					// add the number of column attributes to 
					int numDims = vcc.getNumDetailAttributes();
					int numFound = 0;
					for (int i = 0; i < details.size() && numFound < numDims; ) {
						AdhocColumn ac = details.get(i);
						if (ac.isDimension()) {
							node.getCategories().add(ac.reportIndex);
							details.remove(i);
							numFound++;
						}
						else
							i++;
					}
				}
				
			}
			if (firstMeasureType == 0 && chartType != ChartType.Line) {
				// might need to convert orientation to horizontal
				this.orientation = Orientation.Horizontal;
			}
			// set threshold of colors to this field
			if (details.size() > 0) {
				for (int i = 0; i < details.size(); i++) {
					AdhocColumn ac = details.get(i);
					if (ac.isMeasure()) {
						node.setThresholding(ac.reportIndex);
						details.remove(i);
						break;
					}
				}
			}
			break;
			
		case RangeBar: //1,2
		case RangeColumn: //1,2
		case Scatter: //1,2
		case Bubble: // 1,3
			node.getSeries2().clear();
			// add all row and dimension measures
			for (int i = 0; i < rows.size(); ) {
				AdhocColumn ac = rows.get(i);
				if (ac.isMeasure()) {
					node.getSeries2().add(ac.reportIndex);
					rows.remove(i);
				}
				else
					i++;
			}
			for (int i = 0; i < columns.size(); ) {
				AdhocColumn ac = columns.get(i);
				if (ac.isMeasure()) {
					node.getSeries().add(ac.reportIndex);
					columns.remove(i);
				}
				else
					i++;
			}
			if (chartType == ChartType.Bubble) {
				node.getSeries3().clear();
				for (AdhocColumn ac : details) {
					if (ac.isMeasure()) {
						node.getSeries3().add(ac.reportIndex);
						details.remove(ac);
						break;
					}
				}
				if (node.getSeries3().size() == 0)
					node.setChartType(ChartType.Scatter);
			}
			else {
				if (vcc.getNumDetailTimeAttributes() > 0) {
					// add that many time attributes to categories
					int numTimeDims = vcc.getNumDetailTimeAttributes();
					int numFound = 0;
					for (int i = 0; i < details.size() && numFound < numTimeDims; ) {
						AdhocColumn ac = details.get(i);
						if (ac.isDimension() && timeDims.contains(ac.getDimension())) {
							node.getCategories().add(ac.reportIndex);
							details.remove(i);
							numFound++;
						}
						else
							i++;
					}
				}
				if (vcc.getNumDetailAttributes() > 0) {
					// add the number of column attributes to 
					int numDims = vcc.getNumDetailAttributes();
					int numFound = 0;
					for (int i = 0; i < details.size() && numFound < numDims; ) {
						AdhocColumn ac = details.get(i);
						if (ac.isDimension()) {
							node.getCategories().add(ac.reportIndex);
							details.remove(i);
							numFound++;
						}
						else
							i++;
					}
				}
				
			}
			break;
		case NewGauge://0,1
			for (AdhocColumn ac : (firstMeasureType == 1 ? columns : rows))
				node.getSeries().add(ac.reportIndex);
			NewGaugeSeries ngs = node.getDataPlotSettings().getNewGaugeSeries();
			ngs.setOrientationVertical(firstMeasureType == 0);
			break;
		}
		
		// if there are any columns dims left, use the last one as the horizontal trellis
		isTrellis(false);
		trellisColumnIdx = -1;
		trellisColumn2Idx = -1;

		AdhocColumn lastDim = null;
		for (AdhocColumn ac : columns) {
			if (ac.isDimension())
				lastDim = ac;
		}
		if (lastDim != null) {
			trellisColumnIdx = lastDim.reportIndex;
			isTrellis(true);
		}
		lastDim = null;
		for (AdhocColumn ac : rows) {
			if (ac.isDimension())
				lastDim = ac;
		}
		if (lastDim != null) {
			trellisColumn2Idx = lastDim.reportIndex;
			isTrellis(true);
		}
	}

}
