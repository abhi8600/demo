package com.successmetricsinc.adhoc;

import java.io.Serializable;
import java.util.Iterator;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

public class PivotTableOptions implements Serializable {

	private static final long serialVersionUID = 1L;

	//Root element xml tag for our options
	public final static String ROOT_TAG = "PivotTable";
	
	//Children xml element tags
	private final static String TAG_DRAW_LABEL = "DrawLabel";
	private final static String TAG_AUTO_SIZE = "AutoSize";
	private final static String TAG_MEASURE_LABEL_POS = "MeasureLabelPosition";
	
	//Possible values for AutoSize
	public final static String NO_AUTO_SIZE = "None";
	public final static String SIZE_ROW_LABEL = "RowLabel";
	public final static String SIZE_ALL_LABEL = "AllLabel";
	public final static String SIZE_ROW_MEASURE_COLUMN = "RowMeasureColumn";
	public final static String SIZE_ROW_COL_COLUMN = "RowColColumn";
	
	private final static String MEASURE_POS_COLUMN = "Column";
	
	//Member variables for current report pivot table options
	private boolean hasLabel = true;
	private String autoSizeBy = NO_AUTO_SIZE;
	private String measurePos = MEASURE_POS_COLUMN;
	
	public PivotTableOptions(){}
	
	//Adding our current configuration to xml structure
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		OMElement pt = fac.createOMElement(ROOT_TAG, ns);
		XmlUtils.addContent(fac, pt, TAG_DRAW_LABEL, hasLabel, ns);
		XmlUtils.addContent(fac, pt, TAG_AUTO_SIZE, autoSizeBy, ns);
		XmlUtils.addContent(fac, pt, TAG_MEASURE_LABEL_POS, measurePos, ns);
		parent.addChild(pt);
	}
	
	//Parsing the XML structure and storing it as our configuration
	public void parseElement(OMElement parent)
	{
		for (@SuppressWarnings("unchecked")
		Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();
			if (TAG_DRAW_LABEL.equals(elName)) {
				this.hasLabel = XmlUtils.getBooleanContent(el);
			}else if (TAG_AUTO_SIZE.equals(elName)){
				this.autoSizeBy = XmlUtils.getStringContent(el);
			}else if (TAG_MEASURE_LABEL_POS.equals(elName)){
				this.measurePos = XmlUtils.getStringContent(el);
			}
		}
	}
	
	public void parseElement(XMLStreamReader parser) throws Exception {
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
			iter.next();
			String elName = parser.getLocalName();
			if (TAG_DRAW_LABEL.equals(elName)) {
				this.hasLabel = XmlUtils.getBooleanContent(parser);
			}else if (TAG_AUTO_SIZE.equals(elName)){
				this.autoSizeBy = XmlUtils.getStringContent(parser);
			}else if (TAG_MEASURE_LABEL_POS.equals(elName)){
				this.measurePos = XmlUtils.getStringContent(parser);
			}
		}
	}

	//Is the measure label on column or row side
	public boolean isMeasureLabelsColumnWise(){
		return MEASURE_POS_COLUMN.equals(measurePos);
	}
	
	public boolean hasLabel(){
		return hasLabel;
	}
	
	public String getAutoResizeType(){
		return autoSizeBy;
	}
	
	public boolean isResizeTypeRowLabel(){
		return PivotTableOptions.SIZE_ROW_LABEL.equals(autoSizeBy);
	}
	
	public boolean isResizeTypeAllLabel(){
		return PivotTableOptions.SIZE_ALL_LABEL.equals(autoSizeBy);
	}
	
	public boolean isResizeTypeRowColColumns(){
		return PivotTableOptions.SIZE_ROW_COL_COLUMN.equals(autoSizeBy);
	}
	
	public boolean isResizeTypeRowMeasureColumns(){
		return PivotTableOptions.SIZE_ROW_MEASURE_COLUMN.equals(autoSizeBy);
	}

	public void applyStyle(PivotTableOptions pivotOpts) {
		if (pivotOpts != null) {
			hasLabel = pivotOpts.hasLabel;
			autoSizeBy = pivotOpts.autoSizeBy;
			measurePos = pivotOpts.measurePos;
		}
	}
}
