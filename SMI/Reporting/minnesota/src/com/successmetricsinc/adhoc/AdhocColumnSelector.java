/**
 * 
 */
package com.successmetricsinc.adhoc;

import java.util.ArrayList;
import java.util.List;

/**
 * @author agarrison
 *
 */
public class AdhocColumnSelector {

	private String name;
	private String selected;
	private List<ColumnSelector> items;
	
	public AdhocColumnSelector(String n, String s) {
		name = n;
		selected = s;
	}
	
	public void addColumnSelector(ColumnSelector cs) {
		if (items == null) {
			items = new ArrayList<ColumnSelector>();
		}
		items.add(cs);
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @return the selected
	 */
	public String getSelected() {
		return selected;
	}
	/**
	 * @return the items
	 */
	public List<ColumnSelector> getItems() {
		if (items == null) {
			items = new ArrayList<ColumnSelector>();
		}
		return items;
	}

	public void merge(AdhocColumnSelector acs) {
		// need to merge the 2 acs's into 1
		if (items == null) {
			return;
		}
		
		List<String> labels = new ArrayList<String>();
		for (ColumnSelector item : items) {
			String label = item.getID();
			labels.add(label);
		}
		
		for (ColumnSelector item : acs.getItems()) {
			String label = item.getID();
			if (! labels.contains(label)) {
				addColumnSelector(item);
			}
		}
	}
}
