/**
 * $Id: AdhocLabel.java,v 1.21 2012-09-11 00:55:01 BIRST\agarrison Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */


package com.successmetricsinc.adhoc;

import java.awt.Color;
import java.awt.Font;
import java.util.Map;

import javax.xml.stream.XMLStreamReader;

import net.sf.jasperreports.engine.design.JRDesignElement;
import net.sf.jasperreports.engine.design.JRDesignExpression;
import net.sf.jasperreports.engine.design.JRDesignTextField;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.type.PenEnum;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.successmetricsinc.Repository;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class AdhocLabel extends AdhocTextEntity {

	private static final long serialVersionUID = 1L;
	private static final String LABEL = "Label";
	private static final Color BORDER_COLOR = new Color(214,214,214); //d6d6d6
	protected String label = "static text";
	
	public AdhocLabel() {
		init();
	}
	public AdhocLabel(String label) {
		this.label = label;
		init();
	}
	
	private void init() {
		this.setFont(new Font(PageInfo.DEFAULT_FONT_NAME, Font.BOLD, PageInfo.DEFAULT_FONT_SIZE));
		this.setBorderBottom(PenEnum.ONE_POINT);
		this.setBorderColor(BORDER_COLOR); 
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	public String getHtmlDisplayLabel() {
		return "l: " + getLabel();
	}
	public boolean parseAttribute(OMElement el, String elName) {
		if (LABEL.equals(elName)) {
			this.setLabel(XmlUtils.getStringContent(el));
			return true;
		}
		
		return super.parseAttribute(el, elName);
	}
	
	public boolean parseAttribute(XMLStreamReader parser, String elName) throws Exception {
		if (LABEL.equals(elName)) {
			this.setLabel(XmlUtils.getStringContent(parser));
			return true;
		}
		
		return super.parseAttribute(parser, elName);
	}
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		super.addContent(fac, parent, ns);
		XmlUtils.addContent(fac, parent, LABEL, this.getLabel(), ns);
	}

	@Override
	public JRDesignElement getElement(AdhocReport report, Repository r, JasperDesign jasperDesign, PageInfo pi, boolean isPdf, Map<String, ColumnSelector> selectors) {
		if (isShowInReport() == false)
			return null;
		
		JRDesignTextField element = new JRDesignTextField();
		if (renderHTML)
			setHTMLStyle(report, jasperDesign, element);
		setDefaultEntityProperties(element, false);
		
		SMIWebUserBean ub = SMIWebUserBean.getUserBean();
		if (ub != null && ub.isFreeTrial()) {
			BorderProperties.setPenAttributes(element.getLineBox().getBottomPen(), PenEnum.ONE_POINT, 
					this.getBorderColor());
		}
		
		JRDesignExpression expression = new JRDesignExpression();
		String l = getLabel();
		if (l != null) {
			l = l.replaceAll("\\\\", "\\\\\\\\");
		}
		expression.addTextChunk("\"" + l + "\"");
		element.setExpression(expression);
		return element;
	}
	@Override
	public void setLabelField(AdhocTextEntity t) {
	}
}
