/**
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */


package com.successmetricsinc.adhoc;

import java.awt.Color;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Currency;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import jxl.write.DateTime;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRPropertiesMap;
import net.sf.jasperreports.engine.JRStyle;
import net.sf.jasperreports.engine.design.JRDesignBand;
import net.sf.jasperreports.engine.design.JRDesignElement;
import net.sf.jasperreports.engine.design.JRDesignExpression;
import net.sf.jasperreports.engine.design.JRDesignField;
import net.sf.jasperreports.engine.design.JRDesignStyle;
import net.sf.jasperreports.engine.design.JRDesignTextField;
import net.sf.jasperreports.engine.design.JRDesignVariable;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.type.CalculationEnum;
import net.sf.jasperreports.engine.type.EvaluationTimeEnum;
import net.sf.jasperreports.engine.type.HorizontalAlignEnum;
import net.sf.jasperreports.engine.type.HyperlinkTargetEnum;
import net.sf.jasperreports.engine.type.ResetTypeEnum;
import net.sf.jasperreports.engine.util.DefaultFormatFactory;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;

import com.successmetricsinc.DrillPath;
import com.successmetricsinc.Repository;
import com.successmetricsinc.ServerContext;
import com.successmetricsinc.Session;
import com.successmetricsinc.UserBean;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.WebServices.IWebServiceResult;
import com.successmetricsinc.chart.Chart;
import com.successmetricsinc.query.BirstInterval;
import com.successmetricsinc.query.BirstRatio;
import com.successmetricsinc.query.DimensionColumn;
import com.successmetricsinc.query.DisplayExpression;
import com.successmetricsinc.query.Level;
import com.successmetricsinc.query.MeasureColumn;
import com.successmetricsinc.query.QueryFilter;
import com.successmetricsinc.query.QueryResultSet;
import com.successmetricsinc.query.olap.BirstOlapField;
import com.successmetricsinc.query.olap.MemberSelectionExpression;
import com.successmetricsinc.query.olap.OlapLogicalQueryString.MEMBEREXPRESSIONTYPE;
import com.successmetricsinc.transformation.BirstScriptParser.booleanAndExpression_return;
import com.successmetricsinc.transformation.integer.ToTime;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class AdhocColumn extends AdhocTextEntity implements Cloneable, IWebServiceResult {
	public static final String DRILL_TO_PARAM_NAME = "DrillToParamName";
	private static final String SOURCE_FORMAT = "SourceFormat";
	public static final String SELECTED_PLUS_LEAVES = "SELECTED_PLUS_LEAVES";
	public static final String PARENTS = "PARENTS";
	public static final String SELECTED_PLUS_ALLCHILDREN = "SELECTED_PLUS_ALLCHILDREN";
	public static final String SELECTED_PLUS_CHILDREN = "SELECTED_PLUS_CHILDREN";
	public static final String SELECTED_PLUS_PARENTS = "SELECTED_PLUS_PARENTS";
	public static final String LEAVES = "LEAVES";
	public static final String CHILDREN = "CHILDREN";
	public static final String ALLCHILDREN = "ALLCHILDREN";
	public static final String SELECTED = "SELECTED";

	public static final String ADDITIONAL_OLAP_COLUMNS = "AdditionalOlapColumns";
	private static final String ADDITIONAL_OLAP_COLUMN = "Column";
	private static final String HIERARCHY = "Hierarchy";
	private static final String WRITE_BACK_TYPE = "WriteBackType";
	private static final String VISUALIZATION_PANEL = "VisualizationPanel";
	public static final String VISUALIZATION_PANEL_ROWS = "rows";
	public static final String VISUALIZATION_PANEL_COLUMNS = "columns";
	public static final String VISUALIZATION_PANEL_MEASURE = "measure";
	public static final String DRILL_TO_DASH_PAGE = "DrillToDashPage";
	public static final String DRILL_TO_DASH = "DrillToDash";
	private static final String IS_LEAF_CONDITIONAL_FORMAT_CREATED = "IsLeafCFC";
	public static final String ADDITIONAL_DRILL_THRU_STRING = "AdditionalDrillThruString";
	private static final long serialVersionUID = 1L;
	private static final String REMOVE_COLUMN_ON_DRILL = "RemoveColumnOnDrill";
	private static final String DENOMINATOR_EXPRESSION = "DenominatorExpression";
	private static final String ADD_DRILLED_COLUMNS_TO_NAVIGATE = "AddDrilledColumnsToNavigate";
	private static final String PERCENT_OF_COLUMN_VALUE = "PercentOfColumnValue";
	private static final String EXPRESSION_CLASS = "ExpressionClass";
	private static final String IS_MEASURE_EXPRESSION = "IsMeasureExpression";
	private static final String VERSION = "Version";
	private static final String DRILL_DOWN_COLUMNS = "DrillDownColumns";
	private static final String DRILL_DOWN_COLUMN = "DrillDownColumn";

	public static final String ADDITIONAL_DRILL_THRU_COLUMN = "AdditionalDrillThruColumn";

	public static final String ADDITIONAL_DRILL_THRU_COLUMNS = "AdditionalDrillThruColumns";

	private static final String COLUMN_SELECTORS = "ColumnSelectors";
	private static final String EXPRESSION_AGG_RULE = "ExpressionAggRule";
	private static final String COLUMN_TYPE = "ColumnType";
	private static final String LABEL_NAME = "LabelName";
	private static final String DRILL_PARAMS = "DrillParams";
	private static final String CONDITIONAL_FORMATS = "ConditionalFormats";
	private static final String FILTER_LIST = "FilterList";
	private static final String DISPLAY_EXPRESSION = "DisplayExpression";
	private static final String PIVOT_TOTAL = "PivotTotal";
	private static final String PIVOT_COLUMN = "PivotColumn";
	private static final String DRILL_TYPE = "DrillType";
	private static final String FILTER_ON_DRILL = "FilterOnDrill";
	public static final String DRILL_TO = "DrillTo";
	public static final String FORMAT = "Format";
	private static final String ORIGINAL_COLUMN = "OriginalColumn";
	public static final String COLUMN = "Column";
	public static final String DIMENSION = "Dimension";
	private static final String NAME = "Name";
	private static final String TECH_NAME = "TechnicalName";
	private static final String DATATYPE = "DataType";
	private static final String DATETIME_FORMAT = "DateTimeFormat";
	private static final String SHOW_REPEATING_VALUES = "ShowRepeatingValues";
	
	private static final int UNKNOWN_VERSION = 0;
	private static final int CURRENT_VERSION = 1;
	private static final int PIVOT_VALUE = -1;

	@SuppressWarnings("unchecked")
	public static final Class RATIO_CLASS = Number.class;

	private static Logger logger = Logger.getLogger(AdhocColumn.class);
	
	private static HashMap<ColumnType, String> columnTypeNames;
	
	private ColumnType Type;
	private String Name;
	private String Dimension;
	private String hierarchy;
	private String techName;
	private String Column;
	private String OriginalColumn;
	private String DataType;
	private String Format;
	private String sourceFormat;
	private List<OlapColumn> additionalOlapColumns;
	public enum ColumnType
	{
		Dimension, Measure, Expression, Ratio
	};
	private DisplayExpression Expression;
	private DisplayExpression denominatorInRatio;
	// do not read isMeasureExpression from anywhere - set when Expression is set
	private Boolean isMeasureExpression = null;
	// do not read classType from anywhere - set when Expression is set
	@SuppressWarnings("unchecked")
	private Class classType;
	private List<QueryFilter> filters;
	private boolean pivotColumn = false;
	private boolean pivotTotal = false;
	private List<ConditionalFormat> ConditionalFormats;
	private AdhocTextEntity label;
	private String drillTo;
	private String drillToParameterName;
	private String drillToDash;
	private String drillToDashPage;
	private String drillType = Chart.DRILL_TYPE_DRILL;
	private transient String labelLabel;
	private DateFormat _columnDateFormat = null; 
	private String exprAggRule = "SUM";
	private List<DrillParam> drillParamList;
	private boolean filterOnDrill = true;
	private boolean removeColumnOnDrill = false;
	private List<ColumnSelector> columnSelectors;
	private List<AdditionalDrillThruColumn> additionalDrillThuColumns;
	private String additionalDrillThruString;
	private List<DrillColumnList> drillToColumns;
	private int pivotValuePctOfCol = PIVOT_VALUE;
	private boolean addDrilledColumnsToNavigate = false;
	private boolean showRepeatingValues = true;

	private transient String expressionString;
	private transient String denominatorExpressionString; 
    private int version = UNKNOWN_VERSION;
    private boolean isLeafConditionalFormatCreated = false;
    private String visualizationPanel = null;
    
    public enum WriteBackType { None, CheckBox, TextField };
    private WriteBackType writeBackType = WriteBackType.None;
    
    private transient boolean sortablePivotField;
    
    private static HashMap<WriteBackType, String> writeBackTypeNames;
	
	static {
		columnTypeNames = new HashMap<ColumnType, String>();
		columnTypeNames.put(ColumnType.Dimension, DIMENSION);
		columnTypeNames.put(ColumnType.Measure, "Measure");
		columnTypeNames.put(ColumnType.Expression, "Expression");
		columnTypeNames.put(ColumnType.Ratio, "Ratio");
		
		writeBackTypeNames = new HashMap<WriteBackType, String>();
		writeBackTypeNames.put(WriteBackType.None, "None");
		writeBackTypeNames.put(WriteBackType.CheckBox, "CheckBox");
		writeBackTypeNames.put(WriteBackType.TextField, "TextField");
	}
	
	public AdhocColumn() {
		
	}
	
	public AdhocColumn(String measureName)
	{
		this.Type = ColumnType.Measure;
		isMeasureExpression = Boolean.TRUE;
		this.Column = measureName;
		this.Name = measureName;
		this.OriginalColumn = measureName;
		Repository rep = ServerContext.getRepository();
		if (rep != null) {
			MeasureColumn mc = rep.findMeasureColumn(measureName);
			if (mc != null)
			{
				this.Format = mc.Format;
			}
			else {
				// its an expression
				try {
					this.expressionString = this.Column;
					this.Type = ColumnType.Expression;
					DisplayExpression de = new DisplayExpression(rep, this.expressionString, null, false); //isOlapDimension());
					de.setName(this.Name);
					this.Expression = de;
					this.Column = this.Name;
					if (!this.isOlapDimension())
						this.Dimension = "EXPR";
				}
				catch (Exception e) {
					logger.warn("Error adding expression " + this.Column + " to Column Selector", e);
				}
			}
		}
		init();
	}

	public AdhocColumn(String dimension, String column)
	{
		this.Type = ColumnType.Dimension;
		this.Dimension = dimension;
		isMeasureExpression = Boolean.FALSE;
		this.Column = column;
		this.OriginalColumn = column;
		this.Name = column;
		Repository rep = ServerContext.getRepository();
		if (rep != null) {
			DimensionColumn dc = rep.findDimensionColumn(dimension, column);
			if (dc != null)
			{
				this.Format = dc.Format;
				initDrillColumns(rep, dc);
			}
		}
		init();
	}
	
	public AdhocColumn(String dimension, String hierarchy, String column, String techName, String datatype, String format) {
		this.Type = ColumnType.Dimension;
		this.Dimension = dimension;
		this.hierarchy = hierarchy;
		isMeasureExpression = Boolean.FALSE;
		this.Column = column;
		this.techName = techName;
		this.OriginalColumn = column;
		this.Name = column;
		this.DataType = datatype;
		this.Format = format;
		init();
	}
	
	public void initDrillColumns(Repository rep, DimensionColumn dc) {
		String dim = this.getDimension();
		if (rep == null || dim == null || !Util.hasNonWhiteSpaceCharacters(dim))
			return;
		
		if (dc == null) {
			dc = rep.findDimensionColumn(dim, this.getColumn());
		}
		
		if (dc == null) {
			return;
		}
		
		this.drillToColumns = new ArrayList<DrillColumnList>();
				
	}
	
	public void setExpression(String expression, String name) throws Exception {
		this.OriginalColumn = name;
		this.Name = name;
		this.setType(ColumnType.Expression);
		DisplayExpression de = new DisplayExpression(ServerContext.getRepository(), expression, null);			
		this.setExpression(de);
		
		String fname = this.getFieldName();
		if (fname != null && this.Expression != null) {
			this.Expression.setName(fname);
		}
		if (!isOlapDimension())
			this.Dimension = "EXPR";
		this.Column = this.OriginalColumn + this.reportIndex;
	}
	
	public void setDenominatorExpression(String expression) throws Exception {
		DisplayExpression de;
		de = new DisplayExpression(ServerContext.getRepository(), expression, null);
		denominatorInRatio = de;
	}
	
	public String getDefaultPromptName() {
		return getInternalName();
	}

	public void parseElement(OMElement parent) throws Exception {
		super.parseElement(parent);
		postParseElement();
	}
	public void parseElement(XMLStreamReader parent) throws Exception {
		super.parseElement(parent);
		postParseElement();
	}
	
	private void postParseElement() throws Exception {
		Session session = null;
		com.successmetricsinc.UserBean user = SMIWebUserBean.getUserBean();
		if (user != null)
			session = user.getRepSession();
		if (session == null)
			session = Session.getCurrentThreadSession();
		Repository r = ServerContext.getRepository(); // to handle email notification
		if (this.expressionString != null && (isExpression(expressionString) || Type == ColumnType.Ratio)) {
			try {
				DisplayExpression de = new DisplayExpression(r, expressionString, session, false); //this.isOlapDimension());
				boolean isMacro = expressionString.toUpperCase().contains("<EVAL!");
				if (de.isColumn() && Type != ColumnType.Ratio && !isMacro) {
					// not an expression
					String dim = de.getColumnDimension();
					String col = de.getColumnName();
					this.Column = col;
					if (dim != null && Util.hasNonWhiteSpaceCharacters(dim)) {
						if (de.containsOlapMemberSet() && dim.indexOf('-') > 0) {
							String[] parts = dim.split("-");
							if (parts.length > 1) {
								this.Dimension = parts[0];
								this.hierarchy = parts[1];
							}
							else {
								this.Dimension = parts[0];
							}
						}
						else {
							this.Dimension = dim;
						}
						setType(ColumnType.Dimension);
					}
					else {
						setType(ColumnType.Measure);
						this.Dimension = null;
					}
				}
				else {
					this.setType(ColumnType.Expression);
					this.setExpression(de);
					if (isMeasureExpression == null) {
						isMeasureExpression = Expression.hasMeasuresExpressionColumns();
					}
					String name = this.getFieldName();
					if (name != null && this.Expression != null) {
						this.Expression.setName(name);
					}
					if (!isOlapDimension()) {
						this.Dimension = "EXPR";
						this.Column = this.OriginalColumn + this.reportIndex;
					}
				}
			}
			catch (Exception e) {
				logger.error("Error parsing expression " + expressionString);
			}
		}
		
		if (denominatorExpressionString != null && Util.hasNonWhiteSpaceCharacters(denominatorExpressionString)) {
			this.Type = ColumnType.Ratio;
			setDenominatorExpression(denominatorExpressionString);
		}
		
		if (Type == ColumnType.Measure || (Type == ColumnType.Expression && isMeasureExpression.booleanValue()) || Type == ColumnType.Ratio) {
			// can not pivot on measures
			setPivotColumn(false);
			setPivotTotal(false);
		}
		
		if (this.version < CURRENT_VERSION) {
			if (this.version == 0) {
				this.initDrillColumns(ServerContext.getRepository(), null);
				this.version = 1;
			}
		}
	}
	
	private boolean isExpression(String expression) {
		expression = expression.trim();
		String defaultExpression = AdhocColumn.getDefaultColumnExpression(this, ServerContext.getRepository().isUseNewQueryLanguage());
		if (expression.equals(defaultExpression)) {
			if (this.isOlapDimension())
				this.Type = ColumnType.Dimension;
			return false;
		}
		return true;
	}
	
	public boolean parseAttribute(XMLStreamReader el, String elName) throws Exception {
		if (NAME.equals(elName)) {
			this.setName(XmlUtils.getStringContent(el));
			return true;
		}
		else if (TECH_NAME.equals(elName)) {
			this.setTechName(XmlUtils.getStringContent(el));
			return true;
		}
		else if (VERSION.equals(elName)) {
			this.version = XmlUtils.getIntContent(el);
			return true;
		}
		else if (DIMENSION.equals(elName)) {
			this.setDimension(XmlUtils.getStringContent(el));
			return true;
		}
		else if (HIERARCHY.equals(elName)) {
			this.hierarchy = XmlUtils.getStringContent(el);
			return true;
		}
		else if (ADDITIONAL_OLAP_COLUMNS.equals(elName)) {
			additionalOlapColumns = OlapColumn.parseOlapColumns(el);
		}
		else if (COLUMN.equals(elName)) {
			this.setColumn(XmlUtils.getStringContent(el));
			return true;
		}
		else if (ORIGINAL_COLUMN.equals(elName)) {
			this.setOriginalColumn(XmlUtils.getStringContent(el));
			return true;
		}
		else if (FORMAT.equals(elName)) {
			this.setFormat(XmlUtils.getStringContent(el));
			return true;
		}
		else if (SOURCE_FORMAT.equals(elName)) {
			this.sourceFormat = XmlUtils.getStringContent(el);
		}
		else if (DRILL_TO.equals(elName)) {
			this.setDrillTo(XmlUtils.getStringContent(el));
			return true;
		}
		else if (DRILL_TO_DASH.equals(elName)) {
			drillToDash = XmlUtils.getStringContent(el);
			return true;
		}
		else if (DRILL_TO_DASH_PAGE.equals(elName)) {
			drillToDashPage = XmlUtils.getStringContent(el);
			return true;
		}
		else if (DRILL_TYPE.equals(elName)) {
			String s = XmlUtils.getStringContent(el);
			// convert from old names to new ones
			if ("Navigate".equals(s)) {
				s = Chart.DRILL_TYPE_NAVIGATE;
			}
			else if ("Drill Down".equals(s)) {
				s = Chart.DRILL_TYPE_DRILL;
			}
			this.setDrillType(s);
			return true;
		}
		else if (DRILL_TO_PARAM_NAME.equals(elName)) {
			drillToParameterName = XmlUtils.getStringContent(el);
			return true;
		}
		else if (FILTER_ON_DRILL.equals(elName)) {
			this.filterOnDrill = XmlUtils.getBooleanContent(el);
			return true;
		}
		else if (REMOVE_COLUMN_ON_DRILL.equals(elName)) {
			this.removeColumnOnDrill = XmlUtils.getBooleanContent(el);
			return true;
		}
		else if (PIVOT_COLUMN.equals(elName)) {
			this.setPivotColumn(XmlUtils.getBooleanContent(el));
			return true;
		}
		else if (PIVOT_TOTAL.equals(elName)) {
			this.setPivotTotal(XmlUtils.getBooleanContent(el));
			return true;
		}
		else if (DISPLAY_EXPRESSION.equals(elName)) {
			expressionString = XmlUtils.getStringContent(el);
			return true;
		}
		else if (DENOMINATOR_EXPRESSION.equals(elName)) {
			denominatorExpressionString = XmlUtils.getStringContent(el);
			return true;
		}
		else if (FILTER_LIST.equals(elName)) {
			parseFilters(el);
			return true;
		}
		else if (CONDITIONAL_FORMATS.equals(elName)) {
			parseConditionalFormats(el);
			return true;
		}
		else if (COLUMN_SELECTORS.equals(elName)) {
			parseColumnSelectors(el);
			return true;
		}
		else if (ADD_DRILLED_COLUMNS_TO_NAVIGATE.equals(elName)) {
			addDrilledColumnsToNavigate = XmlUtils.getBooleanContent(el);
			return true;
		}
		else if (SHOW_REPEATING_VALUES.equals(elName)) {
			showRepeatingValues = XmlUtils.getBooleanContent(el);
			return true;
		}
		else if (DRILL_DOWN_COLUMNS.equals(elName)) {
			parseDrillDownColumns(el);
			return true;
		}
		else if (DRILL_PARAMS.equals(elName)) {
			parseDrillParams(el);
			return true;
		}
		else if (LABEL_NAME.equals(elName)) {
			this.labelLabel = XmlUtils.getStringContent(el);
			return true;
		}
		else if (COLUMN_TYPE.equals(elName)) {
			String s = XmlUtils.getStringContent(el);
			if (s != null) {
				for (Map.Entry<ColumnType,String> entry : columnTypeNames.entrySet()) {
					if (entry.getValue().equals(s)) {
						try {
							this.setType(entry.getKey());
						} catch (Exception e) {
							logger.error(e, e);
						}
						break;
					}
				}
			}
			return true;
		}
		else if (EXPRESSION_AGG_RULE.equals(elName)) {
			this.exprAggRule = XmlUtils.getStringContent(el);
			return true;
		}
		else if ("CustomDrillParams".equals(elName)) {
			// do this to skip to next item in stream
			XmlUtils.getStringContent(el);
			// do nothing, but don't warn about this
			// handled in DRILL_PARAMS
			return true;
		}
		else if (DATATYPE.equals(elName)) {
			// do this to skip to next item in stream
			this.DataType = XmlUtils.getStringContent(el);
			// do nothing, but don't warn about this
			return true;
		}
		else if (DATETIME_FORMAT.equals(elName)) {
			// do this to skip to next item in stream
			XmlUtils.getStringContent(el);
			// do nothing, but don't warn about this
			return true;
		}
		else if (ADDITIONAL_DRILL_THRU_COLUMNS.equals(elName)) {
			this.additionalDrillThuColumns = AdditionalDrillThruColumn.parseAdditionalDrillThruColumn(el);
			return true;
		}
		else if (ADDITIONAL_DRILL_THRU_STRING.equals(elName)) {
			additionalDrillThruString = XmlUtils.getStringContent(el);
			return true;
		}
		else if (IS_MEASURE_EXPRESSION.equals(elName)) {
			isMeasureExpression = XmlUtils.getBooleanContent(el);
			return true;
		}
		else if (EXPRESSION_CLASS.equals(elName)) {
			// do this to skip to next item in stream
			XmlUtils.getStringContent(el);
			// ignore these as they will be set by the expression
			return true;
		}
		else if (PERCENT_OF_COLUMN_VALUE.equals(elName)) {
			pivotValuePctOfCol = XmlUtils.getIntContent(el);
			return true;
		}
		else if (IS_LEAF_CONDITIONAL_FORMAT_CREATED.equals(elName)) {
			isLeafConditionalFormatCreated = XmlUtils.getBooleanContent(el);
			return true;
		}
		else if (VISUALIZATION_PANEL.equals(elName)) {
			visualizationPanel = XmlUtils.getStringContent(el);
			return true;
		}
		else if (WRITE_BACK_TYPE.equals(elName)) {
			String s = XmlUtils.getStringContent(el);
			if (s != null) {
				for (Map.Entry<WriteBackType,String> entry : writeBackTypeNames.entrySet()) {
					if (entry.getValue().equals(s)) {
						try {
							writeBackType = (entry.getKey());
						} catch (Exception e) {
							logger.error(e, e);
						}
						break;
					}
				}
			}
			return true;
		}
		return super.parseAttribute(el, elName);
	}

	public boolean parseAttribute(OMElement el, String elName) {
		if (NAME.equals(elName)) {
			this.setName(XmlUtils.getStringContent(el));
			return true;
		}
		else if (TECH_NAME.equals(elName)) {
			this.setTechName(XmlUtils.getStringContent(el));
			return true;
		}
		else if (VERSION.equals(elName)) {
			this.version = XmlUtils.getIntContent(el);
			return true;
		}
		else if (DIMENSION.equals(elName)) {
			this.setDimension(XmlUtils.getStringContent(el));
			return true;
		}
		else if (HIERARCHY.equals(elName)) {
			this.hierarchy = XmlUtils.getStringContent(el);
			return true;
		}
		else if (ADDITIONAL_OLAP_COLUMNS.equals(elName)) {
			additionalOlapColumns = OlapColumn.parseOlapColumns(el);
		}
		else if (COLUMN.equals(elName)) {
			this.setColumn(XmlUtils.getStringContent(el));
			return true;
		}
		else if (ORIGINAL_COLUMN.equals(elName)) {
			this.setOriginalColumn(XmlUtils.getStringContent(el));
			return true;
		}
		else if (FORMAT.equals(elName)) {
			this.setFormat(XmlUtils.getStringContent(el));
			return true;
		}
		else if (SOURCE_FORMAT.equals(elName)) {
			this.sourceFormat = XmlUtils.getStringContent(el);
		}
		else if (DRILL_TO.equals(elName)) {
			this.setDrillTo(XmlUtils.getStringContent(el));
			return true;
		}
		else if (DRILL_TO_DASH.equals(elName)) {
			drillToDash = XmlUtils.getStringContent(el);
			return true;
		}
		else if (DRILL_TO_DASH_PAGE.equals(elName)) {
			drillToDashPage = XmlUtils.getStringContent(el);
			return true;
		}
		else if (DRILL_TYPE.equals(elName)) {
			String s = XmlUtils.getStringContent(el);
			// convert from old names to new ones
			if ("Navigate".equals(s)) {
				s = Chart.DRILL_TYPE_NAVIGATE;
			}
			else if ("Drill Down".equals(s)) {
				s = Chart.DRILL_TYPE_DRILL;
			}
			this.setDrillType(s);
			return true;
		}
		else if (DRILL_TO_PARAM_NAME.equals(elName)) {
			drillToParameterName = XmlUtils.getStringContent(el);
			return true;
		}
		else if (FILTER_ON_DRILL.equals(elName)) {
			this.filterOnDrill = XmlUtils.getBooleanContent(el);
			return true;
		}
		else if (REMOVE_COLUMN_ON_DRILL.equals(elName)) {
			this.removeColumnOnDrill = XmlUtils.getBooleanContent(el);
			return true;
		}
		else if (PIVOT_COLUMN.equals(elName)) {
			this.setPivotColumn(XmlUtils.getBooleanContent(el));
			return true;
		}
		else if (PIVOT_TOTAL.equals(elName)) {
			this.setPivotTotal(XmlUtils.getBooleanContent(el));
			return true;
		}
		else if (DISPLAY_EXPRESSION.equals(elName)) {
			expressionString = XmlUtils.getStringContent(el);
			return true;
		}
		else if (DENOMINATOR_EXPRESSION.equals(elName)) {
			denominatorExpressionString = XmlUtils.getStringContent(el);
			return true;
		}
		else if (FILTER_LIST.equals(elName)) {
			parseFilters(el);
			return true;
		}
		else if (CONDITIONAL_FORMATS.equals(elName)) {
			parseConditionalFormats(el);
			return true;
		}
		else if (COLUMN_SELECTORS.equals(elName)) {
			parseColumnSelectors(el);
			return true;
		}
		else if (ADD_DRILLED_COLUMNS_TO_NAVIGATE.equals(elName)) {
			addDrilledColumnsToNavigate = XmlUtils.getBooleanContent(el);
			return true;
		}
		else if (SHOW_REPEATING_VALUES.equals(elName)) {
			showRepeatingValues = XmlUtils.getBooleanContent(el);
			return true;
		}
		else if (DRILL_DOWN_COLUMNS.equals(elName)) {
			parseDrillDownColumns(el);
			return true;
		}
		else if (DRILL_PARAMS.equals(elName)) {
			parseDrillParams(el);
			return true;
		}
		else if (LABEL_NAME.equals(elName)) {
			this.labelLabel = XmlUtils.getStringContent(el);
			return true;
		}
		else if (COLUMN_TYPE.equals(elName)) {
			String s = XmlUtils.getStringContent(el);
			if (s != null) {
				for (Map.Entry<ColumnType,String> entry : columnTypeNames.entrySet()) {
					if (entry.getValue().equals(s)) {
						try {
							this.setType(entry.getKey());
						} catch (Exception e) {
							logger.error(e, e);
						}
						break;
					}
				}
			}
			return true;
		}
		else if (EXPRESSION_AGG_RULE.equals(elName)) {
			this.exprAggRule = XmlUtils.getStringContent(el);
			return true;
		}
		else if ("CustomDrillParams".equals(elName)) {
			// do nothing, but don't warn about this
			// handled in DRILL_PARAMS
			return true;
		}
		else if (DATATYPE.equals(elName)) {
			// do nothing, but don't warn about this
			this.DataType = XmlUtils.getStringContent(el);
			return true;
		}
		else if (DATETIME_FORMAT.equals(elName)) {
			// do nothing, but don't warn about this
			return true;
		}
		else if (ADDITIONAL_DRILL_THRU_COLUMNS.equals(elName)) {
			this.additionalDrillThuColumns = AdditionalDrillThruColumn.parseAdditionalDrillThruColumn(el);
			return true;
		}
		else if (ADDITIONAL_DRILL_THRU_STRING.equals(elName)) {
			additionalDrillThruString = XmlUtils.getStringContent(el);
			return true;
		}
		else if (IS_MEASURE_EXPRESSION.equals(elName)) {
			isMeasureExpression = XmlUtils.getBooleanContent(el);
			return true;
		}
		else if (EXPRESSION_CLASS.equals(elName)) {
			// ignore these as they will be set by the expression
			return true;
		}
		else if (PERCENT_OF_COLUMN_VALUE.equals(elName)) {
			pivotValuePctOfCol = XmlUtils.getIntContent(el);
			return true;
		}
		else if (IS_LEAF_CONDITIONAL_FORMAT_CREATED.equals(elName)) {
			isLeafConditionalFormatCreated = XmlUtils.getBooleanContent(el);
			return true;
		}
		else if (VISUALIZATION_PANEL.equals(elName)) {
			visualizationPanel = XmlUtils.getStringContent(el);
			return true;
		}
		else if (WRITE_BACK_TYPE.equals(elName)) {
			String s = XmlUtils.getStringContent(el);
			if (s != null) {
				for (Map.Entry<WriteBackType,String> entry : writeBackTypeNames.entrySet()) {
					if (entry.getValue().equals(s)) {
						try {
							writeBackType = (entry.getKey());
						} catch (Exception e) {
							logger.error(e, e);
						}
						break;
					}
				}
			}
			return true;
		}
		return super.parseAttribute(el, elName);
	}
	
	@SuppressWarnings("unchecked")
	protected void parseFilters(OMElement element) {
		this.filters = new ArrayList<QueryFilter>();
		for (Iterator<OMElement> iter = element.getChildElements(); iter.hasNext(); ) {
			QueryFilter filter = new QueryFilter();
			filter.parseElement(iter.next());
			this.filters.add(filter);
		}
	}
	
	protected void parseFilters(XMLStreamReader parser) throws Exception {
		this.filters = new ArrayList<QueryFilter>();
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
			String elName = parser.getLocalName();
			if (QueryFilter.FILTER_NAME.equals(elName)) {
				QueryFilter filter = new QueryFilter();
				filter.parseElement(parser);
				this.filters.add(filter);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	protected void parseConditionalFormats(OMElement element) {
		this.ConditionalFormats = new ArrayList<ConditionalFormat>();
		for (Iterator<OMElement> iter = element.getChildElements(); iter.hasNext(); ) {
			ConditionalFormat format = new ConditionalFormat();
			format.parseElement(iter.next());
			format.setName(getConditionalFormatExpressionName(format));
			this.ConditionalFormats.add(format);
		}
	}
	protected void parseConditionalFormats(XMLStreamReader parser) throws Exception {
		this.ConditionalFormats = new ArrayList<ConditionalFormat>();
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
			String elName = parser.getLocalName();
			if (ConditionalFormat.CONDITIONAL_FORMAT.equals(elName)) {
				ConditionalFormat cf = new ConditionalFormat();
				cf.parseElement(parser);
				cf.setName(getConditionalFormatExpressionName(cf));
				this.ConditionalFormats.add(cf);
			}
		}
	}
	
	protected void parseColumnSelectors(XMLStreamReader parser) throws Exception {
		this.columnSelectors = new ArrayList<ColumnSelector>();
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
			String elName = parser.getLocalName();
			if (ColumnSelector.COLUMN_SELECTOR.equals(elName)) {
				ColumnSelector cs = new ColumnSelector();
				cs.parseElement(parser);
				columnSelectors.add(cs);
			}
		}
		
	}
	protected void parseColumnSelectors(OMElement element) {
		this.columnSelectors = new ArrayList<ColumnSelector>();
		for (@SuppressWarnings("unchecked")
		Iterator<OMElement> iter = element.getChildElements(); iter.hasNext(); ) {
			ColumnSelector cs = new ColumnSelector();
			cs.parseElement(iter.next());
			columnSelectors.add(cs);
		}
	}
	
	protected void parseDrillDownColumns(OMElement element) {
		this.drillToColumns = new ArrayList<DrillColumnList>();
		for (@SuppressWarnings("unchecked")
		Iterator<OMElement> iter = element.getChildElements(); iter.hasNext(); ) {
			DrillColumnList dcl = new DrillColumnList();
			try {
				dcl.parseElement(iter.next());
				drillToColumns.add(dcl);
			}
			catch (Exception e) {
				logger.error(e,e);
			}
		}
	}
	
	protected void parseDrillDownColumns(XMLStreamReader parser) {
		this.drillToColumns = new ArrayList<DrillColumnList>();
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
			DrillColumnList dcl = new DrillColumnList();
			try {
				dcl.parseElement(parser);
				drillToColumns.add(dcl);
			}
			catch (Exception e) {
				logger.error(e,e);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	protected void parseDrillParams(OMElement element) {
		this.drillParamList = new ArrayList<DrillParam>();
		for (Iterator<OMElement> iter = element.getChildElements(); iter.hasNext(); ) {
			DrillParam drillParam = new DrillParam();
			drillParam.parseElement(iter.next());
			this.drillParamList.add(drillParam);
		}
	}
	
	protected void parseDrillParams(XMLStreamReader parser) throws Exception {
		this.drillParamList = new ArrayList<DrillParam>();
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
			iter.next();
			DrillParam drillParam = new DrillParam();
			drillParam.parseElement(parser);
			this.drillParamList.add(drillParam);
		}
	}
	
	public String getColumnType() {
		return columnTypeNames.get(this.getType());
	}
	
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		super.addContent(fac, parent, ns);
		XmlUtils.addContent(fac, parent, COLUMN_TYPE, getColumnType(), ns);
		XmlUtils.addContent(fac, parent, NAME, this.getName(), ns);
		XmlUtils.addContent(fac, parent, TECH_NAME, this.getTechName(), ns);
		XmlUtils.addContent(fac, parent, VERSION, this.version, ns);
		XmlUtils.addContent(fac, parent, DIMENSION, this.getDimension(), ns);
		if (this.hierarchy != null && this.hierarchy.length() > 0)
			XmlUtils.addContent(fac, parent, HIERARCHY, this.hierarchy, ns);
		if (this.additionalOlapColumns != null && additionalOlapColumns.size() > 0) {
			OlapColumn.addContent(fac, parent, additionalOlapColumns, ns);
		}
		XmlUtils.addContent(fac, parent, COLUMN, this.getColumn(), ns);		
		XmlUtils.addContent(fac, parent, ORIGINAL_COLUMN, this.getOriginalColumn(), ns);

		Repository rep = ServerContext.getRepository();

		if (this.getType() == AdhocColumn.ColumnType.Dimension && rep != null)
		{
			List<DimensionColumn> cols = rep.findDimensionColumns(this.getDimension(), this.getColumn());
			if (cols != null) {
				for (DimensionColumn dc : cols)
				{
					if (dc.DataType != null)
					{
						XmlUtils.addContent(fac, parent, DATATYPE, dc.DataType, ns);
						if (dc.DataType.equals("Date") || dc.DataType.equals("DateTime"))
						{
							String dtFormat = this.getDateTimeFormat(dc.DataType, dc.Format);
							if (this.Format == null)
								this.Format = dtFormat;
							XmlUtils.addContent(fac, parent, DATETIME_FORMAT, dtFormat, ns);
						}
						break;
					}
				}
			}		
			else if (isOlapDimension()) {
				if (DataType == null)
					DataType = "Varchar";
				XmlUtils.addContent(fac, parent, DATATYPE, DataType, ns);
			}
		}
		else if (this.getType() == AdhocColumn.ColumnType.Measure && rep != null)
		{
			List<MeasureColumn> cols = rep.findMeasureColumns(this.getColumn());
			if (cols != null) {
				for (MeasureColumn mc : cols)
				{
					String dtype = mc.DataType;
					if (mc.AggregationRule != null && (mc.AggregationRule.equals("COUNT") || mc.AggregationRule.equals("COUNT DISTINCT")))
						dtype = "Integer";					
					if (dtype != null)
					{
						XmlUtils.addContent(fac, parent, DATATYPE, dtype, ns);
						if (dtype.equals("Date") || dtype.equals("DateTime"))
						{
							XmlUtils.addContent(fac, parent, DATETIME_FORMAT, this.getDateTimeFormat(dtype, mc.Format), ns);
						}
						break;
					}
				}
			}
		}
		else if (this.getType() == ColumnType.Expression) {
			String dtype = isMeasureExpression() ? "Float" : "Varchar";
			if (classType != null) {
				XmlUtils.addContent(fac, parent, EXPRESSION_CLASS, classType.getCanonicalName(), ns);
				if (classType.isInstance(Integer.class) || classType.isInstance(Long.class))
					dtype = "Integer";
				else if (classType.isInstance(Date.class))
					dtype = "Date";
				else if (classType.isInstance(DateTime.class))
					dtype = "DateTime";
				else if (classType.isInstance(String.class))
					dtype = "Varchar";
				else if (classType.isInstance(Float.class))
					dtype = "Float";
				XmlUtils.addContent(fac, parent, DATATYPE, dtype, ns);
			}
		}
		XmlUtils.addContent(fac, parent, IS_MEASURE_EXPRESSION, Boolean.toString(isMeasureExpression()), ns);
		XmlUtils.addContent(fac, parent, FORMAT, this.getFormat(), ns);
		if (this.sourceFormat != null && this.sourceFormat.trim().length() > 0)
			XmlUtils.addContent(fac, parent, SOURCE_FORMAT, this.sourceFormat, ns);
		XmlUtils.addContent(fac, parent, PIVOT_COLUMN, Boolean.toString(this.isPivotColumn()), ns);
		XmlUtils.addContent(fac, parent, PIVOT_TOTAL, Boolean.toString(this.isPivotTotal()), ns);
		XmlUtils.addContent(fac, parent, DRILL_TO, this.getDrillTo(), ns);
		XmlUtils.addContent(fac, parent, DRILL_TO_DASH, drillToDash, ns);
		XmlUtils.addContent(fac, parent, DRILL_TO_DASH_PAGE, drillToDashPage, ns);
		XmlUtils.addContent(fac, parent, DRILL_TYPE, this.getDrillType(), ns);
		XmlUtils.addContent(fac, parent, DRILL_TO_PARAM_NAME, this.getDrillToParameterName(), ns);
		XmlUtils.addContent(fac, parent, FILTER_ON_DRILL, this.isFilterOnDrill(), ns);
		XmlUtils.addContent(fac, parent, REMOVE_COLUMN_ON_DRILL, this.removeColumnOnDrill, ns);

		if (this.getExpression() != null && this.getExpression().getExpression() != null) {
			XmlUtils.addContent(fac, parent, DISPLAY_EXPRESSION, this.getExpression().getExpression(), ns);
		}
		else if (this.getExpressionString() != null) { // && this.getExpressionString().toUpperCase().contains(LogicalQueryString.BEGINMACRO)) {
			XmlUtils.addContent(fac, parent, DISPLAY_EXPRESSION, this.getExpressionString(), ns);
		}
		else {
			XmlUtils.addContent(fac, parent, DISPLAY_EXPRESSION, AdhocColumn.getDefaultColumnExpression(this, rep.isUseNewQueryLanguage()), ns);
		}
		if (denominatorInRatio != null && denominatorInRatio.getExpression() != null) {
			XmlUtils.addContent(fac, parent, DENOMINATOR_EXPRESSION, denominatorInRatio.getExpression(), ns);
		}

		if (this.getFilters() != null) {
			OMElement type = fac.createOMElement(FILTER_LIST, ns);
			
			for (QueryFilter filter : this.getFilters()) {
				filter.addContent(fac, type, ns, rep.getServerParameters().getProcessingTimeZone(), UserBean.getUserBean().getDateFormat(this.getFormat()), UserBean.getUserBean().getDateTimeFormat(this.getFormat()));
			}
			parent.addChild(type);
		}
		
		if (this.getConditionalFormats() != null) {
			OMElement type = fac.createOMElement(CONDITIONAL_FORMATS, ns);
			for (ConditionalFormat format : this.getConditionalFormats()) {
				format.addContent(fac, type, ns);
			}
			parent.addChild(type);
		}
		
		XmlUtils.addContent(fac, parent, ADD_DRILLED_COLUMNS_TO_NAVIGATE, addDrilledColumnsToNavigate, ns);
		if (this.columnSelectors != null) {
			OMElement type = fac.createOMElement(COLUMN_SELECTORS, ns);
			for (ColumnSelector cs : this.columnSelectors) {
				cs.addContent(type, fac, ns);
			}
			parent.addChild(type);
		}
		XmlUtils.addContent(fac, parent, SHOW_REPEATING_VALUES, showRepeatingValues, ns);
		
		if (this.drillParamList != null) {
			OMElement type = fac.createOMElement(DRILL_PARAMS, ns);
			for (DrillParam dp : this.drillParamList) {
				dp.addContent(fac, type, ns);
			}
			parent.addChild(type);
		}
		
		if (this.getLabelField() != null) {
			XmlUtils.addContent(fac, parent, LABEL_NAME, this.getLabelField().reportIndex, ns);
		}
		XmlUtils.addContent(fac, parent, EXPRESSION_AGG_RULE, this.exprAggRule, ns);
		XmlUtils.addContent(fac, parent, "CustomDrillParams", this.getDrillParams(), ns);
		if (this.additionalDrillThuColumns != null && additionalDrillThuColumns.size() > 0) {
			AdditionalDrillThruColumn.addContent(fac, parent, this.additionalDrillThuColumns, ns);
		}
		if (additionalDrillThruString != null && additionalDrillThruString.length() > 0) 
			XmlUtils.addContent(fac, parent, ADDITIONAL_DRILL_THRU_STRING, additionalDrillThruString, ns);
		
		if (this.drillToColumns != null && drillToColumns.size() > 0) {
			OMElement drillTo = fac.createOMElement(DRILL_DOWN_COLUMNS, ns);
			for (DrillColumnList dcl : this.drillToColumns) {
				OMElement drillDownCol = fac.createOMElement(DRILL_DOWN_COLUMN, ns);
				dcl.addContent(fac, drillDownCol, ns);
				drillTo.addChild(drillDownCol);
			}
			parent.addChild(drillTo);
		}
		XmlUtils.addContent(fac, parent,PERCENT_OF_COLUMN_VALUE, pivotValuePctOfCol, ns);
		XmlUtils.addContent(fac, parent, IS_LEAF_CONDITIONAL_FORMAT_CREATED, isLeafConditionalFormatCreated, ns);
		XmlUtils.addContent(fac, parent, VISUALIZATION_PANEL, getVisualizationPanel(), ns);
		XmlUtils.addContent(fac, parent, WRITE_BACK_TYPE, writeBackTypeNames.get(writeBackType), ns);
	}
	
	private void init() {
		Align align = Align.Left;
		if (treatAsMeasure())
		{
			align = Align.Right;
		}
		this.setAlignment(align);
		if (this.isNumeric()) {
			Integer numberWidth = PageInfo.getNumberWidth();
			Repository rep = ServerContext.getRepository();
			if (rep != null) {
				numberWidth = rep.getDefaultAdhocNumberWidth();
				if (numberWidth == null) {
					numberWidth = PageInfo.getNumberWidth();
				}
			}
			this.setWidth(numberWidth); 
		}
		this.version = CURRENT_VERSION;
	}
	
	public boolean equals(Object obj)
	{
		if (obj == null || !AdhocColumn.class.isInstance(obj))
			return false;
		AdhocColumn rc = (AdhocColumn) obj;
		if (rc.Type != Type)
			return (false);
		if (reportIndex >= 0) {
			if (rc.getReportIndex() == reportIndex)
				return true;
			return false;
		}
		
		if (Type == ColumnType.Expression || Type == ColumnType.Ratio)
		{
			return rc.Name.equals(Name);
		}
		if (Type == ColumnType.Dimension)
		{
			return rc.Column.equals(Column) && rc.Dimension.equals(Dimension);
		}
		if (Type == ColumnType.Measure)
		{
			boolean sameMeasure = rc.Column.equals(Column);
			boolean sameFilter = (rc.filters == null && filters == null) || (rc.filters != null && filters != null && rc.filters.equals(filters));
			return sameMeasure && sameFilter;
		}
		return false;
	}
	
	public DateFormat getColumnDateFormat()
	{
		return _columnDateFormat;
	}
	
	public void setColumnDateFormat(DateFormat newval)
	{
		_columnDateFormat = newval;
	}
	
	/**
	 * @param filter
	 *            Adds a filter to the filter list.
	 */
	public void addFilter(QueryFilter filter)
	{
		if (filters == null)
		{
			Name = filter.getMeasureFilterName(Column);
			filters = new ArrayList<QueryFilter>();
		} else
			Name = Name + "," + filter.getString(false);
		filters.add(filter);
		Column = Name;
	}

	/**
	 * @param filter
	 *            Adds a filter to the filter list.
	 */
	public void addFilters(List<QueryFilter> flist)
	{
		for (QueryFilter qf : flist)
			addFilter(qf);
	}
	
	/**
	 * Return the result set name of a conditional format expression
	 * 
	 * @param cf
	 *            ConditionalFormat to name
	 * 
	 * @return
	 */
	public String getConditionalFormatExpressionName(ConditionalFormat cf)
	{
		return ("CFEXPR_" + Util.replaceWithPhysicalString(Column) + cf.getID());
	}
	
	@SuppressWarnings("unchecked")
	public static Class getClassFromTypes(String sType) {
		
		int type = QueryResultSet.getTypeForDataTypeString(sType);
		switch (type)
		{
		case Types.BIGINT:
		case Types.INTEGER:
		case Types.TINYINT:
		case Types.SMALLINT:
		case -7: // Tinyint in MYSQL
			return Long.class;
		case Types.DOUBLE:
		case Types.FLOAT:
		case Types.DECIMAL:
		case Types.NUMERIC:
		case Types.REAL:
			return Double.class;
		case -8:
		case Types.CHAR:
		case Types.NCHAR:
		case Types.NVARCHAR:
		case Types.VARCHAR:
		case Types.LONGVARCHAR:
		case Types.LONGNVARCHAR:
			return String.class;
		case Types.BOOLEAN:
			return Boolean.class;
		case Types.DATE:
		case Types.TIME:
		case Types.TIMESTAMP:
		case Types.VARBINARY:
			return GregorianCalendar.class;
		case 0:
		default:
			logger.warn("query returned unknown data type (might be [N]VARCHAR(MAX): " + type);
			// guessing...
			break;
		}
		return null;
	}

	public boolean isDateTime(Repository r) {		
		if (getType() == ColumnType.Dimension) {
			List<DimensionColumn> dimCols = r.findDimensionColumns(this.Dimension, this.Column);
			if (dimCols != null && dimCols.size() > 0) {
				if ("datetime".equalsIgnoreCase(dimCols.get(0).DataType))
					return true;
			}
		}
		else if (getType() == ColumnType.Measure) {
			MeasureColumn mc = r.findMeasureColumn(this.Column);
			if ("datetime".equalsIgnoreCase(mc.DataType))
				return true;
		}
		return false;
	}
	
	@SuppressWarnings("unchecked")
	public Class getFieldClass(Repository r, Map<String, ColumnSelector> selectors) {
		Class fieldClass = null;
		// ratios are always doubles - numerator/denominator
		if (getType() == ColumnType.Ratio)
			return RATIO_CLASS;
		
		String key = getLabelString();
		ColumnSelector columnSelector = null;
		if (selectors != null && key != null) {
			columnSelector = selectors.get(key);
		}
		if (columnSelector != null) {
			return columnSelector.getAdhocColumn().getFieldClass(r, null);
		}
		
		if (getType() == ColumnType.Expression && getExpression() != null) {
			fieldClass = getExpression().getCompiledClass();
		}
		else if (getType() == ColumnType.Dimension) {
			List<DimensionColumn> dimCols = r.findDimensionColumns(this.Dimension, this.Column);
			if (dimCols != null && dimCols.size() > 0)
				fieldClass = getClassFromTypes(dimCols.get(0).DataType);
		}
		else if (getType() == ColumnType.Measure) {
			MeasureColumn mc = r.findMeasureColumn(this.Column);
			if (mc != null)
				fieldClass = getClassFromTypes(mc.LogicalDataType);
		}
		
		if (fieldClass == null)
		{
			if (treatAsMeasure())
				fieldClass = Double.class;
			else
				fieldClass = String.class;
		}
		
		if (isOlapDimension()) {
			return BirstOlapField.class;
		}
		if (Format != null && Format.toUpperCase().startsWith(ToTime.TO_TIME_FORMAT_PREFIX) && (Number.class.isAssignableFrom(fieldClass))) {
			return BirstInterval.class;
		}
		return fieldClass;
	}

	/* (non-Javadoc)
	 * @see com.successmetricsinc.adhoc.AdhocEntity#getElement()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public JRDesignElement getElement(AdhocReport report, Repository r,
									  JasperDesign jasperDesign,
									  PageInfo pi,
									  boolean isPdf,
									  Map<String, ColumnSelector> selectors) 
	{
		if (isShowInReport() == false)
			return null;
		
		JRDesignStyle checkBoxStyle = null;
		if (writeBackType != null && writeBackType.equals(WriteBackType.CheckBox))
		{
			String cbStyleName = null;
			for (AdhocEntity entity : report.getEntities()) {
				if (entity instanceof AdhocButton && ((AdhocButton)entity).isWriteBack()) {
					Integer writeBackField = ((AdhocButton)entity).getWriteBackField();
					if (writeBackField != null && writeBackField == this.reportIndex) {
						cbStyleName = "CHECKBOX_" + this.getName();
					}
				}
			}
			if (cbStyleName != null)
			{
				try
				{
					boolean found = false;
					for (JRStyle style : jasperDesign.getStyles())
					{
						if (style.getName().equals(cbStyleName))
						{
							found = true;
							checkBoxStyle = (JRDesignStyle) style;
							break;
						}
					}
					if (!found)
					{
						checkBoxStyle = new JRDesignStyle();
						checkBoxStyle.setName(cbStyleName);
						jasperDesign.addStyle(checkBoxStyle);
					}
				} 
				catch (JRException e) {
					logger.error(e,e);
				}
			}
		}
		else if (writeBackType != null && writeBackType.equals(WriteBackType.TextField)) {
			String cbStyleName = null;
			for (AdhocEntity entity : report.getEntities()) {
				if (entity instanceof AdhocButton && ((AdhocButton)entity).isWriteBack()) {
					Integer writeBackField = ((AdhocButton)entity).getWriteBackField();
					if (writeBackField != null && writeBackField == this.reportIndex) {
						cbStyleName = "TEXTBOX_" + this.getName();
					}
				}
			}
			if (cbStyleName != null)
			{
				try
				{
					boolean found = false;
					for (JRStyle style : jasperDesign.getStyles())
					{
						if (style.getName().equals(cbStyleName))
						{
							found = true;
							checkBoxStyle = (JRDesignStyle) style;
							break;
						}
					}
					if (!found)
					{
						checkBoxStyle = new JRDesignStyle();
						checkBoxStyle.setName(cbStyleName);
						jasperDesign.addStyle(checkBoxStyle);
					}
				} 
				catch (JRException e) {
					logger.error(e,e);
				}
			}
		}

		/*
		 * Set the class of the field. Use string or double as default (e.g. when hiding data and there is no result
		 * set), or get from the result set.
		 */
		@SuppressWarnings("rawtypes")
		Class fieldClass = getFieldClass(r, selectors);
		String fieldName = getFieldName();
		JRDesignTextField textField = new JRDesignTextField();
		if (checkBoxStyle != null)
			textField.setStyle(checkBoxStyle);
		
		if (renderHTML)
			setHTMLStyle(report, jasperDesign, textField);

		/*
		 *  if setNull_Zero is true, jasper does not attempt to show blank cells for null values.
		 *  Need to alter this behaviour only for columns with measures or expressions, not for dimension columns.  
		 */
		this.setDefaultEntityProperties(textField, report.isShowNullZero() 
				&& this.getType() != ColumnType.Dimension && 
				fieldClass.isAssignableFrom(Number.class));

		boolean isToTimeFormat = false;
		String toTimeFields = "";
		String toTimeFormat = "";
		String format = getValidFormat();
		if (format != null)
		{
			if (format.toUpperCase().startsWith(ToTime.TO_TIME_FORMAT_PREFIX))
			{
				String formatArgs[]=(format.substring(format.indexOf("(") + 1, format.lastIndexOf(")"))).split("','");
				toTimeFields = formatArgs[0].substring(formatArgs[0].indexOf("'") + 1);
				toTimeFormat = formatArgs[1].substring(0, formatArgs[1].indexOf("'"));
				fieldClass = BirstInterval.class;
				isToTimeFormat = true;
			}
//			else
			{
				textField.setPattern(format);
			}
		}
		
		
		JRDesignExpression expression = new JRDesignExpression();
		if (pi.isPivot()) {
			expression.addVariableChunk(this.getFieldName());
		}
		else {
			expression.addFieldChunk(this.getFieldName());
		}
		if (fieldClass != null && !isSortablePivotField()) {
			if(fieldClass.equals(GregorianCalendar.class) || (fieldClass.equals(BirstOlapField.class) && this.DataType != null && (this.DataType.equals("Date") || this.DataType.equals("DateTime"))))
			{
				JRPropertiesMap props = textField.getPropertiesMap();
				boolean isDateTime = (fieldClass.equals(BirstOlapField.class) ? this.DataType.equals("DateTime") :isDateTime(r));
				if(isDateTime) {
					expression.setText((isOlapDimension() ? "((java.util.GregorianCalendar)" : "") + "$F{" + fieldName + "}" + (isOlapDimension() ? ".getValue())" : "") + ".getTime()");
					props.setProperty(AdhocReport.JASPERREPORTS_EXPORT_PREFIX + "IsDateTime", "true");
				}
				else { // Need to apply reverse delta first to Dates, so that when jasper applies delta it will not change the date 
					expression.setText("com.successmetricsinc.util.DateUtil.applyReverseDeltaForDate(" + 
							(isOlapDimension() ? "((java.util.GregorianCalendar)" : "") + "$F{" + fieldName + "}" + (isOlapDimension() ? ".getValue())" : "") + ".getTime(), " +
							"com.successmetricsinc.UserBean.getParentThreadUserBean().getRepository().getServerParameters().getProcessingTimeZone(), " +
							"com.successmetricsinc.UserBean.getParentThreadUserBean().getTimeZone())");
					
					props.setProperty(AdhocReport.JASPERREPORTS_EXPORT_PREFIX + "IsDate", "true");
				}
				if (format == null)
					textField.setPattern(DefaultFormatFactory.STANDARD_DATE_FORMAT_SHORT + "," + 
							(isDateTime ? DefaultFormatFactory.STANDARD_DATE_FORMAT_SHORT : DefaultFormatFactory.STANDARD_DATE_FORMAT_HIDE));			
			}
			else if (fieldClass.equals(String.class) && isToTimeFormat)
			{
				expression.setText("com.successmetricsinc.transformation.integer.ToTime.evaluate(" + "$F{" + fieldName + "},\""+toTimeFields+"\",\""+toTimeFormat+"\")");
			}
		}

		textField.setExpression(expression);
		if (report.isHidedata()) {
			 if (Integer.class == fieldClass){
				 expression.setText("Integer.valueOf(0)");
			 }else if(Long.class == fieldClass){
				 expression.setText("Long.valueOf(0)");
			 }else if(Double.class == fieldClass || Number.class == fieldClass){
				 expression.setText("Double.valueOf(0)");
			 }else if(Timestamp.class == fieldClass){
				 expression.setText("new java.sql.Timestamp((new Date()).getTime())");
			 }else if(Boolean.class == fieldClass){
				 expression.setText("Boolean.valueOf(false)");
			 }else if(GregorianCalendar.class == fieldClass)
			 {
				expression.setText("new Date()"); 
			 }
			 else
			 {
				 expression.setText("\"" + Name + "\"");
			}
			return textField;
		}
		
		if (! isPdf ) {
			List<DrillColumnList> list = retrieveAllDrillToColumns(r); 
			if (treatAsDimension() && Chart.DRILL_TYPE_DRILL.equals(this.getDrillType()) && getType() != ColumnType.Expression) {
				if (list != null && list.size() > 0)
				{
					setUpDrillThru(textField, r, report, pi.isPivot(), jasperDesign, selectors, list);
				}
				else if (isOlapDimension()) {
					JRDesignExpression refExp = new JRDesignExpression();
					StringBuffer drillCols = new StringBuffer();
					if ("Date".equals(DataType) || "DateTime".equals(DataType)) {
						if ("DateTime".equals(DataType)) {
							drillCols.append("com.successmetricsinc.util.DateUtil.convertDateTimeToStandardFormat((java.util.GregorianCalendar)$");
						}
						else {
							drillCols.append("com.successmetricsinc.util.DateUtil.convertDateToStandardFormat((java.util.GregorianCalendar)$");
						}
						drillCols.append(pi.isPivot() ? 'V' : 'F');
						drillCols.append("{" + getFieldName() + "}.getValue()");
						if ("DateTime".equals(DataType)) {
							drillCols.append(", TimeZone.getTimeZone(\"GMT\")");
						}
						drillCols.append(")");
					}
					else {
						drillCols.append("URLEncoder.encode(");
						drillCols.append('$');
						drillCols.append(pi.isPivot() ? 'V' : 'F');
						drillCols.append("{");
						drillCols.append(getFieldName());
						drillCols.append("}.toString()");
						drillCols.append(", \"UTF-8\")");
					}
					StringBuffer techName = new StringBuffer("URLEncoder.encode(");
					techName.append('$');
					techName.append(pi.isPivot() ? 'V' : 'F');
					techName.append("{");
					techName.append(getFieldName());
					techName.append("}.getTechName()");
					techName.append(", \"UTF-8\")");
					try {
					refExp.setText("\"AdhocContent.OlapDrillThru('" + URLEncoder.encode(getDefaultPromptName(), "UTF-8") + "=\" + " + drillCols + "+ \"=\" + " + techName + " + \"') \"");
					}
					catch (Exception e) {}

					textField.setHyperlinkReferenceExpression(refExp);
					textField.setHyperlinkTarget(HyperlinkTargetEnum.SELF);
					textField.setLinkType("javascript");
					if (! report.isDoNotStyleHyperlinks()) {
						textField.setUnderline(true);
						textField.setForecolor(Color.BLUE);
					}
				}
			}		
			else if (Chart.DRILL_TYPE_NAVIGATE.equals(this.getDrillType())) {
				setUpDrillThru(textField, r, report, pi.isPivot(), jasperDesign, selectors, null);
			}
			else if (! Chart.DRILL_TYPE_NONE.equals(this.getDrillType()) &&
					! Chart.DRILL_TYPE_DRILL.equals(this.getDrillType())) {
				setUpCustomDrilling(textField, fieldName, report, pi.isPivot(), jasperDesign);
			}
		}
		
		if (this.getBand() != AdhocReport.Band.Detail && treatAsMeasure()) {
			// need to aggregate
			JRDesignVariable jrdv = new JRDesignVariable();
			jrdv.setExpression(textField.getExpression());
			jrdv.setName(fieldName);
			if(fieldClass.equals(GregorianCalendar.class))
				jrdv.setValueClass(Date.class);
			else
				jrdv.setValueClass(fieldClass);
			if (this.getBand() == AdhocReport.Band.GroupHeader || 
					this.getBand() == AdhocReport.Band.GroupFooter) {
				jrdv.setResetType(ResetTypeEnum.GROUP);
				jrdv.setResetGroup(pi.groups[this.getGroupNumber()]);
			}
			
			if (getType() == ColumnType.Ratio) {
				jrdv.setIncrementerFactoryClassName("com.successmetricsinc.query.BirstRatioIncrementerFactory");
			}
			if (isToTimeFormat) {
				jrdv.setIncrementerFactoryClassName("com.successmetricsinc.query.BirstIntervalIncrementorFactory");
			}
			CalculationEnum rule = getRuleFromString(getExprAggRule());
			jrdv.setCalculation(rule);
			try {
				jasperDesign.addVariable(jrdv);
			} catch (JRException e) {
				logger.error(e, e);
			}
			JRDesignExpression ve = new JRDesignExpression();
			ve.setText("$V{" + fieldName + "}");
			textField.setExpression(ve);
			if (this.getBand() == AdhocReport.Band.GroupHeader || 
					this.getBand() == AdhocReport.Band.GroupFooter)
			{
				textField.setEvaluationTime(EvaluationTimeEnum.GROUP);
				textField.setEvaluationGroup(pi.groups[this.getGroupNumber()]);
			} else
				textField.setEvaluationTime(EvaluationTimeEnum.REPORT);
		}
		
		// add Grand Total
		if (treatAsMeasure() && report.isGrandtotals() && !report.isHidedata() && ! report.isPivot && !report.isUseFlexGrid()) {									
			JRDesignTextField totalTextField = new JRDesignTextField();
			this.setDefaultEntityProperties(totalTextField, report.isShowNullZero() 
					&& this.getType() != ColumnType.Dimension && 
					(fieldClass == Double.class || fieldClass == Float.class || fieldClass == Integer.class));
			JRDesignExpression totalTextExpression = new JRDesignExpression();
			JRDesignVariable totalVariable = new JRDesignVariable();
			JRDesignExpression totalVariableExpression = new JRDesignExpression();
			totalVariable.setName("GT" + fieldName);
			totalVariable.setExpression(totalVariableExpression);
			totalVariableExpression.setText("$F{" + fieldName + "}");
			if (getType() == ColumnType.Ratio) {
				totalVariable.setIncrementerFactoryClassName("com.successmetricsinc.query.BirstRatioIncrementerFactory");
			}
			if (isToTimeFormat) {
				totalVariable.setIncrementerFactoryClassName("com.successmetricsinc.query.BirstIntervalIncrementorFactory");
			}
			CalculationEnum rule = getRuleFromString(getExprAggRule());
			totalVariable.setCalculation(rule);			
			totalTextField.setExpression(totalTextExpression);
			
			if (fieldClass.equals(String.class) && isToTimeFormat)
			{
				totalTextExpression.setText("com.successmetricsinc.transformation.integer.ToTime.evaluate(" + "$V{GT" + fieldName + "},\""+toTimeFields+"\",\""+toTimeFormat+"\")");
				//totalTextExpression.setText("$V{GT" + fieldName + "}");
			}
			else
			{
				totalTextExpression.setText("$V{GT" + fieldName + "}");
			}
			if (getType() == ColumnType.Ratio) {
				totalVariable.setValueClass(RATIO_CLASS);
			}
			else {
				@SuppressWarnings("rawtypes")
				Class clazz = getFieldClass(r, selectors);
				totalVariable.setValueClass(clazz);
				if(rule == CalculationEnum.AVERAGE)
					totalVariable.setValueClass(Double.class);
				totalTextField.setExpression(totalTextExpression);
			}
			totalTextField.setEvaluationTime(EvaluationTimeEnum.NOW); //EvaluationTimeEnum.REPORT);
			totalTextField.setY(0);
			((JRDesignElement)totalTextField).getPropertiesMap().setProperty(AdhocReport.JASPERREPORTS_EXPORT_PREFIX + "showNaNblank", Boolean.toString(report.isShowNaNBlank()));
			((JRDesignElement)totalTextField).getPropertiesMap().setProperty(AdhocReport.JASPERREPORTS_EXPORT_PREFIX + "showINFblank", Boolean.toString(report.isShowINFBlank()));
			@SuppressWarnings("rawtypes")
			Class fieldType = this.getFieldClass(r, selectors);
			if (report.isShowNullZero() && this.getType() != ColumnType.Dimension && 
					(fieldType == Double.class || fieldType == Float.class || fieldType == Integer.class || fieldType == RATIO_CLASS))
			{
				((JRDesignElement)totalTextField).getPropertiesMap().setProperty(AdhocReport.JASPERREPORTS_EXPORT_PREFIX + "showNullZero", Boolean.toString(report.isShowNullZero()));
			}
			((JRDesignBand)jasperDesign.getSummary()).addElement(totalTextField);
			try {
				jasperDesign.addVariable(totalVariable);
			} catch (JRException e) {
				logger.error(e, e);
			}
			//totalTextField.setHorizontalAlignment(HorizontalAlignEnum.RIGHT);
			totalTextField.getLineBox().getTopPen().setLineWidth(1f);
			if (format != null) // && !format.toUpperCase().startsWith(ToTime.TO_TIME_FORMAT_PREFIX))
				totalTextField.setPattern(format);
			else if (this.Format != null)
				totalTextField.setPattern(this.Format);
			
		}
		textField.setPrintRepeatedValues(this.showRepeatingValues);
		return textField;
	}
	
	private CalculationEnum getRuleFromString(String aggrule) {
		CalculationEnum rule = CalculationEnum.SUM;
		if (aggrule.equals("MIN"))
			rule = CalculationEnum.LOWEST;
		else if (aggrule.equals("MAX"))
			rule = CalculationEnum.HIGHEST;
		else if (aggrule.equals("AVG"))
			rule = CalculationEnum.AVERAGE;
		else if (aggrule.equals("FIRST")) 
			rule = CalculationEnum.FIRST;
		
		return rule;
	}
	
	public String getDrillThruString(AdhocReport report, Repository r, boolean isPivot, Map<String, ColumnSelector> selectors, List<DrillColumnList> list) {
		if (Chart.DRILL_TYPE_NONE.equals(this.getDrillType()))
			return null;
		
		if ((treatAsDimension() && Chart.DRILL_TYPE_DRILL.equals(this.getDrillType()) && getType() != ColumnType.Expression) ||
				Chart.DRILL_TYPE_NAVIGATE.equals(this.getDrillType())) {
			String goTo = report.getFileName();
			String drillType = this.getDrillType();
			if (Chart.DRILL_TYPE_NAVIGATE.equals(drillType)) {
				goTo = getDrillGoTo();
			}
			
			String drillCols = this.getInternalName();
			
			if(list != null && list.size() > 0)
			{
				drillCols = new Drill(this, list).toString();
			}

			StringBuilder filter = new StringBuilder();
			addFilterString(this, r, filter, isPivot, selectors, this.getDrillToParameterName());
			if (addDrilledColumnsToNavigate) {
				// find all columns that are drilled from here.
				addDrilledColumns(drillToColumns, r, report, filter, isPivot);
			}
			if (additionalDrillThruString != null && additionalDrillThruString.length() > 0) {
				if (filter.length() > 0)
					filter.append('&');
				
				String[] filters = this.additionalDrillThruString.split("&");
				for(int y = 0; y < filters.length; y++){
					String[] addDrills = filters[y].split("=");
					for(int i = 0; i < addDrills.length; i++){
						String arg = addDrills[i];
						try {
							arg = URLEncoder.encode(arg, "UTF-8");
						} catch (UnsupportedEncodingException e) {
							logger.error(e, e);
						}
						filter.append(arg);
						if(i != addDrills.length - 1){
							filter.append("=");
						}
					}
					if(y != filters.length - 1){
						filter.append("&");
					}
				}
			}
			
			return getDrillThruString(goTo, r, drillType, filter, additionalDrillThuColumns, report, drillCols, isPivot, addDrilledColumnsToNavigate); 
		}
		return null;
		
	}
		
	private String getDrillGoTo() {
		if (getDrillToDash() == null || getDrillToDashPage() == null)
			return getDrillTo();
		
		return getDrillToDash() + "','" + getDrillToDashPage();	
	}

	/**
	 * First report level drilling information is fetched
	 * If it is not available then see if drill maps have any
	 * In the end, look for level based drilling
	 * @return
	 */
	public List<DrillColumnList> retrieveAllDrillToColumns(Repository rep)
	{
		// Check for reports first
		if (drillToColumns != null && drillToColumns.size() > 0)
		{
			return drillToColumns;
		}
		
		return getAllDrillColumns(getDimension(), getColumn(), rep, getType() == ColumnType.Dimension);
	}
	
	public static List<DrillColumnList> getAllDrillColumns(String dimension, String column, Repository rep, boolean isDimension) {
		// Check for drill maps
		SMIWebUserBean smiWebUserBean = SMIWebUserBean.getUserBean();
		if(smiWebUserBean != null)
		{
			DrillMapList drillMaps = smiWebUserBean.getDrillMaps();
			if(drillMaps != null)
			{
				List<DrillColumnList> list = drillMaps.getDrillDownColumns(dimension, column);
				if(list != null && list.size() > 0)
				{
					return list;
				}
			}
		}
		
		// Check for level based
		if (rep != null && isDimension) {
			DimensionColumn dc = rep.findDimensionColumn(dimension, column);
			if (dc != null)
			{
				List<DrillColumnList> levelBasedDrillColumns = new ArrayList<DrillColumnList>();
				DrillPath drillPath = new DrillPath(rep, dc);
				if (drillPath != null && drillPath.getLevels() != null && drillPath.getLevels().length > 0) {
					DrillColumnList dcl = new DrillColumnList(drillPath, dc.DimensionTable.DimensionName);
					levelBasedDrillColumns.add(dcl);
					return levelBasedDrillColumns;
				}
			}
		}
		
		return null;
	}
	
	public static String getDrillThruString(String goTo, Repository r, String drillType, StringBuilder filter, 
			List<AdditionalDrillThruColumn> additionalDrillThuColumns, AdhocReport report, String drillCols, boolean isPivot, boolean addDrilledColumnsToNavigate) {
		if (goTo != null)
			goTo = goTo.replace("\\", "/");
		
		if (additionalDrillThuColumns != null && additionalDrillThuColumns.size() > 0) {
			for (AdditionalDrillThruColumn adtc : additionalDrillThuColumns) {
				AdhocEntity entity = report.getEntityById(adtc.column);
				if (entity != null && entity instanceof AdhocColumn) {
					addFilterString((AdhocColumn)entity, r, filter, isPivot, null, adtc.paramName);
					if (addDrilledColumnsToNavigate) {
						// find all columns that are drilled from here.
						addDrilledColumns(((AdhocColumn)entity).getDrillToColumns(), r, report, filter, isPivot);
					}
				}
			}
		}
		
		return "\"AdhocContent.DrillThru('"  + goTo + "', '" + drillType + "', '" + filter + "','" + drillCols + "')\"";
	}

/**
 * 
 * @param textField
 * @param report
 * @param isPivot
 */
	private void setUpDrillThru(JRDesignTextField textField, Repository r, AdhocReport report, boolean isPivot, JasperDesign jDesign, 
			Map<String, ColumnSelector> selectors, List<DrillColumnList> list) {
		String drillThruText = getDrillThruString(report, r, isPivot, selectors, list);
		if (drillThruText == null || drillThruText.trim().length() == 0)
			return;
		
		boolean styleText = true;
		
		// now see if there is a special column defined
		if (this.Dimension != null) {
			Level lvl = r.findDimensionLevelColumn(this.Dimension, this.Column);
			if (lvl != null) {
				String isLeafName = "Birst" + lvl.Name + "IsLeaf";
				DimensionColumn dc = r.findDimensionColumn(this.Dimension, isLeafName);
				if (dc != null) {
					if (! jDesign.getFieldsMap().containsKey(isLeafName)) {
						JRDesignField field = new JRDesignField();
						field.setName(isLeafName);
						field.setValueClass(String.class);
						try {
							jDesign.addField(field);
						} catch (JRException e) {
							logger.error(e,e);
						}
					}
					styleText = false;
					drillThruText = "(\"N\".equals($F{" + isLeafName + "})?" + drillThruText + ":null)";
				}
			}
		}
		
		JRDesignExpression refExp = new JRDesignExpression();
		refExp.setText(drillThruText);

		textField.setHyperlinkReferenceExpression(refExp);
		textField.setHyperlinkTarget(HyperlinkTargetEnum.SELF);
		textField.setLinkType("javascript");
		if (! report.isDoNotStyleHyperlinks() && styleText) {
			textField.setUnderline(true);
			textField.setForecolor(Color.BLUE);
		}
	}
	
	public void setupConditionalFormatForLeafNode(AdhocReport report, Repository r, String isLeafName) {
		if (isLeafConditionalFormatCreated == false && ! report.isDoNotStyleHyperlinks()) {
			isLeafConditionalFormatCreated = true;
			ConditionalFormat cf = new ConditionalFormat();
			cf.setBackgroundColor(getBackground());
			cf.setForegroundColor(Color.BLUE);
			cf.setUnderline(Boolean.TRUE);
			cf.setFont(getFont());
			cf.setID(report.getNextId());
			String name = getConditionalFormatExpressionName(cf);
			cf.setName(name);
			try {
				if (r.isUseNewQueryLanguage())
					cf.setExpression("\'N\' = [" + this.Dimension + "." + isLeafName + "]", name);
				else
					cf.setExpression("\"N\".equals($F{" + name + "})", name);
			}
			catch (Exception e) {}
			addConditionalFormat(cf);
		}
	}
	
	public static void addDrilledColumns(List<DrillColumnList> dclList, Repository r, AdhocReport report, StringBuilder filter,
		boolean useVariable) {
		if (dclList != null && dclList.size() > 0){
			for (DrillColumnList dcl: dclList) {
				for (DrillColumn dc: dcl.getDrillColumns()) {
					for (AdhocColumn ac: report.getColumns()) {
						if (ac.getType() == ColumnType.Dimension && 
								dc.getDimension() != null &&
								dc.getColumn() != null &&
								dc.getDimension().equals(ac.getDimension()) &&
								dc.getColumn().equals(ac.getColumn())) {
							addFilterString(ac, r, filter, useVariable, null, ac.getFilterStringInternalName());
							addDrilledColumns(ac.retrieveAllDrillToColumns(r), r, report, filter, useVariable);
						}
					}
				}
			}
		}
	}

	private static void addFilterString(AdhocColumn adhocColumn, Repository r, StringBuilder filter, boolean useVariable, Map<String, ColumnSelector> selectors, String paramName) {
		if (adhocColumn.isMeasure() || adhocColumn.isMeasureExpression())
			return;
		
		if (filter.length() > 0) {
			filter.append('&');
		}
		
		@SuppressWarnings("unchecked")
		Class clazz = adhocColumn.getFieldClass(r, selectors);
		boolean isCalendar = clazz.equals(GregorianCalendar.class);
		boolean isDate = adhocColumn.isDateTime(r);
		
		String colInternalName = "";
		try {
			colInternalName = URLEncoder.encode(paramName, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error(e, e);
		}
		filter.append(colInternalName);

		if (isCalendar)
		{
			
			filter.append("=\" +");
			if (isDate) {
				filter.append("com.successmetricsinc.util.DateUtil.convertDateTimeToStandardFormat(");
			}
			else {
				filter.append("com.successmetricsinc.util.DateUtil.convertDateToStandardFormat(");
			}
			if (adhocColumn.isSortablePivotField()) {
				filter.append("(Date)(");
			}
			filter.append(useVariable ? "$V" : "$F");
			filter.append("{" + adhocColumn.getFieldName() + "}");
			if (adhocColumn.isSortablePivotField()) {
				filter.append(".getValue())");
			}
			if (isDate) {
				filter.append(", TimeZone.getTimeZone(\"GMT\")");
			}
			
			filter.append(") + \"");
		}
		else
		{
			filter.append("=\" + ");
			if (clazz.equals(String.class)) {
				filter.append("URLEncoder.encode(");
			}
			filter.append('$');
			filter.append(useVariable ? 'V' : 'F');
			filter.append("{");
			filter.append(adhocColumn.getFieldName());
			filter.append("}");
			if (adhocColumn.isSortablePivotField()) {
				filter.append(".toString()");
			}
			if (clazz.equals(String.class)) {
				filter.append(", \"UTF-8\")");
			}
			filter.append("+ \"");
		}		
	}

	private void setUpCustomDrilling(JRDesignTextField textField, String fieldName, AdhocReport report, boolean isPivot, JasperDesign jasperDesign) {
		if (Chart.DRILL_TYPE_NONE.equals(this.getDrillType()))
			return;
		
		String drillType = this.getDrillType();
		
		JRDesignExpression refExp = new JRDesignExpression();
		StringBuilder text = new StringBuilder();
		text.append("\"" + drillType + "('\" ");
		refExp.addTextChunk(text.toString());
		if (this.drillParamList != null && drillParamList.size() > 0) {
			boolean first = true;
			for (DrillParam dp : this.drillParamList) {
				JRDesignField field = new JRDesignField();
				field.setName(dp.getName());
				field.setValueClass(dp.getClazz());
				try {
					jasperDesign.addField(field);
				} catch (JRException e) {
					logger.error(e, e);
				}
				
				refExp.addTextChunk("+");
				if (first == false) {
					refExp.addTextChunk("\"', '\" + ");
				}
				first = false;
				
				refExp.addFieldChunk(dp.getName());
			}
		}
		refExp.addTextChunk("+ \"');\"");
		textField.setHyperlinkReferenceExpression(refExp);
		textField.setHyperlinkTarget(HyperlinkTargetEnum.SELF);
		textField.setLinkType("javascript");
		if (! report.isDoNotStyleHyperlinks()) {
			textField.setUnderline(true);
			textField.setForecolor(Color.BLUE);
		}
	}
	
	public boolean isAllowInChartCategory() {
		return (getType() == ColumnType.Dimension || (getType() == ColumnType.Expression && !isNumeric()));
	}

	public ColumnType getType() {
		return Type;
	}

	/**
	 * @param type
	 *            The type to set.
	 */
	public void setType(ColumnType type) throws Exception
	{
		Repository r = ServerContext.getRepository();
		if (this.Type != ColumnType.Expression && type == ColumnType.Expression)
		{
			String expressionStr = AdhocColumn.getDefaultColumnExpression(this, r.isUseNewQueryLanguage());
			if (expressionStr != null && !expressionStr.isEmpty())
			{
				DisplayExpression de = new DisplayExpression(r, expressionStr, null, false); //isOlapDimension());
				de.setName(this.Name);
				this.Expression = de;
				this.Column = this.Name;
				if (!this.isOlapDimension())
					this.Dimension = "EXPR";
			}
		}
		this.Type = type;
	}

	/**
	 * Add a conditional format to this column
	 * 
	 * @param cf
	 */
	public void addConditionalFormat(ConditionalFormat cf)
	{
		if (ConditionalFormats == null)
			ConditionalFormats = new ArrayList<ConditionalFormat>();
		cf.setName(getConditionalFormatExpressionName(cf));
		ConditionalFormats.add(cf);
	}

	/**
	 * Returns whether this column is numeric
	 * 
	 * @return
	 */
	public boolean isNumeric()
	{
		return (Type == ColumnType.Measure || Type == ColumnType.Ratio || 
				(Type == ColumnType.Expression && Expression != null && Expression.isNumeric()));
	}
	
	public String getName() {
		if (this.getExpression() != null) {
			String name = null;
			if (this.getLabelField() != null) {
				name = this.getLabelField().getLabel();
			}
			if (name == null || !Util.hasNonWhiteSpaceCharacters(name)) {
				name = this.Name;
			}
			else {
				this.Name = name;
			}
		}
		return Name;
	}
	
	public String getTechName() {
		return techName;
	}

	public void setTechName(String techName) {
		this.techName = techName;
	}

	public void setName(String name) {
		Name = name;
		if (Type == ColumnType.Expression)
		{
			if (Expression != null) {
				Expression.setName(name);
			}
			setColumn(name);
		}
	}

	public String getDimension() {
		return Dimension;
	}

	public void setDimension(String dimension) {
		Dimension = dimension;
	}

	public String getColumn() {
		return Column;
	}

	public void setColumn(String column) {
		Column = column;
		if (ConditionalFormats != null)
			for (ConditionalFormat cf : ConditionalFormats)
			{
				cf.setName(getConditionalFormatExpressionName(cf));
			}
	}

	public String getOriginalColumn() {
		return OriginalColumn;
	}

	public void setOriginalColumn(String originalColumn) {
		OriginalColumn = originalColumn;
	}
	
	public String getAdhocColumnFormat() {
		if (Format != null && !Format.isEmpty())
			return Format;
		else if (this.getType() == AdhocColumn.ColumnType.Dimension)
		{
			List<DimensionColumn> cols = ServerContext.getRepository().findDimensionColumns(this.getDimension(), this.getColumn());
			if (cols != null) {
				for (DimensionColumn dc : cols)
				{
					if (dc.DataType != null && (dc.DataType.equals("Date") || dc.DataType.equals("DateTime")))
						return getDateTimeFormat(dc.DataType, dc.Format);
					if (dc.Format != null && !dc.Format.isEmpty())
						return dc.Format;
					break;
				}
			}
		}
		else if (this.getType() == AdhocColumn.ColumnType.Measure)
		{
			List<MeasureColumn> cols = ServerContext.getRepository().findMeasureColumns(this.getColumn());
			if (cols != null) {
				for (MeasureColumn mc : cols)
				{
					if (mc.DataType != null && (mc.DataType.equals("Date") || mc.DataType.equals("DateTime")))
						return getDateTimeFormat(mc.DataType, mc.Format);
					if (mc.Format != null && !mc.Format.isEmpty())
						return mc.Format;
					break;
				}
			}
		}
		return null;
	}

	public String getFormat() {
		return Format;
	}

	public void setFormat(String format) {
		if (format != null && !format.isEmpty() && !format.equalsIgnoreCase("null"))
			Format = format;
		else
			Format = null;
	}
	
	public String getDateTimeFormat()
	{
		Repository rep = ServerContext.getRepository();
		if (this.getType() == AdhocColumn.ColumnType.Dimension)
		{
			List<DimensionColumn> cols = rep.findDimensionColumns(this.getDimension(), this.getColumn());
			if (cols != null) {
				for (DimensionColumn dc : cols)
				{
					if (dc.DataType != null && (dc.DataType.equals("Date") || dc.DataType.equals("DateTime")))
					{
						return getDateTimeFormat(dc.DataType, dc.Format);
					}
					break;
				}
			}
		}
		else if (this.getType() == AdhocColumn.ColumnType.Measure)
		{
			List<MeasureColumn> cols = rep.findMeasureColumns(this.getColumn());
			if (cols != null) {
				for (MeasureColumn mc : cols)
				{
					if (mc.DataType != null && (mc.DataType.equals("Date") || mc.DataType.equals("DateTime")))
					{
						return getDateTimeFormat(mc.DataType, mc.Format);
					}
					break;
				}
			}
		}
		return null;
	}
	
	public String getDateTimeFormat(String dataType, String repFormat) {
		if (Format != null && !Format.isEmpty())
			return Format;
		else if (repFormat != null && !repFormat.isEmpty())
			return repFormat;
		else if (dataType.equals("Date"))
		{
			com.successmetricsinc.UserBean ub1 = SMIWebUserBean.getUserBean();
			if (ub1 != null)
			{
				return ub1.getDateFormat().toPattern();
			} else
			{
				com.successmetricsinc.UserBean ub2 = com.successmetricsinc.UserBean.getParentThreadUserBean();
				if (ub2 != null)
					return ub2.getDateFormat().toPattern();
			}
			//return UserBean.getUserBean().getDateFormat().toPattern();
		}			
		else if (dataType.equals("DateTime"))
		{
			com.successmetricsinc.UserBean ub1 = SMIWebUserBean.getUserBean();
			if (ub1 != null)
			{
				return ub1.getDateTimeFormat().toPattern();
			} else
			{
				com.successmetricsinc.UserBean ub2 = com.successmetricsinc.UserBean.getParentThreadUserBean();
				if (ub2 != null)
					return ub2.getDateTimeFormat().toPattern();
			}			
			//return UserBean.getUserBean().getDateTimeFormat().toPattern();
		}
		return null;
	}

	public DisplayExpression getExpression() {
		return Expression;
	}

	public void setExpression(DisplayExpression expression) {
		Expression = expression;
//		isMeasureExpression = Expression.hasMeasuresExpressionColumns();
		this.classType = Expression.getCompiledClass();
	}

	public List<QueryFilter> getFilters() {
		return filters;
	}

	public void setFilters(List<QueryFilter> filters) {
		this.filters = filters;
	}

	public boolean isPivotColumn() {
		return pivotColumn;
	}

	public void setPivotColumn(boolean pivotColumn) {
		this.pivotColumn = pivotColumn;
	}

	public boolean isPivotTotal() {
		return pivotTotal;
	}

	public void setPivotTotal(boolean pivotTotal) {
		this.pivotTotal = pivotTotal;
	}

	public List<ConditionalFormat> getConditionalFormats() {
		return ConditionalFormats;
	}

	public void setConditionalFormats(List<ConditionalFormat> conditionalFormats) {
		ConditionalFormats = conditionalFormats;
	}

	public List<DisplayExpression> getFormatExpressions()
	{
		List<DisplayExpression> list = null;
		if (ConditionalFormats != null)
		{
			list = new ArrayList<DisplayExpression>();
			for (ConditionalFormat cf : ConditionalFormats)
			{
				DisplayExpression de = cf.getDE();
				if (de != null) {
					// Reset the position so that it can be determined at runtime
					de.setPosition(-1);
					list.add(de);
				}
			}
		}
		if (this.drillParamList != null) {
			if (list == null) {
				list = new ArrayList<DisplayExpression>();
			}
			for (DrillParam dp : drillParamList) {
				DisplayExpression de = dp.getDE();
				if (de != null) {
					de.setPosition(-1);
					list.add(de);
				}
			}
		}
		return (list);
	}

	/**
	 * Return whether this column should be treated as a dimension (i.e., not a numeric expression or is a dimension
	 * column) - and hence is groupable
	 * 
	 * @return
	 */
	public boolean treatAsDimension()
	{
		return (Type == ColumnType.Dimension || (Type == ColumnType.Expression && !isMeasureExpression()));
	}

	/**
	 * Return whether this column should be treated as a measure (i.e. something that is aggregatable)
	 * 
	 * @return
	 */
	public boolean treatAsMeasure()
	{
		return (Type == ColumnType.Measure || Type == ColumnType.Ratio || (Type == ColumnType.Expression && isMeasureExpression()));
	}

	public void addGrandTotal(String fieldName, Repository r, String aggRule, AdhocReport report, JRDesignBand summaryBand,
			JasperDesign jasperDesign, JRDesignTextField designTextField) throws JRException
	{
		JRDesignTextField totalTextField = getTotalTextField(designTextField);
		JRDesignExpression totalTextExpression = new JRDesignExpression(null);
		JRDesignVariable totalVariable = new JRDesignVariable();
		JRDesignExpression totalVariableExpression = new JRDesignExpression();
		totalVariable.setName("GT" + fieldName);
		totalVariable.setExpression(totalVariableExpression);
		totalVariableExpression.setText("$F{" + fieldName + "}");
		if (aggRule.equals("SUM"))
			totalVariable.setCalculation(CalculationEnum.SUM);
		else if (aggRule.equals("AVG"))
			totalVariable.setCalculation(CalculationEnum.AVERAGE);
		else if (aggRule.equals("MAX"))
			totalVariable.setCalculation(CalculationEnum.HIGHEST);
		else if (aggRule.equals("MIN"))
			totalVariable.setCalculation(CalculationEnum.LOWEST);
		else
			totalVariable.setCalculation(CalculationEnum.SUM);
		totalTextField.setExpression(totalTextExpression);
		totalTextExpression.setText("$V{GT" + fieldName + "}");
		@SuppressWarnings("unchecked")
		Class clazz = getFieldClass(r, null);
		totalVariable.setValueClass(clazz);
		
		totalTextField.setEvaluationTime(EvaluationTimeEnum.REPORT);
		totalTextField.setX(this.getLeft());
		totalTextField.setY(0);
		summaryBand.addElement(totalTextField);
		jasperDesign.addVariable(totalVariable);
		totalTextField.setHorizontalAlignment(HorizontalAlignEnum.RIGHT);
		totalTextField.getLineBox().getTopPen().setLineWidth(1f);
	}

	/**
	 * Return a design total text field for the given report column design text field
	 * 
	 * @param designTextField
	 * @return totalTextField
	 */
	public JRDesignTextField getTotalTextField(JRDesignTextField designTextField)
	{
		JRDesignTextField totalTextField = new JRDesignTextField();
		totalTextField.setWidth(designTextField.getWidth());
		totalTextField.setHeight(designTextField.getHeight());
		totalTextField.setMode(designTextField.getModeValue());
		totalTextField.setFontName(designTextField.getFontName());
		totalTextField.setBold(true);
		totalTextField.setFontSize(designTextField.getFontSize());
		totalTextField.setPattern(designTextField.getPattern());
		totalTextField.getLineBox().setLeftPadding(designTextField.getLineBox().getLeftPadding());
		totalTextField.getLineBox().setRightPadding(designTextField.getLineBox().getRightPadding());
		totalTextField.setStretchType(designTextField.getStretchTypeValue());
		return (totalTextField);
	}

	public String getFieldName()
	{
		StringBuilder fieldName = new StringBuilder();
		fieldName.append('F').append(reportIndex); //.append('_');
//		fieldName.append(this.getName());
		
		if (Type == ColumnType.Ratio) {
			fieldName.append(BirstRatio.RATIO);
		}
		else if (isOlapDimension()) {
			fieldName.append(BirstOlapField.OLAP);
		}
		else if (this.Format != null && Format.toUpperCase().startsWith(ToTime.TO_TIME_FORMAT_PREFIX)) {
			fieldName.append(BirstInterval.INTERVAL_SUFFIX);
		}

		return Util.replaceWithPhysicalString(fieldName.toString());
	}

	/**
	 * Return a value class from a given database type
	 * 
	 * @param type
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Class getClassFromType(int type)
	{
		switch (type)
		{
		case Types.TINYINT:
			return (Short.class);
		case Types.INTEGER:
			return (Integer.class);
		case Types.BIGINT:
			return (Long.class);
		case Types.FLOAT:
			return (Float.class);
		case Types.DOUBLE:
			return (Double.class);
		case Types.VARCHAR:
		case Types.NVARCHAR:
		case Types.CHAR:
		case Types.NCHAR:
		case Types.CLOB:
		case Types.NCLOB:
		case Types.LONGVARCHAR:
		case Types.LONGNVARCHAR:
			return (String.class);
		case Types.TIMESTAMP:
			return (GregorianCalendar.class);
		case Types.BOOLEAN:
			return (Boolean.class);
		}
		logger.error("getClassFromType received unknown type: " + type);
		return (null);
	}

	public String getHtmlDisplayLabel() {
		return "c: " + this.getColumn();
	}
	public AdhocTextEntity getLabelField() {
		return label;
	}
	
	public String getLabelString() {
		if (label != null) {
			return label.getLabel();
		}
		return this.getName();
	}

	public void setLabelField(AdhocTextEntity label) {
		this.label = label;
	}

	public String getLabelLabel() {
		return labelLabel;
	}
	
	public String getLabel() {
		return "";
	}
	
	public void setLabel(String s) {
		
	}
	
	public String getDisplayName() {
		if (label == null)
			return getColumn();
		else
			return label.getLabel();
	}

	public static void validateNumberFormat(String columnFormat) throws Exception 
	{
		if (columnFormat != null && !columnFormat.isEmpty())
		{
			if (!columnFormat.toUpperCase().startsWith(ToTime.TO_TIME_FORMAT_PREFIX))
			{
				if (columnFormat.indexOf('\u00a4') >= 0) {
					Locale locale = SMIWebUserBean.getUserBean().getLocale();
					NumberFormat nt = NumberFormat.getCurrencyInstance(locale);
					nt.setCurrency(Currency.getInstance(locale)); // tries it, throws exception if bad
					logger.info("Valid currency format selected from string: " + columnFormat + " instance: " + nt.getCurrency().getSymbol());
				} else
				{
					@SuppressWarnings("unused")
					DecimalFormat ft = new DecimalFormat(columnFormat);
				}
			}
			else
			{
				ToTime.validateColumnFormat(columnFormat);
			}
		}
	}
	
	
/**
 * 
 * @return String
 */
	public String getValidFormat()
	{
		return Format;
	}

	public String getDrillTo() {
		return drillTo;
	}

	public void setDrillTo(String drillTo) {
		this.drillTo = drillTo;
	}

	public String getDrillType() {
		return drillType;
	}

	public void setDrillType(String drillType) {
		this.drillType = drillType;
	}
	
	private String getInternalName() {
		if (isOlapDimension()) {
			return getDimension() + "-" + getHierarchy();
		}
		if (getType() == AdhocColumn.ColumnType.Dimension) {
			return getDimension() + "." + getColumn();
		}
		else if (getType() == AdhocColumn.ColumnType.Measure) {
			return getColumn();
		}
		else if (getType() == AdhocColumn.ColumnType.Expression) {
			if (getLabelField() != null) {
				return getLabelString();
			}
			return "EXPR." + getColumn();
		}
		else if (getType() == ColumnType.Ratio) {
			return "RATIO." + getColumn();
		}
		return "";
	}
	
	private String getFilterStringInternalName() {
		return this.getDrillToParameterName();
		/*
		if (getType() == ColumnType.Expression) {
			DisplayExpression ex = getExpression();
			if(!isMeasureExpression() &&  ex.getCompiledOperator() instanceof SavedExpression){
				return "EXPR." + ex.getExpression();
			}
			AdhocTextEntity label = getLabelField();
			if (label != null)
				return label.getLabel();
			
			return getFieldName();
		}
		
		return getInternalName();
		*/
	}

	public String getExprAggRule() {
		return exprAggRule;
	}
	
	public void setExprAggRule(String rule) {
		this.exprAggRule = rule;
	}

	public String getDrillParams() {
		if (this.drillParamList == null) {
			return null;
		}
		
		StringBuilder builder = new StringBuilder();
		for (DrillParam dp : this.drillParamList) {
			if (builder.length() > 0) {
				builder.append(',');
			}
			builder.append(dp.getName());
		}
		return builder.toString();
	}

	public boolean isFilterOnDrill() {
		return filterOnDrill;
	}

	public List<ColumnSelector> getColumnSelectors() {
		return columnSelectors;
	}

	public void setColumnSelectors(List<ColumnSelector> columnSelectors) {
		this.columnSelectors = columnSelectors;
	}

	public List<AdditionalDrillThruColumn> getAdditionalDrillThuColumns() {
		return additionalDrillThuColumns;
	}

	public void setAdditionalDrillThuColumns(List<AdditionalDrillThruColumn> additionalDrillThuColumns) {
		this.additionalDrillThuColumns = additionalDrillThuColumns;
	}
	
	public String getDrillToParameterName() {
		if (this.drillToParameterName != null)
			return this.drillToParameterName;
		
		return getInternalName();
	}

	public List<DrillColumnList> getDrillToColumns() {
		return drillToColumns;
	}

	public void setDrillToColumns(List<DrillColumnList> drillToColumns) {
		this.drillToColumns = drillToColumns;
	}

	/**
	 * @return the isMeasureExpression
	 */
	public boolean isMeasureExpression() {
		// make sure that it is also numeric or its not really a measure expression.
		if (isMeasureExpression == null) {
			if (this.Type == ColumnType.Dimension)
				isMeasureExpression = Boolean.FALSE;
			else if (this.Type == ColumnType.Measure || Type == ColumnType.Ratio)
				isMeasureExpression = Boolean.TRUE;
			else if (Expression == null){
				isMeasureExpression = Boolean.FALSE;
			}
			else {
				isMeasureExpression = Expression.hasMeasuresExpressionColumns();
			}
		}
		return isMeasureExpression.booleanValue() && isNumeric();
	}

	public void setIsMeasureExpression(Boolean isMeasureExpression) {
		this.isMeasureExpression = isMeasureExpression;
	}

	/* (non-Javadoc)
	 * @see com.successmetricsinc.adhoc.AdhocEntity#copyDisplayAttributes(com.successmetricsinc.adhoc.AdhocEntity)
	 */
	@Override
	public void copyDisplayAttributes(AdhocEntity copyTo) {
		super.copyDisplayAttributes(copyTo);
		if (copyTo instanceof AdhocColumn) {
			((AdhocColumn)copyTo).pivotTotal = pivotTotal;
			((AdhocColumn)copyTo).filterOnDrill = filterOnDrill;
		}
	}

	/**
	 * @return the pivotValuePctOfCol
	 */
	public int getPivotValuePctOfCol() {
		return pivotValuePctOfCol;
	}

	/**
	 * @return the denominatorInRatio
	 */
	public DisplayExpression getDenominatorInRatio() {
		return denominatorInRatio;
	}

	/**
	 * @param denominatorInRatio the denominatorInRatio to set
	 */
	public void setDenominatorInRatio(DisplayExpression denominatorInRatio) {
		this.denominatorInRatio = denominatorInRatio;
	}

	/** 
	 * 
	 * @param column
	 * @return String
	 */
	public static String getDefaultColumnExpression(AdhocColumn column, boolean useNewQueryLanguage)
	{
		if (column.getType() == ColumnType.Dimension || column.isOlapDimension())
		{
			if (useNewQueryLanguage) {
				if (column.isOlapDimension()) {
					StringBuffer buf = new StringBuffer("MDXExpression('[");
					buf.append(column.getDimension());
					buf.append("].[");
					buf.append(column.getHierarchy());
					buf.append("].CurrentMember.Name')");
					return buf.toString(); 
				}
				return '[' + column.getDimension() + '.' + column.getColumn() + ']';
			}
			else
				return "DC{" + column.getDimension() + '.' + column.getColumn() + '}';
		} else if (column.getType() == ColumnType.Measure)
		{
			if (useNewQueryLanguage)
				return '[' + column.getColumn() + ']';
			else
				return "M{" + column.getColumn() + '}';
		}
		return "";
	}

	public boolean isRemoveColumnOnDrill() {
		return removeColumnOnDrill;
	}
	public void setRemoveColumnOnDrill(boolean b) {
		removeColumnOnDrill = b;
	}

	public String getDrillToDash() {
		if (drillToDash == null && drillTo != null) {
			if (drillTo.indexOf('_') > 0) {
				String[] drillToParts = drillTo.split("_");
				drillToDash = drillToParts[0];
				drillToDashPage = drillToParts[1];
			}
		}
		return drillToDash;
	}

	public String getDrillToDashPage() {
		if (drillToDashPage == null && drillTo != null) {
			if (drillTo.indexOf('_') > 0) {
				String[] drillToParts = drillTo.split("_");
				drillToDash = drillToParts[0];
				drillToDashPage = drillToParts[1];
			}
		}
		return drillToDashPage;
	}
	
	public boolean isMeasure() {
		if (getType() == ColumnType.Measure || getType() == ColumnType.Ratio)
			return true;
		
		if (getType() == ColumnType.Dimension)
			return false;
		
		if (getType() == ColumnType.Expression && isMeasureExpression())
			return true;
		
		return false;
	}
	
	public boolean isDimension() {
		return !isMeasure();
	}

	public String getVisualizationPanel() {
		if (visualizationPanel == null) {
			if (isMeasure())
				return VISUALIZATION_PANEL_MEASURE;
			if (isPivotColumn())
				return VISUALIZATION_PANEL_COLUMNS;
			
			return VISUALIZATION_PANEL_ROWS;
		}
		return visualizationPanel;
	}

	public void setVisualizationPanel(String visPanel) {
		if ((visPanel != null && visPanel.trim().length() > 0) &&
				(VISUALIZATION_PANEL_COLUMNS.equals(visPanel) || VISUALIZATION_PANEL_MEASURE.equals(visPanel) || VISUALIZATION_PANEL_ROWS.equals(visPanel)))
			visualizationPanel = visPanel;
	}

	public String getExpressionString() {
		return expressionString;
	}

	public String getAdditionalDrillThruString() {
		return additionalDrillThruString;
	}
	
	public boolean isOlapDimension() {
		return this.hierarchy != null && this.hierarchy.length() > 0;
	}
	
	public String getHierarchy() {
		return this.hierarchy;
	}
	
	public String getDataType() {
		return this.DataType;
	}
	
	public void addAdditionalOlapColumn(String column, String techName) {
		addAdditionalOlapColumn(column, techName, SELECTED);
	}
	
	public void addAdditionalOlapColumn(String column, String techName, String aggregate) {
		List<OlapColumn> columns = getAdditionalOlapColumns();
		boolean found = false;
		for (OlapColumn oc : columns) {
			if (column.equals(oc.columnName) && oc.getExpressionTypeString().equals(aggregate)) {
				found = true;
				break;
			}
		}
		if (!found)
			columns.add(new OlapColumn(column, techName, aggregate));
	}
	
	public List<OlapColumn> getAdditionalOlapColumns() {
		if (additionalOlapColumns == null) {
			additionalOlapColumns = new ArrayList<OlapColumn>();
			additionalOlapColumns.add(new OlapColumn(this.Column, this.techName, SELECTED));
		}
		return additionalOlapColumns;
	}
	
	public List<MemberSelectionExpression> getOlapMemberExpressionMap() {
		return OlapColumn.getOlapMemberExpressionMap(getAdditionalOlapColumns());
	}
	
	
	public static class OlapColumn {
		private static String COLUMN_NAME = "Column";
		private static String COLUMN_TECH_NAME = "TechnicalName";
		private static String EXPRESSION_TYPE = "ExpressionType";
		
		private static Map<String, ExpressionType> expressionTypes;
		
		public enum ExpressionType { Selected, Children, AllChildren, Parent, SelPlusChildren, SelPlusAllChildren, Leaves, SelPlusLeaves, SelPlusParents };
		
		static {
			expressionTypes = new HashMap<String, ExpressionType>();
			expressionTypes.put(SELECTED, ExpressionType.Selected);
			expressionTypes.put(ALLCHILDREN, ExpressionType.AllChildren);
			expressionTypes.put(CHILDREN, ExpressionType.Children);
			expressionTypes.put(LEAVES, ExpressionType.Leaves);
			expressionTypes.put(PARENTS, ExpressionType.Parent);
			expressionTypes.put(SELECTED_PLUS_PARENTS, ExpressionType.SelPlusParents);
			expressionTypes.put(SELECTED_PLUS_CHILDREN, ExpressionType.SelPlusChildren);
			expressionTypes.put(SELECTED_PLUS_ALLCHILDREN, ExpressionType.SelPlusAllChildren);
			expressionTypes.put(SELECTED_PLUS_LEAVES, ExpressionType.SelPlusLeaves);
		}
		private String columnName;
		private String columnTechName;
		private ExpressionType expressionType;
		
		public OlapColumn(String columnName, String techName, String expressionType) {
			this.columnName = columnName;
			this.columnTechName = techName;
			this.expressionType = getExpressionType(expressionType);
		}
		
		private ExpressionType getExpressionType(String type) {
			ExpressionType mType = expressionTypes.get(type);
			if (mType == null)
				return ExpressionType.Selected;
			
			return mType;
		}
		
		private String getExpressionTypeString() {
			for (Map.Entry<String, ExpressionType> entry: expressionTypes.entrySet()) {
				if (entry.getValue() == this.expressionType)
					return entry.getKey();
			}
			
			return SELECTED;
		}
		
		public static void addContent(OMFactory fac, OMElement parent,
				List<OlapColumn> additionalOlapColumns, OMNamespace ns) {
			OMElement addParams = fac.createOMElement(ADDITIONAL_OLAP_COLUMNS, ns);
			for (OlapColumn oc: additionalOlapColumns) {
				OMElement col = fac.createOMElement(ADDITIONAL_OLAP_COLUMN, ns);
				col.addAttribute(COLUMN_NAME, oc.columnName, ns);
				col.addAttribute(COLUMN_TECH_NAME, oc.columnTechName, ns);
				col.addAttribute(EXPRESSION_TYPE, oc.getExpressionTypeString(), ns);
				addParams.addChild(col);
			}
			parent.addChild(addParams);
		}

		public static List<OlapColumn> parseOlapColumns(XMLStreamReader el) throws XMLStreamException {
			ArrayList<OlapColumn> ret = new ArrayList<OlapColumn>();
			for (XMLStreamIterator iter = new XMLStreamIterator(el); iter.hasNext(); ) {
				iter.next();
				int attCount = el.getAttributeCount();
				String name = null;
				String tname = null;
				String expType = null;
				for (int i = 0; i < attCount; i++) {
					if (COLUMN_NAME.equals(el.getAttributeName(i).getLocalPart()))
						name = el.getAttributeValue(i);
					else if (COLUMN_TECH_NAME.equals(el.getAttributeName(i).getLocalPart()))
						tname = el.getAttributeValue(i);
					else if (EXPRESSION_TYPE.equals(el.getAttributeName(i).getLocalPart()))
						expType = el.getAttributeValue(i);
				}
				
				if (name != null && expType != null) {
					OlapColumn oc = new OlapColumn(name, tname, expType);
					ret.add(oc);
				}
			}
			return ret;
		}
		
		public static List<OlapColumn> parseOlapColumns(OMElement el) {
			ArrayList<OlapColumn> ret = new ArrayList<OlapColumn>();
			for (@SuppressWarnings("unchecked")
					Iterator<OMElement> iter = el.getChildElements(); iter.hasNext(); ) {
				OMElement child = iter.next();
				String name = null;
				String tname = null;
				String expType = null;
				
				name = child.getAttributeValue(new QName(COLUMN_NAME));
				tname = child.getAttributeValue(new QName(COLUMN_TECH_NAME));
				expType = child.getAttributeValue(new QName(EXPRESSION_TYPE));
				if (name != null && expType != null) {
					OlapColumn oc = new OlapColumn(name, tname, expType);
					ret.add(oc);
				}
			}
			return ret;
		}
		
		public static List<MemberSelectionExpression> getOlapMemberExpressionMap(List<OlapColumn> olapColumns) {
			List<MemberSelectionExpression> ret = new ArrayList<MemberSelectionExpression>();
			for (OlapColumn oc : olapColumns) {
				OlapColumn.ExpressionType et = oc.expressionType;
				if (et == ExpressionType.Selected || et == ExpressionType.SelPlusAllChildren || et == ExpressionType.SelPlusChildren ||
						et == ExpressionType.SelPlusLeaves || et == ExpressionType.SelPlusParents)
					addOlapMemberExpression(ret, MEMBEREXPRESSIONTYPE.SELECTED, oc.columnTechName);
				if (et == ExpressionType.Children || et == ExpressionType.SelPlusChildren)
					addOlapMemberExpression(ret, MEMBEREXPRESSIONTYPE.CHILDREN, oc.columnTechName);
				if (et == ExpressionType.AllChildren || et == ExpressionType.SelPlusAllChildren)
					addOlapMemberExpression(ret, MEMBEREXPRESSIONTYPE.ALLCHILDREN, oc.columnTechName);
				if (et == ExpressionType.Leaves || et == ExpressionType.SelPlusLeaves)
					addOlapMemberExpression(ret, MEMBEREXPRESSIONTYPE.LEAVES, oc.columnTechName);
				if (et == ExpressionType.Parent || et == ExpressionType.SelPlusParents)
					addOlapMemberExpression(ret, MEMBEREXPRESSIONTYPE.PARENTS, oc.columnTechName);
			}
			return ret;
		}
		
		private static void addOlapMemberExpression(List<MemberSelectionExpression> membersList, MEMBEREXPRESSIONTYPE type, String member) {
			membersList.add(new MemberSelectionExpression(type, "'" + member + "'"));
		}
		
	}

	
	public static class AdditionalDrillThruColumn {
		
		private static final String PARAM_NAME = "ParamName";
		private static final String COL = "Col";
		private static final String COLUMN = "Column";
		public Integer column;
		public String paramName; 
		
		public static void addContent(OMFactory fac, OMElement parent,
				List<AdditionalDrillThruColumn> additionalDrillThruColumns, OMNamespace ns) {
			OMElement addParams = fac.createOMElement(AdhocColumn.ADDITIONAL_DRILL_THRU_COLUMNS, ns);
			for (AdditionalDrillThruColumn oc: additionalDrillThruColumns) {
				OMElement col = fac.createOMElement(AdditionalDrillThruColumn.COLUMN, ns);
				col.addAttribute(AdditionalDrillThruColumn.COL, String.valueOf(oc.column), ns);
				col.addAttribute(AdditionalDrillThruColumn.PARAM_NAME, oc.paramName, ns);
				addParams.addChild(col);
			}
			parent.addChild(addParams);
		}

		public static List<AdditionalDrillThruColumn> parseAdditionalDrillThruColumn(XMLStreamReader el) throws XMLStreamException {
			ArrayList<AdditionalDrillThruColumn> ret = new ArrayList<AdditionalDrillThruColumn>();
			for (XMLStreamIterator iter = new XMLStreamIterator(el); iter.hasNext(); ) {
				iter.next();
				String elName = el.getLocalName();
				if (AdhocColumn.ADDITIONAL_DRILL_THRU_COLUMN.equals(elName)) {
					// old style - convert to new
					AdditionalDrillThruColumn col = new AdditionalDrillThruColumn();
					col.column = XmlUtils.getIntContent(el);
					ret.add(col);
				}
				else {
					int attCount = el.getAttributeCount();
					String name = null;
					Integer col = null;
					for (int i = 0; i < attCount; i++) {
						if (COL.equals(el.getAttributeName(i).getLocalPart()))
							col = Integer.valueOf(el.getAttributeValue(i));
						else if (PARAM_NAME.equals(el.getAttributeName(i).getLocalPart()))
							name = el.getAttributeValue(i);
					}
					
					if (name != null && col != null) {
						AdditionalDrillThruColumn oc = new AdditionalDrillThruColumn();
						oc.column = col;
						oc.paramName = name;
						ret.add(oc);
					}
				}
			}
			return ret;
		}
		
		public static List<AdditionalDrillThruColumn> parseAdditionalDrillThruColumn(OMElement el) {
			ArrayList<AdditionalDrillThruColumn> ret = new ArrayList<AdditionalDrillThruColumn>();
			for (@SuppressWarnings("unchecked")
					Iterator<OMElement> iter = el.getChildElements(); iter.hasNext(); ) {
				OMElement child = iter.next();
				String elName = child.getLocalName();
				if (AdhocColumn.ADDITIONAL_DRILL_THRU_COLUMN.equals(elName)) {
					AdditionalDrillThruColumn col = new AdditionalDrillThruColumn();
					col.column = XmlUtils.getIntContent(child);
					ret.add(col);
				}
				else {
					String name = null;
					Integer col = null;
					
					name = child.getAttributeValue(new QName(PARAM_NAME));
					col = Integer.valueOf(child.getAttributeValue(new QName(COL)));
					if (name != null && col != null) {
						AdditionalDrillThruColumn oc = new AdditionalDrillThruColumn();
						oc.column = col;
						oc.paramName = name;
						ret.add(oc);
					}
				}
			}
			return ret;
		}
	}
	
	public String getUniqueExpressionString() {
		if (Type == ColumnType.Ratio) {
			return expressionString + " : " + denominatorExpressionString;
		}
		else if (Type == ColumnType.Expression)
			return expressionString;
		
		return getDefaultPromptName();
	}

	public String getSourceFormat() {
		return sourceFormat;
	}

	@Override
	public void addContent(OMElement parent, OMFactory factory, OMNamespace ns) {
		this.addContent(factory, parent, ns);
	}

	public boolean isSortablePivotField() {
		return sortablePivotField;
	}

	public void setSortablePivotField(boolean sortablePivotField) {
		this.sortablePivotField = sortablePivotField;
	}
	
	public Class getClassType()
	{
		return this.classType;
	}
}
