/**
 * 
 */
package com.successmetricsinc.adhoc;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.successmetricsinc.Repository;
import com.successmetricsinc.UserBean;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.query.DisplayFilter;
import com.successmetricsinc.query.Filter;
import com.successmetricsinc.query.QueryFilter;
import com.successmetricsinc.query.Filter.FilterType;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class ChartFilters implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final String DISPLAY_FILTERS = "DisplayFilters";
	private static final String QUERY_FILTERS = "QueryFilters";
	private static final String FILTERS = "Filters";
	private static final String CHART_FILTERS = "ChartFilters";
	private List<Filter> filters;
	private List<QueryFilter> queryFilters;
	private List<DisplayFilter> displayFilters;
	
	public ChartFilters() {
		filters = new ArrayList<Filter>();
		queryFilters = new ArrayList<QueryFilter>();
		displayFilters = new ArrayList<DisplayFilter>();
	}
	
	public void addFilter(String dimension, String column, String operator,	String string) {
		List<String> list = new ArrayList<String>();
		list.add(string);
		addFilter(dimension, column, operator, list);
	}
	
	public void addFilter(String dimension, String column, String operator, List<String> operand) {
		if (dimension != null && Util.hasNonWhiteSpaceCharacters(dimension)) {
			column = dimension + "." + column;
		}
		Filter cf = new Filter(operator, operand, "AND", FilterType.DATA, column);
		filters.add(cf);
	}
	
	public String getXmlText() {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;
		OMElement parent = fac.createOMElement(CHART_FILTERS, ns);
		if (filters != null && filters.size() > 0) {
			OMElement Filters = XmlUtils.addContent(fac, parent, FILTERS, ns);
			for (Filter f : filters) {
				f.addContent(fac, Filters, ns);
			}
		}
		
		if (this.queryFilters != null && this.queryFilters.size() > 0) {
			UserBean sub = SMIWebUserBean.getUserBean();
			if (sub == null)
				sub = UserBean.getUserBean();
			Repository rep = sub.getRepository();
			TimeZone tz = rep.getServerParameters().getProcessingTimeZone();
			SimpleDateFormat sdff = sub.getDefaultFormatDateOrDateTime(false);
			SimpleDateFormat sdft = sub.getDefaultFormatDateOrDateTime(true);
			OMElement queryFilters = XmlUtils.addContent(fac, parent, QUERY_FILTERS, ns);
			for (QueryFilter f : this.queryFilters) {
				f.addContent(fac, queryFilters, ns, tz, sdff, sdft);
			}
		}

		if (this.displayFilters != null && this.displayFilters.size() > 0) {
			UserBean sub = SMIWebUserBean.getUserBean();
			if (sub == null)
				sub = UserBean.getUserBean();
			Repository rep = sub.getRepository();
			TimeZone tz = rep.getServerParameters().getProcessingTimeZone();
			SimpleDateFormat sdff = sub.getDefaultFormatDateOrDateTime(false);
			SimpleDateFormat sdft = sub.getDefaultFormatDateOrDateTime(true);
			OMElement displayFilters = XmlUtils.addContent(fac, parent, DISPLAY_FILTERS, ns);
			for (DisplayFilter f: this.displayFilters) {
				f.addContent(fac, displayFilters, ns, tz, sdff, sdft);
			}
		}

		return XmlUtils.convertToString(parent);
	}

	public void addFilter(QueryFilter qf) {
		queryFilters.add(qf);
	}

	public void addFilter(DisplayFilter df) {
		displayFilters.add(df);
	}
	
	public void addFilter(Filter f) {
		filters.add(f);
	}

	@SuppressWarnings("unchecked")
	public void parseElement(OMElement parent) {
		for (Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String name = el.getLocalName();

			if (FILTERS.equals(name)) {
				for (Iterator<OMElement> iter2 = el.getChildElements(); iter2.hasNext(); ) {
					OMElement el2 = iter2.next();
					if (el2 != null) {
						Filter f = new Filter();
						f.parseElement(el2);
						filters.add(f);
					}
				}
			}
			else if (DISPLAY_FILTERS.equals(name)) {
				for (Iterator<OMElement> iter2 = el.getChildElements(); iter2.hasNext(); ) {
					OMElement el2 = iter2.next();
					if (el2 != null) {
						DisplayFilter f = new DisplayFilter ();
						f.parseElement(el2);
						displayFilters.add(f);
					}
				}
			}
			else if (QUERY_FILTERS.equals(name)) {
				for (Iterator<OMElement> iter2 = el.getChildElements(); iter2.hasNext(); ) {
					OMElement el2 = iter2.next();
					if (el2 != null) {
						QueryFilter f = new QueryFilter ();
						f.parseElement(el2);
						queryFilters.add(f);
					}
				}
			}

		}
	}

	/**
	 * @return the filters
	 */
	public List<Filter> getFilters() {
		return filters;
	}

	/**
	 * @return the queryFilters
	 */
	public List<QueryFilter> getQueryFilters() {
		return queryFilters;
	}

	/**
	 * @return the displayFilters
	 */
	public List<DisplayFilter> getDisplayFilters() {
		return displayFilters;
	}

	public void normalizeFilters() {
		if (filters == null || filters.isEmpty())
			return;
		
		// go thru the list of filters and remove any that are set to NO_FILTER
		List<Filter> newFilters = new ArrayList<Filter>();
		for (int i = 0; i < filters.size(); i++) {
			Filter filter = filters.get(i);
			filter.normalize();
			if (!filter.requiresAValue() || filter.getValues() != null)
				newFilters.add(filter);
		}
		
		// now all NO_FILTERS are gone - consolidate all with the same name and same operator
		List<Filter> nonEqualityFilters = new ArrayList<Filter>();
		Map<String, Filter> equalityFilters = new HashMap<String, Filter>();
		for (Filter filter: newFilters) {
			if (! "=".equals(filter.getOperator())) {
				nonEqualityFilters.add(filter);
				continue;
			}
			Filter existingFilter = equalityFilters.get(filter.getColumnName());
			if (existingFilter != null) {
				existingFilter.getValues().addAll(filter.getValues());
			}
			else {
				equalityFilters.put(filter.getColumnName(), filter);
			}
		}
		
		newFilters = new ArrayList<Filter>();
		for (Filter filter: nonEqualityFilters) {
			List<String> newFilterValues = new ArrayList<String>();
			for (String s: filter.getValues()) {
				if (! newFilterValues.contains(s))
					newFilterValues.add(s);
			}
			filter.setValues(newFilterValues);
			newFilters.add(filter);
		}
		
		for (Filter filter : equalityFilters.values()) {
			List<String> newFilterValues = new ArrayList<String>();
			for (String s: filter.getValues()) {
				if (! newFilterValues.contains(s))
					newFilterValues.add(s);
			}
			filter.setValues(newFilterValues);
			newFilters.add(filter);
		}
		
		filters = newFilters;
	}

}
