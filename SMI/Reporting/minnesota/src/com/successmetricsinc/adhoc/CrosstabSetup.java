/**
 * $Id: CrosstabSetup.java,v 1.1 2008-04-30 20:25:38 agarrison Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.adhoc;

import net.sf.jasperreports.crosstabs.design.JRDesignCellContents;
import net.sf.jasperreports.crosstabs.design.JRDesignCrosstab;
import net.sf.jasperreports.crosstabs.design.JRDesignCrosstabCell;

class CrosstabSetup
{
	JRDesignCrosstab crosstab;
	JRDesignCrosstabCell cell;
	JRDesignCellContents contents;
}