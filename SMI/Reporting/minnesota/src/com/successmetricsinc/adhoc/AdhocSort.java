/**
 * 
 */
package com.successmetricsinc.adhoc;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;

import com.successmetricsinc.util.BirstXMLSerializable;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class AdhocSort extends BirstXMLSerializable {

	private static final long serialVersionUID = 1L;
	
	private static final String COLUMN = AdhocColumn.class.getName();
	private static final String TYPE = "Type";
	private static final String ORDER = "Order";
	
	private static Logger logger = Logger.getLogger(AdhocSort.class);

	public enum SortOrder { ASCENDING, DESCENDING};
	public enum SortType { DATA, DISPLAY };
	private AdhocColumn column;
	private SortOrder order = SortOrder.ASCENDING;
	private SortType type = SortType.DATA;
	
	private static Map<SortOrder, String> sortOrders;
	private static Map<SortType, String>  sortTypes;
	
	static {
		sortOrders = new HashMap<SortOrder, String>();
		sortTypes = new HashMap<SortType, String>();
		
		sortOrders.put(SortOrder.ASCENDING, "Asc");
		sortOrders.put(SortOrder.DESCENDING, "Desc");
		sortTypes.put(SortType.DATA, "Data");
		sortTypes.put(SortType.DISPLAY, "Display");
	}
	
	public AdhocSort() {
	}
	
	public AdhocSort(AdhocColumn column, boolean ascending){
		this.column = column;
		order = ascending ? SortOrder.ASCENDING : SortOrder.DESCENDING;
		type = SortType.DATA;
	}
	
	/* (non-Javadoc)
	 * @see com.successmetricsinc.util.BirstXMLSerializable#addContent(org.apache.axiom.om.OMFactory, org.apache.axiom.om.OMElement, org.apache.axiom.om.OMNamespace)
	 */
	@Override
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		OMElement el = fac.createOMElement(COLUMN, ns);
		column.addContent(fac, el, ns);
		parent.addChild(el);
		XmlUtils.addContent(fac, parent, ORDER, sortOrders.get(order), ns);
		XmlUtils.addContent(fac, parent, TYPE, sortTypes.get(type), ns);

	}

	/* (non-Javadoc)
	 * @see com.successmetricsinc.util.BirstXMLSerializable#createNew()
	 */
	@Override
	public BirstXMLSerializable createNew() {
		return new AdhocSort();
	}

	/* (non-Javadoc)
	 * @see com.successmetricsinc.util.BirstXMLSerializable#parseElement(org.apache.axiom.om.OMElement)
	 */
	@Override
	public void parseElement(OMElement parent) {
		for (@SuppressWarnings("unchecked")
				Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();
			if (COLUMN.equals(elName)) {
				column = new AdhocColumn();
				try {
					column.parseElement(el);
				} catch (Exception e) {
					logger.error("Cannot parse sort column", e);
				}
			}
			else if (ORDER.equals(elName))
				order = parseTypeFromString(XmlUtils.getStringContent(el), sortOrders, SortOrder.ASCENDING);
			else if (TYPE.equals(elName))
				type = parseTypeFromString(XmlUtils.getStringContent(el), sortTypes, SortType.DATA);
		}
	}

	/* (non-Javadoc)
	 * @see com.successmetricsinc.util.BirstXMLSerializable#parseElement(javax.xml.stream.XMLStreamReader)
	 */
	@Override
	public void parseElement(XMLStreamReader parser) throws Exception {
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
			String elName = parser.getLocalName();
			if (COLUMN.equals(elName)) {
				column = new AdhocColumn();
				column.parseElement(parser);
			}
			else if (ORDER.equals(elName))
				order = parseTypeFromString(XmlUtils.getStringContent(parser), sortOrders, SortOrder.ASCENDING);
			else if (TYPE.equals(elName))
				type = parseTypeFromString(XmlUtils.getStringContent(parser), sortTypes, SortType.DATA);
		}
	}

	public AdhocColumn getColumn() {
		return column;
	}

	public SortOrder getOrder() {
		return order;
	}

	public SortType getType() {
		return type;
	}

}
