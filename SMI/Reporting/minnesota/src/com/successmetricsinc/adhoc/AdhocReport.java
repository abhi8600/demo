/**
 * $Id: AdhocReport.java,v 1.497 2012-12-04 12:51:03 BIRST\mpandit Exp $
 *
 * Copyright (C) 2007-2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.adhoc;

import java.awt.Color;
import java.awt.Font;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

import net.sf.jasperreports.crosstabs.JRCellContents;
import net.sf.jasperreports.crosstabs.JRCrosstabColumnGroup;
import net.sf.jasperreports.crosstabs.JRCrosstabRowGroup;
import net.sf.jasperreports.crosstabs.design.JRDesignCellContents;
import net.sf.jasperreports.crosstabs.design.JRDesignCrosstab;
import net.sf.jasperreports.crosstabs.design.JRDesignCrosstabBucket;
import net.sf.jasperreports.crosstabs.design.JRDesignCrosstabCell;
import net.sf.jasperreports.crosstabs.design.JRDesignCrosstabColumnGroup;
import net.sf.jasperreports.crosstabs.design.JRDesignCrosstabMeasure;
import net.sf.jasperreports.crosstabs.design.JRDesignCrosstabParameter;
import net.sf.jasperreports.crosstabs.design.JRDesignCrosstabRowGroup;
import net.sf.jasperreports.crosstabs.type.CrosstabColumnPositionEnum;
import net.sf.jasperreports.crosstabs.type.CrosstabRowPositionEnum;
import net.sf.jasperreports.crosstabs.type.CrosstabTotalPositionEnum;
import net.sf.jasperreports.engine.JRBand;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRElement;
import net.sf.jasperreports.engine.JRElementGroup;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExpression;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRPropertiesMap;
import net.sf.jasperreports.engine.JRSection;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignBand;
import net.sf.jasperreports.engine.design.JRDesignConditionalStyle;
import net.sf.jasperreports.engine.design.JRDesignElement;
import net.sf.jasperreports.engine.design.JRDesignExpression;
import net.sf.jasperreports.engine.design.JRDesignField;
import net.sf.jasperreports.engine.design.JRDesignGroup;
import net.sf.jasperreports.engine.design.JRDesignImage;
import net.sf.jasperreports.engine.design.JRDesignParameter;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JRDesignSection;
import net.sf.jasperreports.engine.design.JRDesignStaticText;
import net.sf.jasperreports.engine.design.JRDesignStyle;
import net.sf.jasperreports.engine.design.JRDesignTextField;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.type.CalculationEnum;
import net.sf.jasperreports.engine.type.HorizontalAlignEnum;
import net.sf.jasperreports.engine.type.HyperlinkTargetEnum;
import net.sf.jasperreports.engine.type.HyperlinkTypeEnum;
import net.sf.jasperreports.engine.type.ModeEnum;
import net.sf.jasperreports.engine.type.OrientationEnum;
import net.sf.jasperreports.engine.type.PositionTypeEnum;
import net.sf.jasperreports.engine.type.SortOrderEnum;
import net.sf.jasperreports.engine.type.SplitTypeEnum;
import net.sf.jasperreports.engine.type.StretchTypeEnum;
import net.sf.jasperreports.engine.type.VerticalAlignEnum;
import net.sf.jasperreports.engine.type.WhenNoDataTypeEnum;
import net.sf.jasperreports.engine.type.WhenResourceMissingTypeEnum;
import net.sf.jasperreports.engine.xml.JRXmlWriter;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.tree.Tree;
import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMAttribute;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMException;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.OMText;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfObject;
import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.User;
import com.successmetricsinc.UserBean;
import com.successmetricsinc.UI.SMIPojoBase;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.WebServices.IWebServiceResult;
import com.successmetricsinc.adhoc.AdhocColumn.AdditionalDrillThruColumn;
import com.successmetricsinc.adhoc.AdhocColumn.ColumnType;
import com.successmetricsinc.adhoc.AdhocEntity.Align;
import com.successmetricsinc.adhoc.AdhocSort.SortOrder;
import com.successmetricsinc.adhoc.AdhocSort.SortType;
import com.successmetricsinc.adhoc.PageInfo.ForcePageBreak;
import com.successmetricsinc.adhoc.PageInfo.PageSizeProperty;
import com.successmetricsinc.catalog.CatalogManager;
import com.successmetricsinc.catalog.CatalogPermission;
import com.successmetricsinc.chart.Chart;
import com.successmetricsinc.chart.ChartOptions;
import com.successmetricsinc.chart.AnyChart.AnyChartNode;
import com.successmetricsinc.chart.ChartOptions.ChartType;
import com.successmetricsinc.query.BadColumnNameException;
import com.successmetricsinc.query.BirstInterval;
import com.successmetricsinc.query.BirstRatio;
import com.successmetricsinc.query.DimensionColumn;
import com.successmetricsinc.query.DimensionColumnNav;
import com.successmetricsinc.query.DisplayExpression;
import com.successmetricsinc.query.DisplayFilter;
import com.successmetricsinc.query.DisplayOrder;
import com.successmetricsinc.query.Filter;
import com.successmetricsinc.query.GroupBy;
import com.successmetricsinc.query.JasperDataSourceProvider;
import com.successmetricsinc.query.Level;
import com.successmetricsinc.query.LogicalQueryString;
import com.successmetricsinc.query.MeasureColumnNav;
import com.successmetricsinc.query.NavigationException;
import com.successmetricsinc.query.OrderBy;
import com.successmetricsinc.query.Query;
import com.successmetricsinc.query.QueryFilter;
import com.successmetricsinc.query.QueryResultSet;
import com.successmetricsinc.query.QueryString;
import com.successmetricsinc.query.SingleRowDataSource;
import com.successmetricsinc.query.SortablePivotField;
import com.successmetricsinc.query.SyntaxErrorException;
import com.successmetricsinc.query.TopFilter;
import com.successmetricsinc.query.DisplayFilter.Type;
import com.successmetricsinc.query.olap.BirstOlapField;
import com.successmetricsinc.query.olap.DimensionMemberSetNav;
import com.successmetricsinc.query.olap.MemberSelectionExpression;
import com.successmetricsinc.transformation.BirstScriptLexer;
import com.successmetricsinc.transformation.BirstScriptParser;
import com.successmetricsinc.transformation.Expression;
import com.successmetricsinc.transformation.Operator;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.transformation.BirstScriptParser.logicalExpression_return;
import com.successmetricsinc.transformation.integer.ToTime;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.ColorUtils;
import com.successmetricsinc.util.FileUtils;
import com.successmetricsinc.util.JasperUtil;
import com.successmetricsinc.util.QuickReport;
import com.successmetricsinc.util.QuickReportDimension;
import com.successmetricsinc.util.TrellisTooLargeException;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class AdhocReport implements IWebServiceResult, Cloneable, Serializable {
	private static final String OLAP_USE_INDENTATION = "OlapUseIndentation";

	private static final String ASYNCH_REPORT_GENERATION = "AsynchReportGeneration";

	private static final String IN_VISUALIZATION = "IsInVisualization";

	private static final String SORT = "Sort";

	private static final String SORTS = "Sorts";

	private static final String TOP_FILTER = "TopFilter";

	private static final String CREATED_BY = "createdBy";

	public static final String SHARED_STYLES_DEFAULT_ADHOC_REPORT = "shared/styles/.default.AdhocReport";

	private static final String DO_NOT_STYLE_HYPERLINKS = "DoNotStyleHyperlinks";

	private static final long serialVersionUID = 1L;

	private static final String USE_FLEX_GRID = "UseFlexGrid";

	private static final String BACKGROUND_INVISIBLE = "BackgroundInvisible";

	private static final String USE_OLD_PARSER = "UseOldParser";

	private static final String USE_OLD_SIZING = "UseOldSizing";

	public static final String DEBUG_ADHOC_REPORTS = "DebugAdhocReports";
	
	private static final String DEFAULT_CHART_INDEX = "DefaultChartIndex";
	private static final String FILTERS_ALIGNMENT = "FiltersAlignment";
	private static final String FILTERS_FOREGROUND = "FiltersForeground";
	private static final String FILTERS_BACKGROUND = "FiltersBackground";
	private static final String FILTERS_FONT = "FiltersFont";
	private static final String FILTERS_SHOW_IN_DASH = "FilterShowInDashboards";
	private static final String FILTERS_SHOW_ON_ONE_LINE = "FilterShowOnOneLine";
	private static final String FILTERS_SKIP_ALL = "FilterSkipAll";
	private static final String SHOW_FILTERS = "ShowFilters";
	public static final String ADHOC_REPORT = "AdhocReport";
	public static final String JASPERREPORTS_EXPORT_PREFIX = "net.sf.jasperreports.export";
	private static final String DRILL_COLUMN = "DrillColumn_";
	public static final String COLUMN_SELECTOR_COUNT = "Count";
	public static final String COLUMN_SELECTOR_ITEM_COUNT = COLUMN_SELECTOR_COUNT;
	public static final String COLUMN_SELECTOR_ITEM_COLUMN = ".Column";
	public static final String COLUMN_SELECTOR_ITEM_DIMENSION = ".Dimension";
	public static final String COLUMN_SELECTOR_ITEM_LABEL = ".Label";
	public static final String COLUMN_SELECTOR_SELECTED = ".Selected";
	public static final String COLUMN_SELECTOR_NAME = ".Name";
	public static final String COLUMN_SELECTOR = "ColumnSelector.";
	public static final String SELECTED = "Selected";
	public static final String COLUMN = "Column";
	public static final String COLUMN_SELECTORS = "ColumnSelectors";
	public static final String CHART_TYPE = "ChartType";
	public static final String INCLUDE_CHART_SELECTOR = "IncludeChartSelector";
	private static final String DASHLET_PROMPTS = "Selectors";
	private static final String NAME = "Name";
	private static final String CSV_FILENAME = "CSV_Filename";
	private static final String CSV_MIMETYPE = "CSV_Mimetype";
	private static final String ADHOC_PATH = "Path";
	private static final String ADHOC_NAME = NAME;
	private static final String DESCRIPTION_HYPERLINK = "DescriptionHyperlink";
	private static final String DESCRIPTION = "Description";
	private static final String HIDE_TITLE = "HideTitle";
	private static final String TITLE = "Title";
	private static final String CSVSEPARATOR = "CSVSeparator";
	private static final String NUM_GROUPS = "NumGroups";
	private static final String SECOND_ALTERNATE_R_G_B = "SecondAlternateRGB";
	private static final String FIRST_ALTERNATE_R_G_B = "FirstAlternateRGB";
	private static final String BACKGROUND_COLOR = "BackgroundColor";
	private static final String ALTERNATE_ROW_COLORS = "AlternateRowColors";
	private static final String ROW_HEIGHT = "RowHeight";
	private static final String TITLE_HEIGHT = "TitleHeight";
	private static final String BOTTOM_MARGIN = "BottomMargin";
	private static final String TOP_MARGIN = "TopMargin";
	private static final String RIGHT_MARGIN = "RightMargin";
	private static final String LEFT_MARGIN = "LeftMargin";
	private static final String PAGE_SIZE = "PageSize";
	private static final String PAGE_ORIENTATION = "PageOrientation";
	private static final String LANDSCAPE = "Landscape";
	private static final String PAGE_WIDTH = "PageWidth";
	private static final String PAGE_HEIGHT = "PageHeight";
	private static final String INCLUDE_TABLE = "IncludeTable";
	private static final String GRAND_TOTALS = "GrandTotals";
	private static final String CHART_OPTIONS = "ChartOptions";
	private static final String TOP_N = "TopN";
	private static final String PAGE_INFO = "PageInfo";
	private static final String DISPLAY_FILTERS = "DisplayFilters";
	private static final String QUERY_FILTERS = "Filters";
	private static final String REPORT_FILTERS = "ReportFilters";
	private static final String SORT_ORDERS = "SortOrders";
	private static final String SORT_ENTITIES = "SortEntities";
	private static final String ENTITIES = "Entities";
	private static final String AUTOMATIC_LAYOUT = "AutomaticLayout";
	private static final String NEXT_ID = "NextId";
	private static final String HIDE_CONTENT = "HideContent";
	public static final String REPORT_INDEX = "ReportIndex";
	private static final String VERSION = "version";
	private static Logger logger = Logger.getLogger(AdhocReport.class);
	private static final String ADHOC_NAMESPACE = "http://www.successmetricsinc.com/AdhocReport";
	private static final int BETWEEN_COLUMN_SPACING = 0;
	private static final String REPLACE_NULL_TO_ZERO = "Replace_Null_To_Zero";
	private static final String REPLACE_NAN_TO_BLANK = "Replace_NaN_To_Blank";
	private static final String REPLACE_INFINITE_TO_BLANK = "Replace_Infinite_To_Blank";
    
    // Pattern to detect possible XXE fields
	private static Pattern XXE = Pattern.compile("<!ENTITY(.)+(SYSTEM|PUBLIC)( )+\"(.)+\"( )+>");
	
	public enum Band {
        Header, Detail, Summary, GroupHeader, GroupFooter, Title, PageHeader, PageFooter
    }
	private static final Color DEFAULT_HEAD_BACK_COLOR = new Color(0xe6, 0xe6, 0xe6);
	public static final Color DEFAULT_HEAD_FORE_COLOR = Color.WHITE;
	public static final Color DEFAULT_BACK_COLOR = DEFAULT_HEAD_BACK_COLOR;
	private static final Color DEFAULT_ALTERNATE_COLOR = Color.WHITE;
	// Minimum space to be reserved for a table
	public static final int MINIMUM_TABLE_HEIGHT = 100;
	private static final int DEFAULT_TITLE_FONT_SIZE = 14;
	private static final String DEFAULT_FILTERS_FONT_STR = "Arial-PLAIN-8";
	
	private String createdBy;
	private String name;
	private List<AdhocEntity> entities;
	private List<AdhocColumn> columns;
	private List<AdhocColumn> sortEntities;
	private List<Boolean> sortOrders;
	private List<AdhocSort> sorts;
	private List<QueryFilter> filters;
	private List<DisplayFilter> displayFilters;
	private List<ReportFilter> reportFilters;
//	private int topN;
	private TopFilter topFilter;
	private ChartOptions chartOptions;
	private boolean grandtotals;
	private boolean includeTable;
	private int pageHeight = PageInfo.LETTER_HEIGHT;
	private int pageWidth = PageInfo.LETTER_WIDTH;
	private OrientationEnum pageOrientation = OrientationEnum.PORTRAIT; //JRReport.ORIENTATION_PORTRAIT;
	private String pageSize = PageInfo.PAGE_SIZE_LETTER;
	private int leftMargin = 76;
	private int rightMargin = 76;
	private int topMargin = 20;
	private int bottomMargin = 20;
	private int titleHeight = PageInfo.DEFAULT_TITLE_BAND_HEIGHT;
	// Height of a table row
	private int rowHeight = PageInfo.DEFAULT_ROW_HEIGHT;
	private boolean alternateRowColors;
	private String firstAlternateRGB = ColorUtils.colorToString(new Color(0xF7, 0xF7, 0xF7));
	private String secondAlternateAltRGB = ColorUtils.colorToString(DEFAULT_ALTERNATE_COLOR);
	private Color backgroundColor = Color.WHITE;
	private boolean backgroundInvisible = false;
	private int numGroups = 0;
	private boolean [] doNotPageBreakGroup;
	public static final String CSV_SEPARATOR = ",";
	private String separatorCSV = CSV_SEPARATOR;
	public static final String CSV_MIME_TYPE = "text/csv";
	private String mimeTypeCSV = CSV_MIME_TYPE;
	public static final String CSV_FILE_NAME = "file.csv";
	private String filenameCSV = CSV_FILE_NAME;
	private boolean automaticLayout = true;
	private String path;
	// Positions added by expanding expressions
	private transient List<DisplayExpression> expressionList;
	private PageInfo pi;
	private static boolean DEBUG = false;
	transient boolean isPivot;
	private int nextId = 1;
	private String title;
	private boolean hideTitle;
	private String description;
	private String descriptionHyperlink;
	private int titleFontSize = DEFAULT_TITLE_FONT_SIZE;
	private boolean editDashlet = false;
    private boolean hidedata = false;
    private PivotTableOptions pivotOpts;
    private boolean showNullZero = false;
    private boolean showINFBlank = false;
    private boolean showNaNBlank = false;
    private boolean showFilters = false;
	private String filtersFontStr = "Arial-PLAIN-6";
	private Color filtersBackground = Color.white;
	private Color filtersForeground = AdhocEntity.DEFAULT_FOREGROUND_COLOR;
	private AdhocEntity.Align filtersAlignment = Align.Default;
	private boolean showReportFilterStrInDash = false;
	private boolean filtersShowOnOneLine = false;
	private boolean filtersSkipAll = false;
	
	private boolean useFlexGrid = false;
	private boolean doNotStyleHyperlinks = false;
    
    //Column data text font width
//    private boolean doneMaxColumnDataWidthCalc = false;
//    private int maxColumnDataWidth = -1;
    private int defaultChartIndex = -1;
   
	private static boolean USE_DOM_PARSER;
    private Map<String, Font> fontMap = new HashMap<String, Font>();
    // bug 9990
    private boolean useOldSizing = true;
    
    // do not save anywhere.  only used by visualization to allow changing chart types.
    private boolean inVisualization = false;
    
    private boolean asynchReportGeneration = false;
    private boolean olapUseIndentation = true;

	static {
		Properties properties = Util.getProperties(Util.CUSTOMER_PROPERTIES_FILENAME, true);
		if (properties != null) {
			String d = properties.getProperty(DEBUG_ADHOC_REPORTS);
			if (d != null && "true".equals(d.toLowerCase()))
				DEBUG = true;
			
			d = properties.getProperty(USE_OLD_PARSER);
			if (d != null && "true".equals(d.toLowerCase()))
				USE_DOM_PARSER = true;
		}
	}
	
	public static AdhocReport getDefaultReportProperties() {
		try {
			UserBean ub = UserBean.getUserBean();
			Session session = ub.getRepSession();
			if (session.isLoadingDefaultReportProperties())
				return null;
			
			Object o = session.getDefaultReportProperties();
			if (o == null || ! (o instanceof AdhocReport)) {
				session.setLoadingDefaultReportProperties(true);
				String root = session.getCatalogDir();
				AdhocReport report = AdhocReport.read(new File(root + "/" + SHARED_STYLES_DEFAULT_ADHOC_REPORT));
				session.setDefaultReportProperties(report);
				o = report;
				session.setLoadingDefaultReportProperties(false);
			}
			return (AdhocReport)o;
		}
		catch (Exception e) {
			// don't log.  no one cares
//			logger.error(e, e);
		}
		return null;
		
	}
	
	public void save(File file) throws IOException {
		
		if (! file.exists()) {
			SMIWebUserBean ub = SMIWebUserBean.getUserBean();
			if (ub != null)
				createdBy = ub.getUserName();
			else {
				UserBean userB = UserBean.getUserBean();
				if (userB != null) {
					User user = userB.getUser();
					if (user != null)
						createdBy = userB.getUser().getUserName();
				}
			}
		}
		OMElement doc = getSaveXMLDocument();
		cleanDocForSave(doc);
		FileOutputStream fos = null;
		
		try {
			if (! file.exists()) {
				file.createNewFile();
			}
			fos = new FileOutputStream(file);
			XMLStreamWriter writer = XMLOutputFactory.newInstance().createXMLStreamWriter(fos);
			doc.serialize(writer);
			writer.flush();
		} catch (IOException e) {
			logger.error("Error saving adhoc report: (with attempted file name: " + file.toString() +")", e);
			throw (e);
		} catch (XMLStreamException e) {
			logger.error(e, e);
		} catch (FactoryConfigurationError e) {
			logger.error(e, e);
		}
		finally {
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
				}
			}
		}
		
	}
	
	@SuppressWarnings("rawtypes")
	private void cleanDocForSave(OMElement doc) {
		for (Iterator iter = doc.getChildren(); iter.hasNext(); ) {
			OMElement el = (OMElement) iter.next();
			if (el.getLocalName().equals(ENTITIES)) {
				for (Iterator iter2 = el.getChildren(); iter2.hasNext(); ) {
					OMElement element = (OMElement) iter2.next();
					if (element.getLocalName().equals(AdhocChart.class.getName())) { 
						for (Iterator iter3 = element.getChildren(); iter3.hasNext(); ) {
							OMElement adhocChart = (OMElement) iter3.next();
							if (adhocChart.getLocalName().equals(AdhocChart.CHARTS)) {
								for (Iterator iter4 = adhocChart.getChildren(); iter4.hasNext(); ) {
									OMElement charts = (OMElement) iter4.next();
									if (charts.getLocalName().equals("Chart")) {
										for (Iterator iter5 = charts.getChildren(); iter5.hasNext(); ) {
											OMElement chart = (OMElement)iter5.next();
											if (chart.getLocalName().equals(AdhocReport.class.getName())) {
												iter5.remove();
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		
	}

	public String toString() {
		StringWriter s = new StringWriter();
		OMElement doc = getSaveXMLDocument();

		try {
			XMLStreamWriter xmlStreamWriter = XMLOutputFactory.newInstance().createXMLStreamWriter(s);
			doc.serialize(xmlStreamWriter);
			xmlStreamWriter.flush();
		}
		catch (Exception e) 
		{ 
			logger.debug(e, e);
		}
		return s.toString();
	}
	
	public static AdhocReport read(File file) throws Exception {
		XMLStreamReader parser = null;
		FileInputStream fileInputStream = null;
		InputStreamReader is = null;
		try {
			fileInputStream = new FileInputStream(file);
			is = new InputStreamReader(fileInputStream, "UTF-8");
			XMLInputFactory inputFactory = XmlUtils.getSecureXMLInputFactory();
			parser = inputFactory.createXMLStreamReader(is);

			return convert(parser, file, null);
		} catch (FileNotFoundException e) {
			logger.info("AdhocReport:read: " + e.getMessage());
		} catch (XMLStreamException e) {
			logger.error(e, e);
		} catch (FactoryConfigurationError e) {
			logger.error(e, e);
		}finally{
			try	{
				if(parser != null){
					parser.close();
				}
			}catch(Exception ex){
				logger.warn("Error in closing parser object");
			}
			try {
				if(is != null)	{
					is.close();
				}
			}catch(Exception ex){
				logger.warn("Error in closing InputStreamReader object");
			}
			try {
				if(fileInputStream != null)	{
					fileInputStream.close();
				}
			}catch(Exception ex){
				logger.warn("Error in closing FileInputStream object");
			}
		}		
		return null;
	}
		
	public static AdhocReport convert(OMElement doc, File file) throws Exception {
		if (doc != null) {
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMNamespace ns = factory.createOMNamespace(ADHOC_NAMESPACE, null);
			AdhocReport report = preConvert(file);
			return report.internalConvert(doc, file, ns);
		}
		return null;
	}
	
	public static AdhocReport convert(String xml, File file, OMNamespace ns) throws Exception {

		XMLStreamReader parser;
		try {
			XMLInputFactory inputFactory = XmlUtils.getSecureXMLInputFactory();
			parser = inputFactory.createXMLStreamReader(new StringReader(xml));

			return convert(parser, file, ns);
		} catch (XMLStreamException e) {
			logger.error(e, e);
		} catch (FactoryConfigurationError e) {
			logger.error(e, e);
		}
		return null;
	}
	
	//Checking the SOAP XML for external entities that could be use in an XXE attack
	
	private static AdhocReport preConvert(File file) {
		AdhocReport report = new AdhocReport();
		
		// default these values and let the xml overwrite if necessary
		report.rowHeight = PageInfo.DEFAULT_ROW_HEIGHT;
		report.useOldSizing = true;
		
		String name = null;
		String path = null;
		SMIWebUserBean userBean = SMIWebUserBean.getUserBean();
		if (userBean != null) {
			if (file != null) {
				name = file.getName();
				int index = name.lastIndexOf('.');
				if (index > 0) {
					name = name.substring(0, index);
				}
				path = file.getParent();
				
				String rootFolder = userBean.getCatalogManager().getCatalogRootPath();
				if (path.startsWith(rootFolder)) {
					path = path.substring(rootFolder.length());
				}
			}
			if (path != null) {
				path = path.replace('\\', '/');
			}
		}
		report.setName(name);
		report.setPath(path);
		return report;
	}
	public static AdhocReport convert(XMLStreamReader parser, File file, OMNamespace ns) throws Exception {
		if (parser != null) {
			AdhocReport report = preConvert(file);
			if (USE_DOM_PARSER) {
				StAXOMBuilder builder = new StAXOMBuilder(parser);
				OMElement doc = null;
				doc = builder.getDocumentElement();
				doc.build();
				try {
					doc.detach();
				}
				catch (OMException ome) {}
				return report.internalConvert(doc, file, ns);
			}
			else {
				return report.internalConvert(parser, file, ns);
			}
		}
		return null;
	}
	
	public static AdhocReport convert(XMLStreamReader parser) throws Exception {
		return new AdhocReport().internalConvert(parser, null, null);
	}
	
	public static String getCreatedBy(File file) {
		XMLStreamReader parser = null;
		FileInputStream fileInputStream = null;
		InputStreamReader is = null;
		try {
			fileInputStream = new FileInputStream(file);
			is = new InputStreamReader(fileInputStream, "UTF-8");
			XMLInputFactory inputFactory = XmlUtils.getSecureXMLInputFactory();
			parser = inputFactory.createXMLStreamReader(is);

			XMLStreamIterator iter = new XMLStreamIterator(parser);
			iter.hasNext();
			int attCount = parser.getAttributeCount();
			for (int i = 0; i < attCount; i++) {
				if (CREATED_BY.equals(parser.getAttributeLocalName(i))) {
					return parser.getAttributeValue(i);
				}
			}
		} catch (FileNotFoundException e) {
			logger.info("AdhocReport:read: " + e.getMessage());
		} catch (XMLStreamException | IllegalStateException | FactoryConfigurationError e) {
			logger.error(e, e);
		} catch (UnsupportedEncodingException e) {
			logger.error(e, e);
		}finally{
			try	{
				if(parser != null){
					parser.close();
				}
			}catch(Exception ex){
				logger.warn("Error in closing parser object");
			}
			try {
				if(is != null)	{
					is.close();
				}
			}catch(Exception ex){
				logger.warn("Error in closing InputStreamReader object");
			}
			try {
				if(fileInputStream != null)	{
					fileInputStream.close();
				}
			}catch(Exception ex){
				logger.warn("Error in closing fileinputStream object");
			}
		}		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	private AdhocReport internalConvert(XMLStreamReader parser, File file, OMNamespace ns) throws Exception {
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
			int attCount = parser.getAttributeCount();
			for (int i = 0; i < attCount; i++) {
				if (CREATED_BY.equals(parser.getAttributeLocalName(i))) {
					createdBy = parser.getAttributeValue(i);
				}
			}
			String elName = parser.getLocalName();
			if (NAME.equals(elName)) {
				String n = XmlUtils.getStringContent(parser);
				if (file == null) {
					name = n;
				}
			}
			else if (VERSION.equals(elName)) {
				XmlUtils.getIntContent(parser);
			}
			else if ("Path".equals(elName)) {
				String p = XmlUtils.getStringContent(parser);
				if (file == null)
					path = p;
			}
			else if (HIDE_CONTENT.equals(elName))
				setHidedata(XmlUtils.getBooleanContent(parser));
			else if (REPLACE_NULL_TO_ZERO.equals(elName))
				setShowNullZero(XmlUtils.getBooleanContent(parser));
			else if (REPLACE_NAN_TO_BLANK.equals(elName))
				setShowNaNBlank(XmlUtils.getBooleanContent(parser));
			else if (REPLACE_INFINITE_TO_BLANK.equals(elName))
				setShowINFBlank(XmlUtils.getBooleanContent(parser));
			else if (NEXT_ID.equals(elName))
				nextId = XmlUtils.getIntContent(parser);
			else if (AUTOMATIC_LAYOUT.equals(elName))
				automaticLayout = XmlUtils.getBooleanContent(parser);
			else if (ENTITIES.equals(elName))
				parseEntities(parser);
			else if (SORT_ENTITIES.equals(elName))
				parseSortEntities(parser);
			else if (SORT_ORDERS.equals(elName)) 
				parseSortOrders(parser);
			else if (SORTS.equals(elName))
				sorts = XmlUtils.parseAllChildren(parser, new AdhocSort(), new ArrayList<AdhocSort>());
			else if (QUERY_FILTERS.equals(elName))
				parseQueryFilters(parser);
			else if (DISPLAY_FILTERS.equals(elName))
				parseDisplayFilters(parser);
			else if (REPORT_FILTERS.equals(elName))
				parseReportFilters(parser);
			else if (PAGE_INFO.equals(elName))
				parsePageInfo(parser);
			else if (TOP_N.equals(elName))
				topFilter = new TopFilter(null, XmlUtils.getIntContent(parser));
			else if (TOP_FILTER.equals(elName))
				topFilter = (TopFilter)XmlUtils.parseFirstChild(parser, new TopFilter());
			else if (CHART_OPTIONS.equals(elName))
				setChartOptions(new ChartOptions(XmlUtils.getStringContent(parser)));
			else if (GRAND_TOTALS.equals(elName))
				setGrandtotals(XmlUtils.getBooleanContent(parser));
			else if (INCLUDE_TABLE.equals(elName))
				setIncludeTable(XmlUtils.getBooleanContent(parser));
			else if (PAGE_HEIGHT.equals(elName))
				setPageHeight(XmlUtils.getIntContent(parser));
			else if (PAGE_WIDTH.equals(elName))
				setPageWidth(XmlUtils.getIntContent(parser));
			else if (PAGE_ORIENTATION.equals(elName))
				setPageOrientation(LANDSCAPE.equals(XmlUtils.getStringContent(parser)) ? OrientationEnum.LANDSCAPE : OrientationEnum.PORTRAIT);
			else if (PAGE_SIZE.equals(elName))
				pageSize = XmlUtils.getStringContent(parser);
			else if (LEFT_MARGIN.equals(elName))
				setLeftMargin(XmlUtils.getIntContent(parser));
			else if (RIGHT_MARGIN.equals(elName))
				setRightMargin(XmlUtils.getIntContent(parser));
			else if (TOP_MARGIN.equals(elName))
				setTopMargin(XmlUtils.getIntContent(parser));
			else if (BOTTOM_MARGIN.equals(elName))
				setBottomMargin(XmlUtils.getIntContent(parser));
			else if (TITLE_HEIGHT.equals(elName))
				setTitleHeight(XmlUtils.getIntContent(parser));
			else if (ROW_HEIGHT.equals(elName))
				setRowHeight(XmlUtils.getIntContent(parser));
			else if (ALTERNATE_ROW_COLORS.equals(elName))
				setAlternateRowColors(XmlUtils.getBooleanContent(parser));
			else if (FIRST_ALTERNATE_R_G_B.equals(elName))
				setFirstAlternateRGB(XmlUtils.getStringContent(parser));
			else if (SECOND_ALTERNATE_R_G_B.equals(elName))
				setSecondAlternateAltRGB(XmlUtils.getStringContent(parser));
			else if (BACKGROUND_COLOR.equals(elName))
				backgroundColor = XmlUtils.getColorContent(parser);
			else if (BACKGROUND_INVISIBLE.equals(elName))
				backgroundInvisible = XmlUtils.getBooleanContent(parser);
			else if (NUM_GROUPS.equals(elName))
				setNumGroups(XmlUtils.getIntContent(parser));
			else if (CSVSEPARATOR.equals(elName))
				setSeparatorCSV(XmlUtils.getStringContent(parser));
			else if (CSV_MIMETYPE.equals(elName))
				setMimeTypeCSV(XmlUtils.getStringContent(parser));
			else if (CSV_FILENAME.equals(elName))
				setFilenameCSV(XmlUtils.getStringContent(parser));
			else if (TITLE.equals(elName))
				setTitle(XmlUtils.getStringContent(parser));
			else if (HIDE_TITLE.equals(elName))
				setHideTitle(XmlUtils.getBooleanContent(parser));
			else if (DESCRIPTION.equals(elName))
				setDescription(XmlUtils.getStringContent(parser));
			else if (DESCRIPTION_HYPERLINK.equals(elName))
				setDescriptionHyperlink(XmlUtils.getStringContent(parser));
			else if (PivotTableOptions.ROOT_TAG.equals(elName))
				parsePivotTableOptions(parser);
			else if (SHOW_FILTERS.equals(elName))
				showFilters = XmlUtils.getBooleanContent(parser);
			else if (FILTERS_FONT.equals(elName))
				filtersFontStr = XmlUtils.getStringContent(parser);
			else if (FILTERS_BACKGROUND.equals(elName))
				filtersBackground = XmlUtils.getColorContent(parser);
			else if (FILTERS_FOREGROUND.equals(elName))
				filtersForeground = XmlUtils.getColorContent(parser);
			else if (FILTERS_ALIGNMENT.equals(elName))
				filtersAlignment = AdhocEntity.parseAlignment(XmlUtils.getStringContent(parser));
			else if (FILTERS_SHOW_IN_DASH.equals(elName))
				showReportFilterStrInDash = XmlUtils.getBooleanContent(parser);
			else if (FILTERS_SHOW_ON_ONE_LINE.equals(elName))
				filtersShowOnOneLine = XmlUtils.getBooleanContent(parser);
			else if (FILTERS_SKIP_ALL.equals(elName))
				filtersSkipAll = XmlUtils.getBooleanContent(parser);
			else if (DEFAULT_CHART_INDEX.equals(elName))
				defaultChartIndex = XmlUtils.getIntContent(parser);
			else if (USE_OLD_SIZING.equals(elName))
				useOldSizing = XmlUtils.getBooleanContent(parser);
			else if (USE_FLEX_GRID.equals(elName))
				useFlexGrid = XmlUtils.getBooleanContent(parser);
			else if (DO_NOT_STYLE_HYPERLINKS.equals(elName))
				doNotStyleHyperlinks = XmlUtils.getBooleanContent(parser);
			else if (IN_VISUALIZATION.equals(elName))
				inVisualization = XmlUtils.getBooleanContent(parser);
			else if (AdhocReport.ASYNCH_REPORT_GENERATION.equals(elName))
				setAsynchReportGeneration(XmlUtils.getBooleanContent(parser));
			else if (AdhocReport.OLAP_USE_INDENTATION.equals(elName))
				olapUseIndentation = XmlUtils.getBooleanContent(parser);
		}
		postParse(SMIWebUserBean.getUserBean());
		return this;
	}
	
	@SuppressWarnings("unchecked")
	private AdhocReport internalConvert(OMElement doc, File file, OMNamespace ns) throws Exception {
		for (@SuppressWarnings("unchecked")
				Iterator iter1 = doc.getAllAttributes(); iter1.hasNext(); ) {
			OMAttribute attrib = (OMAttribute)iter1.next();
			if (CREATED_BY.equals(attrib.getLocalName()))
				createdBy = attrib.getAttributeValue();
		}
		for (@SuppressWarnings("unchecked")
		Iterator<OMElement> iter = doc.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();
			if (NAME.equals(elName)) {
				if (file == null) {
					name = XmlUtils.getStringContent(el);
				}
			}
			else if (VERSION.equals(elName)) {
				XmlUtils.getIntContent(el);
			}
			else if ("Path".equals(elName)) {
				if (file == null) {
					path = XmlUtils.getStringContent(el);
				}
			}
			else if (HIDE_CONTENT.equals(elName)) {
				setHidedata(XmlUtils.getBooleanContent(el));
			}
			else if (REPLACE_NULL_TO_ZERO.equals(elName)) {
				setShowNullZero(XmlUtils.getBooleanContent(el));
			}
			else if (REPLACE_NAN_TO_BLANK.equals(elName)) {
				setShowNaNBlank(XmlUtils.getBooleanContent(el));
			}
			else if (REPLACE_INFINITE_TO_BLANK.equals(elName)) {
				setShowINFBlank(XmlUtils.getBooleanContent(el));
			}
			else if (NEXT_ID.equals(elName)) {
				nextId = XmlUtils.getIntContent(el);
			}
			else if (AUTOMATIC_LAYOUT.equals(elName)) {
				automaticLayout = XmlUtils.getBooleanContent(el);
			}
			else if (ENTITIES.equals(elName)) {
				parseEntities(el);
			}
			else if (SORT_ENTITIES.equals(elName)) {
				parseSortEntities(el);
			}
			else if (SORTS.equals(elName))
				sorts = XmlUtils.parseAllChildren(el, new AdhocSort(), new ArrayList<AdhocSort>());
			else if (SORT_ORDERS.equals(elName)) {
				parseSortOrders(el);
			}
			else if (QUERY_FILTERS.equals(elName)) {
				parseQueryFilters(el);
			}
			else if (DISPLAY_FILTERS.equals(elName)) {
				parseDisplayFilters(el);
			}
			else if (REPORT_FILTERS.equals(elName)) {
				parseReportFilters(el);
			}
			else if (PAGE_INFO.equals(elName)) {
				parsePageInfo(el);
			}
			else if (TOP_N.equals(elName)) 
				topFilter = new TopFilter(null, XmlUtils.getIntContent(el));
			else if (TOP_FILTER.equals(elName))
				topFilter = (TopFilter)XmlUtils.parseFirstChild(el, new TopFilter());
			else if (CHART_OPTIONS.equals(elName)) {
				setChartOptions(new ChartOptions(XmlUtils.getStringContent(el)));
			}
			else if (GRAND_TOTALS.equals(elName)) {
				setGrandtotals(XmlUtils.getBooleanContent(el));
			}
			else if (INCLUDE_TABLE.equals(elName)) {
				setIncludeTable(XmlUtils.getBooleanContent(el));
			}
			else if (PAGE_HEIGHT.equals(elName)) {
				setPageHeight(XmlUtils.getIntContent(el));
			}
			else if (PAGE_WIDTH.equals(elName)) {
				setPageWidth(XmlUtils.getIntContent(el));
			}
			else if (PAGE_ORIENTATION.equals(elName)) {
				setPageOrientation(LANDSCAPE.equals(XmlUtils.getStringContent(el)) ? OrientationEnum.LANDSCAPE : OrientationEnum.PORTRAIT);
			}
			else if (PAGE_SIZE.equals(elName)) {
				pageSize = XmlUtils.getStringContent(el);
			}
			else if (LEFT_MARGIN.equals(elName)) {
				setLeftMargin(XmlUtils.getIntContent(el));
			}
			else if (RIGHT_MARGIN.equals(elName)) {
				setRightMargin(XmlUtils.getIntContent(el));
			}
			else if (TOP_MARGIN.equals(elName)) {
				setTopMargin(XmlUtils.getIntContent(el));
			}
			else if (BOTTOM_MARGIN.equals(elName)) {
				setBottomMargin(XmlUtils.getIntContent(el));
			}
			else if (TITLE_HEIGHT.equals(elName)) {
				setTitleHeight(XmlUtils.getIntContent(el));
			}
			else if (ROW_HEIGHT.equals(elName)) {
				setRowHeight(XmlUtils.getIntContent(el));
			}
			else if (ALTERNATE_ROW_COLORS.equals(elName)) {
				setAlternateRowColors(XmlUtils.getBooleanContent(el));
			}
			else if (FIRST_ALTERNATE_R_G_B.equals(elName)) {
				setFirstAlternateRGB(XmlUtils.getStringContent(el));
			}
			else if (SECOND_ALTERNATE_R_G_B.equals(elName)) {
				setSecondAlternateAltRGB(XmlUtils.getStringContent(el));
			}
			else if (BACKGROUND_COLOR.equals(elName))
				backgroundColor = XmlUtils.getColorContent(el);
			else if (BACKGROUND_INVISIBLE.equals(elName))
				backgroundInvisible = XmlUtils.getBooleanContent(el);
			else if (NUM_GROUPS.equals(elName)) {
				setNumGroups(XmlUtils.getIntContent(el));
			}
			else if (CSVSEPARATOR.equals(elName)) {
				setSeparatorCSV(XmlUtils.getStringContent(el));
			}
			else if (CSV_MIMETYPE.equals(elName)) {
				setMimeTypeCSV(XmlUtils.getStringContent(el));
			}
			else if (CSV_FILENAME.equals(elName)) {
				setFilenameCSV(XmlUtils.getStringContent(el));
			}
			else if (TITLE.equals(elName)) {
				setTitle(XmlUtils.getStringContent(el));
			}
			else if (HIDE_TITLE.equals(elName)) {
				setHideTitle(XmlUtils.getBooleanContent(el));
			}
			else if (DESCRIPTION.equals(elName)) {
				setDescription(XmlUtils.getStringContent(el));
			}
			else if (DESCRIPTION_HYPERLINK.equals(elName)) {
				setDescriptionHyperlink(XmlUtils.getStringContent(el));
			}else if (PivotTableOptions.ROOT_TAG.equals(elName)){
				parsePivotTableOptions(el);
			}
			else if (SHOW_FILTERS.equals(elName))
				showFilters = XmlUtils.getBooleanContent(el);
			else if (FILTERS_FONT.equals(elName))
				filtersFontStr = XmlUtils.getStringContent(el);
			else if (FILTERS_BACKGROUND.equals(elName))
				filtersBackground = XmlUtils.getColorContent(el);
			else if (FILTERS_FOREGROUND.equals(elName))
				filtersForeground = XmlUtils.getColorContent(el);
			else if (FILTERS_ALIGNMENT.equals(elName))
				filtersAlignment = AdhocEntity.parseAlignment(XmlUtils.getStringContent(el));
			else if (FILTERS_SHOW_IN_DASH.equals(elName))
				showReportFilterStrInDash = XmlUtils.getBooleanContent(el);
			else if (FILTERS_SHOW_ON_ONE_LINE.equals(elName))
				filtersShowOnOneLine = XmlUtils.getBooleanContent(el);
			else if (FILTERS_SKIP_ALL.equals(elName))
				filtersSkipAll = XmlUtils.getBooleanContent(el);				
			else if (DEFAULT_CHART_INDEX.equals(elName))
				defaultChartIndex = XmlUtils.getIntContent(el);
			else if (USE_OLD_SIZING.equals(elName))
				useOldSizing = XmlUtils.getBooleanContent(el);
			else if (USE_FLEX_GRID.equals(elName))
				useFlexGrid = XmlUtils.getBooleanContent(el);
			else if (DO_NOT_STYLE_HYPERLINKS.equals(elName))
				doNotStyleHyperlinks = XmlUtils.getBooleanContent(el);
			else if (IN_VISUALIZATION.equals(true))
				inVisualization = XmlUtils.getBooleanContent(el);
			else if (AdhocReport.ASYNCH_REPORT_GENERATION.equals(elName))
				setAsynchReportGeneration(XmlUtils.getBooleanContent(el));
			else if (AdhocReport.OLAP_USE_INDENTATION.equals(elName))
				olapUseIndentation = XmlUtils.getBooleanContent(el);
		}
		postParse(SMIWebUserBean.getUserBean());
		return this;
	}
	
	
	private void postParse(SMIWebUserBean userBean) {
		// clear out name and path if not writeable
		if (userBean != null) {
			String p = getPath();
			if (p != null) {
				CatalogManager cm = userBean.getCatalogManager();
				String root = cm.getCatalogRootPath();
				File f = new File(p);
				if (f.isAbsolute())
					p = FileUtils.normalizePath(p);
				if (p.indexOf(root) != 0){
					p = root + File.separator + p;
				}
				
				CatalogPermission perm = null;
				try {
					perm = cm.getPermission(p);
				}
				catch (Exception e) {}
				
				if (perm == null || (perm != null && perm.canModify(userBean.getUserGroups(), userBean.isSuperUserOwnerOrAdmin()) == false)) {
					setPath(null);
					setName(null);
				}
			}
		}
		
		if (automaticLayout == false) {
			// go thru columns and show any that were hidden, but move to null band
			List<Integer> alwaysHidden = new ArrayList<Integer>();
			for (AdhocEntity ae: getEntities()) {
				if (ae instanceof AdhocImage) {
					int index = ((AdhocImage)ae).getLocationReportIndex(); 
					if (index >= 0)
						alwaysHidden.add(index);
				}
				int index = ae.getHyperlinkReportIndex();
				if (index >= 0)
					alwaysHidden.add(index);
				
				index = ae.getIncludeIfReportIndex();
				if (index >= 0)
					alwaysHidden.add(index);
				
			}
			for (AdhocColumn ac: getColumns()) {
				if (ac.showInReport == false && ! alwaysHidden.contains(ac.getReportIndex())) {
					ac.showInReport = true;
					ac.setBand(null);
					AdhocTextEntity label = ac.getLabelField();
					if (label != null) {
						label.setBand(null);
						label.showInReport = true;
					}
				}
			}
		}
		
		// go thru pageLayout items to see if includeIf's have changed
		if (pi != null)
		{
			List<PageInfo.Band> bands = new ArrayList<PageInfo.Band>();
			bands.add(pi.titleBand);
			bands.add(pi.detailBand);
			if (pi.groupFooters != null) {
				for (PageInfo.Band b : pi.groupFooters) 
					bands.add(b);
			}
			if (pi.groupHeaders != null) {
				for (PageInfo.Band b : pi.groupHeaders) 
					bands.add(b);
			}
			bands.add(pi.summaryBand);
			bands.add(pi.headerBand);
			for (PageInfo.Band b : bands) {
				if (b.getIncludeIfExpression() != null) {
					String expr = b.getIncludeIfExpression().trim();
					if (expr.length() > 0) {
						int reportIndex = b.getIncludeIfReportIndex();
						AdhocEntity ent = this.getEntityById(reportIndex);
						AdhocColumn col = null;
						if (ent != null && ent instanceof AdhocColumn)
							col = (AdhocColumn)ent;
						try {
							if (col == null) {
								// create a new hidden column
								col = new AdhocColumn();
								col.reportIndex = nextId++;
								col.setShowInReport(false);
								col.setExpression(expr, "includeIf" + col.reportIndex);
								this.getEntities().add(col);
								this.columns.add(col);
								b.setIncludeIfReportIndex(col.reportIndex);
								ent = col;
							}
							if (! ent.equals(col.getExpression().getExpression()))
								col.setExpression(expr, "includeIf" + col.reportIndex);
						}
						catch (Exception e)
						{
							logger.error("Could not add include if expression <" + expr + "> on band", e);
						}
					}
					else
						b.setIncludeIfReportIndex(-1);
				}
				else 
					b.setIncludeIfReportIndex(-1);
			}
		}
		
		if (sortEntities != null && sortEntities.size() > 0) {
			if (sorts != null && sorts.size() > 0) {
				sortEntities = null;
				sortOrders = null;
			}
			else {
				if (sorts == null)
					sorts = new ArrayList<AdhocSort>();
				for (int i = 0; i < sortEntities.size(); i++) {
					sorts.add(new AdhocSort(sortEntities.get(i), sortOrders.size() > i ? sortOrders.get(i) : true));
				}
				
				sortEntities = null;
				sortOrders = null;
			}
		}
		
		for (AdhocEntity ae : getEntities()) {
			List<AdditionalDrillThruColumn> adcs = null;
			if (ae instanceof AdhocColumn)
				adcs = ((AdhocColumn)ae).getAdditionalDrillThuColumns();
			else if (ae instanceof AdhocImage)
				adcs = ((AdhocImage)ae).getAdditionalDrillThuColumns();
			else if (ae instanceof AdhocButton)
				adcs = ((AdhocButton)ae).getAdditionalDrillThuColumns();
			if (adcs != null && adcs.size() > 0) {
				for (AdditionalDrillThruColumn adc : adcs) {
					if (adc.paramName == null && adc.column != null) {
						//upgrade
						AdhocEntity ae1 = this.getEntityById(adc.column);
						if (ae1 instanceof AdhocColumn) {
							adc.paramName = ((AdhocColumn)ae1).getDrillToParameterName();
						}
					}
					
					if (adc.paramName == null)
						adc.paramName = "";
				}
			}
		}
		
		List<AdhocSort> sorts = getSorts();
		if (sorts != null) {
			for (Iterator<AdhocSort> iter = sorts.iterator(); iter.hasNext(); ) {
				AdhocSort as = iter.next();
				AdhocColumn ac = as.getColumn();
				if (ac != null) {
					if (ac.isOlapDimension()) {
						AdhocEntity ae1 = this.getEntityById(ac.reportIndex);
						if (ae1 == null || ! (ae1 instanceof AdhocColumn) || ((AdhocColumn)ae1).getType() != AdhocColumn.ColumnType.Dimension) {
							iter.remove();
							continue;
						}
					}
					List<AdditionalDrillThruColumn> adcs = ac.getAdditionalDrillThuColumns();
					if (adcs != null && adcs.size() > 0) {
						for (AdditionalDrillThruColumn adc : adcs) {
							if (adc.paramName == null && adc.column != null) {
								//upgrade
								AdhocEntity ae1 = this.getEntityById(adc.column);
								if (ae1 instanceof AdhocColumn) {
									adc.paramName = ((AdhocColumn)ae1).getDrillToParameterName();
								}
							}
							
							if (adc.paramName == null)
								adc.paramName = "";
						}
					}
				}
			}
		}
		
		upgradeReport();
		
		setElementPositions(false, false, false, 
				getPageOrientation() == OrientationEnum.LANDSCAPE  ? getPageHeight() : getPageWidth(),
				getPageOrientation() == OrientationEnum.LANDSCAPE ? getPageWidth() : getPageHeight()); 
	}
	
	private void upgradeReport() {
		if (chartOptions != null && chartOptions.type != ChartType.None) {
			AdhocChart ac = new AdhocChart(chartOptions, this);
			ac.reportIndex = -1;
			chartOptions.type = ChartType.None;
			ac.setBand(Band.Title);
			addEntity(ac);
			pi.titleBand.height = ac.getHeight();
			defaultChartIndex = ac.getReportIndex();
		}
		organizeColumns();
	}
	
	public AdhocChart getDefaultChart() {
		if (defaultChartIndex >= 0) {
			AdhocEntity entity = getEntityById(defaultChartIndex);
			if (entity instanceof AdhocChart)
				return (AdhocChart)entity;
		}
		return null;
	}
	
	public void setTitleBandHeight(int height)
	{
		pi.titleBand.height = height;
	}
	
	@SuppressWarnings("unchecked")
	private void parseEntities(OMElement entities) throws Exception {
	
		if (this.entities == null) {
			this.entities = new ArrayList<AdhocEntity>();
		}
		for (Iterator<OMElement> iter = entities.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String name = el.getLocalName();
			try {
				AdhocEntity entity = (AdhocEntity) Class.forName(name).newInstance();
				entity.parseElement(el);
				if (entity.getReportIndex() < 0) {
					entity.reportIndex = this.nextId++;
				}
				
				this.entities.add(entity);
			} catch (InstantiationException e) {
				logger.error(e, e);
			} catch (IllegalAccessException e) {
				logger.error(e, e);
			} catch (ClassNotFoundException e) {
				logger.error(e, e);
			}
		}
		postParseEntities();
	}
	
	private void parseEntities(XMLStreamReader parser) throws Exception {
		if (this.entities == null)
			this.entities = new ArrayList<AdhocEntity>();
		
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
			String elName = parser.getLocalName();
			try {
				AdhocEntity entity = (AdhocEntity) Class.forName(elName).newInstance();
				entity.parseElement(parser);
				if (entity.getReportIndex() < 0) {
					entity.reportIndex = this.nextId++;
				}
				
				this.entities.add(entity);
			} catch (InstantiationException e) {
				logger.error(e, e);
			} catch (IllegalAccessException e) {
				logger.error(e, e);
			} catch (ClassNotFoundException e) {
				logger.error(e, e);
			}
		}
		postParseEntities();
	}
	
	private void postParseEntities() throws Exception {
		this.columns = new ArrayList<AdhocColumn>();
		List<AdhocColumn> addList = new ArrayList<AdhocColumn>();
		List<AdhocEntity> removeList = new ArrayList<AdhocEntity>();
		
		for (AdhocEntity entity : this.getEntities()) {
			
			String includeIfExpression = entity.getIncludeIfExpression(); 
			if (includeIfExpression != null && Util.hasNonWhiteSpaceCharacters(includeIfExpression)) {
				int index = entity.getIncludeIfReportIndex();
				if (entity.getIncludeIfReportIndex() < 0 || getEntityById(index) == null) {
					// create a new hidden column
					AdhocColumn ac = new AdhocColumn();
					ac.reportIndex = nextId++;
					ac.setShowInReport(false);
					ac.setExpression(includeIfExpression, "includeIf" + ac.reportIndex);
//					this.getEntities().add(ac);
					addList.add(ac);
					entity.setIncludeIfReportIndex(ac.reportIndex);
				}
				else {
					// see if the column has the same expression
					for (AdhocEntity ac : this.getEntities()) {
						if (ac.reportIndex == index && ac instanceof AdhocColumn) {
							AdhocColumn a = (AdhocColumn)ac;
							DisplayExpression de = a.getExpression();
							if (de != null && !de.getExpression().equals(includeIfExpression)) {
								de.setExpression(includeIfExpression);
							}
						}
					}
				}
			}
			else if (entity.getIncludeIfReportIndex() >= 0) {
				int index = entity.getIncludeIfReportIndex();
				entity.setIncludeIfReportIndex(-1);
				AdhocEntity otherEntity = getEntityById(index);
				if (otherEntity != null) {
					removeList.add(otherEntity);
				}
			}
			
			if (entity instanceof AdhocImage) {
				AdhocImage ai = (AdhocImage)entity;
				String locationExpression = ai.getLocationReportExpression();
				if (locationExpression != null && Util.hasNonWhiteSpaceCharacters(locationExpression)) {
					int index = ai.getLocationReportIndex();
					if (index < 0 || getEntityById(index) == null) {
						// create a new hidden column
						AdhocColumn ac = new AdhocColumn();
						ac.reportIndex = nextId++;
						ac.setShowInReport(false);
						ai.setLocationReportIndex(ac.reportIndex);
						ac.setExpression(locationExpression, ai.getLocationFieldName());
						addList.add(ac);
					}
					else {
						// see if the column has the same expression
						for (AdhocEntity ac : this.getEntities()) {
							if (ac.reportIndex == index && ac instanceof AdhocColumn) {
								AdhocColumn a = (AdhocColumn)ac;
								DisplayExpression de = a.getExpression();
								if (de != null && !de.getExpression().equals(locationExpression)) {
									de.setExpression(locationExpression);
								}
							}
						}
					}
				}
				else if (ai.getLocationReportIndex() >= 0) {
					int index = ai.getLocationReportIndex();
					ai.setLocationReportIndex(-1);
					AdhocEntity otherEntity = getEntityById(index);
					if (otherEntity != null) {
						removeList.add(otherEntity);
					}
				}
			}
			String hyperlinkExpression = entity.getHyperlinkExpression();
			if (hyperlinkExpression != null && Util.hasNonWhiteSpaceCharacters(hyperlinkExpression)) {
				int index = entity.getHyperlinkReportIndex();
				if (index < 0) {
					AdhocColumn ac = new AdhocColumn();
					ac.reportIndex = nextId++;
					ac.setShowInReport(false);
					ac.setExpression(hyperlinkExpression, "hyperlink" + ac.reportIndex);
					ac.setHyperlinkSelftarget(entity.isHyperlinkSelfTarget());
//					this.getEntities().add(ac);
					addList.add(ac);
					entity.setHyperlinkReportIndex(ac.reportIndex);
				}
				else {
					for (AdhocEntity ac : this.getEntities()) {
						if (ac.reportIndex == index && ac instanceof AdhocColumn) {
							AdhocColumn a = (AdhocColumn)ac;
							DisplayExpression de = a.getExpression();
							if (de != null && !de.getExpression().equals(hyperlinkExpression)) {
								de.setExpression(hyperlinkExpression);
							}
							ac.setHyperlinkSelftarget(entity.isHyperlinkSelfTarget());
						}
					}
				}
			}

			if (entity instanceof AdhocColumn) {
				this.columns.add((AdhocColumn)entity);
				List<ConditionalFormat> formats = ((AdhocColumn)entity).getConditionalFormats();
				if (formats != null) {
					for (ConditionalFormat cf: formats) {
						if (cf.getID() == 0) {
							cf.setID(nextId++);
							cf.setName(((AdhocColumn)entity).getConditionalFormatExpressionName(cf));
						}
					}
				}
				
				String label = ((AdhocColumn)entity).getLabelLabel();
				if (label != null && Util.hasNonWhiteSpaceCharacters(label)) {
					label = label.trim();
					try {
						if (Character.isDigit(label.charAt(0))) {
							Integer i = Integer.valueOf(label);
							if (i != null) {
								((AdhocColumn)entity).setLabelField((AdhocTextEntity) this.getEntityById(i.intValue()));
								continue;
							}
						}
					}
					catch (NumberFormatException e) {}
					
					for (AdhocEntity other : this.getEntities()) {
						if (other instanceof AdhocTextEntity) {
							if (((AdhocTextEntity)other).getLabel().equals(label)) {
								((AdhocTextEntity)entity).setLabelField((AdhocTextEntity)other);
								break;
							}
						}
					}
				}
			}
		}
		if (addList.size() > 0) {
			this.getEntities().addAll(addList);
			this.columns.addAll(addList);
		}
		if (removeList.size() > 0) {
			for (AdhocEntity e: removeList) {
				removeEntity(e);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void parseSortEntities(OMElement entities) throws Exception {
		this.sortEntities = new ArrayList<AdhocColumn>();
		for (Iterator<OMElement> iter = entities.getChildElements(); iter.hasNext(); ) { 
			AdhocColumn column = new AdhocColumn();
			column.parseElement(iter.next());
			this.sortEntities.add(column);
		}
	}
	private void parseSortEntities(XMLStreamReader parser) throws Exception {
		this.sortEntities = new ArrayList<AdhocColumn>();
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
				String elName = parser.getLocalName();
				if (AdhocColumn.class.getName().equals(elName)) {
					AdhocColumn column = new AdhocColumn();
					column.parseElement(parser);
					this.sortEntities.add(column);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void parseSortOrders(OMElement entities) {
		this.sortOrders = new ArrayList<Boolean>();
		for (Iterator<OMElement> iter = entities.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			this.sortOrders.add(Boolean.valueOf(el.getText()));
		}
	}
	private void parseSortOrders(XMLStreamReader parser) throws Exception {
		this.sortOrders = new ArrayList<Boolean>();
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
			this.sortOrders.add(XmlUtils.getBooleanContent(parser));
		}
	}
	
	@SuppressWarnings("unchecked")
	private void parseQueryFilters(OMElement entities) {
		this.filters = new ArrayList<QueryFilter>();
		for (Iterator<OMElement> iter = entities.getChildElements(); iter.hasNext(); ) {
			QueryFilter filter = new QueryFilter();
			filter.parseElement(iter.next());
			AdhocColumn ac = this.getColumnByName(filter.getColumn());
			if (ac != null)
				filter.DateTime_Format = ac.getDateTimeFormat();
			this.filters.add(filter);
		}
	}
	private void parseQueryFilters(XMLStreamReader parser) throws Exception {
		this.filters = new ArrayList<QueryFilter>();
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
			String elName = parser.getLocalName();
			if (QueryFilter.FILTER_NAME.equals(elName)) {
				QueryFilter filter = new QueryFilter();
				filter.parseElement(parser);
				AdhocColumn ac = this.getColumnByName(filter.getColumn());
				if (ac != null)
					filter.DateTime_Format = ac.getDateTimeFormat();
				this.filters.add(filter);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void parseDisplayFilters(OMElement entities) {
		this.displayFilters = new ArrayList<DisplayFilter>();
		for (Iterator<OMElement> iter = entities.getChildElements(); iter.hasNext(); ) {
			DisplayFilter filter = new DisplayFilter();
			filter.parseElement(iter.next());
			AdhocColumn ac = this.getColumnByDisplayFilter(filter);
			if (ac != null) {
				filter.DateTime_Format = ac.getDateTimeFormat();
				filter.alias = ac.getFieldName();
			}
			this.addFilter(filter);
		}
	}
	private void parseDisplayFilters(XMLStreamReader parser) throws Exception {
		this.displayFilters = new ArrayList<DisplayFilter>();
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
			String elName = parser.getLocalName();
			if (DisplayFilter.FILTER.equals(elName)) {
				DisplayFilter filter = new DisplayFilter();
				filter.parseElement(parser);
				AdhocColumn ac = this.getColumnByDisplayFilter(filter);
				if(ac != null && ac.getClassType() != null && ac.getClassType().equals(GregorianCalendar.class))					
				{
					filter.dataType = "Date";
				}
				if (ac != null) {
					filter.DateTime_Format = ac.getDateTimeFormat();
					filter.alias = ac.getFieldName();
				}
				this.addFilter(filter);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void parseReportFilters(OMElement entities) {
		this.reportFilters = new ArrayList<ReportFilter>();
		for (Iterator<OMElement> iter = entities.getChildElements(); iter.hasNext(); ) {
			ReportFilter filter = new ReportFilter();
			filter.parseElement(iter.next());
			this.addFilter(filter);
		}
	}
	private void parseReportFilters(XMLStreamReader parser) throws Exception {
		this.reportFilters = new ArrayList<ReportFilter>();
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
			String elName = parser.getLocalName();
			if (ReportFilter.FILTER.equals(elName)) {
				ReportFilter filter = new ReportFilter();
				filter.parseElement(parser);
				this.addFilter(filter);
			}
		}
	}
	
	private void parsePageInfo(OMElement element) {
		this.pi = new PageInfo();
		this.pi.parseElement(element);
	}
	private void parsePageInfo(XMLStreamReader parser) throws Exception {
		this.pi = new PageInfo();
		this.pi.parseElement(parser);
	}
	
	private void parsePivotTableOptions(OMElement element){
		this.pivotOpts = new PivotTableOptions();
		this.pivotOpts.parseElement(element);
	}
	
	private void parsePivotTableOptions(XMLStreamReader parser) throws Exception {
		this.pivotOpts = new PivotTableOptions();
		this.pivotOpts.parseElement(parser);
	}

	public AdhocReport clone() {
		OMElement xml = getSaveXMLDocument();
		AdhocReport report = null;
		try {
			report = AdhocReport.convert(xml, null);
		} catch (Exception e) {
			logger.error(e, e);
		}
		return report;
	}
	
	/**
	 * adjust the size of a chart if it is a trellis chart
	 * - based upon page size and number of attributes in the trellis chart
	 * @throws TrellisTooLargeException 
	 */
	public void adjustChartSizeForTrellis(JRDataSource jrds) throws TrellisTooLargeException
	{
		for (AdhocEntity ent : entities)
		{
			if (ent instanceof AdhocChart)
			{
				AdhocChart ch = (AdhocChart) ent;
				if (ch.isTrellis())
					ch.adjustChartSizeForTrellis(this, jrds);
			}
		}
	}
	
	/**
	 * @return
	 */
	protected OMElement getSaveXMLDocument() {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		return getSaveXMLDocument(null, fac);
	}
	
	protected OMElement getSaveXMLDocument(OMNamespace ns, OMFactory fac) {
		OMElement root = fac.createOMElement(this.getClass().getName(), ns);
		if (createdBy != null)
			root.addAttribute(CREATED_BY, createdBy, ns);
		XmlUtils.addContent(fac, root, ADHOC_NAME, this.getName(), ns);
		XmlUtils.addContent(fac, root, ADHOC_PATH, this.getPath(), ns);
		XmlUtils.addContent(fac, root, NEXT_ID, this.nextId, ns);
		XmlUtils.addContent(fac, root, HIDE_CONTENT, this.hidedata, ns);
		XmlUtils.addContent(fac, root, REPLACE_NULL_TO_ZERO, this.isShowNullZero(), ns);
		XmlUtils.addContent(fac, root, REPLACE_NAN_TO_BLANK, this.isShowNaNBlank(), ns);
		XmlUtils.addContent(fac, root, REPLACE_INFINITE_TO_BLANK, this.isShowINFBlank(), ns);
		addEntityContent(fac, root, ENTITIES, this.getEntities(), ns);
		addSortContent(fac, root, this.getSorts(), ns);
		addQueryFilterContent(fac, root, QUERY_FILTERS, this.filters, ns); // because this.getFilters() returns the column filters also
		addDisplayFilterContent(fac, root, DISPLAY_FILTERS, this.getDisplayFilters(), ns);
		addReportFilterContent(fac, root, REPORT_FILTERS, this.getReportFilters(), ns);

		if (this.pi != null) {
			pi.addContent(fac, root, ns);
		}
		
//		XmlUtils.addContent(fac, root, TOP_N, this.getTopN(), ns);
		XmlUtils.addContent(fac, root, TOP_FILTER, topFilter, ns);
		XmlUtils.addContent(fac, root, CHART_OPTIONS, this.getChartOptions().toString(), ns);
		XmlUtils.addContent(fac, root, GRAND_TOTALS, this.isGrandtotals(), ns);
		XmlUtils.addContent(fac, root, INCLUDE_TABLE, this.isIncludeTable(), ns);
		XmlUtils.addContent(fac, root, PAGE_HEIGHT, this.getPageHeight(), ns);
		XmlUtils.addContent(fac, root, PAGE_WIDTH, this.getPageWidth(), ns);
		XmlUtils.addContent(fac, root, PAGE_ORIENTATION, this.getPageOrientation() == OrientationEnum.LANDSCAPE ? LANDSCAPE : "Portrait", ns);
		XmlUtils.addContent(fac, root, PAGE_SIZE, this.getPageSize(), ns);
		XmlUtils.addContent(fac, root, LEFT_MARGIN, this.getLeftMargin(), ns);
		XmlUtils.addContent(fac, root, RIGHT_MARGIN, this.getRightMargin(), ns);
		XmlUtils.addContent(fac, root, TOP_MARGIN, this.getTopMargin(), ns);
		XmlUtils.addContent(fac, root, BOTTOM_MARGIN, this.getBottomMargin(), ns);
		XmlUtils.addContent(fac, root, TITLE_HEIGHT, this.getTitleHeight(), ns);
		XmlUtils.addContent(fac, root, ROW_HEIGHT, this.getRowHeight(), ns);
		XmlUtils.addContent(fac, root, ALTERNATE_ROW_COLORS, this.isAlternateRowColors(), ns);
		XmlUtils.addContent(fac, root, FIRST_ALTERNATE_R_G_B, this.getFirstAlternateRGB(), ns);
		XmlUtils.addContent(fac, root, SECOND_ALTERNATE_R_G_B, this.getSecondAlternateAltRGB(), ns);
		XmlUtils.addContent(fac, root, BACKGROUND_COLOR, backgroundColor, ns);
		XmlUtils.addContent(fac, root, BACKGROUND_INVISIBLE, backgroundInvisible, ns);
		XmlUtils.addContent(fac, root, NUM_GROUPS, this.getNumGroups(), ns);
		XmlUtils.addContent(fac, root, CSVSEPARATOR, this.getSeparatorCSV(), ns);
		XmlUtils.addContent(fac, root, "CSV_Mimetype", this.getMimeTypeCSV(), ns);
		XmlUtils.addContent(fac, root, "CSV_Filename", this.getFilenameCSV(), ns);
		XmlUtils.addContent(fac, root, AUTOMATIC_LAYOUT, this.isAutomaticLayout(), ns);
		XmlUtils.addContent(fac, root, TITLE, this.getTitle(), ns);
		XmlUtils.addContent(fac, root, HIDE_TITLE, this.isHideTitle(), ns);
		XmlUtils.addContent(fac, root, DESCRIPTION, this.getDescription(), ns);
		XmlUtils.addContent(fac, root, DESCRIPTION_HYPERLINK, this.getDescriptionHyperlink(), ns);
		XmlUtils.addContent(fac, root, SHOW_FILTERS, showFilters, ns);
		XmlUtils.addContent(fac, root, FILTERS_FONT, filtersFontStr, ns);
		XmlUtils.addContent(fac, root, FILTERS_BACKGROUND, filtersBackground, ns);
		XmlUtils.addContent(fac, root, FILTERS_FOREGROUND, filtersForeground, ns);
		XmlUtils.addContent(fac, root, FILTERS_ALIGNMENT, AdhocEntity.encodeAlignment(filtersAlignment), ns);
		XmlUtils.addContent(fac, root, FILTERS_SHOW_IN_DASH, showReportFilterStrInDash, ns);
		XmlUtils.addContent(fac, root, FILTERS_SHOW_ON_ONE_LINE, filtersShowOnOneLine, ns);
		XmlUtils.addContent(fac, root, FILTERS_SKIP_ALL, filtersSkipAll, ns);
		XmlUtils.addContent(fac, root, DEFAULT_CHART_INDEX, defaultChartIndex, ns);
		XmlUtils.addContent(fac, root, USE_OLD_SIZING, useOldSizing, ns);
		XmlUtils.addContent(fac, root, USE_FLEX_GRID, useFlexGrid, ns);
		XmlUtils.addContent(fac, root, DO_NOT_STYLE_HYPERLINKS, doNotStyleHyperlinks, ns);
		XmlUtils.addContent(fac, root, ASYNCH_REPORT_GENERATION, isAsynchReportGeneration(), ns);
		XmlUtils.addContent(fac, root, OLAP_USE_INDENTATION, olapUseIndentation, ns);
		
		//Pivot table options
		getPivotTableOpts().addContent(fac, root, ns);

		return root;
	}
	
	private void addSortContent(OMFactory fac, OMElement parent,
			List<AdhocSort> sorts, OMNamespace ns) {
		if (sorts != null && sorts.size() > 0) {
			OMElement sortsParent = fac.createOMElement(SORTS, ns);
			OMElement sortEntitiesParent = fac.createOMElement(SORT_ENTITIES, ns);
			OMElement sortOrdersParent = fac.createOMElement(SORT_ORDERS, ns);
			for (AdhocSort as : sorts) {
				XmlUtils.addContent(fac, sortsParent, SORT, as, ns);
				
				OMElement el = fac.createOMElement(as.getColumn().getClass().getName(), ns);
				as.getColumn().addContent(fac, el, ns);
				sortEntitiesParent.addChild(el);
				
				OMElement c = fac.createOMElement("String", ns);
				OMText text = fac.createOMText(as.getOrder() == SortOrder.ASCENDING ? Boolean.TRUE.toString() : Boolean.FALSE.toString());
				c.addChild(text);
				sortOrdersParent.addChild(c);
			}
			parent.addChild(sortsParent);
			parent.addChild(sortEntitiesParent);
			parent.addChild(sortOrdersParent);
		}
	}

	protected void addEntityContent(OMFactory fac, OMElement parent, String name, List<AdhocEntity> entities, OMNamespace ns) {
		OMElement element = fac.createOMElement(name, ns);
		for (AdhocEntity entity : entities) {
			OMElement content = fac.createOMElement(entity.getClass().getName(), ns);
			entity.addContent(fac, content, ns);
			element.addChild(content);
		}
		parent.addChild(element);
	}
	
	protected void addQueryFilterContent(OMFactory fac, OMElement parent, String name, List<QueryFilter> filters, OMNamespace ns) {
		if (filters != null && filters.size() > 0) {
			OMElement el = fac.createOMElement(name, ns);
			com.successmetricsinc.UserBean ub = SMIWebUserBean.getUserBean();
			if (ub == null)
				ub = UserBean.getUserBean();
			for (QueryFilter filter : filters) {
				AdhocColumn col = getColumnByName(filter.getColumn());
				filter.addContent(fac, el, ns, ub.getRepository().getServerParameters().getProcessingTimeZone(), 
						ub.getDateFormat(col == null ? null : col.getFormat()), 
						ub.getDateTimeFormat(col == null ? null : col.getFormat()));
			}
			parent.addChild(el);
		}
	}
	
	protected void addDisplayFilterContent(OMFactory fac, OMElement parent, String name, List<DisplayFilter> filters, OMNamespace ns) {
		if (filters != null && filters.size() > 0) {
			OMElement el = fac.createOMElement(name, ns);
			com.successmetricsinc.UserBean ub = SMIWebUserBean.getUserBean();
			if (ub == null)
				ub = UserBean.getUserBean();
			for (DisplayFilter filter : filters) {
				AdhocColumn col = getColumnByDisplayFilter(filter);
				filter.addContent(fac, el, ns, ub.getRepository().getServerParameters().getProcessingTimeZone(), 
						ub.getDateFormat(col == null ? null : col.getFormat()), 
						ub.getDateTimeFormat(col == null ? null : col.getFormat()));
			}
			parent.addChild(el);
		}
	}
	
	private AdhocColumn getColumnByDisplayFilter(DisplayFilter filter) {
		/* XXX There exists old reports with old display filters
		* where getColumnPosition is in the range, but returns the wrong column
		* ignore this code path for now and retest
		if (filter.getColumnPosition() >= 0 && filter.getColumnPosition() < getColumns().size()) {
			return getColumns().get(filter.getColumnPosition());
		}
		*/
		return getColumnByName(filter.getColumn());
	}
	
	protected void addReportFilterContent(OMFactory fac, OMElement parent, String name, List<ReportFilter> filters, OMNamespace ns) {
		if (filters != null && filters.size() > 0) {
			OMElement el = fac.createOMElement(name, ns);
			for (ReportFilter filter : filters) {
				filter.addContent(fac, el, ns);
			}
			parent.addChild(el);
		}
	}	
	
	private void init() {
		entities = Collections.synchronizedList(new ArrayList<AdhocEntity>());
		columns = new ArrayList<AdhocColumn>();
		sortEntities = new ArrayList<AdhocColumn>();
		sortOrders = new ArrayList<Boolean>();
		sorts = new ArrayList<AdhocSort>();
		filters = new ArrayList<QueryFilter>();
		displayFilters = new ArrayList<DisplayFilter>();
		reportFilters = new ArrayList<ReportFilter>();
		name = "";
		path = "";
		topFilter = null;
		grandtotals = false;
		chartOptions = new ChartOptions();
		includeTable = true;
		alternateRowColors = true;
		showNaNBlank = true;
		showINFBlank = true;
		showNullZero = false;
		useOldSizing = false;
		rowHeight = PageInfo.DEFAULT_NEW_ROW_HEIGHT;
	}
	public AdhocReport()
	{
		init();
	}
	public AdhocReport(QuickReport qr, String navigateTo, Repository r) {
		init();
		
		this.setIncludeTable(qr.isShowTable());
		
		if (this.isIncludeTable() == false) {
			this.setPageSize("CUSTOM");
			this.setPageHeight(ChartOptions.DEFAULT_CHART_HEIGHT + 70);
			this.setPageWidth(ChartOptions.DEFAULT_CHART_WIDTH);
			this.setLeftMargin(0);
			this.setRightMargin(0);
			this.setTopMargin(0);
			this.setBottomMargin(0);
		}
		
		for (QuickReportDimension dim : qr.getDimensionColumns()) {
			this.addDimensionColumn(dim.getDimensionName(), dim.getColumnName(), null);
			AdhocColumn col = this.getLastColumn();
			if (col != null) {
				col.setDrillType(Chart.DRILL_TYPE_NAVIGATE);
				col.setDrillTo(navigateTo);
				if (dim.getOrderBy() > 0) {
					this.sorts.add(new AdhocSort(col, dim.getOrderBy() == QuickReport.SORT_ASC ? true : false));
				}
			}
		}
		
		for (QuickReport.QuickReportMeasure meas : qr.getMeasures()) {
			this.addMeasure(meas.getMeasureName(), meas.getMeasureLabel());
			
			if (meas.getOrderBy() > 0) {
				List<AdhocColumn> list = this.getColumns();
				AdhocColumn col = list.get(list.size() - 1);
				if (col != null) {
					this.sorts.add(new AdhocSort(col, meas.getOrderBy() == QuickReport.SORT_ASC ? true : false));
				}
			}
		}
		
		for (QuickReport.QuickReportExpression expr : qr.getExpressions()) {
			this.addExpression(expr.getFormula(), expr.getLabel(), true);
			AdhocColumn col = this.getLastColumn();
			if (col != null && col.getLabelField() != null) {
				col.getLabelField().setLabel(expr.getLabel());
			}
			if (expr.getOrderBy() > 0) {
				if (col != null) {
					this.sorts.add(new AdhocSort(col, expr.getOrderBy() == QuickReport.SORT_ASC ? true : false));
				}
			}
		}
		
		for (DisplayFilter displayFilter : qr.getDisplayFilters()) {
			String name = displayFilter.getColumnName();
			// now find the column associated with a label with this value
			for (AdhocColumn col : this.getColumns()) {
				AdhocTextEntity label = col.getLabelField();
				if (label != null && name.equals(label.getLabel())) {
					/*
					if (!r.isUseNewQueryLanguage() && col.getType() == AdhocColumn.ColumnType.Expression) {
						List<MeasureExpressionColumn> mec = col.getExpression().getMeasureExpressionColumns();
						if (mec != null && mec.size() > 0) {
							name = mec.get(0).getDisplayName();
						}
					}
					*/
					displayFilter.setColumnName(name);
					displayFilter.alias = col.getFieldName();
					// displayFilter.setPromptName(name);
					displayFilter.setDisplayName(true);
					break;
				}
			}
			this.addFilter(displayFilter);
		}
		String temp = qr.getChartType();
		if (temp != null) {
			ChartOptions.ChartType type = ChartOptions.ChartType.None;
			if ("Bar".equalsIgnoreCase(temp)) {
				type = ChartOptions.ChartType.Bar;
			}
			else if (COLUMN.equalsIgnoreCase(temp)) {
				type = ChartOptions.ChartType.Column;
			}
			else if ("Line".equalsIgnoreCase(temp)) {
				type = ChartOptions.ChartType.Line;
			}
			else if ("BarLine".equalsIgnoreCase(temp)) {
				type = ChartOptions.ChartType.BarLine;
			}
			else if ("Pie".equalsIgnoreCase(temp)) {
				type = ChartOptions.ChartType.Pie;
			}
			else if ("StackedBar".equalsIgnoreCase(temp)) {
				type = ChartOptions.ChartType.StackedBar;
			}
			else if ("StackedColumn".equalsIgnoreCase(temp)) {
				type = ChartOptions.ChartType.StackedColumn;
			}
			else if ("PercentStackedBar".equalsIgnoreCase(temp)) {
				type = ChartOptions.ChartType.PercentStackedBar;
			}
			else if ("PercentStackedColumn".equalsIgnoreCase(temp)) {
				type = ChartOptions.ChartType.PercentStackedColumn;
			}			
			else if ("Area".equalsIgnoreCase(temp)) {
				type = ChartOptions.ChartType.Area;
			}
			else if ("StackedArea".equalsIgnoreCase(temp)) {
				type = ChartOptions.ChartType.StackedArea;
			}
			else if ("Scatterplot".equalsIgnoreCase(temp)) {
				type = ChartOptions.ChartType.Scatterplot;
			}
			else if ("BubbleChart".equalsIgnoreCase(temp)) {
				type = ChartOptions.ChartType.BubbleChart;
			}
			else if ("ContourPlot".equalsIgnoreCase(temp)) {
				type = ChartOptions.ChartType.ContourPlot;
			}
			else if ("SpiderWebPlot".equalsIgnoreCase(temp)) {
				type = ChartOptions.ChartType.SpiderWebPlot;
			}
			else if ("MeterPlot".equalsIgnoreCase(temp)) {
				type = ChartOptions.ChartType.MeterPlot;
			}
			else if ("HeatMap".equalsIgnoreCase(temp)) {
				type = ChartOptions.ChartType.HeatMap;
			}
			else if ("StackedColumnLine".equalsIgnoreCase(temp)) {
				type = ChartOptions.ChartType.StackedColumnLine;
			}
			this.setChartType(type);
		}
	}
	private AdhocColumn getLastColumn() {
		List<AdhocColumn> list = this.getColumns();
		if (list != null && list.size() > 0) {
			return list.get(list.size() - 1);
		}
		return null;
	}
	private void setChartType(ChartOptions.ChartType type) {
		getChartOptions().type = type;
		if (type != ChartOptions.ChartType.Pie)
			getChartOptions().colorColumn = null;
		if (getShowGradients(type))
		{
			String gradient = getChartOptions().gradient;
			if (gradient == null || gradient.length() == 0) {
				getChartOptions().gradient = "3D";
			}
		}
		else
		{
			getChartOptions().gradient = null;
		}
	}
	public boolean getShowGradients(ChartType type)
	{
		if ((type == ChartOptions.ChartType.Pie) && getChartOptions().threeD) // gradients and 3D pies don't play well together
			return false;
		
		return
			(type == ChartOptions.ChartType.Bar) ||
			(type == ChartOptions.ChartType.Column) ||
			(type == ChartOptions.ChartType.Pie) ||
			(type == ChartOptions.ChartType.BarLine) ||
			(type == ChartOptions.ChartType.StackedBar) ||
			(type == ChartOptions.ChartType.StackedColumn) ||
			(type == ChartOptions.ChartType.PercentStackedBar) ||
			(type == ChartOptions.ChartType.PercentStackedColumn) ||			
			(type == ChartOptions.ChartType.StackedColumnLine);
	}
	
	private PageInfo getPageInfo() {
		if (pi == null) {
			this.pi = new PageInfo(this, 500, 500, false, false, 1, 0, 100, false, false);
		}
		return pi;
	}
	
	private PivotTableOptions getPivotTableOpts(){
		if (pivotOpts == null){
			pivotOpts = new PivotTableOptions();
		}
		return pivotOpts;
	}
	
	/**
	 * Add a new query filter to the report - make sure there are no duplicates
	 * 
	 * @param qf
	 */
	public void addFilter(QueryFilter qf)
	{
		boolean found = false;
		for (QueryFilter eqf : filters)
		{
			if (eqf.equals(qf))
			{
				found = true;
				break;
			}
		}
		if (!found)
			filters.add(qf);
	}
	
	/**
	 * Add a new display filter to the report - make sure there are no duplicates
	 * 
	 * @param qf
	 */
	public void addFilter(DisplayFilter df)
	{
		boolean found = false;
		for (DisplayFilter edf : displayFilters)
		{
			if (edf.equals(df))
			{
				found = true;
				break;
			}
		}
		if (!found)
			displayFilters.add(df);
	}
	
	/**
	 * Add a new report filter to the report - make sure there are no duplicates
	 * 
	 * @param qf
	 */
	public void addFilter(ReportFilter qf)
	{
		boolean found = false;
		for (ReportFilter eqf : reportFilters)
		{
			if (eqf.equals(qf))
			{
				found = true;
				break;
			}
		}
		if (!found)
			reportFilters.add(qf);
	}

	/**
	 * Add a measure to the report
	 * 
	 * @param measureName
	 */
	public int addMeasure(String measureName, String displayName)
	{
		AdhocColumn rc = new AdhocColumn(measureName);
		addAdhocColumn(rc, displayName);
		return rc.reportIndex;
	}
	
	public AdhocColumn addExpression(String expr, String label, boolean addColumn) {
		AdhocColumn rc = new AdhocColumn();
		String name = label;
		int index = expr.indexOf('{');
		if (index > 0 && index + 1 < expr.length()) {
			name = expr.substring(index + 1);
			index = name.indexOf(',');
			if (index > 0) {
				name = name.substring(0, index);
			}
			else {
				index = name.indexOf('}');
				if (index > 0) {
					name = name.substring(0, index);
				}
			}
		}
		name = getUniqueName(name);
		try {
			rc.setType(AdhocColumn.ColumnType.Expression);
			rc.setName(name);
			rc.setDimension("EXPR");
			rc.setColumn(name);
			rc.setOriginalColumn(name);
			if (addColumn)
				addAdhocColumn(rc, label);
			DisplayExpression de = new DisplayExpression();
			try {
				Repository r = SMIPojoBase.getRepository();
				if (r != null) {
					de = new DisplayExpression(r, expr, SMIWebUserBean.getUserBean().getRepSession(true));
				}
			}
			catch (Exception e) {
				de.setExpression(expr);
			}
			rc.setExpression(de);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e, e);
		}
		
		return rc;
	}
	
	/**
	 * Add a dimension column to the report
	 * 
	 * @param measureName
	 */
	public int addDimensionColumn(String dimension, String column, String drillFromColumn){
		return addDimensionColumn(dimension, null, column, null, column, drillFromColumn, false);
	}
	
	/**
	 * Add a dimension column to the report
	 * 
	 * @param measureName
	 */
	public int addDimensionColumn(String dimension, String hierarchy, String column, String techName,String displayName, String drillFromColumn, boolean isPivot)
	{
		if (dimension != null && hierarchy != null && hierarchy.length() > 0) {
			// OLAP column
			if (column.indexOf('\'') == 0)
				column = column.substring(1);
			if (column.lastIndexOf('\'') == column.length() - 1)
				column = column.substring(0, column.length() - 1);
			// see if the same Dimension & Hierarchy exist in an existing column..
			// if so, add this column to that one.
			for (AdhocColumn ac : columns) {
				if (dimension.equals(ac.getDimension()) && hierarchy.equals(ac.getHierarchy())) {
					ac.addAdditionalOlapColumn(column, techName);
					return ac.getReportIndex();
				}
			}
		}
		AdhocColumn rc = new AdhocColumn(dimension, hierarchy, column, techName, "Varchar", null);
		if (isPivot){
			rc.setPivotColumn(true);
		}
		AdhocColumn fromCol = null;
		if (drillFromColumn != null)
		{
			for (AdhocColumn reportColumn : columns)
			{
				if (reportColumn.getName().equals(drillFromColumn) && (reportColumn.isPivotColumn()))
				{
					rc.setPivotColumn(true);
					fromCol = reportColumn;
					break;
				}
			}
		}
		addAdhocColumn(rc, displayName, fromCol);
		//addLabelBordersIfIsPivotTable(rc);
		return rc.getReportIndex();
	}
	
	private void addAdhocColumn(AdhocColumn column, String displayName) {
		addAdhocColumn(column, displayName, null);
	}
	private void addAdhocColumn(AdhocColumn column, String displayName, AdhocColumn copyOf) {
		if (this.isAutomaticLayout()) {
			// do this now so that the layout works right.
			column.setBand(Band.Detail);
		}
		
		if (copyOf == null)
			copyOf = findLastAdhocColumnOfType(column);
		if (copyOf != null)
			copyOf.copyDisplayAttributes(column);
		this.addEntity(column);
		
		if (useOldSizing == false) {
			if (column.getWidth() == PageInfo.DEFAULT_NUMBER_WIDTH)
				column.setWidth(PageInfo.DEFAULT_NEW_NUMBER_WIDTH);
			
			column.setHeight(PageInfo.DEFAULT_NEW_ROW_HEIGHT);
		}
		
		String name = getUniqueColumnDisplayName(displayName);
		AdhocTextEntity label = new AdhocLabel(name);
//		column.setName(name);
		
		SMIWebUserBean user = SMIWebUserBean.getUserBean();
		Repository repository = user.getRepository();
		
		// check for numeric label
		boolean isNumericLabel = true;
		for(int i=0; i < name.length();  i++ ) {
			if(Character.isDigit(name.charAt(i))) {
				continue;
			}
			if(name.charAt(i) == '_'){
					continue;
			}
			isNumericLabel = false;
		}
			
		if(!isNumericLabel){
			// check to see if displayName is actually an expression
		    try {
			    // throws an error if just a regular label 
			    @SuppressWarnings("unused")
				DisplayExpression de = new DisplayExpression(repository, displayName, user.getRepSession(true));
				label = addExpression(displayName, name, false);
			} catch (Exception e) {
				// do nothing
			}
		}
		// add header text
		// validate uniqueness
		if (this.isAutomaticLayout()) {
			label.setBand(Band.Header);
		}
		
//		label.setBackground(DEFAULT_HEAD_BACK_COLOR);
		label.setAlignment(column.getAlignment());
		column.setLabelField(label);
		if (copyOf != null && copyOf.getLabelField() != null) {
			copyOf.getLabelField().copyDisplayAttributes(label);
		}
		label.setWidth(column.getWidth());
		label.setHeight(column.getHeight());
		this.addEntity(label);
		
		columns.add(column);
	}
	
	private AdhocColumn findLastAdhocColumnOfType(AdhocColumn col) {
		if (columns != null && columns.size() > 0) {
			AdhocColumn defaultColumn = null;
			boolean isColDimension = col.getType() == ColumnType.Dimension;
			if (col.getType() == ColumnType.Expression && col.isMeasureExpression() == false)
				isColDimension = true;
			for (int i = columns.size(); i > 0; i--) {
				AdhocColumn ac = columns.get(i - 1);
				if (col.getBand() != ac.getBand())
					continue;
				
				if (defaultColumn == null)
					defaultColumn = ac;
				
				boolean isAcDimension = ac.getType() == ColumnType.Dimension;
				if (ac.getType() == ColumnType.Expression && ac.isMeasureExpression() == false)
					isAcDimension = true;
				if (isColDimension == isAcDimension) {
					// all measures work the same
					if (isColDimension == false)
						return ac;
					
					// if its an expression - check the pivoting
					if (ac.isPivotColumn() == col.isPivotColumn())
						return ac;
				}
			}
			
			// didn't find an exact match
			// return the last column by default
			return defaultColumn;
		}
		return null;
	}

	private boolean isUniqueColumnDisplayName(String name)
	{
		// does it already exist?
		for (AdhocColumn col : columns)
		{
			AdhocTextEntity ate = col.getLabelField();
			if (ate != null && name.equals(ate.getLabel()))
			{
				return false;
			}
		}
		return true;
	}
	
	private boolean isUniqueName(String name) {
		// does it already exist?
		for (AdhocEntity col : entities)
		{
			if ((col instanceof AdhocColumn) && name.equals(((AdhocColumn)col).getName()))
			{
				return false;
			}
		}
		return true;
	}
	
	/**
	 * generate a unique column display name - for charting we need unique names
	 * @param name
	 * @return
	 */
	public String getUniqueColumnDisplayName(String name)
	{
		if (isUniqueColumnDisplayName(name))
			return name;
		int i = 2;
		String nm = name + '_' + i;
		while (!isUniqueColumnDisplayName(nm))
		{
			i++;
			nm = name + '_' + i;
		}
		return nm;
	}
	
	public String getUniqueName(String name) {
		if (isUniqueName(name))
			return name;
		int i = 2;
		String nm = name + '_' + i;
		while (!isUniqueName(nm))
		{
			i++;
			nm = name + '_' + i;
		}
		return nm;
	}

	public void removeEntity(AdhocEntity entity) {
		if (this.entities != null) {
			this.entities.remove(entity);
			if (this.columns != null) {
				int index = this.columns.indexOf(entity);
				if (index >= 0) {
					removeColumn(index);
					if (entity instanceof AdhocColumn && ((AdhocColumn)entity).getLabelField() != null) {
						removeEntity(((AdhocColumn)entity).getLabelField());
					}
				}
				for (AdhocColumn column : this.columns) {
					if (column.getLabelField() == entity) {
						column.setLabelField(null);
					}
				}
			}
		}
	}
	/**
	 * Remove a column from a report (by index)
	 * 
	 * @param dimension
	 * @param column
	 */
	public void removeColumn(int index)
	{
		AdhocColumn rc = columns.get(index);
		columns.remove(rc);
	}

	
	/**
	 * Organize the columns such the dimensions are first and measures follow
	 * 
	 */
	public void organizeColumns()
	{
		List<AdhocColumn> attributes = new ArrayList<AdhocColumn>();
		List<AdhocColumn> measures = new ArrayList<AdhocColumn>();
		List<AdhocColumn> copy = new ArrayList<AdhocColumn>(columns);
		for (AdhocColumn rc : copy)
		{
			if (!rc.isShowInReport()) // || isLabel(rc))
				continue;
			if (rc.treatAsDimension())
				attributes.add(rc);
			else if (rc.treatAsMeasure())
				measures.add(rc);
		}
		copy.clear();
		List<AdhocEntity> allEntities = this.getEntities();
		this.entities = new ArrayList<AdhocEntity>();
		for (AdhocColumn rc : attributes) {
			copy.add(rc);
			if (isLabel(rc))
				continue;
			
			this.addEntity(rc);
			AdhocTextEntity label = rc.getLabelField();
			if (label != null && ! entities.contains(label)) {
				entities.add(label);
			}
		}
		for (AdhocColumn rc : measures) {
			copy.add(rc);
			if (isLabel(rc))
				continue;
			this.addEntity(rc);
			AdhocTextEntity label = rc.getLabelField();
			if (label != null && ! entities.contains(label)) {
				entities.add(label);
			}
		}
		for (AdhocEntity element : allEntities) {
			if (! entities.contains(element)) {
				this.addEntity(element);
				if (element instanceof AdhocColumn) {
					copy.add((AdhocColumn) element);
				}
			}
		}
		columns.clear();
		columns.addAll(copy);
	}
	
	private boolean isLabel(AdhocColumn label) {
		for (AdhocColumn ac: columns) {
			AdhocTextEntity ate = ac.getLabelField();
			if (ate != null && ate instanceof AdhocColumn && ((AdhocColumn)ate) == label)
				return true;
		}
		return false;
	}
	
	public void addEntity(AdhocEntity entity) {
		if (this.entities == null) {
			this.entities = new ArrayList<AdhocEntity>();
		}
		if (entity.reportIndex < 0) {
			entity.reportIndex = nextId++;
		}
		boolean exists = false;
		for (AdhocEntity ae : entities) {
			if (ae.reportIndex == entity.reportIndex) {
				exists = true;
				break;
			}
		}
		if (! exists)
			this.entities.add(entity);
		
		if (entity.getLeft() == 0 && 
				(this.isAutomaticLayout() == true)) { // || ! (entity instanceof AdhocColumn))) {
			// need to place this entity somewhere
			Band band = entity.getBand();
			int xPos = 0;
			for (AdhocEntity e : this.getEntities()) {
				if (entity.equals(e)) {
					continue;
				}
				if (e.getBand() != band) {
					continue;
				}
				xPos = Math.max(xPos, e.getLeft() + e.getWidth());
			}
			entity.setLeft(xPos);
		}
	}
	public Query getQuery(Repository r) throws NavigationException, BadColumnNameException, NullPointerException, SyntaxErrorException, CloneNotSupportedException
	{
		return getQuery(r, null);
	}
	
	public Query getQuery(Repository r, String selectorsXml) throws NavigationException, BadColumnNameException, NullPointerException, SyntaxErrorException, CloneNotSupportedException
	{
		UserBean ub = UserBean.getUserBean();
		Session session = ub.getRepSession();
		expressionList = new ArrayList<DisplayExpression>();
		Query q = new Query(r);
		if(columns == null || columns.isEmpty())
		{
			return null;
		}
		Map<String, ColumnSelector> selectors = null;
		if (selectorsXml != null && Util.hasNonWhiteSpaceCharacters(selectorsXml)) {
			 selectors = parseColumnSelectors(selectorsXml);
		}
		
		List<String> leafNames = new ArrayList<String>();
		List<AdhocSort> sortColumns = this.getSorts();
		
		for (AdhocColumn rc : columns)
		{
			String label = rc.getLabelString();
			ColumnSelector cs = selectors == null ? null : selectors.get(label);
			AdhocColumn colToUse = cs == null ? rc : cs.getAdhocColumn();
			if (colToUse.getType() == AdhocColumn.ColumnType.Dimension)
			{
				addDimension(null, q, colToUse, rc.getFieldName());
				if (Chart.DRILL_TYPE_DRILL.equals(rc.getDrillType())) {
					Level lvl = r.findDimensionLevelColumn(colToUse.getDimension(), colToUse.getColumn());
					if (lvl != null) {
						String isLeafName = "Birst" + lvl.Name + "IsLeaf";
						if (!leafNames.contains(isLeafName)) {
							leafNames.add(isLeafName);
							DimensionColumn dc = r.findDimensionColumn(colToUse.getDimension(), isLeafName);
							if (dc != null) {
								// add isLeafName
								q.addDimensionColumn(colToUse.getDimension(), isLeafName, isLeafName);
								q.addGroupBy(new GroupBy(colToUse.getDimension(), isLeafName));
								
								rc.setupConditionalFormatForLeafNode(this, r, isLeafName);
							}
						}
					}
				}
				// add pivot sort field
				addPivotSortFields(sortColumns, rc, q, selectors);
			} else if (colToUse.getType() == AdhocColumn.ColumnType.Measure)
			{
				addMeasureColumn(null, q, colToUse, rc.getFieldName());
			} else if (colToUse.getType() == AdhocColumn.ColumnType.Expression)
			{
				DisplayExpression de = colToUse.getExpression();
				if (de == null && colToUse.getExpressionString() != null) {
					String expression = colToUse.getExpressionString();
					if (expression.toUpperCase().indexOf(LogicalQueryString.BEGINMACRO) >= 0) {
						try {
						// assume new query syntax if BEGINMACRO is in the expression
							de = new DisplayExpression(r, expression, session);
							colToUse.setExpression(de);
							if (de.isColumn()) {
								String dim = de.getColumnDimension();
								String col = de.getColumnName();
								if (dim != null && Util.hasNonWhiteSpaceCharacters(dim)) {
									q.addDimensionColumn(dim, col, rc.getFieldName());
									q.addGroupBy(new GroupBy(dim, col));
								}
								else {
									q.addMeasureColumn(col, rc.getFieldName(), false, null);
								}
								de = null;
							}
						}
						catch (Exception e) {
							logger.error("Error evaluating expression", e);
						}
					}
				}
				if (de != null)
				{
					de.setName(rc.getFieldName());
					expressionList.add(de);
					// add to end of column list.
					de.setPosition(-1);
					if (rc.treatAsDimension()) {
						addPivotSortFields(sortColumns, rc, q, selectors);
					}
					
					if (rc.isOlapDimension()) {
						addDimension(null, q, colToUse, rc.getFieldName() + "_Orig");
					}
				}
			}
			else if (colToUse.getType() == ColumnType.Ratio) {
				StringBuilder fieldName = new StringBuilder();
				fieldName.append('F').append(rc.getReportIndex()); //.append('_');
				String name = fieldName.toString();
				
				DisplayExpression de = colToUse.getExpression();
				if (de != null) {
					de.setName(name + BirstRatio.NUMERATOR);
					expressionList.add(de);
					de.setPosition(-1);
				}
				else {
					// assume measure
					addMeasureColumn(null, q, colToUse, name + BirstRatio.NUMERATOR);
				}
				
				de = colToUse.getDenominatorInRatio();
				if (de != null) {
					de.setName(name + BirstRatio.DENOMINATOR);
					expressionList.add(de);
					de.setPosition(-1);
				}
			}
			List<DisplayExpression> formatList = colToUse.getFormatExpressions();
			if (formatList != null)
				expressionList.addAll(formatList);
		}
		
		// add pivot totals
		/*
		 * For conditional formatting expressions, column position isn't set, so add to end of colums
		 */
		int colIndex = columns.size();
		for (DisplayExpression de : expressionList)
		{
			if (de.getPosition() < 0)
			{
				de.setPosition(colIndex++);
			}
		}
		
		/* 
		 * First iterate sortEntries as it may require to add non-navigateOnly column to query.
		 */
		if (sortColumns != null) {
			for (Iterator<AdhocSort> iter = sortColumns.iterator(); iter.hasNext(); ) {
				AdhocSort as = iter.next();
				AdhocColumn ac = as.getColumn();
				if (ac.getType() == ColumnType.Ratio)
					continue;
				
				AdhocColumn colToUse = getSortColumn(as, selectors);
				
				if (ac.getType() == ColumnType.Dimension)
				{
					if(!q.containsDimensionColumn(colToUse.getDimension(), colToUse.getColumn()) && !colToUse.isOlapDimension())
					{
						q.addDimensionColumn(colToUse.getDimension(), colToUse.getColumn());
						q.addGroupBy(new GroupBy(colToUse.getDimension(), colToUse.getColumn()));
					}
					if (as.getType() == SortType.DATA) {
						if (colToUse.isOlapDimension()) {
							// find this column - if its now an expression remove sort
							boolean addSort = true;
							AdhocEntity ae = getEntityById(colToUse.reportIndex);
							if (ae == null || ! (ae instanceof AdhocColumn) || ((AdhocColumn)ae).getType() == ColumnType.Expression)
								addSort = false;
							if (addSort) {
								OrderBy ob = new OrderBy(colToUse.getDimension(), colToUse.getColumn(), ac.getFieldName(), (as.getOrder() == SortOrder.ASCENDING ? true : false));
								q.addOrderBy(ob);
							}
							else {
								iter.remove();
							}
						}
						else {
							OrderBy ob = new OrderBy(colToUse.getDimension(), colToUse.getColumn(), (as.getOrder() == SortOrder.ASCENDING ? true : false));
							q.addOrderBy(ob);
						}
					}
					else {
						DisplayOrder dispO = new DisplayOrder(colToUse.getDimension(), colToUse.getColumn(), (as.getOrder() == SortOrder.ASCENDING ? true : false));
						q.addDisplayBy(dispO);
					}
				} else if (ac.getType() == ColumnType.Measure) 
				{
					if(!q.containsMeasureColumn(colToUse.getColumn()))
					{
						q.addMeasureColumn(colToUse.getColumn(), colToUse.getColumn(), false);
					}
					if (as.getType() == SortType.DATA) {
						OrderBy ob = new OrderBy(colToUse.getColumn(), (as.getOrder() == SortOrder.ASCENDING ? true : false));
						q.addOrderBy(ob);
					}
					else {
						DisplayOrder dispO = new DisplayOrder(colToUse.getDimension(), ac.getColumn(), (as.getOrder() == SortOrder.ASCENDING ? true : false));
						q.addDisplayBy(dispO);
					}
				}
			}
		}
		/*
		 * Iterate on filters and add navigate only column if requires
		 */
		List<QueryFilter> processedFilters = this.getProcessedFilters(filters);
		if (processedFilters != null)
		{
			for (QueryFilter qf: processedFilters)
			{
				boolean extractNavigationColumn = true;
				if (qf.isPromptedFilterWithNoValue() || qf.containsVirtualColumn(r))
					extractNavigationColumn = false;
				q.addFilter(qf, extractNavigationColumn);
			}
		}
		if (reportFilters != null)
		{
			for (ReportFilter rf : reportFilters)
			{
				try
				{
					QueryFilter qf = rf.getQueryFilter();
					if (qf == null)
						continue;
					q.addFilter(qf);
				}
				catch (Exception ex)
				{
					logger.info(ex.getMessage());
				}
			}
		}
		if (topFilter != null)
		{
			int topN = -1;
			if (topFilter.isPrompted()) {
				Filter f = Session.getFilter(topFilter.getPromptName());
				try {
					List<String> values = f.getValues();
					if (values != null && values.size() > 0)
						topN = Integer.valueOf(values.get(0));
				}
				catch (Exception e) { }
			}
			else if (topFilter.getOperand() != null) {
				if (r.isUseNewQueryLanguage() == true) {
					try {
						ANTLRStringStream stream = new ANTLRStringStream(topFilter.getOperand().toString());
						BirstScriptLexer lex = new BirstScriptLexer(stream);
						CommonTokenStream cts = new CommonTokenStream(lex);
						BirstScriptParser parser = new BirstScriptParser(cts);
						logicalExpression_return logicalExp = parser.logicalExpression();
						Tree tree = (Tree)logicalExp.getTree();
						
						TransformationScript ts = new TransformationScript(r, ub.getRepSession());
						Expression ex = new Expression(ts);
						Operator op = ex.compile(tree);
						Object value = op.evaluate();
						if (value != null && value instanceof Number) {
							q.setTopn(((Number)value).intValue());
						}
					}
					catch (Exception e) {}
					
				}
				else if (ub != null){
					String filterString = r.replaceVariables(session, topFilter.getOperand().toString());
					try {
						topN = Integer.valueOf(filterString);
					}
					catch (Exception e) { }
				}
			}
			
			if (topN >= 0)
			q.addTopN(topN);
		}
		return (q);
	}
	
	private void addPivotSortFields(List<AdhocSort> sortColumns, AdhocColumn rc, Query q, Map<String, ColumnSelector> selectors) throws NavigationException {
		if (isPivot() && sortColumns != null) {
			int index = 0;
			for (Iterator<AdhocSort> iter = sortColumns.iterator(); iter.hasNext(); ) {
				AdhocSort as = iter.next();
				AdhocColumn ac = as.getColumn();
				
				AdhocEntity ae = this.getEntityById(ac.getReportIndex());
				if (ae == null || ! (ae instanceof AdhocColumn))
					continue;
				ac = (AdhocColumn)ae;
				if (ac.getType() == ColumnType.Ratio || ac.treatAsMeasure())
					continue;
				if (rc.isShowInReport() == false)
					continue;
				if (ac.isPivotColumn() != rc.isPivotColumn())
					continue;
				
				AdhocColumn selColToSort = getSortColumn(as, selectors);
				
				if (ac.getType() == ColumnType.Expression) {
					addExpression(q, selColToSort, rc.getFieldName() + SortablePivotField.PIVOT_SORT + index);
				}
				else {
					addDimension(null, q, selColToSort, rc.getFieldName() + SortablePivotField.PIVOT_SORT + index);
				}
				index++;
				rc.setSortablePivotField(true);
				
				if (ac.getReportIndex() == rc.getReportIndex())
					break;
			}
		}
	}

	private AdhocColumn getSortColumn(AdhocSort as, Map<String, ColumnSelector> selectors) {
		AdhocColumn ac = as.getColumn();
		
		// find column selector if exists
		ColumnSelector cs = null;
		if (selectors != null) {
			for (ColumnSelector csTemp : selectors.values()) {
				AdhocColumn acTemp = csTemp.getOriginalColumn();
				if (ac.getType() != acTemp.getType())
					continue;
				
				if (ac.getType() == ColumnType.Dimension) {
					if (ac.getDimension().equals(acTemp.getDimension()) && ac.getColumn().equals(acTemp.getColumn())) {
						cs = csTemp;
						break;
					}
				}
				else if (ac.getType() == ColumnType.Measure && ac.getColumn().equals(acTemp.getColumn())) {
					cs = csTemp;
					break;
				}
			}
		}
		return (cs == null ? ac : cs.getAdhocColumn());

		
	}
	
	private void addMeasureColumn(ColumnSelector cs, Query q, AdhocColumn rc, String alias) throws NavigationException, BadColumnNameException {
		if (cs != null) {
			addMeasureColumn(null, q, cs.getAdhocColumn(), alias);
			return;
		}
		else {
			String measure = rc.getColumn();
			if (measure == null)
				measure = rc.getColumn();
			List<QueryFilter> measureFilters = rc.getFilters();
			if (measureFilters != null && measureFilters.size() > 1)
				q.addMeasureColumn(measure, alias, false, 
						new QueryFilter(determineMeasureFilterExpression(measureFilters), measureFilters));
			else if (measureFilters != null && measureFilters.size() == 1)
				q.addMeasureColumn(measure, alias, false, measureFilters.get(0));
			else
				q.addMeasureColumn(measure, alias, false, null);
		}
	}

	private void addDimension(ColumnSelector cs, Query q, AdhocColumn rc, String alias) throws NavigationException {
		if (rc.isOlapDimension()) {
			List<MemberSelectionExpression> memberExpressions = rc.getOlapMemberExpressionMap();
			String dname = rc.getDimension() + "-" + rc.getHierarchy();
			String fieldName = alias;
			String dataType = rc.getDataType();
			String format = rc.getSourceFormat();
			DimensionMemberSetNav dms = q.addDimensionMemberSet(dname, memberExpressions, fieldName, dataType, format);
			
			// add parent name and ordinal now
			q.addDimMemberSetParent(dms, fieldName + BirstOlapField.PARENT);
			q.addDimMemberSetParentName(dms, fieldName + BirstOlapField.PARENTNAME);
			q.addDimMemberSetOrdinal(dms, fieldName + BirstOlapField.ORDINAL);
			q.addDimMemberSetUniqueName(dms, fieldName + BirstOlapField.UNIQUENAME);
		}
		else if (cs != null) {
			addDimension(null, q, cs.getAdhocColumn(), alias);
		}
		else {
			q.addDimensionColumn(rc.getDimension(), rc.getColumn(), alias);
			q.addGroupBy(new GroupBy(rc.getDimension(), rc.getColumn()));
		}
	}
	
	private void addExpression(Query q, AdhocColumn rc, String alias) {
		DisplayExpression de = rc.getExpression();
		if (de != null)
		{
			de = (DisplayExpression) de.clone();
			de.setName(alias);
			expressionList.add(de);
			// add to end of column list.
			de.setPosition(-1);
		}
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, ColumnSelector> parseColumnSelectors(String selectorXml) {
		Map<String, ColumnSelector> ret = new HashMap<String, ColumnSelector>();
		XMLStreamReader parser;
		try {
			XMLInputFactory inputFactory = XmlUtils.getSecureXMLInputFactory();
			parser = inputFactory.createXMLStreamReader(new StringReader(selectorXml));

			StAXOMBuilder builder = new StAXOMBuilder(parser);
			OMElement doc = null;
			doc = builder.getDocumentElement();
			doc.build();
			doc.detach();
			for (Iterator<OMElement> iter = doc.getChildElements(); iter.hasNext(); ) {
				OMElement el = iter.next();
				if (el.getLocalName().equals(AdhocReport.CHART_TYPE)) {
//					AdhocChart chart = getDefaultChart();
				}
				else if (el.getLocalName().equals(AdhocReport.COLUMN_SELECTORS)) {
					String col = null;
					String sel = null;
					AdhocColumn originalColumn = null;
					for (Iterator<OMElement> iter2 = el.getChildElements(); iter2.hasNext(); ) {
						OMElement el2 = iter2.next();
						String nm = el2.getLocalName();
						if (AdhocReport.COLUMN.equals(nm)) {
							col = XmlUtils.getStringContent(el2);
						}
						else if (AdhocReport.SELECTED.equals(nm)) {
							sel = XmlUtils.getStringContent(el2);
						}
						else if (ColumnSelector.COLUMN_SELECTOR.equals(nm)) {
							ColumnSelector cs = new ColumnSelector();
							cs.parseElement(el2);
							if (originalColumn == null)
								originalColumn = cs.getAdhocColumn();
							String column = cs.getID();
							if (sel.equals(column)) {
								cs.setOriginalColumn(originalColumn);
								ret.put(col, cs);
								break;
							}
						}
					}
				}
			}
		} catch (XMLStreamException e) {
			logger.error(e, e);
		} catch (FactoryConfigurationError e) {
			logger.error(e, e);
		}
		return ret;
	}
	
	private String determineMeasureFilterExpression(
			List<QueryFilter> measureFilters) {
		/*
		 * see bug 7445
		  Here are the rules:

			1. If combining two FDCs on different columns, use FAND
			2. If combining two FDCs on the same column
			   a) If one or both FDCs use the = or the LIKE operator, combine by FOR
			   b) If one or both FDCs use the >,<,>=,<= or <> operator, combine by FAND

		 */
		
		String column = null;
		String operator = null;
		
		for (QueryFilter qf : measureFilters) {
			if (column == null) {
				column = qf.getDimension() + "." + qf.getColumn();
				operator = qf.getOperator();
				continue;
			}
			if (column.equals(qf.getDimension() + "." + qf.getColumn())) {
				// same column
				if (operator.equals("=") || operator.equals(QueryFilter.LIKE)) {
					return "OR";
				}
			}
			return "AND";
		}
		return "OR";
	}

	public List<QueryFilter> getProcessedFilters(List<QueryFilter> filters) {
		List<QueryFilter> processedFilters = new ArrayList<QueryFilter>();
		List<QueryFilter> returnedFilters = new ArrayList<QueryFilter>();
		for (int i = 0; i < filters.size(); i++)
		{
			QueryFilter qf = (QueryFilter) filters.get(i);
			
			if (qf.getType() == QueryFilter.TYPE_INPUT_TEXT) {
				returnedFilters.add(qf);
			}
			else if (qf.getColumnType() == QueryFilter.TYPE_MEASURE && qf.getType() == QueryFilter.TYPE_PREDICATE)
			{
				if(!processedFilters.contains(qf))
				{
					String measure = qf.getColumn();
					String logicalOperator = getLogicalOperator(filters, i);
					for (int j = i + 1; j < filters.size(); j++)
					{
						QueryFilter qf2 = (QueryFilter) filters.get(j);
						if (qf2.containsMeasure(measure))
						{
							qf = new QueryFilter(qf, logicalOperator, qf2);
							processedFilters.add(qf2);
						}
					}
					returnedFilters.add(qf);
				}
			} else
			{
				if (!processedFilters.contains(qf))
				{
					if (qf.getOperator().equals("=") || qf.getOperator().equalsIgnoreCase(QueryFilter.LIKE) || qf.getOperator().equalsIgnoreCase(QueryFilter.ISNULL))
					{
						String dim = qf.getDimension();
						String col = qf.getColumn();
						
						if (!qf.isUnaryOperator() && (qf.getOperand() == null || "null".equals(qf.getOperand()))) {
							// special case - create an OR of both = "" and ISNULL
							QueryFilter qfNull = new QueryFilter(dim, col, QueryFilter.ISNULL, (String)null);
							if (qf.dataType != null && qf.dataType.equalsIgnoreCase("Varchar"))
							{
								QueryFilter qfEmpty = new QueryFilter(dim, col, qf.getOperator(), "");
								QueryFilter qfCombined = new QueryFilter(qfNull, "OR", qfEmpty);
								qf = qfCombined;
							}
							else
							{
								qf = qfNull;
							}
						}
						/*
						 * Look for implicit ors - i.e. other filters on same dimension and column with an equal or like
						 */
						for (int j = i + 1; j < filters.size(); j++)
						{
							QueryFilter qf2 = (QueryFilter) filters.get(j);
							if ((qf2.getColumnType() == QueryFilter.TYPE_DIMENSION_COLUMN) && qf2.containsAttribute(dim, col) && 
									(qf2.getOperator().equals("=") || qf2.getOperator().equalsIgnoreCase(QueryFilter.LIKE ) || (qf2.getOperator().equalsIgnoreCase(QueryFilter.ISNULL ))))
							{
								if ((qf2.getOperand() == null || "null".equals(qf2.getOperand())) && (!qf2.isUnaryOperator())){
									// special case - create an OR of both = "" and ISNULL
									QueryFilter qfNull = new QueryFilter(dim, col, QueryFilter.ISNULL, (String)null);
									qf = new QueryFilter(qf, "OR", qfNull);
									if (qf2.dataType != null && qf2.dataType.equalsIgnoreCase("Varchar"))
									{
										QueryFilter qfEmpty = new QueryFilter(dim, col, qf2.getOperator(), "");
										qf = new QueryFilter(qf, "OR", qfEmpty);
									}
								}
								else
									qf = new QueryFilter(qf, "OR", qf2);
								processedFilters.add(qf2);
							}
						}
					}
					returnedFilters.add(qf);
				}
			}
		}
		return returnedFilters;
	}

	/**
	 * For multiple filters defined on a given measure column, this method determines if the filters should be ORed or ANDed.
	 * The method applies following rules for determination.
	 * col >/>= x; col </<= y: if x < y then combine filters by AND else use OR
	 * col >/>= x; col >/>= y: combine by AND
	 * col </<= x; col </<= y: combine by AND
	 * col = x; col >/>=/</<=: combine by OR
	 * @param filters List of all filters for a given query
	 * @param index Index for the filter that needs to be compared with the rest in the list
	 * @return logical operator that the filters should be combined with
	 */
	private String getLogicalOperator(List<QueryFilter> filters, int index)
	{
		String logicalOperator = "AND";
		QueryFilter qf = filters.get(index);
		String measure = qf.getColumn();
		for (int j = index + 1; j < filters.size(); j++)
		{
			QueryFilter qf2 = (QueryFilter) filters.get(j);
			if (qf2.containsMeasure(measure))
			{
				String operator1 = qf.getOperator();
				String operator2 = qf2.getOperator();
				Double operand1 = null;
				Double operand2 = null;
				try
				{
					String o = qf.getOperand();
					if (o != null)
						operand1 = Double.valueOf(o);
					o = qf2.getOperand();
					if (o != null)
						operand2 = Double.valueOf(o);
				}
				catch(NumberFormatException nfe)
				{
					operand1 = null;
					operand2 = null;
				}
				if(operator1.equals("=") && ((operator2.equals(">")) || (operator2.equals(">=")) || (operator2.equals("<")) || (operator2.equals("<="))))
				{
					logicalOperator = "OR";
					break;
				}
				if(operator2.equals("=") && ((operator1.equals(">")) || (operator1.equals(">=")) || (operator1.equals("<")) || (operator1.equals("<="))))
				{
					logicalOperator = "OR";
					break;
				}
				if((operator1.equals(">") || operator1.equals(">=")) &&
					(operator2.equals("<") || operator2.equals("<=")))
				{
					if(operand1 != null && operand2 != null && operand1.compareTo(operand2) > 0)
					{
						logicalOperator = "OR";
						break;
					}
				}
				if((operator2.equals(">") || operator2.equals(">=")) &&
						(operator1.equals("<") || operator1.equals("<=")))
				{
					if(operand1 != null && operand2 != null && operand1.compareTo(operand2) < 0)
					{
						logicalOperator = "OR";
						break;
					}
				}
			}
		}
		return logicalOperator;
	}
	
	/**
	 * For multiple filters defined on a given measure column, this method determines if the filters should be ORed or ANDed.
	 * The method applies following rules for determination.
	 * col >/>= x; col </<= y: if x < y then combine filters by AND else use OR
	 * col >/>= x; col >/>= y: combine by AND
	 * col </<= x; col </<= y: combine by AND
	 * col = x; col >/>=/</<=: combine by OR
	 * @param filters List of all filters for a given query
	 * @param index Index for the filter that needs to be compared with the rest in the list
	 * @return logical operator that the filters should be combined with
	 */
	private String getDFLogicalOperator(List<DisplayFilter> filters, int index)
	{
		String logicalOperator = "AND";
		DisplayFilter df = filters.get(index);
		String measure = df.getColumn();
		for (int j = index + 1; j < filters.size(); j++)
		{
			DisplayFilter df2 = (DisplayFilter) filters.get(j);
			if (df2.containsMeasure(measure))
			{
				DisplayFilter.Operator operator1 = df.getOperator();
				DisplayFilter.Operator operator2 = df2.getOperator();
				Double operand1 = null;
				Double operand2 = null;
				try
				{
					operand1 = Double.valueOf(df.getOperand().toString());
					operand2 = Double.valueOf(df2.getOperand().toString());
				}
				catch(NumberFormatException nfe)
				{
					operand1 = null;
					operand2 = null;
				}
				if(operator1 == DisplayFilter.Operator.Equal && 
						((operator2 == DisplayFilter.Operator.GreaterThan) || (operator2 == DisplayFilter.Operator.GreaterThanOrEqual) 
								|| (operator2 == DisplayFilter.Operator.LessThan) || (operator2 == DisplayFilter.Operator.LessThanOrEqual)))
				{
					logicalOperator = "OR";
					break;
				}
				if(operator2 == DisplayFilter.Operator.Equal && 
						((operator1 == DisplayFilter.Operator.GreaterThan) || (operator1 == DisplayFilter.Operator.GreaterThanOrEqual) || 
								(operator1 == DisplayFilter.Operator.LessThan) || (operator1 == DisplayFilter.Operator.LessThanOrEqual)))
				{
					logicalOperator = "OR";
					break;
				}
				if((operator1 == DisplayFilter.Operator.GreaterThan || operator1 == DisplayFilter.Operator.GreaterThanOrEqual) &&
					(operator2 == DisplayFilter.Operator.LessThan || operator2 == DisplayFilter.Operator.LessThanOrEqual))
				{
					if(operand1 != null && operand2 != null && operand1.compareTo(operand2) > 0)
					{
						logicalOperator = "OR";
						break;
					}
				}
				if((operator2 == DisplayFilter.Operator.GreaterThan || operator2 == DisplayFilter.Operator.GreaterThanOrEqual) &&
						(operator1 == DisplayFilter.Operator.LessThan || operator1 == DisplayFilter.Operator.LessThanOrEqual))
				{
					if(operand1 != null && operand2 != null && operand1.compareTo(operand2) < 0)
					{
						logicalOperator = "OR";
						break;
					}
				}
			}
		}
		return logicalOperator;
	}

	
	public BindInfo setElementPositions(boolean print, boolean csv, 
			boolean standalone, int outputWidth, int outputHeight) {
		// Bind to columns in the data set
		BindInfo bi = new BindInfo(this);
		/*
		 * Figure out whether to use a table or a pivot table
		 */
		
		// determine if pivot should be available
		int numMeasures = 0;
		int numNonPivotColumns = 0;
		int numPivotColumns = 0;
		for (AdhocColumn rc : columns) {
			if (rc.isPivotColumn())
				numPivotColumns++;
			if (rc.treatAsMeasure()) {
				numMeasures++;
			}
			else if (rc.treatAsDimension() &&
					rc.isPivotColumn() == false) {
				numNonPivotColumns++;
			}
		}
		isPivot = numPivotColumns > 0;
		if (numMeasures == 0 || numNonPivotColumns == 0) {
			isPivot = false;
		}
		/*
		 * Create chart at top of report if appropriate
		 */
		// Set default sizing
		PageInfo pageInfo = new PageInfo(this, outputHeight, outputWidth, print, standalone, 
				bi.numColumns, numPivotColumns, bi.maxMeasureWidth, isPivot, csv);
		// Carry over the parameters set by report design layout servlet if it is a manual layout. If you add some other page info parameters to
		// edit report layout servlet, they need to be copied over in the following piece of code.
//		if (!automaticLayout)
		{
			pageInfo.setPrint(print);
			if(pi != null)
			{
				pageInfo.titleBand.copyFrom(pi.titleBand);
				pageInfo.headerBand.copyFrom(pi.headerBand);
				pageInfo.detailBand.copyFrom(pi.detailBand);
				pageInfo.summaryBand.copyFrom(pi.summaryBand);
				pageInfo.groupHeaders = pi.groupHeaders;
				pageInfo.groupFooters = pi.groupFooters;
				pageInfo.doNotBreakGroups = pi.doNotBreakGroups;
				pageInfo.startGroupOnNewPage = pi.startGroupOnNewPage;
				pageInfo.getPageHeaderBand().copyFrom(pi.getPageHeaderBand());
				pageInfo.getPageFooterBand().copyFrom(pi.getPageFooterBand());
			}
		}
		pi = pageInfo;
		
		int headerX = 0;
		int detailX = 0;
		int summaryX = 0;
		int titleX = 0;
		int titleY = 0;
		int[] groupHeaderX = new int[this.getNumGroups()];
		int[] groupFooterX = new int[this.getNumGroups()];
		for (int i = 0; i < this.getNumGroups(); i++) {
			groupHeaderX[i] = 0;
			groupFooterX[i] = 0;
		}
		
		if (this.entities != null) {
			
			for (AdhocEntity entity : this.entities) {
				if (entity.isShowInReport() == false) 
					continue;
				
				if (this.includeTable == false) {
					if (entity instanceof AdhocColumn) 
						continue;
					
					if (entity instanceof AdhocLabel) {
						boolean foundColumn = false;
						for (AdhocColumn c : this.columns) {
							if (c.getLabelField() == entity) {
								foundColumn = true;
								break;
							}
						}
						if (foundColumn == true) 
							continue;
					}
				}
				
				if (this.automaticLayout) {
					boolean set = false;
					// check to see if the matching item has already been positioned.  If so, position
					// left accordingly -- see bug 7270
					if (entity instanceof AdhocLabel) {
						// find column
						for (AdhocEntity ae : this.entities) {
							
							if (ae == entity) {
								// if we're on this item, we haven't positioned its matching item yet
								// position this one according to the rules and catch its match later
								break;
							}
							
							if (ae instanceof AdhocColumn) {
								AdhocColumn ac = (AdhocColumn)ae;
								if (ac.getLabelField() == entity) {
									// found it
									set = true;
									entity.setLeft(ac.getLeft());
								}
							}
						}
					}
					else if (entity instanceof AdhocColumn) {
						// find label
						for (AdhocEntity ae: this.entities) {
							if (ae == entity) {
								break;
							}
							
							if (ae instanceof AdhocLabel && ((AdhocColumn)entity).getLabelField() == ae) {
								set = true;
								entity.setLeft(ae.getLeft());
							}
						}
					}
					
					if (set == false) {
						if (entity.getBand() == Band.Header) {
							entity.setLeft(headerX);
						}
						else if (entity.getBand() == Band.Summary) {
							entity.setLeft(summaryX);
						}
						else if (entity.getBand() == Band.GroupHeader && 
								entity.getGroupNumber() <= this.getNumGroups()) {
							entity.setLeft(groupHeaderX[entity.getGroupNumber()]);
						}
						else if (entity.getBand() == Band.GroupFooter && 
								entity.getGroupNumber() <= this.getNumGroups()) {
							entity.setLeft(groupFooterX[entity.getGroupNumber()]);
						}
						else if (entity.getBand() == Band.Title) {
							if (titleX > 0 && 
									entity.getWidth() + titleX > 
										((getPageOrientation() == OrientationEnum.LANDSCAPE) ? outputHeight : outputWidth)) {
								titleX = 0;
								titleY = titleY + entity.getHeight();
							}
							entity.setLeft(titleX);
							entity.setTop(titleY);
						}
						else {
							entity.setLeft(detailX);
						}
					}
				}
				if (entity.getBand() == Band.Header) {
					if (this.automaticLayout) {
						headerX += entity.getWidth() + BETWEEN_COLUMN_SPACING;
						if (pi.headerBand.height < entity.getTop() + entity.getHeight()) {
							pi.headerBand.height = entity.getTop() + entity.getHeight() + BETWEEN_COLUMN_SPACING;
						}
					}
				}
				else if (entity.getBand() == Band.Summary) {
					summaryX += entity.getWidth() + BETWEEN_COLUMN_SPACING;
					if (pi.summaryBand.height < entity.getHeight() + entity.getTop()) {
						pi.summaryBand.height = entity.getHeight() + entity.getTop();
					}
				}
				else if (entity.getBand() == Band.GroupHeader && 
						entity.getGroupNumber() <= pi.groupHeaders.length) {
					groupHeaderX[entity.getGroupNumber()] += entity.getWidth() + BETWEEN_COLUMN_SPACING;
				}
				else if (entity.getBand() == Band.GroupFooter && 
						entity.getGroupNumber() <= pi.groupFooters.length) {
					groupFooterX[entity.getGroupNumber()] += entity.getWidth() + BETWEEN_COLUMN_SPACING;
				}
				else if (entity.getBand() == Band.Title) {
					titleX += entity.getWidth() + BETWEEN_COLUMN_SPACING;
					int tempHeight = entity.getHeight() + entity.getTop(); 
					if (pi.titleBand.height < tempHeight) {
						pi.titleBand.height = tempHeight;
					}
				}
				else {
					if (this.automaticLayout) {
						detailX += entity.getWidth() + BETWEEN_COLUMN_SPACING;
					}
				}
			}
		}
		
		return bi;
	}
	
	public boolean isShowNullZero() {
		return showNullZero;
	}

	public void setShowNullZero(boolean showNullZero) {
		this.showNullZero = showNullZero;
	}

	public boolean isShowINFBlank() {
		return showINFBlank;
	}

	public void setShowINFBlank(boolean showINFBlank) {
		this.showINFBlank = showINFBlank;
	}

	public boolean isShowNaNBlank() {
		return showNaNBlank;
	}

	public void setShowNaNBlank(boolean showNaNBlank) {
		this.showNaNBlank = showNaNBlank;
	}
	
	public synchronized JasperReport getDesign(Repository r, boolean print, boolean csv, boolean standalone, 
			boolean substitutePrompts, Map<String, Object> ps, boolean isPdf, boolean viewSelectorNoChart, String selectorsXml) throws Exception
	{
		return getDesign(r, print, csv, standalone, substitutePrompts, ps, isPdf, viewSelectorNoChart, selectorsXml, false);
	}
	
	public synchronized JasperReport getDesign(Repository r, boolean print, boolean csv, boolean standalone, 
			boolean substitutePrompts, Map<String, Object> ps, boolean isPdf, boolean viewSelectorNoChart, String selectorsXml, boolean isSubReport) throws Exception
	{
		int outputHeight = this.getPageHeight();
		int outputWidth = this.getPageWidth();
		BindInfo bi = setElementPositions(print, csv, standalone, outputWidth, outputHeight);
		
		SMIWebUserBean userBean = SMIWebUserBean.getUserBean();
		
		Map<String, ColumnSelector> selectors = null;
		if (selectorsXml != null && Util.hasNonWhiteSpaceCharacters(selectorsXml)) {
			 selectors = parseColumnSelectors(selectorsXml);
		}
		// JasperDesign
		JasperDesign jasperDesign = new JasperDesign();
		jasperDesign.setWhenNoDataType(WhenNoDataTypeEnum.ALL_SECTIONS_NO_DETAIL);
		jasperDesign.setName("Results");
		jasperDesign.addImport("com.successmetricsinc.*");
		jasperDesign.addImport("com.successmetricsinc.query.*");
		jasperDesign.addImport("com.successmetricsinc.query.olap.*");
		jasperDesign.addImport("java.net.URLEncoder");

		JRDesignParameter param = new JRDesignParameter();
		param.setValueClass(String.class);
		param.setName(AdhocChart.BIRST_SELECTORS);
		JRDesignExpression expression = new JRDesignExpression();
		param.setDefaultValueExpression(expression);
		jasperDesign.addParameter(param);
		
		param = new JRDesignParameter();
		param.setValueClass(Boolean.class);
		param.setName(AdhocChart.APPLY_ALL_PROMPTS);
		expression = new JRDesignExpression();
		expression.setText("Boolean.TRUE");
		param.setDefaultValueExpression(expression);
		jasperDesign.addParameter(param);
		
		JRPropertiesMap props = jasperDesign.getPropertiesMap();
		List<AdhocColumnSelector> csl = this.getColumnSelectorList();
		int count = 0;
		if (csl != null && csl.size() > 0) {
			for (AdhocColumnSelector cs : csl) {
				if (cs.getItems() == null || cs.getItems().size() <= 0) {
					continue;
				}
				count++;
				String propPrefix = JASPERREPORTS_EXPORT_PREFIX + COLUMN_SELECTOR + count;
				props.setProperty(propPrefix + COLUMN_SELECTOR_NAME, XmlUtils.encode(cs.getName()));
				props.setProperty(propPrefix + COLUMN_SELECTOR_SELECTED, XmlUtils.encode(cs.getSelected()));
				
				int itemCount = 0;
				for (ColumnSelector colSel : cs.getItems()) {
					itemCount++;
					String itemPrefix = propPrefix + "." + itemCount;
					AdhocColumn cc = colSel.getAdhocColumn();
					props.setProperty(itemPrefix + COLUMN_SELECTOR_ITEM_LABEL, XmlUtils.encode(cc.getLabelString()));
					props.setProperty(itemPrefix + COLUMN_SELECTOR_ITEM_DIMENSION, XmlUtils.encode(cc.getDimension()));
					props.setProperty(itemPrefix + COLUMN_SELECTOR_ITEM_COLUMN, XmlUtils.encode(cc.getColumn()));
				}
				props.setProperty(propPrefix + COLUMN_SELECTOR_ITEM_COUNT, String.valueOf(itemCount));
				
				String prefix = cs.getName();
				param = new JRDesignParameter();
				param.setValueClass(String.class);
				param.setName(prefix + ColumnSelector.LABEL);
				expression = new JRDesignExpression();
				expression.setText("\"" + prefix + "\"");
				param.setDefaultValueExpression(expression);
				jasperDesign.addParameter(param);
				
				ColumnSelector first = cs.getItems().get(0);
				JRDesignParameter dimension = new JRDesignParameter();
				dimension.setValueClass(String.class);
				dimension.setName(prefix + AdhocColumn.DIMENSION);
				JRDesignExpression dimExp = new JRDesignExpression();
				dimExp.setText("\"" + first.getAdhocColumn().getDimension()  + "\"");
				dimension.setDefaultValueExpression(dimExp);
				jasperDesign.addParameter(dimension);
				
				JRDesignParameter column = new JRDesignParameter();
				column.setValueClass(String.class);
				column.setName(prefix + AdhocColumn.COLUMN);
				JRDesignExpression colExp = new JRDesignExpression();
				colExp.setText("\"" + first.getAdhocColumn().getColumn() + "\"");
				column.setDefaultValueExpression(colExp);
				jasperDesign.addParameter(column);
			}
			
			props.setProperty(JASPERREPORTS_EXPORT_PREFIX + COLUMN_SELECTOR + COLUMN_SELECTOR_COUNT, String.valueOf(count));
		}
		
		if (useFlexGrid && isAutomaticLayout()) {
			props.setProperty(JASPERREPORTS_EXPORT_PREFIX + USE_FLEX_GRID, "true");
		}
		
		int drillColumnCount = 0;
		// add the drill down list stuff
		for (AdhocColumn ac : columns) {
			if (! (ac.treatAsDimension() && Chart.DRILL_TYPE_DRILL.equals(ac.getDrillType()))) {
				continue;
			}
			List<DrillColumnList> drillToColumns = ac.retrieveAllDrillToColumns(r);
			if (drillToColumns != null) {
				Drill drill = new Drill(ac, r);
				String xml = drill.toString();
				String propName = DRILL_COLUMN + drillColumnCount;
				props.setProperty(JASPERREPORTS_EXPORT_PREFIX + propName + "_0", xml);
				props.setProperty(JASPERREPORTS_EXPORT_PREFIX + propName + "_index", String.valueOf(ac.getReportIndex()));
				
				int i = 1;
				for (; i < drillToColumns.size(); i++) {
					DrillColumnList dcl = drillToColumns.get(i);
					drill.setDrillColumns(dcl.getDrillColumns());
					xml = drill.toString();
					props.setProperty(JASPERREPORTS_EXPORT_PREFIX + propName + '_' + String.valueOf(i), xml);
				}
				props.setProperty(JASPERREPORTS_EXPORT_PREFIX + propName + "_Count", String.valueOf(i));
				drillColumnCount++;
			}
		}
		props.setProperty(JASPERREPORTS_EXPORT_PREFIX + DRILL_COLUMN + "Count", String.valueOf(drillColumnCount));
		
		/*
		 * Generate the logical query for this report
		 */
		JRDesignQuery query = new JRDesignQuery();
		StringBuilder queryText = new StringBuilder();
		
		Query q = getQuery(r, selectorsXml);
		if(q != null)
		{
			q.setFullOuterJoin(true);
			queryText.append(q.getLogicalQueryString(substitutePrompts, expressionList, true, isSubReport));
		}
		if (topFilter != null && topFilter.isPrompted()) {
			// replace value with prompt name
			int topIndex = queryText.indexOf("TOP");
			if (topIndex >= 0) {
				if (r.isUseNewQueryLanguage()) {
					int endIndex = 3;
					while (StringUtils.isWhitespace(queryText.subSequence(topIndex + endIndex, topIndex + endIndex + 1))) {
						endIndex++;
					}
					while (!StringUtils.isWhitespace(queryText.subSequence(topIndex + endIndex, topIndex + endIndex + 1))) {
						endIndex++;
					}
					queryText.replace(topIndex, topIndex + endIndex, "TOP GetPromptValue('" + topFilter.getPromptName() + "') ");
				}
				else {
					int endIndex = queryText.indexOf("}", topIndex + 4);
					if (endIndex >= 0) {
						queryText.replace(topIndex, endIndex, "TOP{GetPromptValue('" + topFilter.getPromptName() + "') ");
					}
				}
			}
			else if (r.isUseNewQueryLanguage()){
				int selectIndex = queryText.indexOf("SELECT ");
				if (selectIndex >= 0) {
					selectIndex += 7;
					queryText.insert(selectIndex, "') ");
					queryText.insert(selectIndex, topFilter.getPromptName());
					queryText.insert(selectIndex, " TOP GetPromptValue('");
				}
			}
			else {
				queryText.insert(0, "')} ");
				queryText.insert(0, topFilter.getPromptName());
				queryText.insert(0, " TOP{GetPromptValue('");
			}
		}
		if (displayFilters != null)
		{
			queryText.append(DisplayFilter.getQueryFilters(displayFilters, substitutePrompts, r.isUseNewQueryLanguage()));
		}
		List<DisplayOrder> displayOrders = getDisplayOrders(selectors);
		if (displayOrders != null)
		{
			queryText.append(DisplayOrder.getQueryOrders(displayOrders, r.isUseNewQueryLanguage()));
		}
		String queryTextStr = queryText.toString();
		if((standalone) && (queryTextStr == null || !Util.hasNonWhiteSpaceCharacters(queryTextStr)) || isHidedata())
		{
			queryTextStr = "SingleRow{}";
		}
		if (queryTextStr != null)
			queryTextStr = queryTextStr.replace("\n", "");
		query.setText(queryTextStr);
		jasperDesign.setQuery(query);
		
		if (reportFilters != null) {
			String catalogRoot = null;
			if (userBean != null)
				catalogRoot = userBean.getCatalogPath();
			else {
				catalogRoot = r.getServerParameters().getApplicationPath() + "\\catalog";
			}
			AdhocReport.addSetBasedFilterParameters(this, catalogRoot, jasperDesign);
		}

		/*
		 * Create parameters for prompts if needed
		 */
		addPromptParameters(jasperDesign);
		
		
		int designPageHeight = getPageHeight();
		int designPageWidth = getPageWidth();
		int designLeftMargin = getLeftMargin();
		int designRightMargin = getRightMargin();
		int designTopMargin = getTopMargin();
		int designBottomMargin = getBottomMargin();
		
		if (!csv)
		{
			boolean useReportMargins = standalone && print;
			if (!useReportMargins) // Use the margins from page info.
			{
				if (standalone)
				{
					designPageWidth = getPageWidth(); // - getLeftMargin() - getRightMargin() + (pi.leftMar + pi.rightMar);
					designPageHeight = getPageHeight(); // - getTopMargin() - getBottomMargin() + (pi.topMar + pi.bottomMar);
				} else
				{
					designPageWidth = outputWidth;
					designPageHeight = outputHeight;
				}

				designLeftMargin = Math.min(PageInfo.DEFAULT_HORIZONTAL_MARGIN, this.getLeftMargin());
				designRightMargin = Math.min(PageInfo.DEFAULT_HORIZONTAL_MARGIN, getRightMargin());
				designTopMargin = Math.min(PageInfo.DEFAULT_VERTICAL_MARGIN, getTopMargin());
				designBottomMargin = Math.min(PageInfo.DEFAULT_VERTICAL_MARGIN, getBottomMargin());
			}

			jasperDesign.setPageWidth(designPageWidth);
			jasperDesign.setPageHeight(designPageHeight);
			jasperDesign.setOrientation(this.getPageOrientation());
			jasperDesign.setLeftMargin(designLeftMargin);
			jasperDesign.setRightMargin(designRightMargin);
			jasperDesign.setTopMargin(designTopMargin);
			jasperDesign.setBottomMargin(designBottomMargin);
			jasperDesign.setColumnWidth(pi.columnWidth);
			jasperDesign.setColumnSpacing(0);
		} else
		{
			jasperDesign.setPageHeight(Integer.MAX_VALUE);
		}
		// Setup bands
		JRDesignBand titleBand = createBand(pi.titleBand);
		JRDesignBand headerBand = createBand(pi.headerBand);
		JRDesignBand summaryBand = createBand(pi.summaryBand);
		JRDesignBand pageHeaderBand = new JRDesignBand();
		pageHeaderBand.setSplitType(SplitTypeEnum.PREVENT);
		JRDesignBand pageFooterBand = new JRDesignBand();
		pageFooterBand.setSplitType(SplitTypeEnum.PREVENT);
		JRDesignBand detailBand = null;
		// Setup the report title
		if (pi.detailBand.height == PageInfo.DEFAULT_ROW_HEIGHT)
			pi.detailBand.height = rowHeight;
		if (pi.groupHeaderBands != null) {
			for (int i = 0; i < pi.groupHeaderBands.length; i++) {
				if (pi.groupHeaderBands[i] != null && pi.groupHeaderBands[i].getHeight() == PageInfo.DEFAULT_ROW_HEIGHT)
					pi.groupHeaderBands[i].setHeight(rowHeight);
				
				if (pi.groupFooterBands[i] != null && pi.groupFooterBands[i].getHeight() == PageInfo.DEFAULT_ROW_HEIGHT)
					pi.groupFooterBands[i].setHeight(rowHeight);
			}
		}
		
		pageHeaderBand.setHeight(pi.getPageHeaderHeight());
		pageFooterBand.setHeight(pi.getPageFooterHeight());
		jasperDesign.setPageHeader(pageHeaderBand);
		jasperDesign.setPageFooter(pageFooterBand);
		
		titleBand.setHeight((pi.titleBand.height == PageInfo.DEFAULT_ROW_HEIGHT) ? rowHeight : pi.titleBand.height);
		int titleOffset = addTitle(title, titleBand, pi, print);
		jasperDesign.setTitle(titleBand);
		if (print || Session.getRenderer() != Session.Renderer.Dashboard || 
				(Session.getRenderer() == Session.Renderer.Dashboard && showReportFilterStrInDash)){
			titleOffset = addFilterField(jasperDesign, titleBand, pi, print, titleOffset);
		}
		titleBand.setHeight(pi.titleBand.height + titleOffset);
		
		pageBreak(titleBand, pi.titleBand.forcePageBreak);
		pageBreak(headerBand, pi.headerBand.forcePageBreak);
		pageBreak(detailBand, pi.detailBand.forcePageBreak);
		pageBreak(summaryBand, pi.summaryBand.forcePageBreak);
		
		// Page header
		if (isPivot && !print && !csv)
			headerBand.setHeight(getRowHeight());
		else if (this.automaticLayout)
			headerBand.setHeight(getRowHeight());
		else 
			headerBand.setHeight(pi.headerBand.height);

		// Summary
		if (!isPivot)
		{
			summaryBand.setHeight(pi.summaryBand.height);
		}
//		if (includeTable)
		{
			/*
			 * If this is a pivot table, setup
			 */
			CrosstabSetup cs = null;
			if (includeTable || viewSelectorNoChart) {
				if (isPivot)
				{
					//reset the max column data width calculations
//					doneMaxColumnDataWidthCalc = false;
					
					int crossTabHeight = 10; //designPageHeight - designTopMargin - designBottomMargin - headerBand.getHeight() - titleBand.getHeight() - 1;
					int crossTabWidth = designPageWidth - designLeftMargin - designRightMargin;
					//System.out.println("crosstab height = " + crossTabHeight + " width = " + crossTabWidth);
					cs = setupPivot(pi, summaryBand, bi.numMeasures, bi.numOnRows, crossTabHeight, crossTabWidth);
				} else
				{
					detailBand = createBand(pi.detailBand);
					if (this.automaticLayout) {
						detailBand.setHeight(pi.detailBand.height + BETWEEN_COLUMN_SPACING);
					}
				}
			}
			
			JRDesignStyle detailTextFieldStyle = new JRDesignStyle();
			detailTextFieldStyle.setName("GBBASE");
			jasperDesign.addStyle(detailTextFieldStyle);
			if (! isSubReport) {
				/*
				 * Setup alternate style
				 */
				
				if (this.isAlternateRowColors() && !isPivot)
				{
					JRDesignConditionalStyle cgstyle = new JRDesignConditionalStyle();
					JRDesignExpression gbexpression = new JRDesignExpression();
					gbexpression.setText("Boolean.valueOf($V{REPORT_COUNT}.intValue()%2==0)");
					cgstyle.setConditionExpression(gbexpression);
					cgstyle.setBackcolor(this.getFirstAlternateColor());
					cgstyle.setMode(ModeEnum.OPAQUE);
					detailTextFieldStyle.addConditionalStyle(cgstyle);
					cgstyle = new JRDesignConditionalStyle();
					gbexpression = new JRDesignExpression();
					gbexpression.setText("Boolean.valueOf($V{REPORT_COUNT}.intValue()%2==1)");
					cgstyle.setConditionExpression(gbexpression);
					cgstyle.setBackcolor(this.getSecondAlternateAlt());
					cgstyle.setMode(ModeEnum.OPAQUE);
					detailTextFieldStyle.addConditionalStyle(cgstyle);
				}
			}
			
			if (!(isPivot && (print || csv)))
				jasperDesign.setColumnHeader(headerBand);
			if (detailBand != null) {
				JRDesignSection section = (JRDesignSection) jasperDesign.getDetailSection();
				if (section != null) {
					JRBand[] bands = section.getBands(); 
					if (bands == null || bands.length == 0)
					{
						((JRDesignSection)section).addBand(detailBand);
					}
					else
					{
						((JRDesignSection)section).removeBand(0);
						((JRDesignSection)section).addBand(0, detailBand);
					}
				}
			}
			jasperDesign.setSummary(summaryBand);
			

			/*
			 * Setup groups
			 */
			if (this.includeTable || viewSelectorNoChart) {
				setupGroups(jasperDesign, pi);
			}

			int left = 0;
			int right = 0;
			int top = 100;
			Band topBand = Band.Detail;
//			if (this.includeTable) {
				for (AdhocEntity entity : this.entities) {
					if (topBand == Band.Detail && entity.getBand() == Band.Header) {
						topBand = Band.Header;
						top = entity.getTop();
					}
					else if (topBand == entity.getBand()) {
						top = Math.min(top, entity.getTop());
					}
					right = Math.max(right, entity.getLeft() + entity.getWidth());
					left = Math.min(left, entity.getLeft());
				}
					for (AdhocColumn column : this.columns) {
						// add fields - only if we need to
						@SuppressWarnings("rawtypes")
						Class fieldClass = column.getFieldClass(r, selectors);
						JRDesignField field = new JRDesignField();
						field.setName(column.getFieldName());
						field.setValueClass(column.isSortablePivotField() ? SortablePivotField.class : fieldClass);
						jasperDesign.addField(field);
						
						if (column.isPivotTotal()) {
							for (AdhocColumn measure: this.columns) {
								if ((measure.getType() == ColumnType.Measure) ||
										((measure.getType() == ColumnType.Expression) && measure.isMeasureExpression())) {
									fieldClass = measure.getFieldClass(r, selectors);
									field = new JRDesignField();
									field.setName(measure.getFieldName() + "_" + column.getFieldName());
									field.setValueClass(fieldClass);
									jasperDesign.addField(field);
								}
							}
						}
					}
			if (entities != null) { //includeTable == true && entities != null) {
				
				//Pivot variables collections 
				int pivotY = 0;
				int pivotMeasureMaxWidth = 0;
				int pivotHeaderHeight = 0;
				List<JRDesignTextField> pivotMeasuresColumnDesign = new ArrayList<JRDesignTextField>();
				List<JRDesignTextField> pivotMeasuresLabelDesign = new ArrayList<JRDesignTextField>();
				List<JRDesignCrosstabCell> pivotTotalCells = new ArrayList<JRDesignCrosstabCell>();
				
				boolean hasCharts = false;
				
				for (AdhocEntity entity : this.entities) {
					if (this.includeTable == false && viewSelectorNoChart == false) {
						if (entity instanceof AdhocColumn) 
							continue;
						
						if (entity instanceof AdhocLabel) {
							boolean foundColumn = false;
							for (AdhocColumn c : this.columns) {
								if (c.getLabelField() == entity) {
									foundColumn = true;
									break;
								}
							}
							if (foundColumn == true) 
								continue;
						}
					}
					
					if (entity instanceof AdhocChart)
						hasCharts = true;
					
					if (viewSelectorNoChart && defaultChartIndex == entity.getReportIndex())
						continue;
					
					JRDesignElement element = entity.getElement(this, r, jasperDesign, pi, isPdf, selectors);
					if (element != null) {
						// add show if expression
						int index = entity.getIncludeIfReportIndex();
						if (index >= 0) {
							for (AdhocEntity ae : entities) {
								if (ae.reportIndex == index && ae instanceof AdhocColumn) {
									AdhocColumn ac = (AdhocColumn)ae;
									JRDesignExpression printWhenExpression = new JRDesignExpression();
									printWhenExpression.addFieldChunk(ac.getFieldName());
									element.setPrintWhenExpression(printWhenExpression);
									break;
								}
							}
						}
						
						index = entity.getHyperlinkReportIndex();
						if (index >= 0 && !isPivot) {
							for (AdhocEntity ae : entities) {
								if (ae.reportIndex == index && ae instanceof AdhocColumn) {
									AdhocColumn ac = (AdhocColumn)ae;
									JRDesignExpression hyperlinkPageExpression = new JRDesignExpression();
									hyperlinkPageExpression.addFieldChunk(ac.getFieldName());
									HyperlinkTargetEnum targetType = ac.isHyperlinkSelfTarget() ? HyperlinkTargetEnum.SELF : HyperlinkTargetEnum.BLANK;
									if (element instanceof JRDesignTextField) {
										((JRDesignTextField)element).setHyperlinkTarget(targetType);
//										((JRDesignTextField)element).setHyperlinkTarget(HyperlinkTargetEnum.BLANK);
										((JRDesignTextField)element).setHyperlinkType(HyperlinkTypeEnum.REFERENCE);
										((JRDesignTextField)element).setHyperlinkReferenceExpression(hyperlinkPageExpression);
										if (! isDoNotStyleHyperlinks()) {
											((JRDesignTextField)element).setUnderline(true);
											((JRDesignTextField)element).setForecolor(Color.BLUE);
										}
									}
									else if (element instanceof JRDesignImage) {
										((JRDesignImage)element).setHyperlinkTarget(targetType);
//										((JRDesignImage)element).setHyperlinkTarget(HyperlinkTargetEnum.BLANK);
										((JRDesignImage)element).setHyperlinkType(HyperlinkTypeEnum.REFERENCE);
										((JRDesignImage)element).setHyperlinkReferenceExpression(hyperlinkPageExpression);
									}
								}
							}
						}
						
						JRDesignBand band = detailBand;
						
						if (entity.getBand() == null) {
							// if in NEW band, don't show on report.
							continue;
						}
						
						if (includeTable == false && viewSelectorNoChart == false && (entity.getBand() == Band.Header || entity.getBand() == Band.Detail)) {
							// if don't show table, and in header or detail band, don't show on report
							continue;
						}
						
						if (entity instanceof AdhocColumn) {
							AdhocColumn ac = (AdhocColumn)entity;
							
							String key = ac.getLabelString();
							ColumnSelector columnSelector = null;
							if (selectors != null && key != null) {
								columnSelector = selectors.get(key);
							}
							props = element.getPropertiesMap();
							String label = columnSelector == null ? key : columnSelector.getLabel();
							props.setProperty(JASPERREPORTS_EXPORT_PREFIX + "columnLabel", XmlUtils.encode(label));
							props.setProperty(JASPERREPORTS_EXPORT_PREFIX + "columnIndex", String.valueOf(entity.getReportIndex()));
							props.setProperty(JASPERREPORTS_EXPORT_PREFIX + "columnType", XmlUtils.encode(ac.getColumnType()));
							if (ac.getType() != ColumnType.Measure) {
								props.setProperty(JASPERREPORTS_EXPORT_PREFIX + "dimension", XmlUtils.encode(columnSelector == null ? ac.getDimension() : columnSelector.getAdhocColumn().getDimension()));
							}
							if (ac.getHierarchy() != null) {
								props.setProperty(JASPERREPORTS_EXPORT_PREFIX + "hierarchy", XmlUtils.encode(columnSelector == null ? ac.getHierarchy() : columnSelector.getAdhocColumn().getHierarchy()));
							}
							String col = columnSelector == null ? ac.getColumn() : columnSelector.getAdhocColumn().getColumn();
							if (ac.getType() == ColumnType.Expression) { // && ac.getExpressionString().startsWith("SavedExpression(")) {
								col = ac.getExpressionString();
							}
							props.setProperty(JASPERREPORTS_EXPORT_PREFIX + "column", XmlUtils.encode(col));
							
							//If the type is date, we add in the date format
							String dateTimeFormat = columnSelector == null ? ac.getDateTimeFormat() : columnSelector.getAdhocColumn().getDateTimeFormat();
							if( dateTimeFormat != null ){
								props.setProperty(JASPERREPORTS_EXPORT_PREFIX + "datetimeFormat", XmlUtils.encode(ac.getDateTimeFormat()));
							}
							
							props.setProperty(JASPERREPORTS_EXPORT_PREFIX + "showNaNblank", Boolean.toString(showNaNBlank));
							props.setProperty(JASPERREPORTS_EXPORT_PREFIX + "showINFblank", Boolean.toString(showINFBlank));
							@SuppressWarnings("rawtypes")
							Class fieldType = ac.getFieldClass(r, selectors);
							if (showNullZero && ac.getType() != ColumnType.Dimension && 
									(fieldType == Double.class || fieldType == Float.class || fieldType == Integer.class || fieldType == Number.class))
							{
								props.setProperty(JASPERREPORTS_EXPORT_PREFIX + "showNullZero", Boolean.toString(showNullZero));
							}
							
							// XXX alternate colors check because setStyle will override the conditional style
							// XXX missing isXLS, probably should encapsulate all into isExport()
							
							/*
							 * TODO:  patterns aren't supported using expressions
							List<ColumnSelector> css = ((AdhocColumn)entity).getColumnSelectors();
							if (css != null && css.size() > 0) {
								((JRDesignTextField)element).setPattern(pattern)
							}
							*/
						}
						
						if (entity instanceof AdhocLabel) {
							for (AdhocColumn ac: this.getColumns()) {
								if (ac.getLabelField() == entity) {
									List<ColumnSelector> css = ac.getColumnSelectors();
									if (css != null && css.size() > 0  && element instanceof JRDesignTextField) {
										JRDesignExpression de = new JRDesignExpression();
										de.setText("$P{" + ac.getLabelString() + ColumnSelector.LABEL + "}");
										((JRDesignTextField)element).setExpression(de);
									}
								}
							}
						}
						
						if (entity.getBand() == Band.Header) {
							band = headerBand;
							// see if this is a column label
							if (!pi.isPivot()){
								if (entity instanceof AdhocTextEntity) {
									for (AdhocColumn ac: this.getColumns()) {
										if (ac.getLabelField() != null && ac.getLabelField().equals(entity)) {
											props = element.getPropertiesMap();
											props.setProperty(JASPERREPORTS_EXPORT_PREFIX + "columnIndex", String.valueOf(ac.getReportIndex()));
											props.setProperty(JASPERREPORTS_EXPORT_PREFIX + "labelIndex", String.valueOf(entity.getReportIndex()));
											props.setProperty(JASPERREPORTS_EXPORT_PREFIX + "isMenu", "true");
											for (AdhocSort as : getSorts())
											{
												AdhocColumn rc = as.getColumn();
												if (rc.getReportIndex() == ac.getReportIndex())
												{
													props.setProperty(JASPERREPORTS_EXPORT_PREFIX + "sortOrder", as.getOrder() == SortOrder.ASCENDING ? "Ascending" : "Descending");
													break;
												}
											}
										}
									}
								}
							}
							if (pi.isPivot())
								continue;
							
							if (!pi.isPrint() && !pi.isCsv() && pi.isPivot()) {
								// pivot header is in place
								// add 4 * default row height to top
								// change position type to relative to top
								// needed to allow height to stretch
								element.setY(element.getY() + getRowHeight());
								element.setPositionType(PositionTypeEnum.FIX_RELATIVE_TO_TOP);
							}
						}
						else if (entity.getBand() == Band.Summary) {
							band = summaryBand;
						}
						else if (entity.getBand() == Band.PageHeader)
							band = pageHeaderBand;
						else if (entity.getBand() == Band.PageFooter)
							band = pageFooterBand;
						else if (entity.getBand() == Band.GroupHeader && 
								entity.getGroupNumber() <= pi.groupHeaderBands.length) {
							band = pi.groupHeaderBands[entity.getGroupNumber()];
						}
						else if (entity.getBand() == Band.GroupFooter && 
								entity.getGroupNumber() <= pi.groupFooterBands.length) {
							band = pi.groupFooterBands[entity.getGroupNumber()];
						}
						else if (entity.getBand() == Band.Title) {
							band = titleBand;
							// move down a little.
							element.setY(element.getY() + titleOffset);
							element.setPositionType(PositionTypeEnum.FLOAT);
						}

						if (band == detailBand && detailTextFieldStyle != null) {
							if (this.isAlternateRowColors() && element.getStyle() == null)
							{
								element.setStyle(detailTextFieldStyle);
							}
						}
						
						if (band == null) {
							// adding to pivot
							if (entity instanceof AdhocColumn) {
								AdhocColumn column = (AdhocColumn)entity;
								element.setX(0);
								
								if (column.treatAsMeasure())
								{
									
									if (! column.isPivotColumn()) {
										element.setY(pivotY);
										pivotY += element.getHeight();
									}
									String aggRule = column.getExprAggRule();
									if (aggRule == null)
										aggRule = column.getExprAggRule();
									if (aggRule == null) 
										aggRule = "SUM";
									if (pi.isPivot())
									{
										/*
										 * Setup pivot cells
										 */
										/*
										PivotTableOptions pot = getPivotTableOpts();
										if(pot.isResizeTypeRowMeasureColumns()){
											resetColumnWidthBasedOnQueryData((JRDesignTextField)element, column, qs, isPdf);
										}
										
										if(pot.isResizeTypeRowColColumns()){
											int maxColWidth = findMaxColumnHeaderDataWidth((JRDesignTextField) element, qs, isPdf);
											//TODO - figure out how to partition the width if the measures are column wise and > 0
											if(qs.numMeasures() == 1){
												element.setWidth(maxColWidth);
											}
										}
										*/
										
										int measureWidth = element.getWidth();
										if (measureWidth > pivotMeasureMaxWidth)
											pivotMeasureMaxWidth = measureWidth;
										pivotMeasuresColumnDesign.add((JRDesignTextField)element);
										JRDesignTextField lbl = (JRDesignTextField)column.getLabelField().getElement(this, r, jasperDesign, pi, isPdf, selectors);
										if (userBean != null && userBean.isFreeTrial()) {
											lbl.getLineBox().getBottomPen().setLineWidth(0.0f);
											lbl.setBold(false);
										}
										lbl.setStretchWithOverflow(true);
										List<ColumnSelector> css = column.getColumnSelectors();
										if (css != null && css.size() > 0  && element instanceof JRDesignTextField) {
											JRDesignExpression de = new JRDesignExpression();
											de.setText("$P{" + column.getLabelString() + ColumnSelector.LABEL + "}");
											lbl.setExpression(de);
										}
										pivotMeasuresLabelDesign.add(lbl);
										
										setupPivotMeasure(jasperDesign, r, aggRule, column.getFieldName(), column.getFieldClass(r, selectors), 
												(JRDesignTextField)element, cs, column);
									}
								} else if (column.treatAsDimension())
								{
//									if( pi.isPivot()){
										PivotTableOptions pot = getPivotTableOpts();
										/*
										if(!(pot.isResizeTypeRowMeasureColumns()) && ! pot.getAutoResizeType().equals(PivotTableOptions.NO_AUTO_SIZE) ) { // && column.isPivotColumn()) ){
											resetColumnWidthBasedOnQueryData((JRDesignTextField)element, column, qs, isPdf);
										}
										*/
//									}
									
									//DimensionColumn dc = r.findDimensionColumn(column.getDimension(), column.getColumn());
									/*
									 * Setup pivot column/row headers
									 */
									pivotHeaderHeight = setupPivotDimension(this, r, column, column.getFieldName(), 
																			column.getFieldClass(r, selectors), (JRDesignTextField)element, //headerText, 
																			cs, column.getValidFormat(), 
																			pi, bi, column.getBackground(), pivotHeaderHeight, jasperDesign,
																			pivotTotalCells, isPdf, userBean);
								}
							}
						}
						else {
							if (!(pi.isPivot() && entity instanceof AdhocLabel))
									band.addElement(element);
						}
						if (entity instanceof AdhocColumn) {
							/*
							 * Add conditional formats if necessary
							 */
							AdhocColumn column = (AdhocColumn)entity;
							if (column.getConditionalFormats() != null)
							{
								addConditionalFormats(this, column, jasperDesign, (JRDesignTextField)element, detailTextFieldStyle, column.getLeft(), 
										band, pi.isPivot(), cs);
							}
						}
					}
				} //for (AdhocEntity ..
				
				if (hasCharts == true)
					jasperDesign.getPropertiesMap().setProperty(JASPERREPORTS_EXPORT_PREFIX + ADHOC_REPORT, this.toString() );
					
				//Fix up the rest of the pivot stuff.
				if (pi.isPivot() && (includeTable || viewSelectorNoChart)){
					setupPivotMeasureLabels(cs, pivotMeasuresColumnDesign, pivotMeasuresLabelDesign, pivotHeaderHeight, pivotMeasureMaxWidth, pivotTotalCells, isPdf);
					fixupTotalCellWidthHeight(pivotTotalCells, pivotMeasureMaxWidth);
					
					if (!getPivotTableOpts().hasLabel()){
						constraintColumnHeaderWidth(cs, pivotMeasureMaxWidth);
						alignMeasureCells(cs, pivotMeasuresColumnDesign);
					}
				}
				
				//Width bounds check. 
				if (pi.isPivot() && (includeTable && viewSelectorNoChart)){
					cs.crosstab.preprocess();
					JRCellContents headerCell = cs.crosstab.getHeaderCell();
					int headerWidth = (headerCell != null) ? headerCell.getWidth() : 0;
					int pivotTableWidth = headerWidth + cs.contents.getWidth();
					int pageWidth = pi.getWidth() + pi.getLeftMar() + pi.getRightMar();
					//System.out.println("pivot table width: " + pivotTableWidth + " page width = " + pageWidth);
					if (pivotTableWidth > pageWidth){
						throw new PivotTableTooWideException("Pivot table width of " + pivotTableWidth + " is bigger than page width of " + pageWidth);
					}
					
					this.setCrosstabParameters(cs.crosstab);
				}
			}
		}
		// Make sure headers print if no data
		jasperDesign.setWhenNoDataType(WhenNoDataTypeEnum.ALL_SECTIONS_NO_DETAIL);
		jasperDesign.setWhenResourceMissingType(WhenResourceMissingTypeEnum.EMPTY);

		if (headerBand != null && headerBand.getChildren().isEmpty()) {
			headerBand.setHeight(0);
		}
		if (titleBand != null){
			if(titleBand.getChildren().isEmpty()) {
				titleBand.setHeight(0);
			}else{
				if(viewSelectorNoChart){
					AdhocChart chart = this.getDefaultChart();
					if(chart != null){
						int titleBandHeight = 0;
						for (int i = 0; i < titleBand.getChildren().size(); i++) {
							Object o = titleBand.getChildren().get(i);
							if (o instanceof JRDesignElement) {
								titleBandHeight = Math.max(titleBandHeight, 
										((JRDesignElement)o).getY() + ((JRDesignElement)o).getHeight());
							}
						}
						titleBand.setHeight(titleBandHeight );
					}
				}
			}
		}
		if (summaryBand != null && summaryBand.getChildren().isEmpty()) {
			summaryBand.setHeight(0);
		}

		if (DEBUG) {
			try
			{
				File file = File.createTempFile("Adhoc", ".jrxml");
				FileOutputStream os = new FileOutputStream(file);
				JRXmlWriter.writeReport(jasperDesign, os, "UTF-8");
				os.close();
			}
			catch (Exception ex)
			{
				logger.debug("Failed to create the temporary adhoc report file (debug only): " + ex.getMessage());
			}
		}
		
		return JasperCompileManager.compileReport(jasperDesign);
	}
	
	private JRDesignBand createBand(PageInfo.Band pib) {
		JRDesignBand ret = new JRDesignBand();
		ret.setSplitType(SplitTypeEnum.IMMEDIATE);
		AdhocEntity ent = getEntityById(pib.getIncludeIfReportIndex());
		if (ent != null && ent instanceof AdhocColumn) {
			AdhocColumn ac = (AdhocColumn)ent;
			JRDesignExpression printWhenExpression = new JRDesignExpression();
			printWhenExpression.addFieldChunk(ac.getFieldName());
			ret.setPrintWhenExpression(printWhenExpression);
		}
		if (pib.height == PageInfo.DEFAULT_ROW_HEIGHT) 
			pib.height = rowHeight;
		ret.setHeight(pib.height);
		return ret;
	}
	
	private static void addSetBasedFilterParameters(AdhocReport report, String catalogRoot, JasperDesign jasperDesign) throws Exception {
		for (ReportFilter rf: report.getReportFilters()) {
			QueryFilter qf = rf.getQueryFilter();
			if (qf != null) {
				// handle set based filters.
				String fileName = rf.getOperand();
				fileName = CatalogManager.getNormalizedPath(fileName, catalogRoot);
				File aFile = CatalogManager.getAdhocReportFile(fileName, catalogRoot);
				if (aFile == null || aFile.exists() == false)
					continue;
				
				AdhocReport filterReport = AdhocReport.read(aFile);
				for (QueryFilter qf1: filterReport.getFilters()) {
					if (qf1.isPrompted()) {
						String promptName = qf1.getPromptName();
						if (! jasperDesign.getParametersMap().containsKey(promptName)) {
							// add this as a new parameter
							JRDesignParameter designParam = new JRDesignParameter();
							designParam.setName(promptName);
							designParam.setValueClass(String.class);
							jasperDesign.addParameter(designParam);
						}
					}
				}
				addSetBasedFilterParameters(filterReport, catalogRoot, jasperDesign);
			}
		}
		
	}
	
	private int addFilterField(JasperDesign jasperDesign, JRDesignBand band, PageInfo pi, boolean print,
			int titleOffset) {
		if (showFilters) {
			JRDesignTextField textField = new JRDesignTextField();
			textField.setStretchWithOverflow(true);
			textField.setStretchType(StretchTypeEnum.NO_STRETCH);
			textField.getLineBox().setLeftPadding(AdhocTextEntity.DEFAULT_PADDING);
			textField.getLineBox().setRightPadding(AdhocTextEntity.DEFAULT_PADDING);
			textField.getLineBox().setTopPadding(AdhocTextEntity.DEFAULT_VERTICAL_PADDING);
			textField.getLineBox().setBottomPadding(AdhocTextEntity.DEFAULT_VERTICAL_PADDING);
			textField.setX(0);
			textField.setY(titleOffset);
			textField.setWidth(pi.width - pi.leftMar - pi.rightMar);
			textField.setHeight(PageInfo.DEFAULT_NEW_ROW_HEIGHT);
			if (! filtersBackground.equals(Color.WHITE))
				textField.setBackcolor(filtersBackground);
			
			textField.setForecolor(filtersForeground);
			textField.setHorizontalAlignment(AdhocTextEntity.getJRAlignment(filtersAlignment));
			
			// set font
			Font f = getFiltersFont();
			textField.setFontName(f.getFamily());
			textField.setFontSize(f.getSize());
			textField.setBold(f.isBold());
			textField.setItalic(f.isItalic());
			
			// set expression  $F{Prompt_Values}
			JRDesignExpression expression = new JRDesignExpression();
			String temp = null;
			if (this.filtersShowOnOneLine)
			{
				if (this.filtersSkipAll)
					temp = JasperDataSourceProvider.PROMPT_VALUES_ONE_LINE_SKIP_ALL;
				else
					temp = JasperDataSourceProvider.PROMPT_VALUES_ONE_LINE;
			}
			else
			{
				if (this.filtersSkipAll)
					temp = JasperDataSourceProvider.PROMPT_VALUES_SKIP_ALL;
				else
					temp = JasperDataSourceProvider.PROMPT_VALUES;
			}
			expression.setText("$F{" + temp + "}");
			textField.setExpression(expression);
			textField.setBlankWhenNull(true);
			
			JRDesignField field = new JRDesignField();
			field.setValueClass(String.class);
			field.setName(temp);
			try {
				jasperDesign.addField(field);
			} catch (JRException e) {
				logger.error(e,e);
			}
			
			band.addElement(textField);
			int minHeight = titleOffset + PageInfo.DEFAULT_NEW_ROW_HEIGHT;
			
			if (band.getHeight() < minHeight)
				band.setHeight(minHeight);
			
			return minHeight;
		}
		
		return titleOffset;
	}
	
	private Font getFiltersFont() {
		if (filtersFontStr == null || !Util.hasNonWhiteSpaceCharacters(filtersFontStr))
			filtersFontStr = DEFAULT_FILTERS_FONT_STR;
		
		Font font = Font.decode(filtersFontStr);
		return font;
	}

	/*
	//Finds the max column width of each column data.
	private void resetColumnWidthBasedOnQueryData(JRDesignTextField element, AdhocColumn column, QueryResultSet qs, boolean isPdf){
		
		Integer bc = column.getBoundColumn();
		
		if (bc == null || qs == null)
			return;
		
		int maxColWidth = -1;
		for (int i = 0; i < qs.numRows(); i++) {
			int stringFontWidth = getTextFontWidth(element, qs.getRows()[i][bc], isPdf);
			if (stringFontWidth > maxColWidth){
				maxColWidth = stringFontWidth;
			}
		}
		
		if(maxColWidth != -1){
			element.setWidth(maxColWidth);
		}
	}
	*/
	
/*	private int findMaxColumnHeaderDataWidth(JRDesignTextField referenceElement, QueryResultSet qs, boolean isPdf){

		if (qs == null)
			return 0;
		
		//We'd calculated before, use the previous value
		if (doneMaxColumnDataWidthCalc){
			return maxColumnDataWidth;
		}
		
		maxColumnDataWidth = -1;
		for (int i = 0; i < qs.numRows(); i++) {
			for (AdhocColumn rc : columns) {
				Integer bc = rc.getBoundColumn();
				if (bc != null && rc.isPivotColumn()){
					int stringFontWidth = getTextFontWidth(referenceElement, qs.getRows()[i][bc], isPdf);
					if (stringFontWidth > maxColumnDataWidth){
						maxColumnDataWidth = stringFontWidth;
					}
				}
			}
		}
		
		doneMaxColumnDataWidthCalc = true;
		
		return maxColumnDataWidth;
	}
	*/
	
	private void setCrosstabParameters(JRDesignCrosstab cs){
		//Auto-resize type
		JRDesignExpression expr = new JRDesignExpression();
		expr.setText("\"" + getPivotTableOpts().getAutoResizeType() + "\"");
		JRDesignCrosstabParameter param = new JRDesignCrosstabParameter();
		param.setName("auto_resize_type");
		param.setExpression(expr);
		
		try {
			cs.addParameter(param);
		} catch (JRException e) {
			logger.error(e, e);
		}
	}
	
	private void fixupTotalCellWidthHeight(List<JRDesignCrosstabCell>pivotTotalCells, int columnWidth){
		for (JRDesignCrosstabCell cell: pivotTotalCells){
			JRDesignCellContents fields = (JRDesignCellContents)cell.getContents();
			JRElement[] cols = fields.getElements();
			int totalColWidth = 0;
			int maxHeight = 0;
			int yPos = 0;
			for (int i = 0; i < cols.length; i++){
				JRDesignTextField c = (JRDesignTextField)cols[i];
				//drawBorderAll(c.getLineBox());
				if (isPivotMeasureLabelsColumnWise()){
					if (i > 0){
						JRDesignTextField prev = (JRDesignTextField)cols[i - 1];
						int xPos = prev.getX() + prev.getWidth();
						c.setX(xPos);
						c.setY(0);
					}else{
						c.setX(0);
						c.setY(0);
					}
					
				}else{
					c.setX(0);
					c.setY(yPos);
					yPos += c.getHeight();
					c.setWidth(columnWidth);
				}
				int height = c.getHeight();
				if (height > maxHeight)
					maxHeight = height;
				totalColWidth += c.getWidth();
			}
			
			
			if (isPivotMeasureLabelsColumnWise()){
				cell.setWidth(totalColWidth);
				cell.setHeight(maxHeight);
			}else{
				cell.setWidth(columnWidth);
			}
			//System.out.println("total cell width = " + cell.getWidth() + " height = " + cell.getHeight());	
			
		}
	}

	private void setupPivotMeasureLabels(CrosstabSetup cs, 
					List<JRDesignTextField> pivotMeasures, 
					List<JRDesignTextField> pivotMeasureDesigns, 
					int pivotHeaderHeight,
					int pivotMeasureMaxWidth, 
					List<JRDesignCrosstabCell> pivotTotalCells,
					boolean isPdf) throws JRException{
		
//		JRDesignBand band = new JRDesignBand();
		JRDesignCellContents h2 = new JRDesignCellContents();
	
		JRDesignCellContents headerCells = (JRDesignCellContents) cs.crosstab.getHeaderCell();
		if (headerCells != null){

			//Add measure headers column wise..
			int pivotMeasureColHeight = 0;
			if (getPivotTableOpts().isMeasureLabelsColumnWise()){
				pivotMeasureColHeight = alignPivotMeasureLabelsColumnWise(cs, pivotMeasures, pivotMeasureDesigns, pivotTotalCells, isPdf);
			}
			
			//Position the header row cells appropriately
			JRElement[] elems = headerCells.getElements();
			int height = 0;
			int posX = 0;
			int posY = 0;

			for(int i = 0; i < elems.length; i++){ 
				JRDesignTextField text = (JRDesignTextField)elems[i];
				height = text.getHeight();
				text.setStretchWithOverflow(true);
				//System.out.println("pivotHeaderHeight = " + pivotHeaderHeight + " text height = " + height + " pivotMeasureColHeight = " + pivotMeasureColHeight);
				int textYPos = pivotHeaderHeight + pivotMeasureColHeight - height;
				//System.out.println("setting header cell y pos - " + textYPos);
				text.setY(textYPos);
				
				text.setPrintWhenDetailOverflows(true);
				
				text.setStretchType(StretchTypeEnum.RELATIVE_TO_TALLEST_OBJECT);
				text.setPositionType(PositionTypeEnum.FLOAT);
				text.setVerticalAlignment(VerticalAlignEnum.TOP);
			
				posX = text.getX() + text.getWidth();
				posY = text.getY();
				text.setWidth(text.getWidth());
//				band.addElement(text);
				h2.addElement(text);
			}
			
//			h2.addElementGroup(band);
//			h2.setElementGroup(band);
			
//			band.setHeight(height);
			cs.crosstab.setHeaderCell(h2);
			
			//Add measure headers row wise at the end of the header row cells..
			if (!getPivotTableOpts().isMeasureLabelsColumnWise()){
				alignPivotMeasureLabelsRowWise(cs, pivotMeasures, pivotMeasureDesigns, height, posX, posY, pivotMeasureMaxWidth, pivotTotalCells, isPdf);
			}
			
		}//if (headerCells != null
		
	}
	
	private void alignMeasureCells(CrosstabSetup cs, List<JRDesignTextField> pivotMeasures){
		
		if(getPivotTableOpts().isMeasureLabelsColumnWise()) {
			int posX = 0;
			int maxHeight = 0;
			for(JRDesignTextField measure : pivotMeasures){
				measure.setX(posX);
				measure.setY(0);
				posX += measure.getWidth();
				
				int height = measure.getHeight();
				if (maxHeight < height){
					maxHeight = height;
				}
			}
			if(posX > 0){
				pivotColumnResizeWidth(cs, posX);
			}
			
			if(maxHeight > 0){
				cs.cell.setHeight(maxHeight);
			}
		}
	}
	
	private void constraintColumnHeaderWidth(CrosstabSetup cs, int measuresWidth){
		JRDesignCrosstab crosstab = cs.crosstab;
		if (crosstab != null){
			JRCrosstabColumnGroup[] groups = crosstab.getColumnGroups();
			for(int i = 0; i < groups.length; i++){
				JRCellContents headers = groups[i].getHeader();
				JRElement[] els = headers.getElements();
				for(int j = 0; j < els.length; j++){
					els[j].setWidth(measuresWidth);
				}
			}
			if (cs.cell != null){
				cs.cell.setWidth(measuresWidth);
			}
		}
	}
	
	/**
	 * Make sure that the column's label width is the same as the column's
	 * @return the sum of the measure column width
	 */
	private int constraintLabelWidthToColumnWidth(List<JRDesignTextField> measureCols, 
												  List<JRDesignTextField> measureDesigns, 
												  int constantWidth, List<JRDesignCrosstabCell> pivotTotalCells,
												  boolean isPdf)
	{
		
		//Calculate total width, and make sure that label's width is the same as our column's width
		int measuresWidth = 0;
		int size = measureCols.size();
		for(int i = 0; i < size; i++){
			JRDesignTextField col = measureCols.get(i);
			JRDesignTextField label = measureDesigns.get(i);
			JRDesignTextField total = null;

			if (pivotTotalCells != null) {
				try {
					JRExpression jre = col.getExpression();
					String chunk = jre.getText();
					for (JRDesignCrosstabCell cc : pivotTotalCells) {
						JRCellContents jrcc = cc.getContents();
						for (Object o : jrcc.getChildren()) {
							JRDesignTextField tf = (JRDesignTextField)o;
							String marker = tf.getPropertiesMap().getProperty("FieldMarker");
							String expr = (marker != null) ? marker :  tf.getExpression().getText();
							if (chunk.equals(expr)) {
								total = tf;
								break;
							}
						}
					}
				}
				catch (Exception e) { }
			}
			
			int colWidth = col.getWidth();
	
			if (constantWidth < 0){
				if (getPivotTableOpts().isResizeTypeAllLabel()){
					int c = getTextFontWidth(label, null, isPdf);
					if (c > 0)
						colWidth = c;
					label.setWidth(colWidth);
					col.setWidth(colWidth);
					if (total != null)
						total.setWidth(colWidth);
				}else{
					if (label.getWidth() != colWidth){
						label.setWidth(colWidth);
					}
				}
			}else{
				col.setWidth(constantWidth);
				label.setWidth(constantWidth);
				if (total != null)
					total.setWidth(constantWidth);
			}
	
			measuresWidth += colWidth;
		}
		
		return measuresWidth;
	}
	
	
	private int alignPivotMeasureLabelsColumnWise(CrosstabSetup cs, 
												  List<JRDesignTextField> measureCols, 
												  List<JRDesignTextField> measureDesigns, 
												  List<JRDesignCrosstabCell> pivotTotalCells,
												  boolean isPdf) throws JRException{
		JRDesignCellContents contents = cs.contents;
		int columnHeight = 0;
		if (contents != null){

			//Calculate total width, and make sure that label's width is the same as our column's width
			int measuresWidth = constraintLabelWidthToColumnWidth(measureCols, measureDesigns, -1, pivotTotalCells, isPdf);
			
			//Resize our column
			JRDesignCrosstabColumnGroup lastGroup = pivotColumnResizeWidth(cs, measuresWidth);
			
			//Add our measure column headers, basically add another row
			if (lastGroup != null){
				
				int xPos = 0;
				int maxHgt = 0;
				
				//Create our extra column group
				JRDesignCrosstabColumnGroup columnGroup = new JRDesignCrosstabColumnGroup();
				columnGroup.setName("Measure Column Headers");
				columnGroup.setPosition(CrosstabColumnPositionEnum.STRETCH);
				JRDesignCellContents cnt = new JRDesignCellContents();
				JRDesignBand band = new JRDesignBand();
				cnt.addElementGroup(band);
				columnGroup.setHeader(cnt);
				JRDesignCrosstabBucket bucket = new JRDesignCrosstabBucket();
				bucket.setValueClassName(Integer.class.getName());
				JRDesignExpression bucketExp = new JRDesignExpression();
				bucketExp.setText("Integer.valueOf(0)");
				bucket.setExpression(bucketExp);
				columnGroup.setBucket(bucket);
				cs.crosstab.addColumnGroup(columnGroup);
				
				JRElement[] measures = contents.getElements();
				int size = measureCols.size();
				for(int i = 0; i < size; i++){
					//Add measure labels
					JRDesignTextField measureColumn = (JRDesignTextField)measureDesigns.get(i).clone();
					measureColumn.setKey("MEASURE_LABEL_" + measureColumn.getExpression().getText());
					measureColumn.setStretchWithOverflow(true);
					measureColumn.setStretchType(StretchTypeEnum.RELATIVE_TO_BAND_HEIGHT);
//					measureColumn.setHorizontalAlignment(HorizontalAlignEnum.LEFT);
//					measureColumn.setVerticalAlignment(VerticalAlignEnum.TOP);
					measureColumn.setPositionType(PositionTypeEnum.FLOAT);
					columnHeight = measureColumn.getHeight();
					if (columnHeight > maxHgt)
						maxHgt = columnHeight;
					
					measureColumn.setY(0);
					measureColumn.setX(xPos); 
	
					band.addElement(measureColumn);
					
					//Horizontally align the measure cells
					
					// find the correct measure
					// with conditional formatting, you can't rely on the index (i).
					JRDesignTextField target = (JRDesignTextField)measures[i];
					int index = 0;
					int x = -1; 
					int y = -1;
					for (JRElement t : measures) {
						if (index == i) {
							target = (JRDesignTextField)t;
							break;
						}
						if (x < 0) {
							x = t.getX();
						}
						if (y < 0) {
							y = t.getY();
						}
						if (t.getX() == x && t.getY() == y) {
							// same one
							continue;
						}
						index++;
						x = t.getX();
						y = t.getY();
						if (index == i) {
							target = (JRDesignTextField)t;
							break;
						}
					}

					x = target.getX();
					y = target.getY();
					// find all measures that have the same x & y (conditional formatting)
					for (int j = 0; j < measures.length; j++) {
						if (measures[j].getX() == x && measures[j].getY() == y) {
							do {
								((JRDesignElement) measures[j]).setY(0);
								measures[j].setX(xPos);
								j++;
							} while (j < measures.length && measures[j].getX() == x && measures[j].getY() == y);
						}
					}
					
					//increment to next horizontal position
					xPos += measureColumn.getWidth();	
					
					//add right border
					//drawBorder(measureColumn.getLineBox(), false, true, false, false);
				}
				band.setHeight(maxHgt);
				columnGroup.setHeight(maxHgt);
				if (cs.cell != null){
					cs.cell.setWidth(xPos);
					cs.cell.setHeight(maxHgt);
				}
		
			}
		}
		
		return columnHeight;
		
	}
	
	private void alignPivotMeasureLabelsRowWise(CrosstabSetup cs, 
									List<JRDesignTextField> measures,
									List<JRDesignTextField> measureDesigns,
									int height, int posX, int posY, int pivotMeasureMaxWidth,
									List<JRDesignCrosstabCell> pivotTotalCells,
									boolean isPdf) throws JRException{
		
		//Pick the end of the header cells and gets the required properties for us to align and format.
		//Side effects - the fonts, colors, etc is inherit..
		JRDesignCellContents cells = (JRDesignCellContents) cs.crosstab.getHeaderCell();

		if (cells != null){
			/*
			JRDesignBand band = (JRDesignBand)cells.getElementGroup();
			if (band != null){
				JRElement[] els = band.getElements();
			*/
			JRElement[] els = cells.getElements();
				if (els.length > 0){
					JRDesignTextField measureLabel = (JRDesignTextField) els[els.length - 1].clone();
					measureLabel.setX(measureLabel.getX() + measureLabel.getWidth());
					measureLabel.setExpression(createStringExpression("Measures"));
					measureLabel.setWidth(pivotMeasureMaxWidth);
					
					//Draw borders
					/*
					JRLineBox box = measureLabel.getLineBox();
					box.getLeftPen().setLineColor(Color.BLACK);
					box.getLeftPen().setLineWidth(1.0f);
					box.getRightPen().setLineColor(Color.BLACK);
					box.getRightPen().setLineWidth(1.0f);
					box.getTopPen().setLineColor(Color.BLACK);
					box.getTopPen().setLineWidth(1.0f);
					*/
					//Append to the end
//					band.addElement(measureLabel);
					cells.addElement(measureLabel);
					
				}
//			}
		}
		
		
		//Add the row group
		JRDesignCrosstabRowGroup rowGroup = new JRDesignCrosstabRowGroup();
		rowGroup.setName("Measure Labels Row");
		rowGroup.setWidth(pivotMeasureMaxWidth);
		rowGroup.setPosition(CrosstabRowPositionEnum.TOP);
		
		JRDesignCrosstabBucket bucket = new JRDesignCrosstabBucket();
		bucket.setValueClassName(String.class.getName());
		JRDesignExpression bucketExp = new JRDesignExpression();
		bucketExp.setText("new String(\"\")");
		bucket.setExpression(bucketExp);
		rowGroup.setBucket(bucket);
		
		JRDesignCellContents colHeadContents = new JRDesignCellContents();
//		JRDesignBand band = new JRDesignBand();
//		colHeadContents.addElementGroup(band);
		int pY = 0;
		for(int i = 0; i < measures.size(); i++){
			
			//Add measure labels
			JRDesignTextField measureColumn = (JRDesignTextField)measureDesigns.get(i).clone();
			measureColumn.setWidth(pivotMeasureMaxWidth);
			measureColumn.setStretchWithOverflow(true);
			measureColumn.setStretchType(StretchTypeEnum.NO_STRETCH);
			measureColumn.setHorizontalAlignment(HorizontalAlignEnum.LEFT);
			measureColumn.setVerticalAlignment(VerticalAlignEnum.TOP);
			measureColumn.setPositionType(PositionTypeEnum.FLOAT);
			
			measureColumn.setX(0);
			measureColumn.setY(pY);
			
			pY += measureColumn.getHeight();
			
//			band.addElement(measureColumn);
			colHeadContents.addElement(measureColumn);
		}
//		band.setHeight(pY);
		
		rowGroup.setBucket(bucket);
		rowGroup.setHeader(colHeadContents);
		
		pivotRowResizeHeight(cs, pY);
	
		cs.cell.setHeight(pY);
		
		//Calculate total width, and make sure that label's width is the same as our column's width
		constraintLabelWidthToColumnWidth(measures, measureDesigns, pivotMeasureMaxWidth, pivotTotalCells, isPdf);
		pivotColumnResizeWidth(cs, pivotMeasureMaxWidth);
		
		cs.crosstab.addRowGroup(rowGroup);
	}
	
	private void pivotRowResizeHeight(CrosstabSetup cs, int height){
		JRCrosstabRowGroup[] rows = cs.crosstab.getRowGroups();
		for(int i = 0; i < rows.length; i++){
			JRElement[] elems = rows[i].getHeader().getElements();
			if (elems.length == 1){
				JRDesignTextField text = (JRDesignTextField) elems[0];
				text.setHeight(height);
			}
		}
	}
	
	
	private JRDesignCrosstabColumnGroup pivotColumnResizeWidth(CrosstabSetup cs, int width){
		//Resize the width of the columns
		JRDesignCrosstabColumnGroup group = null;
		JRCrosstabColumnGroup[] groups = cs.crosstab.getColumnGroups();
		for(int i = 0; i < groups.length; i++){
			 group = (JRDesignCrosstabColumnGroup)groups[i];
			 JRElement[] cols = group.getHeader().getElements();
			for(int y = 0; y < cols.length; y++){
				JRDesignElement col = (JRDesignElement)cols[y];
				col.setWidth(width);
			}
			
			//Resize the total header as well
			JRDesignCellContents totalHeader = (JRDesignCellContents)group.getTotalHeader();
			if (totalHeader != null){
				JRElement[] totalCols = totalHeader.getElements();
				for(int x = 0; x < totalCols.length; x++){
					totalCols[x].setWidth(width);
				}
			}
		}
		cs.cell.setWidth(width);
		return group;
	}
	
	private JRDesignExpression createStringExpression(String text){
		JRDesignExpression expr = new JRDesignExpression();
		expr.setText("new String(\"" + text + "\")");
		
		return expr;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<AdhocEntity> getEntities() {
		if (entities == null) {
			entities = new ArrayList<AdhocEntity>();
		}
		return entities;
	}

	public List<ReportFilter> getReportFilters() {
		List<ReportFilter> list = new ArrayList<ReportFilter>();
		if (this.reportFilters != null) {
			list.addAll(this.reportFilters);
		}
		return Collections.unmodifiableList(list);
	}
	
	public List<QueryFilter> getFilters() {
		List<QueryFilter> list = new ArrayList<QueryFilter>();
		if (this.filters != null) {
			list.addAll(this.filters);
		}
		
		for (AdhocColumn column : columns) {
			if (column.getFilters() != null) {
				list.addAll(column.getFilters());
			}
		}
		return Collections.unmodifiableList(list);
	}
	
	public void setQueryFilters(List<QueryFilter> list)
	{
		if(this.filters != null){
			this.filters.addAll(list);
		}else{
			this.filters = list;
		}
	}
	
	public List<DisplayFilter> getDisplayFilters() {
		return this.displayFilters;
	}
	
	public List<DisplayFilter> getQueryDisplayFilters() {
		return getQueryDisplayFilters(false);
	}
	public List<DisplayFilter> getQueryDisplayFilters(boolean useDisplayNames) {
		List<DisplayFilter> processedDFilters = new ArrayList<DisplayFilter>();
		List<DisplayFilter> retDisplayFilters = new ArrayList<DisplayFilter>();
		for (int i = 0; i < this.displayFilters.size(); i++)
		{
			DisplayFilter df = displayFilters.get(i);
			if (df.getTableName() == null && df.getType() == DisplayFilter.Type.Predicate)
			{
				if(!processedDFilters.contains(df))
				{
					String measure = df.getColumn();
					String logicalOperator = getDFLogicalOperator(displayFilters, i);
					for (int j = i + 1; j < displayFilters.size(); j++)
					{
						DisplayFilter df2 = displayFilters.get(j);
						if (df2.containsMeasure(measure))
						{
							df = new DisplayFilter(df, logicalOperator, df2);
							processedDFilters.add(df2);
						}
					}
					retDisplayFilters.add(df);					
				}
			} else
			{
				if (!processedDFilters.contains(df))
				{
					if ((df.getOperator() == DisplayFilter.Operator.Equal) || (df.getOperator() == DisplayFilter.Operator.Like))
					{
						String dim = df.getTableName();
						String col = df.getColumn();
						if (useDisplayNames) {
							try {
								DisplayFilter f = (DisplayFilter) df.clone();
								for (AdhocColumn ac : columns)
								{
									if (((ac.getDimension() == null && dim == null) || (dim != null && dim.equals(ac.getDimension()))) &&
											(col.equals(ac.getColumn())))
									{
										f.setColumnName(ac.getDisplayName());
										break;
									}
									else if ("EXPR".equals(dim) && ac.getType() == ColumnType.Expression && col.equals(ac.getFieldName())) {
										break;
									}
								}
								df = f;
							} catch (CloneNotSupportedException e) {
								logger.error(e,e);
							}
						}
						/*
						 * Look for implicit ors - i.e. other filters on same dimension and column with an equal or like operator
						 */
						for (int j = i + 1; j < displayFilters.size(); j++)
						{
							DisplayFilter df2 = displayFilters.get(j);
							if (df2.containsAttribute(dim, col))
							{
								DisplayFilter df3 = null;
								try {
									df3 = (DisplayFilter) df2.clone();
								} catch (CloneNotSupportedException e) {
									logger.error(e, e);
								}
								processedDFilters.add(df2);
								if (useDisplayNames && df3 != null) {
									for (AdhocColumn ac : columns)
									{
										if (((ac.getDimension() == null && dim == null) || (dim != null && dim.equals(ac.getDimension()))) &&
												(col.equals(ac.getColumn())))
										{
											df3.setColumnName(ac.getDisplayName());
											break;
										}
									}
									df2 = df3;
								}
								df = new DisplayFilter(df, "OR", df2);
							}
						}
					}
					retDisplayFilters.add(df);
				}
			}
		}
		for (Iterator<DisplayFilter> dfIter = retDisplayFilters.iterator(); dfIter.hasNext(); ) {
			DisplayFilter df = dfIter.next();
			setupDisplayFilters(dfIter, df);
		}
		return retDisplayFilters;
	}
	
	private void setupDisplayFilters(Iterator<DisplayFilter> dfIter, DisplayFilter df) {
		if (df.getType() == Type.Predicate) {
			// df.columnName is the label
			for (AdhocColumn ac: getColumns()) {
				if ((df.getReportIndex() >= 0 && df.getReportIndex() == ac.reportIndex) ||
					(ac.getLabelString().equals(df.getColumn()))) {
					if (ac.getType() == ColumnType.Ratio)
						dfIter.remove();
					
					df.alias = ac.getFieldName();
				}
			}
		}
		else if (df.getType() == Type.LogicalOperator) {
			for (Iterator<DisplayFilter> dfIter1 = df.getFilterList().iterator(); dfIter1.hasNext(); ) { 
				setupDisplayFilters(dfIter1, dfIter1.next());
			}
		}
	}

	public ChartOptions getChartOptions() {
		return chartOptions;
	}

	public void setChartOptions(ChartOptions chartOptions) {
		this.chartOptions = chartOptions;
	}

	public boolean isGrandtotals() {
		return grandtotals;
	}

	private void setGrandtotals(boolean grandtotals) {
		this.grandtotals = grandtotals;
		if (grandtotals == true) {
			PageInfo pageInfo = this.getPageInfo();
			if (pageInfo != null && pageInfo.getSummaryBandHeight() < getRowHeight()) {
				pageInfo.setSummaryBandHeight(getRowHeight());
			}
		}
	}

	public boolean isIncludeTable() {
		return includeTable;
	}

	public void setIncludeTable(boolean includeTable) {
		this.includeTable = includeTable;
	}

	public int getPageHeight() {
		return pageHeight;
	}

	public void setPageHeight(int pageHeight) {
		this.pageHeight = pageHeight;
	}

	public int getPageWidth() {
		return pageWidth;
	}

	public void setPageWidth(int pageWidth) {
		this.pageWidth = pageWidth;
	}

	public OrientationEnum getPageOrientation() {
		return pageOrientation;
	}

	public void setPageOrientation(OrientationEnum pageOrientation) {
		this.pageOrientation = pageOrientation;
	}

	public String getPageSize() {
		return pageSize;
	}

	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
		PageSizeProperty prop = PageInfo.getPageSize(pageSize);
		if (prop != null && prop.pixelHeight > 0 && prop.pixelWidth > 0) {
			this.setPageHeight(prop.pixelHeight);
			this.setPageWidth(prop.pixelWidth);
		}
	}

	public int getLeftMargin() {
		return leftMargin;
	}

	public void setLeftMargin(int leftMargin) {
		this.leftMargin = leftMargin;
	}

	public int getRightMargin() {
		return rightMargin;
	}

	public void setRightMargin(int rightMargin) {
		this.rightMargin = rightMargin;
	}

	public int getTopMargin() {
		return topMargin;
	}

	public void setTopMargin(int topMargin) {
		this.topMargin = topMargin;
	}

	public int getBottomMargin() {
		return bottomMargin;
	}

	public void setBottomMargin(int bottomMargin) {
		this.bottomMargin = bottomMargin;
	}

	public int getTitleHeight() {
		return titleHeight;
	}

	public void setTitleHeight(int titleHeight) {
		this.titleHeight = titleHeight;
	}

	public int getRowHeight() {
		return rowHeight;
	}

	public void setRowHeight(int rowHeight) {
		this.rowHeight = rowHeight;
	}

	public boolean isAlternateRowColors() {
		return alternateRowColors;
	}

	public void setAlternateRowColors(boolean alternateRowColors) {
		this.alternateRowColors = alternateRowColors;
	}

	public String getFirstAlternateRGB() {
		return firstAlternateRGB;
	}

	public void setFirstAlternateRGB(String firstAlternateRGB) {
		this.firstAlternateRGB = firstAlternateRGB;
	}

	public String getSecondAlternateAltRGB() {
		return secondAlternateAltRGB;
	}

	public void setSecondAlternateAltRGB(String secondAlternateAltRGB) {
		this.secondAlternateAltRGB = secondAlternateAltRGB;
	}

	public Color getFirstAlternateColor() {
		return ColorUtils.stringToColor(getFirstAlternateRGB());
	}

	public Color getSecondAlternateAlt() {
		return ColorUtils.stringToColor(getSecondAlternateAltRGB());
	}

	public int getNumGroups() {
		return numGroups;
	}

	public void setNumGroups(int numGroups) {
		this.numGroups = numGroups;
		boolean[] dnpbgs = new boolean[numGroups];
		if (numGroups > 0 && doNotPageBreakGroup != null && doNotPageBreakGroup.length > 0) {
			int size = Math.min(numGroups, doNotPageBreakGroup.length);
			for (int i = 0; i < size; i++) {
				dnpbgs[i] = doNotPageBreakGroup[i];
			}
		}
		this.doNotPageBreakGroup = dnpbgs;
		PageInfo pageInfo = this.getPageInfo();
		if (pageInfo != null) {
			pageInfo.setGroups(numGroups, this);
		}
	}

	public String getSeparatorCSV() {
		return separatorCSV;
	}

	public void setSeparatorCSV(String separatorCSV) {
		this.separatorCSV = separatorCSV;
	}

	private boolean isAutomaticLayout() {
		return automaticLayout;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getFileName() {
		if (this.getPath() == null || this.getPath().trim().length() == 0) {
			return this.getName();
		}
		return this.getPath() + "/" + this.getName();
	}
	public List<DisplayExpression> getExpressionList() {
		return expressionList;
	}

	public String getQueryString(Repository r, boolean substitutePrompts)
	{
		Query q = null;
		StringBuilder sb = new StringBuilder();
		String reportQuery = null;
		try
		{
			q = getQuery(r);
			if(q != null)
			{
				q.setFullOuterJoin(true);
				for (DimensionColumnNav dcn : q.dimensionColumns)
				{
					String display = dcn.getDisplayName();
					String processedDisplay = Util.removeSystemGeneratedFieldNumber(display);
					if (display.equals(dcn.getColumnName()) || processedDisplay.equals(dcn.getColumnName()))
					{
						dcn.setDisplayName(null);
					}
					else
					{
						dcn.setDisplayName(processedDisplay);					
					}
				}
				for (MeasureColumnNav mcn : q.measureColumns)
				{
					String display = mcn.getDisplayName();
					String processedDisplay = Util.removeSystemGeneratedFieldNumber(display);					
					if (display.equals(mcn.getMeasureName()) || processedDisplay.equals(mcn.getMeasureName()))
					{
						mcn.setDisplayName(null);
					}
					else
					{
						mcn.setDisplayName(processedDisplay);
					}
				}
			}
			List<DisplayExpression> displayExpressionList = this.getExpressionList();
			if (q != null)
			{
				sb.append(q.getLogicalQueryString(substitutePrompts, displayExpressionList, true));
			}
			if (displayFilters != null)
			{
				sb.append(DisplayFilter.getQueryFilters(this.getQueryDisplayFilters(), substitutePrompts, r.isUseNewQueryLanguage()));
			}
			String dashletPrompts = getColumnSelectors().toString();
			Map<String, ColumnSelector> selectors = null;
			if (dashletPrompts != null && Util.hasNonWhiteSpaceCharacters(dashletPrompts)) {
				 selectors = parseColumnSelectors(dashletPrompts);
			}
			List<DisplayOrder> displayOrders = getDisplayOrders(selectors);
			if (displayOrders != null)
			{
				sb.append(DisplayOrder.getQueryOrders(displayOrders, r.isUseNewQueryLanguage()));
			}
			reportQuery = sb.toString();
			
		} catch (Exception e)
		{
			logger.error("Can not create report query", e);
			reportQuery = "Invalid report query";
		}
		return reportQuery;
	}

	/**
	 * Return a list of display orderings that corresponds to the current report sorting specification
	 * @param selectors 
	 * 
	 * @return
	 */
	public List<DisplayOrder> getDisplayOrders(Map<String, ColumnSelector> selectors)
	{
		List<DisplayOrder> dorders = new ArrayList<DisplayOrder>();
		for (AdhocSort as : getSorts())
		{
			AdhocColumn rc = as.getColumn();
			if (rc.getColumn() == null || rc.getColumn().trim().length() == 0) {
				continue;
			}
			if (selectors != null) {
				for (ColumnSelector cs : selectors.values()) {
					AdhocColumn ac = cs.getOriginalColumn();
					if (rc.getExpressionString() != null && rc.getExpressionString().equals(ac.getExpressionString())) {
						rc = cs.getAdhocColumn();
						break;
					}
					else if (rc.getType() == ColumnType.Dimension && rc.getDimension().equals(ac.getDimension()) && rc.getColumn().equals(ac.getColumn())) {
						rc = cs.getAdhocColumn();
						break;
					}
					else if (rc.getType() == ColumnType.Measure && rc.getColumn().equals(ac.getColumn())) {
						rc = cs.getAdhocColumn();
						break;
					}
				}
			}
			if (rc.getExpression() != null) {
				DisplayOrder dorder = new DisplayOrder("EXPR", rc.getExpression().getName(), as.getOrder() == SortOrder.ASCENDING ? true : false);
				dorders.add(dorder);
			}
			else if (as.getType() == SortType.DISPLAY) {
				if (rc.getType() == ColumnType.Dimension) {
					DisplayOrder dorder = new DisplayOrder(rc.getDimension(), rc.getColumn(), as.getOrder() == SortOrder.ASCENDING ? true : false);
					dorders.add(dorder);
				}
				else if (rc.getType() == ColumnType.Measure) {
					DisplayOrder dorder = new DisplayOrder(rc.getColumn(), as.getOrder() == SortOrder.ASCENDING ? true : false);
					dorders.add(dorder);
				}
			}
		}
		return (dorders);
	}


	public List<AdhocColumn> getColumns() {
		if (columns == null) {
			columns = new ArrayList<AdhocColumn>();
		}
		return columns;
	}
	
	private void setupGroups(JasperDesign jd, PageInfo pi) throws JRException
	{
		String[] keys = new String[numGroups];
		boolean[] headers = new boolean[numGroups];
		boolean[] footers = new boolean[numGroups];
		for (int i = 0; i < numGroups; i++)
			keys[i] = "";
		for (AdhocEntity rc : this.getEntities())
		{
			if (rc.getBand()== Band.GroupHeader || rc.getBand() == Band.GroupFooter)
			{
				if (rc instanceof AdhocColumn && ((AdhocColumn)rc).treatAsDimension())
				{
					if (keys[rc.getGroupNumber()].length() > 0)
						keys[rc.getGroupNumber()] += "+";
					keys[rc.getGroupNumber()] += "$F{" + ((AdhocColumn)rc).getFieldName() + "}.toString()+\":\"";
				}
				if (rc.getBand() == Band.GroupHeader)
					headers[rc.getGroupNumber()] = true;
				else
					footers[rc.getGroupNumber()] = true;
			}
		}
		pi.groupHeaderBands = new JRDesignBand[numGroups];
		pi.groupFooterBands = new JRDesignBand[numGroups];
		// default these values if not set...
		if (pi.groupHeaders == null || pi.groupHeaders.length < numGroups) {
			PageInfo.Band[] groupBandHeaderHeight = new PageInfo.Band[numGroups];
			int i = 0;
			if (pi.groupHeaders != null) {
				for (; i < pi.groupHeaders.length && i < numGroups; i++) {
					groupBandHeaderHeight[i] = pi.groupHeaders[i];
				}
			}
			for (; i < numGroups; i++) {
				groupBandHeaderHeight[i] = new PageInfo.Band(rowHeight);
			}
			pi.groupHeaders = groupBandHeaderHeight;
		}
		if (pi.groupFooters == null || pi.groupFooters.length < numGroups) {
			PageInfo.Band[] groupBandFooterHeights = new PageInfo.Band[numGroups];
			int i = 0;
			if (pi.groupFooters != null) {
				for (; i < pi.groupFooters.length && i < numGroups; i++) {
					groupBandFooterHeights[i] = pi.groupFooters[i];
				}
			}
			for (; i < numGroups; i++) {
				groupBandFooterHeights[i] = new PageInfo.Band(rowHeight);
			}
			pi.groupFooters = groupBandFooterHeights;
		}
		if (pi.doNotBreakGroups == null || pi.doNotBreakGroups.length < numGroups) {
			boolean[] breakGroups = new boolean[numGroups];
			int i = 0;
			if (pi.doNotBreakGroups != null) {
				for (; i < pi.doNotBreakGroups.length && i < numGroups; i++) {
					breakGroups[i] = pi.doNotBreakGroups[i];
				}
			}
			for (; i < numGroups; i++) {
				breakGroups[i] = false;
			}
			pi.doNotBreakGroups = breakGroups;
		}
		pi.groups = new JRDesignGroup[numGroups];
		for (int i = 0; i < numGroups; i++)
		{
			/*
			 * Only setup a group if there's something in it
			 */
			if (headers[i] || footers[i])
			{
				JRDesignGroup jrg = new JRDesignGroup();
				JRDesignExpression jrge = new JRDesignExpression();
				jrge.setText(keys[i]);
				jrg.setExpression(jrge);
				jrg.setName("Group " + i);
				jrg.setKeepTogether(pi.doNotBreakGroups != null && i < pi.doNotBreakGroups.length ? pi.doNotBreakGroups[i] : false);
				jrg.setStartNewPage(pi.startGroupOnNewPage != null && i < pi.startGroupOnNewPage.length ? pi.startGroupOnNewPage[i] : false);
				jd.addGroup(jrg);
				pi.groups[i] = jrg;
				/*
				 * Only populate a band if there's something in it
				 */
				if (headers[i])
				{
					pi.groupHeaderBands[i] = createBand(pi.groupHeaders[i]);
					pageBreak(pi.groupHeaderBands[i], pi.groupHeaders[i].forcePageBreak);
					JRSection section = jrg.getGroupHeaderSection();
					if (section != null && section instanceof JRDesignSection) {
						((JRDesignSection)section).addBand(pi.groupHeaderBands[i]);
					}
				}
				if (footers[i])
				{
					pi.groupFooterBands[i] = createBand(pi.groupFooters[i]);
					pageBreak(pi.groupFooterBands[i], pi.groupFooters[i].forcePageBreak);
					JRSection section = jrg.getGroupFooterSection();
					if (section != null && section instanceof JRDesignSection) {
						((JRDesignSection)section).addBand(pi.groupFooterBands[i]);
					}
				}
			}
		}
	}
	private void pageBreak(JRDesignBand designBand,
			ForcePageBreak forcePageBreak) {
	}

	/**
	 * 
	 * @param pi
	 * @param summaryBand
	 * @param numMeasures
	 * @param numOnRows
	 * @param crossTabHeight
	 * @param crossTabWidth
	 * @return CrosstabSetup 
	 * @throws JRException
	 */
	private CrosstabSetup setupPivot(PageInfo pi, 
									 JRDesignBand summaryBand, int numMeasures, 
									 int numOnRows, int crossTabHeight, int crossTabWidth)
			throws JRException
	{
//		Page Summary
		summaryBand.setHeight(crossTabHeight);
		CrosstabSetup cs = new CrosstabSetup();
		JRDesignCrosstab crosstab = new JRDesignCrosstab();
		JRDesignExpression mapExpression = new JRDesignExpression();
		mapExpression.setText("$P{REPORT_PARAMETERS_MAP}");
		crosstab.setParametersMapExpression(mapExpression);
		
		JRDesignCrosstabParameter parameter = new JRDesignCrosstabParameter();
		parameter.setName(JRParameter.REPORT_DATA_SOURCE);
		parameter.setValueClass(JRDataSource.class);
		crosstab.addParameter(parameter);
		
		for (AdhocColumn ac : getColumns()) {
			List<ColumnSelector> css = ac.getColumnSelectors();
			if (css != null && css.size() > 0) {
				JRDesignCrosstabParameter param = new JRDesignCrosstabParameter();
				param.setValueClass(String.class);
				param.setName(ac.getLabelString() + ColumnSelector.LABEL);
				crosstab.addParameter(param);
				
				JRDesignCrosstabParameter dimension = new JRDesignCrosstabParameter();
				dimension.setValueClass(String.class);
				dimension.setName(ac.getLabelString() + AdhocColumn.DIMENSION);
				crosstab.addParameter(dimension);
				
				JRDesignCrosstabParameter column = new JRDesignCrosstabParameter();
				column.setValueClass(String.class);
				column.setName(ac.getLabelString() + AdhocColumn.COLUMN);
				crosstab.addParameter(column);
			}
		}
		
		cs.crosstab = crosstab;
		crosstab.setX(0);
		crosstab.setY(0);
		crosstab.setWidth(crossTabWidth);
		crosstab.setHeight(crossTabHeight);
		crosstab.setMode(ModeEnum.OPAQUE);
		summaryBand.addElement(crosstab);
		/*
		 * Generate default row if needed (i.e. nothing on rows - so need a blank, fixed value)
		 */
		if (numOnRows == 0)
		{
			JRDesignCrosstabRowGroup rowGroup = new JRDesignCrosstabRowGroup();
			rowGroup.setName("Default");
			rowGroup.setWidth(0);
			crosstab.addRowGroup(rowGroup);
			JRDesignCrosstabBucket bucket = new JRDesignCrosstabBucket();
			bucket.setValueClassName(Integer.class.getName());
			JRDesignExpression bucketExp = new JRDesignExpression();
			bucketExp.setText("Integer.valueOf(0)");
			bucket.setExpression(bucketExp);
			rowGroup.setBucket(bucket);
		}
//		Contents for the crosstab cell
		JRDesignCellContents contents = new JRDesignCellContents();
		JRDesignCrosstabCell cell = new JRDesignCrosstabCell();
		cell.setWidth(pi.pivotColumnWidth + 2);
		cell.setHeight((rowHeight + 2) * numMeasures);
		cell.setContents(contents);
		crosstab.addCell(cell);
		cs.cell = cell;
		cs.contents = contents;
//		Set column break offset
		crosstab.setColumnBreakOffset(PageInfo.DEFAULT_PIVOT_COLUMN_BREAK_OFFSET);
		return (cs);
	}
	
	private void setupPivotMeasure(JasperDesign jasperDesign, Repository r, String aggRule, String fieldName, @SuppressWarnings("rawtypes") Class fieldClass, 
			JRDesignTextField detailTextField, CrosstabSetup cs, AdhocColumn column)
			throws JRException
	{
		JRDesignCrosstabMeasure crosstabMeasure = new JRDesignCrosstabMeasure();
		if (aggRule.equals("SUM"))
			crosstabMeasure.setCalculation(CalculationEnum.SUM);
		else if (aggRule.equals("AVG"))
			crosstabMeasure.setCalculation(CalculationEnum.AVERAGE);
		else if (aggRule.equals("COUNT"))
			crosstabMeasure.setCalculation(CalculationEnum.SUM);
		else if (aggRule.equals("MIN"))
			crosstabMeasure.setCalculation(CalculationEnum.LOWEST);
		else if (aggRule.equals("MAX"))
			crosstabMeasure.setCalculation(CalculationEnum.HIGHEST);
		else
			crosstabMeasure.setCalculation(CalculationEnum.SUM);
		
		boolean isToTimeFormat = false;
		String toTimeFields = "";
		String toTimeFormat = "";
		String format = column.getValidFormat();
		if (format != null)
		{
			if (format.toUpperCase().startsWith(ToTime.TO_TIME_FORMAT_PREFIX))
			{
				String formatArgs[]=(format.substring(format.indexOf("(") + 1, format.lastIndexOf(")"))).split("','");
				toTimeFields = formatArgs[0].substring(formatArgs[0].indexOf("'") + 1);
				toTimeFormat = formatArgs[1].substring(0, formatArgs[1].indexOf("'"));
				fieldClass = BirstInterval.class;
				isToTimeFormat = true;
			}
		}
				
		JRDesignExpression crosstabMeasureExp = new JRDesignExpression();
		if (GregorianCalendar.class.equals(fieldClass) && !column.isSortablePivotField()) {
			fieldClass = Date.class;
			boolean isDateTime = column.isDateTime(r);
			if(isDateTime)
				crosstabMeasureExp.setText("$F{" + fieldName + "}.getTime()");
			else // Need to apply reverse delta first to Dates, so that when jasper applies delta it will not change the date
				crosstabMeasureExp.setText("com.successmetricsinc.util.DateUtil.applyReverseDeltaForDate(" + "$F{" + fieldName + "}.getTime(), " +
						"com.successmetricsinc.UserBean.getParentThreadUserBean().getRepository().getServerParameters().getProcessingTimeZone(), " +
						"com.successmetricsinc.UserBean.getParentThreadUserBean().getTimeZone())");
		}
		else if (fieldClass.equals(String.class) && isToTimeFormat)
		{
			crosstabMeasureExp.setText("com.successmetricsinc.transformation.integer.ToTime.evaluate(" + "$F{" + fieldName + "},\""+toTimeFields+"\",\""+toTimeFormat+"\")");
		}
		else {
			StringBuilder measureExpressionText = new StringBuilder("$F{" + fieldName + "}");
			crosstabMeasureExp.setText(measureExpressionText.toString());
		}
		crosstabMeasure.setValueClassName(fieldClass.getName());
		crosstabMeasure.setValueExpression(crosstabMeasureExp);
		crosstabMeasure.setName(fieldName);
		if (column.getType() == ColumnType.Ratio) {
			crosstabMeasure.setIncrementerFactoryClassName("com.successmetricsinc.query.BirstRatioIncrementerFactory");
		}
		if (isToTimeFormat) {
			crosstabMeasure.setIncrementerFactoryClassName("com.successmetricsinc.query.BirstIntervalIncrementorFactory");
		}
		cs.crosstab.addMeasure(crosstabMeasure);
		JRDesignExpression crosstabMeasureFieldExp = new JRDesignExpression();
		
		int index = column.getPivotValuePctOfCol();
		if (index >= 0) {
			for (AdhocColumn ac : getColumns()) {
				if (ac.getReportIndex() == index) {
					StringBuilder measureExpressionText = new StringBuilder();
					measureExpressionText.append("Double.valueOf(");
					measureExpressionText.append("$V{" + fieldName + "}");
					measureExpressionText.append(".doubleValue()/$V{");
					measureExpressionText.append(fieldName);
					measureExpressionText.append('_');
					measureExpressionText.append(ac.getFieldName());
					measureExpressionText.append("_ALL}.doubleValue())");
					crosstabMeasureFieldExp.setText(measureExpressionText.toString());
					fieldClass = Double.class;
				}
			}
		}
		else {
			crosstabMeasureFieldExp.setText("$V{" + fieldName + "}");
		}
			
		detailTextField.setExpression(crosstabMeasureFieldExp);
		
		if (this.isPivotMeasureLabelsColumnWise()){
			detailTextField.setStretchType(StretchTypeEnum.RELATIVE_TO_BAND_HEIGHT);
		}else{
			detailTextField.setStretchType(StretchTypeEnum.NO_STRETCH);
			detailTextField.setPositionType(PositionTypeEnum.FLOAT);
		}
		
		cs.contents.addElement(detailTextField);
	}

	/**
	 * Return the URL parameter string that needs to be added to drill URLs to include the current prompts
	 * 
	 * @param jasperDesign
	 * @param q
	 * @return
	 * @throws JRException
	 */
	private void addPromptParameters(JasperDesign jasperDesign) throws JRException
	{
		Map<String, Object> qpmap = new HashMap<String, Object>();
		for (QueryFilter qf : this.getFilters()) {
			qf.getParameterMap(qpmap);
		}
		if (displayFilters != null) {
			for (DisplayFilter df : displayFilters)
			{
				df.getParameterMap(qpmap);
			}
		}
		for (Iterator<Entry<String, Object>> it = qpmap.entrySet().iterator(); it.hasNext();)
		{
			Entry<String, Object> en = it.next();
			if (jasperDesign.getParametersMap().containsKey(en.getKey()))
				continue;
			
			JRDesignParameter jrdp = new JRDesignParameter();
			@SuppressWarnings("rawtypes")
			Class cl = en.getValue() == null ? String.class : en.getValue().getClass();
			JRDesignExpression jrde = new JRDesignExpression();
			jrdp.setValueClass(cl);
			if (cl == String.class)
				jrde.setText("\"" + en.getValue() + "\"");
			else if (cl == Integer.class)
				jrde.setText("Integer.valueOf(" + en.getValue() + ")");
			else if (cl == Double.class || Number.class.isInstance(en.getValue()))
				jrde.setText("Double.valueOf(" + en.getValue() + ")");
			jrdp.setDefaultValueExpression(jrde);
			jrdp.setName(en.getKey());
			jasperDesign.addParameter(jrdp);
		}
	}
	
/**
 * 
 * @param report
 * @param column
 * @param qs
 * @param fieldName
 * @param fieldClass
 * @param detailTextField
 * @param cs
 * @param format
 * @param pi
 * @param bi
 * @param backColor
 * @throws JRException
 */
	private int setupPivotDimension(AdhocReport report, Repository r, AdhocColumn column, String fieldName, 
									@SuppressWarnings("rawtypes") Class fieldClass, JRDesignTextField detailTextField, 
								    CrosstabSetup cs, String format, PageInfo pi, 
								    BindInfo bi, Color backColor, int pivotHeaderHeight, JasperDesign jasperDesign,
								    List<JRDesignCrosstabCell> totalCells,
								    boolean isPdf,
								    SMIWebUserBean ub) throws JRException
	{
		JRDesignCrosstabBucket bucket = new JRDesignCrosstabBucket();
		List<AdhocSort> sortEntities = report.getSorts();
		if (sortEntities != null && !column.isOlapDimension() && !column.isSortablePivotField()) {
			for (AdhocSort as: sortEntities) {
				if (as.getColumn().reportIndex == column.reportIndex) {
					if (as.getOrder() == SortOrder.DESCENDING)
						bucket.setOrder(SortOrderEnum.DESCENDING);
				}
			}
		}
		JRDesignExpression crosstabDimExp = new JRDesignExpression();
		if(fieldClass.equals(GregorianCalendar.class) && !column.isSortablePivotField()) 
		{
			// convert to Date type which jasper can understand
			bucket.setValueClassName(Date.class.getName());
			crosstabDimExp.setText("$F{" + fieldName + "}.getTime()");									
		}
		else
		{
			bucket.setValueClassName(column.isSortablePivotField() ? SortablePivotField.class.getName() : fieldClass.getName());
			crosstabDimExp.setText("$F{" + fieldName + "}");
		}
		bucket.setExpression(crosstabDimExp);
		JRDesignExpression colHeadExp = new JRDesignExpression();
		colHeadExp.setText("$V{" + fieldName + "}");
		if (fieldClass.equals(BirstOlapField.class) && column.getDataType().equals("Date")) {
			colHeadExp.setText("((GregorianCalendar)$V{" + fieldName + "}.getValue()).getTime()");
		}
		
		JRDesignCellContents colHeadContents = new JRDesignCellContents();
		//this.drawBorderAllPt5px(colHeadContents.getLineBox());
		detailTextField.setExpression(colHeadExp);
		
		colHeadContents.setMode(detailTextField.getModeValue());
		colHeadContents.setBackcolor(detailTextField.getBackcolor());
		/* In pivot, these columns are basically headers, so use header formatting */
//		Util.copyTextFieldFormat(headerText, detailTextField, false, false);
		detailTextField.setFontSize(column.getFont().getSize());
		detailTextField.setFontName(column.getFont().getFamily());
		//detailTextField.setWidth(pi.pivotColumnWidth);
		detailTextField.setHeight(report.rowHeight);
		//detailTextField.setBorder((byte) 0);
		JRDesignCellContents colTotalContents = null;
		JRDesignTextField colTotalField = null;
		if (column.isPivotTotal())
		{
			/*
			 * Setup row/column total contents
			 */
			colTotalContents = new JRDesignCellContents();
			//drawBorder(colTotalContents.getLineBox(), true, true, false, false);
			
			colTotalField = JasperUtil.createTextFieldSameFormat(detailTextField);
			colTotalField.setVerticalAlignment(VerticalAlignEnum.TOP);
			colTotalField.setStretchType(StretchTypeEnum.RELATIVE_TO_BAND_HEIGHT);
			JRDesignExpression colTotalExpression = new JRDesignExpression();
			colTotalExpression.setText("\"Total\"");
			colTotalField.setExpression(colTotalExpression);

			colTotalContents.addElement(colTotalField);
			colTotalField.setX(0);
			colTotalField.setY(0);
			// colTotalContents width and height aren't set
			//colTotalField.setWidth(colTotalContents.getWidth());
//			colTotalField.setHeight(colTotalContents.getHeight());
			colTotalContents.setMode(detailTextField.getModeValue());
			colTotalContents.setBackcolor(detailTextField.getBackcolor());
			
		}
		/*
		 * Create cells for totals
		 */
		JRDesignCrosstabCell totCell = null;
		JRDesignTextField totCellField = null;
		if (column.isPivotTotal())
		{
			/*
			 * Create total cell
			 */
			totCell = new JRDesignCrosstabCell();
			JRDesignCellContents totContents = new JRDesignCellContents();
			for (AdhocColumn trc : report.columns)
			{
				if (trc.isShowInReport() == false)
					continue;
				
				totCellField = (JRDesignTextField)trc.getElement(this,  r, jasperDesign, pi, isPdf, null); //.getTextField(false, pi.fontSize, Align.Right, format, backColor, report.isAlternateRowColors());
				if (trc.treatAsMeasure())
				{					
					format = trc.getFormat();
					
					JRDesignExpression crosstabMeasureFieldExp = new JRDesignExpression();
					int index = trc.getPivotValuePctOfCol();
					if (index >= 0) {
						for (AdhocColumn ac : getColumns()) {
							if (ac.getReportIndex() == index) {
								StringBuilder measureExpressionText = new StringBuilder();
								measureExpressionText.append("Double.valueOf(");
								measureExpressionText.append("$V{" + Util.replaceWithPhysicalString(trc.getFieldName()) + "_" + Util.replaceWithPhysicalString(column.getFieldName()) + "_ALL}"); 
															//"$V{" + trc.getFieldName() + "}");
								measureExpressionText.append(".doubleValue()/$V{");
								measureExpressionText.append(trc.getFieldName());
								measureExpressionText.append('_');
								if (ac.getReportIndex() != column.getReportIndex() && ac.isPivotColumn() != column.isPivotColumn()) {
									// non-pivot column is always first
									if (column.isPivotColumn()) {
										measureExpressionText.append(ac.getFieldName());
										measureExpressionText.append('_');
										measureExpressionText.append(column.getFieldName());
									}
									else {
										measureExpressionText.append(column.getFieldName());
										measureExpressionText.append('_');
										measureExpressionText.append(ac.getFieldName());
									}
								}
								else {
									measureExpressionText.append(ac.getFieldName());
								}
								measureExpressionText.append("_ALL}.doubleValue())");
								crosstabMeasureFieldExp.setText(measureExpressionText.toString());
							}
						}
					}
					else {
						crosstabMeasureFieldExp.setText("$V{" + Util.replaceWithPhysicalString(trc.getFieldName()) + "_" + Util.replaceWithPhysicalString(column.getFieldName()) + "_ALL}");
						@SuppressWarnings("rawtypes")
						Class valueClass = trc.getFieldClass(r, null);
						if (GregorianCalendar.class.equals(valueClass))
							valueClass = Date.class;
					}
					totCellField.setExpression(crosstabMeasureFieldExp);
					//Use in constraintLabelWidthToColumnWidth() to locate total pivot column to column
					totCellField.getPropertiesMap().setProperty("FieldMarker", "$V{" + Util.replaceWithPhysicalString(trc.getFieldName()) + "}");
					totCellField.setX(0);
					totCellField.setY(0);
					//totCellField.setY(report.rowHeight * countTotMeasures);
					totCellField.setPattern(format);
					
					if (isPivotMeasureLabelsColumnWise()){
						totCellField.setStretchType(StretchTypeEnum.RELATIVE_TO_BAND_HEIGHT);
					}else
						totCellField.setStretchType(StretchTypeEnum.NO_STRETCH);
					
					totCellField.setPositionType(PositionTypeEnum.FLOAT);
					
					totContents.addElement(totCellField);
				}
				totCellField.getPropertiesMap().setProperty(AdhocReport.JASPERREPORTS_EXPORT_PREFIX + "showNaNblank", Boolean.toString(report.isShowNaNBlank()));
				totCellField.getPropertiesMap().setProperty(AdhocReport.JASPERREPORTS_EXPORT_PREFIX + "showINFblank", Boolean.toString(report.isShowINFBlank()));
				@SuppressWarnings("rawtypes")
				Class fieldType = trc.getFieldClass(r, null);
				if (report.isShowNullZero() && trc.getType() != ColumnType.Dimension && 
						(fieldType == Double.class || fieldType == Float.class || fieldType == Integer.class || fieldType == AdhocColumn.RATIO_CLASS))
				{
					totCellField.getPropertiesMap().setProperty(AdhocReport.JASPERREPORTS_EXPORT_PREFIX + "showNullZero", Boolean.toString(report.isShowNullZero()));
				}
			}
			totCell.setContents(totContents);
			
			//Add to our array list for later fix ups.
			totalCells.add(totCell);
		}
		/*
		 * Now create row/column groupings
		 */
		if (column.isPivotColumn())
		{
			AdhocTextEntity label = column.getLabelField();
			JRDesignCrosstabColumnGroup columnGroup = new JRDesignCrosstabColumnGroup();
			columnGroup.setName(fieldName);
			columnGroup.setHeight(report.rowHeight);
			
			//SNF
			/*
			if(getPivotTableOpts().isResizeTypeRowColColumns()){
				int maxWidth = findMaxColumnHeaderDataWidth(detailTextField, qs, isPdf);
				detailTextField.setWidth(maxWidth);
			}
			*/
			
			//Add the column's label on top.
			if (getPivotTableOpts().hasLabel()){
				//System.out.println("adding pivot column header - " + column.getName());
				
				JRDesignTextField colName = (JRDesignTextField)label.getElement(this, r, jasperDesign, pi, isPdf, null).clone();
				for (AdhocColumn ac: this.getColumns()) {
					if (ac.getLabelField() == label) {
						List<ColumnSelector> css = ac.getColumnSelectors();
						if (css != null && css.size() > 0) {
							JRDesignExpression de = new JRDesignExpression();
							de.setText("$P{" + ac.getLabelString() + ColumnSelector.LABEL + "}");
							colName.setExpression(de);
						}
					}
				}
				
				if (label instanceof AdhocColumn) {
					// change the expresssion of colName to 
					// ServerContext.getFirstRowsValue($JRDataSource, label.getFieldName());
					JRDesignExpression expression = new JRDesignExpression();
					expression.addTextChunk("ServerContext.getFirstRowsValue((JRDataSource)");
					/*
					expression.addParameterChunk(JRParameter.REPORT_PARAMETERS_MAP);
					expression.addTextChunk(".get(\"");
					expression.addTextChunk(JRParameter.REPORT_DATA_SOURCE);
					expression.addTextChunk("\"), \"");
					*/
					expression.addParameterChunk(JRParameter.REPORT_DATA_SOURCE);
					expression.addTextChunk(", \"");
					expression.addTextChunk(((AdhocColumn)label).getFieldName());
					expression.addTextChunk("\")");
					colName.setExpression(expression);
				}
			
				colName.setX(0);
				colName.setY(0);
				colName.setKey("COLUMN_LABEL_" + label.getLabel());
				colName.setWidth(detailTextField.getWidth());
				colName.setStretchWithOverflow(false);
				colName.setPositionType(PositionTypeEnum.FLOAT);
				colName.setStretchType(StretchTypeEnum.NO_STRETCH);
				colName.setFontSize(column.getFont().getSize());
				colName.setFontName(column.getFont().getFamily());
				colName.setVerticalAlignment(VerticalAlignEnum.TOP);

				if (ub != null && ub.isFreeTrial()) {
					boolean found = false;
					boolean morePivotColumns = false;
					for (AdhocColumn ac : getColumns()) {
						if (found == true && ac.isPivotColumn()) {
							morePivotColumns = true;
							break;
						}
						if (ac.reportIndex == column.reportIndex)
							found = true;
					}
					colName.setHorizontalAlignment(HorizontalAlignEnum.CENTER);
					colName.getLineBox().getBottomPen().setLineWidth(0.0f);
					detailTextField.setHorizontalAlignment(morePivotColumns ? HorizontalAlignEnum.CENTER : HorizontalAlignEnum.RIGHT);
				}
				
				detailTextField.setY(colName.getHeight());
				detailTextField.setStretchWithOverflow(true);
				if (colTotalField != null) {
					colTotalField.setY(colName.getHeight());
				}
				detailTextField.setStretchType(StretchTypeEnum.NO_STRETCH);
				detailTextField.setPositionType(PositionTypeEnum.FLOAT);
				detailTextField.setVerticalAlignment(VerticalAlignEnum.TOP);

				colHeadContents.addElement(colName);

				//pivotHeaderHeight denotes the height of the columns including its label on the right side
				pivotHeaderHeight += detailTextField.getHeight() + colName.getHeight();
				
				columnGroup.setHeight(detailTextField.getHeight() + colName.getHeight());
			}
			//End SNF
			
			columnGroup.setPosition(CrosstabColumnPositionEnum.STRETCH);
			cs.crosstab.addColumnGroup(columnGroup);
			columnGroup.setBucket(bucket);
			columnGroup.setHeader(colHeadContents);
			//detailTextField.setHorizontalAlignment(JRDesignTextField.HORIZONTAL_ALIGN_CENTER);
			// Total
			if (column.isPivotTotal())
			{
				//Mark this as a column total field
				colTotalField.setKey("COLUMN_TOTAL_" + label.getLabel());
				
				columnGroup.setTotalHeader(colTotalContents);
				columnGroup.setTotalPosition(CrosstabTotalPositionEnum.END);
				colTotalField.setHorizontalAlignment(HorizontalAlignEnum.CENTER);
				totCell.setColumnTotalGroup(fieldName);
				// Create line separator
				/* TODO: what does this do?
				JRBaseBox totBox = new JRBaseBox();
				// totCellField.setLeftBorder((byte) 1);
				colTotalContents.setBox(totBox);
				cs.crosstab.addCell(totCell);
				*/
			}
		} else
		{
			//SNF
			//System.out.println("adding pivot row header - " + column.getName());
			AdhocTextEntity label = column.getLabelField();
			
			PivotTableOptions pot = this.getPivotTableOpts();
			if (pot.hasLabel()){
				JRDesignTextField colName = (JRDesignTextField)label.getElement(this, r, jasperDesign, pi, isPdf, null).clone();
				for (AdhocColumn ac: this.getColumns()) {
					if (ac.getLabelField() == label) {
						List<ColumnSelector> css = ac.getColumnSelectors();
						if (css != null && css.size() > 0) {
							JRDesignExpression de = new JRDesignExpression();
							de.setText("$P{" + ac.getLabelString() + ColumnSelector.LABEL + "}");
							colName.setExpression(de);
						}
					}
				}
				if (label instanceof AdhocColumn) {
					// change the expresssion of colName to 
					// ServerContext.getFirstRowsValue($JRDataSource, label.getFieldName());
					JRDesignExpression expression = new JRDesignExpression();
					expression.addTextChunk("ServerContext.getFirstRowsValue((JRDataSource)");
					expression.addParameterChunk(JRParameter.REPORT_DATA_SOURCE);
					expression.addTextChunk(", \"");
					expression.addTextChunk(((AdhocColumn)label).getFieldName());
					expression.addTextChunk("\")");
					colName.setExpression(expression);
				}
				colName.setX(0);
				colName.setY(0);
				colName.setVerticalAlignment(VerticalAlignEnum.TOP);
				if (pot.isResizeTypeRowLabel() || pot.isResizeTypeAllLabel()){
					int calcWidth = this.getTextFontWidth(colName, null, isPdf);
					if (calcWidth == 0)
						calcWidth = colName.getWidth();
					if (calcWidth > 0) {
						colName.setWidth(calcWidth);
						detailTextField.setWidth(calcWidth);
						if (colTotalField != null) {
							colTotalField.setWidth(calcWidth);
						}
					}
				}else{
					colName.setWidth(detailTextField.getWidth());
				}
				
				if (ub != null && ub.isFreeTrial()) {
					colName.getLineBox().getBottomPen().setLineWidth(0.0f);
				}
				JRDesignCellContents headerCells = (JRDesignCellContents) cs.crosstab.getHeaderCell();
				if (headerCells == null){
					headerCells = new JRDesignCellContents();
					headerCells.addElement(colName);
					cs.crosstab.setHeaderCell(headerCells);
				}else{
					JRElement[] elems = headerCells.getElements();
					JRElement lastElem = elems[elems.length - 1];
					colName.setX(lastElem.getX() + lastElem.getWidth());
					headerCells.addElement(colName);
				}
			}
			//End SNF
			
			JRDesignCrosstabRowGroup rowGroup = new JRDesignCrosstabRowGroup();
			rowGroup.setName(fieldName);
			rowGroup.setWidth(detailTextField.getWidth());
			rowGroup.setPosition(CrosstabRowPositionEnum.STRETCH);
			cs.crosstab.addRowGroup(rowGroup);
			rowGroup.setBucket(bucket);
			rowGroup.setHeader(colHeadContents);
			
			// Total
			if (column.isPivotTotal())
			{
				rowGroup.setTotalHeader(colTotalContents);
				rowGroup.setTotalPosition(CrosstabTotalPositionEnum.END); // JRDesignCrosstab.POSITION_TYPE_FIX_RELATIVE_TO_TOP);
				totCell.setRowTotalGroup(fieldName);

				cs.crosstab.addCell(totCell);
			}
			
			//Stretch out the height
//			detailTextField.setStretchType(JRElement.STRETCH_TYPE_RELATIVE_TO_BAND_HEIGHT);
			
		}
		
		colHeadContents.addElement(detailTextField);
		return pivotHeaderHeight;
	}
	
	public List<AdhocSort> getSorts() {
		return sorts;
	}

	private int getTextFontWidth(JRDesignTextField text, Object alternateText, boolean isPdf){
		String altText = null;
		int errorOffset = 0;
		if(alternateText != null){
			if (alternateText instanceof Integer){
				errorOffset = 10;
			}
			altText = alternateText.toString();
		}
		String textExpr = (altText == null) ? text.getExpression().getText() : altText;
		if (text.getExpression().getChunks().length > 1)
			return 0;
		
		if (isPdf) {
			BaseFont font = null;
			String fontName = text.getPdfFontName();
			String pdfEncoding = text.getPdfEncoding();
			if (BaseFont.IDENTITY_H.equals(pdfEncoding))
				pdfEncoding = PdfObject.TEXT_PDFDOCENCODING;
			try {
				font = BaseFont.createFont(fontName, pdfEncoding, false);
			} catch (Exception e) {
				logger.error(e, e);
				return text.getWidth();
			}
			
			return Math.round(font.getWidthPoint(textExpr, (float)text.getFontSize())) + errorOffset;
		}
		
		// not pdf
		Font font = getFont(text);
		TextLayout layout = new TextLayout(textExpr, font, getFontRenderContext());
		Rectangle2D rect = layout.getBounds();
		return (int)Math.round(rect.getWidth()) + errorOffset;
	}
	
	private static FontRenderContext _fontRenderContext = null;
	private FontRenderContext getFontRenderContext() {
		if (_fontRenderContext == null) {
			_fontRenderContext = new FontRenderContext(null, true, true);
		}
		return _fontRenderContext;
	}
	private Font getFont(JRDesignTextField text) {
		int fontStyle = Font.PLAIN | (text.isBold() ? Font.BOLD : 0) | (text.isItalic() ? Font.ITALIC : 0);
		StringBuffer key = new StringBuffer();
		key.append(text.getFontName());
		key.append('|');
		key.append(fontStyle);
		key.append('|');
		key.append(text.getFontSize());
		Font font = fontMap.get(key.toString());
		if (font == null) {
			font = new Font(text.getFontName(), fontStyle, text.getFontSize());
			fontMap.put(key.toString(), font);
		}
		return font;
	}
	
/**
 * 	
 * @param report
 * @param column
 * @param jasperDesign
 * @param detailTextField
 * @param detailTextFieldStyle
 * @param xPosition
 * @param detailBand
 * @param isPivot
 * @param cs
 * @throws JRException
 */
	@SuppressWarnings({ "unchecked" })
	private void addConditionalFormats(AdhocReport report, AdhocColumn column,
									   JasperDesign jasperDesign, JRDesignTextField detailTextField, 
									   JRDesignStyle detailTextFieldStyle,
									   int xPosition, JRDesignBand detailBand, boolean isPivot, 
									   CrosstabSetup cs) throws JRException
	{
		StringBuilder printWhen = new StringBuilder();
		String prepend = null;
		int index = column.getIncludeIfReportIndex(); 
		if (index >= 0) {
			for (AdhocEntity ae : report.getEntities()) {
				if (ae.reportIndex == index && ae instanceof AdhocColumn) {
					AdhocColumn ac = (AdhocColumn)ae;
					prepend = "Boolean.TRUE.equals($F{" + ac.getFieldName() + "})";
					printWhen.append(prepend);
					break;
				}
				
			}
		}
		for (int j = 0; j < column.getConditionalFormats().size(); j++)
		{
			ConditionalFormat cf = column.getConditionalFormats().get(j);
			/*
			 * Add condition to base expression (when all conditionals fail)
			 */
			if (printWhen.length() > 0)
				printWhen.append("&&");
			/*
			 * Create expression for this conditional
			 */
			JRDesignTextField cffield = new JRDesignTextField();
			JasperUtil.copyTextFieldFormat(detailTextField, cffield, 
					(column.getTransparent() ? column.getTransparent().booleanValue(): column.getBackground().equals(Color.white)), false);
			if (detailTextFieldStyle != null)
				cffield.setStyle(detailTextFieldStyle);
			
			cffield.setEvaluationTime(detailTextField.getEvaluationTimeValue());
			cffield.setEvaluationGroup(detailTextField.getEvaluationGroup());
			
			JRDesignImage jrimage = null;
			if (cf.getForegroundColor() != null)
				cffield.setForecolor(cf.getForegroundColor());
			if (cf.getBackgroundColor() != null && ! cf.getBackgroundColor().equals(Color.white) )
			{
				cffield.setBackcolor(cf.getBackgroundColor());
				cffield.setMode(ModeEnum.OPAQUE);
			}
			if (cf.getFormat() != null && cf.getFormat().trim().length() > 0) {
				cffield.setPattern(cf.getFormat());
			}
			cffield.setUnderline(cf.isUnderline());
			if (cf.getImagePath() != null && cf.getImagePath().trim().length() > 0)
			{
				if (isPivot) {
					cffield.setX(cffield.getX() + cf.getImageWidth());
				}
				else {
					cffield.setX(xPosition + cf.getImageWidth());
				}

				cffield.setWidth(cffield.getWidth() - cf.getImageWidth());
				jrimage = new JRDesignImage(null);
				JRDesignExpression urlexp = new JRDesignExpression();
				urlexp.setText("\"" + Util.replaceStr(Util.replaceStr(cf.getImagePath(), "\\", "$$"), "$$", "\\\\") + "\"");
				jrimage.setExpression(urlexp);
				jrimage.setX(cffield.getX() - cf.getImageWidth());
				jrimage.setX(xPosition);

				jrimage.setY(detailTextField.getY());
				jrimage.setWidth(cf.getImageWidth());
				jrimage.setHeight(Math.min(detailTextField.getHeight(), cf.getImageHeight()));
				// Transfer border
				jrimage.getLineBox().getLeftPen().setLineColor(detailTextField.getLineBox().getLeftPen().getLineColor());
				jrimage.getLineBox().getLeftPen().setLineWidth(detailTextField.getLineBox().getLeftPen().getLineWidth());
				cffield.getLineBox().getLeftPen().setLineWidth(0f);
				if (detailTextFieldStyle != null)
					jrimage.setStyle(detailTextFieldStyle);
			} else if (! isPivot)
				cffield.setX(xPosition);
			
			cffield.setY(detailTextField.getY());
			cffield.setExpression(detailTextField.getExpression());
			if (cf.getFontName() != null)
				cffield.setFontName(cf.getFontName());
			if (cf.getFontSize() != null)
				cffield.setFontSize(cf.getFontSize());
			Integer style = cf.getFontStyle();
			if (style != null)
			{
				cffield.setBold((style & Font.BOLD) == 0 ? false : true);
				cffield.setItalic((style & Font.ITALIC) == 0 ? false : true);
			}
			JRDesignExpression cfpwexpression = new JRDesignExpression();
			cffield.setPrintWhenExpression(cfpwexpression);
			if (isPivot)
			{
				JRElementGroup jreg = detailTextField.getElementGroup();
				if (jreg != null) {
					jreg.getChildren().add(cffield);
				}
				else  {
					cs.contents.addElement(cffield);
					
					if (jrimage != null)
						cs.contents.addElement(jrimage);
				}
				JRDesignCrosstabMeasure crosstabMeasure = new JRDesignCrosstabMeasure();
				crosstabMeasure.setCalculation(CalculationEnum.SUM);
				JRDesignExpression crosstabMeasureExp = new JRDesignExpression();
				crosstabMeasureExp.setText("$F{" + column.getConditionalFormatExpressionName(cf) + "}");
				crosstabMeasure.setValueExpression(crosstabMeasureExp);
				crosstabMeasure.setValueClassName("java.lang.Boolean");
				crosstabMeasure.setName(column.getConditionalFormatExpressionName(cf));
				cs.crosstab.addMeasure(crosstabMeasure);
				printWhen.append("(Boolean.FALSE.equals($V{" + column.getConditionalFormatExpressionName(cf) + "})");
				printWhen.append("||$V{" + column.getConditionalFormatExpressionName(cf) + "} == null)");
				StringBuilder ex = new StringBuilder();
				if (prepend != null) {
					ex.append("Boolean.valueOf(");
					ex.append(prepend);
					ex.append("&&");
				}
				ex.append("$V{" + column.getConditionalFormatExpressionName(cf) + "}");
				if (prepend != null) {
					ex.append(")");
				}
				cfpwexpression.setText(ex.toString());
			} else
			{
				detailBand.addElement(cffield);
				if (jrimage != null)
					detailBand.addElement(jrimage);
				printWhen.append("(Boolean.FALSE.equals($F{" + column.getConditionalFormatExpressionName(cf) + "})");
				printWhen.append("||$F{" + column.getConditionalFormatExpressionName(cf) + "} == null)");
				StringBuilder ex = new StringBuilder();
				if (prepend != null) {
					ex.append("Boolean.valueOf(");
					ex.append(prepend);
					ex.append("&&");
				}
				ex.append("$F{" + column.getConditionalFormatExpressionName(cf) + "}");
				if (prepend != null) {
					ex.append(".booleanValue())");
				}
				cfpwexpression.setText(ex.toString());
			}
			if (jrimage != null)
			{
				JRDesignExpression cfipwexpression = new JRDesignExpression();
				cfipwexpression.setText(cfpwexpression.getText());
				jrimage.setPrintWhenExpression(cfipwexpression);
			}
			JRDesignField fld = new JRDesignField();
			fld.setName(column.getConditionalFormatExpressionName(cf));
			fld.setValueClass(Boolean.class);
			jasperDesign.addField(fld);
		}
		if (printWhen.length() > 0)
		{
			JRDesignExpression pwexpression = new JRDesignExpression();
			pwexpression.setText("Boolean.valueOf(" + printWhen.toString() + ")");
			detailTextField.setPrintWhenExpression(pwexpression);
		}
	}

	/**
	 * 
	 * @param name
	 * @return AdhocColumn instance
	 */
	public AdhocColumn getColumnByName(String name) {
		for (AdhocColumn column : getColumns()) {
			if (column.getColumn().equals(name)) {
				return column;
			}
		}
		
		for (AdhocColumn column : getColumns()) {
			if (column.getName().equals(name)) {
				return column;
			}
		}
		return null;
	}

	private String getTitle() {
		return title;
	}

	private void setTitle(String title) {
		this.title = title;
	}

	private boolean isHideTitle() {
		return hideTitle;
	}

	private void setHideTitle(boolean hideTitle) {
		this.hideTitle = hideTitle;
	}
	
	/**
	 * Is our pivot table's measure labels on the column or not (then it is in the row level)
	 * @return 
	 */
	private boolean isPivotMeasureLabelsColumnWise(){
		return getPivotTableOpts().isMeasureLabelsColumnWise();
	}
	
	/**
	 * Add the report title (and description)
	 * 
	 * @param title
	 * @param titleBand
	 * @param pi
	 * @param print
	 * @param hasChart
	 * @return
	 */
	private int addTitle(String title, JRDesignBand titleBand, PageInfo pi, boolean print)
	{
		int titleOffset = 0;
		String titleStr = title;
		boolean hasTitle = titleStr != null && titleStr.length() > 0 && !hideTitle;
		if (hasTitle)
		{
			JRDesignStaticText reportTitle = new JRDesignStaticText();
			reportTitle.setText(titleStr);
			reportTitle.setX(0);
			reportTitle.setY(0);
			reportTitle.setHeight(PageInfo.DEFAULT_TITLE_BAND_HEIGHT - 1);
			reportTitle.setWidth(pi.width - pi.leftMar - pi.rightMar);
			reportTitle.setFontName(PageInfo.DEFAULT_FONT_NAME);
			reportTitle.setFontSize(titleFontSize);
			titleBand.addElement(reportTitle);
			titleOffset = titleHeight;
			if (description != null && description.length() > 0)
			{
				JRDesignTextField desc = new JRDesignTextField();
				JRDesignExpression expression = new JRDesignExpression();
				expression.addTextChunk("\"" + description + "\"");
				desc.setExpression(expression);

				desc.setX(0);
				desc.setY(PageInfo.DEFAULT_TITLE_BAND_HEIGHT);
				desc.setHeight(PageInfo.DEFAULT_ROW_HEIGHT);
				desc.setWidth(pi.width);
				desc.setFontName(PageInfo.DEFAULT_FONT_NAME);
				desc.setFontSize(pi.fontSize);
				titleBand.addElement(desc);
				titleOffset += desc.getHeight();
				
				if (this.descriptionHyperlink != null && Util.hasNonWhiteSpaceCharacters(descriptionHyperlink)) {
					desc.setHyperlinkType(HyperlinkTypeEnum.REFERENCE);
					JRDesignExpression hyperlinkReferenceExpression = new JRDesignExpression();
					hyperlinkReferenceExpression.addTextChunk("\"" + descriptionHyperlink + "\"");
					desc.setHyperlinkReferenceExpression(hyperlinkReferenceExpression);
				}
			}
			titleBand.setHeight(titleOffset + pi.chartMar);
		}
		if (!print && hasTitle)
		{
			JRDesignStaticText spacerText = new JRDesignStaticText();
			spacerText.setY(titleOffset);
			spacerText.setX(0);
			spacerText.setHeight(pi.chartMar);
			spacerText.setMode(ModeEnum.OPAQUE);
			spacerText.setText(" ");
			titleBand.addElement(spacerText);
			titleOffset += pi.chartMar;
		}
		return (titleOffset);
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescriptionHyperlink() {
		return descriptionHyperlink;
	}

	public void setDescriptionHyperlink(String descriptionHyperlink) {
		this.descriptionHyperlink = descriptionHyperlink;
	}
	
	public void compileAndSaveReport(File file) throws Exception {
	}
	
	public String getMimeTypeCSV() {
		return mimeTypeCSV;
	}

	public void setMimeTypeCSV(String mimeTypeCSV) {
		this.mimeTypeCSV = mimeTypeCSV;
	}
	
	public String getFilenameCSV() {
		return filenameCSV;
	}

	public void setFilenameCSV(String filenameCSV) {
		this.filenameCSV = filenameCSV;
	}
	
	public AdhocEntity getEntityById(int id) {
		for (AdhocEntity entity : this.getEntities()) {
			if (entity.getReportIndex() == id) 
				return entity;
		}
		return null;
	}

	public void setEditDashlet(boolean editDashlet){
		this.editDashlet = editDashlet;
	}
	
	public boolean isHidedata() {
		return hidedata && (editDashlet || (Session.getRenderer() == Session.Renderer.Adhoc));
	}

	public boolean isHidedataFlagSet() {
		return hidedata;
	}
	public void setHidedata(boolean hidedata) {
		this.hidedata = hidedata;
	}
	
	public void validateQuery() throws BaseException, CloneNotSupportedException, IOException, SQLException, ArrayIndexOutOfBoundsException
	{
		com.successmetricsinc.UserBean ub = SMIWebUserBean.getUserBean();
		if (ub == null)
			ub = UserBean.getUserBean();
		Session repSession = ub.getRepSession();
		Query q = getQuery(ub.getRepository());
		if (q != null)
		{
			// must extract the expressions before validating a query
			List<DisplayExpression> expressionList = getExpressionList();
			if (expressionList != null)
			{
				List<String> addedNames = new ArrayList<String>();
				for (DisplayExpression de : expressionList)
				{
					de.extractAllExpressions(q, addedNames);
				}
			}
			q.setValidateOnly(true);
			q.generateQuery(repSession, true);
		}
	}
	
	
	@Override
	public void addContent(OMElement parent, OMFactory factory, OMNamespace ns) {
		OMElement el = getSaveXMLDocument(ns, factory);
		parent.addChild(el);
	}

	public boolean containsDimensionColumn(String dimensionName, String columnName) {
		if (dimensionName == null || !Util.hasNonWhiteSpaceCharacters(dimensionName) || 
				columnName == null || !Util.hasNonWhiteSpaceCharacters(columnName))
			return false;
		
		for (AdhocColumn column: this.columns) {
			if (dimensionName.equals(column.getDimension()) && columnName.equals(column.getColumn()))
				return true;
		}
		return false;
	}
	
	private boolean includeChartSelector() {
		if (this.isIncludeTable() == false) {
			AdhocChart chart = getDefaultChart();
			return (chart != null && !chart.getCharts().isEmpty());
		}
		return false;
	}
	
	private String getIncludeChartSelectorType(){
		AdhocChart chart = getDefaultChart();
		if(chart != null){
			List<AnyChartNode.ChartType> charts = chart.getChartTypes();
			if(chart != null){
				int numCharts = charts.size();
				if(numCharts == 1){
					return AnyChartNode.chartTypeToString(charts.get(0));
				}else if (numCharts > 1){
					//We return 'Composite', the ui doesn't really care as long as it is
					//not a basic chart type and switches to chart or table selector.
					return "Composite"; 
				}
			}
		}
		return "";
	}
	
	public List<AdhocColumnSelector> getColumnSelectorList() {
		List<AdhocColumnSelector> ret = new ArrayList<AdhocColumnSelector>();
		Map<String, AdhocColumnSelector> acsMap = new HashMap<String, AdhocColumnSelector>();
		
		// add all columns
		for (AdhocColumn ac : getColumns()) {
			List<ColumnSelector> css = ac.getColumnSelectors();
			if (css != null && css.size() > 0) {
				String lbl = ac.getLabelString();
				AdhocColumnSelector acs = new AdhocColumnSelector(lbl, ac.getUniqueExpressionString());
				acs.addColumnSelector(new ColumnSelector(ac));
				acsMap.put(lbl, acs);
				ret.add(acs);
				for (ColumnSelector cs : css) {
					acs.addColumnSelector(cs);
				}
			}
		}
		
		// now merge in subreports
		for (AdhocEntity ae : getEntities()) {
			if (ae instanceof AdhocSubreport) {
				AdhocSubreport sub = (AdhocSubreport)ae;
				List<AdhocColumnSelector> acss = sub.getColumnSelectors();
				if (acss != null && acss.size() > 0) {
					for (AdhocColumnSelector acs : acss) {
						String lbl = acs.getName();
						AdhocColumnSelector got = acsMap.get(lbl);
						if (got == null) {
							acsMap.put(lbl, acs);
							ret.add(acs);
						}
						else {
							got.merge(acs);
						}
					}
				}
			}
		}
		return ret;
	}
	
	public OMElement getColumnSelectors() {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;
		OMElement ret = fac.createOMElement(DASHLET_PROMPTS, ns);
		boolean includeChart = includeChartSelector();
		XmlUtils.addContent(fac, ret, INCLUDE_CHART_SELECTOR, includeChart, ns);
		if (includeChart) {
			XmlUtils.addContent(fac, ret, CHART_TYPE, getIncludeChartSelectorType(), ns);
		}
		for (AdhocColumnSelector acs : getColumnSelectorList()) {
			List<ColumnSelector> css = acs.getItems();
			if (css != null && css.size() > 0) {
				OMElement cs = fac.createOMElement(COLUMN_SELECTORS, ns);
				XmlUtils.addContent(fac, cs, COLUMN, acs.getName(), ns);
				XmlUtils.addContent(fac, cs, SELECTED, acs.getSelected(), ns);
				for (ColumnSelector colSel : css) {
					colSel.addContent(cs, fac, ns);
				}
				ret.addChild(cs);
			}
		}
		return ret;
	}

	/**
	 * @param report
	 * @param queryString
	 * @param ps 
	 * @return
	 */
	public String convertColumnSelectorsInQueryString(String queryString, Repository r, Map<String, Object> ps) {
		String param = null;
		for (AdhocColumn ac : getColumns()) {
			List<ColumnSelector> css = ac.getColumnSelectors();
			if (css != null && css.size() > 0) {
				String rep = AdhocColumn.getDefaultColumnExpression(ac, r.isUseNewQueryLanguage());
				if (ac.getType() == ColumnType.Dimension) {
					String paramName = ac.getLabelString() + AdhocColumn.DIMENSION;
					if (ps!= null && ps.get(paramName) == null)
						continue;
					
					if (r.isUseNewQueryLanguage())
						param =   "[\" + $P{" + ac.getLabelString() + AdhocColumn.DIMENSION + "} + \".\" + $P{" + ac.getLabelString() + AdhocColumn.COLUMN + "} + \"]";
					else
						param = "DC{\" + $P{" + ac.getLabelString() + AdhocColumn.DIMENSION + "} + \".\" + $P{" + ac.getLabelString() + AdhocColumn.COLUMN + "} + \"}";
					queryString = queryString.replace(rep, param);
					
					// check for sorts
					if (r.isUseNewQueryLanguage()) {
						rep = "ORDER BY " + rep;
						param = "ORDER BY " + param;
						queryString = queryString.replace(rep, param);
					}
					else {
						int index = rep.lastIndexOf('}');
						if (index > 0) {
							rep = "O" + rep.substring(0, index) + ",";
							param = "ODC{\" + $P{" + ac.getLabelString() + AdhocColumn.DIMENSION + "} + \".\" + $P{" + ac.getLabelString() + AdhocColumn.COLUMN + "} + \" ,";
							queryString = queryString.replace(rep, param);
						}
					}
					
					
				}
				else if (ac.getType() == ColumnType.Measure) {
					String labelString = ac.getLabelString() + AdhocColumn.COLUMN;
					if (ps != null && ps.get(labelString) == null)
						continue;
					
					if (r.isUseNewQueryLanguage())
						param =  "[\" + $P{" + ac.getLabelString() + AdhocColumn.COLUMN + "} + \"]";
					else
						param = "M{\" + $P{" + ac.getLabelString() + AdhocColumn.COLUMN + "} + \"}";
					queryString = queryString.replace(rep, param);
					
					if (r.isUseNewQueryLanguage()) {
						rep = "ORDER BY " + rep;
						param = "ORDER BY " + param;
						queryString = queryString.replace(rep, param);
					}
					else {
						int index = rep.lastIndexOf('}');
						if (index > 0) {
							rep = "O" + rep.substring(0, index) + ",";
							param = "OM{\" + $P{" + ac.getLabelString() + AdhocColumn.COLUMN + "} + \",";
							queryString = queryString.replace(rep, param);
						}
					}
				}
			}
		}
		return queryString;
	}

	public JRDataSource getDashboardJRDataSource(String dashletPrompts, Repository r, List<Filter> filterParams, UserBean ub, Session session, boolean applyAllPrompts, boolean isPrint) throws Exception {
		JasperDataSourceProvider jdsp = new JasperDataSourceProvider(r);
		jdsp.setSession(session);
		jdsp.setParams(filterParams);
		jdsp.setApplyAllPrompts(applyAllPrompts);
		
		if (isHidedata())
			return new SingleRowDataSource(jdsp);
		
		if (dashletPrompts == null) {
			dashletPrompts = getColumnSelectors().toString();
		}
		Map<String, ColumnSelector> selectors = null;
		if (dashletPrompts != null && Util.hasNonWhiteSpaceCharacters(dashletPrompts)) {
			 selectors = parseColumnSelectors(dashletPrompts);
		}
		
		Map<String, Object> ps = AdhocReportHelper.getReportFilterParams(filterParams, dashletPrompts);
		String queryString = "SingleRow{}";
		Query q = getQuery(r, dashletPrompts);
		
		if (q != null) {
			if (q.dimensionMemberSets != null && q.dimensionMemberSets.size() > 0)
			{
				Map<String, String> dimensionMemberSetDataTypeMap = new HashMap<String, String>();
				Map<String, String> dimensionMemberSetFormatMap = new HashMap<String, String>();
				for (DimensionMemberSetNav dms : q.dimensionMemberSets)
				{
					if (dms.displayName != null && dms.dataType != null && !dimensionMemberSetDataTypeMap.containsKey(dms.displayName))
					{
						dimensionMemberSetDataTypeMap.put(dms.displayName, dms.dataType);
					}
					if (dms.displayName != null && dms.format != null && !dimensionMemberSetFormatMap.containsKey(dms.displayName))
					{
						dimensionMemberSetFormatMap.put(dms.displayName, dms.format);
					}
				}
				jdsp.setDimensionMemberSetDataTypeMap(dimensionMemberSetDataTypeMap);
				jdsp.setDimensionMemberSetFormatMap(dimensionMemberSetFormatMap);
			}
			q.setFullOuterJoin(true); // make sure we get all rows (see defect 6633)
			List<DisplayOrder> displayOrders = getDisplayOrders(selectors);
			List<DisplayFilter> displayFilters = getQueryDisplayFilters(); //getDisplayFilters();
			List<DisplayExpression> expressionList = getExpressionList();
			
			StringBuilder sb = new StringBuilder();
			
			if(expressionList != null){
				String exprQueryStr = q.getLogicalQueryString(true, expressionList, true);
				exprQueryStr = Util.replaceAllParameters(exprQueryStr, ps);
				sb.append(exprQueryStr);
			}
			
			if (displayFilters != null)
			{
				/**
				 * Fix display filters defined in auto-generated dashboards in 4.2.x or earlier that were 
				 * marked as prompted and had anactual operand value as well. If the corresponding prompt 
				 * name is not part of parameter map, they will end up getting discarded. To prevent 
				 * from getting these filters discarded and to preserve the behavior from 4.2.x, mark 
				 * these filters as non-prompted filters. 
				**/
				for(DisplayFilter df : displayFilters)
				{
					if(df.isPrompted() && df.getOperand() != null && !ps.containsKey(df.getPromptName()))
					{
						df.setPromptName(null);
					}
				}
				sb.append(DisplayFilter.getQueryFilters(displayFilters, true, r.isUseNewQueryLanguage()));
			}
			
			if (displayOrders != null)
			{
				sb.append(DisplayOrder.getQueryOrders(displayOrders, r.isUseNewQueryLanguage()));
			}
			
			queryString = sb.toString();
		}

		queryString = convertColumnSelectorsInQueryString(queryString, r, ps);
		
		// convert $P{...}s
		queryString = Util.replaceParameters(queryString, ps);
		
		if (!r.isUseNewQueryLanguage())
		{
			// replace "+$P{parameterName}+" with NO_FILTER_TEXT - if it is still here, means that there was no value set for the parameter
			String regExp = "\\\"\\p{Space}*\\+\\$P\\{[^\\}]*\\}\\+\\p{Space}*\\\"";
			queryString = queryString.replaceAll(regExp, QueryString.NO_FILTER_TEXT);
			queryString = queryString.replace("\\\"", "\"");
		}
		
		return jdsp.create(queryString, session);
	}
	
	public QueryResultSet getDashboardQueryResultSet(String dashletPrompts, Repository r, List<Filter> filterParams, SMIWebUserBean ub, Session session, boolean applyAllPrompts) throws Exception {
		JRDataSource jrds = getDashboardJRDataSource(dashletPrompts, r, filterParams, ub, session, applyAllPrompts, true);
		QueryResultSet qrs = null;
		
		if (jrds instanceof QueryResultSet) {
			qrs = (QueryResultSet)jrds;
		}
		
		return qrs;
	}

	/**
	 * @return the useOldSizing
	 */
	public boolean isUseOldSizing() {
		return useOldSizing;
	}

	public Color getBackgroundColor() {
		return backgroundColor;
	}
	
	public boolean isBackgroundVisible() {
		return ! backgroundInvisible;
	}

	public boolean isDoNotStyleHyperlinks() {
		return doNotStyleHyperlinks;
	}

	public void setDoNotStyleHyperlinks(boolean doNotStyleHyperlinks) {
		this.doNotStyleHyperlinks = doNotStyleHyperlinks;
	}

	public void applyStyle(AdhocReport styleReport) {
		includeTable = styleReport.includeTable;
		pageHeight = styleReport.pageHeight;
		pageWidth = styleReport.pageWidth;
		pageOrientation = styleReport.pageOrientation;
		pageSize = styleReport.pageSize;
		leftMargin = styleReport.leftMargin;
		rightMargin = styleReport.rightMargin;
		topMargin = styleReport.topMargin;
		bottomMargin = styleReport.bottomMargin;
		alternateRowColors = styleReport.alternateRowColors;
		firstAlternateRGB = styleReport.firstAlternateRGB;
		secondAlternateAltRGB = styleReport.secondAlternateAltRGB;
		backgroundColor = styleReport.backgroundColor;
		backgroundInvisible = styleReport.backgroundInvisible;
		hideTitle = styleReport.hideTitle;
		titleFontSize = styleReport.titleFontSize;
	    showNullZero = styleReport.showNullZero;
	    showINFBlank = styleReport.showINFBlank;
	    showNaNBlank = styleReport.showNaNBlank;
		useFlexGrid = styleReport.useFlexGrid;
		doNotStyleHyperlinks = styleReport.doNotStyleHyperlinks;
		
		if (pi != null)
			pi.applyStyle(styleReport.pi);
		if (pivotOpts != null)
			pivotOpts.applyStyle(styleReport.pivotOpts);
		
		if (includeTable) {
			// only apply style if we're not hiding the table
			AdhocColumn dimensionStyle = null;
			AdhocColumn measureStyle = null;
			AdhocColumn pivotStyle = null;
			for (AdhocColumn ac: styleReport.columns) {
				if (ac.getType() == ColumnType.Dimension || 
						(ac.getType() == ColumnType.Expression && ! ac.isMeasureExpression())) {
					if (ac.isPivotColumn() && pivotStyle == null)
						pivotStyle = ac;
					else if (dimensionStyle == null)
						dimensionStyle = ac;
				}
				else if (measureStyle == null)
					measureStyle = ac;
			}
			
			if (dimensionStyle == null) {
				if (pivotStyle != null)
					dimensionStyle = pivotStyle;
				else 
					dimensionStyle = measureStyle;
			}
			if (measureStyle == null)
				measureStyle = dimensionStyle;
			if (pivotStyle == null)
				pivotStyle = dimensionStyle;
			
			if (dimensionStyle != null) {
				for (AdhocColumn ac: columns) {
					AdhocColumn styleColumn = dimensionStyle;
					if (ac.getType() == ColumnType.Dimension || 
							(ac.getType() == ColumnType.Expression && ! ac.isMeasureExpression())) {
						if (ac.isPivotColumn())
							styleColumn = pivotStyle;
					}
					else 
						styleColumn = measureStyle;
					
					ac.applyStyle(styleColumn);
					AdhocTextEntity label = ac.getLabelField();
					AdhocTextEntity styleLabel = styleColumn.getLabelField();
					if (label != null && styleLabel != null)
						label.applyStyle(styleLabel);
				}
			}
		}
		
		AdhocChart styleChart = styleReport.getDefaultChart();
		if (styleChart != null) {
			for (AdhocEntity ae: entities) {
				if (ae instanceof AdhocChart)
					((AdhocChart)ae).applyStyle((AdhocChart)styleChart);
			}
		}
	}

	public boolean isUseFlexGrid() {
		return useFlexGrid;
	}

	public TopFilter getTopFilter() {
		return topFilter;
	}

	public void setTopFilter(TopFilter topFilter) {
		this.topFilter = topFilter;
	}

	public int getNextId() {
		return nextId++;
	}
	
	public boolean isPivot() {
		int numMeasures = 0;
		int numNonPivotColumns = 0;
		int numPivotColumns = 0;
		for (AdhocColumn rc : columns) {
			if (rc.isPivotColumn())
				numPivotColumns++;
			if (rc.getType() == AdhocColumn.ColumnType.Measure || 
					rc.getType() == ColumnType.Ratio ||
					rc.getType() == AdhocColumn.ColumnType.Expression) {
				numMeasures++;
			}
			else if (rc.getType() == AdhocColumn.ColumnType.Dimension &&
					rc.isPivotColumn() == false) {
				numNonPivotColumns++;
			}
		}
		return numPivotColumns > 0 && numMeasures > 0 && numNonPivotColumns > 0;
	}

	public void setDefaultChart(AdhocChart chart) {
		chart.reportIndex = getNextId();
		defaultChartIndex = chart.reportIndex;
		addEntity(chart);
	}

	public boolean isInVisualization() {
		return inVisualization;
	}

	public boolean isAsynchReportGeneration() {
		return asynchReportGeneration;
	}

	public void setAsynchReportGeneration(boolean asynchReportGeneration) {
		this.asynchReportGeneration = asynchReportGeneration;
	}
	
	public boolean isOlapUseIndentation() {
		return olapUseIndentation;
	}
	
}
