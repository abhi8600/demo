/**
 * 
 */
package com.successmetricsinc.adhoc;

import java.util.Iterator;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class DrillColumn {
	private static final String COLUMN = "Column";
	private static final String DIMENSION = "Dimension";
	private static final String LABEL = "Label";
	private static final String HEADER_LABEL = "HeaderLabel";
	private String dimension;
	private String column;
	private String label;
	private String headerLabel;
	
	public DrillColumn() {
		
	}
	
	public DrillColumn(String d, String c) {
		dimension = d;
		column = c;
	}
	
	/**
	 * @param dimension the dimension to set
	 */
	public void setDimension(String dimension) {
		this.dimension = dimension;
	}
	/**
	 * @return the dimension
	 */
	public String getDimension() {
		return dimension;
	}
	public String getColumn() {
		return column;
	}
	
	public String getLabel() {
		return label;
	}
	
	public String getHeaderLabel() {
		return headerLabel;
	}
	
	public void setColumn(String column) {
		this.column = column;
	}
	
	public void setLabel(String label) {
		this.label = label;
	}
	
	public void setHeaderLabel(String headerLabel) {
		this.headerLabel = headerLabel;
	}
	
	public String getDisplayName() {
		if(this.headerLabel != null && headerLabel.trim().length() > 0){
			return headerLabel;
		}
		return column;
	}
	
	public void parseElement(OMElement parent) throws Exception {
		for (@SuppressWarnings("unchecked")
		Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String name = el.getLocalName();
			if (DIMENSION.equals(name)) {
				dimension = XmlUtils.getStringContent(el);
			}
			else if (COLUMN.equals(name)) {
				column = XmlUtils.getStringContent(el);
			}
			else if(LABEL.equals(name)) {
				label = XmlUtils.getStringContent(el);
			}
			else if(HEADER_LABEL.equals(name)) {
				headerLabel = XmlUtils.getStringContent(el);
			}
		}
	}
	
	public void parseElement(XMLStreamReader parser) throws Exception {
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
			iter.next();
			String name = parser.getLocalName();
			if (DIMENSION.equals(name)) {
				dimension = XmlUtils.getStringContent(parser);
			}
			else if (COLUMN.equals(name)) {
				column = XmlUtils.getStringContent(parser);
			}
			else if(LABEL.equals(name)) {
				label = XmlUtils.getStringContent(parser);
			}
			else if(HEADER_LABEL.equals(name)) {
				headerLabel = XmlUtils.getStringContent(parser);
			}
		}
	}
	
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, DIMENSION, dimension, ns);
		XmlUtils.addContent(fac, parent, COLUMN, column, ns);
		if(label == null || label.trim().length() == 0){
			XmlUtils.addContent(fac, parent, LABEL, column, ns); // make the column name as the label
		}
		else
		{
			XmlUtils.addContent(fac, parent, LABEL, label, ns);
		}
		
		
		if(headerLabel == null || headerLabel.trim().length() == 0){
			XmlUtils.addContent(fac, parent, HEADER_LABEL, column, ns); // make the column name as the header label
		}
		else
		{
			XmlUtils.addContent(fac, parent, HEADER_LABEL, headerLabel, ns);
		}

	}

}
