/**
 * $Id: BindInfo.java,v 1.7 2011-09-24 01:16:00 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.adhoc;


/**
 * Class to capture basic data binding information. Given a result set and the current report, columns are bound to
 * result set columns. Also is responsible for doing the binding between report columns and a data set.
 * 
 * @author Brad Peters
 * 
 */
class BindInfo
{
	int numColumns = 0;
	int numMeasures = 0;
	int numOnColumns = 0;
	int numOnRows = 0;
	int numColumnTotals = 0;
	int maxMeasureWidth = -1;

	public BindInfo(AdhocReport report)
	{
		/*
		 * Get a list of visible columns and bind them to the result set (if one exists). Also, calculate the number of
		 * measure columns, and the number of dimensions on rows or columns (for pivots)
		 */
		for (AdhocEntity entity : report.getEntities())
		{
			if (entity instanceof AdhocColumn) {
				if (entity.isShowInReport() == false)
					continue;
				
				AdhocColumn rc = (AdhocColumn) entity;
				numColumns++;
				if (rc.treatAsDimension())
				{
					if (rc.isPivotColumn())
					{
						numOnColumns++;
						if (rc.isPivotTotal())
							numColumnTotals++;
					} else
						numOnRows++;
				} else
				{
					numMeasures++;
					if (rc.getWidth() > maxMeasureWidth)
						maxMeasureWidth = rc.getWidth();
				}
			}
		}
	}
}