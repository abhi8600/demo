/**
 * $Id: ReportFilter.java,v 1.21 2012-04-26 16:05:27 ricks Exp $
 *
 * Copyright (C) 2009 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.adhoc;

import java.io.File;
import java.io.Serializable;
import java.util.Iterator;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.catalog.CatalogManager;
import com.successmetricsinc.query.Query;
import com.successmetricsinc.query.QueryFilter;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

/**
 * ReportFilter - use the query in a report as an IN/NOT IN filter for a dimension column
 * - limits: only works for dimension columns, query must return a single column result set
 */
public class ReportFilter implements Cloneable, Serializable {

	private static final long serialVersionUID = 1L;

	public static final String FILTER = "Filter";

	private static Logger logger = Logger.getLogger(ReportFilter.class);
	
	private static final String TYPE = "Type";
	private static final String OPERAND = "Operand";
	private static final String OPERATOR = "Operator";
	private static final String COLUMN = "Column";
	private static final String DIMENSION = "Dimension";
	private static final String DATATYPE = "DataType";
	
	private String Dimension;
	private String Column;
	private String Operator; // IN, NOT IN
	private String Operand; // report name
	private String DataType;
	
	public ReportFilter()
	{
	}

	public ReportFilter(String dim, String col, String op, String name, String type)
	{
		this.Dimension = dim;
		this.Column = col;
		this.Operator = op;
		this.Operand = name;
		this.DataType = type;
	}

	public String getColumn() {
		return Column;
	}

	public String getOperator() {
		return Operator;
	}

	public String getOperand() {
		return Operand;
	}

	public String getDimension() {
		return Dimension;
	}	
	
	public String getDataType(){
		return DataType;
	}
	
	/**
	 * build a query filter from the report filter (FDC{Dimension.Column IN/NOT IN Q{reportQuery}})
	 * @return query filter
	 */
	public QueryFilter getQueryFilter()
	{
		try
		{
			if (QueryFilter.IN.equals(this.getOperator()) || QueryFilter.NOTIN.equalsIgnoreCase(this.getOperator()))
			{
				com.successmetricsinc.UserBean ub = SMIWebUserBean.getUserBean();
				String catalogRootPath = null;
				if (ub == null)
					ub = com.successmetricsinc.UserBean.getParentThreadUserBean();
				if (ub == null) {
					logger.error("Could not get user for this report filter: " + getOperand());
				}
				Session session = ub.getRepSession();
				catalogRootPath = session.getCatalogDir();
				String fileName = this.Operand;
				fileName = CatalogManager.getNormalizedPath(fileName, catalogRootPath);
				File aFile = CatalogManager.getAdhocReportFile(fileName, catalogRootPath);
				if (aFile == null || aFile.exists() == false)
					return null;
				
				AdhocReport report = AdhocReport.read(aFile);
				Repository r = ub.getRepository();
				Query q = report.getQuery(r);
				String queryText = q.getLogicalQueryString(true, report.getExpressionList(), r.isUseNewQueryLanguage());
				if (queryText == null)
				{
					logger.warn("Could not get the query text for the report filter: " + getOperand());
					return null;
				}
				if (!queryText.toLowerCase().startsWith("select"))
				{
					queryText = encloseINQConstruct(queryText);
				}
				return new QueryFilter(this.getDimension(), this.getColumn(), this.getOperator(), queryText);
			}
		}
		catch (Exception e)
		{
			logger.error(e.getMessage(), e);
		}
		return null;
	}
	
	/**
	 * process the XML representation
	 * @param parent
	 */
	@SuppressWarnings("unchecked")
	public void parseElement(OMElement parent)
	{
		for (Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();
			if (DIMENSION.equals(elName)) {
				this.Dimension = XmlUtils.getStringContent(el);
			}
			else if (COLUMN.equals(elName)) {
				this.Column = XmlUtils.getStringContent(el);
			}
			else if (OPERAND.equals(elName)) {
				this.Operand = XmlUtils.getStringContent(el);
			}
			else if (OPERATOR.equals(elName)) {
				this.Operator = XmlUtils.getStringContent(el);
			}else if (DATATYPE.equals(elName)){
				this.DataType = XmlUtils.getStringContent(el);
			}
		}
	}
	public void parseElement(XMLStreamReader parser) throws Exception {
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
			String elName = parser.getLocalName();
			if (DIMENSION.equals(elName)) {
				this.Dimension = XmlUtils.getStringContent(parser);
			}
			else if (COLUMN.equals(elName)) {
				this.Column = XmlUtils.getStringContent(parser);
			}
			else if (OPERAND.equals(elName)) {
				this.Operand = XmlUtils.getStringContent(parser);
			}
			else if (OPERATOR.equals(elName)) {
				this.Operator = XmlUtils.getStringContent(parser);
			}else if (DATATYPE.equals(elName)){
				this.DataType = XmlUtils.getStringContent(parser);
			}
		}
		
	}

	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns)
	{
		OMElement filter = fac.createOMElement(FILTER, ns);
		XmlUtils.addContent(fac, filter, TYPE, "Set", ns);
		XmlUtils.addContent(fac, filter, DIMENSION, this.Dimension, ns);
		XmlUtils.addContent(fac, filter, COLUMN, this.Column, ns);
		XmlUtils.addContent(fac, filter, OPERATOR, this.Operator, ns);
		XmlUtils.addContent(fac, filter, OPERAND, this.Operand, ns);
		XmlUtils.addContent(fac, filter, DATATYPE, this.DataType, ns);
		parent.addChild(filter);
	}

	public String getDisplayString()
	{
		return (Dimension + "_" + Column + " " + Operator + " " + Operand);
	}
	
	/**
	 * build up Q{logical query from the report}
	 * @return logical query with Q{...}
	 */
	private String encloseINQConstruct(String queryText)
	{
		// should check for a single projection (i.e., one DC and/or one M)
		return "Q{" + queryText + "}";
	}

	/**
	 * Clone filter - does not copy any navigation information. Does a deep copy.
	 * 
	 * @see java.lang.Object#clone()
	 */
	public Object clone() throws CloneNotSupportedException
	{
		ReportFilter newrf = (ReportFilter) super.clone();
		newrf.Dimension = this.Dimension;
		newrf.Column = this.Column;
		newrf.Operator = this.Operator;
		newrf.Operand = this.Operand;
		return newrf;
	}
	
	/**
	 * Return whether two filters are logically equal
	 * 
	 * @param qf
	 * @return
	 */
	public boolean equals(Object o)
	{
		if (this == o)
			return true;
		if (!(o instanceof ReportFilter))
			return false;
		ReportFilter df = (ReportFilter) o;
		if (!df.Dimension.equals(this.Dimension))
			return false;
		if (!df.Column.equals(this.Column))
			return false;
		if (!df.Operator.equals(this.Operator))
			return false;
		if (!df.Operand.equals(this.Operand))
			return false;
		return true;
	}
		
}
