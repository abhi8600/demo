package com.successmetricsinc.adhoc;

import java.util.Iterator;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.successmetricsinc.Column;
import com.successmetricsinc.Repository;
import com.successmetricsinc.ServerContext;
import com.successmetricsinc.WebServices.IWebServiceResult;
import com.successmetricsinc.query.MeasureColumn;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class ColumnSelector implements IWebServiceResult {
	public static final String UNIQUE_ID = "ID";
	public static final String ADHOC_COLUMN = AdhocColumn.class.getName();
	public static final String COLUMN_SELECTOR = "ColumnSelector";
	public static final String AGG_RULE = "AggRule";
	public static final String LABEL = "Label";
	private AdhocColumn adhocColumn;
	private String label;
	private String ID;
	private AdhocColumn originalColumn;

	public ColumnSelector() {
		
	}
	
	public ColumnSelector(Column node) {
		init(node.getDisplayName(), node.getDimension(), node.getName());
	}
	
	public void init(String label, String dimension, String column) {
		this.label = label;
		if (dimension != null && dimension.trim().length() > 0) {
			adhocColumn = new AdhocColumn(dimension, column);
		}
		else {
			adhocColumn = new AdhocColumn(column);
		}
		if (adhocColumn != null)
			ID = adhocColumn.getUniqueExpressionString();
	}
	
	public ColumnSelector(AdhocColumn col) {
		label = col.getLabelString();
		adhocColumn = col;
		if (adhocColumn != null)
			ID = adhocColumn.getUniqueExpressionString();
	}

	public void parseElement(OMElement parent) {
		String aggRule = null;
		String format = null;
		String dimension = null;
		String column = null;
		for (@SuppressWarnings("unchecked")
		Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();
			if (LABEL.equals(elName)) {
				label = XmlUtils.getStringContent(el);
			}
			else if (AGG_RULE.equals(elName)) {
				aggRule = XmlUtils.getStringContent(el);
			}
			else if (AdhocColumn.FORMAT.equals(elName)) {
				format = XmlUtils.getStringContent(el);
			}
			else if (AdhocColumn.DIMENSION.equals(elName)) {
				dimension = XmlUtils.getStringContent(el);
			}
			else if (AdhocColumn.COLUMN.equals(elName)) {
				column = XmlUtils.getStringContent(el);
			}
			else if (ADHOC_COLUMN.equals(elName)) {
				adhocColumn = new AdhocColumn();
				try {
					adhocColumn.parseElement(el);
					ID = adhocColumn.getUniqueExpressionString();
				}
				catch (Exception e) { }
			}
		}
		
		if (adhocColumn == null) {
			init(label, dimension, column);
			if (format != null && adhocColumn != null)
				adhocColumn.setFormat(format);
			if (aggRule != null && adhocColumn != null)
				adhocColumn.setExprAggRule(aggRule);
		}
	}
	public void parseElement(XMLStreamReader parser) throws Exception {
		String aggRule = null;
		String format = null;
		String dimension = null;
		String column = null;
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
			String elName = parser.getLocalName();
			if (LABEL.equals(elName)) {
				label = XmlUtils.getStringContent(parser);
			}
			else if (AGG_RULE.equals(elName)) {
				aggRule = XmlUtils.getStringContent(parser);
			}
			else if (AdhocColumn.FORMAT.equals(elName)) {
				format = XmlUtils.getStringContent(parser);
			}
			else if (AdhocColumn.DIMENSION.equals(elName)) {
				dimension = XmlUtils.getStringContent(parser);
			}
			else if (AdhocColumn.COLUMN.equals(elName)) {
				column = XmlUtils.getStringContent(parser);
			}
			else if (ADHOC_COLUMN.equals(elName)) {
				adhocColumn = new AdhocColumn();
				adhocColumn.parseElement(parser);
				ID = adhocColumn.getUniqueExpressionString();
			}
		}
		if (adhocColumn == null) {
			init(label, dimension, column);
			if (format != null && adhocColumn != null)
				adhocColumn.setFormat(format);
			if (aggRule != null && adhocColumn != null)
				adhocColumn.setExprAggRule(aggRule);
		}
	}
	
	@Override
	public void addContent(OMElement parent, OMFactory fac, OMNamespace ns) {
		OMElement formatEl = fac.createOMElement(COLUMN_SELECTOR, ns);
		XmlUtils.addContent(fac, formatEl, LABEL, label, ns);
		XmlUtils.addContent(fac, formatEl, UNIQUE_ID, ID, ns);
		OMElement column = fac.createOMElement(ADHOC_COLUMN, ns);
		adhocColumn.addContent(fac, column, ns);
		formatEl.addChild(column);
		parent.addChild(formatEl);
	}

	public AdhocColumn getAdhocColumn() {
		return adhocColumn;
	}

	public String getLabel() {
		return label;
	}
	
	public String getID() {
		return ID;
	}

	public AdhocColumn getOriginalColumn() {
		return originalColumn;
	}

	public void setOriginalColumn(AdhocColumn originalColumn) {
		this.originalColumn = originalColumn;
	}

	
}