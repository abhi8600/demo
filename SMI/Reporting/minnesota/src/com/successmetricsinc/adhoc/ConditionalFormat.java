/**
 * $Id: ConditionalFormat.java,v 1.7 2012-05-30 19:35:45 agarrison Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.adhoc;

import java.awt.Color;
import java.awt.Font;
import java.util.Iterator;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;

import com.successmetricsinc.ServerContext;
import com.successmetricsinc.query.DisplayExpression;
import com.successmetricsinc.query.NavigationException;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

/**
 * Class to encapsulate conditional formatting options
 * 
 * @author bpeters
 * 
 */
public class ConditionalFormat
{
	private static final String FONT_SIZE = "FontSize";

	private static final String FONT_STYLE = "FontStyle";

	private static final String FONT_NAME = "FontName";

	private static final String UNDERLINE = "Underline";

	public static final String CONDITIONAL_FORMAT = "ConditionalFormat";

	private static Logger logger = Logger.getLogger(ConditionalFormat.class);

    private static final String ELEMENT_NAME_ID = "Id";
	private static final String IMAGE_HEIGHT = "ImageHeight";
	private static final String IMAGE_WIDTH = "ImageWidth";
	private static final String IMAGE_PATH = "ImagePath";
	private static final String BACKGROUND_COLOR = "BackgroundColor";
	private static final String FOREGROUND_COLOR = "ForegroundColor";
	private static final String FONT = "Font";
	private static final String NAME = "Name";
	private static final String EXPRESSION = "Expression";
	private static final String FORMAT = "Format";
	private String Expression;
    private String fontName;
    private Integer fontStyle;
    private Integer fontSize;
    private Color ForegroundColor;
    private Color BackgroundColor;
    private String imagePath;
    private Integer imageWidth;
    private Integer imageHeight;
    private transient DisplayExpression DE;
    private Long ID = new Long(0); // = System.currentTimeMillis();
    private String format;
    private Boolean underline = false;

    public ConditionalFormat() {
//    	font = new Font(PageInfo.DEFAULT_FONT_NAME, PageInfo.DEFAULT_FONT_FACE, PageInfo.DEFAULT_FONT_SIZE);
    }
    
    public void parseElement(XMLStreamReader el) throws Exception {
		String expression = null;
		String name = null;
		for (XMLStreamIterator iter = new XMLStreamIterator(el); iter.hasNext(); ) {
			String elName = el.getLocalName();
			if (EXPRESSION.equals(elName)) {
				expression = XmlUtils.getStringContent(el);
			}
			else if (NAME.equals(elName)) {
				name = XmlUtils.getStringContent(el);
			}
			else if (FONT.equals(elName)) {
				Font f = XmlUtils.getFontContent(el);
				this.fontName = f.getFamily();
				this.fontStyle = f.getStyle();
				this.fontSize = f.getSize();
			}
			else if (FONT_NAME.equals(elName))
				this.fontName = XmlUtils.getStringContent(el);
			else if (FONT_STYLE.equals(elName))
				this.fontStyle = XmlUtils.convertStyleName(XmlUtils.getStringContent(el));
			else if (FONT_SIZE.equals(elName))
				this.fontSize = XmlUtils.getIntContent(el);
			else if (FOREGROUND_COLOR.equals(elName)) {
				this.ForegroundColor = XmlUtils.getColorContent(el);
			}
			else if (BACKGROUND_COLOR.equals(elName)) {
				this.BackgroundColor = XmlUtils.getColorContent(el);
			}
			else if (IMAGE_PATH.equals(elName)) {
				this.imagePath = XmlUtils.getStringContent(el);
			}
			else if (IMAGE_WIDTH.equals(elName)) {
				this.imageWidth = XmlUtils.getIntContent(el);
			}
			else if (IMAGE_HEIGHT.equals(elName)) {
				this.imageHeight = XmlUtils.getIntContent(el);
			}
			else if (ELEMENT_NAME_ID.equals(elName)) {
				this.ID = XmlUtils.getLongContent(el);
			}
			else if (FORMAT.equals(elName)) {
				this.format = XmlUtils.getStringContent(el);
			}
			else if (UNDERLINE.equals(elName)) {
				this.underline = XmlUtils.getBooleanContent(el);
			}
		}
		try {
			this.setExpression(expression, name);
		} catch (Exception e) {
			logger.error(e, e);
		}
    }
	@SuppressWarnings("unchecked")
	public void parseElement(OMElement parent) {
		String name = null;
		String expression = null;
		for (Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();
			if (EXPRESSION.equals(elName)) {
				expression = XmlUtils.getStringContent(el);
			}
			else if (NAME.equals(elName)) {
				name = XmlUtils.getStringContent(el);
			}
			else if (FONT.equals(elName)) {
				Font f = XmlUtils.getFontContent(el);
				this.fontName = f.getFamily();
				this.fontStyle = f.getStyle();
				this.fontSize = f.getSize();
			}
			else if (FONT_NAME.equals(elName))
				this.fontName = XmlUtils.getStringContent(el);
			else if (FONT_STYLE.equals(elName))
				this.fontStyle = XmlUtils.convertStyleName(XmlUtils.getStringContent(el));
			else if (FONT_SIZE.equals(elName))
				this.fontSize = XmlUtils.getIntContent(el);
			else if (FOREGROUND_COLOR.equals(elName)) {
				this.ForegroundColor = XmlUtils.getColorContent(el);
			}
			else if (BACKGROUND_COLOR.equals(elName)) {
				this.BackgroundColor = XmlUtils.getColorContent(el);
			}
			else if (IMAGE_PATH.equals(elName)) {
				this.imagePath = XmlUtils.getStringContent(el);
			}
			else if (IMAGE_WIDTH.equals(elName)) {
				this.imageWidth = XmlUtils.getIntContent(el);
			}
			else if (IMAGE_HEIGHT.equals(elName)) {
				this.imageHeight = XmlUtils.getIntContent(el);
			}
			else if (ELEMENT_NAME_ID.equals(elName)) {
				this.ID = XmlUtils.getLongContent(el);
			}
			else if (FORMAT.equals(elName)) {
				this.format = XmlUtils.getStringContent(el);
			}
			else if (UNDERLINE.equals(elName)) {
				this.underline = XmlUtils.getBooleanContent(el);
			}
		}
		try {
			this.setExpression(expression, name);
		} catch (Exception e) {
			logger.error(e, e);
		}
	}
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		OMElement formatEl = fac.createOMElement(CONDITIONAL_FORMAT, ns);
		XmlUtils.addContent(fac, formatEl, EXPRESSION, this.getExpression(), ns);
		XmlUtils.addContent(fac, formatEl, NAME, this.getName(), ns);
//		XmlUtils.addContent(fac, formatEl, FONT, XmlUtils.encodeFont(this.getFont()), ns);
		XmlUtils.addContent(fac, formatEl, FONT_NAME, this.fontName, ns);
		if (this.fontStyle != null)
			XmlUtils.addContent(fac, formatEl, FONT_STYLE, XmlUtils.getStyleName(this.fontStyle), ns);
		if (this.fontSize != null)
			XmlUtils.addContent(fac, formatEl, FONT_SIZE, this.fontSize, ns);
		XmlUtils.addContent(fac, formatEl, FOREGROUND_COLOR, this.getForegroundColor(), ns);
		XmlUtils.addContent(fac, formatEl, BACKGROUND_COLOR, this.getBackgroundColor(), ns);
		XmlUtils.addContent(fac, formatEl, IMAGE_PATH, this.getImagePath(), ns);
		if (this.getImageWidth() != null)
			XmlUtils.addContent(fac, formatEl, IMAGE_WIDTH, Integer.toString(this.getImageWidth()), ns);
		if (this.getImageHeight() != null)
			XmlUtils.addContent(fac, formatEl, IMAGE_HEIGHT, Integer.toString(this.getImageHeight()), ns);
		XmlUtils.addContent(fac, formatEl, ELEMENT_NAME_ID, Long.toString(this.getID()), ns);
		XmlUtils.addContent(fac, formatEl, FORMAT, format, ns);
		if (underline != null)
			XmlUtils.addContent(fac, formatEl, UNDERLINE, underline, ns);
		parent.addChild(formatEl);
	}
    /**
     * @return Returns the backgroundColor.
     */
    public Color getBackgroundColor()
    {
        return BackgroundColor;
    }

    /**
     * @param backgroundColor
     *            The backgroundColor to set.
     */
    public void setBackgroundColor(Color backgroundColor)
    {
        BackgroundColor = backgroundColor;
    }

    /**
     * @return Returns the expression.
     */
    public String getExpression()
    {
        return Expression;
    }

    /**
     * Set the expression associated with a conditional format
     * 
     * @param expression
     * @param name
     * @return Returns whether it was able to create a valid expression (i.e. it compiles and is boolean)
     * @throws DisplayExpressionException
     *             throws an exception if there's a problem
     * @throws NavigationException 
     */
    public boolean setExpression(String expression, String name)
    throws BaseException, CloneNotSupportedException
    {
        Expression = expression;
        DisplayExpression de = new DisplayExpression(ServerContext.getRepository(), expression, null);
        de.setName(name);
        de.setPosition(-1);
        if (de.isBoolean())
        {
            this.DE = de;
            return (true);
        }
        return (false);
    }

    /**
     * Return the format expression's name
     * 
     * @return
     */
    public String getName()
    {
        if (DE==null)
            return (null);
        return (DE.getName());
    }

    /**
     * Set the format expression's name
     * 
     * @return
     */
    public void setName(String name)
    {
        if (DE!=null)
            DE.setName(name);
        return;
    }

    /**
     * @param font
     *            The font to set.
     */
    public void setFont(Font font)
    {
        this.fontName = font.getFamily();
        this.fontStyle = font.getStyle();
        this.fontSize = font.getSize();
    }
    
    public String getFontName() {
    	return this.fontName;
    }
    
    public Integer getFontStyle() {
    	return this.fontStyle;
    }
    
    public Integer getFontSize() {
    	return this.fontSize;
    }

    /**
     * @return Returns the foregroundColor.
     */
    public Color getForegroundColor()
    {
        return ForegroundColor;
    }

    /**
     * @param foregroundColor
     *            The foregroundColor to set.
     */
    public void setForegroundColor(Color foregroundColor)
    {
        ForegroundColor = foregroundColor;
    }

    /**
     * @return Returns the dE.
     */
    public DisplayExpression getDE()
    {
        return DE;
    }

    /**
     * @return Returns the iD.
     */
    public long getID()
    {
        return ID;
    }

    public void setID(long id) {
    	this.ID = id;
    }
    /**
     * @return Returns the imagePath
     */
    public String getImagePath()
    {
        return imagePath;
    }

    /**
     * @return Returns the imageHeight.
     */
    public Integer getImageHeight()
    {
        return imageHeight;
    }

    /**
     * @return Returns the imageWidth
     */
    public Integer getImageWidth()
    {
        return imageWidth;
    }

	/**
	 * @return the format
	 */
	public String getFormat() {
		return format;
	}

	public Boolean isUnderline() {
		return underline;
	}

	public void setUnderline(Boolean underline) {
		this.underline = underline;
	}
}
