/**
 * $Id: PageInfo.java,v 1.34 2012-04-02 21:27:11 agarrison Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.adhoc;

import java.awt.Font;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.stream.XMLStreamReader;

import net.sf.jasperreports.engine.design.JRDesignBand;
import net.sf.jasperreports.engine.design.JRDesignGroup;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

public class PageInfo implements Serializable
{

	private static final String INCLUDE_IF_INDEX = "IncludeIfIndex";
	private static final String INCLUDE_IF = "IncludeIf";
	private static final String PAGE_FOOTER_INCLUDE_IF_INDEX = "PageFooterIncludeIfIndex";
	private static final String PAGE_FOOTER_INCLUDE_IF = "PageFooterIncludeIf";
	private static final String PAGE_HEADER_INCLUDE_IF_INDEX = "PageHeaderIncludeIfIndex";
	private static final String PAGE_HEADER_INCLUDE_IF = "PageHeaderIncludeIf";
	private static final String SUMMARY_INCLUDE_IF_INDEX = "SummaryIncludeIfIndex";
	private static final String SUMMARY_INCLUDE_IF = "SummaryIncludeIf";
	private static final String DETAIL_INCLUDE_IF_INDEX = "DetailIncludeIfIndex";
	private static final String DETAIL_INCLUDE_IF = "DetailIncludeIf";
	private static final String HEADER_INCLUDE_IF_INDEX = "HeaderIncludeIfIndex";
	private static final String HEADER_INCLUDE_IF = "HeaderIncludeIf";
	private static final String TITLE_INCLUDE_IF_INDEX = "TitleIncludeIfIndex";
	private static final String TITLE_INCLUDE_IF = "TitleIncludeIf";
	private static final long serialVersionUID = 1L;
	private static final String START_ON_NEW_PAGE = "StartOnNewPage";
	private static final String START_ON_NEW_PAGES = "StartOnNewPages";
	private static final String FORCE_PAGE_BREAK = "ForcePageBreak";
	private static final String SUMMARY_FORCE_BREAK = "SummaryForceBreak";
	private static final String DETAIL_FORCE_BREAK = "DetailForceBreak";
	private static final String HEADER_FORCE_BREAK = "HeaderForceBreak";
	private static final String TITLE_FORCE_BREAK = "TitleForceBreak";
	private static final String DO_NOT_BREAK_GROUP = "DoNotBreakGroup";
	private static final String DO_NOT_BREAK_GROUPS = "DoNotBreakGroups";
	private static final String GROUP_FOOTER = "GroupFooter";
	private static final String HEIGHT = "Height";
	private static final String GROUP_HEADER = "GroupHeader";
	private static final String STANDALONE = "Standalone";
	private static final String IS_PIVOT = "IsPivot";
	private static final String SUMMARY_BAND_HEIGHT = "SummaryBandHeight";
	private static final String DETAIL_BAND_HEIGHT = "DetailBandHeight";
	private static final String HEADER_BAND_HEIGHT = "HeaderBandHeight";
	private static final String TITLE_BAND_HEIGHT = "TitleBandHeight";
	private static final String PAGE_HEADER_BAND_HEIGHT = "PageHeaderBandHeight";
	private static final String PAGE_FOOTER_BAND_HEIGHT = "PageFooterBandHeight";
	private static final String CHART_MARGIN = "ChartMargin";
	private static final String FONT_SIZE = "FontSize";
	private static final String BOTTOM_MARGIN = "BottomMargin";
	private static final String RIGHT_MARGIN = "RightMargin";
	private static final String TOP_MARGIN = "TopMargin";
	private static final String LEFT_MARGIN = "LeftMargin";
	private static final String PIVOT_COLUMN_WIDTH = "PivotColumnWidth";
	private static final String COLUMN_WIDTH = "ColumnWidth";
	private static final String WIDTH = "Width";

	public static class PageSizeProperty {
		public int pixelWidth;
		public int pixelHeight;
		
		public PageSizeProperty(int width, int height) {
			pixelWidth = width;
			pixelHeight = height;
		}
	};
	
	private static Map<String, PageSizeProperty> pageSizeMap = new HashMap<String, PageSizeProperty>(); 
	
	static {
		pageSizeMap.put("LETTER", new PageSizeProperty(612, 792));
		pageSizeMap.put("NOTE", new PageSizeProperty(540, 720));
		pageSizeMap.put("LEGAL", new PageSizeProperty(612, 1008));
		pageSizeMap.put("A0", new PageSizeProperty(2380, 3368));
		pageSizeMap.put("A1", new PageSizeProperty(1684, 2380));
		pageSizeMap.put("A2", new PageSizeProperty(1190, 1684));
		pageSizeMap.put("A3", new PageSizeProperty(842, 1190));
		pageSizeMap.put("A4", new PageSizeProperty(595, 842));
		pageSizeMap.put("A5", new PageSizeProperty(421, 595));
		pageSizeMap.put("A6", new PageSizeProperty(297, 421));
		pageSizeMap.put("A7", new PageSizeProperty(210, 297));
		pageSizeMap.put("A8", new PageSizeProperty(148, 210));
		pageSizeMap.put("A9", new PageSizeProperty(105, 148));
		pageSizeMap.put("A10", new PageSizeProperty(74, 105));
		pageSizeMap.put("B0", new PageSizeProperty(2836, 4008));
		pageSizeMap.put("B1", new PageSizeProperty(2004, 2836));
		pageSizeMap.put("B2", new PageSizeProperty(1418, 2004));
		pageSizeMap.put("B3", new PageSizeProperty(1002, 1418));
		pageSizeMap.put("B4", new PageSizeProperty(709, 1002));
		pageSizeMap.put("B5", new PageSizeProperty(501, 709));
		pageSizeMap.put("ARCH_A", new PageSizeProperty(595, 842));
		pageSizeMap.put("ARCH_B", new PageSizeProperty(595, 842));
		pageSizeMap.put("ARCH_C", new PageSizeProperty(595, 842));
		pageSizeMap.put("ARCH_D", new PageSizeProperty(595, 842));
		pageSizeMap.put("ARCH_E", new PageSizeProperty(595, 842));
		pageSizeMap.put("FLSA", new PageSizeProperty(612, 936));
		pageSizeMap.put("FLSE", new PageSizeProperty(612, 936));
		pageSizeMap.put("HALFLETTER", new PageSizeProperty(396, 612));
		pageSizeMap.put("11X17", new PageSizeProperty(792, 1224));
		pageSizeMap.put("LEDGER", new PageSizeProperty(792, 1224));
		pageSizeMap.put("CUSTOM", new PageSizeProperty(0, 0));
	}
	
	// default font settings for everything except the charts
	public static final String DEFAULT_FONT_NAME = "Arial";
	public static final String DEFAULT_FONT_FACE_NAME = "PLAIN";
	public static final int DEFAULT_FONT_FACE = Font.PLAIN;
	public static final int DEFAULT_FONT_SIZE = 8;
	
	public static final int STANDALONE_HBORDER = 20;
	public static final int STANDALONE_VBORDER = 20;
	public static final int EMBEDDED_HBORDER = 2;
	public static final int EMBEDDED_VBORDER = 5;
	public static final int LETTER_WIDTH = 612;
	public static final int LETTER_HEIGHT = 792;
	public static final int DEFAULT_COLUMN_WIDTH = 100;
	public static final int DEFAULT_NUMBER_WIDTH = 54;
	public static final int DEFAULT_NEW_NUMBER_WIDTH = 50;
	public static int DEFAULT_DATE_WIDTH = getNumberWidth();
	public static int DEFAULT_NEW_DATE_WIDTH = DEFAULT_NEW_NUMBER_WIDTH;
	public static final int DEFAULT_PIVOT_COLUMN_WIDTH = 100;
	public static final int DEFAULT_CHART_VBORDER = 10;
	public static final int DEFAULT_PIVOT_COLUMN_BREAK_OFFSET = 20;
	public static final int PRINT_MAX_WIDTH = 40;
	public static final int MAX_WIDTH = 45;
	public static final int DEFAULT_ROW_HEIGHT = 13;
	public static final int DEFAULT_NEW_ROW_HEIGHT = 15;
	public static final int MIN_ROW_HEIGHT = 10;
	public static final int DEFAULT_TITLE_BAND_HEIGHT = 24;
	
	public static final String PAGE_SIZE_LETTER = "LETTER";
	public static final String PAGE_SIZE_CUSTOM = "CUSTOM";	
	
	public static final int DEFAULT_HORIZONTAL_MARGIN = 10;
	public static final int DEFAULT_VERTICAL_MARGIN = 10;
	
	public enum ForcePageBreak { None, Before, After };
	
	public static class Band implements Serializable {

		private static final long serialVersionUID = 1L;
		private static final String NONE = "None";
		private static final String BEFORE = "Before";
		private static final String AFTER = "After";
		public int height;
		public ForcePageBreak forcePageBreak;
		private String includeIfExpression;
		private int includeIfReportIndex;
		
		public Band(int h, String b) {
			height = h;
			setForcePageBreak(b);
		}
		
		public Band(int h) {
			height = h;
			forcePageBreak = ForcePageBreak.None;
		}

		public String getForcePageBreak() {
			if (forcePageBreak == ForcePageBreak.After)
				return AFTER;
			if (forcePageBreak == ForcePageBreak.Before)
				return BEFORE;
			
			return NONE;
		}

		public void setForcePageBreak(String b) {
			if (AFTER.equals(b))
				forcePageBreak = ForcePageBreak.After;
			else if (BEFORE.equals(b))
				forcePageBreak = ForcePageBreak.After;
			else
				forcePageBreak = ForcePageBreak.None;
		}

		public String getIncludeIfExpression() {
			return includeIfExpression;
		}

		public void setIncludeIfExpression(String includeIfExpression) {
			this.includeIfExpression = includeIfExpression;
		}

		public int getIncludeIfReportIndex() {
			return includeIfReportIndex;
		}

		public void setIncludeIfReportIndex(Integer includeIfReportIndex) {
			if (includeIfReportIndex != null)
				this.includeIfReportIndex = includeIfReportIndex.intValue();
		}
		
		public void copyFrom(Band o) {
			height = o.height;
			forcePageBreak = o.forcePageBreak;
			includeIfExpression = o.includeIfExpression;
			includeIfReportIndex = o.includeIfReportIndex;
		}
	}
	
	// Usage area width
	int width;
	// Width of a table column
	int columnWidth = DEFAULT_COLUMN_WIDTH;
	int pivotColumnWidth = DEFAULT_PIVOT_COLUMN_WIDTH;
	// Effective left margin
	int leftMar = DEFAULT_HORIZONTAL_MARGIN;
	int rightMar = DEFAULT_HORIZONTAL_MARGIN;
	// Effective top margin
	int topMar = DEFAULT_VERTICAL_MARGIN;
	int bottomMar = DEFAULT_VERTICAL_MARGIN;
	// Effective font size for table columns
	int fontSize = DEFAULT_FONT_SIZE;
	// Effective margin between chart and table
	int chartMar = DEFAULT_CHART_VBORDER;
	Band titleBand = new Band(0);
	// Height of header band
	Band headerBand = new Band(DEFAULT_ROW_HEIGHT);
	// Height of header band
	Band detailBand = new Band(DEFAULT_ROW_HEIGHT);
	// Height of header band
	Band summaryBand = new Band(0);
	private boolean isPivot = false;
	private boolean print = false;
	private boolean csv = false;
	private boolean standalone = true;
	// Group Band information
	Band[] groupHeaders;
	Band[] groupFooters;
	boolean[] doNotBreakGroups;
	boolean[] startGroupOnNewPage;
	JRDesignBand[] groupHeaderBands;
	JRDesignBand[] groupFooterBands;
	JRDesignGroup[] groups;
	
	private Band pageHeaderBand = new Band(0);
	private Band pageFooterBand = new Band(0);

	public PageInfo() {
		
	}
	/**
	 * Set the internal sizing parameters based on output size and parameters
	 * 
	 * @param report
	 * @param chartOptions
	 * @param outputHeight
	 * @param outputWidth
	 * @param print
	 * @param standalone
	 * @param numColumns
	 * @param numPivotColumns
	 * @param maxMeasureWidth
	 * @param isPivot
	 */
	public PageInfo(AdhocReport report, int outputHeight, int outputWidth, boolean print, boolean standalone,
			int numColumns, int numPivotColumns, int maxMeasureWidth, boolean isPivot, boolean csv)
	{
		this.print = print;
		this.isPivot = isPivot;
		this.standalone = standalone;
		this.csv = csv;
		if (numColumns == 0)
			return;
		if (report.getRowHeight() == 0)
			report.setRowHeight(report.isUseOldSizing() ? DEFAULT_ROW_HEIGHT : DEFAULT_NEW_ROW_HEIGHT);
		leftMar = report.getLeftMargin();
		rightMar = report.getRightMargin();
		topMar = report.getTopMargin();
		bottomMar = report.getBottomMargin();
		chartMar = DEFAULT_CHART_VBORDER;
		report.setTitleHeight(DEFAULT_TITLE_BAND_HEIGHT);
		fontSize = Math.max(print ? 5 : 6, DEFAULT_FONT_SIZE - ((isPivot ? numPivotColumns : numColumns) / 4));
		if(standalone)
		{
			width = report.getPageWidth() - report.getLeftMargin() - report.getRightMargin();	
		}
		else
		{
			width = outputWidth - 2 * leftMar;
		}
		/*
		 * Figure out how many columns have widths set and total
		 */
		int numWithSetWidths = 0;
		int totalSetWidth = 0;
		for (AdhocColumn rc : report.getColumns())
		{
			int width = rc.getWidth();
			if (width != DEFAULT_COLUMN_WIDTH) {
				numWithSetWidths++;
				totalSetWidth += rc.getWidth();
			}
		}
		if (numColumns > numWithSetWidths)
			columnWidth = Math.min(DEFAULT_COLUMN_WIDTH, Math.max(print ? PRINT_MAX_WIDTH : MAX_WIDTH, (width - totalSetWidth)
					/ (numColumns - numWithSetWidths)));
		else
			columnWidth = DEFAULT_COLUMN_WIDTH;
		if (numPivotColumns > 0)
			pivotColumnWidth = Math.min(DEFAULT_PIVOT_COLUMN_WIDTH, Math.max(print ? PRINT_MAX_WIDTH : MAX_WIDTH, width / numPivotColumns));
		else
			pivotColumnWidth = DEFAULT_PIVOT_COLUMN_WIDTH;
		if (maxMeasureWidth > 0 && maxMeasureWidth > pivotColumnWidth)
			pivotColumnWidth = maxMeasureWidth;
		// Adjust row height for smaller fonts
		report.setRowHeight(Math.max(MIN_ROW_HEIGHT, Math.min(report.getRowHeight(),
				(int) (report.getRowHeight() * (1 - 0.4 * ((DEFAULT_FONT_SIZE - fontSize) / DEFAULT_FONT_SIZE))))));
		
		for (AdhocEntity entity : report.getEntities()) {
			if (entity.getBand() == AdhocReport.Band.Detail) {
				report.setRowHeight(Math.max(entity.getHeight(), report.getRowHeight()));
			}
		}
		
		detailBand.height = Math.max(detailBand.height, report.getRowHeight());
		
		for (AdhocEntity entity : report.getEntities()) {
			if (entity.getBand() == AdhocReport.Band.Header) {
				headerBand.height = Math.max(entity.getHeight(), headerBand.height);
			}
			else if (entity.getBand() == AdhocReport.Band.Title)
				titleBand.height = Math.max(entity.getHeight() + entity.getTop(), titleBand.height);
		}
		
		/*
		 * Setup group bands
		 */
		groupHeaders = new Band[report.getNumGroups()];
		groupFooters = new Band[report.getNumGroups()];
		doNotBreakGroups = new boolean[report.getNumGroups()];
		startGroupOnNewPage = new boolean[report.getNumGroups()];
		for (int i = 0; i < report.getNumGroups(); i++)
		{
			groupHeaders[i] = new Band(report.getRowHeight());
			groupFooters[i] = new Band(report.getRowHeight());
			doNotBreakGroups[i] = false;
			startGroupOnNewPage[i] = false;
		}
		/*
		 * Make room for grand total
		 */
		if (report.isGrandtotals())
			summaryBand.height = detailBand.height;
	}

	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		OMElement pi = fac.createOMElement("PageInfo", ns);
		XmlUtils.addContent(fac, pi, WIDTH, width, ns);
		XmlUtils.addContent(fac, pi, COLUMN_WIDTH, columnWidth, ns);
		XmlUtils.addContent(fac, pi, PIVOT_COLUMN_WIDTH, pivotColumnWidth, ns);
		XmlUtils.addContent(fac, pi, LEFT_MARGIN, leftMar, ns);
		XmlUtils.addContent(fac, pi, TOP_MARGIN, topMar, ns);
		XmlUtils.addContent(fac, pi, RIGHT_MARGIN, rightMar, ns);
		XmlUtils.addContent(fac, pi, BOTTOM_MARGIN, bottomMar, ns);
		XmlUtils.addContent(fac, pi, FONT_SIZE, fontSize, ns);
		XmlUtils.addContent(fac, pi, CHART_MARGIN, chartMar, ns);
		XmlUtils.addContent(fac, pi, TITLE_BAND_HEIGHT, titleBand.height, ns);
		XmlUtils.addContent(fac, pi, TITLE_FORCE_BREAK, titleBand.getForcePageBreak(), ns);
		XmlUtils.addContent(fac, pi, TITLE_INCLUDE_IF, titleBand.getIncludeIfExpression(), ns);
		XmlUtils.addContent(fac, pi, TITLE_INCLUDE_IF_INDEX, titleBand.getIncludeIfReportIndex(), ns);
		XmlUtils.addContent(fac, pi, HEADER_BAND_HEIGHT, headerBand.height, ns);
		XmlUtils.addContent(fac, pi, HEADER_FORCE_BREAK, headerBand.getForcePageBreak(), ns);
		XmlUtils.addContent(fac, pi, HEADER_INCLUDE_IF, headerBand.getIncludeIfExpression(), ns);
		XmlUtils.addContent(fac, pi, HEADER_INCLUDE_IF_INDEX, headerBand.getIncludeIfReportIndex(), ns);
		XmlUtils.addContent(fac, pi, DETAIL_BAND_HEIGHT, detailBand.height, ns);
		XmlUtils.addContent(fac, pi, DETAIL_FORCE_BREAK, detailBand.getForcePageBreak(), ns);
		XmlUtils.addContent(fac, pi, DETAIL_INCLUDE_IF, detailBand.getIncludeIfExpression(), ns);
		XmlUtils.addContent(fac, pi, DETAIL_INCLUDE_IF_INDEX, detailBand.getIncludeIfReportIndex(), ns);
		XmlUtils.addContent(fac, pi, SUMMARY_BAND_HEIGHT, summaryBand.height, ns);
		XmlUtils.addContent(fac, pi, SUMMARY_FORCE_BREAK, summaryBand.getForcePageBreak(), ns);
		XmlUtils.addContent(fac, pi, SUMMARY_INCLUDE_IF, summaryBand.getIncludeIfExpression(), ns);
		XmlUtils.addContent(fac, pi, SUMMARY_INCLUDE_IF_INDEX, summaryBand.getIncludeIfReportIndex(), ns);
		XmlUtils.addContent(fac, pi, PAGE_HEADER_BAND_HEIGHT, pageHeaderBand.height, ns);
		XmlUtils.addContent(fac, pi, PAGE_HEADER_INCLUDE_IF, pageHeaderBand.getIncludeIfExpression(), ns);
		XmlUtils.addContent(fac, pi, PAGE_HEADER_INCLUDE_IF_INDEX, pageHeaderBand.getIncludeIfReportIndex(), ns);
		XmlUtils.addContent(fac, pi, PAGE_FOOTER_BAND_HEIGHT, pageFooterBand.height, ns);
		XmlUtils.addContent(fac, pi, PAGE_FOOTER_INCLUDE_IF, pageFooterBand.getIncludeIfExpression(), ns);
		XmlUtils.addContent(fac, pi, PAGE_FOOTER_INCLUDE_IF_INDEX, pageFooterBand.getIncludeIfReportIndex(), ns);
		XmlUtils.addContent(fac, pi, IS_PIVOT, isPivot, ns);
		XmlUtils.addContent(fac, pi, STANDALONE, standalone, ns);
		if (groupHeaders != null) {
			for (Band b : groupHeaders) {
				OMElement bandHeight = fac.createOMElement(GROUP_HEADER, ns);
				XmlUtils.addContent(fac, bandHeight, HEIGHT, b.height, ns);
				XmlUtils.addContent(fac, bandHeight, FORCE_PAGE_BREAK, b.getForcePageBreak(), ns);
				XmlUtils.addContent(fac, bandHeight, INCLUDE_IF, b.getIncludeIfExpression(), ns);
				XmlUtils.addContent(fac, bandHeight, INCLUDE_IF_INDEX, b.getIncludeIfReportIndex(), ns);
				pi.addChild(bandHeight);
			}
		}
		if (groupFooters != null) {
			for (Band b : groupFooters) {
				OMElement bandHeight = fac.createOMElement(GROUP_FOOTER, ns);
				XmlUtils.addContent(fac, bandHeight, HEIGHT, b.height, ns);
				XmlUtils.addContent(fac, bandHeight, FORCE_PAGE_BREAK, b.getForcePageBreak(), ns);
				XmlUtils.addContent(fac, bandHeight, INCLUDE_IF, b.getIncludeIfExpression(), ns);
				XmlUtils.addContent(fac, bandHeight, INCLUDE_IF_INDEX, b.getIncludeIfReportIndex(), ns);
				pi.addChild(bandHeight);
			}
		}
		if (doNotBreakGroups != null) {
			OMElement  d = fac.createOMElement(DO_NOT_BREAK_GROUPS, ns);
			for (boolean b: doNotBreakGroups) {
				XmlUtils.addContent(fac, d, DO_NOT_BREAK_GROUP, b, ns);
				pi.addChild(d);
			}
		}
		if (startGroupOnNewPage != null) {
			OMElement d = fac.createOMElement(START_ON_NEW_PAGES, ns);
			for (boolean b : startGroupOnNewPage) {
				XmlUtils.addContent(fac, d, START_ON_NEW_PAGE, b, ns);
				pi.addChild(d);
			}
		}
		parent.addChild(pi);
	}
	
	public void parseElement(XMLStreamReader parser) throws Exception {
		List<Band> groupHeaderList = new ArrayList<Band>();
		List<Band> groupFooterList = new ArrayList<Band>();
		List<Boolean> dontBreakGroups = new ArrayList<Boolean>();
		List<Boolean> startNewPages = new ArrayList<Boolean>();
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
			iter.next();
			String elName = parser.getLocalName();
			if (WIDTH.equals(elName)) {
				this.width = XmlUtils.getIntContent(parser);
			}
			else if (COLUMN_WIDTH.equals(elName)) {
				this.columnWidth = XmlUtils.getIntContent(parser);
			}
			else if (PIVOT_COLUMN_WIDTH.equals(elName)) {
				this.pivotColumnWidth = XmlUtils.getIntContent(parser);
			}
			else if (LEFT_MARGIN.equals(elName)) {
				this.leftMar = XmlUtils.getIntContent(parser);
			}
			else if (TOP_MARGIN.equals(elName)) {
				this.topMar = XmlUtils.getIntContent(parser);
			}
			else if (RIGHT_MARGIN.equals(elName)) {
				this.rightMar = XmlUtils.getIntContent(parser);
			}
			else if (BOTTOM_MARGIN.equals(elName)) {
				this.bottomMar = XmlUtils.getIntContent(parser);
			}
			else if (FONT_SIZE.equals(elName)) {
				this.fontSize = XmlUtils.getIntContent(parser);
			}
			else if (CHART_MARGIN.equals(elName)) {
				this.chartMar = XmlUtils.getIntContent(parser);
			}
			else if (TITLE_BAND_HEIGHT.equals(elName))
				this.titleBand.height = XmlUtils.getIntContent(parser);
			else if (TITLE_FORCE_BREAK.equals(elName))
				this.titleBand.setForcePageBreak(XmlUtils.getStringContent(parser));
			else if (TITLE_INCLUDE_IF.equals(elName))
				titleBand.setIncludeIfExpression(XmlUtils.getStringContent(parser));
			else if (TITLE_INCLUDE_IF_INDEX.equals(elName))
				titleBand.setIncludeIfReportIndex(XmlUtils.getIntContent(parser));
			else if (HEADER_BAND_HEIGHT.equals(elName)) {
				this.headerBand.height = XmlUtils.getIntContent(parser);
			}
			else if (HEADER_FORCE_BREAK.equals(elName))
				this.headerBand.setForcePageBreak(XmlUtils.getStringContent(parser));
			else if (HEADER_INCLUDE_IF.equals(elName))
				headerBand.setIncludeIfExpression(XmlUtils.getStringContent(parser));
			else if (HEADER_INCLUDE_IF_INDEX.equals(elName))
				headerBand.setIncludeIfReportIndex(XmlUtils.getIntContent(parser));
			else if (DETAIL_BAND_HEIGHT.equals(elName)) {
				this.detailBand.height = XmlUtils.getIntContent(parser);
			}
			else if (DETAIL_FORCE_BREAK.equals(elName))
				this.detailBand.setForcePageBreak(XmlUtils.getStringContent(parser));
			else if (DETAIL_INCLUDE_IF.equals(elName))
				detailBand.setIncludeIfExpression(XmlUtils.getStringContent(parser));
			else if (DETAIL_INCLUDE_IF_INDEX.equals(elName))
				detailBand.setIncludeIfReportIndex(XmlUtils.getIntContent(parser));
			else if (SUMMARY_BAND_HEIGHT.equals(elName)) {
				this.summaryBand.height = XmlUtils.getIntContent(parser);
			}
			else if (SUMMARY_FORCE_BREAK.equals(elName))
				this.summaryBand.setForcePageBreak(XmlUtils.getStringContent(parser));
			else if (SUMMARY_INCLUDE_IF.equals(elName))
				summaryBand.setIncludeIfExpression(XmlUtils.getStringContent(parser));
			else if (SUMMARY_INCLUDE_IF_INDEX.equals(elName))
				summaryBand.setIncludeIfReportIndex(XmlUtils.getIntContent(parser));
			else if (PAGE_HEADER_BAND_HEIGHT.equals(elName))
				this.pageHeaderBand.height = XmlUtils.getIntContent(parser);
			else if (PAGE_HEADER_INCLUDE_IF.equals(elName))
				pageHeaderBand.setIncludeIfExpression(XmlUtils.getStringContent(parser));
			else if (PAGE_HEADER_INCLUDE_IF_INDEX.equals(elName))
				pageHeaderBand.setIncludeIfReportIndex(XmlUtils.getIntContent(parser));
			else if (PAGE_FOOTER_BAND_HEIGHT.equals(elName))
				this.pageFooterBand.height = XmlUtils.getIntContent(parser);
			else if (PAGE_FOOTER_INCLUDE_IF.equals(elName))
				pageFooterBand.setIncludeIfExpression(XmlUtils.getStringContent(parser));
			else if (PAGE_FOOTER_INCLUDE_IF_INDEX.equals(elName))
				pageFooterBand.setIncludeIfReportIndex(XmlUtils.getIntContent(parser));
			else if (IS_PIVOT.equals(elName)) {
				this.isPivot = XmlUtils.getBooleanContent(parser);
			}
			else if (STANDALONE.equals(elName)) {
				this.standalone = XmlUtils.getBooleanContent(parser);
			}
			else if (GROUP_HEADER.equals(elName)) {
				Band b = parseGroupHeaders(parser);
				if (b != null) {
					groupHeaderList.add(b);
				}
			}
			else if (GROUP_FOOTER.equals(elName)) {
				Band b = parseGroupHeaders(parser);
				if (b != null) {
					groupFooterList.add(b);
				}
			}
			else if (DO_NOT_BREAK_GROUPS.equals(elName)) {
				for (XMLStreamIterator iter2 = new XMLStreamIterator(parser); iter2.hasNext(); ) {
					iter2.next();
					String el2Name = parser.getLocalName();
					if (DO_NOT_BREAK_GROUP.equals(el2Name)) {
						dontBreakGroups.add(XmlUtils.getBooleanContent(parser));
					}
				}
			}
			else if (START_ON_NEW_PAGES.equals(elName)) {
				for (XMLStreamIterator iter2 = new XMLStreamIterator(parser); iter2.hasNext(); ) {
					iter2.next();
					String el2Name = parser.getLocalName();
					if (START_ON_NEW_PAGE.equals(el2Name)) {
						startNewPages.add(XmlUtils.getBooleanContent(parser));
					}
				}
			}
		}
		groupHeaders = new Band[groupHeaderList.size()];
		for (int i = 0; i < groupHeaderList.size(); i++ ) {
			groupHeaders[i] = groupHeaderList.get(i);
		}
		groupFooters = new Band[groupFooterList.size()];
		for (int i = 0; i < groupFooterList.size(); i++ ) {
			groupFooters[i] = groupFooterList.get(i);
		}
		doNotBreakGroups = new boolean[dontBreakGroups.size()];
		for (int i = 0; i < dontBreakGroups.size(); i++) {
			doNotBreakGroups[i] = dontBreakGroups.get(i);
		}
		startGroupOnNewPage = new boolean[startNewPages.size()];
		for (int i = 0; i < startNewPages.size(); i++) {
			startGroupOnNewPage[i] = startNewPages.get(i);
		}
	}
	@SuppressWarnings("unchecked")
	public void parseElement(OMElement parent)
	{
		List<Band> groupHeaderList = new ArrayList<Band>();
		List<Band> groupFooterList = new ArrayList<Band>();
		List<Boolean> dontBreakGroups = new ArrayList<Boolean>();
		List<Boolean> startNewPages = new ArrayList<Boolean>();
		
		for (Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();
			if (WIDTH.equals(elName)) {
				this.width = XmlUtils.getIntContent(el);
			}
			else if (COLUMN_WIDTH.equals(elName)) {
				this.columnWidth = XmlUtils.getIntContent(el);
			}
			else if (PIVOT_COLUMN_WIDTH.equals(elName)) {
				this.pivotColumnWidth = XmlUtils.getIntContent(el);
			}
			else if (LEFT_MARGIN.equals(elName)) {
				this.leftMar = XmlUtils.getIntContent(el);
			}
			else if (TOP_MARGIN.equals(elName)) {
				this.topMar = XmlUtils.getIntContent(el);
			}
			else if (RIGHT_MARGIN.equals(elName)) {
				this.rightMar = XmlUtils.getIntContent(el);
			}
			else if (BOTTOM_MARGIN.equals(elName)) {
				this.bottomMar = XmlUtils.getIntContent(el);
			}
			else if (FONT_SIZE.equals(elName)) {
				this.fontSize = XmlUtils.getIntContent(el);
			}
			else if (CHART_MARGIN.equals(elName)) {
				this.chartMar = XmlUtils.getIntContent(el);
			}
			else if (TITLE_BAND_HEIGHT.equals(elName))
				this.titleBand.height = XmlUtils.getIntContent(el);
			else if (TITLE_FORCE_BREAK.equals(elName))
				titleBand.setForcePageBreak(XmlUtils.getStringContent(el));
			else if (TITLE_INCLUDE_IF.equals(elName))
				titleBand.setIncludeIfExpression(XmlUtils.getStringContent(el));
			else if (TITLE_INCLUDE_IF_INDEX.equals(elName))
				titleBand.setIncludeIfReportIndex(XmlUtils.getIntContent(el));
			else if (HEADER_BAND_HEIGHT.equals(elName)) {
				this.headerBand.height = XmlUtils.getIntContent(el);
			}
			else if (HEADER_FORCE_BREAK.equals(elName))
				headerBand.setForcePageBreak(XmlUtils.getStringContent(el));
			else if (HEADER_INCLUDE_IF.equals(elName))
				headerBand.setIncludeIfExpression(XmlUtils.getStringContent(el));
			else if (HEADER_INCLUDE_IF_INDEX.equals(elName))
				headerBand.setIncludeIfReportIndex(XmlUtils.getIntContent(el));
			else if (DETAIL_BAND_HEIGHT.equals(elName)) {
				this.detailBand.height = XmlUtils.getIntContent(el);
			}
			else if (DETAIL_FORCE_BREAK.equals(elName))
				detailBand.setForcePageBreak(XmlUtils.getStringContent(el));
			else if (DETAIL_INCLUDE_IF.equals(elName))
				detailBand.setIncludeIfExpression(XmlUtils.getStringContent(el));
			else if (DETAIL_INCLUDE_IF_INDEX.equals(elName))
				detailBand.setIncludeIfReportIndex(XmlUtils.getIntContent(el));
			else if (SUMMARY_BAND_HEIGHT.equals(elName)) {
				this.summaryBand.height = XmlUtils.getIntContent(el);
			}
			else if (SUMMARY_FORCE_BREAK.equals(elName))
				summaryBand.setForcePageBreak(XmlUtils.getStringContent(el));
			else if (SUMMARY_INCLUDE_IF.equals(elName))
				summaryBand.setIncludeIfExpression(XmlUtils.getStringContent(el));
			else if (SUMMARY_INCLUDE_IF_INDEX.equals(elName))
				summaryBand.setIncludeIfReportIndex(XmlUtils.getIntContent(el));
			else if (PAGE_HEADER_BAND_HEIGHT.equals(elName))
				this.pageHeaderBand.height = XmlUtils.getIntContent(el);
			else if (PAGE_HEADER_INCLUDE_IF.equals(elName))
				pageHeaderBand.setIncludeIfExpression(XmlUtils.getStringContent(el));
			else if (PAGE_HEADER_INCLUDE_IF_INDEX.equals(elName))
				pageHeaderBand.setIncludeIfReportIndex(XmlUtils.getIntContent(el));
			else if (PAGE_FOOTER_BAND_HEIGHT.equals(elName))
				this.pageFooterBand.height = XmlUtils.getIntContent(el);
			else if (PAGE_FOOTER_INCLUDE_IF.equals(elName))
				pageFooterBand.setIncludeIfExpression(XmlUtils.getStringContent(el));
			else if (PAGE_FOOTER_INCLUDE_IF_INDEX.equals(elName))
				pageFooterBand.setIncludeIfReportIndex(XmlUtils.getIntContent(el));
			else if (IS_PIVOT.equals(elName)) {
				this.isPivot = XmlUtils.getBooleanContent(el);
			}
			else if (STANDALONE.equals(elName)) {
				this.standalone = XmlUtils.getBooleanContent(el);
			}
			else if (GROUP_HEADER.equals(elName)) {
				Band b = parseGroupHeaders(el);
				if (b != null) {
					groupHeaderList.add(b);
				}
			}
			else if (GROUP_FOOTER.equals(elName)) {
				Band i = parseGroupHeaders(el);
				if (i != null) {
					groupFooterList.add(i);
				}
			}
			else if (DO_NOT_BREAK_GROUPS.equals(elName)) {
				for (Iterator<OMElement> iter2 = el.getChildElements(); iter2.hasNext(); ) {
					OMElement el2 = iter2.next();
					String el2Name = el2.getLocalName();
					if (DO_NOT_BREAK_GROUP.equals(el2Name)) {
						dontBreakGroups.add(XmlUtils.getBooleanContent(el2));
					}
				}
			}
			else if (START_ON_NEW_PAGES.equals(elName)) {
				for (Iterator<OMElement> iter2 = el.getChildElements(); iter2.hasNext(); ) {
					OMElement el2 = iter2.next();
					String el2Name = el2.getLocalName();
					if (START_ON_NEW_PAGE.equals(el2Name)) {
						startNewPages.add(XmlUtils.getBooleanContent(el2));
					}
				}
			}
		}
		groupHeaders = new Band[groupHeaderList.size()];
		for (int i = 0; i < groupHeaderList.size(); i++ ) {
			groupHeaders[i] = groupHeaderList.get(i);
		}
		groupFooters = new Band[groupFooterList.size()];
		for (int i = 0; i < groupFooterList.size(); i++ ) {
			groupFooters[i] = groupFooterList.get(i);
		}
		doNotBreakGroups = new boolean[dontBreakGroups.size()];
		for (int i = 0; i < dontBreakGroups.size(); i++) {
			doNotBreakGroups[i] = dontBreakGroups.get(i);
		}
		startGroupOnNewPage = new boolean[startNewPages.size()];
		for (int i = 0; i < startNewPages.size(); i++) {
			startGroupOnNewPage[i] = startNewPages.get(i);
		}
	}
	
	private Band parseGroupHeaders(XMLStreamReader parser) throws Exception {
		Band ret = new Band(0);
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
			String elName = parser.getLocalName();
			if (HEIGHT.equals(elName)) {
				ret.height = XmlUtils.getIntContent(parser);
			}
			else if (FORCE_PAGE_BREAK.equals(elName))
				ret.setForcePageBreak(XmlUtils.getStringContent(parser));
			else if (INCLUDE_IF.equals(elName))
				ret.setIncludeIfExpression(XmlUtils.getStringContent(parser));
			else if (INCLUDE_IF_INDEX.equals(elName))
				ret.setIncludeIfReportIndex(XmlUtils.getIntContent(parser));
		}
		return ret;
	}
	@SuppressWarnings("unchecked")
	private Band parseGroupHeaders(OMElement parent) {
		Band ret = new Band(0);
		for (Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();
			if (HEIGHT.equals(elName))
				ret.height = XmlUtils.getIntContent(el);
			else if (FORCE_PAGE_BREAK.equals(elName))
				ret.setForcePageBreak(XmlUtils.getStringContent(el));
			else if (INCLUDE_IF.equals(elName))
				ret.setIncludeIfExpression(XmlUtils.getStringContent(el));
			else if (INCLUDE_IF_INDEX.equals(elName))
				ret.setIncludeIfReportIndex(XmlUtils.getIntContent(el));
		}
		return ret;
	}

	public static PageSizeProperty getPageSize(String name) {
		return pageSizeMap.get(name);
	}
	public void setGroups(int numGroups, AdhocReport adhocReport)
	{
		if ((groupHeaders == null && numGroups > 0) || 
				(groupHeaders != null && numGroups != groupHeaders.length))
		{
			/*
			 * Setup group bands
			 */
			Band[] newh = new Band[numGroups];
			Band[] newf = new Band[numGroups];
			boolean[] newb = new boolean[numGroups];
			boolean[] startg = new boolean[numGroups];
			for (int i = 0; i < numGroups; i++) {
				newh[i] = new Band(adhocReport.getRowHeight());
				newf[i] = new Band(adhocReport.getRowHeight());
				newb[i] = false;
				startg[i] = false;
			}
			
			if (groupHeaders != null) {
				for (int i = 0; i < Math.min(numGroups, groupHeaders.length); i++)
				{
					newh[i] = groupHeaders[i];
					newf[i] = groupFooters[i];
					newb[i] = doNotBreakGroups[i];
					startg[i] = startGroupOnNewPage[i];
				}
			}
			groupHeaders = newh;
			groupFooters = newf;
			doNotBreakGroups = newb;
			startGroupOnNewPage = startg;
		}
	}
    
	public int getWidth() {
		return width;
	}

	public int getLeftMar() {
		return leftMar;
	}

	public int getRightMar() {
		return rightMar;
	}

	public int getSummaryBandHeight() {
		return summaryBand.height;
	}

	public void setSummaryBandHeight(int summaryBandHeight) {
		this.summaryBand.height = summaryBandHeight;
	}
	
	public Band getPageHeaderBand() {
		return pageHeaderBand;
	}
	
	public Band getPageFooterBand() {
		return pageFooterBand;
	}
	
	public int getPageHeaderHeight() {
		return this.pageHeaderBand.height;
	}
	public void setPageHeaderHeight(int h) {
		this.pageHeaderBand.height = h;
	}
	public int getPageFooterHeight() {
		return this.pageFooterBand.height;
	}
	public void setPageFooterHeight(int h) {
		this.pageFooterBand.height = h;
	}

	public boolean isPivot() {
		return isPivot;
	}

	public boolean isPrint() {
		return print;
	}

	public void setPrint(boolean print) {
		this.print = print;
	}

	public boolean isCsv() {
		return csv;
	}

	/**
	 * @return the DEFAULT_NUMBER_WIDTH
	 */
	public static int getNumberWidth() {
		return DEFAULT_NUMBER_WIDTH;
	}
	
	public void applyStyle(PageInfo pi) {
		if (pi != null) {
			width = pi.width;
			columnWidth = pi.columnWidth;
			pivotColumnWidth = pi.columnWidth;
			leftMar = pi.leftMar;
			rightMar = pi.rightMar;
			topMar = pi.topMar;
			bottomMar = pi.bottomMar;
			fontSize = pi.fontSize;
			chartMar = pi.chartMar;
		}
	}
}