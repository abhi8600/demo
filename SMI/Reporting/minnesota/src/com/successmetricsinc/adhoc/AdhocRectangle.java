/**
 * 
 */
package com.successmetricsinc.adhoc;

import java.awt.Color;
import java.util.Map;

import javax.xml.stream.XMLStreamReader;

import net.sf.jasperreports.engine.JRPropertiesMap;
import net.sf.jasperreports.engine.design.JRDesignElement;
import net.sf.jasperreports.engine.design.JRDesignRectangle;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.type.ModeEnum;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.successmetricsinc.Repository;
import com.successmetricsinc.util.ColorUtils;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class AdhocRectangle extends AdhocEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String LINE_WIDTH = "LineWidth";
	private static final String GRADIENT_END_COLOR = "GradientEndColor";
	private static final String LINE_COLOR = "LineColor";
	private static final String ELLIPSE_HEIGHT = "EllipseHeight";
	private static final String ELLIPSE_WIDTH = "EllipseWidth";
	private static final String USE_GRADIENT = "UseGradient";

	private int ellipseWidth = 20;
	private int ellipseHeight = 20;
	private Color lineColor;
	private Color gradientEndColor;
	private float lineWidth = 1.0f;
	private boolean useGradient = false;

	/* (non-Javadoc)
	 * @see com.successmetricsinc.adhoc.AdhocEntity#getElement(com.successmetricsinc.adhoc.AdhocReport, net.sf.jasperreports.engine.design.JasperDesign, com.successmetricsinc.adhoc.PageInfo, boolean)
	 */
	@Override
	public JRDesignElement getElement(AdhocReport report, Repository r,
			JasperDesign jasperDesign, PageInfo pi, boolean isPdf, Map<String, ColumnSelector> selectors) {
		JRDesignRectangle rect = new JRDesignRectangle();
		if (this.getTransparent())
			rect.setMode(ModeEnum.TRANSPARENT);
		rect.setX(getLeft());
		rect.setY(getTop());
		rect.setWidth(getWidth());
		rect.setHeight(getHeight());
		rect.setForecolor(getForeground());
		rect.setBackcolor(getBackground());
		rect.getLinePen().setLineColor(lineColor);
		rect.getLinePen().setLineWidth(lineWidth);
		JRPropertiesMap props = rect.getPropertiesMap();
		props.setProperty(AdhocReport.JASPERREPORTS_EXPORT_PREFIX + "EllipseWidth", String.valueOf(ellipseWidth));
		props.setProperty(AdhocReport.JASPERREPORTS_EXPORT_PREFIX + "EllipseHeight", String.valueOf(ellipseHeight));
		if (gradientEndColor != null && useGradient == true)
			props.setProperty(AdhocReport.JASPERREPORTS_EXPORT_PREFIX + "GradientEndColor", ColorUtils.colorToString(gradientEndColor));
		return rect;
	}
	
	@Override
	public boolean parseAttribute(OMElement el, String elName) {
		if (ELLIPSE_WIDTH.equals(elName)) {
			ellipseWidth = XmlUtils.getIntContent(el);
			return true;
		}
		else if (ELLIPSE_HEIGHT.equals(elName)) {
			ellipseHeight = XmlUtils.getIntContent(el);
			return true;
		}
		else if (LINE_COLOR.equals(elName)) {
			lineColor = XmlUtils.getColorContent(el);
			return true;
		}
		else if (GRADIENT_END_COLOR.equals(elName)) {
			gradientEndColor = XmlUtils.getColorContent(el);
			return true;
		}
		else if (LINE_WIDTH.equals(elName)) {
			lineWidth = XmlUtils.getIntContent(el);
			return true;
		}
		else if (USE_GRADIENT.equals(elName)) {
			useGradient = XmlUtils.getBooleanContent(el);
			return true;
		}
		return super.parseAttribute(el, elName);
	}
	
	@Override
	public boolean parseAttribute(XMLStreamReader el, String elName)
			throws Exception {
		if (ELLIPSE_WIDTH.equals(elName)) {
			ellipseWidth = XmlUtils.getIntContent(el);
			return true;
		}
		else if (ELLIPSE_HEIGHT.equals(elName)) {
			ellipseHeight = XmlUtils.getIntContent(el);
			return true;
		}
		else if (LINE_COLOR.equals(elName)) {
			lineColor = XmlUtils.getColorContent(el);
			return true;
		}
		else if (GRADIENT_END_COLOR.equals(elName)) {
			gradientEndColor = XmlUtils.getColorContent(el);
			return true;
		}
		else if (LINE_WIDTH.equals(elName)) {
			lineWidth = XmlUtils.getIntContent(el);
			return true;
		}
		else if (USE_GRADIENT.equals(elName)) {
			useGradient = XmlUtils.getBooleanContent(el);
			return true;
		}
		return super.parseAttribute(el, elName);
	}
	
	@Override
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, ELLIPSE_WIDTH, ellipseWidth, ns);
		XmlUtils.addContent(fac, parent, ELLIPSE_HEIGHT, ellipseHeight, ns);
		XmlUtils.addContent(fac, parent, LINE_COLOR, lineColor, ns);
		XmlUtils.addContent(fac, parent, USE_GRADIENT, useGradient, ns);
		XmlUtils.addContent(fac, parent, GRADIENT_END_COLOR, gradientEndColor, ns);
		XmlUtils.addContent(fac, parent, LINE_WIDTH, lineWidth, ns);
		super.addContent(fac, parent, ns);
	}

	public int getEllipseWidth() {
		return ellipseWidth;
	}

	public void setEllipseWidth(int ellipseWidth) {
		this.ellipseWidth = ellipseWidth;
	}

	public int getEllipseHeight() {
		return ellipseHeight;
	}

	public void setEllipseHeight(int ellipseHeight) {
		this.ellipseHeight = ellipseHeight;
	}

	public Color getLineColor() {
		return lineColor;
	}

	public void setLineColor(Color lineColor) {
		this.lineColor = lineColor;
	}

	public Color getGradientEndColor() {
		return gradientEndColor;
	}

	public void setGradientEndColor(Color gradientEndColor) {
		this.gradientEndColor = gradientEndColor;
	}

	public void setLineWidth(float lineWidth) {
		this.lineWidth = lineWidth;
	}

	public float getLineWidth() {
		return lineWidth;
	}

}
