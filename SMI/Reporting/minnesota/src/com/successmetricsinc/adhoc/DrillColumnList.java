/**
 * 
 */
package com.successmetricsinc.adhoc;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.successmetricsinc.DrillPath;
import com.successmetricsinc.query.Level;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class DrillColumnList {
	private static final String DRILL_COLUMN = "DrillColumn";
	private static final String DRILL_COLUMNS = "DrillColumns";
	private static final String LABEL = "Label";
	private String label;
	private List<DrillColumn> drillColumns;
	
	public DrillColumnList() {
		
	}
	
	public DrillColumnList(DrillPath drillPath, String dimensionName) {
		for (Level level : drillPath.getLevels()) {
			for (int index = 0; index < level.ColumnNames.length; index++) {
				if (! level.HiddenColumns[index]) {
					String lbl = dimensionName + "." + level.ColumnNames[index];
					if (getLabel() == null || getLabel().trim().length() == 0) {
						setLabel(lbl);
					}
					else {
						setLabel(getLabel() + ", " + lbl);
					}
					DrillColumn drillCol = new DrillColumn(dimensionName, level.ColumnNames[index]);
					addColumn(drillCol);
				}
			}
		}
	}
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public List<DrillColumn> getDrillColumns() {
		return drillColumns;
	}
	public void setDrillColumns(List<DrillColumn> drillColumns) {
		this.drillColumns = drillColumns;
	}
	public void parseElement(OMElement parent) throws Exception {
		for (@SuppressWarnings("unchecked")
		Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String name = el.getLocalName();
			if (LABEL.equals(name)) {
				label = XmlUtils.getStringContent(el);
			}
			else if (DRILL_COLUMNS.equals(name)) {
				parseDrillColumns(el);
			}
		}
		
	}
	public void parseElement(XMLStreamReader parser) throws Exception {
		for (@SuppressWarnings("rawtypes")
		Iterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
			String name = parser.getLocalName();
			if (LABEL.equals(name)) {
				label = XmlUtils.getStringContent(parser);
			}
			else if (DRILL_COLUMNS.equals(name)) {
				parseDrillColumns(parser);
			}
		}
	}
	protected void parseDrillColumns(OMElement element) throws Exception {
		drillColumns = new ArrayList<DrillColumn>();
		for (@SuppressWarnings("unchecked")
		Iterator<OMElement> iter = element.getChildElements(); iter.hasNext(); ) {
			DrillColumn dc = new DrillColumn();
			dc.parseElement(iter.next());
			drillColumns.add(dc);
		}
	}
	protected void parseDrillColumns(XMLStreamReader parser) throws Exception {
		drillColumns = new ArrayList<DrillColumn>();
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
			iter.next();
			DrillColumn dc = new DrillColumn();
			dc.parseElement(parser);
			drillColumns.add(dc);
		}
	}
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, LABEL, label, ns);
		if (drillColumns != null) {
			OMElement type = fac.createOMElement(DRILL_COLUMNS, ns);
			
			for (DrillColumn dc : drillColumns) {
				OMElement drillCol = fac.createOMElement(DRILL_COLUMN, ns);
				dc.addContent(fac, drillCol, ns);
				type.addChild(drillCol);
			}
			parent.addChild(type);
		}
	}

	public void addColumn(DrillColumn dc) {
		if (drillColumns == null) {
			drillColumns = new ArrayList<DrillColumn>();
		}
		drillColumns.add(dc);
	}
	
	public String toString() {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;
		OMElement parent = fac.createOMElement("DrillTo", ns);
		addContent(fac, parent, ns);
		return parent.toString();
	}
}
