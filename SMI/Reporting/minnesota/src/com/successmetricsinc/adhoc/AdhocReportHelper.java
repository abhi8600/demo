/**
 * 
 */
package com.successmetricsinc.adhoc;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRQuery;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.SimpleJasperReportsContext;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.fill.JRBaseFiller;
import net.sf.jasperreports.engine.fill.JRFillInterruptedException;
import net.sf.jasperreports.engine.fill.JRFiller;
import net.sf.jasperreports.engine.fill.JRVerticalFiller;
import net.sf.jasperreports.engine.type.OrientationEnum;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.apache.axis2.AxisFault;
import org.apache.axis2.json.JSONOMBuilder;
import org.apache.log4j.Logger;
import org.apache.log4j.rolling.RollingFileAppender;

import com.successmetricsinc.Column;
import com.successmetricsinc.Prompt;
import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.UserBean;
import com.successmetricsinc.UI.SMIPojoBase;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.WebServices.BirstWebServiceResult;
import com.successmetricsinc.adhoc.AdhocColumn.ColumnType;
import com.successmetricsinc.adhoc.AdhocEntity.Align;
import com.successmetricsinc.adhoc.AsynchReportGenerationException.Type;
import com.successmetricsinc.catalog.CatalogManager;
import com.successmetricsinc.chart.ChartOptions;
import com.successmetricsinc.chart.ChartOptions.ChartType;
import com.successmetricsinc.chart.AnyChart.AnyChartNode;
import com.successmetricsinc.query.DimensionColumn;
import com.successmetricsinc.query.DisplayExpression;
import com.successmetricsinc.query.DisplayFilter;
import com.successmetricsinc.query.DisplayOrder;
import com.successmetricsinc.query.Filter;
import com.successmetricsinc.query.GroupBy;
import com.successmetricsinc.query.IJasperDataSourceProvider;
import com.successmetricsinc.query.JasperDataSourceProvider;
import com.successmetricsinc.query.Query;
import com.successmetricsinc.query.QueryCancelledException;
import com.successmetricsinc.query.QueryFilter;
import com.successmetricsinc.query.QueryResultSet;
import com.successmetricsinc.query.QueryString;
import com.successmetricsinc.query.ResultSetCache;
import com.successmetricsinc.queueing.Message;
import com.successmetricsinc.queuing.AsyncReportResult;
import com.successmetricsinc.queuing.AsynchReportGeneration.AsyncReportGenerationMessageFactory;
import com.successmetricsinc.queuing.AsynchReportGeneration.AsynchReportMessage;
import com.successmetricsinc.queuing.AsynchReportGeneration.AsynchReportProducer;
import com.successmetricsinc.transformation.object.SavedExpressions;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.DateUtil;
import com.successmetricsinc.util.JasperUtil;
import com.successmetricsinc.util.SelectItem;
import com.successmetricsinc.util.SessionUtils;
import com.successmetricsinc.util.UserNotLoggedInException;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class AdhocReportHelper {
	private static Logger logger = Logger.getLogger(AdhocReportHelper.class);
	private static final int MAX_NUMBER_OF_FILTER_VALUES = 1000;
	private static boolean asynchReportGenerationEnabled = false;
	

	public static JasperPrint getDashboardJasperPrint(AdhocReport report, 
													  String prompts, 
													  String dashletPrompts, 
													  Repository r, 
													  boolean ignorePagination, 
													  SMIWebUserBean ub, 
													  boolean applyAllPrompts,
													  boolean isPdf,
													  boolean isPrint) throws Exception {
		if (report.isAsynchReportGeneration() && isAsynchReportGenerationEnabled() && ub.isAsyncReportGenerationEnabled()) {
			AsynchReportMessage msg = AsyncReportGenerationMessageFactory.createAsyncReportGenerationMessage(report, prompts, dashletPrompts, r, ignorePagination, ub, applyAllPrompts, isPdf, isPrint);
			String reportRenderRequest = ub.getQueueIdFromReportRenderRequest(msg.getReportKey());
			if (reportRenderRequest != null) {
				if (! AsynchReportProducer.isReportRendered(ub, reportRenderRequest))
					throw new AsynchReportGenerationException(reportRenderRequest, "Report is being generated asynchronously");
				
				//delete messagedatafile
				File msgDataFile = new File(msg.getMessageDataFileName());
				if (msgDataFile.exists())
				{
					msgDataFile.delete();
				}
				
				// return jprint object
				return getAsynchronouslyGeneratedJasperPrintObject(ub, reportRenderRequest);
			}
			throw new AsynchReportGenerationException(msg, "Report is set to be generated asynchronously");
		}
		
		return getSynchDashboardJasperPrint(report, prompts, dashletPrompts, r, ignorePagination, ub, applyAllPrompts, isPdf, isPrint);
	}
	
	public static JasperPrint getAsynchronouslyGeneratedJasperPrintObject(UserBean ub, String reportRenderRequest) throws Exception {
		String filename = ub.getResultsPathFromQueueId(reportRenderRequest);
		if (filename != null) {
			AsyncReportResult result = AsyncReportResult.parseAsyncReportResult(filename);
			if (result.isSuccess() && result.getJasperPrintFileName() != null)
			{
				File jpFile = new File(result.getJasperPrintFileName());
				if (jpFile.exists())
				{
					Object o = JRLoader.loadObject(jpFile);
					if (o != null && o instanceof JasperPrint)
						return (JasperPrint)o;
				}
				else
				{
					throw new ReportGenerationException("Resultant print object not found.");
				}
			}
			else
			{
				throw new ReportGenerationException(result);
			}					
		}
		return null;
	}
	
	public static JasperPrint getSynchDashboardJasperPrint(AsyncReportGenerationMessageFactory.ReportGenerationData repData, Repository r, UserBean ub) throws Exception {
		return getSynchDashboardJasperPrint(repData.getReport(), repData.getPrompts(), repData.getDashletPrompts(), r,
				repData.isIgnorePagination(), ub, repData.isApplyAllPrompts(), repData.isPdf(), repData.isPrint());
	}
	
	public static JasperPrint getSynchDashboardJasperPrint(AdhocReport report,
		String prompts, String dashletPrompts, Repository r,
		boolean ignorePagination, UserBean ub, boolean applyAllPrompts,
		boolean isPdf, boolean isPrint) throws Exception {
			List<Filter> filterParams = AdhocReportHelper.parsePromptData(prompts);
			
			// run query with prompts
			Session session = null;
			if (ub != null) {
				session = ub.getRepSession();
			}
			if (session == null) {
				session = new Session();
			}
			session.setCancelled(false);
			Session.setParams(filterParams);
			Session.setDashletPrompts(dashletPrompts);
			Session.setPdf(isPdf);
			Session.setPrint(isPrint);
			Session.addThreadRequest(Thread.currentThread().getId(), session);
			Session.setUseOlapIndentation(report.isOlapUseIndentation());
			
			try {
				Map<String, Object> ps = getReportFilterParams(filterParams, dashletPrompts);
				
				long start = System.currentTimeMillis();
				JRDataSource jrds = report.getDashboardJRDataSource(dashletPrompts, r, filterParams, ub, session, applyAllPrompts, isPrint);
				logger.debug("+++ get query result set: " + (System.currentTimeMillis() - start));
				
				// adjust chart size for trellis chart (if it exists)
				report.adjustChartSizeForTrellis(jrds);

				start = System.currentTimeMillis();
				boolean selectorViewNoChart = ps.containsKey(ChartOptions.CHART_TYPE) && ps.get(ChartOptions.CHART_TYPE) == null;				
				JasperReport jasperReport = report.getDesign(r, isPrint, false, 
						true, true, ps, isPdf, selectorViewNoChart, dashletPrompts);
				
				JasperPrint prt = fillReport(jasperReport, ps, ignorePagination, dashletPrompts, applyAllPrompts, jrds);
				logger.debug("+++ get filled report: " + (System.currentTimeMillis() - start));				
				return prt;
			}
			finally {
				Session.removeThreadRequest(Thread.currentThread().getId());
			}
		}
	
	private static JasperPrint fillReport(JasperReport jasperReport, Map<String, Object> ps, boolean ignorePagination, String dashletPrompts, boolean applyAllPrompts, JRDataSource dataSource)
	throws JRException, QueryCancelledException
	{
		Thread.interrupted(); // clear the interrupt state
		
		SimpleJasperReportsContext context = new SimpleJasperReportsContext();
		Map<String, Object> params = JasperUtil.createParameterMap();
		params.putAll(ps);
		params.put("IS_IGNORE_PAGINATION", Boolean.valueOf(ignorePagination));
		params.put(AdhocChart.BIRST_SELECTORS, dashletPrompts);
		params.put(AdhocChart.APPLY_ALL_PROMPTS, Boolean.valueOf(applyAllPrompts));
		
		JRBaseFiller filler = JRFiller.createFiller(context, jasperReport);
		int maxPageCount = Session.getMaxPageCount();
		if (maxPageCount > 0 && filler instanceof JRVerticalFiller) {
			((JRVerticalFiller)filler).setMaxNumberOfPages(maxPageCount);
		}
		
		Session session = null;

		if (dataSource != null && dataSource instanceof IJasperDataSourceProvider)
		{
			 session = ((IJasperDataSourceProvider) dataSource).getJasperDataSourceProvider().getSession();
		}
		Session.addToCurrentFillers(filler);
		if (session != null) {
			session.setCancelled(false);
		}
		com.successmetricsinc.UserBean ub = com.successmetricsinc.UserBean.getParentThreadUserBean();
		boolean removeUserBean = false;
		if (ub == null)
		{
			removeUserBean = true;
			ub = SMIWebUserBean.getUserBean();
			SMIWebUserBean.addThreadRequest(Long.valueOf(Thread.currentThread().getId()), ub);
		}
		JasperPrint jasperPrint = null;
		try {
			params.put(JRParameter.REPORT_LOCALE, ub.getLocale());
			params.put(JRParameter.REPORT_TIME_ZONE, ub.getTimeZone());
			
			if (dataSource != null)
				jasperPrint = filler.fill(params, dataSource);
			else
				jasperPrint = filler.fill(params);
		}
		// for our use, this means that the filler was cancelled via our 'cancel' request, turn it into our 
		// exception
		catch (JRFillInterruptedException jre) {
			Thread.interrupted(); // clear the interrupt state			
			throw new QueryCancelledException("Query cancelled by user");
		}
		finally {
			if (removeUserBean)
				com.successmetricsinc.UserBean.removeThreadRequest(Long.valueOf(Thread.currentThread().getId()));
			Session.removeFromCurrentFillers(filler);			
		}
		
		Thread.interrupted(); // clear the interrupt state
		return jasperPrint;
	}
	
	public static Map<String, Object> getReportFilterParams(List<Filter> filterParams, String dashletPrompts){
		Map<String, Object> ps = new HashMap<String, Object>();
		if (filterParams != null) {
			for (Filter f: filterParams) {
				String value = QueryString.NO_FILTER_TEXT;
				// ignore unary operators
				if (QueryFilter.ISNOTNULL.equals(f.getOperator()) || 
						QueryFilter.ISNULL.equals(f.getOperator())) {
					if (ps.get(f.getColumnName()) == null)
						ps.put(f.getColumnName(), value);
					continue;
				}
				
				if (f.getValues().size() > 0) {
					StringBuilder sb = new StringBuilder();
					for (String s : f.getValues()) {
						if (sb.length() > 0) {
							if (f.getLogicalOperator() != null && f.getLogicalOperator().equals("AND"))
								sb.append("&&");
							else
								sb.append("||");
						}
						sb.append(s);
					}
					value = sb.toString();
				}
				ps.put(f.getColumnName(), value);
			}
		}
		AdhocReportHelper.addDashletPrompts(dashletPrompts, ps);
		return ps;
	}
	@SuppressWarnings("unchecked")
	public static void addDashletPrompts(String dashletPrompts,
			Map<String, Object> ps) {
		if (dashletPrompts != null && dashletPrompts.trim().length() > 0) {
			XMLStreamReader parser;
			try {
				XMLInputFactory inputFactory = XmlUtils.getSecureXMLInputFactory();
				parser = inputFactory.createXMLStreamReader(new StringReader(dashletPrompts));

				StAXOMBuilder builder = new StAXOMBuilder(parser);
				OMElement doc = null;
				doc = builder.getDocumentElement();
				doc.build();
				doc.detach();
				for (Iterator<OMElement> iter = doc.getChildElements(); iter.hasNext(); ) {
					OMElement el = iter.next();
					if (el.getLocalName().equals(AdhocReport.CHART_TYPE)) {
						ps.put(ChartOptions.CHART_TYPE, XmlUtils.getStringContent(el)); 
					}
					else if (el.getLocalName().equals(AdhocReport.COLUMN_SELECTORS)) {
						String col = null;
						String sel = null;
						OMElement selectedItem = null;
						for (Iterator<OMElement> iter2 = el.getChildElements(); iter2.hasNext(); ) {
							OMElement el2 = iter2.next();
							String nm = el2.getLocalName();
							if (AdhocReport.COLUMN.equals(nm)) {
								col = XmlUtils.getStringContent(el2);
							}
							else if (AdhocReport.SELECTED.equals(nm)) {
								sel = XmlUtils.getStringContent(el2);
							}
							else if (ColumnSelector.COLUMN_SELECTOR.equals(nm)) {
								String label = null;
								for (Iterator<OMElement> iter3 = el2.getChildElements(); iter3.hasNext(); ) {
									OMElement el3 = iter3.next();
									String nm3 = el3.getLocalName();
									if (ColumnSelector.UNIQUE_ID.equals(nm3)) {
										label = XmlUtils.getStringContent(el3);
									}
								}
								if (sel.equals(label)) {
									selectedItem = el2;
									break;
								}
							}
						}
						if (selectedItem != null) {
							for (Iterator<OMElement> iter2 = selectedItem.getChildElements(); iter2.hasNext(); ) {
								OMElement el2 = iter2.next();
								String name = el2.getLocalName();
								if (ColumnSelector.ADHOC_COLUMN.equals(name)) {
									try {
										AdhocColumn col1 = new AdhocColumn();
										col1.parseElement(el2);
										ps.put(col + "AdhocColumn", col1);
									}
									catch (Exception e) { }
								}
								else
									ps.put(col + name, XmlUtils.getStringContent(el2));
							}
						}
					}
				}
			} catch (XMLStreamException e) {
				logger.error(e, e);
			} catch (FactoryConfigurationError e) {
				logger.error(e, e);
			}
		}
	}

	public static JasperPrint getDashboardJasperPrint(String filename, String prompts, Repository r, SMIWebUserBean ub, boolean ignorePagination, boolean applyAllPrompts) throws Exception
	{
		return getDashboardJasperPrint(filename, prompts, r, ub, ignorePagination, applyAllPrompts, null);
	}
	public static JasperPrint getDashboardJasperPrint(String filename, String prompts, Repository r, SMIWebUserBean ub, boolean ignorePagination, boolean applyAllPrompts, String dashletPrompts) throws Exception
	{
		CatalogManager cm = ub.getCatalogManager();
		filename = cm.getNormalizedPath(filename);
		File adhocReportFile = CatalogManager.getAdhocReportFile(filename, cm.getCatalogRootPath());
		if (adhocReportFile != null) {
			AdhocReport report = AdhocReport.read(new File(filename));
			return getDashboardJasperPrint(report, prompts, dashletPrompts, r, ignorePagination, ub, applyAllPrompts, false, false);
		}
		String path = CatalogManager.getJRXMLFile(filename, cm.getCatalogRootPath());
		if (path == null)
		{
			logger.debug("bad report name for getDashboardJasperPrint: " + filename);
			return null;
		}
		JasperDesign jrxmlDesign =  JRXmlLoader.load(path);
		if (jrxmlDesign != null) {
			JasperReport jreport = JasperCompileManager.compileReport(jrxmlDesign);
			if (jreport != null) {
				Session session = null;
				session = ub.getRepSession();
				if (session == null) {
					session = new Session();
				}
				session.clear();
				List<Filter> filterParams = AdhocReportHelper.parsePromptData(prompts);
				if (session != null) {
					session.setCancelled(false);
				}
				Session.setParams(filterParams);
				Session.setDashletPrompts(null);
				Session.setPdf(false);
				Session.setPrint(false);
				Session.addThreadRequest(Thread.currentThread().getId(), session);
				try {
					long start = System.currentTimeMillis();
					Map<String, Object> ps = getReportFilterParams(filterParams, dashletPrompts);
					JRQuery query = jreport.getQuery();
					JasperDataSourceProvider jdsp = new JasperDataSourceProvider(r);
					jdsp.setSession(session);
					jdsp.setParams(filterParams);
					jdsp.setApplyAllPrompts(applyAllPrompts);
					JRDataSource dataSource = null;
					if (query != null)
					{
						dataSource = jdsp.create(query.getText(), session);
					}
					logger.debug("+++ get query result set (2): " + (System.currentTimeMillis() - start));
					start = System.currentTimeMillis();
					JasperPrint jprt = fillReport(jreport, ps, ignorePagination, dashletPrompts, applyAllPrompts, dataSource);
					logger.debug("+++ get filled report (2): " + (System.currentTimeMillis() - start));				
					return jprt;
				}
				finally {
					Session.removeThreadRequest(Thread.currentThread().getId());
				}
			}
		}
		
		return null;
	}

	public static OMElement jsonToOMElement(String json) throws UnsupportedEncodingException, AxisFault{
		ByteArrayInputStream bis = new ByteArrayInputStream(json.getBytes("UTF-8"));
		JSONOMBuilder omBuilder = new JSONOMBuilder();
		return omBuilder.processDocument(bis, null, null);
	}
	
	public static OMElement xmlToOMElement(String xml) throws XMLStreamException{
		XMLStreamReader parser;
		
		XMLInputFactory inputFactory = XmlUtils.getSecureXMLInputFactory();
		parser = inputFactory.createXMLStreamReader(new StringReader(xml));

		StAXOMBuilder builder = new StAXOMBuilder(parser);
		OMElement doc = null;
		doc = builder.getDocumentElement();
		doc.build();
		doc.detach();
		
		return doc;
	}
	
	public static List<Filter> parsePromptDataJson(String prompts) throws Exception{
		List<Filter> filterParams = new ArrayList<Filter>();
		if (prompts != null && prompts.trim().length() > 0) {
			try {
				OMElement el = jsonToOMElement(prompts);
				filterParams = parsePromptDataFromOMElement(el);
			} catch (AxisFault e) {
				logger.error(e, e);
			} catch (UnsupportedEncodingException e) {
				logger.error(e, e);
			}
		}
		
		return filterParams;
	}
	
	@SuppressWarnings("unchecked")
	public static List<Filter> parsePromptData(String prompts) throws Exception {
		List<Filter> filterParams = null;
		if (prompts != null && prompts.trim().length() > 0) {
			try {
				OMElement doc = xmlToOMElement(prompts);
				filterParams = parsePromptDataFromOMElement(doc);
			} catch (XMLStreamException e) {
				logger.error(e, e);
			} catch (FactoryConfigurationError e) {
				logger.error(e, e);
			}
		}
		return filterParams;
	}

	private static List<Filter> parsePromptDataFromOMElement(OMElement doc) throws SQLException{
		List<Filter> filterParams = new ArrayList<Filter>();
		
		com.successmetricsinc.UserBean userBean = SMIWebUserBean.getUserBean();
		Locale userLocale = userBean.getLocale();
		NumberFormat fromNF = null;
		if (userLocale != null)
			fromNF = NumberFormat.getNumberInstance(userLocale);
		else
			fromNF = NumberFormat.getNumberInstance();
		for (Iterator<OMElement> iter = doc.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			Prompt prompt = new Prompt();
			prompt.parseElement(el);
			List<String> selectedValues = prompt.getSelectedValues();
			if ((selectedValues != null) && (selectedValues.size() == 1)) {
				String value = selectedValues.get(0);
				if (value == null || !Util.hasNonWhiteSpaceCharacters(value)) {
					value = QueryString.NO_FILTER_TEXT;
					selectedValues.set(0, value);
				}
			}
			if (selectedValues != null) {
				String clazz = prompt.getClazz();
				if (Prompt.INTEGER.equals(clazz) || Prompt.FLOAT.equals(clazz) || Prompt.NUMBER.equals(clazz)) {
					
					for (int i = 0; i < selectedValues.size(); i++) {
						String v = selectedValues.get(i);
						if (QueryString.NO_FILTER_TEXT.equals(v))
							continue;
						
						try {
							Number n = fromNF.parse(v);
							selectedValues.set(i, String.valueOf(n));
						}
						catch (ParseException pe) {
							logger.warn("Error parsing prompt as a number: "
									+ "column name = " + prompt.getColumnName()
									+ ", parameter name = " + prompt.getParameterName() 
									+ ", clazz = " + clazz + ", value = " + v);
						}
					}
				}
				else if ((Prompt.DATE.equals(clazz) || Prompt.DATE_TIME.equals(clazz))) {
					List<SimpleDateFormat> formats = new ArrayList<SimpleDateFormat>();
					SimpleDateFormat sdf = null;
					if (prompt.getColumnFormat() != null && !prompt.getColumnFormat().isEmpty()) {
						sdf = new SimpleDateFormat(prompt.getColumnFormat());
						formats.add(sdf);
					}
					formats.add(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
					formats.add(new SimpleDateFormat("yyyy-MM-dd HH:mm"));
					formats.add(new SimpleDateFormat("yyyy-MM-dd"));
					TimeZone processingTZ = userBean.getRepository().getServerParameters().getProcessingTimeZone();
					TimeZone userTZ = userBean.getTimeZone();
					for (int i = 0; i < selectedValues.size(); i++) {
						String v = selectedValues.get(i);
						if (QueryString.NO_FILTER_TEXT.equals(v))
							continue;
						if (v == null)
							selectedValues.set(i, "null");
						else {
							if (v.length() > 11 && v.charAt(10) == 'T' && Character.isDigit(v.charAt(9)) && Character.isDigit(v.charAt(11))) {
								v = v.replace('T', ' ').replace('Z', ' ');
							}
							selectedValues.set(i, DateUtil.convertToDBFormat(v, formats, processingTZ, userTZ, Prompt.DATE_TIME.equals(clazz)));
						}
					}
				}
			}
			Filter filter = new Filter(prompt.getOperator(), selectedValues, prompt.getMultiSelectType()== Prompt.MultipleSelectionType.AND ? "AND" : prompt.getMultiSelectType() == Prompt.MultipleSelectionType.OR ? "OR" : "NONE", prompt.getFilterType(), prompt.getColumnName(), ! prompt.isExpression(), prompt.getClazz(),prompt.getTypeAsString());
			filterParams.add(filter);
		}
		
		return filterParams;
	}
	
	public static Map<String, Prompt> parsePromptMap(String prompts) throws Exception {
		Map<String, Prompt> promptMap = new HashMap<String, Prompt>();
		if (prompts != null && prompts.trim().length() > 0) {
			XMLStreamReader parser;
			try {
				XMLInputFactory inputFactory = XmlUtils.getSecureXMLInputFactory();
				parser = inputFactory.createXMLStreamReader(new StringReader(prompts));
				
				StAXOMBuilder builder = new StAXOMBuilder(parser);
				OMElement doc = null;
				doc = builder.getDocumentElement();
				doc.build();
				doc.detach();
				com.successmetricsinc.UserBean userBean = SMIWebUserBean.getUserBean();
				Locale userLocale = userBean.getLocale();
				NumberFormat fromNF = null;
				if (userLocale != null)
					fromNF = NumberFormat.getNumberInstance(userLocale);
				else
					fromNF = NumberFormat.getNumberInstance();
				for (Iterator<OMElement> iter = doc.getChildElements(); iter.hasNext(); ) {
					OMElement el = iter.next();
					Prompt prompt = new Prompt();
					prompt.parseElement(el);
					List<String> selectedValues = prompt.getSelectedValues();
					if ((selectedValues != null) && (selectedValues.size() == 1)) {
						String value = selectedValues.get(0);
						if (value == null || !Util.hasNonWhiteSpaceCharacters(value)) {
							value = QueryString.NO_FILTER_TEXT;
							selectedValues.set(0, value);
						}
					}
					if (selectedValues != null) {
						String clazz = prompt.getClazz();
						if (Prompt.INTEGER.equals(clazz) || Prompt.FLOAT.equals(clazz) || Prompt.NUMBER.equals(clazz)) {
							
							for (int i = 0; i < selectedValues.size(); i++) {
								String v = selectedValues.get(i);
								if (QueryString.NO_FILTER_TEXT.equals(v))
									continue;
								
								try {
									Number n = fromNF.parse(v);
									selectedValues.set(i, String.valueOf(n));
								}
								catch (ParseException pe) {
									logger.warn("Error parsing prompt as a number: "
											+ "column name = " + prompt.getColumnName()
											+ ", parameter name = " + prompt.getParameterName() 
											+ ", clazz = " + clazz + ", value = " + v);
								}
							}
						}
						else if ((Prompt.DATE.equals(clazz) || Prompt.DATE_TIME.equals(clazz))) {
							List<SimpleDateFormat> formats = new ArrayList<SimpleDateFormat>();
							SimpleDateFormat sdf = null;
							if (prompt.getColumnFormat() != null && !prompt.getColumnFormat().isEmpty()) {
								sdf = new SimpleDateFormat(prompt.getColumnFormat());
								formats.add(sdf);
							}
							formats.add(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
							formats.add(new SimpleDateFormat("yyyy-MM-dd HH:mm"));
							formats.add(new SimpleDateFormat("yyyy-MM-dd"));
							TimeZone processingTZ = userBean.getRepository().getServerParameters().getProcessingTimeZone();
							TimeZone userTZ = userBean.getTimeZone();
							for (int i = 0; i < selectedValues.size(); i++) {
								String v = selectedValues.get(i);
								if (QueryString.NO_FILTER_TEXT.equals(v))
									continue;
								if (v == null)
									selectedValues.set(i, "null");
								else {
									if (v.length() > 11 && v.charAt(10) == 'T' && Character.isDigit(v.charAt(9)) && Character.isDigit(v.charAt(11))) {
										v = v.replace('T', ' ').replace('Z', ' ');
									}
									selectedValues.set(i, DateUtil.convertToDBFormat(v, formats, processingTZ, userTZ, Prompt.DATE_TIME.equals(clazz)));
								}
							}
						}
					}
					promptMap.put(prompt.getParameterName(), prompt);
				}
			} catch (XMLStreamException e) {
				logger.error(e, e);
			} catch (FactoryConfigurationError e) {
				logger.error(e, e);
			}
		}
		return promptMap;
	}
	
	public static List<Drill> parseDrillCols(String xml, AdhocReport report) {
		List<Drill> ret = new ArrayList<Drill>();
		if (xml.startsWith("<")) {
			XMLStreamReader parser;
			try {
				XMLInputFactory inputFactory = XmlUtils.getSecureXMLInputFactory();
				parser = inputFactory.createXMLStreamReader(new StringReader(xml));
				
				StAXOMBuilder builder = new StAXOMBuilder(parser);
				OMElement doc = null;
				doc = builder.getDocumentElement();
				Drill drill = new Drill();
				drill.parseElement(doc);
				ret.add(drill);
				return ret;
			} catch (XMLStreamException e) {
				logger.error(e, e);
			} catch (FactoryConfigurationError e) {
				logger.error(e, e);
			} catch (Exception e) {
				logger.error(e, e);
			}
		}
		else {
			// old style dim.col&dim2.col2
			String[] drills = xml.split("&");
			for (int i = 0; i < drills.length; i++) {
				try {
					String[] cols = drills[i].split("\\.");
					String dim = null;
					String col = null;
					AdhocColumn ac = null;
					if (cols.length > 1) {
						dim = URLDecoder.decode(cols[0], "UTF-8");
						col = URLDecoder.decode(cols[1], "UTF-8");
					
						for (AdhocColumn col1 : report.getColumns()) {
							if (dim.equals(col1.getDimension()) && col.equals(col1.getColumn())) {
								ac = col1;
								break;
							}
						}
						if (ac == null) {
							ac = new AdhocColumn(dim, col);
							ac.initDrillColumns(SessionUtils.getUserBean().getRepository(), null);
						}
					}
					else {
						col = URLDecoder.decode(cols[0], "UTF-8");
					
						for (AdhocColumn col1 : report.getColumns()) {
							if (col.equals(col1.getColumn())) {
								ac = col1;
								break;
							}
						}
						if (ac == null) {
							ac = new AdhocColumn(col);
							ac.initDrillColumns(SessionUtils.getUserBean().getRepository(), null);
						}
					}
					Drill drill = new Drill(ac, SessionUtils.getUserBean().getRepository());
					ret.add(drill);
				}
				catch (UnsupportedEncodingException e) {
					logger.error(e, e);
				}
			}
			return ret;
		}
		return null;
	}

	/**
		 * Drill down along the drill columns adding columns and filters as needed.  This is the stateless version of the method 
		 * @param report The report that is being drilled down
		 * @param drill2 The columns that are being drilled on
		 * @param filters Additional filters being added to the report
		 */
		public static void drillDown(AdhocReport report, List<Drill> drills, String filters) {
			
			for (Drill drill : drills) {
				boolean addToSeries = false;
				boolean addToSeries2 = false;
				boolean addToCategory = false;
				Boolean sortOrder = null;
				int sortPosition = -1;
				ChartOptions chartOptions = report.getChartOptions();
				String column = drill.getFromColumn();
				AdhocColumn ac1 = report.getColumnByName(column);
				if (ac1 != null) {
					column = ac1.getLabelString();
				}
				if (drill.getDrillColumns() != null) {
					if (chartOptions.type != ChartType.None) {
						if (chartOptions.series.contains(column)) {
							chartOptions.series.remove(column);
							addToSeries = true;
						}
						if (chartOptions.series2.contains(column)) {
							chartOptions.series2.remove(column);
							addToSeries2 = true;
						}
						if (chartOptions.categories.contains(column)) {
							chartOptions.categories.remove(column);
							addToCategory = true;
						}
					}
					if (report.getSorts() != null) {
						List<AdhocSort> sorts = report.getSorts();
						for (int j = 0; j < sorts.size(); j++) {
							AdhocSort as = sorts.get(j);
							AdhocColumn ac = as.getColumn();
							if (ac.getType() == AdhocColumn.ColumnType.Dimension &&
									ac.getDimension().equals(drill.getFromDimension()) && ac.getColumn().equals(column)) {

								if(as.getOrder() == AdhocSort.SortOrder.ASCENDING) 
									sortOrder = true;
								else
									sortOrder = false;
								
								if(ac.isRemoveColumnOnDrill()) {
								    sorts.remove(j);
							    }
							    sortPosition = j;
							    break;

							}	
						}
					}
				
					for (com.successmetricsinc.adhoc.DrillColumn dc : drill.getDrillColumns()) {
						boolean addedToChart = false;
						if (! report.containsDimensionColumn(dc.getDimension(), dc.getColumn())) {
							int rIndex = report.addDimensionColumn(dc.getDimension(), null, dc.getColumn(), null, dc.getDisplayName(), column, false);
							AdhocEntity newDim = report.getEntityById(rIndex);
							if (newDim != null && newDim instanceof AdhocColumn && ac1 != null)
								((AdhocColumn)newDim).setRemoveColumnOnDrill(ac1.isRemoveColumnOnDrill());
							if (sortOrder != null) {
								if (newDim != null && newDim instanceof AdhocColumn) {
									report.getSorts().add(sortPosition, new AdhocSort((AdhocColumn)newDim, sortOrder));
								}
							}
							if (chartOptions.type != ChartType.None && addedToChart == false)
							{
								if (addToSeries && !chartOptions.series.contains(dc.getColumn())) {
									chartOptions.series.add(dc.getColumn());
									addedToChart = true;
								}
								if (addToSeries2 && !chartOptions.series2.contains(dc.getColumn())) {
									chartOptions.series2.add(dc.getColumn());
									addedToChart = true;
								}
								if (addToCategory && !chartOptions.categories.contains(dc.getColumn())) {
									chartOptions.categories.add(dc.getColumn());
									addedToChart = true;
								}
							}
	//						Adhoc.updatePivotBorders(report);
						}
					}
					
					for (AdhocEntity ae : report.getEntities()) {
						if (ae instanceof AdhocChart) {
							((AdhocChart)ae).drillDown(report, drill);
						}
					}
				}
				if (ac1 != null && ac1.isRemoveColumnOnDrill())
					report.removeEntity(ac1);
			}
			
			if (filters != null && Util.hasNonWhiteSpaceCharacters(filters)) {
				String filter[] = filters.split("&");
				for (String f : filter) {
					String pairs[] = f.split("=");
					if (pairs.length < 2)
						continue;
						
					String[] col = pairs[0].split("\\.");
					if (col.length < 2)
						continue;
						
					QueryFilter qf;
					try {
						qf = new QueryFilter(URLDecoder.decode(col[0], "UTF-8"), 
											 URLDecoder.decode(col[1], "UTF-8"), 
											 "=", 
											 URLDecoder.decode(pairs[1], "UTF-8"));
						report.addFilter(qf);
					} catch (UnsupportedEncodingException e) {
						logger.error(e, e);
					}
				}
			}
			
			report.organizeColumns();
		}

	public static List<SelectItem> getColumnAvailableFilterValues(AdhocReport report, String dimensionName, String columnName, String queryValue, String format, boolean showAll)
	throws BaseException, SQLException, ClassNotFoundException, CloneNotSupportedException, IOException
	{
		if(format == null)
		{
			format = "";
		}
		int columnIndex = -1;
		Query q = null;
		List<DisplayFilter> dFilters = null;
		List<DisplayExpression> dExpressions = null;
		List<DisplayOrder> dOrders = null;
		Repository r = SMIPojoBase.getRepository();
	
		if (!showAll)
		{
			// only return those values that are in the result of the report query
			q = report.getQuery(r);
			int topN = q.getTopn();
			if (topN <= 0 || topN > MAX_NUMBER_OF_FILTER_VALUES)
				q.setTopn(MAX_NUMBER_OF_FILTER_VALUES);
			// do not make sense for this query
			// dFilters = report.getDisplayFilters();
			// dOrders = report.getDisplayOrders();
			dExpressions = report.getExpressionList();
		} 
		else
		{
			columnIndex = 0;
			q = new Query(SMIPojoBase.getRepository());
			q.setTopn(MAX_NUMBER_OF_FILTER_VALUES);
			q.addDimensionColumn(dimensionName, columnName);
			q.addGroupBy(new GroupBy(dimensionName, columnName));
			DimensionColumn dc = SMIPojoBase.getRepository().findDimensionColumn(dimensionName, columnName);
			if (queryValue != null && queryValue.length() > 0)
			{
				if (dc.DataType.equals("Varchar"))
				{
					q.addFilter(new QueryFilter(dimensionName, columnName, " LIKE ", queryValue + "%"));
				}
			}
		}
		
		// make sure we order by the column
		DisplayOrder dorder = null;
		if (dimensionName != null)
			dorder = new DisplayOrder(dimensionName, columnName, true);
		else
			dorder = new DisplayOrder(columnName, true);
		dOrders = new ArrayList<DisplayOrder>();
		dOrders.add(dorder);
		
		// run the query
		List<SelectItem> values = new ArrayList<SelectItem>();		
		Session repSession = SMIPojoBase.getSession();
		SMIWebUserBean ub = SMIPojoBase.getUserBean();
		ResultSetCache cache = ub.getCache();
		int maxRecords = r.getServerParameters().getMaxRecords();
		int maxQueryTime = r.getServerParameters().getMaxQueryTime();
		QueryResultSet qs = cache.getQueryResultSet(q, maxRecords, maxQueryTime, dFilters, dExpressions, dOrders, repSession, false, ub.getTimeZone(), false);
	
		if (!showAll)
		{
			// need to look at the result set
			int columns = qs.getTableNames().length;
			for (int i = 0; i < columns; i++)
			{
				if (qs.getTableNames()[i].equals(dimensionName) && qs.getColumnNames()[i].equals(columnName))
				{
					columnIndex = i;
					break;
				}
			}
			if (columnIndex < 0)
			{
				// this column is not in the projection list for the report
				return values;
			}
		}
		qs.resetCursor(-1);
		Object[][] rows = qs.getRows();
		Set<String> unique = new HashSet<String>();
		for (int i = 0; i < rows.length; i++)
		{
			if (rows[i][columnIndex] != null)
			{
				String value = rows[i][columnIndex].toString().trim();
				
				if (unique.contains(value)) // only add it once
					continue;
				unique.add(value);
				String[] types = qs.getWarehouseColumnsDataTypes();
				String dataType = types[columnIndex];
				if (dataType != null && (dataType.equals("Date") || dataType.equals("DateTime")))
				{
					//Bug 7412 - Date formats should always use processing time zone
					boolean isDateTime = dataType.equals("DateTime");
					
					Calendar cal = (Calendar)rows[i][columnIndex];
					
					SimpleDateFormat outputFormat = ub.getDisplayFormatDateOrDateTime(isDateTime, format);
					String label = outputFormat.format(cal.getTime());
					
					SimpleDateFormat valueFormat = ub.getDefaultFormatDateOrDateTime(isDateTime);
					String val = valueFormat.format(cal.getTime());
					
					values.add(new SelectItem(val, label));
				}
				else
				{
					value = value.replaceAll("  ", "&nbsp;&nbsp;");
					values.add(new SelectItem(value, rows[i][columnIndex].toString().trim()));
				}
				if(values.size() == MAX_NUMBER_OF_FILTER_VALUES)
				{
					break;
				}
			}
		}
		
		return values;
	}

	public static String convertDateToDBDate(String queryValue, String format, boolean isTime) {
		List<SimpleDateFormat> formats = new ArrayList<SimpleDateFormat>();
		if(format != null && Util.hasNonWhiteSpaceCharacters(format))
		{
			if (isTime)
				formats.add(SMIPojoBase.getUserBean().getDateTimeFormat(format));
			else
				formats.add(SMIPojoBase.getUserBean().getDateFormat(format));
		}
		if (isTime)
			formats.add(SMIPojoBase.getUserBean().getDateTimeFormat());
		else
			formats.add(SMIPojoBase.getUserBean().getDateFormat());
		return DateUtil.convertToDBFormat(queryValue, formats, 
				SMIPojoBase.getRepository().getServerParameters().getProcessingTimeZone(), 
				SMIPojoBase.getUserBean().getTimeZone(), isTime);
	
	}

	public static Column getColumn(String fullNodeName, String olapDimension, String olapHierarchy) {
		if (fullNodeName == null) 
			return null;
		
		Column node = null;
		if (fullNodeName.startsWith("EXPR.")) {
			node = new Column();
			node.setDimension("EXPR");
			node.setName(fullNodeName.substring("EXPR.".length()));
			return node;
		}
		node = new Column();
		node.setName(fullNodeName);
		if (olapDimension != null && !olapDimension.isEmpty())
		{
			node.setDimension(olapDimension);
			if (olapHierarchy != null && !olapHierarchy.isEmpty())
				node.setHierarchy(olapHierarchy);
		}
		else 
		{
			String[] names = fullNodeName.split("\\.");
			if (names.length > 2) {
				node.setDimension(names[0].substring(1, names[0].length() - 1));
				node.setHierarchy(names[1].substring(1, names[1].length() - 1));
				node.setName(names[2].substring(1, names[2].length() - 1));
			}
			else if (names.length > 1) 
			{
				node.setDimension(names[0]);
				node.setName(names[1]);
			}
		}
		return node;
		
	}
	
	public static int addColumnToAdhocReport(AdhocReport report, Column column, boolean isPivot) {
		return addColumnToAdhocReport(report, column, isPivot, false, null);
	}

	public static int addColumnToAdhocReport(AdhocReport report, Column column, boolean isPivot, boolean doNotAddIfExists, String vizPanel) {
			int reportIndex = -1;
			if (column != null && column.getName() != null) {
				if (column.getDimension() == null) {
					if (column.getName().startsWith("SavedExpression('")) {
						// parse it to get the name of the saved expression
						String label = column.getName().substring("SavedExpression('".length());
						if (label.endsWith("')")) {
							label = label.substring(0, label.length() - 2);
						}
						String originalLabel = label;
						String displayName = column.getDisplayName();
						if (displayName != null && !displayName.equals(column.getName()) && displayName.trim().length() > 0) {
							label = displayName;
						}
						AdhocColumn rc = report.addExpression(column.getName(), label, true);
						com.successmetricsinc.UserBean user = SMIWebUserBean.getUserBean();
						if (user != null) {
							SavedExpressions savedExpressions = user.getRepository().getSavedExpressions();
							if (savedExpressions != null) {
								com.successmetricsinc.transformation.object.SavedExpressions.Expression exp = savedExpressions.get(originalLabel);
								if (exp != null) {
									String format = exp.getFormat();
									if (format != null)
										rc.setFormat(format);
									if (exp.getColumnType() == SavedExpressions.ColumnType.Dimension)
										rc.setIsMeasureExpression(Boolean.FALSE);
									else { //if (exp.getColumnType() == ColumnType.Measure)
										rc.setIsMeasureExpression(Boolean.TRUE);
										rc.setAlignment(Align.Right);
										AdhocTextEntity lbl = rc.getLabelField();
										if (lbl != null)
											lbl.setAlignment(Align.Right);
									}
								}
							}
						}
						reportIndex = rc.getReportIndex();
					}
					else {
						// measure
						reportIndex = report.addMeasure(column.getName(), column.getDisplayName());
					}
				}
				else {
					if (doNotAddIfExists)
					{
						// check for existence, if so, return that index (used in trellis charts)
						for (AdhocEntity ae : report.getEntities())
						{
							if (ae instanceof AdhocColumn)
							{
								AdhocColumn ac = (AdhocColumn) ae;
								if (column.getDimension().equals(ac.getDimension()) && column.getName().equals(ac.getName()))
								{
									return ae.getReportIndex();
								}
							}
						}
					}
					reportIndex = report.addDimensionColumn(column.getDimension(), column.getHierarchy(), column.getMemberName(), column.getName(), column.getDisplayName(), null, isPivot);
				}
				
				
				if (vizPanel != null && vizPanel.trim().length() > 0) {
					AdhocEntity ae = report.getEntityById(reportIndex);
					if (ae != null && ae instanceof AdhocColumn) {
						AdhocColumn ac = (AdhocColumn)ae; 
						ac.setVisualizationPanel(vizPanel);
						setupVisualizationChart(report);
					}
				}
	//			updatePivotBorders(report);
				
				report.organizeColumns();
				report.setElementPositions(false, false, false, 
						report.getPageOrientation() == OrientationEnum.LANDSCAPE ? report.getPageHeight() : report.getPageWidth(),
						report.getPageOrientation() == OrientationEnum.LANDSCAPE ? report.getPageWidth() : report.getPageHeight()); 
			}
			return reportIndex;
		}
	
	public static void setupVisualizationChart(AdhocReport report) {
		List<AdhocColumn> rows = new ArrayList<AdhocColumn>();
		List<AdhocColumn> columns = new ArrayList<AdhocColumn>();
		List<String> timeDimesions = getTimeDimensions();
		int numColumnsMeasures = 0;
		int numRowsMeasures = 0;
		int numDetailsMeasures = 0;
		int numColumnsTimeDimensions = 0;
		int numRowsTimeDimensions = 0;
		int numDetailsTimeDimensions = 0;
		int numColumnsDimensions = 0;
		int numRowsDimensions = 0;
		int numDetailsDimensions = 0;
		for (AdhocColumn ac : report.getColumns()) {
			if (AdhocColumn.VISUALIZATION_PANEL_COLUMNS.equals(ac.getVisualizationPanel())) {
				columns.add(ac);
				if (ac.isMeasure())
					numColumnsMeasures++;
				if (ac.getType() == ColumnType.Dimension) {
					if (timeDimesions.contains(ac.getDimension()))
						numColumnsTimeDimensions++;
					numColumnsDimensions++;
				}
			}
			else if (AdhocColumn.VISUALIZATION_PANEL_ROWS.equals(ac.getVisualizationPanel())) {
				rows.add(ac);
				if (ac.isMeasure())
					numRowsMeasures++;
				if (ac.getType() == ColumnType.Dimension) {
					if (timeDimesions.contains(ac.getDimension()))
						numRowsTimeDimensions++;
					numRowsDimensions++;
				}
			}
			else if (AdhocColumn.VISUALIZATION_PANEL_MEASURE.equals(ac.getVisualizationPanel())) {
				if (ac.isMeasure())
					numDetailsMeasures++;
				if (ac.getType() == ColumnType.Dimension) {
					if (timeDimesions.contains(ac.getDimension()))
						numDetailsTimeDimensions++;
					numDetailsDimensions++;
				}
			}
		}
		
		VisualizationChartConfig vcc = VisualizationChartConfig.findChartType(numRowsMeasures, numColumnsMeasures, numDetailsMeasures, 
				numRowsTimeDimensions, numColumnsTimeDimensions, numDetailsTimeDimensions, 0, 0, 0, numRowsDimensions, numColumnsDimensions, numDetailsDimensions);
		AnyChartNode.ChartType chartType = null;
		if (vcc != null)
			chartType = vcc.getDefaultChartType();
		report.setIncludeTable(chartType == null);
		AdhocChart chart = report.getDefaultChart();
		if (chartType != null) {
			if (chart == null) {
				chart = new AdhocChart();
				chart.setBand(AdhocReport.Band.Title);
				report.setDefaultChart(chart);
			}
			chart.setupVisualizationChart(vcc, report);
		}
		else {
			if (chart != null)
				report.removeEntity(chart);
		}
	}

	public static List<String> getTimeDimensions() {
		UserBean ub = UserBean.getUserBean();
		Repository r = null;
		if (ub != null)
			r = ub.getRepository();
		List<String> ret = new ArrayList<String>();
		if (r != null) {
			if (r.getTimeDefinition() != null)
				ret.add(r.getTimeDefinition().Name);
			else
				ret.add("Time");
			
		}
		else
			ret.add("Time");
		
		return ret;
	}

	public static OMElement openReport(String filename, boolean isLoggedIn, SMIWebUserBean ub) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn) {
				throw new UserNotLoggedInException();
			}
			CatalogManager cm = ub.getCatalogManager();
			String root = cm.getCatalogRootPath();
			AdhocReport report = AdhocReport.read(new File(root + "/" + filename)); 
			result.setResult(report);
			
			// clear out jasper print cache 12923
			Session session = ub.getRepSession();
			session.getDesignerPageCache().clear();
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
		
	}
	
	public static boolean handleAsynchReportGenerationException(AsynchReportGenerationException arge, SMIWebUserBean ub, BirstWebServiceResult result) throws Exception {
		String id = null;
		if (arge.getType() == Type.NotRunning) {
			try {
				AsynchReportMessage m = arge.getAsynchMessage();
				id = AsynchReportProducer.postMessage(m, ub);
			} catch (Exception e) {
				// it looks like the queue server is down.  Turn off processing
				logger.error("Error posting a message to the queue.", e);
				//AdhocReportHelper.asynchReportGenerationEnabled = false;
				throw e;
			}
		}
		else {
			id = arge.getId();
		}
		
		// return id to client
		if (id != null) {
			result.setErrorCode(BaseException.ERROR_ASYNCHRONOUS_RENDERING);
			result.setAsynchronousResultId(id);
		}
		return true;
		
	}
	
	/**
	 * @param asynchReportGenerationEnabled the asynchReportGenerationEnabled to set
	 */
	public static void setAsynchReportGenerationEnabled(
			boolean asynchReportGenerationEnabled) {
		AdhocReportHelper.asynchReportGenerationEnabled = asynchReportGenerationEnabled;
	}

	/**
	 * @return the asynchReportGenerationEnabled
	 */
	public static boolean isAsynchReportGenerationEnabled() {
		return asynchReportGenerationEnabled;
	}
}
