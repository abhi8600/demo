/**
 * $Id: AdhocLine.java,v 1.7 2012-03-06 17:48:35 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */


package com.successmetricsinc.adhoc;

import java.util.Map;

import com.successmetricsinc.Repository;

import net.sf.jasperreports.engine.design.JRDesignElement;
import net.sf.jasperreports.engine.design.JRDesignLine;
import net.sf.jasperreports.engine.design.JasperDesign;

/**
 * @author agarrison
 *
 */
public class AdhocLine extends AdhocEntity {

	private static final long serialVersionUID = 1L;

	public AdhocLine() {
		
	}

	@Override
	public JRDesignElement getElement(AdhocReport report, Repository r, JasperDesign jasperDesign, PageInfo pi, boolean isPdf, Map<String, ColumnSelector> selectors) {
		JRDesignLine element = new JRDesignLine();
		this.setDefaultEntityProperties(element);
		return element;
	}
}
