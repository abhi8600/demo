package com.successmetricsinc.adhoc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.xml.stream.XMLStreamReader;

import org.apache.log4j.Logger;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.successmetricsinc.Repository;
import com.successmetricsinc.chart.Chart;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

public class DrillRow {
	
	
	private static final Logger logger = Logger.getLogger(DrillRow.class);
	
	private static final String DRILL_ROW_TYPE = "DrillType";
	private static final String DRILL_TO_DASHBOARD = "DrillToDashboard";
	private static final String DRILL_ROW_ID = "Id";
	public static final String DRILL_FROM_LABEL = "Label";
	public static final String DRILL_FROM_HEADER_LABEL = "HeaderLabel";
	public static final String DRILL_FROM_DIMENSION = "Dimension";
	public static final String DRILL_FROM_COLUMN = "Column";
	public static final String DRILL_FROM_COLUMN_TYPE = "ColumnType";
	//private static final String DRILL_DOWN_COLUMNS = "DrillDownColumns";
	private static final String DRILL_DOWN_COLUMN = "DrillDownColumn";
	private static final String DRILL_ROW_VALID = "Valid";
	
	
	// value of drill type
	private String drillType = Chart.DRILL_TYPE_DRILL;
	// value of drill from column	
	private Type type;
	private String Id;
	private String Name;
	private String headerLabel;
	private String Dimension;
	private String Column;
	// value of drill to columns for dimension
	private DrillColumnList drillToColumn;
	// value of drill to dashboard
	private String drillToDashboard;
	
	// valid flag to be shown on UI
	private boolean valid = true;
	
	public enum Type
	{
		Dimension, Measure
	};
	
	private static HashMap<Type,String> typeNames;
	
	static {
		typeNames = new HashMap<Type, String>();
		typeNames.put(Type.Dimension, "Dimension");
		typeNames.put(Type.Measure, "Measure");
	}
	
	public DrillRow(){
		
	}

	public void parseElement(XMLStreamReader parser) throws Exception 
	{
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
			
			String elName = parser.getLocalName();
			if (DRILL_ROW_TYPE.equals(elName))
			{
				String drillRowType = XmlUtils.getStringContent(parser);
				if(drillRowType != null)
				{
					// if nothing is given, get the default value
					if(Chart.DRILL_TYPE_DRILL.equals(drillRowType) || Chart.DRILL_TYPE_NAVIGATE.equals(drillRowType))
					{
						setDrillType(drillRowType); 
					}
				}
			}
			else if (DRILL_ROW_ID.equals(elName))
			{
				setId(XmlUtils.getStringContent(parser));
			}
			else if (DRILL_TO_DASHBOARD.equals(elName))
			{
				setDrillToDashboard(XmlUtils.getStringContent(parser));
			}
			else if (DRILL_FROM_LABEL.equals(elName))
			{
				setName(XmlUtils.getStringContent(parser));
			}
			else if (DRILL_FROM_DIMENSION.equals(elName))
			{
				setDimension(XmlUtils.getStringContent(parser));
			}
			else if (DRILL_FROM_COLUMN.equals(elName))
			{
				setColumn(XmlUtils.getStringContent(parser));
			}
			else if (DRILL_FROM_COLUMN_TYPE.equals(elName))
			{
				setColumnType(XmlUtils.getStringContent(parser));
			}
			else if (DRILL_DOWN_COLUMN.equals(elName))
			{
				parseDrillToColumn(parser);
			}
			else if(DRILL_FROM_HEADER_LABEL.equals(elName)){
				setHeaderLabel(XmlUtils.getStringContent(parser));
			}
		}
	}


	private void parseDrillToColumn(XMLStreamReader parser) throws Exception 
	{
		DrillColumnList dcl = new DrillColumnList();
		dcl.parseElement(parser);
		drillToColumn = dcl;
	}
	
	public String getDrillType() {
		return drillType;
	}

	public String getTypeString() {
		//return type != null ? typeNames.get(type) : null;
		return getTypeString(type);
	}
	
	public static String getTypeString(Type type)
	{
		return type != null ? typeNames.get(type) : null; 
	}
	
	public Type getType() {
		return type;
	}

	public String getName() {
		return Name;
	}

	public String getDimension() {
		return Dimension;
	}

	public String getColumn() {
		return Column;
	}
	
	public String getHeaderLabel() {
		return headerLabel;
	}

	public DrillColumnList getDrillToColumn() {
		return drillToColumn;
	}

	public void setDrillType(String drillType) {
		this.drillType = drillType;
	}
	
	public void setColumnType(String columnType) {
		for(Entry<Type, String> entry : typeNames.entrySet())
		{
			Type key = entry.getKey();
			String value = entry.getValue();
			if(value.equalsIgnoreCase(columnType))
			{
				this.type = key;
			}
		}
	}

	public void setName(String name) {
		Name = name;
	}

	public void setDimension(String dimension) {
		Dimension = dimension;
	}

	public void setColumn(String column) {
		Column = column;
	}

	public void setDrillToColumn(DrillColumnList drillToColumn) {
		this.drillToColumn = drillToColumn;
	}

	public String getDrillToDashboard() {
		return drillToDashboard;
	}

	public void setDrillToDashboard(String drillToDashboard) {
		this.drillToDashboard = drillToDashboard;
	}
	
	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}
	
	public void setHeaderLabel(String headerLabel) {
		this.headerLabel = headerLabel;
	}

	public void addContent(OMFactory fac, OMElement content, OMNamespace ns, boolean includeValidFlag) 
	{
		XmlUtils.addContent(fac, content, DRILL_ROW_ID, getId(), ns);
		XmlUtils.addContent(fac, content, DRILL_ROW_TYPE, getDrillType(), ns);
		XmlUtils.addContent(fac, content, DRILL_TO_DASHBOARD, getDrillToDashboard(), ns);
		XmlUtils.addContent(fac, content, DRILL_FROM_LABEL, getName(), ns);
		XmlUtils.addContent(fac, content, DRILL_FROM_HEADER_LABEL, getHeaderLabel(), ns);
		XmlUtils.addContent(fac, content, DRILL_FROM_DIMENSION, getDimension(), ns);
		XmlUtils.addContent(fac, content, DRILL_FROM_COLUMN, getColumn(), ns);
		XmlUtils.addContent(fac, content, DRILL_FROM_COLUMN_TYPE, getTypeString(), ns);
		if(includeValidFlag)
		{
			XmlUtils.addContent(fac, content, DRILL_ROW_VALID, isValid(), ns);
		}
		if(drillToColumn != null)
		{
			OMElement drillDownCol = fac.createOMElement(DRILL_DOWN_COLUMN, ns);
			drillToColumn.addContent(fac, drillDownCol, ns);
			content.addChild(drillDownCol);
		}
	}

	public void validate(Repository repository) 
	{
		String fromDimension = getDimension();
		String fromColumnName = getColumn();
		boolean valid = repository.findDimensionColumn(fromDimension, fromColumnName) != null;
		if(valid)
		{
			DrillColumnList list = getDrillToColumn();
			if(list != null )
			{
				List<DrillColumn> drillColumns = list.getDrillColumns();
				if(drillColumns != null && drillColumns.size() > 0)
				{
					for(DrillColumn drillColumn : list.getDrillColumns())
					{
						if(repository.findDimensionColumn(drillColumn.getDimension(), drillColumn.getColumn()) == null)
						{
							valid = false;
							break;
						}
					}
				}
			}
		}
		setValid(valid);
	}
	
}
