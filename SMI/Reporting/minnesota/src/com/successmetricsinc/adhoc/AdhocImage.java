/**
 * $Id: AdhocImage.java,v 1.24 2012-08-24 23:21:53 BIRST\agarrison Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */


package com.successmetricsinc.adhoc;

import java.util.List;
import java.util.Map;

import javax.xml.stream.XMLStreamReader;

import net.sf.jasperreports.engine.design.JRDesignElement;
import net.sf.jasperreports.engine.design.JRDesignExpression;
import net.sf.jasperreports.engine.design.JRDesignImage;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.type.HyperlinkTargetEnum;
import net.sf.jasperreports.engine.type.ScaleImageEnum;
import net.sf.jasperreports.engine.type.StretchTypeEnum;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.successmetricsinc.Repository;
import com.successmetricsinc.chart.Chart;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XmlUtils;


/**
 * @author agarrison
 *
 */
public class AdhocImage extends AdhocEntity {

	private static final String LOCATION_REPORT_INDEX = "LocationReportIndex";
	private static final String LOCATION_EXPRESSION = "LocationExpression";
	private static final String IS_DRILL_TO_DASHBOARD = "IsDrillToDashboard";
	private static final long serialVersionUID = 1L;
	private static final String URL = "URL";
	private static final String FILE = "File";
	private static final String DATA = "Data";
	private static final String LOCATION = "Location";
	private static final String IMAGE_TYPE = "ImageType";
	private String imageLocation;
	private boolean drillToDashboard = false;
	private String drillTo = null;
	private String drillToDash;
	private String drillToDashPage;
	
	public enum LocationType { None, File, Url, Data };
	
	private LocationType locationType = LocationType.File;
	private List<AdhocColumn.AdditionalDrillThruColumn> additionalDrillThuColumns;
	private String additionalDrillThruString;
	private int locationReportIndex = -1;
	private String locationReportExpression;

	public AdhocImage() {
		this.setStretchType(StretchTypeEnum.NO_STRETCH);
	}
	
	private String getImageLocation() {
		return imageLocation == null ? null : imageLocation.replaceAll("\\\\", "/");
	}

	private void setImageLocation(String imageLocation) {
		this.imageLocation = imageLocation;
	}

	private void setLocationType(LocationType locationType) {
		this.locationType = locationType;
	}

	@Override
	public JRDesignElement getElement(AdhocReport report, Repository r, JasperDesign jasperDesign, PageInfo pi, boolean isPdf, Map<String, ColumnSelector> selectors) {
		if (locationType == LocationType.None || (this.imageLocation == null && this.locationReportExpression == null)) {
			return null;
		}
		JRDesignImage image = new JRDesignImage(null);
		setDefaultEntityProperties(image);
		JRDesignExpression expression = new JRDesignExpression();
		if (this.locationType == LocationType.File) {
			expression.setText("ServerContext.getCatalogObject(\"" + getImageLocation() + "\")");
		}
		else if (this.locationType == LocationType.Url) {
			expression.setText("new java.net.URL(\"" + imageLocation + "\")");
		}
		else if (this.locationType == LocationType.Data) {
			expression.setText("new java.net.URL($F{" + getLocationFieldName() + "})");
		}
		image.setExpression(expression);
		image.setScaleImage(ScaleImageEnum.FILL_FRAME);
		if (drillToDashboard) {
			StringBuilder filter = new StringBuilder();
			if (additionalDrillThruString != null && additionalDrillThruString.length() > 0) {
				if (filter.length() > 0)
					filter.append('&');
				filter.append(additionalDrillThruString);
			}
			String drillThruText = AdhocColumn.getDrillThruString(getDrillTo(), r, Chart.DRILL_TYPE_NAVIGATE, filter, 
					additionalDrillThuColumns, report, null, false, false);
			if (drillThruText != null && drillThruText.trim().length() > 0) {
				JRDesignExpression refExp = new JRDesignExpression();
				refExp.setText(drillThruText);
	
				image.setHyperlinkReferenceExpression(refExp);
				image.setHyperlinkTarget(HyperlinkTargetEnum.SELF);
				image.setLinkType("javascript");
			}
			
		}
		return image;
	}
	public String getDrillToDash() {
		if (drillToDash == null && drillTo != null) {
			if (drillTo.indexOf('_') > 0) {
				String[] drillToParts = drillTo.split("_");
				drillToDash = drillToParts[0];
				drillToDashPage = drillToParts[1];
			}
		}
		return drillToDash;
	}
	public String getDrillToDashPage() {
		if (drillToDashPage == null && drillTo != null) {
			if (drillTo.indexOf('_') > 0) {
				String[] drillToParts = drillTo.split("_");
				drillToDash = drillToParts[0];
				drillToDashPage = drillToParts[1];
			}
		}
		return drillToDashPage;
	}
	private String getDrillTo() {
		if (getDrillToDash() == null || getDrillToDashPage() == null)
			return drillTo;
		return getDrillToDash() + "','" + getDrillToDashPage();
	}

	public String getHtmlDisplayLabel() {
		if (imageLocation != null) {
			return "i: " + this.getRelativePathname(imageLocation);
		}
		return "Image";
	}

	public boolean parseAttribute(OMElement el, String elName) {
		if (IMAGE_TYPE.equals(elName)) {
			String locationType = XmlUtils.getStringContent(el);
			if (FILE.equals(locationType)) {
				this.setLocationType(LocationType.File);
			}
			else if (URL.equals(locationType)) {
				this.setLocationType(LocationType.Url);
			}
			else if (DATA.equals(locationType))
				setLocationType(LocationType.Data);
			else
				this.setLocationType(LocationType.None);
			return true;
		}
		else if (LOCATION.equals(elName)) {
			this.setImageLocation(XmlUtils.getStringContent(el));
			return true;
		}
		else if (AdhocColumn.ADDITIONAL_DRILL_THRU_COLUMNS.equals(elName)) {
			this.additionalDrillThuColumns = AdhocColumn.AdditionalDrillThruColumn.parseAdditionalDrillThruColumn(el);
			return true;
		}
		else if (IS_DRILL_TO_DASHBOARD.equals(elName)){
			Boolean b = XmlUtils.getBooleanContent(el);
			this.drillToDashboard = (b == null ? false : b.booleanValue());
			return true;
		}
		else if (AdhocColumn.DRILL_TO.equals(elName)) {
			drillTo = XmlUtils.getStringContent(el);
			return true;
		}
		else if (AdhocColumn.DRILL_TO_DASH.equals(elName)) {
			drillToDash = XmlUtils.getStringContent(el);
			return true;
		}
		else if (AdhocColumn.DRILL_TO_DASH_PAGE.equals(elName)) {
			drillToDashPage = XmlUtils.getStringContent(el);
			return true;
		}
		else if (AdhocColumn.ADDITIONAL_DRILL_THRU_STRING.equals(elName)) {
			additionalDrillThruString = XmlUtils.getStringContent(el);
			return true;
		}
		else if (LOCATION_EXPRESSION.equals(elName)) {
			locationReportExpression = XmlUtils.getStringContent(el);
			return true;
		}
		else if (LOCATION_REPORT_INDEX.equals(elName)) {
			Integer i = XmlUtils.getIntContent(el);
			locationReportIndex = i == null ? -1 : i.intValue();
			return true;
		}
		return super.parseAttribute(el, elName);
	}
	
	public boolean parseAttribute(XMLStreamReader el, String elName) throws Exception {
		if (IMAGE_TYPE.equals(elName)) {
			String locationType = XmlUtils.getStringContent(el);
			if (FILE.equals(locationType)) {
				this.setLocationType(LocationType.File);
			}
			else if (URL.equals(locationType)) {
				this.setLocationType(LocationType.Url);
			}
			else if (DATA.equals(locationType))
				setLocationType(LocationType.Data);
			else
				this.setLocationType(LocationType.None);
			return true;
		}
		else if (LOCATION.equals(elName)) {
			this.setImageLocation(XmlUtils.getStringContent(el));
			return true;
		}
		else if (AdhocColumn.ADDITIONAL_DRILL_THRU_COLUMNS.equals(elName)) {
			this.additionalDrillThuColumns = AdhocColumn.AdditionalDrillThruColumn.parseAdditionalDrillThruColumn(el);
			return true;
		}
		else if (IS_DRILL_TO_DASHBOARD.equals(elName)){
			Boolean b = XmlUtils.getBooleanContent(el);
			this.drillToDashboard = (b == null ? false : b.booleanValue());
			return true;
		}
		else if (AdhocColumn.DRILL_TO.equals(elName)) {
			drillTo = XmlUtils.getStringContent(el);
			return true;
		}
		else if (AdhocColumn.DRILL_TO_DASH.equals(elName)) {
			drillToDash = XmlUtils.getStringContent(el);
			return true;
		}
		else if (AdhocColumn.DRILL_TO_DASH_PAGE.equals(elName)) {
			drillToDashPage = XmlUtils.getStringContent(el);
			return true;
		}
		else if (AdhocColumn.ADDITIONAL_DRILL_THRU_STRING.equals(elName)) {
			additionalDrillThruString = XmlUtils.getStringContent(el);
			return true;
		}
		else if (LOCATION_EXPRESSION.equals(elName)) {
			locationReportExpression = XmlUtils.getStringContent(el);
			return true;
		}
		else if (LOCATION_REPORT_INDEX.equals(elName)) {
			Integer i = XmlUtils.getIntContent(el);
			locationReportIndex = i == null ? -1 : i.intValue();
			return true;
		}
		return super.parseAttribute(el, elName);
	}
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		super.addContent(fac, parent, ns);
		if (this.locationType == LocationType.File) {
			XmlUtils.addContent(fac, parent, IMAGE_TYPE, FILE, ns);
			XmlUtils.addContent(fac, parent, LOCATION, this.getImageLocation(), ns);
			
		}
		else if (this.locationType == LocationType.Url) {
			XmlUtils.addContent(fac, parent, IMAGE_TYPE, URL, ns);
			XmlUtils.addContent(fac, parent, LOCATION, this.imageLocation, ns);
			
		}
		else if (locationType == LocationType.Data) {
			XmlUtils.addContent(fac, parent, IMAGE_TYPE, DATA, ns);
			XmlUtils.addContent(fac, parent, LOCATION_EXPRESSION, this.locationReportExpression, ns);
		}
		else {
			XmlUtils.addContent(fac, parent, IMAGE_TYPE, "None", ns);
		}
		XmlUtils.addContent(fac, parent, IS_DRILL_TO_DASHBOARD, drillToDashboard, ns);
		XmlUtils.addContent(fac, parent, AdhocColumn.DRILL_TO, drillTo, ns);
		XmlUtils.addContent(fac, parent, AdhocColumn.DRILL_TO_DASH, drillToDash, ns);
		XmlUtils.addContent(fac, parent, AdhocColumn.DRILL_TO_DASH_PAGE, drillToDashPage, ns);
		if (this.additionalDrillThuColumns != null && additionalDrillThuColumns.size() > 0) {
			AdhocColumn.AdditionalDrillThruColumn.addContent(fac, parent, additionalDrillThuColumns, ns);
		}
		if (additionalDrillThruString != null && additionalDrillThruString.length() > 0) 
			XmlUtils.addContent(fac, parent, AdhocColumn.ADDITIONAL_DRILL_THRU_STRING, additionalDrillThruString, ns);
		if (locationReportIndex >= 0) {
			XmlUtils.addContent(fac, parent, LOCATION_REPORT_INDEX, locationReportIndex, ns);
		}
	}
	
	public String getLocationFieldName()
	{
		StringBuilder fieldName = new StringBuilder();
		fieldName.append('F').append(this.locationReportIndex); //.append('_');
		return Util.replaceWithPhysicalString(fieldName.toString());
	}

	public int getLocationReportIndex() {
		return locationReportIndex;
	}
	
	public void setLocationReportIndex(int i) {
		locationReportIndex = i;
	}

	public String getLocationReportExpression() {
		return locationReportExpression;
	}

	public List<AdhocColumn.AdditionalDrillThruColumn> getAdditionalDrillThuColumns() {
		return additionalDrillThuColumns;
	}
}
