/**
 * $Id: AdhocEntity.java,v 1.83 2012-09-12 00:44:53 BIRST\ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */


package com.successmetricsinc.adhoc;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.Serializable;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamReader;

import net.sf.jasperreports.engine.design.JRDesignElement;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.type.ModeEnum;
import net.sf.jasperreports.engine.type.PenEnum;
import net.sf.jasperreports.engine.type.PositionTypeEnum;
import net.sf.jasperreports.engine.type.StretchTypeEnum;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.catalog.CatalogManager;
import com.successmetricsinc.util.SwapSpaceInProgressException;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public abstract class AdhocEntity implements Cloneable, Serializable {

	private static final long serialVersionUID = 1L;

	private static final String TRANSPARENT = "InvisibleBackground";

	private static Logger logger = Logger.getLogger(AdhocEntity.class);
	
	private static final String STRETCH_TYPE = "StretchType";
	private static final String POSITION_TYPE = "PositionType";
	private static final String ALIGNMENT = "Alignment";
	private static final String FONT = "Font";
	private static final String GROUP_NUMBER = "GroupNumber";
	private static final String BAND = "Band";
	private static final String TOP = "Top";
	private static final String LEFT = "Left";
	private static final String ELEMENT_HEIGHT = "Height";
	private static final String ELEMENT_WIDTH = "Width";
	private static final String BORDER_COLOR = "BorderColor";
	private static final String BACKGROUND_COLOR = "BackgroundColor";
	private static final String FOREGROUND_COLOR = "ForegroundColor";
	private static final String REPORT_INDEX = "ReportIndex";
	private static final String BOTTOM_BORDER = "BottomBorder";
	private static final String TOP_BORDER = "TopBorder";
	private static final String RIGHT_BORDER = "RightBorder";
	private static final String LEFT_BORDER = "LeftBorder";
	public static final String BACKGROUNDCOLOR = "backgroundcolor";
	public static final String FOREGROUNDCOLOR = "foregroundcolor";
	public static final String FONTSIZE = "fontsize";
	public static final String FONTSTYLE = "fontstyle";
	public static final String FONTFAMILY = "fontfamily";
	public static final String HEIGHT = "height";
	public static final String WIDTH = "width";
	public static final String Y = "y";
	public static final String X = "x";
	public static final PositionTypeEnum DEFAULT_POSITION_TYPE = PositionTypeEnum.FLOAT;
	public static final StretchTypeEnum DEFAULT_STRETCH_TYPE = StretchTypeEnum.RELATIVE_TO_TALLEST_OBJECT;
	public static final Color DEFAULT_FOREGROUND_COLOR = new Color(0x03, 0x3D, 0x3D);
	private static final Color DEFAULT_BORDER_COLOR = new Color(0x99,0x99,0x99);
	private static final String INCLUDE_IF_REPORT_INDEX = "includeIfReportIndex";
	private static final String INCLUDE_IF_EXPRESSION = "includeIfExpression";
	private static final String HYPERLINK_REPORT_INDEX = "hyperlinkReportIndex";
	private static final String HYPERLINK_EXPRESSION = "hyperlinkExpression";
	private static final String HYPERLINK_TARGET_SELF = "hyperlinkTargetSelf";
	private static final String SHOW_IN_REPORT = "showInReport";
	
	private static HashMap<AdhocReport.Band, String> bandNames;
	private static HashMap<Align, String> alignmentNames;
	private static HashMap<PositionTypeEnum, String> positionTypeNames;
	private static HashMap<StretchTypeEnum, String> stretchTypeNames;
	private PenEnum borderLeft = PenEnum.NONE;
	private PenEnum borderRight = PenEnum.NONE;
	private PenEnum borderTop = PenEnum.NONE;
	private PenEnum borderBottom = PenEnum.NONE;
	private Color foreground = DEFAULT_FOREGROUND_COLOR;
	private Color background = Color.WHITE;
	private Color borderColor = DEFAULT_BORDER_COLOR;
	private int width = PageInfo.DEFAULT_COLUMN_WIDTH;
	private int height = PageInfo.DEFAULT_ROW_HEIGHT;
	private int left = 0;
	private int top = 0;
	private AdhocReport.Band band = null;
    private int groupNumber;
    private transient String newBandName;
    protected int reportIndex = -1;
    private PositionTypeEnum positionType = DEFAULT_POSITION_TYPE; 
    private StretchTypeEnum stretchType = DEFAULT_STRETCH_TYPE;
    private String includeIfExpression;
    private int includeIfReportIndex = -1;
    private String hyperlinkExpression;
    private int hyperlinkReportIndex = -1;
    private boolean isHyperlinkSelfTarget = false;
	protected boolean showInReport = true;

	//Fonts
	private transient Font font;
	private String fontStr;
	public enum Align
	{
		Default, Left, Center, Right, Justified
	}
	private Align alignment;
	private Boolean transparent = Boolean.FALSE;
	
	static {
		bandNames = new HashMap<AdhocReport.Band, String>();
		bandNames.put(AdhocReport.Band.Detail, "Detail");
		bandNames.put(AdhocReport.Band.GroupFooter, "GroupFooter");
		bandNames.put(AdhocReport.Band.GroupHeader, "GroupHeader");
		bandNames.put(AdhocReport.Band.Header, "Header");
		bandNames.put(AdhocReport.Band.Summary, "Summary");
		bandNames.put(AdhocReport.Band.Title, "Title");
		bandNames.put(AdhocReport.Band.PageHeader, "Page Header");
		bandNames.put(AdhocReport.Band.PageFooter, "Page Footer");
		
		alignmentNames = new HashMap<Align, String>();
		alignmentNames.put(Align.Center, "Center");
		alignmentNames.put(Align.Default, "Default");
		alignmentNames.put(Align.Justified, "Justified");
		alignmentNames.put(Align.Left, LEFT);
		alignmentNames.put(Align.Right, "Right");
		
		positionTypeNames = new HashMap<PositionTypeEnum, String>();
		positionTypeNames.put(PositionTypeEnum.FIX_RELATIVE_TO_TOP, "Relative to Top");
		positionTypeNames.put(PositionTypeEnum.FIX_RELATIVE_TO_BOTTOM, "Relative to Bottom");
		positionTypeNames.put(PositionTypeEnum.FLOAT, "Float");
		
		stretchTypeNames = new HashMap<StretchTypeEnum, String>();
		stretchTypeNames.put(StretchTypeEnum.NO_STRETCH, "No Stretch");
		stretchTypeNames.put(StretchTypeEnum.RELATIVE_TO_BAND_HEIGHT, "Relative to Band Height");
		stretchTypeNames.put(StretchTypeEnum.RELATIVE_TO_TALLEST_OBJECT, "Relative to Tallest Object");
	}

	public AdhocEntity() {
	}
	
	@SuppressWarnings("unchecked")
	public void parseElement(OMElement parent) throws Exception {
		for (Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();
			if (!parseAttribute(el, elName))
			{
				/*
				String elstring = el.toString();
				if (elstring != null && !elstring.contains(":showPercentOfColumn") && !elstring.contains("Transparent")) // old report that has not yet been saved in 4.3+
					logger.info("Unknown element in entity xml: " + elstring + " on line: " + el.getLineNumber());
				 */
			}
		}
		postParseElement();
	}
	
	private void postParseElement() {
		if (transparent == null) {
			transparent = Boolean.FALSE;
		}
		
	}
	public void parseElement(XMLStreamReader parser) throws Exception {
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
			String elName = parser.getLocalName();
			if (!parseAttribute(parser, elName))
			{
				/*
				String elstring = elName;
				if (elstring != null && !elstring.contains(":showPercentOfColumn") && !elstring.contains("Transparent")) // old report that has not yet been saved in 4.3+
					logger.info("Unknown element in entity xml: " + elstring + " on line: " + parser.getLocation().getLineNumber());
				*/
				// do this to skip to next item in stream if necessary
				if (parser.getEventType() == XMLStreamConstants.START_ELEMENT)
					parser.next();
			}
		}
		postParseElement();
	}
	
	public boolean parseAttribute(XMLStreamReader el, String elName) throws Exception {
		if (LEFT_BORDER.equals(elName)) {
			this.setBorderLeft(BorderProperties.encodeBorderSize(XmlUtils.getStringContent(el)));
			return true;
		}
		else if (RIGHT_BORDER.equals(elName)) {
			this.setBorderRight(BorderProperties.encodeBorderSize(XmlUtils.getStringContent(el)));
			return true;
		}
		else if (TOP_BORDER.equals(elName)) {
			this.setBorderTop(BorderProperties.encodeBorderSize(XmlUtils.getStringContent(el)));
			return true;
		}
		else if (BOTTOM_BORDER.equals(elName)) {
			this.setBorderBottom(BorderProperties.encodeBorderSize(XmlUtils.getStringContent(el)));
			return true;
		}
		else if (REPORT_INDEX.equals(elName)) {
			this.reportIndex = XmlUtils.getIntContent(el);
			return true;
		}
		else if (TRANSPARENT.equals(elName)) {
			this.transparent = XmlUtils.getBooleanContent(el);
			return true;
		}
		else if (FOREGROUND_COLOR.equals(elName)) {
			this.setForeground(XmlUtils.getColorContent(el));
			return true;
		}
		else if (BACKGROUND_COLOR.equals(elName)) {
			this.setBackground(XmlUtils.getColorContent(el));
			return true;
		}
		else if (BORDER_COLOR.equals(elName)) {
			this.setBorderColor(XmlUtils.getColorContent(el));
			return true;
		}
		else if (ELEMENT_WIDTH.equals(elName)) {
			this.setWidth(XmlUtils.getIntContent(el));
			return true;
		}
		else if (ELEMENT_HEIGHT.equals(elName)) {
			this.setHeight(XmlUtils.getIntContent(el));
			return true;
		}
		else if (LEFT.equals(elName)) {
			this.setLeft(XmlUtils.getIntContent(el));
			return true;
		}
		else if (TOP.equals(elName)) {
			this.setTop(XmlUtils.getIntContent(el));
			return true;
		}
		else if (BAND.equals(elName)) {
			String bandName = XmlUtils.getStringContent(el);
			if (bandName != null) {
				for (Map.Entry<AdhocReport.Band,String> entry : bandNames.entrySet()) {
					if (entry.getValue().equals(bandName)) {
						this.setBand(entry.getKey());
					}
				}
			}
			return true;
		}
		else if (GROUP_NUMBER.equals(elName)) {
			this.setGroupNumber(XmlUtils.getIntContent(el));
			return true;
		}
		else if (FONT.equals(elName)) {
			this.setFontStr(XmlUtils.getStringContent(el));
			return true;
		}
		else if (ALIGNMENT.equals(elName)) {
			String alignment = XmlUtils.getStringContent(el);
			this.setAlignment(parseAlignment(alignment));
			return true;
		}
		else if (POSITION_TYPE.equals(elName)) {
			String positionTypeString = XmlUtils.getStringContent(el);
			if (positionTypeString != null) {
				for (Map.Entry<PositionTypeEnum, String> entry : positionTypeNames.entrySet()) {
					if (entry.getValue().equals(positionTypeString)) {
						this.setPositionType(entry.getKey());
						break;
					}
				}
			}
			return true;
		}
		else if (STRETCH_TYPE.equals(elName)) {
			String stretchTypeString = XmlUtils.getStringContent(el);
			if (stretchTypeString != null) {
				for (Entry<StretchTypeEnum, String> entry : stretchTypeNames.entrySet()) {
					if (entry.getValue().equals(stretchTypeString)) {
						this.setStretchType(entry.getKey());
						break;
					}
				}
			}
			return true;
		}
		else if (INCLUDE_IF_EXPRESSION.equals(elName)) {
			includeIfExpression = XmlUtils.getStringContent(el);
			return true;
		}
		else if (INCLUDE_IF_REPORT_INDEX.equals(elName)) {
			includeIfReportIndex = XmlUtils.getIntContent(el);
			return true;
		}
		else if (HYPERLINK_EXPRESSION.equals(elName)) {
			hyperlinkExpression = XmlUtils.getStringContent(el);
			return true;
		}
		else if (HYPERLINK_REPORT_INDEX.equals(elName)) {
			hyperlinkReportIndex = XmlUtils.getIntContent(el);
			return true;
		}else if (HYPERLINK_TARGET_SELF.equals(elName)){
			isHyperlinkSelfTarget = XmlUtils.getBooleanContent(el);
			return true;
		}
		else if (SHOW_IN_REPORT.equals(elName)) {
			showInReport = XmlUtils.getBooleanContent(el);
			return true;
		}
		return false;
	}
	
	public boolean parseAttribute(OMElement el, String elName) {
		if (LEFT_BORDER.equals(elName)) {
			this.setBorderLeft(BorderProperties.encodeBorderSize(XmlUtils.getStringContent(el)));
			return true;
		}
		else if (RIGHT_BORDER.equals(elName)) {
			this.setBorderRight(BorderProperties.encodeBorderSize(XmlUtils.getStringContent(el)));
			return true;
		}
		else if (TOP_BORDER.equals(elName)) {
			this.setBorderTop(BorderProperties.encodeBorderSize(XmlUtils.getStringContent(el)));
			return true;
		}
		else if (BOTTOM_BORDER.equals(elName)) {
			this.setBorderBottom(BorderProperties.encodeBorderSize(XmlUtils.getStringContent(el)));
			return true;
		}
		else if (REPORT_INDEX.equals(elName)) {
			this.reportIndex = XmlUtils.getIntContent(el);
			return true;
		}
		else if (TRANSPARENT.equals(elName)) {
			this.transparent = XmlUtils.getBooleanContent(el);
			return true;
		}
		else if (FOREGROUND_COLOR.equals(elName)) {
			this.setForeground(XmlUtils.getColorContent(el));
			return true;
		}
		else if (BACKGROUND_COLOR.equals(elName)) {
			this.setBackground(XmlUtils.getColorContent(el));
			return true;
		}
		else if (BORDER_COLOR.equals(elName)) {
			this.setBorderColor(XmlUtils.getColorContent(el));
			return true;
		}
		else if (ELEMENT_WIDTH.equals(elName)) {
			this.setWidth(XmlUtils.getIntContent(el));
			return true;
		}
		else if (ELEMENT_HEIGHT.equals(elName)) {
			this.setHeight(XmlUtils.getIntContent(el));
			return true;
		}
		else if (LEFT.equals(elName)) {
			this.setLeft(XmlUtils.getIntContent(el));
			return true;
		}
		else if (TOP.equals(elName)) {
			this.setTop(XmlUtils.getIntContent(el));
			return true;
		}
		else if (BAND.equals(elName)) {
			String bandName = XmlUtils.getStringContent(el);
			if (bandName != null) {
				for (Map.Entry<AdhocReport.Band,String> entry : bandNames.entrySet()) {
					if (entry.getValue().equals(bandName)) {
						this.setBand(entry.getKey());
					}
				}
			}
			return true;
		}
		else if (GROUP_NUMBER.equals(elName)) {
			this.setGroupNumber(XmlUtils.getIntContent(el));
			return true;
		}
		else if (FONT.equals(elName)) {
			this.setFontStr(XmlUtils.getStringContent(el));
			return true;
		}
		else if (ALIGNMENT.equals(elName)) {
			String alignment = XmlUtils.getStringContent(el);
			this.setAlignment(parseAlignment(alignment));
			return true;
		}
		else if (POSITION_TYPE.equals(elName)) {
			String positionTypeString = XmlUtils.getStringContent(el);
			if (positionTypeString != null) {
				for (Map.Entry<PositionTypeEnum, String> entry : positionTypeNames.entrySet()) {
					if (entry.getValue().equals(positionTypeString)) {
						this.setPositionType(entry.getKey());
						break;
					}
				}
			}
			return true;
		}
		else if (STRETCH_TYPE.equals(elName)) {
			String stretchTypeString = XmlUtils.getStringContent(el);
			if (stretchTypeString != null) {
				for (Entry<StretchTypeEnum, String> entry : stretchTypeNames.entrySet()) {
					if (entry.getValue().equals(stretchTypeString)) {
						this.setStretchType(entry.getKey());
						break;
					}
				}
			}
			return true;
		}
		else if (INCLUDE_IF_EXPRESSION.equals(elName)) {
			includeIfExpression = XmlUtils.getStringContent(el);
			return true;
		}
		else if (INCLUDE_IF_REPORT_INDEX.equals(elName)) {
			includeIfReportIndex = XmlUtils.getIntContent(el);
			return true;
		}
		else if (HYPERLINK_EXPRESSION.equals(elName)) {
			hyperlinkExpression = XmlUtils.getStringContent(el);
			return true;
		}
		else if (HYPERLINK_REPORT_INDEX.equals(elName)) {
			hyperlinkReportIndex = XmlUtils.getIntContent(el);
			return true;
		}else if (HYPERLINK_TARGET_SELF.equals(elName)){
			isHyperlinkSelfTarget = XmlUtils.getBooleanContent(el);
			return true;
		}
		else if (SHOW_IN_REPORT.equals(elName)) {
			showInReport = XmlUtils.getBooleanContent(el);
			return true;
		}
		return false;
	}
	
	public static Align parseAlignment(String alignment) {
		if (alignment != null) {
			for (Map.Entry<Align,String> entry : alignmentNames.entrySet()) {
				if (entry.getValue().equals(alignment)) {
					return entry.getKey();
				}
			}
		}
		return null;
	}
	
	public static String encodeAlignment(Align align) {
		return alignmentNames.get(align);
	}

	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, LEFT_BORDER, BorderProperties.decodeBorderSize(this.getBorderLeft()), ns);
		XmlUtils.addContent(fac, parent, RIGHT_BORDER, BorderProperties.decodeBorderSize(this.getBorderRight()), ns);
		XmlUtils.addContent(fac, parent, TOP_BORDER, BorderProperties.decodeBorderSize(this.getBorderTop()), ns);
		XmlUtils.addContent(fac, parent, BOTTOM_BORDER, BorderProperties.decodeBorderSize(this.getBorderBottom()), ns);
		XmlUtils.addContent(fac, parent, REPORT_INDEX, reportIndex, ns);
		XmlUtils.addContent(fac, parent, FOREGROUND_COLOR, this.getForeground(), ns);
		XmlUtils.addContent(fac, parent, TRANSPARENT, this.getTransparent(), ns);
		XmlUtils.addContent(fac, parent, BACKGROUND_COLOR, this.getBackground(), ns);
		XmlUtils.addContent(fac, parent, BORDER_COLOR, this.getBorderColor(), ns);
		XmlUtils.addContent(fac, parent, ELEMENT_WIDTH, Integer.toString(this.getWidth()), ns);
		XmlUtils.addContent(fac, parent, ELEMENT_HEIGHT, Integer.toString(this.getHeight()), ns);
		XmlUtils.addContent(fac, parent, LEFT, Integer.toString(this.getLeft()), ns);
		XmlUtils.addContent(fac, parent, TOP, Integer.toString(this.getTop()), ns);
		XmlUtils.addContent(fac, parent, BAND, bandNames.get(this.getBand()), ns);
		XmlUtils.addContent(fac, parent, GROUP_NUMBER, Integer.toString(this.getGroupNumber()), ns);
		XmlUtils.addContent(fac, parent, FONT, this.getFont(), ns);
		XmlUtils.addContent(fac, parent, ALIGNMENT, alignmentNames.get(this.getAlignment()), ns);
		XmlUtils.addContent(fac, parent, POSITION_TYPE, positionTypeNames.get(this.getPositionType()), ns);
		XmlUtils.addContent(fac, parent, STRETCH_TYPE, stretchTypeNames.get(this.getStretchType()), ns);
		XmlUtils.addContent(fac, parent, INCLUDE_IF_EXPRESSION, includeIfExpression, ns);
		XmlUtils.addContent(fac, parent, INCLUDE_IF_REPORT_INDEX, includeIfReportIndex, ns);
		XmlUtils.addContent(fac, parent, HYPERLINK_EXPRESSION, hyperlinkExpression, ns);
		XmlUtils.addContent(fac, parent, HYPERLINK_REPORT_INDEX, hyperlinkReportIndex, ns);
		XmlUtils.addContent(fac, parent, HYPERLINK_TARGET_SELF, isHyperlinkSelfTarget, ns);
		XmlUtils.addContent(fac, parent, SHOW_IN_REPORT, showInReport, ns);
	}
	
	public PenEnum getBorderLeft() {
		return borderLeft;
	}
	public void setBorderLeft(PenEnum borderLeft) {
		this.borderLeft = borderLeft;
	}
	public PenEnum getBorderRight() {
		return borderRight;
	}
	public void setBorderRight(PenEnum borderRight) {
		this.borderRight = borderRight;
	}
	public PenEnum getBorderTop() {
		return borderTop;
	}
	public void setBorderTop(PenEnum borderTop) {
		this.borderTop = borderTop;
	}
	public PenEnum getBorderBottom() {
		return borderBottom;
	}
	public void setBorderBottom(PenEnum borderBottom) {
		this.borderBottom = borderBottom;
	}
	public Color getForeground() {
		return foreground;
	}
	public void setForeground(Color foreground) {
		this.foreground = foreground;
	}
	public Color getBackground() {
		return background;
	}
	public void setBackground(Color background) {
		this.background = background;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getLeft() {
		return left;
	}
	public void setLeft(int left) {
		this.left = left;
	}
	public int getTop() {
		return top;
	}
	public void setTop(int top) {
		this.top = top;
	}
	public AdhocReport.Band getBand() {
		return band;
	}
	public void setBand(AdhocReport.Band bandName) {
		this.band = bandName;
	}
	public int getGroupNumber() {
		return groupNumber;
	}
	public void setGroupNumber(int group) {
		this.groupNumber = group;
	}
	public Font getFont() {
		if (font == null) {
			//create default font
			font = new Font(PageInfo.DEFAULT_FONT_NAME, PageInfo.DEFAULT_FONT_FACE, PageInfo.DEFAULT_FONT_SIZE);
		}
		return font;
	}
	public void setFont(Font font) {
		if (this.font == null || ! this.font.equals(font)) {
			this.font = font;
			if (font != null) {
				fontStr = XmlUtils.encodeFont(font);
			}
		}
	}
	public String getFontStr() {
		return fontStr;
	}
	public void setFontStr(String fontStr) {
		if (this.fontStr == null || ! this.fontStr.equals(fontStr)) {
			this.fontStr = fontStr;
			this.font = Font.decode(fontStr);
		}
	}
	public Align getAlignment() {
		return alignment;
	}
	public void setAlignment(Align alignment) {
		this.alignment = alignment;
	}

	/**
	 * Gets the relative pathname from an actual pathname based on user root directory structure
	 * @return the relative pathname
	 * @param filename the absolute path
	 * @return
	 */
	public String getRelativePathname(String filename) {
		if (filename == null) {
			return null;
		}
		File file = new File(filename);
		if (file.isAbsolute()) {
			filename = file.getAbsolutePath();
			Session session = Session.getCurrentThreadSession();
			String s = null;
			if (session != null) 
				s = session.getCatalogDir();
			
			if (s == null) {
		    	SMIWebUserBean userBean = SMIWebUserBean.getUserBean();
		    	CatalogManager manager = userBean.getCatalogManager();
	    		s = manager.getCatalogRootPath();
			}
    		if (s != null && filename.toUpperCase().startsWith(s.toUpperCase())) {
    			return filename.substring(s.length() + 1);
    		}
		}
		return filename;
	}
	
	public String resolveRelativePathname(String location) throws SwapSpaceInProgressException {
    	SMIWebUserBean userBean = SMIWebUserBean.getUserBean();
    	CatalogManager manager = userBean.getCatalogManager();
    	String fileName = "/" + location;
    	
    	// try absolute path
    	String s = manager.getCatalogRootPath();
		File file = new File(s + fileName);
		if (file.exists()) {
			return file.getAbsolutePath();
		}
		
		// didn't work, try old style relative to shared and private
    	List<File> roots = manager.getRootDirectories(userBean);
    	for (File s1 : roots) {
    		file = new File(s1.getAbsolutePath() + File.separator + fileName);
    		if (file.exists()) {
    			return file.getAbsolutePath();
    		}
    	}

    	return location;
	}
	
	public abstract JRDesignElement getElement(AdhocReport report, Repository r, JasperDesign jasperDesign, PageInfo pi, boolean isPdf, Map<String, ColumnSelector> selectors);
	
	protected void setDefaultEntityProperties(JRDesignElement element) {
		if (! this.getBackground().equals(Color.white)) {
			element.setBackcolor(this.getBackground());
		}
		element.setForecolor(this.getForeground());
		element.setHeight(this.getHeight());
		element.setWidth(this.getWidth());
		element.setX(this.getLeft());
		element.setY(this.getTop());
		element.setMode(this.transparent.equals(Boolean.TRUE) ? ModeEnum.TRANSPARENT : ModeEnum.OPAQUE);
		element.setStretchType(this.getStretchType());
		element.setPositionType(positionType);
	}

	public String getNewBandName() {
		return newBandName;
	}

	public void setNewBandName(String newBandName) {
		this.newBandName = newBandName;
	}

	public Color getBorderColor() {
		return borderColor;
	}

	public void setBorderColor(Color borderColor) {
		this.borderColor = borderColor;
	}
		
	public String getHtmlDisplayLabel() {
		return "Object";
	}
	
	public int getReportIndex() {
		return reportIndex;
	}

	public PositionTypeEnum getPositionType() {
		return positionType;
	}

	public void setPositionType(PositionTypeEnum positionType) {
		this.positionType = positionType;
	}
	public StretchTypeEnum getStretchType() {
		return stretchType;
	}
	public void setStretchType(StretchTypeEnum stretchType) {
		this.stretchType = stretchType;
	}
	
	public boolean overlaps(AdhocEntity other) {
		if (this.getBand() == other.getBand()) {
			return (overlapsX(other) && overlapsY(other));
		}
		return false;
	}
	private boolean overlapsX(AdhocEntity other) {
		int thisLeft = this.getLeft();
		int otherLeft = other.getLeft();
		return ((thisLeft >= otherLeft && thisLeft < otherLeft + other.getWidth()) ||
				(otherLeft >= thisLeft && otherLeft < thisLeft + this.getWidth()));
	}
	private boolean overlapsY(AdhocEntity other) {
		int thisTop = this.getTop();
		int otherTop = other.getTop();
		return ((thisTop >= otherTop && thisTop < other.getHeight() + otherTop) ||
				(otherTop >= thisTop && otherTop < this.getHeight() + thisTop));
	}
	
	public String getEntityXML() {
		StringBuilder xml = new StringBuilder("<element ID=\"");
		xml.append(this.getReportIndex());
		xml.append("\"><label>");
		xml.append(getHtmlDisplayLabel());
		xml.append("</label><" + X + ">");
		xml.append(this.getLeft());
		xml.append("</" + X + "><" + Y + ">");
		xml.append(this.getTop());
		xml.append("</" + Y + "><" + WIDTH + ">");
		xml.append(this.getWidth());
		xml.append("</" + WIDTH + "><" + HEIGHT + ">");
		xml.append(this.getHeight());
		xml.append("</" + HEIGHT + "><" + FONTFAMILY + ">");
		Font font = this.getFont();
		xml.append(font.getFamily());
		xml.append("</" + FONTFAMILY + "><" + FONTSTYLE + ">");
		switch (font.getStyle()) {
		case Font.PLAIN: xml.append("normal"); break;
		case Font.BOLD:  xml.append("bold");   break;
		case Font.ITALIC: xml.append("italic"); break;
		case Font.BOLD | Font.ITALIC: xml.append("bold-italic"); break;
		}

		xml.append("</" + FONTSTYLE + "><" + FONTSIZE + ">");
		xml.append(font.getSize());
		xml.append("</" + FONTSIZE + "><" + FOREGROUNDCOLOR + ">");
		xml.append(convertColorToString(this.getForeground()));
		xml.append("</" + FOREGROUNDCOLOR + "><" + BACKGROUNDCOLOR + ">");
		xml.append(convertColorToString(this.getBackground()));
		xml.append("</" + BACKGROUNDCOLOR + ">");
		this.addEntitySpecificXML(xml);
		xml.append("</element>");
		
		return xml.toString();
	}
	
	public void addEntitySpecificXML(StringBuilder xml) {
		xml.append("<type>");
		xml.append(this.getClass().getSimpleName());
		xml.append("</type>");
		
	}
	
	private String convertColorToString(Color color) {
		StringBuilder s = new StringBuilder();
		Formatter format = new Formatter(s);
		format.format("0x%02x%02x%02x", color.getRed(), color.getGreen(), color.getBlue());
		return s.toString();
	}
	public String getIncludeIfExpression() {
		return includeIfExpression;
	}
	public void setIncludeIfExpression(String includeIfExpression) {
		this.includeIfExpression = includeIfExpression;
	}
	public int getIncludeIfReportIndex() {
		return includeIfReportIndex;
	}
	public void setIncludeIfReportIndex(int includeIfReportIndex) {
		this.includeIfReportIndex = includeIfReportIndex;
	}
	public String getHyperlinkExpression() {
		return hyperlinkExpression;
	}
	public void setHyperlinkExpression(String hyperlinkExpression) {
		this.hyperlinkExpression = hyperlinkExpression;
	}
	public int getHyperlinkReportIndex() {
		return hyperlinkReportIndex;
	}
	public boolean isHyperlinkSelfTarget() {
		return isHyperlinkSelfTarget;
	}
	public void setHyperlinkSelftarget(boolean isSelf) {
		this.isHyperlinkSelfTarget = isSelf;
	}
	public void setHyperlinkReportIndex(int hyperlinkReportIndex) {
		this.hyperlinkReportIndex = hyperlinkReportIndex;
	}
	public Boolean getTransparent() {
		return transparent;
	}
	public void setTransparent(Boolean transparent) {
		this.transparent = transparent;
	}
	
	public void copyDisplayAttributes(AdhocEntity copyTo) {
		copyTo.borderLeft = borderLeft;
		copyTo.borderRight = borderLeft;
		copyTo.borderTop = borderTop;
		copyTo.borderBottom = borderBottom;
		copyTo.foreground = foreground;
		copyTo.background = background;
		copyTo.borderColor = borderColor;
		copyTo.positionType = positionType; 
		copyTo.stretchType = stretchType;
		copyTo.setFontStr(fontStr);
	}

	public boolean isShowInReport() {
		return showInReport;
	}

	public void setShowInReport(boolean showInReport) {
		this.showInReport = showInReport;
	}

	public void applyStyle(AdhocEntity style) {
		borderLeft = style.borderLeft;
		borderRight = style.borderRight;
		borderTop = style.borderTop;
		borderBottom = style.borderBottom;
		foreground = style.foreground;
		background = style.background;
		borderColor = style.borderColor;
	    positionType = style.positionType; 
	    stretchType = style.stretchType;
		this.setFontStr(style.fontStr);
		alignment = style.alignment;
		transparent = style.transparent;
	}

}
