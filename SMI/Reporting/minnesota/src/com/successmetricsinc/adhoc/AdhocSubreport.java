/**
 * $Id: AdhocSubreport.java,v 1.67 2012-05-22 00:19:30 agarrison Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */


package com.successmetricsinc.adhoc;

import java.awt.Color;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.stream.XMLStreamReader;

import net.sf.jasperreports.engine.JRBand;
import net.sf.jasperreports.engine.JRDataset;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.JRGroup;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRQuery;
import net.sf.jasperreports.engine.JRSection;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignElement;
import net.sf.jasperreports.engine.design.JRDesignExpression;
import net.sf.jasperreports.engine.design.JRDesignParameter;
import net.sf.jasperreports.engine.design.JRDesignSubreport;
import net.sf.jasperreports.engine.design.JRDesignSubreportParameter;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.type.StretchTypeEnum;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.ServerContext;
import com.successmetricsinc.Session;
import com.successmetricsinc.UserBean;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.adhoc.AdhocReport.Band;
import com.successmetricsinc.catalog.CatalogManager;
import com.successmetricsinc.chart.ChartOptions;
import com.successmetricsinc.query.DisplayFilter;
import com.successmetricsinc.query.QueryFilter;
import com.successmetricsinc.query.TopFilter;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XmlUtils;


/**
 * @author agarrison
 *
 */
public class AdhocSubreport extends AdhocEntity {

	private static final long serialVersionUID = 1L;
	private static final String SUBREPORT = "Subreport";
	private static final String PASS_VALUE_FROM_PARENT = "PassValueFromParent";

	private static Logger logger = Logger.getLogger(AdhocSubreport.class);

	private String subreport;
	private transient JRParameter[] parameters;
	private transient JRQuery jrQuery;
	private transient List<AdhocColumnSelector> columnSelectors;
	private boolean passValueFromParent = true;
	
	public AdhocSubreport() {
		this.setStretchType(StretchTypeEnum.NO_STRETCH);
	}

	public String getSubreport() {
		if (subreport != null) {
			return subreport.replaceAll("\\\\", "/");
		}
		return null;
	}

	private void setSubreport(String subreport) {
		this.subreport = getRelativePathname(subreport);
		try {
			String catalogPath = null;
			Repository rep = null;
			SMIWebUserBean userBean = SMIWebUserBean.getUserBean();
			if (userBean != null) {
				catalogPath = userBean.getCatalogPath();
				rep = userBean.getRepository();
			}
			else {
				UserBean ub = UserBean.getParentThreadUserBean();
				rep = ub.getRepository();
				catalogPath = rep.getServerParameters().getApplicationPath() + "\\catalog";
			}
			String normalizedPath = CatalogManager.getNormalizedPath(subreport, catalogPath);
			JasperReport report = ServerContext.getReport(normalizedPath, rep, null, true, true, 
					Session.getDashletPrompts(), null, true);

			this.setWidth(report.getPageWidth());
			this.setHeight(getSubreportHeight(report));
			JRDataset dataset = report.getMainDataset();
			if (dataset != null) {
				jrQuery = dataset.getQuery();
			}
			parameters =  report.getParameters();
		} catch (Exception e) {
			logger.error("Error setting subreport object", e);
		}
	}
	
	private int getSubreportHeight(JasperReport report) {
		int height = report.getTopMargin() + report.getBottomMargin();
		
		JRBand band = report.getColumnHeader();
		height += getBandHeight(band);
		
		band = report.getPageHeader();
		height += getBandHeight(band);
		
		JRSection section = report.getDetailSection();
		if (section != null) {
			JRBand[] bands = section.getBands();
			if (bands != null) {
				for (int i = 0; i < bands.length; i++)
				height += getBandHeight(bands[i]);
			}
		}
		
		band = report.getPageFooter();
		height += getBandHeight(band);

		band = report.getSummary();
		height += getBandHeight(band);
		
		band = report.getTitle();
		height += getBandHeight(band);
		
		JRGroup[] groups = report.getGroups();
		if (groups != null) {
			for (JRGroup group : groups) {
				section = group.getGroupHeaderSection();
				if (section != null) {
					JRBand[] bands = section.getBands();
					if (bands != null) {
						for (int i = 0; i < bands.length; i++)
							height += getBandHeight(bands[i]);
					}
				}
				
				section = group.getGroupFooterSection();
				if (section != null) {
					JRBand[] bands = section.getBands();
					if (bands != null) {
						for (int i = 0; i < bands.length; i++)
							height += getBandHeight(bands[i]);
					}
				}
			}
		}
		
		if (height > report.getPageHeight()) {
			height = report.getPageHeight();
		}
		return height < PageInfo.DEFAULT_ROW_HEIGHT ? PageInfo.DEFAULT_ROW_HEIGHT : height;
	}
	
	private int getBandHeight(JRBand band) {
		return band == null ? 0 : band.getHeight();
	}
	
	@Override
	public JRDesignElement getElement(AdhocReport report, Repository r, JasperDesign jasperDesign, PageInfo pi, boolean isPdf, Map<String, ColumnSelector> selectors) {
		JRDesignSubreport element = new JRDesignSubreport(jasperDesign);
		this.setDefaultEntityProperties(element);
		element.setPrintWhenDetailOverflows(true);
		element.setBackcolor(Color.white);
	
		String name = this.subreport;
		if (name != null)
			name = name.replace("\\", "/");
		JRDesignExpression expression = new JRDesignExpression();
		expression.setText("ServerContext.getReport(\"" + name + "\")");
		element.setExpression(expression);
		
		// add top filter parameter
		TopFilter topFilter = report.getTopFilter();
		if (topFilter != null && topFilter.isPrompted()) {
			JRDesignParameter param = new JRDesignParameter();
			param.setName(topFilter.getPromptName());
			param.setValueClass(String.class);
			try {
				jasperDesign.addParameter(param);
			} catch (JRException e) {
				logger.error("Error adding prompted filter parameter.", e);
			}
		}
		
		for (QueryFilter filter : report.getFilters()) {
			if (filter.isPrompted()) {
				// add this as a parameter
				AdhocColumn column = report.getColumnByName(filter.getColumn());
				if (designContainsParam(jasperDesign, filter.getPromptName()) || 
						! columnBandContainsMe(column)) {
					continue;
				}
				JRDesignParameter param = new JRDesignParameter();
				param.setName(filter.getPromptName());
				param.setValueClass(column == null ? String.class : column.getFieldClass(r, selectors));
				JRDesignExpression expr = new JRDesignExpression();
				expr.setText("\""+filter.getOperand()+"\"");
				param.setDefaultValueExpression(expr);
				try {
					jasperDesign.addParameter(param);
				} catch (JRException e) {
					logger.error("Error adding prompted filter parameter.", e);
				}
			}
		}
		for (DisplayFilter filter : report.getDisplayFilters()) {
			if (filter.isPrompted()) {
				// add this as a parameter
				AdhocColumn column = report.getColumnByName(filter.getColumn());
				if (designContainsParam(jasperDesign, filter.getPromptName()) ||
						! columnBandContainsMe(column)) {
					continue;
				}
				JRDesignParameter param = new JRDesignParameter();
				param.setName(filter.getPromptName());
				param.setValueClass(column == null ? String.class : column.getFieldClass(r, selectors));
				JRDesignExpression expr = new JRDesignExpression();
				expr.setText("\""+filter.getOperand()+"\"");
				param.setDefaultValueExpression(expr);
				try {
					jasperDesign.addParameter(param);
				} catch (JRException e) {
					logger.error("Error adding prompted display filter parameter.", e);
				}
			}
		}
		String queryText = null;
		if (jrQuery != null) {
			queryText = jrQuery.getText();
		}
		if (queryText == null || !Util.hasNonWhiteSpaceCharacters(queryText)) {
			queryText = "SingleRow{}";
		}
		
		HashMap<String, AdhocColumn> columnNames = new HashMap<String, AdhocColumn>();
		for (AdhocColumn column : report.getColumns()) {
			columnNames.put(column.getDefaultPromptName(), column);
		}
		if (parameters != null) {
			for (JRParameter param : parameters) {
				if (param.isSystemDefined()) {
					continue;
				}
				String key = param.getName();
				// TODO: this won't work if we need to allow the chart type selector to change subreport charts
				if (ChartOptions.CHART_TYPE.equals(key)) {
					continue;
				}
				boolean paramAdded = false;
				// add subreport parameters to parent report
				// unless the subreport param is a field in this report
				if (! this.designContainsParam(jasperDesign, key) &&
						! columnNames.containsKey(key)) {
					try {
						jasperDesign.addParameter((JRParameter) param.clone());
						paramAdded = true;
					} catch (JRException e) {
						// TODO Auto-generated catch block
						logger.error("Error adding subreport's parameter to report.", e);
					}
				}
				
				// pass parameter from parent to subreport
				if (this.passValueFromParent == false && 
						paramAdded == false && 
						!this.designContainsParam(jasperDesign, key)) {
					// change the query to use NO_FILTER instead of parameter if used
					String pName = "$P{" + key + "}";
					String fName = "\"NO_FILTER\"";
					queryText = queryText.replace(pName, fName);
					continue;
				}
				JRDesignSubreportParameter subreportParameter = new JRDesignSubreportParameter();
				subreportParameter.setName(key);
				JRDesignExpression parameterEx = new JRDesignExpression();
				if (this.passValueFromParent && columnNames.containsKey(key)) {
					AdhocColumn column = columnNames.get(key);
					if (! columnBandContainsMe(column)) {
						// change the query to use NO_FILTER instead of parameter if used
						String pName = "$P{" + key + "}";
						String fName = "\"NO_FILTER\"";
						queryText = queryText.replace(pName, fName);
						continue;
					}
					
					JRField fld = (JRField) jasperDesign.getMainDesignDataset().getFieldsMap().get(column.getFieldName());
					String fName = "$F{" + column.getFieldName() + "}";
					if (fld.getValueClass().equals(GregorianCalendar.class)) {
						fName = "com.successmetricsinc.util.DateUtil.convertToDefaultFormat($F{" + column.getFieldName() + "}, $P{REPORT_TIME_ZONE})";
						parameterEx.setText(fName);
					}
					else if (fld.getValueClass().equals(String.class)) {
						fName = "com.successmetricsinc.util.Util.quote($F{" + column.getFieldName() + "}.toString())";
						parameterEx.setText(fName);
					}
					else
					{
						parameterEx.setText("$F{" + column.getFieldName() + "}.toString()");
					}
					
					// change the query to use this field instead of parameter if used (both old and new query language forms)
					String pName = "$P{" + key + "}";
					queryText = queryText.replace(pName, fName);
					pName = "GETPROMPTVALUE('" + key + "')";
					if (fld.getValueClass().equals(String.class))
					{
						fName = "'\" + " + fName + " + \"'";
					}
					else if (fld.getValueClass().equals(GregorianCalendar.class))
					{
						fName = "#\" + " + fName + " + \"#";
					}
					else
					{
						fName = "\" + " + fName + " + \"";
					}
					queryText = queryText.replace(pName, fName);
				}
				else {
					parameterEx.setText("$P{" + key + "}");
				}
				subreportParameter.setExpression(parameterEx);
				try {
					element.addParameter(subreportParameter);
				} catch (JRException e) {
					logger.error("Error adding subreport parameter.", e);
				}
			}
		}
		
		JRDesignExpression dsExpression = new JRDesignExpression();
		String query = "((IJasperDataSourceProvider)$P{REPORT_DATA_SOURCE}).getJasperDataSourceProvider().create(\"" + queryText + "\")";

		dsExpression.setText(query);
		element.setDataSourceExpression(dsExpression);
		
		return element;
	}
	
	private boolean columnBandContainsMe(AdhocColumn column) {
		if (column == null)
			return false;
		
		Band colBand = column.getBand();
		if (colBand == Band.Title || colBand == Band.Summary)
			return true;
		
		if (colBand == Band.GroupHeader || colBand == Band.GroupFooter) {
			if ((this.getBand() == Band.GroupHeader || this.getBand() == Band.GroupFooter)
					&& this.getGroupNumber() >= column.getGroupNumber())
				return true;
			else if (this.getBand() == Band.Detail)
				return true;
		}
		
		if (colBand == Band.Detail && this.getBand() == Band.Detail)
			return true;
		
		return false;
	}

	public boolean parseAttribute(OMElement el, String elName) {
		if (SUBREPORT.equals(elName)) {
			setSubreport(XmlUtils.getStringContent(el));
			return true;
		}
		else if (PASS_VALUE_FROM_PARENT.equals(elName)) {
			passValueFromParent = XmlUtils.getBooleanContent(el);
			return true;
		}
		return super.parseAttribute(el, elName);
	}
	public boolean parseAttribute(XMLStreamReader el, String elName) throws Exception {
		if (SUBREPORT.equals(elName)) {
			setSubreport(XmlUtils.getStringContent(el));
			return true;
		}
		else if (PASS_VALUE_FROM_PARENT.equals(elName)) {
			passValueFromParent = XmlUtils.getBooleanContent(el);
			return true;
		}
		return super.parseAttribute(el, elName);
	}
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		super.addContent(fac, parent, ns);
		XmlUtils.addContent(fac, parent, SUBREPORT, this.getSubreport(), ns);
		XmlUtils.addContent(fac, parent, PASS_VALUE_FROM_PARENT, this.passValueFromParent, ns);
	}

	public String getHtmlDisplayLabel() {
		if (subreport != null) {
			return "s: " + this.getRelativePathname(subreport);
		}
		return SUBREPORT;
	}
	
	private boolean designContainsParam(JasperDesign jasperDesign, String fieldName) {
		return jasperDesign.getParametersMap().containsKey(fieldName);
	}

	public List<AdhocColumnSelector> getColumnSelectors() {
		return columnSelectors;
	}
}
