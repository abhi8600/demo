/**
 * 
 */
package com.successmetricsinc.adhoc;

import com.successmetricsinc.queueing.Message;
import com.successmetricsinc.queuing.AsynchReportGeneration.AsynchReportMessage;

/**
 * @author agarrison
 *
 */
public class AsynchReportGenerationException extends Exception {

	private static final long serialVersionUID = 1L;
	public enum Type { AlreadyRunning, NotRunning };
	
	private Type type;
	private AsynchReportMessage asynchMessage;
	private String id;
	
	public AsynchReportGenerationException(AsynchReportMessage msg, String errorMessage) {
		super(errorMessage);
		this.type = Type.NotRunning;
		this.asynchMessage = msg;
	}
	
	public AsynchReportGenerationException(String id, String errorMessage) {
		super(errorMessage);
		this.type = Type.AlreadyRunning;
		this.id = id;
	}

	public AsynchReportMessage getAsynchMessage() {
		return asynchMessage;
	}

	public Type getType() {
		return type;
	}

	public String getId() {
		return id;
	}
}
