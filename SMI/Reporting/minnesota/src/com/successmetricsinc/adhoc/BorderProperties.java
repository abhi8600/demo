/**
 * $Id: BorderProperties.java,v 1.3 2011-01-14 21:10:07 agarrison Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.adhoc;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.base.JRBoxPen;
import net.sf.jasperreports.engine.type.PenEnum;
import net.sf.jasperreports.engine.util.JRPenUtil;

public class BorderProperties
{
	private static final String BORDER_SIZE_NONE = "None";
	private static final String BORDER_SIZE_THIN = "Thin";	
	private static final String BORDER_SIZE_1PT = "1Point";	
	private static final String BORDER_SIZE_2PT = "2Point";
	private static final String BORDER_SIZE_4PT = "4Point";	
	private static final String BORDER_SIZE_DOTTED = "Dotted";	

	private static Map<String, PenEnum> encodeMap = null;
	private static Map<PenEnum, String> decodeMap = null;	
	
	public BorderProperties()
	{
	}

	public static String decodeBorderSize(PenEnum borderSize)
    {
    	String decodedStr;
    	decodedStr = getDecodeMap().get(borderSize);
    	if(decodedStr == null)
    	{
    		decodedStr = BORDER_SIZE_NONE;
    	}
    	return decodedStr;
    }

    public static PenEnum encodeBorderSize(String borderSizeStr)
    {
    	return getEncodeMap().get(borderSizeStr);
    }
 
	public static void setPenAttributes(JRBoxPen pen, PenEnum border, Color penColor) {
		JRPenUtil.setLinePenFromPen(border, pen);
		pen.setLineColor(penColor);
	}
	
    private static Map<String, PenEnum> getEncodeMap()
    {
    	if(encodeMap == null)
    	{
    		// should always create a new object and populate it, then assign to the static, that way you won't get partial reads
    		Map<String, PenEnum> tempMap = new HashMap<String, PenEnum>();
    		tempMap.put(BORDER_SIZE_NONE, PenEnum.NONE);
    		tempMap.put(BORDER_SIZE_THIN, PenEnum.THIN);
    		tempMap.put(BORDER_SIZE_1PT, PenEnum.ONE_POINT);
    		tempMap.put(BORDER_SIZE_2PT, PenEnum.TWO_POINT);
    		tempMap.put(BORDER_SIZE_4PT, PenEnum.FOUR_POINT);
    		tempMap.put(BORDER_SIZE_DOTTED, PenEnum.DOTTED);   
    		encodeMap = tempMap;
    	}
    	return encodeMap;
    }

    private static Map<PenEnum, String> getDecodeMap()
    {
    	if(decodeMap == null)
    	{
    		// should always create a new object and populate it, then assign to the static, that way you won't get partial reads
    		Map<PenEnum, String> tempMap = new HashMap<PenEnum, String>();
    		tempMap.put(PenEnum.NONE, BORDER_SIZE_NONE);
    		tempMap.put(PenEnum.THIN, BORDER_SIZE_THIN);
    		tempMap.put(PenEnum.ONE_POINT, BORDER_SIZE_1PT);
    		tempMap.put(PenEnum.TWO_POINT, BORDER_SIZE_2PT);
    		tempMap.put(PenEnum.FOUR_POINT, BORDER_SIZE_4PT);
    		tempMap.put(PenEnum.DOTTED, BORDER_SIZE_DOTTED);
    		decodeMap = tempMap;
    	}
    	return decodeMap;
    }
    
}
