/**
 * 
 */
package com.successmetricsinc.adhoc;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.successmetricsinc.Repository;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class Drill {

	private static final String DRILL_COLUMN = "DrillColumn";
	private static final String DRILL_COLUMNS = "DrillColumns";
	private static final String FROM_COLUMN = "FromColumn";
	private static final String FROM_DIMENSION = "FromDimension";
	private String fromDimension;
	private String fromColumn;
	private List<DrillColumn> drillColumns;
	
	public Drill() {
		
	}
	
	public Drill(AdhocColumn ac, List<DrillColumnList> list) {
		fromDimension = ac.getDimension();
		fromColumn = ac.getColumn();
		if (list != null && list.size() > 0) {
			DrillColumnList dc = list.get(0);
			drillColumns = dc.getDrillColumns();
		}
	}
	
	public Drill(AdhocColumn ac, Repository r) {
		fromDimension = ac.getDimension();
		fromColumn = ac.getColumn();
		List<DrillColumnList> dcs = ac.retrieveAllDrillToColumns(r);
		if (dcs != null && dcs.size() > 0) {
			DrillColumnList dc = dcs.get(0);
			drillColumns = dc.getDrillColumns();
		}
	}
	public String getFromDimension() {
		return fromDimension;
	}
	public void setFromDimension(String fromDimension) {
		this.fromDimension = fromDimension;
	}
	public String getFromColumn() {
		return fromColumn;
	}
	public void setFromColumn(String fromColumn) {
		this.fromColumn = fromColumn;
	}
	public List<DrillColumn> getDrillColumns() {
		return drillColumns;
	}
	public void setDrillColumns(List<DrillColumn> drillColumns) {
		this.drillColumns = drillColumns;
	}
	
	public void parseElement(OMElement parent) throws Exception {
		for (@SuppressWarnings("unchecked")
		Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String name = el.getLocalName();
			if (FROM_DIMENSION.equals(name)) {
				fromDimension = XmlUtils.getStringContent(el);
			}
			if (FROM_COLUMN.equals(name)) {
				fromColumn = XmlUtils.getStringContent(el);
			}
			else if (DRILL_COLUMNS.equals(name)) {
				parseDrillColumns(el);
			}
		}
		
	}
	protected void parseDrillColumns(OMElement element) throws Exception {
		drillColumns = new ArrayList<DrillColumn>();
		for (@SuppressWarnings("unchecked")
		Iterator<OMElement> iter = element.getChildElements(); iter.hasNext(); ) {
			DrillColumn dc = new DrillColumn();
			dc.parseElement(iter.next());
			drillColumns.add(dc);
		}
	}
	
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, FROM_DIMENSION, fromDimension, ns);
		XmlUtils.addContent(fac, parent, FROM_COLUMN, fromColumn, ns);
		if (drillColumns != null) {
			OMElement type = fac.createOMElement(DRILL_COLUMNS, ns);
			
			for (DrillColumn dc : drillColumns) {
				OMElement drillCol = fac.createOMElement(DRILL_COLUMN, ns);
				dc.addContent(fac, drillCol, ns);
				type.addChild(drillCol);
			}
			parent.addChild(type);
		}
	}
	
	public String toString() {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;
		OMElement parent = fac.createOMElement("DrillTo", ns);
		addContent(fac, parent, ns);
		return parent.toString();
	}
}
