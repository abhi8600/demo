/**
 * 
 */
package com.successmetricsinc.chart.AnyChart;

import java.util.Iterator;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.successmetricsinc.chart.ChartOptions;
import com.successmetricsinc.util.BirstXMLSerializable;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class Controls extends BirstXMLSerializable {

	private static final long serialVersionUID = 1L;
	private static final String COLOR_SWATCH = "ColorSwatch";

	private ColorSwatch colorSwatch;
	
	public Controls() {
		
	}
	
	public Controls(ChartOptions co) {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, COLOR_SWATCH, colorSwatch, ns);
	}

	@Override
	public BirstXMLSerializable createNew() {
		return new Controls();
	}

	@Override
	public void parseElement(OMElement parent) {
		for (@SuppressWarnings("unchecked")
		Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();

			if (COLOR_SWATCH.equals(elName)) {
				colorSwatch = (ColorSwatch) XmlUtils.parseFirstChild(el, new ColorSwatch());
			}
		}
	}

	@Override
	public void parseElement(XMLStreamReader parser) throws Exception {
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
			String elName = parser.getLocalName();

			if (COLOR_SWATCH.equals(elName)) {
				colorSwatch = (ColorSwatch) XmlUtils.parseFirstChild(parser, new ColorSwatch());
			}
		}
	}

	public void applyStyle(Controls style) {
		if (style == null || colorSwatch == null)
			return;
		
		colorSwatch.applyStyle(style.colorSwatch);
	}
}
