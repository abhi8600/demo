/**
 * 
 */
package com.successmetricsinc.chart.AnyChart;

import java.util.Iterator;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.successmetricsinc.chart.ChartOptions;
import com.successmetricsinc.util.BirstXMLSerializable;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class GaugeSeries extends BirstXMLSerializable {

	private static final String NUMBER_OF_MINOR_INTERVALS = "NumberOfMinorIntervals";
	private static final String NUMBER_OF_MAJOR_INTERVALS = "NumberOfMajorIntervals";
	private final static int DEFAULT_NUMBER_INTERVALS = 10;
	
	private static final long serialVersionUID = 1L;
	private static final String MAXIMUM = "Maximum";
	private static final String NORMAL = "Normal";
	private static final String CRITICAL = "Critical";
	private static final String MINIMUM = "Minimum";
	private double minimum;
	private double critical;
	private double normal;
	private double maximum;
	private int numMajorIntervals = DEFAULT_NUMBER_INTERVALS;
	private int numMinorIntervals = DEFAULT_NUMBER_INTERVALS;
	
	public GaugeSeries() {
		
	}
	
	public GaugeSeries(ChartOptions co, OMElement defaultSettings) {
		parseElement(defaultSettings);
		minimum = co.meterMin;
		critical = co.meterCritical;
		normal = co.meterNormal;
		maximum = co.meterMax;
	}

	public GaugeSeries(OMElement defaultSettings) {
		parseElement(defaultSettings);
	}

	/* (non-Javadoc)
	 * @see com.successmetricsinc.util.BirstXMLSerializable#addContent(org.apache.axiom.om.OMFactory, org.apache.axiom.om.OMElement, org.apache.axiom.om.OMNamespace)
	 */
	@Override
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, MINIMUM, minimum, ns);
		XmlUtils.addContent(fac, parent, CRITICAL, critical, ns);
		XmlUtils.addContent(fac, parent, NORMAL, normal, ns);
		XmlUtils.addContent(fac, parent, MAXIMUM, maximum, ns);
		XmlUtils.addContent(fac, parent, NUMBER_OF_MAJOR_INTERVALS, numMajorIntervals, ns);
		XmlUtils.addContent(fac, parent, NUMBER_OF_MINOR_INTERVALS, numMinorIntervals, ns);
	}

	/* (non-Javadoc)
	 * @see com.successmetricsinc.util.BirstXMLSerializable#createNew()
	 */
	@Override
	public BirstXMLSerializable createNew() {
		// TODO Auto-generated method stub
		return new GaugeSeries();
	}

	/* (non-Javadoc)
	 * @see com.successmetricsinc.util.BirstXMLSerializable#parseElement(org.apache.axiom.om.OMElement)
	 */
	@Override
	public void parseElement(OMElement parent) {
		if (parent == null)
			return;
		for (@SuppressWarnings("unchecked")
		Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();
			
			if (MINIMUM.equals(elName))
				minimum = XmlUtils.getDoubleContent(el);
			else if (CRITICAL.equals(elName))
				critical = XmlUtils.getDoubleContent(el);
			else if (NORMAL.equals(elName))
				normal = XmlUtils.getDoubleContent(el);
			else if (MAXIMUM.equals(elName))
				maximum = XmlUtils.getDoubleContent(el);
			else if (NUMBER_OF_MAJOR_INTERVALS.equals(elName))
				numMajorIntervals = XmlUtils.getIntContent(el);
			else if (NUMBER_OF_MINOR_INTERVALS.equals(elName))
				numMinorIntervals = XmlUtils.getIntContent(el);
		}
	}

	@Override
	public void parseElement(XMLStreamReader el) throws Exception {
		for (XMLStreamIterator iter = new XMLStreamIterator(el); iter.hasNext(); ) {
			String elName = el.getLocalName();
			
			if (MINIMUM.equals(elName))
				minimum = XmlUtils.getDoubleContent(el);
			else if (CRITICAL.equals(elName))
				critical = XmlUtils.getDoubleContent(el);
			else if (NORMAL.equals(elName))
				normal = XmlUtils.getDoubleContent(el);
			else if (MAXIMUM.equals(elName))
				maximum = XmlUtils.getDoubleContent(el);
			else if (NUMBER_OF_MAJOR_INTERVALS.equals(elName))
				numMajorIntervals = XmlUtils.getIntContent(el);
			else if (NUMBER_OF_MINOR_INTERVALS.equals(elName))
				numMinorIntervals = XmlUtils.getIntContent(el);
		}
	}
	/**
	 * @return the minimum
	 */
	public double getMinimum() {
		return minimum;
	}

	/**
	 * @return the critical
	 */
	public double getCritical() {
		return critical;
	}

	/**
	 * @return the normal
	 */
	public double getNormal() {
		return normal;
	}

	/**
	 * @return the maximum
	 */
	public double getMaximum() {
		return maximum;
	}

	public int getNumMajorIntervals() {
		return numMajorIntervals < 0 ? 0 : numMajorIntervals;
	}

	public void setNumMajorIntervals(int numMajorIntervals) {
		this.numMajorIntervals = numMajorIntervals;
	}

	public int getNumMinorIntervals() {
		return numMinorIntervals < 0 ? 0 : numMinorIntervals;
	}

	public void setNumMinorIntervals(int numMinorIntervals) {
		this.numMinorIntervals = numMinorIntervals;
	}

	public void applyStyle(GaugeSeries style) {
		if (style == null)
			return;
		
		numMajorIntervals = style.numMajorIntervals;
		numMinorIntervals = style.numMinorIntervals;
	}


}
