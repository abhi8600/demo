package com.successmetricsinc.chart.AnyChart;

import java.util.Iterator;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.successmetricsinc.chart.ChartOptions;
import com.successmetricsinc.util.BirstXMLSerializable;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

public class RangeBarSeries extends BirstXMLSerializable {

	private static final long serialVersionUID = 1L;
	private static final String START_POINT_DISPLAY = "StartPointDisplay";
	private static final String START_POINT_ANCHOR = "StartPointAnchor";
	private static final String START_POINT_HALIGN = "StartPointHAlign";
	private static final String START_POINT_VALIGN = "StartPointVAlign";
	private static final String START_POINT_PADDING = "StartPointPadding";
	private static final String START_POINT_FORMAT = "StartPointFormat";
	
	private static final String END_POINT_DISPLAY = "EndPointDisplay";
	private static final String END_POINT_ANCHOR = "EndPointAnchor";
	private static final String END_POINT_HALIGN = "EndPointHAlign";
	private static final String END_POINT_VALIGN = "EndPointVAlign";
	private static final String END_POINT_PADDING = "EndPointPadding";	
	private static final String END_POINT_FORMAT = "EndPointFormat";
	
	private static final String SHOW_BAR_BORDERS = "ShowBarBorders";
	private static final String BAR_SHAPE_TYPE = "BarShapeType";
	private static final String POINT_PADDING = "PointPadding";
	private static final String GROUP_PADDING = "GroupPadding";
	
	private boolean startPointDisplay = false;
	private String startPointAnchor = "CenterBottom";
	private String startPointHAlign = "Center";
	private String startPointVAlign = "Bottom";
	private String startPointPadding = null;
	private String startPointFormat = null;
	
	private boolean endPointDisplay = false;
	private String endPointAnchor = "CenterTop";
	private String endPointHAlign = "Center";
	private String endPointVAlign = "Top";
	private String endPointPadding = null;
	private String endPointFormat = null;
	
	private static String DEFAULT_SHAPE_TYPE = "Box"; // Box, Cylinder
	
	// group_padding=
	protected double groupPadding = 0.75;
	
	// point_padding=
	protected double pointPadding = 0.25;
		
	protected String shapeType = DEFAULT_SHAPE_TYPE;
	
	protected boolean showBarBorder = false;
	
	public RangeBarSeries() {
	}
	
	public RangeBarSeries(ChartOptions co, OMElement defaultSettings) {
		parseElement(defaultSettings);
	}

	public RangeBarSeries(OMElement defaultSettings) {
		parseElement(defaultSettings);
	}
	
	public void parseElement(OMElement parent) {
		if (parent == null)
			return;
		for (@SuppressWarnings("unchecked")
		Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();
			
			if (START_POINT_DISPLAY.equals(elName)) {
				startPointDisplay = XmlUtils.getBooleanContent(el);
			}
			else if (START_POINT_ANCHOR.equals(elName)) {
				startPointAnchor = XmlUtils.getStringContent(el);
			}
			else if (START_POINT_VALIGN.equals(elName)) {
				startPointVAlign = XmlUtils.getStringContent(el);
			}
			else if (START_POINT_HALIGN.equals(elName)) {
				startPointHAlign = XmlUtils.getStringContent(el);
			}
			else if (START_POINT_FORMAT.equals(elName)) {
				startPointFormat = XmlUtils.getStringContent(el);
			}
			else if (START_POINT_PADDING.equals(elName)) {
				startPointPadding = XmlUtils.getStringContent(el);
			}			
			else if (END_POINT_DISPLAY.equals(elName)) {
				endPointDisplay = XmlUtils.getBooleanContent(el);
			}
			else if (END_POINT_ANCHOR.equals(elName)) {
				endPointAnchor = XmlUtils.getStringContent(el);
			}
			else if (END_POINT_VALIGN.equals(elName)) {
				endPointVAlign = XmlUtils.getStringContent(el);
			}
			else if (END_POINT_HALIGN.equals(elName)) {
				endPointHAlign = XmlUtils.getStringContent(el);
			}
			else if (END_POINT_PADDING.equals(elName)) {
				endPointPadding = XmlUtils.getStringContent(el);
			}						
			else if (END_POINT_FORMAT.equals(elName)) {
				endPointFormat = XmlUtils.getStringContent(el);
			} 
			else if (GROUP_PADDING.equals(elName)) {
				groupPadding = XmlUtils.getDoubleContent(el);
			}
			else if (POINT_PADDING.equals(elName)) {
				pointPadding = XmlUtils.getDoubleContent(el);
			}
			else if (BAR_SHAPE_TYPE.equals(elName)) {
				shapeType = XmlUtils.getStringContent(el);
			}
			else if (SHOW_BAR_BORDERS.equals(elName)) 
				showBarBorder = XmlUtils.getBooleanContent(el);
		}
	}
	@Override
	public void parseElement(XMLStreamReader el) throws Exception {
		for (XMLStreamIterator iter = new XMLStreamIterator(el); iter.hasNext(); ) {
			String elName = el.getLocalName();
			
			if (START_POINT_DISPLAY.equals(elName)) {
				startPointDisplay = XmlUtils.getBooleanContent(el);
			}
			else if (START_POINT_ANCHOR.equals(elName)) {
				startPointAnchor = XmlUtils.getStringContent(el);
			}
			else if (START_POINT_VALIGN.equals(elName)) {
				startPointVAlign = XmlUtils.getStringContent(el);
			}
			else if (START_POINT_HALIGN.equals(elName)) {
				startPointHAlign = XmlUtils.getStringContent(el);
			}
			else if (START_POINT_PADDING.equals(elName)) {
				startPointPadding = XmlUtils.getStringContent(el);
			}						
			else if (START_POINT_FORMAT.equals(elName)) {
				startPointFormat = XmlUtils.getStringContent(el);
			}
			else if (END_POINT_DISPLAY.equals(elName)) {
				endPointDisplay = XmlUtils.getBooleanContent(el);
			}
			else if (END_POINT_ANCHOR.equals(elName)) {
				endPointAnchor = XmlUtils.getStringContent(el);
			}
			else if (END_POINT_VALIGN.equals(elName)) {
				endPointVAlign = XmlUtils.getStringContent(el);
			}
			else if (END_POINT_HALIGN.equals(elName)) {
				endPointHAlign = XmlUtils.getStringContent(el);
			}
			else if (END_POINT_PADDING.equals(elName)) {
				endPointPadding = XmlUtils.getStringContent(el);
			}			
			else if (END_POINT_FORMAT.equals(elName)) {
				endPointFormat = XmlUtils.getStringContent(el);
			}
			else if (GROUP_PADDING.equals(elName)) {
				groupPadding = XmlUtils.getDoubleContent(el);
			}
			else if (POINT_PADDING.equals(elName)) {
				pointPadding = XmlUtils.getDoubleContent(el);
			}
			else if (BAR_SHAPE_TYPE.equals(elName)) {
				shapeType = XmlUtils.getStringContent(el);
			}
			else if (SHOW_BAR_BORDERS.equals(elName)) 
				showBarBorder = XmlUtils.getBooleanContent(el);
		}
	}
	
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, START_POINT_DISPLAY, startPointDisplay, ns);
		XmlUtils.addContent(fac, parent, START_POINT_ANCHOR, startPointAnchor, ns);
		XmlUtils.addContent(fac, parent, START_POINT_HALIGN, startPointHAlign, ns);
		XmlUtils.addContent(fac, parent, START_POINT_VALIGN, startPointVAlign, ns);
		XmlUtils.addContent(fac, parent, START_POINT_PADDING, startPointPadding, ns);
		XmlUtils.addContent(fac, parent, START_POINT_FORMAT, startPointFormat, ns);

		XmlUtils.addContent(fac, parent, END_POINT_DISPLAY, endPointDisplay, ns);
		XmlUtils.addContent(fac, parent, END_POINT_ANCHOR, endPointAnchor, ns);
		XmlUtils.addContent(fac, parent, END_POINT_HALIGN, endPointHAlign, ns);
		XmlUtils.addContent(fac, parent, END_POINT_VALIGN, endPointVAlign, ns);
		XmlUtils.addContent(fac, parent, END_POINT_PADDING, endPointPadding, ns);
		XmlUtils.addContent(fac, parent, END_POINT_FORMAT, endPointFormat, ns);
		
		XmlUtils.addContent(fac, parent, GROUP_PADDING, groupPadding, ns);
		XmlUtils.addContent(fac, parent, POINT_PADDING, pointPadding, ns);
		XmlUtils.addContent(fac, parent, BAR_SHAPE_TYPE, shapeType, ns);
		XmlUtils.addContent(fac, parent, SHOW_BAR_BORDERS, showBarBorder, ns);
	}
	
	public BirstXMLSerializable createNew() {
		return new RangeBarSeries();
	}
	
	public boolean isStartPointDisplay() {
		return startPointDisplay;
	}
	public String getStartPointAnchor() {
		return startPointAnchor;
	}
	public String getStartPointHAlign() {
		return startPointHAlign;
	}
	public String getStartPointVAlign() {
		return startPointVAlign;
	}
	public String getStartPointFormat() {
		return startPointFormat;
	}
	public boolean isEndPointDisplay() {
		return endPointDisplay;
	}
	public String getEndPointAnchor() {
		return endPointAnchor;
	}
	public String getEndPointHAlign() {
		return endPointHAlign;
	}
	public String getEndPointVAlign() {
		return endPointVAlign;
	}
	public String getEndPointFormat() {
		return endPointFormat;
	}
	public String getStartPointPadding() {
		return startPointPadding;
	}
	public String getEndPointPadding() {
		return endPointPadding;
	}
	
	public String getShapeType() {
		return shapeType;
	}
	public boolean isShowBarBorder() {
		return showBarBorder;
	}
	
	public double getGroupPadding() {
		return groupPadding;
	}
	public double getPointPadding() {
		return pointPadding;
	}
	
	public void applyStyle(RangeBarSeries style) {
		if (style == null)
			return;
		
		startPointDisplay = style.startPointDisplay;
		startPointAnchor = style.startPointAnchor;
		startPointHAlign = style.startPointHAlign;
		startPointVAlign = style.startPointVAlign;
		startPointPadding = style.startPointPadding;
		startPointFormat = style.startPointFormat;
		
		endPointDisplay = style.endPointDisplay;
		endPointAnchor = style.endPointAnchor;
		endPointHAlign = style.endPointHAlign;
		endPointVAlign = style.endPointVAlign;
		endPointPadding = style.endPointPadding;
		endPointFormat = style.endPointFormat;
		
		groupPadding = style.groupPadding;
		pointPadding = style.pointPadding;
		shapeType = style.shapeType;
		showBarBorder = style.showBarBorder;
	}
}
