package com.successmetricsinc.chart.AnyChart;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.successmetricsinc.adhoc.AdhocColumn;
import com.successmetricsinc.adhoc.AdhocReport;
import com.successmetricsinc.chart.ChartOptions;
import com.successmetricsinc.chart.ChartOptions.GeoMapType;
import com.successmetricsinc.util.BirstXMLSerializable;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

public class MapSeries extends BirstXMLSerializable {
	private static final long serialVersionUID = 1L;
	public static final String CUSTOM = "Custom";
	private static final String CUSTOM_THRESHOLDS = "customThresholds";
	private static final String OTHER = "Other";
	private static final String LONGITUDE = "longitude";
	private static final String LATITUDE = "latitude";
	private static final String MAP_NAME_COLUMN = "mapNameColumn";
	private static final String ENABLE_MAP_SCROLLING = "enableMapScrolling";
	private static final String ENABLE_MAP_ZOOMING = "enableMapZooming";
	private static final String ENABLE_COLOR_SWATCH = "enableColorSwatch";
	private static final String THRESHOLD_TYPE = "thresholdType";
	private static final String NUM_THRESHOLDS = "numThresholds";
	private static final String HIDE_COORDINATE_GRID = "hideCoordinateGrid";
	private static final String SHOW_GEO_MAP_LABEL = "showGeoMapLabel";
	private static final String ZOOM_FACTOR = "zoomFactor";
	private static final String ZOOM_RANGE_MAX = "zoomRangeMax";
	private static final String ZOOM_RANGE_MIN = "zoomRangeMin";
	private static final String PROJECTION_TYPE = "ProjectionType";
	private static final String SUB_MAP_NAME = "SubMapName";
	private static final String GEO_MAP_TYPE = "GeoMapType";
	private static final String USSTATE_WITH_ZIP3_MAP = "USStateWithZip3Map";
	private static final String USSTATE_WITH_COUNTIES_MAP = "USStateWithCountiesMap";
	private static final String COUNTRY_MAP = "CountryMap";
	private static final String WORLD_MAP = "WorldMap";
	private static final String CONTINENT_MAP = "ContinentMap";
	private static final String MAP_ID_COLUMN = "MapIdColumn";
	private static final GeoMapType DEFAULT_GEO_MAP = GeoMapType.WorldMap;
	
	//Google Maps
	protected static final String GOOG_COLUMN_KML = "gKml";
	protected static final String GOOG_RENDER_PLACEMARKS = "useGPlacemarks";
	protected static final String GOOG_PLACEMARKS_KML = "gPlacemarksKml";
	private static final String GOOG_MAP_TYPE_NORMAL = "normal";
	private static final String GOOG_MAP_TYPE_SATELLITE = "satellite";
	private static final String GOOG_MAP_TYPE_HYBRID = "hybrid";
	private static final String GOOG_MAP_TYPE_PHYSICAL = "physical";
	
	private static final MapIdColumn DEFAULT_MAP_ID_COLUMN = MapIdColumn.RegionName;
	private static final ThresholdType DEFAULT_THRESHOLD_TYPE = ThresholdType.Quantiles;
	
	public enum MapIdColumn { RegionId, RegionName};
	public enum ThresholdType {Quantiles, EqualSteps, AbsoluteDeviation, Custom};
	private MapIdColumn idColumn = DEFAULT_MAP_ID_COLUMN;
	public GeoMapType geoMapType = DEFAULT_GEO_MAP;
	public String subMapName;
	public String projectionType = "Equirectangular";
	protected int zoomRangeMin;
	protected int zoomRangeMax;
	protected int zoomFactor;
	protected boolean showGeoMapLabel;
	protected boolean hideCoordinateGrid;
	
	protected int numThresholds = 3;
	protected ThresholdType thresholdType = DEFAULT_THRESHOLD_TYPE;
	protected boolean enableColorSwatch;
	protected boolean enableMapZooming;
	protected boolean enableMapScrolling;
	protected int mapNameIndex = -1;
	protected int longitudeIndex = -1;
	protected int latitudeIndex = -1;
	protected List<CustomThreshold> customThresholds;
	
	private static Map<MapIdColumn, String> mapIdColumnMap = new HashMap<MapIdColumn, String>();
	private static Map<GeoMapType, String> mapTypeMap = new HashMap<GeoMapType, String>();
	private static Map<ThresholdType, String> thresholdTypeMap = new HashMap<ThresholdType, String>();
	
	static {
		mapIdColumnMap.put(MapIdColumn.RegionId, "REGION_ID");
		mapIdColumnMap.put(MapIdColumn.RegionName, "REGION_NAME");
		
		mapTypeMap.put(GeoMapType.ContinentMap, CONTINENT_MAP);
		mapTypeMap.put(GeoMapType.CountryMap, COUNTRY_MAP);
		mapTypeMap.put(GeoMapType.USStateWithCountiesMap, USSTATE_WITH_COUNTIES_MAP);
		mapTypeMap.put(GeoMapType.USStateWithZip3Map, USSTATE_WITH_ZIP3_MAP);
		mapTypeMap.put(GeoMapType.WorldMap, WORLD_MAP);
		mapTypeMap.put(GeoMapType.Other, OTHER);
		
		thresholdTypeMap.put(ThresholdType.AbsoluteDeviation, "AbsoluteDeviation");
		thresholdTypeMap.put(ThresholdType.Custom, CUSTOM);
		thresholdTypeMap.put(ThresholdType.EqualSteps, "EqualSteps");
		thresholdTypeMap.put(ThresholdType.Quantiles, "Quantiles");
	}
	
	//Google Maps
	protected String uMapKml = "none";
	protected boolean renderGooglePlacemarks = false;
	protected String placemarksKml = "";
	private static final String uMapType = null;
	
	public MapSeries() {
		
	}

	public MapSeries(ChartOptions co, AdhocReport report, OMElement defaultSettings) {
		parseElement(defaultSettings);
		idColumn = parseTypeFromString(co.mapColumnName, mapIdColumnMap, DEFAULT_MAP_ID_COLUMN);
		geoMapType = co.geoMapType;
		subMapName = co.subMapName;
		projectionType = co.mapProjection;
		zoomRangeMin = co.zoomRangeMin;
		zoomRangeMax = co.zoomRangeMax;
		zoomFactor = co.zoomFactor;
		showGeoMapLabel = co.showGeoMapLabel;
		hideCoordinateGrid = co.hideCoordinateGrid;
		numThresholds = co.numSegments;
		thresholdType = parseTypeFromString(co.thresholdType, thresholdTypeMap, DEFAULT_THRESHOLD_TYPE);
		if (thresholdType == ThresholdType.Custom) {
			String[] oldCustomThresholds = co.customThresholds.split("#");
			if (oldCustomThresholds.length >= 2) {
				this.customThresholds = new ArrayList<CustomThreshold>();
				for (int i = 0; i < oldCustomThresholds.length; i++) {
					String[] ct = oldCustomThresholds[i].split(":");
					if (ct.length < 4)
						continue;
					
					CustomThreshold customThreshold = new CustomThreshold();
					try {
						Integer clr = Integer.valueOf(ct[3]);
						customThreshold.color = new Color(clr);
						customThreshold.maxValue = ct[2].length() > 0 ? Double.valueOf(ct[2]) : null;
						customThreshold.minValue = ct[1].length() > 0 ? Double.valueOf(ct[1]) : null;

						if (customThreshold.maxValue == null && customThreshold.minValue == null) {
							continue;
						}
						customThreshold.label = ct[0].length() > 0 ? ct[0] : "Condition" + i;
						this.customThresholds.add(customThreshold);
					}
					catch (NumberFormatException e) { }
				}
			}
		}
		enableColorSwatch = co.enableColorSwatch;
		enableMapScrolling = co.enableMapScrolling;
		enableMapZooming = co.enableMapZooming;
		for (AdhocColumn ac: report.getColumns()) {
			String label = ac.getLabelString();
			if (label.equals(co.mapNameColumn))
				mapNameIndex = ac.getReportIndex();
			else if (label.equals(co.latitude))
				latitudeIndex = ac.getReportIndex();
			else if (label.equals(co.longitude))
				longitudeIndex = ac.getReportIndex();
		}
	}

	public MapSeries(OMElement defaultSettings) {
		parseElement(defaultSettings);
	}
	
	@Override
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, MAP_ID_COLUMN, mapIdColumnMap.get(idColumn), ns);
		XmlUtils.addContent(fac, parent, GEO_MAP_TYPE, mapTypeMap.get(geoMapType), ns);
		XmlUtils.addContent(fac, parent, SUB_MAP_NAME, subMapName, ns);
		XmlUtils.addContent(fac, parent, PROJECTION_TYPE, projectionType, ns);
		XmlUtils.addContent(fac, parent, ZOOM_RANGE_MIN, zoomRangeMin, ns);
		XmlUtils.addContent(fac, parent, ZOOM_RANGE_MAX, zoomRangeMax, ns);
		XmlUtils.addContent(fac, parent, ZOOM_FACTOR, zoomFactor, ns);
		XmlUtils.addContent(fac, parent, SHOW_GEO_MAP_LABEL, showGeoMapLabel, ns);
		XmlUtils.addContent(fac, parent, HIDE_COORDINATE_GRID, hideCoordinateGrid, ns);
		XmlUtils.addContent(fac, parent, NUM_THRESHOLDS, numThresholds, ns);
		XmlUtils.addContent(fac, parent, THRESHOLD_TYPE, thresholdTypeMap.get(thresholdType), ns);
		XmlUtils.addContent(fac, parent, ENABLE_COLOR_SWATCH, enableColorSwatch, ns);
		XmlUtils.addContent(fac, parent, ENABLE_MAP_ZOOMING, enableMapZooming, ns);
		XmlUtils.addContent(fac, parent, ENABLE_MAP_SCROLLING, enableMapScrolling, ns);
		XmlUtils.addContent(fac, parent, MAP_NAME_COLUMN, mapNameIndex, ns);
		XmlUtils.addContent(fac, parent, LATITUDE, latitudeIndex, ns);
		XmlUtils.addContent(fac, parent, LONGITUDE, longitudeIndex, ns);
		XmlUtils.addContent(fac, parent, CUSTOM_THRESHOLDS, customThresholds, ns);
		XmlUtils.addContent(fac, parent, GOOG_RENDER_PLACEMARKS, renderGooglePlacemarks, ns);
		XmlUtils.addContent(fac, parent, GOOG_PLACEMARKS_KML, placemarksKml, ns);
		XmlUtils.addContent(fac, parent, GOOG_COLUMN_KML, uMapKml, ns);
	}

	@Override
	public BirstXMLSerializable createNew() {
		return new MapSeries();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void parseElement(OMElement parent) {
		if (parent == null)
			return;
		for (Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();

			if (MAP_ID_COLUMN.equals(elName)) {
				idColumn = parseTypeFromString(XmlUtils.getStringContent(el), mapIdColumnMap, DEFAULT_MAP_ID_COLUMN);
			}
			else if (GEO_MAP_TYPE.equals(elName))
				geoMapType = decodeGeoMapTypeString(XmlUtils.getStringContent(el));
			else if (SUB_MAP_NAME.equals(elName))
				subMapName = XmlUtils.getStringContent(el);
			else if (PROJECTION_TYPE.equals(elName))
				projectionType = XmlUtils.getStringContent(el);
			else if (ZOOM_RANGE_MIN.equals(elName))
				zoomRangeMin = XmlUtils.getIntContent(el);
			else if (ZOOM_RANGE_MAX.equals(elName))
				zoomRangeMax = XmlUtils.getIntContent(el);
			else if (ZOOM_FACTOR.equals(elName))
				zoomFactor = XmlUtils.getIntContent(el);
			else if (SHOW_GEO_MAP_LABEL.equals(elName))
				showGeoMapLabel = XmlUtils.getBooleanContent(el);
			else if (HIDE_COORDINATE_GRID.equals(elName))
				hideCoordinateGrid = XmlUtils.getBooleanContent(el);
			else if (NUM_THRESHOLDS.equals(elName))
				numThresholds = XmlUtils.getIntContent(el);
			else if (THRESHOLD_TYPE.equals(elName)) 
				thresholdType = decodeThresholdTypeString(XmlUtils.getStringContent(el));
			else if (ENABLE_COLOR_SWATCH.equals(elName))
				enableColorSwatch = XmlUtils.getBooleanContent(el);
			else if (ENABLE_MAP_ZOOMING.equals(elName))
				enableMapZooming = XmlUtils.getBooleanContent(el);
			else if (ENABLE_MAP_SCROLLING.equals(elName))
				enableMapScrolling = XmlUtils.getBooleanContent(el);
			else if (MAP_NAME_COLUMN.equals(elName))
				mapNameIndex = XmlUtils.getIntContent(el);
			else if (LATITUDE.equals(elName))
				latitudeIndex = XmlUtils.getIntContent(el);
			else if (LONGITUDE.equals(elName))
				longitudeIndex = XmlUtils.getIntContent(el);
			else if (CUSTOM_THRESHOLDS.equals(elName))
				customThresholds = (List<CustomThreshold>)XmlUtils.parseAllChildren(el, new CustomThreshold(), new ArrayList<CustomThreshold>());
			else if (GOOG_RENDER_PLACEMARKS.equals(elName))
				renderGooglePlacemarks = XmlUtils.getBooleanContent(el);
			else if (GOOG_PLACEMARKS_KML.equals(elName))
				placemarksKml = XmlUtils.getStringContent(el);
			else if (GOOG_COLUMN_KML.equals(elName))
				uMapKml = XmlUtils.getStringContent(el);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void parseElement(XMLStreamReader el) throws Exception {
		for (XMLStreamIterator iter = new XMLStreamIterator(el); iter.hasNext(); ) {
			String elName = el.getLocalName();

			if (MAP_ID_COLUMN.equals(elName)) {
				idColumn = parseTypeFromString(XmlUtils.getStringContent(el), mapIdColumnMap, DEFAULT_MAP_ID_COLUMN);
			}
			else if (GEO_MAP_TYPE.equals(elName))
				geoMapType = decodeGeoMapTypeString(XmlUtils.getStringContent(el));
			else if (SUB_MAP_NAME.equals(elName))
				subMapName = XmlUtils.getStringContent(el);
			else if (PROJECTION_TYPE.equals(elName))
				projectionType = XmlUtils.getStringContent(el);
			else if (ZOOM_RANGE_MIN.equals(elName))
				zoomRangeMin = XmlUtils.getIntContent(el);
			else if (ZOOM_RANGE_MAX.equals(elName))
				zoomRangeMax = XmlUtils.getIntContent(el);
			else if (ZOOM_FACTOR.equals(elName))
				zoomFactor = XmlUtils.getIntContent(el);
			else if (SHOW_GEO_MAP_LABEL.equals(elName))
				showGeoMapLabel = XmlUtils.getBooleanContent(el);
			else if (HIDE_COORDINATE_GRID.equals(elName))
				hideCoordinateGrid = XmlUtils.getBooleanContent(el);
			else if (NUM_THRESHOLDS.equals(elName))
				numThresholds = XmlUtils.getIntContent(el);
			else if (THRESHOLD_TYPE.equals(elName)) 
				thresholdType = decodeThresholdTypeString(XmlUtils.getStringContent(el));
			else if (ENABLE_COLOR_SWATCH.equals(elName))
				enableColorSwatch = XmlUtils.getBooleanContent(el);
			else if (ENABLE_MAP_ZOOMING.equals(elName))
				enableMapZooming = XmlUtils.getBooleanContent(el);
			else if (ENABLE_MAP_SCROLLING.equals(elName))
				enableMapScrolling = XmlUtils.getBooleanContent(el);
			else if (MAP_NAME_COLUMN.equals(elName))
				mapNameIndex = XmlUtils.getIntContent(el);
			else if (LATITUDE.equals(elName))
				latitudeIndex = XmlUtils.getIntContent(el);
			else if (LONGITUDE.equals(elName))
				longitudeIndex = XmlUtils.getIntContent(el);
			else if (CUSTOM_THRESHOLDS.equals(elName))
				customThresholds = (List<CustomThreshold>)XmlUtils.parseAllChildren(el, new CustomThreshold(), new ArrayList<CustomThreshold>());
			else if (GOOG_RENDER_PLACEMARKS.equals(elName))
				renderGooglePlacemarks = XmlUtils.getBooleanContent(el);
			else if (GOOG_PLACEMARKS_KML.equals(elName))
				placemarksKml = XmlUtils.getStringContent(el);
			else if (GOOG_COLUMN_KML.equals(elName))
				uMapKml = XmlUtils.getStringContent(el);
		}
	}
	
	private ThresholdType decodeThresholdTypeString(String stringContent) {
		for (ThresholdType key : thresholdTypeMap.keySet()) {
			if (thresholdTypeMap.get(key).equals(stringContent))
				return key;
		}
		return DEFAULT_THRESHOLD_TYPE;
	}

	public GeoMapType decodeGeoMapTypeString(String typeString) {
		for (GeoMapType key : mapTypeMap.keySet()) {
			if (mapTypeMap.get(key).equals(typeString))
				return key;
		}
		return DEFAULT_GEO_MAP;
	}
	
	public String getMapIdColumn() {
		return mapIdColumnMap.get(idColumn);
	}

	public String getThresholdType() {
		return thresholdTypeMap.get(thresholdType);
	}

	public void applyStyle(MapSeries style) {
		if (style == null)
			return;
		
		projectionType = style.projectionType;
		zoomRangeMin = style.zoomRangeMin;
		zoomRangeMax = style.zoomRangeMax;
		zoomFactor = style.zoomFactor;
		showGeoMapLabel = style.showGeoMapLabel;
		hideCoordinateGrid = style.hideCoordinateGrid;
		enableColorSwatch = style.enableColorSwatch;
		enableMapZooming = style.enableMapZooming;
		enableMapScrolling = style.enableMapScrolling;
	}

}
