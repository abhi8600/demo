/**
 * $Id: NoDataException.java,v 1.1 2010-09-16 22:19:17 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.chart.AnyChart;

import com.successmetricsinc.util.BaseException;

/**
 * Exception thrown when requested data is empty
 * 
 */
public class NoDataException extends BaseException
{
	private static final long serialVersionUID = 1L;

	/**
	 * @param s
	 */
	public NoDataException(String s, Throwable cause)
	{
		super(s, cause);
	}
	
	public NoDataException(String s)
	{
		super(s);
	}

	@Override
	public int getErrorCode() {
		return ERROR_NO_DATA;
	}
}
