/**
 * 
 */
package com.successmetricsinc.chart.AnyChart;

import java.util.Iterator;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.jdom.Element;

import com.successmetricsinc.chart.ChartOptions;
import com.successmetricsinc.chart.AnyChart.AnyChartNode.FillType;
import com.successmetricsinc.util.BirstXMLSerializable;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class BubbleSeries extends BirstXMLSerializable {

	private static final long serialVersionUID = 1L;
	private static final String MIN_BUBBLE_SIZE = "MinBubbleSize";
	private static final String MAX_BUBBLE_SIZE = "MaxBubbleSize";
	private static final String BUBBLE_SIZE_MULTIPLIER = "BubbleSizeMultiplier";
	private static final String BUBBLE_BORDER_THICKNESS = "BubbleBorderThickness";
	private static final String BUBBLE_OPACITY = "BubbleOpacity";

	public static final int DEFAULT_MIN_BUBBLE_SIZE = 5;
	public static final int DEFAULT_MAX_BUBBLE_SIZE = 20;
	
	protected int minBubbleSize = DEFAULT_MIN_BUBBLE_SIZE;
	protected int maxBubbleSize = DEFAULT_MAX_BUBBLE_SIZE;
	private String bubbleSizeMultiplier = null;
	
	private String bubbleBorderThickness = null;
	private String bubbleOpacity = null;
	
	public BubbleSeries() {
	}
	
	public BubbleSeries(OMElement defaultSettings) {
		parseElement(defaultSettings);
	}
	
	public BubbleSeries(ChartOptions co, OMElement defaultSettings) {
		parseElement(defaultSettings);
	}	

	@Override
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, MIN_BUBBLE_SIZE, minBubbleSize, ns);
		XmlUtils.addContent(fac, parent, MAX_BUBBLE_SIZE, maxBubbleSize, ns);
		XmlUtils.addContent(fac, parent, BUBBLE_SIZE_MULTIPLIER, bubbleSizeMultiplier, ns);
		XmlUtils.addContent(fac, parent, BUBBLE_BORDER_THICKNESS, bubbleBorderThickness, ns);
		XmlUtils.addContent(fac, parent, BUBBLE_OPACITY, bubbleOpacity, ns);
	}

	@Override
	public BirstXMLSerializable createNew() {
		return new BubbleSeries();
	}

	@Override
	public void parseElement(OMElement parent) {
		if (parent == null)
			return;
		for (@SuppressWarnings("unchecked")
		Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();
			
			if (MIN_BUBBLE_SIZE.equals(elName))
				minBubbleSize = XmlUtils.getIntContent(el);
			else if (MAX_BUBBLE_SIZE.equals(elName))
				maxBubbleSize = XmlUtils.getIntContent(el);
			else if (BUBBLE_SIZE_MULTIPLIER.equals(elName))
				bubbleSizeMultiplier = XmlUtils.getStringContent(el);
			else if (BUBBLE_BORDER_THICKNESS.equals(elName))
				bubbleBorderThickness = XmlUtils.getStringContent(el);	
			else if (BUBBLE_OPACITY.equals(elName))
				bubbleOpacity = XmlUtils.getStringContent(el);						
		}
	}

	public void getXML(Element chart, AnyChartNode node) {
		Element dataPlotSettings = XmlUtils.findOrCreateChild(chart, "data_plot_settings");
		Element bubbleSeriesEl = XmlUtils.findOrCreateChild(dataPlotSettings, "bubble_series");
		bubbleSeriesEl.setAttribute("maximum_bubble_size", String.valueOf(maxBubbleSize) + "%");
		bubbleSeriesEl.setAttribute("minimum_bubble_size", String.valueOf(minBubbleSize) + "%");
		if (bubbleSizeMultiplier != null)
			bubbleSeriesEl.setAttribute("bubble_size_multiplier", bubbleSizeMultiplier + "%");
		if (bubbleOpacity == null && bubbleBorderThickness == null)
			bubbleSeriesEl.setAttribute("style", "Aqua");
		else
		{
			Element bubbleStyleEl = XmlUtils.findOrCreateChild(bubbleSeriesEl, "bubble_style");	
			Element fillEl = XmlUtils.findOrCreateChild(bubbleStyleEl, "fill");
			// set fill
			fillEl.setAttribute("type", node.getFillTypeString());
			if (node.getFillType() == FillType.Gradient) {
				Gradient gradient = node.getFillGradient();
				gradient.getXML(fillEl);
			}
			if (bubbleOpacity != null && bubbleOpacity.length() > 0)
			{
				fillEl.setAttribute("opacity", bubbleOpacity);
			}
			if (bubbleBorderThickness != null && bubbleBorderThickness.length() > 0)
			{
				Element borderEl = XmlUtils.findOrCreateChild(bubbleStyleEl, "border");
				borderEl.setAttribute("thickness", bubbleBorderThickness);
			}
		}
	}

	@Override
	public void parseElement(XMLStreamReader el) throws Exception {
		for (XMLStreamIterator iter = new XMLStreamIterator(el); iter.hasNext(); ){
			String elName = el.getLocalName();
			
			if (MIN_BUBBLE_SIZE.equals(elName))
				minBubbleSize = XmlUtils.getIntContent(el);
			else if (MAX_BUBBLE_SIZE.equals(elName))
				maxBubbleSize = XmlUtils.getIntContent(el);
			else if (BUBBLE_SIZE_MULTIPLIER.equals(elName))
				bubbleSizeMultiplier = XmlUtils.getStringContent(el);	
			else if (BUBBLE_BORDER_THICKNESS.equals(elName))
				bubbleBorderThickness = XmlUtils.getStringContent(el);	
			else if (BUBBLE_OPACITY.equals(elName))
				bubbleOpacity = XmlUtils.getStringContent(el);			
		}
	}

	public int getMinBubbleSize() {
		return minBubbleSize;
	}

	public void setMinBubbleSize(int minBubbleSize) {
		this.minBubbleSize = minBubbleSize;
	}

	public int getMaxBubbleSize() {
		return maxBubbleSize;
	}

	public void setMaxBubbleSize(int maxBubbleSize) {
		this.maxBubbleSize = maxBubbleSize;
	}

	public String getBubbleSizeMultiplier() {
		return bubbleSizeMultiplier;
	}

	public void setBubbleSizeMultiplier(String bubbleSizeMultiplier) {
		this.bubbleSizeMultiplier = bubbleSizeMultiplier;
	}

	public void applyStyle(BubbleSeries style) {
		if (style == null)
			return;

		minBubbleSize = style.minBubbleSize;
		maxBubbleSize = style.maxBubbleSize;
	}
}
