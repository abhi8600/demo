/**
 * 
 */
package com.successmetricsinc.chart.AnyChart;

import java.awt.Color;
import java.awt.Font;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.jdom.Element;
import org.jfree.ui.RectangleEdge;

import com.successmetricsinc.chart.ChartOptions;
import com.successmetricsinc.util.BirstXMLSerializable;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class LegendSettings extends BirstXMLSerializable {

	private static final long serialVersionUID = 1L;
	private static final String COLOR = "Color";
	private static final String FONT = "Font";
	private static final String LEGEND_LOCATION = "Location";
	private static final String LEGEND_FORMAT = "LegendFormat";
	private static final String LEGEND_SOURCE = "LegendSource";
	public static final LegendLocation DEFAULT_LEGEND_LOCATION = LegendLocation.Bottom;

	public enum LegendLocation { Bottom, Left, Top, Right };
	private Font font;
	private Color color = Color.black;
	private LegendLocation location = DEFAULT_LEGEND_LOCATION;
	private String legendFormat;
	private String legendSource;
	
	private static Map<LegendLocation, String> legendLocationsMap = new HashMap<LegendLocation, String>();
	
	static {
		legendLocationsMap.put(LegendLocation.Bottom, "Bottom");
		legendLocationsMap.put(LegendLocation.Left, "Left");
		legendLocationsMap.put(LegendLocation.Top, "Top");
		legendLocationsMap.put(LegendLocation.Right, "Right");
	}
	
	public LegendSettings() {
		
	}
	
	public LegendSettings(ChartOptions co) {
		font = co.getLegendFont();
		if (co.legendAnchor == RectangleEdge.RIGHT)
		{
			location = LegendLocation.Right;
		} else if (co.legendAnchor == RectangleEdge.TOP)
		{
			location = LegendLocation.Top;
		} else if (co.legendAnchor == RectangleEdge.LEFT)
		{
			location = LegendLocation.Left;
		}
	}

	/* (non-Javadoc)
	 * @see com.successmetricsinc.util.BirstXMLSerializable#addContent(org.apache.axiom.om.OMFactory, org.apache.axiom.om.OMElement, org.apache.axiom.om.OMNamespace)
	 */
	@Override
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, LEGEND_LOCATION, legendLocationsMap.get(location), ns);
		XmlUtils.addContent(fac, parent, FONT, font, ns);
		XmlUtils.addContent(fac, parent, COLOR, color, ns);
		XmlUtils.addContent(fac, parent, LEGEND_FORMAT, legendFormat, ns);
		XmlUtils.addContent(fac, parent, LEGEND_SOURCE, legendSource, ns);
	}

	/* (non-Javadoc)
	 * @see com.successmetricsinc.util.BirstXMLSerializable#createNew()
	 */
	@Override
	public BirstXMLSerializable createNew() {
		return new LegendSettings();
	}

	/* (non-Javadoc)
	 * @see com.successmetricsinc.util.BirstXMLSerializable#parseElement(org.apache.axiom.om.OMElement)
	 */
	@Override
	public void parseElement(OMElement parent) {
		for (@SuppressWarnings("unchecked")
		Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();

			if (LEGEND_LOCATION.equals(elName))
				location = decodeLocationType(XmlUtils.getStringContent(el));
			else if (FONT.equals(elName))
				font = XmlUtils.getFontContent(el);
			else if (COLOR.equals(elName))
				color = XmlUtils.getColorContent(el);
			else if (LEGEND_FORMAT.equals(elName))
				legendFormat = XmlUtils.getStringContent(el);
			else if (LEGEND_SOURCE.equals(elName))
				legendSource = XmlUtils.getStringContent(el);
		}
	}
	@Override
	public void parseElement(XMLStreamReader parser) throws Exception {
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
			String elName = parser.getLocalName();

			if (LEGEND_LOCATION.equals(elName))
				location = decodeLocationType(XmlUtils.getStringContent(parser));
			else if (FONT.equals(elName))
				font = XmlUtils.getFontContent(parser);
			else if (COLOR.equals(elName))
				color = XmlUtils.getColorContent(parser);
			else if (LEGEND_FORMAT.equals(elName))
				legendFormat = XmlUtils.getStringContent(parser);
			else if (LEGEND_SOURCE.equals(elName))
				legendSource = XmlUtils.getStringContent(parser);			
		}
	}

	private LegendLocation decodeLocationType(String stringContent) {
		for (LegendLocation key : legendLocationsMap.keySet()) {
			if (legendLocationsMap.get(key).equals(stringContent))
				return key;
		}
		return DEFAULT_LEGEND_LOCATION;
	}
	
	public static void addLegendSettings(Element parent, LegendSettings l, double scaleFactor) {
		Element legendEl = XmlUtils.findOrCreateChild(parent, "legend");
		legendEl.setAttribute("enabled", "True");
		legendEl.setAttribute("padding", "5");
		legendEl.setAttribute("rows_padding", "0");
		legendEl.setAttribute("columns_padding", "15");
		legendEl.setAttribute("ignore_auto_item", "True");
		legendEl.setAttribute("align", "Spread");
		legendEl.setAttribute("position", legendLocationsMap.get(l == null ? LegendSettings.DEFAULT_LEGEND_LOCATION : l.location));
		
		Element titleEl = XmlUtils.findOrCreateChild(legendEl, "title");
		titleEl.setAttribute("enabled", "False");
		
		Font font = (l == null ? null : l.font);
		if (font == null)
			font = Font.decode(ChartOptions.DEFAULT_LEGEND_FONT_STRING);
		Element fontEl = BirstXMLSerializable.getFontElement(font, scaleFactor);
		legendEl.addContent(fontEl);
		Color clr = (l == null ? null : l.color);
		if (clr == null)
			clr = Color.BLACK;
		AnyChartNode.addColorAttribute(fontEl, clr);
		
		Element columnsSeperatorEl = XmlUtils.findOrCreateChild(legendEl, "columns_separator");
		columnsSeperatorEl.setAttribute("enabled", "False");
		Element rowsSeparatorEl = XmlUtils.findOrCreateChild(legendEl, "rows_separator");
		rowsSeparatorEl.setAttribute("enabled", "False");
	}
	
	public String getLegendFormat()
	{
		return legendFormat;
	}
	
	public String getLegendSource()
	{
		return legendSource;
	}

	public void applyStyle(LegendSettings style) {
		if (style == null)
			return;
		font = style.font;
		color = style.color;
		location = style.location;
		legendFormat = style.legendFormat;
		legendSource = style.legendSource;
	}


}
