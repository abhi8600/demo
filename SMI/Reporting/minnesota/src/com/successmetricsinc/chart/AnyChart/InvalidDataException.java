/**
 * 
 */
package com.successmetricsinc.chart.AnyChart;

import com.successmetricsinc.util.BaseException;

/**
 * @author agarrison
 *
 */
@SuppressWarnings("serial")
public class InvalidDataException extends BaseException {

	/**
	 * @param s
	 */
	public InvalidDataException(String s, Throwable cause)
	{
		super(s, cause);
	}
	
	public InvalidDataException(String s)
	{
		super(s);
	}

	@Override
	public int getErrorCode() {
		return ERROR_INVALID_DATA;
	}
}
