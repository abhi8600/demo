/**
 * 
 */
package com.successmetricsinc.chart.AnyChart;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.jdom.Element;

import com.successmetricsinc.util.BirstXMLSerializable;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class Gradient extends BirstXMLSerializable {

	private static final long serialVersionUID = 1L;
	
	private static final String FOCAL_POINT = "FocalPoint";
	private static final String ANGLE = "Angle";
	private static final String DIRECTION = "Direction";
	private static final String TYPE = "Type";
	private static final String GRADIENT = "Gradient";
	private static GradientType DEFAULT_GRADIENT_TYPE = GradientType.Linear;
	private static GradientDirection DEFAULT_GRADIENT_DIRECTION = GradientDirection.LightToNormal;

	public enum GradientType { Linear, Radial };
	public enum GradientDirection { LightToNormal, LightToDark, NormalToLight, NormalToDark, DarkToLight, DarkToNormal };
	
	private GradientType type = DEFAULT_GRADIENT_TYPE;
	private GradientDirection direction = DEFAULT_GRADIENT_DIRECTION;
	private int angle = 90;
	private double focalPoint = 0.0;
	
	private static Map<GradientType, String> gradientTypeMap = new HashMap<GradientType, String>();
	private static Map<GradientDirection, String> gradientDirectionMap = new HashMap<GradientDirection, String>();
	
	static {
		gradientTypeMap.put(GradientType.Linear, "Linear");
		gradientTypeMap.put(GradientType.Radial, "Radial");
		
		gradientDirectionMap.put(GradientDirection.LightToNormal, "Light To Normal");
		gradientDirectionMap.put(GradientDirection.LightToDark, "Light To Dark");
		gradientDirectionMap.put(GradientDirection.NormalToLight, "Normal To Light");
		gradientDirectionMap.put(GradientDirection.NormalToDark, "Normal To Dark");
		gradientDirectionMap.put(GradientDirection.DarkToLight, "Dark To Light");
		gradientDirectionMap.put(GradientDirection.DarkToNormal, "Dark To Normal");
	}
	/* (non-Javadoc)
	 * @see com.successmetricsinc.util.BirstXMLSerializable#addContent(org.apache.axiom.om.OMFactory, org.apache.axiom.om.OMElement, org.apache.axiom.om.OMNamespace)
	 */
	@Override
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, TYPE, gradientTypeMap.get(type), ns);
		XmlUtils.addContent(fac, parent, DIRECTION, gradientDirectionMap.get(direction), ns);
		XmlUtils.addContent(fac, parent, ANGLE, angle, ns);
		XmlUtils.addContent(fac, parent, FOCAL_POINT, focalPoint, ns);
	}

	/* (non-Javadoc)
	 * @see com.successmetricsinc.util.BirstXMLSerializable#createNew()
	 */
	@Override
	public BirstXMLSerializable createNew() {
		return new Gradient();
	}

	/* (non-Javadoc)
	 * @see com.successmetricsinc.util.BirstXMLSerializable#parseElement(org.apache.axiom.om.OMElement)
	 */
	@Override
	public void parseElement(OMElement parent) {
		for (@SuppressWarnings("unchecked")
				Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
					OMElement el = iter.next();
					String elName = el.getLocalName();

					if (TYPE.equals(elName))
						type = parseTypeFromString(XmlUtils.getStringContent(el), gradientTypeMap, DEFAULT_GRADIENT_TYPE);
					else if (DIRECTION.equals(elName))
						direction = parseTypeFromString(XmlUtils.getStringContent(el), gradientDirectionMap, DEFAULT_GRADIENT_DIRECTION);
					else if (ANGLE.equals(elName))
						angle = XmlUtils.getIntContent(el);
					else if (FOCAL_POINT.equals(elName))
						focalPoint = XmlUtils.getDoubleContent(el);
		}
	}

	/* (non-Javadoc)
	 * @see com.successmetricsinc.util.BirstXMLSerializable#parseElement(javax.xml.stream.XMLStreamReader)
	 */
	@Override
	public void parseElement(XMLStreamReader el) throws Exception {
		for (XMLStreamIterator iter = new XMLStreamIterator(el); iter.hasNext(); ) {
			String elName = el.getLocalName();
			if (TYPE.equals(elName))
				type = parseTypeFromString(XmlUtils.getStringContent(el), gradientTypeMap, DEFAULT_GRADIENT_TYPE);
			else if (DIRECTION.equals(elName))
				direction = parseTypeFromString(XmlUtils.getStringContent(el), gradientDirectionMap, DEFAULT_GRADIENT_DIRECTION);
			else if (ANGLE.equals(elName))
				angle = XmlUtils.getIntContent(el);
			else if (FOCAL_POINT.equals(elName))
				focalPoint = XmlUtils.getDoubleContent(el);
		}

	}

	public Element getXML(Element parent) {
		Element gradientEl = XmlUtils.findOrCreateChild(parent, "gradient");
		gradientEl.setAttribute("type", gradientTypeMap.get(type));
		if (type == GradientType.Radial) {
			gradientEl.setAttribute("focal_point", String.valueOf(focalPoint));
		}
		gradientEl.setAttribute("angle", String.valueOf(angle));
		Element keyEl = new Element("key");
		keyEl.setAttribute("position", "0");
		keyEl.setAttribute("color", getFirstPositionDirection());
		keyEl.setAttribute("opacity", "1");
		gradientEl.addContent(keyEl);
		keyEl = new Element("key");
		keyEl.setAttribute("position", "1");
		keyEl.setAttribute("color", getSecondPositionDirection());
		keyEl.setAttribute("opacity", "1");
		gradientEl.addContent(keyEl);
		return gradientEl;
	}
	
	private String getFirstPositionDirection() {
		if (direction == GradientDirection.DarkToLight || direction == GradientDirection.DarkToNormal)
			return "DarkColor(%Color)";
		if (direction == GradientDirection.LightToDark || direction == GradientDirection.LightToNormal)
			return "LightColor(%Color)";
		return "%Color";
	}
	
	private String getSecondPositionDirection() {
		if (direction == GradientDirection.LightToDark || direction == GradientDirection.NormalToDark)
			return "DarkColor(%Color)";
		if (direction == GradientDirection.DarkToLight || direction == GradientDirection.NormalToLight)
			return "LightColor(%Color)";
		return "%Color";
	}

	public void applyStyle(Gradient style) {
		if (style == null)
			return;
		
		type = style.type;
		direction = style.direction;
		angle = style.angle;
		focalPoint = style.focalPoint;
	}
	
}
