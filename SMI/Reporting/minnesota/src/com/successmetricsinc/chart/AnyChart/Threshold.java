/**
 * 
 */
package com.successmetricsinc.chart.AnyChart;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.successmetricsinc.util.BirstXMLSerializable;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class Threshold extends BirstXMLSerializable{

	private static final long serialVersionUID = 1L;
	private static final String CONDITION = "Condition";
	private static final String CONDITIONS = "Conditions";
	private static final String RANGE_COUNT = "RangeCount";
	private static final String PALETTE = "Palette";
	private static final String THRESHOLD_TYPE = "ThresholdType";
	
	private static final ThresholdType DEFAULT_THRESHOLD_TYPE = ThresholdType.EqualInterval;

	private static final String NAME = "Name";

	private String name;
	
	public enum ThresholdType { Custom, EqualInterval, EqualDistribution };
	private ThresholdType type;
	
	private String palette;
	private int rangeCount;
	
	private List<Condition> conditions;
	
	private static Map<ThresholdType, String> thresholdTypeMap;
	static {
		thresholdTypeMap = new HashMap<ThresholdType, String>();
		thresholdTypeMap.put(ThresholdType.Custom, "Custom");
		thresholdTypeMap.put(ThresholdType.EqualInterval, "EqualInterval");
		thresholdTypeMap.put(ThresholdType.EqualDistribution, "EqualDistribution");
	}

	@Override
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, NAME, name, ns);
		XmlUtils.addContent(fac, parent, THRESHOLD_TYPE,  thresholdTypeMap.get(type), ns);
		XmlUtils.addContent(fac, parent, PALETTE, palette, ns);
		XmlUtils.addContent(fac, parent, RANGE_COUNT, rangeCount, ns);
		if (conditions != null) {
			OMElement cs = fac.createOMElement(CONDITIONS, ns);
			for (Condition c: conditions) {
				XmlUtils.addContent(fac, cs, CONDITION, c, ns);
			}
			parent.addChild(cs);
		}
		
	}

	@Override
	public BirstXMLSerializable createNew() {
		return new Threshold();
	}

	@Override
	public void parseElement(OMElement parent) {
		for (@SuppressWarnings("unchecked")
		Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();
			
			if (NAME.equals(elName)) {
				name = XmlUtils.getStringContent(el);
			}
			else if (THRESHOLD_TYPE.equals(elName)) {
				type = parseTypeFromString(XmlUtils.getStringContent(el), thresholdTypeMap, DEFAULT_THRESHOLD_TYPE);
			}
			else if (PALETTE.equals(elName)) {
				palette = XmlUtils.getStringContent(el);
			}
			else if (RANGE_COUNT.equals(elName)) {
				rangeCount = XmlUtils.getIntContent(el);
			}
			else if (CONDITIONS.equals(elName)) {
				conditions = parseConditionList(el);
			}
		}
	}
	@Override
	public void parseElement(XMLStreamReader el) throws Exception {
		for (XMLStreamIterator iter = new XMLStreamIterator(el); iter.hasNext(); ) {
			String elName = el.getLocalName();
			
			if (NAME.equals(elName)) {
				name = XmlUtils.getStringContent(el);
			}
			else if (THRESHOLD_TYPE.equals(elName)) {
				type = parseTypeFromString(XmlUtils.getStringContent(el), thresholdTypeMap, DEFAULT_THRESHOLD_TYPE);
			}
			else if (PALETTE.equals(elName)) {
				palette = XmlUtils.getStringContent(el);
			}
			else if (RANGE_COUNT.equals(elName)) {
				rangeCount = XmlUtils.getIntContent(el);
			}
			else if (CONDITIONS.equals(elName)) {
				conditions = parseConditionList(el);
			}
			
		}
	}

	private List<Condition> parseConditionList(OMElement parent) {
		List<Condition> ret = new ArrayList<Condition>();
		for (Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();
			if (CONDITION.equals(elName)) {
				Condition c = new Condition();
				c.parseElement(el);
				ret.add(c);
			}
		}
		return ret;
	}
	private List<Condition> parseConditionList(XMLStreamReader el) throws Exception {
		List<Condition> ret = new ArrayList<Condition>();
		for (Iterator iter = new XMLStreamIterator(el); iter.hasNext(); ) {
			String elName = el.getLocalName();
			if (CONDITION.equals(elName)) {
				Condition c = new Condition();
				c.parseElement(el);
				ret.add(c);
			}
		}
		return ret;
	}

}
