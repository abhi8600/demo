package com.successmetricsinc.chart.AnyChart;

import java.util.Iterator;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.jdom.Element;

import com.successmetricsinc.chart.ChartOptions;
import com.successmetricsinc.util.BirstXMLSerializable;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

public class AreaSeries extends BirstXMLSerializable {

	private static final long serialVersionUID = 1L;
	private static final String TYPE = "Type";
	private static final String DEFAULT_AREA_TYPE="Area";
	//<line_series><line_style><line thickness=
	private String type = DEFAULT_AREA_TYPE; // Area, SplineArea
	
	public AreaSeries() {
	}
	public AreaSeries(ChartOptions co, OMElement defaultSettings) {
		parseElement(defaultSettings);
	}

	public AreaSeries(OMElement defaultSettings) {
		parseElement(defaultSettings);
	}
	
	public void parseElement(OMElement parent) {
		if (parent == null)
			return;
		for (@SuppressWarnings("unchecked")
		Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();
			if (TYPE.equals(elName))
				type = XmlUtils.getStringContent(el);
		}
	}
	@Override
	public void parseElement(XMLStreamReader el) throws Exception {
		for (XMLStreamIterator iter = new XMLStreamIterator(el); iter.hasNext(); ) {
			String elName = el.getLocalName();
			if (TYPE.equals(elName))
				type = XmlUtils.getStringContent(el);
		}
	}
	
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, TYPE, type, ns);
	}
	
	public BirstXMLSerializable createNew() {
		return new AreaSeries();
	}

	public void addConfig(Element styles, String styleName, Element areaSeries) {
	}
	
	public void applyStyle(AreaSeries style) {
		if (style == null)
			return;
		type = style.type;
	}
	
	public String getType()
	{
		return type;
	}
	
}
