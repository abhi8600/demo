/**
 * 
 */
package com.successmetricsinc.chart.AnyChart;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.jdom.Element;

import com.successmetricsinc.util.BirstXMLSerializable;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class ThreeD extends BirstXMLSerializable {

	private static final String ELEVATION = "Elevation";
	private static final String ROTATION = "Rotation";
	private static final String ASPECT = "Aspect";
	private static final String ASPECT_MODE = "AspectMode";
	public static AspectMode DEFAULT_ASPECT_MODE = AspectMode.PercentOfClusterWidth;
	
	public enum AspectMode { PercentOfClusterWidth, PercentOfMinSide, PixelAspect };
	private AspectMode aspectMode = DEFAULT_ASPECT_MODE;
	private double aspect = 1.0;
	private int rotation = 90;
	private int elevation = 90;
	
	private static Map<AspectMode, String> aspectModeMap;
	
	static {
		aspectModeMap = new HashMap<AspectMode, String>();
		aspectModeMap.put(AspectMode.PercentOfClusterWidth, "PercentOfClusterWidth");
		aspectModeMap.put(AspectMode.PercentOfMinSide, "PercentOfMinSide");
		aspectModeMap.put(AspectMode.PixelAspect, "PixelAspect");
	}
	
	/* (non-Javadoc)
	 * @see com.successmetricsinc.util.BirstXMLSerializable#addContent(org.apache.axiom.om.OMFactory, org.apache.axiom.om.OMElement, org.apache.axiom.om.OMNamespace)
	 */
	@Override
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, ASPECT_MODE, aspectModeMap.get(aspectMode), ns);
		XmlUtils.addContent(fac, parent, ASPECT, String.valueOf(aspect), ns);
		XmlUtils.addContent(fac, parent, ROTATION, String.valueOf(rotation), ns);
		XmlUtils.addContent(fac, parent, ELEVATION, String.valueOf(elevation), ns);
	}

	/* (non-Javadoc)
	 * @see com.successmetricsinc.util.BirstXMLSerializable#createNew()
	 */
	@Override
	public BirstXMLSerializable createNew() {
		return new ThreeD();
	}

	/* (non-Javadoc)
	 * @see com.successmetricsinc.util.BirstXMLSerializable#parseElement(org.apache.axiom.om.OMElement)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void parseElement(OMElement parent) {
		for (@SuppressWarnings("unchecked")
				Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
					OMElement el = iter.next();
					String elName = el.getLocalName();

				if (ASPECT_MODE.equals(elName))
					aspectMode = parseTypeFromString(XmlUtils.getStringContent(el), aspectModeMap, DEFAULT_ASPECT_MODE);
				else if (ASPECT.equals(elName))
					aspect = XmlUtils.getDoubleContent(el);
				else if (ROTATION.equals(elName))
					rotation = XmlUtils.getIntContent(el);
				else if (ELEVATION.equals(elName))
					elevation = XmlUtils.getIntContent(el);
		}
	}

	/* (non-Javadoc)
	 * @see com.successmetricsinc.util.BirstXMLSerializable#parseElement(javax.xml.stream.XMLStreamReader)
	 */
	@Override
	public void parseElement(XMLStreamReader el) throws Exception {
		for (XMLStreamIterator iter = new XMLStreamIterator(el); iter.hasNext(); ) {
			String elName = el.getLocalName();
			if (ASPECT_MODE.equals(elName))
				aspectMode = parseTypeFromString(XmlUtils.getStringContent(el), aspectModeMap, DEFAULT_ASPECT_MODE);
			else if (ASPECT.equals(elName))
				aspect = XmlUtils.getDoubleContent(el);
			else if (ROTATION.equals(elName))
				rotation = XmlUtils.getIntContent(el);
			else if (ELEVATION.equals(elName))
				elevation = XmlUtils.getIntContent(el);
		}
	}
	
	public void getXML(Element parent) {
		parent.setAttribute("z_rotation", String.valueOf(rotation));
		parent.setAttribute("z_elevation", String.valueOf(elevation));
		parent.setAttribute("z_aspect", String.valueOf(aspect));
		parent.setAttribute("z_aspect_mode", aspectModeMap.get(aspectMode));
	}

	public void applyStyle(ThreeD style) {
		if (style == null)
			return;
		
		aspectMode = style.aspectMode;
		aspect = style.aspect;
		rotation = style.rotation;
		elevation = style.elevation;
	}

}
