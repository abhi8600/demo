/**
 * 
 */
package com.successmetricsinc.chart.AnyChart;

import java.awt.Font;
import java.util.Iterator;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.jdom.Element;

import com.successmetricsinc.chart.ChartOptions;
import com.successmetricsinc.util.BirstXMLSerializable;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class ChartSettings extends BirstXMLSerializable {

	private static final long serialVersionUID = 1L;
	private static final String TITLE_FONT = "TitleFont";
	private static final String CONTROLS = "Controls";
	private static final String TITLE_TEXT = "TitleText";
	private static final String TITLE_ENABLED = "TitleEnabled";

	private boolean titleEnabled = false;
	private String titleText;
	private Font titleFont;
	
	private Controls controls;

	public ChartSettings() {
	}
	
	public ChartSettings(ChartOptions co) {
		titleEnabled = (co.title != null);
		titleText = co.title;
		titleFont = co.getTitleFont();
		
		controls = new Controls(co);
	}

	@Override
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, TITLE_ENABLED, titleEnabled, ns);
		XmlUtils.addContent(fac, parent, TITLE_TEXT, titleText, ns);
		XmlUtils.addContent(fac, parent, CONTROLS, controls, ns);
		XmlUtils.addContent(fac, parent, TITLE_FONT, titleFont, ns);
	}

	@Override
	public BirstXMLSerializable createNew() {
		return new ChartSettings();
	}

	@Override
	public void parseElement(OMElement parent) {
		for (@SuppressWarnings("unchecked")
		Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();

			if (TITLE_ENABLED.equals(elName)) {
				titleEnabled = XmlUtils.getBooleanContent(el);
			}
			else if (TITLE_TEXT.equals(elName)) {
				titleText = XmlUtils.getStringContent(el);
			}
			else if (CONTROLS.equals(elName)) {
				controls = (Controls)XmlUtils.parseFirstChild(el, new Controls());
			}
			else if (TITLE_FONT.equals(elName)) {
				titleFont = XmlUtils.getFontContent(el);
			}
		}
	}

	public void getXML(Element settings) {
	}

	@Override
	public void parseElement(XMLStreamReader parser) throws Exception {
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
			String elName = parser.getLocalName();

			if (TITLE_ENABLED.equals(elName)) {
				titleEnabled = XmlUtils.getBooleanContent(parser);
			}
			else if (TITLE_TEXT.equals(elName)) {
				titleText = XmlUtils.getStringContent(parser);
			}
			else if (CONTROLS.equals(elName)) {
				controls = (Controls)XmlUtils.parseFirstChild(parser, new Controls());
			}
			else if (TITLE_FONT.equals(elName)) {
				titleFont = XmlUtils.getFontContent(parser);
			}
		}
		
	}

	public void applyStyle(ChartSettings style) {
		if (style == null)
			return;
		
		titleEnabled = style.titleEnabled;
		titleFont = style.titleFont;
		
		controls.applyStyle(style.controls);
	}
}
