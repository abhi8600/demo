/**
 * 
 */
package com.successmetricsinc.chart.AnyChart;

import java.util.Iterator;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.successmetricsinc.util.BirstXMLSerializable;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class TooltipSettings extends BirstXMLSerializable {

	private static final long serialVersionUID = 1L;
	private static final String FONT = "Font";
	private static final String FORMAT = "Format";
	private static final String ENABLED = "Enabled";

	// enabled=
	private boolean enabled;
	
	//<format>
	private String format;
	
	//<font>
	private Font font;

	public void parseElement(OMElement parent) {
		for (@SuppressWarnings("unchecked")
		Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();
		
			if (ENABLED.equals(elName)) {
				enabled = XmlUtils.getBooleanContent(el);
			}
			else if (FORMAT.equals(elName)) {
				format = XmlUtils.getStringContent(el);
			}
			else if (FONT.equals(elName)) {
				font = (Font) XmlUtils.parseFirstChild(el, new Font());
			}
		}
	}
	
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, ENABLED, enabled, ns);
		XmlUtils.addContent(fac, parent, FORMAT, format, ns);
		XmlUtils.addContent(fac, parent, FONT, font, ns);
	}

	@Override
	public BirstXMLSerializable createNew() {
		return new Font();
	}

	@Override
	public void parseElement(XMLStreamReader el) throws Exception {
		for (XMLStreamIterator iter = new XMLStreamIterator(el); iter.hasNext(); ) {
			String elName = el.getLocalName();
			
			if (ENABLED.equals(elName)) {
				enabled = XmlUtils.getBooleanContent(el);
			}
			else if (FORMAT.equals(elName)) {
				format = XmlUtils.getStringContent(el);
			}
			else if (FONT.equals(elName)) {
				font = (Font) XmlUtils.parseFirstChild(el, new Font());
			}
		}
	}
}
