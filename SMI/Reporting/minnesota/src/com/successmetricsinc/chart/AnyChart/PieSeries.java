package com.successmetricsinc.chart.AnyChart;

import java.util.Iterator;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.jdom.Element;

import com.successmetricsinc.chart.ChartOptions;
import com.successmetricsinc.util.BirstXMLSerializable;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

public class PieSeries extends BirstXMLSerializable {

	private static final long serialVersionUID = 1L;
	private static final String PIE_SORT = "PieSort";
	private static final String START_ANGLE = "PieStartAngle";
	private static final String INNER_RADIUS = "PieInnerRadius"; // only for Doughnut

	private String start_angle;
	private String inner_radius;
	private String sort; // None, Desc, Asc
	
	public PieSeries() {
		sort = "None";
		start_angle = "180";
		inner_radius = "50";
	}
	public PieSeries(ChartOptions co, OMElement defaultSettings) {
		parseElement(defaultSettings);
	}

	public PieSeries(OMElement defaultSettings) {
		parseElement(defaultSettings);
	}
	
	public void parseElement(OMElement parent) {
		if (parent == null)
			return;
		for (@SuppressWarnings("unchecked")
		Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();
			if (START_ANGLE.equals(elName)) {
				start_angle = XmlUtils.getStringContent(el);
			}
			else if (PIE_SORT.equals(elName)) {
				sort = XmlUtils.getStringContent(el);
			}
			else if (INNER_RADIUS.equals(elName)) {
				inner_radius = XmlUtils.getStringContent(el);
			}
		}
	}

	@Override
	public void parseElement(XMLStreamReader el) throws Exception {
		for (XMLStreamIterator iter = new XMLStreamIterator(el); iter.hasNext(); ) {
			String elName = el.getLocalName();
			if (START_ANGLE.equals(elName)) {
				start_angle = XmlUtils.getStringContent(el);
			}
			else if (PIE_SORT.equals(elName)) {
				sort = XmlUtils.getStringContent(el);
			}
			else if (INNER_RADIUS.equals(elName)) {
				inner_radius = XmlUtils.getStringContent(el);
			}
		}
	}
	
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, START_ANGLE, start_angle, ns);
		XmlUtils.addContent(fac, parent, PIE_SORT, sort, ns);
		XmlUtils.addContent(fac, parent, INNER_RADIUS, inner_radius, ns);
	}
	
	public void getXML(Element chart) {
		Element dataPlotSettings = XmlUtils.findOrCreateChild(chart, "data_plot_settings");
		Element pieSeriesEl = XmlUtils.findOrCreateChild(dataPlotSettings, "pie_series");
		if (start_angle != null)
			pieSeriesEl.setAttribute("start_angle", start_angle);
		if (inner_radius != null)
			pieSeriesEl.setAttribute("inner_radius", inner_radius);
		if (sort != null)
			pieSeriesEl.setAttribute("sort", sort);
	}
	
	public BirstXMLSerializable createNew() {
		return new PieSeries();
	}

	public void applyStyle(PieSeries style) {
		if (style == null)
			return;
		
		start_angle = style.start_angle;
		sort = style.sort;
		inner_radius = style.inner_radius;
	}
}
