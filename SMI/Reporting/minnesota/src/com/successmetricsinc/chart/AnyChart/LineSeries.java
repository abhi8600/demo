package com.successmetricsinc.chart.AnyChart;

import java.util.Iterator;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.jdom.Element;

import com.successmetricsinc.chart.ChartOptions;
import com.successmetricsinc.util.BirstXMLSerializable;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

public class LineSeries extends MarkerSettings {

	private static final long serialVersionUID = 1L;
	private static final int DEFAULT_LINE_MARKER_SIZE = 5;
	private static final String DEFAULT_LINE_TYPE = "Line";
	private static final String THICKNESS = "Thickness";
	private static final String TYPE = "Type";
	//<line_series><line_style><line thickness=
	private int thickness = 1;
	private String type = DEFAULT_LINE_TYPE; // Line, Spline, StepLineForward, StepLineBackward
	
	public LineSeries() {
		markerType = MarkerType.None;
		size = DEFAULT_LINE_MARKER_SIZE;
	}
	public LineSeries(ChartOptions co, OMElement defaultSettings) {
		parseElement(defaultSettings);
		thickness = co.lineWidth;
		markerType = MarkerType.None;
		size = DEFAULT_LINE_MARKER_SIZE;
	}

	public LineSeries(OMElement defaultSettings) {
		parseElement(defaultSettings);
	}
	
	public void parseElement(OMElement parent) {
		if (parent == null)
			return;
		for (@SuppressWarnings("unchecked")
		Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();
			
			if (THICKNESS.equals(elName)) {
				thickness = XmlUtils.getIntContent(el);
			}
			else if (MARKER_TYPE.equals(elName)) {
				markerType = parseMarkerType(XmlUtils.getStringContent(el));
			}
			else if (SIZE.equals(elName))
				size = XmlUtils.getIntContent(el);
			else if (TYPE.equals(elName))
				type = XmlUtils.getStringContent(el);
		}
	}
	@Override
	public void parseElement(XMLStreamReader el) throws Exception {
		for (XMLStreamIterator iter = new XMLStreamIterator(el); iter.hasNext(); ) {
			String elName = el.getLocalName();
			
			if (THICKNESS.equals(elName)) {
				thickness = XmlUtils.getIntContent(el);
			}
			else if (MARKER_TYPE.equals(elName)) {
				markerType = parseMarkerType(XmlUtils.getStringContent(el));
			}
			else if (SIZE.equals(elName))
				size = XmlUtils.getIntContent(el);
			else if (TYPE.equals(elName))
				type = XmlUtils.getStringContent(el);
		}
	}
	
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, THICKNESS, thickness, ns);
		XmlUtils.addContent(fac, parent, TYPE, type, ns);
		super.addContent(fac, parent, ns);
	}
	
	public BirstXMLSerializable createNew() {
		return new LineSeries();
	}

	public void addConfig(Element styles, String styleName, Element lineSeries) {
		Element lineStyleEl = null;
		
		if (styleName != null) {
			lineStyleEl = new Element("line_style");
			styles.addContent(lineStyleEl);
			lineStyleEl.setAttribute("name", styleName);
		}
		else
			lineStyleEl = XmlUtils.findOrCreateChild(styles, "line_style");
		
		Element lineEl = XmlUtils.findOrCreateChild(lineStyleEl, "line");
		if (thickness > 0)
			lineEl.setAttribute("thickness", String.valueOf(thickness));
		else
			lineEl.setAttribute("enabled", "False");
		
		Element markerStyleEl = new Element("marker_settings");
		lineSeries.addContent(markerStyleEl);
		markerStyleEl.setAttribute("enabled", "True");
		markerStyleEl.setAttribute("marker_type", markerTypeMap.get(markerType));
		Element markerEl = XmlUtils.findOrCreateChild(markerStyleEl, "marker");
		markerEl.setAttribute("enabled", "true");
		markerEl.setAttribute("size", String.valueOf(size));
		
		AnyChartNode.setHoverColor(lineStyleEl);
		AnyChartNode.setHoverColor(markerStyleEl);
	}
	
	public void applyStyle(LineSeries style) {
		if (style == null)
			return;
		
		super.applyStyle(style);
		thickness = style.thickness;
		type = style.type;
	}
	
	public String getType()
	{
		return type;
	}
	
}
