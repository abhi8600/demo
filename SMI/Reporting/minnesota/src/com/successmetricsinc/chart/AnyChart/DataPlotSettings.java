/**
 * 
 */
package com.successmetricsinc.chart.AnyChart;

import java.util.Iterator;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.successmetricsinc.adhoc.AdhocReport;
import com.successmetricsinc.chart.ChartOptions;
import com.successmetricsinc.util.BirstXMLSerializable;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class DataPlotSettings extends BirstXMLSerializable {
	private static final long serialVersionUID = 1L;
	private static final String GAUGE_SERIES = "GaugeSeries";
	private static final String NEW_GAUGE_SERIES = "NewGaugeSeries";
	private static final String ALLOW_INTERACTIVITY_SELECT = "AllowInteractivitySelect";
	private static final String FUNNEL_SERIES = "FunnelSeries";
	private static final String MAP_SERIES = "MapSeries";
	private static final String PIE_SERIES = "PieSeries";
	private static final String LINE_SERIES = "LineSeries";
	private static final String BUBBLE_SERIES = "BubbleSeries";
	private static final String BAR_SERIES = "BarSeries";
	private static final String RADAR_SERIES = "RadarSeries";
	private static final String AREA_SERIES = "AreaSeries";
	private static final String RANGE_BAR_SERIES = "RangeBarSeries";

	private static final String MARKER_SETTINGS = "MarkerSettings";

	//<marker_settings>
	protected MarkerSettings markerSettings;
	
	// <bar_series>
	private BarSeries barSeries;
	
	// <bubble_series>
	private BubbleSeries bubbleSeries;
	
	// <candlestick_series>
	//private CandlestickSeries candlestickSeries;
	
	//<line_series>
	private LineSeries lineSeries;
	
	//<marker_series>
	//private MarkerSeries markerSeries;
	
	//<ohlc_series>
	//private OhlcSeries ohlcSeries;
	
	// <pie_series>
	private PieSeries pieSeries;
	
	// <range_area_series>
//	private RangeAreaSeries rangeAreaSeries;
	
	//<range_bar_series>
//	private RangeBarSeries rangeBarSeries;
	
	//<map_series>
	private MapSeries mapSeries;
	
	//<connector_series>
	//private ConnectorSeries connectorSeries;
	
	//<funnel_series>
	private FunnelSeries funnelSeries;
	
	private RadarSeries radarSeries;
	
	private GaugeSeries gaugeSeries;
	
	private NewGaugeSeries newGaugeSeries;
	
	private AreaSeries areaSeries;
	
	private RangeBarSeries rangeBarSeries;
	
	private boolean interactivityAllowSelect = false;
	
	public DataPlotSettings() {
		
	}

	public DataPlotSettings(OMElement defaults) {
		if (defaults!=null)
		{
			markerSettings = new MarkerSettings(defaults.getFirstChildWithName(new QName(MARKER_SETTINGS)));
			barSeries = new BarSeries(defaults.getFirstChildWithName(new QName(BAR_SERIES)));
			lineSeries = new LineSeries(defaults.getFirstChildWithName(new QName(LINE_SERIES)));
			mapSeries = new MapSeries(defaults.getFirstChildWithName(new QName(MAP_SERIES)));
			funnelSeries = new FunnelSeries(defaults.getFirstChildWithName(new QName(FUNNEL_SERIES)));
			gaugeSeries = new GaugeSeries(defaults.getFirstChildWithName(new QName(GAUGE_SERIES)));
			newGaugeSeries = new NewGaugeSeries(defaults.getFirstChildWithName(new QName(NEW_GAUGE_SERIES)));		
			radarSeries = new RadarSeries(defaults.getFirstChildWithName(new QName(RADAR_SERIES)));
			areaSeries = new AreaSeries(defaults.getFirstChildWithName(new QName(AREA_SERIES)));
			pieSeries = new PieSeries(defaults.getFirstChildWithName(new QName(PIE_SERIES)));
			rangeBarSeries = new RangeBarSeries(defaults.getFirstChildWithName(new QName(RANGE_BAR_SERIES)));
			bubbleSeries = new BubbleSeries(defaults.getFirstChildWithName(new QName(BUBBLE_SERIES)));
		}
	}
	
	public DataPlotSettings(ChartOptions co, AdhocReport report, OMElement defaults) {
		markerSettings = new MarkerSettings(defaults == null ? null : defaults.getFirstChildWithName(new QName(MARKER_SETTINGS)));
		barSeries = new BarSeries(co, defaults == null ? null : defaults.getFirstChildWithName(new QName(BAR_SERIES)));
		lineSeries = new LineSeries(co, defaults == null ? null : defaults.getFirstChildWithName(new QName(LINE_SERIES)));
		mapSeries = new MapSeries(co, report, defaults == null ? null : defaults.getFirstChildWithName(new QName(MAP_SERIES)));
		funnelSeries = new FunnelSeries(co, defaults == null ? null : defaults.getFirstChildWithName(new QName(FUNNEL_SERIES)));
		gaugeSeries = new GaugeSeries(co, defaults == null ? null : defaults.getFirstChildWithName(new QName(GAUGE_SERIES)));
		newGaugeSeries = new NewGaugeSeries(co, defaults == null ? null : defaults.getFirstChildWithName(new QName(NEW_GAUGE_SERIES)));		
		radarSeries = new RadarSeries(co, defaults == null ? null : defaults.getFirstChildWithName(new QName(RADAR_SERIES)));
		areaSeries = new AreaSeries(co, defaults == null ? null : defaults.getFirstChildWithName(new QName(AREA_SERIES)));
		pieSeries = new PieSeries(co, defaults == null ? null : defaults.getFirstChildWithName(new QName(PIE_SERIES)));
		rangeBarSeries = new RangeBarSeries(co, defaults == null ? null : defaults.getFirstChildWithName(new QName(RANGE_BAR_SERIES)));
		bubbleSeries = new BubbleSeries(co, defaults == null ? null : defaults.getFirstChildWithName(new QName(BUBBLE_SERIES)));
	}

	@SuppressWarnings("unchecked")
	public void parseElement(OMElement parent) {
		for (Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();
			
			if (BAR_SERIES.equals(elName)) {
				if (barSeries == null)
					barSeries = new BarSeries();
				barSeries.parseElement(el);
			}
			else if (BUBBLE_SERIES.equals(elName)) {
				if (bubbleSeries == null)
					bubbleSeries = new BubbleSeries();
				bubbleSeries.parseElement(el);
			}
			else if (LINE_SERIES.equals(elName)) {
				if (lineSeries == null)
					lineSeries = new LineSeries();
				lineSeries.parseElement(el);
			}
			else if (PIE_SERIES.equals(elName)) {
				if (pieSeries == null)
					pieSeries = new PieSeries();
				pieSeries.parseElement(el);
			}
			else if (MAP_SERIES.equals(elName)) {
				if (mapSeries == null)
					mapSeries = new MapSeries();
				mapSeries.parseElement(el);
			}
			else if (FUNNEL_SERIES.equals(elName)) {
				if (funnelSeries == null)
					funnelSeries = new FunnelSeries();
				funnelSeries.parseElement(el);
			}
			else if (GAUGE_SERIES.equals(elName)) {
				if (gaugeSeries == null)
					gaugeSeries = new GaugeSeries();
				gaugeSeries.parseElement(el);
			}
			else if (NEW_GAUGE_SERIES.equals(elName)) {
				if (newGaugeSeries == null)
					newGaugeSeries = new NewGaugeSeries();
				newGaugeSeries.parseElement(el);
			}			
			else if (RADAR_SERIES.equals(elName)) {
				if (radarSeries == null)
					radarSeries = new RadarSeries();
				radarSeries.parseElement(el);
			}			
			else if (AREA_SERIES.equals(elName)) {
				if (areaSeries == null)
					areaSeries = new AreaSeries();
				areaSeries.parseElement(el);
			}
			else if (RANGE_BAR_SERIES.equals(elName)) {
				if (rangeBarSeries == null)
					rangeBarSeries = new RangeBarSeries();
				rangeBarSeries.parseElement(el);
			}			
			else if (ALLOW_INTERACTIVITY_SELECT.equals(elName)) {
				interactivityAllowSelect = XmlUtils.getBooleanContent(el);
			}
			else if (MARKER_SETTINGS.equals(elName)) {
				markerSettings = (MarkerSettings) XmlUtils.parseFirstChild(el, new MarkerSettings());
			}
		}
	}
	
	@Override
	public void parseElement(XMLStreamReader el) throws Exception {
		for (XMLStreamIterator iter = new XMLStreamIterator(el); iter.hasNext(); ) {
			String elName = el.getLocalName();
			
			if (BAR_SERIES.equals(elName)) {
				if (barSeries == null)
					barSeries = new BarSeries();
				barSeries.parseElement(el);
			}
			else if (BUBBLE_SERIES.equals(elName)) {
				if (bubbleSeries == null)
					bubbleSeries = new BubbleSeries();
				bubbleSeries.parseElement(el);
			}
			else if (LINE_SERIES.equals(elName)) {
				if (lineSeries == null)
					lineSeries = new LineSeries();
				lineSeries.parseElement(el);
			}
			else if (PIE_SERIES.equals(elName)) {
				if (pieSeries == null)
					pieSeries = new PieSeries();
				pieSeries.parseElement(el);
			}
			else if (MAP_SERIES.equals(elName)) {
				if (mapSeries == null)
					mapSeries = new MapSeries();
				mapSeries.parseElement(el);
			}
			else if (FUNNEL_SERIES.equals(elName)) {
				if (funnelSeries == null)
					funnelSeries = new FunnelSeries();
				funnelSeries.parseElement(el);
			}
			else if (GAUGE_SERIES.equals(elName)) {
				if (gaugeSeries == null)
					gaugeSeries = new GaugeSeries();
				gaugeSeries.parseElement(el);
			}
			else if (NEW_GAUGE_SERIES.equals(elName)) {
				if (newGaugeSeries == null)
					newGaugeSeries = new NewGaugeSeries();
				newGaugeSeries.parseElement(el);
			}			
			else if (RADAR_SERIES.equals(elName)) {
				if (radarSeries == null)
					radarSeries = new RadarSeries();
				radarSeries.parseElement(el);
			}			
			else if (AREA_SERIES.equals(elName)) {
				if (areaSeries == null)
					areaSeries = new AreaSeries();
				areaSeries.parseElement(el);
			}	
			else if (RANGE_BAR_SERIES.equals(elName)) {
				if (rangeBarSeries == null)
					rangeBarSeries = new RangeBarSeries();
				rangeBarSeries.parseElement(el);
			}			
			else if (ALLOW_INTERACTIVITY_SELECT.equals(elName)) {
				interactivityAllowSelect = XmlUtils.getBooleanContent(el);
			}
			else if (MARKER_SETTINGS.equals(elName)) {
				markerSettings = (MarkerSettings) XmlUtils.parseFirstChild(el, new MarkerSettings());
			}
		}
	}

	@Override
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, BAR_SERIES, barSeries, ns);
		XmlUtils.addContent(fac, parent, BUBBLE_SERIES, bubbleSeries, ns);
		XmlUtils.addContent(fac, parent, LINE_SERIES, lineSeries, ns);
		XmlUtils.addContent(fac, parent, PIE_SERIES, pieSeries, ns);
		XmlUtils.addContent(fac, parent, MAP_SERIES, mapSeries, ns);
		XmlUtils.addContent(fac, parent, FUNNEL_SERIES, funnelSeries, ns);
		XmlUtils.addContent(fac, parent, RADAR_SERIES, radarSeries, ns);		
		XmlUtils.addContent(fac, parent, GAUGE_SERIES, gaugeSeries, ns);
		XmlUtils.addContent(fac, parent, NEW_GAUGE_SERIES, newGaugeSeries, ns);		
		XmlUtils.addContent(fac, parent, AREA_SERIES, areaSeries, ns);	
		XmlUtils.addContent(fac, parent, RANGE_BAR_SERIES, rangeBarSeries, ns);			
		XmlUtils.addContent(fac, parent, ALLOW_INTERACTIVITY_SELECT, interactivityAllowSelect, ns);
		XmlUtils.addContent(fac, parent, MARKER_SETTINGS, markerSettings, ns);
	}

	@Override
	public BirstXMLSerializable createNew() {
		return new DataPlotSettings();
	}

	/**
	 * @return the barSeries
	 */
	public BarSeries getBarSeries() {
		return barSeries;
	}

	/**
	 * @return the bubbleSeries
	 */
	public BubbleSeries getBubbleSeries() {
		return bubbleSeries;
	}

	/**
	 * @return the lineSeries
	 */
	public LineSeries getLineSeries() {
		return lineSeries;
	}

	/**
	 * @return the pieSeries
	 */
	public PieSeries getPieSeries() {
		return pieSeries;
	}

	/**
	 * @return the mapSeries
	 */
	public MapSeries getMapSeries() {
		return mapSeries;
	}

	/**
	 * @return the funnelSeries
	 */
	public FunnelSeries getFunnelSeries() {
		return funnelSeries;
	}
	
	public RadarSeries getRadarSeries() {
		return radarSeries;
	}

	public AreaSeries getAreaSeries() {
		return areaSeries;
	}
	
	public RangeBarSeries getRangeBarSeries() {
		return rangeBarSeries;
	}

	public GaugeSeries getGaugeSeries() {
		return gaugeSeries;
	}
	
	public NewGaugeSeries getNewGaugeSeries() {
		return newGaugeSeries;
	}
	
	/**
	 * @return the interactivityAllowSelect
	 */
	public boolean isInteractivityAllowSelect() {
		return interactivityAllowSelect;
	}

	public void applyStyle(DataPlotSettings style) {
		if (style == null)
			return;
		interactivityAllowSelect = style.interactivityAllowSelect;
		if (markerSettings != null)
			markerSettings.applyStyle(style.markerSettings);
		if (barSeries != null)
			barSeries.applyStyle(style.barSeries);
		if (bubbleSeries != null)
			bubbleSeries.applyStyle(style.bubbleSeries);
		if (lineSeries != null)
			lineSeries.applyStyle(style.lineSeries);
		if (pieSeries != null)
			pieSeries.applyStyle(style.pieSeries);
		if (mapSeries != null)
			mapSeries.applyStyle(style.mapSeries);
		if (funnelSeries != null)
			funnelSeries.applyStyle(style.funnelSeries);
		if (radarSeries != null)
			radarSeries.applyStyle(style.radarSeries);
		if (areaSeries != null)
			areaSeries.applyStyle(style.areaSeries);
		if (rangeBarSeries != null)
			rangeBarSeries.applyStyle(style.rangeBarSeries);		
		if (gaugeSeries != null)
			gaugeSeries.applyStyle(style.gaugeSeries);
		if (newGaugeSeries != null)
			newGaugeSeries.applyStyle(style.newGaugeSeries);
	}
}
