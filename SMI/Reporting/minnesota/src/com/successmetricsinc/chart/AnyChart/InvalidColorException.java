/**
 * 
 */
package com.successmetricsinc.chart.AnyChart;

import com.successmetricsinc.util.BaseException;

/**
 * @author agarrison
 *
 */
@SuppressWarnings("serial")
public class InvalidColorException extends BaseException {
	
	public InvalidColorException(String s, Throwable cause)
	{
		super(s, cause);
	}
	
	public InvalidColorException(String s)
	{
		super(s);
	}

	@Override
	public int getErrorCode() {
		return ERROR_INVALID_COLOR;
	}

}
