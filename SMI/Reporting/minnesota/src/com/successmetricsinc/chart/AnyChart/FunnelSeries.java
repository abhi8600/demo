package com.successmetricsinc.chart.AnyChart;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.successmetricsinc.chart.ChartOptions;
import com.successmetricsinc.util.BirstXMLSerializable;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

public class FunnelSeries extends BirstXMLSerializable {

	private static final long serialVersionUID = 1L;
	private static final String PADDING = "Padding";
	private static final String FUNNEL_MODE = "FunnelMode";
	private static final String INVERTED = "Inverted";
	private static final FunnelMode DEFAULT_FUNNEL_MODE = FunnelMode.Square;
	//inverted=
	private boolean inverted = true;
	
	//mode=
	public enum FunnelMode { Square, Circular };
	
	private FunnelMode mode = DEFAULT_FUNNEL_MODE;
	
	//padding=
	private double padding = 0.05;
	
	private static Map<FunnelMode, String> funnelModeMap = new HashMap<FunnelMode, String>();
	
	static {
		funnelModeMap.put(FunnelMode.Circular, "Circular");
		funnelModeMap.put(FunnelMode.Square, "Square");
	}
	
	public FunnelSeries() {
		
	}
	
	public FunnelSeries(ChartOptions co, OMElement defaultSettings) {
		parseElement(defaultSettings);
		mode = ("3D".equals(co.gradient) ? FunnelMode.Circular : FunnelMode.Square);
		padding = co.gapWidth / 2;
	}

	public FunnelSeries(OMElement defaultSettings) {
		parseElement(defaultSettings);
	}

	public void parseElement(OMElement parent) {
		if (parent == null)
			return;
		for (@SuppressWarnings("unchecked")
		Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();
			
			if (INVERTED.equals(elName)) {
				inverted = XmlUtils.getBooleanContent(el);
			}
			else if (FUNNEL_MODE.equals(elName)) {
				mode = parseFunnelMode(XmlUtils.getStringContent(el));
			}
			else if (PADDING.equals(elName)) {
				padding = XmlUtils.getDoubleContent(el);
			}
		}
	}
	
	@Override
	public void parseElement(XMLStreamReader el) throws Exception {
		for (XMLStreamIterator iter = new XMLStreamIterator(el); iter.hasNext(); ) {
			String elName = el.getLocalName();
			
			if (INVERTED.equals(elName)) {
				inverted = XmlUtils.getBooleanContent(el);
			}
			else if (FUNNEL_MODE.equals(elName)) {
				mode = parseFunnelMode(XmlUtils.getStringContent(el));
			}
			else if (PADDING.equals(elName)) {
				padding = XmlUtils.getDoubleContent(el);
			}
		}
	}
	
	private FunnelMode parseFunnelMode(String stringContent) {
		for (FunnelMode fm : funnelModeMap.keySet()) {
			if (funnelModeMap.get(fm).equals(stringContent))
				return fm;
		}
		return DEFAULT_FUNNEL_MODE;
	}

	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, INVERTED, inverted, ns);
		XmlUtils.addContent(fac, parent, FUNNEL_MODE, funnelModeMap.get(mode), ns);
		XmlUtils.addContent(fac, parent, PADDING, padding, ns);
	}
	
	public BirstXMLSerializable createNew() {
		return new FunnelSeries();
	}
	
	public String getFunnelMode()
	{
		return funnelModeMap.get(mode);
	}

	public void applyStyle(FunnelSeries style) {
		if (style == null)
			return;
		
		inverted = style.inverted;
		mode = style.mode;
		padding = style.padding;
	}

}
