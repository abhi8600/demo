/**
 * 
 */
package com.successmetricsinc.chart.AnyChart;

import java.awt.Color;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.jdom.Element;

import com.successmetricsinc.adhoc.AdhocChart;
import com.successmetricsinc.chart.ChartOptions;
import com.successmetricsinc.util.BirstXMLSerializable;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class Background extends BirstXMLSerializable {

	private static final long serialVersionUID = 1L;
	private static final String PLOT_OPACITY = "PlotOpacity";
	private static final String BACKGROUND_OPACITY = "BackgroundOpacity";
	private static final String PLOT_FILL_COLOR = "PlotFillColor";
	private static final String BORDER_COLOR = "BorderColor";
	private static final String FILL_COLOR = "FillColor";
	private static final String INSIDE_MARGIN_BOTTOM = "InsideMarginBottom";
	private static final String INSIDE_MARGIN_RIGHT = "InsideMarginRight";
	private static final String INSIDE_MARGIN_TOP = "InsideMarginTop";
	private static final String INSIDE_MARGIN_LEFT = "InsideMarginLeft";
	private static final String CORNER_TYPE = "CornerType";
	private static final String BORDER_JOINS = "BorderJoins";
	private static final String BORDER_THICKNESS = "BorderThickness";
	private static final String BORDER_ENABLED = "BorderEnabled";
	private static final String ENABLED = "Enabled";
	private static final BorderJoins DEFAULT_BORDER_JOINS = BorderJoins.Round;
	private static final CornerType DEFAULT_CORNER_TYPE = CornerType.Square;
	// enabled=
	private boolean enabled;
	
	private boolean borderEnabled = false;
	private int borderThickness = 1;
	private Color borderColor = Color.BLACK;
	
	public enum BorderJoins {Miter, Round, Bevel};
	private BorderJoins borderJoints = DEFAULT_BORDER_JOINS;
	
	public enum CornerType { Square, Cut, Rounded, RoundedInner };
	private CornerType cornerType = DEFAULT_CORNER_TYPE;
	
	private static Map<BorderJoins, String> borderJoinsMap = new HashMap<BorderJoins, String>();
	private static Map<CornerType, String> cornerTypeMap = new HashMap<CornerType, String>();
	
	static {
		borderJoinsMap.put(BorderJoins.Miter, "Miter");
		borderJoinsMap.put(BorderJoins.Round, "Round");
		borderJoinsMap.put(BorderJoins.Bevel, "Bevel");
		
		cornerTypeMap.put(CornerType.Square, "Square");
		cornerTypeMap.put(CornerType.Cut, "Cut");
		cornerTypeMap.put(CornerType.Rounded, "Rounded");
		cornerTypeMap.put(CornerType.RoundedInner, "RoundedInner");
	}
	
	private int leftMargin = 0;
	private int topMargin = 0;
	private int rightMargin = 0;
	private int bottomMargin = 0;
	
	private Color fillColor = Color.WHITE;
	private Color plotFillColor = Color.WHITE;
	
	private double backgroundOpacity = 0;
	private double plotOpacity = 0;

	public Background(){
		
	}
	public Background(ChartOptions co) {
		enabled = (co.plotBackgroundColor != null);
		borderEnabled = !co.noborder;
		borderColor = new Color(Integer.valueOf(co.borderColor, 16));
		if (borderEnabled) {
			leftMargin = rightMargin = topMargin = bottomMargin = 5;
		}
		fillColor = new Color(Integer.valueOf(co.chartBackgroundColor, 16));
		backgroundOpacity = co.chartBackgroundOpacity;
		plotFillColor = new Color(Integer.valueOf(co.plotBackgroundColor, 16));
		plotOpacity = co.plotBackgroundOpacity;
	}

	public void parseElement(OMElement parent) {
		for (@SuppressWarnings("unchecked")
		Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();
			
			if (ENABLED.equals(elName)) {
				enabled = XmlUtils.getBooleanContent(el);
			}
			else if (BORDER_ENABLED.equals(elName)) {
				borderEnabled = XmlUtils.getBooleanContent(el);
			}
			else if (BORDER_THICKNESS.equals(elName)) {
				borderThickness = XmlUtils.getIntContent(el);
			}
			else if (BORDER_COLOR.equals(elName)) {
				borderColor = XmlUtils.getColorContent(el);
			}
			else if (BORDER_JOINS.equals(elName)) {
				borderJoints = BirstXMLSerializable.parseTypeFromString(XmlUtils.getStringContent(el), borderJoinsMap, DEFAULT_BORDER_JOINS);
			}
			else if (CORNER_TYPE.equals(elName)) {
				cornerType = parseTypeFromString(XmlUtils.getStringContent(el), cornerTypeMap, DEFAULT_CORNER_TYPE);
			}
			else if (INSIDE_MARGIN_LEFT.equals(elName)) {
				leftMargin = XmlUtils.getIntContent(el);
			}
			else if (INSIDE_MARGIN_TOP.equals(elName)) {
				topMargin = XmlUtils.getIntContent(el);
			}
			else if (INSIDE_MARGIN_RIGHT.equals(elName)) {
				rightMargin = XmlUtils.getIntContent(el);
			}
			else if (INSIDE_MARGIN_BOTTOM.equals(elName)) {
				bottomMargin = XmlUtils.getIntContent(el);
			}
			else if (FILL_COLOR.equals(elName)) {
				fillColor = XmlUtils.getColorContent(el);
			}
			else if (PLOT_FILL_COLOR.equals(elName))
				plotFillColor = XmlUtils.getColorContent(el);
			else if (BACKGROUND_OPACITY.equals(elName))
				backgroundOpacity = XmlUtils.getDoubleContent(el);
			else if (PLOT_OPACITY.equals(elName))
				plotOpacity = XmlUtils.getDoubleContent(el);
		}
	}
	
	@Override
	public void parseElement(XMLStreamReader el) throws Exception {
		for (XMLStreamIterator iter = new XMLStreamIterator(el); iter.hasNext(); ){
			String elName = el.getLocalName();
			
			if (ENABLED.equals(elName)) {
				enabled = XmlUtils.getBooleanContent(el);
			}
			else if (BORDER_ENABLED.equals(elName)) {
				borderEnabled = XmlUtils.getBooleanContent(el);
			}
			else if (BORDER_THICKNESS.equals(elName)) {
				borderThickness = XmlUtils.getIntContent(el);
			}
			else if (BORDER_COLOR.equals(elName)) {
				borderColor = XmlUtils.getColorContent(el);
			}
			else if (BORDER_JOINS.equals(elName)) {
				borderJoints = BirstXMLSerializable.parseTypeFromString(XmlUtils.getStringContent(el), borderJoinsMap, DEFAULT_BORDER_JOINS);
			}
			else if (CORNER_TYPE.equals(elName)) {
				cornerType = parseTypeFromString(XmlUtils.getStringContent(el), cornerTypeMap, DEFAULT_CORNER_TYPE);
			}
			else if (INSIDE_MARGIN_LEFT.equals(elName)) {
				leftMargin = XmlUtils.getIntContent(el);
			}
			else if (INSIDE_MARGIN_TOP.equals(elName)) {
				topMargin = XmlUtils.getIntContent(el);
			}
			else if (INSIDE_MARGIN_RIGHT.equals(elName)) {
				rightMargin = XmlUtils.getIntContent(el);
			}
			else if (INSIDE_MARGIN_BOTTOM.equals(elName)) {
				bottomMargin = XmlUtils.getIntContent(el);
			}
			else if (FILL_COLOR.equals(elName)) {
				fillColor = XmlUtils.getColorContent(el);
			}
			else if (PLOT_FILL_COLOR.equals(elName))
				plotFillColor = XmlUtils.getColorContent(el);
			else if (BACKGROUND_OPACITY.equals(elName))
				backgroundOpacity = XmlUtils.getDoubleContent(el);
			else if (PLOT_OPACITY.equals(elName))
				plotOpacity = XmlUtils.getDoubleContent(el);
		}
	}
	

	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, ENABLED, enabled, ns);
		XmlUtils.addContent(fac, parent, BORDER_ENABLED, borderEnabled, ns);
		XmlUtils.addContent(fac, parent, BORDER_THICKNESS, borderThickness, ns);
		XmlUtils.addContent(fac, parent, BORDER_JOINS, borderJoinsMap.get(borderJoints), ns);
		XmlUtils.addContent(fac, parent, BORDER_COLOR, borderColor, ns);
		XmlUtils.addContent(fac, parent, CORNER_TYPE, cornerTypeMap.get(cornerType), ns);
		XmlUtils.addContent(fac, parent, INSIDE_MARGIN_LEFT, leftMargin, ns);
		XmlUtils.addContent(fac, parent, INSIDE_MARGIN_TOP, topMargin, ns);
		XmlUtils.addContent(fac, parent, INSIDE_MARGIN_RIGHT, rightMargin, ns);
		XmlUtils.addContent(fac, parent, INSIDE_MARGIN_BOTTOM, bottomMargin, ns);
		XmlUtils.addContent(fac, parent, FILL_COLOR, fillColor, ns);
		XmlUtils.addContent(fac, parent, PLOT_FILL_COLOR, plotFillColor, ns);
		XmlUtils.addContent(fac, parent, BACKGROUND_OPACITY, backgroundOpacity, ns);
		XmlUtils.addContent(fac, parent, PLOT_OPACITY, plotOpacity, ns);
	}

	@Override
	public BirstXMLSerializable createNew() {
		return new Background();
	}

	public void getXML(Element chartSettings, boolean showBackground, boolean showBorder) {
		Element chartBackground = XmlUtils.findOrCreateChild(chartSettings, "chart_background");
		Element border = XmlUtils.findOrCreateChild(chartBackground, AdhocChart.ANYCHART_BORDER);
		chartBackground.setAttribute(AdhocChart.ANYCHART_ENABLED, "True"); //showBackground ? AdhocChart.ANYCHART_TRUE : AdhocChart.ANYCHART_FALSE);
		border.setAttribute(AdhocChart.ANYCHART_ENABLED, showBorder ? AdhocChart.ANYCHART_TRUE : AdhocChart.ANYCHART_FALSE);
				
		if (showBorder) {
			border.setAttribute("thickness", String.valueOf(borderThickness));
			border.setAttribute("joints", borderJoinsMap.get(borderJoints));
			XmlUtils.addColorAttribute(border, "color", borderColor);
			leftMargin = Math.max(leftMargin, borderThickness);
			rightMargin = Math.max(rightMargin, borderThickness);
			topMargin = Math.max(topMargin, borderThickness);
			bottomMargin = Math.max(bottomMargin, borderThickness);
		}
		Element bfill = XmlUtils.findOrCreateChild(chartBackground, "fill");
		bfill.setAttribute("enabled", "False");
		
		Element dataPlotBackground = XmlUtils.findOrCreateChild(chartSettings, "data_plot_background");
		dataPlotBackground.setAttribute(AdhocChart.ANYCHART_ENABLED, showBackground ? AdhocChart.ANYCHART_TRUE : AdhocChart.ANYCHART_FALSE);
		
		// currently don't allow control over data plot border
		Element dpborder = XmlUtils.findOrCreateChild(dataPlotBackground, "border");
		dpborder.setAttribute("enabled", AdhocChart.ANYCHART_FALSE);
		
		if (showBackground) {
			dataPlotBackground.setAttribute(AdhocChart.ANYCHART_ENABLED, showBackground ? AdhocChart.ANYCHART_TRUE : AdhocChart.ANYCHART_FALSE);
			if (fillColor != null && backgroundOpacity >= 0.0) {
				Element fill = XmlUtils.findOrCreateChild(chartBackground, "fill");
				XmlUtils.addColorAttribute(fill, "color", fillColor);
				fill.setAttribute("type", "Solid");
				fill.setAttribute("opacity", String.valueOf(backgroundOpacity));
				fill.setAttribute("enabled", AdhocChart.ANYCHART_TRUE);
			}
			if (plotFillColor != null && plotOpacity >= 0.0) {
				Element fill = XmlUtils.findOrCreateChild(dataPlotBackground, "fill");
				XmlUtils.addColorAttribute(fill, "color", plotFillColor);
				fill.setAttribute("type", "Solid");
				fill.setAttribute("opacity", String.valueOf(plotOpacity));
				fill.setAttribute("enabled", AdhocChart.ANYCHART_TRUE);
			}
		}
		Element corners = XmlUtils.findOrCreateChild(chartBackground, "corners");
		corners.setAttribute("type", cornerTypeMap.get(cornerType));
		
		Element insideMargin = XmlUtils.findOrCreateChild(chartBackground, "inside_margin");
		insideMargin.setAttribute("left", String.valueOf(leftMargin));
		insideMargin.setAttribute("top", String.valueOf(topMargin));
		insideMargin.setAttribute("right", String.valueOf(rightMargin));
		insideMargin.setAttribute("bottom", String.valueOf(bottomMargin));
	}

	/**
	 * @return the fillColor
	 */
	public Color getFillColor() {
		return fillColor;
	}

	/**
	 * @param fillColor the fillColor to set
	 */
	public void setFillColor(Color fillColor) {
		this.fillColor = fillColor;
	}
	public void applyStyle(Background style) {
		if (style == null)
			return;
		
		enabled = style.enabled;
		borderEnabled = style.borderEnabled;
		borderThickness = style.borderThickness;
		borderColor = style.borderColor;
		borderJoints = style.borderJoints;
		cornerType = style.cornerType;
		leftMargin = style.leftMargin;
		topMargin = style.topMargin;
		rightMargin = style.rightMargin;
		bottomMargin = style.bottomMargin;
		fillColor = style.fillColor;
		plotFillColor = style.plotFillColor;
		backgroundOpacity = style.backgroundOpacity;
		plotOpacity = style.plotOpacity;
	}
}
