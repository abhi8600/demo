package com.successmetricsinc.chart.AnyChart;

import java.util.Iterator;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.jdom.Element;

import com.successmetricsinc.chart.ChartOptions;
import com.successmetricsinc.util.BirstXMLSerializable;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

public class RadarSeries extends BirstXMLSerializable {

	private static final long serialVersionUID = 1L;
	private static final String STARTANGLE = "StartAngle";
	private static final String USEPOLARCOORDS = "UsePolarCoords";
	private static final String DRAWINGSTYLE = "DrawingStyle";
	private static final String DISPLAYDATAMODE = "DisplayDataMode";
	private static final String CLOSECONTOUR = "CloseContour";
	private static final String AREASERIES = "AreaSeries";
	private static final String CIRCULARLABELSSTYLE = "CircularLabelsStyle";
	
	private String startAngle;
	private boolean usePolarCoords;
	private String drawingStyle;
	private String displayDataMode;
	private boolean closeContour = true;
	private boolean areaSeries = false;
	private String circularLabelsStyle;

	public RadarSeries() {
	}
	
	public RadarSeries(ChartOptions co, OMElement defaultSettings) {
		parseElement(defaultSettings);
	}

	public RadarSeries(OMElement defaultSettings) {
		parseElement(defaultSettings);
	}

	public void parseElement(OMElement parent) {
		if (parent == null)
			return;
		for (@SuppressWarnings("unchecked")
		Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();
			
			if (STARTANGLE.equals(elName)) {
				startAngle = XmlUtils.getStringContent(el);
			}
			else if (USEPOLARCOORDS.equals(elName)) {
				usePolarCoords = XmlUtils.getBooleanContent(el);
			}
			else if (DRAWINGSTYLE.equals(elName)) {
				drawingStyle = XmlUtils.getStringContent(el);
			}
			else if (DISPLAYDATAMODE.equals(elName)) {
				displayDataMode = XmlUtils.getStringContent(el);
			}
			else if (CLOSECONTOUR.equals(elName)) {
					closeContour = XmlUtils.getBooleanContent(el);
			}	
			else if (AREASERIES.equals(elName)) {
				areaSeries = XmlUtils.getBooleanContent(el);
			}
			else if (CIRCULARLABELSSTYLE.equals(elName)) {
				circularLabelsStyle = XmlUtils.getStringContent(el);
			}
		}
	}
	
	@Override
	public void parseElement(XMLStreamReader el) throws Exception {
		for (XMLStreamIterator iter = new XMLStreamIterator(el); iter.hasNext(); ) {
			String elName = el.getLocalName();
			
			if (STARTANGLE.equals(elName)) {
				startAngle = XmlUtils.getStringContent(el);
			}
			else if (USEPOLARCOORDS.equals(elName)) {
				usePolarCoords = XmlUtils.getBooleanContent(el);
			}
			else if (DRAWINGSTYLE.equals(elName)) {
				drawingStyle = XmlUtils.getStringContent(el);
			}
			else if (DISPLAYDATAMODE.equals(elName)) {
				displayDataMode = XmlUtils.getStringContent(el);
			}
			else if (CLOSECONTOUR.equals(elName)) {
				closeContour = XmlUtils.getBooleanContent(el);
			}
			else if (AREASERIES.equals(elName)) {
				areaSeries = XmlUtils.getBooleanContent(el);
			}
			else if (CIRCULARLABELSSTYLE.equals(elName)) {
				circularLabelsStyle = XmlUtils.getStringContent(el);
			}			
		}
	}
	
	public void getXML(Element chart) {
		Element dataPlotSettings = XmlUtils.findOrCreateChild(chart, "data_plot_settings");
		Element radarSeriesEl = XmlUtils.findOrCreateChild(dataPlotSettings, "radar");
		if (startAngle != null)
			radarSeriesEl.setAttribute("start_angle", startAngle);
		radarSeriesEl.setAttribute("use_polar_coords", String.valueOf(usePolarCoords));
		if (drawingStyle != null)
			radarSeriesEl.setAttribute("drawing_style", drawingStyle);
		if (displayDataMode != null)
			radarSeriesEl.setAttribute("display_data_mode", displayDataMode);
	}

	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, STARTANGLE, startAngle, ns);
		XmlUtils.addContent(fac, parent, USEPOLARCOORDS, usePolarCoords, ns);
		XmlUtils.addContent(fac, parent, DRAWINGSTYLE, drawingStyle, ns);
		XmlUtils.addContent(fac, parent, DISPLAYDATAMODE, displayDataMode, ns);
		XmlUtils.addContent(fac, parent, CLOSECONTOUR, closeContour, ns);
		XmlUtils.addContent(fac, parent, AREASERIES, areaSeries, ns);
		XmlUtils.addContent(fac, parent, CIRCULARLABELSSTYLE, circularLabelsStyle, ns);
	}
	
	public BirstXMLSerializable createNew() {
		return new RadarSeries();
	}

	public boolean isCloseContour()
	{
		return closeContour;
	}
	
	public boolean isAreaSeries()
	{
		return areaSeries;
	}
	
	public String getCircularLabelsStyle()
	{
		return circularLabelsStyle;
	}

	public void applyStyle(RadarSeries style) {
		if (style == null)
			return;
		
		startAngle = style.startAngle;
		usePolarCoords = style.usePolarCoords;
		drawingStyle = style.drawingStyle;
		displayDataMode = style.displayDataMode;
		closeContour = style.closeContour;
		areaSeries = style.areaSeries;
		circularLabelsStyle = style.circularLabelsStyle;
	}
}
