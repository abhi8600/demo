/**
 * 
 */
package com.successmetricsinc.chart.AnyChart;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.successmetricsinc.util.BirstXMLSerializable;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class Palette extends BirstXMLSerializable {
	private static final String GRADIENT_KEYS = "GradientKeys";
	private static final String GRADIENT_TYPE = "GradientType";
	private static final String COLOR = "Color";
	private static final String ITEMS = "Items";
	private static final String COLOR_COUNT = "ColorCount";
	private static final String AUTO_COLOR_COUNT = "AutoColorCount";
	private static final String PALETTE_TYPE = "PaletteType";
	private static final String NAME = "Name";
	private static final PaletteType DEFAULT_PALETTE_TYPE = PaletteType.DistinctColors;
	private static final GradientType DEFAULT_GRADIENT_TYPE = GradientType.Linear;
	private String name;
	
	public enum PaletteType { DistinctColors, ColorRange };
	private PaletteType type = DEFAULT_PALETTE_TYPE;
	
	private boolean autoColorCount = true;
	private int colorCount;

	private List<Color> items;
	
	public enum GradientType { Linear, Radial };
	private GradientType gradientType = DEFAULT_GRADIENT_TYPE;
	private List<Color> gradientKeys;
	
	private static Map<PaletteType, String> paletteTypeMap;
	private static Map<GradientType, String> gradientTypeMap;
	static {
		paletteTypeMap = new HashMap<PaletteType, String>();
		paletteTypeMap.put(PaletteType.DistinctColors, "DistinctColors");
		paletteTypeMap.put(PaletteType.ColorRange, "ColorRange");
		
		gradientTypeMap = new HashMap<GradientType, String>();
		gradientTypeMap.put(GradientType.Linear, "Linear");
		gradientTypeMap.put(GradientType.Radial, "Radial");
	}
	
	@Override
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, NAME, name, ns);
		XmlUtils.addContent(fac, parent, PALETTE_TYPE, paletteTypeMap.get(type), ns);
		XmlUtils.addContent(fac, parent, AUTO_COLOR_COUNT, autoColorCount, ns);
		XmlUtils.addContent(fac, parent, COLOR_COUNT, colorCount, ns);
		if (items != null) {
			OMElement is = fac.createOMElement(ITEMS, ns);
			for (Color c : items) {
				XmlUtils.addContent(fac, is, COLOR, c, ns);
			}
			parent.addChild(is);
		}
		
		XmlUtils.addContent(fac, parent, GRADIENT_TYPE, gradientTypeMap.get(gradientType), ns);
		if (gradientKeys != null) {
			OMElement gks = fac.createOMElement(GRADIENT_KEYS, ns);
			for (Color c : gradientKeys) {
				XmlUtils.addContent(fac, gks, COLOR, c, ns);
			}
			parent.addChild(gks);
		}
	}
	@Override
	public BirstXMLSerializable createNew() {
		return new Palette();
	}
	@Override
	public void parseElement(OMElement parent) {
		for (@SuppressWarnings("unchecked")
		Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();

			if (NAME.equals(elName)) {
				name = XmlUtils.getStringContent(el);
			}
			else if (PALETTE_TYPE.equals(elName)) {
				type = parseTypeFromString(XmlUtils.getStringContent(el), paletteTypeMap, DEFAULT_PALETTE_TYPE);
			}
			else if (AUTO_COLOR_COUNT.equals(elName)) {
				autoColorCount = XmlUtils.getBooleanContent(el);
			}
			else if (COLOR_COUNT.equals(elName)) {
				colorCount = XmlUtils.getIntContent(el);
			}
			else if (ITEMS.equals(elName)) {
				items = parseColorList(el);
			}
			else if (GRADIENT_TYPE.equals(elName)) {
				gradientType = parseTypeFromString(XmlUtils.getStringContent(el), gradientTypeMap, DEFAULT_GRADIENT_TYPE);
			}
			else if (GRADIENT_KEYS.equals(elName)) {
				gradientKeys = parseColorList(el);
			}
		}
	}
	@Override
	public void parseElement(XMLStreamReader el) throws Exception {
		for (XMLStreamIterator iter = new XMLStreamIterator(el); iter.hasNext(); ) {
			String elName = el.getLocalName();

			if (NAME.equals(elName)) {
				name = XmlUtils.getStringContent(el);
			}
			else if (PALETTE_TYPE.equals(elName)) {
				type = parseTypeFromString(XmlUtils.getStringContent(el), paletteTypeMap, DEFAULT_PALETTE_TYPE);
			}
			else if (AUTO_COLOR_COUNT.equals(elName)) {
				autoColorCount = XmlUtils.getBooleanContent(el);
			}
			else if (COLOR_COUNT.equals(elName)) {
				colorCount = XmlUtils.getIntContent(el);
			}
			else if (ITEMS.equals(elName)) {
				items = parseColorList(el);
			}
			else if (GRADIENT_TYPE.equals(elName)) {
				gradientType = parseTypeFromString(XmlUtils.getStringContent(el), gradientTypeMap, DEFAULT_GRADIENT_TYPE);
			}
			else if (GRADIENT_KEYS.equals(elName)) {
				gradientKeys = parseColorList(el);
			}
		}
	}
	private List<Color> parseColorList(OMElement parent) {
		List<Color> list = new ArrayList<Color>();
		for (@SuppressWarnings("unchecked")
		Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();
			if (COLOR.equals(elName)) {
				list.add(XmlUtils.getColorContent(el));
			}
		}
		return list;
	}
	private List<Color> parseColorList(XMLStreamReader parser) throws Exception {
		List<Color> list = new ArrayList<Color>();
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
			String elName = parser.getLocalName();
			if (COLOR.equals(elName)) {
				list.add(XmlUtils.getColorContent(parser));
			}
		}
		return list;
	}
}
