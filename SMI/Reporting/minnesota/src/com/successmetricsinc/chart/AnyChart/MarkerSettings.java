/**
 * 
 */
package com.successmetricsinc.chart.AnyChart;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.jdom.Element;

import com.successmetricsinc.chart.AnyChart.AnyChartNode.FillType;
import com.successmetricsinc.util.BirstXMLSerializable;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class MarkerSettings extends BirstXMLSerializable {

	public static final String SIZE = "Size";
	public static final String MARKER_TYPE = "MarkerType";

	public enum MarkerType { Circle, Square, Diamond, Cross, DiagonalCross, HLine, VLine, Star4,
		Star5, Star6, Star7, Star10, TriangleUp, TriangleDown, None
	}

	private static final MarkerType DEFAULT_MARKER_TYPE = MarkerType.TriangleUp;
	private static final int DEFAULT_MARKER_SIZE = 20;
	
	protected MarkerType markerType;
	protected int size = DEFAULT_MARKER_SIZE;
	
	public static Map<MarkerType, String> markerTypeMap = new HashMap<MarkerType, String>();
	static {
		markerTypeMap.put(MarkerType.None, "None");
		markerTypeMap.put(MarkerType.Circle, "Circle");
		markerTypeMap.put(MarkerType.Square, "Square");
		markerTypeMap.put(MarkerType.Diamond, "Diamond");
		markerTypeMap.put(MarkerType.Cross, "Cross");
		markerTypeMap.put(MarkerType.DiagonalCross, "DiagonalCross");
		markerTypeMap.put(MarkerType.HLine, "HLine");
		markerTypeMap.put(MarkerType.VLine, "VLine");
		markerTypeMap.put(MarkerType.Star4, "Star4");
		markerTypeMap.put(MarkerType.Star5, "Star5");
		markerTypeMap.put(MarkerType.Star6, "Star6");
		markerTypeMap.put(MarkerType.Star7, "Star7");
		markerTypeMap.put(MarkerType.Star10, "Star10");
		markerTypeMap.put(MarkerType.TriangleUp, "TriangleUp");
		markerTypeMap.put(MarkerType.TriangleDown, "TriangleDown");
	}
	
	public MarkerSettings() {
		
	}
	
	public MarkerSettings(OMElement defaultSettings) {
		parseElement(defaultSettings);
	}
	public void parseElement(OMElement parent) {
		if (parent == null)
			return;
		
		for (Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();
			
			if (MARKER_TYPE.equals(elName)) {
				markerType = parseMarkerType(XmlUtils.getStringContent(el));
			}
			else if (SIZE.equals(elName))
				size = XmlUtils.getIntContent(el);
		}
	}
	@Override
	public void parseElement(XMLStreamReader el) throws Exception {
		for (XMLStreamIterator iter = new XMLStreamIterator(el); iter.hasNext(); ) {
			String elName = el.getLocalName();
			
			if (MARKER_TYPE.equals(elName)) {
				markerType = parseMarkerType(XmlUtils.getStringContent(el));
			}
			else if (SIZE.equals(elName))
				size = XmlUtils.getIntContent(el);
		}
	}
		
	protected MarkerType parseMarkerType(String mts) {
		for (MarkerType mt : markerTypeMap.keySet()) {
			if (markerTypeMap.get(mt).equals(mts)) {
				return mt;
			}
		}
		return DEFAULT_MARKER_TYPE;
	}
		

	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, MARKER_TYPE, markerTypeMap.get(markerType), ns);
		XmlUtils.addContent(fac, parent, SIZE, size, ns);
	}

	@Override
	public BirstXMLSerializable createNew() {
		return new MarkerSettings();
	}

	public void getXML(Element chart, String styleName, AnyChartNode anyChartNode) {
		Element stylesEl = XmlUtils.findOrCreateChild(chart, "styles");
		Element markerStyleEl = new Element("marker_style");
		stylesEl.addContent(markerStyleEl);
		markerStyleEl.setAttribute("name", styleName);
		markerStyleEl.setAttribute("marker_type", markerTypeMap.get(markerType));
		Element markerEl = XmlUtils.findOrCreateChild(markerStyleEl, "marker");
		markerEl.setAttribute("enabled", "true");
		markerEl.setAttribute("size", String.valueOf(size));
		
		Element fillEl = XmlUtils.findOrCreateChild(markerStyleEl, "fill");
		fillEl.setAttribute("enabled", "True");
		fillEl.setAttribute("type", anyChartNode.getFillTypeString());
		fillEl.setAttribute("opacity", "1");
		if (anyChartNode.getFillType() == FillType.Gradient) {
			anyChartNode.getFillGradient().getXML(fillEl);
		}

		XmlUtils.findOrCreateChild(markerStyleEl, "effect").setAttribute("enabled", "False");
		XmlUtils.findOrCreateChild(markerStyleEl, "border").setAttribute("enabled", "False");
		
		AnyChartNode.setHoverColor(markerStyleEl);
	}
	public void applyStyle(MarkerSettings style) {
		if (style == null)
			return;
		
		markerType = style.markerType;
		size = style.size;
	}

}
