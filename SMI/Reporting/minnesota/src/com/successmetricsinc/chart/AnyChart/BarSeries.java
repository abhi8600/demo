/**
 * 
 */
package com.successmetricsinc.chart.AnyChart;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.successmetricsinc.chart.ChartOptions;
import com.successmetricsinc.util.BirstXMLSerializable;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class BarSeries extends BirstXMLSerializable {

	private static final String SHOW_BAR_BORDERS = "ShowBarBorders";
	private static final long serialVersionUID = 1L;
	private static final String BAR_SHAPE_TYPE = "BarShapeType";
	private static final String POINT_PADDING = "PointPadding";
	private static final String GROUP_PADDING = "GroupPadding";
	
	private static BarShapeType DEFAULT_SHAPE_TYPE = BarShapeType.Box;
	
	// group_padding=
	protected double groupPadding = 0.75;
	
	// point_padding=
	protected double pointPadding = 0.25;
	
	public enum BarShapeType { Box, Cylinder, Pyramid, Cone };
	// shape_type=
	protected BarShapeType shapeType = DEFAULT_SHAPE_TYPE;
	
	protected boolean showBarBorder = false;
	
	private static Map<BarShapeType, String> shapeTypeMap = new HashMap<BarShapeType, String>();
	static {
		shapeTypeMap.put(BarShapeType.Box, "Box");
		shapeTypeMap.put(BarShapeType.Cylinder, "Cylinder");
		shapeTypeMap.put(BarShapeType.Pyramid, "Pyramid");
		shapeTypeMap.put(BarShapeType.Cone, "Cone");
	}
	
	public BarSeries() {
		
	}
	public BarSeries(ChartOptions co, OMElement defaultSettings) {
		parseElement(defaultSettings);
		if ("3D".equals(co.gradient))
			shapeType = BarShapeType.Cylinder;
	}

	public BarSeries(OMElement defaultSettings) {
		parseElement(defaultSettings);
	}
	
	public void parseElement(OMElement parent) {
		if (parent == null)
			return;
		for (@SuppressWarnings("unchecked")
		Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();
			
			if (GROUP_PADDING.equals(elName)) {
				groupPadding = XmlUtils.getDoubleContent(el);
			}
			else if (POINT_PADDING.equals(elName)) {
				pointPadding = XmlUtils.getDoubleContent(el);
			}
			else if (BAR_SHAPE_TYPE.equals(elName)) {
				shapeType = BirstXMLSerializable.parseTypeFromString(XmlUtils.getStringContent(el), shapeTypeMap, DEFAULT_SHAPE_TYPE);
			}
			else if (SHOW_BAR_BORDERS.equals(elName)) 
				showBarBorder = XmlUtils.getBooleanContent(el);
		}
	}
	
	@Override
	public void parseElement(XMLStreamReader el) throws Exception {
		for (XMLStreamIterator iter = new XMLStreamIterator(el); iter.hasNext(); ) {
			String elName = el.getLocalName();
			
			if (GROUP_PADDING.equals(elName)) {
				groupPadding = XmlUtils.getDoubleContent(el);
			}
			else if (POINT_PADDING.equals(elName)) {
				pointPadding = XmlUtils.getDoubleContent(el);
			}
			else if (BAR_SHAPE_TYPE.equals(elName)) {
				shapeType = BirstXMLSerializable.parseTypeFromString(XmlUtils.getStringContent(el), shapeTypeMap, DEFAULT_SHAPE_TYPE);
			}
			else if (SHOW_BAR_BORDERS.equals(elName)) 
				showBarBorder = XmlUtils.getBooleanContent(el);
		}
	}
	
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, GROUP_PADDING, groupPadding, ns);
		XmlUtils.addContent(fac, parent, POINT_PADDING, pointPadding, ns);
		XmlUtils.addContent(fac, parent, BAR_SHAPE_TYPE, shapeTypeMap.get(shapeType), ns);
		XmlUtils.addContent(fac, parent, SHOW_BAR_BORDERS, showBarBorder, ns);
	}
	
	public BirstXMLSerializable createNew() {
		return new BarSeries();
	}
	
	public String getShapeType() {
		return shapeTypeMap.get(shapeType);
	}
	public boolean isShowBarBorder() {
		return showBarBorder;
	}
	
	public double getGroupPadding() {
		return groupPadding;
	}
	public double getPointPadding() {
		return pointPadding;
	}

	public void setGroupPadding(double groupPadding) {
		this.groupPadding = groupPadding;
	}
	public void setPointPadding(double pointPadding) {
		this.pointPadding = pointPadding;
	}
	public void applyStyle(BarSeries style) {
		if (style == null)
			return;
		
		groupPadding = style.groupPadding;
		pointPadding = style.pointPadding;
		shapeType = style.shapeType;
		showBarBorder = style.showBarBorder;
	}
}
