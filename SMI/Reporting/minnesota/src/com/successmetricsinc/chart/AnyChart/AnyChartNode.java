/**
 * 
 */
package com.successmetricsinc.chart.AnyChart;

import java.awt.Color;
import java.awt.Font;
import java.awt.Paint;
import java.io.File;
import java.io.Serializable;
import java.sql.Types;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.Map.Entry;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamReader;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;
import org.jdom.Attribute;
import org.jdom.Element;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.UserBean;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.adhoc.AdhocChart;
import com.successmetricsinc.adhoc.AdhocColumn;
import com.successmetricsinc.adhoc.AdhocEntity;
import com.successmetricsinc.adhoc.AdhocReport;
import com.successmetricsinc.adhoc.ChartFilters;
import com.successmetricsinc.adhoc.ColumnSelector;
import com.successmetricsinc.adhoc.Drill;
import com.successmetricsinc.adhoc.DrillColumnList;
import com.successmetricsinc.adhoc.AdhocChart.PlotType;
import com.successmetricsinc.adhoc.AdhocColumn.AdditionalDrillThruColumn;
import com.successmetricsinc.adhoc.AdhocColumn.ColumnType;
import com.successmetricsinc.catalog.CatalogManager;
import com.successmetricsinc.chart.BirstCategoryDataset;
import com.successmetricsinc.chart.Chart;
import com.successmetricsinc.chart.ChartColors;
import com.successmetricsinc.chart.ChartGradientPaint;
import com.successmetricsinc.chart.ChartOptions;
import com.successmetricsinc.chart.FlashRenderer;
import com.successmetricsinc.chart.AnyChart.Axes;
import com.successmetricsinc.chart.AnyChart.CustomThreshold;
import com.successmetricsinc.chart.AnyChart.DataPlotSettings;
import com.successmetricsinc.chart.AnyChart.Gradient;
import com.successmetricsinc.chart.AnyChart.LegendSettings;
import com.successmetricsinc.chart.AnyChart.NoDataException;
import com.successmetricsinc.chart.AnyChart.Palette;
import com.successmetricsinc.chart.AnyChart.Threshold;
import com.successmetricsinc.chart.AnyChart.XAxis;
import com.successmetricsinc.chart.AnyChart.AnyChartNode.DataRows.DataRow;
import com.successmetricsinc.chart.ChartOptions.GeoMapType;
import com.successmetricsinc.query.BirstInterval;
import com.successmetricsinc.query.BirstRatio;
import com.successmetricsinc.query.DisplayFilter;
import com.successmetricsinc.query.Filter;
import com.successmetricsinc.query.IJasperDataSourceProvider;
import com.successmetricsinc.query.JasperDataSourceProvider;
import com.successmetricsinc.query.QueryFilter;
import com.successmetricsinc.query.QueryResultSet;
import com.successmetricsinc.query.QueryString;
import com.successmetricsinc.query.ResultSetTooBigException;
import com.successmetricsinc.query.SingleRowDataSource;
import com.successmetricsinc.query.SyntaxErrorException;
import com.successmetricsinc.query.Filter.FilterType;
import com.successmetricsinc.query.olap.BirstOlapField;
import com.successmetricsinc.transformation.integer.ToTime;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.BirstXMLSerializable;
import com.successmetricsinc.util.ColorUtils;
import com.successmetricsinc.util.DateUtil;
import com.successmetricsinc.util.UMapWebServiceHelper;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class AnyChartNode extends BirstXMLSerializable {
	private static final String THRESHOLDING = "Thresholding";
	private static final int MAP_CONTROLS_HEIGHT = 252;
	private static final int MAP_SCROLLING_HEIGHT = 63;
	private static final int MAP_ZOOMING_HEIGHT = 180;
	private static final String FILL_GRADIENT = "FillGradient";
	private static final String FILL_TYPE = "FillType";
	private static final long serialVersionUID = 1L;
	private static final String SHOW_MEASURE_NAMES_IN_TOOLTIP = "ShowMeasureNamesInTooltip";
	private static final String SHOW_VALUES_VERTICAL_ALIGNMENT = "ShowValuesVerticalAlignment";
	private static final String UNIFORM_COLOR = "UniformColor";
	private static final String SHOW_VALUES = "ShowValues";
	private static final String COLOR_LIST = "ColorList";
	private static final String DISPLAY_VALUE_FORMAT = "DisplayValueFormat";
	private static final String TOOLTIP_FORMAT = "TooltipFormat";
	private static final String TOOLTIP_FONT = "TooltipFont";
	private static final String TOOLTIP_COLOR =  "TooltipColor";
	private static final String DISPLAY_VALUE_FONT = "DisplayValueFont";
	private static final String DISPLAY_VALUE_COLOR = "DisplayValueColor";
	private static final String DISPLAY_VALUE_ROTATION = "DisplayValueRotation";
	private static final String ADHOC_REPORT = AdhocReport.class.getName();
	private static final double PIE_LABEL_THRESHOLD = 0.01; // do not display values less than this percentage
	
	public enum StackedType { NotStacked, Stacked, PercentStacked };

	public class DrillData {

		String formatStr = null;
		List<String> categoryNames = new ArrayList<String>();
		List<String> categoryDimensions = new ArrayList<String>();
		List<String> categoryHierarchies = new ArrayList<String>();
		List<String> categoryFormats = new ArrayList<String>();
		List<Boolean> categoryIsDateTime = new ArrayList<Boolean>();
		List<String> categoryAliases = new ArrayList<String>();
		List<String> seriesDimensionNames = new ArrayList<String>();
		List<String> seriesDimensionHierarchies = new ArrayList<String>();
		List<String> seriesDimensionDimensions = new ArrayList<String>();
		List<String> seriesDimensionAliases = new ArrayList<String>();
		List<String> seriesFormats = new ArrayList<String>();
		List<Boolean> seriesIsDateTime = new ArrayList<Boolean>();
		int seriesMeasureCount = 0;
		boolean drillDown = false;
		boolean drillAcross = false;
		boolean olapDrillDown = false;
		String drillAcrossDash = null;
		List<AdhocColumn> drillBys = new ArrayList<AdhocColumn>();
		private StringBuilder drillFilters;
		private TimeZone processingTimeZone = TimeZone.getDefault();;
		private String additionalDrillThruString = null;
		
		public DrillData() {
			UserBean ub = UserBean.getParentThreadUserBean();
			Repository r = null;
			if (ub != null) {
				r = ub.getRepository();
				if (r != null) {
					processingTimeZone = r.getServerParameters().getProcessingTimeZone();
				}
			}
			for (AdhocColumn col : getReport().getColumns()) {
				String label = col.getLabelString();
				ColumnSelector sel = null;
				if (selectors != null) {
					sel = selectors.get(label);
				}
				Integer reportIndex = Integer.valueOf(col.getReportIndex());
				if (series.contains(reportIndex)) {
					if (col.getType() == ColumnType.Dimension) {
						seriesDimensionNames.add(col.getColumn());
						seriesDimensionDimensions.add(col.getDimension());
						categoryHierarchies.add(col.getHierarchy());
						seriesDimensionAliases.add(col.getFieldName());
						int i = getQrsIndexFromReportIndex(col);
						if (i >= 0 && qrs.getColumnDataTypes()[i] == Types.TIMESTAMP) {
							String format = col.getFormat();
							if (format == null) {
								boolean isDateTime = qrs.getWarehouseColumnsDataTypes()[i].equals("DateTime");
								// get the default format
								SimpleDateFormat sdf = null;  
								if (!isDateTime) {
									if (ub != null)
										sdf = ub.getDateFormat();
									else
										sdf = DateUtil.getShortDateFormat(Locale.getDefault());
								}
								else {
									if (ub != null)
										sdf = ub.getDateTimeFormat();
									else
										sdf = DateUtil.getShortDateTimeFormat(Locale.getDefault());
								}
								seriesIsDateTime.add(isDateTime ? Boolean.TRUE : Boolean.FALSE);
								if (sdf != null) {
									format = sdf.toPattern();
								}
							}
							else {
								seriesIsDateTime.add(Boolean.FALSE);
							}
							seriesFormats.add(format);
						}
						else {
							seriesFormats.add(null);
							seriesIsDateTime.add(null);
						}
						if (! col.getDrillType().equals(Chart.DRILL_TYPE_NONE) && col.getType() != ColumnType.Expression && col.getType() != ColumnType.Ratio) {
							if (col.getDrillType().equals(Chart.DRILL_TYPE_NAVIGATE) && drillAcrossDash == null) {
								// only navigate to the first one
								drillAcross = true;
								drillDown = false;
								drillAcrossDash = col.getDrillToDash() + "," + col.getDrillToDashPage();
								drillBys.add(col);
							}
							else if (col.getDrillType().equals(Chart.DRILL_TYPE_DRILL) && drillAcross == false) {
								if (col.retrieveAllDrillToColumns(r) != null && col.retrieveAllDrillToColumns(r).size() > 0) {
									drillDown = true;
									drillBys.add(col);
								}
								else if (col.isOlapDimension()) {
									olapDrillDown = true;
									drillBys.add(col);
								}
							}
							
							// add additionalDrillThruColumns
							if (col.getAdditionalDrillThuColumns() != null) {
								for (AdditionalDrillThruColumn adtc : col.getAdditionalDrillThuColumns()) {
									AdhocEntity ae = report.getEntityById(adtc.column);
									if (ae != null && ae instanceof AdhocColumn && !drillBys.contains(ae))
										drillBys.add((AdhocColumn) ae);
								}
							}
						}
					}
					else {
						formatStr = col.getFormat();
						seriesMeasureCount++;
					}
				}
				if (categories.contains(reportIndex)) {
					AdhocColumn colToUse = sel == null ? col : sel.getAdhocColumn();
					if (colToUse.treatAsDimension()) {
						if (colToUse.getType() == ColumnType.Expression)
							categoryNames.add(colToUse.getExpressionString());
						else
							categoryNames.add(colToUse.getColumn());
						categoryDimensions.add(colToUse.getDimension());
						categoryHierarchies.add(colToUse.getHierarchy());
						categoryAliases.add(col.getFieldName());
						int i = getQrsIndexFromReportIndex(col);
						if (i >= 0 && qrs.getColumnDataTypes()[i] == Types.TIMESTAMP) {
							String format = col.getFormat();
							if (format == null) {
								boolean isDateTime = qrs.getWarehouseColumnsDataTypes()[i].equals("DateTime");
								// get the default format
								SimpleDateFormat sdf = null;  
								if (!isDateTime) {
									if (ub != null)
										sdf = ub.getDateFormat();
									else
										sdf = DateUtil.getShortDateFormat(Locale.getDefault());
								}
								else {
									if (ub != null)
										sdf = ub.getDateTimeFormat();
									else
										sdf = DateUtil.getShortDateTimeFormat(Locale.getDefault());
								}
								categoryIsDateTime.add(isDateTime ? Boolean.TRUE : Boolean.FALSE);
								if (sdf != null) {
									format = sdf.toPattern();
								}
							}
							else {
								categoryIsDateTime.add(Boolean.FALSE);
							}
							categoryFormats.add(format);
						}
						else {
							categoryFormats.add(null);
							categoryIsDateTime.add(null);
						}
						
						if (! col.getDrillType().equals(Chart.DRILL_TYPE_NONE)) { // && col.getType() != ColumnType.Expression) {
							if (col.getDrillType().equals(Chart.DRILL_TYPE_NAVIGATE) && drillAcrossDash == null) {
								// only navigate to the first one
								drillAcross = true;
								drillDown = false;
								drillAcrossDash = col.getDrillToDash() + "," + col.getDrillToDashPage();
								drillBys.add(col);
							}
							else if (col.getDrillType().equals(Chart.DRILL_TYPE_DRILL) && drillAcross == false && col.getType() != ColumnType.Expression && col.getType() != ColumnType.Ratio) {
								List<DrillColumnList> list = col.retrieveAllDrillToColumns(r);
								if (list != null && list.size() > 0) {							
									drillDown = true;
									drillBys.add(col);
								}
								else if (col.isOlapDimension()) {
									olapDrillDown = true;
									drillBys.add(col);
								}
							}
							
							// add additionalDrillThruColumns
							if (col.getAdditionalDrillThuColumns() != null) {
								for (AdditionalDrillThruColumn adtc : col.getAdditionalDrillThuColumns()) {
									AdhocEntity ae = report.getEntityById(adtc.column);
									if (ae != null && ae instanceof AdhocColumn && !drillBys.contains(ae))
										drillBys.add((AdhocColumn) ae);
								}
							}
							
							additionalDrillThruString = col.getAdditionalDrillThruString();
							if (additionalDrillThruString != null)
								additionalDrillThruString = additionalDrillThruString.trim();
						}
					}
				}
			}
		}

		public void addExtraData(Element point, Element attributes, CellKey columnKey, CellKey rowKey) {
			int count = 0;
			drillFilters = new StringBuilder();
			if (additionalDrillThruString != null)
				drillFilters.append(additionalDrillThruString);
			Map<String, Object> additionalDrills = new HashMap<String, Object>();
			for (; count < categoryNames.size(); count++) {
				Map<String, Object> entry = null;
				String alias = categoryAliases.get(count);
				Object value = null;
				String techName = null;
				Object v = null;
				if (alias != null) {
					entry = additionalDrillParams.get(columnKey.toString());
					if (entry != null) {
						v = entry.get(alias);
						if (v != null && v instanceof Calendar) {
							Boolean isDate = categoryIsDateTime.size() > count ? categoryIsDateTime.get(count) : Boolean.FALSE;
							if (isDate != null && isDate.booleanValue() == true) {
								value = DateUtil.convertDateTimeToStandardFormat((Calendar)v, TimeZone.getTimeZone("GMT"));
							}
							else {
								value = DateUtil.convertDateToStandardFormat((Calendar)v);
							}
						}
						
						if (v != null && v instanceof BirstOlapField) {
							techName = ((BirstOlapField)v).getTechName();
						}
					}
				}
				addCustomAttribute(attributes, "column" + count, categoryNames.get(count));
				addCustomAttribute(attributes, "dimension" + count, categoryDimensions.get(count));
				if(categoryHierarchies.size() > count && categoryHierarchies.get(count) != null)
					addCustomAttribute(attributes, "hierarchy" + count, categoryHierarchies.get(count));
				if (categoryFormats.get(count) != null) {
					addCustomAttribute(attributes, "datetimeFormat" + count, categoryFormats.get(count));
				}
				
				if (value == null && columnKey != null && columnKey.size() > count) {
					value = columnKey.get(count);
					String fmt = categoryFormats.get(count);
					if (fmt != null) {
						try {
							SimpleDateFormat sdf = new SimpleDateFormat(fmt);
							Date dt = sdf.parse(value.toString());
							if (categoryIsDateTime.get(count) != null && categoryIsDateTime.get(count).booleanValue()) {
								value = DateUtil.convertDateTimeToStandardFormat(dt, TimeZone.getTimeZone("GMT"));
							}
							else {
								value = DateUtil.convertDateToStandardFormat(dt);
							}
						}
						catch (Exception e) {}
					}
					
					if (value instanceof BirstOlapField) {
						v = value;
					}
				}
				if (value != null && !(v instanceof BirstOlapField || v instanceof BirstRatio))
					v = value;
				if (value != null)
					addCustomAttribute(attributes, "value" + count, value.toString());
				addCustomAttribute(attributes, "label" + count, categoryNames.get(count));
				
				if (v != null) {
					for (AdhocColumn ac: drillBys) {
						String dim = ac.getDimension();
						String col = ac.getColumn();
						if ("EXPR".equals(dim))
							col = ac.getExpressionString();
						if (categoryDimensions.get(count).equals(dim) &&
								categoryNames.get(count).equals(col)) {
							String key = ac.getDrillToParameterName();
							if (!additionalDrills.containsKey(key)) {
								additionalDrills.put(key, v);
							}
						}
					}
				}
				
				// add rest of drill bys
				if (entry != null) {
					for (AdhocColumn ac: drillBys) {
						String key = ac.getDrillToParameterName(); //.getDefaultPromptName();
						if (!additionalDrills.containsKey(key)) {
							value = null;
							v = entry.get(ac.getFieldName());
							if (v == null)
								continue;
							if (v instanceof Calendar) {
								Boolean isDate = categoryIsDateTime.size() > count ? categoryIsDateTime.get(count) : Boolean.FALSE;
								if (isDate != null && isDate.booleanValue() == true) {
									value = DateUtil.convertDateTimeToStandardFormat((Calendar)v, TimeZone.getTimeZone("GMT"));
								}
								else {
									value = DateUtil.convertDateToStandardFormat((Calendar)v);
								}
							}
							else if (v instanceof BirstOlapField) {
								BirstOlapField b = (BirstOlapField)v;
								value = b.getValue().toString() + '=' + b.getTechName();
							}
							else {
								value = v;
							}
							
							additionalDrills.put(key, value);
						}
					}
				}
			}
			
			for (Entry<String, Object> e : additionalDrills.entrySet()) {
				if (drillFilters.length() > 0){
					drillFilters.append("&");
				}
				String key = Chart.encodeParamForDrilling(e.getKey());
				drillFilters.append(key);
				drillFilters.append('=');
				Object val = e.getValue();
				if (val instanceof BirstOlapField) {
					BirstOlapField b = (BirstOlapField)val;
					String filter = Chart.encodeParamForDrilling(b.getValue().toString()) + '=' + Chart.encodeParamForDrilling(b.getTechName()); 
					drillFilters.append(filter);
				}
				else if (val != null)
					drillFilters.append(Chart.encodeParamForDrilling(val.toString()));
			}
			
			
			if (rowKey != null)
			{
				for (int l = 0; l < seriesDimensionNames.size(); l++, count++) {
					addCustomAttribute(attributes, "column" + count, seriesDimensionNames.get(l));
					addCustomAttribute(attributes, "dimension" + count, seriesDimensionDimensions.get(l));
					if (seriesDimensionHierarchies.size() > l && seriesDimensionHierarchies.get(l) != null)
						addCustomAttribute(attributes, "hierarchy" + count, seriesDimensionHierarchies.get(l));
					addCustomAttribute(attributes, "label" + count, seriesDimensionNames.get(l));
					Object value = null;
					if (l < rowKey.size()) {
						value = rowKey.get(l);
						String fmt = seriesFormats.get(l);
						if (fmt != null) {
							try {
								SimpleDateFormat sdf = new SimpleDateFormat(fmt);
								Date dt = sdf.parse(value.toString());
								if (seriesIsDateTime.get(l) != null && seriesIsDateTime.get(l).booleanValue()) {
									value = DateUtil.convertDateTimeToStandardFormat(dt, TimeZone.getTimeZone("GMT"));
								}
								else {
									value = DateUtil.convertDateToStandardFormat(dt);
								}
							}
							catch (Exception e) {}
						}
					
						addCustomAttribute(attributes, "value" + count, value.toString());
						for (AdhocColumn ac: drillBys) {
							if (seriesDimensionDimensions.get(l).equals(ac.getDimension()) &&
									seriesDimensionNames.get(l).equals(ac.getColumn())) {
								if (drillFilters.length() > 0)
									drillFilters.append("&");
								drillFilters.append(Chart.encodeParamForDrilling(ac.getDrillToParameterName()));
								drillFilters.append('=');
								drillFilters.append(Chart.encodeParamForDrilling(value.toString()));
								if (value instanceof BirstOlapField) {
									drillFilters.append('=');
									drillFilters.append(Chart.encodeParamForDrilling(((BirstOlapField)value).getTechName()));
								}
							}
						}
					}
				}
			}
			
			if (drillDown) {
				addCustomAttribute(attributes, "DrillType", "Drill Down");
				addCustomAttribute(attributes, "targetURL", report.getFileName());
				addCustomAttribute(attributes, "filters", drillFilters.toString());
				StringBuilder drillBy = new StringBuilder();
				for (AdhocColumn ac : drillBys) {
					if (drillBy.length() > 0)
						drillBy.append('&');
					drillBy.append(ac.getDefaultPromptName());
				}
				addCustomAttribute(attributes, "drillBy", drillBy.toString());
			}
			else if (drillAcross && drillAcrossDash != null && Util.hasNonWhiteSpaceCharacters(drillAcrossDash)) {
				addCustomAttribute(attributes, "DrillType", "Drill To Dashboard");
				addCustomAttribute(attributes, "targetURL", drillAcrossDash);
				addCustomAttribute(attributes, "filters", drillFilters.toString());
			}
			else if (olapDrillDown) {
				addCustomAttribute(attributes, "DrillType", "Olap Drill Down");
				StringBuilder drillBy = new StringBuilder();
				for (AdhocColumn ac : drillBys) {
					if (drillBy.length() > 0)
						drillBy.append('&');
					drillBy.append(ac.getFieldName());
				}
				addCustomAttribute(attributes, "alias", drillBy.toString());
				addCustomAttribute(attributes, "filters", drillFilters.toString());
			}
			else
			{
				point.setAttribute("use_hand_cursor", "False"); // no change in cursor and no select if no drill
				point.setAttribute("allow_select", "false");
			}
		}
		
	}

	private static final String USE_CUSTOM_THRESHOLDS = "UseCustomThresholds";
	private static final String CUSTOM_THRESHOLDS = "CustomThresholds";
	private static final String FULL_NAME = "fullName";
	private static final String UMAP_TOOLTIP_VALUE = "umap";
	private static final String AXES = "Axes";
	private static final String THRESHOLDS = "Thresholds";
	private static final String LABEL = "Label";
	private static final String SERIES2 = "Series2";
	private static final String SERIES3 = "Series3";
	private static final String SERIES = "Series";
	private static final String CATEGORIES = "Categories";
	private static final String COLORS = "colorColumn";
	private static final String MAP_NAMES = "MapNames";
	private static final String CHART_TYPE = "ChartType";
	private static final String PALETTES = "Palettes";
	private static final String DATA_PLOT_SETTINGS = "DataPlotSettings";
	private static final String DATA_SOURCE_PATH = "DataSourcePath";
	private static Logger logger = Logger.getLogger(AnyChartNode.class);
	
	public enum ChartType { Area, Bar, Bubble, Line, Pie, Doughnut, Pyramid, Scatter, StackedBar, Gauge, NewGauge, 
		Treemap, Heatmap, Funnel, GeoMap, Marker, Column, StackedColumn, StackedArea, Radar, UMap, RangeBar, RangeColumn, Waterfall, PercentStackedBar, PercentStackedColumn
	};

	public enum VerticalAlignment { Top, Center, Bottom };
	
	private static final ChartType DEFAULT_CHART_TYPE = ChartType.Bar;
	private static final VerticalAlignment DEFAULT_VERTICAL_ALIGNMENT = VerticalAlignment.Top;
	private static Map<ChartType, String> chartTypesMap = new HashMap<ChartType, String>();
	private static Map<ChartType, List<ChartType>> validChartCombinations = new HashMap<ChartType, List<ChartType>>();
	private static List<ChartType> chartsSupport3D = new ArrayList<ChartType>();
	private static Map<VerticalAlignment, String> verticalAlignmentMap = new HashMap<VerticalAlignment, String>(); 
	
	static {
		initializeClass();
	}
	
	private String label;
	private ChartType chartType = DEFAULT_CHART_TYPE; 
//	private boolean enable_animation = false;
	private String dataSourcePath = null;
	
	// report index of categories from datasource
	// multiple categories concatenate on each other to form a new category
	// i.e. product/year
	private List<Integer> categories;
	// report index of series from datasource
	// multiple series means different things for different chart types
	private List<Integer> series;
	
	// report index of series2 from datasource
	// series2 has different meanings based on chart type
	// stacked bar series 2 is the category used to determine the bar slices
	// bubble and scatter chart series 2 is the y position
	// for rangebar/column, series 1 is start, series 2 is end
	private List<Integer> series2;
	
	// report index of series3 from datasource
	// series2 has different meanings based on chart type
	// bubble chart series 3 is the size of the bubble
	private List<Integer> series3;
	
	private int thresholding = -1;
	
	private Integer colorColumn;
	private List<Integer> mapNames;
	
	// <data_plot_settings>
	private DataPlotSettings dataPlotSettings;
	
	private Axes axes;
	
	private boolean useCustomThresholds = false;
	private List<CustomThreshold> customThresholds; 
	private List<Color> colorList;
	
	public enum FillType { Gradient, Solid };
	public static final FillType DEFAULT_FILL_TYPE = FillType.Gradient;
	private FillType fillType = DEFAULT_FILL_TYPE; 
	private Gradient fillGradient = new Gradient();
	
	private boolean showValues;
	private VerticalAlignment showValuesVerticalAlignment = DEFAULT_VERTICAL_ALIGNMENT;
	
	private String displayValueFormat;
	private Font displayValueFont;
	private Color displayValueColor = Color.BLACK;
	private String displayValueRotation;
	
	private String tooltipFormat;	
	private Font tooltipFont;
	private Color tooltipColor = Color.BLACK;

	private LegendSettings legendSettings;
	
	private boolean useSameColor = false;
	private boolean showMeasureNamesInTooltip = true;
	
	private transient QueryResultSet qrs;
	private transient AdhocReport report;
	private transient Map<String, ColumnSelector> selectors;
	private transient DataRows _dataRows = null;
	
	// <styles>
	//private Styles styles;
	
	// <palettes>
	private List<Palette> palettes;
	
	// <thresholds>
	private List<Threshold> thresholds;
	
	private transient Element yAxis = null;
	private transient Font labelFont;
	private transient Color labelColor;
	private transient double scaleFactor;
	
	private Map<String, Map<String, Object>> additionalDrillParams = new HashMap<String, Map<String, Object>>();
	
	private static Map<FillType, String> fillTypeMap;
	static {
		fillTypeMap = new HashMap<FillType, String>();
		fillTypeMap.put(FillType.Gradient, "Gradient");
		fillTypeMap.put(FillType.Solid, "Solid");
	}
	public AnyChartNode() {
		
	}

	public AnyChartNode(ChartOptions co, AdhocReport report, OMElement defaults) {
		if (co.type == ChartOptions.ChartType.Area)
			chartType = ChartType.Area;
		else if (co.type == ChartOptions.ChartType.Column || co.type == ChartOptions.ChartType.BarLine)
			chartType = ChartType.Column;
		else if (co.type == ChartOptions.ChartType.Bar)
			chartType = ChartType.Bar;
		else if (co.type == ChartOptions.ChartType.BubbleChart)
			chartType = ChartType.Bubble;
		else if (co.type == ChartOptions.ChartType.Funnel)
			chartType = ChartType.Funnel;
		else if (co.type == ChartOptions.ChartType.HeatMap)
			chartType = ChartType.Heatmap;
		else if (co.type == ChartOptions.ChartType.Line)
			chartType = ChartType.Line;
		else if (co.type == ChartOptions.ChartType.Map)
			chartType = ChartType.GeoMap;
		else if (co.type == ChartOptions.ChartType.MeterPlot)
			chartType = ChartType.Gauge;
		else if (co.type == ChartOptions.ChartType.Pie)
			chartType = ChartType.Pie;
		else if (co.type == ChartOptions.ChartType.Doughnut)
			chartType = ChartType.Doughnut;		
		else if (co.type == ChartOptions.ChartType.Pyramid)
			chartType = ChartType.Pyramid;
		else if (co.type == ChartOptions.ChartType.Scatterplot)
			chartType = ChartType.Scatter;
//		else if (co.type == ChartOptions.ChartType.StackedArea)
//			chartType = S
		else if (co.type == ChartOptions.ChartType.StackedColumn || co.type == ChartOptions.ChartType.StackedColumnLine)
			chartType = ChartType.StackedColumn;
		else if (co.type == ChartOptions.ChartType.StackedBar)
			chartType = ChartType.StackedBar;
		else if (co.type == ChartOptions.ChartType.PercentStackedColumn)
			chartType = ChartType.PercentStackedColumn;
		else if (co.type == ChartOptions.ChartType.PercentStackedBar)
			chartType = ChartType.PercentStackedBar;		
		else if (co.type == ChartOptions.ChartType.StackedArea)
			chartType = ChartType.StackedArea;
		else if (co.type == ChartOptions.ChartType.TreeMap)
			chartType = ChartType.Treemap;
		else if (co.type == ChartOptions.ChartType.Radar)
			chartType = ChartType.Radar;
		
//		dataSourcePath = report.getFileName() == null ? null : report.getFileName() + ".AdhocReport";
		categories = new ArrayList<Integer>();
		series = new ArrayList<Integer>();
		for (AdhocColumn ac : report.getColumns()) {
			String label = ac.getLabelString();
			if (co.categories.contains(label))
				categories.add(ac.getReportIndex());
			else if (co.series.contains(label))
				series.add(ac.getReportIndex());
			else if (label.equals(co.colorColumn))
				colorColumn = ac.getReportIndex();
		}
		if (categories.isEmpty()) {
			// add first dimension as a category
			for (AdhocColumn ac : report.getColumns()) {
				if (ac.getType() == ColumnType.Dimension || 
						ac.getType() == ColumnType.Ratio ||
						(ac.getType() == ColumnType.Expression && ac.isMeasureExpression() == false)) {
					categories.add(ac.getReportIndex());
					break;
				}
			}
		}
		if (series.isEmpty()) {
			// add first measure as series
			for (AdhocColumn ac : report.getColumns()) {
				if (ac.getType() == ColumnType.Measure || 
						ac.getType() == ColumnType.Ratio ||
						(ac.getType() == ColumnType.Expression && ac.isMeasureExpression() == true)) {
					series.add(ac.getReportIndex());
					break;
				}
			}
		}
		if (co.type == ChartOptions.ChartType.BarLine || co.type == ChartOptions.ChartType.StackedColumnLine) {
			if ((co.series2 == null || co.series2.isEmpty()) && series.size() > 1) {
				// remove last series - its for line chart
				series.remove(series.size() - 1);
			}
		}
		if (chartType == ChartType.Scatter /* || chartType == ChartType.RangeBar || chartType == ChartType.RangeColumn XXX */) {
			series2 = new ArrayList<Integer>();
			series2.add(series.remove(series.size() - 1));
		}
		else if (chartType == ChartType.Bubble) {
			series3 = new ArrayList<Integer>();
			series3.add(series.remove(series.size() - 1));
			series2 = new ArrayList<Integer>();
			series2.add(series.remove(series.size() - 1));
		}
		
		showValues = co.displayValues;
		
		if (chartType == ChartType.GeoMap) {
			showValues = showValues || co.showGeoMapLabel;
		}
		
		dataPlotSettings = new DataPlotSettings(co, report, defaults == null ? null : defaults.getFirstChildWithName(new QName(DATA_PLOT_SETTINGS)));
		axes = new Axes(co, report, defaults == null ? null : defaults.getFirstChildWithName(new QName(AXES)));
		fillType = FillType.Gradient;
		if (defaults != null) {
			OMElement fillTypeEl = defaults.getFirstChildWithName(new QName(FILL_TYPE));
			if (fillTypeEl != null)
				fillType = parseTypeFromString(XmlUtils.getStringContent(fillTypeEl), fillTypeMap, DEFAULT_FILL_TYPE);
			OMElement fillGradientEl = defaults.getFirstChildWithName(new QName(FILL_GRADIENT));
			if (fillGradientEl != null)
				fillGradient.parseElement(fillGradientEl);
		}
	}

	public AnyChartNode(OMElement defaultNode) {
		dataPlotSettings = new DataPlotSettings(defaultNode == null ? null : defaultNode.getFirstChildWithName(new QName(DATA_PLOT_SETTINGS)));
		axes = new Axes(defaultNode == null ? null : defaultNode.getFirstChildWithName(new QName(AXES)));
		fillType = FillType.Gradient;
		if (defaultNode != null) {
			OMElement fillTypeEl = defaultNode.getFirstChildWithName(new QName(FILL_TYPE));
			if (fillTypeEl != null)
				fillType = parseTypeFromString(XmlUtils.getStringContent(fillTypeEl), fillTypeMap, DEFAULT_FILL_TYPE);
			OMElement fillGradientEl = defaultNode.getFirstChildWithName(new QName(FILL_GRADIENT));
			if (fillGradientEl != null)
				fillGradient.parseElement(fillGradientEl);
		}
	}
	
	public DataPlotSettings getDataPlotSettings()
	{
		return dataPlotSettings;
	}

	@SuppressWarnings("unchecked")
	public void parseElement(OMElement parent) {
		for (Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();
			if (LABEL.equals(elName)) {
				label = XmlUtils.getStringContent(el);
			}
			else if (CHART_TYPE.equals(elName)) {
				chartType = parseTypeFromString(XmlUtils.getStringContent(el), chartTypesMap, DEFAULT_CHART_TYPE);
			}
			else if (DATA_PLOT_SETTINGS.equals(elName)) {
				dataPlotSettings = (DataPlotSettings) XmlUtils.parseFirstChild(el, dataPlotSettings == null ? new DataPlotSettings() : dataPlotSettings);
			}
			else if (PALETTES.equals(elName)) {
				palettes = (List<Palette>)XmlUtils.parseAllChildren(el, new Palette(), new ArrayList<Palette>());
			}
			else if (THRESHOLDS.equals(elName)) {
				thresholds = XmlUtils.parseAllChildren(el, new Threshold(), new ArrayList<Threshold>());
			}
			else if (DATA_SOURCE_PATH.equals(elName)) {
				dataSourcePath = XmlUtils.getStringContent(el);
			}
			else if (ADHOC_REPORT.equals(elName)) {
				try {
					report = AdhocReport.convert(el, null);
				} catch (Exception e) {
					logger.warn(e,e);
				}
			}
			else if (CATEGORIES.equals(elName)) {
				categories = parseIntList(el);
			}
			else if (SERIES.equals(elName)) {
				series = parseIntList(el);
			}
			else if (SERIES2.equals(elName)) {
				series2 = parseIntList(el);
			}
			else if (SERIES3.equals(elName)) {
				series3 = parseIntList(el);
			}
			else if (THRESHOLDING.equals(elName)) {
				Integer i = XmlUtils.getIntContent(el);
				if (i != null)
					thresholding = i.intValue(); 
			}
			else if (COLORS.equals(elName)) {
				colorColumn = XmlUtils.getIntContent(el);
			}
			else if (MAP_NAMES.equals(elName)) {
				mapNames = parseFirstInt(el);
			}
			else if (AXES.equals(elName)) {
				axes = (Axes) XmlUtils.parseFirstChild(el, axes == null ? new Axes() : axes);
			}
			else if (CUSTOM_THRESHOLDS.equals(elName)) {
				customThresholds = XmlUtils.parseAllChildren(el, new CustomThreshold(), new ArrayList<CustomThreshold>());
			}
			else if (USE_CUSTOM_THRESHOLDS.equals(elName))
				useCustomThresholds = XmlUtils.getBooleanContent(el);
			else if (SHOW_VALUES.equals(elName))
				showValues = XmlUtils.getBooleanContent(el);
			else if (DISPLAY_VALUE_FORMAT.equals(elName))
				displayValueFormat = XmlUtils.getStringContent(el);		
			else if (DISPLAY_VALUE_FONT.equals(elName))
				displayValueFont = XmlUtils.getFontContent(el);		
			else if (DISPLAY_VALUE_COLOR.equals(elName))
				displayValueColor = XmlUtils.getColorContent(el);	
			else if (DISPLAY_VALUE_ROTATION.equals(elName))
				displayValueRotation = XmlUtils.getStringContent(el);					
			else if (TOOLTIP_FORMAT.equals(elName))
				tooltipFormat = XmlUtils.getStringContent(el);
			else if (TOOLTIP_FONT.equals(elName))
				tooltipFont = XmlUtils.getFontContent(el);		
			else if (TOOLTIP_COLOR.equals(elName))
				tooltipColor = XmlUtils.getColorContent(el);			
			else if (SHOW_VALUES_VERTICAL_ALIGNMENT.equals(elName))
				showValuesVerticalAlignment = BirstXMLSerializable.parseTypeFromString(XmlUtils.getStringContent(el), verticalAlignmentMap, DEFAULT_VERTICAL_ALIGNMENT);
			else if (UNIFORM_COLOR.equals(elName))
				useSameColor = XmlUtils.getBooleanContent(el);
			else if (SHOW_MEASURE_NAMES_IN_TOOLTIP.equals(elName))
				showMeasureNamesInTooltip = XmlUtils.getBooleanContent(el);
			else if (COLOR_LIST.equals(elName)) {
				String clrs = XmlUtils.getStringContent(el);
				if (clrs != null) {
					String[] colors = clrs.split(";");
					colorList = new ArrayList<Color>();
					for (String s: colors) {
						String[] color = s.split("=");
						if (color.length > 1) {
							Color c = new Color(Integer.valueOf(color[1]));
							colorList.add(c);
						}
					}
				}
			}
			else if (FILL_TYPE.equals(elName))
				fillType = parseTypeFromString(XmlUtils.getStringContent(el), fillTypeMap, DEFAULT_FILL_TYPE);
			else if (FILL_GRADIENT.equals(elName))
				fillGradient.parseElement(el);
		}
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void parseElement(XMLStreamReader parser) throws Exception {
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
			String elName = parser.getLocalName();
			if (LABEL.equals(elName)) {
				label = XmlUtils.getStringContent(parser);
			}
			else if (CHART_TYPE.equals(elName)) {
				chartType = parseTypeFromString(XmlUtils.getStringContent(parser), chartTypesMap, DEFAULT_CHART_TYPE);
			}
			else if (DATA_PLOT_SETTINGS.equals(elName)) {
				dataPlotSettings = (DataPlotSettings) XmlUtils.parseFirstChild(parser, dataPlotSettings == null ? new DataPlotSettings() : dataPlotSettings);
			}
			else if (PALETTES.equals(elName)) {
				palettes = (List<Palette>)XmlUtils.parseAllChildren(parser, new Palette(), new ArrayList<Palette>());
			}
			else if (THRESHOLDS.equals(elName)) {
				thresholds = XmlUtils.parseAllChildren(parser, new Threshold(), new ArrayList<Threshold>());
			}
			else if (DATA_SOURCE_PATH.equals(elName)) {
				dataSourcePath = XmlUtils.getStringContent(parser);
			}
			else if (ADHOC_REPORT.equals(elName)) {
				try {
					report = AdhocReport.convert(parser);
				} catch (Exception e) {
					logger.warn(e,e);
				}
			}
			else if (CATEGORIES.equals(elName)) {
				categories = parseIntList(parser);
			}
			else if (SERIES.equals(elName)) {
				series = parseIntList(parser);
			}
			else if (SERIES2.equals(elName)) {
				series2 = parseIntList(parser);
			}
			else if (SERIES3.equals(elName)) {
				series3 = parseIntList(parser);
			}
			else if (THRESHOLDING.equals(elName)) {
				Integer i = XmlUtils.getIntContent(parser);
				if (i != null)
					thresholding = i.intValue(); 
			}
			else if (COLORS.equals(elName)) {
				colorColumn = XmlUtils.getIntContent(parser);
			}
			else if (MAP_NAMES.equals(elName)) {
				mapNames = parseFirstInt(parser);
			}
			else if (AXES.equals(elName)) {
				axes = (Axes) XmlUtils.parseFirstChild(parser, axes == null ? new Axes() : axes);
			}
			else if (CUSTOM_THRESHOLDS.equals(elName)) {
				customThresholds = XmlUtils.parseAllChildren(parser, new CustomThreshold(), new ArrayList<CustomThreshold>());
			}
			else if (USE_CUSTOM_THRESHOLDS.equals(elName))
				useCustomThresholds = XmlUtils.getBooleanContent(parser);
			else if (SHOW_VALUES.equals(elName))
				showValues = XmlUtils.getBooleanContent(parser);
			else if (DISPLAY_VALUE_FORMAT.equals(elName))
				displayValueFormat = XmlUtils.getStringContent(parser);		
			else if (DISPLAY_VALUE_FONT.equals(elName))
				displayValueFont = XmlUtils.getFontContent(parser);		
			else if (DISPLAY_VALUE_COLOR.equals(elName))
				displayValueColor = XmlUtils.getColorContent(parser);
			else if (DISPLAY_VALUE_ROTATION.equals(elName))
				displayValueRotation = XmlUtils.getStringContent(parser);								
			else if (TOOLTIP_FORMAT.equals(elName))
				tooltipFormat = XmlUtils.getStringContent(parser);
			else if (TOOLTIP_FONT.equals(elName))
				tooltipFont = XmlUtils.getFontContent(parser);		
			else if (TOOLTIP_COLOR.equals(elName))
				tooltipColor = XmlUtils.getColorContent(parser);			
			else if (SHOW_VALUES_VERTICAL_ALIGNMENT.equals(elName))
				showValuesVerticalAlignment = BirstXMLSerializable.parseTypeFromString(XmlUtils.getStringContent(parser), verticalAlignmentMap, DEFAULT_VERTICAL_ALIGNMENT);
			else if (UNIFORM_COLOR.equals(elName))
				useSameColor = XmlUtils.getBooleanContent(parser);
			else if (SHOW_MEASURE_NAMES_IN_TOOLTIP.equals(elName))
				showMeasureNamesInTooltip = XmlUtils.getBooleanContent(parser);
			else if (COLOR_LIST.equals(elName)) {
				String clrs = XmlUtils.getStringContent(parser);
				if (clrs != null) {
					String[] colors = clrs.split(";");
					colorList = new ArrayList<Color>();
					for (String s: colors) {
						String[] color = s.split("=");
						if (color.length > 1) {
							Color c = new Color(Integer.valueOf(color[1]));
							colorList.add(c);
						}
					}
				}
			}
			else if (FILL_TYPE.equals(elName))
				fillType = parseTypeFromString(XmlUtils.getStringContent(parser), fillTypeMap, DEFAULT_FILL_TYPE);
			else if (FILL_GRADIENT.equals(elName))
				fillGradient.parseElement(parser);
		}
	}

	private List<Integer> parseFirstInt(OMElement parent) {
		List<Integer> l = parseIntList(parent);
		if (l == null || l.size() <= 1)
			return l;
		
		List<Integer> ret = new ArrayList<Integer>(1);
		ret.add(l.get(0));
		return ret;
	}
	private List<Integer> parseFirstInt(XMLStreamReader parent) throws Exception {
		List<Integer> l = parseIntList(parent);
		if (l == null || l.size() <= 1)
			return l;
		
		List<Integer> ret = new ArrayList<Integer>(1);
		ret.add(l.get(0));
		return ret;
	}

	@SuppressWarnings("unchecked")
	private List<Integer> parseIntList(OMElement parent) {
		List<Integer> ret = new ArrayList<Integer>();
		for (Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			ret.add(XmlUtils.getIntContent(el));
		}
		return ret;
	}
	
	private List<Integer> parseIntList(XMLStreamReader parser) throws Exception {
		List<Integer> ret = new ArrayList<Integer>();
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
			ret.add(XmlUtils.getIntContent(parser));
		}
		return ret;
	}


	@Override
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, LABEL, label, ns);
		XmlUtils.addContent(fac, parent, CHART_TYPE, chartTypesMap.get(chartType), ns);
		XmlUtils.addContent(fac, parent, DATA_PLOT_SETTINGS, dataPlotSettings, ns);
		if (palettes != null) {
			OMElement ps = fac.createOMElement(PALETTES, ns);
			for (Palette p : palettes) {
				XmlUtils.addContent(fac, ps, "Palette", p, ns);
			}
			parent.addChild(ps);
		}
		if (thresholds != null) {
			OMElement ts = fac.createOMElement(THRESHOLDS, ns);
			for (Threshold t : thresholds) {
				XmlUtils.addContent(fac, ts, "Threshold", t, ns);
			}
			parent.addChild(ts);
		}
		XmlUtils.addContent(fac, parent, DATA_SOURCE_PATH, dataSourcePath, ns);
		if (report != null) {
			report.addContent(parent, fac, ns);
		}
		addIntList(fac, parent, CATEGORIES, categories, ns);
		addIntList(fac, parent, SERIES, series, ns);
		addIntList(fac, parent, SERIES2, series2, ns);
		addIntList(fac, parent, SERIES3, series3, ns);
		if (thresholding >= 0) {
			XmlUtils.addContent(fac, parent, THRESHOLDING, thresholding, ns);
		}
		if (colorColumn != null)
			XmlUtils.addContent(fac, parent, COLORS, colorColumn, ns);
		addIntList(fac, parent, MAP_NAMES, mapNames, ns);
		XmlUtils.addContent(fac, parent, AXES, axes, ns);
		XmlUtils.addContent(fac, parent, CUSTOM_THRESHOLDS, customThresholds, ns);
		XmlUtils.addContent(fac, parent, USE_CUSTOM_THRESHOLDS, useCustomThresholds, ns);
		XmlUtils.addContent(fac, parent, SHOW_VALUES, showValues, ns);
		XmlUtils.addContent(fac, parent, DISPLAY_VALUE_FORMAT, displayValueFormat, ns);
		XmlUtils.addContent(fac, parent, DISPLAY_VALUE_FONT, displayValueFont, ns);
		XmlUtils.addContent(fac, parent, DISPLAY_VALUE_COLOR, displayValueColor, ns);
		XmlUtils.addContent(fac, parent, DISPLAY_VALUE_ROTATION, displayValueRotation, ns);		
		XmlUtils.addContent(fac, parent, TOOLTIP_FORMAT, tooltipFormat, ns);
		XmlUtils.addContent(fac, parent, TOOLTIP_FONT, tooltipFont, ns);
		XmlUtils.addContent(fac, parent, TOOLTIP_COLOR, tooltipColor, ns);		
		XmlUtils.addContent(fac, parent, SHOW_VALUES_VERTICAL_ALIGNMENT, verticalAlignmentMap.get(showValuesVerticalAlignment), ns);
		XmlUtils.addContent(fac, parent, UNIFORM_COLOR, useSameColor, ns);
		XmlUtils.addContent(fac, parent, SHOW_MEASURE_NAMES_IN_TOOLTIP, showMeasureNamesInTooltip, ns);
		if (colorList != null && colorList.size() > 0) {
			StringBuilder color = new StringBuilder();
			for (Color c: colorList) {
				int clr = c.getRGB() & 0xFFFFFF; 
				if (color.length() > 0)
					color.append(';');
				color.append("color=");
				color.append(clr);
			}
			
			XmlUtils.addContent(fac, parent, COLOR_LIST, color.toString(), ns);
		}
		XmlUtils.addContent(fac, parent, FILL_TYPE, fillTypeMap.get(fillType), ns);
		XmlUtils.addContent(fac, parent, FILL_GRADIENT, fillGradient, ns);
	}

	private void addIntList(OMFactory fac, OMElement parent,
			String name, List<Integer> list, OMNamespace ns) {
		if (list == null || list.isEmpty())
			return;
		
		OMElement l = fac.createOMElement(name, ns);
		for (Integer i: list) {
			XmlUtils.addContent(fac, l, "Int", i, ns);
		}
		parent.addChild(l);
	}


	@Override
	public BirstXMLSerializable createNew() {
		OMElement defaultChart = AdhocChart.getDefaults();
		if (defaultChart != null) {
			OMElement charts = defaultChart.getFirstChildWithName(new QName(AdhocChart.CHARTS));
			if (charts != null) {
				OMElement defaultNode = charts.getFirstElement();
				if (defaultNode != null)
					return new AnyChartNode(defaultNode);
			}
		}
		
		
		return new AnyChartNode();
	}

	public static OMElement getValidChartCombinations() {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;
		OMElement parent = fac.createOMElement("ValidChartCombinations", ns);
		for (ChartType ct : validChartCombinations.keySet()) {
			OMElement chartType = fac.createOMElement("Combination", ns);
			XmlUtils.addAttribute(fac, chartType, "ChartType", chartTypesMap.get(ct), ns);
			parent.addChild(chartType);
			List<ChartType> l = validChartCombinations.get(ct);
			if (l == null || l.isEmpty())
				continue;
			for (ChartType child: l) {
				XmlUtils.addContent(fac, chartType, "Valid", chartTypesMap.get(child), ns);
			}
		}
		return parent;
	}
	
	public static OMElement getValid3DCharts() {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;
		OMElement parent = fac.createOMElement("Valid3DCharts", ns);
		for (ChartType ct : chartsSupport3D) {
			XmlUtils.addContent(fac, parent, "Valid", chartTypesMap.get(ct), ns);
		}
		return parent;
	}
	
	private static void initializeClass() {
		chartTypesMap.put(ChartType.Area, "Area");
		chartTypesMap.put(ChartType.Bar, "Bar");
		chartTypesMap.put(ChartType.Bubble, "Bubble");
		chartTypesMap.put(ChartType.Line, "Line");
		chartTypesMap.put(ChartType.Pie, "Pie");
		chartTypesMap.put(ChartType.Doughnut, "Doughnut");
		chartTypesMap.put(ChartType.Pyramid, "Pyramid");
		chartTypesMap.put(ChartType.Scatter, "Scatter");
		chartTypesMap.put(ChartType.StackedBar, "StackedBar");
		chartTypesMap.put(ChartType.PercentStackedBar, "PercentStackedBar");
		chartTypesMap.put(ChartType.Gauge, "Gauge");
		chartTypesMap.put(ChartType.NewGauge, "NewGauge");
		chartTypesMap.put(ChartType.Treemap, "Treemap");
		chartTypesMap.put(ChartType.Heatmap, "Heatmap");
		chartTypesMap.put(ChartType.Funnel, "Funnel");
		chartTypesMap.put(ChartType.GeoMap, "GeoMap");
		chartTypesMap.put(ChartType.UMap, "UMap");
		chartTypesMap.put(ChartType.Marker, "Marker");
		chartTypesMap.put(ChartType.Column, "Column");
		chartTypesMap.put(ChartType.StackedColumn, "StackedColumn");
		chartTypesMap.put(ChartType.PercentStackedColumn, "PercentStackedColumn");
		chartTypesMap.put(ChartType.StackedArea, "StackedArea");
		chartTypesMap.put(ChartType.Radar, "Radar");
		
		chartTypesMap.put(ChartType.RangeBar, "RangeBar");
		chartTypesMap.put(ChartType.RangeColumn, "RangeColumn");
		chartTypesMap.put(ChartType.Waterfall, "Waterfall");
		
		chartsSupport3D.add(ChartType.Bar);
		chartsSupport3D.add(ChartType.Column);
		chartsSupport3D.add(ChartType.Pie);
		chartsSupport3D.add(ChartType.Doughnut);
		chartsSupport3D.add(ChartType.Pyramid);
		chartsSupport3D.add(ChartType.StackedBar);
		chartsSupport3D.add(ChartType.StackedColumn);
		chartsSupport3D.add(ChartType.PercentStackedBar);
		chartsSupport3D.add(ChartType.PercentStackedColumn);
		chartsSupport3D.add(ChartType.Funnel);
		chartsSupport3D.add(ChartType.RangeBar);
		chartsSupport3D.add(ChartType.RangeColumn);
		chartsSupport3D.add(ChartType.Waterfall);
		
		verticalAlignmentMap.put(VerticalAlignment.Bottom, "Bottom");
		verticalAlignmentMap.put(VerticalAlignment.Center, "Center");
		verticalAlignmentMap.put(VerticalAlignment.Top, "Top");
	}
	
	public static String chartTypeToString(ChartType type){
		return chartTypesMap.get(type);
	}
	
	public void generateQueryResultSet(JRDataSource dataSource, String selectorXml, ChartFilters chartFilters, AdhocReport parentReport, boolean applyAllPrompts, @SuppressWarnings("rawtypes") Map parameterMap, boolean isTrellisChart) throws Exception {
		if (Session.getRenderer() == Session.Renderer.Adhoc && parentReport.isHidedata())
			throw new NoDataException("Hiding data.");
		JasperDataSourceProvider jdsp = null;
		if (QueryResultSet.class.isInstance(dataSource))
			jdsp = ((IJasperDataSourceProvider) dataSource).getJasperDataSourceProvider();
		if (SingleRowDataSource.class.isInstance(dataSource)) {
			jdsp = ((IJasperDataSourceProvider) dataSource).getJasperDataSourceProvider();
		}
		if (jdsp == null)
			jdsp = new JasperDataSourceProvider(com.successmetricsinc.UserBean.getParentThreadUserBean().getRepository());
		
		if ((dataSourcePath == null || !Util.hasNonWhiteSpaceCharacters(dataSourcePath)) && dataSource instanceof QueryResultSet) {
			qrs = (QueryResultSet) dataSource;
			if(isTrellisChart){ 
				//Reset the data rows because the qrs was filtered and different from previous
				_dataRows = new DataRows(qrs);
			}
			if (parentReport.isShowNullZero())
				setRows(qrs.convertNullsToZeros());
			if (getRows().size() == 0) {
				throw new NoDataException("Query returned no data.\nTry removing some filters.");
			}			
			return;
		}
		
		logger.debug("Generating query result set for " + dataSourcePath);
		List<Filter> ps = jdsp.getParams();
		if (ps != null) {
			int size = ps.size();
			for (int i = 0; i < size; i++) {
				chartFilters.addFilter(ps.get(i));
			}
		}
		
		List<QueryFilter> queryFilters = chartFilters.getQueryFilters();
		if (queryFilters != null) {
			for (QueryFilter qf: queryFilters) {
				if (qf.isPrompted() && !parameterMap.containsKey(qf.getPromptName()))
					continue;
				
				if (qf.isPrompted() && QueryString.NO_FILTER_TEXT.equals(parameterMap.get(qf.getPromptName())))
					continue;
				
				List<String> v = new ArrayList<String>();
				if (qf.isPrompted()) {
					String value = (String)parameterMap.get(qf.getPromptName());
					if (value != null) {
						String[] parts = value.split("[\\|||\\&]{2}");
						for (int i = 0; i < parts.length; i++) {
							v.add(parts[i]);
						}
					}
					else 
						v.add(value);
				}
				else  {
					v.add(qf.getOperand());
				}
				StringBuilder b = new StringBuilder();
				if (qf.getDimension() != null) {
					b.append(qf.getDimension());
					b.append('.');
				}
				b.append(qf.getColumn());
				Filter f = new Filter(qf.getOperator(), v, "OR", FilterType.DATA, b.toString());
				chartFilters.addFilter(f);
			}
		}
		List<DisplayFilter> displayFilters = chartFilters.getDisplayFilters();
		if (displayFilters != null) {
			for (DisplayFilter df: displayFilters) {
				if (df.isPrompted())
					continue;
				
				List<String> v = new ArrayList<String>();
				v.add(df.getOperand().toString());
				Filter f = new Filter(df.getOperatorString(), v, "OR", FilterType.DISPLAY, df.getColumn());
				chartFilters.addFilter(f);
			}
		}
		
		chartFilters.normalizeFilters();
		
		qrs = getReport().getDashboardQueryResultSet(selectorXml, jdsp.getRepository(), 
				chartFilters.getFilters(), com.successmetricsinc.UI.SMIWebUserBean.getUserBean(), jdsp.getSession(), applyAllPrompts);
		
		if (parentReport.isShowNullZero() && qrs != null)
			setRows(qrs.convertNullsToZeros());
		
		DataRows dr = getRows();
		if (dr == null || dr.size() == 0) {
			throw new NoDataException("Query returned no data.\nTry removing some filters.");
		}
	}
	
	private DataRows getRows() {
		if (_dataRows == null && qrs != null) {
			_dataRows = new DataRows(qrs);
		}
		return _dataRows;
	}

	private void setRows(Object[][] newRows) {
		getRows().setRows(newRows);
	}

	public BirstCategoryDataset getCategoryDataset() throws Exception {
		return createCategoryDataset(qrs, categories, series, colorColumn);
	}

	public Element getXML(JRDataSource dataSource, String selectorXml, Element parent, ChartFilters chartFilters, ChartColors chartColors, double scale, boolean flipYAxis, XAxis xAxis, LegendSettings legendSettings, PlotType plotType, boolean isPrinted, AdhocReport parentReport, boolean applyAllPrompts, @SuppressWarnings("rawtypes") Map parameterMap, boolean isTrellisChart, AdhocChart adhocChart) throws Exception {
		try {
			additionalDrillParams.clear();
			selectors = null;
			this.legendSettings = legendSettings;
			if (selectorXml != null && Util.hasNonWhiteSpaceCharacters(selectorXml)) {
				AdhocReport report = getReport();
				if (report == null)
				{
					throw new SyntaxErrorException("Missing report or bad query within the report");
				}
				selectors = report.parseColumnSelectors(selectorXml);
			}
			if (colorList != null && colorList.size() > 0) {
				chartColors = new ChartColors((chartType == ChartType.Pie || chartType == ChartType.Doughnut) ? 1 : 0, 1);
				Color[] list = new Color[colorList.size()];
				for (int i = 0; i < list.length; i++) {
					list[i] = colorList.get(i);
				}
				chartColors.setColors(list);
			}
			if (xAxis != null) {
				labelFont = xAxis.getLabelFont();
				labelColor = xAxis.getLabelColor();
				this.scaleFactor = scale;
			}
			else {
				scaleFactor = 1.0;
			}
			if (xAxis != null && chartType != ChartType.Funnel && chartType != ChartType.Pie && chartType != ChartType.Doughnut)
				xAxis.getXML(parent, this, scaleFactor);
			if (axes != null && chartType != ChartType.Funnel && xAxis != null && chartType != ChartType.Pie && chartType != ChartType.Doughnut)
				yAxis = axes.getXML(parent, this, scaleFactor, flipYAxis, xAxis.getLabelFont(), xAxis.getLabelColor(), xAxis.getTitleFont(), xAxis.getTitleColor(), getStackedType(), xAxis.isShowInterlace(), xAxis);
			
			try {
			generateQueryResultSet(dataSource, selectorXml, chartFilters, parentReport, applyAllPrompts, parameterMap, isTrellisChart);
			}
			catch (NoDataException nde) {
				// ignore this for geo maps
				if (chartType != ChartType.GeoMap)
					throw nde;
			}
			
			if (chartType == ChartType.Bar || chartType == ChartType.Column)
				addBarChart(parent, chartColors, scaleFactor);
			else if (chartType == ChartType.RangeBar || chartType == ChartType.RangeColumn)
				addRangeBarChart(parent, chartColors, scaleFactor, false);
			else if (chartType == ChartType.Waterfall)
				addRangeBarChart(parent, chartColors, scaleFactor, true);
			else if (chartType == ChartType.Line)
				addLineChart(parent, chartColors, scaleFactor);
			else if (chartType == ChartType.GeoMap) 
				addGeoMapChart(parent, chartColors, scaleFactor, isPrinted, adhocChart);
			else if (chartType == ChartType.Scatter)
				addMarkerChart(parent, chartColors, plotType);
			else if (chartType == ChartType.Bubble)
				addBubbleChart(parent, chartColors, plotType, xAxis, axes);
			else if (chartType == ChartType.Area)
				addAreaChart(parent, chartColors, xAxis, false);
			else if (chartType == ChartType.Pie)
				addPieChart(parent, chartColors, scaleFactor, xAxis);
			else if (chartType == ChartType.Doughnut)
				addDoughnutChart(parent, chartColors, scaleFactor, xAxis);			
			else if (chartType == ChartType.Heatmap)
				addHeatMapChart(parent, scaleFactor, chartColors);
			else if (chartType == ChartType.Treemap)
				addTreeMapChart(parent, scaleFactor, xAxis, chartColors);
			else if (chartType == ChartType.StackedBar || chartType == ChartType.StackedColumn)
				addStackedBarChart(parent, chartColors, scaleFactor, xAxis, false);
			else if (chartType == ChartType.PercentStackedBar || chartType == ChartType.PercentStackedColumn)
				addStackedBarChart(parent, chartColors, scaleFactor, xAxis, true);			
			else if (chartType == ChartType.StackedArea)
				addStackedAreaChart(parent, chartColors, scaleFactor, xAxis, false);
			else if (chartType == ChartType.Funnel) 
				addFunnelChart(parent,  chartColors, scaleFactor, xAxis);
			else if (chartType == ChartType.Pyramid)
				addPyramidChart(parent, chartColors, scaleFactor, xAxis);
			else if (chartType == ChartType.Radar)
				addRadarChart(parent, chartColors, scaleFactor, xAxis);		
			else if (chartType == ChartType.UMap)
				addUMapChart(parent, chartColors, scaleFactor, isPrinted);
		} catch (NoDataException e) {
			logger.info(e.getMessage());
			throw e;	
		} catch (InvalidDataException e) {			
			logger.info(e.getMessage());
			throw e;
		} catch (InvalidColorException e) {			
			logger.info(e.getMessage());
			throw e;			
		} catch (Exception e) {
			logger.error(e,e);
			throw e;
		}
		return parent;
	}
	
	private void addRadarChart(Element chart, ChartColors chartColors,
			double scaleFactor, XAxis xAxis) throws Exception {
		if (this.dataPlotSettings != null && this.dataPlotSettings.getRadarSeries() != null) {
			this.dataPlotSettings.getRadarSeries().getXML(chart);
		}
		Element dataPlotSettingsEl = XmlUtils.findOrCreateChild(chart, "data_plot_settings");
		Element radarSeriesEl = XmlUtils.findOrCreateChild(dataPlotSettingsEl, "radar");
		setInteractivity(radarSeriesEl, false);
		addCategoryTooltips(radarSeriesEl);
		setLegend(chart, "{%Icon} {%Name}{enabled:False}", "Series");
		BirstCategoryDataset cs = getCategoryDataset();
		Map<String, Element> seriesMap = addCategoryPoints(cs, chart, "Bar", chartColors, scaleFactor, true);
		addDisplayValues(radarSeriesEl, series, seriesMap);
		
		RadarSeries rs = dataPlotSettings.getRadarSeries();
		if (!rs.isCloseContour())
		{
			if (seriesMap != null) {
				for (Element seriesEl : seriesMap.values()) {
					seriesEl.setAttribute("close_contour", "false");
				}
			}
		}
		if (rs.isAreaSeries())
		{
			if (seriesMap != null) {
				for (Element seriesEl : seriesMap.values()) {
					seriesEl.setAttribute("type", "Area");
				}
			}
		}
	}

	private void addPyramidChart(Element chart, ChartColors chartColors, double scaleFactor, XAxis xAxis) throws Exception {
		addFunnelChart(chart, chartColors, scaleFactor, xAxis);
		Element dataPlotSettings = XmlUtils.findOrCreateChild(chart, "data_plot_settings");
		Element funnelSeries = XmlUtils.findOrCreateChild(dataPlotSettings, "funnel_series");
		funnelSeries.setAttribute("neck_height", "0");
		funnelSeries.setAttribute("min_width", "0");
		funnelSeries.setAttribute("inverted", "true");
	}
	
	private void addFunnelChart(Element chart, ChartColors chartColors,
			double scaleFactor, XAxis xAxis) throws Exception {
		Element dataPlotSettingsEl = XmlUtils.findOrCreateChild(chart, "data_plot_settings");
		Element funnelSeriesEl = XmlUtils.findOrCreateChild(dataPlotSettingsEl, "funnel_series");
		funnelSeriesEl.setAttribute("point_padding", "0.025");
		funnelSeriesEl.setAttribute("group_padding", "0.5");
		
		if (this.dataPlotSettings != null)
		{
			funnelSeriesEl.setAttribute("mode", this.dataPlotSettings.getFunnelSeries().getFunnelMode()); 
		}			

		String formatStr = null;
		for (AdhocColumn col : getReport().getColumns()) {
			if (series.contains(Integer.valueOf(col.getReportIndex())))
				formatStr = col.getFormat();
		}
		String formatText =  "{%fullName}{enabled:False}: " + FlashRenderer.getAnychartNumberFormat("Value", formatStr) + " ({%YPercentOfSeries}{numDecimals:1}%)";
		addToolTip(funnelSeriesEl, formatText);
		addLabels(funnelSeriesEl, scaleFactor, "{%Name}{enabled:False}", xAxis);
		setInteractivity(funnelSeriesEl, false);
		
		BirstCategoryDataset cs = getCategoryDataset();
		setLegend(chart, "{%Icon} {%Name}{enabled:False}", "Points");
		Map<String, Element> seriesMap = addCategoryPoints(cs, chart, "Bar", chartColors, scaleFactor, true);
		showValuesVerticalAlignment = VerticalAlignment.Center; // always force this to be center, even if set to Top XXX
		addDisplayValues(funnelSeriesEl, series, seriesMap);
	}

	private void addHeatMapChart(Element chart, double scaleFactor, ChartColors chartColors) throws Exception {
		Element dataPlotSettings = XmlUtils.findOrCreateChild(chart, "data_plot_settings");
		Element heatMapEl = XmlUtils.findOrCreateChild(dataPlotSettings, "heat_map");
		heatMapEl.setAttribute("sort", "none");
		
		Element styleEl = XmlUtils.findOrCreateChild(heatMapEl, "heat_map_style");
//		XmlUtils.findOrCreateChild(styleEl, "effects").setAttribute("enabled", "False");
		XmlUtils.findOrCreateChild(styleEl, "border").setAttribute("color", "White");
		Element fillEl = XmlUtils.findOrCreateChild(styleEl, "fill");
		fillEl.setAttribute("type", fillTypeMap.get(fillType));
		fillEl.setAttribute("opacity", "1");
		if (fillType == FillType.Gradient) {
			fillGradient.getXML(fillEl);
		}

		String formatStr = null;
		for (AdhocColumn col : getReport().getColumns()) {
			if (series.contains(Integer.valueOf(col.getReportIndex()))) {
				formatStr = col.getFormat();
				break;
			}
		}
		String formatText = "{%Row}{enabled:False}, {%Column}{enabled:False}: " +
			FlashRenderer.getAnychartNumberFormat("Value", formatStr);
		addToolTip(heatMapEl, formatText);
		String displayValueFormatText = (formatStr == null ? "{%Value}" : FlashRenderer.getAnychartNumberFormat("Value", formatStr));
		showValuesVerticalAlignment = VerticalAlignment.Center; // always force this to be center, even if set to Top XXX
		addDisplayValues(heatMapEl, displayValueFormatText, null);
		setInteractivity(heatMapEl, false);
		String threshold = "MyThreshold";
		
		MapSeries mapSeries = getMapSeries();
		if (mapSeries != null)
			addPalettesAndThresholds(chart, threshold, mapSeries.getThresholdType(), mapSeries.numThresholds, chartColors, mapSeries.customThresholds);
		
		Element data = XmlUtils.findOrCreateChild(chart, "data");
		Element seriesEl = XmlUtils.findOrCreateChild(data, "series");
		seriesEl.setAttribute("threshold", threshold);
		
		// 2 categories, 1 series
		AdhocColumn cat1Col = null;
		AdhocColumn cat2Col = null;
		AdhocColumn seriesCol = null;
		String aggregationRule = "SUM";
		for (AdhocColumn col : getReport().getColumns()) {
			if (categories.contains(Integer.valueOf(col.getReportIndex()))) {
				if (cat1Col == null) {
					cat1Col = col;
				}
				else if (cat2Col == null) {
					cat2Col = col;
				}
			}
			if (series.contains(Integer.valueOf(col.getReportIndex())) && seriesCol == null) {
				seriesCol = col;
				aggregationRule = col.getExprAggRule();
			}
		}
		
		if (cat1Col == null || cat2Col == null || seriesCol == null) {
			throw new InvalidDataException("Not enough categories or series for heat map.");
		}
		
		Element chartSettingsEl = XmlUtils.findOrCreateChild(chart, "chart_settings");
		Element controlsEl = XmlUtils.findOrCreateChild(chartSettingsEl, "controls");
		if (mapSeries.enableColorSwatch && ! MapSeries.CUSTOM.equals(mapSeries.getThresholdType())) { 
			Element colorSwatchEl = XmlUtils.findOrCreateChild(controlsEl, "color_swatch");
			colorSwatchEl.setAttribute("threshold", threshold);
			colorSwatchEl.setAttribute("position", "Bottom");
			colorSwatchEl.setAttribute("width", "100%");
			
			Element labelsEl = XmlUtils.findOrCreateChild(colorSwatchEl, "labels");
			labelsEl.setAttribute("enabled", "True");
			
			addFormat(labelsEl, FlashRenderer.getAnychartNumberFormat("Value", seriesCol.getFormat()));
		}
		
		java.util.TreeMap<String, java.util.TreeMap<String, Cell>> rowsMap = new java.util.TreeMap<String, java.util.TreeMap<String, Cell>>();
		List<String> rowsSortOrder = new LinkedList<String>();
		List<String> colsSortOrder = new LinkedList<String>();
		int rowCount = 0;
		for (DataRows.DataRow row : getRows())
		{
			Object d = row.getFieldValue(seriesCol.getFieldName(), seriesCol);
			if (FlashRenderer.unsupportedValueToChart(d))
				continue;

			if (((Number)d).doubleValue() < 0.0)
				throw new InvalidDataException("This type of chart can not have negative values.  Please filter your data appropriately.");
				
			if (rowCount++ > Chart.MAX_DATASET_SIZE) {
				throw new InvalidDataException("Too many rows were returned for this chart.  Please filter your data appropriately.");
			}
			
			Object o = formatVal(row, cat1Col);
			String rowString = (o == null ? "" : o.toString());
			o = formatVal(row, cat2Col);
			String colString = (o == null ? "" : o.toString());
			
			java.util.TreeMap<String, Cell> rowMapEntry = rowsMap.get(rowString);
			if (rowMapEntry == null) {
				rowMapEntry = new java.util.TreeMap<String, Cell>();
				rowsMap.put(rowString, rowMapEntry);
				if (!rowsSortOrder.contains(rowString))
					rowsSortOrder.add(rowString);
			}
			saveAdditionalDrillValues(rowString, row);
			Cell val = (Cell)rowMapEntry.get(colString);
			if (val == null) {
				val = new Cell();
				val.aggRule = aggregationRule;
				val.value = ((Number)d).doubleValue();
				val.count = 1;
				if (!colsSortOrder.contains(colString))
					colsSortOrder.add(colString);
			}
			else {
				val.value = (Double)aggregate(aggregationRule, val.value, (Number)d);
				val.count++;
			}
			rowMapEntry.put(colString, val);
			
		}
		
		for (java.util.TreeMap<String, Cell> rowMapEntry : rowsMap.values()) {
			for (Cell c : rowMapEntry.values())
			{
				if (c.aggRule != null && c.aggRule.equals("AVG")) {
					if (c.value instanceof BirstInterval) {
						BirstInterval bi = (BirstInterval)c.value;
						c.value = new BirstInterval(bi.doubleValue() / c.count, bi.getFields(), bi.getFormat());
					}
					else if (c.value instanceof Double) {
						c.value = (Double)c.value / c.count;
					}
					else if (c.value instanceof Integer) {
						c.value = (Integer)c.value / c.count;
					}
				}
			}
		}
		
		DrillData dd = new DrillData();
		List<Element> points = new ArrayList<Element>();
		for (String rowKey : rowsSortOrder) {
			java.util.TreeMap<String, Cell> columns = rowsMap.get(rowKey);
			if (columns != null) {
			for (String colKey: colsSortOrder) {
				Element pt = new Element("point");
				pt.setAttribute("row", rowKey);
				pt.setAttribute("column", colKey);
				Cell cell = columns.get(colKey);
				Number d = cell == null ? null : cell.value;
					if (d != null)
						pt.setAttribute("y", d.toString());
					else
						continue; // do not send row/colum without y to AnyChart, it displays a NaN blue colored square 12610
					if (d instanceof BirstInterval) {
						String tooltip = ToTime.evaluate(((BirstInterval) d).doubleValue(), ((BirstInterval) d).getFields(), ((BirstInterval) d).getFormat());
						addToolTip(pt, tooltip, "tooltip");
						
						if (this.showValues) {
							Element labelEl = XmlUtils.findOrCreateChild(pt, "label");
							labelEl.setAttribute("enabled", "True");
							addFormat(labelEl, tooltip);
						}
					}
				Element attributesEl = XmlUtils.findOrCreateChild(pt, "attributes");
				CellKey cKey = new CellKey();
				// first category is rowKey, second is colKey 
				cKey.add(rowKey);
				cKey.add(colKey);
				dd.addExtraData(pt, attributesEl, cKey, null);
				points.add(pt);
			}
		}
		}
		if (points.size() > 0) 
			seriesEl.addContent(points);
		
		setLegend(chart, 
				(isCustomThresholds() ?
						"{%Icon} {%Name}" :
						"{%Icon} {%RangeMin} - {%RangeMax}"),
				"Thresholds");
	}
	
	/* 
	 * Saves additional drill parameters for later
	 * If the key does not exist, add a new map containing an entry for each column
	 * If it does exist, remove anything from the map where this row has a different value than the previous row
	 * When all rows have been iterated, only values that are the same in all rows that have been rolled up may be used
	 * as an additional drill param
	 */
	private void saveAdditionalDrillValues(String rowString, DataRow row) throws InvalidDataException {
		if (rowString == null || rowString.length() == 0)
			return;
		
		Map<String, Object> additionalParams = additionalDrillParams.get(rowString);
		if (additionalParams == null) {
			additionalParams = new HashMap<String, Object>();
			additionalDrillParams.put(rowString, additionalParams);
			for (String name : row.getColNames()) {
				if (name == null || name.trim().length() == 0)
					continue;
				
				additionalParams.put(name, row.getFieldValue(name, null));
			}
		}
		else {
			for (String name : row.getColNames()) {
				if (name == null || name.trim().length() == 0)
					continue;
				Object value = additionalParams.get(name);
				if (value != null &&
						! value.equals(row.getFieldValue(name, null)))
				additionalParams.remove(name);
			}
		}
	}

	private MapSeries getMapSeries() {
		if (dataPlotSettings != null)
			return dataPlotSettings.getMapSeries();
		
		return null;
	}

	private boolean isCustomThresholds() {
		boolean response = false;
		MapSeries mapSeries = getMapSeries();
		if(mapSeries != null)
		{
			response = mapSeries.customThresholds != null && mapSeries.customThresholds.size() > 0;
		}
		return  response;
	}
	
	private void addTreeMapChart(Element chart, double scaleFactor, XAxis xAxis, ChartColors chartColors) throws Exception {
		BirstCategoryDataset cs = getCategoryDataset();
		Element dataPlotSettings = XmlUtils.findOrCreateChild(chart, "data_plot_settings");
		Element treeMapEl = XmlUtils.findOrCreateChild(dataPlotSettings, "tree_map");
		treeMapEl.setAttribute("sort", "none");
		String formatStr = null;
		for (AdhocColumn col : getReport().getColumns()) {
			if (series.contains(Integer.valueOf(col.getReportIndex()))) {
				formatStr = col.getFormat();
				break;
			}
		}
		String formatText = "{%fullName}{enabled:False}: " +
			FlashRenderer.getAnychartNumberFormat("Value", formatStr);
		addToolTip(treeMapEl, formatText);
		
		// XXX ugly encapsulation, should fix later
		String displayValueFormatText = (formatStr == null ? "{%Value}" : FlashRenderer.getAnychartNumberFormat("Value", formatStr));
		showValuesVerticalAlignment = VerticalAlignment.Center; // always force this to be center, even if set to Top XXX
		String labelText = "{%fullName}{enabled:False}";
		if (showValues)
			labelText = labelText + ":\n" + displayValueFormatText;
		addLabels(treeMapEl, scaleFactor, labelText, xAxis);
		if (!xAxis.showDomainLabels())
			addDisplayValues(treeMapEl, displayValueFormatText, null);
		setInteractivity(treeMapEl, false);
		
		Element styleEl = XmlUtils.findOrCreateChild(treeMapEl, "tree_map_style");
//		XmlUtils.findOrCreateChild(styleEl, "effects").setAttribute("enabled", "False");
		XmlUtils.findOrCreateChild(styleEl, "border").setAttribute("color", "White");
		Element fillEl = XmlUtils.findOrCreateChild(styleEl, "fill");
		fillEl.setAttribute("type", fillTypeMap.get(fillType));
		fillEl.setAttribute("opacity", "1");
		if (fillType == FillType.Gradient) {
			fillGradient.getXML(fillEl);
		}
		
		String threshold = "MyThreshold";
		MapSeries mapSeries = getMapSeries();
		boolean colorSet = (cs.categoryColorMap != null && cs.categoryColorMap.size() > 0);
		if (mapSeries != null && ! colorSet)
			addPalettesAndThresholds(chart, threshold, mapSeries.getThresholdType(), mapSeries.numThresholds, chartColors, mapSeries.customThresholds);
		
		addCategoryPoints(cs, chart, threshold);
		
		Element chartSettingsEl = XmlUtils.findOrCreateChild(chart, "chart_settings");
		Element controlsEl = XmlUtils.findOrCreateChild(chartSettingsEl, "controls");
		if (mapSeries != null && mapSeries.enableColorSwatch && ! MapSeries.CUSTOM.equals(mapSeries.getThresholdType()) && ! colorSet) { 
			Element colorSwatchEl = XmlUtils.findOrCreateChild(controlsEl, "color_swatch");
			colorSwatchEl.setAttribute("threshold", threshold);
			colorSwatchEl.setAttribute("position", "Bottom");
			colorSwatchEl.setAttribute("width", "100%");
			
			Element labelsEl = XmlUtils.findOrCreateChild(colorSwatchEl, "labels");
			labelsEl.setAttribute("enabled", "True");
			
			addFormat(labelsEl, FlashRenderer.getAnychartNumberFormat("Value", formatStr));
		}
		
		if (colorSet) {
			setLegend(chart, "{%Icon} {%Name}{enabled:False}", "Points");
		}
		else {
			setLegend(chart, 
				(isCustomThresholds() ?
						"{%Icon} {%Name}" :
						"{%Icon} {%RangeMin} - {%RangeMax}"),
				"Thresholds");
		}
	}
	
	private void addLabels(Element parent, double scaleFactor, String formatText, XAxis xAxis) {
		if (xAxis.showDomainLabels()) {
			Element extraLabelSettingsEl = XmlUtils.findOrCreateChild(parent, "extra_labels");
			Element labelSettingsEl = XmlUtils.findOrCreateChild(extraLabelSettingsEl, "label");
			labelSettingsEl.setAttribute("enabled", "true");
			labelSettingsEl.setAttribute("multi_line_align", "Center");
			Element backgroundEl = XmlUtils.findOrCreateChild(labelSettingsEl, "background");
			backgroundEl.setAttribute("enabled", "false");
			Element positionEl = addPosition(labelSettingsEl, "Center", "Center", "Center", "5");
			Element formatEl = XmlUtils.findOrCreateChild(labelSettingsEl, "format");
			formatEl.setText(formatText);
			Element f = BirstXMLSerializable.getFontElement(xAxis.getLabelFont(), scaleFactor);
			AnyChartNode.addColorAttribute(f, xAxis.getLabelColor());
			labelSettingsEl.addContent(f);
		}
	}

	private void setInteractivity(Element parent, boolean b) {
		Element interactivityEl = XmlUtils.findOrCreateChild(parent, "interactivity");
		interactivityEl.setAttribute("allow_select", b ? "True" : "False");
	}
	
	private void addToolTip(Element parent, String formatText) {
		addToolTip(parent, formatText, "tooltip_settings");
	}
	private void addToolTip(Element parent, String formatText, String elementName) {
		Element toolTipSettingsEl = XmlUtils.findOrCreateChild(parent, elementName);
		toolTipSettingsEl.setAttribute("enabled", "True");
		if (tooltipFont != null)
		{
			BirstXMLSerializable.addFontElement(toolTipSettingsEl, tooltipFont, scaleFactor);
		}
		if (tooltipColor != null)
		{
			Element f = XmlUtils.findOrCreateChild(toolTipSettingsEl, "font");
			AnyChartNode.addColorAttribute(f, tooltipColor);
		}
		if (tooltipFormat != null)
			formatText = tooltipFormat;
		addFormat(toolTipSettingsEl, formatText);
	}
	
	public void addFormat(Element parent, String formatText) {
		Element formatEl = XmlUtils.findOrCreateChild(parent, "format");
		formatEl.setText(formatText);
	}
	
	private void addPalettesAndThresholds(Element chart, String thresholdName, String thresholdType, int numThresholds, ChartColors chartColors, List<CustomThreshold> customThresholds) {
		// for now just use 3 color palette
		String paletteName = "MyColorPalette";
		Element palettes = XmlUtils.findOrCreateChild(chart, "palettes");
		Element palette = new Element("palette");
		palette.setAttribute("name", paletteName);
		palette.setAttribute("type", "ColorRange");
		palette.setAttribute("color_count", String.valueOf(numThresholds));
		palettes.addContent(palette);
		Element gradientEl = XmlUtils.findOrCreateChild(palette, "gradient");
		if (chartColors != null && chartColors.getNumColors() >= 2) {
			for (int i = 0; i < chartColors.getNumColors(); i++) {
				Element keyEl = new Element("key");
				addColorAttribute(keyEl, chartColors);
				gradientEl.addContent(keyEl);
			}
		}
		else {
			Element keyEl = new Element("key");
			keyEl.setAttribute("color", "green");
			gradientEl.addContent(keyEl);
			if (chartType != ChartType.GeoMap) {
				keyEl = new Element("key");
				keyEl.setAttribute("color", "yellow");
				gradientEl.addContent(keyEl);
			}
			keyEl = new Element("key");
			keyEl.setAttribute("color", "red");
			gradientEl.addContent(keyEl);
		}
		
		Element thresholds = XmlUtils.findOrCreateChild(chart, "thresholds");
		Element threshold = new Element("threshold");
		threshold.setAttribute("name", thresholdName);
		threshold.setAttribute("type", thresholdType == null ? "Quantiles" : thresholdType);
		
		if ("Custom".equals(thresholdType) && customThresholds != null) {
			// add custom thresholds based on customThresholds
			for (CustomThreshold customThresh : customThresholds) {
				Element el = new Element("condition");
				el.setAttribute("name", customThresh.label);
				el.setAttribute("value_1", "{%Value}");
				if (customThresh.minValue == null) {
					el.setAttribute("type", "lessThan");
					el.setAttribute("value_2", String.valueOf(customThresh.maxValue));
				}
				else if (customThresh.maxValue == null) {
					el.setAttribute("type", "greaterThanOrEqualTo");
					el.setAttribute("value_2", String.valueOf(customThresh.minValue));
				}
				else {
					el.setAttribute("type", "between");
					el.setAttribute("value_2", String.valueOf(customThresh.minValue));
					el.setAttribute("value_3", String.valueOf(customThresh.maxValue));
				}
				addColorAttribute(el, customThresh.color);
				threshold.addContent(el);
			}
			threshold.setAttribute("range_count", String.valueOf(customThresholds.size()));
		}
		else {
			threshold.setAttribute("palette", paletteName);
			threshold.setAttribute("range_count", String.valueOf(numThresholds));
		}
		thresholds.addContent(threshold);
	}
	
	private void addPieChart(Element chart, ChartColors chartColors, double scaleFactor, XAxis xAxis) throws Exception {
		addPieChart(chart, chartColors, scaleFactor, xAxis, false);
	}

	private void addDoughnutChart(Element chart, ChartColors chartColors, double scaleFactor, XAxis xAxis) throws Exception {
		addPieChart(chart, chartColors, scaleFactor, xAxis, true);
	}

	private void addPieChart(Element chart, ChartColors chartColors, double scaleFactor, XAxis xAxis, boolean doughnut) throws Exception {
		Element dataPlotSettingsEl = XmlUtils.findOrCreateChild(chart, "data_plot_settings");
		if (this.dataPlotSettings != null && this.dataPlotSettings.getPieSeries() != null) {
			this.dataPlotSettings.getPieSeries().getXML(chart);
		}
		Element pieSeries = XmlUtils.findOrCreateChild(dataPlotSettingsEl, "pie_series");
		Element labelSettings = XmlUtils.findOrCreateChild(pieSeries, "label_settings");
		labelSettings.setAttribute("enabled", "false");
		if (xAxis.showDomainLabels()) {
			labelSettings.setAttribute("enabled", "true");
			labelSettings.setAttribute("mode", "Outside");
			labelSettings.setAttribute("multi_line_align", "Center");
			String formatText = xAxis.getXAxisFormat(this, "{%Name}");
			addFormat(labelSettings, formatText);
			Element f = BirstXMLSerializable.getFontElement(xAxis.getLabelFont(), scaleFactor);
			AnyChartNode.addColorAttribute(f, xAxis.getLabelColor());
			labelSettings.addContent(f);
			Element connectorSettings = XmlUtils.findOrCreateChild(pieSeries, "connector");
			connectorSettings.setAttribute("enabled", "true");
			//AnyChartNode.addColorAttribute(connectorSettings, xAxis.getLabelColor());
			connectorSettings.setAttribute("color", "#666666");
			connectorSettings.setAttribute("opacity", "0.4");
			connectorSettings.setAttribute("thickness", "1");
		}
		
		pieSeries.setAttribute("default_series_type", "Pie");
		pieSeries.setAttribute("style", "Default");
		
		Element pieStyleEl = XmlUtils.findOrCreateChild(pieSeries, "pie_style");
		XmlUtils.findOrCreateChild(pieStyleEl, "border").setAttribute("enabled", "False");
		Element fillEl = XmlUtils.findOrCreateChild(pieStyleEl, "fill");
		fillEl.setAttribute("enabled", "True");
		fillEl.setAttribute("type", fillTypeMap.get(fillType));
		fillEl.setAttribute("opacity", "1");
		if (fillType == FillType.Gradient) {
			fillGradient.getXML(fillEl);
		}
		
		setHoverColor(pieStyleEl);

		String formatStr = null;
		StringBuilder label = new StringBuilder();;
		for (AdhocColumn col : getReport().getColumns()) {
			if (series.contains(Integer.valueOf(col.getReportIndex()))) {
				if (label.length() > 0) {
					label.append(" / ");
				}
				label.append(col.getLabelString());
				formatStr = col.getFormat();
			}
		}
		
		UserBean ub = com.successmetricsinc.UserBean.getParentThreadUserBean();
		DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance(ub != null ? ub.getLocale() : Locale.getDefault());
		String formatText = "{%Name}{enabled:False}: " +
			FlashRenderer.getAnychartNumberFormat("Value", formatStr) +
			" ({%YPercentOfSeries}{useNegativeSign:false,leadingZeros:1,numDecimals:1," 
			+ "decimalSeparator:" + FlashRenderer.getEscapedSymbol(symbols.getDecimalSeparator()) + ",thousandsSeparator:}%)";
		addToolTip(pieSeries, formatText);
		if (showValues) {
			Element labelSettingsEl = XmlUtils.findOrCreateChild(pieSeries, "label_settings");
			if (xAxis.showDomainLabels()) {
				// create an extra_labels
				Element extraLabels = XmlUtils.findOrCreateChild(pieSeries, "extra_labels");
				labelSettingsEl = new Element("label");
				Element f = BirstXMLSerializable.getFontElement(xAxis.getLabelFont(), scaleFactor);
				AnyChartNode.addColorAttribute(f, xAxis.getLabelColor());
				labelSettingsEl.addContent(f);
				extraLabels.addContent(labelSettingsEl);
			}
			labelSettingsEl.setAttribute("enabled", "True");
			String formatText2;
			if (displayValueFormat != null)
				formatText2 = displayValueFormat;
			else
				formatText2 = FlashRenderer.getAnychartNumberFormat("Value", formatStr);
			addFormat(labelSettingsEl, formatText2);
			if (displayValueFont != null)
			{
				BirstXMLSerializable.addFontElement(labelSettingsEl, displayValueFont, scaleFactor);
			}
			if (displayValueColor != null)
			{
				Element f = XmlUtils.findOrCreateChild(labelSettingsEl, "font");
				AnyChartNode.addColorAttribute(f, displayValueColor);
			}			
			addPosition(labelSettingsEl, "Center", "Center", "Center", null);
			labelSettingsEl.addContent(BirstXMLSerializable.getFontElement(xAxis.getLabelFont(), scaleFactor));
		}

		Element dataEl = XmlUtils.findOrCreateChild(chart, "data");
		Element seriesEl = new Element("series");
		seriesEl.setAttribute("name", label.toString());
		seriesEl.setAttribute("type", "Pie");
		dataEl.addContent(seriesEl);
		DrillData dd = new DrillData();
		BirstCategoryDataset cs = getCategoryDataset();
		int numRows = cs.getRowCount();
		int count = 0;
		int totalCount = 0;
		// calculate the total, so we can disable labels below a threshold
		double total = 0;
		for (int j = 0; j < numRows; j++)
		{
			for (int i = 0; i < cs.getColumnCount(); i++)
			{
				Object d = cs.getValue(j, i);
				if ((d == null) || (d.toString().length() == 0))
					continue;
				if (((Number)d).doubleValue() < 0.0)
					continue;
				total += ((Number) d).doubleValue();
				totalCount++;
			}
		}
		for (int j = 0; j < numRows; j++)
		{
			CellKey o = (CellKey)cs.getRowKey(j);
			
			String columnName = o.get(o.size() - 1).toString();
			DecimalFormat df = getNumberFormatForData(columnName, symbols);
			
			for (int i = 0; i < cs.getColumnCount(); i++)
			{
				Object d = cs.getValue(j, i);
				if ((d == null) || (d.toString().length() == 0))
					continue;
				if (((Number)d).doubleValue() < 0.0) {
					throw new InvalidDataException("Pie charts can not have negative values.  Please filter your data appropriately.");
				}
				if (((Number)d).doubleValue() > Math.pow(10, df.getMaximumIntegerDigits()))
					throw new InvalidDataException("A value for column " + columnName + " is too large.  Please check your calculations.");
				CellKey ko = (CellKey)cs.getColumnKey(i);
				String key = (ko == null) ? "" : ko.toString();
				Element p = new Element("point");
				p.setAttribute("name", key);
				p.setAttribute("y", df.format(d));
				if (d instanceof BirstInterval) {
					String tooltip = ToTime.evaluate(((BirstInterval) d).doubleValue(), ((BirstInterval) d).getFields(), ((BirstInterval) d).getFormat());
					addToolTip(p, tooltip, "tooltip");
					
					if (this.showValues) {
						Element labelEl = XmlUtils.findOrCreateChild(p, "label");
						labelEl.setAttribute("enabled", "True");
						addFormat(labelEl, tooltip);
					}
				}
				CellKey colorKey = new CellKey();
				if (ko != null)
					colorKey.addAll(ko);
				colorKey.addAll(o);
				addColorAttribute(p, cs.categoryColorMap, colorKey.toString(), chartColors);
				Element attributes = XmlUtils.findOrCreateChild(p, "attributes");
				// do not show values less than PIE_LABEL_THRESHOLD of the pie (AnyChart has a really bad label layout algorithm)
				// XXX bug in AnyChart - must have atleast two labels displayed to get any labels
				if ((((Number) d).doubleValue() / total) < PIE_LABEL_THRESHOLD && count < (totalCount - 2))
				{
					count++;
					Element labelEl = XmlUtils.findOrCreateChild(p, "label");
					labelEl.setAttribute("enabled", "False");
				}				
				dd.addExtraData(p, attributes, ko, o);
				seriesEl.addContent(p);
			}
		}
		/* XXX fix this up to allow multiple series - just need to fix up labels
		BirstCategoryDataset cs = getCategoryDataset();
		Map<String, Element> seriesMap = addCategoryPoints(cs, chart, "Pie", chartColors);
		addDisplayValues(pieSeries, series, seriesMap);
		*/
		setLegend(chart, "{%Icon} {%Name}{enabled:False}", "Points");
	}
	
	private DecimalFormat getNumberFormatForData(String columnName, DecimalFormatSymbols symbols) {
		return getNumberFormatForData(columnName, symbols, false);
	}

	private DecimalFormat getNumberFormatForData(String columnName, DecimalFormatSymbols symbols, boolean ignorePercent) {
		String format = convertColumnNameToFormat(columnName);
		if (format == null || !Util.hasNonWhiteSpaceCharacters(format))
			format = "#";
		StringBuilder newFormat = new StringBuilder();
		newFormat.append('#');
		boolean dotFound = false;
		boolean percentFound = false;
		for (int k = 0; k < format.length(); k++) {
			char c = format.charAt(k);
			if (c == '.') {	// XXX all '.' should really be symbols.getDecimalSeparator()
				dotFound = true;
				newFormat.append('.');
			}
			else if (c == '#') {
				if (dotFound)
					newFormat.append('#');
			}
			else if (c == '0')
				newFormat.append('0');
			else if (c == '%') {
				percentFound = true;
				if (dotFound && !ignorePercent)
					newFormat.append("##");
			}
			else if (c == ';')
				break;
		}
		if (percentFound && !dotFound && !ignorePercent) {
			newFormat.append('.').append("##");
		}
		if (percentFound && ignorePercent)
			newFormat.append('%');
		DecimalFormat df = new DecimalFormat(newFormat.toString() /*, symbols */);
		df.setMaximumIntegerDigits(300);
		return df;
	}

	private void addAreaChart(Element chart, ChartColors chartColors, XAxis xAxis, boolean includePercent) throws Exception {
		BirstCategoryDataset cs = getCategoryDataset();
		Element dataPlotSettingsEl = XmlUtils.findOrCreateChild(chart, "data_plot_settings");
		Element areaSettingEl = XmlUtils.findOrCreateChild(dataPlotSettingsEl, "area_series");
		Element tooltipSettingsEl = XmlUtils.findOrCreateChild(areaSettingEl, "tooltip_settings");
		tooltipSettingsEl.setAttribute("enabled", "True");
		
		setLegend(chart, "{%Icon} {%Name}{enabled:False}", "Series");
		AreaSeries as = dataPlotSettings.getAreaSeries();
		if (seriesContainsDimension())
		{
			Map<String, Element> seriesMap = addSeriesPoints(chart, cs, as.getType(), chartColors, xAxis, includePercent);
			addDisplayValues(areaSettingEl, series, seriesMap);
		} else {
			addCategoryPoints(cs, chart, as.getType(), chartColors);
		}
	}

	private boolean seriesContainsDimension() {
		for (AdhocColumn col : getReport().getColumns()) {
			String dim = col.getDimension();
			if (series.contains(Integer.valueOf(col.getReportIndex())) && dim != null && Util.hasNonWhiteSpaceCharacters(dim) && !dim.equals("EXPR"))
				return true;
			if (series2 != null && series2.contains(Integer.valueOf(col.getReportIndex())) && dim != null && Util.hasNonWhiteSpaceCharacters(dim) && !dim.equals("EXPR"))
				return true;
			if (series3 != null && series3.contains(Integer.valueOf(col.getReportIndex())) && dim != null && Util.hasNonWhiteSpaceCharacters(dim) && !dim.equals("EXPR"))
				return true;
		}
		return false;
	}

	private void addBubbleChart(Element chart, ChartColors chartColors, PlotType plotType, XAxis xAxis, Axes axes) throws BaseException {
		if (this.dataPlotSettings != null && this.dataPlotSettings.getBubbleSeries() != null) {
			this.dataPlotSettings.getBubbleSeries().getXML(chart, this);
		}
		
		String xAxisFormatStr = "#";
		String yAxisFormatStr = "#";
		String zAxisFormatStr = "#";
		String xAxisLabel = null;
		String yAxisLabel = null;
		String bubbleSizeLabel = null;
		for (AdhocColumn col : getReport().getColumns()) {
			Integer reportIndex = Integer.valueOf(col.getReportIndex()); 
			if (series.contains(reportIndex)) {
				xAxisFormatStr = col.getFormat();
				String label = xAxis.getxLabel();
				if (label == null || label.trim().length() == 0)
					xAxisLabel = col.getLabelString();
			}
			else if (series2 != null && series2.contains(reportIndex)) {
				yAxisFormatStr = col.getFormat();
				String label = axes.getyLabel();
				if (label == null || label.trim().length() == 0)
					yAxisLabel = col.getLabelString();
			}
			else if (series3 != null && series3.contains(reportIndex)) {
				zAxisFormatStr = col.getFormat();
				bubbleSizeLabel = col.getLabelString();
			}
		}
		if (xAxisLabel == null)
			xAxisLabel = "";
		else
			xAxisLabel += ": ";
		if (yAxisLabel == null)
			yAxisLabel = "";
		else
			yAxisLabel += ": ";
		if (bubbleSizeLabel == null)
			bubbleSizeLabel = "";
		else
			bubbleSizeLabel += ": ";
		
		String formatText = "{%SeriesName}{enabled:False}" +  
			"\n" + xAxisLabel + 
			FlashRenderer.getAnychartNumberFormat("XValue", xAxisFormatStr) +
			"\n" + yAxisLabel +  
			FlashRenderer.getAnychartNumberFormat("YValue", yAxisFormatStr) +
			"\n" + bubbleSizeLabel +   
			FlashRenderer.getAnychartNumberFormat("BubbleSize", zAxisFormatStr);
		if (plotType == PlotType.Map) {
			formatText = "{%SeriesName}{enabled:False}" +  
			"\n" + bubbleSizeLabel + 
			FlashRenderer.getAnychartNumberFormat("BubbleSize", zAxisFormatStr);
		}
		
		Element dataPlotSettings = XmlUtils.findOrCreateChild(chart, "data_plot_settings");
		Element bubbleSeriesEl = XmlUtils.findOrCreateChild(dataPlotSettings, "bubble_series");
		addToolTip(bubbleSeriesEl, formatText);
		addDisplayValues(bubbleSeriesEl, "{%SeriesName}", null);
		Element data = XmlUtils.findOrCreateChild(chart, "data");
		setLegend(chart, "{%Icon} {%Name}{enabled:False}", "Series");
		addXYZPoints(data, chartColors, "Bubble");
	}

	private void addMarkerChart(Element chart, ChartColors chartColors, PlotType plotType) throws BaseException {
		String styleName = null;
		if (this.dataPlotSettings != null && this.dataPlotSettings.markerSettings != null) {
			styleName = "style" + getLabel();
			this.dataPlotSettings.markerSettings.getXML(chart, styleName, this);
		}
		
		String xAxisFormatStr = "#";
		String yAxisFormatStr = "#";
		for (AdhocColumn col : getReport().getColumns()) {
			Integer reportIndex = Integer.valueOf(col.getReportIndex()); 
			if (series.contains(reportIndex)) {
				xAxisFormatStr = col.getFormat();
			}
			else if (series2 != null && series2.contains(reportIndex)) {
				yAxisFormatStr = col.getFormat();
			}
		}
		
		Element dataPlotSettings = XmlUtils.findOrCreateChild(chart, "data_plot_settings");
		Element markerSeriesEl = XmlUtils.findOrCreateChild(dataPlotSettings, "marker_series");
		String formatText = "{%SeriesName}{enabled:False}" + 
			(plotType == AdhocChart.PlotType.Map ? "" : 
			("\n{%XAxisName}{enabled:False}: " + 
			FlashRenderer.getAnychartNumberFormat("XValue", xAxisFormatStr) +
			"\n{%YAxisName}{enabled:False}: " + 
			FlashRenderer.getAnychartNumberFormat("YValue", yAxisFormatStr)));
		addToolTip(markerSeriesEl, formatText);
		addDisplayValues(markerSeriesEl, "{%SeriesName}", null);
		
		Element data = XmlUtils.findOrCreateChild(chart, "data");
		Element legendEl = setLegend(chart, "{%Icon} {%Name}{enabled:False}", "Series");
		if (legendEl != null) {
			Element iconEl = XmlUtils.findOrCreateChild(legendEl, "icon");
			iconEl.setAttribute("type", "Box");
		}
		addXYPoints(data, chartColors, "Marker", styleName);
	}

	private void addGeoMapChart(Element chart, ChartColors chartColors, double scaleFactor, boolean isPrinted, AdhocChart adhocChart) throws Exception {
		BirstCategoryDataset cs = getCategoryDataset();
		MapSeries mapSeries = getMapSeries();
		Font tahomaTen = Font.decode("Tahoma-PLAIN-8");
		
		//Bug 13574. GeoMap chart barfs on sizing color swatch when there is no data
		if(cs != null && cs.getRowCount() == 0 && mapSeries != null && mapSeries.enableColorSwatch){
			mapSeries.enableColorSwatch = false;
		}
		
		// add map specific chart settings
		String format = getYSeriesFormat();
		String threshold = "MyThreshold";
		Element chartSettingsEl = XmlUtils.findOrCreateChild(chart, "chart_settings");
		if (mapSeries != null && (mapSeries.enableMapScrolling || mapSeries.enableMapZooming || mapSeries.enableColorSwatch)) {
			// show controls
			Element controlsEl = XmlUtils.findOrCreateChild(chartSettingsEl, "controls");
			Element zoomPanelEl = null;
			if (mapSeries.enableMapZooming && !isPrinted && ! adhocChart.isTrellis()) {
				zoomPanelEl = XmlUtils.findOrCreateChild(controlsEl, "zoom_panel");
				zoomPanelEl.setAttribute("enabled", "True");
				/*
				if (mapSeries.enableMapScrolling == false) {
					// need to scale just zooming controls if size is too small
					if (adhocChart.getHeight() < MAP_ZOOMING_HEIGHT) {
						zoomPanelEl.setAttribute("height", String.valueOf(adhocChart.getHeight() * .95));
					}
				}
				*/
			}
			Element navigationPanelEl = null;
			if (mapSeries.enableMapScrolling && !isPrinted && !adhocChart.isTrellis()) {
				navigationPanelEl = XmlUtils.findOrCreateChild(controlsEl, "navigation_panel");
				navigationPanelEl.setAttribute("enabled", "True");
				/*
				if (mapSeries.enableMapZooming == false) {
					// need to scale just zooming controls if size is too small
					if (adhocChart.getHeight() < MAP_SCROLLING_HEIGHT) {
						navigationPanelEl.setAttribute("height", String.valueOf(adhocChart.getHeight() * .95));
					}
				}
				*/
			}
			
			/*
			if (mapSeries.enableMapZooming && mapSeries.enableMapScrolling && !isPrinted && zoomPanelEl != null && navigationPanelEl != null) {
				if (adhocChart.getHeight() < MAP_CONTROLS_HEIGHT) {
					zoomPanelEl.setAttribute("height", String.valueOf(adhocChart.getHeight() * .68));
					navigationPanelEl.setAttribute("height", String.valueOf(adhocChart.getHeight() * .25));
				}
			}
			*/
			
			if (mapSeries.enableColorSwatch && ! MapSeries.CUSTOM.equals(mapSeries.getThresholdType()) && !adhocChart.isTrellis()) { 
				Element colorSwatchEl = XmlUtils.findOrCreateChild(controlsEl, "color_swatch");
				colorSwatchEl.setAttribute("threshold", threshold);
				colorSwatchEl.setAttribute("position", "Bottom");
				colorSwatchEl.setAttribute("width", "100%");
				
				Element labelsEl = XmlUtils.findOrCreateChild(colorSwatchEl, "labels");
				labelsEl.setAttribute("enabled", "True");
				
				addFormat(labelsEl, FlashRenderer.getAnychartNumberFormat("Value", format));
				addFontElement(labelsEl, tahomaTen, scaleFactor);
			}
		}
		
		Element dataPlotSettings = XmlUtils.findOrCreateChild(chart, "data_plot_settings");
		if (dataPlotSettings.getChild("interactivity") != null)
		{
			dataPlotSettings.removeChild("interactivity");
		}
		Element mapSeriesEl = new Element("map_series");
		try {
			if (mapSeries != null) {
				String mapNameColumn = null;
				if (mapSeries.mapNameIndex >= 0) {
					AdhocEntity ae = getReport().getEntityById(mapSeries.mapNameIndex);
					if (ae != null && ae instanceof AdhocColumn) {
						mapNameColumn = ((AdhocColumn)ae).getFieldName();
					}
				}
				String mapName = ChartOptions.getMapLocation(qrs, mapSeries.geoMapType, mapSeries.subMapName, mapNameColumn);
				/*
				if (isPrinted) {
					mapName = "C:/SMI/Acorn/maps/" + mapName;
				}
				*/
				mapSeriesEl.setAttribute("source", mapName + ".amap");
			}
		} catch (JRException e) {
			logger.error(e,e);
		}
		
		if (mapSeries != null) {
			if (mapSeries.zoomRangeMax != mapSeries.zoomRangeMin) {
				Element zoomEl = XmlUtils.findOrCreateChild(mapSeriesEl, "zoom");
				zoomEl.setAttribute("minimum", String.valueOf(Math.max(mapSeries.zoomRangeMin, 1)));
				zoomEl.setAttribute("maximum", String.valueOf(mapSeries.zoomRangeMax));
				zoomEl.setAttribute("factor", String.valueOf(mapSeries.zoomFactor > 0 ? mapSeries.zoomFactor : 1));
				
				if (mapSeries.latitudeIndex >= 0 && mapSeries.longitudeIndex >= 0 && getRows().size() > 0) {
					String lat = null;
					String lon = null;
					DataRows rows = getRows();
					DataRow row = rows.getFirst();
					for (AdhocColumn col : getReport().getColumns()) {
						int reportIndex = col.getReportIndex();
						if (reportIndex == mapSeries.latitudeIndex) {
							Object o = row.getFieldValue(col.getFieldName(), col);
							lat = (o == null ? null : o.toString());
						}
						if (reportIndex == mapSeries.longitudeIndex) {
							Object o = row.getFieldValue(col.getFieldName(), col);
							lon = (o == null ? null : o.toString());
						}
					}
					
					if (lat != null && lon != null) {
						zoomEl.setAttribute("geo_y", lat);
						zoomEl.setAttribute("geo_x", lon);
					}
				}
			}
			mapSeriesEl.setAttribute("id_column", mapSeries.geoMapType == GeoMapType.Other ? "REGION_ID" : mapSeries.getMapIdColumn());
			Element gridEl = XmlUtils.findOrCreateChild(mapSeriesEl, "grid");
			if (mapSeries.hideCoordinateGrid) {
				gridEl.setAttribute("enabled", "False");
			}
			else {
				Element parallels = XmlUtils.findOrCreateChild(gridEl, "parallels");
				Element gridLabels = XmlUtils.findOrCreateChild(parallels, "labels");
				addFontElement(gridLabels, tahomaTen, scaleFactor);
				parallels = XmlUtils.findOrCreateChild(gridEl, "meridians");
				gridLabels = XmlUtils.findOrCreateChild(parallels, "labels");
				addFontElement(gridLabels, tahomaTen, scaleFactor);
			}
			
		}
		
		Element undefinedEl = XmlUtils.findOrCreateChild(mapSeriesEl, "undefined_map_region");
		Element interactivityEl = XmlUtils.findOrCreateChild(undefinedEl, "interactivity");
		interactivityEl.setAttribute("hoverable", "False");
		interactivityEl.setAttribute("use_hand_cursor", "False");
		interactivityEl.setAttribute("allow_select", "False");
		
		Element definedEl = XmlUtils.findOrCreateChild(mapSeriesEl, "defined_map_region");
		interactivityEl = XmlUtils.findOrCreateChild(definedEl, "interactivity");
		interactivityEl.setAttribute("hoverable", "True");
		interactivityEl.setAttribute("use_hand_cursor", "True");
		interactivityEl.setAttribute("allow_select", "True");
		addToolTip(definedEl, "{%Name}{enabled:False} - " + FlashRenderer.getAnychartNumberFormat("Value", format));
		if (showValues) {
			Element labelSettingsEl = XmlUtils.findOrCreateChild(definedEl, "label_settings");
			labelSettingsEl.setAttribute("enabled", "True");
			if (displayValueRotation != null)
			{
				labelSettingsEl.setAttribute("rotation", displayValueRotation);
			}			
			String formatStr;
			if (displayValueFormat != null)
				formatStr = displayValueFormat;
			else
				formatStr = FlashRenderer.getAnychartNumberFormat("Value", format);
			addFormat(labelSettingsEl, formatStr);
			if (displayValueFont != null)
			{
				BirstXMLSerializable.addFontElement(labelSettingsEl, displayValueFont, scaleFactor);
			}
			if (displayValueColor != null)
			{
				Element f = XmlUtils.findOrCreateChild(labelSettingsEl, "font");
				AnyChartNode.addColorAttribute(f, displayValueColor);
			}			
			addPosition(labelSettingsEl, "Center", "Center", "Center", null);
			Element f = BirstXMLSerializable.getFontElement(labelFont, scaleFactor);
			AnyChartNode.addColorAttribute(f, labelColor);
			labelSettingsEl.addContent(f);
		}
		
		if (mapSeries != null) {
			Element projectionEl = XmlUtils.findOrCreateChild(mapSeriesEl, "projection");
			projectionEl.setAttribute("type", mapSeries.projectionType);
			
			addPalettesAndThresholds(chart, threshold, mapSeries.getThresholdType(), mapSeries.numThresholds, chartColors, mapSeries.customThresholds);
		}
		
		dataPlotSettings.addContent(mapSeriesEl);
		
		Map<String, Element> seriesMap = addCategoryPoints(cs, chart, "Bar", chartColors);
		for (String key: seriesMap.keySet()) {
			Element seriesEl = seriesMap.get(key);
			seriesEl.setAttribute("threshold", threshold);
		}
		
		setLegend(chart, 
				(isCustomThresholds() ?
						"{%Icon} {%Name}{enabled:False}" :
						"{%Icon} {%RangeMin} - {%RangeMax}"),
				"Thresholds");
	}
	
	private void addUMapChart(Element chart, ChartColors chartColors, double scaleFactor, boolean isPrinted) throws Exception {
		BirstCategoryDataset cs = getCategoryDataset();
		MapSeries mapSeries = getMapSeries();
		
		Element dataPlotSettings = XmlUtils.findOrCreateChild(chart, "data_plot_settings");
		
		//Which kml to use for map regions
		Element mapSeriesEl = new Element("map_series");
		mapSeriesEl.setAttribute("source", mapSeries.uMapKml);
		
		//Which kml to use for rendering placemarks if set
		if(mapSeries.renderGooglePlacemarks && !mapSeries.placemarksKml.isEmpty()){
			mapSeriesEl.setAttribute("placemarks", "true");
			mapSeriesEl.setAttribute("placemarksKml", mapSeries.placemarksKml);
		}
		dataPlotSettings.addContent(mapSeriesEl);
		
		Element chartSettingsEl = XmlUtils.findOrCreateChild(chart, "chart_settings");
		Element controlsEl = XmlUtils.findOrCreateChild(chartSettingsEl, "controls");
		if (mapSeries != null) {
			// show controls
			if (mapSeries.enableColorSwatch) {
				Element colorSwatchEl = XmlUtils.findOrCreateChild(controlsEl, "color_swatch");
			}
			
			addPalettesAndThresholds(chart, "UMapThreshold", mapSeries.getThresholdType(), mapSeries.numThresholds, chartColors, mapSeries.customThresholds);
		}
		Map<String, Element> seriesMap = addCategoryPoints(cs, chart, "Bar", chartColors);
		List keys = cs.getColumnKeys();
		ArrayList<String> colKeys = new ArrayList<String>();
		for(int i = 0; i < keys.size(); i++){
			CellKey ko = (CellKey)keys.get(i);
			String key = (ko == null) ? "" : ko.toString();	
			colKeys.add(key);
		}
		
		//Add the polygons information
		if(mapSeries.uMapKml != null && !mapSeries.uMapKml.equalsIgnoreCase("none")){
			Element placemarks = UMapWebServiceHelper.getPolygons(mapSeries.uMapKml, colKeys);
			chart.addContent(placemarks);
		}
	}

	private void addLineChart(Element chart, ChartColors chartColors, double scaleFactor) throws Exception {
		BirstCategoryDataset cs = getCategoryDataset();
		Element dataPlotSettingsEl = XmlUtils.findOrCreateChild(chart, "data_plot_settings");
		Element lineSeries = XmlUtils.findOrCreateChild(dataPlotSettingsEl, "line_series");
		addCategoryTooltips(lineSeries);
		
		String styleName = "style" + label;
		Element stylesEl = XmlUtils.findOrCreateChild(chart, "styles");
		addLineSeriesConfig(stylesEl, styleName, lineSeries);
		
		setLegend(chart, "{%Icon} {%Name}{enabled:False}", "Series");
		LineSeries ls = dataPlotSettings.getLineSeries();
		Map<String, Element> seriesMap = addCategoryPoints(cs, chart, ls.getType(), chartColors, styleName);
		addDisplayValues(lineSeries, series, seriesMap);
	}
	
	private void addLineSeriesConfig(Element styles, String styleName, Element lineSeries) {
		if (dataPlotSettings != null) {
			LineSeries ls = dataPlotSettings.getLineSeries();
			if (ls != null)
				ls.addConfig(styles, styleName, lineSeries);
		}
	}

	private void addXYZPoints(Element data, ChartColors chartColors, String seriesType) throws BaseException {
		addXYZPoints(data, chartColors, seriesType, true, null);
	}

	private void addXYPoints(Element data, ChartColors chartColors, String seriesType, String styleName) throws BaseException {
		addXYZPoints(data, chartColors, seriesType, false, styleName);
	}
	
	private void addXYZPoints(Element data, ChartColors chartColors, String seriesType, boolean includeZ, String styleName) throws BaseException {
		List<AdhocColumn> catCols = new ArrayList<AdhocColumn>();
		AdhocColumn xCol = null;
		AdhocColumn yCol = null;
		AdhocColumn zCol = null;
		AdhocColumn colorCol = null;
		
		DrillData dd = new DrillData();
		
		for (AdhocColumn col : getReport().getColumns()) {
			Integer reportIndex = Integer.valueOf(col.getReportIndex());
			if (categories.contains(reportIndex)) {
				catCols.add(col);
			}
			if (series.contains(reportIndex)) {
				xCol = col; 
			}
			if (series2.contains(reportIndex)) {
				yCol = col;
			}
			if (includeZ && series3 != null && series3.contains(reportIndex)) {
				zCol = col;
			}
			if (colorColumn != null && colorColumn.intValue() == reportIndex.intValue())
				colorCol = col;
		}

		if ((catCols.size() < 1 || xCol == null || yCol == null) || (includeZ && zCol == null)) {
			logger.warn("Invalid configuration");
			return;
		}
		
		Color sameColor = null;
		if (useSameColor && chartColors != null)
			sameColor = getNextColor(chartColors);

		// initializes rows member if not already
		TreeMap<String, Element> seriesMap = new TreeMap<String, Element>();
		for (DataRow dataRow : getRows()) {
			Object xO = dataRow.getFieldValue(xCol.getFieldName(), xCol);
			Object yO = dataRow.getFieldValue(yCol.getFieldName(), yCol);
			Object zO = null;
			if (includeZ) 
				zO = dataRow.getFieldValue(zCol.getFieldName(), zCol);
			
			if ((xO == null || yO == null) || (includeZ && zO == null))
				continue;
			
			CellKey ddKey = new CellKey();
			
			StringBuilder s = new StringBuilder();
			for (AdhocColumn catCol : catCols) {
				Object k = formatVal(dataRow, catCol);
				if (s.length() > 0) {
					s.append("\n");
				}
				if (catCols.size() > 1) {
					s.append(catCol.getLabelString());
					s.append(": ");
				}
				String key = (k == null ? "" : k.toString());
				s.append(key);
				ddKey.add(key);
			}
			
			String key = s.toString();
			Element series = seriesMap.get(key);
			if (series == null) {
				series = new Element("series");
				series.setAttribute("name", key);
				series.setAttribute("type", seriesType);
				if (useSameColor) {
					addColorAttribute(series, sameColor);
				}
				else {
					addColorAttribute(series, chartColors);
				}
				if (styleName != null)
					series.setAttribute("style", styleName);
				data.addContent(series);
				seriesMap.put(key, series);
			}
			// handle labels that are too big
			Element p = new Element("point");
			p.setAttribute("x", formatVal(dataRow, xCol));
			p.setAttribute("y", formatVal(dataRow, yCol));
			// For BPD-12209 Always set tooltip for trellis chart
				// need to set tooltip  
			    StringBuffer formatText = new StringBuffer(key + "\n" + xCol.getLabelString() + ": ");
				if (xO instanceof BirstInterval) { 
					formatText.append(ToTime.evaluate(((BirstInterval) xO).doubleValue(), ((BirstInterval) xO).getFields(), ((BirstInterval) xO).getFormat())); 
				}
				else {
					formatText.append(FlashRenderer.getAnychartNumberFormat("XValue", xCol.getFormat()));
				}
				formatText.append("\n" + yCol.getLabelString() +  ": ");
				if (yO instanceof BirstInterval) {
					formatText.append(ToTime.evaluate(((BirstInterval) yO).doubleValue(), ((BirstInterval) yO).getFields(), ((BirstInterval) yO).getFormat())); 
				}
				else {
					formatText.append(FlashRenderer.getAnychartNumberFormat("YValue", yCol.getFormat()));
				}
				if (includeZ) {
					
					formatText.append("\n" + zCol.getLabelString() + ": ");
					if (zO instanceof BirstInterval) {
						formatText.append(ToTime.evaluate(((BirstInterval) zO).doubleValue(), ((BirstInterval) zO).getFields(), ((BirstInterval) zO).getFormat())); 
					}
					else {
						formatText.append(FlashRenderer.getAnychartNumberFormat("BubbleSize", zCol.getFormat()));
					}
				}
				addToolTip(p, formatText.toString(), "tooltip");

			Element attributes = XmlUtils.findOrCreateChild(p, "attributes");
			CellKey ck = new CellKey();
			ck.add(key);
			dd.addExtraData(p, attributes, ddKey, null);
			if (includeZ) 
				p.setAttribute("size", formatVal(dataRow, zCol));
			if (useSameColor == false) {
				Color clr = null;
				if (colorCol != null) {
					try {
						clr = ColorUtils.stringToColor(dataRow.getFieldValue(colorCol.getFieldName(), colorCol).toString());
					}
					catch (Exception e) {
						
					}
					if (clr == null) {
						throw new InvalidColorException(dataRow.getFieldValue(colorCol.getFieldName(), colorCol) + " is an invalid color value.");
					}
				}
				if (clr != null) {
					addColorAttribute(p, clr);
				}
			}
			series.addContent(p);
		}
	}

	private void addCategoryPoints(BirstCategoryDataset cs, Element chart, String threshold) throws Exception {
		Element dataEl = XmlUtils.findOrCreateChild(chart, "data");
		DrillData dd = new DrillData();
		UserBean ub = com.successmetricsinc.UserBean.getParentThreadUserBean();
		DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance(ub != null ? ub.getLocale() : Locale.getDefault());
		int numRows = cs.getRowCount();
		for (int j = 0; j < numRows; j++)
		{
			CellKey o = (CellKey)cs.getRowKey(j);
			String columnName = o.get(o.size() - 1).toString();
			DecimalFormat df = getNumberFormatForData(columnName, symbols);
			
			for (int i = 0; i < cs.getColumnCount(); i++)
			{
				Object d = cs.getValue(j, i);
				if ((d == null) || (d.toString().length() == 0))
					continue;
				
				if (((Number)d).doubleValue() < 0.0)
					throw new InvalidDataException("This type of chart can not have negative values.  Please filter your data appropriately.");
				if (((Number)d).doubleValue() > Math.pow(10, df.getMaximumIntegerDigits()))
					throw new InvalidDataException("A value for column " + columnName + " is too large.  Please check your calculations.");
					
				CellKey ko = (CellKey)cs.getColumnKey(i);
				String key = (ko == null) ? "" : ko.toString();				
				Element p = new Element("point");
				p.setAttribute("name", key);
				p.setAttribute("y", df.format(d));
				if (d instanceof BirstInterval) {
					String tooltip = ToTime.evaluate(((BirstInterval) d).doubleValue(), ((BirstInterval) d).getFields(), ((BirstInterval) d).getFormat());
					addToolTip(p, tooltip, "tooltip");
					
					if (this.showValues) {
						Element labelEl = XmlUtils.findOrCreateChild(p, "label");
						labelEl.setAttribute("enabled", "True");
						addFormat(labelEl, tooltip);
					}
				}
				CellKey colorKey = new CellKey();
				if (ko!=null)
				{
					colorKey.addAll(ko);
				}
				colorKey.addAll(o);
				if (cs.categoryColorMap != null && cs.categoryColorMap.size() > 0) {
					addColorAttribute(p, cs.categoryColorMap, colorKey.toString(), null);
				}
				else {
					p.setAttribute("threshold", threshold);
				}
				Element attributes = XmlUtils.findOrCreateChild(p, "attributes");
				addCustomAttribute(attributes, FULL_NAME, key);
				dd.addExtraData(p, attributes, ko, null);
				dataEl.addContent(p);
			}
		}
	}
	
	private Map<String, Element> addCategoryPoints(BirstCategoryDataset cs, Element chart,
			String type, ChartColors chartColors) throws Exception {
		return addCategoryPoints(cs, chart, type, chartColors, null);
	}
	private Map<String, Element> addCategoryPoints(BirstCategoryDataset cs, Element chart,
			String type, ChartColors chartColors, String styleName) throws Exception {
		return addCategoryPoints(cs, chart, type, chartColors, scaleFactor, false, styleName, false);
	}
	
	private Map<String, Element> addCategoryPoints(BirstCategoryDataset cs, Element chart,
			String type, ChartColors chartColors, double scaleFactor, boolean throwExceptionOnNegativeValue) throws Exception {
		return addCategoryPoints(cs, chart, type, chartColors, scaleFactor, throwExceptionOnNegativeValue, null, false);
	}
	private Map<String, Element> addCategoryPoints(BirstCategoryDataset cs, Element chart,
			String type, ChartColors chartColors, double scaleFactor, boolean throwExceptionOnNegativeValue, String styleName, boolean includePercent) throws Exception {

		Map<String, Element> seriesEls = new HashMap<String, Element>();
		
		DrillData dd = new DrillData();
		Element dataEl = XmlUtils.findOrCreateChild(chart, "data");
		UserBean ub = com.successmetricsinc.UserBean.getParentThreadUserBean();
		DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance(ub != null ? ub.getLocale() : Locale.getDefault());
		if (cs!=null) {
			int numRows = cs.getRowCount();
			for (int j = 0; j < numRows; j++) {
				CellKey o = (CellKey) cs.getRowKey(j);
				String columnName = o.get(o.size() - 1).toString();
				DecimalFormat df = getNumberFormatForData(columnName, symbols);

				// 7318
				String sname = o.get(0).toString();
				int index = sname.indexOf('|');
				if (index > 0) {
					sname = sname.substring(0, index);
				}
				sname = convertColumnNameToDisplayName(sname);
				Element seriesEl = seriesEls.get(sname); //o.toString());
				if (seriesEl == null) {
					String formatName = o.get(o.size() - 1).toString();
				seriesEl = createSeriesEl(dataEl, sname, sname, type, cs.categoryColorMap, 
						chartColors, convertColumnNameToFormat(formatName), numRows > 1, sname, includePercent);
					if (styleName != null)
						seriesEl.setAttribute("style", styleName);

					seriesEls.put(sname, seriesEl); //o.toString(), seriesEl);
				}
			for (int i = 0; i < cs.getColumnCount(); i++)
			{
					Object d = cs.getValue(j, i);

				if (throwExceptionOnNegativeValue && d != null && d.toString().length() > 0 && ((Number)d).doubleValue() < 0.0) {
					throw new InvalidDataException("This type of chart can not have negative values.  Please filter your data appropriately.");
					}
				CellKey ko = (CellKey)cs.getColumnKey(i);
					String key = (ko == null) ? "" : ko.toString();
					Element p = new Element("point");
				Element attributes = XmlUtils.findOrCreateChild(p, "attributes");
					p.setAttribute("name", key);
					if ((d != null) && (d.toString().length() > 0)) {
					if (((Number)d).doubleValue() > Math.pow(10, df.getMaximumIntegerDigits()))
						throw new InvalidDataException("A value for column " + columnName + " is too large.  Please check your calculations.");
						String value = df.format(d);
						p.setAttribute("y", value);
						if (d instanceof BirstInterval) {
							String tooltip = ToTime.evaluate(((BirstInterval) d).doubleValue(), ((BirstInterval) d).getFields(), ((BirstInterval) d).getFormat());
							addToolTip(p, tooltip, "tooltip");
							
							if (this.showValues) {
								Element labelEl = XmlUtils.findOrCreateChild(p, "label");
								labelEl.setAttribute("enabled", "True");
								addFormat(labelEl, tooltip);
							}
						}
					if (chartType == ChartType.UMap)
					{
							// for UMAP, need an extra attribute that has the fully formatted value (with %) for the tooltip
						String format = this.convertColumnNameToFormat(columnName);
						if (format.contains("%"))
						{
							DecimalFormat umapdf = getNumberFormatForData(columnName, symbols, true);
							value = umapdf.format(((Number)d).doubleValue());
							}
						addCustomAttribute(attributes, UMAP_TOOLTIP_VALUE, value);
						}
					}

					CellKey colorKey = new CellKey();
					if (ko != null)
						colorKey.addAll(ko);
					colorKey.addAll(o);
				addColorAttribute(p, cs.categoryColorMap, colorKey.toString(), null);
					if (dd.seriesMeasureCount > 1 || showMeasureNamesInTooltip)
						key = sname + "\n" + key;
					addCustomAttribute(attributes, FULL_NAME, key);
					dd.addExtraData(p, attributes, ko, o);
					seriesEl.addContent(p);

				}
			}
		}
		return seriesEls;
	}
	
	private void addCategoryRanges(BirstCategoryDataset cs, Element chart,
			String type, ChartColors chartColors, double scaleFactor, boolean throwExceptionOnNegativeValue, String styleName, boolean waterfall) throws Exception {

		DrillData dd = new DrillData();
		Element dataEl = XmlUtils.findOrCreateChild(chart, "data");
		UserBean ub = com.successmetricsinc.UserBean.getParentThreadUserBean();
		DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance(ub != null ? ub.getLocale() : Locale.getDefault());
		int numRows = cs.getRowCount();
		if (!waterfall && numRows != 2)
		{
			throw new InvalidDataException("Ranges require 2 series, " + numRows + " were specified");
		}
		if (waterfall && numRows != 1)
		{
			throw new InvalidDataException("Waterfalls require 1 series, " + numRows + " were specified");
		}	
		
		// indexes into the rows
		int start = 0;
		int end = 1;

		// build the series name and then create the series
		CellKey o = (CellKey)cs.getRowKey(start);
		String columnNameStart = o.get(o.size() - 1).toString();
		String seriesName = columnNameStart;
		DecimalFormat df = getNumberFormatForData(columnNameStart, symbols);
		if (!waterfall)
		{
			o = (CellKey)cs.getRowKey(end);
			String columnNameEnd = o.get(o.size() - 1).toString();
			seriesName = columnNameStart + " - " + columnNameEnd;
		}

		Element seriesEl = new Element("series");
		seriesEl.setAttribute("name", seriesName);
		seriesEl.setAttribute("type", "RangeBar");
		String yaxis = getName();
		if (yaxis != null) {
			seriesEl.setAttribute("y_axis", yaxis);
		}
		addColorAttribute(seriesEl, cs.categoryColorMap, seriesName, chartColors);		
		dataEl.addContent(seriesEl);

		Number nextWaterFallStartO = (Number) 0;
		String lastTypeName = null;
		
		// iterate over all category values
		for (int i = 0; i < cs.getColumnCount(); i++)
		{
			boolean typeNameChanged = false;
			Number startO = cs.getValue(start, i);
			Number endO = null;
			Number deltaO = null;
			
			CellKey ko = (CellKey)cs.getColumnKey(i);
			String key = ko.get(0).toString();

			// if waterfall, munge the data
			boolean isNeg = false;
			if (waterfall)
			{
				String typeName = ko.get(1).toString();
				if (lastTypeName == null)
				{
					typeNameChanged = false;
				} else if (!lastTypeName.equals(typeName))
				{
					typeNameChanged = true;
				}
				if (typeNameChanged)
				{
					addTotalBar(seriesEl, lastTypeName, nextWaterFallStartO, df, chartColors, dd, o);
				}
				lastTypeName = typeName;
				
				if (startO == null) {
					continue;
				}
				deltaO = startO;
				
				if (startO.getClass() == Integer.class)
				{
					endO = nextWaterFallStartO.longValue() + startO.longValue();
					isNeg = (startO.longValue() < 0);
				}
				else if (startO.getClass() == Long.class)
				{
					endO = nextWaterFallStartO.longValue() + startO.longValue();
					isNeg = (startO.longValue() < 0);
				}
				else
				{
					endO = nextWaterFallStartO.doubleValue() + startO.doubleValue();
					isNeg = (startO.doubleValue() < 0.0);
				}
				startO = nextWaterFallStartO;
				nextWaterFallStartO = endO;
				
				// swap values if needed
				if (isNeg)
				{
					Number temp = startO;
					startO = endO;
					endO = temp;
				}
			}
			else
			{
				endO = cs.getValue(end, i);
			}
				
			Element p = new Element("point");
			p.setAttribute("name", key);
			if (startO != null)
				p.setAttribute("start", df.format(startO));
			if (endO != null)
				p.setAttribute("end", df.format(endO));
			
			// waterfall has specific coloring
			if (waterfall)
			{
				if (isNeg)
					addColorAttribute(p, (chartColors == null || chartColors.getNumColors() < 2) ? Color.RED : getColor(chartColors, 0));
				else
					addColorAttribute(p, (chartColors == null || chartColors.getNumColors() < 2) ? Color.GREEN : getColor(chartColors, 1));
			}
			else
			{
				CellKey colorKey = new CellKey();
				if (ko != null)
					colorKey.addAll(ko);
				colorKey.addAll(o);
				addColorAttribute(p, cs.categoryColorMap, colorKey.toString(), null);
			}
			Element attributes = XmlUtils.findOrCreateChild(p, "attributes");
			// deprecating this attribute; addCustomAttribute(attributes, FULL_NAME, key);
			if (waterfall && deltaO != null)
				addCustomAttribute(attributes, "YRangeDelta", df.format(deltaO));
			dd.addExtraData(p, attributes, ko, o);
			seriesEl.addContent(p);
		}
		if (waterfall)
		{
			addTotalBar(seriesEl, lastTypeName, nextWaterFallStartO, df, chartColors, dd, o);
		}
	}
	
	private void addTotalBar(Element seriesEl, String columnNameEnd, Number totalO, DecimalFormat df, ChartColors chartColors, DrillData dd, CellKey o)
	{
		CellKey ko = new CellKey();
		ko.add(columnNameEnd);

		String key = (ko == null) ? "" : ko.toString();
		Element p = new Element("point");
		p.setAttribute("name", key);
		p.setAttribute("start", df.format((Number) 0));
		p.setAttribute("end", df.format(totalO));

		Color totalClr = Color.BLUE;
		if (chartColors != null)
		{
			if (chartColors.getNumColors() > 2)
				totalClr = getColor(chartColors, 2);
		}
		addColorAttribute(p,totalClr);
		Element attributes = XmlUtils.findOrCreateChild(p, "attributes");
		addCustomAttribute(attributes, FULL_NAME, key);
		addCustomAttribute(attributes, "YRangeDelta", df.format(totalO));
		dd.addExtraData(p, attributes, ko, o);
		seriesEl.addContent(p);
	}
	
	private void addCustomAttribute(Element attributes, String name, String text) {
		Element element = new Element("attribute");
		element.setAttribute("name", name);
		element.setText(text);
		attributes.addContent(element);
	}


	private String convertColumnNameToDisplayName(String sname) {
		for (AdhocColumn col : getReport().getColumns()) {
			String label = col.getLabelString();
			if (sname.equals(label)) {
				if (selectors != null) {
					ColumnSelector selector = this.selectors.get(label);
					if (selector != null) {
						label = selector.getLabel();
					}
				}
				return label;
			}
		}
		return sname;
	}
	
	private String convertColumnNameToFormat(String sname) {
		for (AdhocColumn col : getReport().getColumns()) {
			String label = col.getLabelString();
			if (sname.equals(label)) {
				if (selectors != null) {
					ColumnSelector selector = this.selectors.get(label);
					if (selector != null && selector.getAdhocColumn() != null) {
						return selector.getAdhocColumn().getFormat();
					}
				}
				return col.getFormat();
			}
		}
		return "#";
	}

	private void addStackedBarChart(Element chart, ChartColors chartColors, double scaleFactor, XAxis xAxis, boolean includePercent) throws Exception {
		Element lineSeries = addBarChartWithoutData(chart, chartColors);
		BirstCategoryDataset cs = getCategoryDataset();
		setLegend(chart, "{%Icon} {%Name}{enabled:False}", "Series");
		Map<String, Element> seriesMap = addSeriesPoints(chart, cs, "Bar", chartColors, xAxis, includePercent);
		addDisplayValues(lineSeries, series, seriesMap);
	}

	private void addStackedAreaChart(Element chart, ChartColors chartColors, double scaleFactor, XAxis xAxis, boolean includePercent) throws Exception {
		Element areaSettingEl = addAreaChartWithoutData(chart, chartColors);
		BirstCategoryDataset cs = getCategoryDataset();
		setLegend(chart, "{%Icon} {%Name}{enabled:False}", "Series");
		AreaSeries as = dataPlotSettings.getAreaSeries();
		Map<String, Element> seriesMap = addSeriesPoints(chart, cs, as.getType(), chartColors, xAxis, includePercent);
		addDisplayValues(areaSettingEl, series, seriesMap);
	}
	

	private Map<String, Element> addSeriesPoints(Element chart, BirstCategoryDataset cs, String type, ChartColors chartColors, XAxis xAxis, boolean includePercent) throws InvalidDataException {
		Element data = XmlUtils.findOrCreateChild(chart, "data");
		// now add 1 series per bar (dimension in series)

		DrillData dd = new DrillData();
		
		boolean seriesContainsDimension = seriesContainsDimension();
		List<String> measures = new ArrayList<String>();
		for (AdhocColumn col : getReport().getColumns()) {
			String dim = col.getDimension();
			if (dim == null || !Util.hasNonWhiteSpaceCharacters(dim) || dim.equals("EXPR"))
				measures.add(col.getLabelString());
		}
		List<String> groupKeys = getGroupKeys(cs, qrs, measures);
		if (groupKeys.size() > 1 && seriesContainsDimension) {
			for (int i = 1; i < groupKeys.size(); i++) {
				AddHiddenYAxis(chart, groupKeys.get(i));
			}
			cs.categoryColorMap = new HashMap<String, Color>();
		}
		
		String formatStr = null;
		int numMeasuresInSeries = 0;
		for (AdhocColumn col : getReport().getColumns()) {
			if (series.contains(Integer.valueOf(col.getReportIndex()))) {
				if (col.isNumeric())
					numMeasuresInSeries++;
				formatStr = col.getFormat();
			}
		}
		// use fifo order
		Map<String, Element> elements = new LinkedHashMap<String, Element>();
		/*  removing this fix that was made for 9451 to fix 9779
		 * I believe 9451 is fixed with the sparse() function
		if (this.chartType == ChartType.StackedBar || this.chartType == ChartType.StackedColumn) {
			// use sorted order
			elements = new TreeMap<String, Element>();
		}
		*/
		UserBean ub = com.successmetricsinc.UserBean.getParentThreadUserBean();
		DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance(ub != null ? ub.getLocale() : Locale.getDefault());
		Map<String, String> customLegendValues = new LinkedHashMap<String, String>(); // keep the proper order
		int numRows = cs.getRowKeys().size();
		for (int i = 0; i < numRows; i++) {
			
			CellKey rowKey = (CellKey)cs.getRowKey(i);
			
			String columnName = rowKey.get(rowKey.size() - 1).toString();
			DecimalFormat df = getNumberFormatForData(columnName, symbols);
			
			String seriesName = rowKey.toString();
			CellKey seriesSliceKey = new CellKey();
			int endAt = rowKey.size() - numMeasuresInSeries;
			if (endAt <= 0)
				endAt = 1;
			for (int j = 0; j < endAt; j++) {
				seriesSliceKey.add(rowKey.get(j));
			}
			
			Element seriesEl = createSeriesEl(data, seriesName, seriesSliceKey.toString(), type, cs.categoryColorMap, 
					chartColors, formatStr, numRows > 1, seriesSliceKey.toString(), includePercent);
			seriesEl.detach();
			elements.put(seriesName, seriesEl);
			
			int groupKeyIndex = 0;
			for ( ; groupKeyIndex < groupKeys.size(); groupKeyIndex++) {
				if (seriesName.indexOf(groupKeys.get(groupKeyIndex)) >= 0) {
					break;
				}
			}
			String yaxisName = null;
			if (groupKeyIndex > 0 && groupKeyIndex < groupKeys.size() && seriesContainsDimension) {
				yaxisName = groupKeys.get(groupKeyIndex);
				seriesEl.setAttribute("y_axis", yaxisName);
				
			}
			else {
				customLegendValues.put(seriesSliceKey.toString(), seriesSliceKey.toString());
			}
			
			// add each point with the name of each category
			int columnCount = cs.getColumnCount();
			for (int k = 0; k < columnCount; k++) {
				Object d = cs.getValue(i, k);
				
				CellKey ko = (CellKey)cs.getColumnKey(k);
				String key = (ko == null) ? "" : ko.toString();
				Element point = new Element("point");
				point.setAttribute("name", key);
				// null value not supported in charts.
				if (! valueNotSupportedByChart(d, df))
					point.setAttribute("y", df.format(d));
				if (d instanceof BirstInterval) {
					String tooltip = ToTime.evaluate(((BirstInterval) d).doubleValue(), ((BirstInterval) d).getFields(), ((BirstInterval) d).getFormat());
					addToolTip(point, tooltip, "tooltip");
					
					if (this.showValues) {
						Element labelEl = XmlUtils.findOrCreateChild(point, "label");
						labelEl.setAttribute("enabled", "True");
						addFormat(labelEl, tooltip);
					}
				}

				seriesEl.addContent(point);
				Element attributesEl = XmlUtils.findOrCreateChild(point, "attributes");
				String fullName = key;
//				if (seriesContainsDimesion) {
					if (numMeasuresInSeries > 1) {
						//String[] parts = seriesName.split("\\|");
						// use rowKey
						StringBuilder builder = new StringBuilder();
						if (seriesContainsDimension) { 
							for (int j = 0; j < rowKey.size(); j++) {
								if (builder.length() > 0)
									builder.append("\n");
								builder.append(convertColumnNameToDisplayName(rowKey.get(j).toString()));
							}
						}
						else if (rowKey.size() > 0){
							builder.append(convertColumnNameToDisplayName(rowKey.get(0).toString()));
						}
						fullName = builder.append("\n").append(key).toString();
					}
					else {
						fullName = seriesSliceKey + "\n" + key;
					}
//				}
				addCustomAttribute(attributesEl, "fullName", fullName);
				dd.addExtraData(point, attributesEl, ko, rowKey);
				CellKey colorKey = new CellKey();
				if (ko!=null)
				{
					colorKey.addAll(ko);
				}
				colorKey.addAll(rowKey);
				addColorAttribute(point, cs.categoryColorMap, colorKey.toString(), null);
				// change the series color so that the legend shows up correctly
				// bug 10345
				if (cs.categoryColorMap != null) {
					Color clr = cs.categoryColorMap.get(colorKey.toString());
					if (clr != null) {
						addColorAttribute(seriesEl, clr);
					}
				}
//				addURL(sb, urlgen, i, k, key);
				if (xAxis.isShowGroupLabels() == true && groupKeys.size() > 1 && seriesContainsDimension) {
					if (groupKeyIndex >= 0 && groupKeyIndex < groupKeys.size()) {
						Element extraLabels = XmlUtils.findOrCreateChild(point, "extra_labels");
						Element label = XmlUtils.findOrCreateChild(extraLabels, "label");
						label.setAttribute("enabled", "true");
						// vertical vs horizontal
//						if (co.type != ChartType.Bar && co.type != ChartType.StackedBar) {
						String plotType = chart.getAttributeValue("plot_type");
						if (plotType.endsWith("Vertical"))
							label.setAttribute("rotation", "90");
						Element formatEl = XmlUtils.findOrCreateChild(label, "format");
						String xtraLabel = convertColumnNameToDisplayName(groupKeys.get(groupKeyIndex));
						formatEl.setText(xtraLabel);
						Element position = XmlUtils.findOrCreateChild(label, "position");
						position.setAttribute("anchor", "XAxis");
						position.setAttribute("halign", "Right");
						Element f = BirstXMLSerializable.getFontElement(labelFont, scaleFactor);
						AnyChartNode.addColorAttribute(f, labelColor);
						label.addContent(f);
					}
				}
			}
		}
		
		for (String s: elements.keySet()){
			data.addContent(elements.get(s));
		}
		
		if (groupKeys.size() > 1 && seriesContainsDimension) {
			// do custom legend
			Element chartSettingsEl = chart.getChild("chart_settings");
			if (chartSettingsEl != null) {
				Element legendEl = chartSettingsEl.getChild("legend");
				if (legendEl != null) {
					String enabled = legendEl.getAttributeValue("enabled");
					if (enabled != null && enabled.toLowerCase().trim().equals("true")) {
						// ok, legend turned on
						// do the custom legend
						legendEl.setAttribute("ignore_auto_item", "True");
						Element formatEl = XmlUtils.findOrCreateChild(legendEl, "format");
						formatEl.setText("{%Icon} {%sName}{enabled:False}");
						
						List<Element> items = new ArrayList<Element>();
						for (String key: customLegendValues.keySet()) {
							if (cs.categoryColorMap != null)
							{
								Color c = cs.categoryColorMap.get(key);
								if (c != null) {
									Element item = new Element("item");
									Element attribute = XmlUtils.findOrCreateChild(item, "attribute");
									attribute.setAttribute("name", "sname");
									attribute.setText(key);
									Element icon = XmlUtils.findOrCreateChild(item, "icon");
									icon.setAttribute("series_type", "Bar");
									icon.setAttribute("type", "SeriesIcon");
									addColorAttribute(icon, c);
									Element text = XmlUtils.findOrCreateChild(item, "text");
									text.setText("{%Icon} " + key);
									items.add(item);
								}
							}
						}
						Element itemsEl = XmlUtils.findOrCreateChild(legendEl, "items");
						itemsEl.setContent(items);
					}
				}
			}
			
		}
		
		return elements;
	}

	private List<String> getGroupKeys(BirstCategoryDataset cs,
			QueryResultSet qrs2, List<String> measures) {
		// determine number of groups
		List<String> groupKeys = new ArrayList<String>();
		if (cs.getRowKeys() != null && cs.getRowKeys().size() > 0) {
			for (int j = 0; j < cs.getRowKeys().size(); j++) {
				CellKey rowKey = (CellKey)cs.getRowKeys().get(j);
				for (Object k : rowKey) {
					String key = k.toString();
					if (measures.contains(key) && ! groupKeys.contains(key)) {
						groupKeys.add(key);
					}
				}
			}
		}
		return groupKeys;
	}

	private void AddHiddenYAxis(Element chart, String name) {
		Element chartSettings = XmlUtils.findOrCreateChild(chart, "chart_settings");
		Element axes = XmlUtils.findOrCreateChild(chartSettings, "axes");
		Element extra = XmlUtils.findOrCreateChild(axes, "extra");
		Element yAxis = new Element("y_axis");
		yAxis.setAttribute("name", name);
		yAxis.setAttribute("enabled", "false");
		if (getStackedType() != StackedType.NotStacked) {
			Element scale = XmlUtils.findOrCreateChild(yAxis, "scale");
			scale.setAttribute("mode", getAnyChartStackedType(getStackedType()));
		}
		extra.addContent(yAxis);
	}
	
	public static String getAnyChartStackedType(StackedType type)
	{
		switch (type)
		{
		case Stacked:
			return "Stacked";
		case PercentStacked:
			return "PercentStacked";
		default:
			return null;
		}
	}
	
	public static StackedType getStackedType(String anyChartStackedType)
	{
		if (anyChartStackedType.equals("PercentStacked"))
			return StackedType.PercentStacked;
		else if (anyChartStackedType.equals("Stacked"))
			return StackedType.Stacked;
		return StackedType.NotStacked;
	}
	
	private StackedType getStackedType()
	{
		if (this.chartType == ChartType.StackedBar || this.chartType == ChartType.StackedColumn || this.chartType == ChartType.StackedArea)
			return StackedType.Stacked;
		if (this.chartType == ChartType.PercentStackedBar || this.chartType == ChartType.PercentStackedColumn)
			return StackedType.PercentStacked;
		return StackedType.NotStacked;
	}
	
	public static StackedType getStackedType(Element yAxis) {
		Element scaleEl = yAxis.getChild("scale");
		if (scaleEl == null)
			return StackedType.NotStacked;

		Attribute modeA = scaleEl.getAttribute("mode");
		if (modeA == null)
			return StackedType.NotStacked;
		
		String mode = modeA.getValue();
		if (mode == null)
			return StackedType.NotStacked;
		
		return AnyChartNode.getStackedType(mode);
	}

	private Element createSeriesEl(Element data, String name, String label, String type, Map<String, Color> categoryColorMap, ChartColors chartColors, String formatStr, boolean includeSeriesInTooltip, String colorKey, boolean includePercent) {
		Element seriesEl = new Element("series");
		seriesEl.setAttribute("name", label);
		seriesEl.setAttribute("type", type);
		String yaxis = getName();
		if (yaxis != null) {
			seriesEl.setAttribute("y_axis", yaxis);
		}
		data.addContent(seriesEl);
		addColorAttribute(seriesEl, categoryColorMap, colorKey, chartColors);
		String formatText = "{%fullName}{enabled:False}: " + FlashRenderer.getAnychartNumberFormat("Value", formatStr);
		if (includePercent)
			formatText += " (" + FlashRenderer.getAnychartNumberFormat("YPercentOfCategory", "##.#") + "%)";
		addToolTip(seriesEl, formatText, "tooltip");
		return seriesEl;
	}

	private boolean valueNotSupportedByChart(Object d, DecimalFormat df) throws InvalidDataException {
		if (d == null)
			return true;
		
		if (d instanceof Number && df != null) {
			if (((Number)d).doubleValue() > Math.pow(10, df.getMaximumIntegerDigits()))
				throw new InvalidDataException("A value for the chart is too large.  Please check your calculations.");
				
		}
		
		if (d instanceof Double) {
			return (((Double)d).isInfinite() || ((Double)d).isNaN());
		}
		if (d instanceof Float) {
			return (((Float)d).isInfinite() || ((Float)d).isNaN());
		}
		if (d instanceof Integer) {
			return (((Integer)d).intValue() == Integer.MAX_VALUE);
		}
		
		return false;
	}

	private void addBarChart(Element chart, ChartColors chartColors, double scaleFactor) throws Exception {
		Element lineSeries = addBarChartWithoutData(chart, chartColors);
		BirstCategoryDataset cs = getCategoryDataset();
		setLegend(chart, "{%Icon} {%Name}{enabled:False}", "Series");
		Map<String, Element> seriesMap = addCategoryPoints(cs, chart, "Bar", chartColors);
		addDisplayValues(lineSeries, series, seriesMap);
	}
	
	private Element addBarChartWithoutData(Element chart, ChartColors chartColors)
	{
		Element seriesEl = addBarChartWithoutData(chart, chartColors, "bar_series");
		addCategoryTooltips(seriesEl);
		return seriesEl;
	}
	
	private Element addBarChartWithoutData(Element chart, ChartColors chartColors, String seriesType) {
		Element dataPlotSettings = XmlUtils.findOrCreateChild(chart, "data_plot_settings");
		Element seriesEl = new Element(seriesType);
		BarSeries bs = getBarSeries();
		if (bs != null) {
			seriesEl.setAttribute("shape_type", bs.getShapeType());
			seriesEl.setAttribute("group_padding", String.valueOf(bs.getGroupPadding()));
			seriesEl.setAttribute("point_padding", String.valueOf(bs.getPointPadding()));
			Element barStyleEl = XmlUtils.findOrCreateChild(seriesEl, "bar_style");
			Element borderEl = XmlUtils.findOrCreateChild(barStyleEl, "border");
			borderEl.setAttribute("enabled", bs.isShowBarBorder() ? "True" : "False");
			XmlUtils.findOrCreateChild(barStyleEl, "effects").setAttribute("enabled", "False");
//			Element effectsEl = XmlUtils.findOrCreateChild(barStyleEl, "effects");
//			XmlUtils.findOrCreateChild(effectsEl, "bevel").setAttribute("enabled", "False");
			Element fillEl = XmlUtils.findOrCreateChild(barStyleEl, "fill");
			fillEl.setAttribute("enabled", "True");
			fillEl.setAttribute("type", fillTypeMap.get(fillType));
			fillEl.setAttribute("opacity", "1");
			if (fillType == FillType.Gradient) {
				fillGradient.getXML(fillEl);
			}
			setHoverColor(barStyleEl);
		}
		dataPlotSettings.addContent(seriesEl);
		return seriesEl;		
	}
	
	// this sets the hover color for solid and gradient fills (and the legend works also)
	public static void setHoverColor(Element styleEl)
	{
		Element statesEl = XmlUtils.findOrCreateChild(styleEl, "states");
		Element hoverEl = XmlUtils.findOrCreateChild(statesEl, "hover");
		Element fillHoverEl = XmlUtils.findOrCreateChild(hoverEl, "fill");
		fillHoverEl.setAttribute("type", "Solid");
		fillHoverEl.setAttribute("color", "Rgb(218,218,218)");
	}
	
	private Element addPosition(Element parent, String anchor, String valign, String halign, String padding)
	{
		Element positionEl = XmlUtils.findOrCreateChild(parent, "position");
		if (anchor != null)
			positionEl.setAttribute("anchor", anchor);
		if (valign != null)
			positionEl.setAttribute("valign", valign);
		if (halign != null)
			positionEl.setAttribute("halign", halign);
		if (padding != null)
			positionEl.setAttribute("padding", padding);
		return positionEl;
	}
	
	private void addRangeBarChart(Element chart, ChartColors chartColors, double scaleFactor, boolean waterfall) throws Exception {
		RangeBarSeries rbSeries = dataPlotSettings.getRangeBarSeries();
		BirstCategoryDataset cs = null;
		if (waterfall)
			cs = getCategoryDataset();
		else
		{
			List<Integer> newSeries = new ArrayList<Integer>();
			newSeries.addAll(series);
			newSeries.addAll(series2);
			cs = this.createCategoryDataset(qrs, categories, newSeries, colorColumn);
		}
		addCategoryRanges(cs, chart, "Bar", chartColors, scaleFactor, false, null, waterfall);
		setLegend(chart, "{%Icon} {%Name}{enabled:False}", "Series");
		
		String formatStr = null;
		for (AdhocColumn col : getReport().getColumns()) {
			if (series.contains(Integer.valueOf(col.getReportIndex())))
				formatStr = col.getFormat();
		}

		Element dataPlotSettingsEl = XmlUtils.findOrCreateChild(chart, "data_plot_settings");
		Element seriesEl = XmlUtils.findOrCreateChild(dataPlotSettingsEl, "range_bar_series");

		seriesEl.setAttribute("shape_type", rbSeries.getShapeType());
		seriesEl.setAttribute("point_padding", String.valueOf(rbSeries.getPointPadding())); 
		seriesEl.setAttribute("group_padding", String.valueOf(rbSeries.getGroupPadding()));
		
		// XXX encapsulate this?
		Element barStyleEl = XmlUtils.findOrCreateChild(seriesEl, "bar_style");
		Element borderEl = XmlUtils.findOrCreateChild(barStyleEl, "border");
		borderEl.setAttribute("enabled", rbSeries.isShowBarBorder() ? "True" : "False");
		XmlUtils.findOrCreateChild(barStyleEl, "effects").setAttribute("enabled", "False");
		Element fillEl = XmlUtils.findOrCreateChild(barStyleEl, "fill");
		fillEl.setAttribute("enabled", "True");
		fillEl.setAttribute("type", fillTypeMap.get(fillType));
		fillEl.setAttribute("opacity", "1");
		if (fillType == FillType.Gradient) {
			fillGradient.getXML(fillEl);
		}

		Element startPointEl = XmlUtils.findOrCreateChild(seriesEl, "start_point");
		Element tooltipSettingsEl = XmlUtils.findOrCreateChild(startPointEl, "tooltip_settings");
		tooltipSettingsEl.setAttribute("enabled", "False");		

		// end starting point for waterfall
		if (!waterfall && rbSeries.isStartPointDisplay())
		{
			addDisplayValue(startPointEl, "{%YRangeStart}", formatStr, rbSeries.getStartPointFormat(), rbSeries.getStartPointAnchor(), rbSeries.getStartPointVAlign(), rbSeries.getStartPointHAlign(), rbSeries.getStartPointPadding());
		}
		Element endPointEl = XmlUtils.findOrCreateChild(seriesEl, "end_point");
		tooltipSettingsEl = XmlUtils.findOrCreateChild(endPointEl, "tooltip_settings");
		tooltipSettingsEl.setAttribute("enabled", "False");
		if (rbSeries.isEndPointDisplay())
		{
			// waterfall uses a different identifier for the display value (the increment rather than the end value)
			if (waterfall)
				addDisplayValue(endPointEl, "{%YRangeDelta}", formatStr, rbSeries.getEndPointFormat(), rbSeries.getEndPointAnchor(), rbSeries.getEndPointVAlign(), rbSeries.getEndPointHAlign(), rbSeries.getEndPointPadding());
			else
				addDisplayValue(endPointEl, "{%YRangeEnd}", formatStr, rbSeries.getEndPointFormat(), rbSeries.getEndPointAnchor(), rbSeries.getEndPointVAlign(), rbSeries.getEndPointHAlign(), rbSeries.getEndPointPadding());
		}
	}
	
	// XXX eventually do this for all display values, and probably add font also
	private void addDisplayValue(Element parent, String defaultAnyChartFormat, String columnFormat, String anyChartFormat, String anchor, String vAlign, String hAlign, String padding)
	{
		Element labelSettingsEl = XmlUtils.findOrCreateChild(parent, "label_settings");
		labelSettingsEl.setAttribute("enabled", "True");
		if (displayValueRotation != null)
			labelSettingsEl.setAttribute("rotation", displayValueRotation);
		Element formatEl = XmlUtils.findOrCreateChild(labelSettingsEl, "format");
		String formatText = (columnFormat == null ? defaultAnyChartFormat : FlashRenderer.getAnychartNumberFormat(defaultAnyChartFormat, columnFormat));
		if (anyChartFormat != null)
			formatText = anyChartFormat;
		formatEl.setText(formatText);
		addPosition(labelSettingsEl, anchor, vAlign, hAlign, padding);
	}
	
	private Element addAreaChartWithoutData(Element chart, ChartColors chartColors) {
		Element dataPlotSettings = XmlUtils.findOrCreateChild(chart, "data_plot_settings");
		Element areaSettingEl = new Element("area_series");
		Element tooltipSettingsEl = XmlUtils.findOrCreateChild(areaSettingEl, "tooltip_settings");
		tooltipSettingsEl.setAttribute("enabled", "True");
		
		Element areaStyleEl = XmlUtils.findOrCreateChild(areaSettingEl, "area_style");
		Element fillEl = XmlUtils.findOrCreateChild(areaStyleEl, "fill");
		fillEl.setAttribute("type", fillTypeMap.get(fillType));
		fillEl.setAttribute("opacity", "1");
		if (fillType == FillType.Gradient) {
			fillGradient.getXML(fillEl);
		}
//		XmlUtils.findOrCreateChild(areaStyleEl, "effects").setAttribute("enabled", "False");
		XmlUtils.findOrCreateChild(areaStyleEl, "line").setAttribute("enabled", "False");
		
		setHoverColor(areaStyleEl);
		
		dataPlotSettings.addContent(areaSettingEl);

		addCategoryTooltips(areaSettingEl);
		
		return areaSettingEl;
	}
	
	private BarSeries getBarSeries() {
		if (dataPlotSettings != null)
			return dataPlotSettings.getBarSeries();
		return null;
	}

	private void addDisplayValues(Element parent, List<Integer> seriesList, Map<String, Element> seriesMap) {
		if (showValues) {
			String formatStr = null;
			for (AdhocColumn col : getReport().getColumns()) {
				if (seriesList.contains(Integer.valueOf(col.getReportIndex())))
					formatStr = col.getFormat();
			}
			String formatText = (formatStr == null ? "{%Value}" : FlashRenderer.getAnychartNumberFormat("Value", formatStr));
			addDisplayValues(parent, formatText, seriesMap);
		}
	}
	private void addDisplayValues(Element parent, String formatText, Map<String, Element> seriesMap) {
		if (showValues) {
			Element labelSettingsEl = XmlUtils.findOrCreateChild(parent, "label_settings");
			labelSettingsEl.setAttribute("enabled", "True");
			if (displayValueRotation != null)
			{
				labelSettingsEl.setAttribute("rotation", displayValueRotation);
			}			
			
			String anchor = null;
			if (showValuesVerticalAlignment == VerticalAlignment.Center) {
				anchor = "Center";
			}
			else if (showValuesVerticalAlignment == VerticalAlignment.Bottom) {
				anchor = "CenterBottom";
			}
			addPosition(labelSettingsEl, anchor, verticalAlignmentMap.get(showValuesVerticalAlignment), null, null);

			Element backgroundEl = XmlUtils.findOrCreateChild(labelSettingsEl, "background");
			backgroundEl.setAttribute("enabled", "False");
			
			Element f = BirstXMLSerializable.getFontElement(labelFont, scaleFactor);
			AnyChartNode.addColorAttribute(f, labelColor);
			labelSettingsEl.addContent(f);
			
			if (displayValueFormat != null)
				formatText = displayValueFormat;
			
			if (seriesMap != null) {
				for (Element series : seriesMap.values()) {
					Element labelEl = XmlUtils.findOrCreateChild(series, "label");
					labelEl.setAttribute("enabled", "True");
					addFormat(labelEl, formatText);
					if (displayValueFont != null)
					{
						BirstXMLSerializable.addFontElement(labelEl, displayValueFont, scaleFactor);
					}
					if (displayValueColor != null)
					{
						Element f2 = XmlUtils.findOrCreateChild(labelEl, "font");
						AnyChartNode.addColorAttribute(f2, displayValueColor);
					}
				}
			}
			else {
				addFormat(labelSettingsEl, formatText);
				if (displayValueFont != null)
				{
					BirstXMLSerializable.addFontElement(labelSettingsEl, displayValueFont, scaleFactor);
				}
				if (displayValueColor != null)
				{
					Element f2 = XmlUtils.findOrCreateChild(labelSettingsEl, "font");
					AnyChartNode.addColorAttribute(f2, displayValueColor);
				}
			}
		}
	}
	
	private void addColorAttribute(Element p,
			Map<String, Color> categoryColorMap, String key,
			ChartColors chartColors) {
		if (categoryColorMap != null) {
			Color clr = categoryColorMap.get(key);
			if (clr == null && chartColors != null) {
				clr = getNextColor(chartColors);
				categoryColorMap.put(key, clr);
			}
			addColorAttribute(p, clr);
		}
		else {
			addColorAttribute(p, chartColors);
		}
	}
	
	private void addColorAttribute(Element parent, ChartColors chartColors) {
		if (chartColors == null)
			return;
		
		Color clr = getNextColor(chartColors);
		addColorAttribute(parent, clr);
	}
	
	public static void addColorAttribute(Element parent, Color clr) {
		if (clr != null) {
			parent.setAttribute("color", convertColorToRGBString(clr));
		}
		
	}
	
	public static String convertColorToRGBString(Color clr) {
		return "Rgb(" + clr.getRed() + ","
		+ clr.getGreen() + "," + clr.getBlue() + ")"; 
	}
	
	private Color getNextColor(ChartColors chartColors) {
		Paint pnt = chartColors.getNextPaint();
		Color clr = null;
		if (pnt instanceof Color)
			clr = (Color) pnt;
		else if (pnt instanceof ChartGradientPaint)
		{
			ChartGradientPaint cgp = (ChartGradientPaint) pnt;
			clr = cgp.getBaseColor();
		}
		
		return clr;
	}
	
	private Color getColor(ChartColors chartColors, int index) {
		Paint pnt = chartColors.getNextPaint(index);
		Color clr = null;
		if (pnt instanceof Color)
			clr = (Color) pnt;
		else if (pnt instanceof ChartGradientPaint)
		{
			ChartGradientPaint cgp = (ChartGradientPaint) pnt;
			clr = cgp.getBaseColor();
		}
		return clr;
	}

	private void addCategoryTooltips(Element parent) {
		String formatText;
		if (tooltipFormat != null)
		{
			formatText = tooltipFormat;
		}
		else
		{
			String formatStr = null;
			for (AdhocColumn col : getReport().getColumns()) {
				if (series.contains(Integer.valueOf(col.getReportIndex())))
					formatStr = col.getFormat();
			}
			formatText = "{%" + FULL_NAME + "}{enabled:False}: \n" + 
					(formatStr == null ? "{%Value}" : FlashRenderer.getAnychartNumberFormat("Value", formatStr));
		}
		addToolTip(parent, formatText);
	}
	
	/**
	 * @return the chartType
	 */
	public ChartType getChartType() {
		return chartType;
	}
	
	public void setChartType(ChartType ct) {
		chartType = ct;
		if (categories == null)
			categories = new ArrayList<Integer>();
		if (series == null)
			series = new ArrayList<Integer>();
		switch (ct) {
		case RangeBar:
		case RangeColumn:
		case Scatter:
			if (series2 == null)
				series2 = new ArrayList<Integer>();
			break;
		case Bubble:
			if (series2 == null)
				series2 = new ArrayList<Integer>();
			if (series3 == null)
				series3 = new ArrayList<Integer>();
		}
	}

	public String getDefaultXAxisLabel() {
		if (chartType == ChartType.Scatter || chartType == ChartType.Bubble) 
			return getDefaultAxisLabel(series);
		if (chartType == ChartType.Heatmap)
			return getDefaultAxisLabel(categories.subList(1, 2));
		return getDefaultAxisLabel(categories);
	}
	
	public String getDefaultAxisLabel(List<Integer> columns) {
		return getDefaultAxisLabel(columns, getReport());
	}
	
	public String getDefaultAxisLabel(List<Integer> columns, AdhocReport report) {
		if (columns == null)
			return null;
		
		List<String> categoryNames = new ArrayList<String>();
		for (AdhocColumn col : report.getColumns()) {
			if (columns.contains(Integer.valueOf(col.getReportIndex()))) {
				String label = col.getLabelString();
				if (this.selectors != null) {
					ColumnSelector selector = this.selectors.get(label);
					if (selector != null) {
						label = selector.getLabel();
					}
				}
				categoryNames.add(label);
			}
		}
		
		StringBuilder builder = new StringBuilder();
		for (String s : categoryNames) {
			if (builder.length() > 0) {
				builder.append(" / ");
			}
			builder.append(s);
		}
		return builder.toString();
	}

	public String getName() {
		if (this.yAxis != null) {
			String name = yAxis.getAttributeValue("name");
			return name;
		}
		return label;
	}
	public void setName(String l) {
		label = l;
	}

	public String getDefaultYAxisLabel() {
		if (chartType == ChartType.Scatter || chartType == ChartType.Bubble) 
			return getDefaultAxisLabel(series2);
		if (chartType == ChartType.Heatmap)
			return getDefaultAxisLabel(categories.subList(0, 1));
		return getDefaultAxisLabel(series);
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @return the dataSourcePath
	 */
	public String getDataSourcePath() {
		return dataSourcePath;
	}

	public void setReport(AdhocReport report) {
		this.report = report;
	}

	public AdhocReport getReport() {
		if (report == null && dataSourcePath != null) {
			// load dataSourcePath
			try {
				String reportName = dataSourcePath;
				String catDir = null;
				Session session = Session.getCurrentThreadSession();
				if (session != null)
					catDir = session.getCatalogDir();
				else
					catDir = SMIWebUserBean.getUserBean().getCatalogPath();
				reportName = CatalogManager.getNormalizedPath(reportName, catDir);

				File adhocReportFile = CatalogManager.getAdhocReportFile(reportName, catDir);
				setReport(AdhocReport.read(adhocReportFile));
			}
			catch (Exception e) {
				logger.error(e,e);
			}
		}
		return report;
	}
	
	public String getYSeriesFormat() {
		String format = "#";
		
		if (series2 != null && series2.size() > 0) {
			format = getFormat(series2);
		}
		else if (series != null && series.size() > 0) {
			format = getFormat(series);
		}
		return format;
	}
	
	public String getFormat(List<Integer> list) {
		String format = "#";
		for (AdhocColumn col : getReport().getColumns()) {
			if (! col.isMeasureExpression())
				continue;
			
			if (list.contains(Integer.valueOf(col.getReportIndex()))) {
				format = col.getFormat();
				break;
			}
		}
		return format;
	}

	public Element getNewGaugeChart(Element root, JRDataSource dataSource, String selectorXml, ChartFilters chartFilters, AdhocReport parentReport, boolean applyAllPrompts, @SuppressWarnings("rawtypes") Map parameterMap, boolean isTrellisChart) throws Exception {
		getReport();
		try {
			generateQueryResultSet(dataSource, selectorXml, chartFilters, parentReport, applyAllPrompts, parameterMap, isTrellisChart);
		} catch (NoDataException e)
		{
			logger.info(e.getMessage());
			throw e;
		} catch (Exception e) {
			logger.error(e,e);
			throw e;
		}
		
		NewGaugeSeries newGaugeSeries = getNewGaugeSeries();
		
		Element gauges = XmlUtils.findOrCreateChild(root, "gauges"); 
		Element gaugeEl = XmlUtils.findOrCreateChild(gauges, "gauge");
		Element typeEl = XmlUtils.findOrCreateChild(gaugeEl, newGaugeSeries.getType());
		typeEl.setAttribute("orientation", newGaugeSeries.getOrientation());

		Element axisEl = XmlUtils.findOrCreateChild(typeEl, "axis");
		
		if (newGaugeSeries.getType().equalsIgnoreCase("circular"))
		{
			if (newGaugeSeries.getRadius() != null)
				axisEl.setAttribute("radius", newGaugeSeries.getRadius());
			if (newGaugeSeries.getStartAngle() != null)
				axisEl.setAttribute("start_angle", newGaugeSeries.getStartAngle());
			if (newGaugeSeries.getSweepAngle() != null)
				axisEl.setAttribute("sweep_angle", newGaugeSeries.getSweepAngle());
		}
		
		Element scaleEl = XmlUtils.findOrCreateChild(axisEl, "scale");

		if (newGaugeSeries.getMinimum() != null)
			scaleEl.setAttribute("minimum", newGaugeSeries.getMinimum());

		if (newGaugeSeries.getMaximum() != null)
			scaleEl.setAttribute("maximum", newGaugeSeries.getMaximum());

		if (newGaugeSeries.getMajorInterval() != null)
			scaleEl.setAttribute("major_interval", newGaugeSeries.getMajorInterval());
		else if (newGaugeSeries.getMaximum() != null)
			scaleEl.setAttribute("major_interval", newGaugeSeries.getMaximum()); // XXX hack to stop it from defaulting to 10

		if (newGaugeSeries.getMinorInterval() != null)
			scaleEl.setAttribute("minor_interval", newGaugeSeries.getMinorInterval());
		else if (newGaugeSeries.getMaximum() != null)
			scaleEl.setAttribute("minor_interval", newGaugeSeries.getMaximum()); // XXX hack to stop it from defaulting to 2
		
		Element scaleBarEl = XmlUtils.findOrCreateChild(axisEl, "scale_bar");
		scaleBarEl.setAttribute("enabled", String.valueOf(newGaugeSeries.isShowScaleBar()));
		if (newGaugeSeries.getScaleBarColor() != null)
		{
			Element fillEl = XmlUtils.findOrCreateChild(scaleBarEl, "fill");
			XmlUtils.addColorAttribute(fillEl, "color", newGaugeSeries.getScaleBarColor());
		}

		List<CustomThreshold> thresholds = newGaugeSeries.getCustomThresholds();
		if (thresholds != null)
		{
			Element colorRangesEl = XmlUtils.findOrCreateChild(axisEl, "color_ranges");
			List<Element> ranges = new ArrayList<Element>();
			for (CustomThreshold thresh : thresholds)
			{
				Element rangeEl = new Element("color_range");
				rangeEl.setAttribute("start", String.valueOf(thresh.minValue));
				rangeEl.setAttribute("end", String.valueOf(thresh.maxValue));
				if (newGaugeSeries.getColorRangeAlign() != null)
					rangeEl.setAttribute("align", newGaugeSeries.getColorRangeAlign()); // inside/outside/center
				if (newGaugeSeries.getColorRangeStartSize() != null)
					rangeEl.setAttribute("start_size", newGaugeSeries.getColorRangeStartSize()); // single value, % fill
				if (newGaugeSeries.getColorRangeEndSize() != null)
					rangeEl.setAttribute("end_size", newGaugeSeries.getColorRangeEndSize());
				XmlUtils.addColorAttribute(rangeEl, "color", thresh.color);
				if (thresh.label != null && thresh.label.length() > 0)
				{
					Element labelEl = XmlUtils.findOrCreateChild(rangeEl, "label");
					Element format2El = XmlUtils.findOrCreateChild(labelEl, "format");
					format2El.setText(thresh.label);
				}
				ranges.add(rangeEl);
			}
			colorRangesEl.addContent(ranges);
		}
		
		axes.setupNewGaugeAxis(axisEl); // enable/disable labels, major/minor tickmarks

		DataRows rows = getRows();
		if (rows.size() > 0) {
			DataRow row = rows.getFirst();
			
			// there should be 1 row, the value of the series in that row is
			AdhocColumn xCol = null;
			String format = null;
			for (AdhocColumn col : getReport().getColumns()) {
				Integer reportIndex = Integer.valueOf(col.getReportIndex());
				if (series.contains(reportIndex)) {
					xCol = col;
					format = col.getFormat();
					break;
				}
			}
			if (format == null)
				format = "#";

			if (xCol!=null)
			{
				Object xO = row.getFieldValue(xCol.getFieldName(), xCol);
				if (! FlashRenderer.unsupportedValueToChart(xO)) {
					Element pointersEl = XmlUtils.findOrCreateChild(typeEl, "pointers");
					Element pointerEl = XmlUtils.findOrCreateChild(pointersEl, "pointer");
					pointerEl.setAttribute("type", newGaugeSeries.getPointerType());
					pointerEl.setAttribute("value", xO.toString());
					if (newGaugeSeries.getPointerColor() != null)
						XmlUtils.addColorAttribute(pointerEl, "color",  newGaugeSeries.getPointerColor());
					Element tooltipEL = XmlUtils.findOrCreateChild(pointerEl, "tooltip");
					tooltipEL.setAttribute("enabled", "true");
					Element formatEl = XmlUtils.findOrCreateChild(tooltipEL, "format");
					formatEl.setText((showMeasureNamesInTooltip ? (xCol.getDisplayName() + ": ") : "") + FlashRenderer.getAnychartNumberFormat("Value", format));			
				}
			}
		}
		return root;
	}
	
	private NewGaugeSeries getNewGaugeSeries() {
		if (dataPlotSettings != null)
			return dataPlotSettings.getNewGaugeSeries();
		
		return null;
	}
	public Element getGaugeChart(Element root, JRDataSource dataSource, String selectorXml, ChartFilters chartFilters, AdhocReport parentReport, boolean applyAllPrompts, @SuppressWarnings("rawtypes") Map parameterMap, boolean isTrellisChart) throws Exception {
		getReport();
		try {
			generateQueryResultSet(dataSource, selectorXml, chartFilters, parentReport, applyAllPrompts, parameterMap, isTrellisChart);
		} catch (NoDataException e)
		{
			logger.info(e.getMessage());
			throw e;
		} catch (Exception e) {
			logger.error(e,e);
			throw e;
		}
		
		Element gauges = XmlUtils.findOrCreateChild(root, "gauges"); 
		Element gaugeEl = XmlUtils.findOrCreateChild(gauges, "gauge");
		Element circularEl = XmlUtils.findOrCreateChild(gaugeEl, "circular");
		Element axisEl = XmlUtils.findOrCreateChild(circularEl, "axis");
		axisEl.setAttribute("radius", "42");
		axisEl.setAttribute("start_angle", "90");
		axisEl.setAttribute("sweep_angle", "180");
		axisEl.setAttribute("size", "3");
		
		Element labelsEl = XmlUtils.findOrCreateChild(axisEl, "labels");
		labelsEl.setAttribute("align", "Outside");
		labelsEl.setAttribute("rotate_circular", "true");
		labelsEl.setAttribute("auto_orientation", "true");
		Element formatEl = XmlUtils.findOrCreateChild(labelsEl, "format");
		formatEl.setText(FlashRenderer.getAnychartNumberFormat("Value", getYSeriesFormat()));
		
		Element scaleBarEl = XmlUtils.findOrCreateChild(axisEl, "scale_bar");
		Element fill = XmlUtils.findOrCreateChild(scaleBarEl, "fill");
		fill.setAttribute("color", "#393939");
		
		Element majorTickmarksEl = XmlUtils.findOrCreateChild(axisEl, "major_tickmark");
		majorTickmarksEl.setAttribute("width", "2");
		majorTickmarksEl.setAttribute("length", "10");
		
		Element minorTickmarksEl = XmlUtils.findOrCreateChild(axisEl, "minor_tickmark");
		minorTickmarksEl.setAttribute("width", "1");
		minorTickmarksEl.setAttribute("length", "3");
		fill = XmlUtils.findOrCreateChild(minorTickmarksEl, "fill");
		fill.setAttribute("color", "#777777");
		
		// there should be 1 row 
		// the value of the series in that row is
		AdhocColumn xCol = null;
		String format = null;
		for (AdhocColumn col : getReport().getColumns()) {
			Integer reportIndex = Integer.valueOf(col.getReportIndex());
			if (series.contains(reportIndex)) {
				xCol = col;
				format = col.getFormat();
				break;
			}
		}
		if (format == null)
			format = "#";
		
		GaugeSeries gaugeSeries = getGaugeSeries();
		if (gaugeSeries != null) {
			Element scaleEl = XmlUtils.findOrCreateChild(axisEl, "scale");
			if (gaugeSeries.getNumMajorIntervals() > 0) {
				double major = (gaugeSeries.getMaximum() - gaugeSeries.getMinimum()) / gaugeSeries.getNumMajorIntervals();
				scaleEl.setAttribute("major_interval", String.valueOf(major));
				if (gaugeSeries.getNumMinorIntervals() > 0) {
					double minor = major / gaugeSeries.getNumMinorIntervals();
					scaleEl.setAttribute("minor_interval", String.valueOf(minor));
				}
			}
			
			if (gaugeSeries.getMinimum() != gaugeSeries.getMaximum())
			{
				scaleEl.setAttribute("minimum", String.valueOf(gaugeSeries.getMinimum()));
				scaleEl.setAttribute("maximum", String.valueOf(gaugeSeries.getMaximum()));
			}
			
			if (gaugeSeries.getNormal() != gaugeSeries.getCritical() || gaugeSeries.getNormal() != 0)
			{
				double normal = Math.min(gaugeSeries.getMaximum(), gaugeSeries.getNormal());
				normal = Math.max(gaugeSeries.getMinimum(), normal);
				double critical = Math.min(gaugeSeries.getMaximum(), gaugeSeries.getCritical());
				critical = Math.max(gaugeSeries.getMinimum(), critical);
				double first = 0;
				double second = 0;
				String color1 = null;
				String color2 = null;
				String color3 = null;
				if (gaugeSeries.getNormal() >= gaugeSeries.getCritical())
				{
					first = critical;
					second = normal;
					color1 = "Red";
					color2 = "Yellow";
					color3 = "Green";
				} else
				{
					first = normal;
					second = critical;
					color1 = "Green";
					color2 = "Yellow";
					color3 = "Red";
				}
				if (colorList != null && colorList.size() > 2) {
					color1 = AnyChartNode.convertColorToRGBString(colorList.get(0));
					color2 = AnyChartNode.convertColorToRGBString(colorList.get(1));
					color3 = AnyChartNode.convertColorToRGBString(colorList.get(2));
				}
				
				Element colorRangesEl = XmlUtils.findOrCreateChild(axisEl, "color_ranges");
				List<Element> ranges = new ArrayList<Element>();
				Element range = new Element("color_range");
				range.setAttribute("start", String.valueOf(gaugeSeries.getMinimum()));
				range.setAttribute("end", String.valueOf(first));
				range.setAttribute("color", color1);
				setDefaultRangeAttributes(range);
				ranges.add(range);
				range = new Element("color_range");
				range.setAttribute("start", String.valueOf(first));
				range.setAttribute("end", String.valueOf(second));
				range.setAttribute("color", color2);
				setDefaultRangeAttributes(range);
				ranges.add(range);
				range = new Element("color_range");
				range.setAttribute("start", String.valueOf(second));
				range.setAttribute("end", String.valueOf(gaugeSeries.getMaximum()));
				range.setAttribute("color", color3);
				setDefaultRangeAttributes(range);
				ranges.add(range);
				colorRangesEl.addContent(ranges);
				
				DataRows rows = getRows();
				if (rows.size() > 0) {
					DataRow row = rows.getFirst();
					Object xO = row.getFieldValue(xCol.getFieldName(), xCol);
					if (! FlashRenderer.unsupportedValueToChart(xO)) {
						// add pointers section
						Element pointersEl = XmlUtils.findOrCreateChild(circularEl, "pointers");
						pointersEl.setAttribute("editable", "false");
						pointersEl.setAttribute("allow_select", "false");
		
						// for some reason tooltips in a gauge is tooltip, not tooltip_settings like everywhere else
	//					addToolTip(pointersEl, FlashRenderer.getAnychartNumberFormat("Value", format));
						Element tooltipEl = XmlUtils.findOrCreateChild(pointersEl, "tooltip");
						tooltipEl.setAttribute("enabled", "true");
						formatEl = XmlUtils.findOrCreateChild(tooltipEl, "format");
						String formatText;
						if (tooltipFormat != null)
							formatText = tooltipFormat;
						else
							formatText = FlashRenderer.getAnychartNumberFormat("Value", format);
						formatEl.setText(formatText);
						
						Element pointerEl = XmlUtils.findOrCreateChild(pointersEl, "pointer");
						pointerEl.setAttribute("name", "blah"); 
						pointerEl.setAttribute("type", "needle");
						pointerEl.setAttribute("value", xO.toString());
					}
				}
				
				
			}
		}
		return root;
		
	}
	
	private GaugeSeries getGaugeSeries() {
		if (dataPlotSettings != null)
			return dataPlotSettings.getGaugeSeries();
		
		return null;
	}

	private void setDefaultRangeAttributes(Element range) {
		range.setAttribute("start_size", "40");
		range.setAttribute("end_size", "40");
		range.setAttribute("align", "Inside");
		range.setAttribute("padding", "5");
		Element fill = XmlUtils.findOrCreateChild(range, "fill");
		fill.setAttribute("opacity", "0.6");
	}
	
	private Element setLegend(Element chart, String format, String itemSource) {
		Element chartSettingsEl = XmlUtils.findOrCreateChild(chart, "chart_settings");
		Element legendEl = chartSettingsEl.getChild("legend");
		if (legendEl != null) {
			// if null, legend is not enabled.
			Element formatEl = XmlUtils.findOrCreateChild(legendEl, "format");
			if (legendSettings.getLegendFormat() != null)
				format = legendSettings.getLegendFormat();
			formatEl.setText(format);
			Element backgroundEl = XmlUtils.findOrCreateChild(legendEl, "background");
			backgroundEl.setAttribute("enabled", "False");
			
			if (legendSettings.getLegendSource() != null)
				itemSource = legendSettings.getLegendSource();
			
			if (itemSource != null) {
				Element itemsEl = XmlUtils.findOrCreateChild(legendEl, "items");
				Element itemEl = XmlUtils.findOrCreateChild(itemsEl, "item");
				itemEl.setAttribute("source", itemSource);
			}
		}
		
		return legendEl;
	}
	
	/**
	 * Create a category data set from a query result set
	 * 
	 * @param qrs
	 *            Query result set
	 * @param categories
	 *            List of column names of categories (must be dimension columns)
	 * @param series
	 *            List of column names of series - Can be either measures or dimension columns. Series will be the
	 *            cross-product of the measures and the dimension columns. For example, if 2 dimension columns are
	 *            chosen and two measures, then each series will be a measure plus a combination of dimension column
	 *            attribute values
	 * @param categoryFormat
	 *            Format for category strings (if they are numeric or date) - ignored if null or more than one category
	 * @param swapSeriesAndCategories
	 *            Swap series and categories in the dataset
	 * @return CategoryDataset instance
	 * @throws Exception 
	 */
	private BirstCategoryDataset createCategoryDataset(QueryResultSet qrs, 
														 List<Integer> categories, 
														 List<Integer> series, 
														 Integer colorColumn) throws Exception
	{
		if (qrs == null || categories == null || series == null)
			return (null);
		
		BirstCategoryDataset cds = new BirstCategoryDataset();
		
		AdhocColumn[] categoryColumns  = new AdhocColumn[categories.size()];
		for (int i = 0; i < categoryColumns.length; i++) {
			Integer index = categories.get(i);
			AdhocEntity e = report.getEntityById(index);
			if (e != null && e instanceof AdhocColumn) {
				categoryColumns[i] = (AdhocColumn)e;
			}
		}
		for (int i = 0; i < categoryColumns.length; i++) {
			if (categoryColumns[i] == null) {
				throw new InvalidDataException("Invalid category in chart.");
			}
		}
		AdhocColumn[] seriesColumns = new AdhocColumn[series.size()];
		for (int i = 0; i < seriesColumns.length; i++) {
			Integer index = series.get(i);
			AdhocEntity e = report.getEntityById(index);
			if (e != null && e instanceof AdhocColumn) {
				seriesColumns[i] = (AdhocColumn)e;
			}
		}
		for (int i = 0; i < seriesColumns.length; i++) {
			if (seriesColumns[i] == null) {
				throw new InvalidDataException("Invalid series in chart.");
			}
		}
		AdhocColumn colorCol = null;
		if (colorColumn != null) {
			colorCol = (AdhocColumn)report.getEntityById(colorColumn);
		}
		
		if (colorCol != null) {
			cds.categoryColorMap = new HashMap<String, Color>();
		}
			
		Map<String, Cell> cellMap = new LinkedHashMap<String, Cell>();
		int rowCount = 0;
		for (DataRow row : getRows()) 
		{
			if (rowCount++ > Chart.MAX_DATASET_SIZE)
				break;
			CellKey category = new CellKey();
			/*
			 * Determine the category
			 */
			for (AdhocColumn ac: categoryColumns) {
				if (!ac.isMeasureExpression())
				{
					Object val = row.getFieldValue(ac.getFieldName(), ac);
					if (!(val instanceof BirstOlapField || val instanceof BirstRatio)) {
						val = formatVal(row, ac);
					}
					else if (val instanceof BirstOlapField) {
						val = formatBirstOlapField((BirstOlapField)val, ac);
					}
					category.add(val);
				}
			}
			CellKey dcseries = new CellKey();
			/*
			 * Determine the dimension column part of series
			 */
			for (AdhocColumn ac: seriesColumns) {
				if (!ac.isMeasureExpression())
				{
					Object val = row.getFieldValue(ac.getFieldName(), ac);
					if (!(val instanceof BirstOlapField || val instanceof BirstRatio)) {
						val = formatVal(row, ac);
					}
					else if (val instanceof BirstOlapField) {
						val = formatBirstOlapField((BirstOlapField)val, ac);
					}
					dcseries.add(val);
				}
			}
			
			saveAdditionalDrillValues(category.toString(), row);
			/*
			 * Now add values for each series entry (measure)
			 */
			try {
				for (AdhocColumn seriesCol: seriesColumns)
				{
					if (seriesCol.isMeasureExpression())
					{
						CellKey seriesStr = new CellKey();
						CellKey rowKey = new CellKey();
						rowKey.addAll(dcseries);
						seriesStr.addAll(dcseries);
						rowKey.add(seriesCol.getLabelString());

						String key = rowKey.toString() + "|" + category.toString();
						Cell c = cellMap.get(key);
						Number val = (Number) row.getFieldValue(seriesCol.getFieldName(), seriesCol);
						if (c == null)
						{
							c = new Cell();
							c.rowKey = rowKey; //numRows == 1 ? category : rowKey;
							c.colKey = category; //numRows == 1 ? rowKey : category;
							c.value = val;
							c.count = (val == null ? 0 : 1);
							if (colorCol != null) {
								Object o = row.getFieldValue(colorCol.getFieldName(), colorCol);
								c.color = o == null ? c.color : o.toString();
							}
							cellMap.put(key, c);
						} else if (val != null)
						{
							String rule = seriesCol.getExprAggRule();
							if (val.getClass() == Double.class)
							{
								c.value = aggregate(rule, c.value, val);
							} else if (val.getClass() == Integer.class || val.getClass() == Long.class)
							{
								c.value = aggregate(rule, c.value, val);
							}
							else if (val.getClass() == BirstRatio.class) {
								c.value = ((BirstRatio)val).aggregate(rule, (BirstRatio)c.value);
							}
							else if (val.getClass() == BirstInterval.class) {
								c.value = ((BirstInterval)val).aggregate(rule, (BirstInterval)c.value);
							}
							else
								continue;
							
							c.count++;
							c.aggRule = rule;
						}
					}
				}
			} catch (ClassCastException e){
				logger.warn(e,e);
				logger.warn("The chart cannot be generated because some of the attributes and/or measures have been defined as the wrong type. \n Please edit using Chart Options and try again.\n");
				throw e;	
			}
			for (Cell c : cellMap.values())
			{
				cds.addValue(c.value, c.rowKey, c.colKey);
				if (c.color != null) {
					Color clr = null;
					try {
						clr = ColorUtils.stringToColor(c.color);
					}
					catch (Exception e) {
						// ignore reason
					}
					
					if (clr == null) {
						throw new InvalidColorException(c.color + " is an invalid color value.");
					}
					
					CellKey colorKey = new CellKey();
					colorKey.addAll(c.colKey);
					colorKey.addAll(c.rowKey);
					cds.categoryColorMap.put(colorKey.toString(), clr);
				}
			}
		}
		for (Cell c : cellMap.values())
		{
			if (c.aggRule != null && c.aggRule.equals("AVG")) {
				if (c.value instanceof Double) {
					c.value = (Double)c.value / c.count;
				}
				else if (c.value instanceof Integer) {
					c.value = (Integer)c.value / c.count;
				}
				cds.addValue(c.value, c.rowKey, c.colKey);
			}
		}
		
		if (cds.getRowCount() * cds.getColumnCount() > 10000)
			throw new ResultSetTooBigException("Too many points in the chart.");
		return (cds);
	}

	private Number aggregate(String rule, Number value1, Number value2) {
		if (value1 == null || Double.isNaN(value1.doubleValue()))
			return value2;
		else if (value2 == null || Double.isNaN(value2.doubleValue()))
			return value1;
		
		return Chart.aggregate(rule, value1.doubleValue(), value2.doubleValue());
	}

	private String formatVal(DataRow row, AdhocColumn col) throws InvalidDataException {
		String fieldName = col.getFieldName();
		Object val = row.getFieldValue(fieldName, col);
		if (val == null)
			return "";
		
		String dataType = row.getWarehouseColumnDataType(fieldName);;
		String f = col.getFormat();
		try {
			UserBean ub = UserBean.getParentThreadUserBean();
			// try/catch because sometimes a number is not a number, a year could be a string...
			if (f != null)
			{
				if (val instanceof java.util.Calendar)
				{
					val = ((java.util.Calendar) val).getTime();
					if (ub != null)
					{
						if (dataType != null && dataType.equals("Date"))
							return ub.getDateFormat(f).format(val);						
						else
							return ub.getDateTimeFormat(f).format(val);						
					}
				}
			}
			else
			{
				if (val instanceof java.util.Calendar)
				{
					SimpleDateFormat sdf = null;
					if (dataType != null && dataType.equals("Date"))
						if (ub != null)
							sdf = ub.getDateFormat();
						else
							sdf = DateUtil.getShortDateFormat(Locale.getDefault());
					else
						if (ub != null)
							sdf = ub.getDateTimeFormat();
						else
							sdf = DateUtil.getShortDateTimeFormat(Locale.getDefault());
					return sdf.format(((java.util.Calendar)val).getTime());
				}
			}
		} catch (Exception ex)
		{
		}
		return val.toString();
	}
	
	private BirstOlapField formatBirstOlapField(BirstOlapField value, AdhocColumn col) throws InvalidDataException {
		String fieldName = col.getFieldName();
		String dataType = col.getDataType();
		String f = col.getFormat();
		Object val = value.getValue();
		try {
			UserBean ub = UserBean.getParentThreadUserBean();
			// try/catch because sometimes a number is not a number, a year could be a string...
			if (f != null)
			{
				if (value.getValue() instanceof java.util.Calendar)
				{
					val = ((java.util.Calendar) value.getValue()).getTime();
					if (ub != null)
					{
						if (dataType != null && dataType.equals("Date"))
							val = ub.getDateFormat(f).format(val);						
						else
							val = ub.getDateTimeFormat(f).format(val);						
					}
				}
			}
			else
			{
				if (value.getValue() instanceof java.util.Calendar)
				{
					SimpleDateFormat sdf = null;
					if (dataType != null && dataType.equals("Date"))
						if (ub != null)
							sdf = ub.getDateFormat();
						else
							sdf = DateUtil.getShortDateFormat(Locale.getDefault());
					else
						if (ub != null)
							sdf = ub.getDateTimeFormat();
						else
							sdf = DateUtil.getShortDateTimeFormat(Locale.getDefault());
					val = sdf.format(((java.util.Calendar)value.getValue()).getTime());
				}
			}
		} catch (Exception ex)
		{
		}
		if (val instanceof Serializable) {
			value.setValue((Serializable)val);
		}
		return value;
	}

	private int getQrsIndexFromReportIndex(AdhocEntity e) {
		if (e != null && e instanceof AdhocColumn) {
			String columnName = ((AdhocColumn)e).getFieldName();
			String[] colNames = qrs.getDisplayNames();
			for (int i = 0; i < colNames.length; i++) 
				if (columnName.equals(colNames[i]))
					return i;
		}
		
		return -1;
	}
	
	@SuppressWarnings("serial")
	private static class CellKey extends ArrayList<Object> implements Comparable<CellKey> {

		@Override
		public int compareTo(
				com.successmetricsinc.chart.AnyChart.AnyChartNode.CellKey o) {
			return toString().compareTo(o.toString());
		}

		/* (non-Javadoc)
		 * @see java.util.AbstractCollection#toString()
		 */
		@Override
		public String toString() {
			StringBuilder b = new StringBuilder();
			for (Object s : this) {
				if (b.length() > 0)
					b.append("\n");
				b.append(s == null ? "null" : s.toString());
			}
			return b.toString();
		}
		
		@Override
		public boolean equals(Object o) {
			if (! (o instanceof CellKey))
				return false;
			
			return toString().equals(o.toString());
		}
		
		@Override
		public int hashCode() {
			return toString().hashCode();
		}
	}
	
	private static class Cell
	{
		CellKey rowKey;
		CellKey colKey;
		Number value;
		String color;
		int count;
		String aggRule;
	}

	public static List<AnyChartNode> createChartList(ChartOptions co, AdhocReport report, OMElement defaults) {
		List<AnyChartNode> ret = new ArrayList<AnyChartNode>();
		AnyChartNode origNode = new AnyChartNode(co, report, defaults); 
		ret.add(origNode);
		if (co.type == ChartOptions.ChartType.BarLine || co.type == ChartOptions.ChartType.StackedColumnLine) {
			AnyChartNode node = new AnyChartNode();
			node.chartType = ChartType.Line;
			node.dataPlotSettings = new DataPlotSettings(co, report, defaults == null ? null : defaults.getFirstChildWithName(new QName(DATA_PLOT_SETTINGS)));
			node.axes = new Axes(co, report, defaults == null ? null : defaults.getFirstChildWithName(new QName(AXES)));
			node.axes.setLocation(false);
			node.categories = new ArrayList<Integer>();
			node.series = new ArrayList<Integer>();
			String lastSeries = co.series.get(co.series.size() - 1);
			for (AdhocColumn ac : report.getColumns()) {
				String label = ac.getLabelString();
				if (co.categories.contains(label))
					node.categories.add(ac.getReportIndex());
				else if (co.series2 != null && co.series2.contains(label)) {
					node.series.add(ac.getReportIndex());
					origNode.axes.setShare(co.shareAxis);
					if (co.shareAxis)
						node.axes.setLocation(true);
				}
				else if ((co.series2 == null || co.series2.isEmpty()) && lastSeries.equals(label)) {
					node.series.add(ac.getReportIndex());
					origNode.axes.setShare(true);
					node.axes.setLocation(true);
				}
			}
			
			ret.add(0, node);
		}
		return ret;
	}

	public void drillDown(AdhocReport report2, Drill drill) {
		AdhocReport report = getReport();
		if (report == null)
			report = report2;
		
		AdhocColumn drillFromColumn = null;
		for (AdhocColumn ac : report.getColumns()) {
			if (ac.getColumn().equals(drill.getFromColumn())) {
				if (((ac.getDimension() == null || ac.getDimension().trim().length() == 0) &&
						(drill.getFromDimension() == null || drill.getFromDimension().trim().length() == 0)) ||
						(ac.getDimension() != null && ac.getDimension().equals(drill.getFromDimension()))) {
					drillFromColumn = ac;
					break;
				}
			}
		}
		
		// don't drill if the report says not to drill, but do even if its supposed to navigate
		// if they clicked on this chart they would have navigated
		if (drillFromColumn != null && ! Chart.DRILL_TYPE_NONE.equals(drillFromColumn.getDrillType())) {
			List<Integer> catOrSeriesList = null;
			boolean added = false;
			Integer fromReportIndex = Integer.valueOf(drillFromColumn.getReportIndex());
			if (categories != null && categories.contains(fromReportIndex))
				catOrSeriesList = categories;
			else if (series != null && series.contains(fromReportIndex))
				catOrSeriesList = series;
			else if (series2 != null && series2.contains(fromReportIndex))
				catOrSeriesList = series2;
			else if (series3 != null && series3.contains(fromReportIndex))
				catOrSeriesList = series3;
			for (com.successmetricsinc.adhoc.DrillColumn dc : drill.getDrillColumns()) {
				Integer toReportIndex = null;
				if (! report.containsDimensionColumn(dc.getDimension(), dc.getColumn())) {
					toReportIndex = Integer.valueOf(report.addDimensionColumn(dc.getDimension(), null, dc.getColumn(), null, dc.getDisplayName(), drillFromColumn.getName(), false));
				}
				if (toReportIndex == null) {
					for (AdhocColumn col: report.getColumns()) {
						if (col.getColumn().equals(dc.getColumn())) {
							if (((col.getDimension() == null || col.getDimension().trim().length() == 0) &&
									(dc.getDimension() == null || dc.getDimension().trim().length() == 0)) ||
									(col.getDimension() != null && col.getDimension().equals(dc.getDimension()))) {
								toReportIndex = Integer.valueOf(col.getReportIndex());
								break;
							}
						}
					}
				}
				if (toReportIndex != null && catOrSeriesList != null && added == false) {
					catOrSeriesList.remove(fromReportIndex);
					catOrSeriesList.add(toReportIndex);
					added = true;
				}
			}
		}
	}

	public List<Integer> getCategories() {
		return categories;
	}

	public List<Integer> getSeries() {
		return series;
	}
	
	public List<Integer> getSeries2() {
		return series2;
	}
	
	public List<Integer> getSeries3() {
		return series3;
	}

	public class DataRows implements Iterable<DataRows.DataRow> {
		private QueryResultSet qrs;
		private HashMap<String, Integer> fieldNameIndex = new HashMap<String, Integer>();
		private Object[][] rows;
		
		public DataRows(QueryResultSet qs) {
			qrs = qs;
			rows = qrs.getRows();
		}
		
		public int size() {
			return rows.length;
		}
		
		public DataRow getFirst() {
			return new DataRow(0);
		}

		public void setRows(Object[][] newRows) {
			rows = newRows;
		}

		int getIndexForFieldName(String fieldName) {
			Integer index = fieldNameIndex.get(fieldName);
			if (index == null) {
				int i = qrs.getIndexForDisplayName(fieldName);
				index = Integer.valueOf(i);
				fieldNameIndex.put(fieldName, index);
			}
			
			return index == null ? -1 : index.intValue();
		}
		
		public class DataRow {
			int row;
			
			public DataRow(int r) {
				row = r;
			}
			public String getWarehouseColumnDataType(String fieldName) {
				int col = getIndexForFieldName(fieldName);
				String[] warehouseColumnDataTypes = qrs.getWarehouseColumnsDataTypes();
				if (col < 0 || col >= warehouseColumnDataTypes.length)
					return (null);
				return warehouseColumnDataTypes[col];
			}
			public Object getFieldValue(String fieldName, AdhocColumn ac) throws InvalidDataException {
				int index = fieldName.indexOf(BirstRatio.RATIO);
				if (index > 0) {
					String prefix = fieldName.substring(0, index);
					String numerator = prefix + BirstRatio.NUMERATOR;
					String denominator = prefix + BirstRatio.DENOMINATOR;
					Number n = (Number)internalGetFieldValue(numerator);
					Number d = (Number)internalGetFieldValue(denominator);
					
					if (n == null || d == null)
						return null;
					
					return new BirstRatio(n, d);
				}
				
				index = fieldName.indexOf(BirstOlapField.OLAP);
				if (index >= 0) {
					return QueryResultSet.getBirstOlapFieldFromRow(fieldName, rows[row], qrs.getDisplayNames(), qrs);
				}
				
				Object ret = internalGetFieldValue(fieldName);
				if (ac != null && ret != null && ret instanceof Number) {
					String format = ac.getFormat();
					if (format != null && format.toUpperCase().startsWith(ToTime.TO_TIME_FORMAT_PREFIX)) {
						String formatArgs[]=(format.substring(format.indexOf("(") + 1, format.lastIndexOf(")"))).split("','");
						String toTimeFields = formatArgs[0].substring(formatArgs[0].indexOf("'") + 1);
						String toTimeFormat = formatArgs[1].substring(0, formatArgs[1].indexOf("'"));
						
						return new BirstInterval((Number)ret, toTimeFields, toTimeFormat);
					}
				}
				return ret;
			}
			
			private Object internalGetFieldValue(String fieldName) throws InvalidDataException {
				int col = getIndexForFieldName(fieldName);
				if (row < 0 || row >= rows.length)
					return (null);
				if (col < 0 || col >= rows[row].length)
					return (null);
				Object ret = rows[row][col];
				if (valueNotSupportedByChart(ret, null))
					return null;
				
				return ret;
			}
			
			public String[] getColNames() {
				return qrs.getDisplayNames();
			}
		}
		
		public class DataRowIterator implements Iterator<DataRow> {

			private int row = 0;
			private int rowCount;
			
			public DataRowIterator() {
				row = -1;
				rowCount = qrs.numRows();
			}

			@Override
			public boolean hasNext() {
				return rowCount > 0 && (row == -1 || row < rowCount - 1);
			}

			@Override
			public DataRow next() {
				row++;
				return new DataRow(row);
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}

			
		}

		@Override
		public Iterator<DataRows.DataRow> iterator() {
			// TODO Auto-generated method stub
			return new DataRowIterator();
		}
		
	}

	public FillType getFillType() {
		return fillType;
	}

	public Gradient getFillGradient() {
		return fillGradient;
	}
	
	public String getFillTypeString() {
		return fillTypeMap.get(fillType);
	}
	
	public Axes getAxes() {
		return axes;
	}
	
	public void setShowDisplayValues(boolean a)
	{
		showValues = a;
	}

	public void applyStyle(AnyChartNode style) {
		if (style == null)
			return;
		
		
		// don't use thresholds in style
		//useCustomThresholds = style.useCustomThresholds;
		// <thresholds>
		// private List<Threshold> thresholds;
		//private List<CustomThreshold> customThresholds;
		colorList = new ArrayList<Color>();
		if (style.colorList != null && style.colorList.size() > 0)
			colorList.addAll(style.colorList);
		
		fillType = style.fillType;
		
		showValues = style.showValues;
		showValuesVerticalAlignment = style.showValuesVerticalAlignment;
		useSameColor = style.useSameColor;

		showMeasureNamesInTooltip = style.showMeasureNamesInTooltip;
		displayValueFormat = style.displayValueFormat;
		displayValueFont = style.displayValueFont;
		displayValueColor = style.displayValueColor;
		displayValueRotation = style.displayValueRotation;
		tooltipFormat = style.tooltipFormat;
		tooltipFont = style.tooltipFont;
		tooltipColor = style.tooltipColor;

		dataPlotSettings.applyStyle(style.dataPlotSettings);
		axes.applyStyle(style.axes);
		fillGradient.applyStyle(style.fillGradient);
		
	}

	public void setThresholding(int thresholding) {
		this.thresholding = thresholding;
	}
}
