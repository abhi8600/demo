package com.successmetricsinc.chart.AnyChart;

import java.awt.Color;
import java.util.Iterator;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.successmetricsinc.util.BirstXMLSerializable;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

public class Font extends BirstXMLSerializable {

	private static final long serialVersionUID = 1L;
	private static final String UNDERLINE = "Underline";
	private static final String COLOR = "Color";
	private static final String ITALIC = "Italic";
	private static final String BOLD = "Bold";
	private static final String SIZE = "Size";
	private static final String FAMILY = "Family";

	// family=
	private String family;
	
	// size=
	private int size;

	//bold=
	private boolean isBold;
	
	//italic=
	private boolean isItalic;
	
	//underline=
	private boolean isUnderline;
	
	//color=
	private Color color;

	public void parseElement(OMElement parent) {
		for (@SuppressWarnings("unchecked")
		Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();
			
			if (FAMILY.equals(elName)) {
				family = XmlUtils.getStringContent(el);
			}
			else if (SIZE.equals(elName)) {
				size = XmlUtils.getIntContent(el);
			}
			else if (BOLD.equals(elName)) {
				isBold = XmlUtils.getBooleanContent(el);
			}
			else if (ITALIC.equals(elName)) {
				isItalic = XmlUtils.getBooleanContent(el);
			}
			else if (UNDERLINE.equals(elName)) {
				isUnderline = XmlUtils.getBooleanContent(el);
			}
			else if (COLOR.equals(elName)) {
				color = XmlUtils.getColorContent(el);
			}
		}
	}
	
	@Override
	public void parseElement(XMLStreamReader el) throws Exception {
		for (XMLStreamIterator iter = new XMLStreamIterator(el); iter.hasNext(); ) {
			String elName = el.getLocalName();
			
			if (FAMILY.equals(elName)) {
				family = XmlUtils.getStringContent(el);
			}
			else if (SIZE.equals(elName)) {
				size = XmlUtils.getIntContent(el);
			}
			else if (BOLD.equals(elName)) {
				isBold = XmlUtils.getBooleanContent(el);
			}
			else if (ITALIC.equals(elName)) {
				isItalic = XmlUtils.getBooleanContent(el);
			}
			else if (UNDERLINE.equals(elName)) {
				isUnderline = XmlUtils.getBooleanContent(el);
			}
			else if (COLOR.equals(elName)) {
				color = XmlUtils.getColorContent(el);
			}
		}
	}
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, FAMILY, family, ns);
		XmlUtils.addContent(fac, parent, SIZE, size, ns);
		XmlUtils.addContent(fac, parent, BOLD, isBold, ns);
		XmlUtils.addContent(fac, parent, ITALIC, isItalic, ns);
		XmlUtils.addContent(fac, parent, UNDERLINE, isUnderline, ns);
		XmlUtils.addContent(fac, parent, COLOR, color, ns);
	}

	@Override
	public BirstXMLSerializable createNew() {
		return new Font();
	}

}
