/**
 * 
 */
package com.successmetricsinc.chart.AnyChart;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.successmetricsinc.chart.ChartOptions;
import com.successmetricsinc.util.BirstXMLSerializable;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class NewGaugeSeries extends BirstXMLSerializable {
	private static final long serialVersionUID = 1L;
	
	private static final String TYPE = "Type";
	private static final String ORIENTATION = "Orientation";
	private static final String CUSTOM_THRESHOLDS = "customThresholds";
	private static final String POINTER_TYPE = "PointerType";
	private static final String POINTER_COLOR = "PointerColor";
	private static final String MINIMUM = "Minimum";
	private static final String MAXIMUM = "Maximum";
	private static final String MAJOR_INTERVAL = "MajorInterval";
	private static final String MINOR_INTERVAL = "MinorInterval";
	private static final String START_ANGLE = "StartAngle";
	private static final String SWEEP_ANGLE = "SweepAngle";
	private static final String RADIUS = "Radius";
	private static final String COLOR_RANGE_START_SIZE = "ColorRangeStartSize";
	private static final String COLOR_RANGE_END_SIZE = "ColorRangeEndSize";
	private static final String COLOR_RANGE_ALIGN = "ColorRangeAlign";
	private static final String SHOW_SCALE_BAR = "ShowScaleBar";
	private static final String SCALE_BAR_COLOR = "ScaleBarColor";
	
	private String type = "linear";
	private String orientation = "vertical"; // vertical, horizontal
	private List<CustomThreshold> customThresholds = null;
	private String pointerType = "Bar"; // Needle, Bar
	private Color pointerColor = Color.GRAY;

	private String minimum;
	private String maximum;
	private String majorInterval;
	private String minorInterval;
	private String startAngle;
	private String sweepAngle;
	private String radius;
	
	private String colorRangeStartSize;
	private String colorRangeEndSize;
	private String colorRangeAlign = "Center";
	
	private boolean showScaleBar = true;
	private Color scaleBarColor = Color.BLUE;

	public NewGaugeSeries() {
		
	}
	
	public NewGaugeSeries(ChartOptions co, OMElement defaultSettings) {
		parseElement(defaultSettings);
	}

	public NewGaugeSeries(OMElement defaultSettings) {
		parseElement(defaultSettings);
	}

	/* (non-Javadoc)
	 * @see com.successmetricsinc.util.BirstXMLSerializable#addContent(org.apache.axiom.om.OMFactory, org.apache.axiom.om.OMElement, org.apache.axiom.om.OMNamespace)
	 */
	@Override
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, CUSTOM_THRESHOLDS, customThresholds, ns);
		XmlUtils.addContent(fac, parent, TYPE, type, ns);
		XmlUtils.addContent(fac, parent, ORIENTATION, orientation, ns);		
		XmlUtils.addContent(fac, parent, POINTER_TYPE, pointerType, ns);
		XmlUtils.addContent(fac, parent, POINTER_COLOR, pointerColor, ns);
		XmlUtils.addContent(fac, parent, MINIMUM, minimum, ns);
		XmlUtils.addContent(fac, parent, MAXIMUM, maximum, ns);
		XmlUtils.addContent(fac, parent, MAJOR_INTERVAL, majorInterval, ns);
		XmlUtils.addContent(fac, parent, MINOR_INTERVAL, minorInterval, ns);
		XmlUtils.addContent(fac, parent, START_ANGLE, startAngle, ns);
		XmlUtils.addContent(fac, parent, SWEEP_ANGLE, sweepAngle, ns);
		XmlUtils.addContent(fac, parent, RADIUS, radius, ns);
		XmlUtils.addContent(fac, parent, COLOR_RANGE_START_SIZE, colorRangeStartSize, ns);
		XmlUtils.addContent(fac, parent, COLOR_RANGE_END_SIZE, colorRangeEndSize, ns);
		XmlUtils.addContent(fac, parent, COLOR_RANGE_ALIGN, colorRangeAlign, ns);	
		XmlUtils.addContent(fac, parent, SHOW_SCALE_BAR, showScaleBar, ns);	
		XmlUtils.addContent(fac, parent, SCALE_BAR_COLOR, scaleBarColor, ns);	
	}

	/* (non-Javadoc)
	 * @see com.successmetricsinc.util.BirstXMLSerializable#createNew()
	 */
	@Override
	public BirstXMLSerializable createNew() {
		// TODO Auto-generated method stub
		return new NewGaugeSeries();
	}

	/* (non-Javadoc)
	 * @see com.successmetricsinc.util.BirstXMLSerializable#parseElement(org.apache.axiom.om.OMElement)
	 */
	@Override
	public void parseElement(OMElement parent) {
		if (parent == null)
			return;
		for (@SuppressWarnings("unchecked")
		Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();
			
			if (POINTER_TYPE.equals(elName))
				pointerType = XmlUtils.getStringContent(el);
			else if (POINTER_COLOR.equals(elName))
				pointerColor = XmlUtils.getColorContent(el);				
			else if (TYPE.equals(elName))
				type = XmlUtils.getStringContent(el);	
			else if (ORIENTATION.equals(elName))
				orientation = XmlUtils.getStringContent(el);	
			else if (MINIMUM.equals(elName))
				minimum = XmlUtils.getStringContent(el);	
			else if (MAXIMUM.equals(elName))
				maximum = XmlUtils.getStringContent(el);	
			else if (MAJOR_INTERVAL.equals(elName))
				majorInterval = XmlUtils.getStringContent(el);		
			else if (MINOR_INTERVAL.equals(elName))
				minorInterval = XmlUtils.getStringContent(el);		
			else if (START_ANGLE.equals(elName))
				startAngle = XmlUtils.getStringContent(el);		
			else if (SWEEP_ANGLE.equals(elName))
				sweepAngle = XmlUtils.getStringContent(el);	
			else if (RADIUS.equals(elName))
				radius = XmlUtils.getStringContent(el);				
			else if (CUSTOM_THRESHOLDS.equals(elName))
				customThresholds = (List<CustomThreshold>)XmlUtils.parseAllChildren(el, new CustomThreshold(), new ArrayList<CustomThreshold>());	
			else if (COLOR_RANGE_ALIGN.equals(elName))
				colorRangeAlign = XmlUtils.getStringContent(el);
			else if (COLOR_RANGE_START_SIZE.equals(elName))
				colorRangeStartSize = XmlUtils.getStringContent(el);
			else if (COLOR_RANGE_END_SIZE.equals(elName))
				colorRangeEndSize = XmlUtils.getStringContent(el);
			else if (SHOW_SCALE_BAR.equals(elName))
				showScaleBar = XmlUtils.getBooleanContent(el);
			else if (SCALE_BAR_COLOR.equals(elName))
				scaleBarColor = XmlUtils.getColorContent(el);				
		}
	}

	@Override
	public void parseElement(XMLStreamReader el) throws Exception {
		for (XMLStreamIterator iter = new XMLStreamIterator(el); iter.hasNext(); ) {
			String elName = el.getLocalName();
			
			if (POINTER_TYPE.equals(elName))
				pointerType = XmlUtils.getStringContent(el);	
			else if (POINTER_COLOR.equals(elName))
				pointerColor = XmlUtils.getColorContent(el);							
			else if (TYPE.equals(elName))
				type = XmlUtils.getStringContent(el);	
			else if (ORIENTATION.equals(elName))
				orientation = XmlUtils.getStringContent(el);	
			else if (MINIMUM.equals(elName))
				minimum = XmlUtils.getStringContent(el);	
			else if (MAXIMUM.equals(elName))
				maximum = XmlUtils.getStringContent(el);	
			else if (MAJOR_INTERVAL.equals(elName))
				majorInterval = XmlUtils.getStringContent(el);		
			else if (MINOR_INTERVAL.equals(elName))
				minorInterval = XmlUtils.getStringContent(el);
			else if (START_ANGLE.equals(elName))
				startAngle = XmlUtils.getStringContent(el);		
			else if (SWEEP_ANGLE.equals(elName))
				sweepAngle = XmlUtils.getStringContent(el);	
			else if (RADIUS.equals(elName))
				radius = XmlUtils.getStringContent(el);				
			else if (CUSTOM_THRESHOLDS.equals(elName))
				customThresholds = (List<CustomThreshold>)XmlUtils.parseAllChildren(el, new CustomThreshold(), new ArrayList<CustomThreshold>());
			else if (COLOR_RANGE_ALIGN.equals(elName))
				colorRangeAlign = XmlUtils.getStringContent(el);
			else if (COLOR_RANGE_START_SIZE.equals(elName))
				colorRangeStartSize = XmlUtils.getStringContent(el);
			else if (COLOR_RANGE_END_SIZE.equals(elName))
				colorRangeEndSize = XmlUtils.getStringContent(el);
			else if (SHOW_SCALE_BAR.equals(elName))
				showScaleBar = XmlUtils.getBooleanContent(el);
			else if (SCALE_BAR_COLOR.equals(elName))
				scaleBarColor = XmlUtils.getColorContent(el);							
		}
	}
	
	public List<CustomThreshold> getCustomThresholds()
	{
		return this.customThresholds;
	}
	
	public String getPointerType()
	{
		return pointerType;
	}
	
	public Color getPointerColor()
	{
		return pointerColor;
	}
	
	public String getType()
	{
		return type;
	}

	public String getOrientation() {
		return orientation;
	}
	
	public void setOrientationVertical(boolean b) {
		orientation = (b ? "vertical" : "horizontal");
	}

	public String getMinimum() {
		return minimum;
	}

	public String getMaximum() {
		return maximum;
	}

	public String getMajorInterval() {
		return majorInterval;
	}

	public String getMinorInterval() {
		return minorInterval;
	}

	public String getStartAngle() {
		return startAngle;
	}

	public String getSweepAngle() {
		return sweepAngle;
	}

	public String getRadius() {
		return radius;
	}

	public String getColorRangeStartSize() {
		return colorRangeStartSize;
	}

	public String getColorRangeEndSize() {
		return colorRangeEndSize;
	}

	public String getColorRangeAlign() {
		return colorRangeAlign;
	}

	public boolean isShowScaleBar() {
		return showScaleBar;
	}

	public Color getScaleBarColor() {
		return scaleBarColor;
	}

	public void applyStyle(NewGaugeSeries style) {
		if (style == null)
			return;
		
		type = style.type;
		if (style.customThresholds != null)
		{
			customThresholds = new ArrayList<CustomThreshold>(style.customThresholds.size());
			customThresholds.addAll(style.customThresholds);
		}
		pointerType = style.pointerType;
		pointerColor = style.pointerColor;
		orientation = style.orientation;
		minimum = style.minimum;
		maximum = style.maximum;
		minorInterval = style.minorInterval;
		majorInterval = style.majorInterval;
		startAngle = style.startAngle;
		sweepAngle = style.sweepAngle;
		radius = style.radius;
		colorRangeAlign = style.colorRangeAlign;
		colorRangeStartSize = style.colorRangeStartSize;
		colorRangeEndSize = style.colorRangeEndSize;
		showScaleBar = style.showScaleBar;
		scaleBarColor = style.scaleBarColor;
	}
}
