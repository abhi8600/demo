package com.successmetricsinc.chart.AnyChart;

import java.util.Iterator;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.successmetricsinc.util.BirstXMLSerializable;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

public class ColorSwatch extends BirstXMLSerializable {

	private static final String LABELS = "Labels";

	private static final String THRESHOLD = "Threshold";

	private String threshold;
	

	@Override
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, THRESHOLD, threshold, ns);
	}

	@Override
	public BirstXMLSerializable createNew() {
		return new ColorSwatch();
	}

	@Override
	public void parseElement(OMElement parent) {
		for (Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();

			if (THRESHOLD.equals(elName)) {
				threshold = XmlUtils.getStringContent(el);
			}

		}
	}

	@Override
	public void parseElement(XMLStreamReader parser) throws Exception {
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
			String elName = parser.getLocalName();

			if (THRESHOLD.equals(elName)) {
				threshold = XmlUtils.getStringContent(parser);
			}
		}
	}

	public void applyStyle(ColorSwatch style) {
		if (style == null)
			return;
		
		threshold = style.threshold;
	}
}
