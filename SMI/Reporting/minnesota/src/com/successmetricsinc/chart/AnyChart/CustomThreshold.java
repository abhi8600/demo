/**
 * 
 */
package com.successmetricsinc.chart.AnyChart;

import java.awt.Color;
import java.util.Iterator;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.successmetricsinc.util.BirstXMLSerializable;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class CustomThreshold extends BirstXMLSerializable {

	private static final long serialVersionUID = 1L;
	private static final String COLOR = "color";
	private static final String MAXIMUM = "max";
	private static final String MINIMUM = "min";
	private static final String LABEL = "label";
	
	protected String label;
	protected Double minValue;
	protected Double maxValue; 
	protected Color color;
	
	/* (non-Javadoc)
	 * @see com.successmetricsinc.util.BirstXMLSerializable#addContent(org.apache.axiom.om.OMFactory, org.apache.axiom.om.OMElement, org.apache.axiom.om.OMNamespace)
	 */
	@Override
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, LABEL, label, ns);
		if (minValue != null)
			XmlUtils.addContent(fac, parent, MINIMUM, minValue, ns);
		if (maxValue != null)
			XmlUtils.addContent(fac, parent, MAXIMUM, maxValue, ns);
		XmlUtils.addContent(fac, parent, COLOR, color, ns);
	}

	/* (non-Javadoc)
	 * @see com.successmetricsinc.util.BirstXMLSerializable#createNew()
	 */
	@Override
	public BirstXMLSerializable createNew() {
		return new CustomThreshold();
	}

	/* (non-Javadoc)
	 * @see com.successmetricsinc.util.BirstXMLSerializable#parseElement(org.apache.axiom.om.OMElement)
	 */
	@Override
	public void parseElement(OMElement parent) {
		for (@SuppressWarnings("unchecked")
		Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();

			if (LABEL.equals(elName))
				label = XmlUtils.getStringContent(el);
			else if (MINIMUM.equals(elName))
				minValue = XmlUtils.getDoubleContent(el);
			else if (MAXIMUM.equals(elName))
				maxValue = XmlUtils.getDoubleContent(el);
			else if (COLOR.equals(elName))
				color = XmlUtils.getColorContent(el);
		}
	}

	@Override
	public void parseElement(XMLStreamReader el) throws Exception {
		for (XMLStreamIterator iter = new XMLStreamIterator(el); iter.hasNext(); ) {
			String elName = el.getLocalName();

			if (LABEL.equals(elName))
				label = XmlUtils.getStringContent(el);
			else if (MINIMUM.equals(elName))
				minValue = XmlUtils.getDoubleContent(el);
			else if (MAXIMUM.equals(elName))
				maxValue = XmlUtils.getDoubleContent(el);
			else if (COLOR.equals(elName))
				color = XmlUtils.getColorContent(el);
		}
	}

}
