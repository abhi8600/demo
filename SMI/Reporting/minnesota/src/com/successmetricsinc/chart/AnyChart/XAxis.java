/**
 * 
 */
package com.successmetricsinc.chart.AnyChart;

import java.awt.Color;
import java.awt.Font;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.jdom.Element;

import com.successmetricsinc.UserBean;
import com.successmetricsinc.chart.ChartOptions;
import com.successmetricsinc.chart.FlashRenderer;
import com.successmetricsinc.chart.AnyChart.AnyChartNode.ChartType;
import com.successmetricsinc.transformation.integer.ToTime;
import com.successmetricsinc.util.BirstXMLSerializable;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class XAxis extends BirstXMLSerializable {
	private static final String XAXIS_LINE_COLOR = "XAxisLineColor";
	private static final String XAXIS_LINE_WIDTH = "XAxisLineWidth";
	private static final String XAXIS_LINE_STYLE = "XAxisLineStyle";
	private static final String XAXIS_MINOR_TICKMARK_COLOR = "XAxisMinorTickmarkColor";
	private static final String XAXIS_MINOR_TICKMARK_WIDTH = "XAxisMinorTickmarkWidth";
	private static final String XAXIS_MAJOR_TICKMARK_COLOR = "XAxisMajorTickmarkColor";
	private static final String XAXIS_MAJOR_TICKMARK_WIDTH = "XAxisMajorTickmarkWidth";
	private static final String XAXIS_MINOR_GRID_LINE_COLOR = "XAxisMinorGridLineColor";
	private static final String XAXIS_MINOR_GRID_LINE_WIDTH = "XAxisMinorGridLineWidth";
	private static final String XAXIS_MINOR_GRID_LINE_TYPE = "XAxisMinorGridLineType";
	private static final String XAXIS_MAJOR_GRID_LINE_COLOR = "XAxisMajorGridLineColor";
	private static final String XAXIS_MAJOR_GRID_LINE_WIDTH = "XAxisMajorGridLineWidth";
	private static final String XAXIS_MAJOR_GRID_LINE_TYPE = "XAxisMajorGridLineType";
	private static final long serialVersionUID = 1L;
	private static final String XAXIS_SHOW_INTERLACE = "XAxisShowInterlace";
	private static final String TITLE_COLOR = "TitleColor";
	private static final String LABEL_COLOR = "LabelColor";
	private static final String TITLE_FONT = "TitleFont";
	private static final String XAXIS_TITLE = "XAxisTitle";
	private static final String XAXIS_DEFAULT_LABEL = "XAxisDefaultTitle";
	private static final String XAXIS_POSITION = "XAxisPosition";
	private static final String XAXIS_GROUP_LABELS = "XAxisGroupLabels";
	private static final String XAXIS_SHOW_MINOR_TICKMARKS = "XAxisShowMinorTickmarks";
	private static final String XAXIS_SHOW_MAJOR_TICKMARKS = "XAxisShowMajorTickmarks";
	private static final String XAXIS_TICKMARK_LABELS = "XAxisTickmarkLabels";
	private static final String XAXIS_SHOW_MINOR_GRID = "XAxisShowMinorGrid";
	private static final String XAXIS_SHOW_MAJOR_GRID = "XAxisShowMajorGrid";
	private static final String XAXIS_LABEL_ANGLE = "XAxisLabelAngle";
	private static final String XAXIS_LABELS = "XAxisLabels";
	private static final String XAXIS_ENABLED = "XAxisEnabled";
	private static final String FONT = "font";
	private static final String XAXIS_MAJOR_INTERVAL = "XAxisMajorInterval";
	private static final String XAXIS_MINOR_INTERVAL = "XAxisMinorInterval";
	private static final String XAXIS_SCALE_TYPE = "XAxisScaleType";
	private static final String XAXIS_MAX_CHAR = "XAxisMaxChar";
	private static final String XAXIS_ZERO_LINE_COLOR = "XAxisZeroLineColor";
	private static final String XAXIS_ZERO_LINE_WIDTH = "XAxisZeroLineWidth";
	private static final String XAXIS_LABEL_FORMAT = "XAxisLabelFormat";
	private static final String XAXIS_MINIMUM = "XAxisMinimum";
	private static final String XAXIS_MAXIMUM = "XAxisMaximum";

	public enum LineStyle { Solid, Dashed };
	public static final Map<LineStyle, String> lineStyleMap;
	public static final LineStyle DEFAULT_LINE_STYLE = LineStyle.Dashed;
	public static final Color DEFAULT_LINE_COLOR = new Color(204, 204, 204);  // #CCCCCC
	public static final Color DEFAULT_ZERO_LINE_COLOR = new Color(255, 0, 0); // red
	public static final String DEFAULT_SCALE_TYPE = "Linear";
	
	private Font font;
	private Color labelColor = Color.BLACK;
	private Font titleFont;
	private Color titleColor = Color.BLACK;
	
	private boolean xEnabled;
	private boolean xShowLabels;
	private double xLabelAngle;
	private LineStyle axisStyle = LineStyle.Solid;
	private int axisWidth = 1;
	private Color axisColor = DEFAULT_LINE_COLOR;
	private boolean xShowMajorGrid;
	private LineStyle xMajorGridLineStyle = DEFAULT_LINE_STYLE;
	private Color xMajorGridLineColor = DEFAULT_LINE_COLOR;
	private int xMajorGridLineThickness = 1;
	private boolean xShowMinorGrid;
	private LineStyle xMinorGridLineStyle = DEFAULT_LINE_STYLE;
	private Color xMinorGridLineColor = DEFAULT_LINE_COLOR;
	private int xMinorGridLineThickness = 1;
	private boolean xShowTickmarkLabels;
	private boolean xShowMajorTickmarks;
	private int xMajorTickmarkWidth = 1;
	private Color xMajorTickmarkColor = DEFAULT_LINE_COLOR;
	private boolean xShowMinorTickmarks;
	private int xMinorTickmarkWidth = 1;
	private Color xMinorTickmarkColor = DEFAULT_LINE_COLOR;
	private boolean xShowGroupLabels;
	private String xPosition;
	private boolean xUseDefaultLabel;
	private String xLabel;
	private double xMajorInterval;
	private double xMinorInterval;
	private boolean xShowInterlace = false;
	private String xScaleType;
	private String xMaxChar = "30";
	private Color xAxisZeroLineColor = XAxis.DEFAULT_ZERO_LINE_COLOR;
	private String xAxisZeroLineWidth;
	private String xAxisLabelFormat;
	private String xMaximum;
	private String xMinimum;
	
	static {
		lineStyleMap = new HashMap<LineStyle, String>();
		lineStyleMap.put(LineStyle.Solid, "Solid");
		lineStyleMap.put(LineStyle.Dashed, "Dashed");
	}
	
	
	public XAxis() {
		xScaleType = DEFAULT_SCALE_TYPE;
	}

	public XAxis(ChartOptions co) {
		font = co.getLabelFont();
		titleFont = font.deriveFont(Font.BOLD);;
		xEnabled = true;
		xShowLabels = co.domainLabels;
		xLabelAngle = co.categoryLabelAngle;
		xShowMajorGrid = co.showMajorGrid;
		xShowMinorGrid = co.showMinorGrid;
		xShowTickmarkLabels = co.domainTickMarkLabels;
		xShowMajorTickmarks = co.showMajorTickmarks;
		xShowMinorTickmarks = co.showMinorTickmarks;
		xShowGroupLabels = co.domainGroupLabels;
		xPosition = "Normal";
		xUseDefaultLabel = co.domainLabels && (co.domainLabel == null);
		xLabel = co.domainLabel;
		xMajorInterval = co.getMajorInterval();
		xMinorInterval = co.getMinorInterval();
		xScaleType = DEFAULT_SCALE_TYPE;
	}

	/* (non-Javadoc)
	 * @see com.successmetricsinc.util.BirstXMLSerializable#addContent(org.apache.axiom.om.OMFactory, org.apache.axiom.om.OMElement, org.apache.axiom.om.OMNamespace)
	 */
	@Override
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, FONT, font, ns);
		XmlUtils.addContent(fac, parent, TITLE_FONT, titleFont, ns);
		XmlUtils.addContent(fac, parent, LABEL_COLOR, labelColor, ns);
		XmlUtils.addContent(fac, parent, TITLE_COLOR, titleColor, ns);
		XmlUtils.addContent(fac, parent, XAXIS_ENABLED, xEnabled, ns);
		XmlUtils.addContent(fac, parent, XAXIS_LABELS, xShowLabels, ns);
		XmlUtils.addContent(fac, parent, XAXIS_LABEL_ANGLE, xLabelAngle, ns);
		XmlUtils.addContent(fac, parent, XAXIS_LINE_STYLE, getLineStyleString(axisStyle), ns);
		XmlUtils.addContent(fac, parent, XAXIS_LINE_WIDTH, axisWidth, ns);
		XmlUtils.addContent(fac, parent, XAXIS_LINE_COLOR, axisColor, ns);
		XmlUtils.addContent(fac, parent, XAXIS_SHOW_MAJOR_GRID, xShowMajorGrid, ns);
		XmlUtils.addContent(fac, parent, XAXIS_MAJOR_GRID_LINE_TYPE, getLineStyleString(xMajorGridLineStyle), ns);
		XmlUtils.addContent(fac, parent, XAXIS_MAJOR_GRID_LINE_WIDTH, xMajorGridLineThickness, ns);
		XmlUtils.addContent(fac, parent, XAXIS_MAJOR_GRID_LINE_COLOR, xMajorGridLineColor, ns);
		XmlUtils.addContent(fac, parent, XAXIS_SHOW_MINOR_GRID, xShowMinorGrid, ns);
		XmlUtils.addContent(fac, parent, XAXIS_MINOR_GRID_LINE_TYPE, getLineStyleString(xMinorGridLineStyle), ns);
		XmlUtils.addContent(fac, parent, XAXIS_MINOR_GRID_LINE_WIDTH, xMinorGridLineThickness, ns);
		XmlUtils.addContent(fac, parent, XAXIS_MINOR_GRID_LINE_COLOR, xMinorGridLineColor, ns);
		XmlUtils.addContent(fac, parent, XAXIS_TICKMARK_LABELS, xShowTickmarkLabels, ns);
		XmlUtils.addContent(fac, parent, XAXIS_SHOW_MAJOR_TICKMARKS, xShowMajorTickmarks, ns);
		XmlUtils.addContent(fac, parent, XAXIS_MAJOR_TICKMARK_WIDTH, xMajorTickmarkWidth, ns);
		XmlUtils.addContent(fac, parent, XAXIS_MAJOR_TICKMARK_COLOR, xMajorTickmarkColor, ns);
		XmlUtils.addContent(fac, parent, XAXIS_SHOW_MINOR_TICKMARKS, xShowMinorTickmarks, ns);
		XmlUtils.addContent(fac, parent, XAXIS_MINOR_TICKMARK_WIDTH, xMinorTickmarkWidth, ns);
		XmlUtils.addContent(fac, parent, XAXIS_MINOR_TICKMARK_COLOR, xMinorTickmarkColor, ns);
		XmlUtils.addContent(fac, parent, XAXIS_GROUP_LABELS, xShowGroupLabels, ns);
		XmlUtils.addContent(fac, parent, XAXIS_POSITION, xPosition, ns);
		XmlUtils.addContent(fac, parent, XAXIS_DEFAULT_LABEL, xUseDefaultLabel, ns);
		XmlUtils.addContent(fac, parent, XAXIS_TITLE, xLabel, ns);
		XmlUtils.addContent(fac, parent, XAXIS_MAJOR_INTERVAL, xMajorInterval, ns);
		XmlUtils.addContent(fac, parent, XAXIS_MINOR_INTERVAL, xMinorInterval, ns);
		XmlUtils.addContent(fac, parent, XAXIS_SHOW_INTERLACE, xShowInterlace, ns);
		XmlUtils.addContent(fac, parent, XAXIS_SCALE_TYPE, xScaleType, ns);
		XmlUtils.addContent(fac, parent, XAXIS_MAX_CHAR, xMaxChar, ns);
		XmlUtils.addContent(fac, parent, XAXIS_ZERO_LINE_COLOR, xAxisZeroLineColor, ns);
		XmlUtils.addContent(fac, parent, XAXIS_ZERO_LINE_WIDTH, xAxisZeroLineWidth, ns);
		XmlUtils.addContent(fac, parent, XAXIS_LABEL_FORMAT, xAxisLabelFormat, ns);
		XmlUtils.addContent(fac, parent, XAXIS_MAXIMUM, xMaximum, ns);
		XmlUtils.addContent(fac, parent, XAXIS_MINIMUM, xMinimum, ns);
	}

	/* (non-Javadoc)
	 * @see com.successmetricsinc.util.BirstXMLSerializable#createNew()
	 */
	@Override
	public BirstXMLSerializable createNew() {
		return new XAxis();
	}

	/* (non-Javadoc)
	 * @see com.successmetricsinc.util.BirstXMLSerializable#parseElement(org.apache.axiom.om.OMElement)
	 */
	@Override
	public void parseElement(OMElement parent) {
		boolean titleFontFound = false;
		for (@SuppressWarnings("unchecked")
		Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();

			if (FONT.equals(elName))
				font = XmlUtils.getFontContent(el);
			else if (TITLE_FONT.equals(elName)) {
				titleFontFound = true;
				titleFont = XmlUtils.getFontContent(el);
			}
			else if (LABEL_COLOR.equals(elName))
				labelColor = XmlUtils.getColorContent(el);
			else if (TITLE_COLOR.equals(elName))
				titleColor = XmlUtils.getColorContent(el);
			else if (XAXIS_ENABLED.equals(elName))
				xEnabled = XmlUtils.getBooleanContent(el);
			else if (XAXIS_LABELS.equals(elName))
				xShowLabels = XmlUtils.getBooleanContent(el);
			else if (XAXIS_LABEL_ANGLE.equals(elName))
				xLabelAngle = XmlUtils.getDoubleContent(el);
			else if (XAXIS_LINE_STYLE.equals(elName))
				axisStyle = parseTypeFromString(XmlUtils.getStringContent(el), lineStyleMap, LineStyle.Solid);
			else if (XAXIS_LINE_WIDTH.equals(elName))
				axisWidth = XmlUtils.getIntContent(el);
			else if (XAXIS_LINE_COLOR.equals(elName))
				axisColor = XmlUtils.getColorContent(el);
			else if (XAXIS_SHOW_MAJOR_GRID.equals(elName))
				xShowMajorGrid = XmlUtils.getBooleanContent(el);
			else if (XAXIS_MAJOR_GRID_LINE_COLOR.equals(elName))
				xMajorGridLineColor = XmlUtils.getColorContent(el);
			else if (XAXIS_MAJOR_GRID_LINE_TYPE.equals(elName))
				xMajorGridLineStyle = parseTypeFromString(XmlUtils.getStringContent(el), lineStyleMap, DEFAULT_LINE_STYLE);
			else if (XAXIS_MAJOR_GRID_LINE_WIDTH.equals(elName))
				xMajorGridLineThickness = XmlUtils.getIntContent(el);
			else if (XAXIS_SHOW_MINOR_GRID.equals(elName))
				xShowMinorGrid = XmlUtils.getBooleanContent(el);
			else if (XAXIS_MINOR_GRID_LINE_COLOR.equals(elName))
				xMinorGridLineColor = XmlUtils.getColorContent(el);
			else if (XAXIS_MINOR_GRID_LINE_TYPE.equals(elName))
				xMinorGridLineStyle = parseTypeFromString(XmlUtils.getStringContent(el), lineStyleMap, DEFAULT_LINE_STYLE);
			else if (XAXIS_MINOR_GRID_LINE_WIDTH.equals(elName))
				xMinorGridLineThickness = XmlUtils.getIntContent(el);
			else if (XAXIS_TICKMARK_LABELS.equals(elName))
				xShowTickmarkLabels = XmlUtils.getBooleanContent(el);
			else if (XAXIS_SHOW_MAJOR_TICKMARKS.equals(elName))
				xShowMajorTickmarks = XmlUtils.getBooleanContent(el);
			else if (XAXIS_MAJOR_TICKMARK_WIDTH.equals(elName))
				xMajorTickmarkWidth = XmlUtils.getIntContent(el);
			else if (XAXIS_MAJOR_TICKMARK_COLOR.equals(elName))
				xMajorTickmarkColor = XmlUtils.getColorContent(el);
			else if (XAXIS_MINOR_TICKMARK_WIDTH.equals(elName))
				xMinorTickmarkWidth = XmlUtils.getIntContent(el);
			else if (XAXIS_MINOR_TICKMARK_COLOR.equals(elName))
				xMinorTickmarkColor = XmlUtils.getColorContent(el);
			else if (XAXIS_SHOW_MINOR_TICKMARKS.equals(elName))
				xShowMinorTickmarks = XmlUtils.getBooleanContent(el);
			else if (XAXIS_GROUP_LABELS.equals(elName))
				xShowGroupLabels = XmlUtils.getBooleanContent(el);
			else if (XAXIS_POSITION.equals(elName))
				xPosition = XmlUtils.getStringContent(el);
			else if (XAXIS_DEFAULT_LABEL.equals(elName))
				xUseDefaultLabel = XmlUtils.getBooleanContent(el);
			else if (XAXIS_TITLE.equals(elName))
				xLabel = XmlUtils.getStringContent(el);
			else if (XAXIS_MAJOR_INTERVAL.equals(elName))
				xMajorInterval = XmlUtils.getDoubleContent(el);
			else if (XAXIS_MINOR_INTERVAL.equals(elName))
				xMinorInterval = XmlUtils.getDoubleContent(el);
			else if (XAXIS_SHOW_INTERLACE.equals(elName))
				xShowInterlace = XmlUtils.getBooleanContent(el);
			else if (XAXIS_SCALE_TYPE.equals(elName))
				xScaleType = XmlUtils.getStringContent(el);
			else if (XAXIS_MAX_CHAR.equals(elName))
				xMaxChar = XmlUtils.getStringContent(el);	
			else if (XAXIS_ZERO_LINE_COLOR.equals(elName))
				xAxisZeroLineColor = XmlUtils.getColorContent(el);	
			else if (XAXIS_ZERO_LINE_WIDTH.equals(elName))
				xAxisZeroLineWidth = XmlUtils.getStringContent(el);	
			else if (XAXIS_LABEL_FORMAT.equals(elName))
				xAxisLabelFormat = XmlUtils.getStringContent(el);	
			else if (XAXIS_MAXIMUM.equals(elName))
				xMaximum = XmlUtils.getStringContent(el);
			else if (XAXIS_MINIMUM.equals(elName))
				xMinimum = XmlUtils.getStringContent(el);			
		}
		if (titleFontFound == false) {
			titleFont = font.deriveFont(Font.BOLD);
		}
	}

	@Override
	public void parseElement(XMLStreamReader parser) throws Exception {
		boolean titleFontFound = false;
		for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter.hasNext(); ) {
			String elName = parser.getLocalName();

			if (FONT.equals(elName))
				font = XmlUtils.getFontContent(parser);
			else if (TITLE_FONT.equals(elName)) {
				titleFontFound = true;
				titleFont = XmlUtils.getFontContent(parser);
			}
			else if (LABEL_COLOR.equals(elName))
				labelColor = XmlUtils.getColorContent(parser);
			else if (TITLE_COLOR.equals(elName))
				titleColor = XmlUtils.getColorContent(parser);
			else if (XAXIS_ENABLED.equals(elName))
				xEnabled = XmlUtils.getBooleanContent(parser);
			else if (XAXIS_LABELS.equals(elName))
				xShowLabels = XmlUtils.getBooleanContent(parser);
			else if (XAXIS_LABEL_ANGLE.equals(elName))
				xLabelAngle = XmlUtils.getDoubleContent(parser);
			else if (XAXIS_LINE_STYLE.equals(elName))
				axisStyle = parseTypeFromString(XmlUtils.getStringContent(parser), lineStyleMap, LineStyle.Solid);
			else if (XAXIS_LINE_WIDTH.equals(elName))
				axisWidth = XmlUtils.getIntContent(parser);
			else if (XAXIS_LINE_COLOR.equals(elName))
				axisColor = XmlUtils.getColorContent(parser);
			else if (XAXIS_SHOW_MAJOR_GRID.equals(elName))
				xShowMajorGrid = XmlUtils.getBooleanContent(parser);
			else if (XAXIS_MAJOR_GRID_LINE_COLOR.equals(elName))
				xMajorGridLineColor = XmlUtils.getColorContent(parser);
			else if (XAXIS_MAJOR_GRID_LINE_TYPE.equals(elName))
				xMajorGridLineStyle = parseTypeFromString(XmlUtils.getStringContent(parser), lineStyleMap, DEFAULT_LINE_STYLE);
			else if (XAXIS_MAJOR_GRID_LINE_WIDTH.equals(elName))
				xMajorGridLineThickness = XmlUtils.getIntContent(parser);
			else if (XAXIS_SHOW_MINOR_GRID.equals(elName))
				xShowMinorGrid = XmlUtils.getBooleanContent(parser);
			else if (XAXIS_MINOR_GRID_LINE_COLOR.equals(elName))
				xMinorGridLineColor = XmlUtils.getColorContent(parser);
			else if (XAXIS_MINOR_GRID_LINE_TYPE.equals(elName))
				xMinorGridLineStyle = parseTypeFromString(XmlUtils.getStringContent(parser), lineStyleMap, DEFAULT_LINE_STYLE);
			else if (XAXIS_MINOR_GRID_LINE_WIDTH.equals(elName))
				xMinorGridLineThickness = XmlUtils.getIntContent(parser);
			else if (XAXIS_TICKMARK_LABELS.equals(elName))
				xShowTickmarkLabels = XmlUtils.getBooleanContent(parser);
			else if (XAXIS_SHOW_MAJOR_TICKMARKS.equals(elName))
				xShowMajorTickmarks = XmlUtils.getBooleanContent(parser);
			else if (XAXIS_MAJOR_TICKMARK_WIDTH.equals(elName))
				xMajorTickmarkWidth = XmlUtils.getIntContent(parser);
			else if (XAXIS_MAJOR_TICKMARK_COLOR.equals(elName))
				xMajorTickmarkColor = XmlUtils.getColorContent(parser);
			else if (XAXIS_MINOR_TICKMARK_WIDTH.equals(elName))
				xMinorTickmarkWidth = XmlUtils.getIntContent(parser);
			else if (XAXIS_MINOR_TICKMARK_COLOR.equals(elName))
				xMinorTickmarkColor = XmlUtils.getColorContent(parser);
			else if (XAXIS_SHOW_MINOR_TICKMARKS.equals(elName))
				xShowMinorTickmarks = XmlUtils.getBooleanContent(parser);
			else if (XAXIS_GROUP_LABELS.equals(elName))
				xShowGroupLabels = XmlUtils.getBooleanContent(parser);
			else if (XAXIS_POSITION.equals(elName))
				xPosition = XmlUtils.getStringContent(parser);
			else if (XAXIS_DEFAULT_LABEL.equals(elName))
				xUseDefaultLabel = XmlUtils.getBooleanContent(parser);
			else if (XAXIS_TITLE.equals(elName))
				xLabel = XmlUtils.getStringContent(parser);
			else if (XAXIS_MAJOR_INTERVAL.equals(elName))
				xMajorInterval = XmlUtils.getDoubleContent(parser);
			else if (XAXIS_MINOR_INTERVAL.equals(elName))
				xMinorInterval = XmlUtils.getDoubleContent(parser);
			else if (XAXIS_SHOW_INTERLACE.equals(elName))
				xShowInterlace = XmlUtils.getBooleanContent(parser);
			else if (XAXIS_SCALE_TYPE.equals(elName))
				xScaleType = XmlUtils.getStringContent(parser);		
			else if (XAXIS_MAX_CHAR.equals(elName))
				xMaxChar = XmlUtils.getStringContent(parser);	
			else if (XAXIS_ZERO_LINE_COLOR.equals(elName))
				xAxisZeroLineColor = XmlUtils.getColorContent(parser);
			else if (XAXIS_ZERO_LINE_WIDTH.equals(elName))
				xAxisZeroLineWidth = XmlUtils.getStringContent(parser);	
			else if (XAXIS_LABEL_FORMAT.equals(elName))
				xAxisLabelFormat = XmlUtils.getStringContent(parser);
			else if (XAXIS_MAXIMUM.equals(elName))
				xMaximum = XmlUtils.getStringContent(parser);
			else if (XAXIS_MINIMUM.equals(elName))
				xMinimum = XmlUtils.getStringContent(parser);			
		}
		if (titleFontFound == false) {
			titleFont = font.deriveFont(Font.BOLD);;
		}
	}
	
	public Element getXML(Element parent, AnyChartNode anyChartNode, double scaleFactor) {
		Element chartSettings = XmlUtils.findOrCreateChild(parent, "chart_settings");
		Element axes = XmlUtils.findOrCreateChild(chartSettings, "axes");
		
		Element xAxis = axes.getChild("x_axis");
		if (xAxis == null) {
			xAxis = new Element("x_axis");
			axes.addContent(xAxis);
			setupXAxis(xAxis, anyChartNode, scaleFactor);
		}
		
		return parent;
	}
	
	private void setupXAxis(Element xAxis, AnyChartNode anyChartNode, double scaleFactor) {
		Element lineEl = XmlUtils.findOrCreateChild(xAxis, "line");
		AnyChartNode.addColorAttribute(lineEl, axisColor);
		lineEl.setAttribute("thickness", String.valueOf(axisWidth));
		if (axisStyle == LineStyle.Dashed) {
			lineEl.setAttribute("dash", "True");
			lineEl.setAttribute("dash_length", "5");
		}
		if (xAxisZeroLineColor != null)
		{
			Element zeroLineEl = XmlUtils.findOrCreateChild(xAxis, "zero_line");
			AnyChartNode.addColorAttribute(zeroLineEl, xAxisZeroLineColor);
		}	
		if (xAxisZeroLineWidth != null)
		{
			Element zeroLineEl = XmlUtils.findOrCreateChild(xAxis, "zero_line");
			zeroLineEl.setAttribute("thickness", xAxisZeroLineWidth);
		}
		xAxis.setAttribute("enabled", xEnabled ? "True" : "False");
		xAxis.setAttribute("position", xPosition);
		String labelValue = xUseDefaultLabel ? anyChartNode.getDefaultXAxisLabel() : xLabel;
		Element title = XmlUtils.findOrCreateChild(xAxis, "title");
		title.setAttribute("enabled", xShowLabels ? "True" : "False");
//		Font titleFont = font.deriveFont(Font.BOLD);
		Element f = getFontElement(titleFont, scaleFactor);
		AnyChartNode.addColorAttribute(f, titleColor);
		title.addContent(f);
		Element labelsEl = XmlUtils.findOrCreateChild(xAxis, "labels");
		labelsEl.setAttribute("enabled", xShowTickmarkLabels ? "True" : "False");
		if (anyChartNode.getChartType() == ChartType.Radar)
		{
			RadarSeries rs = anyChartNode.getDataPlotSettings().getRadarSeries();
			if (rs.getCircularLabelsStyle() != null)
			{
				labelsEl.setAttribute("circular_labels_style", rs.getCircularLabelsStyle());
			}
		}
		labelsEl.setAttribute("rotation", String.valueOf(xLabelAngle));
		
		String formatText = getXAxisFormat(anyChartNode);
		
		anyChartNode.addFormat(labelsEl, formatText);

		f = getFontElement(font, scaleFactor);
		AnyChartNode.addColorAttribute(f, labelColor);
		labelsEl.addContent(f);
		Element text = XmlUtils.findOrCreateChild(title, "text");
		text.setText(labelValue == null ? " " : labelValue);
		Element scaleEl = XmlUtils.findOrCreateChild(xAxis, "scale");
		if (xMajorInterval > 0.0) {
			scaleEl.setAttribute("major_interval", String.valueOf(xMajorInterval));
			if (xMinorInterval > 0.0)
				scaleEl.setAttribute("minor_interval", String.valueOf(xMinorInterval));
		}
		// only scale XAxis if a numeric axis (scatter, bubble)
		if (anyChartNode.getChartType() == ChartType.Scatter || anyChartNode.getChartType() == ChartType.Bubble)
		{
			if (xMinimum != null)
				scaleEl.setAttribute("minimum", xMinimum);
			if (xMaximum != null)
				scaleEl.setAttribute("maximum", xMaximum);
		}
		if (xShowMajorGrid == false) 
			XmlUtils.findOrCreateChild(xAxis, "major_grid").setAttribute("enabled", "False");
		else {
			Element g = XmlUtils.findOrCreateChild(xAxis, "major_grid");
			g.setAttribute("interlaced", isShowInterlace() ? "True" : "False");
			Element dashedEl = XmlUtils.findOrCreateChild(g, "line");
			dashedEl.setAttribute("enabled", "True");
			dashedEl.setAttribute("thickness", String.valueOf(xMajorGridLineThickness));
			AnyChartNode.addColorAttribute(dashedEl, xMajorGridLineColor);
			if (xMajorGridLineStyle == LineStyle.Dashed) {
				dashedEl.setAttribute("dashed", "True");
				dashedEl.setAttribute("dash_length", "5");
			}
		}
		if (xShowMinorGrid == false) 
			XmlUtils.findOrCreateChild(xAxis, "minor_grid").setAttribute("enabled", "False");
		else {
			Element g = XmlUtils.findOrCreateChild(xAxis, "minor_grid");
			g.setAttribute("interlaced", isShowInterlace() ? "True" : "False");
			Element dashedEl = XmlUtils.findOrCreateChild(g, "line");
			dashedEl.setAttribute("enabled", "True");
			dashedEl.setAttribute("thickness", String.valueOf(xMinorGridLineThickness));
			AnyChartNode.addColorAttribute(dashedEl, xMinorGridLineColor);
			if (xMinorGridLineStyle == LineStyle.Dashed) {
				dashedEl.setAttribute("dashed", "True");
				dashedEl.setAttribute("dash_length", "5");
			}
		}
		Element tickmarkEl = XmlUtils.findOrCreateChild(xAxis, "major_tickmark");
		if (xShowMajorTickmarks == false) {
			tickmarkEl.setAttribute("enabled", "False");
		}
		else {
			tickmarkEl.setAttribute("thickness", String.valueOf(xMajorTickmarkWidth));
			AnyChartNode.addColorAttribute(tickmarkEl, xMajorTickmarkColor);
		}
		tickmarkEl = XmlUtils.findOrCreateChild(xAxis, "minor_tickmark"); 
		if (xShowMinorTickmarks == false) {
			tickmarkEl.setAttribute("enabled", "False");
		}
		else {
			tickmarkEl.setAttribute("thickness", String.valueOf(xMinorTickmarkWidth));
			AnyChartNode.addColorAttribute(tickmarkEl, xMinorTickmarkColor);
		}
		if (xScaleType != null && !xScaleType.equals("Linear")) // forcing linear screws up date axes and 'Date or Time' is not yet supported
			scaleEl.setAttribute("type", xScaleType);
	}
	
	public String getXAxisFormat(AnyChartNode anyChartNode)
	{
		return getXAxisFormat(anyChartNode, "{%Value}");
	}
	
	public String getXAxisFormat(AnyChartNode anyChartNode, String baseText)
	{
		String formatText;

		if (xAxisLabelFormat != null)
		{
			formatText = xAxisLabelFormat;
		}
		else
		{
			formatText = baseText;
			StringBuilder options = new StringBuilder();
			StringBuilder prefix = new StringBuilder();
			StringBuilder postfix = new StringBuilder();

			if (anyChartNode.getChartType() == ChartType.Bubble || anyChartNode.getChartType() == ChartType.Scatter) {
				String formatStr = anyChartNode.getFormat(anyChartNode.getSeries());
				if (formatStr != null)
					addOption(options,getAnychartNumberFormat(formatStr, prefix, postfix)); // these are series, so number format them
			}
			else if (xMaxChar != null && xMaxChar.length() > 0)
			{
				addOption(options,getAnychartNumberFormat(null, prefix, postfix)); // can not use enabled:False and maxChar together, so try to set formatting...
				addOption(options, "maxChar:" + xMaxChar);
			}
			else
			{
				addOption(options, "enabled:False"); // so number-based domain labels don't get formatted (mainly for year)
			}

			if (options.length() > 0)
				formatText = formatText + '{' + options.toString() + '}';
			formatText = prefix.toString() + formatText + postfix.toString();
		}
		return formatText;
	}
	
	public void addOption(StringBuilder options, String newOption)
	{
		if (options.length() > 0)
			options.append(',');
		options.append(newOption);
	}
	
	public static String getAnychartNumberFormat(String formatStr, StringBuilder prefix, StringBuilder postfix) {
		if (formatStr == null || !Util.hasNonWhiteSpaceCharacters(formatStr))
			formatStr = "#";
		if (formatStr.toUpperCase().startsWith(ToTime.TO_TIME_FORMAT_PREFIX)) {
			return "scale:(1)(1000)(60)(60)(24)|( ms)( s)( m)( h)( d)";
		}
		UserBean ub = com.successmetricsinc.UserBean.getParentThreadUserBean();
		DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance(ub != null ? ub.getLocale() : Locale.getDefault());
		DecimalFormat df = new DecimalFormat(formatStr, symbols);

		if (df.getPositivePrefix().length() > 0)
			prefix.append(df.getPositivePrefix());

		StringBuilder suffix = new StringBuilder();
		if (df.getNegativePrefix().length() > 0)
		{
			if (suffix.length() > 0)
				suffix.append(',');
			suffix.append("useNegativeSign:" + (df.getNegativePrefix().equals("-") ? "true" : "false"));
		}
		if (df.getMinimumIntegerDigits() > 0)
		{
			if (suffix.length() > 0)
				suffix.append(',');
			suffix.append("leadingZeros:" + df.getMinimumIntegerDigits());
		}
		/*
		 * this does bad things - 9609
		if (df.getMinimumFractionDigits() > 0)
		{
			if (suffix.length() > 0)
				suffix.append(',');
			suffix.append("trailingZeros:" + df.getMinimumFractionDigits());
		}
		*/
		if (suffix.length() > 0)
			suffix.append(',');
		suffix.append("numDecimals:" + df.getMaximumFractionDigits());
		if (suffix.length() > 0)
			suffix.append(',');
		suffix.append("decimalSeparator:" + FlashRenderer.getEscapedSymbol(df.getDecimalFormatSymbols().getDecimalSeparator()));
		boolean isPercent = false;
		if (df.getPositiveSuffix().equals("%"))
		{
			if (suffix.length() > 0)
				suffix.append(',');
			suffix.append("scale:(0.01)|(%)");
			isPercent = true;
		}
		if (df.isGroupingUsed() && df.getGroupingSize() == 3)
		{
			if (suffix.length() > 0)
				suffix.append(',');
			suffix.append("thousandsSeparator:" + FlashRenderer.getEscapedSymbol(df.getDecimalFormatSymbols().getGroupingSeparator()));
		}
		else if (!df.isGroupingUsed()) // remove thousandsSeperator
		{
			if (suffix.length() > 0)
				suffix.append(',');
			suffix.append("thousandsSeparator:");
		}
		if (!isPercent && df.getPositiveSuffix().length() > 0)
			postfix.append(df.getPositiveSuffix());

		return suffix.toString();
	}
	
	public boolean showDomainLabels() {
		return xShowLabels;
	}
	
	public void setShowDomainLabels(boolean show){
		xShowLabels = show;
	}
	
	public Font getLabelFont() {
		return font;
	}
	
	public void setLabelFont(String str)
	{
		font = XmlUtils.getFontContent(str);
	}
	
	public void setLabelFontSize(float newSize){
		if (font == null)
		{
			setLabelFont("Arial-PLAIN-8");
		}
		font = font.deriveFont(newSize);
	}
	
	public Font getTitleFont() {
		return titleFont;
	}
	
	public void setTitleFont(String str)
	{
		titleFont = XmlUtils.getFontContent(str);
	}
	
	public void setTitleFontSize(float newSize)
	{
		if (titleFont == null)
		{
			setTitleFont("Arial-PLAIN-8");
		}
		titleFont = titleFont.deriveFont(newSize);
	}
	
	public Color getLabelColor() {
		return labelColor;
	}
	
	public Color getTitleColor() {
		return titleColor;
	}
	
	public boolean isShowGroupLabels() {
		return xShowGroupLabels;
	}

	public boolean isShowInterlace() {
		return xShowInterlace;
	}
	
	public static String getLineStyleString(LineStyle style) {
		return lineStyleMap.get(style);
	}
	
	public String getMaxChar()
	{
		return xMaxChar;
	}
	
	public void setMaxChar(String maxChar)
	{
		xMaxChar = maxChar;
	}
	
	public String getXAxisLabelFormat()
	{
		return xAxisLabelFormat;
	}

	public boolean isxEnabled() {
		return xEnabled;
	}

	public void setxEnabled(boolean xEnabled) {
		this.xEnabled = xEnabled;
	}

	public String getxMaximum() {
		return xMaximum;
	}

	public void setxMaximum(String xMaximum) {
		this.xMaximum = xMaximum;
	}

	public String getxMinimum() {
		return xMinimum;
	}

	public void setxMinimum(String xMinimum) {
		this.xMinimum = xMinimum;
	}

	public boolean isxUseDefaultLabel() {
		return xUseDefaultLabel;
	}

	public void setxUseDefaultLabel(boolean xUseDefaultLabel) {
		this.xUseDefaultLabel = xUseDefaultLabel;
	}

	public String getxLabel() {
		return xLabel;
	}

	public void setxLabel(String xLabel) {
		this.xLabel = xLabel;
	}

	public boolean isxShowLabels() {
		return xShowLabels;
	}

	public void setxShowLabels(boolean xShowLabels) {
		this.xShowLabels = xShowLabels;
	}

	public String getxAxisLabelFormat() {
		return xAxisLabelFormat;
	}

	public void setxAxisLabelFormat(String xAxisLabelFormat) {
		this.xAxisLabelFormat = xAxisLabelFormat;
	}

	public boolean isxShowTickmarkLabels() {
		return xShowTickmarkLabels;
	}

	public void setxShowTickmarkLabels(boolean xShowTickmarkLabels) {
		this.xShowTickmarkLabels = xShowTickmarkLabels;
	}

	public boolean isxShowGroupLabels() {
		return xShowGroupLabels;
	}

	public void setxShowGroupLabels(boolean xShowGroupLabels) {
		this.xShowGroupLabels = xShowGroupLabels;
	}

	public void applyStyle(XAxis style) {
		if (style == null)
			return;
		font = style.font;
		labelColor = style.labelColor;
		titleFont = style.titleFont;
		titleColor = style.titleColor;
		xEnabled = style.xEnabled;
		xShowLabels = style.xShowLabels;
		xLabelAngle = style.xLabelAngle;
		axisStyle = style.axisStyle;
		axisWidth = style.axisWidth;
		axisColor = style.axisColor;
		xShowMajorGrid = style.xShowMajorGrid;
		xMajorGridLineStyle = style.xMajorGridLineStyle;
		xMajorGridLineColor = style.xMajorGridLineColor;
		xMajorGridLineThickness = style.xMajorGridLineThickness;
		xShowMinorGrid = style.xShowMinorGrid;
		xMinorGridLineStyle = style.xMinorGridLineStyle;
		xMinorGridLineColor = style.xMinorGridLineColor;
		xMinorGridLineThickness = style.xMinorGridLineThickness;
		xShowTickmarkLabels = style.xShowTickmarkLabels;
		xShowMajorTickmarks = style.xShowMajorTickmarks;
		xMajorTickmarkWidth = style.xMajorTickmarkWidth;
		xMajorTickmarkColor = style.xMajorTickmarkColor;
		xShowMinorTickmarks = style.xShowMinorTickmarks;
		xMinorTickmarkWidth = style.xMinorTickmarkWidth;
		xMinorTickmarkColor = style.xMinorTickmarkColor;
		xShowGroupLabels = style.xShowGroupLabels;
		xPosition = style.xPosition;
		xUseDefaultLabel = style.xUseDefaultLabel;
		xLabel = style.xLabel;
		xMajorInterval = style.xMajorInterval;
		xMinorInterval = style.xMinorInterval;
		xShowInterlace = style.xShowInterlace;
		xScaleType = style.xScaleType;
		xMaxChar = style.xMaxChar;
		xAxisZeroLineColor = style.xAxisZeroLineColor;
		xAxisZeroLineWidth = style.xAxisZeroLineWidth;
		xAxisLabelFormat = style.xAxisLabelFormat;
	}
}
