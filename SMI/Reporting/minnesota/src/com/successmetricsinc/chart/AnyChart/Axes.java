/**
 * 
 */
package com.successmetricsinc.chart.AnyChart;

import java.awt.Color;
import java.awt.Font;
import java.util.Iterator;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.jdom.Attribute;
import org.jdom.Element;

import com.successmetricsinc.adhoc.AdhocReport;
import com.successmetricsinc.chart.ChartOptions;
import com.successmetricsinc.chart.FlashRenderer;
import com.successmetricsinc.chart.AnyChart.AnyChartNode.ChartType;
import com.successmetricsinc.chart.AnyChart.AnyChartNode.StackedType;
import com.successmetricsinc.chart.AnyChart.XAxis.LineStyle;
import com.successmetricsinc.util.BirstXMLSerializable;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class Axes extends BirstXMLSerializable {
	private static final String TITLE_COLOR = "TitleColor";
	private static final String LABEL_COLOR = "LabelColor";
	private static final String TITLE_FONT = "TitleFont";
	private static final String FONT = "font";
	private static final String YAXIS_MINOR_TICKMARK_COLOR = "YAxisMinorTickmarkColor";
	private static final String YAXIS_MINOR_TICKMARK_WIDTH = "YAxisMinorTickmarkWidth";
	private static final String YAXIS_MAJOR_TICKMARK_COLOR = "YAxisMajorTickmarkColor";
	private static final String YAXIS_MAJOR_TICKMARK_WIDTH = "YAxisMajorTickmarkWidth";
	private static final String YAXIS_MINOR_GRID_LINE_COLOR = "YAxisMinorGridLineColor";
	private static final String YAXIS_MINOR_GRID_LINE_WIDTH = "YAxisMinorGridLineWidth";
	private static final String YAXIS_MINOR_GRID_LINE_STYLE = "YAxisMinorGridLineStyle";
	private static final String YAXIS_MAJOR_GRID_LINE_COLOR = "YAxisMajorGridLineColor";
	private static final String YAXIS_MAJOR_GRID_LINE_WIDTH = "YAxisMajorGridLineWidth";
	private static final String YAXIS_MAJOR_GRID_LINE_STYLE = "YAxisMajorGridLineStyle";
	private static final String YAXIS_LINE_COLOR = "YAxisLineColor";
	private static final String YAXIS_LINE_WIDTH = "YAxisLineWidth";
	private static final String YAXIS_LINE_STYLE = "YAxisLineStyle";
	private static final long serialVersionUID = 1L;
	private static final String OPPOSITE = "Opposite";
	private static final String NORMAL = "Normal";
	private static final String YAXIS_SHARE = "YAxisShare";
	private static final String YAXIS_TITLE = "YAxisTitle";
	private static final String YAXIS_DEFAULT_LABEL = "YAxisDefaultTitle";
	private static final String YAXIS_POSITION = "YAxisPosition";
	private static final String YAXIS_GROUP_LABELS = "YAxisGroupLabels";
	private static final String YAXIS_SHOW_MINOR_TICKMARKS = "YAxisShowMinorTickmarks";
	private static final String YAXIS_SHOW_MAJOR_TICKMARKS = "YAxisShowMajorTickmarks";
	private static final String YAXIS_TICKMARK_LABELS = "YAxisTickmarkLabels";
	private static final String YAXIS_SHOW_MINOR_GRID = "YAxisShowMinorGrid";
	private static final String YAXIS_SHOW_MAJOR_GRID = "YAxisShowMajorGrid";
	private static final String YAXIS_LABEL_ANGLE = "YAxisLabelAngle";
	private static final String YAXIS_LABELS = "YAxisLabels";
	private static final String YAXIS_ENABLED = "YAxisEnabled";
	private static final String YAXIS_MAJOR_INTERVAL = "YAxisMajorInterval";
	private static final String YAXIS_MINOR_INTERVAL = "YAxisMinorInterval";
	private static final String YAXIS_MINIMUM = "YAxisMinimum";
	private static final String YAXIS_MAXIMUM = "YAxisMaximum";
	private static final String YAXIS_SCALE_TYPE = "YAxisScaleType";
	private static final String YAXIS_ZERO_LINE_COLOR = "YAxisZeroLineColor";
	private static final String YAXIS_ZERO_LINE_WIDTH = "YAxisZeroLineWidth";
	private static final String YAXIS_LABEL_FORMAT = "YAxisLabelFormat";

	private Font font;
	private Color labelColor = Color.BLACK;
	private Font titleFont;
	private Color titleColor = Color.BLACK;

	private boolean yShare;
	private boolean yEnabled;
	private XAxis.LineStyle axisStyle = LineStyle.Solid;
	private int axisWidth = 1;
	private Color axisColor = XAxis.DEFAULT_LINE_COLOR;
	private boolean yShowLabels;
	private double yLabelAngle = 0.0;
	private boolean yShowMajorGrid;
	private LineStyle yMajorGridLineStyle = XAxis.DEFAULT_LINE_STYLE;
	private Color yMajorGridLineColor = XAxis.DEFAULT_LINE_COLOR;
	private int yMajorGridLineThickness = 1;
	private boolean yShowMinorGrid;
	private LineStyle yMinorGridLineStyle = XAxis.DEFAULT_LINE_STYLE;
	private Color yMinorGridLineColor = XAxis.DEFAULT_LINE_COLOR;
	private int yMinorGridLineThickness = 1;
	private boolean yShowTickmarkLabels;
	private boolean yShowMajorTickmarks;
	private int yMajorTickmarkWidth = 1;
	private Color yMajorTickmarkColor = XAxis.DEFAULT_LINE_COLOR;
	private boolean yShowMinorTickmarks;
	private int yMinorTickmarkWidth = 1;
	private Color yMinorTickmarkColor = XAxis.DEFAULT_LINE_COLOR;
	private boolean yShowGroupLabels;
	private String yPosition = NORMAL;
	private boolean yUseDefaultLabel;
	private String yLabel;
	private double yMajorInterval;
	private double yMinorInterval;
	private String yMaximum;
	private String yMinimum;
	private String yScaleType;
	private Color yAxisZeroLineColor = XAxis.DEFAULT_ZERO_LINE_COLOR;
	private String yAxisZeroLineWidth;
	private String yAxisLabelFormat;
	
	public Axes() {
		yScaleType = XAxis.DEFAULT_SCALE_TYPE;
	}
	
	public Axes(ChartOptions co, AdhocReport report, OMElement defaults) {
		parseElement(defaults);
		yShare = co.shareAxis;
		yEnabled = true;
		yShowLabels = co.rangeLabels;
		yShowMajorGrid = true;
		yShowMinorGrid = co.showMinorGrid;
		yShowMajorTickmarks = co.showMajorTickmarks;
		yShowTickmarkLabels = co.rangeTickMarkLabels;
		yShowMinorTickmarks = co.showMinorTickmarks;
		yShowGroupLabels = co.domainGroupLabels;
//		yPosition =
		yUseDefaultLabel = (co.rangeLabel == null || co.rangeLabel.trim().length() == 0) && co.rangeLabels;
		yLabel = co.rangeLabel;
		yMajorInterval = co.getMajorInterval();
		yMinorInterval = co.getMinorInterval();
		yMinimum = (co.valueMin == null ? null : String.valueOf(co.valueMin));
		yMaximum = (co.valueMax == null ? null : String.valueOf(co.valueMax));
		if (yMinimum != null && yMinimum.equals(yMaximum))
		{
			yMaximum = null;
			yMinimum = null;
		}
		yScaleType = XAxis.DEFAULT_SCALE_TYPE;
	}

	public Axes(OMElement defaults) {
		parseElement(defaults);
	}

	/* (non-Javadoc)
	 * @see com.successmetricsinc.util.BirstXMLSerializable#addContent(org.apache.axiom.om.OMFactory, org.apache.axiom.om.OMElement, org.apache.axiom.om.OMNamespace)
	 */
	@Override
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, FONT, font, ns);
		XmlUtils.addContent(fac, parent, TITLE_FONT, titleFont, ns);
		XmlUtils.addContent(fac, parent, LABEL_COLOR, labelColor, ns);
		XmlUtils.addContent(fac, parent, TITLE_COLOR, titleColor, ns);		
		XmlUtils.addContent(fac, parent, YAXIS_SHARE, yShare, ns);
		XmlUtils.addContent(fac, parent, YAXIS_ENABLED, yEnabled, ns);
		XmlUtils.addContent(fac, parent, YAXIS_LINE_STYLE, XAxis.getLineStyleString(axisStyle), ns);
		XmlUtils.addContent(fac, parent, YAXIS_LINE_WIDTH, axisWidth, ns);
		XmlUtils.addContent(fac, parent, YAXIS_LINE_COLOR, axisColor, ns);
		XmlUtils.addContent(fac, parent, YAXIS_MAJOR_GRID_LINE_STYLE, XAxis.getLineStyleString(yMajorGridLineStyle), ns);
		XmlUtils.addContent(fac, parent, YAXIS_MAJOR_GRID_LINE_WIDTH, yMajorGridLineThickness, ns);
		XmlUtils.addContent(fac, parent, YAXIS_MAJOR_GRID_LINE_COLOR, yMajorGridLineColor, ns);
		XmlUtils.addContent(fac, parent, YAXIS_MINOR_GRID_LINE_STYLE, XAxis.getLineStyleString(yMinorGridLineStyle), ns);
		XmlUtils.addContent(fac, parent, YAXIS_MINOR_GRID_LINE_WIDTH, yMinorGridLineThickness, ns);
		XmlUtils.addContent(fac, parent, YAXIS_MINOR_GRID_LINE_COLOR, yMinorGridLineColor, ns);
		XmlUtils.addContent(fac, parent, YAXIS_MAJOR_TICKMARK_WIDTH, yMajorTickmarkWidth, ns);
		XmlUtils.addContent(fac, parent, YAXIS_MAJOR_TICKMARK_COLOR, yMajorTickmarkColor, ns);
		XmlUtils.addContent(fac, parent, YAXIS_MINOR_TICKMARK_WIDTH, yMinorTickmarkWidth, ns);
		XmlUtils.addContent(fac, parent, YAXIS_MINOR_TICKMARK_COLOR, yMinorTickmarkColor, ns);
		XmlUtils.addContent(fac, parent, YAXIS_LABELS, yShowLabels, ns);
		XmlUtils.addContent(fac, parent, YAXIS_LABEL_ANGLE, yLabelAngle, ns);
		XmlUtils.addContent(fac, parent, YAXIS_SHOW_MAJOR_GRID, yShowMajorGrid, ns);
		XmlUtils.addContent(fac, parent, YAXIS_SHOW_MINOR_GRID, yShowMinorGrid, ns);
		XmlUtils.addContent(fac, parent, YAXIS_TICKMARK_LABELS, yShowTickmarkLabels, ns);
		XmlUtils.addContent(fac, parent, YAXIS_SHOW_MAJOR_TICKMARKS, yShowMajorTickmarks, ns);
		XmlUtils.addContent(fac, parent, YAXIS_SHOW_MINOR_TICKMARKS, yShowMinorTickmarks, ns);
		XmlUtils.addContent(fac, parent, YAXIS_GROUP_LABELS, yShowGroupLabels, ns);
		XmlUtils.addContent(fac, parent, YAXIS_POSITION, yPosition, ns);
		XmlUtils.addContent(fac, parent, YAXIS_DEFAULT_LABEL, yUseDefaultLabel, ns);
		XmlUtils.addContent(fac, parent, YAXIS_TITLE, yLabel, ns);
		XmlUtils.addContent(fac, parent, YAXIS_MAJOR_INTERVAL, yMajorInterval, ns);
		XmlUtils.addContent(fac, parent, YAXIS_MINOR_INTERVAL, yMinorInterval, ns);
		XmlUtils.addContent(fac, parent, YAXIS_MAXIMUM, yMaximum, ns);
		XmlUtils.addContent(fac, parent, YAXIS_MINIMUM, yMinimum, ns);
		XmlUtils.addContent(fac, parent, YAXIS_SCALE_TYPE, yScaleType, ns);
		XmlUtils.addContent(fac, parent, YAXIS_ZERO_LINE_COLOR, yAxisZeroLineColor, ns);
		XmlUtils.addContent(fac, parent, YAXIS_ZERO_LINE_WIDTH, yAxisZeroLineWidth, ns);
		XmlUtils.addContent(fac, parent, YAXIS_LABEL_FORMAT, yAxisLabelFormat, ns);
	}

	/* (non-Javadoc)
	 * @see com.successmetricsinc.util.BirstXMLSerializable#createNew()
	 */
	@Override
	public BirstXMLSerializable createNew() {
		return new Axes();
	}

	/* (non-Javadoc)
	 * @see com.successmetricsinc.util.BirstXMLSerializable#parseElement(org.apache.axiom.om.OMElement)
	 */
	@Override
	public void parseElement(OMElement parent) {
		if (parent == null)
			return;
		for (@SuppressWarnings("unchecked")
		Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();

			if (YAXIS_ENABLED.equals(elName))
				yEnabled = XmlUtils.getBooleanContent(el);
			if (FONT.equals(elName))
				font = XmlUtils.getFontContent(el);
			else if (TITLE_FONT.equals(elName)) {
				titleFont = XmlUtils.getFontContent(el);
			}
			else if (LABEL_COLOR.equals(elName))
				labelColor = XmlUtils.getColorContent(el);
			else if (TITLE_COLOR.equals(elName))
				titleColor = XmlUtils.getColorContent(el);			
			else if (YAXIS_SHARE.equals(elName))
				yShare = XmlUtils.getBooleanContent(el);
			else if (YAXIS_LABELS.equals(elName))
				yShowLabels = XmlUtils.getBooleanContent(el);
			else if (YAXIS_LABEL_ANGLE.equals(elName)) {
				Double d = XmlUtils.getDoubleContent(el);;
				if (d != null)
					yLabelAngle = d.doubleValue();
			}
			else if (YAXIS_LINE_STYLE.equals(elName))
				axisStyle = parseTypeFromString(XmlUtils.getStringContent(el), XAxis.lineStyleMap, LineStyle.Solid);
			else if (YAXIS_LINE_WIDTH.equals(elName))
				axisWidth = XmlUtils.getIntContent(el);
			else if (YAXIS_LINE_COLOR.equals(elName))
				axisColor = XmlUtils.getColorContent(el);
			else if (YAXIS_MAJOR_GRID_LINE_STYLE.equals(elName))
				yMajorGridLineStyle = parseTypeFromString(XmlUtils.getStringContent(el), XAxis.lineStyleMap, XAxis.DEFAULT_LINE_STYLE);
			else if (YAXIS_MAJOR_GRID_LINE_WIDTH.equals(elName))
				yMajorGridLineThickness = XmlUtils.getIntContent(el);
			else if (YAXIS_MAJOR_GRID_LINE_COLOR.equals(elName))
				yMajorGridLineColor = XmlUtils.getColorContent(el);
			else if (YAXIS_MINOR_GRID_LINE_STYLE.equals(elName))
				yMinorGridLineStyle = parseTypeFromString(XmlUtils.getStringContent(el), XAxis.lineStyleMap, XAxis.DEFAULT_LINE_STYLE);
			else if (YAXIS_MINOR_GRID_LINE_WIDTH.equals(elName))
				yMinorGridLineThickness = XmlUtils.getIntContent(el);
			else if (YAXIS_MINOR_GRID_LINE_COLOR.equals(elName))
				yMinorGridLineColor = XmlUtils.getColorContent(el);
			else if (YAXIS_MAJOR_TICKMARK_WIDTH.equals(elName))
				yMajorTickmarkWidth = XmlUtils.getIntContent(el);
			else if (YAXIS_MAJOR_TICKMARK_COLOR.equals(elName))
				yMajorTickmarkColor = XmlUtils.getColorContent(el);
			else if (YAXIS_MINOR_TICKMARK_WIDTH.equals(elName))
				yMinorTickmarkWidth = XmlUtils.getIntContent(el);
			else if (YAXIS_MINOR_TICKMARK_COLOR.equals(elName))
				yMinorTickmarkColor = XmlUtils.getColorContent(el);
			else if (YAXIS_SHOW_MAJOR_GRID.equals(elName))
				yShowMajorGrid = XmlUtils.getBooleanContent(el);
			else if (YAXIS_SHOW_MINOR_GRID.equals(elName))
				yShowMinorGrid = XmlUtils.getBooleanContent(el);
			else if (YAXIS_TICKMARK_LABELS.equals(elName))
				yShowTickmarkLabels = XmlUtils.getBooleanContent(el);
			else if (YAXIS_SHOW_MAJOR_TICKMARKS.equals(elName))
				yShowMajorTickmarks = XmlUtils.getBooleanContent(el);
			else if (YAXIS_SHOW_MINOR_TICKMARKS.equals(elName))
				yShowMinorTickmarks = XmlUtils.getBooleanContent(el);
			else if (YAXIS_GROUP_LABELS.equals(elName))
				yShowGroupLabels = XmlUtils.getBooleanContent(el);
			else if (YAXIS_POSITION.equals(elName))
				yPosition = XmlUtils.getStringContent(el);
			else if (YAXIS_DEFAULT_LABEL.equals(elName))
				yUseDefaultLabel = XmlUtils.getBooleanContent(el);
			else if (YAXIS_TITLE.equals(elName))
				yLabel = XmlUtils.getStringContent(el);
			else if (YAXIS_MAJOR_INTERVAL.equals(elName)) {
				Double d = XmlUtils.getDoubleContent(el);
				if (d != null)
					yMajorInterval = d.doubleValue();
			}
			else if (YAXIS_MINOR_INTERVAL.equals(elName)) {
				Double d = XmlUtils.getDoubleContent(el);
				if (d != null)
					yMinorInterval = d.doubleValue();
			}
			else if (YAXIS_MAXIMUM.equals(elName))
				yMaximum = XmlUtils.getStringContent(el);
			else if (YAXIS_MINIMUM.equals(elName))
				yMinimum = XmlUtils.getStringContent(el);
			else if (YAXIS_SCALE_TYPE.equals(elName))
				yScaleType = XmlUtils.getStringContent(el);
			else if (YAXIS_ZERO_LINE_COLOR.equals(elName))
				yAxisZeroLineColor = XmlUtils.getColorContent(el);
			else if (YAXIS_ZERO_LINE_WIDTH.equals(elName))
				yAxisZeroLineWidth = XmlUtils.getStringContent(el);
			else if (YAXIS_LABEL_FORMAT.equals(elName))
				yAxisLabelFormat = XmlUtils.getStringContent(el);						
		}
		if (yMinimum != null && yMinimum.equals(yMaximum))
		{
			yMaximum = null;
			yMinimum = null;
		}
	}

	@Override
	public void parseElement(XMLStreamReader el) throws Exception {
		for (XMLStreamIterator iter = new XMLStreamIterator(el); iter.hasNext(); ) {
			String elName = el.getLocalName();

			if (YAXIS_ENABLED.equals(elName))
				yEnabled = XmlUtils.getBooleanContent(el);
			if (FONT.equals(elName))
				font = XmlUtils.getFontContent(el);
			else if (TITLE_FONT.equals(elName)) {
				titleFont = XmlUtils.getFontContent(el);
			}
			else if (LABEL_COLOR.equals(elName))
				labelColor = XmlUtils.getColorContent(el);
			else if (TITLE_COLOR.equals(elName))
				titleColor = XmlUtils.getColorContent(el);			
			else if (YAXIS_SHARE.equals(elName))
				yShare = XmlUtils.getBooleanContent(el);
			else if (YAXIS_LABELS.equals(elName))
				yShowLabels = XmlUtils.getBooleanContent(el);
			else if (YAXIS_LABEL_ANGLE.equals(elName)) {
				Double d = XmlUtils.getDoubleContent(el);
				if (d != null)
					yLabelAngle = d.doubleValue();
			}
			else if (YAXIS_LINE_STYLE.equals(elName))
				axisStyle = parseTypeFromString(XmlUtils.getStringContent(el), XAxis.lineStyleMap, LineStyle.Solid);
			else if (YAXIS_LINE_WIDTH.equals(elName))
				axisWidth = XmlUtils.getIntContent(el);
			else if (YAXIS_LINE_COLOR.equals(elName))
				axisColor = XmlUtils.getColorContent(el);
			else if (YAXIS_MAJOR_GRID_LINE_STYLE.equals(elName))
				yMajorGridLineStyle = parseTypeFromString(XmlUtils.getStringContent(el), XAxis.lineStyleMap, XAxis.DEFAULT_LINE_STYLE);
			else if (YAXIS_MAJOR_GRID_LINE_WIDTH.equals(elName))
				yMajorGridLineThickness = XmlUtils.getIntContent(el);
			else if (YAXIS_MAJOR_GRID_LINE_COLOR.equals(elName))
				yMajorGridLineColor = XmlUtils.getColorContent(el);
			else if (YAXIS_MINOR_GRID_LINE_STYLE.equals(elName))
				yMinorGridLineStyle = parseTypeFromString(XmlUtils.getStringContent(el), XAxis.lineStyleMap, XAxis.DEFAULT_LINE_STYLE);
			else if (YAXIS_MINOR_GRID_LINE_WIDTH.equals(elName))
				yMinorGridLineThickness = XmlUtils.getIntContent(el);
			else if (YAXIS_MINOR_GRID_LINE_COLOR.equals(elName))
				yMinorGridLineColor = XmlUtils.getColorContent(el);
			else if (YAXIS_MAJOR_TICKMARK_WIDTH.equals(elName))
				yMajorTickmarkWidth = XmlUtils.getIntContent(el);
			else if (YAXIS_MAJOR_TICKMARK_COLOR.equals(elName))
				yMajorTickmarkColor = XmlUtils.getColorContent(el);
			else if (YAXIS_MINOR_TICKMARK_WIDTH.equals(elName))
				yMinorTickmarkWidth = XmlUtils.getIntContent(el);
			else if (YAXIS_MINOR_TICKMARK_COLOR.equals(elName))
				yMinorTickmarkColor = XmlUtils.getColorContent(el);
			else if (YAXIS_SHOW_MAJOR_GRID.equals(elName))
				yShowMajorGrid = XmlUtils.getBooleanContent(el);
			else if (YAXIS_SHOW_MINOR_GRID.equals(elName))
				yShowMinorGrid = XmlUtils.getBooleanContent(el);
			else if (YAXIS_TICKMARK_LABELS.equals(elName))
				yShowTickmarkLabels = XmlUtils.getBooleanContent(el);
			else if (YAXIS_SHOW_MAJOR_TICKMARKS.equals(elName))
				yShowMajorTickmarks = XmlUtils.getBooleanContent(el);
			else if (YAXIS_SHOW_MINOR_TICKMARKS.equals(elName))
				yShowMinorTickmarks = XmlUtils.getBooleanContent(el);
			else if (YAXIS_GROUP_LABELS.equals(elName))
				yShowGroupLabels = XmlUtils.getBooleanContent(el);
			else if (YAXIS_POSITION.equals(elName))
				yPosition = XmlUtils.getStringContent(el);
			else if (YAXIS_DEFAULT_LABEL.equals(elName))
				yUseDefaultLabel = XmlUtils.getBooleanContent(el);
			else if (YAXIS_TITLE.equals(elName))
				yLabel = XmlUtils.getStringContent(el);
			else if (YAXIS_MAJOR_INTERVAL.equals(elName)) {
				Double d = XmlUtils.getDoubleContent(el);
				if (d != null)
					yMajorInterval = d.doubleValue();
			}
			else if (YAXIS_MINOR_INTERVAL.equals(elName)) {
				Double d = XmlUtils.getDoubleContent(el);
				if (d != null)
					yMinorInterval = d.doubleValue();
			}
			else if (YAXIS_MAXIMUM.equals(elName))
				yMaximum = XmlUtils.getStringContent(el);
			else if (YAXIS_MINIMUM.equals(elName))
				yMinimum = XmlUtils.getStringContent(el);
			else if (YAXIS_SCALE_TYPE.equals(elName))
				yScaleType = XmlUtils.getStringContent(el);	
			else if (YAXIS_ZERO_LINE_COLOR.equals(elName))
				yAxisZeroLineColor = XmlUtils.getColorContent(el);	
			else if (YAXIS_ZERO_LINE_WIDTH.equals(elName))
				yAxisZeroLineWidth = XmlUtils.getStringContent(el);
			else if (YAXIS_LABEL_FORMAT.equals(elName))
				yAxisLabelFormat = XmlUtils.getStringContent(el);						
		}
		if (yMinimum != null && yMinimum.equals(yMaximum))
		{
			yMaximum = null;
			yMinimum = null;
		}
	}
	
	public Element getXML(Element parent, AnyChartNode anyChartNode, double scaleFactor, boolean flipYAxis, Font font, Color labelColor, Font titleFont, Color titleColor, AnyChartNode.StackedType stacked, boolean interlaced, XAxis xAxis) {
		Element yAxis = getYAxis(parent, stacked, anyChartNode);
		if (yAxis != null)
			setupYAxis(yAxis, anyChartNode, scaleFactor, flipYAxis, font, labelColor, titleFont, titleColor, stacked, interlaced, xAxis);
		else {
			Element chartSettings = XmlUtils.findOrCreateChild(parent, "chart_settings");
			Element axes = XmlUtils.findOrCreateChild(chartSettings, "axes");
			yAxis = axes.getChild("y_axis");
		}
		return yAxis;
	}
	
	private Element getYAxis(Element parent, AnyChartNode.StackedType stacked, AnyChartNode anyChartNode) {
		Element chartSettings = XmlUtils.findOrCreateChild(parent, "chart_settings");
		Element axes = XmlUtils.findOrCreateChild(chartSettings, "axes");
		Element yAxis = axes.getChild("y_axis");
		if (yAxis == null) {
			yAxis = new Element("y_axis");
			axes.addContent(yAxis);
			anyChartNode.setName("y_axis");
			yAxis.setAttribute("enabled", yEnabled ? "True" : "False");
			return yAxis;
		}

		if (yShare) {
			// see if it has the same stacked value
			if (stacked == AnyChartNode.getStackedType(yAxis))
				return null;
		}
		Element extra = XmlUtils.findOrCreateChild(axes, "extra");
		Element extraY = new Element("y_axis");
		extraY.setAttribute("name", anyChartNode.getName());
		extra.addContent(extraY);
		if (yShare) {
			extraY.setAttribute("enabled", "false");
		}
		else {
			extraY.setAttribute("enabled", yEnabled ? "True" : "False");
			
		}
		return extraY;
	}
	
	public void setupNewGaugeAxis(Element yAxis)
	{
		Element labelsEL = XmlUtils.findOrCreateChild(yAxis, "labels");
		labelsEL.setAttribute("enabled", yShowTickmarkLabels ? "True" : "False");
		Element majorEL = XmlUtils.findOrCreateChild(yAxis, "major_tickmark");
		majorEL.setAttribute("enabled", yShowMajorTickmarks ? "True" : "False");
		Element minorEL = XmlUtils.findOrCreateChild(yAxis, "minor_tickmark");
		minorEL.setAttribute("enabled", yShowMinorTickmarks ? "True" : "False");
	}
	
	private String getStackedType()
	{
		return "Stacked";
	}
	
	private void setupYAxis(Element yAxis, AnyChartNode anyChartNode, double scaleFactor, boolean flipYAxis, Font xfont, Color xlabelColor, Font xtitleFont, Color xtitleColor, AnyChartNode.StackedType stacked, boolean interlaced, XAxis xAxis) {
		
		// set up axis title/label font/color
		if (titleFont == null)
			titleFont = xtitleFont;
		if (titleColor == null)
			titleColor = xtitleColor;
		if (font == null)
			font = xfont;
		if (labelColor == null)
			labelColor = xlabelColor;
	
		String pos = yPosition;
		if (flipYAxis) {
			if (NORMAL.equals(yPosition))
				pos = OPPOSITE;
			else
				pos = NORMAL;
		}
		Element lineEl = XmlUtils.findOrCreateChild(yAxis, "line");
		AnyChartNode.addColorAttribute(lineEl, axisColor);
		lineEl.setAttribute("thickness", String.valueOf(axisWidth));
		if (axisStyle == LineStyle.Dashed) {
			lineEl.setAttribute("dash", "True");
			lineEl.setAttribute("dash_length", "5");
		}
		if (yAxisZeroLineColor != null)
		{
			Element zeroLineEl = XmlUtils.findOrCreateChild(yAxis, "zero_line");
			AnyChartNode.addColorAttribute(zeroLineEl, yAxisZeroLineColor);
		}
		if (yAxisZeroLineWidth != null)
		{
			Element zeroLineEl = XmlUtils.findOrCreateChild(yAxis, "zero_line");
			zeroLineEl.setAttribute("thickness", yAxisZeroLineWidth);
		}
		yAxis.setAttribute("position", pos);
		if (stacked != AnyChartNode.StackedType.NotStacked) {
			Element scaleEl = XmlUtils.findOrCreateChild(yAxis, "scale");
			scaleEl.setAttribute("mode", AnyChartNode.getAnyChartStackedType(stacked));
		}
		Element title = XmlUtils.findOrCreateChild(yAxis, "title");
		String labelValue = yUseDefaultLabel ? anyChartNode.getDefaultYAxisLabel() : yLabel;
		title.setAttribute("enabled", yShowLabels ? "True" : "False");
		Element f = getFontElement(titleFont, scaleFactor);
		AnyChartNode.addColorAttribute(f, titleColor);
		title.addContent(f);
		Element text = XmlUtils.findOrCreateChild(title, "text");
		text.setText(labelValue == null ? " " : labelValue);
		Element labelsEl = XmlUtils.findOrCreateChild(yAxis, "labels");
		labelsEl.setAttribute("enabled", yShowTickmarkLabels ? "True" : "False");
		labelsEl.setAttribute("rotation", String.valueOf(yLabelAngle));
		
		String formatText;
		if (yAxisLabelFormat != null)
		{
			formatText = yAxisLabelFormat;
		}
		else
		{
			if (anyChartNode.getChartType() == ChartType.Heatmap)
			{
				formatText = xAxis.getXAxisFormat(anyChartNode);
			}
			else
			{
				String formatStr = anyChartNode.getYSeriesFormat();
				formatText = (formatStr == null ? "{%Value}" : FlashRenderer.getAnychartNumberFormat("Value", formatStr));
			}
		}
		anyChartNode.addFormat(labelsEl, formatText);

		f = getFontElement(font, scaleFactor);
		AnyChartNode.addColorAttribute(f, labelColor);
		labelsEl.addContent(f);
		if (yMajorInterval > 0.0) {
			Element scaleEl = XmlUtils.findOrCreateChild(yAxis, "scale");
			scaleEl.setAttribute("major_interval", String.valueOf(yMajorInterval));
			if (yMinorInterval > 0.0)
				scaleEl.setAttribute("minor_interval", String.valueOf(yMinorInterval));
		}
		if (yShowMajorGrid == false) 
			XmlUtils.findOrCreateChild(yAxis, "major_grid").setAttribute("enabled", "False");
		else {
			Element g = XmlUtils.findOrCreateChild(yAxis, "major_grid");
			g.setAttribute("interlaced", interlaced ? "True" : "False");
			Element dashedEl = XmlUtils.findOrCreateChild(g, "line");
			dashedEl.setAttribute("enabled", "True");
			dashedEl.setAttribute("thickness", String.valueOf(yMajorGridLineThickness));
			AnyChartNode.addColorAttribute(dashedEl, yMajorGridLineColor);
			if (yMajorGridLineStyle == LineStyle.Dashed) {
				dashedEl.setAttribute("dashed", "True");
				dashedEl.setAttribute("dash_length", "5");
			}
		}
		if (yShowMinorGrid == false) 
			XmlUtils.findOrCreateChild(yAxis, "minor_grid").setAttribute("enabled", "False");
		else {
			Element g = XmlUtils.findOrCreateChild(yAxis, "minor_grid");
			g.setAttribute("interlaced", interlaced ? "True" : "False");
			Element dashedEl = XmlUtils.findOrCreateChild(g, "line");
			dashedEl.setAttribute("enabled", "True");
			dashedEl.setAttribute("thickness", String.valueOf(yMinorGridLineThickness));
			AnyChartNode.addColorAttribute(dashedEl, yMinorGridLineColor);
			if (yMinorGridLineStyle == LineStyle.Dashed) {
				dashedEl.setAttribute("dashed", "True");
				dashedEl.setAttribute("dash_length", "5");
			}
		}
		Element tickmarkEl = XmlUtils.findOrCreateChild(yAxis, "major_tickmark");
		if (yShowMajorTickmarks == false) {
			tickmarkEl.setAttribute("enabled", "False");
		}
		else {
			tickmarkEl.setAttribute("thickness", String.valueOf(yMajorTickmarkWidth));
			AnyChartNode.addColorAttribute(tickmarkEl, yMajorTickmarkColor);
		}
		tickmarkEl = XmlUtils.findOrCreateChild(yAxis, "minor_tickmark"); 
		if (yShowMinorTickmarks == false) {
			tickmarkEl.setAttribute("enabled", "False");
		}
		else {
			tickmarkEl.setAttribute("thickness", String.valueOf(yMinorTickmarkWidth));
			AnyChartNode.addColorAttribute(tickmarkEl, yMinorTickmarkColor);
		}
		Element scale = XmlUtils.findOrCreateChild(yAxis, "scale");
		if (stacked == StackedType.PercentStacked)
		{
			yMinimum = "0";
			yMaximum = "100";
		}
		if (yMinimum != null)
			scale.setAttribute("minimum", yMinimum);
		if (yMaximum != null)
			scale.setAttribute("maximum", yMaximum);
		if (yScaleType != null && !yScaleType.equals("Linear")) // forcing linear screws up date axes and 'Date or Time' is not yet supported
			scale.setAttribute("type", yScaleType);
	}
	
	public boolean notShareAxes() {
		return (yShare == false);
	}

	public void setShare(boolean b) {
		this.yShare = b;
	}
	public void setLocation(boolean normal) {
		yPosition = (normal ? NORMAL : OPPOSITE);
	}

	public boolean isyEnabled() {
		return yEnabled;
	}

	public void setyEnabled(boolean yEnabled) {
		this.yEnabled = yEnabled;
	}

	public String getyMaximum() {
		return yMaximum;
	}

	public void setyMaximum(String yMaximum) {
		this.yMaximum = yMaximum;
	}

	public String getyMinimum() {
		return yMinimum;
	}

	public void setyMinimum(String yMinimum) {
		this.yMinimum = yMinimum;
	}
	
	public Font getLabelFont() {
		return font;
	}
	
	public void setLabelFont(String str)
	{
		font = XmlUtils.getFontContent(str);
	}
	
	public void setLabelFontSize(float newSize){
		if (font == null)
		{
			setLabelFont("Arial-PLAIN-8");
		}
		font = font.deriveFont(newSize);
	}
	
	public Font getTitleFont() {
		return titleFont;
	}
	
	public void setTitleFont(String str)
	{
		titleFont = XmlUtils.getFontContent(str);
	}
	
	public void setTitleFontSize(float newSize)
	{
		if (titleFont == null)
		{
			setTitleFont("Arial-PLAIN-8");
		}
		titleFont = titleFont.deriveFont(newSize);
	}

	public boolean isyUseDefaultLabel() {
		return yUseDefaultLabel;
	}

	public void setyUseDefaultLabel(boolean yUseDefaultLabel) {
		this.yUseDefaultLabel = yUseDefaultLabel;
	}

	public boolean isyShowLabels() {
		return yShowLabels;
	}

	public void setyShowLabels(boolean yShowLabels) {
		this.yShowLabels = yShowLabels;
	}

	public String getyLabel() {
		return yLabel;
	}

	public void setyLabel(String yLabel) {
		this.yLabel = yLabel;
	}

	public String getyAxisLabelFormat() {
		return yAxisLabelFormat;
	}

	public void setyAxisLabelFormat(String yAxisLabelFormat) {
		this.yAxisLabelFormat = yAxisLabelFormat;
	}

	public boolean isyShowTickmarkLabels() {
		return yShowTickmarkLabels;
	}

	public void setyShowTickmarkLabels(boolean yShowTickmarkLabels) {
		this.yShowTickmarkLabels = yShowTickmarkLabels;
	}

	public boolean isyShowGroupLabels() {
		return yShowGroupLabels;
	}

	public void setyShowGroupLabels(boolean yShowGroupLabels) {
		this.yShowGroupLabels = yShowGroupLabels;
	}

	public void applyStyle(Axes style) {
		if (style == null)
			return;
		
		font = style.font;
		labelColor = style.labelColor;
		titleFont = style.titleFont;
		titleColor = style.titleColor;		
		yShare = style.yShare;
		yEnabled = style.yEnabled;
		axisStyle = style.axisStyle;
		axisWidth = style.axisWidth;
		axisColor = style.axisColor;
		yShowLabels = style.yShowLabels;
		yLabelAngle = style.yLabelAngle;
		yShowMajorGrid = style.yShowMajorGrid;
		yMajorGridLineStyle = style.yMajorGridLineStyle;
		yMajorGridLineColor = style.yMajorGridLineColor;
		yMajorGridLineThickness = style.yMajorGridLineThickness;
		yShowMinorGrid = style.yShowMinorGrid;
		yMinorGridLineStyle = style.yMinorGridLineStyle;
		yMinorGridLineColor = style.yMinorGridLineColor;
		yMinorGridLineThickness = style.yMinorGridLineThickness;
		yShowTickmarkLabels = style.yShowTickmarkLabels;
		yShowMajorTickmarks = style.yShowMajorTickmarks;
		yMajorTickmarkWidth = style.yMajorTickmarkWidth;
		yMajorTickmarkColor = style.yMajorTickmarkColor;
		yShowMinorTickmarks = style.yShowMinorTickmarks;
		yMinorTickmarkWidth = style.yMinorTickmarkWidth;
		yMinorTickmarkColor = style.yMinorTickmarkColor;
		yShowGroupLabels = style.yShowGroupLabels;
		yPosition = style.yPosition;
		yMajorInterval = style.yMajorInterval;
		yMinorInterval = style.yMinorInterval;
		yScaleType = style.yScaleType;
		yAxisZeroLineColor = style.yAxisZeroLineColor;
		yAxisZeroLineWidth = style.yAxisZeroLineWidth;
		yAxisLabelFormat = style.yAxisLabelFormat;
	}
}
