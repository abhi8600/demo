package com.successmetricsinc.chart.AnyChart;

import java.awt.Color;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.successmetricsinc.util.BirstXMLSerializable;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;

public class Condition extends BirstXMLSerializable{
	private static final String COLOR = "Color";
	private static final String VALUE3 = "Value3";
	private static final String VALUE2 = "Value2";
	private static final String VALUE1 = "Value1";
	private static final String TYPE = "Type";
	private static final String NAME = "Name";
	
	private static final ConditionType DEFAULT_CONDITION_TYPE = ConditionType.between;
	
	private String name;
	
	public enum ConditionType { between, notBetween, equalTo, notEqualTo, greaterThan, 
		greaterThanOrEqualTo, lessThan, lessThanOrEqualTo
	}
	
	private ConditionType type = DEFAULT_CONDITION_TYPE;
	private String value1;
	private String value2;
	private String value3;
	private Color color;
	
	private static Map<ConditionType, String> conditionTypeMap;
	static {
		conditionTypeMap = new HashMap<ConditionType, String>();
		conditionTypeMap.put(ConditionType.between, "between");
		conditionTypeMap.put(ConditionType.notBetween, "notBetween");
		conditionTypeMap.put(ConditionType.equalTo, "equalTo");
		conditionTypeMap.put(ConditionType.notEqualTo, "notEqualTo");
		conditionTypeMap.put(ConditionType.greaterThan, "greaterThan");
		conditionTypeMap.put(ConditionType.greaterThanOrEqualTo, "greaterThanOrEqualTo");
		conditionTypeMap.put(ConditionType.lessThan, "lessThan");
		conditionTypeMap.put(ConditionType.lessThanOrEqualTo, "lessThanOrEqualTo");
	}
	@Override
	public void addContent(OMFactory fac, OMElement parent, OMNamespace ns) {
		XmlUtils.addContent(fac, parent, NAME, name, ns);
		XmlUtils.addContent(fac, parent, TYPE, conditionTypeMap.get(type), ns);
		XmlUtils.addContent(fac, parent, VALUE1, value1, ns);
		XmlUtils.addContent(fac, parent, VALUE2, value2, ns);
		XmlUtils.addContent(fac, parent, VALUE3, value3, ns);
		XmlUtils.addContent(fac, parent, COLOR, color, ns);
	}
	@Override
	public BirstXMLSerializable createNew() {
		return new Condition();
	}
	@Override
	public void parseElement(OMElement parent) {
		for (Iterator<OMElement> iter = parent.getChildElements(); iter.hasNext(); ) {
			OMElement el = iter.next();
			String elName = el.getLocalName();
			
			if (NAME.equals(elName)) {
				name = XmlUtils.getStringContent(el);
			}
			else if (TYPE.equals(elName)) {
				type = parseTypeFromString(XmlUtils.getStringContent(el), conditionTypeMap, DEFAULT_CONDITION_TYPE);
			}
			else if (VALUE1.equals(elName)) {
				value1 = XmlUtils.getStringContent(el);
			}
			else if (VALUE2.equals(elName)) {
				value2 = XmlUtils.getStringContent(el);
			}
			else if (VALUE3.equals(elName)) {
				value3 = XmlUtils.getStringContent(el);
			}
			else if (COLOR.equals(elName)) {
				color = XmlUtils.getColorContent(el);
			}
		}
	}
	@Override
	public void parseElement(XMLStreamReader el) throws Exception {
		for (Iterator iter = new XMLStreamIterator(el); iter.hasNext(); ) {
			String elName = el.getLocalName();
			
			if (NAME.equals(elName)) {
				name = XmlUtils.getStringContent(el);
			}
			else if (TYPE.equals(elName)) {
				type = parseTypeFromString(XmlUtils.getStringContent(el), conditionTypeMap, DEFAULT_CONDITION_TYPE);
			}
			else if (VALUE1.equals(elName)) {
				value1 = XmlUtils.getStringContent(el);
			}
			else if (VALUE2.equals(elName)) {
				value2 = XmlUtils.getStringContent(el);
			}
			else if (VALUE3.equals(elName)) {
				value3 = XmlUtils.getStringContent(el);
			}
			else if (COLOR.equals(elName)) {
				color = XmlUtils.getColorContent(el);
			}
		}
		
	}
}
