/**
 * $Id: BirstWebServiceResult.java,v 1.35 2012-12-12 05:17:38 BIRST\mpandit Exp $
 *
 * Copyright (C) 2009-2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.successmetricsinc.WebServices;

import java.io.FileNotFoundException;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRRuntimeBirstException;
import net.sf.jasperreports.engine.JRRuntimeException;
import net.sf.jasperreports.engine.design.JRValidationException;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;

import com.successmetricsinc.Session;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.catalog.CatalogAccessException;
import com.successmetricsinc.jasperReports.JRSubreportRecursionException;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.JRBandOverflowException;
import com.successmetricsinc.util.XmlUtils;

/**
 * An BirstWebServiceResult is the implementation of all BirstWebService results.  Each instance contains an error 
 * value, an error message and/or the actual results of the web service call.
 * 
 * @author agarrison
 *
 */
public class BirstWebServiceResult  {
	private static final String ASYNCH_RESULT_ID = "AsynchResultId";
	public final static String ERR_CODE = "ErrorCode";
	public final static String ERR_MSG = "ErrorMessage";
	public final static String RESULT = "Result";
	
	private static Logger logger = Logger.getLogger(BirstWebServiceResult.class);
	private int errorCode = BaseException.SUCCESS;
	private String errorMessage;
	private IWebServiceResult result;
	private String asynchronousResultId;
	
	/**
	 * Is this result valid?
	 * @return true if the web service call succeeded, false otherwise.
	 */
	public boolean isSuccess() {
		return errorCode == BaseException.SUCCESS;
	}
	
	/**
	 * Returns the error code.
	 * @return Returns the error code.  A value of SUCCESS_ERROR_CODE indicates no error.
	 */
	public int getErrorCode() {
		return errorCode;
	}
	
	/**
	 * set up the error message and error code from the Exception
	 * @param ex
	 */
	public void setException(Exception ex)
	{
		com.successmetricsinc.UserBean ub = SMIWebUserBean.getUserBean();
		if (ub != null)
		{
			Session session = ub.getRepSession();
			if (session != null && session.isCancelled()) {
				errorCode = BaseException.ERROR_QUERY_CANCELLED;
				return;
			}
		}
		errorMessage = ex.getMessage();

		// figure out the correct error code and error message
		// only log certain errors
		boolean logged = false;
		if (ex instanceof BaseException)
		{
			errorCode = ((BaseException) ex).getErrorCode();
			if (errorCode == BaseException.ERROR_DATA_UNAVAILABLE) // for this error, the cause is the actual exception
			{
				Throwable t = ex.getCause();
				if (t != null)
					logger.warn(t, t);
				else
					logger.warn(ex, ex);
				logged = true;
			}
		}
		else if (ex instanceof FileNotFoundException)
		{
			logger.warn(ex, ex);
			logged = true;
			errorCode = BaseException.ERROR_FILE_NOT_FOUND;
			errorMessage = "Could not find the item"; // do not return error messages that might provide file path information (information leakage issue)
		}
		else if (ex instanceof JRValidationException) // band overflow errors
		{
			errorCode = BaseException.ERROR_JASPER_DISPLAY;
		}
		else if ((ex instanceof JRSubreportRecursionException) || (ex instanceof JRBandOverflowException) || (ex instanceof JRRuntimeBirstException))
		{
			errorCode = BaseException.ERROR_JASPER_DISPLAY;
			// all error messages for these errors are safe
		}
		else if ((ex instanceof JRRuntimeException) || (ex instanceof JRException))
		{
			Throwable t = ex.getCause();
			if (t != null)
				errorMessage = t.getMessage(); // the message on the cause is the real message
			if ((t instanceof JRSubreportRecursionException) || (t instanceof JRBandOverflowException) || (ex instanceof JRRuntimeBirstException))
			{
				errorCode = BaseException.ERROR_JASPER_DISPLAY;
				// all error messages for these errors are safe
			}
			else if (t instanceof BaseException) // is the cause something we understand
			{
				errorCode = ((BaseException) t).getErrorCode();
			}
			else
			{
				logger.warn(ex, ex);
				logged = true;
				errorCode = BaseException.ERROR_JASPER_OTHER; // do not display the error message, but categorize it as different than ERROR_OTHER
				errorMessage = null; // do not return error messages that might provide file path information (information leakage issue)
			}
		} else if (ex instanceof CatalogAccessException)
		{
			errorCode = BaseException.ERROR_PERMISSION;
		}
		else
		{
			logger.warn(ex, ex);
			logged = true;
			errorCode = BaseException.ERROR_OTHER; // do not display the error message
			errorMessage = null; // do not return error messages that might provide file path information (information leakage issue)
		}
		if (!logged)
		{
			logger.info(ex.getMessage());
		}
	}
	
	/**
	 * Sets the error code
	 * @param errorCode	the new error code value
	 */
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	
	/**
	 * Returns a description of the error
	 * @return	Gets a description of the error
	 */
	public String getErrorMessage() {
		return errorMessage;
	}
	
	/**
	 * Sets the description of the error
	 * @param errorMessage	the new description of the error
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	/**
	 * Returns the result of the web service call if the call was successful.
	 * @return	Gets the result of the web service call.
	 */
	public IWebServiceResult getResult() {
		return result;
	}
	
	/**
	 * Sets the result of the web service call
	 * @param result	the new result of the web service call.
	 */
	public void setResult(IWebServiceResult result) {
		this.result = result;
	}

	/**
	 * Convert the result object into it's OMElement representation
	 * @return
	 */
	public OMElement toOMElement() {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace ns = fac.createOMNamespace("", "");
		OMElement ret = fac.createOMElement(this.getClass().getName(), ns);
		XmlUtils.addContent(fac, ret, ERR_CODE, this.errorCode, ns);
		XmlUtils.addContent(fac, ret, ERR_MSG, XmlUtils.encode(this.errorMessage), ns);
		
		if (asynchronousResultId != null)
			XmlUtils.addContent(fac, ret, BirstWebServiceResult.ASYNCH_RESULT_ID, asynchronousResultId, ns);
		
		if (this.result != null) {
			OMElement result = fac.createOMElement(RESULT, ns);
			this.result.addContent(result, fac, ns);
			ret.addChild(result);
		}
		return ret;
	}

	public String getAsynchronousResultId() {
		return asynchronousResultId;
	}

	public void setAsynchronousResultId(String asynchronousResultId) {
		this.asynchronousResultId = asynchronousResultId;
	}
}
