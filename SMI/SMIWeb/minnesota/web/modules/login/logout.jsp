<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="com.successmetricsinc.*" %>
<jsp:useBean id="userBean" scope="session" class="com.successmetricsinc.UI.UserBean"></jsp:useBean>
<jsp:setProperty name="userBean" property="httpServletRequest" value="<%= request %>"/> 
<% 
    String url = ServerContext.getLoginPageURL();
   	if (!url.startsWith("http"))
    {
    	url = request.getContextPath() + url;
    }
    session.invalidate();
    boolean acornP = ServerContext.isEnableAcorn(); 
%>
<html>
	<head>
	<fmt:setBundle basename="com.successmetricsinc.UI.bundle.Messages"/>
	<title><fmt:message key="login_title"/></title>
	</head>
	<body class="systemerror">
		<table>
			<tr><td>
				<div class="logo"/>
			</td></tr>
		</table>
		<br/>
		<div class="systemerror1">
			<fmt:message key="login_logout_top"/>
		</div>
		<br/>
		<div class="systemerror2">
			<fmt:message key="login_logout">
				<fmt:param>
					<%=url%>
				</fmt:param>
			</fmt:message>
		</div>
		<br/>
		<br/>
		<br/>
		<br/>
		<div class="copyright">
			<fmt:message key="login_unauthorized_use"/>
		</div>
		<div class="copyright">
			<fmt:message key="login_copyright"/>
		</div>
	</body>
</html>