<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="com.successmetricsinc.*" %>
<html>
	<head>
	<fmt:setBundle basename="com.successmetricsinc.UI.bundle.Messages"/>
	<title><fmt:message key="login_title"/></title>
	</head>
	<body class="login">
		<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
			<tr height="10%">
				<td>
					<div class="loginlogo">
					</div>
				</td>
			</tr>
			<tr height="80%">
				<td>
					<div class="login" align="center">
						<br/>
						<div class="logintext">
							<fmt:message key="login_fail" />
							Click <a href='<%=ServerContext.getLoginPageURL().startsWith("http") ? ServerContext.getLoginPageURL() : request.getContextPath() + ServerContext.getLoginPageURL() %>'>here</a> to login.
						</div>
							
						</div>
						<br/>
					</div>
				</td>
			</tr>
			<tr height="10%">
				<td>
				<div class="copyright">
					<fmt:message key="login_unauthorized_use"/>
				</div>
				<div class="copyright">
					<fmt:message key="login_copyright"/>
				</div>
				<div class="copyright">	
					<fmt:message key="login_apache"/>
				</div>
				<div class="copyright">	
					<%= Util.getBuildString() %>
				</div>
				</td>
			</tr>
		</table>
	</body>
</html>