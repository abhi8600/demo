<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="com.successmetricsinc.*" %>
<html>
	<head>
	<meta http-equiv="pragma" content="NO-CACHE"></meta>
	<meta http-equiv="expires" content="0"></meta>
	<meta http-equiv="Cache-Control" content="no-cache" ></meta>
	<fmt:setBundle basename="com.successmetricsinc.UI.bundle.Messages"/>
	<title><fmt:message key="login_title"/></title>
	</head>
	<body class="login" onload="setFocus();">
		<script type="text/javascript">
			function setFocus() {
				document.getElementById("j_username").focus();
			}
		</script>
		<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
			<tr style="height:10%">
				<td>
					<div class="loginlogo">
					</div>
				</td>
			</tr>
			<tr style="height:80%">
				<td>
					<div class="login" >
						<table width="100%">
							<tr>
								<td></td>
								<td class="loginwelcome">
									<fmt:message key="login_welcome" />
								</td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td>
								<table align="center" class="logintext">
									<form id="login" action="j_security_check" method="POST" AUTOCOMPLETE="off">
										<tr>
											<td class="logintext"><fmt:message key="login_username"/></td>
											<td><input type="INPUT" id="j_username" name="j_username" size=30></input></td>
										</tr>
										<tr>
											<td class="logintext"><fmt:message key="login_password"/></td>
											<td><input type="PASSWORD" id="j_password" name="j_password" size=30></input></td>
										</tr>
										<tr><td></td></tr>
										<tr>
											<td></td>
											<td align="left"><input type="SUBMIT" value="Login"></td>
										</tr>
									</form>
								</table>
								</td>
								<td></td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
			<tr style="height:10%">
				<td>
				<div class="copyright">
					<fmt:message key="login_unauthorized_use"/>
				</div>
				<div class="copyright">
					<fmt:message key="login_copyright"/>
				</div>
				<div class="copyright">	
					<fmt:message key="login_apache"/>
				</div>
				<div class="copyright">	
					<%= Util.getBuildString() %>
				</div>
				</td>
			</tr>
		</table>
	</body>
</html>
