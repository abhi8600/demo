
tcnative-1.dll is the 32-bit native library for tomcat - improves production performance
tcnative-1.dll.amd64 is the 64-bit native library tomcat (must be renamed to tcnative-1.dll)

Copy the appropriate one into the PATH.  Note that the 64 bit is only appropriate if you are using the 64 bit version of Java.

Add
	<Valve className="com.jamonapi.http.JAMonTomcatValve"/>
to after 
   <Engine name="Catalina" defaultHost="localhost">
   in server.xml