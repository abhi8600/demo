/**
 * $Id: ImageServlet.java,v 1.32 2011-11-08 02:05:59 agarrison Exp $
 *
 * Copyright (C) 2007-2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc;

import java.awt.Dimension;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRImage;
import net.sf.jasperreports.engine.JRImageRenderer;
import net.sf.jasperreports.engine.JRPrintElement;
import net.sf.jasperreports.engine.JRPrintElementIndex;
import net.sf.jasperreports.engine.JRPrintImage;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JRRenderable;
import net.sf.jasperreports.engine.JRWrappingSvgRenderer;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.fill.JRTemplatePrintFrame;
import net.sf.jasperreports.engine.type.OnErrorTypeEnum;
import net.sf.jasperreports.engine.util.JRTypeSniffer;

import org.apache.log4j.Logger;

import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.chart.Chart;
import com.successmetricsinc.chart.FlashRenderer;
import com.successmetricsinc.chart.JCommonDrawableRenderer;
import com.successmetricsinc.util.Util;

/**
 * @author Teodor Danciu (teodord@users.sourceforge.net)
 */
public class ImageServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	
	private static Logger logger = Logger.getLogger(ImageServlet.class);

	public static final String DEFAULT_JASPER_PRINT_LIST_SESSION_ATTRIBUTE = "net.sf.jasperreports.j2ee.jasper_print_list";
	public static final String DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE = "net.sf.jasperreports.j2ee.jasper_print";
	public static final String JASPER_PRINT_LIST_REQUEST_PARAMETER = "jrprintlist";
	public static final String JASPER_PRINT_REQUEST_PARAMETER = "jrprint";
	public static final String IMAGE_NAME_REQUEST_PARAMETER = "image";

	/**
	 * 
	 */
	public void service(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		byte[] imageData = null;
		String imageMimeType = null;
		String token = request.getParameter(IMAGE_NAME_REQUEST_PARAMETER);
		// parse imageName=timestamp-image
		String[] pair = token.split("-");
		String imageName = pair[1];
		if ("px".equals(imageName))
		{
			try
			{
				JRRenderable pxRenderer = JRImageRenderer.getInstance("net/sf/jasperreports/engine/images/pixel.GIF", OnErrorTypeEnum.ERROR);
				imageData = pxRenderer.getImageData();
			} catch (JRException e)
			{
				logger.warn(e, e);
				response.sendError(HttpServletResponse.SC_BAD_REQUEST);
				return;
			}
		} else
		{
			List<JasperPrint> jasperPrintList = SMIWebUserBean.getJasperPrintObject(request);
			if (jasperPrintList == null || jasperPrintList.size() == 0)
			{
				logger.warn("ImageServlet: No JasperPrint documents found on the HTTP session.  Token=" + token);
				response.sendError(HttpServletResponse.SC_BAD_REQUEST);
				return;
			}
			
			boolean xml = imageName.endsWith(".xml");
			String iname = xml ? imageName.substring(0, imageName.length() - 4) : imageName;
			JRPrintElementIndex imageIndex = JRHtmlExporter.getPrintElementIndex(iname);
			JasperPrint report = (JasperPrint) jasperPrintList.get(imageIndex.getReportIndex());
			JRPrintPage page = (JRPrintPage) report.getPages().get(imageIndex.getPageIndex());
			JRPrintElement element = (JRPrintElement) page.getElements().get(imageIndex.getAddressArray()[0].intValue());
			JRPrintImage image = null;
			if (element instanceof JRPrintImage)
				image = (JRPrintImage)element;
			else if (element instanceof JRTemplatePrintFrame ) {
				@SuppressWarnings("rawtypes")
				List elements = ((JRTemplatePrintFrame)element).getElements();
				if (elements != null && elements.size() > 0) {
					for (int i = 0; i < elements.size() && image == null; i++) {
						if (elements.get(i) instanceof JRPrintImage) 
							image = (JRPrintImage)elements.get(i);
					}
				}
			}
			if (image == null) {
				logger.warn("ImageServlet: Could not find image in HTTP session.  Token=" + token);
				response.sendError(HttpServletResponse.SC_BAD_REQUEST);
				return;
			}
			JRRenderable renderer = image.getRenderer();
			if (xml)
			{
				imageMimeType = "text/html; charset=UTF-8";
				Double scaleFactor = (Double) request.getSession(false).getAttribute("scaleFactor");
				if (scaleFactor == null)
				{
					scaleFactor = ServerContext.getDefaultScaleFactor();
				}
				String xmlData = ((FlashRenderer) renderer).getXML(scaleFactor);
				String result = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">"
						+ "<html xmlns=\"http://www.w3.org/1999/xhtml\">"
						+ "<head><script type=\"text/javascript\" language=\"javascript\" src=\"/SMIWeb/js/AnyChart.js\"></script>"
						+ "<script type=\"text/javascript\" language=\"javascript\" src=\"/SMIWeb/smi/adhocContent.js\"></script>"
						+ "<script type=\"text/javascript\" language=\"javascript\">"
						+ "function setchart() {var chart=new AnyChart('/SMIWeb/swf/AnyChart.swf','/SMIWeb/swf/Preloader.swf');chart.xmlLoadingText=\"Loading: \";chart.preloaderLoadingText=\"Loading...\";chart.noDataText=\"No Data\";chart.width="
						+ ((int) ((image.getWidth() * scaleFactor)))
						+ ";chart.height ="
						+ ((int) ((image.getHeight() * scaleFactor)))
						+ ";var xdata=document.getElementById('cdata').value.toString();chart.setData(xdata);"
						+ "chart.wMode=\"transparent\";chart.write('flash');}"
						+ "if(window.attachEvent){window.attachEvent('onload',setchart);}else if (window.addEventListener){window.addEventListener('load',setchart,false);}"
						+ "</script></head><body>"
						+ "<input type=\"hidden\" id=\"cdata\" value=\""
						+ (Chart.xmlify(xmlData))
						+ "\"></input>" + "<div id=\"flash\"></div></body></html>";
				if (imageMimeType != null)
				{
					response.setHeader("Content-Type", imageMimeType);
				}
				response.setCharacterEncoding("UTF-8");
				// the following two lines deal with an IE6/AJAX (innerHTML) issue of not caching images properly
				response.setDateHeader("Expires", System.currentTimeMillis() + 24 * 60 * 60 * 1000);
				response.setHeader("Cache-control", "max-age=86400");
				PrintWriter writer = response.getWriter();
				writer.write(result);
				writer.flush();
				writer.close();
				return;
			} else
			{
				try
				{
					HttpSession session = request.getSession();
					com.successmetricsinc.UserBean userBean = (com.successmetricsinc.UserBean) session.getAttribute("userBean");
					SMIWebUserBean.setHttpServletRequest(request);
					SMIWebUserBean.addThreadRequest(Long.valueOf(Thread.currentThread().getId()), userBean);
					
					if (JCommonDrawableRenderer.class.isInstance(renderer))
					{
						imageMimeType = "image/png";
						imageData = ((JCommonDrawableRenderer) renderer).getByteArray();
					}
					if (imageData == null)
					{
						if (renderer.getType() == JRRenderable.TYPE_SVG)
						{
							Double scaleFactor = (Double) request.getSession(false).getAttribute("scaleFactor");
							if (scaleFactor == null)
							{
								scaleFactor = ServerContext.getDefaultScaleFactor();
							}
							renderer = new JRWrappingSvgRenderer(renderer, new Dimension(Util.scalePx(scaleFactor.doubleValue(), image.getWidth()), Util.scalePx(
									scaleFactor.doubleValue(), image.getHeight())), image.getBackcolor());
						}
						imageMimeType = JRTypeSniffer.getImageMimeType(renderer.getImageType());
						try
						{
							imageData = renderer.getImageData();
						} catch (JRException e)
						{
							logger.warn(e, e);
							response.sendError(HttpServletResponse.SC_BAD_REQUEST);						
							return;
						}
					}
				}
				finally
				{
					SMIWebUserBean.removeThreadRequest(Long.valueOf(Thread.currentThread().getId()));
				}
			}
		}
		if (imageData != null && imageData.length > 0)
		{
			if (imageMimeType != null)
			{
				response.setHeader("Content-Type", imageMimeType);
			}
			response.setCharacterEncoding("UTF-8");
			// the following two lines deal with an IE6/AJAX (innerHTML) issue of not caching images properly
			response.setDateHeader("Expires", System.currentTimeMillis() + 24 * 60 * 60 * 1000);
			response.setHeader("Cache-control", "max-age=86400");
			response.setContentLength(imageData.length);

			ServletOutputStream ouputStream = response.getOutputStream();
			ouputStream.write(imageData, 0, imageData.length);
			ouputStream.flush();
			ouputStream.close();
		}
	}
}
