/**
 * $Id: SMIContextListener.java,v 1.4 2011-10-11 22:48:20 ricks Exp $
 *
 * Copyright (C) 2007-2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.successmetricsinc;

import javax.servlet.*;
import org.apache.log4j.LogManager;

/**
  * This class is used to shutdown log4j properly.
  * When the context of the application is destroyed, log4j should
  * be shut down to avoid keeping locked log files.
  * 
  * This class is put in a JAR file after compilation, and the JAR file 
  * itself is in the WEB-INF/lib directory of my webapp.
  *
  * Add to the WEB-INF/web.xml file:
  *	
  * <!-- ServletContextListener called when the application ends -->
  * <listener>
  * <listener-class>com.successmetricsinc.SMIContextListener</listener-class>
  * </listener>
  */
public class SMIContextListener implements ServletContextListener
{
   /**
    * Do nothing.
    */
   public void contextInitialized(ServletContextEvent sce)
   {
	   // Do nothing here...
   }

   /**
    * Shuts log4j down.
    * This is necessary to avoid lost log files: when the app
    * is reloaded, the JVM might keep a lock on the last log file
    * otherwise, and will not be able to rename it before
    * restarting it.
    */
   public void contextDestroyed(ServletContextEvent sce)
   {
	   LogManager.shutdown();
   }
}