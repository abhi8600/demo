package com.successmetricsinc;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.successmetricsinc.adhoc.AdhocColumn;
import com.successmetricsinc.adhoc.AdhocColumn.ColumnType;
import com.successmetricsinc.adhoc.AdhocEntity;
import com.successmetricsinc.adhoc.AdhocReport;
import com.successmetricsinc.adhoc.AdhocSort;
import com.successmetricsinc.adhoc.AdhocTextEntity;
import com.successmetricsinc.query.DisplayFilter;
import com.successmetricsinc.query.QueryFilter;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.util.Util;

public class SearchReports
{
	private static Logger logger = Logger.getLogger(SearchReports.class);

	private enum EntityType
	{
		Column, Sort, Filter, DisplayFilter
	};
	private String spaceDirectory;
	private String path;
	private String dimValue;
	private String lowerDimValue;
	private String exprValue;
	private String lowerExprValue;
	private String labelValue;
	private String lowerLabelValue;
	private String replaceDimValue;
	private String replaceExprValue;
	private String replaceLabelValue;
	private Pattern p;
	private File outputFile;
	private List<String> reportNames = new ArrayList<String>();
	private List<String> reportPaths = new ArrayList<String>();
	private List<String> expressions = new ArrayList<String>();
	private List<String> colNames = new ArrayList<String>();

	public SearchReports(String spaceDirectory, String path, String fileNameFilter, String dimValue, String exprValue, String labelValue, File outputFile)
	{
		this.dimValue = dimValue == null ? null : decode(dimValue);
		this.exprValue = exprValue == null ? null : decode(exprValue);
		this.labelValue = labelValue == null ? null : decode(labelValue);
		if (dimValue != null)
			lowerDimValue = dimValue.toLowerCase();
		if (exprValue != null)
		{
			/*
			 * Handle case where they enter a dimension/column value in the expression rather than using the appropriate dimension field
			 */
			lowerExprValue = exprValue.toLowerCase();
			if (lowerDimValue == null || lowerDimValue.trim().length() == 0)
			{
				String s = lowerExprValue;
				if (s.startsWith("[") && s.endsWith("]"))
					s = s.substring(1, s.length()-1);
				if (s.indexOf('[') < 0)
				{
					String[] values = s.split("[.]");
					if (values.length == 2)
					{
						lowerExprValue = values[1];
						lowerDimValue = values[0];
					}
				}
			}
		}
		if (labelValue != null)
			lowerLabelValue = labelValue.toLowerCase();
		this.spaceDirectory = spaceDirectory;
		this.path = path;
		this.outputFile = outputFile;
		if (fileNameFilter != null && !fileNameFilter.equals("*") && fileNameFilter.length() > 0)
			p = Pattern.compile(fileNameFilter, Pattern.CASE_INSENSITIVE);
	}

	public SearchReports(String spaceDirectory, String[] reportPaths, String[] names, String dimValue, String exprValue, String labelValue,
			String replaceDimValue, String replaceExprValue, String replaceLabelValue)
	{
		this.dimValue = dimValue == null ? null : decode(dimValue);
		this.exprValue = exprValue == null ? null : decode(exprValue);
		this.labelValue = labelValue == null ? null : decode(labelValue);
		if (dimValue != null)
			lowerDimValue = dimValue.toLowerCase();
		if (exprValue != null)
			lowerExprValue = exprValue.toLowerCase();
		if (labelValue != null)
			lowerLabelValue = labelValue.toLowerCase();
		if (exprValue != null)
		{
			/*
			 * Handle case where they enter a dimension/column value in the expression rather than using the appropriate dimension field
			 */
			lowerExprValue = exprValue.toLowerCase();
			if (lowerDimValue == null || lowerDimValue.trim().length() == 0)
			{
				String s = lowerExprValue;
				if (s.startsWith("[") && s.endsWith("]"))
					s = s.substring(1, s.length()-1);
				if (s.indexOf('[') < 0)
				{
					String[] values = s.split("[.]");
					if (values.length == 2)
					{
						lowerExprValue = values[1];
						lowerDimValue = values[0];
					}
				}
			}
		}
		this.spaceDirectory = spaceDirectory;
		this.replaceDimValue = decode(replaceDimValue);
		this.replaceExprValue = decode(replaceExprValue);
		this.replaceLabelValue = decode(replaceLabelValue);
		if (replaceExprValue != null)
		{
			/*
			 * Handle case where they enter a dimension/column value in the expression rather than using the appropriate dimension field
			 */
			replaceExprValue = exprValue.toLowerCase();
			if (replaceDimValue == null || replaceDimValue.trim().length() == 0)
			{
				String s = replaceExprValue;
				if (s.startsWith("[") && s.endsWith("]"))
					s = s.substring(1, s.length()-1);
				if (s.indexOf('[') < 0)
				{
					String[] values = s.split("[.]");
					if (values.length == 2)
					{
						replaceExprValue = values[1];
						replaceDimValue = values[0];
					}
				}
			}
		}
		for (String s : reportPaths)
			this.reportPaths.add(s);
		for (String s : names)
			this.colNames.add(s);
	}

	private String decode(String s)
	{
		s = Util.replaceStr(s, "%27", "'");
		return s;
	}

	public void search()
	{
		if (path == null)
			return;
		File f = new File(spaceDirectory + File.separator + "catalog" + File.separator + path);
		List<File> fileList = new ArrayList<File>();
		getFiles(f, fileList, false);
		for (File cf : fileList)
		{
			AdhocReport ar = null;
			try
			{
				ar = AdhocReport.read(cf);
				for (AdhocEntity ae : ar.getEntities())
					processEntity(EntityType.Column, ae, ar);
				if (labelValue != null && labelValue.length() > 0)
					continue;
				for (AdhocSort as : ar.getSorts())
					processEntity(EntityType.Sort, as.getColumn(), ar);
				for (QueryFilter qf : ar.getFilters())
					processFilter(qf, ar);
				for (DisplayFilter df : ar.getDisplayFilters())
					processFilter(df, ar);
			} catch (Exception e)
			{
				logger.error(e);
				continue;
			}
		}
		if (outputFile != null)
		{
			try
			{
				BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile, false));
				writer.write("Report Name\tColumn Name\tExpression\tPath");
				writer.newLine();
				for (int i = 0; i < reportNames.size(); i++)
				{
					writer.write(reportNames.get(i) + "\t" + colNames.get(i) + "\t" + expressions.get(i) + "\t" + reportPaths.get(i));
					writer.newLine();
				}
				writer.close();
			} catch (IOException e)
			{
				logger.error(e);
				return;
			}
		}
	}

	private void processFilter(QueryFilter qf, AdhocReport ar)
	{
		if (!qf.searchFilter(dimValue, exprValue))
			return;
		colNames.add("Filter");
		try
		{
			expressions.add(qf.getNewQueryString(false));
		} catch (ScriptException e)
		{
			return;
		}
		reportNames.add(ar.getName());
		String rpath = ar.getPath();
		int pi = rpath.indexOf(path);
		rpath = rpath.substring(pi);
		reportPaths.add(rpath);
	}

	private void processFilter(DisplayFilter df, AdhocReport ar)
	{
		if (!df.searchFilter(dimValue, exprValue))
			return;
		colNames.add("Display Filter");
		expressions.add(df.getNewQueryString(false, false));
		reportNames.add(ar.getName());
		String rpath = ar.getPath();
		int pi = rpath.indexOf(path);
		rpath = rpath.substring(pi);
		reportPaths.add(rpath);
	}

	public int replace()
	{
		removeBackups();
		int count = 0;
		// Dedupe report paths (necessary so undo works across elements)
		Map<String, List<String>> pathMap = new HashMap<String, List<String>>();
		for (int i = 0; i < reportPaths.size(); i++)
		{
			List<String> colList = pathMap.get(reportPaths.get(i));
			if (colList == null)
			{
				colList = new ArrayList<String>();
				pathMap.put(reportPaths.get(i), colList);
			}
			colList.add(colNames.get(i));
		}
		for (Entry<String, List<String>> entry : pathMap.entrySet())
		{
			File cf = new File(spaceDirectory + File.separator + "catalog" + File.separator + entry.getKey() + ".AdhocReport");
			AdhocReport ar = null;
			try
			{
				boolean changed = false;
				ar = AdhocReport.read(cf);
				for (String colName : entry.getValue())
				{
					EntityType type = EntityType.Column;
					if (colName.equals("Sort"))
						type = EntityType.Sort;
					else if (colName.equals("Filter"))
						type = EntityType.Filter;
					else if (colName.equals("Display Filter"))
						type = EntityType.DisplayFilter;
					if (type == EntityType.Column)
					{
						for (AdhocEntity ae : ar.getEntities())
							changed |= replaceEntity(EntityType.Column, ae, ar);
					}
					if (labelValue != null && labelValue.length() > 0)
						continue;
					if (type == EntityType.Sort)
					{
						for (AdhocSort as : ar.getSorts())
							changed |= replaceEntity(EntityType.Sort, as.getColumn(), ar);
					} else if (type == EntityType.Filter)
					{
						for (QueryFilter qf : ar.getFilters())
							changed |= qf.replaceFilter(dimValue, exprValue, replaceDimValue, replaceExprValue);
					} else if (type == EntityType.DisplayFilter)
					{
						for (DisplayFilter df : ar.getDisplayFilters())
							changed |= df.replaceFilter(dimValue, exprValue, replaceDimValue, replaceExprValue);
					}
				}
				if (changed)
				{
					File backupf = new File(cf.getPath() + ".replacebackup");
					if (backupf.exists())
						backupf.delete();
					// Rename existing report to backup name
					cf.renameTo(backupf);
					// Save report
					ar.save(cf);
					/*
					 * Set modified times such that the backup is newer than the saved copy. This way, as long as this
					 * condition holds, we can restore the old version. If the current version is newer than the backup,
					 * we won't restore.
					 */
					cf.setLastModified(System.currentTimeMillis());
					backupf.setLastModified(System.currentTimeMillis() + 1000);
					count++;
				}
			} catch (Exception e)
			{
				logger.error(e);
				continue;
			}
		}
		return count;
	}

	private void removeBackups()
	{
		File f = new File(spaceDirectory + File.separator + "catalog" + File.separator + path);
		List<File> fileList = new ArrayList<File>();
		getFiles(f, fileList, true);
		for (File cf : fileList)
		{
			cf.delete();
		}
	}

	public int restore()
	{
		int count = 0;
		if (path == null)
			return 0;
		File f = new File(spaceDirectory + File.separator + "catalog" + File.separator + path);
		List<File> fileList = new ArrayList<File>();
		getFiles(f, fileList, false);
		for (File cf : fileList)
		{
			File backupf = new File(cf.getPath() + ".replacebackup");
			try
			{
				if (!backupf.exists())
					continue;
				if (backupf.lastModified() > cf.lastModified())
				{
					// Restore
					cf.delete();
					backupf.renameTo(cf);
					count++;
				}
			} catch (Exception e)
			{
				logger.error(e);
				continue;
			}
		}
		return count;
	}

	private void processEntity(EntityType type, AdhocEntity ae, AdhocReport ar)
	{
		if (ae instanceof AdhocColumn)
		{
			AdhocColumn ac = (AdhocColumn) ae;
			ColumnType ct = ac.getType();
			String dim = ac.getDimension();
			if (labelValue != null && labelValue.length() > 0)
			{
				AdhocTextEntity ate = ac.getLabelField();
				if (ate != null)
				{
					String label = ate.getLabel();
					if (label.toLowerCase().indexOf(lowerLabelValue) >= 0)
					{
						expressions.add(label);
						colNames.add("Label");
					} else
						return;
				} else
					return;
			} else if (ct == ColumnType.Dimension)
			{
				if (dimValue != null && dimValue.length() > 0 && dim.toLowerCase().indexOf(lowerDimValue) < 0)
					return;
				String col = ac.getColumn();
				if (exprValue != null && exprValue.length() > 0 && col.toLowerCase().indexOf(lowerExprValue) < 0)
					return;
				expressions.add("[" + dim + "." + col + "]");
				colNames.add(type == EntityType.Sort ? "Sort" : ac.getDisplayName());
			} else if (ct == ColumnType.Measure)
			{
				if (dimValue != null && dimValue.length() > 0)
					return;
				String col = ac.getColumn();
				if (exprValue != null && exprValue.length() > 0 && col.toLowerCase().indexOf(lowerExprValue) < 0)
					return;
				expressions.add("[" + ac.getColumn() + "]");
				colNames.add(type == EntityType.Sort ? "Sort" : ac.getDisplayName());
			} else if (ct == ColumnType.Expression)
			{
				if (dimValue != null && dimValue.length() > 0)
					return;
				String expr = ac.getExpression().getExpression();
				if (exprValue != null && exprValue.length() > 0 && expr.toLowerCase().indexOf(lowerExprValue) < 0)
					return;
				expressions.add(expr);
				if (type == EntityType.Sort)
					colNames.add("Sort");
				else
				{
					String name = ac.getName();
					colNames.add(name);
				}
			}
			reportNames.add(ar.getName());
			String rpath = ar.getPath();
			int pi = rpath.indexOf(path);
			rpath = rpath.substring(pi);
			reportPaths.add(rpath);
		}
	}

	private boolean replaceEntity(EntityType type, AdhocEntity ae, AdhocReport ar)
	{
		if (ae instanceof AdhocColumn)
		{
			AdhocColumn ac = (AdhocColumn) ae;
			ColumnType ct = ac.getType();
			String dim = ac.getDimension();
			if (labelValue != null && labelValue.length() > 0)
			{
				AdhocTextEntity ate = ac.getLabelField();
				if (ate != null)
				{
					String label = ate.getLabel();
					if (label.toLowerCase().indexOf(lowerLabelValue) >= 0)
					{
						ate.setLabel(Util.replaceStrIgnoreCase(label, labelValue, replaceLabelValue));
						return true;
					} else
						return false;
				} else
					return false;
			}
			if (ct == ColumnType.Dimension)
			{
				if (dimValue != null && dimValue.length() > 0 && dim.toLowerCase().indexOf(lowerDimValue) < 0)
					return false;
				String col = ac.getColumn();
				if (exprValue != null && exprValue.length() > 0 && col.toLowerCase().indexOf(lowerExprValue) < 0)
					return false;
				if (dimValue != null && dimValue.length() > 0)
					ac.setDimension(Util.replaceStrIgnoreCase(dim, dimValue, replaceDimValue));
				if (exprValue != null && exprValue.length() > 0)
				{
					ac.setColumn(Util.replaceStrIgnoreCase(col, exprValue, replaceExprValue));
					ac.setOriginalColumn(ac.getColumn());
					ac.setName(ac.getColumn());
				}
				return true;
			} else if (ct == ColumnType.Measure)
			{
				if (dimValue != null && dimValue.length() > 0)
					return false;
				String col = ac.getColumn();
				if (exprValue != null && exprValue.length() > 0 && col.toLowerCase().indexOf(lowerExprValue) < 0)
					return false;
				if (exprValue != null && exprValue.length() > 0)
				{
					ac.setColumn(Util.replaceStrIgnoreCase(col, exprValue, replaceExprValue));
					ac.setOriginalColumn(ac.getColumn());
					ac.setName(ac.getColumn());
				}
				return true;
			} else if (ct == ColumnType.Expression)
			{
				if (dimValue != null && dimValue.length() > 0)
					return false;
				String expr = ac.getExpression().getExpression();
				if (exprValue != null && exprValue.length() > 0 && expr.toLowerCase().indexOf(lowerExprValue) < 0)
					return false;
				ac.getExpression().setExpression(Util.replaceStrIgnoreCase(expr, exprValue, replaceExprValue));
				return true;
			}
		}
		return false;
	}

	private void getFiles(File f, List<File> fileList, boolean backups)
	{
		if (f.isFile())
		{
			if (!f.getName().endsWith(".AdhocReport" + (backups ? ".replacebackup" : "")))
				return;
			if (p == null)
			{
				fileList.add(f);
			} else
			{
				String fname = f.getName();
				int i = fname.lastIndexOf('.');
				// Get rid of extension
				if (i > 0)
					fname = fname.substring(0, i);
				Matcher m = p.matcher(fname);
				if (m.find())
				{
					fileList.add(f);
				}
			}
			return;
		}
		if (f.getName().equals("styles"))
			return;
		File[] files = f.listFiles();
		if (files == null)
			return;
		for (File childf : files)
			getFiles(childf, fileList, backups);
	}

	public String[] getReportNames()
	{
		String[] result = new String[reportNames.size()];
		reportNames.toArray(result);
		return result;
	}

	public String[] getReportPaths()
	{
		String[] result = new String[reportPaths.size()];
		reportPaths.toArray(result);
		return result;
	}

	public String[] getExpressions()
	{
		String[] result = new String[expressions.size()];
		expressions.toArray(result);
		return result;
	}

	public String[] getColNames()
	{
		String[] result = new String[colNames.size()];
		colNames.toArray(result);
		return result;
	}
}
