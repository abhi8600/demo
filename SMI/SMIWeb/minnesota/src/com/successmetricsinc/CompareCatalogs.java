package com.successmetricsinc;

import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import org.apache.log4j.Logger;

import com.successmetricsinc.WebServices.trustedservice.CatalogCompareResult;
import com.successmetricsinc.util.Util;

public class CompareCatalogs
{
	private static Logger logger = Logger.getLogger(CompareCatalogs.class);
	private String spaceDirectory;
	private String spaceDirectoryCompare;
	private List<String> itemNames = new ArrayList<String>();
	private List<String> itemPaths = new ArrayList<String>();
	private List<String> itemTypes = new ArrayList<String>();
	private List<String> differences = new ArrayList<String>();
	private String rootDir;
	private String compareRootDir;
	private SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public CompareCatalogs(String spaceDirectory, String spaceDirectoryCompare)
	{
		this.spaceDirectory = spaceDirectory;
		this.spaceDirectoryCompare = spaceDirectoryCompare;
		rootDir = this.spaceDirectory + File.separator + "catalog" + File.separator + "shared";
		compareRootDir = this.spaceDirectoryCompare + File.separator + "catalog" + File.separator + "shared";
	}

	public void compare()
	{
		File f = new File(rootDir);
		File cf = new File(compareRootDir);
		TreeSet<String> pathSet = new TreeSet<String>();
		getFiles(f, rootDir.length(), pathSet);
		TreeSet<String> comparePathSet = new TreeSet<String>();
		getFiles(cf, compareRootDir.length(), comparePathSet);
		/*
		 * Look for files that are in the base bot not in the compare
		 */
		for (String s : pathSet)
		{
			String fileName = s.replace(File.separator, "/");
			int index = fileName.indexOf('.');
			String suffix = null;
			String baseName = fileName;
			if (index > 0)
			{
				suffix = fileName.substring(index + 1);
				baseName = fileName.substring(0, index);
			}
			if (!comparePathSet.contains(s))
			{
				if (suffix.equals("AdhocReport"))
				{
					itemNames.add(baseName);
					itemPaths.add(s);
					itemTypes.add("Report");
					differences.add(getBaseOnlyExists());
				} else if (suffix.equals("Page"))
				{
					itemNames.add(baseName);
					itemPaths.add(s);
					itemTypes.add("Page");
					differences.add(getBaseOnlyExists());
				}
			}
		}
		/*
		 * Now go through all compare files
		 */
		for (String s : comparePathSet)
		{
			String fileName = s.replace(File.separator, "/");
			int index = fileName.indexOf('.');
			String suffix = null;
			String baseName = fileName;
			if (index > 0)
			{
				suffix = fileName.substring(index + 1);
				baseName = fileName.substring(0, index);
			}
			if (!pathSet.contains(s))
			{
				/*
				 * Files not in base
				 */
				if (suffix.equals("AdhocReport"))
				{
					itemNames.add(baseName);
					itemPaths.add(s);
					itemTypes.add("Report");
					differences.add(getCompareOnlyExists());
				} else if (suffix.equals("Page"))
				{
					itemNames.add(baseName);
					itemPaths.add(s);
					itemTypes.add("Page");
					differences.add(getCompareOnlyExists());
				}
			} else
			{
				File basef = new File(rootDir + File.separator + s);
				File comparef = new File(compareRootDir + File.separator + s);
				boolean same = true;
				if (basef.length() != comparef.length())
					same = false;
				if (same && !fileCompare(basef, comparef))
					same = false;
				if (!same)
				{
					if (suffix.equals("AdhocReport"))
					{
						itemNames.add(baseName);
						itemPaths.add(s);
						itemTypes.add("Report");
						differences.add("Difference|Mod: " + sdf.format(basef.lastModified()) + "|Mod: " + sdf.format(comparef.lastModified()));
					} else if (suffix.equals("Page"))
					{
						itemNames.add(baseName);
						itemPaths.add(s);
						itemTypes.add("Page");
						differences.add("Difference|Mod: " + sdf.format(basef.lastModified()) + "|Mod: " + sdf.format(comparef.lastModified()));
					}
				}
			}
		}
	}

	private boolean fileCompare(File f1, File f2)
	{
		if (f1.length() != f2.length())
			return false;
		byte[] arr1 = new byte[(int) f1.length()];
		byte[] arr2 = new byte[(int) f2.length()];
		try
		{
			FileInputStream fis1 = new FileInputStream(f1);
			FileInputStream fis2 = new FileInputStream(f2);
			fis1.read(arr1);
			fis2.read(arr2);
			fis1.close();
			fis2.close();
			for (int i = 0; i < arr1.length; i++)
			{
				if (arr1[i] != arr2[i])
					return false;
			}
		} catch (Exception e)
		{
			logger.error(e);
			return false;
		}
		return true;
	}

	private boolean isDiffFile(String filename)
	{
		if (filename.endsWith(".AdhocReport"))
			return true;
		return false;
	}

	private void getFiles(File f, int prefixLength, TreeSet<String> pathSet)
	{
		if (f.isFile())
		{
			String fname = f.getPath().substring(prefixLength);
			if (isDiffFile(fname))
				pathSet.add(fname);
			return;
		}
		if (f.getName().equals("styles"))
			return;
		File[] files = f.listFiles();
		if (files == null)
			return;
		for (File childf : files)
			getFiles(childf, prefixLength, pathSet);
	}

	public void setCatalogCompareRestul(CatalogCompareResult ccr)
	{
		String[] arr = new String[itemNames.size()];
		itemNames.toArray(arr);
		ccr.setItemNames(arr);
		arr = new String[itemPaths.size()];
		itemPaths.toArray(arr);
		ccr.setItemPaths(arr);
		arr = new String[itemTypes.size()];
		itemTypes.toArray(arr);
		ccr.setItemTypes(arr);
		arr = new String[differences.size()];
		differences.toArray(arr);
		ccr.setDifferences(arr);
	}

	public static String getBaseOnlyExists()
	{
		return "Object|Exists|Does not exist";
	}

	public static String getCompareOnlyExists()
	{
		return "Object|Does not exist|Exists";
	}

	public void pull(String[] paths)
	{
		for (int i = 0; i < paths.length; i++)
		{
			File f = new File(rootDir + File.separator + paths[i]);
			File cf = new File(compareRootDir + File.separator + paths[i]);
			try
			{
				if (f.exists())
					f.delete();
				if (cf.exists())
				{
					Util.copyFile(cf, f);
				}
			} catch (Exception e)
			{
				logger.error(e);
			}
		}
	}
}
