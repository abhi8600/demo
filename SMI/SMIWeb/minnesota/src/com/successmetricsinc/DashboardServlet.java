/**
 * Copyright (C) 2009-2013 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMAttribute;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.util.AXIOMUtil;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.WebServices.dashboard.DashboardMongoDBCache;
import com.successmetricsinc.WebServices.dashboard.DashboardWebService;
import com.successmetricsinc.WebServices.dashboard.DashboardWebServiceHelper;
import com.successmetricsinc.annotations.rest.GET;
import com.successmetricsinc.annotations.rest.POST;
import com.successmetricsinc.annotations.rest.Path;
import com.successmetricsinc.annotations.rest.PathParam;
import com.successmetricsinc.annotations.rest.QueryParam;
import com.successmetricsinc.catalog.CatalogAccessException;
import com.successmetricsinc.catalog.CatalogManager;
import com.successmetricsinc.catalog.CatalogPermission;
import com.successmetricsinc.security.SecurityUtil;
import com.successmetricsinc.util.SwapSpaceInProgressException;
import com.successmetricsinc.util.XmlUtils;

public class DashboardServlet extends HttpServlet
{
	private static Logger logger = Logger.getLogger(DashboardServlet.class);
	private static final long serialVersionUID = 1L;
	
	private static final String URI_SET_DASHLET_TITLE = "/SMIWeb/Dashboards/dashlet/setTitle";
	//private static final String URI_SET_LAYOUT = "/SMIWeb/Dashboards/page/setLayout";
	private static final String URI_PAGE = "/SMIWeb/Dashboards/page";
	
	private static final String URI_SEARCH_REPORT = "/SMIWeb/Dashboards/reports/search";
	private static final String URI_SEARCH_REPORT_AGG = "/SMIWeb/Dashboards/reports/searchAggregate";
	private static final String ES_SERVER_URL = "ES_Url";
	
	//Cache rest methods
	private static HashMap<String, Method> postMethods = new HashMap<String, Method>();
	private static HashMap<String, Method> getMethods = new HashMap<String, Method>();
	
	private static HashMap<String, HashMap<String, Resource>> resourceMap = new HashMap<String, HashMap<String, Resource>>();
	
	static{
		Method[] ms = DashboardServlet.class.getDeclaredMethods();
		for(Method m: ms){
			boolean isGet = m.isAnnotationPresent(GET.class);
			boolean isPost = m.isAnnotationPresent(POST.class);
			if( isGet || isPost){
				Path p = m.getAnnotation(Path.class);
				if(p != null){
					String path = p.value();
					if(path != null){
						if(isGet){
							getMethods.put(path, m);
						}else if (isPost){
							postMethods.put(path, m);
						}
					}
				}
			}
		}
	}
	
	private SMIWebUserBean passSecurityCheck(HttpServletRequest request, HttpServletResponse response) throws IOException{
		HttpSession session = request.getSession(false);
		boolean passSession = false;
		if (session == null || session.isNew()) {
			String sessionId = request.getHeader("X-BirstSessionId");
			if (sessionId == null || sessionId.trim().length() == 0) {
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
				return null;
			}
			ServletContext sc = this.getServletContext();
			session = (HttpSession)sc.getAttribute(sessionId);
			passSession = true;
		}
		if (session == null)
		{
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
			return null;
		}
		if (!SecurityUtil.validateCSRF(request, passSession ? session : null))
		{
			response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "CSRF Failure");
			return null;
		}
		
		SMIWebUserBean userBean = (SMIWebUserBean) session.getAttribute("userBean");
		if (userBean == null)
		{
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
			return null;
		}
		
		return userBean;
		
	}
	
	private class Resource{
		private String regexUri;
		private HashMap<String,Integer> pathParamIdxKey = new HashMap<String,Integer>();
		private HashMap<String, Integer> queryParamIdxKey  = new HashMap<String, Integer>();
		private Method method;
	}
	
	/*
	@SuppressWarnings("unchecked")
	public void init(ServletConfig config) throws ServletException{
		super.init(config);
		
		String restClz = config.getInitParameter("restClz");
		if(restClz != null){
			try {
				Class rc = Class.forName(restClz);
				logger.debug("restClz class = " + rc.getName());
				Path clzPath = (Path)rc.getAnnotation(Path.class);
				String baseUri = clzPath.value();
				if(baseUri != null){
					Method[] methods = rc.getDeclaredMethods();
					for(Method m: methods){
						HashMap<String, Method> methodHash = null;
						String path = null;
						logger.debug("restClz method: " + m.getName());
						Annotation[] mAnons = m.getAnnotations();
						for(Annotation mA : mAnons){
							if (mA instanceof GET){
								logger.debug("\tGET");
								methodHash = getMethods;
							}else if (mA instanceof POST){
								methodHash = postMethods;
								logger.debug("\tPOST");
							}else if (mA instanceof Path){
								path = ((Path)mA).value();
								logger.debug("\tPATH = " + ((Path)mA).value());
							}
						}
						
						Resource uriList = new Resource(); 
						Annotation[][] paramAnons = m.getParameterAnnotations();
						for(int i = 0; i < paramAnons.length; i++){
							Annotation[] pAnon = paramAnons[i];
							logger.debug("param " + i + " # anons = " + pAnon.length);
							for(int y = 0; y < pAnon.length; y++){
								Annotation parameterAnon = pAnon[y];
								if(parameterAnon instanceof PathParam){
									String key = ((PathParam)parameterAnon).value();
									uriList.pathParamIdxKey.put(key, i);
									logger.debug("\tParameter = " + key);
								}else if (parameterAnon instanceof QueryParam){
									String key =  ((QueryParam)parameterAnon).value();
									logger.debug("\tQueryParam =  " + key);
									uriList.queryParamIdxKey.put(key, i);
								}
							}
						}
						
						if(!uriList.pathParamIdxKey.isEmpty()){
							
						}
						
						if(methodHash != null && path != null){
							methodHash.put(path, m);
						}
					}
				}
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	*/
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		doGet(request, response);
	}

	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		SMIWebUserBean ub = passSecurityCheck(request, response);
		if(ub == null){
			return;
		}
		
		response.setCharacterEncoding("UTF-8");
		
		String uri = request.getRequestURI();
		String method = request.getMethod();
		
		Method m = null;
		if(method.equals("POST")){
			m = postMethods.get(uri);
		}else if (method.equals("GET")){
			m = getMethods.get(uri);
		}
		
		//No such uri mapping
		if(m == null){
			logger.error("invalid uri " + uri);
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		
		try {
			m.invoke(this, request, response, ub);
		} catch (IllegalAccessException e) {
			logMethodException(e, response);
		} catch (IllegalArgumentException e) {
			logMethodException(e, response);
		} catch (InvocationTargetException e) {
			logMethodException(e, response);
		}
		
	}
	
	private void logMethodException(Exception e, HttpServletResponse response) throws IOException{
		logger.error(e);
		response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
	}
	
	private String getESUrl(){
		String serverUrl = System.getProperty(ES_SERVER_URL);
		if(serverUrl == null){
			serverUrl = System.getenv(ES_SERVER_URL);
			if(serverUrl == null){
				serverUrl = "http://localhost:9200";
			}
		}
		
		return serverUrl;
	}

	private String getESSearchUri(SMIWebUserBean ub){
		String spaceId = ub.getRepository().getApplicationIdentifier();
		return getESUrl() + "/birst_" + spaceId + "/adhocReport,viz_dashlet/_search";
	}
	
	private String getESCreateIndexUri(SMIWebUserBean ub){
		String spaceId = ub.getRepository().getApplicationIdentifier();
		return getESUrl() + "/_spaceIndexer/" + spaceId;
	}
	
	private void logSearchError(Exception e, String msg, HttpServletResponse response) throws IOException{
		logger.error(msg, e);
		response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
	}
	
	@SuppressWarnings({"unused"})
	@GET
	@Path(DashboardServlet.URI_SEARCH_REPORT)
	private void searchReports(HttpServletRequest request, HttpServletResponse response, SMIWebUserBean ub) throws HttpException, IOException{
		String query = request.getParameter("q");

		if(query != null){
			String esSearchUrl = getESSearchUri(ub);
			String queryUrl = esSearchUrl + "?size=10000&q=" + query;
			try{
				HttpClient client = new HttpClient();
				client.getParams().setSoTimeout(2*60000);
				
				GetMethod get = new GetMethod(queryUrl);
				
				int rc = client.executeMethod(get);
				String searchResponse = get.getResponseBodyAsString();
				if(rc == 200){
					response.getWriter().write(searchResponse);
					return;
				}else if (rc == 404){
					if(searchResponse.indexOf("IndexMissingException") >= 0){
						int createRc = createIndexOnSpace(client, ub);
						if(createRc == 200){
							rc = client.executeMethod(get);
							searchResponse = get.getResponseBodyAsString();
							if(rc == 200){
								response.getWriter().write(searchResponse);
								return;
							}else{
								response.getWriter().write(searchResponse);
								response.sendError(rc);
								return;
							}
						}else{
							
						}
					}
				}else{
					response.getWriter().write(searchResponse);
					response.sendError(rc);
					return;
				}
			}catch(HttpException ex){
				logSearchError(ex, queryUrl, response);
			}catch(IOException ex){
				logSearchError(ex, queryUrl, response);
			}
		}
	}
	
	@SuppressWarnings("unused")
	@GET
	@Path(DashboardServlet.URI_SEARCH_REPORT_AGG)
	private void searchReportsAggregation(HttpServletRequest request, HttpServletResponse response, SMIWebUserBean ub) throws HttpException, IOException, JSONException{
		String query = request.getParameter("q");

		if(query != null){
			String esSearchUrl = getESSearchUri(ub);
			String queryUrl = esSearchUrl;
			try{
				HttpClient client = new HttpClient();
				client.getParams().setSoTimeout(2*60000);
				
				JSONObject queryDsl = getESQueryDSL(query, 0, 10000);
				String queryStr = queryDsl.toString();
				StringRequestEntity entity = new StringRequestEntity(queryStr, "application/json", "UTF-8");
				
				HttpGetWithEntity get = new HttpGetWithEntity(queryUrl);
				get.setRequestEntity(entity);
				
				int rc = client.executeMethod(get);
				String searchResponse = get.getResponseBodyAsString();
				logger.debug("searchResult: " + searchResponse);
				if(rc == 200){
					response.getWriter().write(searchResponse);
					response.flushBuffer();
					return;
				}else if (rc == 404){
					if(searchResponse.indexOf("IndexMissingException") >= 0){
						int createRc = createIndexOnSpace(client, ub);
						if(createRc == 200){
							rc = client.executeMethod(get);
							searchResponse = get.getResponseBodyAsString();
							if(rc == 200){
								response.getWriter().write(searchResponse);
								return;
							}else{
								response.getWriter().write(searchResponse);
								response.sendError(rc);
								return;
							}
						}else{
							
						}
					}
				}else{
					response.getWriter().write(searchResponse);
					response.sendError(rc);
					return;
				}
			}catch(HttpException ex){
				logSearchError(ex, queryUrl, response);
			}catch(IOException ex){
				logSearchError(ex, queryUrl, response);
			}
		}
	}
	
	private JSONObject getESQueryDSL(String queryStr, int from, int size) throws JSONException{
		JSONObject root = new JSONObject();
		String[] stringFields = new String[] { 
				"creator", 
				"dimensions", 
				"measures", 
				"charts", 
				"expressions",
				"dir"
		};
		
		JSONArray fields = new JSONArray(stringFields);
		fields.put("filename");
		fields.put("lastModified");
		root.put("fields", fields).
		put("from", from).
		put("size", size);
		
		JSONObject query = new JSONObject();
		query.put("query", queryStr);
		
		JSONObject queryString = new JSONObject();
		queryString.put("query_string", query);
		
		appendQueryFields(query, fields);
		
		root.put("query", 
				(new JSONObject()).put("filtered", 
					(new JSONObject().put("query", queryString)
		)));
								
		JSONObject agg = new JSONObject();
		root.put("aggregations", agg);
		appendAggTerms(agg, stringFields);
		appendAggDateRange(agg);
		
		JSONObject highlightFields = new JSONObject();
		root.put("highlight", (new JSONObject()).put("fields", highlightFields));
		appendHighlights(highlightFields, fields);
		return root;
	}
	
	private void appendQueryFields(JSONObject queryString, JSONArray fieldTerms) throws JSONException{
		JSONArray fields = new JSONArray();
		for(int i = 0; i < fieldTerms.length(); i++){
			String term = fieldTerms.getString(i);
			if(term.equals("filename")){
				term = "filename^10";
			}else if (term.equals("dir")){
				term = "dir^5";
			}else if (term.equals("lastModified")){
				continue;
			}
			fields.put(term);
		}
		queryString.put("fields", fields);
	}
	
	private void appendHighlights(JSONObject highlight, JSONArray terms) throws JSONException{
		for(int i = 0; i < terms.length(); i++){
			highlight.put(terms.getString(i), new JSONObject());
		}
	}
	
	private void appendAggTerms(JSONObject aggregations, String[] terms) throws JSONException{
		for( int i = 0; i < terms.length; i++){
			String term = terms[i];
			
			JSONObject aggParams = new JSONObject();
			aggParams.put("field", term + ".raw");
			aggParams.put("size", 30);
			
			JSONObject agg = new JSONObject();
			agg.put("terms", aggParams);
			aggregations.put(term, agg);
		}
	}
	
	private void appendAggDateRange(JSONObject aggregations) throws JSONException {
		JSONObject range = new JSONObject();
		aggregations.put("range", range);
		JSONObject dateRange = new JSONObject();
		range.put("date_range", dateRange);
		dateRange.put("field", "lastModified");
		dateRange.put("format", "MM-dd-yyyy");
		
		JSONArray dateRanges = new JSONArray();
		dateRange.put("ranges", dateRanges);
		
		Date current = new Date();
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		String midnight = fmt.format(current);
		midnight = midnight + "T00:00:00";
		
		appendDateRange(dateRanges, midnight, "now");
		appendDateRange(dateRanges, "now-7d", "now");
		appendDateRange(dateRanges, "now-1M", "now");
		appendDateRange(dateRanges, "now-1y", "now");
		appendDateRange(dateRanges, null, "now-1y");
	}
	
	private void appendDateRange(JSONArray ranges, String from, String to) throws JSONException{
		JSONObject fromTo = new JSONObject();
		if(from != null){
			fromTo.put("from", from);
		}
		
		if(to != null){
			fromTo.put("to", to);
		}
		
		ranges.put(fromTo);
	}
	
	private int createIndexOnSpace(HttpClient client, SMIWebUserBean ub) throws HttpException, IOException{
		String createIndexUrl = getESCreateIndexUri(ub);
		PostMethod post = new PostMethod(createIndexUrl);
		int rc = client.executeMethod(post);
		return rc;
	}
	
	@SuppressWarnings({ "unused"})
	@POST
	@Path(DashboardServlet.URI_SET_DASHLET_TITLE)
	private void setDashletTitle(HttpServletRequest request, HttpServletResponse response, SMIWebUserBean ub) throws IOException{
		String pageDir = request.getParameter("pageDir");
		String pageName = request.getParameter("pageName");
		String dashletReport = request.getParameter("dashletReport");
		String newTitle = request.getParameter("newTitle");
		
		if(pageDir == null || dashletReport == null || newTitle == null || pageName == null){
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}
		
		if(!ub.isSuperUserOwnerOrAdmin() && !ub.hasEditDashboardPermission()){
			response.sendError(HttpServletResponse.SC_FORBIDDEN);
			return;
		}
		
		CatalogManager cm = ub.getCatalogManager();
		if (! cm.getPermission(pageDir).canModify(ub.getUserGroups(), ub.isSuperUserOwnerOrAdmin())){
			response.sendError(HttpServletResponse.SC_FORBIDDEN);
			return;
		}
		
		try {
			String pagePath = getPagePath(pageDir, pageName, cm);
			OMElement pageXml = DashboardWebServiceHelper.readXmlFile(pagePath);
			OMElement layout = pageXml.getFirstChildWithName(new QName("Layout"));
			if(layout != null){
				OMElement dashlet = findDashletByPath(layout, dashletReport);
				if(dashlet != null){
					OMElement title = dashlet.getFirstChildWithName(new QName("Title"));
					if(title != null){
						title.setText(newTitle);
						DashboardWebServiceHelper.writeXmlFile(pagePath, pageXml);
						DashboardMongoDBCache.createMarkerFile(ub);
					}
				}
			}
		} catch (XMLStreamException e) {
			logger.error(e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return;
		}
		
	}
	
	private String getPagePath(String pageDir, String pageName, CatalogManager cm){
		return cm.getCatalogRootPath() + File.separator + 
				pageDir.replace("/", File.separator) + File.separator + 
				pageName.replace("/", File.separator) + 
				(pageName.endsWith(".page") ? "" : ".page");
	}
	
	private OMElement findDashletByPath(OMElement layout, String dashletPath){
		if(layout != null){
			OMElement dashlet = layout.getFirstChildWithName(new QName("dashlet"));
			if(dashlet != null){
				OMElement path = dashlet.getFirstChildWithName(new QName("Path"));
				if(path != null){
					String reportPath = path.getText();
					if(reportPath != null && reportPath.equals(dashletPath)){
						return dashlet;
					}
				}
			}else{
				OMElement layoutChild = layout.getFirstChildWithName(new QName("Layout"));
				if(layoutChild != null){
					return findDashletByPath(layoutChild, dashletPath);
				}
			}
			OMElement next = (OMElement)layout.getNextOMSibling();
			if(next != null){
				return findDashletByPath(next, dashletPath);
			}
		}
		
		return null;
	}
	
	/*
	@SuppressWarnings("unused")
	@POST
	@Path(DashboardServlet.URI_SET_LAYOUT)
	private void setPageLayout(HttpServletRequest request, HttpServletResponse response, SMIWebUserBean ub) throws IOException{
		String pageDir = request.getParameter("pageDir");
		String pageName = request.getParameter("pageName");
		String layoutJson = request.getParameter("layout");
		
		if(pageDir == null || pageName == null || layoutJson == null){
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}
		
		if(!ub.isSuperUserOwnerOrAdmin() && !ub.hasEditDashboardPermission()){
			response.sendError(HttpServletResponse.SC_FORBIDDEN);
			return;
		}
		
		CatalogManager cm = ub.getCatalogManager();
		if (! cm.getPermission(pageDir).canModify(ub.getUserGroups(), ub.isSuperUserOwnerOrAdmin())){
			response.sendError(HttpServletResponse.SC_FORBIDDEN);
			return;
		}
		
		try {
			JSONObject obj = new JSONObject(layoutJson);
			String xmlStr = XML.toString(obj);
			OMElement om = AXIOMUtil.stringToOM(xmlStr);
			
			//Quick check
			String tag = om.getLocalName();
			if(!tag.equals("Layout")){
				response.sendError(HttpServletResponse.SC_BAD_REQUEST);
				response.getWriter().write("Invalid xml, expecting <Layout>...</Layout>");
				return;
			}
			
			//Replace or add layout to page
			String pagePath = getPagePath(pageDir, pageName, cm);
			OMElement pageXml = DashboardWebServiceHelper.readXmlFile(pagePath);
			OMElement layout = pageXml.getFirstChildWithName(new QName("Layout"));
			if(layout != null){
				layout.insertSiblingAfter(om);
				layout.detach();
			}else{
				pageXml.addChild(om);
			}
			
			//Write back
			DashboardWebServiceHelper.writeXmlFile(pagePath, pageXml);
			DashboardMongoDBCache.createMarkerFile(ub);
			
		} catch (JSONException e) {
			logger.error(e);
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().write("Cannot parse invalid json");
			return;
		} catch (XMLStreamException e) {
			logger.error(e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return;
		}
		
	}*/
	
	@SuppressWarnings("unused")
	@GET
	@Path(URI_PAGE)
	private void getPage(HttpServletRequest request, HttpServletResponse response, SMIWebUserBean ub) throws IOException{
		String uuid = request.getParameter("uuid");
		
		if(uuid == null){
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}
		
		if(!ub.isSuperUserOwnerOrAdmin() && !ub.hasEditDashboardPermission()){
			response.sendError(HttpServletResponse.SC_FORBIDDEN);
			return;
		}
		
		if(DashboardMongoDBCache.useMongoDB()){
			try{
				String jsonPage = DashboardMongoDBCache.readPageInJson(ub, uuid);
				response.getWriter().write(jsonPage);
				return;
			}catch(Exception e){
				logger.error(e);
			}
		}else{
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
	}
	
	@SuppressWarnings("unused")
	@POST
	@Path(URI_PAGE)
	private void setPage(HttpServletRequest request, HttpServletResponse response, SMIWebUserBean ub) throws IOException{
		String pageData = request.getParameter("page");
		String dashboardOrder = request.getParameter("dashboardOrder");
		
		if(pageData == null){
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}
		
		if(!ub.isSuperUserOwnerOrAdmin() && !ub.hasEditDashboardPermission()){
			response.sendError(HttpServletResponse.SC_FORBIDDEN);
			return;
		}
		
		BasicDBObject pageObj = (BasicDBObject)com.mongodb.util.JSON.parse(pageData);
		
		if(pageObj == null){
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}
		
		String dir = pageObj.getString("@dir");
		String name = pageObj.getString("@name");
		
		if(dir == null || name == null){
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}
		
		String uuid = null;
		try{
			uuid = DashboardWebServiceHelper.addUuidAttrIfNotExists(pageObj);
			OMElement pageEl = DashboardMongoDBCache.pageElementFromDBObject(pageObj);
			
			BasicDBList  layoutV2 = (BasicDBList)pageObj.get("LayoutV2");
			if(layoutV2 != null){
				DashboardMongoDBCache.writeLayoutMetadata(uuid, layoutV2);
				pageObj.removeField("LayoutV2");
			}
			
			writePage(dir, pageEl, name, dashboardOrder, ub.getCatalogManager(), ub);
		}catch(Exception ex){
			logger.error(ex);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return;
		}
		
		response.setContentType("application/json");
		response.getWriter().write("{ \"uuid\" : \"" + uuid + "\" }");
	}
	
	//Taken and adapted from DashboardWebService
	private void writePage(String dir, OMElement page, String file, String order, CatalogManager cm, SMIWebUserBean ub) throws CatalogAccessException, SwapSpaceInProgressException, XMLStreamException, IOException{
		if(dir == null || dir.isEmpty()){
			throw new IllegalArgumentException("Invalid directory");
		}
		
		CatalogManager.checkForSwapSpaceOperation(cm);
		
		String dashboarddir = cm.getCatalogRootPath() + File.separator + convertFileSeparator(dir);
		if(dashboarddir.indexOf("private") != -1){
			dashboarddir = cm.addUserFolderToPrivate(dashboarddir);
		}
		File dbdir = new File(dashboarddir);
		if(!dbdir.exists()){	
			if(!cm.createFolder(dashboarddir)){
				throw new CatalogAccessException("Cannot create dashboard directory");
			}
		}
		
		CatalogPermission perm = cm.getPermission(dashboarddir);
		if(!perm.canModify(ub.getUserGroups(), ub.isSuperUserOwnerOrAdmin())){
			throw new CatalogAccessException("Cannot modify directory");
		}
		
		boolean newDashboardFolder = false;
		
		//Write the dashboards.xml if it doesn't exist to indicate this as a dashboard directory
		String dashboardXml = dashboarddir + File.separator + DashboardWebService.DASH_XML_FILE;
		File indicator = new File(dashboardXml);
		if(!indicator.exists()){
			if(!indicator.createNewFile()){
				throw new CatalogAccessException("Cannot convert directory to dashboard directory");
			}
			
			int orderNum = (order != null && !order.isEmpty()) ? Integer.parseInt(order) : 9999;
			writeDashboardOrderCreatedBy(dashboardXml, orderNum, ub.getUserName());
			newDashboardFolder = true;
		}
		
		//Write the Page xml
		String filepath = dashboarddir + File.separator + file;
		if(!file.endsWith(DashboardWebService.DOT_PAGE)){
			filepath += DashboardWebService.DOT_PAGE;
		}
		
		//Add creator of the page
		if(!(new File(filepath)).exists()){
			XmlUtils.addAttribute(OMAbstractFactory.getOMFactory(), page, DashboardWebService.CREATED_BY, ub.getUserName(), null);
		}
		
		DashboardWebServiceHelper.writeXmlFile(filepath, page);
		
		//Write to Mongo
		if(DashboardMongoDBCache.useMongoDB()){
			DashboardMongoDBCache.upsertPage(ub, new File(filepath));
		
			//Re-cache the dashboard folders order
			if(newDashboardFolder){
				DashboardMongoDBCache.regenerateDashboardsOrdersPaths(ub);
			}
		}
	}
	
	//Taken and adapted from DashboardWebService
	private String convertFileSeparator(String path){
		return path.replace('/', File.separatorChar);
	}
	
	//Taken and adapted from DashboardWebService
	private void writeDashboardOrderCreatedBy(String file, int index, String createdBy) throws XMLStreamException, IOException{
		OMElement pageRoot = null;
		
		//Write directly if the file is empty..
		File target = new File(file);
		if(target.exists() && target.length() == 0){
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMNamespace ns = null;
			pageRoot = factory.createOMElement("Configuration", ns);
		}else{
			pageRoot = DashboardWebServiceHelper.readXmlFile(file);
		}
		OMElement orderEl = pageRoot.getFirstChildWithName(new QName("", "Order"));	
		if(orderEl == null){
			XmlUtils.addContent(OMAbstractFactory.getOMFactory(), pageRoot, "Order", index, null);
		}else{
			orderEl.setText(Integer.toString(index));
		}
		
		//User who first created the dashboard
		if(createdBy != null){
			XmlUtils.addAttribute(OMAbstractFactory.getOMFactory(), pageRoot, "createdBy", createdBy, null);
		}
		
		DashboardWebServiceHelper.writeXmlFile(file, pageRoot);
	}
	
	public class HttpGetWithEntity extends org.apache.commons.httpclient.methods.PostMethod{
		public final static String METHOD_NAME = "GET";

		public HttpGetWithEntity(String queryUrl) {
			super(queryUrl);
		}

		@Override
		public String getName(){
			return METHOD_NAME;
		}
	}
}
