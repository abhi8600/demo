/**
 * 
 */
package com.successmetricsinc;

import java.util.HashMap;

import net.sf.jasperreports.engine.export.HtmlResourceHandler;

/**
 * @author agarrison
 *
 */
public class JRImageHandler extends HashMap<String, byte[]> implements HtmlResourceHandler {

	public static final String BIRST_COM = "@birst.com";

	public JRImageHandler() {
	}
	/* (non-Javadoc)
	 * @see net.sf.jasperreports.engine.export.HtmlResourceHandler#getResourcePath(java.lang.String)
	 */
	@Override
	public String getResourcePath(String id) {
		return "cid:" + id + JRImageHandler.BIRST_COM;
	}

	/* (non-Javadoc)
	 * @see net.sf.jasperreports.engine.export.HtmlResourceHandler#handleResource(java.lang.String, byte[])
	 */
	@Override
	public void handleResource(String id, byte[] data) {
		this.put(id, data);
	}

}
