/**
 * $Id: Queries.java,v 1.35 2011-09-28 17:01:58 ricks Exp $
 *
 * Copyright (C) 2007-2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.successmetricsinc.UI.admin.system;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.UI.SMIPojoBase;
import com.successmetricsinc.query.AbstractQueryString;
import com.successmetricsinc.query.BadColumnNameException;
import com.successmetricsinc.query.Filter;
import com.successmetricsinc.query.JasperDataSourceProvider;
import com.successmetricsinc.query.Navigation;
import com.successmetricsinc.query.NavigationException;
import com.successmetricsinc.query.Query;
import com.successmetricsinc.query.QueryResultSet;
import com.successmetricsinc.query.QueryString;
import com.successmetricsinc.query.ResultSetCache;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.Util;

public class Queries extends SMIPojoBase {
	
	private static Logger logger = Logger.getLogger(Queries.class);
	 
	QueryResultSet resultSet;
	private String text;
	private String query;
	private boolean validResults;
	private String separator;
	private String errorMessage;	
	private int errorCode = BaseException.SUCCESS;

	public void setSeparator(String separator) {
		this.separator = separator;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public QueryResultSet getResultSet() {
		return resultSet;
	}

	public void setResultSet(QueryResultSet resultSet) {
		this.resultSet = resultSet;
	}

	public int getNumRows() {
		if (resultSet != null)
			return resultSet.numRows();
		return -1;
	}

	public Object[] getRows()
	{
		if (resultSet != null)
			return resultSet.getRows();
		return null;
	}
	
	private void clearResults()
	{
		resultSet = null;
		text = null;
		errorMessage = null;
		validResults = false;
	} 
    
    public String getErrorMessage(){
    	return errorMessage;
    }
    
    public int getErrorCode()
    {
    	return errorCode;
    }
    
    public boolean getValidResults(){
    	return validResults;
    }
    
    public void run() throws Exception {
    	run(getRepository(), getCache(), getSession());
    }
    
	public void run(final Repository repository, final ResultSetCache cache, final Session session) throws Exception {
		clearResults();
		Session.clear();
		JasperDataSourceProvider jdsp = new JasperDataSourceProvider(repository, cache);
		if (session != null)
			session.setParams(new ArrayList<Filter>());
		resultSet = (QueryResultSet) jdsp.create(getQuery(), session);
		validResults = true;
	}
	
	public void generateFile(HttpServletResponse response){
		
		char sep = ',';
		if (separator != null && !separator.isEmpty())
		{
			if (separator.equals("\\t"))
				sep = '\t';
			else
				sep = separator.charAt(0);
		}
		String contentType = "text/csv; charset=UTF-8";
		String fileName = "file.csv";
		if (sep != ',')
		{
			contentType = "text/plain; charset=UTF-8";
			fileName = "file.txt";
		}
		
		clearResults();
		try
		{
			JasperDataSourceProvider jdsp = new JasperDataSourceProvider(getRepository(), getCache());			
			resultSet = (QueryResultSet) jdsp.create(getQuery(), getSession());
			validResults = true;
			ByteArrayOutputStream outStream = new ByteArrayOutputStream();
			PrintStream stream = new PrintStream(outStream, true, "UTF-8");
			resultSet.print(stream, sep, getRepository(), false, null, false);
			
			response.setContentType(contentType);
			response.setContentLength( outStream.size() );
			response.setHeader("Content-disposition", "attachment; filename=\"" + fileName + "\"");
			response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate, private");
			response.setHeader("Pragma", "public");

			ServletOutputStream output = response.getOutputStream();
			output.write(outStream.toByteArray() );
			
			outStream.close();
		}
		catch (Exception ex)
		{
			logger.debug(ex.getMessage(), ex);			
			validResults = false;
		}
	}
	
	
	public void generate(boolean isSuperUser) throws Exception
	{
		clearResults();
		Repository r = getRepository();
		String query2 = QueryString.preProcessQueryString(getQuery(), r, getSession());		
		AbstractQueryString aqs = AbstractQueryString.getInstance(r, query2);
		Query q = aqs.getQuery();
		if(!isSuperUser && !continueQueryGeneration(q))
		{
			validResults = false;
			errorCode = BaseException.ERROR_PERMISSION_QUERY_GENERATION;
		}
		else
		{
			text = Util.getPhysicalQuery(aqs, q, query2);
			validResults = true;
		}
	}
	
	public void navigate(boolean isSuperUser) throws Exception
	{
		clearResults();
		Repository r = getRepository();
		String query2 = QueryString.preProcessQueryString(getQuery(), r, getSession());
		AbstractQueryString aqs = AbstractQueryString.getInstance(r, query2);
		Query q = aqs.getQuery();	
		q.navigateQuery(true, false, getSession(), true);
		text = Util.getNavigationExplanation(aqs, q, query2, isSuperUser);
		validResults = true;
	}
	
	public boolean continueQueryGeneration(Query q) throws NavigationException, BadColumnNameException, CloneNotSupportedException
	{		
		q.navigateQuery(true, false, getSession(), true);
		return Queries.hasAllRealTimeTableSources(q);
	}
	
	public String getText()
	{
		return text;
	}

	public static boolean hasAllRealTimeTableSources(Query queryAfterNavigation)
	{
		boolean allRealTimeTableSources = true;
		List<Navigation> navList = queryAfterNavigation.getNavigationList();
		if (navList == null || navList.isEmpty())
		{
			allRealTimeTableSources = false;
		} else
		{
			for (Navigation nav : navList)
			{
				if (!nav.containsAllRealTimeTableSources())
				{
					logger.warn("Query contains atleast one Real Time Table Source");
					allRealTimeTableSources = false;
					break;
				}
			}
		}
		return allRealTimeTableSources;
	}
}
