/**
 * $Id: Cache.java,v 1.21 2011-09-28 16:05:14 ricks Exp $
 *
 * Copyright (C) 2007-2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.successmetricsinc.UI.admin.system;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.successmetricsinc.UI.SMIPojoBase;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.query.CacheOperations;

public class Cache extends SMIPojoBase {
	private boolean clearForAllServers = false;
	private String[] clearForQueryOptions = null;
	private boolean exactMatch = false;
	private boolean reseed = false;
	private String query;
	private List<String> operationOuput;	

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}
	public boolean isClearForAllServers() {
		return clearForAllServers;
	}

	public void setClearForAllServers(boolean clearForAllServers) {
		this.clearForAllServers = clearForAllServers;
	}

	public String[] getClearForQueryOptions() {
		return clearForQueryOptions;
	}

	public void setClearForQueryOptions(String[] clearForQueryOptions) {
		exactMatch = false;
		reseed = false;
		this.clearForQueryOptions = clearForQueryOptions;
		if (this.clearForQueryOptions != null)
		{
			for (String option : this.clearForQueryOptions)
			{
				if ("exact".equals(option))
					exactMatch = true;
				else if ("reseed".equals(option))
					reseed = true;
			}
		}
	}

	public void setExactMatch(boolean exactMatch) {
		this.exactMatch = exactMatch;
	}

	public void setReseed(boolean reseed) {
		this.reseed = reseed;
	}

	private void setOperationOutput(List<String> results){
		this.operationOuput = results;
	}
	
	public List<String> getOperationOutput(){
		return operationOuput;
	}
	
	public String clearForQuery() {
       	Map<String, String> servletParameterMap = new HashMap<String, String> ();
		servletParameterMap.put("cmd", "ClearCacheForQuery");
		servletParameterMap.put("cmdParameter", query);
		if (exactMatch)
			servletParameterMap.put("ExactMatchOnly", "true");
		if (reseed)
			servletParameterMap.put("Reseed", "true");	
		return clearCache(servletParameterMap);
	}

	public String clearForLogical() {
      	Map<String, String> servletParameterMap = new HashMap<String, String> ();
		servletParameterMap.put("cmd", "ClearCacheForLogicalTable");
		servletParameterMap.put("cmdParameter", query);	
		return clearCache(servletParameterMap);
	}

	public String clearForPhysical() {
     	Map<String, String> servletParameterMap = new HashMap<String, String> ();
		servletParameterMap.put("cmd", "ClearCacheForPhysicalTable");
		servletParameterMap.put("cmdParameter", query);	
		return clearCache(servletParameterMap);
	}

	public String clearForAll()
	{
     	Map<String, String> servletParameterMap = new HashMap<String, String> ();
		servletParameterMap.put("cmd", "ClearCache");
		return clearCache(servletParameterMap);
	}

	private String clearCache(Map<String, String> servletParameterMap)
	{
    	CacheOperations co = new CacheOperations();
    	List<String> results = co.performCacheOperation(servletParameterMap, getRepository(), getSession(), getCache());
     	setOperationOutput(results);
		return null;
	}
	
	public String reloadContext()
	{
		SMIWebUserBean ub = SMIWebUserBean.getUserBean();
		ub.getApplicationContext().reloadContext();
		List<String> results = new ArrayList<String>();
		results.add("Reloaded Application Context");
		setOperationOutput(results);		
		return null;
	}
}