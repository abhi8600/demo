package com.successmetricsinc.UI.util;

import com.successmetricsinc.util.BaseException;

public class UnAuthorizedIPSourceException extends BaseException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UnAuthorizedIPSourceException()
	{
		super();
	}
	
	public UnAuthorizedIPSourceException(String s)
	{
		super(s);
	}
	
	public UnAuthorizedIPSourceException(String s, Throwable t)
	{
		super(s, t);
	}

	public int getErrorCode()
	{
		return ERROR_INVALID_IP;
	}
}
