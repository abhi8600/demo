/**
 * 
 */
package com.successmetricsinc;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * @author agarrison
 *
 */
public class BirstSessionListener implements HttpSessionListener {

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpSessionListener#sessionCreated(javax.servlet.http.HttpSessionEvent)
	 */
	@Override
	public void sessionCreated(HttpSessionEvent se) {
    	final HttpSession session = se.getSession();
    	final ServletContext context = session.getServletContext();
    	context.setAttribute(session.getId(), session);
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpSessionListener#sessionDestroyed(javax.servlet.http.HttpSessionEvent)
	 */
	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
    	final HttpSession session = se.getSession();
    	final ServletContext context = session.getServletContext();
    	context.removeAttribute(session.getId());
	}
	
}
