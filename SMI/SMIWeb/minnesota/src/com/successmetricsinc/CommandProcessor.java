/**
 * $Id: CommandProcessor.java,v 1.26 2011-10-11 22:48:20 ricks Exp $
 *
 * Copyright (C) 2007-2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc;

import org.apache.log4j.Logger;

public class CommandProcessor
{
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(CommandProcessor.class);

	public String executeCommand(String username, String password, String command)
	{
		StringBuilder sb = new StringBuilder();
		/*
		try
		{
			authenticateUserAndInitRepSession(username, password, false);
			if (command != null)
			{
				command = command + '\r'; // give it a trailing delimiter
				List<String> tokens = Main.tokenize(command, null, false);
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				cmd.setWriter(new PrintStream(stream));
				// cmd.setServerContextWrapper(new ServerContextWrapperImpl());
				Main.processTokens(tokens, cmd, true);
				stream.flush();
				sb.append(stream.toString());
				cmd.getWriter().flush();
				cmd.getWriter().close();
				stream.close();
			}
		} catch (Exception e)
		{
			logger.error(e.toString(), e);
			sb.append(e.toString());
		}
		*/
		return sb.toString();
	}
	
	public String clearCache(String username, String password)
	{
		StringBuilder sb = new StringBuilder();
		sb.append(executeCommand(username, password, "clearcache"));
		return sb.toString();
	}
	
	public String clearDashboards(String username, String password)
	{
		StringBuilder sb = new StringBuilder();
		sb.append(executeCommand(username, password, "cleardashboards"));
		return sb.toString();
	}

	public String clearCacheForQuery(String username, String password, String query, boolean exactMatchOnly)
	{
		StringBuilder sb = new StringBuilder();
		sb.append(executeCommand(username, password, "removecacheforquery \"" + query + "\" " + exactMatchOnly));
		return sb.toString();
	}

	public String clearCacheForLogicalTable(String username, String password, String logicalTableName)
	{
		StringBuilder sb = new StringBuilder();
		sb.append(executeCommand(username, password, "removecacheforlogicaltable \"" + logicalTableName + "\""));
		return sb.toString();
	}

	public String clearCacheForPhysicalTable(String username, String password, String physicalTableName)
	{
		StringBuilder sb = new StringBuilder();
		sb.append(executeCommand(username, password, "removecacheforphysicaltable \"" + physicalTableName + "\""));
		return sb.toString();
	}

	public String getCatalogs(String username, String password, String connectionName)
	{
		StringBuilder sb = new StringBuilder();
		/*
		try
		{
			authenticateUserAndInitRepSession(username, password, true);
			for (DatabaseConnection dc : r.getConnections())
			{
				if (dc.isValid())
				{
					Connection conn = dc.ConnectionPool.getConnection();
					if (conn == null)
						throw (new Exception("Server Unable to connect to database: " + dc.Name));
					List<String> list = Database.getCatalogs(dc, conn);
					if(list != null)
					{
						boolean firstT = true;
						for (String s : list)
						{
							if(!firstT)
							{
								sb.append("\n");
							}
							else
							{
								firstT = false;
							}
							sb.append(dc.Name + "\t" + s );
						}
					}
					conn.close();
				}
			}
		} catch (Exception e)
		{
			logger.error(e.toString(), e);
			sb.append("ERROR:"+e.toString() + "\n");
		}
		*/
		return sb.toString();
	}

	public String getTables(String username, String password, String connectionName, String catalogName)
	{
		StringBuilder sb = new StringBuilder();
		/*
		try
		{
			authenticateUserAndInitRepSession(username, password, true);
			for (DatabaseConnection dc : r.getConnections())
			{
				if (dc.isValid())
					if (connectionName == null || connectionName.equals(dc.Name))
					{
						List<String> list = Database.getTables(dc, catalogName);
						boolean firstT = true;
						for (String s : list)
						{
							if(!firstT)
							{
								sb.append("\n");
							}
							else
							{
								firstT = false;
							}
							sb.append(dc.Name + "\t" + s );
						}
					}
			}
		} catch (Exception e)
		{
			logger.error(e.toString(), e);
			sb.append("ERROR:"+e.toString());
		}
		*/
		return sb.toString();
	}

	public String getTableSchema(String username, String password, String connectionName, String catalogName, String tableName)
	{
		StringBuilder sb = new StringBuilder();
		/*
		try
		{
			authenticateUserAndInitRepSession(username, password, true);
			for (DatabaseConnection dc : r.getConnections())
			{
				if (dc.isValid())
					if (connectionName == null || connectionName.equals(dc.Name))
					{
						Connection conn = dc.ConnectionPool.getConnection();
						List<Object[]> schema = Database.getTableSchema(dc, conn, catalogName, dc.Schema, tableName);
						boolean firstT = true;
						for (Object[] row : schema)
						{
							if(!firstT)
							{
								sb.append("\n");
							}
							else
							{
								firstT = false;
							}
							sb.append(dc.Name + "\t" + row[0] + "\t" + row[1] + "\t" + row[2]);
						}
						conn.close();
					}
			}
		} catch (Exception e)
		{
			logger.error(e.toString(), e);
			sb.append("ERROR:"+e.toString());
		}
		*/
		return sb.toString();
	}

	public String getRepositoryXML(String username, String password)
	{
		StringBuilder sb = new StringBuilder();
		/*
		try
		{
			authenticateUserAndInitRepSession(username, password, true);
            String fname = ServerContext.getRepositoryFilename();
            File f = new File(fname);
            InputStreamReader reader = new InputStreamReader(new FileInputStream(f));
            char[] buffer = new char[(int) f.length()];
            reader.read(buffer);
            sb.append(buffer);
            reader.close();
		} catch (Exception e)
		{
			logger.error(e.toString(), e);
			sb.append("ERROR:"+e.toString());
		}
		*/
		return sb.toString();
	}

	public String saveRepository(String username, String password, String repositoryXML)
	{
		StringBuilder sb = new StringBuilder();
		/*
		try
		{
			authenticateUserAndInitRepSession(username, password, true);
            String fname = ServerContext.getRepositoryFilename();
            File f = new File(fname);
            File nf = new File(fname+".bak");
            if (nf.exists())
                nf.delete();
            f.renameTo(nf);
            f = new File(fname);
            PrintWriter writer = new PrintWriter(f);
            writer.write(repositoryXML);
            writer.close();
            String reloadURL = getURL("Login?u="+username+"&p="+password+"&reload=true");
            connectToURL(reloadURL);
		} catch (Exception e)
		{
			logger.error(e.toString(), e);
			sb.append("ERROR:"+e.toString());
		}
		*/
		return sb.toString();
	}
}
