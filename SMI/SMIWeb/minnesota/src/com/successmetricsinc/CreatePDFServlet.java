/**
 * $Id: CreatePDFServlet.java,v 1.5 2011-12-28 19:08:15 ricks Exp $
 *
 * Copyright (C) 2009-2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc;

import javax.servlet.*;
import javax.servlet.http.*;

import com.successmetricsinc.util.Util;
import com.successmetricsinc.security.SecurityUtil;

import java.io.*;

/**
 * This servlet is used for generation of PDF called from client side flex AlivePDF api
 * 
 * @author sfoo
 */
public class CreatePDFServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		HttpSession session = req.getSession(false);
		if (session == null)
		{
			resp.sendError(HttpServletResponse.SC_UNAUTHORIZED);
			return;
		}	
		if (!SecurityUtil.validateCSRF(req))
		{
			resp.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "CSRF Failure");
			return;
		}
		int i = 0;
		int k = 0;
		int maxLength = req.getContentLength();
		byte[] bytes = new byte[maxLength];
		String method = req.getParameter("method");
		String name = req.getParameter("name");
		ServletInputStream si = req.getInputStream();
		while (true) {
			k = si.read(bytes, i, maxLength);
			i += k;
			if (k <= 0)
				break;
		}
		ServletOutputStream stream = resp.getOutputStream();
		resp.setContentType("application/pdf");
		resp.setContentLength(bytes.length);
		resp.setHeader("Content-Disposition", Util.removeCRLF(method) + ";filename=" + Util.cleanFilename(name));
		stream.write(bytes);
		stream.flush();
		stream.close();
	}
}
