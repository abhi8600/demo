/**
 * $Id: PostToChatterFeed.java,v 1.3 2012-10-03 01:15:13 BIRST\ricks Exp $
 *
 * Copyright (C) 2009-2012 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.axiom.om.util.Base64;
import org.apache.log4j.Logger;

import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.security.SecurityUtil;
import com.successmetricsinc.sfdc.Chatter;

/**
 * This servlet is used for generation of PDF called from client side flex AlivePDF api
 * 
 * @author sfoo
 */
public class PostToChatterFeed extends HttpServlet
{

	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(PostToChatterFeed.class);

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		doGet(req, resp);
	}

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		HttpSession session = req.getSession(false);
		if (session == null)
		{
			resp.sendError(HttpServletResponse.SC_UNAUTHORIZED);
			return;
		}	
		if (!SecurityUtil.validateCSRF(req))
		{
			resp.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "CSRF Failure");
			return;
		}

		String name = req.getParameter("name"); 		// report/dashboard page name
		String type = req.getParameter("type"); 		// type: link or file
		String comment = req.getParameter("comment"); 	// user input ('hey, check this out!')
		String link = req.getParameter("link");			// URL

		try
		{
			if (type == null)
			{
				logger.warn("Missing Post to Chatter type");
				resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
				return;
			}


			SMIWebUserBean userBean = (SMIWebUserBean) session.getAttribute("userBean");
			if (userBean == null)
			{
				resp.sendError(HttpServletResponse.SC_UNAUTHORIZED);
				return;
			}

			Chatter chtr = new Chatter(userBean.getRepository(), userBean.getSFDC_sessionId(), userBean.getSFDC_serverURL());
			if (type.equals("link"))
			{
				chtr.postLink(comment, link, name);
			}
			else if (type.equals("file"))
			{
				String data = req.getParameter("pdfData"); // base64 encoded
				byte[] bytes = Base64.decode(data);
				chtr.postFile(comment, name, bytes);
			}
			else
			{
				logger.warn("Bad Post to Chatter type: " + type);
				resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
				return;
			}
		}
		catch (Exception e)
		{
			logger.error(e, e);
			resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
		}
	}
}
