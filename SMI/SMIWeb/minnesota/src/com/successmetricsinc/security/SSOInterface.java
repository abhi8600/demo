/**
 * $Id: SSOInterface.java,v 1.1 2007-10-16 17:18:57 ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.successmetricsinc.security;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * all implementations of SSO need to implement this interface
 */
public interface SSOInterface
{
	// set the parameters
	abstract public void init(Map<String,String> params);
	// pull the user name out of the HTTP request
	abstract public String getUser(HttpServletRequest request);
}