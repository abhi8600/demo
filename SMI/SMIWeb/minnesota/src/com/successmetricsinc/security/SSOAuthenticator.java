/**
 * $Id: SSOAuthenticator.java,v 1.12 2011-10-01 00:05:45 ricks Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.successmetricsinc.security;

import java.io.IOException;
import java.security.Principal;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.FilterConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.securityfilter.authenticator.Authenticator;
import org.securityfilter.config.SecurityConfig;
import org.securityfilter.filter.SecurityRequestWrapper;
import org.securityfilter.filter.URLPatternMatcher;
import org.securityfilter.realm.SecurityRealmInterface;

/**
 * SSOAuthenticator - SSO authenticator for use with SecurityFilter
 */
public class SSOAuthenticator implements Authenticator 
{
	private static Logger logger = Logger.getLogger(SSOAuthenticator.class);
	private SecurityRealmInterface realm;
	private Map<String,String> params;
	private SSOInterface ssoClass;
	private boolean paranoid = true; // force checks on every request - needed for multitenant
	private boolean debug = false;
	
	public String getAuthMethod() {
		return "CUSTOM_AUTH";
	}

	/**
	 * initialize the authenticator, set up the parameters and instantiate the actual SSO class
	 */
	@SuppressWarnings("rawtypes")
	public void init(FilterConfig filterConfig, SecurityConfig securityConfig) throws Exception
	{   
		logger.debug("Initializing SSOAuthenticator");
		// get the filter config parameters - headerField, sharedKey, etc.
		params = new HashMap<String,String>();
		Enumeration itr = filterConfig.getInitParameterNames();
		while (itr.hasMoreElements())
		{
			String key = (String) itr.nextElement();
			String value = (String) filterConfig.getInitParameter(key);
			logger.debug("SSOAuthenticator - key='" + key + "'='" + value + "'");
			params.put(key, value);
		}

		// get and initialize the SSO class
		String ssoClassName = params.get("SSOClass");
		try
		{
			Class claz = Class.forName(ssoClassName);
			ssoClass = (SSOInterface) claz.newInstance(); 
		}
		catch (ClassNotFoundException cnfex)
		{
			logger.error("SSOAuthenticator - could not find the SSO class - " + ssoClassName);
			System.exit(-1);
		}
		ssoClass.init(params);

		realm = securityConfig.getRealm();
	}

	/**
	 * Process any login information that was included in the request, if any.
	 * Returns true if SecurityFilter should abort further processing after the method completes (for example, if a
	 * redirect was sent as part of the login processing).
	 *
	 * @param request
	 * @param response
	 * @return true if the filter should return after this method ends, false otherwise
	 */
	public boolean processLogin(SecurityRequestWrapper request, HttpServletResponse response) throws Exception
	{   
		String username = null;
		if (debug)
		{
			logger.debug("uri: " + request.getRequestURI());
			logger.debug("query: " + request.getQueryString());
			@SuppressWarnings("rawtypes")
			Enumeration en = request.getHeaderNames();
			while (en.hasMoreElements())
			{
				String key = en.nextElement().toString();
				logger.debug("header: " + key + "=" + request.getHeader(key));
			}
			en = request.getAttributeNames();
			while (en.hasMoreElements())
			{
				String key = en.nextElement().toString();
				logger.debug("attr: " + key + "=" + request.getAttribute(key));
			}
			en = request.getParameterNames();
			while (en.hasMoreElements())
			{
				String key = en.nextElement().toString();
				logger.debug("param: " + key + "=" + request.getParameter(key));
			}
		}
		if (paranoid)
		{
			// paranoid check on each request to see if the SSO header is now different from the authenticated user
			username = ssoClass.getUser(request);
			String remoteUser = request.getRemoteUser();
			if (remoteUser != null && username != null && !username.equals(remoteUser))
			{
				// if different, null out the principal and invalidate the session
				request.setUserPrincipal(null);
				HttpSession session = request.getSession(false);
				if (session != null)
					session.invalidate();
			}
		}
		if (request.getUserPrincipal() == null)
		{
			HttpSession session = request.getSession(); // make sure that we have a session
			// attempt to dig out authentication info only if the user has not yet been authenticated
			// - for multitenant, userName is userName@application
			if (username == null)
				username = ssoClass.getUser(request);
			if (username != null)
			{
				Principal principal = ((CustomRealm) realm).getPrincipal(username);
				if (principal != null)
				{
					// user authenticated, set principal and remoteuser
					request.setUserPrincipal(principal);
				} 
				else 
				{
					// authentication failed (could not get a valid username)
					logger.warn("SSOAuthenticator: invalid username on request");
					if (session != null)
						session.invalidate();
					response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
					return true;
				}
			}
			else
			{
				logger.warn("SSOAuthenticator: missing username on request");
				if (session != null)
					session.invalidate();
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
				return true;
			}
		}
		if (debug)
			logger.debug("Principal: " + request.getUserPrincipal().toString());
		return false;
	}

	public boolean processLogout(SecurityRequestWrapper request, HttpServletResponse response, URLPatternMatcher patternMatcher) throws Exception
	{
		return false;
	}

	public boolean bypassSecurityForThisRequest(SecurityRequestWrapper request, URLPatternMatcher patternMatcher) throws Exception
	{
		return false;
	}

	/**
	 * Show the login page.
	 *
	 * @param request the current request
	 * @param response the current response
	 */
	public void showLogin(HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		// SecurityFilter.saveRequestInformation(request);
		response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
	}
}

