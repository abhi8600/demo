/**
 * $Id: SSOHeader.java,v 1.3 2011-09-30 23:33:51 ricks Exp $
 *
 * Copyright (C) 2007-2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.successmetricsinc.security;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * SSOHeader
 * 
 * parameter: headerField (see SMIWeb web.xml and Acorn)
 */
public class SSOHeader implements SSOInterface
{
	private String headerField;
	
	public SSOHeader()
	{
	}
	
	/**
	 * initialize the parameters
	 */
	public void init(Map<String,String> params)
	{
		headerField = params.get("headerField");
	}
	
	/**
	 * get the user out of the header
	 */
	public String getUser(HttpServletRequest request)
	{
		String result = request.getHeader(headerField);
		if (result == null)
			return null;
		try {
			byte[] b = Base64.decodeBase64(result);
			return new String(b, "UTF-8");
		}
		catch (UnsupportedEncodingException uee) {
		}
		return result;
	}
}