/**
 * $Id: CustomRealm.java,v 1.17 2011-10-11 22:48:34 ricks Exp $
 *
 * Copyright (C) 2007-2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.successmetricsinc.security;

import java.security.Principal;

import org.apache.log4j.Logger;
import org.securityfilter.realm.SimplePrincipal;
import org.securityfilter.realm.SimpleSecurityRealmBase;

import com.successmetricsinc.Repository;
import com.successmetricsinc.ServerContext;
import com.successmetricsinc.User;

/**
 * Birst Realm for use with SecurityFilter
 * - encapsulates SMIRealm
 */

public class CustomRealm extends SimpleSecurityRealmBase
{
	private static Logger logger = Logger.getLogger(CustomRealm.class);
	
	public String getName()
	{
		return this.getClass().getSimpleName(); 
	}

	/**
	 * return true if the user is in the role (for SMI, user is in a group that has the role)
	 * @param username	name of the user
	 * @param role	name of the role
	 */
	public boolean isUserInRole(String username, String role)
	{
		if ("User".equals(role)) // every authenticated user has the 'User' role
			return true;
		
		Repository r = ServerContext.getRepository();
		if (r == null || !r.isRepositoryInitialized())
		{
			logger.fatal("Invalid repository found in CustomRealm::isUserInRole(" + username + ", " + role + ")");
			return false;
		}
		// strip off the ,application
		if (username == null)
		{
			logger.error("Username is null");
			return false;
		}
		int idx = username.indexOf(',');
		if (idx != -1)
			username = username.substring(0, idx);
		User user = r.findUser(username);
		if (user == null)
		{
			logger.error("Could not find user in CustomRealm::isUserInRole(" + username + ", " + role + ")");
			return false;
		}
		return r.hasRole(user, role);
	}

	/**
	 * return true if the the username is a valid user and the credentials check
	 * - used in form-based authentication, not SSO
	 * @param username	name of the user
	 * @param credentials	hashed password
	 */
	public boolean booleanAuthenticate(String username, String credentials)
	{
		return false;
	}

	/**
	 * Return a security principal object for the username
	 * @param username	name of the user
	 * @return
	 */
	public Principal getPrincipal(String username)
	{
		return new SimplePrincipal(username);
	}
}