
package com.successmetricsinc.security;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

/**
 * Implementation of CSRF for the servlets
 * - base methods for the web service CSRF also, should combine over time
 * @author ricks
 *
 */
public class SecurityUtil
{
	private static Logger logger = Logger.getLogger(SecurityUtil.class);

	/**
	 * validate CSRF
	 * - origin/referer, csrf token (header)
	 * @param req
	 * @param resp
	 * @return
	 */
	public static boolean validateCSRF(HttpServletRequest req)
	{
		return validateCSRF(req, null);
	}
	public static boolean validateCSRF(HttpServletRequest req, HttpSession sess) 
	{
		String remoteHost = req.getRemoteHost();

		if (!validateURLs(req))
			return false;
		Enumeration en = req.getHeaderNames();
		while (en.hasMoreElements())
		{
			String name = (String) en.nextElement();
			String val = req.getHeader(name);
			logger.debug(name + "=" + val);
		}
		
		String token = req.getHeader("X-XSRF-TOKEN");
		if (token == null)
		{
			token = req.getParameter("birst.anticsrftoken"); // just in case you can't easily pass this via a header
		}
		if (token == null) 
		{
			String userAgent = req.getHeader("User-Agent");
			if (userAgent != null && userAgent.startsWith("BirstMobile")) // iPad application has not been updated to pass the token
				return true;
			logger.warn("CSRF attempt: missing CSRF token: " + remoteHost);
			return false;
		}
		String sessionCookie = getSessionCookie(req, "ASP.NET_SessionId");
		if (sessionCookie != null)
			sessionCookie = EncryptionService.getInstance().encryptXSRFToken(sessionCookie);
		if (sess != null) {
			sessionCookie = sess.getId();
		}
		if (sessionCookie == null)
		{
			sessionCookie = getSessionCookie(req, "JSESSIONID"); // special case because the web service call from Acorn does not have an ASP.NET session id
		}
		if (sessionCookie == null)
		{
			logger.warn("CSRF attempt: missing cookie: " + remoteHost);
			return false;
		}
		if (!sessionCookie.equals(token))
		{
			// try JSESSIONID now - ExportServlet always uses JSESSIONID
			HttpSession sess2 = req.getSession(false);
			if (sess2 == null || !token.equals(sess2.getId())) {
				logger.warn("CSRF attempt: bad CSRF token: " + remoteHost + ": " + sessionCookie + " / " + token);
				return false;
			}
		}
		return true;
	}
	
	public static boolean validateURLs(HttpServletRequest req)
	{
		String remoteHost = req.getRemoteHost();
		String requrl = req.getRequestURL().toString();
		String referer = req.getHeader("Referer");
		String origin = req.getHeader("Origin");

		if (referer != null && !referer.equals("null"))
		{
			if (!compareUrls(referer, requrl))
			{
				logger.warn("CSRF attempt: bad referer header: " + remoteHost + ", " + referer + " / " + requrl);
				return false;
			}
		}
		if (origin != null && !origin.equals("null"))
		{
			if (!compareUrls(origin, requrl))
			{
				logger.warn("CSRF attempt: bad origin header: " + remoteHost + ", " + origin + " / " + requrl);
				return false;
			}
		}
		return true;
	}
	
	public static String getSessionCookie(HttpServletRequest request, String name)
	{
		Cookie[] cookies = request.getCookies();
		if (cookies != null)
		{
			for (int i = 0; i < cookies.length; i++)
			{
				if (cookies[i].getName().equals(name))
				{
					return cookies[i].getValue();
				}
			}
		}
		return null;
	}
	
	public static boolean compareUrls(String first, String second)
	{
		try
		{
			URL firstURL = new URL(first);
			URL secondURL = new URL(second);
			/* SSL termination issues
			if (!firstURL.getProtocol().equals(secondURL.getProtocol()))
				return false;
			*/
			if (!firstURL.getHost().equals(secondURL.getHost()))
				return false;
			/* port redirection issues
			if (firstURL.getPort() != secondURL.getPort())
				return false;
			*/
		}
		catch (MalformedURLException ex)
		{
			return false;
		}
		return true;
	}
}