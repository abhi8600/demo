/**
 * $Id: PushUserHandler.java,v 1.9 2012-01-09 18:30:19 ricks Exp $
 *
 * Copyright (C) 2009-2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.WebServices;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.axis2.AxisFault;
import org.apache.axis2.context.MessageContext;
import org.apache.axis2.handlers.AbstractHandler;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.log4j.MDC;

import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.util.logging.AuditLog;

public class PushUserHandler extends AbstractHandler
{
	public PushUserHandler()
	{
	}

	public InvocationResponse invoke(MessageContext msgContext) throws AxisFault
	{
		// clear out the current MDC settings
		AuditLog.reset(true);

		// set the username and spaceID in the MDC for usage tracking
		String userName = getUserName(msgContext);
		if (userName != null)
		{
			String[] parts = userName.split(",");
			if (parts != null && parts.length == 3)
			{
				MDC.put("username", parts[0]);
				MDC.put("spaceID", parts[2]);
				MDC.put("sessionid", getSessionID(msgContext));
			}
		}
		return InvocationResponse.CONTINUE;
	}
	
	private String getSessionID(MessageContext msgContext) {
		HttpServletRequest request = (HttpServletRequest) msgContext.getProperty(HTTPConstants.MC_HTTP_SERVLETREQUEST);
		if (request == null)
			return null;
		SMIWebUserBean.setHttpServletRequest(request);
		HttpSession session = request.getSession(false);
		if (session == null)
			return null;
		return session.getId();
	}

	/**
	 * get the username (username, spaceID) from the web service message context
	 * @param msgContext
	 * @return
	 */
	private String getUserName(MessageContext msgContext)
	{
		if (msgContext == null)
		{
			return null;
		}
		HttpServletRequest request = (HttpServletRequest) msgContext.getProperty(HTTPConstants.MC_HTTP_SERVLETREQUEST);
		if (request == null)
		{
			return null;
		}
		Principal prin = request.getUserPrincipal();
		return prin == null ? null : prin.getName();
	}
}