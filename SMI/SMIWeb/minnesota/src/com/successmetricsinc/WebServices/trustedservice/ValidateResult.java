/**
 * 
 */
package com.successmetricsinc.WebServices.trustedservice;

import java.io.StringWriter;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.impl.util.OMSerializerUtil;
import org.apache.log4j.Logger;

import com.successmetricsinc.WebServices.BirstWebServiceResult;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author Brad
 * 
 */
public class ValidateResult
{
	private static final XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
	private static Logger logger = Logger.getLogger(BirstWebServiceResult.class);
	private int errorCode = BaseException.SUCCESS;
	private String errorMessage;
	private String inputErrorMessage;
	private int inputLine;
	private int inputStart;
	private int inputStop;
	private String outputErrorMessage;
	private int outputLine;
	private int outputStart;
	private int outputStop;
	private String scriptErrorMessage;
	private int scriptLine;
	private int scriptStart;
	private int scriptStop;
	private String loadGroups;

	/**
	 * Is this result valid?
	 * 
	 * @return true if the web service call succeeded, false otherwise.
	 */
	public boolean isSuccess()
	{
		return errorCode == BaseException.SUCCESS;
	}

	/**
	 * Returns the error code.
	 * 
	 * @return Returns the error code. A value of SUCCESS_ERROR_CODE indicates no error.
	 */
	public int getErrorCode()
	{
		return errorCode;
	}

	/**
	 * Sets the error code
	 * 
	 * @param errorCode
	 *            the new error code value
	 */
	public void setErrorCode(int errorCode)
	{
		this.errorCode = errorCode;
	}

	public OMElement toOMElement()
	{
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace ns = fac.createOMNamespace("", "");
		OMElement ret = fac.createOMElement(this.getClass().getName(), ns);
		XmlUtils.addContent(fac, ret, "ErrorCode", this.errorCode, ns);
		// XmlUtils.addContent(fac, ret, "ErrorMessage", this.errorMessage, ns);
		// XmlUtils.addContent(fac, ret, "Part", this.part, ns);
		return ret;
	}

	public String toString()
	{
		OMElement el = this.toOMElement();
		StringWriter w = new StringWriter();
		XMLStreamWriter writer;
		try
		{
			writer = outputFactory.createXMLStreamWriter(w);
			OMSerializerUtil.serializeByPullStream(el, writer);
			writer.flush();
		} catch (XMLStreamException e)
		{
			logger.error(e, e);
		}
		return w.toString();
	}

	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage()
	{
		return errorMessage;
	}

	/**
	 * @param errorMessage
	 *            the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage)
	{
		this.errorMessage = errorMessage;
	}

	/**
	 * @return the inputErrorMessage
	 */
	public String getInputErrorMessage()
	{
		return inputErrorMessage;
	}

	/**
	 * @param inputErrorMessage
	 *            the inputErrorMessage to set
	 */
	public void setInputErrorMessage(String inputErrorMessage)
	{
		this.inputErrorMessage = inputErrorMessage;
	}

	/**
	 * @return the inputLine
	 */
	public int getInputLine()
	{
		return inputLine;
	}

	/**
	 * @param inputLine
	 *            the inputLine to set
	 */
	public void setInputLine(int inputLine)
	{
		this.inputLine = inputLine;
	}

	/**
	 * @return the inputStart
	 */
	public int getInputStart()
	{
		return inputStart;
	}

	/**
	 * @param inputStart
	 *            the inputStart to set
	 */
	public void setInputStart(int inputStart)
	{
		this.inputStart = inputStart;
	}

	/**
	 * @return the inputStop
	 */
	public int getInputStop()
	{
		return inputStop;
	}

	/**
	 * @param inputStop
	 *            the inputStop to set
	 */
	public void setInputStop(int inputStop)
	{
		this.inputStop = inputStop;
	}

	/**
	 * @return the outputErrorMessage
	 */
	public String getOutputErrorMessage()
	{
		return outputErrorMessage;
	}

	/**
	 * @param outputErrorMessage
	 *            the outputErrorMessage to set
	 */
	public void setOutputErrorMessage(String outputErrorMessage)
	{
		this.outputErrorMessage = outputErrorMessage;
	}

	/**
	 * @return the outputLine
	 */
	public int getOutputLine()
	{
		return outputLine;
	}

	/**
	 * @param outputLine
	 *            the outputLine to set
	 */
	public void setOutputLine(int outputLine)
	{
		this.outputLine = outputLine;
	}

	/**
	 * @return the outputStart
	 */
	public int getOutputStart()
	{
		return outputStart;
	}

	/**
	 * @param outputStart
	 *            the outputStart to set
	 */
	public void setOutputStart(int outputStart)
	{
		this.outputStart = outputStart;
	}

	/**
	 * @return the outputStop
	 */
	public int getOutputStop()
	{
		return outputStop;
	}

	/**
	 * @param outputStop
	 *            the outputStop to set
	 */
	public void setOutputStop(int outputStop)
	{
		this.outputStop = outputStop;
	}

	/**
	 * @return the scriptErrorMessage
	 */
	public String getScriptErrorMessage()
	{
		return scriptErrorMessage;
	}

	/**
	 * @param scriptErrorMessage
	 *            the scriptErrorMessage to set
	 */
	public void setScriptErrorMessage(String scriptErrorMessage)
	{
		this.scriptErrorMessage = scriptErrorMessage;
	}

	/**
	 * @return the scriptLine
	 */
	public int getScriptLine()
	{
		return scriptLine;
	}

	/**
	 * @param scriptLine
	 *            the scriptLine to set
	 */
	public void setScriptLine(int scriptLine)
	{
		this.scriptLine = scriptLine;
	}

	/**
	 * @return the scriptStart
	 */
	public int getScriptStart()
	{
		return scriptStart;
	}

	/**
	 * @param scriptStart
	 *            the scriptStart to set
	 */
	public void setScriptStart(int scriptStart)
	{
		this.scriptStart = scriptStart;
	}

	/**
	 * @return the scriptStop
	 */
	public int getScriptStop()
	{
		return scriptStop;
	}

	/**
	 * @param scriptStop
	 *            the scriptStop to set
	 */
	public void setScriptStop(int scriptStop)
	{
		this.scriptStop = scriptStop;
	}

	public String getLoadGroups()
	{
		return loadGroups;
	}

	public void setLoadGroups(String loadGroups)
	{
		this.loadGroups = loadGroups;
	}
}
