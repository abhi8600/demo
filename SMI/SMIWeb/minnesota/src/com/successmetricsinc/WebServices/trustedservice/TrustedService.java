/**
 * $Id: TrustedService.java,v 1.156 2012-12-12 05:17:38 BIRST\mpandit Exp $
 *
 * Copyright (C) 2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.WebServices.trustedservice;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.crypto.SecretKey;
import javax.jws.WebService;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import org.apache.axiom.om.OMElement;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.rosuda.REngine.Rserve.RFileOutputStream;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.successmetricsinc.Broadcast;
import com.successmetricsinc.CompareCatalogs;
import com.successmetricsinc.EmailDelivery;
import com.successmetricsinc.Group;
import com.successmetricsinc.PackageSpaceProperties;
import com.successmetricsinc.RServerUtil;
import com.successmetricsinc.Repository;
import com.successmetricsinc.SearchReports;
import com.successmetricsinc.ServerContext;
import com.successmetricsinc.Session;
import com.successmetricsinc.User;
import com.successmetricsinc.Variable;
import com.successmetricsinc.R.RServer;
import com.successmetricsinc.UI.ApplicationContext;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.UI.util.UnAuthorizedIPSourceException;
import com.successmetricsinc.WebServices.BirstWebService;
import com.successmetricsinc.WebServices.BirstWebServiceResult;
import com.successmetricsinc.WebServices.StringWebServiceResult;
import com.successmetricsinc.WebServices.SubjectArea.ExtendedSubjectAreaNode;
import com.successmetricsinc.WebServices.SubjectArea.SubjectAreaManager;
import com.successmetricsinc.WebServices.admin.AdminCache;
import com.successmetricsinc.WebServices.connector.ConnectorData;
import com.successmetricsinc.WebServices.connector.ConnectorServices;
import com.successmetricsinc.WebServices.trustedservice.ExportReportManager.ReportStatus;
import com.successmetricsinc.adhoc.AdhocColumn;
import com.successmetricsinc.adhoc.AdhocReport;
import com.successmetricsinc.catalog.CatalogManager;
import com.successmetricsinc.connectors.ConnectorUtil;
import com.successmetricsinc.liveaccessdirectconnect.DirectConnectUtil;
import com.successmetricsinc.metadatavalidation.util.MetadataValidationServiceUtil;
import com.successmetricsinc.query.AbstractQueryString;
import com.successmetricsinc.query.BadColumnNameException;
import com.successmetricsinc.query.Database.DatabaseType;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.DimensionTable;
import com.successmetricsinc.query.DisplayExpression;
import com.successmetricsinc.query.Filter;
import com.successmetricsinc.query.JasperDataSourceProvider;
import com.successmetricsinc.query.LogicalQueryString;
import com.successmetricsinc.query.MeasureTable;
import com.successmetricsinc.query.MeasureTableGrain;
import com.successmetricsinc.query.NavigationException;
import com.successmetricsinc.query.Query;
import com.successmetricsinc.query.QueryResultSet;
import com.successmetricsinc.query.QueryString;
import com.successmetricsinc.query.ResultSetCache;
import com.successmetricsinc.security.Net;
import com.successmetricsinc.sfdc.SFDCDataExtractor;
import com.successmetricsinc.status.Status;
import com.successmetricsinc.transformation.ParseResult;
import com.successmetricsinc.transformation.ScriptDefinition;
import com.successmetricsinc.transformation.ScriptException;
import com.successmetricsinc.transformation.TransformationScript;
import com.successmetricsinc.transformation.TransformationScript.ScriptResult;
import com.successmetricsinc.util.AWSUtil;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.DateUtil;
import com.successmetricsinc.util.FileUtils;
import com.successmetricsinc.util.RepositoryException;
import com.successmetricsinc.util.SoftHashMap;
import com.successmetricsinc.util.TimeZoneUtil;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.logging.AuditLog;
import com.successmetricsinc.util.logging.LogUtil;
import com.successmetricsinc.warehouse.Sample;
import com.successmetricsinc.warehouse.Sample.SampleMetadata;
import com.successmetricsinc.warehouse.StagingTable;

/**
 * @author bpeters
 * 
 */
@WebService(serviceName = "TrustedService")
public class TrustedService extends BirstWebService
{
	private static Logger logger = Logger.getLogger(TrustedService.class);
	@SuppressWarnings("unused")
	private static char DELIM = '|';
	@SuppressWarnings("unused")
	private static String EQUALS = "=";
	private static String COLON = ":";
	private static String LEFT_VAR_DELIM = "V{";
	private static String RIGHT_VAR_DELIM = "}";
	private static String EMPTY_STR = "";
	private static String ERROR_INVALID_IP = "Unable to validate request";
	
	public ExecuteResult extractSFDCData(String username, String spaceID, String path, String SFDCClientID, String sandBoxURL, int noOfExtractionThreads)
	{
		ExecuteResult er = checkValidIP("extractSFDCData");
		ApplicationContext ac = null;
		try
		{
			MDC.put("username", username);
			MDC.put("spaceID", spaceID);
			if (er.getErrorCode() != BaseException.SUCCESS)
				return er;
			ac = ApplicationContext.getInstance(path, null);
			Repository r = ac.getRepository();
			if (r == null)
				return er;
			SFDCDataExtractor sfExtractor = new SFDCDataExtractor(r, SFDCClientID, sandBoxURL, noOfExtractionThreads);
			sfExtractor.extractData();
		} catch (Exception e)
		{
			logger.debug(e, e);
			er.setErrorCode(BaseException.ERROR_OTHER);
			er.setErrorMessage(e.getMessage());
		} finally
		{
			MDC.remove("username");
			MDC.remove("spaceID");
			if (ac != null)
				ac.dispose();
		}
		return er;
	}
	private static Map<String, Repository> devRepositoryCache = Collections.synchronizedMap(new SoftHashMap<String, Repository>("Repository (dev) cache"));

	
	/**
	 * 
	 * @param callerName  -- name of the caller function or some other reference id for logging purposes
	 * @return ExecuteResult - errorCode of the object is flagged as unsuccessful
	 */
	private ExecuteResult checkValidIP(String callerName)
	{
		// check for valid IP
		String requestIP = getHttpServletRequest().getRemoteAddr();
		List<String> allowedIPs = ServerContext.getLoginServiceAllowedIPs();
		ExecuteResult er = new ExecuteResult();

		if (!Net.containIp(allowedIPs, requestIP))
		{
			logger.warn("checkValidIP: " + (callerName != null ? callerName : "") + " : bad request IP: " + requestIP);
			er.setErrorCode(BaseException.ERROR_OTHER);
			er.setErrorMessage("Unable to validate request");
		}

		return er;
	}
	
	private boolean isTrustedRequest(String callerName)
	{	
		ExecuteResult validIpResult = checkValidIP(callerName);
		return validIpResult.getErrorCode() == BaseException.SUCCESS;
	}

	private boolean isTrustedRequest(String callerName, BirstWebServiceResult result){
		if(!isTrustedRequest(callerName))
		{	
			if(result != null)
			{
				result.setErrorCode(BaseException.ERROR_OTHER);
				result.setErrorMessage(TrustedService.ERROR_INVALID_IP);				
			}
			return false;
		}
		return true;
	}
	
	/**
	 * Get the repository for interactive script editing/execution
	 * 
	 * @param path
	 * @param er
	 * @param spacePropertiesList
	 * @return
	 */
	private Repository getRepository(String path, ExecuteResult er, List<PackageSpaceProperties> spacePropertiesList)
	{
		Repository r = devRepositoryCache.get(path);
		if (r == null)
		{
			r = ApplicationContext.loadRepository(path, "dev", spacePropertiesList);
			devRepositoryCache.put(path, r);
		} else
		{
			r = ApplicationContext.loadRepositoryIfMoreRecent(path, "dev", r, spacePropertiesList);
			devRepositoryCache.put(path, r);
		}
		if (r == null)
		{
			er.setErrorCode(BaseException.ERROR_OTHER);
			er.setErrorMessage("Unable to load configuration metadata");
			return null;
		}
		try
		{
			r.buildDimensionLevelMap();
			r.instantiateRepositoryVariables(null, new JasperDataSourceProvider(r));
		} catch (RepositoryException e)
		{
			logger.error(e);
			er.setErrorCode(BaseException.ERROR_OTHER);
			er.setErrorMessage(e.getMessage());
			return null;
		}
		return r;
	}

	/*
	 * XXX note that this does not execute the script in the correct processing engine
	 */
	public ExecuteResult executeScript(String username, String spaceID, int loadNumber, String path, String databasePath, String loadGroupName,
			String inputQuery, String output, String script, int numRows, String awsAccessKeyId, String awsSecretKey, String[] packageSpacesProperties)
	{
		ExecuteResult er = checkValidIP("executeScript");
		try
		{
			MDC.put("username", username);
			MDC.put("spaceID", spaceID);
			if (er.getErrorCode() != BaseException.SUCCESS)
				return er;
			Repository r = getRepository(path, er, Util.getPackageSpacePropertiesList(packageSpacesProperties));
			if (r == null)
				return er;
			r.processImports();
			// Run using the default connection - not a query connection
			r.setDefaultConnection(r.getConnections().get(0).Name);
			r.setRepositoryVariable("USER", username);
			ScriptResult sr = null;
			TransformationScript sc = new TransformationScript(r, null);
			Status status = new Status(r, Integer.toString(loadNumber), null);
			// Don't log when executing for testing purposes
			status.setDisableLogging(true);
			sc.setLoadAllBaseTables(true);// when executing script from UI, allow all base tables to be loaded even if
											// they are already loaded
			sc.setDoNotLoadVisulationSources(false);
			AWSCredentials s3credentials = null;
			if (awsAccessKeyId != null && awsSecretKey != null)
			{
				s3credentials = new BasicAWSCredentials(awsAccessKeyId, awsSecretKey);
				List<SecretKey> keys = new ArrayList<SecretKey>();
				AmazonS3 s3client = AWSUtil.getS3Client(s3credentials, keys);							
				boolean encrypted = AWSUtil.isEncrypted(s3client);
				SecretKey s3secretKey = null;
				if (encrypted)
					s3secretKey = keys.get(0);
				sc.setS3Credentials(s3credentials);
				sc.setS3Client(s3client);
				sc.setS3SecretKey(s3secretKey);
			}
			try
			{
				sr = sc.executeScript(status, inputQuery, output, script, loadGroupName, path + "\\data", r.replaceVariables(null, databasePath), numRows,
						null, null, null);
			} catch (ScriptException ex)
			{
				logger.error(ex);
				er.setErrorCode(BaseException.ERROR_OTHER);
				er.setErrorMessage(ex.getMessage());
				return er;
			}
			er.setNumInputRows(sr.numInputRows);
			er.setNumOutputRows(sr.numOutputRows);
		} catch (Exception e)
		{
			logger.debug(e, e);
			er.setErrorCode(BaseException.ERROR_OTHER);
			er.setErrorMessage(e.getMessage());
		} finally
		{
			MDC.remove("username");
			MDC.remove("spaceID");
		}
		return er;
	}

	/*
	 * XXX note that this does not validate the script in the correct processing engine
	 */
	public ValidateResult validateScript(String username, String spaceID, String path, String inputQuery, String output, String script,
			String[] packageSpacesProperties)
	{
		ValidateResult vr = new ValidateResult();
		ExecuteResult er = checkValidIP("validateQuery");
		try
		{
			MDC.put("username", username);
			MDC.put("spaceID", spaceID);
			if (er.getErrorCode() != BaseException.SUCCESS)
			{
				vr.setErrorCode(er.getErrorCode());
				vr.setErrorMessage(er.getErrorMessage());
				return vr;
			}
			Repository r = getRepository(path, er, Util.getPackageSpacePropertiesList(packageSpacesProperties));
			if (r == null)
			{
				vr.setErrorCode(BaseException.ERROR_OTHER);
				vr.setErrorMessage("Unable to initialize script validation");
				return vr;
			}
			r.processImports();
			vr.setErrorCode(BaseException.SUCCESS);
			ScriptDefinition sd = new ScriptDefinition();
			sd.InputQuery = inputQuery;
			sd.Output = output;
			sd.Script = script;
			r.setDefaultConnection(r.getConnections().get(0).Name);
			TransformationScript ts = new TransformationScript(r, null);
			ParseResult pr = ts.parseScript(sd, 100, null);
			if (pr.InputError != null)
			{
				vr.setInputErrorMessage(pr.InputError.getMessage());
				vr.setInputLine(pr.InputError.getLine());
				vr.setInputStart(pr.InputError.getStart());
				vr.setInputStop(pr.InputError.getEnd());
				vr.setErrorCode(BaseException.ERROR_OTHER);
			}
			if (pr.OutputError != null)
			{
				vr.setOutputErrorMessage(pr.OutputError.getMessage());
				vr.setOutputLine(pr.OutputError.getLine());
				vr.setOutputStart(pr.OutputError.getStart());
				vr.setOutputStop(pr.OutputError.getEnd());
				vr.setErrorCode(BaseException.ERROR_OTHER);
			}
			if (pr.ScriptError != null)
			{
				vr.setScriptErrorMessage(pr.ScriptError.getMessage());
				vr.setScriptLine(pr.ScriptError.getLine());
				vr.setScriptStart(pr.ScriptError.getStart());
				vr.setScriptStop(pr.ScriptError.getEnd());
				vr.setErrorCode(BaseException.ERROR_OTHER);
			}
			if (pr.LoadGroups != null && pr.LoadGroups.length > 0)
			{
				StringBuilder sb = new StringBuilder();
				for (String s : pr.LoadGroups)
				{
					if (sb.length() > 0)
						sb.append(',');
					sb.append(s);
				}
				vr.setLoadGroups(sb.toString());
			}
		} catch (Exception ex)
		{
			vr.setErrorCode(BaseException.ERROR_OTHER);
		} finally
		{
			MDC.remove("username");
			MDC.remove("spaceID");
		}
		return vr;
	}

	/*
	 */
	public WizardResult scriptWizard(String username, String spaceID, String path, String inputQuery, String output, String script,
			String[] packageSpacesProperties)
	{
		WizardResult wr = new WizardResult();
		ExecuteResult er = checkValidIP("scriptWizard");
		try
		{
			MDC.put("username", username);
			MDC.put("spaceID", spaceID);
			if (er.getErrorCode() != BaseException.SUCCESS)
			{
				wr.setErrorCode(er.getErrorCode());
				wr.setErrorMessage(er.getErrorMessage());
				return wr;
			}
			Repository r = getRepository(path, er, Util.getPackageSpacePropertiesList(packageSpacesProperties));
			if (r == null)
			{
				wr.setErrorCode(BaseException.ERROR_OTHER);
				wr.setErrorMessage("Unable to initialize script wizard");
				return wr;
			}
			r.processImports();
			wr.setErrorCode(BaseException.SUCCESS);
			ScriptDefinition sd = new ScriptDefinition();
			sd.InputQuery = inputQuery;
			sd.Output = output;
			sd.Script = script;
			r.setDefaultConnection(r.getConnections().get(0).Name);
			TransformationScript ts = new TransformationScript(r, null);
			ParseResult pr = ts.parseScript(sd, 100, null);
			String validError = ts.validForWizard();
			if (validError != null)
			{
				wr.setErrorCode(BaseException.ERROR_OTHER);
				wr.setErrorMessage(validError);
			} else if (pr.ScriptError != null || pr.InputError != null)
			{
				wr.setErrorCode(BaseException.ERROR_OTHER);
				wr.setErrorMessage("Unable to parse script");
			} else
			{
				wr.setInputTables(ts.getInputTables());
				wr.setInputColumns(ts.getInputColumns());
				wr.setOutputColumns(ts.getOutputColumns());
				wr.setSortColumns(ts.getSortColumns());
				wr.setSortDirections(ts.getSortDirections());
				wr.setInputJoinsTypes(ts.getJoinTypes());
				wr.setInputJoins(ts.getJoinConditions());
			}
		} catch (Exception ex)
		{
			wr.setErrorCode(BaseException.ERROR_OTHER);
		} finally
		{
			MDC.remove("username");
			MDC.remove("spaceID");
		}
		return wr;
	}

	/*
	 */
	public WizardResult formatScript(String username, String spaceID, String path, String inputQuery, String output, String script,
			String[] packageSpacesProperties)
	{
		WizardResult wr = new WizardResult();
		ExecuteResult er = checkValidIP("formatScript");
		try
		{
			MDC.put("username", username);
			MDC.put("spaceID", spaceID);
			if (er.getErrorCode() != BaseException.SUCCESS)
			{
				wr.setErrorCode(er.getErrorCode());
				wr.setErrorMessage(er.getErrorMessage());
				return wr;
			}
			Repository r = getRepository(path, er, Util.getPackageSpacePropertiesList(packageSpacesProperties));
			if (r == null)
			{
				wr.setErrorCode(BaseException.ERROR_OTHER);
				wr.setErrorMessage("Unable to initialize script wizard");
				return wr;
			}
			r.processImports();
			wr.setErrorCode(BaseException.SUCCESS);
			ScriptDefinition sd = new ScriptDefinition();
			sd.InputQuery = inputQuery;
			sd.Output = output;
			sd.Script = script;
			r.setDefaultConnection(r.getConnections().get(0).Name);
			TransformationScript ts = new TransformationScript(r, null);
			ParseResult pr = ts.parseScript(sd, 100, null);
			if (pr.ScriptError != null || pr.InputError != null)
			{
				wr.setErrorCode(BaseException.ERROR_OTHER);
				wr.setErrorMessage("May contain elements that do not have associated formatting rules");
			} else
			{
				wr.setBodyScript(ts.getFormattedBody());
			}
		} catch (Exception ex)
		{
			wr.setErrorCode(BaseException.ERROR_OTHER);
			wr.setErrorMessage("May contain elements that do not have associated formatting rules");
		} finally
		{
			MDC.remove("username");
			MDC.remove("spaceID");
		}
		return wr;
	}

	public String getConnectorVariables(String path, String username, String spaceID, String connectionString, String extractGroups)
	{
		ExecuteResult execResult = checkValidIP("getConnectorVariables");
		StringBuilder sb = new StringBuilder();
		try
		{
			// Initialize call.
			MDC.put("username", username);
			MDC.put("spaceID", spaceID);
			if (execResult.getErrorCode() != BaseException.SUCCESS)
			{
				logger.error(execResult.getErrorCode() + COLON + execResult.getErrorMessage());
				throw new Exception("IP address is invalid.");
			}
			// Build formatted variable list.
			String connectorName = ConnectorData.getConnectorNameFromConnectorConnectionString(connectionString);
			String variables = ConnectorUtil.getVariablesForExtractGroups(path, connectorName, extractGroups);
			sb.append(formatVariables(getVariables(path, username, spaceID, variables)));
		} catch (Exception ex)
		{
			logger.error(ex.getMessage(), ex);
		} finally
		{
			MDC.remove("username");
			MDC.remove("spaceID");
		}
		return sb.toString();
	}

	public String formatVariables(String variables)
	{
		if (variables.length() > 2)
		{
		variables = variables.substring(1);
		variables = variables.replace(LEFT_VAR_DELIM, EMPTY_STR);
		variables = variables.replace(RIGHT_VAR_DELIM, EMPTY_STR);
		}
		return variables;
	}

	public String getVariables(String path, String username, String spaceID, String variables)
	{
		ExecuteResult er = checkValidIP("getVariables");
		ApplicationContext ac = null;
		HttpSession session = null;
		try
		{
			MDC.put("username", username);
			MDC.put("spaceID", spaceID);
			if (er.getErrorCode() != BaseException.SUCCESS)
			{
				logger.error(er.getErrorCode() + ": " + er.getErrorMessage());
				return "|";
			}
			Set<String> searchVarsSet = ((variables == null) ? null : variables.length() == 0 ? new HashSet<String>() : new HashSet<String>(Arrays.asList(variables.split(","))));
			logger.info("getVariables request for space: " + spaceID + " variables: " + searchVarsSet);
			String resVariables = null;
			if (searchVarsSet != null && searchVarsSet.isEmpty())
			{
				resVariables = "|";
			}
			else
			{
				ac = ApplicationContext.getInstance(path, null);
				Repository r = ac.getRepository();
				SMIWebUserBean ub = new SMIWebUserBean();
				ub.setApplicationContext(ac);
				ub.setUser(r.findUser(username));
				ub.setRepository(r);
				session = getHttpServletRequest().getSession();
				SMIWebUserBean.setHttpServletRequest(getHttpServletRequest());
				session.setAttribute("userBean", ub);
				resVariables = r.getVariablesStr(ub.getRepSession(true, searchVarsSet), searchVarsSet, spaceID);
			}
			logger.info("getVariables response for space: " + spaceID + " variables: " + resVariables);
			return resVariables;
		} catch (Exception ex)
		{
			logger.error(ex.getMessage(), ex);
			return "|";
		} finally
		{
			MDC.remove("username");
			MDC.remove("spaceID");
			if (session != null)
				session.invalidate();
			else if (ac != null)
				ac.dispose();
		}
	}

	/**
	 * @deprecated should be using scheduleExportReport
	 * @param mailHost
	 * @param path
	 * @param spaceID
	 * @param type
	 * @param name
	 * @param username
	 * @param from
	 * @param subject
	 * @param body
	 * @param report
	 * @param toList
	 * @param toReport
	 * @param triggerReport
	 * @param maxReports
	 * @param reportLocale
	 * @param reportTimeZone
	 * @return
	 */
	public ExecuteResult exportReport(String mailHost, String path, String spaceID, String type, String name, String username, String from, String subject,
			String body, String report, String[] toList, String toReport, String triggerReport, int maxReports, String reportLocale, String reportTimeZone)
	{
		ExecuteResult er = checkValidIP("exportReport");
		ApplicationContext ac = null;
		HttpSession session = null;
		try
		{
			MDC.put("username", username);
			MDC.put("spaceID", spaceID);
			if (er.getErrorCode() != BaseException.SUCCESS)
				return er;
			ac = ApplicationContext.getInstance(path, null);
			Repository r = ac.getRepository();
			if (r == null)
			{
				er.setErrorCode(BaseException.ERROR_OTHER);
				er.setErrorMessage("Unable to load configuration metadata");
				return er;
			}
			if (report == null)
			{
				er.setErrorCode(BaseException.ERROR_OTHER);
				er.setErrorMessage("exportReport: null report for spaceID: " + spaceID);
				return er;
			}
			logger.info("Exporting report " + report + " (via email) for repository " + spaceID);
			String catalogPath = r.getServerParameters().getCatalogPath();
			String jasperRpt = report;
			String toReportName = toReport;
			String triggerReportName = triggerReport;
			if (catalogPath == null)
				catalogPath = r.getServerParameters().getApplicationPath() + "\\catalog";
			if (catalogPath != null)
			{
				jasperRpt = jasperRpt.replace("V{CatalogDir}", catalogPath);
				if (toReportName != null)
					toReportName = toReportName.replace("V{CatalogDir}", catalogPath);
				if (triggerReportName != null)
					triggerReportName = triggerReportName.replace("V{CatalogDir}", catalogPath);
			}
			com.successmetricsinc.UserBean userBean = new com.successmetricsinc.UserBean();
			if (reportLocale != null && !reportLocale.isEmpty())
			{
				String[] locale = reportLocale.split("-");
				userBean.setLocale(new Locale(locale[0], locale[1]));
			}
			TimeZone userTZ = null;
			if (reportTimeZone != null && !reportTimeZone.isEmpty())
			{
				userTZ = TimeZoneUtil.getTimeZoneForWindowsTZID(reportTimeZone);
			}
			if (userTZ == null)
			{
				userTZ = r.getServerParameters().getDisplayTimeZone();
			}
			userBean.setTimeZone(userTZ);
			userBean.setRepository(r);
			Broadcast b = new Broadcast(name, spaceID, username, from, subject, body, jasperRpt, toList, toReportName, triggerReportName, type);
			EmailDelivery ed = new EmailDelivery(r, b, userBean, mailHost);
			int numRows = ed.deliverReports(maxReports);
			er.setNumInputRows(numRows);
		} catch (Exception ex)
		{
			logger.error("Exception ", ex);
			er.setErrorCode(BaseException.ERROR_OTHER);
		} finally
		{
			MDC.remove("username");
			MDC.remove("spaceID");
			if (session != null)
				session.invalidate();
			else if (ac != null)
				ac.dispose();
		}
		return er;
	}

	public ExecuteResult seedCache(String path, String spaceID, String username, String userlocale, String userTimeZone, boolean dashboards,
			boolean saveReportXmls, String[] packageSpacesProperties)
	{
		ExecuteResult er = checkValidIP("seedCache");
		ApplicationContext ac = null;
		HttpSession session = null;
		boolean seedingStartedForThisRequest = false;
		try
		{
			MDC.put("username", username);
			MDC.put("spaceID", spaceID);
			if (er.getErrorCode() != BaseException.SUCCESS)
				return er;
			ac = ApplicationContext.getInstance(path, Util.getPackageSpacePropertiesList(packageSpacesProperties));
			Repository r = ac.getRepository();
			if (r == null)
			{
				er.setErrorCode(BaseException.ERROR_OTHER);
				er.setErrorMessage("Unable to load configuration metadata");
				return er;
			}
			SMIWebUserBean ub = new SMIWebUserBean();
			ub.setApplicationContext(ac);
			ub.setUser(r.findUser(username));
			if (userlocale != null && !userlocale.isEmpty())
			{
				String[] locale = userlocale.split("-");
				ub.setLocale(new Locale(locale[0], locale[1]));
			}
			TimeZone userTZ = null;
			if (userTimeZone != null && !userTimeZone.isEmpty())
			{
				userTZ = TimeZoneUtil.getTimeZoneForWindowsTZID(userTimeZone);
			}
			if (userTZ == null)
			{
				userTZ = r.getServerParameters().getDisplayTimeZone();
			}
			ub.setTimeZone(userTZ);
			ub.setRepository(r);
			session = getHttpServletRequest().getSession();
			SMIWebUserBean.setHttpServletRequest(getHttpServletRequest());
			session.setAttribute("userBean", ub);
			synchronized (ac)
			{
				if (ac.isCacheBeingSeed())
				{
					throw new Exception("SeedCache already in progress spaceID: " + spaceID + ", cannot seed queries again.");
				}
				ac.setCacheBeingSeed(true);
				seedingStartedForThisRequest = true;
			}
			AdminCache cache = new AdminCache();
			cache.seedCache(dashboards, saveReportXmls);
			logger.info("Seedcache operation successfully completed for spaceID : " + spaceID);
		} catch (Exception ex)
		{
			er.setErrorCode(BaseException.ERROR_OTHER);
			logger.error("Exception in seedcache operation for spaceID : " + spaceID + " : " + ex.getMessage());
		} finally
		{
			MDC.remove("username");
			MDC.remove("spaceID");
			if (ac != null && seedingStartedForThisRequest)
				ac.setCacheBeingSeed(false);
			if (session != null)
				session.invalidate();
			else if (ac != null)
				ac.dispose();
		}
		return er;
	}

	@SuppressWarnings("static-access")
	public ExecuteResult exportQueryData(String path, String spaceID, String username, String userDisplayTimezone, String query, int maxRecords,
			String filename, boolean useLogicalQuery, String[] packageSpacesProperties)
	{
		ApplicationContext ac = null;
		ExecuteResult er = checkValidIP("exportQueryData");
		Session session = null;
		try
		{
			MDC.put("username", username);
			MDC.put("spaceID", spaceID);
			long start = System.currentTimeMillis();
			if (er.getErrorCode() != BaseException.SUCCESS)
				return er;
			ac = ApplicationContext.getInstance(path, Util.getPackageSpacePropertiesList(packageSpacesProperties));
			Repository r = ac.getRepository();
			if (r == null)
			{
				logger.debug("Unable to load repository");
				er.setErrorCode(BaseException.ERROR_OTHER);
				er.setErrorMessage("Unable to load configuration metadata");
				return er;
			}
			// set up the session for the user to get the various DEFAULT session variables
			User u = r.findUser(username);
			if (u == null)
			{
				logger.debug("Unable to find user");
				er.setErrorCode(BaseException.ERROR_OTHER);
				er.setErrorMessage("Unable to load user");
				return er;
			}
			SMIWebUserBean ub = new SMIWebUserBean();
			ub.setApplicationContext(ac);
			ub.setUser(u);
			session = new Session(username);
			session.clear();
			SMIWebUserBean.setHttpServletRequest(getHttpServletRequest());
			ub.setRepository(r);
			SMIWebUserBean.addThreadRequest(Thread.currentThread().getId(), ub);
			JasperDataSourceProvider jdsp = null;
			logger.info("Exporting data for query " + query + " (to a zip file " + filename + ") for repository " + spaceID);
			ResultSetCache rsc = ResultSetCache.getInstance(r.getApplicationIdentifier(), false);
			jdsp = new JasperDataSourceProvider(r, rsc);
			session.fillSessionVariables(r, rsc);
			logger.info("Session variables (TrustedService:exportData): " + session.toString());
			session.setParams(new ArrayList<Filter>());
			// create the report
			jdsp.setSession(session);
			jdsp.setMaxRecords(maxRecords);
			jdsp.setExecutePhysicalQueryOnly(!useLogicalQuery);
			int queryTimeOut = 30 * 60 * 1000;// 30 minutes timeout for query execution
			jdsp.setQueryTimeOut(queryTimeOut);
			jdsp.setUserDisplayTimezone(userDisplayTimezone);
			jdsp.create(query);
			if (useLogicalQuery)
			{
				QueryResultSet qrs = jdsp.getQueryResultSet();
				writeQRStoFile(path, filename, userDisplayTimezone, r, qrs, maxRecords, null, null, '\t');
				if (qrs != null)
					er.setNumInputRows(qrs.numRows());
				else
					er.setNumInputRows(0);
			} else
			{
				// Only generate and execute physical query and capture results to a file
				logger.warn("Exporting Query Data - only physical query will be exported, display expressions, display filters and display ordres will not be processed.");
				long numRows = jdsp.executeAndPersistPhysicalQueryToFile(path + "\\data\\export\\" + filename + ".zip");
				er.setNumInputRows(numRows);
			}
			AuditLog.log("bulk", query, null, System.currentTimeMillis() - start);
		} catch (Exception ex)
		{
			logger.debug(ex, ex);
			er.setErrorCode(BaseException.ERROR_OTHER);
			if (ex.getMessage() != null && !ex.getMessage().isEmpty())
				er.setErrorMessage(ex.getMessage());
		} finally
		{
			MDC.remove("username");
			MDC.remove("spaceID");
			SMIWebUserBean.removeThreadRequest(Thread.currentThread().getId());
			if (session != null)
			{
				session.setCancelled(true);
			}
			if (ac != null)
				ac.dispose();
		}
		return er;
	}

	private void writeQRStoFile(String path, String name, String userDisplayTimezone, Repository r, QueryResultSet qrs, int maxRecords, AdhocReport ar,
			String rServerPath, char delimeter) throws IOException
	{
		FileOutputStream fos = null;
		ZipOutputStream zos = null;
		OutputStreamWriter writer = null;
		Map<String, String> aliasToDisplayNames = new HashMap<String, String>();
		boolean isRserver = rServerPath != null && rServerPath.trim().length() > 0;
		RFileOutputStream rfos = null;
		if (ar != null)
		{
			for (AdhocColumn ac : ar.getColumns()) {
				aliasToDisplayNames.put(ac.getFieldName(), ac.getLabelString());
			}
		}
		try
		{
			if (isRserver)
			{
				RServer rs = r.getRServer();
				if (rs == null)
					return;
				try
				{
					rfos = rs.getOutputStream(rServerPath);
					writer = new OutputStreamWriter(rfos, "UTF-8");
				} catch (Exception e)
				{
					logger.error(e,e);
					return;
				}
			} else
			{
				File f = new File(path + "\\data\\export");
				if (!f.exists())
					f.mkdir();
				logger.debug("writing report to: " + path + "\\data\\export\\" + name + ".zip");
				fos = new FileOutputStream(path + "\\data\\export\\" + name + ".zip", false);
				zos = new ZipOutputStream(fos);
				ZipEntry ze = new ZipEntry(name + ".txt");
				zos.putNextEntry(ze);
				writer = new OutputStreamWriter(zos, "UTF-8");
			}
			if (qrs != null)
			{
				String[] displayNames = qrs.getDisplayNames();
				for (int i = 0; i < displayNames.length; i++)
				{
					if (displayNames[i] != null)
					{
						String n = aliasToDisplayNames.get(displayNames[i]);
						if (n == null) {
							// Remove possible leading field indicator for Adhoc (old style)
							if (displayNames[i].length() > 3 && displayNames[i].charAt(0) == 'F' && displayNames[i].charAt(2) == '_')
								n = displayNames[i].substring(3);
							else if (displayNames[i].length() > 4 && displayNames[i].charAt(0) == 'F' && displayNames[i].charAt(3) == '_')
								n = displayNames[i].substring(4);
							else
								n = displayNames[i];
						}
						writer.write(n + delimeter);
					}
				}
				writer.write("\r\n");
				// Set date format to a standard format
				SimpleDateFormat outputDateFormat = new SimpleDateFormat("yyyy-MM-dd HHH:mm:ss.SSS");
				TimeZone userTZ = null;
				if (userDisplayTimezone != null && !userDisplayTimezone.isEmpty())
				{
					userTZ = TimeZoneUtil.getTimeZoneForWindowsTZID(userDisplayTimezone);
				}
				if (userTZ == null)
				{
					userTZ = r.getServerParameters().getDisplayTimeZone();
				}
				outputDateFormat.setTimeZone(userTZ);
				int numRows = 0;
				for (Object[] row : qrs.getRows())
				{
					boolean first = true;
					for (int i = 0; i < row.length; i++)
					{
						if (displayNames[i] != null)
						{
							if (!first)
								writer.write(delimeter);
							if (row[i] != null)
							{
								if (GregorianCalendar.class.isInstance(row[i]))
								{
									String s = outputDateFormat.format(((Calendar) row[i]).getTime());
									writer.write(s);
								} else
									writer.write(Util.quote(row[i].toString(), '\t'));
							}
							first = false;
						}
					}
					if (!first)
						writer.write("\r\n");
					numRows++;
					if (maxRecords != -1 && numRows >= maxRecords)
						break;
				}
			}
		} finally
		{
			try
			{
				if (writer != null)
					writer.close();
				if (zos != null)
					zos.close();
				if (fos != null)
					fos.close();
				if (isRserver && rfos != null)
					rfos.close();
			} catch (IOException e)
			{
				logger.debug(e, e);
			}
		}
	}

	@SuppressWarnings("static-access")
	public ExecuteResult exportBulkReportData(String path, String spaceID, String username, String userDisplayTimezone, String report, int maxRecords,
			String filename, boolean useLogicalQuery, String[] packageSpacesProperties)
	{
		ApplicationContext ac = null;
		ExecuteResult er = checkValidIP("exportBulkReportData");
		Session session = null;
		try
		{
			MDC.put("username", username);
			MDC.put("spaceID", spaceID);
			long start = System.currentTimeMillis();
			if (er.getErrorCode() != BaseException.SUCCESS)
				return er;
			ac = ApplicationContext.getInstance(path, Util.getPackageSpacePropertiesList(packageSpacesProperties));
			Repository r = ac.getRepository();
			if (r == null)
			{
				logger.debug("Unable to load repository");
				er.setErrorCode(BaseException.ERROR_OTHER);
				er.setErrorMessage("Unable to load configuration metadata");
				return er;
			}
			String catalogPath = r.getServerParameters().getCatalogPath();
			if (catalogPath == null)
				catalogPath = r.getServerParameters().getApplicationPath() + "\\catalog";
			if (catalogPath != null && report != null)
				report = report.replace("V{CatalogDir}", catalogPath);
			// set up the session for the user to get the various DEFAULT session variables
			User u = r.findUser(username);
			if (u == null)
			{
				logger.debug("Unable to find user");
				er.setErrorCode(BaseException.ERROR_OTHER);
				er.setErrorMessage("Unable to load user");
				return er;
			}
			SMIWebUserBean ub = new SMIWebUserBean();
			ub.setApplicationContext(ac);
			ub.setUser(u);
			session = new Session(username);
			session.clear();
			SMIWebUserBean.setHttpServletRequest(getHttpServletRequest());
			ub.setRepository(r);
			SMIWebUserBean.addThreadRequest(Thread.currentThread().getId(), ub);
			JasperDataSourceProvider jdsp = null;
			String queryString = null;
			File reportFile = CatalogManager.getAdhocReportFile(report, catalogPath);
			AdhocReport ar = null;
			try
			{
				if (reportFile != null)
				{
					ar = AdhocReport.read(reportFile);
					queryString = ar.getQueryString(r, true);
				} else
				{
					JasperDesign jrxmlDesign = JRXmlLoader.load(report);
					queryString = jrxmlDesign.getQuery().getText();
				}
			} catch (Exception e)
			{
				logger.error("Problem loading the report file: " + report, e);
				er.setErrorCode(BaseException.ERROR_JASPER_OTHER);
				er.setErrorMessage("Problem was encountered loading the report file: " + FileUtils.getFileBasename(report));
				return er;
			}
			logger.info("Exporting report " + report + " (to a zip file) for repository " + spaceID);
			ResultSetCache rsc = ResultSetCache.getInstance(r.getApplicationIdentifier(), false);
			jdsp = new JasperDataSourceProvider(r, rsc);
			session.fillSessionVariables(r, rsc);
			logger.info("Session variables (TrustedService:exportData): " + session.toString());
			session.setParams(new ArrayList<Filter>());
			// create the report
			jdsp.setSession(session);
			jdsp.setMaxRecords(maxRecords);
			jdsp.setExecutePhysicalQueryOnly(!useLogicalQuery);
			int queryTimeOut = 30 * 60 * 1000;// 30 minutes timeout for query execution
			jdsp.setQueryTimeOut(queryTimeOut);
			jdsp.setUserDisplayTimezone(userDisplayTimezone);
			jdsp.create(queryString);
			if (filename == null || filename.isEmpty())
			{
				int index = report.lastIndexOf('\\');
				filename = report.substring(index + 1);
				index = filename.lastIndexOf('.');
				if (index > 0)
					filename = filename.substring(0, index);
			}
			if (useLogicalQuery)
			{
				QueryResultSet qrs = jdsp.getQueryResultSet();
				writeQRStoFile(path, filename, userDisplayTimezone, r, qrs, maxRecords, ar, null, '\t');
				if (qrs != null)
					er.setNumInputRows(qrs.numRows());
				else
					er.setNumInputRows(0);
			} else
			{
				// Only generate and execute physical query and capture results to a file
				logger.warn("Exporting Query Data - only physical query will be exported, display expressions, display filters and display ordres will not be processed.");
				long numRows = jdsp.executeAndPersistPhysicalQueryToFile(path + "\\data\\export\\" + filename + ".zip");
				er.setNumInputRows(numRows);
			}
			AuditLog.log("bulk", report, null, System.currentTimeMillis() - start);
		} catch (Exception ex)
		{
			logger.debug(ex, ex);
			er.setErrorCode(BaseException.ERROR_OTHER);
			if (ex.getMessage() != null && !ex.getMessage().isEmpty())
				er.setErrorMessage(ex.getMessage());
		} finally
		{
			MDC.remove("username");
			MDC.remove("spaceID");
			SMIWebUserBean.removeThreadRequest(Thread.currentThread().getId());
			if (session != null)
			{
				session.setCancelled(true);
			}
			if (ac != null)
				ac.dispose();
		}
		return er;
	}

	@SuppressWarnings("static-access")
	public ExecuteResult exportData(String path, String spaceID, String username, String userDisplayTimezone, String report, int maxRecords, String rServerPath,
			String[] packageSpacesProperties)
	{
		ApplicationContext ac = null;
		ExecuteResult er = checkValidIP("exportData");
		Session session = null;
		try
		{
			MDC.put("username", username);
			MDC.put("spaceID", spaceID);
			long start = System.currentTimeMillis();
			if (er.getErrorCode() != BaseException.SUCCESS)
				return er;
			ac = ApplicationContext.getInstance(path, Util.getPackageSpacePropertiesList(packageSpacesProperties));
			Repository r = ac.getRepository();
			if (r == null)
			{
				logger.debug("Unable to load repository");
				er.setErrorCode(BaseException.ERROR_OTHER);
				er.setErrorMessage("Unable to load configuration metadata");
				return er;
			}
			String catalogPath = r.getServerParameters().getCatalogPath();
			if (catalogPath == null)
				catalogPath = r.getServerParameters().getApplicationPath() + "\\catalog";
			if (catalogPath != null && report != null)
				report = report.replace("V{CatalogDir}", catalogPath);
			// set up the session for the user to get the various DEFAULT session variables
			User u = r.findUser(username);
			if (u == null)
			{
				logger.debug("Unable to find user");
				er.setErrorCode(BaseException.ERROR_OTHER);
				er.setErrorMessage("Unable to load user");
				return er;
			}
			SMIWebUserBean ub = new SMIWebUserBean();
			ub.setApplicationContext(ac);
			ub.setUser(u);
			session = new Session(username);
			session.clear();
			SMIWebUserBean.setHttpServletRequest(getHttpServletRequest());
			ub.setRepository(r);
			SMIWebUserBean.addThreadRequest(Thread.currentThread().getId(), ub);
			JasperDataSourceProvider jdsp = null;
			String queryString = null;
			File reportFile = CatalogManager.getAdhocReportFile(report, catalogPath);
			AdhocReport ar = null;
			try
			{
				if (reportFile != null)
				{
					ar = AdhocReport.read(reportFile);
					queryString = ar.getQueryString(r, true);
				} else
				{
					JasperDesign jrxmlDesign = JRXmlLoader.load(report);
					queryString = jrxmlDesign.getQuery().getText();
				}
			} catch (Exception e)
			{
				logger.error("Problem loading the report file: " + report, e);
				er.setErrorCode(BaseException.ERROR_JASPER_OTHER);
				er.setErrorMessage("Problem was encountered loading the report file: " + FileUtils.getFileBasename(report));
				return er;
			}
			logger.info("Exporting report " + report + " (to a zip file) for repository " + spaceID);
			ResultSetCache rsc = ResultSetCache.getInstance(r.getApplicationIdentifier(), false);
			jdsp = new JasperDataSourceProvider(r, rsc);
			session.fillSessionVariables(r, rsc);
			logger.info("Session variables (TrustedService:exportData): " + session.toString());
			session.setParams(new ArrayList<Filter>());
			// create the report
			jdsp.setSession(session);
			jdsp.setMaxRecords(maxRecords);
			jdsp.setUserDisplayTimezone(userDisplayTimezone);
			jdsp.create(queryString);
			int index = report.lastIndexOf('\\');
			String name = report.substring(index + 1);
			index = name.lastIndexOf('.');
			if (index > 0)
				name = name.substring(0, index);
			QueryResultSet qrs = jdsp.getQueryResultSet();
			er.setPart(rServerPath);
			writeQRStoFile(path, name, userDisplayTimezone, r, qrs, maxRecords, ar, rServerPath, rServerPath != null && rServerPath.trim().length() > 0 ? '|' : '\t');
			if (qrs != null)
				er.setNumInputRows(qrs.numRows());
			else
				er.setNumInputRows(0);
			AuditLog.log("bulk", report, null, System.currentTimeMillis() - start);
		} catch (Exception ex)
		{
			logger.debug(ex, ex);
			er.setErrorCode(BaseException.ERROR_OTHER);
			if (ex.getMessage() != null && !ex.getMessage().isEmpty())
				er.setErrorMessage(ex.getMessage());
		} finally
		{
			MDC.remove("username");
			MDC.remove("spaceID");
			SMIWebUserBean.removeThreadRequest(Thread.currentThread().getId());
			if (session != null)
			{
				session.setCancelled(true);
			}
			if (ac != null)
				ac.dispose();
		}
		return er;
	}

	public QueryResult getQueryResults(String path, String spaceID, String query, String username, String[] packageSpacesProperties)
	{
		ApplicationContext ac = null;
		Session session = null;
		try
		{
			MDC.put("username", username);
			MDC.put("spaceID", spaceID);
			ExecuteResult er = checkValidIP("getQueryResults");
			QueryResult qr = new QueryResult();
			if (er.getErrorCode() != BaseException.SUCCESS)
			{
				qr.setErrorCode(er.getErrorCode());
				return qr;
			}
			ac = ApplicationContext.getInstance(path, Util.getPackageSpacePropertiesList(packageSpacesProperties));
			Repository r = ac.getRepository();
			if (r == null)
			{
				return qr;
			}
			session = new Session(username);
			ResultSetCache rsc = ResultSetCache.getInstance(r.getApplicationIdentifier(), false);
			session.fillSessionVariables(r, rsc);
			logger.info("Session variables (TrustedService:getQueryResults): " + session.toString());
			TimeZone userTZ = r.getServerParameters().getDisplayTimeZone();
			LogicalQueryString lqs = new LogicalQueryString(r, session, query);
			Query q = lqs.getQuery();
			QueryResultSet qrs = rsc.getQueryResultSet(q, r.getServerParameters().getMaxRecords(), r.getServerParameters().getMaxQueryTime(),
					lqs.getDisplayFilters(), lqs.getExpressionList(), lqs.getDisplayOrderList(), session, false, r.getServerParameters().getDisplayTimeZone(),
					false);
			StringBuilder sb = new StringBuilder();
			Object[][] rows = qrs.getRows();
			for (int row = 0; row < rows.length; row++)
			{
				for (int col = 0; col < rows[row].length; col++)
				{
					if (col > 0)
						sb.append('|');
					Object o = rows[row][col];
					if (o != null)
					{
						String s;
						if (GregorianCalendar.class.isInstance(o))
						{
							s = DateUtil.convertToDBFormat(((GregorianCalendar) o).getTime(), userTZ);
						} else
						{
							s = o.toString();
						}
						s = Util.replaceStr(s, "|", "\\|");
						sb.append(s);
					}
				}
				sb.append("\r");
			}
			qr.setRows(sb.toString());
			qr.setDataTypes(qrs.getColumnDataTypes());
			qr.setColumnNames(qrs.getColumnNames());
			qr.setDisplayNames(qrs.getDisplayNames());
			return qr;
		} catch (Exception ex)
		{
			logger.error(ex.getMessage(), ex);
			return null;
		} finally
		{
			MDC.remove("username");
			MDC.remove("spaceID");
			if (session != null)
			{
				session.setCancelled(true);
			}
			if (ac != null)
				ac.dispose();
		}
	}

	public QueryResult getJdbcQueryResults(String driver, String connUrl, String user, String pwd, String query)
	{
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		QueryResult qr = null;
		try
		{
			qr = new QueryResult();
			if(!isTrustedRequest("getJdbcQueryResults"))
			{
				qr.setErrorCode(BaseException.ERROR_OTHER);
				return qr;
			}
			// Set up JDBC connection.
			driver = DatabaseConnection.fixupDriverClassName(driver); // remove any database type information encoded in
																		// the driver class name
			Class.forName(driver);
			conn = DriverManager.getConnection(connUrl, user, pwd);
			stmt = conn.prepareStatement(query);
			rs = stmt.executeQuery();
			// Iterate over results and build string version of results.
			StringBuilder sb = new StringBuilder();			
			int colCnt = rs.getMetaData().getColumnCount();
			while (rs.next())
			{
				for (int colNum = 1; colNum <= colCnt; colNum++)
				{
					sb.append(rs.getString(colNum)).append("|");
				}
				sb.append("\r\n");
			}
			// Wrap in QueryResult construct.
			qr.setRows(sb.toString());
		} catch (Exception ex)
		{
			logger.error(ex.getMessage(), ex);
			return null;
		} finally
		{
			close(conn, stmt, rs);
		}
		return qr;
	}

	private static void close(Connection conn, Statement stmt, ResultSet rs)
	{
		if (rs != null)
		{
			try
			{
				rs.close();
			} catch (Exception ex)
			{
				logger.warn(ex.getMessage(), ex);
			}
		}
		if (stmt != null)
		{
			try
			{
				stmt.close();
			} catch (Exception ex)
			{
				logger.warn(ex.getMessage(), ex);
			}
		}
		if (conn != null)
		{
			try
			{
				conn.close();
			} catch (Exception ex)
			{
				logger.warn(ex.getMessage(), ex);
			}
		}
	}

	public ExecuteResult getCustomSubjectAreasList(String spaceDirectory, String username, String spaceID)
	{
		ExecuteResult executeResult = checkValidIP("getCustomSubjectAreasList");
		try
		{
			MDC.put("username", username);
			MDC.put("spaceID", spaceID);
			if (executeResult.getErrorCode() != BaseException.SUCCESS)
				return executeResult;
			// no need to initialize repository and a performance hit
			List<Element> csaElements = SubjectAreaManager.getCustomSubjectAreasNodes(spaceDirectory);
			Set<String> csaNames = new HashSet<String>();
			if (csaElements != null && csaElements.size() > 0)
			{
				for (Element csaElement : csaElements)
				{
					String csaName = csaElement.getChildText(ExtendedSubjectAreaNode.LABEL);
					if (csaName != null)
					{
						csaNames.add(csaName);
					}
				}
			}
			executeResult.setGroups(csaNames.toArray(new String[csaNames.size()]));
			return executeResult;
		} catch (Exception e)
		{
			logger.debug(e, e);
			return null;
		} finally
		{
			MDC.remove("username");
			MDC.remove("spaceID");
		}
	}

	/**
	 * For a specified user and space, this webservice returns a list of the user's dynamic groups.
	 * 
	 * @param applicationRoot
	 *            is the full file path for the space in question; e.g.,
	 * @param spaceID
	 *            is the String-ified Guid of the space in question.
	 * @param username
	 *            is the email address of the user whose groups are being requested.
	 * @return an instance of ExecuteResult that contains the list of the user's group names for this space.
	 */
	@SuppressWarnings("unchecked")
	public ExecuteResult getDynamicGroups(String path, String username, String spaceID)
	{
		ApplicationContext ac = null;
		ExecuteResult executeResult = checkValidIP("getDynamicGroups");
		Session session = null;
		try
		{
			MDC.put("username", username);
			MDC.put("spaceID", spaceID);
			if (executeResult.getErrorCode() != BaseException.SUCCESS)
				return executeResult;
			ac = ApplicationContext.getInstance(path, null);
			Repository r = ac.getRepository();
			if (r == null)
				return executeResult;
			// Retrieve the query string from Variable: "Birst$Groups"
			final Variable variable = r.findVariable("Birst$Groups");
			// No dynamic group variable defined. Return empty list.
			if (variable == null)
			{
				executeResult.setErrorMessage("Dynamic groups requested but 'Manage Groups' definition was not found.");
				executeResult.setGroups(new String[0]);
				return executeResult;
			}
			// Run the query
			session = new Session(username);
			JasperDataSourceProvider jdsp = new JasperDataSourceProvider(r);
			Object o = r.fillVariable(null, variable, session, jdsp);
			if (o == null)
			{
				executeResult.setErrorMessage("Dynamic groups query returned 'null' result.");
				executeResult.setGroups(new String[0]);
				return executeResult;
			}
			// Filter out groups not in repository.xml and return
			List<String> groupNames = (List<String>) o;
			List<String> filteredGroupNames = filterNonexistentGroups(groupNames, r.getGroups());
			String[] groups = new String[filteredGroupNames.size()];
			groups = filteredGroupNames.toArray(groups);
			executeResult.setGroups(groups);
			return executeResult;
		} catch (Exception e)
		{
			logger.debug(e, e);
			return null;
		} finally
		{
			MDC.remove("username");
			MDC.remove("spaceID");
			if (session != null)
			{
				session.setCancelled(true);
			}
			if (ac != null)
				ac.dispose();
		}
	}

	public ExecuteResult validateFilterExpression(String username, String spaceID, String path, String filterExpression, String[] packageSpacesProperties)
	{
		ExecuteResult response = checkValidIP("validateFilterExpression");
		if (response.getErrorCode() == BaseException.SUCCESS)
		{
			try
			{
				MDC.put("username", username);
				MDC.put("spaceID", spaceID);
				Repository repository = ApplicationContext.loadRepository(path, "prod", Util.getPackageSpacePropertiesList(packageSpacesProperties));
				if (repository == null)
				{
					response.setErrorCode(BaseException.ERROR_OTHER);
					response.setErrorMessage("Unable to initialize filter validation");
					return response;
				}
				repository.buildDimensionLevelMap();
				repository.instantiateRepositoryVariables(null, new JasperDataSourceProvider(repository));
				if (filterExpression != null && filterExpression.trim().length() > 0)
				{
					new DisplayExpression(repository, filterExpression, null);
				}
			} catch (Exception ex)
			{
				logger.error("Exception during validation " + ex);
				if (ex instanceof BaseException)
				{
					String msg = ex.getMessage();
					int errorCode = ((BaseException) ex).getErrorCode();
					if (ex instanceof BadColumnNameException)
					{
						response.setErrorCode(BaseException.ERROR_BAD_COLUMN);
						msg = "Bad Column Name : " + msg;
					}
					if (ex instanceof NavigationException)
					{
						msg = "Navigation Error: " + msg;
					}
					response.setErrorCode(errorCode);
					response.setErrorMessage(msg);
				} else
				{
					response.setErrorCode(BaseException.ERROR_OTHER);
					response.setErrorMessage("Validation Error");
				}
			} finally
			{
				MDC.remove("username");
				MDC.remove("spaceID");
			}
		}
		return response;
	}

	public ExecuteResult validateQuery(String username, String spaceID, String path, String query, String filterExpression, String buildFilterExpression,
			String connectionName, String[] packageSpacesProperties)
	{
		ExecuteResult response = checkValidIP("validateQuery");
		if (response.getErrorCode() == BaseException.SUCCESS)
		{
			boolean executedFilterExpression = false;
			boolean executedBuildFilterExpression = false;
			try
			{
				MDC.put("username", username);
				MDC.put("spaceID", spaceID);
				Repository repository = ApplicationContext.loadRepository(path, "prod", Util.getPackageSpacePropertiesList(packageSpacesProperties));
				if (repository == null)
				{
					response.setErrorCode(BaseException.ERROR_OTHER);
					response.setErrorMessage("Unable to initialize query validation");
					return response;
				}
				repository.buildDimensionLevelMap();
				repository.instantiateRepositoryVariables(null, new JasperDataSourceProvider(repository));
				String query2 = QueryString.preProcessQueryString(query, repository, null);
				AbstractQueryString aqs = AbstractQueryString.getInstance(repository, query2);
				Query q = aqs.getQuery();
				List<String> addedNames = new ArrayList<String>();
				List<DisplayExpression> expressionList = aqs.getExpressionList();
				if (expressionList != null)
				{
					for (DisplayExpression de : expressionList)
					{
						de.extractAllExpressions(q, addedNames);
					}
				}
				q.navigateQuery(true, false, null, true);
				if (filterExpression != null && filterExpression.trim().length() > 0)
				{
					executedFilterExpression = true;
					new DisplayExpression(repository, filterExpression, null);
				}
				if (buildFilterExpression != null && !buildFilterExpression.isEmpty())
				{
					executedBuildFilterExpression = true;
					new DisplayExpression(repository, buildFilterExpression, null);
				}
				if (connectionName == null || connectionName.isEmpty())
				{
					logger.debug("Database connection not provided. Using default connection to validate query " + query);
					connectionName = Repository.DEFAULT_DB_CONNECTION;
				}
				List<DatabaseConnection> connectionList = q.getConnections();
				if (!Util.containsOnlyConnection(connectionList, connectionName))
				{
					if (connectionList != null)
					{
						if (connectionList.size() > 1)
							logger.debug("Validate Query \"" + query
									+ "\" returns false because query navigation connection list size should be one and found " + connectionList.size());
						else
							logger.debug("Validate Query \"" + query + "\" returns false because supplied query connection \"" + connectionName
									+ "\" not matching with query navigation connection \"" + connectionList.get(0).Name + "\"");
					}
					DatabaseConnection dc = repository.findConnection(connectionName);
					String connectionVisibleName = dc != null ? dc.VisibleName : connectionName;
					response.setErrorCode(BaseException.ERROR_OTHER);
					response.setErrorMessage("Not able to validate query using \"" + connectionVisibleName + "\" connection");
				}
			} catch (Exception ex)
			{
				String messagePrefix = executedBuildFilterExpression ? "ERROR_BFE-" : executedFilterExpression ? "ERROR_FE-" : "";
				logger.error("Exception during validation " + ex);
				if (ex instanceof BaseException)
				{
					String msg = ex.getMessage();
					int errorCode = ((BaseException) ex).getErrorCode();
					if (ex instanceof BadColumnNameException)
					{
						response.setErrorCode(BaseException.ERROR_BAD_COLUMN);
						msg = "Bad Column Name : " + msg;
					}
					if (ex instanceof NavigationException)
					{
						msg = "Navigation Error: " + msg;
					}
					response.setErrorCode(errorCode);
					response.setErrorMessage(messagePrefix + msg);
				} else
				{
					response.setErrorCode(BaseException.ERROR_OTHER);
					response.setErrorMessage(messagePrefix + "Validation Error");
				}
			} finally
			{
				MDC.remove("username");
				MDC.remove("spaceID");
			}
		}
		return response;
	}

	/**
	 * Removes any dynamic group names if they are not defined in the repository. The {@link Repository#getGroups()} is
	 * used to retrieve the Groups[] array of groups defined in the repository.xml file. Repository ACLs are not
	 * examined at this point.
	 * 
	 * @param groupNames
	 *            the list of group names to filter.
	 * @param existingGroups
	 *            the list of group names currently defined in the repository.
	 * @return the list of dynamic groups that are also defined in the repository.
	 */
	private List<String> filterNonexistentGroups(final List<String> groupNames, final Group[] existingGroups)
	{
		final List<String> filteredGroupNames = new ArrayList<String>();
		for (String groupName : groupNames)
		{
			for (Group existingGroup : existingGroups)
			{
				if (groupName.equals(existingGroup.getName()))
				{
					filteredGroupNames.add(groupName);
					break;
				}
			}
		}
		return filteredGroupNames;
	}

	/**
	 * Clears the MongoDB db files and query file caches if we're using MongoDB Current usage is by Acorn when removing
	 * space. Bug 10925
	 * 
	 * @param repositoryId
	 */
	public ExecuteResult clearMongoDBAndFileCaches(String repositoryId)
	{
		ExecuteResult er = checkValidIP("clearMongoDBAndFileCaches");
		if (er.getErrorCode() != BaseException.SUCCESS)
		{
			logger.error("Clear MongoDB Cache Request not from a valid IP. Skipping the task");
			return er;
		}
		logger.info("Clearing MongoDB and file caches for repository: " + repositoryId);
		ResultSetCache rs = ResultSetCache.getInstance(repositoryId, false);
		rs.resetCache();
		return er;
	}

	/*
	 * return the log file name for this server
	 */
	public String getLogFilename()
	{
		String filename = null;
		try
		{
			ExecuteResult er = checkValidIP("getLogFilename");
			if (er.getErrorCode() != BaseException.SUCCESS)
			{
				throw new UnAuthorizedIPSourceException();
			}
			filename = LogUtil.getLogfileName();
		} catch (Exception ex)
		{
			logger.debug(ex, ex);
		}
		return filename;
	}

	public ExecuteResult updatePackageSpacesInfo(String path, String[] updatedSpacesProperties)
	{
		ExecuteResult response = null;
		try
		{
			response = checkValidIP("updatePackageSpacesInfo");
			if (response != null && response.getErrorCode() == BaseException.SUCCESS)
			{
				ApplicationContext ac = ApplicationContext.getApplicationContextIfAvailable(path);
				if (ac != null)
				{
					ac.updatePackageSpacesProperties(Util.getPackageSpacePropertiesList(updatedSpacesProperties));
				}
			}
		} catch (Exception ex)
		{
			logger.error("Error in updating updatePackageSpacesInfo", ex);
			response.setErrorMessage(ex.getMessage());
		}
		return response;
	}

	/**
	 * Execute the scheduled report in asynchronous mode. It returns a unique identifier which can be used to check the
	 * status of report
	 * 
	 * @param path
	 * @param spaceID
	 * @param type
	 * @param name
	 * @param username
	 * @param from
	 * @param subject
	 * @param body
	 * @param report
	 * @param toList
	 * @param toReport
	 * @param triggerReport
	 * @param maxReports
	 * @param reportLocale
	 * @param reportTimeZone
	 * @return
	 */
	public OMElement scheduleExportReport(String path, String spaceID, String type, String name, String username, String from, String subject, String body,
			String report, String[] toList, String toReport, String triggerReport, int maxReports, String reportLocale, String reportTimeZone,
			String compressionFormat, String csvSeparator, String csvExtension, String mailHost, String promptsXmlString, String overrideVariablesXmlString,
			String[] packageSpacesProperties)
	{
		BirstWebServiceResult result = new BirstWebServiceResult();
		try
		{
			MDC.put("username", username);
			MDC.put("spaceID", spaceID);
			ExecuteResult er = checkValidIP("scheduleExportReport");
			if (er.getErrorCode() != BaseException.SUCCESS)
			{
				throw new UnAuthorizedIPSourceException();
			}
			// String mailHost = "192.168.90.100";
			String uuid = ExportReportManager.queueExportReport(mailHost, path, spaceID, type, name, username, from, subject, body, report, toList, toReport,
					triggerReport, maxReports, reportLocale, reportTimeZone, csvSeparator, csvExtension, compressionFormat, promptsXmlString,
					overrideVariablesXmlString, packageSpacesProperties);
			result.setResult(new StringWebServiceResult(wrapInExportReportRoot(buildUIDXMLString(uuid))));
		} catch (Exception ex)
		{
			result.setException(ex);
		}
		finally
		{
			MDC.remove("username");
			MDC.remove("spaceID");
		}
		return result.toOMElement();
	}

	/**
	 * Returns the status of the scheduled report
	 * 
	 * @param uuid
	 *            - unique identifier to identify report
	 * @return
	 */
	public OMElement getExportReportStatus(String uuid)
	{
		BirstWebServiceResult result = new BirstWebServiceResult();
		try
		{
			ExecuteResult er = checkValidIP("getExportReportStatus");
			if (er.getErrorCode() != BaseException.SUCCESS)
			{
				throw new UnAuthorizedIPSourceException();
			}
			ExportReportThread reportThread = null;
			reportThread = ExportReportManager.getExportReportThread(uuid);
			ReportStatus reportStatus = null;
			if (reportThread != null)
			{
				reportStatus = reportThread.getStatus();
			} else
			{
				String msg = "Report Not found ";
				logger.debug(msg + " : " + uuid);
				result.setErrorCode(BaseException.ERROR_OTHER);
				result.setErrorMessage(msg);
			}
			result.setResult(new StringWebServiceResult(wrapInExportReportRoot(buildReportStatus(uuid, reportStatus))));
			// clear the status from stored hash map if the export report is finished
			if (reportStatus != null && (reportStatus == ReportStatus.COMPLETE || reportStatus == ReportStatus.FAILED))
			{
				ExportReportManager.removeExportReportThread(uuid);
			}
		} catch (Exception ex)
		{
			logger.error("Exception in getting export report status for " + uuid, ex);
			result.setException(ex);
		}
		return result.toOMElement();
	}

	public OMElement startTimeCreation(String username, String timeRepositoryPath, String dbType, String folderName, String awsAccessKeyId,
			String awsSecretKey, String awsToken)
	{
		BirstWebServiceResult response = new BirstWebServiceResult();
		try
		{
			ExecuteResult er = checkValidIP("startTimeCreation");
			if (er.getErrorCode() != BaseException.SUCCESS)
			{
				throw new UnAuthorizedIPSourceException();
			}
			logUserInfo(username);
			logger.info("Time Creation Request " + timeRepositoryPath + " by user " + username);
			TimeCreationManager tcManager = new TimeCreationManager(timeRepositoryPath);
			if (dbType.equalsIgnoreCase(DatabaseType.Redshift.toString()))
			{
				RedshiftTimeCredentials params = new RedshiftTimeCredentials();
				params.setFolderName(Util.nullifyString(folderName));
				params.setAwsAccessKeyId(Util.nullifyString(awsAccessKeyId));
				params.setAwsSecretKey(Util.nullifyString(awsSecretKey));
				params.setAwsToken(Util.nullifyString(awsToken));
				tcManager.setTimeAccessParams(params);
			}else if(dbType.equalsIgnoreCase("SAP Hana"))
			{
				SAPHanaTimeAccessParams params = new SAPHanaTimeAccessParams(Util.nullifyString(folderName));
				tcManager.setTimeAccessParams(params);
			}
			tcManager.startTableCreation();
		} catch (Exception ex)
		{
			response.setException(ex);
		} finally
		{
			removeUserInfo();
		}
		return response.toOMElement();
	}

	private String buildUIDXMLString(String uuid)
	{
		if (uuid == null || uuid.length() == 0)
		{
			return "</uid>";
		}
		return "<uid>" + uuid + "</uid>";
	}

	private String buildReportStatus(String uuid, ReportStatus status)
	{
		StringBuilder builder = new StringBuilder();
		builder.append(buildUIDXMLString(uuid));
		builder.append("<status>");
		if (status != null)
		{
			builder.append(status.toString());
		}
		builder.append("</status>");
		return builder.toString();
	}

	private String wrapInExportReportRoot(String str)
	{
		return "<ExportReportStatus>" + str + "</ExportReportStatus>";
	}

	public FlowResult getDataFlow(String path, String spaceID, String username, String loadGroup, String[] packageSpacesProperties, String[] subGroups)
	{
		try
		{
			MDC.put("username", username);
			MDC.put("spaceID", spaceID);
			ExecuteResult er = checkValidIP("getDataFlow");
			FlowResult fr = new FlowResult();
			if (er.getErrorCode() != BaseException.SUCCESS)
			{
				fr.setErrorCode(er.getErrorCode());
				return fr;
			}
			Repository r = getRepository(path, er, Util.getPackageSpacePropertiesList(packageSpacesProperties));
			if (r == null)
			{
				fr.setErrorCode(BaseException.ERROR_OTHER);
				fr.setErrorMessage("Unable to initialize data flow");
				return fr;
			}
			r.processImports();
			List<StagingTable> stlist = new ArrayList<StagingTable>();
			Set<StagingTable> stset = new HashSet<StagingTable>();
			for (StagingTable st : r.getStagingTables())
			{
				boolean belongs = st.belongsToLoadGroup(loadGroup);
				if (!belongs && loadGroup != null && loadGroup.length() > 0)
					continue;
				belongs = st.belongsToSubGroups(subGroups);
				if (!belongs && subGroups != null && subGroups.length > 0)
					continue;
				st.setRepository(r);
				stlist.add(st);
				stset.add(st);
			}
			List<String[]> dependencies = new ArrayList<String[]>();
			for (StagingTable st : stlist)
			{
				if (st.Script != null && st.Script.InputQuery != null && st.Script.Script != null && st.Script.Script.length() > 0)
				{
					TransformationScript sc = new TransformationScript(r, null);
					try
					{
						ParseResult pr = sc.parseScript(st.Script, 0, null);
						for (Object source : sc.getSourceTables())
						{
							if (source instanceof StagingTable)
							{
								dependencies.add(new String[]
								{ ((StagingTable) source).Name, st.Name });
							} else if (source instanceof MeasureTable)
							{
								Collection<MeasureTableGrain> grainInfo = ((MeasureTable) source).getGrainInfo(r);
								String grainStr = "";
								for (MeasureTableGrain mtg : grainInfo)
								{
									if (grainStr.length() > 0)
										grainStr += ",";
									grainStr += mtg.DimensionName + "." + mtg.DimensionLevel;
								}
								dependencies.add(new String[]
								{ grainStr, st.Name });
							} else if (source instanceof DimensionTable)
							{
								dependencies.add(new String[]
								{ ((DimensionTable) source).DimensionName + "." + ((DimensionTable) source).Level, st.Name });
							}
						}
						if (pr.ScriptError == null && pr.InputError == null && pr.OutputError == null && stset.contains(st))
							stset.remove(st);
					} catch (Exception ex)
					{
						logger.info(ex.getMessage());
					}
				}
			}
			for (StagingTable st : stset)
			{
				dependencies.add(new String[]
				{ null, st.Name });
			}
			// Convert to output
			String[] sourcearr = new String[dependencies.size()];
			String[] targetarr = new String[dependencies.size()];
			for (int i = 0; i < dependencies.size(); i++)
			{
				sourcearr[i] = dependencies.get(i)[0];
				targetarr[i] = dependencies.get(i)[1];
			}
			fr.setSources(sourcearr);
			fr.setTargets(targetarr);
			return fr;
		} catch (Exception ex)
		{
			logger.error(ex.getMessage(), ex);
			return null;
		} finally
		{
			MDC.remove("username");
			MDC.remove("spaceID");
		}
	}

	public OMElement importLiveAccessMetaDataUsingDirectConnection(String userName, String spaceID, String path, String realtimeConnection, String command,
			String commandArgs)
	{
		ApplicationContext ac = null;
		try
		{
			logUserSpaceInfo(userName, spaceID);
			if(!isTrustedRequest("importLiveAccessMetaDataUsingDirectConnection"))
			{
				return (new DirectConnectUtil()).getResponseError(ERROR_INVALID_IP);
			}
			ac = ApplicationContext.getInstance(path, null);
			Repository r = ac.getRepository();
			Session session = null;
			try
			{
				session = new Session(userName);
				session.setRepository(r);
				session.fillSessionVariables(r, ac.getCache());
			} catch (SQLException sqle)
			{
				logger.error(sqle.toString(), sqle);
			}
			return (new DirectConnectUtil()).importLiveAccessMetaDataUsingDirectConnection(userName, spaceID, r, realtimeConnection, command, commandArgs,
					session);
		} finally
		{
			if (ac != null)
				ac.dispose();
			removeUserSpaceInfo(userName, spaceID);
		}
	}

	public OMElement getConnectorsList(String userName, String spaceID)
	{
		try
		{
			logUserSpaceInfo(userName, spaceID);
			BirstWebServiceResult result = new BirstWebServiceResult();
			if(!isTrustedRequest("getConnectorsList", result))
			{
				return result.toOMElement();
			}
			return (new ConnectorServices()).getConnectorsList(userName, spaceID);
		} finally
		{
			removeUserSpaceInfo(userName, spaceID);
		}
	}
	
	public OMElement getCloudConnectorConfigXML(String userName, String spaceID, String spaceDirectory)
	{
		try
		{
			logUserSpaceInfo(userName, spaceID);
			BirstWebServiceResult result = new BirstWebServiceResult();
			if(!isTrustedRequest("getCloudConnectorConfigXML", result))
			{
				return result.toOMElement();
			}
			return (new ConnectorServices()).getCloudConnectorConfigXML(userName, spaceID, spaceDirectory);
		} finally
		{
			removeUserSpaceInfo(userName, spaceID);
		}		
	}
	
	// Connector related services
	public OMElement getConnectorCredentials(String userName, String spaceID, String spaceDirectory, String connectorName)
	{
		try
		{
			logUserSpaceInfo(userName, spaceID);
			BirstWebServiceResult result = new BirstWebServiceResult();
			if(!isTrustedRequest("getConnectorCredentials", result))
			{
				return result.toOMElement();
			}
			return (new ConnectorServices()).getConnectorCredentials(spaceID, spaceDirectory, connectorName);
		} finally
		{
			removeUserSpaceInfo(userName, spaceID);
		}
	}

	public OMElement saveConnectorCredentials(String userName, String spaceID, String spaceDirectory, String connectorName, String xmlData)
	{
		try
		{
			logUserSpaceInfo(userName, spaceID);
			BirstWebServiceResult result = new BirstWebServiceResult();
			if(!isTrustedRequest("saveConnectorCredentials", result))
			{
				return result.toOMElement();
			}
			return (new ConnectorServices()).saveConnectorCredentials(userName, spaceID, spaceDirectory, connectorName, xmlData);
		} finally
		{
			removeUserSpaceInfo(userName, spaceID);
		}
	}
	
	public OMElement testConnection(String userName, String spaceID, String spaceDirectory, String connectorConnectionString, String credentialsXML)
	{
		try
		{
			logUserSpaceInfo(userName, spaceID);
			BirstWebServiceResult result = new BirstWebServiceResult();
			if(!isTrustedRequest("testConnection", result))
			{
				return result.toOMElement();
			}
			return (new ConnectorServices()).testConnection(userName, spaceID, spaceDirectory, connectorConnectionString, credentialsXML);
		} finally
		{
			removeUserSpaceInfo(userName, spaceID);
		}
	}
	
	public OMElement saveConnection(String userName, String spaceID, String spaceDirectory, String connectorConnectionString, String connectionDetailsXML)
	{
		try
		{
			logUserSpaceInfo(userName, spaceID);
			BirstWebServiceResult result = new BirstWebServiceResult();
			if(!isTrustedRequest("testConnection", result))
			{
				return result.toOMElement();
			}
			return (new ConnectorServices()).saveConnection(userName, spaceID, spaceDirectory, connectorConnectionString, connectionDetailsXML);
		} finally
		{
			removeUserSpaceInfo(userName, spaceID);
		}
	}
	
	public OMElement getConnectorDynamicParameters(String userName, String spaceID, String spaceDirectory, String connectorName, String xmlData)
	{
		try
		{
			logUserSpaceInfo(userName, spaceID);
			BirstWebServiceResult result = new BirstWebServiceResult();
			if(!isTrustedRequest("getConnectorDynamicParameters", result))
			{
				return result.toOMElement();
			}
			return (new ConnectorServices()).getDynamicParameters(userName, spaceID, spaceDirectory, connectorName, xmlData);
		} finally
		{
			removeUserSpaceInfo(userName, spaceID);
		}
	}

	public OMElement getAllConnectorObjects(String userName, String spaceID, String spaceDirectory, String connectorName, String xmlData)
	{
		try
		{
			logUserSpaceInfo(userName, spaceID);
			BirstWebServiceResult result = new BirstWebServiceResult();
			if(!isTrustedRequest("getAllConnectorObjects", result))
			{
				return result.toOMElement();
			}
			return (new ConnectorServices()).getAllObjects(userName, spaceID, spaceDirectory, connectorName, xmlData);
		} finally
		{
			removeUserSpaceInfo(userName, spaceID);
		}
	}
	
	public OMElement getAllObjectsForCloudConnection(String userName, String spaceID, String spaceDirectory, String connectionID, String xmlData)
	{
		try
		{
			logUserSpaceInfo(userName, spaceID);
			BirstWebServiceResult result = new BirstWebServiceResult();
			if(!isTrustedRequest("getAllConnectorObjects", result))
			{
				return result.toOMElement();
			}
			return (new ConnectorServices()).getAllObjectsForCloudConnection(userName, spaceID, spaceDirectory, connectionID, xmlData);
		} finally
		{
			removeUserSpaceInfo(userName, spaceID);
		}
	}

	public OMElement getSelectedConnectorObjects(String userName, String spaceID, String spaceDirectory, String connectorName)
	{
		try
		{
			logUserSpaceInfo(userName, spaceID);
			BirstWebServiceResult result = new BirstWebServiceResult();
			if(!isTrustedRequest("getSelectedConnectorObjects", result))
			{
				return result.toOMElement();
			}
			return (new ConnectorServices()).getSelectedObjects(spaceID, spaceDirectory, connectorName);
		} finally
		{
			removeUserSpaceInfo(userName, spaceID);
		}
	}
	
	public OMElement updateSelectedObjects(String userName, String spaceID, String spaceDirectory, String connectionName, String connectorType, String xmlData)
	{
		try
		{
			logUserSpaceInfo(userName, spaceID);
			BirstWebServiceResult result = new BirstWebServiceResult();
			if(!isTrustedRequest("saveSelectedObjects", result))
			{
				return result.toOMElement();
			}
			return (new ConnectorServices()).updateSelectedObjects(userName, spaceID, spaceDirectory, connectionName, connectorType, xmlData);
		} finally
		{
			removeUserSpaceInfo(userName, spaceID);
		}
	}

	public OMElement saveSelectedObjects(String userName, String spaceID, String spaceDirectory, String connectorName, String xmlData)
	{
		try
		{
			logUserSpaceInfo(userName, spaceID);
			BirstWebServiceResult result = new BirstWebServiceResult();
			if(!isTrustedRequest("saveSelectedObjects", result))
			{
				return result.toOMElement();
			}
			return (new ConnectorServices()).saveSelectedObjects(userName, spaceID, spaceDirectory, connectorName, xmlData);
		} finally
		{
			removeUserSpaceInfo(userName, spaceID);
		}
	}

	public OMElement startExtract(String userName, String spaceID, String spaceDirectory, String connectorName, String xmlData, String extractGroupsXML)
	{
		try
		{
			logUserSpaceInfo(userName, spaceID);
			BirstWebServiceResult result = new BirstWebServiceResult();
			if(!isTrustedRequest("startExtract", result))
			{
				return result.toOMElement();
			}
			String extractGroupsStr = ConnectorUtil.getExtractGroups(extractGroupsXML);
			return (new ConnectorServices()).startExtract(userName, spaceID, spaceDirectory, connectorName, xmlData,
					getConnectorVariables(spaceDirectory, userName, spaceID, connectorName, extractGroupsStr), extractGroupsXML);
		} finally
		{
			removeUserSpaceInfo(userName, spaceID);
		}
	}

	public OMElement cancelExtract(String userName, String spaceID, String spaceDirectory, String connectorName)
	{
		try
		{
			logUserSpaceInfo(userName, spaceID);
			BirstWebServiceResult result = new BirstWebServiceResult();
			if(!isTrustedRequest("cancelExtract", result))
			{
				return result.toOMElement();
			}
			return (new ConnectorServices()).cancelExtract(spaceID, spaceDirectory, connectorName);
		} finally
		{
			removeUserSpaceInfo(userName, spaceID);
		}
	}

	public OMElement getConnectorObjectQuery(String userName, String spaceID, String spaceDirectory, String connectorName, String xmlData)
	{
		try
		{
			logUserSpaceInfo(userName, spaceID);
			BirstWebServiceResult result = new BirstWebServiceResult();
			if(!isTrustedRequest("getConnectorObjectQuery", result))
			{
				return result.toOMElement();
			}
			return (new ConnectorServices()).getObjectQuery(userName, spaceID, spaceDirectory, connectorName, xmlData);
		} finally
		{
			removeUserSpaceInfo(userName, spaceID);
		}
	}

	public OMElement getConnectorObject(String userName, String spaceID, String spaceDirectory, String connectorName, String xmlData)
	{
		try
		{
			logUserSpaceInfo(userName, spaceID);
			BirstWebServiceResult result = new BirstWebServiceResult();
			if(!isTrustedRequest("getConnectorObject", result))
			{
				return result.toOMElement();
			}
			String variables = ConnectorUtil.getVariables(xmlData); 
			variables = formatVariables(getVariables(spaceDirectory, userName, spaceID, variables));
			return (new ConnectorServices()).getObject(userName, spaceID, spaceDirectory, connectorName, xmlData,variables);
		} finally
		{
			removeUserSpaceInfo(userName, spaceID);
		}
	}

	public OMElement validateConnectorObjectQuery(String userName, String spaceID, String spaceDirectory, String connectorName, String xmlData)
	{
		try
		{
			logUserSpaceInfo(userName, spaceID);
			BirstWebServiceResult result = new BirstWebServiceResult();
			if(!isTrustedRequest("validateConnectorObjectQuery", result))
			{
				return result.toOMElement();
			}
			String variables = ConnectorUtil.getVariables(xmlData); 
			variables = formatVariables(getVariables(spaceDirectory, userName, spaceID, variables));
			return (new ConnectorServices()).validateObjectQuery(userName, spaceID, spaceDirectory, connectorName, xmlData, variables);
		} finally
		{
			removeUserSpaceInfo(userName, spaceID);
		}
	}

	public OMElement validateConnectorObject(String userName, String spaceID, String spaceDirectory, String connectorName, String xmlData)
	{
		try
		{
			logUserSpaceInfo(userName, spaceID);
			BirstWebServiceResult result = new BirstWebServiceResult();
			if(!isTrustedRequest("validateConnectorObject", result))
			{
				return result.toOMElement();
			}
			String variables = ConnectorUtil.getVariables(xmlData); 
			variables = formatVariables(getVariables(spaceDirectory, userName, spaceID, variables));
			return (new ConnectorServices()).validateObject(userName, spaceID, spaceDirectory, connectorName, xmlData, variables);
		} finally
		{
			removeUserSpaceInfo(userName, spaceID);
		}
	}

	public OMElement getConnectorMetaData(String userName, String spaceID, String spaceDirectory, String connectorName)
	{
		try
		{
			logUserSpaceInfo(userName, spaceID);
			BirstWebServiceResult result = new BirstWebServiceResult();
			if(!isTrustedRequest("getConnectorMetaData", result))
			{
				return result.toOMElement();
			}
			return (new ConnectorServices()).getMetaData(userName, spaceID, spaceDirectory, connectorName);
		} finally
		{
			removeUserSpaceInfo(userName, spaceID);
		}
	}

	public OMElement getConnectorObjectDetails(String userName, String spaceID, String spaceDirectory, String connectorName, String xmlData)
	{
		try
		{
			logUserSpaceInfo(userName, spaceID);
			BirstWebServiceResult result = new BirstWebServiceResult();
			if(!isTrustedRequest("getConnectorObjectDetails", result))
			{
				return result.toOMElement();
			}
			return (new ConnectorServices()).getObjectDetails(spaceID, spaceDirectory, connectorName, xmlData);
		} finally
		{
			removeUserSpaceInfo(userName, spaceID);
		}
	}

	public OMElement getSavedObjectDetails(String userName, String spaceID, String spaceDirectory, String connectorName, String xmlData)
	{
		try
		{
			logUserSpaceInfo(userName, spaceID);
			BirstWebServiceResult result = new BirstWebServiceResult();
			if(!isTrustedRequest("getSavedObjectDetails", result))
			{
				return result.toOMElement();
			}
			String variables = ConnectorUtil.getVariables(xmlData); 
			variables = formatVariables(getVariables(spaceDirectory, userName, spaceID, variables));
			return (new ConnectorServices()).getSavedObjectDetails(spaceID, spaceDirectory, userName, connectorName, xmlData, variables);		
		} 
		finally
		{
			removeUserSpaceInfo(userName, spaceID);
		}
	}

	public OMElement saveConnectorQueryObject(String userName, String spaceID, String spaceDirectory, String connectorName, String xmlData)
	{
		try
		{
			logUserSpaceInfo(userName, spaceID);
			BirstWebServiceResult result = new BirstWebServiceResult();
			if(!isTrustedRequest("saveConnectorQueryObject", result))
			{
				return result.toOMElement();
			}
			return (new ConnectorServices()).saveQueryObject(spaceID, spaceDirectory, userName, connectorName, xmlData);
		} finally
		{
			removeUserSpaceInfo(userName, spaceID);
		}
	}

	public OMElement saveConnectorObject(String userName, String spaceID, String spaceDirectory, String connectorName, String xmlData)
	{
		try
		{
			logUserSpaceInfo(userName, spaceID);
			BirstWebServiceResult result = new BirstWebServiceResult();
			if(!isTrustedRequest("saveConnectorObject", result))
			{
				return result.toOMElement();
			}
			return (new ConnectorServices()).saveObject(userName, spaceID, spaceDirectory, connectorName, xmlData);
		} finally
		{
			removeUserSpaceInfo(userName, spaceID);
		}
	}

	private void logUserSpaceInfo(String username, String spaceID)
	{
		logUserInfo(username);
		logSpaceInfo(spaceID);
	}

	private void removeUserSpaceInfo(String username, String spaceID)
	{
		removeUserInfo();
		removeSpaceInfo();
	}

	private void removeUserInfo()
	{
		MDC.remove("username");
	}

	private void removeSpaceInfo()
	{
		MDC.remove("spaceID");
	}

	private void logUserInfo(String username)
	{
		MDC.put("username", username);
	}

	private void logSpaceInfo(String spaceID)
	{
		MDC.put("spaceID", spaceID);
	}

	public CatalogCompareResult compareCatalogs(String spaceDirectory, String spaceDirectoryCompare, String username, String spaceID)
	{
		CatalogCompareResult ccResult = new CatalogCompareResult();
		try
		{
			MDC.put("username", username);
			MDC.put("spaceID", spaceID);
			if (!isTrustedRequest("compareCatalogs"))
			{
				ccResult.setErrorCode(BaseException.ERROR_OTHER);
				ccResult.setErrorMessage(ERROR_INVALID_IP);
				return ccResult;
			}
			ApplicationContext ac = ApplicationContext.getInstance(spaceDirectory, null);
			Repository repository = ac.getRepository();
			SMIWebUserBean ub = new SMIWebUserBean();
			ub.setApplicationContext(ac);
			ub.setUser(repository.findUser(username));
			ub.setRepository(repository);
			HttpSession session = getHttpServletRequest().getSession();
			SMIWebUserBean.setHttpServletRequest(getHttpServletRequest());
			session.setAttribute("userBean", ub);
			CompareCatalogs cc = new CompareCatalogs(spaceDirectory, spaceDirectoryCompare);
			cc.compare();
			cc.setCatalogCompareRestul(ccResult);
			return ccResult;
		} catch (Exception e)
		{
			logger.debug(e, e);
			return null;
		} finally
		{
			MDC.remove("username");
			MDC.remove("spaceID");
		}
	}

	public ManageSamplesResult getSampleMetadata(String spaceDirectory, String username, String spaceID)
	{
		ManageSamplesResult msr = new ManageSamplesResult();
		try
		{
			MDC.put("username", username);
			MDC.put("spaceID", spaceID);
			if(!isTrustedRequest("getSampleMetadata"))
			{
				msr.setErrorCode(BaseException.ERROR_OTHER);
				msr.setErrorMessage(ERROR_INVALID_IP);
				return msr;
			}
			ApplicationContext ac = ApplicationContext.getInstance(spaceDirectory, null);
			Repository repository = ac.getRepository();
			SampleMetadata smd = Sample.getSampleMetadata(spaceDirectory, repository);
			String[] sarr = new String[smd.dimensions.size()];
			smd.dimensions.toArray(sarr);
			msr.setDimensions(sarr);
			sarr = new String[smd.levels.size()];
			smd.levels.toArray(sarr);
			msr.setLevels(sarr);
			int[] iarr = new int[smd.numSamples.size()];
			for (int i = 0; i < iarr.length; i++)
				iarr[i] = smd.numSamples.get(i);
			msr.setNumSamples(iarr);
			iarr = new int[smd.fileSize.size()];
			for (int i = 0; i < iarr.length; i++)
				iarr[i] = smd.fileSize.get(i);
			msr.setFileSize(iarr);
			sarr = new String[smd.mapDimensions.size()];
			smd.mapDimensions.toArray(sarr);
			msr.setMapDimensions(sarr);
			sarr = new String[smd.mapLevels.size()];
			smd.mapLevels.toArray(sarr);
			msr.setMapLevels(sarr);
			sarr = new String[smd.mapSources.size()];
			smd.mapSources.toArray(sarr);
			msr.setMapSources(sarr);
			return msr;
		} catch (Exception e)
		{
			logger.debug(e, e);
			return null;
		} finally
		{
			MDC.remove("username");
			MDC.remove("spaceID");
		}
	}

	public ManageSamplesResult createSample(String spaceDirectory, String username, String spaceID, String dimension, String level, String source,
			double percent)
	{
		ManageSamplesResult msr = new ManageSamplesResult();
		try
		{
			MDC.put("username", username);
			MDC.put("spaceID", spaceID);
			if(!isTrustedRequest("createSample"))
			{
				msr.setErrorCode(BaseException.ERROR_OTHER);
				msr.setErrorMessage(ERROR_INVALID_IP);
				return msr;
			}
			ApplicationContext ac = ApplicationContext.getInstance(spaceDirectory, null);
			Repository repository = ac.getRepository();
			Sample sa = new Sample(repository, spaceDirectory + File.separator + "data", dimension, level);
			StagingTable st = repository.findStagingTable(source);
			if (st != null)
			{
				sa.createSample(st.Name, percent);
			} else
			{
				msr.setErrorMessage("Bad data source name");
			}
		} catch (Exception e)
		{
			logger.debug(e, e);
			return null;
		} finally
		{
			MDC.remove("username");
			MDC.remove("spaceID");
		}
		return msr;
	}
	
	public ManageSamplesResult deriveSample(String spaceDirectory, String username, String spaceID, String sourceDimension, String sourceLevel,
			String targetDimension, String targetLevel, String source)
	{
		ManageSamplesResult msr = new ManageSamplesResult();
		try
		{
			MDC.put("username", username);
			MDC.put("spaceID", spaceID);
			if(!isTrustedRequest("deriveSample"))
			{
				msr.setErrorCode(BaseException.ERROR_OTHER);
				msr.setErrorMessage(ERROR_INVALID_IP);
				return msr;
			}
			ApplicationContext ac = ApplicationContext.getInstance(spaceDirectory, null);
			Repository repository = ac.getRepository();
			Sample sa = new Sample(repository, spaceDirectory + File.separator + "data", targetDimension, targetLevel);
			StagingTable st = repository.findStagingTable(source);
			if (st != null)
			{
				sa.deriveSample(sourceDimension, sourceLevel, st.Name);
			} else
			{
				msr.setErrorMessage("Bad data source name");
			}
		} catch (Exception e)
		{
			logger.debug(e, e);
			return null;
		} finally
		{
			MDC.remove("username");
			MDC.remove("spaceID");
		}
		return msr;
	}

	public ManageSamplesResult applySample(String spaceDirectory, String username, String spaceID, String dimension, String level, String source)
	{
		ManageSamplesResult msr = new ManageSamplesResult();
		try
		{
			MDC.put("username", username);
			MDC.put("spaceID", spaceID);			 
			if (!isTrustedRequest("applySample"))
			{
				msr.setErrorCode(BaseException.ERROR_OTHER);
				msr.setErrorMessage(ERROR_INVALID_IP);
				return msr;
			}
			ApplicationContext ac = ApplicationContext.getInstance(spaceDirectory, null);
			Repository repository = ac.getRepository();
			Sample sa = new Sample(repository, spaceDirectory + File.separator + "data", dimension, level);
			StagingTable st = repository.findStagingTable(source);
			if (st != null)
			{
				sa.applySample(st.Name);
			} else
			{
				msr.setErrorMessage("Bad data source name");
			}
		} catch (Exception e)
		{
			logger.debug(e, e);
			return null;
		} finally
		{
			MDC.remove("username");
			MDC.remove("spaceID");
		}
		return msr;
	}

	public ManageSamplesResult removeSample(String spaceDirectory, String username, String spaceID, String dimension, String level, String source)
	{
		ManageSamplesResult msr = new ManageSamplesResult();
		try
		{
			MDC.put("username", username);
			MDC.put("spaceID", spaceID);
			if(!isTrustedRequest("removeSample"))
			{
				msr.setErrorCode(BaseException.ERROR_OTHER);
				msr.setErrorMessage(ERROR_INVALID_IP);
				return msr;
			}
			ApplicationContext ac = ApplicationContext.getInstance(spaceDirectory, null);
			Repository repository = ac.getRepository();
			Sample sa = new Sample(repository, spaceDirectory + File.separator + "data", dimension, level);
			StagingTable st = repository.findStagingTable(source);
			if (st != null)
			{
				sa.removeSample(st.Name);
			} else
			{
				msr.setErrorMessage("Bad data source name");
			}
		} catch (Exception e)
		{
			logger.debug(e, e);
			return null;
		} finally
		{
			MDC.remove("username");
			MDC.remove("spaceID");
		}
		return msr;
	}

	public ManageSamplesResult deleteSamples(String spaceDirectory, String username, String spaceID)
	{
		ManageSamplesResult msr = new ManageSamplesResult();
		try
		{
			MDC.put("username", username);
			MDC.put("spaceID", spaceID);
			if(!isTrustedRequest("deleteSamples"))
			{
				msr.setErrorCode(BaseException.ERROR_OTHER);
				msr.setErrorMessage(ERROR_INVALID_IP);
				return msr;
			}
			Sample.deleteSamples(spaceDirectory + File.separator + "data");
		} catch (Exception e)
		{
			logger.debug(e, e);
			return null;
		} finally
		{
			MDC.remove("username");
			MDC.remove("spaceID");
		}
		return msr;
	}

    private StagingTable getStagingTableFromDisplayName(Repository r, String displayName)
    {
        for (StagingTable st: r.getStagingTables())
        {
            String dname = st.getDisplayName();
            if (dname == displayName)
            {
                return st;
            }
        }
        return null;
    }

	public CatalogCompareResult pullFromCatalog(String spaceDirectory, String spaceDirectoryCompare, String username, String spaceID, String[] paths)
	{
		CatalogCompareResult ccResult = new CatalogCompareResult();
		try
		{
			MDC.put("username", username);
			MDC.put("spaceID", spaceID);
			if(!isTrustedRequest("pullFromCatalog"))
			{
				ccResult.setErrorCode(BaseException.ERROR_OTHER);
				ccResult.setErrorMessage(ERROR_INVALID_IP);
				return ccResult;
			}
			ApplicationContext ac = ApplicationContext.getInstance(spaceDirectory, null);
			Repository repository = ac.getRepository();
			SMIWebUserBean ub = new SMIWebUserBean();
			ub.setApplicationContext(ac);
			ub.setUser(repository.findUser(username));
			ub.setRepository(repository);
			HttpSession session = getHttpServletRequest().getSession();
			SMIWebUserBean.setHttpServletRequest(getHttpServletRequest());
			session.setAttribute("userBean", ub);
			CompareCatalogs cc = new CompareCatalogs(spaceDirectory, spaceDirectoryCompare);
			cc.pull(paths);
			return ccResult;
		} catch (Exception e)
		{
			logger.debug(e, e);
			return null;
		} finally
		{
			MDC.remove("username");
			MDC.remove("spaceID");
		}
	}

	public SearchResult searchReports(String spaceDirectory, String path, String fileNameFilter, String dimValue, String exprValue, String labelValue,
			String outputFileName, String username, String spaceID, String[] packageSpacesProperties)
	{
		SearchResult searchResult = new SearchResult();
		try
		{
			MDC.put("username", username);
			MDC.put("spaceID", spaceID);
			if(!isTrustedRequest("searchReports"))
			{
				searchResult.setErrorCode(BaseException.ERROR_OTHER);
				searchResult.setErrorMessage(ERROR_INVALID_IP);
				return searchResult;
			}
			ApplicationContext ac = ApplicationContext.getInstance(spaceDirectory, null);
			Repository repository = ac.getRepository();
			SMIWebUserBean ub = new SMIWebUserBean();
			ub.setApplicationContext(ac);
			ub.setUser(repository.findUser(username));
			ub.setRepository(repository);
			HttpSession session = getHttpServletRequest().getSession();
			SMIWebUserBean.setHttpServletRequest(getHttpServletRequest());
			session.setAttribute("userBean", ub);
			File outputFile = null;
			if (outputFileName != null && outputFileName.length() > 0)
				outputFile = new File(outputFileName);
			SearchReports sr = new SearchReports(spaceDirectory, path, fileNameFilter, dimValue, exprValue, labelValue, outputFile);
			sr.search();
			searchResult.setReportNames(sr.getReportNames());
			searchResult.setReportPaths(sr.getReportPaths());
			searchResult.setExpressions(sr.getExpressions());
			searchResult.setColNames(sr.getColNames());
			return searchResult;
		} catch (Exception e)
		{
			logger.debug(e, e);
			return null;
		} finally
		{
			MDC.remove("username");
			MDC.remove("spaceID");
		}
	}

	public SearchResult replaceReportContent(String spaceDirectory, String[] reportPaths, String[] names, String dimValue, String exprValue, String labelValue,
			String replaceDimValue, String replaceExprValue, String replaceLabelValue, String username, String spaceID, String[] packageSpacesProperties)
	{
		SearchResult searchResult = new SearchResult();
		try
		{
			MDC.put("username", username);
			MDC.put("spaceID", spaceID);
			if(!isTrustedRequest("replaceReportContent"))
			{
				searchResult.setErrorCode(BaseException.ERROR_OTHER);
				searchResult.setErrorMessage(ERROR_INVALID_IP);
				return searchResult;
			}
			ApplicationContext ac = ApplicationContext.getInstance(spaceDirectory, null);
			Repository repository = ac.getRepository();
			SMIWebUserBean ub = new SMIWebUserBean();
			ub.setApplicationContext(ac);
			ub.setUser(repository.findUser(username));
			ub.setRepository(repository);
			HttpSession session = getHttpServletRequest().getSession();
			SMIWebUserBean.setHttpServletRequest(getHttpServletRequest());
			session.setAttribute("userBean", ub);
			SearchReports sr = new SearchReports(spaceDirectory, reportPaths, names, dimValue, exprValue, labelValue, replaceDimValue, replaceExprValue,
					replaceLabelValue);
			int numChanged = sr.replace();
			searchResult.setReportsModified(numChanged);
			return searchResult;
		} catch (Exception e)
		{
			logger.debug(e, e);
			return null;
		} finally
		{
			MDC.remove("username");
			MDC.remove("spaceID");
		}
	}

	public SearchResult restoreFromReplace(String spaceDirectory, String path, String fileNameFilter, String username, String spaceID,
			String[] packageSpacesProperties)
	{
		SearchResult searchResult = new SearchResult();
		try
		{			
			MDC.put("username", username);
			MDC.put("spaceID", spaceID);
			if(!isTrustedRequest("restoreFromReplace"))
			{
				searchResult.setErrorCode(BaseException.ERROR_OTHER);
				searchResult.setErrorMessage(ERROR_INVALID_IP);
				return searchResult;
			}
			ApplicationContext ac = ApplicationContext.getInstance(spaceDirectory, null);
			Repository repository = ac.getRepository();
			SMIWebUserBean ub = new SMIWebUserBean();
			ub.setApplicationContext(ac);
			ub.setUser(repository.findUser(username));
			ub.setRepository(repository);
			HttpSession session = getHttpServletRequest().getSession();
			SMIWebUserBean.setHttpServletRequest(getHttpServletRequest());
			session.setAttribute("userBean", ub);
			SearchReports sr = new SearchReports(spaceDirectory, path, fileNameFilter, null, null, null, null);
			int numChanged = sr.restore();
			searchResult.setReportsModified(numChanged);
			return searchResult;
		} catch (Exception e)
		{
			logger.debug(e, e);
			return null;
		} finally
		{
			MDC.remove("username");
			MDC.remove("spaceID");
		}
	}

	public OMElement getMetadataRulesForValidation(String userName, boolean isUserRepoAdmin, String spaceID, String path, String locale)
	{
		ApplicationContext ac = null;
		try
		{
			MetadataValidationServiceUtil mdServiceUtil = new MetadataValidationServiceUtil();
			logUserSpaceInfo(userName, spaceID);
			if(!isTrustedRequest("getMetadataRulesForValidation"))
			{
				return mdServiceUtil.getGeneralErrorMessage(ERROR_INVALID_IP);
			}
			ac = ApplicationContext.getInstance(path, null);
			Repository r = ac.getRepository();
			try
			{
				SMIWebUserBean ub = new SMIWebUserBean();
				ub.setApplicationContext(ac);
				ub.setUser(r.findUser(userName));
				ub.setRepository(r);
				if (locale != null && !locale.isEmpty())
				{
					ub.setLocale(new Locale(locale));
				}
				HttpSession httpSession = getHttpServletRequest().getSession();
				SMIWebUserBean.setHttpServletRequest(getHttpServletRequest());
				httpSession.setAttribute("userBean", ub);
			} catch (Exception e)
			{
				logger.error(e.toString(), e);
			}
			return mdServiceUtil.getMetadataValidationRules(isUserRepoAdmin);
		} finally
		{
			if (ac != null)
				ac.dispose();
			removeUserSpaceInfo(userName, spaceID);
		}
	}

	public OMElement validateRepositoryMetadata(String userName, boolean isUserRepoAdmin, String spaceID, String path, String execFlag, String ruleOrGrpName, String locale)
	{
		ApplicationContext ac = null;
		try
		{
			MetadataValidationServiceUtil mdServiceUtil = new MetadataValidationServiceUtil();			
			logUserSpaceInfo(userName, spaceID);
			if(!isTrustedRequest("validateRepositoryMetadata"))
			{
				return mdServiceUtil.getGeneralErrorMessage(ERROR_INVALID_IP);
			}
			ac = ApplicationContext.getInstance(path, null);
			Repository r = ac.getRepository();
			Repository repositoryNonDynamic = null;
			try
			{
				SAXBuilder builder = new SAXBuilder();
				File repoFile = new File(r.getServerParameters().getApplicationPath() + File.separator + "repository.xml");
				Document d = builder.build(repoFile);
				repositoryNonDynamic = new Repository(d, false, repoFile.lastModified(), true);
				SMIWebUserBean ub = new SMIWebUserBean();
				ub.setApplicationContext(ac);
				ub.setUser(r.findUser(userName));
				ub.setRepository(r);
				if (locale != null && !locale.isEmpty())
				{
					ub.setLocale(new Locale(locale));
				}
				HttpSession httpSession = getHttpServletRequest().getSession();
				SMIWebUserBean.setHttpServletRequest(getHttpServletRequest());
				httpSession.setAttribute("userBean", ub);
			} catch (Exception e)
			{
				logger.error(e.toString(), e);
			}
			return mdServiceUtil.validateRepository(repositoryNonDynamic, execFlag, ruleOrGrpName, isUserRepoAdmin);
		} finally
		{
			if (ac != null)
				ac.dispose();
			removeUserSpaceInfo(userName, spaceID);
		}
	}

	public OMElement fixRepositoryMetadata(String userName, String spaceID, String path, String execFlag, String ruleOrGrpName, String locale)
	{
		ApplicationContext ac = null;
		try
		{
			MetadataValidationServiceUtil mdServiceUtil = new MetadataValidationServiceUtil();
			if(!isTrustedRequest("fixRepositoryMetadata"))
			{
				return mdServiceUtil.getGeneralErrorMessage(ERROR_INVALID_IP);
			}
			logUserSpaceInfo(userName, spaceID);
			ac = ApplicationContext.getInstance(path, null);
			Repository r = ac.getRepository();
			Repository repositoryNonDynamic = null;
			try
			{
				SAXBuilder builder = new SAXBuilder();
				File repoFile = new File(r.getServerParameters().getApplicationPath() + File.separator + "repository.xml");
				Document d = builder.build(repoFile);
				repositoryNonDynamic = new Repository(d, false, repoFile.lastModified(), false);
				SMIWebUserBean ub = new SMIWebUserBean();
				ub.setApplicationContext(ac);
				ub.setUser(r.findUser(userName));
				ub.setRepository(r);
				if (locale != null && !locale.isEmpty())
				{
					ub.setLocale(new Locale(locale));
				}
				HttpSession httpSession = getHttpServletRequest().getSession();
				SMIWebUserBean.setHttpServletRequest(getHttpServletRequest());
				httpSession.setAttribute("userBean", ub);
			} catch (Exception e)
			{
				logger.error(e.toString(), e);
			}
			return mdServiceUtil.fixRepository(repositoryNonDynamic, execFlag, ruleOrGrpName);
		} finally
		{
			if (ac != null)
				ac.dispose();
			removeUserSpaceInfo(userName, spaceID);
		}
	}
	
	public RServerResult getRServerFile(String spaceDirectory, String path, String rexpression, String username, String spaceID)
	{
		RServerResult rsr = new RServerResult();
		try
		{
			MDC.put("username", username);
			MDC.put("spaceID", spaceID);
			Repository repository = ApplicationContext.loadRepository(spaceDirectory, "dev", null);
			RServerUtil rsutil = new RServerUtil(repository, spaceDirectory);;
			String errorMessage = rsutil.getRServerFile(path, rexpression);
			if (errorMessage != null)
			{
				rsr.setErrorCode(BaseException.ERROR_DATA_UNAVAILABLE);
				rsr.setErrorMessage(errorMessage);
			}
			return rsr;
		} catch (Exception e)
		{
			logger.debug(e, e);
			return null;
		} finally
		{
			MDC.remove("username");
			MDC.remove("spaceID");
		}
	}
	
	public RServerResult testRServerConnection(String spaceDirectory)
	{
		RServerResult rsr = new RServerResult();
		Repository repository = ApplicationContext.loadRepository(spaceDirectory, "dev", null);
		RServer rs = repository.getRServer();
		if (rs == null)
			rsr.setErrorCode(BaseException.ERROR_CONNECTOR_LOGIN_FAILURE);
		String result = rs.getConnectionError();
		if (result != null)
		{
			rsr.setErrorMessage(result);
			rsr.setErrorCode(BaseException.ERROR_CONNECTOR_LOGIN_FAILURE);
		}
		return rsr;
	}
}
