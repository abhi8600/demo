package com.successmetricsinc.WebServices.trustedservice;

import java.io.StringWriter;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.impl.util.OMSerializerUtil;
import org.apache.log4j.Logger;

import com.successmetricsinc.WebServices.BirstWebServiceResult;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.XmlUtils;

public class CatalogCompareResult
{
	private static final XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
	private static Logger logger = Logger.getLogger(BirstWebServiceResult.class);
	private int errorCode = BaseException.SUCCESS;
	private String errorMessage;
	private String[] itemNames;
	private String[] itemPaths;
	private String[] itemTypes;
	// Each difference is an array separated by "~"
	private String[] differences;

	/**
	 * Is this result valid?
	 * 
	 * @return true if the web service call succeeded, false otherwise.
	 */
	public boolean isSuccess()
	{
		return errorCode == BaseException.SUCCESS;
	}

	/**
	 * Returns the error code.
	 * 
	 * @return Returns the error code. A value of SUCCESS_ERROR_CODE indicates no error.
	 */
	public int getErrorCode()
	{
		return errorCode;
	}

	/**
	 * Sets the error code
	 * 
	 * @param errorCode
	 *            the new error code value
	 */
	public void setErrorCode(int errorCode)
	{
		this.errorCode = errorCode;
	}

	public OMElement toOMElement()
	{
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace ns = fac.createOMNamespace("", "");
		OMElement ret = fac.createOMElement(this.getClass().getName(), ns);
		XmlUtils.addContent(fac, ret, "ErrorCode", this.errorCode, ns);
		return ret;
	}

	public String toString()
	{
		OMElement el = this.toOMElement();
		StringWriter w = new StringWriter();
		XMLStreamWriter writer;
		try
		{
			writer = outputFactory.createXMLStreamWriter(w);
			OMSerializerUtil.serializeByPullStream(el, writer);
			writer.flush();
		} catch (XMLStreamException e)
		{
			logger.error(e, e);
		}
		return w.toString();
	}

	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage()
	{
		return errorMessage;
	}

	/**
	 * @param errorMessage
	 *            the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage)
	{
		this.errorMessage = errorMessage;
	}

	public String[] getItemNames()
	{
		return itemNames;
	}

	public void setItemNames(String[] itemNames)
	{
		this.itemNames = itemNames;
	}

	public String[] getItemPaths()
	{
		return itemPaths;
	}

	public void setItemPaths(String[] itemPaths)
	{
		this.itemPaths = itemPaths;
	}

	public String[] getItemTypes()
	{
		return itemTypes;
	}

	public void setItemTypes(String[] itemTypes)
	{
		this.itemTypes = itemTypes;
	}

	public String[] getDifferences()
	{
		return differences;
	}

	public void setDifferences(String[] differences)
	{
		this.differences = differences;
	}
}
