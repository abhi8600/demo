package com.successmetricsinc.WebServices.trustedservice;

public class RedshiftTimeCredentials implements TimeAccessParams {

	private String folderName;
	private String awsAccessKeyId;
	private String awsSecretKey;
	private String awsToken;
	
	public String getFolderName() {
		return folderName;
	}
	
	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}
	
	public String getAwsAccessKeyId() {
		return awsAccessKeyId;
	}
	
	public void setAwsAccessKeyId(String awsAccessKeyId) {
		this.awsAccessKeyId = awsAccessKeyId;
	}
	
	public String getAwsSecretKey() {
		return awsSecretKey;
	}
	
	public void setAwsSecretKey(String awsSecretKey) {
		this.awsSecretKey = awsSecretKey;
	}
	
	public String getAwsToken() {
		return awsToken;
	}
	
	public void setAwsToken(String awsToken) {
		this.awsToken = awsToken;
	}
	
	@Override
	public String toString() {
		return "RedshiftTimeCredentials [folderName=" + folderName
				+ ", awsAccessKeyId=" + awsAccessKeyId + ", awsSecretKey="
				+ awsSecretKey + ", awsToken=" + awsToken + "]";
	}
}
