package com.successmetricsinc.WebServices.trustedservice;

public class SAPHanaTimeAccessParams implements TimeAccessParams {

	private String folderName;
	
	SAPHanaTimeAccessParams(String folderName){
		this.folderName = folderName;
	}
	
	public String getFolderName() {
		return folderName;
	}
			
	@Override
	public String toString() {
		return "SAPHana [folderName=" + folderName + "]";
	}

}
