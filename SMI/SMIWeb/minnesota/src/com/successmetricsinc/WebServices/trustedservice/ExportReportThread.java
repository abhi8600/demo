package com.successmetricsinc.WebServices.trustedservice;

import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.log4j.Logger;

import com.successmetricsinc.Broadcast;
import com.successmetricsinc.EmailDelivery;
import com.successmetricsinc.PackageSpaceProperties;
import com.successmetricsinc.Repository;
import com.successmetricsinc.UI.ApplicationContext;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.WebServices.trustedservice.ExportReportManager.ReportStatus;
import com.successmetricsinc.util.TimeZoneUtil;
import com.successmetricsinc.util.Util;

public class ExportReportThread extends Thread
{

	private static final Logger logger = Logger.getLogger(ExportReportThread.class);

	private ReportStatus status;
	private Exception errorException;
	private String path;
	private String spaceID;
	private String type;
	private String name;
	private String username;
	private String from;
	private String subject;
	private String body;
	private String report;
	private String[] toList;
	private String toReport;
	private String triggerReport;
	private int maxReports;
	private String reportLocale;
	private String reportTimeZone;
	private String mailHost;
	private String csvSeparator;
	private String csvExtension;
	private String compressionFormat;
	private String prompts;
	private String overrideVariables;
	private List<PackageSpaceProperties> packageSpacePropertiesList;
	
	public ExportReportThread()
	{
		this.setName("ExportReportThread - " + this.getId());
		setDaemon(true); // if the JVM wants to exit due to errors, let it
	}

	public Exception getErrorException()
	{
		return errorException;
	}
	
	public void run()
	{
		ApplicationContext ac = null;
		try
		{
			status = ReportStatus.RUNNING;
			ac = ApplicationContext.getInstance(path, packageSpacePropertiesList);
			Repository r = ac.getRepository();
			logger.debug("Exporting report for repository " + spaceID);
			String catalogPath = r.getServerParameters().getCatalogPath();
			if (catalogPath == null)
				catalogPath = r.getServerParameters().getApplicationPath() + "\\catalog";
			if (catalogPath != null)
			{
				report = report.replace("V{CatalogDir}", catalogPath);				
			}
			

			// Making the use of userbean and adhoc bean in case they are needed 
			// during reading/compiling from adhoc reports
			SMIWebUserBean ub = new SMIWebUserBean();
			ub.setApplicationContext(ac);
			ub.setUser(r.findUser(username));
			Long threadId = Long.valueOf(Thread.currentThread().getId());
			SMIWebUserBean.addThreadRequest(threadId, ub);
			
			if(toReport != null)
			{
				toReport = toReport.replace("V{CatalogDir}", catalogPath);
			}
			
			if(triggerReport != null)
			{
				triggerReport = triggerReport.replace("V{CatalogDir}", catalogPath) ;
			}
			
			com.successmetricsinc.UserBean userBean = new com.successmetricsinc.UserBean();
			if (reportLocale != null && !reportLocale.isEmpty())
			{
				String[] locale = reportLocale.split("-");
				Locale loc = new Locale(locale[0], locale[1]);
				userBean.setLocale(loc);
				ub.setLocale(loc);
			}
			TimeZone userTZ = null;
			if (reportTimeZone != null && !reportTimeZone.isEmpty())
			{
				userTZ = TimeZoneUtil.getTimeZoneForWindowsTZID(reportTimeZone);
			}
			if (userTZ == null)
			{
				userTZ = r.getServerParameters().getDisplayTimeZone();
			}
			userBean.setTimeZone(userTZ);
			userBean.setRepository(r);
			ub.setTimeZone(userTZ);
			ub.setRepository(r);
			Broadcast b = new Broadcast(name, spaceID, username, from, subject,
					body, report, toList, toReport, triggerReport, type, 
					csvSeparator, csvExtension, compressionFormat, prompts, overrideVariables);
			EmailDelivery ed = new EmailDelivery(r, b, userBean, mailHost);
			int numReportsSent = ed.deliverReports(maxReports);
			if (numReportsSent > 0)
			{
				status = ReportStatus.COMPLETE;
			}
			else
			{
				// Either there is an exception in sending or the quota has been exhausted
				status = ReportStatus.FAILED;
			}
		} catch (Exception ex)
		{
			status = ExportReportManager.ReportStatus.FAILED;
			errorException = ex;
			logger.error("Exception in exporting report : " + this.name, ex);
		} finally
		{
			SMIWebUserBean.removeThreadRequest(Thread.currentThread().getId());
			if (ac != null)
				ac.dispose();
		}
	}
	
	public ReportStatus getStatus()
	{
		return status;
	}

	public void setStatus(ReportStatus status)
	{
		this.status = status;
	}

	public String getPath()
	{
		return path;
	}

	public void setPath(String path)
	{
		this.path = path;
	}

	public String getSpaceID()
	{
		return spaceID;
	}

	public void setSpaceID(String spaceID)
	{
		this.spaceID = spaceID;
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}

	public String getReportName()
	{
		return name;
	}

	public void setReportName(String name)
	{
		this.name = name;
	}

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public String getFrom()
	{
		return from;
	}

	public void setFrom(String from)
	{
		this.from = from;
	}

	public String getSubject()
	{
		return subject;
	}

	public void setSubject(String subject)
	{
		this.subject = subject;
	}

	public String getBody()
	{
		return body;
	}

	public void setBody(String body)
	{
		this.body = body;
	}

	public String getReport()
	{
		return report;
	}

	public void setReport(String report)
	{
		this.report = report;
	}

	public String[] getToList()
	{
		return toList;
	}

	public void setToList(String[] toList)
	{
		this.toList = toList;
	}

	public String getToReport()
	{
		return toReport;
	}

	public void setToReport(String toReport)
	{
		this.toReport = toReport;
	}

	public String getTriggerReport()
	{
		return triggerReport;
	}

	public void setTriggerReport(String triggerReport)
	{
		this.triggerReport = triggerReport;
	}

	public int getMaxReports()
	{
		return maxReports;
	}

	public void setMaxReports(int maxReports)
	{
		this.maxReports = maxReports;
	}

	public String getReportLocale()
	{
		return reportLocale;
	}

	public void setReportLocale(String reportLocale)
	{
		this.reportLocale = reportLocale;
	}

	public String getReportTimeZone()
	{
		return reportTimeZone;
	}

	public void setReportTimeZone(String reportTimeZone)
	{
		this.reportTimeZone = reportTimeZone;
	}

	public String getMailHost()
	{
		return mailHost;
	}

	public void setMailHost(String mailHost)
	{
		this.mailHost = mailHost;
	}

	public String getCsvSeparator() {
		return csvSeparator;
	}

	public void setCsvSeparator(String csvSeparator) {
		this.csvSeparator = csvSeparator;
	}

	public String getCsvExtension() {
		return csvExtension;
	}

	public void setCsvExtension(String csvExtension) {
		this.csvExtension = csvExtension;
	}

	public String getCompressionFormat() {
		return compressionFormat;
	}

	public void setCompressionFormat(String compressionFormat) {
		this.compressionFormat = compressionFormat;
	}

	public String getPromptsXmlString() {
		return prompts;
	}

	public void setPrompts(String promptsXmlString) {
		this.prompts = promptsXmlString;
	}

	public String getOverrideVariables() {
		return overrideVariables;
	}

	public void setOverrideVariables(String overrideVariablesXmlString) {
		this.overrideVariables = overrideVariablesXmlString;
	}

	public List<PackageSpaceProperties> getPackageSpacePropertiesList() {
		return packageSpacePropertiesList;
	}

	public void setPackageSpacePropertiesList(List<PackageSpaceProperties> packageSpacePropertiesList) {
		this.packageSpacePropertiesList = packageSpacePropertiesList;
	}
}
