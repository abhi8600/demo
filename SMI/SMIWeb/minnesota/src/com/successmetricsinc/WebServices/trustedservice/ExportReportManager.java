package com.successmetricsinc.WebServices.trustedservice;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.successmetricsinc.util.Util;

public class ExportReportManager
{
	public static Map<String, ExportReportThread> uidToExportReportThreadMapping = Collections.synchronizedMap(new HashMap<String, ExportReportThread>());	
	private static final Logger logger = Logger.getLogger(ExportReportManager.class);
	
	public static enum ReportStatus
	{
		SCHEDULED, RUNNING, COMPLETE, FAILED 
	};
	
	/**
	 * 
	 * @return a unique identifier
	 */
	public static String queueExportReport(String mailHost, String path, String spaceID, String type, String name, String username, String from, String subject, String body,
			String report, String[] toList, String toReport, String triggerReport, int maxReports, String reportLocale, String reportTimeZone,
			String csvSeparator, String csvExtension, String compressionFormat, String promptsXmlString, String overrideVariablesXmlString, String[] packageSpacePropertiesArray)
	{
		String uuid = UUID.randomUUID().toString();
		ExportReportThread exportReportThread = new ExportReportThread();
		exportReportThread.setPath(path);
		exportReportThread.setSpaceID(spaceID);
		exportReportThread.setType(type);
		exportReportThread.setReportName(name);
		exportReportThread.setUsername(username);
		exportReportThread.setFrom(from);
		exportReportThread.setSubject(subject);	
		exportReportThread.setBody(body);
		exportReportThread.setReport(report);
		exportReportThread.setToList(toList);
		exportReportThread.setToReport(toReport);
		exportReportThread.setTriggerReport(triggerReport);
		exportReportThread.setMaxReports(maxReports);
		exportReportThread.setReportLocale(reportLocale);
		exportReportThread.setReportTimeZone(reportTimeZone);		
		exportReportThread.setStatus(ExportReportManager.ReportStatus.SCHEDULED);
		exportReportThread.setMailHost(mailHost);
		exportReportThread.setCsvSeparator(csvSeparator);
		exportReportThread.setCsvExtension(csvExtension);
		exportReportThread.setCompressionFormat(compressionFormat);
		exportReportThread.setPrompts(promptsXmlString);
		exportReportThread.setOverrideVariables(overrideVariablesXmlString);
		exportReportThread.setPackageSpacePropertiesList(Util.getPackageSpacePropertiesList(packageSpacePropertiesArray));
		uidToExportReportThreadMapping.put(uuid, exportReportThread);	
		logger.debug("Starting export Report for " + uuid + " : " + spaceID + " : " + name);
		exportReportThread.start();		
		return uuid;
	}	
	
	public static ExportReportThread getExportReportThread(String uuid)
	{		
		return uidToExportReportThreadMapping.get(uuid);		
	}
	
	public static void removeExportReportThread(String uuid)
	{
		uidToExportReportThreadMapping.remove(uuid);		
	}
}
