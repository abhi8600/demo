/**
 * 
 */
package com.successmetricsinc.WebServices.trustedservice;

import java.io.StringWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.impl.util.OMSerializerUtil;
import org.apache.log4j.Logger;

import com.successmetricsinc.WebServices.BirstWebServiceResult;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author Brad
 * 
 */
public class ExecuteResult
{
	private static final XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
	private static Logger logger = Logger.getLogger(BirstWebServiceResult.class);
	private int errorCode = BaseException.SUCCESS;
	private String errorMessage;
	private String part;
	private String[] groups;
	private long numInputRows;
	private long numOutputRows;
	
	/**
	 * Is this result valid?
	 * 
	 * @return true if the web service call succeeded, false otherwise.
	 */
	public boolean isSuccess()
	{
		return errorCode == BaseException.SUCCESS;
	}

	/**
	 * Returns the error code.
	 * 
	 * @return Returns the error code. A value of SUCCESS_ERROR_CODE indicates no error.
	 */
	public int getErrorCode()
	{
		return errorCode;
	}

	/**
	 * Sets the error code
	 * 
	 * @param errorCode
	 *            the new error code value
	 */
	public void setErrorCode(int errorCode)
	{
		this.errorCode = errorCode;
	}

	/**
	 * Returns a description of the error
	 * 
	 * @return Gets a description of the error
	 */
	public String getErrorMessage()
	{
		return errorMessage;
	}

	/**
	 * Sets the description of the error
	 * 
	 * @param errorMessage
	 *            the new description of the error
	 */
	public void setErrorMessage(String errorMessage)
	{
		this.errorMessage = errorMessage;
	}

	public OMElement toOMElement()
	{
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace ns = fac.createOMNamespace("", "");
		OMElement resultElement = fac.createOMElement(this.getClass().getName(), ns);
		XmlUtils.addContent(fac, resultElement, "ErrorCode", this.errorCode, ns);
		XmlUtils.addContent(fac, resultElement, "ErrorMessage", this.errorMessage, ns);
		XmlUtils.addContent(fac, resultElement, "Part", this.part, ns);
		OMElement groupsElement = fac.createOMElement("Groups", ns);
		if (groups != null) {
			for (String group : groups) {
				XmlUtils.addContent(fac, groupsElement, "Group", group, ns);
			}
		}
		resultElement.addChild(groupsElement);
		return resultElement;
	}

	public String toString()
	{
		OMElement el = this.toOMElement();
		StringWriter w = new StringWriter();
		XMLStreamWriter writer;
		try
		{
			writer = outputFactory.createXMLStreamWriter(w);
			OMSerializerUtil.serializeByPullStream(el, writer);
			writer.flush();
		} catch (XMLStreamException e)
		{
			logger.error(e, e);
		}
		return w.toString();
	}

	public String getPart()
	{
		return part;
	}

	public void setPart(String part)
	{
		this.part = part;
	}

	/**
	 * @return the groups
	 */
	public String[] getGroups() {
		return groups;
	}

	/**
	 * @param groups the groups to set
	 */
	public void setGroups(String[] groups) {
		this.groups = groups;
	}

	public long getNumInputRows()
	{
		return numInputRows;
	}

	public void setNumInputRows(long numInputRows)
	{
		this.numInputRows = numInputRows;
	}

	public long getNumOutputRows()
	{
		return numOutputRows;
	}

	public void setNumOutputRows(long numOutputRows)
	{
		this.numOutputRows = numOutputRows;
	}
	
}
