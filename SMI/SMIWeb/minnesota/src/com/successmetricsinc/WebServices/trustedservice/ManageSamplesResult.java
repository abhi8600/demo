package com.successmetricsinc.WebServices.trustedservice;

import java.io.StringWriter;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.impl.util.OMSerializerUtil;
import org.apache.log4j.Logger;

import com.successmetricsinc.WebServices.BirstWebServiceResult;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.XmlUtils;

public class ManageSamplesResult
{
	private static final XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
	private static Logger logger = Logger.getLogger(BirstWebServiceResult.class);
	private int errorCode = BaseException.SUCCESS;
	private String errorMessage;
	private String[] dimensions;
	private String[] levels;
	private int[] numSamples;
	private int[] fileSize;
	private String[] mapDimensions;
	private String[] mapLevels;
	private String[] mapSources;

	/**
	 * Is this result valid?
	 * 
	 * @return true if the web service call succeeded, false otherwise.
	 */
	public boolean isSuccess()
	{
		return errorCode == BaseException.SUCCESS;
	}

	/**
	 * Returns the error code.
	 * 
	 * @return Returns the error code. A value of SUCCESS_ERROR_CODE indicates no error.
	 */
	public int getErrorCode()
	{
		return errorCode;
	}

	/**
	 * Sets the error code
	 * 
	 * @param errorCode
	 *            the new error code value
	 */
	public void setErrorCode(int errorCode)
	{
		this.errorCode = errorCode;
	}

	public OMElement toOMElement()
	{
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace ns = fac.createOMNamespace("", "");
		OMElement ret = fac.createOMElement(this.getClass().getName(), ns);
		XmlUtils.addContent(fac, ret, "ErrorCode", this.errorCode, ns);
		return ret;
	}

	public String toString()
	{
		OMElement el = this.toOMElement();
		StringWriter w = new StringWriter();
		XMLStreamWriter writer;
		try
		{
			writer = outputFactory.createXMLStreamWriter(w);
			OMSerializerUtil.serializeByPullStream(el, writer);
			writer.flush();
		} catch (XMLStreamException e)
		{
			logger.error(e, e);
		}
		return w.toString();
	}

	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage()
	{
		return errorMessage;
	}

	/**
	 * @param errorMessage
	 *            the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage)
	{
		this.errorMessage = errorMessage;
	}

	public String[] getDimensions()
	{
		return dimensions;
	}

	public void setDimensions(String[] dimensions)
	{
		this.dimensions = dimensions;
	}

	public String[] getLevels()
	{
		return levels;
	}

	public void setLevels(String[] levels)
	{
		this.levels = levels;
	}

	public int[] getNumSamples()
	{
		return numSamples;
	}

	public void setNumSamples(int[] numSamples)
	{
		this.numSamples = numSamples;
	}

	public int[] getFileSize()
	{
		return fileSize;
	}

	public void setFileSize(int[] fileSize)
	{
		this.fileSize = fileSize;
	}

	public String[] getMapDimensions()
	{
		return mapDimensions;
	}

	public void setMapDimensions(String[] mapDimensions)
	{
		this.mapDimensions = mapDimensions;
	}

	public String[] getMapLevels()
	{
		return mapLevels;
	}

	public void setMapLevels(String[] mapLevels)
	{
		this.mapLevels = mapLevels;
	}

	public String[] getMapSources()
	{
		return mapSources;
	}

	public void setMapSources(String[] mapSources)
	{
		this.mapSources = mapSources;
	}
}
