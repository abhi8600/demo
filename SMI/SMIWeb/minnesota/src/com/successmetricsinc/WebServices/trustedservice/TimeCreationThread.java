package com.successmetricsinc.WebServices.trustedservice;

import com.amazonaws.auth.AWSCredentials;
import com.successmetricsinc.ExecuteSteps.Step;
import com.successmetricsinc.ExecuteSteps;
import com.successmetricsinc.Repository;
import com.successmetricsinc.query.Database.DatabaseType;
import com.successmetricsinc.status.Status;
import com.successmetricsinc.status.Status.StatusCode;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.warehouse.TimeDefinition;
import org.apache.log4j.Logger;


public class TimeCreationThread  extends Thread{

	private static Logger logger = Logger.getLogger(TimeCreationThread.class);
	
	private TimeAccessParams params;
	private Repository timeRepository;
	private Status status;
	
	public TimeCreationThread(Status status, Repository timeRepository, TimeAccessParams params){
		this.status = status;
		this.timeRepository = timeRepository;
		this.params = params;			
	}
	
	@Override
	public void run() {
		
		TimeDefinition td = timeRepository.getTimeDefinition();
		boolean success = false;
		try {			
			td.createTimeSchema(timeRepository);
			DatabaseType dbType = timeRepository.getDefaultConnection().DBType;
			if(dbType == DatabaseType.Redshift){
				RedshiftTimeCredentials redshiftParams = (RedshiftTimeCredentials) params;
				String folder = redshiftParams.getFolderName();
				String awsAccessKeyId = redshiftParams.getAwsAccessKeyId();
				String awsSecretKey = redshiftParams.getAwsSecretKey();
				String awsToken = redshiftParams.getAwsToken();
				AWSCredentials s3credentials = Util.getAWSCredentions(awsAccessKeyId, awsSecretKey, awsToken);
				td.loadTimeSchema(timeRepository, folder, s3credentials); 
				success = true;
			}else if(dbType == DatabaseType.Hana){
				SAPHanaTimeAccessParams hanaParams = (SAPHanaTimeAccessParams) params;
				td.loadTimeSchema(timeRepository, hanaParams.getFolderName(), null);
			}
		} 
		catch (Exception ex) {
			success = false;
			logger.error("Exception while creating time dimension tables", ex);
		}
		finally {
			try{
				StatusCode statusCode = success ? StatusCode.Complete : StatusCode.Failed;
				Step step = new Step(ExecuteSteps.StepCommand.CreateTimeSchema, com.successmetricsinc.ExecuteSteps.StepCommandType.ETL, null);
				status.logStatus(step, "TimeCreation", statusCode);
			}catch(Exception ex2){
				logger.error("Exception in setting the end status for the time creation process", ex2);
			}
		}
		
		
	}

}
