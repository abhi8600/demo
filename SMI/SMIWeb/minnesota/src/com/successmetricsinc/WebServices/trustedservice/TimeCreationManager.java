package com.successmetricsinc.WebServices.trustedservice;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.input.SAXBuilder;

import com.successmetricsinc.ExecuteSteps;
import com.successmetricsinc.ExecuteSteps.Step;
import com.successmetricsinc.Repository;
import com.successmetricsinc.query.Database;
import com.successmetricsinc.query.Database.DatabaseType;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.status.Status;
import com.successmetricsinc.status.Status.StatusCode;
import com.successmetricsinc.util.TimeCreationException;


public class TimeCreationManager {

	private static final Logger logger = Logger.getLogger(TimeCreationManager.class);
		
	private Repository timeRepository;	
	private String timeRepositoryPath;	
	private TimeAccessParams timeAccessParams;
	private Status status;
	
	private boolean initialized = false;
	public TimeCreationManager(String timeRepositoryPath){
		this.timeRepositoryPath = timeRepositoryPath;		
	}
	
	private void init() throws Exception{
		if(!initialized){			
			initializeRepository();
			validateParams();
			initStatus();
			initialized = true;
		}		
	}
	
	private void validateParams() throws TimeCreationException{
		DatabaseType dbType = timeRepository.getDefaultConnection().DBType;
		if(dbType == DatabaseType.Redshift){
			validateRedshiftParams();
		}
		if(dbType == DatabaseType.Hana)
		{
			validateSAPHanaParams();	
		}
		
	}
	
	private void validateRedshiftParams() throws TimeCreationException{
		if(timeAccessParams == null){
			logger.error("No time access credentials given");
			throw new TimeCreationException("No paramters available"); 
		}
		else{
			if(timeAccessParams instanceof RedshiftTimeCredentials){
				RedshiftTimeCredentials accessParams = (RedshiftTimeCredentials) timeAccessParams;
				String awsAccessID = accessParams.getAwsAccessKeyId();
				String awsSecretKey = accessParams.getAwsSecretKey();
				if(awsAccessID == null || awsAccessID.trim().length() == 0 
						|| awsSecretKey == null || awsSecretKey.trim().length() == 0){
					logger.error("Invalid redshift access parameters provided " + timeAccessParams);
					throw new TimeCreationException("Invalid access paramertes provided");
				}							
			}
		}
	}	
	
	private void validateSAPHanaParams() throws TimeCreationException
	{
		if(timeAccessParams == null){
			logger.error("No time access credentials given");
			throw new TimeCreationException("No paramters available"); 
		}else{
			if(timeAccessParams instanceof SAPHanaTimeAccessParams){
				SAPHanaTimeAccessParams accessParams = (SAPHanaTimeAccessParams) timeAccessParams;
				if(accessParams.getFolderName() == null)
				{
					logger.error("Invalid folderName provided for SAP Hana" + timeAccessParams);
					throw new TimeCreationException("Invalid folderName provided for SAP Hana");
				}				
			}
		}
	}
	
	private void initStatus() throws Exception{
		// create dbo schema first
		createMainGlobalSchema();
		status = new Status(timeRepository, "1", null);
	}
	
	private void initializeRepository() throws Exception{
		SAXBuilder builder = new SAXBuilder();
		Document d = null;
		File nf = new File(timeRepositoryPath);
		try
		{
			d = builder.build(nf);
			timeRepository = new Repository(d, false, null, nf.lastModified(), false, null, null);
		} catch (FileNotFoundException ex)
		{
			logger.error("Time Repository file not found " + this.timeRepositoryPath, ex);
			throw ex;
		}catch(Exception ex2){
			logger.error("Unable to initalize time repository " + this.timeRepositoryPath, ex2);
			throw ex2;
		}
		
	}
	
	private void createMainGlobalSchema() throws Exception{
		DatabaseConnection dconn = timeRepository.getDefaultConnection();
		if (!Database.schemaExists(dconn))
			Database.createSchema(dconn);
	}
	
	public void setTimeAccessParams(TimeAccessParams timeAccessParams){
		this.timeAccessParams = timeAccessParams;
	}
	
	public void startTableCreation() throws Exception{
		init();
		clearTimeCreationHistory();
		addStatusEntry(StatusCode.Running);
		try{
			TimeCreationThread tcThread = new TimeCreationThread(status, timeRepository, timeAccessParams);
			tcThread.start();
		}
		finally{
			
		}
		
	}
	
	private void addStatusEntry(Status.StatusCode statusCode) throws SQLException{
		Step step = new Step(ExecuteSteps.StepCommand.CreateTimeSchema, com.successmetricsinc.ExecuteSteps.StepCommandType.ETL, null);
		status.logStatus(step, "TimeCreation", statusCode);
	}
	
	
	private void clearTimeCreationHistory(){
		status.clearETLHistory(false);
		status.clearModelingHistory();
	}
}
