/**
 * $Id: SubjectAreaManager.java,v 1.10 2012-10-25 01:31:02 BIRST\gsingh Exp $
 *
 * Copyright (c) 2007-2010 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.WebServices.SubjectArea;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.jdom.Element;

import com.successmetricsinc.PackageSpaceProperties;
import com.successmetricsinc.Repository;
import com.successmetricsinc.packages.PObject;
import com.successmetricsinc.packages.PackageDefinition;

/**
 * The purpose of this class is to encapsulate all the operations/behaviors required by
 * the {@link SubjectAreaWS} webservice for retrieving and persisting subject areas.
 * @author pconnolly
 */
public class SubjectAreaManager {

	public static final String XML_EXTENSION = "xml";
	public static final String SUBJECT_AREA = "subjectarea_";
	public static final String SUBJECT_AREA_METADATA = "subjectarea-metadata_";
	public static final String SUBJECT_AREA_DIR = "custom-subject-areas";
	public static final String SUBJECT_AREA_NS = "http://SubjectArea.WebServices.successmetricsinc.com";
	public static final String DEFAULT_SUBJECT_AREA = "Default Subject Area";

	private static Logger logger = Logger.getLogger(SubjectAreaWS.class);

	private Repository repository;
	
	public SubjectAreaManager(final Repository repository) {
		this.repository = repository;
	}
	
	public List<Element> getCustomSubjectAreasNodes() {
		// Get list of custom subject area files
		final List<File> csaFiles = getCustomSubjectAreaFiles();
		return getCustomSubjectAreasNodes(csaFiles);
	}
	
	public List<Element> getCustomSubjectAreasNodes(List<File> csaFiles) {
		final List<Element> customSubjectAreasNodes = new ArrayList<Element>();		
		// Read each file and convert to a subject area, adding it to the list of sa's
		for (File csaFile : csaFiles) {
			final Element csaElement = com.successmetricsinc.util.FileUtils.readFileToDocument(csaFile);
			customSubjectAreasNodes.add(csaElement);
		}
		return customSubjectAreasNodes;
	}
	
	public static List<Element> getCustomSubjectAreasNodes(String spaceDirectory){
		if(spaceDirectory == null){
			return null;
		}
		SubjectAreaManager saManager = new SubjectAreaManager(null);
		File csaDirectory = saManager.getCustomSubjectAreaDirectory(new File(spaceDirectory));
		final List<File> csaFiles = saManager.getCustomSubjectAreaFiles(csaDirectory);
		return saManager.getCustomSubjectAreasNodes(csaFiles);
	}
	
	/**
	 * retrieves the custom subject area from the imported packages
	 * @return
	 */
	public List<Element> getImportedCustomSubjectAreas(){
		List<Element> response = new ArrayList<Element>();
		List<PackageDefinition> importedPackagesList = repository.getImportedPackageDefinitionList();
		if(importedPackagesList != null && importedPackagesList.size() > 0){
			// if there are multiple packages imported from the same directory, read 
			// custom subject area files only once
			Map<String, List<Element>> spaceIDToCsaElements = new HashMap<String, List<Element>>();
			Map<String, List<String>> spaceIDToCSAPackages = new HashMap<String, List<String>>();
			for(PackageDefinition packageDefinition : importedPackagesList){
				PackageSpaceProperties packageSpaceProps = packageDefinition.getPackageSpaceProperties();
				if(packageSpaceProps == null){	
					continue;
				}
				
				List<String> importedCSANames = PackageDefinition.getPObjectNames(packageDefinition.getCustomSubjectAreas());
				if(importedCSANames != null && importedCSANames.size() > 0){
					String spaceID = packageSpaceProps.getSpaceID();
					if(!spaceIDToCsaElements.containsKey(spaceID)){
						final File repositoryRoot = new File(packageSpaceProps.getSpaceDirectory());
						File importedPacakgeSubjectAreaDir = getCustomSubjectAreaDirectory(repositoryRoot);
						List<File> importedCSAFiles = getCustomSubjectAreaFiles(importedPacakgeSubjectAreaDir);
						List<Element> importedCSAElements = new ArrayList<Element>();
						for (File csaFile : importedCSAFiles) {
							Element csaElement = com.successmetricsinc.util.FileUtils.readFileToDocument(csaFile);
							importedCSAElements.add(csaElement);
						}
						spaceIDToCsaElements.put(spaceID, importedCSAElements);
					}
					
					if(!spaceIDToCSAPackages.containsKey(spaceID)){
						spaceIDToCSAPackages.put(spaceID, new ArrayList<String>());
					}
					spaceIDToCSAPackages.get(spaceID).addAll(importedCSANames);
				}
			}
			
			// filter out CSAs for packages
			for(Entry<String, List<String>> csaObject : spaceIDToCSAPackages.entrySet()){
				String importedSpaceID = csaObject.getKey();
				List<String> csaNames = csaObject.getValue();
				List<Element> csaElements = spaceIDToCsaElements.get(importedSpaceID);
				if(csaElements != null && csaElements.size() > 0 && 
						csaNames != null && csaNames.size() > 0){
					for(Element csaElement : csaElements){
						String name = csaElement.getChildText(ExtendedSubjectAreaNode.LABEL);
						boolean found = false;
						for(String csaName : csaNames){
							if(csaName.equalsIgnoreCase(name)){
								found = true;
								break;
							}
						}
						if(found){
							response.add(csaElement);
						}
					}
				}
			}
		}
		
    	return response;
	}
	
	
	/**
	 * This method is specifically for text files.  Files are input as Strings.  A filename is
	 * specified as input and all files are stored in the current space's root directory.
	 * Initially used to store custom subject area XML files.
	 * @param fileName is the file name, including extension.
	 * @param fileInput is the String content to be written to this file.
	 * @throws IOException in case of pear-shaped IO catastrophes.
	 */
	public void saveRepositoryFile(final String fileName, final String fileInput) 
			throws IOException {
		if (logger.isTraceEnabled())
		{
			logger.trace("'saveRepositoryFile()' entered with 'fileName':  " + fileName);
			logger.trace("'saveRepositoryFile()' entered with 'fileInput': " + fileInput.substring(0, 64));
		}
		
		// Make sure that the subdirectory is there
		final String saDirPath = createSubjectAreaDirectory();
		
		// Build file path and write it
		writeSubjectAreaFile(saDirPath, fileName, fileInput);
	}
	
	public String deleteSubjectArea(final String subjectAreaName) {
		logger.trace("'deleteSubjectArea()' for subject area: " + subjectAreaName);
		String errorMessage = "";

		// Construct the full directory path for the subject area file
    	final File repositoryRootDir = repository.getRepositoryRoot();
		final String fullFilePath = repositoryRootDir + File.separator + SUBJECT_AREA_DIR 
				+ File.separator + SUBJECT_AREA + subjectAreaName + ".xml";
		
		// Delete it
		final File subjectAreaFile = new File(fullFilePath);
		subjectAreaFile.delete();
		
		// Construct the full directory path for the metadata file
		final String fullMetadataFilePath = repositoryRootDir + File.separator + SUBJECT_AREA_DIR 
				+ File.separator + SUBJECT_AREA_METADATA + subjectAreaName + ".xml";
		
		// Delete that too
		final File subjectAreaMetadataFile = new File(fullMetadataFilePath);
		subjectAreaMetadataFile.delete();
		
		return errorMessage;
	}

	public SubjectAreasGroups getSubjectAreasGroups() {
		logger.trace("'getSubjectAreasGroups()' for all subject areas");

		// Construct the full path for the subject area directory
    	final File repositoryRootDir = repository.getRepositoryRoot();
		final String fullSubjectAreaDirPath = repositoryRootDir + File.separator + SUBJECT_AREA_DIR; 
		
		// Get the list of metadata files
		final File fullSubjectAreaDir = new File(fullSubjectAreaDirPath);
		final String[] metadataFiles = fullSubjectAreaDir.list(SubjectAreaMetadataFilenameFilter.getInstance());
		if (metadataFiles == null || metadataFiles.length == 0) {
			final SubjectAreasGroups emptySaGroups = new SubjectAreasGroups(new String[0]);
			return emptySaGroups;
		}
		final String[] metadataFullFilepaths = new String[metadataFiles.length];
		for (int i = 0; i < metadataFiles.length; i++) {
			metadataFullFilepaths[i] = fullSubjectAreaDirPath + File.separator + metadataFiles[i];
		}
		
		// Dump the netadataFiles into an instance of SubjectAreasGroups and return
		final SubjectAreasGroups subjectAreasGroups = new SubjectAreasGroups(metadataFullFilepaths);
		
		return subjectAreasGroups;
	}

	
	public String createSubjectAreaMetadata(final String subjectAreaName, final String[] groupNames) {
		StringBuilder sb = new StringBuilder();
		
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<CustomSubjectArea>");
		sb.append("<Name>").append(subjectAreaName).append("</Name>");
		sb.append("<Groups>");
		for (String groupName : groupNames) {
			sb.append("<Group>").append(groupName).append("</Group>");
		}
		sb.append("</Groups>");
	    sb.append("</CustomSubjectArea>");
	    
	    logger.trace("'createSubjectAreaMetadata()': " + sb.toString());
	    return sb.toString();
	}
	
	public boolean areNoCustomSubjectAreas() {
		final List<File> subjectAreaFiles = getCustomSubjectAreaFiles();
		if (subjectAreaFiles.isEmpty()) {
			return true;
		} else {
			return false;
		}
	}
	
	public void addDefaultSubjectAreaMetadata() throws IOException {
		logger.trace("'addDefaultSubjectAreaMetadata()' for space: " + getNameOfSpace());
		// Generate standard default USER$ XML
		final String fileInput = createSubjectAreaMetadata(DEFAULT_SUBJECT_AREA, new String[]{"USER$"});

		// Make sure that the subdirectory is there
		final String saDirPath = createSubjectAreaDirectory();
		
		// Build file path and write it
		final String fileName = "subjectarea-metadata_" + DEFAULT_SUBJECT_AREA + ".xml";
		writeSubjectAreaFile(saDirPath, fileName, fileInput);
	}

	/* --------------------------------------------------------------------------------------------
	 * --------------------------------------------------------------------------------------------
	 * --- Helper methods                                                                       ---
	 * --------------------------------------------------------------------------------------------
	 * -------------------------------------------------------------------------------------------- */
	
	
	private File getCustomSubjectAreaDirectory() {
		final File repositoryRootDir = repository.getRepositoryRoot();
		return getCustomSubjectAreaDirectory(repositoryRootDir);
	}
	
    private File getCustomSubjectAreaDirectory(File repositoryRootDir) {    	
		final String subjectAreaPath = repositoryRootDir + File.separator + SUBJECT_AREA_DIR;
		final File subjectAreaFile = new File(subjectAreaPath);
		return subjectAreaFile;
    }
    
    public List<File> getCustomSubjectAreaFiles() {
    	final File subjectAreaDir = getCustomSubjectAreaDirectory();
    	return getCustomSubjectAreaFiles(subjectAreaDir);
    }
    
    public List<File> getCustomSubjectAreaFiles(File subjectAreaDir) {
    	final List<File> files = new ArrayList<File>();
    	final String[] filenames = subjectAreaDir.list(SubjectAreaFilenameFilter.getInstance());
    	if (filenames != null && filenames.length > 0) {
    		for (String filename : filenames) {
    			final File file = new File(subjectAreaDir, filename);
        		files.add(file);
    		}
    	}
    	return files;
    }
    
    public List<File> getCustomSubjectAreaMetadataFiles() {
    	final List<File> files = new ArrayList<File>();
    	final File subjectAreaDir = getCustomSubjectAreaDirectory();
    	final String[] filenames = subjectAreaDir.list(SubjectAreaMetadataFilenameFilter.getInstance());
    	if (filenames != null && filenames.length > 0) {
    		for (String filename : filenames) {
    			final File file = new File(subjectAreaDir, filename);
        		files.add(file);
    		}
    	}
    	return files;
    }
    
    private String createSubjectAreaDirectory() {
		// Make sure that the subdirectory is there
    	final File repositoryRootDir = repository.getRepositoryRoot();
		final String saDirPath = repositoryRootDir + File.separator + SUBJECT_AREA_DIR;
		File saDir = new File(saDirPath);
		saDir.mkdir();
		return saDirPath;
    }
    
    private void writeSubjectAreaFile(final String saDirPath, final String fileName, final String fileInput) 
    		throws IOException {
		// Build file path and write it
		String fullFilePath = saDirPath + File.separator + fileName; 
		File subjectAreaFile = new File(fullFilePath);
		subjectAreaFile.delete();
		logger.trace("Writing repository file: " + subjectAreaFile.getCanonicalPath());
		FileUtils.writeStringToFile(subjectAreaFile, fileInput, "UTF-8");
    }
    
    private String getNameOfSpace() {
    	return repository.getApplicationName();
    }
    
	/**
	 * A FilenameFilter that allows only custom subject area files. 
	 */
	public static class SubjectAreaFilenameFilter implements FilenameFilter {
		
		private static SubjectAreaFilenameFilter instance = new SubjectAreaFilenameFilter(); 

		public static SubjectAreaFilenameFilter getInstance() { return instance; }
		
		public boolean accept(File dir, String name) {
			// Get the file extension
			final String extension = com.successmetricsinc.util.FileUtils.getFileExtension(name);
			if (extension != null && extension.equals("xml")) {
				// Strip off extension
				final String basename = com.successmetricsinc.util.FileUtils.getFileBasename(name);
				if (basename.startsWith(SUBJECT_AREA)) {
					return true;
				}
			}
			return false;
		}
	}
	
	/**
	 * A FilenameFilter that allows only custom subject area metadata files. 
	 */
	public static class SubjectAreaMetadataFilenameFilter implements FilenameFilter {
		
		private static SubjectAreaMetadataFilenameFilter instance = new SubjectAreaMetadataFilenameFilter(); 

		public static SubjectAreaMetadataFilenameFilter getInstance() { return instance; }
		
		public boolean accept(File dir, String name) {
			// Get the file extension
			final String extension = com.successmetricsinc.util.FileUtils.getFileExtension(name);
			if (extension != null && extension.equals(XML_EXTENSION)) {
				// Strip off extension
				final String basename = com.successmetricsinc.util.FileUtils.getFileBasename(name);
				if (basename.startsWith(SUBJECT_AREA_METADATA)) {
					return true;
				}
			}
			return false;
		}
	}
}
