package com.successmetricsinc.WebServices.SubjectArea;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.OMText;

import com.successmetricsinc.WebServices.IWebServiceResult;
import com.successmetricsinc.util.XmlUtils;

public class SubjectAreaModifiedNodes implements IWebServiceResult {
	private Map<String, List<SubjectAreaNode>> _saModifiedNodes;
	
	public SubjectAreaModifiedNodes(){	
		_saModifiedNodes = new HashMap<String, List<SubjectAreaNode>>();
	}
	
	public void addModifiedSubjectAreaNodes(String subjectAreaName, List<SubjectAreaNode> nodes){
		_saModifiedNodes.put(subjectAreaName, nodes);
	}
	
	@Override
	public void addContent(OMElement parent, OMFactory factory, OMNamespace ns) {
		java.util.Set<String> keys = this._saModifiedNodes.keySet();
		
		Iterator<String> it = keys.iterator();
		while(it.hasNext()){
			String k = it.next();
			List<SubjectAreaNode> saNodes = _saModifiedNodes.get(k);
			StringBuilder buf = new StringBuilder();
			buf.append("<SA name=\"" + k + "\">");
			for(SubjectAreaNode node : saNodes){
				buf.append("<n><v>");
				buf.append(XmlUtils.encode(node.getName()));
				buf.append("</v><vl>");
				buf.append(node.isValid());
				buf.append("</vl>");
				buf.append("<av>");
				buf.append(node.isAllChildValid());
				buf.append("</av>");
				buf.append("</n>");
			}
			buf.append("</SA>");
			OMText text = factory.createOMText(buf.toString());
			parent.addChild(text);
		}
	}
}
