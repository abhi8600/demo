package com.successmetricsinc.WebServices.SubjectArea;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.List;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import com.successmetricsinc.WebServices.SubjectArea.ExtendedSubjectAreaNode;

public class CustomSubjectArea implements Serializable
{
	// Member var(s).
	private StringReader _extendedXml;
	private StringBuilder _xml;
	private String _name;
	private int _depth = 0;
	
	// Constant(s).
	private static final long serialVersionUID = 1L;
	private static final String _DEPTH_ROOT_NODE = "1";
	private static final String _DEPTH_FORMAT = "#.#";
	private static final String _TRANSFORM_EXCEPTION = "Custom subject area xml transformation failed";
	public static final String _REPORT_LABEL = ExtendedSubjectAreaNode.REPORT_LABEL;
	public static final String _DESCRIPTION = ExtendedSubjectAreaNode.DESCRIPTION;
	public static final String _ID = ExtendedSubjectAreaNode.ID;
	public static final String _TYPE = ExtendedSubjectAreaNode.TYPE;
	public static final String _PERIOD = ExtendedSubjectAreaNode.PERIOD;
	public static final String _EMPTY_STRING = ExtendedSubjectAreaNode.EMPTY_STRING;
	public static final String _LABEL = SubjectAreaNode.LABEL;
	public static final String _VAL = SubjectAreaNode.NAME;
	public static final String _CHILD = SubjectAreaNode.CHILD;
	public static final String _NODE = SubjectAreaNode.NODE;
	public static final String _VALID = SubjectAreaNode.VALID;
	public static final String _VALID_TRUE = "true";
	public static final String _TYPE_T = "t";
	public static final String _FORWARD_SLASH = "/";
	public static final String _START_TAG_OPEN = "<";
	public static final String _START_TAG_CLOSE = ">";
	public static final String _END_TAG_OPEN = "</";
	public static final String _END_TAG_CLOSE = ">";
	public static final String _EQUALS = "=";
	public static final String _SPACE = " ";
	public static final String _DOUBLE_QUOTE = "\"";
	public static final String _LEAF = "lf";
	public static final String _OLAP_MEASURE = "om";
	public static final String _OLAP_DIM = "ohf";
	public static final String _IMPORTED = "isImported";

	// Constructor(s).
	public CustomSubjectArea(String extendedXml)
	{
		setExtendedXml(extendedXml);
		setXml(_EMPTY_STRING);
		parseCSAName(extendedXml);
	}
	
	// Helper method(s).
	private void buildNodeTag(Element element, StringBuilder sb) {
		sb.append(_START_TAG_OPEN).append(_NODE).append(_SPACE).
			append(_ID).append(_EQUALS).append(_DOUBLE_QUOTE).append(_DEPTH_ROOT_NODE).append(_PERIOD).append(getDepth()).append(_DOUBLE_QUOTE).
			append(_SPACE).append(_TYPE).append(_EQUALS).append(_DOUBLE_QUOTE).append(element.getAttributeValue(_TYPE_T)).append(_DOUBLE_QUOTE).
			append(_END_TAG_CLOSE);
		buildTag(_VAL, element.getAttributeValue(_VAL), sb);
		buildTag(_REPORT_LABEL, element.getAttributeValue(_REPORT_LABEL), sb);
		buildTag(_LABEL, element.getAttributeValue(_LABEL), sb);
		buildTag(_DESCRIPTION, element.getAttributeValue(_DESCRIPTION), sb);
		buildTag(_VALID, _VALID_TRUE, sb);
		
		// Verify test cases for these...
		buildTag(_OLAP_DIM, element.getAttributeValue(_OLAP_DIM), sb);
		buildTag(_LEAF, element.getAttributeValue(_LEAF), sb);
		buildTag(_OLAP_MEASURE, element.getAttributeValue(_OLAP_MEASURE), sb);
		buildTag(_IMPORTED, element.getAttributeValue(_IMPORTED), sb);
	}
	
	private void buildOpeningTag(String name, StringBuilder sb) {
		sb.append(_START_TAG_OPEN).append(name).append(_START_TAG_CLOSE);
	}
	
	private void buildTag(String name, String value, StringBuilder sb) {
		if (value != null && !value.equals(_EMPTY_STRING)) {
			sb.append(_START_TAG_OPEN).append(name).append(_START_TAG_CLOSE).append(value).append(_END_TAG_OPEN).append(name).append(_END_TAG_CLOSE);
		} else {
			if (name.equals(_DESCRIPTION)) {
				sb.append(_START_TAG_OPEN).append(_DESCRIPTION).append(_FORWARD_SLASH).append(_START_TAG_CLOSE);
			}
		}
	}
	
	private void buildClosingTag(String name, StringBuilder sb) {
		sb.append(_END_TAG_OPEN).append(name).append(_END_TAG_CLOSE);
	}
	
	private void incrementDepth() {
		setDepth(getDepth() + 1);
	}
	
	public void parseCSAName(String extendedXml) {
		SAXBuilder sb = new SAXBuilder();
		try {
			Document doc = sb.build(new StringReader(extendedXml));
			Element root = doc.getRootElement();
			setCSAName(root.getChild(_NODE).getAttributeValue(_LABEL));
		} catch (JDOMException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	
	public void transform() {
		try {
			SAXBuilder saxBuilder = new SAXBuilder();
			Document doc = saxBuilder.build(getExtendedXml());
			StringBuilder sb = new StringBuilder();
			traverseTree(doc.getRootElement().getChild(_NODE), sb);
			setXml(sb);
		} catch (Exception ex) {
			setXml(_EMPTY_STRING);
			throw new RuntimeException(_TRANSFORM_EXCEPTION);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void traverseTree(Element element, StringBuilder sb) {
		// Transform node.
		String name = element.getName();
		if (name.equals(_CHILD)) {
			buildOpeningTag(name, sb);
		} else {
			if (name.equals(_NODE)) {
				buildNodeTag(element, sb);
				incrementDepth();
			}
		}

		// Iterate over children.
		List<Element> children = element.getChildren();
		Iterator<Element> iterator = (Iterator<Element>) children.iterator();
		while (iterator.hasNext()) {
			traverseTree((Element)iterator.next(), sb);
		}
		buildClosingTag(name, sb);
	}
	
	public String extendedXmlToString() {
		return getExtendedXml().toString();
	}
	
	private double roundTwoDecimals(double d) { 
	      DecimalFormat twoDForm = new DecimalFormat(_DEPTH_FORMAT); 
	      return Double.valueOf(twoDForm.format(d));
	}
	
	public String xmlToString() {
		return getXml().toString();
	}

	// Getter and setter helper method(s).
	private int getDepth() {
		return _depth;
	}
	
	public StringReader getExtendedXml() {
		return _extendedXml;
	}
	
	public String getName() {
		return _name;
	}

	public StringBuilder getXml() {
		return _xml;
	}
	
	private void setDepth(int depth) {
		_depth = depth;
	}

	public void setExtendedXml(String extendedXml) {
		_extendedXml = new StringReader(extendedXml);
	}
	
	private void setCSAName(String name) {
		_name = name;
	}

	public void setXml(String xml) {
		_xml = new StringBuilder(xml);
	}
	
	public void setXml(StringBuilder xml) {
		_xml = xml;
	}
}