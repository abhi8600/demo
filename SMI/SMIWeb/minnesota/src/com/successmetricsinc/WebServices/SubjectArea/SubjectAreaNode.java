/**
 * $Id: SubjectAreaNode.java,v 1.22 2012-10-23 19:20:04 BIRST\gsingh Exp $
 *
 * Copyright (c) 2007-2010 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */


package com.successmetricsinc.WebServices.SubjectArea;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;

import com.successmetricsinc.Column;
import com.successmetricsinc.Folder;
import com.successmetricsinc.WebServices.IWebServiceResult;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
public class SubjectAreaNode implements IWebServiceResult{
	
	public static final String NODE = "n";
	public static final String NAME = "v";
	public static final String LABEL = "l";
	public static final String CHILD = "c";
	public static final String VALID = "vl";

	private static Logger logger = Logger.getLogger(SubjectAreaNode.class);
	
	protected String label;
	protected String name;
	protected boolean isValid;
	protected boolean isLeafNode;
	protected boolean isTopNodeSA = false;
	protected boolean isAllChildValid = false;
	protected boolean isImported = false;
	
	private List<SubjectAreaNode> children;

	public SubjectAreaNode() {
	}
	
	public SubjectAreaNode(String name, String label) {
		init(name, label, false, true);
	}
	
	public SubjectAreaNode(String name, String label, boolean isLeafNode, boolean isValid) {
		init(name, label, isLeafNode, isValid);
	}
	
	public boolean isAllChildValid(){
		return this.isAllChildValid;
	}

	public void markAsRootSubjectArea(){
		isTopNodeSA = true;
	}
	
	public boolean isRootSubjectArea(){
		return isTopNodeSA;
	}
	
	public void markAsImportedSubjectArea(){
		isImported = true;
	}
	
	public boolean isImportedSubjectArea(){
		return isImported;
	}
	
	@SuppressWarnings("unchecked")
	private SubjectAreaNode(Element parent, Namespace ns) {
		this.setName(XmlUtils.getStringContent(parent, "name", ns));
		this.setLabel(XmlUtils.getStringContent(parent, "label", ns));
		this.setValid(XmlUtils.getBooleanContent(parent, "isValid", ns));
		this.setLeafNode(XmlUtils.getBooleanContent(parent, "isLeafNode", ns));
		
		List<Element> entities = parent.getChildren();
		if (entities != null) {
			List<SubjectAreaNode> childrens = new ArrayList<SubjectAreaNode>();
			for (Element el : entities) {
				SubjectAreaNode child = new SubjectAreaNode(el, ns);
				childrens.add(child);
			}
			this.setChildren(childrens);
		}
	}
	
	public static SubjectAreaNode parseXML(String xml) {
		SAXBuilder builder = new SAXBuilder();
		Document doc = null;
		StringReader characterStream = null;
		try {
			characterStream = new StringReader(xml);
			doc = builder.build(characterStream);
		} catch (JDOMException e) {
			logger.error(e, e);
		} catch (IOException e) {
			logger.error(e, e);
		}
		finally {
			if (characterStream != null) {
				characterStream.close();
			}
		}
		if (doc != null) {
			Element root = doc.getRootElement();
			
			// no namespace right now
			SubjectAreaNode node = new SubjectAreaNode(root, null);
			
			return node;
		}
		
		return null;
	}
	
	private void init(String name, String label, boolean isLeafNode, boolean isValid) {
		this.name = name;
		setLabel(label);
		this.isLeafNode = isLeafNode;
		this.isValid = isValid;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getName() {
		if (name == null) {
			return getLabel();
		}
		return name;
	}
	
	public String getColumnName(){
		String column = name;
		String[] paths = name.split(Pattern.quote(SubjectAreaWS.NODE_SEPARATOR));
		if (paths.length > 0){
			column = paths[paths.length - 1];
		}
		return column;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isValid() {
		return isValid;
	}

	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}

	public boolean isLeafNode() {
		return isLeafNode;
	}

	public void setLeafNode(boolean isLeafNode) {
		this.isLeafNode = isLeafNode;
	}

	public List<SubjectAreaNode> getChildren() {
		return children;
	}

	public void setChildren(List<SubjectAreaNode> children) {
		if (this.getName() != null && this.getName().trim().length() > 0) {
			for (SubjectAreaNode child: children) {
				child.setName(this.getName().concat(SubjectAreaWS.NODE_SEPARATOR).concat(child.getName()));
			}
		}
		this.children = children;
	}
	
	public void addChild(SubjectAreaNode child) {
		if (this.children == null) {
			this.children = new ArrayList<SubjectAreaNode>();
		}
		this.children.add(child);
	}
	
	public void updateValidity() {
		if (this.children != null) {
			boolean valid = false;
			for (SubjectAreaNode child: this.children) {
				child.updateValidity();
				valid = valid || child.isValid();
			}
			this.setValid(valid);
		}
	}
	
	public List<SubjectAreaNode> updateExistingValidity(List<SubjectAreaNode>disabledNodes, 
			                                            Map<String, Column> columnMap, 
			                                            Map<String, Set<Object>> currentDimTables, 
			                                            Map<String, Set<Object>> currentMeasureTables, 
			                                            String prefix) {
		if (this.children != null && !this.isLeafNode()) {
			
			boolean valid = false;
			boolean anyChildValid = false;
			
			List<SubjectAreaNode> disabledChildNodes = new ArrayList<SubjectAreaNode>();
			for (SubjectAreaNode child: this.children) {
				child.updateExistingValidity(disabledChildNodes, columnMap, currentDimTables, currentMeasureTables, prefix + SubjectAreaWS.NODE_SEPARATOR + this.label);	
				valid = valid || child.isValid();
				anyChildValid = anyChildValid || child.isValid();
			}
			isAllChildValid = anyChildValid;
			
			this.setValid(valid);

			//Don't add the children if the parent has been disabled
			if (anyChildValid){
				disabledNodes.addAll(disabledChildNodes);
			}
			else if (!valid) {
				disabledNodes.add(this);
			}
			
		}else{
			//we're the leaf node
			//System.out.println("checking.. " + this.name);
			Column col = columnMap.get(this.name);
			if (col != null){
				boolean valid = Folder.isNavigatable(col, currentDimTables, currentMeasureTables);
				this.setValid(valid);
				if (! valid){
					//System.out.println("**Changed - " + this.label + " is " + this.isValid() + " but current value is " + valid);
					disabledNodes.add(this);
				}
			}
		}
		
		return disabledNodes;
	}

	public String getContentString() {
		StringBuilder buf = new StringBuilder();
		for (SubjectAreaNode node : this.children) {
			buf.append(addChildContent(node));
		}
		return buf.toString();
	}
	@Override
	public void addContent(OMElement parent, OMFactory factory, OMNamespace ns) {
		for (SubjectAreaNode node : this.children) {
			addChildContent(parent, factory, ns, node);
		}
	}
	
	private StringBuilder addChildContent(OMElement parent, OMFactory fac, OMNamespace ns, SubjectAreaNode san) {
		try {
			StringBuilder buf = new StringBuilder();
			if (this.isLeafNode()){
				buf.append("<n><v>");
				buf.append(XmlUtils.encode(san.getColumnName()));
				buf.append("</v><l>");
			} else {
				buf.append("<n><l>");
			}
			buf.append(XmlUtils.encode(san.getLabel()));
			buf.append("</l>");
			//Marker, tells the UI that this is a root level Subject Area node
			if (san.isRootSubjectArea()) {
				buf.append("<isRootSA/>");
			}
			buf.append("<vl>");
			buf.append(san.isValid());
			buf.append("</vl>");
			addChildContent(buf, san);
			buf.append("</n>");
			parent.addChild(fac.createOMText(buf.toString()));
		} catch (UnsupportedEncodingException e) {
			logger.error(e, e);
		}
		return null;
	}
	
	private StringBuilder addChildContent(SubjectAreaNode san) {
		try {
			StringBuilder buf = new StringBuilder();
			if (this.isLeafNode()){
				buf.append("<n><v>");
				buf.append(XmlUtils.encode(san.getColumnName()));
				buf.append("</v><l>");
			} else {
				buf.append("<n><l>");
			}
			buf.append(XmlUtils.encode(san.getLabel()));
			buf.append("</l>");
			//Marker, tells the UI that this is a root level Subject Area node
			if (san.isRootSubjectArea()) {
				buf.append("<isRootSA/>");
			}
			buf.append("<vl>");
			buf.append(san.isValid());
			buf.append("</vl>");
			addChildContent(buf, san);
			buf.append("</n>");
			return buf;
		} catch (UnsupportedEncodingException e) {
			logger.error(e, e);
		}
		return null;
	}
	
	private void addChildContent(StringBuilder buf, SubjectAreaNode san) throws UnsupportedEncodingException {
		List<SubjectAreaNode> children = san.getChildren();
		if (children != null) {
			buf.append("<c>");
			for (SubjectAreaNode node : children) {
				if(node.isLeafNode()){
					buf.append("<n><v>");
					buf.append(XmlUtils.encode(node.getColumnName()));
					buf.append("</v><l>");
				}else{
					buf.append("<n><l>");
				}
				buf.append(XmlUtils.encode(node.getLabel()));
				buf.append("</l>");
				//Marker, tells the UI that this is a root level Subject Area node
				if (node.isRootSubjectArea()){
					buf.append("<isRootSA/>");
				}
				buf.append("<vl>");
				buf.append(node.isValid());
				buf.append("</vl>");
				addChildContent(buf, node);
				buf.append("</n>");
			}
			buf.append("</c>");
		}
	}
}
