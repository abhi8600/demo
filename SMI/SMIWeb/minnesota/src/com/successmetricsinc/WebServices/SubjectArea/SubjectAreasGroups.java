package com.successmetricsinc.WebServices.SubjectArea;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;
import org.jdom.Element;

import com.successmetricsinc.WebServices.IWebServiceResult;
import com.successmetricsinc.util.FileUtils;
import com.successmetricsinc.util.XmlUtils;

/**
 * The two purposes of this class are:
 * <ul>
 * <li>to provide a map of each subject area's list of groups</li>
 * <li>to be able to generate the OMElement results for Flex representing all the subject areas' groups in the form:
 * <p/>
 * <code>
 * &lt;SubjectAreas><br/>
 * &nbsp;&nbsp;&lt;SubjectArea>Default Subject Area<br/>
 * &nbsp;&nbsp;&lt;Groups><br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&lt;Group>Administrator&lt;/Group><br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&lt;Group>OWNER$&lt;/Group><br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&lt;Group>USER$&lt;/Group><br/>
 * &nbsp;&nbsp;&lt;/Groups><br/>
 * &nbsp;&nbsp;&lt;/SubjectArea><br/>
 * &nbsp;&nbsp;&lt;SubjectArea>Custom Subject Area #1<br/>
 * &nbsp;&nbsp;&lt;Groups><br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&lt;Group>Beginners&lt;/Group><br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&lt;Group>Amateurs&lt;/Group><br/>
 * &nbsp;&nbsp;&lt;/Groups><br/>
 * &nbsp;&nbsp;&lt;/SubjectArea><br/>
 * ...<br/>
 * &lt;SubjectAreas><br/>
 * </code>
 * </li>
 * </ul>
 * @author pconnolly
 */
public class SubjectAreasGroups implements IWebServiceResult {

	// Element names for the JDOM input
	public static final String NAME = "Name";
	
	// Element names for the OMElement output
	private static final String SUBJECT_AREAS = "SubjectAreas";
	private static final String SUBJECT_AREA = "SubjectArea";
	public static final String GROUPS = "Groups";
	public static final String GROUP = "Group";
	
	private static final Logger logger = Logger.getLogger(SubjectAreasGroups.class);
	
	private String[] metadataFilepaths;
	
	public SubjectAreasGroups(final String[] metadataFilepaths) {
		this.metadataFilepaths= metadataFilepaths;
	}
	
	public Map<String, List<String>> getSubjectAreasGroupsMap() {
		final Map<String, List<String>> sagsMap = new HashMap<String, List<String>>();
		
		for (String filepath : metadataFilepaths) {
			// Read the metadata file contents in as JDOM Element
			final Element metadataElement = FileUtils.readFileToDocument(new File(filepath));
			final String subjectAreaName = metadataElement.getChildText(NAME);
			Element inGroupsElement = metadataElement.getChild(GROUPS);
			List<String> groups = new ArrayList<String>();
			for (Object groupObject : inGroupsElement.getChildren(GROUP)) {
				Element groupElement = (Element) groupObject;
				groups.add(groupElement.getText());
			}
			sagsMap.put(subjectAreaName, groups);
		}
		return sagsMap;
	}
	
	@Override
	public void addContent(OMElement parent, OMFactory factory, OMNamespace ns) {
		// <SubjectAreas>
		OMElement subjectAreasElement = factory.createOMElement(SUBJECT_AREAS, ns);
		for (String filepath : metadataFilepaths) {
			// Read the metadata file contents in as JDOM Element
			final Element metadataElement = FileUtils.readFileToDocument(new File(filepath));
			final String subjectAreaName = metadataElement.getChildText(NAME);
			// <SubjectArea>
			OMElement subjectAreaElement = factory.createOMElement(SUBJECT_AREA, ns);
			subjectAreaElement.addChild(factory.createOMText(subjectAreaElement, subjectAreaName));
			// <Groups>
			OMElement groupsElement = factory.createOMElement(GROUPS, ns);
			Element inGroupsElement = metadataElement.getChild(GROUPS);
			for (Object groupObject : inGroupsElement.getChildren(GROUP)) {
				Element groupElement = (Element) groupObject;
				// <Group>
				XmlUtils.addContent(factory, groupsElement, GROUP, groupElement.getText(), ns);
			}
			subjectAreaElement.addChild(groupsElement);
			subjectAreasElement.addChild(subjectAreaElement);
		}
		logger.trace("'subjectAreasElement': " + subjectAreasElement.toString());
		parent.addChild(subjectAreasElement);
	}
}
