/**
 * $Id: SubjectAreaWS.java,v 1.77 2012-10-25 01:31:02 BIRST\gsingh Exp $
 *
 * Copyright (c) 2007-2010 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.successmetricsinc.WebServices.SubjectArea;

import static com.successmetricsinc.SubjectArea.MeasureFolderName;
import static com.successmetricsinc.SubjectArea.TimeMeasureFolderName;
import static com.successmetricsinc.WebServices.SubjectArea.ExtendedSubjectAreaNode.TYPE_FOLDER;
import static com.successmetricsinc.WebServices.SubjectArea.SubjectAreaNode.LABEL;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;

import javax.jws.WebService;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.successmetricsinc.Column;
import com.successmetricsinc.Folder;
import com.successmetricsinc.Group;
import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.SubjectArea;
import com.successmetricsinc.Folder.FolderType;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.WebServices.BirstWebService;
import com.successmetricsinc.WebServices.BirstWebServiceResult;
import com.successmetricsinc.WebServices.OMTextWebServiceResult;
import com.successmetricsinc.WebServices.XMLWebServiceResult;
import com.successmetricsinc.WebServices.SubjectArea.ExtendedSubjectAreaNode.NODE_TYPE;
import com.successmetricsinc.adhoc.AdhocColumn;
import com.successmetricsinc.adhoc.AdhocReport;
import com.successmetricsinc.adhoc.AdhocColumn.ColumnType;
import com.successmetricsinc.query.AbstractQueryString;
import com.successmetricsinc.query.DatabaseConnection;
import com.successmetricsinc.query.DatabaseConnectionResultSet;
import com.successmetricsinc.query.DatabaseConnectionStatement;
import com.successmetricsinc.query.DimensionColumn;
import com.successmetricsinc.query.DimensionColumnNav;
import com.successmetricsinc.query.DimensionExpressionColumn;
import com.successmetricsinc.query.DimensionTable;
import com.successmetricsinc.query.DisplayExpression;
import com.successmetricsinc.query.DisplayFilter;
import com.successmetricsinc.query.Hierarchy;
import com.successmetricsinc.query.LogicalQueryString;
import com.successmetricsinc.query.MeasureColumn;
import com.successmetricsinc.query.MeasureColumnNav;
import com.successmetricsinc.query.MeasureExpressionColumn;
import com.successmetricsinc.query.MeasureTable;
import com.successmetricsinc.query.Query;
import com.successmetricsinc.query.QueryFilter;
import com.successmetricsinc.query.QueryString;
import com.successmetricsinc.query.olap.OlapLogicalQueryString;
import com.successmetricsinc.transformation.object.SavedExpressions;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.UserNotLoggedInException;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XmlUtils;
import com.successmetricsinc.warehouse.TimeDefinition;
import com.successmetricsinc.warehouse.TimePeriod;

/**
 * The class that implements the subject area web service.  This service has one method that gets the subject area available
 * to the user including a validity flag
 * 
 * @author agarrison
 */
@WebService( serviceName="SubjectArea")
public class SubjectAreaWS extends BirstWebService {
	
	private static final String DEFAULT_SUBJECT_AREA_NAME = "Default Subject Area";
	public static final String SUBJECT_AREAS_NAME = "Subject Areas";
	private static Logger logger = Logger.getLogger(SubjectAreaWS.class);

	// don't change without changing SubjectAreaTreeDataDescriptor.as
	public static final String NODE_SEPARATOR = SubjectArea.NODE_SEPARATOR;
	public static final String CACHE_SA_LIMITED_NODE_KEY = "SA_Limited_Node";
	public static final String CACHE_NODE_KEY = "SA_Node";
	public static final String CACHE_DASHBOARD_KEY = "Dashboard_SA_Node";
	public static final String CACHE_DEFAULT_SA_NODE_KEY = "SA_Default_Node";
	public static final String CACHE_SA_FILTERED_NODE_KEY = "SA_Filtered_Node";
	private static final String DEFAULT_DESCRIPTION 
		= "This is the default subject area generated from the database schema for the current space.";
	private Map<String, Set<Object>> emptyTables = new HashMap<String, Set<Object>>();
	private static final String STATUS = "Status";
	private static final String DEFAULT_SUBJECT_AREA_NAME_EXCEPTION = "Can not use default subject area name";

	/* --------------------------------------------------------------------------------------------
	 * --------------------------------------------------------------------------------------------
	 * ---                                                                                      ---
	 * ---  Regular Subject Area methods - used for Adhoc & Dashboards                          ---
	 * ---                                                                                      ---
	 * --------------------------------------------------------------------------------------------
	 * -------------------------------------------------------------------------------------------- */
	/**
	 * Gets the subject area used in Adhoc (Designer) and Dashboards.
	 * @return an XML string containing a BirstWebServiceResult object.  
	 * The result field of that object contains the tree representation of the subjects in XML format.
	 */
	public OMElement getSubjectArea() {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			ExtendedSubjectAreaNode node = null;
			Object obj = getSessionObject(CACHE_NODE_KEY);
			if( obj != null && obj instanceof ExtendedSubjectAreaNode)
			{
				node = (ExtendedSubjectAreaNode) obj;
			}
			
			if (node == null) {
				// This retrieves all subject areas
				node = generateExtendedSubjectArea();
				// If there's only one subject area (default) use 'node' unchanged
				if (node.getExtendedChildren().size() == 1) {
					// But promote it to root before returning
					node = node.getExtendedChildren().get(0);
				} else {
					// For 2 or more, process each subject area
					ArrayList<ExtendedSubjectAreaNode> newExtendedChildren = new ArrayList<ExtendedSubjectAreaNode>();
					Map<String, List<String>> sagMap = getSubjectAreasGroupsMap().getSubjectAreasGroupsMap();
					for (ExtendedSubjectAreaNode child : node.getExtendedChildren()) {
						// Filter out subject areas not belonging to this user's groups
						if (!child.isImportedSubjectArea() && !groupCanViewSubjectArea(sagMap.get(child.getLabel()))) {
							// Skip to next subject area if not visible
							continue;
						}
						newExtendedChildren.add(child);
					}
					node.setExtendedChildren(newExtendedChildren);
					if (newExtendedChildren.size() == 1) {
						// Since only one, promote to root before returning
						node = newExtendedChildren.get(0);
					}
				}
				setSessionObject(CACHE_NODE_KEY, node);
			}
			else {
				// clear out prefix in node - prefix is set when updating
				node.clearPrefix();
			}
			SMIWebUserBean userBean = getUserBean();
			// Get default subject area(s) using Repository
			Repository repository = userBean.getRepository();
			result.setResult(new OMTextWebServiceResult(node.getContentString(repository.getDimensionalStructure())));
            if (logger.isTraceEnabled())
            	logger.trace("'getSubjectArea()' node: " + result);
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement getSubjectAreaByName(String saName) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			if (saName == null || saName.trim().length() == 0) {
				result.setErrorCode(BaseException.ERROR_OTHER);
				throw new Exception(ExtendedSubjectAreaNode.SA_NOT_FOUND_EX);
			}
			
			ExtendedSubjectAreaNode node = null;			
			if (node == null) {
				// This retrieves all subject areas.
				ExtendedSubjectAreaNode esaNode = generateExtendedSubjectArea(); 
				// If there's only one subject area use 'node' unchanged.
				if (esaNode.getExtendedChildren().size() == 1) {
					// But promote it to root before returning.
					String esaNodeLabel = esaNode.getExtendedChildren().get(0).getLabel();
					if (!isDefaultSubjectArea(esaNodeLabel)) {
						if (esaNodeLabel.equalsIgnoreCase(saName)) {
							node = esaNode;
						}
					} else {
						result.setErrorCode(BaseException.ERROR_OTHER);
						throw new Exception(ExtendedSubjectAreaNode.SA_NOT_FOUND_EX);
					}
				} else {
					// For 2 or more, process each subject area.
					node = esaNode;
					ArrayList<ExtendedSubjectAreaNode> newExtendedChildren = new ArrayList<ExtendedSubjectAreaNode>();
					for (ExtendedSubjectAreaNode child : node.getExtendedChildren()) {
						String childLabel = child.getLabel();
						if (!isDefaultSubjectArea(childLabel)) {
							if (childLabel.equalsIgnoreCase(saName)) {
								newExtendedChildren.add(child);
							}
						}
					}
					node.setExtendedChildren(newExtendedChildren);
				}
			}

			// Get default subject area(s) using Repository.
			if (node != null) {
				SMIWebUserBean userBean = getUserBean();
				Repository repository = userBean.getRepository();
				node.setIsOptimized(false);
				String txtWsResult = node.getContentString(repository.getDimensionalStructure(), saName);
				OMTextWebServiceResult wsResult = new OMTextWebServiceResult(txtWsResult); 
				result.setResult(wsResult);
				if (txtWsResult.contains(ExtendedSubjectAreaNode.SA_NOT_FOUND_EX)) {
					result.setErrorCode(BaseException.ERROR_OTHER);
				}
				if (logger.isTraceEnabled()) {
	            	logger.trace("'getSubjectAreaByName()' node: " + result);
	            }
			}
		} catch (Exception ex) {
			result.setException(ex);
			result.setErrorCode(BaseException.ERROR_OTHER);
			result.setErrorMessage("Exception getting " + saName + " custom subject area: " + ex.getMessage());
		}
		return result.toOMElement();
	}
	
	public OMElement addRootLevelMembers(String dimHierarchy, int start, int length)
	{
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean ub = getUserBean();
			Repository r = ub.getRepository();
			Hierarchy hr = r.findDimensionHierarchy(dimHierarchy);
			if (hr == null)
			{
				throw new Exception("Hierarchy " + dimHierarchy + " does not exist");
			}
			DimensionTable dt = r.findDimensionTableAtLevel(hr.DimensionName, hr.DimensionKeyLevel.Name);
			if (dt == null)
			{
				throw new Exception("No DimensionTable exists at " + hr.DimensionName + "." + hr.DimensionKeyLevel.Name + " level");
			}
			ub.getRepSession().setRepository(r);
			DatabaseConnection dc = dt.TableSource.connection;
			if (dc != null && DatabaseConnection.isDBTypeXMLA(dc.DBType))
			{
				if (!dc.getDBTypeNameForXMLAConnection().equals(com.birst.dataconductor.DatabaseConnection.MICROSOFT_ANALYSIS_SERVICES))
				{
					throw new Exception("This operation is only supported for Microsoft Analysis Services (XMLA) connections");
				}
				DatabaseConnectionStatement statement = null;
				if (dc.isRealTimeConnection())
				{
					statement = new DatabaseConnectionStatement(r, dc);
				}
				else
				{
					if (dc.ConnectionPool.isDynamicConnnection() && ub.getRepSession() != null)
					{
						statement = new DatabaseConnectionStatement(dc, dc.ConnectionPool.getDynamicConnection(ub.getRepSession()).createStatement(), r);
					}
					else
					{
						statement = new DatabaseConnectionStatement(dc, dc.ConnectionPool.getConnection().createStatement(), r);
					}
				}
				Query q = new Query(r);
				q.setSession(ub.getRepSession());
				String dimName = dimHierarchy.substring(0, dimHierarchy.indexOf("-"));
				String hName = dimHierarchy.substring(dimHierarchy.indexOf("-") + 1);
				DatabaseConnectionResultSet dcrs = statement.executeQuery("([Measures].[M-MemberName]|Varchar|0),([Measures].[M-ChildCount]|Integer|1),([Measures].[M-IsLeaf]|Integer|2),([Measures].[M-MemberUniqueName]|Varchar|3)~~" +
						"WITH " + 
						"MEMBER [Measures].[M-MemberName] AS '[" + dimName + "].[" + hName + "].CurrentMember.Name' " +
						"MEMBER [Measures].[M-ChildCount] AS 'DESCENDANTS([" + dimName + "].[" + hName + "].CurrentMember,[" + dimName + "].[" + hName + "].CurrentMember.Level.Ordinal,SELF).Count' " +
						"MEMBER [Measures].[M-IsLeaf] AS 'IIF(IsLeaf([" + dimName + "].[" + hName + "].CurrentMember),1,0)' " +
						"MEMBER [Measures].[M-MemberUniqueName] AS '[" + dimName + "].[" + hName + "].CurrentMember.UniqueName' " +
						"SELECT  {[Measures].[M-MemberName],[Measures].[M-ChildCount],[Measures].[M-IsLeaf],[Measures].[M-MemberUniqueName]} ON 0, " + 
						"SUBSET({[" + dimName + "].[" + hName + "].Levels(0).AllMembers}, " + start + ", " + length + ") ON 1 " +
						"FROM [" + dt.TableSource.Tables[0].PhysicalName + "]", q);
				if (dcrs.getNumRows() > 0)
				{
					dcrs.next();
					List<ExtendedSubjectAreaNode> childNodes = new ArrayList<ExtendedSubjectAreaNode>();
					for (int i=0; i<dcrs.getNumRows(); i++,dcrs.next())
					{
						ExtendedSubjectAreaNode cNode = new ExtendedSubjectAreaNode(dcrs.getString(4), dcrs.getString(1), dcrs.getLong(3) == 1, true, SubjectArea.AttributeFolderName, 0);
						cNode.setReportElementLabel(hName);
						cNode.setChildCount(dcrs.getLong(2));
						cNode.setOlapDimension(dimName);
						cNode.setOlapHierarchy(hName);
						cNode.setNodeType(NODE_TYPE.OLAP_MEMBER);
						childNodes.add(cNode);
					}
					if (childNodes.size() > 0)
					{
						StringBuilder sb = new StringBuilder();
						sb.append("");
						ExtendedSubjectAreaNode node = new ExtendedSubjectAreaNode("", "");
						node.setExtendedChildren(childNodes);
						node.addChildContent(sb, node, r.getDimensionalStructure());
						result.setResult(new OMTextWebServiceResult(sb.toString()));
					}
				}
			}
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement addChildMembers(String memberName, String olapDimension, String olapHierarchy, int start, int length) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean ub = getUserBean();
			Repository r = ub.getRepository();
			String dName = olapDimension;
			String hName = olapHierarchy;
			//String mName = memPhysicalNameParts[2].substring(1, memPhysicalNameParts[2].length() - 1);
			String dimHierarchy = dName + "-" + hName;
			Hierarchy hr = r.findDimensionHierarchy(dimHierarchy);
			if (hr == null)
			{
				throw new Exception("Hierarchy " + dimHierarchy + " does not exist");
			}
			DimensionTable dt = r.findDimensionTableAtLevel(hr.DimensionName, hr.DimensionKeyLevel.Name);
			if (dt == null)
			{
				throw new Exception("No DimensionTable exists at " + hr.DimensionName + "." + hr.DimensionKeyLevel.Name + " level");
			}
			ub.getRepSession().setRepository(r);
			DatabaseConnection dc = dt.TableSource.connection;
			if (dc != null && DatabaseConnection.isDBTypeXMLA(dc.DBType))
			{
				if (!dc.getDBTypeNameForXMLAConnection().equals(com.birst.dataconductor.DatabaseConnection.MICROSOFT_ANALYSIS_SERVICES))
				{
					throw new Exception("This operation is only supported for Microsoft Analysis Services (XMLA) connections");
				}
				DatabaseConnectionStatement statement = null;
				if (dc.isRealTimeConnection())
				{
					statement = new DatabaseConnectionStatement(r, dc);
				}
				else
				{
					if (dc.ConnectionPool.isDynamicConnnection() && ub.getRepSession() != null)
					{
						statement = new DatabaseConnectionStatement(dc, dc.ConnectionPool.getDynamicConnection(ub.getRepSession()).createStatement(), r);
					}
					else
					{
						statement = new DatabaseConnectionStatement(dc, dc.ConnectionPool.getConnection().createStatement(), r);
					}
				}
				Query q = new Query(r);
				q.setSession(ub.getRepSession());
				String mdxQuery = "([Measures].[M-MemberName]|Varchar|0),([Measures].[M-ChildCount]|Integer|1),([Measures].[M-IsLeaf]|Integer|2),([Measures].[M-MemberUniqueName]|Varchar|3)~~" +
						"WITH " +
						"MEMBER [Measures].[M-MemberName] AS '[" + dName + "].[" + hName + "].CurrentMember.Name' " +
						"MEMBER [Measures].[M-ChildCount] AS 'DESCENDANTS([" + dName + "].[" + hName + "].CurrentMember,[" + dName + "].[" + hName + "].CurrentMember.Level.Ordinal,SELF).Count' " +
						"MEMBER [Measures].[M-IsLeaf] AS 'IIF(IsLeaf([" + dName + "].[" + hName + "].CurrentMember),1,0)' " +
						"MEMBER [Measures].[M-MemberUniqueName] AS '[" + dName + "].[" + hName + "].CurrentMember.UniqueName' " +
						"SELECT {[Measures].[M-MemberName],[Measures].[M-ChildCount],[Measures].[M-IsLeaf],[Measures].[M-MemberUniqueName]} ON 0, " +
						"SUBSET({" + memberName + ".Children}, " + start + ", " + length + 1 + ") on 1 FROM [" + dt.TableSource.Tables[0].PhysicalName + "]";
				DatabaseConnectionResultSet dcrs = statement.executeQuery(mdxQuery, q);
				int size = dcrs.getNumRows();
				if (size > 0)
				{
					dcrs.next();
					boolean addMore = false;
					if (size > length) 
						addMore = true;
					
					size = Math.min(size, length);
					List<ExtendedSubjectAreaNode> childNodes = new ArrayList<ExtendedSubjectAreaNode>();
					for (int i=0; i<size; i++,dcrs.next())
					{
						ExtendedSubjectAreaNode cNode = new ExtendedSubjectAreaNode(dcrs.getString(4), dcrs.getString(1), dcrs.getLong(3) == 1, true, SubjectArea.AttributeFolderName, 0);
						cNode.setChildCount(dcrs.getLong(2));
						cNode.setReportElementLabel(hName);
						cNode.setOlapDimension(dName);
						cNode.setOlapHierarchy(hName);
						cNode.setNodeType(NODE_TYPE.OLAP_MEMBER);
						childNodes.add(cNode);
					}
					if (addMore) {
						// add more... node to get the next set
						ExtendedSubjectAreaNode cNode = new ExtendedSubjectAreaNode(memberName, "more...", "Get more children", ExtendedSubjectAreaNode.TYPE_MORE, 0);
						cNode.setLeafNode(true);
						cNode.setStart(start + length);
						cNode.setOlapDimension(dName);
						cNode.setOlapHierarchy(hName);
						childNodes.add(cNode);
					}
					if (childNodes.size() > 0)
					{
						StringBuilder sb = new StringBuilder();
						sb.append("");
						ExtendedSubjectAreaNode node = new ExtendedSubjectAreaNode("", "");
						node.setExtendedChildren(childNodes);
						node.addChildContent(sb, node, r.getDimensionalStructure());
						result.setResult(new OMTextWebServiceResult(sb.toString()));
					}
				}
			}
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	public OMElement getVisualizerUpdatedSubjectArea(String query) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}

			SubjectAreaNode node = (SubjectAreaNode)getSessionObject(CACHE_NODE_KEY);
			SMIWebUserBean ub = getUserBean();
			Session session = ub.getRepSession();
			Repository r = ub.getRepository();
			query = Util.convertMulValueVariablesToNewLanguageSyntax(r, query);
			query = QueryString.preProcessQueryString(query, r, session);
			AbstractQueryString aqs = null;
			Matcher matcher = AbstractQueryString.olapQueryPattern.matcher(query);
			if (matcher.find())
			{
				aqs = new OlapLogicalQueryString(r, null, query);
			}
			else
			{
				aqs = new LogicalQueryString(r, session, query);
			}
			Query q = aqs.getQuery();
			Map<String, Set<Object>> currentDimTables = new HashMap<String, Set<Object>>();
			Map<String, Set<Object>> currentMeasureTables = new HashMap<String, Set<Object>>();
			
			for (DimensionColumnNav dcn : q.dimensionColumns) {
				Set<Object> l = getColumnTables(dcn.getDimensionName(), dcn.getColumnName(), r);
				if (l != null) 
				{
					currentDimTables.put(dcn.getDimensionName()+'.'+dcn.getColumnName(), l);
				}
			}
			for (MeasureColumnNav mn : q.measureColumns)
			{
				Set<Object> l = getColumnTables(null, mn.getMeasureName(), r);
				if (l != null) 
				{
					currentMeasureTables.put(mn.getMeasureName(), l);
				}
			}
			for (MeasureColumnNav mn : q.derivedMeasureColumns)
			{
				Set<Object> l = getColumnTables(null, mn.getMeasureName(), r);
				if (l != null) 
				{
					currentMeasureTables.put(mn.getMeasureName(), l);
				}
			}
			if (node != null) {
				SubjectArea[] subjectAreas = r.getSubjectAreas();
				
				if (subjectAreas != null) {
					SubjectAreaModifiedNodes updatedNodes = getModifiedNodes(currentDimTables, currentMeasureTables, subjectAreas, node, r);
					result.setResult(updatedNodes);
				}
			}
			else {
				node = generateSubjectArea(currentDimTables, currentMeasureTables, r, ub);
				//result.setResult(node);
				setSessionObject(CACHE_NODE_KEY, node);
			}
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement getUpdatedSubjectArea(String xmlData){
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			AdhocReport report = AdhocReport.convert(xmlData, null, null);
			SubjectAreaNode node = (SubjectAreaNode)getSessionObject(CACHE_NODE_KEY);
			if (report != null) {
				report = report.clone();
			}
			if (node != null){
				Repository repository = getUserBean().getRepository();
				SubjectArea[] subjectAreas = repository.getSubjectAreas();
				
				if (subjectAreas != null) {
					Map<String, Set<Object>> currentDimTables = SubjectAreaWS.getCurrentDimTables(report, repository);
					Map<String, Set<Object>> currentMeasureTables = getCurrentMeasureTables(report, repository);
					
					SubjectAreaModifiedNodes updatedNodes = getModifiedNodes(currentDimTables, currentMeasureTables, subjectAreas, node, repository);
					result.setResult(updatedNodes);
				}
			} else {
				node = generateSubjectArea(report);
				//result.setResult(node);
				setSessionObject(CACHE_NODE_KEY, node);
			}
            if (logger.isTraceEnabled())
            	logger.trace("'getUpdatedSubjectArea()' node: " + result);
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	private SubjectAreaModifiedNodes getModifiedNodes(Map<String, Set<Object>> currentDimTables, Map<String, Set<Object>> currentMeasureTables, 
			SubjectArea[] subjectAreas, SubjectAreaNode node, Repository repository) {
		SubjectAreaModifiedNodes updatedNodes = new SubjectAreaModifiedNodes();
		// Only Default Subject Area? Then always show it
		if (subjectAreas.length == 1) {
			SubjectArea sa = subjectAreas[0];
			HashMap<String, Column> flatFolderColumns = new HashMap<String, Column>();
			for (Folder folder : sa.getFolders()) {
				this.populateFlatColumnMap(flatFolderColumns, folder);
			}
			List<SubjectAreaNode> modifiedNodes = new ArrayList<SubjectAreaNode>();
			node.updateExistingValidity(modifiedNodes, flatFolderColumns, currentDimTables, currentMeasureTables, "");
			updatedNodes.addModifiedSubjectAreaNodes("", modifiedNodes);
		} else {
			for (SubjectArea sa : repository.getSubjectAreas()) {
				if (groupCanViewSubjectArea(Arrays.asList(sa.getGroups()))) {
					HashMap<String, Column> flatFolderColumns = new HashMap<String, Column>();
					for (Folder folder : sa.getFolders()) {
						populateFlatColumnMap(flatFolderColumns, folder);
					}
					List<SubjectAreaNode> modifiedNodes = new ArrayList<SubjectAreaNode>();
					node.updateExistingValidity(modifiedNodes, flatFolderColumns, currentDimTables, currentMeasureTables, "");
					updatedNodes.addModifiedSubjectAreaNodes(sa.getName(), modifiedNodes);
				}
			}
		}
		return updatedNodes;
	}
	
	/**
	 * Only returns the default subject area with Attributes.
	 * @return
	 */
	public OMElement getSubjectAreaLimited(){
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			
			SMIWebUserBean userBean = getUserBean();
			// Get default subject area(s) using Repository
			List<Group> groups = userBean.getGroupList(false);
			Repository repository = userBean.getRepository();
			ExtendedSubjectAreaNode node = (ExtendedSubjectAreaNode) getSessionObject(CACHE_SA_LIMITED_NODE_KEY);
			if (node == null) {
				ExtendedSubjectAreaNode defaultNode = null;
				SubjectArea[] subjectAreas = repository.getSubjectAreas();
				if (subjectAreas != null && subjectAreas.length > 0) {
					// First subject area is always the default subject area
					defaultNode = new ExtendedSubjectAreaNode("", DEFAULT_SUBJECT_AREA_NAME, DEFAULT_DESCRIPTION, TYPE_FOLDER, 0);
					SubjectArea defaultSubjectArea = subjectAreas[0];
					defaultNode.markAsRootSubjectArea();
					for (Folder folder : defaultSubjectArea.getFolders()) {
						if(folder.getName().equals(SubjectArea.AttributeFolderName))
						{
							// Process children 
							ExtendedSubjectAreaNode child = generateExtendedFolderTree("", 0, folder, folder.getName(), groups, emptyTables, emptyTables);
							if (child != null) {
								defaultNode.addExtendedChild(child);
							}
						}
					}
				}
				
				node = defaultNode;
				// get custom subject areas
				// Get any custom subject areas using the SubjectAreaManager
				final SubjectAreaManager subjectAreaManager = new SubjectAreaManager(repository);
				List<Element> customSubjectAreasNodes = subjectAreaManager.getCustomSubjectAreasNodes();
				// Process each custom subject area. Convert direct from Element, don't use SubjectArea & Folder
				if (customSubjectAreasNodes != null && customSubjectAreasNodes.size() > 0) {
					node = new ExtendedSubjectAreaNode("", SUBJECT_AREAS_NAME);
					node.addExtendedChild(defaultNode);
					// add the default subject area
					// Handle multiple custom subject areas. 'j' increment keeps '<n id="X.Y"...' X prefix unique across all subject areas
					for (int i = 0, j = subjectAreas.length; i < customSubjectAreasNodes.size(); i++, j++) {
						final Element csaElement = customSubjectAreasNodes.get(i);
						ExtendedSubjectAreaNode customNode = new ExtendedSubjectAreaNode(csaElement, j);
						customNode.markAsRootSubjectArea();
						node.addExtendedChild(customNode);
					}
				}
				
				setSessionObject(CACHE_SA_LIMITED_NODE_KEY, node);
			}
			else {
				node.clearPrefix();
			}
			
			result.setResult(new OMTextWebServiceResult(node.getContentString(repository.getDimensionalStructure())));
            if (logger.isTraceEnabled())
            	logger.trace("'getSubjectAreaLimited()' node: " + result);
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement getDefaultSubjectArea() {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}

			SMIWebUserBean userBean = getUserBean();
			List<Group> groups = userBean.getGroupList(false);
			Repository repository = userBean.getRepository();
			ExtendedSubjectAreaNode node = (ExtendedSubjectAreaNode) getSessionObject(CACHE_DEFAULT_SA_NODE_KEY);
			if (node == null) {
				ExtendedSubjectAreaNode defaultNode = null;
				SubjectArea[] subjectAreas = repository.getSubjectAreas();
				if (subjectAreas != null && subjectAreas.length > 0) {
					// First subject area is always the default subject area.
					defaultNode = new ExtendedSubjectAreaNode("", DEFAULT_SUBJECT_AREA_NAME, DEFAULT_DESCRIPTION, TYPE_FOLDER, 0);
					SubjectArea defaultSubjectArea = subjectAreas[0];
					defaultNode.markAsRootSubjectArea();
					for (Folder folder : defaultSubjectArea.getFolders()) {
						// Process children.
						ExtendedSubjectAreaNode child = generateExtendedFolderTree(ExtendedSubjectAreaNode.EMPTY_STRING, 0, folder, folder.getName(), groups, emptyTables, emptyTables);
						if (child != null) {
							defaultNode.addExtendedChild(child);
						}
					}
				}
				node = defaultNode;
				node = new ExtendedSubjectAreaNode(ExtendedSubjectAreaNode.EMPTY_STRING, SUBJECT_AREAS_NAME);
				node.addExtendedChild(defaultNode);				
				setSessionObject(CACHE_DEFAULT_SA_NODE_KEY, node);
			}
			else {
				node.clearPrefix();
			}

			// Get default subject area.
			result.setResult(new OMTextWebServiceResult(node.getContentString(repository.getDimensionalStructure())));
            if (logger.isTraceEnabled()) {
            	logger.trace("'getDefaultSubjectArea()' node: " + result);
            }
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	/* --------------------------------------------------------------------------------------------
	 * --------------------------------------------------------------------------------------------
	 * ---                                                                                      ---
	 * ---  Extended Subject Area methods - used for Custom Subject Areas Administration        ---
	 * ---                                                                                      ---
	 * --------------------------------------------------------------------------------------------
	 * -------------------------------------------------------------------------------------------- */
	
	/**
	 * Gets the subject area similarly to {@link #getSubjectArea(String)} except that it has been 
	 * enhanced to include two new attributes in the <n> element:
	 * <dl>
	 * <dt>id</dt><dd>to uniquely identify the element</dd>
	 * <dt>type</dt><dd>indicates the type of element:
	 * <ul>
	 * <li>"f": folder</li>
	 * <li>"a": attribute</li>
	 * <li>"m": measure</li>
	 * </ul>
	 * </dd>
	 * </dl>
	 * These new attributes were added in order to facilitate the creation and editing of 
	 * custom subject areas.
	 * <p/>
	 * The 'id' will be of the format 9.999 where the integer portion is '1' for the default
	 * subject area and 2... for all custom subject areas.  Each element within that subject
	 * area is then numbered sequentially.  So, for example, the default subject area would
	 * look like:<br/>
	 * <code>
	 * &lt;n id="1.0" type="f"><br/>
	 * &nbsp;&nbsp;&lt;l>Default Subject Area&lt;/l><br/>
	 * &nbsp;&nbsp;&lt;vl>true&lt;/vl><br/>
	 * &nbsp;&nbsp;&lt;c><br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;&lt;n id="1.1" type="f"><br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;l>Attributes&lt;/l><br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;vl>true&lt;/vl><br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;c><br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;n id="1.2" type="f"><br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;l>Time&lt;/l><br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;vl>true&lt;/vl><br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;c><br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;n id="1.3" type="a"><br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;v>|~%#|~%#Attributes|~%#Time|~%#Year&lt;/v><br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;l>Year&lt;/l><br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;vl>true&lt;/vl><br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/n><br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;n id="1.4" type="a"><br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;v>|~%#|~%#Attributes|~%#Time|~%#Year Seq Number&lt;/v><br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;l>Year Seq Number&lt;/l><br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;vl>true&lt;/vl><br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/n><br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;n id="1.5" type="a"><br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;...<br/>
	 * </code>
	 * Note that each 'id' is unique within the structure.  An additional custom subject area would have the
	 * sequence: 2.0, 2.1, 2.2,...  Each 'n' element also has the 'type' attribute to denote the type of 
	 * element it is.
	 * @return an OMElement generated from the {@link BirstWebServiceResult} object.  
	 * The OMElement contains the tree representation of the subjects in XML format.
	 */
	public OMElement getExtendedSubjectArea() {
		if (logger.isTraceEnabled())
			logger.trace("'getExtendedSubjectArea()' for "  + getUserBean().getRepository().getApplicationName());
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			// Unlike getSubjectArea(), DON'T use cache
			ExtendedSubjectAreaNode node = generateExtendedSubjectArea();
			// Attach subject area to result and return
			result.setResult(node);
            if (logger.isTraceEnabled())
            	logger.trace("'getExtendedSubjectArea()' node: " + result);
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	private String getCSAFileName(String customSubjectAreaName){
		return SubjectAreaManager.SUBJECT_AREA + customSubjectAreaName + ".xml";
	}
	
	private String getCSAMetadataFileName(String customSubjectAreaName){
		return SubjectAreaManager.SUBJECT_AREA_METADATA + customSubjectAreaName + ".xml";
	}
	
	public OMElement createCustomSubjectArea(String name, String[] groupNames, String description)
	{
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			
			if(isDefaultSubjectArea(name)){
				result.setErrorCode(BaseException.ERROR_OTHER);
				result.setErrorMessage("Cannot create a Default Subject Area");
			}
			else{
				final SubjectAreaManager subjectAreaManager = new SubjectAreaManager(SMIWebUserBean.getUserBean().getRepository());
				List<File> currentCSAFiles = subjectAreaManager.getCustomSubjectAreaFiles();
				boolean exists = false;			
				final String subjectAreaFilename = getCSAFileName(name);
				int id = 1;
				if(currentCSAFiles != null && currentCSAFiles.size() > 0){
					id = currentCSAFiles.size() + 1; // id stored in CSA fileName
					for (File csaFile : currentCSAFiles) {
						String fileName = csaFile.getName();
						if(fileName != null && fileName.equalsIgnoreCase(subjectAreaFilename)){
							exists = true;
							break;
						}
					}
				}

				if(!exists){
					String customSubjectAreaXML = getStringFiedXML(name, description, id + ".0", true);
					saveCustomSubjectAreaProps(subjectAreaManager, name, customSubjectAreaXML);
					saveCustomSubjectAreaMetadata(subjectAreaManager, name, groupNames);
				}else
				{
					result.setErrorCode(BaseException.ERROR_OTHER);
					result.setErrorMessage(name + " custom subject area already exists.");
				}
			}
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement setSubjectAreaPermissions(String subjectAreaName, String[] groupNames)
	{
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			
			if(subjectAreaName != null && subjectAreaName.length() > 0 && 
					groupNames != null && groupNames.length > 0){
				final SubjectAreaManager subjectAreaManager = new SubjectAreaManager(SMIWebUserBean.getUserBean().getRepository());
				saveCustomSubjectAreaMetadata(subjectAreaManager, subjectAreaName, groupNames);
			}
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement getSubjectAreaPermissions(String name)
	{
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
						
			final SubjectAreaManager subjectAreaManager = new SubjectAreaManager(SMIWebUserBean.getUserBean().getRepository());
			Element metadataElement = getCSAMetadataElement(subjectAreaManager, name);			
			if(metadataElement != null){
				OMFactory factory = OMAbstractFactory.getOMFactory();
				OMNamespace ns = null;
				OMElement groupsElement = factory.createOMElement(SubjectAreasGroups.GROUPS, ns);
				
				Element inGroupsElement = metadataElement.getChild(SubjectAreasGroups.GROUPS);
				for (Object groupObject : inGroupsElement.getChildren(SubjectAreasGroups.GROUP)) {
					Element groupElement = (Element) groupObject;
					// <Group>
					XmlUtils.addContent(factory, groupsElement, SubjectAreasGroups.GROUP, groupElement.getText(), ns);
				}				
				result.setResult(new XMLWebServiceResult(groupsElement));
			}
			else{
				result.setErrorCode(BaseException.ERROR_OTHER);
				result.setErrorMessage(name + " custom subject area not found");				
			}
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	private Element getCSAElement(SubjectAreaManager subjectAreaManager, String name){
		Element foundElement = null;
		if(name != null && name.length() > 0){
			List<File> currentCSAFiles = subjectAreaManager.getCustomSubjectAreaFiles();
			if(currentCSAFiles != null && currentCSAFiles.size() > 0){				
				final String subjectAreaFilename = this.getCSAFileName(name);
				for (File csaFile : currentCSAFiles) {
					String fileName = csaFile.getName();
					if(fileName != null && fileName.equalsIgnoreCase(subjectAreaFilename)){
						foundElement = com.successmetricsinc.util.FileUtils.readFileToDocument(csaFile);						 
						break;
					}
				}
			}
		}
		return foundElement;
	}
	
	private Element getCSAMetadataElement(SubjectAreaManager subjectAreaManager, String name){
		Element metadataElement = null;
		if(name != null && name.length() > 0){
			List<File> currentCSAMetadataFiles = subjectAreaManager.getCustomSubjectAreaMetadataFiles();
			if(currentCSAMetadataFiles != null && currentCSAMetadataFiles.size() > 0){				
				final String subjectAreaMetadataFilename = getCSAMetadataFileName(name);
				for (File csaFile : currentCSAMetadataFiles) {
					String fileName = csaFile.getName();
					if(fileName != null && fileName.equalsIgnoreCase(subjectAreaMetadataFilename)){
						metadataElement = com.successmetricsinc.util.FileUtils.readFileToDocument(csaFile);						
						break;
					}
				}
			}
		}
		return metadataElement;
	}
	
	public OMElement setSubjectAreaDescription(String name, String description)
	{
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			
			if(isDefaultSubjectArea(name)){				
				result.setErrorCode(BaseException.ERROR_OTHER);
				result.setErrorMessage("Cannot change the description of Default Subject Area");
			}
			else{
				final SubjectAreaManager subjectAreaManager = new SubjectAreaManager(SMIWebUserBean.getUserBean().getRepository());			
				Element foundElement = getCSAElement(subjectAreaManager, name);			
				if(foundElement != null){				
					foundElement.getChild(ExtendedSubjectAreaNode.DESCRIPTION).setText(description);					
					String customSubjectAreaXML = new XMLOutputter().outputString(foundElement);				
					saveCustomSubjectAreaProps(subjectAreaManager, name, customSubjectAreaXML);					
				}
				else{
					result.setErrorCode(BaseException.ERROR_OTHER);
					result.setErrorMessage(name + " custom subject area not found");				
				}
			}
			
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	@SuppressWarnings("unchecked")
	public OMElement setCustomSubjectArea(String csaXml) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		String name = CustomSubjectArea._EMPTY_STRING;
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SAXBuilder sb = new SAXBuilder();
			Document doc = sb.build(new StringReader(csaXml));
			Element root = doc.getRootElement();
			List<Element> children = root.getChildren();
			Iterator<Element> iterator = (Iterator<Element>)children.iterator();
			String currCSAXml;
			String currCSAXmlOutput;
			CustomSubjectArea csa;
			while (iterator.hasNext()) {
				// Traverse and build native xml.
				currCSAXml = (new XMLOutputter()).outputString(iterator.next());
				csa = new CustomSubjectArea(currCSAXml);
				name = csa.getName();
				
				// Validate CSA name.
				if (isDefaultSubjectArea(name)) {
					throw new Exception(DEFAULT_SUBJECT_AREA_NAME_EXCEPTION);
				}
				OMElement elem = getSubjectAreaByName(name);
				if (elem != null)
				{
					Iterator<OMElement> iter = elem.getChildElements();
					OMElement connElem = iter.next();
					String elemName = connElem.getLocalName();
					if (elemName.equals(BirstWebServiceResult.ERR_CODE)) {
						int errCode = XmlUtils.getIntContent(connElem);
						if (errCode != 0) {
							throw new Exception(ExtendedSubjectAreaNode.SA_NOT_FOUND_EX);
						}
					}
				}
				else {
					throw new Exception(ExtendedSubjectAreaNode.SA_NOT_FOUND_EX);
				}

				// Transform to native xml.
				csa.transform();
				currCSAXmlOutput = csa.xmlToString();

				// Validate results.
				if (currCSAXmlOutput != null && !currCSAXmlOutput.equals(CustomSubjectArea._EMPTY_STRING)) {
					OMFactory factory = OMAbstractFactory.getOMFactory();
					OMNamespace ns = null;
					OMElement statusElement = factory.createOMElement(STATUS, ns);
					statusElement.setText(currCSAXmlOutput);
					result.setResult(new XMLWebServiceResult(statusElement));
				} else {
					throw new Exception();
				}

				// Save native xml to disk.
				saveCustomSubjectArea(getGroupNames(name), currCSAXmlOutput);
			}
		} catch (Exception ex) {
			result.setException(ex);
			result.setErrorCode(BaseException.ERROR_OTHER);
			result.setErrorMessage("Exception setting " + name + " custom subject area: " + ex.getMessage());
		}
		return result.toOMElement();
	}
	
	private String[] getGroupNames(String name) {
		final SubjectAreaManager subjectAreaManager = new SubjectAreaManager(SMIWebUserBean.getUserBean().getRepository());
		Element metadataElement = getCSAMetadataElement(subjectAreaManager, name);
		Element inGroupsElement = null;
		String[] groupNames = null;
		if (metadataElement != null) {
			inGroupsElement = metadataElement.getChild(SubjectAreasGroups.GROUPS);
		}
		if (inGroupsElement != null) {
			int i = 0;
			groupNames = new String[inGroupsElement.getChildren().size()];
			for (Object groupObject : inGroupsElement.getChildren(SubjectAreasGroups.GROUP)) {
				Element groupElement = (Element) groupObject;
				groupNames[i] = groupElement.getValue();
				i++;
			}
		}
		return groupNames;
	}
	
	private boolean isDefaultSubjectArea(String name){
		return name != null && name.equalsIgnoreCase(DEFAULT_SUBJECT_AREA_NAME);
	}
	
	public OMElement getSubjectAreaDescription(String name)
	{
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			
			String description = null;
			if(isDefaultSubjectArea(name)){
				description = DEFAULT_DESCRIPTION;
			}
			else{
				final SubjectAreaManager subjectAreaManager = new SubjectAreaManager(SMIWebUserBean.getUserBean().getRepository());
				Element csaElement = this.getCSAElement(subjectAreaManager, name);
				if(csaElement != null){
					description = csaElement.getChildText(ExtendedSubjectAreaNode.DESCRIPTION);
				}
			}
			
			if(description != null){
				OMFactory factory = OMAbstractFactory.getOMFactory();
				OMNamespace ns = null;
				OMElement descriptionElement = factory.createOMElement("Description", ns);
				descriptionElement.setText(description);
				result.setResult(new XMLWebServiceResult(descriptionElement));
			}
			else{
				result.setErrorCode(BaseException.ERROR_OTHER);
				result.setErrorMessage(name + " custom subject area not found");				
			}
			
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement renameCustomSubjectArea(String name, String newName)
	{
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			
			if(isDefaultSubjectArea(name)){				
				result.setErrorCode(BaseException.ERROR_OTHER);
				result.setErrorMessage("Cannot rename Default Subject Area");
			}
			else{
				final SubjectAreaManager subjectAreaManager = new SubjectAreaManager(SMIWebUserBean.getUserBean().getRepository());			
				Element subjectAreaElement = getCSAElement(subjectAreaManager, name);
				Element metadataElement = getCSAMetadataElement(subjectAreaManager, name);

				if(subjectAreaElement != null && metadataElement != null){
					subjectAreaElement.getChild(LABEL).setText(newName);					
					String customSubjectAreaXML = new XMLOutputter().outputString(subjectAreaElement);				
					saveCustomSubjectAreaProps(subjectAreaManager, newName, customSubjectAreaXML);

					List<String> groupNames = new ArrayList<String>();
					Element inGroupsElement = metadataElement.getChild(SubjectAreasGroups.GROUPS);
					for (Object groupObject : inGroupsElement.getChildren(SubjectAreasGroups.GROUP)) {					
						Element groupElement = (Element) groupObject;
						groupNames.add(groupElement.getText());
					}				
					saveCustomSubjectAreaMetadata(subjectAreaManager, newName, groupNames.toArray(new String[groupNames.size()]));
					// now delete the old one
					deleteCSA(name, false);
				}else{
					result.setErrorCode(BaseException.ERROR_OTHER);
					result.setErrorMessage(name + " custom subject area not found");				
				}
			}
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	/*
	public OMElement editCustomSubjectArea(String name, String[] groupNames, String description)
	{
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			final SubjectAreaManager subjectAreaManager = new SubjectAreaManager(SMIWebUserBean.getUserBean().getRepository());
			List<File> currentCSAFiles = subjectAreaManager.getCustomSubjectAreaFiles();
			String foundId = null;
			final String subjectAreaFilename = this.getCSAFileName(name);
			for (File csaFile : currentCSAFiles) {
				String fileName = csaFile.getName();
				if(fileName != null && fileName.equalsIgnoreCase(subjectAreaFilename)){
					Element csaElement = com.successmetricsinc.util.FileUtils.readFileToDocument(csaFile);
					foundId = csaElement.getAttributeValue("id");
					break;
				}
			}
			
			if(foundId != null){
				String customSubjectAreaXML = getStringFiedXML(name, description, foundId, true);
				saveCustomSubjectAreaProps(subjectAreaManager, customSubjectAreaXML);
				saveCustomSubjectAreaMetadata(subjectAreaManager, name, groupNames);
			}else{
				result.setErrorCode(BaseException.ERROR_OTHER);
				result.setErrorMessage(name + " not found under custom subject area");				
			}
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	*/
	public OMElement deleteCustomSubjectAreaLite(String name)
	{
		return deleteCSA(name, false);
	}
	
	private String getStringFiedXML(String customSubjectAreaName, String description, String id, boolean valid)
	{	
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;
		OMElement root = fac.createOMElement(SubjectAreaNode.NODE, ns);		
		XmlUtils.addAttribute(fac, root, ExtendedSubjectAreaNode.ID, id, ns);
		XmlUtils.addAttribute(fac, root, ExtendedSubjectAreaNode.TYPE, ExtendedSubjectAreaNode.TYPE_FOLDER, ns);
		XmlUtils.addContent(fac, root, SubjectAreaNode.LABEL, customSubjectAreaName, ns);
		XmlUtils.addContent(fac, root, ExtendedSubjectAreaNode.DESCRIPTION, description, ns);
		XmlUtils.addContent(fac, root, SubjectAreaNode.VALID, valid, ns);
	    return root.toString();
	}

	private void addDefaultSubjectAreaMetaDataIfRequired(SubjectAreaManager subjectAreaManager) throws IOException
	{
		// If this is the first custom subject area, set DSA group = USER$
		if (subjectAreaManager.areNoCustomSubjectAreas()) {
			subjectAreaManager.addDefaultSubjectAreaMetadata();
		}
	}
	
	private void saveCustomSubjectAreaProps(SubjectAreaManager subjectAreaManager, 
			String subjectAreaName, String customSubjectAreaXmlString) throws JDOMException, IOException
	{
		logger.info("Saving custom subject area: " + subjectAreaName);
		addDefaultSubjectAreaMetaDataIfRequired(subjectAreaManager);

		// Save custom subject area
		final String subjectAreaFilename = getCSAFileName(subjectAreaName);
		subjectAreaManager.saveRepositoryFile(subjectAreaFilename, customSubjectAreaXmlString);
	}

	private void saveCustomSubjectAreaMetadata(SubjectAreaManager subjectAreaManager, String subjectAreaName, String[] groupNames) throws IOException
	{
		// Save custom subject area metadata
		final String metadata = subjectAreaManager.createSubjectAreaMetadata(subjectAreaName, groupNames);
		if (logger.isTraceEnabled())
			logger.trace("Metadata for: " + subjectAreaName + " ==> " + metadata);
		final String metadataFilename = getCSAMetadataFileName(subjectAreaName);
		subjectAreaManager.saveRepositoryFile(metadataFilename, metadata);
	}	
	
	/**
	 * Saves the custom subject area and the custom subject area's group associations
	 * in the <tt>&lt;repositoryRootDirectory&gt;/custom-subject-areas</tt> directory.
	 * The custom subject area information is saved in XML format with the naming
	 * convention of: <tt>subjectarea_&lt;subject area name&gt;.xml</tt>.
	 * The group associations (metadata) for the custom subject area is stored with
	 * a naming convention of: <tt>subjectarea-metadata_<subject area name>.xml</tt>.
	 * <p/>
	 * If this is the first subject area being saved, a default metadata file is 
	 * also created for the Default Subject Area that associates all users (i.e., 
	 * USER$ group) to the Default Subject Area.  In this way the Default Subject
	 * Area will be available to users until they are explicitly excluded (by the
	 * space admin removing the USER$ group from the Default Subject Area).
	 * @param groupNames are the groups associated with this custom subject area.
	 * @param customSubjectArea is the stringified XML of the custom subject area.
	 * @return the standard Axis2 OMElement containing the response, in this case
	 * the re-retrieved set of subject areas.
	 */
	public OMElement saveCustomSubjectArea(final String[] groupNames,
			                               final String customSubjectArea) {
		if (logger.isTraceEnabled())
		{
			logger.trace("Entering 'saveCustomSubjectArea()', 'customSubjectArea': " + customSubjectArea);
			for (String groupName : groupNames) {
				logger.trace("Entering 'saveCustomSubjectArea()', 'groupName': " + groupName);
			}
		}
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			// Get String-ified XML into workable format
			SAXBuilder builder = new SAXBuilder();
			StringReader sr = new StringReader(customSubjectArea);
			Document document = builder.build(sr);

			// Get name of subject area.
			Element rootElement = document.getRootElement();
			Element labelElement = rootElement.getChild(LABEL);
			String subjectAreaName = labelElement.getTextTrim();
			logger.info("Saving custom subject area: " + subjectAreaName);

			final SubjectAreaManager subjectAreaManager = new SubjectAreaManager(SMIWebUserBean.getUserBean().getRepository());

			// If this is the first custom subject area, set DSA group = USER$
			if (subjectAreaManager.areNoCustomSubjectAreas()) {
				subjectAreaManager.addDefaultSubjectAreaMetadata();
			}
			
			// Save custom subject area
			final String subjectAreaFilename = "subjectarea_" + subjectAreaName + ".xml";
			subjectAreaManager.saveRepositoryFile(subjectAreaFilename, customSubjectArea);

			// Save custom subject area metadata
			final String metadata = subjectAreaManager.createSubjectAreaMetadata(subjectAreaName, groupNames);
			if (logger.isTraceEnabled())
				logger.trace("Metadata for: " + subjectAreaName + " ==> " + metadata);
			final String metadataFilename = "subjectarea-metadata_" + subjectAreaName + ".xml";
			subjectAreaManager.saveRepositoryFile(metadataFilename, metadata);

			// Re-retrieve the subject areas
			ExtendedSubjectAreaNode node = generateExtendedSubjectArea();
			if (logger.isTraceEnabled())
				logger.trace("'saveCustomSubjectArea()' re-retrieved ExtendedSubjectAreaNode: " + node.toString().substring(0, 64));

			// Attach subject area to result and return
			result.setResult(node);
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	public OMElement saveSubjectAreaGroups(final String subjectAreaName, final String[] groupNames) {
		if (logger.isTraceEnabled())
		{
			logger.trace("Entering 'saveSubjectAreaGroups()', 'subjectAreaName': " + subjectAreaName);
			for (String groupName : groupNames) {
				logger.trace("Entering 'saveSubjectAreaGroups()', 'groupName': [" + groupName + "].");
			}
		}
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			// Save custom subject area metadata
			final SubjectAreaManager subjectAreaManager = new SubjectAreaManager(SMIWebUserBean.getUserBean().getRepository());
			final String metadata = subjectAreaManager.createSubjectAreaMetadata(subjectAreaName, groupNames);
			if (logger.isTraceEnabled())
				logger.trace("Metadata for: [" + subjectAreaName + "] ==> '" + metadata + "'");
			final String metadataFilename = "subjectarea-metadata_" + subjectAreaName + ".xml";
			subjectAreaManager.saveRepositoryFile(metadataFilename, metadata);

			// Attach subject area to result and return
			result.setErrorCode(0);
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	public OMElement deleteCustomSubjectArea(final String subjectAreaName) {
		return deleteCSA(subjectAreaName, true);
	}
	
	private OMElement deleteCSA(final String subjectAreaName, boolean returnUpdatedSubjectArea) {
		if (logger.isTraceEnabled())
			logger.trace("Entering 'deleteCustomSubjectArea()', 'subjectAreaName': " + subjectAreaName);
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			// Delete custom subject area
			final SubjectAreaManager subjectAreaManager = new SubjectAreaManager(SMIWebUserBean.getUserBean().getRepository());
			final String errorMessage = subjectAreaManager.deleteSubjectArea(subjectAreaName);
			if (!errorMessage.equals("")) {
				result.setErrorCode(BaseException.ERROR_OTHER);
				result.setErrorMessage(errorMessage);
				return result.toOMElement();
			}

			if(returnUpdatedSubjectArea){
				// Re-retrieve the subject areas
				ExtendedSubjectAreaNode node = generateExtendedSubjectArea();
				if (logger.isTraceEnabled())
					logger.trace("'saveCustomSubjectArea()' re-retrieved ExtendedSubjectAreaNode: " + node.toString().substring(0, 64));

				// Attach subject area to result and return
				result.setResult(node);
			}
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	/**
	 * This method retrieves all the groups that are available for the current space.
	 * Filter the list of groups so that all internal groups other than "USER$" are
	 * not returned.  CatMan and Custom Subject Area both expose the "USER$" group
	 * in addition to the external groups.
	 * @return the Axis2 OMElement representation of the space's groups.
	 */
	public OMElement getGroups() {
		if (logger.isTraceEnabled())
			logger.trace("'getGroups()'...");
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			// Get distinct list of all the groups (i.e., USER$, OWNER$, Administrator,...)
			final List<Group> allGroups = SMIWebUserBean.getUserBean().getRepository().getGroups(false);
			// Add groups to a IWebServiceResult implementer
			final List<String> groupNames = new ArrayList<String>();
			for (Group group : allGroups) {
				// Only add external groups or the "USER$" group
				if (!group.isInternalGroup() || group.getName().equals(Group.GROUP_USER$)) {
					groupNames.add(group.getName());
				}
			}
			final Groups groups = new Groups(groupNames);
			result.setResult(groups);
			if (logger.isTraceEnabled()) {
				for (String groupName : groupNames) {
					logger.trace("External groups for the current space: [" + groupName + "].");
				}
			}
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement getExternalGroups() {
		if (logger.isTraceEnabled())
			logger.trace("'getExternalGroups()'...");
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			
			
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMNamespace ns = null;
			OMElement root = factory.createOMElement("Groups", ns);
			final List<Group> allGroups = SMIWebUserBean.getUserBean().getRepository().getGroups(false);
			if(allGroups != null && allGroups.size() > 0){
				for (Group group : allGroups) {
					// Only add external groups or the "USER$" group
					if (!group.isInternalGroup()) {
						OMElement groupElement = factory.createOMElement("Group", ns);
						XmlUtils.addContent(factory, groupElement, "Id", group.getId(), ns);
						XmlUtils.addContent(factory, groupElement, "Name", group.getName(), ns);
						root.addChild(groupElement);
					}
				}
			}
			result.setResult(new XMLWebServiceResult(root));
			
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	

	/**
	 * This method retrieves all the groups that are available for the current space.
	 * 
	 * @return the Axis2 OMElement representation of the space's groups.
	 */
	public OMElement getSubjectAreasGroups() {
		if (logger.isTraceEnabled())
			logger.trace("'getSubjetAreasGroups()'...");
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			// Get instance of SubjectAreaManager
			final Repository repository = getUserBean().getRepository();
			final SubjectAreaManager subjectAreaManager = new SubjectAreaManager(repository);
			
			// Get all the subject area's metadata (groups)
			final SubjectAreasGroups subjectAreasGroups = subjectAreaManager.getSubjectAreasGroups();
			
			// Uses alternative approach - getSubjectAreasGroupsMap() - for debugging output
			if (logger.isTraceEnabled()) {
				final Map<String, List<String>> sagsMap = subjectAreasGroups.getSubjectAreasGroupsMap();
				for (String subjectAreaName : sagsMap.keySet()) {
					for (String groupName : sagsMap.get(subjectAreaName)) {
						logger.trace("'subjectAreasGroupsMap': [" + subjectAreaName + "] ==> '" + groupName + "'");
					}
				}
			}
			result.setResult(subjectAreasGroups);
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	/**
	 * This method retrieves all the groups that are available for the current space.
	 * 
	 * @return the spaces groups
	 */
	private SubjectAreasGroups getSubjectAreasGroupsMap() {
		// Get instance of SubjectAreaManager
		final Repository repository = getUserBean().getRepository();
		final SubjectAreaManager subjectAreaManager = new SubjectAreaManager(repository);
		// Get all the subject area's metadata (groups)
		return subjectAreaManager.getSubjectAreasGroups();
	}

	/* --------------------------------------------------------------------------------------------
	 * --------------------------------------------------------------------------------------------
	 * --- Helper methods                                                                       ---
	 * --------------------------------------------------------------------------------------------
	 * -------------------------------------------------------------------------------------------- */
	
	private void populateFlatColumnMap(HashMap<String, Column> map, Folder folder){
		if (folder.getSubfolders() != null) {
			for (Folder childFolder : folder.getSubfolders()) {
				populateFlatColumnMap(map, childFolder);
			}
		}
		
		if (folder.getColumns() != null) {
			for (Column column : folder.getColumns()) {
				String name = column.getName();
				if (column.getDimension() != null && column.getDimension().trim().length() > 0) {
					name = column.getDimension() + "." + name;
				}
				map.put(name, column);
			}
		}
	}

	private SubjectAreaNode generateSubjectArea(AdhocReport report){
		SMIWebUserBean ub = getUserBean();
		Repository repository = ub.getRepository();
		Map<String, Set<Object>> currentDimTables = SubjectAreaWS.getCurrentDimTables(report, repository);
		Map<String, Set<Object>> currentMeasureTables = getCurrentMeasureTables(report, repository);
		
		return generateSubjectArea(currentDimTables, currentMeasureTables, repository, ub);
	}
	
	private SubjectAreaNode generateSubjectArea(
			Map<String, Set<Object>> currentDimTables,
			Map<String, Set<Object>> currentMeasureTables,
			Repository repository,
			SMIWebUserBean ub) {
		SubjectAreaNode node = new SubjectAreaNode("", "Subject Area");
		// Get only non-internal groups, including the internal group "USER$"
		List<Group> groups = ub.getExternalGroupList(true);
		SubjectArea[] subjectAreas = repository.getSubjectAreas();
		if (subjectAreas != null) {
			if (subjectAreas.length == 1) {
				SubjectArea sa = subjectAreas[0];
				for (Folder folder : sa.getFolders()) {
					// children of complete
					SubjectAreaNode child = generateFolderTree("", folder, groups, currentDimTables, currentMeasureTables);
					if (child != null) {
						node.addChild(child);
					}
				}
			}
			else {
				for (SubjectArea sa : repository.getSubjectAreas()) {
					if (groupCanViewSubjectArea(Arrays.asList(sa.getGroups()))) {
						SubjectAreaNode subjectNode = new SubjectAreaNode(sa.getName(), sa.getName());
						subjectNode.markAsRootSubjectArea();
						for (Folder folder : sa.getFolders()) {
							// children of complete
							SubjectAreaNode child = generateFolderTree("", folder, groups, currentDimTables, currentMeasureTables);
							if (child != null) {
								subjectNode.addChild(child);
							}
						}
						node.addChild(subjectNode);
					}
				}
			}
		}

		node.updateValidity();
		
		return node;
	}
	
	
	private SubjectAreaNode generateFolderTree(String prefix, Folder folder, List<Group> groups, Map<String, Set<Object>> currentDimTables, Map<String, Set<Object>> currentMeasureTables) {
		String folderValue = prefix + SubjectAreaWS.NODE_SEPARATOR + folder.getName();
		SubjectAreaNode parent = new SubjectAreaNode(folderValue, folder.getName());
		
		if (folder.getSubfolders() != null) {
			for (Folder childFolder : folder.getSubfolders()) {
				SubjectAreaNode child = generateFolderTree(folderValue, childFolder, groups, currentDimTables, currentMeasureTables);
				if (child != null) {
					parent.addChild(child);
				}
			}
		}
		
		if (folder.getColumns() != null) {
			for (Column column : folder.getColumns()) {
				parent.addChild(new SubjectAreaNode(folderValue + SubjectAreaWS.NODE_SEPARATOR + column.getName(), column.getDisplayName(), true, folder.columnIsNavigatable(column, currentDimTables, currentMeasureTables)));
			}
		}

		return parent;
	}
	
	/**
	 * Gets the subject areas in two steps:
	 * <ul>
	 * <li>
	 * Retrieves the default subject area(s) using the existing {@link Repository#getSubjectAreas()}.
	 * In the case of Birst platform, there is normally just one default subject area that is dynamically
	 * generated.  <i>A big future TODO is to refactor Repository due to its size and complexity.</i>
	 * Each subject area folder is processed by {@link #generateExtendedFolderTree(String, int, Folder, String, List, Map, Map)}
	 * recursively to create each sub-node.
	 * </li>
	 * <li>
	 * {@link SubjectAreaManager#getCustomSubjectAreasNodes()} reads in all custom subject
	 * areas in JDOM Element form.  A {@link ExtendedSubjectAreaNode} is constructed (recursively)
	 * for each subject area using the corresponding Element.
	 * </li>
	 * </ul>
	 * @return a fully-populated, recursive instance of {@link ExtendedSubjectAreaNode}.
	 */
	private ExtendedSubjectAreaNode generateExtendedSubjectArea() {
		// Create root node
		long start = System.currentTimeMillis();
		ExtendedSubjectAreaNode rootNode = new ExtendedSubjectAreaNode("", SUBJECT_AREAS_NAME);
		
		SMIWebUserBean userBean = getUserBean();
		// Get default subject area(s) using Repository
		List<Group> groups = userBean.getGroupList(false);
		Repository repository = userBean.getRepository();
		SubjectArea[] subjectAreas = repository.getSubjectAreas();
		
		logger.debug("+++ generateExtendedSubjectArea (get subject areas): " + (System.currentTimeMillis() - start));
		
		Map<String, SavedExpressions.Expression> privateSavedExpressions = userBean.getRepSession(true).getPrivateSavedExpressions();
		Map<String, SavedExpressions.Expression> savedExpressions = repository.getSavedExpressions();
		if (savedExpressions == null) {
			if (privateSavedExpressions != null)
				savedExpressions = privateSavedExpressions;
			else
				savedExpressions = new HashMap<String, SavedExpressions.Expression>();
		}
		else if (privateSavedExpressions != null) {
			// private saved expressions will overwrite public ones with the same name
			savedExpressions.putAll(privateSavedExpressions);
		}
		
		ExtendedSubjectAreaNode defaultNode = null;
		if (subjectAreas != null && subjectAreas.length > 0) {
			// First subject area is always the default subject area
			defaultNode = new ExtendedSubjectAreaNode("", DEFAULT_SUBJECT_AREA_NAME, DEFAULT_DESCRIPTION, TYPE_FOLDER, 0);
			SubjectArea defaultSubjectArea = subjectAreas[0];
			defaultNode.markAsRootSubjectArea();
			for (Folder folder : defaultSubjectArea.getFolders()) {
				// Process children 
				ExtendedSubjectAreaNode child = generateExtendedFolderTree("", 0, folder, folder.getName(), groups, emptyTables, emptyTables);
				if (child != null) {
					defaultNode.addExtendedChild(child);
				}
			}
			rootNode.addExtendedChild(defaultNode);
		}
		
		for (SavedExpressions.Expression exp : savedExpressions.values()) {
			String loc = exp.getLocation();
			
			// find loc in rootNode children
			ExtendedSubjectAreaNode parent = null;
			int index = 0;
			String[] locs = loc == null ? new String[0] : loc.split("/");
			
			// handle case where its Subject Areas/Default Subject Area
			if (locs != null && locs.length > index && locs[index].equals(SUBJECT_AREAS_NAME))
				index++;
			if (locs != null && locs.length > index && locs[index].equals(DEFAULT_SUBJECT_AREA_NAME))
				index++;
			if (index >= locs.length) {
				locs = new String[0];
				loc = null;
			}
			
			List<ExtendedSubjectAreaNode> children = rootNode.getExtendedChildren();
			if (loc == null)
				parent = rootNode;
			if (children.size() == 1 && children.get(0).getName().length() == 0) {
				if (loc == null)
					parent = children.get(0);
				
				children = children.get(0).getExtendedChildren();
			}
			while (parent == null) {
				String currentLoc = locs[index];
				int i = 0;
				for (; i < children.size(); i++) {
					ExtendedSubjectAreaNode c = children.get(i);
					if (c.isLeafNode() == false && c.getName().equals(currentLoc)) {
						if (index == locs.length - 1) {
							parent = c;
						}
						else {
							index++;
							children = c.getExtendedChildren();
							break;
						}
					}
				}
				if (i >= children.size())
					break;
			}
			if (parent != null) {
				parent.addExtendedChild(new ExtendedSubjectAreaNode(exp));
			}
			else
				children.add(new ExtendedSubjectAreaNode(exp));
		}
		
		TimeDefinition td = repository.getTimeDefinition();
		List<String> periods = new ArrayList<String>();
		if (td != null && td.Periods != null)
		{
			for (TimePeriod p : td.Periods)
			{
				periods.add(p.Prefix);
			}
		}
		for (ExtendedSubjectAreaNode node : rootNode.getExtendedChildren()) {
			setDefaultSubjectAreaNodeReportLabel(node, periods);
		}
		
		// Get any custom subject areas using the SubjectAreaManager
		final SubjectAreaManager subjectAreaManager = new SubjectAreaManager(repository);
		List<Element> customSubjectAreasNodes = subjectAreaManager.getCustomSubjectAreasNodes();
		List<Element> importedCustomSubjectAreaNodes = subjectAreaManager.getImportedCustomSubjectAreas();
		customSubjectAreasNodes.addAll(importedCustomSubjectAreaNodes);
		// Process each custom subject area. Convert direct from Element, don't use SubjectArea & Folder
		if (customSubjectAreasNodes != null && customSubjectAreasNodes.size() > 0) {
			// Handle multiple custom subject areas. 'j' increment keeps '<n id="X.Y"...' X prefix unique across all subject areas
			for (int i = 0, j = subjectAreas.length; i < customSubjectAreasNodes.size(); i++, j++) {
				final Element csaElement = customSubjectAreasNodes.get(i);
				ExtendedSubjectAreaNode customNode = new ExtendedSubjectAreaNode(csaElement, j);
				customNode.markAsRootSubjectArea();
				
				if(importedCustomSubjectAreaNodes != null && importedCustomSubjectAreaNodes.size() > 0
						&& importedCustomSubjectAreaNodes.contains(csaElement)){
					customNode.markAsImportedSubjectArea();
				}
				// Check that metadata still exists in the default subject area
				/*
				 * this is taking he vast majority of the time.  removing it for now, will look at putting a 'auto prune' button in the admin panel
				long starti = System.currentTimeMillis();
				pruneMissingNodes(customNode, defaultNode);
				logger.debug("+++ generateExtendedSubjectArea (prune custom subject area node - " + i + "): " + (System.currentTimeMillis() - starti));
				*/
				rootNode.addExtendedChild(customNode);
			}
		}
		logger.debug("+++ generateExtendedSubjectArea (full): " + (System.currentTimeMillis() - start));
		return rootNode;
	}
	
	private void setDefaultSubjectAreaNodeReportLabel(ExtendedSubjectAreaNode node, List<String> periods) {
		if (node.isLeafNode()) {
			String lbl = node.getName();
			String prefix = null;
			int index = lbl.indexOf(' ');
			if (index > 0) {
				String firstWord = lbl.substring(0, index);
				if (periods.contains(firstWord)) 
					prefix = firstWord;
			}
			index = lbl.indexOf('.');
			if (index > 0) {
				lbl = lbl.substring(index + 1);
			}
			else {
				index = lbl.lastIndexOf(':');
				if (index > 0) {
					lbl = lbl.substring(index + 1).trim();
				}
			}
			if (prefix != null) {
				lbl = prefix + " " + lbl;
			}
			node.setReportElementLabel(lbl);
		}
		else {
			List<ExtendedSubjectAreaNode> children = node.getExtendedChildren();
			if (children != null && children.size() > 0) {
				for (ExtendedSubjectAreaNode saNode: children) {
					setDefaultSubjectAreaNodeReportLabel(saNode, periods);
				}
			}
		}
	}

	private ExtendedSubjectAreaNode generateExtendedFolderTree(String prefix,
			                                                   int idPrefix,
			                                                   Folder folder,
			                                                   String keyFolderName,
			                                                   List<Group> groups, 
			                                                   Map<String, Set<Object>> currentDimTables, 
			                                                   Map<String, Set<Object>> currentMeasureTables) {
		final String folderName = folder.getName();
		String type = folder.getType() == FolderType.olapMember ? ExtendedSubjectAreaNode.TYPE_OLAP_MEMBER : 
			folder.getType() == FolderType.olapDimension ? ExtendedSubjectAreaNode.TYPE_OLAP_DIMENSION :
				folder.getType() == FolderType.olapHierarchy ? ExtendedSubjectAreaNode.TYPE_OLAP_HIERARCHY : TYPE_FOLDER; 
		ExtendedSubjectAreaNode parent = new ExtendedSubjectAreaNode(folderName, folderName, "", type, idPrefix);
		
		if (folder.getSubfolders() != null) {
			for (Folder childFolder : folder.getSubfolders()) {
				ExtendedSubjectAreaNode child = generateExtendedFolderTree(folderName, idPrefix, childFolder, keyFolderName, groups, currentDimTables, currentMeasureTables);
				if (child != null) {
					parent.addExtendedChild(child);
				}
			}
		}
		
		if (folder.getColumns() != null) {
			for (Column column : folder.getColumns()) {
				final String columnName = deriveColumnName(keyFolderName, folder, column);
				parent.addExtendedChild(new ExtendedSubjectAreaNode(columnName, column.getDisplayName(), true, folder.columnIsNavigatable(column, currentDimTables, currentMeasureTables), keyFolderName, idPrefix));
			}
		}

		return parent;
	}
	
	
	private String deriveColumnName(final String keyFolderName, final Folder folder, final Column column) {
		String folderName = folder.getName();
		if (keyFolderName.equals(SubjectArea.AttributeFolderName)) {
			return column.getDimension() + "." + column.getName();
		} else {
			if (keyFolderName.equals(MeasureFolderName) || keyFolderName.equals(TimeMeasureFolderName)) {
				return column.getName();
			}
		}
		return folderName;
	}
	
	private boolean groupCanViewSubjectArea(final List<String> subjectAreaGroups) {
		boolean show = false;
		final List<Group> userGroups = getUserBean().getGroupList(false);
		// If user belongs to OWNER$ always show subject areas
		final List<String> userGroupNames = Group.getGroupNamesList(userGroups);
		if (userGroupNames.contains(Group.GROUP_OWNER$)) {
			show = true;
		} else {
			// Otherwise check to see if the user's groups intersects with the subject area's groups
			if (subjectAreaGroups != null) {
				if (subjectAreaGroups.size() > 0) {
					// 1 or more groups. Check each for inclusion
					for (Group g : userGroups) {
						if (subjectAreaGroups.contains(g.getName())) {
							show = true;
							break;
						}
					}
				}
			} 
		}
		return show;
	}

	private static void addDimensionColumnsToSet(Set<Object> dcSet, String dimension, String column, Repository r)
	{
		List<DimensionColumn> dclist = r.findDimensionColumns(dimension, column);
		if (dclist != null)
		{
			for (DimensionColumn dc : dclist)
			{
				dcSet.add(dc.DimensionTable);
			}
		}
	}

	private static void addMeasureColumnsToSet(Set<Object> mcSet, String measureColumnName, Repository r)
	{
		List<MeasureColumn> mclist = r.findMeasureColumns(measureColumnName);
		if (mclist != null)
		{
			for (MeasureColumn mc : mclist)
			{
				if(mc.MeasureTable.Type == MeasureTable.DERIVED)
				{
					if((mc.BaseMeasures != null) && (mc.BaseMeasures.size() > 0))
					{
						for(String bmcName : mc.BaseMeasures)
						{
							List<MeasureColumn> bmcList = r.findMeasureColumns(bmcName);
							for(MeasureColumn bmc : bmcList)
							{
								mcSet.add(bmc.MeasureTable);
							}
						}
					}
				}
				else
				{
					mcSet.add(mc.MeasureTable);
				}
			}
		}
	}

	private static Set<Object> getColumnTables(String dim, String col, Repository r) {
		Set<Object> oset = new HashSet<Object>();
		if (dim == null || dim.trim().equals(""))
		{
			addMeasureColumnsToSet(oset, col, r);
		} else
		{
			addDimensionColumnsToSet(oset, dim, col, r);
		}
		return oset;
	}

	private static Set<Object> getColumnTables(AdhocColumn rc, Repository r) {
		String dim = rc.getDimension();
		String col = rc.getColumn();
		return getColumnTables(dim, col, r);
	}

	@SuppressWarnings("unchecked")
	private static Map<String, Set<Object>> getCurrentMeasureTables(AdhocReport report, Repository r)
	{
		Map<String, Set<Object>> omap = new HashMap<String, Set<Object>>();
		if (report == null) {
			return omap;
		}
		List<AdhocColumn> columns = (ArrayList<AdhocColumn>)((ArrayList<AdhocColumn>)report.getColumns()).clone();
		
		for (AdhocColumn rc : columns)
		{
			if(rc.getType() == AdhocColumn.ColumnType.Measure) 
			{
				Set<Object> l = getColumnTables(rc, r);
				if (l != null) 
				{
					omap.put(rc.getName(), l);
				}
			}
			else if(rc.getType() == AdhocColumn.ColumnType.Expression)
			{
				DisplayExpression expression = rc.getExpression();
				if(expression != null)
				{
					List<MeasureExpressionColumn> mecList = expression.getMeasureExpressionColumns();
					if(mecList != null && mecList.size() > 0)
					{
						Set<Object> oset = new HashSet<Object>();
						for(MeasureExpressionColumn mec : mecList)
						{
							if(mec.getMeasureColumn() != null)
							{
								addMeasureColumnsToSet(oset, mec.getMeasureColumn().ColumnName, r);
							}
						}
						if(!oset.isEmpty())
						{
							omap.put(rc.getDimension()+'.'+rc.getColumn(), oset);
						}
					}
				}
			}
			else if (rc.getType() == ColumnType.Ratio) {
				Set<Object> oset = new HashSet<Object>();
				DisplayExpression expression = rc.getExpression();
				if(expression != null)
				{
					List<MeasureExpressionColumn> mecList = expression.getMeasureExpressionColumns();
					if(mecList != null && mecList.size() > 0)
					{
						for(MeasureExpressionColumn mec : mecList)
						{
							if(mec.getMeasureColumn() != null)
							{
								addMeasureColumnsToSet(oset, mec.getMeasureColumn().ColumnName, r);
							}
						}
					}
				}
				else {
					oset = getColumnTables(rc, r);
				}
				
				expression = rc.getDenominatorInRatio();
				if(expression != null)
				{
					List<MeasureExpressionColumn> mecList = expression.getMeasureExpressionColumns();
					if(mecList != null && mecList.size() > 0)
					{
						for(MeasureExpressionColumn mec : mecList)
						{
							if(mec.getMeasureColumn() != null)
							{
								addMeasureColumnsToSet(oset, mec.getMeasureColumn().ColumnName, r);
							}
						}
					}
				}
				
				if(!oset.isEmpty())
				{
					omap.put(rc.getDimension()+'.'+rc.getColumn(), oset);
				}
			}
		}
		
		// assume that the query filters are simple one level filters (for now XXX)
		List<QueryFilter> qfList = report.getFilters();
		if (qfList != null)
		{
			for (QueryFilter qf : qfList)
			{
				if (qf.getType() == QueryFilter.TYPE_PREDICATE)
				{
					if (qf.getColumnType() == QueryFilter.TYPE_MEASURE)
					{
						String name = qf.getColumn();
						List<MeasureColumn> mcList = r.findMeasureColumns(name);
						if (mcList != null)
						{
							Set<Object> oset = omap.get(name);
							if (oset == null)
							{
								oset = new HashSet<Object>();
								omap.put(name, oset);
							}
							for (MeasureColumn mc : mcList)
							{
								oset.add(mc.MeasureTable);
							}
						}
					}
				}
			}
		}
		List<DisplayFilter> dflist = report.getDisplayFilters();
		if (dflist != null)
		{
			for (DisplayFilter df : dflist)
			{
				List<String> mList = df.getMeasures();
				if (mList != null)
				{
					for (String name : mList)
					{
						List<MeasureColumn> mcList = r.findMeasureColumns(name);
						if (mcList != null)
						{
							Set<Object> oset = omap.get(name);
							if (oset == null)
							{
								oset = new HashSet<Object>();
								omap.put(name, oset);
							}
							for (MeasureColumn mc : mcList)
							{
								oset.add(mc.MeasureTable);
							}
						}
					}
				}
			}
		}
		return (omap);
	}

	@SuppressWarnings("unchecked")
	private static Map<String, Set<Object>> getCurrentDimTables(AdhocReport report, Repository r)
	{
		Map<String, Set<Object>> omap = new HashMap<String, Set<Object>>();
		if (report == null) {
			return omap;
		}
		List<AdhocColumn> columns = (ArrayList<AdhocColumn>)((ArrayList<AdhocColumn>)report.getColumns()).clone();
		for (AdhocColumn rc : columns)
		{
			if(rc.getType() == AdhocColumn.ColumnType.Dimension && ! rc.isOlapDimension()) 
			{
				Set<Object> l = getColumnTables(rc, r);
				if (l != null) 
				{
					omap.put(rc.getDimension()+'.'+rc.getColumn(), l);
				}
			}
			else if(rc.getType() == AdhocColumn.ColumnType.Expression)
			{
				DisplayExpression expression = rc.getExpression();
				if(expression != null)
				{
					List<DimensionExpressionColumn> decList = expression.getDimensionExpressionColumns();
					if(decList != null && decList.size() > 0)
					{
						Set<Object> oset = new HashSet<Object>();
						for(DimensionExpressionColumn dec : decList)
						{
							addDimensionColumnsToSet(oset, dec.getDimension(), dec.getColumn(), r);
						}
						if(!oset.isEmpty())
						{
							omap.put(rc.getDimension()+'.'+rc.getColumn(), oset);
						}
					}
				}
			}
		}
		
		// assume that the query filters are simple one level filters (for now XXX)
		List<QueryFilter> qfList = report.getFilters();
		if (qfList != null)
		{
			for (QueryFilter qf : qfList)
			{
				if (qf.getType() == QueryFilter.TYPE_PREDICATE)
				{
					if (qf.getColumnType() == QueryFilter.TYPE_DIMENSION_COLUMN)
					{
						String dim = qf.getDimension();
						String col = qf.getColumn();
						List<DimensionColumn> dcList = r.findDimensionColumns(dim, col);
						if (dcList != null)
						{
							String name = dim + '.' + col;
							Set<Object> oset = omap.get(name);
							if (oset == null)
							{
								oset = new HashSet<Object>();
								omap.put(name, oset);
							}
							for (DimensionColumn dc : dcList)
							{
								oset.add(dc.DimensionTable);
							}
						}
					}
				}
			}
		}
		List<DisplayFilter> dflist = report.getDisplayFilters();
		if (dflist != null)
		{
			for (DisplayFilter df : dflist)
			{
				List<String> aList = df.getAttributes(); // list of dim.col
				if (aList != null)
				{
					for (String name : aList)
					{
						String[] pair = name.split("\\."); // crack into dim, col
						List<DimensionColumn> dcList = r.findDimensionColumns(pair[0], pair[1]);
						if (dcList != null)
						{
							Set<Object> oset = omap.get(name);
							if (oset == null)
							{
								oset = new HashSet<Object>();
								omap.put(name, oset);
							}
							for (DimensionColumn dc : dcList)
							{
								oset.add(dc.DimensionTable);
							}
						}
					}
				}
			}
		}
		return (omap);
	}
}
