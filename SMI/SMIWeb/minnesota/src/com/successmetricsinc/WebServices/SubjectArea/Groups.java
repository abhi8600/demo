package com.successmetricsinc.WebServices.SubjectArea;

import java.util.List;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.successmetricsinc.WebServices.IWebServiceResult;
import com.successmetricsinc.util.XmlUtils;

public class Groups implements IWebServiceResult {

	private static final String GROUPS = "Groups";
	private static final String GROUP = "Group";
	private List<String> groupNames;
	
	public Groups(final List<String> groupNames) {
		this.groupNames = groupNames;
	}
	
	@Override
	public void addContent(OMElement parent, OMFactory factory, OMNamespace ns) {
		OMElement element = factory.createOMElement(GROUPS, ns);
		for (String groupName : groupNames) {
			XmlUtils.addContent(factory, element, GROUP, groupName, ns);
		}
		parent.addChild(element);
	}
}
