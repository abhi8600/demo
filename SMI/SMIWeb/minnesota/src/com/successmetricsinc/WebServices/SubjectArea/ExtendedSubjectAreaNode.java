/**
 * $Id: ExtendedSubjectAreaNode.java,v 1.30 2012-10-25 01:31:02 BIRST\gsingh Exp $
 *
 * Copyright (C) 2009-2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.WebServices.SubjectArea;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.OMNode;

import org.apache.log4j.Logger;

import org.jdom.Attribute;
import org.jdom.Element;

import com.successmetricsinc.Column;
import com.successmetricsinc.Folder;
import com.successmetricsinc.Repository;
import com.successmetricsinc.SubjectArea;
import com.successmetricsinc.transformation.object.SavedExpressions.Expression;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XmlUtils;

/**
 * An extension of {@link SubjectAreaNode} that adds a unique ID and an element type
 * to each element.  This facilitates the dragging-and-dropping and sorting of the
 * custom subject areas.
 * @author pconnolly
 */
public class ExtendedSubjectAreaNode extends SubjectAreaNode implements Cloneable {
	
	public static final String START = "s";
	public static final String REPORT_LABEL = "rl";
	public static final String DESCRIPTION = "d";
	public static final String ID = "id";
	public static final String TYPE = "type";
	public static final String TYPE_FOLDER = "f";
	public static final String TYPE_ATTRIBUTE = "a";
	public static final String TYPE_MEASUREMENT = "m";
	public static final String TYPE_SAVED_EXPRESSION = "s";
	public static final String TYPE_OLAP_MEMBER = "om";
	public static final String TYPE_OLAP_DIMENSION = "od";
	public static final String TYPE_OLAP_HIERARCHY = "oh";
	public static final String TYPE_MORE = "r";
	public static final String PERIOD = ".";
	public static final String EMPTY_STRING = "";
	public static final String SA_NOT_FOUND_EX = "Subject area does not exist.";
	public static final String SA_BACK_SLASH = "\"";
	public static final String SA_EMPTY_RESULTS = "<c></c>";
	public static final String SA_START_NODE = "<n t=\"";
	public static final String SA_END_NODE = "</n>";
	public static final String SA_START_BASE_NAME = " v=\"";
	public static final String SA_END_BASE_NAME = ">";
	public static final String SA_LABEL = " l=\"";
	public static final String SA_ROOT_TRUE = " isRootSA=\"true\"";
	public static final String SA_DESCRIPTION = " d=\"";
	public static final String SA_VALID_FALSE = " vl=\"false\"";
	public static final String SA_START = " s=\"";
	public static final String SA_BEGIN_CHILD = "<c>";
	public static final String SA_END_CHILD = "</c>";
	public static final String SA_OLAP_DIM = " ohf=\"true\"";
	public static final String SA_LEAF_NODE = " lf=\"true\"";
	public static final String SA_OLAP = " om=\"true\"";
	public static final String SA_CHILD_CNT = " c=\"";
	public static final String SA_REPORT_LABLE = " rl=\"";
	public static final String SA_IMPORTED = " isImported=\"true\"";

	protected static Logger logger = Logger.getLogger(ExtendedSubjectAreaNode.class);
		
	public enum NODE_TYPE {
		FOLDER,
		ATTRIBUTE,
		MEASUREMENT,
		SAVED_EXPRESSION,
		MORE,
		OLAP_HIERARCHY,
		OLAP_DIMENSION,
		OLAP_MEMBER
	};
	private List<ExtendedSubjectAreaNode> extendedChildren;
	private int idPrefix;
	private int idSeqn;
	private NODE_TYPE nodeType;
	private String description;
	private Element rootElement;
	private long childCount = -1;
	private boolean isOptimized = true;
	private int start = -1;
	private String olapDimension;
	private String olapHierarchy;
	
	// only used for updating validity of subject area
	private transient String prefix;
	
	private String reportElementLabel; 
	
	/**
	 * Basically only used for the root node that contains all the other subject area roots.
	 * @param name doesn't normally have a name.
	 * @param label should be "Subject Areas".
	 */
	public ExtendedSubjectAreaNode(String name, String label) {
		super(name, label);
		nodeType = NODE_TYPE.FOLDER;
	}
	
	public ExtendedSubjectAreaNode(String name, String label, String description, String type, int idPrefix) {
		super(name, label);
		this.description = description;
		this.idPrefix = idPrefix;
		this.nodeType = checkNodeType(type);
	}
	
	public ExtendedSubjectAreaNode(String name, String label, boolean isLeafNode, boolean isValid, String keyFolderName, int idPrefix) {
		super(name, label, isLeafNode, isValid);
		this.idPrefix = idPrefix;
		this.nodeType = deriveNodeTypeFromPrefix(keyFolderName);
	}
	
	public void setChildCount(long noChildren) {
		this.childCount = noChildren;
	}
	
	/**
	 * Used exclusively by the custom subject areas to instantiate the esaNode.
	 * Each custom subject area is stored as separate XML file, so it can be 
	 * read into a JDOM document and parsed to create the full structure of the
	 * esaNode without requiring the intermediation of Folder or SubjectAreaNode.
	 * @param element is the JDOM document element used to create this instance.
	 * @param idPrefix is the prefix of the "id" attribute to be used in order 
	 * to force 'id' uniqueness across all subject areas.
	 */
	public ExtendedSubjectAreaNode(final Element element, final int idPrefix) {
		this.rootElement = element;
		
		// Set superclass properties
		setLabel(element.getChildText(LABEL));
		setReportElementLabel(element.getChildText(REPORT_LABEL));
		setName(element.getChildText(NAME));
		setValid(element.getChildText(VALID).equals("true") ? true : false);
		this.description = element.getChildText(DESCRIPTION);
		setLeafNode(isLeafNode(this));
		
		// Set 'this' properties
		final Attribute typeAttr = element.getAttribute(TYPE);
		this.nodeType = checkNodeType(typeAttr);
		this.idPrefix = idPrefix;
		
		// Build any children
		if (!isLeafNode(this)) {
			extendedChildren = new ArrayList<ExtendedSubjectAreaNode>();
			for (Element childElement : getChildElements(element)) {
				final ExtendedSubjectAreaNode ecsaNode = new ExtendedSubjectAreaNode(childElement, idPrefix);
				extendedChildren.add(ecsaNode);
			}
		}
	}
	
	public ExtendedSubjectAreaNode(Expression exp) {
		String n = exp.getName();
		setLabel(n);
		setReportElementLabel(n);
		setName("SavedExpression('" + n + "')");
		setLeafNode(true);
		setNodeType(NODE_TYPE.SAVED_EXPRESSION);
		setValid(true);
	}

	@Override
	public void addContent(OMElement parent, OMFactory factory, OMNamespace ns) {
		// Create top-level 'element'
		OMElement element = factory.createOMElement(NODE, ns);
		
		// Add 'this' contents
		XmlUtils.addContent(factory, element, LABEL, getLabel(), ns);
		XmlUtils.addContent(factory, element, NAME, getName(), ns);
		XmlUtils.addContent(factory, element, DESCRIPTION, description, ns);
		if (isValid()) {
			XmlUtils.addContent(factory, element, VALID, true, ns);
		} else {
			XmlUtils.addContent(factory, element, VALID, false, ns);
		}
		XmlUtils.addAttribute(factory, element, ID, "X.Y", ns);
		XmlUtils.addAttribute(factory, element, TYPE, TYPE_FOLDER, ns);
		if (start >= 0) {
			XmlUtils.addAttribute(factory, element, START, String.valueOf(start), ns);
		}
		
		// If 'this' has any children, create <c>, add children to it and <c> back to parent
		if (!isLeafNode(this)) {
			OMElement childElement = factory.createOMElement(CHILD, ns);
			for (ExtendedSubjectAreaNode childNode : extendedChildren) {
				childElement.addChild(createChildElement(factory, ns, childNode, 0));
			}
			element.addChild(childElement);
		}
		parent.addChild(element);
	}
	
	public String getContentString(int dimensionalStructure) {
		// because of the size of this type of object, we optimize this to create
		// a string instead of an OMElement.
		return addChildContent(this, dimensionalStructure).toString();
	}
	
	private StringBuilder addChildContent(ExtendedSubjectAreaNode san, int dimensionalStructure) {
		try {
			StringBuilder sb = new StringBuilder();
			// Node
			sb.append("<n t=\"").append(san.getType()).append("\"");
			// Base name
			if (san.getName() != null && !san.getName().equals("")) {
				sb.append(" v=\"").append(XmlUtils.encode(san.getName())).append("\"");
			}
			if (san.getOlapDimension() != null && !san.getOlapDimension().equals("")) {
				sb.append(" od=\"").append(XmlUtils.encode(san.getOlapDimension())).append("\"");
			} 
			if (san.getOlapHierarchy() != null && !san.getOlapHierarchy().equals("")) {
				sb.append(" oh=\"").append(XmlUtils.encode(san.getOlapHierarchy())).append("\"");
			} 
			// Label should always exist
			sb.append(" l=\"").append(XmlUtils.encode(san.getLabel())).append("\"");
			
			//Marker, tells the UI that this is a root level Subject Area node
			if (san.isRootSubjectArea()) {
				sb.append(" isRootSA=\"true\"");
			}
			// Description. Used for tool tips.
			if (san.getDescription() != null && san.getDescription().equals("")) {
				sb.append(" d=\"").append(san.getDescription()).append("\"");
			}
			// Valid flag
			if(!san.isValid()){
				sb.append(" vl=\"false\"");
			}
			sb.append(">");
			// Add children nodes
			addChildContent(sb, san, dimensionalStructure);
			sb.append("</n>");
			return sb;
		} catch (UnsupportedEncodingException e) {
			logger.error(e, e);
		}
		return null;
	}
	
	public String getContentString(int dimensionalStructure, String saName) {
		// Because of the size of this type of object, we optimize this to create a string instead of an OMElement.
		return addChildContent(this, dimensionalStructure, saName).toString();
	}
	
	private StringBuilder addChildContent(ExtendedSubjectAreaNode san, int dimensionalStructure, String saName) {
		StringBuilder sb = new StringBuilder();
		try {
			// Node.
			sb.append(SA_START_NODE).append(san.getType()).append(SA_BACK_SLASH);
			
			// Base name.
			if (san.getName() != null && !san.getName().equals("")) {
				sb.append(SA_START_BASE_NAME).append(XmlUtils.encode(san.getName())).append(SA_BACK_SLASH);
			} 

			// Label should always exist.
			sb.append(SA_LABEL).append(XmlUtils.encode(san.getLabel())).append(SA_BACK_SLASH);
			
			// Marker, tells the UI that this is a root level Subject Area node.
			if (san.isRootSubjectArea()) {
				sb.append(SA_ROOT_TRUE);
			}
			
			// Description. Used for tool tips.
			if (san.getDescription() != null && san.getDescription().equals(EMPTY_STRING)) {
				sb.append(SA_DESCRIPTION).append(san.getDescription()).append(SA_BACK_SLASH);
			}
			
			// Valid flag.
			if(!san.isValid()) {
				sb.append(SA_VALID_FALSE);
			}
			sb.append(SA_END_BASE_NAME);
			
			// Add children nodes.
			addChildContent(sb, san, dimensionalStructure, saName);
			if (sb.toString().contains(SA_EMPTY_RESULTS)) {
				throw new Exception(SA_NOT_FOUND_EX);
			}
			sb.append(SA_END_NODE);
			
			return sb;
		} catch (Exception ex) {
			logger.error(ex, ex);
			sb.setLength(0);
			sb.append(ex.toString());
		}
		return sb;
	}
	
	//Designer and Dashboard node xml generation comes through here.
	public void addChildContent(StringBuilder sb, ExtendedSubjectAreaNode san, int dimensionalStructure) throws UnsupportedEncodingException {
		List<ExtendedSubjectAreaNode> children = san.getExtendedChildren();
		if (children != null) {
			sb.append("<c>");
			for (ExtendedSubjectAreaNode node : children) {
				sb.append("<n");
				sb.append(" t=\"").append(node.getType()).append("\"");
				if ((node.getType() == TYPE_FOLDER || node.getType() == TYPE_OLAP_DIMENSION || node.getType() == TYPE_OLAP_HIERARCHY) && dimensionalStructure == Repository.OLAP_DIMENSIONAL_STRUCTURE)
				{
					sb.append (" ohf=\"true\"");
				}
				
				String name = node.getName();
				if (dimensionalStructure == Repository.RELATIONAL_DIMENSIONAL_STRUCTURE)
				{
					if(node.isLeafNode() && name != null){
						sb.append(" v=\"").append(XmlUtils.encode(name)).append("\"");
					}
				}
				else if (dimensionalStructure == Repository.OLAP_DIMENSIONAL_STRUCTURE)
				{
					sb.append(" v=\"").append(XmlUtils.encode(name)).append("\"");
					if(node.isLeafNode())
						sb.append(" lf=\"true\"");
					sb.append(" om=\"true\"");
					if (node.childCount >= 0)
						sb.append(" c=\"" + node.childCount + "\"");
					if (node.getOlapDimension() != null && !node.getOlapDimension().equals("")) {
						sb.append(" od=\"").append(XmlUtils.encode(node.getOlapDimension())).append("\"");
					} 
					if (node.getOlapHierarchy() != null && !node.getOlapHierarchy().equals("")) {
						sb.append(" oh=\"").append(XmlUtils.encode(node.getOlapHierarchy())).append("\"");
					}
				}
				
				//Don't show label if it is the end part of ATTR.LABEL
				String label = node.getLabel();
				boolean isOptimized = getIsOptimized();
				if(name != null && (!name.endsWith("." + label) || !isOptimized)){
					sb.append(" l=\"").append(XmlUtils.encode(label)).append("\"");
				}
				
				String reportLabel = node.getReportElementLabel();
				if ((node.isLeafNode() || (dimensionalStructure == Repository.OLAP_DIMENSIONAL_STRUCTURE && node.getType() == TYPE_OLAP_MEMBER ))
						&&  reportLabel != null && Util.hasNonWhiteSpaceCharacters(reportLabel) && (!reportLabel.equals(label) || !isOptimized)) {
				//if (node.isLeafNode() &&  reportLabel != null && reportLabel.trim().length() > 0) {
					sb.append(" rl=\"").append(XmlUtils.encode(node.getReportElementLabel())).append("\"");
				}
				
				//Marker, tells the UI that this is a root level Subject Area node
				if (node.isRootSubjectArea()) {
					sb.append(" isRootSA=\"true\"");
				}
				
				if (node.isImportedSubjectArea()) {
					sb.append(" isImported=\"true\"");
				}
				
				if (node.getDescription() != null && !node.getDescription().equals("")) {
					sb.append(" d=\"").append(node.getDescription()).append("\"");
				}
				if(!node.isValid()){
					sb.append(" vl=\"false\"");
				}
				if (node.getStart() >= 0) {
					sb.append(" s=\"").append(node.getStart()).append("\"");
				}
				sb.append(">");
				addChildContent(sb, node, dimensionalStructure);
				sb.append("</n>");
			}
			sb.append("</c>");
		}
	}
	
	public void addChildContent(StringBuilder sb, ExtendedSubjectAreaNode san, int dimensionalStructure, String saName) 
	throws UnsupportedEncodingException, Exception {
		List<ExtendedSubjectAreaNode> children = san.getExtendedChildren();
		if (children != null) {
			boolean isOptimized = getIsOptimized();
			String label;
			String name;
			String reportLabel;
			sb.append(SA_BEGIN_CHILD);
			for (ExtendedSubjectAreaNode node : children) {
				// Initialize vars and filter if necessary.
				label = node.getLabel();
				name = node.getName();
				reportLabel = node.getReportElementLabel();
				if (node.isRootSubjectArea() && !label.equals(saName)) {
					continue;
				}

				// Build data xml.
				sb.append(SA_START_NODE).append((getIsOptimized()) ? san.getType() : node.getType()).append(SA_BACK_SLASH);
				
				if ((node.getType() == TYPE_FOLDER || node.getType() == TYPE_OLAP_DIMENSION || node.getType() == TYPE_OLAP_HIERARCHY) && dimensionalStructure == Repository.OLAP_DIMENSIONAL_STRUCTURE) {
					sb.append (SA_OLAP_DIM);
				}
				if (dimensionalStructure == Repository.RELATIONAL_DIMENSIONAL_STRUCTURE) {
					if(node.isLeafNode() && name != null){
						sb.append(SA_START_BASE_NAME).append(XmlUtils.encode(name)).append(SA_BACK_SLASH);
					}
				}
				else if (dimensionalStructure == Repository.OLAP_DIMENSIONAL_STRUCTURE) {
					sb.append(SA_START_BASE_NAME).append(XmlUtils.encode(name)).append(SA_BACK_SLASH);
					if (node.isLeafNode()) {
						sb.append(SA_LEAF_NODE);
					}
					sb.append(SA_OLAP);
					if (node.childCount >= 0) {
						sb.append(SA_CHILD_CNT).append(node.childCount).append(SA_BACK_SLASH);
					}
				}
				if(name != null && (!name.endsWith(PERIOD + label) || !isOptimized)){
					sb.append(SA_LABEL).append(XmlUtils.encode(label)).append(SA_BACK_SLASH);
				}
				if (node.isLeafNode() &&  reportLabel != null && Util.hasNonWhiteSpaceCharacters(reportLabel) && (!reportLabel.equals(label) || !isOptimized)) {
					sb.append(SA_REPORT_LABLE).append(XmlUtils.encode(node.getReportElementLabel())).append(SA_BACK_SLASH);
				}
				if (node.isRootSubjectArea()) {
					sb.append(SA_ROOT_TRUE);
				}
				if (node.isImportedSubjectArea()) {
					sb.append(SA_IMPORTED);
				}
				if (node.getDescription() != null && !node.getDescription().equals(EMPTY_STRING)) {
					sb.append(SA_DESCRIPTION).append(node.getDescription()).append(SA_BACK_SLASH);
				}
				if(!node.isValid()){
					sb.append(SA_VALID_FALSE);
				}
				if (node.getStart() >= 0) {
					sb.append(SA_START);
					sb.append(node.getStart());
					sb.append("\"");
				}
				sb.append(SA_END_BASE_NAME);
				addChildContent(sb, node, dimensionalStructure, saName);
				sb.append(SA_END_NODE);
			}
			sb.append(SA_END_CHILD);
		}
	}
	
	/*
	private void addChildContent(StringBuilder sb, ExtendedSubjectAreaNode san) throws UnsupportedEncodingException {
		List<ExtendedSubjectAreaNode> children = san.getExtendedChildren();
		if (children != null) {
			sb.append("<c>");
			for (ExtendedSubjectAreaNode node : children) {
				sb.append("<n type=\"").append(node.getType()).append("\">");
				if(node.isLeafNode() && node.getName() != null){
					sb.append("<v>").append(XmlUtils.encode(node.getName())).append("</v>");
				} 
				sb.append("<l>").append(XmlUtils.encode(node.getLabel())).append("</l>");
				
				if (node.isLeafNode() && node.getReportElementLabel() != null && node.getReportElementLabel().trim().length() > 0) {
					sb.append("<rl>").append(XmlUtils.encode(node.getReportElementLabel())).append("</rl>");
				}
				
				//Marker, tells the UI that this is a root level Subject Area node
				if (node.isRootSubjectArea()) {
					sb.append("<isRootSA/>");
				}
				if (node.getDescription() != null && !node.getDescription().equals("")) {
					sb.append("<d>").append(node.getDescription()).append("</d>");
				}
				sb.append("<vl>").append(node.isValid()).append("</vl>");
				addChildContent(sb, node);
				sb.append("</n>");
			}
			sb.append("</c>");
		}
	}*/
	
	@SuppressWarnings("unchecked")
	public boolean isLeafNode(final ExtendedSubjectAreaNode esaNode) {
		boolean isLeafNode = false;
		final Element rootElement = esaNode.getRootElement();
		if (rootElement == null) {
			return false;
		}
		final Element child = esaNode.getRootElement().getChild(CHILD);
		if (child == null) {
			isLeafNode = true;
		} else {
			final List<Element> childNodes = child.getChildren(NODE);
			if (childNodes == null || childNodes.isEmpty()) {
				isLeafNode = true;
			}
		}
		return isLeafNode;
	}
	
	@Override
	public void setLabel(String label) {
		super.setLabel(label);
		//reportElementLabel = label;
	}

	public List<SubjectAreaNode> updateExistingValidity(List<SubjectAreaNode>disabledNodes, 
            Map<String, Column> columnMap, 
            Map<String, Set<Object>> currentDimTables, 
            Map<String, Set<Object>> currentMeasureTables,
            String prefix) {
		this.prefix = prefix;
		if (this.extendedChildren != null && !this.isLeafNode()) {

			boolean valid = false;
			boolean anyChildValid = false;

			List<SubjectAreaNode> disabledChildNodes = new ArrayList<SubjectAreaNode>();
			for (SubjectAreaNode child: this.extendedChildren) {
				child.updateExistingValidity(disabledChildNodes, columnMap, currentDimTables, currentMeasureTables, prefix + SubjectAreaWS.NODE_SEPARATOR + this.label);	
				valid = valid || child.isValid();
				anyChildValid = anyChildValid || child.isValid();
			}
			isAllChildValid = anyChildValid;

			this.setValid(valid);
			//Don't add the children if the parent has been disabled
			if (anyChildValid){
				disabledNodes.addAll(disabledChildNodes);
			}
			else if (! valid) {
				disabledNodes.add(this);
			}
		}else{
			//we're the leaf node
			//System.out.println("checking.. " + this.name);
			Column col = columnMap.get(this.name);
			if (col != null){
				boolean valid = Folder.isNavigatable(col, currentDimTables, currentMeasureTables);
				this.setValid(valid);
				if (! valid)
					disabledNodes.add(this);
			}
			else {
				// assume it is navigable
				// probably a saved expression
				this.setValid(true);
			}
		}

		return disabledNodes;
}

	public String getName() {
		String n = super.getName();
		if (prefix != null && Util.hasNonWhiteSpaceCharacters(prefix)) {
			return prefix + SubjectAreaWS.NODE_SEPARATOR + n;
		}
		return n;
	}
	
	/**
	 * Look out, here comes some recursion!
	 * Scan through all the dependent nodes 'extendedChildren' and see if specified 'nodeName'
	 * is in the current tree of child nodes.  (The parent node is not examined.)
	 * This works generally, but is primarily used when the current instance of  
	 * {@link ExtendedSubjectAreaNode} is the default subject area node, to make sure
	 * that the current metadata in the space (via changes in Manage Sources) is
	 * accurately represented in the custom subject area node. In other words, it checks
	 * the case where the custom subject area node was created, but then some metadata 
	 * from the space sources was deleted.  The updated custom subject area should not have
	 * these deleted metadata.  Sorry.  That explanation was overly long and longer than the method!
	 * @param nodeName is the immutable name of the node to be searched for.
	 * @return true if 'nodeName' is contained within this instance of {@link ExtendedSubjectAreaNode}.
	 */
	public boolean hasNodeName(final String nodeName) {
		boolean hasNode = false;
		if (getExtendedChildren() == null || getExtendedChildren().isEmpty()) {
			if (getName().equals(nodeName)) {
				hasNode = true;
			} 
		} else {
			for (ExtendedSubjectAreaNode childNode : getExtendedChildren()) {
				if (childNode.hasNodeName(nodeName)) {
					hasNode = true;
					break;
				}
			}
		}
		return hasNode;
	}
	
	/**
	 * This method is typically used by the {@link ExtendedSubjectAreaNode} custom subject 
	 * area node to remove one of its child nodes.
	 * @param nodesToDelete are the nodes to be removed from the 'customNode' tree.
	 * @param customNode is the section of the custom subject area node tree to be processed.
	 */
	public void pruneChildNodes(final List<ExtendedSubjectAreaNode> nodesToDelete,
			                    ExtendedSubjectAreaNode customNode) {
		List<ExtendedSubjectAreaNode> mappedChildren = new ArrayList<ExtendedSubjectAreaNode>();
		// Look at each child of 'childNode'
		for (ExtendedSubjectAreaNode childNode : customNode.getExtendedChildren()) {
			// Examine it to see if it's one to be pruned
			boolean addIt = true;
			for (ExtendedSubjectAreaNode nodeToDelete : nodesToDelete) {
				if (childNode.getName().equals(nodeToDelete.getName())) {
					// If it's not in the kill list, add it
					addIt= false;
					break;
				}
			}
			if (addIt) {
				// If it's not in the kill list, add it
				mappedChildren.add(childNode);
			}
		}
		// Set the pruned list without any base name path concatenation
		customNode.setExtendedChildren(mappedChildren, false);
	}
	
	/* --------------------------------------------------------------------------------------------
	 * --------------------------------------------------------------------------------------------
	 * --- Helper methods                                                                       ---
	 * --------------------------------------------------------------------------------------------
	 * -------------------------------------------------------------------------------------------- */
	
	private OMNode createChildElement(final OMFactory factory, 
			                          final OMNamespace ns, 
			                          final ExtendedSubjectAreaNode esaNode,
			                          int idSeqn) {
		this.idSeqn = idSeqn;
		final OMElement childElement = factory.createOMElement(NODE, ns);
		
		// Folders have labels, no name or report label
		if (esaNode.getType().equals(TYPE_FOLDER) || esaNode.getType() == TYPE_OLAP_DIMENSION || esaNode.getType() == TYPE_OLAP_HIERARCHY) {
			XmlUtils.addContent(factory, childElement, LABEL, esaNode.getLabel(), ns);
		} else {
			// Attributes & measures always have names and report labels
			XmlUtils.addContent(factory, childElement, NAME, esaNode.getName(), ns);
			XmlUtils.addContent(factory, childElement, REPORT_LABEL, esaNode.getReportElementLabel(), ns);
			// Attributes & measures may have labels
			if (esaNode.getLabel() != null && !esaNode.getLabel().equals("")) {
				XmlUtils.addContent(factory, childElement, LABEL, esaNode.getLabel(), ns);
			} else {
				// If there's no label, default to the last portion of the column name
				XmlUtils.addContent(factory, childElement, LABEL, getColumnName(esaNode), ns);
			}
		}
		
		XmlUtils.addContent(factory, childElement, DESCRIPTION, esaNode.getDescription(), ns);
		if (esaNode.isValid()) {
			XmlUtils.addContent(factory, childElement, VALID, true, ns);
		} else {
			XmlUtils.addContent(factory, childElement, VALID, false, ns);
		}
		if(esaNode.isImportedSubjectArea())
		{
			XmlUtils.addAttribute(factory, childElement, "isImported", Boolean.TRUE.toString(), ns);
		}

		XmlUtils.addAttribute(factory, childElement, ID, esaNode.idPrefix + "." + this.idSeqn++ , ns);
		XmlUtils.addAttribute(factory, childElement, TYPE, esaNode.getType(), ns);
		// If 'this' has any children, create <c>, add children to it and <c> back to parent
		if (!esaNode.isLeafNode(esaNode) && esaNode.extendedChildren != null) {
			OMElement grandchildElement = factory.createOMElement(CHILD, ns);
			for (ExtendedSubjectAreaNode grandchildNode : esaNode.extendedChildren) {
				grandchildElement.addChild(createChildElement(factory, ns, grandchildNode, this.idSeqn++));
			}
			childElement.addChild(grandchildElement);
		}
		
		return childElement;
	}
	

	private NODE_TYPE checkNodeType(final Attribute typeAttr) {
		final String type = typeAttr.getValue();
		return checkNodeType(type);
	}
	
	private NODE_TYPE checkNodeType(final String type) {
		NODE_TYPE nodeType;
		if (type.equals(TYPE_FOLDER)) {
			nodeType = NODE_TYPE.FOLDER;
		} else {
			if (type.equals(TYPE_ATTRIBUTE)) {
				nodeType = NODE_TYPE.ATTRIBUTE;
			} else {
				if (type.equals(TYPE_MEASUREMENT)) {
					nodeType = NODE_TYPE.MEASUREMENT;
				} else if (type.equals(TYPE_SAVED_EXPRESSION)) {
					nodeType = NODE_TYPE.SAVED_EXPRESSION;
				} else if (type.equals(TYPE_MORE)) {
					nodeType = NODE_TYPE.MORE;
				} else if (type.equals(TYPE_OLAP_DIMENSION)) {
					nodeType = NODE_TYPE.OLAP_DIMENSION;
				} else if (type.equals(TYPE_OLAP_HIERARCHY)) {
					nodeType = NODE_TYPE.OLAP_HIERARCHY;
				} else if (type.equals(TYPE_OLAP_MEMBER)) {
					nodeType = NODE_TYPE.OLAP_MEMBER;
				} else {
					throw new RuntimeException("'ExtendedSubjectAreaNode' constructor passed invalid NODE_TYPE: " + type);
				}
			}
		}
		return nodeType;
	}
	
	private String getType() {
		String type;
		if (nodeType.equals(NODE_TYPE.FOLDER)) {
			type = TYPE_FOLDER;
		} else {
			if (nodeType.equals(NODE_TYPE.ATTRIBUTE)) {
				type = TYPE_ATTRIBUTE;
			} else {
				if (nodeType.equals(NODE_TYPE.MEASUREMENT)) {
					type = TYPE_MEASUREMENT;
				}
				else if (nodeType.equals(NODE_TYPE.SAVED_EXPRESSION)) {
					type = TYPE_SAVED_EXPRESSION;
				} else if (nodeType.equals(NODE_TYPE.MORE)) {
					type = TYPE_MORE;
				} else if (nodeType.equals(NODE_TYPE.OLAP_DIMENSION)) {
					type = TYPE_OLAP_DIMENSION;
				} else if (nodeType.equals(NODE_TYPE.OLAP_HIERARCHY)) {
					type = TYPE_OLAP_HIERARCHY;
				} else if (nodeType.equals(NODE_TYPE.OLAP_MEMBER)) {
					type = TYPE_OLAP_MEMBER;
				} else {
					type = "";
				}
			}
		}
		return type;
	}
	
	private NODE_TYPE deriveNodeTypeFromPrefix(final String prefix) {
		NODE_TYPE nodeType = NODE_TYPE.FOLDER;
		if (prefix.equals(SubjectArea.AttributeFolderName)) {
			nodeType = NODE_TYPE.ATTRIBUTE;
		} else {
			if (prefix.equals(SubjectArea.MeasureFolderName)) {
				nodeType = NODE_TYPE.MEASUREMENT;
			} else {
				if (prefix.equals(SubjectArea.TimeMeasureFolderName)) {
					nodeType = NODE_TYPE.MEASUREMENT;
				} 
			}
		}
		return nodeType;
	}
	
	@SuppressWarnings("unchecked")
	private List<Element> getChildElements(final Element element) {
		List<Element> childElements = null;
		final Element child = element.getChild(CHILD);
		if (child != null) {
			final List<Element> childNodes = child.getChildren(NODE);
			if (childNodes != null && childNodes.size() > 0) {
				childElements = new ArrayList<Element>();
				for (Element childElement : childNodes) {
					childElements.add(childElement);
				}
			}
		}
		return childElements;
	}

	public String getColumnName(final ExtendedSubjectAreaNode esaNode) {
		String column = esaNode.getName();
		if (column != null) {
			String[] paths = column.split(Pattern.quote(SubjectAreaWS.NODE_SEPARATOR));
			if (paths.length > 0){
				column = paths[paths.length - 1];
			}
		}
		return column;
	}

	/* --------------------------------------------------------------------------------------------
	 * --------------------------------------------------------------------------------------------
	 * --- Accessor methods                                                                     ---
	 * --------------------------------------------------------------------------------------------
	 * -------------------------------------------------------------------------------------------- */
	
	public List<ExtendedSubjectAreaNode> getExtendedChildren() {
		return extendedChildren;
	}

	/**
	 * Takes the default concatenation option of 'true' which prefixes each element
	 * with its folder structure.
	 * @param extendedChildren is the {@link ExtendedSubjectAreaNode} to be set to 'this's
	 * list of extended children.
	 */
	public void setExtendedChildren(List<ExtendedSubjectAreaNode> extendedChildren) {
		setExtendedChildren(extendedChildren, true);
	}

	/**
	 * Sets the extended children list.  Optionally modifies the base name of the 
	 * elements so that it contains the full folder structure. 
	 * @param extendedChildren is the {@link ExtendedSubjectAreaNode} to be set to 'this's
	 * list of extended children.
	 * @param concat if 'true' then modify the base names to include the full folder structure; otherwise
	 * 'false' will just do a straight 'set'.
	 */
	public void setExtendedChildren(List<ExtendedSubjectAreaNode> extendedChildren, final boolean concat) {
		if (concat) {
			if (this.getName() != null && this.getName().trim().length() > 0) {
				for (ExtendedSubjectAreaNode extendedChild: extendedChildren) {
					extendedChild.setName(this.getName().concat(SubjectAreaWS.NODE_SEPARATOR).concat(extendedChild.getName()));
				}
			}
		}
		this.extendedChildren = extendedChildren;
	}

	public void addExtendedChild(ExtendedSubjectAreaNode extendedChild) {
		if (this.extendedChildren == null) {
			this.extendedChildren = new ArrayList<ExtendedSubjectAreaNode>();
		}
		this.extendedChildren.add(extendedChild);
	}
	
	/**
	 * @return the idPrefix
	 */
	public int getIdPrefix() {
		return idPrefix;
	}

	/**
	 * @param idPrefix the idPrefix to set
	 */
	public void setIdPrefix(int idPrefix) {
		this.idPrefix = idPrefix;
	}

	/**
	 * @return the type
	 */
	public NODE_TYPE getNodeType() {
		return nodeType;
	}

	/**
	 * @param type the type to set
	 */
	public void setNodeType(NODE_TYPE nodeType) {
		this.nodeType = nodeType;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getOlapDimension() {
		return olapDimension;
	}

	public void setOlapDimension(String olapDimension) {
		this.olapDimension = olapDimension;
	}

	public String getOlapHierarchy() {
		return olapHierarchy;
	}

	public void setOlapHierarchy(String olapHierarchy) {
		this.olapHierarchy = olapHierarchy;
	}

	/**
	 * @return the JDOM root element underpinning this instance. 
	 */
	public Element getRootElement() {
		return rootElement;
	}
	
	public void clearPrefix() {
		this.prefix = null;
		if (extendedChildren != null) {
			for (ExtendedSubjectAreaNode child : extendedChildren) {
				child.clearPrefix();
			}
		}
	}

	public void setReportElementLabel(String reportElementLabel) {
		this.reportElementLabel = reportElementLabel;
	}

	public String getReportElementLabel() {
		return reportElementLabel;
	}
	
	public void setIsOptimized(boolean isOptimized) {
		this.isOptimized = isOptimized;
	}
	
	public boolean getIsOptimized() {
		return this.isOptimized;
	}
	
	/**
	 * A shallow copy. Clones everything except the child nodes in 'extendedChildren'.
	 * @return a shallow copy of the current instance.
	 */
	@Override
	public ExtendedSubjectAreaNode clone() {
		final ExtendedSubjectAreaNode node = new ExtendedSubjectAreaNode(name, label);
		
		// SubjectAreaNode properties
		node.isValid = isValid;
		node.isLeafNode = isLeafNode;
		node.isTopNodeSA = isTopNodeSA;
		node.isAllChildValid = isAllChildValid;
		
		// ExtendedSubjectAreaNode properties
		node.idPrefix = idPrefix;
		node.idSeqn = idSeqn;
		node.nodeType = nodeType;
		node.description = description;
		node.rootElement = rootElement;
		
		return node;
	}
	
	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

}
