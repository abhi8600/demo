/** $Id: BaseFileWS.java,v 1.82 2012-09-07 20:46:39 BIRST\ricks Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.WebServices.file;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jws.WebService;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.OMText;
import org.apache.log4j.Logger;
import org.apache.xerces.impl.dv.util.Base64;

import com.successmetricsinc.SessionHelper;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.WebServices.BirstWebService;
import com.successmetricsinc.WebServices.BirstWebServiceResult;
import com.successmetricsinc.WebServices.XMLWebServiceResult;
import com.successmetricsinc.WebServices.dashboard.DashboardMongoDBCache;
import com.successmetricsinc.adhoc.AdhocReport;
import com.successmetricsinc.catalog.CatalogAccessException;
import com.successmetricsinc.catalog.CatalogManager;
import com.successmetricsinc.catalog.CatalogPermission;
import com.successmetricsinc.catalog.DirectoryNode;
import com.successmetricsinc.catalog.FileDownload;
import com.successmetricsinc.catalog.FileNode;
import com.successmetricsinc.catalog.OldFileNode;
import com.successmetricsinc.catalog.CatalogManager.ACTION;
import com.successmetricsinc.catalog.CatalogManager.CatalogFile;
import com.successmetricsinc.rest.IndexingServices;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.FileUtils;
import com.successmetricsinc.util.SessionUtils;
import com.successmetricsinc.util.UserNotLoggedInException;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XmlUtils;

/**
 * There is a significant amount of logic in this class driven by the 'writeable' property.
 * It's not clear if the CatalogManagement-related features should be using this property or not.
 * @author agarrison
 * @author pconnolly
 */
@WebService(serviceName = "File")
public class BaseFileWS extends BirstWebService {

	private static Logger logger = Logger.getLogger(BaseFileWS.class);
	private static final String SLASH_REGEX = "\\" + File.separator;
	private static final String INSUFFICIENT_PERMISSIONS = "Insufficient permissions to complete request.";
	private static final String DELETE_AUTH = "Insufficient authority to delete %s by %s.";

	/**
	 * Specifically used to tunnel the error code from file operations (e.g.,
	 * moveFile(), renameFile(),...) into the BirstWebServiceResult of the
	 * subsequent getExtendedVisibleFilesystem() method. Should be reset to
	 * 'null' as soon as the transfer has been made.
	 */
	private Integer fileOperationErrorCode;
	/**
	 * Specifically used to tunnel the error message from file operations (e.g.,
	 * moveFile(), renameFile(),...) into the BirstWebServiceResult of the
	 * subsequent getExtendedVisibleFilesystem() method. Should be reset to
	 * 'null' as soon as the transfer has been made.
	 */
	private String fileOperationErrorMessage;

	/**
	 * Returns a tree of the file system visible to the user
	 * @param writeable if true, only return those directories that are writable by
	 * this user, otherwise return
	 * @return the file system that is available to the user
	 */
	public OMElement getVisibleFilesystem(Boolean writeable) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (isLoggedIn() == false) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean ub = getUserBean();
			if (!ub.isSuperUserOwnerOrAdmin() && !ub.hasEditReportCatalog())
			{
				return notAdminOMElement("getVisibleFilesystem()", result);
			}
			CatalogManager cm = ub.getCatalogManager();
			String root = cm.getCatalogRootPath();
			OldFileNode fn = new OldFileNode("", "RootDirs", true, new Date(0), false);
			List<File> flist = cm.getRootDirs(ub);
			int privateCount = 0;
			for (File file : flist) {
				String f = file.getAbsolutePath();
				int i = f.lastIndexOf(File.separatorChar);
				if (i > 0) {
					i = f.lastIndexOf(File.separatorChar, i - 1);
				}
				if (i > 0) {
					if (f.substring(i + 1).startsWith(
							CatalogManager.PRIVATE_DIR))
						privateCount++;
				}
			}
			for (File file : flist) {
				String f = file.getAbsolutePath();
				String[] parts = f.split(SLASH_REGEX);
				String label = parts[parts.length - 1];

				String dir = f.substring(root.length() + 1);
				if (privateCount == 1
						&& parts[parts.length - 2].equals(CatalogManager.PRIVATE_DIR)) {
					label = CatalogManager.PRIVATE_DIR;
					dir = dir.replace('\\', '/');
				}

				CatalogPermission perm = cm.getPermission(f, ub.isSuperUserOwnerOrAdmin());
				OldFileNode child = new OldFileNode(dir, label, true, file.lastModified(), 
						perm.canModify(ub.getUserGroups(), ub.isSuperUserOwnerOrAdmin()));
				if (child != null) {
					addAllChildren(child, cm, writeable, ub);
					fn.addChild(child);
				}
			}
			result.setResult(fn);
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	/**
	 * Returns a tree structure of the entire directory structure visible to the user
	 * @param writeable
	 * @return the file system that is available to the user
	 */
	public OMElement getVisibleFolderStructure(Boolean writeable) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (isLoggedIn() == false) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean ub = getUserBean();

			CatalogManager cm = ub.getCatalogManager();
			String root = cm.getCatalogRootPath();
			DirectoryNode fn = new DirectoryNode("");
			List<File> flist = cm.getRootDirs(ub);
			int privateCount = 0;
			for (File file : flist) {
				String f = file.getAbsolutePath();
				int i = f.lastIndexOf(File.separatorChar);
				if (i > 0) {
					i = f.lastIndexOf(File.separatorChar, i - 1);
				}
				if (i > 0) {
					if (f.substring(i + 1).startsWith(
							CatalogManager.PRIVATE_DIR))
						privateCount++;
				}
			}
			for (File file : flist) {
				String f = file.getAbsolutePath();
				String[] parts = f.split(SLASH_REGEX);
				String label = parts[parts.length - 1];

				String dir = f.substring(root.length() + 1);
				if (privateCount == 1
						&& parts[parts.length - 2].equals(CatalogManager.PRIVATE_DIR)) {
					label = CatalogManager.PRIVATE_DIR + FileNode.NODE_SEPARATOR + parts[parts.length - 1];
					dir = dir.replace('\\', '/');
				}

				DirectoryNode child = new DirectoryNode(label);
				if (child != null) {
					fn.addChild(child);
					addAllChildren(child, cm, ub);
				}
			}
			result.setResult(fn);
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	public OMElement getDirectory(String dir, Boolean writeable) {
		return getDirectoryWithSuffix(dir, writeable, null);
	}
	
	public OMElement getDirectoryWithSuffix(String dir, Boolean writeable, String[] validSuffixes) {
		// ignore writeable - doesn't matter any longer
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (isLoggedIn() == false) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean ub = getUserBean();
			CatalogManager cm = ub.getCatalogManager();
			String root = cm.getCatalogRootPath();
			FileNode fn = null;
			
			//Root directory, explicit. Bug 10977
			if(dir != null && dir.trim().equals("/")){
				dir = null;
			}
			
			boolean isPrivateDir = false;
			
			//Non root directory
			if (dir != null && Util.hasNonWhiteSpaceCharacters(dir)) {	
				//determine if dir exists
				String newDir = root + "/" + dir;
				File f = new File(newDir);
				if (f.exists() == false)
					dir = null;

				String d = f.getName();
				if (f.isDirectory() == false) {
					dir = f.getParent();
					d = f.getParentFile().getName();
				}
				else {
					dir = f.getAbsolutePath();
				}
				
				if (CatalogManager.PRIVATE_DIR.equalsIgnoreCase(d))
					isPrivateDir = true;
			}
			
			List<File> flist = cm.getRootDirs(ub);
			
			int privateCount = 0;
			for (File file : flist) {
				File parentFile = file.getParentFile();
				if (parentFile != null && CatalogManager.PRIVATE_DIR.equalsIgnoreCase(parentFile.getName())) {
					privateCount++;
				}
			}
			
			if (isPrivateDir && privateCount == 1)
				dir = null;
			
			//Root directory listing
			if (dir == null || !Util.hasNonWhiteSpaceCharacters(dir)) {
				fn = new FileNode("", "RootDirs", true, 0, false);
				for (File file : flist) {
					String f = file.getAbsolutePath();
					String[] parts = f.split(SLASH_REGEX);
					String label = parts[parts.length - 1];

					String d = f.substring(root.length() + 1);
					if (privateCount == 1
							&& parts[parts.length - 2].equals(CatalogManager.PRIVATE_DIR)) {
						label = CatalogManager.PRIVATE_DIR;
						d = d.replace('\\', '/');
					}
					
					CatalogPermission perm = cm.getPermission(f, ub.isSuperUserOwnerOrAdmin());

					FileNode child = new FileNode(d, label, true, 0, 
							perm.canModify(ub.getUserGroups(), ub.isSuperUserOwnerOrAdmin()));
					if (child != null) {
						fn.addChild(child);
					}
				}
			}
			else {
				// dir is now full path name
				File f = new File(dir);
				CatalogPermission perm = cm.getPermission(f.getAbsolutePath());
				String s1 = "";
				if (dir.length() > root.length() + 1){
					s1 = dir.substring(root.length() + 1);
					s1 = s1.replace('\\', '/');
				}
				fn = new FileNode(s1, f.getName(), f.isDirectory(), new Date(f.lastModified()), 
						perm.canModify(ub.getUserGroups(), ub.isSuperUserOwnerOrAdmin()));
				File[] children = f.listFiles();
				if (children != null) {
					for (File c:children) {
						if (c.getName().equalsIgnoreCase("dashboard.xml")) {
							fn.setIsDashboardDirectory(true);
							fn.setCreatedBy(AdhocReport.getCreatedBy(c));
							break;
						}
					}
				}
				List<String> folders = null;
				folders = cm.getViewFolders(dir, ub.getUserGroups(), ub.isSuperUserOwnerOrAdmin());
				
				if (folders != null) {
					for (String s : folders) {
						perm = cm.getPermission(s);
						String[] parts = s.split(SLASH_REGEX);
						String label = parts[parts.length - 1];
						if (ub.isFreeTrial() && label.equals("styles"))
							continue;
						File child = new File(s);
						FileNode node = new FileNode(label, label, true, new Date(child.lastModified()), perm.canModify(ub.getUserGroups(), ub.isSuperUserOwnerOrAdmin()));
						children = child.listFiles();
						if (children != null) {
							for (File c:children) {
								if (c.getName().equalsIgnoreCase("dashboard.xml")) {
									node.setIsDashboardDirectory(true);
									node.setCreatedBy(AdhocReport.getCreatedBy(c));
									break;
								}
							}
						}
						fn.addChild(node);
					}
				}
				List<CatalogFile> files = null;
				if (validSuffixes == null || validSuffixes.length == 0) {
					files = cm.getBirstFiles(dir, ub);
				} else {
					files = cm.getFiles(dir, new CatalogManager.UserDefinedFilenameFilter(validSuffixes), ub);
				}
				
				if (files != null) {
					for (CatalogFile cf : files) {
						String label = cf.getName();
						int index = label.lastIndexOf('.');
						if (index > 0) {
							String extension = label.substring(index);
							if (extension != null && extension.toLowerCase().equals(".dashlet")) {
								int index2 = label.lastIndexOf('.', index - 1);
								if (index2 > 0) 
									index = index2;
							}
							label = label.substring(0, index);
						}
						FileNode node = new FileNode(cf.getName(), label, false, cf.getModifiedDate(), false);
						File cfFile = cf.getFile();
						String cfName = cfFile.getName().toLowerCase();
						if (cfName.endsWith(".adhocreport") || cfName.endsWith(".page")) {
							node.setCreatedBy(AdhocReport.getCreatedBy(cfFile));
						}
						fn.addChild(node);
					}
				}
			}
			
			result.setResult(fn);
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	/**
	 * Returns a BirstWebServiceResult containing the tree of the file system visible
	 * to the user that includes file sizes and dates. This version will be somewhat
	 * easier to unit test since it returns an object rather than a String-ified
	 * version of the result XML.
	 * @param writeable if true, only return those directories that are writable by this user.
	 * @return a String-ified version of the FileNode hierarchy that is visible to the user
	 */
	protected BirstWebServiceResult getExtendedVisibleFilesystemResult(final Boolean writeable) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		// Check that the user is logged in and there's a current session
		if (isLoggedIn() == false) {
			return notLoggedInResult("getExtendedVisibleFilesystemResult()", result);
		}
		final SMIWebUserBean userBean = getUserBean();
		if (!userBean.isSuperUserOwnerOrAdmin() && !userBean.hasEditReportCatalog())
		{
			return notAdminResult("getExtendedVisibleFilesystemResult()", result);
		}
		
		// Get user's catalog area
		final CatalogManager catalogManager = getCatalogManager();
		// Create the root FileNode
		OldFileNode rootFileNode = new OldFileNode("", "RootDirs", true, 0, false);
		try {
			// Get the root directories for this user
			List<File> dirNames = catalogManager.getRootDirs(userBean);
			// Determine the 'private' directories
			for (File file : dirNames) {
				String dirName = file.getAbsolutePath();
				CatalogPermission perm = catalogManager.getPermission(dirName);
				// Change label field from 'private/user@domain' to 'private'
				final String filteredLabel = filterDirectoryLabel(dirName, catalogManager);
				final String filteredDirName = getRelativeFilepath(dirName, catalogManager);
				// Create directory FileNode
				OldFileNode child = new OldFileNode(filteredDirName, filteredLabel, true, file.lastModified(),
						perm.canModify(userBean.getUserGroups(), userBean.isSuperUserOwnerOrAdmin()));
				// Starting with private/shared nodes, populate all the descendant FileNodes
				if (child != null) {
					// Add children recursively. When finished add to 'root' FileNode.
					addAllChildrenExtended(child, catalogManager, userBean);
					rootFileNode.addChild(child);
				}
			}
			result.setResult(rootFileNode);
		} catch (Exception e) {
			logger.error("Problem processing 'getExtendedVisibleFilesystemResult()'\n", e);
			result.setException(e);
		}
		return result;
	}

	/**
	 * Returns a string-ified version of the tree of the file system visible to
	 * the user that includes file sizes and dates.
	 * 
	 * @param writeable if true, only return those directories that are writable by this user.
	 * @return a String-ified version of the FileNode hierarchy that is visible to the user
	 */
	public OMElement getExtendedVisibleFilesystem(Boolean writeable) {
		BirstWebServiceResult result = getExtendedVisibleFilesystemResult(writeable);
		return result.toOMElement();
	}
	
	/**
	 * Moves a set of files represented by the 'sourcePaths' array to the 'targetDirectory'
	 * folder.  Processing continues even if one of the sub-requests fails.  If multiple
	 * source files/folders fail, then all the associated error messages are returned.
	 * @param sourcePaths is the set of files to be moved.
	 * @param targetDirectory is the folder to which the source files/folders should be moved.
	 * @return an Axis2 response containing the extended file system response 
	 * (see {@link #getExtendedVisibleFilesystemResult(boolean)} annotated with any error messages 
	 * relevant to the requested file moves.
	 */
	public OMElement moveFiles(final String[] sourcePaths, final String targetDirectory) {
		if (logger.isTraceEnabled())
		{
			for (String sourcePath : sourcePaths) {
				logger.trace("'moveFiles().sourcePath':      " + sourcePath);
			}
			logger.trace("'moveFiles().targetDirectory': " + targetDirectory);
		}
		return moveCopyFiles("Move", sourcePaths, targetDirectory);
	}

	/**
	 * Moves a single file or folder to the specified 'targetDirectory'.
	 * @param sourcePath is the file to be moved.
	 * @param targetDirectory is the folder to which the 'sourcePath' file/folder will be moved.
	 * @return an Axis2 response containing the extended file system response 
	 * (see {@link #getExtendedVisibleFilesystemResult(boolean)} annotated with any error messages 
	 * relevant to the requested file moves.
	 */
	public OMElement moveFile(final String sourcePath, final String targetDirectory) {
		if (logger.isTraceEnabled())
		{
			logger.trace("'moveFile().sourcePath':      " + sourcePath);
			logger.trace("'moveFile().targetDirectory': " + targetDirectory);
		}
		return moveCopyFile("Move", sourcePath, targetDirectory);
	}

	
	/**
	 * Copies a set of files represented by the 'sourcePaths' array to the 'targetDirectory'
	 * folder.  Processing continues even if one of the sub-requests fails.  If multiple
	 * source files/folders fail, then all the associated error messages are returned.
	 * @param sourcePaths is the set of files to be copied.
	 * @param targetDirectory is the folder to which the source files/folders should be copied.
	 * @return an Axis2 response containing the extended file system response 
	 * (see {@link #getExtendedVisibleFilesystemResult(boolean)} annotated with any error messages 
	 * relevant to the requested file copies.
	 */
	public OMElement copyFiles(final String[] sourcePaths, final String targetDirectory) {
		if (logger.isTraceEnabled())
		{
			for (String sourcePath : sourcePaths) {
				logger.trace("'copyFile().sourcePath':      " + sourcePath);
			}
			logger.trace("'copyFile().targetDirectory': " + targetDirectory);
		}
		return moveCopyFiles("Copy", sourcePaths, targetDirectory);
	}

	/**
	 * Copies a single file or folder to the specified 'targetDirectory'.
	 * @param sourcePath is the file to be copied.
	 * @param targetDirectory is the folder to which the 'sourcePath' file/folder will be copied.
	 * @return an Axis2 response containing the extended file system response 
	 * (see {@link #getExtendedVisibleFilesystemResult(boolean)} annotated with any error messages 
	 * relevant to the requested file copies.
	 */
	public OMElement copyFile(final String sourcePath, final String targetDirectory) {
		if (logger.isTraceEnabled())
		{
			logger.trace("'copyFile().sourcePath':      " + sourcePath);
			logger.trace("'copyFile().targetDirectory': " + targetDirectory);
		}
		return moveCopyFile("Copy", sourcePath, targetDirectory);
	}

	/**
	 * Very similar to {@link #moveCopyFile(String, String, String)} except that it loops
	 * through the array of provided 'sourcePaths' and copies each one to the 'targetDirectory'.
	 * @param action is to 'Move' or 'Copy'.
	 * @param sourcePaths is the array of files' or folders' file paths to be moved or copied.
	 * @param targetDirectory is the target folder to which the 'sourcePaths' will be moved 
	 * or copied.
	 * @return an Axis2 response containing the extended file system response 
	 * (see {@link #getExtendedVisibleFilesystemResult(boolean)} annotated with any error messages 
	 * relevant to the requested file copies provided by {@link #refreshAndAnnotateFilesystem(String)}.
	 */
	protected OMElement moveCopyFiles(final String action,
                                      final String[] sourcePaths, 
                                      final String targetDirectory) {
		if (logger.isTraceEnabled())
		{
			logger.trace("'moveCopyFileResult().action':          " + action);
			for (String sourcePath : sourcePaths) {
				logger.trace("'moveCopyFileResult().sourcePath':      " + sourcePath);
			}
			logger.trace("'moveCopyFileResult().targetDirectory': " + targetDirectory);
		}
	    final CatalogManager.ACTION moveCopyAction = validateMoveCopyAction(action);
		
		// Check that the user is logged in and there's a current session
		if (isLoggedIn() == false) {
			return notLoggedInOMElement("moveCopyFiles()", new BirstWebServiceResult());
		}

		SMIWebUserBean ub = SMIWebUserBean.getUserBean();		
		if (!ub.isSuperUserOwnerOrAdmin() && !ub.hasEditReportCatalog())
		{
			return notAdminOMElement("moveCopyFiles()", new BirstWebServiceResult());
		}

		// Get user's catalog area
		CatalogManager catalogManager = ub.getCatalogManager();
		// Set catalog manager to seeing 'viewable' or 'modifiable' files & directories
		catalogManager.setWriteable(true);
		
		StringBuilder sb = new StringBuilder();
		for (String sourcePath : sourcePaths) {
		    BirstWebServiceResult moveCopyResult = new BirstWebServiceResult();
			try {
				moveCopyResult.setErrorMessage(catalogManager.moveCopyFile(moveCopyAction, 
				                                                           sourcePath, 
				                                                           targetDirectory,
				                                                           ub.getUserGroups(),
				                                                           ub.isSuperUserOwnerOrAdmin()));
			} catch (CatalogAccessException e) {
				moveCopyResult.setErrorCode(403);
				moveCopyResult.setErrorMessage(INSUFFICIENT_PERMISSIONS);
				logger.warn("Insufficient permissions to " + action + " " + sourcePath + " to " + targetDirectory);
				// They're all going to have the same problem
				break;
			}
			if (!moveCopyResult.getErrorMessage().equals("")) {
				sb.append(moveCopyResult.getErrorMessage()).append("\n");
			}
		}
		// Re-retrieve the newly updated catalog file system and add any error message
		return refreshAndAnnotateFilesystem(sb.length() > 0 ? sb.substring(0, sb.length() - 1).toString() : "");
	}

	/**
	 * Moves or copies one file or folder to the 'targetDirectory'.
	 * @param action is to 'Move' or 'Copy'.
	 * @param sourcePath is the file or folder's file path to be moved or copied.
	 * @param targetDirectory is the target folder to which the 'sourcePaths' will be moved 
	 * or copied.
	 * @return an Axis2 response containing the extended file system response 
	 * (see {@link #getExtendedVisibleFilesystemResult(boolean)} annotated with any error messages 
	 * relevant to the requested file copies provided by {@link #refreshAndAnnotateFilesystem(String)}.
	 */
	protected OMElement moveCopyFile(final String action,
                                     final String sourcePath, 
                                     final String targetDirectory) {
		if (logger.isTraceEnabled())
		{
			logger.trace("'moveCopyFileResult().action':          " + action);
			logger.trace("'moveCopyFileResult().sourcePath':      " + sourcePath);
			logger.trace("'moveCopyFileResult().targetDirectory': " + targetDirectory);
		}
	    final CatalogManager.ACTION moveCopyAction = validateMoveCopyAction(action);
	
	    BirstWebServiceResult moveCopyResult = new BirstWebServiceResult();
	
		// Check that the user is logged in and there's a current session
		if (isLoggedIn() == false) {
			return notLoggedInOMElement("moveCopyFile()", moveCopyResult);
		}
		
		SMIWebUserBean ub = SMIWebUserBean.getUserBean();
		if (!ub.isSuperUserOwnerOrAdmin() && !ub.hasEditReportCatalog())
		{
			return notAdminOMElement("moveCopyFile()", moveCopyResult);
		}		
		
		// Get user's catalog area
		CatalogManager catalogManager = ub.getCatalogManager();
		// Set catalog manager to seeing 'viewable' or 'modifiable' files & directories
		catalogManager.setWriteable(true);
		
		try {
			moveCopyResult.setErrorMessage(catalogManager.moveCopyFile(moveCopyAction, 
			                                                           sourcePath, 
			                                                           targetDirectory, 
			                                                           ub.getUserGroups(), 
			                                                           ub.isSuperUserOwnerOrAdmin()));
			invalidateDashboardCache(ub, sourcePath);
		} catch (CatalogAccessException e) {
			moveCopyResult.setErrorCode(403);
			moveCopyResult.setErrorMessage("Insufficient permissions to complete request.");
			logger.warn("Insufficient permissions to " + action + " " + sourcePath + " to " + targetDirectory);
		}
		// Re-retrieve the newly updated catalog file system and add any error message
		return refreshAndAnnotateFilesystem(moveCopyResult.getErrorMessage());
	}

	public OMElement simpleRenameFile(final String newName, final String oldRelativeFilePath) {
		
		return renameFileOnly(newName, oldRelativeFilePath).toOMElement();
	}
	
	private BirstWebServiceResult renameFileOnly(final String newName, final String oldRelativeFilePath) {
		if (logger.isTraceEnabled())
		{
			logger.trace("'renameFile().newName': " + newName);
			logger.trace("'renameFile().oldRelativeFilePath': " + oldRelativeFilePath);
		}
		BirstWebServiceResult renameResult = new BirstWebServiceResult();

		// Check that the user is logged in and there's a current session
		if (isLoggedIn() == false) {
			return notLoggedInResult("renameFile()", renameResult);
		}
		
		SMIWebUserBean userBean = SMIWebUserBean.getUserBean();
		if (!userBean.isSuperUserOwnerOrAdmin() && !userBean.hasEditReportCatalog())
		{
			return notLoggedInResult("renameFile()", renameResult);
		}

		// Get user's catalog area
		CatalogManager catalogManager = userBean.getCatalogManager();
		// Set catalog manager to seeing 'viewable' or 'modifiable' files & directories
		catalogManager.setWriteable(true);
		try {
			String errorMsg = catalogManager.renameFile(newName, oldRelativeFilePath, userBean.getUserGroups(), userBean.isSuperUserOwnerOrAdmin());
			if (errorMsg != null && errorMsg.trim().length() > 0) {
				renameResult.setErrorMessage(errorMsg);
				renameResult.setErrorCode(-1);
			}else{
				renameReportIndex(oldRelativeFilePath, newName);
			}
			invalidateDashboardCache(userBean, oldRelativeFilePath);
		} catch (CatalogAccessException e) {
			renameResult.setErrorCode(403);
			renameResult.setErrorMessage("Insufficient permissions to complete request.");
			logger.warn(fileOperationErrorCode + ": " + fileOperationErrorMessage);
			if (logger.isTraceEnabled())
				logger.trace(e);
		}
		
		return renameResult;
	}
	
	private void renameReportIndex(String oldRelativeFilePath, String newName){
		try{
			IndexingServices idxSvc = 
				new IndexingServices(SessionHelper.getHttpServletRequest(), SessionHelper.getSession().getServletContext());
			idxSvc.renameReport(oldRelativeFilePath, newName);
		}catch(Exception e){
			logger.error(e);
		}
	}
	
	/**
	 * @param newName is the new name that the file should have after the rename operation.
	 * @param oldRelativeFilePath is the original full filepath of the file to be renamed.
	 * @return a string-ified version of the BirstWebServiceResult for the rename operation.
	 */
	public OMElement renameFile(final String newName, final String oldRelativeFilePath) {
		
		BirstWebServiceResult renameResult = renameFileOnly(newName, oldRelativeFilePath);
		
		// Re-retrieve the newly updated catalog file system
		BirstWebServiceResult filesystemResult = getExtendedVisibleFilesystemResult(true);
		// Pick up any error messages from previous file operations (moveFile(), renameFile(),...)
		if (renameResult.getErrorCode() != 0) {
			filesystemResult.setErrorCode(renameResult.getErrorCode());
		}
		if (renameResult.getErrorMessage() != null) {
			filesystemResult.setErrorMessage(renameResult.getErrorMessage());
		}
		
		return filesystemResult.toOMElement();
	}

	/**
	 * Deletes the set of specified files and/or folders.
	 * If errors occur on one or more of the items, processing continues; error messages
	 * for the failed requests are combined and returned to the end-user. 
	 * @param relativeFilePaths is the list of file paths to be deleted.
	 * @return the web service result, which will have a non-blank error message if any
	 * of the requests encountered an error.  When there is more than one error message,
	 * there will be an EOL marker embedded in the error message string that indicates 
	 * the end of the message for an individual request. 
	 */
	public OMElement deleteFiles(String[] relativeFilePaths) {
		// Combine any error messages from each delete request
		String combinedErrors = deleteFilesOnly(relativeFilePaths);
		
		return refreshAndAnnotateFilesystem(combinedErrors);
	}
	
	public OMElement simpleDeleteFiles(String[] relativeFilePaths) {
		BirstWebServiceResult deleteResult = new BirstWebServiceResult();
		try {
			String combinedErrors = deleteFilesOnly(relativeFilePaths);
			deleteResult.setErrorMessage(combinedErrors);
		}
		catch (Exception e) {
			deleteResult.setErrorCode(-1);
			deleteResult.setErrorMessage(e.getMessage());
		}
		return deleteResult.toOMElement();
	}
	
	private String deleteFilesOnly(String[] relativeFilePaths) {
		if (logger.isTraceEnabled())
		{
			logger.trace("'deleteFiles()' request for:");
			for (String relativeFilePath : relativeFilePaths) {
				logger.trace("'deleteFiles()' file: " + relativeFilePath);
			}
		}
		
		// Process each file/folder, retaining any error messages
		List<String> errorMessages = new ArrayList<String>();
		for (String relativeFilePath : relativeFilePaths) {
			// Do the delete
			final BirstWebServiceResult result = deleteEachFile(relativeFilePath);
			if (!result.isSuccess() || !result.getErrorMessage().equals("")) {
				errorMessages.add(result.getErrorMessage());
			}
		}
		
		String combinedErrors = "";
		if (errorMessages.size() > 0) {
			logger.error("'deleteFiles()' encountered the following error(s): " + errorMessages);
			StringBuilder sb = new StringBuilder();
			for (String errorMessage : errorMessages) {
				sb.append(errorMessage).append("\n");
			}
			combinedErrors = sb.substring(0, sb.length() - 1).toString();
		}
		return combinedErrors;
	}
	
	/**
	 * Deletes the single, specified file or folder.
	 * @param relativeFilePath is the file or folder path to delete.
	 * @return the web service result.
	 */
	public OMElement deleteFile(String relativeFilePath) {
		if (logger.isTraceEnabled())
			logger.trace("'deleteFile().relativeFilePath': " + relativeFilePath);
		// Delete the file/folder
		final BirstWebServiceResult result = deleteEachFile(relativeFilePath);
		
		// Package the result and return
		final String errorMessage = result.getErrorMessage();
		return refreshAndAnnotateFilesystem(errorMessage);
	}

	/**
	 * This is the delete logic reused by {@link #deleteFile(String)} and {@link #deleteFiles(String[])}.
	 * @param relativeFilePath is the relative file path of the file or folder to be deleted.
	 * @return an instance of the web service result with any relevant error messages included.
	 * Since the delete processes end with a {@link #getExtendedVisibleFilesystemResult(boolean)},
	 * which will ultimately provide the OMElement required for the web services client, this
	 * result does not normally contain the OMElement payload.
	 */
	private BirstWebServiceResult deleteEachFile(String relativeFilePath) {
		if (logger.isTraceEnabled())
			logger.trace("'deleteEachFile().relativeFilePath': " + relativeFilePath);
		
		BirstWebServiceResult deleteResult = new BirstWebServiceResult();
		
		// Check that the user is logged in and there's a current session
		if (!isLoggedIn()) {
			return notLoggedInResult("deleteEachFile()", deleteResult);
		}

		SMIWebUserBean ub = getUserBean();
		if (!ub.isSuperUserOwnerOrAdmin() && !ub.hasEditReportCatalog())
		{
			return notAdminResult("moveCopyFile()", deleteResult);
		}

		// Validate input
		if (relativeFilePath == null || relativeFilePath.equals("")) {
			deleteResult.setErrorMessage("'deleteFile(s)' services cannot accept empty file paths.");
			deleteResult.setErrorCode(BaseException.ERROR_OTHER);
			logger.error("deleteEachFile()");
			return deleteResult;
		}
		
		// Get user's catalog area
		CatalogManager catalogManager = ub.getCatalogManager();
		try {
			String errMsg = catalogManager.deleteFile(relativeFilePath, ub.getUserGroups(), ub.isSuperUserOwnerOrAdmin());
			deleteResult.setErrorMessage(errMsg);
			invalidateDashboardCache(ub, relativeFilePath);
			
			if("".equals(errMsg)){
				deleteReportIndex(relativeFilePath);
			}
		} catch (CatalogAccessException e) {
			deleteResult.setErrorCode(403);
			deleteResult.setErrorMessage("Insufficient permissions to complete request.");
			logger.warn(String.format(DELETE_AUTH, relativeFilePath, SMIWebUserBean.getUserBean().getUserName()));
		}
		return deleteResult;
	}
	
	private void deleteReportIndex(String relativeFilePath){
		try{
			IndexingServices idxSvc = 
				new IndexingServices(SessionHelper.getHttpServletRequest(), SessionHelper.getSession().getServletContext());
			idxSvc.deleteReport(relativeFilePath);
		}catch(Exception e){
			logger.error(e);
		}
	}

	/**
	 * Gets a fresh copy of the visible file system using the 
	 * {@link #getExtendedVisibleFilesystemResult(boolean)}.  It then takes the
	 * provided 'errorMessages' String and incorporates it into the final result.
	 * @param errorMessages are the error messages to be injected into the 
	 * {@link #getExtendedVisibleFilesystemResult(boolean)} result.
	 * @return an Axis2 response containing the extended file system response 
	 * (see {@link #getExtendedVisibleFilesystemResult(boolean)} annotated with 
	 * any provided error messages. 
	 */
	private OMElement refreshAndAnnotateFilesystem(String errorMessages) {		
		// Re-retrieve the newly updated catalog file system
		BirstWebServiceResult filesystemResult = getExtendedVisibleFilesystemResult(true);
		
		// Pick up any error messages and remove absolute paths
		filesystemResult.setErrorMessage(stripAbsolutePaths(errorMessages));
		
		return filesystemResult.toOMElement();
	}
	
	/** 
	 * Create a new folder named New folder, New folder (2), etc... 
	 * @param relativeFilePath
	 * @return
	 */
	public OMElement createNewDirectory(final String relativeFilePath) {
		logger.trace("createNewDirectory in " + relativeFilePath);
		BirstWebServiceResult createDirectoryResult = new BirstWebServiceResult();
		try {
			SMIWebUserBean ub = SMIWebUserBean.getUserBean();		
			CatalogManager catalogManager = ub.getCatalogManager();
			// Set catalog manager to seeing 'viewable' or 'modifiable' files & directories
			catalogManager.setWriteable(true);
			
			String filePath = catalogManager.createNewDirectory(relativeFilePath, ub.getUserGroups(), ub.isSuperUserOwnerOrAdmin());
			
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMNamespace ns = null;
			OMElement root = factory.createOMElement("newDirectoryName",ns);
			OMText text = factory.createOMText(filePath);
			root.addChild(text);
			createDirectoryResult.setResult(new XMLWebServiceResult(root));
			
		}
		catch (Exception e) {
			createDirectoryResult.setErrorCode(-1);
			createDirectoryResult.setErrorMessage(e.getMessage());
		}
		return createDirectoryResult.toOMElement();
	}
	
	/**
	 * Create a new folder named 'dirName' in the current directory.
	 * 
	 * 
	 * @param dirName is the name that the new subdirectory should have after the create operation.
	 * @param relativeFilePath is the filepath where the subdirectory is to be created.
	 * @return a string-ified version of the BirstWebServiceResult for the rename operation.
	 */
	public OMElement createDirectory(final String dirName, final String relativeFilePath) {
		if (logger.isTraceEnabled())
		{
			logger.trace("'createDirectory().dirName': " + dirName);
			logger.trace("'createDirectory().relativeFilePath': " + relativeFilePath);
		}
		BirstWebServiceResult createDirectoryResult = createDirectoryResult(dirName, relativeFilePath);
		return createDirectoryResult.toOMElement();
	}

	/**
	 * Create a new folder named 'dirName' in the current directory. Mainly exists so that unit
	 * tests can access and verify the web service result more easily than querying the string-
	 * ified version of the result.
	 * 
	 * @param dirName is the name that the new subdirectory should have after the create operation.
	 * @param relativeFilePath is the filepath where the subdirectory is to be created.
	 * @return a string-ified version of the BirstWebServiceResult for the rename operation.
	 */
	protected BirstWebServiceResult createDirectoryResult(final String dirName, final String relativeFilePath) {
		if (logger.isTraceEnabled())
		{
			logger.trace("'createDirectoryResult().dirName':          " + dirName);
			logger.trace("'createDirectoryResult().relativeFilePath': " + relativeFilePath);
		}
		BirstWebServiceResult createDirectoryResult = new BirstWebServiceResult();

		// Check that the user is logged in and there's a current session
		if (!isLoggedIn()) {
			return notLoggedInResult("createDirectoryResult()", createDirectoryResult);
		}
		
		SMIWebUserBean ub = SMIWebUserBean.getUserBean();		
		if (!ub.isSuperUserOwnerOrAdmin() && ! ub.hasEditReportCatalog())
		{
			return notAdminResult("createDirectoryResult()", createDirectoryResult);
		}

		// Get user's catalog area
		CatalogManager catalogManager = ub.getCatalogManager();
		// Set catalog manager to seeing 'viewable' or 'modifiable' files & directories
		catalogManager.setWriteable(true);
		try {
			fileOperationErrorMessage = catalogManager.createDirectory(dirName, relativeFilePath, ub.getUserGroups(), ub.isSuperUserOwnerOrAdmin());
		} catch (CatalogAccessException e) {
			fileOperationErrorCode = 403;
			fileOperationErrorMessage = "Insufficient permissions to complete request.";
			logger.warn(fileOperationErrorCode + ": " + fileOperationErrorMessage);
			if (logger.isTraceEnabled())
				logger.trace(e);
		}
		// Re-retrieve the newly updated catalog file system
		BirstWebServiceResult filesystemResult = getExtendedVisibleFilesystemResult(true);
		// Pick up any error messages from previous file operations (moveFile(), renameFile(),...)
		if (fileOperationErrorCode != null) {
			filesystemResult.setErrorCode(fileOperationErrorCode);
		}
		if (fileOperationErrorMessage != null) {
			filesystemResult.setErrorMessage(fileOperationErrorMessage);
		}
		// Reset the error fields
		fileOperationErrorCode = null;
		fileOperationErrorMessage = null;

		return filesystemResult;
	}

	public OMElement getPermissions(final String folder) {
		if (logger.isTraceEnabled())
			logger.trace("'getPermissions()' for 'folder': " + folder);
		return getPermissionsResult(folder).toOMElement();
	}

	protected BirstWebServiceResult getPermissionsResult(String folder) {
		if (logger.isTraceEnabled())
			logger.trace("'getPermissionsResult()' for 'folder': " + folder);
		final BirstWebServiceResult permissionsResult = new BirstWebServiceResult();

		// Check that the user is logged in and there's a current session
		if (isLoggedIn() == false) {
			return notLoggedInResult("getPermissionsResult()", permissionsResult);
		}

		// Get user's catalog area
		CatalogManager catalogManager = getCatalogManager();

		//Root directory, explicit. Bug 10977
		if(folder != null && folder.trim().equals("/")){
			folder = null;
		}
		
		//Non root directory
		if (folder != null && Util.hasNonWhiteSpaceCharacters(folder)) {	
			//determine if dir exists
			String newDir = catalogManager.getNormalizedPath(folder);
			File f = new File(newDir);
			if (f.exists() == false)
				folder = null;
			
			if (f.isDirectory() == false)
				folder = f.getParent();
			else
				folder = f.getAbsolutePath();
		}
		else {
			folder = catalogManager.getCatalogRootPath();
		}
		// Get the permissions using CatalogManager
		final CatalogPermission catalogPermission = catalogManager.getPermission(folder, true);
		permissionsResult.setResult(catalogPermission);
		// Observe and report
		if (logger.isTraceEnabled())
			logger.trace("...permissions for: " + folder + "\n" + catalogPermission.toString());

		return permissionsResult;
	}

	public OMElement savePermissions(final String folder, final String permissions) {
		return savePermissionsResult(folder, permissions).toOMElement();
	}

	protected BirstWebServiceResult savePermissionsResult(String folder, final String permissions) {
		if (logger.isTraceEnabled())
		{
			logger.trace("'savePermissionsResult()' for 'folder':      " + folder);
			logger.trace("'savePermissionsResult()' for 'permission ': " + permissions);
		}
		final BirstWebServiceResult permissionsResult = new BirstWebServiceResult();

		// Check that the user is logged in and there's a current session
		if (!isLoggedIn()) {
			return notLoggedInResult("savePermissionsResult()", permissionsResult);
		}
		SMIWebUserBean userBean = SMIWebUserBean.getUserBean();
		if (!userBean.isSuperUserOwnerOrAdmin() && !userBean.hasEditReportCatalog())
		{
			return notAdminResult("savePermissionsResult()", permissionsResult);
		}

		// Get user's catalog area
		CatalogManager catalogManager = getCatalogManager();

		// Convert to full file path and normalize
		//Root directory, explicit. Bug 10977
		if(folder != null && folder.trim().equals("/")){
			folder = null;
		}
		
		//Non root directory
		if (folder != null && Util.hasNonWhiteSpaceCharacters(folder)) {	
			//determine if dir exists
			String newDir = catalogManager.getNormalizedPath(folder);
			File f = new File(newDir);
			if (f.exists() == false)
				folder = null;
			
			if (f.isDirectory() == false)
				folder = f.getParent();
			else
				folder = f.getAbsolutePath();
		}
		else {
			folder = catalogManager.getCatalogRootPath();
		}
		
		// Save the permissions
		catalogManager.savePermissions(folder, permissions);

		// Get the updated permissions
		final CatalogPermission updatedCatalogPermission = catalogManager.getPermission(folder);
		permissionsResult.setResult(updatedCatalogPermission);

		return permissionsResult;
	}

	/**
	 * Uploads a new file named 'filename' in the specified directory.
	 * 
	 * @param uploadFolder is the name that the subdirectory to which the file will be written.
	 * @param filename is the name of the file that will be written.
	 * @param digest is the SHA256 digest of the encoded file contents.
	 * @param encodedContents is the base64-encoded contents of the file to be written.
	 * @return a string-ified version of the BirstWebServiceResult for the file upload operation.
	 */
	public OMElement uploadFile(final String uploadFolder, 
			                    final String filename,
			                    final String digest, 
			                    final String encodedContents) {
		if (logger.isTraceEnabled())
		{
			logger.trace("'uploadFile().uploadFolder': " + uploadFolder);
			logger.trace("'uploadFile().filename': " + filename);
			logger.trace("'uploadFile().digest': " + digest);
			logger.trace("'uploadFile().encodedContents': " + encodedContents.substring(0, 64));
		}
		BirstWebServiceResult result = uploadFileResult(uploadFolder, filename, digest, encodedContents);
		return result.toOMElement();
	}

	/**
	 * Uploads a new file named 'filename' in the specified directory. Mainly
	 * exists so that unit tests can access and verify the web service result
	 * more easily than querying the string-ified version of the result.
	 * 
	 * @param uploadFolder is the name that the subdirectory to which the file will be written.
	 * @param filename is the name of the file that will be written.
	 * @param digest is the SHA256 digest of the encoded file contents.
	 * @param encodedContents is the base64-encoded contents of the file to be written.
	 * @return the BirstWebServiceResult for the file upload operation.
	 */
	protected BirstWebServiceResult uploadFileResult(final String uploadFolder,
			                                         final String filename, 
			                                         final String digest,
			                                         final String encodedContents) {
		if (logger.isTraceEnabled())
		{
			logger.trace("'uploadFileResult().uploadFolder': " + uploadFolder);
			logger.trace("'uploadFileResult().filename': " + filename);
			logger.trace("'uploadFileResult().digest': " + digest);
			logger.trace("'uploadFileResult().encodedContents': " + encodedContents.substring(0, 64));
		}
		BirstWebServiceResult uploadFileResult = new BirstWebServiceResult();

		// Check that the user is logged in and there's a current session
		if (!isLoggedIn()) {
			return notLoggedInResult("uploadFileResult()", uploadFileResult);
		}

		SMIWebUserBean userBean = SMIWebUserBean.getUserBean();
		if (!userBean.isSuperUserOwnerOrAdmin() && !userBean.hasEditReportCatalog())
		{
			return notAdminResult("uploadFileResult()", uploadFileResult);
		}

		// Get user's catalog area
		CatalogManager catalogManager = userBean.getCatalogManager();
		// Set catalog manager to seeing 'viewable' or 'modifiable' files & directories
		catalogManager.setWriteable(true);
		// TODO: Check SHA256 digest here
		// ...
		// Decode from base64
		byte[] decodedContents = Base64.decode(encodedContents);
		try {
			fileOperationErrorMessage = catalogManager.writeFile(uploadFolder, filename, decodedContents,
					userBean.getUserGroups(), userBean.isSuperUserOwnerOrAdmin());
			invalidateDashboardCache(userBean, filename);
		} catch (CatalogAccessException e) {
			fileOperationErrorCode = 403;
			if (e.getReason().equals(CatalogAccessException.Reason.Error)) {
				fileOperationErrorMessage = e.getMessage();
			} else {
				fileOperationErrorMessage = "Insufficient permissions to complete request.";
			}
			logger.warn(fileOperationErrorCode + ": " + fileOperationErrorMessage);
			if (logger.isTraceEnabled())
				logger.trace(e);
		}
		// Re-retrieve the newly updated catalog file system
		BirstWebServiceResult filesystemResult = getExtendedVisibleFilesystemResult(true);
		// Pick up any error messages from previous file operations (moveFile(), renameFile(),...)
		if (fileOperationErrorCode != null) {
			filesystemResult.setErrorCode(fileOperationErrorCode);
		}
		if (fileOperationErrorMessage != null) {
			filesystemResult.setErrorMessage(fileOperationErrorMessage);
		}
		// Reset the error fields
		fileOperationErrorCode = null;
		fileOperationErrorMessage = null;

		return filesystemResult;
	}

	/**
	 * "Downloads" an existing file in the user's catalog area specified by the
	 * 'relativeFilePath' parameter.
	 * 
	 * @param relativeFilePath is the path of the requested file relative to the root of the
	 * current user's catalog area.
	 * @return a string-ified version of the BirstWebServiceResult for the file upload operation.
	 */
	public OMElement downloadFile(final String relativeFilePath) {
		if (logger.isTraceEnabled())
			logger.trace("'downloadFile().relativeFilePath': " + relativeFilePath);

		BirstWebServiceResult result = downloadFileResult(relativeFilePath);
		return result.toOMElement();
	}

	/**
	 * Downloads an existing file from the user's catalog area using the
	 * 'relativeFilePath'. This version of the method exists so that unit tests
	 * can access and verify the web service result more easily than querying
	 * the string-ified version of the result.
	 * 
	 * @param relativeFilePath is the path of the requested file relative to the root of the
	 * current user's catalog area.
	 * @return the BirstWebServiceResult for the file download operation.
	 */
	protected BirstWebServiceResult downloadFileResult(final String relativeFilePath) {
		if (logger.isTraceEnabled())
			logger.trace("'downloadFileResult().relativeFilePath': " + relativeFilePath);

		BirstWebServiceResult downloadFileResult = new BirstWebServiceResult();

		// Check that the user is logged in and there's a current session
		if (!isLoggedIn()) {
			return notLoggedInResult("downloadFileResult()", downloadFileResult);
		}
		SMIWebUserBean ub = SMIWebUserBean.getUserBean();		
		if (!ub.isSuperUserOwnerOrAdmin() && !ub.hasEditReportCatalog())
		{
			return notAdminResult("downloadFileResult()", downloadFileResult);
		}

		// Get user's catalog area
		CatalogManager catalogManager = getCatalogManager();

		// Get the file as a byte array
		byte[] unencodedFileContents = null;
		try {
			unencodedFileContents = catalogManager.readFile(relativeFilePath);
		} catch (CatalogAccessException e) {
			fileOperationErrorCode = 403;
			if (e.getReason().equals(CatalogAccessException.Reason.Error)) {
				fileOperationErrorMessage = e.getMessage();
			} else {
				fileOperationErrorMessage = "Insufficient permissions to complete request.";
			}
			logger.warn(fileOperationErrorCode + ": " + fileOperationErrorMessage);
			if (logger.isTraceEnabled())
				logger.trace(e);
		} catch (IOException e) {
			fileOperationErrorCode = 500;
			fileOperationErrorMessage = "Problem reading file from catalog area.";
			logger.error(fileOperationErrorCode + ": " + fileOperationErrorMessage, e);
		}
		// Decode from base64
		String encodedFileContents = Base64.encode(unencodedFileContents);

		// Add Axis2-friendly adapter object to the WS result
		final String filename = FileUtils.getFileBasename(relativeFilePath);
		final FileDownload fileDownload = new FileDownload(filename, encodedFileContents);
		downloadFileResult.setResult(fileDownload);

		// Pick up any error messages from previous file operations (moveFile(), renameFile(),...)
		if (fileOperationErrorCode != null) {
			downloadFileResult.setErrorCode(fileOperationErrorCode);
		}
		if (fileOperationErrorMessage != null) {
			downloadFileResult.setErrorMessage(fileOperationErrorMessage);
		}
		// Reset the error fields
		fileOperationErrorCode = null;
		fileOperationErrorMessage = null;

		return downloadFileResult;
	}

	/* --------------------------------------------------------------------------
	 * -- The set of methods that return just name and path in the FileNode    --
	 * -- instances. (used in Adhoc file dialogs)                              --
	 * -------------------------------------------------------------------------- */

	private void addAllChildren(DirectoryNode child, CatalogManager manager, SMIWebUserBean ub) {
		String dirName = manager.getCatalogRootPath() + "/" + child.getFullPath();
		List<String> folders = manager.getViewFolders(dirName, ub.getUserGroups(), ub.isSuperUserOwnerOrAdmin());
		if (folders != null) {
			for (String s : folders) {
				String[] parts = s.split(SLASH_REGEX);
				String label = parts[parts.length - 1];
				DirectoryNode dn = new DirectoryNode(label);
				child.addChild(dn);
				addAllChildren(dn, manager, ub);
			}
		}
	}
	private void addAllChildren(OldFileNode child, CatalogManager manager, boolean writeable, SMIWebUserBean ub) {
		// ignore writeable 
		addAllViewableChildren(child, manager, ub);
	}

	private void addAllViewableChildren(OldFileNode child, CatalogManager manager, SMIWebUserBean ub) {
		String dirName = manager.getCatalogRootPath() + "/" + child.getName();
		List<String> folders = manager.getViewFolders(dirName, ub.getUserGroups(), ub.isSuperUserOwnerOrAdmin());
		if (folders != null) {
			for (String s : folders) {
				String[] parts = s.split(SLASH_REGEX);
				String label = parts[parts.length - 1];
				String dir = child.getName() + "/" + label;
				CatalogPermission perm = manager.getPermission(dir);
				OldFileNode fn = new OldFileNode(dir, label, true, 0, perm.canModify(ub.getUserGroups(), ub.isSuperUserOwnerOrAdmin()));
				addAllViewableChildren(fn, manager, ub);
				child.addChild(fn);
			}
		}
		List<CatalogFile> files = manager.getBirstFiles(dirName, ub);
		if (files != null) {
			for (CatalogFile cf : files) {
				String label = cf.getName();
				int index = label.lastIndexOf('.');
				if (index > 0) {
					label = label.substring(0, index);
				}
				OldFileNode fn = new OldFileNode(child.getName()
						+ FileNode.NODE_SEPARATOR + cf.getName(), label, false, cf.getModifiedDate(), cf.canModify());
				child.addChild(fn);
			}
		}
	}

	/* ------------------------------------------------------------------------ */
	/* The set of methods that return full file info in the FileNode instances. */
	/* (used in CatalogManagement)                                              */
	/* ------------------------------------------------------------------------ */

	private void addAllChildrenExtended(final OldFileNode child, final CatalogManager catalogManager, final SMIWebUserBean userBean) {
		// Recreate full directory filepath
		String fullDirPath = catalogManager.getCatalogRootPath() + File.separator + child.getName();
		// Get list of viewable/modifiable folders under that 'dirName'
		List<File> subdirectories = catalogManager.getFolderFiles(fullDirPath);
		if (subdirectories != null) {
			for (File subdirectory : subdirectories) {
				
				CatalogPermission perm = catalogManager.getPermission(subdirectory.getAbsolutePath());
				
				// Skip if this folder is not viewable
				if (!perm.canView(userBean.getUserGroups(), userBean.isSuperUserOwnerOrAdmin())) {
					continue;
				}
				// Create FileNode instance for this 'subdirectory'
				String label = subdirectory.getName();
				final String relativePath = getRelativeFilepath(subdirectory.getAbsolutePath(), catalogManager);
				boolean isDirectory = true;
				Date modifiedDate = new Date(subdirectory.lastModified());
				OldFileNode grandchildFileNode = new OldFileNode(relativePath, 
                                                           label, 
                                                           isDirectory, 
                                                           modifiedDate,
                                                           perm.canModify(userBean.getUserGroups(), userBean.isSuperUserOwnerOrAdmin()));
				// Recurse through children of this most current child
				addAllChildrenExtended(grandchildFileNode, catalogManager, userBean);
				child.addChild(grandchildFileNode);
			}
		}
		// Now process any files under the child folder
		List<CatalogFile> catalogFiles = catalogManager.getAllFiles(fullDirPath, userBean);
		if (catalogFiles != null) {
			boolean found1stDashboard = false;
			for (CatalogFile catalogFile : catalogFiles) {
				String label = catalogFile.getName();
				if (CatalogManager.isBirstFileType(catalogFile.getFile())) {
					String name = getRelativeFilepath(catalogFile.getFile().getAbsolutePath(), catalogManager);
					Date modifiedDate = catalogFile.getModifiedDate();
					OldFileNode fn = new OldFileNode(name, label, false, modifiedDate, catalogFile.canModify());
					child.addChild(fn);
				} else {
					// If the 'child' contains dashboard marker file, set 'child's flag
					if (label.equalsIgnoreCase(CatalogManager.DASHBOARD_MARKER_FILE)) {
						if (!found1stDashboard) {
							child.setIsDashboardDirectory(true);
							found1stDashboard = true;
						}
					}
				}
			}
		}
	}

	/**
	 * Just validates that a 'move' or 'copy' action request was entered.
	 * 
	 * @return the CatalogManager.ACTION version of the entered String.
	 */
	private ACTION validateMoveCopyAction(String action) {
		if (action.equalsIgnoreCase("move")) {
			return ACTION.MOVE;
		} else {
			if (action.equalsIgnoreCase("copy")) {
				return ACTION.COPY;
			} else {
				throw new IllegalArgumentException("Invalid file move/copy action was encountered.");
			}
		}
	}

	/**
	 * Sends back the last subdir in the path, unless the parent is 'private' in which case the 
	 * 'private' portion is sent back too. For example, if path is .../private/user@domain then 
	 * the returned string will be 'private/user@domain'.
	 * 
	 * @param dirName is the full directory name to be filtered.
	 * @param catalogManager is the user's catalog manager instance.
	 * @return is the filtered (or not) label for the directory.
	 */
	private String filterDirectoryLabel(final String dirName, final CatalogManager catalogManager) {
		String[] parts = dirName.split(SLASH_REGEX);
		// If penultimate subdir is 'private', send back 'private/user@domain'
		if (parts[parts.length - 2].equals(CatalogManager.PRIVATE_DIR)) {
			return parts[parts.length - 2] + "/" + parts[parts.length - 1];
		} else {
			// Otherwise just send the last subdir in the path
			return parts[parts.length - 1];
		}
	}

	/**
	 * Converts a directory filepath to one that is relative to the user's catalog root and
	 * convert any backslashes to forward slashes.
	 * 
	 * @param directoryPath is the full directory filepath.
	 * @param catalogManager is the user's catalog manager instance.
	 * @return a directory filepath that is relative to the user's catalog root
	 * with all forward slashes, no backslashes.
	 */
	private String getRelativeFilepath(final String directoryPath,
			                           final CatalogManager catalogManager) {
		// Get full filepath for the user's catalog
		String rootFilePath = catalogManager.getCatalogRootPath();
		// Normalize (resolve) the current directory name
		String normalizedDirectoryPath = null;
		try {
			normalizedDirectoryPath = new File(directoryPath).getCanonicalPath();
		} catch (IOException e) {
			logger.error("Could not get the canonical path of the directory: " + directoryPath, e);
			
		}
		String name = "";
		if (normalizedDirectoryPath != null) {
			// Strip off the user catalog path from the beginning of the directoryPath
			name = directoryPath.substring(rootFilePath.length() + 1);
			name = name.replace('\\', '/');
		}
		// Convert any backslashes to forward slashes and return
		return name;
	}
	
	/**
	 * Removes the root folder from any input string.
	 * @param errorMessage is the string from which to excise the root folder.
	 * @return a modified version of the 'errorMessage' input that contains relative 
	 * file path references.
	 */
	private String stripAbsolutePaths(final String errorMessage) {
		// First get the root folder
		final CatalogManager catalogManager = getCatalogManager();
		final String regexRootFolder = catalogManager.getCatalogRootPath().replace("\\", "\\\\");
		// Remove root folder from messages
		String filteredErrorMessage = errorMessage.replaceAll(regexRootFolder, "");
		// Convert remaining relative file path to use forward slashes
		filteredErrorMessage = filteredErrorMessage.replaceAll("\\\\", "/");
		
		return filteredErrorMessage;
	}
	
	private BirstWebServiceResult swappedProgressInResult(String request, BirstWebServiceResult result) {
		result.setErrorMessage(request + "' webservice requested but swap space is in progress.");
		result.setErrorCode(BaseException.ERROR_SWAP_SPACE_IN_PROGRESS);
		logger.error("'" + request + "' - Error code: " + result.getErrorCode() 
				   + "; error message: " + result.getErrorMessage());
		return result;
	}
	
	private BirstWebServiceResult notLoggedInResult(String request, BirstWebServiceResult result) {
		result.setErrorMessage(request + "' webservice requested but user was not logged in.");
		result.setErrorCode(BaseException.ERROR_NOT_LOGGED_IN);
		logger.error("'" + request + "' - Error code: " + result.getErrorCode() 
				   + "; error message: " + result.getErrorMessage());
		return result;
	}
	
	private BirstWebServiceResult notAdminResult(String request, BirstWebServiceResult result) {
		result.setErrorMessage(request + "' webservice requested but user is not an admin.");
		result.setErrorCode(BaseException.ERROR_PERMISSION);
		logger.error("'" + request + "' - Error code: " + result.getErrorCode() 
				   + "; error message: " + result.getErrorMessage());
		return result;
	}
	
	private OMElement notLoggedInOMElement(String request, BirstWebServiceResult result) {
		return notLoggedInResult(request, result).toOMElement();
	}
	
	private OMElement notAdminOMElement(String request, BirstWebServiceResult result) {
		return notAdminResult(request, result).toOMElement();
	}
	
	private CatalogManager getCatalogManager() {
		// Get user's catalog area
		SMIWebUserBean userBean = getUserBean();
		CatalogManager catalogManager = userBean.getCatalogManager();
		// Set catalog manager to seeing 'viewable' or 'modifiable' files & directories
		catalogManager.setWriteable(true);
		return catalogManager;
	}

	// invalidate the MongoDB dashboard page cache, if the path ends in .Page
	private void invalidateDashboardCache(SMIWebUserBean ub, String path)
	{
		if (!path.toLowerCase().endsWith(".AdhocReport"))
		{
			DashboardMongoDBCache.createMarkerFile(ub);
		}
	}
	
	/**
	 * Uploads a new file named 'filename' in the specified directory.
	 * 
	 * @param uploadFolder is the name that the subdirectory to which the file will be written.
	 * @param filename is the name of the file that will be written.	
	 * @param imageByte is the bytes of the file to be written.
	 * @return a string-ified version of the BirstWebServiceResult for the file upload operation.
	 */
	public OMElement uploadReportPathImageFile(final String uploadFolder, 
			                    final String filename,			                     
			                    final byte[] imageByte) {
		if (logger.isTraceEnabled())
		{
			logger.trace("'uploadReportPathImageFile().uploadFolder': " + uploadFolder);
			logger.trace("'uploadReportPathImageFile().filename': " + filename);			
			logger.trace("'uploadReportPathImageFile().imageByte': " + imageByte);
		}
		BirstWebServiceResult result = uploadReportPathImageFileResult(uploadFolder, filename, imageByte);
		return result.toOMElement();
	}

	/**
	 * Uploads a new file named 'filename' in the specified directory. Mainly
	 * exists so that unit tests can access and verify the web service result
	 * more easily than querying the string-ified version of the result.
	 * 
	 * @param uploadFolder is the name that the subdirectory to which the file will be written.
	 * @param filename is the name of the file that will be written.
	 * @param digest is the SHA256 digest of the encoded file contents.
	 * @param imageByte is the bytes of the file to be written.
	 * @return the BirstWebServiceResult for the file upload operation.
	 */
	protected BirstWebServiceResult uploadReportPathImageFileResult(final String uploadFolder,
			                                         final String filename, 			                                         
			                                         final byte[] imageByte) {
		if (logger.isTraceEnabled())
		{
			logger.trace("'uploadReportPathImageFile().uploadFolder': " + uploadFolder);
			logger.trace("'uploadReportPathImageFile().filename': " + filename);			
			logger.trace("'uploadReportPathImageFile().imageByte': " + imageByte);
		}
		BirstWebServiceResult uploadReportPathImageFileResult = new BirstWebServiceResult();

		// Check that the user is logged in and there's a current session
		if (!isLoggedIn()) {
			return notLoggedInResult("uploadReportPathImageFileResult()", uploadReportPathImageFileResult);
		}

		SMIWebUserBean userBean = SMIWebUserBean.getUserBean();
		if (!userBean.isSuperUserOwnerOrAdmin() && !userBean.hasEditReportCatalog())
		{
			return notAdminResult("uploadReportPathImageFileResult", uploadReportPathImageFileResult);
		}

		// Get user's catalog area
		CatalogManager catalogManager = userBean.getCatalogManager();
		// Set catalog manager to seeing 'viewable' or 'modifiable' files & directories
		catalogManager.setWriteable(true);	
		
		try {
			fileOperationErrorMessage = catalogManager.writeFile(uploadFolder, filename, imageByte,
					userBean.getUserGroups(), userBean.isSuperUserOwnerOrAdmin());
			invalidateDashboardCache(userBean, filename);
		} catch (CatalogAccessException e) {
			fileOperationErrorCode = 403;
			if (e.getReason().equals(CatalogAccessException.Reason.Error)) {
				fileOperationErrorMessage = e.getMessage();
			} else {
				fileOperationErrorMessage = "Insufficient permissions to complete request.";
			}
			logger.warn(fileOperationErrorCode + ": " + fileOperationErrorMessage);
			if (logger.isTraceEnabled())
				logger.trace(e);
		}
		// Re-retrieve the newly updated catalog file system
		BirstWebServiceResult filesystemResult = getExtendedVisibleFilesystemResult(true);
		// Pick up any error messages from previous file operations (moveFile(), renameFile(),...)
		if (fileOperationErrorCode != null) {
			filesystemResult.setErrorCode(fileOperationErrorCode);
		}
		if (fileOperationErrorMessage != null) {
			filesystemResult.setErrorMessage(fileOperationErrorMessage);
		}
		// Reset the error fields
		fileOperationErrorCode = null;
		fileOperationErrorMessage = null;

		return filesystemResult;
	}

}
