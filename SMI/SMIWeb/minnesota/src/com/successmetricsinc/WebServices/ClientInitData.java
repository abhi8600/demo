/**
 * $Id: ClientInitData.java,v 1.45 2012-12-06 01:39:02 BIRST\sfoo Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */


package com.successmetricsinc.WebServices;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import javax.jws.WebService;

import net.sf.jasperreports.engine.fonts.FontFamily;
import net.sf.jasperreports.extensions.ExtensionsEnvironment;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.OMText;
import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.Session;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.WebServices.dashboard.DashboardWebServiceHelper;
import com.successmetricsinc.adhoc.AdhocReport;
import com.successmetricsinc.adhoc.AdhocReportHelper;
import com.successmetricsinc.sfdc.Chatter;
import com.successmetricsinc.util.UserNotLoggedInException;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XmlUtils;

/**
 * @author agarrison
 *
 */
@WebService( serviceName="ClientInitData")
public class ClientInitData extends BirstWebService {
	private static final String PERM_ENABLE_ASYNC_REPORT_GENERATION = SMIWebUserBean.PERM_ENABLE_ASYNC_REPORT_GENERATION;
	private static final String PERM_IS_DISCOVER_FREE_TRIAL = SMIWebUserBean.PERM_IS_DISCOVER_FREE_TRIAL;
	public static final String PERM_IS_OLAP_CUBE = SMIWebUserBean.PERM_IS_OLAP_CUBE;
	public static final String PERM_EDIT_REPORT_CATALOG = SMIWebUserBean.PERM_EDIT_REPORT_CATALOG;
	private static final String PERM_MODIFY_SAVED_EXPRESSIONS = SMIWebUserBean.PERM_MODIFY_SAVED_EXPRESSIONS;
	private static final String USERNAME = "Username";
	private static final String EL_PERMISSIONS = "Permissions";
	public static final String PERM_ADHOC = SMIWebUserBean.PERM_ADHOC;
	public static final String PERM_EDIT_DASHBOARD = SMIWebUserBean.PERM_EDIT_DASHBOARD;
	public static final String PERM_ENABLE_DOWNLOAD = SMIWebUserBean.PERM_ENABLE_DOWNLOAD;
	private static final String PERM_ENABLE_SELF_SCHEDULE = SMIWebUserBean.PERM_ENABLE_SELF_SCHEDULE;
	private static final String SETTINGS = "Settings";
	private static final String PLUGINS = "Plugins";
	private static final String PAGE_TITLE = "PageTitle";
	private static final String PERM_PROMPT_GROUP_ALLOWED = "PromptGroupAllowed";
	public static final String OVERRIDE_VARIABLES_ROOT = "OverrideVariables";
	public static final String OVERRIDE_VARIABLE = "Variable";
	public static final String OVERRIDE_VARIABLE_NAME = "Name";
	public static final String OVERRIDE_VARIABLE_VALUE_TYPE = "ValueType";
	public static final String OVERRIDE_VARIABLE_VALUES_ROOT = "Values";
	public static final String OVERRIDE_VARIABLE_VALUE = "Value";
	
	public static final String COLLISION_STATUS = "Collision";
	
	private static final String PERM_ENABLE_ADVANCED_CHARTING = "EnableAdvancedCharting";
	private static final String PERM_DISCOVERY_SPACE = "DiscoverySpace";
	private static final String PERM_FREE_TRIAL = "FreeTrial";
	private static final String PERM_CHATTER_ALLOWED = "EnableChatter";
	private static final String PERM_ENABLE_VISUALIZER = "EnableVisualizer";

	// negative ACLs
	private static final String MAKE_SHARED_FOLDER_THE_ROOT_FOR_FILE_OPERATIONS = "MakeSharedFolderTheRootForFileOperations";
	private static final String DISABLE_PIVOT_CONTROL_IN_DASHLETS = "DisablePivotControlInDashlets";
	private static final String DISABLE_LAYOUT_IN_DESIGNER = "DisableLayoutInDesigner";

	// positive ACLs passed to client
	private static final String ENABLE_ALL_FOLDERS_FOR_FILE_OPERATIONS = "EnableAllFoldersForFileOperations";
	private static final String ENABLE_PIVOT_CONTROL_IN_DASHLETS = "EnablePivotControlInDashlets";

	private static final String IS_ADMIN = "IsAdmin";
	
	private static String[] US_TIME_ZONE_IDS = 
		{ "America/New_York", "America/Chicago", "America/Denver", "America/Los_Angeles", "America/Anchorage", "America/Adak",  "America/Phoenix", "Pacific/Honolulu", "Pacific/Apia"};
	
	private static Logger logger = Logger.getLogger(ClientInitData.class);
	
	public OMElement getLocaleInfo(){
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			
			com.successmetricsinc.UserBean ub = getUserBean();
			
			OMFactory factory = OMAbstractFactory.getOMFactory();
	
			OMElement root = factory.createOMElement("LocaleInfo", null);
			
			//Doesn't exactly belong here, but we should refactor the name getLocaleInfo() if we add more of this additional 
			//non locale related stuff.
			setChildParamValue(factory, root, "UseNewQueryLang", Boolean.toString(ub.getRepository().isUseNewQueryLanguage()));
			
			setChildParamValue(factory, root, "DateFormat", "MM/DD/YYYY"); // bug # BPD-1235, BPD-1190 cause - ub.getDateFormat() returns incorrectly M/D/Y
			setChildParamValue(factory, root, "DateTimeFormat", ub.getDateTimeFormat().toPattern());
			
			TimeZone processingTimeZone = SMIWebUserBean.getUserBean().getRepository().getServerParameters().getProcessingTimeZone();
			TimeZone displayTimeZone = ub.getTimeZone();
			
			Date current = new Date();
			SimpleDateFormat fmtProc = new SimpleDateFormat("Z");
			fmtProc.setTimeZone(processingTimeZone);
			
			setChildParamValue(factory, root, "ApplyUSDSTOffset", Boolean.toString(applyUSTimeZoneDSTOffset(processingTimeZone, displayTimeZone)));
			setChildParamValue(factory, root, "ProcTimeZoneUTC", "UTC" + fmtProc.format(current));				
			setChildParamValue(factory, root, "ProcTimeZoneUTCOffset", Integer.toString(ub.getProcessingTimeZoneOffset()));
		
			fmtProc.setTimeZone(SMIWebUserBean.getUserBean().getTimeZone());
			setChildParamValue(factory, root, "DispTimeZoneUTC", "UTC" + fmtProc.format(current));			
			setChildParamValue(factory, root, "DispTimeZoneUTCOffset", Integer.toString(ub.getDisplayTimeZoneOffset()));

			//Which side to apply timezone compensation on in case of two US timezones, one with DST and the other not
			//eg - Arizona (no DST) vs California (DST)
			String applyUSDSTOn = this.applyUS2USDstOn(processingTimeZone, displayTimeZone);
			if(applyUSDSTOn != null){
				setChildParamValue(factory, root, "ApplyUSTZCompensationOn", applyUSDSTOn);
			}
			
			result.setResult(new XMLWebServiceResult(root));
		}catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	private boolean applyUSTimeZoneDSTOffset(TimeZone processing, TimeZone display){
		if(processing.hasSameRules(display)){
			return false;
		}
		
		if(isUSTimeZone(processing) && isUSTimeZone(display)){
			if((processing.useDaylightTime() && !display.useDaylightTime()) ||
			    (!processing.useDaylightTime() && display.useDaylightTime())){
				return true;
			}
			return false;
		}
		
		if(isUSTimeZone(processing) && processing.useDaylightTime() && !isUSTimeZone(display)){
			return true;
		}
		
		return false;
	}
	
	private String applyUS2USDstOn(TimeZone processing, TimeZone display){
		if(isUSTimeZone(processing) && isUSTimeZone(display)){
			if(processing.useDaylightTime() && !display.useDaylightTime()){
				return "Processing";
			}
			if(!processing.useDaylightTime() && display.useDaylightTime()){
				return "Display";
			}
		}
		return null;
	}
	
	private boolean isUSTimeZone(TimeZone tz){
		String id = tz.getID();
		
		for(int i = 0; i < US_TIME_ZONE_IDS.length; i++){
			if(id.equals(US_TIME_ZONE_IDS[i])){
				return true;
			}
		}
		
		return false;
	}
	
	private void setChildParamValue(OMFactory factory, OMElement root, String param, String value){
		OMElement el = factory.createOMElement(param, null);
		el.setText(value);
		root.addChild(el);
	}
	
	@SuppressWarnings("rawtypes")
	public OMElement getFontFamilies() {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			List families = ExtensionsEnvironment.getExtensionsRegistry().getExtensions(FontFamily.class);
			StringBuilder builder = new StringBuilder();
			for (Iterator itf = families.iterator(); itf.hasNext();)
			{
				FontFamily family = (FontFamily)itf.next();
				if (builder.length() > 0) {
					builder.append(',');
				}
				builder.append(family.getName());
			}
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMText text = factory.createOMText(builder.toString());
			result.setResult(new XMLWebServiceResult(text));
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	// GS 
	// Changing the module path to not use /SMIWeb/swf. instead just use swf (directory on Birst Application)
	public OMElement getPermissionsAndOverrides() {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			StringBuilder builder = new StringBuilder();
			SMIWebUserBean userBean = getUserBean();
			builder.append("<Modules>");
			if (userBean.getCanDownload()) {
				builder.append("<Module name=\"/swf/GeneratePdfCsvModule\" location=\"farRightMenubar\"/>");
			}
			// right now always show subject area.
			builder.append("<Module name=\"/swf/SubjectAreaModule\" location=\"subjectArea\"/>");

			if (!userBean.isDiscoverySpace() && (userBean.isOwner() || !userBean.isEmbedded())) {
				builder.append("<Module name=\"/swf/LayoutModule\" location=\"mainTab\"/>");
			}
			builder.append("</Modules>");
			
			builder.append("<").append(PERM_ENABLE_DOWNLOAD).append(">");
			builder.append(Boolean.toString(userBean.hasRole(PERM_ENABLE_DOWNLOAD)));
			builder.append("</").append(PERM_ENABLE_DOWNLOAD).append(">");
			builder.append("<").append(PAGE_TITLE).append(">").append(userBean.getPageTitle()).append("</").append(PAGE_TITLE).append(">");
			
			builder.append("<").append(PERM_ENABLE_SELF_SCHEDULE).append(">");
			builder.append(Boolean.toString(userBean.isFreeTrial() ? false : (userBean.isOwner() ? true : userBean.hasRole(PERM_ENABLE_SELF_SCHEDULE))));
			builder.append("</").append(PERM_ENABLE_SELF_SCHEDULE).append(">");
			
			builder.append("<").append(PERM_MODIFY_SAVED_EXPRESSIONS).append('>');
			builder.append(Boolean.toString(userBean.isFreeTrial() ? false : (userBean.isOwner() ? true : userBean.hasRole(PERM_MODIFY_SAVED_EXPRESSIONS))));
			builder.append("</").append(PERM_MODIFY_SAVED_EXPRESSIONS).append(">");
			
			builder.append("<").append(ENABLE_ALL_FOLDERS_FOR_FILE_OPERATIONS).append('>');
			builder.append(Boolean.toString(userBean.isOwner() ? true : !userBean.hasRole(MAKE_SHARED_FOLDER_THE_ROOT_FOR_FILE_OPERATIONS)));
			builder.append("</").append(ENABLE_ALL_FOLDERS_FOR_FILE_OPERATIONS).append(">");
			
			builder.append("<").append(PERM_ENABLE_ADVANCED_CHARTING).append('>');
			builder.append(Boolean.toString(userBean.isSuperUser() || userBean.hasRole(PERM_ENABLE_ADVANCED_CHARTING)));
			builder.append("</").append(PERM_ENABLE_ADVANCED_CHARTING).append(">");
			
			builder.append("<").append(PERM_DISCOVERY_SPACE).append('>');
			builder.append(Boolean.toString(userBean.isDiscoverySpace()));
			builder.append("</").append(PERM_DISCOVERY_SPACE).append(">");
			
			builder.append("<").append(PERM_FREE_TRIAL).append('>');
			builder.append(Boolean.toString(userBean.isFreeTrial()));
			builder.append("</").append(PERM_FREE_TRIAL).append(">");

			builder.append("<").append(PERM_EDIT_REPORT_CATALOG).append('>');
			builder.append(Boolean.toString(userBean.isFreeTrial() ? false : (userBean.isOwner() ? true : userBean.hasRole(PERM_EDIT_REPORT_CATALOG))));
			builder.append("</").append(PERM_EDIT_REPORT_CATALOG).append(">");

			Repository rep = userBean.getRepository();
			builder.append("<").append(PERM_IS_OLAP_CUBE).append('>');
			builder.append(Boolean.toString(rep.getDimensionalStructure() == 1));
			builder.append("</").append(PERM_IS_OLAP_CUBE).append(">");
			
			builder.append('<').append(PERM_IS_DISCOVER_FREE_TRIAL).append('>');
			builder.append(Boolean.toString(userBean.isDiscoveryFreeTrial()));
			builder.append("</").append(PERM_IS_DISCOVER_FREE_TRIAL).append(">");
			
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMText text = factory.createOMText(builder.toString());
			
			result.setResult(new XMLWebServiceResult(text));
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement getDashboardSettings(){
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean userBean = getUserBean();
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMElement root = factory.createOMElement(SETTINGS, null);
			
			//Permissions
			OMElement permissions = createDashboardPermissions(userBean);
			root.addChild(permissions);
			
			//Style Settings
			try{
				OMElement styles = DashboardWebServiceHelper.getDashboardStyleSettings(userBean);
				if(styles != null){
					root.addChild(styles);
				}
			}catch(Exception ex){
				logger.error("Unable to get dashboard style settings", ex);
			}
			
			//Plugin settings
			try{
				OMElement plugins = DashboardWebServiceHelper.getDashboardDashletContainerPlugins(userBean);
				if(plugins != null){
					root.addChild(plugins);
				}
			}catch(Exception e){
				logger.error("Unable to get dashboard container plugin settings", e);
			}
			
			result.setResult(new XMLWebServiceResult(root));
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	public OMElement getDashboardPermissions(){
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean userBean = getUserBean();
			OMElement root = createDashboardPermissions(userBean);
			result.setResult(new XMLWebServiceResult(root));
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement getDefaultReportProperties() {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			AdhocReport report = AdhocReport.getDefaultReportProperties(); 
			result.setResult(report);
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement getCollisionStatus()
	{
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean userBean = getUserBean();
			Repository repository = userBean.getRepository();
			OMFactory factory = OMAbstractFactory.getOMFactory();
			boolean collision = repository != null ? repository.anyCollisions() : false;
			OMElement root = factory.createOMElement(COLLISION_STATUS, null);
			OMText text = factory.createOMText(Boolean.toString(collision));
			root.addChild(text);
			result.setResult(new XMLWebServiceResult(root));
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement getOverrideVariables()
	{
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean userBean = getUserBean();
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMElement root = factory.createOMElement(OVERRIDE_VARIABLES_ROOT, null);
			List<String> overrideVariableNames = userBean.getOverridenSessionVariables();
			if(overrideVariableNames != null && overrideVariableNames.size() > 0)
			{
				Session session = userBean.getRepSession(false);
				if(session != null)
				{
					for(String sessionVariableName : overrideVariableNames)
					{
						OMElement variableRoot = factory.createOMElement(OVERRIDE_VARIABLE, null);
						XmlUtils.addContent(factory, variableRoot, OVERRIDE_VARIABLE_NAME, sessionVariableName, null);
						OMElement variableValuesRoot = factory.createOMElement(OVERRIDE_VARIABLE_VALUES_ROOT, null);
						
						String valueType = null;
						Object object = session.getVariable(sessionVariableName);
						if(object instanceof ArrayList && object != null)
						{
							List<Object> list = (ArrayList<Object>)object;
							if(list != null && list.size() > 0)
							{
								// All values in the list are of same type
								valueType = this.getVariableValueType(list.get(0));
								if(valueType != null)
								{	
									XmlUtils.addContent(factory, variableRoot, OVERRIDE_VARIABLE_VALUE_TYPE, valueType, null);
									for(Object listObj : list)
									{
										OMElement elem = getVariableValueElement(userBean, listObj);
										if(elem != null)
										{
											variableValuesRoot.addChild(elem);
										}
									}
								}
							}
						}
						else 
						{
							valueType = this.getVariableValueType(object);
							if(valueType != null)
							{
								XmlUtils.addContent(factory, variableRoot, OVERRIDE_VARIABLE_VALUE_TYPE, valueType, null);
								OMElement elem = getVariableValueElement(userBean, object);
								if(elem != null)
								{
									variableValuesRoot.addChild(elem);
								}
							}
						}
						
						variableRoot.addChild(variableValuesRoot);
						root.addChild(variableRoot);
					}
				}
			}
			 
			result.setResult(new XMLWebServiceResult(root));
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement getFlashVars() {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMElement valueElement = factory.createOMElement("FlashVars", null);
			SMIWebUserBean user = SMIWebUserBean.getUserBean();
			if (user != null) {
				if (user.getAcornRoot() != null)
					valueElement.addAttribute("acornRoot", user.getAcornRoot(), null);
				if (user.getAspSessionId() != null)
					valueElement.addAttribute("aspNetSessionId", user.getAspSessionId(), null);
				if (user.getSchedulerRoot() != null)
					valueElement.addAttribute("schedulerRoot", user.getSchedulerRoot(), null);
				valueElement.addAttribute("bingMaps", Boolean.toString(user.isHasBingMaps()), null);
				if (user.getGaTrackingAccount() != null)
					valueElement.addAttribute("gaAccount", user.getGaTrackingAccount(), null);
				if (user.getJSessionId() != null)
					valueElement.addAttribute("jsessionId", user.getJSessionId(), null);
				if (user.getAspxFormsAuth() != null)
					valueElement.addAttribute("aspxFormsAuth", user.getAspxFormsAuth(), null);
				Repository rep = user.getRepository();
				if (rep != null)
					valueElement.addAttribute(PERM_IS_OLAP_CUBE, Boolean.toString(rep.getDimensionalStructure() == 1), null);
			}
			
			result.setResult(new XMLWebServiceResult(valueElement));
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	private OMElement getVariableValueElement(SMIWebUserBean userBean, Object object)
	{	
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement valueElement = factory.createOMElement(OVERRIDE_VARIABLE_VALUE, null);
		String value = Util.convertToString(object, false, userBean.getRepository(), true);
		OMText text = factory.createOMText(value);
		valueElement.addChild(text);
		return valueElement;
	}
	
	private String getVariableValueType(Object object)
	{
		String type = null;
		if (object instanceof String)
		{
			type = "String"; 
		}
		else if(object instanceof Integer)
		{
			type = "Integer";
		}
		else if(object instanceof Long)
		{
			type = "Long";
		}
		else if(object instanceof Double)
		{
			type = "Double";
		}
		else if( object instanceof Date)
		{
			type = "Date";
		}
		else if( object instanceof Calendar)
		{
			type = "Calender";
		}
			
		if(type == null)
		{
			logger.warn("Unable to get the correct value type for the variable " + object.toString());
		}
		
		return type;
	}
	
	private OMElement createDashboardPermissions(SMIWebUserBean userBean){
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;
		OMElement root = factory.createOMElement(EL_PERMISSIONS, ns);
		
		//Username
		OMElement username = factory.createOMElement(USERNAME, ns);
		username.setText(userBean.getUserName());
		root.addChild(username);
		
		//User is administrator?
		OMElement admin = factory.createOMElement(IS_ADMIN, ns);
		admin.setText(Boolean.toString(userBean.isAdministrator()));
		root.addChild(admin);
		
		//User permissions
		setPermissionElement(factory, root, userBean, PERM_ADHOC);
		setPermissionElement(factory, root, userBean, PERM_EDIT_DASHBOARD);
		setPermissionElement(factory, root, userBean, PERM_ENABLE_DOWNLOAD);
		setPermissionElement(factory, root, userBean, PERM_PROMPT_GROUP_ALLOWED);
		setPermissionElement(factory, root, userBean, PERM_EDIT_REPORT_CATALOG, true);
		
		// chatter allowed - see if there is a valid SFDC session (only true when AppExchangeSSO is used)
		try
		{
			OMElement element = factory.createOMElement(PERM_CHATTER_ALLOWED, null);
			Chatter ch = new Chatter(userBean.getRepository(), userBean.getSFDC_sessionId(), userBean.getSFDC_serverURL());
			boolean chatterAllowed = ch.validSession(userBean.getSFDC_sessionId(), userBean.getSFDC_serverURL());
			element.setText(Boolean.toString(chatterAllowed));
			root.addChild(element);
		}
		catch (Throwable ex)
		{
			logger.debug(ex, ex);
		}

		OMElement element = factory.createOMElement(PERM_FREE_TRIAL, null);
		element.setText(Boolean.toString(userBean.isFreeTrial()));
		root.addChild(element);
		
		element = factory.createOMElement(PERM_IS_DISCOVER_FREE_TRIAL, null);
		element.setText(Boolean.toString(userBean.isDiscoveryFreeTrial()));
		root.addChild(element);
		
		if (userBean.isFreeTrial()) {
			element = factory.createOMElement(PERM_ENABLE_SELF_SCHEDULE, null);
			element.setText(Boolean.FALSE.toString());
			root.addChild(element);
		}
		else
			setPermissionElement(factory, root, userBean, PERM_ENABLE_SELF_SCHEDULE, true);
		
		element = factory.createOMElement(PERM_ENABLE_VISUALIZER, null);
		element.setText(Boolean.toString(userBean.isVisualizerEnabled()));
		root.addChild(element);
		
		setNegativePermissionElement(factory, root, userBean, ENABLE_ALL_FOLDERS_FOR_FILE_OPERATIONS, MAKE_SHARED_FOLDER_THE_ROOT_FOR_FILE_OPERATIONS, true);
		setNegativePermissionElement(factory, root, userBean, ENABLE_PIVOT_CONTROL_IN_DASHLETS, DISABLE_PIVOT_CONTROL_IN_DASHLETS, true);

		element = factory.createOMElement(ClientInitData.PERM_ENABLE_ASYNC_REPORT_GENERATION, null);
		element.setText(Boolean.toString(AdhocReportHelper.isAsynchReportGenerationEnabled() && userBean.isAsyncReportGenerationEnabled()));
		root.addChild(element);
		
		return root;
	}
	
	private void setPermissionElement(OMFactory factory, OMElement root, SMIWebUserBean userBean, String role){
		setPermissionElement(factory, root, userBean, role, false);
	}
	
	private void setPermissionElement(OMFactory factory, OMElement root, SMIWebUserBean userBean, String role, boolean checkForOwner){
		OMElement element = factory.createOMElement(role, null);
		
		boolean hasRole = false;
		if(checkForOwner && userBean.isOwner()){
			hasRole = true;
		}
		
		if(!hasRole){
			hasRole = userBean.hasRole(role);
		}
		
		element.setText(Boolean.toString(hasRole));
//		element.setText(Boolean.toString(userBean.hasRole(role)));
		root.addChild(element);
	}
	
	// handle negative ACLs that we turn in to client side positive ACLs
	private void setNegativePermissionElement(OMFactory factory, OMElement root, SMIWebUserBean userBean, String posRole, String negRole, boolean checkForOwner){
		OMElement element = factory.createOMElement(posRole, null);
		
		boolean hasNegRole = userBean.hasRole(negRole);
		boolean hasPosRole = !hasNegRole;
		
		if(!hasPosRole && checkForOwner && userBean.isOwner())
		{
			hasPosRole = true;
		}

		element.setText(Boolean.toString(hasPosRole));
		root.addChild(element);
	}
}
