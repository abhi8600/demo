package com.successmetricsinc.WebServices.dashboard;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMAttribute;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.apache.log4j.Logger;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.successmetricsinc.Prompt;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.WebServices.BirstWebServiceResult;
import com.successmetricsinc.adhoc.AdhocReport;
import com.successmetricsinc.catalog.CatalogAccessException;
import com.successmetricsinc.catalog.CatalogManager;
import com.successmetricsinc.chart.ChartColors;
import com.successmetricsinc.chart.ChartOptions;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.FileUtils;
import com.successmetricsinc.util.QuickDashboard;
import com.successmetricsinc.util.QuickReport;
import com.successmetricsinc.util.SwapSpaceInProgressException;
import com.successmetricsinc.util.XmlUtils;
import com.successmetricsinc.util.QuickDashboard.QuickPrompt;

public class DashboardWebServiceHelper {
	private static Logger logger = Logger.getLogger(DashboardWebServiceHelper.class);
	private static final String UUID = "uuid";
	private static final String DOT_PAGE = ".page";
	private static final String PARTIAL = "partial";
	private static final String DASHLETPLUGIN_CFG = File.separator + "config" + File.separator + "DashletContainerPlugins.xml";
	
	public static OMElement getDashboardStyleSettings(SMIWebUserBean userBean)throws XMLStreamException, FileNotFoundException, UnsupportedEncodingException{
		String configPath = DashboardWebServiceHelper.defaultStyleConfigPath(userBean.getCatalogManager());
		File cfg = new File(configPath);
		if(!cfg.exists()){
			return null;
		}
		
		return readXmlFile(configPath);
	}
	
	public static OMElement getDashboardDashletContainerPlugins(SMIWebUserBean userBean)throws XMLStreamException, FileNotFoundException, UnsupportedEncodingException{
		String acornRoot = userBean.getAcornRoot();
		if(acornRoot != null){
			String configFilePath = acornRoot + DASHLETPLUGIN_CFG;
			File cfg = new File(configFilePath);
			if(!cfg.exists()){
				return null;
			}
			return readXmlFile(configFilePath);
		}
		return null;
	}
	
	public static OMElement readXmlFile(String absFilePath) throws XMLStreamException, FileNotFoundException, UnsupportedEncodingException{
		OMElement root = null;
		
		FileInputStream fis = null;
		InputStreamReader is = null;
		XMLStreamReader reader = null;
		StAXOMBuilder builder = null;
		
		try{
			//Read contents of file
			fis = new FileInputStream(absFilePath);
			is = new InputStreamReader(fis, "UTF-8");
			XMLInputFactory inputFactory = XmlUtils.getSecureXMLInputFactory();
            reader = inputFactory.createXMLStreamReader(is);

			builder = new StAXOMBuilder(reader);
			root = builder.getDocumentElement();
			root.detach();
		}finally{
			if(fis != null){
				try{
					fis.close();
				}catch(Exception e){}
			}
			if(is != null){
				try{
					is.close();
				}catch(Exception e){}
			}
			if(reader != null){
				try{
					reader.close();
				}catch(Exception e){}
			}
			if(builder != null){
				try{
					builder.close();
				}catch(Exception e){}
			}
		}
		return root;
	}
	
	public static void writeXmlFile(String filepath, OMElement xml) throws XMLStreamException, IOException{
		FileOutputStream out = null;
		OutputStreamWriter os = null;
		XMLStreamWriter writer = null;
		try{
			out = new FileOutputStream(filepath);
			os = new OutputStreamWriter(out, "UTF-8");
			writer = XMLOutputFactory.newInstance().createXMLStreamWriter(os);
			xml.serialize(writer); 
			writer.flush();
		}catch(IOException e){
			throw e;
		}finally{
			if(writer != null){
				try{
					writer.close();
				}catch(Exception e){}
			}
			if(os != null){
				try{
					os.close();
				}catch(Exception e){}
			}
			if(out != null){
				try{
					out.close();
				}catch(Exception e){}
			}
		}
	}
	
	public static String getParentPath(File dir, CatalogManager manager){
		//Get the relative path
		String parentDir = dir.getParent();
		String root = manager.getCatalogRootPath();
		int idx = parentDir.indexOf(root);
		if( idx == 0){
			if (parentDir.length() > root.length()){
				parentDir = parentDir.substring(manager.getCatalogRootPath().length() + 1);
				parentDir = parentDir.replace('\\', '/');
			}else{
				parentDir = "";
			}
		}
		return parentDir;
	}
	
	public static List<String> getDashboardFoldersFromCM(SMIWebUserBean ub) throws SwapSpaceInProgressException{
		// bug 6851
		boolean su = ub.isSuperUser();
		ub.setSuperUser(false);
		ub.setRootDirectories(null);
		//logger.debug("getDashboardListOperation() start getDashboardFolders()");
		long start = System.currentTimeMillis();
		List<String> dashboards = ub.getCatalogManager().getDashboardFolders(ub);
		logger.debug("getDashboards() time = " + (System.currentTimeMillis() - start));
		//logger.debug("getDashboardListOperation() end getDashboardFolders()");
		if (su) {
			ub.setSuperUser(su);
			ub.setRootDirectories(null);
		}
		return dashboards;
	}
	
	public static Map<String, List<File>> getDashboardNameToDirectories(List<String> dashboards, CatalogManager cm, BasicDBObject dbObj){
		Map<String, List<File>> dashboardPages = new HashMap<String, List<File>>();
		for (String dash : dashboards) {
			File folder = new File(cm.getCatalogRootPath() + File.separator + dash);
			if(folder.exists()) {
				String name = dash.substring(dash.lastIndexOf('/') + 1);
				List<File> dirs = dashboardPages.get(name);
				if (dirs == null) {
					dirs = new ArrayList<File>();
					dashboardPages.put(name, dirs);
				}
				dirs.add(folder);
				
				if(dbObj != null){
					BasicDBList paths =  (BasicDBList)dbObj.get(name);
					if(paths == null){
						paths = new BasicDBList();
						dbObj.put(name, paths);
					}
					paths.add(dash);
				}
			}
		}
		return dashboardPages;
	}
	
	public static void addAutoGeneratedDashboards(SMIWebUserBean ub, Map<String, List<File>> dashboardPages, OMElement dashboardList) throws SwapSpaceInProgressException {
		List<QuickDashboard> qbs = ub.getRepository().getQuickDashboards();
		if (qbs != null) {
			for (QuickDashboard qb : qbs) {
				String key = qb.getDashboardName();
				if (! dashboardPages.containsKey(key)) {
					// create this quick dashboard
					createQuickDashboard(qb, ub, dashboardList);
				}
			}
		}
	}
	
	public static boolean createQuickDashboardPageFilesIfNotExist(SMIWebUserBean ub) throws SwapSpaceInProgressException{
		List<QuickDashboard> qbs = ub.getRepository().getQuickDashboards();
		boolean pageCreated = false;
		if (qbs != null) {
			String parentDir = ub.getCatalogManager().getCatalogRootPath() + "/" + CatalogManager.SHARED_DIR + SMIWebUserBean.AUTO_REPORT_DIR;
			for (QuickDashboard qb : qbs) {
				String dashName = FileUtils.sanitizeFileName(qb.getDashboardName());
				File f = new File(parentDir + "/" + dashName + "/" + qb.getPageName() + ".page");
				if(!f.exists()){
					createQuickDashboardPageFile(qb, ub);
					pageCreated = true;
				}
			}
		}
		return pageCreated;
	}
	
	private static OMElement createQuickDashboardPageFile(QuickDashboard qb, SMIWebUserBean ub) throws SwapSpaceInProgressException {
		String dashName = qb.getDashboardName();
		dashName = FileUtils.sanitizeFileName(dashName);
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;
		
		CatalogManager cm = ub.getCatalogManager();
		CatalogManager.checkForSwapSpaceOperation(cm);
		
		String parentDir = cm.getCatalogRootPath() + "/" + CatalogManager.SHARED_DIR + SMIWebUserBean.AUTO_REPORT_DIR;
		String dir = parentDir + "/" + dashName;
		
		try {
			// make sure that the directories exist.
			File f = new File(dir);
			
			f.mkdirs();
			
			cm.createDirectory(dashName, parentDir, ub.getUserGroups(), ub.isSuperUserOwnerOrAdmin());
			File file = new File(dir, DashboardWebService.DASH_XML_FILE);
			file.createNewFile();
		} catch (CatalogAccessException e) {
			logger.error(e, e);
		} catch (IOException e) {
			logger.error(e, e);
		}
		String relativePath = CatalogManager.SHARED_DIR + SMIWebUserBean.AUTO_REPORT_DIR + "/" + dashName + "/";
		Map<String, String> dashlets = new HashMap<String, String>();
		List<String> dashletNames = new ArrayList<String>();
		List<Color> chartColors = new ArrayList<Color>(Arrays.asList(ChartColors.colors));
		for (QuickReport qr : qb.getReports()) {
			String filename = qr.getTitle();
			filename = FileUtils.sanitizeFileName(filename);
			AdhocReport report = new AdhocReport(qr, dashName + "_" + qb.getPageName(), ub.getRepository());
			ChartOptions co = report.getChartOptions();
			if (co != null) {
				co.chartColors = chartColors;
			}
			if (! filename.toUpperCase().endsWith(".ADHOCREPORT")) {
				String namew = filename.concat(".AdhocReport");
				
				filename = namew;
			}
			File file = new File(dir + "/" + filename);
			try {
//				report.compileAndSaveReport(file);
				report.save(file);
			} catch (Exception e) {
				logger.error(e, e);
			}
			dashlets.put(qr.getTitle(), relativePath + filename);
			dashletNames.add(qr.getTitle());
			Color c = chartColors.remove(0);
			chartColors.add(c);
		}
		
		// create page
		int numberOfDashlets = qb.getReports().size();
		OMElement page = factory.createOMElement("Page", ns);
		page.addAttribute("name", qb.getPageName(), ns);
//		page.addAttribute("dir", null, ns);
		XmlUtils.addContent(factory, page, "ScaleDashlets", true, ns);
		
		// add prompts
		OMElement prompts = factory.createOMElement("Prompts", ns);
		for (QuickPrompt qp : qb.getPrompts()) {
			Prompt prompt = new Prompt(qp);
			prompt.addContent(prompts, factory, ns);
		}
		page.addChild(prompts);
		
		
		OMElement topLayout = factory.createOMElement("Layout", ns);
		topLayout.addAttribute("direction", "vertical", ns);
		topLayout.addAttribute("percentWidth", "100", ns);
		topLayout.addAttribute("percentHeight", "100", ns);
		page.addChild(topLayout);
		OMElement firstRowLayout = topLayout;
		if (numberOfDashlets > 1) {
			firstRowLayout = factory.createOMElement("Layout", ns);
			firstRowLayout.addAttribute("direction", "horizontal", ns);
			firstRowLayout.addAttribute("percentWidth", "100", ns);
			if (numberOfDashlets > 2)
				firstRowLayout.addAttribute("percentHeight", "50", ns);
			else
				firstRowLayout.addAttribute("percentHeight", "100", ns);
				
			topLayout.addChild(firstRowLayout);
		}
		OMElement pageLayout = firstRowLayout;
		if (numberOfDashlets > 1) {
			pageLayout = factory.createOMElement("Layout", ns);
			pageLayout.addAttribute("direction", "horizontal", ns);
			pageLayout.addAttribute("percentWidth", "50", ns);
			pageLayout.addAttribute("percentHeight", "100", ns);
			firstRowLayout.addChild(pageLayout);
		}
		pageLayout.addChild(addDashletToLayout(factory, ns, dashlets, dashletNames, 0));
		
		pageLayout = firstRowLayout;
		if (numberOfDashlets > 1) {
			pageLayout = factory.createOMElement("Layout", ns);
			pageLayout.addAttribute("direction", "horizontal", ns);
			pageLayout.addAttribute("percentWidth", "50", ns);
			pageLayout.addAttribute("percentHeight", "100", ns);
			firstRowLayout.addChild(pageLayout);
			pageLayout.addChild(addDashletToLayout(factory, ns, dashlets, dashletNames, 1));
		}
		
		if (numberOfDashlets > 2) {
			firstRowLayout = factory.createOMElement("Layout", ns);
			firstRowLayout.addAttribute("direction", "horizontal", ns);
			firstRowLayout.addAttribute("percentWidth", "100", ns);
			firstRowLayout.addAttribute("percentHeight", "50", ns);
			topLayout.addChild(firstRowLayout);
			
			pageLayout = firstRowLayout;
			if (numberOfDashlets > 3) {
				pageLayout = factory.createOMElement("Layout", ns);
				pageLayout.addAttribute("direction", "horizontal", ns);
				pageLayout.addAttribute("percentWidth", "50", ns);
				pageLayout.addAttribute("percentHeight", "100", ns);
				firstRowLayout.addChild(pageLayout);
			}
			pageLayout.addChild(addDashletToLayout(factory, ns, dashlets, dashletNames, 2));
			if (numberOfDashlets > 3) {
				pageLayout = factory.createOMElement("Layout", ns);
				pageLayout.addAttribute("direction", "horizontal", ns);
				pageLayout.addAttribute("percentWidth", "50", ns);
				pageLayout.addAttribute("percentHeight", "100", ns);
				firstRowLayout.addChild(pageLayout);
				pageLayout.addChild(addDashletToLayout(factory, ns, dashlets, dashletNames, 3));
			}
		}
		FileOutputStream fos = null;
		DataOutputStream dos = null;
		try {
			File file = new File(dir, qb.getPageName() + ".page");
			file.createNewFile();
			fos = new FileOutputStream(file);
			XMLStreamWriter writer = XMLOutputFactory.newInstance().createXMLStreamWriter(fos);
			page.serialize(writer);
			writer.flush();
		}
		catch (Exception e) {
			logger.error(e,e);
		}
		finally {
			if (dos != null) {
				try {
					dos.close();
				} catch (IOException e) {
					logger.error(e, e);
				}
			}
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					logger.error(e, e);
				}
			}
		}
		
		return page;
	}
	
	private static void createQuickDashboard(QuickDashboard qb, SMIWebUserBean ub, OMElement dashboardList) throws SwapSpaceInProgressException {
		String dashName = qb.getDashboardName();
		dashName = FileUtils.sanitizeFileName(dashName);
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;
		
		OMElement dashboard = null;
		for (@SuppressWarnings("rawtypes")
		Iterator iter = dashboardList.getChildrenWithLocalName(DashboardWebService.DASHBOARD); iter.hasNext(); ) {
			OMElement el = (OMElement)iter.next();
			String n = el.getAttributeValue(new QName(DashboardWebService.NAME));
			if (dashName.equals(n)) {
				dashboard = el;
				break;
			}
		}
		
		if (dashboard == null) {
			dashboard = factory.createOMElement(DashboardWebService.DASHBOARD, ns);
			XmlUtils.addAttribute(factory, dashboard, DashboardWebService.NAME, dashName, ns);
			dashboardList.addChild(dashboard);
		}
		
		OMElement page = createQuickDashboardPageFile(qb, ub);
		if(page != null){
			dashboard.addChild(page);
		}
	}
	
	private static OMElement addDashletToLayout(OMFactory factory, OMNamespace ns, Map<String, String> dashlets, List<String> dashletNames, int index) {
		OMElement dash = factory.createOMElement("dashlet", ns);
		XmlUtils.addContent(factory, dash, "Path", dashlets.get(dashletNames.get(index)), ns);
		XmlUtils.addContent(factory, dash, "Title", dashletNames.get(index), ns);
		XmlUtils.addContent(factory, dash, "EnablePDF", Boolean.toString(true), ns);
		XmlUtils.addContent(factory, dash, "EnableCSV", Boolean.toString(true), ns);
		XmlUtils.addContent(factory, dash, "EnableExcel", Boolean.toString(true), ns);
		XmlUtils.addContent(factory, dash, "ApplyAllPrompts", Boolean.toString(true), ns);
		return dash;
	}
	
	public static String readFile(String absFilePath) throws FileNotFoundException, IOException{
		StringBuffer sb = new StringBuffer();
		BufferedReader br = null;
		FileInputStream fis = null;
		InputStreamReader is = null;
		try{
			fis = new FileInputStream(absFilePath);
			is = new InputStreamReader(fis, "UTF-8");
			br = new BufferedReader(is);
			String nextLine = "";
		
			while ((nextLine = br.readLine()) != null) {
				sb.append(nextLine);
			}
		}finally{
			if(br != null){
				try{
					br.close();
				}catch(Exception e){}
			}
			if(is != null){
				try{
					is.close();
				}catch(Exception e){}
			}
			if(fis != null){
				try{
					fis.close();
				}catch(Exception e){}
			}
		}
		
		return sb.toString();
	}
	
	public static Integer getDashboardOrder(File dashFolder) throws UnsupportedEncodingException{
		String dashxml = dashFolder.getAbsolutePath() + File.separator + DashboardWebService.DASH_XML_FILE;
		try{
			 return getOrder(dashxml);
		}catch (FileNotFoundException e) {
			logger.error("Cannot get dashboard / page order from " + dashxml, e);
		} catch (XMLStreamException e) {
			logger.error("Cannot get dashboard / page order from " + dashxml, e);
		}
		
		return 9999;
	}
	
	public static Integer getOrder(String file) throws FileNotFoundException, XMLStreamException, UnsupportedEncodingException{
		OMElement root = null;
		
		//Bad file, put to end..
		File target = new File(file);
		if(!target.exists() || (target.exists() && target.length() == 0)){
			return 9999;
		}else{
			root = readXmlFile(file);
		}
		OMElement orderEl = root.getFirstChildWithName(new QName("", "Order"));	
		if(orderEl == null){
			return 9999;
		}
		
		try{
			return Integer.parseInt(orderEl.getText());
		}catch(NumberFormatException ex){
			return 9999;
		}
	}
	
	public static String addUuidAttrIfNotExists(BasicDBObject pageObj){
		String uuidAttr = "@uuid";
		String uuid = pageObj.getString(uuidAttr);
		if(uuid == null || uuid.equals("null")){
			uuid = java.util.UUID.randomUUID().toString();
			pageObj.put(uuidAttr, uuid);
		}
		return uuid;
	}
	
	public static String addUuidAttrIfNotExists(OMElement el){
		String uuid = el.getAttributeValue(qname(UUID));
		if(uuid == null || uuid.equals("null")){
			uuid = java.util.UUID.randomUUID().toString();
			el.addAttribute(UUID, uuid, null);
		}
		return uuid;
	}
	
	public static String addUuidAttribute(OMElement el){
		String uuid = java.util.UUID.randomUUID().toString();
		el.addAttribute(UUID, uuid, null);
		return uuid;
	}
	
	public static void recreateUuidAttr(OMElement el){
		OMAttribute uidAttr = el.getAttribute(qname(UUID));
		if(uidAttr == null){
			addUuidAttribute(el);
		}else{
			uidAttr.setAttributeValue(java.util.UUID.randomUUID().toString());
		}
	}
	
	public static QName qname(String name){
		return new QName("", name);
	}
	
	public static String defaultStyleConfigPath(CatalogManager cm){
		return catalogParentDir(cm) + File.separator + "DashboardStyleSettings.xml";
	}
	
	public static String catalogParentDir(CatalogManager cm){
		return cm.getCatalogRootPath() + File.separator + "..";
	}
	
	public static String templateDir(CatalogManager cm){
		return catalogParentDir(cm) + File.separator + "DashboardTemplates";
	}
	
	public static OMElement getStyleTemplates(BirstWebServiceResult result, SMIWebUserBean userBean, String acornRoot) throws FileNotFoundException, IOException, XMLStreamException{
		OMFactory factory = OMAbstractFactory.getOMFactory();
		
		OMElement root = factory.createOMElement(qname("Templates"));
		
		OMElement defaultDef = factory.createOMElement(qname("Default"));
		root.addChild(defaultDef);
		
		CatalogManager cm = userBean.getCatalogManager();
		
		//Default
		OMElement defaultApplied = DashboardWebServiceHelper.getDashboardStyleSettings(userBean);
		if(defaultApplied != null){
			defaultDef.addChild(defaultApplied);
			OMAttribute nameAttr = defaultApplied.getAttribute(qname("name"));
			if(nameAttr == null){ //previous generation 
				defaultApplied.addAttribute(factory.createOMAttribute("name", null, "Previously saved"));
				defaultApplied.addAttribute(factory.createOMAttribute("type", null, "user"));
				if(!writeStyleTemplate(result, cm, defaultApplied, false)){
					return null; //Failed to write back default
				}
				if(!writeStyleTemplate(result, cm, defaultApplied, true)){
					return null; //Failed to write back to user template
				}
			}
		}
		
		//System
		OMElement system = DashboardWebServiceHelper.templateStyleFileList("System", acornRoot + "/config/DashboardStyles", "sys");
		root.addChild(system);
		
		//User defined
		OMElement user = DashboardWebServiceHelper.templateStyleFileList("User", templateDir(cm), "user");
		root.addChild(user);
	
		return root;
	}
	
	public static boolean writeStyleTemplate(BirstWebServiceResult result, CatalogManager cm, OMElement doc, boolean saveAsTemplateOnly) throws XMLStreamException, IOException{
		OMAttribute nameAttr = doc.getAttribute(qname("name"));
		
		String name = nameAttr.getAttributeValue();
		String templateDirPath = DashboardWebServiceHelper.templateDir(cm);
		File templateDir = new File(templateDirPath);
		if(!templateDir.exists()){
			if(!templateDir.mkdir()){
				result.setErrorCode(BaseException.ERROR_PERMISSION);
				result.setErrorMessage("Cannot create template directory");
				return false;
			}
		}
		
		String sanitizedName = name.replace("/", "###");
		
		if(saveAsTemplateOnly){
			String templateFilePath = templateDirPath + File.separator + sanitizedName + ".style";
			DashboardWebServiceHelper.writeXmlFile(templateFilePath, doc);
		}else{
			//This is the default to be applied
			String configPath = DashboardWebServiceHelper.defaultStyleConfigPath(cm);
			DashboardWebServiceHelper.writeXmlFile(configPath, doc);
		}
		
		return true;
	}
	
	public static OMElement templateStyleFileList(String rootName, String rootDir, String type) throws FileNotFoundException, IOException, XMLStreamException{
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;
		
		OMElement root = factory.createOMElement(qname(rootName));
		
		File templateDir = new File(rootDir);
		if(templateDir.exists() && templateDir.isDirectory()){
			File[] templateFiles = templateDir.listFiles();
			for(int i = 0; i < templateFiles.length; i++){
				if (templateFiles[i].getName().endsWith(".style"))
				{
					OMElement template = DashboardWebServiceHelper.readXmlFile(templateFiles[i].getAbsolutePath());
					OMAttribute typeAttr = template.getAttribute(qname("type"));
					if(typeAttr != null){
						typeAttr.setAttributeValue(type);
					}else{
						typeAttr = factory.createOMAttribute("type", ns, type);
						template.addAttribute(typeAttr);
					}
					root.addChild(template);
				}
			}
		}
		
		return root;
	}
	
	public static OMElement createMinimalPageInfo(String uuid, String name, String dir, boolean isInvisible){
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;
		OMElement pageEl = factory.createOMElement(DashboardWebService.PAGE, ns);
	
		XmlUtils.addAttribute(factory, pageEl, DashboardWebService.UUID, uuid, ns);
		XmlUtils.addAttribute(factory, pageEl, DashboardWebService.NAME, name, ns);
		XmlUtils.addAttribute(factory, pageEl, DashboardWebService.DIR, dir, ns);
		XmlUtils.addAttribute(factory, pageEl, PARTIAL, Boolean.TRUE.toString(), ns);
		
		XmlUtils.addContent(factory, pageEl, DashboardWebService.INVISIBLE, isInvisible, ns);
		
		return pageEl;
	}
	
	public static BasicDBObject createMinimalPageInfoDBObject(String uuid, String name, String dir, boolean isInvisible){
		BasicDBObject pageEl = new BasicDBObject();
	
		pageEl.put(DashboardWebService.UUID, uuid);
		pageEl.put(DashboardWebService.NAME, name);
		pageEl.put(DashboardWebService.DIR, dir);
		pageEl.put(PARTIAL, Boolean.TRUE.toString());
		pageEl.put(DashboardWebService.INVISIBLE, isInvisible);
		
		return pageEl;
	}
	
}
