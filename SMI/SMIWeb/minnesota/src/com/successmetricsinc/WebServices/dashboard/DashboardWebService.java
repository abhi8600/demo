/**
 * $Id: DashboardWebService.java,v 1.113 2012-08-22 01:07:32 BIRST\agarrison Exp $
 *
 * Copyright (C) 2009-2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.WebServices.dashboard;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMAttribute;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMException;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;

import com.successmetricsinc.Column;
import com.successmetricsinc.Prompt;
import com.successmetricsinc.Repository;
import com.successmetricsinc.UserBean;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.WebServices.BirstWebService;
import com.successmetricsinc.WebServices.BirstWebServiceResult;
import com.successmetricsinc.WebServices.XMLWebServiceResult;
import com.successmetricsinc.adhoc.AdhocColumn;
import com.successmetricsinc.adhoc.AdhocEntity;
import com.successmetricsinc.adhoc.AdhocReport;
import com.successmetricsinc.adhoc.AdhocReportHelper;
import com.successmetricsinc.adhoc.AdhocSubreport;
import com.successmetricsinc.adhoc.Drill;
import com.successmetricsinc.adhoc.AdhocColumn.ColumnType;
import com.successmetricsinc.catalog.CatalogAccessException;
import com.successmetricsinc.catalog.CatalogManager;
import com.successmetricsinc.catalog.CatalogPermission;
import com.successmetricsinc.catalog.CatalogManager.ACTION;
import com.successmetricsinc.jasperReports.JRSubreportRecursionException;
import com.successmetricsinc.query.DisplayExpression;
import com.successmetricsinc.query.DisplayFilter;
import com.successmetricsinc.query.JasperDataSourceProvider;
import com.successmetricsinc.query.QueryFilter;
import com.successmetricsinc.query.TopFilter;
import com.successmetricsinc.query.Filter.FilterType;
import com.successmetricsinc.transformation.object.SavedExpression;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.FileUtils;
import com.successmetricsinc.util.SwapSpaceInProgressException;
import com.successmetricsinc.util.UserNotLoggedInException;
import com.successmetricsinc.util.UserOperationNotAuthorizedException;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XmlUtils;

@WebService( serviceName="Dashboard")
public class DashboardWebService extends BirstWebService {

	public static final String DOT_PAGE = ".page";
	public static final String MODIFIABLE = "modifiable";
	public static final String NAME = "name";
	public static final String PATH = "Path";
	public static final String DIR = "dir";
	public static final String PAGE = "Page";
	public static final String DASHBOARD = "Dashboard";
	public static final String DASH_XML_FILE = "dashboard.xml";
	public static final String PAGE_LOCATION = "PageLocation";
	public static final String INVISIBLE = "Invisible";
	public static final String UUID = "uuid";
	public static final String NOTES = "Notes";
	public static final String NOTE = "note";
	public static final String STICKY_NOTES = "StickyNotes";
	public static final String CREATOR = "creator";
	public static final String IS_PUB = "isPublic";
	public static final String ORDER = "Order";
	public static final String CREATED_BY = "createdBy";
    public static final String URL = "Url";
	
    // Pattern to detect possible XXE fields
	private static Pattern XXE = Pattern.compile("<!ENTITY(.)+(SYSTEM|PUBLIC)( )+\"(.)+\"( )+>");
	
	private static final int MAX_ORDER = 9999;
	
	private static Logger logger = Logger.getLogger(DashboardWebService.class);
	
	public OMElement changeDashboardName(String oldName, String newName){
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (isLoggedIn() == false) {
				throw new UserNotLoggedInException();
			}
			if (oldName == null || newName == null || oldName.isEmpty() || newName.isEmpty()){
				throw new FileNotFoundException("One or both of the dashboard names is invalid");
			}
			
			SMIWebUserBean ub = getUserBean();
			if(!ub.isSuperUserOwnerOrAdmin() && !ub.hasEditDashboardPermission())
			{
				throw new UserOperationNotAuthorizedException("changeDashboardName : User not authorized to execute the operation");
			}
			CatalogManager cm = ub.getCatalogManager();
			
			//Invalidate cache
			deleteCacheListings(ub, "changed dashboard name from " + oldName + " to " + newName);
			
			// for each oldName directory, rename to the newName 
			List<String> dashboards = cm.getDashboardFolders(ub);
			boolean found = false;
			for (String dash: dashboards) {
				String name = dash.substring(dash.lastIndexOf('/') + 1);
				if (! name.equals(oldName))
					continue;

				found = true;
				if (! cm.getPermission(dash).canModify(ub.getUserGroups(), ub.isSuperUserOwnerOrAdmin())) {
					result.setErrorCode(BaseException.ERROR_PERMISSION);
					logger.warn("Can not modify directory " + dash + ".  Rename dashboard failed.");
					continue;
				}
				String errMsg = cm.renameFile(newName, dash, ub.getUserGroups(), ub.isSuperUserOwnerOrAdmin());
				if(errMsg != null && !errMsg.isEmpty()){
					logger.error(errMsg);
					result.setErrorMessage(errMsg);
					result.setErrorCode(BaseException.ERROR_PERMISSION);
				}else{
					if(DashboardMongoDBCache.useMongoDB()){
						DashboardMongoDBCache.renameDashboard(ub, newName, dash);
					}
				}
			}
			
			if (!found) {
				result.setErrorCode(BaseException.ERROR_INVALID_DATA);
				result.setErrorMessage("Could not find dashboard named " + oldName + " to rename.");
			}
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement changePageName(String dashboardName, String oldPageName, String newPageName) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (isLoggedIn() == false) {
				throw new UserNotLoggedInException();
			}
			if (dashboardName == null || dashboardName.isEmpty()){
				throw new FileNotFoundException("The dashboard name is invalid");
			}
			
			SMIWebUserBean ub = getUserBean();
			if(!ub.isSuperUserOwnerOrAdmin() && !ub.hasEditDashboardPermission())
			{
				throw new UserOperationNotAuthorizedException("changePageName : User not authorized to execute the operation");
			}
			CatalogManager cm = ub.getCatalogManager();
			
			//Invalidate cache
			deleteCacheListings(ub, "changed page name from " + oldPageName + " to " + newPageName);
			
			// for each oldName directory, rename to the newName 
			boolean foundDashboard = false;
			boolean foundPage = false;
			List<String> dashboards = cm.getDashboardFolders(ub);
			for (String dash: dashboards) {
				String name = dash.substring(dash.lastIndexOf('/') + 1);
				if (! name.equals(dashboardName))
					continue;

				String normPath = FilenameUtils.concat(dash, newPageName);
				normPath = FilenameUtils.normalizeNoEndSeparator(normPath);
				if(normPath == null){
					Exception e = new RuntimeException("Invalid page path");
					logger.error("Invalid file path: " + FilenameUtils.concat(dash, newPageName), e);
					throw e;
				}
				String path = cm.getCatalogRootPath() + File.separator + convertFileSeparator(normPath) + ".page";
				File file = new File(path);
				if (file.exists()) {
					result.setErrorCode(BaseException.ERROR_INVALID_DATA);
					result.setErrorMessage("Page " + newPageName + " already exists.  Can not rename " + oldPageName);
					return result.toOMElement();
				}
			}
				
			for (String dash: dashboards) {
				String name = dash.substring(dash.lastIndexOf('/') + 1);
				if (! name.equals(dashboardName))
					continue;
				
				if (! cm.getPermission(dash).canModify(ub.getUserGroups(), ub.isSuperUserOwnerOrAdmin())) {
					result.setErrorCode(BaseException.ERROR_PERMISSION);
					logger.warn("Can not modify directory " + dash + ".  Rename page failed.");
					continue;
				}
				
				foundDashboard = true;
				
				String normPath = FilenameUtils.concat(dash, oldPageName);
				normPath = FilenameUtils.normalizeNoEndSeparator(normPath);
				if(normPath == null){
					Exception e = new RuntimeException("Invalid page path");
					logger.error("Invalid file path: " + FilenameUtils.concat(dash, oldPageName), e);
					throw e;
				}
				String path = cm.getCatalogRootPath() + File.separator + convertFileSeparator(normPath) + ".page";
				File file = new File(path);
				if (! file.exists())
					continue;
				
				foundPage = true;
				OMElement page = DashboardWebServiceHelper.readXmlFile(path);
				
				// rename to new name
				OMAttribute nameAttribute = page.getAttribute(new QName("name"));
				if (nameAttribute != null)
					page.removeAttribute(nameAttribute);
				page.addAttribute("name", newPageName, null);
				String order = page.getAttributeValue(new QName("", "dashboardOrder"));
				writePage(dash, newPageName, order, cm, page, ub);
				cm.deleteFile(path, ub.getUserGroups(), ub.isSuperUserOwnerOrAdmin());
			}
			
			if (!foundDashboard) {
				result.setErrorCode(BaseException.ERROR_INVALID_DATA);
				result.setErrorMessage("Could not find dashboard " + dashboardName + " to rename a page in.");
			}else if (!foundPage) {
				result.setErrorCode(BaseException.ERROR_INVALID_DATA);
				result.setErrorMessage("Could not find page " + oldPageName + " to rename.");
			}
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement deleteDashboard(String dashboardName){
		if (logger.isTraceEnabled())
			logger.trace("'deleteDashboard()' - 'dashboardName': " + dashboardName);
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean ub = getUserBean();
			if(!ub.isSuperUserOwnerOrAdmin() && !ub.hasEditDashboardPermission())
			{
				throw new UserOperationNotAuthorizedException("deleteDashboard : User not authorized to execute the operation");
			}
			//Invalidate cache
			deleteCacheListings(ub, "deleting dashboard " + dashboardName);
			
			CatalogManager cm = ub.getCatalogManager();
			List<String> dashboards = cm.getDashboardFolders(ub);
			List<File> undeletableDirs = new ArrayList<File>();
			if (dashboards != null) {
				for (String dash : dashboards) {
					File folder = new File(cm.getCatalogRootPath() + File.separator + dash);
					if(folder.exists()) {
						String name = dash.substring(dash.lastIndexOf('/') + 1);
						if(name.equals(dashboardName)){
							String errMsg = null;
							try{
								errMsg = cm.deactivateDashboardFolder(folder.getAbsolutePath(), ub.getUserGroups(), ub.isSuperUserOwnerOrAdmin());
							}catch(CatalogAccessException ex){
								undeletableDirs.add(folder);
							}
							if(errMsg != null && !errMsg.isEmpty()){
								logger.error(errMsg);
								undeletableDirs.add(folder);
							}else{
								if(DashboardMongoDBCache.useMongoDB()){
									DashboardMongoDBCache.deleteDashboard(ub, dash);
								}
							}
						}
					}
				}
			}
			
			//List of directories which we don't have permission or had errors in trying delete
			if(undeletableDirs.size() > 0){
				OMFactory factory = OMAbstractFactory.getOMFactory();
				OMNamespace ns = null;
				OMElement dashboard = factory.createOMElement(DASHBOARD, ns);
				XmlUtils.addAttribute(factory, dashboard, NAME, dashboardName, ns);
				List<OMElement> pageElems = new ArrayList<OMElement>();
				for (File folder : undeletableDirs) {
					addDashboardPages(folder, dashboard, factory, ns, pageElems);
				}
				Collections.sort(pageElems, OrderComparator.getInstance());
				for(int i = 0; i < pageElems.size(); i++){
					dashboard.addChild(pageElems.get(i));
				}
				result.setResult(new XMLWebServiceResult(dashboard));
			}
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement movePageToDashboard(String page, String dashboard, int order){
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			if(page == null || dashboard == null || page.isEmpty() || dashboard.isEmpty()){
				throw new FileNotFoundException("The page's or dashboard's or both paths are invalid");
			}
			
			SMIWebUserBean ub = getUserBean();
			if(!ub.isSuperUserOwnerOrAdmin() && !ub.hasEditDashboardPermission())
			{
				throw new UserOperationNotAuthorizedException("movePageToDashboard : User not authorized to execute the operation");
			}
			CatalogManager cm = ub.getCatalogManager();
			
			//Invalidate cache
			deleteCacheListings(ub, "moved page " + page + " to dashboard " + dashboard);
			
			String errMsg = cm.moveCopyFile(ACTION.MOVE, page, dashboard, ub.getUserGroups(), ub.isSuperUserOwnerOrAdmin());
			if(errMsg != null && !errMsg.isEmpty()){
				logger.error(errMsg);
				result.setErrorMessage(errMsg);
				result.setErrorCode(BaseException.ERROR_PERMISSION);
			}else{
				//Write new order
				String newPagePath =  cm.getNormalizedPath(dashboard + File.separator + FileUtils.getFileBasename(page));
				writeDashboardOrderCreatedBy(newPagePath, order, null);
				
				//Update MongoDB
				if(DashboardMongoDBCache.useMongoDB()){
					DashboardMongoDBCache.movePageToDashboard(ub, page, dashboard, order);
				}
			}
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement deletePage(String path){
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean ub = getUserBean();
			if(!ub.isSuperUserOwnerOrAdmin() && !ub.hasEditDashboardPermission())
			{
				throw new UserOperationNotAuthorizedException("deletePage : User not authorized to execute the operation");
			}
			CatalogManager cm = ub.getCatalogManager();
			
			//Invalidate cache
			deleteCacheListings(ub, " deleted page " + path);
			
			String errMsg = cm.deleteFile(path, ub.getUserGroups(), ub.isSuperUserOwnerOrAdmin());
			if(errMsg != null && !errMsg.isEmpty()){
				//Empty dashboards directory was deleted, lazy regenerate cache
				if(errMsg.equals("Empty dashboard deleted")){
					DashboardMongoDBCache.createMarkerFile(ub);
					return result.toOMElement();
				}
				logger.error(errMsg);
				result.setErrorMessage(errMsg);
				result.setErrorCode(BaseException.ERROR_PERMISSION);
			}else{
				if(DashboardMongoDBCache.useMongoDB()){
					DashboardMongoDBCache.deletePage(ub, path);
				}
			}
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement copyPage(String dir, String fromPage, String toPageName){
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean ub = getUserBean();
			if(!ub.isSuperUserOwnerOrAdmin() && !ub.hasEditDashboardPermission())
			{
				throw new UserOperationNotAuthorizedException("copyPage : User not authorized to execute the operation");
			}
			CatalogManager cm = ub.getCatalogManager();
			
			//Add .page extension if not attached
			if(fromPage.indexOf(DOT_PAGE) < 0){
				fromPage += DOT_PAGE;
			}
			
			String fromPagePath = 
				cm.getCatalogRootPath() + File.separator + convertFileSeparator(dir) + File.separator + fromPage;
			File from = new File(fromPagePath);
			
			//Duplicate and assign a new uuid, name
			OMElement p = readPage(from.getAbsolutePath());
			OMAttribute nameAttr = p.getAttribute(DashboardWebServiceHelper.qname(NAME));
			nameAttr.setAttributeValue(toPageName);
			DashboardWebServiceHelper.recreateUuidAttr(p);
			
			//Write the duplicate page
			writePage(dir, toPageName, "0", cm, p, ub);
			
			//Return the minimal xml page information
			OMElement partial = appendMinimalPageInfo(p);
			result.setResult(new XMLWebServiceResult(partial));
		}catch(Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	
	@SuppressWarnings("unchecked")
	public OMElement copyDashboard(String xmlStr){
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean ub = getUserBean();
			if(!ub.isSuperUserOwnerOrAdmin() && !ub.hasEditDashboardPermission())
			{
				throw new UserOperationNotAuthorizedException("copyDashboard : User not authorized to execute the operation");
			}
			CatalogManager cm = ub.getCatalogManager();
			
			OMElement xml = parseXmlString(xmlStr);
			String to = xml.getAttributeValue(DashboardWebServiceHelper.qname(DIR));
			String name = xml.getAttributeValue(DashboardWebServiceHelper.qname(NAME));
			
			OMFactory factory = OMAbstractFactory.getOMFactory();
			
			OMElement root = factory.createOMElement(DashboardWebServiceHelper.qname(DASHBOARD));
			root.addAttribute(factory.createOMAttribute(NAME, null, name));
			root.addAttribute(factory.createOMAttribute(DIR, null, to));
			
			File toDir = new File(cm.getCatalogRootPath() + File.separator + convertFileSeparator(to));
			if(toDir.exists()){
				result.setErrorCode(BaseException.ERROR_PERMISSION);
				result.setErrorMessage("Cannot copy dashboard pages to an existing directory");
			}else{
				Iterator ps = xml.getChildElements();
				while(ps.hasNext()){
					OMElement el = (OMElement)ps.next();
					String path = cm.getCatalogRootPath() + File.separator + convertFileSeparator(el.getText());
					File f = new File(path);

					// Read the specified page file from disk, fixup @dir, @uuid
					OMElement p = readPage(f.getAbsolutePath());
					DashboardWebServiceHelper.recreateUuidAttr(p);
					OMAttribute dirAttr = p.getAttribute(DashboardWebServiceHelper.qname(DIR));
					dirAttr.setAttributeValue(to);

					// Check that we don't have duplicates
					String pageName = f.getName();
					String filepath = toDir.getAbsolutePath() + File.separator + pageName;
					File toFile = new File(filepath);
					if(toFile.exists()){
						//File already exists, we rename it something else. Edge case where same page names exist
						String pageNameNoSuffix = pageName.substring(0, pageName.indexOf(".page"));
						OMAttribute pName = p.getAttribute(DashboardWebServiceHelper.qname(NAME));
						pName.setAttributeValue("Copy of " + pageNameNoSuffix + "-" + System.currentTimeMillis());
					}

					writePage(to, pageName, "0", cm, p, ub);

					// Add to reply
					root.addChild(appendMinimalPageInfo(p));
				}

				result.setResult(new XMLWebServiceResult(root));
			}
		}catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement isUrlIFrameable(String url){
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean ub = getUserBean();
			if(!ub.isSuperUserOwnerOrAdmin() && !ub.hasEditDashboardPermission())
			{
				throw new UserOperationNotAuthorizedException("isUrlIframeable : User not authorized to execute the operation");
			}
			
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMElement root = factory.createOMElement(DashboardWebServiceHelper.qname(URL));
			OMElement xframeOrigin = factory.createOMElement(DashboardWebServiceHelper.qname("X-Frame-Options"));
			OMElement statusCode = factory.createOMElement(DashboardWebServiceHelper.qname("StatusCode"));
			OMElement validUrl = factory.createOMElement(DashboardWebServiceHelper.qname("Valid"));
			validUrl.setText("true");
			
			root.addChild(xframeOrigin);
			root.addChild(statusCode);
			root.addChild(validUrl);
			
			GetMethod get = new GetMethod(url);
			get.setRequestHeader("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:28.0) Gecko/20100101 Firefox/28.0");
			get.setFollowRedirects(false);
			
			HttpClient httpClient = new HttpClient();
			httpClient.getParams().setSoTimeout(60000);
			int rc = 404;
			try{
				rc = httpClient.executeMethod(get);
			}catch(java.net.UnknownHostException e){
				validUrl.setText("false");
			}
			
			if(rc >= 301 && rc <= 303){
				Header location = get.getResponseHeader("Location");
				String urlValue = location.getValue();
				if(urlValue != null && !urlValue.startsWith("https")){
					validUrl.setText("RedirectedToHTTP");
				}else{
					//ok, let's try to follow the https redirect
					get.setFollowRedirects(true);
					try{
						rc = httpClient.executeMethod(get);
					}catch(java.net.UnknownHostException e){
						validUrl.setText("false");
					}
				}
			}
			
			statusCode.setText(Integer.toString(rc));
			
			Header header = get.getResponseHeader("X-Frame-Options");
			if(header != null){
				String value = header.getValue();
				if(value != null){
					xframeOrigin.setText(value);
				}
				logger.debug("isUrlIFrameable, url = " + url + " header = " + value);
			}
			
			result.setResult(new XMLWebServiceResult(root));
		}catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	private OMElement appendMinimalPageInfo(OMElement full){
		OMAttribute invisible = full.getAttribute(DashboardWebServiceHelper.qname(INVISIBLE));
		boolean isInvisible = (invisible == null) ? true : invisible.getAttributeValue().equalsIgnoreCase("true");
		
		return DashboardWebServiceHelper.createMinimalPageInfo(
				full.getAttributeValue(DashboardWebServiceHelper.qname(UUID)),
				full.getAttributeValue(DashboardWebServiceHelper.qname(NAME)),
				full.getAttributeValue(DashboardWebServiceHelper.qname(DIR)),
				isInvisible);
	}
	
	/**
	 * Save the space's dashboard settings
	 * @param xml
	 * @return
	 */
	public OMElement saveStyleSettings(String xml){
		return saveStyleSettingsTemplate(xml, false);
	}
	
	/**
	 * Save a dashboard style template
	 * @param xml
	 * @param saveAsTemplateOnly
	 * @return
	 */
	public OMElement saveStyleSettingsTemplate(String xml, boolean saveAsTemplateOnly){
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean ub = getUserBean();
			CatalogManager cm = ub.getCatalogManager();
			OMElement doc = parseXmlString(xml);
			
			String configPath = DashboardWebServiceHelper.defaultStyleConfigPath(cm);
			
			DashboardWebServiceHelper.writeXmlFile(configPath, doc);
			OMAttribute nameAttr = doc.getAttribute(new QName("", "name"));
			
			if(nameAttr != null){
				DashboardWebServiceHelper.writeStyleTemplate(result, cm, doc, saveAsTemplateOnly);
			}else{
				//We must have a name. 
				result.setErrorCode(BaseException.ERROR_INVALID_DATA);
				result.setErrorMessage("The style template must have a name");
			}
		}catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement deleteStyleTemplate(String name){
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean ub = getUserBean();
			CatalogManager cm = ub.getCatalogManager();
			
			String sanitizedName = name.replace("/", "###");
			String templateFilePath = DashboardWebServiceHelper.templateDir(cm) + File.separator + sanitizedName + ".style";
			
			File templateFile = new File(templateFilePath);
			if(!templateFile.delete()){
				result.setErrorCode(BaseException.ERROR_PERMISSION);
				result.setErrorMessage("Failed to delete the style template file");
			}
		}catch(Exception e){
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement getStyleTemplates(String acornRoot){
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean ub = getUserBean();
			OMElement rs = DashboardWebServiceHelper.getStyleTemplates(result, ub, acornRoot);
			if(rs != null){
				result.setResult(new XMLWebServiceResult(rs));
			}
		}catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	@SuppressWarnings("unchecked")
	public OMElement saveDashboardOrder(String xml){
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean ub = getUserBean();
			if(!ub.isSuperUserOwnerOrAdmin() && !ub.hasEditDashboardPermission())
			{
				throw new UserOperationNotAuthorizedException("saveDashboardOrder : User not authorized to execute the operation");
			}
			CatalogManager cm = ub.getCatalogManager();
			
			//Invalidate cache
			deleteCacheListings(ub, "reordered dashboards");
			
			OMElement doc = parseXmlString(xml);
			
			HashMap<String, Integer> dashboardOrder = new HashMap<String, Integer>();
			Iterator<OMElement> itr = doc.getChildrenWithName(new QName("", DASHBOARD));
			int idx = 0;
			while(itr.hasNext()){
				OMElement dash = itr.next();
				Iterator<OMElement> pathsItr = dash.getChildrenWithName(new QName("", PATH));
				boolean wroteToMongo = false;
				while(pathsItr.hasNext()){
					OMElement path = pathsItr.next();
					String location = path.getText();
					String absPath = cm.getCatalogRootPath() + File.separator + convertFileSeparator(location) + File.separator + DASH_XML_FILE;
					writeDashboardOrderCreatedBy(absPath, idx, null);
					
					if(!wroteToMongo){
						String dashName = location;
						int lastSlash = location.lastIndexOf("/");
						if(lastSlash >= 0){
							dashName = dashName.substring(lastSlash + 1);
						}
						dashboardOrder.put(dashName, idx);
						wroteToMongo = true;
					}
				}
				idx++;
			}
			if(DashboardMongoDBCache.useMongoDB()){
				DashboardMongoDBCache.upsertDashboardOrder(ub, dashboardOrder);
			}
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	@SuppressWarnings("unchecked")
	public OMElement savePageOrder(String xml){
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean ub = getUserBean();
			if(!ub.isSuperUserOwnerOrAdmin() && !ub.hasEditDashboardPermission())
			{
				throw new UserOperationNotAuthorizedException("savePageOrder : User not authorized to execute the operation");
			}
			CatalogManager cm = ub.getCatalogManager();
			
			//Invalidate cache
			deleteCacheListings(ub, "reordered page");
			
			OMElement doc = parseXmlString(xml);
			
			Iterator<OMElement> itr = doc.getChildrenWithName(new QName("", PAGE_LOCATION));
			int idx = 0;
			while(itr.hasNext()){
				OMElement order = itr.next();
				String path = order.getAttributeValue(new QName("", "path"));
				String absPath = cm.getCatalogRootPath() + File.separator + convertFileSeparator(path);
				writeDashboardOrderCreatedBy(absPath, idx, null);
				if(DashboardMongoDBCache.useMongoDB()){
					DashboardMongoDBCache.savePageOrder(ub, path, idx);
				}
				idx++;
			}
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	private void writeDashboardOrderCreatedBy(String file, int index, String createdBy) throws XMLStreamException, IOException{
		OMElement pageRoot = null;
		
		//Write directly if the file is empty..
		File target = new File(file);
		if(target.exists() && target.length() == 0){
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMNamespace ns = null;
			pageRoot = factory.createOMElement("Configuration", ns);
		}else{
			pageRoot = readPage(file);
		}
		OMElement orderEl = pageRoot.getFirstChildWithName(new QName("", "Order"));	
		if(orderEl == null){
			XmlUtils.addContent(OMAbstractFactory.getOMFactory(), pageRoot, "Order", index, null);
		}else{
			orderEl.setText(Integer.toString(index));
		}
		
		//User who first created the dashboard
		if(createdBy != null){
			XmlUtils.addAttribute(OMAbstractFactory.getOMFactory(), pageRoot, "createdBy", createdBy, null);
		}
		
		DashboardWebServiceHelper.writeXmlFile(file, pageRoot);
	}
	
	public OMElement setPageProperties(String xml){
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean ub = getUserBean();
			if(!ub.isSuperUserOwnerOrAdmin() && !ub.hasEditDashboardPermission())
			{
				throw new UserOperationNotAuthorizedException("setPageProperties : User not authorized to execute the operation");
			}
			CatalogManager cm = ub.getCatalogManager();
			
			//Invalidate cache
			deleteCacheListings(ub, "changed properties / new / rename page");
			XMLInputFactory inputFactory = XmlUtils.getSecureXMLInputFactory();
			XMLStreamReader parser = inputFactory.createXMLStreamReader(new StringReader(xml));

			StAXOMBuilder builder = new StAXOMBuilder(parser);
			OMElement doc = builder.getDocumentElement();
			
			//Get the Page section
			OMElement page = doc.getFirstChildWithName(new QName("", "Page"));
			
			//Get the operations section
			OMElement ops = doc.getFirstChildWithName(new QName("", "Operations"));
			
			if(ops != null){
				
				@SuppressWarnings("rawtypes")
				Iterator itr = ops.getChildren();
				while(itr.hasNext()){
					Object obj = itr.next();
					if (!(obj instanceof OMElement))
						continue;
					
					OMElement op = (OMElement)obj;
					String opName = op.getLocalName();
					
					if (("Write").equals(opName)){
						//Add UUID if not exists
						String uuid = DashboardWebServiceHelper.addUuidAttrIfNotExists(page);
						writePage(cm, op, page, ub);	
						
						//Return UUID in response
						setUuidResult(result, uuid);
					}else if (("Rename").equals(opName)){
						renamePage(cm, op, page, ub);
					}
				}
			}
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	/* <Operation>
	 * 	<Rename>
	 * 		<Dir name="shared/Dashbaord 1"/>
	 *   	<From file="OldPage"/>
	 *   	<To file="NewPage"/>
	 * 	</Rename>
	 * </Operation>
	 * 
	 * <Page>...</Page>
	 */
	private void renamePage(CatalogManager cm, OMElement op, OMElement page, SMIWebUserBean ub) throws CatalogAccessException{
		OMElement fromEl = op.getFirstChildWithName(new QName("", "From"));
		OMElement toEl = op.getFirstChildWithName(new QName("", "To"));
		OMElement dirEl = op.getFirstChildWithName(new QName("", "Dir"));
		
		String newName = toEl.getAttributeValue(new QName("", "file")) + DOT_PAGE;
		
		String dir = dirEl.getAttributeValue(new QName("", NAME));
		String toFile = fromEl.getAttributeValue(new QName("", "file"));
		
		String originalFullFilePath = this.convertToAbsolutePathPage(cm, dir, toFile);
		
		String errorMsg = cm.renameFile(newName, originalFullFilePath, ub.getUserGroups(), ub.isSuperUserOwnerOrAdmin());		
		if(errorMsg != ""){
			throw new CatalogAccessException(errorMsg);
		}
	}
	
	/*<Operation>
	 *  <Write dir="shared/dashboard/" file="Page"/>
	 * </Operation>
	 * 
	 * <Page>...</Page>
	 * 
	 * Writes the page xml to the file listed. If the file doesn't exist, create it. 
	 * If directory doesn't exist, create it as well and mark it as a dashboard directory
	 */
	private void writePage(CatalogManager cm, OMElement op, OMElement page, SMIWebUserBean ub) 
		throws XMLStreamException, FactoryConfigurationError, CatalogAccessException, IOException, JSONException, SwapSpaceInProgressException{

		String dir = op.getAttributeValue(new QName("", "dir"));
		String file = op.getAttributeValue(new QName("", "file"));
		String order = op.getAttributeValue(new QName("", "dashboardOrder"));
		
		writePage(dir, file, order, cm, page, ub);
	}
	
	private void writePage(String dir, String file, String order, CatalogManager cm, OMElement page, SMIWebUserBean ub) throws SwapSpaceInProgressException, CatalogAccessException, IOException, NumberFormatException, XMLStreamException {
		if(dir == null || dir.isEmpty()){
			throw new IllegalArgumentException("Invalid directory");
		}
		
		checkForSwapSpaceOperation(cm);
		String dashboarddir = cm.getCatalogRootPath() + File.separator + convertFileSeparator(dir);
		if(dashboarddir.indexOf("private") != -1){
			dashboarddir = cm.addUserFolderToPrivate(dashboarddir);
		}
		File dbdir = new File(dashboarddir);
		if(!dbdir.exists()){	
			if(!cm.createFolder(dashboarddir)){
				throw new CatalogAccessException("Cannot create dashboard directory");
			}
		}
		
		CatalogPermission perm = cm.getPermission(dashboarddir);
		if(!perm.canModify(ub.getUserGroups(), ub.isSuperUserOwnerOrAdmin())){
			throw new CatalogAccessException("Cannot modify directory");
		}
		
		boolean newDashboardFolder = false;
		
		//Write the dashboards.xml if it doesn't exist to indicate this as a dashboard directory
		String dashboardXml = dashboarddir + File.separator + DASH_XML_FILE;
		File indicator = new File(dashboardXml);
		if(!indicator.exists()){
			if(!indicator.createNewFile()){
				throw new CatalogAccessException("Cannot convert directory to dashboard directory");
			}
			
			int orderNum = (order != null && !order.isEmpty()) ? Integer.parseInt(order) : MAX_ORDER;
			writeDashboardOrderCreatedBy(dashboardXml, orderNum, ub.getUserName());
			newDashboardFolder = true;
		}
		
		//Write the Page xml
		String filepath = dashboarddir + File.separator + file;
		if(!file.endsWith(DOT_PAGE)){
			filepath += DOT_PAGE;
		}
		
		//Add creator of the page
		if(!(new File(filepath)).exists()){
			XmlUtils.addAttribute(OMAbstractFactory.getOMFactory(), page, CREATED_BY, ub.getUserName(), null);
		}
		
		DashboardWebServiceHelper.writeXmlFile(filepath, page);
		
		//Write to Mongo
		if(DashboardMongoDBCache.useMongoDB()){
			DashboardMongoDBCache.upsertPage(ub, new File(filepath));
		
			//Re-cache the dashboard folders order
			if(newDashboardFolder){
				DashboardMongoDBCache.regenerateDashboardsOrdersPaths(ub);
			}
		}
	}
	
	private OMElement parseXmlString(String xml) throws XMLStreamException, FactoryConfigurationError{
		XMLInputFactory inputFactory = XmlUtils.getSecureXMLInputFactory();
		XMLStreamReader parser = inputFactory.createXMLStreamReader(new StringReader(xml));

		StAXOMBuilder builder = new StAXOMBuilder(parser);
		return builder.getDocumentElement();
	}
	
	private String readFile(String absFilePath) throws FileNotFoundException, IOException{
		StringBuffer sb = new StringBuffer();
		FileInputStream fis = null;
		InputStreamReader is = null;
		BufferedReader br = null;
		
		try{
			fis = new FileInputStream(absFilePath);
			is = new InputStreamReader(fis, "UTF-8");
			br = new BufferedReader(is);

			//br = new BufferedReader(new FileReader(absFilePath));
			String nextLine = "";
		
			while ((nextLine = br.readLine()) != null) {
				sb.append(nextLine);
			}
		}finally{
			if(br != null){
				try{
					br.close();
				}catch(Exception e){}
			}
			if(is != null){
				try{
					is.close();
				}catch(Exception e){}
			}
			if(fis != null){
				try{
					fis.close();
				}catch(Exception e){}
			}
		}
		return sb.toString();
	}
	
	private OMElement readPage(String absFilePath) throws XMLStreamException, FileNotFoundException, UnsupportedEncodingException{
		return DashboardWebServiceHelper.readXmlFile(absFilePath);
	}
	
	private String convertFileSeparator(String path){
		return path.replace('/', File.separatorChar);
	}
	
	private String convertToAbsolutePathPage(CatalogManager cm, String directory, String file){
		return convertToAbsolutePath(cm, directory, file, DOT_PAGE);
	}
	
	private String convertToAbsolutePath(CatalogManager cm, String directory, String file, String extension){
		String absPath = cm.getCatalogRootPath() + File.separator + convertFileSeparator(directory);
		absPath += File.separator + file;
		absPath += extension;
		
		return absPath;
	}
	
	public OMElement getDashboardReportPrompts(String xml) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean ub = getUserBean();
			CatalogManager cm = ub.getCatalogManager();
			HashMap<String, Prompt> promptsMap = new HashMap<String, Prompt>();
			
			OMElement doc = parseXmlString(xml);
			
			for (@SuppressWarnings("unchecked")
			Iterator<OMElement> iter = doc.getChildrenWithName(new QName("", PATH)); iter.hasNext(); ) {
				OMElement pathEl = iter.next();
				String path = XmlUtils.getStringContent(pathEl);
				path = cm.getNormalizedPath(path);
				try {
					List<Prompt> reportPrompts = getReportPromptedFilterPrompts(path, ub, cm);
					if (reportPrompts != null) {
						for (Prompt p: reportPrompts) {
							String name = p.getParameterName();
							if (! promptsMap.containsKey(name)) {
								promptsMap.put(name, p);
							}
						}
					}
				} catch (Exception e) {
						// ignore - will be handled by report rendering
				}
			}
			
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMNamespace ns = null;
			OMElement promptList = factory.createOMElement("Prompts", ns);
			for (Prompt p : promptsMap.values()) {
				p.addContent(promptList, factory, ns);
			}
			
			result.setResult(new XMLWebServiceResult(promptList));
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	private List<Prompt> getReportPromptedFilterPrompts(String path, com.successmetricsinc.UserBean ub, CatalogManager cm) throws Exception {
		return getReportPromptedFilterPrompts(path, ub, cm, new ArrayList<String>());
	}
	
	private List<Prompt> getReportPromptedFilterPrompts(String path, com.successmetricsinc.UserBean ub, CatalogManager cm, List<String> reports) throws Exception {
		
		if (reports.contains(path)) {
			throw new JRSubreportRecursionException("Subreport contains iteself " + path);
		}
		reports.add(path);
		// try to load .AdhocReport file
		File aFile = CatalogManager.getAdhocReportFile(path, cm.getCatalogRootPath());
		if (aFile == null || aFile.exists() == false)
			return null;
		
		AdhocReport report = AdhocReport.read(aFile);
		List<Prompt> prompts = new ArrayList<Prompt>();
		TopFilter topFilter = report.getTopFilter();
		if (topFilter != null && topFilter.isPrompted()) {
			Prompt p = new Prompt(topFilter);
			prompts.add(p);
		}
		
		List<QueryFilter> qfs = report.getFilters();
		if (qfs != null) {
			for (QueryFilter qf : qfs) {
				if (qf.isPrompted()) {
					Prompt p = new Prompt(qf);
					AdhocColumn ac = report.getColumnByName(qf.getColumn());
					if (ac != null)
					{
						p.setColumnFormat(ac.getAdhocColumnFormat());						
					}
					prompts.add(p);
				}
			}
		}
		List<DisplayFilter> dfs = report.getDisplayFilters();
		if (dfs != null) {
			for (DisplayFilter df : dfs) {
				if (df.isPrompted()) {
					Prompt p = new Prompt(df);
					AdhocColumn ac = report.getColumnByName(df.getColumn());
					if (ac != null)
					{
						if(ac.getType() == AdhocColumn.ColumnType.Expression) {
							DisplayExpression ex = ac.getExpression();
							if(!ac.isMeasureExpression() &&  ex.getCompiledOperator() instanceof SavedExpression){
								p.setAsSavedExpression(ex.getExpression());
							}
							p.setClazz(ex.getCompiledClass().toString());
						}
						p.setColumnFormat(ac.getAdhocColumnFormat());
					}					
					prompts.add(p);
				}
			}
		}
		
		for (AdhocEntity el : report.getEntities()) {
			if (el instanceof AdhocSubreport) {
				AdhocSubreport sub = (AdhocSubreport)el;
				String p = sub.getSubreport();
				List<Prompt> ps = getReportPromptedFilterPrompts(p, ub, cm, reports);
				if (ps != null) {
					prompts.addAll(ps);
				}
			}
		}
		
		return prompts;
	}
	
	public OMElement drillDown(String xmlReport, String drillCols, String filters) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			getUserBean().getRepSession().setCancelled(false);
			AdhocReport report = AdhocReport.convert(xmlReport, null, null);
			List<Drill> drills = AdhocReportHelper.parseDrillCols(drillCols, report);
			AdhocReportHelper.drillDown(report, drills, null);
			List<Prompt> prompts = new ArrayList<Prompt>();
			if (filters != null && Util.hasNonWhiteSpaceCharacters(filters)) {
				SMIWebUserBean ub = SMIWebUserBean.getUserBean();
				JasperDataSourceProvider jdsp = ub.getJasperDataSourceProvider();
				jdsp = new JasperDataSourceProvider(jdsp);				
				String filter[] = filters.split("&");
				for (String f : filter) {
					String pairs[] = f.split("=");
					if (pairs.length < 2)
						continue;
						
					String[] col = pairs[0].split("\\.");
					if (col.length == 1) {
						// see if there is an expression in this report with the same value as col[0]
						for (AdhocColumn ac : report.getColumns()) {
							if (ac.getType() == ColumnType.Expression) {
								if (ac.getExpressionString() != null && ac.getExpressionString().equals(col[0])) {
									String[] newCol = new String[2];
									newCol[0] = "EXPR";
									newCol[1] = col[0];
									col = newCol;
									break;
								}
							}
						}
					}
					if (col.length < 2) {
						continue;
					}
						
					Prompt p = new Prompt(new Column(URLDecoder.decode(col[0], "UTF-8"), 
													URLDecoder.decode(col[1], "UTF-8"), 
													URLDecoder.decode(pairs[0], "UTF-8")));
					if ("EXPR".equalsIgnoreCase(col[0])) {
						p.setClazz("VARCHAR");
						p.setFilterType(FilterType.DISPLAY);
					}
					
					// now that the prompt was created - set the format based on the column in the report
					for (AdhocColumn ac : report.getColumns()) {
						if (ac.getDimension() != null && ac.getDimension().equals(col[0]) &&
								ac.getColumn().equals(col[1])) {
							p.setColumnFormat(ac.getFormat());
							break;
						}
					}
					
					p.setSelectedValue(URLDecoder.decode(pairs[1], "UTF-8"));
					prompts.add(p);
					p.fillIfNecessary(new HashMap<String, Object>(), jdsp, new HashMap<String, String>());
				}
			}
			
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMNamespace ns = null;
			OMElement promptList = factory.createOMElement("Prompts", ns);
			for (Prompt p : prompts) {
				p.addContent(promptList, factory, ns);
			}
			OMElement root = factory.createOMElement("root", ns);
			report.addContent(root, factory, ns);
			root.addChild(promptList);
			result.setResult(new XMLWebServiceResult(root));
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	private OMElement getDashboardListOperationNoCache(boolean readPages){
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			
			// bug 6851
			SMIWebUserBean ub = getUserBean();
			boolean su = ub.isSuperUser();
			ub.setSuperUser(false);
			ub.setRootDirectories(null);
			CatalogManager cm = ub.getCatalogManager();
			List<String> dashboards = cm.getDashboardFolders(ub);
			
			if (su) {
				ub.setSuperUser(su);
				ub.setRootDirectories(null);
			}
			
			OMElement rs = getDashboardListNoCache(result, dashboards, ub, readPages);
			
			result.setResult(new XMLWebServiceResult(rs));
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();	
	}
	
	/**
	 * Non-cached, original version of getting the list of dashboards names only.
	 * @return
	 */
	public OMElement getDashboardListLite(){
		return getDashboardListOperationNoCache(false);
	}
	
	/**
	 * Non-cached, original version of the getting the list of dashboards with its contents 
	 * in its entirety.
	 * @return
	 */
	public OMElement getDashboardList(){
		return getDashboardListOperationNoCache(true);
	}
	
	/**
	 * Cached version of getDashboardListLite(), returning string directly from the 
	 * cache file if it exists. Otherwise it runs the same routine as getDashboardListLite() and 
	 * caches the result.
	 * @return String result
	 */
	public String getDashboardListLiteCached() {
		return getDashboardListOperation(false, false);
	}
	
	/**
	 * Cached version of getDashboardList(), returning string directly from the cache
	 * file if it exists. Otherwise it runs the same routine as getDashboardList() and 
	 * caches the result.
	 * @return String result
	 */
	public String getDashboardListCached() {
		return getDashboardListOperation(true, false);
	}
	
	/**
	 * Return the cached dashboard list utilizing MongoDB
	 * @return
	 */
	public String getDashbardListFromDB() {
		return getDashboardListOperation(true, true);
	}
	
	/**
	 * Converts variables to actual values
	 * @param expression
	 * @return the converted expression
	 */
	public OMElement evaluateExpression(String expression) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			
			UserBean ub = BirstWebService.getUserBean();
			Repository r = ub.getRepository();
			
			OMNamespace ns = null;
			OMElement doc = parseXmlString(expression);
			@SuppressWarnings("unchecked")
			Iterator<OMElement> itr = doc.getChildrenWithName(new QName("", "Expression"));
			while(itr.hasNext()){
				OMElement expr = itr.next();
				String ex = expr.getAttributeValue(new QName("", "Expression"));
				ex = r.replaceVariables(null, ex);
				ex = r.replaceVariables(ub.getRepSession(), ex);
				expr.addAttribute("Value", ex, ns);
			}
			
			result.setResult(new XMLWebServiceResult(doc));
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement getPage(String uuid, String pagePath) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			
			SMIWebUserBean ub = BirstWebService.getUserBean();
			OMElement page = null;
			
			boolean readFromFile = true;
			
			if(DashboardMongoDBCache.useMongoDB()){
				try{
					page = DashboardMongoDBCache.readPage(ub, uuid);
					readFromFile = (page == null);
				}catch(Exception e){
					logger.error(e);
				}
			}
			
			if(readFromFile){
				String normPath = FilenameUtils.normalizeNoEndSeparator(pagePath);
				if(normPath == null){
					Exception e = new RuntimeException("Invalid page path");
					logger.error("Invalid file path: " + pagePath, e);
					throw e;
				}
				String path = ub.getCatalogManager().getCatalogRootPath() + File.separator + convertFileSeparator(normPath);
				page = DashboardWebServiceHelper.readXmlFile(path);
			}
			result.setResult(new XMLWebServiceResult(page));
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement getPageStates(String path){
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean ub = BirstWebService.getUserBean();
			CatalogManager cm = ub.getCatalogManager();
			
			//Retrieve the list of files for now
			File stateDir = getBookmarkStateFolder(cm, ub, path);
			if(!stateDir.exists()){
				//Ok if we don't find the directory, it means we didn't save any
				return result.toOMElement();
			}
			
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMElement states = factory.createOMElement("states", null);
			
			String defaultFilename = null;
			File defaultFile = new File(stateDir.getAbsolutePath() + File.separator + "default");
			if(defaultFile.exists()){
				BufferedReader reader = new BufferedReader(new FileReader(defaultFile.getAbsolutePath()));
				defaultFilename = reader.readLine();
				reader.close();
			}
			
			String[] stateFiles = stateDir.list();
			Arrays.sort(stateFiles, Collections.reverseOrder());
			for(int i = 0; i < stateFiles.length; i++){
				String filename = stateFiles[i];
				if(filename.equals("default")){
					continue;
				}
				String stateFilePath = stateDir.getAbsolutePath() + File.separator + filename;
				
				OMElement stateInfo = this.readPage(stateFilePath);
				OMElement title = stateInfo.getFirstChildWithName(new QName("", "title"));
				OMElement descr = stateInfo.getFirstChildWithName(new QName("", "description"));
				
				if (title != null){
					title.detach();
					
					OMElement time = factory.createOMElement("date", null);
					time.setText(stateFiles[i]);
					
					OMElement state = factory.createOMElement("state", null);
					state.addChild(title);
					state.addChild(time);
					if (descr != null){
						descr.detach();
						state.addChild(descr);
					}
					//default?
					if(defaultFilename != null && defaultFilename.equals(filename)){
						OMElement defaultEl = factory.createOMElement("isDefault", null);
						state.addChild(defaultEl);
					}
					states.addChild(state);
				}
			}
			result.setResult(new XMLWebServiceResult(states));
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement setDefaultPageState(String path, String createTime){
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean ub = BirstWebService.getUserBean();
			CatalogManager cm = ub.getCatalogManager();
			
			//Retrieve the state by directory for now
			File stateDir = getBookmarkStateFolder(cm, ub, path);
			if(!stateDir.exists()){
				throw new Exception("Cannot find state directory " + stateDir.getPath());
			}
			
			FileWriter writer = null;
			try
			{
				writer = new FileWriter(stateDir.getAbsolutePath() + File.separator + "default");
				writer.write(createTime);
			}
			finally
			{
				if (writer != null)
					writer.close();
			}
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();	
	}
	
	public OMElement applyDefaultPageState(String path){
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean ub = BirstWebService.getUserBean();
			CatalogManager cm = ub.getCatalogManager();
			
			//Retrieve the state by directory for now
			File stateDir = getBookmarkStateFolder(cm, ub, path);
			if(!stateDir.exists()){
				result.setErrorCode(BaseException.ERROR_NO_DEFAULT_BOOKMARK);
				return result.toOMElement();
			}
			
			String defaultFilename = null;
			File defaultFile = new File(stateDir.getAbsolutePath() + File.separator + "default");
			if(defaultFile.exists()){
				BufferedReader reader = new BufferedReader(new FileReader(defaultFile.getAbsolutePath()));
				defaultFilename = reader.readLine();
				reader.close();
			}else{
				result.setErrorCode(BaseException.ERROR_NO_DEFAULT_BOOKMARK);
				return result.toOMElement();
			}
			return getPageState(path, "", defaultFilename);
		}catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement getPageState(String path, String name, String createTime){
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean ub = BirstWebService.getUserBean();
			CatalogManager cm = ub.getCatalogManager();
			
			//Retrieve the state by directory for now
			File stateDir = getBookmarkStateFolder(cm, ub, path);
			if(!stateDir.exists()){
				throw new Exception("Cannot find the page state directory - " + path); // do not include the real directory path here
			}
			
			String[] stateFiles = stateDir.list();
			for(int i = 0; i < stateFiles.length; i++){
				String filename = stateFiles[i];
				File stateFile = new File(stateDir.getAbsolutePath() + File.separator + filename);
				if (filename.equals(createTime)){
					OMElement stateXml = readPage(stateFile.getAbsolutePath());
					result.setResult(new XMLWebServiceResult(stateXml));
					return result.toOMElement();
				}
			}
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement deletePageState(String path, String createTime){
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean ub = BirstWebService.getUserBean();
			CatalogManager cm = ub.getCatalogManager();
			
			String bookmarkFile = getBookmarkStateFolder(cm, ub, path) + File.separator + createTime;
			File stateDir = new File(bookmarkFile);
			if(!stateDir.exists()){
				throw new Exception("Bookmark doesn't exist.");
			}
			
			if(!stateDir.delete()){
				throw new Exception("Delete bookmark file failed.");
			}
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement savePageState(String state){
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean ub = BirstWebService.getUserBean();
			CatalogManager cm = ub.getCatalogManager();
			checkForSwapSpaceOperation(cm);
			OMElement el = parseXmlString(state);
			
			File userBookmarkFolder = this.getBookmarkFolder(cm, ub);
			
			//For now, save to a directory
			OMElement path = el.getFirstChildWithName(new QName("", "path"));
			
			if (path != null){
				String pagePath = path.getText().replace("/", "###");
				File stateDir = new File(userBookmarkFolder.getCanonicalPath(), pagePath);
				if (!stateDir.exists()){
					if(!stateDir.mkdir()){
						throw new Exception("Cannot create state directory - " + path);
					}
				}
				
				OMFactory factory = OMAbstractFactory.getOMFactory();
				
				//Use current time as the name of our current page state
				String createTime = Long.toString((new Date()).getTime());
				OMElement createTimeEl = factory.createOMElement(new QName("", "date"));
				createTimeEl.setText(createTime);
				el.addChild(createTimeEl);
				
				String savePath = stateDir.getAbsolutePath() + File.separator + createTime;
				DashboardWebServiceHelper.writeXmlFile(savePath, el);
			}
			
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	private File getBookmarkFolder(CatalogManager cm, SMIWebUserBean ub) throws Exception{	
		File bookmarkFolder = new File(cm.getPrivateDirectory(ub).getAbsolutePath(), ".bookmarks");
		if (!bookmarkFolder.exists()){
			if(!bookmarkFolder.mkdir()){
				throw new Exception("Cannot create bookmark directory"); // do not include the real path directory
			}
		}
		return bookmarkFolder;
	}
	
	private File getBookmarkStateFolder(CatalogManager cm, SMIWebUserBean ub, String path) throws Exception{
		return new File(getBookmarkFolder(cm, ub).getCanonicalPath(), path.replace("/", "###"));
	}
	
	public OMElement saveStickyNote(String pageUuid, String stickyNoteId, String note){
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			
			OMElement el = parseXmlString(note);
			SMIWebUserBean ub = BirstWebService.getUserBean();
			CatalogManager cm = ub.getCatalogManager();
			
			//Generate a id for the note if it is new
			if(stickyNoteId == null || stickyNoteId.equals("")){
				stickyNoteId = java.util.UUID.randomUUID().toString();
			}
			
			//Create the note folder for permissions and write the note file
			File noteDir = getStickyNotesPageFolder(cm, ub, pageUuid, stickyNoteId);
			String noteFile = noteDir.getAbsolutePath() + File.separator + "note";
			DashboardWebServiceHelper.writeXmlFile(noteFile, el);
			
			//Return the note's id
			this.setUuidResult(result, stickyNoteId);
		}catch(Exception e) {
			result.setException(e);
		}
		
		return result.toOMElement(); 	
	}
	
	public OMElement deleteStickyNote(String pageUuid, String stickyNoteUuid){
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			if(pageUuid == null || pageUuid.equals("") || stickyNoteUuid == null || stickyNoteUuid.equals("")){
				throw new Exception("Invalid page / sticky note id");
			}
			SMIWebUserBean ub = BirstWebService.getUserBean();
			CatalogManager cm = ub.getCatalogManager();
			File stickyNoteFile = getStickyNoteFile(cm, ub, pageUuid, stickyNoteUuid);
			if(stickyNoteFile.exists()){
				String errMsg = cm.deleteFile(stickyNoteFile.getAbsolutePath(), ub.getUserGroups(), ub.isSuperUserOwnerOrAdmin());
				if(errMsg != null && !errMsg.isEmpty()){
					logger.error(errMsg);
					result.setErrorMessage(errMsg);
					result.setErrorCode(BaseException.ERROR_PERMISSION);
				}
			}
		}catch(Exception e) {
			result.setException(e);
		}
		
		return result.toOMElement(); 	
	}
	
	public OMElement getStickyNotes(String pageUuid){
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			
			SMIWebUserBean ub = BirstWebService.getUserBean();
			CatalogManager cm = ub.getCatalogManager();
			
			OMElement notesEl = OMAbstractFactory.getOMFactory().createOMElement(DashboardWebServiceHelper.qname(NOTES));
			
			//Get the sticky note directory, iterate and add the file contents
			File notesDir = new File(stickyNoteRootDir(cm) + File.separator + pageUuid);
			if(notesDir.exists()){
				File[] notes = notesDir.listFiles();
				for(int i = 0; i < notes.length; i++){
					String dirNote = notes[i].getAbsolutePath();
					//Check that we can read this
					CatalogPermission perm = cm.getPermission(dirNote);
					if (perm.canView(ub.getUserGroups(), ub.isSuperUserOwnerOrAdmin())){
						OMElement noteEl = this.readPage(dirNote + File.separator + NOTE);
						//Check that we can read this
						String isPub = noteEl.getAttributeValue(DashboardWebServiceHelper.qname(IS_PUB));
						String creator = noteEl.getAttributeValue(DashboardWebServiceHelper.qname(CREATOR));
						if(isPub != null && isPub.equals("false") && 
						   creator != null && !(creator.equals(ub.getUserName())) ){
							continue;
						}
						XmlUtils.addAttribute(OMAbstractFactory.getOMFactory(), noteEl, UUID, notes[i].getName(), null);
						notesEl.addChild(noteEl);
					}
				}
			}
			
			result.setResult(new XMLWebServiceResult(notesEl));
		}catch(Exception e) {
			result.setException(e);
		}
		
		return result.toOMElement(); 	
	}
	
	private File getStickyNotesPageFolder(CatalogManager cm, com.successmetricsinc.UserBean ub, String pageUuid, String noteId) throws Exception{
		File notesDir = new File(stickyNoteRootDir(cm));
		if(!notesDir.exists()){
			checkForSwapSpaceOperation(cm);
			if(!notesDir.mkdir()){
				throw new Exception("Cannot create sticky notes directory");
			}
		}
	
		File pageDir = new File(notesDir.getAbsolutePath() + File.separator + pageUuid);
		if(!pageDir.exists()){
			checkForSwapSpaceOperation(cm);
			if(!pageDir.mkdir()){
				throw new Exception("Cannot create sticky notes page directory - " + pageUuid);
			}
		}
		
		File stickyNotesDir = new File(pageDir.getAbsoluteFile() + File.separator + noteId);
		if(!stickyNotesDir.exists()){
			checkForSwapSpaceOperation(cm);
			if(!stickyNotesDir.mkdir()){
				throw new Exception("Cannot create sticky notes directory - " + noteId);
			}
		}
		return stickyNotesDir;
	}
	
	private File getStickyNoteFile(CatalogManager cm, com.successmetricsinc.UserBean ub, String pageUuid, String noteId){
		String path =  stickyNoteRootDir(cm) + File.separator + pageUuid + File.separator + noteId;
		return new File(path);
	}
	
	private String stickyNoteRootDir(CatalogManager cm){
		return cm.getCatalogRootPath() + File.separator + STICKY_NOTES;
	}
	
	/**
	 * 
	 * @return String result
	 */
	private String getDashboardListOperation(boolean readPages, boolean useMongo) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		OMElement rs = null;
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			
			SMIWebUserBean ub = getUserBean();
			CatalogManager cm = ub.getCatalogManager();
			
			//Need to create quick dashboards page files first
			if(useMongo && DashboardMongoDBCache.useMongoDB()){
				if(DashboardWebServiceHelper.createQuickDashboardPageFilesIfNotExist(ub)){
					DashboardMongoDBCache.regenerateCache(ub);
				}
			}
			
			//Try to retrieve via MongoDB
			if(useMongo && DashboardMongoDBCache.useMongoDB()){
				try{
					return DashboardMongoDBCache.getDashboardList(result, ub, readPages);
				}catch(Exception ex){
					logger.error("Failed to retrieve dashboard list via MongoDB cache", ex);
				}
			}
			
			List<String> dashboards = DashboardWebServiceHelper.getDashboardFoldersFromCM(ub);
			
			boolean hitCache = false;
			//logger.debug("getDashboardListOperation() hasDashboardsChanged() start");
			boolean dashNotChanged = !hasDashboardsChanged(ub, cm, dashboards);
			//logger.debug("getDashboardListOperation() hasDashboardsChanged() end");
			if(dashNotChanged){
				//logger.debug("getDashboardListOperation() getCachedListings() start");
				String dashList = getCachedListings(ub, cm, readPages);
				//logger.debug("getDashboardListOperation() getCachedListings() end");
				if(dashList != null){
					hitCache = true;
					logger.debug("Cache hit for dashboard list");
					return dashList;
				}
			}
			
			if(!hitCache){
				logger.debug("Cache miss for dashboard list, regenerating");
				
				//No cache yet, save the modification dates
				saveUserDashboardDirsModification(ub, cm, dashboards);

				//Recursively read all pages and parse xml
				rs = getDashboardListNoCache(result, dashboards, ub, readPages);
				
				// write the output to disk for cache
				saveCacheListings(ub, cm, rs, readPages);
				
				logger.debug("Cache miss for dashboard list, regeneration done");
			} 
			
		}
		catch (Exception e) {
			result.setException(e);
		}
		
		if(rs == null){
			rs = result.toOMElement();
		}
		
		
		
		return rs.toString();
	}
	
	private OMElement getDashboardListNoCache(BirstWebServiceResult result, List<String> dashboards, SMIWebUserBean ub, boolean readPages) throws SwapSpaceInProgressException{
		CatalogManager cm = ub.getCatalogManager();
		
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;
		OMElement dashboardList = factory.createOMElement("Dashboards", ns);
		
		Map<String, List<File>> dashboardPages = null;
		
		if (dashboards != null) {
			dashboardPages = DashboardWebServiceHelper.getDashboardNameToDirectories(dashboards, cm, null);
			
			List<OMElement> dashElements = new ArrayList<OMElement>();

			for (String name : dashboardPages.keySet()) {

				OMElement dashboard = factory.createOMElement(DASHBOARD, ns);
				XmlUtils.addAttribute(factory, dashboard, NAME, name, ns);

				List<OMElement> pageElements = new ArrayList<OMElement>();

				boolean allPagesInvisible = true;
				for (File folder : dashboardPages.get(name)) {
					addOrder(dashboard, folder);
					if (readPages){
						boolean isAllInvisible = addDashboardPages(folder, dashboard, factory, ns, pageElements);
						if (!isAllInvisible){
							allPagesInvisible = false;
						}
					}else{
						addDashboardPageNamesOnly(folder, dashboard, factory, ns, pageElements);
						allPagesInvisible = false;
					}
				}

				//Mark all pages invisible
				if (allPagesInvisible){
					XmlUtils.addAttribute(factory, dashboard, INVISIBLE, "true", ns);
				}

				//Sort based on <Order>
				if(readPages){
					java.util.Collections.sort(pageElements, OrderComparator.getInstance());
				}

				//Add to <Dashboard>
				for(int y = 0; y < pageElements.size(); y++){
					dashboard.addChild(pageElements.get(y));
				}

				if(pageElements.size() > 0){
					dashElements.add(dashboard);
				}
			}

			//Sort based on <Order>
			if(readPages){
				java.util.Collections.sort(dashElements, OrderComparator.getInstance());
			}

			//Add to <Dashboards>
			for(int y = 0; y < dashElements.size(); y++){
				dashboardList.addChild(dashElements.get(y));
			}
		}

		// add Auto-generated pages if not exist...
		DashboardWebServiceHelper.addAutoGeneratedDashboards(ub, dashboardPages, dashboardList);

		result.setResult(new XMLWebServiceResult(dashboardList));
		
		return  result.toOMElement();
	}
	

	
	//Blow away our cache files
	private void deleteCacheListings(SMIWebUserBean ub, String reason){
		logger.warn("Invalidating dashboard listing cache, reason: " + reason);
		CatalogManager cm = ub.getCatalogManager();
		File rootDir =  new File(getCacheRootDir(cm));
		deleteDirectory(rootDir);
	}
	
	//Recursively delete the directory and its contents
	static private boolean deleteDirectory(File path) {
		if( path.exists() ) {
			File[] files = path.listFiles();
			for(int i=0; i<files.length; i++) {
				if(files[i].isDirectory()) {
					deleteDirectory(files[i]);
				}
				else {
					files[i].delete();
				}
			}
		}
		return( path.delete() );
	}

	private File getCacheUserDirFile(SMIWebUserBean ub, CatalogManager cm){
		return new File(getCacheUserDir(ub, cm));
	}
	
	private String getCacheUserDir(SMIWebUserBean ub, CatalogManager cm){
		return getCacheRootDir(cm) + File.separator + ub.getUserName(); 
	}
	
	private String getCacheRootDir(CatalogManager cm){
		return cm.getCatalogRootPath() + File.separator + "DashboardDirsCache";
	}
	
	private String getCacheFilename(boolean readPages){
		return (readPages) ? "dashboardsPages" : "dashboardsPagesLite";
	}
	
	private String getCachedListings(SMIWebUserBean ub, CatalogManager cm, boolean readPages){
		File parentDir = getCacheUserDirFile(ub, cm);
		if(!parentDir.exists()){
			return null;
		}
		
		File cachedListing = new File(parentDir.getAbsolutePath() + File.separator + getCacheFilename(readPages));
		if(!cachedListing.exists()){
			return null;
		}
		
		try{
			String e = readFile(cachedListing.getAbsolutePath());
			return e;
		}catch(Exception ex){
			logger.error("Unabled to get cached dashboard listing", ex);
		}
		
		return null;
	}
	
	private void saveCacheListings(SMIWebUserBean ub, CatalogManager cm, OMElement cacheEl, boolean readPages) throws SwapSpaceInProgressException{
		File parentDir = getCacheUserDirFile(ub, cm);
		if(!parentDir.exists()){
			// Before creating directory check for swap space operation
			checkForSwapSpaceOperation(cm);
			parentDir.mkdir();
		}
		
		File cachedListing = new File(parentDir.getAbsolutePath() + File.separator + getCacheFilename(readPages));
		try{
			DashboardWebServiceHelper.writeXmlFile(cachedListing.getAbsolutePath(), cacheEl);
		}catch(Exception e){
			logger.error("Unabled to save cached dashboard listing", e);
		}
	}
	
	private boolean hasDashboardsChanged(SMIWebUserBean ub, CatalogManager cm, List<String> dashboards) throws SwapSpaceInProgressException{
		
		HashMap<String, Long> dirMods = getUserDashboardDirsModification(ub, cm);
		int existingSize = dirMods.size();
		int listingSize = dashboards.size();
		
		if(existingSize != listingSize){
			logger.warn("Invalidating dashboard listing cache, reason: number of dashboard tabs changed, cached is " + existingSize + " new listing size " + listingSize);
			return true;
		}
		
		for (String dash : dashboards) {
			File folder = new File(cm.getCatalogRootPath() + File.separator + dash);
			Long fMod = dirMods.get(dash);
			
			if(fMod == null){
				logger.warn("Invalidating dashboard listing cache, reason: new dashboard directory " + dash);
				return true;
			}
			
			if(folder.lastModified() > fMod){
				Date cached = new Date(fMod);
				Date folderMod = new Date(folder.lastModified());
				logger.warn("Invalidating dashboard listing cache, reason: dashboard directory " + dash + " is newer than cache, " + folderMod + " vs " + cached);
				return true;
			}	
		}
		
		return false;
	}
	
	private HashMap<String, Long> getUserDashboardDirsModification(SMIWebUserBean ub, CatalogManager cm) throws SwapSpaceInProgressException{
		HashMap<String, Long> dirs = new HashMap<String, Long>();
	
		File parentDir = new File(getCacheRootDir(cm));
		if(!parentDir.exists()){
			checkForSwapSpaceOperation(cm);
			parentDir.mkdir();
		}
		
		File uDir = getCacheUserDirFile(ub, cm);
		if(!uDir.exists()){
			checkForSwapSpaceOperation(cm);
			uDir.mkdir();
		}
		
		File uFile = new File(uDir.getAbsoluteFile() + File.separator + "dashboardDirModTime");
		if(uFile.exists()){
			try{
				OMElement e = readPage(uFile.getAbsolutePath());
				@SuppressWarnings("rawtypes")
				Iterator itr = e.getChildElements();
				while(itr.hasNext()){
					OMElement f = (OMElement)itr.next();
					String dir = f.getAttributeValue(new QName("", "d"));
					String mod = f.getAttributeValue(new QName("", "m"));
					long modTime = Long.parseLong(mod);
					dirs.put(dir, modTime);
				}
			}catch(Exception e){
				logger.error("getUserDashboardDirsModification(), unable to get user directories modification time", e);
				return dirs;
			}
		}
		return dirs;
	}
	
	private void saveUserDashboardDirsModification(SMIWebUserBean ub, CatalogManager cm, List<String> dirs) throws SwapSpaceInProgressException{
		File parentDir = new File(cm.getCatalogRootPath() + File.separator + "DashboardDirsCache");
		checkForSwapSpaceOperation(cm);
		if(!parentDir.exists()){
			parentDir.mkdir();
		}
		
		File uDir = new File(parentDir.getAbsolutePath() + File.separator + ub.getUserName());
		if(!uDir.exists()){
			uDir.mkdir();
		}
		
		File uFile = new File(uDir.getAbsoluteFile() + File.separator + "dashboardDirModTime");
		
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement c = factory.createOMElement(new QName("", "cache"));
		for(int i = 0; i < dirs.size(); i++){
			String dirPath = dirs.get(i);
			File folder = new File(cm.getCatalogRootPath() + File.separator + dirPath);
			OMElement dir = factory.createOMElement(new QName("", "dir"));
			dir.addAttribute("d", dirs.get(i), null);
			dir.addAttribute("m", Long.toString(folder.lastModified()), null);
			c.addChild(dir);
		}
		try{
			DashboardWebServiceHelper.writeXmlFile(uFile.getAbsolutePath(), c);
		}catch(Exception ex){
			logger.error("Unable to save dashboard dirs modification times", ex);
		}
	}
	
	private void addOrder(OMElement dashEl, File dashFolder){
		String dashxml = dashFolder.getAbsolutePath() + File.separator + DASH_XML_FILE;
		try{
			String order = DashboardWebServiceHelper.getOrder(dashxml).toString();
			OMElement orderEl = dashEl.getFirstChildWithName(new QName("", "Order"));
			if(orderEl == null){
				OMElement newOrderEl = XmlUtils.convertToOMElement("<Order>" + order + "</Order>");
				dashEl.addChild(newOrderEl);
			}else{
				orderEl.setText(order);
			}
		}catch(OMException | FileNotFoundException | XMLStreamException e){
			logger.error("Cannot get dashboard / page order from " + dashxml, e);
		} catch (UnsupportedEncodingException e) {
			logger.error("Cannot get dashboard / page order from " + dashxml, e);
		}
	}
	
	private void checkForSwapSpaceOperation(CatalogManager cm) throws SwapSpaceInProgressException
	{	
		CatalogManager.checkForSwapSpaceOperation(cm);
	}
	
	/**
	 * Just add the dashboard pages' file names to the xml. Used by Adhoc's column dialog box to specify which
	 * dashboard page to drill to.
	 * @param folder
	 * @param dashboardEl
	 * @param factory
	 * @param ns
	 * @param pageElements
	 */
	private void addDashboardPageNamesOnly(File folder, OMElement dashboardEl, OMFactory factory, OMNamespace ns, List<OMElement> pageElements){
		File[] pages = folder.listFiles(DashboardPageOnly.getInstance());
		if(pages != null){
			for (File page : pages){
				if(page.canRead()){
					String pageName = page.getName();
					String name = pageName.substring(0, pageName.toLowerCase().lastIndexOf(DOT_PAGE));
					OMElement pageEl = factory.createOMElement(PAGE, ns);
					pageEl.addAttribute(NAME, name, ns);
					pageElements.add(pageEl);
				}
			}
		}
	}
	
	/**
	 * Reads the dashboard pages' file contents and add them to the xml
	 * @param folder
	 * @param dashboardEl
	 * @param factory
	 * @param ns
	 * @param pageElements
	 */
	@SuppressWarnings("rawtypes")
	private boolean addDashboardPages(File folder, OMElement dashboardEl, OMFactory factory, OMNamespace ns, List<OMElement> pageElements){
		File[] pages = folder.listFiles(DashboardPageOnly.getInstance());
		SMIWebUserBean ub = SMIWebUserBean.getUserBean();
		CatalogManager manager = ub.getCatalogManager();
		final CatalogPermission catalogPermission = manager.getPermission(folder.getAbsolutePath());
		boolean allInvisible = true;
		if(pages != null){
			for (File page : pages){
				FileInputStream fis = null;
				InputStreamReader is = null;
				XMLStreamReader reader = null;
				StAXOMBuilder builder = null;
				
				try{
					//Read contents of file
					fis = new FileInputStream(page);
					is = new InputStreamReader(fis, "UTF-8");
					XMLInputFactory inputFactory = XmlUtils.getSecureXMLInputFactory();
		            reader = inputFactory.createXMLStreamReader(is);

					builder = new StAXOMBuilder(reader);
					OMElement pageContent = builder.getDocumentElement();
				
					//Add the file contents
					if(pageContent != null){
						String pageName = page.getName();
						String name = pageName.substring(0, pageName.lastIndexOf(DOT_PAGE));
						String parentDir = this.getParentPath(page, manager);
						
						//Flag to indicate we need to write back the prompt type to the page
						boolean writeBack = false;
				
						// handle changes to Prompt structure - parse each one assuming that the parsing code will 
						// initialize any new fields as appropriate
						for (Iterator iter = pageContent.getChildElements(); iter.hasNext(); ) {
							OMElement prompts = (OMElement)iter.next();
							if ("Prompts".equals(prompts.getLocalName())) {
								List<Prompt> pList = new ArrayList<Prompt>();
								for (Iterator iter2 = prompts.getChildElements(); iter2.hasNext(); ) {
									OMElement prompt = (OMElement)iter2.next();
									if (! "Prompt".equals(prompt.getLocalName()))
										continue;
									
									prompt.detach();
									Prompt p = new Prompt();
									p.setDashboardPage(parentDir + '/' + pageName);
									if(!p.parseElement(prompt)){
										writeBack = true;
									}
									pList.add(p);
								}
								
								for (Prompt p: pList) {
									p.addContent(prompts, factory, ns);
								}
							}
							else if ("Layout".equals(prompts.getLocalName())) {
								for (Iterator iter2 = prompts.getChildElements(); iter2.hasNext(); ) {
									OMElement layout = (OMElement)iter2.next();
									if ("dashlet".equals(layout.getLocalName())) {
										boolean hasApplyAllPrompts = false;
										for (Iterator iter3 = layout.getChildElements(); iter3.hasNext(); ) {
											OMElement el = (OMElement)iter3.next();
											if ("ApplyAllPrompts".equals(el.getLocalName())) {
												hasApplyAllPrompts = true;
												break;
											}
										}
										if (hasApplyAllPrompts == false) {
											layout.addAttribute("ApplyAllPrompts", Boolean.toString(false), ns);
											writeBack = true;
										}
									}
								}
								
							}
						}
						
						//Check if this page has uuid set. Older pages didn't have them
						String uuid = pageContent.getAttributeValue(DashboardWebServiceHelper.qname(UUID));
						if(uuid == null || uuid.equals("null")){
							DashboardWebServiceHelper.addUuidAttribute(pageContent);
							writeBack = true;
						}
					
						pageContent.addAttribute(NAME, name, ns);
						pageContent.addAttribute(DIR, parentDir, ns);
						pageContent.addAttribute(MODIFIABLE, Boolean.toString(catalogPermission.canModify(ub.getUserGroups(), ub.isSuperUserOwnerOrAdmin())), ns);
						pageContent.detach();
						pageElements.add(pageContent);
						
						//The prompt has type information not available in the file. Write the information back
						if(writeBack){
							if(fis != null){
								try{
									fis.close();
								}catch(IOException e)
								{
									logger.debug(e, e);
								}
								fis = null;
							}
							DashboardWebServiceHelper.writeXmlFile(page.getAbsolutePath(), pageContent);
						}
						
						OMElement invisibleEl = pageContent.getFirstChildWithName(new QName("", INVISIBLE));
						if (allInvisible){
							if (invisibleEl == null || (invisibleEl != null && (!invisibleEl.getText().toUpperCase().equals("TRUE")) ) ){
								allInvisible = false;
							}
						}
					}
				}catch(OMException | IOException | XMLStreamException | SQLException ex){
					logger.error(ex, ex);
				}finally{
					if(is != null){
						try{
							is.close();
						}catch(IOException e)
						{
							logger.debug(e, e);
						}
					}
					if(fis != null){
						try{
							fis.close();
						}catch(IOException e)
						{
							logger.debug(e, e);
						}
					}
					if(reader != null){
						try{
							reader.close();
						}catch(XMLStreamException e)
						{
							logger.debug(e, e);
						}
					}
					if(builder != null){
						try{
							builder.close();
						}catch(Exception e)
						{
							logger.debug(e, e);
						}
					}
				}
			}
		}else{
			return false;
		}
		
		return allInvisible;
	}
	
	private String getParentPath(File page, CatalogManager manager){
		//Get the relative path
		String parentDir = page.getParent();
		String root = manager.getCatalogRootPath();
		int idx = parentDir.indexOf(root);
		if( idx == 0){
			if (parentDir.length() > root.length()){
				parentDir = parentDir.substring(manager.getCatalogRootPath().length() + 1);
				parentDir = parentDir.replace('\\', '/');
			}else{
				parentDir = "";
			}
		}
		return parentDir;
	}
	
	private void setUuidResult(BirstWebServiceResult result, String uuid){
		OMElement id = OMAbstractFactory.getOMFactory().createOMElement(UUID, null);
		id.setText(uuid);
		result.setResult(new XMLWebServiceResult(id));
	}
	
	public static class DashboardPageOnly implements FilenameFilter {
		private static DashboardPageOnly instance = new DashboardPageOnly(); 
		public static DashboardPageOnly getInstance() { return instance; }
		
		public boolean accept(File dir, String name) {
			// Get the file extension
			final int dotIndex = name.lastIndexOf('.');
			if (dotIndex == -1) {
				return false;
			}
			final String fileExtension = name.substring(dotIndex + 1);
			return fileExtension.equalsIgnoreCase("PAGE");
		}
	}
	
	public static class OrderComparator implements java.util.Comparator<OMElement> {
		private static OrderComparator instance = new OrderComparator(); 
		public static OrderComparator getInstance() { return instance; }
		
		@Override
		public int compare(OMElement one, OMElement two) {
			int orderOne = getTextValue(one);
			int orderTwo = (getTextValue(two));
			
			return orderOne - orderTwo;
		}
		
		public int getTextValue(OMElement element){
			int index = MAX_ORDER;
			OMElement order = element.getFirstChildWithName(new QName("", "Order"));
			if(order == null){
				return index;
			}
			try{
				index = Integer.parseInt(order.getText());
			}catch(NumberFormatException ex){
				logger.error("Invalid order number", ex);
			}
			return index;
		}
	}
}
