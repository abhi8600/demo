package com.successmetricsinc.WebServices.dashboard;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.stream.XMLStreamException;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.CommandResult;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.mongodb.MongoException;
import com.mongodb.WriteResult;
import com.successmetricsinc.Group;
import com.successmetricsinc.Repository;
import com.successmetricsinc.UserBean;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.WebServices.BirstWebService;
import com.successmetricsinc.WebServices.BirstWebServiceResult;
import com.successmetricsinc.WebServices.XMLWebServiceResult;
import com.successmetricsinc.WebServices.dashboard.DashboardWebService.DashboardPageOnly;
import com.successmetricsinc.catalog.CatalogManager;
import com.successmetricsinc.catalog.CatalogPermission;
import com.successmetricsinc.query.MongoMgr;
import com.successmetricsinc.util.SwapSpaceInProgressException;
import com.successmetricsinc.util.XmlUtils;

public class DashboardMongoDBCache {
	
	private enum CacheReason {
		NoCollection, CollectionExists, MarkerFileFound, PreviousIncomplete
	};
	private static final String MARKER_FILENAME =  "DashboardMongoCacheRebuild";
	private static final String DBNAME = "BirstDashboards";
	private static final String ID = "_id";
	private static final String DASH_ORDER = "dashboardOrder";
	private static final String DASH_PATHS = "dashboardPaths";
	private static final String ATTR_NAME = "@name";
	private static final String ATTR_DIR = "@dir";
	private static final String ATTR_UUID = "@uuid";
	private static final String ORDER = "order";
	private static final String PAGE_SUFFIX = ".page";
	private static final String MARKER_ID = "CachingInProgress";
	private static final String START_TIME = "startTime";
	private static final String DASH_PR_HOSTNAME = "DashboardMongoHostname";
	private static final String PROMPT = "Prompt";
	
	private static Boolean propUseMongo = null;
	private static Integer propPartialPagesThreshold = null;
	
	public static final String USE_MONGO = "dashboardUseMongoDB";
	public static final String PARTIAL_PAGES_THRESHOLD = "partialPagesThreshold";
	public static final int DEFAULT_PARTIAL_PAGES_THRESHOLD = 50;
	
	private static Logger logger = Logger.getLogger(DashboardMongoDBCache.class);
	private static Mongo mongo;
	
	public static boolean useMongoDB(){
		if(propUseMongo == null){
			String propValue = System.getProperty(USE_MONGO);
			boolean value = (propValue != null) ? Boolean.parseBoolean(propValue) : true;
			propUseMongo = Boolean.valueOf(value);
		}
		return propUseMongo;
	}
	
	public static int partialPagesThreshold(){
		if(propPartialPagesThreshold == null){
			try{
				int envValue = Integer.parseInt(System.getProperty(PARTIAL_PAGES_THRESHOLD));
				if(envValue < 0){
					envValue = DEFAULT_PARTIAL_PAGES_THRESHOLD; 
				}
				propPartialPagesThreshold = Integer.valueOf(envValue);	
			}catch(Exception ex){
				propPartialPagesThreshold = Integer.valueOf(DEFAULT_PARTIAL_PAGES_THRESHOLD);
			}
		}
		return propPartialPagesThreshold;
	}
	
	/**
	 * Remove the cache collection
	 * @param ub
	 * @throws UnknownHostException
	 */
	public static void dropCollections(com.successmetricsinc.UserBean ub) throws UnknownHostException{
		DB db = getDB();
		DBCollection coll = DashboardMongoDBCache.getPageCollection(db, ub);
		coll.drop();
	}
	
	/**
	 * Deletes the cache and re-do.
	 * @param ub
	 * @throws UnknownHostException
	 * @throws SwapSpaceInProgressException 
	 * @throws UnsupportedEncodingException 
	 */
	public static void regenerateCache(SMIWebUserBean ub) throws UnknownHostException, SwapSpaceInProgressException, UnsupportedEncodingException{
		dropCollections(ub);
		cacheDashboardsToMongoDB(ub);
	}
	
	/**
	 * Determines if a rebuild of the cache is required - nonexistent collection, marker file, previously incomplete rebuild
	 * @param ub
	 * @param db
	 * @return
	 * @throws UnknownHostException
	 */
	private static CacheReason cacheBuildRequired(SMIWebUserBean ub, DB db) throws UnknownHostException{
		String collectionName = pageCollectionName(ub);
		
		//Check for a file marker
		File mkfile = markerFile(ub);
		if(mkfile.exists()){
			if(db.collectionExists(collectionName)){
				db.getCollection(collectionName).drop();
			}
			return CacheReason.MarkerFileFound;
		}
		
		//No collection, we build one
		if(!db.collectionExists(collectionName)){
			return CacheReason.NoCollection;
		}else{
			DBCollection coll = db.getCollection(collectionName);
			BasicDBObject markerCriteria = new BasicDBObject(ID, MARKER_ID);
			BasicDBObject marker = (BasicDBObject)coll.findOne(markerCriteria);
			if(marker != null){
				// Check when the caching was last started. If it was more than 30 minutes
				// ago, we assume that caching was unsuccessful and we redo it
				long startTime = marker.getLong(START_TIME);
				long currentTime = (new java.util.Date()).getTime();
				long diff = currentTime - startTime;
				if(diff / (1000 * 60) > 30){
					logger.warn("Last dashboard cache attempt was more than 30 minutes ago, assuming failed and retrying");
					coll.drop();
					return CacheReason.PreviousIncomplete;
				}
			}
		}
		
		return CacheReason.CollectionExists;
	}
	
	private static File markerFile(SMIWebUserBean ub){
		CatalogManager cm = ub.getCatalogManager();
		return new File(cm.getCatalogRootPath() + File.separator + MARKER_FILENAME);
	}
	
	public static void createMarkerFile(SMIWebUserBean ub){
		File mkfile = markerFile(ub);
		try {
			mkfile.createNewFile();
		} catch (IOException e) {
			logger.error("Cannot create dashboard cache recreate marker file", e);
		}
	}
	
	/**
	 * Returns the list of all the dashboard directory paths
	 * @param ub
	 * @return
	 * @throws SwapSpaceInProgressException 
	 */
	private static List<String> getAllDashboardDirectories(SMIWebUserBean ub) throws SwapSpaceInProgressException{
		//Set us to be superuser so we can get the whole list
		boolean su = ub.isSuperUser();
		ub.setSuperUser(true);
		ub.setRootDirectories(null);
		CatalogManager cm = ub.getCatalogManager();
		List<String> dashboards = cm.getDashboardFolders(ub);
		ub.setSuperUser(su);
		ub.setRootDirectories(null);
		return dashboards;
	}
	
	/**
	 * Re-reads the dashboard order from the dashboard directory's dashboard.xml, remove and insert the new dashboards orders
	 * row in cache. Also rewrites the dashboard paths
	 * @param ub
	 */
	public static void regenerateDashboardsOrdersPaths(SMIWebUserBean ub){
		try{
			List<String> dashboards = getAllDashboardDirectories(ub);
			CatalogManager cm = ub.getCatalogManager();
			BasicDBObject dashboardPaths = new BasicDBObject(ID, DASH_PATHS);
			Map<String, List<File>> dashboardPages = DashboardWebServiceHelper.getDashboardNameToDirectories(dashboards, cm, dashboardPaths);
			BasicDBObject dashboardOrder = new BasicDBObject(ID, DASH_ORDER);

			for (String name : dashboardPages.keySet()) {
				for (File folder : dashboardPages.get(name)) {
					if(folder.listFiles(DashboardPageOnly.getInstance()).length > 0){
						int order = DashboardWebServiceHelper.getDashboardOrder(folder);
						dashboardOrder.put(name, order);
						break;
					}
				}
			}

			logger.debug("regenerateDashboardsOrderPaths() write to MongoDB order - " + dashboardOrder);
			
			//Write order
			DB db = getDB();
			db.requestStart();
			DBCollection coll = DashboardMongoDBCache.getPageCollection(db, ub);
			CommandResult rs = coll.update(new BasicDBObject(ID, DASH_ORDER), dashboardOrder).getLastError();
			if(!rs.ok()){
				logger.error("regeneratedDashboardsOrderPath() writing dashboard order failed: " + rs.getErrorMessage());
				db.requestDone();
				throw rs.getException();
			}
			
			rs = coll.update(new BasicDBObject(ID, DASH_PATHS), dashboardPaths).getLastError();
			if(!rs.ok()){
				logger.error("regeneratedDashboardsOrderPath() writing dashboard path failed: " + rs.getErrorMessage());
				db.requestDone();
				throw rs.getException();
			}
			
			db.requestDone();
		}catch(Exception ex){
			logger.error("Failed to update dashboard order", ex);
			createMarkerFile(ub);
		}
	}
	
	/**
	 * Check first if we need to rebuild the cache, then from the list of dashboard subdirectories, read all the dashboard pages files,
	 * convert them from xml to json to Mongo's BSON object and insert each into our collection. Set and delete a row marker in the 
	 * collection before and after cache operation.
	 * @param ub
	 * @throws UnknownHostException
	 * @throws SwapSpaceInProgressException 
	 */
	public static void cacheDashboardsToMongoDB(SMIWebUserBean ub) throws UnknownHostException, UnsupportedEncodingException, SwapSpaceInProgressException{
		Date start = new Date();

		DB db = getDB();
		db.requestStart();
		
		//Check if we need to build the cache first
		CacheReason reason = cacheBuildRequired(ub, db);
		if(reason == CacheReason.CollectionExists){
			db.requestDone();
			return;
		}
		
		CatalogManager cm = ub.getCatalogManager();
		
		List<String> dashboards = getAllDashboardDirectories(ub);
		DBCollection coll = DashboardMongoDBCache.getPageCollection(db, ub);
		coll.ensureIndex(new BasicDBObject(DashboardWebService.ORDER, 1));
		coll.ensureIndex(new BasicDBObject(ATTR_DIR, 1));
		
		//Set a marker such that other cacheDashboardsToMongoDB() do not try to build the cache too
		BasicDBObject marker = new BasicDBObject(ID, MARKER_ID);
		marker.append(START_TIME, (new Date()).getTime());
		CommandResult rs = coll.insert(marker).getLastError();
		if(!rs.ok()){
			db.requestDone();
			throw rs.getException();
		}
		
		if (dashboards != null) {
			BasicDBObject dashboardPaths = new BasicDBObject(ID, DASH_PATHS);
			Map<String, List<File>> dashboardPages = DashboardWebServiceHelper.getDashboardNameToDirectories(dashboards, cm, dashboardPaths);
			HashMap<String, Integer> uuidMap = new HashMap<String, Integer>();
			BasicDBObject dashboardOrder = new BasicDBObject(ID, DASH_ORDER);
			
			for (String name : dashboardPages.keySet()) {
				dashboardOrder.put(name, 9999);
				//Read the pages into JSON format and populate to the page collection
				boolean readOrder = false;
				for (File folder : dashboardPages.get(name)) {
					//Read the list of pages and seriealize them
					List<DBObject> jsonPages = new ArrayList<DBObject>();
					addDashboardPagesJson(folder, jsonPages, uuidMap); //addDashboardPages(folder, dashboard, factory, ns, pageElements);
					
					//Read the dashboard order, only the first one
					if(!readOrder && jsonPages.size() > 0){
						int order = DashboardWebServiceHelper.getDashboardOrder(folder);
						dashboardOrder.put(name, order);
						readOrder = true;
					}
					
					//Save pages to Mongodb
					if(jsonPages.size() > 0){
						coll.insert(jsonPages);
					}
					
				}// for(File folder :..
				
			}
			
			//Write the dashboard order
			logger.debug("cacheDashboardsToMongoDB() write to MongoDB order - " + dashboardOrder);
			coll.insert(dashboardOrder);
			coll.insert(dashboardPaths);
			
			db.requestDone();
			
			//Remove the collection marker
			rs = coll.remove(marker).getLastError();
			if(!rs.ok()){
				db.requestDone();
				throw rs.getException();
			}
			
			//Remove file marker
			if(reason == CacheReason.MarkerFileFound){
				File mkfile = markerFile(ub);
				if(mkfile.exists()){
					if(!mkfile.delete()){
						logger.error("Cannot delete the dashboard file marker this would cause excessing cache rebuilds! File is " + mkfile.getAbsolutePath());
					}
				}
			}
			
			long diff = (new Date()).getTime() - start.getTime();
			logger.info("Generated MongoDB dashboard cache in " + diff + " milisecs");
		}
	}
	
	/**
	 * Remove the rows of page entries based on the @dir attribute corresponding 
	 * to our dashboard
	 * @param ub
	 * @param path
	 */
	public static void deleteDashboard(SMIWebUserBean ub, String path){
		try{
			DB db = getDB();

			db.requestStart();
			DBCollection page = getPageCollection(db, ub);
			BasicDBObject criterion = new BasicDBObject(ATTR_DIR, path);
			WriteResult result = page.remove(criterion);
			db.requestDone();

			CommandResult err = result.getLastError();
			if(!err.ok()){
				throw err.getException();
			}
		}catch(Exception ex){
			logger.error("Cannot delete dashboard from cache", ex);
			createMarkerFile(ub);
		}
	}
	
	/**
	 * Delete the page based on the page path
	 * @param ub
	 * @param path
	 */
	public static void deletePage(SMIWebUserBean ub, String path){
		try{
			String pageFile = getPageName(path);
			String parentPath = getParentPath(path);

			DB db = getDB();

			db.requestStart();
			DBCollection page = getPageCollection(db, ub);

			//{ "@dir" : "XXX/Dashboard", "@name" : "page" }
			BasicDBObject criterion = new BasicDBObject(ATTR_DIR, parentPath);
			criterion.append(ATTR_NAME, pageFile);

			WriteResult result = page.remove(criterion);
			db.requestDone();

			CommandResult err = result.getLastError();
			if(!err.ok()){
				throw err.getException();
			}
		}catch(Exception ex){
			logger.error("Cannot delete page from cache", ex);
			createMarkerFile(ub);
		}
	}
	
	/**
	 * Read the row page information from db
	 * @param ub
	 * @param uuid
	 * @return
	 * @throws UnknownHostException
	 * @throws JSONException
	 */
	public static OMElement readPage(com.successmetricsinc.UserBean ub, String uuid) throws UnknownHostException, JSONException{
		BasicDBObject page = getPageFromDB(ub, uuid);
		return pageElementFromDBObject(page);
	}
	
	private static BasicDBObject getPageFromDB(com.successmetricsinc.UserBean ub, String uuid) throws UnknownHostException{
		DB db = getDB();
		db.requestStart();
		DBCollection coll = getPageCollection(db, ub);
		BasicDBObject query = new BasicDBObject(ID, uuid);
		BasicDBObject page = (BasicDBObject)coll.findOne(query);
		db.requestDone();
		
		return page;
	}
	
	public static String readPageInJson(com.successmetricsinc.UserBean ub, String uuid) throws IOException{
		BasicDBObject page = getPageFromDB(ub, uuid);
		if(page != null){
			page.removeField(ID);
			BasicDBList layoutV2 = getLayoutMetadata(uuid);
			if(layoutV2 != null){
				page.append("LayoutV2", layoutV2);
			}
			return com.mongodb.util.JSON.serialize(page);
		}
		return null;
	}
	
	private static BasicDBList getLayoutMetadata(String uuid) throws IOException{
		String path = getPageIntermediateMetaDataDir() + File.separator + uuid + ".layout";
		return (BasicDBList) readJsonToDBObject(path);
	}
	
	private static Object readJsonToDBObject(String path) throws IOException{
		File metadataFile = new File(path);
		if(metadataFile.exists()){
			byte[] data = Files.readAllBytes(Paths.get(metadataFile.getAbsolutePath()));
			String jsonStr = StandardCharsets.UTF_8.decode(ByteBuffer.wrap(data)).toString();
			return com.mongodb.util.JSON.parse(jsonStr);
		}
		
		return null;
	}
	
	public static void writeLayoutMetadata(String uuid, BasicDBList layoutV2) throws FileNotFoundException, UnsupportedEncodingException{
		String path = getPageIntermediateMetaDataDir() + File.separator + uuid + ".layout";
		printWriterPrint(path, layoutV2);
	}
	
	private static void printWriterPrint(String path, Object obj) throws FileNotFoundException, UnsupportedEncodingException{
		PrintWriter writer = new PrintWriter(path, "UTF-8");
		writer.print(obj);
		writer.close();
	}
	
	public static BasicDBList getSharablePrompts(String[] uuids) throws IOException{
		BasicDBList list = new BasicDBList();
		for (String uuid: uuids){
			String path = getPromptMetaDataDir() + File.separator + uuid;
			BasicDBObject pObj = (BasicDBObject)readJsonToDBObject(path);
			if(pObj != null){
				pObj.put("uuid", uuid);
				list.add(pObj);
			}
		}
		
		return list;
	}
	
	public static BasicDBList getAllSharablePrompts(){
		BasicDBList list = new BasicDBList();
		File promptsDir = new File(getPromptMetaDataDir());
		if(promptsDir.exists()){
			String[] promptFiles = promptsDir.list();
			for(String promptFile : promptFiles){
				try {
					BasicDBObject pObj = (BasicDBObject)readJsonToDBObject(promptsDir + File.separator + promptFile);
					if(pObj != null){
						pObj.put("uuid", promptFile);
						list.add(pObj);
					}
				} catch (IOException e) {
					logger.error("Unable to read prompt file: " + promptFile, e);
				}
			}
		}
		return list;
	}
	
	public static void writeSharablePrompt(String uuid, BasicDBObject prompt) throws FileNotFoundException, UnsupportedEncodingException{
		String path = getPromptMetaDataDir() + File.separator + uuid;
		printWriterPrint(path, prompt);
	}
	
	public static boolean deleteSharablePrompt(String uuid){
		String path = getPromptMetaDataDir() + File.separator + uuid;
		File f = new File(path);
		if(!f.exists()){
			return true;
		}
		
		return f.delete();
	}
	
	public static boolean existsSharablePrompt(String uuid){
		String path = getPromptMetaDataDir() + File.separator + uuid;
		File f = new File(path);
		return f.exists();
	}
	
	private static String getPromptMetaDataDir(){
		return getMetaDataDir("PromptMetaData");
	}
	
	private static String getPageIntermediateMetaDataDir(){
		return getMetaDataDir("PageMetadata");
	}
	
	private static String getMetaDataDir(String name){
		SMIWebUserBean ub = BirstWebService.getUserBean();
		String catalogPath;
		if(ub == null){
			UserBean userBean = UserBean.getParentThreadUserBean();
			Repository rep = userBean.getRepository();
            catalogPath = rep.getServerParameters().getApplicationPath() + "\\catalog";
		}else{
			catalogPath = ub.getCatalogPath();
		}
		
		String metadataPath = catalogPath + File.separator + name;
		File f = new File(metadataPath);
		if (!f.exists()){
			f.mkdir();
		}
		return metadataPath;
	}
	
	public static OMElement pageElementFromDBObject(BasicDBObject row) throws JSONException{
		OMElement page = null;
		if(row != null){
			String uuid = row.getString(ID);
			
			row.removeField(ID);
			
			BasicDBList layoutV2 = null;
			try {
				layoutV2 = getLayoutMetadata(uuid);
			} catch (IOException e) {
				logger.error("Failed to read layout v2 for page " + uuid, e);
			}
			
			if(layoutV2 != null){
				row.append("LayoutV2", layoutV2);
			}
						
			String jsonStr = com.mongodb.util.JSON.serialize(row);
			JSONObject obj = new JSONObject(jsonStr);

			String pageXml = org.json.XML.toString(obj, DashboardWebService.PAGE); 
			page = XmlUtils.convertToOMElement(pageXml);
		}
		
		return page;
	}
	
	public static OMElement promptElementFromDBObject(BasicDBObject row) throws JSONException{
		OMElement prompt = null;
		if(row != null){
			row.removeField(ID);
			String jsonStr = com.mongodb.util.JSON.serialize(row);
			JSONObject obj = new JSONObject(jsonStr);

			String pageXml = org.json.XML.toString(obj, PROMPT); 
			prompt = XmlUtils.convertToOMElement(pageXml);
		}
		
		return prompt;
	}
	
	/**
	 * Update the page's @dir path to the new @dir path
	 * @param ub
	 * @param pagePath
	 * @param dashboardPath
	 * @param order
	 * @throws UnknownHostException
	 * @throws MongoException
	 */
	public static void movePageToDashboard(SMIWebUserBean ub, String pagePath, String dashboardPath, int order) throws UnknownHostException, MongoException{
		try{
			String previousDashPath = getParentPath(pagePath);
			String pageName = getPageName(pagePath);

			DB db = getDB();
			db.requestStart();
			DBCollection page = getPageCollection(db, ub);

			//{ "@name" : "pageName", "@dir" : "shared/OldDashboard/pageName" }
			BasicDBObject criteria = new BasicDBObject(ATTR_NAME, pageName);
			criteria.append(ATTR_DIR, previousDashPath);

			//{ "$set" : {"@dir" : "shared/NewPath", "Order" : 1} }
			BasicDBObject updateFields = new BasicDBObject(ATTR_DIR, dashboardPath);
			updateFields.append(DashboardWebService.ORDER, order);
			BasicDBObject setFields = new BasicDBObject("$set", updateFields);
			WriteResult result = page.update(criteria, setFields);
			db.requestDone();

			CommandResult err = result.getLastError();
			if(!err.ok()){
				throw err.getException();
			}
		}catch(Exception ex){
			logger.error("Cannot move page to dashboard in cache", ex);
			createMarkerFile(ub);
		}
	}
	
	/**
	 * Change the @dir value for all pages which matches the old @dir value
	 * @param ub
	 * @param newName
	 * @param oldPath
	 * @throws UnknownHostException
	 * @throws MongoException
	 */
	public static void renameDashboard(SMIWebUserBean ub, String newName, String oldPath) throws UnknownHostException, MongoException{
		try{
			String parentPath = getParentPath(oldPath);
			String newPath = parentPath + "/" + newName;

			DB db = getDB();
			db.requestStart();
			DBCollection page = getPageCollection(db, ub);

			//{ "@dir" : "shared/OldDashboardName" }
			BasicDBObject criteria = new BasicDBObject(ATTR_DIR, oldPath);

			//{ "$set" : {"@dir" : "shared/NewPath", "Order" : 1} }
			BasicDBObject updateFields = new BasicDBObject(ATTR_DIR, newPath);
			BasicDBObject setFields = new BasicDBObject("$set", updateFields);
			WriteResult result = page.update(criteria, setFields, false, true);
			db.requestDone();

			CommandResult err = result.getLastError();
			if(!err.ok()){
				throw err.getException();
			}
			
			//Update the paths.
			DashboardMongoDBCache.regenerateDashboardsOrdersPaths(ub);
		}catch(Exception ex){
			logger.error("Cannot rename dashboard in cache", ex);
			createMarkerFile(ub);
		}
	}
	
	/**
	 * Update the Order value of the page
	 * @param ub
	 * @param pagePath
	 * @param index
	 */
	public static void savePageOrder(SMIWebUserBean ub, String pagePath, int index){
		try{
			DB db = getDB();
			db.requestStart();
			DBCollection page = getPageCollection(db, ub);

			String parentPath = getParentPath(pagePath); 
			String pageName = getPageName(pagePath);

			//{ "@dir" : "shared/OldDashboardName", "@name" : "Page" }
			BasicDBObject criteria = new BasicDBObject(ATTR_DIR, parentPath);
			criteria.append(ATTR_NAME, pageName);

			//{ "$set" : {"Order" : 1} }
			BasicDBObject updateFields = new BasicDBObject(DashboardWebService.ORDER, index);
			BasicDBObject setFields = new BasicDBObject("$set", updateFields);
			WriteResult result = page.update(criteria, setFields, false, true);
			db.requestDone();

			CommandResult err = result.getLastError();
			if(!err.ok()){
				throw err.getException();
			}
		}catch(Exception ex){
			logger.error("Cannot save page order in cache", ex);
			createMarkerFile(ub);
		}
	}
	
	private static String getPageName(String path){
		//Get page name only
		File f = new File(path);
		String pageFile = f.getName();
		String parentPath = f.getParent();
		parentPath = parentPath.replace('\\', '/');
		if(pageFile.endsWith(PAGE_SUFFIX)){
			pageFile = pageFile.substring(0, pageFile.length() - PAGE_SUFFIX.length());
		}
		return pageFile;
	}
	
	private static String getParentPath(String pagePath){
		File f = new File(pagePath);
		String parentPath = f.getParent();
		parentPath = parentPath.replace('\\', '/');
		return parentPath;
	}
	
	/**
	 * Update the page row contents from the page file contents
	 * @param ub
	 * @param pageFile
	 */
	public static void upsertPage(SMIWebUserBean ub, File pageFile){
		try{
			if(pageFile != null){
				DB db = getDB();

				db.requestStart();

				//Update if there is an existing record
				DBCollection coll = getPageCollection(db, ub);
				BasicDBObject pageObj = DashboardMongoDBCache.createPageDBObjectFromFile(pageFile, ub.getCatalogManager(), null);
				BasicDBObject existingRecord = new BasicDBObject(ID, pageObj.getString(ID));
				WriteResult rst = coll.update(existingRecord, pageObj, true, false);

				db.requestDone();

				CommandResult cmd = rst.getLastError();
				if(!cmd.ok()){
					throw cmd.getException();
				}
			}
		}catch(Exception ex){
			logger.error("Cannot upsert page to cache", ex);
			createMarkerFile(ub);
		}
	}
	
	/**
	 * Update the row containing the dashboards ordering
	 * @param ub
	 * @param order
	 */
	public static void upsertDashboardOrder(SMIWebUserBean ub, Map<String, Integer>order){
		try{
			DB db = getDB();

			db.requestStart();
			db.requestDone();
			DBCollection coll = getPageCollection(db, ub);

			BasicDBObject obj = new BasicDBObject(ID, DASH_ORDER);

			BasicDBObject info = new BasicDBObject(order);
			info.append(ID, DASH_ORDER);

			logger.debug("upsertDashboardOrder() write to MongoDB order - " + order);
			
			WriteResult rst = coll.update(obj, info, true, false);
			CommandResult cmd = rst.getLastError();

			if(!cmd.ok()){
				throw cmd.getException();
			}
		}catch(Exception ex){
			logger.error("Cannot upsert dashboard order to cache", ex);
			createMarkerFile(ub);
		}
	}
	
	public static void addDashboardPagesJson(File folder, List<DBObject> jsonPages, Map<String, Integer>uuidMap){
		File[] pages = folder.listFiles(DashboardPageOnly.getInstance());
		SMIWebUserBean ub = SMIWebUserBean.getUserBean();
		CatalogManager manager = ub.getCatalogManager();
		if(pages != null){
			for (File page : pages){
				try{
					BasicDBObject dbObject = createPageDBObjectFromFile(page, manager, uuidMap);
					jsonPages.add(dbObject);
				}catch(Exception ex){
					logger.error("JSON read exception", ex);
					continue;
				}
			}
		}
	}

	/**
	 * Read the page file, convert it from xml to json to Mongo's bson object. Check for existence of the page uuid and generated if 
	 * required. If the uuid exists and clashes with an existing uuid, regenerate a new uuid, rewrite it to the file and redo the conversion.
	 * @param page
	 * @param manager
	 * @param uuidMap
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws JSONException
	 * @throws XMLStreamException
	 */
	public static BasicDBObject createPageDBObjectFromFile(File page, CatalogManager manager, Map<String, Integer>uuidMap) throws FileNotFoundException, IOException, JSONException, XMLStreamException{
		String parentDir = DashboardWebServiceHelper.getParentPath(page, manager);
		String fileContents = DashboardWebServiceHelper.readFile(page.getAbsolutePath());
		
		JSONObject json = XML.toJSONObject(fileContents);
		JSONObject pageObj = json.getJSONObject(DashboardWebService.PAGE);
		
		String uuid = null;
		try{
			uuid = pageObj.getString(ATTR_UUID);
			if(uuidMap != null && uuidMap.containsKey(uuid)){
				throw new JSONException("Duplicate uuid");
			}
			if(uuidMap != null){
				uuidMap.put(uuid, 1);
			}
		}catch(JSONException ex){
			//No uuid, generate uuid, write to file, read file again, recreate object
			OMElement pageXml = XmlUtils.convertToOMElement(fileContents);
			DashboardWebServiceHelper.addUuidAttribute(pageXml);
			DashboardWebServiceHelper.writeXmlFile(page.getAbsolutePath(), pageXml);
			fileContents = DashboardWebServiceHelper.readFile(page.getAbsolutePath());
			json = XML.toJSONObject(fileContents);
			pageObj = json.getJSONObject(DashboardWebService.PAGE);
			uuid = pageObj.getString(ATTR_UUID);
			if(uuidMap != null){
				uuidMap.put(uuid, 1);
			}
		}
		
		String jsonStr = pageObj.toString();
		BasicDBObject dbObject = (BasicDBObject)com.mongodb.util.JSON.parse(jsonStr);
		dbObject.put(ID, uuid);
		
		dbObject.put(ATTR_DIR, parentDir); //change the @dir property as it could have been modified
		Object name = dbObject.get(ATTR_NAME);
		if(!(name instanceof String)){ //Sometimes json thinks it is an integer or double, etc. convert it to string
			dbObject.put(ATTR_NAME, name.toString());
		}
		return dbObject;
	}
	
	/**
	 * Similar to DashboardWebService.getDashboardList(), returns the xml of the list of dashboard and pages from cache.
	 * @param result
	 * @param dashboards
	 * @param ub
	 * @param readPages
	 * @return
	 * @throws JSONException 
	 * @throws SwapSpaceInProgressException 
	 * @throws IOException 
	 */
	public static String getDashboardList(BirstWebServiceResult result, SMIWebUserBean ub, boolean readPages) throws JSONException, SwapSpaceInProgressException, IOException{
		OMElement dashboardsEl = getDashboardListOMElement(ub, readPages);
		result.setResult(new XMLWebServiceResult(dashboardsEl));
		String rs = result.toOMElement().toString();
		
		return rs;
	}
	
	public static OMElement getDashboardListOMElement(SMIWebUserBean ub, boolean readPages) throws JSONException, SwapSpaceInProgressException, IOException{
		//Cache if required
		cacheDashboardsToMongoDB(ub);
		
		CatalogManager cm = ub.getCatalogManager();
		
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;
		OMElement dashboardsEl = factory.createOMElement("Dashboards", ns);
		
		Map<String, List<File>> dashboardPages = getDashboardPaths(ub);
		
		if(dashboardPages != null){
			DashboardOrderComparator comparator = new DashboardOrderComparator(ub);
			ArrayList<String> dashes = new ArrayList<String>(dashboardPages.keySet());
			Collections.sort(dashes, comparator);
			
			DB db = getDB();
			DBCollection pageColl = getPageCollection(db, ub);
			pageColl.ensureIndex(new BasicDBObject(DashboardWebService.ORDER, 1));
			pageColl.ensureIndex(new BasicDBObject(ATTR_DIR, 1));
			
			boolean optimizeReadPartialPage = pageColl.getCount() > partialPagesThreshold();
			
			for (String name : dashes) {
				OMElement dashboardEl = factory.createOMElement(DashboardWebService.DASHBOARD, ns);
				XmlUtils.addAttribute(factory, dashboardEl, DashboardWebService.NAME, name, ns);

				boolean allPagesInvisible = false;
				BasicDBList dashboardPaths = new BasicDBList();
				List<File> pages = dashboardPages.get(name);
				for (File folder : pages) {
					String path = DashboardWebServiceHelper.getParentPath(folder, cm) + "/" + folder.getName();
					dashboardPaths.add(path);
				}
				BasicDBObject query = new BasicDBObject(ATTR_DIR, new BasicDBObject("$in", dashboardPaths));
			
				String firstUuid = null;
				if(optimizeReadPartialPage){
					addPageToDashboard(true, dashboardEl, db, pageColl, query, 1, null);
					OMElement pageEl = dashboardEl.getFirstChildWithName(DashboardWebServiceHelper.qname(DashboardWebService.PAGE));
					if(pageEl != null){
						firstUuid = pageEl.getAttributeValue(DashboardWebServiceHelper.qname(DashboardWebService.UUID));
					}
				}
				allPagesInvisible = addPageToDashboard(!optimizeReadPartialPage, dashboardEl, db, pageColl, query, -1, firstUuid);
				
				
				//Mark all pages invisible
				if (allPagesInvisible){
					XmlUtils.addAttribute(factory, dashboardEl, DashboardWebService.INVISIBLE, "true", ns);
				}
				
				dashboardsEl.addChild(dashboardEl);
			}
		}

		return dashboardsEl;
	}
	
	public static BasicDBList getDashboardListDBObject(SMIWebUserBean ub, boolean readPages) throws JSONException, SwapSpaceInProgressException, IOException{
		//Cache if required
		cacheDashboardsToMongoDB(ub);
		
		CatalogManager cm = ub.getCatalogManager();
		
		BasicDBList dashboardsEl = new BasicDBList();
		
		Map<String, List<File>> dashboardPages = getDashboardPaths(ub);
		
		if(dashboardPages != null){
			DashboardOrderComparator comparator = new DashboardOrderComparator(ub);
			ArrayList<String> dashes = new ArrayList<String>(dashboardPages.keySet());
			Collections.sort(dashes, comparator);
			
			DB db = getDB();
			DBCollection pageColl = getPageCollection(db, ub);
			pageColl.ensureIndex(new BasicDBObject(DashboardWebService.ORDER, 1));
			pageColl.ensureIndex(new BasicDBObject(ATTR_DIR, 1));
			
			boolean optimizeReadPartialPage = pageColl.getCount() > partialPagesThreshold();
			
			for (String name : dashes) {
				BasicDBObject dashboardEl = new BasicDBObject(DashboardWebService.NAME, name);

				boolean allPagesInvisible = false;
				BasicDBList dashboardPaths = new BasicDBList();
				List<File> pages = dashboardPages.get(name);
				for (File folder : pages) {
					String path = DashboardWebServiceHelper.getParentPath(folder, cm) + "/" + folder.getName();
					dashboardPaths.add(path);
				}
				BasicDBObject query = new BasicDBObject(ATTR_DIR, new BasicDBObject("$in", dashboardPaths));
			
				String firstUuid = null;
				if(optimizeReadPartialPage){
					addPageToDashboardDBObject(true, dashboardEl, db, pageColl, query, 1, null);
					BasicDBList pagesEl = (BasicDBList)dashboardEl.get("pages");
					if(pagesEl != null && pagesEl.size() > 0){
						firstUuid =  ((BasicDBObject)pagesEl.get(0)).getString(DashboardWebService.UUID);
					}
				}
				allPagesInvisible = addPageToDashboardDBObject(!optimizeReadPartialPage, dashboardEl, db, pageColl, query, -1, firstUuid);
				
				
				//Mark all pages invisible
				if (allPagesInvisible){
					dashboardEl.put(DashboardWebService.INVISIBLE, true);
				}
				
				dashboardsEl.add(dashboardEl);
			}
		}

		return dashboardsEl;
	}
	
	/**
	 * Retrieve from MongoDB the list of dashboard directory paths and filter them per user's permission.
	 * Create and insert the list if it doesn't exist in the db
	 * @param ub
	 * @return
	 * @throws IOException
	 * @throws SwapSpaceInProgressException
	 */
	private static Map<String, List<File>> getDashboardPaths(SMIWebUserBean ub) throws IOException, SwapSpaceInProgressException{
		long start = System.currentTimeMillis();
		
		
		Map<String, List<File>> dashboardPages = new HashMap<String, List<File>>();
		
		DB db = getDB();
		db.requestStart();
		
		DBCollection pageColl = getPageCollection(db, ub);
		BasicDBObject query = new BasicDBObject(ID, DASH_PATHS);
		BasicDBObject dbObj = (BasicDBObject)pageColl.findOne(query);
		if(dbObj != null){
			viewableDashboards(ub, dbObj, dashboardPages);
		}else{
			//Looks like an older db s, generate the entry
			BasicDBObject dashboardPaths = new BasicDBObject(ID, DASH_PATHS);
			List<String> dashboards = getAllDashboardDirectories(ub);
			if(dashboards.size() > 0){
				DashboardWebServiceHelper.getDashboardNameToDirectories(dashboards, ub.getCatalogManager(), dashboardPaths);
				pageColl.insert(dashboardPaths);
			}
			viewableDashboards(ub, dashboardPaths, dashboardPages);
		}
		
		db.requestDone();
		logger.debug("DashboardMongoDBCache.getDashboardPaths() time =  " + (System.currentTimeMillis() - start));
		return dashboardPages;
	}
	
	/**
	 * Prune the list of dashboard directory paths retrieved from MongoDB which can be seen by the user.
	 * @param ub
	 * @param dbObj
	 * @param dashboardPages
	 * @throws IOException
	 * @throws SwapSpaceInProgressException 
	 */
	private static void viewableDashboards(SMIWebUserBean ub, BasicDBObject dbObj, Map<String, List<File>> dashboardPages) throws IOException, SwapSpaceInProgressException{
		CatalogManager cm = ub.getCatalogManager();
		
		String catalogRootPath = ub.getCatalogPath();
		Set<String> keys = dbObj.keySet();
		
		//Root nodes for which the user can see
		//We unset ub's superuser to get the correct list
		//Like /share and /private/user@birst.com
		// bug 6851
		boolean su = ub.isSuperUser();
		ub.setSuperUser(false);
		ub.setRootDirectories(null);
		
		List<File> rootDirs = cm.getRootDirectories(ub);
		List<Group> groups = ub.getUserGroups();
		
		if (su) {
			ub.setSuperUser(su);
			ub.setRootDirectories(null);
		}
		
		boolean isOwnerOrAdmin = ub.isAdministrator() || ub.isOwner();
		
		for (String key : keys){
			if(key.equals(ID)){
				continue;
			}
			
			BasicDBList list = (BasicDBList) dbObj.get(key);
			for(int i = 0; i < list.size(); i++) {
				String path = (String)list.get(i);
		
				File folder = new File(catalogRootPath + File.separator + path);
				File privateFolder = null;
				if(folder.exists()) {
					boolean canView = true;
		
					//Check permissions only if the user is not an administrator or owner
					CatalogPermission perm = cm.getPermission(folder.getAbsolutePath());

					// removed the if (!isOwnerOrAdmin) check since we need to execute this code for the 'private' check to work
					// all of the routines below properly deal with isOwnerOrAdmin

					if (!perm.canView(groups, isOwnerOrAdmin)){
						continue;
					}

					//Check that we can view the folder based on its parent folders
					//If any of them prohibits us from viewing then we omit the dashboard folder
					File parentFolder = folder.getParentFile();
					while(!parentFolder.getCanonicalPath().equals(catalogRootPath)){
						perm = cm.getPermission(parentFolder.getAbsolutePath());
						if(!perm.canView(groups, isOwnerOrAdmin)){
							canView = false;
							break;
						}

						File parentChild = parentFolder;
						parentFolder = parentFolder.getParentFile();

						//This file was under a private/username folder. Check if it is ours later
						if(parentFolder.getCanonicalPath().equals(catalogRootPath + File.separator + "private")){
							privateFolder = parentChild;
						}
					}

					if(canView){
						//Skip if the private folder is not ours
						if(privateFolder != null && !isInRootPath(rootDirs, privateFolder)){
							continue;
						}
						
						//Add to our viewable dashboards list
						List<File> validPaths = dashboardPages.get(key);
						if(validPaths == null){
							validPaths = new ArrayList<File>();
							dashboardPages.put(key, validPaths);
						}
						validPaths.add(folder);
					}
				}//if(folderExists
			}
		}
	}
	
	private static boolean isInRootPath(List<File> rootDirs, File parentChild) throws IOException{
		boolean matchesRoot = false;
		for(int y = 0; y < rootDirs.size(); y++){
			File root = rootDirs.get(y);
			if(root.getCanonicalPath().equals(parentChild.getCanonicalPath())){
				matchesRoot = true;
				break;
			}
		}
		return matchesRoot;
	}
	
	private static boolean addPageToDashboardDBObject(boolean readPages, BasicDBObject dashboardEl, DB db, DBCollection pageColl, BasicDBObject query, int limit, String discardUuid) throws JSONException{
		boolean allPagesInvisible = true;
	
		//Retrieve from mongo the list of pages
		db.requestStart();

		BasicDBObject fields = new BasicDBObject();
		if(!readPages){
			fields.put(ATTR_NAME, 1);
			fields.put(ATTR_UUID, 1);
			fields.put(ATTR_DIR, 1);
			fields.put(DashboardWebService.INVISIBLE, 1);
		}

		DBCursor cursor = pageColl.find(query, fields);
		cursor.sort(new BasicDBObject(DashboardWebService.ORDER, 1));
		if(limit > 0){
			cursor.limit(limit);
		}
		
		BasicDBList pageDBList = new BasicDBList();
		
		boolean found = false;
		while(cursor.hasNext()){
			BasicDBObject row = (BasicDBObject)cursor.next();

			boolean isInvisible = row.getBoolean(DashboardWebService.INVISIBLE, false);
			if(!isInvisible){
				allPagesInvisible = false;
			}

			if(discardUuid != null && !found){
				if(row.get(ID).equals(discardUuid)){
					found = true;
					continue;
				}
			}
			
			if(readPages){
				String uuid = row.getString(ID);
				row.removeField(ID);
				BasicDBList layoutV2 = null;
				try {
					layoutV2 = getLayoutMetadata(uuid);
				} catch (IOException e) {
					logger.error("Failed to read layout v2 for page " + uuid, e);
				}

				if(layoutV2 != null){
					row.append("LayoutV2", layoutV2);
				}
			}else{
				row = getMinimalPageInfo(row);
			}
			
			pageDBList.add(row);
		}
		db.requestDone();
		
		dashboardEl.append("pages", pageDBList);
		
		return allPagesInvisible;
	}
	
	private static boolean addPageToDashboard(boolean readPages, OMElement dashboardEl, DB db, DBCollection pageColl, BasicDBObject query, int limit, String discardUuid) throws JSONException{
		boolean allPagesInvisible = true;
	
		//Retrieve from mongo the list of pages
		db.requestStart();

		BasicDBObject fields = new BasicDBObject();
		if(!readPages){
			fields.put(ATTR_NAME, 1);
			fields.put(ATTR_UUID, 1);
			fields.put(ATTR_DIR, 1);
			fields.put(DashboardWebService.INVISIBLE, 1);
		}

		DBCursor cursor = pageColl.find(query, fields);
		cursor.sort(new BasicDBObject(DashboardWebService.ORDER, 1));
		if(limit > 0){
			cursor.limit(limit);
		}
		
		boolean found = false;
		while(cursor.hasNext()){
			BasicDBObject row = (BasicDBObject)cursor.next();

			boolean isInvisible = row.getBoolean(DashboardWebService.INVISIBLE, false);
			if(!isInvisible){
				allPagesInvisible = false;
			}

			if(discardUuid != null && !found){
				if(row.get(ID).equals(discardUuid)){
					found = true;
					continue;
				}
			}
			
			OMElement page = null;
			if(readPages){
				try{
					page = pageElementFromDBObject(row);
				}catch(Exception ex){
					logger.error("Cannot convert to xml", ex);
					continue;
				}
			}else{
				page = appendMinimalPageInfo(row);
			}
			dashboardEl.addChild(page);
		}
		db.requestDone();
		
		return allPagesInvisible;
	}
	
	public static OMElement appendMinimalPageInfo(BasicDBObject row){
		return DashboardWebServiceHelper.createMinimalPageInfo(
				row.getString(ATTR_UUID), 
				row.getString(ATTR_NAME),
				row.getString(ATTR_DIR),
				row.getBoolean(DashboardWebService.INVISIBLE, false)
				);
	}
	
	public static BasicDBObject getMinimalPageInfo(BasicDBObject row){
		return DashboardWebServiceHelper.createMinimalPageInfoDBObject(row.getString(ATTR_UUID), 
				row.getString(ATTR_NAME),
				row.getString(ATTR_DIR),
				row.getBoolean(DashboardWebService.INVISIBLE, false)
				);
	}

	private static DB getDB() throws UnknownHostException{
		if(mongo == null){
			String hostname = System.getProperty(DASH_PR_HOSTNAME);
			mongo = (hostname != null && !hostname.isEmpty()) ? MongoMgr.createMongoByHostname(hostname) : MongoMgr.getMongo();
		}
		return mongo.getDB(DBNAME);
	}
	
	private static DBCollection getPageCollection(DB db, com.successmetricsinc.UserBean ub){
		return db.getCollection(pageCollectionName(ub));
	}
	
	private static String pageCollectionName(com.successmetricsinc.UserBean ub){
		return ub.getRepository().getApplicationIdentifier() + PAGE_SUFFIX;
	}
	
	
	public static class DBObjectOrderComparator implements java.util.Comparator<BasicDBObject>{
		private static DBObjectOrderComparator instance = new DBObjectOrderComparator();
		
		public static DBObjectOrderComparator getInstance(){ 
			return instance;
		}
		
		@Override
		public int compare(BasicDBObject one, BasicDBObject two){
			return one.getInt(ORDER) - two.getInt(ORDER);
		}
	}
	
	public static class DashboardOrderComparator implements Comparator<String>{
		BasicDBObject order;
		
		public DashboardOrderComparator(com.successmetricsinc.UserBean ub){
			try{
				//Retrieve the Dashboard's order list
				DB db = getDB();
				DBCollection coll = getPageCollection(db, ub);
				db.requestStart();
				BasicDBObject criteria = new BasicDBObject(ID, DASH_ORDER);
				order = (BasicDBObject)coll.findOne(criteria);
				db.requestDone();

				if(order == null){
					order = new BasicDBObject();
				}
			}catch(Exception ex){
				logger.error(ex);
				order = new BasicDBObject();
			}
		}
		
		@Override
		public int compare(String dashboardOne, String dashboardTwo){
			Object o1 = order.get(dashboardOne);
			Object o2 = order.get(dashboardTwo);
			
			if(o1 == null){
				logger.warn("No order value for dashboard: " + dashboardOne);
				return -1;
			}
			
			if(o2 == null){
				logger.warn("No order value for dashboard: " + dashboardTwo);
				return -1;
			}
			
			int one = (Integer)o1; 
			int two = (Integer)o2;
	
			int compare = one - two;
			if(compare == 0){
				return -1; //one before two
			}
			
			return compare;
		}
	}
}
