/**
 * Copyright (C) 2009-2013 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.WebServices;

import javax.servlet.http.HttpServletRequest;

import org.apache.axis2.AxisFault;
import org.apache.axis2.context.MessageContext;
import org.apache.axis2.handlers.AbstractHandler;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.log4j.Logger;

import com.successmetricsinc.security.EncryptionService;
import com.successmetricsinc.security.SecurityUtil;

public class CSRFValidation extends AbstractHandler
{
	private static Logger logger = Logger.getLogger(CSRFValidation.class);

	public CSRFValidation()
	{
	}

	public InvocationResponse invoke(MessageContext msgContext) throws AxisFault
	{
		if (msgContext == null)
			return InvocationResponse.CONTINUE;

		HttpServletRequest request = (HttpServletRequest) msgContext.getProperty(HTTPConstants.MC_HTTP_SERVLETREQUEST);
		if (request == null)
			return InvocationResponse.CONTINUE;

		String remoteHost = request.getRemoteHost();
		String contentType = request.getContentType();
		if (contentType == null || !contentType.contains("text/xml"))
		{
			logger.warn("CSRF attempt: web service request ContentType header is not text/xml: " + remoteHost + ", " + contentType);
			return InvocationResponse.ABORT;
		}
		
		String method = request.getMethod();
		if (method == null || !method.equalsIgnoreCase("POST"))
		{
			logger.warn("CSRF attempt: web wervice request not via POST: " + remoteHost + ", " + method);
			return InvocationResponse.ABORT;
		}
		
		if (!SecurityUtil.validateURLs(request))
			return InvocationResponse.ABORT;

		String sessionCookie = SecurityUtil.getSessionCookie(request, "ASP.NET_SessionId");
		
		if (sessionCookie != null)
		{
			String encrypted = EncryptionService.getInstance().encryptXSRFToken(sessionCookie);
			String token = request.getHeader("X-XSRF-TOKEN"); // used by AngularJS
			if (token == null)
			{
				logger.warn("CSRF attempt: web service Anti-CSRF header is missing (" + remoteHost + ")");
				return InvocationResponse.ABORT;
			}
			else if (!encrypted.equals(token))
			{
				logger.warn("CSRF attempt: web service Anti-CSRF header is not correct - " + remoteHost + ", " + sessionCookie + " / " + token);
				return InvocationResponse.ABORT;
			}
		}
		return InvocationResponse.CONTINUE;
	}
}