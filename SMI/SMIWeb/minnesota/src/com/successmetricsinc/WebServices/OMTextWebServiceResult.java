/**
 * 
 */
package com.successmetricsinc.WebServices;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.OMText;
import org.apache.log4j.Logger;

/**
 * @author agarrison
 *
 */
public class OMTextWebServiceResult implements IWebServiceResult {

	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(OMTextWebServiceResult.class);

	private String str;
	
	public OMTextWebServiceResult(String string) {
		str = string;
	}
	
	/* (non-Javadoc)
	 * @see com.successmetricsinc.WebServices.IWebServiceResult#addContent(org.apache.axiom.om.OMElement, org.apache.axiom.om.OMFactory, org.apache.axiom.om.OMNamespace)
	 */
	@Override
	public void addContent(OMElement parent, OMFactory factory, OMNamespace ns) {
		OMText text = factory.createOMText(str);
		parent.addChild(text);
	}

}
