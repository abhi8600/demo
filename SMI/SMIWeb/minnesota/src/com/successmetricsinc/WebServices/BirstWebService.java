/**
 * $Id: BirstWebService.java,v 1.16 2011-08-17 21:35:13 agarrison Exp $
 *
 * Copyright (C) 2007 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */


package com.successmetricsinc.WebServices;

import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.axis2.context.MessageContext;
import org.apache.axis2.transport.http.HTTPConstants;

import com.successmetricsinc.SessionHelper;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.adhoc.AdhocReport;
import com.successmetricsinc.util.SessionUtils;

/**
 * The class that all SMI Web Services should extend.  It contains helper methods for using web services.
 * @author agarrison
 *
 */
@WebService
public abstract class BirstWebService {

	private static HttpSession testHttpSession;

	/**
	 * Is this user logged into the SMI system.
	 * @return	true if the user is logged in, false otherwise.
	 */
	public boolean isLoggedIn() {
        return testHttpSession != null || (SessionHelper.getSession() != null);
    }
	
	protected static MessageContext getMessageContext() {
		return SessionHelper.getMessageContext();
	}

	public static HttpServletRequest getHttpServletRequest() {
		return SessionHelper.getHttpServletRequest();
	}
	
	/**
	 * Gets the user bean object for this logged in user
	 * @return	the user bean; null if there is no user bean because there is no session.
	 */
	public static SMIWebUserBean getUserBean() {
		return SessionUtils.getUserBean();
	}
	
	public static AdhocReport getReport() {
		return SessionUtils.getReport();
	}
	
	public static void setReport(AdhocReport r) {
		SessionUtils.setReport(r);
	}
	
	public static Object getSessionObject(String name) {
        if (testHttpSession != null) {
            return testHttpSession.getAttribute(name);
        }
        return SessionUtils.getSessionObject(name);
	}
	
	public static void setSessionObject(String name, Object o) {
		SessionUtils.setSessionObject(name, o);
	}

    /**
     * Unit testing only!  Used to mock up the HTTP session object.
     * @param testHttpSession is the mocked-up HTTP session object.
     */
    public void setTestHttpSession(HttpSession testHttpSession) {
        BirstWebService.testHttpSession = testHttpSession;
    }
}
