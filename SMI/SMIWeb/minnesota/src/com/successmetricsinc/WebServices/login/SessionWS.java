package com.successmetricsinc.WebServices.login;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.axis2.context.MessageContext;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.log4j.Logger;

import com.successmetricsinc.UserBean;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.WebServices.BirstWebService;

public class SessionWS {

	private static final Logger logger = Logger.getLogger(SessionWS.class);
	
	/*
	 * Logout server - invalidate the session, triggering the release of resources
	 */
	public String logout() {
		boolean success = false;
		try {
			MessageContext context = MessageContext.getCurrentMessageContext();
			HttpServletRequest request = (HttpServletRequest) context.getProperty(HTTPConstants.MC_HTTP_SERVLETREQUEST);
			HttpSession session = request.getSession(false);
			SMIWebUserBean ub = BirstWebService.getUserBean();
			if (ub != null) {
				ub.cleanup();
				ub.unregisterSubscribers();
			}
			if (session != null) {
				logger.info("invalidating the session");
				session.invalidate();
				success = true;
			} else {
				logger.info("No Session found in SessionWS.logout. Nothing to invalidate");
				// already got logged out before
				success = true;
			}
		} catch (Exception e) {
			logger.error("Exception in logout operation : " + e.getMessage(), e);
			success = false;
		}	
		String result = Boolean.toString(success);
		return result;
	}	
	
	/*
	 * Polling service - does nothing, used to stop the session from timing out
	 */
	public void poll(){
		MessageContext context = MessageContext.getCurrentMessageContext();
		HttpServletRequest request = (HttpServletRequest) context.getProperty(HTTPConstants.MC_HTTP_SERVLETREQUEST);
		HttpSession session = request.getSession(false);		
		if(session != null){
			if (logger.isTraceEnabled())
				logger.trace("Polling trace: Session " + session.getId());
		}
		
	}
}
