/**
 * $Id: TrustedLoginWS.java,v 1.40 2012-10-03 01:15:13 BIRST\ricks Exp $
 *
 * Copyright (C) 2009-2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.WebServices.login;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.axis2.context.MessageContext;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.log4j.Logger;

import com.successmetricsinc.PackageSpaceProperties;
import com.successmetricsinc.ServerContext;
import com.successmetricsinc.Session;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.security.Net;
import com.successmetricsinc.util.SwapSpaceInProgressException;
import com.successmetricsinc.util.TimeZoneUtil;
import com.successmetricsinc.util.Util;

public class TrustedLoginWS {

private static final String ASPXFORMSAUTH = "aspxformsauth";

private static final String GA_TRACKING_ACCOUNT = "GaTrackingAccount";

private static final String BING_MAPS = "BingMaps";

private static final String SCHEDULER_ROOT = "SchedulerRoot";

private static final String ACORN_ROOT = "AcornRoot";

private static final String ASP_NET_SESSION_ID = "ASP.NET_SessionId";

private static Logger logger = Logger.getLogger(TrustedLoginWS.class);

	public static final String LOGIN_PARAM_SUPERUSER = "superUser";
	public static final String LOGIN_PARAM_SPACE_ADMIN = "spaceAdmin";
	public static final String LOGIN_PARAM_FILL_SESSION = "fillSession";
	public static final String LOGIN_PARAM_SESSION_VARS = "sessionVars";
	public static final String LOGIN_PARAM_LOCALE = "Locale";
	public static final String LOGIN_PARAM_TIMEZONE = "TimeZone";
	public static final String LOGIN_PARAM_PAGETITLE = "PageTitle";
	public static final String LOGIN_PARAM_USER_ID = "UserID";
	public static final String LOGIN_PARAM_USER_NAME = "UserName";
	public static final String LOGIN_PARAM_SPACE_ID = "SpaceID";
	public static final String LOGIN_PARAM_SPACE_DISCOVERY = "DiscoverySpace";
	public static final String LOGIN_PARAM_FREE_TRIAL = "FreeTrial";
	public static final String LOGIN_PARAM_SPACES_INFO = "PackageSpacesInfo";
	public static final String LOGIN_PARAM_SFDC_SESSIONID = "SFDC_sessionId";
	public static final String LOGIN_PARAM_SFDC_SERVERURL = "SFDC_serverURL";
	public static final String LOGIN_PARAM_DISCOVERY_FREE_TRIAL = "DiscoveryFreeTrial";
	public static final String LOGIN_PARAM_VISUALIZER = "VisualizerEnabled";
	public static final String LOGIN_PARAM_ASYNC_REPORT_GENERATION = "EnableAsyncReportGeneration";

	public String login(String[] loginCredentials) {		

		boolean success = false;
		String result = null;
		StringBuilder sb = new StringBuilder();		
		try {
			MessageContext context = MessageContext.getCurrentMessageContext();
			HttpServletRequest request = (HttpServletRequest) context.getProperty(HTTPConstants.MC_HTTP_SERVLETREQUEST);

			// check for trusted sender
			List<String> loginAllowedIPs = ServerContext.getLoginServiceAllowedIPs();
			String requestIP = request.getRemoteAddr();
			if (loginAllowedIPs != null) {
				if (!Net.containIp(loginAllowedIPs, requestIP)) {					
					logger.error("TrustedLoginWS : Request IP not allowed: bad request IP: " + requestIP);
					logger.error("TrustedLogin WS: Please check the SMIWebLogin.AllowsIPs in web.xml or customer.properties");
					sb.append("Request IP not allowed: bad request IP");
					return sb.toString();				
				} 
				if (logger.isTraceEnabled())
					logger.trace("TrustedLoginWS: Valid request from host " + requestIP);
			} else {				
				logger.error("No valid IPs list for SMIWebLogin Service.");
				logger.error("Please check the SMIWebLogin.AllowsIPs in web.xml or customer.properties");
				sb.append("No valid IPs list for SMIWebLogin Service.");
				return sb.toString();
			}

			// session should have been created by the
			// Security filter SSOAuthenticator

			HttpSession session = request.getSession(false);
			if (session != null) {
				
				if (session.getAttribute("userBean") == null) {
					if (logger.isTraceEnabled())
						logger.trace("TrustedLoginWS: Start : Creating session attributes ");
					// initialize all the beans, userbean, adhoc, dashboards
					SMIWebUserBean userBean = new SMIWebUserBean();
					SMIWebUserBean.setHttpServletRequest(request);
					userBean.setSpacePropertiesList(getPackagesSpacesProperties(loginCredentials, request));
					setPageTitle(loginCredentials, userBean);
					setLocaleTimezonePreferences(loginCredentials, userBean);
					setUserAndSpaceInfo(loginCredentials,userBean);
					session.setAttribute("userBean", userBean);
					
					String asp_net_sessionId = getParamValue(loginCredentials, ASP_NET_SESSION_ID);
					String acornRoot = getParamValue(loginCredentials, ACORN_ROOT);
					String schedulerRoot = getParamValue(loginCredentials, SCHEDULER_ROOT);
					boolean hasBingMaps = Boolean.parseBoolean(getParamValue(loginCredentials, BING_MAPS));
					String gaTrackingAccount = getParamValue(loginCredentials, GA_TRACKING_ACCOUNT);
					String aspxFormsAuth = getParamValue(loginCredentials, ASPXFORMSAUTH);
					userBean.setFlashVars(asp_net_sessionId, session.getId(), acornRoot, schedulerRoot, hasBingMaps, gaTrackingAccount, aspxFormsAuth);
					
					// get superuser flag from request header
					String isSuperUser = getParamValue(loginCredentials, TrustedLoginWS.LOGIN_PARAM_SUPERUSER);// request.getHeader("superUser");					
					if(isSuperUser != null && isSuperUser.equalsIgnoreCase("true"))
					{
						userBean.setSuperUser(true);
					}
					String enableAsynReportGeneration = getParamValue(loginCredentials, TrustedLoginWS.LOGIN_PARAM_ASYNC_REPORT_GENERATION);
					userBean.setAsyncReportGenerationEnabled(enableAsynReportGeneration != null && enableAsynReportGeneration.equalsIgnoreCase("true"));
					
					String isSpaceAdmin = getParamValue(loginCredentials, TrustedLoginWS.LOGIN_PARAM_SPACE_ADMIN); //request.getHeader("spaceAdmin");					
					if(isSpaceAdmin != null && isSpaceAdmin.equalsIgnoreCase("true"))
					{
						userBean.setIsAdministrator(true);
					}
					
					String isDiscoverySpace = getParamValue(loginCredentials, TrustedLoginWS.LOGIN_PARAM_SPACE_DISCOVERY); //request.getHeader("DiscoverySpace");					
					if(isDiscoverySpace != null && isDiscoverySpace.equalsIgnoreCase("true"))
					{
						userBean.setDiscoverySpace(true);
					}
					
					String isFreeTrial = getParamValue(loginCredentials, TrustedLoginWS.LOGIN_PARAM_FREE_TRIAL); //request.getHeader("FreeTrial");					
					if(isFreeTrial != null && isFreeTrial.equalsIgnoreCase("true"))
					{
						userBean.setFreeTrial(true);
					}
					
					String isFreeDiscoveryTrial = getParamValue(loginCredentials, LOGIN_PARAM_DISCOVERY_FREE_TRIAL);
					if (isFreeDiscoveryTrial != null && isFreeDiscoveryTrial.equalsIgnoreCase("true")) {
						userBean.setDiscoveryFreeTrial(true);
					}
					
					String SFDC_sessionId = getParamValue(loginCredentials, TrustedLoginWS.LOGIN_PARAM_SFDC_SESSIONID); //request.getHeader("SFDC_sessionId");					
					if(SFDC_sessionId != null)
					{
						userBean.setSFDC_sessionId(SFDC_sessionId);
					}
					
					String SFDC_serverURL = getParamValue(loginCredentials, TrustedLoginWS.LOGIN_PARAM_SFDC_SERVERURL); //request.getHeader("SFDC_serverURL");					
					if(SFDC_serverURL != null)
					{
						SFDC_serverURL = SFDC_serverURL.replace("/c/", "/u/"); // change from enterprise.wsdl endpoint to partner.wsdl endpoint
						// set to 25.0 WSDL because that's what we are using for the API (need to make this based upon the version info in the jar)
						int versionIndex = SFDC_serverURL.indexOf("/Soap/u/") + "/Soap/u/".length();
						int versionEnd = SFDC_serverURL.indexOf("/", versionIndex);
						SFDC_serverURL = SFDC_serverURL.substring(0, versionIndex) + "25.0" + SFDC_serverURL.substring(versionEnd);
						userBean.setSFDC_serverURL(SFDC_serverURL);
					}					
					
					String isVisualizerEnabled = getParamValue(loginCredentials, TrustedLoginWS.LOGIN_PARAM_VISUALIZER);					
					if(isVisualizerEnabled != null && isVisualizerEnabled.equalsIgnoreCase("true"))
					{
						userBean.setVisualizerFlag(true);
					}
					
					// flag used for faster session creation without filling session variables
					// required for CatMan and Custom SA on flex Admin
					String fillSession = getParamValue(loginCredentials, TrustedLoginWS.LOGIN_PARAM_FILL_SESSION); //request.getHeader("fillSession");
					boolean fillCurrentSession = true;
					if(fillSession != null && fillSession.equalsIgnoreCase("false"))
					{
						fillCurrentSession = false;
					}
					
					try
					{
					if(fillCurrentSession)
					{
						userBean.getCatalogManager().getRootDirectories(userBean);
					
						// overwrite the session variables if the session Variables are passed in headers -- passed by the customer on SSO request to Birst
						populateSessionVariables(loginCredentials, request, userBean);
					}
					else
					{
						userBean.getCatalogManager().getRootDirectories(userBean, userBean.isOwner());
						// session time out listener is set during call to userBean.getRepsession. But in this case
						// we are not filling session variables. Thus an explicit call to set sessionTimeOutListener
						userBean.setSessionTimeOutListener(null);
						if (logger.isTraceEnabled())
							logger.trace("TrustedLoginWS: Only Created UserBean. Not filling session variables");
					}
					}catch(SwapSpaceInProgressException spEx)
					{
						// catch the exception, so that user can log into and then see the error. Otherwise 
						// user gets redirecting to the screen without any knowledge.
						// TO DO
						// Have a better way to display any login exception to the user on UI main page when it gets re directed.
						logger.error("TrustedLoginWS - swap space going on", spEx);
					}
					
					userBean.initializeDrillMapsList();
					userBean.syncUpUserBeanAppContextTime();
				}
				else
				{
					SMIWebUserBean existingUserBean = (SMIWebUserBean) session.getAttribute("userBean");
					String asp_net_sessionId = getParamValue(loginCredentials, ASP_NET_SESSION_ID);
					String acornRoot = getParamValue(loginCredentials, ACORN_ROOT);
					String schedulerRoot = getParamValue(loginCredentials, SCHEDULER_ROOT);
					boolean hasBingMaps = Boolean.parseBoolean(getParamValue(loginCredentials, BING_MAPS));
					String gaTrackingAccount = getParamValue(loginCredentials, GA_TRACKING_ACCOUNT);
					String aspxFormsAuth = getParamValue(loginCredentials, ASPXFORMSAUTH);
					existingUserBean.setFlashVars(asp_net_sessionId, session.getId(), acornRoot, schedulerRoot, hasBingMaps, gaTrackingAccount, aspxFormsAuth);
					List<PackageSpaceProperties> updatedPackagesSpacesProperties = getPackagesSpacesProperties(loginCredentials, request);
					existingUserBean.getApplicationContext().updatePackageSpacesProperties(updatedPackagesSpacesProperties);
				}
				success = true;
				
			} else {
				// Session should have been created.
				success = false;				
				logger.error("No Session found in TrustedLoginWS.login.");
				sb.append("No Session found in TrustedLoginWS.login. Session :");
			}
		} catch (Exception e) {
			success = false;			
			logger.error("Exception in TrustedLoginWS. Not able to login : "
					+ e.getMessage(), e);
			sb.append("Exception during login. Please check SMIWebLog: " + e.getMessage());
		}
		if (success){
			if (logger.isTraceEnabled())
				logger.trace("TrustedLoginWS: Successful Login");
			 result = Boolean.toString(success);
		} else {
			result = sb.toString();
		}
		return result;
	}
	
	private List<PackageSpaceProperties> getPackagesSpacesProperties(String[] loginCredentials, HttpServletRequest request)
	{	
		String packagesSpacePropertiesInfo = getParamValue(loginCredentials, TrustedLoginWS.LOGIN_PARAM_SPACES_INFO); //request.getHeader("SpacesInfo");
		if(packagesSpacePropertiesInfo != null && packagesSpacePropertiesInfo.trim().length() > 0)
		{	
			String[] packageSpacesProperties = packagesSpacePropertiesInfo.split(";");
			return Util.getPackageSpacePropertiesList(packageSpacesProperties);
		}
		return null;
	}
	
	private void setUserAndSpaceInfo(String[] loginCredentials, SMIWebUserBean userBean) {
		String userID = getParamValue(loginCredentials, LOGIN_PARAM_USER_ID);
		String spaceID = getParamValue(loginCredentials, LOGIN_PARAM_SPACE_ID);
		userBean.setSpaceID(spaceID);
		userBean.setUserID(userID);
	}

	private static String getParamValue(String[] keyValuePairArray, String param)
	{
		String value = null;
		for(String str : keyValuePairArray)
		{
			if(str.startsWith(param))
			{
				value = str.substring(param.length() + 1);
				break;
			}
		}		
		return value;
	}
	
	public static void populateSessionVariables(String[] loginCredentials, HttpServletRequest request, SMIWebUserBean userBean)
	{
		try
		{
			List<String> modifiedSessionVars = new ArrayList<String>();
			Session repSession = userBean.getRepSession();
			if(repSession == null)
			{
				logger.error("Unable to get repository session object for the current user");
				return;
			}
			String requestSessionVars = getParamValue(loginCredentials, TrustedLoginWS.LOGIN_PARAM_SESSION_VARS) ; //request.getHeader("sessionVars");
			if(requestSessionVars != null && requestSessionVars.length() > 0)
			{
				// iterate and get the session variables separated by semi-colon
				String[] sessionVars = requestSessionVars.split(";");
				if(sessionVars == null || sessionVars.length == 0)
				{
					// nothing to do
					return;
				}
				for(String sessionVar : sessionVars)
				{
					
					String[] str = sessionVar.split("=");
					// making sure we are talking about key value pairs
					if(str.length != 2)
					{
						logger.warn("Skipping: doesn't look like key=value pair " + sessionVar);
						continue;
					}
					String variableName = str[0];
					String variableValue = str[1];
					// ignore any session variable named USER, since V{USER} is used internally by the system
					// to store usernames
					if(variableName.equalsIgnoreCase("user"))
					{
						continue;
					}
					if(str != null && str.length == 2)
					{
						// see if variable exists, overwrite else create a new one
						if(repSession.getVariable(variableName) != null)
						{
							if (logger.isTraceEnabled())
								logger.trace("Overwriting existing session variable " + variableName);
							if(repSession.updateVariable(userBean.getRepository(), variableName, variableValue, true))
							{
								modifiedSessionVars.add(variableName);
							}
						}
						else
						{
							if (logger.isTraceEnabled())
								logger.trace("Creating new session variable " + variableName);
							/*
						boolean alwaysMultiValue = false;
						// special treatment for Birst$Groups. To always create multi value so
						// the underlying session object is list.
						if(variableName.equals("Birst$Groups"))
						{
							alwaysMultiValue = true;
						}
						*/
							if(repSession.createNewVariable(true, variableName, variableValue))
							{
								modifiedSessionVars.add(variableName);
							}
						}
					}
				}
				
				repSession.instantiateAllSessionVarsNotInList(userBean.getRepository(), modifiedSessionVars);
				// modified session variables are stored in user bean
				// these are used during self scheduling report generation and are passed to the scheduler
				userBean.setOverridenSessionVariables(modifiedSessionVars);
				logger.info("Updated session variables: " + repSession.toString());
			}	
		}catch(Exception ex)
		{
			logger.error("Exception while populating session variables : "+ ex.getMessage(), ex);
		}
	}
	
	private void setPageTitle(String[] loginCredentials, SMIWebUserBean userBean)
	{
		String pageTitle = getParamValue(loginCredentials, LOGIN_PARAM_PAGETITLE);//UserBean.getHttpServletRequest().getHeader("PageTitle");
		if (pageTitle == null)
			userBean.setPageTitle("");
		else
			userBean.setPageTitle(pageTitle);
	}

	private void setLocaleTimezonePreferences(String[] loginCredentials, com.successmetricsinc.UserBean userBean) {
		// Get the locale & timezone values from the HTTP request headers
		final String locale = getParamValue(loginCredentials, LOGIN_PARAM_LOCALE);//request.getHeader("Locale");
		String languageCode = Locale.ENGLISH.getLanguage();
		String countryCode = Locale.US.getCountry();
		if (locale != null) {
			final String[] parts = locale.split("-");
			languageCode = parts[0];
			countryCode = parts.length == 2 ? parts[1] : null;
		}
		
		// Set the locale in UserBean, defaulting to en_US
		if (countryCode != null) {
			userBean.setLocale(new Locale(languageCode, countryCode));
		} else {
			userBean.setLocale(new Locale(languageCode));
		}
		if (logger.isTraceEnabled())
			logger.trace("Set the locale on UserBean to value from user preferences: " + userBean.getLocale());
		
		// Set the timezone in UserBean, defaulting to Repository display time zone
		final String timeZoneID = getParamValue(loginCredentials, TrustedLoginWS.LOGIN_PARAM_TIMEZONE);//request.getHeader("TimeZone");
		TimeZone userTZ = null;
		if (timeZoneID != null && !timeZoneID.isEmpty()) 
		{
			userTZ = TimeZoneUtil.getTimeZoneForWindowsTZID(timeZoneID);						
		}
		if (userTZ == null)
		{
			userTZ = userBean.getRepository().getServerParameters().getDisplayTimeZone();
		}
		userBean.setTimeZone(userTZ);
		if (logger.isTraceEnabled())
			logger.trace("Set the time zone on UserBean to value from user preferences: " + userBean.getTimeZone().getDisplayName());
	}
}
