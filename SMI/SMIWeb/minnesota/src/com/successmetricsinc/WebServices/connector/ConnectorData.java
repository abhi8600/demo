package com.successmetricsinc.WebServices.connector;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.TreeMap;
import java.util.UUID;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.successmetricsinc.Repository;
import com.successmetricsinc.UI.ApplicationContext;
import com.successmetricsinc.WebServices.connector.ConnectorServices.ConnectorInfo;
import com.successmetricsinc.connectors.BaseConnector;
import com.successmetricsinc.connectors.CloudConnection;
import com.successmetricsinc.connectors.CloudConnectorConfig;
import com.successmetricsinc.connectors.ConnectionProperty;
import com.successmetricsinc.connectors.ConnectorConfig;
import com.successmetricsinc.connectors.ConnectorManager;
import com.successmetricsinc.connectors.ConnectorMetaData;
import com.successmetricsinc.connectors.ConnectorOperations;
import com.successmetricsinc.connectors.ExtractionGroup;
import com.successmetricsinc.connectors.ExtractionObject;
import com.successmetricsinc.connectors.ExtractionObjectWrapper;
import com.successmetricsinc.connectors.IConnector;
import com.successmetricsinc.connectors.RecoverableException;
import com.successmetricsinc.connectors.UnrecoverableException;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.ConnectorUtils;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XmlUtils;

public class ConnectorData {
	
	public static final String CONNECTOR_NAME_SFDC = "sfdc";
	
	public static final String NODE_CREDENTIALS_ROOT = "Credentials";
	
	public static final String CREDENTIALS_USER_NAME = "Name";
	public static final String CREDENTIALS_PASSWORD = "Password";
	public static final String CREDENTIALS_SAVE_LOGIN = "SaveLogin";
	
	public static final String NODE_OBJECTS_ALL = "AllObjects";
	public static final String NODE_OBJECTS_SELECTED_ROOT = "SelectedObjects";
	public static final String NODE_OBJECT_ROOT = "Object";
	public static final String NODE_OBJECT_ID = "ObjectId";
	public static final String NODE_OBJECT_URL = "URL";
	public static final String NODE_CONNECTION_ID = "ConnectionID";
	public static final String NODE_OBJECT_NAME = "Name";
	public static final String NODE_OBJECT_TECH_NAME = "ObjectName";
	public static final String NODE_OBJECT_QUERY = "Query";
	public static final String NODE_INCLUDE_ALL_RECORDS="IncludeAllRecords";
	public static final String NODE_OBJECT_TYPE = "Type";
	public static final String NODE_OBJECT_RECORD_TYPE = "RecordType";
	public static final String NODE_OBJECT_MAPPING = "Mapping";
	public static final String NODE_OBJECT_COLUMNS = "Columns";
	public static final String NODE_OBJECT_PROFILE_ID = "ProfileID";
	public static final String NODE_OBJECT_DIMENSIONS = "Dimensions";
	public static final String NODE_OBJECT_METRICS = "Metrics";
	public static final String NODE_OBJECT_SEGMENT_ID = "SegmentID";
	public static final String NODE_OBJECT_FILTERS = "Filters";
	public static final String NODE_OBJECT_START_DATE = "StartDate";
	public static final String NODE_OBJECT_END_DATE = "EndDate";
	public static final String NODE_OBJECT_SAMPLING_LEVEL = "SamplingLevel";
	public static final String NODE_OBJECT_REPORT_SUITE_ID = "ReportSuiteID";
	public static final String NODE_OBJECT_ELEMENTS = "Elements";
	public static final String NODE_OBJECT_PAGESIZE = "PageSize";
	public static final String NODE_OBJECT_SELECTION_CRITERIA = "SelectionCriteria";
	public static final String NODE_OBJECT_ISVALIDQUERY = "IsValidQuery";
	public static final String NODE_OBJECT_SELECTED = "Selected";
	public static final String NODE_OBJECT_ENABLED = "Enabled";
	public static final String NODE_EXTRACT_STATUS = "ExtractStatus";
	public static final String NODE_STATUS = "Status";
	public static final String NODE_OBJECT_NUM_RECORDS = "NumRecords";
	public static final String NODE_METADATA_ROOT = "MetaData";
	public static final String NODE_CONN_PROPERTY_ROOT = "ConnectionProperties";
	public static final String NODE_CONN_PROPERTY = "ConnectionProperty";
	public static final String NODE_CONN_PROPERTY_NAME = "Name";
	public static final String NODE_CONN_PROPERTY_VALUE = "Value";
	public static final String NODE_CONN_PROPERTY_REQUIRED = "Required";
	public static final String NODE_CONN_PROPERTY_SECRET = "Secret";
	public static final String NODE_EXTRACT_ROOT = "Extract";
	public static final String NODE_EXTRACT_ID = "ExtractId";
	
	private static final int maxRetries = 5;
	private static final String PROP_NAME_NUM_THREADS  = "NumThreads";
	private static final String COMMA = ",";	 
	private static final Logger logger = Logger.getLogger(ConnectorData.class);
	
	protected String connectorName;
	protected String spaceID;
	protected String spaceDirectory;
	protected String name;
	protected String password;
	
	public ConnectorData(String spaceID, String spaceDirectory, String connectorName){
		this.spaceID = spaceID;
		this.spaceDirectory = spaceDirectory;
		this.connectorName = connectorName;
	}
	
	public OMElement getCredentials() throws Exception{
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;			
		OMElement root = factory.createOMElement(NODE_CREDENTIALS_ROOT, ns);
		Properties properties = getConnectorCredentials();
		if(properties != null && properties.size() > 0){
			for(String key : properties.stringPropertyNames()){
				XmlUtils.addContent(factory, root, key, properties.getProperty(key), ns);
			}
		}
		return root;
	}
	
	public void saveCredentials(String xmlData) throws Exception{
		SAXBuilder builder = new SAXBuilder();
		Document d = builder.build(new StringReader(xmlData));
		Element root = d.getRootElement();
		Properties properties = parseConnectorProperties(root);
		properties.setProperty(BaseConnector.SPACE_ID, spaceID);
		properties.setProperty(BaseConnector.SPACE_DIR, spaceDirectory);
		IConnector connector = ConnectorManager.getConnector(ConnectorUtils.getConnectorUrl(connectorName));
		int count = 0;
		boolean retry = false;
		do{
			try{
				connector.connect(properties);
				retry = false;
			}
			catch(RecoverableException ex){
				count++;
				retry = count < maxRetries;
				if(retry){
					Thread.sleep(500);
					logger.warn("Retrying saveCredentials : " +count + " ", ex);
				}
				else{
					logger.error("Exhausted the retries for saveCredentials : " +count + " ", ex);
				}
			}
		}while(retry);
		
		// No exceptions. Save it
		saveCredentials(root);
	}
	
	
	/**
	 * The underlying implementation parses the dom element to parse all the properties it needs
	 * e.g. username, password, sfdcclient id etc.
	 * @param root
	 * @return
	 * @throws UnrecoverableException 
	 */
	private Properties parseConnectorProperties(Element el) throws UnrecoverableException{
		Properties properties = new Properties();
		IConnector connector = ConnectorManager.getConnector(ConnectorUtils.getConnectorUrl(connectorName));
		ConnectorMetaData connectorMetaData = connector.getMetaData();
		List<ConnectionProperty> connectorPropertiesList = connectorMetaData.getConnectionProperties();
		if(connectorPropertiesList != null)
		{
			for(ConnectionProperty connectionProperty : connectorPropertiesList)
			{
				fillInPropsIfNotNull(el, connectionProperty.name, properties);
			}
		}
		fillInPropsIfNotNull(el, BaseConnector.BIRST_PARAMETERS, properties);
		return properties;
	}
	
	private void fillInPropsIfNotNull(Element el, String propName, Properties properties){
		String propValue = getPropertyValue(el, propName);
		fillInPropsIfNotNull(propName, propValue, properties);
	}
	
	private String getPropertyValue(Element el, String propName){
		if(el != null && propName != null && propName.trim().length() > 0){
			return el.getChildText(propName);
		}
		return null;
	}
	
	private void fillInPropsIfNotNull(String propName, String propValue, Properties properties){
		if(propValue != null && propValue.trim().length() > 0){
			properties.setProperty(propName, propValue);
		}
	}
	
	private void fillInPropsIfNotNull(Element el, ConnectionProperty connProperty){
		if(connProperty != null){
			String propValue = getPropertyValue(el, connProperty.name);
			if(propValue != null && propValue.trim().length() > 0){
				connProperty.value = propValue;
			}
		}
	}
	
	public OMElement getAllObjectsForCloudConnection(String userName, String xmlData, ConnectorInfo connectorInfo, CloudConnection conn, CloudConnectorConfig cldConnectorConfig) throws Exception{
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;
		OMElement allObjectsRoot = factory.createOMElement(NODE_OBJECTS_ALL, ns);
		String[] allObjects = null;
		if (connectorInfo != null)
		{
			allObjects = validateConnectionPropertiesAndGetCatalog(userName, connectorInfo, xmlData);
		}
		else
		{
			throw new Exception ("No suitable connector found for " + connectorName);
		}		
		
		Map<String, String> allObjectsMap = new TreeMap<String,String>();
		if(allObjects != null && allObjects.length > 0){
			for(String object : allObjects){
				if(object != null){
					allObjectsMap.put(object.toLowerCase(), object);
				}
			}
		}
		
		List<ExtractionObjectWrapper> objectsForConnection = cldConnectorConfig.getObjectsForConnection(conn.getName(), conn.getConnectorType());
		List<ExtractionObject> extractionObjects = new ArrayList<>();
		if (objectsForConnection != null)
		{
			for (ExtractionObjectWrapper wrp : objectsForConnection)
			{
				extractionObjects.add(wrp.getExtractionObject());
			}
		}
		List<Properties> selectedOjects = getConnectorSelectedObjects(extractionObjects);
		return  getConnectorSelectedObjectsMap(selectedOjects, allObjectsMap);
	}
	
	private OMElement getConnectorSelectedObjectsMap(List<Properties> selectedOjects, Map<String, String> allObjectsMap) throws Exception
	{
		Map<String, Properties> selectedObjectsMap = new HashMap<String, Properties>();
		if(selectedOjects != null && selectedOjects.size() > 0){
			for(Properties prop : selectedOjects){
				String name = prop.getProperty("Name");
				if(name != null && name.trim().length() > 0){
					// case-insensitive key -- to make sure the standard object and query objects of the same name cannot exist
					selectedObjectsMap.put(name.toLowerCase(), prop); 
				}
			}
		}
		
		// merge selected object names that are different from standard object names -- renaming operation from users
		if(selectedOjects != null && selectedOjects.size() > 0){
			for(Properties selectedObject : selectedOjects){
				String objName = selectedObject.getProperty("Name");
				if(!allObjectsMap.containsKey(objName.toLowerCase())){
					allObjectsMap.put(objName.toLowerCase(), objName);
				}
			}
		}
		
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;
		OMElement allObjectsRoot = factory.createOMElement(NODE_OBJECTS_ALL, ns);
		if(allObjectsMap != null && allObjectsMap.size() > 0){
			for(String key : allObjectsMap.keySet()){
				String object = allObjectsMap.get(key);
				OMElement objectElement = factory.createOMElement(NODE_OBJECT_ROOT, ns);
				XmlUtils.addContent(factory, objectElement, NODE_OBJECT_NAME, object, ns);
				Properties selectedObject = selectedObjectsMap.get(object.toLowerCase());
				boolean selected = false;
				boolean enabled = true;
				String extGroups = null;
				if(selectedObject != null){
					String query = selectedObject.getProperty("Query");
					String type = selectedObject.getProperty("Type");
					XmlUtils.addContent(factory, objectElement, NODE_OBJECT_TYPE, type, ns);
					enabled = ((query == null || query.trim().length() == 0) && !type.equalsIgnoreCase(ExtractionObject.Types.SAVED_OBJECT.toString()) 
							&& !type.equalsIgnoreCase(ExtractionObject.Types.SAVED_SEARCH_OBJECT.toString()) && !type.equalsIgnoreCase(ExtractionObject.Types.SAVED_GOOGLE_ANALYTICS_QUERY.toString()));
					selected = true;
					extGroups = selectedObject.getProperty(ExtractionObject.EXT_GROUP_NAMES);
				}
				XmlUtils.addContent(factory, objectElement, NODE_OBJECT_SELECTED, selected, ns);
				XmlUtils.addContent(factory, objectElement, NODE_OBJECT_ENABLED, enabled, ns);
				if (extGroups != null) {
					XmlUtils.addContent(factory, objectElement, ExtractionObject.EXT_GROUP_NAMES, extGroups, ns);
				}
				allObjectsRoot.addChild(objectElement);
			}
		}
		return allObjectsRoot;
	}
	
	public OMElement getAllObjects(String userName, String xmlData, ConnectorInfo connectorInfo) throws Exception{
		String[] allObjects = null;
		if (!Repository.isUseNewConnectorFramework())
		{
			allObjects = validateConnectionPropertiesAndGetCatalog(xmlData);
		}
		else
		{
			if (connectorInfo != null)
			{
				allObjects = validateConnectionPropertiesAndGetCatalog(userName, connectorInfo, xmlData);
			}
			else
			{
				throw new Exception ("No suitable connector found for " + connectorName);
			}
		}
		
		Map<String, String> allObjectsMap = new TreeMap<String,String>();
		if(allObjects != null && allObjects.length > 0){
			for(String object : allObjects){
				if(object != null){
					allObjectsMap.put(object.toLowerCase(), object);
				}
			}
		}
		
		ConnectorConfig settings = getConnectorSettings();
		List<ExtractionObject> extractionObjects = settings.getExtractionObjects();
		List<Properties> selectedOjects = getConnectorSelectedObjects(extractionObjects);
		return  getConnectorSelectedObjectsMap(selectedOjects, allObjectsMap);
	}
 
	
	private String[] validateConnectionPropertiesAndGetCatalog(String xmlData) throws Exception
	{
		String[] allObjectsList = null;
		Properties allProps = getConnectorCredentials();
		allProps.setProperty(BaseConnector.SPACE_ID, spaceID);
		allProps.setProperty(BaseConnector.SPACE_DIR, spaceDirectory);
		if(xmlData != null && xmlData.trim().length() > 0){
			// get credentials from incoming request
			SAXBuilder builder = new SAXBuilder();
			Document d = builder.build(new StringReader(xmlData));
			Element root = d.getRootElement();
			Properties incomingProps = parseConnectorProperties(root);
			allProps.putAll(incomingProps);
		}
		IConnector connector = ConnectorManager.getConnector(ConnectorUtils.getConnectorUrl(connectorName));
		// are all the properties available
		for (ConnectionProperty property : connector.getMetaData().getConnectionProperties()){
			if (property.isRequired && !allProps.containsKey(property.name)){
				logger.error("Required property not found for connector : " + connectorName + " : " + property.name);
				throw new Exception("Required Property Not Found : " + property.name);
			}
		}
		
		int count = 0;
		boolean retry = false;
		do{
			try{
				connector.connect(allProps);
				allObjectsList = connector.getCatalog();
				retry = false;
			}
			catch(RecoverableException ex){
				count++;
				retry = count < maxRetries;
				if(retry){
					Thread.sleep(500);
					logger.warn("Retrying getCatalog : " +count + " ", ex);
				}
				else{
					logger.warn("Exhausted the retries for getCatalog : " +count + " ", ex);
				}
			}
		}while(retry);
		return allObjectsList;
	}
	
	private String[] validateConnectionPropertiesAndGetCatalog(String userName, ConnectorInfo connectorInfo, String xmlData) throws Exception
	{
		ConnectorHttpClient client = new ConnectorHttpClient();
		Map<String, String> argumentsMap = ConnectorServices.getConnectorDefaultArguments(userName, spaceID, spaceDirectory);
		argumentsMap.put("Method", "validateConnectionPropertiesAndGetCatalog");
		argumentsMap.put("CredentialsXML", xmlData);
		String result = client.callConnectorServlet(connectorInfo.connectorServerURL, connectorInfo.connectorApplication, ConnectorServices.ICONNECTOR_SERVLET, true, argumentsMap);
		OMElement resultEl = XmlUtils.convertToOMElement(result);
		if (resultEl == null)
		{
			UnrecoverableException ure = new UnrecoverableException("Unable to parse result received from Connector", new Exception());
			ure.setErrorCode(BaseException.ERROR_OTHER);
			throw ure;
		}
		boolean isSuccess = false;
		for (Iterator<OMElement> resultIterator = resultEl.getChildElements(); resultIterator.hasNext(); )
		{
			OMElement el = resultIterator.next();
			if ("Success".equals(el.getLocalName()))
			{
				isSuccess = Boolean.valueOf(XmlUtils.getBooleanContent(el));
				break;
			}
		}
		if (isSuccess)
		{
			List<String> catalog = new ArrayList<String>();
			for (Iterator<OMElement> resultIterator = resultEl.getChildElements(); resultIterator.hasNext(); )
			{
				OMElement el = resultIterator.next();
				if ("Result".equals(el.getLocalName()))
				{
					for (Iterator<OMElement> elIterator = el.getChildElements(); elIterator.hasNext(); )
					{
						OMElement catalogRootelement = (OMElement) elIterator.next();
						if ("Catalog".equals(catalogRootelement.getLocalName()))
						{
							for (Iterator<OMElement> catalogRootIterator = catalogRootelement.getChildElements(); catalogRootIterator.hasNext(); )
							{
								OMElement catalogItemElement = (OMElement) catalogRootIterator.next();
								catalog.add(catalogItemElement.getText());
							}
							break;
						}
					}					
				}
			}
			return catalog.toArray(new String[0]);
		}
		else
		{
			for (Iterator<OMElement> resultIterator = resultEl.getChildElements(); resultIterator.hasNext(); )
			{
				OMElement el = resultIterator.next();
				if ("Reason".equals(el.getLocalName()))
				{
					throw new Exception(el.getText());
				}
			}
		}		
		return null;
	}
	
	public OMElement getSelectedObjects() throws Exception{
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;			
		OMElement selectedObjectsRoot = factory.createOMElement(NODE_OBJECTS_SELECTED_ROOT, ns);
		ConnectorConfig settings = getConnectorSettings();
		List<ExtractionObject> extractionObjects = settings.getExtractionObjects();
		List<Properties> selectedObjectsList = getConnectorSelectedObjects(extractionObjects);
		if(selectedObjectsList != null && selectedObjectsList.size() > 0){
			for(Properties props : selectedObjectsList){
				if(props.size() > 0){
					OMElement objectElement = factory.createOMElement(NODE_OBJECT_ROOT, ns);
					for(String key : props.stringPropertyNames()){
						XmlUtils.addContent(factory, objectElement, key, props.getProperty(key), ns);
					}
					selectedObjectsRoot.addChild(objectElement);
				}
			}
		}
		return selectedObjectsRoot;
	}
	
	public OMElement getExtractStatus(){
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;			
		OMElement statusObjectsRoot = factory.createOMElement(NODE_EXTRACT_STATUS, ns);
		XmlUtils.addContent(factory, statusObjectsRoot, NODE_STATUS, isExtractionInProgress() ? "Running" : "Complete", ns);
		/*if(isRunning){
			List<Properties> extractedObjectStatusList = isExtractionInProgress();
			for(Properties props : extractedObjectStatusList){
				if(props.size() > 0){
					OMElement objectElement = factory.createOMElement(NODE_OBJECT_ROOT, ns);
					for(String key : props.stringPropertyNames()){
						XmlUtils.addContent(factory, objectElement, key, props.getProperty(key), ns);
					}
					selectedObjectsRoot.addChild(objectElement);
				}
			}
		}*/
		return statusObjectsRoot;
	}
	
	public OMElement updateSelectedObjects(String userName, String connectionName, String connectorType, String xmlData) throws Exception{
		CloudConnectorConfig config = CloudConnectorConfig.getCloudConnectorConfig(spaceDirectory);
		List<ExtractionObjectWrapper> objects = new ArrayList<ExtractionObjectWrapper>();
		if(xmlData != null && xmlData.trim().length() > 0){
			SAXBuilder builder = new SAXBuilder();
			Document d = builder.build(new StringReader(xmlData));
			Element objectsList = d.getRootElement();
			objects = parseWrapperObjectsList(config, objectsList);
		}
		return updateSelectedObjects(userName, connectionName, connectorType, config, objects);
	}
	
	public void saveSelectedObjects(String userName, String xmlData) throws Exception{		
		List<ExtractionObject> objects = new ArrayList<ExtractionObject>();
		if(xmlData != null && xmlData.trim().length() > 0){
			SAXBuilder builder = new SAXBuilder();
			Document d = builder.build(new StringReader(xmlData));
			Element objectsList = d.getRootElement();
			objects = parseObjectsList(objectsList);
		}
		saveSelectedObjects(userName, objects);
	}
	
	public OMElement startExtract(String xmlData) throws Exception{
		Properties allProps = getConnectorCredentials();
		allProps.setProperty(BaseConnector.SPACE_ID, spaceID);
		allProps.setProperty(BaseConnector.SPACE_DIR, spaceDirectory);
		if(xmlData != null && xmlData.trim().length() > 0){
			// get credentials from incoming request
			SAXBuilder builder = new SAXBuilder();
			Document d = builder.build(new StringReader(xmlData));
			Element root = d.getRootElement();
			Properties incomingProps = parseConnectorProperties(root);
			allProps.putAll(incomingProps);
		}
		
		String uid = UUID.randomUUID().toString();
		ExtractThread t = new ExtractThread(uid, allProps);
		t.start();		
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;			
		OMElement root = factory.createOMElement(NODE_EXTRACT_ROOT, ns);
		XmlUtils.addContent(factory, root, NODE_EXTRACT_ID, uid, ns);
		return root;
	}
	
	public OMElement getMetaData() throws UnrecoverableException{
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;			
		OMElement metaDataRoot = factory.createOMElement(NODE_METADATA_ROOT, ns);
		ConnectorMetaData connectorMetaData = ConnectorManager.getConnector(ConnectorUtils.getConnectorUrl(connectorName)).getMetaData();
		List<ConnectionProperty> connectorProperties = connectorMetaData.getConnectionProperties();
		if(connectorProperties != null && connectorProperties.size() > 0){
			for(ConnectionProperty connectionProperty : connectorProperties){
				if(connectionProperty.name.equals(BaseConnector.SOURCEFILE_NAME_PREFIX) && !connectorMetaData.allowsSourceFileNamePrefix()){
					continue;
				}
				OMElement connPropElement = factory.createOMElement(NODE_CONN_PROPERTY, ns);
				XmlUtils.addContent(factory, connPropElement, "Name", connectionProperty.name, ns);
				XmlUtils.addContent(factory, connPropElement, "Required", connectionProperty.isRequired, ns);
				XmlUtils.addContent(factory, connPropElement, "Secret", connectionProperty.isSecret, ns);
				XmlUtils.addContent(factory, connPropElement, "Encrypted", connectionProperty.isEncrypted, ns);
				XmlUtils.addContent(factory, connPropElement, "DisplayIndex", connectionProperty.displayIndex, ns);
				XmlUtils.addContent(factory, connPropElement, "DisplayLabel", connectionProperty.displayLabel, ns);
				XmlUtils.addContent(factory, connPropElement, "Value", connectionProperty.value, ns);
				metaDataRoot.addChild(connPropElement);
			}
		}
		
		
		// send back the supported type in the form of conn property
		List<ExtractionObject.Types> supportedTypes = connectorMetaData.getSupportedTypes();
		if(supportedTypes != null && supportedTypes.size() > 0){
			for(ExtractionObject.Types type : supportedTypes){
				OMElement connPropElement = factory.createOMElement(NODE_CONN_PROPERTY, ns);
				XmlUtils.addContent(factory, connPropElement, "Name", type.toString(), ns);
				metaDataRoot.addChild(connPropElement);
			}
		}
		return metaDataRoot;
	}
	
	private Properties getConnectorCredentials() throws Exception{
		List<ConnectionProperty> connProps = getConnectorCredentialProperties();
		Properties props = new Properties();
		if(connProps != null && connProps.size() > 0){
			for(ConnectionProperty connProperty : connProps){
				if(connProperty.value != null){
					props.setProperty(connProperty.name, connProperty.value);
				}
			}
		}
		return props;
	}
	
	private List<ConnectionProperty> getConnectorCredentialProperties() throws Exception{
		ConnectorConfig settings  = getConnectorSettings();
		return settings.getConnectionProperties();
	}
	
	private void saveCredentials(Element el) throws Exception{
		
		ConnectorMetaData connectorMetaData = ConnectorManager.getConnector(ConnectorUtils.getConnectorUrl(connectorName)).getMetaData();
		List<ConnectionProperty> connectionProperties = connectorMetaData.getConnectionProperties();
		List<ConnectionProperty> toSaveConnectionProperties = new ArrayList<ConnectionProperty>();
		for(ConnectionProperty connProperty : connectionProperties){
			ConnectionProperty toSaveConnProperty = new ConnectionProperty();
			toSaveConnProperty.name = connProperty.name;
			toSaveConnProperty.isRequired = connProperty.isRequired;
			toSaveConnProperty.isEncrypted = connProperty.isEncrypted;
			toSaveConnProperty.isSecret = connProperty.isSecret;
			toSaveConnProperty.saveToConfig = connProperty.saveToConfig;
			String propValue = getPropertyValue(el, connProperty.name);
			if(propValue != null && propValue.trim().length() > 0){
				toSaveConnProperty.value = propValue;
			}
			toSaveConnectionProperties.add(toSaveConnProperty);
		}
		ConnectorConfig config = getConnectorSettings();
		config.setConnectionProperties(toSaveConnectionProperties);
		config.saveConnectorConfig(spaceDirectory, connectorName);				
	}
	
	@SuppressWarnings("unchecked")
	private List<ExtractionObjectWrapper> parseWrapperObjectsList(CloudConnectorConfig config, Element objectsRoot) throws UnrecoverableException{
		List<ExtractionObjectWrapper> response = new ArrayList<ExtractionObjectWrapper>();
		List<Element> list = objectsRoot.getChildren(NODE_OBJECT_ROOT);
		Map<String, CloudConnection> connectionsMap = new HashMap<String, CloudConnection>();
		Map<String, ExtractionGroup> extractionGroupsMap = new HashMap<String, ExtractionGroup>();
		if (config.getCloudConnections() != null)
		{
			for (CloudConnection conn : config.getCloudConnections())
			{
				connectionsMap.put(conn.getName(), conn);
			}
		}
		if (config.getExtractionGroups() != null)
		{
			for (ExtractionGroup eg : config.getExtractionGroups())
			{
				extractionGroupsMap.put(eg.getName(), eg);
			}
		}
		if(list != null && list.size() > 0){
			for(int i=0; i<list.size(); i++){

				Element socElement = list.get(i);
				String socName = socElement.getChildText("Name");
				String socType = socElement.getChildText("Type");
				String extractionGroupNames = socElement.getChildText(ExtractionObject.EXT_GROUP_NAMES);
				ExtractionObjectWrapper wrp = new ExtractionObjectWrapper();
				ExtractionObject exObject = new ExtractionObject();
				exObject.name = socName;
				exObject.type = getExtractionType(socType);
				exObject.setExtractionGroupNames(extractionGroupNames);
				wrp.setExtractionObject(exObject);
				wrp.setConnectorType(socElement.getChildText("ConnectorType"));
				String cldConnectionNames = socElement.getChildText("ConnectionNames");
				List<CloudConnection> cldConnectionsList = new ArrayList<CloudConnection>();
				if (cldConnectionNames != null && !cldConnectionNames.trim().isEmpty())
				{
					for (String connName : cldConnectionNames.split(COMMA))
					{
						if (connName != null && !connName.trim().isEmpty())
						{
							cldConnectionsList.add(connectionsMap.get(connName.trim()));
						}
					}
					wrp.setCloudConnections(cldConnectionsList);
				}
				List<ExtractionGroup> extractionGroupsList = new ArrayList<ExtractionGroup>();
				if (extractionGroupNames != null && !extractionGroupNames.trim().isEmpty())
				{
					for (String egName : extractionGroupNames.split(COMMA))
					{
						if (egName != null && !egName.trim().isEmpty())
						{
							extractionGroupsList.add(extractionGroupsMap.get(egName.trim()));
						}
					}
					wrp.setExtractionGroups(extractionGroupsList);
				}
				response.add(wrp);
			}
		}
		return response;
	}
	
	@SuppressWarnings("unchecked")
	private List<ExtractionObject> parseObjectsList(Element objectsRoot){
		List<ExtractionObject> response = new ArrayList<ExtractionObject>();
		List<Element> list = objectsRoot.getChildren(NODE_OBJECT_ROOT);
		if(list != null && list.size() > 0){
			for(int i=0; i<list.size(); i++){

				Element socElement = list.get(i);
				String socName = socElement.getChildText("Name");
				String socType = socElement.getChildText("Type");
				String extractionGroupNames = socElement.getChildText(ExtractionObject.EXT_GROUP_NAMES);
				ExtractionObject exObject = new ExtractionObject();
				exObject.name = socName;
				exObject.type = getExtractionType(socType);
				exObject.setExtractionGroupNames(extractionGroupNames);
				response.add(exObject);
			}
		}
		return response;
	}
	
	private OMElement updateSelectedObjects(String userName, String connectionName, String connectorType, CloudConnectorConfig config, List<ExtractionObjectWrapper> objects) throws Exception{
		// Prepopulate the saving list with non-standard objects
		// save it as a general config also
		List<ExtractionObjectWrapper> allConfigObjects = new ArrayList<ExtractionObjectWrapper>(config.getExtractionObjectWrappers());
		List<ExtractionObjectWrapper> currentObjectsForConnection = connectorType.equals("All") ? config.getExtractionObjectWrappers() : config.getObjectsForConnection(connectionName, connectorType);
		Date now = new Date();
		List<ExtractionObjectWrapper> toRemoveList = new ArrayList<ExtractionObjectWrapper>();
		if(objects != null && objects.size() > 0){
			for (ExtractionObjectWrapper eobj : currentObjectsForConnection)
			{
				boolean found = false;
				for(ExtractionObjectWrapper exObject : objects){
					if (eobj.extractionObject.name.equals(exObject.extractionObject.name) && eobj.connectorType.equals(exObject.connectorType))
					{
						found = true;
						break;
					}
				}
				if(!found){
					toRemoveList.add(eobj);					
				}								
			}
		}
		allConfigObjects.removeAll(currentObjectsForConnection);
		if (objects ==  null || objects.size() == 0)
		{
			currentObjectsForConnection = new ArrayList<>();
		}
		else
		{
			currentObjectsForConnection.removeAll(toRemoveList);
		}
		if(objects != null && objects.size() > 0){
			for (ExtractionObjectWrapper exObject : objects)
			{
				ExtractionObjectWrapper object = null;
				for(ExtractionObjectWrapper eobj : currentObjectsForConnection){
					if (eobj.extractionObject.name.equals(exObject.extractionObject.name) && eobj.connectorType.equals(exObject.connectorType))
					{
						object = eobj;
						break;
					}
				}
				if (object == null)
				{
					object = new ExtractionObjectWrapper();
					object.setCreatedBy(userName);
					object.setDateCreated(now);
					currentObjectsForConnection.add(object);
				}
				object.extractionObject = exObject.extractionObject;	
				object.connectorType = exObject.connectorType;
				object.cloudConnections = exObject.cloudConnections;
				object.extractionGroups = exObject.extractionGroups;
				object.setModifiedBy(userName);
				object.setDateModified(now);					
			}
		}
		allConfigObjects.addAll(currentObjectsForConnection);
		config.setExtractionObjectWrappers(allConfigObjects);		
		config.saveCloudConnectorConfig(spaceDirectory, userName);
		Element extObjectsElement = config.getExtractionObjectWrappersXml();
		return XmlUtils.convertToOMElement(XmlUtils.convertElementToString(extObjectsElement));
	}
	
	private void saveSelectedObjects(String userName, List<ExtractionObject> objects) throws Exception{
		// Prepopulate the saving list with non-standard objects
		// save it as a general config also
		ConnectorConfig settings = getConnectorSettings();
		List<ExtractionObject> updated = new ArrayList<ExtractionObject>();
		if(objects != null && objects.size() > 0){
			for(ExtractionObject exObject : objects){
				ExtractionObject object = settings.findObject(exObject.name);
				if(object == null){
					object = new ExtractionObject();
					object.name = exObject.name;
					object.type = exObject.type;
					object.setExtractionGroupNames(exObject.toString());
				}
				if(object != null){
					object.setExtractionGroupNames(exObject.toString());
					updated.add(object);
				}
			}
		}
		settings.setExtractionObjects(updated);		
		if (!Repository.isUseNewConnectorFramework())
		{
			settings.saveConnectorConfig(spaceDirectory, connectorName);
		}
		else
		{
			//set API version if current version is not default version
			String currentAPIVersion = connectorName.substring(connectorName.lastIndexOf(":") + 1);
			String currentAPIVersionConnectionString = connectorName;
			String defaultAPIVersion = ConnectorServices.getDefaultAPIVersion(userName, spaceID, spaceDirectory, currentAPIVersionConnectionString);
			boolean isDefaultVersion = false;
			if (defaultAPIVersion != null)
			{
				String[] versionParts = defaultAPIVersion.substring("connector:birst://".length()).split(":");
				isDefaultVersion =  (versionParts != null && versionParts.length == 2 && versionParts[1].length() > 0 && versionParts[1].equals(currentAPIVersion));
				if (isDefaultVersion || defaultAPIVersion.equals(currentAPIVersionConnectionString))
				{
					settings.setConnectorAPIVersion(null);//wipe out API version from config if it is default version
				}
				else
				{
					settings.setConnectorAPIVersion(currentAPIVersionConnectionString);//write current API version to config
				}
			}			
			settings.saveConnectorConfig(spaceDirectory, getConnectorNameFromConnectorConnectionString(connectorName));
		}
	}
	
	private List<Properties> getConnectorSelectedObjects(List<ExtractionObject> extractionObjects) throws Exception{
		List<Properties> response = new ArrayList<Properties>();
		if(extractionObjects != null && extractionObjects.size() > 0){
			for(ExtractionObject exObject : extractionObjects){
				Properties props = new Properties();
				props.put("Name", exObject.name);
				if(exObject.query != null && exObject.query.length() > 0){
					props.put("Query", exObject.query );
				}
				if(exObject.lastUpdatedDate != null){
					props.put("LastUpdatedDate", XmlUtils.getDateString(exObject.lastUpdatedDate, false));
				}
				if(exObject.lastSystemModStamp != null){
					props.put("lastSystemModStamp", XmlUtils.getDateString(exObject.lastSystemModStamp, false));
				}
				if(exObject.getExtractionGroupNames() != null && exObject.getExtractionGroupNames().size() > 0) {
					props.put(ExtractionObject.EXT_GROUP_NAMES, exObject.toString());
				}
				props.put("Type", exObject.type.toString());
				response.add(props);
			}
			return response;
		}
		return null;
	}
	
	// Get the config file. Check for sforce.xml file and also. 
	// See which is the latest, sync
	private ConnectorConfig getConnectorSettings() throws Exception{
		if (!Repository.isUseNewConnectorFramework())
			return ConnectorConfig.getConnectorConfig(spaceDirectory, connectorName);
		else
			return ConnectorConfig.getConnectorConfig(spaceDirectory, getConnectorNameFromConnectorConnectionString(connectorName));
	}
	
	public static String getConnectorNameFromConnectorConnectionString(String connectionString)
	{
		String connName = connectionString.substring("connector:birst://".length());
		if (connName.indexOf(":") >= 0)
			connName = connName.substring(0, connName.indexOf(":"));
		return connName;
	}
	
	public void cancelExtract()throws Exception{
		String killFileName = Util.getKillFileName(spaceDirectory, Util.KILL_EXTRACT_FILE);
		File file = new File(killFileName);
		if(!file.exists()){
			try {
				file.createNewFile();
			} catch (IOException e) {
				logger.warn("Error while creating killFileName" + killFileName, e);
			}
		}
	}
	
	private boolean isExtractionInProgress(){
		File file = new File(spaceDirectory + File.separator + "SFDC_extract.lock");
		return file.exists();
	}
	 
	private class ExtractThread extends Thread{

		String uid;
		Properties properties;
		public ExtractThread(String uid, Properties properties) {
			this.uid = uid;
			this.properties = properties;
		}

		@Override
		public void run() {
			ApplicationContext ac = null;
			try {
				ac = ApplicationContext.getInstance(spaceDirectory, null);
				Repository r = ac.getRepository();
				ConnectorConfig settings = ConnectorConfig.getConnectorConfig(spaceDirectory, connectorName);
				if(settings != null){
					List<ExtractionObject> extractObjects = settings.getExtractionObjects();
					if(extractObjects != null && extractObjects.size() > 0){
						ConnectorOperations connectorOperations = new ConnectorOperations(r, connectorName, 
								properties.getProperty(BaseConnector.BIRST_PARAMETERS), properties.getProperty(BaseConnector.SANDBOX_URL));
						connectorOperations.setUid(uid);

						int numThreads = 4;
						String numThreadsString = properties.getProperty(PROP_NAME_NUM_THREADS,"4");
						try{
							numThreads = Integer.parseInt(numThreadsString);
						}
						catch(Exception ex)
						{
							logger.warn("Error parsing the num of parallel threads property. Using default ");
							numThreads = 4;
						}
						connectorOperations.extractData(numThreads);
					}
				}				
			}catch(Exception ex){
				logger.error("Error while running extraction", ex);
			}
			finally
			{
				try
				{
					if(ac != null){
						ac.dispose();
					}
				}catch(Exception ex){
					logger.warn("Exception while disposing of application context in extract :" + spaceDirectory);
				}
			}
		}
	}

	public OMElement getObjectQuery(String xmlData) throws Exception {
		Properties allProps = getConnectorCredentials();
		allProps.setProperty(BaseConnector.SPACE_ID, spaceID);
		allProps.setProperty(BaseConnector.SPACE_DIR, spaceDirectory);
		SAXBuilder builder = new SAXBuilder();
		Document d = builder.build(new StringReader(xmlData));
		Element rootEl = d.getRootElement();
		Element connectorPropertiesRoot = rootEl.getChild(NODE_CONN_PROPERTY_ROOT);
		Properties inputProps = parseConnectorProperties(connectorPropertiesRoot);
		
		if(inputProps != null && inputProps.size() > 0){
			allProps.putAll(inputProps);
		}
		IConnector connector = ConnectorManager.getConnector(ConnectorUtils.getConnectorUrl(connectorName));
		Element objectRoot = rootEl.getChild(NODE_OBJECT_ROOT);
		ExtractionObject object = paraseQueryObjectDetails(objectRoot);
		
		int count = 0;
		boolean retry = false;
		do{
			try{
				connector.connect(allProps);
				connector.fillObjectDetails(object);
				retry = false;
			}
			catch(RecoverableException ex){
				count++;
				retry = count < maxRetries;
				if(retry){
					Thread.sleep(500);
					logger.warn("Retrying getObjectQuery : " +count + " ", ex);
				}
				else{
					logger.warn("Exhausted the retries for getObjectQuery : " +count + " ", ex);
				}
			}
		}while(retry);
		
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;			
		OMElement root = factory.createOMElement(NODE_OBJECT_ROOT, ns);
		XmlUtils.addContent(factory, root, NODE_OBJECT_NAME, object.name, ns);
		XmlUtils.addContent(factory, root, NODE_OBJECT_TYPE, object.type.toString(), ns);
		XmlUtils.addContent(factory, root, NODE_OBJECT_QUERY, object.query, ns);
		XmlUtils.addContent(factory, root, NODE_OBJECT_MAPPING, object.columnMappings, ns);
		return root;
	}
	
	public OMElement validateObjectQuery(String xmlData) throws Exception {
		Properties allProps = getConnectorCredentials();
		allProps.setProperty(BaseConnector.SPACE_ID, spaceID);
		allProps.setProperty(BaseConnector.SPACE_DIR, spaceDirectory);
		SAXBuilder builder = new SAXBuilder();
		Document d = builder.build(new StringReader(xmlData));
		Element rootEl = d.getRootElement();
		Element connectorPropertiesRoot = rootEl.getChild(NODE_CONN_PROPERTY_ROOT);
		Properties inputProps = parseConnectorProperties(connectorPropertiesRoot);
		
		if(inputProps != null && inputProps.size() > 0){
			allProps.putAll(inputProps);
		}
		IConnector connector = ConnectorManager.getConnector(ConnectorUtils.getConnectorUrl(connectorName));
		Element objectRoot = rootEl.getChild(NODE_OBJECT_ROOT);
		ExtractionObject object = paraseQueryObjectDetails(objectRoot);
		connector.connect(allProps);
		boolean isValid = connector.validateQueryForObject(object);			
		
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;			
		OMElement root = factory.createOMElement(NODE_OBJECT_ROOT, ns);
		XmlUtils.addContent(factory, root, NODE_OBJECT_NAME, object.name, ns);
		XmlUtils.addContent(factory, root, NODE_OBJECT_TYPE, object.type.toString(), ns);
		XmlUtils.addContent(factory, root, NODE_OBJECT_QUERY, object.query, ns);
		XmlUtils.addContent(factory, root, NODE_OBJECT_ISVALIDQUERY, String.valueOf(isValid), ns);
		return root;
	}

	public OMElement getQueryObjectDetails(String xmlData) throws Exception {
		SAXBuilder builder = new SAXBuilder();
		Document d = builder.build(new StringReader(xmlData));
		Element rootEl = d.getRootElement();
		Element objectRoot = rootEl.getChild(NODE_OBJECT_ROOT);
		
		ExtractionObject object = paraseQueryObjectDetails(objectRoot);
		
		ConnectorConfig config = getConnectorSettings();
		ExtractionObject existingObject = config.findObject(object.name);
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;			
		OMElement root = factory.createOMElement(NODE_OBJECT_ROOT, ns);
		if(existingObject != null)
		{
			XmlUtils.addContent(factory, root, NODE_OBJECT_NAME, existingObject.name, ns);
			XmlUtils.addContent(factory, root, NODE_OBJECT_TYPE, existingObject.type.toString(), ns);
			XmlUtils.addContent(factory, root, NODE_OBJECT_QUERY, existingObject.query, ns);
			XmlUtils.addContent(factory, root, NODE_OBJECT_MAPPING, existingObject.columnMappings, ns);
			XmlUtils.addContent(factory, root, NODE_INCLUDE_ALL_RECORDS, existingObject.includeAllRecords, ns);
		}
		return root;
	}
	
	public OMElement getSavedObjectDetails(String xmlData) throws Exception {
		// Parse object xml.
		SAXBuilder builder = new SAXBuilder();
		Document d = builder.build(new StringReader(xmlData));
		Element rootEl = d.getRootElement();
		Element objectRoot = rootEl.getChild(NODE_OBJECT_ROOT);
		ExtractionObject object = parseObjectDetails(objectRoot);
		
		// Get configuration and build xml.
		ConnectorConfig config = getConnectorSettings();
		ExtractionObject existingObject = config.findObject(object.name);
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;			
		OMElement root = factory.createOMElement(NODE_OBJECT_ROOT, ns);
		if(existingObject != null)
		{
			XmlUtils.addContent(factory, root, NODE_OBJECT_NAME, existingObject.name, ns);
			XmlUtils.addContent(factory, root, NODE_OBJECT_TYPE, existingObject.type.toString(), ns);
			XmlUtils.addContent(factory, root, NODE_OBJECT_TECH_NAME, existingObject.techName, ns);
			XmlUtils.addContent(factory, root, NODE_OBJECT_SELECTION_CRITERIA, existingObject.selectionCriteria, ns);
			if (existingObject.columnNames != null) {
				XmlUtils.addContent(factory, root, NODE_OBJECT_COLUMNS, StringUtils.join(existingObject.columnNames, COMMA), ns);
			} if (existingObject.isIncludeAllRecords() != null){
				XmlUtils.addContent(factory, root, NODE_INCLUDE_ALL_RECORDS, existingObject.isIncludeAllRecords(), ns);
			} if (existingObject.getObjectId() != null){
				XmlUtils.addContent(factory, root, NODE_OBJECT_ID, existingObject.getObjectId(), ns);
			} if (existingObject.getURL() != null){
				XmlUtils.addContent(factory, root, NODE_OBJECT_URL, existingObject.getURL(), ns);
			} if (existingObject.getRecordType() != null){
				XmlUtils.addContent(factory, root, NODE_OBJECT_RECORD_TYPE, existingObject.getRecordType(), ns);
			} if (existingObject.getProfileID() != null){
				XmlUtils.addContent(factory, root, NODE_OBJECT_PROFILE_ID, existingObject.getProfileID(), ns);
			} if (existingObject.getDimensions() != null){
				XmlUtils.addContent(factory, root, NODE_OBJECT_DIMENSIONS, existingObject.getDimensions(), ns);
			} if (existingObject.getMetrics() != null){
				XmlUtils.addContent(factory, root, NODE_OBJECT_METRICS, existingObject.getMetrics(), ns);
			} if (existingObject.getSegmentID() != null){
				XmlUtils.addContent(factory, root, NODE_OBJECT_SEGMENT_ID, existingObject.getSegmentID(), ns);
			} if (existingObject.getFilters() != null){
				XmlUtils.addContent(factory, root, NODE_OBJECT_FILTERS, existingObject.getFilters(), ns);
			} if (existingObject.getStartDate() != null){
				XmlUtils.addContent(factory, root, NODE_OBJECT_START_DATE, existingObject.getStartDate(), ns);
			} if (existingObject.getEndDate() != null){
				XmlUtils.addContent(factory, root, NODE_OBJECT_END_DATE, existingObject.getEndDate(), ns);
			} if (existingObject.getSamplingLevel() != null){
				XmlUtils.addContent(factory, root, NODE_OBJECT_SAMPLING_LEVEL, existingObject.getSamplingLevel(), ns);
			}if (existingObject.getReportSuiteID() != null){
				XmlUtils.addContent(factory, root, NODE_OBJECT_REPORT_SUITE_ID, existingObject.getReportSuiteID(), ns);
			}if (existingObject.getElements() != null){
				XmlUtils.addContent(factory, root, NODE_OBJECT_ELEMENTS, existingObject.getElements(), ns);
			}if (existingObject.getPageSize() != null){
				XmlUtils.addContent(factory, root, NODE_OBJECT_PAGESIZE, existingObject.getPageSize(), ns);
			}if (existingObject.getQuery() != null){
				XmlUtils.addContent(factory, root, NODE_OBJECT_QUERY, existingObject.getQuery(), ns);
			}
			XmlUtils.addContent(factory, root, NODE_OBJECT_MAPPING, existingObject.columnMappings, ns);
		}
		return root;
	}
	
	public void saveQueryObject(String userName, String xmlData) throws Exception {
		SAXBuilder builder = new SAXBuilder();
		Document d = builder.build(new StringReader(xmlData));
		Element rootEl = d.getRootElement();
		Element objectRoot = rootEl.getChild(NODE_OBJECT_ROOT);
		ExtractionObject object = paraseQueryObjectDetails(objectRoot);
		Element credentialsEl = rootEl.getChild(NODE_CONN_PROPERTY_ROOT);
		if (credentialsEl != null)
		{
			String connectionID = credentialsEl.getChildText(NODE_CONNECTION_ID);
			if (connectionID != null && !connectionID.isEmpty())
			{
				saveObjectToNewConfig(connectionID, object, userName);
				return;				
			}
		}
		
		ConnectorConfig settings = getConnectorSettings();
		List<ExtractionObject> exObjects = settings.getExtractionObjects();
		if(exObjects == null){
			exObjects = new ArrayList<ExtractionObject>();
			settings.setExtractionObjects(exObjects);
		}
		// find and update the extraction object
		ExtractionObject soc = settings.findObject(object.name);
		if(soc == null)
		{
			soc = new ExtractionObject();
			soc.name = object.name;
			soc.type = object.type;
			exObjects.add(soc);
		}
		else if(soc.query == null || soc.query.trim().length() == 0)
		{
			throw new Exception("An existing standard object with the same name exists");
		}
		soc.query = object.query;
		soc.columnMappings = object.columnMappings;
		soc.includeAllRecords = object.includeAllRecords;
		boolean isSFDCConnector = false; 
		if (Repository.isUseNewConnectorFramework())
		{
			isSFDCConnector = CONNECTOR_NAME_SFDC.equalsIgnoreCase(getConnectorNameFromConnectorConnectionString(connectorName));
		}
		else
		{
			isSFDCConnector = CONNECTOR_NAME_SFDC.equalsIgnoreCase(connectorName);
		}
		if(object.type == ExtractionObject.Types.QUERY && isSFDCConnector)
		{
			saveObjectMappingFile(object);
		}
		if (!Repository.isUseNewConnectorFramework())
		{
			settings.saveConnectorConfig(spaceDirectory, connectorName);
		}
		else
		{
			settings.saveConnectorConfig(spaceDirectory, getConnectorNameFromConnectorConnectionString(connectorName));
		}		
	}
	
	public void saveObjectToNewConfig(String connectionID, ExtractionObject object, String userName) throws Exception
	{
		CloudConnectorConfig config = CloudConnectorConfig.getCloudConnectorConfig(spaceDirectory);
		CloudConnection conn = config.findCloudConnectionByGuid(connectionID);
		if (conn != null)
		{
			Date now = new Date();
			List<ExtractionObjectWrapper> allObjects = config.getExtractionObjectWrappers();
			List<ExtractionObjectWrapper> currentObjects = config.getObjectsForConnection(conn.getName(), conn.getConnectorType());
			allObjects.removeAll(currentObjects);
			ExtractionObjectWrapper wrp = null;
			if (currentObjects != null)
			{
				for (ExtractionObjectWrapper obj : currentObjects)
				{
					if (obj.getExtractionObject().name.equals(object.name))
					{
						wrp = obj;
					}
				}
			}
			if (wrp == null)
			{
				wrp = new ExtractionObjectWrapper();
				wrp.connectorType = conn.getConnectorType();
				wrp.addCloudConnection(conn);
				wrp.setCreatedBy(userName);
				wrp.setDateCreated(now);
				currentObjects.add(wrp);
			}
			wrp.setExtractionObject(object);
			wrp.setDateModified(now);
			wrp.setModifiedBy(userName);
			allObjects.addAll(currentObjects);
			config.saveCloudConnectorConfig(spaceDirectory, userName);
		}
	}
	
	public void saveObject(String userName, String xmlData) throws Exception {
		// Parse the object xml.
		SAXBuilder builder = new SAXBuilder();
		Document d = builder.build(new StringReader(xmlData));
		Element rootEl = d.getRootElement();
		Element objectRoot = rootEl.getChild(NODE_OBJECT_ROOT);
		ExtractionObject object = parseObjectDetails(objectRoot);
		Element credentialsEl = rootEl.getChild(NODE_CONN_PROPERTY_ROOT);
		if (credentialsEl != null)
		{
			String connectionID = credentialsEl.getChildText(NODE_CONNECTION_ID);
			if (connectionID != null && !connectionID.isEmpty())
			{
				saveObjectToNewConfig(connectionID, object, userName);
				return;				
			}
		}
		
		ConnectorConfig settings = getConnectorSettings();
		List<ExtractionObject> exObjects = settings.getExtractionObjects();
		if(exObjects == null){
			exObjects = new ArrayList<ExtractionObject>();
			settings.setExtractionObjects(exObjects);
		}
		
		// Find and update the extraction object.
		ExtractionObject soc = settings.findObject(object.name);
		if(soc == null)
		{
			soc = new ExtractionObject();
			soc.name = object.name;
			soc.type = object.type;
			exObjects.add(soc);
		}
		soc.techName = object.techName;
		if(object.query != null)
		{
			soc.query = object.query;
		}
		soc.columnNames = object.columnNames;
		soc.columnMappings = object.columnMappings;
		soc.selectionCriteria = object.selectionCriteria;
		soc.objectId = object.objectId;
		soc.includeAllRecords = object.isIncludeAllRecords();
		soc.url = object.url;
		soc.recordType = object.recordType;
		soc.profileID = object.profileID;
		soc.dimensions = object.dimensions;
		soc.metrics = object.metrics;
		soc.segmentID = object.segmentID;
		soc.filters = object.filters;
		soc.startDate = object.startDate;
		soc.endDate = object.endDate;
		soc.samplingLevel = object.samplingLevel;
		soc.reportSuiteID = object.reportSuiteID;
		soc.elements = object.elements;
		soc.pageSize = object.pageSize;
		
		// Save to appropriate config.
		boolean isSFDCConnector = false; 
		if (Repository.isUseNewConnectorFramework()) {
			isSFDCConnector = CONNECTOR_NAME_SFDC.equalsIgnoreCase(getConnectorNameFromConnectorConnectionString(connectorName));
		} else {
			isSFDCConnector = CONNECTOR_NAME_SFDC.equalsIgnoreCase(connectorName);
		}
		if(object.type == ExtractionObject.Types.QUERY && isSFDCConnector) {
			saveObjectMappingFile(object);
		}
		if (!Repository.isUseNewConnectorFramework()) {
			settings.saveConnectorConfig(spaceDirectory, connectorName);
		} else {
			settings.saveConnectorConfig(spaceDirectory, getConnectorNameFromConnectorConnectionString(connectorName));
		}		
	}
	
	public void saveConnection(String userName, OMElement connectorMetaDataXML, String connectionDetailsXML) throws Exception
	{
		List<ConnectionProperty> connectionProperties = new ArrayList<ConnectionProperty>();
		for (Iterator<OMElement> rootIterator = connectorMetaDataXML.getChildElements(); rootIterator.hasNext(); )
		{
			OMElement cpEl = rootIterator.next();
			if (NODE_CONN_PROPERTY.equals(cpEl.getLocalName()))
			{
				Iterator<OMElement> propertiesWithValueChild = cpEl.getChildrenWithLocalName("Value");
				if (propertiesWithValueChild.hasNext())
				{
					ConnectionProperty cp = new ConnectionProperty();
					for (Iterator<OMElement> connPropertiesIterator = cpEl.getChildElements(); connPropertiesIterator.hasNext(); )
					{
						OMElement propEl = connPropertiesIterator.next();
						switch (propEl.getLocalName())
						{
							case "Name"	:	cp.name = propEl.getText(); break;
							case "Required"	:	cp.isRequired = Boolean.parseBoolean(propEl.getText()); break;
							case "Secret"	:	cp.isSecret = Boolean.parseBoolean(propEl.getText()); break;
							case "Encrypted"	:	cp.isEncrypted = Boolean.parseBoolean(propEl.getText()); break;
							case "SaveToConfig"	:	cp.saveToConfig = Boolean.parseBoolean(propEl.getText()); break;
							case "DisplayIndex"	:	cp.displayIndex = Integer.parseInt(propEl.getText()); break;
							case "DisplayLabel"	:	cp.displayLabel = propEl.getText(); break;
							case "DisplayType"	:	cp.displayLabel = propEl.getText(); break;
						}						
					}
					connectionProperties.add(cp);
				}
			}				
		}
		StringReader stringReader = new StringReader(connectionDetailsXML);
		SAXBuilder builder = new SAXBuilder();
		Document doc = builder.build(stringReader);
		Element connDetailsEl = doc.getRootElement();
		connDetailsEl.detach();
		String connectionName = connDetailsEl.getChildText("ConnectionName");
		String connectorType = connDetailsEl.getChildText("ConnectorType");
		boolean isNewConnection = Boolean.parseBoolean(connDetailsEl.getChildText("IsNewConnection"));
		String connectionID = null;
		if (!isNewConnection)
		{
			connectionID = connDetailsEl.getChildText("ConnectionID");
		}
		Properties properties = new Properties();
		for(ConnectionProperty connectionProperty : connectionProperties)
		{
			fillInPropsIfNotNull(connDetailsEl, connectionProperty.name, properties);
		}
		for (String prop : properties.stringPropertyNames())			
		{
			for(ConnectionProperty connectionProperty : connectionProperties)
			{
				if (connectionProperty.name.equals(prop))
				{
					connectionProperty.value = properties.getProperty(prop);
					break;
				}
			}			
		}
		CloudConnectorConfig config = CloudConnectorConfig.getCloudConnectorConfig(spaceDirectory);
		CloudConnection conn = null;
		Date now = new Date();
		if (!isNewConnection && connectionID != null && !connectionID.isEmpty())
		{
			conn = config.findCloudConnectionByGuid(connectionID);
		}
		else
		{
			conn = new CloudConnection();
			conn.setGuid(UUID.randomUUID().toString());
			conn.setCreatedBy(userName);
			conn.setDateCreated(now);
			config.addCloudConnection(conn);			
		}
		conn.setName(connectionName);
		conn.setConnectorType(connectorType);
		conn.setConnectorAPIVersion(connectorName);
		conn.setConnectionProperties(connectionProperties);
		conn.setModifiedBy(userName);
		conn.setDateModified(now);
		config.saveCloudConnectorConfig(spaceDirectory, userName);
	}
	
	private void saveObjectMappingFile(ExtractionObject object) throws IOException{
		String objectMappingFileName = getOldSforceMappingFile(object.name);
		BufferedWriter writer = null;
		try
		{	
			File mappingFile = new File(objectMappingFileName);
			if (mappingFile.getParent() != null && !mappingFile.getParent().isEmpty())
			{
				File parentDir = new File(mappingFile.getParent());
				parentDir.mkdirs();
			}
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(objectMappingFileName, false), "UTF-8"));
			writer.write(object.columnMappings);
			logger.debug("Successfully written mapping file : " + objectMappingFileName);
		}finally
		{
			try
			{
				if(writer != null)
					writer.close();
			}
			catch(Exception ex2)
			{
				logger.warn("Error while closing writer for " + objectMappingFileName);
			}
		}
	}
	
	private ExtractionObject paraseQueryObjectDetails(Element root) throws JDOMException, IOException
	{
		String objectName = root.getChildText(NODE_OBJECT_NAME);
		String objectType = root.getChildText(NODE_OBJECT_TYPE);
		String objectQuery = root.getChildText(NODE_OBJECT_QUERY);
		String objectMapping = root.getChildText(NODE_OBJECT_MAPPING);
		Boolean includeAllRecords = root.getChildText(NODE_INCLUDE_ALL_RECORDS) == null ? false : Boolean.valueOf(root.getChildText(NODE_INCLUDE_ALL_RECORDS));
		ExtractionObject extractionObject = new ExtractionObject();
		extractionObject.name = objectName;
		extractionObject.type = getExtractionType(objectType);
		extractionObject.query = objectQuery;
		extractionObject.columnMappings = objectMapping;
		extractionObject.includeAllRecords = includeAllRecords;
		return extractionObject;
	}
	
	private ExtractionObject parseObjectDetails(Element root) throws JDOMException, IOException
	{
		ExtractionObject extractionObject = new ExtractionObject();
		extractionObject.name = root.getChildText(NODE_OBJECT_NAME);
		extractionObject.type = getExtractionType(root.getChildText(NODE_OBJECT_TYPE));
		extractionObject.techName = root.getChildText(NODE_OBJECT_TECH_NAME);
		if (root.getChildText(NODE_OBJECT_COLUMNS) != null) {
			extractionObject.columnNames = root.getChildText(NODE_OBJECT_COLUMNS).split(COMMA);
		}
		if (root.getChildText(NODE_INCLUDE_ALL_RECORDS) != null) {
			extractionObject.includeAllRecords = Boolean.valueOf(root.getChildText(NODE_INCLUDE_ALL_RECORDS));
		}
		if (root.getChildText(NODE_OBJECT_QUERY) != null) {
			extractionObject.query = root.getChildText(NODE_OBJECT_QUERY);
		}
		extractionObject.columnMappings = root.getChildText(NODE_OBJECT_MAPPING);
		extractionObject.selectionCriteria = root.getChildText(NODE_OBJECT_SELECTION_CRITERIA);
		extractionObject.objectId = root.getChildText(NODE_OBJECT_ID);
		extractionObject.url = root.getChildText(NODE_OBJECT_URL);
		extractionObject.recordType = root.getChildText(NODE_OBJECT_RECORD_TYPE);
		extractionObject.profileID = root.getChildText(NODE_OBJECT_PROFILE_ID);
		extractionObject.dimensions = root.getChildText(NODE_OBJECT_DIMENSIONS);
		extractionObject.metrics = root.getChildText(NODE_OBJECT_METRICS);
		extractionObject.segmentID = root.getChildText(NODE_OBJECT_SEGMENT_ID);
		extractionObject.filters = root.getChildText(NODE_OBJECT_FILTERS);
		extractionObject.startDate = root.getChildText(NODE_OBJECT_START_DATE);
		extractionObject.endDate = root.getChildText(NODE_OBJECT_END_DATE);
		extractionObject.samplingLevel = root.getChildText(NODE_OBJECT_SAMPLING_LEVEL);
		extractionObject.reportSuiteID = root.getChildText(NODE_OBJECT_REPORT_SUITE_ID);
		extractionObject.elements = root.getChildText(NODE_OBJECT_ELEMENTS);
		extractionObject.pageSize = root.getChildText(NODE_OBJECT_PAGESIZE);
		return extractionObject;
	}
	
	public static ExtractionObject.Types getExtractionType(String objectType)
	{
		if(objectType == null)
			return null;
		ExtractionObject.Types extractionType = ExtractionObject.Types.OBJECT;
		if(ExtractionObject.Types.QUERY.toString().equalsIgnoreCase(objectType))
		{
			extractionType = ExtractionObject.Types.QUERY;
		}
		
		if(ExtractionObject.Types.OBJECT.toString().equalsIgnoreCase(objectType))
		{
			extractionType = ExtractionObject.Types.OBJECT;
		}
		
		if(ExtractionObject.Types.SAVED_OBJECT.toString().equalsIgnoreCase(objectType)) {
			extractionType = ExtractionObject.Types.SAVED_OBJECT; 
		}
		
		if(ExtractionObject.Types.SAVED_SEARCH_OBJECT.toString().equalsIgnoreCase(objectType)) {
			extractionType = ExtractionObject.Types.SAVED_SEARCH_OBJECT; 
		}
		
		if(ExtractionObject.Types.SAVED_GOOGLE_ANALYTICS_QUERY.toString().equalsIgnoreCase(objectType)) {
			extractionType = ExtractionObject.Types.SAVED_GOOGLE_ANALYTICS_QUERY;
		}
		
		if(ExtractionObject.Types.SAVED_OMNITURE_SITECATALYST_OBJECT.toString().equalsIgnoreCase(objectType)) {
			extractionType = ExtractionObject.Types.SAVED_OMNITURE_SITECATALYST_OBJECT;
		}
		
		return extractionType;
	}
	
	public String getOldSforceConfigFile(){
		return spaceDirectory + File.separator + "sforce.xml";
	}
	
	public String getOldSforceMappingFile(String objectName){
		return spaceDirectory + File.separator + "mapping" + File.separator + objectName + "-mapping.txt";
	}
	
	public String getConnectorConfigFile()
	{	
		return spaceDirectory + File.separator + connectorName + File.separator + "sforce.xml"; 
	}
	
	public String getConnectorMappingFile(String objectName)
	{
		return spaceDirectory + File.separator + connectorName + File.separator + "mapping" + File.separator + objectName + "-mapping.txt";
		//return spaceDirectory + File.separator + connectorName + "mapping" + File.separator + objectName + "-mapping.txt"; 
	}
}
