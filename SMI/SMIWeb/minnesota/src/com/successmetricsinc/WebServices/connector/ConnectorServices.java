package com.successmetricsinc.WebServices.connector;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;
import org.jdom.Element;

import com.successmetricsinc.Repository;
import com.successmetricsinc.ServerContext;
import com.successmetricsinc.UI.ApplicationContext;
import com.successmetricsinc.WebServices.BirstWebServiceResult;
import com.successmetricsinc.WebServices.XMLWebServiceResult;
import com.successmetricsinc.WebServices.trustedservice.TrustedService;
import com.successmetricsinc.connectors.BaseConnector;
import com.successmetricsinc.connectors.CloudConnection;
import com.successmetricsinc.connectors.CloudConnectorConfig;
import com.successmetricsinc.connectors.ConnectionProperty;
import com.successmetricsinc.connectors.ConnectorConfig;
import com.successmetricsinc.connectors.ConnectorMetaData;
import com.successmetricsinc.connectors.ConnectorUtil;
import com.successmetricsinc.connectors.ExtractionObject;
import com.successmetricsinc.connectors.UnrecoverableException;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XmlUtils;
import com.successmetricsinc.warehouse.StagingTable;

public class ConnectorServices {
	
	public static final String NODE_CLOUDCONNECTORS_ROOT = "CloudConnectors";
	public static final String NODE_CONNECTOR_NAME = "ConnectorName";
	public static final String NODE_CONNECTOR_VERSION = "ConnectorVersion";
	public static final String NODE_CONNECTOR_CONNECTIONSTRING = "ConnectorConnectionString";
	public static final String NODE_CONNECTOR_IS_DEFAULT_VERSION = "IsDefaultAPIVersion";
	public static final String NODE_CONNECTOR_SERVER_URL = "ConnectorServerURL";
	public static final String NODE_CONNECTOR_APPLICATION = "ConnectorApplication";
	private static final Logger logger = Logger.getLogger(ConnectorServices.class);
	private static final String CONNECTORCONTROLLER_APPLICATION = "ConnectorController";
	private static final String LISTCONNECTORS_SERVLET = "ListConnectors";
	public static final String ICONNECTOR_SERVLET = "IConnectorServlet";
	private static final String GET_DEFAULT_VERSION_SERVLET = "GetDefaultVersion";
	
	private static final String NODE_ISINCREMENTAL_ROOT = "IsIncrementalRoot";
	private static final String NODE_EXTRACTION_OBJECT = "ExtractionObject";
	private static final String NODE_OBJECT_NAME = "ObjectName";
	private static final String NODE_IS_INCREMENTAL = "IsIncremental";
	
	public static final String ConnectorClient = "SMIWeb";
	public static final int BirstConnectorVersion = 1;
	
	public class ConnectorInfo {
		String connectorServerURL;
		String connectorApplication;
	}
	
	public static Map<String, String> getConnectorDefaultArguments(String userID, String spaceID, String spaceDirectory) {
		Map<String, String> argumentsMap = new HashMap<String, String>();
		argumentsMap.put("UserID", userID);
		argumentsMap.put("SpaceID", spaceID);
		if (spaceDirectory != null)
		{
			argumentsMap.put("SpaceDirectory", spaceDirectory);
		}
		argumentsMap.put("Client", ConnectorClient);
		argumentsMap.put("BirstConnectorVersion", String.valueOf(BirstConnectorVersion));
		return argumentsMap;
	}
	
	public OMElement getConnectorsList(String userName, String spaceID) {
		BirstWebServiceResult response = new BirstWebServiceResult();
		try{
			ConnectorHttpClient client = new ConnectorHttpClient();
			Map<String, String> argumentsMap = getConnectorDefaultArguments(userName, spaceID, null);
			String result = client.callConnectorServlet(ServerContext.getConnectorControllerURI(), CONNECTORCONTROLLER_APPLICATION, LISTCONNECTORS_SERVLET, true, argumentsMap);
			OMElement root = XmlUtils.convertToOMElement(result);
			if (root == null)
			{
				UnrecoverableException ure = new UnrecoverableException("Unable to get List of Connectors", new Exception());
				ure.setErrorCode(BaseException.ERROR_OTHER);
				throw ure;
			}
			response.setResult(new XMLWebServiceResult(root));
		}catch(Exception ex){
			response.setException(ex);
		}
		return response.toOMElement();
	}
	
	public OMElement getCloudConnectorConfigXML(String userName, String spaceID, String spaceDirectory) {
		BirstWebServiceResult response = new BirstWebServiceResult();
		try{
			ConnectorHttpClient client = new ConnectorHttpClient();
			Map<String, String> argumentsMap = getConnectorDefaultArguments(userName, spaceID, null);
			String result = client.callConnectorServlet(ServerContext.getConnectorControllerURI(), CONNECTORCONTROLLER_APPLICATION, LISTCONNECTORS_SERVLET, true, argumentsMap);
			OMElement root = XmlUtils.convertToOMElement(result);
			if (root == null)
			{
				UnrecoverableException ure = new UnrecoverableException("Unable to get List of Connectors", new Exception());
				ure.setErrorCode(BaseException.ERROR_OTHER);
				throw ure;
			}
			List<Connector> connectors = new ArrayList<Connector>();
			Iterator<OMElement> iter = root.getChildren();
			Iterator<OMElement> iter2;
			while (iter.hasNext()) {
				OMElement connectorRootElement = iter.next();
				iter2 = connectorRootElement.getChildren();
				Connector conn = new Connector();
				while (iter2.hasNext()) {
					OMElement connectorElement = iter2.next();
					String name = connectorElement.getLocalName();
					switch (name)
					{
						case NODE_CONNECTOR_NAME	:	conn.ConnectorName = connectorElement.getText(); break;
						case NODE_CONNECTOR_VERSION	:	conn.ConnectorAPIVersion = connectorElement.getText(); break;
						case NODE_CONNECTOR_CONNECTIONSTRING	:	conn.ConnectionString = connectorElement.getText(); break;
						case NODE_CONNECTOR_IS_DEFAULT_VERSION	:	conn.IsDefaultAPIVersion = Boolean.parseBoolean(connectorElement.getText()); break;
					}
				}	
				connectors.add(conn);
			}
			//migrate old configs if needed
			Map<String, String> availableConnectorTypes = new HashMap<String, String>();
			for (Connector conn : connectors)
			{
				if (conn.IsDefaultAPIVersion)
				{
					availableConnectorTypes.put(conn.ConnectorName, conn.ConnectionString);					
				}					
			}
			CloudConnectorConfig cloudConfig = CloudConnectorConfig.getCloudConnectorConfig(spaceDirectory);
			boolean isMigrated = false;
			for (String connType : availableConnectorTypes.keySet())
			{
				String connectorName = ConnectorData.getConnectorNameFromConnectorConnectionString(availableConnectorTypes.get(connType));
				if(ConnectorUtil.getConnectorConfigLastModified(connectorName, spaceDirectory) > CloudConnectorConfig.getLastModified(spaceDirectory))
				{
					//need to migrate old config
					isMigrated = true;
					ConnectorConfig oldConfig = ConnectorConfig.getConnectorConfig(spaceDirectory, connectorName);
					String connAPIVersion = oldConfig.getConnectorAPIVersion() == null ? availableConnectorTypes.get(connType) : oldConfig.getConnectorAPIVersion();
					cloudConfig.migrateCloudConnectorConfig(connType, connType.replaceAll(" ", ""), connAPIVersion,  oldConfig, userName);
				}
			}
			if (isMigrated)
			{
				Date now = new Date();
				if (cloudConfig.getCreatedBy() == null || cloudConfig.getCreatedBy().isEmpty())
				{
					cloudConfig.setCreatedBy(userName);
					cloudConfig.setDateCreated(now);
				}
				cloudConfig.setModifiedBy(userName);
				cloudConfig.setDateModified(now);
				cloudConfig.saveCloudConnectorConfig(spaceDirectory, userName);
			}
			OMElement configElement = XmlUtils.convertToOMElement(XmlUtils.convertElementToString(cloudConfig.getCloudConnectorConfigXML()));
			OMNamespace ns = null;
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMElement rootEl = factory.createOMElement(NODE_CLOUDCONNECTORS_ROOT, ns);
			rootEl.addChild(root);
			rootEl.addChild(configElement);
			response.setResult(new XMLWebServiceResult(rootEl));
		}catch(Exception ex){
			response.setException(ex);
		}
		return response.toOMElement();
	}
	
	public class Connector
    {
        String ConnectorName;
        String ConnectorAPIVersion;
        boolean IsDefaultAPIVersion;
        boolean IsSelectedAPIVersion;
        String ConnectionString;
		public boolean isIsSelectedAPIVersion()
		{
			return IsSelectedAPIVersion;
		}
		public void setIsSelectedAPIVersion(boolean isSelectedAPIVersion)
		{
			IsSelectedAPIVersion = isSelectedAPIVersion;
		}
		public String getConnectorName()
		{
			return ConnectorName;
		}
		public String getConnectorAPIVersion()
		{
			return ConnectorAPIVersion;
		}
		public boolean isIsDefaultAPIVersion()
		{
			return IsDefaultAPIVersion;
		}
		public String getConnectionString()
		{
			return ConnectionString;
		}
    }

	public OMElement getConnectorCredentials(String spaceID, String spaceDirectory, String connectorName){
		BirstWebServiceResult response = new BirstWebServiceResult();
		try{
			OMElement root = (new ConnectorData(spaceID, spaceDirectory, connectorName)).getCredentials();
			response.setResult(new XMLWebServiceResult(root));
		}catch(Exception ex){
			response.setException(ex);
		}
		return response.toOMElement();
	}
	
	public ConnectorInfo getConnectorServletURL(String userName, String spaceID, String spaceDirectory, String connectionString){
		ConnectorInfo connectorInfo = null;
		try{
			ConnectorHttpClient client = new ConnectorHttpClient();
			Map<String, String> argumentsMap = getConnectorDefaultArguments(userName, spaceID, spaceDirectory);
			argumentsMap.put("ConnectionString", connectionString);			
			String result = client.callConnectorServlet(ServerContext.getConnectorControllerURI(), CONNECTORCONTROLLER_APPLICATION, LISTCONNECTORS_SERVLET, true, argumentsMap);
			OMElement connectorsEl = XmlUtils.convertToOMElement(result);
			if (connectorsEl == null)
			{
				UnrecoverableException ure = new UnrecoverableException("Unable to get ConnectorServletURL for connectionstring: " + connectionString, new Exception());
				ure.setErrorCode(BaseException.ERROR_OTHER);
				throw ure;
			}
			String connectorServerURL = null;
			String connectorApplication = null;
			if (connectorsEl != null)
			{
				Iterator<OMElement> connectorIterator = connectorsEl.getChildElements();
				OMElement connectorEl = connectorIterator.next();
				for (Iterator<OMElement> connectorDetailIterator = connectorEl.getChildElements(); connectorDetailIterator.hasNext(); )
				{
					OMElement el = connectorDetailIterator.next();
					String elName = el.getLocalName();
					if (NODE_CONNECTOR_SERVER_URL.equals(elName))
					{
						connectorServerURL = XmlUtils.getStringContent(el);
					}
					else if (NODE_CONNECTOR_APPLICATION.equals(elName))
					{
						connectorApplication = XmlUtils.getStringContent(el);
					}
				}
			}
			if (connectorServerURL != null && connectorApplication != null)
			{
				connectorInfo = new ConnectorInfo();
				connectorInfo.connectorServerURL = connectorServerURL;
				connectorInfo.connectorApplication = connectorApplication;				
			}
		}
		catch (Exception ex)
		{
			logger.error("Problem in getConnectorInfo for " + connectionString + " - " + ex.getMessage(), ex);
		}		
		return connectorInfo;
	}
	
	public OMElement saveConnectorCredentials(String userName, String spaceID, String spaceDirectory, String connectorName, String credentialsXml){
		BirstWebServiceResult response = new BirstWebServiceResult();
		try{
			if (!Repository.isUseNewConnectorFramework())
			{
				(new ConnectorData(spaceID, spaceDirectory, connectorName)).saveCredentials(credentialsXml);
			}
			else
			{
				ConnectorInfo connectorInfo = getConnectorServletURL(userName, spaceID, spaceDirectory, connectorName);
				if (connectorInfo != null)
				{
					boolean isSaved = saveConnectorCredentials(userName, spaceID, spaceDirectory, connectorInfo, credentialsXml);
					if (!isSaved)
					{
						throw new Exception("Problem saving credentials");
					}					
				}
				else
				{
					throw new Exception ("No suitable connector found for " + connectorName);
				}
			}
		}catch(Exception ex){
			response.setException(ex);
		}
		return response.toOMElement();
	}
	
	public OMElement testConnection(String userName, String spaceID, String spaceDirectory, String connectorConnectionString, String credentialsXml){
		BirstWebServiceResult response = new BirstWebServiceResult();
		try{
			ConnectorInfo connectorInfo = getConnectorServletURL(userName, spaceID, spaceDirectory, connectorConnectionString);
			if (connectorInfo != null)
			{
				boolean success = testConnection(userName, spaceID, spaceDirectory, connectorInfo, credentialsXml);
				if (!success)
				{
					throw new Exception("Problem testing connection");
				}					
			}
			else
			{
				throw new Exception ("No suitable connector found for " + connectorConnectionString);
			}
		}catch(Exception ex){
			response.setException(ex);
		}
		return response.toOMElement();
	}
	
	public OMElement saveConnection(String userName, String spaceID, String spaceDirectory, String connectorConnectionString, String connectionDetailsXML){
		BirstWebServiceResult response = new BirstWebServiceResult();
		try{
			ConnectorInfo connectorInfo = getConnectorServletURL(userName, spaceID, spaceDirectory, connectorConnectionString);
			if (connectorInfo != null)
			{
				OMElement connectorMetaDataXML = getMetaDataForConnector(userName, spaceID, spaceDirectory, connectorInfo);
				(new ConnectorData(spaceID, spaceDirectory, connectorConnectionString)).saveConnection(userName, connectorMetaDataXML, connectionDetailsXML);
				CloudConnectorConfig config = CloudConnectorConfig.getCloudConnectorConfig(spaceDirectory);
				Element cloudConnectionsElement = config.getCloudConnectionsXml();
				OMElement root = XmlUtils.convertToOMElement(XmlUtils.convertElementToString(cloudConnectionsElement));
				response.setResult(new XMLWebServiceResult(root));
			}
			else
			{
				throw new Exception ("No suitable connector found for " + connectorConnectionString);
			}						
		}catch(Exception ex){
			response.setException(ex);
		}
		return response.toOMElement();
	}
		
	private boolean startExtract(String userName, String spaceID, String spaceDirectory, ConnectorInfo connectorInfo, String credentialsXml, 
			String isIncrementalXml, String uid, String numThreads, String variables, String extractGroupsXML) throws Exception
	{
		ConnectorHttpClient client = new ConnectorHttpClient();
		Map<String, String> argumentsMap = getConnectorDefaultArguments(userName, spaceID, spaceDirectory);
		argumentsMap.put("Method", "startExtract");
		argumentsMap.put("CredentialsXML", credentialsXml);
		argumentsMap.put("isIncrementalXml", isIncrementalXml);
		argumentsMap.put("UID", uid);
		argumentsMap.put("NumThreads", numThreads);
		argumentsMap.put("Variables", variables);
		argumentsMap.put("ExtractGroupsXML", extractGroupsXML);
		String result = client.callConnectorServlet(connectorInfo.connectorServerURL, connectorInfo.connectorApplication, ICONNECTOR_SERVLET, true, argumentsMap);
		return parseConnectorBooleanResult(result);		
	}
	
	private boolean saveConnectorCredentials(String userName, String spaceID, String spaceDirectory, ConnectorInfo connectorInfo, String credentialsXml) throws Exception
	{
		ConnectorHttpClient client = new ConnectorHttpClient();
		Map<String, String> argumentsMap = getConnectorDefaultArguments(userName, spaceID, spaceDirectory);
		argumentsMap.put("Method", "saveConnectorCredentials");
		argumentsMap.put("CredentialsXML", credentialsXml);
		String result = client.callConnectorServlet(connectorInfo.connectorServerURL, connectorInfo.connectorApplication, ICONNECTOR_SERVLET, true, argumentsMap);
		return parseConnectorBooleanResult(result);		
	}
	
	private boolean testConnection(String userName, String spaceID, String spaceDirectory, ConnectorInfo connectorInfo, String credentialsXml) throws Exception
	{
		ConnectorHttpClient client = new ConnectorHttpClient();
		Map<String, String> argumentsMap = getConnectorDefaultArguments(userName, spaceID, spaceDirectory);
		argumentsMap.put("Method", "testConnection");
		argumentsMap.put("CredentialsXML", credentialsXml);
		String result = client.callConnectorServlet(connectorInfo.connectorServerURL, connectorInfo.connectorApplication, ICONNECTOR_SERVLET, true, argumentsMap);
		return parseConnectorBooleanResult(result);		
	}
	
	public static String getDefaultAPIVersion(String userName, String spaceID, String spaceDirectory, String connectorString) throws Exception
	{
		OMElement element = null;
		try
		{
			ConnectorHttpClient client = new ConnectorHttpClient();
			Map<String, String> argumentsMap = getConnectorDefaultArguments(userName, spaceID, spaceDirectory);
			argumentsMap.put("ConnectorString", connectorString);
			String result = client.callConnectorServlet(ServerContext.getConnectorControllerURI(), CONNECTORCONTROLLER_APPLICATION, GET_DEFAULT_VERSION_SERVLET, true, argumentsMap);		
			return parseConnectorStringResultElement(result);			
		}
		catch (Exception ex)
		{
			logger.error(ex.toString(), ex);
		}
		return null;		
	}
	
	private OMElement getMetaDataForConnector(String userName, String spaceID, String spaceDirectory, ConnectorInfo connectorInfo) throws Exception
	{
		ConnectorHttpClient client = new ConnectorHttpClient();
		Map<String, String> argumentsMap = getConnectorDefaultArguments(userName, spaceID, spaceDirectory);
		argumentsMap.put("Method", "getMetaData");			
		String result = client.callConnectorServlet(connectorInfo.connectorServerURL, connectorInfo.connectorApplication, ICONNECTOR_SERVLET, true, argumentsMap);
		return parseConnectorResultElement(result);		
	}
	
	public OMElement getDynamicParameters(String userName, String spaceID, String spaceDirectory, String connectorName, String credentialsXml)
	{
		BirstWebServiceResult response = new BirstWebServiceResult();
		try
		{
			ConnectorInfo connectorInfo = getConnectorServletURL(userName, spaceID, spaceDirectory, connectorName);
			if (connectorInfo != null)
			{
				ConnectorHttpClient client = new ConnectorHttpClient();
				Map<String, String> argumentsMap = getConnectorDefaultArguments(userName, spaceID, spaceDirectory);
				argumentsMap.put("Method", "getDynamicParameters");
				argumentsMap.put("CredentialsXML", credentialsXml);
				String result = client.callConnectorServlet(connectorInfo.connectorServerURL, connectorInfo.connectorApplication, ICONNECTOR_SERVLET, true, argumentsMap);
				OMElement root = parseConnectorResultElement(result);
				response.setResult(new XMLWebServiceResult(root));
				
			}
			else
			{
				throw new Exception ("No suitable connector found for " + connectorName);
			}
		}
		catch(Exception ex)
		{
			response.setException(ex);
		}
		return response.toOMElement();
	}
	
	public OMElement getMetaData(String userName, String spaceID, String spaceDirectory, String connectorName){
		BirstWebServiceResult response = new BirstWebServiceResult();
		try{
			if (!Repository.isUseNewConnectorFramework())
			{
				OMElement root = (new ConnectorData(spaceID, spaceDirectory, connectorName)).getMetaData();
				response.setResult(new XMLWebServiceResult(root));
			}
			else
			{
				ConnectorInfo connectorInfo = getConnectorServletURL(userName, spaceID, spaceDirectory, connectorName);
				if (connectorInfo != null)
				{
					OMElement root = getMetaDataForConnector(userName, spaceID, spaceDirectory, connectorInfo);
					response.setResult(new XMLWebServiceResult(root));
				}
				else
				{
					throw new Exception ("No suitable connector found for " + connectorName);
				}
			}
		}catch(Exception ex){
			response.setException(ex);
		}
		return response.toOMElement();
	}
	
	public OMElement getSelectedObjects(String spaceID, String spaceDirectory, String connectorName){
		BirstWebServiceResult response = new BirstWebServiceResult();
		try{
			OMElement root = (new ConnectorData(spaceID, spaceDirectory, connectorName)).getSelectedObjects();
			response.setResult(new XMLWebServiceResult(root));
		}catch(Exception ex){
			response.setException(ex);
		}
		return response.toOMElement();
	}
	
	public OMElement updateSelectedObjects(String userName, String spaceID, String spaceDirectory, String connectionName, String connectorType, String xmlString){
		BirstWebServiceResult response = new BirstWebServiceResult();
		try{
			OMElement root = (new ConnectorData(spaceID, spaceDirectory, null)).updateSelectedObjects(userName, connectionName, connectorType, xmlString);
			response.setResult(new XMLWebServiceResult(root));
		}catch(Exception ex){
			response.setException(ex);
		}
		return response.toOMElement();
	}
	
	public OMElement saveSelectedObjects(String userName, String spaceID, String spaceDirectory, String connectorName, String xmlString){
		BirstWebServiceResult response = new BirstWebServiceResult();
		try{
			(new ConnectorData(spaceID, spaceDirectory, connectorName)).saveSelectedObjects(userName, xmlString);
		}catch(Exception ex){
			response.setException(ex);
		}
		return response.toOMElement();
	}
	
	public OMElement getAllObjectsForCloudConnection(String userName, String spaceID, String spaceDirectory, String connectionID, String xmlData){
		BirstWebServiceResult response = new BirstWebServiceResult();
		try{
			CloudConnectorConfig cldConnectorConfig = CloudConnectorConfig.getCloudConnectorConfig(spaceDirectory);
			CloudConnection conn = cldConnectorConfig.findCloudConnectionByGuid(connectionID);
			if (conn == null)
			{
				throw new Exception("No cloud connection found for id: " + connectionID);
			}
			ConnectorInfo connectorInfo = getConnectorServletURL(userName, spaceID, spaceDirectory, conn.getConnectorAPIVersion());
			if (connectorInfo != null)
			{
				OMElement root = (new ConnectorData(spaceID, spaceDirectory, conn.getConnectorAPIVersion())).getAllObjectsForCloudConnection(userName, xmlData, connectorInfo, conn, cldConnectorConfig);
				response.setResult(new XMLWebServiceResult(root));
			}
			else
			{
				throw new Exception ("No suitable connector found for " + conn.getConnectorAPIVersion());
			}
		}catch(Exception ex){
			response.setException(ex);
		}
		return response.toOMElement();
	}
	
	public OMElement getAllObjects(String userName, String spaceID, String spaceDirectory, String connectorName, String xmlData){
		BirstWebServiceResult response = new BirstWebServiceResult();
		try{
			ConnectorInfo connectorInfo = null;
			if (Repository.isUseNewConnectorFramework())
			{
				connectorInfo = getConnectorServletURL(userName, spaceID, spaceDirectory, connectorName);
			}
			OMElement root = (new ConnectorData(spaceID, spaceDirectory, connectorName)).getAllObjects(userName, xmlData, connectorInfo);
			response.setResult(new XMLWebServiceResult(root));
		}catch(Exception ex){
			response.setException(ex);
		}
		return response.toOMElement();
	}
	
	public OMElement startExtract(String userName, String spaceID, String spaceDirectory, String connectorName, String xmlData, String variables, String extractGroupsXML){
		BirstWebServiceResult response = new BirstWebServiceResult();
		try{
			if (!Repository.isUseNewConnectorFramework())
			{
				OMElement root = (new ConnectorData(spaceID, spaceDirectory, connectorName)).startExtract(xmlData);
				response.setResult(new XMLWebServiceResult(root));
			}
			else
			{
				ConnectorInfo connectorInfo = getConnectorServletURL(userName, spaceID, spaceDirectory, connectorName);
				if (connectorInfo != null)
				{
					ConnectorConfig config = ConnectorConfig.getConnectorConfig(spaceDirectory, ConnectorData.getConnectorNameFromConnectorConnectionString(connectorName));
					if (config == null)
						throw new Exception("No config file found for connector - " + connectorName + " in space - " + spaceID);
					Properties props = new Properties();
			    	for (ConnectionProperty cp : config.getConnectionProperties())
			    	{
			    		props.put(cp.name, cp.value);
			    	}
			    	Map<String, String> isExtractionObjectTransactional = new HashMap<String, String>();
			    	ApplicationContext ac = null;
					try {
						ac = ApplicationContext.getInstance(spaceDirectory, null);
						Repository r = ac.getRepository();
						List<ExtractionObject> objects = config.getExtractionObjects();
						for (ExtractionObject obj : objects) 
				    	{
				    		String sfNamePrefix = (connectorName.equals("sfdc") ? "" : props.getProperty(BaseConnector.SOURCEFILE_NAME_PREFIX)); 
				    		String sfName = ((sfNamePrefix == null || sfNamePrefix.trim().isEmpty()) ? "" : (sfNamePrefix + "_")) +  obj.name;
				    		StagingTable st = r.findStagingTable("ST_" + sfName);
							if (st == null)
				    		{
				    			st = r.findStagingTable("ST_" + Util.replaceStr(sfName, " " , "_"));
				    		}
				    		boolean isTransactional = (st != null && st.Transactional);
				    		isExtractionObjectTransactional.put(obj.name, String.valueOf(isTransactional));
				    	}					
					}catch(Exception ex){
						logger.error("Error while running extraction", ex);
					}
					finally
					{
						try
						{
							if(ac != null){
								ac.dispose();
							}
						}catch(Exception ex){
							logger.warn("Exception while disposing of application context in extract :" + spaceDirectory);
						}
					}
					OMFactory factory = OMAbstractFactory.getOMFactory();
					OMNamespace ns = null;
					OMElement isIncrementalRootEl = factory.createOMElement(NODE_ISINCREMENTAL_ROOT, ns);
					for (String key : isExtractionObjectTransactional.keySet())
					{
						OMElement eobjEl = factory.createOMElement(NODE_EXTRACTION_OBJECT, ns);
						XmlUtils.addContent(factory, eobjEl, NODE_OBJECT_NAME, key, ns);
						XmlUtils.addContent(factory, eobjEl, NODE_IS_INCREMENTAL, isExtractionObjectTransactional.get(key), ns);
						isIncrementalRootEl.addChild(eobjEl);
					}
					String uid = UUID.randomUUID().toString();
					int numThreads = 4;
					startExtract(userName, spaceID, spaceDirectory, connectorInfo, xmlData, XmlUtils.convertToString(isIncrementalRootEl), uid, String.valueOf(numThreads), variables, extractGroupsXML);
					OMElement root = factory.createOMElement(ConnectorData.NODE_EXTRACT_ROOT, ns);
					XmlUtils.addContent(factory, root, ConnectorData.NODE_EXTRACT_ID, uid, ns);
					response.setResult(new XMLWebServiceResult(root));
				}
				else
				{
					throw new Exception ("No suitable connector found for " + connectorName);
				}
			}
		}catch(Exception ex){
			response.setException(ex);
		}
		return response.toOMElement();
		
	}
	
	public OMElement cancelExtract(String spaceID, String spaceDirectory, String connectorName){
		BirstWebServiceResult response = new BirstWebServiceResult();
		try{
			(new ConnectorData(spaceID, spaceDirectory, connectorName)).cancelExtract();
		}catch(Exception ex){
			response.setException(ex);
		}
		return response.toOMElement();
	}
	
	public OMElement getExtractionStatus(String spaceID, String spaceDirectory, String connectorName){
		BirstWebServiceResult response = new BirstWebServiceResult();
		try{
			OMElement root = (new ConnectorData(spaceID, spaceDirectory, connectorName)).getExtractStatus();
			response.setResult(new XMLWebServiceResult(root));
		}catch(Exception ex){
			response.setException(ex);
		}
		return response.toOMElement();
	}
	
	public OMElement getObjectQuery(String userName, String spaceID, String spaceDirectory, String connectorName, String xmlData)
	{
		BirstWebServiceResult response = new BirstWebServiceResult();
		try{
			if (!Repository.isUseNewConnectorFramework())
			{
				OMElement root = (new ConnectorData(spaceID, spaceDirectory, connectorName)).getObjectQuery(xmlData);
				response.setResult(new XMLWebServiceResult(root));
			}
			else
			{
				ConnectorInfo connectorInfo = getConnectorServletURL(userName, spaceID, spaceDirectory, connectorName);
				if (connectorInfo != null)
				{
					OMElement root = getObjectQuery(userName, spaceID, spaceDirectory, connectorInfo, xmlData);
					response.setResult(new XMLWebServiceResult(root));
				}
				else
				{
					throw new Exception ("No suitable connector found for " + connectorName);
				}
			}			
		}catch(Exception ex){
			response.setException(ex);
		}
		return response.toOMElement();
	}
	
	public OMElement getObject(String userName, String spaceID, String spaceDirectory, String connectorName, String xmlData, String variables)
	{
		BirstWebServiceResult response = new BirstWebServiceResult();
		try{
			ConnectorInfo connectorInfo = getConnectorServletURL(userName, spaceID, spaceDirectory, connectorName);
			if (connectorInfo != null)
			{
				OMElement root = getObject(userName, spaceID, spaceDirectory, connectorInfo, xmlData, variables);
				response.setResult(new XMLWebServiceResult(root));
			}
			else
			{
				throw new Exception ("No suitable connector found for " + connectorName);
			}		
		}catch(Exception ex){
			response.setException(ex);
		}
		return response.toOMElement();
	}
	
	private OMElement getObjectQuery(String userName, String spaceID, String spaceDirectory, ConnectorInfo connectorInfo, String xmlData) throws Exception
	{
		ConnectorHttpClient client = new ConnectorHttpClient();
		Map<String, String> argumentsMap = getConnectorDefaultArguments(userName, spaceID, spaceDirectory);
		argumentsMap.put("Method", "getObjectQuery");
		argumentsMap.put("xmlData", xmlData);
		String result = client.callConnectorServlet(connectorInfo.connectorServerURL, connectorInfo.connectorApplication, ICONNECTOR_SERVLET, true, argumentsMap);
		return parseConnectorResultElement(result);			
	}
	
	private OMElement getObject(String userName, String spaceID, String spaceDirectory, ConnectorInfo connectorInfo, String xmlData, String variables) throws Exception
	{
		ConnectorHttpClient client = new ConnectorHttpClient();
		Map<String, String> argumentsMap = getConnectorDefaultArguments(userName, spaceID, spaceDirectory);
		argumentsMap.put("Method", "getObject");
		argumentsMap.put("xmlData", xmlData);
		argumentsMap.put("Variables", variables);
		String result = client.callConnectorServlet(connectorInfo.connectorServerURL, connectorInfo.connectorApplication, ICONNECTOR_SERVLET, true, argumentsMap);
		return parseConnectorResultElement(result);			
	}
	
	public OMElement validateObjectQuery(String userName, String spaceID, String spaceDirectory, String connectorName, String xmlData, String variables)
	{
		BirstWebServiceResult response = new BirstWebServiceResult();
		try{
			if (!Repository.isUseNewConnectorFramework())
			{
				OMElement root = (new ConnectorData(spaceID, spaceDirectory, connectorName)).validateObjectQuery(xmlData);
				response.setResult(new XMLWebServiceResult(root));
			}
			else
			{
				ConnectorInfo connectorInfo = getConnectorServletURL(userName, spaceID, spaceDirectory, connectorName);
				if (connectorInfo != null)
				{					
					OMElement root = validateObjectQuery(userName, spaceID, spaceDirectory, connectorInfo, xmlData, variables);
					response.setResult(new XMLWebServiceResult(root));
				}
				else
				{
					throw new Exception ("No suitable connector found for " + connectorName);
				}
			}
		}catch(Exception ex){
			response.setException(ex);
		}
		return response.toOMElement();
	}
	
	public OMElement validateObject(String userName, String spaceID, String spaceDirectory, String connectorName, String xmlData, String variables)
	{
		BirstWebServiceResult response = new BirstWebServiceResult();
		try{
			ConnectorInfo connectorInfo = getConnectorServletURL(userName, spaceID, spaceDirectory, connectorName);
			if (connectorInfo != null)
			{					
				OMElement root = validateObject(userName, spaceID, spaceDirectory, connectorInfo, xmlData, variables);
				response.setResult(new XMLWebServiceResult(root));
			}
			else
			{
				throw new Exception ("No suitable connector found for " + connectorName);
			}
		}catch(Exception ex){
			response.setException(ex);
		}
		return response.toOMElement();
	}
	
	private OMElement validateObjectQuery(String userName, String spaceID, String spaceDirectory, ConnectorInfo connectorInfo, String xmlData, String variables) throws Exception
	{
		ConnectorHttpClient client = new ConnectorHttpClient();
		Map<String, String> argumentsMap = getConnectorDefaultArguments(userName, spaceID, spaceDirectory);
		argumentsMap.put("Method", "validateObjectQuery");
		argumentsMap.put("xmlData", xmlData);
		argumentsMap.put("Variables", variables);
		String result = client.callConnectorServlet(connectorInfo.connectorServerURL, connectorInfo.connectorApplication, ICONNECTOR_SERVLET, true, argumentsMap);
		return parseConnectorResultElement(result);				
	}
	
	private OMElement validateObject(String userName, String spaceID, String spaceDirectory, ConnectorInfo connectorInfo, String xmlData, String variables) throws Exception
	{
		ConnectorHttpClient client = new ConnectorHttpClient();
		Map<String, String> argumentsMap = getConnectorDefaultArguments(userName, spaceID, spaceDirectory);
		argumentsMap.put("Method", "validateObject");
		argumentsMap.put("xmlData", xmlData);
		argumentsMap.put("Variables", variables);
		String result = client.callConnectorServlet(connectorInfo.connectorServerURL, connectorInfo.connectorApplication, ICONNECTOR_SERVLET, true, argumentsMap);
		return parseConnectorResultElement(result);				
	}
	
	public static OMElement parseConnectorResultElement(String result) throws Exception
	{
		OMElement resultEl = XmlUtils.convertToOMElement(result);
		if (resultEl == null)
		{
			UnrecoverableException ure = new UnrecoverableException("Unable to parse result received from Connector", new Exception());
			ure.setErrorCode(BaseException.ERROR_OTHER);
			throw ure;
		}
		boolean isSuccess = false;
		OMElement root = null;
		for (Iterator<OMElement> resultIterator = resultEl.getChildElements(); resultIterator.hasNext(); )
		{
			OMElement el = resultIterator.next();
			if ("Success".equals(el.getLocalName()))
			{
				isSuccess = Boolean.valueOf(XmlUtils.getBooleanContent(el));
				break;
			}
		}
		if (isSuccess)
		{
			for (Iterator<OMElement> resultIterator = resultEl.getChildElements(); resultIterator.hasNext(); )
			{
				OMElement el = resultIterator.next();
				if ("Result".equals(el.getLocalName()))
				{
					root = (OMElement) el.getChildElements().next();					
				}
			}
		}
		else
		{
			String message = null;
			int errorCode = BaseException.ERROR_OTHER;
			for (Iterator<OMElement> resultIterator = resultEl.getChildElements(); resultIterator.hasNext(); )
			{
				OMElement el = resultIterator.next();
				if ("Reason".equals(el.getLocalName()))
				{
					message = el.getText();
				}
				if ("ErrorCode".equals(el.getLocalName()))
				{
					errorCode = Integer.parseInt(el.getText());
				}
			}
			UnrecoverableException ure = new UnrecoverableException(message, new Exception());
			ure.setErrorCode(errorCode);
			throw ure;
		}
		return root;
	}
	
	public static String parseConnectorStringResultElement(String result) throws Exception
	{
		OMElement resultEl = XmlUtils.convertToOMElement(result);
		if (resultEl == null)
		{
			UnrecoverableException ure = new UnrecoverableException("Unable to parse result received from Controller", new Exception());
			ure.setErrorCode(BaseException.ERROR_OTHER);
			throw ure;
		}
		boolean isSuccess = false;
		String root = null;
		for (Iterator<OMElement> resultIterator = resultEl.getChildElements(); resultIterator.hasNext(); )
		{
			OMElement el = resultIterator.next();
			if ("Success".equals(el.getLocalName()))
			{
				isSuccess = Boolean.valueOf(XmlUtils.getBooleanContent(el));
				break;
			}
		}
		if (isSuccess)
		{
			for (Iterator<OMElement> resultIterator = resultEl.getChildElements(); resultIterator.hasNext(); )
			{
				OMElement el = resultIterator.next();
				if ("Result".equals(el.getLocalName()))
				{
					root = el.getText();					
				}
			}
		}
		else
		{
			String message = null;
			int errorCode = BaseException.ERROR_OTHER;
			for (Iterator<OMElement> resultIterator = resultEl.getChildElements(); resultIterator.hasNext(); )
			{
				OMElement el = resultIterator.next();
				if ("Reason".equals(el.getLocalName()))
				{
					message = el.getText();
				}
				if ("ErrorCode".equals(el.getLocalName()))
				{
					errorCode = Integer.parseInt(el.getText());
				}
			}
			UnrecoverableException ure = new UnrecoverableException(message, new Exception());
			ure.setErrorCode(errorCode);
			throw ure;
		}
		return root;
	}
	
	public static boolean parseConnectorBooleanResult(String result) throws Exception
	{
		OMElement resultEl = XmlUtils.convertToOMElement(result);
		boolean isSuccess = false;
		OMElement root = null;
		for (Iterator<OMElement> resultIterator = resultEl.getChildElements(); resultIterator.hasNext(); )
		{
			OMElement el = resultIterator.next();
			if ("Success".equals(el.getLocalName()))
			{
				isSuccess = Boolean.valueOf(XmlUtils.getBooleanContent(el));
				break;
			}
		}
		boolean resultValue = false;
		if (isSuccess)
		{
			for (Iterator<OMElement> resultIterator = resultEl.getChildElements(); resultIterator.hasNext(); )
			{
				OMElement el = resultIterator.next();
				if ("Result".equals(el.getLocalName()))
				{
					resultValue = Boolean.valueOf(XmlUtils.getBooleanContent(el));								
				}
			}
		}
		else
		{
			String message = null;
			int errorCode = BaseException.ERROR_OTHER;
			for (Iterator<OMElement> resultIterator = resultEl.getChildElements(); resultIterator.hasNext(); )
			{
				OMElement el = resultIterator.next();
				if ("Reason".equals(el.getLocalName()))
				{
					message = el.getText();
				}
				if ("ErrorCode".equals(el.getLocalName()))
				{
					errorCode = Integer.parseInt(el.getText());
				}
			}
			UnrecoverableException ure = new UnrecoverableException(message, new Exception());
			ure.setErrorCode(errorCode);
			throw ure;
		}		
		return resultValue;
	}
	
	public OMElement getObjectDetails(String spaceID, String spaceDirectory, String connectorName, String xmlData)
	{
		BirstWebServiceResult response = new BirstWebServiceResult();
		try{
			OMElement root = (new ConnectorData(spaceID, spaceDirectory, connectorName)).getQueryObjectDetails(xmlData);
			response.setResult(new XMLWebServiceResult(root));
		}catch(Exception ex){
			response.setException(ex);
		}
		return response.toOMElement();
	}
	
	public OMElement getSavedObjectDetails(String spaceID, String spaceDirectory, String userName, String connectorName, String xmlData,String variables)
	{
		BirstWebServiceResult response = new BirstWebServiceResult();
		try{
			OMElement root = (new ConnectorData(spaceID, spaceDirectory, connectorName)).getSavedObjectDetails(xmlData);
			OMElement root1;
			ConnectorInfo connectorInfo = getConnectorServletURL(userName, spaceID, spaceDirectory, connectorName);
			if (connectorInfo != null)
			{
				root1 = getDynamicParamsforObject(userName, spaceID, spaceDirectory, connectorInfo, xmlData, variables,root.toString());
				response.setResult(new XMLWebServiceResult(root1));
			}
			else
			{
				response.setResult(new XMLWebServiceResult(root));
			}
		}catch(Exception ex){
			response.setException(ex);
		}
		return response.toOMElement();
	}
	
	public OMElement saveQueryObject(String spaceID, String spaceDirectory, String userName, String connectorName, String xmlData)
	{
		BirstWebServiceResult response = new BirstWebServiceResult();
		try{
			(new ConnectorData(spaceID, spaceDirectory, connectorName)).saveQueryObject(userName, xmlData);
		}catch(Exception ex){
			response.setException(ex);
		}
		return response.toOMElement();
	}
	
	public OMElement saveObject(String userName, String spaceID, String spaceDirectory, String connectorName, String xmlData) {
		BirstWebServiceResult response = new BirstWebServiceResult();
		try{
			(new ConnectorData(spaceID, spaceDirectory, connectorName)).saveObject(userName, xmlData);
		}catch(Exception ex){
			response.setException(ex);
		}
		return response.toOMElement();
	}
	private OMElement getDynamicParamsforObject(String userName, String spaceID, String spaceDirectory, ConnectorInfo connectorInfo, String xmlData, String variables,String objectDefinition) throws Exception
	{
		TrustedService ts=new TrustedService();
		String variablesList = ConnectorUtil.getVariables(objectDefinition);
		variablesList = ts.formatVariables(ts.getVariables(spaceDirectory, userName, spaceID, variablesList));
		ConnectorHttpClient client = new ConnectorHttpClient();
		Map<String, String> argumentsMap = getConnectorDefaultArguments(userName, spaceID, spaceDirectory);
		argumentsMap.put("Method", "getDynamicParamsforObject");
		argumentsMap.put("xmlData", xmlData);
		argumentsMap.put("Variables", variablesList);
		argumentsMap.put("ObjectDefinition", objectDefinition);
		String result = client.callConnectorServlet(connectorInfo.connectorServerURL, connectorInfo.connectorApplication, ICONNECTOR_SERVLET, true, argumentsMap);
		return parseConnectorResultElement(result);			
	}
}
