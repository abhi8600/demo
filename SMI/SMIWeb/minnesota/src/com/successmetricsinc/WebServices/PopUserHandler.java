/**
 * $Id: PopUserHandler.java,v 1.3 2010-10-26 18:01:13 ricks Exp $
 *
 * Copyright (C) 2009 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.successmetricsinc.WebServices;

import org.apache.axis2.AxisFault;
import org.apache.axis2.context.MessageContext;
import org.apache.axis2.handlers.AbstractHandler;

import com.successmetricsinc.util.logging.AuditLog;

public class PopUserHandler extends AbstractHandler
{
	public PopUserHandler()
	{
	}

	public InvocationResponse invoke(MessageContext msgContext) throws AxisFault
	{
		// clear out the current MDC settings
		AuditLog.reset(true);
		return InvocationResponse.CONTINUE;
	}
}