/**
 * 
 */
package com.successmetricsinc.WebServices.adhoc;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

import com.successmetricsinc.WebServices.StringWebServiceResult;
import com.successmetricsinc.WebServices.XMLWebServiceResult;
import com.successmetricsinc.adhoc.AdhocReport;

/**
 * @author agarrison
 *
 */
public class CombinedReportAndRenderedDataResult extends XMLWebServiceResult {


	public CombinedReportAndRenderedDataResult(AdhocReport report,
			String reportRenderData) {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace ns = fac.createOMNamespace("", "");
		OMElement el = fac.createOMElement("data", ns);
		
		OMElement rep = fac.createOMElement("report", ns);
		report.addContent(rep, fac, ns);
		el.addChild(rep);
		
		OMElement rend = fac.createOMElement("renderer", ns);
		StringWebServiceResult sw = new StringWebServiceResult(reportRenderData);
		sw.addContent(rend, fac, ns);
		
		el.addChild(rend);
		
		this.result = el;
	}

}
