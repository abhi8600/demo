/**
 * 
 */
package com.successmetricsinc.WebServices.adhoc;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeSet;
import java.util.UUID;

import javax.jws.WebService;
import javax.servlet.http.HttpSession;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRTooManyPagesException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMException;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.OMNode;
import org.apache.axiom.om.OMText;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.successmetricsinc.Column;
import com.successmetricsinc.CustomDrill;
import com.successmetricsinc.Prompt;
import com.successmetricsinc.Repository;
import com.successmetricsinc.ServerContext;
import com.successmetricsinc.Session;
import com.successmetricsinc.SessionHelper;
import com.successmetricsinc.Variable;
import com.successmetricsinc.Session.Renderer;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.WebServices.BirstWebService;
import com.successmetricsinc.WebServices.BirstWebServiceResult;
import com.successmetricsinc.WebServices.StringWebServiceResult;
import com.successmetricsinc.WebServices.XMLWebServiceResult;
import com.successmetricsinc.WebServices.SubjectArea.SubjectAreaWS;
import com.successmetricsinc.adhoc.AdhocChart;
import com.successmetricsinc.adhoc.AdhocColumn;
import com.successmetricsinc.adhoc.AdhocReport;
import com.successmetricsinc.adhoc.AdhocReportHelper;
import com.successmetricsinc.adhoc.AsynchReportGenerationException;
import com.successmetricsinc.adhoc.ColumnSelector;
import com.successmetricsinc.adhoc.Drill;
import com.successmetricsinc.adhoc.DrillColumn;
import com.successmetricsinc.adhoc.DrillColumnList;
import com.successmetricsinc.adhoc.DrillMap;
import com.successmetricsinc.adhoc.DrillMapList;
import com.successmetricsinc.adhoc.DrillRow;
import com.successmetricsinc.adhoc.AdhocColumn.ColumnType;
import com.successmetricsinc.catalog.CatalogManager;
import com.successmetricsinc.catalog.CatalogPermission;
import com.successmetricsinc.chart.Chart;
import com.successmetricsinc.chart.CustomGeoMap;
import com.successmetricsinc.engine.operation.Operation;
import com.successmetricsinc.jasperReports.JRFlexExporter;
import com.successmetricsinc.jasperReports.JRFlexExporterParameter;
import com.successmetricsinc.query.AbstractQueryString;
import com.successmetricsinc.query.BadColumnNameException;
import com.successmetricsinc.query.DisplayExpression;
import com.successmetricsinc.query.Filter;
import com.successmetricsinc.query.JasperDataSourceProvider;
import com.successmetricsinc.query.LogicalQueryString;
import com.successmetricsinc.query.Query;
import com.successmetricsinc.query.QueryCancelledException;
import com.successmetricsinc.query.QueryFilter;
import com.successmetricsinc.query.QueryResultSet;
import com.successmetricsinc.query.QueryString;
import com.successmetricsinc.query.ResultSetTooBigException;
import com.successmetricsinc.query.SyntaxErrorException;
import com.successmetricsinc.queuing.AsynchReportGeneration.AsynchReportProducer;
import com.successmetricsinc.rest.IndexingServices;
import com.successmetricsinc.transformation.object.SavedExpressions;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.DateUtil;
import com.successmetricsinc.util.SelectItem;
import com.successmetricsinc.util.SessionUtils;
import com.successmetricsinc.util.SimpleMRUCache;
import com.successmetricsinc.util.TrellisTooLargeException;
import com.successmetricsinc.util.UserNotLoggedInException;
import com.successmetricsinc.util.UserOperationNotAuthorizedException;
import com.successmetricsinc.util.XMLStreamIterator;
import com.successmetricsinc.util.XmlUtils;
import com.successmetricsinc.util.logging.AuditLog;

/**
 * This is used to handle all the web service calls for adhoc.
 * 
 * @author agarrison
 * 
 */
@WebService(serviceName = "Adhoc")
public class AdhocWS extends BirstWebService {

	private final static String UC_REPORT_EXTENSION = ".ADHOCREPORT";
	private final static String REPORT_EXTENSION = ".AdhocReport";

	static Logger logger = Logger.getLogger(AdhocWS.class);
	
	private boolean isParseInJson = false;
	
	public void setJsonParsing(boolean isParseInJson){
		this.isParseInJson = isParseInJson;
	}
	
	/**
	 * Gets the XML data that can be used to render the report in the report SWF
	 * file.
	 * 
	 * @param pageNumber
	 *            the page to render
	 * @return an XML string containing a BirstWebServiceResult object. The
	 *         result field of that object contains the data if successful.
	 */
	public OMElement getRenderedReport(String xmlData, int pageNumber) {
		long start = System.currentTimeMillis();
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			AdhocReport report = AdhocReport.convert(xmlData, null, null);
			if (report.isInVisualization()) {
				AdhocReportHelper.setupVisualizationChart(report);
				report.setPageHeight(Integer.MAX_VALUE);
				report.setPageWidth(Integer.MAX_VALUE);
			}
			try {
				String renderedData = getReportRenderData(report,
						SimpleMRUCache.hash(xmlData.getBytes("UTF-8")),
						pageNumber);
				result.setResult(new StringWebServiceResult(renderedData));
			} catch (TrellisTooLargeException et) {
				result.setException(et);

				AdhocChart chart = report.getDefaultChart();
				if (chart != null) {
					report.removeEntity(chart);
				}
				report.setIncludeTable(true);

				String renderedData = getReportRenderData(report,
						SimpleMRUCache.hash(xmlData.getBytes("UTF-8")),
						pageNumber);
				result.setResult(new StringWebServiceResult(renderedData));
			}

			String id = null;
			HttpSession session = SessionHelper.getSession();
			if (session != null)
				id = session.getId();
			AuditLog.log("designer", report.getFileName(), id, System
					.currentTimeMillis()
					- start);
		} catch (AsynchReportGenerationException arge) {
			try {
				AdhocReportHelper.handleAsynchReportGenerationException(arge, getUserBean(), result);
			} catch (Exception e) {
				result.setException(e);
			} 
		} catch (Exception e) {
			result.setException(e);
		}
		OMElement ret = result.toOMElement();
		logger
				.debug("+++ getRenderedReport (flex client calls this - total time in server): "
						+ (System.currentTimeMillis() - start));
		return ret;
	}

	public OMElement isReportRenderJobComplete(String[] asynchronousJobIds) {
		long start = System.currentTimeMillis();
		BirstWebServiceResult result = new BirstWebServiceResult();

		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}

			if (asynchronousJobIds == null || asynchronousJobIds.length == 0) {
				throw new SyntaxErrorException("Invalid asynchronous job id");
			}

			SMIWebUserBean ub = getUserBean();
			OMFactory fac = OMAbstractFactory.getOMFactory();
			OMNamespace ns = fac.createOMNamespace("", "");
			OMElement el = fac.createOMElement("Finished", ns);
			for (int i = 0; i < asynchronousJobIds.length; i++) {
				OMElement job = fac.createOMElement("AsyncJob", ns);
				XmlUtils
						.addAttribute(fac, job, "Id", asynchronousJobIds[i], ns);
				String path = ub
						.getResultsPathFromQueueId(asynchronousJobIds[i]);
				boolean isComplete = AsynchReportProducer.isReportRendered(ub, asynchronousJobIds[i]);
				if (path == null) {
					XmlUtils.addAttribute(fac, job, "InvalidId", Boolean.TRUE
							.toString(), ns);
				} else if (isComplete){
					try {
						JasperPrint jp = AdhocReportHelper.getAsynchronouslyGeneratedJasperPrintObject(ub, asynchronousJobIds[i]);
						XmlUtils.addAttribute(fac, job, "IsComplete", Boolean
								.toString(jp != null), ns);
					}
					catch (Exception e) {
						XmlUtils.addAttribute(fac, job, "InvalidId", Boolean.TRUE.toString(), ns);
					}
				}
				else {
					XmlUtils.addAttribute(fac, job, "IsComplete", Boolean.FALSE.toString(), ns);
					
				}
				el.addChild(job);
			}

			result.setResult(new XMLWebServiceResult(el));
		}

		catch (Exception e) {
			result.setException(e);
		}
		OMElement ret = result.toOMElement();
		return ret;
	}

	private String getReportRenderData(AdhocReport report, long hashCode,
			int page) throws NullPointerException, BaseException,
			CloneNotSupportedException, Exception {
		/*
		 * if (report.isPivot()) { return getPivotData(report); }
		 */
		SMIWebUserBean userbean = SMIWebUserBean.getUserBean();
		// clear this out here bug 12123
		userbean.clearJasperPrintObject();
		Session session = userbean.getRepSession();
		session.setCancelled(false);
		Session.clear();
		Session.setParams(new ArrayList<Filter>()); // wipe out the params for
													// designer
		Session.setUseOlapIndentation(report.isOlapUseIndentation());
		String ret = null;
		JasperPrint jprint = null;
		try {
			report.organizeColumns();
			Map<Long, JasperPrint> printCache = session.getDesignerPageCache();
			if (userbean.isUserBeanAppContextModified()) {
				// make sure that if the application context is reloaded in the
				// background
				// the jasperprint print cache is cleared
				printCache.clear();
				userbean.syncUpUserBeanAppContextTime();
			}
			jprint = printCache.get(hashCode);
			if (jprint != null) {
				logger
						.debug("+++ found JasperPrint object in cache, not refilling");
			} else {
				jprint = AdhocReportHelper.getDashboardJasperPrint(report,
						null, null, userbean.getRepository(), false, userbean,
						false, false, false);
				printCache.put(hashCode, jprint);
			}
			if (session.isCancelled()) {
				throw new QueryCancelledException("Cancelled");
			}
			if (jprint != null) {
				ret = getRenderedReport(report, jprint, Integer.valueOf(page),
						new HashMap<String, Prompt>());
			}
		} catch (QueryCancelledException qce) {
			throw qce;
		} catch (AsynchReportGenerationException arge) {
			throw arge;
		} catch (Exception e) {
			StringBuilder builder = new StringBuilder();
			builder
					.append("<report width=\"998\" height=\"1200\" color=\"FFFFFF\">");
			builder
					.append("<text x=\"16\" y=\"16\" width=\"980\" height=\"100\" color=\"#033D3D\" backcolor=\"#FFFFFF\" bold=\"false\" italic=\"false\" face=\"Arial\" size=\"12\" value=\"");
			if (e instanceof JRTooManyPagesException
					|| e instanceof ResultSetTooBigException) {
				builder.append(XmlUtils.encode(e.getMessage()));
			} else {
				if (e instanceof BadColumnNameException) {
					builder.append("This report refers to a bad column &lt;");
				} else if (e instanceof TrellisTooLargeException) {
					throw e;
				} else {
					builder.append("This report generates an error &lt;");
					logger.debug(e, e);
				}
				builder.append(XmlUtils.encode(e.getMessage()));
				builder.append("&gt;.  Please correct the report.");
			}
			builder.append("\" alignment=\"left\" />");
			builder.append("</report>");
			ret = builder.toString();
		}
		return addReportRenderData(ret, jprint == null ? 0 : jprint.getPages()
				.size(), page);
	}

	private String addReportRenderData(String report, int numberOfPages,
			int page) {
		StringBuilder builder = new StringBuilder();
		builder.append("<reportRenderer>");
		builder.append("<data>");
		builder.append(report == null ? "" : report);
		builder.append("</data>");
		builder.append("<numberOfPages>");
		builder.append(numberOfPages);
		builder.append("</numberOfPages>");
		builder.append("<page>");
		builder.append(page);
		builder.append("</page>");
		builder.append("</reportRenderer>");
		return builder.toString();
	}

	/**
	 * Cancels the adhoc rendering, if possible
	 * 
	 * @return an XML string containing a BirstWebServiceResult object.
	 */
	public OMElement cancelReportRendering() {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			Session session = SMIWebUserBean.getUserBean().getRepSession();
			session.resetState();
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	/**
	 * Gets the XML data that represents an adhoc report. This data can be used
	 * in all of the dialogs for adhoc.
	 * 
	 * @return an XML string containing a BirstWebServiceResult object. The
	 *         result field of that object contains the XML representation of an
	 *         adhoc report.
	 * @deprecated
	 */
	// public OMElement getAdhocReport(OMElement element) {
	public OMElement getAdhocReport() {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			result.setResult(getReport());
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	public OMElement openReport(String filename) {
		return AdhocReportHelper.openReport(filename, isLoggedIn(),
				getUserBean());
	}

	public OMElement saveReport(String path, String filename, String xmlData) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}

			SMIWebUserBean ub = getUserBean();
			if (!ub.isSuperUserOwnerOrAdmin() && !ub.hasAdhocPermission()) {
				throw new UserOperationNotAuthorizedException(
						"saveReport : User not authorized to execute the operation");
			}
			CatalogPermission perm = ub.getCatalogManager().getPermission(path);
			if (perm == null
					|| !perm.canModify(ub.getUserGroups(), ub
							.isSuperUserOwnerOrAdmin())) {
				result.setErrorCode(BaseException.ERROR_PERMISSION);
				result
						.setErrorMessage("No permission to save file to this directory - "
								+ path);
			} else {

				AdhocReport report = AdhocReport.convert(xmlData, null, null);

				// update report filenames and the cached report with that name
				String name = filename;
				int index = name.lastIndexOf(UC_REPORT_EXTENSION);
				if (index > 0) {
					name = name.substring(0, index);
				}

				report.setName(name);
				report.setPath(path);
				BirstWebService.setReport(report);

				ub.getRepSession().setCancelled(false);
				CatalogManager cm = ub.getCatalogManager();
				String root = cm.getCatalogRootPath();
				String relativePath = path;
				path = root + "/" + path;

				if (!filename.toUpperCase().endsWith(UC_REPORT_EXTENSION)) {
					String namew = filename.concat(REPORT_EXTENSION);
					filename = namew;
				}

				File file = new File(path + "/" + filename);
				// report.compileAndSaveReport(file);
				report.save(file);
				
				upsertReportIndex(relativePath + "/" + filename);
				
				Session session = ub.getRepSession();
				if (session != null)
					session.clearJasperReport(file.getCanonicalPath());
				result.setResult(report);
			}
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	public OMElement saveFile(String path, String filename, String data) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}

			SMIWebUserBean ub = getUserBean();
			/*
			 * if(!ub.isSuperUserOwnerOrAdmin() && !ub.hasAdhocPermission()) {
			 * throw newUserOperationNotAuthorizedException(
			 * "saveReport : User not authorized to execute the operation"); }
			 */
			CatalogPermission perm = ub.getCatalogManager().getPermission(path);
			if (perm == null
					|| !perm.canModify(ub.getUserGroups(), ub
							.isSuperUserOwnerOrAdmin())) {
				result.setErrorCode(BaseException.ERROR_PERMISSION);
				result.setErrorMessage("No permission to save file to this directory - " + path);
			}
			else {
				String relativePath = path + '/' + filename;
				
				ub.getRepSession().setCancelled(false);
				CatalogManager cm = ub.getCatalogManager();
				String root = cm.getCatalogRootPath();
				path = root + "/" + path;

				File file = new File(path + "/" + filename);

				try {
					if (!file.exists()) {
						file.createNewFile();
					}
					FileUtils.writeStringToFile(file, data, "UTF-8");
				} catch (IOException e) {
					logger.error("Error saving: (with attempted file name: "
							+ file.toString() + ")", e);
					throw (e);
				}
				
				upsertReportIndex(relativePath);
				
				result.setResult(new StringWebServiceResult(data));
			}
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	private void upsertReportIndex(String report){
		try{
			IndexingServices idxSvc = 
				new IndexingServices(SessionHelper.getHttpServletRequest(), SessionHelper.getSession().getServletContext());
			idxSvc.upsertReport(report);
		}catch(Exception e){
			logger.error(e);
		}
	}
	
	public  OMElement readFile(String path, String filename) {

		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}

			SMIWebUserBean ub = getUserBean();
			/*
			 * if(!ub.isSuperUserOwnerOrAdmin() && !ub.hasAdhocPermission()) {
			 * throw newUserOperationNotAuthorizedException(
			 * "saveReport : User not authorized to execute the operation"); }
			 */
			CatalogPermission perm = ub.getCatalogManager().getPermission(path);
			if (perm == null
					|| !perm.canModify(ub.getUserGroups(), ub
							.isSuperUserOwnerOrAdmin())) {
				result.setErrorCode(BaseException.ERROR_PERMISSION);
				result
						.setErrorMessage("No permission to save file to this directory - "
								+ path);
			} else {

				ub.getRepSession().setCancelled(false);
				CatalogManager cm = ub.getCatalogManager();
				String root = cm.getCatalogRootPath();
				path = root + "/" + path;

				File file = new File(path + "/" + filename);
				String data = null;

				try {
					if (!file.exists()) {
						file.createNewFile();
					}
					data = FileUtils.readFileToString(file, "UTF-8");
				} catch (IOException e) {
					logger.error(
							"Error reading file: (with attempted file name: "
									+ file.toString() + ")", e);
					throw (e);
				}

				result.setResult(new StringWebServiceResult(data));
			}
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	public OMElement drillDown(String xmlData, String drillCols, String filters) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			getUserBean().getRepSession().setCancelled(false);
			AdhocReport report = AdhocReport.convert(xmlData, null, null);
			List<Drill> drills = AdhocReportHelper.parseDrillCols(drillCols,
					report);
			AdhocReportHelper.drillDown(report, drills, filters);
			CombinedReportAndRenderedDataResult r = new CombinedReportAndRenderedDataResult(
					report, getReportRenderData(report, SimpleMRUCache
							.hash(report.toString().getBytes("UTF-8")), 0));
			result.setResult(r);
		} catch (AsynchReportGenerationException arge) {
			try {
				AdhocReportHelper.handleAsynchReportGenerationException(arge, getUserBean(), result);
			} catch (Exception e) {
				result.setException(e);
			}	
			
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	public OMElement olapDrillDown(String xmlData, String value) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			getUserBean().getRepSession().setCancelled(false);
			AdhocReport report = AdhocReport.convert(xmlData, null, null);
			String values[] = value.split("&");
			for (int i = 0; i < values.length; i++) {
				String parts[] = values[i].split("=");
				if (parts.length > 2) {
					for (AdhocColumn ac : report.getColumns()) {
						String name = URLDecoder.decode(parts[0], "UTF-8");
						if (ac.getDefaultPromptName().equals(name)) {
							String val = URLDecoder.decode(parts[1], "UTF-8");
							val = val.trim();
							String techName = URLDecoder.decode(parts[2],
									"UTF-8");
							ac.addAdditionalOlapColumn(val, techName.trim(),
									AdhocColumn.CHILDREN);
							break;
						}
					}
				}
			}
			CombinedReportAndRenderedDataResult r = new CombinedReportAndRenderedDataResult(
					report, getReportRenderData(report, SimpleMRUCache
							.hash(report.toString().getBytes("UTF-8")), 0));
			result.setResult(r);
		} catch (AsynchReportGenerationException arge) {
			try {
				AdhocReportHelper.handleAsynchReportGenerationException(arge, getUserBean(), result);
			} catch (Exception e) {
				result.setException(e);
			}
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	public OMElement getDrillDownPrompts(String promptData) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		Prompt p = new Prompt();
		XMLStreamReader parser;
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			XMLInputFactory inputFactory = XmlUtils.getSecureXMLInputFactory();
			parser = inputFactory.createXMLStreamReader(new StringReader(
					promptData));

			StAXOMBuilder builder = new StAXOMBuilder(parser);
			OMElement doc = null;
			doc = builder.getDocumentElement();
			doc.build();
			try {
				doc.detach();
			} catch (OMException ome) {
			}
			p.parseElement(doc);

			String colName = p.getColumnName();
			String dim = null;
			if (colName != null && colName.trim().length() > 0) {
				int index = colName.indexOf('.');
				if (index > 0) {
					dim = colName.substring(0, index);
					colName = colName.substring(index + 1);
				}
			}

			OMFactory fac = OMAbstractFactory.getOMFactory();
			OMNamespace ns = fac.createOMNamespace("", "");
			OMElement el = fac.createOMElement("prompts", ns);
			SMIWebUserBean ub = getUserBean();
			Repository r = ub.getRepository();
			addAllChildrenPrompts(dim, colName, r, dim != null, fac, ns, el, p);
			result.setResult(new XMLWebServiceResult(el));
		} catch (Exception e) {
			logger.error(e, e);
			result.setException(e);
		}

		return result.toOMElement();
	}

	private void addAllChildrenPrompts(String dim, String colName,
			Repository r, boolean isDimension, OMFactory fac, OMNamespace ns,
			OMElement el, Prompt parent) {
		List<DrillColumnList> drillCols = AdhocColumn.getAllDrillColumns(dim,
				colName, r, isDimension);
		if (drillCols != null && drillCols.size() > 0) {
			// use the first item in the list
			DrillColumnList dcl = drillCols.get(0);
			for (DrillColumn dc : dcl.getDrillColumns()) {
				Column c = new Column(dc.getDimension(), dc.getColumn(), (dc
						.getDimension() == null || dc.getDimension().trim()
						.length() == 0) ? dc.getColumn() : dc.getDimension()
						+ "." + dc.getColumn());
				Prompt child = new Prompt(c);
				child.addParent(parent.getParameterName());
				child.addContent(el, fac, ns);
				addAllChildrenPrompts(c.getDimension(), c.getName(), r, c
						.getDimension() != null, fac, ns, el, child);
			}
		}

	}

	/**
	 * Sets the XML data that represents an adhoc report.
	 * 
	 * @return an XML string containing a BirstWebServiceResult object.
	 */
	// public OMElement setAdhocReport(OMElement element) {
	public OMElement setAdhocReport(String xmlData) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			AdhocReport report = AdhocReport.convert(xmlData, null, null);
			if (report.isInVisualization()) {
				AdhocReportHelper.setupVisualizationChart(report);
			}

			result.setResult(report);

			setReport(report);
			return result.toOMElement();
		} catch (Exception e) {
			result.setException(e);
		}
		// Set the previously valid adhoc report xml
		result.setResult(getReport());

		return result.toOMElement();

	}

	/**
	 * Empties the adhoc report on the server.
	 * 
	 * @return an XML string containing a BirstWebServiceResult object,
	 *         including the report's XML structure.
	 */
	// public OMElement clearReport(OMElement element) {
	public OMElement clearReport() {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			result.setResult(new AdhocReport());
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	/**
	 * Adds a new column to the report on the server
	 * 
	 * @param columnName
	 *            the column to add. The value comes from the subject area.
	 * @return an XML string containing a BirstWebServiceResult object,
	 *         including the report's XML structure.
	 */
	public OMElement addColumnToReport(String xmlData, String column,
			String olapDimension, String olapHierarchy, String memberName,
			String label, boolean isPivot, String vizPanel) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			getUserBean().getRepSession().setCancelled(false);
			AdhocReport report = AdhocReport.convert(xmlData, null, null);
			Column node = AdhocReportHelper.getColumn(column, olapDimension,
					olapHierarchy);
			node.setDisplayName(label);
			if (olapDimension != null && !olapDimension.isEmpty())
				node.setMemberName(memberName);
			AdhocReportHelper.addColumnToAdhocReport(report, node, isPivot,
					false, vizPanel);
			result.setResult(report);
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	public OMElement getColumnDefinition(String column) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			getUserBean().getRepSession().setCancelled(false);
			AdhocReport report = new AdhocReport();
			Column node = AdhocReportHelper.getColumn(column, null, null);
			AdhocReportHelper.addColumnToAdhocReport(report, node, false,
					false, null);
			result.setResult(report.getColumns().get(0));
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	public OMElement getDrillDownColumnFromNode(String column) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			getUserBean().getRepSession().setCancelled(false);
			Column node = AdhocReportHelper.getColumn(column, null, null);
			DrillColumn dc = new DrillColumn(node.getDimension(), node
					.getName());
			DrillColumnList dcl = new DrillColumnList();
			dcl.addColumn(dc);
			dcl.setLabel(node.getName());
			OMFactory fac = OMAbstractFactory.getOMFactory();
			OMNamespace ns = null;
			OMElement parent = fac.createOMElement("DrillDownColumn", ns);
			dcl.addContent(fac, parent, ns);
			result.setResult(new XMLWebServiceResult(parent));
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	public OMElement getColumnSelectorForColumn(String column) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			getUserBean().getRepSession().setCancelled(false);
			Column node = AdhocReportHelper.getColumn(column, null, null);
			ColumnSelector cs = new ColumnSelector(node);
			result.setResult(cs);
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	/**
	 * Gets the XML data that can be used to render the report in the report SWF
	 * file.
	 * 
	 * @return an XML string containing a BirstWebServiceResult object. The
	 *         result field of that object contains the data if successful.
	 * @throws JRException
	 */
	private String getRenderedReport(AdhocReport report, JasperPrint print,
			Integer page, Map<String, Prompt> promptMap) throws JRException {
		try {
			long start = System.currentTimeMillis();
			com.successmetricsinc.UserBean userBean = getUserBean();
			userBean.getRepSession().setCancelled(false);
			JRFlexExporter exporter = new JRFlexExporter();
			Map<JRExporterParameter, Object> params = new HashMap<JRExporterParameter, Object>();
			params.put(JRFlexExporterParameter.SMI_SERVLET_PATH, "/SMIWeb");
			exporter.setParameters(params);
			Double scaleFactor = getScaleFactorAsDouble();
			exporter.setScaleFactor(scaleFactor.floatValue());
			exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
			userBean.setJasperPrintObject(print);
			if (page != null) {
				if (print.getPages().size() > 0) {
					exporter.setParameter(JRHtmlExporterParameter.PAGE_INDEX,
							page);
				}
			}
			exporter.setParameter(JRHtmlExporterParameter.IMAGES_URI,
					"charts.jsp?image=" + System.currentTimeMillis() + '-');
			exporter.setParameter(
					JRHtmlExporterParameter.IS_OUTPUT_IMAGES_TO_DIR,
					Boolean.FALSE);
			exporter.setParameter(
					JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN,
					Boolean.FALSE);
			StringBuffer s = new StringBuffer();
			exporter.setParameter(JRExporterParameter.OUTPUT_STRING_BUFFER, s);
			if (report != null) {
				exporter.setParameter(JRFlexExporterParameter.ADHOC_REPORT,
						report);
				if (report.isBackgroundVisible())
					exporter.setParameter(
							JRFlexExporterParameter.BACKGROUND_COLOR, report
									.getBackgroundColor());
			}
			com.successmetricsinc.UserBean ub = com.successmetricsinc.UserBean
					.getParentThreadUserBean();
			if (ub == null) {
				ub = SMIWebUserBean.getUserBean();
				SMIWebUserBean.addThreadRequest(Long.valueOf(Thread
						.currentThread().getId()), ub);
			}
			exporter.exportReport(promptMap);

			String str = s.toString();
			logger.debug("+++ getRenderedReport (time to export report): "
					+ (System.currentTimeMillis() - start));
			return str;
		} finally {
			com.successmetricsinc.UserBean.removeThreadRequest(Long
					.valueOf(Thread.currentThread().getId()));
		}
	}

	/**
	 * Gets the scale factor to use when rendering layout.
	 * 
	 * @return the scale factor
	 */
	// public OMElement getScaleFactor(OMElement element) {
	public OMElement getScaleFactor() {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			Double scaleFactor = getScaleFactorAsDouble();
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMText text = factory.createOMText(scaleFactor.toString());
			result.setResult(new XMLWebServiceResult(text));
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	public OMElement getDrillToList() {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMNamespace ns = null;
			OMElement node = factory.createOMElement("DrillTypes", ns);
			addDrillType(factory, node, ns, Chart.DRILL_TYPE_NONE);
			addDrillType(factory, node, ns, Chart.DRILL_TYPE_DRILL);
			addDrillType(factory, node, ns, Chart.DRILL_TYPE_NAVIGATE);
			com.successmetricsinc.UserBean user = getUserBean();
			List<CustomDrill> customDrills = user.getRepository()
					.getCustomDrills();
			if (customDrills != null) {
				for (CustomDrill cd : customDrills) {
					List<CustomDrill.Function> fns = cd.getFunctions();
					if (fns != null) {
						for (Iterator<CustomDrill.Function> iter = fns
								.iterator(); iter.hasNext();) {
							CustomDrill.Function fn = (CustomDrill.Function) iter
									.next();
							addDrillType(factory, node, ns, fn.getLabel(), fn
									.getName(), fn.getDescription());
						}
					}
				}
			}
			result.setResult(new XMLWebServiceResult(node));
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	private void addDrillType(OMFactory factory, OMElement parent,
			OMNamespace ns, String label) {
		addDrillType(factory, parent, ns, label, label, null);
	}

	private void addDrillType(OMFactory factory, OMElement parent,
			OMNamespace ns, String label, String value, String description) {
		OMElement node = factory.createOMElement("DrillType", ns);
		XmlUtils.addContent(factory, node, "label", label, ns);
		XmlUtils.addContent(factory, node, "value", value, ns);
		if (description != null) {
			XmlUtils.addContent(factory, node, "descriptions", description, ns);
		}
		parent.addChild(node);
	}

	public OMElement getReportQuery(String xmlData) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			OMFactory factory = OMAbstractFactory.getOMFactory();
			AdhocReport report = AdhocReport.convert(xmlData, null, null);
			OMText text = factory.createOMText(report.getQueryString(
					ServerContext.getRepository(), true));
			result.setResult(new XMLWebServiceResult(text));
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	/**
	 * Gets a list of values to put in the apply to measure drop down during a
	 * filter operation
	 * 
	 * @return the list of apply to measure filter columns
	 */
	// public OMElement getApplyToMeasureFilterCols(OMElement element) {
	public OMElement getApplyToMeasureFilterCols(String xmlData) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			AdhocReport report = AdhocReport.convert(xmlData, null, null);
			List<String> measureColumns = new ArrayList<String>();
			for (AdhocColumn column : report.getColumns()) {
				if (column.getType() == AdhocColumn.ColumnType.Measure
						&& (column.getFilters() == null || column.getFilters()
								.isEmpty())) {
					measureColumns.add(column.getColumn());
				}
			}

			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMNamespace ns = null;

			// builder.append("<MeasureFilterColumns>");
			OMElement measureFilterColumns = factory.createOMElement(
					"MeasureFilterColumns", ns);
			// builder.append("<column><label>All</label><value></value></column>");
			OMElement col = factory.createOMElement("column", ns);
			measureFilterColumns.addChild(col);

			XmlUtils.addContent(factory, col, "label", "All", ns);
			for (String s : measureColumns) {
				OMElement col1 = factory.createOMElement("column", ns);
				measureFilterColumns.addChild(col1);
				XmlUtils.addContent(factory, col1, "label", s, ns);
				XmlUtils.addContent(factory, col1, "value", s, ns);
			}
			result.setResult(new XMLWebServiceResult(measureFilterColumns));
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	/**
	 * can we get a list of values for this column filter.
	 * 
	 * @param reportIndex
	 *            the column unique id
	 * @return a boolean whether you can get a list of values for this filter.
	 */
	// public OMElement isReportColumnFilterSearchable(OMElement element) {
	public OMElement isReportColumnFilterSearchable(String xmlData,
			int reportIndex) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			AdhocReport report = AdhocReport.convert(xmlData, null, null);
			AdhocColumn col = findReportColumnByIndex(report, reportIndex);
			boolean b = (col.getType() == AdhocColumn.ColumnType.Dimension);
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMText text = factory.createOMText(Boolean.valueOf(b).toString());
			result.setResult(new XMLWebServiceResult(text));
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	/**
	 * Gets a list of filter values to display to the user when filtering a
	 * dimension
	 * 
	 * @param dim
	 *            the dimension name
	 * @param col
	 *            the column name
	 * @param filter
	 *            the initial value of the filter - used to further filter the
	 *            filter values
	 * @param format
	 *            the format of the column. Null if no format exists.
	 * @return a list of filter values and labels.
	 */
	// public OMElement getFilterValues(OMElement element) {
	public OMElement getFilterValues(String xmlData, String dim, String col,
			String filter, String format, boolean showAll) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			getUserBean().getRepSession().setCancelled(false);
			AdhocReport report = AdhocReport.convert(xmlData, null, null);

			boolean found = false;
			for (AdhocColumn ac : report.getColumns()) {
				if ((dim == null || dim.trim().length() == 0 || dim.equals(ac
						.getDimension()))
						&& (col != null && col.equals(ac.getColumn()))) {
					found = true;
					break;
				}
			}
			if (!found) {
				// need to add this column to the report
				if (dim == null || dim.trim().length() == 0)
					report.addMeasure(col, "temp");
				else
					report.addDimensionColumn(dim, col, null);
			}

			List<SelectItem> values = AdhocReportHelper
					.getColumnAvailableFilterValues(report, dim, col, filter,
							format, showAll);

			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMNamespace ns = null;
			// builder.append("<filters>");
			OMElement filters = factory.createOMElement("filters", ns);
			for (SelectItem sel : values) {
				// builder.append("<filter>");
				OMElement f = factory.createOMElement("filter", ns);
				XmlUtils.addContent(factory, f, "label", sel.getLabel(), ns);
				XmlUtils.addContent(factory, f, "value", sel.getValue()
						.toString(), ns);
				filters.addChild(f);
			}
			result.setResult(new XMLWebServiceResult(filters));
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	public OMElement getDBFormatValue(String dateOperand, String operandFormat,
			boolean isDateTime) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			if (dateOperand == null || dateOperand.isEmpty())
				throw new Exception(
						"Cannot create Filter. Invalid Date value specified.");
			String dbOperand = AdhocReportHelper.convertDateToDBDate(
					dateOperand, operandFormat, isDateTime);
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMNamespace ns = null;
			OMElement f = factory.createOMElement("Operand", ns);
			XmlUtils.addContent(factory, f, "DBFormat", dbOperand, ns);
			result.setResult(new XMLWebServiceResult(f));
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	public OMElement validateNumberFormat(String columnFormat) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			AdhocColumn.validateNumberFormat(columnFormat);
		} catch (Exception e) {
			Exception ee = new Exception("Invalid Number Format: "
					+ columnFormat);
			result.setException(ee);
		}
		return result.toOMElement();
	}

	/**
	 * Evaluate an expression for correctness
	 * 
	 * @param expr
	 *            the expression to evaluate
	 * @return
	 */
	// public OMElement evaluateExpression(String expr, boolean
	// isOlapExpression) {
	public OMElement evaluateExpression(String expr) {

		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			// assume that the macro will work. It throws exceptions if
			// GetPromptValue doesn't exist
			if (!expr.toUpperCase().contains(LogicalQueryString.BEGINMACRO)) {
				com.successmetricsinc.UserBean user = getUserBean();
				// new DisplayExpression(user.getRepository(), expr,
				// user.getRepSession(), isOlapExpression);
				new DisplayExpression(user.getRepository(), expr, user
						.getRepSession());
			}
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	/**
	 * Evaluate a query for correctness
	 * 
	 * @param query
	 *            the query to evaluate
	 * @return
	 */
	public OMElement evaluateQuery(String query) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			com.successmetricsinc.UserBean user = getUserBean();
			Repository r = user.getRepository();
			Session session = user.getRepSession();
			String query2 = QueryString
					.preProcessQueryString(query, r, session);
			AbstractQueryString aqs = AbstractQueryString
					.getInstance(r, query2);
			Query q = aqs.getQuery();
			q.navigateQuery(true, false, session, true);
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	public OMElement executeQuery(String queryString,
			int maxNumberOfRowsReturned) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			com.successmetricsinc.UserBean user = getUserBean();
			Repository r = user.getRepository();
			Session session = user.getRepSession();
			JasperDataSourceProvider jdsp = new JasperDataSourceProvider(r);
			jdsp.setSession(session);
			TimeZone userTZ = r.getServerParameters().getDisplayTimeZone();
			JRDataSource jrds = jdsp.create(queryString, session);
			QueryResultSet qrs = null;

			if (jrds instanceof QueryResultSet) {
				qrs = (QueryResultSet) jrds;
			}

			// limit to 10000 rows
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMNamespace ns = null;
			OMElement f = factory.createOMElement("Result", ns);
			OMElement rows = factory.createOMElement("rows", ns);
			if (qrs != null) {
				int size = Math.min(maxNumberOfRowsReturned, qrs.numRows());
				int numCols = qrs.getColumnNames().length;
				for (int row = 0; row < size; row++) {
					OMElement arr = factory
							.createOMElement("ArrayOfString", ns);
					for (int col = 0; col < numCols; col++) {
						Object o = qrs.getRows()[row][col];
						String s = null;
						if (o != null) {
							if (GregorianCalendar.class.isInstance(o)) {
								s = DateUtil.convertToDBFormat(
										((GregorianCalendar) o).getTime(),
										userTZ);
							} else {
								s = o.toString();
							}
						}
						XmlUtils.addContent(factory, arr, "string", s, ns);
					}
					rows.addChild(arr);
				}

				f.addChild(rows);
				OMElement columnNames = factory.createOMElement("columnNames",
						ns);
				OMElement displayNames = factory.createOMElement(
						"displayNames", ns);
				OMElement dataTypes = factory.createOMElement("dataTypes", ns);
				OMElement warehouseColumnsDataType = factory.createOMElement("warehouseColumnDataTypes", ns);
				OMElement columnFormats = factory.createOMElement("columnFormats", ns);
				for (int col = 0; col < numCols; col++) {
					XmlUtils.addContent(factory, columnNames, "string", qrs
							.getColumnNames()[col], ns);
					XmlUtils.addContent(factory, displayNames, "string", qrs
							.getDisplayNames()[col], ns);
					XmlUtils.addContent(factory, dataTypes, "int", qrs
							.getColumnDataTypes()[col], ns);
					String s = qrs.getWarehouseColumnsDataTypes()[col]; 
					XmlUtils.addContent(factory, warehouseColumnsDataType, "string", 
							s == null ? "" : s, ns);
					s = qrs.getColumnFormats()[col];
					XmlUtils.addContent(factory, columnFormats, "string", s == null ? "" : s, ns);
				}
				f.addChild(columnNames);
				f.addChild(displayNames);
				f.addChild(dataTypes);
				f.addChild(warehouseColumnsDataType);
				f.addChild(columnFormats);
				XmlUtils.addContent(factory, f, "numRowsReturned", size, ns);
				XmlUtils.addContent(factory, f, "hasMoreRows", size < qrs
						.numRows(), ns);

			}

			result.setResult(new XMLWebServiceResult(f));
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	public OMElement getVisualizerData(String filename) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean ub = getUserBean();
			CatalogManager cm = ub.getCatalogManager();
			String root = cm.getCatalogRootPath();
			AdhocReport report = AdhocReport.read(new File(root + "/"
					+ filename));

			String query = report.getQueryString(ub.getRepository(), false);

			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMNamespace ns = null;
			OMElement f = factory.createOMElement("Result", ns);
			f.addAttribute("query", query, ns);
			OMElement cols = factory.createOMElement("Columns", ns);
			for (AdhocColumn ac : report.getColumns()) {
				OMElement col = factory.createOMElement("Col", ns);
				col.addAttribute("alias", ac.getFieldName(), ns);
				String expression = ac.getUniqueExpressionString();
				if (ac.getType() == ColumnType.Dimension
						|| ac.getType() == ColumnType.Measure)
					col.addAttribute("column", "[" + expression + "]", ns);
				else
					col.addAttribute("column", expression, ns);
				if (ac.treatAsMeasure())
					col.addAttribute("format", ac.getFormat(), ns);
				if (ac.getType() == ColumnType.Ratio)
					col.addAttribute("isRatio", "true", ns);
				cols.addChild(col);
			}
			f.addChild(cols);

			result.setResult(new XMLWebServiceResult(f));
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();

	}

	private AdhocColumn findReportColumnByIndex(AdhocReport report,
			int reportIndex) {
		for (AdhocColumn col : report.getColumns()) {
			if (col.getReportIndex() == reportIndex) {
				return col;
			}
		}
		return null;
	}

	private Double getScaleFactorAsDouble() {
		Double scaleFactor = (Double) getSessionObject("scaleFactor");
		if (scaleFactor == null) {
			scaleFactor = ServerContext.getDefaultScaleFactor();
		}
		return scaleFactor;
	}

	public OMElement getDashboardRenderedReport(String name, String prompts,
			Integer page, boolean applyAllPrompts, String pagePath, String guid) {
		return getDashboardRenderedReportWithLogging(name, prompts, page,
				applyAllPrompts, pagePath, guid, true, false);
	}

	public OMElement getDashboardRenderedReportV2(String name, String prompts,
			Integer page, boolean applyAllPrompts, String pagePath,
			String guid, boolean isDashletEdit) {
		return getDashboardRenderedReportWithLogging(name, prompts, page,
				applyAllPrompts, pagePath, guid, true, isDashletEdit);
	}

	public OMElement getDashboardRenderedReportWithLogging(String name,
			String prompts, Integer page, boolean applyAllPrompts,
			String pagePath, String guid, boolean logUsage,
			boolean isDashletEdit) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		AdhocReport report = null;
		SMIWebUserBean ub = null;
		try {
			long start = System.currentTimeMillis();
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			List<Filter> filterParams = AdhocReportHelper
					.parsePromptData(prompts);

			// prepopulate prompts now so that report conversion will use the
			// correct value of getPromptValue
			Session.clear();
			Session.setParams(filterParams);

			ub = getUserBean();
			ub.getRepSession().setCancelled(false);
			CatalogManager cm = ub.getCatalogManager();
			name = cm.getNormalizedPath(name);
			File f = new File(name);

			// XXX should clean this up to throw exceptions in the new way
			// catalog manager getpermissions throws illegal argument exception
			try {
				if (!cm.isViewableFolder(f.getParent())) {
					result.setErrorMessage(f.getName()
							+ " is in a folder that is not accessible.");
					result.setErrorCode(BaseException.ERROR_PERMISSION);
					return result.toOMElement();
				}
			} catch (IllegalArgumentException iae) {
				result.setErrorMessage(f.getName() + " does not exist");
				result.setErrorCode(BaseException.ERROR_FILE_NOT_FOUND);
				return result.toOMElement();
			}

			File aFile = CatalogManager.getAdhocReportFile(name, cm
					.getCatalogRootPath());
			if (aFile == null || aFile.exists() == false) {
				result.setResult(new StringWebServiceResult(getDashboardReport(
						name, prompts, page, applyAllPrompts)));
			} else {
				Session.setRenderer(Session.Renderer.Dashboard);
				report = AdhocReport.read(aFile);
				if (!isDashletEdit) {
					report.setHidedata(false);
				} else {
					report.setEditDashlet(true);
				}
				OMFactory factory = OMAbstractFactory.getOMFactory();
				OMNamespace ns = null;
				OMElement el = factory.createOMElement("root", ns);
				report.addContent(el, factory, ns);
				OMElement colSelectors = report.getColumnSelectors();
				el.addChild(colSelectors);

				String rep = getDashboardReport(report, prompts, page,
						colSelectors.toString(), applyAllPrompts);
				XMLInputFactory inputFactory = XmlUtils
						.getSecureXMLInputFactory();
				XMLStreamReader parser = inputFactory
						.createXMLStreamReader(new StringReader(rep));

				StAXOMBuilder builder = new StAXOMBuilder(parser);
				OMElement doc = null;
				doc = builder.getDocumentElement();
				OMNode node = doc.detach();
				el.addChild(node);

				// Add modification time of report
				OMElement modEl = factory.createOMElement("lastModified", ns);
				modEl.setText(Long.toString(aFile.lastModified()));
				el.addChild(modEl);

				result.setResult(new XMLWebServiceResult(el));
			}
			if (logUsage && pagePath != null
					&& !pagePath.equalsIgnoreCase("NO_LOG")) {
				String id = null;
				HttpSession session = SessionHelper.getSession();
				if (session != null)
					id = session.getId();
				AuditLog.log("dashboard", name, pagePath, guid, id, System
						.currentTimeMillis()
						- start);
			}
		} catch (AsynchReportGenerationException arge) {
			try {
				AdhocReportHelper.handleAsynchReportGenerationException(arge, getUserBean(), result);
			} catch (Exception e) {
				result.setException(e);
			}
		} catch (Exception e) {
			result.setException(e);
		} finally {
			if (report != null) {
				report.setEditDashlet(false);
			}
		}
		Session.setRenderer(Session.Renderer.Adhoc);
		return result.toOMElement();
	}
	
	public OMElement getReportColumnSelectors(String name) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		AdhocReport report = null;
		SMIWebUserBean ub = null;
		try {
			long start = System.currentTimeMillis();
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			ub = getUserBean();
			CatalogManager cm = ub.getCatalogManager();
			name = cm.getNormalizedPath(name);
			File f = new File(name);

			// XXX should clean this up to throw exceptions in the new way
			// catalog manager getpermissions throws illegal argument exception
			try {
				if (!cm.isViewableFolder(f.getParent())) {
					result.setErrorMessage(f.getName()
							+ " is in a folder that is not accessible.");
					result.setErrorCode(BaseException.ERROR_PERMISSION);
					return result.toOMElement();
				}
			} catch (IllegalArgumentException iae) {
				result.setErrorMessage(f.getName() + " does not exist");
				result.setErrorCode(BaseException.ERROR_FILE_NOT_FOUND);
				return result.toOMElement();
			}

			File aFile = CatalogManager.getAdhocReportFile(name, cm
					.getCatalogRootPath());
			
			if (aFile != null && aFile.exists()) {
				Session.setRenderer(Session.Renderer.Dashboard);
				report = AdhocReport.read(aFile);
				OMElement colSelectors = report.getColumnSelectors();
				result.setResult(new XMLWebServiceResult(colSelectors));
			}
		} catch (Exception e) {
			result.setException(e);
		}
		Session.setRenderer(Session.Renderer.Adhoc);
		return result.toOMElement();
	}

	public OMElement getDashletPluginReportFile(String name, String pagePath,
			String guid, boolean logUsage, boolean isDashletEdit) {
		BirstWebServiceResult result = new BirstWebServiceResult();

		try {
			long start = System.currentTimeMillis();
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}

			SMIWebUserBean ub = getUserBean();
			ub.getRepSession().setCancelled(false);

			// Should be .dashlet file only
			if (!name.toLowerCase().endsWith(".dashlet")) {
				result.setErrorMessage(name + " is invalid file type");
				result.setErrorCode(BaseException.ERROR_PERMISSION);
				return result.toOMElement();
			}

			CatalogManager cm = ub.getCatalogManager();
			name = cm.getNormalizedPath(name);
			File f = new File(name);

			// XXX should clean this up to throw exceptions in the new way
			// catalog manager getpermissions throws illegal argument exception
			try {
				if (!cm.isViewableFolder(f.getParent())) {
					result.setErrorMessage(f.getName()
							+ " is in a folder that is not accessible.");
					result.setErrorCode(BaseException.ERROR_PERMISSION);
					return result.toOMElement();
				}
			} catch (IllegalArgumentException iae) {
				result.setErrorMessage(f.getName() + " does not exist");
				result.setErrorCode(BaseException.ERROR_FILE_NOT_FOUND);
				return result.toOMElement();
			}

			if (!f.exists()) {
				result.setErrorMessage(f.getName() + " does not exist");
				result.setErrorCode(BaseException.ERROR_FILE_NOT_FOUND);
				return result.toOMElement();
			}

			String data = new String(java.nio.file.Files.readAllBytes(f
					.toPath()), "UTF-8");
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMNamespace ns = null;
			OMElement el = factory.createOMElement("data", ns);
			el.setText(data);
			result.setResult(new XMLWebServiceResult(el));

			if (logUsage && pagePath != null
					&& !pagePath.equalsIgnoreCase("NO_LOG")) {
				String id = null;
				HttpSession session = SessionHelper.getSession();
				if (session != null)
					id = session.getId();
				AuditLog.log("dashboard", name, pagePath, guid, id, System
						.currentTimeMillis()
						- start);
			}
		} catch (Exception e) {
			result.setException(e);
		}

		return result.toOMElement();
	}
	
	/**
	 * Handles async processing for export, but also will handle synchronous.  Pre-generates the jasper print object, allowing better error handling
	 * in export.
	 *  
	 * @param exportType					Which export format (pdf, excel, ...)
	 * @param reportXML						The XML of the report.  If empty, the reportName will be used to load the report
	 * @param reportName					The name of the report.  Only used if the XML is empty
	 * @param promptsXML					Prompts to use
	 * @param applyAllPrompts				Apply all the prompts even if the column is not in the projection list
	 * @param dashboardParams				Additional params passed by an SSO request
	 * @param dashboardParamsSeparator		The separator for dashboardParams
	 * @param dashletParams					Column selector params
	 * @return
	 */
	public OMElement preExport(String exportType, String reportXML, String reportName, 
			String promptsXML, boolean applyAllPrompts, String dashboardParams, 
			String dashboardParamsSeparator, String dashletParams) {
		
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			Session.setRenderer(Renderer.Dashboard);
			if (dashboardParamsSeparator != null && dashboardParamsSeparator.length() > 0) {
				Session.setDashboardParamsSeperator(dashboardParamsSeparator);	
			}
			if(dashboardParams != null) {
				Session.setDashboardParams(dashboardParams);
			}
			
			if (promptsXML != null && promptsXML.length() > 0) {
				List<Filter> filterParams = AdhocReportHelper.parsePromptData(promptsXML);
				Session.setParams(filterParams);
			}
			
			boolean ignorePagination = false;
			if (exportType != null && (exportType.equalsIgnoreCase("xls") || exportType.equalsIgnoreCase("csv")))
				ignorePagination = true;
			

			boolean isPdf = false;
			if (exportType != null && exportType.equalsIgnoreCase("pdf"))
				isPdf = true;

			SMIWebUserBean userBean = SessionUtils.getUserBean();
			AdhocReport report = null;
			JasperPrint jprint = null;
			if (reportXML != null && reportXML.length() > 0) {
				report = AdhocReport.convert(reportXML, null, null);
			}
			else if (reportName != null){
				CatalogManager cm = userBean.getCatalogManager();
				String filename = cm.getNormalizedPath(reportName);
				File adhocReportFile = CatalogManager.getAdhocReportFile(filename, cm.getCatalogRootPath());
				if (adhocReportFile != null) {
					report = AdhocReport.read(new File(filename));
				}
			}
			
			if (report == null) {
				// jrxml file
				jprint = AdhocReportHelper.getDashboardJasperPrint(reportName, promptsXML, userBean.getRepository(), userBean, ignorePagination, applyAllPrompts, dashletParams);
			}
			else {
				jprint = AdhocReportHelper.getDashboardJasperPrint(report, promptsXML, dashletParams, userBean.getRepository(), ignorePagination, userBean, applyAllPrompts, isPdf, true);
			}
			
			// now associate the jasper print object with an async result id
			String key = userBean.addExportJasperPrintObject(jprint);
			result.setAsynchronousResultId(key);
		} catch (AsynchReportGenerationException arge) {
			try {
				AdhocReportHelper.handleAsynchReportGenerationException(arge,getUserBean(), result);
			} catch (Exception e) {
				result.setException(e);
			}
		} catch (Exception e) {
			logger.error(e, e);
			result.setException(e);
		}
		return result.toOMElement();
	}

	public OMElement addColumnToDashboardReport(String xmlData, String prompts,
			Integer page, String dashletPrompts, String column, String label,
			boolean isPivotColumn, boolean applyAllPrompts) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			long start = System.currentTimeMillis();
			List<Filter> filterParams = AdhocReportHelper
					.parsePromptData(prompts);

			// prepopulate prompts now so that report conversion will use the
			// correct value of getPromptValue
			Session.clear();
			Session.setParams(filterParams);
			Session.setDashletPrompts(dashletPrompts);

			AdhocReport report = AdhocReport.convert(xmlData, null, null);
			Column node = AdhocReportHelper.getColumn(column, null, null);
			node.setDisplayName(label);
			AdhocReportHelper.addColumnToAdhocReport(report, node,
					isPivotColumn);
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMNamespace ns = null;
			OMElement el = factory.createOMElement("root", ns);
			report.addContent(el, factory, ns);
			String rep = getDashboardReport(report, prompts, page,
					dashletPrompts, applyAllPrompts);
			XMLInputFactory inputFactory = XmlUtils.getSecureXMLInputFactory();
			XMLStreamReader parser = inputFactory
					.createXMLStreamReader(new StringReader(rep));

			StAXOMBuilder builder = new StAXOMBuilder(parser);
			OMElement doc = null;
			doc = builder.getDocumentElement();
			OMNode node1 = doc.detach();
			el.addChild(node1);
			result.setResult(new XMLWebServiceResult(el));

			String id = null;
			HttpSession session = SessionHelper.getSession();
			if (session != null)
				id = session.getId();
			AuditLog.log("dashboard", report.getFileName(), id, System
					.currentTimeMillis()
					- start);
		} catch (AsynchReportGenerationException arge) {
			try {
				AdhocReportHelper.handleAsynchReportGenerationException(arge, getUserBean(), result);
			} catch (Exception e) {
				result.setException(e);
			}
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	public OMElement getDashboardRenderedReportFromXML(String xmlData,
			String prompts, Integer page, String dashletPrompts,
			boolean applyAllPrompts, String pagePath, String guid) {
		return getDashboardRenderedReportFromXMLV2(xmlData, prompts, page,
				dashletPrompts, applyAllPrompts, pagePath, guid, false);
	}

	public OMElement getDashboardRenderedReportFromXMLV2(String xmlData,
			String prompts, Integer page, String dashletPrompts,
			boolean applyAllPrompts, String pagePath, String guid,
			boolean isDashletEdit) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		AdhocReport report = null;
		try {
			long start = System.currentTimeMillis();
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			Session.clear();
			Session.setRenderer(Session.Renderer.Dashboard);
			Session.setDashletPrompts(dashletPrompts);
			List<Filter> filterParams = AdhocReportHelper
					.parsePromptData(prompts);

			// prepopulate prompts now so that report conversion will use the
			// correct value of getPromptValue
			Session.setParams(filterParams);
			Session.setDashletPrompts(dashletPrompts);

			report = AdhocReport.convert(xmlData, null, null);
			report.setEditDashlet(isDashletEdit);
			String rep = getDashboardReport(report, prompts, page,
					dashletPrompts, applyAllPrompts);
			result.setResult(new StringWebServiceResult(rep));
			if (pagePath != null && !pagePath.equalsIgnoreCase("NO_LOG")) {
				String id = null;
				HttpSession session = SessionHelper.getSession();
				if (session != null)
					id = session.getId();
				AuditLog.log("dashboard", report.getFileName(), pagePath, guid,
						id, System.currentTimeMillis() - start);
			}
		} catch (AsynchReportGenerationException arge) {
			try {
				AdhocReportHelper.handleAsynchReportGenerationException(arge, getUserBean(), result);
			} catch (Exception e) {
				result.setException(e);
			}
		} catch (Exception e) {
			result.setException(e);
		} finally {
			if (report != null) {
				report.setEditDashlet(false);
			}
		}
		Session.setRenderer(Session.Renderer.Adhoc);
		return result.toOMElement();
	}

	private String getDashboardReport(AdhocReport report, String prompts,
			Integer page, String dashletPrompts, boolean applyAllPrompts)
			throws Exception {
		SMIWebUserBean ub = getUserBean();
		Repository r = ub.getRepository();
		List<Filter> filterParams = AdhocReportHelper.parsePromptData(prompts);

		// prepopulate prompts now so that report conversion will use the
		// correct value of getPromptValue
		Session.setParams(filterParams);
		Session.setDashletPrompts(dashletPrompts);

		JasperPrint jprint = AdhocReportHelper.getDashboardJasperPrint(report,
				prompts, dashletPrompts, r, false, ub, applyAllPrompts, false,
				false);
		String rep = this.getRenderedReport(report, jprint, page,
				AdhocReportHelper.parsePromptMap(prompts));
		return addReportRenderData(rep, jprint == null ? 0 : jprint.getPages()
				.size(), page);
	}

	@SuppressWarnings("unchecked")
	private String getDashboardReport(String filename, String prompts,
			Integer page, boolean applyAllPrompts) throws Exception {
		String result = "";

		List<Filter> filterParams = AdhocReportHelper.parsePromptData(prompts);

		// prepopulate prompts now so that report conversion will use the
		// correct value of getPromptValue
		Session.setParams(filterParams);

		SMIWebUserBean ub = getUserBean();
		ub.getRepSession().setCancelled(false);
		// jdsp.params = null;
		JasperPrint print = AdhocReportHelper.getDashboardJasperPrint(filename,
				prompts, ub.getRepository(), ub, false, applyAllPrompts);

		Map<String, Prompt> promptMap = new HashMap<String, Prompt>();
		XMLInputFactory inputFactory = XmlUtils.getSecureXMLInputFactory();
		XMLStreamReader parser = inputFactory
				.createXMLStreamReader(new StringReader(prompts));

		StAXOMBuilder builder = new StAXOMBuilder(parser);
		OMElement doc = null;
		doc = builder.getDocumentElement();
		doc.build();
		doc.detach();
		for (Iterator<OMElement> iter = doc.getChildElements(); iter.hasNext();) {
			OMElement el = iter.next();
			Prompt prompt = new Prompt();
			prompt.parseElement(el);
			promptMap.put(prompt.getVisibleName(), prompt);
		}
		result = this.getRenderedReport(null, print, page, promptMap);
		return addReportRenderData(result, print == null ? 0 : print.getPages()
				.size(), page);
	}

	/**
	 * Returns the XML representation of a query filter object
	 * 
	 * @param column
	 * @return
	 */
	public OMElement getFilterPrompt(String nodeString) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			Document node = DocumentHelper.parseText(nodeString);
			Element root = node.getRootElement();
			Attribute type = root.attribute("t");
			Column col = null;
			Attribute val = root.attribute("v");
			if ("om".equalsIgnoreCase(type.getValue())) {
				Attribute dim = root.attribute("od");
				Attribute hier = root.attribute("oh");
				col = AdhocReportHelper.getColumn(val.getValue(), dim
						.getValue(), hier.getValue());
			} else {
				col = AdhocReportHelper.getColumn(val.getValue(), null, null);
			}
			Attribute labela = root.attribute("l");
			String label = null;
			if (labela != null)
				label = labela.getValue();
			if (label == null) {
				label = val.getValue();
				int index = label.lastIndexOf('.');
				if (index > 0) {
					label = label.substring(index);
				}
			}
			Prompt prompt = new Prompt(col, label);
			result.setResult(prompt);
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	public OMElement getFilterPrompts(String column) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			OMFactory fac = OMAbstractFactory.getOMFactory();
			OMNamespace ns = fac.createOMNamespace("", "");
			OMElement el = fac.createOMElement("prompts", ns);
			String[] columns = column.split(",");
			for (int i = 0; i < columns.length; i++) {
				if (columns[i] != null && columns[i].trim().length() > 0) {
					Column node = AdhocReportHelper.getColumn(columns[i], null,
							null);
					Prompt prompt = new Prompt(node);
					prompt.addContent(el, fac, ns);
				}
			}
			result.setResult(new XMLWebServiceResult(el));
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	@SuppressWarnings("unchecked")
	public OMElement getFilledOptions(String prompts) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean userBean = getUserBean();
			// 12123
			userBean.clearJasperPrintObject();
			Session session = userBean.getRepSession();
			Session.clear();
			session.setCancelled(false);
			JasperDataSourceProvider jdsp = userBean
					.getJasperDataSourceProvider();
			jdsp = new JasperDataSourceProvider(jdsp);
			List<Filter> filters = (this.isParseInJson) ? 
									AdhocReportHelper.parsePromptDataJson(prompts) : AdhocReportHelper.parsePromptData(prompts);
			Session.setParams(filters);
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMNamespace ns = null;
			OMElement res = factory.createOMElement("PromptValues", ns);
		
			try {
				OMElement doc = (this.isParseInJson) ? 
								AdhocReportHelper.jsonToOMElement(prompts) : AdhocReportHelper.xmlToOMElement(prompts);
					
				List<Prompt> promptList = new ArrayList<Prompt>();
				for (Iterator<OMElement> iter = doc.getChildElements(); iter
						.hasNext();) {
					OMElement el = iter.next();
					Prompt prompt = new Prompt();
					prompt.parseElement(el);
					promptList.add(prompt);
				}

				HashMap<String, String> hierarchicalMap = new HashMap<String, String>();
				HashMap<String, Object> parameters = new HashMap<String, Object>();
				for (Prompt prompt : promptList) {
					prompt.createQueryWithHierarchicalFilters(ServerContext
							.getRepository(), promptList);
					if (prompt.isQueryBase()) {
						List<String> selectedValues = prompt
								.getSelectedValues();
						String matchedValue = prompt.fillQuery(jdsp,
								hierarchicalMap, parameters);
						if (matchedValue != null && !matchedValue.isEmpty()) {
							setHierarachicalMapPromptValue(parameters,
									hierarchicalMap, prompt, matchedValue);
						}
						// bug 10761
						if (prompt.hasParents())
							prompt.setSelectedValues(selectedValues);

					}
					Filter f = Session.getFilter(prompt.getColumnName());
					if (f != null)
						f.setValues(prompt.getSelectedValues());

					OMElement pv = getFilledOptionValue(prompt, factory, ns);
					if (pv != null) {
						res.addChild(pv);
					}
				}
			} catch (XMLStreamException e) {
				logger.error(e, e);
			} catch (FactoryConfigurationError e) {
				logger.error(e, e);
			}

			result.setResult(new XMLWebServiceResult(res));
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	private void setHierarachicalMapPromptValue(
			HashMap<String, Object> parameters,
			HashMap<String, String> hierarchicalMap, Prompt prompt, String value) {
		parameters.put(prompt.getParameterName(), value);
		hierarchicalMap.put(prompt.getParameterName(), value);
		prompt.setSelectedValue(value);
	}

	private OMElement getFilledOptionValue(Prompt prompt, OMFactory factory,
			OMNamespace ns) {
		List<String> filledOptionLabels = prompt.getFilledOptions();
		List<String> filledValues = prompt.getFilledOptionValues();
		List<Boolean> filledEnabled = prompt.getFilledEnabledValues();

		OMElement pv = factory.createOMElement("PromptValue", ns);
		
		XmlUtils.addContent(factory, pv, "PromptID", prompt.getParameterName(),
					ns);
		
		
		if (filledValues != null && filledValues.size() > 0) {
			for (int i = 0; i < filledValues.size(); i++) {
				String label = filledOptionLabels.size() > i ? filledOptionLabels
						.get(i)
						: filledValues.get(i);
				OMElement val = factory.createOMElement("Value", ns);
				XmlUtils.addContent(factory, val, (this.isParseInJson) ? "l" : "Label", label, ns);
				XmlUtils.addContent(factory, val, (this.isParseInJson) ? "v" : "Value", filledValues.get(i),
						ns);
				if (filledEnabled != null && filledEnabled.size() > i)
					XmlUtils.addContent(factory, val, "Enabled", filledEnabled
							.get(i), ns);
					pv.addChild(val);
			}
		}

		return pv;
	}

	@SuppressWarnings("unchecked")
	public OMElement getFilteredFilledOptions(String prompts) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean userBean = getUserBean();
			Session session = userBean.getRepSession();
			session.setCancelled(false);
			List<Filter> filters = (this.isParseInJson) ? 
					AdhocReportHelper.parsePromptDataJson(prompts) : AdhocReportHelper.parsePromptData(prompts);
			Session.clear();
			Session.setParams(filters);
			JasperDataSourceProvider jdsp = userBean
					.getJasperDataSourceProvider();
			jdsp = new JasperDataSourceProvider(jdsp);
			List<Prompt> promptList = new ArrayList<Prompt>();
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMNamespace ns = null;
			OMElement res = factory.createOMElement("PromptValues", ns);

			try {
				OMElement doc = (this.isParseInJson) ? 
						AdhocReportHelper.jsonToOMElement(prompts) : AdhocReportHelper.xmlToOMElement(prompts);

				Map<String, List<Prompt>> promptMap = new HashMap<String, List<Prompt>>();
				for (Iterator<OMElement> iter = doc.getChildElements(); iter
						.hasNext();) {
					OMElement el = iter.next();
					Prompt prompt = new Prompt();
					prompt.parseElement(el);
					promptList.add(prompt);

					if (prompt.hasParents()) {
						Iterator<String> itr = prompt.getParents().iterator();
						while (itr.hasNext()) {
							String parent = itr.next();
							List<Prompt> l = promptMap.get(parent);
							if (l == null) {
								l = new ArrayList<Prompt>();
								promptMap.put(parent, l);
							}
							l.add(prompt);
						}
					}
				}

				if (promptList.size() > 1) {

					// determine hierarchies of prompts
					List<Prompt> hierarchy = new ArrayList<Prompt>();
					for (Prompt p : promptList) {
						if (!p.hasParents()) {
							hierarchy.add(p);
						}
					}

					for (int i = 0; i < hierarchy.size(); i++) {
						Prompt p = hierarchy.get(i);
						String name = p.getParameterName();
						if (promptMap.get(name) != null) {
							List<Prompt> l = promptMap.get(name);
							for (Prompt p1 : l) {
								if (!hierarchy.contains(p1))
									hierarchy.add(p1);
							}
						}

					}

					Prompt driver = hierarchy.get(0);
					Map<String, String> hierarchicalFilterMap = new HashMap<String, String>();
					Map<String, Object> parameterMap = new HashMap<String, Object>();
					List<String> selectedValues = driver.getSelectedValues();
					String selectedValue = QueryString.NO_FILTER_TEXT;
					if (selectedValues.size() > 0) {
						StringBuilder sb = new StringBuilder();
						for (String s : selectedValues) {
							if (sb.length() > 0) {
								sb.append("||");
							}
							sb.append(s);
						}
						selectedValue = sb.toString();
					}
					String colName = driver.getColumnName();
					if (colName == null || colName.indexOf('.') < 0) {
						colName = driver.getQueryColumnName();
					}
					hierarchicalFilterMap.put(colName, selectedValue);
					Session
							.setFilter(new Filter(
									driver.getOperator(),
									driver.getSelectedValues(),
									driver.getMultiSelectType() == Prompt.MultipleSelectionType.AND ? "AND"
											: driver.getMultiSelectType() == Prompt.MultipleSelectionType.OR ? "OR"
													: "NONE", driver
											.getFilterType(), colName));
					parameterMap.put(colName, selectedValue);
					for (int i = 1; i < hierarchy.size(); i++) {
						Prompt p = hierarchy.get(i);
						p.createQueryWithHierarchicalFilters(ServerContext
								.getRepository(), hierarchy);
						p.fillQuery(jdsp, hierarchicalFilterMap, parameterMap);
						List<String> selValues = p.getSelectedValues();
						Session
								.setFilter(new Filter(
										p.getOperator(),
										p.getSelectedValues(),
										p.getMultiSelectType() == Prompt.MultipleSelectionType.AND ? "AND"
												: p.getMultiSelectType() == Prompt.MultipleSelectionType.OR ? "OR"
														: "NONE", p
												.getFilterType(), p
												.getColumnName()));
						List<String> filledOptions = p.getFilledOptions();
						if (selValues != null && filledOptions != null
								&& selValues.size() > 0
								&& filledOptions.size() > 0) {
							List<String> removedValues = new ArrayList<String>();
							for (String s : selValues) {
								if (!filledOptions.contains(s)) {
									removedValues.add(s);
								}
							}
							if (removedValues.size() > 0) {
								for (String s : removedValues) {
									selValues.remove(s);
								}
							}

							if (selValues.isEmpty()
									&& p.isAddNoSelectionEntry() == false) {
								selValues.add(filledOptions.get(0));
							}
						}
					}
				}

				for (Prompt p : promptList) {
					OMElement pv = getFilledOptionValue(p, factory, ns);
					if (pv != null) {
						res.addChild(pv);
					}
				}

			} catch (XMLStreamException e) {
				logger.error(e, e);
			} catch (FactoryConfigurationError e) {
				logger.error(e, e);
			}
			result.setResult(new XMLWebServiceResult(res));
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	private static class VariableCompare implements Comparator<Variable> {
		public int compare(Variable v1, Variable v2) {
			return v1.getName().compareTo(v2.getName());
		}
	}

	public OMElement getVariableList() {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			com.successmetricsinc.UserBean ub = getUserBean();
			Repository rep = ub.getRepository();
			Variable[] variables = rep.getVariables();
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMNamespace ns = null;
			OMElement res = factory.createOMElement("Variables", ns);
			if (variables != null) {
				Arrays.sort(variables, new VariableCompare());
				for (Variable v : variables) {
					// do not display Load: variables
					if (v.getName() != null && v.getName().startsWith("Load:"))
						continue;
					if (v.getType() == Variable.VariableType.Repository) {
						OMElement var = factory.createOMElement("Variable", ns);
						XmlUtils.addContent(factory, var, "Label", v.getName(),
								ns);
						XmlUtils.addContent(factory, var, "Value", "V{"
								+ v.getName() + "}", ns);
						res.addChild(var);
					}
				}
			}
			Map<String, Object> sessionVars = ub.getRepSession()
					.getSessionVariables();
			if (sessionVars != null) {
				Set<String> vars = new TreeSet<String>(sessionVars.keySet());
				for (String key : vars) {
					if (key != null && key.startsWith("Load:"))
						continue;
					OMElement var = factory.createOMElement("Variable", ns);
					XmlUtils.addContent(factory, var, "Label", key, ns);
					XmlUtils.addContent(factory, var, "Value",
							"V{" + key + "}", ns);
					res.addChild(var);
				}
			}
			result.setResult(new XMLWebServiceResult(res));
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	public OMElement getCustomGeoMaps() {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			com.successmetricsinc.UserBean ub = getUserBean();
			Repository rep = ub.getRepository();
			OMElement customMap = CustomGeoMap.getCustomGeoMaps(rep);
			if (customMap != null)
				result.setResult(new XMLWebServiceResult(customMap));
			else
				result.setResult(new StringWebServiceResult("<empty/>"));
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();

	}

	public OMElement getDrillMaps() {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			com.successmetricsinc.UI.SMIWebUserBean ub = getUserBean();
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMNamespace ns = null;
			DrillMapList drillMapList = ub.getDrillMaps();
			drillMapList.validate(ub.getRepository());
			OMElement root = drillMapList.getDrillMapsXMLDocument(ns, factory,
					false, true);
			result.setResult(new XMLWebServiceResult(root));
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	public OMElement getDrillMapDetails(String xmlData) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			com.successmetricsinc.UI.SMIWebUserBean ub = getUserBean();
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMNamespace ns = null;
			OMElement root = factory.createOMElement("DrillMapDetails", ns);
			DrillMap drillMap = DrillMap.convert(xmlData, ns);
			drillMap.validate(ub.getRepository());
			DrillMap existingDrillMap = ub.getDrillMaps().getExistingDrillMap(
					drillMap);
			if (existingDrillMap != null) {
				existingDrillMap.addContent(factory, root, ns, true, true);
			}
			result.setResult(new XMLWebServiceResult(root));
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	public OMElement addDrillMap(String xmlData) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			com.successmetricsinc.UI.SMIWebUserBean ub = getUserBean();
			DrillMapList drillMaps = ub.getDrillMaps();
			DrillMap drillMap = DrillMap.convert(xmlData, null);
			if (drillMap != null) {
				// validate if the duplicate name exists
				DrillMap existingDrillMap = ub.getDrillMaps().getDrillMap(
						drillMap.getName());
				if (existingDrillMap == null) {
					// create a new id for map and all the drill rows
					drillMap.setId(UUID.randomUUID().toString());
					drillMap.setCreatedDate(new Date());
					drillMap.setCreatedBy(ub.getUserName());
					List<DrillRow> drillRows = drillMap.getDrillRows();
					if (drillRows != null && drillRows.size() > 0) {
						for (DrillRow drillRow : drillRows) {
							drillRow.setId(UUID.randomUUID().toString());
						}
					}

					drillMaps.addDrillMap(drillMap);
					drillMaps.save(ub.getRepository().getServerParameters()
							.getApplicationPath());
				} else {
					result
							.setErrorCode(BaseException.ERROR_DRILL_MAP_DUPLICATE_NAME);
				}
			}

		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	public OMElement deleteDrillMap(String xmlData) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			com.successmetricsinc.UI.SMIWebUserBean ub = getUserBean();
			DrillMapList drillMaps = ub.getDrillMaps();
			DrillMap drillMap = DrillMap.convert(xmlData, null);
			drillMaps.deleteDrillMap(drillMap);
			drillMaps.save(ub.getRepository().getServerParameters()
					.getApplicationPath());
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	public OMElement saveDrillMap(String xmlData) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			com.successmetricsinc.UI.SMIWebUserBean ub = getUserBean();
			DrillMapList drillMaps = ub.getDrillMaps();
			DrillMap drillMap = DrillMap.convert(xmlData, null);
			// validate if the duplicate name exists
			DrillMap existingDrillMap = ub.getDrillMaps().getDrillMap(
					drillMap.getName());
			if (existingDrillMap != null
					&& !existingDrillMap.getId().equalsIgnoreCase(
							drillMap.getId())) {
				result
						.setErrorCode(BaseException.ERROR_DRILL_MAP_DUPLICATE_NAME);
			} else {
				drillMap.setModifiedDate(new Date());
				drillMap.setModifiedBy(ub.getUserName());
				drillMaps.modifyDrillMap(drillMap);
				drillMaps.save(ub.getRepository().getServerParameters()
						.getApplicationPath());
			}
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	/**
	 * Evaluate an drill column for correctness
	 * 
	 * @param drill
	 *            column of the form dimension.column
	 * @return
	 */
	public OMElement evaluateDrillColumn(String drillNodeExpression) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			com.successmetricsinc.UserBean user = getUserBean();
			Repository rep = user.getRepository();
			String expr = rep.isUseNewQueryLanguage() ? "["
					+ drillNodeExpression + "]" : "DC{" + drillNodeExpression
					+ "}";
			new DisplayExpression(user.getRepository(), expr, user
					.getRepSession());
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	public OMElement getDrillNode(String xmlData) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			getUserBean().getRepSession().setCancelled(false);
			XMLInputFactory inputFactory = XmlUtils.getSecureXMLInputFactory();
			XMLStreamReader parser = inputFactory
					.createXMLStreamReader(new StringReader(xmlData));

			// true means requested column is drill from
			// false means requested column is drill to
			boolean isColumnDrillFrom = false;
			String column = null;
			String drillRowId = null;
			String drillMapId = null;
			String columnLabel = null;
			String headerLabel = null;
			for (XMLStreamIterator iter = new XMLStreamIterator(parser); iter
					.hasNext();) {
				String elName = parser.getLocalName();
				if ("DrillMap".equals(elName)) {
					drillMapId = XmlUtils.getStringContent(parser);
				} else if ("DrillColumnFrom".equals(elName)) {
					isColumnDrillFrom = XmlUtils.getBooleanContent(parser);
				} else if ("Column".equals(elName)) {
					column = XmlUtils.getStringContent(parser);
				} else if ("DrillRowId".equals(elName)) {
					drillRowId = XmlUtils.getStringContent(parser);
				} else if ("Label".equals(elName)) {
					columnLabel = XmlUtils.getStringContent(parser);
				} else if ("HeaderLabel".equals(elName)) {
					headerLabel = XmlUtils.getStringContent(parser);
				}

			}
			// always a dimension column
			Column node = AdhocReportHelper.getColumn(column, null, null);
			DrillColumn dc = new DrillColumn(node.getDimension(), node
					.getName());
			if (columnLabel != null && columnLabel.trim().length() > 0) {
				dc.setLabel(columnLabel);
			} else {
				dc.setLabel(node.getName());
			}

			if (headerLabel != null && headerLabel.trim().length() > 0) {
				dc.setHeaderLabel(headerLabel);
			} else {
				dc.setHeaderLabel(node.getName());
			}
			OMFactory fac = OMAbstractFactory.getOMFactory();
			OMNamespace ns = null;
			OMElement parent = fac.createOMElement("DrillNodeDetails", ns);
			boolean checkForDuplicates = false;
			if (isColumnDrillFrom) {
				String drillFromDimension = dc.getDimension();
				String drillFromColumn = dc.getColumn();
				DrillMapList dMapList = getUserBean().getDrillMaps();
				DrillMap existingDrillMap = dMapList.getDrillMapWithDrillFrom(
						drillFromDimension, drillFromColumn, drillRowId);
				// if the drill map is the same as the incoming one. bypass
				// since it is already taken care on the client side
				if (checkForDuplicates
						&& existingDrillMap != null
						&& (drillMapId == null || !drillMapId
								.equalsIgnoreCase(existingDrillMap.getId()))) {
					result
							.setErrorCode(BaseException.ERROR_DRILL_ROW_DUPLICATE_DRILL_FROM);
					result.setErrorMessage("Drill Map : "
							+ existingDrillMap.getName());
				} else {
					XmlUtils.addContent(fac, parent, DrillRow.DRILL_FROM_LABEL,
							dc.getLabel(), ns);
					XmlUtils.addContent(fac, parent,
							DrillRow.DRILL_FROM_HEADER_LABEL, dc
									.getHeaderLabel(), ns);
					XmlUtils.addContent(fac, parent,
							DrillRow.DRILL_FROM_DIMENSION, drillFromDimension,
							ns);
					XmlUtils.addContent(fac, parent,
							DrillRow.DRILL_FROM_COLUMN, drillFromColumn, ns);
					XmlUtils
							.addContent(
									fac,
									parent,
									DrillRow.DRILL_FROM_COLUMN_TYPE,
									DrillRow
											.getTypeString(DrillRow.Type.Dimension),
									ns);
				}
			} else {
				dc.addContent(fac, parent, ns);
			}

			/*
			 * Column node = AdhocReportHelper.getColumn(column); DrillColumn dc
			 * = new DrillColumn(node.getDimension(), node.getName()); OMFactory
			 * fac = OMAbstractFactory.getOMFactory(); OMNamespace ns = null;
			 * OMElement parent = fac.createOMElement("DrillNodeDetails", ns);
			 * dc.addContent(fac, parent, ns);
			 */

			result.setResult(new XMLWebServiceResult(parent));
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();

	}

	public OMElement getSavedExpression(String name) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			com.successmetricsinc.UserBean ub = getUserBean();
			Repository rep = ub.getRepository();
			SavedExpressions savedExpressions = rep.getSavedExpressions();
			SavedExpressions.Expression exp = savedExpressions == null ? null
					: savedExpressions.get(name);

			if (exp != null) {
				OMFactory fac = OMAbstractFactory.getOMFactory();
				OMNamespace ns = null;
				OMElement parent = fac.createOMElement("SavedExpressions", ns);
				exp.addContent(fac, parent, ns);
				result.setResult(new XMLWebServiceResult(parent));
			} else {
				result.setErrorMessage("Saved expression " + name
						+ " not found.");
				result
						.setErrorCode(BaseException.ERROR_SAVED_EXPRESSION_NOT_FOUND);
			}
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	public OMElement saveSavedExpression(String savedExpressionStr) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			SMIWebUserBean ub = getUserBean();
			Repository rep = ub.getRepository();

			SavedExpressions savedExp = rep.getSavedExpressions();

			SavedExpressions.Expression exp = savedExp.new Expression();
			XMLInputFactory inputFactory = XmlUtils.getSecureXMLInputFactory();
			XMLStreamReader parser = inputFactory
					.createXMLStreamReader(new StringReader(savedExpressionStr));

			exp.parseElement(parser);

			setSessionObject(SubjectAreaWS.CACHE_NODE_KEY, null);
			savedExp.updateSavedExpression(exp, ub.getUserName());
			rep.clearSavedExpressions();
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	public OMElement deleteSavedExpression(String name) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			com.successmetricsinc.UserBean ub = getUserBean();
			Repository rep = ub.getRepository();

			SavedExpressions savedExpressions = rep.getSavedExpressions();

			SavedExpressions.Expression exp = savedExpressions == null ? null
					: savedExpressions.get(name);

			if (exp == null) {
				result.setErrorMessage("Saved expression " + name
						+ " not found.");
				result
						.setErrorCode(BaseException.ERROR_SAVED_EXPRESSION_NOT_FOUND);
			} else {
				savedExpressions.delete(name);
				setSessionObject(SubjectAreaWS.CACHE_NODE_KEY, null);

			}
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	public OMElement validateSavedExpressionName(String name) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			com.successmetricsinc.UserBean ub = getUserBean();
			Repository rep = ub.getRepository();

			SavedExpressions savedExpressions = rep.getSavedExpressions();

			SavedExpressions.Expression exp = savedExpressions == null ? null
					: savedExpressions.get(name);

			if (exp != null) {
				result.setErrorMessage("Saved expression " + name
						+ " already exists.");
				result
						.setErrorCode(BaseException.ERROR_SAVED_EXPRESSION_DUPLICATE_NAME);
			}
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	public OMElement applyStyle(String xmlData, String styleFileName) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			AdhocReport report = AdhocReport.convert(xmlData, null, null);
			SMIWebUserBean ub = getUserBean();
			CatalogManager cm = ub.getCatalogManager();
			String root = cm.getCatalogRootPath();
			AdhocReport styleReport = AdhocReport.read(new File(root + "/"
					+ styleFileName));

			if (report != null && styleReport != null)
				report.applyStyle(styleReport);

			result.setResult(report);
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	public OMElement createTrellisChart(String nodeString, String xmlData) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			AdhocReport report = AdhocReport.convert(xmlData, null, null);

			Document node = DocumentHelper.parseText(nodeString);
			Element root = node.getRootElement();
			Attribute type = root.attribute("t");
			Column col = null;
			Attribute val = root.attribute("v");
			if ("om".equalsIgnoreCase(type.getValue())) {
				Attribute dim = root.attribute("od");
				Attribute hier = root.attribute("oh");
				col = AdhocReportHelper.getColumn(val.getValue(), dim
						.getValue(), hier.getValue());
			} else {
				col = AdhocReportHelper.getColumn(val.getValue(), null, null);
			}
			Attribute labela = root.attribute("l");
			String label = null;
			if (labela != null)
				label = labela.getValue();
			if (label == null) {
				label = val.getValue();
				int index = label.lastIndexOf('.');
				if (index > 0) {
					label = label.substring(index);
				}
			}
			int columnIdx = AdhocReportHelper.addColumnToAdhocReport(report,
					col, false, true, null); // get or create a column

			AdhocChart chart = report.getDefaultChart();
			if (chart != null) {
				chart.transformToTrellis(report, node.getName(), columnIdx);
			}
			result.setResult(report);
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	public OMElement getFilterValuesForPaging(String filter) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			QueryFilter f = new QueryFilter();
			XMLInputFactory inputFactory = XmlUtils.getSecureXMLInputFactory();
			XMLStreamReader parser = inputFactory
					.createXMLStreamReader(new StringReader(filter));
			f.parseElement(parser);
			Prompt p = new Prompt(f);
			JasperDataSourceProvider jdsp = SMIWebUserBean.getUserBean()
					.getJasperDataSourceProvider();
			jdsp = new JasperDataSourceProvider(jdsp);
			p.createQueryWithHierarchicalFilters(ServerContext.getRepository(),
					new ArrayList<Prompt>());
			p.fillQuery(jdsp, new HashMap<String, String>(),
					new HashMap<String, Object>());
			OMFactory factory = OMAbstractFactory.getOMFactory();
			result.setResult(new XMLWebServiceResult(getFilledOptionValue(p,
					factory, null)));
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

	public OMElement getOperationNames() {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			OMFactory factory = OMAbstractFactory.getOMFactory();
			Repository r = ServerContext.getRepository();
			OMNamespace ns = null;
			OMElement opName = factory.createOMElement("Operations", ns);
			if (r != null) {
				Operation[] ops = r.getOperations();
				if (ops != null) {
					for (int i = 0; i < ops.length; i++) {
						OMElement op = factory.createOMElement("Operation", ns);
						XmlUtils.addContent(factory, op, "string", ops[i]
								.getName(), ns);
						opName.addChild(op);
					}
				}
			}
			result.setResult(new XMLWebServiceResult(opName));
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}

}
