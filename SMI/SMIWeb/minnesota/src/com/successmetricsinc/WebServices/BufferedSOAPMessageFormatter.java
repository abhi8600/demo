package com.successmetricsinc.WebServices;

import java.io.OutputStream;
import java.io.StringWriter;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMOutputFormat;
import org.apache.axiom.om.impl.MIMEOutputUtils;
import org.apache.axiom.om.util.UUIDGenerator;
import org.apache.axiom.soap.SOAPBody;
import org.apache.axis2.AxisFault;
import org.apache.axis2.Constants;
import org.apache.axis2.context.MessageContext;
import org.apache.axis2.transport.http.SOAPMessageFormatter;
import org.apache.axis2.util.JavaUtils;
import org.apache.axis2.util.Utils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.successmetricsinc.util.BaseException;

public class BufferedSOAPMessageFormatter extends SOAPMessageFormatter {
	private static final Log log = LogFactory.getLog(SOAPMessageFormatter.class);
	
	@Override
	public void writeTo(MessageContext msgCtxt, OMOutputFormat format,
			OutputStream out, boolean preserve) throws AxisFault {
		if (log.isDebugEnabled()) {
			log.debug("start writeTo()");
			log.debug("  preserve=" + preserve);
			log.debug("  isOptimized=" + format.isOptimized());
			log.debug("  isDoingSWA=" + format.isDoingSWA());
		}
		OMElement element = msgCtxt.getEnvelope();

		int optimizedThreshold = Utils.getMtomThreshold(msgCtxt);       
		if(optimizedThreshold > 0){
			if(log.isDebugEnabled()){
				log.debug("Setting MTOM optimized Threshold Value on OMOutputFormat");
			}
			format.setOptimizedThreshold(optimizedThreshold);
		}	        

		//Change from original code where we write as we go instead
		//of all at once from the buffer.
		StringBuffer mainBuffer = new StringBuffer();
		try {
			if (!(format.isOptimized()) & format.isDoingSWA()) {
				StringWriter bufferedSOAPBody = new StringWriter();
				if (preserve) {
					element.serialize(bufferedSOAPBody, format);
				} else {
					element.serializeAndConsume(bufferedSOAPBody, format);
				}
				writeSwAMessage(msgCtxt, bufferedSOAPBody, out, format);
			} else {
				StringWriter buffer = new StringWriter();
				if (preserve) {
					element.serialize(buffer, format);
				} else {
					element.serializeAndConsume(buffer, format);
				}
				mainBuffer.append(buffer);
			}

			//Write out buffer
			out.write(mainBuffer.toString().getBytes("UTF-8"));
			out.flush();
		} catch (XMLStreamException e) {
			SOAPBody body = msgCtxt.getEnvelope().getBody();

			//Find the BirstWebserviceResult part
			OMElement el = body.getFirstElement();
			while(el != null){
				if (el.getLocalName().equals(BirstWebServiceResult.class.getName())){
					break;
				}
				el = el.getFirstElement();
			}
			
			//Found it, clean it up
			if(el != null){
				OMElement errCode = el.getFirstChildWithName(new QName("", BirstWebServiceResult.ERR_CODE));
				if(errCode != null){
					errCode.setText(Integer.toString(BaseException.ERROR_XML_PARSE));
				}
				OMElement errMsg = el.getFirstChildWithName(new QName("", BirstWebServiceResult.ERR_MSG));
				if(errMsg != null){
					errMsg.setText("The processed data was invalid");
				}
				
				//Remove the result where the problem is
				OMElement result = el.getFirstChildWithName(new QName("", BirstWebServiceResult.RESULT));
				if(result != null){
					result.detach();
				}
			}
			
			//re-write to the stream (this is what the original code does)
			try{
				if (preserve) {
					element.serialize(out, format);
				} else {
					element.serializeAndConsume(out, format);
				}
			}catch (Exception ex){
				throw AxisFault.makeFault(ex);
			}
			
			throw AxisFault.makeFault(e);
		} catch(java.io.IOException e){
			throw AxisFault.makeFault(e);
		}finally{ 
			if (log.isDebugEnabled()) {
				log.debug("end writeTo()");
			}
		}
		
		
	}
	
	//Copied verbatim from  Axis
	private void writeSwAMessage(MessageContext msgCtxt,
			StringWriter bufferedSOAPBody, OutputStream outputStream,
			OMOutputFormat format) {
		if (log.isDebugEnabled()) {
			log.debug("start writeSwAMessage()");
		}
		Object property = msgCtxt
		.getProperty(Constants.Configuration.MM7_COMPATIBLE);
		boolean MM7CompatMode = false;
		if (property != null) {
			MM7CompatMode = JavaUtils.isTrueExplicitly(property);
		}
		if (!MM7CompatMode) {
			MIMEOutputUtils.writeSOAPWithAttachmentsMessage(bufferedSOAPBody,
					outputStream,
					msgCtxt.getAttachmentMap(), format);
		} else {
			String innerBoundary;
			String partCID;
			Object innerBoundaryProperty = msgCtxt
			.getProperty(Constants.Configuration.MM7_INNER_BOUNDARY);
			if (innerBoundaryProperty != null) {
				innerBoundary = (String) innerBoundaryProperty;
			} else {
				innerBoundary = "innerBoundary"
				+ UUIDGenerator.getUUID().replace(':', '_');
			}
			Object partCIDProperty = msgCtxt
			.getProperty(Constants.Configuration.MM7_PART_CID);
			if (partCIDProperty != null) {
				partCID = (String) partCIDProperty;
			} else {
				partCID = "innerCID"
					+ UUIDGenerator.getUUID().replace(':', '_');
			}
			MIMEOutputUtils.writeMM7Message(bufferedSOAPBody, outputStream,
					msgCtxt.getAttachmentMap(), format, partCID,
					innerBoundary);
		}
		if (log.isDebugEnabled()) {
			log.debug("end writeSwAMessage()");
		}
	}

}
