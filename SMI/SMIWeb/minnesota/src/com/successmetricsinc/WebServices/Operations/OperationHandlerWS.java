package com.successmetricsinc.WebServices.Operations;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMException;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.apache.log4j.Logger;

import com.successmetricsinc.Repository;
import com.successmetricsinc.User;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.WebServices.BirstWebService;
import com.successmetricsinc.WebServices.BirstWebServiceResult;
import com.successmetricsinc.WebServices.StringWebServiceResult;
import com.successmetricsinc.adhoc.AdhocReportHelper;
import com.successmetricsinc.engine.operation.Operation;
import com.successmetricsinc.engine.operation.Parameter;
import com.successmetricsinc.engine.operation.Parameter.ParameterType;
import com.successmetricsinc.query.Filter;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.UserNotLoggedInException;
import com.successmetricsinc.util.XmlUtils;

public class OperationHandlerWS extends BirstWebService {

	static Logger logger = Logger.getLogger(OperationHandlerWS.class);
	
	@SuppressWarnings("unchecked")
	public OMElement performOperation(String xmlData) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean ub = SMIWebUserBean.getUserBean();
			Repository r = ub.getRepository();
			User user = ub.getUser();
			
			String operationName = null;
			Map<String, List<Object>> paramMap = new HashMap<String, List<Object>>();
			List<Filter> filterParams = null;
			// parse xmlData to find operation name and parameters			
			XMLInputFactory inputFactory = XmlUtils.getSecureXMLInputFactory();
			XMLStreamReader parser = inputFactory.createXMLStreamReader(new StringReader(xmlData));

			StAXOMBuilder builder = new StAXOMBuilder(parser);
			OMElement doc = null;
			doc = builder.getDocumentElement();
			if (doc != null) {
				doc.build();
				try {
					doc.detach();
				}
				catch (OMException ome) {}
				for (Iterator<OMElement> iter = doc.getChildElements(); iter.hasNext(); ) {
					OMElement el = iter.next();
					String elName = el.getLocalName();
					if ("OperationName".equals(elName)) {
						operationName = XmlUtils.getStringContent(el);						
					}
					if ("Parameter".equals(elName)) {
						String paramName = null;
						List<Object> paramValues = new ArrayList<Object>();
						for (Iterator<OMElement> children = el.getChildElements(); children.hasNext(); ) {
							OMElement child = children.next();
							String childName = child.getLocalName();
							if ("ParameterName".equals(childName)) {
								paramName = XmlUtils.getStringContent(child);									
							}							
							if ("ParameterValues".equals(childName)) {
								for (Iterator<OMElement> values = child.getChildElements(); values.hasNext(); ) {
									OMElement ch = values.next();
									String chName = ch.getLocalName();
									if ("ParameterValue".equals(chName)) {
										paramValues.add(XmlUtils.getStringContent(ch));						
									}
								}
							}						
						}
						paramMap.put(paramName, paramValues);
					}
					if("PromptXML".equals(elName))
					{
						if(el.getFirstElement() != null)
						{
							String promptXML = el.getFirstElement().toString();
							filterParams = AdhocReportHelper.parsePromptData(promptXML);	
						}
					}
				}
			}
	        if(operationName == null)
	        {
	        	throw new Exception("Parameter OperationName not provided.");
	        }
	        
	        logger.debug("Invoking operation " + operationName);
	        Operation operation = r.getOperation(operationName);
	        if(operation == null)
	        {
	        	throw new Exception("Operation " + operationName + " not defined in repository.");
	        }
	        operation.setUser(user);
	        Map<String, String> servletParameterMap = new HashMap<String, String> ();
	        operation.setServletParameterMap(servletParameterMap);
	        Parameter [] parameters = operation.getParameters();
	        
	        if((parameters != null) && (parameters.length > 0))
	        {
	        	for(Parameter p : parameters)
	        	{
	        		String name = p.getName();
	        		if (paramMap.containsKey(name))
	        		{
	        			if (p.getType() == ParameterType.SingleValue)
	        			{
	        				if (paramMap.get(name).size() >= 1)
	        					operation.setParameterValue(name, paramMap.get(name).get(0));	        				
	        			}
	        			else
	        			{
	        				operation.setParameterValue(name, paramMap.get(name));
	        			}
	        		}
	        		else if(filterParams != null)
	        		{
	        			Filter f = null;
	        			for (Filter filter : filterParams) {
	        				if (filter.getColumnName() != null && filter.getColumnName().equals(name)) {
	        					f = filter;
	        					break;
	        				}
	        			}

	        			if (f != null) {
		        			List<Object> valuelist = new ArrayList<Object>();
		        			valuelist.addAll(f.getValues());						
		        			if (p.getType() == ParameterType.SingleValue)
		        			{
		        				if (valuelist.size() >= 1)
		        					operation.setParameterValue(name, valuelist.get(0));	        				
		        			}
		        			else
		        			{
		        				operation.setParameterValue(name, valuelist);
		        			}
	        			}
	        		}
	        		else
	        			logger.debug("Parameter " + name + " not found for operation " + operationName);
	        	}
	        }
	        
	        int returnCode = operation.execute(r, ub.getCache(), ub.getRepSession());
	        if (returnCode >= 0)
	        	result.setResult(new StringWebServiceResult("<ReturnCode>" + returnCode + "</ReturnCode>"));
	        else
	        {
	        	result.setErrorCode(BaseException.ERROR_DATA_UNAVAILABLE);
	        	result.setErrorMessage("Operation failed to perform.");
	        }
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();		
	}
}
