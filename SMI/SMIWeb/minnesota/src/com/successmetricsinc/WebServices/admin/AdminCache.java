package com.successmetricsinc.WebServices.admin;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.apache.log4j.Logger;

import com.successmetricsinc.Prompt;
import com.successmetricsinc.UI.admin.system.Cache;
import com.successmetricsinc.WebServices.BirstWebService;
import com.successmetricsinc.WebServices.BirstWebServiceResult;
import com.successmetricsinc.WebServices.StringWebServiceResult;
import com.successmetricsinc.WebServices.adhoc.AdhocWS;
import com.successmetricsinc.WebServices.dashboard.DashboardMongoDBCache;
import com.successmetricsinc.WebServices.dashboard.DashboardWebService;
import com.successmetricsinc.query.CacheOperations;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.XmlUtils;

public class AdminCache {

	public static final Logger logger = Logger.getLogger(AdminWS.class);	
	
	public OMElement clearCacheForQuery(String query, boolean exactMatch,
			boolean reseed, boolean allServers, BirstWebServiceResult result) {
		try {
			Cache cache = new Cache();
			cache.setQuery(query);
			cache.setExactMatch(exactMatch);
			cache.setReseed(reseed);
			cache.setClearForAllServers(allServers);
			
			cache.clearForQuery();
			List<String> results = cache.getOperationOutput();
			boolean success = isOperationSuccessful(results, allServers);
			if (success) {
				String resultsLogString = formatCacheOutput(results, allServers);
				result.setResult(new StringWebServiceResult(resultsLogString));
			} else {
				result.setErrorCode(BaseException.ERROR_OTHER);
				result.setErrorMessage("Clear Cache for Query Failed");
				String errorLogString = formatErrors(results, allServers);
				result.setResult(new StringWebServiceResult(errorLogString));
			}
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	
	public OMElement clearCacheForLogicalTable(String tableName, boolean allServers, BirstWebServiceResult result) {
		try {
			Cache cache = new Cache();
			cache.setQuery(tableName);
			cache.setClearForAllServers(allServers);
			
			cache.clearForLogical();
			List<String> results = cache.getOperationOutput();
			boolean success = isOperationSuccessful(results, allServers);
			if (success) {
				String resultsLogString = formatCacheOutput(results, allServers);
				result.setResult(new StringWebServiceResult(resultsLogString));
			} else {
				result.setErrorCode(BaseException.ERROR_OTHER);
				result.setErrorMessage("Clear Cache for Logical Table Failed");
				String errorLogString = formatErrors(results, allServers);
				result.setResult(new StringWebServiceResult(errorLogString));
			}
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	
	public OMElement clearCacheForPhysicalTable(String tableName, boolean allServers, BirstWebServiceResult result) {

		try {
			Cache cache = new Cache();
			cache.setQuery(tableName);
			cache.setClearForAllServers(allServers);
			
			cache.clearForPhysical();
			List<String> results = cache.getOperationOutput();
			boolean success = isOperationSuccessful(results, allServers);
			if (success) {
				String resultsLogString = formatCacheOutput(results, allServers);
				result.setResult(new StringWebServiceResult(resultsLogString));
			} else {
				result.setErrorCode(BaseException.ERROR_OTHER);
				result.setErrorMessage("Clear Cache for Physical Table Failed");
				String errorLogString = formatErrors(results, allServers);
				result.setResult(new StringWebServiceResult(errorLogString));
			}
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement clearAllCaches(boolean allServers, BirstWebServiceResult result) {
		try {			
			Cache cache = new Cache();
			cache.setClearForAllServers(allServers);
			
			cache.clearForAll();
			List<String> results = cache.getOperationOutput();
			boolean success = isOperationSuccessful(results, allServers);
			if (success) {
				String resultsLogString = formatCacheOutput(results, allServers);
				result.setResult(new StringWebServiceResult(resultsLogString));
			} else {
				result.setErrorCode(BaseException.ERROR_OTHER);
				result.setErrorMessage("Clear All Caches Failed");
				String errorLogString = formatErrors(results, allServers);
				result.setResult(new StringWebServiceResult(errorLogString));
			}
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement clearDashboards(boolean allServers,
			BirstWebServiceResult result) {
		try {
			DashboardMongoDBCache.createMarkerFile(BirstWebService.getUserBean());
			logger.info("Dashboard cache cleared.");
			ArrayList<String> results = new ArrayList<String>();
			results.add("Dashboards cache will be recreated at the next dashboards access");
			String resultsLogString = formatCacheOutput(results, false);
			result.setResult(new StringWebServiceResult(resultsLogString));
		} catch (Exception e) {
			result.setException(e);
			return result.toOMElement();
		}
		return result.toOMElement();
	}
	
	public OMElement reloadContext(BirstWebServiceResult result) {

		try {
			Cache cache = new Cache();
			cache.reloadContext();
			List<String> results = cache.getOperationOutput();
			boolean success = isOperationSuccessful(results, false);
			if (success) {
				String resultsLogString = formatCacheOutput(results, false);
				result.setResult(new StringWebServiceResult(resultsLogString));
			} else {
				result.setErrorCode(BaseException.ERROR_OTHER);
				result.setErrorMessage("Clear Reload Context Failed");
				String errorLogString = formatErrors(results, false);
				result.setResult(new StringWebServiceResult(errorLogString));
			}
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	@SuppressWarnings("unchecked")
	public void seedCache(boolean dashboards, boolean saveReportXmls)
	{
		if (dashboards)
		{
			DashboardWebService dws = new DashboardWebService();
			OMElement doc = dws.getDashboardList();
			if (doc != null)
			{
				for (Iterator<OMElement> iter = doc.getChildElements(); iter.hasNext(); ) 
				{
					OMElement resultEl = iter.next();
					if (resultEl.getLocalName().equals("Result"))
					{
						for (Iterator<OMElement> iter2 = resultEl.getChildElements(); iter2.hasNext(); )
						{
							OMElement resultClildEl = iter2.next();
							for (Iterator<OMElement> iter3 = resultClildEl.getChildElements(); iter3.hasNext(); )
							{
								OMElement resultClildEl2 = iter3.next();
								if (resultClildEl2.getLocalName().equals("Result"))
								{
									for (Iterator<OMElement> iter4 = resultClildEl2.getChildElements(); iter4.hasNext(); )
									{
										OMElement dashboardsEl = iter4.next();
										for (Iterator<OMElement> iter5 = dashboardsEl.getChildElements(); iter5.hasNext(); )
										{
											OMElement dashboardEl = iter5.next();
											String dashName = dashboardEl.getAttributeValue(new QName("name"));
											for (Iterator<OMElement> iter6 = dashboardEl.getChildElements(); iter6.hasNext(); )
											{
												OMElement pageEl = iter6.next();
												if (pageEl.getLocalName().equals("Page"))
												{
													String pageName = pageEl.getAttributeValue(new QName("name"));
													String pageUUID = pageEl.getAttributeValue(new QName("uuid"));
													String pageDir = pageEl.getAttributeValue(new QName("dir"));
													String pagePath = pageDir + "/" + pageName + ".page";
													String guid = System.currentTimeMillis() + "_" + pageUUID;
													String prompts = "<Prompts />";
													int dashletCount = 0;
													for (Iterator<OMElement> iter7 = pageEl.getChildElements(); iter7.hasNext(); )
													{
														OMElement innerEl = iter7.next();
														if (innerEl.getLocalName().equals("Prompts"))
														{
															prompts = innerEl.toString();
															prompts = convertDefaultOptionToSelectedValue(prompts);
															AdhocWS adhocWS = new AdhocWS();
															adhocWS.getFilledOptions(prompts);
														}
														else if (innerEl.getLocalName().equals("Layout"))
														{
															dashletCount = seedCacheForLayoutObject(innerEl, prompts, pagePath, guid, saveReportXmls, dashName, pageName, dashletCount);													
														}
													}											
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}				
		}		
	}
	
	@SuppressWarnings("unchecked")
	private int seedCacheForLayoutObject(OMElement el, String prompts, String pagePath, String guid, boolean saveReportXmls, String dashName, String pageName, int dashletCount)
	{
		for (Iterator<OMElement> iter = el.getChildElements(); iter.hasNext(); )
		{
			OMElement innerEl = iter.next();
			if (innerEl.getLocalName().equals("Layout"))//nested layouts
			{
				dashletCount = seedCacheForLayoutObject(innerEl, prompts, pagePath, guid, saveReportXmls, dashName, pageName, dashletCount);
			}
			else if (innerEl.getLocalName().equals("dashlet"))
			{
				String dashboardName = null;
				boolean applyAllPrompts = false;
				for (Iterator<OMElement> iter2 = innerEl.getChildElements(); iter2.hasNext(); )
				{
					OMElement dashletInnerEl = iter2.next();
					if (dashletInnerEl.getLocalName().equals("Path"))
					{
						dashboardName = dashletInnerEl.getText();
					}
					else if (dashletInnerEl.getLocalName().equals("ApplyAllPrompts"))
					{
						applyAllPrompts = Boolean.parseBoolean(dashletInnerEl.getText());
					}
				}
				AdhocWS adhocWS = new AdhocWS();
				//result.add(adhocWS.getDashboardRenderedReport(dashboardName, prompts, 0, applyAllPrompts, pagePath, guid, false));
				//rptNameLst.add(dashName + "_" + pageName + "_" + dashletCount++);
				OMElement dashletXML = adhocWS.getDashboardRenderedReportWithLogging(dashboardName, prompts, 0, applyAllPrompts, pagePath, guid, false, false);
				if (saveReportXmls)
				{
					//need to save generated dashletXML under tomcat\temp\ATC
					try
					{
						String dashletName = dashName + "_" + pageName + "_" + dashletCount++;
						File fiTemp = File.createTempFile("tempDir", "");
						File ATC_Dir = new File (fiTemp.getParent() + "\\ATC");
						ATC_Dir.mkdir();
						File fi = new File (ATC_Dir + "\\" + dashletName + ".xml");		
						PrintWriter pw = new PrintWriter(fi);
						pw.write(dashletXML.toString());
						pw.flush();
						pw.close();
						fiTemp.delete();
					}
					catch (IOException ex)
					{
						logger.error(ex.getMessage(), ex);
					}
				}
			}
		}
		return dashletCount;
	}
	
	private String convertDefaultOptionToSelectedValue(String prompts)
	{
		try
		{
			if (prompts != null) {
				XMLInputFactory inputFactory = XmlUtils.getSecureXMLInputFactory();
				XMLStreamReader parser = inputFactory.createXMLStreamReader(new StringReader(prompts));

				StAXOMBuilder builder = new StAXOMBuilder(parser);
				OMElement doc = null;
				doc = builder.getDocumentElement();
				doc.build();
				doc.detach();
				
				OMElement replacedPromptsXML = XmlUtils.convertToOMElement("<Prompts />");
				
				for (@SuppressWarnings("unchecked")
				Iterator<OMElement> iter = doc.getChildElements(); iter.hasNext(); ) {
					OMElement el = iter.next();
					Prompt prompt = new Prompt();
					prompt.parseElement(el);
					List<String> selectedValues = prompt.getSelectedValues();
					if ((selectedValues != null) && (selectedValues.isEmpty()))
					{
						String defaultOption = prompt.getDefaultOption();
						if (defaultOption != null && !defaultOption.isEmpty())
						{
							selectedValues.add(defaultOption);							
						}
					}
					prompt.addContent(replacedPromptsXML, el.getOMFactory(), el.getNamespace());
				}
				return replacedPromptsXML.toString();
			}
		}
		catch (Exception e)
		{	
			logger.error("Exception in SeedCacheOperation for converting Prompt's DefaultOption To SelectedValue", e);
		}
		return prompts;
	}
	
	private boolean isOperationSuccessful(List<String> results, boolean allServers){
		if(results == null || results.isEmpty()){
			results = (results==null)? new ArrayList<String>() : results;
			results.add("No Results Obtained from operation. Operation possibly did not run");
			return false;
		}		
		
		if(!allServers){
			return isOperationSuccessfulLocalServer(results);
		}
		else{
			return isOperationSuccessfulRemoteServers(results);
		}		
		
	}
	
	private boolean isOperationSuccessfulLocalServer(List<String> results){
		
		boolean success = true;
		for(int i=0; i < results.size(); i++){
			if(results.get(i).startsWith("Error")){
				success = false;
			}
		}
		
		return success;
	}
	
	private boolean isOperationSuccessfulRemoteServers(List<String> results){
		
		boolean success = true;
		for(int i=0; i < results.size(); i++){
			String res = results.get(i).replaceAll("&lt;","<");
			if(res.contains("<CacheLogError>")){
				success = false;
				break;
			}
		}
		
		return success;
	}
	
	protected String formatCacheOutput(List<String> results, boolean allServers){
		if(!allServers){
			return formatCacheOutputLocalServer(results);
		}
		else{
			return formatCacheOutputRemoteServers(results);
		}
	}
	private String formatCacheOutputLocalServer(List<String> results){
		
	    StringBuilder builder = new StringBuilder();
	    builder.append("<CacheOutput>");
	    
	    for(int i=0; i<results.size(); i++){
	    	builder.append("<CacheLogOutput>");
	    	builder.append(results.get(i));
	    	builder.append("</CacheLogOutput>");
	    }
	    
	    builder.append("</CacheOutput>");
		return builder.toString();
	}
	
	private String formatCacheOutputRemoteServers(List<String> results){
		
		StringBuilder builder = new StringBuilder();
	    builder.append("<CacheOutput>");	    
	    
	    for(int i=0; i<results.size(); i++){
	    	if(results.get(i).contains("Response from " + CacheOperations.DELIMITER_BEGIN)){
	    		String serverOutput = results.get(i);
	    		int nameBeginIndex = serverOutput.indexOf(CacheOperations.DELIMITER_BEGIN)
						+ CacheOperations.DELIMITER_BEGIN.length();
	    		int nameEndIndex = serverOutput.indexOf(CacheOperations.DELIMITER_END);
	    		if(nameBeginIndex <0 || nameEndIndex <0){
	    			logger.error("Unable to find serverURL from output for delimiter " + 
	    					CacheOperations.DELIMITER_BEGIN +  ":" + serverOutput );
	    			continue;
	    		}
	    		
	    		serverOutput = serverOutput.replaceAll("&lt;","<");
	    		String startCacheOutputTag = "<CacheOutput>";
	    		String endCacheOutputTag = "</CacheOutput>";
	    		int outputBeginIndex = serverOutput.indexOf(startCacheOutputTag);
	    		int outputEndIndex = serverOutput.indexOf(endCacheOutputTag);	    		
	    		
	    		if(outputBeginIndex <0 || outputEndIndex <0){
	    			logger.error("Unable to find serverUrl from CacheOutput tag: " + serverOutput );	    			
	    			continue;
	    		}
	    		
	    		builder.append("<Server>");	    		
	    		builder.append("<name>");
	    		String serverURL = serverOutput.substring(nameBeginIndex, nameEndIndex);
	    		builder.append(serverURL);
	    		builder.append("</name>");	    		
	    		serverOutput = serverOutput.substring(outputBeginIndex + startCacheOutputTag.length(), 
	    				outputEndIndex);	    		
	    		builder.append(serverOutput);	    		
	    		builder.append("</Server>");
	    	}	    	
	    }
	    
	    builder.append("</CacheOutput>");
		return builder.toString();
	}

	private String formatErrors(List<String> results, boolean allServers){
		if(!allServers){
			return formatErrors(results);
		}
		else{
			return formatCacheOutputRemoteServers(results);
		}
	}
	private String formatErrors(List<String> results) {
		StringBuilder builder = new StringBuilder();
	    builder.append("<CacheOutput>");
	    
	    for(int i=0; i<results.size(); i++){
	    	builder.append("<CacheLogError>");
	    	builder.append(results.get(i));
	    	builder.append("</CacheLogError>");
	    }
	    
	    builder.append("</CacheOutput>");
		return builder.toString();		
	}	
}
