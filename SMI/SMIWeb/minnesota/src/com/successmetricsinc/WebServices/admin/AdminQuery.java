package com.successmetricsinc.WebServices.admin;

import java.io.IOException;
import java.sql.Types;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;

import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.UI.admin.system.Queries;
import com.successmetricsinc.WebServices.BirstWebService;
import com.successmetricsinc.WebServices.BirstWebServiceResult;
import com.successmetricsinc.WebServices.StringWebServiceResult;
import com.successmetricsinc.WebServices.XMLWebServiceResult;
import com.successmetricsinc.query.NavigationException;
import com.successmetricsinc.query.QueryResultSet;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.XmlUtils;

public class AdminQuery {

	private static final Logger logger = Logger.getLogger(AdminQuery.class);
	private static int PAGE_SIZE = 100;
	private static String ADMIN_CACHE_MAP = "Session_QRS";
	
	public OMElement runQuery(String query, int page , boolean clearCache, BirstWebServiceResult result){		
		QueryResultSet qrs = null;
		try {
			if (clearCache) {
				qrs = getQueryResultSet(query, result);
				setQueryResultSetCache(query, qrs);

			} else {
				qrs = getQueryResultSetFromCache(query, result);
			}
			
			if (qrs != null) {
				OMElement resultElement = formatRunQueryResultSet(qrs, page);
				result.setResult(new XMLWebServiceResult(resultElement));
			} else {
				result.setErrorCode(BaseException.ERROR_OTHER);				
			}
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement navigateQuery(String query, boolean isSuperUser, BirstWebServiceResult result) {
		
		try {			
			Queries queries = new Queries();
			queries.setQuery(query);
			queries.navigate(isSuperUser);
			String text = queries.getText();
			boolean validResult = queries.getValidResults();
			if (text != null && validResult) {
				String resultSetString = formatOutputText(text);
				result.setResult(new StringWebServiceResult(resultSetString));
			} else {
				logger.error("text returned from Queries is null");
				result.setErrorCode(BaseException.ERROR_OTHER);
				if(queries.getErrorMessage() != null){
					result.setErrorMessage(queries.getErrorMessage());
				}
			}
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement generateQuery(String query, boolean isSuperUser, BirstWebServiceResult result) {
		
		try {			
			Queries queries = new Queries();
			queries.setQuery(query);			
			queries.generate(isSuperUser);
			String text = queries.getText();
			boolean validResult = queries.getValidResults();
			if (text != null && validResult) {
				String resultSetString = formatOutputText(text);
				result.setResult(new StringWebServiceResult(resultSetString));
			} else {
				if(queries.getErrorCode() != BaseException.SUCCESS)
				{
					result.setErrorCode(queries.getErrorCode());
				}
				else
				{
					result.setErrorCode(BaseException.ERROR_OTHER);
				}
				if(queries.getErrorMessage() != null){
					result.setErrorMessage(queries.getErrorMessage());
				}
			}
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	private String formatOutputText(String text){
	    StringBuilder builder = new StringBuilder();
	    builder.append("<QueryOutput>");
	    builder.append(XmlUtils.encode(text));
	    builder.append("</QueryOutput>");
		return builder.toString();
	}
	
	private QueryResultSet getQueryResultSet(String query, BirstWebServiceResult result) throws NavigationException, IOException, Exception{		
		Queries queries = new Queries();
		queries.setQuery(query);
		queries.run();
		return queries.getResultSet();
	}
	
	@SuppressWarnings("unchecked")
	private QueryResultSet getQueryResultSetFromCache(String query, BirstWebServiceResult result) throws NavigationException, IOException, Exception{
		
		QueryResultSet qrs = null;
		HashMap<String, QueryResultSet> map = null;
		Object object = BirstWebService.getSessionObject(ADMIN_CACHE_MAP);
	    if(object != null){
	    	map = (HashMap<String, QueryResultSet>) BirstWebService.getSessionObject(ADMIN_CACHE_MAP);
			qrs = map.get(query);
			// some stale query residing. Should not happen
			if(qrs == null){
				BirstWebService.setSessionObject(ADMIN_CACHE_MAP, null);
				// caching did not work. Lets re run the query
				return getQueryResultSet(query, result);
			}
	    }	    
	    
	    return qrs;
	}
	
	
	@SuppressWarnings("unchecked")
	private void setQueryResultSetCache(String query, QueryResultSet qrs){
		HashMap<String, QueryResultSet> map = null;
		Object object = BirstWebService.getSessionObject(ADMIN_CACHE_MAP);
	    if(object != null){
	    	map = (HashMap<String, QueryResultSet>) object;
	    	map.clear();	    	
	    }else{	    	
	    	map = new HashMap<String, QueryResultSet>();
	    }
	    map.put(query, qrs);
	    BirstWebService.setSessionObject(ADMIN_CACHE_MAP, map);
	}	
	
	private OMElement formatRunQueryResultSet(QueryResultSet qrs, int page)
	{
		String[] displayNames = qrs.getDisplayNames();
		String[] columnFormats = qrs.getColumnFormats();
		int[] columnDataTypes = qrs.getColumnDataTypes();
		String[] warehouseColumnsDataTypes = qrs.getWarehouseColumnsDataTypes();
		int totalNumberRows = qrs.numRows();
		int numberOfPages =  totalNumberRows/PAGE_SIZE;
		
		if(totalNumberRows%PAGE_SIZE != 0){
			numberOfPages = numberOfPages + 1;
		}
		
		Object[][] rows = getPagedRows(qrs, page);
		StringBuilder builder = new StringBuilder();		
		String labelStart = "label-NUM";
		//String labelEnd = "</label-NUM>";		
		
		builder.append("<rows>");	
		builder.append("<labels>");
		
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;
		OMElement rowsParent = factory.createOMElement("rows",ns);
		OMElement labels = factory.createOMElement("labels", ns);
		for (int i = 0; i < displayNames.length; i++)
		{
			if (displayNames[i] != null){
				
				String label = labelStart.replaceAll("-NUM", Integer.valueOf(i).toString());	
				XmlUtils.addContent(factory, labels, label, displayNames[i], ns);
								
			}
		}
		rowsParent.addChild(labels);
		
		DecimalFormat[] formats = new DecimalFormat[columnFormats.length];
		// make life easier
		//if (separator != ',')
		//{
		for (int i = 0; i < columnFormats.length; i++) {
			try
			{
				String pattern = QueryResultSet.getFormatPattern(columnFormats[i], columnDataTypes[i]);
				if (pattern != null)
					formats[i] = new DecimalFormat(pattern);
				else
					formats[i] = new DecimalFormat();
			} 
			catch (IllegalArgumentException ex)
			{
				logger.warn("Not able to parse into decimal format: " + columnFormats[i]);
			}
		}
		//}
			
		String columnStart = "column-NUM";
		for (Object[] row : rows)
		{
			OMElement rowElement = factory.createOMElement("row",ns);
			
			for (int i = 0; i < row.length; i++)
			{
				
				String columnStartTag = columnStart.replaceAll("-NUM", Integer.valueOf(i).toString());
				String columnValue = null;
				
				if (displayNames[i] != null)
				{	
					if (row[i] != null && formats[i] != null && Number.class.isInstance(row[i]))
					{
						columnValue = formats[i].format(row[i]);
						columnValue = columnValue.replaceAll("^-(?=0(.0*)?$)", "");
					} 
					else if (row[i] != null && row[i] instanceof Calendar)
					{								
						if (warehouseColumnsDataTypes[i] != null && (warehouseColumnsDataTypes[i].equals("Date") 
								|| warehouseColumnsDataTypes[i].equals("DateTime")))
						{		
							Calendar cal = (Calendar) row[i];
							com.successmetricsinc.UserBean ub = SMIWebUserBean.getUserBean();						
							SimpleDateFormat dateFormat = null;							
							if (warehouseColumnsDataTypes[i].equals("Date"))
							{
								dateFormat = ub.getDateFormat();								
							} else
							{								
								dateFormat = ub.getDateTimeFormat();
							}
							
							columnValue = dateFormat.format(cal.getTime());
						} 								
					}
					else if (row[i] != null)
					{
						columnValue = row[i].toString();
					}
					
					XmlUtils.addContent(factory, rowElement, columnStartTag, columnValue, ns);
					
				}			
			}
			rowsParent.addChild(rowElement);			
		}
		
		XmlUtils.addContent(factory, rowsParent, "numberOfPages", numberOfPages, ns);
		XmlUtils.addContent(factory, rowsParent, "page", page, ns);		
		return rowsParent;
	}
	
	private Object[][] getPagedRows(QueryResultSet qrs, int page){
		
		int totalNumRows = qrs.numRows();
		int copyNumRows = PAGE_SIZE;
		if(totalNumRows < (page + 1)*PAGE_SIZE){
			copyNumRows = totalNumRows - (page*PAGE_SIZE);
		}
		Object[][] rows = (Object[][]) new Object[copyNumRows][];
		List<Object[]> rowList = new ArrayList<Object[]>();
		
		Object[][] allRows = qrs.getRows();		
		int copyPos = page*PAGE_SIZE;
		for (int i = 0; i < copyNumRows; i++) {
			rowList.add(allRows[copyPos + i]);			
		}
        rowList.toArray(rows);
		return rows;
	}	
}
