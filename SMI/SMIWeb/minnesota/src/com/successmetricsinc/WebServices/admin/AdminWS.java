package com.successmetricsinc.WebServices.admin;

import java.util.List;

import javax.jws.WebService;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;

import com.successmetricsinc.Group;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.WebServices.BirstWebService;
import com.successmetricsinc.WebServices.BirstWebServiceResult;
import com.successmetricsinc.WebServices.XMLWebServiceResult;
import com.successmetricsinc.util.UserNotLoggedInException;
import com.successmetricsinc.util.UserOperationNotAuthorizedException;
import com.successmetricsinc.util.XmlUtils;

@WebService( serviceName="Admin")
public class AdminWS extends BirstWebService{

	public static final Logger logger = Logger.getLogger(AdminWS.class);
		
	// Query Operations
	public OMElement runQuery(String query, int page , boolean clearCache){		
		BirstWebServiceResult result = new BirstWebServiceResult();
		
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			
			SMIWebUserBean ub = SMIWebUserBean.getUserBean();		
			if (!ub.isSuperUserOwnerOrAdmin())
			{
				throw new UserOperationNotAuthorizedException("runQuery : User not authorized to execute the operation");
			}
			AdminQuery adminQuery = new AdminQuery();			
			return adminQuery.runQuery(query, page, clearCache, result);
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement navigateQuery(String query, boolean isSuperUser) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean ub = SMIWebUserBean.getUserBean();		
			if (!ub.isSuperUserOwnerOrAdmin())
			{
				throw new UserOperationNotAuthorizedException("navigateQuery : User not authorized to execute the operation");
			}
			AdminQuery adminQuery = new AdminQuery();
			return adminQuery.navigateQuery(query, isSuperUser, result);
		}catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement generateQuery(String query, boolean isSuperUser) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean ub = SMIWebUserBean.getUserBean();		
			if (!ub.isSuperUserOwnerOrAdmin())
			{
				throw new UserOperationNotAuthorizedException("generateQuery : User not authorized to execute the operation");
			}
			AdminQuery adminQuery = new AdminQuery();
			return adminQuery.generateQuery(query, isSuperUser, result);
			
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	// Cache Web Service Operations
	
	public OMElement clearCacheForQuery(String query, boolean exactMatch,
			boolean reseed, boolean allServers) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean ub = SMIWebUserBean.getUserBean();		
			if (!ub.isSuperUserOwnerOrAdmin())
			{
				throw new UserOperationNotAuthorizedException("clearCacheForQuery : User not authorized to execute the operation");
			}
			AdminCache cache = new AdminCache();
			return cache.clearCacheForQuery(query, exactMatch, reseed, false, result);
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement clearCacheForLogicalTable(String tableName, boolean allServers) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean ub = SMIWebUserBean.getUserBean();		
			if (!ub.isSuperUserOwnerOrAdmin())
			{
				throw new UserOperationNotAuthorizedException("clearCacheForLogicalTable : User not authorized to execute the operation");
			}
			AdminCache cache = new AdminCache();
			return cache.clearCacheForLogicalTable(tableName, false, result);
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	
	public OMElement clearCacheForPhysicalTable(String tableName, boolean allServers) {
		BirstWebServiceResult result = new BirstWebServiceResult();

		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean ub = SMIWebUserBean.getUserBean();		
			if (!ub.isSuperUserOwnerOrAdmin())
			{
				throw new UserOperationNotAuthorizedException("clearCacheForPhysicalTable : User not authorized to execute the operation");
			}
			AdminCache cache = new AdminCache();
			return cache.clearCacheForPhysicalTable(tableName, false, result);
		} catch (Exception e) {
			result.setException(e);
			return result.toOMElement();
		}
	}
	
	public OMElement clearAllCaches(boolean allServers) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean ub = SMIWebUserBean.getUserBean();		
			if (!ub.isSuperUserOwnerOrAdmin())
			{
				throw new UserOperationNotAuthorizedException("runQuery : User not authorized to execute the operation");
			}
			AdminCache cache = new AdminCache();
			return cache.clearAllCaches(false, result);
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement clearDashboards(boolean allServers) {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean ub = SMIWebUserBean.getUserBean();		
			if (!ub.isSuperUserOwnerOrAdmin())
			{
				throw new UserOperationNotAuthorizedException("clearDashboards : User not authorized to execute the operation");
			}
			AdminCache cache = new AdminCache();
			return cache.clearDashboards(false, result);
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement reloadContext() {
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (!isLoggedIn()) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean ub = SMIWebUserBean.getUserBean();		
			if (!ub.isSuperUserOwnerOrAdmin())
			{
				throw new UserOperationNotAuthorizedException("reloadContext : User not authorized to execute the operation");
			}
			AdminCache cache = new AdminCache();
			return cache.reloadContext(result);
		} catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	public OMElement getUserAndSpaceDetails(){
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (isLoggedIn() == false) {
				throw new UserNotLoggedInException();
			}
			
			SMIWebUserBean ub = getUserBean();
			OMElement elem = getLoggedInUserAndSpaceDetails(ub);
			result.setResult(new XMLWebServiceResult(elem));
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	private OMElement getLoggedInUserAndSpaceDetails(SMIWebUserBean ub){
		OMElement root = null;
		if(ub != null)
		{	
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMNamespace ns = null;
			root = factory.createOMElement("root",ns);

			String userID = ub.getUserID();
			String spaceID = ub.getSpaceID();
			XmlUtils.addContent(factory, root, "userID", userID, ns);
			XmlUtils.addContent(factory, root, "spaceID", spaceID, ns);
		}
		return root;
	}
	
	public OMElement getGroupList(){
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (isLoggedIn() == false) {
				throw new UserNotLoggedInException();
			}
			
			
			com.successmetricsinc.UserBean ub = getUserBean();
			List<Group> groups = ub.getRepository().getGroups(true);
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMNamespace ns = null;
			OMElement root = factory.createOMElement("Groups", ns);
			for (Group group : groups) {
				XmlUtils.addContent(factory, root, "Group", group.getName(), ns);
			}
			result.setResult(new XMLWebServiceResult(root));
		}
		catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();
	}
}
