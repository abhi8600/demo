package com.successmetricsinc.WebServices.map;

import java.io.File;
import java.io.FileOutputStream;
import java.io.StringReader;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.apache.log4j.Logger;
import org.apache.xerces.impl.dv.util.Base64;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.WebServices.BirstWebService;
import com.successmetricsinc.WebServices.BirstWebServiceResult;
import com.successmetricsinc.WebServices.XMLWebServiceResult;
import com.successmetricsinc.WebServices.dashboard.DashboardWebService;
import com.successmetricsinc.catalog.CatalogManager;
import com.successmetricsinc.query.MongoMgr;
import com.successmetricsinc.util.BaseException;
import com.successmetricsinc.util.UMapWebServiceHelper;
import com.successmetricsinc.util.UserNotLoggedInException;
import com.successmetricsinc.util.XmlUtils;

import de.micromata.opengis.kml.v_2_2_0.Boundary;
import de.micromata.opengis.kml.v_2_2_0.Coordinate;
import de.micromata.opengis.kml.v_2_2_0.Document;
import de.micromata.opengis.kml.v_2_2_0.Feature;
import de.micromata.opengis.kml.v_2_2_0.Folder;
import de.micromata.opengis.kml.v_2_2_0.Geometry;
import de.micromata.opengis.kml.v_2_2_0.Kml;
import de.micromata.opengis.kml.v_2_2_0.LinearRing;
import de.micromata.opengis.kml.v_2_2_0.MultiGeometry;
import de.micromata.opengis.kml.v_2_2_0.Placemark;
import de.micromata.opengis.kml.v_2_2_0.Point;
import de.micromata.opengis.kml.v_2_2_0.Polygon;

@WebService(serviceName="UMap")
public class UMapWebService extends BirstWebService{
	private static Logger logger = Logger.getLogger(DashboardWebService.class);
	private static final String DBNAME = "BirstKmls";

	public OMElement getPolygons(String xmlStr, String acornRoot){
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (isLoggedIn() == false) {
				throw new UserNotLoggedInException();
			}
			
			SMIWebUserBean ub = getUserBean();
			CatalogManager cm = ub.getCatalogManager();
			
			OMElement xml = parseXmlString(xmlStr);
			
			String kmlName = xml.getAttributeValue(new QName("", "kml"));
			if(!kmlName.toLowerCase().endsWith(".kml")){
				kmlName += ".kml";
			}
			
			//Try global, then user's
			File kmlFile = new File(getAcornRootDir(acornRoot) + File.separator + kmlName);
			if(!kmlFile.exists()){
				kmlFile = new File(getKmlDir(cm) + File.separator + kmlName);
			}
			
			if(kmlFile.exists()){
				OMElement response = null;
				try{
					response = findPlacemarkFromDB(ub, kmlName, xml, kmlFile);
				}catch(Exception ex){
					logger.error("Cannot find placemarks from MongoDB", ex);
					
					//Try to read from file
					Kml kml = Kml.unmarshal(kmlFile);
					response = parseFileToResponse(kml, xml);
					
					//TODO: Check if we can re-cache to Mongo
					//writeKmlToMongoDB(kmlName, kml, ub);
				}
				result.setResult(new XMLWebServiceResult(response));
			}else{
				logger.error("Cannot find file " + kmlFile.getName());
				result.setErrorCode(BaseException.ERROR_FILE_NOT_FOUND);
			}
		}catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();	
	}
	
	private OMElement parseFileToResponse(Kml kml, OMElement requestXml){
		HashMap<String, List<Polygon>> placemarkPolys = parseGetPolygons(kml);

		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement response = factory.createOMElement(new QName("", "placemarks"));
		@SuppressWarnings("rawtypes")
		Iterator itr = requestXml.getChildrenWithName(new QName("", "pm"));
		while(itr.hasNext()){
			OMElement pm = (OMElement)itr.next();
			String key = pm.getText();
			List<Polygon> polys = placemarkPolys.get(key);
			//Match
			if(polys != null){
				OMElement polyEl = factory.createOMElement(new QName("", "pm"));
				polyEl.addAttribute("id", key, null);

				for(int i = 0; i < polys.size(); i++){
					Polygon p = polys.get(i);
					Boundary bound = p.getOuterBoundaryIs();
					if(bound != null){
						LinearRing ring = bound.getLinearRing();
						if(ring != null){
							OMElement coordsEl = factory.createOMElement(new QName("", "coords"));
							StringBuffer coordsBuf = new StringBuffer();
							List<Coordinate> coords = ring.getCoordinates();
							for(int y = 0; y < coords.size(); y++){
								Coordinate cord = coords.get(y);
								coordsBuf.append(cord.getLatitude()).append(",").append(cord.getLongitude());
								if(y < coords.size() - 1){
									coordsBuf.append(" ");
								}
							}
							coordsEl.setText(coordsBuf.toString());
							polyEl.addChild(coordsEl);
						}
					}
				}
				response.addChild(polyEl);
			}
		}
		return response;
	}
	
	private OMElement findPlacemarkFromDB(SMIWebUserBean ub, String kmlName, OMElement xml, File kmlFile) throws UnknownHostException{
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMElement response = factory.createOMElement(new QName("", "placemarks"));
		
		//Build the list of placemarks to search for
		BasicDBList keys = new BasicDBList();
		@SuppressWarnings("rawtypes")
		Iterator itr = xml.getChildrenWithName(new QName("", "pm"));
		while(itr.hasNext()){
			OMElement pm = (OMElement)itr.next();
			String key = pm.getText();
			keys.add(key);
		}
		
		if(keys.size() > 0){
			DB db = getDB();
			DBCollection coll = getPageCollection(db, ub);
			db.requestStart();
			
			//Check to see if the file was parsed and cached before
			BasicDBObject checkQuery = new BasicDBObject("file", kmlName);
			BasicDBObject one = (BasicDBObject)coll.findOne(checkQuery);
			if(one == null){
				Kml kml = Kml.unmarshal(kmlFile);
				writeKmlToMongoDB(kmlName, kml, ub);
			}
			
			BasicDBObject query = new BasicDBObject("file", kmlName);
			query.append("name", new BasicDBObject("$in", keys));
			
			DBCursor cursor = coll.find(query);
			Iterator<DBObject> dbItr = cursor.iterator();
			while(dbItr.hasNext()){
				BasicDBObject o = (BasicDBObject)dbItr.next();
				BasicDBObject geom = (BasicDBObject)o.get("geometry");
				String type = geom.getString("type");
				String key = o.getString("name");
				
				OMElement polyEl = factory.createOMElement(new QName("", "pm"));
				polyEl.addAttribute("id", key, null);
				polyEl.addAttribute("type", type, null);
				if(type.equals("multipolygons")){
					BasicDBList polygons = (BasicDBList)geom.get("polygons");
					if(polygons != null){
						for(int i = 0; i < polygons.size(); i++){
							OMElement coords = convertToCoordsElements((BasicDBList)polygons.get(i));
							polyEl.addChild(coords);
						}
					}
				}else if(type.equals("polygon")){
					BasicDBList polygon = (BasicDBList)geom.get("coords");
					OMElement coords = convertToCoordsElements((BasicDBList)polygon);
					polyEl.addChild(coords);
				}
				
				response.addChild(polyEl);
			}
			db.requestDone();
		}
		return response;
	}
	
	private OMElement convertToCoordsElements(BasicDBList coordsList){
		OMElement coordsEl = OMAbstractFactory.getOMFactory().createOMElement(new QName("", "coords"));
		StringBuffer coordsBuf = new StringBuffer();
		for(int y = 0; y < coordsList.size(); y++){
			BasicDBList cord =(BasicDBList)coordsList.get(y);
			double latitude = (Double)cord.get(0);
			double longitude = (Double)cord.get(1);
			coordsBuf.append(latitude).append(",").append(longitude);
			if(y < coordsList.size() - 1){
				coordsBuf.append(" ");
			}
		}
		coordsEl.setText(coordsBuf.toString());
		return coordsEl;
	}
	
	public OMElement uploadKml(final String kmlName, final String encodedContents){
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (isLoggedIn() == false) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean ub = getUserBean();
			CatalogManager cm = ub.getCatalogManager();
			
			byte[] decoded = Base64.decode(encodedContents);
			String data = new String(decoded, "UTF-8");
			
			data = UMapWebServiceHelper.fixupEntity(data);
			
			Kml kml = Kml.unmarshal(data);
			
			if(kml == null){
				logger.error("Unable to parse kml file");
				result.setErrorCode(BaseException.ERROR_INVALID_DATA);
			}else{
				File kmlDir = new File(getKmlDir(cm));
				if(!kmlDir.exists()){
					kmlDir.mkdir();
				}

				File out = new File(kmlDir, kmlName);
				FileOutputStream fo = new FileOutputStream(out);
				fo.write(decoded);
				fo.flush();
				fo.close();

				writeKmlToMongoDB(kmlName, kml, ub);
			}
			
		}catch (Exception e) {
			result.setException(e);
		}
		return result.toOMElement();	
	}
	
	public OMElement getKmlListing(String acornRoot){
		BirstWebServiceResult result = new BirstWebServiceResult();
		try {
			if (isLoggedIn() == false) {
				throw new UserNotLoggedInException();
			}
			SMIWebUserBean ub = getUserBean();
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMElement response = factory.createOMElement(new QName("", "kmls"));
			
			addKmlFilesAtDir(getAcornRootDir(acornRoot), response);
			addKmlFilesAtDir(getKmlDir(ub.getCatalogManager()), response);
			
			result.setResult(new XMLWebServiceResult(response));
		}catch(Exception e){
			result.setException(e);
		}
		return result.toOMElement();
	}
	
	private static void addKmlFilesAtDir(String dir, OMElement response){
		File kmlDir = new File(dir);
		if(kmlDir.exists()){
			OMFactory factory = OMAbstractFactory.getOMFactory();
			
			File[] kmlFiles = kmlDir.listFiles(KmlFileFilter.getInstance());
			for(int i = 0; i < kmlFiles.length; i++){
				File kmlFile = kmlFiles[i];
				String filename = kmlFile.getName();
				filename = filename.substring(0, filename.lastIndexOf(".kml"));
				
				OMElement k = factory.createOMElement(new QName("", "kml"));
				k.setText(filename);
				response.addChild(k);
			}
		}
	}
	
	private static String getKmlDir(CatalogManager cm){
		return cm.getCatalogRootPath() + File.separator + "Kml";
	}
	
	private static String getAcornRootDir(String acornRoot){
		String rootDir = acornRoot;
		if(rootDir == null){
			rootDir = "C:/SMI/Acorn/";
		}
		return rootDir + "/config/Kml";
	}
	
	/**
	 * Write / cache the parsed kml structure into MongoDB 
	 * @param kmlName
	 * @param kml
	 * @param ub
	 * @throws UnknownHostException
	 */
	private void writeKmlToMongoDB(String kmlName, Kml kml, SMIWebUserBean ub) throws UnknownHostException{
		DB db = getDB();
		DBCollection coll = getPageCollection(db, ub);
		db.requestStart();
		
		//Remove previous entries
		coll.remove(new BasicDBObject("file", kmlName));
		
		ArrayList<BasicDBObject> placemarkObjs = new ArrayList<BasicDBObject>();
		
		Document doc = (Document)kml.getFeature();
		List<Feature> features = doc.getFeature();
		for(int i = 0; i < features.size(); i++){
			Feature f = features.get(i);
			if(f instanceof Placemark ){
				parsePlacemark((Placemark)f, kmlName, placemarkObjs);
			}else if (f instanceof Folder){
				Folder folder = (Folder)f;
				List<Feature>folderFeatures = folder.getFeature();
				for(int y = 0; y < folderFeatures.size(); y++){
					Feature featureFolder = folderFeatures.get(y);
					if(featureFolder instanceof Placemark){
						parsePlacemark((Placemark) featureFolder, kmlName, placemarkObjs);
					}
				}
			}
		}
		
		coll.ensureIndex("name");
		for(int i = 0; i < placemarkObjs.size(); i++){
			BasicDBObject o = placemarkObjs.get(i);
			coll.insert(o);
		}
		db.requestDone();
	}
	
	private void parsePlacemark(Placemark p, String kmlName, ArrayList<BasicDBObject> placemarkObjs){
		BasicDBObject pObj = new BasicDBObject("name", p.getName());
		pObj.append("file", kmlName);
		placemarkObjs.add(pObj);
		
		Geometry geometry = p.getGeometry();
		if(geometry instanceof MultiGeometry){
			BasicDBList list = new BasicDBList();
			MultiGeometry multiGeom = (MultiGeometry) geometry;
			List<Geometry> geoms = multiGeom.getGeometry();
			for(int y = 0; y < geoms.size(); y++){
				Geometry geo = geoms.get(y);
				recurseAddPolygons(geo, list);
			}
			BasicDBObject multiObj = new BasicDBObject("type", "multipolygons");
			multiObj.append("polygons", list);
			pObj.append("geometry", multiObj);
		}else if (geometry instanceof Polygon){
			BasicDBObject polyObj = new BasicDBObject("type", "polygon");
			polyObj.append("coords", createPolygonDBList(geometry));
			pObj.append("geometry", polyObj);
		}else if (geometry instanceof Point){
			Point pt = (Point)geometry;
			List<Coordinate>coords = pt.getCoordinates();
			
			BasicDBObject ptObj = new BasicDBObject("type", "point");
			BasicDBList ptList = new BasicDBList();
			
			Coordinate c = coords.get(0);
			ptList.add(c.getLatitude());
			ptList.add(c.getLongitude());
			
			ptObj.append("coords", ptList);
			pObj.append("geometry", ptObj);
		}
	}
	
	/**
	 * 
	 * @param geometry
	 * @param dblist
	 */
	private static void recurseAddPolygons(Geometry geometry, BasicDBList dblist){
		if(geometry instanceof MultiGeometry){
			MultiGeometry multiGeom = (MultiGeometry) geometry;
			List<Geometry> geoms = multiGeom.getGeometry();
			for(int i = 0; i < geoms.size(); i++){
				Geometry geo = geoms.get(i);
				recurseAddPolygons(geo, dblist);
			}
		}else if (geometry instanceof Polygon){
			BasicDBList l = createPolygonDBList(geometry);
			dblist.add(l);
		}
	}
	
	/** 
	 * Create a mongodb basicdblist from the polygon object, eg-
	 * [[-123.23, 33.5], [-123.11, 33.0], ...]]
	 * 
	 * @param geom
	 */
	private static BasicDBList createPolygonDBList(Geometry geom){
		Polygon p = (Polygon)geom;
		
		BasicDBList polyList = new BasicDBList();
		Boundary bound = p.getOuterBoundaryIs();
		if(bound != null){
			LinearRing ring = bound.getLinearRing();
			if(ring != null){
				List<Coordinate> coords = ring.getCoordinates();
				for(int y = 0; y < coords.size(); y++){
					Coordinate cord = coords.get(y);
					BasicDBList point = new BasicDBList();
					point.add(cord.getLatitude());
					point.add(cord.getLongitude());
					polyList.add(point);
				}
			}
		}
		return polyList;
	}
	
	private static DBCollection getPageCollection(DB db, com.successmetricsinc.UserBean ub){
		return db.getCollection(ub.getRepository().getApplicationIdentifier() + ".kml");
	}
	
	private static DB getDB() throws UnknownHostException{
		return MongoMgr.getMongo().getDB(DBNAME);
	}
	
	private static HashMap<String, List<Polygon>> parseGetPolygons(Kml kml){
		Document doc = (Document)kml.getFeature();
		List<Feature> features = doc.getFeature();
		
		HashMap<String, List<Polygon>> placemarkPolys = new HashMap<String, List<Polygon>>();
		
		for(int i = 0; i < features.size(); i++){
			Feature f = features.get(i);
			if(f instanceof Placemark ){
				//Find its geometry
				Placemark p = (Placemark)f;
				ArrayList<Polygon> polygons = new ArrayList<Polygon>();
				recurseAddPolygons(p.getGeometry(), polygons);
				if(polygons.size() > 0){
					placemarkPolys.put(p.getName(), polygons);
				}
			}else if (f instanceof Folder){
				Folder folder = (Folder)f;
				List<Feature>folderFeatures = folder.getFeature();
				for(int y = 0; y < folderFeatures.size(); y++){
					Feature featureFolder = folderFeatures.get(y);
					if(featureFolder instanceof Placemark){
						Placemark p = (Placemark)featureFolder;
						ArrayList<Polygon> polygons = new ArrayList<Polygon>();
						recurseAddPolygons(p.getGeometry(), polygons);
						if(polygons.size() > 0){
							placemarkPolys.put(p.getName(), polygons);
						}
					}
				}
			}
		}
		
		return placemarkPolys;
	}
	
	private static void recurseAddPolygons(Geometry geometry, List<Polygon> polys){
		if(geometry instanceof MultiGeometry){
			MultiGeometry multiGeom = (MultiGeometry) geometry;
			List<Geometry> geoms = multiGeom.getGeometry();
			for(int i = 0; i < geoms.size(); i++){
				Geometry geo = geoms.get(i);
				recurseAddPolygons(geo, polys);
			}
		}else if (geometry instanceof Polygon){
			polys.add((Polygon)geometry);
		}
		
	}
	
	private OMElement parseXmlString(String xml) throws XMLStreamException, FactoryConfigurationError{
		XMLInputFactory inputFactory = XmlUtils.getSecureXMLInputFactory();
		XMLStreamReader parser = inputFactory.createXMLStreamReader(new StringReader(xml));

		StAXOMBuilder builder = new StAXOMBuilder(parser);
		return builder.getDocumentElement();
	}
}

class KmlFileFilter implements java.io.FileFilter{
	private static KmlFileFilter _instance = null;
	
	public static KmlFileFilter getInstance(){
		if(_instance == null){
			_instance = new KmlFileFilter();
		}
		return _instance;
	}
	
	public boolean accept(File f){
		return (f == null) ? false : f.getName().toLowerCase().endsWith(".kml");
	}
}

