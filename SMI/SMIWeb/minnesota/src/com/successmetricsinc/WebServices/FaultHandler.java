package com.successmetricsinc.WebServices;

import javax.xml.namespace.QName;

import org.apache.axiom.om.OMElement;
import org.apache.axis2.AxisFault;
import org.apache.axis2.context.MessageContext;
import org.apache.axis2.handlers.AbstractHandler;

import com.successmetricsinc.util.BaseException;

public class FaultHandler extends AbstractHandler {
	private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

	@Override
	public InvocationResponse invoke(MessageContext msgContext) throws AxisFault {
		try{
			MessageContext out = msgContext.getOperationContext().getMessageContext("Out");
			if(out != null){
				OMElement body = out.getEnvelope().getBody();
				
				if(body != null){
					//Find the BirstWebserviceResult part
					OMElement el = body.getFirstElement();
					while(el != null){
						if (el.getLocalName().equals(BirstWebServiceResult.class.getName())){
							break;
						}
						el = el.getFirstElement();
					}

					//Found it, if it was modified, errcode -300, we do not add fault
					if(el != null){
						OMElement errCode = el.getFirstChildWithName(new QName("", BirstWebServiceResult.ERR_CODE));
						if(errCode != null && Integer.toString(BaseException.ERROR_XML_PARSE).equals(errCode.getText())){
							msgContext.getEnvelope().getBody().detach();
						}
					}
				}
			}
		}catch (Exception e){
			throw AxisFault.makeFault(e);
		}
		return InvocationResponse.CONTINUE;
	}

}
