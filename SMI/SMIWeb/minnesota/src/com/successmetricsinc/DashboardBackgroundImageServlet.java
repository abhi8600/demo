/**
 * $Id: DashboardBackgroundImageServlet.java,v 1.7 2012-06-05 23:52:40 sfoo Exp $
 *
 * Copyright (C) 2009-2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc;

import java.io.IOException;

import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.ServletContext;

import org.apache.log4j.Logger;

import com.successmetricsinc.UI.SMIWebUserBean;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

public class DashboardBackgroundImageServlet extends HttpServlet
{
	private static Logger logger = Logger.getLogger(DashboardBackgroundImageServlet.class);
	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		doGet(request, response);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		HttpSession session = request.getSession(false);
		if (session == null)
		{
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
			return;
		}
		
		SMIWebUserBean userBean = (SMIWebUserBean) session.getAttribute("userBean");
		if (userBean == null)
		{
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
			return;
		}

		try
		{   
			String image = request.getParameter("image");
			if(image == null || image.isEmpty()) {
				response.sendError(HttpServletResponse.SC_BAD_REQUEST);
				return;
			}
			image = (new File(image)).getName();
			
			String imageFilePath = 
				userBean.getCatalogManager().getCatalogRootPath() + File.separator + ".." + File.separator + "DashboardBackgroundImage" + File.separator + image;
			File imageFile = new File(imageFilePath);
			if(!imageFile.exists()){
			   response.sendError(HttpServletResponse.SC_NOT_FOUND);
			   return;
			}
			
			ServletContext sc = this.getServletContext();
			
			String contentType = sc.getMimeType(imageFile.getAbsolutePath());
			response.setContentType(contentType);
			response.setContentLength((int)imageFile.length());
			
			// Open the file and output streams 
			FileInputStream in = null; 
			OutputStream out = null; 
			try
			{
				in = new FileInputStream(imageFile); 
				out = response.getOutputStream(); 

				// Copy the contents of the file to the output stream 
				byte[] buf = new byte[1024]; 
				int count = 0; 
				while ((count = in.read(buf)) >= 0) { 
					out.write(buf, 0, count); 
				}
			}
			finally
			{
				if (in != null)
					in.close();
				if (out != null)
					out.close(); 
			}
			response.flushBuffer();
		}
		catch(Exception e)
		{
			logger.error(e, e);
		}
		
	}
}

