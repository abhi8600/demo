package com.successmetricsinc.annotations.rest;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target(ElementType.PARAMETER)
public @interface QueryParam {
	String value();
}
