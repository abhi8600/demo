/**
 * $Id: GenerateFileServlet.java,v 1.10 2012-06-06 19:28:39 ricks Exp $
 *
 * Copyright (C) 2009-2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc;

import java.io.IOException;
import java.net.URLDecoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.UI.admin.system.Queries;

public class GenerateFileServlet extends HttpServlet {
	private static Logger logger = Logger.getLogger(GenerateFileServlet.class);
	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		doGet(request, response);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		HttpSession session = request.getSession(false);
		if (session == null)
		{
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
			return;
		}
		
		com.successmetricsinc.UI.SMIWebUserBean userBean = (com.successmetricsinc.UI.SMIWebUserBean) session.getAttribute("userBean");
		if (userBean == null)
		{
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
			return;
		}
		
		if (!userBean.isSuperUserOwnerOrAdmin())
		{
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
			return;	
		}

		try{
			String query = URLDecoder.decode(request.getParameter("fileQuery"), "UTF-8");
			String separator = request.getParameter("separator");
			logger.info("Query for Generating File : " + query);
			SMIWebUserBean.setHttpServletRequest(request);
			Queries queries = new Queries();
			queries.setQuery(query);
			queries.setSeparator(separator);
			queries.generateFile(response);
		}
		catch(Exception e){
			ServerContext.errorMessage(e, "Error in File Generation", response);
		}
	}
}
