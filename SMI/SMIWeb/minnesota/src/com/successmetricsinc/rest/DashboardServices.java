/**
 * Copyright (C) 2009-2013 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc.rest;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.log4j.Logger;
import org.codehaus.jettison.mapped.MappedNamespaceConvention;
import org.codehaus.jettison.mapped.MappedXMLStreamWriter;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.util.JSON;
import com.successmetricsinc.Column;
import com.successmetricsinc.Prompt;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.WebServices.BirstWebServiceResult;
import com.successmetricsinc.WebServices.ClientInitData;
import com.successmetricsinc.WebServices.adhoc.AdhocWS;
import com.successmetricsinc.WebServices.dashboard.DashboardMongoDBCache;
import com.successmetricsinc.WebServices.dashboard.DashboardWebService;
import com.successmetricsinc.WebServices.dashboard.DashboardWebServiceHelper;
import com.successmetricsinc.adhoc.AdhocReportHelper;
import com.successmetricsinc.catalog.CatalogAccessException;
import com.successmetricsinc.catalog.CatalogManager;
import com.successmetricsinc.catalog.CatalogPermission;
import com.successmetricsinc.util.SwapSpaceInProgressException;
import com.successmetricsinc.util.XmlUtils;

@Path("/dashboards")
public class DashboardServices extends BaseServices
{
	private static Logger logger = Logger.getLogger(DashboardServices.class);
	private static final long serialVersionUID = 1L;
	
	private static final String ES_SERVER_URL = "ES_Url";
	
	public DashboardServices(@Context HttpServletRequest req, @Context ServletContext ctx){
		super(req, ctx);
	}
	
	private String getESUrl(){
		String serverUrl = System.getProperty(ES_SERVER_URL);
		if(serverUrl == null){
			serverUrl = System.getenv(ES_SERVER_URL);
			if(serverUrl == null){
				serverUrl = "http://localhost:9200";
			}
		}
		
		return serverUrl;
	}

	private String getESSearchUri(SMIWebUserBean ub){
		String spaceId = ub.getRepository().getApplicationIdentifier();
		return getESUrl() + "/birst_" + spaceId + "/adhocReport,viz_dashlet/_search";
	}
	
	private String getESCreateIndexUri(SMIWebUserBean ub){
		String spaceId = ub.getRepository().getApplicationIdentifier();
		return getESUrl() + "/_spaceIndexer/" + spaceId;
	}
	
	private Response logSearchError(Exception e, String msg) {
		logger.error(msg, e);
		return Response.status(Status.INTERNAL_SERVER_ERROR).build();
	}
	
	@GET
	@Path("reports/searchAggregate/{query}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchReportsAggregation(@PathParam("query") String query) throws HttpException, IOException, JSONException{
		Response isSecure = securityCheck();
		if(!(isSecure.getStatusInfo() == Status.OK)){
			return isSecure;
		}
		
		String esSearchUrl = getESSearchUri(userBean);
		String queryUrl = esSearchUrl;
		try{
			HttpClient client = new HttpClient();
			client.getParams().setSoTimeout(2*60000);

			JSONObject queryDsl = getESQueryDSL(query, 0, 10000);
			String queryStr = queryDsl.toString();
			StringRequestEntity entity = new StringRequestEntity(queryStr, "application/json", "UTF-8");

			HttpGetWithEntity get = new HttpGetWithEntity(queryUrl);
			get.setRequestEntity(entity);

			int rc = client.executeMethod(get);
			String searchResponse = get.getResponseBodyAsString();
			logger.debug("searchResult: " + searchResponse);
			if(rc == 200){
				return Response.ok(searchResponse).build();
			}else if (rc == 404){
				if(searchResponse.indexOf("IndexMissingException") >= 0){
					int createRc = createIndexOnSpace(client, userBean);
					if(createRc == 200){
						rc = client.executeMethod(get);
						searchResponse = get.getResponseBodyAsString();
						if(rc == 200){
							return Response.ok(searchResponse).build();
						}else{
							return Response.status(rc).entity(searchResponse).build();
						}
					}else{
						return Response.status(createRc).entity(searchResponse).build();
					}
				}else{
					return Response.status(rc).entity(searchResponse).build();
				}
			}else{
				return Response.status(rc).entity(searchResponse).build();
			}
		}catch(HttpException ex){
			return logSearchError(ex, queryUrl);
		}catch(IOException ex){
			return logSearchError(ex, queryUrl);
		}
		
	}
	
	private JSONObject getESQueryDSL(String queryStr, int from, int size) throws JSONException{
		JSONObject root = new JSONObject();
		String[] stringFields = new String[] { 
				"creator", 
				"dimensions", 
				"measures", 
				"charts", 
				"expressions",
				"dir"
		};
		
		JSONArray fields = new JSONArray(stringFields);
		fields.put("filename");
		fields.put("lastModified");
		root.put("fields", fields).
		put("from", from).
		put("size", size);
		
		JSONObject query = new JSONObject();
		query.put("query", queryStr);
		
		JSONObject queryString = new JSONObject();
		queryString.put("query_string", query);
		
		appendQueryFields(query, fields);
		
		root.put("query", 
				(new JSONObject()).put("filtered", 
					(new JSONObject().put("query", queryString)
		)));
								
		JSONObject agg = new JSONObject();
		root.put("aggregations", agg);
		appendAggTerms(agg, stringFields);
		appendAggDateRange(agg);
		
		JSONObject highlightFields = new JSONObject();
		root.put("highlight", (new JSONObject()).put("fields", highlightFields));
		appendHighlights(highlightFields, fields);
		return root;
	}
	
	private void appendQueryFields(JSONObject queryString, JSONArray fieldTerms) throws JSONException{
		JSONArray fields = new JSONArray();
		for(int i = 0; i < fieldTerms.length(); i++){
			String term = fieldTerms.getString(i);
			if(term.equals("filename")){
				term = "filename^10";
			}else if (term.equals("dir")){
				term = "dir^5";
			}else if (term.equals("lastModified")){
				continue;
			}
			fields.put(term);
		}
		queryString.put("fields", fields);
	}
	
	private void appendHighlights(JSONObject highlight, JSONArray terms) throws JSONException{
		for(int i = 0; i < terms.length(); i++){
			highlight.put(terms.getString(i), new JSONObject());
		}
	}
	
	private void appendAggTerms(JSONObject aggregations, String[] terms) throws JSONException{
		for( int i = 0; i < terms.length; i++){
			String term = terms[i];
			
			JSONObject aggParams = new JSONObject();
			aggParams.put("field", term + ".raw");
			aggParams.put("size", 30);
			
			JSONObject agg = new JSONObject();
			agg.put("terms", aggParams);
			aggregations.put(term, agg);
		}
	}
	
	private void appendAggDateRange(JSONObject aggregations) throws JSONException {
		JSONObject range = new JSONObject();
		aggregations.put("range", range);
		JSONObject dateRange = new JSONObject();
		range.put("date_range", dateRange);
		dateRange.put("field", "lastModified");
		dateRange.put("format", "MM-dd-yyyy");
		
		JSONArray dateRanges = new JSONArray();
		dateRange.put("ranges", dateRanges);
		
		Date current = new Date();
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		String midnight = fmt.format(current);
		midnight = midnight + "T00:00:00";
		
		appendDateRange(dateRanges, midnight, "now");
		appendDateRange(dateRanges, "now-7d", "now");
		appendDateRange(dateRanges, "now-1M", "now");
		appendDateRange(dateRanges, "now-1y", "now");
		appendDateRange(dateRanges, null, "now-1y");
	}
	
	private void appendDateRange(JSONArray ranges, String from, String to) throws JSONException{
		JSONObject fromTo = new JSONObject();
		if(from != null){
			fromTo.put("from", from);
		}
		
		if(to != null){
			fromTo.put("to", to);
		}
		
		ranges.put(fromTo);
	}
	
	private int createIndexOnSpace(HttpClient client, SMIWebUserBean ub) throws HttpException, IOException{
		String createIndexUrl = getESCreateIndexUri(ub);
		PostMethod post = new PostMethod(createIndexUrl);
		int rc = client.executeMethod(post);
		return rc;
	}
	
	@GET
	@Path("settings")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSettings(){

		ClientInitData client = new ClientInitData();
		client.setTestHttpSession(request.getSession());
		
		String jsonOutput;
		try {
			jsonOutput = DashboardServicesHelper.omElementToJson(client.getDashboardSettings());
		} catch (XMLStreamException e) {
			logger.error("Cannot convert getDashboardSettings() to JSON", e);
			return Response.serverError().build();
		}
		
		return Response.ok(jsonOutput).build();
	
	}
	
	@POST
	@Path("prompt/column")
	public Response getPromptByColumn(final String columnFilter){
		Response isSecure = securityCheck();
		if(isSecure.getStatusInfo() != Status.OK){
			return isSecure;
		}
		
		BasicDBObject filterObj = (BasicDBObject)com.mongodb.util.JSON.parse(columnFilter);
		if(filterObj == null){
			logger.error("Unable to parse column filter data");
			return Response.status(Status.BAD_REQUEST).build();
		}
		
		String jsonOutput = "";
		try{
			String type = filterObj.getString("t");
			Column col = null;
			String val = filterObj.getString("v");
			if ("om".equalsIgnoreCase(type)) {
				String dim = filterObj.getString("od");
				String hier = filterObj.getString("oh");
				col = AdhocReportHelper.getColumn(val, dim, hier);
			} else {
				col = AdhocReportHelper.getColumn(val, null, null);
			}
			String labela = filterObj.getString("l");
			String label = null;
			if (labela != null)
				label = labela;
			if (label == null) {
				label = val;
				int index = label.lastIndexOf('.');
				if (index > 0) {
					label = label.substring(index);
				}
			}
			Prompt prompt = new Prompt(col, label);
			BirstWebServiceResult result = new BirstWebServiceResult();
			result.setResult(prompt);
			
			try {
				jsonOutput = DashboardServicesHelper.omElementToJson(result.toOMElement());
			} catch (XMLStreamException e) {
				logger.error("Cannot convert getPromptByColumn() to JSON", e);
				return Response.serverError().build();
			}
		}catch(Exception e){
			logger.error("getPromptByColumn() error ", e);
			return Response.serverError().build();
		}
		
		return Response.ok(jsonOutput).build();
	}
	
	@POST
	@Path("prompt/query")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response queryValues(final String prompts){
		Response isSecure = securityCheck();
		if(isSecure.getStatusInfo() != Status.OK){
			return isSecure;
		}
		
		AdhocWS ws = new AdhocWS();
		ws.setTestHttpSession(request.getSession());
		ws.setJsonParsing(true);
		OMElement om = ws.getFilledOptions(prompts);
		
		String jsonOutput = "";
		try {
			jsonOutput =  DashboardServicesHelper.omElementToJson(om);
		} catch (XMLStreamException e) {
			logger.error("queryValues() error", e);
			return Response.serverError().build();
		}
		
		return Response.ok(jsonOutput).build();
		
	}
	
	@POST
	@Path("prompt/filteredQuery")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response filteredQuery(final String prompts){
		Response isSecure = securityCheck();
		if(isSecure.getStatusInfo() != Status.OK){
			return isSecure;
		}
		
		AdhocWS ws = new AdhocWS();
		ws.setTestHttpSession(request.getSession());
		ws.setJsonParsing(true);
		OMElement om = ws.getFilteredFilledOptions(prompts);
		
		String jsonOutput = "";
		try {
			jsonOutput =  DashboardServicesHelper.omElementToJson(om);
		} catch (XMLStreamException e) {
			logger.error("queryValues() error", e);
			return Response.serverError().build();
		}
		
		return Response.ok(jsonOutput).build();
		
	}
	
	/**
	 * Create a new sharable prompt
	 * @param promptData
	 * @return
	 */
	@POST
	@Path("prompt")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createPrompt(final String promptData){
		Response isSecure = securityCheck();
		if(isSecure.getStatusInfo() != Status.OK){
			return isSecure;
		}
		
		if(!userBean.isSuperUserOwnerOrAdmin() && !userBean.hasEditDashboardPermission()){
			return Response.status(Status.FORBIDDEN).build();
		}
		
		BasicDBObject promptObj = (BasicDBObject)com.mongodb.util.JSON.parse(promptData);
		if(promptObj == null){
			logger.error("Unable to parse prompt data");
			return Response.status(Status.BAD_REQUEST).build();
		}
		
		Response verifyOk = verifyPrompt(promptObj);
		if(verifyOk.getStatusInfo() != Status.OK){
			return verifyOk;
		}
		
		String uuid = java.util.UUID.randomUUID().toString();
		try{
			DashboardMongoDBCache.writeSharablePrompt(uuid, promptObj);
		}catch(Exception ex){
			logger.error(ex);
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
		
		BasicDBObject uuidObj = new BasicDBObject("uuid", uuid);
		try {
			URI createdURI = new URI("/SMIWeb/rest/dashboards/prompt/" + uuid);
			return Response.created(createdURI).entity(JSON.serialize(uuidObj)).build();
		} catch (URISyntaxException e) {
			logger.error(e);
			return Response.status(Status.CREATED).entity(uuidObj).build();	
		}
	}
	
	/**
	 * Update an existing sharable prompt
	 * @param promptData
	 * @param uuid
	 * @return
	 */
	@POST
	@Path("prompt/{uuid}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updatePrompt(final String promptData, @PathParam("uuid") final String uuid){
		Response isSecure = securityCheck();
		if(isSecure.getStatusInfo() != Status.OK){
			return isSecure;
		}
		
		if(!userBean.isSuperUserOwnerOrAdmin() && !userBean.hasEditDashboardPermission()){
			return Response.status(Status.FORBIDDEN).build();
		}
		
		BasicDBObject promptObj = (BasicDBObject)com.mongodb.util.JSON.parse(promptData);
		if(promptObj == null){
			logger.error("Unable to parse prompt data");
			return Response.status(Status.BAD_REQUEST).build();
		}
		
		Response verifyOk = verifyPrompt(promptObj);
		if(verifyOk.getStatusInfo() != Status.OK){
			return verifyOk;
		}
		
		try{
			if(!DashboardMongoDBCache.existsSharablePrompt(uuid)){
				logger.error("Cannot modify nonexistant prompt of id " + uuid);
				return Response.status(Status.NOT_FOUND).build();
			}
			DashboardMongoDBCache.writeSharablePrompt(uuid, promptObj);
		}catch(Exception ex){
			logger.error(ex);
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
		
		return Response.noContent().build();
	}
	
	/**
	 * Read an existing prompt(s)
	 * @param uuid
	 * @return
	 */
	@GET
	@Path("prompt/{uuid}")
	public Response getPrompt(@PathParam("uuid") final String uuid){
		Response isSecure = securityCheck();
		if(isSecure.getStatusInfo() != Status.OK){
			return isSecure;
		}
		
		if(!userBean.isSuperUserOwnerOrAdmin() && !userBean.hasEditDashboardPermission()){
			return Response.status(Status.FORBIDDEN).build();
		}
		
		String[] uuidList = uuid.indexOf(",") != -1 ? uuid.split(",") : new String[] { uuid };
	
		try {
			BasicDBList dblist = DashboardMongoDBCache.getSharablePrompts(uuidList);
			if(dblist == null ){
				return Response.status(Status.NOT_FOUND).build();
			}else{
				String entity = null;
				int size = dblist.size();
				if(size == 0){
					return Response.status(Status.NOT_FOUND).build();
				}else if (size == 1){
					Object obj = dblist.get(0);
					entity = JSON.serialize(obj);
				}else{
					entity = JSON.serialize(dblist);
				}
				return Response.ok(entity).build();
			}
		} catch (IOException e) {
			logger.error("Unable to get prompt(s): " + e.getMessage(), e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	/**
	 * Read an existing prompt(s)
	 * @param uuid
	 * @return
	 */
	@GET
	@Path("prompt")
	public Response getAllPrompts(){
		Response isSecure = securityCheck();
		if(isSecure.getStatusInfo() != Status.OK){
			return isSecure;
		}
		
		if(!userBean.isSuperUserOwnerOrAdmin() && !userBean.hasEditDashboardPermission()){
			return Response.status(Status.FORBIDDEN).build();
		}
		
		BasicDBList dblist = DashboardMongoDBCache.getAllSharablePrompts();
		String entity = JSON.serialize(dblist);
		return Response.ok(entity).build();
	}
	
	
	/**
	 * Delete an existing prompt
	 * @param uuid
	 * @return
	 * @throws IOException
	 */
	@DELETE
	@Path("prompt/{uuid}")
	public Response deletePrompt(@PathParam("uuid") final String uuid){
		Response isSecure = securityCheck();
		if(isSecure.getStatusInfo() != Status.OK){
			return isSecure;
		}
		
		if(!userBean.isSuperUserOwnerOrAdmin() && !userBean.hasEditDashboardPermission()){
			return Response.status(Status.FORBIDDEN).build();
		}
		
		if(!DashboardMongoDBCache.existsSharablePrompt(uuid)){
			logger.error("Cannot delete nonexistant prompt of id " + uuid);
			return Response.status(Status.NOT_FOUND).build();
		}
		
		if(!DashboardMongoDBCache.deleteSharablePrompt(uuid)){
			logger.error("Unable to remove prompt of id " + uuid);
			return Response.status(Status.FORBIDDEN).build();
		}
		
		return Response.noContent().build();
		
	}
	
	public Response verifyPrompt(BasicDBObject promptObj){
		//Basic fields
		String[] keys = { 
							"PromptType", 
							"VisibleName",
							"ReadOnly",
							"ParameterName",
							"IsExpression",
							"ColumnName",
							"FilterType",
							"Operator",
							"DefaultOption",
							"ToolTip",
							"Invisible",
							"variableToUpdate"
						};
		
		
		Response isBasicFieldsOk = checkFields(keys, promptObj);
		if(isBasicFieldsOk.getStatusInfo() != Status.OK){
			return isBasicFieldsOk;
		}
		
		//Check prompt type
		final String[] validPromptTypes = new String[]{ "Query", "Value", "List", "Slider" };
		final Set<String>validPromptTypesSet = new HashSet<String>(Arrays.asList(validPromptTypes));
		String promptType = promptObj.getString("PromptType");
		if(!validPromptTypesSet.contains(promptType)){
			BasicDBObject err = new BasicDBObject("BadRequest", "Invalid prompt type" + promptType);
			return Response.status(Status.BAD_REQUEST).entity(JSON.serialize(err)).build();
		}
		
		//Verify query prompt
		if("Query".equals(promptType)){
			return verifyQueryPrompt(promptObj);
		}
		
		return Response.ok().build();
	}
	
	private Response verifyQueryPrompt(BasicDBObject promptObj){
		String[] keys = new String[] { "Query", "addNoSelectionEntry", "text", "multiSelectType" };
		return checkFields(keys, promptObj);
	}
	
	private Response checkFields(String[]keys, BasicDBObject promptObj){
		ArrayList<String> missingFields = new ArrayList<String>();
		for(String k : keys){
			if(!promptObj.containsField(k)){
				missingFields.add(k);
			}
		}
		
		int missingFieldSz = missingFields.size();
		if(missingFieldSz > 0){
			StringBuilder builder = new StringBuilder();
			for(int i = 0; i < missingFieldSz; i++){
				if( i > 0 ){
					builder.append(",");
				}
				builder.append(missingFields.get(i));
			}
			BasicDBObject err = new BasicDBObject("BadRequest", "Missing fields- " + builder.toString());
			return Response.status(Status.BAD_REQUEST).entity(JSON.serialize(err)).build();
		}
		
		return Response.ok().build();
	}
	
	@GET
	@Path("page")
	public Response getAllDashboardsPages(){
		Response isSecure = securityCheck();
		if(isSecure.getStatusInfo() != Status.OK){
			return isSecure;
		}
		
		if(DashboardMongoDBCache.useMongoDB()){
			try{
				BasicDBList pages = DashboardMongoDBCache.getDashboardListDBObject(userBean, true);
				String jsonOutput = JSON.serialize(pages);
				return Response.ok(jsonOutput, MediaType.APPLICATION_JSON).build();
			}catch(Exception e){
				logger.error("getAllDashboardsPages() error ", e);
				return Response.serverError().build();
			}
		}else{
			BasicDBObject err = new BasicDBObject("Error", "DB server offline");
			return Response.serverError().entity(JSON.serialize(err)).build();
		}
	}
	
	@GET
	@Path("page/{uuid}")
	public Response getPage(@PathParam("uuid") String uuid) throws IOException{
		Response isSecure = securityCheck();
		if(isSecure.getStatusInfo() != Status.OK){
			return isSecure;
		}
		
		if(!userBean.isSuperUserOwnerOrAdmin() && !userBean.hasEditDashboardPermission()){
			return Response.status(Status.FORBIDDEN).build();
		}
		
		if(DashboardMongoDBCache.useMongoDB()){
			try{
				String jsonPage = DashboardMongoDBCache.readPageInJson(userBean, uuid);
				return (jsonPage != null) ? Response.ok(jsonPage).build() : Response.status(Status.NOT_FOUND).build();
			}catch(Exception e){
				logger.error(e);
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			}
		}else{
			return Response.status(Status.NOT_FOUND).build();
		}
	}
	
	@POST
	@Path("page")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response setPage(final String pageData) throws IOException{
		Response isSecure = securityCheck();
		if(isSecure.getStatusInfo() != Status.OK){
			return isSecure;
		}
		
		if(!userBean.isSuperUserOwnerOrAdmin() && !userBean.hasEditDashboardPermission()){
			return Response.status(Status.FORBIDDEN).build();
		}
		
		BasicDBObject pageObj = (BasicDBObject)com.mongodb.util.JSON.parse(pageData);
		
		if(pageObj == null){
			return Response.status(Status.BAD_REQUEST).build();
		}
		
		String dir = pageObj.getString("@dir");
		String name = pageObj.getString("@name");
		String dashboardOrder = pageObj.getString("@dashboardOrder");
		
		if(dir == null || name == null){
			return Response.status(Status.BAD_REQUEST).build();
		}
		
		String uuid = null;
		try{
			uuid = DashboardWebServiceHelper.addUuidAttrIfNotExists(pageObj);
			OMElement pageEl = DashboardMongoDBCache.pageElementFromDBObject(pageObj);
			
			BasicDBList  layoutV2 = (BasicDBList)pageObj.get("LayoutV2");
			if(layoutV2 != null){
				DashboardMongoDBCache.writeLayoutMetadata(uuid, layoutV2);
				pageObj.removeField("LayoutV2");
			}
			
			writePage(dir, pageEl, name, dashboardOrder, userBean.getCatalogManager(), userBean);
		}catch(Exception ex){
			logger.error(ex);
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
		
		
		return Response.ok("{ \"uuid\" : \"" + uuid + "\" }").build();
	}
	
	//Taken and adapted from DashboardWebService
	private void writePage(String dir, OMElement page, String file, String order, CatalogManager cm, SMIWebUserBean ub) throws CatalogAccessException, SwapSpaceInProgressException, XMLStreamException, IOException{
		if(dir == null || dir.isEmpty()){
			throw new IllegalArgumentException("Invalid directory");
		}
		
		CatalogManager.checkForSwapSpaceOperation(cm);
		
		String dashboarddir = cm.getCatalogRootPath() + File.separator + convertFileSeparator(dir);
		if(dashboarddir.indexOf("private") != -1){
			dashboarddir = cm.addUserFolderToPrivate(dashboarddir);
		}
		File dbdir = new File(dashboarddir);
		if(!dbdir.exists()){	
			if(!cm.createFolder(dashboarddir)){
				throw new CatalogAccessException("Cannot create dashboard directory");
			}
		}
		
		CatalogPermission perm = cm.getPermission(dashboarddir);
		if(!perm.canModify(ub.getUserGroups(), ub.isSuperUserOwnerOrAdmin())){
			throw new CatalogAccessException("Cannot modify directory");
		}
		
		boolean newDashboardFolder = false;
		
		//Write the dashboards.xml if it doesn't exist to indicate this as a dashboard directory
		String dashboardXml = dashboarddir + File.separator + DashboardWebService.DASH_XML_FILE;
		File indicator = new File(dashboardXml);
		if(!indicator.exists()){
			if(!indicator.createNewFile()){
				throw new CatalogAccessException("Cannot convert directory to dashboard directory");
			}
			
			int orderNum = (order != null && !order.isEmpty()) ? Integer.parseInt(order) : 9999;
			writeDashboardOrderCreatedBy(dashboardXml, orderNum, ub.getUserName());
			newDashboardFolder = true;
		}
		
		//Write the Page xml
		String filepath = dashboarddir + File.separator + file;
		if(!file.endsWith(DashboardWebService.DOT_PAGE)){
			filepath += DashboardWebService.DOT_PAGE;
		}
		
		//Add creator of the page
		if(!(new File(filepath)).exists()){
			XmlUtils.addAttribute(OMAbstractFactory.getOMFactory(), page, DashboardWebService.CREATED_BY, ub.getUserName(), null);
		}
		
		DashboardWebServiceHelper.writeXmlFile(filepath, page);
		
		//Write to Mongo
		if(DashboardMongoDBCache.useMongoDB()){
			DashboardMongoDBCache.upsertPage(ub, new File(filepath));
		
			//Re-cache the dashboard folders order
			if(newDashboardFolder){
				DashboardMongoDBCache.regenerateDashboardsOrdersPaths(ub);
			}
		}
	}
	
	//Taken and adapted from DashboardWebService
	private String convertFileSeparator(String path){
		return path.replace('/', File.separatorChar);
	}
	
	//Taken and adapted from DashboardWebService
	private void writeDashboardOrderCreatedBy(String file, int index, String createdBy) throws XMLStreamException, IOException{
		OMElement pageRoot = null;
		
		//Write directly if the file is empty..
		File target = new File(file);
		if(target.exists() && target.length() == 0){
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMNamespace ns = null;
			pageRoot = factory.createOMElement("Configuration", ns);
		}else{
			pageRoot = DashboardWebServiceHelper.readXmlFile(file);
		}
		OMElement orderEl = pageRoot.getFirstChildWithName(new QName("", "Order"));	
		if(orderEl == null){
			XmlUtils.addContent(OMAbstractFactory.getOMFactory(), pageRoot, "Order", index, null);
		}else{
			orderEl.setText(Integer.toString(index));
		}
		
		//User who first created the dashboard
		if(createdBy != null){
			XmlUtils.addAttribute(OMAbstractFactory.getOMFactory(), pageRoot, "createdBy", createdBy, null);
		}
		
		DashboardWebServiceHelper.writeXmlFile(file, pageRoot);
	}
	
	public class HttpGetWithEntity extends org.apache.commons.httpclient.methods.PostMethod{
		public final static String METHOD_NAME = "GET";

		public HttpGetWithEntity(String queryUrl) {
			super(queryUrl);
		}

		@Override
		public String getName(){
			return METHOD_NAME;
		}
	}
}
