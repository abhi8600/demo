package com.successmetricsinc.rest;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.security.SecurityUtil;

public class BaseServices {

	protected SMIWebUserBean userBean;
	protected HttpServletRequest request;
	protected ServletContext context;
	
	public BaseServices(HttpServletRequest request, ServletContext context){
		this.request = request;
		this.context = context;
	}
	
	@SuppressWarnings("unused")
	protected Response securityCheck(){
		HttpSession session = request.getSession(false);
		boolean passSession = false;
		if (session == null || session.isNew()) {
			String sessionId = request.getHeader("X-BirstSessionId");
			if (sessionId == null || sessionId.trim().length() == 0) {
				return Response.status(Status.UNAUTHORIZED).build();
			}
			session = (HttpSession)context.getAttribute(sessionId);
			passSession = true;
		}
		if (session == null)
		{
			return Response.status(Status.UNAUTHORIZED).build();
		}
		if (!SecurityUtil.validateCSRF(request, passSession ? session : null))
		{
			return Response.status(Status.PRECONDITION_FAILED).entity("CSRF Failure").build();
		}
		
		userBean = (SMIWebUserBean) session.getAttribute("userBean");
		if (userBean == null)
		{
			return Response.status(Status.UNAUTHORIZED).build();
		}
		
		return Response.ok().build();
	}
}
