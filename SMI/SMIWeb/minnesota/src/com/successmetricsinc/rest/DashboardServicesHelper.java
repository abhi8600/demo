package com.successmetricsinc.rest;

import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.axiom.om.OMElement;
import org.codehaus.jettison.mapped.MappedNamespaceConvention;
import org.codehaus.jettison.mapped.MappedXMLStreamWriter;

public class DashboardServicesHelper {
	public static final String BWS = "com.successmetricsinc.WebServices.BirstWebServiceResult";
	
	public static String omElementToJson(OMElement element) throws XMLStreamException{
		element = trimWSResult(element);
		
		ByteArrayOutputStream bytesOut = new ByteArrayOutputStream();
		MappedNamespaceConvention mnc = new MappedNamespaceConvention();
		XMLStreamWriter jsonWriter = new MappedXMLStreamWriter(mnc, new OutputStreamWriter(bytesOut));
        
		element.serializeAndConsume(jsonWriter);
		jsonWriter.writeEndDocument();
			
		return bytesOut.toString();
	}
	
	public static OMElement trimWSResult(OMElement element){
		if(element != null && element.getLocalName().equals(BWS)){
			element.setLocalName("BirstWS");
		}
		return element;
	}
}
