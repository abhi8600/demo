package com.successmetricsinc.rest;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.DELETE;
import javax.ws.rs.PathParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.log4j.Logger;
import org.eclipse.jdt.internal.compiler.ast.ThisReference;

import com.successmetricsinc.queueing.Message;
//import com.successmetricsinc.queueing.Message.MessageType;
import com.successmetricsinc.queueing.producer.QueuePublishManager;
import com.successmetricsinc.queueing.qClient.Exceptions.MessengerException;
import com.successmetricsinc.queueing.qClient.MessengerFactory.Queue;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.security.SecurityUtil;


@Path("/indexer")
public class IndexingServices {
	@Context private HttpServletRequest request;
	@Context private ServletContext context;
	
	private SMIWebUserBean userBean;
	private static Logger logger = Logger.getLogger(IndexingServices.class);
	
	public static final String ES_SERVER_URL = "ES_Url";
	public static final String ES_ENABLED = "ES_Enabled";
	
	private static Boolean esEnabled;
	
	enum MethodEnum {
		Get, Post, Delete, Put
	}
	
	public IndexingServices(@Context HttpServletRequest req, @Context ServletContext ctx){
		this.request = req;
		this.context = ctx;
	}
	
	@POST
	@Path("report/{report}/newName/{newName}")
	public Response renameReport(@PathParam("report") String report, @PathParam("newName") String newName){
		if(report == null || newName == null || report.length() == 0 || newName.length() == 0 || 
				!(report.endsWith(".AdhocReport") || report.endsWith(".dashlet")) ||
				!(newName.endsWith(".AdhocReport") || newName.endsWith(".dashlet"))){
			return Response.status(Status.BAD_REQUEST).build();
		}
			
		String reportPath = report.substring(0, report.lastIndexOf("/"));
		Response rp = deleteReport(report);
		if(rp.getStatusInfo() == Status.OK){
			rp = upsertReport(reportPath + "/" + newName);
		}
			
		return rp;
	}
	
	@POST
	@Path("report/{report}")
	public Response upsertReport(@PathParam("report") String report){
		return callESReportByMethod(MethodEnum.Post, report);
	}
	
	@DELETE
	@Path("report/{report}")
	public Response deleteReport(@PathParam("report") String report){
		return callESReportByMethod(MethodEnum.Delete, report);
	}
	
	private Response callESReportByMethod(MethodEnum methodType, String report){
		//return asyncCallIndexer(methodType, report);
		return callElasticSearchDirectlyByMethod(methodType, report);
	}
	
	private Response callElasticSearchDirectlyByMethod(MethodEnum methodType, String report){
		//Enabled?
		if(!isESEnabled()){
			logger.debug("ES_Enabled is false, no indexing calls will be made");
			return Response.status(Status.FORBIDDEN).build();
		}
		
		Response isSecure = securityCheck();
		if(!(isSecure.getStatusInfo() == Status.OK)){
			return isSecure;
		}
		
		//Must all start with /
		if(!report.startsWith("/")){
			report = "/" + report;
		}
		
		try {
			String reportUrl = getESReportUrl(report);
			HttpMethod method = null;
			switch(methodType){
			case Get:
				method = new GetMethod(reportUrl);
				break;
			case Post: 
				method = new PostMethod(reportUrl);
				break;
			case Delete:
				method = new DeleteMethod(reportUrl);
				break;
			case Put: 
				method = new PutMethod(reportUrl);
				break;
			}
			return callESReport(method);
		} catch (UnsupportedEncodingException e) {
			logger.error(e);
			return Response.serverError().build();
		}
	}
	/*
	private Response asyncCallIndexer(MethodEnum methodType, String report){
		//Enabled?
		if(!isESEnabled()){
			logger.debug("ES_Enabled is false, no indexing calls will be made");
			return Response.status(Status.FORBIDDEN).build();
		}
		
		Response isSecure = securityCheck();
		if(!(isSecure.getStatusInfo() == Status.OK)){
			return isSecure;
		}
		
		//Must all start with /
		if(!report.startsWith("/")){
			report = "/" + report;
		}
		
		//Async send message
		Message asyncMessage = Message.createMessage(MessageType.Async_Indexing, userBean.getUserName(), userBean.getRepository().getApplicationIdentifier());
		asyncMessage.setMessageDataFileName(methodType.name() + "_" + report);
		
		try {
			QueuePublishManager.publishMessage(Queue.INDEXER, asyncMessage, 2);
		} catch (Exception e) {
			logger.error("Unable to publish message: " + e.getMessage(), e);
			
			//Revert to direct way
			return callElasticSearchDirectlyByMethod(methodType, report);
		}
		
		return Response.noContent().build();
	}*/
	
	private Response callESReport(HttpMethod method){
		HttpClient client = new HttpClient();
		client.getParams().setSoTimeout(30000);
		
		try {
			int rc = client.executeMethod(method);
			if(rc != 200){
				String responseBody = method.getResponseBodyAsString();
				responseBody = (responseBody == null) ? "" : responseBody;
				logger.error("ES " + method.getName() + " request (" + method.getPath() + ") failed, statusCode = " + rc + " message " + responseBody);
				return Response.status(rc).build();
			}else{
				
			}
		} catch (HttpException e) {
			logger.error("Failed in http call to " + getESUrl() + method.getPath(), e);
			return Response.serverError().build();
		} catch (IOException e) {
			logger.error("Failed in http call to " + getESUrl() + method.getPath(), e);
			return Response.serverError().build();
		}
		
		return Response.ok().build();
	}
	
	/* 
	 * Security check - valid session, user, authorization
	 */
	private Response securityCheck(){
		HttpSession session = request.getSession(false);
		boolean passSession = false;
		if (session == null || session.isNew()) {
			String sessionId = request.getHeader("X-BirstSessionId");
			if (sessionId == null || sessionId.trim().length() == 0) {
				return Response.status(Status.UNAUTHORIZED).build();
			}
			session = (HttpSession)context.getAttribute(sessionId);
			passSession = true;
		}
		if (session == null)
		{
			return Response.status(Status.UNAUTHORIZED).build();
		}
		if (!SecurityUtil.validateCSRF(request, passSession ? session : null))
		{
			return Response.status(Status.PRECONDITION_FAILED).entity("CSRF Failure").build();
		}
		
		userBean = (SMIWebUserBean) session.getAttribute("userBean");
		if (userBean == null)
		{
			return Response.status(Status.UNAUTHORIZED).build();
		}
		
		return Response.ok().build();
	}
	
	public String getESReportUrl(String reportPath) throws UnsupportedEncodingException{
		return getESSpaceReportUrl() + "/" + URLEncoder.encode(reportPath, "UTF-8");
	}
	
	/**
	 * URL of ES report 
	 * @return
	 */
	public String getESSpaceReportUrl(){
		String spaceId = userBean.getRepository().getApplicationIdentifier();
		return getESUrl() + "/_spaceIndexer/" + spaceId + "/report";
	}
	
	/**
	 * Address of ElasticSearch server
	 * @return
	 */
	public static String getESUrl(){
		String serverUrl = System.getProperty(ES_SERVER_URL);
		if(serverUrl == null){
			serverUrl = System.getenv(ES_SERVER_URL);
			if(serverUrl == null){
				serverUrl = "http://localhost:9200";
			}
		}
		
		return serverUrl;
	}
	
	public static boolean isESEnabled(){
		if(esEnabled == null){
			String esEnabledStr = System.getProperty(ES_ENABLED);
			if(esEnabledStr == null){
				esEnabledStr = System.getenv(ES_ENABLED);
				if(esEnabledStr == null){
					esEnabledStr = "false"; //default off
				}
			}
			esEnabled = new Boolean("true".equalsIgnoreCase(esEnabledStr));
		}
		return esEnabled.booleanValue();
	}
}
