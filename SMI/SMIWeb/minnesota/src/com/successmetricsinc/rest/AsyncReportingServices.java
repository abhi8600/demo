/**
 * 
 */
package com.successmetricsinc.rest;

import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;

import com.successmetricsinc.ServerContext;
import com.successmetricsinc.adhoc.AdhocReportHelper;
import com.successmetricsinc.security.Net;

/**
 * @author agarrison
 *
 */
@Path("/async")
public class AsyncReportingServices {
	@Context private ServletContext context;
	@Context private HttpServletRequest request;
	private static Logger logger = Logger.getLogger(AsyncReportingServices.class);
	
	public AsyncReportingServices(@Context ServletContext ctx, @Context HttpServletRequest request){
		this.context = ctx;
		this.request = request;
	}
	
	@POST
	@Path("start")
	public Response start() {
		if (! isTrustedRequest(request)) {
			logger.error("Unauthorized access of start async service");
			return Response.status(Status.UNAUTHORIZED).build();
		}
		AdhocReportHelper.setAsynchReportGenerationEnabled(true);
		ServerContext.startupAsyncProcessing(context);
		return Response.status(Status.OK).build();
	}

	@POST
	@Path("stop")
	public Response stop() {
		if (! isTrustedRequest(request)) {
			logger.error("Unauthorized access of stop async service");
			return Response.status(Status.UNAUTHORIZED).build();
		}
		AdhocReportHelper.setAsynchReportGenerationEnabled(false);
		try{
			ServerContext.closeAllPublishersAndSubscribers();
			ServerContext.stopQueueHealthCheck();
		}catch(Exception ex){
			logger.error("Error while stopping queue publishers and subscribers ", ex);
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR); 
		}
		return Response.status(Status.OK).build();
	}
	
	private boolean isTrustedRequest(HttpServletRequest request) {
		String requestIP = request.getRemoteAddr();
		List<String> allowedIPs = ServerContext.getLoginServiceAllowedIPs();

		return Net.containIp(allowedIPs, requestIP);
	}
}
