package com.successmetricsinc.preferences;

/**
 * Represents a single user preference.
 * The 'type' must be set when adding or updating a preference.
 * The 'type' will always be 'Any' when retrieving a preference.
 * @author pconnolly
 */
public class Preference {
	public enum Type {
		Global,
		User,
		UserSpace,
		Any
	};
	
	private Type type;
	private String key;
	private String value;
	/**
	 * @return the type
	 */
	public Type getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(Type type) {
		this.type = type;
	}
	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}
	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}
	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
}
