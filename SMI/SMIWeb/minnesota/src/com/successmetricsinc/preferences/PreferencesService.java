package com.successmetricsinc.preferences;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.axis2.AxisFault;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.log4j.Logger;

import com.successmetricsinc.ServerContext;
import com.successmetricsinc.preferences.PreferencesServiceStub.ArrayOfString;
import com.successmetricsinc.preferences.PreferencesServiceStub.ArrayOfUserPreference;
import com.successmetricsinc.preferences.PreferencesServiceStub.GetUserPreferences;
import com.successmetricsinc.preferences.PreferencesServiceStub.GetUserPreferencesResponse;
import com.successmetricsinc.preferences.PreferencesServiceStub.UserPreference;

/**
 * Provides a service for retrieving and adding/updating user preferences.
 * Shields the calling program from the Axis2 stub and callback handler classes. 
 * @author pconnolly
 */
public class PreferencesService {

	private static Logger logger = Logger.getLogger(PreferencesService.class);
	
	public static Map<String, String> getUserPreferences(final List<String> preferenceKeys) {
		logger.trace("'getUserPreferences()' specifying keys:");
		for (String key : preferenceKeys) {
			logger.trace("..." + key);
		}
		
		// Initialize the Axis2 interface to Acorn PreferencesService.asmx
		final String endpoint = ServerContext.getAcornPreferencesEndpoint(); 
		PreferencesServiceStub service;
		try {
			service = new PreferencesServiceStub(endpoint);
			// IIS does not support chunked encoding
			service._getServiceClient().getOptions().setProperty(HTTPConstants.CHUNKED, "false");
		} catch (AxisFault e) {
			logger.error("Could not create PreferencesServiceStub instance", e);
			return null;
		}
		// Build the Axis2-specific input parameters
		GetUserPreferences getUserPreferences = new GetUserPreferences();
		ArrayOfString preferencesKeys = new ArrayOfString();
		for (String key : preferenceKeys) {
			preferencesKeys.addString(key);
		}
		getUserPreferences.setPreferenceKeys(preferencesKeys);
		// Get the user preferences (via Acorn PreferencesService.asmx webservice)
		GetUserPreferencesResponse response;
		try {
			response = service.getUserPreferences(getUserPreferences);
		} catch (RemoteException e) {
			logger.error("Remote connection to Acorn Preferences webservice failed.", e);
			return null;
		}
		// Convert result to SMIWeb-centric Preference result
		ArrayOfUserPreference preferences = response.getGetUserPreferencesResult();
		Map<String, String> preferencesMap = new HashMap<String, String>();
		for (UserPreference preference : preferences.getUserPreference()) {
			logger.trace("Retrieved user preference: key=[" + preference.getKey() + "]; value=[" 
					+ preference.getValue() + "].");
			preferencesMap.put(preference.getKey(), preference.getValue());
		}
		
		return preferencesMap;
	}
}
