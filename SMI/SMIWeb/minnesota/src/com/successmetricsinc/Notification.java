/**
 * $Id: Notification.java,v 1.14 2012-07-24 19:17:43 ricks Exp $
 *
 * Copyright (C) 2007-2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
/**
 * Class to send notifications to users
 * 
 */
package com.successmetricsinc;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.regex.Pattern;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.activation.DataHandler;
import javax.imageio.ImageIO;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import javax.mail.util.ByteArrayDataSource;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JExcelApiExporter;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRCsvExporterParameter;
import net.sf.jasperreports.engine.export.JRGraphics2DExporter;
import net.sf.jasperreports.engine.export.JRGraphics2DExporterParameter;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.JRXlsAbstractExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRPptxExporter;

import org.apache.log4j.Logger;

import com.successmetricsinc.query.Filter;
import com.successmetricsinc.query.Filter.FilterType;
import com.successmetricsinc.query.JasperDataSourceProvider;
import com.successmetricsinc.query.ResultSetCache;
import com.successmetricsinc.util.JasperUtil;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.logging.AuditLog;
import com.sun.mail.smtp.SMTPSendFailedException;

import org.apache.axiom.util.stax.xop.ContentIDGenerator;
import org.apache.commons.codec.binary.Base64;

public class Notification
{
	private static Logger logger = Logger.getLogger(Notification.class);
	private String smtpServer;
	private javax.mail.Session mailSession;
	private static Hashtable<String, Notification> notificationMap = new Hashtable<String, Notification>();
	private BlockingQueue<Message> messageList;
	private MailSendThread sendThread;
	private int SLEEP_TIME = 5 * 1000; // time to sleep between mail messages
	private static final String COMPRESSION_NONE = "none";
	private static final String COMPRESSION_ZIP = "zip";
	private static final String COMPRESSION_GZIP = "gzip";
	
	public static synchronized Notification getInstance(String mailHost)
	{
		Notification notify = notificationMap.get(mailHost);
		if (notify != null)
			return notify;
		notify = new Notification(mailHost);
		notify.start();
		notificationMap.put(mailHost, notify);
		return notify;
	}

	/**
	 * Create a notification session
	 * 
	 * @param mailHost
	 *            SMTP server
	 */
	
	private Notification(String mailHost)
	{
		messageList = new LinkedBlockingQueue<Message>();
		smtpServer = mailHost;
		sendThread = new MailSendThread(mailHost, messageList);
	}
	
	private void start()
	{
		sendThread.start();
	}

	public int remainingMessages()
	{
		return messageList.size();
	}
	
	public void logMessageList()
	{
		try
		{
			for (Message msg : messageList)
			{
				Address[] addresses = msg.getAllRecipients();
				StringBuilder sb = new StringBuilder();
				for (Address addr: addresses)
				{
					if (sb.length() > 0)
						sb.append(',');
					sb.append(addr.toString());
				}
				logger.info("Message Queue: Subject: " + msg.getSubject() + ", To: " + sb);
			}
		}
		catch (MessagingException e)
		{
			logger.debug(e, e);
		}
	}

	/**
	 * set up the basic message envelope
	 * 
	 * @param from
	 *            who the message is from, can include $P and V variables
	 * @param to
	 *            who the message is to, can include $P and V variables
	 * @param subject
	 *            subject of the message, can include $P and V variables
	 * @return the Message object, ready for adding contents and transporting
	 */
	private Message setUpMessage(String from, String to, String subject)
	{
		try
		{
			Properties props = new Properties();
			props.put("mail.smtp.host", smtpServer);
			if(mailSession == null)
			{
				mailSession = javax.mail.Session.getInstance(props, null);
			}
			Message msg = new MimeMessage(mailSession);
			InternetAddress addressFrom = new InternetAddress(from);
			msg.setFrom(addressFrom);
			msg.setReplyTo(new InternetAddress[]
			{ addressFrom });
			String[] recipients = to.split(";");
			InternetAddress[] addressTo = new InternetAddress[recipients.length];
			for (int i = 0; i < recipients.length; i++)
			{
				addressTo[i] = new InternetAddress(recipients[i]);
			}
			msg.setRecipients(Message.RecipientType.TO, addressTo);
			msg.setSubject(subject);
			msg.setSentDate(new Date());
			return msg;
		} catch (MessagingException ex)
		{
			logger.error(ex.getMessage(), ex);
			return null;
		}
	}

	/**
	 * send a message with optional body text and optional report
	 * 
	 * @param from
	 *            who the message is from, can include $P and V variables
	 * @param to
	 *            who the message is to, can include $P and V variables
	 * @param subject
	 *            subject of the message, can include $P and V variables
	 * @param body
	 *            body of the message, can include $P and V variables (this argument can be null)
	 * @param report
	 *            pathname of the compiled report to embed, can include $P and V variables (this argument can be null)
	 * @param session
	 *            user session
	 * @param rsc
	 *            result set cache
	 * @param paramMap
	 *            parameter map (hashmap of Operation parameters and their values, passed to the report generation)
	 * @return true for success, false for failure
	 */	
	@SuppressWarnings("unchecked")
	public boolean sendReport(Repository r, String name, String from, String to, String subject, String body, String report, Session session, UserBean userBean,
			ResultSetCache rsc, Map<String, Object> paramMap, Map<String, String> operatorMap, Boolean applyAllPrompts,Map<String, Boolean> isExpressionMap, 
			String type, String csvSeparator, String csvExtension, String compressionFormat)
	{
		Long id = Long.valueOf(Thread.currentThread().getId());
		try
		{	
			long start = System.currentTimeMillis();
			Message msg = setUpMessage(from, to, subject);
			if (msg == null)
				return false;
			JasperDataSourceProvider jdsp = new JasperDataSourceProvider(r, rsc);
			jdsp.setApplyAllPrompts(applyAllPrompts);
			// create the report
			List<Filter> filters = new ArrayList<Filter>();
			if (paramMap != null)
			{
				Filter filter;
				// pass parameters to GETPROMPTVALUE (passed in via the 'To' report)
				for (String key : paramMap.keySet())
				{
					Object value = paramMap.get(key);
					String operatorValue= operatorMap.get(key);
					if(value == null)
					{
						logger.warn("Value is null for param " + key);
						continue;
					}

					value = formatPipeDelimitedValues(value);
					List<String> values = new ArrayList<String>(1);
					// what if the value itself is an array List
					if(value instanceof ArrayList)
					{
						List<String> valueList = (ArrayList<String>)value;
						for(String eachValue : valueList)
						{
							values.add(eachValue);
						}
					}
					else
					{
						values.add(value.toString());
					}
					
					operatorValue = operatorValue == null ? "=" : operatorValue;
					filter = new Filter(operatorValue, values, "OR", FilterType.DATA, key);
					if(isExpressionMap.containsKey(key)){
						// if isExpression is true, set createFiltersFrom as false i.e. do not attempt to create filters
						filter.setCreateFiltersFrom(!isExpressionMap.get(key));
					}
					
					filters.add(filter);
				}
			}
			Session.setParams(filters);
			jdsp.setSession(session);
			jdsp.setParams(filters);
			JRDataSource jds = null;
			Session.addThreadRequest(id, session);
			if (userBean == null) // from Email.java or when no locale and timezone are sent from Acorn to deliver report - set default parameters
			{
				userBean = new UserBean();
				userBean.setRepository(r);
				userBean.setLocale(Locale.getDefault());
				userBean.setTimeZone(r.getServerParameters().getDisplayTimeZone());
			}
			UserBean.addThreadRequest(id, userBean);
			userBean.repSession = session;
			
			// use the reportMap if it exists - improves performance when iterating over the same report for multiple
			// recipients
			logger.warn("Trying to load report " + report + " to email.");
			JasperReport jreport = ServerContext.getReport(report, r, paramMap, Session.isPrint(), Session.isPdf(), 
					Session.getDashletPrompts(), session, false);
			if(jreport.getQuery() != null)
			{
				jds = jdsp.create(jreport, paramMap);	
			}
			List<String> typeList = new ArrayList<String>();
			for (String s : type.split(","))
				typeList.add(s.toLowerCase());
			if (paramMap == null)
				paramMap = JasperUtil.createParameterMap();
			paramMap.put(JRParameter.REPORT_LOCALE, userBean.getLocale());
			paramMap.put(JRParameter.REPORT_TIME_ZONE, userBean.getTimeZone());
			
			JasperPrint jpPagination = null;
			if (typeList.contains("html") || typeList.contains("atc_html") || typeList.contains("pdf") ||
					typeList.contains("ppt") || typeList.contains("rtf"))
				jpPagination = JasperFillManager.fillReport(jreport, paramMap, jds);
			JasperPrint jpNoPagination = null;
			if (typeList.contains("excel") || typeList.contains("csv")) {
				paramMap.put("IS_IGNORE_PAGINATION", Boolean.TRUE);
				jpNoPagination = JasperFillManager.fillReport(jreport, paramMap, jds);
			}
			
			// if need for zip compression, create a map of individual filename to content array
			// And then zip all the files
			Map<String, byte[]> map = null;
			if(compressionFormat !=null && COMPRESSION_ZIP.equalsIgnoreCase(compressionFormat))
			{
				map = new HashMap<String, byte[]>();
			}
			
			MimeMultipart mm = new MimeMultipart("mixed");
			if (typeList.contains("html")) {
				exportHTML(jpPagination, mm, body);
//				exportHtmlFileUsingPngs(jpPagination, name, mm, compressionFormat, map);				
			}
			else if (body != null)
			{
				body = fixupBody(body);
				MimeBodyPart mbp1 = new MimeBodyPart();
				mbp1.setDataHandler(new DataHandler(new ByteArrayDataSource(Util.convertImageSizesToPixels(new StringBuffer(body)).toString(), "text/html")));
				mm.addBodyPart(mbp1);
			}
			if (typeList.contains("atc_html"))
				exportHtmlFile(jpPagination, name, mm);
			if (typeList.contains("pdf"))
				exportPDF(jpPagination, name, mm, compressionFormat, map);
			if (typeList.contains("excel"))
				exportExcel(jpNoPagination, name, mm, compressionFormat, map);
			if (typeList.contains("ppt"))
				exportPPT(jpPagination, name, mm, compressionFormat, map);
			if (typeList.contains("rtf"))
				exportRTF(jpPagination, name, mm, compressionFormat, map);
			if (typeList.contains("csv"))
				exportCSV(jpNoPagination, name, mm, csvSeparator, csvExtension, compressionFormat, map);
			
			if(map != null && map.size() > 0)
			{
				compressInZipAndAddToMessage(map, name, mm);
			}
			
			msg.setContent(mm);
			// send the message
			sendThread.send(msg);
			logger.info("Sent email with report to: " + to + ", from: " + from + ", on: " + subject);
			AuditLog.log("scheduled", report, null, System.currentTimeMillis() - start);
		} catch (Exception ex)
		{
			logger.error(ex.getMessage(), ex);
			return false;
		}
		finally
		{
			UserBean.removeThreadRequest(id);
			Session.removeThreadRequest(id);
		}
		return true;
	}

	private Object formatPipeDelimitedValues(Object obj)
	{
		if(obj instanceof String)
		{
			String str = (String) obj;
			String[] array = str.split(Pattern.quote("||"));
			if(array.length > 1)
			{
				ArrayList<String> arrayList = new ArrayList<String>();
				for (String st : array){
					arrayList.add(st);
				}
				return arrayList;	
			}
		}
		return obj;
	}
	
	private void exportPDF(JasperPrint jp, String name, MimeMultipart mm, 
			String compressionFormat, Map<String, byte[]> addToMapIfZip) throws IOException, JRException, MessagingException
	{
		JRPdfExporter exporter = new JRPdfExporter();
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
		exporter.exportReport();
		if(compressionFormat == null || compressionFormat.length() == 0 || COMPRESSION_NONE.equalsIgnoreCase(compressionFormat))
		{
			// attach the report and body to the message
			BodyPart bp = new MimeBodyPart();
			bp.setDataHandler(new DataHandler(new ByteArrayDataSource(baos.toByteArray(), "application/pdf;charset=utf-8")));	
			bp.setFileName(MimeUtility.encodeText(name, "utf-8", "B") + ".pdf" );		
			mm.addBodyPart(bp);
		}
		else
		{
			performCompression(compressionFormat, baos.toByteArray(),name + ".pdf", addToMapIfZip, mm);
		}
	}
	
	private void  performCompression(String compressionFormat, byte[] contentByteArray, 
			String gzipFileNameExtension, Map<String, byte[]> addToMapIfZip,
			MimeMultipart mm ) throws MessagingException, IOException
	{
		// for gzip compression create a .gzip file and add it to the body part
		if(COMPRESSION_GZIP.equalsIgnoreCase(compressionFormat))
		{
			compressInGZipAndAddToMessage(contentByteArray, gzipFileNameExtension, mm);
		}
		else if(COMPRESSION_ZIP.equalsIgnoreCase(compressionFormat))
		{
			addByteArrayToMap(gzipFileNameExtension,contentByteArray, addToMapIfZip);
		}
	}
	
	private void exportRTF(JasperPrint jp, String name, MimeMultipart mm,
			String compressionFormat, Map<String, byte[]> addToMapIfZip) throws IOException, JRException, MessagingException
	{
		JRRtfExporter exporter = new JRRtfExporter();
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
		exporter.exportReport();
		if(compressionFormat == null || compressionFormat.length() == 0 || COMPRESSION_NONE.equalsIgnoreCase(compressionFormat))
		{
			// attach the report and body to the message
			BodyPart bp = new MimeBodyPart();
			bp.setDataHandler(new DataHandler(new ByteArrayDataSource(baos.toByteArray(), "application/rtf;charset=utf-8")));	
			bp.setFileName(MimeUtility.encodeText(name, "utf-8", "B") + ".rtf" );	
			mm.addBodyPart(bp);
			
		}
		else
		{
			performCompression(compressionFormat, baos.toByteArray(), name + ".rtf", addToMapIfZip, mm);
		}
	}
	

	private String fixupBody(String body)
	{
		// Fixup Flex non-standard HTML
		body = body.replaceAll("<TEXTFORMAT.*?>", "");
		body = body.replaceAll("</TEXTFORMAT.*?>", "");
		body = body.replaceAll("LETTERSPACING=\".*?\"", "");
		body = body.replaceAll("KERNING=\".*?\"", "");
		int i = body.indexOf("<FONT ");
		while (i >= 0)
		{
			int end = body.indexOf('>', i + 1);
			int faceindex = body.indexOf("FACE=\"", i);
			String face = null;
			if (faceindex >= 0)
				face = body.substring(faceindex + 6, body.indexOf('"', faceindex + 7));
			int sizeindex = body.indexOf("SIZE=\"", i);
			String size = null;
			if (sizeindex >= 0)
				size = body.substring(sizeindex + 6, body.indexOf('"', sizeindex + 7));
			int colorindex = body.indexOf("COLOR=\"", i);
			String color = null;
			if (colorindex >= 0)
				color = body.substring(colorindex + 7, body.indexOf('"', colorindex + 8));
			body = body.substring(0, i) + "<span style='" + (face != null ? "font-family:" + face : "") + (size != null ? ";font-size:" + size : "")
					+ (color != null ? ";color:" + color : "") + "'>" + body.substring(end + 1);
			i = body.indexOf("<FONT ", i);
		}
		body = body.replaceAll("</FONT>", "</span>");
		return body;
	}
	
	private void exportExcel(JasperPrint jp, String name, MimeMultipart mm,
			String compressionFormat, Map<String, byte[]> addToMapIfZip) throws IOException, JRException, MessagingException
	{
		JExcelApiExporter exporter = new JExcelApiExporter();
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
		exporter.setParameter(JRXlsAbstractExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
		exporter.setParameter(JRExporterParameter.IGNORE_PAGE_MARGINS, Boolean.TRUE);
		exporter.setParameter(JRXlsAbstractExporterParameter.FORMAT_PATTERNS_MAP, ExportServlet.generateFormatPatterns(jp));
		exporter.exportReport();
		
		if(compressionFormat == null || compressionFormat.length() == 0 || COMPRESSION_NONE.equalsIgnoreCase(compressionFormat))
		{
			// attach the report and body to the message
			BodyPart bp = new MimeBodyPart();
			bp.setDataHandler(new DataHandler(new ByteArrayDataSource(baos.toByteArray(), "application/vnd.ms-excel;charset=utf-8")));	
			bp.setFileName(MimeUtility.encodeText(name, "utf-8", "B") + ".xls" );	
			mm.addBodyPart(bp);
		}
		else
		{
			performCompression(compressionFormat, baos.toByteArray(), name + ".xls", addToMapIfZip, mm);
		}
	}
	
	private void exportPPT(JasperPrint jp, String name, MimeMultipart mm,
			String compressionFormat, Map<String, byte[]> addToMapIfZip) throws IOException, JRException, MessagingException
	{
		JRPptxExporter exporter = new JRPptxExporter();
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
		exporter.exportReport();
		if(compressionFormat == null || compressionFormat.length() == 0 || COMPRESSION_NONE.equalsIgnoreCase(compressionFormat))
		{
			// attach the report and body to the message
			BodyPart bp = new MimeBodyPart();
			bp.setDataHandler(new DataHandler(new ByteArrayDataSource(baos.toByteArray(), "application/vnd.ms-powerpoint;charset=utf-8")));	
			bp.setFileName(MimeUtility.encodeText(name, "utf-8", "B") + ".ppt" );
			mm.addBodyPart(bp);
		}
		else
		{
			performCompression(compressionFormat, baos.toByteArray(), name + ".ppt", addToMapIfZip, mm);
		}
	}
	
	private void exportCSV(JasperPrint jp, String name, MimeMultipart mm, 
			String csvSeparator, String extension,
			String compressionFormat, Map<String, byte[]> addToMapIfZip) throws IOException, JRException, MessagingException
	{
		JRCsvExporter exporter = new JRCsvExporter();
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
		if(csvSeparator != null && csvSeparator.length() > 0)
		{
			exporter.setParameter(JRCsvExporterParameter.FIELD_DELIMITER, csvSeparator);
		}
		exporter.exportReport();
		String ext = ".csv";
		if(extension != null && extension.startsWith("."))
		{
			ext = extension;
		}
		if(compressionFormat == null || compressionFormat.length() == 0 || COMPRESSION_NONE.equalsIgnoreCase(compressionFormat))
		{
			// attach the report and body to the message
			BodyPart bp = new MimeBodyPart();
			bp.setDataHandler(new DataHandler(new ByteArrayDataSource(baos.toByteArray(), "text/csv;charset=utf-8")));	
			bp.setFileName(MimeUtility.encodeText(name, "utf-8", "B") + ext );
			mm.addBodyPart(bp);
		}
		else
		{
			performCompression(compressionFormat, baos.toByteArray(), name + ext, addToMapIfZip, mm);
		}
	}

	private void exportHTML(JasperPrint jp, MimeMultipart mm, String body) throws IOException, JRException, MessagingException
	{
		StringWriter sw = new StringWriter();
		JRHtmlExporter exporter = new JRHtmlExporter();
		// Create static images for notifications
		exporter.encodeFlashChartsAsImages = true;
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
		JRImageHandler imageHandler = new JRImageHandler();
		exporter.setParameter(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN, Boolean.FALSE);
		exporter.setParameter(JRHtmlExporterParameter.BETWEEN_PAGES_HTML, "");
		exporter.setParameter(JRHtmlExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.FALSE);
		exporter.setImageHandler(imageHandler);
		if (body != null)
			body = fixupBody(body);
		String header = "<html><body MARGINWIDTH=0 MARGINHEIGHT=0 LEFTMARGIN=0 TOPMARGIN=0>" + (body != null ? body : "")
				+ "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td width=\"0%\"></td><td align=\"center\" width=\"100%\">";
		exporter.setParameter(JRHtmlExporterParameter.HTML_HEADER, header);
		String footer = "</td><td width=\"0%\"></td></tr></table></body></html>";
		exporter.setParameter(JRHtmlExporterParameter.HTML_FOOTER, footer);
		exporter.setParameter(JRExporterParameter.OUTPUT_WRITER, sw);
		exporter.setParameter(JRHtmlExporterParameter.SIZE_UNIT, JRHtmlExporterParameter.SIZE_UNIT_PIXEL);
		exporter.setScaleFactor((float) 1.6313725490196078);
		exporter.exportReport();
		
		MimeMultipart cover = new MimeMultipart("alternative");
		// attach the report and body to the message
		BodyPart bp = new MimeBodyPart();
		bp.setText("This is alternative text.");
		bp.setHeader("Content-Type", "text/plain");
		cover.addBodyPart(bp);
		
		bp = new MimeBodyPart();
		StringBuffer sb = sw.getBuffer();
		bp.setDataHandler(new DataHandler(new ByteArrayDataSource(Util.convertImageSizesToPixels(sb).toString().getBytes("UTF-8"), "application/octet-stream;charset=utf-8")));
		bp.setHeader("Content-Type", "text/html; charset=\"utf-8\"");
		bp.setHeader("Content-Transfer-Encoding", "quoted-printable");
		cover.addBodyPart(bp);
		
		BodyPart wrap = new MimeBodyPart();
		wrap.setContent(cover);
		mm.addBodyPart(wrap);
		
		// process the images
		for (Entry<String, byte[]> en : imageHandler.entrySet())
		{
			if (en.getValue() == null)
				continue; // bad image/chart, ignore it (otherwise the email system throws an error trying to send the bad mime attachment)
			bp = new MimeBodyPart();
			bp.setHeader("Content-ID", "<" + en.getKey() + JRImageHandler.BIRST_COM + ">");
			bp.setHeader("Content-Disposition", "inline; filename=\"" + en.getKey() + ".png\"");
			// Include rendered png image
			bp.setDataHandler(new DataHandler(new ByteArrayDataSource( en.getValue(), "image/png;charset=utf-8")));
			mm.addBodyPart(bp);
		}
	}
	
	private void exportHtmlFileUsingPngs(JasperPrint jprint, String name, MimeMultipart mm)
			throws IOException, JRException, MessagingException {
		List<JRPrintPage> pages = jprint.getPages();
		int numberOfPages = 0;
		if (pages != null)
			numberOfPages = jprint.getPages().size();
		if (numberOfPages > 0) {
			MimeBodyPart bp = new MimeBodyPart();
			bp.setHeader("Content-Type", "text/html; charset=\"utf-8\"");
//			bp.setHeader("Content-Transfer-Encoding", "quoted-printable");
			bp.setDisposition(MimeBodyPart.INLINE);
			mm.addBodyPart(bp);
			StringBuffer html = new StringBuffer();
			html.append("<html><body>");
			for (int i = 0; i < numberOfPages; i++) {
				// generate a png for each page.
				String cid = exportPage(jprint, i, mm);
				html.append("<img src=\"cid:" + cid + "\" />");
			}
			html.append("</body></html>");
//			bp.setText(html.toString());
			bp.setContent(html.toString(), "text/html");
		}
	}
	
	private String exportPage(JasperPrint jprint, int page, MimeMultipart mm) throws JRException, IOException, MessagingException {
		Float zoom = Float.valueOf(2.0f);
		BufferedImage image = new BufferedImage((int)(jprint.getPageWidth() * zoom.doubleValue()), 
				(int)(jprint.getPageHeight() * zoom.doubleValue()), 
				BufferedImage.TYPE_INT_ARGB);
		Graphics2D graphics = image.createGraphics();
		JRGraphics2DExporter ex = new JRGraphics2DExporter();
		ex.setParameter(JRExporterParameter.JASPER_PRINT, jprint);
		ex.setParameter(JRGraphics2DExporterParameter.GRAPHICS_2D, graphics);
		ex.setParameter(JRGraphics2DExporterParameter.ZOOM_RATIO, zoom);
		ex.setParameter(JRExporterParameter.PAGE_INDEX, page);
		ex.exportReport();
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		ImageIO.write(image, "png", output);
		output.flush();
		byte[] bytes = output.toByteArray();

		String cid = generateContentId();
		MimeBodyPart imagePart = new MimeBodyPart();
		imagePart.setDataHandler(new DataHandler(new ByteArrayDataSource(bytes, "image/png")));
		imagePart.setContentID(cid);
		imagePart.setDisposition(MimeBodyPart.INLINE);
		mm.addBodyPart(imagePart);
		return cid;
	}
	
	private void exportHtmlFile(JasperPrint jprint, String name, MimeMultipart mm) 
				throws IOException, JRException, MessagingException {
		StringWriter sw = new StringWriter();
		JRHtmlExporter exporter = new JRHtmlExporter();
		// Create static images for notifications
		exporter.encodeFlashChartsAsImages = true;
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jprint);
		HashMap<String, byte[]> imagesMap = new HashMap<String, byte[]>();
		exporter.setParameter(JRHtmlExporterParameter.IMAGES_MAP, imagesMap);
		exporter.setParameter(JRHtmlExporterParameter.IMAGES_URI, "cid:");
		exporter.setParameter(JRHtmlExporterParameter.IS_OUTPUT_IMAGES_TO_DIR, Boolean.FALSE);
		exporter.setParameter(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN, Boolean.FALSE);
		exporter.setParameter(JRHtmlExporterParameter.BETWEEN_PAGES_HTML, "");
		exporter.setParameter(JRHtmlExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.FALSE);
		String header = "<html><body MARGINWIDTH=0 MARGINHEIGHT=0 LEFTMARGIN=0 TOPMARGIN=0>"
				+ "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td width=\"0%\"></td><td align=\"center\" width=\"100%\">";
		exporter.setParameter(JRHtmlExporterParameter.HTML_HEADER, header);
		String footer = "</td><td width=\"0%\"></td></tr></table></body></html>";
		exporter.setParameter(JRHtmlExporterParameter.HTML_FOOTER, footer);
		exporter.setParameter(JRExporterParameter.OUTPUT_WRITER, sw);
		exporter.setParameter(JRHtmlExporterParameter.SIZE_UNIT, JRHtmlExporterParameter.SIZE_UNIT_PIXEL);
//		exporter.setScaleFactor((float) 1.6313725490196078);
		exporter.exportReport();
		
		String htmlStr = sw.toString();
		// embed the images inline in html
		for (Entry<String, byte[]> en : imagesMap.entrySet())
		{
			if (en.getValue() == null)
				continue; // bad image/chart, ignore it (otherwise the email system throws an error trying to send the bad mime attachment)
			byte [] imageBuf = en.getValue();
			
			byte[] encoded = Base64.encodeBase64(imageBuf);
			String base64String = new String(encoded);
			htmlStr = htmlStr.replace("cid:" + en.getKey().toString(), "data:image/png;base64," + base64String);			
		}
		File fiTemp = File.createTempFile("tempDir", "");
		File ATC_Dir = new File (fiTemp.getParent() + "\\ATC");
		ATC_Dir.mkdir();
		File fi = new File (ATC_Dir + "\\" + name + ".htm");		
		PrintWriter pw = new PrintWriter(fi);
		pw.write(htmlStr);
		pw.flush();
		pw.close();
		fiTemp.delete();

		FileInputStream fis = null;
		try
		{
			fis = new FileInputStream(fi);
			ByteArrayDataSource baos = new ByteArrayDataSource(fis, "text/html;charset=utf-8");
			BodyPart bp = new MimeBodyPart();
			bp.setDataHandler(new DataHandler(baos));
			bp.setFileName(	MimeUtility.encodeText(name, "utf-8", "B") + ".htm");
			mm.addBodyPart(bp);
		}
		finally
		{
			if (fis != null)
				fis.close();
		}
	}
	
	private void addByteArrayToMap(String nameWithExt, byte[] byteArray, Map<String, byte[]> map)
	{
		map.put(nameWithExt, byteArray);
	}
	
	private void compressInGZipAndAddToMessage(byte[] contentByteArray, String name, MimeMultipart mm) throws MessagingException, IOException
	{
		byte[] compressedContent = compressInGZip(contentByteArray, name);
		MimeBodyPart bp = new MimeBodyPart();
		bp.setDataHandler(new DataHandler(new ByteArrayDataSource(compressedContent, "application/x-gzip;charset=utf-8")));
		bp.setFileName(MimeUtility.encodeText(name, "utf-8", "B") + ".gz");
		//bp.setFileName(name + ".gz");
		mm.addBodyPart(bp);
	}
	
	private String generateContentId() {
		return ContentIDGenerator.DEFAULT.generateContentID(null);
	}
	
	private byte[] compressInGZip(byte[] contentByteArray, String name) throws IOException
	{
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try{
            GZIPOutputStream gzipOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            gzipOutputStream.write(contentByteArray);
            gzipOutputStream.close();
        } catch(IOException e){
            throw new IOException("Exception in compressing to gzip format : " + name);
        }
        //("Compression ratio %f\n", (1.0f * contentByteArray.length/byteArrayOutputStream.size()));
        return byteArrayOutputStream.toByteArray();
	}
	
	private void compressInZipAndAddToMessage(Map<String, byte[]> allFiles, 
			String outputZipname, MimeMultipart mm) throws MessagingException, IOException
	{
		BodyPart bp = new MimeBodyPart();		
		byte[] compressedContent = compressInZip(allFiles, MimeUtility.encodeText(outputZipname, "utf-8", "B"));
		bp.setDataHandler(new DataHandler(new ByteArrayDataSource(compressedContent, "application/x-zip-compressed;charset=utf-8")));
		bp.setFileName(MimeUtility.encodeText(outputZipname, "utf-8", "B") + ".zip");
		mm.addBodyPart(bp);
	}
	
	private byte[] compressInZip(Map<String, byte[]> allFiles, String outputZipName) throws IOException
	{
		ByteArrayOutputStream byteArrayOutputStream = null;
		ZipOutputStream gzipOutputStream = null;
        try
		{
        	byteArrayOutputStream = new ByteArrayOutputStream();
            gzipOutputStream = new ZipOutputStream(byteArrayOutputStream);
            for (Entry<String, byte[]> entry : allFiles.entrySet())
            {
            	String toAddFileName = entry.getKey();
            	byte[] toAddByteArray = entry.getValue();
            	ZipEntry ze = new ZipEntry(toAddFileName);
            	gzipOutputStream.putNextEntry(ze);
            	gzipOutputStream.write(toAddByteArray);
            }
            
        } catch(IOException e){
            throw new IOException("Exception in compressing in zip format for " + outputZipName);
        }
        finally
        {
        	if(gzipOutputStream != null)
        	{
        		try
        		{
        			gzipOutputStream.close();
        		}catch(Exception ex)
        		{
        			logger.warn("Exception in closing gzip output stream for " + outputZipName);
        		}
        	}
        }
        return byteArrayOutputStream.toByteArray();
	}

	private class MailSendThread extends Thread
	{
		private final BlockingQueue<Message> queue;

		public MailSendThread(String server, BlockingQueue<Message> q)
		{
			this.setName("Mail Send - " + server);
			this.setDaemon(true); // allows the VM to exit, note that we are not persistent, so you can lose work
			queue = q;
		}

		public void send(Message msg)
		{
			queue.add(msg);
		}

		public void run()
		{
			while (true)
			{
				Message msg = null;
				try
				{
					msg = (Message) queue.take();
					Transport.send(msg);
				} catch (SMTPSendFailedException sfex)
				{
					if (sfex.getReturnCode() == 552) // message too big
					{
						// remove attachment, requeue with error message
						try
						{
							if (msg != null)
							{
								msg.setText("Attachment(s) too large for the email system");
								queue.add(msg);
							}
						}
						catch (Exception ex)
						{
							logger.error(ex.getMessage(), ex);
						}
					}
				} catch (Exception ex)
				{
					logger.error(ex.getMessage(), ex);
				}
				try
				{
					Thread.sleep(SLEEP_TIME);
				} catch (Exception ex)
				{
					logger.error(ex.getMessage(), ex);
				}
			}
		}
	}
}
