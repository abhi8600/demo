/**
 * $Id: ExportServlet.java,v 1.46 2012-07-18 00:01:54 ricks Exp $
 *
 * Copyright (C) 2009-2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.Principal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRPrintElement;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JRPrintText;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JExcelApiExporter;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRCsvExporterParameter;
import net.sf.jasperreports.engine.export.JRGraphics2DExporter;
import net.sf.jasperreports.engine.export.JRGraphics2DExporterParameter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.JRXlsAbstractExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRPptxExporter;
import net.sf.jasperreports.engine.util.JRClassLoader;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.apache.xerces.impl.dv.util.Base64;

import com.successmetricsinc.Session.Renderer;
import com.successmetricsinc.UI.SMIWebUserBean;
import com.successmetricsinc.WebServices.login.TrustedLoginWS;
import com.successmetricsinc.adhoc.AdhocReport;
import com.successmetricsinc.adhoc.AdhocReportHelper;
import com.successmetricsinc.catalog.CatalogManager;
import com.successmetricsinc.query.Filter;
import com.successmetricsinc.security.SecurityUtil;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.logging.AuditLog;

public class ExportServlet extends HttpServlet
{
	private static Logger logger = Logger.getLogger(ExportServlet.class);
	private static final long serialVersionUID = 1L;
	private String userName = null;
	private String spaceId = null;
	private static final String Creator = "Birst";

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		doGet(request, response);
	}

	/**
	 * export a report as an HTTP servlet response
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		HttpSession session = request.getSession(false);
		boolean passSession = false;
		if (session == null || session.isNew()) {
			String sessionId = request.getHeader("X-BirstSessionId");
			if (sessionId == null || sessionId.trim().length() == 0) {
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
				return;
			}
			ServletContext sc = this.getServletContext();
			session = (HttpSession)sc.getAttribute(sessionId);
			passSession = true;
		}
		if (session == null)
		{
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
			return;
		}
		if (!SecurityUtil.validateCSRF(request, passSession ? session : null))
		{
			response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "CSRF Failure");
			return;
		}
		
		SMIWebUserBean userBean = (SMIWebUserBean) session.getAttribute("userBean");
		if (userBean == null)
		{
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
			return;
		}
		Session.clear();
		SMIWebUserBean.addThreadRequest(Long.valueOf(Thread.currentThread().getId()), userBean);

		try
		{
			setMDC(getPrincipal(request));
			
			String sessionVars = request.getParameter(TrustedLoginWS.LOGIN_PARAM_SESSION_VARS);
			if (sessionVars != null && sessionVars.trim().length() > 0) {
				String[] loginCredentials = new String[1];
				loginCredentials[0] = TrustedLoginWS.LOGIN_PARAM_SESSION_VARS + "=" + sessionVars;
				TrustedLoginWS.populateSessionVariables(loginCredentials, request, userBean);
				Session.setSessionVars(sessionVars);
			}

			Session.setRenderer(Renderer.Dashboard);
			
			long start = System.currentTimeMillis();
			
			String encoding = "UTF-8";
			request.setCharacterEncoding(encoding);
			
			boolean decodeXML = Boolean.valueOf(request.getParameter("birst.xmlEncoded"));
			
			String exportType = request.getParameter("birst.exportType");
			String jprintId = request.getParameter("birst.jasperPrintId");
			JasperPrint jprint = null;
			if (jprintId != null) {
				jprint = userBean.getExportJasperPrintObject(jprintId);
			}
			
			String reportName = request.getParameter("birst.reportName");
			String name = reportName;
			
			if (reportName != null && reportName.length() > 0) {
				if (decodeXML) {
					reportName = decode(reportName);
				}
				else {
					reportName = new String(reportName.getBytes(encoding), encoding);
				}
				// if this request comes from javascript, XML needs to be quoted - remove first quote in that case
				if (reportName.startsWith("'"))
					reportName = reportName.substring(1, reportName.length() - 1);
			}
			String reportXML = request.getParameter("birst.reportXML");
			if (reportXML != null && reportXML.length() > 0) {
				if (decodeXML) {
					reportXML = decode(reportXML);
				}
				else {
					reportXML = new String(reportXML.getBytes(encoding), encoding);
				}
				// if this request comes from javascript, XML needs to be quoted - remove first quote in that case
				if (reportXML.startsWith("'"))
					reportXML = reportXML.substring(1, reportXML.length() - 1);
			}
			
			AdhocReport report = null;
			if (reportXML != null) {
				report = AdhocReport.convert(reportXML, null, null);
				name = report.getFileName();
			}
			else if (reportName != null){
				CatalogManager cm = userBean.getCatalogManager();
				String filename = cm.getNormalizedPath(reportName);
				File adhocReportFile = CatalogManager.getAdhocReportFile(filename, cm.getCatalogRootPath());
				if (adhocReportFile != null) {
					report = AdhocReport.read(new File(filename));
				}
			}
			
			if (jprint == null) {
				String dashboardParamsSeparator = request.getParameter("birst.dashParamsSeparator");
				if(dashboardParamsSeparator != null)
					Session.setDashboardParamsSeperator(dashboardParamsSeparator);	
				
				String dashboardParams = request.getParameter("birst.dashboardParams");
				if(dashboardParams != null)
					Session.setDashboardParams(dashboardParams);			
				
				String promptsXML = request.getParameter("birst.prompts");
				if (promptsXML != null && promptsXML.length() > 0) {
					if (decodeXML) {
						promptsXML = decode(promptsXML);
					}
					else {
						promptsXML = new String(promptsXML.getBytes(encoding), encoding);
					}
					// if this request comes from javascript, XML needs to be quoted - remove first quote in that case
					if (promptsXML.startsWith("'"))
						promptsXML = promptsXML.substring(1, promptsXML.length() - 1);
				}
				String dashletParams = request.getParameter("birst.dashletParams");
				if (dashletParams != null && dashletParams.length() > 0) {
					if (decodeXML) {
						dashletParams = decode(dashletParams);
					}
					else {
						dashletParams = new String(dashletParams.getBytes(encoding), encoding);
					}
					// if this request comes from javascript, XML needs to be quoted - remove first quote in that case
					if (dashletParams.startsWith("'"))
						dashletParams = dashletParams.substring(1, dashletParams.length() - 1);
				}
				boolean applyAllPrompts = Boolean.valueOf(request.getParameter("birst.applyAllPrompts"));
				
				SMIWebUserBean.setHttpServletRequest(request);
				SMIWebUserBean.addThreadRequest(Long.valueOf(Thread.currentThread().getId()), userBean);
				
				userBean.getRepSession().setCancelled(false);
				if (promptsXML != null && promptsXML.length() > 0) {
					List<Filter> filterParams = AdhocReportHelper.parsePromptData(promptsXML);
					Session.setParams(filterParams);
				}
				boolean ignorePagination = false;
				if (exportType != null && (exportType.equalsIgnoreCase("xls") || exportType.equalsIgnoreCase("csv")))
					ignorePagination = true;
				
				boolean isPdf = false;
				if (exportType != null && exportType.equalsIgnoreCase("pdf"))
					isPdf = true;
				
				
				// don't do async generation as it relies on the client making multiple calls
				if (report == null) {
					jprint = AdhocReportHelper.getDashboardJasperPrint(reportName, promptsXML, userBean.getRepository(), userBean, ignorePagination, applyAllPrompts, dashletParams);
				}
				else {
					jprint = AdhocReportHelper.getSynchDashboardJasperPrint(report, promptsXML, dashletParams, userBean.getRepository(), ignorePagination, userBean, applyAllPrompts, isPdf, true);
				}
			}

			//http://support.microsoft.com/kb/316431
			response.setHeader("Pragma", "");
			if (jprint == null) {
				ServerContext.errorMessage(null, "Invalid Export Request", response);
			}
			else if (exportType != null && exportType.equalsIgnoreCase("pdf"))
			{
				jprint.setName(name);
				exportToPDF(response, jprint);
			}
			else if (exportType != null && exportType.equalsIgnoreCase("ppt"))
				exportToPPT(response, jprint);
			else if (exportType != null && exportType.equalsIgnoreCase("xls"))
			{
				// clean up name
				// jprint.setName(excelCleanup(name));
				exportToExcel(response, jprint);
			}
			else if (exportType != null && exportType.equalsIgnoreCase("csv"))
				exportToCSV(response, jprint, report);
			else if (exportType != null && exportType.equalsIgnoreCase("rtf"))
				exportToRTF(response, jprint);
			else if (exportType != null && exportType.equalsIgnoreCase("png")) {
				String pageStr = request.getParameter("birst.exportPage");
				String zoomStr = request.getParameter("birst.exportZoom");
				exportToPNG(response, jprint, pageStr, zoomStr);
			}
			else
			{
				ServerContext.errorMessage(null, "Invalid Export Request: " + exportType, response);
				return;
			}
			MDC.put("sessionid", session.getId());
			AuditLog.log("export (" + exportType.toLowerCase() + ")", (jprint != null ? jprint.getName() : null), session.getId(), System.currentTimeMillis() - start);
		}
		catch(Exception e)
		{
			logger.info(e, e);
			if(e.getMessage() != null && e.getMessage().equals("Not enough space to render the crosstab.")){
				ServerContext.errorMessage(null, "The pivot table doesn't fit within the page size. Consider reducing margins or increasing page size.", response);
			}else{
				ServerContext.errorMessage(null, "An error occurred during processing.", response);
			}
		}
		finally {
			MDC.remove("username");
			MDC.remove("spaceID");
			MDC.remove("sessionid");
			com.successmetricsinc.UserBean.removeThreadRequest(Long.valueOf(Thread.currentThread().getId()));
			Session.setRenderer(Renderer.Adhoc);
		}
	}
	
	private String decode(String s) {
		//return URLDecoder.decode(s, "UTF-8");
		byte[] decodedContents = Base64.decode(s);
		try {
			return new String(decodedContents, "UTF-8");
		}
		catch (Exception e) { }
		return null;
	}
	
	private String excelCleanup(String name)
	{
		if (name == null)
			return null;
		// trim to last /
		int last = name.lastIndexOf('/');
		if (last >= 0)
			name = name.substring(last + 1);
		int len = name.length();
		// only supports lengths up to 31 characters for sheet names
		if (len > 31)
			name = name.substring(0, 31);
		// no '/', ':', '\\', '*', '[', ']', '?'
		return name.replaceAll("[/\\\\:\\*\\[\\]\\?]", "_");
	}
	
	private void exportToPNG(HttpServletResponse response, JasperPrint jprint, String pageStr, String zoomStr) throws JRException, IOException {
		Integer page = Integer.valueOf(0);
		if (pageStr != null) {
			try {
				page = Integer.valueOf(pageStr);
			}
			catch (Exception e) {}
		}
		Float zoom = Float.valueOf(2.0f);
		if (zoomStr != null) {
			try {
				zoom = Float.valueOf(zoomStr);
			}
			catch (Exception e) {}
		}
		
		BufferedImage image = new BufferedImage((int)(jprint.getPageWidth() * zoom.doubleValue()), 
				(int)(jprint.getPageHeight() * zoom.doubleValue()), 
				BufferedImage.TYPE_INT_ARGB);
		Graphics2D graphics = image.createGraphics();
		JRGraphics2DExporter ex = new JRGraphics2DExporter();
		ex.setParameter(JRExporterParameter.JASPER_PRINT, jprint);
		ex.setParameter(JRGraphics2DExporterParameter.GRAPHICS_2D, graphics);
		ex.setParameter(JRGraphics2DExporterParameter.ZOOM_RATIO, zoom);
		ex.setParameter(JRExporterParameter.PAGE_INDEX, page);
		ex.exportReport();
		response.setContentType("image/png");
		// String fileName = reportName + ".png";
		// response.setHeader("Content-Disposition", "attachment; filename=\"" + Util.cleanFilename(fileName) + "\"");
		OutputStream output = response.getOutputStream();
		ImageIO.write(image, "png", output);
		response.flushBuffer();
	}

	private void exportToPPT(HttpServletResponse response, JasperPrint jprint) throws IOException, JRException {
		JRPptxExporter ex = new JRPptxExporter();
		File tempFile = File.createTempFile("PPT_Export", "PPT");
		ex.setParameter(JRExporterParameter.JASPER_PRINT, jprint);
		ex.setParameter(JRExporterParameter.OUTPUT_FILE, tempFile);
		response.setContentType("application/ppt");
		//http://support.microsoft.com/kb/316431
//		response.addHeader("Cache-Control", "no-cache");
		String reportName = "file";
		String fileName = reportName + ".ppt";
		response.setHeader("Content-Disposition", "attachment; filename=\"" + Util.cleanFilename(fileName) + "\"");
		ex.exportReport();
		this.exportFileToStream(tempFile, response.getOutputStream());
		response.flushBuffer();
	}

	/**
	 * set up the MDC entries for usage tracking
	 * @param principal	String - username, spaceID
	 */
	private void setMDC(String principal)
	{
		if (principal != null)
		{
			String[] parts = principal.split(",");
			if (parts != null && parts.length == 3)
			{
				MDC.put("username", parts[0]);
				MDC.put("spaceID", parts[2]);
				userName = parts[0];
				spaceId = parts[2];
			}
		}
	}

	/**
	 * get the authentication principal from the HTTP servlet request
	 * @param request
	 * @return
	 */
	private String getPrincipal(HttpServletRequest request)
	{
		if (request == null)
		{
			return null;
		}
		Principal prin = request.getUserPrincipal();
		return prin == null ? null : prin.getName();
	}
	
	/**
	 * render PDF as an attachment to the HTTP Servlet response
	 * @param response
	 * @param jprint
	 * @throws IOException
	 * @throws JRException
	 */
	private void exportToPDF(HttpServletResponse response, JasperPrint jprint) throws IOException, JRException {
		JRPdfExporter ex = new JRPdfExporter();
		File tempFile = File.createTempFile("PDF_Export", "PDF");
		ex.setParameter(JRExporterParameter.JASPER_PRINT, jprint);
		ex.setParameter(JRExporterParameter.OUTPUT_FILE, tempFile);
		ex.setParameter(JRPdfExporterParameter.METADATA_CREATOR, Creator);
		ex.setParameter(JRPdfExporterParameter.METADATA_TITLE, jprint.getName());
		ex.setParameter(JRExporterParameter.CHARACTER_ENCODING, "UTF-8");
		if (userName != null)
			ex.setParameter(JRPdfExporterParameter.METADATA_AUTHOR, userName);
		if (spaceId != null)
			ex.setParameter(JRPdfExporterParameter.METADATA_SUBJECT, spaceId);
		//http://support.microsoft.com/kb/316431
	//	response.addHeader("Cache-Control", "no-cache");
		ex.exportReport();
		
		response.setContentType("application/pdf");
		String reportName = "file";
		String fileName = reportName + ".pdf";
		response.setHeader("Content-Disposition", "attachment; filename=\"" + Util.cleanFilename(fileName) + "\"");
		exportFileToStream(tempFile, response.getOutputStream());
		response.flushBuffer();
	}
	
	private void exportFileToStream(File file, ServletOutputStream sos) throws IOException {
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(file);
			byte[] buffer = new byte[1024];
			int bytesRead = fis.read(buffer);
			while (bytesRead >= 0) {
				sos.write(buffer, 0, bytesRead);
				bytesRead = fis.read(buffer);
			}
		}
		finally {
			if (fis != null)
				fis.close();
			
			file.delete();
		}
	}
	
	/**
	 * render Excel as an attachment to the HTTP Servlet response
	 * @param response
	 * @param jprint
	 * @throws IOException
	 * @throws JRException
	 */
	private void exportToExcel(HttpServletResponse response, JasperPrint jprint) throws IOException, JRException {
		JExcelApiExporter ex = new JExcelApiExporter();
		ex.setParameter(JRExporterParameter.JASPER_PRINT, jprint);
		File tempFile = File.createTempFile("XLS_Export", "XLS");
		ex.setParameter(JRExporterParameter.OUTPUT_FILE, tempFile);
		ex.setParameter(JRXlsAbstractExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
		ex.setParameter(JRExporterParameter.IGNORE_PAGE_MARGINS, Boolean.TRUE);
		ex.setParameter(JRExporterParameter.CHARACTER_ENCODING, "UTF-8");
		ex.setParameter(JRXlsAbstractExporterParameter.FORMAT_PATTERNS_MAP, generateFormatPatterns(jprint));
		
		response.setContentType("application/vnd.ms-excel");
		//http://support.microsoft.com/kb/316431
//		response.addHeader("Cache-Control", "no-cache");
		String reportName = "file";
		String fileName = reportName + ".xls";
		response.setHeader("Content-Disposition", "attachment; filename=\"" + Util.cleanFilename(fileName) + "\"");
		ex.exportReport();
		this.exportFileToStream(tempFile, response.getOutputStream());
		response.flushBuffer();
	}
	
	public static Map<String, String> generateFormatPatterns(JasperPrint jprint) {
		Map<String, String> ret = new HashMap<String, String>();
		for (JRPrintPage page : jprint.getPages()) {
			for (JRPrintElement el : page.getElements()) {
				if (el instanceof JRPrintText) {
					String pattern = ((JRPrintText)el).getPattern();
					if (pattern != null && ! ret.containsKey(pattern)) {
						// generate Excel pattern from this one
						try {
							Class<?> valueClass = JRClassLoader.loadClassForRealName(((JRPrintText)el).getValueClassName());
							if (java.lang.Number.class.isAssignableFrom(valueClass))
							{
								// Numbers
								// any # directly next to decimal separator becomes 0 - #,###.## become #,##0.## - otherwise 0 is blank
								int index = pattern.lastIndexOf('.');
								String newPattern = pattern.toString();
								if (index < 0) {
									char lastChar = pattern.charAt(pattern.length() - 1);
									if (lastChar == '#') {
										newPattern = pattern.substring(0, pattern.length() - 1).concat("0");
									}
								}
								else {
									if (index > 0) {
										char lastCharBeforeDecimal = pattern.charAt(index - 1);
										if (lastCharBeforeDecimal == '#') {
											newPattern = pattern.substring(0, index - 2).concat("0").concat(pattern.substring(index));
										}
									}
								}
								
								ret.put(pattern, newPattern);
							}
							else if (Date.class.isAssignableFrom(valueClass))
							{
								// Dates
								// M for month become m
								ret.put(pattern, pattern.replaceAll("M", "m"));
							}
						}
						catch (Exception e) {
						}
					}
				}
				
			}
		}
		return ret;
	}
	/**
	 * render CVS as an attachment to the HTTP Servlet response
	 * @param response
	 * @param jprint
	 * @throws IOException
	 * @throws JRException
	 */	
	private void exportToCSV(HttpServletResponse response, JasperPrint jprint, AdhocReport report) throws IOException, JRException {
		JRCsvExporter ex = new JRCsvExporter();
		ex.setParameter(JRExporterParameter.JASPER_PRINT, jprint);
		File tempFile = File.createTempFile("CSV_Export", "CSV");
		ex.setParameter(JRExporterParameter.OUTPUT_FILE, tempFile);
		ex.setParameter(JRExporterParameter.CHARACTER_ENCODING, "UTF-8");
		
		String separator = AdhocReport.CSV_SEPARATOR;
		if (report != null)
		{
			separator = report.getSeparatorCSV();
			if (separator != null && !separator.isEmpty())
			{
				if (separator.equals("\\t"))
					separator = "\t";
			}
		}
		ex.setParameter(JRCsvExporterParameter.FIELD_DELIMITER, separator);

		response.setContentType(report == null ? AdhocReport.CSV_MIME_TYPE : report.getMimeTypeCSV());
		response.setCharacterEncoding("UTF-8");
		//http://support.microsoft.com/kb/316431
//		response.addHeader("Cache-Control", "no-cache");
		String fileName = report == null ? AdhocReport.CSV_FILE_NAME : report.getFilenameCSV();
		response.setHeader("Content-Disposition", "attachment; filename=\"" + Util.cleanFilename(fileName) + "\"");
		ex.exportReport();
		ServletOutputStream sos = response.getOutputStream();
		byte b[] = {(byte)0xEF, (byte)0xBB, (byte)0xBF};
		sos.write(b);
		exportFileToStream(tempFile, sos);
		response.flushBuffer();
	}
	
	/**
	 * render RTF (Rich Text Format) as an attachment to the HTTP Servlet response
	 * @param response
	 * @param jprint
	 * @throws IOException
	 * @throws JRException
	 */	
	private void exportToRTF(HttpServletResponse response, JasperPrint jprint) throws IOException, JRException {
		JRRtfExporter ex = new JRRtfExporter();
		ex.setParameter(JRExporterParameter.JASPER_PRINT, jprint);
		File tempFile = File.createTempFile("RTF_Export", "RTF");
		ex.setParameter(JRExporterParameter.OUTPUT_FILE, tempFile);
		ex.setParameter(JRExporterParameter.CHARACTER_ENCODING, "UTF-8");
		response.setContentType("application/rtf");
		//http://support.microsoft.com/kb/316431
//		response.addHeader("Cache-Control", "no-cache");
		String reportName = "file";
		String fileName = reportName + ".rtf";
		response.setHeader("Content-Disposition", "attachment; filename=\"" + Util.cleanFilename(fileName) + "\"");
		ex.exportReport();
		exportFileToStream(tempFile, response.getOutputStream());
		response.flushBuffer();
	}
}

