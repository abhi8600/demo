package com.successmetricsinc;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.rosuda.REngine.Rserve.RFileInputStream;

import com.successmetricsinc.R.RServer;

public class RServerUtil
{
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(RServerUtil.class);
	private Repository r;
	private String spaceDirectory;

	public RServerUtil(Repository r, String spaceDirectory)
	{
		this.r = r;
		this.spaceDirectory = spaceDirectory;
	}

	public String getRServerFile(String path, String rexpression)
	{
		RServer rs = r.getRServer();
		if (rs == null)
			return "No valid connection to R Server";
		if (rexpression != null && rexpression.length() > 0)
		{
			try
			{
				rs.eval(-1, rexpression);
			} catch (Exception e)
			{
				return e.getMessage();
			}
		}
		RFileInputStream rfis;
		try
		{
			rfis = rs.getInputStream(path);
		} catch (Exception e)
		{
			return e.getMessage();
		}
		if (rfis == null)
			return "Unable to connect to R Server";
		int index1 = path.lastIndexOf('/');
		int index2 = path.lastIndexOf('\\');
		String fname = path;
		if (index1 >= 0 && index1 > index2)
			fname = path.substring(index1 + 1);
		else if (index2 >= 0 && index2 > index1)
			fname = path.substring(index2 + 1);
		// Market the file with additional suffix type of rserver so that we know that's how it originated
		File f = new File(spaceDirectory + File.separator + "data" + File.separator + fname + "rserver");
		FileOutputStream fos = null;
		try
		{
			fos = new FileOutputStream(f);
			byte[] arr = new byte[10000];
			int len = 0;
			while ((len = rfis.read(arr)) > 0)
			{
				fos.write(arr, 0, len);
			}
		} catch (FileNotFoundException e1)
		{
			return e1.getMessage();
		} catch (IOException e)
		{
			return e.getMessage();
		} finally
		{
			try
			{
				rfis.close();
			} catch (IOException e)
			{
			}
			try
			{
				fos.close();
			} catch (IOException e)
			{
			}
		}
		return null;
	}
}
