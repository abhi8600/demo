/**
 * $Id: EmailDelivery.java,v 1.16 2012-08-09 21:04:27 ricks Exp $
 *
 * Copyright (C) 2007-2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc;

import java.io.File;
import java.io.StringReader;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;

import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.apache.log4j.Logger;

import com.successmetricsinc.Session.Renderer;
import com.successmetricsinc.WebServices.ClientInitData;
import com.successmetricsinc.adhoc.AdhocReport;
import com.successmetricsinc.catalog.CatalogManager;
import com.successmetricsinc.query.AbstractQueryString;
import com.successmetricsinc.query.JasperDataSourceProvider;
import com.successmetricsinc.query.Query;
import com.successmetricsinc.query.QueryResultSet;
import com.successmetricsinc.query.QueryString;
import com.successmetricsinc.query.ResultSetCache;
import com.successmetricsinc.util.JasperUtil;
import com.successmetricsinc.util.Util;
import com.successmetricsinc.util.XmlUtils;


public class EmailDelivery
{
	private static Logger logger = Logger.getLogger(EmailDelivery.class);
	private int curIndex;
	private QueryResultSet qrs;
	private String[] toList;
	private Repository r;
	private Notification broadcastNotification;
	private String broadcastName;
	private Broadcast broadcast;
	private int numThreads;
	private JasperDataSourceProvider jdsp = null;
	private User u = null;
	private ResultSetCache rsc = null;
	private UserBean userBean = null;
	
	public EmailDelivery(Repository r, Broadcast b, UserBean ub, String mailHost)
	{
		broadcastNotification = Notification.getInstance(mailHost);
		this.r = r;
		this.broadcastName = b.getName();
		this.broadcast = b;
		numThreads = r.getDefaultConnection().MaxConnectionThreads;
		this.userBean = ub;
	}

	public int deliverReports(int maxReports)
	{
		// Spin threads to get the rest of the result sets
		Thread[] tlist = new Thread[numThreads];
		curIndex = 0;
		int numRows = 0;
		if (jdsp == null)
			jdsp = new JasperDataSourceProvider(r, rsc);
		// set up the session for the user to get the various DEFAULT session variables
		if (u == null)
			u = r.findUser(broadcast.getUsername());

		if (u == null) {
			logger.error("Could not find user " + broadcast.getUsername());
			return 0;
		}
		Session session = null;
		try
		{
			session = new Session(broadcast.getUsername());
			session.setRepository(r);
			if (rsc == null)
				rsc = ResultSetCache.getInstance(r.getApplicationIdentifier(), false);
			session.fillSessionVariables(r, rsc);
			fillOverrideSessionVariables(session);
			logger.info("Session variables (EmailDelivery:getQueryResultSetFromReport): " + session.toString());
			if (broadcast.getTriggerReport() != null)
			{
				QueryResultSet triggerResults = getQueryResultSetFromReport(broadcast.getTriggerReport(), session);
				if (triggerResults == null)
				{
					logger.error("Trigger report failed to run, bypassing report");
					return 0;
				}
				if (triggerResults.numRows() == 0)
					return 0;
			}
			toList = broadcast.getToList();
			if (toList != null)
			{
				// Filter out any empty strings
				List<String> toListFilter = new ArrayList<String>();
				for (String s : toList)
				{
					if (s.trim().length() > 0)
						toListFilter.add(s.trim());
				}
				if (maxReports >= 0 && toListFilter.size() > maxReports)
					return 0;
				if (!toListFilter.isEmpty())
				{
					toList = new String[toListFilter.size()];
					for (int i = 0; i < toList.length; i++)
						toList[i] = toListFilter.get(i);
					numRows = toList.length;
				} else
					toList = null;
			}
			if (toList == null)
			{
				String query = broadcast.getQuery();
				if (broadcast.getToReport() != null)
				{
					qrs = getQueryResultSetFromReport(broadcast.getToReport(), session);
				} else if (query != null)
				{
					query = QueryString.preProcessQueryString(query, r, null);
					AbstractQueryString aqs = AbstractQueryString.getInstance(r, query);
					Query q = aqs.getQuery();
					qrs = rsc.getQueryResultSet(q, r.getServerParameters().getMaxRecords(), r.getServerParameters().getMaxQueryTime(), aqs.getDisplayFilters(), aqs.getExpressionList(), aqs.getDisplayOrderList(),
							null, false, false, r.getServerParameters().getDisplayTimeZone(), false);
				}
				if (qrs != null)
					numRows = qrs.numRows();
				if (maxReports >= 0 && numRows > maxReports)
					return 0;

			}
			for (int i = 0; i < numThreads; i++)
			{
				tlist[i] = new ThreadExport(toList, rsc, u, session);
				tlist[i].start();
			}
			// wait for email generation threads to finish
			for (int i = 0; i < numThreads; i++)
			{
				tlist[i].join();
			}
			// still need to wait for mail sender thread to finish
			if(session != null)
			{
				session.setCancelled(true); // clear as soon as possible, if possible
			}
			do
			{
				logger.debug("Mail Send Thread sleeping 10 seconds, " + broadcastNotification.remainingMessages() + " messages remaining to be sent.");
				broadcastNotification.logMessageList();
				Thread.sleep(10 * 1000);
			} while (broadcastNotification.remainingMessages() > 0);

		} catch (Exception ex)
		{
			logger.error(ex.getMessage(), ex);
			return 0;
		}
		finally
		{
			if(session != null)
			{
				session.setCancelled(true);
			}
		}
		return numRows;
	}

		
		@SuppressWarnings("unchecked")
		private void fillOverrideSessionVariables(Session session) throws Exception 
		{
			String overrideVariablesXmlString = broadcast.getOverrideVariablesXmlString();
			XMLStreamReader reader = null;			
			try
			{				
				if(overrideVariablesXmlString != null && overrideVariablesXmlString.length() > 0 )
				{
					String unescapeOverrideXmlString = XmlUtils.decode(overrideVariablesXmlString);
					XMLInputFactory inputFactory = XmlUtils.getSecureXMLInputFactory();
					reader = inputFactory.createXMLStreamReader(new StringReader(unescapeOverrideXmlString));

					StAXOMBuilder builder = new StAXOMBuilder(reader);
					OMElement docElement = builder.getDocumentElement();
					docElement.build();
					docElement.detach();

					/*<Variables>
				    <Variable>
				      <Name>Products.CategoryName</ParameterName>
				      <Values>
				        <Value>Beverages</selectedValue>
				        <Value>Condiments</selectedValue>
				        <Value>Confections</selectedValue>
				        <Value>Dairy Products</selectedValue>
				      </Values>
				    </Variable>
				  </Variables>	*/


					for ( Iterator overrideVariablesIterator = docElement.getChildElements(); overrideVariablesIterator.hasNext(); ) 
					{
						OMElement variableRoot = (OMElement)overrideVariablesIterator.next();
						if (! ClientInitData.OVERRIDE_VARIABLE.equals(variableRoot.getLocalName()))
							continue;

						variableRoot.detach();

						String variableName = null;
						String variableValueType = null;
						// selectedValues needs to be string with multiple values displayed by pipe delimited
						// this is done to make sure that prompted filter directly set on the report works.
						List<String> variableValues = new ArrayList<String>();
						for (Iterator<OMElement> variableDetailsIterator = variableRoot.getChildElements(); variableDetailsIterator.hasNext(); ) 
						{
							OMElement el = variableDetailsIterator.next();
							String elName = el.getLocalName();

							if (ClientInitData.OVERRIDE_VARIABLE_NAME.equals(elName)) 
							{
								variableName = XmlUtils.getStringContent(el);
							}
							else if (ClientInitData.OVERRIDE_VARIABLE_VALUE_TYPE.equals(elName))
							{
								variableValueType = XmlUtils.getStringContent(el);
							}
							else if (ClientInitData.OVERRIDE_VARIABLE_VALUES_ROOT.equals(elName)) 
							{
								for (Iterator<OMElement> children = el.getChildElements(); children.hasNext(); )
								{
									OMElement child = children.next();
									String childName = child.getLocalName();
									if (ClientInitData.OVERRIDE_VARIABLE_VALUE.equals(childName)) 
									{
										String value = XmlUtils.getStringContent(child);
										if(value != null && value.length() > 0)
										{
											variableValues.add(value);
										}
									}
								}
							}							
						}

						if(variableName == null || variableName.length() == 0)
						{
							logger.warn("Variable Name is null");
							continue;
						}
						
						if(variableValueType == null)
						{
							logger.warn("Variable Value Type is null for variable " + variableName);
							continue;
						}
						
						if(variableValues == null || variableValues.size() == 0)
						{
							logger.warn("No Override values given for variable " + variableValues);
							continue;
						}
						
						Object obj = getFormattedVariableValue(variableValueType, variableValues);
						if(obj != null)
						{
							session.getSessionVariables().put(variableName, obj);
						}						
					}
				}
			}
			finally
			{
				if(reader != null)
				{
					try
					{
						reader.close();
					}catch(Exception ex2)
					{
						logger.warn("Error in closing reader ", ex2);
					}
				}
			}
	}
	
	private Object getFormattedVariableValue(String variableValueType, List<String> variableValues) 
	{
		Object obj = null;	
		if(variableValues != null && variableValues.size() > 0)
		{
			List<Object> list = new ArrayList<Object>();
			
			for(String variableVaue : variableValues)
			{
				if("String".equalsIgnoreCase(variableValueType))
				{
					list.add(variableVaue);
				}
				else if("Date".equalsIgnoreCase(variableValueType))
				{
					list.add(Date.valueOf(variableVaue));
				}
				else if("Integer".equalsIgnoreCase(variableValueType))
				{
					list.add(Integer.valueOf(variableVaue));
				}
				else if("Double".equalsIgnoreCase(variableValueType))
				{
					list.add(Double.valueOf(variableVaue));
				} 	
			}
			
			if(list.size() > 0)
			{
				obj = list.size() == 1 ? list.get(0) : list;
			}			
		}
		return obj;
	}
	
	private QueryResultSet getQueryResultSetFromReport(String report, Session session) throws Exception
	{
		if (report == null)
			return null;
		
		String catalogPath = r.getServerParameters().getCatalogPath();
		if (catalogPath == null)
			catalogPath = r.getServerParameters().getApplicationPath() + "\\catalog";
		report = CatalogManager.getNormalizedPath(report, catalogPath);
		File adhocReportFile = CatalogManager.getAdhocReportFile(report, catalogPath);
		String query = null;
		if (adhocReportFile != null) {
			AdhocReport ar = AdhocReport.read(adhocReportFile);
			query = ar.getQueryString(r, true);
		}
		else {
			JasperDesign jrxmlFile = JRXmlLoader.load(new File(report));
			if (jrxmlFile != null) {
				query = jrxmlFile.getQuery().getText();
			}
		}
		
		JasperReport jreport = ServerContext.getReport(report, r, null, false, false, null, session, false);
		if(jreport != null)
		{
			jdsp.setSession(session);
			return (QueryResultSet)jdsp.create(jreport, null);
		}		
		
		return null;
	}

	private synchronized int getNextIndex()
	{
		if (toList != null)
		{
			if (curIndex >= toList.length)
				return (-1);
		} else
		{
			if ((qrs == null && curIndex > 0) || (qrs != null && curIndex >= qrs.getRows().length))
				return (-1);
		}
		return (curIndex++);
	}
	
	public class ThreadExport extends Thread
	{
		private ResultSetCache rsc;
		private String[] toList;
		private User u;
		private Session session;

		public ThreadExport(String[] toList, ResultSetCache rsc, User u, Session session)
		{
			this.setName("Email Delivery - " + this.getId());
			setDaemon(true); // if the JVM wants to exit due to errors, let it
			this.rsc = rsc;
			this.toList = toList;
			this.u = u;
			this.session = session;
		}

		public void run()
		{
			while (true)
			{
				// get the next user to email to
				int index = getNextIndex();
				if (index < 0) {
					Session.setRenderer(Renderer.Adhoc);
					return;
				}
				// Will need to club all maps into one map
				Map<String, Object> parameters = JasperUtil.createParameterMap();
				Map<String, String> operators = new HashMap<String,String>();
				Map<String, Boolean> isExpressionMap = new HashMap<String, Boolean>();
				Object userObj = null;
				Object emailObj = null;
				Boolean applyAllPrompts = false;
				if (qrs != null)
				{
					// set up the parameter map
					for (int i = 0; i < qrs.getColumnNames().length; i++)
					{
						if (qrs.getColumnNames()[i] != null)
							parameters.put(qrs.getColumnNames()[i], qrs.getRows()[index][i] == null ? "null" : qrs.getRows()[index][i].toString());
					}					
					/*
					 * Match for variants of Username and Email. Could also be report name versions (i.e. begin with
					 * F1_)
					 */
					for (String s : parameters.keySet())
					{
						if ("username".equals(s.toLowerCase()))
						{
							userObj = parameters.get(s);
						} else if ("email".equals(s.toLowerCase()))
						{
							emailObj = parameters.get(s);
						}
					}
					if (userObj == null)
						userObj = broadcast.getUsername();
					if (userObj == null || emailObj == null)
						continue;
				} else
				{
					userObj = broadcast.getUsername();
					if (userObj == null)
						continue;
					emailObj = toList[index];
					try
					{
						// create prompts param map for self schedulig.
						applyAllPrompts = parsePromptsAndFillPromptsMap(parameters, operators, isExpressionMap, broadcast.getPromptXmlString());
					}
					catch(Exception ex)
					{
						logger.warn("Exception in parsing prompts for user : " + userObj  + " email : " + emailObj, ex);
						continue;
					}
				}
				
				String user = userObj.toString();
				// set up the session for the user to get the various DEFAULT session variables
				User u = r.findUser(user);
				if (u == null)
				{
					if (this.u != null)
						u = this.u;
					else
						continue;
				}
				logger.info("Session variables (ThreadExport:run): " + session.toString());
				logger.info("TimeZone is " + userBean == null ? "DEFAULT" : userBean.timeZone.getDisplayName());
				String to = null;
				if (emailObj == null)
				{
					to = u.getEmailAddress();
				} else
				{
					to = emailObj.toString();
				}
				if (to == null)
				{
					continue;
				}
				// set up the from, subject, and body
				// - replace variables (session, repository, property) and parameters (from the query)
				String from = Util.replaceParameters(broadcast.getFrom(), parameters);
				from = r.replaceVariables(null, from);
				from = r.replaceVariables(session, from);
				String subject = Util.replaceParameters(broadcast.getSubject(), parameters);
				subject = r.replaceVariables(null, subject);
				subject = r.replaceVariables(session, subject);
				String body = Util.replaceParameters(broadcast.getBody(), parameters);
				body = r.replaceVariables(null, body);
				body = r.replaceVariables(session, body);
				String report = Util.replaceParameters(broadcast.getReport(), parameters);
				report = r.replaceVariables(null, report);
				report = r.replaceVariables(session, report);
				String catalogPath = r.getServerParameters().getCatalogPath();
				if (catalogPath == null)
					catalogPath = r.getServerParameters().getApplicationPath() + "\\catalog";
				if (catalogPath != null && report != null)
					report = report.replace("V{CatalogDir}", catalogPath);
				session.setCatalogDir(catalogPath);
				String outputFileName = r.replaceVariables(session, broadcast.getName()); 
				logger.debug("Sending report " + report + " to email with repository " + r.getApplicationName());
				session.setCancelled(false);
				Session.clear();
				Session.setRenderer(Renderer.Dashboard);

				if (!broadcastNotification.sendReport(r, outputFileName, from, to, subject, body, report, session,
						userBean, rsc, parameters,operators, applyAllPrompts, isExpressionMap, broadcast.getType(), 
						broadcast.getCsvSeparator(), broadcast.getCsvExtension(), broadcast.getCompressionFormat() ))
				{
					logger.error("Email failed for " + user + " to " + to);
				}
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("Email", to);
				params.put("name", broadcastName);
			}
		}
		
		// Need to club all maps into one
		@SuppressWarnings("unchecked")
		private boolean parsePromptsAndFillPromptsMap(Map<String, Object> parametersMap, Map<String,String> operatorMap, 
				Map<String, Boolean> isExpressionMap, String promptXmlString) throws Exception 
		{
			XMLStreamReader reader = null;	
			boolean applyAllPrompts = false;
			try
			{				
				if(promptXmlString != null && promptXmlString.length() > 0 )
				{
					String unescapePromptXmlString = XmlUtils.decode(promptXmlString);
					XMLInputFactory inputFactory = XmlUtils.getSecureXMLInputFactory();
					reader = inputFactory.createXMLStreamReader(new StringReader(unescapePromptXmlString));

					StAXOMBuilder builder = new StAXOMBuilder(reader);
					OMElement docElement = builder.getDocumentElement();
					docElement.build();
					docElement.detach();

					/*<Prompts>
					 <applyAllPrompts></applyAllPrompts>
				    <Prompt>
				      	<ParameterName>Products.CategoryName</ParameterName>
				      	<Operaor></Operator>
				      	<IsExpression><IsExpression>
				        <selectedValues>
				        <selectedValue>Beverages</selectedValue>
				        <selectedValue>Condiments</selectedValue>
				        <selectedValue>Confections</selectedValue>
				        <selectedValue>Dairy Products</selectedValue>
				      </selectedValues>
				    </Prompt>
				  </Prompts>	*/


					for (@SuppressWarnings("rawtypes")
					Iterator promptIterator = docElement.getChildElements(); promptIterator.hasNext(); ) 
					{	
						OMElement prompt = (OMElement)promptIterator.next();
						if("applyAllPrompts".equals(prompt.getLocalName()))
						{						
							applyAllPrompts = XmlUtils.getBooleanContent(prompt);
						}
						
						if (! "Prompt".equals(prompt.getLocalName()))
							continue;

						prompt.detach();

						String paramName = null;
						String operatorName=null;
						boolean isExpression = false;
						// selectedValues needs to be string with multiple values displayed by pipe delimited
						// this is done to make sure that prompted filter directly set on the report works.
						StringBuilder selectedValues = new StringBuilder();
						for (Iterator<OMElement> promptDetailsIterator = prompt.getChildElements(); promptDetailsIterator.hasNext(); ) 
						{
							OMElement el = promptDetailsIterator.next();
							String elName = el.getLocalName();

							if ("ParameterName".equals(elName)) 
							{
								paramName = XmlUtils.getStringContent(el);
							}
							else if("Operator".equals(elName))
							{								
								operatorName = XmlUtils.getStringContent(el);	
							}
							else if("IsExpression".equals(elName))
							{								
								isExpression = XmlUtils.getBooleanContent(el);	
							}
							else if ("selectedValues".equals(elName)) 
							{
								for (Iterator<OMElement> children = el.getChildElements(); children.hasNext(); )
								{
									OMElement child = children.next();
									String childName = child.getLocalName();
									if ("selectedValue".equals(childName)) 
									{
										String value = XmlUtils.getStringContent(child);
										if(value != null && value.length() > 0)
										{
											if(selectedValues.length() > 0)
											{
												selectedValues.append("||");
											}
											selectedValues.append(value);
										}
									}
								}
							}							
						}

						if(paramName == null || paramName.length() == 0 || selectedValues == null || selectedValues.length() == 0)
						{
							continue;
						}
						parametersMap.put(paramName, selectedValues.toString());
						operatorMap.put(paramName,operatorName);
						isExpressionMap.put(paramName, isExpression);
					}
				}
			}
			finally
			{
				if(reader != null)
				{
					try
					{
						reader.close();
					}catch(Exception ex2)
					{
						logger.warn("Error in closing reader ", ex2);
					}
				}
			}
			return applyAllPrompts;
		}
	}
}
