/**
 * $Id: InitializationServlet.java,v 1.31 2012-08-17 05:40:06 gsingh Exp $
 *
 * Copyright (C) 2007-2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.successmetricsinc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServlet;

import org.apache.log4j.Logger;

import com.successmetricsinc.UI.ApplicationContext;
import com.successmetricsinc.query.JasperDataSourceProvider;
import com.successmetricsinc.query.QueryResultSet;
import com.successmetricsinc.query.ResultSetCache;
import com.successmetricsinc.util.Util;

public class InitializationServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static Logger logger = Logger.getLogger(InitializationServlet.class);
    
    public void init(ServletConfig config) 
    {
    	Util.logRuntimeInfo();
    	String home = System.getProperty("smi.home");
    	if (home == null || home.isEmpty())
    	{
    		logger.fatal("smi.home is not defined (java -Dsmi.home=...), exiting");
    		System.exit(-1);
    	}
    	try
    	{
    		ServerContext.initContext(config.getServletContext());
    	}
    	catch (Exception ex)
    	{
    		logger.fatal("Exception caught during web application initialization, exiting");
    		logger.fatal(ex.getMessage(), ex);
    		System.exit(-1);
    	}
    	if(ServerContext.isSeedQueryInMultitenant())
    	{
    		File appRoot = new File(ServerContext.getApplicationRoot());
    		String seedQuerySpaceIDFilePath = ServerContext.getSeedQuerySpaceIDFilePath();
    		logger.info("Seed-Query SpaceID FilePath " + seedQuerySpaceIDFilePath);
    		if (seedQuerySpaceIDFilePath == null)
    		{
    			return;
    		}
    		List<String> spaceList = getSpaceList(seedQuerySpaceIDFilePath); 
    		if (appRoot.isDirectory())
    		{
    			for (String spDirName : spaceList)
    			{
    				File spDir = new File(appRoot + File.separator + spDirName);
    				if(spDir.isDirectory())
    				{
    					seedQueryForSpace(spDir);
    				}
    			}
    		}
    	}
    	return;
    }
    
    private List<String> getSpaceList(String seedQuerySpaceIDFilePath)
    {
    	List<String> spaceList = new ArrayList<String>();
    	BufferedReader input = null;
		try
		{
				input = new BufferedReader(new FileReader(seedQuerySpaceIDFilePath));
				String line = null;
				while ((line = input.readLine()) != null)
				{
					if (line.startsWith("#") || line.startsWith("//")) continue;
					spaceList.add(line);
				}
		}
		catch (FileNotFoundException ex) 
		{
			logger.error("Seed-Query SpaceID FilePath " + seedQuerySpaceIDFilePath + " not found");
		}
		catch (IOException ex)
		{
			logger.error(ex.getMessage(), ex);
		} finally
		{
			try
			{
				if (input != null)
				{
					//flush and close both "input" and its underlying FileReader
					input.close();
				}
			}
			catch (IOException ex)
			{
				logger.error(ex.getMessage(), ex);
			}
		}
		return spaceList;
    }
    
    private List<String> getSeedFileNameList(String spDirPath)
    {
    		File seedDir = new File(spDirPath + File.separator + ServerContext.getSeedDirName());
    		List<String> seedQueryFilenameList = new ArrayList<String>();
			if(seedDir.isDirectory())
			{
					for (File seedFile : seedDir.listFiles()) {
						seedQueryFilenameList.add(seedFile.getAbsolutePath());
					}
			} 
			return seedQueryFilenameList;
    }
    
    private void seedQueryForSpace(File spDir)
    {
    	List<String> seedQueryFileNameList = getSeedFileNameList(spDir.getAbsolutePath());
		if (!seedQueryFileNameList.isEmpty())
		{
			ApplicationContext ac= ApplicationContext.getInstance(spDir.getPath(), null);
			SeedQueryThread sqt = new SeedQueryThread(seedQueryFileNameList, ac);
			sqt.start();
		}
    }
    
	private static class SeedQueryThread extends Thread
	{
		private List<String> seedQueryFilenameList;
		private ResultSetCache rsc;
		private Repository r;
		private ApplicationContext ac;

		public SeedQueryThread(List<String> seedQueryFilenameList, Repository r, ResultSetCache rsc)
		{
			this.setName("Seed Query - " + this.getId());
			this.setDaemon(true);
			this.seedQueryFilenameList = seedQueryFilenameList;
			this.r = r;
			this.rsc = rsc;
		}
		
		public SeedQueryThread(List<String> seedQueryFilenameList, ApplicationContext ac)
		{
			this.setName("Seed Query - " + this.getId());
			this.seedQueryFilenameList = seedQueryFilenameList;
			this.r = ac.getRepository();
			this.rsc = ac.getCache();
			this.ac = ac;
		}
		
		public void run()
		{
			if (seedQueryFilenameList == null)
				return;
			for (String file : seedQueryFilenameList)
			{
				processFile(file);
			}
			if (this.ac != null)
			{
				ac.dispose();
			}
			logger.info("Seed query processing completed.");
		}

		public void processFile(String file)
		{			
			if (file == null)
			{
				return;
			}
	    	// read in the queries and run them to 'seed' the cache
			JasperDataSourceProvider jdsp = new JasperDataSourceProvider(r, rsc);
			logger.info("Starting seed queries in file " + file);

			// open the file(s) and iterate
			BufferedReader input = null;
			try
			{
				input = new BufferedReader(new FileReader(file));
				String line = null;
				while ((line = input.readLine()) != null)
				{
					// skip comment lines
					if (line.startsWith("#") || line.startsWith("//")) continue;
					// skip empty or close to it lines
					if (line.length() < 5) continue;
					
					// process query lines
					try
					{
						QueryResultSet qrs = (QueryResultSet) jdsp.create(line);
						logger.info("Query (" + line + ") returned " + qrs.numRows() + " rows");
					}
					catch (Exception ex2)
					{
						logger.error("Failed query: " + line);
						logger.error(ex2.getMessage(), ex2);
					}
				}
			}
			catch (FileNotFoundException ex)
			{
				logger.warn("Seed file " + file + " not found, skipping");
				return;
			}
			catch (IOException ex)
			{
				logger.error(ex.getMessage(), ex);
			}
			finally
			{
				try
				{
					if (input != null)
					{
						//flush and close both "input" and its underlying FileReader
						input.close();
					}
				}
				catch (IOException ex)
				{
					logger.error(ex.getMessage(), ex);
				}
			}
			logger.info("Seed queries in " + file + " completed.");
		}
	}
}
