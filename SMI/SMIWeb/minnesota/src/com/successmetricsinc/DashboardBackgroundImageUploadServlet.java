/**
 * $Id: DashboardBackgroundImageUploadServlet.java,v 1.8 2012-07-20 01:11:57 ricks Exp $
 *
 * Copyright (C) 2009-2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

import com.successmetricsinc.UI.SMIWebUserBean;

public class DashboardBackgroundImageUploadServlet extends HttpServlet
{
	private static Logger logger = Logger.getLogger(DashboardBackgroundImageUploadServlet.class);
	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		doGet(request, response);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		HttpSession session = request.getSession(false);
		if (session == null)
		{
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
			return;
		}
		
		SMIWebUserBean userBean = (SMIWebUserBean) session.getAttribute("userBean");
		if (userBean == null)
		{
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
			return;
		}

		try
		{
			// Create a factory for disk-based file items
			FileItemFactory factory = new DiskFileItemFactory();

			// Create a new file upload handler
			ServletFileUpload upload = new ServletFileUpload(factory);

			// Parse the request
			@SuppressWarnings("rawtypes")
			List items = upload.parseRequest(request);
			
			// Process the uploaded items
			@SuppressWarnings("rawtypes")
			Iterator iter = items.iterator();
			while (iter.hasNext()) {
			    FileItem item = (FileItem) iter.next();

			    // Process a file upload
			    String fileName = item.getName();
			    if (!item.isFormField() && fileName != null && "application/octet-stream".equalsIgnoreCase(item.getContentType())) {
			        // make sure the string does not contain a null (security issue, endsWith will not detect, but the filename created will end on the null)			    	
			        fileName = (new File(fileName)).getCanonicalFile().getName();
			        String lowerCaseFilename = fileName.toLowerCase();
			        if( !(lowerCaseFilename.endsWith(".png") || 
			        	  lowerCaseFilename.endsWith(".gif") || 
			        	  lowerCaseFilename.endsWith(".jpg")) ){
			        	
			        	response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			        	logger.error("", new RuntimeException("file is not image file (png,gif,jpg): " + fileName));
			        	
			        	return;
			        }
			        
			        String imageDirPath = userBean.getCatalogManager().getCatalogRootPath() + File.separator + ".." + File.separator + "DashboardBackgroundImage";
			        File imageDir = new File(imageDirPath);
			        if(!imageDir.exists()){
			        	imageDir.mkdir();
			        }
			        File uploadedFile = new File(imageDir.getAbsoluteFile() + File.separator + fileName);
			        item.write(uploadedFile);
			        return;
			    }
			}
			
		}
		catch(Exception e)
		{
			logger.error(e, e);
		}
		
	}
}

