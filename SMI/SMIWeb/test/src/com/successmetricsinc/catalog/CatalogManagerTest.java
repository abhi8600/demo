package com.successmetricsinc.catalog;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.successmetricsinc.UI.UserBean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CatalogManagerTest {
	
	private static Logger logger = Logger.getLogger(CatalogManagerTest.class);
	
	private static final String[] TEST_FOLDERS = {
		"C:/SMI/SMIWeb/tomcat/../../Data/1b2cf631-b8ce-4d76-b0e1-b9ade2753597/catalog/private/pconnolly@birst.com/2009-08-09",
		"C:\\SMI\\SMIWeb\\tomcat\\..\\..\\Data\\1b2cf631-b8ce-4d76-b0e1-b9ade2753597\\catalog\\private\\pconnolly@birst.com\\2009-08-09",
	};
	
	private static final String[] EXPECTED_FOLDERS = { 
		"C:\\SMI\\Data\\1b2cf631-b8ce-4d76-b0e1-b9ade2753597\\catalog\\private\\pconnolly@birst.com\\2009-08-09",
		"C:\\SMI\\Data\\1b2cf631-b8ce-4d76-b0e1-b9ade2753597\\catalog\\private\\pconnolly@birst.com\\2009-08-09",
	};
	
	private static final String[] TEST_ROOT_DIRS = { "private/testuser@birst.com", "Shared", };

	private static final String TEST_CATALOG_TEMPLATE = "../test/data/catalog";
	private static final String TEST_CATALOG_PATH = "catalog/Data/12345678-1234-1234-1234-123456789012/catalog";
	private static final String TEST_DASHBOARD_PATH = "private/testuser@birst.com/latest/2009-07-10";
	
	private File testCatalog;
	private String[] rootDirs = new String[TEST_ROOT_DIRS.length];
	
	@Before
	public void setUp() throws Exception {
		logger.info("--- --- --- setUp() --- --- ---");
		logger.info("PWD: " + System.getenv("PWD"));
		// Get 'tmp' directory
		final File tmpDir = new File(System.getenv("TEMP"));
		logger.info("tmpDir: " + tmpDir.getCanonicalPath());
		// Copy template test file structure to 'tmp' directory
		final File templateCatalog = new File(TEST_CATALOG_TEMPLATE);
		FileUtils.copyDirectoryToDirectory(templateCatalog, tmpDir);
		// Set up the test catalog directory
		testCatalog = new File(tmpDir, TEST_CATALOG_PATH);
		logger.info("'testCatalog': " + testCatalog.getCanonicalPath());
		// Set up the root catalog directories
		for (int i = 0; i < TEST_ROOT_DIRS.length; i++) {
			final File rootDir = new File(testCatalog, TEST_ROOT_DIRS[i]);
			logger.info("root dir: " + rootDir.getCanonicalPath());
			rootDirs[i] = rootDir.getCanonicalPath();
		}
	}
	
	@After
	public void tearDown() throws Exception {
		logger.info("--- --- --- tearDown() --- --- ---");
		final File tmpDir = new File(System.getenv("TEMP"));
		final File rootDir = new File(tmpDir, "catalog");
		logger.info("cleaning: " + rootDir.getCanonicalPath());
		FileUtils.cleanDirectory(rootDir);
	}
	
	@Test
	public void testGetRootFolderWithRelativeForwardSlashesPath() throws Exception {
		logger.info("--- testGetRootFolderWithRelativeForwardSlashesPath() ---");
		// Set up test data
		CatalogManager catalogManager = new CatalogManager(TEST_FOLDERS[0]);
		
		// Run test
		final String rootFolder = catalogManager.getCatalogRootPath();
		
		// Check results
		assertEquals(EXPECTED_FOLDERS[0], rootFolder);
	}

	@Test
	public void testGetRootFolderWithRelativeBackSlashesPath() throws Exception {
		logger.info("--- testGetRootFolderWithRelativeBackSlashesPath() ---");
		// Set up test data
		CatalogManager catalogManager = new CatalogManager(TEST_FOLDERS[1]);
		
		// Run test
		final String rootFolder = catalogManager.getCatalogRootPath();
		
		// Check results
		assertEquals(EXPECTED_FOLDERS[1], rootFolder);
	}
	
	@Test 
	public void testGetRootDirs() throws Exception {
		logger.info("--- testGetRootDirs() ---");
		// Set up test data
		CatalogManager catalogManager = new CatalogManager(testCatalog.getCanonicalPath(), true);
		catalogManager.bypassServletContext(rootDirs);
		
		// Run test
		final List<String> testRootDirs = catalogManager.getRootDirs(new UserBean());
		
		// Check results
		for (int i=0; i < testRootDirs.size(); i++) {
			logger.info("rootDir: " + testRootDirs.get(i));
			assertEquals(rootDirs[i], testRootDirs.get(i));
		}
	}
	
	@Test 
	@Ignore("Needs full UserBean set up in order to work properly.")
	public void testUserFileNode() throws Exception {
		logger.info("--- testUserFileNode() ---");
		// Set up test data
		CatalogManager catalogManager = new CatalogManager(testCatalog.getCanonicalPath(), true);
		catalogManager.bypassServletContext(rootDirs);
		
		// Run test
		final FileNode fileNode = catalogManager.getUserFileNode(new UserBean());
		
		// Check results
		logger.info("fileNode: " + fileNode.getName());
	}
	
	@Test 
	@Ignore("Needs full UserBean set up in order to work properly.")
	public void testGetDashboardDirectories() throws Exception {
		logger.info("--- testGetDashboardDirectories() ---");
		// Set up test data
		CatalogManager catalogManager = new CatalogManager(testCatalog.getCanonicalPath(), true);
		catalogManager.bypassServletContext(TEST_ROOT_DIRS);
		
		// Run test
		// final List<String> dashboardPaths = catalogManager.getDashboardFolders(new UserBean());
		
		// Check results
		for (String path : dashboardPaths) {
			logger.info("Dashboard path: " + path);
		}
	}
	
	@Test
	@Ignore("Need to mock up UserBean and Repository")
	public void testDeactivateDashboardFolder() throws Exception {
		logger.info("--- testDeactivateDashboardFolder() ---");
		// Set up test data
		CatalogManager catalogManager = new CatalogManager(testCatalog.getCanonicalPath());
		catalogManager.bypassServletContext(TEST_ROOT_DIRS);
		
		// Run test
		final String errorMessages = catalogManager.deactivateDashboardFolder(TEST_DASHBOARD_PATH, null, true);
		
		// Check results
		assertNotNull(errorMessages);
		assertEquals(errorMessages, "");
	}
}
