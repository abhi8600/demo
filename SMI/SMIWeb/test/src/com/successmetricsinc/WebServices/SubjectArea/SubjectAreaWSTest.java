package com.successmetricsinc.WebServices.SubjectArea;

import com.successmetricsinc.Group;
import com.successmetricsinc.Repository;
import org.apache.axiom.om.OMElement;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.Before;
import org.springframework.mock.web.MockHttpSession;

import javax.servlet.http.HttpSession;

import com.successmetricsinc.UI.UserBean;
import com.successmetricsinc.UI.adhoc.Adhoc;

import java.util.ArrayList;
import java.util.List;
import java.io.File;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.jdom.Document;
import org.jdom.input.SAXBuilder;

public class SubjectAreaWSTest {

    private static final String TEST_DATA_ROOT = "SMIWeb\\test\\data";
    private static final String TEST_SPACE_ROOT = TEST_DATA_ROOT + File.separator
            + "catalog\\Data\\12345678-1234-1234-1234-123456789012";
    private static final String TEST_USER_ROOT = TEST_SPACE_ROOT + File.separator + "catalog";
    private static final String REPOSITORY_FILEPATH = TEST_SPACE_ROOT + File.separator + Repository.REPOSITORY_FILENAME;
    private static final String SECURIAN_FILEPATH = TEST_SPACE_ROOT + File.separator + "repository.sec.xml";

	private Logger logger = Logger.getLogger(SubjectAreaWSTest.class);

    private SubjectAreaWS webservice;

    @Before
    public void setUp() throws Exception {
        // Set up test data
        webservice = new SubjectAreaWS();
        final HttpSession mockHttpSession = new MockHttpSession();
        // Create UserBean
        final UserBean userBean = new UserBean();
        userBean.setEmbedded(false);
        userBean.setUserRootDirectory(TEST_USER_ROOT);
        // Add Group list to UserBean
        List<Group> testGroupList = new ArrayList<Group>();
        List<String> userNames = new ArrayList<String>();
        userNames.add("Test User Name");
        testGroupList.add(new Group("USER$", userNames, true));
        userBean.setTestGroupList(testGroupList);
        // Add Repository to UserBean
		SAXBuilder builder = new SAXBuilder();
        File fi = new File(SECURIAN_FILEPATH);
	    Document repositoryDocument = builder.build(fi);
	    Repository repository = new Repository(repositoryDocument);
        userBean.setTestRepository(repository);
        // Add UserBean to HTTP session
        mockHttpSession.setAttribute("userBean", userBean);
        // Add Adhoc to HTTP session
        mockHttpSession.setAttribute("adhoc", new Adhoc(userBean));
        // Add HTTP session to webservice
        webservice.setTestHttpSession(mockHttpSession);
    }

	@Test
	public void testGetSubjectArea() throws Exception {
		logger.info("entering 'testGetSubjectArea()'...");
		// Set up test data

		// Run test
		final OMElement result = webservice.getSubjectArea(SubjectAreaWS.CACHE_NODE_KEY);
		
		// Check results
		assertNotNull(result);
        OMElement firstElement = result.getFirstElement();
        assertEquals("ErrorCode", firstElement.getLocalName());
        assertEquals("0", firstElement.getText());
        
		logger.info("exiting 'testGetSubjectArea()'...");
	}
}
