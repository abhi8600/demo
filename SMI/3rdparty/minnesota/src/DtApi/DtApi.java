package DtApi;
// DtApi.java
public class DtApi {
	
	// Error codes returned by DTLastError:
	public static final int DTE_DTLOC=0x0001;                // Lookup tables/matchcode file not found    
	public static final int DTE_CASSLOC=0x0002;              // CASS tables not found                     
	public static final int DTE_MATCHCODE=0x0003;            // Matchcode not found in matchcode database 
	public static final int DTE_KEYFILE=0x0004;              // Key file is missing or corrupt            
	public static final int DTE_KEYINDEX=0x0005;             // Key index file is missing or corrupt      
	public static final int DTE_NOCASS=0x0006;               // CASS not available                        
	
	// Matchcode Component Data Types returned by DTComponentType:
	public static final int MC_PREFIX=0x01;            			 // Name's prefix
	public static final int MC_FIRST=0x02;           				 // Name's first name
	public static final int MC_MIDDLE=0x03;            			 // Name's middle name
	public static final int MC_LAST=0x04;                    // Name's last name                             
	public static final int MC_SUFFIX=0x05;                  // Name's suffix                                
	public static final int MC_GENDER=0x06;                  // Sex of a name                                
	public static final int MC_NFIRST=0x07;                  // First name's nickname                        
	public static final int MC_NMIDDLE=0x08;                 // Middle name's nickname                       
	public static final int MC_TITLE=0x09;                   // Title and/or department                      
	public static final int MC_COMP=0x0a;                    // Company                                      
	public static final int MC_COMPACR=0x0b;                 // Company acronym                              
	public static final int MC_SNUMBER=0x0c;                 // Street number                                
	public static final int MC_SPREDIR=0x0d;                 // Street pre-directional                       
	public static final int MC_SNAME=0x0e;                   // Street name                                  
	public static final int MC_SSUFFIX=0x0f;                 // Street suffix                                
	public static final int MC_SPSTDIR=0x10;                 // Street post-directional                      
	public static final int MC_SPOB=0x11;                    // Street PO Box                                
	public static final int MC_SSECO=0x12;                   // Street secondary                             
	public static final int MC_ADDR=0x13;                    // Full address                                 
	public static final int MC_CITY=0x14;                    // City                                         
	public static final int MC_STATE=0x15;                   // State or province                            
	public static final int MC_ZIP9=0x16;                    // Full Zip+4 code                              
	public static final int MC_ZIP5=0x17;                    // Zip code                                     
	public static final int MC_ZIP4=0x18;                    // +4 extension                                 
	public static final int MC_VZIP9=0x19;                   // Validated full Zip+4 code                    
	public static final int MC_VZIP5=0x1a;                   // Validated Zip code                           
	public static final int MC_VZIP4=0x1b;                   // Validated +4 extension                       
	public static final int MC_COUNTRY=0x1c;                 // Country                                      
	public static final int MC_CANPC=0x1d;                   // Canadian postal code                         
	public static final int MC_UKCITY=0x1e;                  // City (UK)                                    
	public static final int MC_UKCOUNTY=0x1f;                // County (UK)                                  
	public static final int MC_UKPC=0x20;                    // Postcode (UK)                                
	public static final int MC_PHONE=0x21;                   // Phone/fax                                    
	public static final int MC_EMAIL=0x22;                   // E-mail address                               
	public static final int MC_CREDCARD=0x23;                // Credit card number                           
	public static final int MC_GENERAL=0x24;                 // General information                          
	public static final int MC_CUSTOM=0x25;                  // Custom processing                            
	public static final int MC_XADDR1=0xfa;                  // First address line (mapping only)            
	public static final int MC_XADDR2=0xfb;                  // Second address line (mapping only)           
	public static final int MC_XADDR3=0xfc;                  // Third address line (mapping only)            
	
	// Matchcode Component Start settings (returned by DTComponentStart):
	public static final int MO_LEFT=0x0008;                  // Start at left
	public static final int MO_RIGHT=0x0010;          			 // Start at right
	public static final int MO_SPOS=0x0020;                  // Start at specified position
	public static final int MO_SWORD=0x0040;                 // Start at specified word
	
	// Matchcode Component Trim settings (returned by DTComponentTrim):
	public static final int MO_LTRIM=0x0002;          			 // Trim left whitespace
	public static final int MO_RTRIM=0x0004;          			 // Trim right whitespace
	
	// Matchcode Component Fuzzy settings (returned by DTComponentFuzzy):
  public static final int MF_SOUNDEX=0x0001;               // SoundEx matching                           
  public static final int MF_PHONETEX=0x0002;              // Phonetex matching                          
  public static final int MF_CONTAINS=0x0004;              // Containment matching                       
  public static final int MF_FREQ=0x0008;                  // Frequency matching                         
  public static final int MF_FNEAR=0x0010;                 // Fast near matching (setting specified)     
  public static final int MF_ANEAR=0x0020;                 // Accurate near matching (setting specified) 
  public static final int MF_VOWEL=0x0040;                 // Vowels only matching                       
  public static final int MF_CONST=0x0080;                 // Consonants only matching                   
  public static final int MF_ALPHA=0x0100;                 // Alphabetic only matching                   
  public static final int MF_NUMERIC=0x0200;               // Numeric only matching                      
  public static final int MF_FREQNEAR=0x0400;              // Frequency near matching

  // Matchcode Component Short/Empty settings (returned by DTComponentShort):
  public static final int MO_EMPTY=0x0100;                 // Both blank fields          
  public static final int MO_SHORT0=0x0200;                // One blank field            
  public static final int MO_SHORT1=0x0400;                // Initial only               

  // Matchcode Component Swap settings (returned by DTComponentSwap):
  public static final int MW_SWAPA=0x0001;                 // In swap pair A    
  public static final int MW_SWAPB=0x0002;                 // In swap pair B    
  public static final int MW_SWAPC=0x0004;                 // ...               
  public static final int MW_SWAPD=0x0008;
  public static final int MW_SWAPE=0x0010;
  public static final int MW_SWAPF=0x0020;
  public static final int MW_SWAPG=0x0040;
  public static final int MW_SWAPH=0x0080;

  // Matchcode Component Combination settings (returned by DTComponentCombinations
  public static final int MM_COMBO1=0x0001;                // Combination #1 
  public static final int MM_COMBO2=0x0002;                // Combination #2 
  public static final int MM_COMBO3=0x0004;                // Combination #3 
  public static final int MM_COMBO4=0x0008;                // ...            
  public static final int MM_COMBO5=0x0010;
  public static final int MM_COMBO6=0x0020;
  public static final int MM_COMBO7=0x0040;
  public static final int MM_COMBO8=0x0080;
  public static final int MM_COMBO9=0x0100;
  public static final int MM_COMBO10=0x0200;
  public static final int MM_COMBO11=0x0400;
  public static final int MM_COMBO12=0x0800;
  public static final int MM_COMBO13=0x1000;
  public static final int MM_COMBO14=0x2000;
  public static final int MM_COMBO15=0x4000;
  public static final int MM_COMBO16=0x8000;

  // Mapping types (DataType parameter for DTMapComponent):
  public static final int MP_PREFIX=0x00;                  // Name's prefix                                                
  public static final int MP_GENDER=0x01;                  // Name's gender                                                
  public static final int MP_FIRST=0x02;                   // Name's first name                                            
  public static final int MP_MFIRST=0x03;                  // Name's mixed first name (Pre-FN-MN)                          
  public static final int MP_MIDDLE=0x04;                  // Name's middle name                                           
  public static final int MP_LAST  =0x05;                  // Name's last name                                             
  public static final int MP_MLAST =0x06;                  // Name's mixed last name (LN-Suf)                              
  public static final int MP_SUFFIX=0x07;                  // Name's suffix                                                
  public static final int MP_FULL=0x08;                    // Full name (Pre-FN-MN-LN-Suf)                                 
  public static final int MP_INV =0x09;                    // Inverse name (LN-Suf-Pre-FN-MN)                              
  public static final int MP_GINV=0x0a;                    // Govt inverse name (LN-FN-MN-Suf)                             
  public static final int MP_TITLE=0x0b;                   // Title and/or department                                      
  public static final int MP_COMP=0x0c;                    // Company                                                      
  public static final int MP_ADDR=0x0d;                    // Full address                                                 
  public static final int MP_CITY=0x0e;                    // City                                                         
  public static final int MP_STATE=0x0f;                   // State                                                        
  public static final int MP_ZIP9=0x10;                    // Zip+4 code                                                   
  public static final int MP_ZIP5=0x11;                    // Zip code                                                     
  public static final int MP_ZIP4=0x12;                    // +4 code                                                      
  public static final int MP_CSZ =0x13;                    // City/state/zip                                               
  public static final int MP_COUNTRY=0x14;                 // Country                                                      
  public static final int MP_CANPC=0x15;                   // Canadian postal code                                         
  public static final int MP_UKCITY =0x16;                 // City (UK)                                                    
  public static final int MP_UKCOUNTY=0x17;                // County (UK)                                                  
  public static final int MP_UKPC=0x18;                    // Postcode (UK)                                                
  public static final int MP_UKCCP=0x19;                   // City/County/Postcode (UK)                                    
  public static final int MP_PHONE=0x1a;                   // Phone/fax                                                    
  public static final int MP_EMAIL=0x1b;                   // E-mail address                                               
  public static final int MP_CREDCARD=0x1c;                // Credit card number                                           
  public static final int MP_GENERAL=0x1d;                 // General information                                          

  // Status codes returned by DTMatchRecord, DTAddRecord, DTMatchResults, DTReadRecord
  public static final int EM_DUPEUSE=0x0010;               // Record has a dupe
  public static final int EM_DUPE=0x0020;                  // Record is a dupe
  public static final int EM_EMASK =0x003f;

  // Status codes returned by DTMatchRecord, DTAddRecord, DTMatchResults, DTReadRecord, DTCompare
  public static final int EM_COMBO1=0x80000000;            // Matched on combination #1     
  public static final int EM_COMBO2=0x40000000;            // Matched on combination #2     
  public static final int EM_COMBO3=0x20000000;            // ...                           
  public static final int EM_COMBO4=0x10000000;
  public static final int EM_COMBO5=0x08000000;
  public static final int EM_COMBO6=0x04000000;
  public static final int EM_COMBO7=0x02000000;
  public static final int EM_COMBO8=0x01000000;
  public static final int EM_COMBO9=0x00800000;
  public static final int EM_COMBO10=0x00400000;
  public static final int EM_COMBO11=0x00200000;
  public static final int EM_COMBO12=0x00100000;
  public static final int EM_COMBO13=0x00080000;
  public static final int EM_COMBO14=0x00040000;
  public static final int EM_COMBO15=0x00020000;
  public static final int EM_COMBO16=0x00010000;
  public static final int EM_MMASK=0xffff0000;

  // Status codes returned by DTCASSAddress:
  public static final int EC_MULTIPLE=0x00000004;          // Error Code M
  public static final int EC_NOSTREET=0x00000008;          // Error Code N
  public static final int EC_RANGE=0x00000010;             // Error Code R
  public static final int EC_COMPONENT=0x00000020;         // Error Code T
  public static final int EC_STREET=0x00000040;            // Error Code U
  public static final int EC_EWS=0x00000080;               // Error Code W
  public static final int EC_NONDELIVERABLE=0x00000100;    // Error Code X
  public static final int EC_ZIP=0x00000200; 							 // Error Code Z
  public static final int EC_HRDEFAULT=0x00000400;         // High Rise Default
  public static final int EC_HREXACT=0x00000800;           // High Rise Exact
  public static final int EC_RRDEFAULT=0x00001000;         // Rural Route Default
  public static final int EC_RREXACT=0x00002000;           // Rural Route Exact
  public static final int EC_LACS=0x00004000;              // LACS
  public static final int EC_EWSCODE=0x00008000;           // EWS
  public static final int EC_ZIP5MATCH=0x00010000; 
  public static final int EC_CRRTMATCH=0x00020000;
  public static final int EC_CASSMATE=0x00040000;
  public static final int EC_ERRORMASK=0x000003ff;
	
	// General purpose functions:
	public native static boolean DTDebugMessages(boolean Debug);
	public native static boolean DTRegister(String Str);
	public native static int DTVersion();
	public native static int DTLastError();
	
	// Environment functions:
	public native static boolean DTSetTranslationTable(String Loc);
	public native static boolean DTSetFileLocCass(String Loc);
	public native static boolean DTSetFileLocCompany(String Loc);
	public native static boolean DTSetFileLocCountry(String Loc);
	public native static boolean DTSetFileLocCsz(String Loc);
	public native static boolean DTSetFileLocDepartment(String Loc);
	public native static boolean DTSetFileLocEMail(String Loc);
	public native static boolean DTSetFileLocName(String Loc);
	public native static boolean DTSetFileLocStreet(String Loc);
	public native static boolean DTSetFileLocUKPostcode(String Loc);
	public native static boolean DTSetFileLocDTakeMC(String Loc);
	
	// Special performance functions:
	public native static boolean DTSortCommand(String Cmd);
	public native static int DTThreadSlice(int SliceSize);
	public native static int DTOptimalCompares(int Compares);
	public native static int DTSortMemory(int SortMemory);
	public native static int DTSortThreads(int SortThreads);
	
	// Matchcode information functions:
	public native static String DTGetMatchcodeName(long hDeduper);	
	public native static int DTGetComponentCount(long hDeduper);
	public native static int DTGetComponentType(long hDeduper,int Pos);
	public native static int DTGetComponentSize(long hDeduper,int Pos);
	public native static String DTGetComponentLabel(long hDeduper,int Pos);
	public native static int DTGetComponentWords(long hDeduper,int Pos);
	public native static int DTGetComponentStart(long hDeduper,int Pos);
	public native static int DTGetComponentStartWord(long hDeduper,int Pos);
	public native static int DTGetComponentTrim(long hDeduper,int Pos);
	public native static int DTGetComponentFuzzy(long hDeduper,int Pos);
	public native static int DTGetComponentNear(long hDeduper,int Pos);
	public native static int DTGetComponentShort(long hDeduper,int Pos);
	public native static int DTGetComponentSwap(long hDeduper,int Pos);
	public native static long DTGetComponentCombinations(long hDeduper,int Pos);

	// Matchcode mapping functions:
	public native static int DTGetMapCount(long hDeduper);
	public native static int DTGetMapComponentType(long hDeduper,int Pos);
	public native static String DTGetMapComponentLabel(long hDeduper,int Pos);
	
	// Functions used by Incremental, Read/Write, and Hybrid:
	public native static boolean DTMapComponent(long hDeduper,int Comp,int DataType);
	public native static boolean DTMapComponentEx(long hDeduper,int Comp,int DataType);
	public native static String DTBuildKey(long hDeduper,String Fields);
	public native static int DTKeySize(long hDeduper);
	public native static int DTBlockSize(long hDeduper);
	public native static long DTCompare(long hDeduper,String Str1,String Str2,int Offset);
	public native static boolean DTClose(long hDeduper);
	
	// Hybrid deduping functions:
	public native static long DTInitHybrid(String Matchcode,String DtLoc,String CassLoc);
	
	// Read/write deduping functions:
	public native static long DTInitReadWrite(String Matchcode,String KeyFile,String DtLoc,String CassLoc);
	public native static boolean DTSetGroupSorting(long hDeduper,boolean SortGroups);
	public native static boolean DTWriteRecord(long hDeduper,String Key,short Source,int Record);
	public native static boolean DTStartProcessing(long hDeduper);
	public native static boolean DTReadRecord(long hDeduper);
	public native static String DTGetReadKey(long hDeduper);
	public native static short DTGetReadSource(long hDeduper);
	public native static int DTGetReadRecord(long hDeduper);
	public native static int DTGetReadDupeGroup(long hDeduper);
	public native static long DTGetReadError(long hDeduper);
	public native static int DTGetReadCount(long hDeduper);
	public native static int DTGetReadEntry(long hDeduper);
	
	// Incremental deduping functions:
	public native static long DTInitIncremental(String Matchcode,String KeyFile,String DtLoc,String CassLoc,boolean MustExist);
	public native static long DTMatchRecord(long hDeduper,String Key);
	public native static long DTAddRecord(long hDeduper,String Key,short Source,int Record);
	public native static int DTMatchCount(long hDeduper);
	public native static int DTMatchGroup(long hDeduper);
	public native static short DTMatchResultSource(long hDeduper,int Entry);
	public native static int DTMatchResultRecord(long hDeduper,int Entry);
	public native static int DTMatchResultGroup(long hDeduper,int Entry);
	public native static long DTMatchResultError(long hDeduper,int Entry);
	
	// Street splitting functions:
	public native static long DTInitStreetSplit(String DtLoc);
	public native static boolean DTSplitStreet(long hSplitter,String In1,String In2,String In3,String Csz);
	public native static String DTStreetNumber(long hSplitter);
	public native static String DTStreetPreDir(long hSplitter);
	public native static String DTStreetName(long hSplitter);
	public native static String DTStreetSuffix(long hSplitter);
	public native static String DTStreetPostDir(long hSplitter);
	public native static String DTStreetSecondary(long hSplitter);
	public native static String DTStreetPOBox(long hSplitter);
	public native static String DTStreetPattern(long hSplitter);	
	
	// CASS functions:
	public native static long DTInitCASS(String DtLoc,String CassLoc,boolean CassMate);
	public native static long DTCASSAddress(long hCASS,String Company,String In1,String In2,String Suite,String City,String State,String Zip5,String Zip4);
	public native static String DTCASSCompany(long hCASS);
	public native static String DTCASSAdd1(long hCASS);
	public native static String DTCASSAdd2(long hCASS);
	public native static String DTCASSSuite(long hCASS);
	public native static String DTCASSNumber(long hCASS);
	public native static String DTCASSPreDir(long hCASS);
	public native static String DTCASSName(long hCASS);
	public native static String DTCASSSuffix(long hCASS);
	public native static String DTCASSPostDir(long hCASS);
	public native static String DTCASSPOBox(long hCASS);
	public native static String DTCASSSecondary(long hCASS);
	public native static String DTCASSCity(long hCASS);
	public native static String DTCASSState(long hCASS);
	public native static String DTCASSZip5(long hCASS);
	public native static String DTCASSZip4(long hCASS);
	public native static String DTCASSCarrierRoute(long hCASS);
	public native static String DTCASSDeliveryPoint(long hCASS);
	public native static String DTCASSDeliveryPointCD(long hCASS);
	public native static String DTCASSUrbanization(long hCASS);
	public native static String DTCASSAbbCity(long hCASS);
	public native static String DTCASSFIPS(long hCASS);
	public native static String DTCASSTimeZone(long hCASS);
	public native static String DTCASSMSA(long hCASS);
	public native static String DTCASSPMSA(long hCASS);
	public native static String DTCASSDistrict(long hCASS);
	public native static double DTCASSLatitude(long hCASS);
	public native static double DTCASSLongitude(long hCASS);
	public native static String DTCASSLACS(long hCASS);
	public native static String DTCASSEWS(long hCASS);
	public native static String DTCASSAddrType(long hCASS);
	public native static String DTCASSError(long hCASS);
	public native static String DTCASSDatabaseDate();
	public native static String DTCASSExpirationDate();
}