/**
 * $Id: SeriesXYZDataset.java,v 1.2 2008-01-25 17:13:03 ricks Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package org.jfree.data.xy;

import java.util.ArrayList;
import java.util.List;
import org.jfree.data.xy.AbstractXYZDataset;

/**
 * @author Brad Peters
 * 
 */
public class SeriesXYZDataset extends AbstractXYZDataset
{
	private static final long serialVersionUID = 1L;
	private List<String> seriesNames = new ArrayList<String>();
	protected List<Number[]> xValues = new ArrayList<Number[]>();
	protected List<Number[]> yValues = new ArrayList<Number[]>();
	protected List<Number[]> zValues = new ArrayList<Number[]>();

	public SeriesXYZDataset()
	{
	}

	public void addSeries(String name, Double[] xData, Double[] yData, Double[] zData)
	{
		seriesNames.add(name);
		xValues.add(xData);
		yValues.add(yData);
		zValues.add(zData);
	}

	@Override
	public int getSeriesCount()
	{
		return (seriesNames.size());
	}

	@Override
	public Comparable getSeriesKey(int series)
	{
		return (seriesNames.get(series));
	}

	public int getItemCount(int series)
	{
		return (xValues.get(series).length);
	}

	public Number getX(int series, int item)
	{
		return (xValues.get(series)[item]);
	}

	public Number getY(int series, int item)
	{
		return (yValues.get(series)[item]);
	}

	public Number getZ(int series, int item)
	{
		return (zValues.get(series)[item]);
	}

	/**
	 * Returns the maximum z-value.
	 * 
	 * @return The maximum z-value.
	 */
	public double getMaxZValue()
	{
		double zMax = -1.e20;
		for (Number[] arr : zValues)
		{
			for (int i = 0; i < arr.length; i++)
			{
				if (arr[i] != null)
				{
					zMax = Math.max(zMax, arr[i].doubleValue());
				}
			}
		}
		return zMax;
	}

	/**
	 * Returns the minimum z-value.
	 * 
	 * @return The minimum z-value.
	 */
	public double getMinZValue()
	{
		double zMin = 1.e20;
		for (Number[] arr : zValues)
		{
			for (int i = 0; i < arr.length; i++)
			{
				if (arr[i] != null)
				{
					zMin = Math.min(zMin, arr[i].doubleValue());
				}
			}
		}
		return zMin;
	}
}
