/**
 * $Id: XYQuadrantPlot.java,v 1.3 2008-01-25 17:13:03 ricks Exp $
 *
 * Copyright (C) 2007-2008 Success Metrics, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package org.jfree.chart.plot;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.List;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.XYDataset;

public class XYQuadrantPlot extends XYPlot
{
	private List<String> names;

	public XYQuadrantPlot(XYDataset dataset, ValueAxis domainAxis, ValueAxis rangeAxis, XYItemRenderer renderer)
	{
		super(dataset, domainAxis, rangeAxis, renderer);
	}
	private static final long serialVersionUID = 1L;

	public boolean render(Graphics2D g2, Rectangle2D dataArea, int index, PlotRenderingInfo info, CrosshairState crosshairState)
	{
		int x = (int) (dataArea.getMaxX() + dataArea.getMinX()) / 2;
		int y = (int) (dataArea.getMaxY() + dataArea.getMinY()) / 2;
		g2.setColor(Color.BLACK);
		g2.setStroke(new BasicStroke());
		g2.drawLine((int) dataArea.getMinX(), y, (int) dataArea.getMaxX(), y);
		g2.drawLine(x, (int) dataArea.getMinY(), x, (int) dataArea.getMaxY());
		if (names != null)
		{
			Font oldFont = g2.getFont();
			g2.setFont(this.getDomainAxis().getLabelFont());
			FontMetrics fm = g2.getFontMetrics();
			if (names.size() >= 1)
			{
				Rectangle2D bounds = fm.getStringBounds(names.get(0), g2);
				g2.drawString(names.get(0), ((int) dataArea.getMinX() + x) / 2 - (bounds.getBounds().width / 2), (int) (dataArea.getMinY() + y) / 2);
			}
			if (names.size() >= 2)
			{
				Rectangle2D bounds = fm.getStringBounds(names.get(1), g2);
				g2.drawString(names.get(1), ((int) dataArea.getMaxX() + x) / 2 - (bounds.getBounds().width / 2), (int) (dataArea.getMinY() + y) / 2);
			}
			if (names.size() >= 3)
			{
				Rectangle2D bounds = fm.getStringBounds(names.get(2), g2);
				g2.drawString(names.get(2), ((int) dataArea.getMinX() + x) / 2 - (bounds.getBounds().width / 2), (int) (dataArea.getMaxY() + y) / 2);
			}
			if (names.size() >= 4)
			{
				Rectangle2D bounds = fm.getStringBounds(names.get(3), g2);
				g2.drawString(names.get(3), ((int) dataArea.getMaxX() + x) / 2 - (bounds.getBounds().width / 2), (int) (dataArea.getMaxY() + y) / 2);
			}
			g2.setFont(oldFont);
			super.render(g2, dataArea, index, info, crosshairState);
		}
		return false;
	}

	public void setQuadrantNames(List<String> names)
	{
		this.names = names;
	}
}
