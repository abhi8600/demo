/**
 * 
 */
package net.sf.jasperreports.engine.export;

/**
 * @author bpeters
 *
 */
public interface XMLExporter
{
	public String getXML(double scaleFactor, boolean animation, boolean isPrinted);
}
