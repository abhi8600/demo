/**
 * $Id: JRTooManyPagesException.java,v 1.1 2012-08-30 00:41:45 BIRST\agarrison Exp $
 *
 * Copyright (C) 2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package net.sf.jasperreports.engine;

@SuppressWarnings("serial")
public class JRTooManyPagesException extends JRRuntimeBirstException {
	public JRTooManyPagesException(String message) {
		super(message);
	}

}
