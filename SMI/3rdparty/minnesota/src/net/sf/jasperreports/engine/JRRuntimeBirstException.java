/**
 * $Id: JRRuntimeBirstException.java,v 1.1 2010-10-15 18:20:41 ricks Exp $
 *
 * Copyright (C) 2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package net.sf.jasperreports.engine;

import net.sf.jasperreports.engine.JRException;

@SuppressWarnings("serial")
public class JRRuntimeBirstException extends JRException {

	public JRRuntimeBirstException(String message) {
		super(message);
	}
}