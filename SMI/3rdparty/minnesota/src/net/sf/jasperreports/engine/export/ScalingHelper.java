package net.sf.jasperreports.engine.export;

/**
 * This is a Birst-specific utility class that provides scaling of chart images.
 * See {@link JRPdfExporter}. 
 * @author pconnolly
 */
public class ScalingHelper {

	/**
	 * Used in {@link #boostRenderSize(double)} and {@link #adjustImageForPage(double, double, double, double)}.
	 */
	private static final int RENDER_BOOST_RATIO = 2;
	private static final int LANDSCAPE_WIDTH_TARGET = 300 * RENDER_BOOST_RATIO;
	private static final int LANDSCAPE_HEIGHT_TARGET = 250 * RENDER_BOOST_RATIO;
	private static final int PORTRAIT_WIDTH_TARGET = 250 * RENDER_BOOST_RATIO;
	private static final int PORTRAIT_HEIGHT_TARGET = 300 * RENDER_BOOST_RATIO;

	/**
	 * BIRST-specific code.
	 * <p/>
	 * Boosts the width and height dimension of the image by {@link #RENDER_BOOST_RATIO} to be 
	 * rendered so that it will have adequate resolution after rendering.  Once rendered, the 
	 * calling method will then call {@link #adjustImageForPage(double, double, double, double)} 
	 * to attempt a "best fit" of the image to the PDF exported page.
	 * @param renderSize is the display size in pixels.
	 */
	public static double boostRenderSize(double renderSize) {
		return renderSize * RENDER_BOOST_RATIO;
	}
	
	/**
	 * Adjusts the 'x' & 'y' ratios so that the rendered image will be resized for a 
	 * "best fit" of the image to the PDF exported page.  The "ideal" portrait sizes:
	 * <ul>
	 * <li>{@link #PORTRAIT_HEIGHT_TARGET}</li>
	 * <li>{@link #PORTRAIT_WIDTH_TARGET}</li>
	 * </ul>
	 * And the "ideal" landscape sizes are set by:
	 * <ul>
	 * <li>{@link #LANDSCAPE_HEIGHT_TARGET}</li>
	 * <li>{@link #LANDSCAPE_WIDTH_TARGET}</li>
	 * </ul>
	 * Since targets are 'int's and ratios are 'double's, the targets are approximate.
	 * <p/>
	 * The PDF document creation will further adjust this "final" image to fit the available
	 * sections of the page.
	 * @param displayWidth is the display width in pixels, increased by the amount in
	 * {@link #boostDisplaySize(double, double)}.
	 * @param displayHeight is the display height in pixels, increased by the amount in
	 * {@link #boostDisplaySize(double, double)}.
	 * @param ratioX is the ratio by which to adjust the width of the rendered image.
	 * @param ratioY is the ratio by which to adjust the height of the rendered image.
	 */
	public static double adjustImageForPage(final double displayWidth, final double displayHeight) { 
		double trialRatioX = 0.0;
		double trialRatioY = 0.0;
		// Handle square inputs
		if (displayWidth == displayHeight) {
			trialRatioX = RENDER_BOOST_RATIO;
			trialRatioY = RENDER_BOOST_RATIO;
		} else {
			// Handle landscape inputs
			if (displayWidth >= displayHeight) {
				if (displayWidth > LANDSCAPE_WIDTH_TARGET) {
					// Adjust landscape width downwards towards target
					trialRatioX = LANDSCAPE_WIDTH_TARGET / displayWidth;
				}
				if (displayHeight > LANDSCAPE_HEIGHT_TARGET) {
					// Adjust landscape height downwards towards target
					trialRatioY = LANDSCAPE_HEIGHT_TARGET / displayHeight;
				}
			} else {
				// Otherwise, handle portrait inputs
				if (displayWidth > PORTRAIT_WIDTH_TARGET) {
					// Adjust portrait width downwards towards target
					trialRatioX = PORTRAIT_WIDTH_TARGET / displayWidth;
				}
				if (displayHeight > PORTRAIT_HEIGHT_TARGET) {
					// Adjust portrait height downwards towards target
					trialRatioY = PORTRAIT_HEIGHT_TARGET / displayHeight;
				}
			}
		}
		
		// If both are smaller than the target, just use the standard adjustment
		double ratio = 0.0;
		if (trialRatioX == 0.0 && trialRatioY == 0.0) {
			ratio = RENDER_BOOST_RATIO;
		} else {
			// If 'x' ratio is zero, use 'y' ratio
			if (trialRatioX == 0.0) {
				ratio = trialRatioY;
			} else {
				// If 'y' ratio is zero, use 'x' ratio
				if (trialRatioY == 0.0) {
					ratio = trialRatioX;
				} else {
					// Otherwise, use the smaller ratio
					if (trialRatioX >= trialRatioY) {
						ratio = trialRatioY;
					} else {
						if (trialRatioX < trialRatioY) {
							ratio = trialRatioX;
						}
					}
				}
			}
		}
		return ratio > 0.50 ? 0.50 : ratio;
	}
}
