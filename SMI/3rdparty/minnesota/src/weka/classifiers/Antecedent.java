package weka.classifiers;

public class Antecedent
{
	int index;	// attribute index
	String op;	// <, <=, =, >, >=,
	double right; // value
	
	public Antecedent(int index, String op, double right)
	{
		this.index = index;
		this.op = op;
		this.right = right;
	}
}
  