﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Acorn;

namespace AcornUploader
{
    class UploaderWrapper
    {
        bool filterExcelRows;
        string spaceDirectory;
        string dir;
        string name;
        string oleDBConnectString;
        List<string[]> restrictions = new List<string[]>();
        bool pruneBlank;
        HashSet<string> processedFiles = new HashSet<string>();
        int beginSkip = 0;
        int endSkip = 0;

        public void uploadAndInitScan(string[] args)
        {
            validateAndExtractArgs(args);
            DataTable newErrorTable = ApplicationUploader.getNewErrorTable();
            OldedbWrapper oledbwrapper = new OldedbWrapper(filterExcelRows, spaceDirectory, dir, name, oleDBConnectString, restrictions, pruneBlank, newErrorTable, processedFiles, beginSkip, endSkip);
            ScanSummaryList list = oledbwrapper.processOleDBFile();
            if (list == null)
            {
                list = new ScanSummaryList();
            }

            list.processedFiles = processedFiles;
            list.errors = formatErrorTableToArray(newErrorTable);
            ScanSummaryList.serializeScanResults(list, ScanSummaryList.getScanResultsFileName(spaceDirectory, name));
        }

        private string[][] formatErrorTableToArray(DataTable errorTable)
        {
            string[][] response = null;
            if (errorTable != null && errorTable.Rows != null && errorTable.Rows.Count > 0)
            {
                int numErrors = errorTable.Rows.Count;
                response = new string[numErrors][];
                for (int i = 0; i < numErrors; i++)
                {
                    DataRow errorRow = errorTable.Rows[i];
                    response[i] = new string[] { (string)errorRow[0], (string)errorRow[1], (string)errorRow[2] };
                }
            }

            return response;
        }

        private const string KEY_FILTER_EXCEL_ROWS = "filterExcelRows";
        private const string KEY_SPACE_DIRECTORY = "spaceDirectory";
        private const string KEY_DIRECTORY = "dir";
        private const string KEY_NAME = "name";
        private const string KEY_OLEDB_CONNSTRING = "oleDBConnectString";
        private const string KEY_RESTRICTIONS = "restrictions";
        private const string KEY_PRUNE_BLANK = "pruneBlank";
        private const string KEY_PROCESSED_FILES = "processedFiles";
        private const string KEY_BEGIN_SKIP = "beginSkip";
        private const string KEY_END_SKIP = "endSkip";

        private void validateAndExtractArgs(string[] args)
        {
            HashSet<string> foundKeys = new HashSet<string>();
            foreach (string arg in args)
            {
                int index = arg.IndexOf("=");
                string key = arg.Substring(0, index);
                if (arg.Length == index + 1)
                {
                    Program.log.Info("Skipping : No value supplied " + arg);
                    continue;
                }
                string value = arg.Substring(index + 1);
                foundKeys.Add(key);
                switch (key)
                {
                    case KEY_FILTER_EXCEL_ROWS:
                        filterExcelRows = bool.Parse(value.Trim());
                        break;
                    case KEY_SPACE_DIRECTORY:
                        spaceDirectory = value;
                        break;
                    case KEY_DIRECTORY:
                        dir = value;
                        break;
                    case KEY_NAME:
                        name = value;
                        break;
                    case KEY_OLEDB_CONNSTRING:
                        oleDBConnectString = value;
                        break;
                    case KEY_RESTRICTIONS:
                        string[] allRestricts = value.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                        if (allRestricts != null && allRestricts.Length > 0)
                        {
                            foreach (string str in allRestricts)
                            {
                                string[] restrict = value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                restrictions.Add(restrict);
                            }
                        }
                        break;
                    case KEY_PRUNE_BLANK:
                        pruneBlank = bool.Parse(value);
                        break;
                    case KEY_PROCESSED_FILES:
                        string[] files = value.Split(',');
                        if (files != null && files.Length > 0)
                        {
                            foreach (string file in files)
                            {
                                processedFiles.Add(file);
                            }
                        }
                        break;
                    case KEY_BEGIN_SKIP:
                        try
                        {
                            beginSkip = int.Parse(value);
                        }
                        catch (Exception)
                        {
                        }
                        break;
                    case KEY_END_SKIP:
                        try
                        {
                            endSkip = int.Parse(value);
                        }
                        catch (Exception)
                        {
                        }
                        break;
                    default:
                        break;
                }
            }

            validateKeys(foundKeys);
        }

        private void validateKeys(HashSet<string> foundKeys)
        {
            string[] mandatoryArgs = new string[]{
                KEY_FILTER_EXCEL_ROWS, 
                KEY_SPACE_DIRECTORY,
                KEY_DIRECTORY, 
                KEY_NAME,
                KEY_OLEDB_CONNSTRING,
                KEY_PRUNE_BLANK                
            };
            foreach (string mandatoryArg in mandatoryArgs)
            {
                if (!foundKeys.Contains(mandatoryArg))
                {
                    string errorMsg = "Required argument not supplied : " + mandatoryArg;
                    Program.log.Error(errorMsg);
                    throw new Exception(errorMsg);
                }
            }
        }
    }
}
