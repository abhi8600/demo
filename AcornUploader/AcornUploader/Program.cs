﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Acorn;
using log4net;
using log4net.Config;
using System.IO;

namespace AcornUploader
{
    class Program
    {
        public static readonly ILog log = LogManager.GetLogger("AcornUploader");
        static void Main(string[] args)
        {            
            try
            {
                log4net.Config.XmlConfigurator.Configure();           
                log.Info("Starting the application uploader");
                UploaderWrapper uWrapper = new UploaderWrapper();
                uWrapper.uploadAndInitScan(args);
                log.Info("Finishing the application uploader");
            }
            catch (Exception ex)
            {
                log.Error("Error in uploader - " + ex.Message, ex);
                Environment.Exit(-1);
            }            
        }        
    }
}
