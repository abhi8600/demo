//
//  TDGAnnotationMgr.h
//  objc_pgsdk
//
//  Created by Sam on 11/11/10.
//  Copyright 2010 Three D Graphics. All rights reserved.
//
/** Annotation Manager APIs
*/
#import <Foundation/Foundation.h>
#import "TDGAnnotation.h"

// To reduce the size of the protocol for TDGChart, move the annotation-managing calls here

@interface TDGAnnotationMgr : NSObject
{
	/** Annotations Array */
	NSMutableArray *raAnnotations;
}

/** Returns the number of annotations
*/
- (int) count;

/** Creates a new annotation, adds it to the managed list and returns it
*/
- (TDGAnnotation*) add;

/** Creates a new annotation with the specified text, adds it to the managed list and returns it
*/
- (TDGAnnotation*) addWithText: (NSString*)text;

/** Gets the annotation at the specified index.  Throws exception if index is out of bounds.
*/
- (TDGAnnotation*) getAtIndex: (int)index;

/** Gets the (first) annotation at the specified text; if not found, return nil.
*/
- (TDGAnnotation*) getWithText: (NSString*)text;

/** Removes the annotation at the specified index, returns the removed annotation
*/
- (TDGAnnotation*) removeWithIndex: (int)index;

/** Removes the specified annotation, returns the removed annotation
*/
- (TDGAnnotation*) remove: (TDGAnnotation*)annotation;

@end
