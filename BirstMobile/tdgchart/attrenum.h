#ifndef __API_ATTR_ENUM_H__
#define __API_ATTR_ENUM_H__

#define A2D_RANGE_BEGIN     1
#define A2D_RANGE_END     255

#define A3D_RANGE_BEGIN   256
#define A3D_RANGE_END     451

#define AAN_RANGE_BEGIN   452
#define AAN_RANGE_END     515

#define ATX_RANGE_BEGIN   516
#define ATX_RANGE_END     547

#define A_RANGE_BEGIN     548
#define A_RANGE_END       612

#define A2D_RANGE2_BEGIN  613
#define A2D_RANGE2_END    868

#define A_RANGE2_BEGIN    869
#define A_RANGE2_END     1000


enum pg_Attribute {

#define ATTR_DECLARE( id, name, type, comment ) name = id,
#include "attrid.h"

PG_ATTRIBUTE_MAX
}; // end of enum pg_Attribute list


#endif /* __API_ATTR_ENUM_H__ */
